﻿using System;
using System.ComponentModel;
using System.Threading;

namespace System.Timers
{
	// Token: 0x020004AE RID: 1198
	[System.ComponentModel.DefaultProperty("Interval")]
	[System.ComponentModel.DefaultEvent("Elapsed")]
	public class Timer : System.ComponentModel.Component, System.ComponentModel.ISupportInitialize
	{
		// Token: 0x06002AF9 RID: 11001 RVA: 0x00093B24 File Offset: 0x00091D24
		public Timer() : this(100.0)
		{
		}

		// Token: 0x06002AFA RID: 11002 RVA: 0x00093B38 File Offset: 0x00091D38
		public Timer(double interval)
		{
			if (interval > 2147483647.0)
			{
				throw new ArgumentException("Invalid value: " + interval, "interval");
			}
			this.autoReset = true;
			this.Interval = interval;
		}

		// Token: 0x1400005E RID: 94
		// (add) Token: 0x06002AFB RID: 11003 RVA: 0x00093B90 File Offset: 0x00091D90
		// (remove) Token: 0x06002AFC RID: 11004 RVA: 0x00093BAC File Offset: 0x00091DAC
		[System.ComponentModel.Category("Behavior")]
		[TimersDescription("Occurs when the Interval has elapsed.")]
		public event ElapsedEventHandler Elapsed;

		// Token: 0x17000BCB RID: 3019
		// (get) Token: 0x06002AFD RID: 11005 RVA: 0x00093BC8 File Offset: 0x00091DC8
		// (set) Token: 0x06002AFE RID: 11006 RVA: 0x00093BD0 File Offset: 0x00091DD0
		[System.ComponentModel.Category("Behavior")]
		[System.ComponentModel.DefaultValue(true)]
		[TimersDescription("Indicates whether the timer will be restarted when it is enabled.")]
		public bool AutoReset
		{
			get
			{
				return this.autoReset;
			}
			set
			{
				this.autoReset = value;
			}
		}

		// Token: 0x17000BCC RID: 3020
		// (get) Token: 0x06002AFF RID: 11007 RVA: 0x00093BDC File Offset: 0x00091DDC
		// (set) Token: 0x06002B00 RID: 11008 RVA: 0x00093C34 File Offset: 0x00091E34
		[TimersDescription("Indicates whether the timer is enabled to fire events at a defined interval.")]
		[System.ComponentModel.Category("Behavior")]
		[System.ComponentModel.DefaultValue(false)]
		public bool Enabled
		{
			get
			{
				object @lock = this._lock;
				bool result;
				lock (@lock)
				{
					result = (this.timer != null);
				}
				return result;
			}
			set
			{
				object @lock = this._lock;
				lock (@lock)
				{
					bool flag = this.timer != null;
					if (flag != value)
					{
						if (value)
						{
							this.timer = new Timer(new TimerCallback(Timer.Callback), this, (int)this.interval, (!this.autoReset) ? 0 : ((int)this.interval));
						}
						else
						{
							this.timer.Dispose();
							this.timer = null;
						}
					}
				}
			}
		}

		// Token: 0x17000BCD RID: 3021
		// (get) Token: 0x06002B01 RID: 11009 RVA: 0x00093CE4 File Offset: 0x00091EE4
		// (set) Token: 0x06002B02 RID: 11010 RVA: 0x00093CEC File Offset: 0x00091EEC
		[System.ComponentModel.DefaultValue(100)]
		[TimersDescription("The number of milliseconds between timer events.")]
		[System.ComponentModel.RecommendedAsConfigurable(true)]
		[System.ComponentModel.Category("Behavior")]
		public double Interval
		{
			get
			{
				return this.interval;
			}
			set
			{
				if (value <= 0.0)
				{
					throw new ArgumentException("Invalid value: " + value);
				}
				object @lock = this._lock;
				lock (@lock)
				{
					this.interval = value;
					if (this.timer != null)
					{
						this.timer.Change((int)this.interval, (!this.autoReset) ? 0 : ((int)this.interval));
					}
				}
			}
		}

		// Token: 0x17000BCE RID: 3022
		// (get) Token: 0x06002B03 RID: 11011 RVA: 0x00093D90 File Offset: 0x00091F90
		// (set) Token: 0x06002B04 RID: 11012 RVA: 0x00093D98 File Offset: 0x00091F98
		public override System.ComponentModel.ISite Site
		{
			get
			{
				return base.Site;
			}
			set
			{
				base.Site = value;
			}
		}

		// Token: 0x17000BCF RID: 3023
		// (get) Token: 0x06002B05 RID: 11013 RVA: 0x00093DA4 File Offset: 0x00091FA4
		// (set) Token: 0x06002B06 RID: 11014 RVA: 0x00093DAC File Offset: 0x00091FAC
		[System.ComponentModel.Browsable(false)]
		[System.ComponentModel.DefaultValue(null)]
		[TimersDescription("The object used to marshal the event handler calls issued when an interval has elapsed.")]
		public System.ComponentModel.ISynchronizeInvoke SynchronizingObject
		{
			get
			{
				return this.so;
			}
			set
			{
				this.so = value;
			}
		}

		// Token: 0x06002B07 RID: 11015 RVA: 0x00093DB8 File Offset: 0x00091FB8
		public void BeginInit()
		{
		}

		// Token: 0x06002B08 RID: 11016 RVA: 0x00093DBC File Offset: 0x00091FBC
		public void Close()
		{
			this.Enabled = false;
		}

		// Token: 0x06002B09 RID: 11017 RVA: 0x00093DC8 File Offset: 0x00091FC8
		public void EndInit()
		{
		}

		// Token: 0x06002B0A RID: 11018 RVA: 0x00093DCC File Offset: 0x00091FCC
		public void Start()
		{
			this.Enabled = true;
		}

		// Token: 0x06002B0B RID: 11019 RVA: 0x00093DD8 File Offset: 0x00091FD8
		public void Stop()
		{
			this.Enabled = false;
		}

		// Token: 0x06002B0C RID: 11020 RVA: 0x00093DE4 File Offset: 0x00091FE4
		protected override void Dispose(bool disposing)
		{
			this.Close();
			base.Dispose(disposing);
		}

		// Token: 0x06002B0D RID: 11021 RVA: 0x00093DF4 File Offset: 0x00091FF4
		private static void Callback(object state)
		{
			Timer timer = (Timer)state;
			if (!timer.Enabled)
			{
				return;
			}
			ElapsedEventHandler elapsed = timer.Elapsed;
			if (!timer.autoReset)
			{
				timer.Enabled = false;
			}
			if (elapsed == null)
			{
				return;
			}
			ElapsedEventArgs elapsedEventArgs = new ElapsedEventArgs(DateTime.Now);
			if (timer.so != null && timer.so.InvokeRequired)
			{
				timer.so.BeginInvoke(elapsed, new object[]
				{
					timer,
					elapsedEventArgs
				});
			}
			else
			{
				try
				{
					elapsed(timer, elapsedEventArgs);
				}
				catch
				{
				}
			}
		}

		// Token: 0x04001B20 RID: 6944
		private double interval;

		// Token: 0x04001B21 RID: 6945
		private bool autoReset;

		// Token: 0x04001B22 RID: 6946
		private Timer timer;

		// Token: 0x04001B23 RID: 6947
		private object _lock = new object();

		// Token: 0x04001B24 RID: 6948
		private System.ComponentModel.ISynchronizeInvoke so;
	}
}
