﻿using System;

namespace System.Timers
{
	// Token: 0x020004AD RID: 1197
	public class ElapsedEventArgs : EventArgs
	{
		// Token: 0x06002AF7 RID: 10999 RVA: 0x00093B0C File Offset: 0x00091D0C
		internal ElapsedEventArgs(DateTime time)
		{
			this.time = time;
		}

		// Token: 0x17000BCA RID: 3018
		// (get) Token: 0x06002AF8 RID: 11000 RVA: 0x00093B1C File Offset: 0x00091D1C
		public DateTime SignalTime
		{
			get
			{
				return this.time;
			}
		}

		// Token: 0x04001B1F RID: 6943
		private DateTime time;
	}
}
