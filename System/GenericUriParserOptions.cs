﻿using System;

namespace System
{
	// Token: 0x0200026F RID: 623
	[Flags]
	public enum GenericUriParserOptions
	{
		// Token: 0x040006DA RID: 1754
		Default = 0,
		// Token: 0x040006DB RID: 1755
		GenericAuthority = 1,
		// Token: 0x040006DC RID: 1756
		AllowEmptyAuthority = 2,
		// Token: 0x040006DD RID: 1757
		NoUserInfo = 4,
		// Token: 0x040006DE RID: 1758
		NoPort = 8,
		// Token: 0x040006DF RID: 1759
		NoQuery = 16,
		// Token: 0x040006E0 RID: 1760
		NoFragment = 32,
		// Token: 0x040006E1 RID: 1761
		DontConvertPathBackslashes = 64,
		// Token: 0x040006E2 RID: 1762
		DontCompressPath = 128,
		// Token: 0x040006E3 RID: 1763
		DontUnescapePathDotsAndSlashes = 256,
		// Token: 0x040006E4 RID: 1764
		Idn = 512,
		// Token: 0x040006E5 RID: 1765
		IriParsing = 1024
	}
}
