﻿using System;
using System.Collections;
using System.Globalization;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;

namespace System
{
	// Token: 0x020004B9 RID: 1209
	public abstract class UriParser
	{
		// Token: 0x06002B93 RID: 11155 RVA: 0x00097F04 File Offset: 0x00096104
		private static System.Text.RegularExpressions.Match ParseAuthority(System.Text.RegularExpressions.Group g)
		{
			return System.UriParser.auth_regex.Match(g.Value);
		}

		// Token: 0x06002B94 RID: 11156 RVA: 0x00097F18 File Offset: 0x00096118
		protected internal virtual string GetComponents(System.Uri uri, System.UriComponents components, System.UriFormat format)
		{
			if (format < System.UriFormat.UriEscaped || format > System.UriFormat.SafeUnescaped)
			{
				throw new ArgumentOutOfRangeException("format");
			}
			System.Text.RegularExpressions.Match match = System.UriParser.uri_regex.Match(uri.OriginalString);
			string value = this.scheme_name;
			int defaultPort = this.default_port;
			if (value == null || value == "*")
			{
				value = match.Groups[2].Value;
				defaultPort = System.Uri.GetDefaultPort(value);
			}
			else if (string.Compare(value, match.Groups[2].Value, true) != 0)
			{
				throw new SystemException("URI Parser: scheme mismatch: " + value + " vs. " + match.Groups[2].Value);
			}
			System.UriComponents uriComponents = components;
			switch (uriComponents)
			{
			case System.UriComponents.Scheme:
				return value;
			case System.UriComponents.UserInfo:
				return System.UriParser.ParseAuthority(match.Groups[4]).Groups[2].Value;
			default:
			{
				if (uriComponents == System.UriComponents.Path)
				{
					return this.Format(this.IgnoreFirstCharIf(match.Groups[5].Value, '/'), format);
				}
				if (uriComponents == System.UriComponents.Query)
				{
					return this.Format(match.Groups[7].Value, format);
				}
				if (uriComponents == System.UriComponents.Fragment)
				{
					return this.Format(match.Groups[9].Value, format);
				}
				if (uriComponents != System.UriComponents.StrongPort)
				{
					if (uriComponents == System.UriComponents.SerializationInfoString)
					{
						components = System.UriComponents.AbsoluteUri;
					}
					System.Text.RegularExpressions.Match match2 = System.UriParser.ParseAuthority(match.Groups[4]);
					StringBuilder stringBuilder = new StringBuilder();
					if ((components & System.UriComponents.Scheme) != (System.UriComponents)0)
					{
						stringBuilder.Append(value);
						stringBuilder.Append(System.Uri.GetSchemeDelimiter(value));
					}
					if ((components & System.UriComponents.UserInfo) != (System.UriComponents)0)
					{
						stringBuilder.Append(match2.Groups[1].Value);
					}
					if ((components & System.UriComponents.Host) != (System.UriComponents)0)
					{
						stringBuilder.Append(match2.Groups[3].Value);
					}
					if ((components & System.UriComponents.StrongPort) != (System.UriComponents)0)
					{
						System.Text.RegularExpressions.Group group = match2.Groups[4];
						stringBuilder.Append((!group.Success) ? (":" + defaultPort) : group.Value);
					}
					if ((components & System.UriComponents.Port) != (System.UriComponents)0)
					{
						string value2 = match2.Groups[5].Value;
						if (value2 != null && value2 != string.Empty && value2 != defaultPort.ToString())
						{
							stringBuilder.Append(match2.Groups[4].Value);
						}
					}
					if ((components & System.UriComponents.Path) != (System.UriComponents)0)
					{
						stringBuilder.Append(match.Groups[5]);
					}
					if ((components & System.UriComponents.Query) != (System.UriComponents)0)
					{
						stringBuilder.Append(match.Groups[6]);
					}
					if ((components & System.UriComponents.Fragment) != (System.UriComponents)0)
					{
						stringBuilder.Append(match.Groups[8]);
					}
					return this.Format(stringBuilder.ToString(), format);
				}
				System.Text.RegularExpressions.Group group2 = System.UriParser.ParseAuthority(match.Groups[4]).Groups[5];
				return (!group2.Success) ? defaultPort.ToString() : group2.Value;
			}
			case System.UriComponents.Host:
				return System.UriParser.ParseAuthority(match.Groups[4]).Groups[3].Value;
			case System.UriComponents.Port:
			{
				string value3 = System.UriParser.ParseAuthority(match.Groups[4]).Groups[5].Value;
				if (value3 != null && value3 != string.Empty && value3 != defaultPort.ToString())
				{
					return value3;
				}
				return string.Empty;
			}
			}
		}

		// Token: 0x06002B95 RID: 11157 RVA: 0x000982FC File Offset: 0x000964FC
		protected internal virtual void InitializeAndValidate(System.Uri uri, out System.UriFormatException parsingError)
		{
			if (uri.Scheme != this.scheme_name && this.scheme_name != "*")
			{
				parsingError = new System.UriFormatException("The argument Uri's scheme does not match");
			}
			else
			{
				parsingError = null;
			}
		}

		// Token: 0x06002B96 RID: 11158 RVA: 0x00098348 File Offset: 0x00096548
		protected internal virtual bool IsBaseOf(System.Uri baseUri, System.Uri relativeUri)
		{
			if (System.Uri.Compare(baseUri, relativeUri, System.UriComponents.Scheme | System.UriComponents.UserInfo | System.UriComponents.Host | System.UriComponents.Port, System.UriFormat.Unescaped, StringComparison.InvariantCultureIgnoreCase) != 0)
			{
				return false;
			}
			string localPath = baseUri.LocalPath;
			int length = localPath.LastIndexOf('/') + 1;
			return string.Compare(localPath, 0, relativeUri.LocalPath, 0, length, StringComparison.InvariantCultureIgnoreCase) == 0;
		}

		// Token: 0x06002B97 RID: 11159 RVA: 0x0009838C File Offset: 0x0009658C
		protected internal virtual bool IsWellFormedOriginalString(System.Uri uri)
		{
			return uri.IsWellFormedOriginalString();
		}

		// Token: 0x06002B98 RID: 11160 RVA: 0x00098394 File Offset: 0x00096594
		protected internal virtual System.UriParser OnNewUri()
		{
			return this;
		}

		// Token: 0x06002B99 RID: 11161 RVA: 0x00098398 File Offset: 0x00096598
		[MonoTODO]
		protected virtual void OnRegister(string schemeName, int defaultPort)
		{
		}

		// Token: 0x06002B9A RID: 11162 RVA: 0x0009839C File Offset: 0x0009659C
		[MonoTODO]
		protected internal virtual string Resolve(System.Uri baseUri, System.Uri relativeUri, out System.UriFormatException parsingError)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000BF0 RID: 3056
		// (get) Token: 0x06002B9B RID: 11163 RVA: 0x000983A4 File Offset: 0x000965A4
		// (set) Token: 0x06002B9C RID: 11164 RVA: 0x000983AC File Offset: 0x000965AC
		internal string SchemeName
		{
			get
			{
				return this.scheme_name;
			}
			set
			{
				this.scheme_name = value;
			}
		}

		// Token: 0x17000BF1 RID: 3057
		// (get) Token: 0x06002B9D RID: 11165 RVA: 0x000983B8 File Offset: 0x000965B8
		// (set) Token: 0x06002B9E RID: 11166 RVA: 0x000983C0 File Offset: 0x000965C0
		internal int DefaultPort
		{
			get
			{
				return this.default_port;
			}
			set
			{
				this.default_port = value;
			}
		}

		// Token: 0x06002B9F RID: 11167 RVA: 0x000983CC File Offset: 0x000965CC
		private string IgnoreFirstCharIf(string s, char c)
		{
			if (s.Length == 0)
			{
				return string.Empty;
			}
			if (s[0] == c)
			{
				return s.Substring(1);
			}
			return s;
		}

		// Token: 0x06002BA0 RID: 11168 RVA: 0x00098400 File Offset: 0x00096600
		private string Format(string s, System.UriFormat format)
		{
			if (s.Length == 0)
			{
				return string.Empty;
			}
			switch (format)
			{
			case System.UriFormat.UriEscaped:
				return System.Uri.EscapeString(s, false, true, true);
			case System.UriFormat.Unescaped:
				return System.Uri.Unescape(s, false);
			case System.UriFormat.SafeUnescaped:
				s = System.Uri.Unescape(s, false);
				return s;
			default:
				throw new ArgumentOutOfRangeException("format");
			}
		}

		// Token: 0x06002BA1 RID: 11169 RVA: 0x00098460 File Offset: 0x00096660
		private static void CreateDefaults()
		{
			if (System.UriParser.table != null)
			{
				return;
			}
			Hashtable hashtable = new Hashtable();
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeFile, -1);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeFtp, 21);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeGopher, 70);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeHttp, 80);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeHttps, 443);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeMailto, 25);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeNetPipe, -1);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeNetTcp, -1);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeNews, 119);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), System.Uri.UriSchemeNntp, 119);
			System.UriParser.InternalRegister(hashtable, new DefaultUriParser(), "ldap", 389);
			object obj = System.UriParser.lock_object;
			lock (obj)
			{
				if (System.UriParser.table == null)
				{
					System.UriParser.table = hashtable;
				}
			}
		}

		// Token: 0x06002BA2 RID: 11170 RVA: 0x00098594 File Offset: 0x00096794
		public static bool IsKnownScheme(string schemeName)
		{
			if (schemeName == null)
			{
				throw new ArgumentNullException("schemeName");
			}
			if (schemeName.Length == 0)
			{
				throw new ArgumentOutOfRangeException("schemeName");
			}
			System.UriParser.CreateDefaults();
			string key = schemeName.ToLower(CultureInfo.InvariantCulture);
			return System.UriParser.table[key] != null;
		}

		// Token: 0x06002BA3 RID: 11171 RVA: 0x000985EC File Offset: 0x000967EC
		private static void InternalRegister(Hashtable table, System.UriParser uriParser, string schemeName, int defaultPort)
		{
			uriParser.SchemeName = schemeName;
			uriParser.DefaultPort = defaultPort;
			if (uriParser is System.GenericUriParser)
			{
				table.Add(schemeName, uriParser);
			}
			else
			{
				table.Add(schemeName, new DefaultUriParser
				{
					SchemeName = schemeName,
					DefaultPort = defaultPort
				});
			}
			uriParser.OnRegister(schemeName, defaultPort);
		}

		// Token: 0x06002BA4 RID: 11172 RVA: 0x00098644 File Offset: 0x00096844
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public static void Register(System.UriParser uriParser, string schemeName, int defaultPort)
		{
			if (uriParser == null)
			{
				throw new ArgumentNullException("uriParser");
			}
			if (schemeName == null)
			{
				throw new ArgumentNullException("schemeName");
			}
			if (defaultPort < -1 || defaultPort >= 65535)
			{
				throw new ArgumentOutOfRangeException("defaultPort");
			}
			System.UriParser.CreateDefaults();
			string text = schemeName.ToLower(CultureInfo.InvariantCulture);
			if (System.UriParser.table[text] != null)
			{
				string text2 = Locale.GetText("Scheme '{0}' is already registred.");
				throw new InvalidOperationException(text2);
			}
			System.UriParser.InternalRegister(System.UriParser.table, uriParser, text, defaultPort);
		}

		// Token: 0x06002BA5 RID: 11173 RVA: 0x000986D0 File Offset: 0x000968D0
		internal static System.UriParser GetParser(string schemeName)
		{
			if (schemeName == null)
			{
				return null;
			}
			System.UriParser.CreateDefaults();
			string key = schemeName.ToLower(CultureInfo.InvariantCulture);
			return (System.UriParser)System.UriParser.table[key];
		}

		// Token: 0x04001B7C RID: 7036
		private static object lock_object = new object();

		// Token: 0x04001B7D RID: 7037
		private static Hashtable table;

		// Token: 0x04001B7E RID: 7038
		internal string scheme_name;

		// Token: 0x04001B7F RID: 7039
		private int default_port;

		// Token: 0x04001B80 RID: 7040
		private static readonly System.Text.RegularExpressions.Regex uri_regex = new System.Text.RegularExpressions.Regex("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?", System.Text.RegularExpressions.RegexOptions.Compiled);

		// Token: 0x04001B81 RID: 7041
		private static readonly System.Text.RegularExpressions.Regex auth_regex = new System.Text.RegularExpressions.Regex("^(([^@]+)@)?(.*?)(:([0-9]+))?$");
	}
}
