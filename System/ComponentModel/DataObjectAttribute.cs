﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000E7 RID: 231
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DataObjectAttribute : Attribute
	{
		// Token: 0x060009AB RID: 2475 RVA: 0x0001C0D4 File Offset: 0x0001A2D4
		public DataObjectAttribute() : this(true)
		{
		}

		// Token: 0x060009AC RID: 2476 RVA: 0x0001C0E0 File Offset: 0x0001A2E0
		public DataObjectAttribute(bool isDataObject)
		{
			this._isDataObject = isDataObject;
		}

		// Token: 0x060009AD RID: 2477 RVA: 0x0001C0F0 File Offset: 0x0001A2F0
		static DataObjectAttribute()
		{
			DataObjectAttribute.Default = DataObjectAttribute.NonDataObject;
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x060009AE RID: 2478 RVA: 0x0001C120 File Offset: 0x0001A320
		public bool IsDataObject
		{
			get
			{
				return this._isDataObject;
			}
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x0001C128 File Offset: 0x0001A328
		public override bool Equals(object obj)
		{
			return obj is DataObjectAttribute && ((DataObjectAttribute)obj).IsDataObject == this.IsDataObject;
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x0001C158 File Offset: 0x0001A358
		public override int GetHashCode()
		{
			return this.IsDataObject.GetHashCode();
		}

		// Token: 0x060009B1 RID: 2481 RVA: 0x0001C174 File Offset: 0x0001A374
		public override bool IsDefaultAttribute()
		{
			return DataObjectAttribute.Default.Equals(this);
		}

		// Token: 0x0400028B RID: 651
		public static readonly DataObjectAttribute DataObject = new DataObjectAttribute(true);

		// Token: 0x0400028C RID: 652
		public static readonly DataObjectAttribute Default;

		// Token: 0x0400028D RID: 653
		public static readonly DataObjectAttribute NonDataObject = new DataObjectAttribute(false);

		// Token: 0x0400028E RID: 654
		private readonly bool _isDataObject;
	}
}
