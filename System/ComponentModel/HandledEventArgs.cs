﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200014E RID: 334
	public class HandledEventArgs : EventArgs
	{
		// Token: 0x06000C48 RID: 3144 RVA: 0x0002018C File Offset: 0x0001E38C
		public HandledEventArgs()
		{
			this.handled = false;
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x0002019C File Offset: 0x0001E39C
		public HandledEventArgs(bool defaultHandledValue)
		{
			this.handled = defaultHandledValue;
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000C4A RID: 3146 RVA: 0x000201AC File Offset: 0x0001E3AC
		// (set) Token: 0x06000C4B RID: 3147 RVA: 0x000201B4 File Offset: 0x0001E3B4
		public bool Handled
		{
			get
			{
				return this.handled;
			}
			set
			{
				this.handled = value;
			}
		}

		// Token: 0x04000372 RID: 882
		private bool handled;
	}
}
