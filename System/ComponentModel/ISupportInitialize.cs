﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200016E RID: 366
	public interface ISupportInitialize
	{
		// Token: 0x06000CCF RID: 3279
		void BeginInit();

		// Token: 0x06000CD0 RID: 3280
		void EndInit();
	}
}
