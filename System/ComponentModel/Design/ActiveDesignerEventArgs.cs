﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F3 RID: 243
	public class ActiveDesignerEventArgs : EventArgs
	{
		// Token: 0x06000A03 RID: 2563 RVA: 0x0001CB10 File Offset: 0x0001AD10
		public ActiveDesignerEventArgs(IDesignerHost oldDesigner, IDesignerHost newDesigner)
		{
			this.oldDesigner = oldDesigner;
			this.newDesigner = newDesigner;
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06000A04 RID: 2564 RVA: 0x0001CB28 File Offset: 0x0001AD28
		public IDesignerHost NewDesigner
		{
			get
			{
				return this.newDesigner;
			}
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x06000A05 RID: 2565 RVA: 0x0001CB30 File Offset: 0x0001AD30
		public IDesignerHost OldDesigner
		{
			get
			{
				return this.oldDesigner;
			}
		}

		// Token: 0x040002A8 RID: 680
		private IDesignerHost oldDesigner;

		// Token: 0x040002A9 RID: 681
		private IDesignerHost newDesigner;
	}
}
