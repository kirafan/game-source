﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010D RID: 269
	public interface IComponentDiscoveryService
	{
		// Token: 0x06000AB3 RID: 2739
		ICollection GetComponentTypes(IDesignerHost designerHost, Type baseType);
	}
}
