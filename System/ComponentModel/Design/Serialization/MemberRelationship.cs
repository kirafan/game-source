﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000133 RID: 307
	public struct MemberRelationship
	{
		// Token: 0x06000B89 RID: 2953 RVA: 0x0001E388 File Offset: 0x0001C588
		public MemberRelationship(object owner, MemberDescriptor member)
		{
			this._owner = owner;
			this._member = member;
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000B8B RID: 2955 RVA: 0x0001E3B4 File Offset: 0x0001C5B4
		public bool IsEmpty
		{
			get
			{
				return this._owner == null;
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000B8C RID: 2956 RVA: 0x0001E3C0 File Offset: 0x0001C5C0
		public object Owner
		{
			get
			{
				return this._owner;
			}
		}

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000B8D RID: 2957 RVA: 0x0001E3C8 File Offset: 0x0001C5C8
		public MemberDescriptor Member
		{
			get
			{
				return this._member;
			}
		}

		// Token: 0x06000B8E RID: 2958 RVA: 0x0001E3D0 File Offset: 0x0001C5D0
		public override int GetHashCode()
		{
			if (this._owner != null && this._member != null)
			{
				return this._member.GetHashCode() ^ this._owner.GetHashCode();
			}
			return base.GetHashCode();
		}

		// Token: 0x06000B8F RID: 2959 RVA: 0x0001E41C File Offset: 0x0001C61C
		public override bool Equals(object o)
		{
			return o is MemberRelationship && (MemberRelationship)o == this;
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x0001E43C File Offset: 0x0001C63C
		public static bool operator ==(MemberRelationship left, MemberRelationship right)
		{
			return left.Owner == right.Owner && left.Member == right.Member;
		}

		// Token: 0x06000B91 RID: 2961 RVA: 0x0001E474 File Offset: 0x0001C674
		public static bool operator !=(MemberRelationship left, MemberRelationship right)
		{
			return !(left == right);
		}

		// Token: 0x04000304 RID: 772
		public static readonly MemberRelationship Empty = default(MemberRelationship);

		// Token: 0x04000305 RID: 773
		private object _owner;

		// Token: 0x04000306 RID: 774
		private MemberDescriptor _member;
	}
}
