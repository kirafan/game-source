﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x0200012F RID: 303
	public interface IDesignerSerializationProvider
	{
		// Token: 0x06000B7C RID: 2940
		object GetSerializer(IDesignerSerializationManager manager, object currentSerializer, Type objectType, Type serializerType);
	}
}
