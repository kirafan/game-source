﻿using System;
using System.Collections;
using System.IO;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000138 RID: 312
	public abstract class SerializationStore : IDisposable
	{
		// Token: 0x06000BAD RID: 2989 RVA: 0x0001E730 File Offset: 0x0001C930
		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000BAE RID: 2990
		public abstract ICollection Errors { get; }

		// Token: 0x06000BAF RID: 2991
		public abstract void Close();

		// Token: 0x06000BB0 RID: 2992
		public abstract void Save(Stream stream);

		// Token: 0x06000BB1 RID: 2993 RVA: 0x0001E73C File Offset: 0x0001C93C
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Close();
			}
		}
	}
}
