﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000500 RID: 1280
	// (Invoke) Token: 0x06002CAC RID: 11436
	public delegate void ResolveNameEventHandler(object sender, ResolveNameEventArgs e);
}
