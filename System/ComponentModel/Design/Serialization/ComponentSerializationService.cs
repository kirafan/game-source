﻿using System;
using System.Collections;
using System.IO;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000128 RID: 296
	public abstract class ComponentSerializationService
	{
		// Token: 0x06000B4B RID: 2891
		public abstract SerializationStore CreateStore();

		// Token: 0x06000B4C RID: 2892
		public abstract ICollection Deserialize(SerializationStore store);

		// Token: 0x06000B4D RID: 2893
		public abstract ICollection Deserialize(SerializationStore store, IContainer container);

		// Token: 0x06000B4E RID: 2894
		public abstract SerializationStore LoadStore(Stream stream);

		// Token: 0x06000B4F RID: 2895
		public abstract void Serialize(SerializationStore store, object value);

		// Token: 0x06000B50 RID: 2896
		public abstract void SerializeAbsolute(SerializationStore store, object value);

		// Token: 0x06000B51 RID: 2897
		public abstract void SerializeMember(SerializationStore store, object owningObject, MemberDescriptor member);

		// Token: 0x06000B52 RID: 2898
		public abstract void SerializeMemberAbsolute(SerializationStore store, object owningObject, MemberDescriptor member);

		// Token: 0x06000B53 RID: 2899 RVA: 0x0001DEDC File Offset: 0x0001C0DC
		public void DeserializeTo(SerializationStore store, IContainer container)
		{
			this.DeserializeTo(store, container, true);
		}

		// Token: 0x06000B54 RID: 2900 RVA: 0x0001DEE8 File Offset: 0x0001C0E8
		public void DeserializeTo(SerializationStore store, IContainer container, bool validateRecycledTypes)
		{
			this.DeserializeTo(store, container, validateRecycledTypes, true);
		}

		// Token: 0x06000B55 RID: 2901
		public abstract void DeserializeTo(SerializationStore store, IContainer container, bool validateRecycledTypes, bool applyDefaults);
	}
}
