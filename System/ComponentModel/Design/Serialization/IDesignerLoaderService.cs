﻿using System;
using System.Collections;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x0200012D RID: 301
	public interface IDesignerLoaderService
	{
		// Token: 0x06000B6A RID: 2922
		void AddLoadDependency();

		// Token: 0x06000B6B RID: 2923
		void DependentLoadComplete(bool successful, ICollection errorCollection);

		// Token: 0x06000B6C RID: 2924
		bool Reload();
	}
}
