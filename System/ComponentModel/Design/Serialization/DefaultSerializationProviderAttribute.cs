﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000139 RID: 313
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class DefaultSerializationProviderAttribute : Attribute
	{
		// Token: 0x06000BB2 RID: 2994 RVA: 0x0001E74C File Offset: 0x0001C94C
		public DefaultSerializationProviderAttribute(string providerTypeName)
		{
			if (providerTypeName == null)
			{
				throw new ArgumentNullException("providerTypeName");
			}
			this._providerTypeName = providerTypeName;
		}

		// Token: 0x06000BB3 RID: 2995 RVA: 0x0001E76C File Offset: 0x0001C96C
		public DefaultSerializationProviderAttribute(Type providerType)
		{
			if (providerType == null)
			{
				throw new ArgumentNullException("providerType");
			}
			this._providerTypeName = providerType.AssemblyQualifiedName;
		}

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000BB4 RID: 2996 RVA: 0x0001E794 File Offset: 0x0001C994
		public string ProviderTypeName
		{
			get
			{
				return this._providerTypeName;
			}
		}

		// Token: 0x0400030F RID: 783
		private string _providerTypeName;
	}
}
