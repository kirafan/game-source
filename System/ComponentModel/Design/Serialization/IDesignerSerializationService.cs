﻿using System;
using System.Collections;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000130 RID: 304
	public interface IDesignerSerializationService
	{
		// Token: 0x06000B7D RID: 2941
		ICollection Deserialize(object serializationData);

		// Token: 0x06000B7E RID: 2942
		object Serialize(ICollection objects);
	}
}
