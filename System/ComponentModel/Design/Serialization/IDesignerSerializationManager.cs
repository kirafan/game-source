﻿using System;
using System.Collections;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x0200012E RID: 302
	public interface IDesignerSerializationManager : IServiceProvider
	{
		// Token: 0x14000032 RID: 50
		// (add) Token: 0x06000B6D RID: 2925
		// (remove) Token: 0x06000B6E RID: 2926
		event ResolveNameEventHandler ResolveName;

		// Token: 0x14000033 RID: 51
		// (add) Token: 0x06000B6F RID: 2927
		// (remove) Token: 0x06000B70 RID: 2928
		event EventHandler SerializationComplete;

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000B71 RID: 2929
		ContextStack Context { get; }

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000B72 RID: 2930
		PropertyDescriptorCollection Properties { get; }

		// Token: 0x06000B73 RID: 2931
		void AddSerializationProvider(IDesignerSerializationProvider provider);

		// Token: 0x06000B74 RID: 2932
		object CreateInstance(Type type, ICollection arguments, string name, bool addToContainer);

		// Token: 0x06000B75 RID: 2933
		object GetInstance(string name);

		// Token: 0x06000B76 RID: 2934
		string GetName(object value);

		// Token: 0x06000B77 RID: 2935
		object GetSerializer(Type objectType, Type serializerType);

		// Token: 0x06000B78 RID: 2936
		Type GetType(string typeName);

		// Token: 0x06000B79 RID: 2937
		void RemoveSerializationProvider(IDesignerSerializationProvider provider);

		// Token: 0x06000B7A RID: 2938
		void ReportError(object errorInformation);

		// Token: 0x06000B7B RID: 2939
		void SetName(object instance, string name);
	}
}
