﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x0200012A RID: 298
	[ComVisible(true)]
	public abstract class DesignerLoader
	{
		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000B5E RID: 2910 RVA: 0x0001E064 File Offset: 0x0001C264
		public virtual bool Loading
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000B5F RID: 2911
		public abstract void BeginLoad(IDesignerLoaderHost host);

		// Token: 0x06000B60 RID: 2912
		public abstract void Dispose();

		// Token: 0x06000B61 RID: 2913 RVA: 0x0001E068 File Offset: 0x0001C268
		public virtual void Flush()
		{
		}
	}
}
