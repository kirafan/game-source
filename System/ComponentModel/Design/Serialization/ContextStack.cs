﻿using System;
using System.Collections;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000129 RID: 297
	public sealed class ContextStack
	{
		// Token: 0x06000B56 RID: 2902 RVA: 0x0001DEF4 File Offset: 0x0001C0F4
		public ContextStack()
		{
			this._contextList = new ArrayList();
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000B57 RID: 2903 RVA: 0x0001DF08 File Offset: 0x0001C108
		public object Current
		{
			get
			{
				int count = this._contextList.Count;
				if (count > 0)
				{
					return this._contextList[count - 1];
				}
				return null;
			}
		}

		// Token: 0x17000289 RID: 649
		public object this[Type type]
		{
			get
			{
				if (type == null)
				{
					throw new ArgumentNullException("type");
				}
				for (int i = this._contextList.Count - 1; i >= 0; i--)
				{
					object obj = this._contextList[i];
					if (type.IsInstanceOfType(obj))
					{
						return obj;
					}
				}
				return null;
			}
		}

		// Token: 0x1700028A RID: 650
		public object this[int level]
		{
			get
			{
				if (level < 0)
				{
					throw new ArgumentOutOfRangeException("level");
				}
				int count = this._contextList.Count;
				if (count > 0 && count > level)
				{
					return this._contextList[count - 1 - level];
				}
				return null;
			}
		}

		// Token: 0x06000B5A RID: 2906 RVA: 0x0001DFDC File Offset: 0x0001C1DC
		public object Pop()
		{
			object result = null;
			int count = this._contextList.Count;
			if (count > 0)
			{
				int index = count - 1;
				result = this._contextList[index];
				this._contextList.RemoveAt(index);
			}
			return result;
		}

		// Token: 0x06000B5B RID: 2907 RVA: 0x0001E01C File Offset: 0x0001C21C
		public void Push(object context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			this._contextList.Add(context);
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x0001E03C File Offset: 0x0001C23C
		public void Append(object context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			this._contextList.Insert(0, context);
		}

		// Token: 0x040002FE RID: 766
		private ArrayList _contextList;
	}
}
