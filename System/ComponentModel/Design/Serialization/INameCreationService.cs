﻿using System;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x02000131 RID: 305
	public interface INameCreationService
	{
		// Token: 0x06000B7F RID: 2943
		string CreateName(IContainer container, Type dataType);

		// Token: 0x06000B80 RID: 2944
		bool IsValidName(string name);

		// Token: 0x06000B81 RID: 2945
		void ValidateName(string name);
	}
}
