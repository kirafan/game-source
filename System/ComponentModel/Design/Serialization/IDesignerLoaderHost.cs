﻿using System;
using System.Collections;

namespace System.ComponentModel.Design.Serialization
{
	// Token: 0x0200012C RID: 300
	public interface IDesignerLoaderHost : IServiceProvider, IDesignerHost, IServiceContainer
	{
		// Token: 0x06000B68 RID: 2920
		void EndLoad(string baseClassName, bool successful, ICollection errorCollection);

		// Token: 0x06000B69 RID: 2921
		void Reload();
	}
}
