﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FF RID: 1279
	// (Invoke) Token: 0x06002CA8 RID: 11432
	[ComVisible(true)]
	public delegate void DesignerTransactionCloseEventHandler(object sender, DesignerTransactionCloseEventArgs e);
}
