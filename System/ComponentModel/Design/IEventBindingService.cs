﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x02000115 RID: 277
	[ComVisible(true)]
	public interface IEventBindingService
	{
		// Token: 0x06000AEB RID: 2795
		string CreateUniqueMethodName(IComponent component, EventDescriptor e);

		// Token: 0x06000AEC RID: 2796
		ICollection GetCompatibleMethods(EventDescriptor e);

		// Token: 0x06000AED RID: 2797
		EventDescriptor GetEvent(PropertyDescriptor property);

		// Token: 0x06000AEE RID: 2798
		PropertyDescriptorCollection GetEventProperties(EventDescriptorCollection events);

		// Token: 0x06000AEF RID: 2799
		PropertyDescriptor GetEventProperty(EventDescriptor e);

		// Token: 0x06000AF0 RID: 2800
		bool ShowCode();

		// Token: 0x06000AF1 RID: 2801
		bool ShowCode(int lineNumber);

		// Token: 0x06000AF2 RID: 2802
		bool ShowCode(IComponent component, EventDescriptor e);
	}
}
