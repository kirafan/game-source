﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F8 RID: 248
	[ComVisible(true)]
	public class ComponentEventArgs : EventArgs
	{
		// Token: 0x06000A1A RID: 2586 RVA: 0x0001CCCC File Offset: 0x0001AECC
		public ComponentEventArgs(IComponent component)
		{
			this.icomp = component;
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000A1B RID: 2587 RVA: 0x0001CCDC File Offset: 0x0001AEDC
		public virtual IComponent Component
		{
			get
			{
				return this.icomp;
			}
		}

		// Token: 0x040002B3 RID: 691
		private IComponent icomp;
	}
}
