﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace System.ComponentModel.Design
{
	// Token: 0x02000104 RID: 260
	public class DesigntimeLicenseContextSerializer
	{
		// Token: 0x06000A81 RID: 2689 RVA: 0x0001D564 File Offset: 0x0001B764
		private DesigntimeLicenseContextSerializer()
		{
		}

		// Token: 0x06000A82 RID: 2690 RVA: 0x0001D56C File Offset: 0x0001B76C
		public static void Serialize(Stream o, string cryptoKey, DesigntimeLicenseContext context)
		{
			object[] array = new object[2];
			array[0] = cryptoKey;
			Hashtable hashtable = new Hashtable();
			foreach (object obj in context.keys)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				hashtable.Add(((Type)dictionaryEntry.Key).AssemblyQualifiedName, dictionaryEntry.Value);
			}
			array[1] = hashtable;
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(o, array);
		}
	}
}
