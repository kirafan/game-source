﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011A RID: 282
	[ComVisible(true)]
	public interface IMenuCommandService
	{
		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000AFF RID: 2815
		DesignerVerbCollection Verbs { get; }

		// Token: 0x06000B00 RID: 2816
		void AddCommand(MenuCommand command);

		// Token: 0x06000B01 RID: 2817
		void AddVerb(DesignerVerb verb);

		// Token: 0x06000B02 RID: 2818
		MenuCommand FindCommand(CommandID commandID);

		// Token: 0x06000B03 RID: 2819
		bool GlobalInvoke(CommandID commandID);

		// Token: 0x06000B04 RID: 2820
		void RemoveCommand(MenuCommand command);

		// Token: 0x06000B05 RID: 2821
		void RemoveVerb(DesignerVerb verb);

		// Token: 0x06000B06 RID: 2822
		void ShowContextMenu(CommandID menuID, int x, int y);
	}
}
