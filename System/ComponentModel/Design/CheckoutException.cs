﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F4 RID: 244
	[Serializable]
	public class CheckoutException : ExternalException
	{
		// Token: 0x06000A06 RID: 2566 RVA: 0x0001CB38 File Offset: 0x0001AD38
		public CheckoutException()
		{
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x0001CB40 File Offset: 0x0001AD40
		public CheckoutException(string message) : base(message)
		{
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x0001CB4C File Offset: 0x0001AD4C
		public CheckoutException(string message, int errorCode) : base(message, errorCode)
		{
		}

		// Token: 0x06000A09 RID: 2569 RVA: 0x0001CB58 File Offset: 0x0001AD58
		public CheckoutException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x0001CB64 File Offset: 0x0001AD64
		protected CheckoutException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040002AA RID: 682
		public static readonly CheckoutException Canceled = new CheckoutException("The user canceled the checkout.", -2147467260);
	}
}
