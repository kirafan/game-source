﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000118 RID: 280
	public interface IHelpService
	{
		// Token: 0x06000AF6 RID: 2806
		void AddContextAttribute(string name, string value, HelpKeywordType keywordType);

		// Token: 0x06000AF7 RID: 2807
		void ClearContextAttributes();

		// Token: 0x06000AF8 RID: 2808
		IHelpService CreateLocalContext(HelpContextType contextType);

		// Token: 0x06000AF9 RID: 2809
		void RemoveContextAttribute(string name, string value);

		// Token: 0x06000AFA RID: 2810
		void RemoveLocalContext(IHelpService localContext);

		// Token: 0x06000AFB RID: 2811
		void ShowHelpFromKeyword(string helpKeyword);

		// Token: 0x06000AFC RID: 2812
		void ShowHelpFromUrl(string helpUrl);
	}
}
