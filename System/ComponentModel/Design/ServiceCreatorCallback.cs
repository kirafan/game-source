﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x02000501 RID: 1281
	// (Invoke) Token: 0x06002CB0 RID: 11440
	[ComVisible(true)]
	public delegate object ServiceCreatorCallback(IServiceContainer container, Type serviceType);
}
