﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FA RID: 1274
	// (Invoke) Token: 0x06002C94 RID: 11412
	[ComVisible(true)]
	public delegate void ComponentChangedEventHandler(object sender, ComponentChangedEventArgs e);
}
