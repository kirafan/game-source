﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x020004F9 RID: 1273
	// (Invoke) Token: 0x06002C90 RID: 11408
	public delegate void ActiveDesignerEventHandler(object sender, ActiveDesignerEventArgs e);
}
