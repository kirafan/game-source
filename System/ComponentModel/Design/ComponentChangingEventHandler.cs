﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FB RID: 1275
	// (Invoke) Token: 0x06002C98 RID: 11416
	[ComVisible(true)]
	public delegate void ComponentChangingEventHandler(object sender, ComponentChangingEventArgs e);
}
