﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x02000120 RID: 288
	public interface ITreeDesigner : IDisposable, IDesigner
	{
		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000B20 RID: 2848
		ICollection Children { get; }

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000B21 RID: 2849
		IDesigner Parent { get; }
	}
}
