﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x02000122 RID: 290
	public interface ITypeDiscoveryService
	{
		// Token: 0x06000B25 RID: 2853
		ICollection GetTypes(Type baseType, bool excludeGlobalTypes);
	}
}
