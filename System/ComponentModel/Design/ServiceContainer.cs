﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x0200013A RID: 314
	public class ServiceContainer : IDisposable, IServiceProvider, IServiceContainer
	{
		// Token: 0x06000BB5 RID: 2997 RVA: 0x0001E79C File Offset: 0x0001C99C
		public ServiceContainer() : this(null)
		{
		}

		// Token: 0x06000BB6 RID: 2998 RVA: 0x0001E7A8 File Offset: 0x0001C9A8
		public ServiceContainer(IServiceProvider parentProvider)
		{
			this.parentProvider = parentProvider;
		}

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000BB7 RID: 2999 RVA: 0x0001E7B8 File Offset: 0x0001C9B8
		private Hashtable Services
		{
			get
			{
				if (this.services == null)
				{
					this.services = new Hashtable();
				}
				return this.services;
			}
		}

		// Token: 0x06000BB8 RID: 3000 RVA: 0x0001E7D8 File Offset: 0x0001C9D8
		public void AddService(Type serviceType, object serviceInstance)
		{
			this.AddService(serviceType, serviceInstance, false);
		}

		// Token: 0x06000BB9 RID: 3001 RVA: 0x0001E7E4 File Offset: 0x0001C9E4
		public void AddService(Type serviceType, ServiceCreatorCallback callback)
		{
			this.AddService(serviceType, callback, false);
		}

		// Token: 0x06000BBA RID: 3002 RVA: 0x0001E7F0 File Offset: 0x0001C9F0
		public virtual void AddService(Type serviceType, object serviceInstance, bool promote)
		{
			if (promote && this.parentProvider != null)
			{
				IServiceContainer serviceContainer = (IServiceContainer)this.parentProvider.GetService(typeof(IServiceContainer));
				serviceContainer.AddService(serviceType, serviceInstance, promote);
				return;
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			if (serviceInstance == null)
			{
				throw new ArgumentNullException("serviceInstance");
			}
			if (this.Services.Contains(serviceType))
			{
				throw new ArgumentException(string.Format("The service {0} already exists in the service container.", serviceType.ToString()), "serviceType");
			}
			this.Services.Add(serviceType, serviceInstance);
		}

		// Token: 0x06000BBB RID: 3003 RVA: 0x0001E890 File Offset: 0x0001CA90
		public virtual void AddService(Type serviceType, ServiceCreatorCallback callback, bool promote)
		{
			if (promote && this.parentProvider != null)
			{
				IServiceContainer serviceContainer = (IServiceContainer)this.parentProvider.GetService(typeof(IServiceContainer));
				serviceContainer.AddService(serviceType, callback, promote);
				return;
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			if (callback == null)
			{
				throw new ArgumentNullException("callback");
			}
			if (this.Services.Contains(serviceType))
			{
				throw new ArgumentException(string.Format("The service {0} already exists in the service container.", serviceType.ToString()), "serviceType");
			}
			this.Services.Add(serviceType, callback);
		}

		// Token: 0x06000BBC RID: 3004 RVA: 0x0001E930 File Offset: 0x0001CB30
		public void RemoveService(Type serviceType)
		{
			this.RemoveService(serviceType, false);
		}

		// Token: 0x06000BBD RID: 3005 RVA: 0x0001E93C File Offset: 0x0001CB3C
		public virtual void RemoveService(Type serviceType, bool promote)
		{
			if (promote && this.parentProvider != null)
			{
				IServiceContainer serviceContainer = (IServiceContainer)this.parentProvider.GetService(typeof(IServiceContainer));
				serviceContainer.RemoveService(serviceType, promote);
				return;
			}
			if (serviceType == null)
			{
				throw new ArgumentNullException("serviceType");
			}
			this.Services.Remove(serviceType);
		}

		// Token: 0x06000BBE RID: 3006 RVA: 0x0001E99C File Offset: 0x0001CB9C
		public virtual object GetService(Type serviceType)
		{
			object obj = null;
			Type[] defaultServices = this.DefaultServices;
			for (int i = 0; i < defaultServices.Length; i++)
			{
				if (defaultServices[i] == serviceType)
				{
					obj = this;
					break;
				}
			}
			if (obj == null)
			{
				obj = this.Services[serviceType];
			}
			if (obj == null && this.parentProvider != null)
			{
				obj = this.parentProvider.GetService(serviceType);
			}
			if (obj != null)
			{
				ServiceCreatorCallback serviceCreatorCallback = obj as ServiceCreatorCallback;
				if (serviceCreatorCallback != null)
				{
					obj = serviceCreatorCallback(this, serviceType);
					this.Services[serviceType] = obj;
				}
			}
			return obj;
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000BBF RID: 3007 RVA: 0x0001EA34 File Offset: 0x0001CC34
		protected virtual Type[] DefaultServices
		{
			get
			{
				return new Type[]
				{
					typeof(IServiceContainer),
					typeof(ServiceContainer)
				};
			}
		}

		// Token: 0x06000BC0 RID: 3008 RVA: 0x0001EA64 File Offset: 0x0001CC64
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000BC1 RID: 3009 RVA: 0x0001EA74 File Offset: 0x0001CC74
		protected virtual void Dispose(bool disposing)
		{
			if (!this._disposed)
			{
				if (disposing && this.services != null)
				{
					foreach (object obj in this.services)
					{
						if (obj is IDisposable)
						{
							((IDisposable)obj).Dispose();
						}
					}
					this.services = null;
				}
				this._disposed = true;
			}
		}

		// Token: 0x04000310 RID: 784
		private IServiceProvider parentProvider;

		// Token: 0x04000311 RID: 785
		private Hashtable services;

		// Token: 0x04000312 RID: 786
		private bool _disposed;
	}
}
