﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011E RID: 286
	[ComVisible(true)]
	public interface ISelectionService
	{
		// Token: 0x1400002F RID: 47
		// (add) Token: 0x06000B10 RID: 2832
		// (remove) Token: 0x06000B11 RID: 2833
		event EventHandler SelectionChanged;

		// Token: 0x14000030 RID: 48
		// (add) Token: 0x06000B12 RID: 2834
		// (remove) Token: 0x06000B13 RID: 2835
		event EventHandler SelectionChanging;

		// Token: 0x06000B14 RID: 2836
		bool GetComponentSelected(object component);

		// Token: 0x06000B15 RID: 2837
		ICollection GetSelectedComponents();

		// Token: 0x06000B16 RID: 2838
		void SetSelectedComponents(ICollection components, SelectionTypes selectionType);

		// Token: 0x06000B17 RID: 2839
		void SetSelectedComponents(ICollection components);

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000B18 RID: 2840
		object PrimarySelection { get; }

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000B19 RID: 2841
		int SelectionCount { get; }
	}
}
