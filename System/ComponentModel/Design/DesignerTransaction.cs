﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000100 RID: 256
	public abstract class DesignerTransaction : IDisposable
	{
		// Token: 0x06000A5A RID: 2650 RVA: 0x0001D300 File Offset: 0x0001B500
		protected DesignerTransaction() : this(string.Empty)
		{
		}

		// Token: 0x06000A5B RID: 2651 RVA: 0x0001D310 File Offset: 0x0001B510
		protected DesignerTransaction(string description)
		{
			this.description = description;
			this.committed = false;
			this.canceled = false;
		}

		// Token: 0x06000A5C RID: 2652 RVA: 0x0001D330 File Offset: 0x0001B530
		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06000A5D RID: 2653 RVA: 0x0001D33C File Offset: 0x0001B53C
		protected virtual void Dispose(bool disposing)
		{
			this.Cancel();
			if (disposing)
			{
				GC.SuppressFinalize(true);
			}
		}

		// Token: 0x06000A5E RID: 2654
		protected abstract void OnCancel();

		// Token: 0x06000A5F RID: 2655
		protected abstract void OnCommit();

		// Token: 0x06000A60 RID: 2656 RVA: 0x0001D358 File Offset: 0x0001B558
		public void Cancel()
		{
			if (!this.Canceled && !this.Committed)
			{
				this.canceled = true;
				this.OnCancel();
			}
		}

		// Token: 0x06000A61 RID: 2657 RVA: 0x0001D380 File Offset: 0x0001B580
		public void Commit()
		{
			if (!this.Canceled && !this.Committed)
			{
				this.committed = true;
				this.OnCommit();
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000A62 RID: 2658 RVA: 0x0001D3A8 File Offset: 0x0001B5A8
		public bool Canceled
		{
			get
			{
				return this.canceled;
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000A63 RID: 2659 RVA: 0x0001D3B0 File Offset: 0x0001B5B0
		public bool Committed
		{
			get
			{
				return this.committed;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000A64 RID: 2660 RVA: 0x0001D3B8 File Offset: 0x0001B5B8
		public string Description
		{
			get
			{
				return this.description;
			}
		}

		// Token: 0x06000A65 RID: 2661 RVA: 0x0001D3C0 File Offset: 0x0001B5C0
		~DesignerTransaction()
		{
			this.Dispose(false);
		}

		// Token: 0x040002C3 RID: 707
		private string description;

		// Token: 0x040002C4 RID: 708
		private bool committed;

		// Token: 0x040002C5 RID: 709
		private bool canceled;
	}
}
