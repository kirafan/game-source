﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011B RID: 283
	public interface IReferenceService
	{
		// Token: 0x06000B07 RID: 2823
		IComponent GetComponent(object reference);

		// Token: 0x06000B08 RID: 2824
		string GetName(object reference);

		// Token: 0x06000B09 RID: 2825
		object GetReference(string name);

		// Token: 0x06000B0A RID: 2826
		object[] GetReferences();

		// Token: 0x06000B0B RID: 2827
		object[] GetReferences(Type baseType);
	}
}
