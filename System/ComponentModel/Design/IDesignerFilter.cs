﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x02000111 RID: 273
	public interface IDesignerFilter
	{
		// Token: 0x06000AC4 RID: 2756
		void PostFilterAttributes(IDictionary attributes);

		// Token: 0x06000AC5 RID: 2757
		void PostFilterEvents(IDictionary events);

		// Token: 0x06000AC6 RID: 2758
		void PostFilterProperties(IDictionary properties);

		// Token: 0x06000AC7 RID: 2759
		void PreFilterAttributes(IDictionary attributes);

		// Token: 0x06000AC8 RID: 2760
		void PreFilterEvents(IDictionary events);

		// Token: 0x06000AC9 RID: 2761
		void PreFilterProperties(IDictionary properties);
	}
}
