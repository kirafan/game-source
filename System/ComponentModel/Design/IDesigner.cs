﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010F RID: 271
	[ComVisible(true)]
	public interface IDesigner : IDisposable
	{
		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000AB6 RID: 2742
		IComponent Component { get; }

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000AB7 RID: 2743
		DesignerVerbCollection Verbs { get; }

		// Token: 0x06000AB8 RID: 2744
		void DoDefaultAction();

		// Token: 0x06000AB9 RID: 2745
		void Initialize(IComponent component);
	}
}
