﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FD RID: 1277
	// (Invoke) Token: 0x06002CA0 RID: 11424
	[ComVisible(true)]
	public delegate void ComponentRenameEventHandler(object sender, ComponentRenameEventArgs e);
}
