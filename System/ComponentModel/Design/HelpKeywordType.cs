﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010B RID: 267
	public enum HelpKeywordType
	{
		// Token: 0x040002E1 RID: 737
		F1Keyword,
		// Token: 0x040002E2 RID: 738
		GeneralKeyword,
		// Token: 0x040002E3 RID: 739
		FilterKeyword
	}
}
