﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010E RID: 270
	public interface IComponentInitializer
	{
		// Token: 0x06000AB4 RID: 2740
		void InitializeExistingComponent(IDictionary defaultValues);

		// Token: 0x06000AB5 RID: 2741
		void InitializeNewComponent(IDictionary defaultValues);
	}
}
