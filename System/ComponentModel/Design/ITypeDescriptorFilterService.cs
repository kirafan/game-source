﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x02000121 RID: 289
	public interface ITypeDescriptorFilterService
	{
		// Token: 0x06000B22 RID: 2850
		bool FilterAttributes(IComponent component, IDictionary attributes);

		// Token: 0x06000B23 RID: 2851
		bool FilterEvents(IComponent component, IDictionary events);

		// Token: 0x06000B24 RID: 2852
		bool FilterProperties(IComponent component, IDictionary properties);
	}
}
