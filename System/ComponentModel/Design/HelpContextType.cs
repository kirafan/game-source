﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000109 RID: 265
	public enum HelpContextType
	{
		// Token: 0x040002DA RID: 730
		Ambient,
		// Token: 0x040002DB RID: 731
		Window,
		// Token: 0x040002DC RID: 732
		Selection,
		// Token: 0x040002DD RID: 733
		ToolWindowSelection
	}
}
