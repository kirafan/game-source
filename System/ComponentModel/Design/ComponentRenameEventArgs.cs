﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F9 RID: 249
	[ComVisible(true)]
	public class ComponentRenameEventArgs : EventArgs
	{
		// Token: 0x06000A1C RID: 2588 RVA: 0x0001CCE4 File Offset: 0x0001AEE4
		public ComponentRenameEventArgs(object component, string oldName, string newName)
		{
			this.component = component;
			this.oldName = oldName;
			this.newName = newName;
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000A1D RID: 2589 RVA: 0x0001CD04 File Offset: 0x0001AF04
		public object Component
		{
			get
			{
				return this.component;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000A1E RID: 2590 RVA: 0x0001CD0C File Offset: 0x0001AF0C
		public virtual string NewName
		{
			get
			{
				return this.newName;
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06000A1F RID: 2591 RVA: 0x0001CD14 File Offset: 0x0001AF14
		public virtual string OldName
		{
			get
			{
				return this.oldName;
			}
		}

		// Token: 0x040002B4 RID: 692
		private object component;

		// Token: 0x040002B5 RID: 693
		private string oldName;

		// Token: 0x040002B6 RID: 694
		private string newName;
	}
}
