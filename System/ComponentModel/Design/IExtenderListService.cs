﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000116 RID: 278
	public interface IExtenderListService
	{
		// Token: 0x06000AF3 RID: 2803
		IExtenderProvider[] GetExtenderProviders();
	}
}
