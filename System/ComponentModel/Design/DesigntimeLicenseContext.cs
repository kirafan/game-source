﻿using System;
using System.Collections;
using System.Reflection;

namespace System.ComponentModel.Design
{
	// Token: 0x02000103 RID: 259
	public class DesigntimeLicenseContext : LicenseContext
	{
		// Token: 0x06000A7E RID: 2686 RVA: 0x0001D53C File Offset: 0x0001B73C
		public override string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
		{
			return (string)this.keys[type];
		}

		// Token: 0x06000A7F RID: 2687 RVA: 0x0001D550 File Offset: 0x0001B750
		public override void SetSavedLicenseKey(Type type, string key)
		{
			this.keys[type] = key;
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000A80 RID: 2688 RVA: 0x0001D560 File Offset: 0x0001B760
		public override LicenseUsageMode UsageMode
		{
			get
			{
				return LicenseUsageMode.Designtime;
			}
		}

		// Token: 0x040002C8 RID: 712
		internal Hashtable keys = new Hashtable();
	}
}
