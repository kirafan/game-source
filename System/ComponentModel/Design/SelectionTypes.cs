﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x02000127 RID: 295
	[Flags]
	[ComVisible(true)]
	public enum SelectionTypes
	{
		// Token: 0x040002F3 RID: 755
		Auto = 1,
		// Token: 0x040002F4 RID: 756
		[Obsolete("This value has been deprecated. Use SelectionTypes.Auto instead.")]
		Normal = 1,
		// Token: 0x040002F5 RID: 757
		Replace = 2,
		// Token: 0x040002F6 RID: 758
		[Obsolete("This value has been deprecated. It is no longer supported.")]
		MouseDown = 4,
		// Token: 0x040002F7 RID: 759
		[Obsolete("This value has been deprecated. It is no longer supported.")]
		MouseUp = 8,
		// Token: 0x040002F8 RID: 760
		[Obsolete("This value has been deprecated. Use SelectionTypes.Primary instead.")]
		Click = 16,
		// Token: 0x040002F9 RID: 761
		Primary = 16,
		// Token: 0x040002FA RID: 762
		[Obsolete("This value has been deprecated. It is no longer supported.")]
		Valid = 31,
		// Token: 0x040002FB RID: 763
		Toggle = 32,
		// Token: 0x040002FC RID: 764
		Add = 64,
		// Token: 0x040002FD RID: 765
		Remove = 128
	}
}
