﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200013E RID: 318
	[ComVisible(true)]
	public enum ViewTechnology
	{
		// Token: 0x04000357 RID: 855
		[Obsolete("Use ViewTechnology.Default.")]
		Passthrough,
		// Token: 0x04000358 RID: 856
		[Obsolete("Use ViewTechnology.Default.")]
		WindowsForms,
		// Token: 0x04000359 RID: 857
		Default
	}
}
