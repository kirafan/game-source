﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FE RID: 1278
	// (Invoke) Token: 0x06002CA4 RID: 11428
	public delegate void DesignerEventHandler(object sender, DesignerEventArgs e);
}
