﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000119 RID: 281
	public interface IInheritanceService
	{
		// Token: 0x06000AFD RID: 2813
		void AddInheritedComponents(IComponent component, IContainer container);

		// Token: 0x06000AFE RID: 2814
		InheritanceAttribute GetInheritanceAttribute(IComponent component);
	}
}
