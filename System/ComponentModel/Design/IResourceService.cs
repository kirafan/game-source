﻿using System;
using System.Globalization;
using System.Resources;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011C RID: 284
	public interface IResourceService
	{
		// Token: 0x06000B0C RID: 2828
		IResourceReader GetResourceReader(CultureInfo info);

		// Token: 0x06000B0D RID: 2829
		IResourceWriter GetResourceWriter(CultureInfo info);
	}
}
