﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000114 RID: 276
	public interface IDictionaryService
	{
		// Token: 0x06000AE8 RID: 2792
		object GetKey(object value);

		// Token: 0x06000AE9 RID: 2793
		object GetValue(object key);

		// Token: 0x06000AEA RID: 2794
		void SetValue(object key, object value);
	}
}
