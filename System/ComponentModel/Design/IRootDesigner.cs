﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011D RID: 285
	[ComVisible(true)]
	public interface IRootDesigner : IDisposable, IDesigner
	{
		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000B0E RID: 2830
		ViewTechnology[] SupportedTechnologies { get; }

		// Token: 0x06000B0F RID: 2831
		object GetView(ViewTechnology technology);
	}
}
