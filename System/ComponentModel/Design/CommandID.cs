﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F5 RID: 245
	[ComVisible(true)]
	public class CommandID
	{
		// Token: 0x06000A0C RID: 2572 RVA: 0x0001CB88 File Offset: 0x0001AD88
		public CommandID(Guid menuGroup, int commandID)
		{
			this.cID = commandID;
			this.guid = menuGroup;
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x06000A0D RID: 2573 RVA: 0x0001CBA0 File Offset: 0x0001ADA0
		public virtual Guid Guid
		{
			get
			{
				return this.guid;
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x06000A0E RID: 2574 RVA: 0x0001CBA8 File Offset: 0x0001ADA8
		public virtual int ID
		{
			get
			{
				return this.cID;
			}
		}

		// Token: 0x06000A0F RID: 2575 RVA: 0x0001CBB0 File Offset: 0x0001ADB0
		public override bool Equals(object obj)
		{
			return obj is CommandID && (obj == this || (((CommandID)obj).Guid.Equals(this.guid) && ((CommandID)obj).ID.Equals(this.cID)));
		}

		// Token: 0x06000A10 RID: 2576 RVA: 0x0001CC10 File Offset: 0x0001AE10
		public override int GetHashCode()
		{
			return this.guid.GetHashCode() ^ this.cID.GetHashCode();
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0001CC2C File Offset: 0x0001AE2C
		public override string ToString()
		{
			return this.guid.ToString() + " : " + this.cID.ToString();
		}

		// Token: 0x040002AB RID: 683
		private int cID;

		// Token: 0x040002AC RID: 684
		private Guid guid;
	}
}
