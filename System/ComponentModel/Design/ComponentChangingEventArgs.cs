﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020000F7 RID: 247
	[ComVisible(true)]
	public sealed class ComponentChangingEventArgs : EventArgs
	{
		// Token: 0x06000A17 RID: 2583 RVA: 0x0001CCA4 File Offset: 0x0001AEA4
		public ComponentChangingEventArgs(object component, MemberDescriptor member)
		{
			this.component = component;
			this.member = member;
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000A18 RID: 2584 RVA: 0x0001CCBC File Offset: 0x0001AEBC
		public object Component
		{
			get
			{
				return this.component;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000A19 RID: 2585 RVA: 0x0001CCC4 File Offset: 0x0001AEC4
		public MemberDescriptor Member
		{
			get
			{
				return this.member;
			}
		}

		// Token: 0x040002B1 RID: 689
		private object component;

		// Token: 0x040002B2 RID: 690
		private MemberDescriptor member;
	}
}
