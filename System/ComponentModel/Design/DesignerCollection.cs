﻿using System;
using System.Collections;

namespace System.ComponentModel.Design
{
	// Token: 0x020000FA RID: 250
	public class DesignerCollection : ICollection, IEnumerable
	{
		// Token: 0x06000A20 RID: 2592 RVA: 0x0001CD1C File Offset: 0x0001AF1C
		public DesignerCollection(IDesignerHost[] designers)
		{
			this.designers = new ArrayList(designers);
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x0001CD30 File Offset: 0x0001AF30
		public DesignerCollection(IList designers)
		{
			this.designers = new ArrayList(designers);
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000A22 RID: 2594 RVA: 0x0001CD44 File Offset: 0x0001AF44
		int ICollection.Count
		{
			get
			{
				return this.Count;
			}
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x0001CD4C File Offset: 0x0001AF4C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000A24 RID: 2596 RVA: 0x0001CD54 File Offset: 0x0001AF54
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.designers.IsSynchronized;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000A25 RID: 2597 RVA: 0x0001CD64 File Offset: 0x0001AF64
		object ICollection.SyncRoot
		{
			get
			{
				return this.designers.SyncRoot;
			}
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x0001CD74 File Offset: 0x0001AF74
		void ICollection.CopyTo(Array array, int index)
		{
			this.designers.CopyTo(array, index);
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000A27 RID: 2599 RVA: 0x0001CD84 File Offset: 0x0001AF84
		public int Count
		{
			get
			{
				return this.designers.Count;
			}
		}

		// Token: 0x1700024E RID: 590
		public virtual IDesignerHost this[int index]
		{
			get
			{
				return (IDesignerHost)this.designers[index];
			}
		}

		// Token: 0x06000A29 RID: 2601 RVA: 0x0001CDA8 File Offset: 0x0001AFA8
		public IEnumerator GetEnumerator()
		{
			return this.designers.GetEnumerator();
		}

		// Token: 0x040002B7 RID: 695
		private ArrayList designers;
	}
}
