﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000110 RID: 272
	public interface IDesignerEventService
	{
		// Token: 0x14000024 RID: 36
		// (add) Token: 0x06000ABA RID: 2746
		// (remove) Token: 0x06000ABB RID: 2747
		event ActiveDesignerEventHandler ActiveDesignerChanged;

		// Token: 0x14000025 RID: 37
		// (add) Token: 0x06000ABC RID: 2748
		// (remove) Token: 0x06000ABD RID: 2749
		event DesignerEventHandler DesignerCreated;

		// Token: 0x14000026 RID: 38
		// (add) Token: 0x06000ABE RID: 2750
		// (remove) Token: 0x06000ABF RID: 2751
		event DesignerEventHandler DesignerDisposed;

		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06000AC0 RID: 2752
		// (remove) Token: 0x06000AC1 RID: 2753
		event EventHandler SelectionChanged;

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000AC2 RID: 2754
		IDesignerHost ActiveDesigner { get; }

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000AC3 RID: 2755
		DesignerCollection Designers { get; }
	}
}
