﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x02000112 RID: 274
	[ComVisible(true)]
	public interface IDesignerHost : IServiceProvider, IServiceContainer
	{
		// Token: 0x14000028 RID: 40
		// (add) Token: 0x06000ACA RID: 2762
		// (remove) Token: 0x06000ACB RID: 2763
		event EventHandler Activated;

		// Token: 0x14000029 RID: 41
		// (add) Token: 0x06000ACC RID: 2764
		// (remove) Token: 0x06000ACD RID: 2765
		event EventHandler Deactivated;

		// Token: 0x1400002A RID: 42
		// (add) Token: 0x06000ACE RID: 2766
		// (remove) Token: 0x06000ACF RID: 2767
		event EventHandler LoadComplete;

		// Token: 0x1400002B RID: 43
		// (add) Token: 0x06000AD0 RID: 2768
		// (remove) Token: 0x06000AD1 RID: 2769
		event DesignerTransactionCloseEventHandler TransactionClosed;

		// Token: 0x1400002C RID: 44
		// (add) Token: 0x06000AD2 RID: 2770
		// (remove) Token: 0x06000AD3 RID: 2771
		event DesignerTransactionCloseEventHandler TransactionClosing;

		// Token: 0x1400002D RID: 45
		// (add) Token: 0x06000AD4 RID: 2772
		// (remove) Token: 0x06000AD5 RID: 2773
		event EventHandler TransactionOpened;

		// Token: 0x1400002E RID: 46
		// (add) Token: 0x06000AD6 RID: 2774
		// (remove) Token: 0x06000AD7 RID: 2775
		event EventHandler TransactionOpening;

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000AD8 RID: 2776
		IContainer Container { get; }

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000AD9 RID: 2777
		bool InTransaction { get; }

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000ADA RID: 2778
		bool Loading { get; }

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000ADB RID: 2779
		IComponent RootComponent { get; }

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000ADC RID: 2780
		string RootComponentClassName { get; }

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000ADD RID: 2781
		string TransactionDescription { get; }

		// Token: 0x06000ADE RID: 2782
		void Activate();

		// Token: 0x06000ADF RID: 2783
		IComponent CreateComponent(Type componentClass);

		// Token: 0x06000AE0 RID: 2784
		IComponent CreateComponent(Type componentClass, string name);

		// Token: 0x06000AE1 RID: 2785
		DesignerTransaction CreateTransaction();

		// Token: 0x06000AE2 RID: 2786
		DesignerTransaction CreateTransaction(string description);

		// Token: 0x06000AE3 RID: 2787
		void DestroyComponent(IComponent component);

		// Token: 0x06000AE4 RID: 2788
		IDesigner GetDesigner(IComponent component);

		// Token: 0x06000AE5 RID: 2789
		Type GetType(string typeName);
	}
}
