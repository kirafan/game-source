﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010C RID: 268
	[ComVisible(true)]
	public interface IComponentChangeService
	{
		// Token: 0x1400001D RID: 29
		// (add) Token: 0x06000AA3 RID: 2723
		// (remove) Token: 0x06000AA4 RID: 2724
		event ComponentEventHandler ComponentAdded;

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x06000AA5 RID: 2725
		// (remove) Token: 0x06000AA6 RID: 2726
		event ComponentEventHandler ComponentAdding;

		// Token: 0x1400001F RID: 31
		// (add) Token: 0x06000AA7 RID: 2727
		// (remove) Token: 0x06000AA8 RID: 2728
		event ComponentChangedEventHandler ComponentChanged;

		// Token: 0x14000020 RID: 32
		// (add) Token: 0x06000AA9 RID: 2729
		// (remove) Token: 0x06000AAA RID: 2730
		event ComponentChangingEventHandler ComponentChanging;

		// Token: 0x14000021 RID: 33
		// (add) Token: 0x06000AAB RID: 2731
		// (remove) Token: 0x06000AAC RID: 2732
		event ComponentEventHandler ComponentRemoved;

		// Token: 0x14000022 RID: 34
		// (add) Token: 0x06000AAD RID: 2733
		// (remove) Token: 0x06000AAE RID: 2734
		event ComponentEventHandler ComponentRemoving;

		// Token: 0x14000023 RID: 35
		// (add) Token: 0x06000AAF RID: 2735
		// (remove) Token: 0x06000AB0 RID: 2736
		event ComponentRenameEventHandler ComponentRename;

		// Token: 0x06000AB1 RID: 2737
		void OnComponentChanged(object component, MemberDescriptor member, object oldValue, object newValue);

		// Token: 0x06000AB2 RID: 2738
		void OnComponentChanging(object component, MemberDescriptor member);
	}
}
