﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x02000101 RID: 257
	[ComVisible(true)]
	public class DesignerVerbCollection : CollectionBase
	{
		// Token: 0x06000A66 RID: 2662 RVA: 0x0001D3FC File Offset: 0x0001B5FC
		public DesignerVerbCollection()
		{
		}

		// Token: 0x06000A67 RID: 2663 RVA: 0x0001D404 File Offset: 0x0001B604
		public DesignerVerbCollection(DesignerVerb[] value)
		{
			base.InnerList.AddRange(value);
		}

		// Token: 0x17000265 RID: 613
		public DesignerVerb this[int index]
		{
			get
			{
				return (DesignerVerb)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x0001D43C File Offset: 0x0001B63C
		public int Add(DesignerVerb value)
		{
			return base.InnerList.Add(value);
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x0001D44C File Offset: 0x0001B64C
		public void AddRange(DesignerVerb[] value)
		{
			base.InnerList.AddRange(value);
		}

		// Token: 0x06000A6C RID: 2668 RVA: 0x0001D45C File Offset: 0x0001B65C
		public void AddRange(DesignerVerbCollection value)
		{
			base.InnerList.AddRange(value);
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x0001D46C File Offset: 0x0001B66C
		public bool Contains(DesignerVerb value)
		{
			return base.InnerList.Contains(value);
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x0001D47C File Offset: 0x0001B67C
		public void CopyTo(DesignerVerb[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x0001D48C File Offset: 0x0001B68C
		public int IndexOf(DesignerVerb value)
		{
			return base.InnerList.IndexOf(value);
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x0001D49C File Offset: 0x0001B69C
		public void Insert(int index, DesignerVerb value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x06000A71 RID: 2673 RVA: 0x0001D4AC File Offset: 0x0001B6AC
		protected override void OnClear()
		{
		}

		// Token: 0x06000A72 RID: 2674 RVA: 0x0001D4B0 File Offset: 0x0001B6B0
		protected override void OnInsert(int index, object value)
		{
		}

		// Token: 0x06000A73 RID: 2675 RVA: 0x0001D4B4 File Offset: 0x0001B6B4
		protected override void OnRemove(int index, object value)
		{
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x0001D4B8 File Offset: 0x0001B6B8
		protected override void OnSet(int index, object oldValue, object newValue)
		{
		}

		// Token: 0x06000A75 RID: 2677 RVA: 0x0001D4BC File Offset: 0x0001B6BC
		protected override void OnValidate(object value)
		{
		}

		// Token: 0x06000A76 RID: 2678 RVA: 0x0001D4C0 File Offset: 0x0001B6C0
		public void Remove(DesignerVerb value)
		{
			base.InnerList.Remove(value);
		}
	}
}
