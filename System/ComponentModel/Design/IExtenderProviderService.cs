﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000117 RID: 279
	public interface IExtenderProviderService
	{
		// Token: 0x06000AF4 RID: 2804
		void AddExtenderProvider(IExtenderProvider provider);

		// Token: 0x06000AF5 RID: 2805
		void RemoveExtenderProvider(IExtenderProvider provider);
	}
}
