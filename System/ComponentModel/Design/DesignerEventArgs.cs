﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x020000FE RID: 254
	public class DesignerEventArgs : EventArgs
	{
		// Token: 0x06000A54 RID: 2644 RVA: 0x0001D2B0 File Offset: 0x0001B4B0
		public DesignerEventArgs(IDesignerHost host)
		{
			this.host = host;
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000A55 RID: 2645 RVA: 0x0001D2C0 File Offset: 0x0001B4C0
		public IDesignerHost Designer
		{
			get
			{
				return this.host;
			}
		}

		// Token: 0x040002C0 RID: 704
		private IDesignerHost host;
	}
}
