﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020004FC RID: 1276
	// (Invoke) Token: 0x06002C9C RID: 11420
	[ComVisible(true)]
	public delegate void ComponentEventHandler(object sender, ComponentEventArgs e);
}
