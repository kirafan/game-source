﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x0200013B RID: 315
	public class StandardCommands
	{
		// Token: 0x06000BC3 RID: 3011 RVA: 0x0001EB20 File Offset: 0x0001CD20
		static StandardCommands()
		{
			Guid menuGroup = new Guid("5efc7975-14bc-11cf-9b2b-00aa00573819");
			Guid menuGroup2 = new Guid("74d21313-2aee-11d1-8bfb-00a0c90f26f7");
			StandardCommands.AlignBottom = new CommandID(menuGroup, 1);
			StandardCommands.AlignHorizontalCenters = new CommandID(menuGroup, 2);
			StandardCommands.AlignLeft = new CommandID(menuGroup, 3);
			StandardCommands.AlignRight = new CommandID(menuGroup, 4);
			StandardCommands.AlignToGrid = new CommandID(menuGroup, 5);
			StandardCommands.AlignTop = new CommandID(menuGroup, 6);
			StandardCommands.AlignVerticalCenters = new CommandID(menuGroup, 7);
			StandardCommands.ArrangeBottom = new CommandID(menuGroup, 8);
			StandardCommands.ArrangeIcons = new CommandID(menuGroup2, 12298);
			StandardCommands.ArrangeRight = new CommandID(menuGroup, 9);
			StandardCommands.BringForward = new CommandID(menuGroup, 10);
			StandardCommands.BringToFront = new CommandID(menuGroup, 11);
			StandardCommands.CenterHorizontally = new CommandID(menuGroup, 12);
			StandardCommands.CenterVertically = new CommandID(menuGroup, 13);
			StandardCommands.Copy = new CommandID(menuGroup, 15);
			StandardCommands.Cut = new CommandID(menuGroup, 16);
			StandardCommands.Delete = new CommandID(menuGroup, 17);
			StandardCommands.F1Help = new CommandID(menuGroup, 377);
			StandardCommands.Group = new CommandID(menuGroup, 20);
			StandardCommands.HorizSpaceConcatenate = new CommandID(menuGroup, 21);
			StandardCommands.HorizSpaceDecrease = new CommandID(menuGroup, 22);
			StandardCommands.HorizSpaceIncrease = new CommandID(menuGroup, 23);
			StandardCommands.HorizSpaceMakeEqual = new CommandID(menuGroup, 24);
			StandardCommands.LineupIcons = new CommandID(menuGroup2, 12299);
			StandardCommands.LockControls = new CommandID(menuGroup, 369);
			StandardCommands.MultiLevelRedo = new CommandID(menuGroup, 30);
			StandardCommands.MultiLevelUndo = new CommandID(menuGroup, 44);
			StandardCommands.Paste = new CommandID(menuGroup, 26);
			StandardCommands.Properties = new CommandID(menuGroup, 28);
			StandardCommands.PropertiesWindow = new CommandID(menuGroup, 235);
			StandardCommands.Redo = new CommandID(menuGroup, 29);
			StandardCommands.Replace = new CommandID(menuGroup, 230);
			StandardCommands.SelectAll = new CommandID(menuGroup, 31);
			StandardCommands.SendBackward = new CommandID(menuGroup, 32);
			StandardCommands.SendToBack = new CommandID(menuGroup, 33);
			StandardCommands.ShowGrid = new CommandID(menuGroup, 103);
			StandardCommands.ShowLargeIcons = new CommandID(menuGroup2, 12300);
			StandardCommands.SizeToControl = new CommandID(menuGroup, 35);
			StandardCommands.SizeToControlHeight = new CommandID(menuGroup, 36);
			StandardCommands.SizeToControlWidth = new CommandID(menuGroup, 37);
			StandardCommands.SizeToFit = new CommandID(menuGroup, 38);
			StandardCommands.SizeToGrid = new CommandID(menuGroup, 39);
			StandardCommands.SnapToGrid = new CommandID(menuGroup, 40);
			StandardCommands.TabOrder = new CommandID(menuGroup, 41);
			StandardCommands.Undo = new CommandID(menuGroup, 43);
			StandardCommands.Ungroup = new CommandID(menuGroup, 45);
			StandardCommands.VerbFirst = new CommandID(menuGroup2, 8192);
			StandardCommands.VerbLast = new CommandID(menuGroup2, 8448);
			StandardCommands.VertSpaceConcatenate = new CommandID(menuGroup, 46);
			StandardCommands.VertSpaceDecrease = new CommandID(menuGroup, 47);
			StandardCommands.VertSpaceIncrease = new CommandID(menuGroup, 48);
			StandardCommands.VertSpaceMakeEqual = new CommandID(menuGroup, 49);
			StandardCommands.ViewGrid = new CommandID(menuGroup, 125);
			StandardCommands.DocumentOutline = new CommandID(menuGroup, 239);
			StandardCommands.ViewCode = new CommandID(menuGroup, 333);
		}

		// Token: 0x04000313 RID: 787
		public static readonly CommandID AlignBottom;

		// Token: 0x04000314 RID: 788
		public static readonly CommandID AlignHorizontalCenters;

		// Token: 0x04000315 RID: 789
		public static readonly CommandID AlignLeft;

		// Token: 0x04000316 RID: 790
		public static readonly CommandID AlignRight;

		// Token: 0x04000317 RID: 791
		public static readonly CommandID AlignToGrid;

		// Token: 0x04000318 RID: 792
		public static readonly CommandID AlignTop;

		// Token: 0x04000319 RID: 793
		public static readonly CommandID AlignVerticalCenters;

		// Token: 0x0400031A RID: 794
		public static readonly CommandID ArrangeBottom;

		// Token: 0x0400031B RID: 795
		public static readonly CommandID ArrangeIcons;

		// Token: 0x0400031C RID: 796
		public static readonly CommandID ArrangeRight;

		// Token: 0x0400031D RID: 797
		public static readonly CommandID BringForward;

		// Token: 0x0400031E RID: 798
		public static readonly CommandID BringToFront;

		// Token: 0x0400031F RID: 799
		public static readonly CommandID CenterHorizontally;

		// Token: 0x04000320 RID: 800
		public static readonly CommandID CenterVertically;

		// Token: 0x04000321 RID: 801
		public static readonly CommandID Copy;

		// Token: 0x04000322 RID: 802
		public static readonly CommandID Cut;

		// Token: 0x04000323 RID: 803
		public static readonly CommandID Delete;

		// Token: 0x04000324 RID: 804
		public static readonly CommandID F1Help;

		// Token: 0x04000325 RID: 805
		public static readonly CommandID Group;

		// Token: 0x04000326 RID: 806
		public static readonly CommandID HorizSpaceConcatenate;

		// Token: 0x04000327 RID: 807
		public static readonly CommandID HorizSpaceDecrease;

		// Token: 0x04000328 RID: 808
		public static readonly CommandID HorizSpaceIncrease;

		// Token: 0x04000329 RID: 809
		public static readonly CommandID HorizSpaceMakeEqual;

		// Token: 0x0400032A RID: 810
		public static readonly CommandID LineupIcons;

		// Token: 0x0400032B RID: 811
		public static readonly CommandID LockControls;

		// Token: 0x0400032C RID: 812
		public static readonly CommandID MultiLevelRedo;

		// Token: 0x0400032D RID: 813
		public static readonly CommandID MultiLevelUndo;

		// Token: 0x0400032E RID: 814
		public static readonly CommandID Paste;

		// Token: 0x0400032F RID: 815
		public static readonly CommandID Properties;

		// Token: 0x04000330 RID: 816
		public static readonly CommandID PropertiesWindow;

		// Token: 0x04000331 RID: 817
		public static readonly CommandID Redo;

		// Token: 0x04000332 RID: 818
		public static readonly CommandID Replace;

		// Token: 0x04000333 RID: 819
		public static readonly CommandID SelectAll;

		// Token: 0x04000334 RID: 820
		public static readonly CommandID SendBackward;

		// Token: 0x04000335 RID: 821
		public static readonly CommandID SendToBack;

		// Token: 0x04000336 RID: 822
		public static readonly CommandID ShowGrid;

		// Token: 0x04000337 RID: 823
		public static readonly CommandID ShowLargeIcons;

		// Token: 0x04000338 RID: 824
		public static readonly CommandID SizeToControl;

		// Token: 0x04000339 RID: 825
		public static readonly CommandID SizeToControlHeight;

		// Token: 0x0400033A RID: 826
		public static readonly CommandID SizeToControlWidth;

		// Token: 0x0400033B RID: 827
		public static readonly CommandID SizeToFit;

		// Token: 0x0400033C RID: 828
		public static readonly CommandID SizeToGrid;

		// Token: 0x0400033D RID: 829
		public static readonly CommandID SnapToGrid;

		// Token: 0x0400033E RID: 830
		public static readonly CommandID TabOrder;

		// Token: 0x0400033F RID: 831
		public static readonly CommandID Undo;

		// Token: 0x04000340 RID: 832
		public static readonly CommandID Ungroup;

		// Token: 0x04000341 RID: 833
		public static readonly CommandID VerbFirst;

		// Token: 0x04000342 RID: 834
		public static readonly CommandID VerbLast;

		// Token: 0x04000343 RID: 835
		public static readonly CommandID VertSpaceConcatenate;

		// Token: 0x04000344 RID: 836
		public static readonly CommandID VertSpaceDecrease;

		// Token: 0x04000345 RID: 837
		public static readonly CommandID VertSpaceIncrease;

		// Token: 0x04000346 RID: 838
		public static readonly CommandID VertSpaceMakeEqual;

		// Token: 0x04000347 RID: 839
		public static readonly CommandID ViewGrid;

		// Token: 0x04000348 RID: 840
		public static readonly CommandID DocumentOutline;

		// Token: 0x04000349 RID: 841
		public static readonly CommandID ViewCode;
	}
}
