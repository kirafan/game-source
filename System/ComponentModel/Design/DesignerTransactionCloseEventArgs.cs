﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x020000FF RID: 255
	[ComVisible(true)]
	public class DesignerTransactionCloseEventArgs : EventArgs
	{
		// Token: 0x06000A56 RID: 2646 RVA: 0x0001D2C8 File Offset: 0x0001B4C8
		public DesignerTransactionCloseEventArgs(bool commit, bool lastTransaction)
		{
			this.commit = commit;
			this.last_transaction = lastTransaction;
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x0001D2E0 File Offset: 0x0001B4E0
		[Obsolete("Use another constructor that indicates lastTransaction")]
		public DesignerTransactionCloseEventArgs(bool commit)
		{
			this.commit = commit;
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000A58 RID: 2648 RVA: 0x0001D2F0 File Offset: 0x0001B4F0
		public bool LastTransaction
		{
			get
			{
				return this.last_transaction;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000A59 RID: 2649 RVA: 0x0001D2F8 File Offset: 0x0001B4F8
		public bool TransactionCommitted
		{
			get
			{
				return this.commit;
			}
		}

		// Token: 0x040002C1 RID: 705
		private bool commit;

		// Token: 0x040002C2 RID: 706
		private bool last_transaction;
	}
}
