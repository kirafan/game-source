﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel.Design
{
	// Token: 0x0200011F RID: 287
	[ComVisible(true)]
	public interface IServiceContainer : IServiceProvider
	{
		// Token: 0x06000B1A RID: 2842
		void AddService(Type serviceType, object serviceInstance);

		// Token: 0x06000B1B RID: 2843
		void AddService(Type serviceType, ServiceCreatorCallback callback);

		// Token: 0x06000B1C RID: 2844
		void AddService(Type serviceType, object serviceInstance, bool promote);

		// Token: 0x06000B1D RID: 2845
		void AddService(Type serviceType, ServiceCreatorCallback callback, bool promote);

		// Token: 0x06000B1E RID: 2846
		void RemoveService(Type serviceType);

		// Token: 0x06000B1F RID: 2847
		void RemoveService(Type serviceType, bool promote);
	}
}
