﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x02000113 RID: 275
	public interface IDesignerOptionService
	{
		// Token: 0x06000AE6 RID: 2790
		object GetOptionValue(string pageName, string valueName);

		// Token: 0x06000AE7 RID: 2791
		void SetOptionValue(string pageName, string valueName, object value);
	}
}
