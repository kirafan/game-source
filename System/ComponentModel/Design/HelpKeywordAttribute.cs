﻿using System;

namespace System.ComponentModel.Design
{
	// Token: 0x0200010A RID: 266
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	[Serializable]
	public sealed class HelpKeywordAttribute : Attribute
	{
		// Token: 0x06000A9B RID: 2715 RVA: 0x0001D900 File Offset: 0x0001BB00
		public HelpKeywordAttribute()
		{
		}

		// Token: 0x06000A9C RID: 2716 RVA: 0x0001D908 File Offset: 0x0001BB08
		public HelpKeywordAttribute(string keyword)
		{
			this.contextKeyword = keyword;
		}

		// Token: 0x06000A9D RID: 2717 RVA: 0x0001D918 File Offset: 0x0001BB18
		public HelpKeywordAttribute(Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			this.contextKeyword = t.FullName;
		}

		// Token: 0x06000A9F RID: 2719 RVA: 0x0001D944 File Offset: 0x0001BB44
		public override bool Equals(object other)
		{
			if (other == null)
			{
				return false;
			}
			HelpKeywordAttribute helpKeywordAttribute = other as HelpKeywordAttribute;
			return helpKeywordAttribute != null && helpKeywordAttribute.contextKeyword == this.contextKeyword;
		}

		// Token: 0x06000AA0 RID: 2720 RVA: 0x0001D97C File Offset: 0x0001BB7C
		public override int GetHashCode()
		{
			return (this.contextKeyword == null) ? 0 : this.contextKeyword.GetHashCode();
		}

		// Token: 0x06000AA1 RID: 2721 RVA: 0x0001D99C File Offset: 0x0001BB9C
		public override bool IsDefaultAttribute()
		{
			return this.contextKeyword == null;
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x0001D9A8 File Offset: 0x0001BBA8
		public string HelpKeyword
		{
			get
			{
				return this.contextKeyword;
			}
		}

		// Token: 0x040002DE RID: 734
		public static readonly HelpKeywordAttribute Default;

		// Token: 0x040002DF RID: 735
		private string contextKeyword;
	}
}
