﻿using System;
using System.Reflection;

namespace System.ComponentModel.Design
{
	// Token: 0x02000123 RID: 291
	public interface ITypeResolutionService
	{
		// Token: 0x06000B26 RID: 2854
		Assembly GetAssembly(AssemblyName name);

		// Token: 0x06000B27 RID: 2855
		Assembly GetAssembly(AssemblyName name, bool throwOnError);

		// Token: 0x06000B28 RID: 2856
		string GetPathOfAssembly(AssemblyName name);

		// Token: 0x06000B29 RID: 2857
		Type GetType(string name);

		// Token: 0x06000B2A RID: 2858
		Type GetType(string name, bool throwOnError);

		// Token: 0x06000B2B RID: 2859
		Type GetType(string name, bool throwOnError, bool ignoreCase);

		// Token: 0x06000B2C RID: 2860
		void ReferenceAssembly(AssemblyName name);
	}
}
