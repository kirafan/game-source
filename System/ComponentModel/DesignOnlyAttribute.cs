﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000125 RID: 293
	[AttributeUsage(AttributeTargets.All)]
	public sealed class DesignOnlyAttribute : Attribute
	{
		// Token: 0x06000B3F RID: 2879 RVA: 0x0001DB90 File Offset: 0x0001BD90
		public DesignOnlyAttribute(bool design_only)
		{
			this.design_only = design_only;
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000B41 RID: 2881 RVA: 0x0001DBC4 File Offset: 0x0001BDC4
		public bool IsDesignOnly
		{
			get
			{
				return this.design_only;
			}
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x0001DBCC File Offset: 0x0001BDCC
		public override bool Equals(object obj)
		{
			return obj is DesignOnlyAttribute && (obj == this || ((DesignOnlyAttribute)obj).IsDesignOnly == this.design_only);
		}

		// Token: 0x06000B43 RID: 2883 RVA: 0x0001DBF8 File Offset: 0x0001BDF8
		public override int GetHashCode()
		{
			return this.design_only.GetHashCode();
		}

		// Token: 0x06000B44 RID: 2884 RVA: 0x0001DC08 File Offset: 0x0001BE08
		public override bool IsDefaultAttribute()
		{
			return this.design_only == DesignOnlyAttribute.Default.IsDesignOnly;
		}

		// Token: 0x040002EC RID: 748
		private bool design_only;

		// Token: 0x040002ED RID: 749
		public static readonly DesignOnlyAttribute Default = new DesignOnlyAttribute(false);

		// Token: 0x040002EE RID: 750
		public static readonly DesignOnlyAttribute No = new DesignOnlyAttribute(false);

		// Token: 0x040002EF RID: 751
		public static readonly DesignOnlyAttribute Yes = new DesignOnlyAttribute(true);
	}
}
