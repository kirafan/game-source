﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000E9 RID: 233
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class DataObjectMethodAttribute : Attribute
	{
		// Token: 0x060009BC RID: 2492 RVA: 0x0001C2F0 File Offset: 0x0001A4F0
		public DataObjectMethodAttribute(DataObjectMethodType methodType) : this(methodType, false)
		{
		}

		// Token: 0x060009BD RID: 2493 RVA: 0x0001C2FC File Offset: 0x0001A4FC
		public DataObjectMethodAttribute(DataObjectMethodType methodType, bool isDefault)
		{
			this._methodType = methodType;
			this._isDefault = isDefault;
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x060009BE RID: 2494 RVA: 0x0001C314 File Offset: 0x0001A514
		public DataObjectMethodType MethodType
		{
			get
			{
				return this._methodType;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x060009BF RID: 2495 RVA: 0x0001C31C File Offset: 0x0001A51C
		public bool IsDefault
		{
			get
			{
				return this._isDefault;
			}
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x0001C324 File Offset: 0x0001A524
		public override bool Match(object obj)
		{
			return obj is DataObjectMethodAttribute && ((DataObjectMethodAttribute)obj).MethodType == this.MethodType;
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x0001C354 File Offset: 0x0001A554
		public override bool Equals(object obj)
		{
			return this.Match(obj) && ((DataObjectMethodAttribute)obj).IsDefault == this.IsDefault;
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x0001C388 File Offset: 0x0001A588
		public override int GetHashCode()
		{
			return this.MethodType.GetHashCode() ^ this.IsDefault.GetHashCode();
		}

		// Token: 0x04000293 RID: 659
		private readonly DataObjectMethodType _methodType;

		// Token: 0x04000294 RID: 660
		private readonly bool _isDefault;
	}
}
