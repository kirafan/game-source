﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000171 RID: 369
	[ComVisible(true)]
	public interface ITypeDescriptorContext : IServiceProvider
	{
		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000CD8 RID: 3288
		IContainer Container { get; }

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000CD9 RID: 3289
		object Instance { get; }

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000CDA RID: 3290
		PropertyDescriptor PropertyDescriptor { get; }

		// Token: 0x06000CDB RID: 3291
		void OnComponentChanged();

		// Token: 0x06000CDC RID: 3292
		bool OnComponentChanging();
	}
}
