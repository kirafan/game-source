﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000174 RID: 372
	public abstract class License : IDisposable
	{
		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000CE5 RID: 3301
		public abstract string LicenseKey { get; }

		// Token: 0x06000CE6 RID: 3302
		public abstract void Dispose();
	}
}
