﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000D8 RID: 216
	public enum CollectionChangeAction
	{
		// Token: 0x0400027A RID: 634
		Add = 1,
		// Token: 0x0400027B RID: 635
		Remove,
		// Token: 0x0400027C RID: 636
		Refresh
	}
}
