﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020004F8 RID: 1272
	// (Invoke) Token: 0x06002C8C RID: 11404
	public delegate void CollectionChangeEventHandler(object sender, CollectionChangeEventArgs e);
}
