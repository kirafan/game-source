﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001A0 RID: 416
	public class RefreshEventArgs : EventArgs
	{
		// Token: 0x06000EB4 RID: 3764 RVA: 0x000263D4 File Offset: 0x000245D4
		public RefreshEventArgs(object componentChanged)
		{
			if (componentChanged == null)
			{
				throw new ArgumentNullException("componentChanged");
			}
			this.component = componentChanged;
			this.type = this.component.GetType();
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x00026408 File Offset: 0x00024608
		public RefreshEventArgs(Type typeChanged)
		{
			this.type = typeChanged;
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06000EB6 RID: 3766 RVA: 0x00026418 File Offset: 0x00024618
		public object ComponentChanged
		{
			get
			{
				return this.component;
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06000EB7 RID: 3767 RVA: 0x00026420 File Offset: 0x00024620
		public Type TypeChanged
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x04000424 RID: 1060
		private object component;

		// Token: 0x04000425 RID: 1061
		private Type type;
	}
}
