﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x020001BE RID: 446
	public abstract class TypeListConverter : TypeConverter
	{
		// Token: 0x06000FB4 RID: 4020 RVA: 0x0002960C File Offset: 0x0002780C
		protected TypeListConverter(Type[] types)
		{
			this.types = types;
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x0002961C File Offset: 0x0002781C
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x00029638 File Offset: 0x00027838
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x00029654 File Offset: 0x00027854
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x00029660 File Offset: 0x00027860
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value != null && value.GetType() == typeof(Type))
			{
				return ((Type)value).ToString();
			}
			throw new InvalidCastException("Cannot cast to System.Type");
		}

		// Token: 0x06000FB9 RID: 4025 RVA: 0x000296B0 File Offset: 0x000278B0
		public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			return new TypeConverter.StandardValuesCollection(this.types);
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x000296C0 File Offset: 0x000278C0
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x000296C4 File Offset: 0x000278C4
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x04000467 RID: 1127
		private Type[] types;
	}
}
