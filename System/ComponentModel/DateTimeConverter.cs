﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x020000EB RID: 235
	public class DateTimeConverter : TypeConverter
	{
		// Token: 0x060009C4 RID: 2500 RVA: 0x0001C3BC File Offset: 0x0001A5BC
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x0001C3D8 File Offset: 0x0001A5D8
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x0001C3F4 File Offset: 0x0001A5F4
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				string text = (string)value;
				try
				{
					if (text != null && text.Trim().Length == 0)
					{
						return DateTime.MinValue;
					}
					if (culture == null)
					{
						return DateTime.Parse(text);
					}
					DateTimeFormatInfo provider = (DateTimeFormatInfo)culture.GetFormat(typeof(DateTimeFormatInfo));
					return DateTime.Parse(text, provider);
				}
				catch
				{
					throw new FormatException(text + " is not a valid DateTime value.");
				}
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x0001C4BC File Offset: 0x0001A6BC
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is DateTime)
			{
				DateTime d = (DateTime)value;
				if (destinationType == typeof(string))
				{
					if (culture == null)
					{
						culture = CultureInfo.CurrentCulture;
					}
					if (d == DateTime.MinValue)
					{
						return string.Empty;
					}
					DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)culture.GetFormat(typeof(DateTimeFormatInfo));
					if (culture == CultureInfo.InvariantCulture)
					{
						if (d.Equals(d.Date))
						{
							return d.ToString("yyyy-MM-dd", culture);
						}
						return d.ToString(culture);
					}
					else
					{
						if (d == d.Date)
						{
							return d.ToString(dateTimeFormatInfo.ShortDatePattern, culture);
						}
						return d.ToString(dateTimeFormatInfo.ShortDatePattern + " " + dateTimeFormatInfo.ShortTimePattern, culture);
					}
				}
				else if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor))
				{
					ConstructorInfo constructor = typeof(DateTime).GetConstructor(new Type[]
					{
						typeof(long)
					});
					return new System.ComponentModel.Design.Serialization.InstanceDescriptor(constructor, new object[]
					{
						d.Ticks
					});
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
