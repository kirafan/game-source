﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200019A RID: 410
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public sealed class ProvidePropertyAttribute : Attribute
	{
		// Token: 0x06000E7F RID: 3711 RVA: 0x00025458 File Offset: 0x00023658
		public ProvidePropertyAttribute(string propertyName, string receiverTypeName)
		{
			this.Property = propertyName;
			this.Receiver = receiverTypeName;
		}

		// Token: 0x06000E80 RID: 3712 RVA: 0x00025470 File Offset: 0x00023670
		public ProvidePropertyAttribute(string propertyName, Type receiverType)
		{
			this.Property = propertyName;
			this.Receiver = receiverType.AssemblyQualifiedName;
		}

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000E81 RID: 3713 RVA: 0x0002548C File Offset: 0x0002368C
		public string PropertyName
		{
			get
			{
				return this.Property;
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06000E82 RID: 3714 RVA: 0x00025494 File Offset: 0x00023694
		public string ReceiverTypeName
		{
			get
			{
				return this.Receiver;
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06000E83 RID: 3715 RVA: 0x0002549C File Offset: 0x0002369C
		public override object TypeId
		{
			get
			{
				return base.TypeId + this.Property;
			}
		}

		// Token: 0x06000E84 RID: 3716 RVA: 0x000254B0 File Offset: 0x000236B0
		public override bool Equals(object obj)
		{
			return obj is ProvidePropertyAttribute && (obj == this || (((ProvidePropertyAttribute)obj).PropertyName == this.Property && ((ProvidePropertyAttribute)obj).ReceiverTypeName == this.Receiver));
		}

		// Token: 0x06000E85 RID: 3717 RVA: 0x00025508 File Offset: 0x00023708
		public override int GetHashCode()
		{
			return (this.Property + this.Receiver).GetHashCode();
		}

		// Token: 0x0400040E RID: 1038
		private string Property;

		// Token: 0x0400040F RID: 1039
		private string Receiver;
	}
}
