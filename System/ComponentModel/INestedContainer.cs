﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200015D RID: 349
	public interface INestedContainer : IDisposable, IContainer
	{
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000C98 RID: 3224
		IComponent Owner { get; }
	}
}
