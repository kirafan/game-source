﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000EA RID: 234
	public enum DataObjectMethodType
	{
		// Token: 0x04000296 RID: 662
		Fill,
		// Token: 0x04000297 RID: 663
		Select,
		// Token: 0x04000298 RID: 664
		Update,
		// Token: 0x04000299 RID: 665
		Insert,
		// Token: 0x0400029A RID: 666
		Delete
	}
}
