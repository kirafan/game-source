﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001A2 RID: 418
	public enum RefreshProperties
	{
		// Token: 0x0400042B RID: 1067
		All = 1,
		// Token: 0x0400042C RID: 1068
		None = 0,
		// Token: 0x0400042D RID: 1069
		Repaint = 2
	}
}
