﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000E8 RID: 232
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class DataObjectFieldAttribute : Attribute
	{
		// Token: 0x060009B2 RID: 2482 RVA: 0x0001C184 File Offset: 0x0001A384
		public DataObjectFieldAttribute(bool primaryKey)
		{
			this.primary_key = primaryKey;
		}

		// Token: 0x060009B3 RID: 2483 RVA: 0x0001C19C File Offset: 0x0001A39C
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity)
		{
			this.primary_key = primaryKey;
			this.is_identity = isIdentity;
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x0001C1BC File Offset: 0x0001A3BC
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity, bool isNullable)
		{
			this.primary_key = primaryKey;
			this.is_identity = isIdentity;
			this.is_nullable = isNullable;
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x0001C1EC File Offset: 0x0001A3EC
		public DataObjectFieldAttribute(bool primaryKey, bool isIdentity, bool isNullable, int length)
		{
			this.primary_key = primaryKey;
			this.is_identity = isIdentity;
			this.is_nullable = isNullable;
			this.length = length;
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x060009B6 RID: 2486 RVA: 0x0001C224 File Offset: 0x0001A424
		public bool IsIdentity
		{
			get
			{
				return this.is_identity;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x060009B7 RID: 2487 RVA: 0x0001C22C File Offset: 0x0001A42C
		public bool IsNullable
		{
			get
			{
				return this.is_nullable;
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x060009B8 RID: 2488 RVA: 0x0001C234 File Offset: 0x0001A434
		public int Length
		{
			get
			{
				return this.length;
			}
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x060009B9 RID: 2489 RVA: 0x0001C23C File Offset: 0x0001A43C
		public bool PrimaryKey
		{
			get
			{
				return this.primary_key;
			}
		}

		// Token: 0x060009BA RID: 2490 RVA: 0x0001C244 File Offset: 0x0001A444
		public override bool Equals(object obj)
		{
			DataObjectFieldAttribute dataObjectFieldAttribute = obj as DataObjectFieldAttribute;
			return dataObjectFieldAttribute != null && (dataObjectFieldAttribute.primary_key == this.primary_key && dataObjectFieldAttribute.is_identity == this.is_identity && dataObjectFieldAttribute.is_nullable == this.is_nullable) && dataObjectFieldAttribute.length == this.length;
		}

		// Token: 0x060009BB RID: 2491 RVA: 0x0001C2A4 File Offset: 0x0001A4A4
		public override int GetHashCode()
		{
			return (((!this.primary_key) ? 0 : 1) | ((!this.is_identity) ? 0 : 2) | ((!this.is_nullable) ? 0 : 4)) ^ this.length;
		}

		// Token: 0x0400028F RID: 655
		private bool primary_key;

		// Token: 0x04000290 RID: 656
		private bool is_identity;

		// Token: 0x04000291 RID: 657
		private bool is_nullable;

		// Token: 0x04000292 RID: 658
		private int length = -1;
	}
}
