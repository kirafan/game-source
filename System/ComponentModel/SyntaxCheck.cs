﻿using System;
using System.IO;

namespace System.ComponentModel
{
	// Token: 0x020001A9 RID: 425
	public static class SyntaxCheck
	{
		// Token: 0x06000ED8 RID: 3800 RVA: 0x000266DC File Offset: 0x000248DC
		public static bool CheckMachineName(string value)
		{
			return value != null && value.Trim().Length != 0 && value.IndexOf('\\') == -1;
		}

		// Token: 0x06000ED9 RID: 3801 RVA: 0x0002670C File Offset: 0x0002490C
		public static bool CheckPath(string value)
		{
			return value != null && value.Trim().Length != 0 && value.StartsWith("\\\\");
		}

		// Token: 0x06000EDA RID: 3802 RVA: 0x00026734 File Offset: 0x00024934
		public static bool CheckRootedPath(string value)
		{
			return value != null && value.Trim().Length != 0 && Path.IsPathRooted(value);
		}
	}
}
