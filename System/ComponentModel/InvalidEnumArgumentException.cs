﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace System.ComponentModel
{
	// Token: 0x0200016A RID: 362
	[Serializable]
	public class InvalidEnumArgumentException : ArgumentException
	{
		// Token: 0x06000CC3 RID: 3267 RVA: 0x000204F8 File Offset: 0x0001E6F8
		public InvalidEnumArgumentException() : this(null)
		{
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x00020504 File Offset: 0x0001E704
		public InvalidEnumArgumentException(string message) : base(message)
		{
		}

		// Token: 0x06000CC5 RID: 3269 RVA: 0x00020510 File Offset: 0x0001E710
		public InvalidEnumArgumentException(string argumentName, int invalidValue, Type enumClass) : base(string.Format(CultureInfo.CurrentCulture, "The value of argument '{0}' ({1}) is invalid for Enum type '{2}'.", new object[]
		{
			argumentName,
			invalidValue,
			enumClass.Name
		}), argumentName)
		{
		}

		// Token: 0x06000CC6 RID: 3270 RVA: 0x00020550 File Offset: 0x0001E750
		public InvalidEnumArgumentException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000CC7 RID: 3271 RVA: 0x0002055C File Offset: 0x0001E75C
		protected InvalidEnumArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
