﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000158 RID: 344
	public interface IEditableObject
	{
		// Token: 0x06000C8A RID: 3210
		void BeginEdit();

		// Token: 0x06000C8B RID: 3211
		void CancelEdit();

		// Token: 0x06000C8C RID: 3212
		void EndEdit();
	}
}
