﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000148 RID: 328
	[ComVisible(true)]
	public abstract class EventDescriptor : MemberDescriptor
	{
		// Token: 0x06000C27 RID: 3111 RVA: 0x0001FD28 File Offset: 0x0001DF28
		protected EventDescriptor(MemberDescriptor desc) : base(desc)
		{
		}

		// Token: 0x06000C28 RID: 3112 RVA: 0x0001FD34 File Offset: 0x0001DF34
		protected EventDescriptor(MemberDescriptor desc, Attribute[] attrs) : base(desc, attrs)
		{
		}

		// Token: 0x06000C29 RID: 3113 RVA: 0x0001FD40 File Offset: 0x0001DF40
		protected EventDescriptor(string str, Attribute[] attrs) : base(str, attrs)
		{
		}

		// Token: 0x06000C2A RID: 3114
		public abstract void AddEventHandler(object component, Delegate value);

		// Token: 0x06000C2B RID: 3115
		public abstract void RemoveEventHandler(object component, Delegate value);

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06000C2C RID: 3116
		public abstract Type ComponentType { get; }

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06000C2D RID: 3117
		public abstract Type EventType { get; }

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06000C2E RID: 3118
		public abstract bool IsMulticast { get; }
	}
}
