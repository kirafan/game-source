﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001A4 RID: 420
	public class RunWorkerCompletedEventArgs : AsyncCompletedEventArgs
	{
		// Token: 0x06000EC4 RID: 3780 RVA: 0x00026540 File Offset: 0x00024740
		public RunWorkerCompletedEventArgs(object result, Exception error, bool cancelled) : base(error, cancelled, null)
		{
			this.result = result;
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06000EC5 RID: 3781 RVA: 0x00026554 File Offset: 0x00024754
		public object Result
		{
			get
			{
				base.RaiseExceptionIfNecessary();
				return this.result;
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06000EC6 RID: 3782 RVA: 0x00026564 File Offset: 0x00024764
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public new object UserState
		{
			get
			{
				return null;
			}
		}

		// Token: 0x04000432 RID: 1074
		private object result;
	}
}
