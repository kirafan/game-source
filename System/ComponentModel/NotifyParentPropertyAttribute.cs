﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000190 RID: 400
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class NotifyParentPropertyAttribute : Attribute
	{
		// Token: 0x06000DF7 RID: 3575 RVA: 0x000240D8 File Offset: 0x000222D8
		public NotifyParentPropertyAttribute(bool notifyParent)
		{
			this.notifyParent = notifyParent;
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000DF9 RID: 3577 RVA: 0x0002410C File Offset: 0x0002230C
		public bool NotifyParent
		{
			get
			{
				return this.notifyParent;
			}
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x00024114 File Offset: 0x00022314
		public override bool Equals(object obj)
		{
			return obj is NotifyParentPropertyAttribute && (obj == this || ((NotifyParentPropertyAttribute)obj).NotifyParent == this.notifyParent);
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x00024140 File Offset: 0x00022340
		public override int GetHashCode()
		{
			return this.notifyParent.GetHashCode();
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x00024150 File Offset: 0x00022350
		public override bool IsDefaultAttribute()
		{
			return this.notifyParent == NotifyParentPropertyAttribute.Default.NotifyParent;
		}

		// Token: 0x040003F3 RID: 1011
		private bool notifyParent;

		// Token: 0x040003F4 RID: 1012
		public static readonly NotifyParentPropertyAttribute Default = new NotifyParentPropertyAttribute(false);

		// Token: 0x040003F5 RID: 1013
		public static readonly NotifyParentPropertyAttribute No = new NotifyParentPropertyAttribute(false);

		// Token: 0x040003F6 RID: 1014
		public static readonly NotifyParentPropertyAttribute Yes = new NotifyParentPropertyAttribute(true);
	}
}
