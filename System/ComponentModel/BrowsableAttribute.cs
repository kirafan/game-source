﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000D3 RID: 211
	[AttributeUsage(AttributeTargets.All)]
	public sealed class BrowsableAttribute : Attribute
	{
		// Token: 0x06000928 RID: 2344 RVA: 0x0001A9C4 File Offset: 0x00018BC4
		public BrowsableAttribute(bool browsable)
		{
			this.browsable = browsable;
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x0600092A RID: 2346 RVA: 0x0001A9F8 File Offset: 0x00018BF8
		public bool Browsable
		{
			get
			{
				return this.browsable;
			}
		}

		// Token: 0x0600092B RID: 2347 RVA: 0x0001AA00 File Offset: 0x00018C00
		public override bool Equals(object obj)
		{
			return obj is BrowsableAttribute && (obj == this || ((BrowsableAttribute)obj).Browsable == this.browsable);
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x0001AA2C File Offset: 0x00018C2C
		public override int GetHashCode()
		{
			return this.browsable.GetHashCode();
		}

		// Token: 0x0600092D RID: 2349 RVA: 0x0001AA3C File Offset: 0x00018C3C
		public override bool IsDefaultAttribute()
		{
			return this.browsable == BrowsableAttribute.Default.Browsable;
		}

		// Token: 0x04000263 RID: 611
		private bool browsable;

		// Token: 0x04000264 RID: 612
		public static readonly BrowsableAttribute Default = new BrowsableAttribute(true);

		// Token: 0x04000265 RID: 613
		public static readonly BrowsableAttribute No = new BrowsableAttribute(false);

		// Token: 0x04000266 RID: 614
		public static readonly BrowsableAttribute Yes = new BrowsableAttribute(true);
	}
}
