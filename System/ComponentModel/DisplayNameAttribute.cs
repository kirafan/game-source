﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200013F RID: 319
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
	public class DisplayNameAttribute : Attribute
	{
		// Token: 0x06000BCD RID: 3021 RVA: 0x0001EF54 File Offset: 0x0001D154
		public DisplayNameAttribute()
		{
			this.attributeDisplayName = string.Empty;
		}

		// Token: 0x06000BCE RID: 3022 RVA: 0x0001EF68 File Offset: 0x0001D168
		public DisplayNameAttribute(string displayName)
		{
			this.attributeDisplayName = displayName;
		}

		// Token: 0x06000BD0 RID: 3024 RVA: 0x0001EF84 File Offset: 0x0001D184
		public override bool IsDefaultAttribute()
		{
			return this.attributeDisplayName != null && this.attributeDisplayName.Length == 0;
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x0001EFA4 File Offset: 0x0001D1A4
		public override int GetHashCode()
		{
			return this.attributeDisplayName.GetHashCode();
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x0001EFB4 File Offset: 0x0001D1B4
		public override bool Equals(object obj)
		{
			if (obj == this)
			{
				return true;
			}
			DisplayNameAttribute displayNameAttribute = obj as DisplayNameAttribute;
			return displayNameAttribute != null && displayNameAttribute.DisplayName == this.attributeDisplayName;
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000BD3 RID: 3027 RVA: 0x0001EFEC File Offset: 0x0001D1EC
		public virtual string DisplayName
		{
			get
			{
				return this.attributeDisplayName;
			}
		}

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x06000BD4 RID: 3028 RVA: 0x0001EFF4 File Offset: 0x0001D1F4
		// (set) Token: 0x06000BD5 RID: 3029 RVA: 0x0001EFFC File Offset: 0x0001D1FC
		protected string DisplayNameValue
		{
			get
			{
				return this.attributeDisplayName;
			}
			set
			{
				this.attributeDisplayName = value;
			}
		}

		// Token: 0x0400035A RID: 858
		public static readonly DisplayNameAttribute Default = new DisplayNameAttribute();

		// Token: 0x0400035B RID: 859
		private string attributeDisplayName;
	}
}
