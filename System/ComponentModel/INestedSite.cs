﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200015E RID: 350
	public interface INestedSite : IServiceProvider, ISite
	{
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000C99 RID: 3225
		string FullName { get; }
	}
}
