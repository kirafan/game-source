﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000144 RID: 324
	public enum EditorBrowsableState
	{
		// Token: 0x04000362 RID: 866
		Always,
		// Token: 0x04000363 RID: 867
		Never,
		// Token: 0x04000364 RID: 868
		Advanced
	}
}
