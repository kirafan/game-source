﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x0200018D RID: 397
	public class MultilineStringConverter : TypeConverter
	{
		// Token: 0x06000DE5 RID: 3557 RVA: 0x00023EA8 File Offset: 0x000220A8
		[MonoTODO]
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000DE6 RID: 3558 RVA: 0x00023EB0 File Offset: 0x000220B0
		[MonoTODO]
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000DE7 RID: 3559 RVA: 0x00023EB8 File Offset: 0x000220B8
		[MonoTODO]
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			throw new NotImplementedException();
		}
	}
}
