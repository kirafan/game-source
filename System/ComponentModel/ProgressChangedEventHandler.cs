﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000521 RID: 1313
	// (Invoke) Token: 0x06002D30 RID: 11568
	public delegate void ProgressChangedEventHandler(object sender, ProgressChangedEventArgs e);
}
