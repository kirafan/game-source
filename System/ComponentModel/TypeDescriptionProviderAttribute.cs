﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001B4 RID: 436
	[AttributeUsage(AttributeTargets.Class, Inherited = true)]
	public sealed class TypeDescriptionProviderAttribute : Attribute
	{
		// Token: 0x06000F3F RID: 3903 RVA: 0x00027190 File Offset: 0x00025390
		public TypeDescriptionProviderAttribute(string typeName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			this.typeName = typeName;
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x000271B0 File Offset: 0x000253B0
		public TypeDescriptionProviderAttribute(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			this.typeName = type.AssemblyQualifiedName;
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000F41 RID: 3905 RVA: 0x000271D8 File Offset: 0x000253D8
		public string TypeName
		{
			get
			{
				return this.typeName;
			}
		}

		// Token: 0x04000449 RID: 1097
		private string typeName;
	}
}
