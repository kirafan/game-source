﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000C3 RID: 195
	public class AddingNewEventArgs : EventArgs
	{
		// Token: 0x0600087C RID: 2172 RVA: 0x00019628 File Offset: 0x00017828
		public AddingNewEventArgs() : this(null)
		{
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x00019634 File Offset: 0x00017834
		public AddingNewEventArgs(object newObject)
		{
			this.obj = newObject;
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x0600087E RID: 2174 RVA: 0x00019644 File Offset: 0x00017844
		// (set) Token: 0x0600087F RID: 2175 RVA: 0x0001964C File Offset: 0x0001784C
		public object NewObject
		{
			get
			{
				return this.obj;
			}
			set
			{
				this.obj = value;
			}
		}

		// Token: 0x04000236 RID: 566
		private object obj;
	}
}
