﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000107 RID: 263
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event)]
	public sealed class DesignerSerializationVisibilityAttribute : Attribute
	{
		// Token: 0x06000A95 RID: 2709 RVA: 0x0001D858 File Offset: 0x0001BA58
		public DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility vis)
		{
			this.visibility = vis;
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000A97 RID: 2711 RVA: 0x0001D8A4 File Offset: 0x0001BAA4
		public DesignerSerializationVisibility Visibility
		{
			get
			{
				return this.visibility;
			}
		}

		// Token: 0x06000A98 RID: 2712 RVA: 0x0001D8AC File Offset: 0x0001BAAC
		public override bool Equals(object obj)
		{
			return obj is DesignerSerializationVisibilityAttribute && (obj == this || ((DesignerSerializationVisibilityAttribute)obj).Visibility == this.visibility);
		}

		// Token: 0x06000A99 RID: 2713 RVA: 0x0001D8D8 File Offset: 0x0001BAD8
		public override int GetHashCode()
		{
			return this.visibility.GetHashCode();
		}

		// Token: 0x06000A9A RID: 2714 RVA: 0x0001D8EC File Offset: 0x0001BAEC
		public override bool IsDefaultAttribute()
		{
			return this.visibility == DesignerSerializationVisibilityAttribute.Default.Visibility;
		}

		// Token: 0x040002D0 RID: 720
		private DesignerSerializationVisibility visibility;

		// Token: 0x040002D1 RID: 721
		public static readonly DesignerSerializationVisibilityAttribute Default = new DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible);

		// Token: 0x040002D2 RID: 722
		public static readonly DesignerSerializationVisibilityAttribute Content = new DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Content);

		// Token: 0x040002D3 RID: 723
		public static readonly DesignerSerializationVisibilityAttribute Hidden = new DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden);

		// Token: 0x040002D4 RID: 724
		public static readonly DesignerSerializationVisibilityAttribute Visible = new DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Visible);
	}
}
