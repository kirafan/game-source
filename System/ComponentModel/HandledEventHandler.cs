﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000503 RID: 1283
	// (Invoke) Token: 0x06002CB8 RID: 11448
	public delegate void HandledEventHandler(object sender, HandledEventArgs e);
}
