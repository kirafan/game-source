﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000159 RID: 345
	public interface IExtenderProvider
	{
		// Token: 0x06000C8D RID: 3213
		bool CanExtend(object extendee);
	}
}
