﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000E3 RID: 227
	public abstract class ContainerFilterService
	{
		// Token: 0x06000992 RID: 2450 RVA: 0x0001BCB8 File Offset: 0x00019EB8
		public virtual ComponentCollection FilterComponents(ComponentCollection components)
		{
			return components;
		}
	}
}
