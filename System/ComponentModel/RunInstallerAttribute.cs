﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001A3 RID: 419
	[AttributeUsage(AttributeTargets.Class)]
	public class RunInstallerAttribute : Attribute
	{
		// Token: 0x06000EBE RID: 3774 RVA: 0x000264B0 File Offset: 0x000246B0
		public RunInstallerAttribute(bool runInstaller)
		{
			this.runInstaller = runInstaller;
		}

		// Token: 0x06000EC0 RID: 3776 RVA: 0x000264E4 File Offset: 0x000246E4
		public override bool Equals(object obj)
		{
			return obj is RunInstallerAttribute && ((RunInstallerAttribute)obj).RunInstaller.Equals(this.runInstaller);
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x00026518 File Offset: 0x00024718
		public override int GetHashCode()
		{
			return this.runInstaller.GetHashCode();
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x00026528 File Offset: 0x00024728
		public override bool IsDefaultAttribute()
		{
			return this.Equals(RunInstallerAttribute.Default);
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000EC3 RID: 3779 RVA: 0x00026538 File Offset: 0x00024738
		public bool RunInstaller
		{
			get
			{
				return this.runInstaller;
			}
		}

		// Token: 0x0400042E RID: 1070
		public static readonly RunInstallerAttribute Yes = new RunInstallerAttribute(true);

		// Token: 0x0400042F RID: 1071
		public static readonly RunInstallerAttribute No = new RunInstallerAttribute(false);

		// Token: 0x04000430 RID: 1072
		public static readonly RunInstallerAttribute Default = new RunInstallerAttribute(false);

		// Token: 0x04000431 RID: 1073
		private bool runInstaller;
	}
}
