﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000160 RID: 352
	public enum InheritanceLevel
	{
		// Token: 0x0400037D RID: 893
		Inherited = 1,
		// Token: 0x0400037E RID: 894
		InheritedReadOnly,
		// Token: 0x0400037F RID: 895
		NotInherited
	}
}
