﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200019B RID: 411
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ReadOnlyAttribute : Attribute
	{
		// Token: 0x06000E86 RID: 3718 RVA: 0x00025520 File Offset: 0x00023720
		public ReadOnlyAttribute(bool read_only)
		{
			this.read_only = read_only;
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000E88 RID: 3720 RVA: 0x00025554 File Offset: 0x00023754
		public bool IsReadOnly
		{
			get
			{
				return this.read_only;
			}
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x0002555C File Offset: 0x0002375C
		public override int GetHashCode()
		{
			return this.read_only.GetHashCode();
		}

		// Token: 0x06000E8A RID: 3722 RVA: 0x0002556C File Offset: 0x0002376C
		public override bool Equals(object o)
		{
			return o is ReadOnlyAttribute && ((ReadOnlyAttribute)o).IsReadOnly.Equals(this.read_only);
		}

		// Token: 0x06000E8B RID: 3723 RVA: 0x000255A0 File Offset: 0x000237A0
		public override bool IsDefaultAttribute()
		{
			return this.Equals(ReadOnlyAttribute.Default);
		}

		// Token: 0x04000410 RID: 1040
		private bool read_only;

		// Token: 0x04000411 RID: 1041
		public static readonly ReadOnlyAttribute No = new ReadOnlyAttribute(false);

		// Token: 0x04000412 RID: 1042
		public static readonly ReadOnlyAttribute Yes = new ReadOnlyAttribute(true);

		// Token: 0x04000413 RID: 1043
		public static readonly ReadOnlyAttribute Default = new ReadOnlyAttribute(false);
	}
}
