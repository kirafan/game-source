﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001AE RID: 430
	[AttributeUsage(AttributeTargets.All)]
	public sealed class TypeConverterAttribute : Attribute
	{
		// Token: 0x06000EF2 RID: 3826 RVA: 0x00026B3C File Offset: 0x00024D3C
		public TypeConverterAttribute()
		{
			this.converter_type = string.Empty;
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x00026B50 File Offset: 0x00024D50
		public TypeConverterAttribute(string typeName)
		{
			this.converter_type = typeName;
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x00026B60 File Offset: 0x00024D60
		public TypeConverterAttribute(Type type)
		{
			this.converter_type = type.AssemblyQualifiedName;
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x00026B80 File Offset: 0x00024D80
		public override bool Equals(object obj)
		{
			return obj is TypeConverterAttribute && ((TypeConverterAttribute)obj).ConverterTypeName == this.converter_type;
		}

		// Token: 0x06000EF7 RID: 3831 RVA: 0x00026BA8 File Offset: 0x00024DA8
		public override int GetHashCode()
		{
			return this.converter_type.GetHashCode();
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06000EF8 RID: 3832 RVA: 0x00026BB8 File Offset: 0x00024DB8
		public string ConverterTypeName
		{
			get
			{
				return this.converter_type;
			}
		}

		// Token: 0x04000442 RID: 1090
		public static readonly TypeConverterAttribute Default = new TypeConverterAttribute();

		// Token: 0x04000443 RID: 1091
		private string converter_type;
	}
}
