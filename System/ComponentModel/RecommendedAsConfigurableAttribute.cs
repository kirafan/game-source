﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200019C RID: 412
	[Obsolete("Use SettingsBindableAttribute instead of RecommendedAsConfigurableAttribute")]
	[AttributeUsage(AttributeTargets.Property)]
	public class RecommendedAsConfigurableAttribute : Attribute
	{
		// Token: 0x06000E8C RID: 3724 RVA: 0x000255B0 File Offset: 0x000237B0
		public RecommendedAsConfigurableAttribute(bool recommendedAsConfigurable)
		{
			this.recommendedAsConfigurable = recommendedAsConfigurable;
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000E8E RID: 3726 RVA: 0x000255E4 File Offset: 0x000237E4
		public bool RecommendedAsConfigurable
		{
			get
			{
				return this.recommendedAsConfigurable;
			}
		}

		// Token: 0x06000E8F RID: 3727 RVA: 0x000255EC File Offset: 0x000237EC
		public override bool Equals(object obj)
		{
			return obj is RecommendedAsConfigurableAttribute && ((RecommendedAsConfigurableAttribute)obj).RecommendedAsConfigurable == this.recommendedAsConfigurable;
		}

		// Token: 0x06000E90 RID: 3728 RVA: 0x0002561C File Offset: 0x0002381C
		public override int GetHashCode()
		{
			return this.recommendedAsConfigurable.GetHashCode();
		}

		// Token: 0x06000E91 RID: 3729 RVA: 0x0002562C File Offset: 0x0002382C
		public override bool IsDefaultAttribute()
		{
			return this.recommendedAsConfigurable == RecommendedAsConfigurableAttribute.Default.RecommendedAsConfigurable;
		}

		// Token: 0x04000414 RID: 1044
		private bool recommendedAsConfigurable;

		// Token: 0x04000415 RID: 1045
		public static readonly RecommendedAsConfigurableAttribute Default = new RecommendedAsConfigurableAttribute(false);

		// Token: 0x04000416 RID: 1046
		public static readonly RecommendedAsConfigurableAttribute No = new RecommendedAsConfigurableAttribute(false);

		// Token: 0x04000417 RID: 1047
		public static readonly RecommendedAsConfigurableAttribute Yes = new RecommendedAsConfigurableAttribute(true);
	}
}
