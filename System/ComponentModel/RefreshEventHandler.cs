﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000507 RID: 1287
	// (Invoke) Token: 0x06002CC8 RID: 11464
	public delegate void RefreshEventHandler(RefreshEventArgs e);
}
