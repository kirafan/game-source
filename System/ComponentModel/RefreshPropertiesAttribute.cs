﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001A1 RID: 417
	[AttributeUsage(AttributeTargets.All)]
	public sealed class RefreshPropertiesAttribute : Attribute
	{
		// Token: 0x06000EB8 RID: 3768 RVA: 0x00026428 File Offset: 0x00024628
		public RefreshPropertiesAttribute(RefreshProperties refresh)
		{
			this.refresh = refresh;
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000EBA RID: 3770 RVA: 0x0002645C File Offset: 0x0002465C
		public RefreshProperties RefreshProperties
		{
			get
			{
				return this.refresh;
			}
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x00026464 File Offset: 0x00024664
		public override bool Equals(object obj)
		{
			return obj is RefreshPropertiesAttribute && (obj == this || ((RefreshPropertiesAttribute)obj).RefreshProperties == this.refresh);
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x00026490 File Offset: 0x00024690
		public override int GetHashCode()
		{
			return this.refresh.GetHashCode();
		}

		// Token: 0x06000EBD RID: 3773 RVA: 0x000264A4 File Offset: 0x000246A4
		public override bool IsDefaultAttribute()
		{
			return this == RefreshPropertiesAttribute.Default;
		}

		// Token: 0x04000426 RID: 1062
		private RefreshProperties refresh;

		// Token: 0x04000427 RID: 1063
		public static readonly RefreshPropertiesAttribute All = new RefreshPropertiesAttribute(RefreshProperties.All);

		// Token: 0x04000428 RID: 1064
		public static readonly RefreshPropertiesAttribute Default = new RefreshPropertiesAttribute(RefreshProperties.None);

		// Token: 0x04000429 RID: 1065
		public static readonly RefreshPropertiesAttribute Repaint = new RefreshPropertiesAttribute(RefreshProperties.Repaint);
	}
}
