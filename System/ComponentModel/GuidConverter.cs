﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x0200014D RID: 333
	public class GuidConverter : TypeConverter
	{
		// Token: 0x06000C44 RID: 3140 RVA: 0x00020014 File Offset: 0x0001E214
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x00020030 File Offset: 0x0001E230
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x06000C46 RID: 3142 RVA: 0x0002006C File Offset: 0x0001E26C
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value.GetType() == typeof(string))
			{
				string text = (string)value;
				try
				{
					return new Guid(text);
				}
				catch
				{
					throw new FormatException(text + "is not a valid GUID.");
				}
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x000200EC File Offset: 0x0001E2EC
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is Guid)
			{
				Guid guid = (Guid)value;
				if (destinationType == typeof(string) && value != null)
				{
					return guid.ToString("D");
				}
				if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor))
				{
					ConstructorInfo constructor = typeof(Guid).GetConstructor(new Type[]
					{
						typeof(string)
					});
					return new System.ComponentModel.Design.Serialization.InstanceDescriptor(constructor, new object[]
					{
						guid.ToString("D")
					});
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
