﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000ED RID: 237
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DefaultBindingPropertyAttribute : Attribute
	{
		// Token: 0x060009CE RID: 2510 RVA: 0x0001C6DC File Offset: 0x0001A8DC
		public DefaultBindingPropertyAttribute()
		{
		}

		// Token: 0x060009CF RID: 2511 RVA: 0x0001C6E4 File Offset: 0x0001A8E4
		public DefaultBindingPropertyAttribute(string name)
		{
			this.name = name;
		}

		// Token: 0x060009D1 RID: 2513 RVA: 0x0001C700 File Offset: 0x0001A900
		public override bool Equals(object obj)
		{
			DefaultBindingPropertyAttribute defaultBindingPropertyAttribute = obj as DefaultBindingPropertyAttribute;
			return obj != null && this.name == defaultBindingPropertyAttribute.Name;
		}

		// Token: 0x060009D2 RID: 2514 RVA: 0x0001C730 File Offset: 0x0001A930
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x060009D3 RID: 2515 RVA: 0x0001C738 File Offset: 0x0001A938
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x0400029B RID: 667
		public static readonly DefaultBindingPropertyAttribute Default = new DefaultBindingPropertyAttribute();

		// Token: 0x0400029C RID: 668
		private string name;
	}
}
