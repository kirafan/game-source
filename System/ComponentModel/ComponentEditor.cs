﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000DF RID: 223
	public abstract class ComponentEditor
	{
		// Token: 0x06000976 RID: 2422 RVA: 0x0001B760 File Offset: 0x00019960
		public bool EditComponent(object component)
		{
			return this.EditComponent(null, component);
		}

		// Token: 0x06000977 RID: 2423
		public abstract bool EditComponent(ITypeDescriptorContext context, object component);
	}
}
