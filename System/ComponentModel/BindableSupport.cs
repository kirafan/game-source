﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000CF RID: 207
	public enum BindableSupport
	{
		// Token: 0x04000252 RID: 594
		No,
		// Token: 0x04000253 RID: 595
		Yes,
		// Token: 0x04000254 RID: 596
		Default
	}
}
