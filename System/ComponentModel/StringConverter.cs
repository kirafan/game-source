﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x020001A8 RID: 424
	public class StringConverter : TypeConverter
	{
		// Token: 0x06000ED6 RID: 3798 RVA: 0x00026694 File Offset: 0x00024894
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000ED7 RID: 3799 RVA: 0x000266B0 File Offset: 0x000248B0
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value == null)
			{
				return string.Empty;
			}
			if (value is string)
			{
				return (string)value;
			}
			return base.ConvertFrom(context, culture, value);
		}
	}
}
