﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000156 RID: 342
	public interface ICustomTypeDescriptor
	{
		// Token: 0x06000C7C RID: 3196
		AttributeCollection GetAttributes();

		// Token: 0x06000C7D RID: 3197
		string GetClassName();

		// Token: 0x06000C7E RID: 3198
		string GetComponentName();

		// Token: 0x06000C7F RID: 3199
		TypeConverter GetConverter();

		// Token: 0x06000C80 RID: 3200
		EventDescriptor GetDefaultEvent();

		// Token: 0x06000C81 RID: 3201
		PropertyDescriptor GetDefaultProperty();

		// Token: 0x06000C82 RID: 3202
		object GetEditor(Type editorBaseType);

		// Token: 0x06000C83 RID: 3203
		EventDescriptorCollection GetEvents();

		// Token: 0x06000C84 RID: 3204
		EventDescriptorCollection GetEvents(Attribute[] arr);

		// Token: 0x06000C85 RID: 3205
		PropertyDescriptorCollection GetProperties();

		// Token: 0x06000C86 RID: 3206
		PropertyDescriptorCollection GetProperties(Attribute[] arr);

		// Token: 0x06000C87 RID: 3207
		object GetPropertyOwner(PropertyDescriptor pd);
	}
}
