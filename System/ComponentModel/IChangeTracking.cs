﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000152 RID: 338
	public interface IChangeTracking
	{
		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000C66 RID: 3174
		bool IsChanged { get; }

		// Token: 0x06000C67 RID: 3175
		void AcceptChanges();
	}
}
