﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000EE RID: 238
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DefaultEventAttribute : Attribute
	{
		// Token: 0x060009D4 RID: 2516 RVA: 0x0001C740 File Offset: 0x0001A940
		public DefaultEventAttribute(string name)
		{
			this.eventName = name;
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x060009D6 RID: 2518 RVA: 0x0001C760 File Offset: 0x0001A960
		public string Name
		{
			get
			{
				return this.eventName;
			}
		}

		// Token: 0x060009D7 RID: 2519 RVA: 0x0001C768 File Offset: 0x0001A968
		public override bool Equals(object o)
		{
			return o is DefaultEventAttribute && ((DefaultEventAttribute)o).eventName == this.eventName;
		}

		// Token: 0x060009D8 RID: 2520 RVA: 0x0001C790 File Offset: 0x0001A990
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x0400029D RID: 669
		private string eventName;

		// Token: 0x0400029E RID: 670
		public static readonly DefaultEventAttribute Default = new DefaultEventAttribute(null);
	}
}
