﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000508 RID: 1288
	// (Invoke) Token: 0x06002CCC RID: 11468
	public delegate void RunWorkerCompletedEventHandler(object sender, RunWorkerCompletedEventArgs e);
}
