﻿using System;
using System.Collections;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x020000E4 RID: 228
	public class CultureInfoConverter : TypeConverter
	{
		// Token: 0x06000994 RID: 2452 RVA: 0x0001BCC4 File Offset: 0x00019EC4
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000995 RID: 2453 RVA: 0x0001BCE0 File Offset: 0x00019EE0
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x0001BD1C File Offset: 0x00019F1C
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			string text = value as string;
			if (text == null)
			{
				return base.ConvertFrom(context, culture, value);
			}
			if (string.Compare(text, "(Default)", false) == 0)
			{
				return CultureInfo.InvariantCulture;
			}
			try
			{
				return new CultureInfo(text);
			}
			catch
			{
				foreach (CultureInfo cultureInfo in CultureInfo.GetCultures(CultureTypes.AllCultures))
				{
					if (string.Compare(cultureInfo.DisplayName, 0, text, 0, text.Length, true) == 0)
					{
						return cultureInfo;
					}
				}
			}
			throw new ArgumentException(string.Format("Culture {0} cannot be converted to a CultureInfo or is not available in this environment.", value));
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x0001BDE0 File Offset: 0x00019FE0
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				if (value == null || !(value is CultureInfo))
				{
					return "(Default)";
				}
				if (value == CultureInfo.InvariantCulture)
				{
					return "(Default)";
				}
				return ((CultureInfo)value).DisplayName;
			}
			else
			{
				if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor) && value is CultureInfo)
				{
					CultureInfo cultureInfo = (CultureInfo)value;
					ConstructorInfo constructor = typeof(CultureInfo).GetConstructor(new Type[]
					{
						typeof(int)
					});
					return new System.ComponentModel.Design.Serialization.InstanceDescriptor(constructor, new object[]
					{
						cultureInfo.LCID
					});
				}
				return base.ConvertTo(context, culture, value, destinationType);
			}
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x0001BEA0 File Offset: 0x0001A0A0
		public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			if (this._standardValues == null)
			{
				CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.AllCultures);
				Array.Sort(cultures, new CultureInfoConverter.CultureInfoComparer());
				CultureInfo[] array = new CultureInfo[cultures.Length + 1];
				array[0] = CultureInfo.InvariantCulture;
				Array.Copy(cultures, 0, array, 1, cultures.Length);
				this._standardValues = new TypeConverter.StandardValuesCollection(array);
			}
			return this._standardValues;
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x0001BEFC File Offset: 0x0001A0FC
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x0001BF00 File Offset: 0x0001A100
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x04000289 RID: 649
		private TypeConverter.StandardValuesCollection _standardValues;

		// Token: 0x020000E5 RID: 229
		private class CultureInfoComparer : IComparer
		{
			// Token: 0x0600099C RID: 2460 RVA: 0x0001BF0C File Offset: 0x0001A10C
			public int Compare(object first, object second)
			{
				if (first == null)
				{
					if (second == null)
					{
						return 0;
					}
					return -1;
				}
				else
				{
					if (second == null)
					{
						return 1;
					}
					return string.Compare(((CultureInfo)first).DisplayName, ((CultureInfo)second).DisplayName, false, CultureInfo.CurrentCulture);
				}
			}
		}
	}
}
