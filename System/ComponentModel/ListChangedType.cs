﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200017E RID: 382
	public enum ListChangedType
	{
		// Token: 0x04000395 RID: 917
		Reset,
		// Token: 0x04000396 RID: 918
		ItemAdded,
		// Token: 0x04000397 RID: 919
		ItemDeleted,
		// Token: 0x04000398 RID: 920
		ItemMoved,
		// Token: 0x04000399 RID: 921
		ItemChanged,
		// Token: 0x0400039A RID: 922
		PropertyDescriptorAdded,
		// Token: 0x0400039B RID: 923
		PropertyDescriptorDeleted,
		// Token: 0x0400039C RID: 924
		PropertyDescriptorChanged
	}
}
