﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000DD RID: 221
	public class ComponentConverter : ReferenceConverter
	{
		// Token: 0x06000964 RID: 2404 RVA: 0x0001B574 File Offset: 0x00019774
		public ComponentConverter(Type type) : base(type)
		{
		}

		// Token: 0x06000965 RID: 2405 RVA: 0x0001B580 File Offset: 0x00019780
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(value, attributes);
		}

		// Token: 0x06000966 RID: 2406 RVA: 0x0001B58C File Offset: 0x0001978C
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
