﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000502 RID: 1282
	// (Invoke) Token: 0x06002CB4 RID: 11444
	public delegate void DoWorkEventHandler(object sender, DoWorkEventArgs e);
}
