﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x02000196 RID: 406
	public class PropertyDescriptorCollection : IDictionary, IList, ICollection, IEnumerable
	{
		// Token: 0x06000E1F RID: 3615 RVA: 0x000245B8 File Offset: 0x000227B8
		public PropertyDescriptorCollection(PropertyDescriptor[] properties)
		{
			this.properties = new ArrayList();
			if (properties == null)
			{
				return;
			}
			this.properties.AddRange(properties);
		}

		// Token: 0x06000E20 RID: 3616 RVA: 0x000245EC File Offset: 0x000227EC
		public PropertyDescriptorCollection(PropertyDescriptor[] properties, bool readOnly) : this(properties)
		{
			this.readOnly = readOnly;
		}

		// Token: 0x06000E21 RID: 3617 RVA: 0x000245FC File Offset: 0x000227FC
		private PropertyDescriptorCollection()
		{
		}

		// Token: 0x06000E23 RID: 3619 RVA: 0x00024614 File Offset: 0x00022814
		int IList.Add(object value)
		{
			return this.Add((PropertyDescriptor)value);
		}

		// Token: 0x06000E24 RID: 3620 RVA: 0x00024624 File Offset: 0x00022824
		void IDictionary.Add(object key, object value)
		{
			if (!(value is PropertyDescriptor))
			{
				throw new ArgumentException("value");
			}
			this.Add((PropertyDescriptor)value);
		}

		// Token: 0x06000E25 RID: 3621 RVA: 0x0002464C File Offset: 0x0002284C
		void IList.Clear()
		{
			this.Clear();
		}

		// Token: 0x06000E26 RID: 3622 RVA: 0x00024654 File Offset: 0x00022854
		void IDictionary.Clear()
		{
			this.Clear();
		}

		// Token: 0x06000E27 RID: 3623 RVA: 0x0002465C File Offset: 0x0002285C
		bool IList.Contains(object value)
		{
			return this.Contains((PropertyDescriptor)value);
		}

		// Token: 0x06000E28 RID: 3624 RVA: 0x0002466C File Offset: 0x0002286C
		bool IDictionary.Contains(object value)
		{
			return this.Contains((PropertyDescriptor)value);
		}

		// Token: 0x06000E29 RID: 3625 RVA: 0x0002467C File Offset: 0x0002287C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000E2A RID: 3626 RVA: 0x00024684 File Offset: 0x00022884
		[MonoTODO]
		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000E2B RID: 3627 RVA: 0x0002468C File Offset: 0x0002288C
		int IList.IndexOf(object value)
		{
			return this.IndexOf((PropertyDescriptor)value);
		}

		// Token: 0x06000E2C RID: 3628 RVA: 0x0002469C File Offset: 0x0002289C
		void IList.Insert(int index, object value)
		{
			this.Insert(index, (PropertyDescriptor)value);
		}

		// Token: 0x06000E2D RID: 3629 RVA: 0x000246AC File Offset: 0x000228AC
		void IDictionary.Remove(object value)
		{
			this.Remove((PropertyDescriptor)value);
		}

		// Token: 0x06000E2E RID: 3630 RVA: 0x000246BC File Offset: 0x000228BC
		void IList.Remove(object value)
		{
			this.Remove((PropertyDescriptor)value);
		}

		// Token: 0x06000E2F RID: 3631 RVA: 0x000246CC File Offset: 0x000228CC
		void IList.RemoveAt(int index)
		{
			this.RemoveAt(index);
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06000E30 RID: 3632 RVA: 0x000246D8 File Offset: 0x000228D8
		bool IDictionary.IsFixedSize
		{
			get
			{
				return ((IList)this).IsFixedSize;
			}
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06000E31 RID: 3633 RVA: 0x000246E0 File Offset: 0x000228E0
		bool IList.IsFixedSize
		{
			get
			{
				return this.readOnly;
			}
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06000E32 RID: 3634 RVA: 0x000246E8 File Offset: 0x000228E8
		bool IDictionary.IsReadOnly
		{
			get
			{
				return ((IList)this).IsReadOnly;
			}
		}

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06000E33 RID: 3635 RVA: 0x000246F0 File Offset: 0x000228F0
		bool IList.IsReadOnly
		{
			get
			{
				return this.readOnly;
			}
		}

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06000E34 RID: 3636 RVA: 0x000246F8 File Offset: 0x000228F8
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06000E35 RID: 3637 RVA: 0x000246FC File Offset: 0x000228FC
		int ICollection.Count
		{
			get
			{
				return this.Count;
			}
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06000E36 RID: 3638 RVA: 0x00024704 File Offset: 0x00022904
		object ICollection.SyncRoot
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06000E37 RID: 3639 RVA: 0x00024708 File Offset: 0x00022908
		ICollection IDictionary.Keys
		{
			get
			{
				string[] array = new string[this.properties.Count];
				int num = 0;
				foreach (object obj in this.properties)
				{
					PropertyDescriptor propertyDescriptor = (PropertyDescriptor)obj;
					array[num++] = propertyDescriptor.Name;
				}
				return array;
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06000E38 RID: 3640 RVA: 0x00024794 File Offset: 0x00022994
		ICollection IDictionary.Values
		{
			get
			{
				return (ICollection)this.properties.Clone();
			}
		}

		// Token: 0x1700034B RID: 843
		object IDictionary.this[object key]
		{
			get
			{
				if (!(key is string))
				{
					return null;
				}
				return this[(string)key];
			}
			set
			{
				if (this.readOnly)
				{
					throw new NotSupportedException();
				}
				if (!(key is string) || !(value is PropertyDescriptor))
				{
					throw new ArgumentException();
				}
				int num = this.properties.IndexOf(value);
				if (num == -1)
				{
					this.Add((PropertyDescriptor)value);
				}
				else
				{
					this.properties[num] = value;
				}
			}
		}

		// Token: 0x1700034C RID: 844
		object IList.this[int index]
		{
			get
			{
				return this.properties[index];
			}
			set
			{
				if (this.readOnly)
				{
					throw new NotSupportedException();
				}
				this.properties[index] = value;
			}
		}

		// Token: 0x06000E3D RID: 3645 RVA: 0x00024864 File Offset: 0x00022A64
		public int Add(PropertyDescriptor value)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.properties.Add(value);
			return this.properties.Count - 1;
		}

		// Token: 0x06000E3E RID: 3646 RVA: 0x00024894 File Offset: 0x00022A94
		public void Clear()
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.properties.Clear();
		}

		// Token: 0x06000E3F RID: 3647 RVA: 0x000248B4 File Offset: 0x00022AB4
		public bool Contains(PropertyDescriptor value)
		{
			return this.properties.Contains(value);
		}

		// Token: 0x06000E40 RID: 3648 RVA: 0x000248C4 File Offset: 0x00022AC4
		public void CopyTo(Array array, int index)
		{
			this.properties.CopyTo(array, index);
		}

		// Token: 0x06000E41 RID: 3649 RVA: 0x000248D4 File Offset: 0x00022AD4
		public virtual PropertyDescriptor Find(string name, bool ignoreCase)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			for (int i = 0; i < this.properties.Count; i++)
			{
				PropertyDescriptor propertyDescriptor = (PropertyDescriptor)this.properties[i];
				if (ignoreCase)
				{
					if (string.Compare(name, propertyDescriptor.Name, StringComparison.OrdinalIgnoreCase) == 0)
					{
						return propertyDescriptor;
					}
				}
				else if (string.Compare(name, propertyDescriptor.Name, StringComparison.Ordinal) == 0)
				{
					return propertyDescriptor;
				}
			}
			return null;
		}

		// Token: 0x06000E42 RID: 3650 RVA: 0x00024954 File Offset: 0x00022B54
		public virtual IEnumerator GetEnumerator()
		{
			return this.properties.GetEnumerator();
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x00024964 File Offset: 0x00022B64
		public int IndexOf(PropertyDescriptor value)
		{
			return this.properties.IndexOf(value);
		}

		// Token: 0x06000E44 RID: 3652 RVA: 0x00024974 File Offset: 0x00022B74
		public void Insert(int index, PropertyDescriptor value)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.properties.Insert(index, value);
		}

		// Token: 0x06000E45 RID: 3653 RVA: 0x00024994 File Offset: 0x00022B94
		public void Remove(PropertyDescriptor value)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.properties.Remove(value);
		}

		// Token: 0x06000E46 RID: 3654 RVA: 0x000249B4 File Offset: 0x00022BB4
		public void RemoveAt(int index)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.properties.RemoveAt(index);
		}

		// Token: 0x06000E47 RID: 3655 RVA: 0x000249D4 File Offset: 0x00022BD4
		private PropertyDescriptorCollection CloneCollection()
		{
			return new PropertyDescriptorCollection
			{
				properties = (ArrayList)this.properties.Clone()
			};
		}

		// Token: 0x06000E48 RID: 3656 RVA: 0x00024A00 File Offset: 0x00022C00
		public virtual PropertyDescriptorCollection Sort()
		{
			PropertyDescriptorCollection propertyDescriptorCollection = this.CloneCollection();
			propertyDescriptorCollection.InternalSort(null);
			return propertyDescriptorCollection;
		}

		// Token: 0x06000E49 RID: 3657 RVA: 0x00024A1C File Offset: 0x00022C1C
		public virtual PropertyDescriptorCollection Sort(IComparer comparer)
		{
			PropertyDescriptorCollection propertyDescriptorCollection = this.CloneCollection();
			propertyDescriptorCollection.InternalSort(comparer);
			return propertyDescriptorCollection;
		}

		// Token: 0x06000E4A RID: 3658 RVA: 0x00024A38 File Offset: 0x00022C38
		public virtual PropertyDescriptorCollection Sort(string[] order)
		{
			PropertyDescriptorCollection propertyDescriptorCollection = this.CloneCollection();
			propertyDescriptorCollection.InternalSort(order);
			return propertyDescriptorCollection;
		}

		// Token: 0x06000E4B RID: 3659 RVA: 0x00024A54 File Offset: 0x00022C54
		public virtual PropertyDescriptorCollection Sort(string[] order, IComparer comparer)
		{
			PropertyDescriptorCollection propertyDescriptorCollection = this.CloneCollection();
			if (order != null)
			{
				ArrayList arrayList = propertyDescriptorCollection.ExtractItems(order);
				propertyDescriptorCollection.InternalSort(comparer);
				arrayList.AddRange(propertyDescriptorCollection.properties);
				propertyDescriptorCollection.properties = arrayList;
			}
			else
			{
				propertyDescriptorCollection.InternalSort(comparer);
			}
			return propertyDescriptorCollection;
		}

		// Token: 0x06000E4C RID: 3660 RVA: 0x00024AA0 File Offset: 0x00022CA0
		protected void InternalSort(IComparer ic)
		{
			if (ic == null)
			{
				ic = MemberDescriptor.DefaultComparer;
			}
			this.properties.Sort(ic);
		}

		// Token: 0x06000E4D RID: 3661 RVA: 0x00024ABC File Offset: 0x00022CBC
		protected void InternalSort(string[] order)
		{
			if (order != null)
			{
				ArrayList arrayList = this.ExtractItems(order);
				this.InternalSort(null);
				arrayList.AddRange(this.properties);
				this.properties = arrayList;
			}
			else
			{
				this.InternalSort(null);
			}
		}

		// Token: 0x06000E4E RID: 3662 RVA: 0x00024B00 File Offset: 0x00022D00
		private ArrayList ExtractItems(string[] names)
		{
			ArrayList arrayList = new ArrayList(this.properties.Count);
			object[] array = new object[names.Length];
			for (int i = 0; i < this.properties.Count; i++)
			{
				PropertyDescriptor propertyDescriptor = (PropertyDescriptor)this.properties[i];
				int num = Array.IndexOf<string>(names, propertyDescriptor.Name);
				if (num != -1)
				{
					array[num] = propertyDescriptor;
					this.properties.RemoveAt(i);
					i--;
				}
			}
			foreach (object obj in array)
			{
				if (obj != null)
				{
					arrayList.Add(obj);
				}
			}
			return arrayList;
		}

		// Token: 0x06000E4F RID: 3663 RVA: 0x00024BB4 File Offset: 0x00022DB4
		internal PropertyDescriptorCollection Filter(Attribute[] attributes)
		{
			ArrayList arrayList = new ArrayList();
			foreach (object obj in this.properties)
			{
				PropertyDescriptor propertyDescriptor = (PropertyDescriptor)obj;
				if (propertyDescriptor.Attributes.Contains(attributes))
				{
					arrayList.Add(propertyDescriptor);
				}
			}
			PropertyDescriptor[] array = new PropertyDescriptor[arrayList.Count];
			arrayList.CopyTo(array);
			return new PropertyDescriptorCollection(array, true);
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06000E50 RID: 3664 RVA: 0x00024C58 File Offset: 0x00022E58
		public int Count
		{
			get
			{
				return this.properties.Count;
			}
		}

		// Token: 0x1700034E RID: 846
		public virtual PropertyDescriptor this[string s]
		{
			get
			{
				return this.Find(s, false);
			}
		}

		// Token: 0x1700034F RID: 847
		public virtual PropertyDescriptor this[int index]
		{
			get
			{
				return (PropertyDescriptor)this.properties[index];
			}
		}

		// Token: 0x04000402 RID: 1026
		public static readonly PropertyDescriptorCollection Empty = new PropertyDescriptorCollection(null, true);

		// Token: 0x04000403 RID: 1027
		private ArrayList properties;

		// Token: 0x04000404 RID: 1028
		private bool readOnly;
	}
}
