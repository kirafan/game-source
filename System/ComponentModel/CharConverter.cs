﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x020000D7 RID: 215
	public class CharConverter : TypeConverter
	{
		// Token: 0x0600094E RID: 2382 RVA: 0x0001B288 File Offset: 0x00019488
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x0600094F RID: 2383 RVA: 0x0001B2A4 File Offset: 0x000194A4
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			string text = value as string;
			if (text == null)
			{
				return base.ConvertFrom(context, culture, value);
			}
			if (text.Length > 1)
			{
				text = text.Trim();
			}
			if (text.Length > 1)
			{
				throw new FormatException(string.Format("String {0} is not a valid Char: it has to be less than or equal to one char long.", text));
			}
			if (text.Length == 0)
			{
				return '\0';
			}
			return text[0];
		}

		// Token: 0x06000950 RID: 2384 RVA: 0x0001B318 File Offset: 0x00019518
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType != typeof(string) || value == null || !(value is char))
			{
				return base.ConvertTo(context, culture, value, destinationType);
			}
			char c = (char)value;
			if (c == '\0')
			{
				return string.Empty;
			}
			return new string(c, 1);
		}
	}
}
