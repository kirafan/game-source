﻿using System;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x02000173 RID: 371
	public class LicenseContext : IServiceProvider
	{
		// Token: 0x06000CE0 RID: 3296 RVA: 0x00020570 File Offset: 0x0001E770
		public virtual string GetSavedLicenseKey(Type type, Assembly resourceAssembly)
		{
			return null;
		}

		// Token: 0x06000CE1 RID: 3297 RVA: 0x00020574 File Offset: 0x0001E774
		public virtual object GetService(Type type)
		{
			return null;
		}

		// Token: 0x06000CE2 RID: 3298 RVA: 0x00020578 File Offset: 0x0001E778
		public virtual void SetSavedLicenseKey(Type type, string key)
		{
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000CE3 RID: 3299 RVA: 0x0002057C File Offset: 0x0001E77C
		public virtual LicenseUsageMode UsageMode
		{
			get
			{
				return LicenseUsageMode.Runtime;
			}
		}
	}
}
