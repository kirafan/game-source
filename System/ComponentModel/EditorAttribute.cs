﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000142 RID: 322
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
	public sealed class EditorAttribute : Attribute
	{
		// Token: 0x06000BDE RID: 3038 RVA: 0x0001F088 File Offset: 0x0001D288
		public EditorAttribute()
		{
			this.name = string.Empty;
		}

		// Token: 0x06000BDF RID: 3039 RVA: 0x0001F09C File Offset: 0x0001D29C
		public EditorAttribute(string typeName, string baseTypeName)
		{
			this.name = typeName;
			this.basename = baseTypeName;
		}

		// Token: 0x06000BE0 RID: 3040 RVA: 0x0001F0B4 File Offset: 0x0001D2B4
		public EditorAttribute(string typeName, Type baseType) : this(typeName, baseType.AssemblyQualifiedName)
		{
		}

		// Token: 0x06000BE1 RID: 3041 RVA: 0x0001F0C4 File Offset: 0x0001D2C4
		public EditorAttribute(Type type, Type baseType) : this(type.AssemblyQualifiedName, baseType.AssemblyQualifiedName)
		{
		}

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06000BE2 RID: 3042 RVA: 0x0001F0D8 File Offset: 0x0001D2D8
		public string EditorBaseTypeName
		{
			get
			{
				return this.basename;
			}
		}

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06000BE3 RID: 3043 RVA: 0x0001F0E0 File Offset: 0x0001D2E0
		public string EditorTypeName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06000BE4 RID: 3044 RVA: 0x0001F0E8 File Offset: 0x0001D2E8
		public override object TypeId
		{
			get
			{
				return base.GetType();
			}
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x0001F0F0 File Offset: 0x0001D2F0
		public override bool Equals(object obj)
		{
			return obj is EditorAttribute && ((EditorAttribute)obj).EditorBaseTypeName.Equals(this.basename) && ((EditorAttribute)obj).EditorTypeName.Equals(this.name);
		}

		// Token: 0x06000BE6 RID: 3046 RVA: 0x0001F140 File Offset: 0x0001D340
		public override int GetHashCode()
		{
			return (this.name + this.basename).GetHashCode();
		}

		// Token: 0x0400035E RID: 862
		private string name;

		// Token: 0x0400035F RID: 863
		private string basename;
	}
}
