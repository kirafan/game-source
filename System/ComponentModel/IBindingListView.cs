﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x02000150 RID: 336
	public interface IBindingListView : IList, ICollection, IEnumerable, IBindingList
	{
		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000C5D RID: 3165
		// (set) Token: 0x06000C5E RID: 3166
		string Filter { get; set; }

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000C5F RID: 3167
		ListSortDescriptionCollection SortDescriptions { get; }

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000C60 RID: 3168
		bool SupportsAdvancedSorting { get; }

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000C61 RID: 3169
		bool SupportsFiltering { get; }

		// Token: 0x06000C62 RID: 3170
		void ApplySort(ListSortDescriptionCollection sorts);

		// Token: 0x06000C63 RID: 3171
		void RemoveFilter();
	}
}
