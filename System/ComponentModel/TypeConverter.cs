﻿using System;
using System.Collections;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x020001AF RID: 431
	[ComVisible(true)]
	public class TypeConverter
	{
		// Token: 0x06000EFA RID: 3834 RVA: 0x00026BC8 File Offset: 0x00024DC8
		public bool CanConvertFrom(Type sourceType)
		{
			return this.CanConvertFrom(null, sourceType);
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x00026BD4 File Offset: 0x00024DD4
		public virtual bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor);
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x00026BEC File Offset: 0x00024DEC
		public bool CanConvertTo(Type destinationType)
		{
			return this.CanConvertTo(null, destinationType);
		}

		// Token: 0x06000EFD RID: 3837 RVA: 0x00026BF8 File Offset: 0x00024DF8
		public virtual bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string);
		}

		// Token: 0x06000EFE RID: 3838 RVA: 0x00026C08 File Offset: 0x00024E08
		public object ConvertFrom(object o)
		{
			return this.ConvertFrom(null, CultureInfo.CurrentCulture, o);
		}

		// Token: 0x06000EFF RID: 3839 RVA: 0x00026C18 File Offset: 0x00024E18
		public virtual object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is System.ComponentModel.Design.Serialization.InstanceDescriptor)
			{
				return ((System.ComponentModel.Design.Serialization.InstanceDescriptor)value).Invoke();
			}
			return this.GetConvertFromException(value);
		}

		// Token: 0x06000F00 RID: 3840 RVA: 0x00026C38 File Offset: 0x00024E38
		public object ConvertFromInvariantString(string text)
		{
			return this.ConvertFromInvariantString(null, text);
		}

		// Token: 0x06000F01 RID: 3841 RVA: 0x00026C44 File Offset: 0x00024E44
		public object ConvertFromInvariantString(ITypeDescriptorContext context, string text)
		{
			return this.ConvertFromString(context, CultureInfo.InvariantCulture, text);
		}

		// Token: 0x06000F02 RID: 3842 RVA: 0x00026C54 File Offset: 0x00024E54
		public object ConvertFromString(string text)
		{
			return this.ConvertFrom(text);
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x00026C60 File Offset: 0x00024E60
		public object ConvertFromString(ITypeDescriptorContext context, string text)
		{
			return this.ConvertFromString(context, CultureInfo.CurrentCulture, text);
		}

		// Token: 0x06000F04 RID: 3844 RVA: 0x00026C70 File Offset: 0x00024E70
		public object ConvertFromString(ITypeDescriptorContext context, CultureInfo culture, string text)
		{
			return this.ConvertFrom(context, culture, text);
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x00026C7C File Offset: 0x00024E7C
		public object ConvertTo(object value, Type destinationType)
		{
			return this.ConvertTo(null, null, value, destinationType);
		}

		// Token: 0x06000F06 RID: 3846 RVA: 0x00026C88 File Offset: 0x00024E88
		public virtual object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}
			if (destinationType != typeof(string))
			{
				return this.GetConvertToException(value, destinationType);
			}
			if (value != null)
			{
				return value.ToString();
			}
			return string.Empty;
		}

		// Token: 0x06000F07 RID: 3847 RVA: 0x00026CD4 File Offset: 0x00024ED4
		public string ConvertToInvariantString(object value)
		{
			return this.ConvertToInvariantString(null, value);
		}

		// Token: 0x06000F08 RID: 3848 RVA: 0x00026CE0 File Offset: 0x00024EE0
		public string ConvertToInvariantString(ITypeDescriptorContext context, object value)
		{
			return (string)this.ConvertTo(context, CultureInfo.InvariantCulture, value, typeof(string));
		}

		// Token: 0x06000F09 RID: 3849 RVA: 0x00026D00 File Offset: 0x00024F00
		public string ConvertToString(object value)
		{
			return (string)this.ConvertTo(null, CultureInfo.CurrentCulture, value, typeof(string));
		}

		// Token: 0x06000F0A RID: 3850 RVA: 0x00026D20 File Offset: 0x00024F20
		public string ConvertToString(ITypeDescriptorContext context, object value)
		{
			return (string)this.ConvertTo(context, CultureInfo.CurrentCulture, value, typeof(string));
		}

		// Token: 0x06000F0B RID: 3851 RVA: 0x00026D40 File Offset: 0x00024F40
		public string ConvertToString(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			return (string)this.ConvertTo(context, culture, value, typeof(string));
		}

		// Token: 0x06000F0C RID: 3852 RVA: 0x00026D5C File Offset: 0x00024F5C
		protected Exception GetConvertFromException(object value)
		{
			string text;
			if (value == null)
			{
				text = "(null)";
			}
			else
			{
				text = value.GetType().FullName;
			}
			throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "{0} cannot convert from {1}.", new object[]
			{
				base.GetType().Name,
				text
			}));
		}

		// Token: 0x06000F0D RID: 3853 RVA: 0x00026DB4 File Offset: 0x00024FB4
		protected Exception GetConvertToException(object value, Type destinationType)
		{
			string text;
			if (value == null)
			{
				text = "(null)";
			}
			else
			{
				text = value.GetType().FullName;
			}
			throw new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "'{0}' is unable to convert '{1}' to '{2}'.", new object[]
			{
				base.GetType().Name,
				text,
				destinationType.FullName
			}));
		}

		// Token: 0x06000F0E RID: 3854 RVA: 0x00026E14 File Offset: 0x00025014
		public object CreateInstance(IDictionary propertyValues)
		{
			return this.CreateInstance(null, propertyValues);
		}

		// Token: 0x06000F0F RID: 3855 RVA: 0x00026E20 File Offset: 0x00025020
		public virtual object CreateInstance(ITypeDescriptorContext context, IDictionary propertyValues)
		{
			return null;
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x00026E24 File Offset: 0x00025024
		public bool GetCreateInstanceSupported()
		{
			return this.GetCreateInstanceSupported(null);
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x00026E30 File Offset: 0x00025030
		public virtual bool GetCreateInstanceSupported(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x00026E34 File Offset: 0x00025034
		public PropertyDescriptorCollection GetProperties(object value)
		{
			return this.GetProperties(null, value);
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x00026E40 File Offset: 0x00025040
		public PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value)
		{
			return this.GetProperties(context, value, new Attribute[]
			{
				BrowsableAttribute.Yes
			});
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x00026E58 File Offset: 0x00025058
		public virtual PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return null;
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x00026E5C File Offset: 0x0002505C
		public bool GetPropertiesSupported()
		{
			return this.GetPropertiesSupported(null);
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00026E68 File Offset: 0x00025068
		public virtual bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x00026E6C File Offset: 0x0002506C
		public ICollection GetStandardValues()
		{
			return this.GetStandardValues(null);
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00026E78 File Offset: 0x00025078
		public virtual TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			return null;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x00026E7C File Offset: 0x0002507C
		public bool GetStandardValuesExclusive()
		{
			return this.GetStandardValuesExclusive(null);
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x00026E88 File Offset: 0x00025088
		public virtual bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x00026E8C File Offset: 0x0002508C
		public bool GetStandardValuesSupported()
		{
			return this.GetStandardValuesSupported(null);
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x00026E98 File Offset: 0x00025098
		public virtual bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return false;
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x00026E9C File Offset: 0x0002509C
		public bool IsValid(object value)
		{
			return this.IsValid(null, value);
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x00026EA8 File Offset: 0x000250A8
		public virtual bool IsValid(ITypeDescriptorContext context, object value)
		{
			return true;
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x00026EAC File Offset: 0x000250AC
		protected PropertyDescriptorCollection SortProperties(PropertyDescriptorCollection props, string[] names)
		{
			props.Sort(names);
			return props;
		}

		// Token: 0x020001B0 RID: 432
		public class StandardValuesCollection : ICollection, IEnumerable
		{
			// Token: 0x06000F20 RID: 3872 RVA: 0x00026EB8 File Offset: 0x000250B8
			public StandardValuesCollection(ICollection values)
			{
				this.values = values;
			}

			// Token: 0x06000F21 RID: 3873 RVA: 0x00026EC8 File Offset: 0x000250C8
			void ICollection.CopyTo(Array array, int index)
			{
				this.CopyTo(array, index);
			}

			// Token: 0x06000F22 RID: 3874 RVA: 0x00026ED4 File Offset: 0x000250D4
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x17000374 RID: 884
			// (get) Token: 0x06000F23 RID: 3875 RVA: 0x00026EDC File Offset: 0x000250DC
			bool ICollection.IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17000375 RID: 885
			// (get) Token: 0x06000F24 RID: 3876 RVA: 0x00026EE0 File Offset: 0x000250E0
			object ICollection.SyncRoot
			{
				get
				{
					return null;
				}
			}

			// Token: 0x17000376 RID: 886
			// (get) Token: 0x06000F25 RID: 3877 RVA: 0x00026EE4 File Offset: 0x000250E4
			int ICollection.Count
			{
				get
				{
					return this.Count;
				}
			}

			// Token: 0x06000F26 RID: 3878 RVA: 0x00026EEC File Offset: 0x000250EC
			public void CopyTo(Array array, int index)
			{
				this.values.CopyTo(array, index);
			}

			// Token: 0x06000F27 RID: 3879 RVA: 0x00026EFC File Offset: 0x000250FC
			public IEnumerator GetEnumerator()
			{
				return this.values.GetEnumerator();
			}

			// Token: 0x17000377 RID: 887
			// (get) Token: 0x06000F28 RID: 3880 RVA: 0x00026F0C File Offset: 0x0002510C
			public int Count
			{
				get
				{
					return this.values.Count;
				}
			}

			// Token: 0x17000378 RID: 888
			public object this[int index]
			{
				get
				{
					return ((IList)this.values)[index];
				}
			}

			// Token: 0x04000444 RID: 1092
			private ICollection values;
		}

		// Token: 0x020001B1 RID: 433
		protected abstract class SimplePropertyDescriptor : PropertyDescriptor
		{
			// Token: 0x06000F2A RID: 3882 RVA: 0x00026F30 File Offset: 0x00025130
			public SimplePropertyDescriptor(Type componentType, string name, Type propertyType) : this(componentType, name, propertyType, null)
			{
			}

			// Token: 0x06000F2B RID: 3883 RVA: 0x00026F3C File Offset: 0x0002513C
			public SimplePropertyDescriptor(Type componentType, string name, Type propertyType, Attribute[] attributes) : base(name, attributes)
			{
				this.componentType = componentType;
				this.propertyType = propertyType;
			}

			// Token: 0x17000379 RID: 889
			// (get) Token: 0x06000F2C RID: 3884 RVA: 0x00026F58 File Offset: 0x00025158
			public override Type ComponentType
			{
				get
				{
					return this.componentType;
				}
			}

			// Token: 0x1700037A RID: 890
			// (get) Token: 0x06000F2D RID: 3885 RVA: 0x00026F60 File Offset: 0x00025160
			public override Type PropertyType
			{
				get
				{
					return this.propertyType;
				}
			}

			// Token: 0x1700037B RID: 891
			// (get) Token: 0x06000F2E RID: 3886 RVA: 0x00026F68 File Offset: 0x00025168
			public override bool IsReadOnly
			{
				get
				{
					return this.Attributes.Contains(ReadOnlyAttribute.Yes);
				}
			}

			// Token: 0x06000F2F RID: 3887 RVA: 0x00026F7C File Offset: 0x0002517C
			public override bool ShouldSerializeValue(object component)
			{
				return false;
			}

			// Token: 0x06000F30 RID: 3888 RVA: 0x00026F80 File Offset: 0x00025180
			public override bool CanResetValue(object component)
			{
				DefaultValueAttribute defaultValueAttribute = (DefaultValueAttribute)this.Attributes[typeof(DefaultValueAttribute)];
				return defaultValueAttribute != null && defaultValueAttribute.Value == this.GetValue(component);
			}

			// Token: 0x06000F31 RID: 3889 RVA: 0x00026FC0 File Offset: 0x000251C0
			public override void ResetValue(object component)
			{
				DefaultValueAttribute defaultValueAttribute = (DefaultValueAttribute)this.Attributes[typeof(DefaultValueAttribute)];
				if (defaultValueAttribute != null)
				{
					this.SetValue(component, defaultValueAttribute.Value);
				}
			}

			// Token: 0x04000445 RID: 1093
			private Type componentType;

			// Token: 0x04000446 RID: 1094
			private Type propertyType;
		}
	}
}
