﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000F2 RID: 242
	[AttributeUsage(AttributeTargets.All)]
	public class DescriptionAttribute : Attribute
	{
		// Token: 0x060009FA RID: 2554 RVA: 0x0001CA6C File Offset: 0x0001AC6C
		public DescriptionAttribute()
		{
			this.desc = string.Empty;
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x0001CA80 File Offset: 0x0001AC80
		public DescriptionAttribute(string name)
		{
			this.desc = name;
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x060009FD RID: 2557 RVA: 0x0001CA9C File Offset: 0x0001AC9C
		public virtual string Description
		{
			get
			{
				return this.DescriptionValue;
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x060009FE RID: 2558 RVA: 0x0001CAA4 File Offset: 0x0001ACA4
		// (set) Token: 0x060009FF RID: 2559 RVA: 0x0001CAAC File Offset: 0x0001ACAC
		protected string DescriptionValue
		{
			get
			{
				return this.desc;
			}
			set
			{
				this.desc = value;
			}
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x0001CAB8 File Offset: 0x0001ACB8
		public override bool Equals(object obj)
		{
			return obj is DescriptionAttribute && (obj == this || ((DescriptionAttribute)obj).Description == this.desc);
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x0001CAF4 File Offset: 0x0001ACF4
		public override int GetHashCode()
		{
			return this.desc.GetHashCode();
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x0001CB04 File Offset: 0x0001AD04
		public override bool IsDefaultAttribute()
		{
			return this == DescriptionAttribute.Default;
		}

		// Token: 0x040002A6 RID: 678
		private string desc;

		// Token: 0x040002A7 RID: 679
		public static readonly DescriptionAttribute Default = new DescriptionAttribute();
	}
}
