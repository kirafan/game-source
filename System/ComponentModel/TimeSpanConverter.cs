﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x020001AA RID: 426
	public class TimeSpanConverter : TypeConverter
	{
		// Token: 0x06000EDC RID: 3804 RVA: 0x0002675C File Offset: 0x0002495C
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x00026778 File Offset: 0x00024978
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string) || destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor) || base.CanConvertTo(context, destinationType);
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x000267B4 File Offset: 0x000249B4
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value.GetType() == typeof(string))
			{
				string text = (string)value;
				try
				{
					return TimeSpan.Parse(text);
				}
				catch
				{
					throw new FormatException(text + "is not valid for a TimeSpan.");
				}
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x00026834 File Offset: 0x00024A34
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value is TimeSpan)
			{
				TimeSpan timeSpan = (TimeSpan)value;
				if (destinationType == typeof(string) && value != null)
				{
					return timeSpan.ToString();
				}
				if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor))
				{
					ConstructorInfo constructor = typeof(TimeSpan).GetConstructor(new Type[]
					{
						typeof(long)
					});
					return new System.ComponentModel.Design.Serialization.InstanceDescriptor(constructor, new object[]
					{
						timeSpan.Ticks
					});
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
