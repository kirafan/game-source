﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020004BF RID: 1215
	public class ProgressChangedEventArgs : EventArgs
	{
		// Token: 0x06002BBE RID: 11198 RVA: 0x00098C68 File Offset: 0x00096E68
		public ProgressChangedEventArgs(int progressPercentage, object userState)
		{
			this.progress = progressPercentage;
			this.state = userState;
		}

		// Token: 0x17000BF4 RID: 3060
		// (get) Token: 0x06002BBF RID: 11199 RVA: 0x00098C80 File Offset: 0x00096E80
		public int ProgressPercentage
		{
			get
			{
				return this.progress;
			}
		}

		// Token: 0x17000BF5 RID: 3061
		// (get) Token: 0x06002BC0 RID: 11200 RVA: 0x00098C88 File Offset: 0x00096E88
		public object UserState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x04001B91 RID: 7057
		private int progress;

		// Token: 0x04001B92 RID: 7058
		private object state;
	}
}
