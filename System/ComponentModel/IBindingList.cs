﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x0200014F RID: 335
	public interface IBindingList : IList, ICollection, IEnumerable
	{
		// Token: 0x14000034 RID: 52
		// (add) Token: 0x06000C4C RID: 3148
		// (remove) Token: 0x06000C4D RID: 3149
		event ListChangedEventHandler ListChanged;

		// Token: 0x06000C4E RID: 3150
		void AddIndex(PropertyDescriptor property);

		// Token: 0x06000C4F RID: 3151
		object AddNew();

		// Token: 0x06000C50 RID: 3152
		void ApplySort(PropertyDescriptor property, ListSortDirection direction);

		// Token: 0x06000C51 RID: 3153
		int Find(PropertyDescriptor property, object key);

		// Token: 0x06000C52 RID: 3154
		void RemoveIndex(PropertyDescriptor property);

		// Token: 0x06000C53 RID: 3155
		void RemoveSort();

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000C54 RID: 3156
		bool AllowEdit { get; }

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000C55 RID: 3157
		bool AllowNew { get; }

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06000C56 RID: 3158
		bool AllowRemove { get; }

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000C57 RID: 3159
		bool IsSorted { get; }

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000C58 RID: 3160
		ListSortDirection SortDirection { get; }

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000C59 RID: 3161
		PropertyDescriptor SortProperty { get; }

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000C5A RID: 3162
		bool SupportsChangeNotification { get; }

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000C5B RID: 3163
		bool SupportsSearching { get; }

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000C5C RID: 3164
		bool SupportsSorting { get; }
	}
}
