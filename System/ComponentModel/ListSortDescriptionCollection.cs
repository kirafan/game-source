﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x0200017F RID: 383
	public class ListSortDescriptionCollection : IList, ICollection, IEnumerable
	{
		// Token: 0x06000D1D RID: 3357 RVA: 0x00020D08 File Offset: 0x0001EF08
		public ListSortDescriptionCollection()
		{
			this.list = new ArrayList();
		}

		// Token: 0x06000D1E RID: 3358 RVA: 0x00020D1C File Offset: 0x0001EF1C
		public ListSortDescriptionCollection(ListSortDescription[] sorts)
		{
			this.list = new ArrayList();
			foreach (ListSortDescription value in sorts)
			{
				this.list.Add(value);
			}
		}

		// Token: 0x170002F9 RID: 761
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				throw new InvalidOperationException("ListSortDescriptorCollection is read only.");
			}
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000D21 RID: 3361 RVA: 0x00020D7C File Offset: 0x0001EF7C
		bool IList.IsFixedSize
		{
			get
			{
				return this.list.IsFixedSize;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000D22 RID: 3362 RVA: 0x00020D8C File Offset: 0x0001EF8C
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.list.IsSynchronized;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000D23 RID: 3363 RVA: 0x00020D9C File Offset: 0x0001EF9C
		object ICollection.SyncRoot
		{
			get
			{
				return this.list.SyncRoot;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000D24 RID: 3364 RVA: 0x00020DAC File Offset: 0x0001EFAC
		bool IList.IsReadOnly
		{
			get
			{
				return this.list.IsReadOnly;
			}
		}

		// Token: 0x06000D25 RID: 3365 RVA: 0x00020DBC File Offset: 0x0001EFBC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06000D26 RID: 3366 RVA: 0x00020DCC File Offset: 0x0001EFCC
		int IList.Add(object value)
		{
			return this.list.Add(value);
		}

		// Token: 0x06000D27 RID: 3367 RVA: 0x00020DDC File Offset: 0x0001EFDC
		void IList.Clear()
		{
			this.list.Clear();
		}

		// Token: 0x06000D28 RID: 3368 RVA: 0x00020DEC File Offset: 0x0001EFEC
		void IList.Insert(int index, object value)
		{
			this.list.Insert(index, value);
		}

		// Token: 0x06000D29 RID: 3369 RVA: 0x00020DFC File Offset: 0x0001EFFC
		void IList.Remove(object value)
		{
			this.list.Remove(value);
		}

		// Token: 0x06000D2A RID: 3370 RVA: 0x00020E0C File Offset: 0x0001F00C
		void IList.RemoveAt(int index)
		{
			this.list.RemoveAt(index);
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000D2B RID: 3371 RVA: 0x00020E1C File Offset: 0x0001F01C
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x170002FF RID: 767
		public ListSortDescription this[int index]
		{
			get
			{
				return this.list[index] as ListSortDescription;
			}
			set
			{
				throw new InvalidOperationException("ListSortDescriptorCollection is read only.");
			}
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x00020E4C File Offset: 0x0001F04C
		public bool Contains(object value)
		{
			return this.list.Contains(value);
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x00020E5C File Offset: 0x0001F05C
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x00020E6C File Offset: 0x0001F06C
		public int IndexOf(object value)
		{
			return this.list.IndexOf(value);
		}

		// Token: 0x0400039D RID: 925
		private ArrayList list;
	}
}
