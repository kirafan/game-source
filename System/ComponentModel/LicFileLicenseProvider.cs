﻿using System;
using System.IO;

namespace System.ComponentModel
{
	// Token: 0x0200017A RID: 378
	public class LicFileLicenseProvider : LicenseProvider
	{
		// Token: 0x06000D08 RID: 3336 RVA: 0x00020AB8 File Offset: 0x0001ECB8
		public override License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions)
		{
			try
			{
				if (context == null || context.UsageMode != LicenseUsageMode.Designtime)
				{
					return null;
				}
				string text = Path.GetDirectoryName(type.Assembly.Location);
				text = Path.Combine(text, type.FullName + ".LIC");
				if (!File.Exists(text))
				{
					return null;
				}
				StreamReader streamReader = new StreamReader(text);
				string key = streamReader.ReadLine();
				streamReader.Close();
				if (this.IsKeyValid(key, type))
				{
					return new LicFileLicense(key);
				}
			}
			catch
			{
				if (allowExceptions)
				{
					throw;
				}
			}
			return null;
		}

		// Token: 0x06000D09 RID: 3337 RVA: 0x00020B78 File Offset: 0x0001ED78
		protected virtual string GetKey(Type type)
		{
			return type.FullName + " is a licensed component.";
		}

		// Token: 0x06000D0A RID: 3338 RVA: 0x00020B8C File Offset: 0x0001ED8C
		protected virtual bool IsKeyValid(string key, Type type)
		{
			return key != null && key.Equals(this.GetKey(type));
		}
	}
}
