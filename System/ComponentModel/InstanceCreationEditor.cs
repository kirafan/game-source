﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000165 RID: 357
	public abstract class InstanceCreationEditor
	{
		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000CAE RID: 3246 RVA: 0x00020398 File Offset: 0x0001E598
		public virtual string Text
		{
			get
			{
				return Locale.GetText("(New ...)");
			}
		}

		// Token: 0x06000CAF RID: 3247
		public abstract object CreateInstance(ITypeDescriptorContext context, Type type);
	}
}
