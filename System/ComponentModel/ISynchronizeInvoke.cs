﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000170 RID: 368
	public interface ISynchronizeInvoke
	{
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000CD4 RID: 3284
		bool InvokeRequired { get; }

		// Token: 0x06000CD5 RID: 3285
		IAsyncResult BeginInvoke(Delegate method, object[] args);

		// Token: 0x06000CD6 RID: 3286
		object EndInvoke(IAsyncResult result);

		// Token: 0x06000CD7 RID: 3287
		object Invoke(Delegate method, object[] args);
	}
}
