﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020004F7 RID: 1271
	// (Invoke) Token: 0x06002C88 RID: 11400
	public delegate void CancelEventHandler(object sender, CancelEventArgs e);
}
