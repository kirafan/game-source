﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000197 RID: 407
	[ComVisible(true)]
	public abstract class PropertyDescriptor : MemberDescriptor
	{
		// Token: 0x06000E53 RID: 3667 RVA: 0x00024C88 File Offset: 0x00022E88
		protected PropertyDescriptor(MemberDescriptor reference) : base(reference)
		{
		}

		// Token: 0x06000E54 RID: 3668 RVA: 0x00024C94 File Offset: 0x00022E94
		protected PropertyDescriptor(MemberDescriptor reference, Attribute[] attrs) : base(reference, attrs)
		{
		}

		// Token: 0x06000E55 RID: 3669 RVA: 0x00024CA0 File Offset: 0x00022EA0
		protected PropertyDescriptor(string name, Attribute[] attrs) : base(name, attrs)
		{
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06000E56 RID: 3670
		public abstract Type ComponentType { get; }

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000E57 RID: 3671 RVA: 0x00024CAC File Offset: 0x00022EAC
		public virtual TypeConverter Converter
		{
			get
			{
				if (this.converter == null && this.PropertyType != null)
				{
					TypeConverterAttribute typeConverterAttribute = (TypeConverterAttribute)this.Attributes[typeof(TypeConverterAttribute)];
					if (typeConverterAttribute != null && typeConverterAttribute != TypeConverterAttribute.Default)
					{
						Type typeFromName = this.GetTypeFromName(typeConverterAttribute.ConverterTypeName);
						if (typeFromName != null && typeof(TypeConverter).IsAssignableFrom(typeFromName))
						{
							this.converter = (TypeConverter)this.CreateInstance(typeFromName);
						}
					}
					if (this.converter == null)
					{
						this.converter = TypeDescriptor.GetConverter(this.PropertyType);
					}
				}
				return this.converter;
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000E58 RID: 3672 RVA: 0x00024D58 File Offset: 0x00022F58
		public virtual bool IsLocalizable
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is LocalizableAttribute)
					{
						return ((LocalizableAttribute)attribute).IsLocalizable;
					}
				}
				return false;
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06000E59 RID: 3673
		public abstract bool IsReadOnly { get; }

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06000E5A RID: 3674
		public abstract Type PropertyType { get; }

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06000E5B RID: 3675 RVA: 0x00024D9C File Offset: 0x00022F9C
		public virtual bool SupportsChangeEvents
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000E5C RID: 3676 RVA: 0x00024DA0 File Offset: 0x00022FA0
		public DesignerSerializationVisibility SerializationVisibility
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is DesignerSerializationVisibilityAttribute)
					{
						DesignerSerializationVisibilityAttribute designerSerializationVisibilityAttribute = (DesignerSerializationVisibilityAttribute)attribute;
						return designerSerializationVisibilityAttribute.Visibility;
					}
				}
				return DesignerSerializationVisibility.Visible;
			}
		}

		// Token: 0x06000E5D RID: 3677 RVA: 0x00024DE8 File Offset: 0x00022FE8
		public virtual void AddValueChanged(object component, EventHandler handler)
		{
			if (component == null)
			{
				throw new ArgumentNullException("component");
			}
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			if (this.notifiers == null)
			{
				this.notifiers = new Hashtable();
			}
			EventHandler eventHandler = (EventHandler)this.notifiers[component];
			if (eventHandler != null)
			{
				eventHandler = (EventHandler)Delegate.Combine(eventHandler, handler);
				this.notifiers[component] = eventHandler;
			}
			else
			{
				this.notifiers[component] = handler;
			}
		}

		// Token: 0x06000E5E RID: 3678 RVA: 0x00024E74 File Offset: 0x00023074
		public virtual void RemoveValueChanged(object component, EventHandler handler)
		{
			if (component == null)
			{
				throw new ArgumentNullException("component");
			}
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			if (this.notifiers == null)
			{
				return;
			}
			EventHandler eventHandler = (EventHandler)this.notifiers[component];
			eventHandler = (EventHandler)Delegate.Remove(eventHandler, handler);
			if (eventHandler == null)
			{
				this.notifiers.Remove(component);
			}
			else
			{
				this.notifiers[component] = eventHandler;
			}
		}

		// Token: 0x06000E5F RID: 3679 RVA: 0x00024EF4 File Offset: 0x000230F4
		protected override void FillAttributes(IList attributeList)
		{
			base.FillAttributes(attributeList);
		}

		// Token: 0x06000E60 RID: 3680 RVA: 0x00024F00 File Offset: 0x00023100
		protected override object GetInvocationTarget(Type type, object instance)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			if (instance is CustomTypeDescriptor)
			{
				CustomTypeDescriptor customTypeDescriptor = (CustomTypeDescriptor)instance;
				return customTypeDescriptor.GetPropertyOwner(this);
			}
			return base.GetInvocationTarget(type, instance);
		}

		// Token: 0x06000E61 RID: 3681 RVA: 0x00024F54 File Offset: 0x00023154
		protected internal EventHandler GetValueChangedHandler(object component)
		{
			if (component == null || this.notifiers == null)
			{
				return null;
			}
			return (EventHandler)this.notifiers[component];
		}

		// Token: 0x06000E62 RID: 3682 RVA: 0x00024F88 File Offset: 0x00023188
		protected virtual void OnValueChanged(object component, EventArgs e)
		{
			if (this.notifiers == null)
			{
				return;
			}
			EventHandler eventHandler = (EventHandler)this.notifiers[component];
			if (eventHandler == null)
			{
				return;
			}
			eventHandler(component, e);
		}

		// Token: 0x06000E63 RID: 3683
		public abstract object GetValue(object component);

		// Token: 0x06000E64 RID: 3684
		public abstract void SetValue(object component, object value);

		// Token: 0x06000E65 RID: 3685
		public abstract void ResetValue(object component);

		// Token: 0x06000E66 RID: 3686
		public abstract bool CanResetValue(object component);

		// Token: 0x06000E67 RID: 3687
		public abstract bool ShouldSerializeValue(object component);

		// Token: 0x06000E68 RID: 3688 RVA: 0x00024FC4 File Offset: 0x000231C4
		protected object CreateInstance(Type type)
		{
			if (type == null || this.PropertyType == null)
			{
				return null;
			}
			Type[] array = new Type[]
			{
				typeof(Type)
			};
			ConstructorInfo constructor = type.GetConstructor(array);
			object result;
			if (constructor != null)
			{
				object[] args = new object[]
				{
					this.PropertyType
				};
				result = TypeDescriptor.CreateInstance(null, type, array, args);
			}
			else
			{
				result = TypeDescriptor.CreateInstance(null, type, null, null);
			}
			return result;
		}

		// Token: 0x06000E69 RID: 3689 RVA: 0x00025034 File Offset: 0x00023234
		public override bool Equals(object obj)
		{
			if (!base.Equals(obj))
			{
				return false;
			}
			PropertyDescriptor propertyDescriptor = obj as PropertyDescriptor;
			return propertyDescriptor != null && propertyDescriptor.PropertyType == this.PropertyType;
		}

		// Token: 0x06000E6A RID: 3690 RVA: 0x0002506C File Offset: 0x0002326C
		public PropertyDescriptorCollection GetChildProperties()
		{
			return this.GetChildProperties(null, null);
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x00025078 File Offset: 0x00023278
		public PropertyDescriptorCollection GetChildProperties(object instance)
		{
			return this.GetChildProperties(instance, null);
		}

		// Token: 0x06000E6C RID: 3692 RVA: 0x00025084 File Offset: 0x00023284
		public PropertyDescriptorCollection GetChildProperties(Attribute[] filter)
		{
			return this.GetChildProperties(null, filter);
		}

		// Token: 0x06000E6D RID: 3693 RVA: 0x00025090 File Offset: 0x00023290
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000E6E RID: 3694 RVA: 0x00025098 File Offset: 0x00023298
		public virtual PropertyDescriptorCollection GetChildProperties(object instance, Attribute[] filter)
		{
			return TypeDescriptor.GetProperties(instance, filter);
		}

		// Token: 0x06000E6F RID: 3695 RVA: 0x000250A4 File Offset: 0x000232A4
		public virtual object GetEditor(Type editorBaseType)
		{
			Type type = null;
			Attribute[] attributeArray = this.AttributeArray;
			if (attributeArray != null && attributeArray.Length != 0)
			{
				foreach (Attribute attribute in attributeArray)
				{
					EditorAttribute editorAttribute = attribute as EditorAttribute;
					if (editorAttribute != null)
					{
						type = this.GetTypeFromName(editorAttribute.EditorTypeName);
						if (type != null && type.IsSubclassOf(editorBaseType))
						{
							break;
						}
					}
				}
			}
			object obj = null;
			if (type != null)
			{
				obj = this.CreateInstance(type);
			}
			if (obj == null)
			{
				obj = TypeDescriptor.GetEditor(this.PropertyType, editorBaseType);
			}
			return obj;
		}

		// Token: 0x06000E70 RID: 3696 RVA: 0x0002514C File Offset: 0x0002334C
		protected Type GetTypeFromName(string typeName)
		{
			if (typeName == null || this.ComponentType == null || typeName.Trim().Length == 0)
			{
				return null;
			}
			Type type = Type.GetType(typeName);
			if (type == null)
			{
				int num = typeName.IndexOf(",");
				if (num != -1)
				{
					typeName = typeName.Substring(0, num);
				}
				type = this.ComponentType.Assembly.GetType(typeName);
			}
			return type;
		}

		// Token: 0x04000405 RID: 1029
		private TypeConverter converter;

		// Token: 0x04000406 RID: 1030
		private Hashtable notifiers;
	}
}
