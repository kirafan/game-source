﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000199 RID: 409
	public enum PropertyTabScope
	{
		// Token: 0x0400040A RID: 1034
		Static,
		// Token: 0x0400040B RID: 1035
		Global,
		// Token: 0x0400040C RID: 1036
		Document,
		// Token: 0x0400040D RID: 1037
		Component
	}
}
