﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000155 RID: 341
	[ComVisible(true)]
	public interface IContainer : IDisposable
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000C78 RID: 3192
		ComponentCollection Components { get; }

		// Token: 0x06000C79 RID: 3193
		void Add(IComponent component);

		// Token: 0x06000C7A RID: 3194
		void Add(IComponent component, string name);

		// Token: 0x06000C7B RID: 3195
		void Remove(IComponent component);
	}
}
