﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000172 RID: 370
	public interface ITypedList
	{
		// Token: 0x06000CDD RID: 3293
		PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors);

		// Token: 0x06000CDE RID: 3294
		string GetListName(PropertyDescriptor[] listAccessors);
	}
}
