﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200018C RID: 396
	[AttributeUsage(AttributeTargets.All)]
	public sealed class MergablePropertyAttribute : Attribute
	{
		// Token: 0x06000DDE RID: 3550 RVA: 0x00023E14 File Offset: 0x00022014
		public MergablePropertyAttribute(bool allowMerge)
		{
			this.mergable = allowMerge;
		}

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06000DE0 RID: 3552 RVA: 0x00023E48 File Offset: 0x00022048
		public bool AllowMerge
		{
			get
			{
				return this.mergable;
			}
		}

		// Token: 0x06000DE1 RID: 3553 RVA: 0x00023E50 File Offset: 0x00022050
		public override bool Equals(object obj)
		{
			return obj is MergablePropertyAttribute && (obj == this || ((MergablePropertyAttribute)obj).AllowMerge == this.mergable);
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x00023E7C File Offset: 0x0002207C
		public override int GetHashCode()
		{
			return this.mergable.GetHashCode();
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x00023E8C File Offset: 0x0002208C
		public override bool IsDefaultAttribute()
		{
			return this.mergable == MergablePropertyAttribute.Default.AllowMerge;
		}

		// Token: 0x040003EB RID: 1003
		private bool mergable;

		// Token: 0x040003EC RID: 1004
		public static readonly MergablePropertyAttribute Default = new MergablePropertyAttribute(true);

		// Token: 0x040003ED RID: 1005
		public static readonly MergablePropertyAttribute No = new MergablePropertyAttribute(false);

		// Token: 0x040003EE RID: 1006
		public static readonly MergablePropertyAttribute Yes = new MergablePropertyAttribute(true);
	}
}
