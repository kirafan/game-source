﻿using System;
using System.Runtime.Serialization;

namespace System.ComponentModel
{
	// Token: 0x02000166 RID: 358
	[Serializable]
	public class InvalidAsynchronousStateException : ArgumentException
	{
		// Token: 0x06000CB0 RID: 3248 RVA: 0x000203A4 File Offset: 0x0001E5A4
		public InvalidAsynchronousStateException() : this("Invalid asynchrinous state is occured")
		{
		}

		// Token: 0x06000CB1 RID: 3249 RVA: 0x000203B4 File Offset: 0x0001E5B4
		public InvalidAsynchronousStateException(string message) : this(message, null)
		{
		}

		// Token: 0x06000CB2 RID: 3250 RVA: 0x000203C0 File Offset: 0x0001E5C0
		public InvalidAsynchronousStateException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x000203CC File Offset: 0x0001E5CC
		protected InvalidAsynchronousStateException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
