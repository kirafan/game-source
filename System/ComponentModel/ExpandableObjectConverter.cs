﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200014B RID: 331
	public class ExpandableObjectConverter : TypeConverter
	{
		// Token: 0x06000C39 RID: 3129 RVA: 0x0001FEDC File Offset: 0x0001E0DC
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(value, attributes);
		}

		// Token: 0x06000C3A RID: 3130 RVA: 0x0001FEE8 File Offset: 0x0001E0E8
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
