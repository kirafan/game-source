﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020004F6 RID: 1270
	// (Invoke) Token: 0x06002C84 RID: 11396
	public delegate void AsyncCompletedEventHandler(object sender, AsyncCompletedEventArgs e);
}
