﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000CE RID: 206
	[AttributeUsage(AttributeTargets.All)]
	public sealed class BindableAttribute : Attribute
	{
		// Token: 0x060008E4 RID: 2276 RVA: 0x0001A374 File Offset: 0x00018574
		public BindableAttribute(BindableSupport flags)
		{
			if (flags == BindableSupport.No)
			{
				this.bindable = false;
			}
			if (flags == BindableSupport.Yes || flags == BindableSupport.Default)
			{
				this.bindable = true;
			}
		}

		// Token: 0x060008E5 RID: 2277 RVA: 0x0001A3AC File Offset: 0x000185AC
		public BindableAttribute(bool bindable)
		{
			this.bindable = bindable;
		}

		// Token: 0x060008E6 RID: 2278 RVA: 0x0001A3BC File Offset: 0x000185BC
		public BindableAttribute(bool bindable, BindingDirection direction)
		{
			this.bindable = bindable;
			this.direction = direction;
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x0001A3D4 File Offset: 0x000185D4
		public BindableAttribute(BindableSupport flags, BindingDirection direction) : this(flags)
		{
			this.direction = direction;
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060008E9 RID: 2281 RVA: 0x0001A408 File Offset: 0x00018608
		public BindingDirection Direction
		{
			get
			{
				return this.direction;
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060008EA RID: 2282 RVA: 0x0001A410 File Offset: 0x00018610
		public bool Bindable
		{
			get
			{
				return this.bindable;
			}
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x0001A418 File Offset: 0x00018618
		public override bool Equals(object obj)
		{
			return obj is BindableAttribute && (obj == this || ((BindableAttribute)obj).Bindable == this.bindable);
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x0001A444 File Offset: 0x00018644
		public override int GetHashCode()
		{
			return this.bindable.GetHashCode();
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x0001A454 File Offset: 0x00018654
		public override bool IsDefaultAttribute()
		{
			return this.bindable == BindableAttribute.Default.Bindable;
		}

		// Token: 0x0400024C RID: 588
		private bool bindable;

		// Token: 0x0400024D RID: 589
		private BindingDirection direction;

		// Token: 0x0400024E RID: 590
		public static readonly BindableAttribute No = new BindableAttribute(BindableSupport.No);

		// Token: 0x0400024F RID: 591
		public static readonly BindableAttribute Yes = new BindableAttribute(BindableSupport.Yes);

		// Token: 0x04000250 RID: 592
		public static readonly BindableAttribute Default = new BindableAttribute(BindableSupport.Default);
	}
}
