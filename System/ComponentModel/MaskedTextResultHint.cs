﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000189 RID: 393
	public enum MaskedTextResultHint
	{
		// Token: 0x040003D8 RID: 984
		PositionOutOfRange = -55,
		// Token: 0x040003D9 RID: 985
		NonEditPosition,
		// Token: 0x040003DA RID: 986
		UnavailableEditPosition,
		// Token: 0x040003DB RID: 987
		PromptCharNotAllowed,
		// Token: 0x040003DC RID: 988
		InvalidInput,
		// Token: 0x040003DD RID: 989
		SignedDigitExpected = -5,
		// Token: 0x040003DE RID: 990
		LetterExpected,
		// Token: 0x040003DF RID: 991
		DigitExpected,
		// Token: 0x040003E0 RID: 992
		AlphanumericCharacterExpected,
		// Token: 0x040003E1 RID: 993
		AsciiCharacterExpected,
		// Token: 0x040003E2 RID: 994
		Unknown,
		// Token: 0x040003E3 RID: 995
		CharacterEscaped,
		// Token: 0x040003E4 RID: 996
		NoEffect,
		// Token: 0x040003E5 RID: 997
		SideEffect,
		// Token: 0x040003E6 RID: 998
		Success
	}
}
