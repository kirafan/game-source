﻿using System;
using System.Collections;
using System.ComponentModel.Design;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x0200018A RID: 394
	[ComVisible(true)]
	public abstract class MemberDescriptor
	{
		// Token: 0x06000DC4 RID: 3524 RVA: 0x000239D8 File Offset: 0x00021BD8
		protected MemberDescriptor(string name, Attribute[] attrs)
		{
			this.name = name;
			this.attrs = attrs;
		}

		// Token: 0x06000DC5 RID: 3525 RVA: 0x000239F0 File Offset: 0x00021BF0
		protected MemberDescriptor(MemberDescriptor reference, Attribute[] attrs)
		{
			this.name = reference.name;
			this.attrs = attrs;
		}

		// Token: 0x06000DC6 RID: 3526 RVA: 0x00023A0C File Offset: 0x00021C0C
		protected MemberDescriptor(string name)
		{
			this.name = name;
		}

		// Token: 0x06000DC7 RID: 3527 RVA: 0x00023A1C File Offset: 0x00021C1C
		protected MemberDescriptor(MemberDescriptor reference)
		{
			this.name = reference.name;
			this.attrs = reference.AttributeArray;
		}

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000DC8 RID: 3528 RVA: 0x00023A3C File Offset: 0x00021C3C
		// (set) Token: 0x06000DC9 RID: 3529 RVA: 0x00023AFC File Offset: 0x00021CFC
		protected virtual Attribute[] AttributeArray
		{
			get
			{
				ArrayList arrayList = new ArrayList();
				if (this.attrs != null)
				{
					arrayList.AddRange(this.attrs);
				}
				this.FillAttributes(arrayList);
				Hashtable hashtable = new Hashtable();
				foreach (object obj in arrayList)
				{
					Attribute attribute = (Attribute)obj;
					hashtable[attribute.TypeId] = attribute;
				}
				Attribute[] array = new Attribute[hashtable.Values.Count];
				hashtable.Values.CopyTo(array, 0);
				return array;
			}
			set
			{
				this.attrs = value;
			}
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x00023B08 File Offset: 0x00021D08
		protected virtual void FillAttributes(IList attributeList)
		{
		}

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000DCB RID: 3531 RVA: 0x00023B0C File Offset: 0x00021D0C
		public virtual AttributeCollection Attributes
		{
			get
			{
				if (this.attrCollection == null)
				{
					this.attrCollection = this.CreateAttributeCollection();
				}
				return this.attrCollection;
			}
		}

		// Token: 0x06000DCC RID: 3532 RVA: 0x00023B2C File Offset: 0x00021D2C
		protected virtual AttributeCollection CreateAttributeCollection()
		{
			return new AttributeCollection(this.AttributeArray);
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000DCD RID: 3533 RVA: 0x00023B3C File Offset: 0x00021D3C
		public virtual string Category
		{
			get
			{
				return ((CategoryAttribute)this.Attributes[typeof(CategoryAttribute)]).Category;
			}
		}

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000DCE RID: 3534 RVA: 0x00023B60 File Offset: 0x00021D60
		public virtual string Description
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is DescriptionAttribute)
					{
						return ((DescriptionAttribute)attribute).Description;
					}
				}
				return string.Empty;
			}
		}

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000DCF RID: 3535 RVA: 0x00023BA8 File Offset: 0x00021DA8
		public virtual bool DesignTimeOnly
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is DesignOnlyAttribute)
					{
						return ((DesignOnlyAttribute)attribute).IsDesignOnly;
					}
				}
				return false;
			}
		}

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000DD0 RID: 3536 RVA: 0x00023BEC File Offset: 0x00021DEC
		public virtual string DisplayName
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is DisplayNameAttribute)
					{
						return ((DisplayNameAttribute)attribute).DisplayName;
					}
				}
				return this.name;
			}
		}

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06000DD1 RID: 3537 RVA: 0x00023C38 File Offset: 0x00021E38
		public virtual string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000DD2 RID: 3538 RVA: 0x00023C40 File Offset: 0x00021E40
		public virtual bool IsBrowsable
		{
			get
			{
				foreach (Attribute attribute in this.AttributeArray)
				{
					if (attribute is BrowsableAttribute)
					{
						return ((BrowsableAttribute)attribute).Browsable;
					}
				}
				return true;
			}
		}

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06000DD3 RID: 3539 RVA: 0x00023C84 File Offset: 0x00021E84
		protected virtual int NameHashCode
		{
			get
			{
				return this.name.GetHashCode();
			}
		}

		// Token: 0x06000DD4 RID: 3540 RVA: 0x00023C94 File Offset: 0x00021E94
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000DD5 RID: 3541 RVA: 0x00023C9C File Offset: 0x00021E9C
		public override bool Equals(object obj)
		{
			MemberDescriptor memberDescriptor = obj as MemberDescriptor;
			return memberDescriptor != null && memberDescriptor.name == this.name;
		}

		// Token: 0x06000DD6 RID: 3542 RVA: 0x00023CCC File Offset: 0x00021ECC
		protected static ISite GetSite(object component)
		{
			if (component is Component)
			{
				return ((Component)component).Site;
			}
			return null;
		}

		// Token: 0x06000DD7 RID: 3543 RVA: 0x00023CE8 File Offset: 0x00021EE8
		[Obsolete("Use GetInvocationTarget")]
		protected static object GetInvokee(Type componentClass, object component)
		{
			if (component is IComponent)
			{
				ISite site = ((IComponent)component).Site;
				if (site != null && site.DesignMode)
				{
					System.ComponentModel.Design.IDesignerHost designerHost = site.GetService(typeof(System.ComponentModel.Design.IDesignerHost)) as System.ComponentModel.Design.IDesignerHost;
					if (designerHost != null)
					{
						System.ComponentModel.Design.IDesigner designer = designerHost.GetDesigner((IComponent)component);
						if (designer != null && componentClass.IsInstanceOfType(designer))
						{
							component = designer;
						}
					}
				}
			}
			return component;
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x00023D5C File Offset: 0x00021F5C
		protected virtual object GetInvocationTarget(Type type, object instance)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return MemberDescriptor.GetInvokee(type, instance);
		}

		// Token: 0x06000DD9 RID: 3545 RVA: 0x00023D88 File Offset: 0x00021F88
		protected static MethodInfo FindMethod(Type componentClass, string name, Type[] args, Type returnType)
		{
			return MemberDescriptor.FindMethod(componentClass, name, args, returnType, true);
		}

		// Token: 0x06000DDA RID: 3546 RVA: 0x00023D94 File Offset: 0x00021F94
		protected static MethodInfo FindMethod(Type componentClass, string name, Type[] args, Type returnType, bool publicOnly)
		{
			BindingFlags bindingAttr;
			if (publicOnly)
			{
				bindingAttr = BindingFlags.Public;
			}
			else
			{
				bindingAttr = (BindingFlags.Public | BindingFlags.NonPublic);
			}
			return componentClass.GetMethod(name, bindingAttr, null, CallingConventions.Any, args, null);
		}

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06000DDB RID: 3547 RVA: 0x00023DC0 File Offset: 0x00021FC0
		internal static IComparer DefaultComparer
		{
			get
			{
				if (MemberDescriptor.default_comparer == null)
				{
					MemberDescriptor.default_comparer = new MemberDescriptor.MemberDescriptorComparer();
				}
				return MemberDescriptor.default_comparer;
			}
		}

		// Token: 0x040003E7 RID: 999
		private string name;

		// Token: 0x040003E8 RID: 1000
		private Attribute[] attrs;

		// Token: 0x040003E9 RID: 1001
		private AttributeCollection attrCollection;

		// Token: 0x040003EA RID: 1002
		private static IComparer default_comparer;

		// Token: 0x0200018B RID: 395
		private class MemberDescriptorComparer : IComparer
		{
			// Token: 0x06000DDD RID: 3549 RVA: 0x00023DE4 File Offset: 0x00021FE4
			public int Compare(object x, object y)
			{
				return string.Compare(((MemberDescriptor)x).Name, ((MemberDescriptor)y).Name, false, CultureInfo.InvariantCulture);
			}
		}
	}
}
