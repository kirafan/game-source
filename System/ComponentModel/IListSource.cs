﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x0200015B RID: 347
	[TypeConverter("System.Windows.Forms.Design.DataSourceConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[MergableProperty(false)]
	public interface IListSource
	{
		// Token: 0x06000C90 RID: 3216
		IList GetList();

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06000C91 RID: 3217
		bool ContainsListCollection { get; }
	}
}
