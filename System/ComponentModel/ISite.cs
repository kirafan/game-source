﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x0200016D RID: 365
	[ComVisible(true)]
	public interface ISite : IServiceProvider
	{
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06000CCA RID: 3274
		IComponent Component { get; }

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000CCB RID: 3275
		IContainer Container { get; }

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06000CCC RID: 3276
		bool DesignMode { get; }

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06000CCD RID: 3277
		// (set) Token: 0x06000CCE RID: 3278
		string Name { get; set; }
	}
}
