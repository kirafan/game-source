﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000505 RID: 1285
	// (Invoke) Token: 0x06002CC0 RID: 11456
	public delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs e);
}
