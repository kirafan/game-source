﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000178 RID: 376
	public abstract class LicenseProvider
	{
		// Token: 0x06000D06 RID: 3334
		public abstract License GetLicense(LicenseContext context, Type type, object instance, bool allowExceptions);
	}
}
