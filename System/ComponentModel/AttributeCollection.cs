﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x020000CB RID: 203
	[ComVisible(true)]
	public class AttributeCollection : ICollection, IEnumerable
	{
		// Token: 0x060008B0 RID: 2224 RVA: 0x00019B00 File Offset: 0x00017D00
		internal AttributeCollection(ArrayList attributes)
		{
			if (attributes != null)
			{
				this.attrList = attributes;
			}
		}

		// Token: 0x060008B1 RID: 2225 RVA: 0x00019B20 File Offset: 0x00017D20
		public AttributeCollection(params Attribute[] attributes)
		{
			if (attributes != null)
			{
				for (int i = 0; i < attributes.Length; i++)
				{
					this.attrList.Add(attributes[i]);
				}
			}
		}

		// Token: 0x060008B3 RID: 2227 RVA: 0x00019B78 File Offset: 0x00017D78
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x060008B4 RID: 2228 RVA: 0x00019B80 File Offset: 0x00017D80
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.attrList.IsSynchronized;
			}
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x060008B5 RID: 2229 RVA: 0x00019B90 File Offset: 0x00017D90
		object ICollection.SyncRoot
		{
			get
			{
				return this.attrList.SyncRoot;
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x060008B6 RID: 2230 RVA: 0x00019BA0 File Offset: 0x00017DA0
		int ICollection.Count
		{
			get
			{
				return this.Count;
			}
		}

		// Token: 0x060008B7 RID: 2231 RVA: 0x00019BA8 File Offset: 0x00017DA8
		public static AttributeCollection FromExisting(AttributeCollection existing, params Attribute[] newAttributes)
		{
			if (existing == null)
			{
				throw new ArgumentNullException("existing");
			}
			AttributeCollection attributeCollection = new AttributeCollection(new Attribute[0]);
			attributeCollection.attrList.AddRange(existing.attrList);
			if (newAttributes != null)
			{
				attributeCollection.attrList.AddRange(newAttributes);
			}
			return attributeCollection;
		}

		// Token: 0x060008B8 RID: 2232 RVA: 0x00019BF8 File Offset: 0x00017DF8
		public bool Contains(Attribute attr)
		{
			Attribute attribute = this[attr.GetType()];
			return attribute != null && attr.Equals(attribute);
		}

		// Token: 0x060008B9 RID: 2233 RVA: 0x00019C24 File Offset: 0x00017E24
		public bool Contains(Attribute[] attributes)
		{
			if (attributes == null)
			{
				return true;
			}
			foreach (Attribute attr in attributes)
			{
				if (!this.Contains(attr))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060008BA RID: 2234 RVA: 0x00019C64 File Offset: 0x00017E64
		public void CopyTo(Array array, int index)
		{
			this.attrList.CopyTo(array, index);
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x00019C74 File Offset: 0x00017E74
		public IEnumerator GetEnumerator()
		{
			return this.attrList.GetEnumerator();
		}

		// Token: 0x060008BC RID: 2236 RVA: 0x00019C84 File Offset: 0x00017E84
		public bool Matches(Attribute attr)
		{
			foreach (object obj in this.attrList)
			{
				Attribute attribute = (Attribute)obj;
				if (attribute.Match(attr))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060008BD RID: 2237 RVA: 0x00019D04 File Offset: 0x00017F04
		public bool Matches(Attribute[] attributes)
		{
			foreach (Attribute attr in attributes)
			{
				if (!this.Matches(attr))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060008BE RID: 2238 RVA: 0x00019D3C File Offset: 0x00017F3C
		protected Attribute GetDefaultAttribute(Type attributeType)
		{
			Attribute attribute = null;
			BindingFlags bindingAttr = BindingFlags.Static | BindingFlags.Public;
			FieldInfo field = attributeType.GetField("Default", bindingAttr);
			if (field == null)
			{
				ConstructorInfo constructor = attributeType.GetConstructor(Type.EmptyTypes);
				if (constructor != null)
				{
					attribute = (constructor.Invoke(null) as Attribute);
				}
				if (attribute != null && !attribute.IsDefaultAttribute())
				{
					attribute = null;
				}
			}
			else
			{
				attribute = (Attribute)field.GetValue(null);
			}
			return attribute;
		}

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x060008BF RID: 2239 RVA: 0x00019DA8 File Offset: 0x00017FA8
		public int Count
		{
			get
			{
				return (this.attrList == null) ? 0 : this.attrList.Count;
			}
		}

		// Token: 0x170001EC RID: 492
		public virtual Attribute this[Type type]
		{
			get
			{
				Attribute attribute = null;
				if (this.attrList != null)
				{
					foreach (object obj in this.attrList)
					{
						Attribute attribute2 = (Attribute)obj;
						if (type.IsAssignableFrom(attribute2.GetType()))
						{
							attribute = attribute2;
							break;
						}
					}
				}
				if (attribute == null)
				{
					attribute = this.GetDefaultAttribute(type);
				}
				return attribute;
			}
		}

		// Token: 0x170001ED RID: 493
		public virtual Attribute this[int index]
		{
			get
			{
				return (Attribute)this.attrList[index];
			}
		}

		// Token: 0x04000242 RID: 578
		private ArrayList attrList = new ArrayList();

		// Token: 0x04000243 RID: 579
		public static readonly AttributeCollection Empty = new AttributeCollection(null);
	}
}
