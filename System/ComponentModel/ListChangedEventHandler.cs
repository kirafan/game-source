﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000504 RID: 1284
	// (Invoke) Token: 0x06002CBC RID: 11452
	public delegate void ListChangedEventHandler(object sender, ListChangedEventArgs e);
}
