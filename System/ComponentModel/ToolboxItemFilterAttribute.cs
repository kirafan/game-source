﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001AC RID: 428
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	[Serializable]
	public sealed class ToolboxItemFilterAttribute : Attribute
	{
		// Token: 0x06000EE9 RID: 3817 RVA: 0x00026A3C File Offset: 0x00024C3C
		public ToolboxItemFilterAttribute(string filterString)
		{
			this.Filter = filterString;
			this.ItemFilterType = ToolboxItemFilterType.Allow;
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x00026A54 File Offset: 0x00024C54
		public ToolboxItemFilterAttribute(string filterString, ToolboxItemFilterType filterType)
		{
			this.Filter = filterString;
			this.ItemFilterType = filterType;
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06000EEB RID: 3819 RVA: 0x00026A6C File Offset: 0x00024C6C
		public string FilterString
		{
			get
			{
				return this.Filter;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06000EEC RID: 3820 RVA: 0x00026A74 File Offset: 0x00024C74
		public ToolboxItemFilterType FilterType
		{
			get
			{
				return this.ItemFilterType;
			}
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000EED RID: 3821 RVA: 0x00026A7C File Offset: 0x00024C7C
		public override object TypeId
		{
			get
			{
				return base.TypeId + this.Filter;
			}
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x00026A90 File Offset: 0x00024C90
		public override bool Equals(object obj)
		{
			return obj is ToolboxItemFilterAttribute && (obj == this || (((ToolboxItemFilterAttribute)obj).FilterString == this.Filter && ((ToolboxItemFilterAttribute)obj).FilterType == this.ItemFilterType));
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x00026AE4 File Offset: 0x00024CE4
		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x00026AF4 File Offset: 0x00024CF4
		public override bool Match(object obj)
		{
			return obj is ToolboxItemFilterAttribute && ((ToolboxItemFilterAttribute)obj).FilterString == this.Filter;
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x00026B1C File Offset: 0x00024D1C
		public override string ToString()
		{
			return string.Format("{0},{1}", this.Filter, this.ItemFilterType);
		}

		// Token: 0x0400043B RID: 1083
		private string Filter;

		// Token: 0x0400043C RID: 1084
		private ToolboxItemFilterType ItemFilterType;
	}
}
