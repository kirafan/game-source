﻿using System;
using System.ComponentModel.Design;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000184 RID: 388
	[Designer("System.Windows.Forms.Design.ComponentDocumentDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(System.ComponentModel.Design.IRootDesigner))]
	[DesignerCategory("Component")]
	[TypeConverter(typeof(ComponentConverter))]
	[ComVisible(true)]
	public class MarshalByValueComponent : IDisposable, IServiceProvider, IComponent
	{
		// Token: 0x14000039 RID: 57
		// (add) Token: 0x06000D46 RID: 3398 RVA: 0x000210C8 File Offset: 0x0001F2C8
		// (remove) Token: 0x06000D47 RID: 3399 RVA: 0x000210DC File Offset: 0x0001F2DC
		public event EventHandler Disposed
		{
			add
			{
				this.Events.AddHandler(this.disposedEvent, value);
			}
			remove
			{
				this.Events.RemoveHandler(this.disposedEvent, value);
			}
		}

		// Token: 0x06000D48 RID: 3400 RVA: 0x000210F0 File Offset: 0x0001F2F0
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000D49 RID: 3401 RVA: 0x00021100 File Offset: 0x0001F300
		[MonoTODO]
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
		}

		// Token: 0x06000D4A RID: 3402 RVA: 0x00021108 File Offset: 0x0001F308
		~MarshalByValueComponent()
		{
			this.Dispose(false);
		}

		// Token: 0x06000D4B RID: 3403 RVA: 0x00021144 File Offset: 0x0001F344
		public virtual object GetService(Type service)
		{
			if (this.mySite != null)
			{
				return this.mySite.GetService(service);
			}
			return null;
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000D4C RID: 3404 RVA: 0x00021160 File Offset: 0x0001F360
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual IContainer Container
		{
			get
			{
				if (this.mySite == null)
				{
					return null;
				}
				return this.mySite.Container;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000D4D RID: 3405 RVA: 0x0002117C File Offset: 0x0001F37C
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual bool DesignMode
		{
			get
			{
				return this.mySite != null && this.mySite.DesignMode;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000D4E RID: 3406 RVA: 0x00021198 File Offset: 0x0001F398
		// (set) Token: 0x06000D4F RID: 3407 RVA: 0x000211A0 File Offset: 0x0001F3A0
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual ISite Site
		{
			get
			{
				return this.mySite;
			}
			set
			{
				this.mySite = value;
			}
		}

		// Token: 0x06000D50 RID: 3408 RVA: 0x000211AC File Offset: 0x0001F3AC
		public override string ToString()
		{
			if (this.mySite == null)
			{
				return base.GetType().ToString();
			}
			return string.Format("{0} [{1}]", this.mySite.Name, base.GetType().ToString());
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000D51 RID: 3409 RVA: 0x000211F0 File Offset: 0x0001F3F0
		protected EventHandlerList Events
		{
			get
			{
				if (this.eventList == null)
				{
					this.eventList = new EventHandlerList();
				}
				return this.eventList;
			}
		}

		// Token: 0x040003AC RID: 940
		private EventHandlerList eventList;

		// Token: 0x040003AD RID: 941
		private ISite mySite;

		// Token: 0x040003AE RID: 942
		private object disposedEvent = new object();
	}
}
