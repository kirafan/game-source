﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200015C RID: 348
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ImmutableObjectAttribute : Attribute
	{
		// Token: 0x06000C92 RID: 3218 RVA: 0x000201C0 File Offset: 0x0001E3C0
		public ImmutableObjectAttribute(bool immutable)
		{
			this.immutable = immutable;
		}

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06000C94 RID: 3220 RVA: 0x000201F4 File Offset: 0x0001E3F4
		public bool Immutable
		{
			get
			{
				return this.immutable;
			}
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x000201FC File Offset: 0x0001E3FC
		public override bool Equals(object obj)
		{
			return obj is ImmutableObjectAttribute && (obj == this || ((ImmutableObjectAttribute)obj).Immutable == this.immutable);
		}

		// Token: 0x06000C96 RID: 3222 RVA: 0x00020228 File Offset: 0x0001E428
		public override int GetHashCode()
		{
			return this.immutable.GetHashCode();
		}

		// Token: 0x06000C97 RID: 3223 RVA: 0x00020238 File Offset: 0x0001E438
		public override bool IsDefaultAttribute()
		{
			return this.immutable == ImmutableObjectAttribute.Default.Immutable;
		}

		// Token: 0x04000373 RID: 883
		private bool immutable;

		// Token: 0x04000374 RID: 884
		public static readonly ImmutableObjectAttribute Default = new ImmutableObjectAttribute(false);

		// Token: 0x04000375 RID: 885
		public static readonly ImmutableObjectAttribute No = new ImmutableObjectAttribute(false);

		// Token: 0x04000376 RID: 886
		public static readonly ImmutableObjectAttribute Yes = new ImmutableObjectAttribute(true);
	}
}
