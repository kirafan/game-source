﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000E6 RID: 230
	public abstract class CustomTypeDescriptor : ICustomTypeDescriptor
	{
		// Token: 0x0600099D RID: 2461 RVA: 0x0001BF54 File Offset: 0x0001A154
		protected CustomTypeDescriptor()
		{
		}

		// Token: 0x0600099E RID: 2462 RVA: 0x0001BF5C File Offset: 0x0001A15C
		protected CustomTypeDescriptor(ICustomTypeDescriptor parent)
		{
			this._parent = parent;
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x0001BF6C File Offset: 0x0001A16C
		public virtual AttributeCollection GetAttributes()
		{
			if (this._parent != null)
			{
				return this._parent.GetAttributes();
			}
			return AttributeCollection.Empty;
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x0001BF8C File Offset: 0x0001A18C
		public virtual string GetClassName()
		{
			if (this._parent != null)
			{
				return this._parent.GetClassName();
			}
			return null;
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x0001BFA8 File Offset: 0x0001A1A8
		public virtual string GetComponentName()
		{
			if (this._parent != null)
			{
				return this._parent.GetComponentName();
			}
			return null;
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x0001BFC4 File Offset: 0x0001A1C4
		public virtual TypeConverter GetConverter()
		{
			if (this._parent != null)
			{
				return this._parent.GetConverter();
			}
			return new TypeConverter();
		}

		// Token: 0x060009A3 RID: 2467 RVA: 0x0001BFE4 File Offset: 0x0001A1E4
		public virtual EventDescriptor GetDefaultEvent()
		{
			if (this._parent != null)
			{
				return this._parent.GetDefaultEvent();
			}
			return null;
		}

		// Token: 0x060009A4 RID: 2468 RVA: 0x0001C000 File Offset: 0x0001A200
		public virtual PropertyDescriptor GetDefaultProperty()
		{
			if (this._parent != null)
			{
				return this._parent.GetDefaultProperty();
			}
			return null;
		}

		// Token: 0x060009A5 RID: 2469 RVA: 0x0001C01C File Offset: 0x0001A21C
		public virtual object GetEditor(Type editorBaseType)
		{
			if (this._parent != null)
			{
				return this._parent.GetEditor(editorBaseType);
			}
			return null;
		}

		// Token: 0x060009A6 RID: 2470 RVA: 0x0001C038 File Offset: 0x0001A238
		public virtual EventDescriptorCollection GetEvents()
		{
			if (this._parent != null)
			{
				return this._parent.GetEvents();
			}
			return EventDescriptorCollection.Empty;
		}

		// Token: 0x060009A7 RID: 2471 RVA: 0x0001C058 File Offset: 0x0001A258
		public virtual EventDescriptorCollection GetEvents(Attribute[] attributes)
		{
			if (this._parent != null)
			{
				return this._parent.GetEvents(attributes);
			}
			return EventDescriptorCollection.Empty;
		}

		// Token: 0x060009A8 RID: 2472 RVA: 0x0001C078 File Offset: 0x0001A278
		public virtual PropertyDescriptorCollection GetProperties()
		{
			if (this._parent != null)
			{
				return this._parent.GetProperties();
			}
			return PropertyDescriptorCollection.Empty;
		}

		// Token: 0x060009A9 RID: 2473 RVA: 0x0001C098 File Offset: 0x0001A298
		public virtual PropertyDescriptorCollection GetProperties(Attribute[] attributes)
		{
			if (this._parent != null)
			{
				return this._parent.GetProperties(attributes);
			}
			return PropertyDescriptorCollection.Empty;
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x0001C0B8 File Offset: 0x0001A2B8
		public virtual object GetPropertyOwner(PropertyDescriptor pd)
		{
			if (this._parent != null)
			{
				return this._parent.GetPropertyOwner(pd);
			}
			return null;
		}

		// Token: 0x0400028A RID: 650
		private ICustomTypeDescriptor _parent;
	}
}
