﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200017C RID: 380
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class ListBindableAttribute : Attribute
	{
		// Token: 0x06000D0E RID: 3342 RVA: 0x00020BC0 File Offset: 0x0001EDC0
		public ListBindableAttribute(bool listBindable)
		{
			this.bindable = listBindable;
		}

		// Token: 0x06000D0F RID: 3343 RVA: 0x00020BD0 File Offset: 0x0001EDD0
		public ListBindableAttribute(BindableSupport flags)
		{
			if (flags == BindableSupport.No)
			{
				this.bindable = false;
			}
			else
			{
				this.bindable = true;
			}
		}

		// Token: 0x06000D11 RID: 3345 RVA: 0x00020C18 File Offset: 0x0001EE18
		public override bool Equals(object obj)
		{
			return obj is ListBindableAttribute && ((ListBindableAttribute)obj).ListBindable.Equals(this.bindable);
		}

		// Token: 0x06000D12 RID: 3346 RVA: 0x00020C4C File Offset: 0x0001EE4C
		public override int GetHashCode()
		{
			return this.bindable.GetHashCode();
		}

		// Token: 0x06000D13 RID: 3347 RVA: 0x00020C5C File Offset: 0x0001EE5C
		public override bool IsDefaultAttribute()
		{
			return this.Equals(ListBindableAttribute.Default);
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000D14 RID: 3348 RVA: 0x00020C6C File Offset: 0x0001EE6C
		public bool ListBindable
		{
			get
			{
				return this.bindable;
			}
		}

		// Token: 0x0400038C RID: 908
		public static readonly ListBindableAttribute Default = new ListBindableAttribute(true);

		// Token: 0x0400038D RID: 909
		public static readonly ListBindableAttribute No = new ListBindableAttribute(false);

		// Token: 0x0400038E RID: 910
		public static readonly ListBindableAttribute Yes = new ListBindableAttribute(true);

		// Token: 0x0400038F RID: 911
		private bool bindable;
	}
}
