﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000157 RID: 343
	public interface IDataErrorInfo
	{
		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000C88 RID: 3208
		string Error { get; }

		// Token: 0x170002D5 RID: 725
		string this[string columnName]
		{
			get;
		}
	}
}
