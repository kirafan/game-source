﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000164 RID: 356
	[AttributeUsage(AttributeTargets.Class)]
	public class InstallerTypeAttribute : Attribute
	{
		// Token: 0x06000CA8 RID: 3240 RVA: 0x00020324 File Offset: 0x0001E524
		public InstallerTypeAttribute(string typeName)
		{
			this.installer = Type.GetType(typeName, false);
		}

		// Token: 0x06000CA9 RID: 3241 RVA: 0x0002033C File Offset: 0x0001E53C
		public InstallerTypeAttribute(Type installerType)
		{
			this.installer = installerType;
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000CAA RID: 3242 RVA: 0x0002034C File Offset: 0x0001E54C
		public virtual Type InstallerType
		{
			get
			{
				return this.installer;
			}
		}

		// Token: 0x06000CAB RID: 3243 RVA: 0x00020354 File Offset: 0x0001E554
		public override bool Equals(object obj)
		{
			return obj is InstallerTypeAttribute && (obj == this || ((InstallerTypeAttribute)obj).InstallerType == this.installer);
		}

		// Token: 0x06000CAC RID: 3244 RVA: 0x00020380 File Offset: 0x0001E580
		public override int GetHashCode()
		{
			return this.installer.GetHashCode();
		}

		// Token: 0x04000381 RID: 897
		private Type installer;
	}
}
