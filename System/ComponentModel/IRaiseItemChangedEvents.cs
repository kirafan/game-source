﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200016C RID: 364
	public interface IRaiseItemChangedEvents
	{
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000CC9 RID: 3273
		bool RaisesItemChangedEvents { get; }
	}
}
