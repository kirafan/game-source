﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000143 RID: 323
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate)]
	public sealed class EditorBrowsableAttribute : Attribute
	{
		// Token: 0x06000BE7 RID: 3047 RVA: 0x0001F158 File Offset: 0x0001D358
		public EditorBrowsableAttribute()
		{
			this.state = EditorBrowsableState.Always;
		}

		// Token: 0x06000BE8 RID: 3048 RVA: 0x0001F168 File Offset: 0x0001D368
		public EditorBrowsableAttribute(EditorBrowsableState state)
		{
			this.state = state;
		}

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x0001F178 File Offset: 0x0001D378
		public EditorBrowsableState State
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x06000BEA RID: 3050 RVA: 0x0001F180 File Offset: 0x0001D380
		public override bool Equals(object obj)
		{
			return obj is EditorBrowsableAttribute && (obj == this || ((EditorBrowsableAttribute)obj).State == this.state);
		}

		// Token: 0x06000BEB RID: 3051 RVA: 0x0001F1AC File Offset: 0x0001D3AC
		public override int GetHashCode()
		{
			return this.state.GetHashCode();
		}

		// Token: 0x04000360 RID: 864
		private EditorBrowsableState state;
	}
}
