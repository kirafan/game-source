﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000177 RID: 375
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class LicenseProviderAttribute : Attribute
	{
		// Token: 0x06000CFD RID: 3325 RVA: 0x000209E4 File Offset: 0x0001EBE4
		public LicenseProviderAttribute()
		{
			this.Provider = null;
		}

		// Token: 0x06000CFE RID: 3326 RVA: 0x000209F4 File Offset: 0x0001EBF4
		public LicenseProviderAttribute(string typeName)
		{
			this.Provider = Type.GetType(typeName, false);
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x00020A0C File Offset: 0x0001EC0C
		public LicenseProviderAttribute(Type type)
		{
			this.Provider = type;
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000D01 RID: 3329 RVA: 0x00020A28 File Offset: 0x0001EC28
		public Type LicenseProvider
		{
			get
			{
				return this.Provider;
			}
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06000D02 RID: 3330 RVA: 0x00020A30 File Offset: 0x0001EC30
		public override object TypeId
		{
			get
			{
				return base.ToString() + ((this.Provider == null) ? null : this.Provider.ToString());
			}
		}

		// Token: 0x06000D03 RID: 3331 RVA: 0x00020A5C File Offset: 0x0001EC5C
		public override bool Equals(object obj)
		{
			return obj is LicenseProviderAttribute && (obj == this || ((LicenseProviderAttribute)obj).LicenseProvider.Equals(this.Provider));
		}

		// Token: 0x06000D04 RID: 3332 RVA: 0x00020A98 File Offset: 0x0001EC98
		public override int GetHashCode()
		{
			return this.Provider.GetHashCode();
		}

		// Token: 0x04000386 RID: 902
		private Type Provider;

		// Token: 0x04000387 RID: 903
		public static readonly LicenseProviderAttribute Default = new LicenseProviderAttribute();
	}
}
