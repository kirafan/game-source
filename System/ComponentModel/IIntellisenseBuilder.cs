﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200015A RID: 346
	public interface IIntellisenseBuilder
	{
		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000C8E RID: 3214
		string Name { get; }

		// Token: 0x06000C8F RID: 3215
		bool Show(string language, string value, ref string newValue);
	}
}
