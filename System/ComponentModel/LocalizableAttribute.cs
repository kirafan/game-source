﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000182 RID: 386
	[AttributeUsage(AttributeTargets.All)]
	public sealed class LocalizableAttribute : Attribute
	{
		// Token: 0x06000D36 RID: 3382 RVA: 0x00020EBC File Offset: 0x0001F0BC
		public LocalizableAttribute(bool localizable)
		{
			this.localizable = localizable;
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000D38 RID: 3384 RVA: 0x00020EF0 File Offset: 0x0001F0F0
		public bool IsLocalizable
		{
			get
			{
				return this.localizable;
			}
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00020EF8 File Offset: 0x0001F0F8
		public override bool Equals(object obj)
		{
			return obj is LocalizableAttribute && (obj == this || ((LocalizableAttribute)obj).IsLocalizable == this.localizable);
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x00020F24 File Offset: 0x0001F124
		public override int GetHashCode()
		{
			return this.localizable.GetHashCode();
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x00020F34 File Offset: 0x0001F134
		public override bool IsDefaultAttribute()
		{
			return this.localizable == LocalizableAttribute.Default.IsLocalizable;
		}

		// Token: 0x040003A3 RID: 931
		private bool localizable;

		// Token: 0x040003A4 RID: 932
		public static readonly LocalizableAttribute Default = new LocalizableAttribute(false);

		// Token: 0x040003A5 RID: 933
		public static readonly LocalizableAttribute No = new LocalizableAttribute(false);

		// Token: 0x040003A6 RID: 934
		public static readonly LocalizableAttribute Yes = new LocalizableAttribute(true);
	}
}
