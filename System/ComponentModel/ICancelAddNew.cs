﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000151 RID: 337
	public interface ICancelAddNew
	{
		// Token: 0x06000C64 RID: 3172
		void CancelNew(int itemIndex);

		// Token: 0x06000C65 RID: 3173
		void EndNew(int itemIndex);
	}
}
