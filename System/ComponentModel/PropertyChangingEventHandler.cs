﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000506 RID: 1286
	// (Invoke) Token: 0x06002CC4 RID: 11460
	public delegate void PropertyChangingEventHandler(object sender, PropertyChangingEventArgs e);
}
