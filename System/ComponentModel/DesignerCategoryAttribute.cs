﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000106 RID: 262
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class DesignerCategoryAttribute : Attribute
	{
		// Token: 0x06000A8D RID: 2701 RVA: 0x0001D774 File Offset: 0x0001B974
		public DesignerCategoryAttribute()
		{
			this.category = string.Empty;
		}

		// Token: 0x06000A8E RID: 2702 RVA: 0x0001D788 File Offset: 0x0001B988
		public DesignerCategoryAttribute(string category)
		{
			this.category = category;
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x0001D7E4 File Offset: 0x0001B9E4
		public override object TypeId
		{
			get
			{
				return base.GetType();
			}
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000A91 RID: 2705 RVA: 0x0001D7EC File Offset: 0x0001B9EC
		public string Category
		{
			get
			{
				return this.category;
			}
		}

		// Token: 0x06000A92 RID: 2706 RVA: 0x0001D7F4 File Offset: 0x0001B9F4
		public override bool Equals(object obj)
		{
			return obj is DesignerCategoryAttribute && (obj == this || ((DesignerCategoryAttribute)obj).Category == this.category);
		}

		// Token: 0x06000A93 RID: 2707 RVA: 0x0001D830 File Offset: 0x0001BA30
		public override int GetHashCode()
		{
			return this.category.GetHashCode();
		}

		// Token: 0x06000A94 RID: 2708 RVA: 0x0001D840 File Offset: 0x0001BA40
		public override bool IsDefaultAttribute()
		{
			return this.category == DesignerCategoryAttribute.Default.Category;
		}

		// Token: 0x040002CB RID: 715
		private string category;

		// Token: 0x040002CC RID: 716
		public static readonly DesignerCategoryAttribute Component = new DesignerCategoryAttribute("Component");

		// Token: 0x040002CD RID: 717
		public static readonly DesignerCategoryAttribute Form = new DesignerCategoryAttribute("Form");

		// Token: 0x040002CE RID: 718
		public static readonly DesignerCategoryAttribute Generic = new DesignerCategoryAttribute("Designer");

		// Token: 0x040002CF RID: 719
		public static readonly DesignerCategoryAttribute Default = new DesignerCategoryAttribute(string.Empty);
	}
}
