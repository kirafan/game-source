﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200014A RID: 330
	public sealed class EventHandlerList : IDisposable
	{
		// Token: 0x170002BF RID: 703
		public Delegate this[object key]
		{
			get
			{
				if (key == null)
				{
					return this.null_entry;
				}
				ListEntry listEntry = this.FindEntry(key);
				if (listEntry != null)
				{
					return listEntry.value;
				}
				return null;
			}
			set
			{
				this.AddHandler(key, value);
			}
		}

		// Token: 0x06000C33 RID: 3123 RVA: 0x0001FD98 File Offset: 0x0001DF98
		public void AddHandler(object key, Delegate value)
		{
			if (key == null)
			{
				this.null_entry = Delegate.Combine(this.null_entry, value);
				return;
			}
			ListEntry listEntry = this.FindEntry(key);
			if (listEntry == null)
			{
				listEntry = new ListEntry();
				listEntry.key = key;
				listEntry.value = null;
				listEntry.next = this.entries;
				this.entries = listEntry;
			}
			listEntry.value = Delegate.Combine(listEntry.value, value);
		}

		// Token: 0x06000C34 RID: 3124 RVA: 0x0001FE08 File Offset: 0x0001E008
		public void AddHandlers(EventHandlerList listToAddFrom)
		{
			if (listToAddFrom == null)
			{
				return;
			}
			for (ListEntry next = listToAddFrom.entries; next != null; next = next.next)
			{
				this.AddHandler(next.key, next.value);
			}
		}

		// Token: 0x06000C35 RID: 3125 RVA: 0x0001FE48 File Offset: 0x0001E048
		public void RemoveHandler(object key, Delegate value)
		{
			if (key == null)
			{
				this.null_entry = Delegate.Remove(this.null_entry, value);
				return;
			}
			ListEntry listEntry = this.FindEntry(key);
			if (listEntry == null)
			{
				return;
			}
			listEntry.value = Delegate.Remove(listEntry.value, value);
		}

		// Token: 0x06000C36 RID: 3126 RVA: 0x0001FE90 File Offset: 0x0001E090
		public void Dispose()
		{
			this.entries = null;
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x0001FE9C File Offset: 0x0001E09C
		private ListEntry FindEntry(object key)
		{
			for (ListEntry next = this.entries; next != null; next = next.next)
			{
				if (next.key == key)
				{
					return next;
				}
			}
			return null;
		}

		// Token: 0x0400036D RID: 877
		private ListEntry entries;

		// Token: 0x0400036E RID: 878
		private Delegate null_entry;
	}
}
