﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020001AD RID: 429
	public enum ToolboxItemFilterType
	{
		// Token: 0x0400043E RID: 1086
		Allow,
		// Token: 0x0400043F RID: 1087
		Custom,
		// Token: 0x04000440 RID: 1088
		Prevent,
		// Token: 0x04000441 RID: 1089
		Require
	}
}
