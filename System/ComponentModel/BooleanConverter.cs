﻿using System;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x020000D2 RID: 210
	public class BooleanConverter : TypeConverter
	{
		// Token: 0x06000923 RID: 2339 RVA: 0x0001A964 File Offset: 0x00018B64
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x0001A980 File Offset: 0x00018B80
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value is string)
			{
				return bool.Parse((string)value);
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x0001A9A8 File Offset: 0x00018BA8
		public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			bool[] array = new bool[2];
			array[0] = true;
			return new TypeConverter.StandardValuesCollection(array);
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0001A9BC File Offset: 0x00018BBC
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x0001A9C0 File Offset: 0x00018BC0
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
