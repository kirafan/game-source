﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020004F5 RID: 1269
	// (Invoke) Token: 0x06002C80 RID: 11392
	public delegate void AddingNewEventHandler(object sender, AddingNewEventArgs e);
}
