﻿using System;
using System.Collections;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x020000DA RID: 218
	public class CollectionConverter : TypeConverter
	{
		// Token: 0x06000955 RID: 2389 RVA: 0x0001B3A0 File Offset: 0x000195A0
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof(string) && value != null && value is ICollection)
			{
				return "(Collection)";
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x0001B3D8 File Offset: 0x000195D8
		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return null;
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x0001B3DC File Offset: 0x000195DC
		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return false;
		}
	}
}
