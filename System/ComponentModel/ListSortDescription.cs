﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000180 RID: 384
	public class ListSortDescription
	{
		// Token: 0x06000D31 RID: 3377 RVA: 0x00020E7C File Offset: 0x0001F07C
		public ListSortDescription(PropertyDescriptor propertyDescriptor, ListSortDirection sortDirection)
		{
			this.propertyDescriptor = propertyDescriptor;
			this.sortDirection = sortDirection;
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000D32 RID: 3378 RVA: 0x00020E94 File Offset: 0x0001F094
		// (set) Token: 0x06000D33 RID: 3379 RVA: 0x00020E9C File Offset: 0x0001F09C
		public PropertyDescriptor PropertyDescriptor
		{
			get
			{
				return this.propertyDescriptor;
			}
			set
			{
				this.propertyDescriptor = value;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000D34 RID: 3380 RVA: 0x00020EA8 File Offset: 0x0001F0A8
		// (set) Token: 0x06000D35 RID: 3381 RVA: 0x00020EB0 File Offset: 0x0001F0B0
		public ListSortDirection SortDirection
		{
			get
			{
				return this.sortDirection;
			}
			set
			{
				this.sortDirection = value;
			}
		}

		// Token: 0x0400039E RID: 926
		private PropertyDescriptor propertyDescriptor;

		// Token: 0x0400039F RID: 927
		private ListSortDirection sortDirection;
	}
}
