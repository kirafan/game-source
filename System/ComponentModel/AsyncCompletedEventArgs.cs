﻿using System;
using System.Reflection;

namespace System.ComponentModel
{
	// Token: 0x020000C7 RID: 199
	public class AsyncCompletedEventArgs : EventArgs
	{
		// Token: 0x0600089B RID: 2203 RVA: 0x000198FC File Offset: 0x00017AFC
		public AsyncCompletedEventArgs(Exception error, bool cancelled, object userState)
		{
			this._error = error;
			this._cancelled = cancelled;
			this._userState = userState;
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x0001991C File Offset: 0x00017B1C
		protected void RaiseExceptionIfNecessary()
		{
			if (this._error != null)
			{
				throw new TargetInvocationException(this._error);
			}
			if (this._cancelled)
			{
				throw new InvalidOperationException("The operation was cancelled");
			}
		}

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x0600089D RID: 2205 RVA: 0x0001994C File Offset: 0x00017B4C
		public bool Cancelled
		{
			get
			{
				return this._cancelled;
			}
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x0600089E RID: 2206 RVA: 0x00019954 File Offset: 0x00017B54
		public Exception Error
		{
			get
			{
				return this._error;
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0001995C File Offset: 0x00017B5C
		public object UserState
		{
			get
			{
				return this._userState;
			}
		}

		// Token: 0x0400023A RID: 570
		private Exception _error;

		// Token: 0x0400023B RID: 571
		private bool _cancelled;

		// Token: 0x0400023C RID: 572
		private object _userState;
	}
}
