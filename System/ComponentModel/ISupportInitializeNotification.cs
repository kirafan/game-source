﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200016F RID: 367
	public interface ISupportInitializeNotification : ISupportInitialize
	{
		// Token: 0x14000038 RID: 56
		// (add) Token: 0x06000CD1 RID: 3281
		// (remove) Token: 0x06000CD2 RID: 3282
		event EventHandler Initialized;

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06000CD3 RID: 3283
		bool IsInitialized { get; }
	}
}
