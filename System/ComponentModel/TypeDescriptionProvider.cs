﻿using System;
using System.Collections;

namespace System.ComponentModel
{
	// Token: 0x020001B2 RID: 434
	public abstract class TypeDescriptionProvider
	{
		// Token: 0x06000F32 RID: 3890 RVA: 0x00026FFC File Offset: 0x000251FC
		protected TypeDescriptionProvider()
		{
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x00027004 File Offset: 0x00025204
		protected TypeDescriptionProvider(TypeDescriptionProvider parent)
		{
			this._parent = parent;
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x00027014 File Offset: 0x00025214
		public virtual object CreateInstance(IServiceProvider provider, Type objectType, Type[] argTypes, object[] args)
		{
			if (this._parent != null)
			{
				return this._parent.CreateInstance(provider, objectType, argTypes, args);
			}
			return Activator.CreateInstance(objectType, args);
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x00027048 File Offset: 0x00025248
		public virtual IDictionary GetCache(object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetCache(instance);
			}
			return null;
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x00027064 File Offset: 0x00025264
		public virtual ICustomTypeDescriptor GetExtendedTypeDescriptor(object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetExtendedTypeDescriptor(instance);
			}
			if (this._emptyCustomTypeDescriptor == null)
			{
				this._emptyCustomTypeDescriptor = new TypeDescriptionProvider.EmptyCustomTypeDescriptor();
			}
			return this._emptyCustomTypeDescriptor;
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x000270A8 File Offset: 0x000252A8
		public virtual string GetFullComponentName(object component)
		{
			if (this._parent != null)
			{
				return this._parent.GetFullComponentName(component);
			}
			return this.GetTypeDescriptor(component).GetComponentName();
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x000270DC File Offset: 0x000252DC
		public Type GetReflectionType(object instance)
		{
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return this.GetReflectionType(instance.GetType(), instance);
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x000270FC File Offset: 0x000252FC
		public Type GetReflectionType(Type objectType)
		{
			return this.GetReflectionType(objectType, null);
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x00027108 File Offset: 0x00025308
		public virtual Type GetReflectionType(Type objectType, object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetReflectionType(objectType, instance);
			}
			return objectType;
		}

		// Token: 0x06000F3B RID: 3899 RVA: 0x00027124 File Offset: 0x00025324
		public ICustomTypeDescriptor GetTypeDescriptor(object instance)
		{
			if (instance == null)
			{
				throw new ArgumentNullException("instance");
			}
			return this.GetTypeDescriptor(instance.GetType(), instance);
		}

		// Token: 0x06000F3C RID: 3900 RVA: 0x00027144 File Offset: 0x00025344
		public ICustomTypeDescriptor GetTypeDescriptor(Type objectType)
		{
			return this.GetTypeDescriptor(objectType, null);
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x00027150 File Offset: 0x00025350
		public virtual ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
		{
			if (this._parent != null)
			{
				return this._parent.GetTypeDescriptor(objectType, instance);
			}
			if (this._emptyCustomTypeDescriptor == null)
			{
				this._emptyCustomTypeDescriptor = new TypeDescriptionProvider.EmptyCustomTypeDescriptor();
			}
			return this._emptyCustomTypeDescriptor;
		}

		// Token: 0x04000447 RID: 1095
		private TypeDescriptionProvider.EmptyCustomTypeDescriptor _emptyCustomTypeDescriptor;

		// Token: 0x04000448 RID: 1096
		private TypeDescriptionProvider _parent;

		// Token: 0x020001B3 RID: 435
		private sealed class EmptyCustomTypeDescriptor : CustomTypeDescriptor
		{
		}
	}
}
