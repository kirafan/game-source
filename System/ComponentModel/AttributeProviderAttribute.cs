﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000CA RID: 202
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class AttributeProviderAttribute : Attribute
	{
		// Token: 0x060008AB RID: 2219 RVA: 0x00019AB4 File Offset: 0x00017CB4
		public AttributeProviderAttribute(Type type)
		{
			this.type_name = type.AssemblyQualifiedName;
		}

		// Token: 0x060008AC RID: 2220 RVA: 0x00019AC8 File Offset: 0x00017CC8
		public AttributeProviderAttribute(string typeName, string propertyName)
		{
			this.type_name = typeName;
			this.property_name = propertyName;
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x00019AE0 File Offset: 0x00017CE0
		public AttributeProviderAttribute(string typeName)
		{
			this.type_name = typeName;
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x060008AE RID: 2222 RVA: 0x00019AF0 File Offset: 0x00017CF0
		public string PropertyName
		{
			get
			{
				return this.property_name;
			}
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x060008AF RID: 2223 RVA: 0x00019AF8 File Offset: 0x00017CF8
		public string TypeName
		{
			get
			{
				return this.type_name;
			}
		}

		// Token: 0x04000240 RID: 576
		private string type_name;

		// Token: 0x04000241 RID: 577
		private string property_name;
	}
}
