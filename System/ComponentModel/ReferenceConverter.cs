﻿using System;
using System.Collections;
using System.ComponentModel.Design;
using System.Globalization;

namespace System.ComponentModel
{
	// Token: 0x0200019D RID: 413
	public class ReferenceConverter : TypeConverter
	{
		// Token: 0x06000E92 RID: 3730 RVA: 0x00025640 File Offset: 0x00023840
		public ReferenceConverter(Type type)
		{
			this.reference_type = type;
		}

		// Token: 0x06000E93 RID: 3731 RVA: 0x00025650 File Offset: 0x00023850
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return (context != null && sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType);
		}

		// Token: 0x06000E94 RID: 3732 RVA: 0x00025680 File Offset: 0x00023880
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string))
			{
				return base.ConvertFrom(context, culture, value);
			}
			if (context != null)
			{
				object obj = null;
				System.ComponentModel.Design.IReferenceService referenceService = context.GetService(typeof(System.ComponentModel.Design.IReferenceService)) as System.ComponentModel.Design.IReferenceService;
				if (referenceService != null)
				{
					obj = referenceService.GetReference((string)value);
				}
				if (obj == null && context.Container != null && context.Container.Components != null)
				{
					obj = context.Container.Components[(string)value];
				}
				return obj;
			}
			return null;
		}

		// Token: 0x06000E95 RID: 3733 RVA: 0x00025710 File Offset: 0x00023910
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType != typeof(string))
			{
				return base.ConvertTo(context, culture, value, destinationType);
			}
			if (value == null)
			{
				return "(none)";
			}
			string text = string.Empty;
			if (context != null)
			{
				System.ComponentModel.Design.IReferenceService referenceService = context.GetService(typeof(System.ComponentModel.Design.IReferenceService)) as System.ComponentModel.Design.IReferenceService;
				if (referenceService != null)
				{
					text = referenceService.GetName(value);
				}
				if ((text == null || text.Length == 0) && value is IComponent)
				{
					IComponent component = (IComponent)value;
					if (component.Site != null && component.Site.Name != null)
					{
						text = component.Site.Name;
					}
				}
			}
			return text;
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x000257C4 File Offset: 0x000239C4
		public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
		{
			ArrayList arrayList = new ArrayList();
			if (context != null)
			{
				System.ComponentModel.Design.IReferenceService referenceService = context.GetService(typeof(System.ComponentModel.Design.IReferenceService)) as System.ComponentModel.Design.IReferenceService;
				if (referenceService != null)
				{
					foreach (object value in referenceService.GetReferences(this.reference_type))
					{
						if (this.IsValueAllowed(context, value))
						{
							arrayList.Add(value);
						}
					}
				}
				else if (context.Container != null && context.Container.Components != null)
				{
					foreach (object obj in context.Container.Components)
					{
						if (obj != null && this.IsValueAllowed(context, obj) && this.reference_type.IsInstanceOfType(obj))
						{
							arrayList.Add(obj);
						}
					}
				}
				arrayList.Add(null);
			}
			return new TypeConverter.StandardValuesCollection(arrayList);
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x000258FC File Offset: 0x00023AFC
		public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x00025900 File Offset: 0x00023B00
		public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
		{
			return true;
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x00025904 File Offset: 0x00023B04
		protected virtual bool IsValueAllowed(ITypeDescriptorContext context, object value)
		{
			return true;
		}

		// Token: 0x04000418 RID: 1048
		private Type reference_type;
	}
}
