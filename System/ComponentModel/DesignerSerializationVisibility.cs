﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x02000108 RID: 264
	[ComVisible(true)]
	public enum DesignerSerializationVisibility
	{
		// Token: 0x040002D6 RID: 726
		Hidden,
		// Token: 0x040002D7 RID: 727
		Visible,
		// Token: 0x040002D8 RID: 728
		Content
	}
}
