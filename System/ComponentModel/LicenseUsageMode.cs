﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000179 RID: 377
	public enum LicenseUsageMode
	{
		// Token: 0x04000389 RID: 905
		Designtime = 1,
		// Token: 0x0400038A RID: 906
		Runtime = 0
	}
}
