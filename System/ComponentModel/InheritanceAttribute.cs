﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200015F RID: 351
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event)]
	public sealed class InheritanceAttribute : Attribute
	{
		// Token: 0x06000C9A RID: 3226 RVA: 0x0002024C File Offset: 0x0001E44C
		public InheritanceAttribute()
		{
			this.level = InheritanceLevel.NotInherited;
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x0002025C File Offset: 0x0001E45C
		public InheritanceAttribute(InheritanceLevel inheritanceLevel)
		{
			this.level = inheritanceLevel;
		}

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000C9D RID: 3229 RVA: 0x0002029C File Offset: 0x0001E49C
		public InheritanceLevel InheritanceLevel
		{
			get
			{
				return this.level;
			}
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x000202A4 File Offset: 0x0001E4A4
		public override bool Equals(object obj)
		{
			return obj is InheritanceAttribute && (obj == this || ((InheritanceAttribute)obj).InheritanceLevel == this.level);
		}

		// Token: 0x06000C9F RID: 3231 RVA: 0x000202D0 File Offset: 0x0001E4D0
		public override int GetHashCode()
		{
			return this.level.GetHashCode();
		}

		// Token: 0x06000CA0 RID: 3232 RVA: 0x000202E4 File Offset: 0x0001E4E4
		public override bool IsDefaultAttribute()
		{
			return this.level == InheritanceAttribute.Default.InheritanceLevel;
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x000202F8 File Offset: 0x0001E4F8
		public override string ToString()
		{
			return this.level.ToString();
		}

		// Token: 0x04000377 RID: 887
		private InheritanceLevel level;

		// Token: 0x04000378 RID: 888
		public static readonly InheritanceAttribute Default = new InheritanceAttribute();

		// Token: 0x04000379 RID: 889
		public static readonly InheritanceAttribute Inherited = new InheritanceAttribute(InheritanceLevel.Inherited);

		// Token: 0x0400037A RID: 890
		public static readonly InheritanceAttribute InheritedReadOnly = new InheritanceAttribute(InheritanceLevel.InheritedReadOnly);

		// Token: 0x0400037B RID: 891
		public static readonly InheritanceAttribute NotInherited = new InheritanceAttribute(InheritanceLevel.NotInherited);
	}
}
