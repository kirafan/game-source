﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000D0 RID: 208
	public enum BindingDirection
	{
		// Token: 0x04000256 RID: 598
		OneWay,
		// Token: 0x04000257 RID: 599
		TwoWay
	}
}
