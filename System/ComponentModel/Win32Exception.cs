﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;

namespace System.ComponentModel
{
	// Token: 0x020001C5 RID: 453
	[SuppressUnmanagedCodeSecurity]
	[Serializable]
	public class Win32Exception : ExternalException
	{
		// Token: 0x06000FDC RID: 4060 RVA: 0x000299D8 File Offset: 0x00027BD8
		public Win32Exception() : base(Win32Exception.W32ErrorMessage(Marshal.GetLastWin32Error()))
		{
			this.native_error_code = Marshal.GetLastWin32Error();
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x000299F8 File Offset: 0x00027BF8
		public Win32Exception(int error) : base(Win32Exception.W32ErrorMessage(error))
		{
			this.native_error_code = error;
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x00029A10 File Offset: 0x00027C10
		public Win32Exception(int error, string message) : base(message)
		{
			this.native_error_code = error;
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x00029A20 File Offset: 0x00027C20
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public Win32Exception(string message) : base(message)
		{
			this.native_error_code = Marshal.GetLastWin32Error();
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x00029A34 File Offset: 0x00027C34
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public Win32Exception(string message, Exception innerException) : base(message, innerException)
		{
			this.native_error_code = Marshal.GetLastWin32Error();
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x00029A4C File Offset: 0x00027C4C
		protected Win32Exception(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.native_error_code = info.GetInt32("NativeErrorCode");
		}

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06000FE2 RID: 4066 RVA: 0x00029A68 File Offset: 0x00027C68
		public int NativeErrorCode
		{
			get
			{
				return this.native_error_code;
			}
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x00029A70 File Offset: 0x00027C70
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("NativeErrorCode", this.native_error_code);
			base.GetObjectData(info, context);
		}

		// Token: 0x06000FE4 RID: 4068
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string W32ErrorMessage(int error_code);

		// Token: 0x0400046C RID: 1132
		private int native_error_code;
	}
}
