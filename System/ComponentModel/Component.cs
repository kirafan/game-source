﻿using System;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x020000DE RID: 222
	[DesignerCategory("Component")]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	public class Component : MarshalByRefObject, IDisposable, IComponent
	{
		// Token: 0x06000967 RID: 2407 RVA: 0x0001B590 File Offset: 0x00019790
		public Component()
		{
			this.event_handlers = null;
		}

		// Token: 0x1400001C RID: 28
		// (add) Token: 0x06000968 RID: 2408 RVA: 0x0001B5AC File Offset: 0x000197AC
		// (remove) Token: 0x06000969 RID: 2409 RVA: 0x0001B5C0 File Offset: 0x000197C0
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		[Browsable(false)]
		public event EventHandler Disposed
		{
			add
			{
				this.Events.AddHandler(this.disposedEvent, value);
			}
			remove
			{
				this.Events.RemoveHandler(this.disposedEvent, value);
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x0600096A RID: 2410 RVA: 0x0001B5D4 File Offset: 0x000197D4
		protected virtual bool CanRaiseEvents
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x0600096B RID: 2411 RVA: 0x0001B5D8 File Offset: 0x000197D8
		// (set) Token: 0x0600096C RID: 2412 RVA: 0x0001B5E0 File Offset: 0x000197E0
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public virtual ISite Site
		{
			get
			{
				return this.mySite;
			}
			set
			{
				this.mySite = value;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x0600096D RID: 2413 RVA: 0x0001B5EC File Offset: 0x000197EC
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public IContainer Container
		{
			get
			{
				if (this.mySite == null)
				{
					return null;
				}
				return this.mySite.Container;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x0600096E RID: 2414 RVA: 0x0001B608 File Offset: 0x00019808
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		protected bool DesignMode
		{
			get
			{
				return this.mySite != null && this.mySite.DesignMode;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x0600096F RID: 2415 RVA: 0x0001B624 File Offset: 0x00019824
		protected EventHandlerList Events
		{
			get
			{
				if (this.event_handlers == null)
				{
					this.event_handlers = new EventHandlerList();
				}
				return this.event_handlers;
			}
		}

		// Token: 0x06000970 RID: 2416 RVA: 0x0001B644 File Offset: 0x00019844
		~Component()
		{
			this.Dispose(false);
		}

		// Token: 0x06000971 RID: 2417 RVA: 0x0001B680 File Offset: 0x00019880
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000972 RID: 2418 RVA: 0x0001B690 File Offset: 0x00019890
		protected virtual void Dispose(bool release_all)
		{
			if (release_all)
			{
				if (this.mySite != null && this.mySite.Container != null)
				{
					this.mySite.Container.Remove(this);
				}
				EventHandler eventHandler = (EventHandler)this.Events[this.disposedEvent];
				if (eventHandler != null)
				{
					eventHandler(this, EventArgs.Empty);
				}
			}
		}

		// Token: 0x06000973 RID: 2419 RVA: 0x0001B6F8 File Offset: 0x000198F8
		protected virtual object GetService(Type service)
		{
			if (this.mySite != null)
			{
				return this.mySite.GetService(service);
			}
			return null;
		}

		// Token: 0x06000974 RID: 2420 RVA: 0x0001B714 File Offset: 0x00019914
		public override string ToString()
		{
			if (this.mySite == null)
			{
				return base.GetType().ToString();
			}
			return string.Format("{0} [{1}]", this.mySite.Name, base.GetType().ToString());
		}

		// Token: 0x04000282 RID: 642
		private EventHandlerList event_handlers;

		// Token: 0x04000283 RID: 643
		private ISite mySite;

		// Token: 0x04000284 RID: 644
		private object disposedEvent = new object();
	}
}
