﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000193 RID: 403
	[AttributeUsage(AttributeTargets.All)]
	public sealed class PasswordPropertyTextAttribute : Attribute
	{
		// Token: 0x06000E14 RID: 3604 RVA: 0x000244D8 File Offset: 0x000226D8
		public PasswordPropertyTextAttribute() : this(false)
		{
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x000244E4 File Offset: 0x000226E4
		public PasswordPropertyTextAttribute(bool password)
		{
			this._password = password;
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x000244F4 File Offset: 0x000226F4
		static PasswordPropertyTextAttribute()
		{
			PasswordPropertyTextAttribute.Default = PasswordPropertyTextAttribute.No;
		}

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000E17 RID: 3607 RVA: 0x00024524 File Offset: 0x00022724
		public bool Password
		{
			get
			{
				return this._password;
			}
		}

		// Token: 0x06000E18 RID: 3608 RVA: 0x0002452C File Offset: 0x0002272C
		public override bool Equals(object o)
		{
			return o is PasswordPropertyTextAttribute && ((PasswordPropertyTextAttribute)o).Password == this.Password;
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x0002455C File Offset: 0x0002275C
		public override int GetHashCode()
		{
			return this.Password.GetHashCode();
		}

		// Token: 0x06000E1A RID: 3610 RVA: 0x00024578 File Offset: 0x00022778
		public override bool IsDefaultAttribute()
		{
			return PasswordPropertyTextAttribute.Default.Equals(this);
		}

		// Token: 0x040003FC RID: 1020
		public static readonly PasswordPropertyTextAttribute Default;

		// Token: 0x040003FD RID: 1021
		public static readonly PasswordPropertyTextAttribute No = new PasswordPropertyTextAttribute(false);

		// Token: 0x040003FE RID: 1022
		public static readonly PasswordPropertyTextAttribute Yes = new PasswordPropertyTextAttribute(true);

		// Token: 0x040003FF RID: 1023
		private bool _password;
	}
}
