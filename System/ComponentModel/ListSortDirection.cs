﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000181 RID: 385
	public enum ListSortDirection
	{
		// Token: 0x040003A1 RID: 929
		Ascending,
		// Token: 0x040003A2 RID: 930
		Descending
	}
}
