﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.ComponentModel
{
	// Token: 0x020000DC RID: 220
	[ComVisible(true)]
	public class ComponentCollection : ReadOnlyCollectionBase
	{
		// Token: 0x06000960 RID: 2400 RVA: 0x0001B4A8 File Offset: 0x000196A8
		public ComponentCollection(IComponent[] components)
		{
			base.InnerList.AddRange(components);
		}

		// Token: 0x1700021F RID: 543
		public virtual IComponent this[int index]
		{
			get
			{
				return (IComponent)base.InnerList[index];
			}
		}

		// Token: 0x17000220 RID: 544
		public virtual IComponent this[string name]
		{
			get
			{
				foreach (object obj in base.InnerList)
				{
					IComponent component = (IComponent)obj;
					if (component.Site != null && component.Site.Name == name)
					{
						return component;
					}
				}
				return null;
			}
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x0001B564 File Offset: 0x00019764
		public void CopyTo(IComponent[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}
	}
}
