﻿using System;

namespace System.ComponentModel
{
	// Token: 0x02000192 RID: 402
	[AttributeUsage(AttributeTargets.All)]
	public sealed class ParenthesizePropertyNameAttribute : Attribute
	{
		// Token: 0x06000E0D RID: 3597 RVA: 0x00024454 File Offset: 0x00022654
		public ParenthesizePropertyNameAttribute()
		{
			this.parenthesis = false;
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x00024464 File Offset: 0x00022664
		public ParenthesizePropertyNameAttribute(bool needParenthesis)
		{
			this.parenthesis = needParenthesis;
		}

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000E10 RID: 3600 RVA: 0x00024480 File Offset: 0x00022680
		public bool NeedParenthesis
		{
			get
			{
				return this.parenthesis;
			}
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x00024488 File Offset: 0x00022688
		public override bool Equals(object o)
		{
			return o is ParenthesizePropertyNameAttribute && (o == this || ((ParenthesizePropertyNameAttribute)o).NeedParenthesis == this.parenthesis);
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x000244B4 File Offset: 0x000226B4
		public override int GetHashCode()
		{
			return this.parenthesis.GetHashCode();
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x000244C4 File Offset: 0x000226C4
		public override bool IsDefaultAttribute()
		{
			return this.parenthesis == ParenthesizePropertyNameAttribute.Default.NeedParenthesis;
		}

		// Token: 0x040003FA RID: 1018
		private bool parenthesis;

		// Token: 0x040003FB RID: 1019
		public static readonly ParenthesizePropertyNameAttribute Default = new ParenthesizePropertyNameAttribute();
	}
}
