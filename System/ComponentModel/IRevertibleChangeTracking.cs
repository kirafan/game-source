﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200016B RID: 363
	public interface IRevertibleChangeTracking : IChangeTracking
	{
		// Token: 0x06000CC8 RID: 3272
		void RejectChanges();
	}
}
