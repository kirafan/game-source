﻿using System;

namespace System.ComponentModel
{
	// Token: 0x0200013D RID: 317
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
	public sealed class DesignTimeVisibleAttribute : Attribute
	{
		// Token: 0x06000BC6 RID: 3014 RVA: 0x0001EEBC File Offset: 0x0001D0BC
		public DesignTimeVisibleAttribute() : this(true)
		{
		}

		// Token: 0x06000BC7 RID: 3015 RVA: 0x0001EEC8 File Offset: 0x0001D0C8
		public DesignTimeVisibleAttribute(bool visible)
		{
			this.visible = visible;
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000BC9 RID: 3017 RVA: 0x0001EEFC File Offset: 0x0001D0FC
		public bool Visible
		{
			get
			{
				return this.visible;
			}
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x0001EF04 File Offset: 0x0001D104
		public override bool Equals(object obj)
		{
			return obj is DesignTimeVisibleAttribute && (obj == this || ((DesignTimeVisibleAttribute)obj).Visible == this.visible);
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x0001EF30 File Offset: 0x0001D130
		public override int GetHashCode()
		{
			return this.visible.GetHashCode();
		}

		// Token: 0x06000BCC RID: 3020 RVA: 0x0001EF40 File Offset: 0x0001D140
		public override bool IsDefaultAttribute()
		{
			return this.visible == DesignTimeVisibleAttribute.Default.Visible;
		}

		// Token: 0x04000352 RID: 850
		private bool visible;

		// Token: 0x04000353 RID: 851
		public static readonly DesignTimeVisibleAttribute Default = new DesignTimeVisibleAttribute(true);

		// Token: 0x04000354 RID: 852
		public static readonly DesignTimeVisibleAttribute No = new DesignTimeVisibleAttribute(false);

		// Token: 0x04000355 RID: 853
		public static readonly DesignTimeVisibleAttribute Yes = new DesignTimeVisibleAttribute(true);
	}
}
