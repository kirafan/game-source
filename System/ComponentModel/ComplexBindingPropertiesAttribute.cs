﻿using System;

namespace System.ComponentModel
{
	// Token: 0x020000DB RID: 219
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class ComplexBindingPropertiesAttribute : Attribute
	{
		// Token: 0x06000958 RID: 2392 RVA: 0x0001B3E0 File Offset: 0x000195E0
		public ComplexBindingPropertiesAttribute(string dataSource, string dataMember)
		{
			this.data_source = dataSource;
			this.data_member = dataMember;
		}

		// Token: 0x06000959 RID: 2393 RVA: 0x0001B3F8 File Offset: 0x000195F8
		public ComplexBindingPropertiesAttribute(string dataSource)
		{
			this.data_source = dataSource;
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x0001B408 File Offset: 0x00019608
		public ComplexBindingPropertiesAttribute()
		{
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600095C RID: 2396 RVA: 0x0001B41C File Offset: 0x0001961C
		public string DataMember
		{
			get
			{
				return this.data_member;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600095D RID: 2397 RVA: 0x0001B424 File Offset: 0x00019624
		public string DataSource
		{
			get
			{
				return this.data_source;
			}
		}

		// Token: 0x0600095E RID: 2398 RVA: 0x0001B42C File Offset: 0x0001962C
		public override bool Equals(object obj)
		{
			ComplexBindingPropertiesAttribute complexBindingPropertiesAttribute = obj as ComplexBindingPropertiesAttribute;
			return complexBindingPropertiesAttribute != null && complexBindingPropertiesAttribute.DataMember == this.data_member && complexBindingPropertiesAttribute.DataSource == this.data_source;
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x0001B474 File Offset: 0x00019674
		public override int GetHashCode()
		{
			int hashCode = (this.data_source + this.data_member).GetHashCode();
			if (hashCode == 0)
			{
				return base.GetHashCode();
			}
			return hashCode;
		}

		// Token: 0x0400027F RID: 639
		private string data_source;

		// Token: 0x04000280 RID: 640
		private string data_member;

		// Token: 0x04000281 RID: 641
		public static readonly ComplexBindingPropertiesAttribute Default = new ComplexBindingPropertiesAttribute();
	}
}
