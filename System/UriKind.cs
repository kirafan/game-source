﻿using System;

namespace System
{
	// Token: 0x020004B8 RID: 1208
	public enum UriKind
	{
		// Token: 0x04001B79 RID: 7033
		RelativeOrAbsolute,
		// Token: 0x04001B7A RID: 7034
		Absolute,
		// Token: 0x04001B7B RID: 7035
		Relative
	}
}
