﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000512 RID: 1298
	// (Invoke) Token: 0x06002CF4 RID: 11508
	public delegate void SerialErrorReceivedEventHandler(object sender, SerialErrorReceivedEventArgs e);
}
