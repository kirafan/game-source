﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000511 RID: 1297
	// (Invoke) Token: 0x06002CF0 RID: 11504
	public delegate void SerialPinChangedEventHandler(object sender, SerialPinChangedEventArgs e);
}
