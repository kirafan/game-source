﻿using System;

namespace System.IO.Ports
{
	// Token: 0x020002A1 RID: 673
	public enum StopBits
	{
		// Token: 0x04000ECC RID: 3788
		None,
		// Token: 0x04000ECD RID: 3789
		One,
		// Token: 0x04000ECE RID: 3790
		Two,
		// Token: 0x04000ECF RID: 3791
		OnePointFive
	}
}
