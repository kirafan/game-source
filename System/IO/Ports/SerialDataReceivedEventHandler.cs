﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000510 RID: 1296
	// (Invoke) Token: 0x06002CEC RID: 11500
	public delegate void SerialDataReceivedEventHandler(object sender, SerialDataReceivedEventArgs e);
}
