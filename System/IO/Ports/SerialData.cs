﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000298 RID: 664
	public enum SerialData
	{
		// Token: 0x04000E94 RID: 3732
		Chars = 1,
		// Token: 0x04000E95 RID: 3733
		Eof
	}
}
