﻿using System;

namespace System.IO.Ports
{
	// Token: 0x0200029C RID: 668
	public class SerialPinChangedEventArgs : EventArgs
	{
		// Token: 0x06001701 RID: 5889 RVA: 0x0003F5C0 File Offset: 0x0003D7C0
		internal SerialPinChangedEventArgs(SerialPinChange eventType)
		{
			this.eventType = eventType;
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06001702 RID: 5890 RVA: 0x0003F5D0 File Offset: 0x0003D7D0
		public SerialPinChange EventType
		{
			get
			{
				return this.eventType;
			}
		}

		// Token: 0x04000EA3 RID: 3747
		private SerialPinChange eventType;
	}
}
