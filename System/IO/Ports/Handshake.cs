﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000295 RID: 661
	public enum Handshake
	{
		// Token: 0x04000E89 RID: 3721
		None,
		// Token: 0x04000E8A RID: 3722
		XOnXOff,
		// Token: 0x04000E8B RID: 3723
		RequestToSend,
		// Token: 0x04000E8C RID: 3724
		RequestToSendXOnXOff
	}
}
