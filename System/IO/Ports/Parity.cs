﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000297 RID: 663
	public enum Parity
	{
		// Token: 0x04000E8E RID: 3726
		None,
		// Token: 0x04000E8F RID: 3727
		Odd,
		// Token: 0x04000E90 RID: 3728
		Even,
		// Token: 0x04000E91 RID: 3729
		Mark,
		// Token: 0x04000E92 RID: 3730
		Space
	}
}
