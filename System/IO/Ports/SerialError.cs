﻿using System;

namespace System.IO.Ports
{
	// Token: 0x02000299 RID: 665
	public enum SerialError
	{
		// Token: 0x04000E97 RID: 3735
		RXOver = 1,
		// Token: 0x04000E98 RID: 3736
		Overrun,
		// Token: 0x04000E99 RID: 3737
		RXParity = 4,
		// Token: 0x04000E9A RID: 3738
		Frame = 8,
		// Token: 0x04000E9B RID: 3739
		TXFull = 256
	}
}
