﻿using System;

namespace System.IO.Ports
{
	// Token: 0x0200029B RID: 667
	public enum SerialPinChange
	{
		// Token: 0x04000E9E RID: 3742
		CtsChanged = 8,
		// Token: 0x04000E9F RID: 3743
		DsrChanged = 16,
		// Token: 0x04000EA0 RID: 3744
		CDChanged = 32,
		// Token: 0x04000EA1 RID: 3745
		Break = 64,
		// Token: 0x04000EA2 RID: 3746
		Ring = 256
	}
}
