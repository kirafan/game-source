﻿using System;

namespace System.IO
{
	// Token: 0x02000294 RID: 660
	[Flags]
	public enum NotifyFilters
	{
		// Token: 0x04000E80 RID: 3712
		Attributes = 4,
		// Token: 0x04000E81 RID: 3713
		CreationTime = 64,
		// Token: 0x04000E82 RID: 3714
		DirectoryName = 2,
		// Token: 0x04000E83 RID: 3715
		FileName = 1,
		// Token: 0x04000E84 RID: 3716
		LastAccess = 32,
		// Token: 0x04000E85 RID: 3717
		LastWrite = 16,
		// Token: 0x04000E86 RID: 3718
		Security = 256,
		// Token: 0x04000E87 RID: 3719
		Size = 8
	}
}
