﻿using System;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000289 RID: 649
	[Serializable]
	public class InternalBufferOverflowException : SystemException
	{
		// Token: 0x060016C9 RID: 5833 RVA: 0x0003E9EC File Offset: 0x0003CBEC
		public InternalBufferOverflowException() : base("Internal buffer overflow occurred.")
		{
		}

		// Token: 0x060016CA RID: 5834 RVA: 0x0003E9FC File Offset: 0x0003CBFC
		public InternalBufferOverflowException(string message) : base(message)
		{
		}

		// Token: 0x060016CB RID: 5835 RVA: 0x0003EA08 File Offset: 0x0003CC08
		protected InternalBufferOverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060016CC RID: 5836 RVA: 0x0003EA14 File Offset: 0x0003CC14
		public InternalBufferOverflowException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
