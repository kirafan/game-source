﻿using System;

namespace System.IO
{
	// Token: 0x020002AB RID: 683
	[Flags]
	public enum WatcherChangeTypes
	{
		// Token: 0x04000F23 RID: 3875
		All = 15,
		// Token: 0x04000F24 RID: 3876
		Changed = 4,
		// Token: 0x04000F25 RID: 3877
		Created = 1,
		// Token: 0x04000F26 RID: 3878
		Deleted = 2,
		// Token: 0x04000F27 RID: 3879
		Renamed = 8
	}
}
