﻿using System;

namespace System.IO
{
	// Token: 0x02000278 RID: 632
	public class ErrorEventArgs : EventArgs
	{
		// Token: 0x06001655 RID: 5717 RVA: 0x0003C550 File Offset: 0x0003A750
		public ErrorEventArgs(Exception exception)
		{
			this.exception = exception;
		}

		// Token: 0x06001656 RID: 5718 RVA: 0x0003C560 File Offset: 0x0003A760
		public virtual Exception GetException()
		{
			return this.exception;
		}

		// Token: 0x04000705 RID: 1797
		private Exception exception;
	}
}
