﻿using System;

namespace System.IO
{
	// Token: 0x020002A6 RID: 678
	public class RenamedEventArgs : FileSystemEventArgs
	{
		// Token: 0x060017B1 RID: 6065 RVA: 0x000410C8 File Offset: 0x0003F2C8
		public RenamedEventArgs(WatcherChangeTypes changeType, string directory, string name, string oldName) : base(changeType, directory, name)
		{
			this.oldName = oldName;
			this.oldFullPath = Path.Combine(directory, oldName);
		}

		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x060017B2 RID: 6066 RVA: 0x000410EC File Offset: 0x0003F2EC
		public string OldFullPath
		{
			get
			{
				return this.oldFullPath;
			}
		}

		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x060017B3 RID: 6067 RVA: 0x000410F4 File Offset: 0x0003F2F4
		public string OldName
		{
			get
			{
				return this.oldName;
			}
		}

		// Token: 0x04000F0D RID: 3853
		private string oldName;

		// Token: 0x04000F0E RID: 3854
		private string oldFullPath;
	}
}
