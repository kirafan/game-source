﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000274 RID: 628
	public class GZipStream : Stream
	{
		// Token: 0x06001636 RID: 5686 RVA: 0x0003BA54 File Offset: 0x00039C54
		public GZipStream(Stream compressedStream, CompressionMode mode) : this(compressedStream, mode, false)
		{
		}

		// Token: 0x06001637 RID: 5687 RVA: 0x0003BA60 File Offset: 0x00039C60
		public GZipStream(Stream compressedStream, CompressionMode mode, bool leaveOpen)
		{
			this.deflateStream = new DeflateStream(compressedStream, mode, leaveOpen, true);
		}

		// Token: 0x06001638 RID: 5688 RVA: 0x0003BA78 File Offset: 0x00039C78
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.deflateStream.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06001639 RID: 5689 RVA: 0x0003BA94 File Offset: 0x00039C94
		public override int Read(byte[] dest, int dest_offset, int count)
		{
			return this.deflateStream.Read(dest, dest_offset, count);
		}

		// Token: 0x0600163A RID: 5690 RVA: 0x0003BAA4 File Offset: 0x00039CA4
		public override void Write(byte[] src, int src_offset, int count)
		{
			this.deflateStream.Write(src, src_offset, count);
		}

		// Token: 0x0600163B RID: 5691 RVA: 0x0003BAB4 File Offset: 0x00039CB4
		public override void Flush()
		{
			this.deflateStream.Flush();
		}

		// Token: 0x0600163C RID: 5692 RVA: 0x0003BAC4 File Offset: 0x00039CC4
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.deflateStream.Seek(offset, origin);
		}

		// Token: 0x0600163D RID: 5693 RVA: 0x0003BAD4 File Offset: 0x00039CD4
		public override void SetLength(long value)
		{
			this.deflateStream.SetLength(value);
		}

		// Token: 0x0600163E RID: 5694 RVA: 0x0003BAE4 File Offset: 0x00039CE4
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			return this.deflateStream.BeginRead(buffer, offset, count, cback, state);
		}

		// Token: 0x0600163F RID: 5695 RVA: 0x0003BAF8 File Offset: 0x00039CF8
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback cback, object state)
		{
			return this.deflateStream.BeginWrite(buffer, offset, count, cback, state);
		}

		// Token: 0x06001640 RID: 5696 RVA: 0x0003BB0C File Offset: 0x00039D0C
		public override int EndRead(IAsyncResult async_result)
		{
			return this.deflateStream.EndRead(async_result);
		}

		// Token: 0x06001641 RID: 5697 RVA: 0x0003BB1C File Offset: 0x00039D1C
		public override void EndWrite(IAsyncResult async_result)
		{
			this.deflateStream.EndWrite(async_result);
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06001642 RID: 5698 RVA: 0x0003BB2C File Offset: 0x00039D2C
		public Stream BaseStream
		{
			get
			{
				return this.deflateStream.BaseStream;
			}
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06001643 RID: 5699 RVA: 0x0003BB3C File Offset: 0x00039D3C
		public override bool CanRead
		{
			get
			{
				return this.deflateStream.CanRead;
			}
		}

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001644 RID: 5700 RVA: 0x0003BB4C File Offset: 0x00039D4C
		public override bool CanSeek
		{
			get
			{
				return this.deflateStream.CanSeek;
			}
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001645 RID: 5701 RVA: 0x0003BB5C File Offset: 0x00039D5C
		public override bool CanWrite
		{
			get
			{
				return this.deflateStream.CanWrite;
			}
		}

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001646 RID: 5702 RVA: 0x0003BB6C File Offset: 0x00039D6C
		public override long Length
		{
			get
			{
				return this.deflateStream.Length;
			}
		}

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001647 RID: 5703 RVA: 0x0003BB7C File Offset: 0x00039D7C
		// (set) Token: 0x06001648 RID: 5704 RVA: 0x0003BB8C File Offset: 0x00039D8C
		public override long Position
		{
			get
			{
				return this.deflateStream.Position;
			}
			set
			{
				this.deflateStream.Position = value;
			}
		}

		// Token: 0x040006F3 RID: 1779
		private DeflateStream deflateStream;
	}
}
