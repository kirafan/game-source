﻿using System;

namespace System.IO.Compression
{
	// Token: 0x02000272 RID: 626
	public enum CompressionMode
	{
		// Token: 0x040006E7 RID: 1767
		Decompress,
		// Token: 0x040006E8 RID: 1768
		Compress
	}
}
