﻿using System;

namespace System.IO
{
	// Token: 0x0200050E RID: 1294
	// (Invoke) Token: 0x06002CE4 RID: 11492
	public delegate void ErrorEventHandler(object sender, ErrorEventArgs e);
}
