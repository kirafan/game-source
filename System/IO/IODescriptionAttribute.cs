﻿using System;
using System.ComponentModel;

namespace System.IO
{
	// Token: 0x0200028B RID: 651
	[AttributeUsage(AttributeTargets.All)]
	public class IODescriptionAttribute : System.ComponentModel.DescriptionAttribute
	{
		// Token: 0x060016D1 RID: 5841 RVA: 0x0003EA78 File Offset: 0x0003CC78
		public IODescriptionAttribute(string description) : base(description)
		{
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x060016D2 RID: 5842 RVA: 0x0003EA84 File Offset: 0x0003CC84
		public override string Description
		{
			get
			{
				return base.DescriptionValue;
			}
		}
	}
}
