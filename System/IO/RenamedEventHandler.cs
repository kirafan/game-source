﻿using System;

namespace System.IO
{
	// Token: 0x02000513 RID: 1299
	// (Invoke) Token: 0x06002CF8 RID: 11512
	public delegate void RenamedEventHandler(object sender, RenamedEventArgs e);
}
