﻿using System;

namespace System.IO
{
	// Token: 0x0200050F RID: 1295
	// (Invoke) Token: 0x06002CE8 RID: 11496
	public delegate void FileSystemEventHandler(object sender, FileSystemEventArgs e);
}
