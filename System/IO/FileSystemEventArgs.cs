﻿using System;

namespace System.IO
{
	// Token: 0x02000280 RID: 640
	public class FileSystemEventArgs : EventArgs
	{
		// Token: 0x06001675 RID: 5749 RVA: 0x0003CF9C File Offset: 0x0003B19C
		public FileSystemEventArgs(WatcherChangeTypes changeType, string directory, string name)
		{
			this.changeType = changeType;
			this.directory = directory;
			this.name = name;
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x0003CFBC File Offset: 0x0003B1BC
		internal void SetName(string name)
		{
			this.name = name;
		}

		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x06001677 RID: 5751 RVA: 0x0003CFC8 File Offset: 0x0003B1C8
		public WatcherChangeTypes ChangeType
		{
			get
			{
				return this.changeType;
			}
		}

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x06001678 RID: 5752 RVA: 0x0003CFD0 File Offset: 0x0003B1D0
		public string FullPath
		{
			get
			{
				return Path.Combine(this.directory, this.name);
			}
		}

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06001679 RID: 5753 RVA: 0x0003CFE4 File Offset: 0x0003B1E4
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x0400072A RID: 1834
		private WatcherChangeTypes changeType;

		// Token: 0x0400072B RID: 1835
		private string directory;

		// Token: 0x0400072C RID: 1836
		private string name;
	}
}
