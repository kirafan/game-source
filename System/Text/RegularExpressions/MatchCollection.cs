﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000484 RID: 1156
	[Serializable]
	public class MatchCollection : ICollection, IEnumerable
	{
		// Token: 0x0600294F RID: 10575 RVA: 0x000886D8 File Offset: 0x000868D8
		internal MatchCollection(Match start)
		{
			this.current = start;
			this.list = new ArrayList();
		}

		// Token: 0x17000B7D RID: 2941
		// (get) Token: 0x06002950 RID: 10576 RVA: 0x000886F4 File Offset: 0x000868F4
		public int Count
		{
			get
			{
				return this.FullList.Count;
			}
		}

		// Token: 0x17000B7E RID: 2942
		// (get) Token: 0x06002951 RID: 10577 RVA: 0x00088704 File Offset: 0x00086904
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000B7F RID: 2943
		// (get) Token: 0x06002952 RID: 10578 RVA: 0x00088708 File Offset: 0x00086908
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B80 RID: 2944
		public virtual Match this[int i]
		{
			get
			{
				if (i < 0 || !this.TryToGet(i))
				{
					throw new ArgumentOutOfRangeException("i");
				}
				return (i >= this.list.Count) ? this.current : ((Match)this.list[i]);
			}
		}

		// Token: 0x17000B81 RID: 2945
		// (get) Token: 0x06002954 RID: 10580 RVA: 0x00088764 File Offset: 0x00086964
		public object SyncRoot
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x06002955 RID: 10581 RVA: 0x0008876C File Offset: 0x0008696C
		public void CopyTo(Array array, int index)
		{
			this.FullList.CopyTo(array, index);
		}

		// Token: 0x06002956 RID: 10582 RVA: 0x0008877C File Offset: 0x0008697C
		public IEnumerator GetEnumerator()
		{
			IEnumerator result;
			if (this.current.Success)
			{
				IEnumerator enumerator = new MatchCollection.Enumerator(this);
				result = enumerator;
			}
			else
			{
				result = this.list.GetEnumerator();
			}
			return result;
		}

		// Token: 0x06002957 RID: 10583 RVA: 0x000887B4 File Offset: 0x000869B4
		private bool TryToGet(int i)
		{
			while (i > this.list.Count && this.current.Success)
			{
				this.list.Add(this.current);
				this.current = this.current.NextMatch();
			}
			return i < this.list.Count || this.current.Success;
		}

		// Token: 0x17000B82 RID: 2946
		// (get) Token: 0x06002958 RID: 10584 RVA: 0x0008882C File Offset: 0x00086A2C
		private ICollection FullList
		{
			get
			{
				if (this.TryToGet(2147483647))
				{
					throw new SystemException("too many matches");
				}
				return this.list;
			}
		}

		// Token: 0x040019F4 RID: 6644
		private Match current;

		// Token: 0x040019F5 RID: 6645
		private ArrayList list;

		// Token: 0x02000485 RID: 1157
		private class Enumerator : IEnumerator
		{
			// Token: 0x06002959 RID: 10585 RVA: 0x00088850 File Offset: 0x00086A50
			internal Enumerator(MatchCollection coll)
			{
				this.coll = coll;
				this.index = -1;
			}

			// Token: 0x0600295A RID: 10586 RVA: 0x00088868 File Offset: 0x00086A68
			void IEnumerator.Reset()
			{
				this.index = -1;
			}

			// Token: 0x17000B83 RID: 2947
			// (get) Token: 0x0600295B RID: 10587 RVA: 0x00088874 File Offset: 0x00086A74
			object IEnumerator.Current
			{
				get
				{
					if (this.index < 0)
					{
						throw new InvalidOperationException("'Current' called before 'MoveNext()'");
					}
					if (this.index > this.coll.list.Count)
					{
						throw new SystemException("MatchCollection in invalid state");
					}
					if (this.index == this.coll.list.Count && !this.coll.current.Success)
					{
						throw new InvalidOperationException("'Current' called after 'MoveNext()' returned false");
					}
					return (this.index >= this.coll.list.Count) ? this.coll.current : this.coll.list[this.index];
				}
			}

			// Token: 0x0600295C RID: 10588 RVA: 0x0008893C File Offset: 0x00086B3C
			bool IEnumerator.MoveNext()
			{
				if (this.index > this.coll.list.Count)
				{
					throw new SystemException("MatchCollection in invalid state");
				}
				return (this.index != this.coll.list.Count || this.coll.current.Success) && this.coll.TryToGet(++this.index);
			}

			// Token: 0x040019F6 RID: 6646
			private int index;

			// Token: 0x040019F7 RID: 6647
			private MatchCollection coll;
		}
	}
}
