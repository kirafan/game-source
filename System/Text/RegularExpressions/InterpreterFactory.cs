﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000474 RID: 1140
	internal class InterpreterFactory : IMachineFactory
	{
		// Token: 0x060028AD RID: 10413 RVA: 0x00085234 File Offset: 0x00083434
		public InterpreterFactory(ushort[] pattern)
		{
			this.pattern = pattern;
		}

		// Token: 0x060028AE RID: 10414 RVA: 0x00085244 File Offset: 0x00083444
		public IMachine NewInstance()
		{
			return new Interpreter(this.pattern);
		}

		// Token: 0x17000B56 RID: 2902
		// (get) Token: 0x060028AF RID: 10415 RVA: 0x00085254 File Offset: 0x00083454
		public int GroupCount
		{
			get
			{
				return (int)this.pattern[1];
			}
		}

		// Token: 0x17000B57 RID: 2903
		// (get) Token: 0x060028B0 RID: 10416 RVA: 0x00085260 File Offset: 0x00083460
		// (set) Token: 0x060028B1 RID: 10417 RVA: 0x00085268 File Offset: 0x00083468
		public int Gap
		{
			get
			{
				return this.gap;
			}
			set
			{
				this.gap = value;
			}
		}

		// Token: 0x17000B58 RID: 2904
		// (get) Token: 0x060028B2 RID: 10418 RVA: 0x00085274 File Offset: 0x00083474
		// (set) Token: 0x060028B3 RID: 10419 RVA: 0x0008527C File Offset: 0x0008347C
		public IDictionary Mapping
		{
			get
			{
				return this.mapping;
			}
			set
			{
				this.mapping = value;
			}
		}

		// Token: 0x17000B59 RID: 2905
		// (get) Token: 0x060028B4 RID: 10420 RVA: 0x00085288 File Offset: 0x00083488
		// (set) Token: 0x060028B5 RID: 10421 RVA: 0x00085290 File Offset: 0x00083490
		public string[] NamesMapping
		{
			get
			{
				return this.namesMapping;
			}
			set
			{
				this.namesMapping = value;
			}
		}

		// Token: 0x040019C0 RID: 6592
		private IDictionary mapping;

		// Token: 0x040019C1 RID: 6593
		private ushort[] pattern;

		// Token: 0x040019C2 RID: 6594
		private string[] namesMapping;

		// Token: 0x040019C3 RID: 6595
		private int gap;
	}
}
