﻿using System;
using System.ComponentModel;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200048D RID: 1165
	[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
	[MonoTODO("RegexRunner is not supported by Mono.")]
	public abstract class RegexRunner
	{
		// Token: 0x060029DE RID: 10718 RVA: 0x0008BD0C File Offset: 0x00089F0C
		[MonoTODO]
		protected internal RegexRunner()
		{
		}

		// Token: 0x060029DF RID: 10719
		protected abstract bool FindFirstChar();

		// Token: 0x060029E0 RID: 10720
		protected abstract void Go();

		// Token: 0x060029E1 RID: 10721
		protected abstract void InitTrackCount();

		// Token: 0x060029E2 RID: 10722 RVA: 0x0008BD14 File Offset: 0x00089F14
		[MonoTODO]
		protected void Capture(int capnum, int start, int end)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E3 RID: 10723 RVA: 0x0008BD1C File Offset: 0x00089F1C
		[MonoTODO]
		protected static bool CharInClass(char ch, string charClass)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E4 RID: 10724 RVA: 0x0008BD24 File Offset: 0x00089F24
		[MonoTODO]
		protected static bool CharInSet(char ch, string set, string category)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E5 RID: 10725 RVA: 0x0008BD2C File Offset: 0x00089F2C
		[MonoTODO]
		protected void Crawl(int i)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E6 RID: 10726 RVA: 0x0008BD34 File Offset: 0x00089F34
		[MonoTODO]
		protected int Crawlpos()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E7 RID: 10727 RVA: 0x0008BD3C File Offset: 0x00089F3C
		[MonoTODO]
		protected void DoubleCrawl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E8 RID: 10728 RVA: 0x0008BD44 File Offset: 0x00089F44
		[MonoTODO]
		protected void DoubleStack()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029E9 RID: 10729 RVA: 0x0008BD4C File Offset: 0x00089F4C
		[MonoTODO]
		protected void DoubleTrack()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029EA RID: 10730 RVA: 0x0008BD54 File Offset: 0x00089F54
		[MonoTODO]
		protected void EnsureStorage()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029EB RID: 10731 RVA: 0x0008BD5C File Offset: 0x00089F5C
		[MonoTODO]
		protected bool IsBoundary(int index, int startpos, int endpos)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029EC RID: 10732 RVA: 0x0008BD64 File Offset: 0x00089F64
		[MonoTODO]
		protected bool IsECMABoundary(int index, int startpos, int endpos)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029ED RID: 10733 RVA: 0x0008BD6C File Offset: 0x00089F6C
		[MonoTODO]
		protected bool IsMatched(int cap)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029EE RID: 10734 RVA: 0x0008BD74 File Offset: 0x00089F74
		[MonoTODO]
		protected int MatchIndex(int cap)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029EF RID: 10735 RVA: 0x0008BD7C File Offset: 0x00089F7C
		[MonoTODO]
		protected int MatchLength(int cap)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029F0 RID: 10736 RVA: 0x0008BD84 File Offset: 0x00089F84
		[MonoTODO]
		protected int Popcrawl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029F1 RID: 10737 RVA: 0x0008BD8C File Offset: 0x00089F8C
		[MonoTODO]
		protected void TransferCapture(int capnum, int uncapnum, int start, int end)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029F2 RID: 10738 RVA: 0x0008BD94 File Offset: 0x00089F94
		[MonoTODO]
		protected void Uncapture()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029F3 RID: 10739 RVA: 0x0008BD9C File Offset: 0x00089F9C
		protected internal Match Scan(Regex regex, string text, int textbeg, int textend, int textstart, int prevlen, bool quick)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001A2B RID: 6699
		[MonoTODO]
		protected internal int[] runcrawl;

		// Token: 0x04001A2C RID: 6700
		[MonoTODO]
		protected internal int runcrawlpos;

		// Token: 0x04001A2D RID: 6701
		[MonoTODO]
		protected internal Match runmatch;

		// Token: 0x04001A2E RID: 6702
		[MonoTODO]
		protected internal Regex runregex;

		// Token: 0x04001A2F RID: 6703
		[MonoTODO]
		protected internal int[] runstack;

		// Token: 0x04001A30 RID: 6704
		[MonoTODO]
		protected internal int runstackpos;

		// Token: 0x04001A31 RID: 6705
		[MonoTODO]
		protected internal string runtext;

		// Token: 0x04001A32 RID: 6706
		[MonoTODO]
		protected internal int runtextbeg;

		// Token: 0x04001A33 RID: 6707
		[MonoTODO]
		protected internal int runtextend;

		// Token: 0x04001A34 RID: 6708
		[MonoTODO]
		protected internal int runtextpos;

		// Token: 0x04001A35 RID: 6709
		[MonoTODO]
		protected internal int runtextstart;

		// Token: 0x04001A36 RID: 6710
		[MonoTODO]
		protected internal int[] runtrack;

		// Token: 0x04001A37 RID: 6711
		[MonoTODO]
		protected internal int runtrackcount;

		// Token: 0x04001A38 RID: 6712
		[MonoTODO]
		protected internal int runtrackpos;
	}
}
