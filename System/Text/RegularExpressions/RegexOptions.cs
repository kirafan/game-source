﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200048C RID: 1164
	[Flags]
	public enum RegexOptions
	{
		// Token: 0x04001A21 RID: 6689
		None = 0,
		// Token: 0x04001A22 RID: 6690
		IgnoreCase = 1,
		// Token: 0x04001A23 RID: 6691
		Multiline = 2,
		// Token: 0x04001A24 RID: 6692
		ExplicitCapture = 4,
		// Token: 0x04001A25 RID: 6693
		Compiled = 8,
		// Token: 0x04001A26 RID: 6694
		Singleline = 16,
		// Token: 0x04001A27 RID: 6695
		IgnorePatternWhitespace = 32,
		// Token: 0x04001A28 RID: 6696
		RightToLeft = 64,
		// Token: 0x04001A29 RID: 6697
		ECMAScript = 256,
		// Token: 0x04001A2A RID: 6698
		CultureInvariant = 512
	}
}
