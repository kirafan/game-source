﻿using System;
using System.ComponentModel;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200048E RID: 1166
	[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
	public abstract class RegexRunnerFactory
	{
		// Token: 0x060029F5 RID: 10741
		protected internal abstract RegexRunner CreateInstance();
	}
}
