﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200047B RID: 1147
	[Serializable]
	public class GroupCollection : ICollection, IEnumerable
	{
		// Token: 0x060028F7 RID: 10487 RVA: 0x00085FE8 File Offset: 0x000841E8
		internal GroupCollection(int n, int gap)
		{
			this.list = new Group[n];
			this.gap = gap;
		}

		// Token: 0x17000B60 RID: 2912
		// (get) Token: 0x060028F8 RID: 10488 RVA: 0x00086004 File Offset: 0x00084204
		public int Count
		{
			get
			{
				return this.list.Length;
			}
		}

		// Token: 0x17000B61 RID: 2913
		// (get) Token: 0x060028F9 RID: 10489 RVA: 0x00086010 File Offset: 0x00084210
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000B62 RID: 2914
		// (get) Token: 0x060028FA RID: 10490 RVA: 0x00086014 File Offset: 0x00084214
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B63 RID: 2915
		public Group this[int i]
		{
			get
			{
				if (i >= this.gap)
				{
					Match match = (Match)this.list[0];
					i = ((match != Match.Empty) ? match.Regex.GetGroupIndex(i) : -1);
				}
				return (i >= 0) ? this.list[i] : Group.Fail;
			}
		}

		// Token: 0x060028FC RID: 10492 RVA: 0x00086078 File Offset: 0x00084278
		internal void SetValue(Group g, int i)
		{
			this.list[i] = g;
		}

		// Token: 0x17000B64 RID: 2916
		public Group this[string groupName]
		{
			get
			{
				Match match = (Match)this.list[0];
				if (match != Match.Empty)
				{
					int num = match.Regex.GroupNumberFromName(groupName);
					if (num != -1)
					{
						return this[num];
					}
				}
				return Group.Fail;
			}
		}

		// Token: 0x17000B65 RID: 2917
		// (get) Token: 0x060028FE RID: 10494 RVA: 0x000860CC File Offset: 0x000842CC
		public object SyncRoot
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x060028FF RID: 10495 RVA: 0x000860D4 File Offset: 0x000842D4
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x06002900 RID: 10496 RVA: 0x000860E4 File Offset: 0x000842E4
		public IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x040019CC RID: 6604
		private Group[] list;

		// Token: 0x040019CD RID: 6605
		private int gap;
	}
}
