﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000489 RID: 1161
	[Serializable]
	public class RegexCompilationInfo
	{
		// Token: 0x06002993 RID: 10643 RVA: 0x0008B1F4 File Offset: 0x000893F4
		public RegexCompilationInfo(string pattern, RegexOptions options, string name, string fullnamespace, bool ispublic)
		{
			this.Pattern = pattern;
			this.Options = options;
			this.Name = name;
			this.Namespace = fullnamespace;
			this.IsPublic = ispublic;
		}

		// Token: 0x17000B8A RID: 2954
		// (get) Token: 0x06002994 RID: 10644 RVA: 0x0008B22C File Offset: 0x0008942C
		// (set) Token: 0x06002995 RID: 10645 RVA: 0x0008B234 File Offset: 0x00089434
		public bool IsPublic
		{
			get
			{
				return this.isPublic;
			}
			set
			{
				this.isPublic = value;
			}
		}

		// Token: 0x17000B8B RID: 2955
		// (get) Token: 0x06002996 RID: 10646 RVA: 0x0008B240 File Offset: 0x00089440
		// (set) Token: 0x06002997 RID: 10647 RVA: 0x0008B248 File Offset: 0x00089448
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Name");
				}
				if (value.Length == 0)
				{
					throw new ArgumentException("Name");
				}
				this.name = value;
			}
		}

		// Token: 0x17000B8C RID: 2956
		// (get) Token: 0x06002998 RID: 10648 RVA: 0x0008B284 File Offset: 0x00089484
		// (set) Token: 0x06002999 RID: 10649 RVA: 0x0008B28C File Offset: 0x0008948C
		public string Namespace
		{
			get
			{
				return this.nspace;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Namespace");
				}
				this.nspace = value;
			}
		}

		// Token: 0x17000B8D RID: 2957
		// (get) Token: 0x0600299A RID: 10650 RVA: 0x0008B2A8 File Offset: 0x000894A8
		// (set) Token: 0x0600299B RID: 10651 RVA: 0x0008B2B0 File Offset: 0x000894B0
		public RegexOptions Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		// Token: 0x17000B8E RID: 2958
		// (get) Token: 0x0600299C RID: 10652 RVA: 0x0008B2BC File Offset: 0x000894BC
		// (set) Token: 0x0600299D RID: 10653 RVA: 0x0008B2C4 File Offset: 0x000894C4
		public string Pattern
		{
			get
			{
				return this.pattern;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("pattern");
				}
				this.pattern = value;
			}
		}

		// Token: 0x04001A0A RID: 6666
		private string pattern;

		// Token: 0x04001A0B RID: 6667
		private string name;

		// Token: 0x04001A0C RID: 6668
		private string nspace;

		// Token: 0x04001A0D RID: 6669
		private RegexOptions options;

		// Token: 0x04001A0E RID: 6670
		private bool isPublic;
	}
}
