﻿using System;
using System.Collections;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200046C RID: 1132
	[Serializable]
	public class CaptureCollection : ICollection, IEnumerable
	{
		// Token: 0x06002867 RID: 10343 RVA: 0x0008052C File Offset: 0x0007E72C
		internal CaptureCollection(int n)
		{
			this.list = new Capture[n];
		}

		// Token: 0x17000B4D RID: 2893
		// (get) Token: 0x06002868 RID: 10344 RVA: 0x00080540 File Offset: 0x0007E740
		public int Count
		{
			get
			{
				return this.list.Length;
			}
		}

		// Token: 0x17000B4E RID: 2894
		// (get) Token: 0x06002869 RID: 10345 RVA: 0x0008054C File Offset: 0x0007E74C
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000B4F RID: 2895
		// (get) Token: 0x0600286A RID: 10346 RVA: 0x00080550 File Offset: 0x0007E750
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B50 RID: 2896
		public Capture this[int i]
		{
			get
			{
				if (i < 0 || i >= this.Count)
				{
					throw new ArgumentOutOfRangeException("Index is out of range");
				}
				return this.list[i];
			}
		}

		// Token: 0x0600286C RID: 10348 RVA: 0x00080588 File Offset: 0x0007E788
		internal void SetValue(Capture cap, int i)
		{
			this.list[i] = cap;
		}

		// Token: 0x17000B51 RID: 2897
		// (get) Token: 0x0600286D RID: 10349 RVA: 0x00080594 File Offset: 0x0007E794
		public object SyncRoot
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x0600286E RID: 10350 RVA: 0x0008059C File Offset: 0x0007E79C
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x0600286F RID: 10351 RVA: 0x000805AC File Offset: 0x0007E7AC
		public IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x04001908 RID: 6408
		private Capture[] list;
	}
}
