﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200047C RID: 1148
	[Serializable]
	public class Group : Capture
	{
		// Token: 0x06002901 RID: 10497 RVA: 0x000860F4 File Offset: 0x000842F4
		internal Group(string text, int index, int length, int n_caps) : base(text, index, length)
		{
			this.success = true;
			this.captures = new CaptureCollection(n_caps);
			this.captures.SetValue(this, n_caps - 1);
		}

		// Token: 0x06002902 RID: 10498 RVA: 0x00086124 File Offset: 0x00084324
		internal Group(string text, int index, int length) : base(text, index, length)
		{
			this.success = true;
		}

		// Token: 0x06002903 RID: 10499 RVA: 0x00086138 File Offset: 0x00084338
		internal Group() : base(string.Empty)
		{
			this.success = false;
			this.captures = new CaptureCollection(0);
		}

		// Token: 0x06002905 RID: 10501 RVA: 0x00086164 File Offset: 0x00084364
		[MonoTODO("not thread-safe")]
		public static Group Synchronized(Group inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			return inner;
		}

		// Token: 0x17000B66 RID: 2918
		// (get) Token: 0x06002906 RID: 10502 RVA: 0x00086178 File Offset: 0x00084378
		public CaptureCollection Captures
		{
			get
			{
				return this.captures;
			}
		}

		// Token: 0x17000B67 RID: 2919
		// (get) Token: 0x06002907 RID: 10503 RVA: 0x00086180 File Offset: 0x00084380
		public bool Success
		{
			get
			{
				return this.success;
			}
		}

		// Token: 0x040019CE RID: 6606
		internal static Group Fail = new Group();

		// Token: 0x040019CF RID: 6607
		private bool success;

		// Token: 0x040019D0 RID: 6608
		private CaptureCollection captures;
	}
}
