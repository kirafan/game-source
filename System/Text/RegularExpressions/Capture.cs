﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200046D RID: 1133
	[Serializable]
	public class Capture
	{
		// Token: 0x06002870 RID: 10352 RVA: 0x000805BC File Offset: 0x0007E7BC
		internal Capture(string text) : this(text, 0, 0)
		{
		}

		// Token: 0x06002871 RID: 10353 RVA: 0x000805C8 File Offset: 0x0007E7C8
		internal Capture(string text, int index, int length)
		{
			this.text = text;
			this.index = index;
			this.length = length;
		}

		// Token: 0x17000B52 RID: 2898
		// (get) Token: 0x06002872 RID: 10354 RVA: 0x000805E8 File Offset: 0x0007E7E8
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x17000B53 RID: 2899
		// (get) Token: 0x06002873 RID: 10355 RVA: 0x000805F0 File Offset: 0x0007E7F0
		public int Length
		{
			get
			{
				return this.length;
			}
		}

		// Token: 0x17000B54 RID: 2900
		// (get) Token: 0x06002874 RID: 10356 RVA: 0x000805F8 File Offset: 0x0007E7F8
		public string Value
		{
			get
			{
				return (this.text != null) ? this.text.Substring(this.index, this.length) : string.Empty;
			}
		}

		// Token: 0x06002875 RID: 10357 RVA: 0x00080634 File Offset: 0x0007E834
		public override string ToString()
		{
			return this.Value;
		}

		// Token: 0x17000B55 RID: 2901
		// (get) Token: 0x06002876 RID: 10358 RVA: 0x0008063C File Offset: 0x0007E83C
		internal string Text
		{
			get
			{
				return this.text;
			}
		}

		// Token: 0x04001909 RID: 6409
		internal int index;

		// Token: 0x0400190A RID: 6410
		internal int length;

		// Token: 0x0400190B RID: 6411
		internal string text;
	}
}
