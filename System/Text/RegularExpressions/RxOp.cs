﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000495 RID: 1173
	internal enum RxOp : byte
	{
		// Token: 0x04001A5A RID: 6746
		Info,
		// Token: 0x04001A5B RID: 6747
		False,
		// Token: 0x04001A5C RID: 6748
		True,
		// Token: 0x04001A5D RID: 6749
		AnyPosition,
		// Token: 0x04001A5E RID: 6750
		StartOfString,
		// Token: 0x04001A5F RID: 6751
		StartOfLine,
		// Token: 0x04001A60 RID: 6752
		StartOfScan,
		// Token: 0x04001A61 RID: 6753
		EndOfString,
		// Token: 0x04001A62 RID: 6754
		EndOfLine,
		// Token: 0x04001A63 RID: 6755
		End,
		// Token: 0x04001A64 RID: 6756
		WordBoundary,
		// Token: 0x04001A65 RID: 6757
		NoWordBoundary,
		// Token: 0x04001A66 RID: 6758
		String,
		// Token: 0x04001A67 RID: 6759
		StringIgnoreCase,
		// Token: 0x04001A68 RID: 6760
		StringReverse,
		// Token: 0x04001A69 RID: 6761
		StringIgnoreCaseReverse,
		// Token: 0x04001A6A RID: 6762
		UnicodeString,
		// Token: 0x04001A6B RID: 6763
		UnicodeStringIgnoreCase,
		// Token: 0x04001A6C RID: 6764
		UnicodeStringReverse,
		// Token: 0x04001A6D RID: 6765
		UnicodeStringIgnoreCaseReverse,
		// Token: 0x04001A6E RID: 6766
		Char,
		// Token: 0x04001A6F RID: 6767
		NoChar,
		// Token: 0x04001A70 RID: 6768
		CharIgnoreCase,
		// Token: 0x04001A71 RID: 6769
		NoCharIgnoreCase,
		// Token: 0x04001A72 RID: 6770
		CharReverse,
		// Token: 0x04001A73 RID: 6771
		NoCharReverse,
		// Token: 0x04001A74 RID: 6772
		CharIgnoreCaseReverse,
		// Token: 0x04001A75 RID: 6773
		NoCharIgnoreCaseReverse,
		// Token: 0x04001A76 RID: 6774
		Range,
		// Token: 0x04001A77 RID: 6775
		NoRange,
		// Token: 0x04001A78 RID: 6776
		RangeIgnoreCase,
		// Token: 0x04001A79 RID: 6777
		NoRangeIgnoreCase,
		// Token: 0x04001A7A RID: 6778
		RangeReverse,
		// Token: 0x04001A7B RID: 6779
		NoRangeReverse,
		// Token: 0x04001A7C RID: 6780
		RangeIgnoreCaseReverse,
		// Token: 0x04001A7D RID: 6781
		NoRangeIgnoreCaseReverse,
		// Token: 0x04001A7E RID: 6782
		Bitmap,
		// Token: 0x04001A7F RID: 6783
		NoBitmap,
		// Token: 0x04001A80 RID: 6784
		BitmapIgnoreCase,
		// Token: 0x04001A81 RID: 6785
		NoBitmapIgnoreCase,
		// Token: 0x04001A82 RID: 6786
		BitmapReverse,
		// Token: 0x04001A83 RID: 6787
		NoBitmapReverse,
		// Token: 0x04001A84 RID: 6788
		BitmapIgnoreCaseReverse,
		// Token: 0x04001A85 RID: 6789
		NoBitmapIgnoreCaseReverse,
		// Token: 0x04001A86 RID: 6790
		UnicodeChar,
		// Token: 0x04001A87 RID: 6791
		NoUnicodeChar,
		// Token: 0x04001A88 RID: 6792
		UnicodeCharIgnoreCase,
		// Token: 0x04001A89 RID: 6793
		NoUnicodeCharIgnoreCase,
		// Token: 0x04001A8A RID: 6794
		UnicodeCharReverse,
		// Token: 0x04001A8B RID: 6795
		NoUnicodeCharReverse,
		// Token: 0x04001A8C RID: 6796
		UnicodeCharIgnoreCaseReverse,
		// Token: 0x04001A8D RID: 6797
		NoUnicodeCharIgnoreCaseReverse,
		// Token: 0x04001A8E RID: 6798
		UnicodeRange,
		// Token: 0x04001A8F RID: 6799
		NoUnicodeRange,
		// Token: 0x04001A90 RID: 6800
		UnicodeRangeIgnoreCase,
		// Token: 0x04001A91 RID: 6801
		NoUnicodeRangeIgnoreCase,
		// Token: 0x04001A92 RID: 6802
		UnicodeRangeReverse,
		// Token: 0x04001A93 RID: 6803
		NoUnicodeRangeReverse,
		// Token: 0x04001A94 RID: 6804
		UnicodeRangeIgnoreCaseReverse,
		// Token: 0x04001A95 RID: 6805
		NoUnicodeRangeIgnoreCaseReverse,
		// Token: 0x04001A96 RID: 6806
		UnicodeBitmap,
		// Token: 0x04001A97 RID: 6807
		NoUnicodeBitmap,
		// Token: 0x04001A98 RID: 6808
		UnicodeBitmapIgnoreCase,
		// Token: 0x04001A99 RID: 6809
		NoUnicodeBitmapIgnoreCase,
		// Token: 0x04001A9A RID: 6810
		UnicodeBitmapReverse,
		// Token: 0x04001A9B RID: 6811
		NoUnicodeBitmapReverse,
		// Token: 0x04001A9C RID: 6812
		UnicodeBitmapIgnoreCaseReverse,
		// Token: 0x04001A9D RID: 6813
		NoUnicodeBitmapIgnoreCaseReverse,
		// Token: 0x04001A9E RID: 6814
		CategoryAny,
		// Token: 0x04001A9F RID: 6815
		NoCategoryAny,
		// Token: 0x04001AA0 RID: 6816
		CategoryAnyReverse,
		// Token: 0x04001AA1 RID: 6817
		NoCategoryAnyReverse,
		// Token: 0x04001AA2 RID: 6818
		CategoryAnySingleline,
		// Token: 0x04001AA3 RID: 6819
		NoCategoryAnySingleline,
		// Token: 0x04001AA4 RID: 6820
		CategoryAnySinglelineReverse,
		// Token: 0x04001AA5 RID: 6821
		NoCategoryAnySinglelineReverse,
		// Token: 0x04001AA6 RID: 6822
		CategoryDigit,
		// Token: 0x04001AA7 RID: 6823
		NoCategoryDigit,
		// Token: 0x04001AA8 RID: 6824
		CategoryDigitReverse,
		// Token: 0x04001AA9 RID: 6825
		NoCategoryDigitReverse,
		// Token: 0x04001AAA RID: 6826
		CategoryWord,
		// Token: 0x04001AAB RID: 6827
		NoCategoryWord,
		// Token: 0x04001AAC RID: 6828
		CategoryWordReverse,
		// Token: 0x04001AAD RID: 6829
		NoCategoryWordReverse,
		// Token: 0x04001AAE RID: 6830
		CategoryWhiteSpace,
		// Token: 0x04001AAF RID: 6831
		NoCategoryWhiteSpace,
		// Token: 0x04001AB0 RID: 6832
		CategoryWhiteSpaceReverse,
		// Token: 0x04001AB1 RID: 6833
		NoCategoryWhiteSpaceReverse,
		// Token: 0x04001AB2 RID: 6834
		CategoryEcmaWord,
		// Token: 0x04001AB3 RID: 6835
		NoCategoryEcmaWord,
		// Token: 0x04001AB4 RID: 6836
		CategoryEcmaWordReverse,
		// Token: 0x04001AB5 RID: 6837
		NoCategoryEcmaWordReverse,
		// Token: 0x04001AB6 RID: 6838
		CategoryEcmaWhiteSpace,
		// Token: 0x04001AB7 RID: 6839
		NoCategoryEcmaWhiteSpace,
		// Token: 0x04001AB8 RID: 6840
		CategoryEcmaWhiteSpaceReverse,
		// Token: 0x04001AB9 RID: 6841
		NoCategoryEcmaWhiteSpaceReverse,
		// Token: 0x04001ABA RID: 6842
		CategoryUnicode,
		// Token: 0x04001ABB RID: 6843
		NoCategoryUnicode,
		// Token: 0x04001ABC RID: 6844
		CategoryUnicodeReverse,
		// Token: 0x04001ABD RID: 6845
		NoCategoryUnicodeReverse,
		// Token: 0x04001ABE RID: 6846
		CategoryUnicodeLetter,
		// Token: 0x04001ABF RID: 6847
		NoCategoryUnicodeLetter,
		// Token: 0x04001AC0 RID: 6848
		CategoryUnicodeLetterReverse,
		// Token: 0x04001AC1 RID: 6849
		NoCategoryUnicodeLetterReverse,
		// Token: 0x04001AC2 RID: 6850
		CategoryUnicodeMark,
		// Token: 0x04001AC3 RID: 6851
		NoCategoryUnicodeMark,
		// Token: 0x04001AC4 RID: 6852
		CategoryUnicodeMarkReverse,
		// Token: 0x04001AC5 RID: 6853
		NoCategoryUnicodeMarkReverse,
		// Token: 0x04001AC6 RID: 6854
		CategoryUnicodeNumber,
		// Token: 0x04001AC7 RID: 6855
		NoCategoryUnicodeNumber,
		// Token: 0x04001AC8 RID: 6856
		CategoryUnicodeNumberReverse,
		// Token: 0x04001AC9 RID: 6857
		NoCategoryUnicodeNumberReverse,
		// Token: 0x04001ACA RID: 6858
		CategoryUnicodeSeparator,
		// Token: 0x04001ACB RID: 6859
		NoCategoryUnicodeSeparator,
		// Token: 0x04001ACC RID: 6860
		CategoryUnicodeSeparatorReverse,
		// Token: 0x04001ACD RID: 6861
		NoCategoryUnicodeSeparatorReverse,
		// Token: 0x04001ACE RID: 6862
		CategoryUnicodePunctuation,
		// Token: 0x04001ACF RID: 6863
		NoCategoryUnicodePunctuation,
		// Token: 0x04001AD0 RID: 6864
		CategoryUnicodePunctuationReverse,
		// Token: 0x04001AD1 RID: 6865
		NoCategoryUnicodePunctuationReverse,
		// Token: 0x04001AD2 RID: 6866
		CategoryUnicodeSymbol,
		// Token: 0x04001AD3 RID: 6867
		NoCategoryUnicodeSymbol,
		// Token: 0x04001AD4 RID: 6868
		CategoryUnicodeSymbolReverse,
		// Token: 0x04001AD5 RID: 6869
		NoCategoryUnicodeSymbolReverse,
		// Token: 0x04001AD6 RID: 6870
		CategoryUnicodeSpecials,
		// Token: 0x04001AD7 RID: 6871
		NoCategoryUnicodeSpecials,
		// Token: 0x04001AD8 RID: 6872
		CategoryUnicodeSpecialsReverse,
		// Token: 0x04001AD9 RID: 6873
		NoCategoryUnicodeSpecialsReverse,
		// Token: 0x04001ADA RID: 6874
		CategoryUnicodeOther,
		// Token: 0x04001ADB RID: 6875
		NoCategoryUnicodeOther,
		// Token: 0x04001ADC RID: 6876
		CategoryUnicodeOtherReverse,
		// Token: 0x04001ADD RID: 6877
		NoCategoryUnicodeOtherReverse,
		// Token: 0x04001ADE RID: 6878
		CategoryGeneral,
		// Token: 0x04001ADF RID: 6879
		NoCategoryGeneral,
		// Token: 0x04001AE0 RID: 6880
		CategoryGeneralReverse,
		// Token: 0x04001AE1 RID: 6881
		NoCategoryGeneralReverse,
		// Token: 0x04001AE2 RID: 6882
		Reference,
		// Token: 0x04001AE3 RID: 6883
		ReferenceIgnoreCase,
		// Token: 0x04001AE4 RID: 6884
		ReferenceReverse,
		// Token: 0x04001AE5 RID: 6885
		ReferenceIgnoreCaseReverse,
		// Token: 0x04001AE6 RID: 6886
		OpenGroup,
		// Token: 0x04001AE7 RID: 6887
		CloseGroup,
		// Token: 0x04001AE8 RID: 6888
		BalanceStart,
		// Token: 0x04001AE9 RID: 6889
		Balance,
		// Token: 0x04001AEA RID: 6890
		IfDefined,
		// Token: 0x04001AEB RID: 6891
		Jump,
		// Token: 0x04001AEC RID: 6892
		SubExpression,
		// Token: 0x04001AED RID: 6893
		Test,
		// Token: 0x04001AEE RID: 6894
		Branch,
		// Token: 0x04001AEF RID: 6895
		TestCharGroup,
		// Token: 0x04001AF0 RID: 6896
		Anchor,
		// Token: 0x04001AF1 RID: 6897
		AnchorReverse,
		// Token: 0x04001AF2 RID: 6898
		Repeat,
		// Token: 0x04001AF3 RID: 6899
		RepeatLazy,
		// Token: 0x04001AF4 RID: 6900
		Until,
		// Token: 0x04001AF5 RID: 6901
		FastRepeat,
		// Token: 0x04001AF6 RID: 6902
		FastRepeatLazy,
		// Token: 0x04001AF7 RID: 6903
		RepeatInfinite,
		// Token: 0x04001AF8 RID: 6904
		RepeatInfiniteLazy
	}
}
