﻿using System;

namespace System.Text.RegularExpressions.Syntax
{
	// Token: 0x020004A4 RID: 1188
	internal class Literal : Expression
	{
		// Token: 0x06002AA9 RID: 10921 RVA: 0x00092EF0 File Offset: 0x000910F0
		public Literal(string str, bool ignore)
		{
			this.str = str;
			this.ignore = ignore;
		}

		// Token: 0x17000BB7 RID: 2999
		// (get) Token: 0x06002AAA RID: 10922 RVA: 0x00092F08 File Offset: 0x00091108
		// (set) Token: 0x06002AAB RID: 10923 RVA: 0x00092F10 File Offset: 0x00091110
		public string String
		{
			get
			{
				return this.str;
			}
			set
			{
				this.str = value;
			}
		}

		// Token: 0x17000BB8 RID: 3000
		// (get) Token: 0x06002AAC RID: 10924 RVA: 0x00092F1C File Offset: 0x0009111C
		// (set) Token: 0x06002AAD RID: 10925 RVA: 0x00092F24 File Offset: 0x00091124
		public bool IgnoreCase
		{
			get
			{
				return this.ignore;
			}
			set
			{
				this.ignore = value;
			}
		}

		// Token: 0x06002AAE RID: 10926 RVA: 0x00092F30 File Offset: 0x00091130
		public static void CompileLiteral(string str, ICompiler cmp, bool ignore, bool reverse)
		{
			if (str.Length == 0)
			{
				return;
			}
			if (str.Length == 1)
			{
				cmp.EmitCharacter(str[0], false, ignore, reverse);
			}
			else
			{
				cmp.EmitString(str, ignore, reverse);
			}
		}

		// Token: 0x06002AAF RID: 10927 RVA: 0x00092F74 File Offset: 0x00091174
		public override void Compile(ICompiler cmp, bool reverse)
		{
			Literal.CompileLiteral(this.str, cmp, this.ignore, reverse);
		}

		// Token: 0x06002AB0 RID: 10928 RVA: 0x00092F8C File Offset: 0x0009118C
		public override void GetWidth(out int min, out int max)
		{
			min = (max = this.str.Length);
		}

		// Token: 0x06002AB1 RID: 10929 RVA: 0x00092FAC File Offset: 0x000911AC
		public override AnchorInfo GetAnchorInfo(bool reverse)
		{
			return new AnchorInfo(this, 0, this.str.Length, this.str, this.ignore);
		}

		// Token: 0x06002AB2 RID: 10930 RVA: 0x00092FCC File Offset: 0x000911CC
		public override bool IsComplex()
		{
			return false;
		}

		// Token: 0x04001B0A RID: 6922
		private string str;

		// Token: 0x04001B0B RID: 6923
		private bool ignore;
	}
}
