﻿using System;

namespace System.Text.RegularExpressions.Syntax
{
	// Token: 0x0200049E RID: 1182
	internal class NonBacktrackingGroup : Group
	{
		// Token: 0x06002A81 RID: 10881 RVA: 0x0009283C File Offset: 0x00090A3C
		public override void Compile(ICompiler cmp, bool reverse)
		{
			LinkRef linkRef = cmp.NewLink();
			cmp.EmitSub(linkRef);
			base.Compile(cmp, reverse);
			cmp.EmitTrue();
			cmp.ResolveLink(linkRef);
		}

		// Token: 0x06002A82 RID: 10882 RVA: 0x0009286C File Offset: 0x00090A6C
		public override bool IsComplex()
		{
			return true;
		}
	}
}
