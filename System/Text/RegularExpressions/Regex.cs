﻿using System;
using System.Collections;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;
using System.Text.RegularExpressions.Syntax;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200048A RID: 1162
	[Serializable]
	public class Regex : ISerializable
	{
		// Token: 0x0600299E RID: 10654 RVA: 0x0008B2E0 File Offset: 0x000894E0
		protected Regex()
		{
		}

		// Token: 0x0600299F RID: 10655 RVA: 0x0008B2E8 File Offset: 0x000894E8
		public Regex(string pattern) : this(pattern, RegexOptions.None)
		{
		}

		// Token: 0x060029A0 RID: 10656 RVA: 0x0008B2F4 File Offset: 0x000894F4
		public Regex(string pattern, RegexOptions options)
		{
			if (pattern == null)
			{
				throw new ArgumentNullException("pattern");
			}
			Regex.validate_options(options);
			this.pattern = pattern;
			this.roptions = options;
			this.Init();
		}

		// Token: 0x060029A1 RID: 10657 RVA: 0x0008B328 File Offset: 0x00089528
		protected Regex(SerializationInfo info, StreamingContext context) : this(info.GetString("pattern"), (RegexOptions)((int)info.GetValue("options", typeof(RegexOptions))))
		{
		}

		// Token: 0x060029A3 RID: 10659 RVA: 0x0008B380 File Offset: 0x00089580
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("pattern", this.ToString(), typeof(string));
			info.AddValue("options", this.Options, typeof(RegexOptions));
		}

		// Token: 0x060029A4 RID: 10660 RVA: 0x0008B3C8 File Offset: 0x000895C8
		[MonoTODO]
		public static void CompileToAssembly(RegexCompilationInfo[] regexes, AssemblyName aname)
		{
			Regex.CompileToAssembly(regexes, aname, new CustomAttributeBuilder[0], null);
		}

		// Token: 0x060029A5 RID: 10661 RVA: 0x0008B3D8 File Offset: 0x000895D8
		[MonoTODO]
		public static void CompileToAssembly(RegexCompilationInfo[] regexes, AssemblyName aname, CustomAttributeBuilder[] attribs)
		{
			Regex.CompileToAssembly(regexes, aname, attribs, null);
		}

		// Token: 0x060029A6 RID: 10662 RVA: 0x0008B3E4 File Offset: 0x000895E4
		[MonoTODO]
		public static void CompileToAssembly(RegexCompilationInfo[] regexes, AssemblyName aname, CustomAttributeBuilder[] attribs, string resourceFile)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060029A7 RID: 10663 RVA: 0x0008B3EC File Offset: 0x000895EC
		public static string Escape(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			return Parser.Escape(str);
		}

		// Token: 0x060029A8 RID: 10664 RVA: 0x0008B408 File Offset: 0x00089608
		public static string Unescape(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			return Parser.Unescape(str);
		}

		// Token: 0x060029A9 RID: 10665 RVA: 0x0008B424 File Offset: 0x00089624
		public static bool IsMatch(string input, string pattern)
		{
			return Regex.IsMatch(input, pattern, RegexOptions.None);
		}

		// Token: 0x060029AA RID: 10666 RVA: 0x0008B430 File Offset: 0x00089630
		public static bool IsMatch(string input, string pattern, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.IsMatch(input);
		}

		// Token: 0x060029AB RID: 10667 RVA: 0x0008B44C File Offset: 0x0008964C
		public static Match Match(string input, string pattern)
		{
			return Regex.Match(input, pattern, RegexOptions.None);
		}

		// Token: 0x060029AC RID: 10668 RVA: 0x0008B458 File Offset: 0x00089658
		public static Match Match(string input, string pattern, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.Match(input);
		}

		// Token: 0x060029AD RID: 10669 RVA: 0x0008B474 File Offset: 0x00089674
		public static MatchCollection Matches(string input, string pattern)
		{
			return Regex.Matches(input, pattern, RegexOptions.None);
		}

		// Token: 0x060029AE RID: 10670 RVA: 0x0008B480 File Offset: 0x00089680
		public static MatchCollection Matches(string input, string pattern, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.Matches(input);
		}

		// Token: 0x060029AF RID: 10671 RVA: 0x0008B49C File Offset: 0x0008969C
		public static string Replace(string input, string pattern, MatchEvaluator evaluator)
		{
			return Regex.Replace(input, pattern, evaluator, RegexOptions.None);
		}

		// Token: 0x060029B0 RID: 10672 RVA: 0x0008B4A8 File Offset: 0x000896A8
		public static string Replace(string input, string pattern, MatchEvaluator evaluator, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.Replace(input, evaluator);
		}

		// Token: 0x060029B1 RID: 10673 RVA: 0x0008B4C8 File Offset: 0x000896C8
		public static string Replace(string input, string pattern, string replacement)
		{
			return Regex.Replace(input, pattern, replacement, RegexOptions.None);
		}

		// Token: 0x060029B2 RID: 10674 RVA: 0x0008B4D4 File Offset: 0x000896D4
		public static string Replace(string input, string pattern, string replacement, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.Replace(input, replacement);
		}

		// Token: 0x060029B3 RID: 10675 RVA: 0x0008B4F4 File Offset: 0x000896F4
		public static string[] Split(string input, string pattern)
		{
			return Regex.Split(input, pattern, RegexOptions.None);
		}

		// Token: 0x060029B4 RID: 10676 RVA: 0x0008B500 File Offset: 0x00089700
		public static string[] Split(string input, string pattern, RegexOptions options)
		{
			Regex regex = new Regex(pattern, options);
			return regex.Split(input);
		}

		// Token: 0x17000B8F RID: 2959
		// (get) Token: 0x060029B5 RID: 10677 RVA: 0x0008B51C File Offset: 0x0008971C
		// (set) Token: 0x060029B6 RID: 10678 RVA: 0x0008B528 File Offset: 0x00089728
		public static int CacheSize
		{
			get
			{
				return Regex.cache.Capacity;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("CacheSize");
				}
				Regex.cache.Capacity = value;
			}
		}

		// Token: 0x060029B7 RID: 10679 RVA: 0x0008B548 File Offset: 0x00089748
		private static void validate_options(RegexOptions options)
		{
			if ((options & ~(RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.ExplicitCapture | RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnorePatternWhitespace | RegexOptions.RightToLeft | RegexOptions.ECMAScript | RegexOptions.CultureInvariant)) != RegexOptions.None)
			{
				throw new ArgumentOutOfRangeException("options");
			}
			if ((options & RegexOptions.ECMAScript) != RegexOptions.None && (options & ~(RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled | RegexOptions.ECMAScript)) != RegexOptions.None)
			{
				throw new ArgumentOutOfRangeException("options");
			}
		}

		// Token: 0x060029B8 RID: 10680 RVA: 0x0008B590 File Offset: 0x00089790
		private void Init()
		{
			this.machineFactory = Regex.cache.Lookup(this.pattern, this.roptions);
			if (this.machineFactory == null)
			{
				this.InitNewRegex();
			}
			else
			{
				this.group_count = this.machineFactory.GroupCount;
				this.gap = this.machineFactory.Gap;
				this.mapping = this.machineFactory.Mapping;
				this.group_names = this.machineFactory.NamesMapping;
			}
		}

		// Token: 0x060029B9 RID: 10681 RVA: 0x0008B614 File Offset: 0x00089814
		private void InitNewRegex()
		{
			this.machineFactory = Regex.CreateMachineFactory(this.pattern, this.roptions);
			Regex.cache.Add(this.pattern, this.roptions, this.machineFactory);
			this.group_count = this.machineFactory.GroupCount;
			this.gap = this.machineFactory.Gap;
			this.mapping = this.machineFactory.Mapping;
			this.group_names = this.machineFactory.NamesMapping;
		}

		// Token: 0x060029BA RID: 10682 RVA: 0x0008B698 File Offset: 0x00089898
		private static IMachineFactory CreateMachineFactory(string pattern, RegexOptions options)
		{
			Parser parser = new Parser();
			RegularExpression regularExpression = parser.ParseRegularExpression(pattern, options);
			ICompiler compiler;
			if (!Regex.old_rx)
			{
				if ((options & RegexOptions.Compiled) != RegexOptions.None)
				{
					compiler = new CILCompiler();
				}
				else
				{
					compiler = new RxCompiler();
				}
			}
			else
			{
				compiler = new PatternCompiler();
			}
			regularExpression.Compile(compiler, (options & RegexOptions.RightToLeft) != RegexOptions.None);
			IMachineFactory machineFactory = compiler.GetMachineFactory();
			Hashtable hashtable = new Hashtable();
			machineFactory.Gap = parser.GetMapping(hashtable);
			machineFactory.Mapping = hashtable;
			machineFactory.NamesMapping = Regex.GetGroupNamesArray(machineFactory.GroupCount, machineFactory.Mapping);
			return machineFactory;
		}

		// Token: 0x17000B90 RID: 2960
		// (get) Token: 0x060029BB RID: 10683 RVA: 0x0008B730 File Offset: 0x00089930
		public RegexOptions Options
		{
			get
			{
				return this.roptions;
			}
		}

		// Token: 0x17000B91 RID: 2961
		// (get) Token: 0x060029BC RID: 10684 RVA: 0x0008B738 File Offset: 0x00089938
		public bool RightToLeft
		{
			get
			{
				return (this.roptions & RegexOptions.RightToLeft) != RegexOptions.None;
			}
		}

		// Token: 0x060029BD RID: 10685 RVA: 0x0008B74C File Offset: 0x0008994C
		public string[] GetGroupNames()
		{
			string[] array = new string[1 + this.group_count];
			Array.Copy(this.group_names, array, 1 + this.group_count);
			return array;
		}

		// Token: 0x060029BE RID: 10686 RVA: 0x0008B77C File Offset: 0x0008997C
		public int[] GetGroupNumbers()
		{
			int[] array = new int[1 + this.group_count];
			Array.Copy(this.GroupNumbers, array, 1 + this.group_count);
			return array;
		}

		// Token: 0x060029BF RID: 10687 RVA: 0x0008B7AC File Offset: 0x000899AC
		public string GroupNameFromNumber(int i)
		{
			i = this.GetGroupIndex(i);
			if (i < 0)
			{
				return string.Empty;
			}
			return this.group_names[i];
		}

		// Token: 0x060029C0 RID: 10688 RVA: 0x0008B7CC File Offset: 0x000899CC
		public int GroupNumberFromName(string name)
		{
			if (!this.mapping.Contains(name))
			{
				return -1;
			}
			int num = (int)this.mapping[name];
			if (num >= this.gap)
			{
				num = int.Parse(name);
			}
			return num;
		}

		// Token: 0x060029C1 RID: 10689 RVA: 0x0008B814 File Offset: 0x00089A14
		internal int GetGroupIndex(int number)
		{
			if (number < this.gap)
			{
				return number;
			}
			if (this.gap > this.group_count)
			{
				return -1;
			}
			return Array.BinarySearch<int>(this.GroupNumbers, this.gap, this.group_count - this.gap + 1, number);
		}

		// Token: 0x060029C2 RID: 10690 RVA: 0x0008B864 File Offset: 0x00089A64
		private int default_startat(string input)
		{
			return (!this.RightToLeft || input == null) ? 0 : input.Length;
		}

		// Token: 0x060029C3 RID: 10691 RVA: 0x0008B884 File Offset: 0x00089A84
		public bool IsMatch(string input)
		{
			return this.IsMatch(input, this.default_startat(input));
		}

		// Token: 0x060029C4 RID: 10692 RVA: 0x0008B894 File Offset: 0x00089A94
		public bool IsMatch(string input, int startat)
		{
			return this.Match(input, startat).Success;
		}

		// Token: 0x060029C5 RID: 10693 RVA: 0x0008B8A4 File Offset: 0x00089AA4
		public Match Match(string input)
		{
			return this.Match(input, this.default_startat(input));
		}

		// Token: 0x060029C6 RID: 10694 RVA: 0x0008B8B4 File Offset: 0x00089AB4
		public Match Match(string input, int startat)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat");
			}
			return this.CreateMachine().Scan(this, input, startat, input.Length);
		}

		// Token: 0x060029C7 RID: 10695 RVA: 0x0008B904 File Offset: 0x00089B04
		public Match Match(string input, int startat, int length)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat");
			}
			if (length < 0 || length > input.Length - startat)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			return this.CreateMachine().Scan(this, input, startat, startat + length);
		}

		// Token: 0x060029C8 RID: 10696 RVA: 0x0008B974 File Offset: 0x00089B74
		public MatchCollection Matches(string input)
		{
			return this.Matches(input, this.default_startat(input));
		}

		// Token: 0x060029C9 RID: 10697 RVA: 0x0008B984 File Offset: 0x00089B84
		public MatchCollection Matches(string input, int startat)
		{
			Match start = this.Match(input, startat);
			return new MatchCollection(start);
		}

		// Token: 0x060029CA RID: 10698 RVA: 0x0008B9A0 File Offset: 0x00089BA0
		public string Replace(string input, MatchEvaluator evaluator)
		{
			return this.Replace(input, evaluator, int.MaxValue, this.default_startat(input));
		}

		// Token: 0x060029CB RID: 10699 RVA: 0x0008B9B8 File Offset: 0x00089BB8
		public string Replace(string input, MatchEvaluator evaluator, int count)
		{
			return this.Replace(input, evaluator, count, this.default_startat(input));
		}

		// Token: 0x060029CC RID: 10700 RVA: 0x0008B9CC File Offset: 0x00089BCC
		public string Replace(string input, MatchEvaluator evaluator, int count, int startat)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (evaluator == null)
			{
				throw new ArgumentNullException("evaluator");
			}
			if (count < -1)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat");
			}
			BaseMachine baseMachine = (BaseMachine)this.CreateMachine();
			if (this.RightToLeft)
			{
				return baseMachine.RTLReplace(this, input, evaluator, count, startat);
			}
			Regex.Adapter @object = new Regex.Adapter(evaluator);
			return baseMachine.LTRReplace(this, input, new BaseMachine.MatchAppendEvaluator(@object.Evaluate), count, startat);
		}

		// Token: 0x060029CD RID: 10701 RVA: 0x0008BA70 File Offset: 0x00089C70
		public string Replace(string input, string replacement)
		{
			return this.Replace(input, replacement, int.MaxValue, this.default_startat(input));
		}

		// Token: 0x060029CE RID: 10702 RVA: 0x0008BA88 File Offset: 0x00089C88
		public string Replace(string input, string replacement, int count)
		{
			return this.Replace(input, replacement, count, this.default_startat(input));
		}

		// Token: 0x060029CF RID: 10703 RVA: 0x0008BA9C File Offset: 0x00089C9C
		public string Replace(string input, string replacement, int count, int startat)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (replacement == null)
			{
				throw new ArgumentNullException("replacement");
			}
			if (count < -1)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat");
			}
			return this.CreateMachine().Replace(this, input, replacement, count, startat);
		}

		// Token: 0x060029D0 RID: 10704 RVA: 0x0008BB10 File Offset: 0x00089D10
		public string[] Split(string input)
		{
			return this.Split(input, int.MaxValue, this.default_startat(input));
		}

		// Token: 0x060029D1 RID: 10705 RVA: 0x0008BB28 File Offset: 0x00089D28
		public string[] Split(string input, int count)
		{
			return this.Split(input, count, this.default_startat(input));
		}

		// Token: 0x060029D2 RID: 10706 RVA: 0x0008BB3C File Offset: 0x00089D3C
		public string[] Split(string input, int count, int startat)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (startat < 0 || startat > input.Length)
			{
				throw new ArgumentOutOfRangeException("startat");
			}
			return this.CreateMachine().Split(this, input, count, startat);
		}

		// Token: 0x060029D3 RID: 10707 RVA: 0x0008BB9C File Offset: 0x00089D9C
		protected void InitializeReferences()
		{
			if (this.refsInitialized)
			{
				throw new NotSupportedException("This operation is only allowed once per object.");
			}
			this.refsInitialized = true;
			this.Init();
		}

		// Token: 0x060029D4 RID: 10708 RVA: 0x0008BBC4 File Offset: 0x00089DC4
		protected bool UseOptionC()
		{
			return (this.roptions & RegexOptions.Compiled) != RegexOptions.None;
		}

		// Token: 0x060029D5 RID: 10709 RVA: 0x0008BBD4 File Offset: 0x00089DD4
		protected bool UseOptionR()
		{
			return (this.roptions & RegexOptions.RightToLeft) != RegexOptions.None;
		}

		// Token: 0x060029D6 RID: 10710 RVA: 0x0008BBE8 File Offset: 0x00089DE8
		public override string ToString()
		{
			return this.pattern;
		}

		// Token: 0x17000B92 RID: 2962
		// (get) Token: 0x060029D7 RID: 10711 RVA: 0x0008BBF0 File Offset: 0x00089DF0
		internal int GroupCount
		{
			get
			{
				return this.group_count;
			}
		}

		// Token: 0x17000B93 RID: 2963
		// (get) Token: 0x060029D8 RID: 10712 RVA: 0x0008BBF8 File Offset: 0x00089DF8
		internal int Gap
		{
			get
			{
				return this.gap;
			}
		}

		// Token: 0x060029D9 RID: 10713 RVA: 0x0008BC00 File Offset: 0x00089E00
		private IMachine CreateMachine()
		{
			return this.machineFactory.NewInstance();
		}

		// Token: 0x060029DA RID: 10714 RVA: 0x0008BC10 File Offset: 0x00089E10
		private static string[] GetGroupNamesArray(int groupCount, IDictionary mapping)
		{
			string[] array = new string[groupCount + 1];
			IDictionaryEnumerator enumerator = mapping.GetEnumerator();
			while (enumerator.MoveNext())
			{
				array[(int)enumerator.Value] = (string)enumerator.Key;
			}
			return array;
		}

		// Token: 0x17000B94 RID: 2964
		// (get) Token: 0x060029DB RID: 10715 RVA: 0x0008BC58 File Offset: 0x00089E58
		private int[] GroupNumbers
		{
			get
			{
				if (this.group_numbers == null)
				{
					this.group_numbers = new int[1 + this.group_count];
					for (int i = 0; i < this.gap; i++)
					{
						this.group_numbers[i] = i;
					}
					for (int j = this.gap; j <= this.group_count; j++)
					{
						this.group_numbers[j] = int.Parse(this.group_names[j]);
					}
					return this.group_numbers;
				}
				return this.group_numbers;
			}
		}

		// Token: 0x04001A0F RID: 6671
		private static FactoryCache cache = new FactoryCache(15);

		// Token: 0x04001A10 RID: 6672
		private static readonly bool old_rx = Environment.GetEnvironmentVariable("MONO_NEW_RX") == null;

		// Token: 0x04001A11 RID: 6673
		private IMachineFactory machineFactory;

		// Token: 0x04001A12 RID: 6674
		private IDictionary mapping;

		// Token: 0x04001A13 RID: 6675
		private int group_count;

		// Token: 0x04001A14 RID: 6676
		private int gap;

		// Token: 0x04001A15 RID: 6677
		private bool refsInitialized;

		// Token: 0x04001A16 RID: 6678
		private string[] group_names;

		// Token: 0x04001A17 RID: 6679
		private int[] group_numbers;

		// Token: 0x04001A18 RID: 6680
		protected internal string pattern;

		// Token: 0x04001A19 RID: 6681
		protected internal RegexOptions roptions;

		// Token: 0x04001A1A RID: 6682
		[MonoTODO]
		protected internal Hashtable capnames;

		// Token: 0x04001A1B RID: 6683
		[MonoTODO]
		protected internal Hashtable caps;

		// Token: 0x04001A1C RID: 6684
		[MonoTODO]
		protected internal RegexRunnerFactory factory;

		// Token: 0x04001A1D RID: 6685
		[MonoTODO]
		protected internal int capsize;

		// Token: 0x04001A1E RID: 6686
		[MonoTODO]
		protected internal string[] capslist;

		// Token: 0x0200048B RID: 1163
		private class Adapter
		{
			// Token: 0x060029DC RID: 10716 RVA: 0x0008BCE4 File Offset: 0x00089EE4
			public Adapter(MatchEvaluator ev)
			{
				this.ev = ev;
			}

			// Token: 0x060029DD RID: 10717 RVA: 0x0008BCF4 File Offset: 0x00089EF4
			public void Evaluate(Match m, StringBuilder sb)
			{
				sb.Append(this.ev(m));
			}

			// Token: 0x04001A1F RID: 6687
			private MatchEvaluator ev;
		}
	}
}
