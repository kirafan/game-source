﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000470 RID: 1136
	internal enum Category : ushort
	{
		// Token: 0x0400192F RID: 6447
		None,
		// Token: 0x04001930 RID: 6448
		Any,
		// Token: 0x04001931 RID: 6449
		AnySingleline,
		// Token: 0x04001932 RID: 6450
		Word,
		// Token: 0x04001933 RID: 6451
		Digit,
		// Token: 0x04001934 RID: 6452
		WhiteSpace,
		// Token: 0x04001935 RID: 6453
		EcmaAny,
		// Token: 0x04001936 RID: 6454
		EcmaAnySingleline,
		// Token: 0x04001937 RID: 6455
		EcmaWord,
		// Token: 0x04001938 RID: 6456
		EcmaDigit,
		// Token: 0x04001939 RID: 6457
		EcmaWhiteSpace,
		// Token: 0x0400193A RID: 6458
		UnicodeL,
		// Token: 0x0400193B RID: 6459
		UnicodeM,
		// Token: 0x0400193C RID: 6460
		UnicodeN,
		// Token: 0x0400193D RID: 6461
		UnicodeZ,
		// Token: 0x0400193E RID: 6462
		UnicodeP,
		// Token: 0x0400193F RID: 6463
		UnicodeS,
		// Token: 0x04001940 RID: 6464
		UnicodeC,
		// Token: 0x04001941 RID: 6465
		UnicodeLu,
		// Token: 0x04001942 RID: 6466
		UnicodeLl,
		// Token: 0x04001943 RID: 6467
		UnicodeLt,
		// Token: 0x04001944 RID: 6468
		UnicodeLm,
		// Token: 0x04001945 RID: 6469
		UnicodeLo,
		// Token: 0x04001946 RID: 6470
		UnicodeMn,
		// Token: 0x04001947 RID: 6471
		UnicodeMe,
		// Token: 0x04001948 RID: 6472
		UnicodeMc,
		// Token: 0x04001949 RID: 6473
		UnicodeNd,
		// Token: 0x0400194A RID: 6474
		UnicodeNl,
		// Token: 0x0400194B RID: 6475
		UnicodeNo,
		// Token: 0x0400194C RID: 6476
		UnicodeZs,
		// Token: 0x0400194D RID: 6477
		UnicodeZl,
		// Token: 0x0400194E RID: 6478
		UnicodeZp,
		// Token: 0x0400194F RID: 6479
		UnicodePd,
		// Token: 0x04001950 RID: 6480
		UnicodePs,
		// Token: 0x04001951 RID: 6481
		UnicodePi,
		// Token: 0x04001952 RID: 6482
		UnicodePe,
		// Token: 0x04001953 RID: 6483
		UnicodePf,
		// Token: 0x04001954 RID: 6484
		UnicodePc,
		// Token: 0x04001955 RID: 6485
		UnicodePo,
		// Token: 0x04001956 RID: 6486
		UnicodeSm,
		// Token: 0x04001957 RID: 6487
		UnicodeSc,
		// Token: 0x04001958 RID: 6488
		UnicodeSk,
		// Token: 0x04001959 RID: 6489
		UnicodeSo,
		// Token: 0x0400195A RID: 6490
		UnicodeCc,
		// Token: 0x0400195B RID: 6491
		UnicodeCf,
		// Token: 0x0400195C RID: 6492
		UnicodeCo,
		// Token: 0x0400195D RID: 6493
		UnicodeCs,
		// Token: 0x0400195E RID: 6494
		UnicodeCn,
		// Token: 0x0400195F RID: 6495
		UnicodeBasicLatin,
		// Token: 0x04001960 RID: 6496
		UnicodeLatin1Supplement,
		// Token: 0x04001961 RID: 6497
		UnicodeLatinExtendedA,
		// Token: 0x04001962 RID: 6498
		UnicodeLatinExtendedB,
		// Token: 0x04001963 RID: 6499
		UnicodeIPAExtensions,
		// Token: 0x04001964 RID: 6500
		UnicodeSpacingModifierLetters,
		// Token: 0x04001965 RID: 6501
		UnicodeCombiningDiacriticalMarks,
		// Token: 0x04001966 RID: 6502
		UnicodeGreek,
		// Token: 0x04001967 RID: 6503
		UnicodeCyrillic,
		// Token: 0x04001968 RID: 6504
		UnicodeArmenian,
		// Token: 0x04001969 RID: 6505
		UnicodeHebrew,
		// Token: 0x0400196A RID: 6506
		UnicodeArabic,
		// Token: 0x0400196B RID: 6507
		UnicodeSyriac,
		// Token: 0x0400196C RID: 6508
		UnicodeThaana,
		// Token: 0x0400196D RID: 6509
		UnicodeDevanagari,
		// Token: 0x0400196E RID: 6510
		UnicodeBengali,
		// Token: 0x0400196F RID: 6511
		UnicodeGurmukhi,
		// Token: 0x04001970 RID: 6512
		UnicodeGujarati,
		// Token: 0x04001971 RID: 6513
		UnicodeOriya,
		// Token: 0x04001972 RID: 6514
		UnicodeTamil,
		// Token: 0x04001973 RID: 6515
		UnicodeTelugu,
		// Token: 0x04001974 RID: 6516
		UnicodeKannada,
		// Token: 0x04001975 RID: 6517
		UnicodeMalayalam,
		// Token: 0x04001976 RID: 6518
		UnicodeSinhala,
		// Token: 0x04001977 RID: 6519
		UnicodeThai,
		// Token: 0x04001978 RID: 6520
		UnicodeLao,
		// Token: 0x04001979 RID: 6521
		UnicodeTibetan,
		// Token: 0x0400197A RID: 6522
		UnicodeMyanmar,
		// Token: 0x0400197B RID: 6523
		UnicodeGeorgian,
		// Token: 0x0400197C RID: 6524
		UnicodeHangulJamo,
		// Token: 0x0400197D RID: 6525
		UnicodeEthiopic,
		// Token: 0x0400197E RID: 6526
		UnicodeCherokee,
		// Token: 0x0400197F RID: 6527
		UnicodeUnifiedCanadianAboriginalSyllabics,
		// Token: 0x04001980 RID: 6528
		UnicodeOgham,
		// Token: 0x04001981 RID: 6529
		UnicodeRunic,
		// Token: 0x04001982 RID: 6530
		UnicodeKhmer,
		// Token: 0x04001983 RID: 6531
		UnicodeMongolian,
		// Token: 0x04001984 RID: 6532
		UnicodeLatinExtendedAdditional,
		// Token: 0x04001985 RID: 6533
		UnicodeGreekExtended,
		// Token: 0x04001986 RID: 6534
		UnicodeGeneralPunctuation,
		// Token: 0x04001987 RID: 6535
		UnicodeSuperscriptsandSubscripts,
		// Token: 0x04001988 RID: 6536
		UnicodeCurrencySymbols,
		// Token: 0x04001989 RID: 6537
		UnicodeCombiningMarksforSymbols,
		// Token: 0x0400198A RID: 6538
		UnicodeLetterlikeSymbols,
		// Token: 0x0400198B RID: 6539
		UnicodeNumberForms,
		// Token: 0x0400198C RID: 6540
		UnicodeArrows,
		// Token: 0x0400198D RID: 6541
		UnicodeMathematicalOperators,
		// Token: 0x0400198E RID: 6542
		UnicodeMiscellaneousTechnical,
		// Token: 0x0400198F RID: 6543
		UnicodeControlPictures,
		// Token: 0x04001990 RID: 6544
		UnicodeOpticalCharacterRecognition,
		// Token: 0x04001991 RID: 6545
		UnicodeEnclosedAlphanumerics,
		// Token: 0x04001992 RID: 6546
		UnicodeBoxDrawing,
		// Token: 0x04001993 RID: 6547
		UnicodeBlockElements,
		// Token: 0x04001994 RID: 6548
		UnicodeGeometricShapes,
		// Token: 0x04001995 RID: 6549
		UnicodeMiscellaneousSymbols,
		// Token: 0x04001996 RID: 6550
		UnicodeDingbats,
		// Token: 0x04001997 RID: 6551
		UnicodeBraillePatterns,
		// Token: 0x04001998 RID: 6552
		UnicodeCJKRadicalsSupplement,
		// Token: 0x04001999 RID: 6553
		UnicodeKangxiRadicals,
		// Token: 0x0400199A RID: 6554
		UnicodeIdeographicDescriptionCharacters,
		// Token: 0x0400199B RID: 6555
		UnicodeCJKSymbolsandPunctuation,
		// Token: 0x0400199C RID: 6556
		UnicodeHiragana,
		// Token: 0x0400199D RID: 6557
		UnicodeKatakana,
		// Token: 0x0400199E RID: 6558
		UnicodeBopomofo,
		// Token: 0x0400199F RID: 6559
		UnicodeHangulCompatibilityJamo,
		// Token: 0x040019A0 RID: 6560
		UnicodeKanbun,
		// Token: 0x040019A1 RID: 6561
		UnicodeBopomofoExtended,
		// Token: 0x040019A2 RID: 6562
		UnicodeEnclosedCJKLettersandMonths,
		// Token: 0x040019A3 RID: 6563
		UnicodeCJKCompatibility,
		// Token: 0x040019A4 RID: 6564
		UnicodeCJKUnifiedIdeographsExtensionA,
		// Token: 0x040019A5 RID: 6565
		UnicodeCJKUnifiedIdeographs,
		// Token: 0x040019A6 RID: 6566
		UnicodeYiSyllables,
		// Token: 0x040019A7 RID: 6567
		UnicodeYiRadicals,
		// Token: 0x040019A8 RID: 6568
		UnicodeHangulSyllables,
		// Token: 0x040019A9 RID: 6569
		UnicodeHighSurrogates,
		// Token: 0x040019AA RID: 6570
		UnicodeHighPrivateUseSurrogates,
		// Token: 0x040019AB RID: 6571
		UnicodeLowSurrogates,
		// Token: 0x040019AC RID: 6572
		UnicodePrivateUse,
		// Token: 0x040019AD RID: 6573
		UnicodeCJKCompatibilityIdeographs,
		// Token: 0x040019AE RID: 6574
		UnicodeAlphabeticPresentationForms,
		// Token: 0x040019AF RID: 6575
		UnicodeArabicPresentationFormsA,
		// Token: 0x040019B0 RID: 6576
		UnicodeCombiningHalfMarks,
		// Token: 0x040019B1 RID: 6577
		UnicodeCJKCompatibilityForms,
		// Token: 0x040019B2 RID: 6578
		UnicodeSmallFormVariants,
		// Token: 0x040019B3 RID: 6579
		UnicodeArabicPresentationFormsB,
		// Token: 0x040019B4 RID: 6580
		UnicodeSpecials,
		// Token: 0x040019B5 RID: 6581
		UnicodeHalfwidthandFullwidthForms,
		// Token: 0x040019B6 RID: 6582
		UnicodeOldItalic,
		// Token: 0x040019B7 RID: 6583
		UnicodeGothic,
		// Token: 0x040019B8 RID: 6584
		UnicodeDeseret,
		// Token: 0x040019B9 RID: 6585
		UnicodeByzantineMusicalSymbols,
		// Token: 0x040019BA RID: 6586
		UnicodeMusicalSymbols,
		// Token: 0x040019BB RID: 6587
		UnicodeMathematicalAlphanumericSymbols,
		// Token: 0x040019BC RID: 6588
		UnicodeCJKUnifiedIdeographsExtensionB,
		// Token: 0x040019BD RID: 6589
		UnicodeCJKCompatibilityIdeographsSupplement,
		// Token: 0x040019BE RID: 6590
		UnicodeTags,
		// Token: 0x040019BF RID: 6591
		LastValue
	}
}
