﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x0200051D RID: 1309
	// (Invoke) Token: 0x06002D20 RID: 11552
	[Serializable]
	public delegate string MatchEvaluator(Match match);
}
