﻿using System;

namespace System.Text.RegularExpressions
{
	// Token: 0x02000486 RID: 1158
	[Serializable]
	public class Match : Group
	{
		// Token: 0x0600295D RID: 10589 RVA: 0x000889C0 File Offset: 0x00086BC0
		private Match()
		{
			this.regex = null;
			this.machine = null;
			this.text_length = 0;
			this.groups = new GroupCollection(1, 1);
			this.groups.SetValue(this, 0);
		}

		// Token: 0x0600295E RID: 10590 RVA: 0x000889F8 File Offset: 0x00086BF8
		internal Match(Regex regex, IMachine machine, string text, int text_length, int n_groups, int index, int length) : base(text, index, length)
		{
			this.regex = regex;
			this.machine = machine;
			this.text_length = text_length;
		}

		// Token: 0x0600295F RID: 10591 RVA: 0x00088A1C File Offset: 0x00086C1C
		internal Match(Regex regex, IMachine machine, string text, int text_length, int n_groups, int index, int length, int n_caps) : base(text, index, length, n_caps)
		{
			this.regex = regex;
			this.machine = machine;
			this.text_length = text_length;
			this.groups = new GroupCollection(n_groups, regex.Gap);
			this.groups.SetValue(this, 0);
		}

		// Token: 0x17000B84 RID: 2948
		// (get) Token: 0x06002961 RID: 10593 RVA: 0x00088A78 File Offset: 0x00086C78
		public static Match Empty
		{
			get
			{
				return Match.empty;
			}
		}

		// Token: 0x06002962 RID: 10594 RVA: 0x00088A80 File Offset: 0x00086C80
		[MonoTODO("not thread-safe")]
		public static Match Synchronized(Match inner)
		{
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			return inner;
		}

		// Token: 0x17000B85 RID: 2949
		// (get) Token: 0x06002963 RID: 10595 RVA: 0x00088A94 File Offset: 0x00086C94
		public virtual GroupCollection Groups
		{
			get
			{
				return this.groups;
			}
		}

		// Token: 0x06002964 RID: 10596 RVA: 0x00088A9C File Offset: 0x00086C9C
		public Match NextMatch()
		{
			if (this == Match.Empty)
			{
				return Match.Empty;
			}
			int num = (!this.regex.RightToLeft) ? (base.Index + base.Length) : base.Index;
			if (base.Length == 0)
			{
				num += ((!this.regex.RightToLeft) ? 1 : -1);
			}
			return this.machine.Scan(this.regex, base.Text, num, this.text_length);
		}

		// Token: 0x06002965 RID: 10597 RVA: 0x00088B28 File Offset: 0x00086D28
		public virtual string Result(string replacement)
		{
			if (replacement == null)
			{
				throw new ArgumentNullException("replacement");
			}
			if (this.machine == null)
			{
				throw new NotSupportedException("Result cannot be called on failed Match.");
			}
			return this.machine.Result(replacement, this);
		}

		// Token: 0x17000B86 RID: 2950
		// (get) Token: 0x06002966 RID: 10598 RVA: 0x00088B6C File Offset: 0x00086D6C
		internal Regex Regex
		{
			get
			{
				return this.regex;
			}
		}

		// Token: 0x040019F8 RID: 6648
		private Regex regex;

		// Token: 0x040019F9 RID: 6649
		private IMachine machine;

		// Token: 0x040019FA RID: 6650
		private int text_length;

		// Token: 0x040019FB RID: 6651
		private GroupCollection groups;

		// Token: 0x040019FC RID: 6652
		private static Match empty = new Match();
	}
}
