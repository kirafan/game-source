﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000050 RID: 80
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeNamespace : CodeObject
	{
		// Token: 0x060002A2 RID: 674 RVA: 0x0000C0AC File Offset: 0x0000A2AC
		public CodeNamespace()
		{
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000C0B4 File Offset: 0x0000A2B4
		public CodeNamespace(string name)
		{
			this.name = name;
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x060002A4 RID: 676 RVA: 0x0000C0C4 File Offset: 0x0000A2C4
		// (remove) Token: 0x060002A5 RID: 677 RVA: 0x0000C0E0 File Offset: 0x0000A2E0
		public event EventHandler PopulateComments;

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x060002A6 RID: 678 RVA: 0x0000C0FC File Offset: 0x0000A2FC
		// (remove) Token: 0x060002A7 RID: 679 RVA: 0x0000C118 File Offset: 0x0000A318
		public event EventHandler PopulateImports;

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x060002A8 RID: 680 RVA: 0x0000C134 File Offset: 0x0000A334
		// (remove) Token: 0x060002A9 RID: 681 RVA: 0x0000C150 File Offset: 0x0000A350
		public event EventHandler PopulateTypes;

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060002AA RID: 682 RVA: 0x0000C16C File Offset: 0x0000A36C
		public CodeCommentStatementCollection Comments
		{
			get
			{
				if (this.comments == null)
				{
					this.comments = new CodeCommentStatementCollection();
					if (this.PopulateComments != null)
					{
						this.PopulateComments(this, EventArgs.Empty);
					}
				}
				return this.comments;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060002AB RID: 683 RVA: 0x0000C1B4 File Offset: 0x0000A3B4
		public CodeNamespaceImportCollection Imports
		{
			get
			{
				if (this.imports == null)
				{
					this.imports = new CodeNamespaceImportCollection();
					if (this.PopulateImports != null)
					{
						this.PopulateImports(this, EventArgs.Empty);
					}
				}
				return this.imports;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060002AC RID: 684 RVA: 0x0000C1FC File Offset: 0x0000A3FC
		// (set) Token: 0x060002AD RID: 685 RVA: 0x0000C218 File Offset: 0x0000A418
		public string Name
		{
			get
			{
				if (this.name == null)
				{
					return string.Empty;
				}
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060002AE RID: 686 RVA: 0x0000C224 File Offset: 0x0000A424
		public CodeTypeDeclarationCollection Types
		{
			get
			{
				if (this.classes == null)
				{
					this.classes = new CodeTypeDeclarationCollection();
					if (this.PopulateTypes != null)
					{
						this.PopulateTypes(this, EventArgs.Empty);
					}
				}
				return this.classes;
			}
		}

		// Token: 0x040000CF RID: 207
		private CodeCommentStatementCollection comments;

		// Token: 0x040000D0 RID: 208
		private CodeNamespaceImportCollection imports;

		// Token: 0x040000D1 RID: 209
		private CodeNamespaceCollection namespaces;

		// Token: 0x040000D2 RID: 210
		private CodeTypeDeclarationCollection classes;

		// Token: 0x040000D3 RID: 211
		private string name;

		// Token: 0x040000D4 RID: 212
		private int populated;
	}
}
