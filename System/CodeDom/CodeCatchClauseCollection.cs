﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200002E RID: 46
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCatchClauseCollection : CollectionBase
	{
		// Token: 0x06000196 RID: 406 RVA: 0x0000AB34 File Offset: 0x00008D34
		public CodeCatchClauseCollection()
		{
		}

		// Token: 0x06000197 RID: 407 RVA: 0x0000AB3C File Offset: 0x00008D3C
		public CodeCatchClauseCollection(CodeCatchClause[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000198 RID: 408 RVA: 0x0000AB4C File Offset: 0x00008D4C
		public CodeCatchClauseCollection(CodeCatchClauseCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x1700002A RID: 42
		public CodeCatchClause this[int index]
		{
			get
			{
				return (CodeCatchClause)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x0600019B RID: 411 RVA: 0x0000AB80 File Offset: 0x00008D80
		public int Add(CodeCatchClause value)
		{
			return base.List.Add(value);
		}

		// Token: 0x0600019C RID: 412 RVA: 0x0000AB90 File Offset: 0x00008D90
		public void AddRange(CodeCatchClause[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000ABCC File Offset: 0x00008DCC
		public void AddRange(CodeCatchClauseCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000AC14 File Offset: 0x00008E14
		public bool Contains(CodeCatchClause value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x0600019F RID: 415 RVA: 0x0000AC24 File Offset: 0x00008E24
		public void CopyTo(CodeCatchClause[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x0000AC34 File Offset: 0x00008E34
		public int IndexOf(CodeCatchClause value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x0000AC44 File Offset: 0x00008E44
		public void Insert(int index, CodeCatchClause value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x0000AC54 File Offset: 0x00008E54
		public void Remove(CodeCatchClause value)
		{
			base.List.Remove(value);
		}
	}
}
