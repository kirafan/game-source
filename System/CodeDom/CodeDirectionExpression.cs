﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003A RID: 58
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDirectionExpression : CodeExpression
	{
		// Token: 0x060001F6 RID: 502 RVA: 0x0000B2EC File Offset: 0x000094EC
		public CodeDirectionExpression()
		{
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000B2F4 File Offset: 0x000094F4
		public CodeDirectionExpression(FieldDirection direction, CodeExpression expression)
		{
			this.direction = direction;
			this.expression = expression;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001F8 RID: 504 RVA: 0x0000B30C File Offset: 0x0000950C
		// (set) Token: 0x060001F9 RID: 505 RVA: 0x0000B314 File Offset: 0x00009514
		public FieldDirection Direction
		{
			get
			{
				return this.direction;
			}
			set
			{
				this.direction = value;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001FA RID: 506 RVA: 0x0000B320 File Offset: 0x00009520
		// (set) Token: 0x060001FB RID: 507 RVA: 0x0000B328 File Offset: 0x00009528
		public CodeExpression Expression
		{
			get
			{
				return this.expression;
			}
			set
			{
				this.expression = value;
			}
		}

		// Token: 0x060001FC RID: 508 RVA: 0x0000B334 File Offset: 0x00009534
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x0400009F RID: 159
		private FieldDirection direction;

		// Token: 0x040000A0 RID: 160
		private CodeExpression expression;
	}
}
