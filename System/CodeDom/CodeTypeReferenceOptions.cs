﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000072 RID: 114
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum CodeTypeReferenceOptions
	{
		// Token: 0x0400011B RID: 283
		GlobalReference = 1,
		// Token: 0x0400011C RID: 284
		GenericTypeParameter = 2
	}
}
