﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003B RID: 59
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeDirectiveCollection : CollectionBase
	{
		// Token: 0x060001FD RID: 509 RVA: 0x0000B340 File Offset: 0x00009540
		public CodeDirectiveCollection()
		{
		}

		// Token: 0x060001FE RID: 510 RVA: 0x0000B348 File Offset: 0x00009548
		public CodeDirectiveCollection(CodeDirective[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000B358 File Offset: 0x00009558
		public CodeDirectiveCollection(CodeDirectiveCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000047 RID: 71
		public CodeDirective this[int index]
		{
			get
			{
				return (CodeDirective)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000B38C File Offset: 0x0000958C
		public int Add(CodeDirective value)
		{
			return base.List.Add(value);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x0000B39C File Offset: 0x0000959C
		public void AddRange(CodeDirective[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0000B3D8 File Offset: 0x000095D8
		public void AddRange(CodeDirectiveCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000B420 File Offset: 0x00009620
		public bool Contains(CodeDirective value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0000B430 File Offset: 0x00009630
		public void CopyTo(CodeDirective[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06000207 RID: 519 RVA: 0x0000B440 File Offset: 0x00009640
		public int IndexOf(CodeDirective value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0000B450 File Offset: 0x00009650
		public void Insert(int index, CodeDirective value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000209 RID: 521 RVA: 0x0000B460 File Offset: 0x00009660
		public void Remove(CodeDirective value)
		{
			base.List.Remove(value);
		}
	}
}
