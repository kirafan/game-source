﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000071 RID: 113
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeReferenceExpression : CodeExpression
	{
		// Token: 0x060003C5 RID: 965 RVA: 0x0000DE04 File Offset: 0x0000C004
		public CodeTypeReferenceExpression()
		{
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000DE0C File Offset: 0x0000C00C
		public CodeTypeReferenceExpression(CodeTypeReference type)
		{
			this.type = type;
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000DE1C File Offset: 0x0000C01C
		public CodeTypeReferenceExpression(string type)
		{
			this.type = new CodeTypeReference(type);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000DE30 File Offset: 0x0000C030
		public CodeTypeReferenceExpression(Type type)
		{
			this.type = new CodeTypeReference(type);
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x060003C9 RID: 969 RVA: 0x0000DE44 File Offset: 0x0000C044
		// (set) Token: 0x060003CA RID: 970 RVA: 0x0000DE64 File Offset: 0x0000C064
		public CodeTypeReference Type
		{
			get
			{
				if (this.type == null)
				{
					return new CodeTypeReference(string.Empty);
				}
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0000DE70 File Offset: 0x0000C070
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x04000119 RID: 281
		private CodeTypeReference type;
	}
}
