﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200006D RID: 109
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeParameterCollection : CollectionBase
	{
		// Token: 0x0600038C RID: 908 RVA: 0x0000D5D8 File Offset: 0x0000B7D8
		public CodeTypeParameterCollection()
		{
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000D5E0 File Offset: 0x0000B7E0
		public CodeTypeParameterCollection(CodeTypeParameter[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000D5F0 File Offset: 0x0000B7F0
		public CodeTypeParameterCollection(CodeTypeParameterCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000D600 File Offset: 0x0000B800
		public int Add(CodeTypeParameter value)
		{
			return base.List.Add(value);
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000D610 File Offset: 0x0000B810
		public void Add(string value)
		{
			base.List.Add(new CodeTypeParameter(value));
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000D624 File Offset: 0x0000B824
		public void AddRange(CodeTypeParameter[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000D660 File Offset: 0x0000B860
		public void AddRange(CodeTypeParameterCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000D6A8 File Offset: 0x0000B8A8
		public bool Contains(CodeTypeParameter value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x06000394 RID: 916 RVA: 0x0000D6B8 File Offset: 0x0000B8B8
		public void CopyTo(CodeTypeParameter[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06000395 RID: 917 RVA: 0x0000D6C8 File Offset: 0x0000B8C8
		public int IndexOf(CodeTypeParameter value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000396 RID: 918 RVA: 0x0000D6D8 File Offset: 0x0000B8D8
		public void Insert(int index, CodeTypeParameter value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x0000D6E8 File Offset: 0x0000B8E8
		public void Remove(CodeTypeParameter value)
		{
			base.List.Remove(value);
		}

		// Token: 0x170000B3 RID: 179
		public CodeTypeParameter this[int index]
		{
			get
			{
				return (CodeTypeParameter)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}
	}
}
