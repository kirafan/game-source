﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003E RID: 62
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeEventReferenceExpression : CodeExpression
	{
		// Token: 0x0600020D RID: 525 RVA: 0x0000B498 File Offset: 0x00009698
		public CodeEventReferenceExpression()
		{
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0000B4A0 File Offset: 0x000096A0
		public CodeEventReferenceExpression(CodeExpression targetObject, string eventName)
		{
			this.targetObject = targetObject;
			this.eventName = eventName;
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600020F RID: 527 RVA: 0x0000B4B8 File Offset: 0x000096B8
		// (set) Token: 0x06000210 RID: 528 RVA: 0x0000B4D4 File Offset: 0x000096D4
		public string EventName
		{
			get
			{
				if (this.eventName == null)
				{
					return string.Empty;
				}
				return this.eventName;
			}
			set
			{
				this.eventName = value;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000211 RID: 529 RVA: 0x0000B4E0 File Offset: 0x000096E0
		// (set) Token: 0x06000212 RID: 530 RVA: 0x0000B4E8 File Offset: 0x000096E8
		public CodeExpression TargetObject
		{
			get
			{
				return this.targetObject;
			}
			set
			{
				this.targetObject = value;
			}
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0000B4F4 File Offset: 0x000096F4
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x040000A1 RID: 161
		private string eventName;

		// Token: 0x040000A2 RID: 162
		private CodeExpression targetObject;
	}
}
