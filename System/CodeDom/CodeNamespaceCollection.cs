﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200004F RID: 79
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeNamespaceCollection : CollectionBase
	{
		// Token: 0x06000295 RID: 661 RVA: 0x0000BF7C File Offset: 0x0000A17C
		public CodeNamespaceCollection()
		{
		}

		// Token: 0x06000296 RID: 662 RVA: 0x0000BF84 File Offset: 0x0000A184
		public CodeNamespaceCollection(CodeNamespace[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000BF94 File Offset: 0x0000A194
		public CodeNamespaceCollection(CodeNamespaceCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000073 RID: 115
		public CodeNamespace this[int index]
		{
			get
			{
				return (CodeNamespace)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000BFC8 File Offset: 0x0000A1C8
		public int Add(CodeNamespace value)
		{
			return base.List.Add(value);
		}

		// Token: 0x0600029B RID: 667 RVA: 0x0000BFD8 File Offset: 0x0000A1D8
		public void AddRange(CodeNamespace[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000C014 File Offset: 0x0000A214
		public void AddRange(CodeNamespaceCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000C05C File Offset: 0x0000A25C
		public bool Contains(CodeNamespace value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x0600029E RID: 670 RVA: 0x0000C06C File Offset: 0x0000A26C
		public void CopyTo(CodeNamespace[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000C07C File Offset: 0x0000A27C
		public int IndexOf(CodeNamespace value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0000C08C File Offset: 0x0000A28C
		public void Insert(int index, CodeNamespace value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000C09C File Offset: 0x0000A29C
		public void Remove(CodeNamespace value)
		{
			base.List.Remove(value);
		}
	}
}
