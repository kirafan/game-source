﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000028 RID: 40
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeAttributeDeclarationCollection : CollectionBase
	{
		// Token: 0x0600016C RID: 364 RVA: 0x0000A7DC File Offset: 0x000089DC
		public CodeAttributeDeclarationCollection()
		{
		}

		// Token: 0x0600016D RID: 365 RVA: 0x0000A7E4 File Offset: 0x000089E4
		public CodeAttributeDeclarationCollection(CodeAttributeDeclaration[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000A7F4 File Offset: 0x000089F4
		public CodeAttributeDeclarationCollection(CodeAttributeDeclarationCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000021 RID: 33
		public CodeAttributeDeclaration this[int index]
		{
			get
			{
				return (CodeAttributeDeclaration)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06000171 RID: 369 RVA: 0x0000A828 File Offset: 0x00008A28
		public int Add(CodeAttributeDeclaration value)
		{
			return base.List.Add(value);
		}

		// Token: 0x06000172 RID: 370 RVA: 0x0000A838 File Offset: 0x00008A38
		public void AddRange(CodeAttributeDeclaration[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000173 RID: 371 RVA: 0x0000A874 File Offset: 0x00008A74
		public void AddRange(CodeAttributeDeclarationCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x0000A8BC File Offset: 0x00008ABC
		public bool Contains(CodeAttributeDeclaration value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x06000175 RID: 373 RVA: 0x0000A8CC File Offset: 0x00008ACC
		public void CopyTo(CodeAttributeDeclaration[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06000176 RID: 374 RVA: 0x0000A8DC File Offset: 0x00008ADC
		public int IndexOf(CodeAttributeDeclaration value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000177 RID: 375 RVA: 0x0000A8EC File Offset: 0x00008AEC
		public void Insert(int index, CodeAttributeDeclaration value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000A8FC File Offset: 0x00008AFC
		public void Remove(CodeAttributeDeclaration value)
		{
			base.List.Remove(value);
		}
	}
}
