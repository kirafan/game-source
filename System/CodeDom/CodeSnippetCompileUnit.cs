﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200005D RID: 93
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeSnippetCompileUnit : CodeCompileUnit
	{
		// Token: 0x0600030E RID: 782 RVA: 0x0000CB1C File Offset: 0x0000AD1C
		public CodeSnippetCompileUnit()
		{
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000CB24 File Offset: 0x0000AD24
		public CodeSnippetCompileUnit(string value)
		{
			this.value = value;
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000310 RID: 784 RVA: 0x0000CB34 File Offset: 0x0000AD34
		// (set) Token: 0x06000311 RID: 785 RVA: 0x0000CB3C File Offset: 0x0000AD3C
		public CodeLinePragma LinePragma
		{
			get
			{
				return this.linePragma;
			}
			set
			{
				this.linePragma = value;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000312 RID: 786 RVA: 0x0000CB48 File Offset: 0x0000AD48
		// (set) Token: 0x06000313 RID: 787 RVA: 0x0000CB64 File Offset: 0x0000AD64
		public string Value
		{
			get
			{
				if (this.value == null)
				{
					return string.Empty;
				}
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x040000EE RID: 238
		private CodeLinePragma linePragma;

		// Token: 0x040000EF RID: 239
		private string value;
	}
}
