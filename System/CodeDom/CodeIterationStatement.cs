﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000045 RID: 69
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeIterationStatement : CodeStatement
	{
		// Token: 0x06000239 RID: 569 RVA: 0x0000B788 File Offset: 0x00009988
		public CodeIterationStatement()
		{
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000B790 File Offset: 0x00009990
		public CodeIterationStatement(CodeStatement initStatement, CodeExpression testExpression, CodeStatement incrementStatement, params CodeStatement[] statements)
		{
			this.initStatement = initStatement;
			this.testExpression = testExpression;
			this.incrementStatement = incrementStatement;
			this.Statements.AddRange(statements);
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600023B RID: 571 RVA: 0x0000B7C8 File Offset: 0x000099C8
		// (set) Token: 0x0600023C RID: 572 RVA: 0x0000B7D0 File Offset: 0x000099D0
		public CodeStatement IncrementStatement
		{
			get
			{
				return this.incrementStatement;
			}
			set
			{
				this.incrementStatement = value;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600023D RID: 573 RVA: 0x0000B7DC File Offset: 0x000099DC
		// (set) Token: 0x0600023E RID: 574 RVA: 0x0000B7E4 File Offset: 0x000099E4
		public CodeStatement InitStatement
		{
			get
			{
				return this.initStatement;
			}
			set
			{
				this.initStatement = value;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600023F RID: 575 RVA: 0x0000B7F0 File Offset: 0x000099F0
		public CodeStatementCollection Statements
		{
			get
			{
				if (this.statements == null)
				{
					this.statements = new CodeStatementCollection();
				}
				return this.statements;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000240 RID: 576 RVA: 0x0000B810 File Offset: 0x00009A10
		// (set) Token: 0x06000241 RID: 577 RVA: 0x0000B818 File Offset: 0x00009A18
		public CodeExpression TestExpression
		{
			get
			{
				return this.testExpression;
			}
			set
			{
				this.testExpression = value;
			}
		}

		// Token: 0x06000242 RID: 578 RVA: 0x0000B824 File Offset: 0x00009A24
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x040000A9 RID: 169
		private CodeStatement incrementStatement;

		// Token: 0x040000AA RID: 170
		private CodeStatement initStatement;

		// Token: 0x040000AB RID: 171
		private CodeStatementCollection statements;

		// Token: 0x040000AC RID: 172
		private CodeExpression testExpression;
	}
}
