﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000044 RID: 68
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeIndexerExpression : CodeExpression
	{
		// Token: 0x06000233 RID: 563 RVA: 0x0000B724 File Offset: 0x00009924
		public CodeIndexerExpression()
		{
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000B72C File Offset: 0x0000992C
		public CodeIndexerExpression(CodeExpression targetObject, params CodeExpression[] indices)
		{
			this.targetObject = targetObject;
			this.Indices.AddRange(indices);
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000235 RID: 565 RVA: 0x0000B748 File Offset: 0x00009948
		public CodeExpressionCollection Indices
		{
			get
			{
				if (this.indices == null)
				{
					this.indices = new CodeExpressionCollection();
				}
				return this.indices;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000236 RID: 566 RVA: 0x0000B768 File Offset: 0x00009968
		// (set) Token: 0x06000237 RID: 567 RVA: 0x0000B770 File Offset: 0x00009970
		public CodeExpression TargetObject
		{
			get
			{
				return this.targetObject;
			}
			set
			{
				this.targetObject = value;
			}
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000B77C File Offset: 0x0000997C
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x040000A7 RID: 167
		private CodeExpression targetObject;

		// Token: 0x040000A8 RID: 168
		private CodeExpressionCollection indices;
	}
}
