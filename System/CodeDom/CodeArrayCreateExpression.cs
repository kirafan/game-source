﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000022 RID: 34
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeArrayCreateExpression : CodeExpression
	{
		// Token: 0x06000131 RID: 305 RVA: 0x0000A368 File Offset: 0x00008568
		public CodeArrayCreateExpression()
		{
		}

		// Token: 0x06000132 RID: 306 RVA: 0x0000A370 File Offset: 0x00008570
		public CodeArrayCreateExpression(CodeTypeReference createType, CodeExpression size)
		{
			this.createType = createType;
			this.sizeExpression = size;
		}

		// Token: 0x06000133 RID: 307 RVA: 0x0000A388 File Offset: 0x00008588
		public CodeArrayCreateExpression(CodeTypeReference createType, params CodeExpression[] initializers)
		{
			this.createType = createType;
			this.Initializers.AddRange(initializers);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x0000A3A4 File Offset: 0x000085A4
		public CodeArrayCreateExpression(CodeTypeReference createType, int size)
		{
			this.createType = createType;
			this.size = size;
		}

		// Token: 0x06000135 RID: 309 RVA: 0x0000A3BC File Offset: 0x000085BC
		public CodeArrayCreateExpression(string createType, CodeExpression size)
		{
			this.createType = new CodeTypeReference(createType);
			this.sizeExpression = size;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000A3D8 File Offset: 0x000085D8
		public CodeArrayCreateExpression(string createType, params CodeExpression[] initializers)
		{
			this.createType = new CodeTypeReference(createType);
			this.Initializers.AddRange(initializers);
		}

		// Token: 0x06000137 RID: 311 RVA: 0x0000A3F8 File Offset: 0x000085F8
		public CodeArrayCreateExpression(string createType, int size)
		{
			this.createType = new CodeTypeReference(createType);
			this.size = size;
		}

		// Token: 0x06000138 RID: 312 RVA: 0x0000A414 File Offset: 0x00008614
		public CodeArrayCreateExpression(Type createType, CodeExpression size)
		{
			this.createType = new CodeTypeReference(createType);
			this.sizeExpression = size;
		}

		// Token: 0x06000139 RID: 313 RVA: 0x0000A430 File Offset: 0x00008630
		public CodeArrayCreateExpression(Type createType, params CodeExpression[] initializers)
		{
			this.createType = new CodeTypeReference(createType);
			this.Initializers.AddRange(initializers);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x0000A450 File Offset: 0x00008650
		public CodeArrayCreateExpression(Type createType, int size)
		{
			this.createType = new CodeTypeReference(createType);
			this.size = size;
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600013B RID: 315 RVA: 0x0000A46C File Offset: 0x0000866C
		// (set) Token: 0x0600013C RID: 316 RVA: 0x0000A4A0 File Offset: 0x000086A0
		public CodeTypeReference CreateType
		{
			get
			{
				if (this.createType == null)
				{
					this.createType = new CodeTypeReference(typeof(void));
				}
				return this.createType;
			}
			set
			{
				this.createType = value;
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600013D RID: 317 RVA: 0x0000A4AC File Offset: 0x000086AC
		public CodeExpressionCollection Initializers
		{
			get
			{
				if (this.initializers == null)
				{
					this.initializers = new CodeExpressionCollection();
				}
				return this.initializers;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600013E RID: 318 RVA: 0x0000A4CC File Offset: 0x000086CC
		// (set) Token: 0x0600013F RID: 319 RVA: 0x0000A4D4 File Offset: 0x000086D4
		public CodeExpression SizeExpression
		{
			get
			{
				return this.sizeExpression;
			}
			set
			{
				this.sizeExpression = value;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000140 RID: 320 RVA: 0x0000A4E0 File Offset: 0x000086E0
		// (set) Token: 0x06000141 RID: 321 RVA: 0x0000A4E8 File Offset: 0x000086E8
		public int Size
		{
			get
			{
				return this.size;
			}
			set
			{
				this.size = value;
			}
		}

		// Token: 0x06000142 RID: 322 RVA: 0x0000A4F4 File Offset: 0x000086F4
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x04000060 RID: 96
		private CodeTypeReference createType;

		// Token: 0x04000061 RID: 97
		private CodeExpressionCollection initializers;

		// Token: 0x04000062 RID: 98
		private CodeExpression sizeExpression;

		// Token: 0x04000063 RID: 99
		private int size;
	}
}
