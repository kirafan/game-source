﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003C RID: 60
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeDirective : CodeObject
	{
	}
}
