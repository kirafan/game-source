﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200006F RID: 111
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTypeReferenceCollection : CollectionBase
	{
		// Token: 0x060003A2 RID: 930 RVA: 0x0000D7B0 File Offset: 0x0000B9B0
		public CodeTypeReferenceCollection()
		{
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x0000D7B8 File Offset: 0x0000B9B8
		public CodeTypeReferenceCollection(CodeTypeReference[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000D7C8 File Offset: 0x0000B9C8
		public CodeTypeReferenceCollection(CodeTypeReferenceCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x170000B8 RID: 184
		public CodeTypeReference this[int index]
		{
			get
			{
				return (CodeTypeReference)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0000D7FC File Offset: 0x0000B9FC
		public int Add(CodeTypeReference value)
		{
			return base.List.Add(value);
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0000D80C File Offset: 0x0000BA0C
		public void Add(string value)
		{
			this.Add(new CodeTypeReference(value));
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x0000D81C File Offset: 0x0000BA1C
		public void Add(Type value)
		{
			this.Add(new CodeTypeReference(value));
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0000D82C File Offset: 0x0000BA2C
		public void AddRange(CodeTypeReference[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000D868 File Offset: 0x0000BA68
		public void AddRange(CodeTypeReferenceCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060003AC RID: 940 RVA: 0x0000D8B0 File Offset: 0x0000BAB0
		public bool Contains(CodeTypeReference value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0000D8C0 File Offset: 0x0000BAC0
		public void CopyTo(CodeTypeReference[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x0000D8D0 File Offset: 0x0000BAD0
		public int IndexOf(CodeTypeReference value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0000D8E0 File Offset: 0x0000BAE0
		public void Insert(int index, CodeTypeReference value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x0000D8F0 File Offset: 0x0000BAF0
		public void Remove(CodeTypeReference value)
		{
			base.List.Remove(value);
		}
	}
}
