﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200004E RID: 78
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeMethodReturnStatement : CodeStatement
	{
		// Token: 0x06000290 RID: 656 RVA: 0x0000BF44 File Offset: 0x0000A144
		public CodeMethodReturnStatement()
		{
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000BF4C File Offset: 0x0000A14C
		public CodeMethodReturnStatement(CodeExpression expression)
		{
			this.expression = expression;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000292 RID: 658 RVA: 0x0000BF5C File Offset: 0x0000A15C
		// (set) Token: 0x06000293 RID: 659 RVA: 0x0000BF64 File Offset: 0x0000A164
		public CodeExpression Expression
		{
			get
			{
				return this.expression;
			}
			set
			{
				this.expression = value;
			}
		}

		// Token: 0x06000294 RID: 660 RVA: 0x0000BF70 File Offset: 0x0000A170
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x040000CE RID: 206
		private CodeExpression expression;
	}
}
