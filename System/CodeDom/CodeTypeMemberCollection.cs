﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200006A RID: 106
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeMemberCollection : CollectionBase
	{
		// Token: 0x0600036C RID: 876 RVA: 0x0000D33C File Offset: 0x0000B53C
		public CodeTypeMemberCollection()
		{
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000D344 File Offset: 0x0000B544
		public CodeTypeMemberCollection(CodeTypeMember[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000D354 File Offset: 0x0000B554
		public CodeTypeMemberCollection(CodeTypeMemberCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x170000AA RID: 170
		public CodeTypeMember this[int index]
		{
			get
			{
				return (CodeTypeMember)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000D388 File Offset: 0x0000B588
		public int Add(CodeTypeMember value)
		{
			return base.List.Add(value);
		}

		// Token: 0x06000372 RID: 882 RVA: 0x0000D398 File Offset: 0x0000B598
		public void AddRange(CodeTypeMember[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000373 RID: 883 RVA: 0x0000D3D4 File Offset: 0x0000B5D4
		public void AddRange(CodeTypeMemberCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000D41C File Offset: 0x0000B61C
		public bool Contains(CodeTypeMember value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000D42C File Offset: 0x0000B62C
		public void CopyTo(CodeTypeMember[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0000D43C File Offset: 0x0000B63C
		public int IndexOf(CodeTypeMember value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000D44C File Offset: 0x0000B64C
		public void Insert(int index, CodeTypeMember value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000D45C File Offset: 0x0000B65C
		public void Remove(CodeTypeMember value)
		{
			base.List.Remove(value);
		}
	}
}
