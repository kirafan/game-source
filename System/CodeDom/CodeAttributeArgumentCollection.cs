﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000026 RID: 38
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeAttributeArgumentCollection : CollectionBase
	{
		// Token: 0x06000158 RID: 344 RVA: 0x0000A640 File Offset: 0x00008840
		public CodeAttributeArgumentCollection()
		{
		}

		// Token: 0x06000159 RID: 345 RVA: 0x0000A648 File Offset: 0x00008848
		public CodeAttributeArgumentCollection(CodeAttributeArgument[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600015A RID: 346 RVA: 0x0000A658 File Offset: 0x00008858
		public CodeAttributeArgumentCollection(CodeAttributeArgumentCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x1700001E RID: 30
		public CodeAttributeArgument this[int index]
		{
			get
			{
				return (CodeAttributeArgument)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x0600015D RID: 349 RVA: 0x0000A68C File Offset: 0x0000888C
		public int Add(CodeAttributeArgument value)
		{
			return base.List.Add(value);
		}

		// Token: 0x0600015E RID: 350 RVA: 0x0000A69C File Offset: 0x0000889C
		public void AddRange(CodeAttributeArgument[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600015F RID: 351 RVA: 0x0000A6D8 File Offset: 0x000088D8
		public void AddRange(CodeAttributeArgumentCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0000A720 File Offset: 0x00008920
		public bool Contains(CodeAttributeArgument value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x06000161 RID: 353 RVA: 0x0000A730 File Offset: 0x00008930
		public void CopyTo(CodeAttributeArgument[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06000162 RID: 354 RVA: 0x0000A740 File Offset: 0x00008940
		public int IndexOf(CodeAttributeArgument value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000163 RID: 355 RVA: 0x0000A750 File Offset: 0x00008950
		public void Insert(int index, CodeAttributeArgument value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000164 RID: 356 RVA: 0x0000A760 File Offset: 0x00008960
		public void Remove(CodeAttributeArgument value)
		{
			base.List.Remove(value);
		}
	}
}
