﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000067 RID: 103
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeTypeDeclarationCollection : CollectionBase
	{
		// Token: 0x06000345 RID: 837 RVA: 0x0000CF28 File Offset: 0x0000B128
		public CodeTypeDeclarationCollection()
		{
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000CF30 File Offset: 0x0000B130
		public CodeTypeDeclarationCollection(CodeTypeDeclaration[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000CF40 File Offset: 0x0000B140
		public CodeTypeDeclarationCollection(CodeTypeDeclarationCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x1700009E RID: 158
		public CodeTypeDeclaration this[int index]
		{
			get
			{
				return (CodeTypeDeclaration)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000CF74 File Offset: 0x0000B174
		public int Add(CodeTypeDeclaration value)
		{
			return base.List.Add(value);
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000CF84 File Offset: 0x0000B184
		public void AddRange(CodeTypeDeclaration[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000CFC0 File Offset: 0x0000B1C0
		public void AddRange(CodeTypeDeclarationCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000D008 File Offset: 0x0000B208
		public bool Contains(CodeTypeDeclaration value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000D018 File Offset: 0x0000B218
		public void CopyTo(CodeTypeDeclaration[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000D028 File Offset: 0x0000B228
		public int IndexOf(CodeTypeDeclaration value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000D038 File Offset: 0x0000B238
		public void Insert(int index, CodeTypeDeclaration value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000D048 File Offset: 0x0000B248
		public void Remove(CodeTypeDeclaration value)
		{
			base.List.Remove(value);
		}
	}
}
