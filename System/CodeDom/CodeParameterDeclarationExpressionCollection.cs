﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000055 RID: 85
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeParameterDeclarationExpressionCollection : CollectionBase
	{
		// Token: 0x060002D8 RID: 728 RVA: 0x0000C758 File Offset: 0x0000A958
		public CodeParameterDeclarationExpressionCollection()
		{
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000C760 File Offset: 0x0000A960
		public CodeParameterDeclarationExpressionCollection(CodeParameterDeclarationExpression[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0000C770 File Offset: 0x0000A970
		public CodeParameterDeclarationExpressionCollection(CodeParameterDeclarationExpressionCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000085 RID: 133
		public CodeParameterDeclarationExpression this[int index]
		{
			get
			{
				return (CodeParameterDeclarationExpression)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000C7A4 File Offset: 0x0000A9A4
		public int Add(CodeParameterDeclarationExpression value)
		{
			return base.List.Add(value);
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000C7B4 File Offset: 0x0000A9B4
		public void AddRange(CodeParameterDeclarationExpression[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000C7F0 File Offset: 0x0000A9F0
		public void AddRange(CodeParameterDeclarationExpressionCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000C838 File Offset: 0x0000AA38
		public bool Contains(CodeParameterDeclarationExpression value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000C848 File Offset: 0x0000AA48
		public void CopyTo(CodeParameterDeclarationExpression[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000C858 File Offset: 0x0000AA58
		public int IndexOf(CodeParameterDeclarationExpression value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000C868 File Offset: 0x0000AA68
		public void Insert(int index, CodeParameterDeclarationExpression value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000C878 File Offset: 0x0000AA78
		public void Remove(CodeParameterDeclarationExpression value)
		{
			base.List.Remove(value);
		}
	}
}
