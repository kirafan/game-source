﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003D RID: 61
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeEntryPointMethod : CodeMemberMethod
	{
		// Token: 0x0600020B RID: 523 RVA: 0x0000B478 File Offset: 0x00009678
		public CodeEntryPointMethod()
		{
			base.Attributes = (MemberAttributes)24579;
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0000B48C File Offset: 0x0000968C
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}
	}
}
