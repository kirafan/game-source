﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000091 RID: 145
	[ComVisible(true)]
	[Serializable]
	public enum MemberAttributes
	{
		// Token: 0x04000191 RID: 401
		Abstract = 1,
		// Token: 0x04000192 RID: 402
		Final,
		// Token: 0x04000193 RID: 403
		Static,
		// Token: 0x04000194 RID: 404
		Override,
		// Token: 0x04000195 RID: 405
		Const,
		// Token: 0x04000196 RID: 406
		ScopeMask = 15,
		// Token: 0x04000197 RID: 407
		New,
		// Token: 0x04000198 RID: 408
		VTableMask = 240,
		// Token: 0x04000199 RID: 409
		Overloaded = 256,
		// Token: 0x0400019A RID: 410
		Assembly = 4096,
		// Token: 0x0400019B RID: 411
		FamilyAndAssembly = 8192,
		// Token: 0x0400019C RID: 412
		Family = 12288,
		// Token: 0x0400019D RID: 413
		FamilyOrAssembly = 16384,
		// Token: 0x0400019E RID: 414
		Private = 20480,
		// Token: 0x0400019F RID: 415
		Public = 24576,
		// Token: 0x040001A0 RID: 416
		AccessMask = 61440
	}
}
