﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200005B RID: 91
	[ComVisible(true)]
	[Serializable]
	public enum CodeRegionMode
	{
		// Token: 0x040000E9 RID: 233
		None,
		// Token: 0x040000EA RID: 234
		Start,
		// Token: 0x040000EB RID: 235
		End
	}
}
