﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000065 RID: 101
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeTryCatchFinallyStatement : CodeStatement
	{
		// Token: 0x0600033C RID: 828 RVA: 0x0000CE30 File Offset: 0x0000B030
		public CodeTryCatchFinallyStatement()
		{
		}

		// Token: 0x0600033D RID: 829 RVA: 0x0000CE38 File Offset: 0x0000B038
		public CodeTryCatchFinallyStatement(CodeStatement[] tryStatements, CodeCatchClause[] catchClauses)
		{
			this.TryStatements.AddRange(tryStatements);
			this.CatchClauses.AddRange(catchClauses);
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000CE64 File Offset: 0x0000B064
		public CodeTryCatchFinallyStatement(CodeStatement[] tryStatements, CodeCatchClause[] catchClauses, CodeStatement[] finallyStatements)
		{
			this.TryStatements.AddRange(tryStatements);
			this.CatchClauses.AddRange(catchClauses);
			this.FinallyStatements.AddRange(finallyStatements);
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600033F RID: 831 RVA: 0x0000CE9C File Offset: 0x0000B09C
		public CodeStatementCollection FinallyStatements
		{
			get
			{
				if (this.finallyStatements == null)
				{
					this.finallyStatements = new CodeStatementCollection();
				}
				return this.finallyStatements;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000340 RID: 832 RVA: 0x0000CEBC File Offset: 0x0000B0BC
		public CodeStatementCollection TryStatements
		{
			get
			{
				if (this.tryStatements == null)
				{
					this.tryStatements = new CodeStatementCollection();
				}
				return this.tryStatements;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000341 RID: 833 RVA: 0x0000CEDC File Offset: 0x0000B0DC
		public CodeCatchClauseCollection CatchClauses
		{
			get
			{
				if (this.catchClauses == null)
				{
					this.catchClauses = new CodeCatchClauseCollection();
				}
				return this.catchClauses;
			}
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000CEFC File Offset: 0x0000B0FC
		internal override void Accept(ICodeDomVisitor visitor)
		{
			visitor.Visit(this);
		}

		// Token: 0x040000F7 RID: 247
		private CodeStatementCollection tryStatements;

		// Token: 0x040000F8 RID: 248
		private CodeStatementCollection finallyStatements;

		// Token: 0x040000F9 RID: 249
		private CodeCatchClauseCollection catchClauses;
	}
}
