﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000032 RID: 50
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeCommentStatementCollection : CollectionBase
	{
		// Token: 0x060001BB RID: 443 RVA: 0x0000AE30 File Offset: 0x00009030
		public CodeCommentStatementCollection()
		{
		}

		// Token: 0x060001BC RID: 444 RVA: 0x0000AE38 File Offset: 0x00009038
		public CodeCommentStatementCollection(CodeCommentStatement[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060001BD RID: 445 RVA: 0x0000AE48 File Offset: 0x00009048
		public CodeCommentStatementCollection(CodeCommentStatementCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000033 RID: 51
		public CodeCommentStatement this[int index]
		{
			get
			{
				return (CodeCommentStatement)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x0000AE7C File Offset: 0x0000907C
		public int Add(CodeCommentStatement value)
		{
			return base.List.Add(value);
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x0000AE8C File Offset: 0x0000908C
		public void AddRange(CodeCommentStatement[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x0000AEC8 File Offset: 0x000090C8
		public void AddRange(CodeCommentStatementCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x0000AF10 File Offset: 0x00009110
		public bool Contains(CodeCommentStatement value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x0000AF20 File Offset: 0x00009120
		public void CopyTo(CodeCommentStatement[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x0000AF30 File Offset: 0x00009130
		public int IndexOf(CodeCommentStatement value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x0000AF40 File Offset: 0x00009140
		public void Insert(int index, CodeCommentStatement value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000AF50 File Offset: 0x00009150
		public void Remove(CodeCommentStatement value)
		{
			base.List.Remove(value);
		}
	}
}
