﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200003F RID: 63
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeExpressionCollection : CollectionBase
	{
		// Token: 0x06000214 RID: 532 RVA: 0x0000B500 File Offset: 0x00009700
		public CodeExpressionCollection()
		{
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000B508 File Offset: 0x00009708
		public CodeExpressionCollection(CodeExpression[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0000B518 File Offset: 0x00009718
		public CodeExpressionCollection(CodeExpressionCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x1700004A RID: 74
		public CodeExpression this[int index]
		{
			get
			{
				return (CodeExpression)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0000B54C File Offset: 0x0000974C
		public int Add(CodeExpression value)
		{
			return base.List.Add(value);
		}

		// Token: 0x0600021A RID: 538 RVA: 0x0000B55C File Offset: 0x0000975C
		public void AddRange(CodeExpression[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000B598 File Offset: 0x00009798
		public void AddRange(CodeExpressionCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0000B5E0 File Offset: 0x000097E0
		public bool Contains(CodeExpression value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0000B5F0 File Offset: 0x000097F0
		public void CopyTo(CodeExpression[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000B600 File Offset: 0x00009800
		public int IndexOf(CodeExpression value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x0600021F RID: 543 RVA: 0x0000B610 File Offset: 0x00009810
		public void Insert(int index, CodeExpression value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x06000220 RID: 544 RVA: 0x0000B620 File Offset: 0x00009820
		public void Remove(CodeExpression value)
		{
			base.List.Remove(value);
		}
	}
}
