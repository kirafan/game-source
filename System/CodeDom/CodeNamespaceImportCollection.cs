﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000051 RID: 81
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[ComVisible(true)]
	[Serializable]
	public class CodeNamespaceImportCollection : IList, ICollection, IEnumerable
	{
		// Token: 0x060002AF RID: 687 RVA: 0x0000C26C File Offset: 0x0000A46C
		public CodeNamespaceImportCollection()
		{
			this.data = new ArrayList();
			this.keys = new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060002B0 RID: 688 RVA: 0x0000C2A0 File Offset: 0x0000A4A0
		int ICollection.Count
		{
			get
			{
				return this.data.Count;
			}
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000C2B0 File Offset: 0x0000A4B0
		void IList.Clear()
		{
			this.Clear();
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000C2B8 File Offset: 0x0000A4B8
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060002B3 RID: 691 RVA: 0x0000C2BC File Offset: 0x0000A4BC
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700007B RID: 123
		object IList.this[int index]
		{
			get
			{
				return this.data[index];
			}
			set
			{
				this[index] = (CodeNamespaceImport)value;
			}
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000C2E0 File Offset: 0x0000A4E0
		int IList.Add(object value)
		{
			this.Add((CodeNamespaceImport)value);
			return this.data.Count - 1;
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x0000C2FC File Offset: 0x0000A4FC
		bool IList.Contains(object value)
		{
			return this.data.Contains(value);
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0000C30C File Offset: 0x0000A50C
		int IList.IndexOf(object value)
		{
			return this.data.IndexOf(value);
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000C31C File Offset: 0x0000A51C
		void IList.Insert(int index, object value)
		{
			this.data.Insert(index, value);
			CodeNamespaceImport codeNamespaceImport = (CodeNamespaceImport)value;
			this.keys[codeNamespaceImport.Namespace] = codeNamespaceImport;
		}

		// Token: 0x060002BA RID: 698 RVA: 0x0000C350 File Offset: 0x0000A550
		void IList.Remove(object value)
		{
			string @namespace = ((CodeNamespaceImport)value).Namespace;
			this.data.Remove(value);
			foreach (object obj in this.data)
			{
				CodeNamespaceImport codeNamespaceImport = (CodeNamespaceImport)obj;
				if (codeNamespaceImport.Namespace == @namespace)
				{
					this.keys[@namespace] = codeNamespaceImport;
					return;
				}
			}
			this.keys.Remove(@namespace);
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0000C400 File Offset: 0x0000A600
		void IList.RemoveAt(int index)
		{
			string @namespace = this[index].Namespace;
			this.data.RemoveAt(index);
			foreach (object obj in this.data)
			{
				CodeNamespaceImport codeNamespaceImport = (CodeNamespaceImport)obj;
				if (codeNamespaceImport.Namespace == @namespace)
				{
					this.keys[@namespace] = codeNamespaceImport;
					return;
				}
			}
			this.keys.Remove(@namespace);
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060002BC RID: 700 RVA: 0x0000C4B0 File Offset: 0x0000A6B0
		object ICollection.SyncRoot
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060002BD RID: 701 RVA: 0x0000C4B4 File Offset: 0x0000A6B4
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.data.IsSynchronized;
			}
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0000C4C4 File Offset: 0x0000A6C4
		void ICollection.CopyTo(Array array, int index)
		{
			this.data.CopyTo(array, index);
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000C4D4 File Offset: 0x0000A6D4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.data.GetEnumerator();
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060002C0 RID: 704 RVA: 0x0000C4E4 File Offset: 0x0000A6E4
		public int Count
		{
			get
			{
				return this.data.Count;
			}
		}

		// Token: 0x1700007F RID: 127
		public CodeNamespaceImport this[int index]
		{
			get
			{
				return (CodeNamespaceImport)this.data[index];
			}
			set
			{
				CodeNamespaceImport codeNamespaceImport = (CodeNamespaceImport)this.data[index];
				this.keys.Remove(codeNamespaceImport.Namespace);
				this.data[index] = value;
				this.keys[value.Namespace] = value;
			}
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x0000C55C File Offset: 0x0000A75C
		public void Add(CodeNamespaceImport value)
		{
			if (value == null)
			{
				throw new NullReferenceException();
			}
			if (!this.keys.ContainsKey(value.Namespace))
			{
				this.keys[value.Namespace] = value;
				this.data.Add(value);
			}
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x0000C5AC File Offset: 0x0000A7AC
		public void AddRange(CodeNamespaceImport[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			foreach (CodeNamespaceImport value2 in value)
			{
				this.Add(value2);
			}
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x0000C5EC File Offset: 0x0000A7EC
		public void Clear()
		{
			this.data.Clear();
			this.keys.Clear();
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x0000C604 File Offset: 0x0000A804
		public IEnumerator GetEnumerator()
		{
			return this.data.GetEnumerator();
		}

		// Token: 0x040000D8 RID: 216
		private Hashtable keys;

		// Token: 0x040000D9 RID: 217
		private ArrayList data;
	}
}
