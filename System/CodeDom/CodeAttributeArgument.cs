﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000027 RID: 39
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeAttributeArgument
	{
		// Token: 0x06000165 RID: 357 RVA: 0x0000A770 File Offset: 0x00008970
		public CodeAttributeArgument()
		{
		}

		// Token: 0x06000166 RID: 358 RVA: 0x0000A778 File Offset: 0x00008978
		public CodeAttributeArgument(CodeExpression value)
		{
			this.value = value;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x0000A788 File Offset: 0x00008988
		public CodeAttributeArgument(string name, CodeExpression value)
		{
			this.name = name;
			this.value = value;
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000168 RID: 360 RVA: 0x0000A7A0 File Offset: 0x000089A0
		// (set) Token: 0x06000169 RID: 361 RVA: 0x0000A7BC File Offset: 0x000089BC
		public string Name
		{
			get
			{
				if (this.name == null)
				{
					return string.Empty;
				}
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600016A RID: 362 RVA: 0x0000A7C8 File Offset: 0x000089C8
		// (set) Token: 0x0600016B RID: 363 RVA: 0x0000A7D0 File Offset: 0x000089D0
		public CodeExpression Value
		{
			get
			{
				return this.value;
			}
			set
			{
				this.value = value;
			}
		}

		// Token: 0x0400006A RID: 106
		private string name;

		// Token: 0x0400006B RID: 107
		private CodeExpression value;
	}
}
