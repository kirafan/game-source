﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000090 RID: 144
	[ComVisible(true)]
	[Serializable]
	public enum FieldDirection
	{
		// Token: 0x0400018D RID: 397
		In,
		// Token: 0x0400018E RID: 398
		Out,
		// Token: 0x0400018F RID: 399
		Ref
	}
}
