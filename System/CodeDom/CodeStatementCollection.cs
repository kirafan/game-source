﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000061 RID: 97
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeStatementCollection : CollectionBase
	{
		// Token: 0x06000322 RID: 802 RVA: 0x0000CC48 File Offset: 0x0000AE48
		public CodeStatementCollection()
		{
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000CC50 File Offset: 0x0000AE50
		public CodeStatementCollection(CodeStatement[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000CC60 File Offset: 0x0000AE60
		public CodeStatementCollection(CodeStatementCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000096 RID: 150
		public CodeStatement this[int index]
		{
			get
			{
				return (CodeStatement)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000CC94 File Offset: 0x0000AE94
		public int Add(CodeStatement value)
		{
			return base.List.Add(value);
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000CCA4 File Offset: 0x0000AEA4
		public int Add(CodeExpression value)
		{
			return this.Add(new CodeExpressionStatement(value));
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000CCB4 File Offset: 0x0000AEB4
		public void AddRange(CodeStatement[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000CCF0 File Offset: 0x0000AEF0
		public void AddRange(CodeStatementCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			int count = value.Count;
			for (int i = 0; i < count; i++)
			{
				this.Add(value[i]);
			}
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000CD38 File Offset: 0x0000AF38
		public bool Contains(CodeStatement value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000CD48 File Offset: 0x0000AF48
		public void CopyTo(CodeStatement[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000CD58 File Offset: 0x0000AF58
		public int IndexOf(CodeStatement value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000CD68 File Offset: 0x0000AF68
		public void Insert(int index, CodeStatement value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000CD78 File Offset: 0x0000AF78
		public void Remove(CodeStatement value)
		{
			base.List.Remove(value);
		}
	}
}
