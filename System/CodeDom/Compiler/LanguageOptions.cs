﻿using System;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200008E RID: 142
	[Flags]
	[Serializable]
	public enum LanguageOptions
	{
		// Token: 0x04000184 RID: 388
		None = 0,
		// Token: 0x04000185 RID: 389
		CaseInsensitive = 1
	}
}
