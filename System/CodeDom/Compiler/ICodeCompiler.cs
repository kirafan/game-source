﻿using System;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200008A RID: 138
	public interface ICodeCompiler
	{
		// Token: 0x060005A2 RID: 1442
		CompilerResults CompileAssemblyFromDom(CompilerParameters options, CodeCompileUnit compilationUnit);

		// Token: 0x060005A3 RID: 1443
		CompilerResults CompileAssemblyFromDomBatch(CompilerParameters options, CodeCompileUnit[] batch);

		// Token: 0x060005A4 RID: 1444
		CompilerResults CompileAssemblyFromFile(CompilerParameters options, string fileName);

		// Token: 0x060005A5 RID: 1445
		CompilerResults CompileAssemblyFromFileBatch(CompilerParameters options, string[] batch);

		// Token: 0x060005A6 RID: 1446
		CompilerResults CompileAssemblyFromSource(CompilerParameters options, string source);

		// Token: 0x060005A7 RID: 1447
		CompilerResults CompileAssemblyFromSourceBatch(CompilerParameters options, string[] batch);
	}
}
