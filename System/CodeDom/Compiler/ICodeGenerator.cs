﻿using System;
using System.IO;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200008B RID: 139
	public interface ICodeGenerator
	{
		// Token: 0x060005A8 RID: 1448
		string CreateEscapedIdentifier(string value);

		// Token: 0x060005A9 RID: 1449
		string CreateValidIdentifier(string value);

		// Token: 0x060005AA RID: 1450
		void GenerateCodeFromCompileUnit(CodeCompileUnit compileUnit, TextWriter output, CodeGeneratorOptions options);

		// Token: 0x060005AB RID: 1451
		void GenerateCodeFromExpression(CodeExpression expression, TextWriter output, CodeGeneratorOptions options);

		// Token: 0x060005AC RID: 1452
		void GenerateCodeFromNamespace(CodeNamespace ns, TextWriter output, CodeGeneratorOptions options);

		// Token: 0x060005AD RID: 1453
		void GenerateCodeFromStatement(CodeStatement statement, TextWriter output, CodeGeneratorOptions options);

		// Token: 0x060005AE RID: 1454
		void GenerateCodeFromType(CodeTypeDeclaration typeDeclaration, TextWriter output, CodeGeneratorOptions options);

		// Token: 0x060005AF RID: 1455
		string GetTypeOutput(CodeTypeReference type);

		// Token: 0x060005B0 RID: 1456
		bool IsValidIdentifier(string value);

		// Token: 0x060005B1 RID: 1457
		bool Supports(GeneratorSupport supports);

		// Token: 0x060005B2 RID: 1458
		void ValidateIdentifier(string value);
	}
}
