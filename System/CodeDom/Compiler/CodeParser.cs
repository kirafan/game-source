﻿using System;
using System.IO;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200007C RID: 124
	public abstract class CodeParser : ICodeParser
	{
		// Token: 0x06000506 RID: 1286
		public abstract CodeCompileUnit Parse(TextReader codeStream);
	}
}
