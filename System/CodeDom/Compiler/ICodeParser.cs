﻿using System;
using System.IO;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200008C RID: 140
	public interface ICodeParser
	{
		// Token: 0x060005B3 RID: 1459
		CodeCompileUnit Parse(TextReader codeStream);
	}
}
