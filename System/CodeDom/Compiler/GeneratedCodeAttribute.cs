﻿using System;

namespace System.CodeDom.Compiler
{
	// Token: 0x02000088 RID: 136
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
	public sealed class GeneratedCodeAttribute : Attribute
	{
		// Token: 0x0600059F RID: 1439 RVA: 0x00011B20 File Offset: 0x0000FD20
		public GeneratedCodeAttribute(string tool, string version)
		{
			this.tool = tool;
			this.version = version;
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x060005A0 RID: 1440 RVA: 0x00011B38 File Offset: 0x0000FD38
		public string Tool
		{
			get
			{
				return this.tool;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x060005A1 RID: 1441 RVA: 0x00011B40 File Offset: 0x0000FD40
		public string Version
		{
			get
			{
				return this.version;
			}
		}

		// Token: 0x04000161 RID: 353
		private string tool;

		// Token: 0x04000162 RID: 354
		private string version;
	}
}
