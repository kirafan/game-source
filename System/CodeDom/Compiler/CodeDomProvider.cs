﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.CodeDom.Compiler
{
	// Token: 0x02000078 RID: 120
	[ComVisible(true)]
	[System.ComponentModel.ToolboxItem(false)]
	public abstract class CodeDomProvider : System.ComponentModel.Component
	{
		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x0600042A RID: 1066 RVA: 0x0000E4D8 File Offset: 0x0000C6D8
		public virtual string FileExtension
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x0600042B RID: 1067 RVA: 0x0000E4E0 File Offset: 0x0000C6E0
		public virtual LanguageOptions LanguageOptions
		{
			get
			{
				return LanguageOptions.None;
			}
		}

		// Token: 0x0600042C RID: 1068
		[Obsolete("ICodeCompiler is obsolete")]
		public abstract ICodeCompiler CreateCompiler();

		// Token: 0x0600042D RID: 1069
		[Obsolete("ICodeGenerator is obsolete")]
		public abstract ICodeGenerator CreateGenerator();

		// Token: 0x0600042E RID: 1070 RVA: 0x0000E4E4 File Offset: 0x0000C6E4
		public virtual ICodeGenerator CreateGenerator(string fileName)
		{
			return this.CreateGenerator();
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0000E4EC File Offset: 0x0000C6EC
		public virtual ICodeGenerator CreateGenerator(TextWriter output)
		{
			return this.CreateGenerator();
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0000E4F4 File Offset: 0x0000C6F4
		[Obsolete("ICodeParser is obsolete")]
		public virtual ICodeParser CreateParser()
		{
			return null;
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x0000E4F8 File Offset: 0x0000C6F8
		public virtual System.ComponentModel.TypeConverter GetConverter(Type type)
		{
			return System.ComponentModel.TypeDescriptor.GetConverter(type);
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x0000E500 File Offset: 0x0000C700
		public virtual CompilerResults CompileAssemblyFromDom(CompilerParameters options, params CodeCompileUnit[] compilationUnits)
		{
			ICodeCompiler codeCompiler = this.CreateCompiler();
			if (codeCompiler == null)
			{
				throw this.GetNotImplemented();
			}
			return codeCompiler.CompileAssemblyFromDomBatch(options, compilationUnits);
		}

		// Token: 0x06000433 RID: 1075 RVA: 0x0000E52C File Offset: 0x0000C72C
		public virtual CompilerResults CompileAssemblyFromFile(CompilerParameters options, params string[] fileNames)
		{
			ICodeCompiler codeCompiler = this.CreateCompiler();
			if (codeCompiler == null)
			{
				throw this.GetNotImplemented();
			}
			return codeCompiler.CompileAssemblyFromFileBatch(options, fileNames);
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x0000E558 File Offset: 0x0000C758
		public virtual CompilerResults CompileAssemblyFromSource(CompilerParameters options, params string[] fileNames)
		{
			ICodeCompiler codeCompiler = this.CreateCompiler();
			if (codeCompiler == null)
			{
				throw this.GetNotImplemented();
			}
			return codeCompiler.CompileAssemblyFromSourceBatch(options, fileNames);
		}

		// Token: 0x06000435 RID: 1077 RVA: 0x0000E584 File Offset: 0x0000C784
		public virtual string CreateEscapedIdentifier(string value)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			return codeGenerator.CreateEscapedIdentifier(value);
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x0000E5AC File Offset: 0x0000C7AC
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static CodeDomProvider CreateProvider(string language)
		{
			CompilerInfo compilerInfo = CodeDomProvider.GetCompilerInfo(language);
			return (compilerInfo != null) ? compilerInfo.CreateProvider() : null;
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x0000E5D4 File Offset: 0x0000C7D4
		public virtual string CreateValidIdentifier(string value)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			return codeGenerator.CreateValidIdentifier(value);
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x0000E5FC File Offset: 0x0000C7FC
		public virtual void GenerateCodeFromCompileUnit(CodeCompileUnit compileUnit, TextWriter writer, CodeGeneratorOptions options)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			codeGenerator.GenerateCodeFromCompileUnit(compileUnit, writer, options);
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x0000E628 File Offset: 0x0000C828
		public virtual void GenerateCodeFromExpression(CodeExpression expression, TextWriter writer, CodeGeneratorOptions options)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			codeGenerator.GenerateCodeFromExpression(expression, writer, options);
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x0000E654 File Offset: 0x0000C854
		public virtual void GenerateCodeFromMember(CodeTypeMember member, TextWriter writer, CodeGeneratorOptions options)
		{
			throw this.GetNotImplemented();
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0000E65C File Offset: 0x0000C85C
		public virtual void GenerateCodeFromNamespace(CodeNamespace codeNamespace, TextWriter writer, CodeGeneratorOptions options)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			codeGenerator.GenerateCodeFromNamespace(codeNamespace, writer, options);
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0000E688 File Offset: 0x0000C888
		public virtual void GenerateCodeFromStatement(CodeStatement statement, TextWriter writer, CodeGeneratorOptions options)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			codeGenerator.GenerateCodeFromStatement(statement, writer, options);
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0000E6B4 File Offset: 0x0000C8B4
		public virtual void GenerateCodeFromType(CodeTypeDeclaration codeType, TextWriter writer, CodeGeneratorOptions options)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			codeGenerator.GenerateCodeFromType(codeType, writer, options);
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x0000E6E0 File Offset: 0x0000C8E0
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static CompilerInfo[] GetAllCompilerInfo()
		{
			return (CodeDomProvider.Config != null) ? CodeDomProvider.Config.CompilerInfos : null;
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0000E6FC File Offset: 0x0000C8FC
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static CompilerInfo GetCompilerInfo(string language)
		{
			if (language == null)
			{
				throw new ArgumentNullException("language");
			}
			if (CodeDomProvider.Config == null)
			{
				return null;
			}
			CompilerCollection compilers = CodeDomProvider.Config.Compilers;
			return compilers[language];
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0000E738 File Offset: 0x0000C938
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static string GetLanguageFromExtension(string extension)
		{
			if (extension == null)
			{
				throw new ArgumentNullException("extension");
			}
			if (CodeDomProvider.Config != null)
			{
				return CodeDomProvider.Config.Compilers.GetLanguageFromExtension(extension);
			}
			return null;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0000E768 File Offset: 0x0000C968
		public virtual string GetTypeOutput(CodeTypeReference type)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			return codeGenerator.GetTypeOutput(type);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0000E790 File Offset: 0x0000C990
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static bool IsDefinedExtension(string extension)
		{
			if (extension == null)
			{
				throw new ArgumentNullException("extension");
			}
			return CodeDomProvider.Config != null && CodeDomProvider.Config.Compilers.GetCompilerInfoForExtension(extension) != null;
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000E7D0 File Offset: 0x0000C9D0
		[ComVisible(false)]
		[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public static bool IsDefinedLanguage(string language)
		{
			if (language == null)
			{
				throw new ArgumentNullException("language");
			}
			return CodeDomProvider.Config != null && CodeDomProvider.Config.Compilers.GetCompilerInfoForLanguage(language) != null;
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000E810 File Offset: 0x0000CA10
		public virtual bool IsValidIdentifier(string value)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			return codeGenerator.IsValidIdentifier(value);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000E838 File Offset: 0x0000CA38
		public virtual CodeCompileUnit Parse(TextReader codeStream)
		{
			ICodeParser codeParser = this.CreateParser();
			if (codeParser == null)
			{
				throw this.GetNotImplemented();
			}
			return codeParser.Parse(codeStream);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000E860 File Offset: 0x0000CA60
		public virtual bool Supports(GeneratorSupport supports)
		{
			ICodeGenerator codeGenerator = this.CreateGenerator();
			if (codeGenerator == null)
			{
				throw this.GetNotImplemented();
			}
			return codeGenerator.Supports(supports);
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000447 RID: 1095 RVA: 0x0000E888 File Offset: 0x0000CA88
		private static CodeDomConfigurationHandler Config
		{
			get
			{
				return ConfigurationManager.GetSection("system.codedom") as CodeDomConfigurationHandler;
			}
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000E89C File Offset: 0x0000CA9C
		private Exception GetNotImplemented()
		{
			return new NotImplementedException();
		}
	}
}
