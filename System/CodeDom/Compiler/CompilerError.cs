﻿using System;
using System.Globalization;

namespace System.CodeDom.Compiler
{
	// Token: 0x02000080 RID: 128
	[Serializable]
	public class CompilerError
	{
		// Token: 0x0600053C RID: 1340 RVA: 0x00010FF0 File Offset: 0x0000F1F0
		public CompilerError() : this(string.Empty, 0, 0, string.Empty, string.Empty)
		{
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x0001100C File Offset: 0x0000F20C
		public CompilerError(string fileName, int line, int column, string errorNumber, string errorText)
		{
			this.fileName = fileName;
			this.line = line;
			this.column = column;
			this.errorNumber = errorNumber;
			this.errorText = errorText;
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x0001103C File Offset: 0x0000F23C
		public override string ToString()
		{
			string text = (!this.isWarning) ? "error" : "warning";
			return string.Format(CultureInfo.InvariantCulture, "{0}({1},{2}) : {3} {4}: {5}", new object[]
			{
				this.fileName,
				this.line,
				this.column,
				text,
				this.errorNumber,
				this.errorText
			});
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600053F RID: 1343 RVA: 0x000110B4 File Offset: 0x0000F2B4
		// (set) Token: 0x06000540 RID: 1344 RVA: 0x000110BC File Offset: 0x0000F2BC
		public int Line
		{
			get
			{
				return this.line;
			}
			set
			{
				this.line = value;
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000541 RID: 1345 RVA: 0x000110C8 File Offset: 0x0000F2C8
		// (set) Token: 0x06000542 RID: 1346 RVA: 0x000110D0 File Offset: 0x0000F2D0
		public int Column
		{
			get
			{
				return this.column;
			}
			set
			{
				this.column = value;
			}
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x000110DC File Offset: 0x0000F2DC
		// (set) Token: 0x06000544 RID: 1348 RVA: 0x000110E4 File Offset: 0x0000F2E4
		public string ErrorNumber
		{
			get
			{
				return this.errorNumber;
			}
			set
			{
				this.errorNumber = value;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x000110F0 File Offset: 0x0000F2F0
		// (set) Token: 0x06000546 RID: 1350 RVA: 0x000110F8 File Offset: 0x0000F2F8
		public string ErrorText
		{
			get
			{
				return this.errorText;
			}
			set
			{
				this.errorText = value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x00011104 File Offset: 0x0000F304
		// (set) Token: 0x06000548 RID: 1352 RVA: 0x0001110C File Offset: 0x0000F30C
		public bool IsWarning
		{
			get
			{
				return this.isWarning;
			}
			set
			{
				this.isWarning = value;
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x00011118 File Offset: 0x0000F318
		// (set) Token: 0x0600054A RID: 1354 RVA: 0x00011120 File Offset: 0x0000F320
		public string FileName
		{
			get
			{
				return this.fileName;
			}
			set
			{
				this.fileName = value;
			}
		}

		// Token: 0x04000137 RID: 311
		private string fileName;

		// Token: 0x04000138 RID: 312
		private int line;

		// Token: 0x04000139 RID: 313
		private int column;

		// Token: 0x0400013A RID: 314
		private string errorNumber;

		// Token: 0x0400013B RID: 315
		private string errorText;

		// Token: 0x0400013C RID: 316
		private bool isWarning;
	}
}
