﻿using System;

namespace System.CodeDom.Compiler
{
	// Token: 0x02000089 RID: 137
	[Flags]
	[Serializable]
	public enum GeneratorSupport
	{
		// Token: 0x04000164 RID: 356
		ArraysOfArrays = 1,
		// Token: 0x04000165 RID: 357
		EntryPointMethod = 2,
		// Token: 0x04000166 RID: 358
		GotoStatements = 4,
		// Token: 0x04000167 RID: 359
		MultidimensionalArrays = 8,
		// Token: 0x04000168 RID: 360
		StaticConstructors = 16,
		// Token: 0x04000169 RID: 361
		TryCatchStatements = 32,
		// Token: 0x0400016A RID: 362
		ReturnTypeAttributes = 64,
		// Token: 0x0400016B RID: 363
		DeclareValueTypes = 128,
		// Token: 0x0400016C RID: 364
		DeclareEnums = 256,
		// Token: 0x0400016D RID: 365
		DeclareDelegates = 512,
		// Token: 0x0400016E RID: 366
		DeclareInterfaces = 1024,
		// Token: 0x0400016F RID: 367
		DeclareEvents = 2048,
		// Token: 0x04000170 RID: 368
		AssemblyAttributes = 4096,
		// Token: 0x04000171 RID: 369
		ParameterAttributes = 8192,
		// Token: 0x04000172 RID: 370
		ReferenceParameters = 16384,
		// Token: 0x04000173 RID: 371
		ChainedConstructorArguments = 32768,
		// Token: 0x04000174 RID: 372
		NestedTypes = 65536,
		// Token: 0x04000175 RID: 373
		MultipleInterfaceMembers = 131072,
		// Token: 0x04000176 RID: 374
		PublicStaticMembers = 262144,
		// Token: 0x04000177 RID: 375
		ComplexExpressions = 524288,
		// Token: 0x04000178 RID: 376
		Win32Resources = 1048576,
		// Token: 0x04000179 RID: 377
		Resources = 2097152,
		// Token: 0x0400017A RID: 378
		PartialTypes = 4194304,
		// Token: 0x0400017B RID: 379
		GenericTypeReference = 8388608,
		// Token: 0x0400017C RID: 380
		GenericTypeDeclaration = 16777216,
		// Token: 0x0400017D RID: 381
		DeclareIndexerProperties = 33554432
	}
}
