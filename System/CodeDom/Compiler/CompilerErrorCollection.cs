﻿using System;
using System.Collections;

namespace System.CodeDom.Compiler
{
	// Token: 0x0200007F RID: 127
	[Serializable]
	public class CompilerErrorCollection : CollectionBase
	{
		// Token: 0x0600052D RID: 1325 RVA: 0x00010E08 File Offset: 0x0000F008
		public CompilerErrorCollection()
		{
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x00010E10 File Offset: 0x0000F010
		public CompilerErrorCollection(CompilerErrorCollection value)
		{
			base.InnerList.AddRange(value.InnerList);
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x00010E34 File Offset: 0x0000F034
		public CompilerErrorCollection(CompilerError[] value)
		{
			base.InnerList.AddRange(value);
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x00010E48 File Offset: 0x0000F048
		public int Add(CompilerError value)
		{
			return base.InnerList.Add(value);
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x00010E58 File Offset: 0x0000F058
		public void AddRange(CompilerError[] value)
		{
			base.InnerList.AddRange(value);
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x00010E68 File Offset: 0x0000F068
		public void AddRange(CompilerErrorCollection value)
		{
			base.InnerList.AddRange(value.InnerList);
		}

		// Token: 0x06000533 RID: 1331 RVA: 0x00010E7C File Offset: 0x0000F07C
		public bool Contains(CompilerError value)
		{
			return base.InnerList.Contains(value);
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x00010E8C File Offset: 0x0000F08C
		public void CopyTo(CompilerError[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x00010E9C File Offset: 0x0000F09C
		public int IndexOf(CompilerError value)
		{
			return base.InnerList.IndexOf(value);
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x00010EAC File Offset: 0x0000F0AC
		public void Insert(int index, CompilerError value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x00010EBC File Offset: 0x0000F0BC
		public void Remove(CompilerError value)
		{
			base.InnerList.Remove(value);
		}

		// Token: 0x170000EF RID: 239
		public CompilerError this[int index]
		{
			get
			{
				return (CompilerError)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x0600053A RID: 1338 RVA: 0x00010EF0 File Offset: 0x0000F0F0
		public bool HasErrors
		{
			get
			{
				foreach (object obj in base.InnerList)
				{
					CompilerError compilerError = (CompilerError)obj;
					if (!compilerError.IsWarning)
					{
						return true;
					}
				}
				return false;
			}
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x0600053B RID: 1339 RVA: 0x00010F70 File Offset: 0x0000F170
		public bool HasWarnings
		{
			get
			{
				foreach (object obj in base.InnerList)
				{
					CompilerError compilerError = (CompilerError)obj;
					if (compilerError.IsWarning)
					{
						return true;
					}
				}
				return false;
			}
		}
	}
}
