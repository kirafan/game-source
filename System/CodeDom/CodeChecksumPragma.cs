﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x02000030 RID: 48
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Serializable]
	public class CodeChecksumPragma : CodeDirective
	{
		// Token: 0x060001AC RID: 428 RVA: 0x0000AD4C File Offset: 0x00008F4C
		public CodeChecksumPragma()
		{
		}

		// Token: 0x060001AD RID: 429 RVA: 0x0000AD54 File Offset: 0x00008F54
		public CodeChecksumPragma(string fileName, Guid checksumAlgorithmId, byte[] checksumData)
		{
			this.fileName = fileName;
			this.checksumAlgorithmId = checksumAlgorithmId;
			this.checksumData = checksumData;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060001AE RID: 430 RVA: 0x0000AD74 File Offset: 0x00008F74
		// (set) Token: 0x060001AF RID: 431 RVA: 0x0000AD7C File Offset: 0x00008F7C
		public Guid ChecksumAlgorithmId
		{
			get
			{
				return this.checksumAlgorithmId;
			}
			set
			{
				this.checksumAlgorithmId = value;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060001B0 RID: 432 RVA: 0x0000AD88 File Offset: 0x00008F88
		// (set) Token: 0x060001B1 RID: 433 RVA: 0x0000AD90 File Offset: 0x00008F90
		public byte[] ChecksumData
		{
			get
			{
				return this.checksumData;
			}
			set
			{
				this.checksumData = value;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060001B2 RID: 434 RVA: 0x0000AD9C File Offset: 0x00008F9C
		// (set) Token: 0x060001B3 RID: 435 RVA: 0x0000ADB8 File Offset: 0x00008FB8
		public string FileName
		{
			get
			{
				if (this.fileName == null)
				{
					return string.Empty;
				}
				return this.fileName;
			}
			set
			{
				this.fileName = value;
			}
		}

		// Token: 0x04000089 RID: 137
		private string fileName;

		// Token: 0x0400008A RID: 138
		private Guid checksumAlgorithmId;

		// Token: 0x0400008B RID: 139
		private byte[] checksumData;
	}
}
