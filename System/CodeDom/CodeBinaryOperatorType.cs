﻿using System;
using System.Runtime.InteropServices;

namespace System.CodeDom
{
	// Token: 0x0200002C RID: 44
	[ComVisible(true)]
	[Serializable]
	public enum CodeBinaryOperatorType
	{
		// Token: 0x04000073 RID: 115
		Add,
		// Token: 0x04000074 RID: 116
		Subtract,
		// Token: 0x04000075 RID: 117
		Multiply,
		// Token: 0x04000076 RID: 118
		Divide,
		// Token: 0x04000077 RID: 119
		Modulus,
		// Token: 0x04000078 RID: 120
		Assign,
		// Token: 0x04000079 RID: 121
		IdentityInequality,
		// Token: 0x0400007A RID: 122
		IdentityEquality,
		// Token: 0x0400007B RID: 123
		ValueEquality,
		// Token: 0x0400007C RID: 124
		BitwiseOr,
		// Token: 0x0400007D RID: 125
		BitwiseAnd,
		// Token: 0x0400007E RID: 126
		BooleanOr,
		// Token: 0x0400007F RID: 127
		BooleanAnd,
		// Token: 0x04000080 RID: 128
		LessThan,
		// Token: 0x04000081 RID: 129
		LessThanOrEqual,
		// Token: 0x04000082 RID: 130
		GreaterThan,
		// Token: 0x04000083 RID: 131
		GreaterThanOrEqual
	}
}
