﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000257 RID: 599
	public enum ThreadWaitReason
	{
		// Token: 0x04000670 RID: 1648
		EventPairHigh = 7,
		// Token: 0x04000671 RID: 1649
		EventPairLow,
		// Token: 0x04000672 RID: 1650
		ExecutionDelay = 4,
		// Token: 0x04000673 RID: 1651
		Executive = 0,
		// Token: 0x04000674 RID: 1652
		FreePage,
		// Token: 0x04000675 RID: 1653
		LpcReceive = 9,
		// Token: 0x04000676 RID: 1654
		LpcReply,
		// Token: 0x04000677 RID: 1655
		PageIn = 2,
		// Token: 0x04000678 RID: 1656
		PageOut = 12,
		// Token: 0x04000679 RID: 1657
		Suspended = 5,
		// Token: 0x0400067A RID: 1658
		SystemAllocation = 3,
		// Token: 0x0400067B RID: 1659
		Unknown = 13,
		// Token: 0x0400067C RID: 1660
		UserRequest = 6,
		// Token: 0x0400067D RID: 1661
		VirtualMemory = 11
	}
}
