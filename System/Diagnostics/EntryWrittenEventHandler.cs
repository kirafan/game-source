﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200050D RID: 1293
	// (Invoke) Token: 0x06002CE0 RID: 11488
	public delegate void EntryWrittenEventHandler(object sender, EntryWrittenEventArgs e);
}
