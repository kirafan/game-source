﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200024D RID: 589
	public class SourceFilter : TraceFilter
	{
		// Token: 0x060014C3 RID: 5315 RVA: 0x0003707C File Offset: 0x0003527C
		public SourceFilter(string source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			this.source = source;
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x060014C4 RID: 5316 RVA: 0x0003709C File Offset: 0x0003529C
		// (set) Token: 0x060014C5 RID: 5317 RVA: 0x000370A4 File Offset: 0x000352A4
		public string Source
		{
			get
			{
				return this.source;
			}
			set
			{
				if (this.source == null)
				{
					throw new ArgumentNullException("value");
				}
				this.source = value;
			}
		}

		// Token: 0x060014C6 RID: 5318 RVA: 0x000370C4 File Offset: 0x000352C4
		public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
		{
			return source == this.source;
		}

		// Token: 0x04000642 RID: 1602
		private string source;
	}
}
