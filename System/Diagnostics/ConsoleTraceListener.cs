﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200020E RID: 526
	public class ConsoleTraceListener : TextWriterTraceListener
	{
		// Token: 0x06001194 RID: 4500 RVA: 0x0002EC24 File Offset: 0x0002CE24
		public ConsoleTraceListener() : this(false)
		{
		}

		// Token: 0x06001195 RID: 4501 RVA: 0x0002EC30 File Offset: 0x0002CE30
		public ConsoleTraceListener(bool useErrorStream) : base((!useErrorStream) ? Console.Out : Console.Error)
		{
		}

		// Token: 0x06001196 RID: 4502 RVA: 0x0002EC50 File Offset: 0x0002CE50
		internal ConsoleTraceListener(string data) : this(Convert.ToBoolean(data))
		{
		}
	}
}
