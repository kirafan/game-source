﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200022F RID: 559
	public class InstanceDataCollection : DictionaryBase
	{
		// Token: 0x06001325 RID: 4901 RVA: 0x00033180 File Offset: 0x00031380
		[Obsolete("Use InstanceDataCollectionCollection indexer instead.")]
		public InstanceDataCollection(string counterName)
		{
			InstanceDataCollection.CheckNull(counterName, "counterName");
			this.counterName = counterName;
		}

		// Token: 0x06001326 RID: 4902 RVA: 0x0003319C File Offset: 0x0003139C
		private static void CheckNull(object value, string name)
		{
			if (value == null)
			{
				throw new ArgumentNullException(name);
			}
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06001327 RID: 4903 RVA: 0x000331AC File Offset: 0x000313AC
		public string CounterName
		{
			get
			{
				return this.counterName;
			}
		}

		// Token: 0x17000473 RID: 1139
		public InstanceData this[string instanceName]
		{
			get
			{
				InstanceDataCollection.CheckNull(instanceName, "instanceName");
				return (InstanceData)base.Dictionary[instanceName];
			}
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06001329 RID: 4905 RVA: 0x000331D4 File Offset: 0x000313D4
		public ICollection Keys
		{
			get
			{
				return base.Dictionary.Keys;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x0600132A RID: 4906 RVA: 0x000331E4 File Offset: 0x000313E4
		public ICollection Values
		{
			get
			{
				return base.Dictionary.Values;
			}
		}

		// Token: 0x0600132B RID: 4907 RVA: 0x000331F4 File Offset: 0x000313F4
		public bool Contains(string instanceName)
		{
			InstanceDataCollection.CheckNull(instanceName, "instanceName");
			return base.Dictionary.Contains(instanceName);
		}

		// Token: 0x0600132C RID: 4908 RVA: 0x00033210 File Offset: 0x00031410
		public void CopyTo(InstanceData[] instances, int index)
		{
			base.Dictionary.CopyTo(instances, index);
		}

		// Token: 0x04000589 RID: 1417
		private string counterName;
	}
}
