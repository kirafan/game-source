﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000230 RID: 560
	public class InstanceData
	{
		// Token: 0x0600132D RID: 4909 RVA: 0x00033220 File Offset: 0x00031420
		public InstanceData(string instanceName, CounterSample sample)
		{
			this.instanceName = instanceName;
			this.sample = sample;
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x0600132E RID: 4910 RVA: 0x00033238 File Offset: 0x00031438
		public string InstanceName
		{
			get
			{
				return this.instanceName;
			}
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x0600132F RID: 4911 RVA: 0x00033240 File Offset: 0x00031440
		public long RawValue
		{
			get
			{
				return this.sample.RawValue;
			}
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x06001330 RID: 4912 RVA: 0x00033250 File Offset: 0x00031450
		public CounterSample Sample
		{
			get
			{
				return this.sample;
			}
		}

		// Token: 0x0400058A RID: 1418
		private string instanceName;

		// Token: 0x0400058B RID: 1419
		private CounterSample sample;
	}
}
