﻿using System;
using System.Runtime.CompilerServices;

namespace System.Diagnostics
{
	// Token: 0x02000253 RID: 595
	public class Stopwatch
	{
		// Token: 0x060014E7 RID: 5351
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTimestamp();

		// Token: 0x060014E8 RID: 5352 RVA: 0x00037508 File Offset: 0x00035708
		public static Stopwatch StartNew()
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			return stopwatch;
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x060014E9 RID: 5353 RVA: 0x00037524 File Offset: 0x00035724
		public TimeSpan Elapsed
		{
			get
			{
				if (Stopwatch.IsHighResolution)
				{
					return TimeSpan.FromTicks(this.ElapsedTicks / (Stopwatch.Frequency / 10000000L));
				}
				return TimeSpan.FromTicks(this.ElapsedTicks);
			}
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x060014EA RID: 5354 RVA: 0x00037560 File Offset: 0x00035760
		public long ElapsedMilliseconds
		{
			get
			{
				if (Stopwatch.IsHighResolution)
				{
					return this.ElapsedTicks / (Stopwatch.Frequency / 1000L);
				}
				return checked((long)this.Elapsed.TotalMilliseconds);
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x060014EB RID: 5355 RVA: 0x0003759C File Offset: 0x0003579C
		public long ElapsedTicks
		{
			get
			{
				return (!this.is_running) ? this.elapsed : (Stopwatch.GetTimestamp() - this.started + this.elapsed);
			}
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x060014EC RID: 5356 RVA: 0x000375C8 File Offset: 0x000357C8
		public bool IsRunning
		{
			get
			{
				return this.is_running;
			}
		}

		// Token: 0x060014ED RID: 5357 RVA: 0x000375D0 File Offset: 0x000357D0
		public void Reset()
		{
			this.elapsed = 0L;
			this.is_running = false;
		}

		// Token: 0x060014EE RID: 5358 RVA: 0x000375E4 File Offset: 0x000357E4
		public void Start()
		{
			if (this.is_running)
			{
				return;
			}
			this.started = Stopwatch.GetTimestamp();
			this.is_running = true;
		}

		// Token: 0x060014EF RID: 5359 RVA: 0x00037604 File Offset: 0x00035804
		public void Stop()
		{
			if (!this.is_running)
			{
				return;
			}
			this.elapsed += Stopwatch.GetTimestamp() - this.started;
			this.is_running = false;
		}

		// Token: 0x04000658 RID: 1624
		public static readonly long Frequency = 10000000L;

		// Token: 0x04000659 RID: 1625
		public static readonly bool IsHighResolution = true;

		// Token: 0x0400065A RID: 1626
		private long elapsed;

		// Token: 0x0400065B RID: 1627
		private long started;

		// Token: 0x0400065C RID: 1628
		private bool is_running;
	}
}
