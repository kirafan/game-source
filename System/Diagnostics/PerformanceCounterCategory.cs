﻿using System;
using System.Runtime.CompilerServices;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x02000235 RID: 565
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public sealed class PerformanceCounterCategory
	{
		// Token: 0x0600136F RID: 4975 RVA: 0x00033ED4 File Offset: 0x000320D4
		public PerformanceCounterCategory() : this(string.Empty, ".")
		{
		}

		// Token: 0x06001370 RID: 4976 RVA: 0x00033EE8 File Offset: 0x000320E8
		public PerformanceCounterCategory(string categoryName) : this(categoryName, ".")
		{
		}

		// Token: 0x06001371 RID: 4977 RVA: 0x00033EF8 File Offset: 0x000320F8
		public PerformanceCounterCategory(string categoryName, string machineName)
		{
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			this.categoryName = categoryName;
			this.machineName = machineName;
		}

		// Token: 0x06001372 RID: 4978
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CategoryDelete(string name);

		// Token: 0x06001373 RID: 4979
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string CategoryHelpInternal(string category, string machine);

		// Token: 0x06001374 RID: 4980
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CounterCategoryExists(string counter, string category, string machine);

		// Token: 0x06001375 RID: 4981
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Create(string categoryName, string categoryHelp, PerformanceCounterCategoryType categoryType, CounterCreationData[] items);

		// Token: 0x06001376 RID: 4982
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int InstanceExistsInternal(string instance, string category, string machine);

		// Token: 0x06001377 RID: 4983
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetCategoryNames(string machine);

		// Token: 0x06001378 RID: 4984
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetCounterNames(string category, string machine);

		// Token: 0x06001379 RID: 4985
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetInstanceNames(string category, string machine);

		// Token: 0x0600137A RID: 4986 RVA: 0x00033F38 File Offset: 0x00032138
		private static void CheckCategory(string categoryName)
		{
			if (categoryName == null)
			{
				throw new ArgumentNullException("categoryName");
			}
			if (categoryName == string.Empty)
			{
				throw new ArgumentException("categoryName");
			}
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x0600137B RID: 4987 RVA: 0x00033F74 File Offset: 0x00032174
		public string CategoryHelp
		{
			get
			{
				string text = PerformanceCounterCategory.CategoryHelpInternal(this.categoryName, this.machineName);
				if (text != null)
				{
					return text;
				}
				throw new InvalidOperationException();
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x0600137C RID: 4988 RVA: 0x00033FA0 File Offset: 0x000321A0
		// (set) Token: 0x0600137D RID: 4989 RVA: 0x00033FA8 File Offset: 0x000321A8
		public string CategoryName
		{
			get
			{
				return this.categoryName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value == string.Empty)
				{
					throw new ArgumentException("value");
				}
				this.categoryName = value;
			}
		}

		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x0600137E RID: 4990 RVA: 0x00033FE0 File Offset: 0x000321E0
		// (set) Token: 0x0600137F RID: 4991 RVA: 0x00033FE8 File Offset: 0x000321E8
		public string MachineName
		{
			get
			{
				return this.machineName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value == string.Empty)
				{
					throw new ArgumentException("value");
				}
				this.machineName = value;
			}
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06001380 RID: 4992 RVA: 0x00034020 File Offset: 0x00032220
		public PerformanceCounterCategoryType CategoryType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x00034028 File Offset: 0x00032228
		public bool CounterExists(string counterName)
		{
			return PerformanceCounterCategory.CounterExists(counterName, this.categoryName, this.machineName);
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x0003403C File Offset: 0x0003223C
		public static bool CounterExists(string counterName, string categoryName)
		{
			return PerformanceCounterCategory.CounterExists(counterName, categoryName, ".");
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x0003404C File Offset: 0x0003224C
		public static bool CounterExists(string counterName, string categoryName, string machineName)
		{
			if (counterName == null)
			{
				throw new ArgumentNullException("counterName");
			}
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			return PerformanceCounterCategory.CounterCategoryExists(counterName, categoryName, machineName);
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x0003408C File Offset: 0x0003228C
		[Obsolete("Use another overload that uses PerformanceCounterCategoryType instead")]
		public static PerformanceCounterCategory Create(string categoryName, string categoryHelp, CounterCreationDataCollection counterData)
		{
			return PerformanceCounterCategory.Create(categoryName, categoryHelp, PerformanceCounterCategoryType.Unknown, counterData);
		}

		// Token: 0x06001385 RID: 4997 RVA: 0x00034098 File Offset: 0x00032298
		[Obsolete("Use another overload that uses PerformanceCounterCategoryType instead")]
		public static PerformanceCounterCategory Create(string categoryName, string categoryHelp, string counterName, string counterHelp)
		{
			return PerformanceCounterCategory.Create(categoryName, categoryHelp, PerformanceCounterCategoryType.Unknown, counterName, counterHelp);
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x000340A4 File Offset: 0x000322A4
		public static PerformanceCounterCategory Create(string categoryName, string categoryHelp, PerformanceCounterCategoryType categoryType, CounterCreationDataCollection counterData)
		{
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (counterData == null)
			{
				throw new ArgumentNullException("counterData");
			}
			if (counterData.Count == 0)
			{
				throw new ArgumentException("counterData");
			}
			CounterCreationData[] array = new CounterCreationData[counterData.Count];
			counterData.CopyTo(array, 0);
			if (!PerformanceCounterCategory.Create(categoryName, categoryHelp, categoryType, array))
			{
				throw new InvalidOperationException();
			}
			return new PerformanceCounterCategory(categoryName, categoryHelp);
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x00034110 File Offset: 0x00032310
		public static PerformanceCounterCategory Create(string categoryName, string categoryHelp, PerformanceCounterCategoryType categoryType, string counterName, string counterHelp)
		{
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (!PerformanceCounterCategory.Create(categoryName, categoryHelp, categoryType, new CounterCreationData[]
			{
				new CounterCreationData(counterName, counterHelp, PerformanceCounterType.NumberOfItems32)
			}))
			{
				throw new InvalidOperationException();
			}
			return new PerformanceCounterCategory(categoryName, categoryHelp);
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x00034158 File Offset: 0x00032358
		public static void Delete(string categoryName)
		{
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (!PerformanceCounterCategory.CategoryDelete(categoryName))
			{
				throw new InvalidOperationException();
			}
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x00034174 File Offset: 0x00032374
		public static bool Exists(string categoryName)
		{
			return PerformanceCounterCategory.Exists(categoryName, ".");
		}

		// Token: 0x0600138A RID: 5002 RVA: 0x00034184 File Offset: 0x00032384
		public static bool Exists(string categoryName, string machineName)
		{
			PerformanceCounterCategory.CheckCategory(categoryName);
			return PerformanceCounterCategory.CounterCategoryExists(null, categoryName, machineName);
		}

		// Token: 0x0600138B RID: 5003 RVA: 0x00034194 File Offset: 0x00032394
		public static PerformanceCounterCategory[] GetCategories()
		{
			return PerformanceCounterCategory.GetCategories(".");
		}

		// Token: 0x0600138C RID: 5004 RVA: 0x000341A0 File Offset: 0x000323A0
		public static PerformanceCounterCategory[] GetCategories(string machineName)
		{
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			string[] categoryNames = PerformanceCounterCategory.GetCategoryNames(machineName);
			PerformanceCounterCategory[] array = new PerformanceCounterCategory[categoryNames.Length];
			for (int i = 0; i < categoryNames.Length; i++)
			{
				array[i] = new PerformanceCounterCategory(categoryNames[i], machineName);
			}
			return array;
		}

		// Token: 0x0600138D RID: 5005 RVA: 0x000341F0 File Offset: 0x000323F0
		public PerformanceCounter[] GetCounters()
		{
			return this.GetCounters(string.Empty);
		}

		// Token: 0x0600138E RID: 5006 RVA: 0x00034200 File Offset: 0x00032400
		public PerformanceCounter[] GetCounters(string instanceName)
		{
			string[] counterNames = PerformanceCounterCategory.GetCounterNames(this.categoryName, this.machineName);
			PerformanceCounter[] array = new PerformanceCounter[counterNames.Length];
			for (int i = 0; i < counterNames.Length; i++)
			{
				array[i] = new PerformanceCounter(this.categoryName, counterNames[i], instanceName, this.machineName);
			}
			return array;
		}

		// Token: 0x0600138F RID: 5007 RVA: 0x00034258 File Offset: 0x00032458
		public string[] GetInstanceNames()
		{
			return PerformanceCounterCategory.GetInstanceNames(this.categoryName, this.machineName);
		}

		// Token: 0x06001390 RID: 5008 RVA: 0x0003426C File Offset: 0x0003246C
		public bool InstanceExists(string instanceName)
		{
			return PerformanceCounterCategory.InstanceExists(instanceName, this.categoryName, this.machineName);
		}

		// Token: 0x06001391 RID: 5009 RVA: 0x00034280 File Offset: 0x00032480
		public static bool InstanceExists(string instanceName, string categoryName)
		{
			return PerformanceCounterCategory.InstanceExists(instanceName, categoryName, ".");
		}

		// Token: 0x06001392 RID: 5010 RVA: 0x00034290 File Offset: 0x00032490
		public static bool InstanceExists(string instanceName, string categoryName, string machineName)
		{
			if (instanceName == null)
			{
				throw new ArgumentNullException("instanceName");
			}
			PerformanceCounterCategory.CheckCategory(categoryName);
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			int num = PerformanceCounterCategory.InstanceExistsInternal(instanceName, categoryName, machineName);
			if (num == 0)
			{
				return false;
			}
			if (num == 1)
			{
				return true;
			}
			throw new InvalidOperationException();
		}

		// Token: 0x06001393 RID: 5011 RVA: 0x000342E4 File Offset: 0x000324E4
		[MonoTODO]
		public InstanceDataCollectionCollection ReadCategory()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000595 RID: 1429
		private string categoryName;

		// Token: 0x04000596 RID: 1430
		private string machineName;

		// Token: 0x04000597 RID: 1431
		private PerformanceCounterCategoryType type = PerformanceCounterCategoryType.Unknown;
	}
}
