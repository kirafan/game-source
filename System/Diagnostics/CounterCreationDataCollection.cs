﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x02000210 RID: 528
	[Serializable]
	public class CounterCreationDataCollection : CollectionBase
	{
		// Token: 0x0600119E RID: 4510 RVA: 0x0002ECC4 File Offset: 0x0002CEC4
		public CounterCreationDataCollection()
		{
		}

		// Token: 0x0600119F RID: 4511 RVA: 0x0002ECCC File Offset: 0x0002CECC
		public CounterCreationDataCollection(CounterCreationData[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x060011A0 RID: 4512 RVA: 0x0002ECDC File Offset: 0x0002CEDC
		public CounterCreationDataCollection(CounterCreationDataCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000404 RID: 1028
		public CounterCreationData this[int index]
		{
			get
			{
				return (CounterCreationData)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x060011A3 RID: 4515 RVA: 0x0002ED10 File Offset: 0x0002CF10
		public int Add(CounterCreationData value)
		{
			return base.InnerList.Add(value);
		}

		// Token: 0x060011A4 RID: 4516 RVA: 0x0002ED20 File Offset: 0x0002CF20
		public void AddRange(CounterCreationData[] value)
		{
			foreach (CounterCreationData value2 in value)
			{
				this.Add(value2);
			}
		}

		// Token: 0x060011A5 RID: 4517 RVA: 0x0002ED50 File Offset: 0x0002CF50
		public void AddRange(CounterCreationDataCollection value)
		{
			foreach (object obj in value)
			{
				CounterCreationData value2 = (CounterCreationData)obj;
				this.Add(value2);
			}
		}

		// Token: 0x060011A6 RID: 4518 RVA: 0x0002EDBC File Offset: 0x0002CFBC
		public bool Contains(CounterCreationData value)
		{
			return base.InnerList.Contains(value);
		}

		// Token: 0x060011A7 RID: 4519 RVA: 0x0002EDCC File Offset: 0x0002CFCC
		public void CopyTo(CounterCreationData[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x060011A8 RID: 4520 RVA: 0x0002EDDC File Offset: 0x0002CFDC
		public int IndexOf(CounterCreationData value)
		{
			return base.InnerList.IndexOf(value);
		}

		// Token: 0x060011A9 RID: 4521 RVA: 0x0002EDEC File Offset: 0x0002CFEC
		public void Insert(int index, CounterCreationData value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x060011AA RID: 4522 RVA: 0x0002EDFC File Offset: 0x0002CFFC
		protected override void OnValidate(object value)
		{
			if (!(value is CounterCreationData))
			{
				throw new NotSupportedException(Locale.GetText("You can only insert CounterCreationData objects into the collection"));
			}
		}

		// Token: 0x060011AB RID: 4523 RVA: 0x0002EE1C File Offset: 0x0002D01C
		public virtual void Remove(CounterCreationData value)
		{
			base.InnerList.Remove(value);
		}
	}
}
