﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x02000229 RID: 553
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public sealed class EventLogTraceListener : TraceListener
	{
		// Token: 0x060012D9 RID: 4825 RVA: 0x000328C4 File Offset: 0x00030AC4
		public EventLogTraceListener()
		{
		}

		// Token: 0x060012DA RID: 4826 RVA: 0x000328CC File Offset: 0x00030ACC
		public EventLogTraceListener(EventLog eventLog)
		{
			if (eventLog == null)
			{
				throw new ArgumentNullException("eventLog");
			}
			this.event_log = eventLog;
		}

		// Token: 0x060012DB RID: 4827 RVA: 0x000328EC File Offset: 0x00030AEC
		public EventLogTraceListener(string source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			this.event_log = new EventLog();
			this.event_log.Source = source;
		}

		// Token: 0x1700044A RID: 1098
		// (get) Token: 0x060012DC RID: 4828 RVA: 0x00032928 File Offset: 0x00030B28
		// (set) Token: 0x060012DD RID: 4829 RVA: 0x00032930 File Offset: 0x00030B30
		public EventLog EventLog
		{
			get
			{
				return this.event_log;
			}
			set
			{
				this.event_log = value;
			}
		}

		// Token: 0x1700044B RID: 1099
		// (get) Token: 0x060012DE RID: 4830 RVA: 0x0003293C File Offset: 0x00030B3C
		// (set) Token: 0x060012DF RID: 4831 RVA: 0x00032960 File Offset: 0x00030B60
		public override string Name
		{
			get
			{
				return (this.name == null) ? this.event_log.Source : this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x060012E0 RID: 4832 RVA: 0x0003296C File Offset: 0x00030B6C
		public override void Close()
		{
			this.event_log.Close();
		}

		// Token: 0x060012E1 RID: 4833 RVA: 0x0003297C File Offset: 0x00030B7C
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.event_log.Dispose();
			}
		}

		// Token: 0x060012E2 RID: 4834 RVA: 0x00032990 File Offset: 0x00030B90
		public override void Write(string message)
		{
			this.TraceData(new TraceEventCache(), this.event_log.Source, TraceEventType.Information, 0, message);
		}

		// Token: 0x060012E3 RID: 4835 RVA: 0x000329B8 File Offset: 0x00030BB8
		public override void WriteLine(string message)
		{
			this.Write(message);
		}

		// Token: 0x060012E4 RID: 4836 RVA: 0x000329C4 File Offset: 0x00030BC4
		[ComVisible(false)]
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
		{
			EventLogEntryType type;
			switch (eventType)
			{
			case TraceEventType.Critical:
			case TraceEventType.Error:
				type = EventLogEntryType.Error;
				goto IL_34;
			case TraceEventType.Warning:
				type = EventLogEntryType.Warning;
				goto IL_34;
			}
			type = EventLogEntryType.Information;
			IL_34:
			this.event_log.WriteEntry((data == null) ? string.Empty : data.ToString(), type, id, 0);
		}

		// Token: 0x060012E5 RID: 4837 RVA: 0x00032A2C File Offset: 0x00030C2C
		[ComVisible(false)]
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
		{
			string data2 = string.Empty;
			if (data != null)
			{
				string[] array = new string[data.Length];
				for (int i = 0; i < data.Length; i++)
				{
					array[i] = ((data[i] == null) ? string.Empty : data[i].ToString());
				}
				data2 = string.Join(", ", array);
			}
			this.TraceData(eventCache, source, eventType, id, data2);
		}

		// Token: 0x060012E6 RID: 4838 RVA: 0x00032A9C File Offset: 0x00030C9C
		[ComVisible(false)]
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
		{
			this.TraceData(eventCache, source, eventType, id, message);
		}

		// Token: 0x060012E7 RID: 4839 RVA: 0x00032AAC File Offset: 0x00030CAC
		[ComVisible(false)]
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
		{
			this.TraceEvent(eventCache, source, eventType, id, (format == null) ? null : string.Format(format, args));
		}

		// Token: 0x04000564 RID: 1380
		private EventLog event_log;

		// Token: 0x04000565 RID: 1381
		private string name;
	}
}
