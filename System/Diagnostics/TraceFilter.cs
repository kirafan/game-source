﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200025B RID: 603
	public abstract class TraceFilter
	{
		// Token: 0x06001532 RID: 5426
		public abstract bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data);
	}
}
