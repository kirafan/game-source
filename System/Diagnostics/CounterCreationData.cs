﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x02000211 RID: 529
	[System.ComponentModel.TypeConverter("System.Diagnostics.Design.CounterCreationDataConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[Serializable]
	public class CounterCreationData
	{
		// Token: 0x060011AC RID: 4524 RVA: 0x0002EE2C File Offset: 0x0002D02C
		public CounterCreationData()
		{
		}

		// Token: 0x060011AD RID: 4525 RVA: 0x0002EE40 File Offset: 0x0002D040
		public CounterCreationData(string counterName, string counterHelp, PerformanceCounterType counterType)
		{
			this.CounterName = counterName;
			this.CounterHelp = counterHelp;
			this.CounterType = counterType;
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x060011AE RID: 4526 RVA: 0x0002EE74 File Offset: 0x0002D074
		// (set) Token: 0x060011AF RID: 4527 RVA: 0x0002EE7C File Offset: 0x0002D07C
		[MonitoringDescription("Description of this counter.")]
		[System.ComponentModel.DefaultValue("")]
		public string CounterHelp
		{
			get
			{
				return this.help;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.help = value;
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x060011B0 RID: 4528 RVA: 0x0002EE98 File Offset: 0x0002D098
		// (set) Token: 0x060011B1 RID: 4529 RVA: 0x0002EEA0 File Offset: 0x0002D0A0
		[MonitoringDescription("Name of this counter.")]
		[System.ComponentModel.DefaultValue("")]
		[System.ComponentModel.TypeConverter("System.Diagnostics.Design.StringValueConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		public string CounterName
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value == string.Empty)
				{
					throw new ArgumentException("value");
				}
				this.name = value;
			}
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x060011B2 RID: 4530 RVA: 0x0002EED8 File Offset: 0x0002D0D8
		// (set) Token: 0x060011B3 RID: 4531 RVA: 0x0002EEE0 File Offset: 0x0002D0E0
		[MonitoringDescription("Type of this counter.")]
		[System.ComponentModel.DefaultValue(typeof(PerformanceCounterType), "NumberOfItems32")]
		public PerformanceCounterType CounterType
		{
			get
			{
				return this.type;
			}
			set
			{
				if (!Enum.IsDefined(typeof(PerformanceCounterType), value))
				{
					throw new System.ComponentModel.InvalidEnumArgumentException();
				}
				this.type = value;
			}
		}

		// Token: 0x0400050B RID: 1291
		private string help = string.Empty;

		// Token: 0x0400050C RID: 1292
		private string name;

		// Token: 0x0400050D RID: 1293
		private PerformanceCounterType type;
	}
}
