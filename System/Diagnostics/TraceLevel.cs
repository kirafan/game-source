﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200025E RID: 606
	public enum TraceLevel
	{
		// Token: 0x0400069C RID: 1692
		Off,
		// Token: 0x0400069D RID: 1693
		Error,
		// Token: 0x0400069E RID: 1694
		Warning,
		// Token: 0x0400069F RID: 1695
		Info,
		// Token: 0x040006A0 RID: 1696
		Verbose
	}
}
