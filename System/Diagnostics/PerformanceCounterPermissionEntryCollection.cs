﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x0200023E RID: 574
	[Serializable]
	public class PerformanceCounterPermissionEntryCollection : CollectionBase
	{
		// Token: 0x060013D1 RID: 5073 RVA: 0x0003497C File Offset: 0x00032B7C
		internal PerformanceCounterPermissionEntryCollection(PerformanceCounterPermission owner)
		{
			this.owner = owner;
			System.Security.Permissions.ResourcePermissionBaseEntry[] entries = owner.GetEntries();
			if (entries.Length > 0)
			{
				foreach (System.Security.Permissions.ResourcePermissionBaseEntry resourcePermissionBaseEntry in entries)
				{
					PerformanceCounterPermissionAccess permissionAccess = (PerformanceCounterPermissionAccess)resourcePermissionBaseEntry.PermissionAccess;
					string machineName = resourcePermissionBaseEntry.PermissionAccessPath[0];
					string categoryName = resourcePermissionBaseEntry.PermissionAccessPath[1];
					PerformanceCounterPermissionEntry value = new PerformanceCounterPermissionEntry(permissionAccess, machineName, categoryName);
					base.InnerList.Add(value);
				}
			}
		}

		// Token: 0x060013D2 RID: 5074 RVA: 0x000349F8 File Offset: 0x00032BF8
		internal PerformanceCounterPermissionEntryCollection(System.Security.Permissions.ResourcePermissionBaseEntry[] entries)
		{
			foreach (System.Security.Permissions.ResourcePermissionBaseEntry resourcePermissionBaseEntry in entries)
			{
				base.List.Add(new PerformanceCounterPermissionEntry((PerformanceCounterPermissionAccess)resourcePermissionBaseEntry.PermissionAccess, resourcePermissionBaseEntry.PermissionAccessPath[0], resourcePermissionBaseEntry.PermissionAccessPath[1]));
			}
		}

		// Token: 0x17000493 RID: 1171
		public PerformanceCounterPermissionEntry this[int index]
		{
			get
			{
				return (PerformanceCounterPermissionEntry)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x00034A70 File Offset: 0x00032C70
		public int Add(PerformanceCounterPermissionEntry value)
		{
			return base.List.Add(value);
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x00034A80 File Offset: 0x00032C80
		public void AddRange(PerformanceCounterPermissionEntry[] value)
		{
			foreach (PerformanceCounterPermissionEntry value2 in value)
			{
				base.List.Add(value2);
			}
		}

		// Token: 0x060013D7 RID: 5079 RVA: 0x00034AB4 File Offset: 0x00032CB4
		public void AddRange(PerformanceCounterPermissionEntryCollection value)
		{
			foreach (object obj in value)
			{
				PerformanceCounterPermissionEntry value2 = (PerformanceCounterPermissionEntry)obj;
				base.List.Add(value2);
			}
		}

		// Token: 0x060013D8 RID: 5080 RVA: 0x00034B24 File Offset: 0x00032D24
		public bool Contains(PerformanceCounterPermissionEntry value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x060013D9 RID: 5081 RVA: 0x00034B34 File Offset: 0x00032D34
		public void CopyTo(PerformanceCounterPermissionEntry[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060013DA RID: 5082 RVA: 0x00034B44 File Offset: 0x00032D44
		public int IndexOf(PerformanceCounterPermissionEntry value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x00034B54 File Offset: 0x00032D54
		public void Insert(int index, PerformanceCounterPermissionEntry value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060013DC RID: 5084 RVA: 0x00034B64 File Offset: 0x00032D64
		protected override void OnClear()
		{
			this.owner.ClearEntries();
		}

		// Token: 0x060013DD RID: 5085 RVA: 0x00034B74 File Offset: 0x00032D74
		protected override void OnInsert(int index, object value)
		{
			this.owner.Add(value);
		}

		// Token: 0x060013DE RID: 5086 RVA: 0x00034B84 File Offset: 0x00032D84
		protected override void OnRemove(int index, object value)
		{
			this.owner.Remove(value);
		}

		// Token: 0x060013DF RID: 5087 RVA: 0x00034B94 File Offset: 0x00032D94
		protected override void OnSet(int index, object oldValue, object newValue)
		{
			this.owner.Remove(oldValue);
			this.owner.Add(newValue);
		}

		// Token: 0x060013E0 RID: 5088 RVA: 0x00034BB0 File Offset: 0x00032DB0
		public void Remove(PerformanceCounterPermissionEntry value)
		{
			base.List.Remove(value);
		}

		// Token: 0x040005B7 RID: 1463
		private PerformanceCounterPermission owner;
	}
}
