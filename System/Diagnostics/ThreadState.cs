﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000256 RID: 598
	public enum ThreadState
	{
		// Token: 0x04000667 RID: 1639
		Initialized,
		// Token: 0x04000668 RID: 1640
		Ready,
		// Token: 0x04000669 RID: 1641
		Running,
		// Token: 0x0400066A RID: 1642
		Standby,
		// Token: 0x0400066B RID: 1643
		Terminated,
		// Token: 0x0400066C RID: 1644
		Transition = 6,
		// Token: 0x0400066D RID: 1645
		Unknown,
		// Token: 0x0400066E RID: 1646
		Wait = 5
	}
}
