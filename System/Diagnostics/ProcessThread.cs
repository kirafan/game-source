﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x0200024B RID: 587
	[System.ComponentModel.Designer("System.Diagnostics.Design.ProcessThreadDesigner, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	public class ProcessThread : System.ComponentModel.Component
	{
		// Token: 0x060014B1 RID: 5297 RVA: 0x0003700C File Offset: 0x0003520C
		[MonoTODO("Parse parameters")]
		internal ProcessThread()
		{
		}

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x060014B2 RID: 5298 RVA: 0x00037014 File Offset: 0x00035214
		[MonoTODO]
		[MonitoringDescription("The base priority of this thread.")]
		public int BasePriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x060014B3 RID: 5299 RVA: 0x00037018 File Offset: 0x00035218
		[MonoTODO]
		[MonitoringDescription("The current priority of this thread.")]
		public int CurrentPriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x060014B4 RID: 5300 RVA: 0x0003701C File Offset: 0x0003521C
		[MonoTODO]
		[MonitoringDescription("The ID of this thread.")]
		public int Id
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170004EB RID: 1259
		// (set) Token: 0x060014B5 RID: 5301 RVA: 0x00037020 File Offset: 0x00035220
		[System.ComponentModel.Browsable(false)]
		[MonoTODO]
		public int IdealProcessor
		{
			set
			{
			}
		}

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x060014B6 RID: 5302 RVA: 0x00037024 File Offset: 0x00035224
		// (set) Token: 0x060014B7 RID: 5303 RVA: 0x00037028 File Offset: 0x00035228
		[MonoTODO]
		[MonitoringDescription("Thread gets a priority boot when interactively used by a user.")]
		public bool PriorityBoostEnabled
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x060014B8 RID: 5304 RVA: 0x0003702C File Offset: 0x0003522C
		// (set) Token: 0x060014B9 RID: 5305 RVA: 0x00037030 File Offset: 0x00035230
		[MonoTODO]
		[MonitoringDescription("The priority level of this thread.")]
		public ThreadPriorityLevel PriorityLevel
		{
			get
			{
				return ThreadPriorityLevel.Idle;
			}
			set
			{
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x060014BA RID: 5306 RVA: 0x00037034 File Offset: 0x00035234
		[MonoTODO]
		[MonitoringDescription("The amount of CPU time used in privileged mode.")]
		public TimeSpan PrivilegedProcessorTime
		{
			get
			{
				return new TimeSpan(0L);
			}
		}

		// Token: 0x170004EF RID: 1263
		// (set) Token: 0x060014BB RID: 5307 RVA: 0x00037040 File Offset: 0x00035240
		[MonoTODO]
		[System.ComponentModel.Browsable(false)]
		public IntPtr ProcessorAffinity
		{
			set
			{
			}
		}

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x060014BC RID: 5308 RVA: 0x00037044 File Offset: 0x00035244
		[MonitoringDescription("The start address in memory of this thread.")]
		[MonoTODO]
		public IntPtr StartAddress
		{
			get
			{
				return (IntPtr)0;
			}
		}

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x060014BD RID: 5309 RVA: 0x0003704C File Offset: 0x0003524C
		[MonitoringDescription("The time this thread was started.")]
		[MonoTODO]
		public DateTime StartTime
		{
			get
			{
				return new DateTime(0L);
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x060014BE RID: 5310 RVA: 0x00037058 File Offset: 0x00035258
		[MonitoringDescription("The current state of this thread.")]
		[MonoTODO]
		public ThreadState ThreadState
		{
			get
			{
				return ThreadState.Initialized;
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x060014BF RID: 5311 RVA: 0x0003705C File Offset: 0x0003525C
		[MonitoringDescription("The total amount of CPU time used.")]
		[MonoTODO]
		public TimeSpan TotalProcessorTime
		{
			get
			{
				return new TimeSpan(0L);
			}
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x060014C0 RID: 5312 RVA: 0x00037068 File Offset: 0x00035268
		[MonoTODO]
		[MonitoringDescription("The amount of CPU time used in user mode.")]
		public TimeSpan UserProcessorTime
		{
			get
			{
				return new TimeSpan(0L);
			}
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x060014C1 RID: 5313 RVA: 0x00037074 File Offset: 0x00035274
		[MonitoringDescription("The reason why this thread is waiting.")]
		[MonoTODO]
		public ThreadWaitReason WaitReason
		{
			get
			{
				return ThreadWaitReason.Executive;
			}
		}

		// Token: 0x060014C2 RID: 5314 RVA: 0x00037078 File Offset: 0x00035278
		[MonoTODO]
		public void ResetIdealProcessor()
		{
		}
	}
}
