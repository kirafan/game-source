﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200023B RID: 571
	[Flags]
	public enum PerformanceCounterPermissionAccess
	{
		// Token: 0x040005AD RID: 1453
		None = 0,
		// Token: 0x040005AE RID: 1454
		[Obsolete]
		Browse = 1,
		// Token: 0x040005AF RID: 1455
		Read = 1,
		// Token: 0x040005B0 RID: 1456
		Write = 2,
		// Token: 0x040005B1 RID: 1457
		[Obsolete]
		Instrument = 3,
		// Token: 0x040005B2 RID: 1458
		Administer = 7
	}
}
