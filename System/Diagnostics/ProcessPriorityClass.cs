﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000248 RID: 584
	public enum ProcessPriorityClass
	{
		// Token: 0x04000623 RID: 1571
		AboveNormal = 32768,
		// Token: 0x04000624 RID: 1572
		BelowNormal = 16384,
		// Token: 0x04000625 RID: 1573
		High = 128,
		// Token: 0x04000626 RID: 1574
		Idle = 64,
		// Token: 0x04000627 RID: 1575
		Normal = 32,
		// Token: 0x04000628 RID: 1576
		RealTime = 256
	}
}
