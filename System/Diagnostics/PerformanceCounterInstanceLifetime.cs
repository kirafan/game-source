﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000239 RID: 569
	public enum PerformanceCounterInstanceLifetime
	{
		// Token: 0x040005AA RID: 1450
		Global,
		// Token: 0x040005AB RID: 1451
		Process
	}
}
