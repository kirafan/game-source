﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000255 RID: 597
	public enum ThreadPriorityLevel
	{
		// Token: 0x0400065F RID: 1631
		AboveNormal = 1,
		// Token: 0x04000660 RID: 1632
		BelowNormal = -1,
		// Token: 0x04000661 RID: 1633
		Highest = 2,
		// Token: 0x04000662 RID: 1634
		Idle = -15,
		// Token: 0x04000663 RID: 1635
		Lowest = -2,
		// Token: 0x04000664 RID: 1636
		Normal = 0,
		// Token: 0x04000665 RID: 1637
		TimeCritical = 15
	}
}
