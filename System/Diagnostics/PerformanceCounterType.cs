﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x02000240 RID: 576
	[System.ComponentModel.TypeConverter(typeof(AlphabeticalEnumConverter))]
	public enum PerformanceCounterType
	{
		// Token: 0x040005BD RID: 1469
		NumberOfItemsHEX32,
		// Token: 0x040005BE RID: 1470
		NumberOfItemsHEX64 = 256,
		// Token: 0x040005BF RID: 1471
		NumberOfItems32 = 65536,
		// Token: 0x040005C0 RID: 1472
		NumberOfItems64 = 65792,
		// Token: 0x040005C1 RID: 1473
		CounterDelta32 = 4195328,
		// Token: 0x040005C2 RID: 1474
		CounterDelta64 = 4195584,
		// Token: 0x040005C3 RID: 1475
		SampleCounter = 4260864,
		// Token: 0x040005C4 RID: 1476
		CountPerTimeInterval32 = 4523008,
		// Token: 0x040005C5 RID: 1477
		CountPerTimeInterval64 = 4523264,
		// Token: 0x040005C6 RID: 1478
		RateOfCountsPerSecond32 = 272696320,
		// Token: 0x040005C7 RID: 1479
		RateOfCountsPerSecond64 = 272696576,
		// Token: 0x040005C8 RID: 1480
		RawFraction = 537003008,
		// Token: 0x040005C9 RID: 1481
		CounterTimer = 541132032,
		// Token: 0x040005CA RID: 1482
		Timer100Ns = 542180608,
		// Token: 0x040005CB RID: 1483
		SampleFraction = 549585920,
		// Token: 0x040005CC RID: 1484
		CounterTimerInverse = 557909248,
		// Token: 0x040005CD RID: 1485
		Timer100NsInverse = 558957824,
		// Token: 0x040005CE RID: 1486
		CounterMultiTimer = 574686464,
		// Token: 0x040005CF RID: 1487
		CounterMultiTimer100Ns = 575735040,
		// Token: 0x040005D0 RID: 1488
		CounterMultiTimerInverse = 591463680,
		// Token: 0x040005D1 RID: 1489
		CounterMultiTimer100NsInverse = 592512256,
		// Token: 0x040005D2 RID: 1490
		AverageTimer32 = 805438464,
		// Token: 0x040005D3 RID: 1491
		ElapsedTime = 807666944,
		// Token: 0x040005D4 RID: 1492
		AverageCount64 = 1073874176,
		// Token: 0x040005D5 RID: 1493
		SampleBase = 1073939457,
		// Token: 0x040005D6 RID: 1494
		AverageBase,
		// Token: 0x040005D7 RID: 1495
		RawBase,
		// Token: 0x040005D8 RID: 1496
		CounterMultiBase = 1107494144
	}
}
