﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x0200025A RID: 602
	public enum TraceEventType
	{
		// Token: 0x04000685 RID: 1669
		Critical = 1,
		// Token: 0x04000686 RID: 1670
		Error,
		// Token: 0x04000687 RID: 1671
		Warning = 4,
		// Token: 0x04000688 RID: 1672
		Information = 8,
		// Token: 0x04000689 RID: 1673
		Verbose = 16,
		// Token: 0x0400068A RID: 1674
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		Start = 256,
		// Token: 0x0400068B RID: 1675
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		Stop = 512,
		// Token: 0x0400068C RID: 1676
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		Suspend = 1024,
		// Token: 0x0400068D RID: 1677
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		Resume = 2048,
		// Token: 0x0400068E RID: 1678
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		Transfer = 4096
	}
}
