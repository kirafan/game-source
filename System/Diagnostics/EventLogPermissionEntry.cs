﻿using System;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x02000228 RID: 552
	[Serializable]
	public class EventLogPermissionEntry
	{
		// Token: 0x060012D5 RID: 4821 RVA: 0x0003287C File Offset: 0x00030A7C
		public EventLogPermissionEntry(EventLogPermissionAccess permissionAccess, string machineName)
		{
			System.Security.Permissions.ResourcePermissionBase.ValidateMachineName(machineName);
			this.permissionAccess = permissionAccess;
			this.machineName = machineName;
		}

		// Token: 0x17000448 RID: 1096
		// (get) Token: 0x060012D6 RID: 4822 RVA: 0x00032898 File Offset: 0x00030A98
		public string MachineName
		{
			get
			{
				return this.machineName;
			}
		}

		// Token: 0x17000449 RID: 1097
		// (get) Token: 0x060012D7 RID: 4823 RVA: 0x000328A0 File Offset: 0x00030AA0
		public EventLogPermissionAccess PermissionAccess
		{
			get
			{
				return this.permissionAccess;
			}
		}

		// Token: 0x060012D8 RID: 4824 RVA: 0x000328A8 File Offset: 0x00030AA8
		internal System.Security.Permissions.ResourcePermissionBaseEntry CreateResourcePermissionBaseEntry()
		{
			return new System.Security.Permissions.ResourcePermissionBaseEntry((int)this.permissionAccess, new string[]
			{
				this.machineName
			});
		}

		// Token: 0x04000562 RID: 1378
		private EventLogPermissionAccess permissionAccess;

		// Token: 0x04000563 RID: 1379
		private string machineName;
	}
}
