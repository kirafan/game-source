﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200020D RID: 525
	[SwitchLevel(typeof(bool))]
	public class BooleanSwitch : Switch
	{
		// Token: 0x0600118F RID: 4495 RVA: 0x0002EBA8 File Offset: 0x0002CDA8
		public BooleanSwitch(string displayName, string description) : base(displayName, description)
		{
		}

		// Token: 0x06001190 RID: 4496 RVA: 0x0002EBB4 File Offset: 0x0002CDB4
		public BooleanSwitch(string displayName, string description, string defaultSwitchValue) : base(displayName, description, defaultSwitchValue)
		{
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x06001191 RID: 4497 RVA: 0x0002EBC0 File Offset: 0x0002CDC0
		// (set) Token: 0x06001192 RID: 4498 RVA: 0x0002EBD0 File Offset: 0x0002CDD0
		public bool Enabled
		{
			get
			{
				return base.SwitchSetting != 0;
			}
			set
			{
				base.SwitchSetting = Convert.ToInt32(value);
			}
		}

		// Token: 0x06001193 RID: 4499 RVA: 0x0002EBE0 File Offset: 0x0002CDE0
		protected override void OnValueChanged()
		{
			int num;
			if (int.TryParse(base.Value, out num))
			{
				this.Enabled = (num != 0);
			}
			else
			{
				this.Enabled = Convert.ToBoolean(base.Value);
			}
		}
	}
}
