﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000234 RID: 564
	public enum OverflowAction
	{
		// Token: 0x04000592 RID: 1426
		DoNotOverwrite = -1,
		// Token: 0x04000593 RID: 1427
		OverwriteAsNeeded,
		// Token: 0x04000594 RID: 1428
		OverwriteOlder
	}
}
