﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000224 RID: 548
	[Flags]
	public enum EventLogPermissionAccess
	{
		// Token: 0x04000558 RID: 1368
		None = 0,
		// Token: 0x04000559 RID: 1369
		[Obsolete]
		Browse = 2,
		// Token: 0x0400055A RID: 1370
		[Obsolete]
		Instrument = 6,
		// Token: 0x0400055B RID: 1371
		[Obsolete]
		Audit = 10,
		// Token: 0x0400055C RID: 1372
		Write = 16,
		// Token: 0x0400055D RID: 1373
		Administer = 48
	}
}
