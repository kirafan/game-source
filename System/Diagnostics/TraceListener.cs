﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x02000260 RID: 608
	public abstract class TraceListener : MarshalByRefObject, IDisposable
	{
		// Token: 0x0600157C RID: 5500 RVA: 0x00038B58 File Offset: 0x00036D58
		protected TraceListener() : this(string.Empty)
		{
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x00038B68 File Offset: 0x00036D68
		protected TraceListener(string name)
		{
			this.Name = name;
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x0600157E RID: 5502 RVA: 0x00038B9C File Offset: 0x00036D9C
		// (set) Token: 0x0600157F RID: 5503 RVA: 0x00038BA4 File Offset: 0x00036DA4
		public int IndentLevel
		{
			get
			{
				return this.indentLevel;
			}
			set
			{
				this.indentLevel = value;
			}
		}

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x06001580 RID: 5504 RVA: 0x00038BB0 File Offset: 0x00036DB0
		// (set) Token: 0x06001581 RID: 5505 RVA: 0x00038BB8 File Offset: 0x00036DB8
		public int IndentSize
		{
			get
			{
				return this.indentSize;
			}
			set
			{
				this.indentSize = value;
			}
		}

		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x06001582 RID: 5506 RVA: 0x00038BC4 File Offset: 0x00036DC4
		// (set) Token: 0x06001583 RID: 5507 RVA: 0x00038BCC File Offset: 0x00036DCC
		public virtual string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000524 RID: 1316
		// (get) Token: 0x06001584 RID: 5508 RVA: 0x00038BD8 File Offset: 0x00036DD8
		// (set) Token: 0x06001585 RID: 5509 RVA: 0x00038BE0 File Offset: 0x00036DE0
		protected bool NeedIndent
		{
			get
			{
				return this.needIndent;
			}
			set
			{
				this.needIndent = value;
			}
		}

		// Token: 0x17000525 RID: 1317
		// (get) Token: 0x06001586 RID: 5510 RVA: 0x00038BEC File Offset: 0x00036DEC
		[MonoLimitation("This property exists but is never considered.")]
		public virtual bool IsThreadSafe
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x00038BF0 File Offset: 0x00036DF0
		public virtual void Close()
		{
			this.Dispose();
		}

		// Token: 0x06001588 RID: 5512 RVA: 0x00038BF8 File Offset: 0x00036DF8
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x00038C08 File Offset: 0x00036E08
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x00038C0C File Offset: 0x00036E0C
		public virtual void Fail(string message)
		{
			this.Fail(message, string.Empty);
		}

		// Token: 0x0600158B RID: 5515 RVA: 0x00038C1C File Offset: 0x00036E1C
		public virtual void Fail(string message, string detailMessage)
		{
			this.WriteLine("---- DEBUG ASSERTION FAILED ----");
			this.WriteLine("---- Assert Short Message ----");
			this.WriteLine(message);
			this.WriteLine("---- Assert Long Message ----");
			this.WriteLine(detailMessage);
			this.WriteLine(string.Empty);
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x00038C64 File Offset: 0x00036E64
		public virtual void Flush()
		{
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x00038C68 File Offset: 0x00036E68
		public virtual void Write(object o)
		{
			this.Write(o.ToString());
		}

		// Token: 0x0600158E RID: 5518
		public abstract void Write(string message);

		// Token: 0x0600158F RID: 5519 RVA: 0x00038C78 File Offset: 0x00036E78
		public virtual void Write(object o, string category)
		{
			this.Write(o.ToString(), category);
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x00038C88 File Offset: 0x00036E88
		public virtual void Write(string message, string category)
		{
			this.Write(category + ": " + message);
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x00038C9C File Offset: 0x00036E9C
		protected virtual void WriteIndent()
		{
			this.NeedIndent = false;
			string message = new string(' ', this.IndentLevel * this.IndentSize);
			this.Write(message);
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x00038CCC File Offset: 0x00036ECC
		public virtual void WriteLine(object o)
		{
			this.WriteLine(o.ToString());
		}

		// Token: 0x06001593 RID: 5523
		public abstract void WriteLine(string message);

		// Token: 0x06001594 RID: 5524 RVA: 0x00038CDC File Offset: 0x00036EDC
		public virtual void WriteLine(object o, string category)
		{
			this.WriteLine(o.ToString(), category);
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x00038CEC File Offset: 0x00036EEC
		public virtual void WriteLine(string message, string category)
		{
			this.WriteLine(category + ": " + message);
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x00038D00 File Offset: 0x00036F00
		internal static string FormatArray(ICollection list, string joiner)
		{
			string[] array = new string[list.Count];
			int num = 0;
			foreach (object obj in list)
			{
				array[num++] = ((obj == null) ? string.Empty : obj.ToString());
			}
			return string.Join(joiner, array);
		}

		// Token: 0x06001597 RID: 5527 RVA: 0x00038D94 File Offset: 0x00036F94
		[ComVisible(false)]
		public virtual void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
		{
			if (this.Filter != null && !this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, data, null))
			{
				return;
			}
			this.WriteLine(string.Format("{0} {1}: {2} : {3}", new object[]
			{
				source,
				eventType,
				id,
				data
			}));
			if (eventCache == null)
			{
				return;
			}
			if ((this.TraceOutputOptions & TraceOptions.ProcessId) != TraceOptions.None)
			{
				this.WriteLine("    ProcessId=" + eventCache.ProcessId);
			}
			if ((this.TraceOutputOptions & TraceOptions.LogicalOperationStack) != TraceOptions.None)
			{
				this.WriteLine("    LogicalOperationStack=" + TraceListener.FormatArray(eventCache.LogicalOperationStack, ", "));
			}
			if ((this.TraceOutputOptions & TraceOptions.ThreadId) != TraceOptions.None)
			{
				this.WriteLine("    ThreadId=" + eventCache.ThreadId);
			}
			if ((this.TraceOutputOptions & TraceOptions.DateTime) != TraceOptions.None)
			{
				this.WriteLine("    DateTime=" + eventCache.DateTime.ToString("o"));
			}
			if ((this.TraceOutputOptions & TraceOptions.Timestamp) != TraceOptions.None)
			{
				this.WriteLine("    Timestamp=" + eventCache.Timestamp);
			}
			if ((this.TraceOutputOptions & TraceOptions.Callstack) != TraceOptions.None)
			{
				this.WriteLine("    Callstack=" + eventCache.Callstack);
			}
		}

		// Token: 0x06001598 RID: 5528 RVA: 0x00038EF8 File Offset: 0x000370F8
		[ComVisible(false)]
		public virtual void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
		{
			if (this.Filter != null && !this.Filter.ShouldTrace(eventCache, source, eventType, id, null, null, null, data))
			{
				return;
			}
			this.TraceData(eventCache, source, eventType, id, TraceListener.FormatArray(data, " "));
		}

		// Token: 0x06001599 RID: 5529 RVA: 0x00038F44 File Offset: 0x00037144
		[ComVisible(false)]
		public virtual void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
		{
			this.TraceEvent(eventCache, source, eventType, id, null);
		}

		// Token: 0x0600159A RID: 5530 RVA: 0x00038F54 File Offset: 0x00037154
		[ComVisible(false)]
		public virtual void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
		{
			this.TraceData(eventCache, source, eventType, id, message);
		}

		// Token: 0x0600159B RID: 5531 RVA: 0x00038F64 File Offset: 0x00037164
		[ComVisible(false)]
		public virtual void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
		{
			this.TraceEvent(eventCache, source, eventType, id, string.Format(format, args));
		}

		// Token: 0x0600159C RID: 5532 RVA: 0x00038F7C File Offset: 0x0003717C
		[ComVisible(false)]
		public virtual void TraceTransfer(TraceEventCache eventCache, string source, int id, string message, Guid relatedActivityId)
		{
			this.TraceEvent(eventCache, source, TraceEventType.Transfer, id, string.Format("{0}, relatedActivityId={1}", message, relatedActivityId));
		}

		// Token: 0x0600159D RID: 5533 RVA: 0x00038FAC File Offset: 0x000371AC
		protected internal virtual string[] GetSupportedAttributes()
		{
			return null;
		}

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x0600159E RID: 5534 RVA: 0x00038FB0 File Offset: 0x000371B0
		public System.Collections.Specialized.StringDictionary Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x0600159F RID: 5535 RVA: 0x00038FB8 File Offset: 0x000371B8
		// (set) Token: 0x060015A0 RID: 5536 RVA: 0x00038FC0 File Offset: 0x000371C0
		[ComVisible(false)]
		public TraceFilter Filter
		{
			get
			{
				return this.filter;
			}
			set
			{
				this.filter = value;
			}
		}

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x060015A1 RID: 5537 RVA: 0x00038FCC File Offset: 0x000371CC
		// (set) Token: 0x060015A2 RID: 5538 RVA: 0x00038FD4 File Offset: 0x000371D4
		[ComVisible(false)]
		public TraceOptions TraceOutputOptions
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		// Token: 0x040006A2 RID: 1698
		[ThreadStatic]
		private int indentLevel;

		// Token: 0x040006A3 RID: 1699
		[ThreadStatic]
		private int indentSize = 4;

		// Token: 0x040006A4 RID: 1700
		[ThreadStatic]
		private System.Collections.Specialized.StringDictionary attributes = new System.Collections.Specialized.StringDictionary();

		// Token: 0x040006A5 RID: 1701
		[ThreadStatic]
		private TraceFilter filter;

		// Token: 0x040006A6 RID: 1702
		[ThreadStatic]
		private TraceOptions options;

		// Token: 0x040006A7 RID: 1703
		private string name;

		// Token: 0x040006A8 RID: 1704
		private bool needIndent = true;
	}
}
