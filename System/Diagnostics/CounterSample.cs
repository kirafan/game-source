﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000213 RID: 531
	public struct CounterSample
	{
		// Token: 0x060011B6 RID: 4534 RVA: 0x0002F2A8 File Offset: 0x0002D4A8
		public CounterSample(long rawValue, long baseValue, long counterFrequency, long systemFrequency, long timeStamp, long timeStamp100nSec, PerformanceCounterType counterType)
		{
			this = new CounterSample(rawValue, baseValue, counterFrequency, systemFrequency, timeStamp, timeStamp100nSec, counterType, 0L);
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x0002F2C8 File Offset: 0x0002D4C8
		public CounterSample(long rawValue, long baseValue, long counterFrequency, long systemFrequency, long timeStamp, long timeStamp100nSec, PerformanceCounterType counterType, long counterTimeStamp)
		{
			this.rawValue = rawValue;
			this.baseValue = baseValue;
			this.counterFrequency = counterFrequency;
			this.systemFrequency = systemFrequency;
			this.timeStamp = timeStamp;
			this.timeStamp100nSec = timeStamp100nSec;
			this.counterType = counterType;
			this.counterTimeStamp = counterTimeStamp;
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x060011B9 RID: 4537 RVA: 0x0002F334 File Offset: 0x0002D534
		public long BaseValue
		{
			get
			{
				return this.baseValue;
			}
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x060011BA RID: 4538 RVA: 0x0002F33C File Offset: 0x0002D53C
		public long CounterFrequency
		{
			get
			{
				return this.counterFrequency;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x060011BB RID: 4539 RVA: 0x0002F344 File Offset: 0x0002D544
		public long CounterTimeStamp
		{
			get
			{
				return this.counterTimeStamp;
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x060011BC RID: 4540 RVA: 0x0002F34C File Offset: 0x0002D54C
		public PerformanceCounterType CounterType
		{
			get
			{
				return this.counterType;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x060011BD RID: 4541 RVA: 0x0002F354 File Offset: 0x0002D554
		public long RawValue
		{
			get
			{
				return this.rawValue;
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x060011BE RID: 4542 RVA: 0x0002F35C File Offset: 0x0002D55C
		public long SystemFrequency
		{
			get
			{
				return this.systemFrequency;
			}
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x060011BF RID: 4543 RVA: 0x0002F364 File Offset: 0x0002D564
		public long TimeStamp
		{
			get
			{
				return this.timeStamp;
			}
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x060011C0 RID: 4544 RVA: 0x0002F36C File Offset: 0x0002D56C
		public long TimeStamp100nSec
		{
			get
			{
				return this.timeStamp100nSec;
			}
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x0002F374 File Offset: 0x0002D574
		public static float Calculate(CounterSample counterSample)
		{
			return CounterSampleCalculator.ComputeCounterValue(counterSample);
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x0002F37C File Offset: 0x0002D57C
		public static float Calculate(CounterSample counterSample, CounterSample nextCounterSample)
		{
			return CounterSampleCalculator.ComputeCounterValue(counterSample, nextCounterSample);
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x0002F388 File Offset: 0x0002D588
		public override bool Equals(object obj)
		{
			return obj is CounterSample && this.Equals((CounterSample)obj);
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x0002F3A4 File Offset: 0x0002D5A4
		public bool Equals(CounterSample other)
		{
			return this.rawValue == other.rawValue && this.baseValue == other.counterFrequency && this.counterFrequency == other.counterFrequency && this.systemFrequency == other.systemFrequency && this.timeStamp == other.timeStamp && this.timeStamp100nSec == other.timeStamp100nSec && this.counterTimeStamp == other.counterTimeStamp && this.counterType == other.counterType;
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x0002F444 File Offset: 0x0002D644
		public override int GetHashCode()
		{
			return (int)(this.rawValue << 28 ^ (this.baseValue << 24 ^ (this.counterFrequency << 20 ^ (this.systemFrequency << 16 ^ (this.timeStamp << 8 ^ (this.timeStamp100nSec << 4 ^ (this.counterTimeStamp ^ (long)this.counterType)))))));
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x0002F49C File Offset: 0x0002D69C
		public static bool operator ==(CounterSample obj1, CounterSample obj2)
		{
			return obj1.Equals(obj2);
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x0002F4A8 File Offset: 0x0002D6A8
		public static bool operator !=(CounterSample obj1, CounterSample obj2)
		{
			return !obj1.Equals(obj2);
		}

		// Token: 0x0400050E RID: 1294
		private long rawValue;

		// Token: 0x0400050F RID: 1295
		private long baseValue;

		// Token: 0x04000510 RID: 1296
		private long counterFrequency;

		// Token: 0x04000511 RID: 1297
		private long systemFrequency;

		// Token: 0x04000512 RID: 1298
		private long timeStamp;

		// Token: 0x04000513 RID: 1299
		private long timeStamp100nSec;

		// Token: 0x04000514 RID: 1300
		private long counterTimeStamp;

		// Token: 0x04000515 RID: 1301
		private PerformanceCounterType counterType;

		// Token: 0x04000516 RID: 1302
		public static CounterSample Empty = new CounterSample(0L, 0L, 0L, 0L, 0L, 0L, PerformanceCounterType.NumberOfItems32, 0L);
	}
}
