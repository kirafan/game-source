﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200020F RID: 527
	public class CorrelationManager
	{
		// Token: 0x06001197 RID: 4503 RVA: 0x0002EC60 File Offset: 0x0002CE60
		internal CorrelationManager()
		{
		}

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x06001198 RID: 4504 RVA: 0x0002EC74 File Offset: 0x0002CE74
		// (set) Token: 0x06001199 RID: 4505 RVA: 0x0002EC7C File Offset: 0x0002CE7C
		public Guid ActivityId
		{
			get
			{
				return this.activity;
			}
			set
			{
				this.activity = value;
			}
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x0600119A RID: 4506 RVA: 0x0002EC88 File Offset: 0x0002CE88
		public Stack LogicalOperationStack
		{
			get
			{
				return this.op_stack;
			}
		}

		// Token: 0x0600119B RID: 4507 RVA: 0x0002EC90 File Offset: 0x0002CE90
		public void StartLogicalOperation()
		{
			this.StartLogicalOperation(Guid.NewGuid());
		}

		// Token: 0x0600119C RID: 4508 RVA: 0x0002ECA4 File Offset: 0x0002CEA4
		public void StartLogicalOperation(object operationId)
		{
			this.op_stack.Push(operationId);
		}

		// Token: 0x0600119D RID: 4509 RVA: 0x0002ECB4 File Offset: 0x0002CEB4
		public void StopLogicalOperation()
		{
			this.op_stack.Pop();
		}

		// Token: 0x04000509 RID: 1289
		private Guid activity;

		// Token: 0x0400050A RID: 1290
		private Stack op_stack = new Stack();
	}
}
