﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace System.Diagnostics
{
	// Token: 0x02000262 RID: 610
	public class TraceSource
	{
		// Token: 0x060015A3 RID: 5539 RVA: 0x00038FE0 File Offset: 0x000371E0
		public TraceSource(string name) : this(name, SourceLevels.Off)
		{
		}

		// Token: 0x060015A4 RID: 5540 RVA: 0x00038FEC File Offset: 0x000371EC
		public TraceSource(string name, SourceLevels sourceLevels)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			Hashtable hashtable = DiagnosticsConfiguration.Settings["sources"] as Hashtable;
			TraceSourceInfo traceSourceInfo = (hashtable == null) ? null : (hashtable[name] as TraceSourceInfo);
			this.source_switch = new SourceSwitch(name);
			if (traceSourceInfo == null)
			{
				this.listeners = new TraceListenerCollection();
			}
			else
			{
				this.source_switch.Level = traceSourceInfo.Levels;
				this.listeners = traceSourceInfo.Listeners;
			}
		}

		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x060015A5 RID: 5541 RVA: 0x00039080 File Offset: 0x00037280
		public System.Collections.Specialized.StringDictionary Attributes
		{
			get
			{
				return this.source_switch.Attributes;
			}
		}

		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x060015A6 RID: 5542 RVA: 0x00039090 File Offset: 0x00037290
		public TraceListenerCollection Listeners
		{
			get
			{
				return this.listeners;
			}
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x060015A7 RID: 5543 RVA: 0x00039098 File Offset: 0x00037298
		public string Name
		{
			get
			{
				return this.source_switch.DisplayName;
			}
		}

		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x060015A8 RID: 5544 RVA: 0x000390A8 File Offset: 0x000372A8
		// (set) Token: 0x060015A9 RID: 5545 RVA: 0x000390B0 File Offset: 0x000372B0
		public SourceSwitch Switch
		{
			get
			{
				return this.source_switch;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.source_switch = value;
			}
		}

		// Token: 0x060015AA RID: 5546 RVA: 0x000390CC File Offset: 0x000372CC
		public void Close()
		{
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.Close();
				}
			}
		}

		// Token: 0x060015AB RID: 5547 RVA: 0x00039170 File Offset: 0x00037370
		public void Flush()
		{
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.Flush();
				}
			}
		}

		// Token: 0x060015AC RID: 5548 RVA: 0x00039214 File Offset: 0x00037414
		[Conditional("TRACE")]
		public void TraceData(TraceEventType eventType, int id, object data)
		{
			if (!this.source_switch.ShouldTrace(eventType))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceData(null, this.Name, eventType, id, data);
				}
			}
		}

		// Token: 0x060015AD RID: 5549 RVA: 0x000392D4 File Offset: 0x000374D4
		[Conditional("TRACE")]
		public void TraceData(TraceEventType eventType, int id, params object[] data)
		{
			if (!this.source_switch.ShouldTrace(eventType))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceData(null, this.Name, eventType, id, data);
				}
			}
		}

		// Token: 0x060015AE RID: 5550 RVA: 0x00039394 File Offset: 0x00037594
		[Conditional("TRACE")]
		public void TraceEvent(TraceEventType eventType, int id)
		{
			if (!this.source_switch.ShouldTrace(eventType))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceEvent(null, this.Name, eventType, id);
				}
			}
		}

		// Token: 0x060015AF RID: 5551 RVA: 0x00039454 File Offset: 0x00037654
		[Conditional("TRACE")]
		public void TraceEvent(TraceEventType eventType, int id, string message)
		{
			if (!this.source_switch.ShouldTrace(eventType))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceEvent(null, this.Name, eventType, id, message);
				}
			}
		}

		// Token: 0x060015B0 RID: 5552 RVA: 0x00039514 File Offset: 0x00037714
		[Conditional("TRACE")]
		public void TraceEvent(TraceEventType eventType, int id, string format, params object[] args)
		{
			if (!this.source_switch.ShouldTrace(eventType))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceEvent(null, this.Name, eventType, id, format, args);
				}
			}
		}

		// Token: 0x060015B1 RID: 5553 RVA: 0x000395D8 File Offset: 0x000377D8
		[Conditional("TRACE")]
		public void TraceInformation(string format)
		{
		}

		// Token: 0x060015B2 RID: 5554 RVA: 0x000395DC File Offset: 0x000377DC
		[Conditional("TRACE")]
		public void TraceInformation(string format, params object[] args)
		{
		}

		// Token: 0x060015B3 RID: 5555 RVA: 0x000395E0 File Offset: 0x000377E0
		[Conditional("TRACE")]
		public void TraceTransfer(int id, string message, Guid relatedActivityId)
		{
			if (!this.source_switch.ShouldTrace(TraceEventType.Transfer))
			{
				return;
			}
			object syncRoot = ((ICollection)this.listeners).SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener = (TraceListener)obj;
					traceListener.TraceTransfer(null, this.Name, id, message, relatedActivityId);
				}
			}
		}

		// Token: 0x060015B4 RID: 5556 RVA: 0x000396A4 File Offset: 0x000378A4
		protected virtual string[] GetSupportedAttributes()
		{
			return null;
		}

		// Token: 0x040006B1 RID: 1713
		private SourceSwitch source_switch;

		// Token: 0x040006B2 RID: 1714
		private TraceListenerCollection listeners;
	}
}
