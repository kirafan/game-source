﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200024A RID: 586
	public class ProcessThreadCollection : ReadOnlyCollectionBase
	{
		// Token: 0x060014A7 RID: 5287 RVA: 0x00036F74 File Offset: 0x00035174
		protected ProcessThreadCollection()
		{
		}

		// Token: 0x060014A8 RID: 5288 RVA: 0x00036F7C File Offset: 0x0003517C
		public ProcessThreadCollection(ProcessThread[] processThreads)
		{
			base.InnerList.AddRange(processThreads);
		}

		// Token: 0x060014A9 RID: 5289 RVA: 0x00036F90 File Offset: 0x00035190
		internal static ProcessThreadCollection GetEmpty()
		{
			return new ProcessThreadCollection();
		}

		// Token: 0x170004E7 RID: 1255
		public ProcessThread this[int index]
		{
			get
			{
				return (ProcessThread)base.InnerList[index];
			}
		}

		// Token: 0x060014AB RID: 5291 RVA: 0x00036FAC File Offset: 0x000351AC
		public int Add(ProcessThread thread)
		{
			return base.InnerList.Add(thread);
		}

		// Token: 0x060014AC RID: 5292 RVA: 0x00036FBC File Offset: 0x000351BC
		public bool Contains(ProcessThread thread)
		{
			return base.InnerList.Contains(thread);
		}

		// Token: 0x060014AD RID: 5293 RVA: 0x00036FCC File Offset: 0x000351CC
		public void CopyTo(ProcessThread[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x060014AE RID: 5294 RVA: 0x00036FDC File Offset: 0x000351DC
		public int IndexOf(ProcessThread thread)
		{
			return base.InnerList.IndexOf(thread);
		}

		// Token: 0x060014AF RID: 5295 RVA: 0x00036FEC File Offset: 0x000351EC
		public void Insert(int index, ProcessThread thread)
		{
			base.InnerList.Insert(index, thread);
		}

		// Token: 0x060014B0 RID: 5296 RVA: 0x00036FFC File Offset: 0x000351FC
		public void Remove(ProcessThread thread)
		{
			base.InnerList.Remove(thread);
		}
	}
}
