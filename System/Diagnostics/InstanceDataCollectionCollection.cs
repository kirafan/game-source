﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200022E RID: 558
	public class InstanceDataCollectionCollection : DictionaryBase
	{
		// Token: 0x0600131E RID: 4894 RVA: 0x000330FC File Offset: 0x000312FC
		[Obsolete("Use PerformanceCounterCategory.ReadCategory()")]
		public InstanceDataCollectionCollection()
		{
		}

		// Token: 0x0600131F RID: 4895 RVA: 0x00033104 File Offset: 0x00031304
		private static void CheckNull(object value, string name)
		{
			if (value == null)
			{
				throw new ArgumentNullException(name);
			}
		}

		// Token: 0x1700046F RID: 1135
		public InstanceDataCollection this[string counterName]
		{
			get
			{
				InstanceDataCollectionCollection.CheckNull(counterName, "counterName");
				return (InstanceDataCollection)base.Dictionary[counterName];
			}
		}

		// Token: 0x17000470 RID: 1136
		// (get) Token: 0x06001321 RID: 4897 RVA: 0x00033134 File Offset: 0x00031334
		public ICollection Keys
		{
			get
			{
				return base.Dictionary.Keys;
			}
		}

		// Token: 0x17000471 RID: 1137
		// (get) Token: 0x06001322 RID: 4898 RVA: 0x00033144 File Offset: 0x00031344
		public ICollection Values
		{
			get
			{
				return base.Dictionary.Values;
			}
		}

		// Token: 0x06001323 RID: 4899 RVA: 0x00033154 File Offset: 0x00031354
		public bool Contains(string counterName)
		{
			InstanceDataCollectionCollection.CheckNull(counterName, "counterName");
			return base.Dictionary.Contains(counterName);
		}

		// Token: 0x06001324 RID: 4900 RVA: 0x00033170 File Offset: 0x00031370
		public void CopyTo(InstanceDataCollection[] counters, int index)
		{
			base.Dictionary.CopyTo(counters, index);
		}
	}
}
