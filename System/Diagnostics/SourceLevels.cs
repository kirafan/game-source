﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x0200024E RID: 590
	[Flags]
	public enum SourceLevels
	{
		// Token: 0x04000644 RID: 1604
		Off = 0,
		// Token: 0x04000645 RID: 1605
		Critical = 1,
		// Token: 0x04000646 RID: 1606
		Error = 3,
		// Token: 0x04000647 RID: 1607
		Warning = 7,
		// Token: 0x04000648 RID: 1608
		Information = 15,
		// Token: 0x04000649 RID: 1609
		Verbose = 31,
		// Token: 0x0400064A RID: 1610
		[System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
		ActivityTracing = 65280,
		// Token: 0x0400064B RID: 1611
		All = -1
	}
}
