﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000261 RID: 609
	[Flags]
	public enum TraceOptions
	{
		// Token: 0x040006AA RID: 1706
		None = 0,
		// Token: 0x040006AB RID: 1707
		LogicalOperationStack = 1,
		// Token: 0x040006AC RID: 1708
		DateTime = 2,
		// Token: 0x040006AD RID: 1709
		Timestamp = 4,
		// Token: 0x040006AE RID: 1710
		ProcessId = 8,
		// Token: 0x040006AF RID: 1711
		ThreadId = 16,
		// Token: 0x040006B0 RID: 1712
		Callstack = 32
	}
}
