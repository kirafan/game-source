﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200050C RID: 1292
	// (Invoke) Token: 0x06002CDC RID: 11484
	public delegate void DataReceivedEventHandler(object sender, DataReceivedEventArgs e);
}
