﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x0200025F RID: 607
	public class TraceListenerCollection : IList, ICollection, IEnumerable
	{
		// Token: 0x0600155B RID: 5467 RVA: 0x000386B4 File Offset: 0x000368B4
		internal TraceListenerCollection() : this(true)
		{
		}

		// Token: 0x0600155C RID: 5468 RVA: 0x000386C0 File Offset: 0x000368C0
		internal TraceListenerCollection(bool addDefault)
		{
			if (addDefault)
			{
				this.Add(new DefaultTraceListener());
			}
		}

		// Token: 0x17000519 RID: 1305
		object IList.this[int index]
		{
			get
			{
				return this.listeners[index];
			}
			set
			{
				TraceListener traceListener = (TraceListener)value;
				this.InitializeListener(traceListener);
				this[index] = traceListener;
			}
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x0600155F RID: 5471 RVA: 0x00038720 File Offset: 0x00036920
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.listeners.IsSynchronized;
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06001560 RID: 5472 RVA: 0x00038730 File Offset: 0x00036930
		object ICollection.SyncRoot
		{
			get
			{
				return this.listeners.SyncRoot;
			}
		}

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06001561 RID: 5473 RVA: 0x00038740 File Offset: 0x00036940
		bool IList.IsFixedSize
		{
			get
			{
				return this.listeners.IsFixedSize;
			}
		}

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06001562 RID: 5474 RVA: 0x00038750 File Offset: 0x00036950
		bool IList.IsReadOnly
		{
			get
			{
				return this.listeners.IsReadOnly;
			}
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x00038760 File Offset: 0x00036960
		void ICollection.CopyTo(Array array, int index)
		{
			this.listeners.CopyTo(array, index);
		}

		// Token: 0x06001564 RID: 5476 RVA: 0x00038770 File Offset: 0x00036970
		int IList.Add(object value)
		{
			if (value is TraceListener)
			{
				return this.Add((TraceListener)value);
			}
			throw new NotSupportedException(Locale.GetText("You can only add TraceListener objects to the collection"));
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x0003879C File Offset: 0x0003699C
		bool IList.Contains(object value)
		{
			return value is TraceListener && this.listeners.Contains(value);
		}

		// Token: 0x06001566 RID: 5478 RVA: 0x000387B8 File Offset: 0x000369B8
		int IList.IndexOf(object value)
		{
			if (value is TraceListener)
			{
				return this.listeners.IndexOf(value);
			}
			return -1;
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x000387D4 File Offset: 0x000369D4
		void IList.Insert(int index, object value)
		{
			if (value is TraceListener)
			{
				this.Insert(index, (TraceListener)value);
				return;
			}
			throw new NotSupportedException(Locale.GetText("You can only insert TraceListener objects into the collection"));
		}

		// Token: 0x06001568 RID: 5480 RVA: 0x0003880C File Offset: 0x00036A0C
		void IList.Remove(object value)
		{
			if (value is TraceListener)
			{
				this.listeners.Remove(value);
			}
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x06001569 RID: 5481 RVA: 0x00038828 File Offset: 0x00036A28
		public int Count
		{
			get
			{
				return this.listeners.Count;
			}
		}

		// Token: 0x1700051F RID: 1311
		public TraceListener this[string name]
		{
			get
			{
				object syncRoot = this.listeners.SyncRoot;
				lock (syncRoot)
				{
					foreach (object obj in this.listeners)
					{
						TraceListener traceListener = (TraceListener)obj;
						if (traceListener.Name == name)
						{
							return traceListener;
						}
					}
				}
				return null;
			}
		}

		// Token: 0x17000520 RID: 1312
		public TraceListener this[int index]
		{
			get
			{
				return (TraceListener)this.listeners[index];
			}
			set
			{
				this.InitializeListener(value);
				this.listeners[index] = value;
			}
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x00038920 File Offset: 0x00036B20
		public int Add(TraceListener listener)
		{
			this.InitializeListener(listener);
			return this.listeners.Add(listener);
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x00038938 File Offset: 0x00036B38
		internal void Add(TraceListener listener, TraceImplSettings settings)
		{
			listener.IndentLevel = settings.IndentLevel;
			listener.IndentSize = settings.IndentSize;
			this.listeners.Add(listener);
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x00038960 File Offset: 0x00036B60
		private void InitializeListener(TraceListener listener)
		{
			listener.IndentLevel = TraceImpl.IndentLevel;
			listener.IndentSize = TraceImpl.IndentSize;
		}

		// Token: 0x06001570 RID: 5488 RVA: 0x00038978 File Offset: 0x00036B78
		private void InitializeRange(IList listeners)
		{
			int count = listeners.Count;
			for (int num = 0; num != count; num++)
			{
				this.InitializeListener((TraceListener)listeners[num]);
			}
		}

		// Token: 0x06001571 RID: 5489 RVA: 0x000389B0 File Offset: 0x00036BB0
		public void AddRange(TraceListener[] value)
		{
			this.InitializeRange(value);
			this.listeners.AddRange(value);
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x000389C8 File Offset: 0x00036BC8
		public void AddRange(TraceListenerCollection value)
		{
			this.InitializeRange(value);
			this.listeners.AddRange(value.listeners);
		}

		// Token: 0x06001573 RID: 5491 RVA: 0x000389E4 File Offset: 0x00036BE4
		public void Clear()
		{
			this.listeners.Clear();
		}

		// Token: 0x06001574 RID: 5492 RVA: 0x000389F4 File Offset: 0x00036BF4
		public bool Contains(TraceListener listener)
		{
			return this.listeners.Contains(listener);
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x00038A04 File Offset: 0x00036C04
		public void CopyTo(TraceListener[] listeners, int index)
		{
			listeners.CopyTo(listeners, index);
		}

		// Token: 0x06001576 RID: 5494 RVA: 0x00038A10 File Offset: 0x00036C10
		public IEnumerator GetEnumerator()
		{
			return this.listeners.GetEnumerator();
		}

		// Token: 0x06001577 RID: 5495 RVA: 0x00038A20 File Offset: 0x00036C20
		public int IndexOf(TraceListener listener)
		{
			return this.listeners.IndexOf(listener);
		}

		// Token: 0x06001578 RID: 5496 RVA: 0x00038A30 File Offset: 0x00036C30
		public void Insert(int index, TraceListener listener)
		{
			this.InitializeListener(listener);
			this.listeners.Insert(index, listener);
		}

		// Token: 0x06001579 RID: 5497 RVA: 0x00038A48 File Offset: 0x00036C48
		public void Remove(string name)
		{
			TraceListener traceListener = null;
			object syncRoot = this.listeners.SyncRoot;
			lock (syncRoot)
			{
				foreach (object obj in this.listeners)
				{
					TraceListener traceListener2 = (TraceListener)obj;
					if (traceListener2.Name == name)
					{
						traceListener = traceListener2;
						break;
					}
				}
				if (traceListener == null)
				{
					throw new ArgumentException(Locale.GetText("TraceListener " + name + " was not in the collection"));
				}
				this.listeners.Remove(traceListener);
			}
		}

		// Token: 0x0600157A RID: 5498 RVA: 0x00038B38 File Offset: 0x00036D38
		public void Remove(TraceListener listener)
		{
			this.listeners.Remove(listener);
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x00038B48 File Offset: 0x00036D48
		public void RemoveAt(int index)
		{
			this.listeners.RemoveAt(index);
		}

		// Token: 0x040006A1 RID: 1697
		private ArrayList listeners = ArrayList.Synchronized(new ArrayList(1));
	}
}
