﻿using System;
using System.Collections;

namespace System.Diagnostics
{
	// Token: 0x02000246 RID: 582
	public class ProcessModuleCollection : ReadOnlyCollectionBase
	{
		// Token: 0x0600146E RID: 5230 RVA: 0x00036A14 File Offset: 0x00034C14
		protected ProcessModuleCollection()
		{
		}

		// Token: 0x0600146F RID: 5231 RVA: 0x00036A1C File Offset: 0x00034C1C
		public ProcessModuleCollection(ProcessModule[] processModules)
		{
			base.InnerList.AddRange(processModules);
		}

		// Token: 0x170004CB RID: 1227
		public ProcessModule this[int index]
		{
			get
			{
				return (ProcessModule)base.InnerList[index];
			}
		}

		// Token: 0x06001471 RID: 5233 RVA: 0x00036A44 File Offset: 0x00034C44
		public bool Contains(ProcessModule module)
		{
			return base.InnerList.Contains(module);
		}

		// Token: 0x06001472 RID: 5234 RVA: 0x00036A54 File Offset: 0x00034C54
		public void CopyTo(ProcessModule[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x06001473 RID: 5235 RVA: 0x00036A64 File Offset: 0x00034C64
		public int IndexOf(ProcessModule module)
		{
			return base.InnerList.IndexOf(module);
		}
	}
}
