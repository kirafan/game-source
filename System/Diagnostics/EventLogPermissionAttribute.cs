﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x02000225 RID: 549
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Event, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public class EventLogPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x060012B6 RID: 4790 RVA: 0x00032500 File Offset: 0x00030700
		public EventLogPermissionAttribute(SecurityAction action) : base(action)
		{
			this.machineName = ".";
			this.permissionAccess = EventLogPermissionAccess.Write;
		}

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x060012B7 RID: 4791 RVA: 0x0003251C File Offset: 0x0003071C
		// (set) Token: 0x060012B8 RID: 4792 RVA: 0x00032524 File Offset: 0x00030724
		public string MachineName
		{
			get
			{
				return this.machineName;
			}
			set
			{
				System.Security.Permissions.ResourcePermissionBase.ValidateMachineName(value);
				this.machineName = value;
			}
		}

		// Token: 0x17000445 RID: 1093
		// (get) Token: 0x060012B9 RID: 4793 RVA: 0x00032534 File Offset: 0x00030734
		// (set) Token: 0x060012BA RID: 4794 RVA: 0x0003253C File Offset: 0x0003073C
		public EventLogPermissionAccess PermissionAccess
		{
			get
			{
				return this.permissionAccess;
			}
			set
			{
				this.permissionAccess = value;
			}
		}

		// Token: 0x060012BB RID: 4795 RVA: 0x00032548 File Offset: 0x00030748
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new EventLogPermission(PermissionState.Unrestricted);
			}
			return new EventLogPermission(this.permissionAccess, this.machineName);
		}

		// Token: 0x0400055E RID: 1374
		private string machineName;

		// Token: 0x0400055F RID: 1375
		private EventLogPermissionAccess permissionAccess;
	}
}
