﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000221 RID: 545
	public enum EventLogEntryType
	{
		// Token: 0x04000551 RID: 1361
		Error = 1,
		// Token: 0x04000552 RID: 1362
		Warning,
		// Token: 0x04000553 RID: 1363
		Information = 4,
		// Token: 0x04000554 RID: 1364
		SuccessAudit = 8,
		// Token: 0x04000555 RID: 1365
		FailureAudit = 16
	}
}
