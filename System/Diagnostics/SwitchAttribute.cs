﻿using System;
using System.Reflection;

namespace System.Diagnostics
{
	// Token: 0x02000251 RID: 593
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Event)]
	[MonoLimitation("This attribute is not considered in trace support.")]
	public sealed class SwitchAttribute : Attribute
	{
		// Token: 0x060014DA RID: 5338 RVA: 0x00037398 File Offset: 0x00035598
		public SwitchAttribute(string switchName, Type switchType)
		{
			if (switchName == null)
			{
				throw new ArgumentNullException("switchName");
			}
			if (switchType == null)
			{
				throw new ArgumentNullException("switchType");
			}
			this.name = switchName;
			this.type = switchType;
		}

		// Token: 0x060014DB RID: 5339 RVA: 0x000373E8 File Offset: 0x000355E8
		public static SwitchAttribute[] GetAll(Assembly assembly)
		{
			object[] customAttributes = assembly.GetCustomAttributes(typeof(SwitchAttribute), false);
			SwitchAttribute[] array = new SwitchAttribute[customAttributes.Length];
			for (int i = 0; i < customAttributes.Length; i++)
			{
				array[i] = (SwitchAttribute)customAttributes[i];
			}
			return array;
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x060014DC RID: 5340 RVA: 0x00037430 File Offset: 0x00035630
		// (set) Token: 0x060014DD RID: 5341 RVA: 0x00037438 File Offset: 0x00035638
		public string SwitchName
		{
			get
			{
				return this.name;
			}
			set
			{
				if (this.name == null)
				{
					throw new ArgumentNullException("value");
				}
				this.name = value;
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x060014DE RID: 5342 RVA: 0x00037458 File Offset: 0x00035658
		// (set) Token: 0x060014DF RID: 5343 RVA: 0x00037460 File Offset: 0x00035660
		public string SwitchDescription
		{
			get
			{
				return this.desc;
			}
			set
			{
				if (this.desc == null)
				{
					throw new ArgumentNullException("value");
				}
				this.desc = value;
			}
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x060014E0 RID: 5344 RVA: 0x00037480 File Offset: 0x00035680
		// (set) Token: 0x060014E1 RID: 5345 RVA: 0x00037488 File Offset: 0x00035688
		public Type SwitchType
		{
			get
			{
				return this.type;
			}
			set
			{
				if (this.type == null)
				{
					throw new ArgumentNullException("value");
				}
				this.type = value;
			}
		}

		// Token: 0x04000654 RID: 1620
		private string name;

		// Token: 0x04000655 RID: 1621
		private string desc = string.Empty;

		// Token: 0x04000656 RID: 1622
		private Type type;
	}
}
