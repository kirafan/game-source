﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200024F RID: 591
	public class SourceSwitch : Switch
	{
		// Token: 0x060014C7 RID: 5319 RVA: 0x000370D4 File Offset: 0x000352D4
		public SourceSwitch(string displayName) : this(displayName, null)
		{
		}

		// Token: 0x060014C8 RID: 5320 RVA: 0x000370E0 File Offset: 0x000352E0
		public SourceSwitch(string displayName, string defaultSwitchValue) : base(displayName, "Source switch.", defaultSwitchValue)
		{
		}

		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x060014C9 RID: 5321 RVA: 0x000370F0 File Offset: 0x000352F0
		// (set) Token: 0x060014CA RID: 5322 RVA: 0x000370F8 File Offset: 0x000352F8
		public SourceLevels Level
		{
			get
			{
				return (SourceLevels)base.SwitchSetting;
			}
			set
			{
				base.SwitchSetting = (int)value;
			}
		}

		// Token: 0x060014CB RID: 5323 RVA: 0x00037104 File Offset: 0x00035304
		public bool ShouldTrace(TraceEventType eventType)
		{
			switch (eventType)
			{
			case TraceEventType.Critical:
				return (this.Level & SourceLevels.Critical) != SourceLevels.Off;
			case TraceEventType.Error:
				return (this.Level & SourceLevels.Error) != SourceLevels.Off;
			default:
				if (eventType != TraceEventType.Verbose)
				{
					if (eventType != TraceEventType.Start && eventType != TraceEventType.Stop && eventType != TraceEventType.Suspend && eventType != TraceEventType.Resume && eventType != TraceEventType.Transfer)
					{
					}
					return (this.Level & SourceLevels.ActivityTracing) != SourceLevels.Off;
				}
				return (this.Level & SourceLevels.Verbose) != SourceLevels.Off;
			case TraceEventType.Warning:
				return (this.Level & SourceLevels.Warning) != SourceLevels.Off;
			case TraceEventType.Information:
				return (this.Level & SourceLevels.Information) != SourceLevels.Off;
			}
		}

		// Token: 0x060014CC RID: 5324 RVA: 0x000371E0 File Offset: 0x000353E0
		protected override void OnValueChanged()
		{
			base.SwitchSetting = (int)Enum.Parse(typeof(SourceLevels), base.Value, true);
		}

		// Token: 0x0400064C RID: 1612
		private const string description = "Source switch.";
	}
}
