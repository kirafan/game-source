﻿using System;
using System.IO;

namespace System.Diagnostics
{
	// Token: 0x02000254 RID: 596
	public class TextWriterTraceListener : TraceListener
	{
		// Token: 0x060014F0 RID: 5360 RVA: 0x00037640 File Offset: 0x00035840
		public TextWriterTraceListener() : base("TextWriter")
		{
		}

		// Token: 0x060014F1 RID: 5361 RVA: 0x00037650 File Offset: 0x00035850
		public TextWriterTraceListener(Stream stream) : this(stream, string.Empty)
		{
		}

		// Token: 0x060014F2 RID: 5362 RVA: 0x00037660 File Offset: 0x00035860
		public TextWriterTraceListener(string fileName) : this(fileName, string.Empty)
		{
		}

		// Token: 0x060014F3 RID: 5363 RVA: 0x00037670 File Offset: 0x00035870
		public TextWriterTraceListener(TextWriter writer) : this(writer, string.Empty)
		{
		}

		// Token: 0x060014F4 RID: 5364 RVA: 0x00037680 File Offset: 0x00035880
		public TextWriterTraceListener(Stream stream, string name) : base((name == null) ? string.Empty : name)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			this.writer = new StreamWriter(stream);
		}

		// Token: 0x060014F5 RID: 5365 RVA: 0x000376C4 File Offset: 0x000358C4
		public TextWriterTraceListener(string fileName, string name) : base((name == null) ? string.Empty : name)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			this.writer = new StreamWriter(new FileStream(fileName, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
		}

		// Token: 0x060014F6 RID: 5366 RVA: 0x00037710 File Offset: 0x00035910
		public TextWriterTraceListener(TextWriter writer, string name) : base((name == null) ? string.Empty : name)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			this.writer = writer;
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x060014F7 RID: 5367 RVA: 0x00037744 File Offset: 0x00035944
		// (set) Token: 0x060014F8 RID: 5368 RVA: 0x0003774C File Offset: 0x0003594C
		public TextWriter Writer
		{
			get
			{
				return this.writer;
			}
			set
			{
				this.writer = value;
			}
		}

		// Token: 0x060014F9 RID: 5369 RVA: 0x00037758 File Offset: 0x00035958
		public override void Close()
		{
			if (this.writer != null)
			{
				this.writer.Flush();
				this.writer.Close();
				this.writer = null;
			}
		}

		// Token: 0x060014FA RID: 5370 RVA: 0x00037790 File Offset: 0x00035990
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Close();
			}
			base.Dispose(disposing);
		}

		// Token: 0x060014FB RID: 5371 RVA: 0x000377A8 File Offset: 0x000359A8
		public override void Flush()
		{
			if (this.writer != null)
			{
				this.writer.Flush();
			}
		}

		// Token: 0x060014FC RID: 5372 RVA: 0x000377C0 File Offset: 0x000359C0
		public override void Write(string message)
		{
			if (this.writer != null)
			{
				if (base.NeedIndent)
				{
					this.WriteIndent();
				}
				this.writer.Write(message);
			}
		}

		// Token: 0x060014FD RID: 5373 RVA: 0x000377F8 File Offset: 0x000359F8
		public override void WriteLine(string message)
		{
			if (this.writer != null)
			{
				if (base.NeedIndent)
				{
					this.WriteIndent();
				}
				this.writer.WriteLine(message);
				base.NeedIndent = true;
			}
		}

		// Token: 0x0400065D RID: 1629
		private TextWriter writer;
	}
}
