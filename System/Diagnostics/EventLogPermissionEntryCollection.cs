﻿using System;
using System.Collections;
using System.Security.Permissions;

namespace System.Diagnostics
{
	// Token: 0x02000227 RID: 551
	[Serializable]
	public class EventLogPermissionEntryCollection : CollectionBase
	{
		// Token: 0x060012C6 RID: 4806 RVA: 0x0003269C File Offset: 0x0003089C
		internal EventLogPermissionEntryCollection(EventLogPermission owner)
		{
			this.owner = owner;
			System.Security.Permissions.ResourcePermissionBaseEntry[] entries = owner.GetEntries();
			if (entries.Length > 0)
			{
				foreach (System.Security.Permissions.ResourcePermissionBaseEntry resourcePermissionBaseEntry in entries)
				{
					EventLogPermissionAccess permissionAccess = (EventLogPermissionAccess)resourcePermissionBaseEntry.PermissionAccess;
					EventLogPermissionEntry value = new EventLogPermissionEntry(permissionAccess, resourcePermissionBaseEntry.PermissionAccessPath[0]);
					base.InnerList.Add(value);
				}
			}
		}

		// Token: 0x17000447 RID: 1095
		public EventLogPermissionEntry this[int index]
		{
			get
			{
				return (EventLogPermissionEntry)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060012C9 RID: 4809 RVA: 0x0003272C File Offset: 0x0003092C
		public int Add(EventLogPermissionEntry value)
		{
			return base.List.Add(value);
		}

		// Token: 0x060012CA RID: 4810 RVA: 0x0003273C File Offset: 0x0003093C
		public void AddRange(EventLogPermissionEntry[] value)
		{
			foreach (EventLogPermissionEntry value2 in value)
			{
				base.List.Add(value2);
			}
		}

		// Token: 0x060012CB RID: 4811 RVA: 0x00032770 File Offset: 0x00030970
		public void AddRange(EventLogPermissionEntryCollection value)
		{
			foreach (object obj in value)
			{
				EventLogPermissionEntry value2 = (EventLogPermissionEntry)obj;
				base.List.Add(value2);
			}
		}

		// Token: 0x060012CC RID: 4812 RVA: 0x000327E0 File Offset: 0x000309E0
		public bool Contains(EventLogPermissionEntry value)
		{
			return base.List.Contains(value);
		}

		// Token: 0x060012CD RID: 4813 RVA: 0x000327F0 File Offset: 0x000309F0
		public void CopyTo(EventLogPermissionEntry[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060012CE RID: 4814 RVA: 0x00032800 File Offset: 0x00030A00
		public int IndexOf(EventLogPermissionEntry value)
		{
			return base.List.IndexOf(value);
		}

		// Token: 0x060012CF RID: 4815 RVA: 0x00032810 File Offset: 0x00030A10
		public void Insert(int index, EventLogPermissionEntry value)
		{
			base.List.Insert(index, value);
		}

		// Token: 0x060012D0 RID: 4816 RVA: 0x00032820 File Offset: 0x00030A20
		protected override void OnClear()
		{
			this.owner.ClearEntries();
		}

		// Token: 0x060012D1 RID: 4817 RVA: 0x00032830 File Offset: 0x00030A30
		protected override void OnInsert(int index, object value)
		{
			this.owner.Add(value);
		}

		// Token: 0x060012D2 RID: 4818 RVA: 0x00032840 File Offset: 0x00030A40
		protected override void OnRemove(int index, object value)
		{
			this.owner.Remove(value);
		}

		// Token: 0x060012D3 RID: 4819 RVA: 0x00032850 File Offset: 0x00030A50
		protected override void OnSet(int index, object oldValue, object newValue)
		{
			this.owner.Remove(oldValue);
			this.owner.Add(newValue);
		}

		// Token: 0x060012D4 RID: 4820 RVA: 0x0003286C File Offset: 0x00030A6C
		public void Remove(EventLogPermissionEntry value)
		{
			base.List.Remove(value);
		}

		// Token: 0x04000561 RID: 1377
		private EventLogPermission owner;
	}
}
