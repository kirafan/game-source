﻿using System;

namespace System.Diagnostics
{
	// Token: 0x0200024C RID: 588
	public enum ProcessWindowStyle
	{
		// Token: 0x0400063E RID: 1598
		Hidden = 1,
		// Token: 0x0400063F RID: 1599
		Maximized = 3,
		// Token: 0x04000640 RID: 1600
		Minimized = 2,
		// Token: 0x04000641 RID: 1601
		Normal = 0
	}
}
