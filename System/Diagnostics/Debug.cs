﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000215 RID: 533
	public sealed class Debug
	{
		// Token: 0x060011CA RID: 4554 RVA: 0x0002F4D0 File Offset: 0x0002D6D0
		private Debug()
		{
		}

		// Token: 0x17000411 RID: 1041
		// (get) Token: 0x060011CB RID: 4555 RVA: 0x0002F4D8 File Offset: 0x0002D6D8
		// (set) Token: 0x060011CC RID: 4556 RVA: 0x0002F4E0 File Offset: 0x0002D6E0
		public static bool AutoFlush
		{
			get
			{
				return TraceImpl.AutoFlush;
			}
			set
			{
				TraceImpl.AutoFlush = value;
			}
		}

		// Token: 0x17000412 RID: 1042
		// (get) Token: 0x060011CD RID: 4557 RVA: 0x0002F4E8 File Offset: 0x0002D6E8
		// (set) Token: 0x060011CE RID: 4558 RVA: 0x0002F4F0 File Offset: 0x0002D6F0
		public static int IndentLevel
		{
			get
			{
				return TraceImpl.IndentLevel;
			}
			set
			{
				TraceImpl.IndentLevel = value;
			}
		}

		// Token: 0x17000413 RID: 1043
		// (get) Token: 0x060011CF RID: 4559 RVA: 0x0002F4F8 File Offset: 0x0002D6F8
		// (set) Token: 0x060011D0 RID: 4560 RVA: 0x0002F500 File Offset: 0x0002D700
		public static int IndentSize
		{
			get
			{
				return TraceImpl.IndentSize;
			}
			set
			{
				TraceImpl.IndentSize = value;
			}
		}

		// Token: 0x17000414 RID: 1044
		// (get) Token: 0x060011D1 RID: 4561 RVA: 0x0002F508 File Offset: 0x0002D708
		public static TraceListenerCollection Listeners
		{
			get
			{
				return TraceImpl.Listeners;
			}
		}

		// Token: 0x060011D2 RID: 4562 RVA: 0x0002F510 File Offset: 0x0002D710
		[Conditional("DEBUG")]
		public static void Assert(bool condition)
		{
			TraceImpl.Assert(condition);
		}

		// Token: 0x060011D3 RID: 4563 RVA: 0x0002F518 File Offset: 0x0002D718
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message)
		{
			TraceImpl.Assert(condition, message);
		}

		// Token: 0x060011D4 RID: 4564 RVA: 0x0002F524 File Offset: 0x0002D724
		[Conditional("DEBUG")]
		public static void Assert(bool condition, string message, string detailMessage)
		{
			TraceImpl.Assert(condition, message, detailMessage);
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x0002F530 File Offset: 0x0002D730
		[Conditional("DEBUG")]
		public static void Close()
		{
			TraceImpl.Close();
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x0002F538 File Offset: 0x0002D738
		[Conditional("DEBUG")]
		public static void Fail(string message)
		{
			TraceImpl.Fail(message);
		}

		// Token: 0x060011D7 RID: 4567 RVA: 0x0002F540 File Offset: 0x0002D740
		[Conditional("DEBUG")]
		public static void Fail(string message, string detailMessage)
		{
			TraceImpl.Fail(message, detailMessage);
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x0002F54C File Offset: 0x0002D74C
		[Conditional("DEBUG")]
		public static void Flush()
		{
			TraceImpl.Flush();
		}

		// Token: 0x060011D9 RID: 4569 RVA: 0x0002F554 File Offset: 0x0002D754
		[Conditional("DEBUG")]
		public static void Indent()
		{
			TraceImpl.Indent();
		}

		// Token: 0x060011DA RID: 4570 RVA: 0x0002F55C File Offset: 0x0002D75C
		[Conditional("DEBUG")]
		public static void Unindent()
		{
			TraceImpl.Unindent();
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x0002F564 File Offset: 0x0002D764
		[Conditional("DEBUG")]
		public static void Write(object value)
		{
			TraceImpl.Write(value);
		}

		// Token: 0x060011DC RID: 4572 RVA: 0x0002F56C File Offset: 0x0002D76C
		[Conditional("DEBUG")]
		public static void Write(string message)
		{
			TraceImpl.Write(message);
		}

		// Token: 0x060011DD RID: 4573 RVA: 0x0002F574 File Offset: 0x0002D774
		[Conditional("DEBUG")]
		public static void Write(object value, string category)
		{
			TraceImpl.Write(value, category);
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x0002F580 File Offset: 0x0002D780
		[Conditional("DEBUG")]
		public static void Write(string message, string category)
		{
			TraceImpl.Write(message, category);
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x0002F58C File Offset: 0x0002D78C
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, object value)
		{
			TraceImpl.WriteIf(condition, value);
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x0002F598 File Offset: 0x0002D798
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, string message)
		{
			TraceImpl.WriteIf(condition, message);
		}

		// Token: 0x060011E1 RID: 4577 RVA: 0x0002F5A4 File Offset: 0x0002D7A4
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, object value, string category)
		{
			TraceImpl.WriteIf(condition, value, category);
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x0002F5B0 File Offset: 0x0002D7B0
		[Conditional("DEBUG")]
		public static void WriteIf(bool condition, string message, string category)
		{
			TraceImpl.WriteIf(condition, message, category);
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x0002F5BC File Offset: 0x0002D7BC
		[Conditional("DEBUG")]
		public static void WriteLine(object value)
		{
			TraceImpl.WriteLine(value);
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x0002F5C4 File Offset: 0x0002D7C4
		[Conditional("DEBUG")]
		public static void WriteLine(string message)
		{
			TraceImpl.WriteLine(message);
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x0002F5CC File Offset: 0x0002D7CC
		[Conditional("DEBUG")]
		public static void WriteLine(object value, string category)
		{
			TraceImpl.WriteLine(value, category);
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x0002F5D8 File Offset: 0x0002D7D8
		[Conditional("DEBUG")]
		public static void WriteLine(string message, string category)
		{
			TraceImpl.WriteLine(message, category);
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x0002F5E4 File Offset: 0x0002D7E4
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, object value)
		{
			TraceImpl.WriteLineIf(condition, value);
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x0002F5F0 File Offset: 0x0002D7F0
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, string message)
		{
			TraceImpl.WriteLineIf(condition, message);
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x0002F5FC File Offset: 0x0002D7FC
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, object value, string category)
		{
			TraceImpl.WriteLineIf(condition, value, category);
		}

		// Token: 0x060011EA RID: 4586 RVA: 0x0002F608 File Offset: 0x0002D808
		[Conditional("DEBUG")]
		public static void WriteLineIf(bool condition, string message, string category)
		{
			TraceImpl.WriteLineIf(condition, message, category);
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0002F614 File Offset: 0x0002D814
		[Conditional("DEBUG")]
		public static void Print(string message)
		{
			TraceImpl.WriteLine(message);
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x0002F61C File Offset: 0x0002D81C
		[Conditional("DEBUG")]
		public static void Print(string format, params object[] args)
		{
			TraceImpl.WriteLine(string.Format(format, args));
		}
	}
}
