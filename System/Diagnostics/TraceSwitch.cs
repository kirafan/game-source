﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000264 RID: 612
	[SwitchLevel(typeof(TraceLevel))]
	public class TraceSwitch : Switch
	{
		// Token: 0x060015BA RID: 5562 RVA: 0x00039718 File Offset: 0x00037918
		public TraceSwitch(string displayName, string description) : base(displayName, description)
		{
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x00039724 File Offset: 0x00037924
		public TraceSwitch(string displayName, string description, string defaultSwitchValue) : base(displayName, description)
		{
			base.Value = defaultSwitchValue;
		}

		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x060015BC RID: 5564 RVA: 0x00039738 File Offset: 0x00037938
		// (set) Token: 0x060015BD RID: 5565 RVA: 0x00039740 File Offset: 0x00037940
		public TraceLevel Level
		{
			get
			{
				return (TraceLevel)base.SwitchSetting;
			}
			set
			{
				if (!Enum.IsDefined(typeof(TraceLevel), value))
				{
					throw new ArgumentException("value");
				}
				base.SwitchSetting = (int)value;
			}
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x060015BE RID: 5566 RVA: 0x0003977C File Offset: 0x0003797C
		public bool TraceError
		{
			get
			{
				return base.SwitchSetting >= 1;
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x060015BF RID: 5567 RVA: 0x0003978C File Offset: 0x0003798C
		public bool TraceWarning
		{
			get
			{
				return base.SwitchSetting >= 2;
			}
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x060015C0 RID: 5568 RVA: 0x0003979C File Offset: 0x0003799C
		public bool TraceInfo
		{
			get
			{
				return base.SwitchSetting >= 3;
			}
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x060015C1 RID: 5569 RVA: 0x000397AC File Offset: 0x000379AC
		public bool TraceVerbose
		{
			get
			{
				return base.SwitchSetting >= 4;
			}
		}

		// Token: 0x060015C2 RID: 5570 RVA: 0x000397BC File Offset: 0x000379BC
		protected override void OnSwitchSettingChanged()
		{
			if (base.SwitchSetting < 0)
			{
				base.SwitchSetting = 0;
			}
			else if (base.SwitchSetting > 4)
			{
				base.SwitchSetting = 4;
			}
		}

		// Token: 0x060015C3 RID: 5571 RVA: 0x000397F4 File Offset: 0x000379F4
		protected override void OnValueChanged()
		{
			base.SwitchSetting = (int)Enum.Parse(typeof(TraceLevel), base.Value, true);
		}
	}
}
