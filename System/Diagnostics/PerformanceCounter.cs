﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace System.Diagnostics
{
	// Token: 0x02000237 RID: 567
	[System.ComponentModel.InstallerType(typeof(PerformanceCounterInstaller))]
	public sealed class PerformanceCounter : System.ComponentModel.Component, System.ComponentModel.ISupportInitialize
	{
		// Token: 0x06001394 RID: 5012 RVA: 0x000342EC File Offset: 0x000324EC
		public PerformanceCounter()
		{
			this.categoryName = (this.counterName = (this.instanceName = string.Empty));
			this.machineName = ".";
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x00034328 File Offset: 0x00032528
		public PerformanceCounter(string categoryName, string counterName) : this(categoryName, counterName, false)
		{
		}

		// Token: 0x06001396 RID: 5014 RVA: 0x00034334 File Offset: 0x00032534
		public PerformanceCounter(string categoryName, string counterName, bool readOnly) : this(categoryName, counterName, string.Empty, readOnly)
		{
		}

		// Token: 0x06001397 RID: 5015 RVA: 0x00034344 File Offset: 0x00032544
		public PerformanceCounter(string categoryName, string counterName, string instanceName) : this(categoryName, counterName, instanceName, false)
		{
		}

		// Token: 0x06001398 RID: 5016 RVA: 0x00034350 File Offset: 0x00032550
		public PerformanceCounter(string categoryName, string counterName, string instanceName, bool readOnly)
		{
			if (categoryName == null)
			{
				throw new ArgumentNullException("categoryName");
			}
			if (counterName == null)
			{
				throw new ArgumentNullException("counterName");
			}
			if (instanceName == null)
			{
				throw new ArgumentNullException("instanceName");
			}
			this.CategoryName = categoryName;
			this.CounterName = counterName;
			if (categoryName == string.Empty || counterName == string.Empty)
			{
				throw new InvalidOperationException();
			}
			this.InstanceName = instanceName;
			this.instanceName = instanceName;
			this.machineName = ".";
			this.readOnly = readOnly;
			this.changed = true;
		}

		// Token: 0x06001399 RID: 5017 RVA: 0x000343F4 File Offset: 0x000325F4
		public PerformanceCounter(string categoryName, string counterName, string instanceName, string machineName) : this(categoryName, counterName, instanceName, false)
		{
			this.machineName = machineName;
		}

		// Token: 0x0600139B RID: 5019
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetImpl(string category, string counter, string instance, string machine, out PerformanceCounterType ctype, out bool custom);

		// Token: 0x0600139C RID: 5020
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetSample(IntPtr impl, bool only_value, out CounterSample sample);

		// Token: 0x0600139D RID: 5021
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern long UpdateValue(IntPtr impl, bool do_incr, long value);

		// Token: 0x0600139E RID: 5022
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FreeData(IntPtr impl);

		// Token: 0x0600139F RID: 5023 RVA: 0x00034414 File Offset: 0x00032614
		private void UpdateInfo()
		{
			if (this.impl != IntPtr.Zero)
			{
				this.Close();
			}
			this.impl = PerformanceCounter.GetImpl(this.categoryName, this.counterName, this.instanceName, this.machineName, out this.type, out this.is_custom);
			if (!this.is_custom)
			{
				this.readOnly = true;
			}
			this.changed = false;
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x060013A0 RID: 5024 RVA: 0x00034484 File Offset: 0x00032684
		// (set) Token: 0x060013A1 RID: 5025 RVA: 0x0003448C File Offset: 0x0003268C
		[System.ComponentModel.TypeConverter("System.Diagnostics.Design.CategoryValueConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[SRDescription("The category name for this performance counter.")]
		[System.ComponentModel.DefaultValue("")]
		[System.ComponentModel.ReadOnly(true)]
		[System.ComponentModel.RecommendedAsConfigurable(true)]
		public string CategoryName
		{
			get
			{
				return this.categoryName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("categoryName");
				}
				this.categoryName = value;
				this.changed = true;
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x060013A2 RID: 5026 RVA: 0x000344B0 File Offset: 0x000326B0
		[System.ComponentModel.ReadOnly(true)]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		[MonitoringDescription("A description describing the counter.")]
		[MonoTODO]
		public string CounterHelp
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000488 RID: 1160
		// (get) Token: 0x060013A3 RID: 5027 RVA: 0x000344B8 File Offset: 0x000326B8
		// (set) Token: 0x060013A4 RID: 5028 RVA: 0x000344C0 File Offset: 0x000326C0
		[SRDescription("The name of this performance counter.")]
		[System.ComponentModel.RecommendedAsConfigurable(true)]
		[System.ComponentModel.TypeConverter("System.Diagnostics.Design.CounterNameConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[System.ComponentModel.ReadOnly(true)]
		[System.ComponentModel.DefaultValue("")]
		public string CounterName
		{
			get
			{
				return this.counterName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("counterName");
				}
				this.counterName = value;
				this.changed = true;
			}
		}

		// Token: 0x17000489 RID: 1161
		// (get) Token: 0x060013A5 RID: 5029 RVA: 0x000344E4 File Offset: 0x000326E4
		[MonitoringDescription("The type of the counter.")]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public PerformanceCounterType CounterType
		{
			get
			{
				if (this.changed)
				{
					this.UpdateInfo();
				}
				return this.type;
			}
		}

		// Token: 0x1700048A RID: 1162
		// (get) Token: 0x060013A6 RID: 5030 RVA: 0x00034500 File Offset: 0x00032700
		// (set) Token: 0x060013A7 RID: 5031 RVA: 0x00034508 File Offset: 0x00032708
		[System.ComponentModel.DefaultValue(PerformanceCounterInstanceLifetime.Global)]
		[MonoTODO]
		public PerformanceCounterInstanceLifetime InstanceLifetime
		{
			get
			{
				return this.lifetime;
			}
			set
			{
				this.lifetime = value;
			}
		}

		// Token: 0x1700048B RID: 1163
		// (get) Token: 0x060013A8 RID: 5032 RVA: 0x00034514 File Offset: 0x00032714
		// (set) Token: 0x060013A9 RID: 5033 RVA: 0x0003451C File Offset: 0x0003271C
		[System.ComponentModel.TypeConverter("System.Diagnostics.Design.InstanceNameConverter, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
		[SRDescription("The instance name for this performance counter.")]
		[System.ComponentModel.ReadOnly(true)]
		[System.ComponentModel.DefaultValue("")]
		[System.ComponentModel.RecommendedAsConfigurable(true)]
		public string InstanceName
		{
			get
			{
				return this.instanceName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.instanceName = value;
				this.changed = true;
			}
		}

		// Token: 0x1700048C RID: 1164
		// (get) Token: 0x060013AA RID: 5034 RVA: 0x00034540 File Offset: 0x00032740
		// (set) Token: 0x060013AB RID: 5035 RVA: 0x00034548 File Offset: 0x00032748
		[MonoTODO("What's the machine name format?")]
		[System.ComponentModel.DefaultValue(".")]
		[System.ComponentModel.Browsable(false)]
		[System.ComponentModel.RecommendedAsConfigurable(true)]
		[SRDescription("The machine where this performance counter resides.")]
		public string MachineName
		{
			get
			{
				return this.machineName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (value == string.Empty || value == ".")
				{
					this.machineName = ".";
					this.changed = true;
					return;
				}
				throw new PlatformNotSupportedException();
			}
		}

		// Token: 0x1700048D RID: 1165
		// (get) Token: 0x060013AC RID: 5036 RVA: 0x000345A0 File Offset: 0x000327A0
		// (set) Token: 0x060013AD RID: 5037 RVA: 0x000345D4 File Offset: 0x000327D4
		[MonitoringDescription("The raw value of the counter.")]
		[System.ComponentModel.Browsable(false)]
		[System.ComponentModel.DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden)]
		public long RawValue
		{
			get
			{
				if (this.changed)
				{
					this.UpdateInfo();
				}
				CounterSample counterSample;
				PerformanceCounter.GetSample(this.impl, true, out counterSample);
				return counterSample.RawValue;
			}
			set
			{
				if (this.changed)
				{
					this.UpdateInfo();
				}
				if (this.readOnly)
				{
					throw new InvalidOperationException();
				}
				PerformanceCounter.UpdateValue(this.impl, false, value);
			}
		}

		// Token: 0x1700048E RID: 1166
		// (get) Token: 0x060013AE RID: 5038 RVA: 0x00034614 File Offset: 0x00032814
		// (set) Token: 0x060013AF RID: 5039 RVA: 0x0003461C File Offset: 0x0003281C
		[MonitoringDescription("The accessability level of the counter.")]
		[System.ComponentModel.Browsable(false)]
		[System.ComponentModel.DefaultValue(true)]
		public bool ReadOnly
		{
			get
			{
				return this.readOnly;
			}
			set
			{
				this.readOnly = value;
			}
		}

		// Token: 0x060013B0 RID: 5040 RVA: 0x00034628 File Offset: 0x00032828
		public void BeginInit()
		{
		}

		// Token: 0x060013B1 RID: 5041 RVA: 0x0003462C File Offset: 0x0003282C
		public void EndInit()
		{
		}

		// Token: 0x060013B2 RID: 5042 RVA: 0x00034630 File Offset: 0x00032830
		public void Close()
		{
			IntPtr value = this.impl;
			this.impl = IntPtr.Zero;
			if (value != IntPtr.Zero)
			{
				PerformanceCounter.FreeData(value);
			}
		}

		// Token: 0x060013B3 RID: 5043 RVA: 0x00034668 File Offset: 0x00032868
		public static void CloseSharedResources()
		{
		}

		// Token: 0x060013B4 RID: 5044 RVA: 0x0003466C File Offset: 0x0003286C
		public long Decrement()
		{
			return this.IncrementBy(-1L);
		}

		// Token: 0x060013B5 RID: 5045 RVA: 0x00034678 File Offset: 0x00032878
		protected override void Dispose(bool disposing)
		{
			this.Close();
		}

		// Token: 0x060013B6 RID: 5046 RVA: 0x00034680 File Offset: 0x00032880
		public long Increment()
		{
			return this.IncrementBy(1L);
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x0003468C File Offset: 0x0003288C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public long IncrementBy(long value)
		{
			if (this.changed)
			{
				this.UpdateInfo();
			}
			if (this.readOnly)
			{
				return 0L;
			}
			return PerformanceCounter.UpdateValue(this.impl, true, value);
		}

		// Token: 0x060013B8 RID: 5048 RVA: 0x000346C8 File Offset: 0x000328C8
		public CounterSample NextSample()
		{
			if (this.changed)
			{
				this.UpdateInfo();
			}
			CounterSample result;
			PerformanceCounter.GetSample(this.impl, false, out result);
			this.valid_old = true;
			this.old_sample = result;
			return result;
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x00034704 File Offset: 0x00032904
		public float NextValue()
		{
			if (this.changed)
			{
				this.UpdateInfo();
			}
			CounterSample newSample;
			PerformanceCounter.GetSample(this.impl, false, out newSample);
			float result;
			if (this.valid_old)
			{
				result = CounterSampleCalculator.ComputeCounterValue(this.old_sample, newSample);
			}
			else
			{
				result = CounterSampleCalculator.ComputeCounterValue(newSample);
			}
			this.valid_old = true;
			this.old_sample = newSample;
			return result;
		}

		// Token: 0x060013BA RID: 5050 RVA: 0x00034764 File Offset: 0x00032964
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[MonoTODO]
		public void RemoveInstance()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400059C RID: 1436
		private string categoryName;

		// Token: 0x0400059D RID: 1437
		private string counterName;

		// Token: 0x0400059E RID: 1438
		private string instanceName;

		// Token: 0x0400059F RID: 1439
		private string machineName;

		// Token: 0x040005A0 RID: 1440
		private IntPtr impl;

		// Token: 0x040005A1 RID: 1441
		private PerformanceCounterType type;

		// Token: 0x040005A2 RID: 1442
		private CounterSample old_sample;

		// Token: 0x040005A3 RID: 1443
		private bool readOnly;

		// Token: 0x040005A4 RID: 1444
		private bool valid_old;

		// Token: 0x040005A5 RID: 1445
		private bool changed;

		// Token: 0x040005A6 RID: 1446
		private bool is_custom;

		// Token: 0x040005A7 RID: 1447
		private PerformanceCounterInstanceLifetime lifetime;

		// Token: 0x040005A8 RID: 1448
		[Obsolete]
		public static int DefaultFileMappingSize = 524288;
	}
}
