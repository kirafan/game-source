﻿using System;
using System.ComponentModel;

namespace System.Diagnostics
{
	// Token: 0x02000232 RID: 562
	[AttributeUsage(AttributeTargets.All)]
	public class MonitoringDescriptionAttribute : System.ComponentModel.DescriptionAttribute
	{
		// Token: 0x06001353 RID: 4947 RVA: 0x00033E10 File Offset: 0x00032010
		public MonitoringDescriptionAttribute(string description) : base(description)
		{
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06001354 RID: 4948 RVA: 0x00033E1C File Offset: 0x0003201C
		public override string Description
		{
			get
			{
				return base.Description;
			}
		}
	}
}
