﻿using System;
using System.Reflection;

namespace System.Diagnostics
{
	// Token: 0x02000258 RID: 600
	public sealed class Trace
	{
		// Token: 0x060014FE RID: 5374 RVA: 0x00037834 File Offset: 0x00035A34
		private Trace()
		{
		}

		// Token: 0x060014FF RID: 5375 RVA: 0x0003783C File Offset: 0x00035A3C
		[MonoNotSupported("")]
		public static void Refresh()
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001500 RID: 5376 RVA: 0x00037844 File Offset: 0x00035A44
		// (set) Token: 0x06001501 RID: 5377 RVA: 0x0003784C File Offset: 0x00035A4C
		public static bool AutoFlush
		{
			get
			{
				return TraceImpl.AutoFlush;
			}
			set
			{
				TraceImpl.AutoFlush = value;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001502 RID: 5378 RVA: 0x00037854 File Offset: 0x00035A54
		// (set) Token: 0x06001503 RID: 5379 RVA: 0x0003785C File Offset: 0x00035A5C
		public static int IndentLevel
		{
			get
			{
				return TraceImpl.IndentLevel;
			}
			set
			{
				TraceImpl.IndentLevel = value;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001504 RID: 5380 RVA: 0x00037864 File Offset: 0x00035A64
		// (set) Token: 0x06001505 RID: 5381 RVA: 0x0003786C File Offset: 0x00035A6C
		public static int IndentSize
		{
			get
			{
				return TraceImpl.IndentSize;
			}
			set
			{
				TraceImpl.IndentSize = value;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001506 RID: 5382 RVA: 0x00037874 File Offset: 0x00035A74
		public static TraceListenerCollection Listeners
		{
			get
			{
				return TraceImpl.Listeners;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06001507 RID: 5383 RVA: 0x0003787C File Offset: 0x00035A7C
		public static CorrelationManager CorrelationManager
		{
			get
			{
				return TraceImpl.CorrelationManager;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x06001508 RID: 5384 RVA: 0x00037884 File Offset: 0x00035A84
		// (set) Token: 0x06001509 RID: 5385 RVA: 0x0003788C File Offset: 0x00035A8C
		public static bool UseGlobalLock
		{
			get
			{
				return TraceImpl.UseGlobalLock;
			}
			set
			{
				TraceImpl.UseGlobalLock = value;
			}
		}

		// Token: 0x0600150A RID: 5386 RVA: 0x00037894 File Offset: 0x00035A94
		[Conditional("TRACE")]
		public static void Assert(bool condition)
		{
			TraceImpl.Assert(condition);
		}

		// Token: 0x0600150B RID: 5387 RVA: 0x0003789C File Offset: 0x00035A9C
		[Conditional("TRACE")]
		public static void Assert(bool condition, string message)
		{
			TraceImpl.Assert(condition, message);
		}

		// Token: 0x0600150C RID: 5388 RVA: 0x000378A8 File Offset: 0x00035AA8
		[Conditional("TRACE")]
		public static void Assert(bool condition, string message, string detailMessage)
		{
			TraceImpl.Assert(condition, message, detailMessage);
		}

		// Token: 0x0600150D RID: 5389 RVA: 0x000378B4 File Offset: 0x00035AB4
		[Conditional("TRACE")]
		public static void Close()
		{
			TraceImpl.Close();
		}

		// Token: 0x0600150E RID: 5390 RVA: 0x000378BC File Offset: 0x00035ABC
		[Conditional("TRACE")]
		public static void Fail(string message)
		{
			TraceImpl.Fail(message);
		}

		// Token: 0x0600150F RID: 5391 RVA: 0x000378C4 File Offset: 0x00035AC4
		[Conditional("TRACE")]
		public static void Fail(string message, string detailMessage)
		{
			TraceImpl.Fail(message, detailMessage);
		}

		// Token: 0x06001510 RID: 5392 RVA: 0x000378D0 File Offset: 0x00035AD0
		[Conditional("TRACE")]
		public static void Flush()
		{
			TraceImpl.Flush();
		}

		// Token: 0x06001511 RID: 5393 RVA: 0x000378D8 File Offset: 0x00035AD8
		[Conditional("TRACE")]
		public static void Indent()
		{
			TraceImpl.Indent();
		}

		// Token: 0x06001512 RID: 5394 RVA: 0x000378E0 File Offset: 0x00035AE0
		[Conditional("TRACE")]
		public static void Unindent()
		{
			TraceImpl.Unindent();
		}

		// Token: 0x06001513 RID: 5395 RVA: 0x000378E8 File Offset: 0x00035AE8
		[Conditional("TRACE")]
		public static void Write(object value)
		{
			TraceImpl.Write(value);
		}

		// Token: 0x06001514 RID: 5396 RVA: 0x000378F0 File Offset: 0x00035AF0
		[Conditional("TRACE")]
		public static void Write(string message)
		{
			TraceImpl.Write(message);
		}

		// Token: 0x06001515 RID: 5397 RVA: 0x000378F8 File Offset: 0x00035AF8
		[Conditional("TRACE")]
		public static void Write(object value, string category)
		{
			TraceImpl.Write(value, category);
		}

		// Token: 0x06001516 RID: 5398 RVA: 0x00037904 File Offset: 0x00035B04
		[Conditional("TRACE")]
		public static void Write(string message, string category)
		{
			TraceImpl.Write(message, category);
		}

		// Token: 0x06001517 RID: 5399 RVA: 0x00037910 File Offset: 0x00035B10
		[Conditional("TRACE")]
		public static void WriteIf(bool condition, object value)
		{
			TraceImpl.WriteIf(condition, value);
		}

		// Token: 0x06001518 RID: 5400 RVA: 0x0003791C File Offset: 0x00035B1C
		[Conditional("TRACE")]
		public static void WriteIf(bool condition, string message)
		{
			TraceImpl.WriteIf(condition, message);
		}

		// Token: 0x06001519 RID: 5401 RVA: 0x00037928 File Offset: 0x00035B28
		[Conditional("TRACE")]
		public static void WriteIf(bool condition, object value, string category)
		{
			TraceImpl.WriteIf(condition, value, category);
		}

		// Token: 0x0600151A RID: 5402 RVA: 0x00037934 File Offset: 0x00035B34
		[Conditional("TRACE")]
		public static void WriteIf(bool condition, string message, string category)
		{
			TraceImpl.WriteIf(condition, message, category);
		}

		// Token: 0x0600151B RID: 5403 RVA: 0x00037940 File Offset: 0x00035B40
		[Conditional("TRACE")]
		public static void WriteLine(object value)
		{
			TraceImpl.WriteLine(value);
		}

		// Token: 0x0600151C RID: 5404 RVA: 0x00037948 File Offset: 0x00035B48
		[Conditional("TRACE")]
		public static void WriteLine(string message)
		{
			TraceImpl.WriteLine(message);
		}

		// Token: 0x0600151D RID: 5405 RVA: 0x00037950 File Offset: 0x00035B50
		[Conditional("TRACE")]
		public static void WriteLine(object value, string category)
		{
			TraceImpl.WriteLine(value, category);
		}

		// Token: 0x0600151E RID: 5406 RVA: 0x0003795C File Offset: 0x00035B5C
		[Conditional("TRACE")]
		public static void WriteLine(string message, string category)
		{
			TraceImpl.WriteLine(message, category);
		}

		// Token: 0x0600151F RID: 5407 RVA: 0x00037968 File Offset: 0x00035B68
		[Conditional("TRACE")]
		public static void WriteLineIf(bool condition, object value)
		{
			TraceImpl.WriteLineIf(condition, value);
		}

		// Token: 0x06001520 RID: 5408 RVA: 0x00037974 File Offset: 0x00035B74
		[Conditional("TRACE")]
		public static void WriteLineIf(bool condition, string message)
		{
			TraceImpl.WriteLineIf(condition, message);
		}

		// Token: 0x06001521 RID: 5409 RVA: 0x00037980 File Offset: 0x00035B80
		[Conditional("TRACE")]
		public static void WriteLineIf(bool condition, object value, string category)
		{
			TraceImpl.WriteLineIf(condition, value, category);
		}

		// Token: 0x06001522 RID: 5410 RVA: 0x0003798C File Offset: 0x00035B8C
		[Conditional("TRACE")]
		public static void WriteLineIf(bool condition, string message, string category)
		{
			TraceImpl.WriteLineIf(condition, message, category);
		}

		// Token: 0x06001523 RID: 5411 RVA: 0x00037998 File Offset: 0x00035B98
		private static void DoTrace(string kind, Assembly report, string message)
		{
			string arg = string.Empty;
			try
			{
				arg = report.Location;
			}
			catch (MethodAccessException)
			{
			}
			TraceImpl.WriteLine(string.Format("{0} {1} : 0 : {2}", arg, kind, message));
		}

		// Token: 0x06001524 RID: 5412 RVA: 0x000379EC File Offset: 0x00035BEC
		[Conditional("TRACE")]
		public static void TraceError(string message)
		{
			Trace.DoTrace("Error", Assembly.GetCallingAssembly(), message);
		}

		// Token: 0x06001525 RID: 5413 RVA: 0x00037A00 File Offset: 0x00035C00
		[Conditional("TRACE")]
		public static void TraceError(string message, params object[] args)
		{
			Trace.DoTrace("Error", Assembly.GetCallingAssembly(), string.Format(message, args));
		}

		// Token: 0x06001526 RID: 5414 RVA: 0x00037A18 File Offset: 0x00035C18
		[Conditional("TRACE")]
		public static void TraceInformation(string message)
		{
			Trace.DoTrace("Information", Assembly.GetCallingAssembly(), message);
		}

		// Token: 0x06001527 RID: 5415 RVA: 0x00037A2C File Offset: 0x00035C2C
		[Conditional("TRACE")]
		public static void TraceInformation(string message, params object[] args)
		{
			Trace.DoTrace("Information", Assembly.GetCallingAssembly(), string.Format(message, args));
		}

		// Token: 0x06001528 RID: 5416 RVA: 0x00037A44 File Offset: 0x00035C44
		[Conditional("TRACE")]
		public static void TraceWarning(string message)
		{
			Trace.DoTrace("Warning", Assembly.GetCallingAssembly(), message);
		}

		// Token: 0x06001529 RID: 5417 RVA: 0x00037A58 File Offset: 0x00035C58
		[Conditional("TRACE")]
		public static void TraceWarning(string message, params object[] args)
		{
			Trace.DoTrace("Warning", Assembly.GetCallingAssembly(), string.Format(message, args));
		}
	}
}
