﻿using System;

namespace System.Diagnostics
{
	// Token: 0x02000236 RID: 566
	public enum PerformanceCounterCategoryType
	{
		// Token: 0x04000599 RID: 1433
		SingleInstance,
		// Token: 0x0400059A RID: 1434
		MultiInstance,
		// Token: 0x0400059B RID: 1435
		Unknown = -1
	}
}
