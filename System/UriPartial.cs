﻿using System;

namespace System
{
	// Token: 0x020004BA RID: 1210
	public enum UriPartial
	{
		// Token: 0x04001B83 RID: 7043
		Scheme,
		// Token: 0x04001B84 RID: 7044
		Authority,
		// Token: 0x04001B85 RID: 7045
		Path,
		// Token: 0x04001B86 RID: 7046
		Query
	}
}
