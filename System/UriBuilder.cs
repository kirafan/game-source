﻿using System;
using System.Text;

namespace System
{
	// Token: 0x020004B0 RID: 1200
	public class UriBuilder
	{
		// Token: 0x06002B10 RID: 11024 RVA: 0x00093EC0 File Offset: 0x000920C0
		public UriBuilder() : this(System.Uri.UriSchemeHttp, "localhost")
		{
		}

		// Token: 0x06002B11 RID: 11025 RVA: 0x00093ED4 File Offset: 0x000920D4
		public UriBuilder(string uri) : this(new System.Uri(uri))
		{
		}

		// Token: 0x06002B12 RID: 11026 RVA: 0x00093EE4 File Offset: 0x000920E4
		public UriBuilder(System.Uri uri)
		{
			this.scheme = uri.Scheme;
			this.host = uri.Host;
			this.port = uri.Port;
			this.path = uri.AbsolutePath;
			this.query = uri.Query;
			this.fragment = uri.Fragment;
			this.username = uri.UserInfo;
			int num = this.username.IndexOf(':');
			if (num != -1)
			{
				this.password = this.username.Substring(num + 1);
				this.username = this.username.Substring(0, num);
			}
			else
			{
				this.password = string.Empty;
			}
			this.modified = true;
		}

		// Token: 0x06002B13 RID: 11027 RVA: 0x00093FA0 File Offset: 0x000921A0
		public UriBuilder(string schemeName, string hostName)
		{
			this.Scheme = schemeName;
			this.Host = hostName;
			this.port = -1;
			this.Path = string.Empty;
			this.query = string.Empty;
			this.fragment = string.Empty;
			this.username = string.Empty;
			this.password = string.Empty;
			this.modified = true;
		}

		// Token: 0x06002B14 RID: 11028 RVA: 0x00094008 File Offset: 0x00092208
		public UriBuilder(string scheme, string host, int portNumber) : this(scheme, host)
		{
			this.Port = portNumber;
		}

		// Token: 0x06002B15 RID: 11029 RVA: 0x0009401C File Offset: 0x0009221C
		public UriBuilder(string scheme, string host, int port, string pathValue) : this(scheme, host, port)
		{
			this.Path = pathValue;
		}

		// Token: 0x06002B16 RID: 11030 RVA: 0x00094030 File Offset: 0x00092230
		public UriBuilder(string scheme, string host, int port, string pathValue, string extraValue) : this(scheme, host, port, pathValue)
		{
			if (extraValue == null || extraValue.Length == 0)
			{
				return;
			}
			if (extraValue[0] == '#')
			{
				this.Fragment = extraValue.Remove(0, 1);
			}
			else
			{
				if (extraValue[0] != '?')
				{
					throw new ArgumentException("extraValue");
				}
				this.Query = extraValue.Remove(0, 1);
			}
		}

		// Token: 0x17000BD1 RID: 3025
		// (get) Token: 0x06002B17 RID: 11031 RVA: 0x000940B0 File Offset: 0x000922B0
		// (set) Token: 0x06002B18 RID: 11032 RVA: 0x000940B8 File Offset: 0x000922B8
		public string Fragment
		{
			get
			{
				return this.fragment;
			}
			set
			{
				this.fragment = value;
				if (this.fragment == null)
				{
					this.fragment = string.Empty;
				}
				else if (this.fragment.Length > 0)
				{
					this.fragment = "#" + value.Replace("%23", "#");
				}
				this.modified = true;
			}
		}

		// Token: 0x17000BD2 RID: 3026
		// (get) Token: 0x06002B19 RID: 11033 RVA: 0x00094120 File Offset: 0x00092320
		// (set) Token: 0x06002B1A RID: 11034 RVA: 0x00094128 File Offset: 0x00092328
		public string Host
		{
			get
			{
				return this.host;
			}
			set
			{
				this.host = ((value != null) ? value : string.Empty);
				this.modified = true;
			}
		}

		// Token: 0x17000BD3 RID: 3027
		// (get) Token: 0x06002B1B RID: 11035 RVA: 0x00094148 File Offset: 0x00092348
		// (set) Token: 0x06002B1C RID: 11036 RVA: 0x00094150 File Offset: 0x00092350
		public string Password
		{
			get
			{
				return this.password;
			}
			set
			{
				this.password = ((value != null) ? value : string.Empty);
				this.modified = true;
			}
		}

		// Token: 0x17000BD4 RID: 3028
		// (get) Token: 0x06002B1D RID: 11037 RVA: 0x00094170 File Offset: 0x00092370
		// (set) Token: 0x06002B1E RID: 11038 RVA: 0x00094178 File Offset: 0x00092378
		public string Path
		{
			get
			{
				return this.path;
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					this.path = "/";
				}
				else
				{
					this.path = System.Uri.EscapeString(value.Replace('\\', '/'), false, true, true);
				}
				this.modified = true;
			}
		}

		// Token: 0x17000BD5 RID: 3029
		// (get) Token: 0x06002B1F RID: 11039 RVA: 0x000941C8 File Offset: 0x000923C8
		// (set) Token: 0x06002B20 RID: 11040 RVA: 0x000941D0 File Offset: 0x000923D0
		public int Port
		{
			get
			{
				return this.port;
			}
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.port = value;
				this.modified = true;
			}
		}

		// Token: 0x17000BD6 RID: 3030
		// (get) Token: 0x06002B21 RID: 11041 RVA: 0x00094200 File Offset: 0x00092400
		// (set) Token: 0x06002B22 RID: 11042 RVA: 0x00094208 File Offset: 0x00092408
		public string Query
		{
			get
			{
				return this.query;
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					this.query = string.Empty;
				}
				else
				{
					this.query = "?" + value;
				}
				this.modified = true;
			}
		}

		// Token: 0x17000BD7 RID: 3031
		// (get) Token: 0x06002B23 RID: 11043 RVA: 0x00094244 File Offset: 0x00092444
		// (set) Token: 0x06002B24 RID: 11044 RVA: 0x0009424C File Offset: 0x0009244C
		public string Scheme
		{
			get
			{
				return this.scheme;
			}
			set
			{
				if (value == null)
				{
					value = string.Empty;
				}
				int num = value.IndexOf(':');
				if (num != -1)
				{
					value = value.Substring(0, num);
				}
				this.scheme = value.ToLower();
				this.modified = true;
			}
		}

		// Token: 0x17000BD8 RID: 3032
		// (get) Token: 0x06002B25 RID: 11045 RVA: 0x00094294 File Offset: 0x00092494
		public System.Uri Uri
		{
			get
			{
				if (!this.modified)
				{
					return this.uri;
				}
				this.uri = new System.Uri(this.ToString(), true);
				this.modified = false;
				return this.uri;
			}
		}

		// Token: 0x17000BD9 RID: 3033
		// (get) Token: 0x06002B26 RID: 11046 RVA: 0x000942C8 File Offset: 0x000924C8
		// (set) Token: 0x06002B27 RID: 11047 RVA: 0x000942D0 File Offset: 0x000924D0
		public string UserName
		{
			get
			{
				return this.username;
			}
			set
			{
				this.username = ((value != null) ? value : string.Empty);
				this.modified = true;
			}
		}

		// Token: 0x06002B28 RID: 11048 RVA: 0x000942F0 File Offset: 0x000924F0
		public override bool Equals(object rparam)
		{
			return rparam != null && this.Uri.Equals(rparam.ToString());
		}

		// Token: 0x06002B29 RID: 11049 RVA: 0x00094310 File Offset: 0x00092510
		public override int GetHashCode()
		{
			return this.Uri.GetHashCode();
		}

		// Token: 0x06002B2A RID: 11050 RVA: 0x00094320 File Offset: 0x00092520
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this.scheme);
			stringBuilder.Append("://");
			if (this.username != string.Empty)
			{
				stringBuilder.Append(this.username);
				if (this.password != string.Empty)
				{
					stringBuilder.Append(":" + this.password);
				}
				stringBuilder.Append('@');
			}
			stringBuilder.Append(this.host);
			if (this.port > 0)
			{
				stringBuilder.Append(":" + this.port);
			}
			if (this.path != string.Empty && stringBuilder[stringBuilder.Length - 1] != '/' && this.path.Length > 0 && this.path[0] != '/')
			{
				stringBuilder.Append('/');
			}
			stringBuilder.Append(this.path);
			stringBuilder.Append(this.query);
			stringBuilder.Append(this.fragment);
			return stringBuilder.ToString();
		}

		// Token: 0x04001B26 RID: 6950
		private string scheme;

		// Token: 0x04001B27 RID: 6951
		private string host;

		// Token: 0x04001B28 RID: 6952
		private int port;

		// Token: 0x04001B29 RID: 6953
		private string path;

		// Token: 0x04001B2A RID: 6954
		private string query;

		// Token: 0x04001B2B RID: 6955
		private string fragment;

		// Token: 0x04001B2C RID: 6956
		private string username;

		// Token: 0x04001B2D RID: 6957
		private string password;

		// Token: 0x04001B2E RID: 6958
		private System.Uri uri;

		// Token: 0x04001B2F RID: 6959
		private bool modified;
	}
}
