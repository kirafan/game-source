﻿using System;

namespace System.Threading
{
	// Token: 0x0200051F RID: 1311
	// (Invoke) Token: 0x06002D28 RID: 11560
	public delegate void ThreadExceptionEventHandler(object sender, ThreadExceptionEventArgs e);
}
