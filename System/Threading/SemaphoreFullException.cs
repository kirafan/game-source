﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020004AB RID: 1195
	[ComVisible(false)]
	[Serializable]
	public class SemaphoreFullException : SystemException
	{
		// Token: 0x06002AF1 RID: 10993 RVA: 0x00093ABC File Offset: 0x00091CBC
		public SemaphoreFullException() : base(Locale.GetText("Exceeding maximum."))
		{
		}

		// Token: 0x06002AF2 RID: 10994 RVA: 0x00093AD0 File Offset: 0x00091CD0
		public SemaphoreFullException(string message) : base(message)
		{
		}

		// Token: 0x06002AF3 RID: 10995 RVA: 0x00093ADC File Offset: 0x00091CDC
		public SemaphoreFullException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06002AF4 RID: 10996 RVA: 0x00093AE8 File Offset: 0x00091CE8
		protected SemaphoreFullException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
