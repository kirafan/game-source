﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Threading
{
	// Token: 0x020004AA RID: 1194
	[ComVisible(false)]
	public sealed class Semaphore : WaitHandle
	{
		// Token: 0x06002AE3 RID: 10979 RVA: 0x00093894 File Offset: 0x00091A94
		private Semaphore(IntPtr handle)
		{
			this.Handle = handle;
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x000938A4 File Offset: 0x00091AA4
		public Semaphore(int initialCount, int maximumCount) : this(initialCount, maximumCount, null)
		{
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x000938B0 File Offset: 0x00091AB0
		public Semaphore(int initialCount, int maximumCount, string name)
		{
			if (initialCount < 0)
			{
				throw new ArgumentOutOfRangeException("initialCount", "< 0");
			}
			if (maximumCount < 1)
			{
				throw new ArgumentOutOfRangeException("maximumCount", "< 1");
			}
			if (initialCount > maximumCount)
			{
				throw new ArgumentException("initialCount > maximumCount");
			}
			bool flag;
			this.Handle = Semaphore.CreateSemaphore_internal(initialCount, maximumCount, name, out flag);
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x00093914 File Offset: 0x00091B14
		public Semaphore(int initialCount, int maximumCount, string name, out bool createdNew) : this(initialCount, maximumCount, name, out createdNew, null)
		{
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x00093924 File Offset: 0x00091B24
		[MonoTODO("Does not support access control, semaphoreSecurity is ignored")]
		public Semaphore(int initialCount, int maximumCount, string name, out bool createdNew, System.Security.AccessControl.SemaphoreSecurity semaphoreSecurity)
		{
			if (initialCount < 0)
			{
				throw new ArgumentOutOfRangeException("initialCount", "< 0");
			}
			if (maximumCount < 1)
			{
				throw new ArgumentOutOfRangeException("maximumCount", "< 1");
			}
			if (initialCount > maximumCount)
			{
				throw new ArgumentException("initialCount > maximumCount");
			}
			this.Handle = Semaphore.CreateSemaphore_internal(initialCount, maximumCount, name, out createdNew);
		}

		// Token: 0x06002AE8 RID: 10984
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateSemaphore_internal(int initialCount, int maximumCount, string name, out bool createdNew);

		// Token: 0x06002AE9 RID: 10985
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ReleaseSemaphore_internal(IntPtr handle, int releaseCount, out bool fail);

		// Token: 0x06002AEA RID: 10986
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr OpenSemaphore_internal(string name, System.Security.AccessControl.SemaphoreRights rights, out MonoIOError error);

		// Token: 0x06002AEB RID: 10987 RVA: 0x00093988 File Offset: 0x00091B88
		[MonoTODO]
		public System.Security.AccessControl.SemaphoreSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEC RID: 10988 RVA: 0x00093990 File Offset: 0x00091B90
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[PrePrepareMethod]
		public int Release()
		{
			return this.Release(1);
		}

		// Token: 0x06002AED RID: 10989 RVA: 0x0009399C File Offset: 0x00091B9C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public int Release(int releaseCount)
		{
			if (releaseCount < 1)
			{
				throw new ArgumentOutOfRangeException("releaseCount");
			}
			bool flag;
			int result = Semaphore.ReleaseSemaphore_internal(this.Handle, releaseCount, out flag);
			if (flag)
			{
				throw new SemaphoreFullException();
			}
			return result;
		}

		// Token: 0x06002AEE RID: 10990 RVA: 0x000939D8 File Offset: 0x00091BD8
		[MonoTODO]
		public void SetAccessControl(System.Security.AccessControl.SemaphoreSecurity semaphoreSecurity)
		{
			if (semaphoreSecurity == null)
			{
				throw new ArgumentNullException("semaphoreSecurity");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06002AEF RID: 10991 RVA: 0x000939F0 File Offset: 0x00091BF0
		public static Semaphore OpenExisting(string name)
		{
			return Semaphore.OpenExisting(name, System.Security.AccessControl.SemaphoreRights.Modify | System.Security.AccessControl.SemaphoreRights.Synchronize);
		}

		// Token: 0x06002AF0 RID: 10992 RVA: 0x00093A00 File Offset: 0x00091C00
		public static Semaphore OpenExisting(string name, System.Security.AccessControl.SemaphoreRights rights)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0 || name.Length > 260)
			{
				throw new ArgumentException("name", Locale.GetText("Invalid length [1-260]."));
			}
			MonoIOError monoIOError;
			IntPtr intPtr = Semaphore.OpenSemaphore_internal(name, rights, out monoIOError);
			if (!(intPtr == (IntPtr)null))
			{
				return new Semaphore(intPtr);
			}
			if (monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				throw new WaitHandleCannotBeOpenedException(Locale.GetText("Named Semaphore handle does not exist: ") + name);
			}
			if (monoIOError == MonoIOError.ERROR_ACCESS_DENIED)
			{
				throw new UnauthorizedAccessException();
			}
			throw new IOException(Locale.GetText("Win32 IO error: ") + monoIOError.ToString());
		}
	}
}
