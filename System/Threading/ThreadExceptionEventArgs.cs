﻿using System;

namespace System.Threading
{
	// Token: 0x020004AC RID: 1196
	public class ThreadExceptionEventArgs : EventArgs
	{
		// Token: 0x06002AF5 RID: 10997 RVA: 0x00093AF4 File Offset: 0x00091CF4
		public ThreadExceptionEventArgs(Exception t)
		{
			this.exception = t;
		}

		// Token: 0x17000BC9 RID: 3017
		// (get) Token: 0x06002AF6 RID: 10998 RVA: 0x00093B04 File Offset: 0x00091D04
		public Exception Exception
		{
			get
			{
				return this.exception;
			}
		}

		// Token: 0x04001B1E RID: 6942
		private Exception exception;
	}
}
