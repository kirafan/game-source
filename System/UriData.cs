﻿using System;

namespace System
{
	// Token: 0x020004D8 RID: 1240
	internal class UriData : IUriData
	{
		// Token: 0x06002C02 RID: 11266 RVA: 0x00098F64 File Offset: 0x00097164
		public UriData(System.Uri uri, System.UriParser parser)
		{
			this.uri = uri;
			this.parser = parser;
		}

		// Token: 0x06002C03 RID: 11267 RVA: 0x00098F7C File Offset: 0x0009717C
		private string Lookup(ref string cache, System.UriComponents components)
		{
			return this.Lookup(ref cache, components, (!this.uri.UserEscaped) ? System.UriFormat.UriEscaped : System.UriFormat.Unescaped);
		}

		// Token: 0x06002C04 RID: 11268 RVA: 0x00098FA0 File Offset: 0x000971A0
		private string Lookup(ref string cache, System.UriComponents components, System.UriFormat format)
		{
			if (cache == null)
			{
				cache = this.parser.GetComponents(this.uri, components, format);
			}
			return cache;
		}

		// Token: 0x17000C12 RID: 3090
		// (get) Token: 0x06002C05 RID: 11269 RVA: 0x00098FC0 File Offset: 0x000971C0
		public string AbsolutePath
		{
			get
			{
				return this.Lookup(ref this.absolute_path, System.UriComponents.Path | System.UriComponents.KeepDelimiter);
			}
		}

		// Token: 0x17000C13 RID: 3091
		// (get) Token: 0x06002C06 RID: 11270 RVA: 0x00098FD4 File Offset: 0x000971D4
		public string AbsoluteUri
		{
			get
			{
				return this.Lookup(ref this.absolute_uri, System.UriComponents.AbsoluteUri);
			}
		}

		// Token: 0x17000C14 RID: 3092
		// (get) Token: 0x06002C07 RID: 11271 RVA: 0x00098FE4 File Offset: 0x000971E4
		public string AbsoluteUri_SafeUnescaped
		{
			get
			{
				return this.Lookup(ref this.absolute_uri_unescaped, System.UriComponents.AbsoluteUri, System.UriFormat.SafeUnescaped);
			}
		}

		// Token: 0x17000C15 RID: 3093
		// (get) Token: 0x06002C08 RID: 11272 RVA: 0x00098FF8 File Offset: 0x000971F8
		public string Authority
		{
			get
			{
				return this.Lookup(ref this.authority, System.UriComponents.Host | System.UriComponents.Port);
			}
		}

		// Token: 0x17000C16 RID: 3094
		// (get) Token: 0x06002C09 RID: 11273 RVA: 0x00099008 File Offset: 0x00097208
		public string Fragment
		{
			get
			{
				return this.Lookup(ref this.fragment, System.UriComponents.Fragment | System.UriComponents.KeepDelimiter);
			}
		}

		// Token: 0x17000C17 RID: 3095
		// (get) Token: 0x06002C0A RID: 11274 RVA: 0x0009901C File Offset: 0x0009721C
		public string Host
		{
			get
			{
				return this.Lookup(ref this.host, System.UriComponents.Host);
			}
		}

		// Token: 0x17000C18 RID: 3096
		// (get) Token: 0x06002C0B RID: 11275 RVA: 0x0009902C File Offset: 0x0009722C
		public string PathAndQuery
		{
			get
			{
				return this.Lookup(ref this.path_and_query, System.UriComponents.PathAndQuery);
			}
		}

		// Token: 0x17000C19 RID: 3097
		// (get) Token: 0x06002C0C RID: 11276 RVA: 0x0009903C File Offset: 0x0009723C
		public string StrongPort
		{
			get
			{
				return this.Lookup(ref this.strong_port, System.UriComponents.StrongPort);
			}
		}

		// Token: 0x17000C1A RID: 3098
		// (get) Token: 0x06002C0D RID: 11277 RVA: 0x00099050 File Offset: 0x00097250
		public string Query
		{
			get
			{
				return this.Lookup(ref this.query, System.UriComponents.Query | System.UriComponents.KeepDelimiter);
			}
		}

		// Token: 0x17000C1B RID: 3099
		// (get) Token: 0x06002C0E RID: 11278 RVA: 0x00099064 File Offset: 0x00097264
		public string UserInfo
		{
			get
			{
				return this.Lookup(ref this.user_info, System.UriComponents.UserInfo);
			}
		}

		// Token: 0x04001BCB RID: 7115
		private System.Uri uri;

		// Token: 0x04001BCC RID: 7116
		private System.UriParser parser;

		// Token: 0x04001BCD RID: 7117
		private string absolute_path;

		// Token: 0x04001BCE RID: 7118
		private string absolute_uri;

		// Token: 0x04001BCF RID: 7119
		private string absolute_uri_unescaped;

		// Token: 0x04001BD0 RID: 7120
		private string authority;

		// Token: 0x04001BD1 RID: 7121
		private string fragment;

		// Token: 0x04001BD2 RID: 7122
		private string host;

		// Token: 0x04001BD3 RID: 7123
		private string path_and_query;

		// Token: 0x04001BD4 RID: 7124
		private string strong_port;

		// Token: 0x04001BD5 RID: 7125
		private string query;

		// Token: 0x04001BD6 RID: 7126
		private string user_info;
	}
}
