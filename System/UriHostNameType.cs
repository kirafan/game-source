﻿using System;

namespace System
{
	// Token: 0x020004B6 RID: 1206
	public enum UriHostNameType
	{
		// Token: 0x04001B6F RID: 7023
		Unknown,
		// Token: 0x04001B70 RID: 7024
		Basic,
		// Token: 0x04001B71 RID: 7025
		Dns,
		// Token: 0x04001B72 RID: 7026
		IPv4,
		// Token: 0x04001B73 RID: 7027
		IPv6
	}
}
