﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004F0 RID: 1264
	// (Invoke) Token: 0x06002C6C RID: 11372
	public delegate void SessionEndingEventHandler(object sender, SessionEndingEventArgs e);
}
