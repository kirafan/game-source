﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x02000017 RID: 23
	public enum SessionEndReasons
	{
		// Token: 0x0400003C RID: 60
		Logoff = 1,
		// Token: 0x0400003D RID: 61
		SystemShutdown
	}
}
