﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x02000019 RID: 25
	public enum SessionSwitchReason
	{
		// Token: 0x04000040 RID: 64
		ConsoleConnect = 1,
		// Token: 0x04000041 RID: 65
		ConsoleDisconnect,
		// Token: 0x04000042 RID: 66
		RemoteConnect,
		// Token: 0x04000043 RID: 67
		RemoteDisconnect,
		// Token: 0x04000044 RID: 68
		SessionLogon,
		// Token: 0x04000045 RID: 69
		SessionLogoff,
		// Token: 0x04000046 RID: 70
		SessionLock,
		// Token: 0x04000047 RID: 71
		SessionUnlock,
		// Token: 0x04000048 RID: 72
		SessionRemoteControl
	}
}
