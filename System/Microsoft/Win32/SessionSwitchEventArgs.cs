﻿using System;
using System.Security.Permissions;

namespace Microsoft.Win32
{
	// Token: 0x02000018 RID: 24
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class SessionSwitchEventArgs : EventArgs
	{
		// Token: 0x060000FA RID: 250 RVA: 0x00009E84 File Offset: 0x00008084
		public SessionSwitchEventArgs(SessionSwitchReason reason)
		{
			this.reason = reason;
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00009E94 File Offset: 0x00008094
		public SessionSwitchReason Reason
		{
			get
			{
				return this.reason;
			}
		}

		// Token: 0x0400003E RID: 62
		private SessionSwitchReason reason;
	}
}
