﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x0200001C RID: 28
	public enum UserPreferenceCategory
	{
		// Token: 0x0400004D RID: 77
		Accessibility = 1,
		// Token: 0x0400004E RID: 78
		Color,
		// Token: 0x0400004F RID: 79
		Desktop,
		// Token: 0x04000050 RID: 80
		General,
		// Token: 0x04000051 RID: 81
		Icon,
		// Token: 0x04000052 RID: 82
		Keyboard,
		// Token: 0x04000053 RID: 83
		Menu,
		// Token: 0x04000054 RID: 84
		Mouse,
		// Token: 0x04000055 RID: 85
		Policy,
		// Token: 0x04000056 RID: 86
		Power,
		// Token: 0x04000057 RID: 87
		Screensaver,
		// Token: 0x04000058 RID: 88
		Window,
		// Token: 0x04000059 RID: 89
		Locale,
		// Token: 0x0400005A RID: 90
		VisualStyle
	}
}
