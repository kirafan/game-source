﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004EF RID: 1263
	// (Invoke) Token: 0x06002C68 RID: 11368
	public delegate void SessionEndedEventHandler(object sender, SessionEndedEventArgs e);
}
