﻿using System;
using System.Security.Permissions;

namespace Microsoft.Win32
{
	// Token: 0x02000013 RID: 19
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class PowerModeChangedEventArgs : EventArgs
	{
		// Token: 0x060000F2 RID: 242 RVA: 0x00009E28 File Offset: 0x00008028
		public PowerModeChangedEventArgs(PowerModes mode)
		{
			this.mymode = mode;
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00009E38 File Offset: 0x00008038
		public PowerModes Mode
		{
			get
			{
				return this.mymode;
			}
		}

		// Token: 0x04000033 RID: 51
		private PowerModes mymode;
	}
}
