﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x02000014 RID: 20
	public enum PowerModes
	{
		// Token: 0x04000035 RID: 53
		Resume = 1,
		// Token: 0x04000036 RID: 54
		StatusChange,
		// Token: 0x04000037 RID: 55
		Suspend
	}
}
