﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004F4 RID: 1268
	// (Invoke) Token: 0x06002C7C RID: 11388
	public delegate void UserPreferenceChangingEventHandler(object sender, UserPreferenceChangingEventArgs e);
}
