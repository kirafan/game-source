﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004F1 RID: 1265
	// (Invoke) Token: 0x06002C70 RID: 11376
	public delegate void SessionSwitchEventHandler(object sender, SessionSwitchEventArgs e);
}
