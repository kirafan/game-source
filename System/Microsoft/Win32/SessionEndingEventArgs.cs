﻿using System;
using System.Security.Permissions;

namespace Microsoft.Win32
{
	// Token: 0x02000016 RID: 22
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class SessionEndingEventArgs : EventArgs
	{
		// Token: 0x060000F6 RID: 246 RVA: 0x00009E58 File Offset: 0x00008058
		public SessionEndingEventArgs(SessionEndReasons reason)
		{
			this.myreason = reason;
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00009E68 File Offset: 0x00008068
		public SessionEndReasons Reason
		{
			get
			{
				return this.myreason;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00009E70 File Offset: 0x00008070
		// (set) Token: 0x060000F9 RID: 249 RVA: 0x00009E78 File Offset: 0x00008078
		public bool Cancel
		{
			get
			{
				return this.mycancel;
			}
			set
			{
				this.mycancel = value;
			}
		}

		// Token: 0x04000039 RID: 57
		private SessionEndReasons myreason;

		// Token: 0x0400003A RID: 58
		private bool mycancel;
	}
}
