﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004F2 RID: 1266
	// (Invoke) Token: 0x06002C74 RID: 11380
	public delegate void TimerElapsedEventHandler(object sender, TimerElapsedEventArgs e);
}
