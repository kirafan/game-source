﻿using System;
using System.Security.Permissions;

namespace Microsoft.Win32
{
	// Token: 0x0200001D RID: 29
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class UserPreferenceChangedEventArgs : EventArgs
	{
		// Token: 0x06000120 RID: 288 RVA: 0x0000A038 File Offset: 0x00008238
		public UserPreferenceChangedEventArgs(UserPreferenceCategory category)
		{
			this.mycategory = category;
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000121 RID: 289 RVA: 0x0000A048 File Offset: 0x00008248
		public UserPreferenceCategory Category
		{
			get
			{
				return this.mycategory;
			}
		}

		// Token: 0x0400005B RID: 91
		private UserPreferenceCategory mycategory;
	}
}
