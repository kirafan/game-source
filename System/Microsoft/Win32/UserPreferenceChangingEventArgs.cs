﻿using System;
using System.Security.Permissions;

namespace Microsoft.Win32
{
	// Token: 0x0200001E RID: 30
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class UserPreferenceChangingEventArgs : EventArgs
	{
		// Token: 0x06000122 RID: 290 RVA: 0x0000A050 File Offset: 0x00008250
		public UserPreferenceChangingEventArgs(UserPreferenceCategory category)
		{
			this.mycategory = category;
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000123 RID: 291 RVA: 0x0000A060 File Offset: 0x00008260
		public UserPreferenceCategory Category
		{
			get
			{
				return this.mycategory;
			}
		}

		// Token: 0x0400005C RID: 92
		private UserPreferenceCategory mycategory;
	}
}
