﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004EE RID: 1262
	// (Invoke) Token: 0x06002C64 RID: 11364
	public delegate void PowerModeChangedEventHandler(object sender, PowerModeChangedEventArgs e);
}
