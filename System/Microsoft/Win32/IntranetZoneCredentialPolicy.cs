﻿using System;
using System.Net;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;

namespace Microsoft.Win32
{
	// Token: 0x02000012 RID: 18
	public class IntranetZoneCredentialPolicy : System.Net.ICredentialPolicy
	{
		// Token: 0x060000F0 RID: 240 RVA: 0x00009DFC File Offset: 0x00007FFC
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public IntranetZoneCredentialPolicy()
		{
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00009E04 File Offset: 0x00008004
		public virtual bool ShouldSendCredential(System.Uri challengeUri, System.Net.WebRequest request, System.Net.NetworkCredential credential, System.Net.IAuthenticationModule authenticationModule)
		{
			Zone zone = Zone.CreateFromUrl(challengeUri.AbsoluteUri);
			return zone.SecurityZone == SecurityZone.Intranet;
		}
	}
}
