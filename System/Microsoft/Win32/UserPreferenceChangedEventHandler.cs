﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x020004F3 RID: 1267
	// (Invoke) Token: 0x06002C78 RID: 11384
	public delegate void UserPreferenceChangedEventHandler(object sender, UserPreferenceChangedEventArgs e);
}
