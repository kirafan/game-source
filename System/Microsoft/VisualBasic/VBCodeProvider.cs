﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Security.Permissions;

namespace Microsoft.VisualBasic
{
	// Token: 0x02000011 RID: 17
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	[PermissionSet((SecurityAction)14, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
	public class VBCodeProvider : System.CodeDom.Compiler.CodeDomProvider
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x00009DC0 File Offset: 0x00007FC0
		public VBCodeProvider()
		{
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00009DC8 File Offset: 0x00007FC8
		public VBCodeProvider(IDictionary<string, string> providerOptions)
		{
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x060000EA RID: 234 RVA: 0x00009DD0 File Offset: 0x00007FD0
		public override string FileExtension
		{
			get
			{
				return "vb";
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x060000EB RID: 235 RVA: 0x00009DD8 File Offset: 0x00007FD8
		public override System.CodeDom.Compiler.LanguageOptions LanguageOptions
		{
			get
			{
				return System.CodeDom.Compiler.LanguageOptions.CaseInsensitive;
			}
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00009DDC File Offset: 0x00007FDC
		[Obsolete("Use CodeDomProvider class")]
		public override System.CodeDom.Compiler.ICodeCompiler CreateCompiler()
		{
			return new VBCodeCompiler();
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00009DE4 File Offset: 0x00007FE4
		[Obsolete("Use CodeDomProvider class")]
		public override System.CodeDom.Compiler.ICodeGenerator CreateGenerator()
		{
			return new VBCodeGenerator();
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00009DEC File Offset: 0x00007FEC
		public override System.ComponentModel.TypeConverter GetConverter(Type type)
		{
			return System.ComponentModel.TypeDescriptor.GetConverter(type);
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00009DF4 File Offset: 0x00007FF4
		[MonoTODO]
		public override void GenerateCodeFromMember(System.CodeDom.CodeTypeMember member, TextWriter writer, System.CodeDom.Compiler.CodeGeneratorOptions options)
		{
			throw new NotImplementedException();
		}
	}
}
