﻿using System;

namespace System.Net
{
	// Token: 0x02000325 RID: 805
	public interface IAuthenticationModule
	{
		// Token: 0x06001C9B RID: 7323
		Authorization Authenticate(string challenge, WebRequest request, ICredentials credentials);

		// Token: 0x06001C9C RID: 7324
		Authorization PreAuthenticate(WebRequest request, ICredentials credentials);

		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x06001C9D RID: 7325
		string AuthenticationType { get; }

		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x06001C9E RID: 7326
		bool CanPreAuthenticate { get; }
	}
}
