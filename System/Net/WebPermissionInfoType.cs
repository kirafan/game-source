﻿using System;

namespace System.Net
{
	// Token: 0x0200041B RID: 1051
	internal enum WebPermissionInfoType
	{
		// Token: 0x0400176F RID: 5999
		InfoString,
		// Token: 0x04001770 RID: 6000
		InfoUnexecutedRegex,
		// Token: 0x04001771 RID: 6001
		InfoRegex
	}
}
