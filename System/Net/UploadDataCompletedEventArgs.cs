﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C0 RID: 1216
	public class UploadDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BC1 RID: 11201 RVA: 0x00098C90 File Offset: 0x00096E90
		internal UploadDataCompletedEventArgs(byte[] result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BF6 RID: 3062
		// (get) Token: 0x06002BC2 RID: 11202 RVA: 0x00098CA4 File Offset: 0x00096EA4
		public byte[] Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B93 RID: 7059
		private byte[] result;
	}
}
