﻿using System;
using System.Net.Sockets;

namespace System.Net
{
	// Token: 0x0200032B RID: 811
	[Serializable]
	public class IPEndPoint : EndPoint
	{
		// Token: 0x06001CC5 RID: 7365 RVA: 0x00054E94 File Offset: 0x00053094
		public IPEndPoint(IPAddress address, int port)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.Address = address;
			this.Port = port;
		}

		// Token: 0x06001CC6 RID: 7366 RVA: 0x00054EBC File Offset: 0x000530BC
		public IPEndPoint(long iaddr, int port)
		{
			this.Address = new IPAddress(iaddr);
			this.Port = port;
		}

		// Token: 0x17000729 RID: 1833
		// (get) Token: 0x06001CC7 RID: 7367 RVA: 0x00054ED8 File Offset: 0x000530D8
		// (set) Token: 0x06001CC8 RID: 7368 RVA: 0x00054EE0 File Offset: 0x000530E0
		public IPAddress Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x06001CC9 RID: 7369 RVA: 0x00054EEC File Offset: 0x000530EC
		public override System.Net.Sockets.AddressFamily AddressFamily
		{
			get
			{
				return this.address.AddressFamily;
			}
		}

		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x06001CCA RID: 7370 RVA: 0x00054EFC File Offset: 0x000530FC
		// (set) Token: 0x06001CCB RID: 7371 RVA: 0x00054F04 File Offset: 0x00053104
		public int Port
		{
			get
			{
				return this.port;
			}
			set
			{
				if (value < 0 || value > 65535)
				{
					throw new ArgumentOutOfRangeException("Invalid port");
				}
				this.port = value;
			}
		}

		// Token: 0x06001CCC RID: 7372 RVA: 0x00054F38 File Offset: 0x00053138
		public override EndPoint Create(SocketAddress socketAddress)
		{
			if (socketAddress == null)
			{
				throw new ArgumentNullException("socketAddress");
			}
			if (socketAddress.Family != this.AddressFamily)
			{
				throw new ArgumentException(string.Concat(new object[]
				{
					"The IPEndPoint was created using ",
					this.AddressFamily,
					" AddressFamily but SocketAddress contains ",
					socketAddress.Family,
					" instead, please use the same type."
				}));
			}
			int size = socketAddress.Size;
			System.Net.Sockets.AddressFamily family = socketAddress.Family;
			System.Net.Sockets.AddressFamily addressFamily = family;
			IPEndPoint result;
			if (addressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
			{
				if (addressFamily != System.Net.Sockets.AddressFamily.InterNetworkV6)
				{
					return null;
				}
				if (size < 28)
				{
					return null;
				}
				int num = ((int)socketAddress[2] << 8) + (int)socketAddress[3];
				int num2 = (int)socketAddress[24] + ((int)socketAddress[25] << 8) + ((int)socketAddress[26] << 16) + ((int)socketAddress[27] << 24);
				ushort[] array = new ushort[8];
				for (int i = 0; i < 8; i++)
				{
					array[i] = (ushort)(((int)socketAddress[8 + i * 2] << 8) + (int)socketAddress[8 + i * 2 + 1]);
				}
				result = new IPEndPoint(new IPAddress(array, (long)num2), num);
			}
			else
			{
				if (size < 8)
				{
					return null;
				}
				int num = ((int)socketAddress[2] << 8) + (int)socketAddress[3];
				long iaddr = ((long)socketAddress[7] << 24) + ((long)socketAddress[6] << 16) + ((long)socketAddress[5] << 8) + (long)socketAddress[4];
				result = new IPEndPoint(iaddr, num);
			}
			return result;
		}

		// Token: 0x06001CCD RID: 7373 RVA: 0x000550D4 File Offset: 0x000532D4
		public override SocketAddress Serialize()
		{
			SocketAddress socketAddress = null;
			System.Net.Sockets.AddressFamily addressFamily = this.address.AddressFamily;
			if (addressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
			{
				if (addressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
				{
					socketAddress = new SocketAddress(System.Net.Sockets.AddressFamily.InterNetworkV6, 28);
					socketAddress[2] = (byte)(this.port >> 8 & 255);
					socketAddress[3] = (byte)(this.port & 255);
					byte[] addressBytes = this.address.GetAddressBytes();
					for (int i = 0; i < 16; i++)
					{
						socketAddress[8 + i] = addressBytes[i];
					}
					socketAddress[24] = (byte)(this.address.ScopeId & 255L);
					socketAddress[25] = (byte)(this.address.ScopeId >> 8 & 255L);
					socketAddress[26] = (byte)(this.address.ScopeId >> 16 & 255L);
					socketAddress[27] = (byte)(this.address.ScopeId >> 24 & 255L);
				}
			}
			else
			{
				socketAddress = new SocketAddress(System.Net.Sockets.AddressFamily.InterNetwork, 16);
				socketAddress[2] = (byte)(this.port >> 8 & 255);
				socketAddress[3] = (byte)(this.port & 255);
				long internalIPv4Address = this.address.InternalIPv4Address;
				socketAddress[4] = (byte)(internalIPv4Address & 255L);
				socketAddress[5] = (byte)(internalIPv4Address >> 8 & 255L);
				socketAddress[6] = (byte)(internalIPv4Address >> 16 & 255L);
				socketAddress[7] = (byte)(internalIPv4Address >> 24 & 255L);
			}
			return socketAddress;
		}

		// Token: 0x06001CCE RID: 7374 RVA: 0x0005526C File Offset: 0x0005346C
		public override string ToString()
		{
			return this.address.ToString() + ":" + this.port;
		}

		// Token: 0x06001CCF RID: 7375 RVA: 0x0005529C File Offset: 0x0005349C
		public override bool Equals(object obj)
		{
			IPEndPoint ipendPoint = obj as IPEndPoint;
			return ipendPoint != null && ipendPoint.port == this.port && ipendPoint.address.Equals(this.address);
		}

		// Token: 0x06001CD0 RID: 7376 RVA: 0x000552DC File Offset: 0x000534DC
		public override int GetHashCode()
		{
			return this.address.GetHashCode() + this.port;
		}

		// Token: 0x04001210 RID: 4624
		public const int MaxPort = 65535;

		// Token: 0x04001211 RID: 4625
		public const int MinPort = 0;

		// Token: 0x04001212 RID: 4626
		private IPAddress address;

		// Token: 0x04001213 RID: 4627
		private int port;
	}
}
