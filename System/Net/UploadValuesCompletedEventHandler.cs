﻿using System;

namespace System.Net
{
	// Token: 0x02000529 RID: 1321
	// (Invoke) Token: 0x06002D50 RID: 11600
	public delegate void UploadValuesCompletedEventHandler(object sender, UploadValuesCompletedEventArgs e);
}
