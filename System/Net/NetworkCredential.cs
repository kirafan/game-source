﻿using System;

namespace System.Net
{
	// Token: 0x02000359 RID: 857
	public class NetworkCredential : ICredentials, ICredentialsByHost
	{
		// Token: 0x06001E3D RID: 7741 RVA: 0x0005CAB4 File Offset: 0x0005ACB4
		public NetworkCredential()
		{
		}

		// Token: 0x06001E3E RID: 7742 RVA: 0x0005CABC File Offset: 0x0005ACBC
		public NetworkCredential(string userName, string password)
		{
			this.userName = userName;
			this.password = password;
		}

		// Token: 0x06001E3F RID: 7743 RVA: 0x0005CAD4 File Offset: 0x0005ACD4
		public NetworkCredential(string userName, string password, string domain)
		{
			this.userName = userName;
			this.password = password;
			this.domain = domain;
		}

		// Token: 0x1700077E RID: 1918
		// (get) Token: 0x06001E40 RID: 7744 RVA: 0x0005CAF4 File Offset: 0x0005ACF4
		// (set) Token: 0x06001E41 RID: 7745 RVA: 0x0005CB14 File Offset: 0x0005AD14
		public string Domain
		{
			get
			{
				return (this.domain != null) ? this.domain : string.Empty;
			}
			set
			{
				this.domain = value;
			}
		}

		// Token: 0x1700077F RID: 1919
		// (get) Token: 0x06001E42 RID: 7746 RVA: 0x0005CB20 File Offset: 0x0005AD20
		// (set) Token: 0x06001E43 RID: 7747 RVA: 0x0005CB40 File Offset: 0x0005AD40
		public string UserName
		{
			get
			{
				return (this.userName != null) ? this.userName : string.Empty;
			}
			set
			{
				this.userName = value;
			}
		}

		// Token: 0x17000780 RID: 1920
		// (get) Token: 0x06001E44 RID: 7748 RVA: 0x0005CB4C File Offset: 0x0005AD4C
		// (set) Token: 0x06001E45 RID: 7749 RVA: 0x0005CB6C File Offset: 0x0005AD6C
		public string Password
		{
			get
			{
				return (this.password != null) ? this.password : string.Empty;
			}
			set
			{
				this.password = value;
			}
		}

		// Token: 0x06001E46 RID: 7750 RVA: 0x0005CB78 File Offset: 0x0005AD78
		public NetworkCredential GetCredential(System.Uri uri, string authType)
		{
			return this;
		}

		// Token: 0x06001E47 RID: 7751 RVA: 0x0005CB7C File Offset: 0x0005AD7C
		public NetworkCredential GetCredential(string host, int port, string authenticationType)
		{
			return this;
		}

		// Token: 0x040012D4 RID: 4820
		private string userName;

		// Token: 0x040012D5 RID: 4821
		private string password;

		// Token: 0x040012D6 RID: 4822
		private string domain;
	}
}
