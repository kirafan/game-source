﻿using System;

namespace System.Net
{
	// Token: 0x0200030A RID: 778
	public enum FtpStatusCode
	{
		// Token: 0x0400107C RID: 4220
		Undefined,
		// Token: 0x0400107D RID: 4221
		RestartMarker = 110,
		// Token: 0x0400107E RID: 4222
		ServiceTemporarilyNotAvailable = 120,
		// Token: 0x0400107F RID: 4223
		DataAlreadyOpen = 125,
		// Token: 0x04001080 RID: 4224
		OpeningData = 150,
		// Token: 0x04001081 RID: 4225
		CommandOK = 200,
		// Token: 0x04001082 RID: 4226
		CommandExtraneous = 202,
		// Token: 0x04001083 RID: 4227
		DirectoryStatus = 212,
		// Token: 0x04001084 RID: 4228
		FileStatus,
		// Token: 0x04001085 RID: 4229
		SystemType = 215,
		// Token: 0x04001086 RID: 4230
		SendUserCommand = 220,
		// Token: 0x04001087 RID: 4231
		ClosingControl,
		// Token: 0x04001088 RID: 4232
		ClosingData = 226,
		// Token: 0x04001089 RID: 4233
		EnteringPassive,
		// Token: 0x0400108A RID: 4234
		LoggedInProceed = 230,
		// Token: 0x0400108B RID: 4235
		ServerWantsSecureSession = 234,
		// Token: 0x0400108C RID: 4236
		FileActionOK = 250,
		// Token: 0x0400108D RID: 4237
		PathnameCreated = 257,
		// Token: 0x0400108E RID: 4238
		SendPasswordCommand = 331,
		// Token: 0x0400108F RID: 4239
		NeedLoginAccount,
		// Token: 0x04001090 RID: 4240
		FileCommandPending = 350,
		// Token: 0x04001091 RID: 4241
		ServiceNotAvailable = 421,
		// Token: 0x04001092 RID: 4242
		CantOpenData = 425,
		// Token: 0x04001093 RID: 4243
		ConnectionClosed,
		// Token: 0x04001094 RID: 4244
		ActionNotTakenFileUnavailableOrBusy = 450,
		// Token: 0x04001095 RID: 4245
		ActionAbortedLocalProcessingError,
		// Token: 0x04001096 RID: 4246
		ActionNotTakenInsufficientSpace,
		// Token: 0x04001097 RID: 4247
		CommandSyntaxError = 500,
		// Token: 0x04001098 RID: 4248
		ArgumentSyntaxError,
		// Token: 0x04001099 RID: 4249
		CommandNotImplemented,
		// Token: 0x0400109A RID: 4250
		BadCommandSequence,
		// Token: 0x0400109B RID: 4251
		NotLoggedIn = 530,
		// Token: 0x0400109C RID: 4252
		AccountNeeded = 532,
		// Token: 0x0400109D RID: 4253
		ActionNotTakenFileUnavailable = 550,
		// Token: 0x0400109E RID: 4254
		ActionAbortedUnknownPageType,
		// Token: 0x0400109F RID: 4255
		FileActionAborted,
		// Token: 0x040010A0 RID: 4256
		ActionNotTakenFilenameNotAllowed
	}
}
