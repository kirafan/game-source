﻿using System;

namespace System.Net
{
	// Token: 0x02000515 RID: 1301
	// (Invoke) Token: 0x06002D00 RID: 11520
	public delegate IPEndPoint BindIPEndPoint(ServicePoint servicePoint, IPEndPoint remoteEndPoint, int retryCount);
}
