﻿using System;
using System.Configuration;

namespace System.Net.Configuration
{
	// Token: 0x020002EA RID: 746
	[ConfigurationCollection(typeof(WebRequestModuleElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
	public sealed class WebRequestModuleElementCollection : ConfigurationElementCollection
	{
		// Token: 0x1700061E RID: 1566
		[MonoTODO]
		public WebRequestModuleElement this[int index]
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700061F RID: 1567
		[MonoTODO]
		public WebRequestModuleElement this[string name]
		{
			get
			{
				return (WebRequestModuleElement)base[name];
			}
			set
			{
				base[name] = value;
			}
		}

		// Token: 0x06001974 RID: 6516 RVA: 0x00045D60 File Offset: 0x00043F60
		public void Add(WebRequestModuleElement element)
		{
			this.BaseAdd(element);
		}

		// Token: 0x06001975 RID: 6517 RVA: 0x00045D6C File Offset: 0x00043F6C
		public void Clear()
		{
			base.BaseClear();
		}

		// Token: 0x06001976 RID: 6518 RVA: 0x00045D74 File Offset: 0x00043F74
		protected override ConfigurationElement CreateNewElement()
		{
			return new WebRequestModuleElement();
		}

		// Token: 0x06001977 RID: 6519 RVA: 0x00045D7C File Offset: 0x00043F7C
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (!(element is WebRequestModuleElement))
			{
				throw new ArgumentException("element");
			}
			return ((WebRequestModuleElement)element).Prefix;
		}

		// Token: 0x06001978 RID: 6520 RVA: 0x00045DA0 File Offset: 0x00043FA0
		public int IndexOf(WebRequestModuleElement element)
		{
			return base.BaseIndexOf(element);
		}

		// Token: 0x06001979 RID: 6521 RVA: 0x00045DAC File Offset: 0x00043FAC
		public void Remove(WebRequestModuleElement element)
		{
			base.BaseRemove(element.Prefix);
		}

		// Token: 0x0600197A RID: 6522 RVA: 0x00045DBC File Offset: 0x00043FBC
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}

		// Token: 0x0600197B RID: 6523 RVA: 0x00045DC8 File Offset: 0x00043FC8
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
	}
}
