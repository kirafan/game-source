﻿using System;
using System.Configuration;

namespace System.Net.Configuration
{
	// Token: 0x020002CC RID: 716
	[ConfigurationCollection(typeof(ConnectionManagementElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
	public sealed class ConnectionManagementElementCollection : ConfigurationElementCollection
	{
		// Token: 0x170005C9 RID: 1481
		[MonoTODO]
		public ConnectionManagementElement this[int index]
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170005CA RID: 1482
		public ConnectionManagementElement this[string name]
		{
			get
			{
				return (ConnectionManagementElement)base[name];
			}
			set
			{
				base[name] = value;
			}
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x00043C18 File Offset: 0x00041E18
		public void Add(ConnectionManagementElement element)
		{
			this.BaseAdd(element);
		}

		// Token: 0x060018A5 RID: 6309 RVA: 0x00043C24 File Offset: 0x00041E24
		public void Clear()
		{
			base.BaseClear();
		}

		// Token: 0x060018A6 RID: 6310 RVA: 0x00043C2C File Offset: 0x00041E2C
		protected override ConfigurationElement CreateNewElement()
		{
			return new ConnectionManagementElement();
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x00043C34 File Offset: 0x00041E34
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (!(element is ConnectionManagementElement))
			{
				throw new ArgumentException("element");
			}
			return ((ConnectionManagementElement)element).Address;
		}

		// Token: 0x060018A8 RID: 6312 RVA: 0x00043C58 File Offset: 0x00041E58
		public int IndexOf(ConnectionManagementElement element)
		{
			return base.BaseIndexOf(element);
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x00043C64 File Offset: 0x00041E64
		public void Remove(ConnectionManagementElement element)
		{
			base.BaseRemove(element);
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x00043C70 File Offset: 0x00041E70
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}

		// Token: 0x060018AB RID: 6315 RVA: 0x00043C7C File Offset: 0x00041E7C
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
	}
}
