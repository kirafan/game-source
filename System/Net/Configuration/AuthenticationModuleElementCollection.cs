﻿using System;
using System.Configuration;

namespace System.Net.Configuration
{
	// Token: 0x020002C7 RID: 711
	[ConfigurationCollection(typeof(AuthenticationModuleElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
	public sealed class AuthenticationModuleElementCollection : ConfigurationElementCollection
	{
		// Token: 0x06001872 RID: 6258 RVA: 0x00043918 File Offset: 0x00041B18
		[MonoTODO]
		public AuthenticationModuleElementCollection()
		{
		}

		// Token: 0x170005BE RID: 1470
		[MonoTODO]
		public AuthenticationModuleElement this[int index]
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170005BF RID: 1471
		[MonoTODO]
		public AuthenticationModuleElement this[string name]
		{
			get
			{
				return (AuthenticationModuleElement)base[name];
			}
			set
			{
				base[name] = value;
			}
		}

		// Token: 0x06001877 RID: 6263 RVA: 0x0004394C File Offset: 0x00041B4C
		public void Add(AuthenticationModuleElement element)
		{
			this.BaseAdd(element);
		}

		// Token: 0x06001878 RID: 6264 RVA: 0x00043958 File Offset: 0x00041B58
		public void Clear()
		{
			base.BaseClear();
		}

		// Token: 0x06001879 RID: 6265 RVA: 0x00043960 File Offset: 0x00041B60
		protected override ConfigurationElement CreateNewElement()
		{
			return new AuthenticationModuleElement();
		}

		// Token: 0x0600187A RID: 6266 RVA: 0x00043968 File Offset: 0x00041B68
		[MonoTODO("argument exception?")]
		protected override object GetElementKey(ConfigurationElement element)
		{
			if (!(element is AuthenticationModuleElement))
			{
				throw new ArgumentException("element");
			}
			return ((AuthenticationModuleElement)element).Type;
		}

		// Token: 0x0600187B RID: 6267 RVA: 0x0004398C File Offset: 0x00041B8C
		public int IndexOf(AuthenticationModuleElement element)
		{
			return base.BaseIndexOf(element);
		}

		// Token: 0x0600187C RID: 6268 RVA: 0x00043998 File Offset: 0x00041B98
		public void Remove(AuthenticationModuleElement element)
		{
			base.BaseRemove(element);
		}

		// Token: 0x0600187D RID: 6269 RVA: 0x000439A4 File Offset: 0x00041BA4
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}

		// Token: 0x0600187E RID: 6270 RVA: 0x000439B0 File Offset: 0x00041BB0
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
	}
}
