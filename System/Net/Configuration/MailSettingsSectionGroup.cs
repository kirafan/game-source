﻿using System;
using System.Configuration;

namespace System.Net.Configuration
{
	// Token: 0x020002D8 RID: 728
	public sealed class MailSettingsSectionGroup : ConfigurationSectionGroup
	{
		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x060018FA RID: 6394 RVA: 0x00044CA4 File Offset: 0x00042EA4
		public SmtpSection Smtp
		{
			get
			{
				return (SmtpSection)base.Sections["smtp"];
			}
		}
	}
}
