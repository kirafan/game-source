﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace System.Net
{
	// Token: 0x02000419 RID: 1049
	[ComVisible(true)]
	[Serializable]
	public class WebHeaderCollection : System.Collections.Specialized.NameValueCollection, ISerializable
	{
		// Token: 0x060025B6 RID: 9654 RVA: 0x000750A8 File Offset: 0x000732A8
		public WebHeaderCollection()
		{
		}

		// Token: 0x060025B7 RID: 9655 RVA: 0x000750B0 File Offset: 0x000732B0
		protected WebHeaderCollection(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			try
			{
				int @int = serializationInfo.GetInt32("Count");
				for (int i = 0; i < @int; i++)
				{
					this.Add(serializationInfo.GetString(i.ToString()), serializationInfo.GetString((@int + i).ToString()));
				}
			}
			catch (SerializationException)
			{
				int @int = serializationInfo.GetInt32("count");
				for (int j = 0; j < @int; j++)
				{
					this.Add(serializationInfo.GetString("k" + j), serializationInfo.GetString("v" + j));
				}
			}
		}

		// Token: 0x060025B8 RID: 9656 RVA: 0x0007517C File Offset: 0x0007337C
		internal WebHeaderCollection(bool internallyCreated)
		{
			this.internallyCreated = internallyCreated;
		}

		// Token: 0x060025B9 RID: 9657 RVA: 0x0007518C File Offset: 0x0007338C
		static WebHeaderCollection()
		{
			WebHeaderCollection.restricted = new Hashtable(CaseInsensitiveHashCodeProvider.DefaultInvariant, CaseInsensitiveComparer.DefaultInvariant);
			WebHeaderCollection.restricted.Add("accept", true);
			WebHeaderCollection.restricted.Add("connection", true);
			WebHeaderCollection.restricted.Add("content-length", true);
			WebHeaderCollection.restricted.Add("content-type", true);
			WebHeaderCollection.restricted.Add("date", true);
			WebHeaderCollection.restricted.Add("expect", true);
			WebHeaderCollection.restricted.Add("host", true);
			WebHeaderCollection.restricted.Add("if-modified-since", true);
			WebHeaderCollection.restricted.Add("range", true);
			WebHeaderCollection.restricted.Add("referer", true);
			WebHeaderCollection.restricted.Add("transfer-encoding", true);
			WebHeaderCollection.restricted.Add("user-agent", true);
			WebHeaderCollection.restricted.Add("proxy-connection", true);
			WebHeaderCollection.restricted_response = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
			WebHeaderCollection.restricted_response.Add("Content-Length", true);
			WebHeaderCollection.restricted_response.Add("Transfer-Encoding", true);
			WebHeaderCollection.restricted_response.Add("WWW-Authenticate", true);
			WebHeaderCollection.multiValue = new Hashtable(CaseInsensitiveHashCodeProvider.DefaultInvariant, CaseInsensitiveComparer.DefaultInvariant);
			WebHeaderCollection.multiValue.Add("accept", true);
			WebHeaderCollection.multiValue.Add("accept-charset", true);
			WebHeaderCollection.multiValue.Add("accept-encoding", true);
			WebHeaderCollection.multiValue.Add("accept-language", true);
			WebHeaderCollection.multiValue.Add("accept-ranges", true);
			WebHeaderCollection.multiValue.Add("allow", true);
			WebHeaderCollection.multiValue.Add("authorization", true);
			WebHeaderCollection.multiValue.Add("cache-control", true);
			WebHeaderCollection.multiValue.Add("connection", true);
			WebHeaderCollection.multiValue.Add("content-encoding", true);
			WebHeaderCollection.multiValue.Add("content-language", true);
			WebHeaderCollection.multiValue.Add("expect", true);
			WebHeaderCollection.multiValue.Add("if-match", true);
			WebHeaderCollection.multiValue.Add("if-none-match", true);
			WebHeaderCollection.multiValue.Add("proxy-authenticate", true);
			WebHeaderCollection.multiValue.Add("public", true);
			WebHeaderCollection.multiValue.Add("range", true);
			WebHeaderCollection.multiValue.Add("transfer-encoding", true);
			WebHeaderCollection.multiValue.Add("upgrade", true);
			WebHeaderCollection.multiValue.Add("vary", true);
			WebHeaderCollection.multiValue.Add("via", true);
			WebHeaderCollection.multiValue.Add("warning", true);
			WebHeaderCollection.multiValue.Add("www-authenticate", true);
			WebHeaderCollection.multiValue.Add("set-cookie", true);
			WebHeaderCollection.multiValue.Add("set-cookie2", true);
		}

		// Token: 0x060025BA RID: 9658 RVA: 0x00075538 File Offset: 0x00073738
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.GetObjectData(serializationInfo, streamingContext);
		}

		// Token: 0x060025BB RID: 9659 RVA: 0x00075544 File Offset: 0x00073744
		public void Add(string header)
		{
			if (header == null)
			{
				throw new ArgumentNullException("header");
			}
			int num = header.IndexOf(':');
			if (num == -1)
			{
				throw new ArgumentException("no colon found", "header");
			}
			this.Add(header.Substring(0, num), header.Substring(num + 1));
		}

		// Token: 0x060025BC RID: 9660 RVA: 0x0007559C File Offset: 0x0007379C
		public override void Add(string name, string value)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.internallyCreated && WebHeaderCollection.IsRestricted(name))
			{
				throw new ArgumentException("This header must be modified with the appropiate property.");
			}
			this.AddWithoutValidate(name, value);
		}

		// Token: 0x060025BD RID: 9661 RVA: 0x000755E4 File Offset: 0x000737E4
		protected void AddWithoutValidate(string headerName, string headerValue)
		{
			if (!WebHeaderCollection.IsHeaderName(headerName))
			{
				throw new ArgumentException("invalid header name: " + headerName, "headerName");
			}
			if (headerValue == null)
			{
				headerValue = string.Empty;
			}
			else
			{
				headerValue = headerValue.Trim();
			}
			if (!WebHeaderCollection.IsHeaderValue(headerValue))
			{
				throw new ArgumentException("invalid header value: " + headerValue, "headerValue");
			}
			base.Add(headerName, headerValue);
		}

		// Token: 0x060025BE RID: 9662 RVA: 0x00075658 File Offset: 0x00073858
		public override string[] GetValues(string header)
		{
			if (header == null)
			{
				throw new ArgumentNullException("header");
			}
			string[] values = base.GetValues(header);
			if (values == null || values.Length == 0)
			{
				return null;
			}
			return values;
		}

		// Token: 0x060025BF RID: 9663 RVA: 0x00075690 File Offset: 0x00073890
		public override string[] GetValues(int index)
		{
			string[] values = base.GetValues(index);
			if (values == null || values.Length == 0)
			{
				return null;
			}
			return values;
		}

		// Token: 0x060025C0 RID: 9664 RVA: 0x000756B8 File Offset: 0x000738B8
		public static bool IsRestricted(string headerName)
		{
			if (headerName == null)
			{
				throw new ArgumentNullException("headerName");
			}
			if (headerName == string.Empty)
			{
				throw new ArgumentException("empty string", "headerName");
			}
			if (!WebHeaderCollection.IsHeaderName(headerName))
			{
				throw new ArgumentException("Invalid character in header");
			}
			return WebHeaderCollection.restricted.ContainsKey(headerName);
		}

		// Token: 0x060025C1 RID: 9665 RVA: 0x00075718 File Offset: 0x00073918
		public static bool IsRestricted(string headerName, bool response)
		{
			if (string.IsNullOrEmpty(headerName))
			{
				throw new ArgumentNullException("headerName");
			}
			if (!WebHeaderCollection.IsHeaderName(headerName))
			{
				throw new ArgumentException("Invalid character in header");
			}
			if (response)
			{
				return WebHeaderCollection.restricted_response.ContainsKey(headerName);
			}
			return WebHeaderCollection.restricted.ContainsKey(headerName);
		}

		// Token: 0x060025C2 RID: 9666 RVA: 0x00075770 File Offset: 0x00073970
		public override void OnDeserialization(object sender)
		{
		}

		// Token: 0x060025C3 RID: 9667 RVA: 0x00075774 File Offset: 0x00073974
		public override void Remove(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.internallyCreated && WebHeaderCollection.IsRestricted(name))
			{
				throw new ArgumentException("restricted header");
			}
			base.Remove(name);
		}

		// Token: 0x060025C4 RID: 9668 RVA: 0x000757B0 File Offset: 0x000739B0
		public override void Set(string name, string value)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.internallyCreated && WebHeaderCollection.IsRestricted(name))
			{
				throw new ArgumentException("restricted header");
			}
			if (!WebHeaderCollection.IsHeaderName(name))
			{
				throw new ArgumentException("invalid header name");
			}
			if (value == null)
			{
				value = string.Empty;
			}
			else
			{
				value = value.Trim();
			}
			if (!WebHeaderCollection.IsHeaderValue(value))
			{
				throw new ArgumentException("invalid header value");
			}
			base.Set(name, value);
		}

		// Token: 0x060025C5 RID: 9669 RVA: 0x00075840 File Offset: 0x00073A40
		public byte[] ToByteArray()
		{
			return Encoding.UTF8.GetBytes(this.ToString());
		}

		// Token: 0x060025C6 RID: 9670 RVA: 0x00075854 File Offset: 0x00073A54
		internal string ToStringMultiValue()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int count = base.Count;
			for (int i = 0; i < count; i++)
			{
				string key = this.GetKey(i);
				if (WebHeaderCollection.IsMultiValue(key))
				{
					foreach (string value in this.GetValues(i))
					{
						stringBuilder.Append(key).Append(": ").Append(value).Append("\r\n");
					}
				}
				else
				{
					stringBuilder.Append(key).Append(": ").Append(this.Get(i)).Append("\r\n");
				}
			}
			return stringBuilder.Append("\r\n").ToString();
		}

		// Token: 0x060025C7 RID: 9671 RVA: 0x0007591C File Offset: 0x00073B1C
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int count = base.Count;
			for (int i = 0; i < count; i++)
			{
				stringBuilder.Append(this.GetKey(i)).Append(": ").Append(this.Get(i)).Append("\r\n");
			}
			return stringBuilder.Append("\r\n").ToString();
		}

		// Token: 0x060025C8 RID: 9672 RVA: 0x00075988 File Offset: 0x00073B88
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			int count = base.Count;
			serializationInfo.AddValue("Count", count);
			for (int i = 0; i < count; i++)
			{
				serializationInfo.AddValue(i.ToString(), this.GetKey(i));
				serializationInfo.AddValue((count + i).ToString(), this.Get(i));
			}
		}

		// Token: 0x17000AAF RID: 2735
		// (get) Token: 0x060025C9 RID: 9673 RVA: 0x000759E8 File Offset: 0x00073BE8
		public override string[] AllKeys
		{
			get
			{
				return base.AllKeys;
			}
		}

		// Token: 0x17000AB0 RID: 2736
		// (get) Token: 0x060025CA RID: 9674 RVA: 0x000759F0 File Offset: 0x00073BF0
		public override int Count
		{
			get
			{
				return base.Count;
			}
		}

		// Token: 0x17000AB1 RID: 2737
		// (get) Token: 0x060025CB RID: 9675 RVA: 0x000759F8 File Offset: 0x00073BF8
		public override System.Collections.Specialized.NameObjectCollectionBase.KeysCollection Keys
		{
			get
			{
				return base.Keys;
			}
		}

		// Token: 0x060025CC RID: 9676 RVA: 0x00075A00 File Offset: 0x00073C00
		public override string Get(int index)
		{
			return base.Get(index);
		}

		// Token: 0x060025CD RID: 9677 RVA: 0x00075A0C File Offset: 0x00073C0C
		public override string Get(string name)
		{
			return base.Get(name);
		}

		// Token: 0x060025CE RID: 9678 RVA: 0x00075A18 File Offset: 0x00073C18
		public override string GetKey(int index)
		{
			return base.GetKey(index);
		}

		// Token: 0x060025CF RID: 9679 RVA: 0x00075A24 File Offset: 0x00073C24
		public void Add(HttpRequestHeader header, string value)
		{
			this.Add(this.RequestHeaderToString(header), value);
		}

		// Token: 0x060025D0 RID: 9680 RVA: 0x00075A34 File Offset: 0x00073C34
		public void Remove(HttpRequestHeader header)
		{
			this.Remove(this.RequestHeaderToString(header));
		}

		// Token: 0x060025D1 RID: 9681 RVA: 0x00075A44 File Offset: 0x00073C44
		public void Set(HttpRequestHeader header, string value)
		{
			this.Set(this.RequestHeaderToString(header), value);
		}

		// Token: 0x060025D2 RID: 9682 RVA: 0x00075A54 File Offset: 0x00073C54
		public void Add(HttpResponseHeader header, string value)
		{
			this.Add(this.ResponseHeaderToString(header), value);
		}

		// Token: 0x060025D3 RID: 9683 RVA: 0x00075A64 File Offset: 0x00073C64
		public void Remove(HttpResponseHeader header)
		{
			this.Remove(this.ResponseHeaderToString(header));
		}

		// Token: 0x060025D4 RID: 9684 RVA: 0x00075A74 File Offset: 0x00073C74
		public void Set(HttpResponseHeader header, string value)
		{
			this.Set(this.ResponseHeaderToString(header), value);
		}

		// Token: 0x060025D5 RID: 9685 RVA: 0x00075A84 File Offset: 0x00073C84
		private string RequestHeaderToString(HttpRequestHeader value)
		{
			switch (value)
			{
			case HttpRequestHeader.CacheControl:
				return "cache-control";
			case HttpRequestHeader.Connection:
				return "connection";
			case HttpRequestHeader.Date:
				return "date";
			case HttpRequestHeader.KeepAlive:
				return "keep-alive";
			case HttpRequestHeader.Pragma:
				return "pragma";
			case HttpRequestHeader.Trailer:
				return "trailer";
			case HttpRequestHeader.TransferEncoding:
				return "transfer-encoding";
			case HttpRequestHeader.Upgrade:
				return "upgrade";
			case HttpRequestHeader.Via:
				return "via";
			case HttpRequestHeader.Warning:
				return "warning";
			case HttpRequestHeader.Allow:
				return "allow";
			case HttpRequestHeader.ContentLength:
				return "content-length";
			case HttpRequestHeader.ContentType:
				return "content-type";
			case HttpRequestHeader.ContentEncoding:
				return "content-encoding";
			case HttpRequestHeader.ContentLanguage:
				return "content-language";
			case HttpRequestHeader.ContentLocation:
				return "content-location";
			case HttpRequestHeader.ContentMd5:
				return "content-md5";
			case HttpRequestHeader.ContentRange:
				return "content-range";
			case HttpRequestHeader.Expires:
				return "expires";
			case HttpRequestHeader.LastModified:
				return "last-modified";
			case HttpRequestHeader.Accept:
				return "accept";
			case HttpRequestHeader.AcceptCharset:
				return "accept-charset";
			case HttpRequestHeader.AcceptEncoding:
				return "accept-encoding";
			case HttpRequestHeader.AcceptLanguage:
				return "accept-language";
			case HttpRequestHeader.Authorization:
				return "authorization";
			case HttpRequestHeader.Cookie:
				return "cookie";
			case HttpRequestHeader.Expect:
				return "expect";
			case HttpRequestHeader.From:
				return "from";
			case HttpRequestHeader.Host:
				return "host";
			case HttpRequestHeader.IfMatch:
				return "if-match";
			case HttpRequestHeader.IfModifiedSince:
				return "if-modified-since";
			case HttpRequestHeader.IfNoneMatch:
				return "if-none-match";
			case HttpRequestHeader.IfRange:
				return "if-range";
			case HttpRequestHeader.IfUnmodifiedSince:
				return "if-unmodified-since";
			case HttpRequestHeader.MaxForwards:
				return "max-forwards";
			case HttpRequestHeader.ProxyAuthorization:
				return "proxy-authorization";
			case HttpRequestHeader.Referer:
				return "referer";
			case HttpRequestHeader.Range:
				return "range";
			case HttpRequestHeader.Te:
				return "te";
			case HttpRequestHeader.Translate:
				return "translate";
			case HttpRequestHeader.UserAgent:
				return "user-agent";
			default:
				throw new InvalidOperationException();
			}
		}

		// Token: 0x17000AB2 RID: 2738
		public string this[HttpRequestHeader hrh]
		{
			get
			{
				return this.Get(this.RequestHeaderToString(hrh));
			}
			set
			{
				this.Add(this.RequestHeaderToString(hrh), value);
			}
		}

		// Token: 0x060025D8 RID: 9688 RVA: 0x00075C60 File Offset: 0x00073E60
		private string ResponseHeaderToString(HttpResponseHeader value)
		{
			switch (value)
			{
			case HttpResponseHeader.CacheControl:
				return "cache-control";
			case HttpResponseHeader.Connection:
				return "connection";
			case HttpResponseHeader.Date:
				return "date";
			case HttpResponseHeader.KeepAlive:
				return "keep-alive";
			case HttpResponseHeader.Pragma:
				return "pragma";
			case HttpResponseHeader.Trailer:
				return "trailer";
			case HttpResponseHeader.TransferEncoding:
				return "transfer-encoding";
			case HttpResponseHeader.Upgrade:
				return "upgrade";
			case HttpResponseHeader.Via:
				return "via";
			case HttpResponseHeader.Warning:
				return "warning";
			case HttpResponseHeader.Allow:
				return "allow";
			case HttpResponseHeader.ContentLength:
				return "content-length";
			case HttpResponseHeader.ContentType:
				return "content-type";
			case HttpResponseHeader.ContentEncoding:
				return "content-encoding";
			case HttpResponseHeader.ContentLanguage:
				return "content-language";
			case HttpResponseHeader.ContentLocation:
				return "content-location";
			case HttpResponseHeader.ContentMd5:
				return "content-md5";
			case HttpResponseHeader.ContentRange:
				return "content-range";
			case HttpResponseHeader.Expires:
				return "expires";
			case HttpResponseHeader.LastModified:
				return "last-modified";
			case HttpResponseHeader.AcceptRanges:
				return "accept-ranges";
			case HttpResponseHeader.Age:
				return "age";
			case HttpResponseHeader.ETag:
				return "etag";
			case HttpResponseHeader.Location:
				return "location";
			case HttpResponseHeader.ProxyAuthenticate:
				return "proxy-authenticate";
			case HttpResponseHeader.RetryAfter:
				return "RetryAfter";
			case HttpResponseHeader.Server:
				return "server";
			case HttpResponseHeader.SetCookie:
				return "set-cookie";
			case HttpResponseHeader.Vary:
				return "vary";
			case HttpResponseHeader.WwwAuthenticate:
				return "www-authenticate";
			default:
				throw new InvalidOperationException();
			}
		}

		// Token: 0x17000AB3 RID: 2739
		public string this[HttpResponseHeader hrh]
		{
			get
			{
				return this.Get(this.ResponseHeaderToString(hrh));
			}
			set
			{
				this.Add(this.ResponseHeaderToString(hrh), value);
			}
		}

		// Token: 0x060025DB RID: 9691 RVA: 0x00075DCC File Offset: 0x00073FCC
		public override void Clear()
		{
			base.Clear();
		}

		// Token: 0x060025DC RID: 9692 RVA: 0x00075DD4 File Offset: 0x00073FD4
		public override IEnumerator GetEnumerator()
		{
			return base.GetEnumerator();
		}

		// Token: 0x060025DD RID: 9693 RVA: 0x00075DDC File Offset: 0x00073FDC
		internal void SetInternal(string header)
		{
			int num = header.IndexOf(':');
			if (num == -1)
			{
				throw new ArgumentException("no colon found", "header");
			}
			this.SetInternal(header.Substring(0, num), header.Substring(num + 1));
		}

		// Token: 0x060025DE RID: 9694 RVA: 0x00075E20 File Offset: 0x00074020
		internal void SetInternal(string name, string value)
		{
			if (value == null)
			{
				value = string.Empty;
			}
			else
			{
				value = value.Trim();
			}
			if (!WebHeaderCollection.IsHeaderValue(value))
			{
				throw new ArgumentException("invalid header value");
			}
			if (WebHeaderCollection.IsMultiValue(name))
			{
				base.Add(name, value);
			}
			else
			{
				base.Remove(name);
				base.Set(name, value);
			}
		}

		// Token: 0x060025DF RID: 9695 RVA: 0x00075E84 File Offset: 0x00074084
		internal void RemoveAndAdd(string name, string value)
		{
			if (value == null)
			{
				value = string.Empty;
			}
			else
			{
				value = value.Trim();
			}
			base.Remove(name);
			base.Set(name, value);
		}

		// Token: 0x060025E0 RID: 9696 RVA: 0x00075EB0 File Offset: 0x000740B0
		internal void RemoveInternal(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			base.Remove(name);
		}

		// Token: 0x060025E1 RID: 9697 RVA: 0x00075ECC File Offset: 0x000740CC
		internal static bool IsMultiValue(string headerName)
		{
			return headerName != null && !(headerName == string.Empty) && WebHeaderCollection.multiValue.ContainsKey(headerName);
		}

		// Token: 0x060025E2 RID: 9698 RVA: 0x00075EF4 File Offset: 0x000740F4
		internal static bool IsHeaderValue(string value)
		{
			int length = value.Length;
			for (int i = 0; i < length; i++)
			{
				char c = value[i];
				if (c == '\u007f')
				{
					return false;
				}
				if (c < ' ' && c != '\r' && c != '\n' && c != '\t')
				{
					return false;
				}
				if (c == '\n' && ++i < length)
				{
					c = value[i];
					if (c != ' ' && c != '\t')
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x060025E3 RID: 9699 RVA: 0x00075F7C File Offset: 0x0007417C
		internal static bool IsHeaderName(string name)
		{
			if (name == null || name.Length == 0)
			{
				return false;
			}
			int length = name.Length;
			for (int i = 0; i < length; i++)
			{
				char c = name[i];
				if (c > '~' || !WebHeaderCollection.allowed_chars[(int)c])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04001767 RID: 5991
		private static readonly Hashtable restricted;

		// Token: 0x04001768 RID: 5992
		private static readonly Hashtable multiValue;

		// Token: 0x04001769 RID: 5993
		private static readonly Dictionary<string, bool> restricted_response;

		// Token: 0x0400176A RID: 5994
		private bool internallyCreated;

		// Token: 0x0400176B RID: 5995
		private static bool[] allowed_chars = new bool[]
		{
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true,
			false,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			true,
			true,
			false,
			true,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			true,
			false
		};
	}
}
