﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net
{
	// Token: 0x020002FE RID: 766
	[Serializable]
	public sealed class DnsPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06001A41 RID: 6721 RVA: 0x00049218 File Offset: 0x00047418
		public DnsPermission(PermissionState state)
		{
			this.m_noRestriction = (state == PermissionState.Unrestricted);
		}

		// Token: 0x06001A42 RID: 6722 RVA: 0x0004922C File Offset: 0x0004742C
		public override IPermission Copy()
		{
			return new DnsPermission((!this.m_noRestriction) ? PermissionState.None : PermissionState.Unrestricted);
		}

		// Token: 0x06001A43 RID: 6723 RVA: 0x00049248 File Offset: 0x00047448
		public override IPermission Intersect(IPermission target)
		{
			DnsPermission dnsPermission = this.Cast(target);
			if (dnsPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted() && dnsPermission.IsUnrestricted())
			{
				return new DnsPermission(PermissionState.Unrestricted);
			}
			return null;
		}

		// Token: 0x06001A44 RID: 6724 RVA: 0x00049284 File Offset: 0x00047484
		public override bool IsSubsetOf(IPermission target)
		{
			DnsPermission dnsPermission = this.Cast(target);
			if (dnsPermission == null)
			{
				return this.IsEmpty();
			}
			return dnsPermission.IsUnrestricted() || this.m_noRestriction == dnsPermission.m_noRestriction;
		}

		// Token: 0x06001A45 RID: 6725 RVA: 0x000492C4 File Offset: 0x000474C4
		public bool IsUnrestricted()
		{
			return this.m_noRestriction;
		}

		// Token: 0x06001A46 RID: 6726 RVA: 0x000492CC File Offset: 0x000474CC
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(typeof(DnsPermission), 1);
			if (this.m_noRestriction)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			return securityElement;
		}

		// Token: 0x06001A47 RID: 6727 RVA: 0x00049308 File Offset: 0x00047508
		public override void FromXml(SecurityElement securityElement)
		{
			PermissionHelper.CheckSecurityElement(securityElement, "securityElement", 1, 1);
			if (securityElement.Tag != "IPermission")
			{
				throw new ArgumentException("securityElement");
			}
			this.m_noRestriction = PermissionHelper.IsUnrestricted(securityElement);
		}

		// Token: 0x06001A48 RID: 6728 RVA: 0x00049350 File Offset: 0x00047550
		public override IPermission Union(IPermission target)
		{
			DnsPermission dnsPermission = this.Cast(target);
			if (dnsPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || dnsPermission.IsUnrestricted())
			{
				return new DnsPermission(PermissionState.Unrestricted);
			}
			return new DnsPermission(PermissionState.None);
		}

		// Token: 0x06001A49 RID: 6729 RVA: 0x00049398 File Offset: 0x00047598
		private bool IsEmpty()
		{
			return !this.m_noRestriction;
		}

		// Token: 0x06001A4A RID: 6730 RVA: 0x000493A4 File Offset: 0x000475A4
		private DnsPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			DnsPermission dnsPermission = target as DnsPermission;
			if (dnsPermission == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(DnsPermission));
			}
			return dnsPermission;
		}

		// Token: 0x04001046 RID: 4166
		private const int version = 1;

		// Token: 0x04001047 RID: 4167
		private bool m_noRestriction;
	}
}
