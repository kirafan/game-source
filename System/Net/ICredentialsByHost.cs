﻿using System;

namespace System.Net
{
	// Token: 0x02000329 RID: 809
	public interface ICredentialsByHost
	{
		// Token: 0x06001CA2 RID: 7330
		NetworkCredential GetCredential(string host, int port, string authType);
	}
}
