﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x0200040A RID: 1034
	public class TcpListener
	{
		// Token: 0x0600245B RID: 9307 RVA: 0x0006D4E0 File Offset: 0x0006B6E0
		[Obsolete("Use TcpListener (IPAddress address, int port) instead")]
		public TcpListener(int port)
		{
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.Init(AddressFamily.InterNetwork, new IPEndPoint(IPAddress.Any, port));
		}

		// Token: 0x0600245C RID: 9308 RVA: 0x0006D518 File Offset: 0x0006B718
		public TcpListener(IPEndPoint local_end_point)
		{
			if (local_end_point == null)
			{
				throw new ArgumentNullException("local_end_point");
			}
			this.Init(local_end_point.AddressFamily, local_end_point);
		}

		// Token: 0x0600245D RID: 9309 RVA: 0x0006D54C File Offset: 0x0006B74C
		public TcpListener(IPAddress listen_ip, int port)
		{
			if (listen_ip == null)
			{
				throw new ArgumentNullException("listen_ip");
			}
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.Init(listen_ip.AddressFamily, new IPEndPoint(listen_ip, port));
		}

		// Token: 0x0600245E RID: 9310 RVA: 0x0006D5A0 File Offset: 0x0006B7A0
		private void Init(AddressFamily family, EndPoint ep)
		{
			this.active = false;
			this.server = new Socket(family, SocketType.Stream, ProtocolType.Tcp);
			this.savedEP = ep;
		}

		// Token: 0x17000A70 RID: 2672
		// (get) Token: 0x0600245F RID: 9311 RVA: 0x0006D5C0 File Offset: 0x0006B7C0
		protected bool Active
		{
			get
			{
				return this.active;
			}
		}

		// Token: 0x17000A71 RID: 2673
		// (get) Token: 0x06002460 RID: 9312 RVA: 0x0006D5C8 File Offset: 0x0006B7C8
		public EndPoint LocalEndpoint
		{
			get
			{
				if (this.active)
				{
					return this.server.LocalEndPoint;
				}
				return this.savedEP;
			}
		}

		// Token: 0x17000A72 RID: 2674
		// (get) Token: 0x06002461 RID: 9313 RVA: 0x0006D5E8 File Offset: 0x0006B7E8
		public Socket Server
		{
			get
			{
				return this.server;
			}
		}

		// Token: 0x17000A73 RID: 2675
		// (get) Token: 0x06002462 RID: 9314 RVA: 0x0006D5F0 File Offset: 0x0006B7F0
		// (set) Token: 0x06002463 RID: 9315 RVA: 0x0006D630 File Offset: 0x0006B830
		public bool ExclusiveAddressUse
		{
			get
			{
				if (this.server == null)
				{
					throw new ObjectDisposedException(base.GetType().ToString());
				}
				if (this.active)
				{
					throw new InvalidOperationException("The TcpListener has been started");
				}
				return this.server.ExclusiveAddressUse;
			}
			set
			{
				if (this.server == null)
				{
					throw new ObjectDisposedException(base.GetType().ToString());
				}
				if (this.active)
				{
					throw new InvalidOperationException("The TcpListener has been started");
				}
				this.server.ExclusiveAddressUse = value;
			}
		}

		// Token: 0x06002464 RID: 9316 RVA: 0x0006D67C File Offset: 0x0006B87C
		public Socket AcceptSocket()
		{
			if (!this.active)
			{
				throw new InvalidOperationException("Socket is not listening");
			}
			return this.server.Accept();
		}

		// Token: 0x06002465 RID: 9317 RVA: 0x0006D6A0 File Offset: 0x0006B8A0
		public TcpClient AcceptTcpClient()
		{
			if (!this.active)
			{
				throw new InvalidOperationException("Socket is not listening");
			}
			Socket tcpClient = this.server.Accept();
			TcpClient tcpClient2 = new TcpClient();
			tcpClient2.SetTcpClient(tcpClient);
			return tcpClient2;
		}

		// Token: 0x06002466 RID: 9318 RVA: 0x0006D6E0 File Offset: 0x0006B8E0
		~TcpListener()
		{
			if (this.active)
			{
				this.Stop();
			}
		}

		// Token: 0x06002467 RID: 9319 RVA: 0x0006D728 File Offset: 0x0006B928
		public bool Pending()
		{
			if (!this.active)
			{
				throw new InvalidOperationException("Socket is not listening");
			}
			return this.server.Poll(0, SelectMode.SelectRead);
		}

		// Token: 0x06002468 RID: 9320 RVA: 0x0006D750 File Offset: 0x0006B950
		public void Start()
		{
			this.Start(5);
		}

		// Token: 0x06002469 RID: 9321 RVA: 0x0006D75C File Offset: 0x0006B95C
		public void Start(int backlog)
		{
			if (this.active)
			{
				return;
			}
			if (this.server == null)
			{
				throw new InvalidOperationException("Invalid server socket");
			}
			this.server.Bind(this.savedEP);
			this.server.Listen(backlog);
			this.active = true;
		}

		// Token: 0x0600246A RID: 9322 RVA: 0x0006D7B0 File Offset: 0x0006B9B0
		public IAsyncResult BeginAcceptSocket(AsyncCallback callback, object state)
		{
			if (this.server == null)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			return this.server.BeginAccept(callback, state);
		}

		// Token: 0x0600246B RID: 9323 RVA: 0x0006D7DC File Offset: 0x0006B9DC
		public IAsyncResult BeginAcceptTcpClient(AsyncCallback callback, object state)
		{
			if (this.server == null)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
			return this.server.BeginAccept(callback, state);
		}

		// Token: 0x0600246C RID: 9324 RVA: 0x0006D808 File Offset: 0x0006BA08
		public Socket EndAcceptSocket(IAsyncResult asyncResult)
		{
			return this.server.EndAccept(asyncResult);
		}

		// Token: 0x0600246D RID: 9325 RVA: 0x0006D818 File Offset: 0x0006BA18
		public TcpClient EndAcceptTcpClient(IAsyncResult asyncResult)
		{
			Socket tcpClient = this.server.EndAccept(asyncResult);
			TcpClient tcpClient2 = new TcpClient();
			tcpClient2.SetTcpClient(tcpClient);
			return tcpClient2;
		}

		// Token: 0x0600246E RID: 9326 RVA: 0x0006D840 File Offset: 0x0006BA40
		public void Stop()
		{
			if (this.active)
			{
				this.server.Close();
				this.server = null;
			}
			this.Init(AddressFamily.InterNetwork, this.savedEP);
		}

		// Token: 0x040016C4 RID: 5828
		private bool active;

		// Token: 0x040016C5 RID: 5829
		private Socket server;

		// Token: 0x040016C6 RID: 5830
		private EndPoint savedEP;
	}
}
