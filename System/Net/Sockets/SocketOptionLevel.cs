﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000404 RID: 1028
	public enum SocketOptionLevel
	{
		// Token: 0x04001676 RID: 5750
		Socket = 65535,
		// Token: 0x04001677 RID: 5751
		IP = 0,
		// Token: 0x04001678 RID: 5752
		IPv6 = 41,
		// Token: 0x04001679 RID: 5753
		Tcp = 6,
		// Token: 0x0400167A RID: 5754
		Udp = 17
	}
}
