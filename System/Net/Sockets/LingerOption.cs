﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003EF RID: 1007
	public class LingerOption
	{
		// Token: 0x060022CC RID: 8908 RVA: 0x00066208 File Offset: 0x00064408
		public LingerOption(bool enable, int secs)
		{
			this.enabled = enable;
			this.seconds = secs;
		}

		// Token: 0x17000A14 RID: 2580
		// (get) Token: 0x060022CD RID: 8909 RVA: 0x00066220 File Offset: 0x00064420
		// (set) Token: 0x060022CE RID: 8910 RVA: 0x00066228 File Offset: 0x00064428
		public bool Enabled
		{
			get
			{
				return this.enabled;
			}
			set
			{
				this.enabled = value;
			}
		}

		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x060022CF RID: 8911 RVA: 0x00066234 File Offset: 0x00064434
		// (set) Token: 0x060022D0 RID: 8912 RVA: 0x0006623C File Offset: 0x0006443C
		public int LingerTime
		{
			get
			{
				return this.seconds;
			}
			set
			{
				this.seconds = value;
			}
		}

		// Token: 0x04001580 RID: 5504
		private bool enabled;

		// Token: 0x04001581 RID: 5505
		private int seconds;
	}
}
