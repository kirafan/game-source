﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000408 RID: 1032
	public class TcpClient : IDisposable
	{
		// Token: 0x06002432 RID: 9266 RVA: 0x0006CC9C File Offset: 0x0006AE9C
		public TcpClient()
		{
			this.Init(AddressFamily.InterNetwork);
			this.client.Bind(new IPEndPoint(IPAddress.Any, 0));
		}

		// Token: 0x06002433 RID: 9267 RVA: 0x0006CCC4 File Offset: 0x0006AEC4
		public TcpClient(AddressFamily family)
		{
			if (family != AddressFamily.InterNetwork && family != AddressFamily.InterNetworkV6)
			{
				throw new ArgumentException("Family must be InterNetwork or InterNetworkV6", "family");
			}
			this.Init(family);
			IPAddress address = IPAddress.Any;
			if (family == AddressFamily.InterNetworkV6)
			{
				address = IPAddress.IPv6Any;
			}
			this.client.Bind(new IPEndPoint(address, 0));
		}

		// Token: 0x06002434 RID: 9268 RVA: 0x0006CD24 File Offset: 0x0006AF24
		public TcpClient(IPEndPoint local_end_point)
		{
			this.Init(local_end_point.AddressFamily);
			this.client.Bind(local_end_point);
		}

		// Token: 0x06002435 RID: 9269 RVA: 0x0006CD50 File Offset: 0x0006AF50
		public TcpClient(string hostname, int port)
		{
			this.Connect(hostname, port);
		}

		// Token: 0x06002436 RID: 9270 RVA: 0x0006CD60 File Offset: 0x0006AF60
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002437 RID: 9271 RVA: 0x0006CD70 File Offset: 0x0006AF70
		private void Init(AddressFamily family)
		{
			this.active = false;
			if (this.client != null)
			{
				this.client.Close();
				this.client = null;
			}
			this.client = new Socket(family, SocketType.Stream, ProtocolType.Tcp);
		}

		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x06002438 RID: 9272 RVA: 0x0006CDB0 File Offset: 0x0006AFB0
		// (set) Token: 0x06002439 RID: 9273 RVA: 0x0006CDB8 File Offset: 0x0006AFB8
		protected bool Active
		{
			get
			{
				return this.active;
			}
			set
			{
				this.active = value;
			}
		}

		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x0600243A RID: 9274 RVA: 0x0006CDC4 File Offset: 0x0006AFC4
		// (set) Token: 0x0600243B RID: 9275 RVA: 0x0006CDCC File Offset: 0x0006AFCC
		public Socket Client
		{
			get
			{
				return this.client;
			}
			set
			{
				this.client = value;
				this.stream = null;
			}
		}

		// Token: 0x17000A67 RID: 2663
		// (get) Token: 0x0600243C RID: 9276 RVA: 0x0006CDDC File Offset: 0x0006AFDC
		public int Available
		{
			get
			{
				return this.client.Available;
			}
		}

		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x0600243D RID: 9277 RVA: 0x0006CDEC File Offset: 0x0006AFEC
		public bool Connected
		{
			get
			{
				return this.client.Connected;
			}
		}

		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x0600243E RID: 9278 RVA: 0x0006CDFC File Offset: 0x0006AFFC
		// (set) Token: 0x0600243F RID: 9279 RVA: 0x0006CE0C File Offset: 0x0006B00C
		public bool ExclusiveAddressUse
		{
			get
			{
				return this.client.ExclusiveAddressUse;
			}
			set
			{
				this.client.ExclusiveAddressUse = value;
			}
		}

		// Token: 0x06002440 RID: 9280 RVA: 0x0006CE1C File Offset: 0x0006B01C
		internal void SetTcpClient(Socket s)
		{
			this.Client = s;
		}

		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x06002441 RID: 9281 RVA: 0x0006CE28 File Offset: 0x0006B028
		// (set) Token: 0x06002442 RID: 9282 RVA: 0x0006CE64 File Offset: 0x0006B064
		public LingerOption LingerState
		{
			get
			{
				if ((this.values & TcpClient.Properties.LingerState) != (TcpClient.Properties)0U)
				{
					return this.linger_state;
				}
				return (LingerOption)this.client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.linger_state = value;
					this.values |= TcpClient.Properties.LingerState;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Linger, value);
			}
		}

		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x06002443 RID: 9283 RVA: 0x0006CEB0 File Offset: 0x0006B0B0
		// (set) Token: 0x06002444 RID: 9284 RVA: 0x0006CEE4 File Offset: 0x0006B0E4
		public bool NoDelay
		{
			get
			{
				if ((this.values & TcpClient.Properties.NoDelay) != (TcpClient.Properties)0U)
				{
					return this.no_delay;
				}
				return (bool)this.client.GetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.Debug);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.no_delay = value;
					this.values |= TcpClient.Properties.NoDelay;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.Debug, (!value) ? 0 : 1);
			}
		}

		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x06002445 RID: 9285 RVA: 0x0006CF34 File Offset: 0x0006B134
		// (set) Token: 0x06002446 RID: 9286 RVA: 0x0006CF70 File Offset: 0x0006B170
		public int ReceiveBufferSize
		{
			get
			{
				if ((this.values & TcpClient.Properties.ReceiveBufferSize) != (TcpClient.Properties)0U)
				{
					return this.recv_buffer_size;
				}
				return (int)this.client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.recv_buffer_size = value;
					this.values |= TcpClient.Properties.ReceiveBufferSize;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, value);
			}
		}

		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x06002447 RID: 9287 RVA: 0x0006CFBC File Offset: 0x0006B1BC
		// (set) Token: 0x06002448 RID: 9288 RVA: 0x0006CFF8 File Offset: 0x0006B1F8
		public int ReceiveTimeout
		{
			get
			{
				if ((this.values & TcpClient.Properties.ReceiveTimeout) != (TcpClient.Properties)0U)
				{
					return this.recv_timeout;
				}
				return (int)this.client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.recv_timeout = value;
					this.values |= TcpClient.Properties.ReceiveTimeout;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, value);
			}
		}

		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x06002449 RID: 9289 RVA: 0x0006D044 File Offset: 0x0006B244
		// (set) Token: 0x0600244A RID: 9290 RVA: 0x0006D078 File Offset: 0x0006B278
		public int SendBufferSize
		{
			get
			{
				if ((this.values & TcpClient.Properties.SendBufferSize) != (TcpClient.Properties)0U)
				{
					return this.send_buffer_size;
				}
				return (int)this.client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendBuffer);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.send_buffer_size = value;
					this.values |= TcpClient.Properties.SendBufferSize;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendBuffer, value);
			}
		}

		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x0600244B RID: 9291 RVA: 0x0006D0B8 File Offset: 0x0006B2B8
		// (set) Token: 0x0600244C RID: 9292 RVA: 0x0006D0EC File Offset: 0x0006B2EC
		public int SendTimeout
		{
			get
			{
				if ((this.values & TcpClient.Properties.SendTimeout) != (TcpClient.Properties)0U)
				{
					return this.send_timeout;
				}
				return (int)this.client.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout);
			}
			set
			{
				if (!this.client.Connected)
				{
					this.send_timeout = value;
					this.values |= TcpClient.Properties.SendTimeout;
					return;
				}
				this.client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, value);
			}
		}

		// Token: 0x0600244D RID: 9293 RVA: 0x0006D12C File Offset: 0x0006B32C
		public void Close()
		{
			((IDisposable)this).Dispose();
		}

		// Token: 0x0600244E RID: 9294 RVA: 0x0006D134 File Offset: 0x0006B334
		public void Connect(IPEndPoint remote_end_point)
		{
			try
			{
				this.client.Connect(remote_end_point);
				this.active = true;
			}
			finally
			{
				this.CheckDisposed();
			}
		}

		// Token: 0x0600244F RID: 9295 RVA: 0x0006D17C File Offset: 0x0006B37C
		public void Connect(IPAddress address, int port)
		{
			this.Connect(new IPEndPoint(address, port));
		}

		// Token: 0x06002450 RID: 9296 RVA: 0x0006D18C File Offset: 0x0006B38C
		private void SetOptions()
		{
			TcpClient.Properties properties = this.values;
			this.values = (TcpClient.Properties)0U;
			if ((properties & TcpClient.Properties.LingerState) != (TcpClient.Properties)0U)
			{
				this.LingerState = this.linger_state;
			}
			if ((properties & TcpClient.Properties.NoDelay) != (TcpClient.Properties)0U)
			{
				this.NoDelay = this.no_delay;
			}
			if ((properties & TcpClient.Properties.ReceiveBufferSize) != (TcpClient.Properties)0U)
			{
				this.ReceiveBufferSize = this.recv_buffer_size;
			}
			if ((properties & TcpClient.Properties.ReceiveTimeout) != (TcpClient.Properties)0U)
			{
				this.ReceiveTimeout = this.recv_timeout;
			}
			if ((properties & TcpClient.Properties.SendBufferSize) != (TcpClient.Properties)0U)
			{
				this.SendBufferSize = this.send_buffer_size;
			}
			if ((properties & TcpClient.Properties.SendTimeout) != (TcpClient.Properties)0U)
			{
				this.SendTimeout = this.send_timeout;
			}
		}

		// Token: 0x06002451 RID: 9297 RVA: 0x0006D224 File Offset: 0x0006B424
		public void Connect(string hostname, int port)
		{
			IPAddress[] hostAddresses = Dns.GetHostAddresses(hostname);
			this.Connect(hostAddresses, port);
		}

		// Token: 0x06002452 RID: 9298 RVA: 0x0006D240 File Offset: 0x0006B440
		public void Connect(IPAddress[] ipAddresses, int port)
		{
			this.CheckDisposed();
			if (ipAddresses == null)
			{
				throw new ArgumentNullException("ipAddresses");
			}
			for (int i = 0; i < ipAddresses.Length; i++)
			{
				try
				{
					IPAddress ipaddress = ipAddresses[i];
					if (ipaddress.Equals(IPAddress.Any) || ipaddress.Equals(IPAddress.IPv6Any))
					{
						throw new SocketException(10049);
					}
					this.Init(ipaddress.AddressFamily);
					if (ipaddress.AddressFamily == AddressFamily.InterNetwork)
					{
						this.client.Bind(new IPEndPoint(IPAddress.Any, 0));
					}
					else
					{
						if (ipaddress.AddressFamily != AddressFamily.InterNetworkV6)
						{
							throw new NotSupportedException("This method is only valid for sockets in the InterNetwork and InterNetworkV6 families");
						}
						this.client.Bind(new IPEndPoint(IPAddress.IPv6Any, 0));
					}
					this.Connect(new IPEndPoint(ipaddress, port));
					if (this.values != (TcpClient.Properties)0U)
					{
						this.SetOptions();
					}
					break;
				}
				catch (Exception ex)
				{
					this.Init(AddressFamily.InterNetwork);
					if (i == ipAddresses.Length - 1)
					{
						throw ex;
					}
				}
			}
		}

		// Token: 0x06002453 RID: 9299 RVA: 0x0006D36C File Offset: 0x0006B56C
		public void EndConnect(IAsyncResult asyncResult)
		{
			this.client.EndConnect(asyncResult);
		}

		// Token: 0x06002454 RID: 9300 RVA: 0x0006D37C File Offset: 0x0006B57C
		public IAsyncResult BeginConnect(IPAddress address, int port, AsyncCallback callback, object state)
		{
			return this.client.BeginConnect(address, port, callback, state);
		}

		// Token: 0x06002455 RID: 9301 RVA: 0x0006D390 File Offset: 0x0006B590
		public IAsyncResult BeginConnect(IPAddress[] addresses, int port, AsyncCallback callback, object state)
		{
			return this.client.BeginConnect(addresses, port, callback, state);
		}

		// Token: 0x06002456 RID: 9302 RVA: 0x0006D3A4 File Offset: 0x0006B5A4
		public IAsyncResult BeginConnect(string host, int port, AsyncCallback callback, object state)
		{
			return this.client.BeginConnect(host, port, callback, state);
		}

		// Token: 0x06002457 RID: 9303 RVA: 0x0006D3B8 File Offset: 0x0006B5B8
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			if (disposing)
			{
				NetworkStream networkStream = this.stream;
				this.stream = null;
				if (networkStream != null)
				{
					networkStream.Close();
					this.active = false;
				}
				else if (this.client != null)
				{
					this.client.Close();
					this.client = null;
				}
			}
		}

		// Token: 0x06002458 RID: 9304 RVA: 0x0006D424 File Offset: 0x0006B624
		~TcpClient()
		{
			this.Dispose(false);
		}

		// Token: 0x06002459 RID: 9305 RVA: 0x0006D460 File Offset: 0x0006B660
		public NetworkStream GetStream()
		{
			NetworkStream result;
			try
			{
				if (this.stream == null)
				{
					this.stream = new NetworkStream(this.client, true);
				}
				result = this.stream;
			}
			finally
			{
				this.CheckDisposed();
			}
			return result;
		}

		// Token: 0x0600245A RID: 9306 RVA: 0x0006D4C0 File Offset: 0x0006B6C0
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x040016B2 RID: 5810
		private NetworkStream stream;

		// Token: 0x040016B3 RID: 5811
		private bool active;

		// Token: 0x040016B4 RID: 5812
		private Socket client;

		// Token: 0x040016B5 RID: 5813
		private bool disposed;

		// Token: 0x040016B6 RID: 5814
		private TcpClient.Properties values;

		// Token: 0x040016B7 RID: 5815
		private int recv_timeout;

		// Token: 0x040016B8 RID: 5816
		private int send_timeout;

		// Token: 0x040016B9 RID: 5817
		private int recv_buffer_size;

		// Token: 0x040016BA RID: 5818
		private int send_buffer_size;

		// Token: 0x040016BB RID: 5819
		private LingerOption linger_state;

		// Token: 0x040016BC RID: 5820
		private bool no_delay;

		// Token: 0x02000409 RID: 1033
		private enum Properties : uint
		{
			// Token: 0x040016BE RID: 5822
			LingerState = 1U,
			// Token: 0x040016BF RID: 5823
			NoDelay,
			// Token: 0x040016C0 RID: 5824
			ReceiveBufferSize = 4U,
			// Token: 0x040016C1 RID: 5825
			ReceiveTimeout = 8U,
			// Token: 0x040016C2 RID: 5826
			SendBufferSize = 16U,
			// Token: 0x040016C3 RID: 5827
			SendTimeout = 32U
		}
	}
}
