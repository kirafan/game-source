﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000401 RID: 1025
	[Flags]
	public enum SocketFlags
	{
		// Token: 0x04001664 RID: 5732
		None = 0,
		// Token: 0x04001665 RID: 5733
		OutOfBand = 1,
		// Token: 0x04001666 RID: 5734
		Peek = 2,
		// Token: 0x04001667 RID: 5735
		DontRoute = 4,
		// Token: 0x04001668 RID: 5736
		MaxIOVectorLength = 16,
		// Token: 0x04001669 RID: 5737
		Truncated = 256,
		// Token: 0x0400166A RID: 5738
		ControlDataTruncated = 512,
		// Token: 0x0400166B RID: 5739
		Broadcast = 1024,
		// Token: 0x0400166C RID: 5740
		Multicast = 2048,
		// Token: 0x0400166D RID: 5741
		Partial = 32768
	}
}
