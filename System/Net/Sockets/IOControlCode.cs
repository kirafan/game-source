﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003EC RID: 1004
	public enum IOControlCode : long
	{
		// Token: 0x0400155A RID: 5466
		AbsorbRouterAlert = 2550136837L,
		// Token: 0x0400155B RID: 5467
		AddMulticastGroupOnInterface = 2550136842L,
		// Token: 0x0400155C RID: 5468
		AddressListChange = 671088663L,
		// Token: 0x0400155D RID: 5469
		AddressListQuery = 1207959574L,
		// Token: 0x0400155E RID: 5470
		AddressListSort = 3355443225L,
		// Token: 0x0400155F RID: 5471
		AssociateHandle = 2281701377L,
		// Token: 0x04001560 RID: 5472
		AsyncIO = 2147772029L,
		// Token: 0x04001561 RID: 5473
		BindToInterface = 2550136840L,
		// Token: 0x04001562 RID: 5474
		DataToRead = 1074030207L,
		// Token: 0x04001563 RID: 5475
		DeleteMulticastGroupFromInterface = 2550136843L,
		// Token: 0x04001564 RID: 5476
		EnableCircularQueuing = 671088642L,
		// Token: 0x04001565 RID: 5477
		Flush = 671088644L,
		// Token: 0x04001566 RID: 5478
		GetBroadcastAddress = 1207959557L,
		// Token: 0x04001567 RID: 5479
		GetExtensionFunctionPointer = 3355443206L,
		// Token: 0x04001568 RID: 5480
		GetGroupQos = 3355443208L,
		// Token: 0x04001569 RID: 5481
		GetQos = 3355443207L,
		// Token: 0x0400156A RID: 5482
		KeepAliveValues = 2550136836L,
		// Token: 0x0400156B RID: 5483
		LimitBroadcasts = 2550136839L,
		// Token: 0x0400156C RID: 5484
		MulticastInterface = 2550136841L,
		// Token: 0x0400156D RID: 5485
		MulticastScope = 2281701386L,
		// Token: 0x0400156E RID: 5486
		MultipointLoopback = 2281701385L,
		// Token: 0x0400156F RID: 5487
		NamespaceChange = 2281701401L,
		// Token: 0x04001570 RID: 5488
		NonBlockingIO = 2147772030L,
		// Token: 0x04001571 RID: 5489
		OobDataRead = 1074033415L,
		// Token: 0x04001572 RID: 5490
		QueryTargetPnpHandle = 1207959576L,
		// Token: 0x04001573 RID: 5491
		ReceiveAll = 2550136833L,
		// Token: 0x04001574 RID: 5492
		ReceiveAllIgmpMulticast = 2550136835L,
		// Token: 0x04001575 RID: 5493
		ReceiveAllMulticast = 2550136834L,
		// Token: 0x04001576 RID: 5494
		RoutingInterfaceChange = 2281701397L,
		// Token: 0x04001577 RID: 5495
		RoutingInterfaceQuery = 3355443220L,
		// Token: 0x04001578 RID: 5496
		SetGroupQos = 2281701388L,
		// Token: 0x04001579 RID: 5497
		SetQos = 2281701387L,
		// Token: 0x0400157A RID: 5498
		TranslateHandle = 3355443213L,
		// Token: 0x0400157B RID: 5499
		UnicastInterface = 2550136838L
	}
}
