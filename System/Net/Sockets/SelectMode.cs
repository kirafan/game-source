﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003F4 RID: 1012
	public enum SelectMode
	{
		// Token: 0x040015C6 RID: 5574
		SelectRead,
		// Token: 0x040015C7 RID: 5575
		SelectWrite,
		// Token: 0x040015C8 RID: 5576
		SelectError
	}
}
