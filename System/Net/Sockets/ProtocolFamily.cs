﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003F2 RID: 1010
	public enum ProtocolFamily
	{
		// Token: 0x0400158C RID: 5516
		Unknown = -1,
		// Token: 0x0400158D RID: 5517
		Unspecified,
		// Token: 0x0400158E RID: 5518
		Unix,
		// Token: 0x0400158F RID: 5519
		InterNetwork,
		// Token: 0x04001590 RID: 5520
		ImpLink,
		// Token: 0x04001591 RID: 5521
		Pup,
		// Token: 0x04001592 RID: 5522
		Chaos,
		// Token: 0x04001593 RID: 5523
		Ipx,
		// Token: 0x04001594 RID: 5524
		Iso,
		// Token: 0x04001595 RID: 5525
		Ecma,
		// Token: 0x04001596 RID: 5526
		DataKit,
		// Token: 0x04001597 RID: 5527
		Ccitt,
		// Token: 0x04001598 RID: 5528
		Sna,
		// Token: 0x04001599 RID: 5529
		DecNet,
		// Token: 0x0400159A RID: 5530
		DataLink,
		// Token: 0x0400159B RID: 5531
		Lat,
		// Token: 0x0400159C RID: 5532
		HyperChannel,
		// Token: 0x0400159D RID: 5533
		AppleTalk,
		// Token: 0x0400159E RID: 5534
		NetBios,
		// Token: 0x0400159F RID: 5535
		VoiceView,
		// Token: 0x040015A0 RID: 5536
		FireFox,
		// Token: 0x040015A1 RID: 5537
		Banyan = 21,
		// Token: 0x040015A2 RID: 5538
		Atm,
		// Token: 0x040015A3 RID: 5539
		InterNetworkV6,
		// Token: 0x040015A4 RID: 5540
		Cluster,
		// Token: 0x040015A5 RID: 5541
		Ieee12844,
		// Token: 0x040015A6 RID: 5542
		Irda,
		// Token: 0x040015A7 RID: 5543
		NetworkDesigners = 28,
		// Token: 0x040015A8 RID: 5544
		Max,
		// Token: 0x040015A9 RID: 5545
		NS = 6,
		// Token: 0x040015AA RID: 5546
		Osi
	}
}
