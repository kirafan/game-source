﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000402 RID: 1026
	[Serializable]
	public struct SocketInformation
	{
		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x0600242E RID: 9262 RVA: 0x0006CC74 File Offset: 0x0006AE74
		// (set) Token: 0x0600242F RID: 9263 RVA: 0x0006CC7C File Offset: 0x0006AE7C
		public SocketInformationOptions Options
		{
			get
			{
				return this.options;
			}
			set
			{
				this.options = value;
			}
		}

		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x06002430 RID: 9264 RVA: 0x0006CC88 File Offset: 0x0006AE88
		// (set) Token: 0x06002431 RID: 9265 RVA: 0x0006CC90 File Offset: 0x0006AE90
		public byte[] ProtocolInformation
		{
			get
			{
				return this.protocol_info;
			}
			set
			{
				this.protocol_info = value;
			}
		}

		// Token: 0x0400166E RID: 5742
		private SocketInformationOptions options;

		// Token: 0x0400166F RID: 5743
		private byte[] protocol_info;
	}
}
