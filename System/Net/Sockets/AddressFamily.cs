﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003EB RID: 1003
	public enum AddressFamily
	{
		// Token: 0x0400153A RID: 5434
		Unknown = -1,
		// Token: 0x0400153B RID: 5435
		Unspecified,
		// Token: 0x0400153C RID: 5436
		Unix,
		// Token: 0x0400153D RID: 5437
		InterNetwork,
		// Token: 0x0400153E RID: 5438
		ImpLink,
		// Token: 0x0400153F RID: 5439
		Pup,
		// Token: 0x04001540 RID: 5440
		Chaos,
		// Token: 0x04001541 RID: 5441
		NS,
		// Token: 0x04001542 RID: 5442
		Ipx = 6,
		// Token: 0x04001543 RID: 5443
		Iso,
		// Token: 0x04001544 RID: 5444
		Osi = 7,
		// Token: 0x04001545 RID: 5445
		Ecma,
		// Token: 0x04001546 RID: 5446
		DataKit,
		// Token: 0x04001547 RID: 5447
		Ccitt,
		// Token: 0x04001548 RID: 5448
		Sna,
		// Token: 0x04001549 RID: 5449
		DecNet,
		// Token: 0x0400154A RID: 5450
		DataLink,
		// Token: 0x0400154B RID: 5451
		Lat,
		// Token: 0x0400154C RID: 5452
		HyperChannel,
		// Token: 0x0400154D RID: 5453
		AppleTalk,
		// Token: 0x0400154E RID: 5454
		NetBios,
		// Token: 0x0400154F RID: 5455
		VoiceView,
		// Token: 0x04001550 RID: 5456
		FireFox,
		// Token: 0x04001551 RID: 5457
		Banyan = 21,
		// Token: 0x04001552 RID: 5458
		Atm,
		// Token: 0x04001553 RID: 5459
		InterNetworkV6,
		// Token: 0x04001554 RID: 5460
		Cluster,
		// Token: 0x04001555 RID: 5461
		Ieee12844,
		// Token: 0x04001556 RID: 5462
		Irda,
		// Token: 0x04001557 RID: 5463
		NetworkDesigners = 28,
		// Token: 0x04001558 RID: 5464
		Max
	}
}
