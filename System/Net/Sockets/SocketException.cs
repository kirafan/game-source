﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace System.Net.Sockets
{
	// Token: 0x02000400 RID: 1024
	[Serializable]
	public class SocketException : System.ComponentModel.Win32Exception
	{
		// Token: 0x06002426 RID: 9254 RVA: 0x0006CC28 File Offset: 0x0006AE28
		public SocketException() : base(SocketException.WSAGetLastError_internal())
		{
		}

		// Token: 0x06002427 RID: 9255 RVA: 0x0006CC38 File Offset: 0x0006AE38
		public SocketException(int error) : base(error)
		{
		}

		// Token: 0x06002428 RID: 9256 RVA: 0x0006CC44 File Offset: 0x0006AE44
		protected SocketException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002429 RID: 9257 RVA: 0x0006CC50 File Offset: 0x0006AE50
		internal SocketException(int error, string message) : base(error, message)
		{
		}

		// Token: 0x0600242A RID: 9258
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int WSAGetLastError_internal();

		// Token: 0x17000A60 RID: 2656
		// (get) Token: 0x0600242B RID: 9259 RVA: 0x0006CC5C File Offset: 0x0006AE5C
		public override int ErrorCode
		{
			get
			{
				return base.NativeErrorCode;
			}
		}

		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x0600242C RID: 9260 RVA: 0x0006CC64 File Offset: 0x0006AE64
		public SocketError SocketErrorCode
		{
			get
			{
				return (SocketError)base.NativeErrorCode;
			}
		}

		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x0600242D RID: 9261 RVA: 0x0006CC6C File Offset: 0x0006AE6C
		public override string Message
		{
			get
			{
				return base.Message;
			}
		}
	}
}
