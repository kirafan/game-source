﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000405 RID: 1029
	public enum SocketOptionName
	{
		// Token: 0x0400167C RID: 5756
		Debug = 1,
		// Token: 0x0400167D RID: 5757
		AcceptConnection,
		// Token: 0x0400167E RID: 5758
		ReuseAddress = 4,
		// Token: 0x0400167F RID: 5759
		KeepAlive = 8,
		// Token: 0x04001680 RID: 5760
		DontRoute = 16,
		// Token: 0x04001681 RID: 5761
		Broadcast = 32,
		// Token: 0x04001682 RID: 5762
		UseLoopback = 64,
		// Token: 0x04001683 RID: 5763
		Linger = 128,
		// Token: 0x04001684 RID: 5764
		OutOfBandInline = 256,
		// Token: 0x04001685 RID: 5765
		DontLinger = -129,
		// Token: 0x04001686 RID: 5766
		ExclusiveAddressUse = -5,
		// Token: 0x04001687 RID: 5767
		SendBuffer = 4097,
		// Token: 0x04001688 RID: 5768
		ReceiveBuffer,
		// Token: 0x04001689 RID: 5769
		SendLowWater,
		// Token: 0x0400168A RID: 5770
		ReceiveLowWater,
		// Token: 0x0400168B RID: 5771
		SendTimeout,
		// Token: 0x0400168C RID: 5772
		ReceiveTimeout,
		// Token: 0x0400168D RID: 5773
		Error,
		// Token: 0x0400168E RID: 5774
		Type,
		// Token: 0x0400168F RID: 5775
		MaxConnections = 2147483647,
		// Token: 0x04001690 RID: 5776
		IPOptions = 1,
		// Token: 0x04001691 RID: 5777
		HeaderIncluded,
		// Token: 0x04001692 RID: 5778
		TypeOfService,
		// Token: 0x04001693 RID: 5779
		IpTimeToLive,
		// Token: 0x04001694 RID: 5780
		MulticastInterface = 9,
		// Token: 0x04001695 RID: 5781
		MulticastTimeToLive,
		// Token: 0x04001696 RID: 5782
		MulticastLoopback,
		// Token: 0x04001697 RID: 5783
		AddMembership,
		// Token: 0x04001698 RID: 5784
		DropMembership,
		// Token: 0x04001699 RID: 5785
		DontFragment,
		// Token: 0x0400169A RID: 5786
		AddSourceMembership,
		// Token: 0x0400169B RID: 5787
		DropSourceMembership,
		// Token: 0x0400169C RID: 5788
		BlockSource,
		// Token: 0x0400169D RID: 5789
		UnblockSource,
		// Token: 0x0400169E RID: 5790
		PacketInformation,
		// Token: 0x0400169F RID: 5791
		NoDelay = 1,
		// Token: 0x040016A0 RID: 5792
		BsdUrgent,
		// Token: 0x040016A1 RID: 5793
		Expedited = 2,
		// Token: 0x040016A2 RID: 5794
		NoChecksum = 1,
		// Token: 0x040016A3 RID: 5795
		ChecksumCoverage = 20,
		// Token: 0x040016A4 RID: 5796
		HopLimit,
		// Token: 0x040016A5 RID: 5797
		UpdateAcceptContext = 28683,
		// Token: 0x040016A6 RID: 5798
		UpdateConnectContext = 28688
	}
}
