﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003FE RID: 1022
	public enum SocketAsyncOperation
	{
		// Token: 0x04001629 RID: 5673
		None,
		// Token: 0x0400162A RID: 5674
		Accept,
		// Token: 0x0400162B RID: 5675
		Connect,
		// Token: 0x0400162C RID: 5676
		Disconnect,
		// Token: 0x0400162D RID: 5677
		Receive,
		// Token: 0x0400162E RID: 5678
		ReceiveFrom,
		// Token: 0x0400162F RID: 5679
		ReceiveMessageFrom,
		// Token: 0x04001630 RID: 5680
		Send,
		// Token: 0x04001631 RID: 5681
		SendPackets,
		// Token: 0x04001632 RID: 5682
		SendTo
	}
}
