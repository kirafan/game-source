﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x0200040C RID: 1036
	public class UdpClient : IDisposable
	{
		// Token: 0x0600246F RID: 9327 RVA: 0x0006D878 File Offset: 0x0006BA78
		public UdpClient() : this(AddressFamily.InterNetwork)
		{
		}

		// Token: 0x06002470 RID: 9328 RVA: 0x0006D884 File Offset: 0x0006BA84
		public UdpClient(AddressFamily family)
		{
			this.family = AddressFamily.InterNetwork;
			base..ctor();
			if (family != AddressFamily.InterNetwork && family != AddressFamily.InterNetworkV6)
			{
				throw new ArgumentException("Family must be InterNetwork or InterNetworkV6", "family");
			}
			this.family = family;
			this.InitSocket(null);
		}

		// Token: 0x06002471 RID: 9329 RVA: 0x0006D8CC File Offset: 0x0006BACC
		public UdpClient(int port)
		{
			this.family = AddressFamily.InterNetwork;
			base..ctor();
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.family = AddressFamily.InterNetwork;
			IPEndPoint localEP = new IPEndPoint(IPAddress.Any, port);
			this.InitSocket(localEP);
		}

		// Token: 0x06002472 RID: 9330 RVA: 0x0006D920 File Offset: 0x0006BB20
		public UdpClient(IPEndPoint localEP)
		{
			this.family = AddressFamily.InterNetwork;
			base..ctor();
			if (localEP == null)
			{
				throw new ArgumentNullException("localEP");
			}
			this.family = localEP.AddressFamily;
			this.InitSocket(localEP);
		}

		// Token: 0x06002473 RID: 9331 RVA: 0x0006D954 File Offset: 0x0006BB54
		public UdpClient(int port, AddressFamily family)
		{
			this.family = AddressFamily.InterNetwork;
			base..ctor();
			if (family != AddressFamily.InterNetwork && family != AddressFamily.InterNetworkV6)
			{
				throw new ArgumentException("Family must be InterNetwork or InterNetworkV6", "family");
			}
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.family = family;
			IPEndPoint localEP;
			if (family == AddressFamily.InterNetwork)
			{
				localEP = new IPEndPoint(IPAddress.Any, port);
			}
			else
			{
				localEP = new IPEndPoint(IPAddress.IPv6Any, port);
			}
			this.InitSocket(localEP);
		}

		// Token: 0x06002474 RID: 9332 RVA: 0x0006D9DC File Offset: 0x0006BBDC
		public UdpClient(string hostname, int port)
		{
			this.family = AddressFamily.InterNetwork;
			base..ctor();
			if (hostname == null)
			{
				throw new ArgumentNullException("hostname");
			}
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.InitSocket(null);
			this.Connect(hostname, port);
		}

		// Token: 0x06002475 RID: 9333 RVA: 0x0006DA34 File Offset: 0x0006BC34
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002476 RID: 9334 RVA: 0x0006DA44 File Offset: 0x0006BC44
		private void InitSocket(EndPoint localEP)
		{
			if (this.socket != null)
			{
				this.socket.Close();
				this.socket = null;
			}
			this.socket = new Socket(this.family, SocketType.Dgram, ProtocolType.Udp);
			if (localEP != null)
			{
				this.socket.Bind(localEP);
			}
		}

		// Token: 0x06002477 RID: 9335 RVA: 0x0006DA94 File Offset: 0x0006BC94
		public void Close()
		{
			((IDisposable)this).Dispose();
		}

		// Token: 0x06002478 RID: 9336 RVA: 0x0006DA9C File Offset: 0x0006BC9C
		private void DoConnect(IPEndPoint endPoint)
		{
			try
			{
				this.socket.Connect(endPoint);
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode != 10013)
				{
					throw;
				}
				this.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
				this.socket.Connect(endPoint);
			}
		}

		// Token: 0x06002479 RID: 9337 RVA: 0x0006DB14 File Offset: 0x0006BD14
		public void Connect(IPEndPoint endPoint)
		{
			this.CheckDisposed();
			if (endPoint == null)
			{
				throw new ArgumentNullException("endPoint");
			}
			this.DoConnect(endPoint);
			this.active = true;
		}

		// Token: 0x0600247A RID: 9338 RVA: 0x0006DB3C File Offset: 0x0006BD3C
		public void Connect(IPAddress addr, int port)
		{
			if (addr == null)
			{
				throw new ArgumentNullException("addr");
			}
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			this.Connect(new IPEndPoint(addr, port));
		}

		// Token: 0x0600247B RID: 9339 RVA: 0x0006DB7C File Offset: 0x0006BD7C
		public void Connect(string hostname, int port)
		{
			if (port < 0 || port > 65535)
			{
				throw new ArgumentOutOfRangeException("port");
			}
			IPAddress[] hostAddresses = Dns.GetHostAddresses(hostname);
			for (int i = 0; i < hostAddresses.Length; i++)
			{
				try
				{
					this.family = hostAddresses[i].AddressFamily;
					this.Connect(new IPEndPoint(hostAddresses[i], port));
					break;
				}
				catch (Exception ex)
				{
					if (i == hostAddresses.Length - 1)
					{
						if (this.socket != null)
						{
							this.socket.Close();
							this.socket = null;
						}
						throw ex;
					}
				}
			}
		}

		// Token: 0x0600247C RID: 9340 RVA: 0x0006DC34 File Offset: 0x0006BE34
		public void DropMulticastGroup(IPAddress multicastAddr)
		{
			this.CheckDisposed();
			if (multicastAddr == null)
			{
				throw new ArgumentNullException("multicastAddr");
			}
			if (this.family == AddressFamily.InterNetwork)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.DropMembership, new MulticastOption(multicastAddr));
			}
			else
			{
				this.socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.DropMembership, new IPv6MulticastOption(multicastAddr));
			}
		}

		// Token: 0x0600247D RID: 9341 RVA: 0x0006DC94 File Offset: 0x0006BE94
		public void DropMulticastGroup(IPAddress multicastAddr, int ifindex)
		{
			this.CheckDisposed();
			if (multicastAddr == null)
			{
				throw new ArgumentNullException("multicastAddr");
			}
			if (this.family == AddressFamily.InterNetworkV6)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.DropMembership, new IPv6MulticastOption(multicastAddr, (long)ifindex));
			}
		}

		// Token: 0x0600247E RID: 9342 RVA: 0x0006DCD4 File Offset: 0x0006BED4
		public void JoinMulticastGroup(IPAddress multicastAddr)
		{
			this.CheckDisposed();
			if (multicastAddr == null)
			{
				throw new ArgumentNullException("multicastAddr");
			}
			if (this.family == AddressFamily.InterNetwork)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(multicastAddr));
			}
			else
			{
				this.socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.AddMembership, new IPv6MulticastOption(multicastAddr));
			}
		}

		// Token: 0x0600247F RID: 9343 RVA: 0x0006DD34 File Offset: 0x0006BF34
		public void JoinMulticastGroup(int ifindex, IPAddress multicastAddr)
		{
			this.CheckDisposed();
			if (multicastAddr == null)
			{
				throw new ArgumentNullException("multicastAddr");
			}
			if (this.family == AddressFamily.InterNetworkV6)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.AddMembership, new IPv6MulticastOption(multicastAddr, (long)ifindex));
				return;
			}
			throw new SocketException(10045);
		}

		// Token: 0x06002480 RID: 9344 RVA: 0x0006DD8C File Offset: 0x0006BF8C
		public void JoinMulticastGroup(IPAddress multicastAddr, int timeToLive)
		{
			this.CheckDisposed();
			if (multicastAddr == null)
			{
				throw new ArgumentNullException("multicastAddr");
			}
			if (timeToLive < 0 || timeToLive > 255)
			{
				throw new ArgumentOutOfRangeException("timeToLive");
			}
			this.JoinMulticastGroup(multicastAddr);
			if (this.family == AddressFamily.InterNetwork)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, timeToLive);
			}
			else
			{
				this.socket.SetSocketOption(SocketOptionLevel.IPv6, SocketOptionName.MulticastTimeToLive, timeToLive);
			}
		}

		// Token: 0x06002481 RID: 9345 RVA: 0x0006DE04 File Offset: 0x0006C004
		public void JoinMulticastGroup(IPAddress multicastAddr, IPAddress localAddress)
		{
			this.CheckDisposed();
			if (this.family == AddressFamily.InterNetwork)
			{
				this.socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(multicastAddr, localAddress));
				return;
			}
			throw new SocketException(10045);
		}

		// Token: 0x06002482 RID: 9346 RVA: 0x0006DE40 File Offset: 0x0006C040
		public byte[] Receive(ref IPEndPoint remoteEP)
		{
			this.CheckDisposed();
			byte[] array = new byte[65536];
			EndPoint endPoint;
			if (this.family == AddressFamily.InterNetwork)
			{
				endPoint = new IPEndPoint(IPAddress.Any, 0);
			}
			else
			{
				endPoint = new IPEndPoint(IPAddress.IPv6Any, 0);
			}
			int num = this.socket.ReceiveFrom(array, ref endPoint);
			if (num < array.Length)
			{
				array = this.CutArray(array, num);
			}
			remoteEP = (IPEndPoint)endPoint;
			return array;
		}

		// Token: 0x06002483 RID: 9347 RVA: 0x0006DEB4 File Offset: 0x0006C0B4
		private int DoSend(byte[] dgram, int bytes, IPEndPoint endPoint)
		{
			int result;
			try
			{
				if (endPoint == null)
				{
					result = this.socket.Send(dgram, 0, bytes, SocketFlags.None);
				}
				else
				{
					result = this.socket.SendTo(dgram, 0, bytes, SocketFlags.None, endPoint);
				}
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode != 10013)
				{
					throw;
				}
				this.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
				if (endPoint == null)
				{
					result = this.socket.Send(dgram, 0, bytes, SocketFlags.None);
				}
				else
				{
					result = this.socket.SendTo(dgram, 0, bytes, SocketFlags.None, endPoint);
				}
			}
			return result;
		}

		// Token: 0x06002484 RID: 9348 RVA: 0x0006DF70 File Offset: 0x0006C170
		public int Send(byte[] dgram, int bytes)
		{
			this.CheckDisposed();
			if (dgram == null)
			{
				throw new ArgumentNullException("dgram");
			}
			if (!this.active)
			{
				throw new InvalidOperationException("Operation not allowed on non-connected sockets.");
			}
			return this.DoSend(dgram, bytes, null);
		}

		// Token: 0x06002485 RID: 9349 RVA: 0x0006DFB4 File Offset: 0x0006C1B4
		public int Send(byte[] dgram, int bytes, IPEndPoint endPoint)
		{
			this.CheckDisposed();
			if (dgram == null)
			{
				throw new ArgumentNullException("dgram is null");
			}
			if (!this.active)
			{
				return this.DoSend(dgram, bytes, endPoint);
			}
			if (endPoint != null)
			{
				throw new InvalidOperationException("Cannot send packets to an arbitrary host while connected.");
			}
			return this.DoSend(dgram, bytes, null);
		}

		// Token: 0x06002486 RID: 9350 RVA: 0x0006E008 File Offset: 0x0006C208
		public int Send(byte[] dgram, int bytes, string hostname, int port)
		{
			return this.Send(dgram, bytes, new IPEndPoint(Dns.GetHostAddresses(hostname)[0], port));
		}

		// Token: 0x06002487 RID: 9351 RVA: 0x0006E024 File Offset: 0x0006C224
		private byte[] CutArray(byte[] orig, int length)
		{
			byte[] array = new byte[length];
			Buffer.BlockCopy(orig, 0, array, 0, length);
			return array;
		}

		// Token: 0x06002488 RID: 9352 RVA: 0x0006E044 File Offset: 0x0006C244
		private IAsyncResult DoBeginSend(byte[] datagram, int bytes, IPEndPoint endPoint, AsyncCallback requestCallback, object state)
		{
			IAsyncResult result;
			try
			{
				if (endPoint == null)
				{
					result = this.socket.BeginSend(datagram, 0, bytes, SocketFlags.None, requestCallback, state);
				}
				else
				{
					result = this.socket.BeginSendTo(datagram, 0, bytes, SocketFlags.None, endPoint, requestCallback, state);
				}
			}
			catch (SocketException ex)
			{
				if (ex.ErrorCode != 10013)
				{
					throw;
				}
				this.socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
				if (endPoint == null)
				{
					result = this.socket.BeginSend(datagram, 0, bytes, SocketFlags.None, requestCallback, state);
				}
				else
				{
					result = this.socket.BeginSendTo(datagram, 0, bytes, SocketFlags.None, endPoint, requestCallback, state);
				}
			}
			return result;
		}

		// Token: 0x06002489 RID: 9353 RVA: 0x0006E110 File Offset: 0x0006C310
		public IAsyncResult BeginSend(byte[] datagram, int bytes, AsyncCallback requestCallback, object state)
		{
			return this.BeginSend(datagram, bytes, null, requestCallback, state);
		}

		// Token: 0x0600248A RID: 9354 RVA: 0x0006E120 File Offset: 0x0006C320
		public IAsyncResult BeginSend(byte[] datagram, int bytes, IPEndPoint endPoint, AsyncCallback requestCallback, object state)
		{
			this.CheckDisposed();
			if (datagram == null)
			{
				throw new ArgumentNullException("datagram");
			}
			return this.DoBeginSend(datagram, bytes, endPoint, requestCallback, state);
		}

		// Token: 0x0600248B RID: 9355 RVA: 0x0006E154 File Offset: 0x0006C354
		public IAsyncResult BeginSend(byte[] datagram, int bytes, string hostname, int port, AsyncCallback requestCallback, object state)
		{
			return this.BeginSend(datagram, bytes, new IPEndPoint(Dns.GetHostAddresses(hostname)[0], port), requestCallback, state);
		}

		// Token: 0x0600248C RID: 9356 RVA: 0x0006E174 File Offset: 0x0006C374
		public int EndSend(IAsyncResult asyncResult)
		{
			this.CheckDisposed();
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult is a null reference");
			}
			return this.socket.EndSend(asyncResult);
		}

		// Token: 0x0600248D RID: 9357 RVA: 0x0006E19C File Offset: 0x0006C39C
		public IAsyncResult BeginReceive(AsyncCallback callback, object state)
		{
			this.CheckDisposed();
			this.recvbuffer = new byte[8192];
			EndPoint endPoint;
			if (this.family == AddressFamily.InterNetwork)
			{
				endPoint = new IPEndPoint(IPAddress.Any, 0);
			}
			else
			{
				endPoint = new IPEndPoint(IPAddress.IPv6Any, 0);
			}
			return this.socket.BeginReceiveFrom(this.recvbuffer, 0, 8192, SocketFlags.None, ref endPoint, callback, state);
		}

		// Token: 0x0600248E RID: 9358 RVA: 0x0006E204 File Offset: 0x0006C404
		public byte[] EndReceive(IAsyncResult asyncResult, ref IPEndPoint remoteEP)
		{
			this.CheckDisposed();
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult is a null reference");
			}
			EndPoint endPoint;
			if (this.family == AddressFamily.InterNetwork)
			{
				endPoint = new IPEndPoint(IPAddress.Any, 0);
			}
			else
			{
				endPoint = new IPEndPoint(IPAddress.IPv6Any, 0);
			}
			int num = this.socket.EndReceiveFrom(asyncResult, ref endPoint);
			remoteEP = (IPEndPoint)endPoint;
			byte[] array = new byte[num];
			Array.Copy(this.recvbuffer, array, num);
			return array;
		}

		// Token: 0x17000A74 RID: 2676
		// (get) Token: 0x0600248F RID: 9359 RVA: 0x0006E280 File Offset: 0x0006C480
		// (set) Token: 0x06002490 RID: 9360 RVA: 0x0006E288 File Offset: 0x0006C488
		protected bool Active
		{
			get
			{
				return this.active;
			}
			set
			{
				this.active = value;
			}
		}

		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x06002491 RID: 9361 RVA: 0x0006E294 File Offset: 0x0006C494
		// (set) Token: 0x06002492 RID: 9362 RVA: 0x0006E29C File Offset: 0x0006C49C
		public Socket Client
		{
			get
			{
				return this.socket;
			}
			set
			{
				this.socket = value;
			}
		}

		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x06002493 RID: 9363 RVA: 0x0006E2A8 File Offset: 0x0006C4A8
		public int Available
		{
			get
			{
				return this.socket.Available;
			}
		}

		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x06002494 RID: 9364 RVA: 0x0006E2B8 File Offset: 0x0006C4B8
		// (set) Token: 0x06002495 RID: 9365 RVA: 0x0006E2C8 File Offset: 0x0006C4C8
		public bool DontFragment
		{
			get
			{
				return this.socket.DontFragment;
			}
			set
			{
				this.socket.DontFragment = value;
			}
		}

		// Token: 0x17000A78 RID: 2680
		// (get) Token: 0x06002496 RID: 9366 RVA: 0x0006E2D8 File Offset: 0x0006C4D8
		// (set) Token: 0x06002497 RID: 9367 RVA: 0x0006E2E8 File Offset: 0x0006C4E8
		public bool EnableBroadcast
		{
			get
			{
				return this.socket.EnableBroadcast;
			}
			set
			{
				this.socket.EnableBroadcast = value;
			}
		}

		// Token: 0x17000A79 RID: 2681
		// (get) Token: 0x06002498 RID: 9368 RVA: 0x0006E2F8 File Offset: 0x0006C4F8
		// (set) Token: 0x06002499 RID: 9369 RVA: 0x0006E308 File Offset: 0x0006C508
		public bool ExclusiveAddressUse
		{
			get
			{
				return this.socket.ExclusiveAddressUse;
			}
			set
			{
				this.socket.ExclusiveAddressUse = value;
			}
		}

		// Token: 0x17000A7A RID: 2682
		// (get) Token: 0x0600249A RID: 9370 RVA: 0x0006E318 File Offset: 0x0006C518
		// (set) Token: 0x0600249B RID: 9371 RVA: 0x0006E328 File Offset: 0x0006C528
		public bool MulticastLoopback
		{
			get
			{
				return this.socket.MulticastLoopback;
			}
			set
			{
				this.socket.MulticastLoopback = value;
			}
		}

		// Token: 0x17000A7B RID: 2683
		// (get) Token: 0x0600249C RID: 9372 RVA: 0x0006E338 File Offset: 0x0006C538
		// (set) Token: 0x0600249D RID: 9373 RVA: 0x0006E348 File Offset: 0x0006C548
		public short Ttl
		{
			get
			{
				return this.socket.Ttl;
			}
			set
			{
				this.socket.Ttl = value;
			}
		}

		// Token: 0x0600249E RID: 9374 RVA: 0x0006E358 File Offset: 0x0006C558
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			if (disposing)
			{
				if (this.socket != null)
				{
					this.socket.Close();
				}
				this.socket = null;
			}
		}

		// Token: 0x0600249F RID: 9375 RVA: 0x0006E39C File Offset: 0x0006C59C
		~UdpClient()
		{
			this.Dispose(false);
		}

		// Token: 0x060024A0 RID: 9376 RVA: 0x0006E3D8 File Offset: 0x0006C5D8
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x040016CE RID: 5838
		private bool disposed;

		// Token: 0x040016CF RID: 5839
		private bool active;

		// Token: 0x040016D0 RID: 5840
		private Socket socket;

		// Token: 0x040016D1 RID: 5841
		private AddressFamily family;

		// Token: 0x040016D2 RID: 5842
		private byte[] recvbuffer;
	}
}
