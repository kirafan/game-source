﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace System.Net.Sockets
{
	// Token: 0x020003FD RID: 1021
	public class SocketAsyncEventArgs : EventArgs, IDisposable
	{
		// Token: 0x060023F3 RID: 9203 RVA: 0x0006C2A0 File Offset: 0x0006A4A0
		public SocketAsyncEventArgs()
		{
			this.AcceptSocket = null;
			this.Buffer = null;
			this.BufferList = null;
			this.BytesTransferred = 0;
			this.Count = 0;
			this.DisconnectReuseSocket = false;
			this.LastOperation = SocketAsyncOperation.None;
			this.Offset = 0;
			this.RemoteEndPoint = null;
			this.SendPacketsElements = null;
			this.SendPacketsFlags = TransmitFileOptions.UseDefaultWorkerThread;
			this.SendPacketsSendSize = -1;
			this.SocketError = SocketError.Success;
			this.SocketFlags = SocketFlags.None;
			this.UserToken = null;
		}

		// Token: 0x14000052 RID: 82
		// (add) Token: 0x060023F4 RID: 9204 RVA: 0x0006C31C File Offset: 0x0006A51C
		// (remove) Token: 0x060023F5 RID: 9205 RVA: 0x0006C338 File Offset: 0x0006A538
		public event EventHandler<SocketAsyncEventArgs> Completed;

		// Token: 0x17000A50 RID: 2640
		// (get) Token: 0x060023F6 RID: 9206 RVA: 0x0006C354 File Offset: 0x0006A554
		// (set) Token: 0x060023F7 RID: 9207 RVA: 0x0006C35C File Offset: 0x0006A55C
		public Socket AcceptSocket { get; set; }

		// Token: 0x17000A51 RID: 2641
		// (get) Token: 0x060023F8 RID: 9208 RVA: 0x0006C368 File Offset: 0x0006A568
		// (set) Token: 0x060023F9 RID: 9209 RVA: 0x0006C370 File Offset: 0x0006A570
		public byte[] Buffer { get; private set; }

		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x060023FA RID: 9210 RVA: 0x0006C37C File Offset: 0x0006A57C
		// (set) Token: 0x060023FB RID: 9211 RVA: 0x0006C384 File Offset: 0x0006A584
		[MonoTODO("not supported in all cases")]
		public IList<ArraySegment<byte>> BufferList
		{
			get
			{
				return this._bufferList;
			}
			set
			{
				if (this.Buffer != null && value != null)
				{
					throw new ArgumentException("Buffer and BufferList properties cannot both be non-null.");
				}
				this._bufferList = value;
			}
		}

		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x060023FC RID: 9212 RVA: 0x0006C3AC File Offset: 0x0006A5AC
		// (set) Token: 0x060023FD RID: 9213 RVA: 0x0006C3B4 File Offset: 0x0006A5B4
		public int BytesTransferred { get; private set; }

		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x060023FE RID: 9214 RVA: 0x0006C3C0 File Offset: 0x0006A5C0
		// (set) Token: 0x060023FF RID: 9215 RVA: 0x0006C3C8 File Offset: 0x0006A5C8
		public int Count { get; private set; }

		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x06002400 RID: 9216 RVA: 0x0006C3D4 File Offset: 0x0006A5D4
		// (set) Token: 0x06002401 RID: 9217 RVA: 0x0006C3DC File Offset: 0x0006A5DC
		public bool DisconnectReuseSocket { get; set; }

		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x06002402 RID: 9218 RVA: 0x0006C3E8 File Offset: 0x0006A5E8
		// (set) Token: 0x06002403 RID: 9219 RVA: 0x0006C3F0 File Offset: 0x0006A5F0
		public SocketAsyncOperation LastOperation { get; private set; }

		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x06002404 RID: 9220 RVA: 0x0006C3FC File Offset: 0x0006A5FC
		// (set) Token: 0x06002405 RID: 9221 RVA: 0x0006C404 File Offset: 0x0006A604
		public int Offset { get; private set; }

		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x06002406 RID: 9222 RVA: 0x0006C410 File Offset: 0x0006A610
		// (set) Token: 0x06002407 RID: 9223 RVA: 0x0006C418 File Offset: 0x0006A618
		public EndPoint RemoteEndPoint { get; set; }

		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x06002408 RID: 9224 RVA: 0x0006C424 File Offset: 0x0006A624
		// (set) Token: 0x06002409 RID: 9225 RVA: 0x0006C42C File Offset: 0x0006A62C
		public IPPacketInformation ReceiveMessageFromPacketInfo { get; private set; }

		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x0600240A RID: 9226 RVA: 0x0006C438 File Offset: 0x0006A638
		// (set) Token: 0x0600240B RID: 9227 RVA: 0x0006C440 File Offset: 0x0006A640
		public SendPacketsElement[] SendPacketsElements { get; set; }

		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x0600240C RID: 9228 RVA: 0x0006C44C File Offset: 0x0006A64C
		// (set) Token: 0x0600240D RID: 9229 RVA: 0x0006C454 File Offset: 0x0006A654
		public TransmitFileOptions SendPacketsFlags { get; set; }

		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x0600240E RID: 9230 RVA: 0x0006C460 File Offset: 0x0006A660
		// (set) Token: 0x0600240F RID: 9231 RVA: 0x0006C468 File Offset: 0x0006A668
		[MonoTODO("unused property")]
		public int SendPacketsSendSize { get; set; }

		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x06002410 RID: 9232 RVA: 0x0006C474 File Offset: 0x0006A674
		// (set) Token: 0x06002411 RID: 9233 RVA: 0x0006C47C File Offset: 0x0006A67C
		public SocketError SocketError { get; set; }

		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x06002412 RID: 9234 RVA: 0x0006C488 File Offset: 0x0006A688
		// (set) Token: 0x06002413 RID: 9235 RVA: 0x0006C490 File Offset: 0x0006A690
		public SocketFlags SocketFlags { get; set; }

		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x06002414 RID: 9236 RVA: 0x0006C49C File Offset: 0x0006A69C
		// (set) Token: 0x06002415 RID: 9237 RVA: 0x0006C4A4 File Offset: 0x0006A6A4
		public object UserToken { get; set; }

		// Token: 0x06002416 RID: 9238 RVA: 0x0006C4B0 File Offset: 0x0006A6B0
		~SocketAsyncEventArgs()
		{
			this.Dispose(false);
		}

		// Token: 0x06002417 RID: 9239 RVA: 0x0006C4EC File Offset: 0x0006A6EC
		private void Dispose(bool disposing)
		{
			Socket acceptSocket = this.AcceptSocket;
			if (acceptSocket != null)
			{
				acceptSocket.Close();
			}
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x06002418 RID: 9240 RVA: 0x0006C518 File Offset: 0x0006A718
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06002419 RID: 9241 RVA: 0x0006C524 File Offset: 0x0006A724
		protected virtual void OnCompleted(SocketAsyncEventArgs e)
		{
			if (e == null)
			{
				return;
			}
			EventHandler<SocketAsyncEventArgs> completed = e.Completed;
			if (completed != null)
			{
				completed(e.curSocket, e);
			}
		}

		// Token: 0x0600241A RID: 9242 RVA: 0x0006C554 File Offset: 0x0006A754
		public void SetBuffer(int offset, int count)
		{
			this.SetBufferInternal(this.Buffer, offset, count);
		}

		// Token: 0x0600241B RID: 9243 RVA: 0x0006C564 File Offset: 0x0006A764
		public void SetBuffer(byte[] buffer, int offset, int count)
		{
			this.SetBufferInternal(buffer, offset, count);
		}

		// Token: 0x0600241C RID: 9244 RVA: 0x0006C570 File Offset: 0x0006A770
		private void SetBufferInternal(byte[] buffer, int offset, int count)
		{
			if (buffer != null)
			{
				if (this.BufferList != null)
				{
					throw new ArgumentException("Buffer and BufferList properties cannot both be non-null.");
				}
				int num = buffer.Length;
				if (offset < 0 || (offset != 0 && offset >= num))
				{
					throw new ArgumentOutOfRangeException("offset");
				}
				if (count < 0 || count > num - offset)
				{
					throw new ArgumentOutOfRangeException("count");
				}
				this.Count = count;
				this.Offset = offset;
			}
			this.Buffer = buffer;
		}

		// Token: 0x0600241D RID: 9245 RVA: 0x0006C5EC File Offset: 0x0006A7EC
		private void ReceiveCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.Receive;
			SocketError socketError = SocketError.Success;
			if (!this.curSocket.Connected)
			{
				this.SocketError = SocketError.NotConnected;
				return;
			}
			try
			{
				this.BytesTransferred = this.curSocket.Receive_nochecks(this.Buffer, this.Offset, this.Count, this.SocketFlags, out socketError);
			}
			finally
			{
				this.SocketError = socketError;
				this.OnCompleted(this);
			}
		}

		// Token: 0x0600241E RID: 9246 RVA: 0x0006C680 File Offset: 0x0006A880
		private void ConnectCallback()
		{
			this.LastOperation = SocketAsyncOperation.Connect;
			SocketError socketError = SocketError.AccessDenied;
			try
			{
				socketError = this.TryConnect(this.RemoteEndPoint);
			}
			finally
			{
				this.SocketError = socketError;
				this.OnCompleted(this);
			}
		}

		// Token: 0x0600241F RID: 9247 RVA: 0x0006C6D8 File Offset: 0x0006A8D8
		private SocketError TryConnect(EndPoint endpoint)
		{
			this.curSocket.Connected = false;
			SocketError result = SocketError.Success;
			try
			{
				if (!this.curSocket.Blocking)
				{
					int num;
					this.curSocket.Poll(-1, SelectMode.SelectWrite, out num);
					result = (SocketError)num;
					if (num != 0)
					{
						return result;
					}
					this.curSocket.Connected = true;
				}
				else
				{
					this.curSocket.seed_endpoint = endpoint;
					this.curSocket.Connect(endpoint);
					this.curSocket.Connected = true;
				}
			}
			catch (SocketException ex)
			{
				result = ex.SocketErrorCode;
			}
			return result;
		}

		// Token: 0x06002420 RID: 9248 RVA: 0x0006C790 File Offset: 0x0006A990
		private void SendCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.Send;
			SocketError socketError = SocketError.Success;
			if (!this.curSocket.Connected)
			{
				this.SocketError = SocketError.NotConnected;
				return;
			}
			try
			{
				if (this.Buffer != null)
				{
					this.BytesTransferred = this.curSocket.Send_nochecks(this.Buffer, this.Offset, this.Count, SocketFlags.None, out socketError);
				}
				else if (this.BufferList != null)
				{
					this.BytesTransferred = 0;
					foreach (ArraySegment<byte> arraySegment in this.BufferList)
					{
						this.BytesTransferred += this.curSocket.Send_nochecks(arraySegment.Array, arraySegment.Offset, arraySegment.Count, SocketFlags.None, out socketError);
						if (socketError != SocketError.Success)
						{
							break;
						}
					}
				}
			}
			finally
			{
				this.SocketError = socketError;
				this.OnCompleted(this);
			}
		}

		// Token: 0x06002421 RID: 9249 RVA: 0x0006C8C8 File Offset: 0x0006AAC8
		private void AcceptCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.Accept;
			try
			{
				this.curSocket.Accept(this.AcceptSocket);
			}
			catch (SocketException ex)
			{
				this.SocketError = ex.SocketErrorCode;
				throw;
			}
			finally
			{
				this.OnCompleted(this);
			}
		}

		// Token: 0x06002422 RID: 9250 RVA: 0x0006C94C File Offset: 0x0006AB4C
		private void DisconnectCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.Disconnect;
			try
			{
				this.curSocket.Disconnect(this.DisconnectReuseSocket);
			}
			catch (SocketException ex)
			{
				this.SocketError = ex.SocketErrorCode;
				throw;
			}
			finally
			{
				this.OnCompleted(this);
			}
		}

		// Token: 0x06002423 RID: 9251 RVA: 0x0006C9D0 File Offset: 0x0006ABD0
		private void ReceiveFromCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.ReceiveFrom;
			try
			{
				EndPoint remoteEndPoint = this.RemoteEndPoint;
				if (this.Buffer != null)
				{
					this.BytesTransferred = this.curSocket.ReceiveFrom_nochecks(this.Buffer, this.Offset, this.Count, this.SocketFlags, ref remoteEndPoint);
				}
				else if (this.BufferList != null)
				{
					throw new NotImplementedException();
				}
			}
			catch (SocketException ex)
			{
				this.SocketError = ex.SocketErrorCode;
				throw;
			}
			finally
			{
				this.OnCompleted(this);
			}
		}

		// Token: 0x06002424 RID: 9252 RVA: 0x0006CA94 File Offset: 0x0006AC94
		private void SendToCallback()
		{
			this.SocketError = SocketError.Success;
			this.LastOperation = SocketAsyncOperation.SendTo;
			int i = 0;
			try
			{
				int count = this.Count;
				while (i < count)
				{
					i += this.curSocket.SendTo_nochecks(this.Buffer, this.Offset, count, this.SocketFlags, this.RemoteEndPoint);
				}
				this.BytesTransferred = i;
			}
			catch (SocketException ex)
			{
				this.SocketError = ex.SocketErrorCode;
				throw;
			}
			finally
			{
				this.OnCompleted(this);
			}
		}

		// Token: 0x06002425 RID: 9253 RVA: 0x0006CB48 File Offset: 0x0006AD48
		internal void DoOperation(SocketAsyncOperation operation, Socket socket)
		{
			this.curSocket = socket;
			ThreadStart start;
			switch (operation)
			{
			case SocketAsyncOperation.Accept:
				start = new ThreadStart(this.AcceptCallback);
				goto IL_BE;
			case SocketAsyncOperation.Connect:
				start = new ThreadStart(this.ConnectCallback);
				goto IL_BE;
			case SocketAsyncOperation.Disconnect:
				start = new ThreadStart(this.DisconnectCallback);
				goto IL_BE;
			case SocketAsyncOperation.Receive:
				start = new ThreadStart(this.ReceiveCallback);
				goto IL_BE;
			case SocketAsyncOperation.ReceiveFrom:
				start = new ThreadStart(this.ReceiveFromCallback);
				goto IL_BE;
			case SocketAsyncOperation.Send:
				start = new ThreadStart(this.SendCallback);
				goto IL_BE;
			case SocketAsyncOperation.SendTo:
				start = new ThreadStart(this.SendToCallback);
				goto IL_BE;
			}
			throw new NotSupportedException();
			IL_BE:
			new Thread(start)
			{
				IsBackground = true
			}.Start();
		}

		// Token: 0x04001616 RID: 5654
		private IList<ArraySegment<byte>> _bufferList;

		// Token: 0x04001617 RID: 5655
		private Socket curSocket;
	}
}
