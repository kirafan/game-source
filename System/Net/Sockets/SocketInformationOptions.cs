﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000403 RID: 1027
	[Flags]
	public enum SocketInformationOptions
	{
		// Token: 0x04001671 RID: 5745
		NonBlocking = 1,
		// Token: 0x04001672 RID: 5746
		Connected = 2,
		// Token: 0x04001673 RID: 5747
		Listening = 4,
		// Token: 0x04001674 RID: 5748
		UseOnlyOverlappedIO = 8
	}
}
