﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000407 RID: 1031
	public enum SocketType
	{
		// Token: 0x040016AC RID: 5804
		Stream = 1,
		// Token: 0x040016AD RID: 5805
		Dgram,
		// Token: 0x040016AE RID: 5806
		Raw,
		// Token: 0x040016AF RID: 5807
		Rdm,
		// Token: 0x040016B0 RID: 5808
		Seqpacket,
		// Token: 0x040016B1 RID: 5809
		Unknown = -1
	}
}
