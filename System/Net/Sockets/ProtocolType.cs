﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003F3 RID: 1011
	public enum ProtocolType
	{
		// Token: 0x040015AC RID: 5548
		IP,
		// Token: 0x040015AD RID: 5549
		Icmp,
		// Token: 0x040015AE RID: 5550
		Igmp,
		// Token: 0x040015AF RID: 5551
		Ggp,
		// Token: 0x040015B0 RID: 5552
		Tcp = 6,
		// Token: 0x040015B1 RID: 5553
		Pup = 12,
		// Token: 0x040015B2 RID: 5554
		Udp = 17,
		// Token: 0x040015B3 RID: 5555
		Idp = 22,
		// Token: 0x040015B4 RID: 5556
		IPv6 = 41,
		// Token: 0x040015B5 RID: 5557
		ND = 77,
		// Token: 0x040015B6 RID: 5558
		Raw = 255,
		// Token: 0x040015B7 RID: 5559
		Unspecified = 0,
		// Token: 0x040015B8 RID: 5560
		Ipx = 1000,
		// Token: 0x040015B9 RID: 5561
		Spx = 1256,
		// Token: 0x040015BA RID: 5562
		SpxII,
		// Token: 0x040015BB RID: 5563
		Unknown = -1,
		// Token: 0x040015BC RID: 5564
		IPv4 = 4,
		// Token: 0x040015BD RID: 5565
		IPv6RoutingHeader = 43,
		// Token: 0x040015BE RID: 5566
		IPv6FragmentHeader,
		// Token: 0x040015BF RID: 5567
		IPSecEncapsulatingSecurityPayload = 50,
		// Token: 0x040015C0 RID: 5568
		IPSecAuthenticationHeader,
		// Token: 0x040015C1 RID: 5569
		IcmpV6 = 58,
		// Token: 0x040015C2 RID: 5570
		IPv6NoNextHeader,
		// Token: 0x040015C3 RID: 5571
		IPv6DestinationOptions,
		// Token: 0x040015C4 RID: 5572
		IPv6HopByHopOptions = 0
	}
}
