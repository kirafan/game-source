﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003ED RID: 1005
	public class IPv6MulticastOption
	{
		// Token: 0x060022BF RID: 8895 RVA: 0x000660B0 File Offset: 0x000642B0
		public IPv6MulticastOption(IPAddress group) : this(group, 0L)
		{
		}

		// Token: 0x060022C0 RID: 8896 RVA: 0x000660BC File Offset: 0x000642BC
		public IPv6MulticastOption(IPAddress group, long ifindex)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			if (ifindex < 0L || ifindex > (long)((ulong)-1))
			{
				throw new ArgumentOutOfRangeException("ifindex");
			}
			this.group = group;
			this.ifIndex = ifindex;
		}

		// Token: 0x17000A10 RID: 2576
		// (get) Token: 0x060022C1 RID: 8897 RVA: 0x0006610C File Offset: 0x0006430C
		// (set) Token: 0x060022C2 RID: 8898 RVA: 0x00066114 File Offset: 0x00064314
		public IPAddress Group
		{
			get
			{
				return this.group;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.group = value;
			}
		}

		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x060022C3 RID: 8899 RVA: 0x00066130 File Offset: 0x00064330
		// (set) Token: 0x060022C4 RID: 8900 RVA: 0x00066138 File Offset: 0x00064338
		public long InterfaceIndex
		{
			get
			{
				return this.ifIndex;
			}
			set
			{
				if (value < 0L || value > (long)((ulong)-1))
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.ifIndex = value;
			}
		}

		// Token: 0x0400157C RID: 5500
		private IPAddress group;

		// Token: 0x0400157D RID: 5501
		private long ifIndex;
	}
}
