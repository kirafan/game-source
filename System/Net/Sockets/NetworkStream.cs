﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Timers;

namespace System.Net.Sockets
{
	// Token: 0x020003F1 RID: 1009
	public class NetworkStream : Stream, IDisposable
	{
		// Token: 0x060022DA RID: 8922 RVA: 0x00066350 File Offset: 0x00064550
		public NetworkStream(Socket socket) : this(socket, FileAccess.ReadWrite, false)
		{
		}

		// Token: 0x060022DB RID: 8923 RVA: 0x0006635C File Offset: 0x0006455C
		public NetworkStream(Socket socket, bool owns_socket) : this(socket, FileAccess.ReadWrite, owns_socket)
		{
		}

		// Token: 0x060022DC RID: 8924 RVA: 0x00066368 File Offset: 0x00064568
		public NetworkStream(Socket socket, FileAccess access) : this(socket, access, false)
		{
		}

		// Token: 0x060022DD RID: 8925 RVA: 0x00066374 File Offset: 0x00064574
		public NetworkStream(Socket socket, FileAccess access, bool owns_socket)
		{
			if (socket == null)
			{
				throw new ArgumentNullException("socket is null");
			}
			if (socket.SocketType != SocketType.Stream)
			{
				throw new ArgumentException("Socket is not of type Stream", "socket");
			}
			if (!socket.Connected)
			{
				throw new IOException("Not connected");
			}
			if (!socket.Blocking)
			{
				throw new IOException("Operation not allowed on a non-blocking socket.");
			}
			this.socket = socket;
			this.owns_socket = owns_socket;
			this.access = access;
			this.readable = this.CanRead;
			this.writeable = this.CanWrite;
		}

		// Token: 0x060022DE RID: 8926 RVA: 0x00066410 File Offset: 0x00064610
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x060022DF RID: 8927 RVA: 0x00066420 File Offset: 0x00064620
		public override bool CanRead
		{
			get
			{
				return this.access == FileAccess.ReadWrite || this.access == FileAccess.Read;
			}
		}

		// Token: 0x17000A1A RID: 2586
		// (get) Token: 0x060022E0 RID: 8928 RVA: 0x0006643C File Offset: 0x0006463C
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x060022E1 RID: 8929 RVA: 0x00066440 File Offset: 0x00064640
		public override bool CanTimeout
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x060022E2 RID: 8930 RVA: 0x00066444 File Offset: 0x00064644
		public override bool CanWrite
		{
			get
			{
				return this.access == FileAccess.ReadWrite || this.access == FileAccess.Write;
			}
		}

		// Token: 0x17000A1D RID: 2589
		// (get) Token: 0x060022E3 RID: 8931 RVA: 0x00066460 File Offset: 0x00064660
		public virtual bool DataAvailable
		{
			get
			{
				this.CheckDisposed();
				return this.socket.Available > 0;
			}
		}

		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x060022E4 RID: 8932 RVA: 0x00066478 File Offset: 0x00064678
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x060022E5 RID: 8933 RVA: 0x00066480 File Offset: 0x00064680
		// (set) Token: 0x060022E6 RID: 8934 RVA: 0x00066488 File Offset: 0x00064688
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000A20 RID: 2592
		// (get) Token: 0x060022E7 RID: 8935 RVA: 0x00066490 File Offset: 0x00064690
		// (set) Token: 0x060022E8 RID: 8936 RVA: 0x00066498 File Offset: 0x00064698
		protected bool Readable
		{
			get
			{
				return this.readable;
			}
			set
			{
				this.readable = value;
			}
		}

		// Token: 0x17000A21 RID: 2593
		// (get) Token: 0x060022E9 RID: 8937 RVA: 0x000664A4 File Offset: 0x000646A4
		// (set) Token: 0x060022EA RID: 8938 RVA: 0x000664B4 File Offset: 0x000646B4
		public override int ReadTimeout
		{
			get
			{
				return this.socket.ReceiveTimeout;
			}
			set
			{
				if (value <= 0 && value != -1)
				{
					throw new ArgumentOutOfRangeException("value", "The value specified is less than or equal to zero and is not Infinite.");
				}
				this.socket.ReceiveTimeout = value;
			}
		}

		// Token: 0x17000A22 RID: 2594
		// (get) Token: 0x060022EB RID: 8939 RVA: 0x000664EC File Offset: 0x000646EC
		protected Socket Socket
		{
			get
			{
				return this.socket;
			}
		}

		// Token: 0x17000A23 RID: 2595
		// (get) Token: 0x060022EC RID: 8940 RVA: 0x000664F4 File Offset: 0x000646F4
		// (set) Token: 0x060022ED RID: 8941 RVA: 0x000664FC File Offset: 0x000646FC
		protected bool Writeable
		{
			get
			{
				return this.writeable;
			}
			set
			{
				this.writeable = value;
			}
		}

		// Token: 0x17000A24 RID: 2596
		// (get) Token: 0x060022EE RID: 8942 RVA: 0x00066508 File Offset: 0x00064708
		// (set) Token: 0x060022EF RID: 8943 RVA: 0x00066518 File Offset: 0x00064718
		public override int WriteTimeout
		{
			get
			{
				return this.socket.SendTimeout;
			}
			set
			{
				if (value <= 0 && value != -1)
				{
					throw new ArgumentOutOfRangeException("value", "The value specified is less than or equal to zero and is not Infinite");
				}
				this.socket.SendTimeout = value;
			}
		}

		// Token: 0x060022F0 RID: 8944 RVA: 0x00066550 File Offset: 0x00064750
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			this.CheckDisposed();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer is null");
			}
			int num = buffer.Length;
			if (offset < 0 || offset > num)
			{
				throw new ArgumentOutOfRangeException("offset exceeds the size of buffer");
			}
			if (size < 0 || offset + size > num)
			{
				throw new ArgumentOutOfRangeException("offset+size exceeds the size of buffer");
			}
			IAsyncResult result;
			try
			{
				result = this.socket.BeginReceive(buffer, offset, size, SocketFlags.None, callback, state);
			}
			catch (Exception innerException)
			{
				throw new IOException("BeginReceive failure", innerException);
			}
			return result;
		}

		// Token: 0x060022F1 RID: 8945 RVA: 0x000665F4 File Offset: 0x000647F4
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int size, AsyncCallback callback, object state)
		{
			this.CheckDisposed();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer is null");
			}
			int num = buffer.Length;
			if (offset < 0 || offset > num)
			{
				throw new ArgumentOutOfRangeException("offset exceeds the size of buffer");
			}
			if (size < 0 || offset + size > num)
			{
				throw new ArgumentOutOfRangeException("offset+size exceeds the size of buffer");
			}
			IAsyncResult result;
			try
			{
				result = this.socket.BeginSend(buffer, offset, size, SocketFlags.None, callback, state);
			}
			catch
			{
				throw new IOException("BeginWrite failure");
			}
			return result;
		}

		// Token: 0x060022F2 RID: 8946 RVA: 0x00066698 File Offset: 0x00064898
		~NetworkStream()
		{
			this.Dispose(false);
		}

		// Token: 0x060022F3 RID: 8947 RVA: 0x000666D4 File Offset: 0x000648D4
		public void Close(int timeout)
		{
			if (timeout < -1)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout is less than -1");
			}
			System.Timers.Timer timer = new System.Timers.Timer();
			timer.Elapsed += this.OnTimeoutClose;
			timer.Interval = (double)timeout;
			timer.AutoReset = false;
			timer.Enabled = true;
		}

		// Token: 0x060022F4 RID: 8948 RVA: 0x00066728 File Offset: 0x00064928
		private void OnTimeoutClose(object source, System.Timers.ElapsedEventArgs e)
		{
			this.Close();
		}

		// Token: 0x060022F5 RID: 8949 RVA: 0x00066730 File Offset: 0x00064930
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			this.disposed = true;
			if (this.owns_socket)
			{
				Socket socket = this.socket;
				if (socket != null)
				{
					socket.Close();
				}
			}
			this.socket = null;
			this.access = (FileAccess)0;
		}

		// Token: 0x060022F6 RID: 8950 RVA: 0x0006677C File Offset: 0x0006497C
		public override int EndRead(IAsyncResult ar)
		{
			this.CheckDisposed();
			if (ar == null)
			{
				throw new ArgumentNullException("async result is null");
			}
			int result;
			try
			{
				result = this.socket.EndReceive(ar);
			}
			catch (Exception innerException)
			{
				throw new IOException("EndRead failure", innerException);
			}
			return result;
		}

		// Token: 0x060022F7 RID: 8951 RVA: 0x000667E4 File Offset: 0x000649E4
		public override void EndWrite(IAsyncResult ar)
		{
			this.CheckDisposed();
			if (ar == null)
			{
				throw new ArgumentNullException("async result is null");
			}
			try
			{
				this.socket.EndSend(ar);
			}
			catch (Exception innerException)
			{
				throw new IOException("EndWrite failure", innerException);
			}
		}

		// Token: 0x060022F8 RID: 8952 RVA: 0x00066848 File Offset: 0x00064A48
		public override void Flush()
		{
		}

		// Token: 0x060022F9 RID: 8953 RVA: 0x0006684C File Offset: 0x00064A4C
		public override int Read([In] [Out] byte[] buffer, int offset, int size)
		{
			this.CheckDisposed();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer is null");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset exceeds the size of buffer");
			}
			if (size < 0 || offset + size > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset+size exceeds the size of buffer");
			}
			int result;
			try
			{
				result = this.socket.Receive(buffer, offset, size, SocketFlags.None);
			}
			catch (Exception innerException)
			{
				throw new IOException("Read failure", innerException);
			}
			return result;
		}

		// Token: 0x060022FA RID: 8954 RVA: 0x000668EC File Offset: 0x00064AEC
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060022FB RID: 8955 RVA: 0x000668F4 File Offset: 0x00064AF4
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060022FC RID: 8956 RVA: 0x000668FC File Offset: 0x00064AFC
		public override void Write(byte[] buffer, int offset, int size)
		{
			this.CheckDisposed();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || offset > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("offset exceeds the size of buffer");
			}
			if (size < 0 || size > buffer.Length - offset)
			{
				throw new ArgumentOutOfRangeException("offset+size exceeds the size of buffer");
			}
			try
			{
				int num = 0;
				while (size - num > 0)
				{
					num += this.socket.Send(buffer, offset + num, size - num, SocketFlags.None);
				}
			}
			catch (Exception innerException)
			{
				throw new IOException("Write failure", innerException);
			}
		}

		// Token: 0x060022FD RID: 8957 RVA: 0x000669B4 File Offset: 0x00064BB4
		private void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x04001585 RID: 5509
		private FileAccess access;

		// Token: 0x04001586 RID: 5510
		private Socket socket;

		// Token: 0x04001587 RID: 5511
		private bool owns_socket;

		// Token: 0x04001588 RID: 5512
		private bool readable;

		// Token: 0x04001589 RID: 5513
		private bool writeable;

		// Token: 0x0400158A RID: 5514
		private bool disposed;
	}
}
