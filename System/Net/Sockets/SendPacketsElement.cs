﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003F5 RID: 1013
	public class SendPacketsElement
	{
		// Token: 0x060022FE RID: 8958 RVA: 0x000669D4 File Offset: 0x00064BD4
		public SendPacketsElement(byte[] buffer) : this(buffer, 0, (buffer == null) ? 0 : buffer.Length)
		{
		}

		// Token: 0x060022FF RID: 8959 RVA: 0x000669F0 File Offset: 0x00064BF0
		public SendPacketsElement(byte[] buffer, int offset, int count) : this(buffer, offset, count, false)
		{
		}

		// Token: 0x06002300 RID: 8960 RVA: 0x000669FC File Offset: 0x00064BFC
		public SendPacketsElement(byte[] buffer, int offset, int count, bool endOfPacket)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			int num = buffer.Length;
			if (offset < 0 || offset >= num)
			{
				throw new ArgumentOutOfRangeException("offset");
			}
			if (count < 0 || offset + count >= num)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this.Buffer = buffer;
			this.Offset = offset;
			this.Count = count;
			this.EndOfPacket = endOfPacket;
			this.FilePath = null;
		}

		// Token: 0x06002301 RID: 8961 RVA: 0x00066A7C File Offset: 0x00064C7C
		public SendPacketsElement(string filepath) : this(filepath, 0, 0, false)
		{
		}

		// Token: 0x06002302 RID: 8962 RVA: 0x00066A88 File Offset: 0x00064C88
		public SendPacketsElement(string filepath, int offset, int count) : this(filepath, offset, count, false)
		{
		}

		// Token: 0x06002303 RID: 8963 RVA: 0x00066A94 File Offset: 0x00064C94
		public SendPacketsElement(string filepath, int offset, int count, bool endOfPacket)
		{
			if (filepath == null)
			{
				throw new ArgumentNullException("filepath");
			}
			this.Buffer = null;
			this.Offset = offset;
			this.Count = count;
			this.EndOfPacket = endOfPacket;
			this.FilePath = filepath;
		}

		// Token: 0x17000A25 RID: 2597
		// (get) Token: 0x06002304 RID: 8964 RVA: 0x00066ADC File Offset: 0x00064CDC
		// (set) Token: 0x06002305 RID: 8965 RVA: 0x00066AE4 File Offset: 0x00064CE4
		public byte[] Buffer { get; private set; }

		// Token: 0x17000A26 RID: 2598
		// (get) Token: 0x06002306 RID: 8966 RVA: 0x00066AF0 File Offset: 0x00064CF0
		// (set) Token: 0x06002307 RID: 8967 RVA: 0x00066AF8 File Offset: 0x00064CF8
		public int Count { get; private set; }

		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x06002308 RID: 8968 RVA: 0x00066B04 File Offset: 0x00064D04
		// (set) Token: 0x06002309 RID: 8969 RVA: 0x00066B0C File Offset: 0x00064D0C
		public bool EndOfPacket { get; private set; }

		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x0600230A RID: 8970 RVA: 0x00066B18 File Offset: 0x00064D18
		// (set) Token: 0x0600230B RID: 8971 RVA: 0x00066B20 File Offset: 0x00064D20
		public string FilePath { get; private set; }

		// Token: 0x17000A29 RID: 2601
		// (get) Token: 0x0600230C RID: 8972 RVA: 0x00066B2C File Offset: 0x00064D2C
		// (set) Token: 0x0600230D RID: 8973 RVA: 0x00066B34 File Offset: 0x00064D34
		public int Offset { get; private set; }
	}
}
