﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003FF RID: 1023
	public enum SocketError
	{
		// Token: 0x04001634 RID: 5684
		AccessDenied = 10013,
		// Token: 0x04001635 RID: 5685
		AddressAlreadyInUse = 10048,
		// Token: 0x04001636 RID: 5686
		AddressFamilyNotSupported = 10047,
		// Token: 0x04001637 RID: 5687
		AddressNotAvailable = 10049,
		// Token: 0x04001638 RID: 5688
		AlreadyInProgress = 10037,
		// Token: 0x04001639 RID: 5689
		ConnectionAborted = 10053,
		// Token: 0x0400163A RID: 5690
		ConnectionRefused = 10061,
		// Token: 0x0400163B RID: 5691
		ConnectionReset = 10054,
		// Token: 0x0400163C RID: 5692
		DestinationAddressRequired = 10039,
		// Token: 0x0400163D RID: 5693
		Disconnecting = 10101,
		// Token: 0x0400163E RID: 5694
		Fault = 10014,
		// Token: 0x0400163F RID: 5695
		HostDown = 10064,
		// Token: 0x04001640 RID: 5696
		HostNotFound = 11001,
		// Token: 0x04001641 RID: 5697
		HostUnreachable = 10065,
		// Token: 0x04001642 RID: 5698
		InProgress = 10036,
		// Token: 0x04001643 RID: 5699
		Interrupted = 10004,
		// Token: 0x04001644 RID: 5700
		InvalidArgument = 10022,
		// Token: 0x04001645 RID: 5701
		IOPending = 997,
		// Token: 0x04001646 RID: 5702
		IsConnected = 10056,
		// Token: 0x04001647 RID: 5703
		MessageSize = 10040,
		// Token: 0x04001648 RID: 5704
		NetworkDown = 10050,
		// Token: 0x04001649 RID: 5705
		NetworkReset = 10052,
		// Token: 0x0400164A RID: 5706
		NetworkUnreachable = 10051,
		// Token: 0x0400164B RID: 5707
		NoBufferSpaceAvailable = 10055,
		// Token: 0x0400164C RID: 5708
		NoData = 11004,
		// Token: 0x0400164D RID: 5709
		NoRecovery = 11003,
		// Token: 0x0400164E RID: 5710
		NotConnected = 10057,
		// Token: 0x0400164F RID: 5711
		NotInitialized = 10093,
		// Token: 0x04001650 RID: 5712
		NotSocket = 10038,
		// Token: 0x04001651 RID: 5713
		OperationAborted = 995,
		// Token: 0x04001652 RID: 5714
		OperationNotSupported = 10045,
		// Token: 0x04001653 RID: 5715
		ProcessLimit = 10067,
		// Token: 0x04001654 RID: 5716
		ProtocolFamilyNotSupported = 10046,
		// Token: 0x04001655 RID: 5717
		ProtocolNotSupported = 10043,
		// Token: 0x04001656 RID: 5718
		ProtocolOption = 10042,
		// Token: 0x04001657 RID: 5719
		ProtocolType = 10041,
		// Token: 0x04001658 RID: 5720
		Shutdown = 10058,
		// Token: 0x04001659 RID: 5721
		SocketError = -1,
		// Token: 0x0400165A RID: 5722
		SocketNotSupported = 10044,
		// Token: 0x0400165B RID: 5723
		Success = 0,
		// Token: 0x0400165C RID: 5724
		SystemNotReady = 10091,
		// Token: 0x0400165D RID: 5725
		TimedOut = 10060,
		// Token: 0x0400165E RID: 5726
		TooManyOpenSockets = 10024,
		// Token: 0x0400165F RID: 5727
		TryAgain = 11002,
		// Token: 0x04001660 RID: 5728
		TypeNotFound = 10109,
		// Token: 0x04001661 RID: 5729
		VersionNotSupported = 10092,
		// Token: 0x04001662 RID: 5730
		WouldBlock = 10035
	}
}
