﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x02000406 RID: 1030
	public enum SocketShutdown
	{
		// Token: 0x040016A8 RID: 5800
		Receive,
		// Token: 0x040016A9 RID: 5801
		Send,
		// Token: 0x040016AA RID: 5802
		Both
	}
}
