﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003EE RID: 1006
	public struct IPPacketInformation
	{
		// Token: 0x060022C5 RID: 8901 RVA: 0x00066168 File Offset: 0x00064368
		internal IPPacketInformation(IPAddress address, int iface)
		{
			this.address = address;
			this.iface = iface;
		}

		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x060022C6 RID: 8902 RVA: 0x00066178 File Offset: 0x00064378
		public IPAddress Address
		{
			get
			{
				return this.address;
			}
		}

		// Token: 0x17000A13 RID: 2579
		// (get) Token: 0x060022C7 RID: 8903 RVA: 0x00066180 File Offset: 0x00064380
		public int Interface
		{
			get
			{
				return this.iface;
			}
		}

		// Token: 0x060022C8 RID: 8904 RVA: 0x00066188 File Offset: 0x00064388
		public override bool Equals(object comparand)
		{
			if (!(comparand is IPPacketInformation))
			{
				return false;
			}
			IPPacketInformation ippacketInformation = (IPPacketInformation)comparand;
			return ippacketInformation.iface == this.iface && ippacketInformation.address.Equals(this.address);
		}

		// Token: 0x060022C9 RID: 8905 RVA: 0x000661D0 File Offset: 0x000643D0
		public override int GetHashCode()
		{
			return this.address.GetHashCode() + this.iface;
		}

		// Token: 0x060022CA RID: 8906 RVA: 0x000661E4 File Offset: 0x000643E4
		public static bool operator ==(IPPacketInformation p1, IPPacketInformation p2)
		{
			return p1.Equals(p2);
		}

		// Token: 0x060022CB RID: 8907 RVA: 0x000661F4 File Offset: 0x000643F4
		public static bool operator !=(IPPacketInformation p1, IPPacketInformation p2)
		{
			return !p1.Equals(p2);
		}

		// Token: 0x0400157E RID: 5502
		private IPAddress address;

		// Token: 0x0400157F RID: 5503
		private int iface;
	}
}
