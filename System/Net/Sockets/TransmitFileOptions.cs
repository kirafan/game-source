﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x0200040B RID: 1035
	[Flags]
	public enum TransmitFileOptions
	{
		// Token: 0x040016C8 RID: 5832
		UseDefaultWorkerThread = 0,
		// Token: 0x040016C9 RID: 5833
		Disconnect = 1,
		// Token: 0x040016CA RID: 5834
		ReuseSocket = 2,
		// Token: 0x040016CB RID: 5835
		WriteBehind = 4,
		// Token: 0x040016CC RID: 5836
		UseSystemThread = 16,
		// Token: 0x040016CD RID: 5837
		UseKernelApc = 32
	}
}
