﻿using System;

namespace System.Net.Sockets
{
	// Token: 0x020003F0 RID: 1008
	public class MulticastOption
	{
		// Token: 0x060022D1 RID: 8913 RVA: 0x00066248 File Offset: 0x00064448
		public MulticastOption(IPAddress group) : this(group, IPAddress.Any)
		{
		}

		// Token: 0x060022D2 RID: 8914 RVA: 0x00066258 File Offset: 0x00064458
		public MulticastOption(IPAddress group, int interfaceIndex)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			if (interfaceIndex < 0 || interfaceIndex > 16777215)
			{
				throw new ArgumentOutOfRangeException("interfaceIndex");
			}
			this.group = group;
			this.iface_index = interfaceIndex;
		}

		// Token: 0x060022D3 RID: 8915 RVA: 0x000662A8 File Offset: 0x000644A8
		public MulticastOption(IPAddress group, IPAddress mcint)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			if (mcint == null)
			{
				throw new ArgumentNullException("mcint");
			}
			this.group = group;
			this.local = mcint;
		}

		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x060022D4 RID: 8916 RVA: 0x000662EC File Offset: 0x000644EC
		// (set) Token: 0x060022D5 RID: 8917 RVA: 0x000662F4 File Offset: 0x000644F4
		public IPAddress Group
		{
			get
			{
				return this.group;
			}
			set
			{
				this.group = value;
			}
		}

		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x060022D6 RID: 8918 RVA: 0x00066300 File Offset: 0x00064500
		// (set) Token: 0x060022D7 RID: 8919 RVA: 0x00066308 File Offset: 0x00064508
		public IPAddress LocalAddress
		{
			get
			{
				return this.local;
			}
			set
			{
				this.local = value;
				this.iface_index = 0;
			}
		}

		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x060022D8 RID: 8920 RVA: 0x00066318 File Offset: 0x00064518
		// (set) Token: 0x060022D9 RID: 8921 RVA: 0x00066320 File Offset: 0x00064520
		public int InterfaceIndex
		{
			get
			{
				return this.iface_index;
			}
			set
			{
				if (value < 0 || value > 16777215)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.iface_index = value;
				this.local = null;
			}
		}

		// Token: 0x04001582 RID: 5506
		private IPAddress group;

		// Token: 0x04001583 RID: 5507
		private IPAddress local;

		// Token: 0x04001584 RID: 5508
		private int iface_index;
	}
}
