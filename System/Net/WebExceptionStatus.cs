﻿using System;

namespace System.Net
{
	// Token: 0x02000418 RID: 1048
	public enum WebExceptionStatus
	{
		// Token: 0x04001752 RID: 5970
		Success,
		// Token: 0x04001753 RID: 5971
		NameResolutionFailure,
		// Token: 0x04001754 RID: 5972
		ConnectFailure,
		// Token: 0x04001755 RID: 5973
		ReceiveFailure,
		// Token: 0x04001756 RID: 5974
		SendFailure,
		// Token: 0x04001757 RID: 5975
		PipelineFailure,
		// Token: 0x04001758 RID: 5976
		RequestCanceled,
		// Token: 0x04001759 RID: 5977
		ProtocolError,
		// Token: 0x0400175A RID: 5978
		ConnectionClosed,
		// Token: 0x0400175B RID: 5979
		TrustFailure,
		// Token: 0x0400175C RID: 5980
		SecureChannelFailure,
		// Token: 0x0400175D RID: 5981
		ServerProtocolViolation,
		// Token: 0x0400175E RID: 5982
		KeepAliveFailure,
		// Token: 0x0400175F RID: 5983
		Pending,
		// Token: 0x04001760 RID: 5984
		Timeout,
		// Token: 0x04001761 RID: 5985
		ProxyNameResolutionFailure,
		// Token: 0x04001762 RID: 5986
		UnknownError,
		// Token: 0x04001763 RID: 5987
		MessageLengthLimitExceeded,
		// Token: 0x04001764 RID: 5988
		CacheEntryNotFound,
		// Token: 0x04001765 RID: 5989
		RequestProhibitedByCachePolicy,
		// Token: 0x04001766 RID: 5990
		RequestProhibitedByProxy
	}
}
