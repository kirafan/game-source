﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C6 RID: 1222
	public class UploadStringCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BCE RID: 11214 RVA: 0x00098D5C File Offset: 0x00096F5C
		internal UploadStringCompletedEventArgs(string result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BFD RID: 3069
		// (get) Token: 0x06002BCF RID: 11215 RVA: 0x00098D70 File Offset: 0x00096F70
		public string Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B9A RID: 7066
		private string result;
	}
}
