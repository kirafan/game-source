﻿using System;

namespace System.Net
{
	// Token: 0x02000522 RID: 1314
	// (Invoke) Token: 0x06002D34 RID: 11572
	public delegate void UploadFileCompletedEventHandler(object sender, UploadFileCompletedEventArgs e);
}
