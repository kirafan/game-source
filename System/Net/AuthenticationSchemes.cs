﻿using System;

namespace System.Net
{
	// Token: 0x020002BA RID: 698
	[Flags]
	public enum AuthenticationSchemes
	{
		// Token: 0x04000F6D RID: 3949
		None = 0,
		// Token: 0x04000F6E RID: 3950
		Digest = 1,
		// Token: 0x04000F6F RID: 3951
		Negotiate = 2,
		// Token: 0x04000F70 RID: 3952
		Ntlm = 4,
		// Token: 0x04000F71 RID: 3953
		IntegratedWindowsAuthentication = 6,
		// Token: 0x04000F72 RID: 3954
		Basic = 8,
		// Token: 0x04000F73 RID: 3955
		Anonymous = 32768
	}
}
