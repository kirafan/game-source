﻿using System;
using Mono.Http;

namespace System.Net
{
	// Token: 0x020003D8 RID: 984
	internal class NtlmClient : IAuthenticationModule
	{
		// Token: 0x060021A7 RID: 8615 RVA: 0x000629AC File Offset: 0x00060BAC
		public NtlmClient()
		{
			this.authObject = new NtlmClient();
		}

		// Token: 0x060021A8 RID: 8616 RVA: 0x000629C0 File Offset: 0x00060BC0
		public Authorization Authenticate(string challenge, WebRequest webRequest, ICredentials credentials)
		{
			if (this.authObject == null)
			{
				return null;
			}
			return this.authObject.Authenticate(challenge, webRequest, credentials);
		}

		// Token: 0x060021A9 RID: 8617 RVA: 0x000629E0 File Offset: 0x00060BE0
		public Authorization PreAuthenticate(WebRequest webRequest, ICredentials credentials)
		{
			return null;
		}

		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x060021AA RID: 8618 RVA: 0x000629E4 File Offset: 0x00060BE4
		public string AuthenticationType
		{
			get
			{
				return "NTLM";
			}
		}

		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x060021AB RID: 8619 RVA: 0x000629EC File Offset: 0x00060BEC
		public bool CanPreAuthenticate
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040014E7 RID: 5351
		private IAuthenticationModule authObject;
	}
}
