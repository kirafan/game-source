﻿using System;

namespace System.Net
{
	// Token: 0x02000528 RID: 1320
	// (Invoke) Token: 0x06002D4C RID: 11596
	public delegate void UploadDataCompletedEventHandler(object sender, UploadDataCompletedEventArgs e);
}
