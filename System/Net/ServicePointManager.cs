﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Configuration;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using Mono.Security.Protocol.Tls;
using Mono.Security.X509;
using Mono.Security.X509.Extensions;

namespace System.Net
{
	// Token: 0x020003E5 RID: 997
	public class ServicePointManager
	{
		// Token: 0x0600226D RID: 8813 RVA: 0x0006431C File Offset: 0x0006251C
		private ServicePointManager()
		{
		}

		// Token: 0x0600226E RID: 8814 RVA: 0x00064324 File Offset: 0x00062524
		static ServicePointManager()
		{
			object section = ConfigurationManager.GetSection("system.net/connectionManagement");
			System.Net.Configuration.ConnectionManagementSection connectionManagementSection = section as System.Net.Configuration.ConnectionManagementSection;
			if (connectionManagementSection != null)
			{
				ServicePointManager.manager = new ConnectionManagementData(null);
				foreach (object obj in connectionManagementSection.ConnectionManagement)
				{
					System.Net.Configuration.ConnectionManagementElement connectionManagementElement = (System.Net.Configuration.ConnectionManagementElement)obj;
					ServicePointManager.manager.Add(connectionManagementElement.Address, connectionManagementElement.MaxConnection);
				}
				ServicePointManager.defaultConnectionLimit = (int)ServicePointManager.manager.GetMaxConnections("*");
				return;
			}
			ServicePointManager.manager = (ConnectionManagementData)System.Configuration.ConfigurationSettings.GetConfig("system.net/connectionManagement");
			if (ServicePointManager.manager != null)
			{
				ServicePointManager.defaultConnectionLimit = (int)ServicePointManager.manager.GetMaxConnections("*");
			}
		}

		// Token: 0x170009F9 RID: 2553
		// (get) Token: 0x0600226F RID: 8815 RVA: 0x00064450 File Offset: 0x00062650
		// (set) Token: 0x06002270 RID: 8816 RVA: 0x00064458 File Offset: 0x00062658
		[Obsolete("Use ServerCertificateValidationCallback instead", false)]
		public static ICertificatePolicy CertificatePolicy
		{
			get
			{
				return ServicePointManager.policy;
			}
			set
			{
				ServicePointManager.policy = value;
			}
		}

		// Token: 0x170009FA RID: 2554
		// (get) Token: 0x06002271 RID: 8817 RVA: 0x00064460 File Offset: 0x00062660
		// (set) Token: 0x06002272 RID: 8818 RVA: 0x00064468 File Offset: 0x00062668
		[MonoTODO("CRL checks not implemented")]
		public static bool CheckCertificateRevocationList
		{
			get
			{
				return ServicePointManager._checkCRL;
			}
			set
			{
				ServicePointManager._checkCRL = false;
			}
		}

		// Token: 0x170009FB RID: 2555
		// (get) Token: 0x06002273 RID: 8819 RVA: 0x00064470 File Offset: 0x00062670
		// (set) Token: 0x06002274 RID: 8820 RVA: 0x00064478 File Offset: 0x00062678
		public static int DefaultConnectionLimit
		{
			get
			{
				return ServicePointManager.defaultConnectionLimit;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				ServicePointManager.defaultConnectionLimit = value;
			}
		}

		// Token: 0x06002275 RID: 8821 RVA: 0x00064494 File Offset: 0x00062694
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		// Token: 0x170009FC RID: 2556
		// (get) Token: 0x06002276 RID: 8822 RVA: 0x0006449C File Offset: 0x0006269C
		// (set) Token: 0x06002277 RID: 8823 RVA: 0x000644A4 File Offset: 0x000626A4
		[MonoTODO]
		public static int DnsRefreshTimeout
		{
			get
			{
				throw ServicePointManager.GetMustImplement();
			}
			set
			{
				throw ServicePointManager.GetMustImplement();
			}
		}

		// Token: 0x170009FD RID: 2557
		// (get) Token: 0x06002278 RID: 8824 RVA: 0x000644AC File Offset: 0x000626AC
		// (set) Token: 0x06002279 RID: 8825 RVA: 0x000644B4 File Offset: 0x000626B4
		[MonoTODO]
		public static bool EnableDnsRoundRobin
		{
			get
			{
				throw ServicePointManager.GetMustImplement();
			}
			set
			{
				throw ServicePointManager.GetMustImplement();
			}
		}

		// Token: 0x170009FE RID: 2558
		// (get) Token: 0x0600227A RID: 8826 RVA: 0x000644BC File Offset: 0x000626BC
		// (set) Token: 0x0600227B RID: 8827 RVA: 0x000644C4 File Offset: 0x000626C4
		public static int MaxServicePointIdleTime
		{
			get
			{
				return ServicePointManager.maxServicePointIdleTime;
			}
			set
			{
				if (value < -2 || value > 2147483647)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				ServicePointManager.maxServicePointIdleTime = value;
			}
		}

		// Token: 0x170009FF RID: 2559
		// (get) Token: 0x0600227C RID: 8828 RVA: 0x000644F8 File Offset: 0x000626F8
		// (set) Token: 0x0600227D RID: 8829 RVA: 0x00064500 File Offset: 0x00062700
		public static int MaxServicePoints
		{
			get
			{
				return ServicePointManager.maxServicePoints;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("value");
				}
				ServicePointManager.maxServicePoints = value;
				ServicePointManager.RecycleServicePoints();
			}
		}

		// Token: 0x17000A00 RID: 2560
		// (get) Token: 0x0600227E RID: 8830 RVA: 0x00064520 File Offset: 0x00062720
		// (set) Token: 0x0600227F RID: 8831 RVA: 0x00064528 File Offset: 0x00062728
		public static SecurityProtocolType SecurityProtocol
		{
			get
			{
				return ServicePointManager._securityProtocol;
			}
			set
			{
				ServicePointManager._securityProtocol = value;
			}
		}

		// Token: 0x17000A01 RID: 2561
		// (get) Token: 0x06002280 RID: 8832 RVA: 0x00064530 File Offset: 0x00062730
		// (set) Token: 0x06002281 RID: 8833 RVA: 0x00064538 File Offset: 0x00062738
		public static System.Net.Security.RemoteCertificateValidationCallback ServerCertificateValidationCallback
		{
			get
			{
				return ServicePointManager.server_cert_cb;
			}
			set
			{
				ServicePointManager.server_cert_cb = value;
			}
		}

		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x06002282 RID: 8834 RVA: 0x00064540 File Offset: 0x00062740
		// (set) Token: 0x06002283 RID: 8835 RVA: 0x00064548 File Offset: 0x00062748
		public static bool Expect100Continue
		{
			get
			{
				return ServicePointManager.expectContinue;
			}
			set
			{
				ServicePointManager.expectContinue = value;
			}
		}

		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x06002284 RID: 8836 RVA: 0x00064550 File Offset: 0x00062750
		// (set) Token: 0x06002285 RID: 8837 RVA: 0x00064558 File Offset: 0x00062758
		public static bool UseNagleAlgorithm
		{
			get
			{
				return ServicePointManager.useNagle;
			}
			set
			{
				ServicePointManager.useNagle = value;
			}
		}

		// Token: 0x06002286 RID: 8838 RVA: 0x00064560 File Offset: 0x00062760
		public static ServicePoint FindServicePoint(System.Uri address)
		{
			return ServicePointManager.FindServicePoint(address, GlobalProxySelection.Select);
		}

		// Token: 0x06002287 RID: 8839 RVA: 0x00064570 File Offset: 0x00062770
		public static ServicePoint FindServicePoint(string uriString, IWebProxy proxy)
		{
			return ServicePointManager.FindServicePoint(new System.Uri(uriString), proxy);
		}

		// Token: 0x06002288 RID: 8840 RVA: 0x00064580 File Offset: 0x00062780
		public static ServicePoint FindServicePoint(System.Uri address, IWebProxy proxy)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			ServicePointManager.RecycleServicePoints();
			bool usesProxy = false;
			bool flag = false;
			if (proxy != null && !proxy.IsBypassed(address))
			{
				usesProxy = true;
				bool flag2 = address.Scheme == "https";
				address = proxy.GetProxy(address);
				if (address.Scheme != "http" && !flag2)
				{
					throw new NotSupportedException("Proxy scheme not supported.");
				}
				if (flag2 && address.Scheme == "http")
				{
					flag = true;
				}
			}
			address = new System.Uri(address.Scheme + "://" + address.Authority);
			ServicePoint servicePoint = null;
			System.Collections.Specialized.HybridDictionary obj = ServicePointManager.servicePoints;
			lock (obj)
			{
				ServicePointManager.SPKey key = new ServicePointManager.SPKey(address, flag);
				servicePoint = (ServicePointManager.servicePoints[key] as ServicePoint);
				if (servicePoint != null)
				{
					return servicePoint;
				}
				if (ServicePointManager.maxServicePoints > 0 && ServicePointManager.servicePoints.Count >= ServicePointManager.maxServicePoints)
				{
					throw new InvalidOperationException("maximum number of service points reached");
				}
				string hostOrIP = address.ToString();
				int maxConnections = (int)ServicePointManager.manager.GetMaxConnections(hostOrIP);
				servicePoint = new ServicePoint(address, maxConnections, ServicePointManager.maxServicePointIdleTime);
				servicePoint.Expect100Continue = ServicePointManager.expectContinue;
				servicePoint.UseNagleAlgorithm = ServicePointManager.useNagle;
				servicePoint.UsesProxy = usesProxy;
				servicePoint.UseConnect = flag;
				ServicePointManager.servicePoints.Add(key, servicePoint);
			}
			return servicePoint;
		}

		// Token: 0x06002289 RID: 8841 RVA: 0x00064720 File Offset: 0x00062920
		internal static void RecycleServicePoints()
		{
			ArrayList arrayList = new ArrayList();
			System.Collections.Specialized.HybridDictionary obj = ServicePointManager.servicePoints;
			lock (obj)
			{
				IDictionaryEnumerator enumerator = ServicePointManager.servicePoints.GetEnumerator();
				while (enumerator.MoveNext())
				{
					ServicePoint servicePoint = (ServicePoint)enumerator.Value;
					if (servicePoint.AvailableForRecycling)
					{
						arrayList.Add(enumerator.Key);
					}
				}
				for (int i = 0; i < arrayList.Count; i++)
				{
					ServicePointManager.servicePoints.Remove(arrayList[i]);
				}
				if (ServicePointManager.maxServicePoints != 0 && ServicePointManager.servicePoints.Count > ServicePointManager.maxServicePoints)
				{
					SortedList sortedList = new SortedList(ServicePointManager.servicePoints.Count);
					enumerator = ServicePointManager.servicePoints.GetEnumerator();
					while (enumerator.MoveNext())
					{
						ServicePoint servicePoint2 = (ServicePoint)enumerator.Value;
						if (servicePoint2.CurrentConnections == 0)
						{
							while (sortedList.ContainsKey(servicePoint2.IdleSince))
							{
								servicePoint2.IdleSince = servicePoint2.IdleSince.AddMilliseconds(1.0);
							}
							sortedList.Add(servicePoint2.IdleSince, servicePoint2.Address);
						}
					}
					int num = 0;
					while (num < sortedList.Count && ServicePointManager.servicePoints.Count > ServicePointManager.maxServicePoints)
					{
						ServicePointManager.servicePoints.Remove(sortedList.GetByIndex(num));
						num++;
					}
				}
			}
		}

		// Token: 0x0400151C RID: 5404
		public const int DefaultNonPersistentConnectionLimit = 4;

		// Token: 0x0400151D RID: 5405
		public const int DefaultPersistentConnectionLimit = 2;

		// Token: 0x0400151E RID: 5406
		private const string configKey = "system.net/connectionManagement";

		// Token: 0x0400151F RID: 5407
		private static System.Collections.Specialized.HybridDictionary servicePoints = new System.Collections.Specialized.HybridDictionary();

		// Token: 0x04001520 RID: 5408
		private static ICertificatePolicy policy = new DefaultCertificatePolicy();

		// Token: 0x04001521 RID: 5409
		private static int defaultConnectionLimit = 2;

		// Token: 0x04001522 RID: 5410
		private static int maxServicePointIdleTime = 900000;

		// Token: 0x04001523 RID: 5411
		private static int maxServicePoints = 0;

		// Token: 0x04001524 RID: 5412
		private static bool _checkCRL = false;

		// Token: 0x04001525 RID: 5413
		private static SecurityProtocolType _securityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls;

		// Token: 0x04001526 RID: 5414
		private static bool expectContinue = true;

		// Token: 0x04001527 RID: 5415
		private static bool useNagle;

		// Token: 0x04001528 RID: 5416
		private static System.Net.Security.RemoteCertificateValidationCallback server_cert_cb;

		// Token: 0x04001529 RID: 5417
		private static ConnectionManagementData manager;

		// Token: 0x020003E6 RID: 998
		private class SPKey
		{
			// Token: 0x0600228A RID: 8842 RVA: 0x000648D8 File Offset: 0x00062AD8
			public SPKey(System.Uri uri, bool use_connect)
			{
				this.uri = uri;
				this.use_connect = use_connect;
			}

			// Token: 0x17000A04 RID: 2564
			// (get) Token: 0x0600228B RID: 8843 RVA: 0x000648F0 File Offset: 0x00062AF0
			public System.Uri Uri
			{
				get
				{
					return this.uri;
				}
			}

			// Token: 0x17000A05 RID: 2565
			// (get) Token: 0x0600228C RID: 8844 RVA: 0x000648F8 File Offset: 0x00062AF8
			public bool UseConnect
			{
				get
				{
					return this.use_connect;
				}
			}

			// Token: 0x0600228D RID: 8845 RVA: 0x00064900 File Offset: 0x00062B00
			public override int GetHashCode()
			{
				return this.uri.GetHashCode() + ((!this.use_connect) ? 0 : 1);
			}

			// Token: 0x0600228E RID: 8846 RVA: 0x00064920 File Offset: 0x00062B20
			public override bool Equals(object obj)
			{
				ServicePointManager.SPKey spkey = obj as ServicePointManager.SPKey;
				return obj != null && this.uri.Equals(spkey.uri) && spkey.use_connect == this.use_connect;
			}

			// Token: 0x0400152A RID: 5418
			private System.Uri uri;

			// Token: 0x0400152B RID: 5419
			private bool use_connect;
		}

		// Token: 0x020003E7 RID: 999
		internal class ChainValidationHelper
		{
			// Token: 0x0600228F RID: 8847 RVA: 0x00064964 File Offset: 0x00062B64
			public ChainValidationHelper(object sender)
			{
				this.sender = sender;
			}

			// Token: 0x17000A06 RID: 2566
			// (get) Token: 0x06002291 RID: 8849 RVA: 0x00064990 File Offset: 0x00062B90
			// (set) Token: 0x06002292 RID: 8850 RVA: 0x000649DC File Offset: 0x00062BDC
			public string Host
			{
				get
				{
					if (this.host == null && this.sender is HttpWebRequest)
					{
						this.host = ((HttpWebRequest)this.sender).Address.Host;
					}
					return this.host;
				}
				set
				{
					this.host = value;
				}
			}

			// Token: 0x06002293 RID: 8851 RVA: 0x000649E8 File Offset: 0x00062BE8
			internal ValidationResult ValidateChain(Mono.Security.X509.X509CertificateCollection certs)
			{
				bool user_denied = false;
				if (certs == null || certs.Count == 0)
				{
					return null;
				}
				ICertificatePolicy certificatePolicy = ServicePointManager.CertificatePolicy;
				System.Net.Security.RemoteCertificateValidationCallback serverCertificateValidationCallback = ServicePointManager.ServerCertificateValidationCallback;
				System.Security.Cryptography.X509Certificates.X509Chain x509Chain = new System.Security.Cryptography.X509Certificates.X509Chain();
				x509Chain.ChainPolicy = new System.Security.Cryptography.X509Certificates.X509ChainPolicy();
				for (int i = 1; i < certs.Count; i++)
				{
					System.Security.Cryptography.X509Certificates.X509Certificate2 certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(certs[i].RawData);
					x509Chain.ChainPolicy.ExtraStore.Add(certificate);
				}
				System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(certs[0].RawData);
				int num = 0;
				System.Net.Security.SslPolicyErrors sslPolicyErrors = System.Net.Security.SslPolicyErrors.None;
				try
				{
					if (!x509Chain.Build(x509Certificate))
					{
						sslPolicyErrors |= ServicePointManager.ChainValidationHelper.GetErrorsFromChain(x509Chain);
					}
				}
				catch (Exception arg)
				{
					Console.Error.WriteLine("ERROR building certificate chain: {0}", arg);
					Console.Error.WriteLine("Please, report this problem to the Mono team");
					sslPolicyErrors |= System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors;
				}
				if (!ServicePointManager.ChainValidationHelper.CheckCertificateUsage(x509Certificate))
				{
					sslPolicyErrors |= System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors;
					num = -2146762490;
				}
				if (!ServicePointManager.ChainValidationHelper.CheckServerIdentity(certs[0], this.Host))
				{
					sslPolicyErrors |= System.Net.Security.SslPolicyErrors.RemoteCertificateNameMismatch;
					num = -2146762481;
				}
				bool flag = false;
				if (ServicePointManager.ChainValidationHelper.is_macosx)
				{
					try
					{
						OSX509Certificates.SecTrustResult secTrustResult = OSX509Certificates.TrustEvaluateSsl(certs);
						flag = (secTrustResult == OSX509Certificates.SecTrustResult.Proceed || secTrustResult == OSX509Certificates.SecTrustResult.Unspecified);
					}
					catch
					{
					}
					if (flag)
					{
						num = 0;
						sslPolicyErrors = System.Net.Security.SslPolicyErrors.None;
					}
				}
				if (certificatePolicy != null && (!(certificatePolicy is DefaultCertificatePolicy) || serverCertificateValidationCallback == null))
				{
					ServicePoint srvPoint = null;
					HttpWebRequest httpWebRequest = this.sender as HttpWebRequest;
					if (httpWebRequest != null)
					{
						srvPoint = httpWebRequest.ServicePoint;
					}
					if (num == 0 && sslPolicyErrors != System.Net.Security.SslPolicyErrors.None)
					{
						num = ServicePointManager.ChainValidationHelper.GetStatusFromChain(x509Chain);
					}
					flag = certificatePolicy.CheckValidationResult(srvPoint, x509Certificate, httpWebRequest, num);
					user_denied = (!flag && !(certificatePolicy is DefaultCertificatePolicy));
				}
				if (serverCertificateValidationCallback != null)
				{
					flag = serverCertificateValidationCallback(this.sender, x509Certificate, x509Chain, sslPolicyErrors);
					user_denied = !flag;
				}
				return new ValidationResult(flag, user_denied, num);
			}

			// Token: 0x06002294 RID: 8852 RVA: 0x00064C1C File Offset: 0x00062E1C
			private static int GetStatusFromChain(System.Security.Cryptography.X509Certificates.X509Chain chain)
			{
				long num = 0L;
				foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus x509ChainStatus in chain.ChainStatus)
				{
					System.Security.Cryptography.X509Certificates.X509ChainStatusFlags status = x509ChainStatus.Status;
					if (status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
					{
						if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NotTimeValid) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762495);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NotTimeNested) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762494);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.Revoked) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762484);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NotSignatureValid) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146869244);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NotValidForUsage) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762480);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.UntrustedRoot) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762487);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.RevocationStatusUnknown) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146885614);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.Cyclic) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762486);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.InvalidExtension) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762485);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.InvalidPolicyConstraints) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762483);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.InvalidBasicConstraints) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146869223);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.InvalidNameConstraints) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762476);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.HasNotSupportedNameConstraint) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762476);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.HasNotDefinedNameConstraint) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762476);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.HasNotPermittedNameConstraint) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762476);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.HasExcludedNameConstraint) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762476);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.PartialChain) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762486);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.CtlNotTimeValid) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762495);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.CtlNotSignatureValid) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146869244);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.CtlNotValidForUsage) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762480);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.OfflineRevocation) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146885614);
						}
						else if ((status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoIssuanceChainPolicy) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							num = (long)((ulong)-2146762489);
						}
						else
						{
							num = (long)((ulong)-2146762485);
						}
						break;
					}
				}
				return (int)num;
			}

			// Token: 0x06002295 RID: 8853 RVA: 0x00064E84 File Offset: 0x00063084
			private static System.Net.Security.SslPolicyErrors GetErrorsFromChain(System.Security.Cryptography.X509Certificates.X509Chain chain)
			{
				System.Net.Security.SslPolicyErrors sslPolicyErrors = System.Net.Security.SslPolicyErrors.None;
				foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus x509ChainStatus in chain.ChainStatus)
				{
					if (x509ChainStatus.Status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
					{
						sslPolicyErrors |= System.Net.Security.SslPolicyErrors.RemoteCertificateChainErrors;
						break;
					}
				}
				return sslPolicyErrors;
			}

			// Token: 0x06002296 RID: 8854 RVA: 0x00064ED8 File Offset: 0x000630D8
			private static bool CheckCertificateUsage(System.Security.Cryptography.X509Certificates.X509Certificate2 cert)
			{
				bool result;
				try
				{
					if (cert.Version < 3)
					{
						result = true;
					}
					else
					{
						System.Security.Cryptography.X509Certificates.X509KeyUsageExtension x509KeyUsageExtension = (System.Security.Cryptography.X509Certificates.X509KeyUsageExtension)cert.Extensions["2.5.29.15"];
						System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension x509EnhancedKeyUsageExtension = (System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension)cert.Extensions["2.5.29.37"];
						if (x509KeyUsageExtension != null && x509EnhancedKeyUsageExtension != null)
						{
							if ((x509KeyUsageExtension.KeyUsages & ServicePointManager.ChainValidationHelper.s_flags) == System.Security.Cryptography.X509Certificates.X509KeyUsageFlags.None)
							{
								result = false;
							}
							else
							{
								result = (x509EnhancedKeyUsageExtension.EnhancedKeyUsages["1.3.6.1.5.5.7.3.1"] != null || x509EnhancedKeyUsageExtension.EnhancedKeyUsages["2.16.840.1.113730.4.1"] != null);
							}
						}
						else if (x509KeyUsageExtension != null)
						{
							result = ((x509KeyUsageExtension.KeyUsages & ServicePointManager.ChainValidationHelper.s_flags) != System.Security.Cryptography.X509Certificates.X509KeyUsageFlags.None);
						}
						else if (x509EnhancedKeyUsageExtension != null)
						{
							result = (x509EnhancedKeyUsageExtension.EnhancedKeyUsages["1.3.6.1.5.5.7.3.1"] != null || x509EnhancedKeyUsageExtension.EnhancedKeyUsages["2.16.840.1.113730.4.1"] != null);
						}
						else
						{
							System.Security.Cryptography.X509Certificates.X509Extension x509Extension = cert.Extensions["2.16.840.1.113730.1.1"];
							if (x509Extension != null)
							{
								string text = x509Extension.NetscapeCertType(false);
								result = (text.IndexOf("SSL Server Authentication") != -1);
							}
							else
							{
								result = true;
							}
						}
					}
				}
				catch (Exception arg)
				{
					Console.Error.WriteLine("ERROR processing certificate: {0}", arg);
					Console.Error.WriteLine("Please, report this problem to the Mono team");
					result = false;
				}
				return result;
			}

			// Token: 0x06002297 RID: 8855 RVA: 0x0006506C File Offset: 0x0006326C
			private static bool CheckServerIdentity(Mono.Security.X509.X509Certificate cert, string targetHost)
			{
				bool result;
				try
				{
					Mono.Security.X509.X509Extension x509Extension = cert.Extensions["2.5.29.17"];
					if (x509Extension != null)
					{
						SubjectAltNameExtension subjectAltNameExtension = new SubjectAltNameExtension(x509Extension);
						foreach (string pattern in subjectAltNameExtension.DNSNames)
						{
							if (ServicePointManager.ChainValidationHelper.Match(targetHost, pattern))
							{
								return true;
							}
						}
						foreach (string a in subjectAltNameExtension.IPAddresses)
						{
							if (a == targetHost)
							{
								return true;
							}
						}
					}
					result = ServicePointManager.ChainValidationHelper.CheckDomainName(cert.SubjectName, targetHost);
				}
				catch (Exception arg)
				{
					Console.Error.WriteLine("ERROR processing certificate: {0}", arg);
					Console.Error.WriteLine("Please, report this problem to the Mono team");
					result = false;
				}
				return result;
			}

			// Token: 0x06002298 RID: 8856 RVA: 0x00065174 File Offset: 0x00063374
			private static bool CheckDomainName(string subjectName, string targetHost)
			{
				string pattern = string.Empty;
				System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("CN\\s*=\\s*([^,]*)");
				System.Text.RegularExpressions.MatchCollection matchCollection = regex.Matches(subjectName);
				if (matchCollection.Count == 1 && matchCollection[0].Success)
				{
					pattern = matchCollection[0].Groups[1].Value.ToString();
				}
				return ServicePointManager.ChainValidationHelper.Match(targetHost, pattern);
			}

			// Token: 0x06002299 RID: 8857 RVA: 0x000651DC File Offset: 0x000633DC
			private static bool Match(string hostname, string pattern)
			{
				int num = pattern.IndexOf('*');
				if (num == -1)
				{
					return string.Compare(hostname, pattern, true, CultureInfo.InvariantCulture) == 0;
				}
				if (num != pattern.Length - 1 && pattern[num + 1] != '.')
				{
					return false;
				}
				int num2 = pattern.IndexOf('*', num + 1);
				if (num2 != -1)
				{
					return false;
				}
				string text = pattern.Substring(num + 1);
				int num3 = hostname.Length - text.Length;
				if (num3 <= 0)
				{
					return false;
				}
				if (string.Compare(hostname, num3, text, 0, text.Length, true, CultureInfo.InvariantCulture) != 0)
				{
					return false;
				}
				if (num == 0)
				{
					int num4 = hostname.IndexOf('.');
					return num4 == -1 || num4 >= hostname.Length - text.Length;
				}
				string text2 = pattern.Substring(0, num);
				return string.Compare(hostname, 0, text2, 0, text2.Length, true, CultureInfo.InvariantCulture) == 0;
			}

			// Token: 0x0400152C RID: 5420
			private object sender;

			// Token: 0x0400152D RID: 5421
			private string host;

			// Token: 0x0400152E RID: 5422
			private static bool is_macosx = File.Exists("/System/Library/Frameworks/Security.framework/Security");

			// Token: 0x0400152F RID: 5423
			private static System.Security.Cryptography.X509Certificates.X509KeyUsageFlags s_flags = System.Security.Cryptography.X509Certificates.X509KeyUsageFlags.KeyAgreement | System.Security.Cryptography.X509Certificates.X509KeyUsageFlags.KeyEncipherment | System.Security.Cryptography.X509Certificates.X509KeyUsageFlags.DigitalSignature;
		}
	}
}
