﻿using System;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x02000417 RID: 1047
	[Serializable]
	public class WebException : InvalidOperationException, ISerializable
	{
		// Token: 0x060025AB RID: 9643 RVA: 0x00074FDC File Offset: 0x000731DC
		public WebException()
		{
		}

		// Token: 0x060025AC RID: 9644 RVA: 0x00074FEC File Offset: 0x000731EC
		public WebException(string message) : base(message)
		{
		}

		// Token: 0x060025AD RID: 9645 RVA: 0x00075000 File Offset: 0x00073200
		protected WebException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060025AE RID: 9646 RVA: 0x00075014 File Offset: 0x00073214
		public WebException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060025AF RID: 9647 RVA: 0x00075028 File Offset: 0x00073228
		public WebException(string message, WebExceptionStatus status) : base(message)
		{
			this.status = status;
		}

		// Token: 0x060025B0 RID: 9648 RVA: 0x00075040 File Offset: 0x00073240
		internal WebException(string message, Exception innerException, WebExceptionStatus status) : base(message, innerException)
		{
			this.status = status;
		}

		// Token: 0x060025B1 RID: 9649 RVA: 0x0007505C File Offset: 0x0007325C
		public WebException(string message, Exception innerException, WebExceptionStatus status, WebResponse response) : base(message, innerException)
		{
			this.status = status;
			this.response = response;
		}

		// Token: 0x060025B2 RID: 9650 RVA: 0x00075080 File Offset: 0x00073280
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		// Token: 0x17000AAD RID: 2733
		// (get) Token: 0x060025B3 RID: 9651 RVA: 0x0007508C File Offset: 0x0007328C
		public WebResponse Response
		{
			get
			{
				return this.response;
			}
		}

		// Token: 0x17000AAE RID: 2734
		// (get) Token: 0x060025B4 RID: 9652 RVA: 0x00075094 File Offset: 0x00073294
		public WebExceptionStatus Status
		{
			get
			{
				return this.status;
			}
		}

		// Token: 0x060025B5 RID: 9653 RVA: 0x0007509C File Offset: 0x0007329C
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
		}

		// Token: 0x0400174F RID: 5967
		private WebResponse response;

		// Token: 0x04001750 RID: 5968
		private WebExceptionStatus status = WebExceptionStatus.UnknownError;
	}
}
