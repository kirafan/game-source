﻿using System;

namespace System.Net
{
	// Token: 0x02000327 RID: 807
	public interface ICredentials
	{
		// Token: 0x06001CA0 RID: 7328
		NetworkCredential GetCredential(System.Uri uri, string authType);
	}
}
