﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net
{
	// Token: 0x02000318 RID: 792
	public class HttpListenerPrefixCollection : IEnumerable, ICollection<string>, IEnumerable<string>
	{
		// Token: 0x06001B9A RID: 7066 RVA: 0x0004EE54 File Offset: 0x0004D054
		internal HttpListenerPrefixCollection(HttpListener listener)
		{
			this.listener = listener;
		}

		// Token: 0x06001B9B RID: 7067 RVA: 0x0004EE70 File Offset: 0x0004D070
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.prefixes.GetEnumerator();
		}

		// Token: 0x170006B4 RID: 1716
		// (get) Token: 0x06001B9C RID: 7068 RVA: 0x0004EE84 File Offset: 0x0004D084
		public int Count
		{
			get
			{
				return this.prefixes.Count;
			}
		}

		// Token: 0x170006B5 RID: 1717
		// (get) Token: 0x06001B9D RID: 7069 RVA: 0x0004EE94 File Offset: 0x0004D094
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006B6 RID: 1718
		// (get) Token: 0x06001B9E RID: 7070 RVA: 0x0004EE98 File Offset: 0x0004D098
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06001B9F RID: 7071 RVA: 0x0004EE9C File Offset: 0x0004D09C
		public void Add(string uriPrefix)
		{
			this.listener.CheckDisposed();
			ListenerPrefix.CheckUri(uriPrefix);
			if (this.prefixes.Contains(uriPrefix))
			{
				return;
			}
			this.prefixes.Add(uriPrefix);
			if (this.listener.IsListening)
			{
				EndPointManager.AddPrefix(uriPrefix, this.listener);
			}
		}

		// Token: 0x06001BA0 RID: 7072 RVA: 0x0004EEF4 File Offset: 0x0004D0F4
		public void Clear()
		{
			this.listener.CheckDisposed();
			this.prefixes.Clear();
			if (this.listener.IsListening)
			{
				EndPointManager.RemoveListener(this.listener);
			}
		}

		// Token: 0x06001BA1 RID: 7073 RVA: 0x0004EF28 File Offset: 0x0004D128
		public bool Contains(string uriPrefix)
		{
			this.listener.CheckDisposed();
			return this.prefixes.Contains(uriPrefix);
		}

		// Token: 0x06001BA2 RID: 7074 RVA: 0x0004EF44 File Offset: 0x0004D144
		public void CopyTo(string[] array, int offset)
		{
			this.listener.CheckDisposed();
			this.prefixes.CopyTo(array, offset);
		}

		// Token: 0x06001BA3 RID: 7075 RVA: 0x0004EF60 File Offset: 0x0004D160
		public void CopyTo(Array array, int offset)
		{
			this.listener.CheckDisposed();
			((ICollection)this.prefixes).CopyTo(array, offset);
		}

		// Token: 0x06001BA4 RID: 7076 RVA: 0x0004EF7C File Offset: 0x0004D17C
		public IEnumerator<string> GetEnumerator()
		{
			return this.prefixes.GetEnumerator();
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x0004EF90 File Offset: 0x0004D190
		public bool Remove(string uriPrefix)
		{
			this.listener.CheckDisposed();
			if (uriPrefix == null)
			{
				throw new ArgumentNullException("uriPrefix");
			}
			bool flag = this.prefixes.Remove(uriPrefix);
			if (flag && this.listener.IsListening)
			{
				EndPointManager.RemovePrefix(uriPrefix, this.listener);
			}
			return flag;
		}

		// Token: 0x04001113 RID: 4371
		private List<string> prefixes = new List<string>();

		// Token: 0x04001114 RID: 4372
		private HttpListener listener;
	}
}
