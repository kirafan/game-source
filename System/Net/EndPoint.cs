﻿using System;
using System.Net.Sockets;

namespace System.Net
{
	// Token: 0x020002FF RID: 767
	[Serializable]
	public abstract class EndPoint
	{
		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x06001A4C RID: 6732 RVA: 0x000493E0 File Offset: 0x000475E0
		public virtual System.Net.Sockets.AddressFamily AddressFamily
		{
			get
			{
				throw EndPoint.NotImplemented();
			}
		}

		// Token: 0x06001A4D RID: 6733 RVA: 0x000493E8 File Offset: 0x000475E8
		public virtual EndPoint Create(SocketAddress address)
		{
			throw EndPoint.NotImplemented();
		}

		// Token: 0x06001A4E RID: 6734 RVA: 0x000493F0 File Offset: 0x000475F0
		public virtual SocketAddress Serialize()
		{
			throw EndPoint.NotImplemented();
		}

		// Token: 0x06001A4F RID: 6735 RVA: 0x000493F8 File Offset: 0x000475F8
		private static Exception NotImplemented()
		{
			return new NotImplementedException();
		}
	}
}
