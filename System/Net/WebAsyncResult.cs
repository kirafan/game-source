﻿using System;
using System.IO;
using System.Threading;

namespace System.Net
{
	// Token: 0x0200040F RID: 1039
	internal class WebAsyncResult : IAsyncResult
	{
		// Token: 0x060024A2 RID: 9378 RVA: 0x0006E400 File Offset: 0x0006C600
		public WebAsyncResult(AsyncCallback cb, object state)
		{
			this.cb = cb;
			this.state = state;
		}

		// Token: 0x060024A3 RID: 9379 RVA: 0x0006E424 File Offset: 0x0006C624
		public WebAsyncResult(HttpWebRequest request, AsyncCallback cb, object state)
		{
			this.cb = cb;
			this.state = state;
		}

		// Token: 0x060024A4 RID: 9380 RVA: 0x0006E448 File Offset: 0x0006C648
		public WebAsyncResult(AsyncCallback cb, object state, byte[] buffer, int offset, int size)
		{
			this.cb = cb;
			this.state = state;
			this.buffer = buffer;
			this.offset = offset;
			this.size = size;
		}

		// Token: 0x060024A5 RID: 9381 RVA: 0x0006E48C File Offset: 0x0006C68C
		internal void SetCompleted(bool synch, Exception e)
		{
			this.synch = synch;
			this.exc = e;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
			}
		}

		// Token: 0x060024A6 RID: 9382 RVA: 0x0006E4FC File Offset: 0x0006C6FC
		internal void Reset()
		{
			this.callbackDone = false;
			this.exc = null;
			this.response = null;
			this.writeStream = null;
			this.exc = null;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = false;
				if (this.handle != null)
				{
					this.handle.Reset();
				}
			}
		}

		// Token: 0x060024A7 RID: 9383 RVA: 0x0006E580 File Offset: 0x0006C780
		internal void SetCompleted(bool synch, int nbytes)
		{
			this.synch = synch;
			this.nbytes = nbytes;
			this.exc = null;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
			}
		}

		// Token: 0x060024A8 RID: 9384 RVA: 0x0006E5F8 File Offset: 0x0006C7F8
		internal void SetCompleted(bool synch, Stream writeStream)
		{
			this.synch = synch;
			this.writeStream = writeStream;
			this.exc = null;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
			}
		}

		// Token: 0x060024A9 RID: 9385 RVA: 0x0006E670 File Offset: 0x0006C870
		internal void SetCompleted(bool synch, HttpWebResponse response)
		{
			this.synch = synch;
			this.response = response;
			this.exc = null;
			object obj = this.locker;
			lock (obj)
			{
				this.isCompleted = true;
				if (this.handle != null)
				{
					this.handle.Set();
				}
			}
		}

		// Token: 0x060024AA RID: 9386 RVA: 0x0006E6E8 File Offset: 0x0006C8E8
		internal void DoCallback()
		{
			if (!this.callbackDone && this.cb != null)
			{
				this.callbackDone = true;
				this.cb(this);
			}
		}

		// Token: 0x060024AB RID: 9387 RVA: 0x0006E714 File Offset: 0x0006C914
		internal void WaitUntilComplete()
		{
			if (this.IsCompleted)
			{
				return;
			}
			this.AsyncWaitHandle.WaitOne();
		}

		// Token: 0x060024AC RID: 9388 RVA: 0x0006E730 File Offset: 0x0006C930
		internal bool WaitUntilComplete(int timeout, bool exitContext)
		{
			return this.IsCompleted || this.AsyncWaitHandle.WaitOne(timeout, exitContext);
		}

		// Token: 0x17000A7C RID: 2684
		// (get) Token: 0x060024AD RID: 9389 RVA: 0x0006E758 File Offset: 0x0006C958
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000A7D RID: 2685
		// (get) Token: 0x060024AE RID: 9390 RVA: 0x0006E760 File Offset: 0x0006C960
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				object obj = this.locker;
				lock (obj)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.isCompleted);
					}
				}
				return this.handle;
			}
		}

		// Token: 0x17000A7E RID: 2686
		// (get) Token: 0x060024AF RID: 9391 RVA: 0x0006E7C4 File Offset: 0x0006C9C4
		public bool CompletedSynchronously
		{
			get
			{
				return this.synch;
			}
		}

		// Token: 0x17000A7F RID: 2687
		// (get) Token: 0x060024B0 RID: 9392 RVA: 0x0006E7CC File Offset: 0x0006C9CC
		public bool IsCompleted
		{
			get
			{
				object obj = this.locker;
				bool result;
				lock (obj)
				{
					result = this.isCompleted;
				}
				return result;
			}
		}

		// Token: 0x17000A80 RID: 2688
		// (get) Token: 0x060024B1 RID: 9393 RVA: 0x0006E81C File Offset: 0x0006CA1C
		internal bool GotException
		{
			get
			{
				return this.exc != null;
			}
		}

		// Token: 0x17000A81 RID: 2689
		// (get) Token: 0x060024B2 RID: 9394 RVA: 0x0006E82C File Offset: 0x0006CA2C
		internal Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x17000A82 RID: 2690
		// (get) Token: 0x060024B3 RID: 9395 RVA: 0x0006E834 File Offset: 0x0006CA34
		// (set) Token: 0x060024B4 RID: 9396 RVA: 0x0006E83C File Offset: 0x0006CA3C
		internal int NBytes
		{
			get
			{
				return this.nbytes;
			}
			set
			{
				this.nbytes = value;
			}
		}

		// Token: 0x17000A83 RID: 2691
		// (get) Token: 0x060024B5 RID: 9397 RVA: 0x0006E848 File Offset: 0x0006CA48
		// (set) Token: 0x060024B6 RID: 9398 RVA: 0x0006E850 File Offset: 0x0006CA50
		internal IAsyncResult InnerAsyncResult
		{
			get
			{
				return this.innerAsyncResult;
			}
			set
			{
				this.innerAsyncResult = value;
			}
		}

		// Token: 0x17000A84 RID: 2692
		// (get) Token: 0x060024B7 RID: 9399 RVA: 0x0006E85C File Offset: 0x0006CA5C
		internal Stream WriteStream
		{
			get
			{
				return this.writeStream;
			}
		}

		// Token: 0x17000A85 RID: 2693
		// (get) Token: 0x060024B8 RID: 9400 RVA: 0x0006E864 File Offset: 0x0006CA64
		internal HttpWebResponse Response
		{
			get
			{
				return this.response;
			}
		}

		// Token: 0x17000A86 RID: 2694
		// (get) Token: 0x060024B9 RID: 9401 RVA: 0x0006E86C File Offset: 0x0006CA6C
		internal byte[] Buffer
		{
			get
			{
				return this.buffer;
			}
		}

		// Token: 0x17000A87 RID: 2695
		// (get) Token: 0x060024BA RID: 9402 RVA: 0x0006E874 File Offset: 0x0006CA74
		internal int Offset
		{
			get
			{
				return this.offset;
			}
		}

		// Token: 0x17000A88 RID: 2696
		// (get) Token: 0x060024BB RID: 9403 RVA: 0x0006E87C File Offset: 0x0006CA7C
		internal int Size
		{
			get
			{
				return this.size;
			}
		}

		// Token: 0x040016D9 RID: 5849
		private ManualResetEvent handle;

		// Token: 0x040016DA RID: 5850
		private bool synch;

		// Token: 0x040016DB RID: 5851
		private bool isCompleted;

		// Token: 0x040016DC RID: 5852
		private AsyncCallback cb;

		// Token: 0x040016DD RID: 5853
		private object state;

		// Token: 0x040016DE RID: 5854
		private int nbytes;

		// Token: 0x040016DF RID: 5855
		private IAsyncResult innerAsyncResult;

		// Token: 0x040016E0 RID: 5856
		private bool callbackDone;

		// Token: 0x040016E1 RID: 5857
		private Exception exc;

		// Token: 0x040016E2 RID: 5858
		private HttpWebResponse response;

		// Token: 0x040016E3 RID: 5859
		private Stream writeStream;

		// Token: 0x040016E4 RID: 5860
		private byte[] buffer;

		// Token: 0x040016E5 RID: 5861
		private int offset;

		// Token: 0x040016E6 RID: 5862
		private int size;

		// Token: 0x040016E7 RID: 5863
		private object locker = new object();

		// Token: 0x040016E8 RID: 5864
		public bool EndCalled;

		// Token: 0x040016E9 RID: 5865
		public bool AsyncWriteAll;
	}
}
