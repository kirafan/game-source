﻿using System;

namespace System.Net
{
	// Token: 0x0200031C RID: 796
	public enum HttpRequestHeader
	{
		// Token: 0x0400113C RID: 4412
		CacheControl,
		// Token: 0x0400113D RID: 4413
		Connection,
		// Token: 0x0400113E RID: 4414
		Date,
		// Token: 0x0400113F RID: 4415
		KeepAlive,
		// Token: 0x04001140 RID: 4416
		Pragma,
		// Token: 0x04001141 RID: 4417
		Trailer,
		// Token: 0x04001142 RID: 4418
		TransferEncoding,
		// Token: 0x04001143 RID: 4419
		Upgrade,
		// Token: 0x04001144 RID: 4420
		Via,
		// Token: 0x04001145 RID: 4421
		Warning,
		// Token: 0x04001146 RID: 4422
		Allow,
		// Token: 0x04001147 RID: 4423
		ContentLength,
		// Token: 0x04001148 RID: 4424
		ContentType,
		// Token: 0x04001149 RID: 4425
		ContentEncoding,
		// Token: 0x0400114A RID: 4426
		ContentLanguage,
		// Token: 0x0400114B RID: 4427
		ContentLocation,
		// Token: 0x0400114C RID: 4428
		ContentMd5,
		// Token: 0x0400114D RID: 4429
		ContentRange,
		// Token: 0x0400114E RID: 4430
		Expires,
		// Token: 0x0400114F RID: 4431
		LastModified,
		// Token: 0x04001150 RID: 4432
		Accept,
		// Token: 0x04001151 RID: 4433
		AcceptCharset,
		// Token: 0x04001152 RID: 4434
		AcceptEncoding,
		// Token: 0x04001153 RID: 4435
		AcceptLanguage,
		// Token: 0x04001154 RID: 4436
		Authorization,
		// Token: 0x04001155 RID: 4437
		Cookie,
		// Token: 0x04001156 RID: 4438
		Expect,
		// Token: 0x04001157 RID: 4439
		From,
		// Token: 0x04001158 RID: 4440
		Host,
		// Token: 0x04001159 RID: 4441
		IfMatch,
		// Token: 0x0400115A RID: 4442
		IfModifiedSince,
		// Token: 0x0400115B RID: 4443
		IfNoneMatch,
		// Token: 0x0400115C RID: 4444
		IfRange,
		// Token: 0x0400115D RID: 4445
		IfUnmodifiedSince,
		// Token: 0x0400115E RID: 4446
		MaxForwards,
		// Token: 0x0400115F RID: 4447
		ProxyAuthorization,
		// Token: 0x04001160 RID: 4448
		Referer,
		// Token: 0x04001161 RID: 4449
		Range,
		// Token: 0x04001162 RID: 4450
		Te,
		// Token: 0x04001163 RID: 4451
		Translate,
		// Token: 0x04001164 RID: 4452
		UserAgent
	}
}
