﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Net
{
	// Token: 0x02000326 RID: 806
	public interface ICertificatePolicy
	{
		// Token: 0x06001C9F RID: 7327
		bool CheckValidationResult(ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem);
	}
}
