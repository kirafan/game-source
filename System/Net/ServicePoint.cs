﻿using System;
using System.Collections;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace System.Net
{
	// Token: 0x020003E4 RID: 996
	public class ServicePoint
	{
		// Token: 0x06002243 RID: 8771 RVA: 0x00063CE4 File Offset: 0x00061EE4
		internal ServicePoint(System.Uri uri, int connectionLimit, int maxIdleTime)
		{
			this.uri = uri;
			this.connectionLimit = connectionLimit;
			this.maxIdleTime = maxIdleTime;
			this.currentConnections = 0;
			this.idleSince = DateTime.Now;
		}

		// Token: 0x170009E4 RID: 2532
		// (get) Token: 0x06002244 RID: 8772 RVA: 0x00063D3C File Offset: 0x00061F3C
		public System.Uri Address
		{
			get
			{
				return this.uri;
			}
		}

		// Token: 0x06002245 RID: 8773 RVA: 0x00063D44 File Offset: 0x00061F44
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		// Token: 0x170009E5 RID: 2533
		// (get) Token: 0x06002246 RID: 8774 RVA: 0x00063D4C File Offset: 0x00061F4C
		// (set) Token: 0x06002247 RID: 8775 RVA: 0x00063D54 File Offset: 0x00061F54
		public BindIPEndPoint BindIPEndPointDelegate
		{
			get
			{
				return this.endPointCallback;
			}
			set
			{
				this.endPointCallback = value;
			}
		}

		// Token: 0x170009E6 RID: 2534
		// (get) Token: 0x06002248 RID: 8776 RVA: 0x00063D60 File Offset: 0x00061F60
		public X509Certificate Certificate
		{
			get
			{
				return this.certificate;
			}
		}

		// Token: 0x170009E7 RID: 2535
		// (get) Token: 0x06002249 RID: 8777 RVA: 0x00063D68 File Offset: 0x00061F68
		public X509Certificate ClientCertificate
		{
			get
			{
				return this.clientCertificate;
			}
		}

		// Token: 0x170009E8 RID: 2536
		// (get) Token: 0x0600224A RID: 8778 RVA: 0x00063D70 File Offset: 0x00061F70
		// (set) Token: 0x0600224B RID: 8779 RVA: 0x00063D78 File Offset: 0x00061F78
		[MonoTODO]
		public int ConnectionLeaseTimeout
		{
			get
			{
				throw ServicePoint.GetMustImplement();
			}
			set
			{
				throw ServicePoint.GetMustImplement();
			}
		}

		// Token: 0x170009E9 RID: 2537
		// (get) Token: 0x0600224C RID: 8780 RVA: 0x00063D80 File Offset: 0x00061F80
		// (set) Token: 0x0600224D RID: 8781 RVA: 0x00063D88 File Offset: 0x00061F88
		public int ConnectionLimit
		{
			get
			{
				return this.connectionLimit;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.connectionLimit = value;
			}
		}

		// Token: 0x170009EA RID: 2538
		// (get) Token: 0x0600224E RID: 8782 RVA: 0x00063DA0 File Offset: 0x00061FA0
		public string ConnectionName
		{
			get
			{
				return this.uri.Scheme;
			}
		}

		// Token: 0x170009EB RID: 2539
		// (get) Token: 0x0600224F RID: 8783 RVA: 0x00063DB0 File Offset: 0x00061FB0
		public int CurrentConnections
		{
			get
			{
				return this.currentConnections;
			}
		}

		// Token: 0x170009EC RID: 2540
		// (get) Token: 0x06002250 RID: 8784 RVA: 0x00063DB8 File Offset: 0x00061FB8
		// (set) Token: 0x06002251 RID: 8785 RVA: 0x00063DC0 File Offset: 0x00061FC0
		public DateTime IdleSince
		{
			get
			{
				return this.idleSince;
			}
			internal set
			{
				object obj = this.locker;
				lock (obj)
				{
					this.idleSince = value;
				}
			}
		}

		// Token: 0x170009ED RID: 2541
		// (get) Token: 0x06002252 RID: 8786 RVA: 0x00063E0C File Offset: 0x0006200C
		// (set) Token: 0x06002253 RID: 8787 RVA: 0x00063E14 File Offset: 0x00062014
		public int MaxIdleTime
		{
			get
			{
				return this.maxIdleTime;
			}
			set
			{
				if (value < -1 || value > 2147483647)
				{
					throw new ArgumentOutOfRangeException();
				}
				this.maxIdleTime = value;
			}
		}

		// Token: 0x170009EE RID: 2542
		// (get) Token: 0x06002254 RID: 8788 RVA: 0x00063E38 File Offset: 0x00062038
		public virtual Version ProtocolVersion
		{
			get
			{
				return this.protocolVersion;
			}
		}

		// Token: 0x170009EF RID: 2543
		// (get) Token: 0x06002255 RID: 8789 RVA: 0x00063E40 File Offset: 0x00062040
		// (set) Token: 0x06002256 RID: 8790 RVA: 0x00063E48 File Offset: 0x00062048
		[MonoTODO]
		public int ReceiveBufferSize
		{
			get
			{
				throw ServicePoint.GetMustImplement();
			}
			set
			{
				throw ServicePoint.GetMustImplement();
			}
		}

		// Token: 0x170009F0 RID: 2544
		// (get) Token: 0x06002257 RID: 8791 RVA: 0x00063E50 File Offset: 0x00062050
		public bool SupportsPipelining
		{
			get
			{
				return HttpVersion.Version11.Equals(this.protocolVersion);
			}
		}

		// Token: 0x170009F1 RID: 2545
		// (get) Token: 0x06002258 RID: 8792 RVA: 0x00063E64 File Offset: 0x00062064
		// (set) Token: 0x06002259 RID: 8793 RVA: 0x00063E6C File Offset: 0x0006206C
		public bool Expect100Continue
		{
			get
			{
				return this.SendContinue;
			}
			set
			{
				this.SendContinue = value;
			}
		}

		// Token: 0x170009F2 RID: 2546
		// (get) Token: 0x0600225A RID: 8794 RVA: 0x00063E78 File Offset: 0x00062078
		// (set) Token: 0x0600225B RID: 8795 RVA: 0x00063E80 File Offset: 0x00062080
		public bool UseNagleAlgorithm
		{
			get
			{
				return this.useNagle;
			}
			set
			{
				this.useNagle = value;
			}
		}

		// Token: 0x170009F3 RID: 2547
		// (get) Token: 0x0600225C RID: 8796 RVA: 0x00063E8C File Offset: 0x0006208C
		// (set) Token: 0x0600225D RID: 8797 RVA: 0x00063ECC File Offset: 0x000620CC
		internal bool SendContinue
		{
			get
			{
				return this.sendContinue && (this.protocolVersion == null || this.protocolVersion == HttpVersion.Version11);
			}
			set
			{
				this.sendContinue = value;
			}
		}

		// Token: 0x170009F4 RID: 2548
		// (get) Token: 0x0600225E RID: 8798 RVA: 0x00063ED8 File Offset: 0x000620D8
		// (set) Token: 0x0600225F RID: 8799 RVA: 0x00063EE0 File Offset: 0x000620E0
		internal bool UsesProxy
		{
			get
			{
				return this.usesProxy;
			}
			set
			{
				this.usesProxy = value;
			}
		}

		// Token: 0x170009F5 RID: 2549
		// (get) Token: 0x06002260 RID: 8800 RVA: 0x00063EEC File Offset: 0x000620EC
		// (set) Token: 0x06002261 RID: 8801 RVA: 0x00063EF4 File Offset: 0x000620F4
		internal bool UseConnect
		{
			get
			{
				return this.useConnect;
			}
			set
			{
				this.useConnect = value;
			}
		}

		// Token: 0x170009F6 RID: 2550
		// (get) Token: 0x06002262 RID: 8802 RVA: 0x00063F00 File Offset: 0x00062100
		internal bool AvailableForRecycling
		{
			get
			{
				return this.CurrentConnections == 0 && this.maxIdleTime != -1 && DateTime.Now >= this.IdleSince.AddMilliseconds((double)this.maxIdleTime);
			}
		}

		// Token: 0x170009F7 RID: 2551
		// (get) Token: 0x06002263 RID: 8803 RVA: 0x00063F48 File Offset: 0x00062148
		internal Hashtable Groups
		{
			get
			{
				if (this.groups == null)
				{
					this.groups = new Hashtable();
				}
				return this.groups;
			}
		}

		// Token: 0x170009F8 RID: 2552
		// (get) Token: 0x06002264 RID: 8804 RVA: 0x00063F68 File Offset: 0x00062168
		internal IPHostEntry HostEntry
		{
			get
			{
				object obj = this.hostE;
				lock (obj)
				{
					if (this.host != null)
					{
						return this.host;
					}
					string text = this.uri.Host;
					if (this.uri.HostNameType == System.UriHostNameType.IPv6 || this.uri.HostNameType == System.UriHostNameType.IPv4)
					{
						if (this.uri.HostNameType == System.UriHostNameType.IPv6)
						{
							text = text.Substring(1, text.Length - 2);
						}
						this.host = new IPHostEntry();
						this.host.AddressList = new IPAddress[]
						{
							IPAddress.Parse(text)
						};
						return this.host;
					}
					try
					{
						this.host = Dns.GetHostByName(text);
					}
					catch
					{
						return null;
					}
				}
				return this.host;
			}
		}

		// Token: 0x06002265 RID: 8805 RVA: 0x00064080 File Offset: 0x00062280
		internal void SetVersion(Version version)
		{
			this.protocolVersion = version;
		}

		// Token: 0x06002266 RID: 8806 RVA: 0x0006408C File Offset: 0x0006228C
		private WebConnectionGroup GetConnectionGroup(string name)
		{
			if (name == null)
			{
				name = string.Empty;
			}
			WebConnectionGroup webConnectionGroup = this.Groups[name] as WebConnectionGroup;
			if (webConnectionGroup != null)
			{
				return webConnectionGroup;
			}
			webConnectionGroup = new WebConnectionGroup(this, name);
			this.Groups[name] = webConnectionGroup;
			return webConnectionGroup;
		}

		// Token: 0x06002267 RID: 8807 RVA: 0x000640D8 File Offset: 0x000622D8
		internal EventHandler SendRequest(HttpWebRequest request, string groupName)
		{
			object obj = this.locker;
			WebConnection connection;
			lock (obj)
			{
				WebConnectionGroup connectionGroup = this.GetConnectionGroup(groupName);
				connection = connectionGroup.GetConnection(request);
			}
			return connection.SendRequest(request);
		}

		// Token: 0x06002268 RID: 8808 RVA: 0x00064134 File Offset: 0x00062334
		public bool CloseConnectionGroup(string connectionGroupName)
		{
			object obj = this.locker;
			lock (obj)
			{
				WebConnectionGroup connectionGroup = this.GetConnectionGroup(connectionGroupName);
				if (connectionGroup != null)
				{
					connectionGroup.Close();
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002269 RID: 8809 RVA: 0x00064194 File Offset: 0x00062394
		internal void IncrementConnection()
		{
			object obj = this.locker;
			lock (obj)
			{
				this.currentConnections++;
				this.idleSince = DateTime.Now.AddMilliseconds(1000000.0);
			}
		}

		// Token: 0x0600226A RID: 8810 RVA: 0x00064200 File Offset: 0x00062400
		internal void DecrementConnection()
		{
			object obj = this.locker;
			lock (obj)
			{
				this.currentConnections--;
				if (this.currentConnections == 0)
				{
					this.idleSince = DateTime.Now;
				}
			}
		}

		// Token: 0x0600226B RID: 8811 RVA: 0x00064268 File Offset: 0x00062468
		internal void SetCertificates(X509Certificate client, X509Certificate server)
		{
			this.certificate = server;
			this.clientCertificate = client;
		}

		// Token: 0x0600226C RID: 8812 RVA: 0x00064278 File Offset: 0x00062478
		internal bool CallEndPointDelegate(System.Net.Sockets.Socket sock, IPEndPoint remote)
		{
			if (this.endPointCallback == null)
			{
				return true;
			}
			int num = 0;
			checked
			{
				for (;;)
				{
					IPEndPoint ipendPoint = null;
					try
					{
						ipendPoint = this.endPointCallback(this, remote, num);
					}
					catch
					{
						return false;
					}
					if (ipendPoint == null)
					{
						break;
					}
					try
					{
						sock.Bind(ipendPoint);
					}
					catch (System.Net.Sockets.SocketException)
					{
						num++;
						continue;
					}
					return true;
				}
				return true;
			}
		}

		// Token: 0x0400150B RID: 5387
		private System.Uri uri;

		// Token: 0x0400150C RID: 5388
		private int connectionLimit;

		// Token: 0x0400150D RID: 5389
		private int maxIdleTime;

		// Token: 0x0400150E RID: 5390
		private int currentConnections;

		// Token: 0x0400150F RID: 5391
		private DateTime idleSince;

		// Token: 0x04001510 RID: 5392
		private Version protocolVersion;

		// Token: 0x04001511 RID: 5393
		private X509Certificate certificate;

		// Token: 0x04001512 RID: 5394
		private X509Certificate clientCertificate;

		// Token: 0x04001513 RID: 5395
		private IPHostEntry host;

		// Token: 0x04001514 RID: 5396
		private bool usesProxy;

		// Token: 0x04001515 RID: 5397
		private Hashtable groups;

		// Token: 0x04001516 RID: 5398
		private bool sendContinue = true;

		// Token: 0x04001517 RID: 5399
		private bool useConnect;

		// Token: 0x04001518 RID: 5400
		private object locker = new object();

		// Token: 0x04001519 RID: 5401
		private object hostE = new object();

		// Token: 0x0400151A RID: 5402
		private bool useNagle;

		// Token: 0x0400151B RID: 5403
		private BindIPEndPoint endPointCallback;
	}
}
