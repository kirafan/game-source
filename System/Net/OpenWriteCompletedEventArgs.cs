﻿using System;
using System.ComponentModel;
using System.IO;

namespace System.Net
{
	// Token: 0x020004C7 RID: 1223
	public class OpenWriteCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BD0 RID: 11216 RVA: 0x00098D78 File Offset: 0x00096F78
		internal OpenWriteCompletedEventArgs(Stream result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BFE RID: 3070
		// (get) Token: 0x06002BD1 RID: 11217 RVA: 0x00098D8C File Offset: 0x00096F8C
		public Stream Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B9B RID: 7067
		private Stream result;
	}
}
