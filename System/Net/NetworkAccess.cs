﻿using System;

namespace System.Net
{
	// Token: 0x02000358 RID: 856
	[Flags]
	public enum NetworkAccess
	{
		// Token: 0x040012D2 RID: 4818
		Accept = 128,
		// Token: 0x040012D3 RID: 4819
		Connect = 64
	}
}
