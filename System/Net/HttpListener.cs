﻿using System;
using System.Collections;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000316 RID: 790
	public sealed class HttpListener : IDisposable
	{
		// Token: 0x06001B78 RID: 7032 RVA: 0x0004E6B8 File Offset: 0x0004C8B8
		public HttpListener()
		{
			this.prefixes = new HttpListenerPrefixCollection(this);
			this.registry = new Hashtable();
			this.ctx_queue = new ArrayList();
			this.wait_queue = new ArrayList();
			this.auth_schemes = AuthenticationSchemes.Anonymous;
		}

		// Token: 0x06001B79 RID: 7033 RVA: 0x0004E704 File Offset: 0x0004C904
		void IDisposable.Dispose()
		{
			if (this.disposed)
			{
				return;
			}
			this.Close(true);
			this.disposed = true;
		}

		// Token: 0x170006AB RID: 1707
		// (get) Token: 0x06001B7A RID: 7034 RVA: 0x0004E720 File Offset: 0x0004C920
		// (set) Token: 0x06001B7B RID: 7035 RVA: 0x0004E728 File Offset: 0x0004C928
		public AuthenticationSchemes AuthenticationSchemes
		{
			get
			{
				return this.auth_schemes;
			}
			set
			{
				this.CheckDisposed();
				this.auth_schemes = value;
			}
		}

		// Token: 0x170006AC RID: 1708
		// (get) Token: 0x06001B7C RID: 7036 RVA: 0x0004E738 File Offset: 0x0004C938
		// (set) Token: 0x06001B7D RID: 7037 RVA: 0x0004E740 File Offset: 0x0004C940
		public AuthenticationSchemeSelector AuthenticationSchemeSelectorDelegate
		{
			get
			{
				return this.auth_selector;
			}
			set
			{
				this.CheckDisposed();
				this.auth_selector = value;
			}
		}

		// Token: 0x170006AD RID: 1709
		// (get) Token: 0x06001B7E RID: 7038 RVA: 0x0004E750 File Offset: 0x0004C950
		// (set) Token: 0x06001B7F RID: 7039 RVA: 0x0004E758 File Offset: 0x0004C958
		public bool IgnoreWriteExceptions
		{
			get
			{
				return this.ignore_write_exceptions;
			}
			set
			{
				this.CheckDisposed();
				this.ignore_write_exceptions = value;
			}
		}

		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x06001B80 RID: 7040 RVA: 0x0004E768 File Offset: 0x0004C968
		public bool IsListening
		{
			get
			{
				return this.listening;
			}
		}

		// Token: 0x170006AF RID: 1711
		// (get) Token: 0x06001B81 RID: 7041 RVA: 0x0004E770 File Offset: 0x0004C970
		public static bool IsSupported
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170006B0 RID: 1712
		// (get) Token: 0x06001B82 RID: 7042 RVA: 0x0004E774 File Offset: 0x0004C974
		public HttpListenerPrefixCollection Prefixes
		{
			get
			{
				this.CheckDisposed();
				return this.prefixes;
			}
		}

		// Token: 0x170006B1 RID: 1713
		// (get) Token: 0x06001B83 RID: 7043 RVA: 0x0004E784 File Offset: 0x0004C984
		// (set) Token: 0x06001B84 RID: 7044 RVA: 0x0004E78C File Offset: 0x0004C98C
		public string Realm
		{
			get
			{
				return this.realm;
			}
			set
			{
				this.CheckDisposed();
				this.realm = value;
			}
		}

		// Token: 0x170006B2 RID: 1714
		// (get) Token: 0x06001B85 RID: 7045 RVA: 0x0004E79C File Offset: 0x0004C99C
		// (set) Token: 0x06001B86 RID: 7046 RVA: 0x0004E7A4 File Offset: 0x0004C9A4
		[MonoTODO("Support for NTLM needs some loving.")]
		public bool UnsafeConnectionNtlmAuthentication
		{
			get
			{
				return this.unsafe_ntlm_auth;
			}
			set
			{
				this.CheckDisposed();
				this.unsafe_ntlm_auth = value;
			}
		}

		// Token: 0x06001B87 RID: 7047 RVA: 0x0004E7B4 File Offset: 0x0004C9B4
		public void Abort()
		{
			if (this.disposed)
			{
				return;
			}
			if (!this.listening)
			{
				return;
			}
			this.Close(true);
		}

		// Token: 0x06001B88 RID: 7048 RVA: 0x0004E7D8 File Offset: 0x0004C9D8
		public void Close()
		{
			if (this.disposed)
			{
				return;
			}
			if (!this.listening)
			{
				this.disposed = true;
				return;
			}
			this.Close(false);
			this.disposed = true;
		}

		// Token: 0x06001B89 RID: 7049 RVA: 0x0004E808 File Offset: 0x0004CA08
		private void Close(bool force)
		{
			this.CheckDisposed();
			EndPointManager.RemoveListener(this);
			this.Cleanup(force);
		}

		// Token: 0x06001B8A RID: 7050 RVA: 0x0004E820 File Offset: 0x0004CA20
		private void Cleanup(bool close_existing)
		{
			Hashtable obj = this.registry;
			lock (obj)
			{
				if (close_existing)
				{
					foreach (object obj2 in this.registry.Keys)
					{
						HttpListenerContext httpListenerContext = (HttpListenerContext)obj2;
						httpListenerContext.Connection.Close();
					}
					this.registry.Clear();
				}
				ArrayList obj3 = this.ctx_queue;
				lock (obj3)
				{
					foreach (object obj4 in this.ctx_queue)
					{
						HttpListenerContext httpListenerContext2 = (HttpListenerContext)obj4;
						httpListenerContext2.Connection.Close();
					}
					this.ctx_queue.Clear();
				}
				ArrayList obj5 = this.wait_queue;
				lock (obj5)
				{
					foreach (object obj6 in this.wait_queue)
					{
						ListenerAsyncResult listenerAsyncResult = (ListenerAsyncResult)obj6;
						listenerAsyncResult.Complete("Listener was closed.");
					}
					this.wait_queue.Clear();
				}
			}
		}

		// Token: 0x06001B8B RID: 7051 RVA: 0x0004EA30 File Offset: 0x0004CC30
		public IAsyncResult BeginGetContext(AsyncCallback callback, object state)
		{
			this.CheckDisposed();
			if (!this.listening)
			{
				throw new InvalidOperationException("Please, call Start before using this method.");
			}
			ListenerAsyncResult listenerAsyncResult = new ListenerAsyncResult(callback, state);
			ArrayList obj = this.wait_queue;
			lock (obj)
			{
				ArrayList obj2 = this.ctx_queue;
				lock (obj2)
				{
					HttpListenerContext contextFromQueue = this.GetContextFromQueue();
					if (contextFromQueue != null)
					{
						listenerAsyncResult.Complete(contextFromQueue, true);
						return listenerAsyncResult;
					}
				}
				this.wait_queue.Add(listenerAsyncResult);
			}
			return listenerAsyncResult;
		}

		// Token: 0x06001B8C RID: 7052 RVA: 0x0004EAF8 File Offset: 0x0004CCF8
		public HttpListenerContext EndGetContext(IAsyncResult asyncResult)
		{
			this.CheckDisposed();
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			ListenerAsyncResult listenerAsyncResult = asyncResult as ListenerAsyncResult;
			if (listenerAsyncResult == null)
			{
				throw new ArgumentException("Wrong IAsyncResult.", "asyncResult");
			}
			if (!listenerAsyncResult.IsCompleted)
			{
				listenerAsyncResult.AsyncWaitHandle.WaitOne();
			}
			ArrayList obj = this.wait_queue;
			lock (obj)
			{
				int num = this.wait_queue.IndexOf(listenerAsyncResult);
				if (num >= 0)
				{
					this.wait_queue.RemoveAt(num);
				}
			}
			HttpListenerContext context = listenerAsyncResult.GetContext();
			if (this.auth_schemes != AuthenticationSchemes.Anonymous)
			{
				context.ParseAuthentication(this.auth_schemes);
			}
			return context;
		}

		// Token: 0x06001B8D RID: 7053 RVA: 0x0004EBCC File Offset: 0x0004CDCC
		internal AuthenticationSchemes SelectAuthenticationScheme(HttpListenerContext context)
		{
			if (this.AuthenticationSchemeSelectorDelegate != null)
			{
				return this.AuthenticationSchemeSelectorDelegate(context.Request);
			}
			return this.auth_schemes;
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x0004EBFC File Offset: 0x0004CDFC
		public HttpListenerContext GetContext()
		{
			if (this.prefixes.Count == 0)
			{
				throw new InvalidOperationException("Please, call AddPrefix before using this method.");
			}
			IAsyncResult asyncResult = this.BeginGetContext(null, null);
			return this.EndGetContext(asyncResult);
		}

		// Token: 0x06001B8F RID: 7055 RVA: 0x0004EC34 File Offset: 0x0004CE34
		public void Start()
		{
			this.CheckDisposed();
			if (this.listening)
			{
				return;
			}
			EndPointManager.AddListener(this);
			this.listening = true;
		}

		// Token: 0x06001B90 RID: 7056 RVA: 0x0004EC58 File Offset: 0x0004CE58
		public void Stop()
		{
			this.CheckDisposed();
			this.listening = false;
			this.Close(false);
		}

		// Token: 0x06001B91 RID: 7057 RVA: 0x0004EC70 File Offset: 0x0004CE70
		internal void CheckDisposed()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException(base.GetType().ToString());
			}
		}

		// Token: 0x06001B92 RID: 7058 RVA: 0x0004EC90 File Offset: 0x0004CE90
		private HttpListenerContext GetContextFromQueue()
		{
			if (this.ctx_queue.Count == 0)
			{
				return null;
			}
			HttpListenerContext result = (HttpListenerContext)this.ctx_queue[0];
			this.ctx_queue.RemoveAt(0);
			return result;
		}

		// Token: 0x06001B93 RID: 7059 RVA: 0x0004ECD0 File Offset: 0x0004CED0
		internal void RegisterContext(HttpListenerContext context)
		{
			try
			{
				Monitor.Enter(this.registry);
				this.registry[context] = context;
				Monitor.Enter(this.wait_queue);
				Monitor.Enter(this.ctx_queue);
				if (this.wait_queue.Count == 0)
				{
					this.ctx_queue.Add(context);
				}
				else
				{
					ListenerAsyncResult listenerAsyncResult = (ListenerAsyncResult)this.wait_queue[0];
					this.wait_queue.RemoveAt(0);
					listenerAsyncResult.Complete(context);
				}
			}
			finally
			{
				Monitor.Exit(this.ctx_queue);
				Monitor.Exit(this.wait_queue);
				Monitor.Exit(this.registry);
			}
		}

		// Token: 0x06001B94 RID: 7060 RVA: 0x0004ED98 File Offset: 0x0004CF98
		internal void UnregisterContext(HttpListenerContext context)
		{
			try
			{
				Monitor.Enter(this.registry);
				Monitor.Enter(this.ctx_queue);
				int num = this.ctx_queue.IndexOf(context);
				if (num >= 0)
				{
					this.ctx_queue.RemoveAt(num);
				}
				this.registry.Remove(context);
			}
			finally
			{
				Monitor.Exit(this.ctx_queue);
				Monitor.Exit(this.registry);
			}
		}

		// Token: 0x04001108 RID: 4360
		private AuthenticationSchemes auth_schemes;

		// Token: 0x04001109 RID: 4361
		private HttpListenerPrefixCollection prefixes;

		// Token: 0x0400110A RID: 4362
		private AuthenticationSchemeSelector auth_selector;

		// Token: 0x0400110B RID: 4363
		private string realm;

		// Token: 0x0400110C RID: 4364
		private bool ignore_write_exceptions;

		// Token: 0x0400110D RID: 4365
		private bool unsafe_ntlm_auth;

		// Token: 0x0400110E RID: 4366
		private bool listening;

		// Token: 0x0400110F RID: 4367
		private bool disposed;

		// Token: 0x04001110 RID: 4368
		private Hashtable registry;

		// Token: 0x04001111 RID: 4369
		private ArrayList ctx_queue;

		// Token: 0x04001112 RID: 4370
		private ArrayList wait_queue;
	}
}
