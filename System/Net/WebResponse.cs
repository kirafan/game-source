﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x02000424 RID: 1060
	[Serializable]
	public abstract class WebResponse : MarshalByRefObject, IDisposable, ISerializable
	{
		// Token: 0x0600265C RID: 9820 RVA: 0x00077488 File Offset: 0x00075688
		protected WebResponse()
		{
		}

		// Token: 0x0600265D RID: 9821 RVA: 0x00077490 File Offset: 0x00075690
		protected WebResponse(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600265E RID: 9822 RVA: 0x000774A0 File Offset: 0x000756A0
		void IDisposable.Dispose()
		{
			this.Close();
		}

		// Token: 0x0600265F RID: 9823 RVA: 0x000774A8 File Offset: 0x000756A8
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000AD1 RID: 2769
		// (get) Token: 0x06002660 RID: 9824 RVA: 0x000774B0 File Offset: 0x000756B0
		// (set) Token: 0x06002661 RID: 9825 RVA: 0x000774B8 File Offset: 0x000756B8
		public virtual long ContentLength
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000AD2 RID: 2770
		// (get) Token: 0x06002662 RID: 9826 RVA: 0x000774C0 File Offset: 0x000756C0
		// (set) Token: 0x06002663 RID: 9827 RVA: 0x000774C8 File Offset: 0x000756C8
		public virtual string ContentType
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000AD3 RID: 2771
		// (get) Token: 0x06002664 RID: 9828 RVA: 0x000774D0 File Offset: 0x000756D0
		public virtual WebHeaderCollection Headers
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06002665 RID: 9829 RVA: 0x000774D8 File Offset: 0x000756D8
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		// Token: 0x17000AD4 RID: 2772
		// (get) Token: 0x06002666 RID: 9830 RVA: 0x000774E0 File Offset: 0x000756E0
		[MonoTODO]
		public virtual bool IsFromCache
		{
			get
			{
				throw WebResponse.GetMustImplement();
			}
		}

		// Token: 0x17000AD5 RID: 2773
		// (get) Token: 0x06002667 RID: 9831 RVA: 0x000774E8 File Offset: 0x000756E8
		[MonoTODO]
		public virtual bool IsMutuallyAuthenticated
		{
			get
			{
				throw WebResponse.GetMustImplement();
			}
		}

		// Token: 0x17000AD6 RID: 2774
		// (get) Token: 0x06002668 RID: 9832 RVA: 0x000774F0 File Offset: 0x000756F0
		public virtual System.Uri ResponseUri
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06002669 RID: 9833 RVA: 0x000774F8 File Offset: 0x000756F8
		public virtual void Close()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600266A RID: 9834 RVA: 0x00077500 File Offset: 0x00075700
		public virtual Stream GetResponseStream()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600266B RID: 9835 RVA: 0x00077508 File Offset: 0x00075708
		[MonoTODO]
		protected virtual void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			throw WebResponse.GetMustImplement();
		}
	}
}
