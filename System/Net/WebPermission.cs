﻿using System;
using System.Collections;
using System.Security;
using System.Security.Permissions;
using System.Text.RegularExpressions;

namespace System.Net
{
	// Token: 0x0200041D RID: 1053
	[MonoTODO("Most private members that include functionallity are not implemented!")]
	[Serializable]
	public sealed class WebPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x060025F2 RID: 9714 RVA: 0x00076224 File Offset: 0x00074424
		public WebPermission()
		{
		}

		// Token: 0x060025F3 RID: 9715 RVA: 0x00076244 File Offset: 0x00074444
		public WebPermission(PermissionState state)
		{
			this.m_noRestriction = (state == PermissionState.Unrestricted);
		}

		// Token: 0x060025F4 RID: 9716 RVA: 0x00076278 File Offset: 0x00074478
		public WebPermission(NetworkAccess access, string uriString)
		{
			this.AddPermission(access, uriString);
		}

		// Token: 0x060025F5 RID: 9717 RVA: 0x000762AC File Offset: 0x000744AC
		public WebPermission(NetworkAccess access, System.Text.RegularExpressions.Regex uriRegex)
		{
			this.AddPermission(access, uriRegex);
		}

		// Token: 0x17000AB9 RID: 2745
		// (get) Token: 0x060025F6 RID: 9718 RVA: 0x000762E0 File Offset: 0x000744E0
		public IEnumerator AcceptList
		{
			get
			{
				return this.m_acceptList.GetEnumerator();
			}
		}

		// Token: 0x17000ABA RID: 2746
		// (get) Token: 0x060025F7 RID: 9719 RVA: 0x000762F0 File Offset: 0x000744F0
		public IEnumerator ConnectList
		{
			get
			{
				return this.m_connectList.GetEnumerator();
			}
		}

		// Token: 0x060025F8 RID: 9720 RVA: 0x00076300 File Offset: 0x00074500
		public void AddPermission(NetworkAccess access, string uriString)
		{
			WebPermissionInfo info = new WebPermissionInfo(WebPermissionInfoType.InfoString, uriString);
			this.AddPermission(access, info);
		}

		// Token: 0x060025F9 RID: 9721 RVA: 0x00076320 File Offset: 0x00074520
		public void AddPermission(NetworkAccess access, System.Text.RegularExpressions.Regex uriRegex)
		{
			WebPermissionInfo info = new WebPermissionInfo(uriRegex);
			this.AddPermission(access, info);
		}

		// Token: 0x060025FA RID: 9722 RVA: 0x0007633C File Offset: 0x0007453C
		internal void AddPermission(NetworkAccess access, WebPermissionInfo info)
		{
			if (access != NetworkAccess.Connect)
			{
				if (access != NetworkAccess.Accept)
				{
					string text = Locale.GetText("Unknown NetworkAccess value {0}.");
					throw new ArgumentException(string.Format(text, access), "access");
				}
				this.m_acceptList.Add(info);
			}
			else
			{
				this.m_connectList.Add(info);
			}
		}

		// Token: 0x060025FB RID: 9723 RVA: 0x000763AC File Offset: 0x000745AC
		public override IPermission Copy()
		{
			return new WebPermission((!this.m_noRestriction) ? PermissionState.None : PermissionState.Unrestricted)
			{
				m_connectList = (ArrayList)this.m_connectList.Clone(),
				m_acceptList = (ArrayList)this.m_acceptList.Clone()
			};
		}

		// Token: 0x060025FC RID: 9724 RVA: 0x00076400 File Offset: 0x00074600
		public override IPermission Intersect(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			WebPermission webPermission = target as WebPermission;
			if (webPermission == null)
			{
				throw new ArgumentException("Argument not of type WebPermission");
			}
			if (this.m_noRestriction)
			{
				IPermission result;
				if (this.IntersectEmpty(webPermission))
				{
					IPermission permission = null;
					result = permission;
				}
				else
				{
					result = webPermission.Copy();
				}
				return result;
			}
			if (webPermission.m_noRestriction)
			{
				IPermission result2;
				if (this.IntersectEmpty(this))
				{
					IPermission permission = null;
					result2 = permission;
				}
				else
				{
					result2 = this.Copy();
				}
				return result2;
			}
			WebPermission webPermission2 = new WebPermission(PermissionState.None);
			this.Intersect(this.m_connectList, webPermission.m_connectList, webPermission2.m_connectList);
			this.Intersect(this.m_acceptList, webPermission.m_acceptList, webPermission2.m_acceptList);
			return (!this.IntersectEmpty(webPermission2)) ? webPermission2 : null;
		}

		// Token: 0x060025FD RID: 9725 RVA: 0x000764C4 File Offset: 0x000746C4
		private bool IntersectEmpty(WebPermission permission)
		{
			return !permission.m_noRestriction && permission.m_connectList.Count == 0 && permission.m_acceptList.Count == 0;
		}

		// Token: 0x060025FE RID: 9726 RVA: 0x00076500 File Offset: 0x00074700
		[MonoTODO]
		private void Intersect(ArrayList list1, ArrayList list2, ArrayList result)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025FF RID: 9727 RVA: 0x00076508 File Offset: 0x00074708
		public override bool IsSubsetOf(IPermission target)
		{
			if (target == null)
			{
				return !this.m_noRestriction && this.m_connectList.Count == 0 && this.m_acceptList.Count == 0;
			}
			WebPermission webPermission = target as WebPermission;
			if (webPermission == null)
			{
				throw new ArgumentException("Parameter target must be of type WebPermission");
			}
			return webPermission.m_noRestriction || (!this.m_noRestriction && ((this.m_acceptList.Count == 0 && this.m_connectList.Count == 0) || ((webPermission.m_acceptList.Count != 0 || webPermission.m_connectList.Count != 0) && this.IsSubsetOf(this.m_connectList, webPermission.m_connectList) && this.IsSubsetOf(this.m_acceptList, webPermission.m_acceptList))));
		}

		// Token: 0x06002600 RID: 9728 RVA: 0x000765EC File Offset: 0x000747EC
		[MonoTODO]
		private bool IsSubsetOf(ArrayList list1, ArrayList list2)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002601 RID: 9729 RVA: 0x000765F4 File Offset: 0x000747F4
		public bool IsUnrestricted()
		{
			return this.m_noRestriction;
		}

		// Token: 0x06002602 RID: 9730 RVA: 0x000765FC File Offset: 0x000747FC
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("IPermission");
			securityElement.AddAttribute("class", base.GetType().AssemblyQualifiedName);
			securityElement.AddAttribute("version", "1");
			if (this.m_noRestriction)
			{
				securityElement.AddAttribute("Unrestricted", "true");
				return securityElement;
			}
			if (this.m_connectList.Count > 0)
			{
				this.ToXml(securityElement, "ConnectAccess", this.m_connectList.GetEnumerator());
			}
			if (this.m_acceptList.Count > 0)
			{
				this.ToXml(securityElement, "AcceptAccess", this.m_acceptList.GetEnumerator());
			}
			return securityElement;
		}

		// Token: 0x06002603 RID: 9731 RVA: 0x000766A8 File Offset: 0x000748A8
		private void ToXml(SecurityElement root, string childName, IEnumerator enumerator)
		{
			SecurityElement securityElement = new SecurityElement(childName, null);
			root.AddChild(securityElement);
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				WebPermissionInfo webPermissionInfo = obj as WebPermissionInfo;
				if (webPermissionInfo != null)
				{
					SecurityElement securityElement2 = new SecurityElement("URI");
					securityElement2.AddAttribute("uri", webPermissionInfo.Info);
					securityElement.AddChild(securityElement2);
				}
			}
		}

		// Token: 0x06002604 RID: 9732 RVA: 0x00076710 File Offset: 0x00074910
		public override void FromXml(SecurityElement securityElement)
		{
			if (securityElement == null)
			{
				throw new ArgumentNullException("securityElement");
			}
			if (securityElement.Tag != "IPermission")
			{
				throw new ArgumentException("securityElement");
			}
			string text = securityElement.Attribute("Unrestricted");
			if (text != null)
			{
				this.m_noRestriction = (string.Compare(text, "true", true) == 0);
				if (this.m_noRestriction)
				{
					return;
				}
			}
			this.m_noRestriction = false;
			this.m_connectList = new ArrayList();
			this.m_acceptList = new ArrayList();
			ArrayList children = securityElement.Children;
			foreach (object obj in children)
			{
				SecurityElement securityElement2 = (SecurityElement)obj;
				if (securityElement2.Tag == "ConnectAccess")
				{
					this.FromXml(securityElement2.Children, NetworkAccess.Connect);
				}
				else if (securityElement2.Tag == "AcceptAccess")
				{
					this.FromXml(securityElement2.Children, NetworkAccess.Accept);
				}
			}
		}

		// Token: 0x06002605 RID: 9733 RVA: 0x0007684C File Offset: 0x00074A4C
		private void FromXml(ArrayList endpoints, NetworkAccess access)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002606 RID: 9734 RVA: 0x00076854 File Offset: 0x00074A54
		public override IPermission Union(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			WebPermission webPermission = target as WebPermission;
			if (webPermission == null)
			{
				throw new ArgumentException("Argument not of type WebPermission");
			}
			if (this.m_noRestriction || webPermission.m_noRestriction)
			{
				return new WebPermission(PermissionState.Unrestricted);
			}
			WebPermission webPermission2 = (WebPermission)webPermission.Copy();
			webPermission2.m_acceptList.InsertRange(webPermission2.m_acceptList.Count, this.m_acceptList);
			webPermission2.m_connectList.InsertRange(webPermission2.m_connectList.Count, this.m_connectList);
			return webPermission2;
		}

		// Token: 0x04001774 RID: 6004
		private ArrayList m_acceptList = new ArrayList();

		// Token: 0x04001775 RID: 6005
		private ArrayList m_connectList = new ArrayList();

		// Token: 0x04001776 RID: 6006
		private bool m_noRestriction;
	}
}
