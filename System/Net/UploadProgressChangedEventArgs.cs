﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C8 RID: 1224
	public class UploadProgressChangedEventArgs : System.ComponentModel.ProgressChangedEventArgs
	{
		// Token: 0x06002BD2 RID: 11218 RVA: 0x00098D94 File Offset: 0x00096F94
		internal UploadProgressChangedEventArgs(long bytesReceived, long totalBytesToReceive, long bytesSent, long totalBytesToSend, int progressPercentage, object userState) : base(progressPercentage, userState)
		{
			this.received = bytesReceived;
			this.total_recv = totalBytesToReceive;
			this.sent = bytesSent;
			this.total_send = totalBytesToSend;
		}

		// Token: 0x17000BFF RID: 3071
		// (get) Token: 0x06002BD3 RID: 11219 RVA: 0x00098DC0 File Offset: 0x00096FC0
		public long BytesReceived
		{
			get
			{
				return this.received;
			}
		}

		// Token: 0x17000C00 RID: 3072
		// (get) Token: 0x06002BD4 RID: 11220 RVA: 0x00098DC8 File Offset: 0x00096FC8
		public long TotalBytesToReceive
		{
			get
			{
				return this.total_recv;
			}
		}

		// Token: 0x17000C01 RID: 3073
		// (get) Token: 0x06002BD5 RID: 11221 RVA: 0x00098DD0 File Offset: 0x00096FD0
		public long BytesSent
		{
			get
			{
				return this.sent;
			}
		}

		// Token: 0x17000C02 RID: 3074
		// (get) Token: 0x06002BD6 RID: 11222 RVA: 0x00098DD8 File Offset: 0x00096FD8
		public long TotalBytesToSend
		{
			get
			{
				return this.total_send;
			}
		}

		// Token: 0x04001B9C RID: 7068
		private long received;

		// Token: 0x04001B9D RID: 7069
		private long sent;

		// Token: 0x04001B9E RID: 7070
		private long total_recv;

		// Token: 0x04001B9F RID: 7071
		private long total_send;
	}
}
