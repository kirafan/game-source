﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net
{
	// Token: 0x020002FD RID: 765
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class DnsPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06001A3F RID: 6719 RVA: 0x000491F0 File Offset: 0x000473F0
		public DnsPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x06001A40 RID: 6720 RVA: 0x000491FC File Offset: 0x000473FC
		public override IPermission CreatePermission()
		{
			return new DnsPermission((!base.Unrestricted) ? PermissionState.None : PermissionState.Unrestricted);
		}
	}
}
