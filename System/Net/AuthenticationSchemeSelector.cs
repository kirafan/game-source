﻿using System;

namespace System.Net
{
	// Token: 0x02000514 RID: 1300
	// (Invoke) Token: 0x06002CFC RID: 11516
	public delegate AuthenticationSchemes AuthenticationSchemeSelector(HttpListenerRequest httpRequest);
}
