﻿using System;

namespace System.Net.Cache
{
	// Token: 0x020002C1 RID: 705
	public class RequestCachePolicy
	{
		// Token: 0x06001853 RID: 6227 RVA: 0x00042D80 File Offset: 0x00040F80
		public RequestCachePolicy()
		{
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x00042D88 File Offset: 0x00040F88
		public RequestCachePolicy(RequestCacheLevel level)
		{
			this.level = level;
		}

		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x06001855 RID: 6229 RVA: 0x00042D98 File Offset: 0x00040F98
		public RequestCacheLevel Level
		{
			get
			{
				return this.level;
			}
		}

		// Token: 0x06001856 RID: 6230 RVA: 0x00042DA0 File Offset: 0x00040FA0
		[MonoTODO]
		public override string ToString()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000F97 RID: 3991
		private RequestCacheLevel level;
	}
}
