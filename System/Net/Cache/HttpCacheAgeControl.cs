﻿using System;

namespace System.Net.Cache
{
	// Token: 0x020002BD RID: 701
	public enum HttpCacheAgeControl
	{
		// Token: 0x04000F7A RID: 3962
		None,
		// Token: 0x04000F7B RID: 3963
		MinFresh,
		// Token: 0x04000F7C RID: 3964
		MaxAge,
		// Token: 0x04000F7D RID: 3965
		MaxAgeAndMinFresh,
		// Token: 0x04000F7E RID: 3966
		MaxStale,
		// Token: 0x04000F7F RID: 3967
		MaxAgeAndMaxStale = 6
	}
}
