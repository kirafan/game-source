﻿using System;

namespace System.Net.Cache
{
	// Token: 0x020002C0 RID: 704
	public enum RequestCacheLevel
	{
		// Token: 0x04000F90 RID: 3984
		Default,
		// Token: 0x04000F91 RID: 3985
		BypassCache,
		// Token: 0x04000F92 RID: 3986
		CacheOnly,
		// Token: 0x04000F93 RID: 3987
		CacheIfAvailable,
		// Token: 0x04000F94 RID: 3988
		Revalidate,
		// Token: 0x04000F95 RID: 3989
		Reload,
		// Token: 0x04000F96 RID: 3990
		NoCacheNoStore
	}
}
