﻿using System;

namespace System.Net.Cache
{
	// Token: 0x020002BF RID: 703
	public class HttpRequestCachePolicy : RequestCachePolicy
	{
		// Token: 0x06001847 RID: 6215 RVA: 0x00042C54 File Offset: 0x00040E54
		public HttpRequestCachePolicy()
		{
		}

		// Token: 0x06001848 RID: 6216 RVA: 0x00042C5C File Offset: 0x00040E5C
		public HttpRequestCachePolicy(DateTime cacheSyncDate)
		{
			this.cacheSyncDate = cacheSyncDate;
		}

		// Token: 0x06001849 RID: 6217 RVA: 0x00042C6C File Offset: 0x00040E6C
		public HttpRequestCachePolicy(HttpRequestCacheLevel level)
		{
			this.level = level;
		}

		// Token: 0x0600184A RID: 6218 RVA: 0x00042C7C File Offset: 0x00040E7C
		public HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan ageOrFreshOrStale)
		{
			switch (cacheAgeControl)
			{
			case HttpCacheAgeControl.MinFresh:
				this.minFresh = ageOrFreshOrStale;
				return;
			case HttpCacheAgeControl.MaxAge:
				this.maxAge = ageOrFreshOrStale;
				return;
			case HttpCacheAgeControl.MaxStale:
				this.maxStale = ageOrFreshOrStale;
				return;
			}
			throw new ArgumentException("ageOrFreshOrStale");
		}

		// Token: 0x0600184B RID: 6219 RVA: 0x00042CE0 File Offset: 0x00040EE0
		public HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan maxAge, TimeSpan freshOrStale)
		{
			this.maxAge = maxAge;
			switch (cacheAgeControl)
			{
			case HttpCacheAgeControl.MinFresh:
				this.minFresh = freshOrStale;
				return;
			case HttpCacheAgeControl.MaxStale:
				this.maxStale = freshOrStale;
				return;
			}
			throw new ArgumentException("freshOrStale");
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x00042D3C File Offset: 0x00040F3C
		public HttpRequestCachePolicy(HttpCacheAgeControl cacheAgeControl, TimeSpan maxAge, TimeSpan freshOrStale, DateTime cacheSyncDate) : this(cacheAgeControl, maxAge, freshOrStale)
		{
			this.cacheSyncDate = cacheSyncDate;
		}

		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x0600184D RID: 6221 RVA: 0x00042D50 File Offset: 0x00040F50
		public DateTime CacheSyncDate
		{
			get
			{
				return this.cacheSyncDate;
			}
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x0600184E RID: 6222 RVA: 0x00042D58 File Offset: 0x00040F58
		public new HttpRequestCacheLevel Level
		{
			get
			{
				return this.level;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x0600184F RID: 6223 RVA: 0x00042D60 File Offset: 0x00040F60
		public TimeSpan MaxAge
		{
			get
			{
				return this.maxAge;
			}
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x06001850 RID: 6224 RVA: 0x00042D68 File Offset: 0x00040F68
		public TimeSpan MaxStale
		{
			get
			{
				return this.maxStale;
			}
		}

		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x06001851 RID: 6225 RVA: 0x00042D70 File Offset: 0x00040F70
		public TimeSpan MinFresh
		{
			get
			{
				return this.minFresh;
			}
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x00042D78 File Offset: 0x00040F78
		[MonoTODO]
		public override string ToString()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000F8A RID: 3978
		private DateTime cacheSyncDate;

		// Token: 0x04000F8B RID: 3979
		private HttpRequestCacheLevel level;

		// Token: 0x04000F8C RID: 3980
		private TimeSpan maxAge;

		// Token: 0x04000F8D RID: 3981
		private TimeSpan maxStale;

		// Token: 0x04000F8E RID: 3982
		private TimeSpan minFresh;
	}
}
