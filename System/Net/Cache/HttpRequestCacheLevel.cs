﻿using System;

namespace System.Net.Cache
{
	// Token: 0x020002BE RID: 702
	public enum HttpRequestCacheLevel
	{
		// Token: 0x04000F81 RID: 3969
		Default,
		// Token: 0x04000F82 RID: 3970
		BypassCache,
		// Token: 0x04000F83 RID: 3971
		CacheOnly,
		// Token: 0x04000F84 RID: 3972
		CacheIfAvailable,
		// Token: 0x04000F85 RID: 3973
		Revalidate,
		// Token: 0x04000F86 RID: 3974
		Reload,
		// Token: 0x04000F87 RID: 3975
		NoCacheNoStore,
		// Token: 0x04000F88 RID: 3976
		CacheOrNextCacheOnly,
		// Token: 0x04000F89 RID: 3977
		Refresh
	}
}
