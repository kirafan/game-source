﻿using System;
using System.Security.Principal;

namespace System.Net
{
	// Token: 0x02000314 RID: 788
	public class HttpListenerBasicIdentity : GenericIdentity
	{
		// Token: 0x06001B6A RID: 7018 RVA: 0x0004E4D8 File Offset: 0x0004C6D8
		public HttpListenerBasicIdentity(string username, string password) : base(username, "Basic")
		{
			this.password = password;
		}

		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x06001B6B RID: 7019 RVA: 0x0004E4F0 File Offset: 0x0004C6F0
		public virtual string Password
		{
			get
			{
				return this.password;
			}
		}

		// Token: 0x04001100 RID: 4352
		private string password;
	}
}
