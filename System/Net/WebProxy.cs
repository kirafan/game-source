﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

namespace System.Net
{
	// Token: 0x0200041E RID: 1054
	[Serializable]
	public class WebProxy : ISerializable, IWebProxy
	{
		// Token: 0x06002607 RID: 9735 RVA: 0x000768E4 File Offset: 0x00074AE4
		public WebProxy() : this(null, false, null, null)
		{
		}

		// Token: 0x06002608 RID: 9736 RVA: 0x000768F0 File Offset: 0x00074AF0
		public WebProxy(string address) : this(WebProxy.ToUri(address), false, null, null)
		{
		}

		// Token: 0x06002609 RID: 9737 RVA: 0x00076904 File Offset: 0x00074B04
		public WebProxy(System.Uri address) : this(address, false, null, null)
		{
		}

		// Token: 0x0600260A RID: 9738 RVA: 0x00076910 File Offset: 0x00074B10
		public WebProxy(string address, bool bypassOnLocal) : this(WebProxy.ToUri(address), bypassOnLocal, null, null)
		{
		}

		// Token: 0x0600260B RID: 9739 RVA: 0x00076924 File Offset: 0x00074B24
		public WebProxy(string host, int port) : this(new System.Uri(string.Concat(new object[]
		{
			"http://",
			host,
			":",
			port
		})))
		{
		}

		// Token: 0x0600260C RID: 9740 RVA: 0x0007695C File Offset: 0x00074B5C
		public WebProxy(System.Uri address, bool bypassOnLocal) : this(address, bypassOnLocal, null, null)
		{
		}

		// Token: 0x0600260D RID: 9741 RVA: 0x00076968 File Offset: 0x00074B68
		public WebProxy(string address, bool bypassOnLocal, string[] bypassList) : this(WebProxy.ToUri(address), bypassOnLocal, bypassList, null)
		{
		}

		// Token: 0x0600260E RID: 9742 RVA: 0x0007697C File Offset: 0x00074B7C
		public WebProxy(System.Uri address, bool bypassOnLocal, string[] bypassList) : this(address, bypassOnLocal, bypassList, null)
		{
		}

		// Token: 0x0600260F RID: 9743 RVA: 0x00076988 File Offset: 0x00074B88
		public WebProxy(string address, bool bypassOnLocal, string[] bypassList, ICredentials credentials) : this(WebProxy.ToUri(address), bypassOnLocal, bypassList, credentials)
		{
		}

		// Token: 0x06002610 RID: 9744 RVA: 0x0007699C File Offset: 0x00074B9C
		public WebProxy(System.Uri address, bool bypassOnLocal, string[] bypassList, ICredentials credentials)
		{
			this.address = address;
			this.bypassOnLocal = bypassOnLocal;
			if (bypassList != null)
			{
				this.bypassList = new ArrayList(bypassList);
			}
			this.credentials = credentials;
			this.CheckBypassList();
		}

		// Token: 0x06002611 RID: 9745 RVA: 0x000769E0 File Offset: 0x00074BE0
		protected WebProxy(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.address = (System.Uri)serializationInfo.GetValue("_ProxyAddress", typeof(System.Uri));
			this.bypassOnLocal = serializationInfo.GetBoolean("_BypassOnLocal");
			this.bypassList = (ArrayList)serializationInfo.GetValue("_BypassList", typeof(ArrayList));
			this.useDefaultCredentials = serializationInfo.GetBoolean("_UseDefaultCredentials");
			this.credentials = null;
			this.CheckBypassList();
		}

		// Token: 0x06002612 RID: 9746 RVA: 0x00076A64 File Offset: 0x00074C64
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.GetObjectData(serializationInfo, streamingContext);
		}

		// Token: 0x17000ABB RID: 2747
		// (get) Token: 0x06002613 RID: 9747 RVA: 0x00076A70 File Offset: 0x00074C70
		// (set) Token: 0x06002614 RID: 9748 RVA: 0x00076A78 File Offset: 0x00074C78
		public System.Uri Address
		{
			get
			{
				return this.address;
			}
			set
			{
				this.address = value;
			}
		}

		// Token: 0x17000ABC RID: 2748
		// (get) Token: 0x06002615 RID: 9749 RVA: 0x00076A84 File Offset: 0x00074C84
		public ArrayList BypassArrayList
		{
			get
			{
				if (this.bypassList == null)
				{
					this.bypassList = new ArrayList();
				}
				return this.bypassList;
			}
		}

		// Token: 0x17000ABD RID: 2749
		// (get) Token: 0x06002616 RID: 9750 RVA: 0x00076AA4 File Offset: 0x00074CA4
		// (set) Token: 0x06002617 RID: 9751 RVA: 0x00076AC0 File Offset: 0x00074CC0
		public string[] BypassList
		{
			get
			{
				return (string[])this.BypassArrayList.ToArray(typeof(string));
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				this.bypassList = new ArrayList(value);
				this.CheckBypassList();
			}
		}

		// Token: 0x17000ABE RID: 2750
		// (get) Token: 0x06002618 RID: 9752 RVA: 0x00076AE0 File Offset: 0x00074CE0
		// (set) Token: 0x06002619 RID: 9753 RVA: 0x00076AE8 File Offset: 0x00074CE8
		public bool BypassProxyOnLocal
		{
			get
			{
				return this.bypassOnLocal;
			}
			set
			{
				this.bypassOnLocal = value;
			}
		}

		// Token: 0x17000ABF RID: 2751
		// (get) Token: 0x0600261A RID: 9754 RVA: 0x00076AF4 File Offset: 0x00074CF4
		// (set) Token: 0x0600261B RID: 9755 RVA: 0x00076AFC File Offset: 0x00074CFC
		public ICredentials Credentials
		{
			get
			{
				return this.credentials;
			}
			set
			{
				this.credentials = value;
			}
		}

		// Token: 0x17000AC0 RID: 2752
		// (get) Token: 0x0600261C RID: 9756 RVA: 0x00076B08 File Offset: 0x00074D08
		// (set) Token: 0x0600261D RID: 9757 RVA: 0x00076B10 File Offset: 0x00074D10
		[MonoTODO("Does not affect Credentials, since CredentialCache.DefaultCredentials is not implemented.")]
		public bool UseDefaultCredentials
		{
			get
			{
				return this.useDefaultCredentials;
			}
			set
			{
				this.useDefaultCredentials = value;
			}
		}

		// Token: 0x0600261E RID: 9758 RVA: 0x00076B1C File Offset: 0x00074D1C
		[Obsolete("This method has been deprecated", false)]
		[MonoTODO("Can we get this info under windows from the system?")]
		public static WebProxy GetDefaultProxy()
		{
			IWebProxy select = GlobalProxySelection.Select;
			if (select is WebProxy)
			{
				return (WebProxy)select;
			}
			return new WebProxy();
		}

		// Token: 0x0600261F RID: 9759 RVA: 0x00076B48 File Offset: 0x00074D48
		public System.Uri GetProxy(System.Uri destination)
		{
			if (this.IsBypassed(destination))
			{
				return destination;
			}
			return this.address;
		}

		// Token: 0x06002620 RID: 9760 RVA: 0x00076B60 File Offset: 0x00074D60
		public bool IsBypassed(System.Uri host)
		{
			if (host == null)
			{
				throw new ArgumentNullException("host");
			}
			if (host.IsLoopback && this.bypassOnLocal)
			{
				return true;
			}
			if (this.address == null)
			{
				return true;
			}
			string host2 = host.Host;
			if (this.bypassOnLocal && host2.IndexOf('.') == -1)
			{
				return true;
			}
			if (!this.bypassOnLocal)
			{
				if (string.Compare(host2, "localhost", true, CultureInfo.InvariantCulture) == 0)
				{
					return true;
				}
				if (string.Compare(host2, "loopback", true, CultureInfo.InvariantCulture) == 0)
				{
					return true;
				}
				IPAddress addr = null;
				if (IPAddress.TryParse(host2, out addr) && IPAddress.IsLoopback(addr))
				{
					return true;
				}
			}
			if (this.bypassList == null || this.bypassList.Count == 0)
			{
				return false;
			}
			bool result;
			try
			{
				string input = host.Scheme + "://" + host.Authority;
				int i;
				for (i = 0; i < this.bypassList.Count; i++)
				{
					System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex((string)this.bypassList[i], System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
					if (regex.IsMatch(input))
					{
						break;
					}
				}
				if (i == this.bypassList.Count)
				{
					result = false;
				}
				else
				{
					while (i < this.bypassList.Count)
					{
						new System.Text.RegularExpressions.Regex((string)this.bypassList[i]);
						i++;
					}
					result = true;
				}
			}
			catch (ArgumentException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06002621 RID: 9761 RVA: 0x00076D28 File Offset: 0x00074F28
		protected virtual void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			serializationInfo.AddValue("_BypassOnLocal", this.bypassOnLocal);
			serializationInfo.AddValue("_ProxyAddress", this.address);
			serializationInfo.AddValue("_BypassList", this.bypassList);
			serializationInfo.AddValue("_UseDefaultCredentials", this.UseDefaultCredentials);
		}

		// Token: 0x06002622 RID: 9762 RVA: 0x00076D7C File Offset: 0x00074F7C
		private void CheckBypassList()
		{
			if (this.bypassList == null)
			{
				return;
			}
			for (int i = 0; i < this.bypassList.Count; i++)
			{
				new System.Text.RegularExpressions.Regex((string)this.bypassList[i]);
			}
		}

		// Token: 0x06002623 RID: 9763 RVA: 0x00076DC8 File Offset: 0x00074FC8
		private static System.Uri ToUri(string address)
		{
			if (address == null)
			{
				return null;
			}
			if (address.IndexOf("://") == -1)
			{
				address = "http://" + address;
			}
			return new System.Uri(address);
		}

		// Token: 0x04001777 RID: 6007
		private System.Uri address;

		// Token: 0x04001778 RID: 6008
		private bool bypassOnLocal;

		// Token: 0x04001779 RID: 6009
		private ArrayList bypassList;

		// Token: 0x0400177A RID: 6010
		private ICredentials credentials;

		// Token: 0x0400177B RID: 6011
		private bool useDefaultCredentials;
	}
}
