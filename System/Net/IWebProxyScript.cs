﻿using System;

namespace System.Net
{
	// Token: 0x0200032F RID: 815
	public interface IWebProxyScript
	{
		// Token: 0x06001CF4 RID: 7412
		void Close();

		// Token: 0x06001CF5 RID: 7413
		bool Load(System.Uri scriptLocation, string Script, Type helperType);

		// Token: 0x06001CF6 RID: 7414
		string Run(string url, string host);
	}
}
