﻿using System;

namespace System.Net
{
	// Token: 0x02000525 RID: 1317
	// (Invoke) Token: 0x06002D40 RID: 11584
	public delegate void DownloadDataCompletedEventHandler(object sender, DownloadDataCompletedEventArgs e);
}
