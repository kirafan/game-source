﻿using System;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x020003DA RID: 986
	[Serializable]
	public class ProtocolViolationException : InvalidOperationException, ISerializable
	{
		// Token: 0x060021AD RID: 8621 RVA: 0x000629F8 File Offset: 0x00060BF8
		public ProtocolViolationException()
		{
		}

		// Token: 0x060021AE RID: 8622 RVA: 0x00062A00 File Offset: 0x00060C00
		public ProtocolViolationException(string message) : base(message)
		{
		}

		// Token: 0x060021AF RID: 8623 RVA: 0x00062A0C File Offset: 0x00060C0C
		protected ProtocolViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060021B0 RID: 8624 RVA: 0x00062A18 File Offset: 0x00060C18
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		// Token: 0x060021B1 RID: 8625 RVA: 0x00062A24 File Offset: 0x00060C24
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
		}
	}
}
