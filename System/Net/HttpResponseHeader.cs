﻿using System;

namespace System.Net
{
	// Token: 0x0200031D RID: 797
	public enum HttpResponseHeader
	{
		// Token: 0x04001166 RID: 4454
		CacheControl,
		// Token: 0x04001167 RID: 4455
		Connection,
		// Token: 0x04001168 RID: 4456
		Date,
		// Token: 0x04001169 RID: 4457
		KeepAlive,
		// Token: 0x0400116A RID: 4458
		Pragma,
		// Token: 0x0400116B RID: 4459
		Trailer,
		// Token: 0x0400116C RID: 4460
		TransferEncoding,
		// Token: 0x0400116D RID: 4461
		Upgrade,
		// Token: 0x0400116E RID: 4462
		Via,
		// Token: 0x0400116F RID: 4463
		Warning,
		// Token: 0x04001170 RID: 4464
		Allow,
		// Token: 0x04001171 RID: 4465
		ContentLength,
		// Token: 0x04001172 RID: 4466
		ContentType,
		// Token: 0x04001173 RID: 4467
		ContentEncoding,
		// Token: 0x04001174 RID: 4468
		ContentLanguage,
		// Token: 0x04001175 RID: 4469
		ContentLocation,
		// Token: 0x04001176 RID: 4470
		ContentMd5,
		// Token: 0x04001177 RID: 4471
		ContentRange,
		// Token: 0x04001178 RID: 4472
		Expires,
		// Token: 0x04001179 RID: 4473
		LastModified,
		// Token: 0x0400117A RID: 4474
		AcceptRanges,
		// Token: 0x0400117B RID: 4475
		Age,
		// Token: 0x0400117C RID: 4476
		ETag,
		// Token: 0x0400117D RID: 4477
		Location,
		// Token: 0x0400117E RID: 4478
		ProxyAuthenticate,
		// Token: 0x0400117F RID: 4479
		RetryAfter,
		// Token: 0x04001180 RID: 4480
		Server,
		// Token: 0x04001181 RID: 4481
		SetCookie,
		// Token: 0x04001182 RID: 4482
		Vary,
		// Token: 0x04001183 RID: 4483
		WwwAuthenticate
	}
}
