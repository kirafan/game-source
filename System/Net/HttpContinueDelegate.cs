﻿using System;

namespace System.Net
{
	// Token: 0x02000516 RID: 1302
	// (Invoke) Token: 0x06002D04 RID: 11524
	public delegate void HttpContinueDelegate(int StatusCode, WebHeaderCollection httpHeaders);
}
