﻿using System;

namespace System.Net
{
	// Token: 0x0200032E RID: 814
	public interface IWebProxy
	{
		// Token: 0x17000734 RID: 1844
		// (get) Token: 0x06001CF0 RID: 7408
		// (set) Token: 0x06001CF1 RID: 7409
		ICredentials Credentials { get; set; }

		// Token: 0x06001CF2 RID: 7410
		System.Uri GetProxy(System.Uri destination);

		// Token: 0x06001CF3 RID: 7411
		bool IsBypassed(System.Uri host);
	}
}
