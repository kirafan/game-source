﻿using System;
using System.Collections;
using System.Security;
using System.Security.Permissions;

namespace System.Net
{
	// Token: 0x020003EA RID: 1002
	[Serializable]
	public sealed class SocketPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x060022AE RID: 8878 RVA: 0x00065774 File Offset: 0x00063974
		public SocketPermission(PermissionState state)
		{
			this.m_noRestriction = (state == PermissionState.Unrestricted);
		}

		// Token: 0x060022AF RID: 8879 RVA: 0x000657A8 File Offset: 0x000639A8
		public SocketPermission(NetworkAccess access, TransportType transport, string hostName, int portNumber)
		{
			this.m_noRestriction = false;
			this.AddPermission(access, transport, hostName, portNumber);
		}

		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x060022B0 RID: 8880 RVA: 0x000657E4 File Offset: 0x000639E4
		public IEnumerator AcceptList
		{
			get
			{
				return this.m_acceptList.GetEnumerator();
			}
		}

		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x060022B1 RID: 8881 RVA: 0x000657F4 File Offset: 0x000639F4
		public IEnumerator ConnectList
		{
			get
			{
				return this.m_connectList.GetEnumerator();
			}
		}

		// Token: 0x060022B2 RID: 8882 RVA: 0x00065804 File Offset: 0x00063A04
		public void AddPermission(NetworkAccess access, TransportType transport, string hostName, int portNumber)
		{
			if (this.m_noRestriction)
			{
				return;
			}
			EndpointPermission value = new EndpointPermission(hostName, portNumber, transport);
			if (access == NetworkAccess.Accept)
			{
				this.m_acceptList.Add(value);
			}
			else
			{
				this.m_connectList.Add(value);
			}
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x00065854 File Offset: 0x00063A54
		public override IPermission Copy()
		{
			return new SocketPermission((!this.m_noRestriction) ? PermissionState.None : PermissionState.Unrestricted)
			{
				m_connectList = (ArrayList)this.m_connectList.Clone(),
				m_acceptList = (ArrayList)this.m_acceptList.Clone()
			};
		}

		// Token: 0x060022B4 RID: 8884 RVA: 0x000658A8 File Offset: 0x00063AA8
		public override IPermission Intersect(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			SocketPermission socketPermission = target as SocketPermission;
			if (socketPermission == null)
			{
				throw new ArgumentException("Argument not of type SocketPermission");
			}
			if (this.m_noRestriction)
			{
				IPermission result;
				if (this.IntersectEmpty(socketPermission))
				{
					IPermission permission = null;
					result = permission;
				}
				else
				{
					result = socketPermission.Copy();
				}
				return result;
			}
			if (socketPermission.m_noRestriction)
			{
				IPermission result2;
				if (this.IntersectEmpty(this))
				{
					IPermission permission = null;
					result2 = permission;
				}
				else
				{
					result2 = this.Copy();
				}
				return result2;
			}
			SocketPermission socketPermission2 = new SocketPermission(PermissionState.None);
			this.Intersect(this.m_connectList, socketPermission.m_connectList, socketPermission2.m_connectList);
			this.Intersect(this.m_acceptList, socketPermission.m_acceptList, socketPermission2.m_acceptList);
			return (!this.IntersectEmpty(socketPermission2)) ? socketPermission2 : null;
		}

		// Token: 0x060022B5 RID: 8885 RVA: 0x0006596C File Offset: 0x00063B6C
		private bool IntersectEmpty(SocketPermission permission)
		{
			return !permission.m_noRestriction && permission.m_connectList.Count == 0 && permission.m_acceptList.Count == 0;
		}

		// Token: 0x060022B6 RID: 8886 RVA: 0x000659A8 File Offset: 0x00063BA8
		private void Intersect(ArrayList list1, ArrayList list2, ArrayList result)
		{
			foreach (object obj in list1)
			{
				EndpointPermission endpointPermission = (EndpointPermission)obj;
				foreach (object obj2 in list2)
				{
					EndpointPermission perm = (EndpointPermission)obj2;
					EndpointPermission endpointPermission2 = endpointPermission.Intersect(perm);
					if (endpointPermission2 != null)
					{
						bool flag = false;
						for (int i = 0; i < result.Count; i++)
						{
							EndpointPermission perm2 = (EndpointPermission)result[i];
							EndpointPermission endpointPermission3 = endpointPermission2.Intersect(perm2);
							if (endpointPermission3 != null)
							{
								result[i] = endpointPermission3;
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							result.Add(endpointPermission2);
						}
					}
				}
			}
		}

		// Token: 0x060022B7 RID: 8887 RVA: 0x00065AD4 File Offset: 0x00063CD4
		public override bool IsSubsetOf(IPermission target)
		{
			if (target == null)
			{
				return !this.m_noRestriction && this.m_connectList.Count == 0 && this.m_acceptList.Count == 0;
			}
			SocketPermission socketPermission = target as SocketPermission;
			if (socketPermission == null)
			{
				throw new ArgumentException("Parameter target must be of type SocketPermission");
			}
			return socketPermission.m_noRestriction || (!this.m_noRestriction && ((this.m_acceptList.Count == 0 && this.m_connectList.Count == 0) || ((socketPermission.m_acceptList.Count != 0 || socketPermission.m_connectList.Count != 0) && this.IsSubsetOf(this.m_connectList, socketPermission.m_connectList) && this.IsSubsetOf(this.m_acceptList, socketPermission.m_acceptList))));
		}

		// Token: 0x060022B8 RID: 8888 RVA: 0x00065BB8 File Offset: 0x00063DB8
		private bool IsSubsetOf(ArrayList list1, ArrayList list2)
		{
			foreach (object obj in list1)
			{
				EndpointPermission endpointPermission = (EndpointPermission)obj;
				bool flag = false;
				foreach (object obj2 in list2)
				{
					EndpointPermission perm = (EndpointPermission)obj2;
					if (endpointPermission.IsSubsetOf(perm))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060022B9 RID: 8889 RVA: 0x00065CA0 File Offset: 0x00063EA0
		public bool IsUnrestricted()
		{
			return this.m_noRestriction;
		}

		// Token: 0x060022BA RID: 8890 RVA: 0x00065CA8 File Offset: 0x00063EA8
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("IPermission");
			securityElement.AddAttribute("class", base.GetType().AssemblyQualifiedName);
			securityElement.AddAttribute("version", "1");
			if (this.m_noRestriction)
			{
				securityElement.AddAttribute("Unrestricted", "true");
				return securityElement;
			}
			if (this.m_connectList.Count > 0)
			{
				this.ToXml(securityElement, "ConnectAccess", this.m_connectList.GetEnumerator());
			}
			if (this.m_acceptList.Count > 0)
			{
				this.ToXml(securityElement, "AcceptAccess", this.m_acceptList.GetEnumerator());
			}
			return securityElement;
		}

		// Token: 0x060022BB RID: 8891 RVA: 0x00065D54 File Offset: 0x00063F54
		private void ToXml(SecurityElement root, string childName, IEnumerator enumerator)
		{
			SecurityElement securityElement = new SecurityElement(childName);
			while (enumerator.MoveNext())
			{
				object obj = enumerator.Current;
				EndpointPermission endpointPermission = obj as EndpointPermission;
				SecurityElement securityElement2 = new SecurityElement("ENDPOINT");
				securityElement2.AddAttribute("host", endpointPermission.Hostname);
				securityElement2.AddAttribute("transport", endpointPermission.Transport.ToString());
				securityElement2.AddAttribute("port", (endpointPermission.Port != -1) ? endpointPermission.Port.ToString() : "All");
				securityElement.AddChild(securityElement2);
			}
			root.AddChild(securityElement);
		}

		// Token: 0x060022BC RID: 8892 RVA: 0x00065DF8 File Offset: 0x00063FF8
		public override void FromXml(SecurityElement securityElement)
		{
			if (securityElement == null)
			{
				throw new ArgumentNullException("securityElement");
			}
			if (securityElement.Tag != "IPermission")
			{
				throw new ArgumentException("securityElement");
			}
			string text = securityElement.Attribute("Unrestricted");
			if (text != null)
			{
				this.m_noRestriction = (string.Compare(text, "true", true) == 0);
				if (this.m_noRestriction)
				{
					return;
				}
			}
			this.m_noRestriction = false;
			this.m_connectList = new ArrayList();
			this.m_acceptList = new ArrayList();
			ArrayList children = securityElement.Children;
			foreach (object obj in children)
			{
				SecurityElement securityElement2 = (SecurityElement)obj;
				if (securityElement2.Tag == "ConnectAccess")
				{
					this.FromXml(securityElement2.Children, NetworkAccess.Connect);
				}
				else if (securityElement2.Tag == "AcceptAccess")
				{
					this.FromXml(securityElement2.Children, NetworkAccess.Accept);
				}
			}
		}

		// Token: 0x060022BD RID: 8893 RVA: 0x00065F34 File Offset: 0x00064134
		private void FromXml(ArrayList endpoints, NetworkAccess access)
		{
			foreach (object obj in endpoints)
			{
				SecurityElement securityElement = (SecurityElement)obj;
				if (!(securityElement.Tag != "ENDPOINT"))
				{
					string hostName = securityElement.Attribute("host");
					TransportType transport = (TransportType)((int)Enum.Parse(typeof(TransportType), securityElement.Attribute("transport"), true));
					string text = securityElement.Attribute("port");
					int portNumber;
					if (text == "All")
					{
						portNumber = -1;
					}
					else
					{
						portNumber = int.Parse(text);
					}
					this.AddPermission(access, transport, hostName, portNumber);
				}
			}
		}

		// Token: 0x060022BE RID: 8894 RVA: 0x00066020 File Offset: 0x00064220
		public override IPermission Union(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			SocketPermission socketPermission = target as SocketPermission;
			if (socketPermission == null)
			{
				throw new ArgumentException("Argument not of type SocketPermission");
			}
			if (this.m_noRestriction || socketPermission.m_noRestriction)
			{
				return new SocketPermission(PermissionState.Unrestricted);
			}
			SocketPermission socketPermission2 = (SocketPermission)socketPermission.Copy();
			socketPermission2.m_acceptList.InsertRange(socketPermission2.m_acceptList.Count, this.m_acceptList);
			socketPermission2.m_connectList.InsertRange(socketPermission2.m_connectList.Count, this.m_connectList);
			return socketPermission2;
		}

		// Token: 0x04001535 RID: 5429
		public const int AllPorts = -1;

		// Token: 0x04001536 RID: 5430
		private ArrayList m_acceptList = new ArrayList();

		// Token: 0x04001537 RID: 5431
		private ArrayList m_connectList = new ArrayList();

		// Token: 0x04001538 RID: 5432
		private bool m_noRestriction;
	}
}
