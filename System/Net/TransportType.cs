﻿using System;

namespace System.Net
{
	// Token: 0x0200040E RID: 1038
	public enum TransportType
	{
		// Token: 0x040016D4 RID: 5844
		Udp = 1,
		// Token: 0x040016D5 RID: 5845
		Connectionless = 1,
		// Token: 0x040016D6 RID: 5846
		Tcp,
		// Token: 0x040016D7 RID: 5847
		ConnectionOriented = 2,
		// Token: 0x040016D8 RID: 5848
		All
	}
}
