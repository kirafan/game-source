﻿using System;

namespace System.Net
{
	// Token: 0x02000526 RID: 1318
	// (Invoke) Token: 0x06002D44 RID: 11588
	public delegate void DownloadProgressChangedEventHandler(object sender, DownloadProgressChangedEventArgs e);
}
