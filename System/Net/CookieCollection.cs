﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace System.Net
{
	// Token: 0x020002EF RID: 751
	[Serializable]
	public class CookieCollection : ICollection, IEnumerable
	{
		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x0600198F RID: 6543 RVA: 0x0004611C File Offset: 0x0004431C
		internal IList<Cookie> List
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06001990 RID: 6544 RVA: 0x00046124 File Offset: 0x00044324
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06001991 RID: 6545 RVA: 0x00046134 File Offset: 0x00044334
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x06001992 RID: 6546 RVA: 0x00046138 File Offset: 0x00044338
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06001993 RID: 6547 RVA: 0x0004613C File Offset: 0x0004433C
		public void CopyTo(Array array, int index)
		{
			((ICollection)this.list).CopyTo(array, index);
		}

		// Token: 0x06001994 RID: 6548 RVA: 0x0004614C File Offset: 0x0004434C
		public void CopyTo(Cookie[] array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x06001995 RID: 6549 RVA: 0x0004615C File Offset: 0x0004435C
		public IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x06001996 RID: 6550 RVA: 0x00046170 File Offset: 0x00044370
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06001997 RID: 6551 RVA: 0x00046174 File Offset: 0x00044374
		public void Add(Cookie cookie)
		{
			if (cookie == null)
			{
				throw new ArgumentNullException("cookie");
			}
			int num = this.SearchCookie(cookie);
			if (num == -1)
			{
				this.list.Add(cookie);
			}
			else
			{
				this.list[num] = cookie;
			}
		}

		// Token: 0x06001998 RID: 6552 RVA: 0x000461C0 File Offset: 0x000443C0
		internal void Sort()
		{
			if (this.list.Count > 0)
			{
				this.list.Sort(CookieCollection.Comparer);
			}
		}

		// Token: 0x06001999 RID: 6553 RVA: 0x000461E4 File Offset: 0x000443E4
		private int SearchCookie(Cookie cookie)
		{
			string name = cookie.Name;
			string domain = cookie.Domain;
			string path = cookie.Path;
			for (int i = this.list.Count - 1; i >= 0; i--)
			{
				Cookie cookie2 = this.list[i];
				if (cookie2.Version == cookie.Version)
				{
					if (string.Compare(domain, cookie2.Domain, true, CultureInfo.InvariantCulture) == 0)
					{
						if (string.Compare(name, cookie2.Name, true, CultureInfo.InvariantCulture) == 0)
						{
							if (string.Compare(path, cookie2.Path, true, CultureInfo.InvariantCulture) == 0)
							{
								return i;
							}
						}
					}
				}
			}
			return -1;
		}

		// Token: 0x0600199A RID: 6554 RVA: 0x000462A4 File Offset: 0x000444A4
		public void Add(CookieCollection cookies)
		{
			if (cookies == null)
			{
				throw new ArgumentNullException("cookies");
			}
			foreach (object obj in cookies)
			{
				Cookie cookie = (Cookie)obj;
				this.Add(cookie);
			}
		}

		// Token: 0x1700062A RID: 1578
		public Cookie this[int index]
		{
			get
			{
				if (index < 0 || index >= this.list.Count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				return this.list[index];
			}
		}

		// Token: 0x1700062B RID: 1579
		public Cookie this[string name]
		{
			get
			{
				foreach (Cookie cookie in this.list)
				{
					if (string.Compare(cookie.Name, name, true, CultureInfo.InvariantCulture) == 0)
					{
						return cookie;
					}
				}
				return null;
			}
		}

		// Token: 0x0400100E RID: 4110
		private List<Cookie> list = new List<Cookie>();

		// Token: 0x0400100F RID: 4111
		private static CookieCollection.CookieCollectionComparer Comparer = new CookieCollection.CookieCollectionComparer();

		// Token: 0x020002F0 RID: 752
		private sealed class CookieCollectionComparer : IComparer<Cookie>
		{
			// Token: 0x0600199E RID: 6558 RVA: 0x000463DC File Offset: 0x000445DC
			public int Compare(Cookie x, Cookie y)
			{
				if (x == null || y == null)
				{
					return 0;
				}
				int num = x.Name.Length + x.Value.Length;
				int num2 = y.Name.Length + y.Value.Length;
				return num - num2;
			}
		}
	}
}
