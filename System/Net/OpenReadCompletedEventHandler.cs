﻿using System;

namespace System.Net
{
	// Token: 0x02000524 RID: 1316
	// (Invoke) Token: 0x06002D3C RID: 11580
	public delegate void OpenReadCompletedEventHandler(object sender, OpenReadCompletedEventArgs e);
}
