﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Net.Cache;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace System.Net
{
	// Token: 0x02000410 RID: 1040
	[ComVisible(true)]
	public class WebClient : System.ComponentModel.Component
	{
		// Token: 0x060024BD RID: 9405 RVA: 0x0006E898 File Offset: 0x0006CA98
		static WebClient()
		{
			int num = 0;
			int i = 48;
			while (i <= 57)
			{
				WebClient.hexBytes[num] = (byte)i;
				i++;
				num++;
			}
			int j = 97;
			while (j <= 102)
			{
				WebClient.hexBytes[num] = (byte)j;
				j++;
				num++;
			}
		}

		// Token: 0x14000053 RID: 83
		// (add) Token: 0x060024BE RID: 9406 RVA: 0x0006E900 File Offset: 0x0006CB00
		// (remove) Token: 0x060024BF RID: 9407 RVA: 0x0006E91C File Offset: 0x0006CB1C
		public event DownloadDataCompletedEventHandler DownloadDataCompleted;

		// Token: 0x14000054 RID: 84
		// (add) Token: 0x060024C0 RID: 9408 RVA: 0x0006E938 File Offset: 0x0006CB38
		// (remove) Token: 0x060024C1 RID: 9409 RVA: 0x0006E954 File Offset: 0x0006CB54
		public event System.ComponentModel.AsyncCompletedEventHandler DownloadFileCompleted;

		// Token: 0x14000055 RID: 85
		// (add) Token: 0x060024C2 RID: 9410 RVA: 0x0006E970 File Offset: 0x0006CB70
		// (remove) Token: 0x060024C3 RID: 9411 RVA: 0x0006E98C File Offset: 0x0006CB8C
		public event DownloadProgressChangedEventHandler DownloadProgressChanged;

		// Token: 0x14000056 RID: 86
		// (add) Token: 0x060024C4 RID: 9412 RVA: 0x0006E9A8 File Offset: 0x0006CBA8
		// (remove) Token: 0x060024C5 RID: 9413 RVA: 0x0006E9C4 File Offset: 0x0006CBC4
		public event DownloadStringCompletedEventHandler DownloadStringCompleted;

		// Token: 0x14000057 RID: 87
		// (add) Token: 0x060024C6 RID: 9414 RVA: 0x0006E9E0 File Offset: 0x0006CBE0
		// (remove) Token: 0x060024C7 RID: 9415 RVA: 0x0006E9FC File Offset: 0x0006CBFC
		public event OpenReadCompletedEventHandler OpenReadCompleted;

		// Token: 0x14000058 RID: 88
		// (add) Token: 0x060024C8 RID: 9416 RVA: 0x0006EA18 File Offset: 0x0006CC18
		// (remove) Token: 0x060024C9 RID: 9417 RVA: 0x0006EA34 File Offset: 0x0006CC34
		public event OpenWriteCompletedEventHandler OpenWriteCompleted;

		// Token: 0x14000059 RID: 89
		// (add) Token: 0x060024CA RID: 9418 RVA: 0x0006EA50 File Offset: 0x0006CC50
		// (remove) Token: 0x060024CB RID: 9419 RVA: 0x0006EA6C File Offset: 0x0006CC6C
		public event UploadDataCompletedEventHandler UploadDataCompleted;

		// Token: 0x1400005A RID: 90
		// (add) Token: 0x060024CC RID: 9420 RVA: 0x0006EA88 File Offset: 0x0006CC88
		// (remove) Token: 0x060024CD RID: 9421 RVA: 0x0006EAA4 File Offset: 0x0006CCA4
		public event UploadFileCompletedEventHandler UploadFileCompleted;

		// Token: 0x1400005B RID: 91
		// (add) Token: 0x060024CE RID: 9422 RVA: 0x0006EAC0 File Offset: 0x0006CCC0
		// (remove) Token: 0x060024CF RID: 9423 RVA: 0x0006EADC File Offset: 0x0006CCDC
		public event UploadProgressChangedEventHandler UploadProgressChanged;

		// Token: 0x1400005C RID: 92
		// (add) Token: 0x060024D0 RID: 9424 RVA: 0x0006EAF8 File Offset: 0x0006CCF8
		// (remove) Token: 0x060024D1 RID: 9425 RVA: 0x0006EB14 File Offset: 0x0006CD14
		public event UploadStringCompletedEventHandler UploadStringCompleted;

		// Token: 0x1400005D RID: 93
		// (add) Token: 0x060024D2 RID: 9426 RVA: 0x0006EB30 File Offset: 0x0006CD30
		// (remove) Token: 0x060024D3 RID: 9427 RVA: 0x0006EB4C File Offset: 0x0006CD4C
		public event UploadValuesCompletedEventHandler UploadValuesCompleted;

		// Token: 0x17000A89 RID: 2697
		// (get) Token: 0x060024D4 RID: 9428 RVA: 0x0006EB68 File Offset: 0x0006CD68
		// (set) Token: 0x060024D5 RID: 9429 RVA: 0x0006EBA4 File Offset: 0x0006CDA4
		public string BaseAddress
		{
			get
			{
				if (this.baseString == null && this.baseAddress == null)
				{
					return string.Empty;
				}
				this.baseString = this.baseAddress.ToString();
				return this.baseString;
			}
			set
			{
				if (value == null || value.Length == 0)
				{
					this.baseAddress = null;
				}
				else
				{
					this.baseAddress = new System.Uri(value);
				}
			}
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x0006EBD0 File Offset: 0x0006CDD0
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		// Token: 0x17000A8A RID: 2698
		// (get) Token: 0x060024D7 RID: 9431 RVA: 0x0006EBD8 File Offset: 0x0006CDD8
		// (set) Token: 0x060024D8 RID: 9432 RVA: 0x0006EBE0 File Offset: 0x0006CDE0
		[MonoTODO]
		public System.Net.Cache.RequestCachePolicy CachePolicy
		{
			get
			{
				throw WebClient.GetMustImplement();
			}
			set
			{
				throw WebClient.GetMustImplement();
			}
		}

		// Token: 0x17000A8B RID: 2699
		// (get) Token: 0x060024D9 RID: 9433 RVA: 0x0006EBE8 File Offset: 0x0006CDE8
		// (set) Token: 0x060024DA RID: 9434 RVA: 0x0006EBF0 File Offset: 0x0006CDF0
		[MonoTODO]
		public bool UseDefaultCredentials
		{
			get
			{
				throw WebClient.GetMustImplement();
			}
			set
			{
				throw WebClient.GetMustImplement();
			}
		}

		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x060024DB RID: 9435 RVA: 0x0006EBF8 File Offset: 0x0006CDF8
		// (set) Token: 0x060024DC RID: 9436 RVA: 0x0006EC00 File Offset: 0x0006CE00
		public ICredentials Credentials
		{
			get
			{
				return this.credentials;
			}
			set
			{
				this.credentials = value;
			}
		}

		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x060024DD RID: 9437 RVA: 0x0006EC0C File Offset: 0x0006CE0C
		// (set) Token: 0x060024DE RID: 9438 RVA: 0x0006EC2C File Offset: 0x0006CE2C
		public WebHeaderCollection Headers
		{
			get
			{
				if (this.headers == null)
				{
					this.headers = new WebHeaderCollection();
				}
				return this.headers;
			}
			set
			{
				this.headers = value;
			}
		}

		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x060024DF RID: 9439 RVA: 0x0006EC38 File Offset: 0x0006CE38
		// (set) Token: 0x060024E0 RID: 9440 RVA: 0x0006EC58 File Offset: 0x0006CE58
		public System.Collections.Specialized.NameValueCollection QueryString
		{
			get
			{
				if (this.queryString == null)
				{
					this.queryString = new System.Collections.Specialized.NameValueCollection();
				}
				return this.queryString;
			}
			set
			{
				this.queryString = value;
			}
		}

		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x060024E1 RID: 9441 RVA: 0x0006EC64 File Offset: 0x0006CE64
		public WebHeaderCollection ResponseHeaders
		{
			get
			{
				return this.responseHeaders;
			}
		}

		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x060024E2 RID: 9442 RVA: 0x0006EC6C File Offset: 0x0006CE6C
		// (set) Token: 0x060024E3 RID: 9443 RVA: 0x0006EC74 File Offset: 0x0006CE74
		public Encoding Encoding
		{
			get
			{
				return this.encoding;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Encoding");
				}
				this.encoding = value;
			}
		}

		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x060024E4 RID: 9444 RVA: 0x0006EC90 File Offset: 0x0006CE90
		// (set) Token: 0x060024E5 RID: 9445 RVA: 0x0006EC98 File Offset: 0x0006CE98
		public IWebProxy Proxy
		{
			get
			{
				return this.proxy;
			}
			set
			{
				this.proxy = value;
			}
		}

		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x060024E6 RID: 9446 RVA: 0x0006ECA4 File Offset: 0x0006CEA4
		public bool IsBusy
		{
			get
			{
				return this.is_busy;
			}
		}

		// Token: 0x060024E7 RID: 9447 RVA: 0x0006ECAC File Offset: 0x0006CEAC
		private void CheckBusy()
		{
			if (this.IsBusy)
			{
				throw new NotSupportedException("WebClient does not support conccurent I/O operations.");
			}
		}

		// Token: 0x060024E8 RID: 9448 RVA: 0x0006ECC4 File Offset: 0x0006CEC4
		private void SetBusy()
		{
			lock (this)
			{
				this.CheckBusy();
				this.is_busy = true;
			}
		}

		// Token: 0x060024E9 RID: 9449 RVA: 0x0006ED10 File Offset: 0x0006CF10
		public byte[] DownloadData(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.DownloadData(this.CreateUri(address));
		}

		// Token: 0x060024EA RID: 9450 RVA: 0x0006ED30 File Offset: 0x0006CF30
		public byte[] DownloadData(System.Uri address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			byte[] result;
			try
			{
				this.SetBusy();
				this.async = false;
				result = this.DownloadDataCore(address, null);
			}
			finally
			{
				this.is_busy = false;
			}
			return result;
		}

		// Token: 0x060024EB RID: 9451 RVA: 0x0006ED9C File Offset: 0x0006CF9C
		private byte[] DownloadDataCore(System.Uri address, object userToken)
		{
			WebRequest webRequest = null;
			byte[] result;
			try
			{
				webRequest = this.SetupRequest(address);
				WebResponse webResponse = this.GetWebResponse(webRequest);
				Stream responseStream = webResponse.GetResponseStream();
				result = this.ReadAll(responseStream, (int)webResponse.ContentLength, userToken);
			}
			catch (ThreadInterruptedException)
			{
				if (webRequest != null)
				{
					webRequest.Abort();
				}
				throw;
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			return result;
		}

		// Token: 0x060024EC RID: 9452 RVA: 0x0006EE5C File Offset: 0x0006D05C
		public void DownloadFile(string address, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			this.DownloadFile(this.CreateUri(address), fileName);
		}

		// Token: 0x060024ED RID: 9453 RVA: 0x0006EE80 File Offset: 0x0006D080
		public void DownloadFile(System.Uri address, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			try
			{
				this.SetBusy();
				this.async = false;
				this.DownloadFileCore(address, fileName, null);
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
		}

		// Token: 0x060024EE RID: 9454 RVA: 0x0006EF40 File Offset: 0x0006D140
		private void DownloadFileCore(System.Uri address, string fileName, object userToken)
		{
			WebRequest webRequest = null;
			using (FileStream fileStream = new FileStream(fileName, FileMode.Create))
			{
				try
				{
					webRequest = this.SetupRequest(address);
					WebResponse webResponse = this.GetWebResponse(webRequest);
					Stream responseStream = webResponse.GetResponseStream();
					int num = (int)webResponse.ContentLength;
					int num2 = (num > -1 && num <= 32768) ? num : 32768;
					byte[] array = new byte[num2];
					long num3 = 0L;
					int num4;
					while ((num4 = responseStream.Read(array, 0, num2)) != 0)
					{
						if (this.async)
						{
							num3 += (long)num4;
							this.OnDownloadProgressChanged(new DownloadProgressChangedEventArgs(num3, webResponse.ContentLength, userToken));
						}
						fileStream.Write(array, 0, num4);
					}
				}
				catch (ThreadInterruptedException)
				{
					if (webRequest != null)
					{
						webRequest.Abort();
					}
					throw;
				}
			}
		}

		// Token: 0x060024EF RID: 9455 RVA: 0x0006F054 File Offset: 0x0006D254
		public Stream OpenRead(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenRead(this.CreateUri(address));
		}

		// Token: 0x060024F0 RID: 9456 RVA: 0x0006F074 File Offset: 0x0006D274
		public Stream OpenRead(System.Uri address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			Stream responseStream;
			try
			{
				this.SetBusy();
				this.async = false;
				WebRequest request = this.SetupRequest(address);
				WebResponse webResponse = this.GetWebResponse(request);
				responseStream = webResponse.GetResponseStream();
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
			return responseStream;
		}

		// Token: 0x060024F1 RID: 9457 RVA: 0x0006F13C File Offset: 0x0006D33C
		public Stream OpenWrite(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenWrite(this.CreateUri(address));
		}

		// Token: 0x060024F2 RID: 9458 RVA: 0x0006F15C File Offset: 0x0006D35C
		public Stream OpenWrite(string address, string method)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.OpenWrite(this.CreateUri(address), method);
		}

		// Token: 0x060024F3 RID: 9459 RVA: 0x0006F180 File Offset: 0x0006D380
		public Stream OpenWrite(System.Uri address)
		{
			return this.OpenWrite(address, null);
		}

		// Token: 0x060024F4 RID: 9460 RVA: 0x0006F18C File Offset: 0x0006D38C
		public Stream OpenWrite(System.Uri address, string method)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			Stream requestStream;
			try
			{
				this.SetBusy();
				this.async = false;
				WebRequest webRequest = this.SetupRequest(address, method, true);
				requestStream = webRequest.GetRequestStream();
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
			return requestStream;
		}

		// Token: 0x060024F5 RID: 9461 RVA: 0x0006F248 File Offset: 0x0006D448
		private string DetermineMethod(System.Uri address, string method, bool is_upload)
		{
			if (method != null)
			{
				return method;
			}
			if (address.Scheme == System.Uri.UriSchemeFtp)
			{
				return (!is_upload) ? "RETR" : "STOR";
			}
			return (!is_upload) ? "GET" : "POST";
		}

		// Token: 0x060024F6 RID: 9462 RVA: 0x0006F2A0 File Offset: 0x0006D4A0
		public byte[] UploadData(string address, byte[] data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadData(this.CreateUri(address), data);
		}

		// Token: 0x060024F7 RID: 9463 RVA: 0x0006F2C4 File Offset: 0x0006D4C4
		public byte[] UploadData(string address, string method, byte[] data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadData(this.CreateUri(address), method, data);
		}

		// Token: 0x060024F8 RID: 9464 RVA: 0x0006F2F4 File Offset: 0x0006D4F4
		public byte[] UploadData(System.Uri address, byte[] data)
		{
			return this.UploadData(address, null, data);
		}

		// Token: 0x060024F9 RID: 9465 RVA: 0x0006F300 File Offset: 0x0006D500
		public byte[] UploadData(System.Uri address, string method, byte[] data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] result;
			try
			{
				this.SetBusy();
				this.async = false;
				result = this.UploadDataCore(address, method, data, null);
			}
			catch (WebException)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
			return result;
		}

		// Token: 0x060024FA RID: 9466 RVA: 0x0006F3C8 File Offset: 0x0006D5C8
		private byte[] UploadDataCore(System.Uri address, string method, byte[] data, object userToken)
		{
			WebRequest webRequest = this.SetupRequest(address, method, true);
			byte[] result;
			try
			{
				int num = data.Length;
				webRequest.ContentLength = (long)num;
				using (Stream requestStream = webRequest.GetRequestStream())
				{
					requestStream.Write(data, 0, num);
				}
				WebResponse webResponse = this.GetWebResponse(webRequest);
				Stream responseStream = webResponse.GetResponseStream();
				result = this.ReadAll(responseStream, (int)webResponse.ContentLength, userToken);
			}
			catch (ThreadInterruptedException)
			{
				if (webRequest != null)
				{
					webRequest.Abort();
				}
				throw;
			}
			return result;
		}

		// Token: 0x060024FB RID: 9467 RVA: 0x0006F484 File Offset: 0x0006D684
		public byte[] UploadFile(string address, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadFile(this.CreateUri(address), fileName);
		}

		// Token: 0x060024FC RID: 9468 RVA: 0x0006F4A8 File Offset: 0x0006D6A8
		public byte[] UploadFile(System.Uri address, string fileName)
		{
			return this.UploadFile(address, null, fileName);
		}

		// Token: 0x060024FD RID: 9469 RVA: 0x0006F4B4 File Offset: 0x0006D6B4
		public byte[] UploadFile(string address, string method, string fileName)
		{
			return this.UploadFile(this.CreateUri(address), method, fileName);
		}

		// Token: 0x060024FE RID: 9470 RVA: 0x0006F4C8 File Offset: 0x0006D6C8
		public byte[] UploadFile(System.Uri address, string method, string fileName)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			byte[] result;
			try
			{
				this.SetBusy();
				this.async = false;
				result = this.UploadFileCore(address, method, fileName, null);
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
			return result;
		}

		// Token: 0x060024FF RID: 9471 RVA: 0x0006F590 File Offset: 0x0006D790
		private byte[] UploadFileCore(System.Uri address, string method, string fileName, object userToken)
		{
			string text = this.Headers["Content-Type"];
			if (text != null)
			{
				string text2 = text.ToLower();
				if (text2.StartsWith("multipart/"))
				{
					throw new WebException("Content-Type cannot be set to a multipart type for this request.");
				}
			}
			else
			{
				text = "application/octet-stream";
			}
			string text3 = "------------" + DateTime.Now.Ticks.ToString("x");
			this.Headers["Content-Type"] = string.Format("multipart/form-data; boundary={0}", text3);
			Stream stream = null;
			Stream stream2 = null;
			byte[] result = null;
			fileName = Path.GetFullPath(fileName);
			WebRequest webRequest = null;
			try
			{
				stream2 = File.OpenRead(fileName);
				webRequest = this.SetupRequest(address, method, true);
				stream = webRequest.GetRequestStream();
				byte[] bytes = Encoding.ASCII.GetBytes("--" + text3 + "\r\n");
				stream.Write(bytes, 0, bytes.Length);
				string s = string.Format("Content-Disposition: form-data; name=\"file\"; filename=\"{0}\"\r\nContent-Type: {1}\r\n\r\n", Path.GetFileName(fileName), text);
				byte[] bytes2 = Encoding.UTF8.GetBytes(s);
				stream.Write(bytes2, 0, bytes2.Length);
				byte[] buffer = new byte[4096];
				int count;
				while ((count = stream2.Read(buffer, 0, 4096)) != 0)
				{
					stream.Write(buffer, 0, count);
				}
				stream.WriteByte(13);
				stream.WriteByte(10);
				stream.Write(bytes, 0, bytes.Length);
				stream.Close();
				stream = null;
				WebResponse webResponse = this.GetWebResponse(webRequest);
				Stream responseStream = webResponse.GetResponseStream();
				result = this.ReadAll(responseStream, (int)webResponse.ContentLength, userToken);
			}
			catch (ThreadInterruptedException)
			{
				if (webRequest != null)
				{
					webRequest.Abort();
				}
				throw;
			}
			finally
			{
				if (stream2 != null)
				{
					stream2.Close();
				}
				if (stream != null)
				{
					stream.Close();
				}
			}
			return result;
		}

		// Token: 0x06002500 RID: 9472 RVA: 0x0006F794 File Offset: 0x0006D994
		public byte[] UploadValues(string address, System.Collections.Specialized.NameValueCollection data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadValues(this.CreateUri(address), data);
		}

		// Token: 0x06002501 RID: 9473 RVA: 0x0006F7B8 File Offset: 0x0006D9B8
		public byte[] UploadValues(string address, string method, System.Collections.Specialized.NameValueCollection data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.UploadValues(this.CreateUri(address), method, data);
		}

		// Token: 0x06002502 RID: 9474 RVA: 0x0006F7E8 File Offset: 0x0006D9E8
		public byte[] UploadValues(System.Uri address, System.Collections.Specialized.NameValueCollection data)
		{
			return this.UploadValues(address, null, data);
		}

		// Token: 0x06002503 RID: 9475 RVA: 0x0006F7F4 File Offset: 0x0006D9F4
		public byte[] UploadValues(System.Uri address, string method, System.Collections.Specialized.NameValueCollection data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] result;
			try
			{
				this.SetBusy();
				this.async = false;
				result = this.UploadValuesCore(address, method, data, null);
			}
			catch (WebException ex)
			{
				throw;
			}
			catch (Exception innerException)
			{
				throw new WebException("An error occurred performing a WebClient request.", innerException);
			}
			finally
			{
				this.is_busy = false;
			}
			return result;
		}

		// Token: 0x06002504 RID: 9476 RVA: 0x0006F8BC File Offset: 0x0006DABC
		private byte[] UploadValuesCore(System.Uri uri, string method, System.Collections.Specialized.NameValueCollection data, object userToken)
		{
			string text = this.Headers["Content-Type"];
			if (text != null && string.Compare(text, WebClient.urlEncodedCType, true) != 0)
			{
				throw new WebException("Content-Type header cannot be changed from its default value for this request.");
			}
			this.Headers["Content-Type"] = WebClient.urlEncodedCType;
			WebRequest webRequest = this.SetupRequest(uri, method, true);
			byte[] result;
			try
			{
				MemoryStream memoryStream = new MemoryStream();
				foreach (object obj in data)
				{
					string text2 = (string)obj;
					byte[] bytes = Encoding.UTF8.GetBytes(text2);
					WebClient.UrlEncodeAndWrite(memoryStream, bytes);
					memoryStream.WriteByte(61);
					bytes = Encoding.UTF8.GetBytes(data[text2]);
					WebClient.UrlEncodeAndWrite(memoryStream, bytes);
					memoryStream.WriteByte(38);
				}
				int num = (int)memoryStream.Length;
				if (num > 0)
				{
					memoryStream.SetLength((long)(--num));
				}
				byte[] buffer = memoryStream.GetBuffer();
				webRequest.ContentLength = (long)num;
				using (Stream requestStream = webRequest.GetRequestStream())
				{
					requestStream.Write(buffer, 0, num);
				}
				memoryStream.Close();
				WebResponse webResponse = this.GetWebResponse(webRequest);
				Stream responseStream = webResponse.GetResponseStream();
				result = this.ReadAll(responseStream, (int)webResponse.ContentLength, userToken);
			}
			catch (ThreadInterruptedException)
			{
				webRequest.Abort();
				throw;
			}
			return result;
		}

		// Token: 0x06002505 RID: 9477 RVA: 0x0006FA8C File Offset: 0x0006DC8C
		public string DownloadString(string address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.encoding.GetString(this.DownloadData(this.CreateUri(address)));
		}

		// Token: 0x06002506 RID: 9478 RVA: 0x0006FAB8 File Offset: 0x0006DCB8
		public string DownloadString(System.Uri address)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			return this.encoding.GetString(this.DownloadData(this.CreateUri(address)));
		}

		// Token: 0x06002507 RID: 9479 RVA: 0x0006FAEC File Offset: 0x0006DCEC
		public string UploadString(string address, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] bytes = this.UploadData(address, this.encoding.GetBytes(data));
			return this.encoding.GetString(bytes);
		}

		// Token: 0x06002508 RID: 9480 RVA: 0x0006FB3C File Offset: 0x0006DD3C
		public string UploadString(string address, string method, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] bytes = this.UploadData(address, method, this.encoding.GetBytes(data));
			return this.encoding.GetString(bytes);
		}

		// Token: 0x06002509 RID: 9481 RVA: 0x0006FB8C File Offset: 0x0006DD8C
		public string UploadString(System.Uri address, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] bytes = this.UploadData(address, this.encoding.GetBytes(data));
			return this.encoding.GetString(bytes);
		}

		// Token: 0x0600250A RID: 9482 RVA: 0x0006FBE4 File Offset: 0x0006DDE4
		public string UploadString(System.Uri address, string method, string data)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] bytes = this.UploadData(address, method, this.encoding.GetBytes(data));
			return this.encoding.GetString(bytes);
		}

		// Token: 0x0600250B RID: 9483 RVA: 0x0006FC3C File Offset: 0x0006DE3C
		private System.Uri CreateUri(string address)
		{
			return this.MakeUri(address);
		}

		// Token: 0x0600250C RID: 9484 RVA: 0x0006FC48 File Offset: 0x0006DE48
		private System.Uri CreateUri(System.Uri address)
		{
			string query = address.Query;
			if (string.IsNullOrEmpty(query))
			{
				query = this.GetQueryString(true);
			}
			if (this.baseAddress == null && query == null)
			{
				return address;
			}
			if (this.baseAddress == null)
			{
				return new System.Uri(address.ToString() + query, query != null);
			}
			if (query == null)
			{
				return new System.Uri(this.baseAddress, address.ToString());
			}
			return new System.Uri(this.baseAddress, address.ToString() + query, query != null);
		}

		// Token: 0x0600250D RID: 9485 RVA: 0x0006FCE8 File Offset: 0x0006DEE8
		private string GetQueryString(bool add_qmark)
		{
			if (this.queryString == null || this.queryString.Count == 0)
			{
				return null;
			}
			StringBuilder stringBuilder = new StringBuilder();
			if (add_qmark)
			{
				stringBuilder.Append('?');
			}
			foreach (object obj in this.queryString)
			{
				string text = (string)obj;
				stringBuilder.AppendFormat("{0}={1}&", text, this.UrlEncode(this.queryString[text]));
			}
			if (stringBuilder.Length != 0)
			{
				stringBuilder.Length--;
			}
			if (stringBuilder.Length == 0)
			{
				return null;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600250E RID: 9486 RVA: 0x0006FDD0 File Offset: 0x0006DFD0
		private System.Uri MakeUri(string path)
		{
			string text = this.GetQueryString(true);
			if (this.baseAddress == null && text == null)
			{
				try
				{
					return new System.Uri(path);
				}
				catch (ArgumentNullException)
				{
					if (Environment.UnityWebSecurityEnabled)
					{
						throw;
					}
					path = Path.GetFullPath(path);
					return new System.Uri("file://" + path);
				}
				catch (System.UriFormatException)
				{
					if (Environment.UnityWebSecurityEnabled)
					{
						throw;
					}
					path = Path.GetFullPath(path);
					return new System.Uri("file://" + path);
				}
			}
			if (this.baseAddress == null)
			{
				return new System.Uri(path + text, text != null);
			}
			if (text == null)
			{
				return new System.Uri(this.baseAddress, path);
			}
			return new System.Uri(this.baseAddress, path + text, text != null);
		}

		// Token: 0x0600250F RID: 9487 RVA: 0x0006FEF4 File Offset: 0x0006E0F4
		private WebRequest SetupRequest(System.Uri uri)
		{
			WebRequest webRequest = this.GetWebRequest(uri);
			if (this.Proxy != null)
			{
				webRequest.Proxy = this.Proxy;
			}
			webRequest.Credentials = this.credentials;
			if (this.headers != null && this.headers.Count != 0 && webRequest is HttpWebRequest)
			{
				HttpWebRequest httpWebRequest = (HttpWebRequest)webRequest;
				string text = this.headers["Expect"];
				string text2 = this.headers["Content-Type"];
				string text3 = this.headers["Accept"];
				string text4 = this.headers["Connection"];
				string text5 = this.headers["User-Agent"];
				string text6 = this.headers["Referer"];
				this.headers.RemoveInternal("Expect");
				this.headers.RemoveInternal("Content-Type");
				this.headers.RemoveInternal("Accept");
				this.headers.RemoveInternal("Connection");
				this.headers.RemoveInternal("Referer");
				this.headers.RemoveInternal("User-Agent");
				webRequest.Headers = this.headers;
				if (text != null && text.Length > 0)
				{
					httpWebRequest.Expect = text;
				}
				if (text3 != null && text3.Length > 0)
				{
					httpWebRequest.Accept = text3;
				}
				if (text2 != null && text2.Length > 0)
				{
					httpWebRequest.ContentType = text2;
				}
				if (text4 != null && text4.Length > 0)
				{
					httpWebRequest.Connection = text4;
				}
				if (text5 != null && text5.Length > 0)
				{
					httpWebRequest.UserAgent = text5;
				}
				if (text6 != null && text6.Length > 0)
				{
					httpWebRequest.Referer = text6;
				}
			}
			this.responseHeaders = null;
			return webRequest;
		}

		// Token: 0x06002510 RID: 9488 RVA: 0x000700DC File Offset: 0x0006E2DC
		private WebRequest SetupRequest(System.Uri uri, string method, bool is_upload)
		{
			WebRequest webRequest = this.SetupRequest(uri);
			webRequest.Method = this.DetermineMethod(uri, method, is_upload);
			return webRequest;
		}

		// Token: 0x06002511 RID: 9489 RVA: 0x00070104 File Offset: 0x0006E304
		private byte[] ReadAll(Stream stream, int length, object userToken)
		{
			MemoryStream memoryStream = null;
			bool flag = length == -1;
			int num = (!flag) ? length : 8192;
			if (flag)
			{
				memoryStream = new MemoryStream();
			}
			int num2 = 0;
			byte[] array = new byte[num];
			int num3;
			while ((num3 = stream.Read(array, num2, num)) != 0)
			{
				if (flag)
				{
					memoryStream.Write(array, 0, num3);
				}
				else
				{
					num2 += num3;
					num -= num3;
				}
				if (this.async)
				{
					this.OnDownloadProgressChanged(new DownloadProgressChangedEventArgs((long)num3, (long)length, userToken));
				}
			}
			if (flag)
			{
				return memoryStream.ToArray();
			}
			return array;
		}

		// Token: 0x06002512 RID: 9490 RVA: 0x000701A4 File Offset: 0x0006E3A4
		private string UrlEncode(string str)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int length = str.Length;
			for (int i = 0; i < length; i++)
			{
				char c = str[i];
				if (c == ' ')
				{
					stringBuilder.Append('+');
				}
				else if ((c < '0' && c != '-' && c != '.') || (c < 'A' && c > '9') || (c > 'Z' && c < 'a' && c != '_') || c > 'z')
				{
					stringBuilder.Append('%');
					int num = (int)(c >> 4);
					stringBuilder.Append((char)WebClient.hexBytes[num]);
					num = (int)(c & '\u000f');
					stringBuilder.Append((char)WebClient.hexBytes[num]);
				}
				else
				{
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06002513 RID: 9491 RVA: 0x00070280 File Offset: 0x0006E480
		private static void UrlEncodeAndWrite(Stream stream, byte[] bytes)
		{
			if (bytes == null)
			{
				return;
			}
			int num = bytes.Length;
			if (num == 0)
			{
				return;
			}
			for (int i = 0; i < num; i++)
			{
				char c = (char)bytes[i];
				if (c == ' ')
				{
					stream.WriteByte(43);
				}
				else if ((c < '0' && c != '-' && c != '.') || (c < 'A' && c > '9') || (c > 'Z' && c < 'a' && c != '_') || c > 'z')
				{
					stream.WriteByte(37);
					int num2 = (int)(c >> 4);
					stream.WriteByte(WebClient.hexBytes[num2]);
					num2 = (int)(c & '\u000f');
					stream.WriteByte(WebClient.hexBytes[num2]);
				}
				else
				{
					stream.WriteByte((byte)c);
				}
			}
		}

		// Token: 0x06002514 RID: 9492 RVA: 0x0007034C File Offset: 0x0006E54C
		public void CancelAsync()
		{
			lock (this)
			{
				if (this.async_thread != null)
				{
					Thread thread = this.async_thread;
					this.CompleteAsync();
					thread.Interrupt();
				}
			}
		}

		// Token: 0x06002515 RID: 9493 RVA: 0x000703AC File Offset: 0x0006E5AC
		private void CompleteAsync()
		{
			lock (this)
			{
				this.is_busy = false;
				this.async_thread = null;
			}
		}

		// Token: 0x06002516 RID: 9494 RVA: 0x000703F8 File Offset: 0x0006E5F8
		public void DownloadDataAsync(System.Uri address)
		{
			this.DownloadDataAsync(address, null);
		}

		// Token: 0x06002517 RID: 9495 RVA: 0x00070404 File Offset: 0x0006E604
		public void DownloadDataAsync(System.Uri address, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						byte[] result = this.DownloadDataCore((System.Uri)array[0], array[1]);
						this.OnDownloadDataCompleted(new DownloadDataCompletedEventArgs(result, null, false, array[1]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnDownloadDataCompleted(new DownloadDataCompletedEventArgs(null, null, true, array[1]));
						throw;
					}
					catch (Exception error)
					{
						this.OnDownloadDataCompleted(new DownloadDataCompletedEventArgs(null, error, false, array[1]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x06002518 RID: 9496 RVA: 0x00070498 File Offset: 0x0006E698
		public void DownloadFileAsync(System.Uri address, string fileName)
		{
			this.DownloadFileAsync(address, fileName, null);
		}

		// Token: 0x06002519 RID: 9497 RVA: 0x000704A4 File Offset: 0x0006E6A4
		public void DownloadFileAsync(System.Uri address, string fileName, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						this.DownloadFileCore((System.Uri)array[0], (string)array[1], array[2]);
						this.OnDownloadFileCompleted(new System.ComponentModel.AsyncCompletedEventArgs(null, false, array[2]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnDownloadFileCompleted(new System.ComponentModel.AsyncCompletedEventArgs(null, true, array[2]));
					}
					catch (Exception error)
					{
						this.OnDownloadFileCompleted(new System.ComponentModel.AsyncCompletedEventArgs(error, false, array[2]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					fileName,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x0600251A RID: 9498 RVA: 0x0007054C File Offset: 0x0006E74C
		public void DownloadStringAsync(System.Uri address)
		{
			this.DownloadStringAsync(address, null);
		}

		// Token: 0x0600251B RID: 9499 RVA: 0x00070558 File Offset: 0x0006E758
		public void DownloadStringAsync(System.Uri address, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						string @string = this.encoding.GetString(this.DownloadDataCore((System.Uri)array[0], array[1]));
						this.OnDownloadStringCompleted(new DownloadStringCompletedEventArgs(@string, null, false, array[1]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnDownloadStringCompleted(new DownloadStringCompletedEventArgs(null, null, true, array[1]));
					}
					catch (Exception error)
					{
						this.OnDownloadStringCompleted(new DownloadStringCompletedEventArgs(null, error, false, array[1]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x0600251C RID: 9500 RVA: 0x000705EC File Offset: 0x0006E7EC
		public void OpenReadAsync(System.Uri address)
		{
			this.OpenReadAsync(address, null);
		}

		// Token: 0x0600251D RID: 9501 RVA: 0x000705F8 File Offset: 0x0006E7F8
		public void OpenReadAsync(System.Uri address, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					WebRequest webRequest = null;
					try
					{
						webRequest = this.SetupRequest((System.Uri)array[0]);
						WebResponse webResponse = this.GetWebResponse(webRequest);
						Stream responseStream = webResponse.GetResponseStream();
						this.OnOpenReadCompleted(new OpenReadCompletedEventArgs(responseStream, null, false, array[1]));
					}
					catch (ThreadInterruptedException)
					{
						if (webRequest != null)
						{
							webRequest.Abort();
						}
						this.OnOpenReadCompleted(new OpenReadCompletedEventArgs(null, null, true, array[1]));
					}
					catch (Exception error)
					{
						this.OnOpenReadCompleted(new OpenReadCompletedEventArgs(null, error, false, array[1]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x0600251E RID: 9502 RVA: 0x0007068C File Offset: 0x0006E88C
		public void OpenWriteAsync(System.Uri address)
		{
			this.OpenWriteAsync(address, null);
		}

		// Token: 0x0600251F RID: 9503 RVA: 0x00070698 File Offset: 0x0006E898
		public void OpenWriteAsync(System.Uri address, string method)
		{
			this.OpenWriteAsync(address, method, null);
		}

		// Token: 0x06002520 RID: 9504 RVA: 0x000706A4 File Offset: 0x0006E8A4
		public void OpenWriteAsync(System.Uri address, string method, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					WebRequest webRequest = null;
					try
					{
						webRequest = this.SetupRequest((System.Uri)array[0], (string)array[1], true);
						Stream requestStream = webRequest.GetRequestStream();
						this.OnOpenWriteCompleted(new OpenWriteCompletedEventArgs(requestStream, null, false, array[2]));
					}
					catch (ThreadInterruptedException)
					{
						if (webRequest != null)
						{
							webRequest.Abort();
						}
						this.OnOpenWriteCompleted(new OpenWriteCompletedEventArgs(null, null, true, array[2]));
					}
					catch (Exception error)
					{
						this.OnOpenWriteCompleted(new OpenWriteCompletedEventArgs(null, error, false, array[2]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					method,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x06002521 RID: 9505 RVA: 0x0007073C File Offset: 0x0006E93C
		public void UploadDataAsync(System.Uri address, byte[] data)
		{
			this.UploadDataAsync(address, null, data);
		}

		// Token: 0x06002522 RID: 9506 RVA: 0x00070748 File Offset: 0x0006E948
		public void UploadDataAsync(System.Uri address, string method, byte[] data)
		{
			this.UploadDataAsync(address, method, data, null);
		}

		// Token: 0x06002523 RID: 9507 RVA: 0x00070754 File Offset: 0x0006E954
		public void UploadDataAsync(System.Uri address, string method, byte[] data, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						byte[] result = this.UploadDataCore((System.Uri)array[0], (string)array[1], (byte[])array[2], array[3]);
						this.OnUploadDataCompleted(new UploadDataCompletedEventArgs(result, null, false, array[3]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnUploadDataCompleted(new UploadDataCompletedEventArgs(null, null, true, array[3]));
					}
					catch (Exception error)
					{
						this.OnUploadDataCompleted(new UploadDataCompletedEventArgs(null, error, false, array[3]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					method,
					data,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x06002524 RID: 9508 RVA: 0x00070804 File Offset: 0x0006EA04
		public void UploadFileAsync(System.Uri address, string fileName)
		{
			this.UploadFileAsync(address, null, fileName);
		}

		// Token: 0x06002525 RID: 9509 RVA: 0x00070810 File Offset: 0x0006EA10
		public void UploadFileAsync(System.Uri address, string method, string fileName)
		{
			this.UploadFileAsync(address, method, fileName, null);
		}

		// Token: 0x06002526 RID: 9510 RVA: 0x0007081C File Offset: 0x0006EA1C
		public void UploadFileAsync(System.Uri address, string method, string fileName, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			lock (this)
			{
				this.SetBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						byte[] result = this.UploadFileCore((System.Uri)array[0], (string)array[1], (string)array[2], array[3]);
						this.OnUploadFileCompleted(new UploadFileCompletedEventArgs(result, null, false, array[3]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnUploadFileCompleted(new UploadFileCompletedEventArgs(null, null, true, array[3]));
					}
					catch (Exception error)
					{
						this.OnUploadFileCompleted(new UploadFileCompletedEventArgs(null, error, false, array[3]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					method,
					fileName,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x06002527 RID: 9511 RVA: 0x000708CC File Offset: 0x0006EACC
		public void UploadStringAsync(System.Uri address, string data)
		{
			this.UploadStringAsync(address, null, data);
		}

		// Token: 0x06002528 RID: 9512 RVA: 0x000708D8 File Offset: 0x0006EAD8
		public void UploadStringAsync(System.Uri address, string method, string data)
		{
			this.UploadStringAsync(address, method, data, null);
		}

		// Token: 0x06002529 RID: 9513 RVA: 0x000708E4 File Offset: 0x0006EAE4
		public void UploadStringAsync(System.Uri address, string method, string data, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			lock (this)
			{
				this.CheckBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						string result = this.UploadString((System.Uri)array[0], (string)array[1], (string)array[2]);
						this.OnUploadStringCompleted(new UploadStringCompletedEventArgs(result, null, false, array[3]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnUploadStringCompleted(new UploadStringCompletedEventArgs(null, null, true, array[3]));
					}
					catch (Exception error)
					{
						this.OnUploadStringCompleted(new UploadStringCompletedEventArgs(null, error, false, array[3]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					method,
					data,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x0600252A RID: 9514 RVA: 0x00070994 File Offset: 0x0006EB94
		public void UploadValuesAsync(System.Uri address, System.Collections.Specialized.NameValueCollection values)
		{
			this.UploadValuesAsync(address, null, values);
		}

		// Token: 0x0600252B RID: 9515 RVA: 0x000709A0 File Offset: 0x0006EBA0
		public void UploadValuesAsync(System.Uri address, string method, System.Collections.Specialized.NameValueCollection values)
		{
			this.UploadValuesAsync(address, method, values, null);
		}

		// Token: 0x0600252C RID: 9516 RVA: 0x000709AC File Offset: 0x0006EBAC
		public void UploadValuesAsync(System.Uri address, string method, System.Collections.Specialized.NameValueCollection values, object userToken)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			lock (this)
			{
				this.CheckBusy();
				this.async = true;
				this.async_thread = new Thread(delegate(object state)
				{
					object[] array = (object[])state;
					try
					{
						byte[] result = this.UploadValuesCore((System.Uri)array[0], (string)array[1], (System.Collections.Specialized.NameValueCollection)array[2], array[3]);
						this.OnUploadValuesCompleted(new UploadValuesCompletedEventArgs(result, null, false, array[3]));
					}
					catch (ThreadInterruptedException)
					{
						this.OnUploadValuesCompleted(new UploadValuesCompletedEventArgs(null, null, true, array[3]));
					}
					catch (Exception error)
					{
						this.OnUploadValuesCompleted(new UploadValuesCompletedEventArgs(null, error, false, array[3]));
					}
				});
				object[] parameter = new object[]
				{
					address,
					method,
					values,
					userToken
				};
				this.async_thread.Start(parameter);
			}
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x00070A5C File Offset: 0x0006EC5C
		protected virtual void OnDownloadDataCompleted(DownloadDataCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.DownloadDataCompleted != null)
			{
				this.DownloadDataCompleted(this, args);
			}
		}

		// Token: 0x0600252E RID: 9518 RVA: 0x00070A7C File Offset: 0x0006EC7C
		protected virtual void OnDownloadFileCompleted(System.ComponentModel.AsyncCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.DownloadFileCompleted != null)
			{
				this.DownloadFileCompleted(this, args);
			}
		}

		// Token: 0x0600252F RID: 9519 RVA: 0x00070A9C File Offset: 0x0006EC9C
		protected virtual void OnDownloadProgressChanged(DownloadProgressChangedEventArgs e)
		{
			if (this.DownloadProgressChanged != null)
			{
				this.DownloadProgressChanged(this, e);
			}
		}

		// Token: 0x06002530 RID: 9520 RVA: 0x00070AB8 File Offset: 0x0006ECB8
		protected virtual void OnDownloadStringCompleted(DownloadStringCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.DownloadStringCompleted != null)
			{
				this.DownloadStringCompleted(this, args);
			}
		}

		// Token: 0x06002531 RID: 9521 RVA: 0x00070AD8 File Offset: 0x0006ECD8
		protected virtual void OnOpenReadCompleted(OpenReadCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.OpenReadCompleted != null)
			{
				this.OpenReadCompleted(this, args);
			}
		}

		// Token: 0x06002532 RID: 9522 RVA: 0x00070AF8 File Offset: 0x0006ECF8
		protected virtual void OnOpenWriteCompleted(OpenWriteCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.OpenWriteCompleted != null)
			{
				this.OpenWriteCompleted(this, args);
			}
		}

		// Token: 0x06002533 RID: 9523 RVA: 0x00070B18 File Offset: 0x0006ED18
		protected virtual void OnUploadDataCompleted(UploadDataCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.UploadDataCompleted != null)
			{
				this.UploadDataCompleted(this, args);
			}
		}

		// Token: 0x06002534 RID: 9524 RVA: 0x00070B38 File Offset: 0x0006ED38
		protected virtual void OnUploadFileCompleted(UploadFileCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.UploadFileCompleted != null)
			{
				this.UploadFileCompleted(this, args);
			}
		}

		// Token: 0x06002535 RID: 9525 RVA: 0x00070B58 File Offset: 0x0006ED58
		protected virtual void OnUploadProgressChanged(UploadProgressChangedEventArgs e)
		{
			if (this.UploadProgressChanged != null)
			{
				this.UploadProgressChanged(this, e);
			}
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x00070B74 File Offset: 0x0006ED74
		protected virtual void OnUploadStringCompleted(UploadStringCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.UploadStringCompleted != null)
			{
				this.UploadStringCompleted(this, args);
			}
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x00070B94 File Offset: 0x0006ED94
		protected virtual void OnUploadValuesCompleted(UploadValuesCompletedEventArgs args)
		{
			this.CompleteAsync();
			if (this.UploadValuesCompleted != null)
			{
				this.UploadValuesCompleted(this, args);
			}
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x00070BB4 File Offset: 0x0006EDB4
		protected virtual WebResponse GetWebResponse(WebRequest request, IAsyncResult result)
		{
			WebResponse webResponse = request.EndGetResponse(result);
			this.responseHeaders = webResponse.Headers;
			return webResponse;
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x00070BD8 File Offset: 0x0006EDD8
		protected virtual WebRequest GetWebRequest(System.Uri address)
		{
			return WebRequest.Create(address);
		}

		// Token: 0x0600253A RID: 9530 RVA: 0x00070BE0 File Offset: 0x0006EDE0
		protected virtual WebResponse GetWebResponse(WebRequest request)
		{
			WebResponse response = request.GetResponse();
			this.responseHeaders = response.Headers;
			return response;
		}

		// Token: 0x040016EA RID: 5866
		private static readonly string urlEncodedCType = "application/x-www-form-urlencoded";

		// Token: 0x040016EB RID: 5867
		private static byte[] hexBytes = new byte[16];

		// Token: 0x040016EC RID: 5868
		private ICredentials credentials;

		// Token: 0x040016ED RID: 5869
		private WebHeaderCollection headers;

		// Token: 0x040016EE RID: 5870
		private WebHeaderCollection responseHeaders;

		// Token: 0x040016EF RID: 5871
		private System.Uri baseAddress;

		// Token: 0x040016F0 RID: 5872
		private string baseString;

		// Token: 0x040016F1 RID: 5873
		private System.Collections.Specialized.NameValueCollection queryString;

		// Token: 0x040016F2 RID: 5874
		private bool is_busy;

		// Token: 0x040016F3 RID: 5875
		private bool async;

		// Token: 0x040016F4 RID: 5876
		private Thread async_thread;

		// Token: 0x040016F5 RID: 5877
		private Encoding encoding = Encoding.Default;

		// Token: 0x040016F6 RID: 5878
		private IWebProxy proxy;
	}
}
