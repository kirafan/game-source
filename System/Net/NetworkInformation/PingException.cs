﻿using System;
using System.Runtime.Serialization;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003B6 RID: 950
	[Serializable]
	public class PingException : InvalidOperationException
	{
		// Token: 0x06002115 RID: 8469 RVA: 0x00061FEC File Offset: 0x000601EC
		public PingException(string message) : base(message)
		{
		}

		// Token: 0x06002116 RID: 8470 RVA: 0x00061FF8 File Offset: 0x000601F8
		public PingException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06002117 RID: 8471 RVA: 0x00062004 File Offset: 0x00060204
		protected PingException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}
	}
}
