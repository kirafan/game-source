﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000379 RID: 889
	public abstract class IPGlobalStatistics
	{
		// Token: 0x1700085B RID: 2139
		// (get) Token: 0x06001F98 RID: 8088
		public abstract int DefaultTtl { get; }

		// Token: 0x1700085C RID: 2140
		// (get) Token: 0x06001F99 RID: 8089
		public abstract bool ForwardingEnabled { get; }

		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x06001F9A RID: 8090
		public abstract int NumberOfInterfaces { get; }

		// Token: 0x1700085E RID: 2142
		// (get) Token: 0x06001F9B RID: 8091
		public abstract int NumberOfIPAddresses { get; }

		// Token: 0x1700085F RID: 2143
		// (get) Token: 0x06001F9C RID: 8092
		public abstract int NumberOfRoutes { get; }

		// Token: 0x17000860 RID: 2144
		// (get) Token: 0x06001F9D RID: 8093
		public abstract long OutputPacketRequests { get; }

		// Token: 0x17000861 RID: 2145
		// (get) Token: 0x06001F9E RID: 8094
		public abstract long OutputPacketRoutingDiscards { get; }

		// Token: 0x17000862 RID: 2146
		// (get) Token: 0x06001F9F RID: 8095
		public abstract long OutputPacketsDiscarded { get; }

		// Token: 0x17000863 RID: 2147
		// (get) Token: 0x06001FA0 RID: 8096
		public abstract long OutputPacketsWithNoRoute { get; }

		// Token: 0x17000864 RID: 2148
		// (get) Token: 0x06001FA1 RID: 8097
		public abstract long PacketFragmentFailures { get; }

		// Token: 0x17000865 RID: 2149
		// (get) Token: 0x06001FA2 RID: 8098
		public abstract long PacketReassembliesRequired { get; }

		// Token: 0x17000866 RID: 2150
		// (get) Token: 0x06001FA3 RID: 8099
		public abstract long PacketReassemblyFailures { get; }

		// Token: 0x17000867 RID: 2151
		// (get) Token: 0x06001FA4 RID: 8100
		public abstract long PacketReassemblyTimeout { get; }

		// Token: 0x17000868 RID: 2152
		// (get) Token: 0x06001FA5 RID: 8101
		public abstract long PacketsFragmented { get; }

		// Token: 0x17000869 RID: 2153
		// (get) Token: 0x06001FA6 RID: 8102
		public abstract long PacketsReassembled { get; }

		// Token: 0x1700086A RID: 2154
		// (get) Token: 0x06001FA7 RID: 8103
		public abstract long ReceivedPackets { get; }

		// Token: 0x1700086B RID: 2155
		// (get) Token: 0x06001FA8 RID: 8104
		public abstract long ReceivedPacketsDelivered { get; }

		// Token: 0x1700086C RID: 2156
		// (get) Token: 0x06001FA9 RID: 8105
		public abstract long ReceivedPacketsDiscarded { get; }

		// Token: 0x1700086D RID: 2157
		// (get) Token: 0x06001FAA RID: 8106
		public abstract long ReceivedPacketsForwarded { get; }

		// Token: 0x1700086E RID: 2158
		// (get) Token: 0x06001FAB RID: 8107
		public abstract long ReceivedPacketsWithAddressErrors { get; }

		// Token: 0x1700086F RID: 2159
		// (get) Token: 0x06001FAC RID: 8108
		public abstract long ReceivedPacketsWithHeadersErrors { get; }

		// Token: 0x17000870 RID: 2160
		// (get) Token: 0x06001FAD RID: 8109
		public abstract long ReceivedPacketsWithUnknownProtocol { get; }
	}
}
