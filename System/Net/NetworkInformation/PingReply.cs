﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003B8 RID: 952
	public class PingReply
	{
		// Token: 0x0600211E RID: 8478 RVA: 0x00062090 File Offset: 0x00060290
		internal PingReply(IPAddress address, byte[] buffer, PingOptions options, long roundtripTime, IPStatus status)
		{
			this.address = address;
			this.buffer = buffer;
			this.options = options;
			this.rtt = roundtripTime;
			this.status = status;
		}

		// Token: 0x17000942 RID: 2370
		// (get) Token: 0x0600211F RID: 8479 RVA: 0x000620C0 File Offset: 0x000602C0
		public IPAddress Address
		{
			get
			{
				return this.address;
			}
		}

		// Token: 0x17000943 RID: 2371
		// (get) Token: 0x06002120 RID: 8480 RVA: 0x000620C8 File Offset: 0x000602C8
		public byte[] Buffer
		{
			get
			{
				return this.buffer;
			}
		}

		// Token: 0x17000944 RID: 2372
		// (get) Token: 0x06002121 RID: 8481 RVA: 0x000620D0 File Offset: 0x000602D0
		public PingOptions Options
		{
			get
			{
				return this.options;
			}
		}

		// Token: 0x17000945 RID: 2373
		// (get) Token: 0x06002122 RID: 8482 RVA: 0x000620D8 File Offset: 0x000602D8
		public long RoundtripTime
		{
			get
			{
				return this.rtt;
			}
		}

		// Token: 0x17000946 RID: 2374
		// (get) Token: 0x06002123 RID: 8483 RVA: 0x000620E0 File Offset: 0x000602E0
		public IPStatus Status
		{
			get
			{
				return this.status;
			}
		}

		// Token: 0x04001430 RID: 5168
		private IPAddress address;

		// Token: 0x04001431 RID: 5169
		private byte[] buffer;

		// Token: 0x04001432 RID: 5170
		private PingOptions options;

		// Token: 0x04001433 RID: 5171
		private long rtt;

		// Token: 0x04001434 RID: 5172
		private IPStatus status;
	}
}
