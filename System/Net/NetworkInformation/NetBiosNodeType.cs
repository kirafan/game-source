﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003A1 RID: 929
	public enum NetBiosNodeType
	{
		// Token: 0x040013CA RID: 5066
		Unknown,
		// Token: 0x040013CB RID: 5067
		Broadcast,
		// Token: 0x040013CC RID: 5068
		Peer2Peer,
		// Token: 0x040013CD RID: 5069
		Mixed = 4,
		// Token: 0x040013CE RID: 5070
		Hybrid = 8
	}
}
