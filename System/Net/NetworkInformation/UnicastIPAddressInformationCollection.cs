﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003C6 RID: 966
	public class UnicastIPAddressInformationCollection : IEnumerable, IEnumerable<UnicastIPAddressInformation>, ICollection<UnicastIPAddressInformation>
	{
		// Token: 0x0600216D RID: 8557 RVA: 0x00062450 File Offset: 0x00060650
		protected internal UnicastIPAddressInformationCollection()
		{
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x00062464 File Offset: 0x00060664
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x0600216F RID: 8559 RVA: 0x00062478 File Offset: 0x00060678
		public virtual void Add(UnicastIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Add(address);
		}

		// Token: 0x06002170 RID: 8560 RVA: 0x000624A8 File Offset: 0x000606A8
		public virtual void Clear()
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Clear();
		}

		// Token: 0x06002171 RID: 8561 RVA: 0x000624CC File Offset: 0x000606CC
		public virtual bool Contains(UnicastIPAddressInformation address)
		{
			return this.list.Contains(address);
		}

		// Token: 0x06002172 RID: 8562 RVA: 0x000624DC File Offset: 0x000606DC
		public virtual void CopyTo(UnicastIPAddressInformation[] array, int offset)
		{
			this.list.CopyTo(array, offset);
		}

		// Token: 0x06002173 RID: 8563 RVA: 0x000624EC File Offset: 0x000606EC
		public virtual IEnumerator<UnicastIPAddressInformation> GetEnumerator()
		{
			return ((IEnumerable<UnicastIPAddressInformation>)this.list).GetEnumerator();
		}

		// Token: 0x06002174 RID: 8564 RVA: 0x000624FC File Offset: 0x000606FC
		public virtual bool Remove(UnicastIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			return this.list.Remove(address);
		}

		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x06002175 RID: 8565 RVA: 0x0006252C File Offset: 0x0006072C
		public virtual int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x06002176 RID: 8566 RVA: 0x0006253C File Offset: 0x0006073C
		public virtual bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000988 RID: 2440
		public virtual UnicastIPAddressInformation this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x0400146B RID: 5227
		private List<UnicastIPAddressInformation> list = new List<UnicastIPAddressInformation>();
	}
}
