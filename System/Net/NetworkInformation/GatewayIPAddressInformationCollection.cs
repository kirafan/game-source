﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200035B RID: 859
	public class GatewayIPAddressInformationCollection : IEnumerable, IEnumerable<GatewayIPAddressInformation>, ICollection<GatewayIPAddressInformation>
	{
		// Token: 0x06001E48 RID: 7752 RVA: 0x0005CB80 File Offset: 0x0005AD80
		protected GatewayIPAddressInformationCollection()
		{
		}

		// Token: 0x06001E49 RID: 7753 RVA: 0x0005CB94 File Offset: 0x0005AD94
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06001E4A RID: 7754 RVA: 0x0005CBA8 File Offset: 0x0005ADA8
		public virtual void Add(GatewayIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Add(address);
		}

		// Token: 0x06001E4B RID: 7755 RVA: 0x0005CBD8 File Offset: 0x0005ADD8
		public virtual void Clear()
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Clear();
		}

		// Token: 0x06001E4C RID: 7756 RVA: 0x0005CBFC File Offset: 0x0005ADFC
		public virtual bool Contains(GatewayIPAddressInformation address)
		{
			return this.list.Contains(address);
		}

		// Token: 0x06001E4D RID: 7757 RVA: 0x0005CC0C File Offset: 0x0005AE0C
		public virtual void CopyTo(GatewayIPAddressInformation[] array, int offset)
		{
			this.list.CopyTo(array, offset);
		}

		// Token: 0x06001E4E RID: 7758 RVA: 0x0005CC1C File Offset: 0x0005AE1C
		public virtual IEnumerator<GatewayIPAddressInformation> GetEnumerator()
		{
			return ((IEnumerable<GatewayIPAddressInformation>)this.list).GetEnumerator();
		}

		// Token: 0x06001E4F RID: 7759 RVA: 0x0005CC2C File Offset: 0x0005AE2C
		public virtual bool Remove(GatewayIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			return this.list.Remove(address);
		}

		// Token: 0x17000781 RID: 1921
		// (get) Token: 0x06001E50 RID: 7760 RVA: 0x0005CC5C File Offset: 0x0005AE5C
		public virtual int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000782 RID: 1922
		// (get) Token: 0x06001E51 RID: 7761 RVA: 0x0005CC6C File Offset: 0x0005AE6C
		public virtual bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000783 RID: 1923
		public virtual GatewayIPAddressInformation this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x040012DD RID: 4829
		private List<GatewayIPAddressInformation> list = new List<GatewayIPAddressInformation>();
	}
}
