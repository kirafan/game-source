﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200035A RID: 858
	public enum DuplicateAddressDetectionState
	{
		// Token: 0x040012D8 RID: 4824
		Invalid,
		// Token: 0x040012D9 RID: 4825
		Tentative,
		// Token: 0x040012DA RID: 4826
		Duplicate,
		// Token: 0x040012DB RID: 4827
		Deprecated,
		// Token: 0x040012DC RID: 4828
		Preferred
	}
}
