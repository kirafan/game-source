﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200039D RID: 925
	public class MulticastIPAddressInformationCollection : IEnumerable, IEnumerable<MulticastIPAddressInformation>, ICollection<MulticastIPAddressInformation>
	{
		// Token: 0x06002063 RID: 8291 RVA: 0x0005FB1C File Offset: 0x0005DD1C
		protected internal MulticastIPAddressInformationCollection()
		{
		}

		// Token: 0x06002064 RID: 8292 RVA: 0x0005FB30 File Offset: 0x0005DD30
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06002065 RID: 8293 RVA: 0x0005FB44 File Offset: 0x0005DD44
		public virtual void Add(MulticastIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Add(address);
		}

		// Token: 0x06002066 RID: 8294 RVA: 0x0005FB74 File Offset: 0x0005DD74
		public virtual void Clear()
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Clear();
		}

		// Token: 0x06002067 RID: 8295 RVA: 0x0005FB98 File Offset: 0x0005DD98
		public virtual bool Contains(MulticastIPAddressInformation address)
		{
			return this.list.Contains(address);
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x0005FBA8 File Offset: 0x0005DDA8
		public virtual void CopyTo(MulticastIPAddressInformation[] array, int offset)
		{
			this.list.CopyTo(array, offset);
		}

		// Token: 0x06002069 RID: 8297 RVA: 0x0005FBB8 File Offset: 0x0005DDB8
		public virtual IEnumerator<MulticastIPAddressInformation> GetEnumerator()
		{
			return ((IEnumerable<MulticastIPAddressInformation>)this.list).GetEnumerator();
		}

		// Token: 0x0600206A RID: 8298 RVA: 0x0005FBC8 File Offset: 0x0005DDC8
		public virtual bool Remove(MulticastIPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			return this.list.Remove(address);
		}

		// Token: 0x17000906 RID: 2310
		// (get) Token: 0x0600206B RID: 8299 RVA: 0x0005FBF8 File Offset: 0x0005DDF8
		public virtual int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000907 RID: 2311
		// (get) Token: 0x0600206C RID: 8300 RVA: 0x0005FC08 File Offset: 0x0005DE08
		public virtual bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000908 RID: 2312
		public virtual MulticastIPAddressInformation this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x040013C3 RID: 5059
		private List<MulticastIPAddressInformation> list = new List<MulticastIPAddressInformation>();
	}
}
