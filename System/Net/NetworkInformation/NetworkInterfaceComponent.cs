﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003A8 RID: 936
	public enum NetworkInterfaceComponent
	{
		// Token: 0x040013DC RID: 5084
		IPv4,
		// Token: 0x040013DD RID: 5085
		IPv6
	}
}
