﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000383 RID: 899
	public abstract class IPv4InterfaceProperties
	{
		// Token: 0x170008BB RID: 2235
		// (get) Token: 0x0600200B RID: 8203
		public abstract int Index { get; }

		// Token: 0x170008BC RID: 2236
		// (get) Token: 0x0600200C RID: 8204
		public abstract bool IsAutomaticPrivateAddressingActive { get; }

		// Token: 0x170008BD RID: 2237
		// (get) Token: 0x0600200D RID: 8205
		public abstract bool IsAutomaticPrivateAddressingEnabled { get; }

		// Token: 0x170008BE RID: 2238
		// (get) Token: 0x0600200E RID: 8206
		public abstract bool IsDhcpEnabled { get; }

		// Token: 0x170008BF RID: 2239
		// (get) Token: 0x0600200F RID: 8207
		public abstract bool IsForwardingEnabled { get; }

		// Token: 0x170008C0 RID: 2240
		// (get) Token: 0x06002010 RID: 8208
		public abstract int Mtu { get; }

		// Token: 0x170008C1 RID: 2241
		// (get) Token: 0x06002011 RID: 8209
		public abstract bool UsesWins { get; }
	}
}
