﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000365 RID: 869
	public abstract class IcmpV6Statistics
	{
		// Token: 0x170007D6 RID: 2006
		// (get) Token: 0x06001EB3 RID: 7859
		public abstract long DestinationUnreachableMessagesReceived { get; }

		// Token: 0x170007D7 RID: 2007
		// (get) Token: 0x06001EB4 RID: 7860
		public abstract long DestinationUnreachableMessagesSent { get; }

		// Token: 0x170007D8 RID: 2008
		// (get) Token: 0x06001EB5 RID: 7861
		public abstract long EchoRepliesReceived { get; }

		// Token: 0x170007D9 RID: 2009
		// (get) Token: 0x06001EB6 RID: 7862
		public abstract long EchoRepliesSent { get; }

		// Token: 0x170007DA RID: 2010
		// (get) Token: 0x06001EB7 RID: 7863
		public abstract long EchoRequestsReceived { get; }

		// Token: 0x170007DB RID: 2011
		// (get) Token: 0x06001EB8 RID: 7864
		public abstract long EchoRequestsSent { get; }

		// Token: 0x170007DC RID: 2012
		// (get) Token: 0x06001EB9 RID: 7865
		public abstract long ErrorsReceived { get; }

		// Token: 0x170007DD RID: 2013
		// (get) Token: 0x06001EBA RID: 7866
		public abstract long ErrorsSent { get; }

		// Token: 0x170007DE RID: 2014
		// (get) Token: 0x06001EBB RID: 7867
		public abstract long MembershipQueriesReceived { get; }

		// Token: 0x170007DF RID: 2015
		// (get) Token: 0x06001EBC RID: 7868
		public abstract long MembershipQueriesSent { get; }

		// Token: 0x170007E0 RID: 2016
		// (get) Token: 0x06001EBD RID: 7869
		public abstract long MembershipReductionsReceived { get; }

		// Token: 0x170007E1 RID: 2017
		// (get) Token: 0x06001EBE RID: 7870
		public abstract long MembershipReductionsSent { get; }

		// Token: 0x170007E2 RID: 2018
		// (get) Token: 0x06001EBF RID: 7871
		public abstract long MembershipReportsReceived { get; }

		// Token: 0x170007E3 RID: 2019
		// (get) Token: 0x06001EC0 RID: 7872
		public abstract long MembershipReportsSent { get; }

		// Token: 0x170007E4 RID: 2020
		// (get) Token: 0x06001EC1 RID: 7873
		public abstract long MessagesReceived { get; }

		// Token: 0x170007E5 RID: 2021
		// (get) Token: 0x06001EC2 RID: 7874
		public abstract long MessagesSent { get; }

		// Token: 0x170007E6 RID: 2022
		// (get) Token: 0x06001EC3 RID: 7875
		public abstract long NeighborAdvertisementsReceived { get; }

		// Token: 0x170007E7 RID: 2023
		// (get) Token: 0x06001EC4 RID: 7876
		public abstract long NeighborAdvertisementsSent { get; }

		// Token: 0x170007E8 RID: 2024
		// (get) Token: 0x06001EC5 RID: 7877
		public abstract long NeighborSolicitsReceived { get; }

		// Token: 0x170007E9 RID: 2025
		// (get) Token: 0x06001EC6 RID: 7878
		public abstract long NeighborSolicitsSent { get; }

		// Token: 0x170007EA RID: 2026
		// (get) Token: 0x06001EC7 RID: 7879
		public abstract long PacketTooBigMessagesReceived { get; }

		// Token: 0x170007EB RID: 2027
		// (get) Token: 0x06001EC8 RID: 7880
		public abstract long PacketTooBigMessagesSent { get; }

		// Token: 0x170007EC RID: 2028
		// (get) Token: 0x06001EC9 RID: 7881
		public abstract long ParameterProblemsReceived { get; }

		// Token: 0x170007ED RID: 2029
		// (get) Token: 0x06001ECA RID: 7882
		public abstract long ParameterProblemsSent { get; }

		// Token: 0x170007EE RID: 2030
		// (get) Token: 0x06001ECB RID: 7883
		public abstract long RedirectsReceived { get; }

		// Token: 0x170007EF RID: 2031
		// (get) Token: 0x06001ECC RID: 7884
		public abstract long RedirectsSent { get; }

		// Token: 0x170007F0 RID: 2032
		// (get) Token: 0x06001ECD RID: 7885
		public abstract long RouterAdvertisementsReceived { get; }

		// Token: 0x170007F1 RID: 2033
		// (get) Token: 0x06001ECE RID: 7886
		public abstract long RouterAdvertisementsSent { get; }

		// Token: 0x170007F2 RID: 2034
		// (get) Token: 0x06001ECF RID: 7887
		public abstract long RouterSolicitsReceived { get; }

		// Token: 0x170007F3 RID: 2035
		// (get) Token: 0x06001ED0 RID: 7888
		public abstract long RouterSolicitsSent { get; }

		// Token: 0x170007F4 RID: 2036
		// (get) Token: 0x06001ED1 RID: 7889
		public abstract long TimeExceededMessagesReceived { get; }

		// Token: 0x170007F5 RID: 2037
		// (get) Token: 0x06001ED2 RID: 7890
		public abstract long TimeExceededMessagesSent { get; }
	}
}
