﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200036D RID: 877
	public class IPAddressInformationCollection : IEnumerable, IEnumerable<IPAddressInformation>, ICollection<IPAddressInformation>
	{
		// Token: 0x06001F2B RID: 7979 RVA: 0x0005DA00 File Offset: 0x0005BC00
		internal IPAddressInformationCollection()
		{
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x0005DA14 File Offset: 0x0005BC14
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06001F2D RID: 7981 RVA: 0x0005DA28 File Offset: 0x0005BC28
		public virtual void Add(IPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Add(address);
		}

		// Token: 0x06001F2E RID: 7982 RVA: 0x0005DA58 File Offset: 0x0005BC58
		public virtual void Clear()
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Clear();
		}

		// Token: 0x06001F2F RID: 7983 RVA: 0x0005DA7C File Offset: 0x0005BC7C
		public virtual bool Contains(IPAddressInformation address)
		{
			return this.list.Contains(address);
		}

		// Token: 0x06001F30 RID: 7984 RVA: 0x0005DA8C File Offset: 0x0005BC8C
		public virtual void CopyTo(IPAddressInformation[] array, int offset)
		{
			this.list.CopyTo(array, offset);
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x0005DA9C File Offset: 0x0005BC9C
		public virtual IEnumerator<IPAddressInformation> GetEnumerator()
		{
			return ((IEnumerable<IPAddressInformation>)this.list).GetEnumerator();
		}

		// Token: 0x06001F32 RID: 7986 RVA: 0x0005DAAC File Offset: 0x0005BCAC
		public virtual bool Remove(IPAddressInformation address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			return this.list.Remove(address);
		}

		// Token: 0x1700083A RID: 2106
		// (get) Token: 0x06001F33 RID: 7987 RVA: 0x0005DADC File Offset: 0x0005BCDC
		public virtual int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x1700083B RID: 2107
		// (get) Token: 0x06001F34 RID: 7988 RVA: 0x0005DAEC File Offset: 0x0005BCEC
		public virtual bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700083C RID: 2108
		public virtual IPAddressInformation this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x0400130F RID: 4879
		private List<IPAddressInformation> list = new List<IPAddressInformation>();
	}
}
