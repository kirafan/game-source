﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003AF RID: 943
	public enum OperationalStatus
	{
		// Token: 0x04001412 RID: 5138
		Up = 1,
		// Token: 0x04001413 RID: 5139
		Down,
		// Token: 0x04001414 RID: 5140
		Testing,
		// Token: 0x04001415 RID: 5141
		Unknown,
		// Token: 0x04001416 RID: 5142
		Dormant,
		// Token: 0x04001417 RID: 5143
		NotPresent,
		// Token: 0x04001418 RID: 5144
		LowerLayerDown
	}
}
