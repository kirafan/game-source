﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200036F RID: 879
	public abstract class IPAddressInformation
	{
		// Token: 0x1700083E RID: 2110
		// (get) Token: 0x06001F3C RID: 7996
		public abstract IPAddress Address { get; }

		// Token: 0x1700083F RID: 2111
		// (get) Token: 0x06001F3D RID: 7997
		public abstract bool IsDnsEligible { get; }

		// Token: 0x17000840 RID: 2112
		// (get) Token: 0x06001F3E RID: 7998
		public abstract bool IsTransient { get; }
	}
}
