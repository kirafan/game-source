﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003B7 RID: 951
	public class PingOptions
	{
		// Token: 0x06002118 RID: 8472 RVA: 0x00062010 File Offset: 0x00060210
		public PingOptions()
		{
		}

		// Token: 0x06002119 RID: 8473 RVA: 0x00062024 File Offset: 0x00060224
		public PingOptions(int ttl, bool dontFragment)
		{
			if (ttl <= 0)
			{
				throw new ArgumentOutOfRangeException("Must be greater than zero.", "ttl");
			}
			this.ttl = ttl;
			this.dont_fragment = dontFragment;
		}

		// Token: 0x17000940 RID: 2368
		// (get) Token: 0x0600211A RID: 8474 RVA: 0x00062068 File Offset: 0x00060268
		// (set) Token: 0x0600211B RID: 8475 RVA: 0x00062070 File Offset: 0x00060270
		public bool DontFragment
		{
			get
			{
				return this.dont_fragment;
			}
			set
			{
				this.dont_fragment = value;
			}
		}

		// Token: 0x17000941 RID: 2369
		// (get) Token: 0x0600211C RID: 8476 RVA: 0x0006207C File Offset: 0x0006027C
		// (set) Token: 0x0600211D RID: 8477 RVA: 0x00062084 File Offset: 0x00060284
		public int Ttl
		{
			get
			{
				return this.ttl;
			}
			set
			{
				this.ttl = value;
			}
		}

		// Token: 0x0400142E RID: 5166
		private int ttl = 128;

		// Token: 0x0400142F RID: 5167
		private bool dont_fragment;
	}
}
