﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003C8 RID: 968
	public abstract class UnicastIPAddressInformation : IPAddressInformation
	{
		// Token: 0x1700098A RID: 2442
		// (get) Token: 0x0600217E RID: 8574
		public abstract long AddressPreferredLifetime { get; }

		// Token: 0x1700098B RID: 2443
		// (get) Token: 0x0600217F RID: 8575
		public abstract long AddressValidLifetime { get; }

		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x06002180 RID: 8576
		public abstract long DhcpLeaseLifetime { get; }

		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x06002181 RID: 8577
		public abstract DuplicateAddressDetectionState DuplicateAddressDetectionState { get; }

		// Token: 0x1700098E RID: 2446
		// (get) Token: 0x06002182 RID: 8578
		public abstract IPAddress IPv4Mask { get; }

		// Token: 0x1700098F RID: 2447
		// (get) Token: 0x06002183 RID: 8579
		public abstract PrefixOrigin PrefixOrigin { get; }

		// Token: 0x17000990 RID: 2448
		// (get) Token: 0x06002184 RID: 8580
		public abstract SuffixOrigin SuffixOrigin { get; }
	}
}
