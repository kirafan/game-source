﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200035E RID: 862
	public abstract class GatewayIPAddressInformation
	{
		// Token: 0x17000786 RID: 1926
		// (get) Token: 0x06001E5D RID: 7773
		public abstract IPAddress Address { get; }
	}
}
