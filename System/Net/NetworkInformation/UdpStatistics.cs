﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003C2 RID: 962
	public abstract class UdpStatistics
	{
		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x0600215B RID: 8539
		public abstract long DatagramsReceived { get; }

		// Token: 0x17000978 RID: 2424
		// (get) Token: 0x0600215C RID: 8540
		public abstract long DatagramsSent { get; }

		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x0600215D RID: 8541
		public abstract long IncomingDatagramsDiscarded { get; }

		// Token: 0x1700097A RID: 2426
		// (get) Token: 0x0600215E RID: 8542
		public abstract long IncomingDatagramsWithErrors { get; }

		// Token: 0x1700097B RID: 2427
		// (get) Token: 0x0600215F RID: 8543
		public abstract int UdpListeners { get; }
	}
}
