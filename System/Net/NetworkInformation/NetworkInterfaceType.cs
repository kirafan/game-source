﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003AE RID: 942
	public enum NetworkInterfaceType
	{
		// Token: 0x040013F8 RID: 5112
		Unknown = 1,
		// Token: 0x040013F9 RID: 5113
		Ethernet = 6,
		// Token: 0x040013FA RID: 5114
		TokenRing = 9,
		// Token: 0x040013FB RID: 5115
		Fddi = 15,
		// Token: 0x040013FC RID: 5116
		BasicIsdn = 20,
		// Token: 0x040013FD RID: 5117
		PrimaryIsdn,
		// Token: 0x040013FE RID: 5118
		Ppp = 23,
		// Token: 0x040013FF RID: 5119
		Loopback,
		// Token: 0x04001400 RID: 5120
		Ethernet3Megabit = 26,
		// Token: 0x04001401 RID: 5121
		Slip = 28,
		// Token: 0x04001402 RID: 5122
		Atm = 37,
		// Token: 0x04001403 RID: 5123
		GenericModem = 48,
		// Token: 0x04001404 RID: 5124
		FastEthernetT = 62,
		// Token: 0x04001405 RID: 5125
		Isdn,
		// Token: 0x04001406 RID: 5126
		FastEthernetFx = 69,
		// Token: 0x04001407 RID: 5127
		Wireless80211 = 71,
		// Token: 0x04001408 RID: 5128
		AsymmetricDsl = 94,
		// Token: 0x04001409 RID: 5129
		RateAdaptDsl,
		// Token: 0x0400140A RID: 5130
		SymmetricDsl,
		// Token: 0x0400140B RID: 5131
		VeryHighSpeedDsl,
		// Token: 0x0400140C RID: 5132
		IPOverAtm = 114,
		// Token: 0x0400140D RID: 5133
		GigabitEthernet = 117,
		// Token: 0x0400140E RID: 5134
		Tunnel = 131,
		// Token: 0x0400140F RID: 5135
		MultiRateSymmetricDsl = 143,
		// Token: 0x04001410 RID: 5136
		HighPerformanceSerialBus
	}
}
