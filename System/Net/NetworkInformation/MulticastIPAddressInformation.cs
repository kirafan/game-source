﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200039F RID: 927
	public abstract class MulticastIPAddressInformation : IPAddressInformation
	{
		// Token: 0x1700090A RID: 2314
		// (get) Token: 0x06002074 RID: 8308
		public abstract long AddressPreferredLifetime { get; }

		// Token: 0x1700090B RID: 2315
		// (get) Token: 0x06002075 RID: 8309
		public abstract long AddressValidLifetime { get; }

		// Token: 0x1700090C RID: 2316
		// (get) Token: 0x06002076 RID: 8310
		public abstract long DhcpLeaseLifetime { get; }

		// Token: 0x1700090D RID: 2317
		// (get) Token: 0x06002077 RID: 8311
		public abstract DuplicateAddressDetectionState DuplicateAddressDetectionState { get; }

		// Token: 0x1700090E RID: 2318
		// (get) Token: 0x06002078 RID: 8312
		public abstract PrefixOrigin PrefixOrigin { get; }

		// Token: 0x1700090F RID: 2319
		// (get) Token: 0x06002079 RID: 8313
		public abstract SuffixOrigin SuffixOrigin { get; }
	}
}
