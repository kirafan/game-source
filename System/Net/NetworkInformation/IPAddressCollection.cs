﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200036B RID: 875
	public class IPAddressCollection : IEnumerable, ICollection<IPAddress>, IEnumerable<IPAddress>
	{
		// Token: 0x06001F17 RID: 7959 RVA: 0x0005D6D4 File Offset: 0x0005B8D4
		protected internal IPAddressCollection()
		{
		}

		// Token: 0x06001F18 RID: 7960 RVA: 0x0005D6E8 File Offset: 0x0005B8E8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x0005D6F8 File Offset: 0x0005B8F8
		internal void SetReadOnly()
		{
			if (!this.IsReadOnly)
			{
				this.list = ((List<IPAddress>)this.list).AsReadOnly();
			}
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x0005D71C File Offset: 0x0005B91C
		public virtual void Add(IPAddress address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Add(address);
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x0005D74C File Offset: 0x0005B94C
		public virtual void Clear()
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			this.list.Clear();
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x0005D770 File Offset: 0x0005B970
		public virtual bool Contains(IPAddress address)
		{
			return this.list.Contains(address);
		}

		// Token: 0x06001F1D RID: 7965 RVA: 0x0005D780 File Offset: 0x0005B980
		public virtual void CopyTo(IPAddress[] array, int offset)
		{
			this.list.CopyTo(array, offset);
		}

		// Token: 0x06001F1E RID: 7966 RVA: 0x0005D790 File Offset: 0x0005B990
		public virtual IEnumerator<IPAddress> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06001F1F RID: 7967 RVA: 0x0005D7A0 File Offset: 0x0005B9A0
		public virtual bool Remove(IPAddress address)
		{
			if (this.IsReadOnly)
			{
				throw new NotSupportedException("The collection is read-only.");
			}
			return this.list.Remove(address);
		}

		// Token: 0x17000836 RID: 2102
		// (get) Token: 0x06001F20 RID: 7968 RVA: 0x0005D7D0 File Offset: 0x0005B9D0
		public virtual int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000837 RID: 2103
		// (get) Token: 0x06001F21 RID: 7969 RVA: 0x0005D7E0 File Offset: 0x0005B9E0
		public virtual bool IsReadOnly
		{
			get
			{
				return this.list.IsReadOnly;
			}
		}

		// Token: 0x17000838 RID: 2104
		public virtual IPAddress this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x0400130C RID: 4876
		private IList<IPAddress> list = new List<IPAddress>();
	}
}
