﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000518 RID: 1304
	// (Invoke) Token: 0x06002D0C RID: 11532
	public delegate void NetworkAddressChangedEventHandler(object sender, EventArgs e);
}
