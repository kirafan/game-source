﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003A7 RID: 935
	[Serializable]
	public sealed class NetworkInformationPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06002093 RID: 8339 RVA: 0x0005FF70 File Offset: 0x0005E170
		[MonoTODO]
		public NetworkInformationPermission(PermissionState state)
		{
		}

		// Token: 0x06002094 RID: 8340 RVA: 0x0005FF78 File Offset: 0x0005E178
		[MonoTODO]
		public NetworkInformationPermission(NetworkInformationAccess access)
		{
		}

		// Token: 0x06002095 RID: 8341 RVA: 0x0005FF80 File Offset: 0x0005E180
		[MonoTODO]
		public void AddPermission(NetworkInformationAccess access)
		{
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x0005FF84 File Offset: 0x0005E184
		[MonoTODO]
		public override IPermission Copy()
		{
			return null;
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x0005FF88 File Offset: 0x0005E188
		[MonoTODO]
		public override void FromXml(SecurityElement securityElement)
		{
		}

		// Token: 0x06002098 RID: 8344 RVA: 0x0005FF8C File Offset: 0x0005E18C
		[MonoTODO]
		public override IPermission Intersect(IPermission target)
		{
			return null;
		}

		// Token: 0x06002099 RID: 8345 RVA: 0x0005FF90 File Offset: 0x0005E190
		[MonoTODO]
		public override bool IsSubsetOf(IPermission target)
		{
			return false;
		}

		// Token: 0x0600209A RID: 8346 RVA: 0x0005FF94 File Offset: 0x0005E194
		[MonoTODO]
		public bool IsUnrestricted()
		{
			return false;
		}

		// Token: 0x0600209B RID: 8347 RVA: 0x0005FF98 File Offset: 0x0005E198
		[MonoTODO]
		public override SecurityElement ToXml()
		{
			return PermissionHelper.Element(typeof(NetworkInformationPermission), 1);
		}

		// Token: 0x0600209C RID: 8348 RVA: 0x0005FFB8 File Offset: 0x0005E1B8
		[MonoTODO]
		public override IPermission Union(IPermission target)
		{
			return null;
		}

		// Token: 0x1700091C RID: 2332
		// (get) Token: 0x0600209D RID: 8349 RVA: 0x0005FFBC File Offset: 0x0005E1BC
		[MonoTODO]
		public NetworkInformationAccess Access
		{
			get
			{
				return NetworkInformationAccess.None;
			}
		}

		// Token: 0x040013DA RID: 5082
		private const int version = 1;
	}
}
