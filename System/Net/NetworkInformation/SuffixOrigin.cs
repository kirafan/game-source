﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003BA RID: 954
	public enum SuffixOrigin
	{
		// Token: 0x0400143C RID: 5180
		Other,
		// Token: 0x0400143D RID: 5181
		Manual,
		// Token: 0x0400143E RID: 5182
		WellKnown,
		// Token: 0x0400143F RID: 5183
		OriginDhcp,
		// Token: 0x04001440 RID: 5184
		LinkLayerAddress,
		// Token: 0x04001441 RID: 5185
		Random
	}
}
