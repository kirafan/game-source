﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003BD RID: 957
	public enum TcpState
	{
		// Token: 0x04001446 RID: 5190
		Unknown,
		// Token: 0x04001447 RID: 5191
		Closed,
		// Token: 0x04001448 RID: 5192
		Listen,
		// Token: 0x04001449 RID: 5193
		SynSent,
		// Token: 0x0400144A RID: 5194
		SynReceived,
		// Token: 0x0400144B RID: 5195
		Established,
		// Token: 0x0400144C RID: 5196
		FinWait1,
		// Token: 0x0400144D RID: 5197
		FinWait2,
		// Token: 0x0400144E RID: 5198
		CloseWait,
		// Token: 0x0400144F RID: 5199
		Closing,
		// Token: 0x04001450 RID: 5200
		LastAck,
		// Token: 0x04001451 RID: 5201
		TimeWait,
		// Token: 0x04001452 RID: 5202
		DeleteTcb
	}
}
