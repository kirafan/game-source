﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200038D RID: 909
	public abstract class IPv6InterfaceProperties
	{
		// Token: 0x17000902 RID: 2306
		// (get) Token: 0x0600205E RID: 8286
		public abstract int Index { get; }

		// Token: 0x17000903 RID: 2307
		// (get) Token: 0x0600205F RID: 8287
		public abstract int Mtu { get; }
	}
}
