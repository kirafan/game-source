﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003B9 RID: 953
	public enum PrefixOrigin
	{
		// Token: 0x04001436 RID: 5174
		Other,
		// Token: 0x04001437 RID: 5175
		Manual,
		// Token: 0x04001438 RID: 5176
		WellKnown,
		// Token: 0x04001439 RID: 5177
		Dhcp,
		// Token: 0x0400143A RID: 5178
		RouterAdvertisement
	}
}
