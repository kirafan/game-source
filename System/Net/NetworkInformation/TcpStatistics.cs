﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003BE RID: 958
	public abstract class TcpStatistics
	{
		// Token: 0x1700094D RID: 2381
		// (get) Token: 0x0600212D RID: 8493
		public abstract long ConnectionsAccepted { get; }

		// Token: 0x1700094E RID: 2382
		// (get) Token: 0x0600212E RID: 8494
		public abstract long ConnectionsInitiated { get; }

		// Token: 0x1700094F RID: 2383
		// (get) Token: 0x0600212F RID: 8495
		public abstract long CumulativeConnections { get; }

		// Token: 0x17000950 RID: 2384
		// (get) Token: 0x06002130 RID: 8496
		public abstract long CurrentConnections { get; }

		// Token: 0x17000951 RID: 2385
		// (get) Token: 0x06002131 RID: 8497
		public abstract long ErrorsReceived { get; }

		// Token: 0x17000952 RID: 2386
		// (get) Token: 0x06002132 RID: 8498
		public abstract long FailedConnectionAttempts { get; }

		// Token: 0x17000953 RID: 2387
		// (get) Token: 0x06002133 RID: 8499
		public abstract long MaximumConnections { get; }

		// Token: 0x17000954 RID: 2388
		// (get) Token: 0x06002134 RID: 8500
		public abstract long MaximumTransmissionTimeout { get; }

		// Token: 0x17000955 RID: 2389
		// (get) Token: 0x06002135 RID: 8501
		public abstract long MinimumTransmissionTimeout { get; }

		// Token: 0x17000956 RID: 2390
		// (get) Token: 0x06002136 RID: 8502
		public abstract long ResetConnections { get; }

		// Token: 0x17000957 RID: 2391
		// (get) Token: 0x06002137 RID: 8503
		public abstract long ResetsSent { get; }

		// Token: 0x17000958 RID: 2392
		// (get) Token: 0x06002138 RID: 8504
		public abstract long SegmentsReceived { get; }

		// Token: 0x17000959 RID: 2393
		// (get) Token: 0x06002139 RID: 8505
		public abstract long SegmentsResent { get; }

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x0600213A RID: 8506
		public abstract long SegmentsSent { get; }
	}
}
