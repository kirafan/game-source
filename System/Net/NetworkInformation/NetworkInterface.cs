﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003A9 RID: 937
	public abstract class NetworkInterface
	{
		// Token: 0x060020A0 RID: 8352
		[DllImport("libc")]
		private static extern int uname(IntPtr buf);

		// Token: 0x060020A1 RID: 8353 RVA: 0x0005FFE8 File Offset: 0x0005E1E8
		[MonoTODO("Only works on Linux and Windows")]
		public static NetworkInterface[] GetAllNetworkInterfaces()
		{
			if (NetworkInterface.runningOnUnix)
			{
				bool flag = false;
				IntPtr intPtr = Marshal.AllocHGlobal(8192);
				if (NetworkInterface.uname(intPtr) == 0)
				{
					string a = Marshal.PtrToStringAnsi(intPtr);
					if (a == "Darwin")
					{
						flag = true;
					}
				}
				Marshal.FreeHGlobal(intPtr);
				try
				{
					if (flag)
					{
						return MacOsNetworkInterface.ImplGetAllNetworkInterfaces();
					}
					return LinuxNetworkInterface.ImplGetAllNetworkInterfaces();
				}
				catch (SystemException ex)
				{
					throw ex;
				}
				catch
				{
					return new NetworkInterface[0];
				}
			}
			if (Environment.OSVersion.Version >= NetworkInterface.windowsVer51)
			{
				return Win32NetworkInterface2.ImplGetAllNetworkInterfaces();
			}
			return new NetworkInterface[0];
		}

		// Token: 0x060020A2 RID: 8354 RVA: 0x000600D0 File Offset: 0x0005E2D0
		[MonoTODO("Always returns true")]
		public static bool GetIsNetworkAvailable()
		{
			return true;
		}

		// Token: 0x060020A3 RID: 8355 RVA: 0x000600D4 File Offset: 0x0005E2D4
		internal static string ReadLine(string path)
		{
			string result;
			using (FileStream fileStream = File.OpenRead(path))
			{
				using (StreamReader streamReader = new StreamReader(fileStream))
				{
					result = streamReader.ReadLine();
				}
			}
			return result;
		}

		// Token: 0x1700091D RID: 2333
		// (get) Token: 0x060020A4 RID: 8356 RVA: 0x00060154 File Offset: 0x0005E354
		[MonoTODO("Only works on Linux. Returns 0 on other systems.")]
		public static int LoopbackInterfaceIndex
		{
			get
			{
				if (NetworkInterface.runningOnUnix)
				{
					try
					{
						return UnixNetworkInterface.IfNameToIndex("lo");
					}
					catch
					{
						return 0;
					}
					return 0;
				}
				return 0;
			}
		}

		// Token: 0x060020A5 RID: 8357
		public abstract IPInterfaceProperties GetIPProperties();

		// Token: 0x060020A6 RID: 8358
		public abstract IPv4InterfaceStatistics GetIPv4Statistics();

		// Token: 0x060020A7 RID: 8359
		public abstract PhysicalAddress GetPhysicalAddress();

		// Token: 0x060020A8 RID: 8360
		public abstract bool Supports(NetworkInterfaceComponent networkInterfaceComponent);

		// Token: 0x1700091E RID: 2334
		// (get) Token: 0x060020A9 RID: 8361
		public abstract string Description { get; }

		// Token: 0x1700091F RID: 2335
		// (get) Token: 0x060020AA RID: 8362
		public abstract string Id { get; }

		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060020AB RID: 8363
		public abstract bool IsReceiveOnly { get; }

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060020AC RID: 8364
		public abstract string Name { get; }

		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060020AD RID: 8365
		public abstract NetworkInterfaceType NetworkInterfaceType { get; }

		// Token: 0x17000923 RID: 2339
		// (get) Token: 0x060020AE RID: 8366
		public abstract OperationalStatus OperationalStatus { get; }

		// Token: 0x17000924 RID: 2340
		// (get) Token: 0x060020AF RID: 8367
		public abstract long Speed { get; }

		// Token: 0x17000925 RID: 2341
		// (get) Token: 0x060020B0 RID: 8368
		public abstract bool SupportsMulticast { get; }

		// Token: 0x040013DE RID: 5086
		private static Version windowsVer51 = new Version(5, 1);

		// Token: 0x040013DF RID: 5087
		internal static readonly bool runningOnUnix = Environment.OSVersion.Platform == PlatformID.Unix;
	}
}
