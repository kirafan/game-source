﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200037D RID: 893
	public abstract class IPInterfaceProperties
	{
		// Token: 0x06001FDE RID: 8158
		public abstract IPv4InterfaceProperties GetIPv4Properties();

		// Token: 0x06001FDF RID: 8159
		public abstract IPv6InterfaceProperties GetIPv6Properties();

		// Token: 0x1700089D RID: 2205
		// (get) Token: 0x06001FE0 RID: 8160
		public abstract IPAddressInformationCollection AnycastAddresses { get; }

		// Token: 0x1700089E RID: 2206
		// (get) Token: 0x06001FE1 RID: 8161
		public abstract IPAddressCollection DhcpServerAddresses { get; }

		// Token: 0x1700089F RID: 2207
		// (get) Token: 0x06001FE2 RID: 8162
		public abstract IPAddressCollection DnsAddresses { get; }

		// Token: 0x170008A0 RID: 2208
		// (get) Token: 0x06001FE3 RID: 8163
		public abstract string DnsSuffix { get; }

		// Token: 0x170008A1 RID: 2209
		// (get) Token: 0x06001FE4 RID: 8164
		public abstract GatewayIPAddressInformationCollection GatewayAddresses { get; }

		// Token: 0x170008A2 RID: 2210
		// (get) Token: 0x06001FE5 RID: 8165
		public abstract bool IsDnsEnabled { get; }

		// Token: 0x170008A3 RID: 2211
		// (get) Token: 0x06001FE6 RID: 8166
		public abstract bool IsDynamicDnsEnabled { get; }

		// Token: 0x170008A4 RID: 2212
		// (get) Token: 0x06001FE7 RID: 8167
		public abstract MulticastIPAddressInformationCollection MulticastAddresses { get; }

		// Token: 0x170008A5 RID: 2213
		// (get) Token: 0x06001FE8 RID: 8168
		public abstract UnicastIPAddressInformationCollection UnicastAddresses { get; }

		// Token: 0x170008A6 RID: 2214
		// (get) Token: 0x06001FE9 RID: 8169
		public abstract IPAddressCollection WinsServersAddresses { get; }
	}
}
