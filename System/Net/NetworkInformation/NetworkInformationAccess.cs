﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003A4 RID: 932
	[Flags]
	public enum NetworkInformationAccess
	{
		// Token: 0x040013D3 RID: 5075
		None = 0,
		// Token: 0x040013D4 RID: 5076
		Read = 1,
		// Token: 0x040013D5 RID: 5077
		Ping = 4
	}
}
