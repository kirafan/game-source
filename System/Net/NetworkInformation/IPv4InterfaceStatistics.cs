﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000389 RID: 905
	public abstract class IPv4InterfaceStatistics
	{
		// Token: 0x170008D2 RID: 2258
		// (get) Token: 0x06002029 RID: 8233
		public abstract long BytesReceived { get; }

		// Token: 0x170008D3 RID: 2259
		// (get) Token: 0x0600202A RID: 8234
		public abstract long BytesSent { get; }

		// Token: 0x170008D4 RID: 2260
		// (get) Token: 0x0600202B RID: 8235
		public abstract long IncomingPacketsDiscarded { get; }

		// Token: 0x170008D5 RID: 2261
		// (get) Token: 0x0600202C RID: 8236
		public abstract long IncomingPacketsWithErrors { get; }

		// Token: 0x170008D6 RID: 2262
		// (get) Token: 0x0600202D RID: 8237
		public abstract long IncomingUnknownProtocolPackets { get; }

		// Token: 0x170008D7 RID: 2263
		// (get) Token: 0x0600202E RID: 8238
		public abstract long NonUnicastPacketsReceived { get; }

		// Token: 0x170008D8 RID: 2264
		// (get) Token: 0x0600202F RID: 8239
		public abstract long NonUnicastPacketsSent { get; }

		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x06002030 RID: 8240
		public abstract long OutgoingPacketsDiscarded { get; }

		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x06002031 RID: 8241
		public abstract long OutgoingPacketsWithErrors { get; }

		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x06002032 RID: 8242
		[MonoTODO("Not implemented for Linux")]
		public abstract long OutputQueueLength { get; }

		// Token: 0x170008DC RID: 2268
		// (get) Token: 0x06002033 RID: 8243
		public abstract long UnicastPacketsReceived { get; }

		// Token: 0x170008DD RID: 2269
		// (get) Token: 0x06002034 RID: 8244
		public abstract long UnicastPacketsSent { get; }
	}
}
