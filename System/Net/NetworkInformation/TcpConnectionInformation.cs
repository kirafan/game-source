﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003BB RID: 955
	public abstract class TcpConnectionInformation
	{
		// Token: 0x17000947 RID: 2375
		// (get) Token: 0x06002125 RID: 8485
		public abstract IPEndPoint LocalEndPoint { get; }

		// Token: 0x17000948 RID: 2376
		// (get) Token: 0x06002126 RID: 8486
		public abstract IPEndPoint RemoteEndPoint { get; }

		// Token: 0x17000949 RID: 2377
		// (get) Token: 0x06002127 RID: 8487
		public abstract TcpState State { get; }
	}
}
