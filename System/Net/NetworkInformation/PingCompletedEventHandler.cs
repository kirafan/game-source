﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x0200051A RID: 1306
	// (Invoke) Token: 0x06002D14 RID: 11540
	public delegate void PingCompletedEventHandler(object sender, PingCompletedEventArgs e);
}
