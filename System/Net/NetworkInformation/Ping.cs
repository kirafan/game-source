﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;

namespace System.Net.NetworkInformation
{
	// Token: 0x020003B2 RID: 946
	[MonoTODO("IPv6 support is missing")]
	public class Ping : System.ComponentModel.Component, IDisposable
	{
		// Token: 0x060020EF RID: 8431 RVA: 0x00061418 File Offset: 0x0005F618
		static Ping()
		{
			if (Environment.OSVersion.Platform == PlatformID.Unix)
			{
				Ping.CheckLinuxCapabilities();
				if (!Ping.canSendPrivileged && WindowsIdentity.GetCurrent().Name == "root")
				{
					Ping.canSendPrivileged = true;
				}
				foreach (string text in Ping.PingBinPaths)
				{
					if (File.Exists(text))
					{
						Ping.PingBinPath = text;
						break;
					}
				}
			}
			else
			{
				Ping.canSendPrivileged = true;
			}
			if (Ping.PingBinPath == null)
			{
				Ping.PingBinPath = "/bin/ping";
			}
		}

		// Token: 0x14000051 RID: 81
		// (add) Token: 0x060020F0 RID: 8432 RVA: 0x000614EC File Offset: 0x0005F6EC
		// (remove) Token: 0x060020F1 RID: 8433 RVA: 0x00061508 File Offset: 0x0005F708
		public event PingCompletedEventHandler PingCompleted;

		// Token: 0x060020F2 RID: 8434 RVA: 0x00061524 File Offset: 0x0005F724
		void IDisposable.Dispose()
		{
		}

		// Token: 0x060020F3 RID: 8435
		[DllImport("libc")]
		private static extern int capget(ref Ping.cap_user_header_t header, ref Ping.cap_user_data_t data);

		// Token: 0x060020F4 RID: 8436 RVA: 0x00061528 File Offset: 0x0005F728
		private static void CheckLinuxCapabilities()
		{
			try
			{
				Ping.cap_user_header_t cap_user_header_t = default(Ping.cap_user_header_t);
				Ping.cap_user_data_t cap_user_data_t = default(Ping.cap_user_data_t);
				cap_user_header_t.version = 537333798U;
				int num = -1;
				try
				{
					num = Ping.capget(ref cap_user_header_t, ref cap_user_data_t);
				}
				catch (Exception)
				{
				}
				if (num != -1)
				{
					Ping.canSendPrivileged = ((cap_user_data_t.effective & 8192U) != 0U);
				}
			}
			catch
			{
				Ping.canSendPrivileged = false;
			}
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x000615D4 File Offset: 0x0005F7D4
		protected void OnPingCompleted(PingCompletedEventArgs e)
		{
			if (this.PingCompleted != null)
			{
				this.PingCompleted(this, e);
			}
			this.user_async_state = null;
			this.worker = null;
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x00061608 File Offset: 0x0005F808
		public PingReply Send(IPAddress address)
		{
			return this.Send(address, 4000);
		}

		// Token: 0x060020F7 RID: 8439 RVA: 0x00061618 File Offset: 0x0005F818
		public PingReply Send(IPAddress address, int timeout)
		{
			return this.Send(address, timeout, Ping.default_buffer);
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x00061628 File Offset: 0x0005F828
		public PingReply Send(IPAddress address, int timeout, byte[] buffer)
		{
			return this.Send(address, timeout, buffer, new PingOptions());
		}

		// Token: 0x060020F9 RID: 8441 RVA: 0x00061638 File Offset: 0x0005F838
		public PingReply Send(string hostNameOrAddress)
		{
			return this.Send(hostNameOrAddress, 4000);
		}

		// Token: 0x060020FA RID: 8442 RVA: 0x00061648 File Offset: 0x0005F848
		public PingReply Send(string hostNameOrAddress, int timeout)
		{
			return this.Send(hostNameOrAddress, timeout, Ping.default_buffer);
		}

		// Token: 0x060020FB RID: 8443 RVA: 0x00061658 File Offset: 0x0005F858
		public PingReply Send(string hostNameOrAddress, int timeout, byte[] buffer)
		{
			return this.Send(hostNameOrAddress, timeout, buffer, new PingOptions());
		}

		// Token: 0x060020FC RID: 8444 RVA: 0x00061668 File Offset: 0x0005F868
		public PingReply Send(string hostNameOrAddress, int timeout, byte[] buffer, PingOptions options)
		{
			IPAddress[] hostAddresses = Dns.GetHostAddresses(hostNameOrAddress);
			return this.Send(hostAddresses[0], timeout, buffer, options);
		}

		// Token: 0x060020FD RID: 8445 RVA: 0x0006168C File Offset: 0x0005F88C
		private static IPAddress GetNonLoopbackIP()
		{
			foreach (IPAddress ipaddress in Dns.GetHostByName(Dns.GetHostName()).AddressList)
			{
				if (!IPAddress.IsLoopback(ipaddress))
				{
					return ipaddress;
				}
			}
			throw new InvalidOperationException("Could not resolve non-loopback IP address for localhost");
		}

		// Token: 0x060020FE RID: 8446 RVA: 0x000616D8 File Offset: 0x0005F8D8
		public PingReply Send(IPAddress address, int timeout, byte[] buffer, PingOptions options)
		{
			if (address == null)
			{
				throw new ArgumentNullException("address");
			}
			if (timeout < 0)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout must be non-negative integer");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (buffer.Length > 65500)
			{
				throw new ArgumentException("buffer");
			}
			if (Ping.canSendPrivileged)
			{
				return this.SendPrivileged(address, timeout, buffer, options);
			}
			return this.SendUnprivileged(address, timeout, buffer, options);
		}

		// Token: 0x060020FF RID: 8447 RVA: 0x00061758 File Offset: 0x0005F958
		private PingReply SendPrivileged(IPAddress address, int timeout, byte[] buffer, PingOptions options)
		{
			IPEndPoint ipendPoint = new IPEndPoint(address, 0);
			IPEndPoint ipendPoint2 = new IPEndPoint(Ping.GetNonLoopbackIP(), 0);
			PingReply result;
			using (System.Net.Sockets.Socket socket = new System.Net.Sockets.Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Raw, System.Net.Sockets.ProtocolType.Icmp))
			{
				if (options != null)
				{
					socket.DontFragment = options.DontFragment;
					socket.Ttl = (short)options.Ttl;
				}
				socket.SendTimeout = timeout;
				socket.ReceiveTimeout = timeout;
				Ping.IcmpMessage icmpMessage = new Ping.IcmpMessage(8, 0, 1, 0, buffer);
				byte[] array = icmpMessage.GetBytes();
				socket.SendBufferSize = array.Length;
				socket.SendTo(array, array.Length, System.Net.Sockets.SocketFlags.None, ipendPoint);
				DateTime now = DateTime.Now;
				array = new byte[100];
				int num;
				long num3;
				Ping.IcmpMessage icmpMessage2;
				for (;;)
				{
					EndPoint endPoint = ipendPoint2;
					num = 0;
					int num2 = socket.ReceiveFrom_nochecks_exc(array, 0, 100, System.Net.Sockets.SocketFlags.None, ref endPoint, false, out num);
					if (num != 0)
					{
						break;
					}
					num3 = (long)(DateTime.Now - now).TotalMilliseconds;
					int num4 = (int)(array[0] & 15) << 2;
					int size = num2 - num4;
					if (!((IPEndPoint)endPoint).Address.Equals(ipendPoint.Address))
					{
						long num5 = (long)timeout - num3;
						if (num5 <= 0L)
						{
							goto Block_7;
						}
						socket.ReceiveTimeout = (int)num5;
					}
					else
					{
						icmpMessage2 = new Ping.IcmpMessage(array, num4, size);
						if (icmpMessage2.Identifier == 1 && icmpMessage2.Type != 8)
						{
							goto IL_1C9;
						}
						long num6 = (long)timeout - num3;
						if (num6 <= 0L)
						{
							goto Block_9;
						}
						socket.ReceiveTimeout = (int)num6;
					}
				}
				if (num == 10060)
				{
					return new PingReply(null, new byte[0], options, 0L, IPStatus.TimedOut);
				}
				throw new NotSupportedException(string.Format("Unexpected socket error during ping request: {0}", num));
				Block_7:
				return new PingReply(null, new byte[0], options, 0L, IPStatus.TimedOut);
				Block_9:
				return new PingReply(null, new byte[0], options, 0L, IPStatus.TimedOut);
				IL_1C9:
				result = new PingReply(address, icmpMessage2.Data, options, num3, icmpMessage2.IPStatus);
			}
			return result;
		}

		// Token: 0x06002100 RID: 8448 RVA: 0x00061984 File Offset: 0x0005FB84
		private PingReply SendUnprivileged(IPAddress address, int timeout, byte[] buffer, PingOptions options)
		{
			DateTime now = DateTime.Now;
			System.Diagnostics.Process process = new System.Diagnostics.Process();
			string arguments = this.BuildPingArgs(address, timeout, options);
			long roundtripTime = 0L;
			process.StartInfo.FileName = Ping.PingBinPath;
			process.StartInfo.Arguments = arguments;
			process.StartInfo.CreateNoWindow = true;
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.RedirectStandardError = true;
			DateTime utcNow = DateTime.UtcNow;
			try
			{
				process.Start();
				roundtripTime = (long)(DateTime.Now - now).TotalMilliseconds;
				if (!process.WaitForExit(timeout) || (process.HasExited && process.ExitCode == 2))
				{
					return new PingReply(address, buffer, options, roundtripTime, IPStatus.TimedOut);
				}
				if (process.ExitCode == 1)
				{
					return new PingReply(address, buffer, options, roundtripTime, IPStatus.TtlExpired);
				}
			}
			catch (Exception)
			{
				return new PingReply(address, buffer, options, roundtripTime, IPStatus.Unknown);
			}
			finally
			{
				if (process != null)
				{
					if (!process.HasExited)
					{
						process.Kill();
					}
					process.Dispose();
				}
			}
			return new PingReply(address, buffer, options, roundtripTime, IPStatus.Success);
		}

		// Token: 0x06002101 RID: 8449 RVA: 0x00061AF4 File Offset: 0x0005FCF4
		public void SendAsync(IPAddress address, int timeout, byte[] buffer, object userToken)
		{
			this.SendAsync(address, 4000, Ping.default_buffer, new PingOptions(), userToken);
		}

		// Token: 0x06002102 RID: 8450 RVA: 0x00061B10 File Offset: 0x0005FD10
		public void SendAsync(IPAddress address, int timeout, object userToken)
		{
			this.SendAsync(address, 4000, Ping.default_buffer, userToken);
		}

		// Token: 0x06002103 RID: 8451 RVA: 0x00061B24 File Offset: 0x0005FD24
		public void SendAsync(IPAddress address, object userToken)
		{
			this.SendAsync(address, 4000, userToken);
		}

		// Token: 0x06002104 RID: 8452 RVA: 0x00061B34 File Offset: 0x0005FD34
		public void SendAsync(string hostNameOrAddress, int timeout, byte[] buffer, object userToken)
		{
			this.SendAsync(hostNameOrAddress, timeout, buffer, new PingOptions(), userToken);
		}

		// Token: 0x06002105 RID: 8453 RVA: 0x00061B48 File Offset: 0x0005FD48
		public void SendAsync(string hostNameOrAddress, int timeout, byte[] buffer, PingOptions options, object userToken)
		{
			IPAddress address = Dns.GetHostEntry(hostNameOrAddress).AddressList[0];
			this.SendAsync(address, timeout, buffer, options, userToken);
		}

		// Token: 0x06002106 RID: 8454 RVA: 0x00061B70 File Offset: 0x0005FD70
		public void SendAsync(string hostNameOrAddress, int timeout, object userToken)
		{
			this.SendAsync(hostNameOrAddress, timeout, Ping.default_buffer, userToken);
		}

		// Token: 0x06002107 RID: 8455 RVA: 0x00061B80 File Offset: 0x0005FD80
		public void SendAsync(string hostNameOrAddress, object userToken)
		{
			this.SendAsync(hostNameOrAddress, 4000, userToken);
		}

		// Token: 0x06002108 RID: 8456 RVA: 0x00061B90 File Offset: 0x0005FD90
		public void SendAsync(IPAddress address, int timeout, byte[] buffer, PingOptions options, object userToken)
		{
			if (this.worker != null)
			{
				throw new InvalidOperationException("Another SendAsync operation is in progress");
			}
			this.worker = new System.ComponentModel.BackgroundWorker();
			this.worker.DoWork += delegate(object o, System.ComponentModel.DoWorkEventArgs ea)
			{
				try
				{
					this.user_async_state = ea.Argument;
					ea.Result = this.Send(address, timeout, buffer, options);
				}
				catch (Exception result)
				{
					ea.Result = result;
				}
			};
			this.worker.WorkerSupportsCancellation = true;
			this.worker.RunWorkerCompleted += delegate(object o, System.ComponentModel.RunWorkerCompletedEventArgs ea)
			{
				this.OnPingCompleted(new PingCompletedEventArgs(ea.Error, ea.Cancelled, this.user_async_state, ea.Result as PingReply));
			};
			this.worker.RunWorkerAsync(userToken);
		}

		// Token: 0x06002109 RID: 8457 RVA: 0x00061C30 File Offset: 0x0005FE30
		public void SendAsyncCancel()
		{
			if (this.worker == null)
			{
				throw new InvalidOperationException("SendAsync operation is not in progress");
			}
			this.worker.CancelAsync();
		}

		// Token: 0x0600210A RID: 8458 RVA: 0x00061C54 File Offset: 0x0005FE54
		private string BuildPingArgs(IPAddress address, int timeout, PingOptions options)
		{
			CultureInfo invariantCulture = CultureInfo.InvariantCulture;
			StringBuilder stringBuilder = new StringBuilder();
			uint num = Convert.ToUInt32(Math.Floor((double)(timeout + 1000) / 1000.0));
			bool flag = Environment.OSVersion.Platform == PlatformID.MacOSX;
			if (!flag)
			{
				stringBuilder.AppendFormat(invariantCulture, "-q -n -c {0} -w {1} -t {2} -M ", new object[]
				{
					1,
					num,
					options.Ttl
				});
			}
			else
			{
				stringBuilder.AppendFormat(invariantCulture, "-q -n -c {0} -t {1} -o -m {2} ", new object[]
				{
					1,
					num,
					options.Ttl
				});
			}
			if (!flag)
			{
				stringBuilder.Append((!options.DontFragment) ? "dont " : "do ");
			}
			else if (options.DontFragment)
			{
				stringBuilder.Append("-D ");
			}
			stringBuilder.Append(address.ToString());
			return stringBuilder.ToString();
		}

		// Token: 0x0400141D RID: 5149
		private const int DefaultCount = 1;

		// Token: 0x0400141E RID: 5150
		private const int default_timeout = 4000;

		// Token: 0x0400141F RID: 5151
		private const int identifier = 1;

		// Token: 0x04001420 RID: 5152
		private const uint linux_cap_version = 537333798U;

		// Token: 0x04001421 RID: 5153
		private static readonly string[] PingBinPaths = new string[]
		{
			"/bin/ping",
			"/sbin/ping",
			"/usr/sbin/ping",
			"/system/bin/ping"
		};

		// Token: 0x04001422 RID: 5154
		private static readonly string PingBinPath;

		// Token: 0x04001423 RID: 5155
		private static readonly byte[] default_buffer = new byte[0];

		// Token: 0x04001424 RID: 5156
		private static bool canSendPrivileged;

		// Token: 0x04001425 RID: 5157
		private System.ComponentModel.BackgroundWorker worker;

		// Token: 0x04001426 RID: 5158
		private object user_async_state;

		// Token: 0x020003B3 RID: 947
		private struct cap_user_header_t
		{
			// Token: 0x04001428 RID: 5160
			public uint version;

			// Token: 0x04001429 RID: 5161
			public int pid;
		}

		// Token: 0x020003B4 RID: 948
		private struct cap_user_data_t
		{
			// Token: 0x0400142A RID: 5162
			public uint effective;

			// Token: 0x0400142B RID: 5163
			public uint permitted;

			// Token: 0x0400142C RID: 5164
			public uint inheritable;
		}

		// Token: 0x020003B5 RID: 949
		private class IcmpMessage
		{
			// Token: 0x0600210B RID: 8459 RVA: 0x00061D60 File Offset: 0x0005FF60
			public IcmpMessage(byte[] bytes, int offset, int size)
			{
				this.bytes = new byte[size];
				Buffer.BlockCopy(bytes, offset, this.bytes, 0, size);
			}

			// Token: 0x0600210C RID: 8460 RVA: 0x00061D84 File Offset: 0x0005FF84
			public IcmpMessage(byte type, byte code, short identifier, short sequence, byte[] data)
			{
				this.bytes = new byte[data.Length + 8];
				this.bytes[0] = type;
				this.bytes[1] = code;
				this.bytes[4] = (byte)(identifier & 255);
				this.bytes[5] = (byte)(identifier >> 8);
				this.bytes[6] = (byte)(sequence & 255);
				this.bytes[7] = (byte)(sequence >> 8);
				Buffer.BlockCopy(data, 0, this.bytes, 8, data.Length);
				ushort num = Ping.IcmpMessage.ComputeChecksum(this.bytes);
				this.bytes[2] = (byte)(num & 255);
				this.bytes[3] = (byte)(num >> 8);
			}

			// Token: 0x1700093A RID: 2362
			// (get) Token: 0x0600210D RID: 8461 RVA: 0x00061E30 File Offset: 0x00060030
			public byte Type
			{
				get
				{
					return this.bytes[0];
				}
			}

			// Token: 0x1700093B RID: 2363
			// (get) Token: 0x0600210E RID: 8462 RVA: 0x00061E3C File Offset: 0x0006003C
			public byte Code
			{
				get
				{
					return this.bytes[1];
				}
			}

			// Token: 0x1700093C RID: 2364
			// (get) Token: 0x0600210F RID: 8463 RVA: 0x00061E48 File Offset: 0x00060048
			public byte Identifier
			{
				get
				{
					return (byte)((int)this.bytes[4] + ((int)this.bytes[5] << 8));
				}
			}

			// Token: 0x1700093D RID: 2365
			// (get) Token: 0x06002110 RID: 8464 RVA: 0x00061E60 File Offset: 0x00060060
			public byte Sequence
			{
				get
				{
					return (byte)((int)this.bytes[6] + ((int)this.bytes[7] << 8));
				}
			}

			// Token: 0x1700093E RID: 2366
			// (get) Token: 0x06002111 RID: 8465 RVA: 0x00061E78 File Offset: 0x00060078
			public byte[] Data
			{
				get
				{
					byte[] array = new byte[this.bytes.Length - 8];
					Buffer.BlockCopy(this.bytes, 0, array, 0, array.Length);
					return array;
				}
			}

			// Token: 0x06002112 RID: 8466 RVA: 0x00061EA8 File Offset: 0x000600A8
			public byte[] GetBytes()
			{
				return this.bytes;
			}

			// Token: 0x06002113 RID: 8467 RVA: 0x00061EB0 File Offset: 0x000600B0
			private static ushort ComputeChecksum(byte[] data)
			{
				uint num = 0U;
				for (int i = 0; i < data.Length; i += 2)
				{
					ushort num2 = (ushort)((i + 1 >= data.Length) ? 0 : data[i + 1]);
					num2 = (ushort)(num2 << 8);
					num2 += (ushort)data[i];
					num += (uint)num2;
				}
				num = (num >> 16) + (num & 65535U);
				return (ushort)(~(ushort)num);
			}

			// Token: 0x1700093F RID: 2367
			// (get) Token: 0x06002114 RID: 8468 RVA: 0x00061F0C File Offset: 0x0006010C
			public IPStatus IPStatus
			{
				get
				{
					byte type = this.Type;
					switch (type)
					{
					case 0:
						return IPStatus.Success;
					default:
						switch (type)
						{
						case 8:
							return IPStatus.Success;
						case 11:
						{
							byte code = this.Code;
							if (code == 0)
							{
								return IPStatus.TimeExceeded;
							}
							if (code == 1)
							{
								return IPStatus.TtlReassemblyTimeExceeded;
							}
							break;
						}
						case 12:
							return IPStatus.ParameterProblem;
						}
						break;
					case 3:
						switch (this.Code)
						{
						case 0:
							return IPStatus.DestinationNetworkUnreachable;
						case 1:
							return IPStatus.DestinationHostUnreachable;
						case 2:
							return IPStatus.DestinationProhibited;
						case 3:
							return IPStatus.DestinationPortUnreachable;
						case 4:
							return IPStatus.BadOption;
						case 5:
							return IPStatus.BadRoute;
						}
						break;
					case 4:
						return IPStatus.SourceQuench;
					}
					return IPStatus.Unknown;
				}
			}

			// Token: 0x0400142D RID: 5165
			private byte[] bytes;
		}
	}
}
