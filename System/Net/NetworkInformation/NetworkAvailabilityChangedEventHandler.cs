﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000519 RID: 1305
	// (Invoke) Token: 0x06002D10 RID: 11536
	public delegate void NetworkAvailabilityChangedEventHandler(object sender, NetworkAvailabilityEventArgs e);
}
