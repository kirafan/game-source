﻿using System;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000382 RID: 898
	public enum IPStatus
	{
		// Token: 0x04001358 RID: 4952
		Unknown = -1,
		// Token: 0x04001359 RID: 4953
		Success,
		// Token: 0x0400135A RID: 4954
		DestinationNetworkUnreachable = 11002,
		// Token: 0x0400135B RID: 4955
		DestinationHostUnreachable,
		// Token: 0x0400135C RID: 4956
		DestinationProhibited,
		// Token: 0x0400135D RID: 4957
		DestinationProtocolUnreachable = 11004,
		// Token: 0x0400135E RID: 4958
		DestinationPortUnreachable,
		// Token: 0x0400135F RID: 4959
		NoResources,
		// Token: 0x04001360 RID: 4960
		BadOption,
		// Token: 0x04001361 RID: 4961
		HardwareError,
		// Token: 0x04001362 RID: 4962
		PacketTooBig,
		// Token: 0x04001363 RID: 4963
		TimedOut,
		// Token: 0x04001364 RID: 4964
		BadRoute = 11012,
		// Token: 0x04001365 RID: 4965
		TtlExpired,
		// Token: 0x04001366 RID: 4966
		TtlReassemblyTimeExceeded,
		// Token: 0x04001367 RID: 4967
		ParameterProblem,
		// Token: 0x04001368 RID: 4968
		SourceQuench,
		// Token: 0x04001369 RID: 4969
		BadDestination = 11018,
		// Token: 0x0400136A RID: 4970
		DestinationUnreachable = 11040,
		// Token: 0x0400136B RID: 4971
		TimeExceeded,
		// Token: 0x0400136C RID: 4972
		BadHeader,
		// Token: 0x0400136D RID: 4973
		UnrecognizedNextHeader,
		// Token: 0x0400136E RID: 4974
		IcmpError,
		// Token: 0x0400136F RID: 4975
		DestinationScopeMismatch
	}
}
