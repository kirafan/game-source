﻿using System;
using System.IO;

namespace System.Net.NetworkInformation
{
	// Token: 0x02000371 RID: 881
	public abstract class IPGlobalProperties
	{
		// Token: 0x06001F44 RID: 8004 RVA: 0x0005DC64 File Offset: 0x0005BE64
		public static IPGlobalProperties GetIPGlobalProperties()
		{
			PlatformID platform = Environment.OSVersion.Platform;
			if (platform != PlatformID.Unix)
			{
				return new Win32IPGlobalProperties();
			}
			if (Directory.Exists("/proc"))
			{
				MibIPGlobalProperties mibIPGlobalProperties = new MibIPGlobalProperties("/proc");
				if (File.Exists(mibIPGlobalProperties.StatisticsFile))
				{
					return mibIPGlobalProperties;
				}
			}
			if (Directory.Exists("/usr/compat/linux/proc"))
			{
				MibIPGlobalProperties mibIPGlobalProperties = new MibIPGlobalProperties("/usr/compat/linux/proc");
				if (File.Exists(mibIPGlobalProperties.StatisticsFile))
				{
					return mibIPGlobalProperties;
				}
			}
			throw new NotSupportedException("This platform is not supported");
		}

		// Token: 0x06001F45 RID: 8005
		public abstract TcpConnectionInformation[] GetActiveTcpConnections();

		// Token: 0x06001F46 RID: 8006
		public abstract IPEndPoint[] GetActiveTcpListeners();

		// Token: 0x06001F47 RID: 8007
		public abstract IPEndPoint[] GetActiveUdpListeners();

		// Token: 0x06001F48 RID: 8008
		public abstract IcmpV4Statistics GetIcmpV4Statistics();

		// Token: 0x06001F49 RID: 8009
		public abstract IcmpV6Statistics GetIcmpV6Statistics();

		// Token: 0x06001F4A RID: 8010
		public abstract IPGlobalStatistics GetIPv4GlobalStatistics();

		// Token: 0x06001F4B RID: 8011
		public abstract IPGlobalStatistics GetIPv6GlobalStatistics();

		// Token: 0x06001F4C RID: 8012
		public abstract TcpStatistics GetTcpIPv4Statistics();

		// Token: 0x06001F4D RID: 8013
		public abstract TcpStatistics GetTcpIPv6Statistics();

		// Token: 0x06001F4E RID: 8014
		public abstract UdpStatistics GetUdpIPv4Statistics();

		// Token: 0x06001F4F RID: 8015
		public abstract UdpStatistics GetUdpIPv6Statistics();

		// Token: 0x17000844 RID: 2116
		// (get) Token: 0x06001F50 RID: 8016
		public abstract string DhcpScopeName { get; }

		// Token: 0x17000845 RID: 2117
		// (get) Token: 0x06001F51 RID: 8017
		public abstract string DomainName { get; }

		// Token: 0x17000846 RID: 2118
		// (get) Token: 0x06001F52 RID: 8018
		public abstract string HostName { get; }

		// Token: 0x17000847 RID: 2119
		// (get) Token: 0x06001F53 RID: 8019
		public abstract bool IsWinsProxy { get; }

		// Token: 0x17000848 RID: 2120
		// (get) Token: 0x06001F54 RID: 8020
		public abstract NetBiosNodeType NodeType { get; }
	}
}
