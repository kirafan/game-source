﻿using System;

namespace System.Net
{
	// Token: 0x02000527 RID: 1319
	// (Invoke) Token: 0x06002D48 RID: 11592
	public delegate void OpenWriteCompletedEventHandler(object sender, OpenWriteCompletedEventArgs e);
}
