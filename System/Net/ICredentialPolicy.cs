﻿using System;

namespace System.Net
{
	// Token: 0x02000328 RID: 808
	public interface ICredentialPolicy
	{
		// Token: 0x06001CA1 RID: 7329
		bool ShouldSendCredential(System.Uri challengeUri, WebRequest request, NetworkCredential credential, IAuthenticationModule authenticationModule);
	}
}
