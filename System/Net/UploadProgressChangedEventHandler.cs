﻿using System;

namespace System.Net
{
	// Token: 0x02000523 RID: 1315
	// (Invoke) Token: 0x06002D38 RID: 11576
	public delegate void UploadProgressChangedEventHandler(object sender, UploadProgressChangedEventArgs e);
}
