﻿using System;

namespace System.Net
{
	// Token: 0x0200052A RID: 1322
	// (Invoke) Token: 0x06002D54 RID: 11604
	public delegate void UploadStringCompletedEventHandler(object sender, UploadStringCompletedEventArgs e);
}
