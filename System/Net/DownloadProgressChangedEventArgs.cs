﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C5 RID: 1221
	public class DownloadProgressChangedEventArgs : System.ComponentModel.ProgressChangedEventArgs
	{
		// Token: 0x06002BCB RID: 11211 RVA: 0x00098D1C File Offset: 0x00096F1C
		internal DownloadProgressChangedEventArgs(long bytesReceived, long totalBytesToReceive, object userState) : base((totalBytesToReceive == -1L) ? 0 : ((int)(bytesReceived * 100L / totalBytesToReceive)), userState)
		{
			this.received = bytesReceived;
			this.total = totalBytesToReceive;
		}

		// Token: 0x17000BFB RID: 3067
		// (get) Token: 0x06002BCC RID: 11212 RVA: 0x00098D4C File Offset: 0x00096F4C
		public long BytesReceived
		{
			get
			{
				return this.received;
			}
		}

		// Token: 0x17000BFC RID: 3068
		// (get) Token: 0x06002BCD RID: 11213 RVA: 0x00098D54 File Offset: 0x00096F54
		public long TotalBytesToReceive
		{
			get
			{
				return this.total;
			}
		}

		// Token: 0x04001B98 RID: 7064
		private long received;

		// Token: 0x04001B99 RID: 7065
		private long total;
	}
}
