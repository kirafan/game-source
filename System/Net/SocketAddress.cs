﻿using System;
using System.Net.Sockets;

namespace System.Net
{
	// Token: 0x020003E8 RID: 1000
	public class SocketAddress
	{
		// Token: 0x0600229A RID: 8858 RVA: 0x000652D0 File Offset: 0x000634D0
		public SocketAddress(System.Net.Sockets.AddressFamily family, int size)
		{
			if (size < 2)
			{
				throw new ArgumentOutOfRangeException("size is too small");
			}
			this.data = new byte[size];
			this.data[0] = (byte)family;
			this.data[1] = (byte)(family >> 8);
		}

		// Token: 0x0600229B RID: 8859 RVA: 0x00065318 File Offset: 0x00063518
		public SocketAddress(System.Net.Sockets.AddressFamily family) : this(family, 32)
		{
		}

		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x0600229C RID: 8860 RVA: 0x00065324 File Offset: 0x00063524
		public System.Net.Sockets.AddressFamily Family
		{
			get
			{
				return (System.Net.Sockets.AddressFamily)((int)this.data[0] + ((int)this.data[1] << 8));
			}
		}

		// Token: 0x17000A08 RID: 2568
		// (get) Token: 0x0600229D RID: 8861 RVA: 0x0006533C File Offset: 0x0006353C
		public int Size
		{
			get
			{
				return this.data.Length;
			}
		}

		// Token: 0x17000A09 RID: 2569
		public byte this[int offset]
		{
			get
			{
				return this.data[offset];
			}
			set
			{
				this.data[offset] = value;
			}
		}

		// Token: 0x060022A0 RID: 8864 RVA: 0x00065360 File Offset: 0x00063560
		public override string ToString()
		{
			string text = ((System.Net.Sockets.AddressFamily)this.data[0]).ToString();
			int num = this.data.Length;
			string text2 = string.Concat(new object[]
			{
				text,
				":",
				num,
				":{"
			});
			for (int i = 2; i < num; i++)
			{
				int num2 = (int)this.data[i];
				text2 += num2;
				if (i < num - 1)
				{
					text2 += ",";
				}
			}
			return text2 + "}";
		}

		// Token: 0x060022A1 RID: 8865 RVA: 0x00065400 File Offset: 0x00063600
		public override bool Equals(object obj)
		{
			SocketAddress socketAddress = obj as SocketAddress;
			if (socketAddress != null && socketAddress.data.Length == this.data.Length)
			{
				byte[] array = socketAddress.data;
				for (int i = 0; i < this.data.Length; i++)
				{
					if (array[i] != this.data[i])
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}

		// Token: 0x060022A2 RID: 8866 RVA: 0x00065464 File Offset: 0x00063664
		public override int GetHashCode()
		{
			int num = 0;
			for (int i = 0; i < this.data.Length; i++)
			{
				num += (int)this.data[i] + i;
			}
			return num;
		}

		// Token: 0x04001530 RID: 5424
		private byte[] data;
	}
}
