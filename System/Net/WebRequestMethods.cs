﻿using System;

namespace System.Net
{
	// Token: 0x02000420 RID: 1056
	public static class WebRequestMethods
	{
		// Token: 0x02000421 RID: 1057
		public static class File
		{
			// Token: 0x04001781 RID: 6017
			public const string DownloadFile = "GET";

			// Token: 0x04001782 RID: 6018
			public const string UploadFile = "PUT";
		}

		// Token: 0x02000422 RID: 1058
		public static class Ftp
		{
			// Token: 0x04001783 RID: 6019
			public const string AppendFile = "APPE";

			// Token: 0x04001784 RID: 6020
			public const string DeleteFile = "DELE";

			// Token: 0x04001785 RID: 6021
			public const string DownloadFile = "RETR";

			// Token: 0x04001786 RID: 6022
			public const string GetFileSize = "SIZE";

			// Token: 0x04001787 RID: 6023
			public const string GetDateTimestamp = "MDTM";

			// Token: 0x04001788 RID: 6024
			public const string ListDirectory = "NLST";

			// Token: 0x04001789 RID: 6025
			public const string ListDirectoryDetails = "LIST";

			// Token: 0x0400178A RID: 6026
			public const string MakeDirectory = "MKD";

			// Token: 0x0400178B RID: 6027
			public const string PrintWorkingDirectory = "PWD";

			// Token: 0x0400178C RID: 6028
			public const string RemoveDirectory = "RMD";

			// Token: 0x0400178D RID: 6029
			public const string Rename = "RENAME";

			// Token: 0x0400178E RID: 6030
			public const string UploadFile = "STOR";

			// Token: 0x0400178F RID: 6031
			public const string UploadFileWithUniqueName = "STOU";
		}

		// Token: 0x02000423 RID: 1059
		public static class Http
		{
			// Token: 0x04001790 RID: 6032
			public const string Connect = "CONNECT";

			// Token: 0x04001791 RID: 6033
			public const string Get = "GET";

			// Token: 0x04001792 RID: 6034
			public const string Head = "HEAD";

			// Token: 0x04001793 RID: 6035
			public const string MkCol = "MKCOL";

			// Token: 0x04001794 RID: 6036
			public const string Post = "POST";

			// Token: 0x04001795 RID: 6037
			public const string Put = "PUT";
		}
	}
}
