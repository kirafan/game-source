﻿using System;

namespace System.Net
{
	// Token: 0x0200052B RID: 1323
	// (Invoke) Token: 0x06002D58 RID: 11608
	public delegate void DownloadStringCompletedEventHandler(object sender, DownloadStringCompletedEventArgs e);
}
