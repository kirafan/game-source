﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x02000317 RID: 791
	[Serializable]
	public class HttpListenerException : System.ComponentModel.Win32Exception
	{
		// Token: 0x06001B95 RID: 7061 RVA: 0x0004EE20 File Offset: 0x0004D020
		public HttpListenerException()
		{
		}

		// Token: 0x06001B96 RID: 7062 RVA: 0x0004EE28 File Offset: 0x0004D028
		public HttpListenerException(int errorCode) : base(errorCode)
		{
		}

		// Token: 0x06001B97 RID: 7063 RVA: 0x0004EE34 File Offset: 0x0004D034
		public HttpListenerException(int errorCode, string message) : base(errorCode, message)
		{
		}

		// Token: 0x06001B98 RID: 7064 RVA: 0x0004EE40 File Offset: 0x0004D040
		protected HttpListenerException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}

		// Token: 0x170006B3 RID: 1715
		// (get) Token: 0x06001B99 RID: 7065 RVA: 0x0004EE4C File Offset: 0x0004D04C
		public override int ErrorCode
		{
			get
			{
				return base.ErrorCode;
			}
		}
	}
}
