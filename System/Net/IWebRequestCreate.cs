﻿using System;

namespace System.Net
{
	// Token: 0x02000330 RID: 816
	public interface IWebRequestCreate
	{
		// Token: 0x06001CF7 RID: 7415
		WebRequest Create(System.Uri uri);
	}
}
