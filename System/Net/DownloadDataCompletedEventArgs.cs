﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C4 RID: 1220
	public class DownloadDataCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BC9 RID: 11209 RVA: 0x00098D00 File Offset: 0x00096F00
		internal DownloadDataCompletedEventArgs(byte[] result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BFA RID: 3066
		// (get) Token: 0x06002BCA RID: 11210 RVA: 0x00098D14 File Offset: 0x00096F14
		public byte[] Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B97 RID: 7063
		private byte[] result;
	}
}
