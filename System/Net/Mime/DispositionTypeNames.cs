﻿using System;

namespace System.Net.Mime
{
	// Token: 0x02000350 RID: 848
	public static class DispositionTypeNames
	{
		// Token: 0x040012B5 RID: 4789
		public const string Attachment = "attachment";

		// Token: 0x040012B6 RID: 4790
		public const string Inline = "inline";
	}
}
