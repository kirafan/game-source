﻿using System;

namespace System.Net.Mime
{
	// Token: 0x02000351 RID: 849
	public static class MediaTypeNames
	{
		// Token: 0x02000352 RID: 850
		public static class Application
		{
			// Token: 0x040012B7 RID: 4791
			private const string prefix = "application/";

			// Token: 0x040012B8 RID: 4792
			public const string Octet = "application/octet-stream";

			// Token: 0x040012B9 RID: 4793
			public const string Pdf = "application/pdf";

			// Token: 0x040012BA RID: 4794
			public const string Rtf = "application/rtf";

			// Token: 0x040012BB RID: 4795
			public const string Soap = "application/soap+xml";

			// Token: 0x040012BC RID: 4796
			public const string Zip = "application/zip";
		}

		// Token: 0x02000353 RID: 851
		public static class Image
		{
			// Token: 0x040012BD RID: 4797
			private const string prefix = "image/";

			// Token: 0x040012BE RID: 4798
			public const string Gif = "image/gif";

			// Token: 0x040012BF RID: 4799
			public const string Jpeg = "image/jpeg";

			// Token: 0x040012C0 RID: 4800
			public const string Tiff = "image/tiff";
		}

		// Token: 0x02000354 RID: 852
		public static class Text
		{
			// Token: 0x040012C1 RID: 4801
			private const string prefix = "text/";

			// Token: 0x040012C2 RID: 4802
			public const string Html = "text/html";

			// Token: 0x040012C3 RID: 4803
			public const string Plain = "text/plain";

			// Token: 0x040012C4 RID: 4804
			public const string RichText = "text/richtext";

			// Token: 0x040012C5 RID: 4805
			public const string Xml = "text/xml";
		}
	}
}
