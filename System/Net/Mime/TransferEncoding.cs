﻿using System;

namespace System.Net.Mime
{
	// Token: 0x02000355 RID: 853
	public enum TransferEncoding
	{
		// Token: 0x040012C7 RID: 4807
		QuotedPrintable,
		// Token: 0x040012C8 RID: 4808
		Base64,
		// Token: 0x040012C9 RID: 4809
		SevenBit,
		// Token: 0x040012CA RID: 4810
		Unknown = -1
	}
}
