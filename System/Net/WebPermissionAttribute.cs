﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net
{
	// Token: 0x0200041A RID: 1050
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class WebPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x060025E4 RID: 9700 RVA: 0x00075FD4 File Offset: 0x000741D4
		public WebPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000AB4 RID: 2740
		// (get) Token: 0x060025E5 RID: 9701 RVA: 0x00075FE0 File Offset: 0x000741E0
		// (set) Token: 0x060025E6 RID: 9702 RVA: 0x00076000 File Offset: 0x00074200
		public string Accept
		{
			get
			{
				if (this.m_accept == null)
				{
					return null;
				}
				return (this.m_accept as WebPermissionInfo).Info;
			}
			set
			{
				if (this.m_accept != null)
				{
					this.AlreadySet("Accept", "Accept");
				}
				this.m_accept = new WebPermissionInfo(WebPermissionInfoType.InfoString, value);
			}
		}

		// Token: 0x17000AB5 RID: 2741
		// (get) Token: 0x060025E7 RID: 9703 RVA: 0x00076038 File Offset: 0x00074238
		// (set) Token: 0x060025E8 RID: 9704 RVA: 0x00076058 File Offset: 0x00074258
		public string AcceptPattern
		{
			get
			{
				if (this.m_accept == null)
				{
					return null;
				}
				return (this.m_accept as WebPermissionInfo).Info;
			}
			set
			{
				if (this.m_accept != null)
				{
					this.AlreadySet("Accept", "AcceptPattern");
				}
				if (value == null)
				{
					throw new ArgumentNullException("AcceptPattern");
				}
				this.m_accept = new WebPermissionInfo(WebPermissionInfoType.InfoUnexecutedRegex, value);
			}
		}

		// Token: 0x17000AB6 RID: 2742
		// (get) Token: 0x060025E9 RID: 9705 RVA: 0x00076094 File Offset: 0x00074294
		// (set) Token: 0x060025EA RID: 9706 RVA: 0x000760B4 File Offset: 0x000742B4
		public string Connect
		{
			get
			{
				if (this.m_connect == null)
				{
					return null;
				}
				return (this.m_connect as WebPermissionInfo).Info;
			}
			set
			{
				if (this.m_connect != null)
				{
					this.AlreadySet("Connect", "Connect");
				}
				this.m_connect = new WebPermissionInfo(WebPermissionInfoType.InfoString, value);
			}
		}

		// Token: 0x17000AB7 RID: 2743
		// (get) Token: 0x060025EB RID: 9707 RVA: 0x000760EC File Offset: 0x000742EC
		// (set) Token: 0x060025EC RID: 9708 RVA: 0x0007610C File Offset: 0x0007430C
		public string ConnectPattern
		{
			get
			{
				if (this.m_connect == null)
				{
					return null;
				}
				return (this.m_connect as WebPermissionInfo).Info;
			}
			set
			{
				if (this.m_connect != null)
				{
					this.AlreadySet("Connect", "ConnectConnectPattern");
				}
				if (value == null)
				{
					throw new ArgumentNullException("ConnectPattern");
				}
				this.m_connect = new WebPermissionInfo(WebPermissionInfoType.InfoUnexecutedRegex, value);
			}
		}

		// Token: 0x060025ED RID: 9709 RVA: 0x00076148 File Offset: 0x00074348
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new WebPermission(PermissionState.Unrestricted);
			}
			WebPermission webPermission = new WebPermission();
			if (this.m_accept != null)
			{
				webPermission.AddPermission(NetworkAccess.Accept, (WebPermissionInfo)this.m_accept);
			}
			if (this.m_connect != null)
			{
				webPermission.AddPermission(NetworkAccess.Connect, (WebPermissionInfo)this.m_connect);
			}
			return webPermission;
		}

		// Token: 0x060025EE RID: 9710 RVA: 0x000761B0 File Offset: 0x000743B0
		internal void AlreadySet(string parameter, string property)
		{
			string text = Locale.GetText("The parameter '{0}' can be set only once.");
			throw new ArgumentException(string.Format(text, parameter), property);
		}

		// Token: 0x0400176C RID: 5996
		private object m_accept;

		// Token: 0x0400176D RID: 5997
		private object m_connect;
	}
}
