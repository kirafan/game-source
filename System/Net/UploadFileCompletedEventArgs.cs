﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C1 RID: 1217
	public class UploadFileCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BC3 RID: 11203 RVA: 0x00098CAC File Offset: 0x00096EAC
		internal UploadFileCompletedEventArgs(byte[] result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BF7 RID: 3063
		// (get) Token: 0x06002BC4 RID: 11204 RVA: 0x00098CC0 File Offset: 0x00096EC0
		public byte[] Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B94 RID: 7060
		private byte[] result;
	}
}
