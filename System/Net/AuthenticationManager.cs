﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Net.Configuration;

namespace System.Net
{
	// Token: 0x020002B9 RID: 697
	public class AuthenticationManager
	{
		// Token: 0x06001823 RID: 6179 RVA: 0x000424F0 File Offset: 0x000406F0
		private AuthenticationManager()
		{
		}

		// Token: 0x06001825 RID: 6181 RVA: 0x0004250C File Offset: 0x0004070C
		private static void EnsureModules()
		{
			object obj = AuthenticationManager.locker;
			lock (obj)
			{
				if (AuthenticationManager.modules == null)
				{
					AuthenticationManager.modules = new ArrayList();
					object section = ConfigurationManager.GetSection("system.net/authenticationModules");
					System.Net.Configuration.AuthenticationModulesSection authenticationModulesSection = section as System.Net.Configuration.AuthenticationModulesSection;
					if (authenticationModulesSection != null)
					{
						foreach (object obj2 in authenticationModulesSection.AuthenticationModules)
						{
							System.Net.Configuration.AuthenticationModuleElement authenticationModuleElement = (System.Net.Configuration.AuthenticationModuleElement)obj2;
							IAuthenticationModule value = null;
							try
							{
								Type type = Type.GetType(authenticationModuleElement.Type, true);
								value = (IAuthenticationModule)Activator.CreateInstance(type);
							}
							catch
							{
							}
							AuthenticationManager.modules.Add(value);
						}
					}
				}
			}
		}

		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x06001826 RID: 6182 RVA: 0x0004262C File Offset: 0x0004082C
		// (set) Token: 0x06001827 RID: 6183 RVA: 0x00042634 File Offset: 0x00040834
		public static ICredentialPolicy CredentialPolicy
		{
			get
			{
				return AuthenticationManager.credential_policy;
			}
			set
			{
				AuthenticationManager.credential_policy = value;
			}
		}

		// Token: 0x06001828 RID: 6184 RVA: 0x0004263C File Offset: 0x0004083C
		private static Exception GetMustImplement()
		{
			return new NotImplementedException();
		}

		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x06001829 RID: 6185 RVA: 0x00042644 File Offset: 0x00040844
		[MonoTODO]
		public static System.Collections.Specialized.StringDictionary CustomTargetNameDictionary
		{
			get
			{
				throw AuthenticationManager.GetMustImplement();
			}
		}

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x0600182A RID: 6186 RVA: 0x0004264C File Offset: 0x0004084C
		public static IEnumerator RegisteredModules
		{
			get
			{
				AuthenticationManager.EnsureModules();
				return AuthenticationManager.modules.GetEnumerator();
			}
		}

		// Token: 0x0600182B RID: 6187 RVA: 0x00042660 File Offset: 0x00040860
		internal static void Clear()
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				AuthenticationManager.modules.Clear();
			}
		}

		// Token: 0x0600182C RID: 6188 RVA: 0x000426B0 File Offset: 0x000408B0
		public static Authorization Authenticate(string challenge, WebRequest request, ICredentials credentials)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (credentials == null)
			{
				throw new ArgumentNullException("credentials");
			}
			if (challenge == null)
			{
				throw new ArgumentNullException("challenge");
			}
			return AuthenticationManager.DoAuthenticate(challenge, request, credentials);
		}

		// Token: 0x0600182D RID: 6189 RVA: 0x000426F0 File Offset: 0x000408F0
		private static Authorization DoAuthenticate(string challenge, WebRequest request, ICredentials credentials)
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule = (IAuthenticationModule)obj2;
					Authorization authorization = authenticationModule.Authenticate(challenge, request, credentials);
					if (authorization != null)
					{
						authorization.Module = authenticationModule;
						return authorization;
					}
				}
			}
			return null;
		}

		// Token: 0x0600182E RID: 6190 RVA: 0x000427B8 File Offset: 0x000409B8
		public static Authorization PreAuthenticate(WebRequest request, ICredentials credentials)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (credentials == null)
			{
				return null;
			}
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule = (IAuthenticationModule)obj2;
					Authorization authorization = authenticationModule.PreAuthenticate(request, credentials);
					if (authorization != null)
					{
						authorization.Module = authenticationModule;
						return authorization;
					}
				}
			}
			return null;
		}

		// Token: 0x0600182F RID: 6191 RVA: 0x00042898 File Offset: 0x00040A98
		public static void Register(IAuthenticationModule authenticationModule)
		{
			if (authenticationModule == null)
			{
				throw new ArgumentNullException("authenticationModule");
			}
			AuthenticationManager.DoUnregister(authenticationModule.AuthenticationType, false);
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				AuthenticationManager.modules.Add(authenticationModule);
			}
		}

		// Token: 0x06001830 RID: 6192 RVA: 0x00042904 File Offset: 0x00040B04
		public static void Unregister(IAuthenticationModule authenticationModule)
		{
			if (authenticationModule == null)
			{
				throw new ArgumentNullException("authenticationModule");
			}
			AuthenticationManager.DoUnregister(authenticationModule.AuthenticationType, true);
		}

		// Token: 0x06001831 RID: 6193 RVA: 0x00042924 File Offset: 0x00040B24
		public static void Unregister(string authenticationScheme)
		{
			if (authenticationScheme == null)
			{
				throw new ArgumentNullException("authenticationScheme");
			}
			AuthenticationManager.DoUnregister(authenticationScheme, true);
		}

		// Token: 0x06001832 RID: 6194 RVA: 0x00042940 File Offset: 0x00040B40
		private static void DoUnregister(string authenticationScheme, bool throwEx)
		{
			AuthenticationManager.EnsureModules();
			ArrayList obj = AuthenticationManager.modules;
			lock (obj)
			{
				IAuthenticationModule authenticationModule = null;
				foreach (object obj2 in AuthenticationManager.modules)
				{
					IAuthenticationModule authenticationModule2 = (IAuthenticationModule)obj2;
					string authenticationType = authenticationModule2.AuthenticationType;
					if (string.Compare(authenticationType, authenticationScheme, true) == 0)
					{
						authenticationModule = authenticationModule2;
						break;
					}
				}
				if (authenticationModule == null)
				{
					if (throwEx)
					{
						throw new InvalidOperationException("Scheme not registered.");
					}
				}
				else
				{
					AuthenticationManager.modules.Remove(authenticationModule);
				}
			}
		}

		// Token: 0x04000F69 RID: 3945
		private static ArrayList modules;

		// Token: 0x04000F6A RID: 3946
		private static object locker = new object();

		// Token: 0x04000F6B RID: 3947
		private static ICredentialPolicy credential_policy = null;
	}
}
