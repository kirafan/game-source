﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Cache;
using System.Net.Configuration;
using System.Net.Security;
using System.Runtime.Serialization;
using System.Security.Principal;

namespace System.Net
{
	// Token: 0x0200041F RID: 1055
	[Serializable]
	public abstract class WebRequest : MarshalByRefObject, ISerializable
	{
		// Token: 0x06002624 RID: 9764 RVA: 0x00076E04 File Offset: 0x00075004
		protected WebRequest()
		{
		}

		// Token: 0x06002625 RID: 9765 RVA: 0x00076E14 File Offset: 0x00075014
		protected WebRequest(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
		}

		// Token: 0x06002626 RID: 9766 RVA: 0x00076E24 File Offset: 0x00075024
		static WebRequest()
		{
			object section = ConfigurationManager.GetSection("system.net/webRequestModules");
			System.Net.Configuration.WebRequestModulesSection webRequestModulesSection = section as System.Net.Configuration.WebRequestModulesSection;
			if (webRequestModulesSection != null)
			{
				foreach (object obj in webRequestModulesSection.WebRequestModules)
				{
					System.Net.Configuration.WebRequestModuleElement webRequestModuleElement = (System.Net.Configuration.WebRequestModuleElement)obj;
					WebRequest.AddPrefix(webRequestModuleElement.Prefix, webRequestModuleElement.Type);
				}
				return;
			}
			System.Configuration.ConfigurationSettings.GetConfig("system.net/webRequestModules");
		}

		// Token: 0x06002627 RID: 9767 RVA: 0x00076ED8 File Offset: 0x000750D8
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002628 RID: 9768 RVA: 0x00076EE0 File Offset: 0x000750E0
		private static void AddDynamicPrefix(string protocol, string implementor)
		{
			Type type = typeof(WebRequest).Assembly.GetType("System.Net." + implementor);
			if (type == null)
			{
				return;
			}
			WebRequest.AddPrefix(protocol, type);
		}

		// Token: 0x06002629 RID: 9769 RVA: 0x00076F1C File Offset: 0x0007511C
		private static Exception GetMustImplement()
		{
			return new NotImplementedException("This method must be implemented in derived classes");
		}

		// Token: 0x17000AC1 RID: 2753
		// (get) Token: 0x0600262A RID: 9770 RVA: 0x00076F28 File Offset: 0x00075128
		// (set) Token: 0x0600262B RID: 9771 RVA: 0x00076F30 File Offset: 0x00075130
		public System.Net.Security.AuthenticationLevel AuthenticationLevel
		{
			get
			{
				return this.authentication_level;
			}
			set
			{
				this.authentication_level = value;
			}
		}

		// Token: 0x17000AC2 RID: 2754
		// (get) Token: 0x0600262C RID: 9772 RVA: 0x00076F3C File Offset: 0x0007513C
		// (set) Token: 0x0600262D RID: 9773 RVA: 0x00076F44 File Offset: 0x00075144
		public virtual System.Net.Cache.RequestCachePolicy CachePolicy
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
			}
		}

		// Token: 0x17000AC3 RID: 2755
		// (get) Token: 0x0600262E RID: 9774 RVA: 0x00076F48 File Offset: 0x00075148
		// (set) Token: 0x0600262F RID: 9775 RVA: 0x00076F50 File Offset: 0x00075150
		public virtual string ConnectionGroupName
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC4 RID: 2756
		// (get) Token: 0x06002630 RID: 9776 RVA: 0x00076F58 File Offset: 0x00075158
		// (set) Token: 0x06002631 RID: 9777 RVA: 0x00076F60 File Offset: 0x00075160
		public virtual long ContentLength
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC5 RID: 2757
		// (get) Token: 0x06002632 RID: 9778 RVA: 0x00076F68 File Offset: 0x00075168
		// (set) Token: 0x06002633 RID: 9779 RVA: 0x00076F70 File Offset: 0x00075170
		public virtual string ContentType
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC6 RID: 2758
		// (get) Token: 0x06002634 RID: 9780 RVA: 0x00076F78 File Offset: 0x00075178
		// (set) Token: 0x06002635 RID: 9781 RVA: 0x00076F80 File Offset: 0x00075180
		public virtual ICredentials Credentials
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC7 RID: 2759
		// (get) Token: 0x06002636 RID: 9782 RVA: 0x00076F88 File Offset: 0x00075188
		// (set) Token: 0x06002637 RID: 9783 RVA: 0x00076F90 File Offset: 0x00075190
		public static System.Net.Cache.RequestCachePolicy DefaultCachePolicy
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC8 RID: 2760
		// (get) Token: 0x06002638 RID: 9784 RVA: 0x00076F98 File Offset: 0x00075198
		// (set) Token: 0x06002639 RID: 9785 RVA: 0x00076FA0 File Offset: 0x000751A0
		public virtual WebHeaderCollection Headers
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AC9 RID: 2761
		// (get) Token: 0x0600263A RID: 9786 RVA: 0x00076FA8 File Offset: 0x000751A8
		// (set) Token: 0x0600263B RID: 9787 RVA: 0x00076FB0 File Offset: 0x000751B0
		public TokenImpersonationLevel ImpersonationLevel
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACA RID: 2762
		// (get) Token: 0x0600263C RID: 9788 RVA: 0x00076FB8 File Offset: 0x000751B8
		// (set) Token: 0x0600263D RID: 9789 RVA: 0x00076FC0 File Offset: 0x000751C0
		public virtual string Method
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACB RID: 2763
		// (get) Token: 0x0600263E RID: 9790 RVA: 0x00076FC8 File Offset: 0x000751C8
		// (set) Token: 0x0600263F RID: 9791 RVA: 0x00076FD0 File Offset: 0x000751D0
		public virtual bool PreAuthenticate
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACC RID: 2764
		// (get) Token: 0x06002640 RID: 9792 RVA: 0x00076FD8 File Offset: 0x000751D8
		// (set) Token: 0x06002641 RID: 9793 RVA: 0x00076FE0 File Offset: 0x000751E0
		public virtual IWebProxy Proxy
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACD RID: 2765
		// (get) Token: 0x06002642 RID: 9794 RVA: 0x00076FE8 File Offset: 0x000751E8
		public virtual System.Uri RequestUri
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACE RID: 2766
		// (get) Token: 0x06002643 RID: 9795 RVA: 0x00076FF0 File Offset: 0x000751F0
		// (set) Token: 0x06002644 RID: 9796 RVA: 0x00076FF8 File Offset: 0x000751F8
		public virtual int Timeout
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000ACF RID: 2767
		// (get) Token: 0x06002645 RID: 9797 RVA: 0x00077000 File Offset: 0x00075200
		// (set) Token: 0x06002646 RID: 9798 RVA: 0x00077008 File Offset: 0x00075208
		public virtual bool UseDefaultCredentials
		{
			get
			{
				throw WebRequest.GetMustImplement();
			}
			set
			{
				throw WebRequest.GetMustImplement();
			}
		}

		// Token: 0x17000AD0 RID: 2768
		// (get) Token: 0x06002647 RID: 9799 RVA: 0x00077010 File Offset: 0x00075210
		// (set) Token: 0x06002648 RID: 9800 RVA: 0x00077074 File Offset: 0x00075274
		public static IWebProxy DefaultWebProxy
		{
			get
			{
				if (!WebRequest.isDefaultWebProxySet)
				{
					object obj = WebRequest.lockobj;
					lock (obj)
					{
						if (WebRequest.defaultWebProxy == null)
						{
							WebRequest.defaultWebProxy = WebRequest.GetDefaultWebProxy();
						}
					}
				}
				return WebRequest.defaultWebProxy;
			}
			set
			{
				WebRequest.defaultWebProxy = value;
				WebRequest.isDefaultWebProxySet = true;
			}
		}

		// Token: 0x06002649 RID: 9801 RVA: 0x00077084 File Offset: 0x00075284
		[MonoTODO("Needs to respect Module, Proxy.AutoDetect, and Proxy.ScriptLocation config settings")]
		private static IWebProxy GetDefaultWebProxy()
		{
			System.Net.Configuration.DefaultProxySection defaultProxySection = ConfigurationManager.GetSection("system.net/defaultProxy") as System.Net.Configuration.DefaultProxySection;
			if (defaultProxySection == null)
			{
				return WebRequest.GetSystemWebProxy();
			}
			System.Net.Configuration.ProxyElement proxy = defaultProxySection.Proxy;
			WebProxy webProxy;
			if (proxy.UseSystemDefault != System.Net.Configuration.ProxyElement.UseSystemDefaultValues.False && proxy.ProxyAddress == null)
			{
				webProxy = (WebProxy)WebRequest.GetSystemWebProxy();
			}
			else
			{
				webProxy = new WebProxy();
			}
			if (proxy.ProxyAddress != null)
			{
				webProxy.Address = proxy.ProxyAddress;
			}
			if (proxy.BypassOnLocal != System.Net.Configuration.ProxyElement.BypassOnLocalValues.Unspecified)
			{
				webProxy.BypassProxyOnLocal = (proxy.BypassOnLocal == System.Net.Configuration.ProxyElement.BypassOnLocalValues.True);
			}
			return webProxy;
		}

		// Token: 0x0600264A RID: 9802 RVA: 0x00077124 File Offset: 0x00075324
		public virtual void Abort()
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x0600264B RID: 9803 RVA: 0x0007712C File Offset: 0x0007532C
		public virtual IAsyncResult BeginGetRequestStream(AsyncCallback callback, object state)
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x0600264C RID: 9804 RVA: 0x00077134 File Offset: 0x00075334
		public virtual IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x0600264D RID: 9805 RVA: 0x0007713C File Offset: 0x0007533C
		public static WebRequest Create(string requestUriString)
		{
			if (requestUriString == null)
			{
				throw new ArgumentNullException("requestUriString");
			}
			return WebRequest.Create(new System.Uri(requestUriString));
		}

		// Token: 0x0600264E RID: 9806 RVA: 0x0007715C File Offset: 0x0007535C
		public static WebRequest Create(System.Uri requestUri)
		{
			if (requestUri == null)
			{
				throw new ArgumentNullException("requestUri");
			}
			return WebRequest.GetCreator(requestUri.AbsoluteUri).Create(requestUri);
		}

		// Token: 0x0600264F RID: 9807 RVA: 0x00077194 File Offset: 0x00075394
		public static WebRequest CreateDefault(System.Uri requestUri)
		{
			if (requestUri == null)
			{
				throw new ArgumentNullException("requestUri");
			}
			return WebRequest.GetCreator(requestUri.Scheme).Create(requestUri);
		}

		// Token: 0x06002650 RID: 9808 RVA: 0x000771CC File Offset: 0x000753CC
		public virtual Stream EndGetRequestStream(IAsyncResult asyncResult)
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x06002651 RID: 9809 RVA: 0x000771D4 File Offset: 0x000753D4
		public virtual WebResponse EndGetResponse(IAsyncResult asyncResult)
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x06002652 RID: 9810 RVA: 0x000771DC File Offset: 0x000753DC
		public virtual Stream GetRequestStream()
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x06002653 RID: 9811 RVA: 0x000771E4 File Offset: 0x000753E4
		public virtual WebResponse GetResponse()
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x06002654 RID: 9812 RVA: 0x000771EC File Offset: 0x000753EC
		[MonoTODO("Look in other places for proxy config info")]
		public static IWebProxy GetSystemWebProxy()
		{
			string text = Environment.GetEnvironmentVariable("http_proxy");
			if (text == null)
			{
				text = Environment.GetEnvironmentVariable("HTTP_PROXY");
			}
			if (text != null)
			{
				try
				{
					if (!text.StartsWith("http://"))
					{
						text = "http://" + text;
					}
					System.Uri uri = new System.Uri(text);
					IPAddress other;
					if (IPAddress.TryParse(uri.Host, out other))
					{
						if (IPAddress.Any.Equals(other))
						{
							uri = new System.UriBuilder(uri)
							{
								Host = "127.0.0.1"
							}.Uri;
						}
						else if (IPAddress.IPv6Any.Equals(other))
						{
							uri = new System.UriBuilder(uri)
							{
								Host = "[::1]"
							}.Uri;
						}
					}
					return new WebProxy(uri);
				}
				catch (System.UriFormatException)
				{
				}
			}
			return new WebProxy();
		}

		// Token: 0x06002655 RID: 9813 RVA: 0x000772E8 File Offset: 0x000754E8
		protected virtual void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			throw WebRequest.GetMustImplement();
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x000772F0 File Offset: 0x000754F0
		public static bool RegisterPrefix(string prefix, IWebRequestCreate creator)
		{
			if (prefix == null)
			{
				throw new ArgumentNullException("prefix");
			}
			if (creator == null)
			{
				throw new ArgumentNullException("creator");
			}
			object syncRoot = WebRequest.prefixes.SyncRoot;
			lock (syncRoot)
			{
				string key = prefix.ToLower(CultureInfo.InvariantCulture);
				if (WebRequest.prefixes.Contains(key))
				{
					return false;
				}
				WebRequest.prefixes.Add(key, creator);
			}
			return true;
		}

		// Token: 0x06002657 RID: 9815 RVA: 0x0007738C File Offset: 0x0007558C
		private static IWebRequestCreate GetCreator(string prefix)
		{
			int num = -1;
			IWebRequestCreate webRequestCreate = null;
			prefix = prefix.ToLower(CultureInfo.InvariantCulture);
			IDictionaryEnumerator enumerator = WebRequest.prefixes.GetEnumerator();
			while (enumerator.MoveNext())
			{
				string text = enumerator.Key as string;
				if (text.Length > num)
				{
					if (prefix.StartsWith(text))
					{
						num = text.Length;
						webRequestCreate = (IWebRequestCreate)enumerator.Value;
					}
				}
			}
			if (webRequestCreate == null)
			{
				throw new NotSupportedException(prefix);
			}
			return webRequestCreate;
		}

		// Token: 0x06002658 RID: 9816 RVA: 0x00077414 File Offset: 0x00075614
		internal static void ClearPrefixes()
		{
			WebRequest.prefixes.Clear();
		}

		// Token: 0x06002659 RID: 9817 RVA: 0x00077420 File Offset: 0x00075620
		internal static void RemovePrefix(string prefix)
		{
			WebRequest.prefixes.Remove(prefix);
		}

		// Token: 0x0600265A RID: 9818 RVA: 0x00077430 File Offset: 0x00075630
		internal static void AddPrefix(string prefix, string typeName)
		{
			Type type = Type.GetType(typeName);
			if (type == null)
			{
				throw new System.Configuration.ConfigurationException(string.Format("Type {0} not found", typeName));
			}
			WebRequest.AddPrefix(prefix, type);
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x00077464 File Offset: 0x00075664
		internal static void AddPrefix(string prefix, Type type)
		{
			object value = Activator.CreateInstance(type, true);
			WebRequest.prefixes[prefix] = value;
		}

		// Token: 0x0400177C RID: 6012
		private static System.Collections.Specialized.HybridDictionary prefixes = new System.Collections.Specialized.HybridDictionary();

		// Token: 0x0400177D RID: 6013
		private static bool isDefaultWebProxySet;

		// Token: 0x0400177E RID: 6014
		private static IWebProxy defaultWebProxy;

		// Token: 0x0400177F RID: 6015
		private System.Net.Security.AuthenticationLevel authentication_level = System.Net.Security.AuthenticationLevel.MutualAuthRequested;

		// Token: 0x04001780 RID: 6016
		private static readonly object lockobj = new object();
	}
}
