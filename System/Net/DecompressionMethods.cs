﻿using System;

namespace System.Net
{
	// Token: 0x020002F7 RID: 759
	[Flags]
	public enum DecompressionMethods
	{
		// Token: 0x04001037 RID: 4151
		None = 0,
		// Token: 0x04001038 RID: 4152
		GZip = 1,
		// Token: 0x04001039 RID: 4153
		Deflate = 2
	}
}
