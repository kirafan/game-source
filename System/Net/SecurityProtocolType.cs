﻿using System;

namespace System.Net
{
	// Token: 0x020003E1 RID: 993
	[Flags]
	public enum SecurityProtocolType
	{
		// Token: 0x04001501 RID: 5377
		Ssl3 = 48,
		// Token: 0x04001502 RID: 5378
		Tls = 192
	}
}
