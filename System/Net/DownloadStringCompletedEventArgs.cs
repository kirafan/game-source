﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C3 RID: 1219
	public class DownloadStringCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BC7 RID: 11207 RVA: 0x00098CE4 File Offset: 0x00096EE4
		internal DownloadStringCompletedEventArgs(string result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BF9 RID: 3065
		// (get) Token: 0x06002BC8 RID: 11208 RVA: 0x00098CF8 File Offset: 0x00096EF8
		public string Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B96 RID: 7062
		private string result;
	}
}
