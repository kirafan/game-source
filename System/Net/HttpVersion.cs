﻿using System;

namespace System.Net
{
	// Token: 0x02000321 RID: 801
	public class HttpVersion
	{
		// Token: 0x040011BD RID: 4541
		public static readonly Version Version10 = new Version(1, 0);

		// Token: 0x040011BE RID: 4542
		public static readonly Version Version11 = new Version(1, 1);
	}
}
