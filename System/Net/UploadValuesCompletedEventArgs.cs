﻿using System;
using System.ComponentModel;

namespace System.Net
{
	// Token: 0x020004C9 RID: 1225
	public class UploadValuesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BD7 RID: 11223 RVA: 0x00098DE0 File Offset: 0x00096FE0
		internal UploadValuesCompletedEventArgs(byte[] result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000C03 RID: 3075
		// (get) Token: 0x06002BD8 RID: 11224 RVA: 0x00098DF4 File Offset: 0x00096FF4
		public byte[] Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001BA0 RID: 7072
		private byte[] result;
	}
}
