﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Net.Mail
{
	// Token: 0x0200034B RID: 843
	[Serializable]
	public sealed class SmtpPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06001DF8 RID: 7672 RVA: 0x0005BAD4 File Offset: 0x00059CD4
		public SmtpPermission(bool unrestricted)
		{
			this.unrestricted = unrestricted;
			this.access = ((!unrestricted) ? SmtpAccess.None : SmtpAccess.ConnectToUnrestrictedPort);
		}

		// Token: 0x06001DF9 RID: 7673 RVA: 0x0005BB04 File Offset: 0x00059D04
		public SmtpPermission(PermissionState state)
		{
			this.unrestricted = (state == PermissionState.Unrestricted);
			this.access = ((!this.unrestricted) ? SmtpAccess.None : SmtpAccess.ConnectToUnrestrictedPort);
		}

		// Token: 0x06001DFA RID: 7674 RVA: 0x0005BB3C File Offset: 0x00059D3C
		public SmtpPermission(SmtpAccess access)
		{
			this.access = access;
		}

		// Token: 0x1700076E RID: 1902
		// (get) Token: 0x06001DFB RID: 7675 RVA: 0x0005BB4C File Offset: 0x00059D4C
		public SmtpAccess Access
		{
			get
			{
				return this.access;
			}
		}

		// Token: 0x06001DFC RID: 7676 RVA: 0x0005BB54 File Offset: 0x00059D54
		public void AddPermission(SmtpAccess access)
		{
			if (!this.unrestricted && access > this.access)
			{
				this.access = access;
			}
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x0005BB74 File Offset: 0x00059D74
		public override IPermission Copy()
		{
			if (this.unrestricted)
			{
				return new SmtpPermission(true);
			}
			return new SmtpPermission(this.access);
		}

		// Token: 0x06001DFE RID: 7678 RVA: 0x0005BB94 File Offset: 0x00059D94
		public override IPermission Intersect(IPermission target)
		{
			SmtpPermission smtpPermission = this.Cast(target);
			if (smtpPermission == null)
			{
				return null;
			}
			if (this.unrestricted && smtpPermission.unrestricted)
			{
				return new SmtpPermission(true);
			}
			if (this.access > smtpPermission.access)
			{
				return new SmtpPermission(smtpPermission.access);
			}
			return new SmtpPermission(this.access);
		}

		// Token: 0x06001DFF RID: 7679 RVA: 0x0005BBF8 File Offset: 0x00059DF8
		public override bool IsSubsetOf(IPermission target)
		{
			SmtpPermission smtpPermission = this.Cast(target);
			if (smtpPermission == null)
			{
				return this.IsEmpty();
			}
			if (this.unrestricted)
			{
				return smtpPermission.unrestricted;
			}
			return this.access <= smtpPermission.access;
		}

		// Token: 0x06001E00 RID: 7680 RVA: 0x0005BC40 File Offset: 0x00059E40
		public bool IsUnrestricted()
		{
			return this.unrestricted;
		}

		// Token: 0x06001E01 RID: 7681 RVA: 0x0005BC48 File Offset: 0x00059E48
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(typeof(SmtpPermission), 1);
			if (this.unrestricted)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				SmtpAccess smtpAccess = this.access;
				if (smtpAccess != SmtpAccess.Connect)
				{
					if (smtpAccess == SmtpAccess.ConnectToUnrestrictedPort)
					{
						securityElement.AddAttribute("Access", "ConnectToUnrestrictedPort");
					}
				}
				else
				{
					securityElement.AddAttribute("Access", "Connect");
				}
			}
			return securityElement;
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x0005BCCC File Offset: 0x00059ECC
		public override void FromXml(SecurityElement securityElement)
		{
			PermissionHelper.CheckSecurityElement(securityElement, "securityElement", 1, 1);
			if (securityElement.Tag != "IPermission")
			{
				throw new ArgumentException("securityElement");
			}
			if (PermissionHelper.IsUnrestricted(securityElement))
			{
				this.access = SmtpAccess.Connect;
			}
			else
			{
				this.access = SmtpAccess.None;
			}
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x0005BD28 File Offset: 0x00059F28
		public override IPermission Union(IPermission target)
		{
			SmtpPermission smtpPermission = this.Cast(target);
			if (smtpPermission == null)
			{
				return this.Copy();
			}
			if (this.unrestricted || smtpPermission.unrestricted)
			{
				return new SmtpPermission(true);
			}
			if (this.access > smtpPermission.access)
			{
				return new SmtpPermission(this.access);
			}
			return new SmtpPermission(smtpPermission.access);
		}

		// Token: 0x06001E04 RID: 7684 RVA: 0x0005BD90 File Offset: 0x00059F90
		private bool IsEmpty()
		{
			return !this.unrestricted && this.access == SmtpAccess.None;
		}

		// Token: 0x06001E05 RID: 7685 RVA: 0x0005BDAC File Offset: 0x00059FAC
		private SmtpPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			SmtpPermission smtpPermission = target as SmtpPermission;
			if (smtpPermission == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(SmtpPermission));
			}
			return smtpPermission;
		}

		// Token: 0x0400128F RID: 4751
		private const int version = 1;

		// Token: 0x04001290 RID: 4752
		private bool unrestricted;

		// Token: 0x04001291 RID: 4753
		private SmtpAccess access;
	}
}
