﻿using System;
using System.Runtime.Serialization;

namespace System.Net.Mail
{
	// Token: 0x0200034A RID: 842
	[Serializable]
	public class SmtpFailedRecipientsException : SmtpFailedRecipientException, ISerializable
	{
		// Token: 0x06001DF0 RID: 7664 RVA: 0x0005BA1C File Offset: 0x00059C1C
		public SmtpFailedRecipientsException()
		{
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x0005BA24 File Offset: 0x00059C24
		public SmtpFailedRecipientsException(string message) : base(message)
		{
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x0005BA30 File Offset: 0x00059C30
		public SmtpFailedRecipientsException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06001DF3 RID: 7667 RVA: 0x0005BA3C File Offset: 0x00059C3C
		public SmtpFailedRecipientsException(string message, SmtpFailedRecipientException[] innerExceptions) : base(message)
		{
			this.innerExceptions = innerExceptions;
		}

		// Token: 0x06001DF4 RID: 7668 RVA: 0x0005BA4C File Offset: 0x00059C4C
		protected SmtpFailedRecipientsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.innerExceptions = (SmtpFailedRecipientException[])info.GetValue("innerExceptions", typeof(SmtpFailedRecipientException[]));
		}

		// Token: 0x06001DF5 RID: 7669 RVA: 0x0005BA88 File Offset: 0x00059C88
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			this.GetObjectData(info, context);
		}

		// Token: 0x1700076D RID: 1901
		// (get) Token: 0x06001DF6 RID: 7670 RVA: 0x0005BA94 File Offset: 0x00059C94
		public SmtpFailedRecipientException[] InnerExceptions
		{
			get
			{
				return this.innerExceptions;
			}
		}

		// Token: 0x06001DF7 RID: 7671 RVA: 0x0005BA9C File Offset: 0x00059C9C
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("innerExceptions", this.innerExceptions);
		}

		// Token: 0x0400128E RID: 4750
		private SmtpFailedRecipientException[] innerExceptions;
	}
}
