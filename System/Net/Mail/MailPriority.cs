﻿using System;

namespace System.Net.Mail
{
	// Token: 0x0200033F RID: 831
	public enum MailPriority
	{
		// Token: 0x04001250 RID: 4688
		Normal,
		// Token: 0x04001251 RID: 4689
		Low,
		// Token: 0x04001252 RID: 4690
		High
	}
}
