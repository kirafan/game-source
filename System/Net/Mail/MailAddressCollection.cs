﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace System.Net.Mail
{
	// Token: 0x0200033C RID: 828
	public class MailAddressCollection : Collection<MailAddress>
	{
		// Token: 0x06001D5B RID: 7515 RVA: 0x00058F70 File Offset: 0x00057170
		public void Add(string addresses)
		{
			foreach (string address in addresses.Split(new char[]
			{
				','
			}))
			{
				this.Add(new MailAddress(address));
			}
		}

		// Token: 0x06001D5C RID: 7516 RVA: 0x00058FB4 File Offset: 0x000571B4
		protected override void InsertItem(int index, MailAddress item)
		{
			if (item == null)
			{
				throw new ArgumentNullException();
			}
			base.InsertItem(index, item);
		}

		// Token: 0x06001D5D RID: 7517 RVA: 0x00058FCC File Offset: 0x000571CC
		protected override void SetItem(int index, MailAddress item)
		{
			if (item == null)
			{
				throw new ArgumentNullException();
			}
			base.SetItem(index, item);
		}

		// Token: 0x06001D5E RID: 7518 RVA: 0x00058FE4 File Offset: 0x000571E4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.Count; i++)
			{
				if (i > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(this[i].ToString());
			}
			return stringBuilder.ToString();
		}
	}
}
