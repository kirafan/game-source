﻿using System;
using System.IO;
using System.Net.Mime;
using System.Text;

namespace System.Net.Mail
{
	// Token: 0x02000334 RID: 820
	public class AlternateView : AttachmentBase
	{
		// Token: 0x06001D14 RID: 7444 RVA: 0x000564BC File Offset: 0x000546BC
		public AlternateView(string fileName) : base(fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException();
			}
		}

		// Token: 0x06001D15 RID: 7445 RVA: 0x000564DC File Offset: 0x000546DC
		public AlternateView(string fileName, System.Net.Mime.ContentType contentType) : base(fileName, contentType)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException();
			}
		}

		// Token: 0x06001D16 RID: 7446 RVA: 0x00056500 File Offset: 0x00054700
		public AlternateView(string fileName, string mediaType) : base(fileName, mediaType)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException();
			}
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x00056524 File Offset: 0x00054724
		public AlternateView(Stream contentStream) : base(contentStream)
		{
		}

		// Token: 0x06001D18 RID: 7448 RVA: 0x00056538 File Offset: 0x00054738
		public AlternateView(Stream contentStream, string mediaType) : base(contentStream, mediaType)
		{
		}

		// Token: 0x06001D19 RID: 7449 RVA: 0x00056550 File Offset: 0x00054750
		public AlternateView(Stream contentStream, System.Net.Mime.ContentType contentType) : base(contentStream, contentType)
		{
		}

		// Token: 0x1700073E RID: 1854
		// (get) Token: 0x06001D1A RID: 7450 RVA: 0x00056568 File Offset: 0x00054768
		// (set) Token: 0x06001D1B RID: 7451 RVA: 0x00056570 File Offset: 0x00054770
		public System.Uri BaseUri
		{
			get
			{
				return this.baseUri;
			}
			set
			{
				this.baseUri = value;
			}
		}

		// Token: 0x1700073F RID: 1855
		// (get) Token: 0x06001D1C RID: 7452 RVA: 0x0005657C File Offset: 0x0005477C
		public LinkedResourceCollection LinkedResources
		{
			get
			{
				return this.linkedResources;
			}
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x00056584 File Offset: 0x00054784
		public static AlternateView CreateAlternateViewFromString(string content)
		{
			if (content == null)
			{
				throw new ArgumentNullException();
			}
			MemoryStream contentStream = new MemoryStream(Encoding.UTF8.GetBytes(content));
			return new AlternateView(contentStream)
			{
				TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable
			};
		}

		// Token: 0x06001D1E RID: 7454 RVA: 0x000565C0 File Offset: 0x000547C0
		public static AlternateView CreateAlternateViewFromString(string content, System.Net.Mime.ContentType contentType)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			Encoding encoding = (contentType.CharSet == null) ? Encoding.UTF8 : Encoding.GetEncoding(contentType.CharSet);
			MemoryStream contentStream = new MemoryStream(encoding.GetBytes(content));
			return new AlternateView(contentStream, contentType)
			{
				TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable
			};
		}

		// Token: 0x06001D1F RID: 7455 RVA: 0x0005661C File Offset: 0x0005481C
		public static AlternateView CreateAlternateViewFromString(string content, Encoding encoding, string mediaType)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			if (encoding == null)
			{
				encoding = Encoding.UTF8;
			}
			MemoryStream contentStream = new MemoryStream(encoding.GetBytes(content));
			return new AlternateView(contentStream, new System.Net.Mime.ContentType
			{
				MediaType = mediaType,
				CharSet = encoding.HeaderName
			})
			{
				TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable
			};
		}

		// Token: 0x06001D20 RID: 7456 RVA: 0x00056680 File Offset: 0x00054880
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (LinkedResource linkedResource in this.linkedResources)
				{
					linkedResource.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		// Token: 0x0400122C RID: 4652
		private System.Uri baseUri;

		// Token: 0x0400122D RID: 4653
		private LinkedResourceCollection linkedResources = new LinkedResourceCollection();
	}
}
