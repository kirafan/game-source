﻿using System;

namespace System.Net.Mail
{
	// Token: 0x02000347 RID: 839
	public enum SmtpDeliveryMethod
	{
		// Token: 0x04001289 RID: 4745
		Network,
		// Token: 0x0400128A RID: 4746
		SpecifiedPickupDirectory,
		// Token: 0x0400128B RID: 4747
		PickupDirectoryFromIis
	}
}
