﻿using System;
using System.Runtime.Serialization;

namespace System.Net.Mail
{
	// Token: 0x02000349 RID: 841
	[Serializable]
	public class SmtpFailedRecipientException : SmtpException, ISerializable
	{
		// Token: 0x06001DE6 RID: 7654 RVA: 0x0005B940 File Offset: 0x00059B40
		public SmtpFailedRecipientException()
		{
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x0005B948 File Offset: 0x00059B48
		public SmtpFailedRecipientException(string message) : base(message)
		{
		}

		// Token: 0x06001DE8 RID: 7656 RVA: 0x0005B954 File Offset: 0x00059B54
		protected SmtpFailedRecipientException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
			if (serializationInfo == null)
			{
				throw new ArgumentNullException("serializationInfo");
			}
			this.failedRecipient = serializationInfo.GetString("failedRecipient");
		}

		// Token: 0x06001DE9 RID: 7657 RVA: 0x0005B98C File Offset: 0x00059B8C
		public SmtpFailedRecipientException(SmtpStatusCode statusCode, string failedRecipient) : base(statusCode)
		{
			this.failedRecipient = failedRecipient;
		}

		// Token: 0x06001DEA RID: 7658 RVA: 0x0005B99C File Offset: 0x00059B9C
		public SmtpFailedRecipientException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x0005B9A8 File Offset: 0x00059BA8
		public SmtpFailedRecipientException(string message, string failedRecipient, Exception innerException) : base(message, innerException)
		{
			this.failedRecipient = failedRecipient;
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x0005B9BC File Offset: 0x00059BBC
		public SmtpFailedRecipientException(SmtpStatusCode statusCode, string failedRecipient, string serverResponse) : base(statusCode, serverResponse)
		{
			this.failedRecipient = failedRecipient;
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x0005B9D0 File Offset: 0x00059BD0
		void ISerializable.GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			this.GetObjectData(serializationInfo, streamingContext);
		}

		// Token: 0x1700076C RID: 1900
		// (get) Token: 0x06001DEE RID: 7662 RVA: 0x0005B9DC File Offset: 0x00059BDC
		public string FailedRecipient
		{
			get
			{
				return this.failedRecipient;
			}
		}

		// Token: 0x06001DEF RID: 7663 RVA: 0x0005B9E4 File Offset: 0x00059BE4
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			if (serializationInfo == null)
			{
				throw new ArgumentNullException("serializationInfo");
			}
			base.GetObjectData(serializationInfo, streamingContext);
			serializationInfo.AddValue("failedRecipient", this.failedRecipient);
		}

		// Token: 0x0400128D RID: 4749
		private string failedRecipient;
	}
}
