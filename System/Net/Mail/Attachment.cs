﻿using System;
using System.IO;
using System.Net.Mime;
using System.Text;

namespace System.Net.Mail
{
	// Token: 0x02000338 RID: 824
	public class Attachment : AttachmentBase
	{
		// Token: 0x06001D39 RID: 7481 RVA: 0x00058BAC File Offset: 0x00056DAC
		public Attachment(string fileName) : base(fileName)
		{
			this.InitName(fileName);
		}

		// Token: 0x06001D3A RID: 7482 RVA: 0x00058BC8 File Offset: 0x00056DC8
		public Attachment(string fileName, string mediaType) : base(fileName, mediaType)
		{
			this.InitName(fileName);
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x00058BE4 File Offset: 0x00056DE4
		public Attachment(string fileName, System.Net.Mime.ContentType contentType) : base(fileName, contentType)
		{
			this.InitName(fileName);
		}

		// Token: 0x06001D3C RID: 7484 RVA: 0x00058C00 File Offset: 0x00056E00
		public Attachment(Stream contentStream, System.Net.Mime.ContentType contentType) : base(contentStream, contentType)
		{
		}

		// Token: 0x06001D3D RID: 7485 RVA: 0x00058C18 File Offset: 0x00056E18
		public Attachment(Stream contentStream, string name) : base(contentStream)
		{
			this.Name = name;
		}

		// Token: 0x06001D3E RID: 7486 RVA: 0x00058C34 File Offset: 0x00056E34
		public Attachment(Stream contentStream, string name, string mediaType) : base(contentStream, mediaType)
		{
			this.Name = name;
		}

		// Token: 0x17000744 RID: 1860
		// (get) Token: 0x06001D3F RID: 7487 RVA: 0x00058C50 File Offset: 0x00056E50
		public System.Net.Mime.ContentDisposition ContentDisposition
		{
			get
			{
				return this.contentDisposition;
			}
		}

		// Token: 0x17000745 RID: 1861
		// (get) Token: 0x06001D40 RID: 7488 RVA: 0x00058C58 File Offset: 0x00056E58
		// (set) Token: 0x06001D41 RID: 7489 RVA: 0x00058C68 File Offset: 0x00056E68
		public string Name
		{
			get
			{
				return base.ContentType.Name;
			}
			set
			{
				base.ContentType.Name = value;
			}
		}

		// Token: 0x17000746 RID: 1862
		// (get) Token: 0x06001D42 RID: 7490 RVA: 0x00058C78 File Offset: 0x00056E78
		// (set) Token: 0x06001D43 RID: 7491 RVA: 0x00058C80 File Offset: 0x00056E80
		public Encoding NameEncoding
		{
			get
			{
				return this.nameEncoding;
			}
			set
			{
				this.nameEncoding = value;
			}
		}

		// Token: 0x06001D44 RID: 7492 RVA: 0x00058C8C File Offset: 0x00056E8C
		public static Attachment CreateAttachmentFromString(string content, System.Net.Mime.ContentType contentType)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			MemoryStream memoryStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memoryStream);
			streamWriter.Write(content);
			streamWriter.Flush();
			memoryStream.Position = 0L;
			return new Attachment(memoryStream, contentType)
			{
				TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable
			};
		}

		// Token: 0x06001D45 RID: 7493 RVA: 0x00058CDC File Offset: 0x00056EDC
		public static Attachment CreateAttachmentFromString(string content, string name)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			MemoryStream memoryStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memoryStream);
			streamWriter.Write(content);
			streamWriter.Flush();
			memoryStream.Position = 0L;
			return new Attachment(memoryStream, new System.Net.Mime.ContentType("text/plain"))
			{
				TransferEncoding = System.Net.Mime.TransferEncoding.QuotedPrintable,
				Name = name
			};
		}

		// Token: 0x06001D46 RID: 7494 RVA: 0x00058D3C File Offset: 0x00056F3C
		public static Attachment CreateAttachmentFromString(string content, string name, Encoding contentEncoding, string mediaType)
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			MemoryStream memoryStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memoryStream, contentEncoding);
			streamWriter.Write(content);
			streamWriter.Flush();
			memoryStream.Position = 0L;
			return new Attachment(memoryStream, name, mediaType)
			{
				TransferEncoding = System.Net.Mime.ContentType.GuessTransferEncoding(contentEncoding),
				ContentType = 
				{
					CharSet = streamWriter.Encoding.BodyName
				}
			};
		}

		// Token: 0x06001D47 RID: 7495 RVA: 0x00058DAC File Offset: 0x00056FAC
		private void InitName(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			this.Name = Path.GetFileName(fileName);
		}

		// Token: 0x04001233 RID: 4659
		private System.Net.Mime.ContentDisposition contentDisposition = new System.Net.Mime.ContentDisposition();

		// Token: 0x04001234 RID: 4660
		private Encoding nameEncoding;
	}
}
