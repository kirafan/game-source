﻿using System;

namespace System.Net.Mail
{
	// Token: 0x02000340 RID: 832
	public enum SmtpAccess
	{
		// Token: 0x04001254 RID: 4692
		None,
		// Token: 0x04001255 RID: 4693
		Connect,
		// Token: 0x04001256 RID: 4694
		ConnectToUnrestrictedPort
	}
}
