﻿using System;
using System.ComponentModel;

namespace System.Net.Mail
{
	// Token: 0x02000517 RID: 1303
	// (Invoke) Token: 0x06002D08 RID: 11528
	public delegate void SendCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
}
