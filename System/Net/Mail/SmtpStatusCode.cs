﻿using System;

namespace System.Net.Mail
{
	// Token: 0x0200034D RID: 845
	public enum SmtpStatusCode
	{
		// Token: 0x04001295 RID: 4757
		BadCommandSequence = 503,
		// Token: 0x04001296 RID: 4758
		CannotVerifyUserWillAttemptDelivery = 252,
		// Token: 0x04001297 RID: 4759
		ClientNotPermitted = 454,
		// Token: 0x04001298 RID: 4760
		CommandNotImplemented = 502,
		// Token: 0x04001299 RID: 4761
		CommandParameterNotImplemented = 504,
		// Token: 0x0400129A RID: 4762
		CommandUnrecognized = 500,
		// Token: 0x0400129B RID: 4763
		ExceededStorageAllocation = 552,
		// Token: 0x0400129C RID: 4764
		GeneralFailure = -1,
		// Token: 0x0400129D RID: 4765
		HelpMessage = 214,
		// Token: 0x0400129E RID: 4766
		InsufficientStorage = 452,
		// Token: 0x0400129F RID: 4767
		LocalErrorInProcessing = 451,
		// Token: 0x040012A0 RID: 4768
		MailboxBusy = 450,
		// Token: 0x040012A1 RID: 4769
		MailboxNameNotAllowed = 553,
		// Token: 0x040012A2 RID: 4770
		MailboxUnavailable = 550,
		// Token: 0x040012A3 RID: 4771
		Ok = 250,
		// Token: 0x040012A4 RID: 4772
		ServiceClosingTransmissionChannel = 221,
		// Token: 0x040012A5 RID: 4773
		ServiceNotAvailable = 421,
		// Token: 0x040012A6 RID: 4774
		ServiceReady = 220,
		// Token: 0x040012A7 RID: 4775
		StartMailInput = 354,
		// Token: 0x040012A8 RID: 4776
		SyntaxError = 501,
		// Token: 0x040012A9 RID: 4777
		SystemStatus = 211,
		// Token: 0x040012AA RID: 4778
		TransactionFailed = 554,
		// Token: 0x040012AB RID: 4779
		UserNotLocalTryAlternatePath = 551,
		// Token: 0x040012AC RID: 4780
		UserNotLocalWillForward = 251,
		// Token: 0x040012AD RID: 4781
		MustIssueStartTlsFirst = 530
	}
}
