﻿using System;

namespace System.Net.Mail
{
	// Token: 0x02000339 RID: 825
	[Flags]
	public enum DeliveryNotificationOptions
	{
		// Token: 0x04001236 RID: 4662
		None = 0,
		// Token: 0x04001237 RID: 4663
		OnSuccess = 1,
		// Token: 0x04001238 RID: 4664
		OnFailure = 2,
		// Token: 0x04001239 RID: 4665
		Delay = 4,
		// Token: 0x0400123A RID: 4666
		Never = 134217728
	}
}
