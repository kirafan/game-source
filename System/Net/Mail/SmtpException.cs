﻿using System;
using System.Runtime.Serialization;

namespace System.Net.Mail
{
	// Token: 0x02000348 RID: 840
	[Serializable]
	public class SmtpException : Exception, ISerializable
	{
		// Token: 0x06001DDC RID: 7644 RVA: 0x0005B81C File Offset: 0x00059A1C
		public SmtpException() : this(SmtpStatusCode.GeneralFailure)
		{
		}

		// Token: 0x06001DDD RID: 7645 RVA: 0x0005B828 File Offset: 0x00059A28
		public SmtpException(SmtpStatusCode statusCode) : this(statusCode, "Syntax error, command unrecognized.")
		{
		}

		// Token: 0x06001DDE RID: 7646 RVA: 0x0005B838 File Offset: 0x00059A38
		public SmtpException(string message) : this(SmtpStatusCode.GeneralFailure, message)
		{
		}

		// Token: 0x06001DDF RID: 7647 RVA: 0x0005B844 File Offset: 0x00059A44
		protected SmtpException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			try
			{
				this.statusCode = (SmtpStatusCode)((int)info.GetValue("Status", typeof(int)));
			}
			catch (SerializationException)
			{
				this.statusCode = (SmtpStatusCode)((int)info.GetValue("statusCode", typeof(SmtpStatusCode)));
			}
		}

		// Token: 0x06001DE0 RID: 7648 RVA: 0x0005B8C0 File Offset: 0x00059AC0
		public SmtpException(SmtpStatusCode statusCode, string message) : base(message)
		{
			this.statusCode = statusCode;
		}

		// Token: 0x06001DE1 RID: 7649 RVA: 0x0005B8D0 File Offset: 0x00059AD0
		public SmtpException(string message, Exception innerException) : base(message, innerException)
		{
			this.statusCode = SmtpStatusCode.GeneralFailure;
		}

		// Token: 0x06001DE2 RID: 7650 RVA: 0x0005B8E4 File Offset: 0x00059AE4
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			this.GetObjectData(info, context);
		}

		// Token: 0x1700076B RID: 1899
		// (get) Token: 0x06001DE3 RID: 7651 RVA: 0x0005B8F0 File Offset: 0x00059AF0
		// (set) Token: 0x06001DE4 RID: 7652 RVA: 0x0005B8F8 File Offset: 0x00059AF8
		public SmtpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
			set
			{
				this.statusCode = value;
			}
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x0005B904 File Offset: 0x00059B04
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("Status", this.statusCode, typeof(int));
		}

		// Token: 0x0400128C RID: 4748
		private SmtpStatusCode statusCode;
	}
}
