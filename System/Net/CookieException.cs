﻿using System;
using System.Runtime.Serialization;

namespace System.Net
{
	// Token: 0x020002F3 RID: 755
	[Serializable]
	public class CookieException : FormatException, ISerializable
	{
		// Token: 0x060019E4 RID: 6628 RVA: 0x00047B34 File Offset: 0x00045D34
		public CookieException()
		{
		}

		// Token: 0x060019E5 RID: 6629 RVA: 0x00047B3C File Offset: 0x00045D3C
		internal CookieException(string msg) : base(msg)
		{
		}

		// Token: 0x060019E6 RID: 6630 RVA: 0x00047B48 File Offset: 0x00045D48
		internal CookieException(string msg, Exception e) : base(msg, e)
		{
		}

		// Token: 0x060019E7 RID: 6631 RVA: 0x00047B54 File Offset: 0x00045D54
		protected CookieException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060019E8 RID: 6632 RVA: 0x00047B60 File Offset: 0x00045D60
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		// Token: 0x060019E9 RID: 6633 RVA: 0x00047B6C File Offset: 0x00045D6C
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
			base.GetObjectData(serializationInfo, streamingContext);
		}
	}
}
