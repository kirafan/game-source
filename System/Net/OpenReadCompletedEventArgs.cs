﻿using System;
using System.ComponentModel;
using System.IO;

namespace System.Net
{
	// Token: 0x020004C2 RID: 1218
	public class OpenReadCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs
	{
		// Token: 0x06002BC5 RID: 11205 RVA: 0x00098CC8 File Offset: 0x00096EC8
		internal OpenReadCompletedEventArgs(Stream result, Exception error, bool cancelled, object userState) : base(error, cancelled, userState)
		{
			this.result = result;
		}

		// Token: 0x17000BF8 RID: 3064
		// (get) Token: 0x06002BC6 RID: 11206 RVA: 0x00098CDC File Offset: 0x00096EDC
		public Stream Result
		{
			get
			{
				return this.result;
			}
		}

		// Token: 0x04001B95 RID: 7061
		private Stream result;
	}
}
