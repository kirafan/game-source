﻿using System;

namespace System.Net.Security
{
	// Token: 0x020003E0 RID: 992
	public enum ProtectionLevel
	{
		// Token: 0x040014FD RID: 5373
		None,
		// Token: 0x040014FE RID: 5374
		Sign,
		// Token: 0x040014FF RID: 5375
		EncryptAndSign
	}
}
