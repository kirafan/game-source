﻿using System;
using System.IO;
using System.Security.Principal;

namespace System.Net.Security
{
	// Token: 0x020003DF RID: 991
	public class NegotiateStream : AuthenticatedStream
	{
		// Token: 0x060021E3 RID: 8675 RVA: 0x000632F8 File Offset: 0x000614F8
		[MonoTODO]
		public NegotiateStream(Stream innerStream) : base(innerStream, false)
		{
		}

		// Token: 0x060021E4 RID: 8676 RVA: 0x00063304 File Offset: 0x00061504
		[MonoTODO]
		public NegotiateStream(Stream innerStream, bool leaveStreamOpen) : base(innerStream, leaveStreamOpen)
		{
		}

		// Token: 0x170009BE RID: 2494
		// (get) Token: 0x060021E5 RID: 8677 RVA: 0x00063310 File Offset: 0x00061510
		public override bool CanRead
		{
			get
			{
				return base.InnerStream.CanRead;
			}
		}

		// Token: 0x170009BF RID: 2495
		// (get) Token: 0x060021E6 RID: 8678 RVA: 0x00063320 File Offset: 0x00061520
		public override bool CanSeek
		{
			get
			{
				return base.InnerStream.CanSeek;
			}
		}

		// Token: 0x170009C0 RID: 2496
		// (get) Token: 0x060021E7 RID: 8679 RVA: 0x00063330 File Offset: 0x00061530
		[MonoTODO]
		public override bool CanTimeout
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C1 RID: 2497
		// (get) Token: 0x060021E8 RID: 8680 RVA: 0x00063338 File Offset: 0x00061538
		public override bool CanWrite
		{
			get
			{
				return base.InnerStream.CanWrite;
			}
		}

		// Token: 0x170009C2 RID: 2498
		// (get) Token: 0x060021E9 RID: 8681 RVA: 0x00063348 File Offset: 0x00061548
		[MonoTODO]
		public virtual TokenImpersonationLevel ImpersonationLevel
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C3 RID: 2499
		// (get) Token: 0x060021EA RID: 8682 RVA: 0x00063350 File Offset: 0x00061550
		[MonoTODO]
		public override bool IsAuthenticated
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C4 RID: 2500
		// (get) Token: 0x060021EB RID: 8683 RVA: 0x00063358 File Offset: 0x00061558
		[MonoTODO]
		public override bool IsEncrypted
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C5 RID: 2501
		// (get) Token: 0x060021EC RID: 8684 RVA: 0x00063360 File Offset: 0x00061560
		[MonoTODO]
		public override bool IsMutuallyAuthenticated
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C6 RID: 2502
		// (get) Token: 0x060021ED RID: 8685 RVA: 0x00063368 File Offset: 0x00061568
		[MonoTODO]
		public override bool IsServer
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C7 RID: 2503
		// (get) Token: 0x060021EE RID: 8686 RVA: 0x00063370 File Offset: 0x00061570
		[MonoTODO]
		public override bool IsSigned
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009C8 RID: 2504
		// (get) Token: 0x060021EF RID: 8687 RVA: 0x00063378 File Offset: 0x00061578
		public override long Length
		{
			get
			{
				return base.InnerStream.Length;
			}
		}

		// Token: 0x170009C9 RID: 2505
		// (get) Token: 0x060021F0 RID: 8688 RVA: 0x00063388 File Offset: 0x00061588
		// (set) Token: 0x060021F1 RID: 8689 RVA: 0x00063398 File Offset: 0x00061598
		public override long Position
		{
			get
			{
				return base.InnerStream.Position;
			}
			set
			{
				base.InnerStream.Position = value;
			}
		}

		// Token: 0x170009CA RID: 2506
		// (get) Token: 0x060021F2 RID: 8690 RVA: 0x000633A8 File Offset: 0x000615A8
		// (set) Token: 0x060021F3 RID: 8691 RVA: 0x000633B0 File Offset: 0x000615B0
		public override int ReadTimeout
		{
			get
			{
				return this.readTimeout;
			}
			set
			{
				this.readTimeout = value;
			}
		}

		// Token: 0x170009CB RID: 2507
		// (get) Token: 0x060021F4 RID: 8692 RVA: 0x000633BC File Offset: 0x000615BC
		[MonoTODO]
		public virtual IIdentity RemoteIdentity
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170009CC RID: 2508
		// (get) Token: 0x060021F5 RID: 8693 RVA: 0x000633C4 File Offset: 0x000615C4
		// (set) Token: 0x060021F6 RID: 8694 RVA: 0x000633CC File Offset: 0x000615CC
		public override int WriteTimeout
		{
			get
			{
				return this.writeTimeout;
			}
			set
			{
				this.writeTimeout = value;
			}
		}

		// Token: 0x060021F7 RID: 8695 RVA: 0x000633D8 File Offset: 0x000615D8
		[MonoTODO]
		public virtual IAsyncResult BeginAuthenticateAsClient(AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021F8 RID: 8696 RVA: 0x000633E0 File Offset: 0x000615E0
		[MonoTODO]
		public virtual IAsyncResult BeginAuthenticateAsClient(NetworkCredential credential, string targetName, AsyncCallback asyncCallback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021F9 RID: 8697 RVA: 0x000633E8 File Offset: 0x000615E8
		[MonoTODO]
		public virtual IAsyncResult BeginAuthenticateAsClient(NetworkCredential credential, string targetName, ProtectionLevel requiredProtectionLevel, TokenImpersonationLevel allowedImpersonationLevel, AsyncCallback asyncCallback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FA RID: 8698 RVA: 0x000633F0 File Offset: 0x000615F0
		[MonoTODO]
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FB RID: 8699 RVA: 0x000633F8 File Offset: 0x000615F8
		[MonoTODO]
		public virtual IAsyncResult BeginAuthenticateAsServer(AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FC RID: 8700 RVA: 0x00063400 File Offset: 0x00061600
		[MonoTODO]
		public virtual IAsyncResult BeginAuthenticateAsServer(NetworkCredential credential, ProtectionLevel requiredProtectionLevel, TokenImpersonationLevel requiredImpersonationLevel, AsyncCallback asyncCallback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FD RID: 8701 RVA: 0x00063408 File Offset: 0x00061608
		[MonoTODO]
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FE RID: 8702 RVA: 0x00063410 File Offset: 0x00061610
		[MonoTODO]
		public virtual void AuthenticateAsClient()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021FF RID: 8703 RVA: 0x00063418 File Offset: 0x00061618
		[MonoTODO]
		public virtual void AuthenticateAsClient(NetworkCredential credential, string targetName)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002200 RID: 8704 RVA: 0x00063420 File Offset: 0x00061620
		[MonoTODO]
		public virtual void AuthenticateAsClient(NetworkCredential credential, string targetName, ProtectionLevel requiredProtectionLevel, TokenImpersonationLevel requiredImpersonationLevel)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002201 RID: 8705 RVA: 0x00063428 File Offset: 0x00061628
		[MonoTODO]
		public virtual void AuthenticateAsServer()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002202 RID: 8706 RVA: 0x00063430 File Offset: 0x00061630
		[MonoTODO]
		public virtual void AuthenticateAsServer(NetworkCredential credential, ProtectionLevel requiredProtectionLevel, TokenImpersonationLevel requiredImpersonationLevel)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002203 RID: 8707 RVA: 0x00063438 File Offset: 0x00061638
		[MonoTODO]
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
		}

		// Token: 0x06002204 RID: 8708 RVA: 0x00063440 File Offset: 0x00061640
		[MonoTODO]
		public virtual void EndAuthenticateAsClient(IAsyncResult asyncResult)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002205 RID: 8709 RVA: 0x00063448 File Offset: 0x00061648
		[MonoTODO]
		public override int EndRead(IAsyncResult asyncResult)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002206 RID: 8710 RVA: 0x00063450 File Offset: 0x00061650
		[MonoTODO]
		public virtual void EndAuthenticateAsServer(IAsyncResult asyncResult)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002207 RID: 8711 RVA: 0x00063458 File Offset: 0x00061658
		[MonoTODO]
		public override void EndWrite(IAsyncResult asyncResult)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002208 RID: 8712 RVA: 0x00063460 File Offset: 0x00061660
		[MonoTODO]
		public override void Flush()
		{
			base.InnerStream.Flush();
		}

		// Token: 0x06002209 RID: 8713 RVA: 0x00063470 File Offset: 0x00061670
		[MonoTODO]
		public override int Read(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x00063478 File Offset: 0x00061678
		[MonoTODO]
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600220B RID: 8715 RVA: 0x00063480 File Offset: 0x00061680
		[MonoTODO]
		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600220C RID: 8716 RVA: 0x00063488 File Offset: 0x00061688
		[MonoTODO]
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040014FA RID: 5370
		private int readTimeout;

		// Token: 0x040014FB RID: 5371
		private int writeTimeout;
	}
}
