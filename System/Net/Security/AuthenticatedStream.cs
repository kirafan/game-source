﻿using System;
using System.IO;

namespace System.Net.Security
{
	// Token: 0x020003DD RID: 989
	public abstract class AuthenticatedStream : Stream
	{
		// Token: 0x060021DA RID: 8666 RVA: 0x00063294 File Offset: 0x00061494
		protected AuthenticatedStream(Stream innerStream, bool leaveInnerStreamOpen)
		{
			this.innerStream = innerStream;
			this.leaveStreamOpen = leaveInnerStreamOpen;
		}

		// Token: 0x170009B7 RID: 2487
		// (get) Token: 0x060021DB RID: 8667 RVA: 0x000632AC File Offset: 0x000614AC
		protected Stream InnerStream
		{
			get
			{
				return this.innerStream;
			}
		}

		// Token: 0x170009B8 RID: 2488
		// (get) Token: 0x060021DC RID: 8668
		public abstract bool IsAuthenticated { get; }

		// Token: 0x170009B9 RID: 2489
		// (get) Token: 0x060021DD RID: 8669
		public abstract bool IsEncrypted { get; }

		// Token: 0x170009BA RID: 2490
		// (get) Token: 0x060021DE RID: 8670
		public abstract bool IsMutuallyAuthenticated { get; }

		// Token: 0x170009BB RID: 2491
		// (get) Token: 0x060021DF RID: 8671
		public abstract bool IsServer { get; }

		// Token: 0x170009BC RID: 2492
		// (get) Token: 0x060021E0 RID: 8672
		public abstract bool IsSigned { get; }

		// Token: 0x170009BD RID: 2493
		// (get) Token: 0x060021E1 RID: 8673 RVA: 0x000632B4 File Offset: 0x000614B4
		public bool LeaveInnerStreamOpen
		{
			get
			{
				return this.leaveStreamOpen;
			}
		}

		// Token: 0x060021E2 RID: 8674 RVA: 0x000632BC File Offset: 0x000614BC
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.innerStream != null)
			{
				if (!this.leaveStreamOpen)
				{
					this.innerStream.Close();
				}
				this.innerStream = null;
			}
		}

		// Token: 0x040014F4 RID: 5364
		private Stream innerStream;

		// Token: 0x040014F5 RID: 5365
		private bool leaveStreamOpen;
	}
}
