﻿using System;

namespace System.Net.Security
{
	// Token: 0x020003E3 RID: 995
	[Flags]
	public enum SslPolicyErrors
	{
		// Token: 0x04001507 RID: 5383
		None = 0,
		// Token: 0x04001508 RID: 5384
		RemoteCertificateNotAvailable = 1,
		// Token: 0x04001509 RID: 5385
		RemoteCertificateNameMismatch = 2,
		// Token: 0x0400150A RID: 5386
		RemoteCertificateChainErrors = 4
	}
}
