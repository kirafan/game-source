﻿using System;

namespace System.Net.Security
{
	// Token: 0x020003DE RID: 990
	public enum AuthenticationLevel
	{
		// Token: 0x040014F7 RID: 5367
		None,
		// Token: 0x040014F8 RID: 5368
		MutualAuthRequested,
		// Token: 0x040014F9 RID: 5369
		MutualAuthRequired
	}
}
