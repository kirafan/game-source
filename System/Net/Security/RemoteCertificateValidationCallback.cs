﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Net.Security
{
	// Token: 0x0200051C RID: 1308
	// (Invoke) Token: 0x06002D1C RID: 11548
	public delegate bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chain, SslPolicyErrors sslPolicyErrors);
}
