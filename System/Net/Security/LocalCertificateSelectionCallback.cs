﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Net.Security
{
	// Token: 0x0200051B RID: 1307
	// (Invoke) Token: 0x06002D18 RID: 11544
	public delegate X509Certificate LocalCertificateSelectionCallback(object sender, string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers);
}
