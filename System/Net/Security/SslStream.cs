﻿using System;
using System.IO;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Protocol.Tls;

namespace System.Net.Security
{
	// Token: 0x020003E2 RID: 994
	[MonoTODO("Non-X509Certificate2 certificate is not supported")]
	public class SslStream : AuthenticatedStream
	{
		// Token: 0x0600220D RID: 8717 RVA: 0x00063490 File Offset: 0x00061690
		public SslStream(Stream innerStream) : this(innerStream, false)
		{
		}

		// Token: 0x0600220E RID: 8718 RVA: 0x0006349C File Offset: 0x0006169C
		public SslStream(Stream innerStream, bool leaveStreamOpen) : base(innerStream, leaveStreamOpen)
		{
		}

		// Token: 0x0600220F RID: 8719 RVA: 0x000634A8 File Offset: 0x000616A8
		[MonoTODO("certValidationCallback is not passed X509Chain and SslPolicyErrors correctly")]
		public SslStream(Stream innerStream, bool leaveStreamOpen, RemoteCertificateValidationCallback certValidationCallback) : this(innerStream, leaveStreamOpen, certValidationCallback, null)
		{
		}

		// Token: 0x06002210 RID: 8720 RVA: 0x000634B4 File Offset: 0x000616B4
		[MonoTODO("certValidationCallback is not passed X509Chain and SslPolicyErrors correctly")]
		public SslStream(Stream innerStream, bool leaveStreamOpen, RemoteCertificateValidationCallback certValidationCallback, LocalCertificateSelectionCallback certSelectionCallback) : base(innerStream, leaveStreamOpen)
		{
			this.validation_callback = certValidationCallback;
			this.selection_callback = certSelectionCallback;
		}

		// Token: 0x170009CD RID: 2509
		// (get) Token: 0x06002211 RID: 8721 RVA: 0x000634D0 File Offset: 0x000616D0
		public override bool CanRead
		{
			get
			{
				return base.InnerStream.CanRead;
			}
		}

		// Token: 0x170009CE RID: 2510
		// (get) Token: 0x06002212 RID: 8722 RVA: 0x000634E0 File Offset: 0x000616E0
		public override bool CanSeek
		{
			get
			{
				return base.InnerStream.CanSeek;
			}
		}

		// Token: 0x170009CF RID: 2511
		// (get) Token: 0x06002213 RID: 8723 RVA: 0x000634F0 File Offset: 0x000616F0
		public override bool CanTimeout
		{
			get
			{
				return base.InnerStream.CanTimeout;
			}
		}

		// Token: 0x170009D0 RID: 2512
		// (get) Token: 0x06002214 RID: 8724 RVA: 0x00063500 File Offset: 0x00061700
		public override bool CanWrite
		{
			get
			{
				return base.InnerStream.CanWrite;
			}
		}

		// Token: 0x170009D1 RID: 2513
		// (get) Token: 0x06002215 RID: 8725 RVA: 0x00063510 File Offset: 0x00061710
		public override long Length
		{
			get
			{
				return base.InnerStream.Length;
			}
		}

		// Token: 0x170009D2 RID: 2514
		// (get) Token: 0x06002216 RID: 8726 RVA: 0x00063520 File Offset: 0x00061720
		// (set) Token: 0x06002217 RID: 8727 RVA: 0x00063530 File Offset: 0x00061730
		public override long Position
		{
			get
			{
				return base.InnerStream.Position;
			}
			set
			{
				throw new NotSupportedException("This stream does not support seek operations");
			}
		}

		// Token: 0x170009D3 RID: 2515
		// (get) Token: 0x06002218 RID: 8728 RVA: 0x0006353C File Offset: 0x0006173C
		public override bool IsAuthenticated
		{
			get
			{
				return this.ssl_stream != null;
			}
		}

		// Token: 0x170009D4 RID: 2516
		// (get) Token: 0x06002219 RID: 8729 RVA: 0x0006354C File Offset: 0x0006174C
		public override bool IsEncrypted
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x170009D5 RID: 2517
		// (get) Token: 0x0600221A RID: 8730 RVA: 0x00063554 File Offset: 0x00061754
		public override bool IsMutuallyAuthenticated
		{
			get
			{
				return this.IsAuthenticated && ((!this.IsServer) ? (this.LocalCertificate != null) : (this.RemoteCertificate != null));
			}
		}

		// Token: 0x170009D6 RID: 2518
		// (get) Token: 0x0600221B RID: 8731 RVA: 0x00063598 File Offset: 0x00061798
		public override bool IsServer
		{
			get
			{
				return this.ssl_stream is SslServerStream;
			}
		}

		// Token: 0x170009D7 RID: 2519
		// (get) Token: 0x0600221C RID: 8732 RVA: 0x000635A8 File Offset: 0x000617A8
		public override bool IsSigned
		{
			get
			{
				return this.IsAuthenticated;
			}
		}

		// Token: 0x170009D8 RID: 2520
		// (get) Token: 0x0600221D RID: 8733 RVA: 0x000635B0 File Offset: 0x000617B0
		// (set) Token: 0x0600221E RID: 8734 RVA: 0x000635C0 File Offset: 0x000617C0
		public override int ReadTimeout
		{
			get
			{
				return base.InnerStream.ReadTimeout;
			}
			set
			{
				base.InnerStream.ReadTimeout = value;
			}
		}

		// Token: 0x170009D9 RID: 2521
		// (get) Token: 0x0600221F RID: 8735 RVA: 0x000635D0 File Offset: 0x000617D0
		// (set) Token: 0x06002220 RID: 8736 RVA: 0x000635E0 File Offset: 0x000617E0
		public override int WriteTimeout
		{
			get
			{
				return base.InnerStream.WriteTimeout;
			}
			set
			{
				base.InnerStream.WriteTimeout = value;
			}
		}

		// Token: 0x170009DA RID: 2522
		// (get) Token: 0x06002221 RID: 8737 RVA: 0x000635F0 File Offset: 0x000617F0
		public virtual bool CheckCertRevocationStatus
		{
			get
			{
				return this.IsAuthenticated && this.ssl_stream.CheckCertRevocationStatus;
			}
		}

		// Token: 0x170009DB RID: 2523
		// (get) Token: 0x06002222 RID: 8738 RVA: 0x0006360C File Offset: 0x0006180C
		public virtual System.Security.Authentication.CipherAlgorithmType CipherAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.CipherAlgorithm)
				{
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Des:
					return System.Security.Authentication.CipherAlgorithmType.Des;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.None:
					return System.Security.Authentication.CipherAlgorithmType.None;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rc2:
					return System.Security.Authentication.CipherAlgorithmType.Rc2;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rc4:
					return System.Security.Authentication.CipherAlgorithmType.Rc4;
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.Rijndael:
				{
					int cipherStrength = this.ssl_stream.CipherStrength;
					if (cipherStrength == 128)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes128;
					}
					if (cipherStrength == 192)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes192;
					}
					if (cipherStrength == 256)
					{
						return System.Security.Authentication.CipherAlgorithmType.Aes256;
					}
					break;
				}
				case Mono.Security.Protocol.Tls.CipherAlgorithmType.TripleDes:
					return System.Security.Authentication.CipherAlgorithmType.TripleDes;
				}
				throw new InvalidOperationException("Not supported cipher algorithm is in use. It is likely a bug in SslStream.");
			}
		}

		// Token: 0x170009DC RID: 2524
		// (get) Token: 0x06002223 RID: 8739 RVA: 0x000636C4 File Offset: 0x000618C4
		public virtual int CipherStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.CipherStrength;
			}
		}

		// Token: 0x170009DD RID: 2525
		// (get) Token: 0x06002224 RID: 8740 RVA: 0x000636D8 File Offset: 0x000618D8
		public virtual System.Security.Authentication.HashAlgorithmType HashAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.HashAlgorithm)
				{
				case Mono.Security.Protocol.Tls.HashAlgorithmType.Md5:
					return System.Security.Authentication.HashAlgorithmType.Md5;
				case Mono.Security.Protocol.Tls.HashAlgorithmType.None:
					return System.Security.Authentication.HashAlgorithmType.None;
				case Mono.Security.Protocol.Tls.HashAlgorithmType.Sha1:
					return System.Security.Authentication.HashAlgorithmType.Sha1;
				default:
					throw new InvalidOperationException("Not supported hash algorithm is in use. It is likely a bug in SslStream.");
				}
			}
		}

		// Token: 0x170009DE RID: 2526
		// (get) Token: 0x06002225 RID: 8741 RVA: 0x00063728 File Offset: 0x00061928
		public virtual int HashStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.HashStrength;
			}
		}

		// Token: 0x170009DF RID: 2527
		// (get) Token: 0x06002226 RID: 8742 RVA: 0x0006373C File Offset: 0x0006193C
		public virtual System.Security.Authentication.ExchangeAlgorithmType KeyExchangeAlgorithm
		{
			get
			{
				this.CheckConnectionAuthenticated();
				switch (this.ssl_stream.KeyExchangeAlgorithm)
				{
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.DiffieHellman:
					return System.Security.Authentication.ExchangeAlgorithmType.DiffieHellman;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.None:
					return System.Security.Authentication.ExchangeAlgorithmType.None;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.RsaKeyX:
					return System.Security.Authentication.ExchangeAlgorithmType.RsaKeyX;
				case Mono.Security.Protocol.Tls.ExchangeAlgorithmType.RsaSign:
					return System.Security.Authentication.ExchangeAlgorithmType.RsaSign;
				}
				throw new InvalidOperationException("Not supported exchange algorithm is in use. It is likely a bug in SslStream.");
			}
		}

		// Token: 0x170009E0 RID: 2528
		// (get) Token: 0x06002227 RID: 8743 RVA: 0x000637A0 File Offset: 0x000619A0
		public virtual int KeyExchangeStrength
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.ssl_stream.KeyExchangeStrength;
			}
		}

		// Token: 0x170009E1 RID: 2529
		// (get) Token: 0x06002228 RID: 8744 RVA: 0x000637B4 File Offset: 0x000619B4
		public virtual X509Certificate LocalCertificate
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return (!this.IsServer) ? ((SslClientStream)this.ssl_stream).SelectedClientCertificate : this.ssl_stream.ServerCertificate;
			}
		}

		// Token: 0x170009E2 RID: 2530
		// (get) Token: 0x06002229 RID: 8745 RVA: 0x000637F4 File Offset: 0x000619F4
		public virtual X509Certificate RemoteCertificate
		{
			get
			{
				this.CheckConnectionAuthenticated();
				return this.IsServer ? ((SslServerStream)this.ssl_stream).ClientCertificate : this.ssl_stream.ServerCertificate;
			}
		}

		// Token: 0x170009E3 RID: 2531
		// (get) Token: 0x0600222A RID: 8746 RVA: 0x00063834 File Offset: 0x00061A34
		public virtual System.Security.Authentication.SslProtocols SslProtocol
		{
			get
			{
				this.CheckConnectionAuthenticated();
				SecurityProtocolType securityProtocol = this.ssl_stream.SecurityProtocol;
				if (securityProtocol == SecurityProtocolType.Default)
				{
					return System.Security.Authentication.SslProtocols.Default;
				}
				if (securityProtocol == SecurityProtocolType.Ssl2)
				{
					return System.Security.Authentication.SslProtocols.Ssl2;
				}
				if (securityProtocol == SecurityProtocolType.Ssl3)
				{
					return System.Security.Authentication.SslProtocols.Ssl3;
				}
				if (securityProtocol != SecurityProtocolType.Tls)
				{
					throw new InvalidOperationException("Not supported SSL/TLS protocol is in use. It is likely a bug in SslStream.");
				}
				return System.Security.Authentication.SslProtocols.Tls;
			}
		}

		// Token: 0x0600222B RID: 8747 RVA: 0x0006389C File Offset: 0x00061A9C
		private X509Certificate OnCertificateSelection(System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCerts, X509Certificate serverCert, string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection serverRequestedCerts)
		{
			string[] array = new string[(serverRequestedCerts == null) ? 0 : serverRequestedCerts.Count];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = serverRequestedCerts[i].GetIssuerName();
			}
			return this.selection_callback(this, targetHost, clientCerts, serverCert, array);
		}

		// Token: 0x0600222C RID: 8748 RVA: 0x000638F8 File Offset: 0x00061AF8
		public virtual IAsyncResult BeginAuthenticateAsClient(string targetHost, AsyncCallback asyncCallback, object asyncState)
		{
			return this.BeginAuthenticateAsClient(targetHost, new System.Security.Cryptography.X509Certificates.X509CertificateCollection(), System.Security.Authentication.SslProtocols.Tls, false, asyncCallback, asyncState);
		}

		// Token: 0x0600222D RID: 8749 RVA: 0x00063910 File Offset: 0x00061B10
		public virtual IAsyncResult BeginAuthenticateAsClient(string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, System.Security.Authentication.SslProtocols sslProtocolType, bool checkCertificateRevocation, AsyncCallback asyncCallback, object asyncState)
		{
			if (this.IsAuthenticated)
			{
				throw new InvalidOperationException("This SslStream is already authenticated");
			}
			SslClientStream sslClientStream = new SslClientStream(base.InnerStream, targetHost, !base.LeaveInnerStreamOpen, this.GetMonoSslProtocol(sslProtocolType), clientCertificates);
			sslClientStream.CheckCertRevocationStatus = checkCertificateRevocation;
			sslClientStream.PrivateKeyCertSelectionDelegate = delegate(X509Certificate cert, string host)
			{
				string certHashString = cert.GetCertHashString();
				foreach (X509Certificate x509Certificate in clientCertificates)
				{
					if (!(x509Certificate.GetCertHashString() != certHashString))
					{
						System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = x509Certificate as System.Security.Cryptography.X509Certificates.X509Certificate2;
						x509Certificate2 = (x509Certificate2 ?? new System.Security.Cryptography.X509Certificates.X509Certificate2(x509Certificate));
						return x509Certificate2.PrivateKey;
					}
				}
				return null;
			};
			if (this.validation_callback != null)
			{
				sslClientStream.ServerCertValidationDelegate = delegate(X509Certificate cert, int[] certErrors)
				{
					System.Security.Cryptography.X509Certificates.X509Chain x509Chain = new System.Security.Cryptography.X509Certificates.X509Chain();
					System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate = cert as System.Security.Cryptography.X509Certificates.X509Certificate2;
					if (x509Certificate == null)
					{
						x509Certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2(cert);
					}
					if (!ServicePointManager.CheckCertificateRevocationList)
					{
						x509Chain.ChainPolicy.RevocationMode = System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck;
					}
					SslPolicyErrors sslPolicyErrors = SslPolicyErrors.None;
					foreach (int num in certErrors)
					{
						int num2 = num;
						if (num2 != -2146762490)
						{
							if (num2 != -2146762481)
							{
								sslPolicyErrors |= SslPolicyErrors.RemoteCertificateChainErrors;
							}
							else
							{
								sslPolicyErrors |= SslPolicyErrors.RemoteCertificateNameMismatch;
							}
						}
						else
						{
							sslPolicyErrors |= SslPolicyErrors.RemoteCertificateNotAvailable;
						}
					}
					x509Chain.Build(x509Certificate);
					foreach (System.Security.Cryptography.X509Certificates.X509ChainStatus x509ChainStatus in x509Chain.ChainStatus)
					{
						if (x509ChainStatus.Status != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
						{
							if ((x509ChainStatus.Status & System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.PartialChain) != System.Security.Cryptography.X509Certificates.X509ChainStatusFlags.NoError)
							{
								sslPolicyErrors |= SslPolicyErrors.RemoteCertificateNotAvailable;
							}
							else
							{
								sslPolicyErrors |= SslPolicyErrors.RemoteCertificateChainErrors;
							}
						}
					}
					return this.validation_callback(this, cert, x509Chain, sslPolicyErrors);
				};
			}
			if (this.selection_callback != null)
			{
				sslClientStream.ClientCertSelectionDelegate = new CertificateSelectionCallback(this.OnCertificateSelection);
			}
			this.ssl_stream = sslClientStream;
			return this.BeginWrite(new byte[0], 0, 0, asyncCallback, asyncState);
		}

		// Token: 0x0600222E RID: 8750 RVA: 0x000639D8 File Offset: 0x00061BD8
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.BeginRead(buffer, offset, count, asyncCallback, asyncState);
		}

		// Token: 0x0600222F RID: 8751 RVA: 0x00063A00 File Offset: 0x00061C00
		public virtual IAsyncResult BeginAuthenticateAsServer(X509Certificate serverCertificate, AsyncCallback callback, object asyncState)
		{
			return this.BeginAuthenticateAsServer(serverCertificate, false, System.Security.Authentication.SslProtocols.Tls, false, callback, asyncState);
		}

		// Token: 0x06002230 RID: 8752 RVA: 0x00063A14 File Offset: 0x00061C14
		public virtual IAsyncResult BeginAuthenticateAsServer(X509Certificate serverCertificate, bool clientCertificateRequired, System.Security.Authentication.SslProtocols sslProtocolType, bool checkCertificateRevocation, AsyncCallback callback, object asyncState)
		{
			if (this.IsAuthenticated)
			{
				throw new InvalidOperationException("This SslStream is already authenticated");
			}
			SslServerStream sslServerStream = new SslServerStream(base.InnerStream, serverCertificate, clientCertificateRequired, !base.LeaveInnerStreamOpen, this.GetMonoSslProtocol(sslProtocolType));
			sslServerStream.CheckCertRevocationStatus = checkCertificateRevocation;
			sslServerStream.PrivateKeyCertSelectionDelegate = delegate(X509Certificate cert, string targetHost)
			{
				System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate = (serverCertificate as System.Security.Cryptography.X509Certificates.X509Certificate2) ?? new System.Security.Cryptography.X509Certificates.X509Certificate2(serverCertificate);
				return (x509Certificate == null) ? null : x509Certificate.PrivateKey;
			};
			if (this.validation_callback != null)
			{
				sslServerStream.ClientCertValidationDelegate = delegate(X509Certificate cert, int[] certErrors)
				{
					System.Security.Cryptography.X509Certificates.X509Chain x509Chain = null;
					if (cert is System.Security.Cryptography.X509Certificates.X509Certificate2)
					{
						x509Chain = new System.Security.Cryptography.X509Certificates.X509Chain();
						x509Chain.Build((System.Security.Cryptography.X509Certificates.X509Certificate2)cert);
					}
					SslPolicyErrors sslPolicyErrors = (certErrors.Length <= 0) ? SslPolicyErrors.None : SslPolicyErrors.RemoteCertificateChainErrors;
					return this.validation_callback(this, cert, x509Chain, sslPolicyErrors);
				};
			}
			this.ssl_stream = sslServerStream;
			return this.BeginRead(new byte[0], 0, 0, callback, asyncState);
		}

		// Token: 0x06002231 RID: 8753 RVA: 0x00063AC0 File Offset: 0x00061CC0
		private SecurityProtocolType GetMonoSslProtocol(System.Security.Authentication.SslProtocols ms)
		{
			if (ms == System.Security.Authentication.SslProtocols.Ssl2)
			{
				return SecurityProtocolType.Ssl2;
			}
			if (ms == System.Security.Authentication.SslProtocols.Ssl3)
			{
				return SecurityProtocolType.Ssl3;
			}
			if (ms != System.Security.Authentication.SslProtocols.Tls)
			{
				return SecurityProtocolType.Default;
			}
			return SecurityProtocolType.Tls;
		}

		// Token: 0x06002232 RID: 8754 RVA: 0x00063B00 File Offset: 0x00061D00
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback asyncCallback, object asyncState)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.BeginWrite(buffer, offset, count, asyncCallback, asyncState);
		}

		// Token: 0x06002233 RID: 8755 RVA: 0x00063B28 File Offset: 0x00061D28
		public virtual void AuthenticateAsClient(string targetHost)
		{
			this.AuthenticateAsClient(targetHost, new System.Security.Cryptography.X509Certificates.X509CertificateCollection(), System.Security.Authentication.SslProtocols.Tls, false);
		}

		// Token: 0x06002234 RID: 8756 RVA: 0x00063B3C File Offset: 0x00061D3C
		public virtual void AuthenticateAsClient(string targetHost, System.Security.Cryptography.X509Certificates.X509CertificateCollection clientCertificates, System.Security.Authentication.SslProtocols sslProtocolType, bool checkCertificateRevocation)
		{
			this.EndAuthenticateAsClient(this.BeginAuthenticateAsClient(targetHost, clientCertificates, sslProtocolType, checkCertificateRevocation, null, null));
		}

		// Token: 0x06002235 RID: 8757 RVA: 0x00063B5C File Offset: 0x00061D5C
		public virtual void AuthenticateAsServer(X509Certificate serverCertificate)
		{
			this.AuthenticateAsServer(serverCertificate, false, System.Security.Authentication.SslProtocols.Tls, false);
		}

		// Token: 0x06002236 RID: 8758 RVA: 0x00063B6C File Offset: 0x00061D6C
		public virtual void AuthenticateAsServer(X509Certificate serverCertificate, bool clientCertificateRequired, System.Security.Authentication.SslProtocols sslProtocolType, bool checkCertificateRevocation)
		{
			this.EndAuthenticateAsServer(this.BeginAuthenticateAsServer(serverCertificate, clientCertificateRequired, sslProtocolType, checkCertificateRevocation, null, null));
		}

		// Token: 0x06002237 RID: 8759 RVA: 0x00063B8C File Offset: 0x00061D8C
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.ssl_stream != null)
				{
					this.ssl_stream.Dispose();
				}
				this.ssl_stream = null;
			}
			base.Dispose(disposing);
		}

		// Token: 0x06002238 RID: 8760 RVA: 0x00063BC4 File Offset: 0x00061DC4
		public virtual void EndAuthenticateAsClient(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			if (this.CanRead)
			{
				this.ssl_stream.EndRead(asyncResult);
			}
			else
			{
				this.ssl_stream.EndWrite(asyncResult);
			}
		}

		// Token: 0x06002239 RID: 8761 RVA: 0x00063C00 File Offset: 0x00061E00
		public virtual void EndAuthenticateAsServer(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			if (this.CanRead)
			{
				this.ssl_stream.EndRead(asyncResult);
			}
			else
			{
				this.ssl_stream.EndWrite(asyncResult);
			}
		}

		// Token: 0x0600223A RID: 8762 RVA: 0x00063C3C File Offset: 0x00061E3C
		public override int EndRead(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			return this.ssl_stream.EndRead(asyncResult);
		}

		// Token: 0x0600223B RID: 8763 RVA: 0x00063C50 File Offset: 0x00061E50
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.CheckConnectionAuthenticated();
			this.ssl_stream.EndWrite(asyncResult);
		}

		// Token: 0x0600223C RID: 8764 RVA: 0x00063C64 File Offset: 0x00061E64
		public override void Flush()
		{
			this.CheckConnectionAuthenticated();
			base.InnerStream.Flush();
		}

		// Token: 0x0600223D RID: 8765 RVA: 0x00063C78 File Offset: 0x00061E78
		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.EndRead(this.BeginRead(buffer, offset, count, null, null));
		}

		// Token: 0x0600223E RID: 8766 RVA: 0x00063C8C File Offset: 0x00061E8C
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("This stream does not support seek operations");
		}

		// Token: 0x0600223F RID: 8767 RVA: 0x00063C98 File Offset: 0x00061E98
		public override void SetLength(long value)
		{
			base.InnerStream.SetLength(value);
		}

		// Token: 0x06002240 RID: 8768 RVA: 0x00063CA8 File Offset: 0x00061EA8
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.EndWrite(this.BeginWrite(buffer, offset, count, null, null));
		}

		// Token: 0x06002241 RID: 8769 RVA: 0x00063CBC File Offset: 0x00061EBC
		public void Write(byte[] buffer)
		{
			this.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x06002242 RID: 8770 RVA: 0x00063CCC File Offset: 0x00061ECC
		private void CheckConnectionAuthenticated()
		{
			if (!this.IsAuthenticated)
			{
				throw new InvalidOperationException("This operation is invalid until it is successfully authenticated");
			}
		}

		// Token: 0x04001503 RID: 5379
		private SslStreamBase ssl_stream;

		// Token: 0x04001504 RID: 5380
		private RemoteCertificateValidationCallback validation_callback;

		// Token: 0x04001505 RID: 5381
		private LocalCertificateSelectionCallback selection_callback;
	}
}
