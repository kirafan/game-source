﻿using System;

namespace System.Net
{
	// Token: 0x0200031E RID: 798
	public enum HttpStatusCode
	{
		// Token: 0x04001185 RID: 4485
		Continue = 100,
		// Token: 0x04001186 RID: 4486
		SwitchingProtocols,
		// Token: 0x04001187 RID: 4487
		OK = 200,
		// Token: 0x04001188 RID: 4488
		Created,
		// Token: 0x04001189 RID: 4489
		Accepted,
		// Token: 0x0400118A RID: 4490
		NonAuthoritativeInformation,
		// Token: 0x0400118B RID: 4491
		NoContent,
		// Token: 0x0400118C RID: 4492
		ResetContent,
		// Token: 0x0400118D RID: 4493
		PartialContent,
		// Token: 0x0400118E RID: 4494
		MultipleChoices = 300,
		// Token: 0x0400118F RID: 4495
		Ambiguous = 300,
		// Token: 0x04001190 RID: 4496
		MovedPermanently,
		// Token: 0x04001191 RID: 4497
		Moved = 301,
		// Token: 0x04001192 RID: 4498
		Found,
		// Token: 0x04001193 RID: 4499
		Redirect = 302,
		// Token: 0x04001194 RID: 4500
		SeeOther,
		// Token: 0x04001195 RID: 4501
		RedirectMethod = 303,
		// Token: 0x04001196 RID: 4502
		NotModified,
		// Token: 0x04001197 RID: 4503
		UseProxy,
		// Token: 0x04001198 RID: 4504
		Unused,
		// Token: 0x04001199 RID: 4505
		TemporaryRedirect,
		// Token: 0x0400119A RID: 4506
		RedirectKeepVerb = 307,
		// Token: 0x0400119B RID: 4507
		BadRequest = 400,
		// Token: 0x0400119C RID: 4508
		Unauthorized,
		// Token: 0x0400119D RID: 4509
		PaymentRequired,
		// Token: 0x0400119E RID: 4510
		Forbidden,
		// Token: 0x0400119F RID: 4511
		NotFound,
		// Token: 0x040011A0 RID: 4512
		MethodNotAllowed,
		// Token: 0x040011A1 RID: 4513
		NotAcceptable,
		// Token: 0x040011A2 RID: 4514
		ProxyAuthenticationRequired,
		// Token: 0x040011A3 RID: 4515
		RequestTimeout,
		// Token: 0x040011A4 RID: 4516
		Conflict,
		// Token: 0x040011A5 RID: 4517
		Gone,
		// Token: 0x040011A6 RID: 4518
		LengthRequired,
		// Token: 0x040011A7 RID: 4519
		PreconditionFailed,
		// Token: 0x040011A8 RID: 4520
		RequestEntityTooLarge,
		// Token: 0x040011A9 RID: 4521
		RequestUriTooLong,
		// Token: 0x040011AA RID: 4522
		UnsupportedMediaType,
		// Token: 0x040011AB RID: 4523
		RequestedRangeNotSatisfiable,
		// Token: 0x040011AC RID: 4524
		ExpectationFailed,
		// Token: 0x040011AD RID: 4525
		InternalServerError = 500,
		// Token: 0x040011AE RID: 4526
		NotImplemented,
		// Token: 0x040011AF RID: 4527
		BadGateway,
		// Token: 0x040011B0 RID: 4528
		ServiceUnavailable,
		// Token: 0x040011B1 RID: 4529
		GatewayTimeout,
		// Token: 0x040011B2 RID: 4530
		HttpVersionNotSupported
	}
}
