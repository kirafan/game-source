﻿using System;

namespace System
{
	// Token: 0x020004B1 RID: 1201
	[Flags]
	public enum UriComponents
	{
		// Token: 0x04001B31 RID: 6961
		Scheme = 1,
		// Token: 0x04001B32 RID: 6962
		UserInfo = 2,
		// Token: 0x04001B33 RID: 6963
		Host = 4,
		// Token: 0x04001B34 RID: 6964
		Port = 8,
		// Token: 0x04001B35 RID: 6965
		Path = 16,
		// Token: 0x04001B36 RID: 6966
		Query = 32,
		// Token: 0x04001B37 RID: 6967
		Fragment = 64,
		// Token: 0x04001B38 RID: 6968
		StrongPort = 128,
		// Token: 0x04001B39 RID: 6969
		KeepDelimiter = 1073741824,
		// Token: 0x04001B3A RID: 6970
		HostAndPort = 132,
		// Token: 0x04001B3B RID: 6971
		StrongAuthority = 134,
		// Token: 0x04001B3C RID: 6972
		AbsoluteUri = 127,
		// Token: 0x04001B3D RID: 6973
		PathAndQuery = 48,
		// Token: 0x04001B3E RID: 6974
		HttpRequestUrl = 61,
		// Token: 0x04001B3F RID: 6975
		SchemeAndServer = 13,
		// Token: 0x04001B40 RID: 6976
		SerializationInfoString = -2147483648
	}
}
