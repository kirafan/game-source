﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000074 RID: 116
	[Preserve]
	public interface IContractResolver
	{
		// Token: 0x060004D7 RID: 1239
		JsonContract ResolveContract(Type type);
	}
}
