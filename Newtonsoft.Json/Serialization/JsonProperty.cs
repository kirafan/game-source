﻿using System;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200007E RID: 126
	[Preserve]
	public class JsonProperty
	{
		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000537 RID: 1335 RVA: 0x00014062 File Offset: 0x00012262
		// (set) Token: 0x06000538 RID: 1336 RVA: 0x0001406A File Offset: 0x0001226A
		internal JsonContract PropertyContract { get; set; }

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000539 RID: 1337 RVA: 0x00014073 File Offset: 0x00012273
		// (set) Token: 0x0600053A RID: 1338 RVA: 0x0001407B File Offset: 0x0001227B
		public string PropertyName
		{
			get
			{
				return this._propertyName;
			}
			set
			{
				this._propertyName = value;
				this._skipPropertyNameEscape = !JavaScriptUtils.ShouldEscapeJavaScriptString(this._propertyName, JavaScriptUtils.HtmlCharEscapeFlags);
			}
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x0600053B RID: 1339 RVA: 0x0001409D File Offset: 0x0001229D
		// (set) Token: 0x0600053C RID: 1340 RVA: 0x000140A5 File Offset: 0x000122A5
		public Type DeclaringType { get; set; }

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x0600053D RID: 1341 RVA: 0x000140AE File Offset: 0x000122AE
		// (set) Token: 0x0600053E RID: 1342 RVA: 0x000140B6 File Offset: 0x000122B6
		public int? Order { get; set; }

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600053F RID: 1343 RVA: 0x000140BF File Offset: 0x000122BF
		// (set) Token: 0x06000540 RID: 1344 RVA: 0x000140C7 File Offset: 0x000122C7
		public string UnderlyingName { get; set; }

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000541 RID: 1345 RVA: 0x000140D0 File Offset: 0x000122D0
		// (set) Token: 0x06000542 RID: 1346 RVA: 0x000140D8 File Offset: 0x000122D8
		public IValueProvider ValueProvider { get; set; }

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x000140E1 File Offset: 0x000122E1
		// (set) Token: 0x06000544 RID: 1348 RVA: 0x000140E9 File Offset: 0x000122E9
		public IAttributeProvider AttributeProvider { get; set; }

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x000140F2 File Offset: 0x000122F2
		// (set) Token: 0x06000546 RID: 1350 RVA: 0x000140FA File Offset: 0x000122FA
		public Type PropertyType
		{
			get
			{
				return this._propertyType;
			}
			set
			{
				if (this._propertyType != value)
				{
					this._propertyType = value;
					this._hasGeneratedDefaultValue = false;
				}
			}
		}

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x00014113 File Offset: 0x00012313
		// (set) Token: 0x06000548 RID: 1352 RVA: 0x0001411B File Offset: 0x0001231B
		public JsonConverter Converter { get; set; }

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x00014124 File Offset: 0x00012324
		// (set) Token: 0x0600054A RID: 1354 RVA: 0x0001412C File Offset: 0x0001232C
		public JsonConverter MemberConverter { get; set; }

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00014135 File Offset: 0x00012335
		// (set) Token: 0x0600054C RID: 1356 RVA: 0x0001413D File Offset: 0x0001233D
		public bool Ignored { get; set; }

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x0600054D RID: 1357 RVA: 0x00014146 File Offset: 0x00012346
		// (set) Token: 0x0600054E RID: 1358 RVA: 0x0001414E File Offset: 0x0001234E
		public bool Readable { get; set; }

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x0600054F RID: 1359 RVA: 0x00014157 File Offset: 0x00012357
		// (set) Token: 0x06000550 RID: 1360 RVA: 0x0001415F File Offset: 0x0001235F
		public bool Writable { get; set; }

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000551 RID: 1361 RVA: 0x00014168 File Offset: 0x00012368
		// (set) Token: 0x06000552 RID: 1362 RVA: 0x00014170 File Offset: 0x00012370
		public bool HasMemberAttribute { get; set; }

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000553 RID: 1363 RVA: 0x00014179 File Offset: 0x00012379
		// (set) Token: 0x06000554 RID: 1364 RVA: 0x0001418B File Offset: 0x0001238B
		public object DefaultValue
		{
			get
			{
				if (!this._hasExplicitDefaultValue)
				{
					return null;
				}
				return this._defaultValue;
			}
			set
			{
				this._hasExplicitDefaultValue = true;
				this._defaultValue = value;
			}
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x0001419B File Offset: 0x0001239B
		internal object GetResolvedDefaultValue()
		{
			if (this._propertyType == null)
			{
				return null;
			}
			if (!this._hasExplicitDefaultValue && !this._hasGeneratedDefaultValue)
			{
				this._defaultValue = ReflectionUtils.GetDefaultValue(this.PropertyType);
				this._hasGeneratedDefaultValue = true;
			}
			return this._defaultValue;
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x06000556 RID: 1366 RVA: 0x000141D8 File Offset: 0x000123D8
		// (set) Token: 0x06000557 RID: 1367 RVA: 0x000141FE File Offset: 0x000123FE
		public Required Required
		{
			get
			{
				Required? required = this._required;
				if (required == null)
				{
					return Required.Default;
				}
				return required.GetValueOrDefault();
			}
			set
			{
				this._required = new Required?(value);
			}
		}

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x0001420C File Offset: 0x0001240C
		// (set) Token: 0x06000559 RID: 1369 RVA: 0x00014214 File Offset: 0x00012414
		public bool? IsReference { get; set; }

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x0600055A RID: 1370 RVA: 0x0001421D File Offset: 0x0001241D
		// (set) Token: 0x0600055B RID: 1371 RVA: 0x00014225 File Offset: 0x00012425
		public NullValueHandling? NullValueHandling { get; set; }

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x0600055C RID: 1372 RVA: 0x0001422E File Offset: 0x0001242E
		// (set) Token: 0x0600055D RID: 1373 RVA: 0x00014236 File Offset: 0x00012436
		public DefaultValueHandling? DefaultValueHandling { get; set; }

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x0600055E RID: 1374 RVA: 0x0001423F File Offset: 0x0001243F
		// (set) Token: 0x0600055F RID: 1375 RVA: 0x00014247 File Offset: 0x00012447
		public ReferenceLoopHandling? ReferenceLoopHandling { get; set; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000560 RID: 1376 RVA: 0x00014250 File Offset: 0x00012450
		// (set) Token: 0x06000561 RID: 1377 RVA: 0x00014258 File Offset: 0x00012458
		public ObjectCreationHandling? ObjectCreationHandling { get; set; }

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000562 RID: 1378 RVA: 0x00014261 File Offset: 0x00012461
		// (set) Token: 0x06000563 RID: 1379 RVA: 0x00014269 File Offset: 0x00012469
		public TypeNameHandling? TypeNameHandling { get; set; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000564 RID: 1380 RVA: 0x00014272 File Offset: 0x00012472
		// (set) Token: 0x06000565 RID: 1381 RVA: 0x0001427A File Offset: 0x0001247A
		public Predicate<object> ShouldSerialize { get; set; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000566 RID: 1382 RVA: 0x00014283 File Offset: 0x00012483
		// (set) Token: 0x06000567 RID: 1383 RVA: 0x0001428B File Offset: 0x0001248B
		public Predicate<object> ShouldDeserialize { get; set; }

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000568 RID: 1384 RVA: 0x00014294 File Offset: 0x00012494
		// (set) Token: 0x06000569 RID: 1385 RVA: 0x0001429C File Offset: 0x0001249C
		public Predicate<object> GetIsSpecified { get; set; }

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x0600056A RID: 1386 RVA: 0x000142A5 File Offset: 0x000124A5
		// (set) Token: 0x0600056B RID: 1387 RVA: 0x000142AD File Offset: 0x000124AD
		public Action<object, object> SetIsSpecified { get; set; }

		// Token: 0x0600056C RID: 1388 RVA: 0x000142B6 File Offset: 0x000124B6
		public override string ToString()
		{
			return this.PropertyName;
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x0600056D RID: 1389 RVA: 0x000142BE File Offset: 0x000124BE
		// (set) Token: 0x0600056E RID: 1390 RVA: 0x000142C6 File Offset: 0x000124C6
		public JsonConverter ItemConverter { get; set; }

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x0600056F RID: 1391 RVA: 0x000142CF File Offset: 0x000124CF
		// (set) Token: 0x06000570 RID: 1392 RVA: 0x000142D7 File Offset: 0x000124D7
		public bool? ItemIsReference { get; set; }

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000571 RID: 1393 RVA: 0x000142E0 File Offset: 0x000124E0
		// (set) Token: 0x06000572 RID: 1394 RVA: 0x000142E8 File Offset: 0x000124E8
		public TypeNameHandling? ItemTypeNameHandling { get; set; }

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000573 RID: 1395 RVA: 0x000142F1 File Offset: 0x000124F1
		// (set) Token: 0x06000574 RID: 1396 RVA: 0x000142F9 File Offset: 0x000124F9
		public ReferenceLoopHandling? ItemReferenceLoopHandling { get; set; }

		// Token: 0x06000575 RID: 1397 RVA: 0x00014302 File Offset: 0x00012502
		internal void WritePropertyName(JsonWriter writer)
		{
			if (this._skipPropertyNameEscape)
			{
				writer.WritePropertyName(this.PropertyName, false);
				return;
			}
			writer.WritePropertyName(this.PropertyName);
		}

		// Token: 0x0400022D RID: 557
		internal Required? _required;

		// Token: 0x0400022E RID: 558
		internal bool _hasExplicitDefaultValue;

		// Token: 0x0400022F RID: 559
		private object _defaultValue;

		// Token: 0x04000230 RID: 560
		private bool _hasGeneratedDefaultValue;

		// Token: 0x04000231 RID: 561
		private string _propertyName;

		// Token: 0x04000232 RID: 562
		internal bool _skipPropertyNameEscape;

		// Token: 0x04000233 RID: 563
		private Type _propertyType;
	}
}
