﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000076 RID: 118
	[Preserve]
	public class JsonArrayContract : JsonContainerContract
	{
		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060004DA RID: 1242 RVA: 0x00013336 File Offset: 0x00011536
		// (set) Token: 0x060004DB RID: 1243 RVA: 0x0001333E File Offset: 0x0001153E
		public Type CollectionItemType { get; private set; }

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060004DC RID: 1244 RVA: 0x00013347 File Offset: 0x00011547
		// (set) Token: 0x060004DD RID: 1245 RVA: 0x0001334F File Offset: 0x0001154F
		public bool IsMultidimensionalArray { get; private set; }

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060004DE RID: 1246 RVA: 0x00013358 File Offset: 0x00011558
		// (set) Token: 0x060004DF RID: 1247 RVA: 0x00013360 File Offset: 0x00011560
		internal bool IsArray { get; private set; }

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060004E0 RID: 1248 RVA: 0x00013369 File Offset: 0x00011569
		// (set) Token: 0x060004E1 RID: 1249 RVA: 0x00013371 File Offset: 0x00011571
		internal bool ShouldCreateWrapper { get; private set; }

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060004E2 RID: 1250 RVA: 0x0001337A File Offset: 0x0001157A
		// (set) Token: 0x060004E3 RID: 1251 RVA: 0x00013382 File Offset: 0x00011582
		internal bool CanDeserialize { get; private set; }

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060004E4 RID: 1252 RVA: 0x0001338B File Offset: 0x0001158B
		internal ObjectConstructor<object> ParameterizedCreator
		{
			get
			{
				if (this._parameterizedCreator == null)
				{
					this._parameterizedCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(this._parameterizedConstructor);
				}
				return this._parameterizedCreator;
			}
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x000133B1 File Offset: 0x000115B1
		// (set) Token: 0x060004E6 RID: 1254 RVA: 0x000133B9 File Offset: 0x000115B9
		public ObjectConstructor<object> OverrideCreator
		{
			get
			{
				return this._overrideCreator;
			}
			set
			{
				this._overrideCreator = value;
				this.CanDeserialize = true;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x000133C9 File Offset: 0x000115C9
		// (set) Token: 0x060004E8 RID: 1256 RVA: 0x000133D1 File Offset: 0x000115D1
		public bool HasParameterizedCreator { get; set; }

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x000133DA File Offset: 0x000115DA
		internal bool HasParameterizedCreatorInternal
		{
			get
			{
				return this.HasParameterizedCreator || this._parameterizedCreator != null || this._parameterizedConstructor != null;
			}
		}

		// Token: 0x060004EA RID: 1258 RVA: 0x000133F8 File Offset: 0x000115F8
		public JsonArrayContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.Array;
			this.IsArray = base.CreatedType.IsArray;
			bool canDeserialize;
			Type type;
			if (this.IsArray)
			{
				this.CollectionItemType = ReflectionUtils.GetCollectionItemType(base.UnderlyingType);
				this.IsReadOnlyOrFixedSize = true;
				this._genericCollectionDefinitionType = typeof(List<>).MakeGenericType(new Type[]
				{
					this.CollectionItemType
				});
				canDeserialize = true;
				this.IsMultidimensionalArray = (this.IsArray && base.UnderlyingType.GetArrayRank() > 1);
			}
			else if (typeof(IList).IsAssignableFrom(underlyingType))
			{
				if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(ICollection<>), out this._genericCollectionDefinitionType))
				{
					this.CollectionItemType = this._genericCollectionDefinitionType.GetGenericArguments()[0];
				}
				else
				{
					this.CollectionItemType = ReflectionUtils.GetCollectionItemType(underlyingType);
				}
				if (underlyingType == typeof(IList))
				{
					base.CreatedType = typeof(List<object>);
				}
				if (this.CollectionItemType != null)
				{
					this._parameterizedConstructor = CollectionUtils.ResolveEnumerableCollectionConstructor(underlyingType, this.CollectionItemType);
				}
				this.IsReadOnlyOrFixedSize = ReflectionUtils.InheritsGenericDefinition(underlyingType, typeof(ReadOnlyCollection<>));
				canDeserialize = true;
			}
			else if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(ICollection<>), out this._genericCollectionDefinitionType))
			{
				this.CollectionItemType = this._genericCollectionDefinitionType.GetGenericArguments()[0];
				if (ReflectionUtils.IsGenericDefinition(underlyingType, typeof(ICollection<>)) || ReflectionUtils.IsGenericDefinition(underlyingType, typeof(IList<>)))
				{
					base.CreatedType = typeof(List<>).MakeGenericType(new Type[]
					{
						this.CollectionItemType
					});
				}
				this._parameterizedConstructor = CollectionUtils.ResolveEnumerableCollectionConstructor(underlyingType, this.CollectionItemType);
				canDeserialize = true;
				this.ShouldCreateWrapper = true;
			}
			else if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(IEnumerable<>), out type))
			{
				this.CollectionItemType = type.GetGenericArguments()[0];
				if (ReflectionUtils.IsGenericDefinition(base.UnderlyingType, typeof(IEnumerable<>)))
				{
					base.CreatedType = typeof(List<>).MakeGenericType(new Type[]
					{
						this.CollectionItemType
					});
				}
				this._parameterizedConstructor = CollectionUtils.ResolveEnumerableCollectionConstructor(underlyingType, this.CollectionItemType);
				if (underlyingType.IsGenericType() && underlyingType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
				{
					this._genericCollectionDefinitionType = type;
					this.IsReadOnlyOrFixedSize = false;
					this.ShouldCreateWrapper = false;
					canDeserialize = true;
				}
				else
				{
					this._genericCollectionDefinitionType = typeof(List<>).MakeGenericType(new Type[]
					{
						this.CollectionItemType
					});
					this.IsReadOnlyOrFixedSize = true;
					this.ShouldCreateWrapper = true;
					canDeserialize = this.HasParameterizedCreatorInternal;
				}
			}
			else
			{
				canDeserialize = false;
				this.ShouldCreateWrapper = true;
			}
			this.CanDeserialize = canDeserialize;
			if (this.CollectionItemType != null && ReflectionUtils.IsNullableType(this.CollectionItemType) && (ReflectionUtils.InheritsGenericDefinition(base.CreatedType, typeof(List<>), out type) || (this.IsArray && !this.IsMultidimensionalArray)))
			{
				this.ShouldCreateWrapper = true;
			}
		}

		// Token: 0x060004EB RID: 1259 RVA: 0x000136F8 File Offset: 0x000118F8
		internal IWrappedCollection CreateWrapper(object list)
		{
			if (this._genericWrapperCreator == null)
			{
				this._genericWrapperType = typeof(CollectionWrapper<>).MakeGenericType(new Type[]
				{
					this.CollectionItemType
				});
				Type type;
				if (ReflectionUtils.InheritsGenericDefinition(this._genericCollectionDefinitionType, typeof(List<>)) || this._genericCollectionDefinitionType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
				{
					type = typeof(ICollection<>).MakeGenericType(new Type[]
					{
						this.CollectionItemType
					});
				}
				else
				{
					type = this._genericCollectionDefinitionType;
				}
				ConstructorInfo constructor = this._genericWrapperType.GetConstructor(new Type[]
				{
					type
				});
				this._genericWrapperCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(constructor);
			}
			return (IWrappedCollection)this._genericWrapperCreator(new object[]
			{
				list
			});
		}

		// Token: 0x060004EC RID: 1260 RVA: 0x000137CC File Offset: 0x000119CC
		internal IList CreateTemporaryCollection()
		{
			if (this._genericTemporaryCollectionCreator == null)
			{
				Type type = (this.IsMultidimensionalArray || this.CollectionItemType == null) ? typeof(object) : this.CollectionItemType;
				Type type2 = typeof(List<>).MakeGenericType(new Type[]
				{
					type
				});
				this._genericTemporaryCollectionCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateDefaultConstructor<object>(type2);
			}
			return (IList)this._genericTemporaryCollectionCreator();
		}

		// Token: 0x040001F6 RID: 502
		private readonly Type _genericCollectionDefinitionType;

		// Token: 0x040001F7 RID: 503
		private Type _genericWrapperType;

		// Token: 0x040001F8 RID: 504
		private ObjectConstructor<object> _genericWrapperCreator;

		// Token: 0x040001F9 RID: 505
		private Func<object> _genericTemporaryCollectionCreator;

		// Token: 0x040001FD RID: 509
		private readonly ConstructorInfo _parameterizedConstructor;

		// Token: 0x040001FE RID: 510
		private ObjectConstructor<object> _parameterizedCreator;

		// Token: 0x040001FF RID: 511
		private ObjectConstructor<object> _overrideCreator;
	}
}
