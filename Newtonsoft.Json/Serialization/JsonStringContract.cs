﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000086 RID: 134
	[Preserve]
	public class JsonStringContract : JsonPrimitiveContract
	{
		// Token: 0x06000631 RID: 1585 RVA: 0x00019A97 File Offset: 0x00017C97
		public JsonStringContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.String;
		}
	}
}
