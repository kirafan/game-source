﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000061 RID: 97
	[Preserve]
	public interface IAttributeProvider
	{
		// Token: 0x0600040D RID: 1037
		IList<Attribute> GetAttributes(bool inherit);

		// Token: 0x0600040E RID: 1038
		IList<Attribute> GetAttributes(Type attributeType, bool inherit);
	}
}
