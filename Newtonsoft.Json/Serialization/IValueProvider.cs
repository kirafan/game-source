﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000075 RID: 117
	[Preserve]
	public interface IValueProvider
	{
		// Token: 0x060004D8 RID: 1240
		void SetValue(object target, object value);

		// Token: 0x060004D9 RID: 1241
		object GetValue(object target);
	}
}
