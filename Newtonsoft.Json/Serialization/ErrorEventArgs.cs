﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200006C RID: 108
	[Preserve]
	public class ErrorEventArgs : EventArgs
	{
		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600047F RID: 1151 RVA: 0x000116F2 File Offset: 0x0000F8F2
		// (set) Token: 0x06000480 RID: 1152 RVA: 0x000116FA File Offset: 0x0000F8FA
		public object CurrentObject { get; private set; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000481 RID: 1153 RVA: 0x00011703 File Offset: 0x0000F903
		// (set) Token: 0x06000482 RID: 1154 RVA: 0x0001170B File Offset: 0x0000F90B
		public ErrorContext ErrorContext { get; private set; }

		// Token: 0x06000483 RID: 1155 RVA: 0x00011714 File Offset: 0x0000F914
		public ErrorEventArgs(object currentObject, ErrorContext errorContext)
		{
			this.CurrentObject = currentObject;
			this.ErrorContext = errorContext;
		}
	}
}
