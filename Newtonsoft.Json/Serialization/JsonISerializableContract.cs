﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000069 RID: 105
	[Preserve]
	public class JsonISerializableContract : JsonContainerContract
	{
		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000477 RID: 1143 RVA: 0x0001156C File Offset: 0x0000F76C
		// (set) Token: 0x06000478 RID: 1144 RVA: 0x00011574 File Offset: 0x0000F774
		public ObjectConstructor<object> ISerializableCreator { get; set; }

		// Token: 0x06000479 RID: 1145 RVA: 0x0001157D File Offset: 0x0000F77D
		public JsonISerializableContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.Serializable;
		}
	}
}
