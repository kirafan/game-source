﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200008B RID: 139
	// (Invoke) Token: 0x0600064A RID: 1610
	[Preserve]
	public delegate object ObjectConstructor<T>(params object[] args);
}
