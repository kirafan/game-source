﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000070 RID: 112
	[Preserve]
	internal class DefaultContractResolverState
	{
		// Token: 0x040001E0 RID: 480
		public Dictionary<ResolverContractKey, JsonContract> ContractCache;

		// Token: 0x040001E1 RID: 481
		public PropertyNameTable NameTable = new PropertyNameTable();
	}
}
