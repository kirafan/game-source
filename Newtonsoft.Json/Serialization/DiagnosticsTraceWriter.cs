﻿using System;
using System.Diagnostics;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000060 RID: 96
	[Preserve]
	public class DiagnosticsTraceWriter : ITraceWriter
	{
		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000408 RID: 1032 RVA: 0x000108B0 File Offset: 0x0000EAB0
		// (set) Token: 0x06000409 RID: 1033 RVA: 0x000108B8 File Offset: 0x0000EAB8
		public TraceLevel LevelFilter { get; set; }

		// Token: 0x0600040A RID: 1034 RVA: 0x000108C1 File Offset: 0x0000EAC1
		private TraceEventType GetTraceEventType(TraceLevel level)
		{
			switch (level)
			{
			case TraceLevel.Error:
				return TraceEventType.Error;
			case TraceLevel.Warning:
				return TraceEventType.Warning;
			case TraceLevel.Info:
				return TraceEventType.Information;
			case TraceLevel.Verbose:
				return TraceEventType.Verbose;
			default:
				throw new ArgumentOutOfRangeException("level");
			}
		}

		// Token: 0x0600040B RID: 1035 RVA: 0x000108F0 File Offset: 0x0000EAF0
		public void Trace(TraceLevel level, string message, Exception ex)
		{
			if (level == TraceLevel.Off)
			{
				return;
			}
			TraceEventCache eventCache = new TraceEventCache();
			TraceEventType traceEventType = this.GetTraceEventType(level);
			foreach (object obj in System.Diagnostics.Trace.Listeners)
			{
				TraceListener traceListener = (TraceListener)obj;
				if (!traceListener.IsThreadSafe)
				{
					TraceListener obj2 = traceListener;
					lock (obj2)
					{
						traceListener.TraceEvent(eventCache, "Newtonsoft.Json", traceEventType, 0, message);
						goto IL_65;
					}
					goto IL_56;
				}
				goto IL_56;
				IL_65:
				if (System.Diagnostics.Trace.AutoFlush)
				{
					traceListener.Flush();
					continue;
				}
				continue;
				IL_56:
				traceListener.TraceEvent(eventCache, "Newtonsoft.Json", traceEventType, 0, message);
				goto IL_65;
			}
		}
	}
}
