﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000078 RID: 120
	// (Invoke) Token: 0x060004EE RID: 1262
	[Preserve]
	public delegate void SerializationCallback(object o, StreamingContext context);
}
