﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000073 RID: 115
	[Preserve]
	public class ErrorContext
	{
		// Token: 0x060004CA RID: 1226 RVA: 0x000132AB File Offset: 0x000114AB
		internal ErrorContext(object originalObject, object member, string path, Exception error)
		{
			this.OriginalObject = originalObject;
			this.Member = member;
			this.Error = error;
			this.Path = path;
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060004CB RID: 1227 RVA: 0x000132D0 File Offset: 0x000114D0
		// (set) Token: 0x060004CC RID: 1228 RVA: 0x000132D8 File Offset: 0x000114D8
		internal bool Traced { get; set; }

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060004CD RID: 1229 RVA: 0x000132E1 File Offset: 0x000114E1
		// (set) Token: 0x060004CE RID: 1230 RVA: 0x000132E9 File Offset: 0x000114E9
		public Exception Error { get; private set; }

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060004CF RID: 1231 RVA: 0x000132F2 File Offset: 0x000114F2
		// (set) Token: 0x060004D0 RID: 1232 RVA: 0x000132FA File Offset: 0x000114FA
		public object OriginalObject { get; private set; }

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x00013303 File Offset: 0x00011503
		// (set) Token: 0x060004D2 RID: 1234 RVA: 0x0001330B File Offset: 0x0001150B
		public object Member { get; private set; }

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x00013314 File Offset: 0x00011514
		// (set) Token: 0x060004D4 RID: 1236 RVA: 0x0001331C File Offset: 0x0001151C
		public string Path { get; private set; }

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x00013325 File Offset: 0x00011525
		// (set) Token: 0x060004D6 RID: 1238 RVA: 0x0001332D File Offset: 0x0001152D
		public bool Handled { get; set; }
	}
}
