﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200006A RID: 106
	[Preserve]
	public class JsonLinqContract : JsonContract
	{
		// Token: 0x0600047A RID: 1146 RVA: 0x0001158D File Offset: 0x0000F78D
		public JsonLinqContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.Linq;
		}
	}
}
