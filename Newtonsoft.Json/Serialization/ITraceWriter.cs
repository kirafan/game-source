﻿using System;
using System.Diagnostics;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000062 RID: 98
	[Preserve]
	public interface ITraceWriter
	{
		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x0600040F RID: 1039
		TraceLevel LevelFilter { get; }

		// Token: 0x06000410 RID: 1040
		void Trace(TraceLevel level, string message, Exception ex);
	}
}
