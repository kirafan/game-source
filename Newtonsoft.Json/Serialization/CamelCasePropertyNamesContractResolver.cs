﻿using System;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200006E RID: 110
	[Preserve]
	public class CamelCasePropertyNamesContractResolver : DefaultContractResolver
	{
		// Token: 0x0600048A RID: 1162 RVA: 0x00011808 File Offset: 0x0000FA08
		public CamelCasePropertyNamesContractResolver() : base(true)
		{
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x00011811 File Offset: 0x0000FA11
		protected override string ResolvePropertyName(string propertyName)
		{
			return StringUtils.ToCamelCase(propertyName);
		}
	}
}
