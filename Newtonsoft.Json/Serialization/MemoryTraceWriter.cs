﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000064 RID: 100
	[Preserve]
	public class MemoryTraceWriter : ITraceWriter
	{
		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600041D RID: 1053 RVA: 0x00010A9D File Offset: 0x0000EC9D
		// (set) Token: 0x0600041E RID: 1054 RVA: 0x00010AA5 File Offset: 0x0000ECA5
		public TraceLevel LevelFilter { get; set; }

		// Token: 0x0600041F RID: 1055 RVA: 0x00010AAE File Offset: 0x0000ECAE
		public MemoryTraceWriter()
		{
			this.LevelFilter = TraceLevel.Verbose;
			this._traceMessages = new Queue<string>();
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00010AC8 File Offset: 0x0000ECC8
		public void Trace(TraceLevel level, string message, Exception ex)
		{
			if (this._traceMessages.Count >= 1000)
			{
				this._traceMessages.Dequeue();
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff", CultureInfo.InvariantCulture));
			stringBuilder.Append(" ");
			stringBuilder.Append(level.ToString("g"));
			stringBuilder.Append(" ");
			stringBuilder.Append(message);
			this._traceMessages.Enqueue(stringBuilder.ToString());
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00010B5F File Offset: 0x0000ED5F
		public IEnumerable<string> GetTraceMessages()
		{
			return this._traceMessages;
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00010B68 File Offset: 0x0000ED68
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (string value in this._traceMessages)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.AppendLine();
				}
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040001CC RID: 460
		private readonly Queue<string> _traceMessages;
	}
}
