﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000080 RID: 128
	[Preserve]
	public interface IReferenceResolver
	{
		// Token: 0x0600057D RID: 1405
		object ResolveReference(object context, string reference);

		// Token: 0x0600057E RID: 1406
		string GetReference(object context, object value);

		// Token: 0x0600057F RID: 1407
		bool IsReferenced(object context, object value);

		// Token: 0x06000580 RID: 1408
		void AddReference(object context, string reference, object value);
	}
}
