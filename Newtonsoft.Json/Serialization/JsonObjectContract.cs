﻿using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000081 RID: 129
	[Preserve]
	public class JsonObjectContract : JsonContainerContract
	{
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x000144F1 File Offset: 0x000126F1
		// (set) Token: 0x06000582 RID: 1410 RVA: 0x000144F9 File Offset: 0x000126F9
		public MemberSerialization MemberSerialization { get; set; }

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x00014502 File Offset: 0x00012702
		// (set) Token: 0x06000584 RID: 1412 RVA: 0x0001450A File Offset: 0x0001270A
		public Required? ItemRequired { get; set; }

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x06000585 RID: 1413 RVA: 0x00014513 File Offset: 0x00012713
		// (set) Token: 0x06000586 RID: 1414 RVA: 0x0001451B File Offset: 0x0001271B
		public JsonPropertyCollection Properties { get; private set; }

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x06000587 RID: 1415 RVA: 0x00014524 File Offset: 0x00012724
		[Obsolete("ConstructorParameters is obsolete. Use CreatorParameters instead.")]
		public JsonPropertyCollection ConstructorParameters
		{
			get
			{
				return this.CreatorParameters;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x0001452C File Offset: 0x0001272C
		public JsonPropertyCollection CreatorParameters
		{
			get
			{
				if (this._creatorParameters == null)
				{
					this._creatorParameters = new JsonPropertyCollection(base.UnderlyingType);
				}
				return this._creatorParameters;
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x0001454D File Offset: 0x0001274D
		// (set) Token: 0x0600058A RID: 1418 RVA: 0x00014555 File Offset: 0x00012755
		[Obsolete("OverrideConstructor is obsolete. Use OverrideCreator instead.")]
		public ConstructorInfo OverrideConstructor
		{
			get
			{
				return this._overrideConstructor;
			}
			set
			{
				this._overrideConstructor = value;
				this._overrideCreator = ((value != null) ? JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(value) : null);
			}
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x0600058B RID: 1419 RVA: 0x00014575 File Offset: 0x00012775
		// (set) Token: 0x0600058C RID: 1420 RVA: 0x0001457D File Offset: 0x0001277D
		[Obsolete("ParametrizedConstructor is obsolete. Use OverrideCreator instead.")]
		public ConstructorInfo ParametrizedConstructor
		{
			get
			{
				return this._parametrizedConstructor;
			}
			set
			{
				this._parametrizedConstructor = value;
				this._parameterizedCreator = ((value != null) ? JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(value) : null);
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x0001459D File Offset: 0x0001279D
		// (set) Token: 0x0600058E RID: 1422 RVA: 0x000145A5 File Offset: 0x000127A5
		public ObjectConstructor<object> OverrideCreator
		{
			get
			{
				return this._overrideCreator;
			}
			set
			{
				this._overrideCreator = value;
				this._overrideConstructor = null;
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x0600058F RID: 1423 RVA: 0x000145B5 File Offset: 0x000127B5
		internal ObjectConstructor<object> ParameterizedCreator
		{
			get
			{
				return this._parameterizedCreator;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000590 RID: 1424 RVA: 0x000145BD File Offset: 0x000127BD
		// (set) Token: 0x06000591 RID: 1425 RVA: 0x000145C5 File Offset: 0x000127C5
		public ExtensionDataSetter ExtensionDataSetter { get; set; }

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000592 RID: 1426 RVA: 0x000145CE File Offset: 0x000127CE
		// (set) Token: 0x06000593 RID: 1427 RVA: 0x000145D6 File Offset: 0x000127D6
		public ExtensionDataGetter ExtensionDataGetter { get; set; }

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000594 RID: 1428 RVA: 0x000145DF File Offset: 0x000127DF
		// (set) Token: 0x06000595 RID: 1429 RVA: 0x000145E7 File Offset: 0x000127E7
		public Type ExtensionDataValueType
		{
			get
			{
				return this._extensionDataValueType;
			}
			set
			{
				this._extensionDataValueType = value;
				this.ExtensionDataIsJToken = (value != null && typeof(JToken).IsAssignableFrom(value));
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000596 RID: 1430 RVA: 0x0001460C File Offset: 0x0001280C
		internal bool HasRequiredOrDefaultValueProperties
		{
			get
			{
				if (this._hasRequiredOrDefaultValueProperties == null)
				{
					this._hasRequiredOrDefaultValueProperties = new bool?(false);
					if (this.ItemRequired.GetValueOrDefault(Required.Default) != Required.Default)
					{
						this._hasRequiredOrDefaultValueProperties = new bool?(true);
					}
					else
					{
						foreach (JsonProperty jsonProperty in this.Properties)
						{
							if (jsonProperty.Required != Required.Default || (jsonProperty.DefaultValueHandling & DefaultValueHandling.Populate) == DefaultValueHandling.Populate)
							{
								this._hasRequiredOrDefaultValueProperties = new bool?(true);
								break;
							}
						}
					}
				}
				return this._hasRequiredOrDefaultValueProperties.GetValueOrDefault();
			}
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x000146F8 File Offset: 0x000128F8
		public JsonObjectContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.Object;
			this.Properties = new JsonPropertyCollection(base.UnderlyingType);
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x00014719 File Offset: 0x00012919
		internal object GetUninitializedObject()
		{
			if (!JsonTypeReflector.FullyTrusted)
			{
				throw new JsonException("Insufficient permissions. Creating an uninitialized '{0}' type requires full trust.".FormatWith(CultureInfo.InvariantCulture, this.NonNullableUnderlyingType));
			}
			return FormatterServices.GetUninitializedObject(this.NonNullableUnderlyingType);
		}

		// Token: 0x04000255 RID: 597
		internal bool ExtensionDataIsJToken;

		// Token: 0x04000256 RID: 598
		private bool? _hasRequiredOrDefaultValueProperties;

		// Token: 0x04000257 RID: 599
		private ConstructorInfo _parametrizedConstructor;

		// Token: 0x04000258 RID: 600
		private ConstructorInfo _overrideConstructor;

		// Token: 0x04000259 RID: 601
		private ObjectConstructor<object> _overrideCreator;

		// Token: 0x0400025A RID: 602
		private ObjectConstructor<object> _parameterizedCreator;

		// Token: 0x0400025B RID: 603
		private JsonPropertyCollection _creatorParameters;

		// Token: 0x0400025C RID: 604
		private Type _extensionDataValueType;
	}
}
