﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200007D RID: 125
	[Preserve]
	public class JsonDictionaryContract : JsonContainerContract
	{
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000522 RID: 1314 RVA: 0x00013D22 File Offset: 0x00011F22
		// (set) Token: 0x06000523 RID: 1315 RVA: 0x00013D2A File Offset: 0x00011F2A
		[Obsolete("PropertyNameResolver is obsolete. Use DictionaryKeyResolver instead.")]
		public Func<string, string> PropertyNameResolver
		{
			get
			{
				return this.DictionaryKeyResolver;
			}
			set
			{
				this.DictionaryKeyResolver = value;
			}
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000524 RID: 1316 RVA: 0x00013D33 File Offset: 0x00011F33
		// (set) Token: 0x06000525 RID: 1317 RVA: 0x00013D3B File Offset: 0x00011F3B
		public Func<string, string> DictionaryKeyResolver { get; set; }

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000526 RID: 1318 RVA: 0x00013D44 File Offset: 0x00011F44
		// (set) Token: 0x06000527 RID: 1319 RVA: 0x00013D4C File Offset: 0x00011F4C
		public Type DictionaryKeyType { get; private set; }

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000528 RID: 1320 RVA: 0x00013D55 File Offset: 0x00011F55
		// (set) Token: 0x06000529 RID: 1321 RVA: 0x00013D5D File Offset: 0x00011F5D
		public Type DictionaryValueType { get; private set; }

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x0600052A RID: 1322 RVA: 0x00013D66 File Offset: 0x00011F66
		// (set) Token: 0x0600052B RID: 1323 RVA: 0x00013D6E File Offset: 0x00011F6E
		internal JsonContract KeyContract { get; set; }

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x0600052C RID: 1324 RVA: 0x00013D77 File Offset: 0x00011F77
		// (set) Token: 0x0600052D RID: 1325 RVA: 0x00013D7F File Offset: 0x00011F7F
		internal bool ShouldCreateWrapper { get; private set; }

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x0600052E RID: 1326 RVA: 0x00013D88 File Offset: 0x00011F88
		internal ObjectConstructor<object> ParameterizedCreator
		{
			get
			{
				if (this._parameterizedCreator == null)
				{
					this._parameterizedCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(this._parameterizedConstructor);
				}
				return this._parameterizedCreator;
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x0600052F RID: 1327 RVA: 0x00013DAE File Offset: 0x00011FAE
		// (set) Token: 0x06000530 RID: 1328 RVA: 0x00013DB6 File Offset: 0x00011FB6
		public ObjectConstructor<object> OverrideCreator
		{
			get
			{
				return this._overrideCreator;
			}
			set
			{
				this._overrideCreator = value;
			}
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000531 RID: 1329 RVA: 0x00013DBF File Offset: 0x00011FBF
		// (set) Token: 0x06000532 RID: 1330 RVA: 0x00013DC7 File Offset: 0x00011FC7
		public bool HasParameterizedCreator { get; set; }

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000533 RID: 1331 RVA: 0x00013DD0 File Offset: 0x00011FD0
		internal bool HasParameterizedCreatorInternal
		{
			get
			{
				return this.HasParameterizedCreator || this._parameterizedCreator != null || this._parameterizedConstructor != null;
			}
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x00013DF0 File Offset: 0x00011FF0
		public JsonDictionaryContract(Type underlyingType) : base(underlyingType)
		{
			this.ContractType = JsonContractType.Dictionary;
			Type type;
			Type type2;
			if (ReflectionUtils.ImplementsGenericDefinition(underlyingType, typeof(IDictionary<, >), out this._genericCollectionDefinitionType))
			{
				type = this._genericCollectionDefinitionType.GetGenericArguments()[0];
				type2 = this._genericCollectionDefinitionType.GetGenericArguments()[1];
				if (ReflectionUtils.IsGenericDefinition(base.UnderlyingType, typeof(IDictionary<, >)))
				{
					base.CreatedType = typeof(Dictionary<, >).MakeGenericType(new Type[]
					{
						type,
						type2
					});
				}
			}
			else
			{
				ReflectionUtils.GetDictionaryKeyValueTypes(base.UnderlyingType, out type, out type2);
				if (base.UnderlyingType == typeof(IDictionary))
				{
					base.CreatedType = typeof(Dictionary<object, object>);
				}
			}
			if (type != null && type2 != null)
			{
				this._parameterizedConstructor = CollectionUtils.ResolveEnumerableCollectionConstructor(base.CreatedType, typeof(KeyValuePair<, >).MakeGenericType(new Type[]
				{
					type,
					type2
				}), typeof(IDictionary<, >).MakeGenericType(new Type[]
				{
					type,
					type2
				}));
			}
			this.ShouldCreateWrapper = !typeof(IDictionary).IsAssignableFrom(base.CreatedType);
			this.DictionaryKeyType = type;
			this.DictionaryValueType = type2;
			Type type3;
			if (this.DictionaryValueType != null && ReflectionUtils.IsNullableType(this.DictionaryValueType) && ReflectionUtils.InheritsGenericDefinition(base.CreatedType, typeof(Dictionary<, >), out type3))
			{
				this.ShouldCreateWrapper = true;
			}
		}

		// Token: 0x06000535 RID: 1333 RVA: 0x00013F60 File Offset: 0x00012160
		internal IWrappedDictionary CreateWrapper(object dictionary)
		{
			if (this._genericWrapperCreator == null)
			{
				this._genericWrapperType = typeof(DictionaryWrapper<, >).MakeGenericType(new Type[]
				{
					this.DictionaryKeyType,
					this.DictionaryValueType
				});
				ConstructorInfo constructor = this._genericWrapperType.GetConstructor(new Type[]
				{
					this._genericCollectionDefinitionType
				});
				this._genericWrapperCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateParameterizedConstructor(constructor);
			}
			return (IWrappedDictionary)this._genericWrapperCreator(new object[]
			{
				dictionary
			});
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x00013FE8 File Offset: 0x000121E8
		internal IDictionary CreateTemporaryDictionary()
		{
			if (this._genericTemporaryDictionaryCreator == null)
			{
				Type type = typeof(Dictionary<, >).MakeGenericType(new Type[]
				{
					this.DictionaryKeyType ?? typeof(object),
					this.DictionaryValueType ?? typeof(object)
				});
				this._genericTemporaryDictionaryCreator = JsonTypeReflector.ReflectionDelegateFactory.CreateDefaultConstructor<object>(type);
			}
			return (IDictionary)this._genericTemporaryDictionaryCreator();
		}

		// Token: 0x04000224 RID: 548
		private readonly Type _genericCollectionDefinitionType;

		// Token: 0x04000225 RID: 549
		private Type _genericWrapperType;

		// Token: 0x04000226 RID: 550
		private ObjectConstructor<object> _genericWrapperCreator;

		// Token: 0x04000227 RID: 551
		private Func<object> _genericTemporaryDictionaryCreator;

		// Token: 0x04000229 RID: 553
		private readonly ConstructorInfo _parameterizedConstructor;

		// Token: 0x0400022A RID: 554
		private ObjectConstructor<object> _overrideCreator;

		// Token: 0x0400022B RID: 555
		private ObjectConstructor<object> _parameterizedCreator;
	}
}
