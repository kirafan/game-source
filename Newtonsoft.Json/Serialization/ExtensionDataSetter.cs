﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200007A RID: 122
	// (Invoke) Token: 0x060004F6 RID: 1270
	[Preserve]
	public delegate void ExtensionDataSetter(object o, string key, object value);
}
