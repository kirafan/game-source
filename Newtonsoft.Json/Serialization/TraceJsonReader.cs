﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000066 RID: 102
	[Preserve]
	internal class TraceJsonReader : JsonReader, IJsonLineInfo
	{
		// Token: 0x06000426 RID: 1062 RVA: 0x00010C10 File Offset: 0x0000EE10
		public TraceJsonReader(JsonReader innerReader)
		{
			this._innerReader = innerReader;
			this._sw = new StringWriter(CultureInfo.InvariantCulture);
			this._sw.Write("Deserialized JSON: " + Environment.NewLine);
			this._textWriter = new JsonTextWriter(this._sw);
			this._textWriter.Formatting = Formatting.Indented;
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x00010C71 File Offset: 0x0000EE71
		public string GetDeserializedJsonMessage()
		{
			return this._sw.ToString();
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x00010C7E File Offset: 0x0000EE7E
		public override bool Read()
		{
			bool result = this._innerReader.Read();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x00010C9F File Offset: 0x0000EE9F
		public override int? ReadAsInt32()
		{
			int? result = this._innerReader.ReadAsInt32();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x00010CC0 File Offset: 0x0000EEC0
		public override string ReadAsString()
		{
			string result = this._innerReader.ReadAsString();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x00010CE1 File Offset: 0x0000EEE1
		public override byte[] ReadAsBytes()
		{
			byte[] result = this._innerReader.ReadAsBytes();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x00010D02 File Offset: 0x0000EF02
		public override decimal? ReadAsDecimal()
		{
			decimal? result = this._innerReader.ReadAsDecimal();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x00010D23 File Offset: 0x0000EF23
		public override double? ReadAsDouble()
		{
			double? result = this._innerReader.ReadAsDouble();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00010D44 File Offset: 0x0000EF44
		public override bool? ReadAsBoolean()
		{
			bool? result = this._innerReader.ReadAsBoolean();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x00010D65 File Offset: 0x0000EF65
		public override DateTime? ReadAsDateTime()
		{
			DateTime? result = this._innerReader.ReadAsDateTime();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x00010D86 File Offset: 0x0000EF86
		public override DateTimeOffset? ReadAsDateTimeOffset()
		{
			DateTimeOffset? result = this._innerReader.ReadAsDateTimeOffset();
			this._textWriter.WriteToken(this._innerReader, false, false, true);
			return result;
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000431 RID: 1073 RVA: 0x00010DA7 File Offset: 0x0000EFA7
		public override int Depth
		{
			get
			{
				return this._innerReader.Depth;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x00010DB4 File Offset: 0x0000EFB4
		public override string Path
		{
			get
			{
				return this._innerReader.Path;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000433 RID: 1075 RVA: 0x00010DC1 File Offset: 0x0000EFC1
		// (set) Token: 0x06000434 RID: 1076 RVA: 0x00010DCE File Offset: 0x0000EFCE
		public override char QuoteChar
		{
			get
			{
				return this._innerReader.QuoteChar;
			}
			protected internal set
			{
				this._innerReader.QuoteChar = value;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000435 RID: 1077 RVA: 0x00010DDC File Offset: 0x0000EFDC
		public override JsonToken TokenType
		{
			get
			{
				return this._innerReader.TokenType;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x00010DE9 File Offset: 0x0000EFE9
		public override object Value
		{
			get
			{
				return this._innerReader.Value;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000437 RID: 1079 RVA: 0x00010DF6 File Offset: 0x0000EFF6
		public override Type ValueType
		{
			get
			{
				return this._innerReader.ValueType;
			}
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x00010E03 File Offset: 0x0000F003
		public override void Close()
		{
			this._innerReader.Close();
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x00010E10 File Offset: 0x0000F010
		bool IJsonLineInfo.HasLineInfo()
		{
			IJsonLineInfo jsonLineInfo = this._innerReader as IJsonLineInfo;
			return jsonLineInfo != null && jsonLineInfo.HasLineInfo();
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x00010E34 File Offset: 0x0000F034
		int IJsonLineInfo.LineNumber
		{
			get
			{
				IJsonLineInfo jsonLineInfo = this._innerReader as IJsonLineInfo;
				if (jsonLineInfo == null)
				{
					return 0;
				}
				return jsonLineInfo.LineNumber;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x0600043B RID: 1083 RVA: 0x00010E58 File Offset: 0x0000F058
		int IJsonLineInfo.LinePosition
		{
			get
			{
				IJsonLineInfo jsonLineInfo = this._innerReader as IJsonLineInfo;
				if (jsonLineInfo == null)
				{
					return 0;
				}
				return jsonLineInfo.LinePosition;
			}
		}

		// Token: 0x040001CF RID: 463
		private readonly JsonReader _innerReader;

		// Token: 0x040001D0 RID: 464
		private readonly JsonTextWriter _textWriter;

		// Token: 0x040001D1 RID: 465
		private readonly StringWriter _sw;
	}
}
