﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000065 RID: 101
	[Preserve]
	public class ReflectionAttributeProvider : IAttributeProvider
	{
		// Token: 0x06000423 RID: 1059 RVA: 0x00010BD8 File Offset: 0x0000EDD8
		public ReflectionAttributeProvider(object attributeProvider)
		{
			ValidationUtils.ArgumentNotNull(attributeProvider, "attributeProvider");
			this._attributeProvider = attributeProvider;
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x00010BF2 File Offset: 0x0000EDF2
		public IList<Attribute> GetAttributes(bool inherit)
		{
			return ReflectionUtils.GetAttributes(this._attributeProvider, null, inherit);
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x00010C01 File Offset: 0x0000EE01
		public IList<Attribute> GetAttributes(Type attributeType, bool inherit)
		{
			return ReflectionUtils.GetAttributes(this._attributeProvider, attributeType, inherit);
		}

		// Token: 0x040001CE RID: 462
		private readonly object _attributeProvider;
	}
}
