﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200007B RID: 123
	// (Invoke) Token: 0x060004FA RID: 1274
	[Preserve]
	public delegate IEnumerable<KeyValuePair<object, object>> ExtensionDataGetter(object o);
}
