﻿using System;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000063 RID: 99
	[Preserve]
	public class JsonContainerContract : JsonContract
	{
		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000411 RID: 1041 RVA: 0x000109AC File Offset: 0x0000EBAC
		// (set) Token: 0x06000412 RID: 1042 RVA: 0x000109B4 File Offset: 0x0000EBB4
		internal JsonContract ItemContract
		{
			get
			{
				return this._itemContract;
			}
			set
			{
				this._itemContract = value;
				if (this._itemContract != null)
				{
					this._finalItemContract = (this._itemContract.UnderlyingType.IsSealed() ? this._itemContract : null);
					return;
				}
				this._finalItemContract = null;
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000413 RID: 1043 RVA: 0x000109EE File Offset: 0x0000EBEE
		internal JsonContract FinalItemContract
		{
			get
			{
				return this._finalItemContract;
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x000109F6 File Offset: 0x0000EBF6
		// (set) Token: 0x06000415 RID: 1045 RVA: 0x000109FE File Offset: 0x0000EBFE
		public JsonConverter ItemConverter { get; set; }

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000416 RID: 1046 RVA: 0x00010A07 File Offset: 0x0000EC07
		// (set) Token: 0x06000417 RID: 1047 RVA: 0x00010A0F File Offset: 0x0000EC0F
		public bool? ItemIsReference { get; set; }

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000418 RID: 1048 RVA: 0x00010A18 File Offset: 0x0000EC18
		// (set) Token: 0x06000419 RID: 1049 RVA: 0x00010A20 File Offset: 0x0000EC20
		public ReferenceLoopHandling? ItemReferenceLoopHandling { get; set; }

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600041A RID: 1050 RVA: 0x00010A29 File Offset: 0x0000EC29
		// (set) Token: 0x0600041B RID: 1051 RVA: 0x00010A31 File Offset: 0x0000EC31
		public TypeNameHandling? ItemTypeNameHandling { get; set; }

		// Token: 0x0600041C RID: 1052 RVA: 0x00010A3C File Offset: 0x0000EC3C
		internal JsonContainerContract(Type underlyingType) : base(underlyingType)
		{
			JsonContainerAttribute cachedAttribute = JsonTypeReflector.GetCachedAttribute<JsonContainerAttribute>(underlyingType);
			if (cachedAttribute != null)
			{
				if (cachedAttribute.ItemConverterType != null)
				{
					this.ItemConverter = JsonTypeReflector.CreateJsonConverterInstance(cachedAttribute.ItemConverterType, cachedAttribute.ItemConverterParameters);
				}
				this.ItemIsReference = cachedAttribute._itemIsReference;
				this.ItemReferenceLoopHandling = cachedAttribute._itemReferenceLoopHandling;
				this.ItemTypeNameHandling = cachedAttribute._itemTypeNameHandling;
			}
		}

		// Token: 0x040001C6 RID: 454
		private JsonContract _itemContract;

		// Token: 0x040001C7 RID: 455
		private JsonContract _finalItemContract;
	}
}
