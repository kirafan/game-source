﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000077 RID: 119
	[Preserve]
	internal enum JsonContractType
	{
		// Token: 0x04000202 RID: 514
		None,
		// Token: 0x04000203 RID: 515
		Object,
		// Token: 0x04000204 RID: 516
		Array,
		// Token: 0x04000205 RID: 517
		Primitive,
		// Token: 0x04000206 RID: 518
		String,
		// Token: 0x04000207 RID: 519
		Dictionary,
		// Token: 0x04000208 RID: 520
		Dynamic,
		// Token: 0x04000209 RID: 521
		Serializable,
		// Token: 0x0400020A RID: 522
		Linq
	}
}
