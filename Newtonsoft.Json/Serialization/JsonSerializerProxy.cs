﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000085 RID: 133
	[Preserve]
	internal class JsonSerializerProxy : JsonSerializer
	{
		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060005F2 RID: 1522 RVA: 0x000196C1 File Offset: 0x000178C1
		// (remove) Token: 0x060005F3 RID: 1523 RVA: 0x000196CF File Offset: 0x000178CF
		public override event EventHandler<ErrorEventArgs> Error
		{
			add
			{
				this._serializer.Error += value;
			}
			remove
			{
				this._serializer.Error -= value;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x060005F4 RID: 1524 RVA: 0x000196DD File Offset: 0x000178DD
		// (set) Token: 0x060005F5 RID: 1525 RVA: 0x000196EA File Offset: 0x000178EA
		public override IReferenceResolver ReferenceResolver
		{
			get
			{
				return this._serializer.ReferenceResolver;
			}
			set
			{
				this._serializer.ReferenceResolver = value;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x060005F6 RID: 1526 RVA: 0x000196F8 File Offset: 0x000178F8
		// (set) Token: 0x060005F7 RID: 1527 RVA: 0x00019705 File Offset: 0x00017905
		public override ITraceWriter TraceWriter
		{
			get
			{
				return this._serializer.TraceWriter;
			}
			set
			{
				this._serializer.TraceWriter = value;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x060005F8 RID: 1528 RVA: 0x00019713 File Offset: 0x00017913
		// (set) Token: 0x060005F9 RID: 1529 RVA: 0x00019720 File Offset: 0x00017920
		public override IEqualityComparer EqualityComparer
		{
			get
			{
				return this._serializer.EqualityComparer;
			}
			set
			{
				this._serializer.EqualityComparer = value;
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x060005FA RID: 1530 RVA: 0x0001972E File Offset: 0x0001792E
		public override JsonConverterCollection Converters
		{
			get
			{
				return this._serializer.Converters;
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x060005FB RID: 1531 RVA: 0x0001973B File Offset: 0x0001793B
		// (set) Token: 0x060005FC RID: 1532 RVA: 0x00019748 File Offset: 0x00017948
		public override DefaultValueHandling DefaultValueHandling
		{
			get
			{
				return this._serializer.DefaultValueHandling;
			}
			set
			{
				this._serializer.DefaultValueHandling = value;
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x060005FD RID: 1533 RVA: 0x00019756 File Offset: 0x00017956
		// (set) Token: 0x060005FE RID: 1534 RVA: 0x00019763 File Offset: 0x00017963
		public override IContractResolver ContractResolver
		{
			get
			{
				return this._serializer.ContractResolver;
			}
			set
			{
				this._serializer.ContractResolver = value;
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x060005FF RID: 1535 RVA: 0x00019771 File Offset: 0x00017971
		// (set) Token: 0x06000600 RID: 1536 RVA: 0x0001977E File Offset: 0x0001797E
		public override MissingMemberHandling MissingMemberHandling
		{
			get
			{
				return this._serializer.MissingMemberHandling;
			}
			set
			{
				this._serializer.MissingMemberHandling = value;
			}
		}

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000601 RID: 1537 RVA: 0x0001978C File Offset: 0x0001798C
		// (set) Token: 0x06000602 RID: 1538 RVA: 0x00019799 File Offset: 0x00017999
		public override NullValueHandling NullValueHandling
		{
			get
			{
				return this._serializer.NullValueHandling;
			}
			set
			{
				this._serializer.NullValueHandling = value;
			}
		}

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000603 RID: 1539 RVA: 0x000197A7 File Offset: 0x000179A7
		// (set) Token: 0x06000604 RID: 1540 RVA: 0x000197B4 File Offset: 0x000179B4
		public override ObjectCreationHandling ObjectCreationHandling
		{
			get
			{
				return this._serializer.ObjectCreationHandling;
			}
			set
			{
				this._serializer.ObjectCreationHandling = value;
			}
		}

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000605 RID: 1541 RVA: 0x000197C2 File Offset: 0x000179C2
		// (set) Token: 0x06000606 RID: 1542 RVA: 0x000197CF File Offset: 0x000179CF
		public override ReferenceLoopHandling ReferenceLoopHandling
		{
			get
			{
				return this._serializer.ReferenceLoopHandling;
			}
			set
			{
				this._serializer.ReferenceLoopHandling = value;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000607 RID: 1543 RVA: 0x000197DD File Offset: 0x000179DD
		// (set) Token: 0x06000608 RID: 1544 RVA: 0x000197EA File Offset: 0x000179EA
		public override PreserveReferencesHandling PreserveReferencesHandling
		{
			get
			{
				return this._serializer.PreserveReferencesHandling;
			}
			set
			{
				this._serializer.PreserveReferencesHandling = value;
			}
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000609 RID: 1545 RVA: 0x000197F8 File Offset: 0x000179F8
		// (set) Token: 0x0600060A RID: 1546 RVA: 0x00019805 File Offset: 0x00017A05
		public override TypeNameHandling TypeNameHandling
		{
			get
			{
				return this._serializer.TypeNameHandling;
			}
			set
			{
				this._serializer.TypeNameHandling = value;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x0600060B RID: 1547 RVA: 0x00019813 File Offset: 0x00017A13
		// (set) Token: 0x0600060C RID: 1548 RVA: 0x00019820 File Offset: 0x00017A20
		public override MetadataPropertyHandling MetadataPropertyHandling
		{
			get
			{
				return this._serializer.MetadataPropertyHandling;
			}
			set
			{
				this._serializer.MetadataPropertyHandling = value;
			}
		}

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x0600060D RID: 1549 RVA: 0x0001982E File Offset: 0x00017A2E
		// (set) Token: 0x0600060E RID: 1550 RVA: 0x0001983B File Offset: 0x00017A3B
		public override FormatterAssemblyStyle TypeNameAssemblyFormat
		{
			get
			{
				return this._serializer.TypeNameAssemblyFormat;
			}
			set
			{
				this._serializer.TypeNameAssemblyFormat = value;
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x0600060F RID: 1551 RVA: 0x00019849 File Offset: 0x00017A49
		// (set) Token: 0x06000610 RID: 1552 RVA: 0x00019856 File Offset: 0x00017A56
		public override ConstructorHandling ConstructorHandling
		{
			get
			{
				return this._serializer.ConstructorHandling;
			}
			set
			{
				this._serializer.ConstructorHandling = value;
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000611 RID: 1553 RVA: 0x00019864 File Offset: 0x00017A64
		// (set) Token: 0x06000612 RID: 1554 RVA: 0x00019871 File Offset: 0x00017A71
		public override SerializationBinder Binder
		{
			get
			{
				return this._serializer.Binder;
			}
			set
			{
				this._serializer.Binder = value;
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x0001987F File Offset: 0x00017A7F
		// (set) Token: 0x06000614 RID: 1556 RVA: 0x0001988C File Offset: 0x00017A8C
		public override StreamingContext Context
		{
			get
			{
				return this._serializer.Context;
			}
			set
			{
				this._serializer.Context = value;
			}
		}

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000615 RID: 1557 RVA: 0x0001989A File Offset: 0x00017A9A
		// (set) Token: 0x06000616 RID: 1558 RVA: 0x000198A7 File Offset: 0x00017AA7
		public override Formatting Formatting
		{
			get
			{
				return this._serializer.Formatting;
			}
			set
			{
				this._serializer.Formatting = value;
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000617 RID: 1559 RVA: 0x000198B5 File Offset: 0x00017AB5
		// (set) Token: 0x06000618 RID: 1560 RVA: 0x000198C2 File Offset: 0x00017AC2
		public override DateFormatHandling DateFormatHandling
		{
			get
			{
				return this._serializer.DateFormatHandling;
			}
			set
			{
				this._serializer.DateFormatHandling = value;
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000619 RID: 1561 RVA: 0x000198D0 File Offset: 0x00017AD0
		// (set) Token: 0x0600061A RID: 1562 RVA: 0x000198DD File Offset: 0x00017ADD
		public override DateTimeZoneHandling DateTimeZoneHandling
		{
			get
			{
				return this._serializer.DateTimeZoneHandling;
			}
			set
			{
				this._serializer.DateTimeZoneHandling = value;
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x0600061B RID: 1563 RVA: 0x000198EB File Offset: 0x00017AEB
		// (set) Token: 0x0600061C RID: 1564 RVA: 0x000198F8 File Offset: 0x00017AF8
		public override DateParseHandling DateParseHandling
		{
			get
			{
				return this._serializer.DateParseHandling;
			}
			set
			{
				this._serializer.DateParseHandling = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x0600061D RID: 1565 RVA: 0x00019906 File Offset: 0x00017B06
		// (set) Token: 0x0600061E RID: 1566 RVA: 0x00019913 File Offset: 0x00017B13
		public override FloatFormatHandling FloatFormatHandling
		{
			get
			{
				return this._serializer.FloatFormatHandling;
			}
			set
			{
				this._serializer.FloatFormatHandling = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x0600061F RID: 1567 RVA: 0x00019921 File Offset: 0x00017B21
		// (set) Token: 0x06000620 RID: 1568 RVA: 0x0001992E File Offset: 0x00017B2E
		public override FloatParseHandling FloatParseHandling
		{
			get
			{
				return this._serializer.FloatParseHandling;
			}
			set
			{
				this._serializer.FloatParseHandling = value;
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000621 RID: 1569 RVA: 0x0001993C File Offset: 0x00017B3C
		// (set) Token: 0x06000622 RID: 1570 RVA: 0x00019949 File Offset: 0x00017B49
		public override StringEscapeHandling StringEscapeHandling
		{
			get
			{
				return this._serializer.StringEscapeHandling;
			}
			set
			{
				this._serializer.StringEscapeHandling = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000623 RID: 1571 RVA: 0x00019957 File Offset: 0x00017B57
		// (set) Token: 0x06000624 RID: 1572 RVA: 0x00019964 File Offset: 0x00017B64
		public override string DateFormatString
		{
			get
			{
				return this._serializer.DateFormatString;
			}
			set
			{
				this._serializer.DateFormatString = value;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000625 RID: 1573 RVA: 0x00019972 File Offset: 0x00017B72
		// (set) Token: 0x06000626 RID: 1574 RVA: 0x0001997F File Offset: 0x00017B7F
		public override CultureInfo Culture
		{
			get
			{
				return this._serializer.Culture;
			}
			set
			{
				this._serializer.Culture = value;
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000627 RID: 1575 RVA: 0x0001998D File Offset: 0x00017B8D
		// (set) Token: 0x06000628 RID: 1576 RVA: 0x0001999A File Offset: 0x00017B9A
		public override int? MaxDepth
		{
			get
			{
				return this._serializer.MaxDepth;
			}
			set
			{
				this._serializer.MaxDepth = value;
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000629 RID: 1577 RVA: 0x000199A8 File Offset: 0x00017BA8
		// (set) Token: 0x0600062A RID: 1578 RVA: 0x000199B5 File Offset: 0x00017BB5
		public override bool CheckAdditionalContent
		{
			get
			{
				return this._serializer.CheckAdditionalContent;
			}
			set
			{
				this._serializer.CheckAdditionalContent = value;
			}
		}

		// Token: 0x0600062B RID: 1579 RVA: 0x000199C3 File Offset: 0x00017BC3
		internal JsonSerializerInternalBase GetInternalSerializer()
		{
			if (this._serializerReader != null)
			{
				return this._serializerReader;
			}
			return this._serializerWriter;
		}

		// Token: 0x0600062C RID: 1580 RVA: 0x000199DA File Offset: 0x00017BDA
		public JsonSerializerProxy(JsonSerializerInternalReader serializerReader)
		{
			ValidationUtils.ArgumentNotNull(serializerReader, "serializerReader");
			this._serializerReader = serializerReader;
			this._serializer = serializerReader.Serializer;
		}

		// Token: 0x0600062D RID: 1581 RVA: 0x00019A00 File Offset: 0x00017C00
		public JsonSerializerProxy(JsonSerializerInternalWriter serializerWriter)
		{
			ValidationUtils.ArgumentNotNull(serializerWriter, "serializerWriter");
			this._serializerWriter = serializerWriter;
			this._serializer = serializerWriter.Serializer;
		}

		// Token: 0x0600062E RID: 1582 RVA: 0x00019A26 File Offset: 0x00017C26
		internal override object DeserializeInternal(JsonReader reader, Type objectType)
		{
			if (this._serializerReader != null)
			{
				return this._serializerReader.Deserialize(reader, objectType, false);
			}
			return this._serializer.Deserialize(reader, objectType);
		}

		// Token: 0x0600062F RID: 1583 RVA: 0x00019A4C File Offset: 0x00017C4C
		internal override void PopulateInternal(JsonReader reader, object target)
		{
			if (this._serializerReader != null)
			{
				this._serializerReader.Populate(reader, target);
				return;
			}
			this._serializer.Populate(reader, target);
		}

		// Token: 0x06000630 RID: 1584 RVA: 0x00019A71 File Offset: 0x00017C71
		internal override void SerializeInternal(JsonWriter jsonWriter, object value, Type rootType)
		{
			if (this._serializerWriter != null)
			{
				this._serializerWriter.Serialize(jsonWriter, value, rootType);
				return;
			}
			this._serializer.Serialize(jsonWriter, value);
		}

		// Token: 0x04000265 RID: 613
		private readonly JsonSerializerInternalReader _serializerReader;

		// Token: 0x04000266 RID: 614
		private readonly JsonSerializerInternalWriter _serializerWriter;

		// Token: 0x04000267 RID: 615
		private readonly JsonSerializer _serializer;
	}
}
