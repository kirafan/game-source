﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200007C RID: 124
	[Preserve]
	public abstract class JsonContract
	{
		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060004FD RID: 1277 RVA: 0x0001383F File Offset: 0x00011A3F
		// (set) Token: 0x060004FE RID: 1278 RVA: 0x00013847 File Offset: 0x00011A47
		public Type UnderlyingType { get; private set; }

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060004FF RID: 1279 RVA: 0x00013850 File Offset: 0x00011A50
		// (set) Token: 0x06000500 RID: 1280 RVA: 0x00013858 File Offset: 0x00011A58
		public Type CreatedType
		{
			get
			{
				return this._createdType;
			}
			set
			{
				this._createdType = value;
				this.IsSealed = this._createdType.IsSealed();
				this.IsInstantiable = (!this._createdType.IsInterface() && !this._createdType.IsAbstract());
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000501 RID: 1281 RVA: 0x00013896 File Offset: 0x00011A96
		// (set) Token: 0x06000502 RID: 1282 RVA: 0x0001389E File Offset: 0x00011A9E
		public bool? IsReference { get; set; }

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x06000503 RID: 1283 RVA: 0x000138A7 File Offset: 0x00011AA7
		// (set) Token: 0x06000504 RID: 1284 RVA: 0x000138AF File Offset: 0x00011AAF
		public JsonConverter Converter { get; set; }

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000505 RID: 1285 RVA: 0x000138B8 File Offset: 0x00011AB8
		// (set) Token: 0x06000506 RID: 1286 RVA: 0x000138C0 File Offset: 0x00011AC0
		internal JsonConverter InternalConverter { get; set; }

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000507 RID: 1287 RVA: 0x000138C9 File Offset: 0x00011AC9
		public IList<SerializationCallback> OnDeserializedCallbacks
		{
			get
			{
				if (this._onDeserializedCallbacks == null)
				{
					this._onDeserializedCallbacks = new List<SerializationCallback>();
				}
				return this._onDeserializedCallbacks;
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000508 RID: 1288 RVA: 0x000138E4 File Offset: 0x00011AE4
		public IList<SerializationCallback> OnDeserializingCallbacks
		{
			get
			{
				if (this._onDeserializingCallbacks == null)
				{
					this._onDeserializingCallbacks = new List<SerializationCallback>();
				}
				return this._onDeserializingCallbacks;
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000509 RID: 1289 RVA: 0x000138FF File Offset: 0x00011AFF
		public IList<SerializationCallback> OnSerializedCallbacks
		{
			get
			{
				if (this._onSerializedCallbacks == null)
				{
					this._onSerializedCallbacks = new List<SerializationCallback>();
				}
				return this._onSerializedCallbacks;
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x0600050A RID: 1290 RVA: 0x0001391A File Offset: 0x00011B1A
		public IList<SerializationCallback> OnSerializingCallbacks
		{
			get
			{
				if (this._onSerializingCallbacks == null)
				{
					this._onSerializingCallbacks = new List<SerializationCallback>();
				}
				return this._onSerializingCallbacks;
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600050B RID: 1291 RVA: 0x00013935 File Offset: 0x00011B35
		public IList<SerializationErrorCallback> OnErrorCallbacks
		{
			get
			{
				if (this._onErrorCallbacks == null)
				{
					this._onErrorCallbacks = new List<SerializationErrorCallback>();
				}
				return this._onErrorCallbacks;
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600050C RID: 1292 RVA: 0x00013950 File Offset: 0x00011B50
		// (set) Token: 0x0600050D RID: 1293 RVA: 0x00013973 File Offset: 0x00011B73
		[Obsolete("This property is obsolete and has been replaced by the OnDeserializedCallbacks collection.")]
		public MethodInfo OnDeserialized
		{
			get
			{
				if (this.OnDeserializedCallbacks.Count <= 0)
				{
					return null;
				}
				return this.OnDeserializedCallbacks[0].Method();
			}
			set
			{
				this.OnDeserializedCallbacks.Clear();
				this.OnDeserializedCallbacks.Add(JsonContract.CreateSerializationCallback(value));
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x0600050E RID: 1294 RVA: 0x00013991 File Offset: 0x00011B91
		// (set) Token: 0x0600050F RID: 1295 RVA: 0x000139B4 File Offset: 0x00011BB4
		[Obsolete("This property is obsolete and has been replaced by the OnDeserializingCallbacks collection.")]
		public MethodInfo OnDeserializing
		{
			get
			{
				if (this.OnDeserializingCallbacks.Count <= 0)
				{
					return null;
				}
				return this.OnDeserializingCallbacks[0].Method();
			}
			set
			{
				this.OnDeserializingCallbacks.Clear();
				this.OnDeserializingCallbacks.Add(JsonContract.CreateSerializationCallback(value));
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000510 RID: 1296 RVA: 0x000139D2 File Offset: 0x00011BD2
		// (set) Token: 0x06000511 RID: 1297 RVA: 0x000139F5 File Offset: 0x00011BF5
		[Obsolete("This property is obsolete and has been replaced by the OnSerializedCallbacks collection.")]
		public MethodInfo OnSerialized
		{
			get
			{
				if (this.OnSerializedCallbacks.Count <= 0)
				{
					return null;
				}
				return this.OnSerializedCallbacks[0].Method();
			}
			set
			{
				this.OnSerializedCallbacks.Clear();
				this.OnSerializedCallbacks.Add(JsonContract.CreateSerializationCallback(value));
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000512 RID: 1298 RVA: 0x00013A13 File Offset: 0x00011C13
		// (set) Token: 0x06000513 RID: 1299 RVA: 0x00013A36 File Offset: 0x00011C36
		[Obsolete("This property is obsolete and has been replaced by the OnSerializingCallbacks collection.")]
		public MethodInfo OnSerializing
		{
			get
			{
				if (this.OnSerializingCallbacks.Count <= 0)
				{
					return null;
				}
				return this.OnSerializingCallbacks[0].Method();
			}
			set
			{
				this.OnSerializingCallbacks.Clear();
				this.OnSerializingCallbacks.Add(JsonContract.CreateSerializationCallback(value));
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x06000514 RID: 1300 RVA: 0x00013A54 File Offset: 0x00011C54
		// (set) Token: 0x06000515 RID: 1301 RVA: 0x00013A77 File Offset: 0x00011C77
		[Obsolete("This property is obsolete and has been replaced by the OnErrorCallbacks collection.")]
		public MethodInfo OnError
		{
			get
			{
				if (this.OnErrorCallbacks.Count <= 0)
				{
					return null;
				}
				return this.OnErrorCallbacks[0].Method();
			}
			set
			{
				this.OnErrorCallbacks.Clear();
				this.OnErrorCallbacks.Add(JsonContract.CreateSerializationErrorCallback(value));
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x06000516 RID: 1302 RVA: 0x00013A95 File Offset: 0x00011C95
		// (set) Token: 0x06000517 RID: 1303 RVA: 0x00013A9D File Offset: 0x00011C9D
		public Func<object> DefaultCreator { get; set; }

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x06000518 RID: 1304 RVA: 0x00013AA6 File Offset: 0x00011CA6
		// (set) Token: 0x06000519 RID: 1305 RVA: 0x00013AAE File Offset: 0x00011CAE
		public bool DefaultCreatorNonPublic { get; set; }

		// Token: 0x0600051A RID: 1306 RVA: 0x00013AB8 File Offset: 0x00011CB8
		internal JsonContract(Type underlyingType)
		{
			ValidationUtils.ArgumentNotNull(underlyingType, "underlyingType");
			this.UnderlyingType = underlyingType;
			this.IsNullable = ReflectionUtils.IsNullable(underlyingType);
			this.NonNullableUnderlyingType = ((this.IsNullable && ReflectionUtils.IsNullableType(underlyingType)) ? Nullable.GetUnderlyingType(underlyingType) : underlyingType);
			this.CreatedType = this.NonNullableUnderlyingType;
			this.IsConvertable = ConvertUtils.IsConvertible(this.NonNullableUnderlyingType);
			this.IsEnum = this.NonNullableUnderlyingType.IsEnum();
			this.InternalReadType = ReadType.Read;
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x00013B40 File Offset: 0x00011D40
		internal void InvokeOnSerializing(object o, StreamingContext context)
		{
			if (this._onSerializingCallbacks != null)
			{
				foreach (SerializationCallback serializationCallback in this._onSerializingCallbacks)
				{
					serializationCallback(o, context);
				}
			}
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x00013B94 File Offset: 0x00011D94
		internal void InvokeOnSerialized(object o, StreamingContext context)
		{
			if (this._onSerializedCallbacks != null)
			{
				foreach (SerializationCallback serializationCallback in this._onSerializedCallbacks)
				{
					serializationCallback(o, context);
				}
			}
		}

		// Token: 0x0600051D RID: 1309 RVA: 0x00013BE8 File Offset: 0x00011DE8
		internal void InvokeOnDeserializing(object o, StreamingContext context)
		{
			if (this._onDeserializingCallbacks != null)
			{
				foreach (SerializationCallback serializationCallback in this._onDeserializingCallbacks)
				{
					serializationCallback(o, context);
				}
			}
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x00013C3C File Offset: 0x00011E3C
		internal void InvokeOnDeserialized(object o, StreamingContext context)
		{
			if (this._onDeserializedCallbacks != null)
			{
				foreach (SerializationCallback serializationCallback in this._onDeserializedCallbacks)
				{
					serializationCallback(o, context);
				}
			}
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x00013C98 File Offset: 0x00011E98
		internal void InvokeOnError(object o, StreamingContext context, ErrorContext errorContext)
		{
			if (this._onErrorCallbacks != null)
			{
				foreach (SerializationErrorCallback serializationErrorCallback in this._onErrorCallbacks)
				{
					serializationErrorCallback(o, context, errorContext);
				}
			}
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x00013CF0 File Offset: 0x00011EF0
		internal static SerializationCallback CreateSerializationCallback(MethodInfo callbackMethodInfo)
		{
			return delegate(object o, StreamingContext context)
			{
				callbackMethodInfo.Invoke(o, new object[]
				{
					context
				});
			};
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00013D09 File Offset: 0x00011F09
		internal static SerializationErrorCallback CreateSerializationErrorCallback(MethodInfo callbackMethodInfo)
		{
			return delegate(object o, StreamingContext context, ErrorContext econtext)
			{
				callbackMethodInfo.Invoke(o, new object[]
				{
					context,
					econtext
				});
			};
		}

		// Token: 0x0400020B RID: 523
		internal bool IsNullable;

		// Token: 0x0400020C RID: 524
		internal bool IsConvertable;

		// Token: 0x0400020D RID: 525
		internal bool IsEnum;

		// Token: 0x0400020E RID: 526
		internal Type NonNullableUnderlyingType;

		// Token: 0x0400020F RID: 527
		internal ReadType InternalReadType;

		// Token: 0x04000210 RID: 528
		internal JsonContractType ContractType;

		// Token: 0x04000211 RID: 529
		internal bool IsReadOnlyOrFixedSize;

		// Token: 0x04000212 RID: 530
		internal bool IsSealed;

		// Token: 0x04000213 RID: 531
		internal bool IsInstantiable;

		// Token: 0x04000214 RID: 532
		private List<SerializationCallback> _onDeserializedCallbacks;

		// Token: 0x04000215 RID: 533
		private IList<SerializationCallback> _onDeserializingCallbacks;

		// Token: 0x04000216 RID: 534
		private IList<SerializationCallback> _onSerializedCallbacks;

		// Token: 0x04000217 RID: 535
		private IList<SerializationCallback> _onSerializingCallbacks;

		// Token: 0x04000218 RID: 536
		private IList<SerializationErrorCallback> _onErrorCallbacks;

		// Token: 0x04000219 RID: 537
		private Type _createdType;
	}
}
