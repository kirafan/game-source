﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200008A RID: 138
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[Preserve]
	public sealed class OnErrorAttribute : Attribute
	{
	}
}
