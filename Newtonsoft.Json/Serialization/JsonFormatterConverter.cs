﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000068 RID: 104
	[Preserve]
	internal class JsonFormatterConverter : IFormatterConverter
	{
		// Token: 0x06000464 RID: 1124 RVA: 0x00011407 File Offset: 0x0000F607
		public JsonFormatterConverter(JsonSerializerInternalReader reader, JsonISerializableContract contract, JsonProperty member)
		{
			ValidationUtils.ArgumentNotNull(reader, "reader");
			ValidationUtils.ArgumentNotNull(contract, "contract");
			this._reader = reader;
			this._contract = contract;
			this._member = member;
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x0001143A File Offset: 0x0000F63A
		private T GetTokenValue<T>(object value)
		{
			ValidationUtils.ArgumentNotNull(value, "value");
			return (T)((object)System.Convert.ChangeType(((JValue)value).Value, typeof(T), CultureInfo.InvariantCulture));
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x0001146C File Offset: 0x0000F66C
		public object Convert(object value, Type type)
		{
			ValidationUtils.ArgumentNotNull(value, "value");
			JToken jtoken = value as JToken;
			if (jtoken == null)
			{
				throw new ArgumentException("Value is not a JToken.", "value");
			}
			return this._reader.CreateISerializableItem(jtoken, type, this._contract, this._member);
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x000114B7 File Offset: 0x0000F6B7
		public object Convert(object value, TypeCode typeCode)
		{
			ValidationUtils.ArgumentNotNull(value, "value");
			if (value is JValue)
			{
				value = ((JValue)value).Value;
			}
			return System.Convert.ChangeType(value, typeCode, CultureInfo.InvariantCulture);
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x000114E5 File Offset: 0x0000F6E5
		public bool ToBoolean(object value)
		{
			return this.GetTokenValue<bool>(value);
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x000114EE File Offset: 0x0000F6EE
		public byte ToByte(object value)
		{
			return this.GetTokenValue<byte>(value);
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x000114F7 File Offset: 0x0000F6F7
		public char ToChar(object value)
		{
			return this.GetTokenValue<char>(value);
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00011500 File Offset: 0x0000F700
		public DateTime ToDateTime(object value)
		{
			return this.GetTokenValue<DateTime>(value);
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00011509 File Offset: 0x0000F709
		public decimal ToDecimal(object value)
		{
			return this.GetTokenValue<decimal>(value);
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00011512 File Offset: 0x0000F712
		public double ToDouble(object value)
		{
			return this.GetTokenValue<double>(value);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x0001151B File Offset: 0x0000F71B
		public short ToInt16(object value)
		{
			return this.GetTokenValue<short>(value);
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00011524 File Offset: 0x0000F724
		public int ToInt32(object value)
		{
			return this.GetTokenValue<int>(value);
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x0001152D File Offset: 0x0000F72D
		public long ToInt64(object value)
		{
			return this.GetTokenValue<long>(value);
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x00011536 File Offset: 0x0000F736
		public sbyte ToSByte(object value)
		{
			return this.GetTokenValue<sbyte>(value);
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x0001153F File Offset: 0x0000F73F
		public float ToSingle(object value)
		{
			return this.GetTokenValue<float>(value);
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00011548 File Offset: 0x0000F748
		public string ToString(object value)
		{
			return this.GetTokenValue<string>(value);
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x00011551 File Offset: 0x0000F751
		public ushort ToUInt16(object value)
		{
			return this.GetTokenValue<ushort>(value);
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x0001155A File Offset: 0x0000F75A
		public uint ToUInt32(object value)
		{
			return this.GetTokenValue<uint>(value);
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x00011563 File Offset: 0x0000F763
		public ulong ToUInt64(object value)
		{
			return this.GetTokenValue<ulong>(value);
		}

		// Token: 0x040001D5 RID: 469
		private readonly JsonSerializerInternalReader _reader;

		// Token: 0x040001D6 RID: 470
		private readonly JsonISerializableContract _contract;

		// Token: 0x040001D7 RID: 471
		private readonly JsonProperty _member;
	}
}
