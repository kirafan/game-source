﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x02000067 RID: 103
	[Preserve]
	internal class TraceJsonWriter : JsonWriter
	{
		// Token: 0x0600043C RID: 1084 RVA: 0x00010E7C File Offset: 0x0000F07C
		public TraceJsonWriter(JsonWriter innerWriter)
		{
			this._innerWriter = innerWriter;
			this._sw = new StringWriter(CultureInfo.InvariantCulture);
			this._sw.Write("Serialized JSON: " + Environment.NewLine);
			this._textWriter = new JsonTextWriter(this._sw);
			this._textWriter.Formatting = Formatting.Indented;
			this._textWriter.Culture = innerWriter.Culture;
			this._textWriter.DateFormatHandling = innerWriter.DateFormatHandling;
			this._textWriter.DateFormatString = innerWriter.DateFormatString;
			this._textWriter.DateTimeZoneHandling = innerWriter.DateTimeZoneHandling;
			this._textWriter.FloatFormatHandling = innerWriter.FloatFormatHandling;
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x00010F32 File Offset: 0x0000F132
		public string GetSerializedJsonMessage()
		{
			return this._sw.ToString();
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00010F3F File Offset: 0x0000F13F
		public override void WriteValue(decimal value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x00010F60 File Offset: 0x0000F160
		public override void WriteValue(bool value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x00010F81 File Offset: 0x0000F181
		public override void WriteValue(byte value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x00010FA2 File Offset: 0x0000F1A2
		public override void WriteValue(byte? value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x00010FC3 File Offset: 0x0000F1C3
		public override void WriteValue(char value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x00010FE4 File Offset: 0x0000F1E4
		public override void WriteValue(byte[] value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x00011005 File Offset: 0x0000F205
		public override void WriteValue(DateTime value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x00011026 File Offset: 0x0000F226
		public override void WriteValue(DateTimeOffset value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x00011047 File Offset: 0x0000F247
		public override void WriteValue(double value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x00011068 File Offset: 0x0000F268
		public override void WriteUndefined()
		{
			this._textWriter.WriteUndefined();
			this._innerWriter.WriteUndefined();
			base.WriteUndefined();
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x00011086 File Offset: 0x0000F286
		public override void WriteNull()
		{
			this._textWriter.WriteNull();
			this._innerWriter.WriteNull();
			base.WriteUndefined();
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x000110A4 File Offset: 0x0000F2A4
		public override void WriteValue(float value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x000110C5 File Offset: 0x0000F2C5
		public override void WriteValue(Guid value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x000110E6 File Offset: 0x0000F2E6
		public override void WriteValue(int value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x00011107 File Offset: 0x0000F307
		public override void WriteValue(long value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x00011128 File Offset: 0x0000F328
		public override void WriteValue(object value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x00011149 File Offset: 0x0000F349
		public override void WriteValue(sbyte value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0001116A File Offset: 0x0000F36A
		public override void WriteValue(short value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0001118B File Offset: 0x0000F38B
		public override void WriteValue(string value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x000111AC File Offset: 0x0000F3AC
		public override void WriteValue(TimeSpan value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x000111CD File Offset: 0x0000F3CD
		public override void WriteValue(uint value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x000111EE File Offset: 0x0000F3EE
		public override void WriteValue(ulong value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0001120F File Offset: 0x0000F40F
		public override void WriteValue(Uri value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x00011230 File Offset: 0x0000F430
		public override void WriteValue(ushort value)
		{
			this._textWriter.WriteValue(value);
			this._innerWriter.WriteValue(value);
			base.WriteValue(value);
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x00011251 File Offset: 0x0000F451
		public override void WriteWhitespace(string ws)
		{
			this._textWriter.WriteWhitespace(ws);
			this._innerWriter.WriteWhitespace(ws);
			base.WriteWhitespace(ws);
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x00011272 File Offset: 0x0000F472
		public override void WriteComment(string text)
		{
			this._textWriter.WriteComment(text);
			this._innerWriter.WriteComment(text);
			base.WriteComment(text);
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x00011293 File Offset: 0x0000F493
		public override void WriteStartArray()
		{
			this._textWriter.WriteStartArray();
			this._innerWriter.WriteStartArray();
			base.WriteStartArray();
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x000112B1 File Offset: 0x0000F4B1
		public override void WriteEndArray()
		{
			this._textWriter.WriteEndArray();
			this._innerWriter.WriteEndArray();
			base.WriteEndArray();
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x000112CF File Offset: 0x0000F4CF
		public override void WriteStartConstructor(string name)
		{
			this._textWriter.WriteStartConstructor(name);
			this._innerWriter.WriteStartConstructor(name);
			base.WriteStartConstructor(name);
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x000112F0 File Offset: 0x0000F4F0
		public override void WriteEndConstructor()
		{
			this._textWriter.WriteEndConstructor();
			this._innerWriter.WriteEndConstructor();
			base.WriteEndConstructor();
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x0001130E File Offset: 0x0000F50E
		public override void WritePropertyName(string name)
		{
			this._textWriter.WritePropertyName(name);
			this._innerWriter.WritePropertyName(name);
			base.WritePropertyName(name);
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x0001132F File Offset: 0x0000F52F
		public override void WritePropertyName(string name, bool escape)
		{
			this._textWriter.WritePropertyName(name, escape);
			this._innerWriter.WritePropertyName(name, escape);
			base.WritePropertyName(name);
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x00011352 File Offset: 0x0000F552
		public override void WriteStartObject()
		{
			this._textWriter.WriteStartObject();
			this._innerWriter.WriteStartObject();
			base.WriteStartObject();
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x00011370 File Offset: 0x0000F570
		public override void WriteEndObject()
		{
			this._textWriter.WriteEndObject();
			this._innerWriter.WriteEndObject();
			base.WriteEndObject();
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x0001138E File Offset: 0x0000F58E
		public override void WriteRawValue(string json)
		{
			this._textWriter.WriteRawValue(json);
			this._innerWriter.WriteRawValue(json);
			base.InternalWriteValue(JsonToken.Undefined);
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x000113B0 File Offset: 0x0000F5B0
		public override void WriteRaw(string json)
		{
			this._textWriter.WriteRaw(json);
			this._innerWriter.WriteRaw(json);
			base.WriteRaw(json);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x000113D1 File Offset: 0x0000F5D1
		public override void Close()
		{
			this._textWriter.Close();
			this._innerWriter.Close();
			base.Close();
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x000113EF File Offset: 0x0000F5EF
		public override void Flush()
		{
			this._textWriter.Flush();
			this._innerWriter.Flush();
		}

		// Token: 0x040001D2 RID: 466
		private readonly JsonWriter _innerWriter;

		// Token: 0x040001D3 RID: 467
		private readonly JsonTextWriter _textWriter;

		// Token: 0x040001D4 RID: 468
		private readonly StringWriter _sw;
	}
}
