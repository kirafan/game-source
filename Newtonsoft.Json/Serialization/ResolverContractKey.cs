﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Serialization
{
	// Token: 0x0200006F RID: 111
	[Preserve]
	internal struct ResolverContractKey
	{
		// Token: 0x0600048C RID: 1164 RVA: 0x00011819 File Offset: 0x0000FA19
		public ResolverContractKey(Type resolverType, Type contractType)
		{
			this._resolverType = resolverType;
			this._contractType = contractType;
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x00011829 File Offset: 0x0000FA29
		public override int GetHashCode()
		{
			return this._resolverType.GetHashCode() ^ this._contractType.GetHashCode();
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x00011842 File Offset: 0x0000FA42
		public override bool Equals(object obj)
		{
			return obj is ResolverContractKey && this.Equals((ResolverContractKey)obj);
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x0001185A File Offset: 0x0000FA5A
		public bool Equals(ResolverContractKey other)
		{
			return this._resolverType == other._resolverType && this._contractType == other._contractType;
		}

		// Token: 0x040001DE RID: 478
		private readonly Type _resolverType;

		// Token: 0x040001DF RID: 479
		private readonly Type _contractType;
	}
}
