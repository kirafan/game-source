﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200002A RID: 42
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonIgnoreAttribute : Attribute
	{
	}
}
