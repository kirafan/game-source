﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200002C RID: 44
	[Preserve]
	[Serializable]
	public class JsonWriterException : JsonException
	{
		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000140 RID: 320 RVA: 0x000066C1 File Offset: 0x000048C1
		// (set) Token: 0x06000141 RID: 321 RVA: 0x000066C9 File Offset: 0x000048C9
		public string Path { get; private set; }

		// Token: 0x06000142 RID: 322 RVA: 0x000066D2 File Offset: 0x000048D2
		public JsonWriterException()
		{
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000066DA File Offset: 0x000048DA
		public JsonWriterException(string message) : base(message)
		{
		}

		// Token: 0x06000144 RID: 324 RVA: 0x000066E3 File Offset: 0x000048E3
		public JsonWriterException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000145 RID: 325 RVA: 0x000066ED File Offset: 0x000048ED
		public JsonWriterException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000146 RID: 326 RVA: 0x000066F7 File Offset: 0x000048F7
		internal JsonWriterException(string message, Exception innerException, string path) : base(message, innerException)
		{
			this.Path = path;
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00006708 File Offset: 0x00004908
		internal static JsonWriterException Create(JsonWriter writer, string message, Exception ex)
		{
			return JsonWriterException.Create(writer.ContainerPath, message, ex);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00006717 File Offset: 0x00004917
		internal static JsonWriterException Create(string path, string message, Exception ex)
		{
			message = JsonPosition.FormatMessage(null, path, message);
			return new JsonWriterException(message, ex, path);
		}
	}
}
