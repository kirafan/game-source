﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DC RID: 220
	[Preserve]
	internal class BsonArray : BsonToken, IEnumerable<BsonToken>, IEnumerable
	{
		// Token: 0x06000A31 RID: 2609 RVA: 0x000268CA File Offset: 0x00024ACA
		public void Add(BsonToken token)
		{
			this._children.Add(token);
			token.Parent = this;
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x0001F223 File Offset: 0x0001D423
		public override BsonType Type
		{
			get
			{
				return BsonType.Array;
			}
		}

		// Token: 0x06000A33 RID: 2611 RVA: 0x000268DF File Offset: 0x00024ADF
		public IEnumerator<BsonToken> GetEnumerator()
		{
			return this._children.GetEnumerator();
		}

		// Token: 0x06000A34 RID: 2612 RVA: 0x000268F1 File Offset: 0x00024AF1
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x04000328 RID: 808
		private readonly List<BsonToken> _children = new List<BsonToken>();
	}
}
