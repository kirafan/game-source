﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DD RID: 221
	[Preserve]
	internal class BsonValue : BsonToken
	{
		// Token: 0x06000A36 RID: 2614 RVA: 0x0002690C File Offset: 0x00024B0C
		public BsonValue(object value, BsonType type)
		{
			this._value = value;
			this._type = type;
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000A37 RID: 2615 RVA: 0x00026922 File Offset: 0x00024B22
		public object Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000A38 RID: 2616 RVA: 0x0002692A File Offset: 0x00024B2A
		public override BsonType Type
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x04000329 RID: 809
		private readonly object _value;

		// Token: 0x0400032A RID: 810
		private readonly BsonType _type;
	}
}
