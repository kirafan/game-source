﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000E3 RID: 227
	[Preserve]
	public class BsonWriter : JsonWriter
	{
		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000A4C RID: 2636 RVA: 0x000269F1 File Offset: 0x00024BF1
		// (set) Token: 0x06000A4D RID: 2637 RVA: 0x000269FE File Offset: 0x00024BFE
		public DateTimeKind DateTimeKindHandling
		{
			get
			{
				return this._writer.DateTimeKindHandling;
			}
			set
			{
				this._writer.DateTimeKindHandling = value;
			}
		}

		// Token: 0x06000A4E RID: 2638 RVA: 0x00026A0C File Offset: 0x00024C0C
		public BsonWriter(Stream stream)
		{
			ValidationUtils.ArgumentNotNull(stream, "stream");
			this._writer = new BsonBinaryWriter(new BinaryWriter(stream));
		}

		// Token: 0x06000A4F RID: 2639 RVA: 0x00026A30 File Offset: 0x00024C30
		public BsonWriter(BinaryWriter writer)
		{
			ValidationUtils.ArgumentNotNull(writer, "writer");
			this._writer = new BsonBinaryWriter(writer);
		}

		// Token: 0x06000A50 RID: 2640 RVA: 0x00026A4F File Offset: 0x00024C4F
		public override void Flush()
		{
			this._writer.Flush();
		}

		// Token: 0x06000A51 RID: 2641 RVA: 0x00026A5C File Offset: 0x00024C5C
		protected override void WriteEnd(JsonToken token)
		{
			base.WriteEnd(token);
			this.RemoveParent();
			if (base.Top == 0)
			{
				this._writer.WriteToken(this._root);
			}
		}

		// Token: 0x06000A52 RID: 2642 RVA: 0x00026A84 File Offset: 0x00024C84
		public override void WriteComment(string text)
		{
			throw JsonWriterException.Create(this, "Cannot write JSON comment as BSON.", null);
		}

		// Token: 0x06000A53 RID: 2643 RVA: 0x00026A92 File Offset: 0x00024C92
		public override void WriteStartConstructor(string name)
		{
			throw JsonWriterException.Create(this, "Cannot write JSON constructor as BSON.", null);
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x00026AA0 File Offset: 0x00024CA0
		public override void WriteRaw(string json)
		{
			throw JsonWriterException.Create(this, "Cannot write raw JSON as BSON.", null);
		}

		// Token: 0x06000A55 RID: 2645 RVA: 0x00026AA0 File Offset: 0x00024CA0
		public override void WriteRawValue(string json)
		{
			throw JsonWriterException.Create(this, "Cannot write raw JSON as BSON.", null);
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x00026AAE File Offset: 0x00024CAE
		public override void WriteStartArray()
		{
			base.WriteStartArray();
			this.AddParent(new BsonArray());
		}

		// Token: 0x06000A57 RID: 2647 RVA: 0x00026AC1 File Offset: 0x00024CC1
		public override void WriteStartObject()
		{
			base.WriteStartObject();
			this.AddParent(new BsonObject());
		}

		// Token: 0x06000A58 RID: 2648 RVA: 0x00026AD4 File Offset: 0x00024CD4
		public override void WritePropertyName(string name)
		{
			base.WritePropertyName(name);
			this._propertyName = name;
		}

		// Token: 0x06000A59 RID: 2649 RVA: 0x00026AE4 File Offset: 0x00024CE4
		public override void Close()
		{
			base.Close();
			if (base.CloseOutput && this._writer != null)
			{
				this._writer.Close();
			}
		}

		// Token: 0x06000A5A RID: 2650 RVA: 0x00026B07 File Offset: 0x00024D07
		private void AddParent(BsonToken container)
		{
			this.AddToken(container);
			this._parent = container;
		}

		// Token: 0x06000A5B RID: 2651 RVA: 0x00026B17 File Offset: 0x00024D17
		private void RemoveParent()
		{
			this._parent = this._parent.Parent;
		}

		// Token: 0x06000A5C RID: 2652 RVA: 0x00026B2A File Offset: 0x00024D2A
		private void AddValue(object value, BsonType type)
		{
			this.AddToken(new BsonValue(value, type));
		}

		// Token: 0x06000A5D RID: 2653 RVA: 0x00026B3C File Offset: 0x00024D3C
		internal void AddToken(BsonToken token)
		{
			if (this._parent != null)
			{
				if (this._parent is BsonObject)
				{
					((BsonObject)this._parent).Add(this._propertyName, token);
					this._propertyName = null;
					return;
				}
				((BsonArray)this._parent).Add(token);
				return;
			}
			else
			{
				if (token.Type != BsonType.Object && token.Type != BsonType.Array)
				{
					throw JsonWriterException.Create(this, "Error writing {0} value. BSON must start with an Object or Array.".FormatWith(CultureInfo.InvariantCulture, token.Type), null);
				}
				this._parent = token;
				this._root = token;
				return;
			}
		}

		// Token: 0x06000A5E RID: 2654 RVA: 0x000060E0 File Offset: 0x000042E0
		public override void WriteValue(object value)
		{
			base.WriteValue(value);
		}

		// Token: 0x06000A5F RID: 2655 RVA: 0x00026BD1 File Offset: 0x00024DD1
		public override void WriteNull()
		{
			base.WriteNull();
			this.AddValue(null, BsonType.Null);
		}

		// Token: 0x06000A60 RID: 2656 RVA: 0x00026BE2 File Offset: 0x00024DE2
		public override void WriteUndefined()
		{
			base.WriteUndefined();
			this.AddValue(null, BsonType.Undefined);
		}

		// Token: 0x06000A61 RID: 2657 RVA: 0x00026BF2 File Offset: 0x00024DF2
		public override void WriteValue(string value)
		{
			base.WriteValue(value);
			if (value == null)
			{
				this.AddValue(null, BsonType.Null);
				return;
			}
			this.AddToken(new BsonString(value, true));
		}

		// Token: 0x06000A62 RID: 2658 RVA: 0x00026C15 File Offset: 0x00024E15
		public override void WriteValue(int value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A63 RID: 2659 RVA: 0x00026C2C File Offset: 0x00024E2C
		[CLSCompliant(false)]
		public override void WriteValue(uint value)
		{
			if (value > 2147483647U)
			{
				throw JsonWriterException.Create(this, "Value is too large to fit in a signed 32 bit integer. BSON does not support unsigned values.", null);
			}
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A64 RID: 2660 RVA: 0x00026C58 File Offset: 0x00024E58
		public override void WriteValue(long value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Long);
		}

		// Token: 0x06000A65 RID: 2661 RVA: 0x00026C6F File Offset: 0x00024E6F
		[CLSCompliant(false)]
		public override void WriteValue(ulong value)
		{
			if (value > 9223372036854775807UL)
			{
				throw JsonWriterException.Create(this, "Value is too large to fit in a signed 64 bit integer. BSON does not support unsigned values.", null);
			}
			base.WriteValue(value);
			this.AddValue(value, BsonType.Long);
		}

		// Token: 0x06000A66 RID: 2662 RVA: 0x00026C9F File Offset: 0x00024E9F
		public override void WriteValue(float value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Number);
		}

		// Token: 0x06000A67 RID: 2663 RVA: 0x00026CB5 File Offset: 0x00024EB5
		public override void WriteValue(double value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Number);
		}

		// Token: 0x06000A68 RID: 2664 RVA: 0x00026CCB File Offset: 0x00024ECB
		public override void WriteValue(bool value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Boolean);
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x00026CE1 File Offset: 0x00024EE1
		public override void WriteValue(short value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x00026CF8 File Offset: 0x00024EF8
		[CLSCompliant(false)]
		public override void WriteValue(ushort value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x00026D10 File Offset: 0x00024F10
		public override void WriteValue(char value)
		{
			base.WriteValue(value);
			string value2 = value.ToString(CultureInfo.InvariantCulture);
			this.AddToken(new BsonString(value2, true));
		}

		// Token: 0x06000A6C RID: 2668 RVA: 0x00026D40 File Offset: 0x00024F40
		public override void WriteValue(byte value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x00026D57 File Offset: 0x00024F57
		[CLSCompliant(false)]
		public override void WriteValue(sbyte value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Integer);
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x00026D6E File Offset: 0x00024F6E
		public override void WriteValue(decimal value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Number);
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x00026D84 File Offset: 0x00024F84
		public override void WriteValue(DateTime value)
		{
			base.WriteValue(value);
			value = DateTimeUtils.EnsureDateTime(value, base.DateTimeZoneHandling);
			this.AddValue(value, BsonType.Date);
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x00026DA9 File Offset: 0x00024FA9
		public override void WriteValue(DateTimeOffset value)
		{
			base.WriteValue(value);
			this.AddValue(value, BsonType.Date);
		}

		// Token: 0x06000A71 RID: 2673 RVA: 0x00026DC0 File Offset: 0x00024FC0
		public override void WriteValue(byte[] value)
		{
			base.WriteValue(value);
			this.AddToken(new BsonBinary(value, BsonBinaryType.Binary));
		}

		// Token: 0x06000A72 RID: 2674 RVA: 0x00026DD6 File Offset: 0x00024FD6
		public override void WriteValue(Guid value)
		{
			base.WriteValue(value);
			this.AddToken(new BsonBinary(value.ToByteArray(), BsonBinaryType.Uuid));
		}

		// Token: 0x06000A73 RID: 2675 RVA: 0x00026DF2 File Offset: 0x00024FF2
		public override void WriteValue(TimeSpan value)
		{
			base.WriteValue(value);
			this.AddToken(new BsonString(value.ToString(), true));
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x00026E14 File Offset: 0x00025014
		public override void WriteValue(Uri value)
		{
			base.WriteValue(value);
			this.AddToken(new BsonString(value.ToString(), true));
		}

		// Token: 0x06000A75 RID: 2677 RVA: 0x00026E2F File Offset: 0x0002502F
		public void WriteObjectId(byte[] value)
		{
			ValidationUtils.ArgumentNotNull(value, "value");
			if (value.Length != 12)
			{
				throw JsonWriterException.Create(this, "An object id must be 12 bytes", null);
			}
			base.UpdateScopeWithFinishedValue();
			base.AutoComplete(JsonToken.Undefined);
			this.AddValue(value, BsonType.Oid);
		}

		// Token: 0x06000A76 RID: 2678 RVA: 0x00026E66 File Offset: 0x00025066
		public void WriteRegex(string pattern, string options)
		{
			ValidationUtils.ArgumentNotNull(pattern, "pattern");
			base.UpdateScopeWithFinishedValue();
			base.AutoComplete(JsonToken.Undefined);
			this.AddToken(new BsonRegex(pattern, options));
		}

		// Token: 0x04000347 RID: 839
		private readonly BsonBinaryWriter _writer;

		// Token: 0x04000348 RID: 840
		private BsonToken _root;

		// Token: 0x04000349 RID: 841
		private BsonToken _parent;

		// Token: 0x0400034A RID: 842
		private string _propertyName;
	}
}
