﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DF RID: 223
	[Preserve]
	internal class BsonBinary : BsonValue
	{
		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000A3E RID: 2622 RVA: 0x00026965 File Offset: 0x00024B65
		// (set) Token: 0x06000A3F RID: 2623 RVA: 0x0002696D File Offset: 0x00024B6D
		public BsonBinaryType BinaryType { get; set; }

		// Token: 0x06000A40 RID: 2624 RVA: 0x00026976 File Offset: 0x00024B76
		public BsonBinary(byte[] value, BsonBinaryType binaryType) : base(value, BsonType.Binary)
		{
			this.BinaryType = binaryType;
		}
	}
}
