﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DB RID: 219
	[Preserve]
	internal class BsonObject : BsonToken, IEnumerable<BsonProperty>, IEnumerable
	{
		// Token: 0x06000A2C RID: 2604 RVA: 0x00026870 File Offset: 0x00024A70
		public void Add(string name, BsonToken token)
		{
			this._children.Add(new BsonProperty
			{
				Name = new BsonString(name, false),
				Value = token
			});
			token.Parent = this;
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000A2D RID: 2605 RVA: 0x0001A875 File Offset: 0x00018A75
		public override BsonType Type
		{
			get
			{
				return BsonType.Object;
			}
		}

		// Token: 0x06000A2E RID: 2606 RVA: 0x0002689D File Offset: 0x00024A9D
		public IEnumerator<BsonProperty> GetEnumerator()
		{
			return this._children.GetEnumerator();
		}

		// Token: 0x06000A2F RID: 2607 RVA: 0x000268AF File Offset: 0x00024AAF
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x04000327 RID: 807
		private readonly List<BsonProperty> _children = new List<BsonProperty>();
	}
}
