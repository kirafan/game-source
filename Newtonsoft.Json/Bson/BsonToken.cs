﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DA RID: 218
	[Preserve]
	internal abstract class BsonToken
	{
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000A26 RID: 2598
		public abstract BsonType Type { get; }

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000A27 RID: 2599 RVA: 0x0002684E File Offset: 0x00024A4E
		// (set) Token: 0x06000A28 RID: 2600 RVA: 0x00026856 File Offset: 0x00024A56
		public BsonToken Parent { get; set; }

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000A29 RID: 2601 RVA: 0x0002685F File Offset: 0x00024A5F
		// (set) Token: 0x06000A2A RID: 2602 RVA: 0x00026867 File Offset: 0x00024A67
		public int CalculatedSize { get; set; }
	}
}
