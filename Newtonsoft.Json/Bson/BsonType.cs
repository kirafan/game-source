﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000E2 RID: 226
	[Preserve]
	internal enum BsonType : sbyte
	{
		// Token: 0x04000333 RID: 819
		Number = 1,
		// Token: 0x04000334 RID: 820
		String,
		// Token: 0x04000335 RID: 821
		Object,
		// Token: 0x04000336 RID: 822
		Array,
		// Token: 0x04000337 RID: 823
		Binary,
		// Token: 0x04000338 RID: 824
		Undefined,
		// Token: 0x04000339 RID: 825
		Oid,
		// Token: 0x0400033A RID: 826
		Boolean,
		// Token: 0x0400033B RID: 827
		Date,
		// Token: 0x0400033C RID: 828
		Null,
		// Token: 0x0400033D RID: 829
		Regex,
		// Token: 0x0400033E RID: 830
		Reference,
		// Token: 0x0400033F RID: 831
		Code,
		// Token: 0x04000340 RID: 832
		Symbol,
		// Token: 0x04000341 RID: 833
		CodeWScope,
		// Token: 0x04000342 RID: 834
		Integer,
		// Token: 0x04000343 RID: 835
		TimeStamp,
		// Token: 0x04000344 RID: 836
		Long,
		// Token: 0x04000345 RID: 837
		MinKey = -1,
		// Token: 0x04000346 RID: 838
		MaxKey = 127
	}
}
