﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000E0 RID: 224
	[Preserve]
	internal class BsonRegex : BsonToken
	{
		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000A41 RID: 2625 RVA: 0x00026987 File Offset: 0x00024B87
		// (set) Token: 0x06000A42 RID: 2626 RVA: 0x0002698F File Offset: 0x00024B8F
		public BsonString Pattern { get; set; }

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000A43 RID: 2627 RVA: 0x00026998 File Offset: 0x00024B98
		// (set) Token: 0x06000A44 RID: 2628 RVA: 0x000269A0 File Offset: 0x00024BA0
		public BsonString Options { get; set; }

		// Token: 0x06000A45 RID: 2629 RVA: 0x000269A9 File Offset: 0x00024BA9
		public BsonRegex(string pattern, string options)
		{
			this.Pattern = new BsonString(pattern, false);
			this.Options = new BsonString(options, false);
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000A46 RID: 2630 RVA: 0x000269CB File Offset: 0x00024BCB
		public override BsonType Type
		{
			get
			{
				return BsonType.Regex;
			}
		}
	}
}
