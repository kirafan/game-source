﻿using System;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000E4 RID: 228
	[Preserve]
	public class BsonObjectId
	{
		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000A77 RID: 2679 RVA: 0x00026E8E File Offset: 0x0002508E
		// (set) Token: 0x06000A78 RID: 2680 RVA: 0x00026E96 File Offset: 0x00025096
		public byte[] Value { get; private set; }

		// Token: 0x06000A79 RID: 2681 RVA: 0x00026E9F File Offset: 0x0002509F
		public BsonObjectId(byte[] value)
		{
			ValidationUtils.ArgumentNotNull(value, "value");
			if (value.Length != 12)
			{
				throw new ArgumentException("An ObjectId must be 12 bytes", "value");
			}
			this.Value = value;
		}
	}
}
