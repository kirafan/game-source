﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000DE RID: 222
	[Preserve]
	internal class BsonString : BsonValue
	{
		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000A39 RID: 2617 RVA: 0x00026932 File Offset: 0x00024B32
		// (set) Token: 0x06000A3A RID: 2618 RVA: 0x0002693A File Offset: 0x00024B3A
		public int ByteCount { get; set; }

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000A3B RID: 2619 RVA: 0x00026943 File Offset: 0x00024B43
		// (set) Token: 0x06000A3C RID: 2620 RVA: 0x0002694B File Offset: 0x00024B4B
		public bool IncludeLength { get; set; }

		// Token: 0x06000A3D RID: 2621 RVA: 0x00026954 File Offset: 0x00024B54
		public BsonString(object value, bool includeLength) : base(value, BsonType.String)
		{
			this.IncludeLength = includeLength;
		}
	}
}
