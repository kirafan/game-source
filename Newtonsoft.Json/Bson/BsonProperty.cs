﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000E1 RID: 225
	[Preserve]
	internal class BsonProperty
	{
		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000A47 RID: 2631 RVA: 0x000269CF File Offset: 0x00024BCF
		// (set) Token: 0x06000A48 RID: 2632 RVA: 0x000269D7 File Offset: 0x00024BD7
		public BsonString Name { get; set; }

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000A49 RID: 2633 RVA: 0x000269E0 File Offset: 0x00024BE0
		// (set) Token: 0x06000A4A RID: 2634 RVA: 0x000269E8 File Offset: 0x00024BE8
		public BsonToken Value { get; set; }
	}
}
