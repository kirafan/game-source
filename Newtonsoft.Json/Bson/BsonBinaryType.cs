﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Bson
{
	// Token: 0x020000D7 RID: 215
	[Preserve]
	internal enum BsonBinaryType : byte
	{
		// Token: 0x0400030B RID: 779
		Binary,
		// Token: 0x0400030C RID: 780
		Function,
		// Token: 0x0400030D RID: 781
		[Obsolete("This type has been deprecated in the BSON specification. Use Binary instead.")]
		BinaryOld,
		// Token: 0x0400030E RID: 782
		[Obsolete("This type has been deprecated in the BSON specification. Use Uuid instead.")]
		UuidOld,
		// Token: 0x0400030F RID: 783
		Uuid,
		// Token: 0x04000310 RID: 784
		Md5,
		// Token: 0x04000311 RID: 785
		UserDefined = 128
	}
}
