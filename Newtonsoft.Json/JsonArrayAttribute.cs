﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200001F RID: 31
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonArrayAttribute : JsonContainerAttribute
	{
		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000047 RID: 71 RVA: 0x000024E9 File Offset: 0x000006E9
		// (set) Token: 0x06000048 RID: 72 RVA: 0x000024F1 File Offset: 0x000006F1
		public bool AllowNullItems
		{
			get
			{
				return this._allowNullItems;
			}
			set
			{
				this._allowNullItems = value;
			}
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000021CA File Offset: 0x000003CA
		public JsonArrayAttribute()
		{
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000024FA File Offset: 0x000006FA
		public JsonArrayAttribute(bool allowNullItems)
		{
			this._allowNullItems = allowNullItems;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x000021D2 File Offset: 0x000003D2
		public JsonArrayAttribute(string id) : base(id)
		{
		}

		// Token: 0x04000045 RID: 69
		private bool _allowNullItems;
	}
}
