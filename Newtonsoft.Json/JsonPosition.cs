﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json
{
	// Token: 0x02000018 RID: 24
	[Preserve]
	internal struct JsonPosition
	{
		// Token: 0x0600003C RID: 60 RVA: 0x0000224A File Offset: 0x0000044A
		public JsonPosition(JsonContainerType type)
		{
			this.Type = type;
			this.HasIndex = JsonPosition.TypeHasIndex(type);
			this.Position = -1;
			this.PropertyName = null;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00002270 File Offset: 0x00000470
		internal int CalculateLength()
		{
			switch (this.Type)
			{
			case JsonContainerType.Object:
				return this.PropertyName.Length + 5;
			case JsonContainerType.Array:
			case JsonContainerType.Constructor:
				return MathUtils.IntLength((ulong)((long)this.Position)) + 2;
			default:
				throw new ArgumentOutOfRangeException("Type");
			}
		}

		// Token: 0x0600003E RID: 62 RVA: 0x000022C4 File Offset: 0x000004C4
		internal void WriteTo(StringBuilder sb)
		{
			switch (this.Type)
			{
			case JsonContainerType.Object:
			{
				string propertyName = this.PropertyName;
				if (propertyName.IndexOfAny(JsonPosition.SpecialCharacters) != -1)
				{
					sb.Append("['");
					sb.Append(propertyName);
					sb.Append("']");
					return;
				}
				if (sb.Length > 0)
				{
					sb.Append('.');
				}
				sb.Append(propertyName);
				return;
			}
			case JsonContainerType.Array:
			case JsonContainerType.Constructor:
				sb.Append('[');
				sb.Append(this.Position);
				sb.Append(']');
				return;
			default:
				return;
			}
		}

		// Token: 0x0600003F RID: 63 RVA: 0x0000235D File Offset: 0x0000055D
		internal static bool TypeHasIndex(JsonContainerType type)
		{
			return type == JsonContainerType.Array || type == JsonContainerType.Constructor;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x0000236C File Offset: 0x0000056C
		internal static string BuildPath(List<JsonPosition> positions, JsonPosition? currentPosition)
		{
			int num = 0;
			if (positions != null)
			{
				for (int i = 0; i < positions.Count; i++)
				{
					num += positions[i].CalculateLength();
				}
			}
			if (currentPosition != null)
			{
				num += currentPosition.GetValueOrDefault().CalculateLength();
			}
			StringBuilder stringBuilder = new StringBuilder(num);
			if (positions != null)
			{
				foreach (JsonPosition jsonPosition in positions)
				{
					jsonPosition.WriteTo(stringBuilder);
				}
			}
			if (currentPosition != null)
			{
				currentPosition.GetValueOrDefault().WriteTo(stringBuilder);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000242C File Offset: 0x0000062C
		internal static string FormatMessage(IJsonLineInfo lineInfo, string path, string message)
		{
			if (!message.EndsWith(Environment.NewLine, StringComparison.Ordinal))
			{
				message = message.Trim();
				if (!message.EndsWith('.'))
				{
					message += ".";
				}
				message += " ";
			}
			message += "Path '{0}'".FormatWith(CultureInfo.InvariantCulture, path);
			if (lineInfo != null && lineInfo.HasLineInfo())
			{
				message += ", line {0}, position {1}".FormatWith(CultureInfo.InvariantCulture, lineInfo.LineNumber, lineInfo.LinePosition);
			}
			message += ".";
			return message;
		}

		// Token: 0x0400002E RID: 46
		private static readonly char[] SpecialCharacters = new char[]
		{
			'.',
			' ',
			'[',
			']',
			'(',
			')'
		};

		// Token: 0x0400002F RID: 47
		internal JsonContainerType Type;

		// Token: 0x04000030 RID: 48
		internal int Position;

		// Token: 0x04000031 RID: 49
		internal string PropertyName;

		// Token: 0x04000032 RID: 50
		internal bool HasIndex;
	}
}
