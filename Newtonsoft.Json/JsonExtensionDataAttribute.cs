﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000016 RID: 22
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
	[Preserve]
	public class JsonExtensionDataAttribute : Attribute
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002212 File Offset: 0x00000412
		// (set) Token: 0x06000038 RID: 56 RVA: 0x0000221A File Offset: 0x0000041A
		public bool WriteData { get; set; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002223 File Offset: 0x00000423
		// (set) Token: 0x0600003A RID: 58 RVA: 0x0000222B File Offset: 0x0000042B
		public bool ReadData { get; set; }

		// Token: 0x0600003B RID: 59 RVA: 0x00002234 File Offset: 0x00000434
		public JsonExtensionDataAttribute()
		{
			this.WriteData = true;
			this.ReadData = true;
		}
	}
}
