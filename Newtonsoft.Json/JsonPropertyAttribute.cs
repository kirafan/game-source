﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000029 RID: 41
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonPropertyAttribute : Attribute
	{
		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000E7 RID: 231 RVA: 0x00005AD8 File Offset: 0x00003CD8
		// (set) Token: 0x060000E8 RID: 232 RVA: 0x00005AE0 File Offset: 0x00003CE0
		public Type ItemConverterType { get; set; }

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000E9 RID: 233 RVA: 0x00005AE9 File Offset: 0x00003CE9
		// (set) Token: 0x060000EA RID: 234 RVA: 0x00005AF1 File Offset: 0x00003CF1
		public object[] ItemConverterParameters { get; set; }

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000EB RID: 235 RVA: 0x00005AFC File Offset: 0x00003CFC
		// (set) Token: 0x060000EC RID: 236 RVA: 0x00005B22 File Offset: 0x00003D22
		public NullValueHandling NullValueHandling
		{
			get
			{
				NullValueHandling? nullValueHandling = this._nullValueHandling;
				if (nullValueHandling == null)
				{
					return NullValueHandling.Include;
				}
				return nullValueHandling.GetValueOrDefault();
			}
			set
			{
				this._nullValueHandling = new NullValueHandling?(value);
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060000ED RID: 237 RVA: 0x00005B30 File Offset: 0x00003D30
		// (set) Token: 0x060000EE RID: 238 RVA: 0x00005B56 File Offset: 0x00003D56
		public DefaultValueHandling DefaultValueHandling
		{
			get
			{
				DefaultValueHandling? defaultValueHandling = this._defaultValueHandling;
				if (defaultValueHandling == null)
				{
					return DefaultValueHandling.Include;
				}
				return defaultValueHandling.GetValueOrDefault();
			}
			set
			{
				this._defaultValueHandling = new DefaultValueHandling?(value);
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000EF RID: 239 RVA: 0x00005B64 File Offset: 0x00003D64
		// (set) Token: 0x060000F0 RID: 240 RVA: 0x00005B8A File Offset: 0x00003D8A
		public ReferenceLoopHandling ReferenceLoopHandling
		{
			get
			{
				ReferenceLoopHandling? referenceLoopHandling = this._referenceLoopHandling;
				if (referenceLoopHandling == null)
				{
					return ReferenceLoopHandling.Error;
				}
				return referenceLoopHandling.GetValueOrDefault();
			}
			set
			{
				this._referenceLoopHandling = new ReferenceLoopHandling?(value);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x00005B98 File Offset: 0x00003D98
		// (set) Token: 0x060000F2 RID: 242 RVA: 0x00005BBE File Offset: 0x00003DBE
		public ObjectCreationHandling ObjectCreationHandling
		{
			get
			{
				ObjectCreationHandling? objectCreationHandling = this._objectCreationHandling;
				if (objectCreationHandling == null)
				{
					return ObjectCreationHandling.Auto;
				}
				return objectCreationHandling.GetValueOrDefault();
			}
			set
			{
				this._objectCreationHandling = new ObjectCreationHandling?(value);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00005BCC File Offset: 0x00003DCC
		// (set) Token: 0x060000F4 RID: 244 RVA: 0x00005BF2 File Offset: 0x00003DF2
		public TypeNameHandling TypeNameHandling
		{
			get
			{
				TypeNameHandling? typeNameHandling = this._typeNameHandling;
				if (typeNameHandling == null)
				{
					return TypeNameHandling.None;
				}
				return typeNameHandling.GetValueOrDefault();
			}
			set
			{
				this._typeNameHandling = new TypeNameHandling?(value);
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00005C00 File Offset: 0x00003E00
		// (set) Token: 0x060000F6 RID: 246 RVA: 0x00005C26 File Offset: 0x00003E26
		public bool IsReference
		{
			get
			{
				return this._isReference ?? false;
			}
			set
			{
				this._isReference = new bool?(value);
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00005C34 File Offset: 0x00003E34
		// (set) Token: 0x060000F8 RID: 248 RVA: 0x00005C5A File Offset: 0x00003E5A
		public int Order
		{
			get
			{
				int? order = this._order;
				if (order == null)
				{
					return 0;
				}
				return order.GetValueOrDefault();
			}
			set
			{
				this._order = new int?(value);
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060000F9 RID: 249 RVA: 0x00005C68 File Offset: 0x00003E68
		// (set) Token: 0x060000FA RID: 250 RVA: 0x00005C8E File Offset: 0x00003E8E
		public Required Required
		{
			get
			{
				Required? required = this._required;
				if (required == null)
				{
					return Required.Default;
				}
				return required.GetValueOrDefault();
			}
			set
			{
				this._required = new Required?(value);
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060000FB RID: 251 RVA: 0x00005C9C File Offset: 0x00003E9C
		// (set) Token: 0x060000FC RID: 252 RVA: 0x00005CA4 File Offset: 0x00003EA4
		public string PropertyName { get; set; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060000FD RID: 253 RVA: 0x00005CB0 File Offset: 0x00003EB0
		// (set) Token: 0x060000FE RID: 254 RVA: 0x00005CD6 File Offset: 0x00003ED6
		public ReferenceLoopHandling ItemReferenceLoopHandling
		{
			get
			{
				ReferenceLoopHandling? itemReferenceLoopHandling = this._itemReferenceLoopHandling;
				if (itemReferenceLoopHandling == null)
				{
					return ReferenceLoopHandling.Error;
				}
				return itemReferenceLoopHandling.GetValueOrDefault();
			}
			set
			{
				this._itemReferenceLoopHandling = new ReferenceLoopHandling?(value);
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060000FF RID: 255 RVA: 0x00005CE4 File Offset: 0x00003EE4
		// (set) Token: 0x06000100 RID: 256 RVA: 0x00005D0A File Offset: 0x00003F0A
		public TypeNameHandling ItemTypeNameHandling
		{
			get
			{
				TypeNameHandling? itemTypeNameHandling = this._itemTypeNameHandling;
				if (itemTypeNameHandling == null)
				{
					return TypeNameHandling.None;
				}
				return itemTypeNameHandling.GetValueOrDefault();
			}
			set
			{
				this._itemTypeNameHandling = new TypeNameHandling?(value);
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000101 RID: 257 RVA: 0x00005D18 File Offset: 0x00003F18
		// (set) Token: 0x06000102 RID: 258 RVA: 0x00005D3E File Offset: 0x00003F3E
		public bool ItemIsReference
		{
			get
			{
				return this._itemIsReference ?? false;
			}
			set
			{
				this._itemIsReference = new bool?(value);
			}
		}

		// Token: 0x06000103 RID: 259 RVA: 0x000021C2 File Offset: 0x000003C2
		public JsonPropertyAttribute()
		{
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00005D4C File Offset: 0x00003F4C
		public JsonPropertyAttribute(string propertyName)
		{
			this.PropertyName = propertyName;
		}

		// Token: 0x040000AC RID: 172
		internal NullValueHandling? _nullValueHandling;

		// Token: 0x040000AD RID: 173
		internal DefaultValueHandling? _defaultValueHandling;

		// Token: 0x040000AE RID: 174
		internal ReferenceLoopHandling? _referenceLoopHandling;

		// Token: 0x040000AF RID: 175
		internal ObjectCreationHandling? _objectCreationHandling;

		// Token: 0x040000B0 RID: 176
		internal TypeNameHandling? _typeNameHandling;

		// Token: 0x040000B1 RID: 177
		internal bool? _isReference;

		// Token: 0x040000B2 RID: 178
		internal int? _order;

		// Token: 0x040000B3 RID: 179
		internal Required? _required;

		// Token: 0x040000B4 RID: 180
		internal bool? _itemIsReference;

		// Token: 0x040000B5 RID: 181
		internal ReferenceLoopHandling? _itemReferenceLoopHandling;

		// Token: 0x040000B6 RID: 182
		internal TypeNameHandling? _itemTypeNameHandling;
	}
}
