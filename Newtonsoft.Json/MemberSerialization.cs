﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000025 RID: 37
	[Preserve]
	public enum MemberSerialization
	{
		// Token: 0x0400008E RID: 142
		OptOut,
		// Token: 0x0400008F RID: 143
		OptIn,
		// Token: 0x04000090 RID: 144
		Fields
	}
}
