﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json
{
	// Token: 0x0200002B RID: 43
	[Preserve]
	public class JsonTextWriter : JsonWriter
	{
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000106 RID: 262 RVA: 0x00005D5B File Offset: 0x00003F5B
		private Base64Encoder Base64Encoder
		{
			get
			{
				if (this._base64Encoder == null)
				{
					this._base64Encoder = new Base64Encoder(this._writer);
				}
				return this._base64Encoder;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000107 RID: 263 RVA: 0x00005D7C File Offset: 0x00003F7C
		// (set) Token: 0x06000108 RID: 264 RVA: 0x00005D84 File Offset: 0x00003F84
		public IArrayPool<char> ArrayPool
		{
			get
			{
				return this._arrayPool;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this._arrayPool = value;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000109 RID: 265 RVA: 0x00005D9B File Offset: 0x00003F9B
		// (set) Token: 0x0600010A RID: 266 RVA: 0x00005DA3 File Offset: 0x00003FA3
		public int Indentation
		{
			get
			{
				return this._indentation;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentException("Indentation value must be greater than 0.");
				}
				this._indentation = value;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00005DBB File Offset: 0x00003FBB
		// (set) Token: 0x0600010C RID: 268 RVA: 0x00005DC3 File Offset: 0x00003FC3
		public char QuoteChar
		{
			get
			{
				return this._quoteChar;
			}
			set
			{
				if (value != '"' && value != '\'')
				{
					throw new ArgumentException("Invalid JavaScript string quote character. Valid quote characters are ' and \".");
				}
				this._quoteChar = value;
				this.UpdateCharEscapeFlags();
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600010D RID: 269 RVA: 0x00005DE7 File Offset: 0x00003FE7
		// (set) Token: 0x0600010E RID: 270 RVA: 0x00005DEF File Offset: 0x00003FEF
		public char IndentChar
		{
			get
			{
				return this._indentChar;
			}
			set
			{
				if (value != this._indentChar)
				{
					this._indentChar = value;
					this._indentChars = null;
				}
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00005E08 File Offset: 0x00004008
		// (set) Token: 0x06000110 RID: 272 RVA: 0x00005E10 File Offset: 0x00004010
		public bool QuoteName
		{
			get
			{
				return this._quoteName;
			}
			set
			{
				this._quoteName = value;
			}
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00005E1C File Offset: 0x0000401C
		public JsonTextWriter(TextWriter textWriter)
		{
			if (textWriter == null)
			{
				throw new ArgumentNullException("textWriter");
			}
			this._writer = textWriter;
			this._quoteChar = '"';
			this._quoteName = true;
			this._indentChar = ' ';
			this._indentation = 2;
			this.UpdateCharEscapeFlags();
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00005E68 File Offset: 0x00004068
		public override void Flush()
		{
			this._writer.Flush();
		}

		// Token: 0x06000113 RID: 275 RVA: 0x00005E78 File Offset: 0x00004078
		public override void Close()
		{
			base.Close();
			if (this._writeBuffer != null)
			{
				BufferUtils.ReturnBuffer(this._arrayPool, this._writeBuffer);
				this._writeBuffer = null;
			}
			if (base.CloseOutput && this._writer != null)
			{
				this._writer.Close();
			}
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00005EC6 File Offset: 0x000040C6
		public override void WriteStartObject()
		{
			base.InternalWriteStart(JsonToken.StartObject, JsonContainerType.Object);
			this._writer.Write('{');
		}

		// Token: 0x06000115 RID: 277 RVA: 0x00005EDD File Offset: 0x000040DD
		public override void WriteStartArray()
		{
			base.InternalWriteStart(JsonToken.StartArray, JsonContainerType.Array);
			this._writer.Write('[');
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00005EF4 File Offset: 0x000040F4
		public override void WriteStartConstructor(string name)
		{
			base.InternalWriteStart(JsonToken.StartConstructor, JsonContainerType.Constructor);
			this._writer.Write("new ");
			this._writer.Write(name);
			this._writer.Write('(');
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00005F28 File Offset: 0x00004128
		protected override void WriteEnd(JsonToken token)
		{
			switch (token)
			{
			case JsonToken.EndObject:
				this._writer.Write('}');
				return;
			case JsonToken.EndArray:
				this._writer.Write(']');
				return;
			case JsonToken.EndConstructor:
				this._writer.Write(')');
				return;
			default:
				throw JsonWriterException.Create(this, "Invalid JsonToken: " + token, null);
			}
		}

		// Token: 0x06000118 RID: 280 RVA: 0x00005F8D File Offset: 0x0000418D
		public override void WritePropertyName(string name)
		{
			base.InternalWritePropertyName(name);
			this.WriteEscapedString(name, this._quoteName);
			this._writer.Write(':');
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00005FB0 File Offset: 0x000041B0
		public override void WritePropertyName(string name, bool escape)
		{
			base.InternalWritePropertyName(name);
			if (escape)
			{
				this.WriteEscapedString(name, this._quoteName);
			}
			else
			{
				if (this._quoteName)
				{
					this._writer.Write(this._quoteChar);
				}
				this._writer.Write(name);
				if (this._quoteName)
				{
					this._writer.Write(this._quoteChar);
				}
			}
			this._writer.Write(':');
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00006021 File Offset: 0x00004221
		internal override void OnStringEscapeHandlingChanged()
		{
			this.UpdateCharEscapeFlags();
		}

		// Token: 0x0600011B RID: 283 RVA: 0x00006029 File Offset: 0x00004229
		private void UpdateCharEscapeFlags()
		{
			this._charEscapeFlags = JavaScriptUtils.GetCharEscapeFlags(base.StringEscapeHandling, this._quoteChar);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x00006044 File Offset: 0x00004244
		protected override void WriteIndent()
		{
			this._writer.WriteLine();
			int i = base.Top * this._indentation;
			if (i > 0)
			{
				if (this._indentChars == null)
				{
					this._indentChars = new string(this._indentChar, 10).ToCharArray();
				}
				while (i > 0)
				{
					int num = Math.Min(i, 10);
					this._writer.Write(this._indentChars, 0, num);
					i -= num;
				}
			}
		}

		// Token: 0x0600011D RID: 285 RVA: 0x000060B4 File Offset: 0x000042B4
		protected override void WriteValueDelimiter()
		{
			this._writer.Write(',');
		}

		// Token: 0x0600011E RID: 286 RVA: 0x000060C3 File Offset: 0x000042C3
		protected override void WriteIndentSpace()
		{
			this._writer.Write(' ');
		}

		// Token: 0x0600011F RID: 287 RVA: 0x000060D2 File Offset: 0x000042D2
		private void WriteValueInternal(string value, JsonToken token)
		{
			this._writer.Write(value);
		}

		// Token: 0x06000120 RID: 288 RVA: 0x000060E0 File Offset: 0x000042E0
		public override void WriteValue(object value)
		{
			base.WriteValue(value);
		}

		// Token: 0x06000121 RID: 289 RVA: 0x000060E9 File Offset: 0x000042E9
		public override void WriteNull()
		{
			base.InternalWriteValue(JsonToken.Null);
			this.WriteValueInternal(JsonConvert.Null, JsonToken.Null);
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00006100 File Offset: 0x00004300
		public override void WriteUndefined()
		{
			base.InternalWriteValue(JsonToken.Undefined);
			this.WriteValueInternal(JsonConvert.Undefined, JsonToken.Undefined);
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00006117 File Offset: 0x00004317
		public override void WriteRaw(string json)
		{
			base.InternalWriteRaw();
			this._writer.Write(json);
		}

		// Token: 0x06000124 RID: 292 RVA: 0x0000612B File Offset: 0x0000432B
		public override void WriteValue(string value)
		{
			base.InternalWriteValue(JsonToken.String);
			if (value == null)
			{
				this.WriteValueInternal(JsonConvert.Null, JsonToken.Null);
				return;
			}
			this.WriteEscapedString(value, true);
		}

		// Token: 0x06000125 RID: 293 RVA: 0x0000614E File Offset: 0x0000434E
		private void WriteEscapedString(string value, bool quote)
		{
			this.EnsureWriteBuffer();
			JavaScriptUtils.WriteEscapedJavaScriptString(this._writer, value, this._quoteChar, quote, this._charEscapeFlags, base.StringEscapeHandling, this._arrayPool, ref this._writeBuffer);
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00006181 File Offset: 0x00004381
		public override void WriteValue(int value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)value);
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00006192 File Offset: 0x00004392
		[CLSCompliant(false)]
		public override void WriteValue(uint value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)((ulong)value));
		}

		// Token: 0x06000128 RID: 296 RVA: 0x000061A3 File Offset: 0x000043A3
		public override void WriteValue(long value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue(value);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x000061B3 File Offset: 0x000043B3
		[CLSCompliant(false)]
		public override void WriteValue(ulong value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue(value);
		}

		// Token: 0x0600012A RID: 298 RVA: 0x000061C3 File Offset: 0x000043C3
		public override void WriteValue(float value)
		{
			base.InternalWriteValue(JsonToken.Float);
			this.WriteValueInternal(JsonConvert.ToString(value, base.FloatFormatHandling, this.QuoteChar, false), JsonToken.Float);
		}

		// Token: 0x0600012B RID: 299 RVA: 0x000061E6 File Offset: 0x000043E6
		public override void WriteValue(float? value)
		{
			if (value == null)
			{
				this.WriteNull();
				return;
			}
			base.InternalWriteValue(JsonToken.Float);
			this.WriteValueInternal(JsonConvert.ToString(value.GetValueOrDefault(), base.FloatFormatHandling, this.QuoteChar, true), JsonToken.Float);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x0000621F File Offset: 0x0000441F
		public override void WriteValue(double value)
		{
			base.InternalWriteValue(JsonToken.Float);
			this.WriteValueInternal(JsonConvert.ToString(value, base.FloatFormatHandling, this.QuoteChar, false), JsonToken.Float);
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00006242 File Offset: 0x00004442
		public override void WriteValue(double? value)
		{
			if (value == null)
			{
				this.WriteNull();
				return;
			}
			base.InternalWriteValue(JsonToken.Float);
			this.WriteValueInternal(JsonConvert.ToString(value.GetValueOrDefault(), base.FloatFormatHandling, this.QuoteChar, true), JsonToken.Float);
		}

		// Token: 0x0600012E RID: 302 RVA: 0x0000627B File Offset: 0x0000447B
		public override void WriteValue(bool value)
		{
			base.InternalWriteValue(JsonToken.Boolean);
			this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Boolean);
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00006181 File Offset: 0x00004381
		public override void WriteValue(short value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)value);
		}

		// Token: 0x06000130 RID: 304 RVA: 0x00006192 File Offset: 0x00004392
		[CLSCompliant(false)]
		public override void WriteValue(ushort value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)((ulong)value));
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00006293 File Offset: 0x00004493
		public override void WriteValue(char value)
		{
			base.InternalWriteValue(JsonToken.String);
			this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.String);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00006192 File Offset: 0x00004392
		public override void WriteValue(byte value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)((ulong)value));
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00006181 File Offset: 0x00004381
		[CLSCompliant(false)]
		public override void WriteValue(sbyte value)
		{
			base.InternalWriteValue(JsonToken.Integer);
			this.WriteIntegerValue((long)value);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x000062AB File Offset: 0x000044AB
		public override void WriteValue(decimal value)
		{
			base.InternalWriteValue(JsonToken.Float);
			this.WriteValueInternal(JsonConvert.ToString(value), JsonToken.Float);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x000062C4 File Offset: 0x000044C4
		public override void WriteValue(DateTime value)
		{
			base.InternalWriteValue(JsonToken.Date);
			value = DateTimeUtils.EnsureDateTime(value, base.DateTimeZoneHandling);
			if (string.IsNullOrEmpty(base.DateFormatString))
			{
				this.EnsureWriteBuffer();
				int num = 0;
				this._writeBuffer[num++] = this._quoteChar;
				num = DateTimeUtils.WriteDateTimeString(this._writeBuffer, num, value, null, value.Kind, base.DateFormatHandling);
				this._writeBuffer[num++] = this._quoteChar;
				this._writer.Write(this._writeBuffer, 0, num);
				return;
			}
			this._writer.Write(this._quoteChar);
			this._writer.Write(value.ToString(base.DateFormatString, base.Culture));
			this._writer.Write(this._quoteChar);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x00006398 File Offset: 0x00004598
		public override void WriteValue(byte[] value)
		{
			if (value == null)
			{
				this.WriteNull();
				return;
			}
			base.InternalWriteValue(JsonToken.Bytes);
			this._writer.Write(this._quoteChar);
			this.Base64Encoder.Encode(value, 0, value.Length);
			this.Base64Encoder.Flush();
			this._writer.Write(this._quoteChar);
		}

		// Token: 0x06000137 RID: 311 RVA: 0x000063F4 File Offset: 0x000045F4
		public override void WriteValue(DateTimeOffset value)
		{
			base.InternalWriteValue(JsonToken.Date);
			if (string.IsNullOrEmpty(base.DateFormatString))
			{
				this.EnsureWriteBuffer();
				int num = 0;
				this._writeBuffer[num++] = this._quoteChar;
				num = DateTimeUtils.WriteDateTimeString(this._writeBuffer, num, (base.DateFormatHandling == DateFormatHandling.IsoDateFormat) ? value.DateTime : value.UtcDateTime, new TimeSpan?(value.Offset), DateTimeKind.Local, base.DateFormatHandling);
				this._writeBuffer[num++] = this._quoteChar;
				this._writer.Write(this._writeBuffer, 0, num);
				return;
			}
			this._writer.Write(this._quoteChar);
			this._writer.Write(value.ToString(base.DateFormatString, base.Culture));
			this._writer.Write(this._quoteChar);
		}

		// Token: 0x06000138 RID: 312 RVA: 0x000064D0 File Offset: 0x000046D0
		public override void WriteValue(Guid value)
		{
			base.InternalWriteValue(JsonToken.String);
			string value2 = value.ToString("D", CultureInfo.InvariantCulture);
			this._writer.Write(this._quoteChar);
			this._writer.Write(value2);
			this._writer.Write(this._quoteChar);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00006528 File Offset: 0x00004728
		public override void WriteValue(TimeSpan value)
		{
			base.InternalWriteValue(JsonToken.String);
			string value2 = value.ToString();
			this._writer.Write(this._quoteChar);
			this._writer.Write(value2);
			this._writer.Write(this._quoteChar);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00006579 File Offset: 0x00004779
		public override void WriteValue(Uri value)
		{
			if (value == null)
			{
				this.WriteNull();
				return;
			}
			base.InternalWriteValue(JsonToken.String);
			this.WriteEscapedString(value.OriginalString, true);
		}

		// Token: 0x0600013B RID: 315 RVA: 0x000065A0 File Offset: 0x000047A0
		public override void WriteComment(string text)
		{
			base.InternalWriteComment();
			this._writer.Write("/*");
			this._writer.Write(text);
			this._writer.Write("*/");
		}

		// Token: 0x0600013C RID: 316 RVA: 0x000065D4 File Offset: 0x000047D4
		public override void WriteWhitespace(string ws)
		{
			base.InternalWriteWhitespace(ws);
			this._writer.Write(ws);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000065E9 File Offset: 0x000047E9
		private void EnsureWriteBuffer()
		{
			if (this._writeBuffer == null)
			{
				this._writeBuffer = BufferUtils.RentBuffer(this._arrayPool, 35);
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00006608 File Offset: 0x00004808
		private void WriteIntegerValue(long value)
		{
			if (value >= 0L && value <= 9L)
			{
				this._writer.Write((char)(48L + value));
				return;
			}
			ulong uvalue = (ulong)((value < 0L) ? (-(ulong)value) : value);
			if (value < 0L)
			{
				this._writer.Write('-');
			}
			this.WriteIntegerValue(uvalue);
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00006658 File Offset: 0x00004858
		private void WriteIntegerValue(ulong uvalue)
		{
			if (uvalue <= 9UL)
			{
				this._writer.Write((char)(48UL + uvalue));
				return;
			}
			this.EnsureWriteBuffer();
			int num = MathUtils.IntLength(uvalue);
			int num2 = 0;
			do
			{
				this._writeBuffer[num - ++num2] = (char)(48UL + uvalue % 10UL);
				uvalue /= 10UL;
			}
			while (uvalue != 0UL);
			this._writer.Write(this._writeBuffer, 0, num2);
		}

		// Token: 0x040000BA RID: 186
		private readonly TextWriter _writer;

		// Token: 0x040000BB RID: 187
		private Base64Encoder _base64Encoder;

		// Token: 0x040000BC RID: 188
		private char _indentChar;

		// Token: 0x040000BD RID: 189
		private int _indentation;

		// Token: 0x040000BE RID: 190
		private char _quoteChar;

		// Token: 0x040000BF RID: 191
		private bool _quoteName;

		// Token: 0x040000C0 RID: 192
		private bool[] _charEscapeFlags;

		// Token: 0x040000C1 RID: 193
		private char[] _writeBuffer;

		// Token: 0x040000C2 RID: 194
		private IArrayPool<char> _arrayPool;

		// Token: 0x040000C3 RID: 195
		private char[] _indentChars;
	}
}
