﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200003A RID: 58
	[Preserve]
	public enum WriteState
	{
		// Token: 0x04000130 RID: 304
		Error,
		// Token: 0x04000131 RID: 305
		Closed,
		// Token: 0x04000132 RID: 306
		Object,
		// Token: 0x04000133 RID: 307
		Array,
		// Token: 0x04000134 RID: 308
		Constructor,
		// Token: 0x04000135 RID: 309
		Property,
		// Token: 0x04000136 RID: 310
		Start
	}
}
