﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200002D RID: 45
	[Preserve]
	[Serializable]
	public class JsonReaderException : JsonException
	{
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000149 RID: 329 RVA: 0x0000672B File Offset: 0x0000492B
		// (set) Token: 0x0600014A RID: 330 RVA: 0x00006733 File Offset: 0x00004933
		public int LineNumber { get; private set; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600014B RID: 331 RVA: 0x0000673C File Offset: 0x0000493C
		// (set) Token: 0x0600014C RID: 332 RVA: 0x00006744 File Offset: 0x00004944
		public int LinePosition { get; private set; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600014D RID: 333 RVA: 0x0000674D File Offset: 0x0000494D
		// (set) Token: 0x0600014E RID: 334 RVA: 0x00006755 File Offset: 0x00004955
		public string Path { get; private set; }

		// Token: 0x0600014F RID: 335 RVA: 0x000066D2 File Offset: 0x000048D2
		public JsonReaderException()
		{
		}

		// Token: 0x06000150 RID: 336 RVA: 0x000066DA File Offset: 0x000048DA
		public JsonReaderException(string message) : base(message)
		{
		}

		// Token: 0x06000151 RID: 337 RVA: 0x000066E3 File Offset: 0x000048E3
		public JsonReaderException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000152 RID: 338 RVA: 0x000066ED File Offset: 0x000048ED
		public JsonReaderException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000153 RID: 339 RVA: 0x0000675E File Offset: 0x0000495E
		internal JsonReaderException(string message, Exception innerException, string path, int lineNumber, int linePosition) : base(message, innerException)
		{
			this.Path = path;
			this.LineNumber = lineNumber;
			this.LinePosition = linePosition;
		}

		// Token: 0x06000154 RID: 340 RVA: 0x0000677F File Offset: 0x0000497F
		internal static JsonReaderException Create(JsonReader reader, string message)
		{
			return JsonReaderException.Create(reader, message, null);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00006789 File Offset: 0x00004989
		internal static JsonReaderException Create(JsonReader reader, string message, Exception ex)
		{
			return JsonReaderException.Create(reader as IJsonLineInfo, reader.Path, message, ex);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x000067A0 File Offset: 0x000049A0
		internal static JsonReaderException Create(IJsonLineInfo lineInfo, string path, string message, Exception ex)
		{
			message = JsonPosition.FormatMessage(lineInfo, path, message);
			int lineNumber;
			int linePosition;
			if (lineInfo != null && lineInfo.HasLineInfo())
			{
				lineNumber = lineInfo.LineNumber;
				linePosition = lineInfo.LinePosition;
			}
			else
			{
				lineNumber = 0;
				linePosition = 0;
			}
			return new JsonReaderException(message, ex, path, lineNumber, linePosition);
		}
	}
}
