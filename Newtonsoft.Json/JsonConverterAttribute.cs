﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000022 RID: 34
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface | AttributeTargets.Parameter, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonConverterAttribute : Attribute
	{
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000060 RID: 96 RVA: 0x0000263F File Offset: 0x0000083F
		public Type ConverterType
		{
			get
			{
				return this._converterType;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000061 RID: 97 RVA: 0x00002647 File Offset: 0x00000847
		// (set) Token: 0x06000062 RID: 98 RVA: 0x0000264F File Offset: 0x0000084F
		public object[] ConverterParameters { get; private set; }

		// Token: 0x06000063 RID: 99 RVA: 0x00002658 File Offset: 0x00000858
		public JsonConverterAttribute(Type converterType)
		{
			if (converterType == null)
			{
				throw new ArgumentNullException("converterType");
			}
			this._converterType = converterType;
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00002675 File Offset: 0x00000875
		public JsonConverterAttribute(Type converterType, params object[] converterParameters) : this(converterType)
		{
			this.ConverterParameters = converterParameters;
		}

		// Token: 0x04000054 RID: 84
		private readonly Type _converterType;
	}
}
