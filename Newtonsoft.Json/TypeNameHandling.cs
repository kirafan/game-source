﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000037 RID: 55
	[Flags]
	[Preserve]
	public enum TypeNameHandling
	{
		// Token: 0x0400010A RID: 266
		None = 0,
		// Token: 0x0400010B RID: 267
		Objects = 1,
		// Token: 0x0400010C RID: 268
		Arrays = 2,
		// Token: 0x0400010D RID: 269
		All = 3,
		// Token: 0x0400010E RID: 270
		Auto = 4
	}
}
