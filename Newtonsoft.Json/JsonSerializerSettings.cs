﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000024 RID: 36
	[Preserve]
	public class JsonSerializerSettings
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600006C RID: 108 RVA: 0x000026DC File Offset: 0x000008DC
		// (set) Token: 0x0600006D RID: 109 RVA: 0x00002702 File Offset: 0x00000902
		public ReferenceLoopHandling ReferenceLoopHandling
		{
			get
			{
				ReferenceLoopHandling? referenceLoopHandling = this._referenceLoopHandling;
				if (referenceLoopHandling == null)
				{
					return ReferenceLoopHandling.Error;
				}
				return referenceLoopHandling.GetValueOrDefault();
			}
			set
			{
				this._referenceLoopHandling = new ReferenceLoopHandling?(value);
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600006E RID: 110 RVA: 0x00002710 File Offset: 0x00000910
		// (set) Token: 0x0600006F RID: 111 RVA: 0x00002736 File Offset: 0x00000936
		public MissingMemberHandling MissingMemberHandling
		{
			get
			{
				MissingMemberHandling? missingMemberHandling = this._missingMemberHandling;
				if (missingMemberHandling == null)
				{
					return MissingMemberHandling.Ignore;
				}
				return missingMemberHandling.GetValueOrDefault();
			}
			set
			{
				this._missingMemberHandling = new MissingMemberHandling?(value);
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000070 RID: 112 RVA: 0x00002744 File Offset: 0x00000944
		// (set) Token: 0x06000071 RID: 113 RVA: 0x0000276A File Offset: 0x0000096A
		public ObjectCreationHandling ObjectCreationHandling
		{
			get
			{
				ObjectCreationHandling? objectCreationHandling = this._objectCreationHandling;
				if (objectCreationHandling == null)
				{
					return ObjectCreationHandling.Auto;
				}
				return objectCreationHandling.GetValueOrDefault();
			}
			set
			{
				this._objectCreationHandling = new ObjectCreationHandling?(value);
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002778 File Offset: 0x00000978
		// (set) Token: 0x06000073 RID: 115 RVA: 0x0000279E File Offset: 0x0000099E
		public NullValueHandling NullValueHandling
		{
			get
			{
				NullValueHandling? nullValueHandling = this._nullValueHandling;
				if (nullValueHandling == null)
				{
					return NullValueHandling.Include;
				}
				return nullValueHandling.GetValueOrDefault();
			}
			set
			{
				this._nullValueHandling = new NullValueHandling?(value);
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000074 RID: 116 RVA: 0x000027AC File Offset: 0x000009AC
		// (set) Token: 0x06000075 RID: 117 RVA: 0x000027D2 File Offset: 0x000009D2
		public DefaultValueHandling DefaultValueHandling
		{
			get
			{
				DefaultValueHandling? defaultValueHandling = this._defaultValueHandling;
				if (defaultValueHandling == null)
				{
					return DefaultValueHandling.Include;
				}
				return defaultValueHandling.GetValueOrDefault();
			}
			set
			{
				this._defaultValueHandling = new DefaultValueHandling?(value);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000076 RID: 118 RVA: 0x000027E0 File Offset: 0x000009E0
		// (set) Token: 0x06000077 RID: 119 RVA: 0x000027E8 File Offset: 0x000009E8
		public IList<JsonConverter> Converters { get; set; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000078 RID: 120 RVA: 0x000027F4 File Offset: 0x000009F4
		// (set) Token: 0x06000079 RID: 121 RVA: 0x0000281A File Offset: 0x00000A1A
		public PreserveReferencesHandling PreserveReferencesHandling
		{
			get
			{
				PreserveReferencesHandling? preserveReferencesHandling = this._preserveReferencesHandling;
				if (preserveReferencesHandling == null)
				{
					return PreserveReferencesHandling.None;
				}
				return preserveReferencesHandling.GetValueOrDefault();
			}
			set
			{
				this._preserveReferencesHandling = new PreserveReferencesHandling?(value);
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00002828 File Offset: 0x00000A28
		// (set) Token: 0x0600007B RID: 123 RVA: 0x0000284E File Offset: 0x00000A4E
		public TypeNameHandling TypeNameHandling
		{
			get
			{
				TypeNameHandling? typeNameHandling = this._typeNameHandling;
				if (typeNameHandling == null)
				{
					return TypeNameHandling.None;
				}
				return typeNameHandling.GetValueOrDefault();
			}
			set
			{
				this._typeNameHandling = new TypeNameHandling?(value);
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600007C RID: 124 RVA: 0x0000285C File Offset: 0x00000A5C
		// (set) Token: 0x0600007D RID: 125 RVA: 0x00002882 File Offset: 0x00000A82
		public MetadataPropertyHandling MetadataPropertyHandling
		{
			get
			{
				MetadataPropertyHandling? metadataPropertyHandling = this._metadataPropertyHandling;
				if (metadataPropertyHandling == null)
				{
					return MetadataPropertyHandling.Default;
				}
				return metadataPropertyHandling.GetValueOrDefault();
			}
			set
			{
				this._metadataPropertyHandling = new MetadataPropertyHandling?(value);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00002890 File Offset: 0x00000A90
		// (set) Token: 0x0600007F RID: 127 RVA: 0x000028B6 File Offset: 0x00000AB6
		public FormatterAssemblyStyle TypeNameAssemblyFormat
		{
			get
			{
				FormatterAssemblyStyle? typeNameAssemblyFormat = this._typeNameAssemblyFormat;
				if (typeNameAssemblyFormat == null)
				{
					return FormatterAssemblyStyle.Simple;
				}
				return typeNameAssemblyFormat.GetValueOrDefault();
			}
			set
			{
				this._typeNameAssemblyFormat = new FormatterAssemblyStyle?(value);
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000080 RID: 128 RVA: 0x000028C4 File Offset: 0x00000AC4
		// (set) Token: 0x06000081 RID: 129 RVA: 0x000028EA File Offset: 0x00000AEA
		public ConstructorHandling ConstructorHandling
		{
			get
			{
				ConstructorHandling? constructorHandling = this._constructorHandling;
				if (constructorHandling == null)
				{
					return ConstructorHandling.Default;
				}
				return constructorHandling.GetValueOrDefault();
			}
			set
			{
				this._constructorHandling = new ConstructorHandling?(value);
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000082 RID: 130 RVA: 0x000028F8 File Offset: 0x00000AF8
		// (set) Token: 0x06000083 RID: 131 RVA: 0x00002900 File Offset: 0x00000B00
		public IContractResolver ContractResolver { get; set; }

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000084 RID: 132 RVA: 0x00002909 File Offset: 0x00000B09
		// (set) Token: 0x06000085 RID: 133 RVA: 0x00002911 File Offset: 0x00000B11
		public IEqualityComparer EqualityComparer { get; set; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000086 RID: 134 RVA: 0x0000291A File Offset: 0x00000B1A
		// (set) Token: 0x06000087 RID: 135 RVA: 0x00002934 File Offset: 0x00000B34
		[Obsolete("ReferenceResolver property is obsolete. Use the ReferenceResolverProvider property to set the IReferenceResolver: settings.ReferenceResolverProvider = () => resolver")]
		public IReferenceResolver ReferenceResolver
		{
			get
			{
				if (this.ReferenceResolverProvider == null)
				{
					return null;
				}
				return this.ReferenceResolverProvider();
			}
			set
			{
				this.ReferenceResolverProvider = ((value != null) ? (() => value) : null);
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000088 RID: 136 RVA: 0x0000296B File Offset: 0x00000B6B
		// (set) Token: 0x06000089 RID: 137 RVA: 0x00002973 File Offset: 0x00000B73
		public Func<IReferenceResolver> ReferenceResolverProvider { get; set; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600008A RID: 138 RVA: 0x0000297C File Offset: 0x00000B7C
		// (set) Token: 0x0600008B RID: 139 RVA: 0x00002984 File Offset: 0x00000B84
		public ITraceWriter TraceWriter { get; set; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600008C RID: 140 RVA: 0x0000298D File Offset: 0x00000B8D
		// (set) Token: 0x0600008D RID: 141 RVA: 0x00002995 File Offset: 0x00000B95
		public SerializationBinder Binder { get; set; }

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600008E RID: 142 RVA: 0x0000299E File Offset: 0x00000B9E
		// (set) Token: 0x0600008F RID: 143 RVA: 0x000029A6 File Offset: 0x00000BA6
		public EventHandler<ErrorEventArgs> Error { get; set; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000090 RID: 144 RVA: 0x000029B0 File Offset: 0x00000BB0
		// (set) Token: 0x06000091 RID: 145 RVA: 0x000029DA File Offset: 0x00000BDA
		public StreamingContext Context
		{
			get
			{
				StreamingContext? context = this._context;
				if (context == null)
				{
					return JsonSerializerSettings.DefaultContext;
				}
				return context.GetValueOrDefault();
			}
			set
			{
				this._context = new StreamingContext?(value);
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000092 RID: 146 RVA: 0x000029E8 File Offset: 0x00000BE8
		// (set) Token: 0x06000093 RID: 147 RVA: 0x000029F9 File Offset: 0x00000BF9
		public string DateFormatString
		{
			get
			{
				return this._dateFormatString ?? "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK";
			}
			set
			{
				this._dateFormatString = value;
				this._dateFormatStringSet = true;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00002A09 File Offset: 0x00000C09
		// (set) Token: 0x06000095 RID: 149 RVA: 0x00002A14 File Offset: 0x00000C14
		public int? MaxDepth
		{
			get
			{
				return this._maxDepth;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentException("Value must be positive.", "value");
				}
				this._maxDepth = value;
				this._maxDepthSet = true;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000096 RID: 150 RVA: 0x00002A5C File Offset: 0x00000C5C
		// (set) Token: 0x06000097 RID: 151 RVA: 0x00002A82 File Offset: 0x00000C82
		public Formatting Formatting
		{
			get
			{
				Formatting? formatting = this._formatting;
				if (formatting == null)
				{
					return Formatting.None;
				}
				return formatting.GetValueOrDefault();
			}
			set
			{
				this._formatting = new Formatting?(value);
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000098 RID: 152 RVA: 0x00002A90 File Offset: 0x00000C90
		// (set) Token: 0x06000099 RID: 153 RVA: 0x00002AB6 File Offset: 0x00000CB6
		public DateFormatHandling DateFormatHandling
		{
			get
			{
				DateFormatHandling? dateFormatHandling = this._dateFormatHandling;
				if (dateFormatHandling == null)
				{
					return DateFormatHandling.IsoDateFormat;
				}
				return dateFormatHandling.GetValueOrDefault();
			}
			set
			{
				this._dateFormatHandling = new DateFormatHandling?(value);
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00002AC4 File Offset: 0x00000CC4
		// (set) Token: 0x0600009B RID: 155 RVA: 0x00002AEA File Offset: 0x00000CEA
		public DateTimeZoneHandling DateTimeZoneHandling
		{
			get
			{
				DateTimeZoneHandling? dateTimeZoneHandling = this._dateTimeZoneHandling;
				if (dateTimeZoneHandling == null)
				{
					return DateTimeZoneHandling.RoundtripKind;
				}
				return dateTimeZoneHandling.GetValueOrDefault();
			}
			set
			{
				this._dateTimeZoneHandling = new DateTimeZoneHandling?(value);
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600009C RID: 156 RVA: 0x00002AF8 File Offset: 0x00000CF8
		// (set) Token: 0x0600009D RID: 157 RVA: 0x00002B1E File Offset: 0x00000D1E
		public DateParseHandling DateParseHandling
		{
			get
			{
				DateParseHandling? dateParseHandling = this._dateParseHandling;
				if (dateParseHandling == null)
				{
					return DateParseHandling.DateTime;
				}
				return dateParseHandling.GetValueOrDefault();
			}
			set
			{
				this._dateParseHandling = new DateParseHandling?(value);
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00002B2C File Offset: 0x00000D2C
		// (set) Token: 0x0600009F RID: 159 RVA: 0x00002B52 File Offset: 0x00000D52
		public FloatFormatHandling FloatFormatHandling
		{
			get
			{
				FloatFormatHandling? floatFormatHandling = this._floatFormatHandling;
				if (floatFormatHandling == null)
				{
					return FloatFormatHandling.String;
				}
				return floatFormatHandling.GetValueOrDefault();
			}
			set
			{
				this._floatFormatHandling = new FloatFormatHandling?(value);
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00002B60 File Offset: 0x00000D60
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x00002B86 File Offset: 0x00000D86
		public FloatParseHandling FloatParseHandling
		{
			get
			{
				FloatParseHandling? floatParseHandling = this._floatParseHandling;
				if (floatParseHandling == null)
				{
					return FloatParseHandling.Double;
				}
				return floatParseHandling.GetValueOrDefault();
			}
			set
			{
				this._floatParseHandling = new FloatParseHandling?(value);
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00002B94 File Offset: 0x00000D94
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x00002BBA File Offset: 0x00000DBA
		public StringEscapeHandling StringEscapeHandling
		{
			get
			{
				StringEscapeHandling? stringEscapeHandling = this._stringEscapeHandling;
				if (stringEscapeHandling == null)
				{
					return StringEscapeHandling.Default;
				}
				return stringEscapeHandling.GetValueOrDefault();
			}
			set
			{
				this._stringEscapeHandling = new StringEscapeHandling?(value);
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00002BC8 File Offset: 0x00000DC8
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00002BD9 File Offset: 0x00000DD9
		public CultureInfo Culture
		{
			get
			{
				return this._culture ?? JsonSerializerSettings.DefaultCulture;
			}
			set
			{
				this._culture = value;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00002BE4 File Offset: 0x00000DE4
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x00002C0A File Offset: 0x00000E0A
		public bool CheckAdditionalContent
		{
			get
			{
				return this._checkAdditionalContent ?? false;
			}
			set
			{
				this._checkAdditionalContent = new bool?(value);
			}
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00002C2F File Offset: 0x00000E2F
		public JsonSerializerSettings()
		{
			this.Converters = new List<JsonConverter>
			{
				new VectorConverter()
			};
			this.Converters.Add(new HashSetConverter());
		}

		// Token: 0x04000058 RID: 88
		internal const ReferenceLoopHandling DefaultReferenceLoopHandling = ReferenceLoopHandling.Error;

		// Token: 0x04000059 RID: 89
		internal const MissingMemberHandling DefaultMissingMemberHandling = MissingMemberHandling.Ignore;

		// Token: 0x0400005A RID: 90
		internal const NullValueHandling DefaultNullValueHandling = NullValueHandling.Include;

		// Token: 0x0400005B RID: 91
		internal const DefaultValueHandling DefaultDefaultValueHandling = DefaultValueHandling.Include;

		// Token: 0x0400005C RID: 92
		internal const ObjectCreationHandling DefaultObjectCreationHandling = ObjectCreationHandling.Auto;

		// Token: 0x0400005D RID: 93
		internal const PreserveReferencesHandling DefaultPreserveReferencesHandling = PreserveReferencesHandling.None;

		// Token: 0x0400005E RID: 94
		internal const ConstructorHandling DefaultConstructorHandling = ConstructorHandling.Default;

		// Token: 0x0400005F RID: 95
		internal const TypeNameHandling DefaultTypeNameHandling = TypeNameHandling.None;

		// Token: 0x04000060 RID: 96
		internal const MetadataPropertyHandling DefaultMetadataPropertyHandling = MetadataPropertyHandling.Default;

		// Token: 0x04000061 RID: 97
		internal const FormatterAssemblyStyle DefaultTypeNameAssemblyFormat = FormatterAssemblyStyle.Simple;

		// Token: 0x04000062 RID: 98
		internal static readonly StreamingContext DefaultContext = default(StreamingContext);

		// Token: 0x04000063 RID: 99
		internal const Formatting DefaultFormatting = Formatting.None;

		// Token: 0x04000064 RID: 100
		internal const DateFormatHandling DefaultDateFormatHandling = DateFormatHandling.IsoDateFormat;

		// Token: 0x04000065 RID: 101
		internal const DateTimeZoneHandling DefaultDateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;

		// Token: 0x04000066 RID: 102
		internal const DateParseHandling DefaultDateParseHandling = DateParseHandling.DateTime;

		// Token: 0x04000067 RID: 103
		internal const FloatParseHandling DefaultFloatParseHandling = FloatParseHandling.Double;

		// Token: 0x04000068 RID: 104
		internal const FloatFormatHandling DefaultFloatFormatHandling = FloatFormatHandling.String;

		// Token: 0x04000069 RID: 105
		internal const StringEscapeHandling DefaultStringEscapeHandling = StringEscapeHandling.Default;

		// Token: 0x0400006A RID: 106
		internal const FormatterAssemblyStyle DefaultFormatterAssemblyStyle = FormatterAssemblyStyle.Simple;

		// Token: 0x0400006B RID: 107
		internal static readonly CultureInfo DefaultCulture = CultureInfo.InvariantCulture;

		// Token: 0x0400006C RID: 108
		internal const bool DefaultCheckAdditionalContent = false;

		// Token: 0x0400006D RID: 109
		internal const string DefaultDateFormatString = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK";

		// Token: 0x0400006E RID: 110
		internal Formatting? _formatting;

		// Token: 0x0400006F RID: 111
		internal DateFormatHandling? _dateFormatHandling;

		// Token: 0x04000070 RID: 112
		internal DateTimeZoneHandling? _dateTimeZoneHandling;

		// Token: 0x04000071 RID: 113
		internal DateParseHandling? _dateParseHandling;

		// Token: 0x04000072 RID: 114
		internal FloatFormatHandling? _floatFormatHandling;

		// Token: 0x04000073 RID: 115
		internal FloatParseHandling? _floatParseHandling;

		// Token: 0x04000074 RID: 116
		internal StringEscapeHandling? _stringEscapeHandling;

		// Token: 0x04000075 RID: 117
		internal CultureInfo _culture;

		// Token: 0x04000076 RID: 118
		internal bool? _checkAdditionalContent;

		// Token: 0x04000077 RID: 119
		internal int? _maxDepth;

		// Token: 0x04000078 RID: 120
		internal bool _maxDepthSet;

		// Token: 0x04000079 RID: 121
		internal string _dateFormatString;

		// Token: 0x0400007A RID: 122
		internal bool _dateFormatStringSet;

		// Token: 0x0400007B RID: 123
		internal FormatterAssemblyStyle? _typeNameAssemblyFormat;

		// Token: 0x0400007C RID: 124
		internal DefaultValueHandling? _defaultValueHandling;

		// Token: 0x0400007D RID: 125
		internal PreserveReferencesHandling? _preserveReferencesHandling;

		// Token: 0x0400007E RID: 126
		internal NullValueHandling? _nullValueHandling;

		// Token: 0x0400007F RID: 127
		internal ObjectCreationHandling? _objectCreationHandling;

		// Token: 0x04000080 RID: 128
		internal MissingMemberHandling? _missingMemberHandling;

		// Token: 0x04000081 RID: 129
		internal ReferenceLoopHandling? _referenceLoopHandling;

		// Token: 0x04000082 RID: 130
		internal StreamingContext? _context;

		// Token: 0x04000083 RID: 131
		internal ConstructorHandling? _constructorHandling;

		// Token: 0x04000084 RID: 132
		internal TypeNameHandling? _typeNameHandling;

		// Token: 0x04000085 RID: 133
		internal MetadataPropertyHandling? _metadataPropertyHandling;
	}
}
