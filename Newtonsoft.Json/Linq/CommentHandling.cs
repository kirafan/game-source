﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200008C RID: 140
	[Preserve]
	public enum CommentHandling
	{
		// Token: 0x04000277 RID: 631
		Ignore,
		// Token: 0x04000278 RID: 632
		Load
	}
}
