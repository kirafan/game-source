﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000095 RID: 149
	[Preserve]
	public interface IJEnumerable<T> : IEnumerable<T>, IEnumerable where T : JToken
	{
		// Token: 0x1700014E RID: 334
		IJEnumerable<JToken> this[object key]
		{
			get;
		}
	}
}
