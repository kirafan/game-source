﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000093 RID: 147
	[Flags]
	[Preserve]
	public enum MergeNullValueHandling
	{
		// Token: 0x04000288 RID: 648
		Ignore = 0,
		// Token: 0x04000289 RID: 649
		Merge = 1
	}
}
