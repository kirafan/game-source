﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000098 RID: 152
	[Preserve]
	public class JConstructor : JContainer
	{
		// Token: 0x1700014F RID: 335
		// (get) Token: 0x06000690 RID: 1680 RVA: 0x0001A819 File Offset: 0x00018A19
		protected override IList<JToken> ChildrenTokens
		{
			get
			{
				return this._values;
			}
		}

		// Token: 0x06000691 RID: 1681 RVA: 0x0001A821 File Offset: 0x00018A21
		internal override int IndexOfItem(JToken item)
		{
			return this._values.IndexOfReference(item);
		}

		// Token: 0x06000692 RID: 1682 RVA: 0x0001A830 File Offset: 0x00018A30
		internal override void MergeItem(object content, JsonMergeSettings settings)
		{
			JConstructor jconstructor = content as JConstructor;
			if (jconstructor == null)
			{
				return;
			}
			if (jconstructor.Name != null)
			{
				this.Name = jconstructor.Name;
			}
			JContainer.MergeEnumerableContent(this, jconstructor, settings);
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x06000693 RID: 1683 RVA: 0x0001A864 File Offset: 0x00018A64
		// (set) Token: 0x06000694 RID: 1684 RVA: 0x0001A86C File Offset: 0x00018A6C
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x0001A875 File Offset: 0x00018A75
		public override JTokenType Type
		{
			get
			{
				return JTokenType.Constructor;
			}
		}

		// Token: 0x06000696 RID: 1686 RVA: 0x0001A878 File Offset: 0x00018A78
		public JConstructor()
		{
		}

		// Token: 0x06000697 RID: 1687 RVA: 0x0001A88B File Offset: 0x00018A8B
		public JConstructor(JConstructor other) : base(other)
		{
			this._name = other.Name;
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x0001A8AB File Offset: 0x00018AAB
		public JConstructor(string name, params object[] content) : this(name, content)
		{
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x0001A8B5 File Offset: 0x00018AB5
		public JConstructor(string name, object content) : this(name)
		{
			this.Add(content);
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x0001A8C5 File Offset: 0x00018AC5
		public JConstructor(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Constructor name cannot be empty.", "name");
			}
			this._name = name;
		}

		// Token: 0x0600069B RID: 1691 RVA: 0x0001A908 File Offset: 0x00018B08
		internal override bool DeepEquals(JToken node)
		{
			JConstructor jconstructor = node as JConstructor;
			return jconstructor != null && this._name == jconstructor.Name && base.ContentsEqual(jconstructor);
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x0001A93B File Offset: 0x00018B3B
		internal override JToken CloneToken()
		{
			return new JConstructor(this);
		}

		// Token: 0x0600069D RID: 1693 RVA: 0x0001A944 File Offset: 0x00018B44
		public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
		{
			writer.WriteStartConstructor(this._name);
			foreach (JToken jtoken in this.Children())
			{
				jtoken.WriteTo(writer, converters);
			}
			writer.WriteEndConstructor();
		}

		// Token: 0x17000152 RID: 338
		public override JToken this[object key]
		{
			get
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				if (!(key is int))
				{
					throw new ArgumentException("Accessed JConstructor values with invalid key value: {0}. Argument position index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				return this.GetItem((int)key);
			}
			set
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				if (!(key is int))
				{
					throw new ArgumentException("Set JConstructor values with invalid key value: {0}. Argument position index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				this.SetItem((int)key, value);
			}
		}

		// Token: 0x060006A0 RID: 1696 RVA: 0x0001AA21 File Offset: 0x00018C21
		internal override int GetDeepHashCode()
		{
			return this._name.GetHashCode() ^ base.ContentsHashCode();
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x0001AA35 File Offset: 0x00018C35
		public new static JConstructor Load(JsonReader reader)
		{
			return JConstructor.Load(reader, null);
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x0001AA40 File Offset: 0x00018C40
		public new static JConstructor Load(JsonReader reader, JsonLoadSettings settings)
		{
			if (reader.TokenType == JsonToken.None && !reader.Read())
			{
				throw JsonReaderException.Create(reader, "Error reading JConstructor from JsonReader.");
			}
			reader.MoveToContent();
			if (reader.TokenType != JsonToken.StartConstructor)
			{
				throw JsonReaderException.Create(reader, "Error reading JConstructor from JsonReader. Current JsonReader item is not a constructor: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
			}
			JConstructor jconstructor = new JConstructor((string)reader.Value);
			jconstructor.SetLineInfo(reader as IJsonLineInfo, settings);
			jconstructor.ReadTokenFrom(reader, settings);
			return jconstructor;
		}

		// Token: 0x0400028A RID: 650
		private string _name;

		// Token: 0x0400028B RID: 651
		private readonly List<JToken> _values = new List<JToken>();
	}
}
