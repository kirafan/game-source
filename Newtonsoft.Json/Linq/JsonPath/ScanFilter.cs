﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AF RID: 175
	[Preserve]
	internal class ScanFilter : PathFilter
	{
		// Token: 0x1700019D RID: 413
		// (get) Token: 0x060008C5 RID: 2245 RVA: 0x000213B0 File Offset: 0x0001F5B0
		// (set) Token: 0x060008C6 RID: 2246 RVA: 0x000213B8 File Offset: 0x0001F5B8
		public string Name { get; set; }

		// Token: 0x060008C7 RID: 2247 RVA: 0x000213C1 File Offset: 0x0001F5C1
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken root in current)
			{
				if (this.Name == null)
				{
					yield return root;
				}
				JToken value = root;
				JToken jtoken = root;
				for (;;)
				{
					if (jtoken != null && jtoken.HasValues)
					{
						value = jtoken.First;
					}
					else
					{
						while (value != null && value != root && value == value.Parent.Last)
						{
							value = value.Parent;
						}
						if (value == null || value == root)
						{
							break;
						}
						value = value.Next;
					}
					JProperty jproperty = value as JProperty;
					if (jproperty != null)
					{
						if (jproperty.Name == this.Name)
						{
							yield return jproperty.Value;
						}
					}
					else if (this.Name == null)
					{
						yield return value;
					}
					jtoken = (value as JContainer);
				}
				value = null;
				root = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
