﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AD RID: 173
	[Preserve]
	internal class BooleanQueryExpression : QueryExpression
	{
		// Token: 0x1700019A RID: 410
		// (get) Token: 0x060008BA RID: 2234 RVA: 0x00021138 File Offset: 0x0001F338
		// (set) Token: 0x060008BB RID: 2235 RVA: 0x00021140 File Offset: 0x0001F340
		public List<PathFilter> Path { get; set; }

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x060008BC RID: 2236 RVA: 0x00021149 File Offset: 0x0001F349
		// (set) Token: 0x060008BD RID: 2237 RVA: 0x00021151 File Offset: 0x0001F351
		public JValue Value { get; set; }

		// Token: 0x060008BE RID: 2238 RVA: 0x0002115C File Offset: 0x0001F35C
		public override bool IsMatch(JToken t)
		{
			foreach (JToken jtoken in JPath.Evaluate(this.Path, t, false))
			{
				JValue jvalue = jtoken as JValue;
				if (jvalue != null)
				{
					switch (base.Operator)
					{
					case QueryOperator.Equals:
						if (this.EqualsWithStringCoercion(jvalue, this.Value))
						{
							return true;
						}
						break;
					case QueryOperator.NotEquals:
						if (!this.EqualsWithStringCoercion(jvalue, this.Value))
						{
							return true;
						}
						break;
					case QueryOperator.Exists:
						return true;
					case QueryOperator.LessThan:
						if (jvalue.CompareTo(this.Value) < 0)
						{
							return true;
						}
						break;
					case QueryOperator.LessThanOrEquals:
						if (jvalue.CompareTo(this.Value) <= 0)
						{
							return true;
						}
						break;
					case QueryOperator.GreaterThan:
						if (jvalue.CompareTo(this.Value) > 0)
						{
							return true;
						}
						break;
					case QueryOperator.GreaterThanOrEquals:
						if (jvalue.CompareTo(this.Value) >= 0)
						{
							return true;
						}
						break;
					}
				}
				else
				{
					QueryOperator @operator = base.Operator;
					if (@operator == QueryOperator.NotEquals || @operator == QueryOperator.Exists)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x060008BF RID: 2239 RVA: 0x0002127C File Offset: 0x0001F47C
		private bool EqualsWithStringCoercion(JValue value, JValue queryValue)
		{
			if (value.Equals(queryValue))
			{
				return true;
			}
			if (queryValue.Type != JTokenType.String)
			{
				return false;
			}
			string b = (string)queryValue.Value;
			string a;
			switch (value.Type)
			{
			case JTokenType.Date:
				using (StringWriter stringWriter = StringUtils.CreateStringWriter(64))
				{
					if (value.Value is DateTimeOffset)
					{
						DateTimeUtils.WriteDateTimeOffsetString(stringWriter, (DateTimeOffset)value.Value, DateFormatHandling.IsoDateFormat, null, CultureInfo.InvariantCulture);
					}
					else
					{
						DateTimeUtils.WriteDateTimeString(stringWriter, (DateTime)value.Value, DateFormatHandling.IsoDateFormat, null, CultureInfo.InvariantCulture);
					}
					a = stringWriter.ToString();
					goto IL_DF;
				}
				break;
			case JTokenType.Raw:
				return false;
			case JTokenType.Bytes:
				break;
			case JTokenType.Guid:
			case JTokenType.TimeSpan:
				a = value.Value.ToString();
				goto IL_DF;
			case JTokenType.Uri:
				a = ((Uri)value.Value).OriginalString;
				goto IL_DF;
			default:
				return false;
			}
			a = Convert.ToBase64String((byte[])value.Value);
			IL_DF:
			return string.Equals(a, b, StringComparison.Ordinal);
		}
	}
}
