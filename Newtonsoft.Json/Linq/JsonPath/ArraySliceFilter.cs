﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000A5 RID: 165
	[Preserve]
	internal class ArraySliceFilter : PathFilter
	{
		// Token: 0x17000192 RID: 402
		// (get) Token: 0x0600088C RID: 2188 RVA: 0x0001FEBB File Offset: 0x0001E0BB
		// (set) Token: 0x0600088D RID: 2189 RVA: 0x0001FEC3 File Offset: 0x0001E0C3
		public int? Start { get; set; }

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x0600088E RID: 2190 RVA: 0x0001FECC File Offset: 0x0001E0CC
		// (set) Token: 0x0600088F RID: 2191 RVA: 0x0001FED4 File Offset: 0x0001E0D4
		public int? End { get; set; }

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x06000890 RID: 2192 RVA: 0x0001FEDD File Offset: 0x0001E0DD
		// (set) Token: 0x06000891 RID: 2193 RVA: 0x0001FEE5 File Offset: 0x0001E0E5
		public int? Step { get; set; }

		// Token: 0x06000892 RID: 2194 RVA: 0x0001FEEE File Offset: 0x0001E0EE
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			if (this.Step == 0)
			{
				throw new JsonException("Step cannot be zero.");
			}
			foreach (JToken t in current)
			{
				JArray a = t as JArray;
				if (a != null)
				{
					int stepCount = this.Step ?? 1;
					int num = this.Start ?? ((stepCount > 0) ? 0 : (a.Count - 1));
					int stopIndex = this.End ?? ((stepCount > 0) ? a.Count : -1);
					if (this.Start < 0)
					{
						num = a.Count + num;
					}
					if (this.End < 0)
					{
						stopIndex = a.Count + stopIndex;
					}
					num = Math.Max(num, (stepCount > 0) ? 0 : int.MinValue);
					num = Math.Min(num, (stepCount > 0) ? a.Count : (a.Count - 1));
					stopIndex = Math.Max(stopIndex, -1);
					stopIndex = Math.Min(stopIndex, a.Count);
					bool positiveStep = stepCount > 0;
					if (this.IsValid(num, stopIndex, positiveStep))
					{
						int i = num;
						while (this.IsValid(i, stopIndex, positiveStep))
						{
							yield return a[i];
							i += stepCount;
						}
					}
					else if (errorWhenNoMatch)
					{
						throw new JsonException("Array slice of {0} to {1} returned no results.".FormatWith(CultureInfo.InvariantCulture, (this.Start != null) ? this.Start.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) : "*", (this.End != null) ? this.End.GetValueOrDefault().ToString(CultureInfo.InvariantCulture) : "*"));
					}
				}
				else if (errorWhenNoMatch)
				{
					throw new JsonException("Array slice is not valid on {0}.".FormatWith(CultureInfo.InvariantCulture, t.GetType().Name));
				}
				a = null;
				t = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x06000893 RID: 2195 RVA: 0x0001FF0C File Offset: 0x0001E10C
		private bool IsValid(int index, int stopIndex, bool positiveStep)
		{
			if (positiveStep)
			{
				return index < stopIndex;
			}
			return index > stopIndex;
		}
	}
}
