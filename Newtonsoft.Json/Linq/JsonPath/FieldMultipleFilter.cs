﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000A7 RID: 167
	[Preserve]
	internal class FieldMultipleFilter : PathFilter
	{
		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000899 RID: 2201 RVA: 0x0001FF49 File Offset: 0x0001E149
		// (set) Token: 0x0600089A RID: 2202 RVA: 0x0001FF51 File Offset: 0x0001E151
		public List<string> Names { get; set; }

		// Token: 0x0600089B RID: 2203 RVA: 0x0001FF5A File Offset: 0x0001E15A
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken t in current)
			{
				JObject o = t as JObject;
				if (o != null)
				{
					foreach (string name in this.Names)
					{
						JToken jtoken = o[name];
						if (jtoken != null)
						{
							yield return jtoken;
						}
						if (errorWhenNoMatch)
						{
							throw new JsonException("Property '{0}' does not exist on JObject.".FormatWith(CultureInfo.InvariantCulture, name));
						}
						name = null;
					}
					List<string>.Enumerator enumerator2 = default(List<string>.Enumerator);
				}
				else if (errorWhenNoMatch)
				{
					throw new JsonException("Properties {0} not valid on {1}.".FormatWith(CultureInfo.InvariantCulture, string.Join(", ", (from n in this.Names
					select "'" + n + "'").ToArray<string>()), t.GetType().Name));
				}
				o = null;
				t = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
