﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AA RID: 170
	[Preserve]
	internal enum QueryOperator
	{
		// Token: 0x040002CE RID: 718
		None,
		// Token: 0x040002CF RID: 719
		Equals,
		// Token: 0x040002D0 RID: 720
		NotEquals,
		// Token: 0x040002D1 RID: 721
		Exists,
		// Token: 0x040002D2 RID: 722
		LessThan,
		// Token: 0x040002D3 RID: 723
		LessThanOrEquals,
		// Token: 0x040002D4 RID: 724
		GreaterThan,
		// Token: 0x040002D5 RID: 725
		GreaterThanOrEquals,
		// Token: 0x040002D6 RID: 726
		And,
		// Token: 0x040002D7 RID: 727
		Or
	}
}
