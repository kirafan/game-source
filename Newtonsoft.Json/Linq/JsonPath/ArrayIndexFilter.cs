﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000A3 RID: 163
	[Preserve]
	internal class ArrayIndexFilter : PathFilter
	{
		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000884 RID: 2180 RVA: 0x0001FE55 File Offset: 0x0001E055
		// (set) Token: 0x06000885 RID: 2181 RVA: 0x0001FE5D File Offset: 0x0001E05D
		public int? Index { get; set; }

		// Token: 0x06000886 RID: 2182 RVA: 0x0001FE66 File Offset: 0x0001E066
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken t in current)
			{
				if (this.Index != null)
				{
					JToken tokenIndex = PathFilter.GetTokenIndex(t, errorWhenNoMatch, this.Index.GetValueOrDefault());
					if (tokenIndex != null)
					{
						yield return tokenIndex;
					}
				}
				else if (t is JArray || t is JConstructor)
				{
					foreach (JToken jtoken in ((IEnumerable<JToken>)t))
					{
						yield return jtoken;
					}
					IEnumerator<JToken> enumerator2 = null;
				}
				else if (errorWhenNoMatch)
				{
					throw new JsonException("Index * not valid on {0}.".FormatWith(CultureInfo.InvariantCulture, t.GetType().Name));
				}
				t = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
