﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000A6 RID: 166
	[Preserve]
	internal class FieldFilter : PathFilter
	{
		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000895 RID: 2197 RVA: 0x0001FF1A File Offset: 0x0001E11A
		// (set) Token: 0x06000896 RID: 2198 RVA: 0x0001FF22 File Offset: 0x0001E122
		public string Name { get; set; }

		// Token: 0x06000897 RID: 2199 RVA: 0x0001FF2B File Offset: 0x0001E12B
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken t in current)
			{
				JObject o = t as JObject;
				if (o != null)
				{
					if (this.Name != null)
					{
						JToken jtoken = o[this.Name];
						if (jtoken != null)
						{
							yield return jtoken;
						}
						else if (errorWhenNoMatch)
						{
							throw new JsonException("Property '{0}' does not exist on JObject.".FormatWith(CultureInfo.InvariantCulture, this.Name));
						}
					}
					else
					{
						foreach (KeyValuePair<string, JToken> keyValuePair in o)
						{
							yield return keyValuePair.Value;
						}
						IEnumerator<KeyValuePair<string, JToken>> enumerator2 = null;
					}
				}
				else if (errorWhenNoMatch)
				{
					throw new JsonException("Property '{0}' not valid on {1}.".FormatWith(CultureInfo.InvariantCulture, this.Name ?? "*", t.GetType().Name));
				}
				o = null;
				t = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
