﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AC RID: 172
	[Preserve]
	internal class CompositeExpression : QueryExpression
	{
		// Token: 0x17000199 RID: 409
		// (get) Token: 0x060008B6 RID: 2230 RVA: 0x00021057 File Offset: 0x0001F257
		// (set) Token: 0x060008B7 RID: 2231 RVA: 0x0002105F File Offset: 0x0001F25F
		public List<QueryExpression> Expressions { get; set; }

		// Token: 0x060008B8 RID: 2232 RVA: 0x00021068 File Offset: 0x0001F268
		public CompositeExpression()
		{
			this.Expressions = new List<QueryExpression>();
		}

		// Token: 0x060008B9 RID: 2233 RVA: 0x0002107C File Offset: 0x0001F27C
		public override bool IsMatch(JToken t)
		{
			QueryOperator @operator = base.Operator;
			if (@operator == QueryOperator.And)
			{
				using (List<QueryExpression>.Enumerator enumerator = this.Expressions.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (!enumerator.Current.IsMatch(t))
						{
							return false;
						}
					}
				}
				return true;
			}
			if (@operator != QueryOperator.Or)
			{
				throw new ArgumentOutOfRangeException();
			}
			using (List<QueryExpression>.Enumerator enumerator = this.Expressions.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current.IsMatch(t))
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
