﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000A4 RID: 164
	[Preserve]
	internal class ArrayMultipleIndexFilter : PathFilter
	{
		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000888 RID: 2184 RVA: 0x0001FE8C File Offset: 0x0001E08C
		// (set) Token: 0x06000889 RID: 2185 RVA: 0x0001FE94 File Offset: 0x0001E094
		public List<int> Indexes { get; set; }

		// Token: 0x0600088A RID: 2186 RVA: 0x0001FE9D File Offset: 0x0001E09D
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken t in current)
			{
				foreach (int index in this.Indexes)
				{
					JToken tokenIndex = PathFilter.GetTokenIndex(t, errorWhenNoMatch, index);
					if (tokenIndex != null)
					{
						yield return tokenIndex;
					}
				}
				List<int>.Enumerator enumerator2 = default(List<int>.Enumerator);
				t = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
