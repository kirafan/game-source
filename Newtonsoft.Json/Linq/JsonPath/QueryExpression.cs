﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AB RID: 171
	[Preserve]
	internal abstract class QueryExpression
	{
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x060008B2 RID: 2226 RVA: 0x00021046 File Offset: 0x0001F246
		// (set) Token: 0x060008B3 RID: 2227 RVA: 0x0002104E File Offset: 0x0001F24E
		public QueryOperator Operator { get; set; }

		// Token: 0x060008B4 RID: 2228
		public abstract bool IsMatch(JToken t);
	}
}
