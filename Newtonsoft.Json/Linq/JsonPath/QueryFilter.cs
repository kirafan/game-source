﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq.JsonPath
{
	// Token: 0x020000AE RID: 174
	[Preserve]
	internal class QueryFilter : PathFilter
	{
		// Token: 0x1700019C RID: 412
		// (get) Token: 0x060008C1 RID: 2241 RVA: 0x00021388 File Offset: 0x0001F588
		// (set) Token: 0x060008C2 RID: 2242 RVA: 0x00021390 File Offset: 0x0001F590
		public QueryExpression Expression { get; set; }

		// Token: 0x060008C3 RID: 2243 RVA: 0x00021399 File Offset: 0x0001F599
		public override IEnumerable<JToken> ExecuteFilter(IEnumerable<JToken> current, bool errorWhenNoMatch)
		{
			foreach (JToken jtoken in current)
			{
				foreach (JToken jtoken2 in ((IEnumerable<JToken>)jtoken))
				{
					if (this.Expression.IsMatch(jtoken2))
					{
						yield return jtoken2;
					}
				}
				IEnumerator<JToken> enumerator2 = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}
	}
}
