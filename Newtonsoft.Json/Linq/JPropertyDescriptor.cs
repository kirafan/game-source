﻿using System;
using System.ComponentModel;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200008E RID: 142
	[Preserve]
	public class JPropertyDescriptor : PropertyDescriptor
	{
		// Token: 0x0600064D RID: 1613 RVA: 0x00019FF4 File Offset: 0x000181F4
		public JPropertyDescriptor(string name) : base(name, null)
		{
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00019FFE File Offset: 0x000181FE
		private static JObject CastInstance(object instance)
		{
			return (JObject)instance;
		}

		// Token: 0x0600064F RID: 1615 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool CanResetValue(object component)
		{
			return false;
		}

		// Token: 0x06000650 RID: 1616 RVA: 0x0001A006 File Offset: 0x00018206
		public override object GetValue(object component)
		{
			return JPropertyDescriptor.CastInstance(component)[this.Name];
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x0000982C File Offset: 0x00007A2C
		public override void ResetValue(object component)
		{
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x0001A01C File Offset: 0x0001821C
		public override void SetValue(object component, object value)
		{
			JToken value2 = (value is JToken) ? ((JToken)value) : new JValue(value);
			JPropertyDescriptor.CastInstance(component)[this.Name] = value2;
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool ShouldSerializeValue(object component)
		{
			return false;
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000654 RID: 1620 RVA: 0x0001A052 File Offset: 0x00018252
		public override Type ComponentType
		{
			get
			{
				return typeof(JObject);
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06000655 RID: 1621 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000656 RID: 1622 RVA: 0x0001A05E File Offset: 0x0001825E
		public override Type PropertyType
		{
			get
			{
				return typeof(object);
			}
		}

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000657 RID: 1623 RVA: 0x0001A06A File Offset: 0x0001826A
		protected override int NameHashCode
		{
			get
			{
				return base.NameHashCode;
			}
		}
	}
}
