﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000099 RID: 153
	[Preserve]
	public abstract class JContainer : JToken, IList<JToken>, ICollection<JToken>, IEnumerable<JToken>, IEnumerable, ITypedList, IBindingList, IList, ICollection
	{
		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060006A3 RID: 1699 RVA: 0x0001AABF File Offset: 0x00018CBF
		// (remove) Token: 0x060006A4 RID: 1700 RVA: 0x0001AAD8 File Offset: 0x00018CD8
		public event ListChangedEventHandler ListChanged
		{
			add
			{
				this._listChanged = (ListChangedEventHandler)Delegate.Combine(this._listChanged, value);
			}
			remove
			{
				this._listChanged = (ListChangedEventHandler)Delegate.Remove(this._listChanged, value);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x060006A5 RID: 1701 RVA: 0x0001AAF1 File Offset: 0x00018CF1
		// (remove) Token: 0x060006A6 RID: 1702 RVA: 0x0001AB0A File Offset: 0x00018D0A
		public event System.ComponentModel.AddingNewEventHandler AddingNew
		{
			add
			{
				this._addingNew = (System.ComponentModel.AddingNewEventHandler)Delegate.Combine(this._addingNew, value);
			}
			remove
			{
				this._addingNew = (System.ComponentModel.AddingNewEventHandler)Delegate.Remove(this._addingNew, value);
			}
		}

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060006A7 RID: 1703
		protected abstract IList<JToken> ChildrenTokens { get; }

		// Token: 0x060006A8 RID: 1704 RVA: 0x0001AB23 File Offset: 0x00018D23
		internal JContainer()
		{
		}

		// Token: 0x060006A9 RID: 1705 RVA: 0x0001AB2C File Offset: 0x00018D2C
		internal JContainer(JContainer other) : this()
		{
			ValidationUtils.ArgumentNotNull(other, "other");
			int num = 0;
			foreach (JToken content in ((IEnumerable<JToken>)other))
			{
				this.AddInternal(num, content, false);
				num++;
			}
		}

		// Token: 0x060006AA RID: 1706 RVA: 0x0001AB90 File Offset: 0x00018D90
		internal void CheckReentrancy()
		{
			if (this._busy)
			{
				throw new InvalidOperationException("Cannot change {0} during a collection change event.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
			}
		}

		// Token: 0x060006AB RID: 1707 RVA: 0x0001ABB5 File Offset: 0x00018DB5
		internal virtual IList<JToken> CreateChildrenCollection()
		{
			return new List<JToken>();
		}

		// Token: 0x060006AC RID: 1708 RVA: 0x0001ABBC File Offset: 0x00018DBC
		protected virtual void OnAddingNew(System.ComponentModel.AddingNewEventArgs e)
		{
			System.ComponentModel.AddingNewEventHandler addingNew = this._addingNew;
			if (addingNew != null)
			{
				addingNew(this, e);
			}
		}

		// Token: 0x060006AD RID: 1709 RVA: 0x0001ABDC File Offset: 0x00018DDC
		protected virtual void OnListChanged(ListChangedEventArgs e)
		{
			ListChangedEventHandler listChanged = this._listChanged;
			if (listChanged != null)
			{
				this._busy = true;
				try
				{
					listChanged(this, e);
				}
				finally
				{
					this._busy = false;
				}
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060006AE RID: 1710 RVA: 0x0001AC1C File Offset: 0x00018E1C
		public override bool HasValues
		{
			get
			{
				return this.ChildrenTokens.Count > 0;
			}
		}

		// Token: 0x060006AF RID: 1711 RVA: 0x0001AC2C File Offset: 0x00018E2C
		internal bool ContentsEqual(JContainer container)
		{
			if (container == this)
			{
				return true;
			}
			IList<JToken> childrenTokens = this.ChildrenTokens;
			IList<JToken> childrenTokens2 = container.ChildrenTokens;
			if (childrenTokens.Count != childrenTokens2.Count)
			{
				return false;
			}
			for (int i = 0; i < childrenTokens.Count; i++)
			{
				if (!childrenTokens[i].DeepEquals(childrenTokens2[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060006B0 RID: 1712 RVA: 0x0001AC88 File Offset: 0x00018E88
		public override JToken First
		{
			get
			{
				IList<JToken> childrenTokens = this.ChildrenTokens;
				if (childrenTokens.Count <= 0)
				{
					return null;
				}
				return childrenTokens[0];
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060006B1 RID: 1713 RVA: 0x0001ACB0 File Offset: 0x00018EB0
		public override JToken Last
		{
			get
			{
				IList<JToken> childrenTokens = this.ChildrenTokens;
				int count = childrenTokens.Count;
				if (count <= 0)
				{
					return null;
				}
				return childrenTokens[count - 1];
			}
		}

		// Token: 0x060006B2 RID: 1714 RVA: 0x0001ACDA File Offset: 0x00018EDA
		public override JEnumerable<JToken> Children()
		{
			return new JEnumerable<JToken>(this.ChildrenTokens);
		}

		// Token: 0x060006B3 RID: 1715 RVA: 0x0001ACE7 File Offset: 0x00018EE7
		public override IEnumerable<T> Values<T>()
		{
			return this.ChildrenTokens.Convert<JToken, T>();
		}

		// Token: 0x060006B4 RID: 1716 RVA: 0x0001ACF4 File Offset: 0x00018EF4
		public IEnumerable<JToken> Descendants()
		{
			return this.GetDescendants(false);
		}

		// Token: 0x060006B5 RID: 1717 RVA: 0x0001ACFD File Offset: 0x00018EFD
		public IEnumerable<JToken> DescendantsAndSelf()
		{
			return this.GetDescendants(true);
		}

		// Token: 0x060006B6 RID: 1718 RVA: 0x0001AD06 File Offset: 0x00018F06
		internal IEnumerable<JToken> GetDescendants(bool self)
		{
			if (self)
			{
				yield return this;
			}
			foreach (JToken o in this.ChildrenTokens)
			{
				yield return o;
				JContainer jcontainer = o as JContainer;
				if (jcontainer != null)
				{
					foreach (JToken jtoken in jcontainer.Descendants())
					{
						yield return jtoken;
					}
					IEnumerator<JToken> enumerator2 = null;
				}
				o = null;
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x060006B7 RID: 1719 RVA: 0x0001AD1D File Offset: 0x00018F1D
		internal bool IsMultiContent(object content)
		{
			return content is IEnumerable && !(content is string) && !(content is JToken) && !(content is byte[]);
		}

		// Token: 0x060006B8 RID: 1720 RVA: 0x0001AD45 File Offset: 0x00018F45
		internal JToken EnsureParentToken(JToken item, bool skipParentCheck)
		{
			if (item == null)
			{
				return JValue.CreateNull();
			}
			if (skipParentCheck)
			{
				return item;
			}
			if (item.Parent != null || item == this || (item.HasValues && base.Root == item))
			{
				item = item.CloneToken();
			}
			return item;
		}

		// Token: 0x060006B9 RID: 1721
		internal abstract int IndexOfItem(JToken item);

		// Token: 0x060006BA RID: 1722 RVA: 0x0001AD7C File Offset: 0x00018F7C
		internal virtual void InsertItem(int index, JToken item, bool skipParentCheck)
		{
			IList<JToken> childrenTokens = this.ChildrenTokens;
			if (index > childrenTokens.Count)
			{
				throw new ArgumentOutOfRangeException("index", "Index must be within the bounds of the List.");
			}
			this.CheckReentrancy();
			item = this.EnsureParentToken(item, skipParentCheck);
			JToken jtoken = (index == 0) ? null : childrenTokens[index - 1];
			JToken jtoken2 = (index == childrenTokens.Count) ? null : childrenTokens[index];
			this.ValidateToken(item, null);
			item.Parent = this;
			item.Previous = jtoken;
			if (jtoken != null)
			{
				jtoken.Next = item;
			}
			item.Next = jtoken2;
			if (jtoken2 != null)
			{
				jtoken2.Previous = item;
			}
			childrenTokens.Insert(index, item);
			if (this._listChanged != null)
			{
				this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
			}
		}

		// Token: 0x060006BB RID: 1723 RVA: 0x0001AE2C File Offset: 0x0001902C
		internal virtual void RemoveItemAt(int index)
		{
			IList<JToken> childrenTokens = this.ChildrenTokens;
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "Index is less than 0.");
			}
			if (index >= childrenTokens.Count)
			{
				throw new ArgumentOutOfRangeException("index", "Index is equal to or greater than Count.");
			}
			this.CheckReentrancy();
			JToken jtoken = childrenTokens[index];
			JToken jtoken2 = (index == 0) ? null : childrenTokens[index - 1];
			JToken jtoken3 = (index == childrenTokens.Count - 1) ? null : childrenTokens[index + 1];
			if (jtoken2 != null)
			{
				jtoken2.Next = jtoken3;
			}
			if (jtoken3 != null)
			{
				jtoken3.Previous = jtoken2;
			}
			jtoken.Parent = null;
			jtoken.Previous = null;
			jtoken.Next = null;
			childrenTokens.RemoveAt(index);
			if (this._listChanged != null)
			{
				this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
			}
		}

		// Token: 0x060006BC RID: 1724 RVA: 0x0001AEE8 File Offset: 0x000190E8
		internal virtual bool RemoveItem(JToken item)
		{
			int num = this.IndexOfItem(item);
			if (num >= 0)
			{
				this.RemoveItemAt(num);
				return true;
			}
			return false;
		}

		// Token: 0x060006BD RID: 1725 RVA: 0x0001AF0B File Offset: 0x0001910B
		internal virtual JToken GetItem(int index)
		{
			return this.ChildrenTokens[index];
		}

		// Token: 0x060006BE RID: 1726 RVA: 0x0001AF1C File Offset: 0x0001911C
		internal virtual void SetItem(int index, JToken item)
		{
			IList<JToken> childrenTokens = this.ChildrenTokens;
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "Index is less than 0.");
			}
			if (index >= childrenTokens.Count)
			{
				throw new ArgumentOutOfRangeException("index", "Index is equal to or greater than Count.");
			}
			JToken jtoken = childrenTokens[index];
			if (JContainer.IsTokenUnchanged(jtoken, item))
			{
				return;
			}
			this.CheckReentrancy();
			item = this.EnsureParentToken(item, false);
			this.ValidateToken(item, jtoken);
			JToken jtoken2 = (index == 0) ? null : childrenTokens[index - 1];
			JToken jtoken3 = (index == childrenTokens.Count - 1) ? null : childrenTokens[index + 1];
			item.Parent = this;
			item.Previous = jtoken2;
			if (jtoken2 != null)
			{
				jtoken2.Next = item;
			}
			item.Next = jtoken3;
			if (jtoken3 != null)
			{
				jtoken3.Previous = item;
			}
			childrenTokens[index] = item;
			jtoken.Parent = null;
			jtoken.Previous = null;
			jtoken.Next = null;
			if (this._listChanged != null)
			{
				this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index));
			}
		}

		// Token: 0x060006BF RID: 1727 RVA: 0x0001B00C File Offset: 0x0001920C
		internal virtual void ClearItems()
		{
			this.CheckReentrancy();
			IList<JToken> childrenTokens = this.ChildrenTokens;
			foreach (JToken jtoken in childrenTokens)
			{
				jtoken.Parent = null;
				jtoken.Previous = null;
				jtoken.Next = null;
			}
			childrenTokens.Clear();
			if (this._listChanged != null)
			{
				this.OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
			}
		}

		// Token: 0x060006C0 RID: 1728 RVA: 0x0001B088 File Offset: 0x00019288
		internal virtual void ReplaceItem(JToken existing, JToken replacement)
		{
			if (existing == null || existing.Parent != this)
			{
				return;
			}
			int index = this.IndexOfItem(existing);
			this.SetItem(index, replacement);
		}

		// Token: 0x060006C1 RID: 1729 RVA: 0x0001B0B2 File Offset: 0x000192B2
		internal virtual bool ContainsItem(JToken item)
		{
			return this.IndexOfItem(item) != -1;
		}

		// Token: 0x060006C2 RID: 1730 RVA: 0x0001B0C4 File Offset: 0x000192C4
		internal virtual void CopyItemsTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException("arrayIndex", "arrayIndex is less than 0.");
			}
			if (arrayIndex >= array.Length && arrayIndex != 0)
			{
				throw new ArgumentException("arrayIndex is equal to or greater than the length of array.");
			}
			if (this.Count > array.Length - arrayIndex)
			{
				throw new ArgumentException("The number of elements in the source JObject is greater than the available space from arrayIndex to the end of the destination array.");
			}
			int num = 0;
			foreach (JToken value in this.ChildrenTokens)
			{
				array.SetValue(value, arrayIndex + num);
				num++;
			}
		}

		// Token: 0x060006C3 RID: 1731 RVA: 0x0001B170 File Offset: 0x00019370
		internal static bool IsTokenUnchanged(JToken currentValue, JToken newValue)
		{
			JValue jvalue = currentValue as JValue;
			return jvalue != null && ((jvalue.Type == JTokenType.Null && newValue == null) || jvalue.Equals(newValue));
		}

		// Token: 0x060006C4 RID: 1732 RVA: 0x0001B19F File Offset: 0x0001939F
		internal virtual void ValidateToken(JToken o, JToken existing)
		{
			ValidationUtils.ArgumentNotNull(o, "o");
			if (o.Type == JTokenType.Property)
			{
				throw new ArgumentException("Can not add {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, o.GetType(), base.GetType()));
			}
		}

		// Token: 0x060006C5 RID: 1733 RVA: 0x0001B1D6 File Offset: 0x000193D6
		public virtual void Add(object content)
		{
			this.AddInternal(this.ChildrenTokens.Count, content, false);
		}

		// Token: 0x060006C6 RID: 1734 RVA: 0x0001B1EB File Offset: 0x000193EB
		internal void AddAndSkipParentCheck(JToken token)
		{
			this.AddInternal(this.ChildrenTokens.Count, token, true);
		}

		// Token: 0x060006C7 RID: 1735 RVA: 0x0001B200 File Offset: 0x00019400
		public void AddFirst(object content)
		{
			this.AddInternal(0, content, false);
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x0001B20C File Offset: 0x0001940C
		internal void AddInternal(int index, object content, bool skipParentCheck)
		{
			if (this.IsMultiContent(content))
			{
				IEnumerable enumerable = (IEnumerable)content;
				int num = index;
				using (IEnumerator enumerator = enumerable.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object content2 = enumerator.Current;
						this.AddInternal(num, content2, skipParentCheck);
						num++;
					}
					return;
				}
			}
			JToken item = JContainer.CreateFromContent(content);
			this.InsertItem(index, item, skipParentCheck);
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x0001B284 File Offset: 0x00019484
		internal static JToken CreateFromContent(object content)
		{
			JToken jtoken = content as JToken;
			if (jtoken != null)
			{
				return jtoken;
			}
			return new JValue(content);
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x0001B2A3 File Offset: 0x000194A3
		public JsonWriter CreateWriter()
		{
			return new JTokenWriter(this);
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x0001B2AB File Offset: 0x000194AB
		public void ReplaceAll(object content)
		{
			this.ClearItems();
			this.Add(content);
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x0001B2BA File Offset: 0x000194BA
		public void RemoveAll()
		{
			this.ClearItems();
		}

		// Token: 0x060006CD RID: 1741
		internal abstract void MergeItem(object content, JsonMergeSettings settings);

		// Token: 0x060006CE RID: 1742 RVA: 0x0001B2C2 File Offset: 0x000194C2
		public void Merge(object content)
		{
			this.MergeItem(content, new JsonMergeSettings());
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x0001B2D0 File Offset: 0x000194D0
		public void Merge(object content, JsonMergeSettings settings)
		{
			this.MergeItem(content, settings);
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x0001B2DC File Offset: 0x000194DC
		internal void ReadTokenFrom(JsonReader reader, JsonLoadSettings options)
		{
			int depth = reader.Depth;
			if (!reader.Read())
			{
				throw JsonReaderException.Create(reader, "Error reading {0} from JsonReader.".FormatWith(CultureInfo.InvariantCulture, base.GetType().Name));
			}
			this.ReadContentFrom(reader, options);
			if (reader.Depth > depth)
			{
				throw JsonReaderException.Create(reader, "Unexpected end of content while loading {0}.".FormatWith(CultureInfo.InvariantCulture, base.GetType().Name));
			}
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x0001B34C File Offset: 0x0001954C
		internal void ReadContentFrom(JsonReader r, JsonLoadSettings settings)
		{
			ValidationUtils.ArgumentNotNull(r, "r");
			IJsonLineInfo lineInfo = r as IJsonLineInfo;
			JContainer jcontainer = this;
			for (;;)
			{
				JProperty jproperty = jcontainer as JProperty;
				if (((jproperty != null) ? jproperty.Value : null) != null)
				{
					if (jcontainer == this)
					{
						break;
					}
					jcontainer = jcontainer.Parent;
				}
				switch (r.TokenType)
				{
				case JsonToken.None:
					goto IL_226;
				case JsonToken.StartObject:
				{
					JObject jobject = new JObject();
					jobject.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jobject);
					jcontainer = jobject;
					goto IL_226;
				}
				case JsonToken.StartArray:
				{
					JArray jarray = new JArray();
					jarray.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jarray);
					jcontainer = jarray;
					goto IL_226;
				}
				case JsonToken.StartConstructor:
				{
					JConstructor jconstructor = new JConstructor(r.Value.ToString());
					jconstructor.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jconstructor);
					jcontainer = jconstructor;
					goto IL_226;
				}
				case JsonToken.PropertyName:
				{
					string name = r.Value.ToString();
					JProperty jproperty2 = new JProperty(name);
					jproperty2.SetLineInfo(lineInfo, settings);
					JProperty jproperty3 = ((JObject)jcontainer).Property(name);
					if (jproperty3 == null)
					{
						jcontainer.Add(jproperty2);
					}
					else
					{
						jproperty3.Replace(jproperty2);
					}
					jcontainer = jproperty2;
					goto IL_226;
				}
				case JsonToken.Comment:
					if (settings != null && settings.CommentHandling == CommentHandling.Load)
					{
						JValue jvalue = JValue.CreateComment(r.Value.ToString());
						jvalue.SetLineInfo(lineInfo, settings);
						jcontainer.Add(jvalue);
						goto IL_226;
					}
					goto IL_226;
				case JsonToken.Integer:
				case JsonToken.Float:
				case JsonToken.String:
				case JsonToken.Boolean:
				case JsonToken.Date:
				case JsonToken.Bytes:
				{
					JValue jvalue = new JValue(r.Value);
					jvalue.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jvalue);
					goto IL_226;
				}
				case JsonToken.Null:
				{
					JValue jvalue = JValue.CreateNull();
					jvalue.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jvalue);
					goto IL_226;
				}
				case JsonToken.Undefined:
				{
					JValue jvalue = JValue.CreateUndefined();
					jvalue.SetLineInfo(lineInfo, settings);
					jcontainer.Add(jvalue);
					goto IL_226;
				}
				case JsonToken.EndObject:
					if (jcontainer == this)
					{
						return;
					}
					jcontainer = jcontainer.Parent;
					goto IL_226;
				case JsonToken.EndArray:
					if (jcontainer == this)
					{
						return;
					}
					jcontainer = jcontainer.Parent;
					goto IL_226;
				case JsonToken.EndConstructor:
					if (jcontainer == this)
					{
						return;
					}
					jcontainer = jcontainer.Parent;
					goto IL_226;
				}
				goto Block_4;
				IL_226:
				if (!r.Read())
				{
					return;
				}
			}
			return;
			Block_4:
			throw new InvalidOperationException("The JsonReader should not be on a token of type {0}.".FormatWith(CultureInfo.InvariantCulture, r.TokenType));
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x0001B58C File Offset: 0x0001978C
		internal int ContentsHashCode()
		{
			int num = 0;
			foreach (JToken jtoken in this.ChildrenTokens)
			{
				num ^= jtoken.GetDeepHashCode();
			}
			return num;
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x0001B5E0 File Offset: 0x000197E0
		string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
		{
			return string.Empty;
		}

		// Token: 0x060006D4 RID: 1748 RVA: 0x0001B5E7 File Offset: 0x000197E7
		PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
		{
			ICustomTypeDescriptor customTypeDescriptor = this.First as ICustomTypeDescriptor;
			if (customTypeDescriptor == null)
			{
				return null;
			}
			return customTypeDescriptor.GetProperties();
		}

		// Token: 0x060006D5 RID: 1749 RVA: 0x0001B5FF File Offset: 0x000197FF
		int IList<JToken>.IndexOf(JToken item)
		{
			return this.IndexOfItem(item);
		}

		// Token: 0x060006D6 RID: 1750 RVA: 0x0001B608 File Offset: 0x00019808
		void IList<JToken>.Insert(int index, JToken item)
		{
			this.InsertItem(index, item, false);
		}

		// Token: 0x060006D7 RID: 1751 RVA: 0x0001B613 File Offset: 0x00019813
		void IList<JToken>.RemoveAt(int index)
		{
			this.RemoveItemAt(index);
		}

		// Token: 0x17000157 RID: 343
		JToken IList<JToken>.this[int index]
		{
			get
			{
				return this.GetItem(index);
			}
			set
			{
				this.SetItem(index, value);
			}
		}

		// Token: 0x060006DA RID: 1754 RVA: 0x0001B62F File Offset: 0x0001982F
		void ICollection<JToken>.Add(JToken item)
		{
			this.Add(item);
		}

		// Token: 0x060006DB RID: 1755 RVA: 0x0001B2BA File Offset: 0x000194BA
		void ICollection<JToken>.Clear()
		{
			this.ClearItems();
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x0001B638 File Offset: 0x00019838
		bool ICollection<JToken>.Contains(JToken item)
		{
			return this.ContainsItem(item);
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x0001B641 File Offset: 0x00019841
		void ICollection<JToken>.CopyTo(JToken[] array, int arrayIndex)
		{
			this.CopyItemsTo(array, arrayIndex);
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060006DE RID: 1758 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool ICollection<JToken>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x0001B64B File Offset: 0x0001984B
		bool ICollection<JToken>.Remove(JToken item)
		{
			return this.RemoveItem(item);
		}

		// Token: 0x060006E0 RID: 1760 RVA: 0x0001B654 File Offset: 0x00019854
		private JToken EnsureValue(object value)
		{
			if (value == null)
			{
				return null;
			}
			JToken jtoken = value as JToken;
			if (jtoken != null)
			{
				return jtoken;
			}
			throw new ArgumentException("Argument is not a JToken.");
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x0001B67C File Offset: 0x0001987C
		int IList.Add(object value)
		{
			this.Add(this.EnsureValue(value));
			return this.Count - 1;
		}

		// Token: 0x060006E2 RID: 1762 RVA: 0x0001B2BA File Offset: 0x000194BA
		void IList.Clear()
		{
			this.ClearItems();
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x0001B693 File Offset: 0x00019893
		bool IList.Contains(object value)
		{
			return this.ContainsItem(this.EnsureValue(value));
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x0001B6A2 File Offset: 0x000198A2
		int IList.IndexOf(object value)
		{
			return this.IndexOfItem(this.EnsureValue(value));
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x0001B6B1 File Offset: 0x000198B1
		void IList.Insert(int index, object value)
		{
			this.InsertItem(index, this.EnsureValue(value), false);
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060006E6 RID: 1766 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060006E7 RID: 1767 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x0001B6C2 File Offset: 0x000198C2
		void IList.Remove(object value)
		{
			this.RemoveItem(this.EnsureValue(value));
		}

		// Token: 0x060006E9 RID: 1769 RVA: 0x0001B613 File Offset: 0x00019813
		void IList.RemoveAt(int index)
		{
			this.RemoveItemAt(index);
		}

		// Token: 0x1700015B RID: 347
		object IList.this[int index]
		{
			get
			{
				return this.GetItem(index);
			}
			set
			{
				this.SetItem(index, this.EnsureValue(value));
			}
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x0001B641 File Offset: 0x00019841
		void ICollection.CopyTo(Array array, int index)
		{
			this.CopyItemsTo(array, index);
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060006ED RID: 1773 RVA: 0x0001B6E2 File Offset: 0x000198E2
		public int Count
		{
			get
			{
				return this.ChildrenTokens.Count;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060006EE RID: 1774 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060006EF RID: 1775 RVA: 0x0001B6EF File Offset: 0x000198EF
		object ICollection.SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x0000982C File Offset: 0x00007A2C
		void IBindingList.AddIndex(PropertyDescriptor property)
		{
		}

		// Token: 0x060006F1 RID: 1777 RVA: 0x0001B714 File Offset: 0x00019914
		object IBindingList.AddNew()
		{
			System.ComponentModel.AddingNewEventArgs addingNewEventArgs = new System.ComponentModel.AddingNewEventArgs();
			this.OnAddingNew(addingNewEventArgs);
			if (addingNewEventArgs.NewObject == null)
			{
				throw new JsonException("Could not determine new value to add to '{0}'.".FormatWith(CultureInfo.InvariantCulture, base.GetType()));
			}
			if (!(addingNewEventArgs.NewObject is JToken))
			{
				throw new JsonException("New item to be added to collection must be compatible with {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JToken)));
			}
			JToken jtoken = (JToken)addingNewEventArgs.NewObject;
			this.Add(jtoken);
			return jtoken;
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060006F2 RID: 1778 RVA: 0x00005AA3 File Offset: 0x00003CA3
		bool IBindingList.AllowEdit
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060006F3 RID: 1779 RVA: 0x00005AA3 File Offset: 0x00003CA3
		bool IBindingList.AllowNew
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060006F4 RID: 1780 RVA: 0x00005AA3 File Offset: 0x00003CA3
		bool IBindingList.AllowRemove
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060006F5 RID: 1781 RVA: 0x0001B791 File Offset: 0x00019991
		void IBindingList.ApplySort(PropertyDescriptor property, ListSortDirection direction)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060006F6 RID: 1782 RVA: 0x0001B791 File Offset: 0x00019991
		int IBindingList.Find(PropertyDescriptor property, object key)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060006F7 RID: 1783 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool IBindingList.IsSorted
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x0000982C File Offset: 0x00007A2C
		void IBindingList.RemoveIndex(PropertyDescriptor property)
		{
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x0001B791 File Offset: 0x00019991
		void IBindingList.RemoveSort()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060006FA RID: 1786 RVA: 0x0000CB98 File Offset: 0x0000AD98
		ListSortDirection IBindingList.SortDirection
		{
			get
			{
				return ListSortDirection.Ascending;
			}
		}

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060006FB RID: 1787 RVA: 0x0001B798 File Offset: 0x00019998
		PropertyDescriptor IBindingList.SortProperty
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x00005AA3 File Offset: 0x00003CA3
		bool IBindingList.SupportsChangeNotification
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060006FD RID: 1789 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool IBindingList.SupportsSearching
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060006FE RID: 1790 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool IBindingList.SupportsSorting
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060006FF RID: 1791 RVA: 0x0001B79C File Offset: 0x0001999C
		internal static void MergeEnumerableContent(JContainer target, IEnumerable content, JsonMergeSettings settings)
		{
			switch (settings.MergeArrayHandling)
			{
			case MergeArrayHandling.Concat:
				using (IEnumerator enumerator = content.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						JToken content2 = (JToken)obj;
						target.Add(content2);
					}
					return;
				}
				break;
			case MergeArrayHandling.Union:
				break;
			case MergeArrayHandling.Replace:
				goto IL_B6;
			case MergeArrayHandling.Merge:
				goto IL_FB;
			default:
				goto IL_18C;
			}
			HashSet<JToken> hashSet = new HashSet<JToken>(target, JToken.EqualityComparer);
			using (IEnumerator enumerator = content.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					object obj2 = enumerator.Current;
					JToken jtoken = (JToken)obj2;
					if (hashSet.Add(jtoken))
					{
						target.Add(jtoken);
					}
				}
				return;
			}
			IL_B6:
			target.ClearItems();
			using (IEnumerator enumerator = content.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					object obj3 = enumerator.Current;
					JToken content3 = (JToken)obj3;
					target.Add(content3);
				}
				return;
			}
			IL_FB:
			int num = 0;
			using (IEnumerator enumerator = content.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					object obj4 = enumerator.Current;
					if (num < target.Count)
					{
						JContainer jcontainer = target[num] as JContainer;
						if (jcontainer != null)
						{
							jcontainer.Merge(obj4, settings);
						}
						else if (obj4 != null)
						{
							JToken jtoken2 = JContainer.CreateFromContent(obj4);
							if (jtoken2.Type != JTokenType.Null)
							{
								target[num] = jtoken2;
							}
						}
					}
					else
					{
						target.Add(obj4);
					}
					num++;
				}
				return;
			}
			IL_18C:
			throw new ArgumentOutOfRangeException("settings", "Unexpected merge array handling when merging JSON.");
		}

		// Token: 0x0400028C RID: 652
		internal ListChangedEventHandler _listChanged;

		// Token: 0x0400028D RID: 653
		internal System.ComponentModel.AddingNewEventHandler _addingNew;

		// Token: 0x0400028E RID: 654
		private object _syncRoot;

		// Token: 0x0400028F RID: 655
		private bool _busy;
	}
}
