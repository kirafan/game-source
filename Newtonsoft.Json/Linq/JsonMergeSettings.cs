﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000091 RID: 145
	[Preserve]
	public class JsonMergeSettings
	{
		// Token: 0x1700014C RID: 332
		// (get) Token: 0x06000671 RID: 1649 RVA: 0x0001A440 File Offset: 0x00018640
		// (set) Token: 0x06000672 RID: 1650 RVA: 0x0001A448 File Offset: 0x00018648
		public MergeArrayHandling MergeArrayHandling
		{
			get
			{
				return this._mergeArrayHandling;
			}
			set
			{
				if (value < MergeArrayHandling.Concat || value > MergeArrayHandling.Merge)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._mergeArrayHandling = value;
			}
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x06000673 RID: 1651 RVA: 0x0001A464 File Offset: 0x00018664
		// (set) Token: 0x06000674 RID: 1652 RVA: 0x0001A46C File Offset: 0x0001866C
		public MergeNullValueHandling MergeNullValueHandling
		{
			get
			{
				return this._mergeNullValueHandling;
			}
			set
			{
				if (value < MergeNullValueHandling.Ignore || value > MergeNullValueHandling.Merge)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._mergeNullValueHandling = value;
			}
		}

		// Token: 0x04000280 RID: 640
		private MergeArrayHandling _mergeArrayHandling;

		// Token: 0x04000281 RID: 641
		private MergeNullValueHandling _mergeNullValueHandling;
	}
}
