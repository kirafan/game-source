﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000092 RID: 146
	[Preserve]
	public enum MergeArrayHandling
	{
		// Token: 0x04000283 RID: 643
		Concat,
		// Token: 0x04000284 RID: 644
		Union,
		// Token: 0x04000285 RID: 645
		Replace,
		// Token: 0x04000286 RID: 646
		Merge
	}
}
