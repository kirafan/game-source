﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000096 RID: 150
	[Preserve]
	public class JTokenEqualityComparer : IEqualityComparer<JToken>
	{
		// Token: 0x0600067B RID: 1659 RVA: 0x0001A50C File Offset: 0x0001870C
		public bool Equals(JToken x, JToken y)
		{
			return JToken.DeepEquals(x, y);
		}

		// Token: 0x0600067C RID: 1660 RVA: 0x0001A515 File Offset: 0x00018715
		public int GetHashCode(JToken obj)
		{
			if (obj == null)
			{
				return 0;
			}
			return obj.GetDeepHashCode();
		}
	}
}
