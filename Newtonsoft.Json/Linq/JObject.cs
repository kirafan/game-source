﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200009B RID: 155
	[Preserve]
	public class JObject : JContainer, IDictionary<string, JToken>, ICollection<KeyValuePair<string, JToken>>, IEnumerable<KeyValuePair<string, JToken>>, IEnumerable, INotifyPropertyChanged, ICustomTypeDescriptor, System.ComponentModel.INotifyPropertyChanging
	{
		// Token: 0x17000169 RID: 361
		// (get) Token: 0x06000708 RID: 1800 RVA: 0x0001BA44 File Offset: 0x00019C44
		protected override IList<JToken> ChildrenTokens
		{
			get
			{
				return this._properties;
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000709 RID: 1801 RVA: 0x0001BA4C File Offset: 0x00019C4C
		// (remove) Token: 0x0600070A RID: 1802 RVA: 0x0001BA84 File Offset: 0x00019C84
		public event PropertyChangedEventHandler PropertyChanged;

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x0600070B RID: 1803 RVA: 0x0001BABC File Offset: 0x00019CBC
		// (remove) Token: 0x0600070C RID: 1804 RVA: 0x0001BAF4 File Offset: 0x00019CF4
		public event System.ComponentModel.PropertyChangingEventHandler PropertyChanging;

		// Token: 0x0600070D RID: 1805 RVA: 0x0001BB29 File Offset: 0x00019D29
		public JObject()
		{
		}

		// Token: 0x0600070E RID: 1806 RVA: 0x0001BB3C File Offset: 0x00019D3C
		public JObject(JObject other) : base(other)
		{
		}

		// Token: 0x0600070F RID: 1807 RVA: 0x0001BB50 File Offset: 0x00019D50
		public JObject(params object[] content) : this(content)
		{
		}

		// Token: 0x06000710 RID: 1808 RVA: 0x0001BB59 File Offset: 0x00019D59
		public JObject(object content)
		{
			this.Add(content);
		}

		// Token: 0x06000711 RID: 1809 RVA: 0x0001BB74 File Offset: 0x00019D74
		internal override bool DeepEquals(JToken node)
		{
			JObject jobject = node as JObject;
			return jobject != null && this._properties.Compare(jobject._properties);
		}

		// Token: 0x06000712 RID: 1810 RVA: 0x0001BB9E File Offset: 0x00019D9E
		internal override int IndexOfItem(JToken item)
		{
			return this._properties.IndexOfReference(item);
		}

		// Token: 0x06000713 RID: 1811 RVA: 0x0001BBAC File Offset: 0x00019DAC
		internal override void InsertItem(int index, JToken item, bool skipParentCheck)
		{
			if (item != null && item.Type == JTokenType.Comment)
			{
				return;
			}
			base.InsertItem(index, item, skipParentCheck);
		}

		// Token: 0x06000714 RID: 1812 RVA: 0x0001BBC4 File Offset: 0x00019DC4
		internal override void ValidateToken(JToken o, JToken existing)
		{
			ValidationUtils.ArgumentNotNull(o, "o");
			if (o.Type != JTokenType.Property)
			{
				throw new ArgumentException("Can not add {0} to {1}.".FormatWith(CultureInfo.InvariantCulture, o.GetType(), base.GetType()));
			}
			JProperty jproperty = (JProperty)o;
			if (existing != null)
			{
				JProperty jproperty2 = (JProperty)existing;
				if (jproperty.Name == jproperty2.Name)
				{
					return;
				}
			}
			if (this._properties.TryGetValue(jproperty.Name, out existing))
			{
				throw new ArgumentException("Can not add property {0} to {1}. Property with the same name already exists on object.".FormatWith(CultureInfo.InvariantCulture, jproperty.Name, base.GetType()));
			}
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x0001BC64 File Offset: 0x00019E64
		internal override void MergeItem(object content, JsonMergeSettings settings)
		{
			JObject jobject = content as JObject;
			if (jobject == null)
			{
				return;
			}
			foreach (KeyValuePair<string, JToken> keyValuePair in jobject)
			{
				JProperty jproperty = this.Property(keyValuePair.Key);
				if (jproperty == null)
				{
					this.Add(keyValuePair.Key, keyValuePair.Value);
				}
				else if (keyValuePair.Value != null)
				{
					JContainer jcontainer = jproperty.Value as JContainer;
					if (jcontainer == null)
					{
						if (keyValuePair.Value.Type != JTokenType.Null || (settings != null && settings.MergeNullValueHandling == MergeNullValueHandling.Merge))
						{
							jproperty.Value = keyValuePair.Value;
						}
					}
					else if (jcontainer.Type != keyValuePair.Value.Type)
					{
						jproperty.Value = keyValuePair.Value;
					}
					else
					{
						jcontainer.Merge(keyValuePair.Value, settings);
					}
				}
			}
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x0001BD58 File Offset: 0x00019F58
		internal void InternalPropertyChanged(JProperty childProperty)
		{
			this.OnPropertyChanged(childProperty.Name);
			if (this._listChanged != null)
			{
				this.OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, this.IndexOfItem(childProperty)));
			}
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x0001BD81 File Offset: 0x00019F81
		internal void InternalPropertyChanging(JProperty childProperty)
		{
			this.OnPropertyChanging(childProperty.Name);
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x0001BD8F File Offset: 0x00019F8F
		internal override JToken CloneToken()
		{
			return new JObject(this);
		}

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x06000719 RID: 1817 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override JTokenType Type
		{
			get
			{
				return JTokenType.Object;
			}
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x0001BD97 File Offset: 0x00019F97
		public IEnumerable<JProperty> Properties()
		{
			return this._properties.Cast<JProperty>();
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x0001BDA4 File Offset: 0x00019FA4
		public JProperty Property(string name)
		{
			if (name == null)
			{
				return null;
			}
			JToken jtoken;
			this._properties.TryGetValue(name, out jtoken);
			return (JProperty)jtoken;
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x0001BDCB File Offset: 0x00019FCB
		public JEnumerable<JToken> PropertyValues()
		{
			return new JEnumerable<JToken>(from p in this.Properties()
			select p.Value);
		}

		// Token: 0x1700016B RID: 363
		public override JToken this[object key]
		{
			get
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				string text = key as string;
				if (text == null)
				{
					throw new ArgumentException("Accessed JObject values with invalid key value: {0}. Object property name expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				return this[text];
			}
			set
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				string text = key as string;
				if (text == null)
				{
					throw new ArgumentException("Set JObject values with invalid key value: {0}. Object property name expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				this[text] = value;
			}
		}

		// Token: 0x1700016C RID: 364
		public JToken this[string propertyName]
		{
			get
			{
				ValidationUtils.ArgumentNotNull(propertyName, "propertyName");
				JProperty jproperty = this.Property(propertyName);
				if (jproperty == null)
				{
					return null;
				}
				return jproperty.Value;
			}
			set
			{
				JProperty jproperty = this.Property(propertyName);
				if (jproperty != null)
				{
					jproperty.Value = value;
					return;
				}
				this.OnPropertyChanging(propertyName);
				this.Add(new JProperty(propertyName, value));
				this.OnPropertyChanged(propertyName);
			}
		}

		// Token: 0x06000721 RID: 1825 RVA: 0x0001BEEF File Offset: 0x0001A0EF
		public new static JObject Load(JsonReader reader)
		{
			return JObject.Load(reader, null);
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x0001BEF8 File Offset: 0x0001A0F8
		public new static JObject Load(JsonReader reader, JsonLoadSettings settings)
		{
			ValidationUtils.ArgumentNotNull(reader, "reader");
			if (reader.TokenType == JsonToken.None && !reader.Read())
			{
				throw JsonReaderException.Create(reader, "Error reading JObject from JsonReader.");
			}
			reader.MoveToContent();
			if (reader.TokenType != JsonToken.StartObject)
			{
				throw JsonReaderException.Create(reader, "Error reading JObject from JsonReader. Current JsonReader item is not an object: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
			}
			JObject jobject = new JObject();
			jobject.SetLineInfo(reader as IJsonLineInfo, settings);
			jobject.ReadTokenFrom(reader, settings);
			return jobject;
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x0001BF77 File Offset: 0x0001A177
		public new static JObject Parse(string json)
		{
			return JObject.Parse(json, null);
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x0001BF80 File Offset: 0x0001A180
		public new static JObject Parse(string json, JsonLoadSettings settings)
		{
			JObject result;
			using (JsonReader jsonReader = new JsonTextReader(new StringReader(json)))
			{
				JObject jobject = JObject.Load(jsonReader, settings);
				if (jsonReader.Read() && jsonReader.TokenType != JsonToken.Comment)
				{
					throw JsonReaderException.Create(jsonReader, "Additional text found in JSON string after parsing content.");
				}
				result = jobject;
			}
			return result;
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x0001BFDC File Offset: 0x0001A1DC
		public new static JObject FromObject(object o)
		{
			return JObject.FromObject(o, JsonSerializer.CreateDefault());
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x0001BFEC File Offset: 0x0001A1EC
		public new static JObject FromObject(object o, JsonSerializer jsonSerializer)
		{
			JToken jtoken = JToken.FromObjectInternal(o, jsonSerializer);
			if (jtoken != null && jtoken.Type != JTokenType.Object)
			{
				throw new ArgumentException("Object serialized to {0}. JObject instance expected.".FormatWith(CultureInfo.InvariantCulture, jtoken.Type));
			}
			return (JObject)jtoken;
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x0001C034 File Offset: 0x0001A234
		public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
		{
			writer.WriteStartObject();
			for (int i = 0; i < this._properties.Count; i++)
			{
				this._properties[i].WriteTo(writer, converters);
			}
			writer.WriteEndObject();
		}

		// Token: 0x06000728 RID: 1832 RVA: 0x0001C076 File Offset: 0x0001A276
		public JToken GetValue(string propertyName)
		{
			return this.GetValue(propertyName, StringComparison.Ordinal);
		}

		// Token: 0x06000729 RID: 1833 RVA: 0x0001C080 File Offset: 0x0001A280
		public JToken GetValue(string propertyName, StringComparison comparison)
		{
			if (propertyName == null)
			{
				return null;
			}
			JProperty jproperty = this.Property(propertyName);
			if (jproperty != null)
			{
				return jproperty.Value;
			}
			if (comparison != StringComparison.Ordinal)
			{
				foreach (JToken jtoken in this._properties)
				{
					JProperty jproperty2 = (JProperty)jtoken;
					if (string.Equals(jproperty2.Name, propertyName, comparison))
					{
						return jproperty2.Value;
					}
				}
			}
			return null;
		}

		// Token: 0x0600072A RID: 1834 RVA: 0x0001C104 File Offset: 0x0001A304
		public bool TryGetValue(string propertyName, StringComparison comparison, out JToken value)
		{
			value = this.GetValue(propertyName, comparison);
			return value != null;
		}

		// Token: 0x0600072B RID: 1835 RVA: 0x0001C115 File Offset: 0x0001A315
		public void Add(string propertyName, JToken value)
		{
			this.Add(new JProperty(propertyName, value));
		}

		// Token: 0x0600072C RID: 1836 RVA: 0x0001C124 File Offset: 0x0001A324
		bool IDictionary<string, JToken>.ContainsKey(string key)
		{
			return this._properties.Contains(key);
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x0600072D RID: 1837 RVA: 0x0001C132 File Offset: 0x0001A332
		ICollection<string> IDictionary<string, JToken>.Keys
		{
			get
			{
				return this._properties.Keys;
			}
		}

		// Token: 0x0600072E RID: 1838 RVA: 0x0001C140 File Offset: 0x0001A340
		public bool Remove(string propertyName)
		{
			JProperty jproperty = this.Property(propertyName);
			if (jproperty == null)
			{
				return false;
			}
			jproperty.Remove();
			return true;
		}

		// Token: 0x0600072F RID: 1839 RVA: 0x0001C164 File Offset: 0x0001A364
		public bool TryGetValue(string propertyName, out JToken value)
		{
			JProperty jproperty = this.Property(propertyName);
			if (jproperty == null)
			{
				value = null;
				return false;
			}
			value = jproperty.Value;
			return true;
		}

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06000730 RID: 1840 RVA: 0x0001C18A File Offset: 0x0001A38A
		ICollection<JToken> IDictionary<string, JToken>.Values
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x06000731 RID: 1841 RVA: 0x0001C191 File Offset: 0x0001A391
		void ICollection<KeyValuePair<string, JToken>>.Add(KeyValuePair<string, JToken> item)
		{
			this.Add(new JProperty(item.Key, item.Value));
		}

		// Token: 0x06000732 RID: 1842 RVA: 0x0001C1AC File Offset: 0x0001A3AC
		void ICollection<KeyValuePair<string, JToken>>.Clear()
		{
			base.RemoveAll();
		}

		// Token: 0x06000733 RID: 1843 RVA: 0x0001C1B4 File Offset: 0x0001A3B4
		bool ICollection<KeyValuePair<string, JToken>>.Contains(KeyValuePair<string, JToken> item)
		{
			JProperty jproperty = this.Property(item.Key);
			return jproperty != null && jproperty.Value == item.Value;
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x0001C1E4 File Offset: 0x0001A3E4
		void ICollection<KeyValuePair<string, JToken>>.CopyTo(KeyValuePair<string, JToken>[] array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException("arrayIndex", "arrayIndex is less than 0.");
			}
			if (arrayIndex >= array.Length && arrayIndex != 0)
			{
				throw new ArgumentException("arrayIndex is equal to or greater than the length of array.");
			}
			if (base.Count > array.Length - arrayIndex)
			{
				throw new ArgumentException("The number of elements in the source JObject is greater than the available space from arrayIndex to the end of the destination array.");
			}
			int num = 0;
			foreach (JToken jtoken in this._properties)
			{
				JProperty jproperty = (JProperty)jtoken;
				array[arrayIndex + num] = new KeyValuePair<string, JToken>(jproperty.Name, jproperty.Value);
				num++;
			}
		}

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x06000735 RID: 1845 RVA: 0x0000CB98 File Offset: 0x0000AD98
		bool ICollection<KeyValuePair<string, JToken>>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x0001C2A0 File Offset: 0x0001A4A0
		bool ICollection<KeyValuePair<string, JToken>>.Remove(KeyValuePair<string, JToken> item)
		{
			if (!((ICollection<KeyValuePair<string, JToken>>)this).Contains(item))
			{
				return false;
			}
			((IDictionary<string, JToken>)this).Remove(item.Key);
			return true;
		}

		// Token: 0x06000737 RID: 1847 RVA: 0x0001C2BC File Offset: 0x0001A4BC
		internal override int GetDeepHashCode()
		{
			return base.ContentsHashCode();
		}

		// Token: 0x06000738 RID: 1848 RVA: 0x0001C2C4 File Offset: 0x0001A4C4
		public IEnumerator<KeyValuePair<string, JToken>> GetEnumerator()
		{
			foreach (JToken jtoken in this._properties)
			{
				JProperty jproperty = (JProperty)jtoken;
				yield return new KeyValuePair<string, JToken>(jproperty.Name, jproperty.Value);
			}
			IEnumerator<JToken> enumerator = null;
			yield break;
			yield break;
		}

		// Token: 0x06000739 RID: 1849 RVA: 0x0001C2D3 File Offset: 0x0001A4D3
		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		// Token: 0x0600073A RID: 1850 RVA: 0x0001C2EF File Offset: 0x0001A4EF
		protected virtual void OnPropertyChanging(string propertyName)
		{
			if (this.PropertyChanging != null)
			{
				this.PropertyChanging(this, new System.ComponentModel.PropertyChangingEventArgs(propertyName));
			}
		}

		// Token: 0x0600073B RID: 1851 RVA: 0x0001C30B File Offset: 0x0001A50B
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
		{
			return ((ICustomTypeDescriptor)this).GetProperties(null);
		}

		// Token: 0x0600073C RID: 1852 RVA: 0x0001C314 File Offset: 0x0001A514
		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(Attribute[] attributes)
		{
			PropertyDescriptorCollection propertyDescriptorCollection = new PropertyDescriptorCollection(null);
			foreach (KeyValuePair<string, JToken> keyValuePair in this)
			{
				propertyDescriptorCollection.Add(new JPropertyDescriptor(keyValuePair.Key));
			}
			return propertyDescriptorCollection;
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x0001C370 File Offset: 0x0001A570
		AttributeCollection ICustomTypeDescriptor.GetAttributes()
		{
			return AttributeCollection.Empty;
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x0001B798 File Offset: 0x00019998
		string ICustomTypeDescriptor.GetClassName()
		{
			return null;
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x0001B798 File Offset: 0x00019998
		string ICustomTypeDescriptor.GetComponentName()
		{
			return null;
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0001C377 File Offset: 0x0001A577
		TypeConverter ICustomTypeDescriptor.GetConverter()
		{
			return new TypeConverter();
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x0001B798 File Offset: 0x00019998
		EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
		{
			return null;
		}

		// Token: 0x06000742 RID: 1858 RVA: 0x0001B798 File Offset: 0x00019998
		PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
		{
			return null;
		}

		// Token: 0x06000743 RID: 1859 RVA: 0x0001B798 File Offset: 0x00019998
		object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
		{
			return null;
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x0001C37E File Offset: 0x0001A57E
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents(Attribute[] attributes)
		{
			return EventDescriptorCollection.Empty;
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x0001C37E File Offset: 0x0001A57E
		EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
		{
			return EventDescriptorCollection.Empty;
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x0001B798 File Offset: 0x00019998
		object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
		{
			return null;
		}

		// Token: 0x04000292 RID: 658
		private readonly JPropertyKeyedCollection _properties = new JPropertyKeyedCollection();
	}
}
