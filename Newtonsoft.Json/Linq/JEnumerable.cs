﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200009A RID: 154
	[Preserve]
	public struct JEnumerable<T> : IJEnumerable<T>, IEnumerable<T>, IEnumerable where T : JToken
	{
		// Token: 0x06000700 RID: 1792 RVA: 0x0001B97C File Offset: 0x00019B7C
		public JEnumerable(IEnumerable<T> enumerable)
		{
			ValidationUtils.ArgumentNotNull(enumerable, "enumerable");
			this._enumerable = enumerable;
		}

		// Token: 0x06000701 RID: 1793 RVA: 0x0001B990 File Offset: 0x00019B90
		public IEnumerator<T> GetEnumerator()
		{
			if (this._enumerable == null)
			{
				return JEnumerable<T>.Empty.GetEnumerator();
			}
			return this._enumerable.GetEnumerator();
		}

		// Token: 0x06000702 RID: 1794 RVA: 0x0001B9BE File Offset: 0x00019BBE
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x17000168 RID: 360
		public IJEnumerable<JToken> this[object key]
		{
			get
			{
				if (this._enumerable == null)
				{
					return JEnumerable<JToken>.Empty;
				}
				return new JEnumerable<JToken>(this._enumerable.Values(key));
			}
		}

		// Token: 0x06000704 RID: 1796 RVA: 0x0001B9F1 File Offset: 0x00019BF1
		public bool Equals(JEnumerable<T> other)
		{
			return object.Equals(this._enumerable, other._enumerable);
		}

		// Token: 0x06000705 RID: 1797 RVA: 0x0001BA04 File Offset: 0x00019C04
		public override bool Equals(object obj)
		{
			return obj is JEnumerable<T> && this.Equals((JEnumerable<T>)obj);
		}

		// Token: 0x06000706 RID: 1798 RVA: 0x0001BA1C File Offset: 0x00019C1C
		public override int GetHashCode()
		{
			if (this._enumerable == null)
			{
				return 0;
			}
			return this._enumerable.GetHashCode();
		}

		// Token: 0x04000290 RID: 656
		public static readonly JEnumerable<T> Empty = new JEnumerable<T>(Enumerable.Empty<T>());

		// Token: 0x04000291 RID: 657
		private readonly IEnumerable<T> _enumerable;
	}
}
