﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x020000A2 RID: 162
	[Preserve]
	public class JValue : JToken, IFormattable, IComparable, IConvertible
	{
		// Token: 0x0600084A RID: 2122 RVA: 0x0001F387 File Offset: 0x0001D587
		internal JValue(object value, JTokenType type)
		{
			this._value = value;
			this._valueType = type;
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x0001F39D File Offset: 0x0001D59D
		public JValue(JValue other) : this(other.Value, other.Type)
		{
		}

		// Token: 0x0600084C RID: 2124 RVA: 0x0001F3B1 File Offset: 0x0001D5B1
		public JValue(long value) : this(value, JTokenType.Integer)
		{
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x0001F3C0 File Offset: 0x0001D5C0
		public JValue(decimal value) : this(value, JTokenType.Float)
		{
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x0001F3CF File Offset: 0x0001D5CF
		public JValue(char value) : this(value, JTokenType.String)
		{
		}

		// Token: 0x0600084F RID: 2127 RVA: 0x0001F3DE File Offset: 0x0001D5DE
		[CLSCompliant(false)]
		public JValue(ulong value) : this(value, JTokenType.Integer)
		{
		}

		// Token: 0x06000850 RID: 2128 RVA: 0x0001F3ED File Offset: 0x0001D5ED
		public JValue(double value) : this(value, JTokenType.Float)
		{
		}

		// Token: 0x06000851 RID: 2129 RVA: 0x0001F3FC File Offset: 0x0001D5FC
		public JValue(float value) : this(value, JTokenType.Float)
		{
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x0001F40B File Offset: 0x0001D60B
		public JValue(DateTime value) : this(value, JTokenType.Date)
		{
		}

		// Token: 0x06000853 RID: 2131 RVA: 0x0001F41B File Offset: 0x0001D61B
		public JValue(DateTimeOffset value) : this(value, JTokenType.Date)
		{
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x0001F42B File Offset: 0x0001D62B
		public JValue(bool value) : this(value, JTokenType.Boolean)
		{
		}

		// Token: 0x06000855 RID: 2133 RVA: 0x0001F43B File Offset: 0x0001D63B
		public JValue(string value) : this(value, JTokenType.String)
		{
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x0001F445 File Offset: 0x0001D645
		public JValue(Guid value) : this(value, JTokenType.Guid)
		{
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x0001F455 File Offset: 0x0001D655
		public JValue(Uri value) : this(value, (value != null) ? JTokenType.Uri : JTokenType.Null)
		{
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x0001F46D File Offset: 0x0001D66D
		public JValue(TimeSpan value) : this(value, JTokenType.TimeSpan)
		{
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x0001F480 File Offset: 0x0001D680
		public JValue(object value) : this(value, JValue.GetValueType(null, value))
		{
		}

		// Token: 0x0600085A RID: 2138 RVA: 0x0001F4A4 File Offset: 0x0001D6A4
		internal override bool DeepEquals(JToken node)
		{
			JValue jvalue = node as JValue;
			return jvalue != null && (jvalue == this || JValue.ValuesEquals(this, jvalue));
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x0600085B RID: 2139 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool HasValues
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600085C RID: 2140 RVA: 0x0001F4CC File Offset: 0x0001D6CC
		internal static int Compare(JTokenType valueType, object objA, object objB)
		{
			if (objA == null && objB == null)
			{
				return 0;
			}
			if (objA != null && objB == null)
			{
				return 1;
			}
			if (objA == null && objB != null)
			{
				return -1;
			}
			switch (valueType)
			{
			case JTokenType.Comment:
			case JTokenType.String:
			case JTokenType.Raw:
			{
				string strA = Convert.ToString(objA, CultureInfo.InvariantCulture);
				string strB = Convert.ToString(objB, CultureInfo.InvariantCulture);
				return string.CompareOrdinal(strA, strB);
			}
			case JTokenType.Integer:
				if (objA is ulong || objB is ulong || objA is decimal || objB is decimal)
				{
					return Convert.ToDecimal(objA, CultureInfo.InvariantCulture).CompareTo(Convert.ToDecimal(objB, CultureInfo.InvariantCulture));
				}
				if (objA is float || objB is float || objA is double || objB is double)
				{
					return JValue.CompareFloat(objA, objB);
				}
				return Convert.ToInt64(objA, CultureInfo.InvariantCulture).CompareTo(Convert.ToInt64(objB, CultureInfo.InvariantCulture));
			case JTokenType.Float:
				return JValue.CompareFloat(objA, objB);
			case JTokenType.Boolean:
			{
				bool flag = Convert.ToBoolean(objA, CultureInfo.InvariantCulture);
				bool value = Convert.ToBoolean(objB, CultureInfo.InvariantCulture);
				return flag.CompareTo(value);
			}
			case JTokenType.Date:
			{
				if (objA is DateTime)
				{
					DateTime dateTime = (DateTime)objA;
					DateTime value2;
					if (objB is DateTimeOffset)
					{
						value2 = ((DateTimeOffset)objB).DateTime;
					}
					else
					{
						value2 = Convert.ToDateTime(objB, CultureInfo.InvariantCulture);
					}
					return dateTime.CompareTo(value2);
				}
				DateTimeOffset dateTimeOffset = (DateTimeOffset)objA;
				DateTimeOffset other;
				if (objB is DateTimeOffset)
				{
					other = (DateTimeOffset)objB;
				}
				else
				{
					other = new DateTimeOffset(Convert.ToDateTime(objB, CultureInfo.InvariantCulture));
				}
				return dateTimeOffset.CompareTo(other);
			}
			case JTokenType.Bytes:
			{
				if (!(objB is byte[]))
				{
					throw new ArgumentException("Object must be of type byte[].");
				}
				byte[] array = objA as byte[];
				byte[] array2 = objB as byte[];
				if (array == null)
				{
					return -1;
				}
				if (array2 == null)
				{
					return 1;
				}
				return MiscellaneousUtils.ByteArrayCompare(array, array2);
			}
			case JTokenType.Guid:
			{
				if (!(objB is Guid))
				{
					throw new ArgumentException("Object must be of type Guid.");
				}
				Guid guid = (Guid)objA;
				Guid value3 = (Guid)objB;
				return guid.CompareTo(value3);
			}
			case JTokenType.Uri:
			{
				if (!(objB is Uri))
				{
					throw new ArgumentException("Object must be of type Uri.");
				}
				Uri uri = (Uri)objA;
				Uri uri2 = (Uri)objB;
				return Comparer<string>.Default.Compare(uri.ToString(), uri2.ToString());
			}
			case JTokenType.TimeSpan:
			{
				if (!(objB is TimeSpan))
				{
					throw new ArgumentException("Object must be of type TimeSpan.");
				}
				TimeSpan timeSpan = (TimeSpan)objA;
				TimeSpan value4 = (TimeSpan)objB;
				return timeSpan.CompareTo(value4);
			}
			}
			throw MiscellaneousUtils.CreateArgumentOutOfRangeException("valueType", valueType, "Unexpected value type: {0}".FormatWith(CultureInfo.InvariantCulture, valueType));
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x0001F76C File Offset: 0x0001D96C
		private static int CompareFloat(object objA, object objB)
		{
			double d = Convert.ToDouble(objA, CultureInfo.InvariantCulture);
			double num = Convert.ToDouble(objB, CultureInfo.InvariantCulture);
			if (MathUtils.ApproxEquals(d, num))
			{
				return 0;
			}
			return d.CompareTo(num);
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x0001F7A4 File Offset: 0x0001D9A4
		internal override JToken CloneToken()
		{
			return new JValue(this);
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x0001F7AC File Offset: 0x0001D9AC
		public static JValue CreateComment(string value)
		{
			return new JValue(value, JTokenType.Comment);
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x0001F7B5 File Offset: 0x0001D9B5
		public static JValue CreateString(string value)
		{
			return new JValue(value, JTokenType.String);
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x0001F7BE File Offset: 0x0001D9BE
		public static JValue CreateNull()
		{
			return new JValue(null, JTokenType.Null);
		}

		// Token: 0x06000862 RID: 2146 RVA: 0x0001F7C8 File Offset: 0x0001D9C8
		public static JValue CreateUndefined()
		{
			return new JValue(null, JTokenType.Undefined);
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x0001F7D4 File Offset: 0x0001D9D4
		private static JTokenType GetValueType(JTokenType? current, object value)
		{
			if (value == null)
			{
				return JTokenType.Null;
			}
			if (value == DBNull.Value)
			{
				return JTokenType.Null;
			}
			if (value is string)
			{
				return JValue.GetStringValueType(current);
			}
			if (value is long || value is int || value is short || value is sbyte || value is ulong || value is uint || value is ushort || value is byte)
			{
				return JTokenType.Integer;
			}
			if (value is Enum)
			{
				return JTokenType.Integer;
			}
			if (value is double || value is float || value is decimal)
			{
				return JTokenType.Float;
			}
			if (value is DateTime)
			{
				return JTokenType.Date;
			}
			if (value is DateTimeOffset)
			{
				return JTokenType.Date;
			}
			if (value is byte[])
			{
				return JTokenType.Bytes;
			}
			if (value is bool)
			{
				return JTokenType.Boolean;
			}
			if (value is Guid)
			{
				return JTokenType.Guid;
			}
			if (value is Uri)
			{
				return JTokenType.Uri;
			}
			if (value is TimeSpan)
			{
				return JTokenType.TimeSpan;
			}
			throw new ArgumentException("Could not determine JSON object type for type {0}.".FormatWith(CultureInfo.InvariantCulture, value.GetType()));
		}

		// Token: 0x06000864 RID: 2148 RVA: 0x0001F8D0 File Offset: 0x0001DAD0
		private static JTokenType GetStringValueType(JTokenType? current)
		{
			if (current == null)
			{
				return JTokenType.String;
			}
			JTokenType valueOrDefault = current.GetValueOrDefault();
			if (valueOrDefault == JTokenType.Comment || valueOrDefault == JTokenType.String || valueOrDefault == JTokenType.Raw)
			{
				return current.GetValueOrDefault();
			}
			return JTokenType.String;
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000865 RID: 2149 RVA: 0x0001F906 File Offset: 0x0001DB06
		public override JTokenType Type
		{
			get
			{
				return this._valueType;
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000866 RID: 2150 RVA: 0x0001F90E File Offset: 0x0001DB0E
		// (set) Token: 0x06000867 RID: 2151 RVA: 0x0001F918 File Offset: 0x0001DB18
		public new object Value
		{
			get
			{
				return this._value;
			}
			set
			{
				Type type = (this._value != null) ? this._value.GetType() : null;
				Type type2 = (value != null) ? value.GetType() : null;
				if (type != type2)
				{
					this._valueType = JValue.GetValueType(new JTokenType?(this._valueType), value);
				}
				this._value = value;
			}
		}

		// Token: 0x06000868 RID: 2152 RVA: 0x0001F96C File Offset: 0x0001DB6C
		public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
		{
			if (converters != null && converters.Length != 0 && this._value != null)
			{
				JsonConverter matchingConverter = JsonSerializer.GetMatchingConverter(converters, this._value.GetType());
				if (matchingConverter != null && matchingConverter.CanWrite)
				{
					matchingConverter.WriteJson(writer, this._value, JsonSerializer.CreateDefault());
					return;
				}
			}
			switch (this._valueType)
			{
			case JTokenType.Comment:
				writer.WriteComment((this._value != null) ? this._value.ToString() : null);
				return;
			case JTokenType.Integer:
				if (this._value is int)
				{
					writer.WriteValue((int)this._value);
					return;
				}
				if (this._value is long)
				{
					writer.WriteValue((long)this._value);
					return;
				}
				if (this._value is ulong)
				{
					writer.WriteValue((ulong)this._value);
					return;
				}
				writer.WriteValue(Convert.ToInt64(this._value, CultureInfo.InvariantCulture));
				return;
			case JTokenType.Float:
				if (this._value is decimal)
				{
					writer.WriteValue((decimal)this._value);
					return;
				}
				if (this._value is double)
				{
					writer.WriteValue((double)this._value);
					return;
				}
				if (this._value is float)
				{
					writer.WriteValue((float)this._value);
					return;
				}
				writer.WriteValue(Convert.ToDouble(this._value, CultureInfo.InvariantCulture));
				return;
			case JTokenType.String:
				writer.WriteValue((this._value != null) ? this._value.ToString() : null);
				return;
			case JTokenType.Boolean:
				writer.WriteValue(Convert.ToBoolean(this._value, CultureInfo.InvariantCulture));
				return;
			case JTokenType.Null:
				writer.WriteNull();
				return;
			case JTokenType.Undefined:
				writer.WriteUndefined();
				return;
			case JTokenType.Date:
				if (this._value is DateTimeOffset)
				{
					writer.WriteValue((DateTimeOffset)this._value);
					return;
				}
				writer.WriteValue(Convert.ToDateTime(this._value, CultureInfo.InvariantCulture));
				return;
			case JTokenType.Raw:
				writer.WriteRawValue((this._value != null) ? this._value.ToString() : null);
				return;
			case JTokenType.Bytes:
				writer.WriteValue((byte[])this._value);
				return;
			case JTokenType.Guid:
				writer.WriteValue((this._value != null) ? ((Guid?)this._value) : null);
				return;
			case JTokenType.Uri:
				writer.WriteValue((Uri)this._value);
				return;
			case JTokenType.TimeSpan:
				writer.WriteValue((this._value != null) ? ((TimeSpan?)this._value) : null);
				return;
			default:
				throw MiscellaneousUtils.CreateArgumentOutOfRangeException("TokenType", this._valueType, "Unexpected token type.");
			}
		}

		// Token: 0x06000869 RID: 2153 RVA: 0x0001FC24 File Offset: 0x0001DE24
		internal override int GetDeepHashCode()
		{
			int num = (this._value != null) ? this._value.GetHashCode() : 0;
			int valueType = (int)this._valueType;
			return valueType.GetHashCode() ^ num;
		}

		// Token: 0x0600086A RID: 2154 RVA: 0x0001FC58 File Offset: 0x0001DE58
		private static bool ValuesEquals(JValue v1, JValue v2)
		{
			return v1 == v2 || (v1._valueType == v2._valueType && JValue.Compare(v1._valueType, v1._value, v2._value) == 0);
		}

		// Token: 0x0600086B RID: 2155 RVA: 0x0001FC8A File Offset: 0x0001DE8A
		public bool Equals(JValue other)
		{
			return other != null && JValue.ValuesEquals(this, other);
		}

		// Token: 0x0600086C RID: 2156 RVA: 0x0001FC98 File Offset: 0x0001DE98
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			JValue jvalue = obj as JValue;
			if (jvalue != null)
			{
				return this.Equals(jvalue);
			}
			return base.Equals(obj);
		}

		// Token: 0x0600086D RID: 2157 RVA: 0x0001FCC3 File Offset: 0x0001DEC3
		public override int GetHashCode()
		{
			if (this._value == null)
			{
				return 0;
			}
			return this._value.GetHashCode();
		}

		// Token: 0x0600086E RID: 2158 RVA: 0x0001FCDA File Offset: 0x0001DEDA
		public override string ToString()
		{
			if (this._value == null)
			{
				return string.Empty;
			}
			return this._value.ToString();
		}

		// Token: 0x0600086F RID: 2159 RVA: 0x0001FCF5 File Offset: 0x0001DEF5
		public string ToString(string format)
		{
			return this.ToString(format, CultureInfo.CurrentCulture);
		}

		// Token: 0x06000870 RID: 2160 RVA: 0x0001FD03 File Offset: 0x0001DF03
		public string ToString(IFormatProvider formatProvider)
		{
			return this.ToString(null, formatProvider);
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x0001FD10 File Offset: 0x0001DF10
		public string ToString(string format, IFormatProvider formatProvider)
		{
			if (this._value == null)
			{
				return string.Empty;
			}
			IFormattable formattable = this._value as IFormattable;
			if (formattable != null)
			{
				return formattable.ToString(format, formatProvider);
			}
			return this._value.ToString();
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x0001FD50 File Offset: 0x0001DF50
		int IComparable.CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			object objB = (obj is JValue) ? ((JValue)obj).Value : obj;
			return JValue.Compare(this._valueType, this._value, objB);
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x0001FD8B File Offset: 0x0001DF8B
		public int CompareTo(JValue obj)
		{
			if (obj == null)
			{
				return 1;
			}
			return JValue.Compare(this._valueType, this._value, obj._value);
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x0001FDAC File Offset: 0x0001DFAC
		TypeCode IConvertible.GetTypeCode()
		{
			if (this._value == null)
			{
				return TypeCode.Empty;
			}
			IConvertible convertible = this._value as IConvertible;
			if (convertible == null)
			{
				return TypeCode.Object;
			}
			return convertible.GetTypeCode();
		}

		// Token: 0x06000875 RID: 2165 RVA: 0x0001FDDA File Offset: 0x0001DFDA
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return (bool)this;
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x0001FDE2 File Offset: 0x0001DFE2
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return (char)this;
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x0001FDEA File Offset: 0x0001DFEA
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return (sbyte)this;
		}

		// Token: 0x06000878 RID: 2168 RVA: 0x0001FDF2 File Offset: 0x0001DFF2
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return (byte)this;
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x0001FDFA File Offset: 0x0001DFFA
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return (short)this;
		}

		// Token: 0x0600087A RID: 2170 RVA: 0x0001FE02 File Offset: 0x0001E002
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return (ushort)this;
		}

		// Token: 0x0600087B RID: 2171 RVA: 0x0001FE0A File Offset: 0x0001E00A
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return (int)this;
		}

		// Token: 0x0600087C RID: 2172 RVA: 0x0001FE12 File Offset: 0x0001E012
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return (uint)this;
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x0001FE1A File Offset: 0x0001E01A
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return (long)this;
		}

		// Token: 0x0600087E RID: 2174 RVA: 0x0001FE22 File Offset: 0x0001E022
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return (ulong)this;
		}

		// Token: 0x0600087F RID: 2175 RVA: 0x0001FE2A File Offset: 0x0001E02A
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return (float)this;
		}

		// Token: 0x06000880 RID: 2176 RVA: 0x0001FE33 File Offset: 0x0001E033
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return (double)this;
		}

		// Token: 0x06000881 RID: 2177 RVA: 0x0001FE3C File Offset: 0x0001E03C
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return (decimal)this;
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x0001FE44 File Offset: 0x0001E044
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return (DateTime)this;
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x0001FE4C File Offset: 0x0001E04C
		object IConvertible.ToType(Type conversionType, IFormatProvider provider)
		{
			return base.ToObject(conversionType);
		}

		// Token: 0x040002C1 RID: 705
		private JTokenType _valueType;

		// Token: 0x040002C2 RID: 706
		private object _value;
	}
}
