﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200009C RID: 156
	[Preserve]
	public class JArray : JContainer, IList<JToken>, ICollection<JToken>, IEnumerable<JToken>, IEnumerable
	{
		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000747 RID: 1863 RVA: 0x0001C385 File Offset: 0x0001A585
		protected override IList<JToken> ChildrenTokens
		{
			get
			{
				return this._values;
			}
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x0001C38D File Offset: 0x0001A58D
		public override JTokenType Type
		{
			get
			{
				return JTokenType.Array;
			}
		}

		// Token: 0x06000749 RID: 1865 RVA: 0x0001C390 File Offset: 0x0001A590
		public JArray()
		{
		}

		// Token: 0x0600074A RID: 1866 RVA: 0x0001C3A3 File Offset: 0x0001A5A3
		public JArray(JArray other) : base(other)
		{
		}

		// Token: 0x0600074B RID: 1867 RVA: 0x0001C3B7 File Offset: 0x0001A5B7
		public JArray(params object[] content) : this(content)
		{
		}

		// Token: 0x0600074C RID: 1868 RVA: 0x0001C3C0 File Offset: 0x0001A5C0
		public JArray(object content)
		{
			this.Add(content);
		}

		// Token: 0x0600074D RID: 1869 RVA: 0x0001C3DC File Offset: 0x0001A5DC
		internal override bool DeepEquals(JToken node)
		{
			JArray jarray = node as JArray;
			return jarray != null && base.ContentsEqual(jarray);
		}

		// Token: 0x0600074E RID: 1870 RVA: 0x0001C3FC File Offset: 0x0001A5FC
		internal override JToken CloneToken()
		{
			return new JArray(this);
		}

		// Token: 0x0600074F RID: 1871 RVA: 0x0001C404 File Offset: 0x0001A604
		public new static JArray Load(JsonReader reader)
		{
			return JArray.Load(reader, null);
		}

		// Token: 0x06000750 RID: 1872 RVA: 0x0001C410 File Offset: 0x0001A610
		public new static JArray Load(JsonReader reader, JsonLoadSettings settings)
		{
			if (reader.TokenType == JsonToken.None && !reader.Read())
			{
				throw JsonReaderException.Create(reader, "Error reading JArray from JsonReader.");
			}
			reader.MoveToContent();
			if (reader.TokenType != JsonToken.StartArray)
			{
				throw JsonReaderException.Create(reader, "Error reading JArray from JsonReader. Current JsonReader item is not an array: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
			}
			JArray jarray = new JArray();
			jarray.SetLineInfo(reader as IJsonLineInfo, settings);
			jarray.ReadTokenFrom(reader, settings);
			return jarray;
		}

		// Token: 0x06000751 RID: 1873 RVA: 0x0001C484 File Offset: 0x0001A684
		public new static JArray Parse(string json)
		{
			return JArray.Parse(json, null);
		}

		// Token: 0x06000752 RID: 1874 RVA: 0x0001C490 File Offset: 0x0001A690
		public new static JArray Parse(string json, JsonLoadSettings settings)
		{
			JArray result;
			using (JsonReader jsonReader = new JsonTextReader(new StringReader(json)))
			{
				JArray jarray = JArray.Load(jsonReader, settings);
				if (jsonReader.Read() && jsonReader.TokenType != JsonToken.Comment)
				{
					throw JsonReaderException.Create(jsonReader, "Additional text found in JSON string after parsing content.");
				}
				result = jarray;
			}
			return result;
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x0001C4EC File Offset: 0x0001A6EC
		public new static JArray FromObject(object o)
		{
			return JArray.FromObject(o, JsonSerializer.CreateDefault());
		}

		// Token: 0x06000754 RID: 1876 RVA: 0x0001C4FC File Offset: 0x0001A6FC
		public new static JArray FromObject(object o, JsonSerializer jsonSerializer)
		{
			JToken jtoken = JToken.FromObjectInternal(o, jsonSerializer);
			if (jtoken.Type != JTokenType.Array)
			{
				throw new ArgumentException("Object serialized to {0}. JArray instance expected.".FormatWith(CultureInfo.InvariantCulture, jtoken.Type));
			}
			return (JArray)jtoken;
		}

		// Token: 0x06000755 RID: 1877 RVA: 0x0001C540 File Offset: 0x0001A740
		public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
		{
			writer.WriteStartArray();
			for (int i = 0; i < this._values.Count; i++)
			{
				this._values[i].WriteTo(writer, converters);
			}
			writer.WriteEndArray();
		}

		// Token: 0x17000172 RID: 370
		public override JToken this[object key]
		{
			get
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				if (!(key is int))
				{
					throw new ArgumentException("Accessed JArray values with invalid key value: {0}. Int32 array index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				return this.GetItem((int)key);
			}
			set
			{
				ValidationUtils.ArgumentNotNull(key, "key");
				if (!(key is int))
				{
					throw new ArgumentException("Set JArray values with invalid key value: {0}. Int32 array index expected.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.ToString(key)));
				}
				this.SetItem((int)key, value);
			}
		}

		// Token: 0x17000173 RID: 371
		public JToken this[int index]
		{
			get
			{
				return this.GetItem(index);
			}
			set
			{
				this.SetItem(index, value);
			}
		}

		// Token: 0x0600075A RID: 1882 RVA: 0x0001C5FB File Offset: 0x0001A7FB
		internal override int IndexOfItem(JToken item)
		{
			return this._values.IndexOfReference(item);
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x0001C60C File Offset: 0x0001A80C
		internal override void MergeItem(object content, JsonMergeSettings settings)
		{
			IEnumerable enumerable = (base.IsMultiContent(content) || content is JArray) ? ((IEnumerable)content) : null;
			if (enumerable == null)
			{
				return;
			}
			JContainer.MergeEnumerableContent(this, enumerable, settings);
		}

		// Token: 0x0600075C RID: 1884 RVA: 0x0001B5FF File Offset: 0x000197FF
		public int IndexOf(JToken item)
		{
			return this.IndexOfItem(item);
		}

		// Token: 0x0600075D RID: 1885 RVA: 0x0001B608 File Offset: 0x00019808
		public void Insert(int index, JToken item)
		{
			this.InsertItem(index, item, false);
		}

		// Token: 0x0600075E RID: 1886 RVA: 0x0001B613 File Offset: 0x00019813
		public void RemoveAt(int index)
		{
			this.RemoveItemAt(index);
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x0001C640 File Offset: 0x0001A840
		public IEnumerator<JToken> GetEnumerator()
		{
			return this.Children().GetEnumerator();
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x0001B62F File Offset: 0x0001982F
		public void Add(JToken item)
		{
			this.Add(item);
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x0001B2BA File Offset: 0x000194BA
		public void Clear()
		{
			this.ClearItems();
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x0001B638 File Offset: 0x00019838
		public bool Contains(JToken item)
		{
			return this.ContainsItem(item);
		}

		// Token: 0x06000763 RID: 1891 RVA: 0x0001B641 File Offset: 0x00019841
		public void CopyTo(JToken[] array, int arrayIndex)
		{
			this.CopyItemsTo(array, arrayIndex);
		}

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x06000764 RID: 1892 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000765 RID: 1893 RVA: 0x0001B64B File Offset: 0x0001984B
		public bool Remove(JToken item)
		{
			return this.RemoveItem(item);
		}

		// Token: 0x06000766 RID: 1894 RVA: 0x0001C2BC File Offset: 0x0001A4BC
		internal override int GetDeepHashCode()
		{
			return base.ContentsHashCode();
		}

		// Token: 0x04000295 RID: 661
		private readonly List<JToken> _values = new List<JToken>();
	}
}
