﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200008D RID: 141
	[Preserve]
	public enum LineInfoHandling
	{
		// Token: 0x0400027A RID: 634
		Ignore,
		// Token: 0x0400027B RID: 635
		Load
	}
}
