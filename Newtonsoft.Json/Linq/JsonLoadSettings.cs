﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000090 RID: 144
	[Preserve]
	public class JsonLoadSettings
	{
		// Token: 0x1700014A RID: 330
		// (get) Token: 0x0600066C RID: 1644 RVA: 0x0001A3F8 File Offset: 0x000185F8
		// (set) Token: 0x0600066D RID: 1645 RVA: 0x0001A400 File Offset: 0x00018600
		public CommentHandling CommentHandling
		{
			get
			{
				return this._commentHandling;
			}
			set
			{
				if (value < CommentHandling.Ignore || value > CommentHandling.Load)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._commentHandling = value;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x0600066E RID: 1646 RVA: 0x0001A41C File Offset: 0x0001861C
		// (set) Token: 0x0600066F RID: 1647 RVA: 0x0001A424 File Offset: 0x00018624
		public LineInfoHandling LineInfoHandling
		{
			get
			{
				return this._lineInfoHandling;
			}
			set
			{
				if (value < LineInfoHandling.Ignore || value > LineInfoHandling.Load)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._lineInfoHandling = value;
			}
		}

		// Token: 0x0400027E RID: 638
		private CommentHandling _commentHandling;

		// Token: 0x0400027F RID: 639
		private LineInfoHandling _lineInfoHandling;
	}
}
