﻿using System;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x0200009E RID: 158
	[Preserve]
	public class JTokenWriter : JsonWriter
	{
		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06000776 RID: 1910 RVA: 0x0001CB49 File Offset: 0x0001AD49
		public JToken CurrentToken
		{
			get
			{
				return this._current;
			}
		}

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x06000777 RID: 1911 RVA: 0x0001CB51 File Offset: 0x0001AD51
		public JToken Token
		{
			get
			{
				if (this._token != null)
				{
					return this._token;
				}
				return this._value;
			}
		}

		// Token: 0x06000778 RID: 1912 RVA: 0x0001CB68 File Offset: 0x0001AD68
		public JTokenWriter(JContainer container)
		{
			ValidationUtils.ArgumentNotNull(container, "container");
			this._token = container;
			this._parent = container;
		}

		// Token: 0x06000779 RID: 1913 RVA: 0x0001CB89 File Offset: 0x0001AD89
		public JTokenWriter()
		{
		}

		// Token: 0x0600077A RID: 1914 RVA: 0x0000982C File Offset: 0x00007A2C
		public override void Flush()
		{
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x0001CB91 File Offset: 0x0001AD91
		public override void Close()
		{
			base.Close();
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x0001CB99 File Offset: 0x0001AD99
		public override void WriteStartObject()
		{
			base.WriteStartObject();
			this.AddParent(new JObject());
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x0001CBAC File Offset: 0x0001ADAC
		private void AddParent(JContainer container)
		{
			if (this._parent == null)
			{
				this._token = container;
			}
			else
			{
				this._parent.AddAndSkipParentCheck(container);
			}
			this._parent = container;
			this._current = container;
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x0001CBDC File Offset: 0x0001ADDC
		private void RemoveParent()
		{
			this._current = this._parent;
			this._parent = this._parent.Parent;
			if (this._parent != null && this._parent.Type == JTokenType.Property)
			{
				this._parent = this._parent.Parent;
			}
		}

		// Token: 0x0600077F RID: 1919 RVA: 0x0001CC2D File Offset: 0x0001AE2D
		public override void WriteStartArray()
		{
			base.WriteStartArray();
			this.AddParent(new JArray());
		}

		// Token: 0x06000780 RID: 1920 RVA: 0x0001CC40 File Offset: 0x0001AE40
		public override void WriteStartConstructor(string name)
		{
			base.WriteStartConstructor(name);
			this.AddParent(new JConstructor(name));
		}

		// Token: 0x06000781 RID: 1921 RVA: 0x0001CC55 File Offset: 0x0001AE55
		protected override void WriteEnd(JsonToken token)
		{
			this.RemoveParent();
		}

		// Token: 0x06000782 RID: 1922 RVA: 0x0001CC60 File Offset: 0x0001AE60
		public override void WritePropertyName(string name)
		{
			JObject jobject = this._parent as JObject;
			if (jobject != null)
			{
				jobject.Remove(name);
			}
			this.AddParent(new JProperty(name));
			base.WritePropertyName(name);
		}

		// Token: 0x06000783 RID: 1923 RVA: 0x0001CC97 File Offset: 0x0001AE97
		private void AddValue(object value, JsonToken token)
		{
			this.AddValue(new JValue(value), token);
		}

		// Token: 0x06000784 RID: 1924 RVA: 0x0001CCA8 File Offset: 0x0001AEA8
		internal void AddValue(JValue value, JsonToken token)
		{
			if (this._parent != null)
			{
				this._parent.Add(value);
				this._current = this._parent.Last;
				if (this._parent.Type == JTokenType.Property)
				{
					this._parent = this._parent.Parent;
					return;
				}
			}
			else
			{
				this._value = (value ?? JValue.CreateNull());
				this._current = this._value;
			}
		}

		// Token: 0x06000785 RID: 1925 RVA: 0x000060E0 File Offset: 0x000042E0
		public override void WriteValue(object value)
		{
			base.WriteValue(value);
		}

		// Token: 0x06000786 RID: 1926 RVA: 0x0001CD16 File Offset: 0x0001AF16
		public override void WriteNull()
		{
			base.WriteNull();
			this.AddValue(null, JsonToken.Null);
		}

		// Token: 0x06000787 RID: 1927 RVA: 0x0001CD27 File Offset: 0x0001AF27
		public override void WriteUndefined()
		{
			base.WriteUndefined();
			this.AddValue(null, JsonToken.Undefined);
		}

		// Token: 0x06000788 RID: 1928 RVA: 0x0001CD38 File Offset: 0x0001AF38
		public override void WriteRaw(string json)
		{
			base.WriteRaw(json);
			this.AddValue(new JRaw(json), JsonToken.Raw);
		}

		// Token: 0x06000789 RID: 1929 RVA: 0x0001CD4E File Offset: 0x0001AF4E
		public override void WriteComment(string text)
		{
			base.WriteComment(text);
			this.AddValue(JValue.CreateComment(text), JsonToken.Comment);
		}

		// Token: 0x0600078A RID: 1930 RVA: 0x0001CD64 File Offset: 0x0001AF64
		public override void WriteValue(string value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.String);
		}

		// Token: 0x0600078B RID: 1931 RVA: 0x0001CD76 File Offset: 0x0001AF76
		public override void WriteValue(int value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x0600078C RID: 1932 RVA: 0x0001CD8C File Offset: 0x0001AF8C
		[CLSCompliant(false)]
		public override void WriteValue(uint value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x0600078D RID: 1933 RVA: 0x0001CDA2 File Offset: 0x0001AFA2
		public override void WriteValue(long value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x0600078E RID: 1934 RVA: 0x0001CDB8 File Offset: 0x0001AFB8
		[CLSCompliant(false)]
		public override void WriteValue(ulong value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x0600078F RID: 1935 RVA: 0x0001CDCE File Offset: 0x0001AFCE
		public override void WriteValue(float value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Float);
		}

		// Token: 0x06000790 RID: 1936 RVA: 0x0001CDE4 File Offset: 0x0001AFE4
		public override void WriteValue(double value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Float);
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x0001CDFA File Offset: 0x0001AFFA
		public override void WriteValue(bool value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Boolean);
		}

		// Token: 0x06000792 RID: 1938 RVA: 0x0001CE11 File Offset: 0x0001B011
		public override void WriteValue(short value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x0001CE27 File Offset: 0x0001B027
		[CLSCompliant(false)]
		public override void WriteValue(ushort value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x06000794 RID: 1940 RVA: 0x0001CE40 File Offset: 0x0001B040
		public override void WriteValue(char value)
		{
			base.WriteValue(value);
			string value2 = value.ToString(CultureInfo.InvariantCulture);
			this.AddValue(value2, JsonToken.String);
		}

		// Token: 0x06000795 RID: 1941 RVA: 0x0001CE6C File Offset: 0x0001B06C
		public override void WriteValue(byte value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x06000796 RID: 1942 RVA: 0x0001CE82 File Offset: 0x0001B082
		[CLSCompliant(false)]
		public override void WriteValue(sbyte value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Integer);
		}

		// Token: 0x06000797 RID: 1943 RVA: 0x0001CE98 File Offset: 0x0001B098
		public override void WriteValue(decimal value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Float);
		}

		// Token: 0x06000798 RID: 1944 RVA: 0x0001CEAE File Offset: 0x0001B0AE
		public override void WriteValue(DateTime value)
		{
			base.WriteValue(value);
			value = DateTimeUtils.EnsureDateTime(value, base.DateTimeZoneHandling);
			this.AddValue(value, JsonToken.Date);
		}

		// Token: 0x06000799 RID: 1945 RVA: 0x0001CED3 File Offset: 0x0001B0D3
		public override void WriteValue(DateTimeOffset value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Date);
		}

		// Token: 0x0600079A RID: 1946 RVA: 0x0001CEEA File Offset: 0x0001B0EA
		public override void WriteValue(byte[] value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.Bytes);
		}

		// Token: 0x0600079B RID: 1947 RVA: 0x0001CEFC File Offset: 0x0001B0FC
		public override void WriteValue(TimeSpan value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.String);
		}

		// Token: 0x0600079C RID: 1948 RVA: 0x0001CF13 File Offset: 0x0001B113
		public override void WriteValue(Guid value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.String);
		}

		// Token: 0x0600079D RID: 1949 RVA: 0x0001CF2A File Offset: 0x0001B12A
		public override void WriteValue(Uri value)
		{
			base.WriteValue(value);
			this.AddValue(value, JsonToken.String);
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x0001CF3C File Offset: 0x0001B13C
		internal override void WriteToken(JsonReader reader, bool writeChildren, bool writeDateConstructorAsDate, bool writeComments)
		{
			JTokenReader jtokenReader = reader as JTokenReader;
			if (jtokenReader == null || !writeChildren || !writeDateConstructorAsDate || !writeComments)
			{
				base.WriteToken(reader, writeChildren, writeDateConstructorAsDate, writeComments);
				return;
			}
			if (jtokenReader.TokenType == JsonToken.None && !jtokenReader.Read())
			{
				return;
			}
			JToken jtoken = jtokenReader.CurrentToken.CloneToken();
			if (this._parent != null)
			{
				this._parent.Add(jtoken);
				this._current = this._parent.Last;
				if (this._parent.Type == JTokenType.Property)
				{
					this._parent = this._parent.Parent;
					base.InternalWriteValue(JsonToken.Null);
				}
			}
			else
			{
				this._current = jtoken;
				if (this._token == null && this._value == null)
				{
					this._token = (jtoken as JContainer);
					this._value = (jtoken as JValue);
				}
			}
			jtokenReader.Skip();
		}

		// Token: 0x0400029A RID: 666
		private JContainer _token;

		// Token: 0x0400029B RID: 667
		private JContainer _parent;

		// Token: 0x0400029C RID: 668
		private JValue _value;

		// Token: 0x0400029D RID: 669
		private JToken _current;
	}
}
