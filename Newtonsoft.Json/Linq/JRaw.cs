﻿using System;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x02000094 RID: 148
	[Preserve]
	public class JRaw : JValue
	{
		// Token: 0x06000676 RID: 1654 RVA: 0x0001A488 File Offset: 0x00018688
		public JRaw(JRaw other) : base(other)
		{
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x0001A491 File Offset: 0x00018691
		public JRaw(object rawJson) : base(rawJson, JTokenType.Raw)
		{
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x0001A49C File Offset: 0x0001869C
		public static JRaw Create(JsonReader reader)
		{
			JRaw result;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				using (JsonTextWriter jsonTextWriter = new JsonTextWriter(stringWriter))
				{
					jsonTextWriter.WriteToken(reader);
					result = new JRaw(stringWriter.ToString());
				}
			}
			return result;
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x0001A504 File Offset: 0x00018704
		internal override JToken CloneToken()
		{
			return new JRaw(this);
		}
	}
}
