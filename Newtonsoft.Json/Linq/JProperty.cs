﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x020000A0 RID: 160
	[Preserve]
	public class JProperty : JContainer
	{
		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000832 RID: 2098 RVA: 0x0001F046 File Offset: 0x0001D246
		protected override IList<JToken> ChildrenTokens
		{
			get
			{
				return this._content;
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000833 RID: 2099 RVA: 0x0001F04E File Offset: 0x0001D24E
		public string Name
		{
			[DebuggerStepThrough]
			get
			{
				return this._name;
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000834 RID: 2100 RVA: 0x0001F056 File Offset: 0x0001D256
		// (set) Token: 0x06000835 RID: 2101 RVA: 0x0001F064 File Offset: 0x0001D264
		public new JToken Value
		{
			[DebuggerStepThrough]
			get
			{
				return this._content._token;
			}
			set
			{
				base.CheckReentrancy();
				JToken item = value ?? JValue.CreateNull();
				if (this._content._token == null)
				{
					this.InsertItem(0, item, false);
					return;
				}
				this.SetItem(0, item);
			}
		}

		// Token: 0x06000836 RID: 2102 RVA: 0x0001F0A1 File Offset: 0x0001D2A1
		public JProperty(JProperty other) : base(other)
		{
			this._name = other.Name;
		}

		// Token: 0x06000837 RID: 2103 RVA: 0x0001F0C1 File Offset: 0x0001D2C1
		internal override JToken GetItem(int index)
		{
			if (index != 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			return this.Value;
		}

		// Token: 0x06000838 RID: 2104 RVA: 0x0001F0D4 File Offset: 0x0001D2D4
		internal override void SetItem(int index, JToken item)
		{
			if (index != 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (JContainer.IsTokenUnchanged(this.Value, item))
			{
				return;
			}
			if (base.Parent != null)
			{
				((JObject)base.Parent).InternalPropertyChanging(this);
			}
			base.SetItem(0, item);
			if (base.Parent != null)
			{
				((JObject)base.Parent).InternalPropertyChanged(this);
			}
		}

		// Token: 0x06000839 RID: 2105 RVA: 0x0001F133 File Offset: 0x0001D333
		internal override bool RemoveItem(JToken item)
		{
			throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
		}

		// Token: 0x0600083A RID: 2106 RVA: 0x0001F133 File Offset: 0x0001D333
		internal override void RemoveItemAt(int index)
		{
			throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
		}

		// Token: 0x0600083B RID: 2107 RVA: 0x0001F153 File Offset: 0x0001D353
		internal override int IndexOfItem(JToken item)
		{
			return this._content.IndexOf(item);
		}

		// Token: 0x0600083C RID: 2108 RVA: 0x0001F161 File Offset: 0x0001D361
		internal override void InsertItem(int index, JToken item, bool skipParentCheck)
		{
			if (item != null && item.Type == JTokenType.Comment)
			{
				return;
			}
			if (this.Value != null)
			{
				throw new JsonException("{0} cannot have multiple values.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
			}
			base.InsertItem(0, item, false);
		}

		// Token: 0x0600083D RID: 2109 RVA: 0x0001F1A0 File Offset: 0x0001D3A0
		internal override bool ContainsItem(JToken item)
		{
			return this.Value == item;
		}

		// Token: 0x0600083E RID: 2110 RVA: 0x0001F1AC File Offset: 0x0001D3AC
		internal override void MergeItem(object content, JsonMergeSettings settings)
		{
			JProperty jproperty = content as JProperty;
			if (jproperty == null)
			{
				return;
			}
			if (jproperty.Value != null && jproperty.Value.Type != JTokenType.Null)
			{
				this.Value = jproperty.Value;
			}
		}

		// Token: 0x0600083F RID: 2111 RVA: 0x0001F133 File Offset: 0x0001D333
		internal override void ClearItems()
		{
			throw new JsonException("Cannot add or remove items from {0}.".FormatWith(CultureInfo.InvariantCulture, typeof(JProperty)));
		}

		// Token: 0x06000840 RID: 2112 RVA: 0x0001F1E8 File Offset: 0x0001D3E8
		internal override bool DeepEquals(JToken node)
		{
			JProperty jproperty = node as JProperty;
			return jproperty != null && this._name == jproperty.Name && base.ContentsEqual(jproperty);
		}

		// Token: 0x06000841 RID: 2113 RVA: 0x0001F21B File Offset: 0x0001D41B
		internal override JToken CloneToken()
		{
			return new JProperty(this);
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x0001F223 File Offset: 0x0001D423
		public override JTokenType Type
		{
			[DebuggerStepThrough]
			get
			{
				return JTokenType.Property;
			}
		}

		// Token: 0x06000843 RID: 2115 RVA: 0x0001F226 File Offset: 0x0001D426
		internal JProperty(string name)
		{
			ValidationUtils.ArgumentNotNull(name, "name");
			this._name = name;
		}

		// Token: 0x06000844 RID: 2116 RVA: 0x0001F24B File Offset: 0x0001D44B
		public JProperty(string name, params object[] content) : this(name, content)
		{
		}

		// Token: 0x06000845 RID: 2117 RVA: 0x0001F258 File Offset: 0x0001D458
		public JProperty(string name, object content)
		{
			ValidationUtils.ArgumentNotNull(name, "name");
			this._name = name;
			this.Value = (base.IsMultiContent(content) ? new JArray(content) : JContainer.CreateFromContent(content));
		}

		// Token: 0x06000846 RID: 2118 RVA: 0x0001F2A8 File Offset: 0x0001D4A8
		public override void WriteTo(JsonWriter writer, params JsonConverter[] converters)
		{
			writer.WritePropertyName(this._name);
			JToken value = this.Value;
			if (value != null)
			{
				value.WriteTo(writer, converters);
				return;
			}
			writer.WriteNull();
		}

		// Token: 0x06000847 RID: 2119 RVA: 0x0001F2DA File Offset: 0x0001D4DA
		internal override int GetDeepHashCode()
		{
			return this._name.GetHashCode() ^ ((this.Value != null) ? this.Value.GetDeepHashCode() : 0);
		}

		// Token: 0x06000848 RID: 2120 RVA: 0x0001F2FE File Offset: 0x0001D4FE
		public new static JProperty Load(JsonReader reader)
		{
			return JProperty.Load(reader, null);
		}

		// Token: 0x06000849 RID: 2121 RVA: 0x0001F308 File Offset: 0x0001D508
		public new static JProperty Load(JsonReader reader, JsonLoadSettings settings)
		{
			if (reader.TokenType == JsonToken.None && !reader.Read())
			{
				throw JsonReaderException.Create(reader, "Error reading JProperty from JsonReader.");
			}
			reader.MoveToContent();
			if (reader.TokenType != JsonToken.PropertyName)
			{
				throw JsonReaderException.Create(reader, "Error reading JProperty from JsonReader. Current JsonReader item is not a property: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
			}
			JProperty jproperty = new JProperty((string)reader.Value);
			jproperty.SetLineInfo(reader as IJsonLineInfo, settings);
			jproperty.ReadTokenFrom(reader, settings);
			return jproperty;
		}

		// Token: 0x040002AC RID: 684
		private readonly JProperty.JPropertyList _content = new JProperty.JPropertyList();

		// Token: 0x040002AD RID: 685
		private readonly string _name;

		// Token: 0x02000121 RID: 289
		private class JPropertyList : IList<JToken>, ICollection<JToken>, IEnumerable<JToken>, IEnumerable
		{
			// Token: 0x06000B4C RID: 2892 RVA: 0x00028660 File Offset: 0x00026860
			public IEnumerator<JToken> GetEnumerator()
			{
				if (this._token != null)
				{
					yield return this._token;
				}
				yield break;
			}

			// Token: 0x06000B4D RID: 2893 RVA: 0x0002866F File Offset: 0x0002686F
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06000B4E RID: 2894 RVA: 0x00028677 File Offset: 0x00026877
			public void Add(JToken item)
			{
				this._token = item;
			}

			// Token: 0x06000B4F RID: 2895 RVA: 0x00028680 File Offset: 0x00026880
			public void Clear()
			{
				this._token = null;
			}

			// Token: 0x06000B50 RID: 2896 RVA: 0x00028689 File Offset: 0x00026889
			public bool Contains(JToken item)
			{
				return this._token == item;
			}

			// Token: 0x06000B51 RID: 2897 RVA: 0x00028694 File Offset: 0x00026894
			public void CopyTo(JToken[] array, int arrayIndex)
			{
				if (this._token != null)
				{
					array[arrayIndex] = this._token;
				}
			}

			// Token: 0x06000B52 RID: 2898 RVA: 0x000286A7 File Offset: 0x000268A7
			public bool Remove(JToken item)
			{
				if (this._token == item)
				{
					this._token = null;
					return true;
				}
				return false;
			}

			// Token: 0x1700022C RID: 556
			// (get) Token: 0x06000B53 RID: 2899 RVA: 0x000286BC File Offset: 0x000268BC
			public int Count
			{
				get
				{
					if (this._token == null)
					{
						return 0;
					}
					return 1;
				}
			}

			// Token: 0x1700022D RID: 557
			// (get) Token: 0x06000B54 RID: 2900 RVA: 0x0000CB98 File Offset: 0x0000AD98
			public bool IsReadOnly
			{
				get
				{
					return false;
				}
			}

			// Token: 0x06000B55 RID: 2901 RVA: 0x000286C9 File Offset: 0x000268C9
			public int IndexOf(JToken item)
			{
				if (this._token != item)
				{
					return -1;
				}
				return 0;
			}

			// Token: 0x06000B56 RID: 2902 RVA: 0x000286D7 File Offset: 0x000268D7
			public void Insert(int index, JToken item)
			{
				if (index == 0)
				{
					this._token = item;
				}
			}

			// Token: 0x06000B57 RID: 2903 RVA: 0x000286E3 File Offset: 0x000268E3
			public void RemoveAt(int index)
			{
				if (index == 0)
				{
					this._token = null;
				}
			}

			// Token: 0x1700022E RID: 558
			public JToken this[int index]
			{
				get
				{
					if (index != 0)
					{
						return null;
					}
					return this._token;
				}
				set
				{
					if (index == 0)
					{
						this._token = value;
					}
				}
			}

			// Token: 0x0400040D RID: 1037
			internal JToken _token;
		}
	}
}
