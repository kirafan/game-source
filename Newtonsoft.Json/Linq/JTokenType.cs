﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Linq
{
	// Token: 0x020000A1 RID: 161
	[Preserve]
	public enum JTokenType
	{
		// Token: 0x040002AF RID: 687
		None,
		// Token: 0x040002B0 RID: 688
		Object,
		// Token: 0x040002B1 RID: 689
		Array,
		// Token: 0x040002B2 RID: 690
		Constructor,
		// Token: 0x040002B3 RID: 691
		Property,
		// Token: 0x040002B4 RID: 692
		Comment,
		// Token: 0x040002B5 RID: 693
		Integer,
		// Token: 0x040002B6 RID: 694
		Float,
		// Token: 0x040002B7 RID: 695
		String,
		// Token: 0x040002B8 RID: 696
		Boolean,
		// Token: 0x040002B9 RID: 697
		Null,
		// Token: 0x040002BA RID: 698
		Undefined,
		// Token: 0x040002BB RID: 699
		Date,
		// Token: 0x040002BC RID: 700
		Raw,
		// Token: 0x040002BD RID: 701
		Bytes,
		// Token: 0x040002BE RID: 702
		Guid,
		// Token: 0x040002BF RID: 703
		Uri,
		// Token: 0x040002C0 RID: 704
		TimeSpan
	}
}
