﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000013 RID: 19
	[AttributeUsage(AttributeTargets.Constructor, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonConstructorAttribute : Attribute
	{
	}
}
