﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000027 RID: 39
	[Preserve]
	internal enum ReadType
	{
		// Token: 0x04000096 RID: 150
		Read,
		// Token: 0x04000097 RID: 151
		ReadAsInt32,
		// Token: 0x04000098 RID: 152
		ReadAsBytes,
		// Token: 0x04000099 RID: 153
		ReadAsString,
		// Token: 0x0400009A RID: 154
		ReadAsDecimal,
		// Token: 0x0400009B RID: 155
		ReadAsDateTime,
		// Token: 0x0400009C RID: 156
		ReadAsDateTimeOffset,
		// Token: 0x0400009D RID: 157
		ReadAsDouble,
		// Token: 0x0400009E RID: 158
		ReadAsBoolean
	}
}
