﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200002E RID: 46
	[Preserve]
	public abstract class JsonConverter
	{
		// Token: 0x06000157 RID: 343
		public abstract void WriteJson(JsonWriter writer, object value, JsonSerializer serializer);

		// Token: 0x06000158 RID: 344
		public abstract object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer);

		// Token: 0x06000159 RID: 345
		public abstract bool CanConvert(Type objectType);

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600015A RID: 346 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public virtual bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public virtual bool CanWrite
		{
			get
			{
				return true;
			}
		}
	}
}
