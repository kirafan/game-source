﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000036 RID: 54
	[Preserve]
	public enum ReferenceLoopHandling
	{
		// Token: 0x04000106 RID: 262
		Error,
		// Token: 0x04000107 RID: 263
		Ignore,
		// Token: 0x04000108 RID: 264
		Serialize
	}
}
