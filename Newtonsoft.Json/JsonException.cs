﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000015 RID: 21
	[Preserve]
	[Serializable]
	public class JsonException : Exception
	{
		// Token: 0x06000032 RID: 50 RVA: 0x000021DB File Offset: 0x000003DB
		public JsonException()
		{
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000021E3 File Offset: 0x000003E3
		public JsonException(string message) : base(message)
		{
		}

		// Token: 0x06000034 RID: 52 RVA: 0x000021EC File Offset: 0x000003EC
		public JsonException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000035 RID: 53 RVA: 0x000021F6 File Offset: 0x000003F6
		public JsonException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002200 File Offset: 0x00000400
		internal static JsonException Create(IJsonLineInfo lineInfo, string path, string message)
		{
			message = JsonPosition.FormatMessage(lineInfo, path, message);
			return new JsonException(message);
		}
	}
}
