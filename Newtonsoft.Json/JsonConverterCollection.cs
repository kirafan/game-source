﻿using System;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x0200002F RID: 47
	[Preserve]
	public class JsonConverterCollection : Collection<JsonConverter>
	{
	}
}
