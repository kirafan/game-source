﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000032 RID: 50
	[Preserve]
	[Serializable]
	public class JsonSerializationException : JsonException
	{
		// Token: 0x060001E5 RID: 485 RVA: 0x000066D2 File Offset: 0x000048D2
		public JsonSerializationException()
		{
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x000066DA File Offset: 0x000048DA
		public JsonSerializationException(string message) : base(message)
		{
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x000066E3 File Offset: 0x000048E3
		public JsonSerializationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x000066ED File Offset: 0x000048ED
		public JsonSerializationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x000082E0 File Offset: 0x000064E0
		internal static JsonSerializationException Create(JsonReader reader, string message)
		{
			return JsonSerializationException.Create(reader, message, null);
		}

		// Token: 0x060001EA RID: 490 RVA: 0x000082EA File Offset: 0x000064EA
		internal static JsonSerializationException Create(JsonReader reader, string message, Exception ex)
		{
			return JsonSerializationException.Create(reader as IJsonLineInfo, reader.Path, message, ex);
		}

		// Token: 0x060001EB RID: 491 RVA: 0x000082FF File Offset: 0x000064FF
		internal static JsonSerializationException Create(IJsonLineInfo lineInfo, string path, string message, Exception ex)
		{
			message = JsonPosition.FormatMessage(lineInfo, path, message);
			return new JsonSerializationException(message, ex);
		}
	}
}
