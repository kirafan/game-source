﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000014 RID: 20
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonDictionaryAttribute : JsonContainerAttribute
	{
		// Token: 0x06000030 RID: 48 RVA: 0x000021CA File Offset: 0x000003CA
		public JsonDictionaryAttribute()
		{
		}

		// Token: 0x06000031 RID: 49 RVA: 0x000021D2 File Offset: 0x000003D2
		public JsonDictionaryAttribute(string id) : base(id)
		{
		}
	}
}
