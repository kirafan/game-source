﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000020 RID: 32
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = false)]
	[Preserve]
	public abstract class JsonContainerAttribute : Attribute
	{
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002509 File Offset: 0x00000709
		// (set) Token: 0x0600004D RID: 77 RVA: 0x00002511 File Offset: 0x00000711
		public string Id { get; set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600004E RID: 78 RVA: 0x0000251A File Offset: 0x0000071A
		// (set) Token: 0x0600004F RID: 79 RVA: 0x00002522 File Offset: 0x00000722
		public string Title { get; set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000050 RID: 80 RVA: 0x0000252B File Offset: 0x0000072B
		// (set) Token: 0x06000051 RID: 81 RVA: 0x00002533 File Offset: 0x00000733
		public string Description { get; set; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000052 RID: 82 RVA: 0x0000253C File Offset: 0x0000073C
		// (set) Token: 0x06000053 RID: 83 RVA: 0x00002544 File Offset: 0x00000744
		public Type ItemConverterType { get; set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000054 RID: 84 RVA: 0x0000254D File Offset: 0x0000074D
		// (set) Token: 0x06000055 RID: 85 RVA: 0x00002555 File Offset: 0x00000755
		public object[] ItemConverterParameters { get; set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000056 RID: 86 RVA: 0x00002560 File Offset: 0x00000760
		// (set) Token: 0x06000057 RID: 87 RVA: 0x00002586 File Offset: 0x00000786
		public bool IsReference
		{
			get
			{
				return this._isReference ?? false;
			}
			set
			{
				this._isReference = new bool?(value);
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000058 RID: 88 RVA: 0x00002594 File Offset: 0x00000794
		// (set) Token: 0x06000059 RID: 89 RVA: 0x000025BA File Offset: 0x000007BA
		public bool ItemIsReference
		{
			get
			{
				return this._itemIsReference ?? false;
			}
			set
			{
				this._itemIsReference = new bool?(value);
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600005A RID: 90 RVA: 0x000025C8 File Offset: 0x000007C8
		// (set) Token: 0x0600005B RID: 91 RVA: 0x000025EE File Offset: 0x000007EE
		public ReferenceLoopHandling ItemReferenceLoopHandling
		{
			get
			{
				ReferenceLoopHandling? itemReferenceLoopHandling = this._itemReferenceLoopHandling;
				if (itemReferenceLoopHandling == null)
				{
					return ReferenceLoopHandling.Error;
				}
				return itemReferenceLoopHandling.GetValueOrDefault();
			}
			set
			{
				this._itemReferenceLoopHandling = new ReferenceLoopHandling?(value);
			}
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x0600005C RID: 92 RVA: 0x000025FC File Offset: 0x000007FC
		// (set) Token: 0x0600005D RID: 93 RVA: 0x00002622 File Offset: 0x00000822
		public TypeNameHandling ItemTypeNameHandling
		{
			get
			{
				TypeNameHandling? itemTypeNameHandling = this._itemTypeNameHandling;
				if (itemTypeNameHandling == null)
				{
					return TypeNameHandling.None;
				}
				return itemTypeNameHandling.GetValueOrDefault();
			}
			set
			{
				this._itemTypeNameHandling = new TypeNameHandling?(value);
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x000021C2 File Offset: 0x000003C2
		protected JsonContainerAttribute()
		{
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002630 File Offset: 0x00000830
		protected JsonContainerAttribute(string id)
		{
			this.Id = id;
		}

		// Token: 0x0400004B RID: 75
		internal bool? _isReference;

		// Token: 0x0400004C RID: 76
		internal bool? _itemIsReference;

		// Token: 0x0400004D RID: 77
		internal ReferenceLoopHandling? _itemReferenceLoopHandling;

		// Token: 0x0400004E RID: 78
		internal TypeNameHandling? _itemTypeNameHandling;
	}
}
