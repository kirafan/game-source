﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json
{
	// Token: 0x02000030 RID: 48
	[Preserve]
	public abstract class JsonReader : IDisposable
	{
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600015E RID: 350 RVA: 0x000067E8 File Offset: 0x000049E8
		protected JsonReader.State CurrentState
		{
			get
			{
				return this._currentState;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600015F RID: 351 RVA: 0x000067F0 File Offset: 0x000049F0
		// (set) Token: 0x06000160 RID: 352 RVA: 0x000067F8 File Offset: 0x000049F8
		public bool CloseInput { get; set; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000161 RID: 353 RVA: 0x00006801 File Offset: 0x00004A01
		// (set) Token: 0x06000162 RID: 354 RVA: 0x00006809 File Offset: 0x00004A09
		public bool SupportMultipleContent { get; set; }

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00006812 File Offset: 0x00004A12
		// (set) Token: 0x06000164 RID: 356 RVA: 0x0000681A File Offset: 0x00004A1A
		public virtual char QuoteChar
		{
			get
			{
				return this._quoteChar;
			}
			protected internal set
			{
				this._quoteChar = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00006823 File Offset: 0x00004A23
		// (set) Token: 0x06000166 RID: 358 RVA: 0x0000682B File Offset: 0x00004A2B
		public DateTimeZoneHandling DateTimeZoneHandling
		{
			get
			{
				return this._dateTimeZoneHandling;
			}
			set
			{
				if (value < DateTimeZoneHandling.Local || value > DateTimeZoneHandling.RoundtripKind)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._dateTimeZoneHandling = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00006847 File Offset: 0x00004A47
		// (set) Token: 0x06000168 RID: 360 RVA: 0x0000684F File Offset: 0x00004A4F
		public DateParseHandling DateParseHandling
		{
			get
			{
				return this._dateParseHandling;
			}
			set
			{
				if (value < DateParseHandling.None || value > DateParseHandling.DateTimeOffset)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._dateParseHandling = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000169 RID: 361 RVA: 0x0000686B File Offset: 0x00004A6B
		// (set) Token: 0x0600016A RID: 362 RVA: 0x00006873 File Offset: 0x00004A73
		public FloatParseHandling FloatParseHandling
		{
			get
			{
				return this._floatParseHandling;
			}
			set
			{
				if (value < FloatParseHandling.Double || value > FloatParseHandling.Decimal)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this._floatParseHandling = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600016B RID: 363 RVA: 0x0000688F File Offset: 0x00004A8F
		// (set) Token: 0x0600016C RID: 364 RVA: 0x00006897 File Offset: 0x00004A97
		public string DateFormatString
		{
			get
			{
				return this._dateFormatString;
			}
			set
			{
				this._dateFormatString = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600016D RID: 365 RVA: 0x000068A0 File Offset: 0x00004AA0
		// (set) Token: 0x0600016E RID: 366 RVA: 0x000068A8 File Offset: 0x00004AA8
		public int? MaxDepth
		{
			get
			{
				return this._maxDepth;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentException("Value must be positive.", "value");
				}
				this._maxDepth = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600016F RID: 367 RVA: 0x000068E6 File Offset: 0x00004AE6
		public virtual JsonToken TokenType
		{
			get
			{
				return this._tokenType;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000170 RID: 368 RVA: 0x000068EE File Offset: 0x00004AEE
		public virtual object Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000171 RID: 369 RVA: 0x000068F6 File Offset: 0x00004AF6
		public virtual Type ValueType
		{
			get
			{
				if (this._value == null)
				{
					return null;
				}
				return this._value.GetType();
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000172 RID: 370 RVA: 0x00006910 File Offset: 0x00004B10
		public virtual int Depth
		{
			get
			{
				int num = (this._stack != null) ? this._stack.Count : 0;
				if (JsonTokenUtils.IsStartToken(this.TokenType) || this._currentPosition.Type == JsonContainerType.None)
				{
					return num;
				}
				return num + 1;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00006954 File Offset: 0x00004B54
		public virtual string Path
		{
			get
			{
				if (this._currentPosition.Type == JsonContainerType.None)
				{
					return string.Empty;
				}
				JsonPosition? currentPosition = (this._currentState != JsonReader.State.ArrayStart && this._currentState != JsonReader.State.ConstructorStart && this._currentState != JsonReader.State.ObjectStart) ? new JsonPosition?(this._currentPosition) : null;
				return JsonPosition.BuildPath(this._stack, currentPosition);
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000174 RID: 372 RVA: 0x000069BB File Offset: 0x00004BBB
		// (set) Token: 0x06000175 RID: 373 RVA: 0x000069CC File Offset: 0x00004BCC
		public CultureInfo Culture
		{
			get
			{
				return this._culture ?? CultureInfo.InvariantCulture;
			}
			set
			{
				this._culture = value;
			}
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000069D5 File Offset: 0x00004BD5
		internal JsonPosition GetPosition(int depth)
		{
			if (this._stack != null && depth < this._stack.Count)
			{
				return this._stack[depth];
			}
			return this._currentPosition;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00006A00 File Offset: 0x00004C00
		protected JsonReader()
		{
			this._currentState = JsonReader.State.Start;
			this._dateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
			this._dateParseHandling = DateParseHandling.DateTime;
			this._floatParseHandling = FloatParseHandling.Double;
			this.CloseInput = true;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00006A2C File Offset: 0x00004C2C
		private void Push(JsonContainerType value)
		{
			this.UpdateScopeWithFinishedValue();
			if (this._currentPosition.Type == JsonContainerType.None)
			{
				this._currentPosition = new JsonPosition(value);
				return;
			}
			if (this._stack == null)
			{
				this._stack = new List<JsonPosition>();
			}
			this._stack.Add(this._currentPosition);
			this._currentPosition = new JsonPosition(value);
			if (this._maxDepth != null && this.Depth + 1 > this._maxDepth && !this._hasExceededMaxDepth)
			{
				this._hasExceededMaxDepth = true;
				throw JsonReaderException.Create(this, "The reader's MaxDepth of {0} has been exceeded.".FormatWith(CultureInfo.InvariantCulture, this._maxDepth));
			}
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00006AEC File Offset: 0x00004CEC
		private JsonContainerType Pop()
		{
			JsonPosition currentPosition;
			if (this._stack != null && this._stack.Count > 0)
			{
				currentPosition = this._currentPosition;
				this._currentPosition = this._stack[this._stack.Count - 1];
				this._stack.RemoveAt(this._stack.Count - 1);
			}
			else
			{
				currentPosition = this._currentPosition;
				this._currentPosition = default(JsonPosition);
			}
			if (this._maxDepth != null && this.Depth <= this._maxDepth)
			{
				this._hasExceededMaxDepth = false;
			}
			return currentPosition.Type;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00006B9D File Offset: 0x00004D9D
		private JsonContainerType Peek()
		{
			return this._currentPosition.Type;
		}

		// Token: 0x0600017B RID: 379
		public abstract bool Read();

		// Token: 0x0600017C RID: 380 RVA: 0x00006BAC File Offset: 0x00004DAC
		public virtual int? ReadAsInt32()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken != JsonToken.None)
			{
				switch (contentToken)
				{
				case JsonToken.Integer:
				case JsonToken.Float:
					if (!(this.Value is int))
					{
						this.SetToken(JsonToken.Integer, Convert.ToInt32(this.Value, CultureInfo.InvariantCulture), false);
					}
					return new int?((int)this.Value);
				case JsonToken.String:
				{
					string s = (string)this.Value;
					return this.ReadInt32String(s);
				}
				case JsonToken.Null:
				case JsonToken.EndArray:
					goto IL_34;
				}
				throw JsonReaderException.Create(this, "Error reading integer. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			IL_34:
			return null;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00006C64 File Offset: 0x00004E64
		internal int? ReadInt32String(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			int num;
			if (int.TryParse(s, NumberStyles.Integer, this.Culture, out num))
			{
				this.SetToken(JsonToken.Integer, num, false);
				return new int?(num);
			}
			this.SetToken(JsonToken.String, s, false);
			throw JsonReaderException.Create(this, "Could not convert string to integer: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00006CD4 File Offset: 0x00004ED4
		public virtual string ReadAsString()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken <= JsonToken.String)
			{
				if (contentToken != JsonToken.None)
				{
					if (contentToken != JsonToken.String)
					{
						goto IL_2E;
					}
					return (string)this.Value;
				}
			}
			else if (contentToken != JsonToken.Null && contentToken != JsonToken.EndArray)
			{
				goto IL_2E;
			}
			return null;
			IL_2E:
			if (JsonTokenUtils.IsPrimitiveToken(contentToken) && this.Value != null)
			{
				string text;
				if (this.Value is IFormattable)
				{
					text = ((IFormattable)this.Value).ToString(null, this.Culture);
				}
				else if (this.Value is Uri)
				{
					text = ((Uri)this.Value).OriginalString;
				}
				else
				{
					text = this.Value.ToString();
				}
				this.SetToken(JsonToken.String, text, false);
				return text;
			}
			throw JsonReaderException.Create(this, "Error reading string. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00006D9C File Offset: 0x00004F9C
		public virtual byte[] ReadAsBytes()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken == JsonToken.None)
			{
				return null;
			}
			if (this.TokenType != JsonToken.StartObject)
			{
				if (contentToken <= JsonToken.String)
				{
					if (contentToken == JsonToken.StartArray)
					{
						return this.ReadArrayIntoByteArray();
					}
					if (contentToken == JsonToken.String)
					{
						string text = (string)this.Value;
						byte[] array;
						Guid guid;
						if (text.Length == 0)
						{
							array = new byte[0];
						}
						else if (ConvertUtils.TryConvertGuid(text, out guid))
						{
							array = guid.ToByteArray();
						}
						else
						{
							array = Convert.FromBase64String(text);
						}
						this.SetToken(JsonToken.Bytes, array, false);
						return array;
					}
				}
				else
				{
					if (contentToken == JsonToken.Null || contentToken == JsonToken.EndArray)
					{
						return null;
					}
					if (contentToken == JsonToken.Bytes)
					{
						if (this.ValueType == typeof(Guid))
						{
							byte[] array2 = ((Guid)this.Value).ToByteArray();
							this.SetToken(JsonToken.Bytes, array2, false);
							return array2;
						}
						return (byte[])this.Value;
					}
				}
				throw JsonReaderException.Create(this, "Error reading bytes. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			this.ReadIntoWrappedTypeObject();
			byte[] array3 = this.ReadAsBytes();
			this.ReaderReadAndAssert();
			if (this.TokenType != JsonToken.EndObject)
			{
				throw JsonReaderException.Create(this, "Error reading bytes. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
			}
			this.SetToken(JsonToken.Bytes, array3, false);
			return array3;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00006EDC File Offset: 0x000050DC
		internal byte[] ReadArrayIntoByteArray()
		{
			List<byte> list = new List<byte>();
			JsonToken contentToken;
			for (;;)
			{
				contentToken = this.GetContentToken();
				if (contentToken == JsonToken.None)
				{
					goto IL_1B;
				}
				if (contentToken != JsonToken.Integer)
				{
					break;
				}
				list.Add(Convert.ToByte(this.Value, CultureInfo.InvariantCulture));
			}
			if (contentToken != JsonToken.EndArray)
			{
				throw JsonReaderException.Create(this, "Unexpected token when reading bytes: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			byte[] array = list.ToArray();
			this.SetToken(JsonToken.Bytes, array, false);
			return array;
			IL_1B:
			throw JsonReaderException.Create(this, "Unexpected end when reading bytes.");
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00006F58 File Offset: 0x00005158
		public virtual double? ReadAsDouble()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken != JsonToken.None)
			{
				switch (contentToken)
				{
				case JsonToken.Integer:
				case JsonToken.Float:
					if (!(this.Value is double))
					{
						double num = Convert.ToDouble(this.Value, CultureInfo.InvariantCulture);
						this.SetToken(JsonToken.Float, num, false);
					}
					return new double?((double)this.Value);
				case JsonToken.String:
					return this.ReadDoubleString((string)this.Value);
				case JsonToken.Null:
				case JsonToken.EndArray:
					goto IL_34;
				}
				throw JsonReaderException.Create(this, "Error reading double. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			IL_34:
			return null;
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00007010 File Offset: 0x00005210
		internal double? ReadDoubleString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			double num;
			if (double.TryParse(s, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, this.Culture, out num))
			{
				this.SetToken(JsonToken.Float, num, false);
				return new double?(num);
			}
			this.SetToken(JsonToken.String, s, false);
			throw JsonReaderException.Create(this, "Could not convert string to double: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00007084 File Offset: 0x00005284
		public virtual bool? ReadAsBoolean()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken != JsonToken.None)
			{
				switch (contentToken)
				{
				case JsonToken.Integer:
				case JsonToken.Float:
				{
					bool flag = Convert.ToBoolean(this.Value, CultureInfo.InvariantCulture);
					this.SetToken(JsonToken.Boolean, flag, false);
					return new bool?(flag);
				}
				case JsonToken.String:
					return this.ReadBooleanString((string)this.Value);
				case JsonToken.Boolean:
					return new bool?((bool)this.Value);
				case JsonToken.Null:
				case JsonToken.EndArray:
					goto IL_34;
				}
				throw JsonReaderException.Create(this, "Error reading boolean. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			IL_34:
			return null;
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00007134 File Offset: 0x00005334
		internal bool? ReadBooleanString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			bool flag;
			if (bool.TryParse(s, out flag))
			{
				this.SetToken(JsonToken.Boolean, flag, false);
				return new bool?(flag);
			}
			this.SetToken(JsonToken.String, s, false);
			throw JsonReaderException.Create(this, "Could not convert string to boolean: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x06000185 RID: 389 RVA: 0x000071A0 File Offset: 0x000053A0
		public virtual decimal? ReadAsDecimal()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken != JsonToken.None)
			{
				switch (contentToken)
				{
				case JsonToken.Integer:
				case JsonToken.Float:
					if (!(this.Value is decimal))
					{
						this.SetToken(JsonToken.Float, Convert.ToDecimal(this.Value, CultureInfo.InvariantCulture), false);
					}
					return new decimal?((decimal)this.Value);
				case JsonToken.String:
					return this.ReadDecimalString((string)this.Value);
				case JsonToken.Null:
				case JsonToken.EndArray:
					goto IL_34;
				}
				throw JsonReaderException.Create(this, "Error reading decimal. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
			}
			IL_34:
			return null;
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00007254 File Offset: 0x00005454
		internal decimal? ReadDecimalString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			decimal num;
			if (decimal.TryParse(s, NumberStyles.Number, this.Culture, out num))
			{
				this.SetToken(JsonToken.Float, num, false);
				return new decimal?(num);
			}
			this.SetToken(JsonToken.String, s, false);
			throw JsonReaderException.Create(this, "Could not convert string to decimal: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x06000187 RID: 391 RVA: 0x000072C4 File Offset: 0x000054C4
		public virtual DateTime? ReadAsDateTime()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken <= JsonToken.String)
			{
				if (contentToken != JsonToken.None)
				{
					if (contentToken != JsonToken.String)
					{
						goto IL_84;
					}
					string s = (string)this.Value;
					return this.ReadDateTimeString(s);
				}
			}
			else if (contentToken != JsonToken.Null && contentToken != JsonToken.EndArray)
			{
				if (contentToken != JsonToken.Date)
				{
					goto IL_84;
				}
				if (this.Value is DateTimeOffset)
				{
					this.SetToken(JsonToken.Date, ((DateTimeOffset)this.Value).DateTime, false);
				}
				return new DateTime?((DateTime)this.Value);
			}
			return null;
			IL_84:
			throw JsonReaderException.Create(this, "Error reading date. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, this.TokenType));
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00007378 File Offset: 0x00005578
		internal DateTime? ReadDateTimeString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			DateTime dateTime;
			if (DateTimeUtils.TryParseDateTime(s, this.DateTimeZoneHandling, this._dateFormatString, this.Culture, out dateTime))
			{
				dateTime = DateTimeUtils.EnsureDateTime(dateTime, this.DateTimeZoneHandling);
				this.SetToken(JsonToken.Date, dateTime, false);
				return new DateTime?(dateTime);
			}
			if (DateTime.TryParse(s, this.Culture, DateTimeStyles.RoundtripKind, out dateTime))
			{
				dateTime = DateTimeUtils.EnsureDateTime(dateTime, this.DateTimeZoneHandling);
				this.SetToken(JsonToken.Date, dateTime, false);
				return new DateTime?(dateTime);
			}
			throw JsonReaderException.Create(this, "Could not convert string to DateTime: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00007430 File Offset: 0x00005630
		public virtual DateTimeOffset? ReadAsDateTimeOffset()
		{
			JsonToken contentToken = this.GetContentToken();
			if (contentToken <= JsonToken.String)
			{
				if (contentToken != JsonToken.None)
				{
					if (contentToken != JsonToken.String)
					{
						goto IL_81;
					}
					string s = (string)this.Value;
					return this.ReadDateTimeOffsetString(s);
				}
			}
			else if (contentToken != JsonToken.Null && contentToken != JsonToken.EndArray)
			{
				if (contentToken != JsonToken.Date)
				{
					goto IL_81;
				}
				if (this.Value is DateTime)
				{
					this.SetToken(JsonToken.Date, new DateTimeOffset((DateTime)this.Value), false);
				}
				return new DateTimeOffset?((DateTimeOffset)this.Value);
			}
			return null;
			IL_81:
			throw JsonReaderException.Create(this, "Error reading date. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, contentToken));
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000074DC File Offset: 0x000056DC
		internal DateTimeOffset? ReadDateTimeOffsetString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				this.SetToken(JsonToken.Null, null, false);
				return null;
			}
			DateTimeOffset dateTimeOffset;
			if (DateTimeUtils.TryParseDateTimeOffset(s, this._dateFormatString, this.Culture, out dateTimeOffset))
			{
				this.SetToken(JsonToken.Date, dateTimeOffset, false);
				return new DateTimeOffset?(dateTimeOffset);
			}
			if (DateTimeOffset.TryParse(s, this.Culture, DateTimeStyles.RoundtripKind, out dateTimeOffset))
			{
				this.SetToken(JsonToken.Date, dateTimeOffset, false);
				return new DateTimeOffset?(dateTimeOffset);
			}
			this.SetToken(JsonToken.String, s, false);
			throw JsonReaderException.Create(this, "Could not convert string to DateTimeOffset: {0}.".FormatWith(CultureInfo.InvariantCulture, s));
		}

		// Token: 0x0600018B RID: 395 RVA: 0x0000757C File Offset: 0x0000577C
		internal void ReaderReadAndAssert()
		{
			if (!this.Read())
			{
				throw this.CreateUnexpectedEndException();
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000758D File Offset: 0x0000578D
		internal JsonReaderException CreateUnexpectedEndException()
		{
			return JsonReaderException.Create(this, "Unexpected end when reading JSON.");
		}

		// Token: 0x0600018D RID: 397 RVA: 0x0000759C File Offset: 0x0000579C
		internal void ReadIntoWrappedTypeObject()
		{
			this.ReaderReadAndAssert();
			if (this.Value.ToString() == "$type")
			{
				this.ReaderReadAndAssert();
				if (this.Value != null && this.Value.ToString().StartsWith("System.Byte[]", StringComparison.Ordinal))
				{
					this.ReaderReadAndAssert();
					if (this.Value.ToString() == "$value")
					{
						return;
					}
				}
			}
			throw JsonReaderException.Create(this, "Error reading bytes. Unexpected token: {0}.".FormatWith(CultureInfo.InvariantCulture, JsonToken.StartObject));
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00007628 File Offset: 0x00005828
		public void Skip()
		{
			if (this.TokenType == JsonToken.PropertyName)
			{
				this.Read();
			}
			if (JsonTokenUtils.IsStartToken(this.TokenType))
			{
				int depth = this.Depth;
				while (this.Read() && depth < this.Depth)
				{
				}
			}
		}

		// Token: 0x0600018F RID: 399 RVA: 0x0000766A File Offset: 0x0000586A
		protected void SetToken(JsonToken newToken)
		{
			this.SetToken(newToken, null, true);
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00007675 File Offset: 0x00005875
		protected void SetToken(JsonToken newToken, object value)
		{
			this.SetToken(newToken, value, true);
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00007680 File Offset: 0x00005880
		internal void SetToken(JsonToken newToken, object value, bool updateIndex)
		{
			this._tokenType = newToken;
			this._value = value;
			switch (newToken)
			{
			case JsonToken.StartObject:
				this._currentState = JsonReader.State.ObjectStart;
				this.Push(JsonContainerType.Object);
				return;
			case JsonToken.StartArray:
				this._currentState = JsonReader.State.ArrayStart;
				this.Push(JsonContainerType.Array);
				return;
			case JsonToken.StartConstructor:
				this._currentState = JsonReader.State.ConstructorStart;
				this.Push(JsonContainerType.Constructor);
				return;
			case JsonToken.PropertyName:
				this._currentState = JsonReader.State.Property;
				this._currentPosition.PropertyName = (string)value;
				return;
			case JsonToken.Comment:
				break;
			case JsonToken.Raw:
			case JsonToken.Integer:
			case JsonToken.Float:
			case JsonToken.String:
			case JsonToken.Boolean:
			case JsonToken.Null:
			case JsonToken.Undefined:
			case JsonToken.Date:
			case JsonToken.Bytes:
				this.SetPostValueState(updateIndex);
				break;
			case JsonToken.EndObject:
				this.ValidateEnd(JsonToken.EndObject);
				return;
			case JsonToken.EndArray:
				this.ValidateEnd(JsonToken.EndArray);
				return;
			case JsonToken.EndConstructor:
				this.ValidateEnd(JsonToken.EndConstructor);
				return;
			default:
				return;
			}
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00007751 File Offset: 0x00005951
		internal void SetPostValueState(bool updateIndex)
		{
			if (this.Peek() != JsonContainerType.None)
			{
				this._currentState = JsonReader.State.PostValue;
			}
			else
			{
				this.SetFinished();
			}
			if (updateIndex)
			{
				this.UpdateScopeWithFinishedValue();
			}
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00007773 File Offset: 0x00005973
		private void UpdateScopeWithFinishedValue()
		{
			if (this._currentPosition.HasIndex)
			{
				this._currentPosition.Position = this._currentPosition.Position + 1;
			}
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00007794 File Offset: 0x00005994
		private void ValidateEnd(JsonToken endToken)
		{
			JsonContainerType jsonContainerType = this.Pop();
			if (this.GetTypeForCloseToken(endToken) != jsonContainerType)
			{
				throw JsonReaderException.Create(this, "JsonToken {0} is not valid for closing JsonType {1}.".FormatWith(CultureInfo.InvariantCulture, endToken, jsonContainerType));
			}
			if (this.Peek() != JsonContainerType.None)
			{
				this._currentState = JsonReader.State.PostValue;
				return;
			}
			this.SetFinished();
		}

		// Token: 0x06000195 RID: 405 RVA: 0x000077EC File Offset: 0x000059EC
		protected void SetStateBasedOnCurrent()
		{
			JsonContainerType jsonContainerType = this.Peek();
			switch (jsonContainerType)
			{
			case JsonContainerType.None:
				this.SetFinished();
				return;
			case JsonContainerType.Object:
				this._currentState = JsonReader.State.Object;
				return;
			case JsonContainerType.Array:
				this._currentState = JsonReader.State.Array;
				return;
			case JsonContainerType.Constructor:
				this._currentState = JsonReader.State.Constructor;
				return;
			default:
				throw JsonReaderException.Create(this, "While setting the reader state back to current object an unexpected JsonType was encountered: {0}".FormatWith(CultureInfo.InvariantCulture, jsonContainerType));
			}
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00007853 File Offset: 0x00005A53
		private void SetFinished()
		{
			if (this.SupportMultipleContent)
			{
				this._currentState = JsonReader.State.Start;
				return;
			}
			this._currentState = JsonReader.State.Finished;
		}

		// Token: 0x06000197 RID: 407 RVA: 0x0000786D File Offset: 0x00005A6D
		private JsonContainerType GetTypeForCloseToken(JsonToken token)
		{
			switch (token)
			{
			case JsonToken.EndObject:
				return JsonContainerType.Object;
			case JsonToken.EndArray:
				return JsonContainerType.Array;
			case JsonToken.EndConstructor:
				return JsonContainerType.Constructor;
			default:
				throw JsonReaderException.Create(this, "Not a valid close JsonToken: {0}".FormatWith(CultureInfo.InvariantCulture, token));
			}
		}

		// Token: 0x06000198 RID: 408 RVA: 0x000078A7 File Offset: 0x00005AA7
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000199 RID: 409 RVA: 0x000078B6 File Offset: 0x00005AB6
		protected virtual void Dispose(bool disposing)
		{
			if (this._currentState != JsonReader.State.Closed && disposing)
			{
				this.Close();
			}
		}

		// Token: 0x0600019A RID: 410 RVA: 0x000078CE File Offset: 0x00005ACE
		public virtual void Close()
		{
			this._currentState = JsonReader.State.Closed;
			this._tokenType = JsonToken.None;
			this._value = null;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x000078E5 File Offset: 0x00005AE5
		internal void ReadAndAssert()
		{
			if (!this.Read())
			{
				throw JsonSerializationException.Create(this, "Unexpected end when reading JSON.");
			}
		}

		// Token: 0x0600019C RID: 412 RVA: 0x000078FB File Offset: 0x00005AFB
		internal bool ReadAndMoveToContent()
		{
			return this.Read() && this.MoveToContent();
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00007910 File Offset: 0x00005B10
		internal bool MoveToContent()
		{
			JsonToken tokenType = this.TokenType;
			while (tokenType == JsonToken.None || tokenType == JsonToken.Comment)
			{
				if (!this.Read())
				{
					return false;
				}
				tokenType = this.TokenType;
			}
			return true;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x00007940 File Offset: 0x00005B40
		private JsonToken GetContentToken()
		{
			while (this.Read())
			{
				JsonToken tokenType = this.TokenType;
				if (tokenType != JsonToken.Comment)
				{
					return tokenType;
				}
			}
			this.SetToken(JsonToken.None);
			return JsonToken.None;
		}

		// Token: 0x040000C8 RID: 200
		private JsonToken _tokenType;

		// Token: 0x040000C9 RID: 201
		private object _value;

		// Token: 0x040000CA RID: 202
		internal char _quoteChar;

		// Token: 0x040000CB RID: 203
		internal JsonReader.State _currentState;

		// Token: 0x040000CC RID: 204
		private JsonPosition _currentPosition;

		// Token: 0x040000CD RID: 205
		private CultureInfo _culture;

		// Token: 0x040000CE RID: 206
		private DateTimeZoneHandling _dateTimeZoneHandling;

		// Token: 0x040000CF RID: 207
		private int? _maxDepth;

		// Token: 0x040000D0 RID: 208
		private bool _hasExceededMaxDepth;

		// Token: 0x040000D1 RID: 209
		internal DateParseHandling _dateParseHandling;

		// Token: 0x040000D2 RID: 210
		internal FloatParseHandling _floatParseHandling;

		// Token: 0x040000D3 RID: 211
		private string _dateFormatString;

		// Token: 0x040000D4 RID: 212
		private List<JsonPosition> _stack;

		// Token: 0x020000E7 RID: 231
		protected internal enum State
		{
			// Token: 0x04000354 RID: 852
			Start,
			// Token: 0x04000355 RID: 853
			Complete,
			// Token: 0x04000356 RID: 854
			Property,
			// Token: 0x04000357 RID: 855
			ObjectStart,
			// Token: 0x04000358 RID: 856
			Object,
			// Token: 0x04000359 RID: 857
			ArrayStart,
			// Token: 0x0400035A RID: 858
			Array,
			// Token: 0x0400035B RID: 859
			Closed,
			// Token: 0x0400035C RID: 860
			PostValue,
			// Token: 0x0400035D RID: 861
			ConstructorStart,
			// Token: 0x0400035E RID: 862
			Constructor,
			// Token: 0x0400035F RID: 863
			Error,
			// Token: 0x04000360 RID: 864
			Finished
		}
	}
}
