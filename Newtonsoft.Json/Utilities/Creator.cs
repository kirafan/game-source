﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000059 RID: 89
	// (Invoke) Token: 0x060003AE RID: 942
	[Preserve]
	internal delegate T Creator<T>();
}
