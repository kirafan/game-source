﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000045 RID: 69
	[Preserve]
	internal struct StringReference
	{
		// Token: 0x17000093 RID: 147
		public char this[int i]
		{
			get
			{
				return this._chars[i];
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060002EE RID: 750 RVA: 0x0000B7D0 File Offset: 0x000099D0
		public char[] Chars
		{
			get
			{
				return this._chars;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060002EF RID: 751 RVA: 0x0000B7D8 File Offset: 0x000099D8
		public int StartIndex
		{
			get
			{
				return this._startIndex;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060002F0 RID: 752 RVA: 0x0000B7E0 File Offset: 0x000099E0
		public int Length
		{
			get
			{
				return this._length;
			}
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000B7E8 File Offset: 0x000099E8
		public StringReference(char[] chars, int startIndex, int length)
		{
			this._chars = chars;
			this._startIndex = startIndex;
			this._length = length;
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0000B7FF File Offset: 0x000099FF
		public override string ToString()
		{
			return new string(this._chars, this._startIndex, this._length);
		}

		// Token: 0x04000167 RID: 359
		private readonly char[] _chars;

		// Token: 0x04000168 RID: 360
		private readonly int _startIndex;

		// Token: 0x04000169 RID: 361
		private readonly int _length;
	}
}
