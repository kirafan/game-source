﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200005E RID: 94
	[Preserve]
	internal static class ValidationUtils
	{
		// Token: 0x06000406 RID: 1030 RVA: 0x000108A4 File Offset: 0x0000EAA4
		public static void ArgumentNotNull(object value, string parameterName)
		{
			if (value == null)
			{
				throw new ArgumentNullException(parameterName);
			}
		}
	}
}
