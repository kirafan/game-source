﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000047 RID: 71
	[Preserve]
	internal class ThreadSafeStore<TKey, TValue>
	{
		// Token: 0x060002F6 RID: 758 RVA: 0x0000B8FB File Offset: 0x00009AFB
		[Preserve]
		public ThreadSafeStore(Func<TKey, TValue> creator)
		{
			if (creator == null)
			{
				throw new ArgumentNullException("creator");
			}
			this._creator = creator;
			this._store = new Dictionary<TKey, TValue>();
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000B930 File Offset: 0x00009B30
		[Preserve]
		public TValue Get(TKey key)
		{
			TValue result;
			if (!this._store.TryGetValue(key, out result))
			{
				return this.AddValue(key);
			}
			return result;
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000B958 File Offset: 0x00009B58
		[Preserve]
		private TValue AddValue(TKey key)
		{
			TValue tvalue = this._creator(key);
			object @lock = this._lock;
			TValue result2;
			lock (@lock)
			{
				if (this._store == null)
				{
					this._store = new Dictionary<TKey, TValue>();
					this._store[key] = tvalue;
				}
				else
				{
					TValue result;
					if (this._store.TryGetValue(key, out result))
					{
						return result;
					}
					Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(this._store);
					dictionary[key] = tvalue;
					this._store = dictionary;
				}
				result2 = tvalue;
			}
			return result2;
		}

		// Token: 0x0400016A RID: 362
		private readonly object _lock = new object();

		// Token: 0x0400016B RID: 363
		private Dictionary<TKey, TValue> _store;

		// Token: 0x0400016C RID: 364
		private readonly Func<TKey, TValue> _creator;
	}
}
