﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200003B RID: 59
	[Preserve]
	internal enum ParserTimeZone
	{
		// Token: 0x04000138 RID: 312
		Unspecified,
		// Token: 0x04000139 RID: 313
		Utc,
		// Token: 0x0400013A RID: 314
		LocalWestOfUtc,
		// Token: 0x0400013B RID: 315
		LocalEastOfUtc
	}
}
