﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000056 RID: 86
	[Preserve]
	internal struct StringBuffer
	{
		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600038A RID: 906 RVA: 0x0000EB7C File Offset: 0x0000CD7C
		// (set) Token: 0x0600038B RID: 907 RVA: 0x0000EB84 File Offset: 0x0000CD84
		public int Position
		{
			get
			{
				return this._position;
			}
			set
			{
				this._position = value;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600038C RID: 908 RVA: 0x0000EB8D File Offset: 0x0000CD8D
		public bool IsEmpty
		{
			get
			{
				return this._buffer == null;
			}
		}

		// Token: 0x0600038D RID: 909 RVA: 0x0000EB98 File Offset: 0x0000CD98
		public StringBuffer(IArrayPool<char> bufferPool, int initalSize)
		{
			this = new StringBuffer(BufferUtils.RentBuffer(bufferPool, initalSize));
		}

		// Token: 0x0600038E RID: 910 RVA: 0x0000EBA7 File Offset: 0x0000CDA7
		private StringBuffer(char[] buffer)
		{
			this._buffer = buffer;
			this._position = 0;
		}

		// Token: 0x0600038F RID: 911 RVA: 0x0000EBB8 File Offset: 0x0000CDB8
		public void Append(IArrayPool<char> bufferPool, char value)
		{
			if (this._position == this._buffer.Length)
			{
				this.EnsureSize(bufferPool, 1);
			}
			char[] buffer = this._buffer;
			int position = this._position;
			this._position = position + 1;
			buffer[position] = value;
		}

		// Token: 0x06000390 RID: 912 RVA: 0x0000EBF8 File Offset: 0x0000CDF8
		public void Append(IArrayPool<char> bufferPool, char[] buffer, int startIndex, int count)
		{
			if (this._position + count >= this._buffer.Length)
			{
				this.EnsureSize(bufferPool, count);
			}
			Array.Copy(buffer, startIndex, this._buffer, this._position, count);
			this._position += count;
		}

		// Token: 0x06000391 RID: 913 RVA: 0x0000EC45 File Offset: 0x0000CE45
		public void Clear(IArrayPool<char> bufferPool)
		{
			if (this._buffer != null)
			{
				BufferUtils.ReturnBuffer(bufferPool, this._buffer);
				this._buffer = null;
			}
			this._position = 0;
		}

		// Token: 0x06000392 RID: 914 RVA: 0x0000EC6C File Offset: 0x0000CE6C
		private void EnsureSize(IArrayPool<char> bufferPool, int appendLength)
		{
			char[] array = BufferUtils.RentBuffer(bufferPool, (this._position + appendLength) * 2);
			if (this._buffer != null)
			{
				Array.Copy(this._buffer, array, this._position);
				BufferUtils.ReturnBuffer(bufferPool, this._buffer);
			}
			this._buffer = array;
		}

		// Token: 0x06000393 RID: 915 RVA: 0x0000ECB7 File Offset: 0x0000CEB7
		public override string ToString()
		{
			return this.ToString(0, this._position);
		}

		// Token: 0x06000394 RID: 916 RVA: 0x0000ECC6 File Offset: 0x0000CEC6
		public string ToString(int start, int length)
		{
			return new string(this._buffer, start, length);
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000395 RID: 917 RVA: 0x0000ECD5 File Offset: 0x0000CED5
		public char[] InternalBuffer
		{
			get
			{
				return this._buffer;
			}
		}

		// Token: 0x040001BD RID: 445
		private char[] _buffer;

		// Token: 0x040001BE RID: 446
		private int _position;
	}
}
