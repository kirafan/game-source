﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000053 RID: 83
	[Preserve]
	internal class EnumValue<T> where T : struct
	{
		// Token: 0x170000AE RID: 174
		// (get) Token: 0x0600037F RID: 895 RVA: 0x0000E6A7 File Offset: 0x0000C8A7
		public string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000380 RID: 896 RVA: 0x0000E6AF File Offset: 0x0000C8AF
		public T Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000E6B7 File Offset: 0x0000C8B7
		public EnumValue(string name, T value)
		{
			this._name = name;
			this._value = value;
		}

		// Token: 0x040001B6 RID: 438
		private readonly string _name;

		// Token: 0x040001B7 RID: 439
		private readonly T _value;
	}
}
