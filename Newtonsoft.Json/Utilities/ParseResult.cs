﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200004B RID: 75
	[Preserve]
	internal enum ParseResult
	{
		// Token: 0x0400019F RID: 415
		None,
		// Token: 0x040001A0 RID: 416
		Success,
		// Token: 0x040001A1 RID: 417
		Overflow,
		// Token: 0x040001A2 RID: 418
		Invalid
	}
}
