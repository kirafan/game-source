﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200005C RID: 92
	[Preserve]
	internal static class StringUtils
	{
		// Token: 0x060003E5 RID: 997 RVA: 0x000104B8 File Offset: 0x0000E6B8
		public static string FormatWith(this string format, IFormatProvider provider, object arg0)
		{
			return format.FormatWith(provider, new object[]
			{
				arg0
			});
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x000104CB File Offset: 0x0000E6CB
		public static string FormatWith(this string format, IFormatProvider provider, object arg0, object arg1)
		{
			return format.FormatWith(provider, new object[]
			{
				arg0,
				arg1
			});
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x000104E2 File Offset: 0x0000E6E2
		public static string FormatWith(this string format, IFormatProvider provider, object arg0, object arg1, object arg2)
		{
			return format.FormatWith(provider, new object[]
			{
				arg0,
				arg1,
				arg2
			});
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x000104FE File Offset: 0x0000E6FE
		public static string FormatWith(this string format, IFormatProvider provider, object arg0, object arg1, object arg2, object arg3)
		{
			return format.FormatWith(provider, new object[]
			{
				arg0,
				arg1,
				arg2,
				arg3
			});
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0001051F File Offset: 0x0000E71F
		private static string FormatWith(this string format, IFormatProvider provider, params object[] args)
		{
			ValidationUtils.ArgumentNotNull(format, "format");
			return string.Format(provider, format, args);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x00010534 File Offset: 0x0000E734
		public static bool IsWhiteSpace(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (s.Length == 0)
			{
				return false;
			}
			for (int i = 0; i < s.Length; i++)
			{
				if (!char.IsWhiteSpace(s[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x0001057B File Offset: 0x0000E77B
		public static string NullEmptyString(string s)
		{
			if (!string.IsNullOrEmpty(s))
			{
				return s;
			}
			return null;
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x00010588 File Offset: 0x0000E788
		public static StringWriter CreateStringWriter(int capacity)
		{
			return new StringWriter(new StringBuilder(capacity), CultureInfo.InvariantCulture);
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x0001059C File Offset: 0x0000E79C
		public static int? GetLength(string value)
		{
			if (value == null)
			{
				return null;
			}
			return new int?(value.Length);
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x000105C4 File Offset: 0x0000E7C4
		public static void ToCharAsUnicode(char c, char[] buffer)
		{
			buffer[0] = '\\';
			buffer[1] = 'u';
			buffer[2] = MathUtils.IntToHex((int)(c >> 12 & '\u000f'));
			buffer[3] = MathUtils.IntToHex((int)(c >> 8 & '\u000f'));
			buffer[4] = MathUtils.IntToHex((int)(c >> 4 & '\u000f'));
			buffer[5] = MathUtils.IntToHex((int)(c & '\u000f'));
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x00010614 File Offset: 0x0000E814
		public static TSource ForgivingCaseSensitiveFind<TSource>(this IEnumerable<TSource> source, Func<TSource, string> valueSelector, string testValue)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (valueSelector == null)
			{
				throw new ArgumentNullException("valueSelector");
			}
			IEnumerable<TSource> source2 = from s in source
			where string.Equals(valueSelector(s), testValue, StringComparison.OrdinalIgnoreCase)
			select s;
			if (source2.Count<TSource>() <= 1)
			{
				return source2.SingleOrDefault<TSource>();
			}
			return (from s in source
			where string.Equals(valueSelector(s), testValue, StringComparison.Ordinal)
			select s).SingleOrDefault<TSource>();
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00010690 File Offset: 0x0000E890
		public static string ToCamelCase(string s)
		{
			if (string.IsNullOrEmpty(s) || !char.IsUpper(s[0]))
			{
				return s;
			}
			char[] array = s.ToCharArray();
			int num = 0;
			while (num < array.Length && (num != 1 || char.IsUpper(array[num])))
			{
				bool flag = num + 1 < array.Length;
				if (num > 0 && flag && !char.IsUpper(array[num + 1]))
				{
					break;
				}
				array[num] = char.ToLower(array[num], CultureInfo.InvariantCulture);
				num++;
			}
			return new string(array);
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0001070B File Offset: 0x0000E90B
		public static bool IsHighSurrogate(char c)
		{
			return char.IsHighSurrogate(c);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x00010713 File Offset: 0x0000E913
		public static bool IsLowSurrogate(char c)
		{
			return char.IsLowSurrogate(c);
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0001071B File Offset: 0x0000E91B
		public static bool StartsWith(this string source, char value)
		{
			return source.Length > 0 && source[0] == value;
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00010732 File Offset: 0x0000E932
		public static bool EndsWith(this string source, char value)
		{
			return source.Length > 0 && source[source.Length - 1] == value;
		}

		// Token: 0x040001C0 RID: 448
		public const string CarriageReturnLineFeed = "\r\n";

		// Token: 0x040001C1 RID: 449
		public const string Empty = "";

		// Token: 0x040001C2 RID: 450
		public const char CarriageReturn = '\r';

		// Token: 0x040001C3 RID: 451
		public const char LineFeed = '\n';

		// Token: 0x040001C4 RID: 452
		public const char Tab = '\t';
	}
}
