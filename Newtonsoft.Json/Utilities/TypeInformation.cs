﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200004A RID: 74
	[Preserve]
	internal class TypeInformation
	{
		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060002FF RID: 767 RVA: 0x0000BB0F File Offset: 0x00009D0F
		// (set) Token: 0x06000300 RID: 768 RVA: 0x0000BB17 File Offset: 0x00009D17
		public Type Type { get; set; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000301 RID: 769 RVA: 0x0000BB20 File Offset: 0x00009D20
		// (set) Token: 0x06000302 RID: 770 RVA: 0x0000BB28 File Offset: 0x00009D28
		public PrimitiveTypeCode TypeCode { get; set; }
	}
}
