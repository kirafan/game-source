﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000042 RID: 66
	// (Invoke) Token: 0x060002D9 RID: 729
	[Preserve]
	internal delegate TResult MethodCall<T, TResult>(T target, params object[] args);
}
