﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000049 RID: 73
	[Preserve]
	internal enum PrimitiveTypeCode
	{
		// Token: 0x04000172 RID: 370
		Empty,
		// Token: 0x04000173 RID: 371
		Object,
		// Token: 0x04000174 RID: 372
		Char,
		// Token: 0x04000175 RID: 373
		CharNullable,
		// Token: 0x04000176 RID: 374
		Boolean,
		// Token: 0x04000177 RID: 375
		BooleanNullable,
		// Token: 0x04000178 RID: 376
		SByte,
		// Token: 0x04000179 RID: 377
		SByteNullable,
		// Token: 0x0400017A RID: 378
		Int16,
		// Token: 0x0400017B RID: 379
		Int16Nullable,
		// Token: 0x0400017C RID: 380
		UInt16,
		// Token: 0x0400017D RID: 381
		UInt16Nullable,
		// Token: 0x0400017E RID: 382
		Int32,
		// Token: 0x0400017F RID: 383
		Int32Nullable,
		// Token: 0x04000180 RID: 384
		Byte,
		// Token: 0x04000181 RID: 385
		ByteNullable,
		// Token: 0x04000182 RID: 386
		UInt32,
		// Token: 0x04000183 RID: 387
		UInt32Nullable,
		// Token: 0x04000184 RID: 388
		Int64,
		// Token: 0x04000185 RID: 389
		Int64Nullable,
		// Token: 0x04000186 RID: 390
		UInt64,
		// Token: 0x04000187 RID: 391
		UInt64Nullable,
		// Token: 0x04000188 RID: 392
		Single,
		// Token: 0x04000189 RID: 393
		SingleNullable,
		// Token: 0x0400018A RID: 394
		Double,
		// Token: 0x0400018B RID: 395
		DoubleNullable,
		// Token: 0x0400018C RID: 396
		DateTime,
		// Token: 0x0400018D RID: 397
		DateTimeNullable,
		// Token: 0x0400018E RID: 398
		DateTimeOffset,
		// Token: 0x0400018F RID: 399
		DateTimeOffsetNullable,
		// Token: 0x04000190 RID: 400
		Decimal,
		// Token: 0x04000191 RID: 401
		DecimalNullable,
		// Token: 0x04000192 RID: 402
		Guid,
		// Token: 0x04000193 RID: 403
		GuidNullable,
		// Token: 0x04000194 RID: 404
		TimeSpan,
		// Token: 0x04000195 RID: 405
		TimeSpanNullable,
		// Token: 0x04000196 RID: 406
		BigInteger,
		// Token: 0x04000197 RID: 407
		BigIntegerNullable,
		// Token: 0x04000198 RID: 408
		Uri,
		// Token: 0x04000199 RID: 409
		String,
		// Token: 0x0400019A RID: 410
		Bytes,
		// Token: 0x0400019B RID: 411
		DBNull
	}
}
