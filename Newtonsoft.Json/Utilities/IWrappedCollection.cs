﻿using System;
using System.Collections;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200004D RID: 77
	[Preserve]
	internal interface IWrappedCollection : IList, ICollection, IEnumerable
	{
		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000318 RID: 792
		object UnderlyingCollection { get; }
	}
}
