﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000051 RID: 81
	[Preserve]
	internal class DictionaryWrapper<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable, IWrappedDictionary, IDictionary, ICollection
	{
		// Token: 0x06000355 RID: 853 RVA: 0x0000DB25 File Offset: 0x0000BD25
		public DictionaryWrapper(IDictionary dictionary)
		{
			ValidationUtils.ArgumentNotNull(dictionary, "dictionary");
			this._dictionary = dictionary;
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000DB3F File Offset: 0x0000BD3F
		public DictionaryWrapper(IDictionary<TKey, TValue> dictionary)
		{
			ValidationUtils.ArgumentNotNull(dictionary, "dictionary");
			this._genericDictionary = dictionary;
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000DB59 File Offset: 0x0000BD59
		public void Add(TKey key, TValue value)
		{
			if (this._dictionary != null)
			{
				this._dictionary.Add(key, value);
				return;
			}
			if (this._genericDictionary != null)
			{
				this._genericDictionary.Add(key, value);
				return;
			}
			throw new NotSupportedException();
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000DB96 File Offset: 0x0000BD96
		public bool ContainsKey(TKey key)
		{
			if (this._dictionary != null)
			{
				return this._dictionary.Contains(key);
			}
			return this._genericDictionary.ContainsKey(key);
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000359 RID: 857 RVA: 0x0000DBBE File Offset: 0x0000BDBE
		public ICollection<TKey> Keys
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary.Keys.Cast<TKey>().ToList<TKey>();
				}
				return this._genericDictionary.Keys;
			}
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000DBE9 File Offset: 0x0000BDE9
		public bool Remove(TKey key)
		{
			if (this._dictionary == null)
			{
				return this._genericDictionary.Remove(key);
			}
			if (this._dictionary.Contains(key))
			{
				this._dictionary.Remove(key);
				return true;
			}
			return false;
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000DC28 File Offset: 0x0000BE28
		public bool TryGetValue(TKey key, out TValue value)
		{
			if (this._dictionary == null)
			{
				return this._genericDictionary.TryGetValue(key, out value);
			}
			if (!this._dictionary.Contains(key))
			{
				value = default(TValue);
				return false;
			}
			value = (TValue)((object)this._dictionary[key]);
			return true;
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600035C RID: 860 RVA: 0x0000DC84 File Offset: 0x0000BE84
		public ICollection<TValue> Values
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary.Values.Cast<TValue>().ToList<TValue>();
				}
				return this._genericDictionary.Values;
			}
		}

		// Token: 0x170000A4 RID: 164
		public TValue this[TKey key]
		{
			get
			{
				if (this._dictionary != null)
				{
					return (TValue)((object)this._dictionary[key]);
				}
				return this._genericDictionary[key];
			}
			set
			{
				if (this._dictionary != null)
				{
					this._dictionary[key] = value;
					return;
				}
				this._genericDictionary[key] = value;
			}
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000DD0B File Offset: 0x0000BF0B
		public void Add(KeyValuePair<TKey, TValue> item)
		{
			if (this._dictionary != null)
			{
				((IList)this._dictionary).Add(item);
				return;
			}
			if (this._genericDictionary != null)
			{
				this._genericDictionary.Add(item);
			}
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000DD41 File Offset: 0x0000BF41
		public void Clear()
		{
			if (this._dictionary != null)
			{
				this._dictionary.Clear();
				return;
			}
			this._genericDictionary.Clear();
		}

		// Token: 0x06000361 RID: 865 RVA: 0x0000DD62 File Offset: 0x0000BF62
		public bool Contains(KeyValuePair<TKey, TValue> item)
		{
			if (this._dictionary != null)
			{
				return ((IList)this._dictionary).Contains(item);
			}
			return this._genericDictionary.Contains(item);
		}

		// Token: 0x06000362 RID: 866 RVA: 0x0000DD90 File Offset: 0x0000BF90
		public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
		{
			if (this._dictionary != null)
			{
				using (IDictionaryEnumerator enumerator = this._dictionary.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						DictionaryEntry entry = enumerator.Entry;
						array[arrayIndex++] = new KeyValuePair<TKey, TValue>((TKey)((object)entry.Key), (TValue)((object)entry.Value));
					}
					return;
				}
			}
			this._genericDictionary.CopyTo(array, arrayIndex);
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000363 RID: 867 RVA: 0x0000DE1C File Offset: 0x0000C01C
		public int Count
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary.Count;
				}
				return this._genericDictionary.Count;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000364 RID: 868 RVA: 0x0000DE3D File Offset: 0x0000C03D
		public bool IsReadOnly
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary.IsReadOnly;
				}
				return this._genericDictionary.IsReadOnly;
			}
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000DE60 File Offset: 0x0000C060
		public bool Remove(KeyValuePair<TKey, TValue> item)
		{
			if (this._dictionary == null)
			{
				return this._genericDictionary.Remove(item);
			}
			if (!this._dictionary.Contains(item.Key))
			{
				return true;
			}
			if (object.Equals(this._dictionary[item.Key], item.Value))
			{
				this._dictionary.Remove(item.Key);
				return true;
			}
			return false;
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000DEE4 File Offset: 0x0000C0E4
		public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
		{
			if (this._dictionary != null)
			{
				return (from DictionaryEntry de in this._dictionary
				select new KeyValuePair<TKey, TValue>((TKey)((object)de.Key), (TValue)((object)de.Value))).GetEnumerator();
			}
			return this._genericDictionary.GetEnumerator();
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000DF39 File Offset: 0x0000C139
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000DF41 File Offset: 0x0000C141
		void IDictionary.Add(object key, object value)
		{
			if (this._dictionary != null)
			{
				this._dictionary.Add(key, value);
				return;
			}
			this._genericDictionary.Add((TKey)((object)key), (TValue)((object)value));
		}

		// Token: 0x170000A7 RID: 167
		object IDictionary.this[object key]
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary[key];
				}
				return this._genericDictionary[(TKey)((object)key)];
			}
			set
			{
				if (this._dictionary != null)
				{
					this._dictionary[key] = value;
					return;
				}
				this._genericDictionary[(TKey)((object)key)] = (TValue)((object)value);
			}
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000DFCC File Offset: 0x0000C1CC
		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			if (this._dictionary != null)
			{
				return this._dictionary.GetEnumerator();
			}
			return new DictionaryWrapper<TKey, TValue>.DictionaryEnumerator<TKey, TValue>(this._genericDictionary.GetEnumerator());
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000DFF7 File Offset: 0x0000C1F7
		bool IDictionary.Contains(object key)
		{
			if (this._genericDictionary != null)
			{
				return this._genericDictionary.ContainsKey((TKey)((object)key));
			}
			return this._dictionary.Contains(key);
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x0600036D RID: 877 RVA: 0x0000E01F File Offset: 0x0000C21F
		bool IDictionary.IsFixedSize
		{
			get
			{
				return this._genericDictionary == null && this._dictionary.IsFixedSize;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x0600036E RID: 878 RVA: 0x0000E036 File Offset: 0x0000C236
		ICollection IDictionary.Keys
		{
			get
			{
				if (this._genericDictionary != null)
				{
					return this._genericDictionary.Keys.ToList<TKey>();
				}
				return this._dictionary.Keys;
			}
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000E05C File Offset: 0x0000C25C
		public void Remove(object key)
		{
			if (this._dictionary != null)
			{
				this._dictionary.Remove(key);
				return;
			}
			this._genericDictionary.Remove((TKey)((object)key));
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000370 RID: 880 RVA: 0x0000E085 File Offset: 0x0000C285
		ICollection IDictionary.Values
		{
			get
			{
				if (this._genericDictionary != null)
				{
					return this._genericDictionary.Values.ToList<TValue>();
				}
				return this._dictionary.Values;
			}
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000E0AB File Offset: 0x0000C2AB
		void ICollection.CopyTo(Array array, int index)
		{
			if (this._dictionary != null)
			{
				this._dictionary.CopyTo(array, index);
				return;
			}
			this._genericDictionary.CopyTo((KeyValuePair<TKey, TValue>[])array, index);
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000372 RID: 882 RVA: 0x0000E0D5 File Offset: 0x0000C2D5
		bool ICollection.IsSynchronized
		{
			get
			{
				return this._dictionary != null && this._dictionary.IsSynchronized;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000373 RID: 883 RVA: 0x0000E0EC File Offset: 0x0000C2EC
		object ICollection.SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
				}
				return this._syncRoot;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000374 RID: 884 RVA: 0x0000E10E File Offset: 0x0000C30E
		public object UnderlyingDictionary
		{
			get
			{
				if (this._dictionary != null)
				{
					return this._dictionary;
				}
				return this._genericDictionary;
			}
		}

		// Token: 0x040001B2 RID: 434
		private readonly IDictionary _dictionary;

		// Token: 0x040001B3 RID: 435
		private readonly IDictionary<TKey, TValue> _genericDictionary;

		// Token: 0x040001B4 RID: 436
		private object _syncRoot;

		// Token: 0x020000F7 RID: 247
		private struct DictionaryEnumerator<TEnumeratorKey, TEnumeratorValue> : IDictionaryEnumerator, IEnumerator
		{
			// Token: 0x06000A9C RID: 2716 RVA: 0x0002707C File Offset: 0x0002527C
			public DictionaryEnumerator(IEnumerator<KeyValuePair<TEnumeratorKey, TEnumeratorValue>> e)
			{
				ValidationUtils.ArgumentNotNull(e, "e");
				this._e = e;
			}

			// Token: 0x17000216 RID: 534
			// (get) Token: 0x06000A9D RID: 2717 RVA: 0x00027090 File Offset: 0x00025290
			public DictionaryEntry Entry
			{
				get
				{
					return (DictionaryEntry)this.Current;
				}
			}

			// Token: 0x17000217 RID: 535
			// (get) Token: 0x06000A9E RID: 2718 RVA: 0x000270A0 File Offset: 0x000252A0
			public object Key
			{
				get
				{
					return this.Entry.Key;
				}
			}

			// Token: 0x17000218 RID: 536
			// (get) Token: 0x06000A9F RID: 2719 RVA: 0x000270BC File Offset: 0x000252BC
			public object Value
			{
				get
				{
					return this.Entry.Value;
				}
			}

			// Token: 0x17000219 RID: 537
			// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x000270D8 File Offset: 0x000252D8
			public object Current
			{
				get
				{
					KeyValuePair<TEnumeratorKey, TEnumeratorValue> keyValuePair = this._e.Current;
					object key = keyValuePair.Key;
					keyValuePair = this._e.Current;
					return new DictionaryEntry(key, keyValuePair.Value);
				}
			}

			// Token: 0x06000AA1 RID: 2721 RVA: 0x0002711F File Offset: 0x0002531F
			public bool MoveNext()
			{
				return this._e.MoveNext();
			}

			// Token: 0x06000AA2 RID: 2722 RVA: 0x0002712C File Offset: 0x0002532C
			public void Reset()
			{
				this._e.Reset();
			}

			// Token: 0x04000384 RID: 900
			private readonly IEnumerator<KeyValuePair<TEnumeratorKey, TEnumeratorValue>> _e;
		}
	}
}
