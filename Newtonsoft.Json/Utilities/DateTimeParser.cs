﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200003C RID: 60
	[Preserve]
	internal struct DateTimeParser
	{
		// Token: 0x060002AE RID: 686 RVA: 0x0000A986 File Offset: 0x00008B86
		public bool Parse(char[] text, int startIndex, int length)
		{
			this._text = text;
			this._end = startIndex + length;
			return this.ParseDate(startIndex) && this.ParseChar(DateTimeParser.Lzyyyy_MM_dd + startIndex, 'T') && this.ParseTimeAndZoneAndWhitespace(DateTimeParser.Lzyyyy_MM_ddT + startIndex);
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000A9C4 File Offset: 0x00008BC4
		private bool ParseDate(int start)
		{
			return this.Parse4Digit(start, out this.Year) && 1 <= this.Year && this.ParseChar(start + DateTimeParser.Lzyyyy, '-') && this.Parse2Digit(start + DateTimeParser.Lzyyyy_, out this.Month) && 1 <= this.Month && this.Month <= 12 && this.ParseChar(start + DateTimeParser.Lzyyyy_MM, '-') && this.Parse2Digit(start + DateTimeParser.Lzyyyy_MM_, out this.Day) && 1 <= this.Day && this.Day <= DateTime.DaysInMonth(this.Year, this.Month);
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x0000AA75 File Offset: 0x00008C75
		private bool ParseTimeAndZoneAndWhitespace(int start)
		{
			return this.ParseTime(ref start) && this.ParseZone(start);
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000AA8C File Offset: 0x00008C8C
		private bool ParseTime(ref int start)
		{
			if (!this.Parse2Digit(start, out this.Hour) || this.Hour > 24 || !this.ParseChar(start + DateTimeParser.LzHH, ':') || !this.Parse2Digit(start + DateTimeParser.LzHH_, out this.Minute) || this.Minute >= 60 || !this.ParseChar(start + DateTimeParser.LzHH_mm, ':') || !this.Parse2Digit(start + DateTimeParser.LzHH_mm_, out this.Second) || this.Second >= 60 || (this.Hour == 24 && (this.Minute != 0 || this.Second != 0)))
			{
				return false;
			}
			start += DateTimeParser.LzHH_mm_ss;
			if (this.ParseChar(start, '.'))
			{
				this.Fraction = 0;
				int num = 0;
				for (;;)
				{
					int num2 = start + 1;
					start = num2;
					if (num2 >= this._end || num >= 7)
					{
						break;
					}
					int num3 = (int)(this._text[start] - '0');
					if (num3 < 0 || num3 > 9)
					{
						break;
					}
					this.Fraction = this.Fraction * 10 + num3;
					num++;
				}
				if (num < 7)
				{
					if (num == 0)
					{
						return false;
					}
					this.Fraction *= DateTimeParser.Power10[7 - num];
				}
				if (this.Hour == 24 && this.Fraction != 0)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0000ABCC File Offset: 0x00008DCC
		private bool ParseZone(int start)
		{
			if (start < this._end)
			{
				char c = this._text[start];
				if (c == 'Z' || c == 'z')
				{
					this.Zone = ParserTimeZone.Utc;
					start++;
				}
				else
				{
					if (start + 2 < this._end && this.Parse2Digit(start + DateTimeParser.Lz_, out this.ZoneHour) && this.ZoneHour <= 99)
					{
						if (c != '+')
						{
							if (c == '-')
							{
								this.Zone = ParserTimeZone.LocalWestOfUtc;
								start += DateTimeParser.Lz_zz;
							}
						}
						else
						{
							this.Zone = ParserTimeZone.LocalEastOfUtc;
							start += DateTimeParser.Lz_zz;
						}
					}
					if (start < this._end)
					{
						if (this.ParseChar(start, ':'))
						{
							start++;
							if (start + 1 < this._end && this.Parse2Digit(start, out this.ZoneMinute) && this.ZoneMinute <= 99)
							{
								start += 2;
							}
						}
						else if (start + 1 < this._end && this.Parse2Digit(start, out this.ZoneMinute) && this.ZoneMinute <= 99)
						{
							start += 2;
						}
					}
				}
			}
			return start == this._end;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0000ACD8 File Offset: 0x00008ED8
		private bool Parse4Digit(int start, out int num)
		{
			if (start + 3 < this._end)
			{
				int num2 = (int)(this._text[start] - '0');
				int num3 = (int)(this._text[start + 1] - '0');
				int num4 = (int)(this._text[start + 2] - '0');
				int num5 = (int)(this._text[start + 3] - '0');
				if (0 <= num2 && num2 < 10 && 0 <= num3 && num3 < 10 && 0 <= num4 && num4 < 10 && 0 <= num5 && num5 < 10)
				{
					num = ((num2 * 10 + num3) * 10 + num4) * 10 + num5;
					return true;
				}
			}
			num = 0;
			return false;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000AD64 File Offset: 0x00008F64
		private bool Parse2Digit(int start, out int num)
		{
			if (start + 1 < this._end)
			{
				int num2 = (int)(this._text[start] - '0');
				int num3 = (int)(this._text[start + 1] - '0');
				if (0 <= num2 && num2 < 10 && 0 <= num3 && num3 < 10)
				{
					num = num2 * 10 + num3;
					return true;
				}
			}
			num = 0;
			return false;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000ADB6 File Offset: 0x00008FB6
		private bool ParseChar(int start, char ch)
		{
			return start < this._end && this._text[start] == ch;
		}

		// Token: 0x0400013C RID: 316
		public int Year;

		// Token: 0x0400013D RID: 317
		public int Month;

		// Token: 0x0400013E RID: 318
		public int Day;

		// Token: 0x0400013F RID: 319
		public int Hour;

		// Token: 0x04000140 RID: 320
		public int Minute;

		// Token: 0x04000141 RID: 321
		public int Second;

		// Token: 0x04000142 RID: 322
		public int Fraction;

		// Token: 0x04000143 RID: 323
		public int ZoneHour;

		// Token: 0x04000144 RID: 324
		public int ZoneMinute;

		// Token: 0x04000145 RID: 325
		public ParserTimeZone Zone;

		// Token: 0x04000146 RID: 326
		private char[] _text;

		// Token: 0x04000147 RID: 327
		private int _end;

		// Token: 0x04000148 RID: 328
		private static readonly int[] Power10 = new int[]
		{
			-1,
			10,
			100,
			1000,
			10000,
			100000,
			1000000
		};

		// Token: 0x04000149 RID: 329
		private static readonly int Lzyyyy = "yyyy".Length;

		// Token: 0x0400014A RID: 330
		private static readonly int Lzyyyy_ = "yyyy-".Length;

		// Token: 0x0400014B RID: 331
		private static readonly int Lzyyyy_MM = "yyyy-MM".Length;

		// Token: 0x0400014C RID: 332
		private static readonly int Lzyyyy_MM_ = "yyyy-MM-".Length;

		// Token: 0x0400014D RID: 333
		private static readonly int Lzyyyy_MM_dd = "yyyy-MM-dd".Length;

		// Token: 0x0400014E RID: 334
		private static readonly int Lzyyyy_MM_ddT = "yyyy-MM-ddT".Length;

		// Token: 0x0400014F RID: 335
		private static readonly int LzHH = "HH".Length;

		// Token: 0x04000150 RID: 336
		private static readonly int LzHH_ = "HH:".Length;

		// Token: 0x04000151 RID: 337
		private static readonly int LzHH_mm = "HH:mm".Length;

		// Token: 0x04000152 RID: 338
		private static readonly int LzHH_mm_ = "HH:mm:".Length;

		// Token: 0x04000153 RID: 339
		private static readonly int LzHH_mm_ss = "HH:mm:ss".Length;

		// Token: 0x04000154 RID: 340
		private static readonly int Lz_ = "-".Length;

		// Token: 0x04000155 RID: 341
		private static readonly int Lz_zz = "-zz".Length;

		// Token: 0x04000156 RID: 342
		private const short MaxFractionDigits = 7;
	}
}
