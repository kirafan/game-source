﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000054 RID: 84
	[Preserve]
	internal static class BufferUtils
	{
		// Token: 0x06000382 RID: 898 RVA: 0x0000E6CD File Offset: 0x0000C8CD
		public static char[] RentBuffer(IArrayPool<char> bufferPool, int minSize)
		{
			if (bufferPool == null)
			{
				return new char[minSize];
			}
			return bufferPool.Rent(minSize);
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000E6E0 File Offset: 0x0000C8E0
		public static void ReturnBuffer(IArrayPool<char> bufferPool, char[] buffer)
		{
			if (bufferPool == null)
			{
				return;
			}
			bufferPool.Return(buffer);
		}

		// Token: 0x06000384 RID: 900 RVA: 0x0000E6ED File Offset: 0x0000C8ED
		public static char[] EnsureBufferSize(IArrayPool<char> bufferPool, int size, char[] buffer)
		{
			if (bufferPool == null)
			{
				return new char[size];
			}
			if (buffer != null)
			{
				bufferPool.Return(buffer);
			}
			return bufferPool.Rent(size);
		}
	}
}
