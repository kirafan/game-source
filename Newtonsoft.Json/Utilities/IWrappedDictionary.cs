﻿using System;
using System.Collections;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000050 RID: 80
	[Preserve]
	internal interface IWrappedDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000354 RID: 852
		object UnderlyingDictionary { get; }
	}
}
