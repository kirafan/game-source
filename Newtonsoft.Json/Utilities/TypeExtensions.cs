﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x0200005D RID: 93
	[Preserve]
	internal static class TypeExtensions
	{
		// Token: 0x060003F5 RID: 1013 RVA: 0x00010750 File Offset: 0x0000E950
		public static MethodInfo Method(this Delegate d)
		{
			return d.Method;
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x00010758 File Offset: 0x0000E958
		public static MemberTypes MemberType(this MemberInfo memberInfo)
		{
			return memberInfo.MemberType;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x00010760 File Offset: 0x0000E960
		public static bool ContainsGenericParameters(this Type type)
		{
			return type.ContainsGenericParameters;
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00010768 File Offset: 0x0000E968
		public static bool IsInterface(this Type type)
		{
			return type.IsInterface;
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00010770 File Offset: 0x0000E970
		public static bool IsGenericType(this Type type)
		{
			return type.IsGenericType;
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x00010778 File Offset: 0x0000E978
		public static bool IsGenericTypeDefinition(this Type type)
		{
			return type.IsGenericTypeDefinition;
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x00010780 File Offset: 0x0000E980
		public static Type BaseType(this Type type)
		{
			return type.BaseType;
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x00010788 File Offset: 0x0000E988
		public static Assembly Assembly(this Type type)
		{
			return type.Assembly;
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x00010790 File Offset: 0x0000E990
		public static bool IsEnum(this Type type)
		{
			return type.IsEnum;
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x00010798 File Offset: 0x0000E998
		public static bool IsClass(this Type type)
		{
			return type.IsClass;
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x000107A0 File Offset: 0x0000E9A0
		public static bool IsSealed(this Type type)
		{
			return type.IsSealed;
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x000107A8 File Offset: 0x0000E9A8
		public static bool IsAbstract(this Type type)
		{
			return type.IsAbstract;
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x000107B0 File Offset: 0x0000E9B0
		public static bool IsVisible(this Type type)
		{
			return type.IsVisible;
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x000107B8 File Offset: 0x0000E9B8
		public static bool IsValueType(this Type type)
		{
			return type.IsValueType;
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x000107C0 File Offset: 0x0000E9C0
		public static bool AssignableToTypeName(this Type type, string fullTypeName, out Type match)
		{
			for (Type type2 = type; type2 != null; type2 = type2.BaseType())
			{
				if (string.Equals(type2.FullName, fullTypeName, StringComparison.Ordinal))
				{
					match = type2;
					return true;
				}
			}
			Type[] interfaces = type.GetInterfaces();
			for (int i = 0; i < interfaces.Length; i++)
			{
				if (string.Equals(interfaces[i].Name, fullTypeName, StringComparison.Ordinal))
				{
					match = type;
					return true;
				}
			}
			match = null;
			return false;
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x00010820 File Offset: 0x0000EA20
		public static bool AssignableToTypeName(this Type type, string fullTypeName)
		{
			Type type2;
			return type.AssignableToTypeName(fullTypeName, out type2);
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x00010838 File Offset: 0x0000EA38
		public static bool ImplementInterface(this Type type, Type interfaceType)
		{
			for (Type type2 = type; type2 != null; type2 = type2.BaseType())
			{
				foreach (Type type3 in ((IEnumerable<Type>)type2.GetInterfaces()))
				{
					if (type3 == interfaceType || (type3 != null && type3.ImplementInterface(interfaceType)))
					{
						return true;
					}
				}
			}
			return false;
		}
	}
}
