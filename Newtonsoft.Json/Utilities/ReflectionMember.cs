﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000043 RID: 67
	[Preserve]
	internal class ReflectionMember
	{
		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060002DC RID: 732 RVA: 0x0000B4E6 File Offset: 0x000096E6
		// (set) Token: 0x060002DD RID: 733 RVA: 0x0000B4EE File Offset: 0x000096EE
		public Type MemberType { get; set; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060002DE RID: 734 RVA: 0x0000B4F7 File Offset: 0x000096F7
		// (set) Token: 0x060002DF RID: 735 RVA: 0x0000B4FF File Offset: 0x000096FF
		public Func<object, object> Getter { get; set; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x0000B508 File Offset: 0x00009708
		// (set) Token: 0x060002E1 RID: 737 RVA: 0x0000B510 File Offset: 0x00009710
		public Action<object, object> Setter { get; set; }
	}
}
