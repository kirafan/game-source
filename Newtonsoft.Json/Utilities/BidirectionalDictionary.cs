﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000048 RID: 72
	[Preserve]
	internal class BidirectionalDictionary<TFirst, TSecond>
	{
		// Token: 0x060002F9 RID: 761 RVA: 0x0000B9F0 File Offset: 0x00009BF0
		public BidirectionalDictionary() : this(EqualityComparer<TFirst>.Default, EqualityComparer<TSecond>.Default)
		{
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000BA02 File Offset: 0x00009C02
		public BidirectionalDictionary(IEqualityComparer<TFirst> firstEqualityComparer, IEqualityComparer<TSecond> secondEqualityComparer) : this(firstEqualityComparer, secondEqualityComparer, "Duplicate item already exists for '{0}'.", "Duplicate item already exists for '{0}'.")
		{
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000BA16 File Offset: 0x00009C16
		public BidirectionalDictionary(IEqualityComparer<TFirst> firstEqualityComparer, IEqualityComparer<TSecond> secondEqualityComparer, string duplicateFirstErrorMessage, string duplicateSecondErrorMessage)
		{
			this._firstToSecond = new Dictionary<TFirst, TSecond>(firstEqualityComparer);
			this._secondToFirst = new Dictionary<TSecond, TFirst>(secondEqualityComparer);
			this._duplicateFirstErrorMessage = duplicateFirstErrorMessage;
			this._duplicateSecondErrorMessage = duplicateSecondErrorMessage;
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000BA48 File Offset: 0x00009C48
		public void Set(TFirst first, TSecond second)
		{
			TSecond tsecond;
			if (this._firstToSecond.TryGetValue(first, out tsecond) && !tsecond.Equals(second))
			{
				throw new ArgumentException(this._duplicateFirstErrorMessage.FormatWith(CultureInfo.InvariantCulture, first));
			}
			TFirst tfirst;
			if (this._secondToFirst.TryGetValue(second, out tfirst) && !tfirst.Equals(first))
			{
				throw new ArgumentException(this._duplicateSecondErrorMessage.FormatWith(CultureInfo.InvariantCulture, second));
			}
			this._firstToSecond.Add(first, second);
			this._secondToFirst.Add(second, first);
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000BAF1 File Offset: 0x00009CF1
		public bool TryGetByFirst(TFirst first, out TSecond second)
		{
			return this._firstToSecond.TryGetValue(first, out second);
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000BB00 File Offset: 0x00009D00
		public bool TryGetBySecond(TSecond second, out TFirst first)
		{
			return this._secondToFirst.TryGetValue(second, out first);
		}

		// Token: 0x0400016D RID: 365
		private readonly IDictionary<TFirst, TSecond> _firstToSecond;

		// Token: 0x0400016E RID: 366
		private readonly IDictionary<TSecond, TFirst> _secondToFirst;

		// Token: 0x0400016F RID: 367
		private readonly string _duplicateFirstErrorMessage;

		// Token: 0x04000170 RID: 368
		private readonly string _duplicateSecondErrorMessage;
	}
}
