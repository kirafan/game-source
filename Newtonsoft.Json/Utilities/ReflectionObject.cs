﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Utilities
{
	// Token: 0x02000044 RID: 68
	[Preserve]
	internal class ReflectionObject
	{
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060002E3 RID: 739 RVA: 0x0000B519 File Offset: 0x00009719
		// (set) Token: 0x060002E4 RID: 740 RVA: 0x0000B521 File Offset: 0x00009721
		public ObjectConstructor<object> Creator { get; private set; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x0000B52A File Offset: 0x0000972A
		// (set) Token: 0x060002E6 RID: 742 RVA: 0x0000B532 File Offset: 0x00009732
		public IDictionary<string, ReflectionMember> Members { get; private set; }

		// Token: 0x060002E7 RID: 743 RVA: 0x0000B53B File Offset: 0x0000973B
		public ReflectionObject()
		{
			this.Members = new Dictionary<string, ReflectionMember>();
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000B54E File Offset: 0x0000974E
		public object GetValue(object target, string member)
		{
			return this.Members[member].Getter(target);
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000B567 File Offset: 0x00009767
		public void SetValue(object target, string member, object value)
		{
			this.Members[member].Setter(target, value);
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000B581 File Offset: 0x00009781
		public Type GetType(string member)
		{
			return this.Members[member].MemberType;
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000B594 File Offset: 0x00009794
		public static ReflectionObject Create(Type t, params string[] memberNames)
		{
			return ReflectionObject.Create(t, null, memberNames);
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000B5A0 File Offset: 0x000097A0
		public static ReflectionObject Create(Type t, MethodBase creator, params string[] memberNames)
		{
			ReflectionObject reflectionObject = new ReflectionObject();
			ReflectionDelegateFactory reflectionDelegateFactory = JsonTypeReflector.ReflectionDelegateFactory;
			if (creator != null)
			{
				reflectionObject.Creator = reflectionDelegateFactory.CreateParameterizedConstructor(creator);
			}
			else if (ReflectionUtils.HasDefaultConstructor(t, false))
			{
				Func<object> ctor = reflectionDelegateFactory.CreateDefaultConstructor<object>(t);
				reflectionObject.Creator = ((object[] args) => ctor());
			}
			int i = 0;
			while (i < memberNames.Length)
			{
				string text = memberNames[i];
				MemberInfo[] member = t.GetMember(text, BindingFlags.Instance | BindingFlags.Public);
				if (member.Length != 1)
				{
					throw new ArgumentException("Expected a single member with the name '{0}'.".FormatWith(CultureInfo.InvariantCulture, text));
				}
				MemberInfo memberInfo = member.Single<MemberInfo>();
				ReflectionMember reflectionMember = new ReflectionMember();
				MemberTypes memberTypes = memberInfo.MemberType();
				if (memberTypes == MemberTypes.Field)
				{
					goto IL_AD;
				}
				if (memberTypes != MemberTypes.Method)
				{
					if (memberTypes == MemberTypes.Property)
					{
						goto IL_AD;
					}
					throw new ArgumentException("Unexpected member type '{0}' for member '{1}'.".FormatWith(CultureInfo.InvariantCulture, memberInfo.MemberType(), memberInfo.Name));
				}
				else
				{
					MethodInfo methodInfo = (MethodInfo)memberInfo;
					if (methodInfo.IsPublic)
					{
						ParameterInfo[] parameters = methodInfo.GetParameters();
						if (parameters.Length == 0 && methodInfo.ReturnType != typeof(void))
						{
							MethodCall<object, object> call = reflectionDelegateFactory.CreateMethodCall<object>(methodInfo);
							reflectionMember.Getter = ((object target) => call(target, new object[0]));
						}
						else if (parameters.Length == 1 && methodInfo.ReturnType == typeof(void))
						{
							MethodCall<object, object> call = reflectionDelegateFactory.CreateMethodCall<object>(methodInfo);
							reflectionMember.Setter = delegate(object target, object arg)
							{
								call(target, new object[]
								{
									arg
								});
							};
						}
					}
				}
				IL_1B8:
				if (ReflectionUtils.CanReadMemberValue(memberInfo, false))
				{
					reflectionMember.Getter = reflectionDelegateFactory.CreateGet<object>(memberInfo);
				}
				if (ReflectionUtils.CanSetMemberValue(memberInfo, false, false))
				{
					reflectionMember.Setter = reflectionDelegateFactory.CreateSet<object>(memberInfo);
				}
				reflectionMember.MemberType = ReflectionUtils.GetMemberUnderlyingType(memberInfo);
				reflectionObject.Members[text] = reflectionMember;
				i++;
				continue;
				IL_AD:
				if (ReflectionUtils.CanReadMemberValue(memberInfo, false))
				{
					reflectionMember.Getter = reflectionDelegateFactory.CreateGet<object>(memberInfo);
				}
				if (ReflectionUtils.CanSetMemberValue(memberInfo, false, false))
				{
					reflectionMember.Setter = reflectionDelegateFactory.CreateSet<object>(memberInfo);
					goto IL_1B8;
				}
				goto IL_1B8;
			}
			return reflectionObject;
		}
	}
}
