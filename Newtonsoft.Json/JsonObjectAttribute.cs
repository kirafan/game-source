﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000023 RID: 35
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, AllowMultiple = false)]
	[Preserve]
	public sealed class JsonObjectAttribute : JsonContainerAttribute
	{
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000065 RID: 101 RVA: 0x00002685 File Offset: 0x00000885
		// (set) Token: 0x06000066 RID: 102 RVA: 0x0000268D File Offset: 0x0000088D
		public MemberSerialization MemberSerialization
		{
			get
			{
				return this._memberSerialization;
			}
			set
			{
				this._memberSerialization = value;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00002698 File Offset: 0x00000898
		// (set) Token: 0x06000068 RID: 104 RVA: 0x000026BE File Offset: 0x000008BE
		public Required ItemRequired
		{
			get
			{
				Required? itemRequired = this._itemRequired;
				if (itemRequired == null)
				{
					return Required.Default;
				}
				return itemRequired.GetValueOrDefault();
			}
			set
			{
				this._itemRequired = new Required?(value);
			}
		}

		// Token: 0x06000069 RID: 105 RVA: 0x000021CA File Offset: 0x000003CA
		public JsonObjectAttribute()
		{
		}

		// Token: 0x0600006A RID: 106 RVA: 0x000026CC File Offset: 0x000008CC
		public JsonObjectAttribute(MemberSerialization memberSerialization)
		{
			this.MemberSerialization = memberSerialization;
		}

		// Token: 0x0600006B RID: 107 RVA: 0x000021D2 File Offset: 0x000003D2
		public JsonObjectAttribute(string id) : base(id)
		{
		}

		// Token: 0x04000056 RID: 86
		private MemberSerialization _memberSerialization;

		// Token: 0x04000057 RID: 87
		internal Required? _itemRequired;
	}
}
