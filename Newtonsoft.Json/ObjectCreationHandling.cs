﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000026 RID: 38
	[Preserve]
	public enum ObjectCreationHandling
	{
		// Token: 0x04000092 RID: 146
		Auto,
		// Token: 0x04000093 RID: 147
		Reuse,
		// Token: 0x04000094 RID: 148
		Replace
	}
}
