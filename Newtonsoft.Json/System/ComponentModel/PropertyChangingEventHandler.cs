﻿using System;
using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	// Token: 0x0200000A RID: 10
	// (Invoke) Token: 0x0600002A RID: 42
	[Preserve]
	public delegate void PropertyChangingEventHandler(object sender, PropertyChangingEventArgs e);
}
