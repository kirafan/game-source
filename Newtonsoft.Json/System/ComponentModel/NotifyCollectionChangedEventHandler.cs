﻿using System;
using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	// Token: 0x02000008 RID: 8
	// (Invoke) Token: 0x06000023 RID: 35
	[Preserve]
	public delegate void NotifyCollectionChangedEventHandler(object sender, NotifyCollectionChangedEventArgs e);
}
