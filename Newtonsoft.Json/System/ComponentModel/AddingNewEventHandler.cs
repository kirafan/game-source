﻿using System;
using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	// Token: 0x02000003 RID: 3
	// (Invoke) Token: 0x06000006 RID: 6
	[Preserve]
	public delegate void AddingNewEventHandler(object sender, AddingNewEventArgs e);
}
