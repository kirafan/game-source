﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	// Token: 0x02000007 RID: 7
	[Preserve]
	public class NotifyCollectionChangedEventArgs
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600000D RID: 13 RVA: 0x00002078 File Offset: 0x00000278
		// (set) Token: 0x0600000E RID: 14 RVA: 0x00002080 File Offset: 0x00000280
		internal NotifyCollectionChangedAction Action { get; set; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600000F RID: 15 RVA: 0x00002089 File Offset: 0x00000289
		// (set) Token: 0x06000010 RID: 16 RVA: 0x00002091 File Offset: 0x00000291
		internal IList NewItems { get; set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000011 RID: 17 RVA: 0x0000209A File Offset: 0x0000029A
		// (set) Token: 0x06000012 RID: 18 RVA: 0x000020A2 File Offset: 0x000002A2
		internal int NewStartingIndex { get; set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000013 RID: 19 RVA: 0x000020AB File Offset: 0x000002AB
		// (set) Token: 0x06000014 RID: 20 RVA: 0x000020B3 File Offset: 0x000002B3
		internal IList OldItems { get; set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000015 RID: 21 RVA: 0x000020BC File Offset: 0x000002BC
		// (set) Token: 0x06000016 RID: 22 RVA: 0x000020C4 File Offset: 0x000002C4
		internal int OldStartingIndex { get; set; }

		// Token: 0x06000017 RID: 23 RVA: 0x000020CD File Offset: 0x000002CD
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action)
		{
			this.Action = action;
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000020DC File Offset: 0x000002DC
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems) : this(action)
		{
			this.NewItems = changedItems;
		}

		// Token: 0x06000019 RID: 25 RVA: 0x000020EC File Offset: 0x000002EC
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem) : this(action)
		{
			this.NewItems = new List<object>
			{
				changedItem
			};
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002107 File Offset: 0x00000307
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems) : this(action, newItems)
		{
			this.OldItems = oldItems;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002118 File Offset: 0x00000318
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int startingIndex) : this(action, changedItems)
		{
			this.NewStartingIndex = startingIndex;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002129 File Offset: 0x00000329
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index) : this(action, changedItem)
		{
			this.NewStartingIndex = index;
		}

		// Token: 0x0600001D RID: 29 RVA: 0x0000213A File Offset: 0x0000033A
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem) : this(action, newItem)
		{
			this.OldItems = new List<object>
			{
				oldItem
			};
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002156 File Offset: 0x00000356
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList newItems, IList oldItems, int startingIndex) : this(action, newItems, oldItems)
		{
			this.NewStartingIndex = startingIndex;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002169 File Offset: 0x00000369
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, IList changedItems, int index, int oldIndex) : this(action, changedItems, index)
		{
			this.OldStartingIndex = oldIndex;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x0000217C File Offset: 0x0000037C
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object changedItem, int index, int oldIndex) : this(action, changedItem, index)
		{
			this.OldStartingIndex = oldIndex;
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000218F File Offset: 0x0000038F
		internal NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction action, object newItem, object oldItem, int index) : this(action, newItem, oldItem)
		{
			this.NewStartingIndex = index;
		}
	}
}
