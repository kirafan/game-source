﻿using System;
using Newtonsoft.Json.Shims;

namespace System.ComponentModel
{
	// Token: 0x02000009 RID: 9
	[Preserve]
	public class PropertyChangingEventArgs : EventArgs
	{
		// Token: 0x06000026 RID: 38 RVA: 0x000021A2 File Offset: 0x000003A2
		public PropertyChangingEventArgs(string propertyName)
		{
			this.PropertyName = propertyName;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000027 RID: 39 RVA: 0x000021B1 File Offset: 0x000003B1
		// (set) Token: 0x06000028 RID: 40 RVA: 0x000021B9 File Offset: 0x000003B9
		public virtual string PropertyName { get; set; }
	}
}
