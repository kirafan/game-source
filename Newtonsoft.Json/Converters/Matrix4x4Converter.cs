﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B8 RID: 184
	public class Matrix4x4Converter : JsonConverter
	{
		// Token: 0x060008F2 RID: 2290 RVA: 0x00021CAC File Offset: 0x0001FEAC
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			Matrix4x4 matrix4x = (Matrix4x4)value;
			writer.WriteStartObject();
			writer.WritePropertyName("m00");
			writer.WriteValue(matrix4x.m00);
			writer.WritePropertyName("m01");
			writer.WriteValue(matrix4x.m01);
			writer.WritePropertyName("m02");
			writer.WriteValue(matrix4x.m02);
			writer.WritePropertyName("m03");
			writer.WriteValue(matrix4x.m03);
			writer.WritePropertyName("m10");
			writer.WriteValue(matrix4x.m10);
			writer.WritePropertyName("m11");
			writer.WriteValue(matrix4x.m11);
			writer.WritePropertyName("m12");
			writer.WriteValue(matrix4x.m12);
			writer.WritePropertyName("m13");
			writer.WriteValue(matrix4x.m13);
			writer.WritePropertyName("m20");
			writer.WriteValue(matrix4x.m20);
			writer.WritePropertyName("m21");
			writer.WriteValue(matrix4x.m21);
			writer.WritePropertyName("m22");
			writer.WriteValue(matrix4x.m22);
			writer.WritePropertyName("m23");
			writer.WriteValue(matrix4x.m23);
			writer.WritePropertyName("m30");
			writer.WriteValue(matrix4x.m30);
			writer.WritePropertyName("m31");
			writer.WriteValue(matrix4x.m31);
			writer.WritePropertyName("m32");
			writer.WriteValue(matrix4x.m32);
			writer.WritePropertyName("m33");
			writer.WriteValue(matrix4x.m33);
			writer.WriteEnd();
		}

		// Token: 0x060008F3 RID: 2291 RVA: 0x00021E48 File Offset: 0x00020048
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				return default(Matrix4x4);
			}
			JObject jobject = JObject.Load(reader);
			return new Matrix4x4
			{
				m00 = (float)jobject["m00"],
				m01 = (float)jobject["m01"],
				m02 = (float)jobject["m02"],
				m03 = (float)jobject["m03"],
				m20 = (float)jobject["m20"],
				m21 = (float)jobject["m21"],
				m22 = (float)jobject["m22"],
				m23 = (float)jobject["m23"],
				m30 = (float)jobject["m30"],
				m31 = (float)jobject["m31"],
				m32 = (float)jobject["m32"],
				m33 = (float)jobject["m33"]
			};
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x060008F4 RID: 2292 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060008F5 RID: 2293 RVA: 0x00021FA3 File Offset: 0x000201A3
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Matrix4x4);
		}
	}
}
