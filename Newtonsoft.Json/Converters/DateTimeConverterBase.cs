﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B3 RID: 179
	[Preserve]
	public abstract class DateTimeConverterBase : JsonConverter
	{
		// Token: 0x060008DB RID: 2267 RVA: 0x000217F2 File Offset: 0x0001F9F2
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime) || objectType == typeof(DateTime?) || (objectType == typeof(DateTimeOffset) || objectType == typeof(DateTimeOffset?));
		}
	}
}
