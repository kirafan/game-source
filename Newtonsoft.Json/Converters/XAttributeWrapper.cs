﻿using System;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D4 RID: 212
	internal class XAttributeWrapper : XObjectWrapper
	{
		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x060009C5 RID: 2501 RVA: 0x00023A64 File Offset: 0x00021C64
		private XAttribute Attribute
		{
			get
			{
				return (XAttribute)base.WrappedNode;
			}
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x000237EA File Offset: 0x000219EA
		public XAttributeWrapper(XAttribute attribute) : base(attribute)
		{
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x060009C7 RID: 2503 RVA: 0x00023A71 File Offset: 0x00021C71
		// (set) Token: 0x060009C8 RID: 2504 RVA: 0x00023A7E File Offset: 0x00021C7E
		public override string Value
		{
			get
			{
				return this.Attribute.Value;
			}
			set
			{
				this.Attribute.Value = value;
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060009C9 RID: 2505 RVA: 0x00023A8C File Offset: 0x00021C8C
		public override string LocalName
		{
			get
			{
				return this.Attribute.Name.LocalName;
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060009CA RID: 2506 RVA: 0x00023A9E File Offset: 0x00021C9E
		public override string NamespaceUri
		{
			get
			{
				return this.Attribute.Name.NamespaceName;
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060009CB RID: 2507 RVA: 0x00023AB0 File Offset: 0x00021CB0
		public override IXmlNode ParentNode
		{
			get
			{
				if (this.Attribute.Parent == null)
				{
					return null;
				}
				return XContainerWrapper.WrapNode(this.Attribute.Parent);
			}
		}
	}
}
