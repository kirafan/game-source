﻿using System;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C9 RID: 201
	internal interface IXmlDocumentType : IXmlNode
	{
		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06000973 RID: 2419
		string Name { get; }

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000974 RID: 2420
		string System { get; }

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000975 RID: 2421
		string Public { get; }

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000976 RID: 2422
		string InternalSubset { get; }
	}
}
