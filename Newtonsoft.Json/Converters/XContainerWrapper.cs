﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D2 RID: 210
	internal class XContainerWrapper : XObjectWrapper
	{
		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x060009B3 RID: 2483 RVA: 0x000238AD File Offset: 0x00021AAD
		private XContainer Container
		{
			get
			{
				return (XContainer)base.WrappedNode;
			}
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x000237EA File Offset: 0x000219EA
		public XContainerWrapper(XContainer container) : base(container)
		{
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x060009B5 RID: 2485 RVA: 0x000238BC File Offset: 0x00021ABC
		public override List<IXmlNode> ChildNodes
		{
			get
			{
				if (this._childNodes == null)
				{
					this._childNodes = new List<IXmlNode>();
					foreach (XNode node in this.Container.Nodes())
					{
						this._childNodes.Add(XContainerWrapper.WrapNode(node));
					}
				}
				return this._childNodes;
			}
		}

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x060009B6 RID: 2486 RVA: 0x00023934 File Offset: 0x00021B34
		public override IXmlNode ParentNode
		{
			get
			{
				if (this.Container.Parent == null)
				{
					return null;
				}
				return XContainerWrapper.WrapNode(this.Container.Parent);
			}
		}

		// Token: 0x060009B7 RID: 2487 RVA: 0x00023958 File Offset: 0x00021B58
		internal static IXmlNode WrapNode(XObject node)
		{
			if (node is XDocument)
			{
				return new XDocumentWrapper((XDocument)node);
			}
			if (node is XElement)
			{
				return new XElementWrapper((XElement)node);
			}
			if (node is XContainer)
			{
				return new XContainerWrapper((XContainer)node);
			}
			if (node is XProcessingInstruction)
			{
				return new XProcessingInstructionWrapper((XProcessingInstruction)node);
			}
			if (node is XText)
			{
				return new XTextWrapper((XText)node);
			}
			if (node is XComment)
			{
				return new XCommentWrapper((XComment)node);
			}
			if (node is XAttribute)
			{
				return new XAttributeWrapper((XAttribute)node);
			}
			if (node is XDocumentType)
			{
				return new XDocumentTypeWrapper((XDocumentType)node);
			}
			return new XObjectWrapper(node);
		}

		// Token: 0x060009B8 RID: 2488 RVA: 0x00023A0B File Offset: 0x00021C0B
		public override IXmlNode AppendChild(IXmlNode newChild)
		{
			this.Container.Add(newChild.WrappedNode);
			this._childNodes = null;
			return newChild;
		}

		// Token: 0x040002FC RID: 764
		private List<IXmlNode> _childNodes;
	}
}
