﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B1 RID: 177
	public class ColorConverter : JsonConverter
	{
		// Token: 0x060008D0 RID: 2256 RVA: 0x00021614 File Offset: 0x0001F814
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			Color color = (Color)value;
			writer.WriteStartObject();
			writer.WritePropertyName("a");
			writer.WriteValue(color.a);
			writer.WritePropertyName("r");
			writer.WriteValue(color.r);
			writer.WritePropertyName("g");
			writer.WriteValue(color.g);
			writer.WritePropertyName("b");
			writer.WriteValue(color.b);
			writer.WriteEndObject();
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x0002169A File Offset: 0x0001F89A
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Color) || objectType == typeof(Color32);
		}

		// Token: 0x060008D2 RID: 2258 RVA: 0x000216B8 File Offset: 0x0001F8B8
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				return default(Color);
			}
			JObject jobject = JObject.Load(reader);
			if (objectType == typeof(Color32))
			{
				return new Color32((byte)jobject["r"], (byte)jobject["g"], (byte)jobject["b"], (byte)jobject["a"]);
			}
			return new Color((float)jobject["r"], (float)jobject["g"], (float)jobject["b"], (float)jobject["a"]);
		}

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x060008D3 RID: 2259 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}
	}
}
