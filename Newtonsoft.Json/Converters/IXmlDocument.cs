﻿using System;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C7 RID: 199
	internal interface IXmlDocument : IXmlNode
	{
		// Token: 0x06000961 RID: 2401
		IXmlNode CreateComment(string text);

		// Token: 0x06000962 RID: 2402
		IXmlNode CreateTextNode(string text);

		// Token: 0x06000963 RID: 2403
		IXmlNode CreateCDataSection(string data);

		// Token: 0x06000964 RID: 2404
		IXmlNode CreateWhitespace(string text);

		// Token: 0x06000965 RID: 2405
		IXmlNode CreateSignificantWhitespace(string text);

		// Token: 0x06000966 RID: 2406
		IXmlNode CreateXmlDeclaration(string version, string encoding, string standalone);

		// Token: 0x06000967 RID: 2407
		IXmlNode CreateXmlDocumentType(string name, string publicId, string systemId, string internalSubset);

		// Token: 0x06000968 RID: 2408
		IXmlNode CreateProcessingInstruction(string target, string data);

		// Token: 0x06000969 RID: 2409
		IXmlElement CreateElement(string elementName);

		// Token: 0x0600096A RID: 2410
		IXmlElement CreateElement(string qualifiedName, string namespaceUri);

		// Token: 0x0600096B RID: 2411
		IXmlNode CreateAttribute(string name, string value);

		// Token: 0x0600096C RID: 2412
		IXmlNode CreateAttribute(string qualifiedName, string namespaceUri, string value);

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x0600096D RID: 2413
		IXmlElement DocumentElement { get; }
	}
}
