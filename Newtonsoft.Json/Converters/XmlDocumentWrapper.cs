﻿using System;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C2 RID: 194
	internal class XmlDocumentWrapper : XmlNodeWrapper, IXmlDocument, IXmlNode
	{
		// Token: 0x06000937 RID: 2359 RVA: 0x00023157 File Offset: 0x00021357
		public XmlDocumentWrapper(XmlDocument document) : base(document)
		{
			this._document = document;
		}

		// Token: 0x06000938 RID: 2360 RVA: 0x00023167 File Offset: 0x00021367
		public IXmlNode CreateComment(string data)
		{
			return new XmlNodeWrapper(this._document.CreateComment(data));
		}

		// Token: 0x06000939 RID: 2361 RVA: 0x0002317A File Offset: 0x0002137A
		public IXmlNode CreateTextNode(string text)
		{
			return new XmlNodeWrapper(this._document.CreateTextNode(text));
		}

		// Token: 0x0600093A RID: 2362 RVA: 0x0002318D File Offset: 0x0002138D
		public IXmlNode CreateCDataSection(string data)
		{
			return new XmlNodeWrapper(this._document.CreateCDataSection(data));
		}

		// Token: 0x0600093B RID: 2363 RVA: 0x000231A0 File Offset: 0x000213A0
		public IXmlNode CreateWhitespace(string text)
		{
			return new XmlNodeWrapper(this._document.CreateWhitespace(text));
		}

		// Token: 0x0600093C RID: 2364 RVA: 0x000231B3 File Offset: 0x000213B3
		public IXmlNode CreateSignificantWhitespace(string text)
		{
			return new XmlNodeWrapper(this._document.CreateSignificantWhitespace(text));
		}

		// Token: 0x0600093D RID: 2365 RVA: 0x000231C6 File Offset: 0x000213C6
		public IXmlNode CreateXmlDeclaration(string version, string encoding, string standalone)
		{
			return new XmlDeclarationWrapper(this._document.CreateXmlDeclaration(version, encoding, standalone));
		}

		// Token: 0x0600093E RID: 2366 RVA: 0x000231DB File Offset: 0x000213DB
		public IXmlNode CreateXmlDocumentType(string name, string publicId, string systemId, string internalSubset)
		{
			return new XmlDocumentTypeWrapper(this._document.CreateDocumentType(name, publicId, systemId, null));
		}

		// Token: 0x0600093F RID: 2367 RVA: 0x000231F1 File Offset: 0x000213F1
		public IXmlNode CreateProcessingInstruction(string target, string data)
		{
			return new XmlNodeWrapper(this._document.CreateProcessingInstruction(target, data));
		}

		// Token: 0x06000940 RID: 2368 RVA: 0x00023205 File Offset: 0x00021405
		public IXmlElement CreateElement(string elementName)
		{
			return new XmlElementWrapper(this._document.CreateElement(elementName));
		}

		// Token: 0x06000941 RID: 2369 RVA: 0x00023218 File Offset: 0x00021418
		public IXmlElement CreateElement(string qualifiedName, string namespaceUri)
		{
			return new XmlElementWrapper(this._document.CreateElement(qualifiedName, namespaceUri));
		}

		// Token: 0x06000942 RID: 2370 RVA: 0x0002322C File Offset: 0x0002142C
		public IXmlNode CreateAttribute(string name, string value)
		{
			return new XmlNodeWrapper(this._document.CreateAttribute(name))
			{
				Value = value
			};
		}

		// Token: 0x06000943 RID: 2371 RVA: 0x00023246 File Offset: 0x00021446
		public IXmlNode CreateAttribute(string qualifiedName, string namespaceUri, string value)
		{
			return new XmlNodeWrapper(this._document.CreateAttribute(qualifiedName, namespaceUri))
			{
				Value = value
			};
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000944 RID: 2372 RVA: 0x00023261 File Offset: 0x00021461
		public IXmlElement DocumentElement
		{
			get
			{
				if (this._document.DocumentElement == null)
				{
					return null;
				}
				return new XmlElementWrapper(this._document.DocumentElement);
			}
		}

		// Token: 0x040002F3 RID: 755
		private readonly XmlDocument _document;
	}
}
