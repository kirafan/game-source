﻿using System;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CD RID: 205
	internal class XDocumentTypeWrapper : XObjectWrapper, IXmlDocumentType, IXmlNode
	{
		// Token: 0x0600098D RID: 2445 RVA: 0x0002363A File Offset: 0x0002183A
		public XDocumentTypeWrapper(XDocumentType documentType) : base(documentType)
		{
			this._documentType = documentType;
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x0002364A File Offset: 0x0002184A
		public string Name
		{
			get
			{
				return this._documentType.Name;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x00023657 File Offset: 0x00021857
		public string System
		{
			get
			{
				return this._documentType.SystemId;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000990 RID: 2448 RVA: 0x00023664 File Offset: 0x00021864
		public string Public
		{
			get
			{
				return this._documentType.PublicId;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x00023671 File Offset: 0x00021871
		public string InternalSubset
		{
			get
			{
				return this._documentType.InternalSubset;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000992 RID: 2450 RVA: 0x00023371 File Offset: 0x00021571
		public override string LocalName
		{
			get
			{
				return "DOCTYPE";
			}
		}

		// Token: 0x040002FB RID: 763
		private readonly XDocumentType _documentType;
	}
}
