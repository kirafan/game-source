﻿using System;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C5 RID: 197
	internal class XmlDocumentTypeWrapper : XmlNodeWrapper, IXmlDocumentType, IXmlNode
	{
		// Token: 0x0600094F RID: 2383 RVA: 0x0002332D File Offset: 0x0002152D
		public XmlDocumentTypeWrapper(XmlDocumentType documentType) : base(documentType)
		{
			this._documentType = documentType;
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000950 RID: 2384 RVA: 0x0002333D File Offset: 0x0002153D
		public string Name
		{
			get
			{
				return this._documentType.Name;
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000951 RID: 2385 RVA: 0x0002334A File Offset: 0x0002154A
		public string System
		{
			get
			{
				return this._documentType.SystemId;
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000952 RID: 2386 RVA: 0x00023357 File Offset: 0x00021557
		public string Public
		{
			get
			{
				return this._documentType.PublicId;
			}
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000953 RID: 2387 RVA: 0x00023364 File Offset: 0x00021564
		public string InternalSubset
		{
			get
			{
				return this._documentType.InternalSubset;
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000954 RID: 2388 RVA: 0x00023371 File Offset: 0x00021571
		public override string LocalName
		{
			get
			{
				return "DOCTYPE";
			}
		}

		// Token: 0x040002F6 RID: 758
		private readonly XmlDocumentType _documentType;
	}
}
