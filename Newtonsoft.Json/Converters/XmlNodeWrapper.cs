﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C6 RID: 198
	internal class XmlNodeWrapper : IXmlNode
	{
		// Token: 0x06000955 RID: 2389 RVA: 0x00023378 File Offset: 0x00021578
		public XmlNodeWrapper(XmlNode node)
		{
			this._node = node;
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000956 RID: 2390 RVA: 0x00023387 File Offset: 0x00021587
		public object WrappedNode
		{
			get
			{
				return this._node;
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000957 RID: 2391 RVA: 0x0002338F File Offset: 0x0002158F
		public XmlNodeType NodeType
		{
			get
			{
				return this._node.NodeType;
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000958 RID: 2392 RVA: 0x0002339C File Offset: 0x0002159C
		public virtual string LocalName
		{
			get
			{
				return this._node.LocalName;
			}
		}

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06000959 RID: 2393 RVA: 0x000233AC File Offset: 0x000215AC
		public List<IXmlNode> ChildNodes
		{
			get
			{
				if (this._childNodes == null)
				{
					this._childNodes = new List<IXmlNode>(this._node.ChildNodes.Count);
					foreach (object obj in this._node.ChildNodes)
					{
						XmlNode node = (XmlNode)obj;
						this._childNodes.Add(XmlNodeWrapper.WrapNode(node));
					}
				}
				return this._childNodes;
			}
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x00023440 File Offset: 0x00021640
		internal static IXmlNode WrapNode(XmlNode node)
		{
			XmlNodeType nodeType = node.NodeType;
			if (nodeType == XmlNodeType.Element)
			{
				return new XmlElementWrapper((XmlElement)node);
			}
			if (nodeType == XmlNodeType.DocumentType)
			{
				return new XmlDocumentTypeWrapper((XmlDocumentType)node);
			}
			if (nodeType != XmlNodeType.XmlDeclaration)
			{
				return new XmlNodeWrapper(node);
			}
			return new XmlDeclarationWrapper((XmlDeclaration)node);
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x0600095B RID: 2395 RVA: 0x00023490 File Offset: 0x00021690
		public List<IXmlNode> Attributes
		{
			get
			{
				if (this._node.Attributes == null)
				{
					return null;
				}
				if (this._attributes == null)
				{
					this._attributes = new List<IXmlNode>(this._node.Attributes.Count);
					foreach (object obj in this._node.Attributes)
					{
						XmlAttribute node = (XmlAttribute)obj;
						this._attributes.Add(XmlNodeWrapper.WrapNode(node));
					}
				}
				return this._attributes;
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x0600095C RID: 2396 RVA: 0x00023530 File Offset: 0x00021730
		public IXmlNode ParentNode
		{
			get
			{
				XmlNode xmlNode = (this._node is XmlAttribute) ? ((XmlAttribute)this._node).OwnerElement : this._node.ParentNode;
				if (xmlNode == null)
				{
					return null;
				}
				return XmlNodeWrapper.WrapNode(xmlNode);
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x0600095D RID: 2397 RVA: 0x00023573 File Offset: 0x00021773
		// (set) Token: 0x0600095E RID: 2398 RVA: 0x00023580 File Offset: 0x00021780
		public string Value
		{
			get
			{
				return this._node.Value;
			}
			set
			{
				this._node.Value = value;
			}
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x00023590 File Offset: 0x00021790
		public IXmlNode AppendChild(IXmlNode newChild)
		{
			XmlNodeWrapper xmlNodeWrapper = (XmlNodeWrapper)newChild;
			this._node.AppendChild(xmlNodeWrapper._node);
			this._childNodes = null;
			this._attributes = null;
			return newChild;
		}

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000960 RID: 2400 RVA: 0x000235C5 File Offset: 0x000217C5
		public string NamespaceUri
		{
			get
			{
				return this._node.NamespaceURI;
			}
		}

		// Token: 0x040002F7 RID: 759
		private readonly XmlNode _node;

		// Token: 0x040002F8 RID: 760
		private List<IXmlNode> _childNodes;

		// Token: 0x040002F9 RID: 761
		private List<IXmlNode> _attributes;
	}
}
