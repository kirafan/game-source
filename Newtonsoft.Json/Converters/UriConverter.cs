﻿using System;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000BD RID: 189
	public class UriConverter : JsonConverter
	{
		// Token: 0x06000913 RID: 2323 RVA: 0x00022804 File Offset: 0x00020A04
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Uri);
		}

		// Token: 0x06000914 RID: 2324 RVA: 0x00022814 File Offset: 0x00020A14
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JsonToken tokenType = reader.TokenType;
			if (tokenType == JsonToken.String)
			{
				return new Uri((string)reader.Value);
			}
			if (tokenType != JsonToken.Null)
			{
				throw new InvalidOperationException("Unhandled case for UriConverter. Check to see if this converter has been applied to the wrong serialization type.");
			}
			return null;
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x00022854 File Offset: 0x00020A54
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			Uri uri = value as Uri;
			if (uri == null)
			{
				throw new InvalidOperationException("Unhandled case for UriConverter. Check to see if this converter has been applied to the wrong serialization type.");
			}
			writer.WriteValue(uri.OriginalString);
		}
	}
}
