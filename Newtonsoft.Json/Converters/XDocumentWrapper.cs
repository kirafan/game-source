﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CE RID: 206
	internal class XDocumentWrapper : XContainerWrapper, IXmlDocument, IXmlNode
	{
		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x0002367E File Offset: 0x0002187E
		private XDocument Document
		{
			get
			{
				return (XDocument)base.WrappedNode;
			}
		}

		// Token: 0x06000994 RID: 2452 RVA: 0x0002368B File Offset: 0x0002188B
		public XDocumentWrapper(XDocument document) : base(document)
		{
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000995 RID: 2453 RVA: 0x00023694 File Offset: 0x00021894
		public override List<IXmlNode> ChildNodes
		{
			get
			{
				List<IXmlNode> childNodes = base.ChildNodes;
				if (this.Document.Declaration != null && childNodes[0].NodeType != XmlNodeType.XmlDeclaration)
				{
					childNodes.Insert(0, new XDeclarationWrapper(this.Document.Declaration));
				}
				return childNodes;
			}
		}

		// Token: 0x06000996 RID: 2454 RVA: 0x000236DD File Offset: 0x000218DD
		public IXmlNode CreateComment(string text)
		{
			return new XObjectWrapper(new XComment(text));
		}

		// Token: 0x06000997 RID: 2455 RVA: 0x000236EA File Offset: 0x000218EA
		public IXmlNode CreateTextNode(string text)
		{
			return new XObjectWrapper(new XText(text));
		}

		// Token: 0x06000998 RID: 2456 RVA: 0x000236F7 File Offset: 0x000218F7
		public IXmlNode CreateCDataSection(string data)
		{
			return new XObjectWrapper(new XCData(data));
		}

		// Token: 0x06000999 RID: 2457 RVA: 0x000236EA File Offset: 0x000218EA
		public IXmlNode CreateWhitespace(string text)
		{
			return new XObjectWrapper(new XText(text));
		}

		// Token: 0x0600099A RID: 2458 RVA: 0x000236EA File Offset: 0x000218EA
		public IXmlNode CreateSignificantWhitespace(string text)
		{
			return new XObjectWrapper(new XText(text));
		}

		// Token: 0x0600099B RID: 2459 RVA: 0x00023704 File Offset: 0x00021904
		public IXmlNode CreateXmlDeclaration(string version, string encoding, string standalone)
		{
			return new XDeclarationWrapper(new XDeclaration(version, encoding, standalone));
		}

		// Token: 0x0600099C RID: 2460 RVA: 0x00023713 File Offset: 0x00021913
		public IXmlNode CreateXmlDocumentType(string name, string publicId, string systemId, string internalSubset)
		{
			return new XDocumentTypeWrapper(new XDocumentType(name, publicId, systemId, internalSubset));
		}

		// Token: 0x0600099D RID: 2461 RVA: 0x00023724 File Offset: 0x00021924
		public IXmlNode CreateProcessingInstruction(string target, string data)
		{
			return new XProcessingInstructionWrapper(new XProcessingInstruction(target, data));
		}

		// Token: 0x0600099E RID: 2462 RVA: 0x00023732 File Offset: 0x00021932
		public IXmlElement CreateElement(string elementName)
		{
			return new XElementWrapper(new XElement(elementName));
		}

		// Token: 0x0600099F RID: 2463 RVA: 0x00023744 File Offset: 0x00021944
		public IXmlElement CreateElement(string qualifiedName, string namespaceUri)
		{
			return new XElementWrapper(new XElement(XName.Get(MiscellaneousUtils.GetLocalName(qualifiedName), namespaceUri)));
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x0002375C File Offset: 0x0002195C
		public IXmlNode CreateAttribute(string name, string value)
		{
			return new XAttributeWrapper(new XAttribute(name, value));
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x0002376F File Offset: 0x0002196F
		public IXmlNode CreateAttribute(string qualifiedName, string namespaceUri, string value)
		{
			return new XAttributeWrapper(new XAttribute(XName.Get(MiscellaneousUtils.GetLocalName(qualifiedName), namespaceUri), value));
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x060009A2 RID: 2466 RVA: 0x00023788 File Offset: 0x00021988
		public IXmlElement DocumentElement
		{
			get
			{
				if (this.Document.Root == null)
				{
					return null;
				}
				return new XElementWrapper(this.Document.Root);
			}
		}

		// Token: 0x060009A3 RID: 2467 RVA: 0x000237AC File Offset: 0x000219AC
		public override IXmlNode AppendChild(IXmlNode newChild)
		{
			XDeclarationWrapper xdeclarationWrapper = newChild as XDeclarationWrapper;
			if (xdeclarationWrapper != null)
			{
				this.Document.Declaration = xdeclarationWrapper.Declaration;
				return xdeclarationWrapper;
			}
			return base.AppendChild(newChild);
		}
	}
}
