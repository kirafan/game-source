﻿using System;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CA RID: 202
	internal interface IXmlElement : IXmlNode
	{
		// Token: 0x06000977 RID: 2423
		void SetAttributeNode(IXmlNode attribute);

		// Token: 0x06000978 RID: 2424
		string GetPrefixOfNamespace(string namespaceUri);

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000979 RID: 2425
		bool IsEmpty { get; }
	}
}
