﻿using System;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C4 RID: 196
	internal class XmlDeclarationWrapper : XmlNodeWrapper, IXmlDeclaration, IXmlNode
	{
		// Token: 0x06000949 RID: 2377 RVA: 0x000232DA File Offset: 0x000214DA
		public XmlDeclarationWrapper(XmlDeclaration declaration) : base(declaration)
		{
			this._declaration = declaration;
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x0600094A RID: 2378 RVA: 0x000232EA File Offset: 0x000214EA
		public string Version
		{
			get
			{
				return this._declaration.Version;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x0600094B RID: 2379 RVA: 0x000232F7 File Offset: 0x000214F7
		// (set) Token: 0x0600094C RID: 2380 RVA: 0x00023304 File Offset: 0x00021504
		public string Encoding
		{
			get
			{
				return this._declaration.Encoding;
			}
			set
			{
				this._declaration.Encoding = value;
			}
		}

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x0600094D RID: 2381 RVA: 0x00023312 File Offset: 0x00021512
		// (set) Token: 0x0600094E RID: 2382 RVA: 0x0002331F File Offset: 0x0002151F
		public string Standalone
		{
			get
			{
				return this._declaration.Standalone;
			}
			set
			{
				this._declaration.Standalone = value;
			}
		}

		// Token: 0x040002F5 RID: 757
		private readonly XmlDeclaration _declaration;
	}
}
