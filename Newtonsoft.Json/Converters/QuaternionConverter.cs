﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B9 RID: 185
	public class QuaternionConverter : JsonConverter
	{
		// Token: 0x060008F7 RID: 2295 RVA: 0x00021FB4 File Offset: 0x000201B4
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			Quaternion quaternion = (Quaternion)value;
			writer.WriteStartObject();
			writer.WritePropertyName("w");
			writer.WriteValue(quaternion.w);
			writer.WritePropertyName("x");
			writer.WriteValue(quaternion.x);
			writer.WritePropertyName("y");
			writer.WriteValue(quaternion.y);
			writer.WritePropertyName("z");
			writer.WriteValue(quaternion.z);
			writer.WritePropertyName("eulerAngles");
			writer.WriteStartObject();
			writer.WritePropertyName("x");
			writer.WriteValue(quaternion.eulerAngles.x);
			writer.WritePropertyName("y");
			writer.WriteValue(quaternion.eulerAngles.y);
			writer.WritePropertyName("z");
			writer.WriteValue(quaternion.eulerAngles.z);
			writer.WriteEndObject();
			writer.WriteEndObject();
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x0002209E File Offset: 0x0002029E
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Quaternion);
		}

		// Token: 0x060008F9 RID: 2297 RVA: 0x000220B0 File Offset: 0x000202B0
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JObject jobject = JObject.Load(reader);
			List<JProperty> source = jobject.Properties().ToList<JProperty>();
			Quaternion quaternion = default(Quaternion);
			if (source.Any((JProperty p) => p.Name == "w"))
			{
				quaternion.w = (float)jobject["w"];
			}
			if (source.Any((JProperty p) => p.Name == "x"))
			{
				quaternion.x = (float)jobject["x"];
			}
			if (source.Any((JProperty p) => p.Name == "y"))
			{
				quaternion.y = (float)jobject["y"];
			}
			if (source.Any((JProperty p) => p.Name == "z"))
			{
				quaternion.z = (float)jobject["z"];
			}
			if (source.Any((JProperty p) => p.Name == "eulerAngles"))
			{
				JToken jtoken = jobject["eulerAngles"];
				quaternion.eulerAngles = new Vector3
				{
					x = (float)jtoken["x"],
					y = (float)jtoken["y"],
					z = (float)jtoken["z"]
				};
			}
			return quaternion;
		}

		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x060008FA RID: 2298 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}
	}
}
