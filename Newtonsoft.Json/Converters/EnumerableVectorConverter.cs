﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B4 RID: 180
	public class EnumerableVectorConverter<T> : JsonConverter
	{
		// Token: 0x060008DD RID: 2269 RVA: 0x00021830 File Offset: 0x0001FA30
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
			}
			IEnumerable<T> enumerable = value as IEnumerable<T>;
			T[] array = (enumerable != null) ? enumerable.ToArray<T>() : null;
			if (array == null)
			{
				writer.WriteNull();
				return;
			}
			writer.WriteStartArray();
			for (int i = 0; i < array.Length; i++)
			{
				EnumerableVectorConverter<T>.VectorConverter.WriteJson(writer, array[i], serializer);
			}
			writer.WriteEndArray();
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x00021895 File Offset: 0x0001FA95
		public override bool CanConvert(Type objectType)
		{
			return typeof(IEnumerable<Vector2>).IsAssignableFrom(objectType) || typeof(IEnumerable<Vector3>).IsAssignableFrom(objectType) || typeof(IEnumerable<Vector4>).IsAssignableFrom(objectType);
		}

		// Token: 0x060008DF RID: 2271 RVA: 0x000218D0 File Offset: 0x0001FAD0
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				return null;
			}
			List<T> list = new List<T>();
			JObject jobject = JObject.Load(reader);
			for (int i = 0; i < jobject.Count; i++)
			{
				list.Add(JsonConvert.DeserializeObject<T>(jobject[i].ToString()));
			}
			return list;
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x060008E0 RID: 2272 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x040002E1 RID: 737
		private static readonly VectorConverter VectorConverter = new VectorConverter();
	}
}
