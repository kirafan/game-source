﻿using System;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C3 RID: 195
	internal class XmlElementWrapper : XmlNodeWrapper, IXmlElement, IXmlNode
	{
		// Token: 0x06000945 RID: 2373 RVA: 0x00023282 File Offset: 0x00021482
		public XmlElementWrapper(XmlElement element) : base(element)
		{
			this._element = element;
		}

		// Token: 0x06000946 RID: 2374 RVA: 0x00023294 File Offset: 0x00021494
		public void SetAttributeNode(IXmlNode attribute)
		{
			XmlNodeWrapper xmlNodeWrapper = (XmlNodeWrapper)attribute;
			this._element.SetAttributeNode((XmlAttribute)xmlNodeWrapper.WrappedNode);
		}

		// Token: 0x06000947 RID: 2375 RVA: 0x000232BF File Offset: 0x000214BF
		public string GetPrefixOfNamespace(string namespaceUri)
		{
			return this._element.GetPrefixOfNamespace(namespaceUri);
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06000948 RID: 2376 RVA: 0x000232CD File Offset: 0x000214CD
		public bool IsEmpty
		{
			get
			{
				return this._element.IsEmpty;
			}
		}

		// Token: 0x040002F4 RID: 756
		private readonly XmlElement _element;
	}
}
