﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D5 RID: 213
	internal class XElementWrapper : XContainerWrapper, IXmlElement, IXmlNode
	{
		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060009CC RID: 2508 RVA: 0x00023AD1 File Offset: 0x00021CD1
		private XElement Element
		{
			get
			{
				return (XElement)base.WrappedNode;
			}
		}

		// Token: 0x060009CD RID: 2509 RVA: 0x0002368B File Offset: 0x0002188B
		public XElementWrapper(XElement element) : base(element)
		{
		}

		// Token: 0x060009CE RID: 2510 RVA: 0x00023AE0 File Offset: 0x00021CE0
		public void SetAttributeNode(IXmlNode attribute)
		{
			XObjectWrapper xobjectWrapper = (XObjectWrapper)attribute;
			this.Element.Add(xobjectWrapper.WrappedNode);
			this._attributes = null;
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060009CF RID: 2511 RVA: 0x00023B0C File Offset: 0x00021D0C
		public override List<IXmlNode> Attributes
		{
			get
			{
				if (this._attributes == null)
				{
					this._attributes = new List<IXmlNode>();
					foreach (XAttribute attribute in this.Element.Attributes())
					{
						this._attributes.Add(new XAttributeWrapper(attribute));
					}
					string namespaceUri = this.NamespaceUri;
					if (!string.IsNullOrEmpty(namespaceUri))
					{
						string a = namespaceUri;
						IXmlNode parentNode = this.ParentNode;
						if (a != ((parentNode != null) ? parentNode.NamespaceUri : null) && string.IsNullOrEmpty(this.GetPrefixOfNamespace(namespaceUri)))
						{
							bool flag = false;
							foreach (IXmlNode xmlNode in this._attributes)
							{
								if (xmlNode.LocalName == "xmlns" && string.IsNullOrEmpty(xmlNode.NamespaceUri) && xmlNode.Value == namespaceUri)
								{
									flag = true;
								}
							}
							if (!flag)
							{
								this._attributes.Insert(0, new XAttributeWrapper(new XAttribute("xmlns", namespaceUri)));
							}
						}
					}
				}
				return this._attributes;
			}
		}

		// Token: 0x060009D0 RID: 2512 RVA: 0x00023C5C File Offset: 0x00021E5C
		public override IXmlNode AppendChild(IXmlNode newChild)
		{
			IXmlNode result = base.AppendChild(newChild);
			this._attributes = null;
			return result;
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060009D1 RID: 2513 RVA: 0x00023C6C File Offset: 0x00021E6C
		// (set) Token: 0x060009D2 RID: 2514 RVA: 0x00023C79 File Offset: 0x00021E79
		public override string Value
		{
			get
			{
				return this.Element.Value;
			}
			set
			{
				this.Element.Value = value;
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060009D3 RID: 2515 RVA: 0x00023C87 File Offset: 0x00021E87
		public override string LocalName
		{
			get
			{
				return this.Element.Name.LocalName;
			}
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060009D4 RID: 2516 RVA: 0x00023C99 File Offset: 0x00021E99
		public override string NamespaceUri
		{
			get
			{
				return this.Element.Name.NamespaceName;
			}
		}

		// Token: 0x060009D5 RID: 2517 RVA: 0x00023CAB File Offset: 0x00021EAB
		public string GetPrefixOfNamespace(string namespaceUri)
		{
			return this.Element.GetPrefixOfNamespace(namespaceUri);
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060009D6 RID: 2518 RVA: 0x00023CBE File Offset: 0x00021EBE
		public bool IsEmpty
		{
			get
			{
				return this.Element.IsEmpty;
			}
		}

		// Token: 0x040002FF RID: 767
		private List<IXmlNode> _attributes;
	}
}
