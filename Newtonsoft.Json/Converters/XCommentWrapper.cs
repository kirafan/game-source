﻿using System;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D0 RID: 208
	internal class XCommentWrapper : XObjectWrapper
	{
		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x060009A9 RID: 2473 RVA: 0x0002382F File Offset: 0x00021A2F
		private XComment Text
		{
			get
			{
				return (XComment)base.WrappedNode;
			}
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x000237EA File Offset: 0x000219EA
		public XCommentWrapper(XComment text) : base(text)
		{
		}

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x060009AB RID: 2475 RVA: 0x0002383C File Offset: 0x00021A3C
		// (set) Token: 0x060009AC RID: 2476 RVA: 0x00023849 File Offset: 0x00021A49
		public override string Value
		{
			get
			{
				return this.Text.Value;
			}
			set
			{
				this.Text.Value = value;
			}
		}

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x060009AD RID: 2477 RVA: 0x00023857 File Offset: 0x00021A57
		public override IXmlNode ParentNode
		{
			get
			{
				if (this.Text.Parent == null)
				{
					return null;
				}
				return XContainerWrapper.WrapNode(this.Text.Parent);
			}
		}
	}
}
