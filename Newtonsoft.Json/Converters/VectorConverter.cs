﻿using System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Shims;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000BE RID: 190
	[Preserve]
	public class VectorConverter : JsonConverter
	{
		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x06000917 RID: 2327 RVA: 0x00022892 File Offset: 0x00020A92
		// (set) Token: 0x06000918 RID: 2328 RVA: 0x0002289A File Offset: 0x00020A9A
		public bool EnableVector2 { get; set; }

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x06000919 RID: 2329 RVA: 0x000228A3 File Offset: 0x00020AA3
		// (set) Token: 0x0600091A RID: 2330 RVA: 0x000228AB File Offset: 0x00020AAB
		public bool EnableVector3 { get; set; }

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x0600091B RID: 2331 RVA: 0x000228B4 File Offset: 0x00020AB4
		// (set) Token: 0x0600091C RID: 2332 RVA: 0x000228BC File Offset: 0x00020ABC
		public bool EnableVector4 { get; set; }

		// Token: 0x0600091D RID: 2333 RVA: 0x000228C5 File Offset: 0x00020AC5
		public VectorConverter()
		{
			this.EnableVector2 = true;
			this.EnableVector3 = true;
			this.EnableVector4 = true;
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x000228E2 File Offset: 0x00020AE2
		public VectorConverter(bool enableVector2, bool enableVector3, bool enableVector4) : this()
		{
			this.EnableVector2 = enableVector2;
			this.EnableVector3 = enableVector3;
			this.EnableVector4 = enableVector4;
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x00022900 File Offset: 0x00020B00
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			Type type = value.GetType();
			if (type == VectorConverter.V2)
			{
				Vector2 vector = (Vector2)value;
				VectorConverter.WriteVector(writer, vector.x, vector.y, null, null);
				return;
			}
			if (type == VectorConverter.V3)
			{
				Vector3 vector2 = (Vector3)value;
				VectorConverter.WriteVector(writer, vector2.x, vector2.y, new float?(vector2.z), null);
				return;
			}
			if (type == VectorConverter.V4)
			{
				Vector4 vector3 = (Vector4)value;
				VectorConverter.WriteVector(writer, vector3.x, vector3.y, new float?(vector3.z), new float?(vector3.w));
				return;
			}
			writer.WriteNull();
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x000229CC File Offset: 0x00020BCC
		private static void WriteVector(JsonWriter writer, float x, float y, float? z, float? w)
		{
			writer.WriteStartObject();
			writer.WritePropertyName("x");
			writer.WriteValue(x);
			writer.WritePropertyName("y");
			writer.WriteValue(y);
			if (z != null)
			{
				writer.WritePropertyName("z");
				writer.WriteValue(z.Value);
				if (w != null)
				{
					writer.WritePropertyName("w");
					writer.WriteValue(w.Value);
				}
			}
			writer.WriteEndObject();
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x00022A4B File Offset: 0x00020C4B
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (objectType == VectorConverter.V2)
			{
				return VectorConverter.PopulateVector2(reader);
			}
			if (objectType == VectorConverter.V3)
			{
				return VectorConverter.PopulateVector3(reader);
			}
			return VectorConverter.PopulateVector4(reader);
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x00022A80 File Offset: 0x00020C80
		public override bool CanConvert(Type objectType)
		{
			return (this.EnableVector2 && objectType == VectorConverter.V2) || (this.EnableVector3 && objectType == VectorConverter.V3) || (this.EnableVector4 && objectType == VectorConverter.V4);
		}

		// Token: 0x06000923 RID: 2339 RVA: 0x00022AB8 File Offset: 0x00020CB8
		private static Vector2 PopulateVector2(JsonReader reader)
		{
			Vector2 result = default(Vector2);
			if (reader.TokenType != JsonToken.Null)
			{
				JObject jobject = JObject.Load(reader);
				result.x = jobject["x"].Value<float>();
				result.y = jobject["y"].Value<float>();
			}
			return result;
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x00022B10 File Offset: 0x00020D10
		private static Vector3 PopulateVector3(JsonReader reader)
		{
			Vector3 result = default(Vector3);
			if (reader.TokenType != JsonToken.Null)
			{
				JObject jobject = JObject.Load(reader);
				result.x = jobject["x"].Value<float>();
				result.y = jobject["y"].Value<float>();
				result.z = jobject["z"].Value<float>();
			}
			return result;
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x00022B7C File Offset: 0x00020D7C
		private static Vector4 PopulateVector4(JsonReader reader)
		{
			Vector4 result = default(Vector4);
			if (reader.TokenType != JsonToken.Null)
			{
				JObject jobject = JObject.Load(reader);
				result.x = jobject["x"].Value<float>();
				result.y = jobject["y"].Value<float>();
				result.z = jobject["z"].Value<float>();
				result.w = jobject["w"].Value<float>();
			}
			return result;
		}

		// Token: 0x040002E9 RID: 745
		private static readonly Type V2 = typeof(Vector2);

		// Token: 0x040002EA RID: 746
		private static readonly Type V3 = typeof(Vector3);

		// Token: 0x040002EB RID: 747
		private static readonly Type V4 = typeof(Vector4);
	}
}
