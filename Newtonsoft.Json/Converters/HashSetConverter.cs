﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B5 RID: 181
	public class HashSetConverter : JsonConverter
	{
		// Token: 0x060008E3 RID: 2275 RVA: 0x0000982C File Offset: 0x00007A2C
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
		}

		// Token: 0x060008E4 RID: 2276 RVA: 0x00021930 File Offset: 0x0001FB30
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			bool flag = serializer.ObjectCreationHandling == ObjectCreationHandling.Replace;
			if (reader.TokenType != JsonToken.Null)
			{
				object obj = (!flag && existingValue != null) ? existingValue : Activator.CreateInstance(objectType);
				Type objectType2 = objectType.GetGenericArguments()[0];
				MethodInfo method = objectType.GetMethod("Add");
				JArray jarray = JArray.Load(reader);
				for (int i = 0; i < jarray.Count; i++)
				{
					object obj2 = serializer.Deserialize(jarray[i].CreateReader(), objectType2);
					method.Invoke(obj, new object[]
					{
						obj2
					});
				}
				return obj;
			}
			if (!flag)
			{
				return existingValue;
			}
			return null;
		}

		// Token: 0x060008E5 RID: 2277 RVA: 0x000219C8 File Offset: 0x0001FBC8
		public override bool CanConvert(Type objectType)
		{
			return objectType.IsGenericType() && objectType.GetGenericTypeDefinition() == typeof(HashSet<>);
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x060008E6 RID: 2278 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}
	}
}
