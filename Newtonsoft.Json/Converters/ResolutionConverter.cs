﻿using System;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000BB RID: 187
	public class ResolutionConverter : JsonConverter
	{
		// Token: 0x06000905 RID: 2309 RVA: 0x0002255C File Offset: 0x0002075C
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			Resolution resolution = (Resolution)value;
			writer.WriteStartObject();
			writer.WritePropertyName("height");
			writer.WriteValue(resolution.height);
			writer.WritePropertyName("width");
			writer.WriteValue(resolution.width);
			writer.WritePropertyName("refreshRate");
			writer.WriteValue(resolution.refreshRate);
			writer.WriteEndObject();
		}

		// Token: 0x06000906 RID: 2310 RVA: 0x000225C4 File Offset: 0x000207C4
		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(Resolution);
		}

		// Token: 0x06000907 RID: 2311 RVA: 0x000225D4 File Offset: 0x000207D4
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JObject jobject = JObject.Load(reader);
			return new Resolution
			{
				height = (int)jobject["height"],
				width = (int)jobject["width"],
				refreshRate = (int)jobject["refreshRate"]
			};
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000908 RID: 2312 RVA: 0x00005AA3 File Offset: 0x00003CA3
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}
	}
}
