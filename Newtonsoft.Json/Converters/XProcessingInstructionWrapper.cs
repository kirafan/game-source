﻿using System;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D1 RID: 209
	internal class XProcessingInstructionWrapper : XObjectWrapper
	{
		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x060009AE RID: 2478 RVA: 0x00023878 File Offset: 0x00021A78
		private XProcessingInstruction ProcessingInstruction
		{
			get
			{
				return (XProcessingInstruction)base.WrappedNode;
			}
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x000237EA File Offset: 0x000219EA
		public XProcessingInstructionWrapper(XProcessingInstruction processingInstruction) : base(processingInstruction)
		{
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x060009B0 RID: 2480 RVA: 0x00023885 File Offset: 0x00021A85
		public override string LocalName
		{
			get
			{
				return this.ProcessingInstruction.Target;
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x060009B1 RID: 2481 RVA: 0x00023892 File Offset: 0x00021A92
		// (set) Token: 0x060009B2 RID: 2482 RVA: 0x0002389F File Offset: 0x00021A9F
		public override string Value
		{
			get
			{
				return this.ProcessingInstruction.Data;
			}
			set
			{
				this.ProcessingInstruction.Data = value;
			}
		}
	}
}
