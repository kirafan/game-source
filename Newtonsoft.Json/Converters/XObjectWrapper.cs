﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000D3 RID: 211
	internal class XObjectWrapper : IXmlNode
	{
		// Token: 0x060009B9 RID: 2489 RVA: 0x00023A26 File Offset: 0x00021C26
		public XObjectWrapper(XObject xmlObject)
		{
			this._xmlObject = xmlObject;
		}

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x060009BA RID: 2490 RVA: 0x00023A35 File Offset: 0x00021C35
		public object WrappedNode
		{
			get
			{
				return this._xmlObject;
			}
		}

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x060009BB RID: 2491 RVA: 0x00023A3D File Offset: 0x00021C3D
		public virtual XmlNodeType NodeType
		{
			get
			{
				return this._xmlObject.NodeType;
			}
		}

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x060009BC RID: 2492 RVA: 0x0001B798 File Offset: 0x00019998
		public virtual string LocalName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x060009BD RID: 2493 RVA: 0x00023A4A File Offset: 0x00021C4A
		public virtual List<IXmlNode> ChildNodes
		{
			get
			{
				return XObjectWrapper.EmptyChildNodes;
			}
		}

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x060009BE RID: 2494 RVA: 0x0001B798 File Offset: 0x00019998
		public virtual List<IXmlNode> Attributes
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x060009BF RID: 2495 RVA: 0x0001B798 File Offset: 0x00019998
		public virtual IXmlNode ParentNode
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x060009C0 RID: 2496 RVA: 0x0001B798 File Offset: 0x00019998
		// (set) Token: 0x060009C1 RID: 2497 RVA: 0x00023A51 File Offset: 0x00021C51
		public virtual string Value
		{
			get
			{
				return null;
			}
			set
			{
				throw new InvalidOperationException();
			}
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x00023A51 File Offset: 0x00021C51
		public virtual IXmlNode AppendChild(IXmlNode newChild)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x060009C3 RID: 2499 RVA: 0x0001B798 File Offset: 0x00019998
		public virtual string NamespaceUri
		{
			get
			{
				return null;
			}
		}

		// Token: 0x040002FD RID: 765
		private static readonly List<IXmlNode> EmptyChildNodes = new List<IXmlNode>();

		// Token: 0x040002FE RID: 766
		private readonly XObject _xmlObject;
	}
}
