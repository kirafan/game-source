﻿using System;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000BC RID: 188
	[Preserve]
	public class StringEnumConverter : JsonConverter
	{
		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x0600090A RID: 2314 RVA: 0x0002263B File Offset: 0x0002083B
		// (set) Token: 0x0600090B RID: 2315 RVA: 0x00022643 File Offset: 0x00020843
		public bool CamelCaseText { get; set; }

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x0600090C RID: 2316 RVA: 0x0002264C File Offset: 0x0002084C
		// (set) Token: 0x0600090D RID: 2317 RVA: 0x00022654 File Offset: 0x00020854
		public bool AllowIntegerValues { get; set; }

		// Token: 0x0600090E RID: 2318 RVA: 0x0002265D File Offset: 0x0002085D
		public StringEnumConverter()
		{
			this.AllowIntegerValues = true;
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x0002266C File Offset: 0x0002086C
		public StringEnumConverter(bool camelCaseText) : this()
		{
			this.CamelCaseText = camelCaseText;
		}

		// Token: 0x06000910 RID: 2320 RVA: 0x0002267C File Offset: 0x0002087C
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			Enum @enum = (Enum)value;
			string text = @enum.ToString("G");
			if (char.IsNumber(text[0]) || text[0] == '-')
			{
				writer.WriteValue(value);
				return;
			}
			string value2 = EnumUtils.ToEnumName(@enum.GetType(), text, this.CamelCaseText);
			writer.WriteValue(value2);
		}

		// Token: 0x06000911 RID: 2321 RVA: 0x000226E4 File Offset: 0x000208E4
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType != JsonToken.Null)
			{
				bool flag = ReflectionUtils.IsNullableType(objectType);
				Type type = flag ? Nullable.GetUnderlyingType(objectType) : objectType;
				try
				{
					if (reader.TokenType == JsonToken.String)
					{
						return EnumUtils.ParseEnumName(reader.Value.ToString(), flag, type);
					}
					if (reader.TokenType == JsonToken.Integer)
					{
						if (!this.AllowIntegerValues)
						{
							throw JsonSerializationException.Create(reader, "Integer value {0} is not allowed.".FormatWith(CultureInfo.InvariantCulture, reader.Value));
						}
						return ConvertUtils.ConvertOrCast(reader.Value, CultureInfo.InvariantCulture, type);
					}
				}
				catch (Exception ex)
				{
					throw JsonSerializationException.Create(reader, "Error converting value {0} to type '{1}'.".FormatWith(CultureInfo.InvariantCulture, MiscellaneousUtils.FormatValueForPrint(reader.Value), objectType), ex);
				}
				throw JsonSerializationException.Create(reader, "Unexpected token {0} when parsing enum.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
			}
			if (!ReflectionUtils.IsNullableType(objectType))
			{
				throw JsonSerializationException.Create(reader, "Cannot convert null value to {0}.".FormatWith(CultureInfo.InvariantCulture, objectType));
			}
			return null;
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x000227EC File Offset: 0x000209EC
		public override bool CanConvert(Type objectType)
		{
			return (ReflectionUtils.IsNullableType(objectType) ? Nullable.GetUnderlyingType(objectType) : objectType).IsEnum();
		}
	}
}
