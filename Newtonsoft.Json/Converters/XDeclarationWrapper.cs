﻿using System;
using System.Xml;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CC RID: 204
	internal class XDeclarationWrapper : XObjectWrapper, IXmlDeclaration, IXmlNode
	{
		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000984 RID: 2436 RVA: 0x000235D2 File Offset: 0x000217D2
		// (set) Token: 0x06000985 RID: 2437 RVA: 0x000235DA File Offset: 0x000217DA
		internal XDeclaration Declaration { get; private set; }

		// Token: 0x06000986 RID: 2438 RVA: 0x000235E3 File Offset: 0x000217E3
		public XDeclarationWrapper(XDeclaration declaration) : base(null)
		{
			this.Declaration = declaration;
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000987 RID: 2439 RVA: 0x000235F3 File Offset: 0x000217F3
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.XmlDeclaration;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000988 RID: 2440 RVA: 0x000235F7 File Offset: 0x000217F7
		public string Version
		{
			get
			{
				return this.Declaration.Version;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000989 RID: 2441 RVA: 0x00023604 File Offset: 0x00021804
		// (set) Token: 0x0600098A RID: 2442 RVA: 0x00023611 File Offset: 0x00021811
		public string Encoding
		{
			get
			{
				return this.Declaration.Encoding;
			}
			set
			{
				this.Declaration.Encoding = value;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x0600098B RID: 2443 RVA: 0x0002361F File Offset: 0x0002181F
		// (set) Token: 0x0600098C RID: 2444 RVA: 0x0002362C File Offset: 0x0002182C
		public string Standalone
		{
			get
			{
				return this.Declaration.Standalone;
			}
			set
			{
				this.Declaration.Standalone = value;
			}
		}
	}
}
