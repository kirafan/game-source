﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CB RID: 203
	internal interface IXmlNode
	{
		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x0600097A RID: 2426
		XmlNodeType NodeType { get; }

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x0600097B RID: 2427
		string LocalName { get; }

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x0600097C RID: 2428
		List<IXmlNode> ChildNodes { get; }

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x0600097D RID: 2429
		List<IXmlNode> Attributes { get; }

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x0600097E RID: 2430
		IXmlNode ParentNode { get; }

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600097F RID: 2431
		// (set) Token: 0x06000980 RID: 2432
		string Value { get; set; }

		// Token: 0x06000981 RID: 2433
		IXmlNode AppendChild(IXmlNode newChild);

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000982 RID: 2434
		string NamespaceUri { get; }

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000983 RID: 2435
		object WrappedNode { get; }
	}
}
