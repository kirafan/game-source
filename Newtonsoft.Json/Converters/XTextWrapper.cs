﻿using System;
using System.Xml.Linq;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000CF RID: 207
	internal class XTextWrapper : XObjectWrapper
	{
		// Token: 0x170001DD RID: 477
		// (get) Token: 0x060009A4 RID: 2468 RVA: 0x000237DD File Offset: 0x000219DD
		private XText Text
		{
			get
			{
				return (XText)base.WrappedNode;
			}
		}

		// Token: 0x060009A5 RID: 2469 RVA: 0x000237EA File Offset: 0x000219EA
		public XTextWrapper(XText text) : base(text)
		{
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x060009A6 RID: 2470 RVA: 0x000237F3 File Offset: 0x000219F3
		// (set) Token: 0x060009A7 RID: 2471 RVA: 0x00023800 File Offset: 0x00021A00
		public override string Value
		{
			get
			{
				return this.Text.Value;
			}
			set
			{
				this.Text.Value = value;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x060009A8 RID: 2472 RVA: 0x0002380E File Offset: 0x00021A0E
		public override IXmlNode ParentNode
		{
			get
			{
				if (this.Text.Parent == null)
				{
					return null;
				}
				return XContainerWrapper.WrapNode(this.Text.Parent);
			}
		}
	}
}
