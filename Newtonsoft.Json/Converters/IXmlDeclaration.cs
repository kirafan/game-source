﻿using System;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000C8 RID: 200
	internal interface IXmlDeclaration : IXmlNode
	{
		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x0600096E RID: 2414
		string Version { get; }

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600096F RID: 2415
		// (set) Token: 0x06000970 RID: 2416
		string Encoding { get; set; }

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06000971 RID: 2417
		// (set) Token: 0x06000972 RID: 2418
		string Standalone { get; set; }
	}
}
