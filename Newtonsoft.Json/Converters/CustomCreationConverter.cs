﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B2 RID: 178
	[Preserve]
	public abstract class CustomCreationConverter<T> : JsonConverter
	{
		// Token: 0x060008D5 RID: 2261 RVA: 0x0002178B File Offset: 0x0001F98B
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotSupportedException("CustomCreationConverter should only be used while deserializing.");
		}

		// Token: 0x060008D6 RID: 2262 RVA: 0x00021798 File Offset: 0x0001F998
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				return null;
			}
			T t = this.Create(objectType);
			if (t == null)
			{
				throw new JsonSerializationException("No object created.");
			}
			serializer.Populate(reader, t);
			return t;
		}

		// Token: 0x060008D7 RID: 2263
		public abstract T Create(Type objectType);

		// Token: 0x060008D8 RID: 2264 RVA: 0x000217E0 File Offset: 0x0001F9E0
		public override bool CanConvert(Type objectType)
		{
			return typeof(T).IsAssignableFrom(objectType);
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x060008D9 RID: 2265 RVA: 0x0000CB98 File Offset: 0x0000AD98
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}
	}
}
