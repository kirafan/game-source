﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json.Shims;
using Newtonsoft.Json.Utilities;

namespace Newtonsoft.Json.Converters
{
	// Token: 0x020000B0 RID: 176
	[Preserve]
	public class BinaryConverter : JsonConverter
	{
		// Token: 0x060008C9 RID: 2249 RVA: 0x000213D8 File Offset: 0x0001F5D8
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				writer.WriteNull();
				return;
			}
			byte[] byteArray = this.GetByteArray(value);
			writer.WriteValue(byteArray);
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x00021400 File Offset: 0x0001F600
		private byte[] GetByteArray(object value)
		{
			if (value.GetType().AssignableToTypeName("System.Data.Linq.Binary"))
			{
				this.EnsureReflectionObject(value.GetType());
				return (byte[])this._reflectionObject.GetValue(value, "ToArray");
			}
			throw new JsonSerializationException("Unexpected value type when writing binary: {0}".FormatWith(CultureInfo.InvariantCulture, value.GetType()));
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x0002145C File Offset: 0x0001F65C
		private void EnsureReflectionObject(Type t)
		{
			if (this._reflectionObject == null)
			{
				this._reflectionObject = ReflectionObject.Create(t, t.GetConstructor(new Type[]
				{
					typeof(byte[])
				}), new string[]
				{
					"ToArray"
				});
			}
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x0002149C File Offset: 0x0001F69C
		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (reader.TokenType == JsonToken.Null)
			{
				if (!ReflectionUtils.IsNullable(objectType))
				{
					throw JsonSerializationException.Create(reader, "Cannot convert null value to {0}.".FormatWith(CultureInfo.InvariantCulture, objectType));
				}
				return null;
			}
			else
			{
				byte[] array;
				if (reader.TokenType == JsonToken.StartArray)
				{
					array = this.ReadByteArray(reader);
				}
				else
				{
					if (reader.TokenType != JsonToken.String)
					{
						throw JsonSerializationException.Create(reader, "Unexpected token parsing binary. Expected String or StartArray, got {0}.".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
					}
					array = Convert.FromBase64String(reader.Value.ToString());
				}
				Type type = ReflectionUtils.IsNullableType(objectType) ? Nullable.GetUnderlyingType(objectType) : objectType;
				if (type.AssignableToTypeName("System.Data.Linq.Binary"))
				{
					this.EnsureReflectionObject(type);
					return this._reflectionObject.Creator(new object[]
					{
						array
					});
				}
				throw JsonSerializationException.Create(reader, "Unexpected object type when writing binary: {0}".FormatWith(CultureInfo.InvariantCulture, objectType));
			}
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0002157C File Offset: 0x0001F77C
		private byte[] ReadByteArray(JsonReader reader)
		{
			List<byte> list = new List<byte>();
			while (reader.Read())
			{
				JsonToken tokenType = reader.TokenType;
				if (tokenType != JsonToken.Comment)
				{
					if (tokenType != JsonToken.Integer)
					{
						if (tokenType != JsonToken.EndArray)
						{
							throw JsonSerializationException.Create(reader, "Unexpected token when reading bytes: {0}".FormatWith(CultureInfo.InvariantCulture, reader.TokenType));
						}
						return list.ToArray();
					}
					else
					{
						list.Add(Convert.ToByte(reader.Value, CultureInfo.InvariantCulture));
					}
				}
			}
			throw JsonSerializationException.Create(reader, "Unexpected end when reading bytes.");
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x000215FA File Offset: 0x0001F7FA
		public override bool CanConvert(Type objectType)
		{
			return objectType.AssignableToTypeName("System.Data.Linq.Binary");
		}

		// Token: 0x040002DE RID: 734
		private const string BinaryTypeName = "System.Data.Linq.Binary";

		// Token: 0x040002DF RID: 735
		private const string BinaryToArrayName = "ToArray";

		// Token: 0x040002E0 RID: 736
		private ReflectionObject _reflectionObject;
	}
}
