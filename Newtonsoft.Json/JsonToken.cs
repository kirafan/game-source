﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000038 RID: 56
	[Preserve]
	public enum JsonToken
	{
		// Token: 0x04000110 RID: 272
		None,
		// Token: 0x04000111 RID: 273
		StartObject,
		// Token: 0x04000112 RID: 274
		StartArray,
		// Token: 0x04000113 RID: 275
		StartConstructor,
		// Token: 0x04000114 RID: 276
		PropertyName,
		// Token: 0x04000115 RID: 277
		Comment,
		// Token: 0x04000116 RID: 278
		Raw,
		// Token: 0x04000117 RID: 279
		Integer,
		// Token: 0x04000118 RID: 280
		Float,
		// Token: 0x04000119 RID: 281
		String,
		// Token: 0x0400011A RID: 282
		Boolean,
		// Token: 0x0400011B RID: 283
		Null,
		// Token: 0x0400011C RID: 284
		Undefined,
		// Token: 0x0400011D RID: 285
		EndObject,
		// Token: 0x0400011E RID: 286
		EndArray,
		// Token: 0x0400011F RID: 287
		EndConstructor,
		// Token: 0x04000120 RID: 288
		Date,
		// Token: 0x04000121 RID: 289
		Bytes
	}
}
