﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000034 RID: 52
	[Preserve]
	public enum MissingMemberHandling
	{
		// Token: 0x04000100 RID: 256
		Ignore,
		// Token: 0x04000101 RID: 257
		Error
	}
}
