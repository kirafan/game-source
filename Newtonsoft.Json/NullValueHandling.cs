﻿using System;
using Newtonsoft.Json.Shims;

namespace Newtonsoft.Json
{
	// Token: 0x02000035 RID: 53
	[Preserve]
	public enum NullValueHandling
	{
		// Token: 0x04000103 RID: 259
		Include,
		// Token: 0x04000104 RID: 260
		Ignore
	}
}
