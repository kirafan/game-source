﻿using System;
using System.Runtime.CompilerServices;
using Microsoft.Win32.SafeHandles;

namespace System.Threading
{
	// Token: 0x02000017 RID: 23
	[NullableContext(1)]
	[Nullable(0)]
	public static class WaitHandleExtensions
	{
		// Token: 0x0600004D RID: 77 RVA: 0x0000291A File Offset: 0x00000D1A
		public static SafeWaitHandle GetSafeWaitHandle(this WaitHandle waitHandle)
		{
			if (waitHandle == null)
			{
				throw new ArgumentNullException("waitHandle");
			}
			return waitHandle.SafeWaitHandle;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002930 File Offset: 0x00000D30
		public static void SetSafeWaitHandle(this WaitHandle waitHandle, [Nullable(2)] SafeWaitHandle value)
		{
			if (waitHandle == null)
			{
				throw new ArgumentNullException("waitHandle");
			}
			waitHandle.SafeWaitHandle = value;
		}
	}
}
