﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200001B RID: 27
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class EnumeratorCancellationAttribute : Attribute
	{
	}
}
