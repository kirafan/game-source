﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x02000019 RID: 25
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	[Nullable(0)]
	[NullableContext(1)]
	public sealed class TargetedPatchingOptOutAttribute : Attribute
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000051 RID: 81 RVA: 0x0000295E File Offset: 0x00000D5E
		public string Reason { get; }

		// Token: 0x06000052 RID: 82 RVA: 0x00002966 File Offset: 0x00000D66
		public TargetedPatchingOptOutAttribute(string reason)
		{
			this.Reason = reason;
		}
	}
}
