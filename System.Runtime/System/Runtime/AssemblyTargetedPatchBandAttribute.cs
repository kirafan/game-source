﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime
{
	// Token: 0x02000018 RID: 24
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[Nullable(0)]
	[NullableContext(1)]
	public sealed class AssemblyTargetedPatchBandAttribute : Attribute
	{
		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600004F RID: 79 RVA: 0x00002947 File Offset: 0x00000D47
		public string TargetedPatchBand { get; }

		// Token: 0x06000050 RID: 80 RVA: 0x0000294F File Offset: 0x00000D4F
		public AssemblyTargetedPatchBandAttribute(string targetedPatchBand)
		{
			this.TargetedPatchBand = targetedPatchBand;
		}
	}
}
