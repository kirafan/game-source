﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x0200001A RID: 26
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
	public sealed class PrePrepareMethodAttribute : Attribute
	{
	}
}
