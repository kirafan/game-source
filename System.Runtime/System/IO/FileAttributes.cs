﻿using System;

namespace System.IO
{
	// Token: 0x0200001D RID: 29
	[Flags]
	public enum FileAttributes
	{
		// Token: 0x04000008 RID: 8
		ReadOnly = 1,
		// Token: 0x04000009 RID: 9
		Hidden = 2,
		// Token: 0x0400000A RID: 10
		System = 4,
		// Token: 0x0400000B RID: 11
		Directory = 16,
		// Token: 0x0400000C RID: 12
		Archive = 32,
		// Token: 0x0400000D RID: 13
		Device = 64,
		// Token: 0x0400000E RID: 14
		Normal = 128,
		// Token: 0x0400000F RID: 15
		Temporary = 256,
		// Token: 0x04000010 RID: 16
		SparseFile = 512,
		// Token: 0x04000011 RID: 17
		ReparsePoint = 1024,
		// Token: 0x04000012 RID: 18
		Compressed = 2048,
		// Token: 0x04000013 RID: 19
		Offline = 4096,
		// Token: 0x04000014 RID: 20
		NotContentIndexed = 8192,
		// Token: 0x04000015 RID: 21
		Encrypted = 16384,
		// Token: 0x04000016 RID: 22
		IntegrityStream = 32768,
		// Token: 0x04000017 RID: 23
		NoScrubData = 131072
	}
}
