﻿using System;

namespace System.IO
{
	// Token: 0x0200001E RID: 30
	public enum HandleInheritability
	{
		// Token: 0x04000019 RID: 25
		None,
		// Token: 0x0400001A RID: 26
		Inheritable
	}
}
