﻿using System;
using System.Runtime.CompilerServices;

namespace System.Collections.Generic
{
	// Token: 0x0200001F RID: 31
	[NullableContext(1)]
	public interface ISet<[Nullable(2)] T> : ICollection<T>, IEnumerable<T>, IEnumerable
	{
		// Token: 0x06000060 RID: 96
		bool Add(T item);

		// Token: 0x06000061 RID: 97
		void UnionWith(IEnumerable<T> other);

		// Token: 0x06000062 RID: 98
		void IntersectWith(IEnumerable<T> other);

		// Token: 0x06000063 RID: 99
		void ExceptWith(IEnumerable<T> other);

		// Token: 0x06000064 RID: 100
		void SymmetricExceptWith(IEnumerable<T> other);

		// Token: 0x06000065 RID: 101
		bool IsSubsetOf(IEnumerable<T> other);

		// Token: 0x06000066 RID: 102
		bool IsSupersetOf(IEnumerable<T> other);

		// Token: 0x06000067 RID: 103
		bool IsProperSupersetOf(IEnumerable<T> other);

		// Token: 0x06000068 RID: 104
		bool IsProperSubsetOf(IEnumerable<T> other);

		// Token: 0x06000069 RID: 105
		bool Overlaps(IEnumerable<T> other);

		// Token: 0x0600006A RID: 106
		bool SetEquals(IEnumerable<T> other);
	}
}
