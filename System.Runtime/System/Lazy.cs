﻿using System;
using System.Runtime.CompilerServices;
using System.Threading;

namespace System
{
	// Token: 0x02000016 RID: 22
	[NullableContext(1)]
	[Nullable(new byte[]
	{
		0,
		1
	})]
	public class Lazy<[Nullable(2)] T, [Nullable(2)] TMetadata> : Lazy<T>
	{
		// Token: 0x06000046 RID: 70 RVA: 0x000028B1 File Offset: 0x00000CB1
		public Lazy(Func<T> valueFactory, TMetadata metadata) : base(valueFactory)
		{
			this._metadata = metadata;
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000028C1 File Offset: 0x00000CC1
		public Lazy(TMetadata metadata)
		{
			this._metadata = metadata;
		}

		// Token: 0x06000048 RID: 72 RVA: 0x000028D0 File Offset: 0x00000CD0
		public Lazy(TMetadata metadata, bool isThreadSafe) : base(isThreadSafe)
		{
			this._metadata = metadata;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000028E0 File Offset: 0x00000CE0
		public Lazy(Func<T> valueFactory, TMetadata metadata, bool isThreadSafe) : base(valueFactory, isThreadSafe)
		{
			this._metadata = metadata;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000028F1 File Offset: 0x00000CF1
		public Lazy(TMetadata metadata, LazyThreadSafetyMode mode) : base(mode)
		{
			this._metadata = metadata;
		}

		// Token: 0x0600004B RID: 75 RVA: 0x00002901 File Offset: 0x00000D01
		public Lazy(Func<T> valueFactory, TMetadata metadata, LazyThreadSafetyMode mode) : base(valueFactory, mode)
		{
			this._metadata = metadata;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002912 File Offset: 0x00000D12
		public TMetadata Metadata
		{
			get
			{
				return this._metadata;
			}
		}

		// Token: 0x04000004 RID: 4
		private readonly TMetadata _metadata;
	}
}
