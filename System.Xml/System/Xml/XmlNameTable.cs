﻿using System;

namespace System.Xml
{
	// Token: 0x020000FD RID: 253
	public abstract class XmlNameTable
	{
		// Token: 0x06000A13 RID: 2579
		public abstract string Add(string name);

		// Token: 0x06000A14 RID: 2580
		public abstract string Add(char[] buffer, int offset, int length);

		// Token: 0x06000A15 RID: 2581
		public abstract string Get(string name);

		// Token: 0x06000A16 RID: 2582
		public abstract string Get(char[] buffer, int offset, int length);
	}
}
