﻿using System;
using System.IO;
using System.Net;

namespace System.Xml
{
	// Token: 0x02000127 RID: 295
	public class XmlUrlResolver : XmlResolver
	{
		// Token: 0x170003CB RID: 971
		// (set) Token: 0x06000D2A RID: 3370 RVA: 0x000424E0 File Offset: 0x000406E0
		public override ICredentials Credentials
		{
			set
			{
				this.credential = value;
			}
		}

		// Token: 0x06000D2B RID: 3371 RVA: 0x000424EC File Offset: 0x000406EC
		public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (ofObjectToReturn == null)
			{
				ofObjectToReturn = typeof(Stream);
			}
			if (ofObjectToReturn != typeof(Stream))
			{
				throw new XmlException("This object type is not supported.");
			}
			if (!absoluteUri.IsAbsoluteUri)
			{
				throw new ArgumentException("uri must be absolute.", "absoluteUri");
			}
			if (!(absoluteUri.Scheme == "file"))
			{
				WebRequest webRequest = WebRequest.Create(absoluteUri);
				if (this.credential != null)
				{
					webRequest.Credentials = this.credential;
				}
				return webRequest.GetResponse().GetResponseStream();
			}
			if (absoluteUri.AbsolutePath == string.Empty)
			{
				throw new ArgumentException("uri must be absolute.", "absoluteUri");
			}
			return new FileStream(this.UnescapeRelativeUriBody(absoluteUri.LocalPath), FileMode.Open, FileAccess.Read, FileShare.Read);
		}

		// Token: 0x06000D2C RID: 3372 RVA: 0x000425BC File Offset: 0x000407BC
		public override Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			return base.ResolveUri(baseUri, relativeUri);
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x000425C8 File Offset: 0x000407C8
		private string UnescapeRelativeUriBody(string src)
		{
			return src.Replace("%3C", "<").Replace("%3E", ">").Replace("%23", "#").Replace("%22", "\"").Replace("%20", " ").Replace("%25", "%");
		}

		// Token: 0x04000616 RID: 1558
		private ICredentials credential;
	}
}
