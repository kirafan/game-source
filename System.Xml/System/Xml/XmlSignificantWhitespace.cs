﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200011C RID: 284
	public class XmlSignificantWhitespace : XmlCharacterData
	{
		// Token: 0x06000C0C RID: 3084 RVA: 0x0003CE50 File Offset: 0x0003B050
		protected internal XmlSignificantWhitespace(string strData, XmlDocument doc) : base(strData, doc)
		{
		}

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000C0D RID: 3085 RVA: 0x0003CE5C File Offset: 0x0003B05C
		public override string LocalName
		{
			get
			{
				return "#significant-whitespace";
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06000C0E RID: 3086 RVA: 0x0003CE64 File Offset: 0x0003B064
		public override string Name
		{
			get
			{
				return "#significant-whitespace";
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06000C0F RID: 3087 RVA: 0x0003CE6C File Offset: 0x0003B06C
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.SignificantWhitespace;
			}
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06000C10 RID: 3088 RVA: 0x0003CE70 File Offset: 0x0003B070
		internal override XPathNodeType XPathNodeType
		{
			get
			{
				return XPathNodeType.SignificantWhitespace;
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06000C11 RID: 3089 RVA: 0x0003CE74 File Offset: 0x0003B074
		// (set) Token: 0x06000C12 RID: 3090 RVA: 0x0003CE7C File Offset: 0x0003B07C
		public override string Value
		{
			get
			{
				return this.Data;
			}
			set
			{
				if (!XmlChar.IsWhitespace(value))
				{
					throw new ArgumentException("Invalid whitespace characters.");
				}
				base.Data = value;
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06000C13 RID: 3091 RVA: 0x0003CE9C File Offset: 0x0003B09C
		public override XmlNode ParentNode
		{
			get
			{
				return base.ParentNode;
			}
		}

		// Token: 0x06000C14 RID: 3092 RVA: 0x0003CEA4 File Offset: 0x0003B0A4
		public override XmlNode CloneNode(bool deep)
		{
			return new XmlSignificantWhitespace(this.Data, this.OwnerDocument);
		}

		// Token: 0x06000C15 RID: 3093 RVA: 0x0003CEC4 File Offset: 0x0003B0C4
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x06000C16 RID: 3094 RVA: 0x0003CEC8 File Offset: 0x0003B0C8
		public override void WriteTo(XmlWriter w)
		{
			w.WriteWhitespace(this.Data);
		}
	}
}
