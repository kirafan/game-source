﻿using System;
using System.Net;
using System.Security;
using System.Security.Policy;

namespace System.Xml
{
	// Token: 0x0200011B RID: 283
	public class XmlSecureResolver : XmlResolver
	{
		// Token: 0x06000C05 RID: 3077 RVA: 0x0003CCAC File Offset: 0x0003AEAC
		public XmlSecureResolver(XmlResolver resolver, Evidence evidence)
		{
			this.resolver = resolver;
			if (SecurityManager.SecurityEnabled)
			{
				this.permissionSet = SecurityManager.ResolvePolicy(evidence);
			}
		}

		// Token: 0x06000C06 RID: 3078 RVA: 0x0003CCD4 File Offset: 0x0003AED4
		public XmlSecureResolver(XmlResolver resolver, PermissionSet permissionSet)
		{
			this.resolver = resolver;
			this.permissionSet = permissionSet;
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x0003CCEC File Offset: 0x0003AEEC
		public XmlSecureResolver(XmlResolver resolver, string securityUrl)
		{
			this.resolver = resolver;
			if (SecurityManager.SecurityEnabled)
			{
				this.permissionSet = SecurityManager.ResolvePolicy(XmlSecureResolver.CreateEvidenceForUrl(securityUrl));
			}
		}

		// Token: 0x06000C08 RID: 3080 RVA: 0x0003CD24 File Offset: 0x0003AF24
		public static Evidence CreateEvidenceForUrl(string securityUrl)
		{
			Evidence evidence = new Evidence();
			if (securityUrl != null && securityUrl.Length > 0)
			{
				try
				{
					Url id = new Url(securityUrl);
					evidence.AddHost(id);
				}
				catch (ArgumentException)
				{
				}
				try
				{
					Zone id2 = Zone.CreateFromUrl(securityUrl);
					evidence.AddHost(id2);
				}
				catch (ArgumentException)
				{
				}
				try
				{
					Site id3 = Site.CreateFromUrl(securityUrl);
					evidence.AddHost(id3);
				}
				catch (ArgumentException)
				{
				}
			}
			return evidence;
		}

		// Token: 0x17000371 RID: 881
		// (set) Token: 0x06000C09 RID: 3081 RVA: 0x0003CDE4 File Offset: 0x0003AFE4
		public override ICredentials Credentials
		{
			set
			{
				this.resolver.Credentials = value;
			}
		}

		// Token: 0x06000C0A RID: 3082 RVA: 0x0003CDF4 File Offset: 0x0003AFF4
		[MonoTODO]
		public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
		{
			if (SecurityManager.SecurityEnabled)
			{
				if (this.permissionSet == null)
				{
					throw new SecurityException(Locale.GetText("Security Manager wasn't active when instance was created."));
				}
				this.permissionSet.PermitOnly();
			}
			return this.resolver.GetEntity(absoluteUri, role, ofObjectToReturn);
		}

		// Token: 0x06000C0B RID: 3083 RVA: 0x0003CE40 File Offset: 0x0003B040
		public override Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			return this.resolver.ResolveUri(baseUri, relativeUri);
		}

		// Token: 0x040005A7 RID: 1447
		private XmlResolver resolver;

		// Token: 0x040005A8 RID: 1448
		private PermissionSet permissionSet;
	}
}
