﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200012A RID: 298
	public abstract class XmlWriter : IDisposable
	{
		// Token: 0x06000D7B RID: 3451 RVA: 0x00043318 File Offset: 0x00041518
		void IDisposable.Dispose()
		{
			this.Dispose(false);
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06000D7C RID: 3452 RVA: 0x00043324 File Offset: 0x00041524
		public virtual XmlWriterSettings Settings
		{
			get
			{
				if (this.settings == null)
				{
					this.settings = new XmlWriterSettings();
				}
				return this.settings;
			}
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06000D7D RID: 3453
		public abstract WriteState WriteState { get; }

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06000D7E RID: 3454 RVA: 0x00043344 File Offset: 0x00041544
		public virtual string XmlLang
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06000D7F RID: 3455 RVA: 0x00043348 File Offset: 0x00041548
		public virtual XmlSpace XmlSpace
		{
			get
			{
				return XmlSpace.None;
			}
		}

		// Token: 0x06000D80 RID: 3456
		public abstract void Close();

		// Token: 0x06000D81 RID: 3457 RVA: 0x0004334C File Offset: 0x0004154C
		public static XmlWriter Create(Stream stream)
		{
			return XmlWriter.Create(stream, null);
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x00043358 File Offset: 0x00041558
		public static XmlWriter Create(string file)
		{
			return XmlWriter.Create(file, null);
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x00043364 File Offset: 0x00041564
		public static XmlWriter Create(TextWriter writer)
		{
			return XmlWriter.Create(writer, null);
		}

		// Token: 0x06000D84 RID: 3460 RVA: 0x00043370 File Offset: 0x00041570
		public static XmlWriter Create(XmlWriter writer)
		{
			return XmlWriter.Create(writer, null);
		}

		// Token: 0x06000D85 RID: 3461 RVA: 0x0004337C File Offset: 0x0004157C
		public static XmlWriter Create(StringBuilder builder)
		{
			return XmlWriter.Create(builder, null);
		}

		// Token: 0x06000D86 RID: 3462 RVA: 0x00043388 File Offset: 0x00041588
		public static XmlWriter Create(Stream stream, XmlWriterSettings settings)
		{
			Encoding encoding = (settings == null) ? Encoding.UTF8 : settings.Encoding;
			return XmlWriter.Create(new StreamWriter(stream, encoding), settings);
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x000433BC File Offset: 0x000415BC
		public static XmlWriter Create(string file, XmlWriterSettings settings)
		{
			Encoding encoding = (settings == null) ? Encoding.UTF8 : settings.Encoding;
			return XmlWriter.CreateTextWriter(new StreamWriter(file, false, encoding), settings, true);
		}

		// Token: 0x06000D88 RID: 3464 RVA: 0x000433F0 File Offset: 0x000415F0
		public static XmlWriter Create(StringBuilder builder, XmlWriterSettings settings)
		{
			return XmlWriter.Create(new StringWriter(builder), settings);
		}

		// Token: 0x06000D89 RID: 3465 RVA: 0x00043400 File Offset: 0x00041600
		public static XmlWriter Create(TextWriter writer, XmlWriterSettings settings)
		{
			if (settings == null)
			{
				settings = new XmlWriterSettings();
			}
			return XmlWriter.CreateTextWriter(writer, settings, settings.CloseOutput);
		}

		// Token: 0x06000D8A RID: 3466 RVA: 0x0004341C File Offset: 0x0004161C
		public static XmlWriter Create(XmlWriter writer, XmlWriterSettings settings)
		{
			if (settings == null)
			{
				settings = new XmlWriterSettings();
			}
			writer.settings = settings;
			return writer;
		}

		// Token: 0x06000D8B RID: 3467 RVA: 0x00043434 File Offset: 0x00041634
		private static XmlWriter CreateTextWriter(TextWriter writer, XmlWriterSettings settings, bool closeOutput)
		{
			if (settings == null)
			{
				settings = new XmlWriterSettings();
			}
			XmlTextWriter writer2 = new XmlTextWriter(writer, settings, closeOutput);
			return XmlWriter.Create(writer2, settings);
		}

		// Token: 0x06000D8C RID: 3468 RVA: 0x00043460 File Offset: 0x00041660
		protected virtual void Dispose(bool disposing)
		{
			this.Close();
		}

		// Token: 0x06000D8D RID: 3469
		public abstract void Flush();

		// Token: 0x06000D8E RID: 3470
		public abstract string LookupPrefix(string ns);

		// Token: 0x06000D8F RID: 3471 RVA: 0x00043468 File Offset: 0x00041668
		private void WriteAttribute(XmlReader reader, bool defattr)
		{
			if (!defattr && reader.IsDefault)
			{
				return;
			}
			this.WriteStartAttribute(reader.Prefix, reader.LocalName, reader.NamespaceURI);
			while (reader.ReadAttributeValue())
			{
				switch (reader.NodeType)
				{
				case XmlNodeType.Text:
					this.WriteString(reader.Value);
					break;
				case XmlNodeType.EntityReference:
					this.WriteEntityRef(reader.Name);
					break;
				}
			}
			this.WriteEndAttribute();
		}

		// Token: 0x06000D90 RID: 3472 RVA: 0x000434F8 File Offset: 0x000416F8
		public virtual void WriteAttributes(XmlReader reader, bool defattr)
		{
			if (reader == null)
			{
				throw new ArgumentException("null XmlReader specified.", "reader");
			}
			XmlNodeType nodeType = reader.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				if (nodeType != XmlNodeType.Attribute)
				{
					if (nodeType != XmlNodeType.XmlDeclaration)
					{
						throw new XmlException("NodeType is not one of Element, Attribute, nor XmlDeclaration.");
					}
					this.WriteAttributeString("version", reader["version"]);
					if (reader["encoding"] != null)
					{
						this.WriteAttributeString("encoding", reader["encoding"]);
					}
					if (reader["standalone"] != null)
					{
						this.WriteAttributeString("standalone", reader["standalone"]);
					}
					return;
				}
			}
			else if (!reader.MoveToFirstAttribute())
			{
				return;
			}
			do
			{
				this.WriteAttribute(reader, defattr);
			}
			while (reader.MoveToNextAttribute());
			reader.MoveToElement();
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x000435E4 File Offset: 0x000417E4
		public void WriteAttributeString(string localName, string value)
		{
			this.WriteAttributeString(string.Empty, localName, null, value);
		}

		// Token: 0x06000D92 RID: 3474 RVA: 0x000435F4 File Offset: 0x000417F4
		public void WriteAttributeString(string localName, string ns, string value)
		{
			this.WriteAttributeString(string.Empty, localName, ns, value);
		}

		// Token: 0x06000D93 RID: 3475 RVA: 0x00043604 File Offset: 0x00041804
		public void WriteAttributeString(string prefix, string localName, string ns, string value)
		{
			this.WriteStartAttribute(prefix, localName, ns);
			if (value != null && value.Length > 0)
			{
				this.WriteString(value);
			}
			this.WriteEndAttribute();
		}

		// Token: 0x06000D94 RID: 3476
		public abstract void WriteBase64(byte[] buffer, int index, int count);

		// Token: 0x06000D95 RID: 3477 RVA: 0x00043634 File Offset: 0x00041834
		public virtual void WriteBinHex(byte[] buffer, int index, int count)
		{
			StringWriter stringWriter = new StringWriter();
			XmlConvert.WriteBinHex(buffer, index, count, stringWriter);
			this.WriteString(stringWriter.ToString());
		}

		// Token: 0x06000D96 RID: 3478
		public abstract void WriteCData(string text);

		// Token: 0x06000D97 RID: 3479
		public abstract void WriteCharEntity(char ch);

		// Token: 0x06000D98 RID: 3480
		public abstract void WriteChars(char[] buffer, int index, int count);

		// Token: 0x06000D99 RID: 3481
		public abstract void WriteComment(string text);

		// Token: 0x06000D9A RID: 3482
		public abstract void WriteDocType(string name, string pubid, string sysid, string subset);

		// Token: 0x06000D9B RID: 3483 RVA: 0x0004365C File Offset: 0x0004185C
		public void WriteElementString(string localName, string value)
		{
			this.WriteStartElement(localName);
			if (value != null && value.Length > 0)
			{
				this.WriteString(value);
			}
			this.WriteEndElement();
		}

		// Token: 0x06000D9C RID: 3484 RVA: 0x00043690 File Offset: 0x00041890
		public void WriteElementString(string localName, string ns, string value)
		{
			this.WriteStartElement(localName, ns);
			if (value != null && value.Length > 0)
			{
				this.WriteString(value);
			}
			this.WriteEndElement();
		}

		// Token: 0x06000D9D RID: 3485 RVA: 0x000436BC File Offset: 0x000418BC
		public void WriteElementString(string prefix, string localName, string ns, string value)
		{
			this.WriteStartElement(prefix, localName, ns);
			if (value != null && value.Length > 0)
			{
				this.WriteString(value);
			}
			this.WriteEndElement();
		}

		// Token: 0x06000D9E RID: 3486
		public abstract void WriteEndAttribute();

		// Token: 0x06000D9F RID: 3487
		public abstract void WriteEndDocument();

		// Token: 0x06000DA0 RID: 3488
		public abstract void WriteEndElement();

		// Token: 0x06000DA1 RID: 3489
		public abstract void WriteEntityRef(string name);

		// Token: 0x06000DA2 RID: 3490
		public abstract void WriteFullEndElement();

		// Token: 0x06000DA3 RID: 3491 RVA: 0x000436EC File Offset: 0x000418EC
		public virtual void WriteName(string name)
		{
			this.WriteNameInternal(name);
		}

		// Token: 0x06000DA4 RID: 3492 RVA: 0x000436F8 File Offset: 0x000418F8
		public virtual void WriteNmToken(string name)
		{
			this.WriteNmTokenInternal(name);
		}

		// Token: 0x06000DA5 RID: 3493 RVA: 0x00043704 File Offset: 0x00041904
		public virtual void WriteQualifiedName(string localName, string ns)
		{
			this.WriteQualifiedNameInternal(localName, ns);
		}

		// Token: 0x06000DA6 RID: 3494 RVA: 0x00043710 File Offset: 0x00041910
		internal void WriteNameInternal(string name)
		{
			ConformanceLevel conformanceLevel = this.Settings.ConformanceLevel;
			if (conformanceLevel == ConformanceLevel.Fragment || conformanceLevel == ConformanceLevel.Document)
			{
				XmlConvert.VerifyName(name);
			}
			this.WriteString(name);
		}

		// Token: 0x06000DA7 RID: 3495 RVA: 0x00043750 File Offset: 0x00041950
		internal virtual void WriteNmTokenInternal(string name)
		{
			bool flag = true;
			ConformanceLevel conformanceLevel = this.Settings.ConformanceLevel;
			if (conformanceLevel == ConformanceLevel.Fragment || conformanceLevel == ConformanceLevel.Document)
			{
				flag = XmlChar.IsNmToken(name);
			}
			if (!flag)
			{
				throw new ArgumentException("Argument name is not a valid NMTOKEN.");
			}
			this.WriteString(name);
		}

		// Token: 0x06000DA8 RID: 3496 RVA: 0x000437A4 File Offset: 0x000419A4
		internal void WriteQualifiedNameInternal(string localName, string ns)
		{
			if (localName == null || localName == string.Empty)
			{
				throw new ArgumentException();
			}
			if (ns == null)
			{
				ns = string.Empty;
			}
			ConformanceLevel conformanceLevel = this.Settings.ConformanceLevel;
			if (conformanceLevel == ConformanceLevel.Fragment || conformanceLevel == ConformanceLevel.Document)
			{
				XmlConvert.VerifyNCName(localName);
			}
			string text = (ns.Length <= 0) ? string.Empty : this.LookupPrefix(ns);
			if (text == null)
			{
				throw new ArgumentException(string.Format("Namespace '{0}' is not declared.", ns));
			}
			if (text != string.Empty)
			{
				this.WriteString(text);
				this.WriteString(":");
				this.WriteString(localName);
			}
			else
			{
				this.WriteString(localName);
			}
		}

		// Token: 0x06000DA9 RID: 3497 RVA: 0x00043870 File Offset: 0x00041A70
		public virtual void WriteNode(XPathNavigator navigator, bool defattr)
		{
			if (navigator == null)
			{
				throw new ArgumentNullException("navigator");
			}
			switch (navigator.NodeType)
			{
			case XPathNodeType.Root:
				if (navigator.MoveToFirstChild())
				{
					do
					{
						this.WriteNode(navigator, defattr);
					}
					while (navigator.MoveToNext());
					navigator.MoveToParent();
				}
				break;
			case XPathNodeType.Element:
				this.WriteStartElement(navigator.Prefix, navigator.LocalName, navigator.NamespaceURI);
				if (navigator.MoveToFirstNamespace(XPathNamespaceScope.Local))
				{
					do
					{
						if (defattr || navigator.SchemaInfo == null || navigator.SchemaInfo.IsDefault)
						{
							this.WriteAttributeString(navigator.Prefix, (!(navigator.LocalName == string.Empty)) ? navigator.LocalName : "xmlns", "http://www.w3.org/2000/xmlns/", navigator.Value);
						}
					}
					while (navigator.MoveToNextNamespace(XPathNamespaceScope.Local));
					navigator.MoveToParent();
				}
				if (navigator.MoveToFirstAttribute())
				{
					do
					{
						if (defattr || navigator.SchemaInfo == null || navigator.SchemaInfo.IsDefault)
						{
							this.WriteAttributeString(navigator.Prefix, navigator.LocalName, navigator.NamespaceURI, navigator.Value);
						}
					}
					while (navigator.MoveToNextAttribute());
					navigator.MoveToParent();
				}
				if (navigator.MoveToFirstChild())
				{
					do
					{
						this.WriteNode(navigator, defattr);
					}
					while (navigator.MoveToNext());
					navigator.MoveToParent();
				}
				if (navigator.IsEmptyElement)
				{
					this.WriteEndElement();
				}
				else
				{
					this.WriteFullEndElement();
				}
				break;
			case XPathNodeType.Attribute:
				break;
			case XPathNodeType.Namespace:
				break;
			case XPathNodeType.Text:
				this.WriteString(navigator.Value);
				break;
			case XPathNodeType.SignificantWhitespace:
				this.WriteWhitespace(navigator.Value);
				break;
			case XPathNodeType.Whitespace:
				this.WriteWhitespace(navigator.Value);
				break;
			case XPathNodeType.ProcessingInstruction:
				this.WriteProcessingInstruction(navigator.Name, navigator.Value);
				break;
			case XPathNodeType.Comment:
				this.WriteComment(navigator.Value);
				break;
			default:
				throw new NotSupportedException();
			}
		}

		// Token: 0x06000DAA RID: 3498 RVA: 0x00043A90 File Offset: 0x00041C90
		public virtual void WriteNode(XmlReader reader, bool defattr)
		{
			if (reader == null)
			{
				throw new ArgumentException();
			}
			if (reader.ReadState == ReadState.Initial)
			{
				reader.Read();
				do
				{
					this.WriteNode(reader, defattr);
				}
				while (!reader.EOF);
				return;
			}
			switch (reader.NodeType)
			{
			case XmlNodeType.None:
				goto IL_218;
			case XmlNodeType.Element:
				this.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
				if (reader.HasAttributes)
				{
					for (int i = 0; i < reader.AttributeCount; i++)
					{
						reader.MoveToAttribute(i);
						this.WriteAttribute(reader, defattr);
					}
					reader.MoveToElement();
				}
				if (reader.IsEmptyElement)
				{
					this.WriteEndElement();
				}
				else
				{
					int depth = reader.Depth;
					reader.Read();
					if (reader.NodeType != XmlNodeType.EndElement)
					{
						do
						{
							this.WriteNode(reader, defattr);
						}
						while (depth < reader.Depth);
					}
					this.WriteFullEndElement();
				}
				goto IL_218;
			case XmlNodeType.Attribute:
				return;
			case XmlNodeType.Text:
				this.WriteString(reader.Value);
				goto IL_218;
			case XmlNodeType.CDATA:
				this.WriteCData(reader.Value);
				goto IL_218;
			case XmlNodeType.EntityReference:
				this.WriteEntityRef(reader.Name);
				goto IL_218;
			case XmlNodeType.Entity:
			case XmlNodeType.Document:
			case XmlNodeType.DocumentFragment:
			case XmlNodeType.Notation:
				goto IL_1E0;
			case XmlNodeType.ProcessingInstruction:
			case XmlNodeType.XmlDeclaration:
				this.WriteProcessingInstruction(reader.Name, reader.Value);
				goto IL_218;
			case XmlNodeType.Comment:
				this.WriteComment(reader.Value);
				goto IL_218;
			case XmlNodeType.DocumentType:
				this.WriteDocType(reader.Name, reader["PUBLIC"], reader["SYSTEM"], reader.Value);
				goto IL_218;
			case XmlNodeType.Whitespace:
				break;
			case XmlNodeType.SignificantWhitespace:
				break;
			case XmlNodeType.EndElement:
				this.WriteFullEndElement();
				goto IL_218;
			case XmlNodeType.EndEntity:
				goto IL_218;
			default:
				goto IL_1E0;
			}
			this.WriteWhitespace(reader.Value);
			goto IL_218;
			IL_1E0:
			throw new XmlException(string.Concat(new object[]
			{
				"Unexpected node ",
				reader.Name,
				" of type ",
				reader.NodeType
			}));
			IL_218:
			reader.Read();
		}

		// Token: 0x06000DAB RID: 3499
		public abstract void WriteProcessingInstruction(string name, string text);

		// Token: 0x06000DAC RID: 3500
		public abstract void WriteRaw(string data);

		// Token: 0x06000DAD RID: 3501
		public abstract void WriteRaw(char[] buffer, int index, int count);

		// Token: 0x06000DAE RID: 3502 RVA: 0x00043CBC File Offset: 0x00041EBC
		public void WriteStartAttribute(string localName)
		{
			this.WriteStartAttribute(null, localName, null);
		}

		// Token: 0x06000DAF RID: 3503 RVA: 0x00043CC8 File Offset: 0x00041EC8
		public void WriteStartAttribute(string localName, string ns)
		{
			this.WriteStartAttribute(null, localName, ns);
		}

		// Token: 0x06000DB0 RID: 3504
		public abstract void WriteStartAttribute(string prefix, string localName, string ns);

		// Token: 0x06000DB1 RID: 3505
		public abstract void WriteStartDocument();

		// Token: 0x06000DB2 RID: 3506
		public abstract void WriteStartDocument(bool standalone);

		// Token: 0x06000DB3 RID: 3507 RVA: 0x00043CD4 File Offset: 0x00041ED4
		public void WriteStartElement(string localName)
		{
			this.WriteStartElement(null, localName, null);
		}

		// Token: 0x06000DB4 RID: 3508 RVA: 0x00043CE0 File Offset: 0x00041EE0
		public void WriteStartElement(string localName, string ns)
		{
			this.WriteStartElement(null, localName, ns);
		}

		// Token: 0x06000DB5 RID: 3509
		public abstract void WriteStartElement(string prefix, string localName, string ns);

		// Token: 0x06000DB6 RID: 3510
		public abstract void WriteString(string text);

		// Token: 0x06000DB7 RID: 3511
		public abstract void WriteSurrogateCharEntity(char lowChar, char highChar);

		// Token: 0x06000DB8 RID: 3512
		public abstract void WriteWhitespace(string ws);

		// Token: 0x06000DB9 RID: 3513 RVA: 0x00043CEC File Offset: 0x00041EEC
		public virtual void WriteValue(bool value)
		{
			this.WriteString(XQueryConvert.BooleanToString(value));
		}

		// Token: 0x06000DBA RID: 3514 RVA: 0x00043CFC File Offset: 0x00041EFC
		public virtual void WriteValue(DateTime value)
		{
			this.WriteString(XmlConvert.ToString(value));
		}

		// Token: 0x06000DBB RID: 3515 RVA: 0x00043D0C File Offset: 0x00041F0C
		public virtual void WriteValue(decimal value)
		{
			this.WriteString(XQueryConvert.DecimalToString(value));
		}

		// Token: 0x06000DBC RID: 3516 RVA: 0x00043D1C File Offset: 0x00041F1C
		public virtual void WriteValue(double value)
		{
			this.WriteString(XQueryConvert.DoubleToString(value));
		}

		// Token: 0x06000DBD RID: 3517 RVA: 0x00043D2C File Offset: 0x00041F2C
		public virtual void WriteValue(int value)
		{
			this.WriteString(XQueryConvert.IntToString(value));
		}

		// Token: 0x06000DBE RID: 3518 RVA: 0x00043D3C File Offset: 0x00041F3C
		public virtual void WriteValue(long value)
		{
			this.WriteString(XQueryConvert.IntegerToString(value));
		}

		// Token: 0x06000DBF RID: 3519 RVA: 0x00043D4C File Offset: 0x00041F4C
		public virtual void WriteValue(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (value is string)
			{
				this.WriteString((string)value);
			}
			else if (value is bool)
			{
				this.WriteValue((bool)value);
			}
			else if (value is byte)
			{
				this.WriteValue((int)value);
			}
			else if (value is byte[])
			{
				this.WriteBase64((byte[])value, 0, ((byte[])value).Length);
			}
			else if (value is char[])
			{
				this.WriteChars((char[])value, 0, ((char[])value).Length);
			}
			else if (value is DateTime)
			{
				this.WriteValue((DateTime)value);
			}
			else if (value is decimal)
			{
				this.WriteValue((decimal)value);
			}
			else if (value is double)
			{
				this.WriteValue((double)value);
			}
			else if (value is short)
			{
				this.WriteValue((int)value);
			}
			else if (value is int)
			{
				this.WriteValue((int)value);
			}
			else if (value is long)
			{
				this.WriteValue((long)value);
			}
			else if (value is float)
			{
				this.WriteValue((float)value);
			}
			else if (value is TimeSpan)
			{
				this.WriteString(XmlConvert.ToString((TimeSpan)value));
			}
			else if (value is XmlQualifiedName)
			{
				XmlQualifiedName xmlQualifiedName = (XmlQualifiedName)value;
				if (!xmlQualifiedName.Equals(XmlQualifiedName.Empty))
				{
					if (xmlQualifiedName.Namespace.Length > 0 && this.LookupPrefix(xmlQualifiedName.Namespace) == null)
					{
						throw new InvalidCastException(string.Format("The QName '{0}' cannot be written. No corresponding prefix is declared", xmlQualifiedName));
					}
					this.WriteQualifiedName(xmlQualifiedName.Name, xmlQualifiedName.Namespace);
				}
				else
				{
					this.WriteString(string.Empty);
				}
			}
			else
			{
				if (!(value is IEnumerable))
				{
					throw new InvalidCastException(string.Format("Type '{0}' cannot be cast to string", value.GetType()));
				}
				bool flag = false;
				foreach (object value2 in ((IEnumerable)value))
				{
					if (flag)
					{
						this.WriteString(" ");
					}
					else
					{
						flag = true;
					}
					this.WriteValue(value2);
				}
			}
		}

		// Token: 0x06000DC0 RID: 3520 RVA: 0x0004400C File Offset: 0x0004220C
		public virtual void WriteValue(float value)
		{
			this.WriteString(XQueryConvert.FloatToString(value));
		}

		// Token: 0x06000DC1 RID: 3521 RVA: 0x0004401C File Offset: 0x0004221C
		public virtual void WriteValue(string value)
		{
			this.WriteString(value);
		}

		// Token: 0x04000623 RID: 1571
		private XmlWriterSettings settings;
	}
}
