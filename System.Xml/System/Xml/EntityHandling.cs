﻿using System;

namespace System.Xml
{
	// Token: 0x020000D7 RID: 215
	public enum EntityHandling
	{
		// Token: 0x04000471 RID: 1137
		ExpandEntities = 1,
		// Token: 0x04000472 RID: 1138
		ExpandCharEntities
	}
}
