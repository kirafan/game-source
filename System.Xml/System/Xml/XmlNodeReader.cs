﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;
using Mono.Xml;

namespace System.Xml
{
	// Token: 0x0200010E RID: 270
	public class XmlNodeReader : XmlReader, IHasXmlParserContext, IXmlNamespaceResolver
	{
		// Token: 0x06000AAA RID: 2730 RVA: 0x00037FE4 File Offset: 0x000361E4
		public XmlNodeReader(XmlNode node)
		{
			this.source = new XmlNodeReaderImpl(node);
		}

		// Token: 0x06000AAB RID: 2731 RVA: 0x00037FF8 File Offset: 0x000361F8
		private XmlNodeReader(XmlNodeReaderImpl entityContainer, bool insideAttribute)
		{
			this.source = new XmlNodeReaderImpl(entityContainer);
			this.entityInsideAttribute = insideAttribute;
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000AAC RID: 2732 RVA: 0x00038014 File Offset: 0x00036214
		XmlParserContext IHasXmlParserContext.ParserContext
		{
			get
			{
				return ((IHasXmlParserContext)this.Current).ParserContext;
			}
		}

		// Token: 0x06000AAD RID: 2733 RVA: 0x00038028 File Offset: 0x00036228
		IDictionary<string, string> IXmlNamespaceResolver.GetNamespacesInScope(XmlNamespaceScope scope)
		{
			return ((IXmlNamespaceResolver)this.Current).GetNamespacesInScope(scope);
		}

		// Token: 0x06000AAE RID: 2734 RVA: 0x0003803C File Offset: 0x0003623C
		string IXmlNamespaceResolver.LookupPrefix(string ns)
		{
			return ((IXmlNamespaceResolver)this.Current).LookupPrefix(ns);
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000AAF RID: 2735 RVA: 0x00038050 File Offset: 0x00036250
		private XmlReader Current
		{
			get
			{
				return (this.entity == null || this.entity.ReadState == ReadState.Initial) ? this.source : this.entity;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000AB0 RID: 2736 RVA: 0x0003808C File Offset: 0x0003628C
		public override int AttributeCount
		{
			get
			{
				return this.Current.AttributeCount;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000AB1 RID: 2737 RVA: 0x0003809C File Offset: 0x0003629C
		public override string BaseURI
		{
			get
			{
				return this.Current.BaseURI;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06000AB2 RID: 2738 RVA: 0x000380AC File Offset: 0x000362AC
		public override bool CanReadBinaryContent
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06000AB3 RID: 2739 RVA: 0x000380B0 File Offset: 0x000362B0
		public override bool CanResolveEntity
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000AB4 RID: 2740 RVA: 0x000380B4 File Offset: 0x000362B4
		public override int Depth
		{
			get
			{
				if (this.entity != null && this.entity.ReadState == ReadState.Interactive)
				{
					return this.source.Depth + this.entity.Depth + 1;
				}
				return this.source.Depth;
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000AB5 RID: 2741 RVA: 0x00038104 File Offset: 0x00036304
		public override bool EOF
		{
			get
			{
				return this.source.EOF;
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x00038114 File Offset: 0x00036314
		public override bool HasAttributes
		{
			get
			{
				return this.Current.HasAttributes;
			}
		}

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000AB7 RID: 2743 RVA: 0x00038124 File Offset: 0x00036324
		public override bool HasValue
		{
			get
			{
				return this.Current.HasValue;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000AB8 RID: 2744 RVA: 0x00038134 File Offset: 0x00036334
		public override bool IsDefault
		{
			get
			{
				return this.Current.IsDefault;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000AB9 RID: 2745 RVA: 0x00038144 File Offset: 0x00036344
		public override bool IsEmptyElement
		{
			get
			{
				return this.Current.IsEmptyElement;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06000ABA RID: 2746 RVA: 0x00038154 File Offset: 0x00036354
		public override string LocalName
		{
			get
			{
				return this.Current.LocalName;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06000ABB RID: 2747 RVA: 0x00038164 File Offset: 0x00036364
		public override string Name
		{
			get
			{
				return this.Current.Name;
			}
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06000ABC RID: 2748 RVA: 0x00038174 File Offset: 0x00036374
		public override string NamespaceURI
		{
			get
			{
				return this.Current.NamespaceURI;
			}
		}

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06000ABD RID: 2749 RVA: 0x00038184 File Offset: 0x00036384
		public override XmlNameTable NameTable
		{
			get
			{
				return this.Current.NameTable;
			}
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06000ABE RID: 2750 RVA: 0x00038194 File Offset: 0x00036394
		public override XmlNodeType NodeType
		{
			get
			{
				if (this.entity != null)
				{
					return (this.entity.ReadState != ReadState.Initial) ? ((!this.entity.EOF) ? this.entity.NodeType : XmlNodeType.EndEntity) : this.source.NodeType;
				}
				return this.source.NodeType;
			}
		}

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06000ABF RID: 2751 RVA: 0x000381FC File Offset: 0x000363FC
		public override string Prefix
		{
			get
			{
				return this.Current.Prefix;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06000AC0 RID: 2752 RVA: 0x0003820C File Offset: 0x0003640C
		public override ReadState ReadState
		{
			get
			{
				return (this.entity == null) ? this.source.ReadState : ReadState.Interactive;
			}
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x06000AC1 RID: 2753 RVA: 0x0003822C File Offset: 0x0003642C
		public override IXmlSchemaInfo SchemaInfo
		{
			get
			{
				IXmlSchemaInfo result;
				if (this.entity != null)
				{
					IXmlSchemaInfo schemaInfo = this.entity.SchemaInfo;
					result = schemaInfo;
				}
				else
				{
					result = this.source.SchemaInfo;
				}
				return result;
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x06000AC2 RID: 2754 RVA: 0x00038264 File Offset: 0x00036464
		public override string Value
		{
			get
			{
				return this.Current.Value;
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x06000AC3 RID: 2755 RVA: 0x00038274 File Offset: 0x00036474
		public override string XmlLang
		{
			get
			{
				return this.Current.XmlLang;
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x06000AC4 RID: 2756 RVA: 0x00038284 File Offset: 0x00036484
		public override XmlSpace XmlSpace
		{
			get
			{
				return this.Current.XmlSpace;
			}
		}

		// Token: 0x06000AC5 RID: 2757 RVA: 0x00038294 File Offset: 0x00036494
		public override void Close()
		{
			if (this.entity != null)
			{
				this.entity.Close();
			}
			this.source.Close();
		}

		// Token: 0x06000AC6 RID: 2758 RVA: 0x000382B8 File Offset: 0x000364B8
		public override string GetAttribute(int attributeIndex)
		{
			return this.Current.GetAttribute(attributeIndex);
		}

		// Token: 0x06000AC7 RID: 2759 RVA: 0x000382C8 File Offset: 0x000364C8
		public override string GetAttribute(string name)
		{
			return this.Current.GetAttribute(name);
		}

		// Token: 0x06000AC8 RID: 2760 RVA: 0x000382D8 File Offset: 0x000364D8
		public override string GetAttribute(string name, string namespaceURI)
		{
			return this.Current.GetAttribute(name, namespaceURI);
		}

		// Token: 0x06000AC9 RID: 2761 RVA: 0x000382E8 File Offset: 0x000364E8
		public override string LookupNamespace(string prefix)
		{
			return this.Current.LookupNamespace(prefix);
		}

		// Token: 0x06000ACA RID: 2762 RVA: 0x000382F8 File Offset: 0x000364F8
		public override void MoveToAttribute(int i)
		{
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity.Close();
				this.entity = null;
			}
			this.Current.MoveToAttribute(i);
			this.insideAttribute = true;
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x00038338 File Offset: 0x00036538
		public override bool MoveToAttribute(string name)
		{
			if (this.entity != null && !this.entityInsideAttribute)
			{
				return this.entity.MoveToAttribute(name);
			}
			if (!this.source.MoveToAttribute(name))
			{
				return false;
			}
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity.Close();
				this.entity = null;
			}
			this.insideAttribute = true;
			return true;
		}

		// Token: 0x06000ACC RID: 2764 RVA: 0x000383AC File Offset: 0x000365AC
		public override bool MoveToAttribute(string localName, string namespaceURI)
		{
			if (this.entity != null && !this.entityInsideAttribute)
			{
				return this.entity.MoveToAttribute(localName, namespaceURI);
			}
			if (!this.source.MoveToAttribute(localName, namespaceURI))
			{
				return false;
			}
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity.Close();
				this.entity = null;
			}
			this.insideAttribute = true;
			return true;
		}

		// Token: 0x06000ACD RID: 2765 RVA: 0x00038424 File Offset: 0x00036624
		public override bool MoveToElement()
		{
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity = null;
			}
			if (!this.Current.MoveToElement())
			{
				return false;
			}
			this.insideAttribute = false;
			return true;
		}

		// Token: 0x06000ACE RID: 2766 RVA: 0x00038460 File Offset: 0x00036660
		public override bool MoveToFirstAttribute()
		{
			if (this.entity != null && !this.entityInsideAttribute)
			{
				return this.entity.MoveToFirstAttribute();
			}
			if (!this.source.MoveToFirstAttribute())
			{
				return false;
			}
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity.Close();
				this.entity = null;
			}
			this.insideAttribute = true;
			return true;
		}

		// Token: 0x06000ACF RID: 2767 RVA: 0x000384D4 File Offset: 0x000366D4
		public override bool MoveToNextAttribute()
		{
			if (this.entity != null && !this.entityInsideAttribute)
			{
				return this.entity.MoveToNextAttribute();
			}
			if (!this.source.MoveToNextAttribute())
			{
				return false;
			}
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity.Close();
				this.entity = null;
			}
			this.insideAttribute = true;
			return true;
		}

		// Token: 0x06000AD0 RID: 2768 RVA: 0x00038548 File Offset: 0x00036748
		public override bool Read()
		{
			this.insideAttribute = false;
			if (this.entity != null && (this.entityInsideAttribute || this.entity.EOF))
			{
				this.entity = null;
			}
			if (this.entity != null)
			{
				this.entity.Read();
				return true;
			}
			return this.source.Read();
		}

		// Token: 0x06000AD1 RID: 2769 RVA: 0x000385B0 File Offset: 0x000367B0
		public override bool ReadAttributeValue()
		{
			if (this.entity != null && this.entityInsideAttribute)
			{
				if (!this.entity.EOF)
				{
					this.entity.Read();
					return true;
				}
				this.entity = null;
			}
			return this.Current.ReadAttributeValue();
		}

		// Token: 0x06000AD2 RID: 2770 RVA: 0x00038608 File Offset: 0x00036808
		public override int ReadContentAsBase64(byte[] buffer, int offset, int length)
		{
			if (this.entity != null)
			{
				return this.entity.ReadContentAsBase64(buffer, offset, length);
			}
			return this.source.ReadContentAsBase64(buffer, offset, length);
		}

		// Token: 0x06000AD3 RID: 2771 RVA: 0x00038640 File Offset: 0x00036840
		public override int ReadContentAsBinHex(byte[] buffer, int offset, int length)
		{
			if (this.entity != null)
			{
				return this.entity.ReadContentAsBinHex(buffer, offset, length);
			}
			return this.source.ReadContentAsBinHex(buffer, offset, length);
		}

		// Token: 0x06000AD4 RID: 2772 RVA: 0x00038678 File Offset: 0x00036878
		public override int ReadElementContentAsBase64(byte[] buffer, int offset, int length)
		{
			if (this.entity != null)
			{
				return this.entity.ReadElementContentAsBase64(buffer, offset, length);
			}
			return this.source.ReadElementContentAsBase64(buffer, offset, length);
		}

		// Token: 0x06000AD5 RID: 2773 RVA: 0x000386B0 File Offset: 0x000368B0
		public override int ReadElementContentAsBinHex(byte[] buffer, int offset, int length)
		{
			if (this.entity != null)
			{
				return this.entity.ReadElementContentAsBinHex(buffer, offset, length);
			}
			return this.source.ReadElementContentAsBinHex(buffer, offset, length);
		}

		// Token: 0x06000AD6 RID: 2774 RVA: 0x000386E8 File Offset: 0x000368E8
		public override string ReadString()
		{
			return base.ReadString();
		}

		// Token: 0x06000AD7 RID: 2775 RVA: 0x000386F0 File Offset: 0x000368F0
		public override void ResolveEntity()
		{
			if (this.entity != null)
			{
				this.entity.ResolveEntity();
			}
			else
			{
				if (this.source.NodeType != XmlNodeType.EntityReference)
				{
					throw new InvalidOperationException("The current node is not an Entity Reference");
				}
				this.entity = new XmlNodeReader(this.source, this.insideAttribute);
			}
		}

		// Token: 0x06000AD8 RID: 2776 RVA: 0x0003874C File Offset: 0x0003694C
		public override void Skip()
		{
			if (this.entity != null && this.entityInsideAttribute)
			{
				this.entity = null;
			}
			this.Current.Skip();
		}

		// Token: 0x04000548 RID: 1352
		private XmlReader entity;

		// Token: 0x04000549 RID: 1353
		private XmlNodeReaderImpl source;

		// Token: 0x0400054A RID: 1354
		private bool entityInsideAttribute;

		// Token: 0x0400054B RID: 1355
		private bool insideAttribute;
	}
}
