﻿using System;

namespace System.Xml
{
	// Token: 0x020000E2 RID: 226
	public enum ReadState
	{
		// Token: 0x04000487 RID: 1159
		Initial,
		// Token: 0x04000488 RID: 1160
		Interactive,
		// Token: 0x04000489 RID: 1161
		Error,
		// Token: 0x0400048A RID: 1162
		EndOfFile,
		// Token: 0x0400048B RID: 1163
		Closed
	}
}
