﻿using System;

namespace System.Xml
{
	// Token: 0x02000105 RID: 261
	public enum XmlNodeChangedAction
	{
		// Token: 0x0400052F RID: 1327
		Insert,
		// Token: 0x04000530 RID: 1328
		Remove,
		// Token: 0x04000531 RID: 1329
		Change
	}
}
