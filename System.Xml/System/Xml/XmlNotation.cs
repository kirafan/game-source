﻿using System;

namespace System.Xml
{
	// Token: 0x020000EE RID: 238
	public class XmlNotation : XmlNode
	{
		// Token: 0x06000896 RID: 2198 RVA: 0x0002F7B0 File Offset: 0x0002D9B0
		internal XmlNotation(string localName, string prefix, string publicId, string systemId, XmlDocument doc) : base(doc)
		{
			this.localName = doc.NameTable.Add(localName);
			this.prefix = doc.NameTable.Add(prefix);
			this.publicId = publicId;
			this.systemId = systemId;
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000897 RID: 2199 RVA: 0x0002F7FC File Offset: 0x0002D9FC
		// (set) Token: 0x06000898 RID: 2200 RVA: 0x0002F804 File Offset: 0x0002DA04
		public override string InnerXml
		{
			get
			{
				return string.Empty;
			}
			set
			{
				throw new InvalidOperationException("This operation is not allowed.");
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000899 RID: 2201 RVA: 0x0002F810 File Offset: 0x0002DA10
		public override bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x0600089A RID: 2202 RVA: 0x0002F814 File Offset: 0x0002DA14
		public override string LocalName
		{
			get
			{
				return this.localName;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x0600089B RID: 2203 RVA: 0x0002F81C File Offset: 0x0002DA1C
		public override string Name
		{
			get
			{
				return (!(this.prefix != string.Empty)) ? this.localName : (this.prefix + ":" + this.localName);
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x0600089C RID: 2204 RVA: 0x0002F860 File Offset: 0x0002DA60
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Notation;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x0600089D RID: 2205 RVA: 0x0002F864 File Offset: 0x0002DA64
		public override string OuterXml
		{
			get
			{
				return string.Empty;
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x0600089E RID: 2206 RVA: 0x0002F86C File Offset: 0x0002DA6C
		public string PublicId
		{
			get
			{
				if (this.publicId != null)
				{
					return this.publicId;
				}
				return null;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0002F884 File Offset: 0x0002DA84
		public string SystemId
		{
			get
			{
				if (this.systemId != null)
				{
					return this.systemId;
				}
				return null;
			}
		}

		// Token: 0x060008A0 RID: 2208 RVA: 0x0002F89C File Offset: 0x0002DA9C
		public override XmlNode CloneNode(bool deep)
		{
			throw new InvalidOperationException("This operation is not allowed.");
		}

		// Token: 0x060008A1 RID: 2209 RVA: 0x0002F8A8 File Offset: 0x0002DAA8
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x060008A2 RID: 2210 RVA: 0x0002F8AC File Offset: 0x0002DAAC
		public override void WriteTo(XmlWriter w)
		{
		}

		// Token: 0x040004BC RID: 1212
		private string localName;

		// Token: 0x040004BD RID: 1213
		private string publicId;

		// Token: 0x040004BE RID: 1214
		private string systemId;

		// Token: 0x040004BF RID: 1215
		private string prefix;
	}
}
