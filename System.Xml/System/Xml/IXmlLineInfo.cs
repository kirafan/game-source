﻿using System;

namespace System.Xml
{
	// Token: 0x020000DA RID: 218
	public interface IXmlLineInfo
	{
		// Token: 0x1700022D RID: 557
		// (get) Token: 0x060007E5 RID: 2021
		int LineNumber { get; }

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x060007E6 RID: 2022
		int LinePosition { get; }

		// Token: 0x060007E7 RID: 2023
		bool HasLineInfo();
	}
}
