﻿using System;

namespace System.Xml
{
	// Token: 0x020000B4 RID: 180
	public enum ConformanceLevel
	{
		// Token: 0x040003C9 RID: 969
		Auto,
		// Token: 0x040003CA RID: 970
		Fragment,
		// Token: 0x040003CB RID: 971
		Document
	}
}
