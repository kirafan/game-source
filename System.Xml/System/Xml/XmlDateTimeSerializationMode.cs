﻿using System;

namespace System.Xml
{
	// Token: 0x020000F9 RID: 249
	public enum XmlDateTimeSerializationMode
	{
		// Token: 0x040004FF RID: 1279
		Local,
		// Token: 0x04000500 RID: 1280
		Utc,
		// Token: 0x04000501 RID: 1281
		Unspecified,
		// Token: 0x04000502 RID: 1282
		RoundtripKind
	}
}
