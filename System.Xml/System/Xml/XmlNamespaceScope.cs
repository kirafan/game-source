﻿using System;

namespace System.Xml
{
	// Token: 0x020000FF RID: 255
	public enum XmlNamespaceScope
	{
		// Token: 0x04000512 RID: 1298
		All,
		// Token: 0x04000513 RID: 1299
		ExcludeXml,
		// Token: 0x04000514 RID: 1300
		Local
	}
}
