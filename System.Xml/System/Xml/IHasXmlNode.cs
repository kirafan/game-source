﻿using System;

namespace System.Xml
{
	// Token: 0x020000D9 RID: 217
	public interface IHasXmlNode
	{
		// Token: 0x060007E4 RID: 2020
		XmlNode GetNode();
	}
}
