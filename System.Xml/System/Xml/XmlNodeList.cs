﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System.Xml
{
	// Token: 0x02000107 RID: 263
	public abstract class XmlNodeList : IEnumerable
	{
		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000A92 RID: 2706
		public abstract int Count { get; }

		// Token: 0x170002F9 RID: 761
		[IndexerName("ItemOf")]
		public virtual XmlNode this[int i]
		{
			get
			{
				return this.Item(i);
			}
		}

		// Token: 0x06000A94 RID: 2708
		public abstract IEnumerator GetEnumerator();

		// Token: 0x06000A95 RID: 2709
		public abstract XmlNode Item(int index);
	}
}
