﻿using System;

namespace System.Xml
{
	// Token: 0x020000E4 RID: 228
	public enum WhitespaceHandling
	{
		// Token: 0x04000493 RID: 1171
		All,
		// Token: 0x04000494 RID: 1172
		Significant,
		// Token: 0x04000495 RID: 1173
		None
	}
}
