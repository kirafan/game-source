﻿using System;

namespace System.Xml
{
	// Token: 0x020000E3 RID: 227
	public enum ValidationType
	{
		// Token: 0x0400048D RID: 1165
		None,
		// Token: 0x0400048E RID: 1166
		[Obsolete]
		Auto,
		// Token: 0x0400048F RID: 1167
		DTD,
		// Token: 0x04000490 RID: 1168
		[Obsolete]
		XDR,
		// Token: 0x04000491 RID: 1169
		Schema
	}
}
