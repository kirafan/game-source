﻿using System;

namespace System.Xml
{
	// Token: 0x020000E1 RID: 225
	public enum NewLineHandling
	{
		// Token: 0x04000483 RID: 1155
		Replace,
		// Token: 0x04000484 RID: 1156
		Entitize,
		// Token: 0x04000485 RID: 1157
		None
	}
}
