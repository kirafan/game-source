﻿using System;

namespace System.Xml
{
	// Token: 0x0200010D RID: 269
	public enum XmlNodeOrder
	{
		// Token: 0x04000544 RID: 1348
		Before,
		// Token: 0x04000545 RID: 1349
		After,
		// Token: 0x04000546 RID: 1350
		Same,
		// Token: 0x04000547 RID: 1351
		Unknown
	}
}
