﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x020001AD RID: 429
	public interface IXsltContextVariable
	{
		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001185 RID: 4485
		bool IsLocal { get; }

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001186 RID: 4486
		bool IsParam { get; }

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06001187 RID: 4487
		XPathResultType VariableType { get; }

		// Token: 0x06001188 RID: 4488
		object Evaluate(XsltContext xsltContext);
	}
}
