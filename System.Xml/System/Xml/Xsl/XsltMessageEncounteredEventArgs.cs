﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x020001B7 RID: 439
	public abstract class XsltMessageEncounteredEventArgs : EventArgs
	{
		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x060011FD RID: 4605
		public abstract string Message { get; }
	}
}
