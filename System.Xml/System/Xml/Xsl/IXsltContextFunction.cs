﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x020001AC RID: 428
	public interface IXsltContextFunction
	{
		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06001180 RID: 4480
		XPathResultType[] ArgTypes { get; }

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001181 RID: 4481
		int Maxargs { get; }

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001182 RID: 4482
		int Minargs { get; }

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001183 RID: 4483
		XPathResultType ReturnType { get; }

		// Token: 0x06001184 RID: 4484
		object Invoke(XsltContext xsltContext, object[] args, XPathNavigator docContext);
	}
}
