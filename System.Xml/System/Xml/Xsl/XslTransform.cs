﻿using System;
using System.IO;
using System.Security.Policy;
using System.Xml.XPath;
using Mono.Xml.Xsl;

namespace System.Xml.Xsl
{
	// Token: 0x020001B1 RID: 433
	public sealed class XslTransform
	{
		// Token: 0x060011AD RID: 4525 RVA: 0x00050EB8 File Offset: 0x0004F0B8
		public XslTransform() : this(XslTransform.GetDefaultDebugger())
		{
		}

		// Token: 0x060011AE RID: 4526 RVA: 0x00050EC8 File Offset: 0x0004F0C8
		internal XslTransform(object debugger)
		{
			this.debugger = debugger;
		}

		// Token: 0x060011AF RID: 4527 RVA: 0x00050EE4 File Offset: 0x0004F0E4
		static XslTransform()
		{
			string environmentVariable = Environment.GetEnvironmentVariable("MONO_XSLT_STACK_FRAME");
			string text = environmentVariable;
			switch (text)
			{
			case "stdout":
				XslTransform.TemplateStackFrameOutput = Console.Out;
				break;
			case "stderr":
				XslTransform.TemplateStackFrameOutput = Console.Error;
				break;
			case "error":
				XslTransform.TemplateStackFrameError = true;
				break;
			}
		}

		// Token: 0x060011B0 RID: 4528 RVA: 0x00050F94 File Offset: 0x0004F194
		private static object GetDefaultDebugger()
		{
			string text = null;
			try
			{
				text = Environment.GetEnvironmentVariable("MONO_XSLT_DEBUGGER");
			}
			catch (Exception)
			{
			}
			if (text == null)
			{
				return null;
			}
			if (text == "simple")
			{
				return new SimpleXsltDebugger();
			}
			return Activator.CreateInstance(Type.GetType(text));
		}

		// Token: 0x1700051C RID: 1308
		// (set) Token: 0x060011B1 RID: 4529 RVA: 0x00051000 File Offset: 0x0004F200
		[MonoTODO]
		public XmlResolver XmlResolver
		{
			set
			{
				this.xmlResolver = value;
			}
		}

		// Token: 0x060011B2 RID: 4530 RVA: 0x0005100C File Offset: 0x0004F20C
		public XmlReader Transform(IXPathNavigable input, XsltArgumentList args)
		{
			return this.Transform(input.CreateNavigator(), args, this.xmlResolver);
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x00051024 File Offset: 0x0004F224
		public XmlReader Transform(IXPathNavigable input, XsltArgumentList args, XmlResolver resolver)
		{
			return this.Transform(input.CreateNavigator(), args, resolver);
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x00051034 File Offset: 0x0004F234
		public XmlReader Transform(XPathNavigator input, XsltArgumentList args)
		{
			return this.Transform(input, args, this.xmlResolver);
		}

		// Token: 0x060011B5 RID: 4533 RVA: 0x00051044 File Offset: 0x0004F244
		public XmlReader Transform(XPathNavigator input, XsltArgumentList args, XmlResolver resolver)
		{
			MemoryStream memoryStream = new MemoryStream();
			this.Transform(input, args, new XmlTextWriter(memoryStream, null), resolver);
			memoryStream.Position = 0L;
			return new XmlTextReader(memoryStream, XmlNodeType.Element, null);
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x00051078 File Offset: 0x0004F278
		public void Transform(IXPathNavigable input, XsltArgumentList args, TextWriter output)
		{
			this.Transform(input.CreateNavigator(), args, output, this.xmlResolver);
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x00051090 File Offset: 0x0004F290
		public void Transform(IXPathNavigable input, XsltArgumentList args, TextWriter output, XmlResolver resolver)
		{
			this.Transform(input.CreateNavigator(), args, output, resolver);
		}

		// Token: 0x060011B8 RID: 4536 RVA: 0x000510A4 File Offset: 0x0004F2A4
		public void Transform(IXPathNavigable input, XsltArgumentList args, Stream output)
		{
			this.Transform(input.CreateNavigator(), args, output, this.xmlResolver);
		}

		// Token: 0x060011B9 RID: 4537 RVA: 0x000510BC File Offset: 0x0004F2BC
		public void Transform(IXPathNavigable input, XsltArgumentList args, Stream output, XmlResolver resolver)
		{
			this.Transform(input.CreateNavigator(), args, output, resolver);
		}

		// Token: 0x060011BA RID: 4538 RVA: 0x000510D0 File Offset: 0x0004F2D0
		public void Transform(IXPathNavigable input, XsltArgumentList args, XmlWriter output)
		{
			this.Transform(input.CreateNavigator(), args, output, this.xmlResolver);
		}

		// Token: 0x060011BB RID: 4539 RVA: 0x000510E8 File Offset: 0x0004F2E8
		public void Transform(IXPathNavigable input, XsltArgumentList args, XmlWriter output, XmlResolver resolver)
		{
			this.Transform(input.CreateNavigator(), args, output, resolver);
		}

		// Token: 0x060011BC RID: 4540 RVA: 0x000510FC File Offset: 0x0004F2FC
		public void Transform(XPathNavigator input, XsltArgumentList args, XmlWriter output)
		{
			this.Transform(input, args, output, this.xmlResolver);
		}

		// Token: 0x060011BD RID: 4541 RVA: 0x00051110 File Offset: 0x0004F310
		public void Transform(XPathNavigator input, XsltArgumentList args, XmlWriter output, XmlResolver resolver)
		{
			if (this.s == null)
			{
				throw new XsltException("No stylesheet was loaded.", null);
			}
			Outputter outputtter = new GenericOutputter(output, this.s.Outputs, null);
			new XslTransformProcessor(this.s, this.debugger).Process(input, outputtter, args, resolver);
			output.Flush();
		}

		// Token: 0x060011BE RID: 4542 RVA: 0x00051168 File Offset: 0x0004F368
		public void Transform(XPathNavigator input, XsltArgumentList args, Stream output)
		{
			this.Transform(input, args, output, this.xmlResolver);
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x0005117C File Offset: 0x0004F37C
		public void Transform(XPathNavigator input, XsltArgumentList args, Stream output, XmlResolver resolver)
		{
			XslOutput xslOutput = (XslOutput)this.s.Outputs[string.Empty];
			this.Transform(input, args, new StreamWriter(output, xslOutput.Encoding), resolver);
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x000511BC File Offset: 0x0004F3BC
		public void Transform(XPathNavigator input, XsltArgumentList args, TextWriter output)
		{
			this.Transform(input, args, output, this.xmlResolver);
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x000511D0 File Offset: 0x0004F3D0
		public void Transform(XPathNavigator input, XsltArgumentList args, TextWriter output, XmlResolver resolver)
		{
			if (this.s == null)
			{
				throw new XsltException("No stylesheet was loaded.", null);
			}
			Outputter outputter = new GenericOutputter(output, this.s.Outputs, output.Encoding);
			new XslTransformProcessor(this.s, this.debugger).Process(input, outputter, args, resolver);
			outputter.Done();
			output.Flush();
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x00051234 File Offset: 0x0004F434
		public void Transform(string inputfile, string outputfile)
		{
			this.Transform(inputfile, outputfile, this.xmlResolver);
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x00051244 File Offset: 0x0004F444
		public void Transform(string inputfile, string outputfile, XmlResolver resolver)
		{
			using (Stream stream = new FileStream(outputfile, FileMode.Create, FileAccess.ReadWrite))
			{
				this.Transform(new XPathDocument(inputfile).CreateNavigator(), null, stream, resolver);
			}
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x0005129C File Offset: 0x0004F49C
		public void Load(string url)
		{
			this.Load(url, null);
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x000512A8 File Offset: 0x0004F4A8
		public void Load(string url, XmlResolver resolver)
		{
			XmlResolver xmlResolver = resolver;
			if (xmlResolver == null)
			{
				xmlResolver = new XmlUrlResolver();
			}
			Uri uri = xmlResolver.ResolveUri(null, url);
			using (Stream stream = xmlResolver.GetEntity(uri, null, typeof(Stream)) as Stream)
			{
				this.Load(new XPathDocument(new XmlValidatingReader(new XmlTextReader(uri.ToString(), stream)
				{
					XmlResolver = xmlResolver
				})
				{
					XmlResolver = xmlResolver,
					ValidationType = ValidationType.None
				}, XmlSpace.Preserve).CreateNavigator(), resolver, null);
			}
		}

		// Token: 0x060011C6 RID: 4550 RVA: 0x00051354 File Offset: 0x0004F554
		public void Load(XmlReader stylesheet)
		{
			this.Load(stylesheet, null, null);
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x00051360 File Offset: 0x0004F560
		public void Load(XmlReader stylesheet, XmlResolver resolver)
		{
			this.Load(stylesheet, resolver, null);
		}

		// Token: 0x060011C8 RID: 4552 RVA: 0x0005136C File Offset: 0x0004F56C
		public void Load(XPathNavigator stylesheet)
		{
			this.Load(stylesheet, null, null);
		}

		// Token: 0x060011C9 RID: 4553 RVA: 0x00051378 File Offset: 0x0004F578
		public void Load(XPathNavigator stylesheet, XmlResolver resolver)
		{
			this.Load(stylesheet, resolver, null);
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x00051384 File Offset: 0x0004F584
		public void Load(IXPathNavigable stylesheet)
		{
			this.Load(stylesheet.CreateNavigator(), null);
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x00051394 File Offset: 0x0004F594
		public void Load(IXPathNavigable stylesheet, XmlResolver resolver)
		{
			this.Load(stylesheet.CreateNavigator(), resolver);
		}

		// Token: 0x060011CC RID: 4556 RVA: 0x000513A4 File Offset: 0x0004F5A4
		public void Load(IXPathNavigable stylesheet, XmlResolver resolver, Evidence evidence)
		{
			this.Load(stylesheet.CreateNavigator(), resolver, evidence);
		}

		// Token: 0x060011CD RID: 4557 RVA: 0x000513B4 File Offset: 0x0004F5B4
		public void Load(XPathNavigator stylesheet, XmlResolver resolver, Evidence evidence)
		{
			this.s = new Compiler(this.debugger).Compile(stylesheet, resolver, evidence);
		}

		// Token: 0x060011CE RID: 4558 RVA: 0x000513D0 File Offset: 0x0004F5D0
		public void Load(XmlReader stylesheet, XmlResolver resolver, Evidence evidence)
		{
			this.Load(new XPathDocument(stylesheet, XmlSpace.Preserve).CreateNavigator(), resolver, evidence);
		}

		// Token: 0x04000751 RID: 1873
		internal static readonly bool TemplateStackFrameError;

		// Token: 0x04000752 RID: 1874
		internal static readonly TextWriter TemplateStackFrameOutput;

		// Token: 0x04000753 RID: 1875
		private object debugger;

		// Token: 0x04000754 RID: 1876
		private CompiledStylesheet s;

		// Token: 0x04000755 RID: 1877
		private XmlResolver xmlResolver = new XmlUrlResolver();
	}
}
