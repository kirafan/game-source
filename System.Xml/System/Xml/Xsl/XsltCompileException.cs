﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x020001B3 RID: 435
	[Serializable]
	public class XsltCompileException : XsltException
	{
		// Token: 0x060011DA RID: 4570 RVA: 0x000516D8 File Offset: 0x0004F8D8
		public XsltCompileException()
		{
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x000516E0 File Offset: 0x0004F8E0
		public XsltCompileException(string message) : base(message)
		{
		}

		// Token: 0x060011DC RID: 4572 RVA: 0x000516EC File Offset: 0x0004F8EC
		public XsltCompileException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060011DD RID: 4573 RVA: 0x000516F8 File Offset: 0x0004F8F8
		protected XsltCompileException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x00051704 File Offset: 0x0004F904
		public XsltCompileException(Exception inner, string sourceUri, int lineNumber, int linePosition) : base((lineNumber == 0) ? "{0}." : "{0} at {1}({2},{3}). See InnerException for details.", "XSLT compile error", inner, lineNumber, linePosition, sourceUri)
		{
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x00051738 File Offset: 0x0004F938
		internal XsltCompileException(string message, Exception innerException, XPathNavigator nav) : base(message, innerException, nav)
		{
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x00051744 File Offset: 0x0004F944
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
}
