﻿using System;

namespace System.Xml.Xsl
{
	// Token: 0x020002D2 RID: 722
	// (Invoke) Token: 0x06001E9D RID: 7837
	public delegate void XsltMessageEncounteredEventHandler(object sender, XsltMessageEncounteredEventArgs e);
}
