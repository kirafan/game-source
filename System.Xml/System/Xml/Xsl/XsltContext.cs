﻿using System;
using System.Xml.XPath;

namespace System.Xml.Xsl
{
	// Token: 0x020001B4 RID: 436
	public abstract class XsltContext : XmlNamespaceManager
	{
		// Token: 0x060011E1 RID: 4577 RVA: 0x00051750 File Offset: 0x0004F950
		protected XsltContext() : base(new NameTable())
		{
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x00051760 File Offset: 0x0004F960
		protected XsltContext(NameTable table) : base(table)
		{
		}

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x060011E3 RID: 4579
		public abstract bool Whitespace { get; }

		// Token: 0x060011E4 RID: 4580
		public abstract bool PreserveWhitespace(XPathNavigator nav);

		// Token: 0x060011E5 RID: 4581
		public abstract int CompareDocument(string baseUri, string nextbaseUri);

		// Token: 0x060011E6 RID: 4582
		public abstract IXsltContextFunction ResolveFunction(string prefix, string name, XPathResultType[] argTypes);

		// Token: 0x060011E7 RID: 4583
		public abstract IXsltContextVariable ResolveVariable(string prefix, string name);

		// Token: 0x060011E8 RID: 4584 RVA: 0x0005176C File Offset: 0x0004F96C
		internal virtual IXsltContextVariable ResolveVariable(XmlQualifiedName name)
		{
			return this.ResolveVariable(this.LookupPrefix(name.Namespace), name.Name);
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x00051794 File Offset: 0x0004F994
		internal virtual IXsltContextFunction ResolveFunction(XmlQualifiedName name, XPathResultType[] argTypes)
		{
			return this.ResolveFunction(name.Name, name.Namespace, argTypes);
		}
	}
}
