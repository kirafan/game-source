﻿using System;

namespace System.Xml
{
	// Token: 0x02000111 RID: 273
	public enum XmlOutputMethod
	{
		// Token: 0x0400056C RID: 1388
		Xml,
		// Token: 0x0400056D RID: 1389
		Html,
		// Token: 0x0400056E RID: 1390
		Text,
		// Token: 0x0400056F RID: 1391
		AutoDetect
	}
}
