﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x02000135 RID: 309
	public interface IXPathNavigable
	{
		// Token: 0x06000E65 RID: 3685
		XPathNavigator CreateNavigator();
	}
}
