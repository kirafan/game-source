﻿using System;
using System.Collections;

namespace System.Xml.XPath
{
	// Token: 0x0200013B RID: 315
	public abstract class XPathNodeIterator : IEnumerable, ICloneable
	{
		// Token: 0x06000F0B RID: 3851 RVA: 0x000491B4 File Offset: 0x000473B4
		object ICloneable.Clone()
		{
			return this.Clone();
		}

		// Token: 0x17000442 RID: 1090
		// (get) Token: 0x06000F0C RID: 3852 RVA: 0x000491BC File Offset: 0x000473BC
		public virtual int Count
		{
			get
			{
				if (this._count == -1)
				{
					XPathNodeIterator xpathNodeIterator = this.Clone();
					while (xpathNodeIterator.MoveNext())
					{
					}
					this._count = xpathNodeIterator.CurrentPosition;
				}
				return this._count;
			}
		}

		// Token: 0x17000443 RID: 1091
		// (get) Token: 0x06000F0D RID: 3853
		public abstract XPathNavigator Current { get; }

		// Token: 0x17000444 RID: 1092
		// (get) Token: 0x06000F0E RID: 3854
		public abstract int CurrentPosition { get; }

		// Token: 0x06000F0F RID: 3855
		public abstract XPathNodeIterator Clone();

		// Token: 0x06000F10 RID: 3856 RVA: 0x00049200 File Offset: 0x00047400
		public virtual IEnumerator GetEnumerator()
		{
			while (this.MoveNext())
			{
				XPathNavigator xpathNavigator = this.Current;
				yield return xpathNavigator;
			}
			yield break;
		}

		// Token: 0x06000F11 RID: 3857
		public abstract bool MoveNext();

		// Token: 0x0400068E RID: 1678
		private int _count = -1;
	}
}
