﻿using System;
using System.Xml.Schema;

namespace System.Xml.XPath
{
	// Token: 0x02000139 RID: 313
	public abstract class XPathItem
	{
		// Token: 0x06000EFE RID: 3838 RVA: 0x00049198 File Offset: 0x00047398
		public virtual object ValueAs(Type type)
		{
			return this.ValueAs(type, null);
		}

		// Token: 0x06000EFF RID: 3839
		public abstract object ValueAs(Type type, IXmlNamespaceResolver nsResolver);

		// Token: 0x17000438 RID: 1080
		// (get) Token: 0x06000F00 RID: 3840
		public abstract bool IsNode { get; }

		// Token: 0x17000439 RID: 1081
		// (get) Token: 0x06000F01 RID: 3841
		public abstract object TypedValue { get; }

		// Token: 0x1700043A RID: 1082
		// (get) Token: 0x06000F02 RID: 3842
		public abstract string Value { get; }

		// Token: 0x1700043B RID: 1083
		// (get) Token: 0x06000F03 RID: 3843
		public abstract bool ValueAsBoolean { get; }

		// Token: 0x1700043C RID: 1084
		// (get) Token: 0x06000F04 RID: 3844
		public abstract DateTime ValueAsDateTime { get; }

		// Token: 0x1700043D RID: 1085
		// (get) Token: 0x06000F05 RID: 3845
		public abstract double ValueAsDouble { get; }

		// Token: 0x1700043E RID: 1086
		// (get) Token: 0x06000F06 RID: 3846
		public abstract int ValueAsInt { get; }

		// Token: 0x1700043F RID: 1087
		// (get) Token: 0x06000F07 RID: 3847
		public abstract long ValueAsLong { get; }

		// Token: 0x17000440 RID: 1088
		// (get) Token: 0x06000F08 RID: 3848
		public abstract Type ValueType { get; }

		// Token: 0x17000441 RID: 1089
		// (get) Token: 0x06000F09 RID: 3849
		public abstract XmlSchemaType XmlType { get; }
	}
}
