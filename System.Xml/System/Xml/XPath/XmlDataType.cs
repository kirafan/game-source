﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x0200013E RID: 318
	public enum XmlDataType
	{
		// Token: 0x040006A3 RID: 1699
		Text = 1,
		// Token: 0x040006A4 RID: 1700
		Number
	}
}
