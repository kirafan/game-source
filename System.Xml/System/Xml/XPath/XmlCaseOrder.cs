﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x02000140 RID: 320
	public enum XmlCaseOrder
	{
		// Token: 0x040006A9 RID: 1705
		None,
		// Token: 0x040006AA RID: 1706
		UpperFirst,
		// Token: 0x040006AB RID: 1707
		LowerFirst
	}
}
