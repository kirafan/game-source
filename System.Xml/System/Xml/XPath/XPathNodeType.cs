﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x0200013D RID: 317
	public enum XPathNodeType
	{
		// Token: 0x04000698 RID: 1688
		Root,
		// Token: 0x04000699 RID: 1689
		Element,
		// Token: 0x0400069A RID: 1690
		Attribute,
		// Token: 0x0400069B RID: 1691
		Namespace,
		// Token: 0x0400069C RID: 1692
		Text,
		// Token: 0x0400069D RID: 1693
		SignificantWhitespace,
		// Token: 0x0400069E RID: 1694
		Whitespace,
		// Token: 0x0400069F RID: 1695
		ProcessingInstruction,
		// Token: 0x040006A0 RID: 1696
		Comment,
		// Token: 0x040006A1 RID: 1697
		All
	}
}
