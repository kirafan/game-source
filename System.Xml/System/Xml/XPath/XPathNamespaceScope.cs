﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x0200013A RID: 314
	public enum XPathNamespaceScope
	{
		// Token: 0x0400068B RID: 1675
		All,
		// Token: 0x0400068C RID: 1676
		ExcludeXml,
		// Token: 0x0400068D RID: 1677
		Local
	}
}
