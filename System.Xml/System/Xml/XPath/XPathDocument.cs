﻿using System;
using System.IO;
using Mono.Xml.XPath;

namespace System.Xml.XPath
{
	// Token: 0x02000141 RID: 321
	public class XPathDocument : IXPathNavigable
	{
		// Token: 0x06000F12 RID: 3858 RVA: 0x0004921C File Offset: 0x0004741C
		public XPathDocument(Stream stream)
		{
			this.Initialize(new XmlValidatingReader(new XmlTextReader(stream))
			{
				ValidationType = ValidationType.None
			}, XmlSpace.None);
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x0004924C File Offset: 0x0004744C
		public XPathDocument(string uri) : this(uri, XmlSpace.None)
		{
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x00049258 File Offset: 0x00047458
		public XPathDocument(TextReader reader)
		{
			this.Initialize(new XmlValidatingReader(new XmlTextReader(reader))
			{
				ValidationType = ValidationType.None
			}, XmlSpace.None);
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x00049288 File Offset: 0x00047488
		public XPathDocument(XmlReader reader) : this(reader, XmlSpace.None)
		{
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00049294 File Offset: 0x00047494
		public XPathDocument(string uri, XmlSpace space)
		{
			XmlValidatingReader xmlValidatingReader = null;
			try
			{
				xmlValidatingReader = new XmlValidatingReader(new XmlTextReader(uri));
				xmlValidatingReader.ValidationType = ValidationType.None;
				this.Initialize(xmlValidatingReader, space);
			}
			finally
			{
				if (xmlValidatingReader != null)
				{
					xmlValidatingReader.Close();
				}
			}
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x000492F4 File Offset: 0x000474F4
		public XPathDocument(XmlReader reader, XmlSpace space)
		{
			this.Initialize(reader, space);
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x00049304 File Offset: 0x00047504
		private void Initialize(XmlReader reader, XmlSpace space)
		{
			this.document = new DTMXPathDocumentBuilder2(reader, space).CreateDocument();
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x00049318 File Offset: 0x00047518
		public XPathNavigator CreateNavigator()
		{
			return this.document.CreateNavigator();
		}

		// Token: 0x040006AC RID: 1708
		private IXPathNavigable document;
	}
}
