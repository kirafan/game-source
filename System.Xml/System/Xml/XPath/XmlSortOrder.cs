﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x0200013F RID: 319
	public enum XmlSortOrder
	{
		// Token: 0x040006A6 RID: 1702
		Ascending = 1,
		// Token: 0x040006A7 RID: 1703
		Descending
	}
}
