﻿using System;
using System.Collections;
using System.Xml.Xsl;
using Mono.Xml.XPath;

namespace System.Xml.XPath
{
	// Token: 0x02000138 RID: 312
	public abstract class XPathExpression
	{
		// Token: 0x06000EF2 RID: 3826 RVA: 0x00049144 File Offset: 0x00047344
		internal XPathExpression()
		{
		}

		// Token: 0x17000436 RID: 1078
		// (get) Token: 0x06000EF3 RID: 3827
		public abstract string Expression { get; }

		// Token: 0x17000437 RID: 1079
		// (get) Token: 0x06000EF4 RID: 3828
		public abstract XPathResultType ReturnType { get; }

		// Token: 0x06000EF5 RID: 3829
		public abstract void AddSort(object expr, IComparer comparer);

		// Token: 0x06000EF6 RID: 3830
		public abstract void AddSort(object expr, XmlSortOrder order, XmlCaseOrder caseOrder, string lang, XmlDataType dataType);

		// Token: 0x06000EF7 RID: 3831
		public abstract XPathExpression Clone();

		// Token: 0x06000EF8 RID: 3832
		public abstract void SetContext(XmlNamespaceManager nsManager);

		// Token: 0x06000EF9 RID: 3833 RVA: 0x0004914C File Offset: 0x0004734C
		public static XPathExpression Compile(string xpath)
		{
			return XPathExpression.Compile(xpath, null, null);
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x00049158 File Offset: 0x00047358
		public static XPathExpression Compile(string xpath, IXmlNamespaceResolver nsmgr)
		{
			return XPathExpression.Compile(xpath, nsmgr, null);
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x00049164 File Offset: 0x00047364
		internal static XPathExpression Compile(string xpath, IXmlNamespaceResolver nsmgr, IStaticXsltContext ctx)
		{
			XPathParser xpathParser = new XPathParser(ctx);
			CompiledExpression compiledExpression = new CompiledExpression(xpath, xpathParser.Compile(xpath));
			compiledExpression.SetContext(nsmgr);
			return compiledExpression;
		}

		// Token: 0x06000EFC RID: 3836
		public abstract void SetContext(IXmlNamespaceResolver nsResolver);
	}
}
