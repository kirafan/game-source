﻿using System;

namespace System.Xml.XPath
{
	// Token: 0x0200013C RID: 316
	public enum XPathResultType
	{
		// Token: 0x04000690 RID: 1680
		Number,
		// Token: 0x04000691 RID: 1681
		String,
		// Token: 0x04000692 RID: 1682
		Boolean,
		// Token: 0x04000693 RID: 1683
		NodeSet,
		// Token: 0x04000694 RID: 1684
		[MonoFIX("MS.NET: 1")]
		Navigator,
		// Token: 0x04000695 RID: 1685
		Any,
		// Token: 0x04000696 RID: 1686
		Error
	}
}
