﻿using System;

namespace System.Xml
{
	// Token: 0x020000E5 RID: 229
	public enum WriteState
	{
		// Token: 0x04000497 RID: 1175
		Start,
		// Token: 0x04000498 RID: 1176
		Prolog,
		// Token: 0x04000499 RID: 1177
		Element,
		// Token: 0x0400049A RID: 1178
		Attribute,
		// Token: 0x0400049B RID: 1179
		Content,
		// Token: 0x0400049C RID: 1180
		Closed,
		// Token: 0x0400049D RID: 1181
		Error
	}
}
