﻿using System;

namespace System.Xml
{
	// Token: 0x02000106 RID: 262
	public class XmlNodeChangedEventArgs : EventArgs
	{
		// Token: 0x06000A8A RID: 2698 RVA: 0x00037B74 File Offset: 0x00035D74
		public XmlNodeChangedEventArgs(XmlNode node, XmlNode oldParent, XmlNode newParent, string oldValue, string newValue, XmlNodeChangedAction action)
		{
			this._node = node;
			this._oldParent = oldParent;
			this._newParent = newParent;
			this._oldValue = oldValue;
			this._newValue = newValue;
			this._action = action;
		}

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06000A8B RID: 2699 RVA: 0x00037BAC File Offset: 0x00035DAC
		public XmlNodeChangedAction Action
		{
			get
			{
				return this._action;
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06000A8C RID: 2700 RVA: 0x00037BB4 File Offset: 0x00035DB4
		public XmlNode Node
		{
			get
			{
				return this._node;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000A8D RID: 2701 RVA: 0x00037BBC File Offset: 0x00035DBC
		public XmlNode OldParent
		{
			get
			{
				return this._oldParent;
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000A8E RID: 2702 RVA: 0x00037BC4 File Offset: 0x00035DC4
		public XmlNode NewParent
		{
			get
			{
				return this._newParent;
			}
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000A8F RID: 2703 RVA: 0x00037BCC File Offset: 0x00035DCC
		public string OldValue
		{
			get
			{
				return (this._oldValue == null) ? this._node.Value : this._oldValue;
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x00037BF0 File Offset: 0x00035DF0
		public string NewValue
		{
			get
			{
				return (this._newValue == null) ? this._node.Value : this._newValue;
			}
		}

		// Token: 0x04000532 RID: 1330
		private XmlNode _oldParent;

		// Token: 0x04000533 RID: 1331
		private XmlNode _newParent;

		// Token: 0x04000534 RID: 1332
		private XmlNodeChangedAction _action;

		// Token: 0x04000535 RID: 1333
		private XmlNode _node;

		// Token: 0x04000536 RID: 1334
		private string _oldValue;

		// Token: 0x04000537 RID: 1335
		private string _newValue;
	}
}
