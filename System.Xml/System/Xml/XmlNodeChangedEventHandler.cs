﻿using System;

namespace System.Xml
{
	// Token: 0x020002D1 RID: 721
	// (Invoke) Token: 0x06001E99 RID: 7833
	public delegate void XmlNodeChangedEventHandler(object sender, XmlNodeChangedEventArgs e);
}
