﻿using System;
using System.Globalization;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml
{
	// Token: 0x020000F6 RID: 246
	[Serializable]
	public class XmlException : SystemException
	{
		// Token: 0x060009B3 RID: 2483 RVA: 0x00033BD0 File Offset: 0x00031DD0
		public XmlException()
		{
			this.res = "Xml_DefaultException";
			this.messages = new string[1];
		}

		// Token: 0x060009B4 RID: 2484 RVA: 0x00033BF0 File Offset: 0x00031DF0
		public XmlException(string message, Exception innerException) : base(message, innerException)
		{
			this.res = "Xml_UserException";
			this.messages = new string[]
			{
				message
			};
		}

		// Token: 0x060009B5 RID: 2485 RVA: 0x00033C18 File Offset: 0x00031E18
		protected XmlException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.lineNumber = info.GetInt32("lineNumber");
			this.linePosition = info.GetInt32("linePosition");
			this.res = info.GetString("res");
			this.messages = (string[])info.GetValue("args", typeof(string[]));
			this.sourceUri = info.GetString("sourceUri");
		}

		// Token: 0x060009B6 RID: 2486 RVA: 0x00033C94 File Offset: 0x00031E94
		public XmlException(string message) : base(message)
		{
			this.res = "Xml_UserException";
			this.messages = new string[]
			{
				message
			};
		}

		// Token: 0x060009B7 RID: 2487 RVA: 0x00033CC4 File Offset: 0x00031EC4
		internal XmlException(IXmlLineInfo li, string sourceUri, string message) : this(li, null, sourceUri, message)
		{
		}

		// Token: 0x060009B8 RID: 2488 RVA: 0x00033CD0 File Offset: 0x00031ED0
		internal XmlException(IXmlLineInfo li, Exception innerException, string sourceUri, string message) : this(message, innerException)
		{
			if (li != null)
			{
				this.lineNumber = li.LineNumber;
				this.linePosition = li.LinePosition;
			}
			this.sourceUri = sourceUri;
		}

		// Token: 0x060009B9 RID: 2489 RVA: 0x00033D0C File Offset: 0x00031F0C
		public XmlException(string message, Exception innerException, int lineNumber, int linePosition) : this(message, innerException)
		{
			this.lineNumber = lineNumber;
			this.linePosition = linePosition;
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x060009BA RID: 2490 RVA: 0x00033D28 File Offset: 0x00031F28
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x060009BB RID: 2491 RVA: 0x00033D30 File Offset: 0x00031F30
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x060009BC RID: 2492 RVA: 0x00033D38 File Offset: 0x00031F38
		public string SourceUri
		{
			get
			{
				return this.sourceUri;
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x060009BD RID: 2493 RVA: 0x00033D40 File Offset: 0x00031F40
		public override string Message
		{
			get
			{
				if (this.lineNumber == 0)
				{
					return base.Message;
				}
				return string.Format(CultureInfo.InvariantCulture, "{0} {3} Line {1}, position {2}.", new object[]
				{
					base.Message,
					this.lineNumber,
					this.linePosition,
					this.sourceUri
				});
			}
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x00033DA4 File Offset: 0x00031FA4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("lineNumber", this.lineNumber);
			info.AddValue("linePosition", this.linePosition);
			info.AddValue("res", this.res);
			info.AddValue("args", this.messages);
			info.AddValue("sourceUri", this.sourceUri);
		}

		// Token: 0x040004EA RID: 1258
		private const string Xml_DefaultException = "Xml_DefaultException";

		// Token: 0x040004EB RID: 1259
		private const string Xml_UserException = "Xml_UserException";

		// Token: 0x040004EC RID: 1260
		private int lineNumber;

		// Token: 0x040004ED RID: 1261
		private int linePosition;

		// Token: 0x040004EE RID: 1262
		private string sourceUri;

		// Token: 0x040004EF RID: 1263
		private string res;

		// Token: 0x040004F0 RID: 1264
		private string[] messages;
	}
}
