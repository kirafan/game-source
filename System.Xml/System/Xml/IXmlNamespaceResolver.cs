﻿using System;
using System.Collections.Generic;

namespace System.Xml
{
	// Token: 0x020000DC RID: 220
	public interface IXmlNamespaceResolver
	{
		// Token: 0x060007EA RID: 2026
		IDictionary<string, string> GetNamespacesInScope(XmlNamespaceScope scope);

		// Token: 0x060007EB RID: 2027
		string LookupNamespace(string prefix);

		// Token: 0x060007EC RID: 2028
		string LookupPrefix(string ns);
	}
}
