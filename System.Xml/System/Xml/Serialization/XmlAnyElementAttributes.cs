﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200027F RID: 639
	public class XmlAnyElementAttributes : CollectionBase
	{
		// Token: 0x17000773 RID: 1907
		public XmlAnyElementAttribute this[int index]
		{
			get
			{
				return (XmlAnyElementAttribute)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060019CE RID: 6606 RVA: 0x00088098 File Offset: 0x00086298
		public int Add(XmlAnyElementAttribute attribute)
		{
			return base.List.Add(attribute);
		}

		// Token: 0x060019CF RID: 6607 RVA: 0x000880A8 File Offset: 0x000862A8
		public bool Contains(XmlAnyElementAttribute attribute)
		{
			return base.List.Contains(attribute);
		}

		// Token: 0x060019D0 RID: 6608 RVA: 0x000880B8 File Offset: 0x000862B8
		public int IndexOf(XmlAnyElementAttribute attribute)
		{
			return base.List.IndexOf(attribute);
		}

		// Token: 0x060019D1 RID: 6609 RVA: 0x000880C8 File Offset: 0x000862C8
		public void Insert(int index, XmlAnyElementAttribute attribute)
		{
			base.List.Insert(index, attribute);
		}

		// Token: 0x060019D2 RID: 6610 RVA: 0x000880D8 File Offset: 0x000862D8
		public void Remove(XmlAnyElementAttribute attribute)
		{
			base.List.Remove(attribute);
		}

		// Token: 0x060019D3 RID: 6611 RVA: 0x000880E8 File Offset: 0x000862E8
		public void CopyTo(XmlAnyElementAttribute[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060019D4 RID: 6612 RVA: 0x000880F8 File Offset: 0x000862F8
		internal void AddKeyHash(StringBuilder sb)
		{
			if (this.Count == 0)
			{
				return;
			}
			sb.Append("XAEAS ");
			for (int i = 0; i < this.Count; i++)
			{
				this[i].AddKeyHash(sb);
			}
			sb.Append('|');
		}
	}
}
