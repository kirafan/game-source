﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002D8 RID: 728
	// (Invoke) Token: 0x06001EB5 RID: 7861
	public delegate void XmlSerializationFixupCallback(object fixup);
}
