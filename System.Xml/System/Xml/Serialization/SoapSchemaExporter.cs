﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000275 RID: 629
	public class SoapSchemaExporter
	{
		// Token: 0x0600195D RID: 6493 RVA: 0x000852B0 File Offset: 0x000834B0
		public SoapSchemaExporter(XmlSchemas schemas)
		{
			this._exporter = new XmlSchemaExporter(schemas, true);
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x000852C8 File Offset: 0x000834C8
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping)
		{
			this._exporter.ExportMembersMapping(xmlMembersMapping, false);
		}

		// Token: 0x0600195F RID: 6495 RVA: 0x000852D8 File Offset: 0x000834D8
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping, bool exportEnclosingType)
		{
			this._exporter.ExportMembersMapping(xmlMembersMapping, exportEnclosingType);
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x000852E8 File Offset: 0x000834E8
		public void ExportTypeMapping(XmlTypeMapping xmlTypeMapping)
		{
			this._exporter.ExportTypeMapping(xmlTypeMapping);
		}

		// Token: 0x04000A86 RID: 2694
		private XmlSchemaExporter _exporter;
	}
}
