﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x02000270 RID: 624
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class SoapElementAttribute : Attribute
	{
		// Token: 0x06001941 RID: 6465 RVA: 0x00085028 File Offset: 0x00083228
		public SoapElementAttribute()
		{
		}

		// Token: 0x06001942 RID: 6466 RVA: 0x00085030 File Offset: 0x00083230
		public SoapElementAttribute(string elementName)
		{
			this.elementName = elementName;
		}

		// Token: 0x17000750 RID: 1872
		// (get) Token: 0x06001943 RID: 6467 RVA: 0x00085040 File Offset: 0x00083240
		// (set) Token: 0x06001944 RID: 6468 RVA: 0x0008505C File Offset: 0x0008325C
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		// Token: 0x17000751 RID: 1873
		// (get) Token: 0x06001945 RID: 6469 RVA: 0x00085068 File Offset: 0x00083268
		// (set) Token: 0x06001946 RID: 6470 RVA: 0x00085084 File Offset: 0x00083284
		public string ElementName
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		// Token: 0x17000752 RID: 1874
		// (get) Token: 0x06001947 RID: 6471 RVA: 0x00085090 File Offset: 0x00083290
		// (set) Token: 0x06001948 RID: 6472 RVA: 0x00085098 File Offset: 0x00083298
		public bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullable = value;
			}
		}

		// Token: 0x06001949 RID: 6473 RVA: 0x000850A4 File Offset: 0x000832A4
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SEA ");
			KeyHelper.AddField(sb, 1, this.elementName);
			KeyHelper.AddField(sb, 2, this.dataType);
			KeyHelper.AddField(sb, 3, this.isNullable);
			sb.Append('|');
		}

		// Token: 0x04000A80 RID: 2688
		private string dataType;

		// Token: 0x04000A81 RID: 2689
		private string elementName;

		// Token: 0x04000A82 RID: 2690
		private bool isNullable;
	}
}
