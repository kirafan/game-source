﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002D6 RID: 726
	// (Invoke) Token: 0x06001EAD RID: 7853
	public delegate void XmlAttributeEventHandler(object sender, XmlAttributeEventArgs e);
}
