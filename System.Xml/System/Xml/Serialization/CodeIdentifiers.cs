﻿using System;
using System.Collections;
using System.Globalization;

namespace System.Xml.Serialization
{
	// Token: 0x02000256 RID: 598
	public class CodeIdentifiers
	{
		// Token: 0x0600182E RID: 6190 RVA: 0x0007A150 File Offset: 0x00078350
		public CodeIdentifiers() : this(true)
		{
		}

		// Token: 0x0600182F RID: 6191 RVA: 0x0007A15C File Offset: 0x0007835C
		public CodeIdentifiers(bool caseSensitive)
		{
			StringComparer equalityComparer = (!caseSensitive) ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal;
			this.table = new Hashtable(equalityComparer);
			this.reserved = new Hashtable(equalityComparer);
		}

		// Token: 0x1700073B RID: 1851
		// (get) Token: 0x06001830 RID: 6192 RVA: 0x0007A1A0 File Offset: 0x000783A0
		// (set) Token: 0x06001831 RID: 6193 RVA: 0x0007A1A8 File Offset: 0x000783A8
		public bool UseCamelCasing
		{
			get
			{
				return this.useCamelCasing;
			}
			set
			{
				this.useCamelCasing = value;
			}
		}

		// Token: 0x06001832 RID: 6194 RVA: 0x0007A1B4 File Offset: 0x000783B4
		public void Add(string identifier, object value)
		{
			this.table.Add(identifier, value);
		}

		// Token: 0x06001833 RID: 6195 RVA: 0x0007A1C4 File Offset: 0x000783C4
		public void AddReserved(string identifier)
		{
			this.reserved.Add(identifier, identifier);
		}

		// Token: 0x06001834 RID: 6196 RVA: 0x0007A1D4 File Offset: 0x000783D4
		public string AddUnique(string identifier, object value)
		{
			string text = this.MakeUnique(identifier);
			this.Add(text, value);
			return text;
		}

		// Token: 0x06001835 RID: 6197 RVA: 0x0007A1F4 File Offset: 0x000783F4
		public void Clear()
		{
			this.table.Clear();
		}

		// Token: 0x06001836 RID: 6198 RVA: 0x0007A204 File Offset: 0x00078404
		public bool IsInUse(string identifier)
		{
			return this.table.ContainsKey(identifier) || this.reserved.ContainsKey(identifier);
		}

		// Token: 0x06001837 RID: 6199 RVA: 0x0007A234 File Offset: 0x00078434
		public string MakeRightCase(string identifier)
		{
			if (this.UseCamelCasing)
			{
				return CodeIdentifier.MakeCamel(identifier);
			}
			return CodeIdentifier.MakePascal(identifier);
		}

		// Token: 0x06001838 RID: 6200 RVA: 0x0007A250 File Offset: 0x00078450
		public string MakeUnique(string identifier)
		{
			string text = identifier;
			int num = 1;
			while (this.IsInUse(text))
			{
				text = string.Format(CultureInfo.InvariantCulture, "{0}{1}", new object[]
				{
					identifier,
					num
				});
				num++;
			}
			return text;
		}

		// Token: 0x06001839 RID: 6201 RVA: 0x0007A29C File Offset: 0x0007849C
		public void Remove(string identifier)
		{
			this.table.Remove(identifier);
		}

		// Token: 0x0600183A RID: 6202 RVA: 0x0007A2AC File Offset: 0x000784AC
		public void RemoveReserved(string identifier)
		{
			this.reserved.Remove(identifier);
		}

		// Token: 0x0600183B RID: 6203 RVA: 0x0007A2BC File Offset: 0x000784BC
		public object ToArray(Type type)
		{
			Array array = Array.CreateInstance(type, this.table.Count);
			this.table.CopyTo(array, 0);
			return array;
		}

		// Token: 0x04000A06 RID: 2566
		private bool useCamelCasing;

		// Token: 0x04000A07 RID: 2567
		private Hashtable table;

		// Token: 0x04000A08 RID: 2568
		private Hashtable reserved;
	}
}
