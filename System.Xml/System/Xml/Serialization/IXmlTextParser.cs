﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000252 RID: 594
	public interface IXmlTextParser
	{
		// Token: 0x17000738 RID: 1848
		// (get) Token: 0x06001824 RID: 6180
		// (set) Token: 0x06001825 RID: 6181
		bool Normalized { get; set; }

		// Token: 0x17000739 RID: 1849
		// (get) Token: 0x06001826 RID: 6182
		// (set) Token: 0x06001827 RID: 6183
		WhitespaceHandling WhitespaceHandling { get; set; }
	}
}
