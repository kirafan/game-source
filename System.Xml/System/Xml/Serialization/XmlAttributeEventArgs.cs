﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000284 RID: 644
	public class XmlAttributeEventArgs : EventArgs
	{
		// Token: 0x06001A0F RID: 6671 RVA: 0x00088610 File Offset: 0x00086810
		internal XmlAttributeEventArgs(XmlAttribute attr, int lineNum, int linePos, object source)
		{
			this.attr = attr;
			this.lineNumber = lineNum;
			this.linePosition = linePos;
			this.obj = source;
		}

		// Token: 0x17000787 RID: 1927
		// (get) Token: 0x06001A10 RID: 6672 RVA: 0x00088638 File Offset: 0x00086838
		public XmlAttribute Attr
		{
			get
			{
				return this.attr;
			}
		}

		// Token: 0x17000788 RID: 1928
		// (get) Token: 0x06001A11 RID: 6673 RVA: 0x00088640 File Offset: 0x00086840
		public int LineNumber
		{
			get
			{
				return this.lineNumber;
			}
		}

		// Token: 0x17000789 RID: 1929
		// (get) Token: 0x06001A12 RID: 6674 RVA: 0x00088648 File Offset: 0x00086848
		public int LinePosition
		{
			get
			{
				return this.linePosition;
			}
		}

		// Token: 0x1700078A RID: 1930
		// (get) Token: 0x06001A13 RID: 6675 RVA: 0x00088650 File Offset: 0x00086850
		public object ObjectBeingDeserialized
		{
			get
			{
				return this.obj;
			}
		}

		// Token: 0x1700078B RID: 1931
		// (get) Token: 0x06001A14 RID: 6676 RVA: 0x00088658 File Offset: 0x00086858
		// (set) Token: 0x06001A15 RID: 6677 RVA: 0x00088660 File Offset: 0x00086860
		public string ExpectedAttributes
		{
			get
			{
				return this.expectedAttributes;
			}
			internal set
			{
				this.expectedAttributes = value;
			}
		}

		// Token: 0x04000ABF RID: 2751
		private XmlAttribute attr;

		// Token: 0x04000AC0 RID: 2752
		private int lineNumber;

		// Token: 0x04000AC1 RID: 2753
		private int linePosition;

		// Token: 0x04000AC2 RID: 2754
		private object obj;

		// Token: 0x04000AC3 RID: 2755
		private string expectedAttributes;
	}
}
