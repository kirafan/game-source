﻿using System;
using System.Collections;
using System.Security.Policy;

namespace System.Xml.Serialization
{
	// Token: 0x020002B5 RID: 693
	public class XmlSerializerFactory
	{
		// Token: 0x06001D2F RID: 7471 RVA: 0x0009B1D8 File Offset: 0x000993D8
		public XmlSerializer CreateSerializer(Type type)
		{
			return this.CreateSerializer(type, null, null, null, null);
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x0009B1E8 File Offset: 0x000993E8
		public XmlSerializer CreateSerializer(XmlTypeMapping xmlTypeMapping)
		{
			Hashtable obj = XmlSerializerFactory.serializersBySource;
			XmlSerializer result;
			lock (obj)
			{
				XmlSerializer xmlSerializer = (XmlSerializer)XmlSerializerFactory.serializersBySource[xmlTypeMapping.Source];
				if (xmlSerializer == null)
				{
					xmlSerializer = new XmlSerializer(xmlTypeMapping);
					XmlSerializerFactory.serializersBySource[xmlTypeMapping.Source] = xmlSerializer;
				}
				result = xmlSerializer;
			}
			return result;
		}

		// Token: 0x06001D31 RID: 7473 RVA: 0x0009B268 File Offset: 0x00099468
		public XmlSerializer CreateSerializer(Type type, string defaultNamespace)
		{
			return this.CreateSerializer(type, null, null, null, defaultNamespace);
		}

		// Token: 0x06001D32 RID: 7474 RVA: 0x0009B278 File Offset: 0x00099478
		public XmlSerializer CreateSerializer(Type type, Type[] extraTypes)
		{
			return this.CreateSerializer(type, null, extraTypes, null, null);
		}

		// Token: 0x06001D33 RID: 7475 RVA: 0x0009B288 File Offset: 0x00099488
		public XmlSerializer CreateSerializer(Type type, XmlAttributeOverrides overrides)
		{
			return this.CreateSerializer(type, overrides, null, null, null);
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x0009B298 File Offset: 0x00099498
		public XmlSerializer CreateSerializer(Type type, XmlRootAttribute root)
		{
			return this.CreateSerializer(type, null, null, root, null);
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x0009B2A8 File Offset: 0x000994A8
		public XmlSerializer CreateSerializer(Type type, XmlAttributeOverrides overrides, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace)
		{
			XmlTypeSerializationSource key = new XmlTypeSerializationSource(type, root, overrides, defaultNamespace, extraTypes);
			Hashtable obj = XmlSerializerFactory.serializersBySource;
			XmlSerializer result;
			lock (obj)
			{
				XmlSerializer xmlSerializer = (XmlSerializer)XmlSerializerFactory.serializersBySource[key];
				if (xmlSerializer == null)
				{
					xmlSerializer = new XmlSerializer(type, overrides, extraTypes, root, defaultNamespace);
					XmlSerializerFactory.serializersBySource[xmlSerializer.Mapping.Source] = xmlSerializer;
				}
				result = xmlSerializer;
			}
			return result;
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x0009B338 File Offset: 0x00099538
		[MonoTODO]
		public XmlSerializer CreateSerializer(Type type, XmlAttributeOverrides overrides, Type[] extraTypes, XmlRootAttribute root, string defaultNamespace, string location, Evidence evidence)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000B9C RID: 2972
		private static Hashtable serializersBySource = new Hashtable();
	}
}
