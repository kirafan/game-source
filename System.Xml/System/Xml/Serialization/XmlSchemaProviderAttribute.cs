﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002A2 RID: 674
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface)]
	public sealed class XmlSchemaProviderAttribute : Attribute
	{
		// Token: 0x06001BAB RID: 7083 RVA: 0x00093594 File Offset: 0x00091794
		public XmlSchemaProviderAttribute(string methodName)
		{
			this._methodName = methodName;
		}

		// Token: 0x170007E3 RID: 2019
		// (get) Token: 0x06001BAC RID: 7084 RVA: 0x000935A4 File Offset: 0x000917A4
		public string MethodName
		{
			get
			{
				return this._methodName;
			}
		}

		// Token: 0x170007E4 RID: 2020
		// (get) Token: 0x06001BAD RID: 7085 RVA: 0x000935AC File Offset: 0x000917AC
		// (set) Token: 0x06001BAE RID: 7086 RVA: 0x000935B4 File Offset: 0x000917B4
		public bool IsAny
		{
			get
			{
				return this._isAny;
			}
			set
			{
				this._isAny = value;
			}
		}

		// Token: 0x04000B3A RID: 2874
		private string _methodName;

		// Token: 0x04000B3B RID: 2875
		private bool _isAny;
	}
}
