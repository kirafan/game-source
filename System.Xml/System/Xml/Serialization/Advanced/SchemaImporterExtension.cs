﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Xml.Schema;

namespace System.Xml.Serialization.Advanced
{
	// Token: 0x020002CC RID: 716
	public abstract class SchemaImporterExtension
	{
		// Token: 0x06001E1D RID: 7709 RVA: 0x0009D8D0 File Offset: 0x0009BAD0
		public virtual string ImportAnyElement(XmlSchemaAny any, bool mixed, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit codeCompileUnit, CodeNamespace codeNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			return null;
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x0009D8D4 File Offset: 0x0009BAD4
		public virtual CodeExpression ImportDefaultValue(string value, string type)
		{
			return null;
		}

		// Token: 0x06001E1F RID: 7711 RVA: 0x0009D8D8 File Offset: 0x0009BAD8
		public virtual string ImportSchemaType(XmlSchemaType type, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit codeCompileUnit, CodeNamespace codeNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			return null;
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x0009D8DC File Offset: 0x0009BADC
		public virtual string ImportSchemaType(string name, string ns, XmlSchemaObject context, XmlSchemas schemas, XmlSchemaImporter importer, CodeCompileUnit codeCompileUnit, CodeNamespace codeNamespace, CodeGenerationOptions options, CodeDomProvider codeProvider)
		{
			return null;
		}
	}
}
