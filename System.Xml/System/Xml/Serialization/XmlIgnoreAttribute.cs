﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000290 RID: 656
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlIgnoreAttribute : Attribute
	{
	}
}
