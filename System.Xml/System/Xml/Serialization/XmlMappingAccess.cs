﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000297 RID: 663
	[Flags]
	public enum XmlMappingAccess
	{
		// Token: 0x04000AFE RID: 2814
		None = 0,
		// Token: 0x04000AFF RID: 2815
		Read = 1,
		// Token: 0x04000B00 RID: 2816
		Write = 2
	}
}
