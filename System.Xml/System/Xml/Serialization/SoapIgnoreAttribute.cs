﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000272 RID: 626
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class SoapIgnoreAttribute : Attribute
	{
	}
}
