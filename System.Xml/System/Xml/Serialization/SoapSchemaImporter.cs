﻿using System;
using System.CodeDom.Compiler;

namespace System.Xml.Serialization
{
	// Token: 0x02000274 RID: 628
	public class SoapSchemaImporter : SchemaImporter
	{
		// Token: 0x06001953 RID: 6483 RVA: 0x0008518C File Offset: 0x0008338C
		public SoapSchemaImporter(XmlSchemas schemas)
		{
			this._importer = new XmlSchemaImporter(schemas);
			this._importer.UseEncodedFormat = true;
		}

		// Token: 0x06001954 RID: 6484 RVA: 0x000851AC File Offset: 0x000833AC
		public SoapSchemaImporter(XmlSchemas schemas, CodeIdentifiers typeIdentifiers)
		{
			this._importer = new XmlSchemaImporter(schemas, typeIdentifiers);
			this._importer.UseEncodedFormat = true;
		}

		// Token: 0x06001955 RID: 6485 RVA: 0x000851D0 File Offset: 0x000833D0
		public SoapSchemaImporter(XmlSchemas schemas, CodeGenerationOptions options, ImportContext context)
		{
			this._importer = new XmlSchemaImporter(schemas, options, context);
			this._importer.UseEncodedFormat = true;
		}

		// Token: 0x06001956 RID: 6486 RVA: 0x00085200 File Offset: 0x00083400
		public SoapSchemaImporter(XmlSchemas schemas, CodeIdentifiers typeIdentifiers, CodeGenerationOptions options)
		{
			this._importer = new XmlSchemaImporter(schemas, typeIdentifiers, options);
			this._importer.UseEncodedFormat = true;
		}

		// Token: 0x06001957 RID: 6487 RVA: 0x00085230 File Offset: 0x00083430
		public SoapSchemaImporter(XmlSchemas schemas, CodeGenerationOptions options, CodeDomProvider codeProvider, ImportContext context)
		{
			this._importer = new XmlSchemaImporter(schemas, options, codeProvider, context);
			this._importer.UseEncodedFormat = true;
		}

		// Token: 0x06001958 RID: 6488 RVA: 0x00085260 File Offset: 0x00083460
		public XmlTypeMapping ImportDerivedTypeMapping(XmlQualifiedName name, Type baseType, bool baseTypeCanBeIndirect)
		{
			return this._importer.ImportDerivedTypeMapping(name, baseType, baseTypeCanBeIndirect);
		}

		// Token: 0x06001959 RID: 6489 RVA: 0x00085270 File Offset: 0x00083470
		public XmlMembersMapping ImportMembersMapping(string name, string ns, SoapSchemaMember member)
		{
			return this._importer.ImportEncodedMembersMapping(name, ns, member);
		}

		// Token: 0x0600195A RID: 6490 RVA: 0x00085280 File Offset: 0x00083480
		public XmlMembersMapping ImportMembersMapping(string name, string ns, SoapSchemaMember[] members)
		{
			return this._importer.ImportEncodedMembersMapping(name, ns, members, false);
		}

		// Token: 0x0600195B RID: 6491 RVA: 0x00085294 File Offset: 0x00083494
		public XmlMembersMapping ImportMembersMapping(string name, string ns, SoapSchemaMember[] members, bool hasWrapperElement)
		{
			return this._importer.ImportEncodedMembersMapping(name, ns, members, hasWrapperElement);
		}

		// Token: 0x0600195C RID: 6492 RVA: 0x000852A8 File Offset: 0x000834A8
		[MonoTODO]
		public XmlMembersMapping ImportMembersMapping(string name, string ns, SoapSchemaMember[] members, bool hasWrapperElement, Type baseType, bool baseTypeCanBeIndirect)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000A85 RID: 2693
		private XmlSchemaImporter _importer;
	}
}
