﻿using System;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x02000257 RID: 599
	public interface IXmlSerializable
	{
		// Token: 0x0600183C RID: 6204
		XmlSchema GetSchema();

		// Token: 0x0600183D RID: 6205
		void ReadXml(XmlReader reader);

		// Token: 0x0600183E RID: 6206
		void WriteXml(XmlWriter writer);
	}
}
