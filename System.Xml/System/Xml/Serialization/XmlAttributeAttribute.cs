﻿using System;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x02000283 RID: 643
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlAttributeAttribute : Attribute
	{
		// Token: 0x06001A00 RID: 6656 RVA: 0x000884CC File Offset: 0x000866CC
		public XmlAttributeAttribute()
		{
		}

		// Token: 0x06001A01 RID: 6657 RVA: 0x000884D4 File Offset: 0x000866D4
		public XmlAttributeAttribute(string attributeName)
		{
			this.attributeName = attributeName;
		}

		// Token: 0x06001A02 RID: 6658 RVA: 0x000884E4 File Offset: 0x000866E4
		public XmlAttributeAttribute(Type type)
		{
			this.type = type;
		}

		// Token: 0x06001A03 RID: 6659 RVA: 0x000884F4 File Offset: 0x000866F4
		public XmlAttributeAttribute(string attributeName, Type type)
		{
			this.attributeName = attributeName;
			this.type = type;
		}

		// Token: 0x17000782 RID: 1922
		// (get) Token: 0x06001A04 RID: 6660 RVA: 0x0008850C File Offset: 0x0008670C
		// (set) Token: 0x06001A05 RID: 6661 RVA: 0x00088528 File Offset: 0x00086728
		public string AttributeName
		{
			get
			{
				if (this.attributeName == null)
				{
					return string.Empty;
				}
				return this.attributeName;
			}
			set
			{
				this.attributeName = value;
			}
		}

		// Token: 0x17000783 RID: 1923
		// (get) Token: 0x06001A06 RID: 6662 RVA: 0x00088534 File Offset: 0x00086734
		// (set) Token: 0x06001A07 RID: 6663 RVA: 0x00088550 File Offset: 0x00086750
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		// Token: 0x17000784 RID: 1924
		// (get) Token: 0x06001A08 RID: 6664 RVA: 0x0008855C File Offset: 0x0008675C
		// (set) Token: 0x06001A09 RID: 6665 RVA: 0x00088564 File Offset: 0x00086764
		public XmlSchemaForm Form
		{
			get
			{
				return this.form;
			}
			set
			{
				this.form = value;
			}
		}

		// Token: 0x17000785 RID: 1925
		// (get) Token: 0x06001A0A RID: 6666 RVA: 0x00088570 File Offset: 0x00086770
		// (set) Token: 0x06001A0B RID: 6667 RVA: 0x00088578 File Offset: 0x00086778
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x17000786 RID: 1926
		// (get) Token: 0x06001A0C RID: 6668 RVA: 0x00088584 File Offset: 0x00086784
		// (set) Token: 0x06001A0D RID: 6669 RVA: 0x0008858C File Offset: 0x0008678C
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x06001A0E RID: 6670 RVA: 0x00088598 File Offset: 0x00086798
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XAA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.attributeName);
			KeyHelper.AddField(sb, 3, this.form.ToString(), XmlSchemaForm.None.ToString());
			KeyHelper.AddField(sb, 4, this.dataType);
			KeyHelper.AddField(sb, 5, this.type);
			sb.Append('|');
		}

		// Token: 0x04000ABA RID: 2746
		private string attributeName;

		// Token: 0x04000ABB RID: 2747
		private string dataType;

		// Token: 0x04000ABC RID: 2748
		private Type type;

		// Token: 0x04000ABD RID: 2749
		private XmlSchemaForm form;

		// Token: 0x04000ABE RID: 2750
		private string ns;
	}
}
