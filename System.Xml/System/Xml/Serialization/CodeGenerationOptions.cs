﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000254 RID: 596
	[Flags]
	public enum CodeGenerationOptions
	{
		// Token: 0x04000A00 RID: 2560
		[XmlIgnore]
		None = 0,
		// Token: 0x04000A01 RID: 2561
		[XmlEnum("properties")]
		GenerateProperties = 1,
		// Token: 0x04000A02 RID: 2562
		[XmlEnum("newAsync")]
		GenerateNewAsync = 2,
		// Token: 0x04000A03 RID: 2563
		[XmlEnum("oldAsync")]
		GenerateOldAsync = 4,
		// Token: 0x04000A04 RID: 2564
		[XmlEnum("order")]
		GenerateOrder = 8,
		// Token: 0x04000A05 RID: 2565
		[XmlEnum("enableDataBinding")]
		EnableDataBinding = 16
	}
}
