﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002D7 RID: 727
	// (Invoke) Token: 0x06001EB1 RID: 7857
	public delegate void XmlSerializationCollectionFixupCallback(object collection, object collectionItems);
}
