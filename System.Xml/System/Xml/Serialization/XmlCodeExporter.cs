﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x02000287 RID: 647
	public class XmlCodeExporter : CodeExporter
	{
		// Token: 0x06001A38 RID: 6712 RVA: 0x00088C74 File Offset: 0x00086E74
		public XmlCodeExporter(CodeNamespace codeNamespace) : this(codeNamespace, null)
		{
		}

		// Token: 0x06001A39 RID: 6713 RVA: 0x00088C80 File Offset: 0x00086E80
		public XmlCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit)
		{
			this.codeGenerator = new XmlMapCodeGenerator(codeNamespace, codeCompileUnit, CodeGenerationOptions.GenerateProperties);
		}

		// Token: 0x06001A3A RID: 6714 RVA: 0x00088C98 File Offset: 0x00086E98
		public XmlCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeGenerationOptions options) : this(codeNamespace, codeCompileUnit, null, options, null)
		{
		}

		// Token: 0x06001A3B RID: 6715 RVA: 0x00088CA8 File Offset: 0x00086EA8
		public XmlCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeGenerationOptions options, Hashtable mappings) : this(codeNamespace, codeCompileUnit, null, options, mappings)
		{
		}

		// Token: 0x06001A3C RID: 6716 RVA: 0x00088CB8 File Offset: 0x00086EB8
		[MonoTODO]
		public XmlCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeDomProvider codeProvider, CodeGenerationOptions options, Hashtable mappings)
		{
			this.codeGenerator = new XmlMapCodeGenerator(codeNamespace, codeCompileUnit, codeProvider, options, mappings);
		}

		// Token: 0x06001A3D RID: 6717 RVA: 0x00088CE0 File Offset: 0x00086EE0
		public void AddMappingMetadata(CodeAttributeDeclarationCollection metadata, XmlMemberMapping member, string ns)
		{
			this.AddMappingMetadata(metadata, member, ns, false);
		}

		// Token: 0x06001A3E RID: 6718 RVA: 0x00088CEC File Offset: 0x00086EEC
		public void AddMappingMetadata(CodeAttributeDeclarationCollection metadata, XmlTypeMapping member, string ns)
		{
			if ((member.TypeData.SchemaType == SchemaTypes.Primitive || member.TypeData.SchemaType == SchemaTypes.Array) && member.Namespace != "http://www.w3.org/2001/XMLSchema")
			{
				metadata.Add(new CodeAttributeDeclaration("System.Xml.Serialization.XmlRoot")
				{
					Arguments = 
					{
						MapCodeGenerator.GetArg(member.ElementName),
						MapCodeGenerator.GetArg("Namespace", member.Namespace),
						MapCodeGenerator.GetArg("IsNullable", member.IsNullable)
					}
				});
			}
		}

		// Token: 0x06001A3F RID: 6719 RVA: 0x00088D98 File Offset: 0x00086F98
		public void AddMappingMetadata(CodeAttributeDeclarationCollection metadata, XmlMemberMapping member, string ns, bool forceUseMemberName)
		{
			TypeData typeData = member.TypeMapMember.TypeData;
			if (member.Any)
			{
				XmlTypeMapElementInfoList elementInfo = ((XmlTypeMapMemberElement)member.TypeMapMember).ElementInfo;
				foreach (object obj in elementInfo)
				{
					XmlTypeMapElementInfo xmlTypeMapElementInfo = (XmlTypeMapElementInfo)obj;
					if (xmlTypeMapElementInfo.IsTextElement)
					{
						metadata.Add(new CodeAttributeDeclaration("System.Xml.Serialization.XmlText"));
					}
					else
					{
						CodeAttributeDeclaration codeAttributeDeclaration = new CodeAttributeDeclaration("System.Xml.Serialization.XmlAnyElement");
						if (!xmlTypeMapElementInfo.IsUnnamedAnyElement)
						{
							codeAttributeDeclaration.Arguments.Add(MapCodeGenerator.GetArg("Name", xmlTypeMapElementInfo.ElementName));
							if (xmlTypeMapElementInfo.Namespace != ns)
							{
								codeAttributeDeclaration.Arguments.Add(MapCodeGenerator.GetArg("Namespace", member.Namespace));
							}
						}
						metadata.Add(codeAttributeDeclaration);
					}
				}
			}
			else if (member.TypeMapMember is XmlTypeMapMemberList)
			{
				XmlTypeMapMemberList xmlTypeMapMemberList = member.TypeMapMember as XmlTypeMapMemberList;
				ListMap listMap = (ListMap)xmlTypeMapMemberList.ListTypeMapping.ObjectMap;
				this.codeGenerator.AddArrayAttributes(metadata, xmlTypeMapMemberList, ns, forceUseMemberName);
				this.codeGenerator.AddArrayItemAttributes(metadata, listMap, typeData.ListItemTypeData, xmlTypeMapMemberList.Namespace, 0);
			}
			else if (member.TypeMapMember is XmlTypeMapMemberElement)
			{
				this.codeGenerator.AddElementMemberAttributes((XmlTypeMapMemberElement)member.TypeMapMember, ns, metadata, forceUseMemberName);
			}
			else
			{
				if (!(member.TypeMapMember is XmlTypeMapMemberAttribute))
				{
					throw new NotSupportedException("Schema type not supported");
				}
				this.codeGenerator.AddAttributeMemberAttributes((XmlTypeMapMemberAttribute)member.TypeMapMember, ns, metadata, forceUseMemberName);
			}
		}

		// Token: 0x06001A40 RID: 6720 RVA: 0x00088F84 File Offset: 0x00087184
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping)
		{
			this.codeGenerator.ExportMembersMapping(xmlMembersMapping);
		}

		// Token: 0x06001A41 RID: 6721 RVA: 0x00088F94 File Offset: 0x00087194
		public void ExportTypeMapping(XmlTypeMapping xmlTypeMapping)
		{
			this.codeGenerator.ExportTypeMapping(xmlTypeMapping, true);
		}
	}
}
