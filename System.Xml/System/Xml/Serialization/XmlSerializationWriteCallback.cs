﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002DA RID: 730
	// (Invoke) Token: 0x06001EBD RID: 7869
	public delegate void XmlSerializationWriteCallback(object o);
}
