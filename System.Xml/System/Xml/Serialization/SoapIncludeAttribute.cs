﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x02000273 RID: 627
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Interface, AllowMultiple = true)]
	public class SoapIncludeAttribute : Attribute
	{
		// Token: 0x06001950 RID: 6480 RVA: 0x00085168 File Offset: 0x00083368
		public SoapIncludeAttribute(Type type)
		{
			this.type = type;
		}

		// Token: 0x17000754 RID: 1876
		// (get) Token: 0x06001951 RID: 6481 RVA: 0x00085178 File Offset: 0x00083378
		// (set) Token: 0x06001952 RID: 6482 RVA: 0x00085180 File Offset: 0x00083380
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x04000A84 RID: 2692
		private Type type;
	}
}
