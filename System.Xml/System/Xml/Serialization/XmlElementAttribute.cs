﻿using System;
using System.Text;
using System.Xml.Schema;

namespace System.Xml.Serialization
{
	// Token: 0x0200028C RID: 652
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, AllowMultiple = true)]
	public class XmlElementAttribute : Attribute
	{
		// Token: 0x06001A7B RID: 6779 RVA: 0x0008A8E4 File Offset: 0x00088AE4
		public XmlElementAttribute()
		{
		}

		// Token: 0x06001A7C RID: 6780 RVA: 0x0008A8F4 File Offset: 0x00088AF4
		public XmlElementAttribute(string elementName)
		{
			this.elementName = elementName;
		}

		// Token: 0x06001A7D RID: 6781 RVA: 0x0008A90C File Offset: 0x00088B0C
		public XmlElementAttribute(Type type)
		{
			this.type = type;
		}

		// Token: 0x06001A7E RID: 6782 RVA: 0x0008A924 File Offset: 0x00088B24
		public XmlElementAttribute(string elementName, Type type)
		{
			this.elementName = elementName;
			this.type = type;
		}

		// Token: 0x170007A1 RID: 1953
		// (get) Token: 0x06001A7F RID: 6783 RVA: 0x0008A944 File Offset: 0x00088B44
		// (set) Token: 0x06001A80 RID: 6784 RVA: 0x0008A960 File Offset: 0x00088B60
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		// Token: 0x170007A2 RID: 1954
		// (get) Token: 0x06001A81 RID: 6785 RVA: 0x0008A96C File Offset: 0x00088B6C
		// (set) Token: 0x06001A82 RID: 6786 RVA: 0x0008A988 File Offset: 0x00088B88
		public string ElementName
		{
			get
			{
				if (this.elementName == null)
				{
					return string.Empty;
				}
				return this.elementName;
			}
			set
			{
				this.elementName = value;
			}
		}

		// Token: 0x170007A3 RID: 1955
		// (get) Token: 0x06001A83 RID: 6787 RVA: 0x0008A994 File Offset: 0x00088B94
		// (set) Token: 0x06001A84 RID: 6788 RVA: 0x0008A99C File Offset: 0x00088B9C
		public XmlSchemaForm Form
		{
			get
			{
				return this.form;
			}
			set
			{
				this.form = value;
			}
		}

		// Token: 0x170007A4 RID: 1956
		// (get) Token: 0x06001A85 RID: 6789 RVA: 0x0008A9A8 File Offset: 0x00088BA8
		// (set) Token: 0x06001A86 RID: 6790 RVA: 0x0008A9B0 File Offset: 0x00088BB0
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x170007A5 RID: 1957
		// (get) Token: 0x06001A87 RID: 6791 RVA: 0x0008A9BC File Offset: 0x00088BBC
		// (set) Token: 0x06001A88 RID: 6792 RVA: 0x0008A9C4 File Offset: 0x00088BC4
		public bool IsNullable
		{
			get
			{
				return this.isNullable;
			}
			set
			{
				this.isNullableSpecified = true;
				this.isNullable = value;
			}
		}

		// Token: 0x170007A6 RID: 1958
		// (get) Token: 0x06001A89 RID: 6793 RVA: 0x0008A9D4 File Offset: 0x00088BD4
		internal bool IsNullableSpecified
		{
			get
			{
				return this.isNullableSpecified;
			}
		}

		// Token: 0x170007A7 RID: 1959
		// (get) Token: 0x06001A8A RID: 6794 RVA: 0x0008A9DC File Offset: 0x00088BDC
		// (set) Token: 0x06001A8B RID: 6795 RVA: 0x0008A9E4 File Offset: 0x00088BE4
		[MonoTODO]
		public int Order
		{
			get
			{
				return this.order;
			}
			set
			{
				this.order = value;
			}
		}

		// Token: 0x170007A8 RID: 1960
		// (get) Token: 0x06001A8C RID: 6796 RVA: 0x0008A9F0 File Offset: 0x00088BF0
		// (set) Token: 0x06001A8D RID: 6797 RVA: 0x0008A9F8 File Offset: 0x00088BF8
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x06001A8E RID: 6798 RVA: 0x0008AA04 File Offset: 0x00088C04
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XEA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.elementName);
			KeyHelper.AddField(sb, 3, this.form.ToString(), XmlSchemaForm.None.ToString());
			KeyHelper.AddField(sb, 4, this.dataType);
			KeyHelper.AddField(sb, 5, this.type);
			KeyHelper.AddField(sb, 6, this.isNullable);
			sb.Append('|');
		}

		// Token: 0x04000ADC RID: 2780
		private string dataType;

		// Token: 0x04000ADD RID: 2781
		private string elementName;

		// Token: 0x04000ADE RID: 2782
		private XmlSchemaForm form;

		// Token: 0x04000ADF RID: 2783
		private string ns;

		// Token: 0x04000AE0 RID: 2784
		private bool isNullable;

		// Token: 0x04000AE1 RID: 2785
		private bool isNullableSpecified;

		// Token: 0x04000AE2 RID: 2786
		private Type type;

		// Token: 0x04000AE3 RID: 2787
		private int order = -1;
	}
}
