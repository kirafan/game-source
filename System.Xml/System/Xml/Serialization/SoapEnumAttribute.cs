﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x02000271 RID: 625
	[AttributeUsage(AttributeTargets.Field)]
	public class SoapEnumAttribute : Attribute
	{
		// Token: 0x0600194A RID: 6474 RVA: 0x000850F0 File Offset: 0x000832F0
		public SoapEnumAttribute()
		{
		}

		// Token: 0x0600194B RID: 6475 RVA: 0x000850F8 File Offset: 0x000832F8
		public SoapEnumAttribute(string name)
		{
			this.name = name;
		}

		// Token: 0x17000753 RID: 1875
		// (get) Token: 0x0600194C RID: 6476 RVA: 0x00085108 File Offset: 0x00083308
		// (set) Token: 0x0600194D RID: 6477 RVA: 0x00085124 File Offset: 0x00083324
		public string Name
		{
			get
			{
				if (this.name == null)
				{
					return string.Empty;
				}
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x00085130 File Offset: 0x00083330
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SENA ");
			KeyHelper.AddField(sb, 1, this.name);
			sb.Append('|');
		}

		// Token: 0x04000A83 RID: 2691
		private string name;
	}
}
