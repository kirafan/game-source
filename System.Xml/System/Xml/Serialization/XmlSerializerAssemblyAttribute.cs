﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002B4 RID: 692
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	public sealed class XmlSerializerAssemblyAttribute : Attribute
	{
		// Token: 0x06001D26 RID: 7462 RVA: 0x0009B174 File Offset: 0x00099374
		public XmlSerializerAssemblyAttribute()
		{
		}

		// Token: 0x06001D27 RID: 7463 RVA: 0x0009B17C File Offset: 0x0009937C
		public XmlSerializerAssemblyAttribute(string assemblyName)
		{
			this._assemblyName = assemblyName;
		}

		// Token: 0x06001D28 RID: 7464 RVA: 0x0009B18C File Offset: 0x0009938C
		public XmlSerializerAssemblyAttribute(string assemblyName, string codeBase) : this(assemblyName)
		{
			this._codeBase = codeBase;
		}

		// Token: 0x170007FB RID: 2043
		// (get) Token: 0x06001D29 RID: 7465 RVA: 0x0009B19C File Offset: 0x0009939C
		// (set) Token: 0x06001D2A RID: 7466 RVA: 0x0009B1A4 File Offset: 0x000993A4
		public string AssemblyName
		{
			get
			{
				return this._assemblyName;
			}
			set
			{
				this._assemblyName = value;
			}
		}

		// Token: 0x170007FC RID: 2044
		// (get) Token: 0x06001D2B RID: 7467 RVA: 0x0009B1B0 File Offset: 0x000993B0
		// (set) Token: 0x06001D2C RID: 7468 RVA: 0x0009B1B8 File Offset: 0x000993B8
		public string CodeBase
		{
			get
			{
				return this._codeBase;
			}
			set
			{
				this._codeBase = value;
			}
		}

		// Token: 0x04000B9A RID: 2970
		private string _assemblyName;

		// Token: 0x04000B9B RID: 2971
		private string _codeBase;
	}
}
