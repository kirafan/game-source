﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002D5 RID: 725
	// (Invoke) Token: 0x06001EA9 RID: 7849
	public delegate void UnreferencedObjectEventHandler(object sender, UnreferencedObjectEventArgs e);
}
