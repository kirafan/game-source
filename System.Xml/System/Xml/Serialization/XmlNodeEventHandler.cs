﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002DC RID: 732
	// (Invoke) Token: 0x06001EC5 RID: 7877
	public delegate void XmlNodeEventHandler(object sender, XmlNodeEventArgs e);
}
