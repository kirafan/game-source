﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002DB RID: 731
	// (Invoke) Token: 0x06001EC1 RID: 7873
	public delegate void XmlElementEventHandler(object sender, XmlElementEventArgs e);
}
