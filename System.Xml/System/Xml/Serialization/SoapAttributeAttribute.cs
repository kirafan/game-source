﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200026B RID: 619
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class SoapAttributeAttribute : Attribute
	{
		// Token: 0x06001910 RID: 6416 RVA: 0x00084744 File Offset: 0x00082944
		public SoapAttributeAttribute()
		{
		}

		// Token: 0x06001911 RID: 6417 RVA: 0x0008474C File Offset: 0x0008294C
		public SoapAttributeAttribute(string attrName)
		{
			this.attrName = attrName;
		}

		// Token: 0x17000745 RID: 1861
		// (get) Token: 0x06001912 RID: 6418 RVA: 0x0008475C File Offset: 0x0008295C
		// (set) Token: 0x06001913 RID: 6419 RVA: 0x00084778 File Offset: 0x00082978
		public string AttributeName
		{
			get
			{
				if (this.attrName == null)
				{
					return string.Empty;
				}
				return this.attrName;
			}
			set
			{
				this.attrName = value;
			}
		}

		// Token: 0x17000746 RID: 1862
		// (get) Token: 0x06001914 RID: 6420 RVA: 0x00084784 File Offset: 0x00082984
		// (set) Token: 0x06001915 RID: 6421 RVA: 0x000847A0 File Offset: 0x000829A0
		public string DataType
		{
			get
			{
				if (this.dataType == null)
				{
					return string.Empty;
				}
				return this.dataType;
			}
			set
			{
				this.dataType = value;
			}
		}

		// Token: 0x17000747 RID: 1863
		// (get) Token: 0x06001916 RID: 6422 RVA: 0x000847AC File Offset: 0x000829AC
		// (set) Token: 0x06001917 RID: 6423 RVA: 0x000847B4 File Offset: 0x000829B4
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x06001918 RID: 6424 RVA: 0x000847C0 File Offset: 0x000829C0
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SAA ");
			KeyHelper.AddField(sb, 1, this.attrName);
			KeyHelper.AddField(sb, 2, this.dataType);
			KeyHelper.AddField(sb, 3, this.ns);
			sb.Append("|");
		}

		// Token: 0x04000A76 RID: 2678
		private string attrName;

		// Token: 0x04000A77 RID: 2679
		private string dataType;

		// Token: 0x04000A78 RID: 2680
		private string ns;
	}
}
