﻿using System;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x020002BA RID: 698
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	public class XmlTypeAttribute : Attribute
	{
		// Token: 0x06001D58 RID: 7512 RVA: 0x0009B5E0 File Offset: 0x000997E0
		public XmlTypeAttribute()
		{
		}

		// Token: 0x06001D59 RID: 7513 RVA: 0x0009B5F0 File Offset: 0x000997F0
		public XmlTypeAttribute(string typeName)
		{
			this.typeName = typeName;
		}

		// Token: 0x1700080A RID: 2058
		// (get) Token: 0x06001D5A RID: 7514 RVA: 0x0009B608 File Offset: 0x00099808
		// (set) Token: 0x06001D5B RID: 7515 RVA: 0x0009B610 File Offset: 0x00099810
		public bool AnonymousType
		{
			get
			{
				return this.anonymousType;
			}
			set
			{
				this.anonymousType = value;
			}
		}

		// Token: 0x1700080B RID: 2059
		// (get) Token: 0x06001D5C RID: 7516 RVA: 0x0009B61C File Offset: 0x0009981C
		// (set) Token: 0x06001D5D RID: 7517 RVA: 0x0009B624 File Offset: 0x00099824
		public bool IncludeInSchema
		{
			get
			{
				return this.includeInSchema;
			}
			set
			{
				this.includeInSchema = value;
			}
		}

		// Token: 0x1700080C RID: 2060
		// (get) Token: 0x06001D5E RID: 7518 RVA: 0x0009B630 File Offset: 0x00099830
		// (set) Token: 0x06001D5F RID: 7519 RVA: 0x0009B638 File Offset: 0x00099838
		public string Namespace
		{
			get
			{
				return this.ns;
			}
			set
			{
				this.ns = value;
			}
		}

		// Token: 0x1700080D RID: 2061
		// (get) Token: 0x06001D60 RID: 7520 RVA: 0x0009B644 File Offset: 0x00099844
		// (set) Token: 0x06001D61 RID: 7521 RVA: 0x0009B660 File Offset: 0x00099860
		public string TypeName
		{
			get
			{
				if (this.typeName == null)
				{
					return string.Empty;
				}
				return this.typeName;
			}
			set
			{
				this.typeName = value;
			}
		}

		// Token: 0x06001D62 RID: 7522 RVA: 0x0009B66C File Offset: 0x0009986C
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("XTA ");
			KeyHelper.AddField(sb, 1, this.ns);
			KeyHelper.AddField(sb, 2, this.typeName);
			KeyHelper.AddField(sb, 4, this.includeInSchema);
			sb.Append('|');
		}

		// Token: 0x04000BA4 RID: 2980
		private bool includeInSchema = true;

		// Token: 0x04000BA5 RID: 2981
		private string ns;

		// Token: 0x04000BA6 RID: 2982
		private string typeName;

		// Token: 0x04000BA7 RID: 2983
		private bool anonymousType;
	}
}
