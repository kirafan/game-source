﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200026C RID: 620
	public class SoapAttributeOverrides
	{
		// Token: 0x06001919 RID: 6425 RVA: 0x0008480C File Offset: 0x00082A0C
		public SoapAttributeOverrides()
		{
			this.overrides = new Hashtable();
		}

		// Token: 0x17000748 RID: 1864
		public SoapAttributes this[Type type]
		{
			get
			{
				return this[type, string.Empty];
			}
		}

		// Token: 0x17000749 RID: 1865
		public SoapAttributes this[Type type, string member]
		{
			get
			{
				return (SoapAttributes)this.overrides[this.GetKey(type, member)];
			}
		}

		// Token: 0x0600191C RID: 6428 RVA: 0x0008484C File Offset: 0x00082A4C
		public void Add(Type type, SoapAttributes attributes)
		{
			this.Add(type, string.Empty, attributes);
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x0008485C File Offset: 0x00082A5C
		public void Add(Type type, string member, SoapAttributes attributes)
		{
			if (this.overrides[this.GetKey(type, member)] != null)
			{
				throw new Exception("The attributes for the given type and Member already exist in the collection");
			}
			this.overrides.Add(this.GetKey(type, member), attributes);
		}

		// Token: 0x0600191E RID: 6430 RVA: 0x000848A0 File Offset: 0x00082AA0
		private TypeMember GetKey(Type type, string member)
		{
			return new TypeMember(type, member);
		}

		// Token: 0x0600191F RID: 6431 RVA: 0x000848AC File Offset: 0x00082AAC
		internal void AddKeyHash(StringBuilder sb)
		{
			sb.Append("SAO ");
			foreach (object obj in this.overrides)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				SoapAttributes soapAttributes = (SoapAttributes)this.overrides[dictionaryEntry.Key];
				sb.Append(dictionaryEntry.Key.ToString()).Append(' ');
				soapAttributes.AddKeyHash(sb);
			}
			sb.Append("|");
		}

		// Token: 0x04000A79 RID: 2681
		private Hashtable overrides;
	}
}
