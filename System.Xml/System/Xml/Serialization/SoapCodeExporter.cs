﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;

namespace System.Xml.Serialization
{
	// Token: 0x0200026E RID: 622
	public class SoapCodeExporter : CodeExporter
	{
		// Token: 0x0600192F RID: 6447 RVA: 0x00084BC8 File Offset: 0x00082DC8
		public SoapCodeExporter(CodeNamespace codeNamespace) : this(codeNamespace, null)
		{
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x00084BD4 File Offset: 0x00082DD4
		public SoapCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit)
		{
			this.codeGenerator = new SoapMapCodeGenerator(codeNamespace, codeCompileUnit);
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x00084BEC File Offset: 0x00082DEC
		public SoapCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeGenerationOptions options) : this(codeNamespace, codeCompileUnit, null, options, null)
		{
		}

		// Token: 0x06001932 RID: 6450 RVA: 0x00084BFC File Offset: 0x00082DFC
		public SoapCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeGenerationOptions options, Hashtable mappings) : this(codeNamespace, codeCompileUnit, null, options, mappings)
		{
		}

		// Token: 0x06001933 RID: 6451 RVA: 0x00084C0C File Offset: 0x00082E0C
		[MonoTODO]
		public SoapCodeExporter(CodeNamespace codeNamespace, CodeCompileUnit codeCompileUnit, CodeDomProvider codeProvider, CodeGenerationOptions options, Hashtable mappings)
		{
			this.codeGenerator = new SoapMapCodeGenerator(codeNamespace, codeCompileUnit, codeProvider, options, mappings);
		}

		// Token: 0x06001934 RID: 6452 RVA: 0x00084C34 File Offset: 0x00082E34
		public void AddMappingMetadata(CodeAttributeDeclarationCollection metadata, XmlMemberMapping member)
		{
			this.AddMappingMetadata(metadata, member, false);
		}

		// Token: 0x06001935 RID: 6453 RVA: 0x00084C40 File Offset: 0x00082E40
		public void AddMappingMetadata(CodeAttributeDeclarationCollection metadata, XmlMemberMapping member, bool forceUseMemberName)
		{
			TypeData typeData = member.TypeMapMember.TypeData;
			CodeAttributeDeclaration codeAttributeDeclaration = new CodeAttributeDeclaration("System.Xml.Serialization.SoapElement");
			if (forceUseMemberName || member.ElementName != member.MemberName)
			{
				codeAttributeDeclaration.Arguments.Add(new CodeAttributeArgument(new CodePrimitiveExpression(member.ElementName)));
			}
			if (!TypeTranslator.IsDefaultPrimitiveTpeData(typeData))
			{
				codeAttributeDeclaration.Arguments.Add(new CodeAttributeArgument("DataType", new CodePrimitiveExpression(member.TypeName)));
			}
			if (codeAttributeDeclaration.Arguments.Count > 0)
			{
				metadata.Add(codeAttributeDeclaration);
			}
		}

		// Token: 0x06001936 RID: 6454 RVA: 0x00084CE4 File Offset: 0x00082EE4
		public void ExportMembersMapping(XmlMembersMapping xmlMembersMapping)
		{
			this.codeGenerator.ExportMembersMapping(xmlMembersMapping);
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x00084CF4 File Offset: 0x00082EF4
		public void ExportTypeMapping(XmlTypeMapping xmlTypeMapping)
		{
			this.codeGenerator.ExportTypeMapping(xmlTypeMapping, true);
		}
	}
}
