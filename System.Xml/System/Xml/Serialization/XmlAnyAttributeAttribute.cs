﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x0200027D RID: 637
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue)]
	public class XmlAnyAttributeAttribute : Attribute
	{
	}
}
