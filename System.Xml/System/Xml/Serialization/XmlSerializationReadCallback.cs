﻿using System;

namespace System.Xml.Serialization
{
	// Token: 0x020002D9 RID: 729
	// (Invoke) Token: 0x06001EB9 RID: 7865
	public delegate object XmlSerializationReadCallback();
}
