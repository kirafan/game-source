﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x02000282 RID: 642
	public class XmlArrayItemAttributes : CollectionBase
	{
		// Token: 0x17000781 RID: 1921
		public XmlArrayItemAttribute this[int index]
		{
			get
			{
				return (XmlArrayItemAttribute)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x060019F9 RID: 6649 RVA: 0x00088418 File Offset: 0x00086618
		public int Add(XmlArrayItemAttribute attribute)
		{
			return base.List.Add(attribute);
		}

		// Token: 0x060019FA RID: 6650 RVA: 0x00088428 File Offset: 0x00086628
		public bool Contains(XmlArrayItemAttribute attribute)
		{
			return base.List.Contains(attribute);
		}

		// Token: 0x060019FB RID: 6651 RVA: 0x00088438 File Offset: 0x00086638
		public void CopyTo(XmlArrayItemAttribute[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x060019FC RID: 6652 RVA: 0x00088448 File Offset: 0x00086648
		public int IndexOf(XmlArrayItemAttribute attribute)
		{
			return base.List.IndexOf(attribute);
		}

		// Token: 0x060019FD RID: 6653 RVA: 0x00088458 File Offset: 0x00086658
		public void Insert(int index, XmlArrayItemAttribute attribute)
		{
			base.List.Insert(index, attribute);
		}

		// Token: 0x060019FE RID: 6654 RVA: 0x00088468 File Offset: 0x00086668
		public void Remove(XmlArrayItemAttribute attribute)
		{
			base.List.Remove(attribute);
		}

		// Token: 0x060019FF RID: 6655 RVA: 0x00088478 File Offset: 0x00086678
		internal void AddKeyHash(StringBuilder sb)
		{
			if (this.Count == 0)
			{
				return;
			}
			sb.Append("XAIAS ");
			for (int i = 0; i < this.Count; i++)
			{
				this[i].AddKeyHash(sb);
			}
			sb.Append('|');
		}
	}
}
