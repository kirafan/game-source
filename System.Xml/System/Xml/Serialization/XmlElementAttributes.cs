﻿using System;
using System.Collections;
using System.Text;

namespace System.Xml.Serialization
{
	// Token: 0x0200028D RID: 653
	public class XmlElementAttributes : CollectionBase
	{
		// Token: 0x170007A9 RID: 1961
		public XmlElementAttribute this[int index]
		{
			get
			{
				return (XmlElementAttribute)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x0008AAB8 File Offset: 0x00088CB8
		public int Add(XmlElementAttribute attribute)
		{
			return base.List.Add(attribute);
		}

		// Token: 0x06001A93 RID: 6803 RVA: 0x0008AAC8 File Offset: 0x00088CC8
		public bool Contains(XmlElementAttribute attribute)
		{
			return base.List.Contains(attribute);
		}

		// Token: 0x06001A94 RID: 6804 RVA: 0x0008AAD8 File Offset: 0x00088CD8
		public int IndexOf(XmlElementAttribute attribute)
		{
			return base.List.IndexOf(attribute);
		}

		// Token: 0x06001A95 RID: 6805 RVA: 0x0008AAE8 File Offset: 0x00088CE8
		public void Insert(int index, XmlElementAttribute attribute)
		{
			base.List.Insert(index, attribute);
		}

		// Token: 0x06001A96 RID: 6806 RVA: 0x0008AAF8 File Offset: 0x00088CF8
		public void Remove(XmlElementAttribute attribute)
		{
			base.List.Remove(attribute);
		}

		// Token: 0x06001A97 RID: 6807 RVA: 0x0008AB08 File Offset: 0x00088D08
		public void CopyTo(XmlElementAttribute[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06001A98 RID: 6808 RVA: 0x0008AB18 File Offset: 0x00088D18
		internal void AddKeyHash(StringBuilder sb)
		{
			if (this.Count == 0)
			{
				return;
			}
			sb.Append("XEAS ");
			for (int i = 0; i < this.Count; i++)
			{
				this[i].AddKeyHash(sb);
			}
			sb.Append('|');
		}
	}
}
