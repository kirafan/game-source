﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000236 RID: 566
	public class XmlSchemaRedefine : XmlSchemaExternal
	{
		// Token: 0x06001670 RID: 5744 RVA: 0x000669F8 File Offset: 0x00064BF8
		public XmlSchemaRedefine()
		{
			this.attributeGroups = new XmlSchemaObjectTable();
			this.groups = new XmlSchemaObjectTable();
			this.items = new XmlSchemaObjectCollection(this);
			this.schemaTypes = new XmlSchemaObjectTable();
		}

		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x06001671 RID: 5745 RVA: 0x00066A30 File Offset: 0x00064C30
		[XmlElement("complexType", typeof(XmlSchemaComplexType))]
		[XmlElement("simpleType", typeof(XmlSchemaSimpleType))]
		[XmlElement("annotation", typeof(XmlSchemaAnnotation))]
		[XmlElement("attributeGroup", typeof(XmlSchemaAttributeGroup))]
		[XmlElement("group", typeof(XmlSchemaGroup))]
		public XmlSchemaObjectCollection Items
		{
			get
			{
				return this.items;
			}
		}

		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x06001672 RID: 5746 RVA: 0x00066A38 File Offset: 0x00064C38
		[XmlIgnore]
		public XmlSchemaObjectTable AttributeGroups
		{
			get
			{
				return this.attributeGroups;
			}
		}

		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x06001673 RID: 5747 RVA: 0x00066A40 File Offset: 0x00064C40
		[XmlIgnore]
		public XmlSchemaObjectTable SchemaTypes
		{
			get
			{
				return this.schemaTypes;
			}
		}

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x06001674 RID: 5748 RVA: 0x00066A48 File Offset: 0x00064C48
		[XmlIgnore]
		public XmlSchemaObjectTable Groups
		{
			get
			{
				return this.groups;
			}
		}

		// Token: 0x06001675 RID: 5749 RVA: 0x00066A50 File Offset: 0x00064C50
		internal override void SetParent(XmlSchemaObject parent)
		{
			base.SetParent(parent);
			foreach (XmlSchemaObject xmlSchemaObject in this.Items)
			{
				xmlSchemaObject.SetParent(this);
				xmlSchemaObject.isRedefinedComponent = true;
				xmlSchemaObject.isRedefineChild = true;
			}
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x00066AD0 File Offset: 0x00064CD0
		internal static XmlSchemaRedefine Read(XmlSchemaReader reader, ValidationEventHandler h)
		{
			XmlSchemaRedefine xmlSchemaRedefine = new XmlSchemaRedefine();
			reader.MoveToElement();
			if (reader.NamespaceURI != "http://www.w3.org/2001/XMLSchema" || reader.LocalName != "redefine")
			{
				XmlSchemaObject.error(h, "Should not happen :1: XmlSchemaRedefine.Read, name=" + reader.Name, null);
				reader.Skip();
				return null;
			}
			xmlSchemaRedefine.LineNumber = reader.LineNumber;
			xmlSchemaRedefine.LinePosition = reader.LinePosition;
			xmlSchemaRedefine.SourceUri = reader.BaseURI;
			while (reader.MoveToNextAttribute())
			{
				if (reader.Name == "id")
				{
					xmlSchemaRedefine.Id = reader.Value;
				}
				else if (reader.Name == "schemaLocation")
				{
					xmlSchemaRedefine.SchemaLocation = reader.Value;
				}
				else if ((reader.NamespaceURI == string.Empty && reader.Name != "xmlns") || reader.NamespaceURI == "http://www.w3.org/2001/XMLSchema")
				{
					XmlSchemaObject.error(h, reader.Name + " is not a valid attribute for redefine", null);
				}
				else
				{
					XmlSchemaUtil.ReadUnhandledAttribute(reader, xmlSchemaRedefine);
				}
			}
			reader.MoveToElement();
			if (reader.IsEmptyElement)
			{
				return xmlSchemaRedefine;
			}
			while (reader.ReadNextElement())
			{
				if (reader.NodeType == XmlNodeType.EndElement)
				{
					if (reader.LocalName != "redefine")
					{
						XmlSchemaObject.error(h, "Should not happen :2: XmlSchemaRedefine.Read, name=" + reader.Name, null);
					}
					break;
				}
				if (reader.LocalName == "annotation")
				{
					XmlSchemaAnnotation xmlSchemaAnnotation = XmlSchemaAnnotation.Read(reader, h);
					if (xmlSchemaAnnotation != null)
					{
						xmlSchemaRedefine.items.Add(xmlSchemaAnnotation);
					}
				}
				else if (reader.LocalName == "simpleType")
				{
					XmlSchemaSimpleType xmlSchemaSimpleType = XmlSchemaSimpleType.Read(reader, h);
					if (xmlSchemaSimpleType != null)
					{
						xmlSchemaRedefine.items.Add(xmlSchemaSimpleType);
					}
				}
				else if (reader.LocalName == "complexType")
				{
					XmlSchemaComplexType xmlSchemaComplexType = XmlSchemaComplexType.Read(reader, h);
					if (xmlSchemaComplexType != null)
					{
						xmlSchemaRedefine.items.Add(xmlSchemaComplexType);
					}
				}
				else if (reader.LocalName == "group")
				{
					XmlSchemaGroup xmlSchemaGroup = XmlSchemaGroup.Read(reader, h);
					if (xmlSchemaGroup != null)
					{
						xmlSchemaRedefine.items.Add(xmlSchemaGroup);
					}
				}
				else if (reader.LocalName == "attributeGroup")
				{
					XmlSchemaAttributeGroup xmlSchemaAttributeGroup = XmlSchemaAttributeGroup.Read(reader, h);
					if (xmlSchemaAttributeGroup != null)
					{
						xmlSchemaRedefine.items.Add(xmlSchemaAttributeGroup);
					}
				}
				else
				{
					reader.RaiseInvalidElementError();
				}
			}
			return xmlSchemaRedefine;
		}

		// Token: 0x040008FA RID: 2298
		private const string xmlname = "redefine";

		// Token: 0x040008FB RID: 2299
		private XmlSchemaObjectTable attributeGroups;

		// Token: 0x040008FC RID: 2300
		private XmlSchemaObjectTable groups;

		// Token: 0x040008FD RID: 2301
		private XmlSchemaObjectCollection items;

		// Token: 0x040008FE RID: 2302
		private XmlSchemaObjectTable schemaTypes;
	}
}
