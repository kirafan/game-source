﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x02000202 RID: 514
	public sealed class XmlSchemaCollectionEnumerator : IEnumerator
	{
		// Token: 0x06001491 RID: 5265 RVA: 0x00059E2C File Offset: 0x0005802C
		internal XmlSchemaCollectionEnumerator(ICollection col)
		{
			this.xenum = col.GetEnumerator();
		}

		// Token: 0x06001492 RID: 5266 RVA: 0x00059E40 File Offset: 0x00058040
		bool IEnumerator.MoveNext()
		{
			return this.xenum.MoveNext();
		}

		// Token: 0x06001493 RID: 5267 RVA: 0x00059E50 File Offset: 0x00058050
		void IEnumerator.Reset()
		{
			this.xenum.Reset();
		}

		// Token: 0x17000649 RID: 1609
		// (get) Token: 0x06001494 RID: 5268 RVA: 0x00059E60 File Offset: 0x00058060
		object IEnumerator.Current
		{
			get
			{
				return this.xenum.Current;
			}
		}

		// Token: 0x1700064A RID: 1610
		// (get) Token: 0x06001495 RID: 5269 RVA: 0x00059E70 File Offset: 0x00058070
		public XmlSchema Current
		{
			get
			{
				return (XmlSchema)this.xenum.Current;
			}
		}

		// Token: 0x06001496 RID: 5270 RVA: 0x00059E84 File Offset: 0x00058084
		public bool MoveNext()
		{
			return this.xenum.MoveNext();
		}

		// Token: 0x040007DD RID: 2013
		private IEnumerator xenum;
	}
}
