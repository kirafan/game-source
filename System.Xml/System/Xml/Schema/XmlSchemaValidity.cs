﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000248 RID: 584
	public enum XmlSchemaValidity
	{
		// Token: 0x040009A4 RID: 2468
		NotKnown,
		// Token: 0x040009A5 RID: 2469
		Valid,
		// Token: 0x040009A6 RID: 2470
		Invalid
	}
}
