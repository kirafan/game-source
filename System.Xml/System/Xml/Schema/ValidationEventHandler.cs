﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020002D3 RID: 723
	// (Invoke) Token: 0x06001EA1 RID: 7841
	public delegate void ValidationEventHandler(object sender, ValidationEventArgs e);
}
