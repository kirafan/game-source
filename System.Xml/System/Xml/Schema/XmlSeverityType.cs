﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200024C RID: 588
	public enum XmlSeverityType
	{
		// Token: 0x040009B0 RID: 2480
		Error,
		// Token: 0x040009B1 RID: 2481
		Warning
	}
}
