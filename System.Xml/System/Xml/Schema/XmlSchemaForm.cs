﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000216 RID: 534
	public enum XmlSchemaForm
	{
		// Token: 0x04000888 RID: 2184
		[XmlIgnore]
		None,
		// Token: 0x04000889 RID: 2185
		[XmlEnum("qualified")]
		Qualified,
		// Token: 0x0400088A RID: 2186
		[XmlEnum("unqualified")]
		Unqualified
	}
}
