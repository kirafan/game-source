﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000250 RID: 592
	[Flags]
	public enum XmlSchemaValidationFlags
	{
		// Token: 0x040009C0 RID: 2496
		None = 0,
		// Token: 0x040009C1 RID: 2497
		ProcessInlineSchema = 1,
		// Token: 0x040009C2 RID: 2498
		ProcessSchemaLocation = 2,
		// Token: 0x040009C3 RID: 2499
		ReportValidationWarnings = 4,
		// Token: 0x040009C4 RID: 2500
		ProcessIdentityConstraints = 8,
		// Token: 0x040009C5 RID: 2501
		[Obsolete("It is really idiotic idea to include such validation option that breaks W3C XML Schema specification compliance and interoperability.")]
		AllowXmlAttributes = 16
	}
}
