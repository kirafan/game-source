﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200020B RID: 523
	public enum XmlSchemaContentType
	{
		// Token: 0x04000805 RID: 2053
		TextOnly,
		// Token: 0x04000806 RID: 2054
		Empty,
		// Token: 0x04000807 RID: 2055
		ElementOnly,
		// Token: 0x04000808 RID: 2056
		Mixed
	}
}
