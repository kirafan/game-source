﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000245 RID: 581
	public enum XmlSchemaUse
	{
		// Token: 0x0400097F RID: 2431
		[XmlIgnore]
		None,
		// Token: 0x04000980 RID: 2432
		[XmlEnum("optional")]
		Optional,
		// Token: 0x04000981 RID: 2433
		[XmlEnum("prohibited")]
		Prohibited,
		// Token: 0x04000982 RID: 2434
		[XmlEnum("required")]
		Required
	}
}
