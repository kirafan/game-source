﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200020D RID: 525
	public enum XmlSchemaDatatypeVariety
	{
		// Token: 0x04000841 RID: 2113
		Atomic,
		// Token: 0x04000842 RID: 2114
		List,
		// Token: 0x04000843 RID: 2115
		Union
	}
}
