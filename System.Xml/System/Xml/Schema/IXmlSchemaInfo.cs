﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020001EE RID: 494
	public interface IXmlSchemaInfo
	{
		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x0600136C RID: 4972
		bool IsDefault { get; }

		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x0600136D RID: 4973
		bool IsNil { get; }

		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x0600136E RID: 4974
		XmlSchemaSimpleType MemberType { get; }

		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x0600136F RID: 4975
		XmlSchemaAttribute SchemaAttribute { get; }

		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x06001370 RID: 4976
		XmlSchemaElement SchemaElement { get; }

		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x06001371 RID: 4977
		XmlSchemaType SchemaType { get; }

		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x06001372 RID: 4978
		XmlSchemaValidity Validity { get; }
	}
}
