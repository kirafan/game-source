﻿using System;
using System.Collections;

namespace System.Xml.Schema
{
	// Token: 0x0200022F RID: 559
	public class XmlSchemaObjectCollection : CollectionBase
	{
		// Token: 0x06001621 RID: 5665 RVA: 0x00065F9C File Offset: 0x0006419C
		public XmlSchemaObjectCollection()
		{
		}

		// Token: 0x06001622 RID: 5666 RVA: 0x00065FA4 File Offset: 0x000641A4
		public XmlSchemaObjectCollection(XmlSchemaObject parent)
		{
		}

		// Token: 0x170006C5 RID: 1733
		public virtual XmlSchemaObject this[int index]
		{
			get
			{
				return (XmlSchemaObject)base.List[index];
			}
			set
			{
				base.List[index] = value;
			}
		}

		// Token: 0x06001625 RID: 5669 RVA: 0x00065FD0 File Offset: 0x000641D0
		public int Add(XmlSchemaObject item)
		{
			return base.List.Add(item);
		}

		// Token: 0x06001626 RID: 5670 RVA: 0x00065FE0 File Offset: 0x000641E0
		public bool Contains(XmlSchemaObject item)
		{
			return base.List.Contains(item);
		}

		// Token: 0x06001627 RID: 5671 RVA: 0x00065FF0 File Offset: 0x000641F0
		public void CopyTo(XmlSchemaObject[] array, int index)
		{
			base.List.CopyTo(array, index);
		}

		// Token: 0x06001628 RID: 5672 RVA: 0x00066000 File Offset: 0x00064200
		public new XmlSchemaObjectEnumerator GetEnumerator()
		{
			return new XmlSchemaObjectEnumerator(base.List);
		}

		// Token: 0x06001629 RID: 5673 RVA: 0x00066010 File Offset: 0x00064210
		public int IndexOf(XmlSchemaObject item)
		{
			return base.List.IndexOf(item);
		}

		// Token: 0x0600162A RID: 5674 RVA: 0x00066020 File Offset: 0x00064220
		public void Insert(int index, XmlSchemaObject item)
		{
			base.List.Insert(index, item);
		}

		// Token: 0x0600162B RID: 5675 RVA: 0x00066030 File Offset: 0x00064230
		protected override void OnClear()
		{
		}

		// Token: 0x0600162C RID: 5676 RVA: 0x00066034 File Offset: 0x00064234
		protected override void OnInsert(int index, object item)
		{
		}

		// Token: 0x0600162D RID: 5677 RVA: 0x00066038 File Offset: 0x00064238
		protected override void OnRemove(int index, object item)
		{
		}

		// Token: 0x0600162E RID: 5678 RVA: 0x0006603C File Offset: 0x0006423C
		protected override void OnSet(int index, object oldValue, object newValue)
		{
		}

		// Token: 0x0600162F RID: 5679 RVA: 0x00066040 File Offset: 0x00064240
		public void Remove(XmlSchemaObject item)
		{
			base.List.Remove(item);
		}
	}
}
