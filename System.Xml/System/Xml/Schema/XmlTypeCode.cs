﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x02000251 RID: 593
	public enum XmlTypeCode
	{
		// Token: 0x040009C7 RID: 2503
		None,
		// Token: 0x040009C8 RID: 2504
		Item,
		// Token: 0x040009C9 RID: 2505
		Node,
		// Token: 0x040009CA RID: 2506
		Document,
		// Token: 0x040009CB RID: 2507
		Element,
		// Token: 0x040009CC RID: 2508
		Attribute,
		// Token: 0x040009CD RID: 2509
		Namespace,
		// Token: 0x040009CE RID: 2510
		ProcessingInstruction,
		// Token: 0x040009CF RID: 2511
		Comment,
		// Token: 0x040009D0 RID: 2512
		Text,
		// Token: 0x040009D1 RID: 2513
		AnyAtomicType,
		// Token: 0x040009D2 RID: 2514
		UntypedAtomic,
		// Token: 0x040009D3 RID: 2515
		String,
		// Token: 0x040009D4 RID: 2516
		Boolean,
		// Token: 0x040009D5 RID: 2517
		Decimal,
		// Token: 0x040009D6 RID: 2518
		Float,
		// Token: 0x040009D7 RID: 2519
		Double,
		// Token: 0x040009D8 RID: 2520
		Duration,
		// Token: 0x040009D9 RID: 2521
		DateTime,
		// Token: 0x040009DA RID: 2522
		Time,
		// Token: 0x040009DB RID: 2523
		Date,
		// Token: 0x040009DC RID: 2524
		GYearMonth,
		// Token: 0x040009DD RID: 2525
		GYear,
		// Token: 0x040009DE RID: 2526
		GMonthDay,
		// Token: 0x040009DF RID: 2527
		GDay,
		// Token: 0x040009E0 RID: 2528
		GMonth,
		// Token: 0x040009E1 RID: 2529
		HexBinary,
		// Token: 0x040009E2 RID: 2530
		Base64Binary,
		// Token: 0x040009E3 RID: 2531
		AnyUri,
		// Token: 0x040009E4 RID: 2532
		QName,
		// Token: 0x040009E5 RID: 2533
		Notation,
		// Token: 0x040009E6 RID: 2534
		NormalizedString,
		// Token: 0x040009E7 RID: 2535
		Token,
		// Token: 0x040009E8 RID: 2536
		Language,
		// Token: 0x040009E9 RID: 2537
		NmToken,
		// Token: 0x040009EA RID: 2538
		Name,
		// Token: 0x040009EB RID: 2539
		NCName,
		// Token: 0x040009EC RID: 2540
		Id,
		// Token: 0x040009ED RID: 2541
		Idref,
		// Token: 0x040009EE RID: 2542
		Entity,
		// Token: 0x040009EF RID: 2543
		Integer,
		// Token: 0x040009F0 RID: 2544
		NonPositiveInteger,
		// Token: 0x040009F1 RID: 2545
		NegativeInteger,
		// Token: 0x040009F2 RID: 2546
		Long,
		// Token: 0x040009F3 RID: 2547
		Int,
		// Token: 0x040009F4 RID: 2548
		Short,
		// Token: 0x040009F5 RID: 2549
		Byte,
		// Token: 0x040009F6 RID: 2550
		NonNegativeInteger,
		// Token: 0x040009F7 RID: 2551
		UnsignedLong,
		// Token: 0x040009F8 RID: 2552
		UnsignedInt,
		// Token: 0x040009F9 RID: 2553
		UnsignedShort,
		// Token: 0x040009FA RID: 2554
		UnsignedByte,
		// Token: 0x040009FB RID: 2555
		PositiveInteger,
		// Token: 0x040009FC RID: 2556
		YearMonthDuration,
		// Token: 0x040009FD RID: 2557
		DayTimeDuration
	}
}
