﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x0200020E RID: 526
	[Flags]
	public enum XmlSchemaDerivationMethod
	{
		// Token: 0x04000845 RID: 2117
		[XmlEnum("")]
		Empty = 0,
		// Token: 0x04000846 RID: 2118
		[XmlEnum("substitution")]
		Substitution = 1,
		// Token: 0x04000847 RID: 2119
		[XmlEnum("extension")]
		Extension = 2,
		// Token: 0x04000848 RID: 2120
		[XmlEnum("restriction")]
		Restriction = 4,
		// Token: 0x04000849 RID: 2121
		[XmlEnum("list")]
		List = 8,
		// Token: 0x0400084A RID: 2122
		[XmlEnum("union")]
		Union = 16,
		// Token: 0x0400084B RID: 2123
		[XmlEnum("#all")]
		All = 255,
		// Token: 0x0400084C RID: 2124
		[XmlIgnore]
		None = 256
	}
}
