﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x0200020A RID: 522
	public enum XmlSchemaContentProcessing
	{
		// Token: 0x04000800 RID: 2048
		[XmlIgnore]
		None,
		// Token: 0x04000801 RID: 2049
		[XmlEnum("skip")]
		Skip,
		// Token: 0x04000802 RID: 2050
		[XmlEnum("lax")]
		Lax,
		// Token: 0x04000803 RID: 2051
		[XmlEnum("strict")]
		Strict
	}
}
