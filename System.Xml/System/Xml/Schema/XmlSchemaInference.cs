﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x0200021E RID: 542
	[MonoTODO]
	public sealed class XmlSchemaInference
	{
		// Token: 0x170006A8 RID: 1704
		// (get) Token: 0x0600159A RID: 5530 RVA: 0x0006239C File Offset: 0x0006059C
		// (set) Token: 0x0600159B RID: 5531 RVA: 0x000623A4 File Offset: 0x000605A4
		public XmlSchemaInference.InferenceOption Occurrence
		{
			get
			{
				return this.occurrence;
			}
			set
			{
				this.occurrence = value;
			}
		}

		// Token: 0x170006A9 RID: 1705
		// (get) Token: 0x0600159C RID: 5532 RVA: 0x000623B0 File Offset: 0x000605B0
		// (set) Token: 0x0600159D RID: 5533 RVA: 0x000623B8 File Offset: 0x000605B8
		public XmlSchemaInference.InferenceOption TypeInference
		{
			get
			{
				return this.typeInference;
			}
			set
			{
				this.typeInference = value;
			}
		}

		// Token: 0x0600159E RID: 5534 RVA: 0x000623C4 File Offset: 0x000605C4
		public XmlSchemaSet InferSchema(XmlReader xmlReader)
		{
			return this.InferSchema(xmlReader, new XmlSchemaSet());
		}

		// Token: 0x0600159F RID: 5535 RVA: 0x000623D4 File Offset: 0x000605D4
		public XmlSchemaSet InferSchema(XmlReader xmlReader, XmlSchemaSet schemas)
		{
			return XsdInference.Process(xmlReader, schemas, this.occurrence == XmlSchemaInference.InferenceOption.Relaxed, this.typeInference == XmlSchemaInference.InferenceOption.Relaxed);
		}

		// Token: 0x040008A1 RID: 2209
		private XmlSchemaInference.InferenceOption occurrence;

		// Token: 0x040008A2 RID: 2210
		private XmlSchemaInference.InferenceOption typeInference;

		// Token: 0x0200021F RID: 543
		public enum InferenceOption
		{
			// Token: 0x040008A4 RID: 2212
			Restricted,
			// Token: 0x040008A5 RID: 2213
			Relaxed
		}
	}
}
