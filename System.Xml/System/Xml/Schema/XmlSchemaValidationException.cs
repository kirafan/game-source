﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.Schema
{
	// Token: 0x02000249 RID: 585
	[Serializable]
	public class XmlSchemaValidationException : XmlSchemaException
	{
		// Token: 0x060017AC RID: 6060 RVA: 0x0007761C File Offset: 0x0007581C
		public XmlSchemaValidationException()
		{
		}

		// Token: 0x060017AD RID: 6061 RVA: 0x00077624 File Offset: 0x00075824
		public XmlSchemaValidationException(string message) : base(message)
		{
		}

		// Token: 0x060017AE RID: 6062 RVA: 0x00077630 File Offset: 0x00075830
		protected XmlSchemaValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060017AF RID: 6063 RVA: 0x0007763C File Offset: 0x0007583C
		public XmlSchemaValidationException(string message, Exception innerException, int lineNumber, int linePosition) : base(message, lineNumber, linePosition, null, null, innerException)
		{
		}

		// Token: 0x060017B0 RID: 6064 RVA: 0x0007764C File Offset: 0x0007584C
		internal XmlSchemaValidationException(string message, int lineNumber, int linePosition, XmlSchemaObject sourceObject, string sourceUri, Exception innerException) : base(message, lineNumber, linePosition, sourceObject, sourceUri, innerException)
		{
		}

		// Token: 0x060017B1 RID: 6065 RVA: 0x00077660 File Offset: 0x00075860
		internal XmlSchemaValidationException(string message, object sender, string sourceUri, XmlSchemaObject sourceObject, Exception innerException) : base(message, sender, sourceUri, sourceObject, innerException)
		{
		}

		// Token: 0x060017B2 RID: 6066 RVA: 0x00077670 File Offset: 0x00075870
		internal XmlSchemaValidationException(string message, XmlSchemaObject sourceObject, Exception innerException) : base(message, sourceObject, innerException)
		{
		}

		// Token: 0x060017B3 RID: 6067 RVA: 0x0007767C File Offset: 0x0007587C
		public XmlSchemaValidationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060017B4 RID: 6068 RVA: 0x00077688 File Offset: 0x00075888
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}

		// Token: 0x060017B5 RID: 6069 RVA: 0x00077694 File Offset: 0x00075894
		protected internal void SetSourceObject(object o)
		{
			this.source_object = o;
		}

		// Token: 0x17000719 RID: 1817
		// (get) Token: 0x060017B6 RID: 6070 RVA: 0x000776A0 File Offset: 0x000758A0
		public object SourceObject
		{
			get
			{
				return this.source_object;
			}
		}

		// Token: 0x040009A7 RID: 2471
		private object source_object;
	}
}
