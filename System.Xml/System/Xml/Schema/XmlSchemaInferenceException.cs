﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Xml.Schema
{
	// Token: 0x02000221 RID: 545
	[Serializable]
	public class XmlSchemaInferenceException : XmlSchemaException
	{
		// Token: 0x060015C7 RID: 5575 RVA: 0x000642C8 File Offset: 0x000624C8
		public XmlSchemaInferenceException()
		{
		}

		// Token: 0x060015C8 RID: 5576 RVA: 0x000642D0 File Offset: 0x000624D0
		public XmlSchemaInferenceException(string message) : base(message)
		{
		}

		// Token: 0x060015C9 RID: 5577 RVA: 0x000642DC File Offset: 0x000624DC
		protected XmlSchemaInferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060015CA RID: 5578 RVA: 0x000642E8 File Offset: 0x000624E8
		public XmlSchemaInferenceException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060015CB RID: 5579 RVA: 0x000642F4 File Offset: 0x000624F4
		public XmlSchemaInferenceException(string message, Exception innerException, int line, int column) : base(message, innerException, line, column)
		{
		}

		// Token: 0x060015CC RID: 5580 RVA: 0x00064304 File Offset: 0x00062504
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
}
