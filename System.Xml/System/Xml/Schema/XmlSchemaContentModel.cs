﻿using System;
using System.Xml.Serialization;

namespace System.Xml.Schema
{
	// Token: 0x02000209 RID: 521
	public abstract class XmlSchemaContentModel : XmlSchemaAnnotated
	{
		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x060014EF RID: 5359
		// (set) Token: 0x060014F0 RID: 5360
		[XmlIgnore]
		public abstract XmlSchemaContent Content { get; set; }
	}
}
