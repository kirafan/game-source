﻿using System;

namespace System.Xml.Schema
{
	// Token: 0x020002D4 RID: 724
	// (Invoke) Token: 0x06001EA5 RID: 7845
	public delegate object XmlValueGetter();
}
