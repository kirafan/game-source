﻿using System;
using System.Xml.XPath;

namespace System.Xml
{
	// Token: 0x0200011E RID: 286
	public class XmlText : XmlCharacterData
	{
		// Token: 0x06000C17 RID: 3095 RVA: 0x0003CED8 File Offset: 0x0003B0D8
		protected internal XmlText(string strData, XmlDocument doc) : base(strData, doc)
		{
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x06000C18 RID: 3096 RVA: 0x0003CEE4 File Offset: 0x0003B0E4
		public override string LocalName
		{
			get
			{
				return "#text";
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x06000C19 RID: 3097 RVA: 0x0003CEEC File Offset: 0x0003B0EC
		public override string Name
		{
			get
			{
				return "#text";
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06000C1A RID: 3098 RVA: 0x0003CEF4 File Offset: 0x0003B0F4
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.Text;
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06000C1B RID: 3099 RVA: 0x0003CEF8 File Offset: 0x0003B0F8
		internal override XPathNodeType XPathNodeType
		{
			get
			{
				return XPathNodeType.Text;
			}
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000C1C RID: 3100 RVA: 0x0003CEFC File Offset: 0x0003B0FC
		// (set) Token: 0x06000C1D RID: 3101 RVA: 0x0003CF04 File Offset: 0x0003B104
		public override string Value
		{
			get
			{
				return this.Data;
			}
			set
			{
				this.Data = value;
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06000C1E RID: 3102 RVA: 0x0003CF10 File Offset: 0x0003B110
		public override XmlNode ParentNode
		{
			get
			{
				return base.ParentNode;
			}
		}

		// Token: 0x06000C1F RID: 3103 RVA: 0x0003CF18 File Offset: 0x0003B118
		public override XmlNode CloneNode(bool deep)
		{
			return this.OwnerDocument.CreateTextNode(this.Data);
		}

		// Token: 0x06000C20 RID: 3104 RVA: 0x0003CF38 File Offset: 0x0003B138
		public virtual XmlText SplitText(int offset)
		{
			XmlText xmlText = this.OwnerDocument.CreateTextNode(this.Data.Substring(offset));
			this.DeleteData(offset, this.Data.Length - offset);
			this.ParentNode.InsertAfter(xmlText, this);
			return xmlText;
		}

		// Token: 0x06000C21 RID: 3105 RVA: 0x0003CF80 File Offset: 0x0003B180
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x06000C22 RID: 3106 RVA: 0x0003CF84 File Offset: 0x0003B184
		public override void WriteTo(XmlWriter w)
		{
			w.WriteString(this.Data);
		}
	}
}
