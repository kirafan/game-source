﻿using System;
using System.IO;
using System.Net;
using System.Security.Permissions;

namespace System.Xml
{
	// Token: 0x0200011A RID: 282
	public abstract class XmlResolver
	{
		// Token: 0x17000370 RID: 880
		// (set) Token: 0x06000C01 RID: 3073
		public abstract ICredentials Credentials { set; }

		// Token: 0x06000C02 RID: 3074
		public abstract object GetEntity(Uri absoluteUri, string role, Type type);

		// Token: 0x06000C03 RID: 3075 RVA: 0x0003CBBC File Offset: 0x0003ADBC
		[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"\nUnrestricted=\"true\"/>\n")]
		public virtual Uri ResolveUri(Uri baseUri, string relativeUri)
		{
			if (baseUri == null)
			{
				if (relativeUri == null)
				{
					throw new ArgumentNullException("Either baseUri or relativeUri are required.");
				}
				if (relativeUri.StartsWith("http:") || relativeUri.StartsWith("https:") || relativeUri.StartsWith("ftp:") || relativeUri.StartsWith("file:"))
				{
					return new Uri(relativeUri);
				}
				return new Uri(Path.GetFullPath(relativeUri));
			}
			else
			{
				if (relativeUri == null)
				{
					return baseUri;
				}
				return new Uri(baseUri, this.EscapeRelativeUriBody(relativeUri));
			}
		}

		// Token: 0x06000C04 RID: 3076 RVA: 0x0003CC50 File Offset: 0x0003AE50
		private string EscapeRelativeUriBody(string src)
		{
			return src.Replace("<", "%3C").Replace(">", "%3E").Replace("#", "%23").Replace("%", "%25").Replace("\"", "%22");
		}
	}
}
