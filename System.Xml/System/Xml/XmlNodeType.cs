﻿using System;

namespace System.Xml
{
	// Token: 0x02000110 RID: 272
	public enum XmlNodeType
	{
		// Token: 0x04000559 RID: 1369
		None,
		// Token: 0x0400055A RID: 1370
		Element,
		// Token: 0x0400055B RID: 1371
		Attribute,
		// Token: 0x0400055C RID: 1372
		Text,
		// Token: 0x0400055D RID: 1373
		CDATA,
		// Token: 0x0400055E RID: 1374
		EntityReference,
		// Token: 0x0400055F RID: 1375
		Entity,
		// Token: 0x04000560 RID: 1376
		ProcessingInstruction,
		// Token: 0x04000561 RID: 1377
		Comment,
		// Token: 0x04000562 RID: 1378
		Document,
		// Token: 0x04000563 RID: 1379
		DocumentType,
		// Token: 0x04000564 RID: 1380
		DocumentFragment,
		// Token: 0x04000565 RID: 1381
		Notation,
		// Token: 0x04000566 RID: 1382
		Whitespace,
		// Token: 0x04000567 RID: 1383
		SignificantWhitespace,
		// Token: 0x04000568 RID: 1384
		EndElement,
		// Token: 0x04000569 RID: 1385
		EndEntity,
		// Token: 0x0400056A RID: 1386
		XmlDeclaration
	}
}
