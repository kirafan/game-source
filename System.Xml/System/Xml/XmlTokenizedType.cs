﻿using System;

namespace System.Xml
{
	// Token: 0x02000126 RID: 294
	public enum XmlTokenizedType
	{
		// Token: 0x04000609 RID: 1545
		CDATA,
		// Token: 0x0400060A RID: 1546
		ID,
		// Token: 0x0400060B RID: 1547
		IDREF,
		// Token: 0x0400060C RID: 1548
		IDREFS,
		// Token: 0x0400060D RID: 1549
		ENTITY,
		// Token: 0x0400060E RID: 1550
		ENTITIES,
		// Token: 0x0400060F RID: 1551
		NMTOKEN,
		// Token: 0x04000610 RID: 1552
		NMTOKENS,
		// Token: 0x04000611 RID: 1553
		NOTATION,
		// Token: 0x04000612 RID: 1554
		ENUMERATION,
		// Token: 0x04000613 RID: 1555
		QName,
		// Token: 0x04000614 RID: 1556
		NCName,
		// Token: 0x04000615 RID: 1557
		None
	}
}
