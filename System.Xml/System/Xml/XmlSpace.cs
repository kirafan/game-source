﻿using System;

namespace System.Xml
{
	// Token: 0x0200011D RID: 285
	public enum XmlSpace
	{
		// Token: 0x040005AA RID: 1450
		None,
		// Token: 0x040005AB RID: 1451
		Default,
		// Token: 0x040005AC RID: 1452
		Preserve
	}
}
