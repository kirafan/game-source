﻿using System;

namespace System.Xml
{
	// Token: 0x020000E9 RID: 233
	public class XmlCDataSection : XmlCharacterData
	{
		// Token: 0x06000845 RID: 2117 RVA: 0x0002E08C File Offset: 0x0002C28C
		protected internal XmlCDataSection(string data, XmlDocument doc) : base(data, doc)
		{
		}

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x0002E098 File Offset: 0x0002C298
		public override string LocalName
		{
			get
			{
				return "#cdata-section";
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000847 RID: 2119 RVA: 0x0002E0A0 File Offset: 0x0002C2A0
		public override string Name
		{
			get
			{
				return "#cdata-section";
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000848 RID: 2120 RVA: 0x0002E0A8 File Offset: 0x0002C2A8
		public override XmlNodeType NodeType
		{
			get
			{
				return XmlNodeType.CDATA;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000849 RID: 2121 RVA: 0x0002E0AC File Offset: 0x0002C2AC
		public override XmlNode ParentNode
		{
			get
			{
				return base.ParentNode;
			}
		}

		// Token: 0x0600084A RID: 2122 RVA: 0x0002E0B4 File Offset: 0x0002C2B4
		public override XmlNode CloneNode(bool deep)
		{
			return new XmlCDataSection(this.Data, this.OwnerDocument);
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x0002E0D4 File Offset: 0x0002C2D4
		public override void WriteContentTo(XmlWriter w)
		{
		}

		// Token: 0x0600084C RID: 2124 RVA: 0x0002E0D8 File Offset: 0x0002C2D8
		public override void WriteTo(XmlWriter w)
		{
			w.WriteCData(this.Data);
		}
	}
}
