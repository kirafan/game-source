﻿using System;

namespace System.Configuration
{
	// Token: 0x0200003A RID: 58
	public enum ConfigurationUserLevel
	{
		// Token: 0x040000C0 RID: 192
		None,
		// Token: 0x040000C1 RID: 193
		PerUserRoaming = 10,
		// Token: 0x040000C2 RID: 194
		PerUserRoamingAndLocal = 20
	}
}
