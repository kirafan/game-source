﻿using System;

namespace System.Configuration
{
	// Token: 0x02000043 RID: 67
	public sealed class DefaultValidator : ConfigurationValidatorBase
	{
		// Token: 0x06000283 RID: 643 RVA: 0x00008104 File Offset: 0x00006304
		public override bool CanValidate(Type type)
		{
			return true;
		}

		// Token: 0x06000284 RID: 644 RVA: 0x00008108 File Offset: 0x00006308
		public override void Validate(object value)
		{
		}
	}
}
