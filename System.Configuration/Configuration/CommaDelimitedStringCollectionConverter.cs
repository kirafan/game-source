﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x02000019 RID: 25
	public sealed class CommaDelimitedStringCollectionConverter : ConfigurationConverterBase
	{
		// Token: 0x060000C8 RID: 200 RVA: 0x00002CA4 File Offset: 0x00000EA4
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			CommaDelimitedStringCollection commaDelimitedStringCollection = new CommaDelimitedStringCollection();
			string[] array = ((string)data).Split(new char[]
			{
				','
			});
			foreach (string text in array)
			{
				commaDelimitedStringCollection.Add(text.Trim());
			}
			return commaDelimitedStringCollection;
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00002CFC File Offset: 0x00000EFC
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value == null)
			{
				return null;
			}
			if (!typeof(StringCollection).IsAssignableFrom(value.GetType()))
			{
				throw new ArgumentException();
			}
			return value.ToString();
		}
	}
}
