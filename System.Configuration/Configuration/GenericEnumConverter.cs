﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x02000048 RID: 72
	public sealed class GenericEnumConverter : ConfigurationConverterBase
	{
		// Token: 0x060002A0 RID: 672 RVA: 0x00008460 File Offset: 0x00006660
		public GenericEnumConverter(Type typeEnum)
		{
			if (typeEnum == null)
			{
				throw new ArgumentNullException("typeEnum");
			}
			this.typeEnum = typeEnum;
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x00008480 File Offset: 0x00006680
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			if (data == null)
			{
				throw new ArgumentException();
			}
			return Enum.Parse(this.typeEnum, (string)data);
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x000084A0 File Offset: 0x000066A0
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			return value.ToString();
		}

		// Token: 0x040000D9 RID: 217
		private Type typeEnum;
	}
}
