﻿using System;
using System.Collections;
using System.Configuration.Internal;
using System.Runtime.Serialization;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x02000027 RID: 39
	[Serializable]
	public class ConfigurationErrorsException : ConfigurationException
	{
		// Token: 0x06000189 RID: 393 RVA: 0x00005C9C File Offset: 0x00003E9C
		public ConfigurationErrorsException()
		{
		}

		// Token: 0x0600018A RID: 394 RVA: 0x00005CA4 File Offset: 0x00003EA4
		public ConfigurationErrorsException(string message) : base(message)
		{
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00005CB0 File Offset: 0x00003EB0
		protected ConfigurationErrorsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.filename = info.GetString("ConfigurationErrors_Filename");
			this.line = info.GetInt32("ConfigurationErrors_Line");
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00005CE8 File Offset: 0x00003EE8
		public ConfigurationErrorsException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600018D RID: 397 RVA: 0x00005CF4 File Offset: 0x00003EF4
		public ConfigurationErrorsException(string message, XmlNode node) : this(message, null, ConfigurationErrorsException.GetFilename(node), ConfigurationErrorsException.GetLineNumber(node))
		{
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00005D0C File Offset: 0x00003F0C
		public ConfigurationErrorsException(string message, Exception inner, XmlNode node) : this(message, inner, ConfigurationErrorsException.GetFilename(node), ConfigurationErrorsException.GetLineNumber(node))
		{
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00005D24 File Offset: 0x00003F24
		public ConfigurationErrorsException(string message, XmlReader reader) : this(message, null, ConfigurationErrorsException.GetFilename(reader), ConfigurationErrorsException.GetLineNumber(reader))
		{
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00005D3C File Offset: 0x00003F3C
		public ConfigurationErrorsException(string message, Exception inner, XmlReader reader) : this(message, inner, ConfigurationErrorsException.GetFilename(reader), ConfigurationErrorsException.GetLineNumber(reader))
		{
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00005D54 File Offset: 0x00003F54
		public ConfigurationErrorsException(string message, string filename, int line) : this(message, null, filename, line)
		{
		}

		// Token: 0x06000192 RID: 402 RVA: 0x00005D60 File Offset: 0x00003F60
		public ConfigurationErrorsException(string message, Exception inner, string filename, int line) : base(message, inner)
		{
			this.filename = filename;
			this.line = line;
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00005D7C File Offset: 0x00003F7C
		public override string BareMessage
		{
			get
			{
				return base.BareMessage;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000194 RID: 404 RVA: 0x00005D84 File Offset: 0x00003F84
		public ICollection Errors
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00005D8C File Offset: 0x00003F8C
		public override string Filename
		{
			get
			{
				return this.filename;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000196 RID: 406 RVA: 0x00005D94 File Offset: 0x00003F94
		public override int Line
		{
			get
			{
				return this.line;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00005D9C File Offset: 0x00003F9C
		public override string Message
		{
			get
			{
				string result;
				if (!string.IsNullOrEmpty(this.filename))
				{
					if (this.line != 0)
					{
						result = string.Concat(new object[]
						{
							this.BareMessage,
							" (",
							this.filename,
							" line ",
							this.line,
							")"
						});
					}
					else
					{
						result = this.BareMessage + " (" + this.filename + ")";
					}
				}
				else if (this.line != 0)
				{
					result = string.Concat(new object[]
					{
						this.BareMessage,
						" (line ",
						this.line,
						")"
					});
				}
				else
				{
					result = this.BareMessage;
				}
				return result;
			}
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00005E7C File Offset: 0x0000407C
		public static string GetFilename(XmlReader reader)
		{
			if (reader is IConfigErrorInfo)
			{
				return ((IConfigErrorInfo)reader).Filename;
			}
			return (reader == null) ? null : reader.BaseURI;
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00005EA8 File Offset: 0x000040A8
		public static int GetLineNumber(XmlReader reader)
		{
			if (reader is IConfigErrorInfo)
			{
				return ((IConfigErrorInfo)reader).LineNumber;
			}
			IXmlLineInfo xmlLineInfo = reader as IXmlLineInfo;
			return (xmlLineInfo == null) ? 0 : xmlLineInfo.LineNumber;
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00005EE8 File Offset: 0x000040E8
		public static string GetFilename(XmlNode node)
		{
			if (!(node is IConfigErrorInfo))
			{
				return null;
			}
			return ((IConfigErrorInfo)node).Filename;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00005F04 File Offset: 0x00004104
		public static int GetLineNumber(XmlNode node)
		{
			if (!(node is IConfigErrorInfo))
			{
				return 0;
			}
			return ((IConfigErrorInfo)node).LineNumber;
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00005F20 File Offset: 0x00004120
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("ConfigurationErrors_Filename", this.filename);
			info.AddValue("ConfigurationErrors_Line", this.line);
		}

		// Token: 0x0400007A RID: 122
		private readonly string filename;

		// Token: 0x0400007B RID: 123
		private readonly int line;
	}
}
