﻿using System;

namespace System.Configuration
{
	// Token: 0x0200005C RID: 92
	public class PositiveTimeSpanValidator : ConfigurationValidatorBase
	{
		// Token: 0x0600033E RID: 830 RVA: 0x00009470 File Offset: 0x00007670
		public override bool CanValidate(Type type)
		{
			return type == typeof(TimeSpan);
		}

		// Token: 0x0600033F RID: 831 RVA: 0x00009480 File Offset: 0x00007680
		public override void Validate(object value)
		{
			TimeSpan t = (TimeSpan)value;
			if (t <= new TimeSpan(0L))
			{
				throw new ArgumentException("The time span value must be positive");
			}
		}
	}
}
