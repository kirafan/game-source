﻿using System;

namespace System.Configuration
{
	// Token: 0x02000085 RID: 133
	// (Invoke) Token: 0x06000447 RID: 1095
	public delegate void ValidatorCallback(object o);
}
