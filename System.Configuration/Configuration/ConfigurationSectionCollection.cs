﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace System.Configuration
{
	// Token: 0x02000037 RID: 55
	[Serializable]
	public sealed class ConfigurationSectionCollection : NameObjectCollectionBase
	{
		// Token: 0x06000221 RID: 545 RVA: 0x00007658 File Offset: 0x00005858
		internal ConfigurationSectionCollection(Configuration config, SectionGroupInfo group) : base(StringComparer.Ordinal)
		{
			this.config = config;
			this.group = group;
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000222 RID: 546 RVA: 0x00007674 File Offset: 0x00005874
		public override NameObjectCollectionBase.KeysCollection Keys
		{
			get
			{
				return this.group.Sections.Keys;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000223 RID: 547 RVA: 0x00007688 File Offset: 0x00005888
		public override int Count
		{
			get
			{
				return this.group.Sections.Count;
			}
		}

		// Token: 0x1700009D RID: 157
		public ConfigurationSection this[string name]
		{
			get
			{
				ConfigurationSection configurationSection = base.BaseGet(name) as ConfigurationSection;
				if (configurationSection == null)
				{
					SectionInfo sectionInfo = this.group.Sections[name] as SectionInfo;
					if (sectionInfo == null)
					{
						return null;
					}
					configurationSection = this.config.GetSectionInstance(sectionInfo, true);
					if (configurationSection == null)
					{
						return null;
					}
					base.BaseSet(name, configurationSection);
				}
				return configurationSection;
			}
		}

		// Token: 0x1700009E RID: 158
		public ConfigurationSection this[int index]
		{
			get
			{
				return this[this.GetKey(index)];
			}
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000770C File Offset: 0x0000590C
		public void Add(string name, ConfigurationSection section)
		{
			this.config.CreateSection(this.group, name, section);
		}

		// Token: 0x06000227 RID: 551 RVA: 0x00007724 File Offset: 0x00005924
		public void Clear()
		{
			if (this.group.Sections != null)
			{
				foreach (object obj in this.group.Sections)
				{
					ConfigInfo configInfo = (ConfigInfo)obj;
					this.config.RemoveConfigInfo(configInfo);
				}
			}
		}

		// Token: 0x06000228 RID: 552 RVA: 0x000077B0 File Offset: 0x000059B0
		public void CopyTo(ConfigurationSection[] array, int index)
		{
			for (int i = 0; i < this.group.Sections.Count; i++)
			{
				array[i + index] = this[i];
			}
		}

		// Token: 0x06000229 RID: 553 RVA: 0x000077EC File Offset: 0x000059EC
		public ConfigurationSection Get(int index)
		{
			return this[index];
		}

		// Token: 0x0600022A RID: 554 RVA: 0x000077F8 File Offset: 0x000059F8
		public ConfigurationSection Get(string name)
		{
			return this[name];
		}

		// Token: 0x0600022B RID: 555 RVA: 0x00007804 File Offset: 0x00005A04
		public override IEnumerator GetEnumerator()
		{
			foreach (object obj in this.group.Sections.AllKeys)
			{
				string key = (string)obj;
				yield return this[key];
			}
			yield break;
		}

		// Token: 0x0600022C RID: 556 RVA: 0x00007820 File Offset: 0x00005A20
		public string GetKey(int index)
		{
			return this.group.Sections.GetKey(index);
		}

		// Token: 0x0600022D RID: 557 RVA: 0x00007834 File Offset: 0x00005A34
		public void Remove(string name)
		{
			SectionInfo sectionInfo = this.group.Sections[name] as SectionInfo;
			if (sectionInfo != null)
			{
				this.config.RemoveConfigInfo(sectionInfo);
			}
		}

		// Token: 0x0600022E RID: 558 RVA: 0x0000786C File Offset: 0x00005A6C
		public void RemoveAt(int index)
		{
			SectionInfo sectionInfo = this.group.Sections[index] as SectionInfo;
			this.config.RemoveConfigInfo(sectionInfo);
		}

		// Token: 0x0600022F RID: 559 RVA: 0x0000789C File Offset: 0x00005A9C
		[MonoTODO]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040000B2 RID: 178
		private SectionGroupInfo group;

		// Token: 0x040000B3 RID: 179
		private Configuration config;
	}
}
