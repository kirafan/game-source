﻿using System;

namespace System.Configuration
{
	// Token: 0x0200001E RID: 30
	public enum ConfigurationAllowExeDefinition
	{
		// Token: 0x0400004D RID: 77
		MachineOnly,
		// Token: 0x0400004E RID: 78
		MachineToApplication = 100,
		// Token: 0x0400004F RID: 79
		MachineToLocalUser = 300,
		// Token: 0x04000050 RID: 80
		MachineToRoamingUser = 200
	}
}
