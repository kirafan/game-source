﻿using System;
using System.Text.RegularExpressions;

namespace System.Configuration
{
	// Token: 0x02000069 RID: 105
	public class RegexStringValidator : ConfigurationValidatorBase
	{
		// Token: 0x06000395 RID: 917 RVA: 0x00009D40 File Offset: 0x00007F40
		public RegexStringValidator(string regex)
		{
			this.regex = regex;
		}

		// Token: 0x06000396 RID: 918 RVA: 0x00009D50 File Offset: 0x00007F50
		public override bool CanValidate(Type type)
		{
			return type == typeof(string);
		}

		// Token: 0x06000397 RID: 919 RVA: 0x00009D60 File Offset: 0x00007F60
		public override void Validate(object value)
		{
			if (!Regex.IsMatch((string)value, this.regex))
			{
				throw new ArgumentException("The string must match the regexp `{0}'", this.regex);
			}
		}

		// Token: 0x0400011C RID: 284
		private string regex;
	}
}
