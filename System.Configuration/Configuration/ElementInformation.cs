﻿using System;
using System.Collections;

namespace System.Configuration
{
	// Token: 0x02000045 RID: 69
	public sealed class ElementInformation
	{
		// Token: 0x0600028A RID: 650 RVA: 0x00008178 File Offset: 0x00006378
		internal ElementInformation(ConfigurationElement owner, PropertyInformation propertyInfo)
		{
			this.propertyInfo = propertyInfo;
			this.owner = owner;
			this.properties = new PropertyInformationCollection();
			foreach (object obj in owner.Properties)
			{
				ConfigurationProperty property = (ConfigurationProperty)obj;
				this.properties.Add(new PropertyInformation(owner, property));
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600028B RID: 651 RVA: 0x00008214 File Offset: 0x00006414
		[MonoTODO]
		public ICollection Errors
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600028C RID: 652 RVA: 0x0000821C File Offset: 0x0000641C
		public bool IsCollection
		{
			get
			{
				return this.owner is ConfigurationElementCollection;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600028D RID: 653 RVA: 0x0000822C File Offset: 0x0000642C
		public bool IsLocked
		{
			get
			{
				return this.propertyInfo != null && this.propertyInfo.IsLocked;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600028E RID: 654 RVA: 0x0000824C File Offset: 0x0000644C
		[MonoTODO]
		public bool IsPresent
		{
			get
			{
				return this.propertyInfo != null;
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x0600028F RID: 655 RVA: 0x0000825C File Offset: 0x0000645C
		public int LineNumber
		{
			get
			{
				return (this.propertyInfo == null) ? 0 : this.propertyInfo.LineNumber;
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000290 RID: 656 RVA: 0x0000827C File Offset: 0x0000647C
		public string Source
		{
			get
			{
				return (this.propertyInfo == null) ? null : this.propertyInfo.Source;
			}
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000291 RID: 657 RVA: 0x0000829C File Offset: 0x0000649C
		public Type Type
		{
			get
			{
				return (this.propertyInfo == null) ? this.owner.GetType() : this.propertyInfo.Type;
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000292 RID: 658 RVA: 0x000082D0 File Offset: 0x000064D0
		public ConfigurationValidatorBase Validator
		{
			get
			{
				return (this.propertyInfo == null) ? new DefaultValidator() : this.propertyInfo.Validator;
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000293 RID: 659 RVA: 0x00008300 File Offset: 0x00006500
		public PropertyInformationCollection Properties
		{
			get
			{
				return this.properties;
			}
		}

		// Token: 0x06000294 RID: 660 RVA: 0x00008308 File Offset: 0x00006508
		internal void Reset(ElementInformation parentInfo)
		{
			foreach (object obj in this.Properties)
			{
				PropertyInformation propertyInformation = (PropertyInformation)obj;
				PropertyInformation parentProperty = parentInfo.Properties[propertyInformation.Name];
				propertyInformation.Reset(parentProperty);
			}
		}

		// Token: 0x040000D1 RID: 209
		private readonly PropertyInformation propertyInfo;

		// Token: 0x040000D2 RID: 210
		private readonly ConfigurationElement owner;

		// Token: 0x040000D3 RID: 211
		private readonly PropertyInformationCollection properties;
	}
}
