﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x0200006B RID: 107
	public sealed class RsaProtectedConfigurationProvider : ProtectedConfigurationProvider
	{
		// Token: 0x0600039C RID: 924 RVA: 0x00009DDC File Offset: 0x00007FDC
		private RSACryptoServiceProvider GetProvider()
		{
			if (this.rsa == null)
			{
				CspParameters cspParameters = new CspParameters();
				cspParameters.ProviderName = this.cspProviderName;
				cspParameters.KeyContainerName = this.keyContainerName;
				if (this.useMachineContainer)
				{
					cspParameters.Flags |= CspProviderFlags.UseMachineKeyStore;
				}
				this.rsa = new RSACryptoServiceProvider(cspParameters);
			}
			return this.rsa;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x00009E40 File Offset: 0x00008040
		[MonoTODO]
		public override XmlNode Decrypt(XmlNode encrypted_node)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(new StringReader(encrypted_node.OuterXml));
			EncryptedXml encryptedXml = new EncryptedXml(xmlDocument);
			encryptedXml.AddKeyNameMapping("Rsa Key", this.GetProvider());
			encryptedXml.DecryptDocument();
			return xmlDocument.DocumentElement;
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00009E88 File Offset: 0x00008088
		[MonoTODO]
		public override XmlNode Encrypt(XmlNode node)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(new StringReader(node.OuterXml));
			EncryptedXml encryptedXml = new EncryptedXml(xmlDocument);
			encryptedXml.AddKeyNameMapping("Rsa Key", this.GetProvider());
			EncryptedData encryptedData = encryptedXml.Encrypt(xmlDocument.DocumentElement, "Rsa Key");
			return encryptedData.GetXml();
		}

		// Token: 0x0600039F RID: 927 RVA: 0x00009EDC File Offset: 0x000080DC
		[MonoTODO]
		public override void Initialize(string name, NameValueCollection configurationValues)
		{
			base.Initialize(name, configurationValues);
			this.keyContainerName = configurationValues["keyContainerName"];
			this.cspProviderName = configurationValues["cspProviderName"];
			string text = configurationValues["useMachineContainer"];
			if (text != null && text.ToLower() == "true")
			{
				this.useMachineContainer = true;
			}
			text = configurationValues["useOAEP"];
			if (text != null && text.ToLower() == "true")
			{
				this.useOAEP = true;
			}
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00009F70 File Offset: 0x00008170
		[MonoTODO]
		public void AddKey(int keySize, bool exportable)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00009F78 File Offset: 0x00008178
		[MonoTODO]
		public void DeleteKey()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00009F80 File Offset: 0x00008180
		[MonoTODO]
		public void ExportKey(string xmlFileName, bool includePrivateParameters)
		{
			RSACryptoServiceProvider provider = this.GetProvider();
			string value = provider.ToXmlString(includePrivateParameters);
			FileStream stream = new FileStream(xmlFileName, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter streamWriter = new StreamWriter(stream);
			streamWriter.Write(value);
			streamWriter.Close();
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00009FBC File Offset: 0x000081BC
		[MonoTODO]
		public void ImportKey(string xmlFileName, bool exportable)
		{
			throw new NotImplementedException();
		}

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x060003A4 RID: 932 RVA: 0x00009FC4 File Offset: 0x000081C4
		public string CspProviderName
		{
			get
			{
				return this.cspProviderName;
			}
		}

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x060003A5 RID: 933 RVA: 0x00009FCC File Offset: 0x000081CC
		public string KeyContainerName
		{
			get
			{
				return this.keyContainerName;
			}
		}

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x060003A6 RID: 934 RVA: 0x00009FD4 File Offset: 0x000081D4
		public RSAParameters RsaPublicKey
		{
			get
			{
				RSACryptoServiceProvider provider = this.GetProvider();
				return provider.ExportParameters(false);
			}
		}

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x060003A7 RID: 935 RVA: 0x00009FF0 File Offset: 0x000081F0
		public bool UseMachineContainer
		{
			get
			{
				return this.useMachineContainer;
			}
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x060003A8 RID: 936 RVA: 0x00009FF8 File Offset: 0x000081F8
		public bool UseOAEP
		{
			get
			{
				return this.useOAEP;
			}
		}

		// Token: 0x0400011F RID: 287
		private string cspProviderName;

		// Token: 0x04000120 RID: 288
		private string keyContainerName;

		// Token: 0x04000121 RID: 289
		private bool useMachineContainer;

		// Token: 0x04000122 RID: 290
		private bool useOAEP;

		// Token: 0x04000123 RID: 291
		private RSACryptoServiceProvider rsa;
	}
}
