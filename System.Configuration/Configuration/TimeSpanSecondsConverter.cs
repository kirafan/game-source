﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x0200007C RID: 124
	public class TimeSpanSecondsConverter : ConfigurationConverterBase
	{
		// Token: 0x06000420 RID: 1056 RVA: 0x0000BAC8 File Offset: 0x00009CC8
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			if (!(data is string))
			{
				throw new ArgumentException("data");
			}
			long num;
			if (!long.TryParse((string)data, out num))
			{
				throw new ArgumentException("data");
			}
			return TimeSpan.FromSeconds((double)num);
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x0000BB14 File Offset: 0x00009D14
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value.GetType() != typeof(TimeSpan))
			{
				throw new ArgumentException();
			}
			return ((long)((TimeSpan)value).TotalSeconds).ToString();
		}
	}
}
