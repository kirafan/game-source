﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x02000080 RID: 128
	public sealed class TypeNameConverter : ConfigurationConverterBase
	{
		// Token: 0x06000435 RID: 1077 RVA: 0x0000BE2C File Offset: 0x0000A02C
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			return Type.GetType((string)data);
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x0000BE3C File Offset: 0x0000A03C
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value == null)
			{
				return null;
			}
			if (!(value is Type))
			{
				throw new ArgumentException("value");
			}
			return ((Type)value).AssemblyQualifiedName;
		}
	}
}
