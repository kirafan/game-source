﻿using System;
using System.Runtime.Serialization;

namespace System.Configuration.Provider
{
	// Token: 0x02000012 RID: 18
	[Serializable]
	public class ProviderException : Exception
	{
		// Token: 0x06000091 RID: 145 RVA: 0x00002498 File Offset: 0x00000698
		public ProviderException()
		{
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000024A0 File Offset: 0x000006A0
		protected ProviderException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000093 RID: 147 RVA: 0x000024AC File Offset: 0x000006AC
		public ProviderException(string message) : base(message)
		{
		}

		// Token: 0x06000094 RID: 148 RVA: 0x000024B8 File Offset: 0x000006B8
		public ProviderException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
