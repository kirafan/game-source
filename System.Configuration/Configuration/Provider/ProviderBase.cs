﻿using System;
using System.Collections.Specialized;

namespace System.Configuration.Provider
{
	// Token: 0x02000011 RID: 17
	public abstract class ProviderBase
	{
		// Token: 0x0600008E RID: 142 RVA: 0x000023EC File Offset: 0x000005EC
		public virtual void Initialize(string name, NameValueCollection config)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Provider name cannot be null or empty.", "name");
			}
			if (this.alreadyInitialized)
			{
				throw new InvalidOperationException("This provider instance has already been initialized.");
			}
			this.alreadyInitialized = true;
			this._name = name;
			if (config != null)
			{
				this._description = config["description"];
				config.Remove("description");
			}
			if (string.IsNullOrEmpty(this._description))
			{
				this._description = this._name;
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x0600008F RID: 143 RVA: 0x00002488 File Offset: 0x00000688
		public virtual string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000090 RID: 144 RVA: 0x00002490 File Offset: 0x00000690
		public virtual string Description
		{
			get
			{
				return this._description;
			}
		}

		// Token: 0x04000020 RID: 32
		private bool alreadyInitialized;

		// Token: 0x04000021 RID: 33
		private string _description;

		// Token: 0x04000022 RID: 34
		private string _name;
	}
}
