﻿using System;
using System.Collections;

namespace System.Configuration.Provider
{
	// Token: 0x02000013 RID: 19
	public class ProviderCollection : ICollection, IEnumerable
	{
		// Token: 0x06000095 RID: 149 RVA: 0x000024C4 File Offset: 0x000006C4
		public ProviderCollection()
		{
			this.lookup = new Hashtable(10, StringComparer.InvariantCultureIgnoreCase);
			this.values = new ArrayList();
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000024EC File Offset: 0x000006EC
		void ICollection.CopyTo(Array array, int index)
		{
			this.values.CopyTo(array, index);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000024FC File Offset: 0x000006FC
		public virtual void Add(ProviderBase provider)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			if (provider == null || provider.Name == null)
			{
				throw new ArgumentNullException();
			}
			int num = this.values.Add(provider);
			try
			{
				this.lookup.Add(provider.Name, num);
			}
			catch
			{
				this.values.RemoveAt(num);
				throw;
			}
		}

		// Token: 0x06000098 RID: 152 RVA: 0x0000258C File Offset: 0x0000078C
		public void Clear()
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			this.values.Clear();
			this.lookup.Clear();
		}

		// Token: 0x06000099 RID: 153 RVA: 0x000025B8 File Offset: 0x000007B8
		public void CopyTo(ProviderBase[] array, int index)
		{
			this.values.CopyTo(array, index);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x000025C8 File Offset: 0x000007C8
		public IEnumerator GetEnumerator()
		{
			return this.values.GetEnumerator();
		}

		// Token: 0x0600009B RID: 155 RVA: 0x000025D8 File Offset: 0x000007D8
		public void Remove(string name)
		{
			if (this.readOnly)
			{
				throw new NotSupportedException();
			}
			object obj = this.lookup[name];
			if (obj == null || !(obj is int))
			{
				throw new ArgumentException();
			}
			int num = (int)obj;
			if (num >= this.values.Count)
			{
				throw new ArgumentException();
			}
			this.values.RemoveAt(num);
			this.lookup.Remove(name);
			ArrayList arrayList = new ArrayList();
			foreach (object obj2 in this.lookup)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
				if ((int)dictionaryEntry.Value > num)
				{
					arrayList.Add(dictionaryEntry.Key);
				}
			}
			foreach (object obj3 in arrayList)
			{
				string key = (string)obj3;
				this.lookup[key] = (int)this.lookup[key] - 1;
			}
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00002760 File Offset: 0x00000960
		public void SetReadOnly()
		{
			this.readOnly = true;
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600009D RID: 157 RVA: 0x0000276C File Offset: 0x0000096C
		public int Count
		{
			get
			{
				return this.values.Count;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600009E RID: 158 RVA: 0x0000277C File Offset: 0x0000097C
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00002780 File Offset: 0x00000980
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000026 RID: 38
		public ProviderBase this[string name]
		{
			get
			{
				object obj = this.lookup[name];
				if (obj == null)
				{
					return null;
				}
				return this.values[(int)obj] as ProviderBase;
			}
		}

		// Token: 0x04000023 RID: 35
		private Hashtable lookup;

		// Token: 0x04000024 RID: 36
		private bool readOnly;

		// Token: 0x04000025 RID: 37
		private ArrayList values;
	}
}
