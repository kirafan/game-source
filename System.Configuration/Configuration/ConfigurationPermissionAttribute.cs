﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Configuration
{
	// Token: 0x0200002F RID: 47
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class ConfigurationPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x060001D8 RID: 472 RVA: 0x00006B84 File Offset: 0x00004D84
		public ConfigurationPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00006B90 File Offset: 0x00004D90
		public override IPermission CreatePermission()
		{
			return new ConfigurationPermission((!base.Unrestricted) ? PermissionState.None : PermissionState.Unrestricted);
		}
	}
}
