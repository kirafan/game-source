﻿using System;

namespace System.Configuration
{
	// Token: 0x0200003C RID: 60
	public abstract class ConfigurationValidatorBase
	{
		// Token: 0x06000252 RID: 594 RVA: 0x00007C74 File Offset: 0x00005E74
		public virtual bool CanValidate(Type type)
		{
			return false;
		}

		// Token: 0x06000253 RID: 595
		public abstract void Validate(object value);
	}
}
