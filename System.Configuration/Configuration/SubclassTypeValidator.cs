﻿using System;

namespace System.Configuration
{
	// Token: 0x02000078 RID: 120
	public sealed class SubclassTypeValidator : ConfigurationValidatorBase
	{
		// Token: 0x06000413 RID: 1043 RVA: 0x0000B944 File Offset: 0x00009B44
		public SubclassTypeValidator(Type baseClass)
		{
			this.baseClass = baseClass;
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x0000B954 File Offset: 0x00009B54
		public override bool CanValidate(Type type)
		{
			return type == typeof(Type);
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x0000B964 File Offset: 0x00009B64
		public override void Validate(object value)
		{
			Type c = (Type)value;
			if (!this.baseClass.IsAssignableFrom(c))
			{
				throw new ArgumentException("The value must be a subclass");
			}
		}

		// Token: 0x04000146 RID: 326
		private Type baseClass;
	}
}
