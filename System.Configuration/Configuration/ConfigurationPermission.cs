﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Configuration
{
	// Token: 0x0200002E RID: 46
	[Serializable]
	public sealed class ConfigurationPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x060001D0 RID: 464 RVA: 0x0000699C File Offset: 0x00004B9C
		public ConfigurationPermission(PermissionState state)
		{
			this.unrestricted = (state == PermissionState.Unrestricted);
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x000069B0 File Offset: 0x00004BB0
		public override IPermission Copy()
		{
			return new ConfigurationPermission((!this.unrestricted) ? PermissionState.None : PermissionState.Unrestricted);
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x000069CC File Offset: 0x00004BCC
		public override void FromXml(SecurityElement securityElement)
		{
			if (securityElement == null)
			{
				throw new ArgumentNullException("securityElement");
			}
			if (securityElement.Tag != "IPermission")
			{
				throw new ArgumentException("securityElement");
			}
			string text = securityElement.Attribute("Unrestricted");
			if (text != null)
			{
				this.unrestricted = (string.Compare(text, "true", StringComparison.InvariantCultureIgnoreCase) == 0);
			}
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00006A34 File Offset: 0x00004C34
		public override IPermission Intersect(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			ConfigurationPermission configurationPermission = target as ConfigurationPermission;
			if (configurationPermission == null)
			{
				throw new ArgumentException("target");
			}
			return new ConfigurationPermission((!this.unrestricted || !configurationPermission.IsUnrestricted()) ? PermissionState.None : PermissionState.Unrestricted);
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x00006A84 File Offset: 0x00004C84
		public override IPermission Union(IPermission target)
		{
			if (target == null)
			{
				return this.Copy();
			}
			ConfigurationPermission configurationPermission = target as ConfigurationPermission;
			if (configurationPermission == null)
			{
				throw new ArgumentException("target");
			}
			return new ConfigurationPermission((!this.unrestricted && !configurationPermission.IsUnrestricted()) ? PermissionState.None : PermissionState.Unrestricted);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x00006AD8 File Offset: 0x00004CD8
		public override bool IsSubsetOf(IPermission target)
		{
			if (target == null)
			{
				return !this.unrestricted;
			}
			ConfigurationPermission configurationPermission = target as ConfigurationPermission;
			if (configurationPermission == null)
			{
				throw new ArgumentException("target");
			}
			return !this.unrestricted || configurationPermission.IsUnrestricted();
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x00006B20 File Offset: 0x00004D20
		public bool IsUnrestricted()
		{
			return this.unrestricted;
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x00006B28 File Offset: 0x00004D28
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("IPermission");
			securityElement.AddAttribute("class", base.GetType().AssemblyQualifiedName);
			securityElement.AddAttribute("version", "1");
			if (this.unrestricted)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			return securityElement;
		}

		// Token: 0x04000092 RID: 146
		private bool unrestricted;
	}
}
