﻿using System;

namespace System.Configuration
{
	// Token: 0x0200004D RID: 77
	public class IntegerValidator : ConfigurationValidatorBase
	{
		// Token: 0x060002B3 RID: 691 RVA: 0x0000860C File Offset: 0x0000680C
		public IntegerValidator(int minValue, int maxValue, bool rangeIsExclusive, int resolution)
		{
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.rangeIsExclusive = rangeIsExclusive;
			this.resolution = resolution;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x00008634 File Offset: 0x00006834
		public IntegerValidator(int minValue, int maxValue, bool rangeIsExclusive) : this(minValue, maxValue, rangeIsExclusive, 0)
		{
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x00008640 File Offset: 0x00006840
		public IntegerValidator(int minValue, int maxValue) : this(minValue, maxValue, false, 0)
		{
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000864C File Offset: 0x0000684C
		public override bool CanValidate(Type type)
		{
			return type == typeof(int);
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x0000865C File Offset: 0x0000685C
		public override void Validate(object value)
		{
			int num = (int)value;
			if (!this.rangeIsExclusive)
			{
				if (num < this.minValue || num > this.maxValue)
				{
					throw new ArgumentException(string.Concat(new object[]
					{
						"The value must be in the range ",
						this.minValue,
						" - ",
						this.maxValue
					}));
				}
			}
			else if (num >= this.minValue && num <= this.maxValue)
			{
				throw new ArgumentException(string.Concat(new object[]
				{
					"The value must not be in the range ",
					this.minValue,
					" - ",
					this.maxValue
				}));
			}
			if (this.resolution != 0 && num % this.resolution != 0)
			{
				throw new ArgumentException("The value must have a resolution of " + this.resolution);
			}
		}

		// Token: 0x040000DC RID: 220
		private bool rangeIsExclusive;

		// Token: 0x040000DD RID: 221
		private int minValue;

		// Token: 0x040000DE RID: 222
		private int maxValue;

		// Token: 0x040000DF RID: 223
		private int resolution;
	}
}
