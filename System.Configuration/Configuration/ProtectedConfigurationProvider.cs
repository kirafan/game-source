﻿using System;
using System.Configuration.Provider;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x02000063 RID: 99
	public abstract class ProtectedConfigurationProvider : ProviderBase
	{
		// Token: 0x0600036A RID: 874
		public abstract XmlNode Decrypt(XmlNode encrypted_node);

		// Token: 0x0600036B RID: 875
		public abstract XmlNode Encrypt(XmlNode node);
	}
}
