﻿using System;

namespace System.Configuration
{
	// Token: 0x02000076 RID: 118
	public class StringValidator : ConfigurationValidatorBase
	{
		// Token: 0x06000406 RID: 1030 RVA: 0x0000B770 File Offset: 0x00009970
		public StringValidator(int minLength)
		{
			this.minLength = minLength;
			this.maxLength = int.MaxValue;
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x0000B78C File Offset: 0x0000998C
		public StringValidator(int minLength, int maxLength)
		{
			this.minLength = minLength;
			this.maxLength = maxLength;
		}

		// Token: 0x06000408 RID: 1032 RVA: 0x0000B7A4 File Offset: 0x000099A4
		public StringValidator(int minLength, int maxLength, string invalidCharacters)
		{
			this.minLength = minLength;
			this.maxLength = maxLength;
			if (invalidCharacters != null)
			{
				this.invalidCharacters = invalidCharacters.ToCharArray();
			}
		}

		// Token: 0x06000409 RID: 1033 RVA: 0x0000B7D8 File Offset: 0x000099D8
		public override bool CanValidate(Type type)
		{
			return type == typeof(string);
		}

		// Token: 0x0600040A RID: 1034 RVA: 0x0000B7E8 File Offset: 0x000099E8
		public override void Validate(object value)
		{
			if (value == null && this.minLength <= 0)
			{
				return;
			}
			string text = (string)value;
			if (text == null || text.Length < this.minLength)
			{
				throw new ArgumentException("The string must be at least " + this.minLength + " characters long.");
			}
			if (text.Length > this.maxLength)
			{
				throw new ArgumentException("The string must be no more than " + this.maxLength + " characters long.");
			}
			if (this.invalidCharacters != null)
			{
				int num = text.IndexOfAny(this.invalidCharacters);
				if (num != -1)
				{
					throw new ArgumentException(string.Format("The string cannot contain any of the following characters: '{0}'.", this.invalidCharacters));
				}
			}
		}

		// Token: 0x0400013F RID: 319
		private char[] invalidCharacters;

		// Token: 0x04000140 RID: 320
		private int maxLength;

		// Token: 0x04000141 RID: 321
		private int minLength;
	}
}
