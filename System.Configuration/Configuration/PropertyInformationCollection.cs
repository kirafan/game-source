﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace System.Configuration
{
	// Token: 0x0200005F RID: 95
	[Serializable]
	public sealed class PropertyInformationCollection : NameObjectCollectionBase
	{
		// Token: 0x0600035B RID: 859 RVA: 0x0000972C File Offset: 0x0000792C
		internal PropertyInformationCollection() : base(StringComparer.Ordinal)
		{
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000973C File Offset: 0x0000793C
		public void CopyTo(PropertyInformation[] array, int index)
		{
			((ICollection)this).CopyTo(array, index);
		}

		// Token: 0x170000FA RID: 250
		public PropertyInformation this[string propertyName]
		{
			get
			{
				return (PropertyInformation)base.BaseGet(propertyName);
			}
		}

		// Token: 0x0600035E RID: 862 RVA: 0x00009758 File Offset: 0x00007958
		public override IEnumerator GetEnumerator()
		{
			return new PropertyInformationCollection.PropertyInformationEnumerator(this);
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00009760 File Offset: 0x00007960
		internal void Add(PropertyInformation pi)
		{
			base.BaseAdd(pi.Name, pi);
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00009770 File Offset: 0x00007970
		[MonoTODO]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x02000060 RID: 96
		private class PropertyInformationEnumerator : IEnumerator
		{
			// Token: 0x06000361 RID: 865 RVA: 0x00009778 File Offset: 0x00007978
			public PropertyInformationEnumerator(PropertyInformationCollection collection)
			{
				this.collection = collection;
				this.position = -1;
			}

			// Token: 0x170000FB RID: 251
			// (get) Token: 0x06000362 RID: 866 RVA: 0x00009790 File Offset: 0x00007990
			public object Current
			{
				get
				{
					if (this.position < this.collection.Count && this.position >= 0)
					{
						return this.collection.BaseGet(this.position);
					}
					throw new InvalidOperationException();
				}
			}

			// Token: 0x06000363 RID: 867 RVA: 0x000097CC File Offset: 0x000079CC
			public bool MoveNext()
			{
				return ++this.position < this.collection.Count;
			}

			// Token: 0x06000364 RID: 868 RVA: 0x00009804 File Offset: 0x00007A04
			public void Reset()
			{
				this.position = -1;
			}

			// Token: 0x04000108 RID: 264
			private PropertyInformationCollection collection;

			// Token: 0x04000109 RID: 265
			private int position;
		}
	}
}
