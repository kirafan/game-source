﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x0200004C RID: 76
	public sealed class InfiniteTimeSpanConverter : ConfigurationConverterBase
	{
		// Token: 0x060002B1 RID: 689 RVA: 0x00008580 File Offset: 0x00006780
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			if ((string)data == "Infinite")
			{
				return TimeSpan.MaxValue;
			}
			return TimeSpan.Parse((string)data);
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x000085C0 File Offset: 0x000067C0
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value.GetType() != typeof(TimeSpan))
			{
				throw new ArgumentException();
			}
			if ((TimeSpan)value == TimeSpan.MaxValue)
			{
				return "Infinite";
			}
			return value.ToString();
		}
	}
}
