﻿using System;

namespace System.Configuration
{
	// Token: 0x02000025 RID: 37
	public enum ConfigurationElementCollectionType
	{
		// Token: 0x04000075 RID: 117
		BasicMap,
		// Token: 0x04000076 RID: 118
		AddRemoveClearMap,
		// Token: 0x04000077 RID: 119
		BasicMapAlternate,
		// Token: 0x04000078 RID: 120
		AddRemoveClearMapAlternate
	}
}
