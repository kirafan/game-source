﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Configuration
{
	// Token: 0x02000032 RID: 50
	public class ConfigurationPropertyCollection : ICollection, IEnumerable
	{
		// Token: 0x060001FB RID: 507 RVA: 0x00006F60 File Offset: 0x00005160
		public ConfigurationPropertyCollection()
		{
			this.collection = new List<ConfigurationProperty>();
		}

		// Token: 0x060001FC RID: 508 RVA: 0x00006F74 File Offset: 0x00005174
		void ICollection.CopyTo(Array array, int index)
		{
			((ICollection)this.collection).CopyTo(array, index);
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060001FD RID: 509 RVA: 0x00006F84 File Offset: 0x00005184
		public int Count
		{
			get
			{
				return this.collection.Count;
			}
		}

		// Token: 0x1700008F RID: 143
		public ConfigurationProperty this[string name]
		{
			get
			{
				foreach (ConfigurationProperty configurationProperty in this.collection)
				{
					if (configurationProperty.Name == name)
					{
						return configurationProperty;
					}
				}
				return null;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00007010 File Offset: 0x00005210
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000200 RID: 512 RVA: 0x00007014 File Offset: 0x00005214
		public object SyncRoot
		{
			get
			{
				return this.collection;
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000701C File Offset: 0x0000521C
		public void Add(ConfigurationProperty property)
		{
			this.collection.Add(property);
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000702C File Offset: 0x0000522C
		public bool Contains(string name)
		{
			ConfigurationProperty configurationProperty = this[name];
			return configurationProperty != null && this.collection.Contains(configurationProperty);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x00007058 File Offset: 0x00005258
		public void CopyTo(ConfigurationProperty[] array, int index)
		{
			this.collection.CopyTo(array, index);
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00007068 File Offset: 0x00005268
		public IEnumerator GetEnumerator()
		{
			return this.collection.GetEnumerator();
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000707C File Offset: 0x0000527C
		public bool Remove(string name)
		{
			return this.collection.Remove(this[name]);
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00007090 File Offset: 0x00005290
		public void Clear()
		{
			this.collection.Clear();
		}

		// Token: 0x0400009F RID: 159
		private List<ConfigurationProperty> collection;
	}
}
