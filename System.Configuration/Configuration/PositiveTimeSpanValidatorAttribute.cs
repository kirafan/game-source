﻿using System;

namespace System.Configuration
{
	// Token: 0x0200005D RID: 93
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class PositiveTimeSpanValidatorAttribute : ConfigurationValidatorAttribute
	{
		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000341 RID: 833 RVA: 0x000094BC File Offset: 0x000076BC
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				if (this.instance == null)
				{
					this.instance = new PositiveTimeSpanValidator();
				}
				return this.instance;
			}
		}

		// Token: 0x040000FF RID: 255
		private ConfigurationValidatorBase instance;
	}
}
