﻿using System;
using System.ComponentModel;

namespace System.Configuration
{
	// Token: 0x02000020 RID: 32
	public abstract class ConfigurationConverterBase : TypeConverter
	{
		// Token: 0x06000118 RID: 280 RVA: 0x00003CA0 File Offset: 0x00001EA0
		public override bool CanConvertFrom(ITypeDescriptorContext ctx, Type type)
		{
			return type == typeof(string) || base.CanConvertFrom(ctx, type);
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00003CBC File Offset: 0x00001EBC
		public override bool CanConvertTo(ITypeDescriptorContext ctx, Type type)
		{
			return type == typeof(string) || base.CanConvertTo(ctx, type);
		}
	}
}
