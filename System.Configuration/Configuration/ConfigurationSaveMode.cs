﻿using System;

namespace System.Configuration
{
	// Token: 0x02000034 RID: 52
	public enum ConfigurationSaveMode
	{
		// Token: 0x040000A6 RID: 166
		Minimal = 1,
		// Token: 0x040000A7 RID: 167
		Full,
		// Token: 0x040000A8 RID: 168
		Modified = 0
	}
}
