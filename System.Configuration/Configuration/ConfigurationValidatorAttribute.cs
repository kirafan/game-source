﻿using System;

namespace System.Configuration
{
	// Token: 0x0200003B RID: 59
	[AttributeUsage(AttributeTargets.Property)]
	public class ConfigurationValidatorAttribute : Attribute
	{
		// Token: 0x0600024D RID: 589 RVA: 0x00007C20 File Offset: 0x00005E20
		protected ConfigurationValidatorAttribute()
		{
		}

		// Token: 0x0600024E RID: 590 RVA: 0x00007C28 File Offset: 0x00005E28
		public ConfigurationValidatorAttribute(Type validator)
		{
			this.validatorType = validator;
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x0600024F RID: 591 RVA: 0x00007C38 File Offset: 0x00005E38
		public virtual ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				if (this.instance == null)
				{
					this.instance = (ConfigurationValidatorBase)Activator.CreateInstance(this.validatorType);
				}
				return this.instance;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000250 RID: 592 RVA: 0x00007C64 File Offset: 0x00005E64
		public Type ValidatorType
		{
			get
			{
				return this.validatorType;
			}
		}

		// Token: 0x040000C3 RID: 195
		private Type validatorType;

		// Token: 0x040000C4 RID: 196
		private ConfigurationValidatorBase instance;
	}
}
