﻿using System;

namespace System.Configuration
{
	// Token: 0x02000026 RID: 38
	public sealed class ConfigurationElementProperty
	{
		// Token: 0x06000187 RID: 391 RVA: 0x00005C84 File Offset: 0x00003E84
		public ConfigurationElementProperty(ConfigurationValidatorBase validator)
		{
			this.validator = validator;
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000188 RID: 392 RVA: 0x00005C94 File Offset: 0x00003E94
		public ConfigurationValidatorBase Validator
		{
			get
			{
				return this.validator;
			}
		}

		// Token: 0x04000079 RID: 121
		private ConfigurationValidatorBase validator;
	}
}
