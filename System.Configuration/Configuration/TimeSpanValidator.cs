﻿using System;

namespace System.Configuration
{
	// Token: 0x0200007E RID: 126
	public class TimeSpanValidator : ConfigurationValidatorBase
	{
		// Token: 0x06000425 RID: 1061 RVA: 0x0000BBD8 File Offset: 0x00009DD8
		public TimeSpanValidator(TimeSpan minValue, TimeSpan maxValue) : this(minValue, maxValue, false, 0L)
		{
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0000BBE8 File Offset: 0x00009DE8
		public TimeSpanValidator(TimeSpan minValue, TimeSpan maxValue, bool rangeIsExclusive) : this(minValue, maxValue, rangeIsExclusive, 0L)
		{
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0000BBF8 File Offset: 0x00009DF8
		public TimeSpanValidator(TimeSpan minValue, TimeSpan maxValue, bool rangeIsExclusive, long resolutionInSeconds)
		{
			this.minValue = minValue;
			this.maxValue = maxValue;
			this.rangeIsExclusive = rangeIsExclusive;
			this.resolutionInSeconds = resolutionInSeconds;
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x0000BC20 File Offset: 0x00009E20
		public override bool CanValidate(Type type)
		{
			return type == typeof(TimeSpan);
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0000BC30 File Offset: 0x00009E30
		public override void Validate(object value)
		{
			TimeSpan t = (TimeSpan)value;
			if (!this.rangeIsExclusive)
			{
				if (t < this.minValue || t > this.maxValue)
				{
					throw new ArgumentException(string.Concat(new object[]
					{
						"The value must be in the range ",
						this.minValue,
						" - ",
						this.maxValue
					}));
				}
			}
			else if (t >= this.minValue && t <= this.maxValue)
			{
				throw new ArgumentException(string.Concat(new object[]
				{
					"The value must not be in the range ",
					this.minValue,
					" - ",
					this.maxValue
				}));
			}
			if (this.resolutionInSeconds != 0L && t.Ticks % (10000000L * this.resolutionInSeconds) != 0L)
			{
				throw new ArgumentException("The value must have a resolution of " + TimeSpan.FromTicks(10000000L * this.resolutionInSeconds));
			}
		}

		// Token: 0x04000149 RID: 329
		private bool rangeIsExclusive;

		// Token: 0x0400014A RID: 330
		private TimeSpan minValue;

		// Token: 0x0400014B RID: 331
		private TimeSpan maxValue;

		// Token: 0x0400014C RID: 332
		private long resolutionInSeconds;
	}
}
