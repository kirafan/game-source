﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x0200007D RID: 125
	public sealed class TimeSpanSecondsOrInfiniteConverter : TimeSpanSecondsConverter
	{
		// Token: 0x06000423 RID: 1059 RVA: 0x0000BB5C File Offset: 0x00009D5C
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			if ((string)data == "Infinite")
			{
				return TimeSpan.MaxValue;
			}
			return base.ConvertFrom(ctx, ci, data);
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x0000BB88 File Offset: 0x00009D88
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value.GetType() != typeof(TimeSpan))
			{
				throw new ArgumentException();
			}
			if ((TimeSpan)value == TimeSpan.MaxValue)
			{
				return "Infinite";
			}
			return base.ConvertTo(ctx, ci, value, type);
		}
	}
}
