﻿using System;

namespace System.Configuration
{
	// Token: 0x02000041 RID: 65
	public sealed class ContextInformation
	{
		// Token: 0x06000276 RID: 630 RVA: 0x00008040 File Offset: 0x00006240
		internal ContextInformation(Configuration config, object ctx)
		{
			this.ctx = ctx;
			this.config = config;
		}

		// Token: 0x06000277 RID: 631 RVA: 0x00008058 File Offset: 0x00006258
		public object GetSection(string sectionName)
		{
			return this.config.GetSection(sectionName);
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000278 RID: 632 RVA: 0x00008068 File Offset: 0x00006268
		public object HostingContext
		{
			get
			{
				return this.ctx;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000279 RID: 633 RVA: 0x00008070 File Offset: 0x00006270
		[MonoInternalNote("should this use HostingContext instead?")]
		public bool IsMachineLevel
		{
			get
			{
				return this.config.ConfigPath == "machine";
			}
		}

		// Token: 0x040000CC RID: 204
		private object ctx;

		// Token: 0x040000CD RID: 205
		private Configuration config;
	}
}
