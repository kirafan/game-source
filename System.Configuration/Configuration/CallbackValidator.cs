﻿using System;

namespace System.Configuration
{
	// Token: 0x02000015 RID: 21
	public sealed class CallbackValidator : ConfigurationValidatorBase
	{
		// Token: 0x060000AC RID: 172 RVA: 0x000029D4 File Offset: 0x00000BD4
		public CallbackValidator(Type type, ValidatorCallback callback)
		{
			this.type = type;
			this.callback = callback;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x000029EC File Offset: 0x00000BEC
		public override bool CanValidate(Type type)
		{
			return type == this.type;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x000029F8 File Offset: 0x00000BF8
		public override void Validate(object value)
		{
			this.callback(value);
		}

		// Token: 0x04000029 RID: 41
		private Type type;

		// Token: 0x0400002A RID: 42
		private ValidatorCallback callback;
	}
}
