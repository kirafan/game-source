﻿using System;
using System.Configuration.Provider;

namespace System.Configuration
{
	// Token: 0x02000064 RID: 100
	public class ProtectedConfigurationProviderCollection : ProviderCollection
	{
		// Token: 0x170000FF RID: 255
		[MonoTODO]
		public ProtectedConfigurationProvider this[string name]
		{
			get
			{
				return (ProtectedConfigurationProvider)base[name];
			}
		}

		// Token: 0x0600036E RID: 878 RVA: 0x00009894 File Offset: 0x00007A94
		[MonoTODO]
		public override void Add(ProviderBase provider)
		{
			base.Add(provider);
		}
	}
}
