﻿using System;

namespace System.Configuration
{
	// Token: 0x02000055 RID: 85
	[ConfigurationCollection(typeof(KeyValueConfigurationElement), CollectionType = ConfigurationElementCollectionType.AddRemoveClearMap)]
	public class KeyValueConfigurationCollection : ConfigurationElementCollection
	{
		// Token: 0x06000309 RID: 777 RVA: 0x00008F04 File Offset: 0x00007104
		public void Add(KeyValueConfigurationElement keyValue)
		{
			keyValue.Init();
			this.BaseAdd(keyValue);
		}

		// Token: 0x0600030A RID: 778 RVA: 0x00008F14 File Offset: 0x00007114
		public void Add(string key, string value)
		{
			this.Add(new KeyValueConfigurationElement(key, value));
		}

		// Token: 0x0600030B RID: 779 RVA: 0x00008F24 File Offset: 0x00007124
		public void Clear()
		{
			base.BaseClear();
		}

		// Token: 0x0600030C RID: 780 RVA: 0x00008F2C File Offset: 0x0000712C
		public void Remove(string key)
		{
			base.BaseRemove(key);
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x0600030D RID: 781 RVA: 0x00008F38 File Offset: 0x00007138
		public string[] AllKeys
		{
			get
			{
				string[] array = new string[this.Count];
				int num = 0;
				foreach (object obj in this)
				{
					KeyValueConfigurationElement keyValueConfigurationElement = (KeyValueConfigurationElement)obj;
					array[num++] = keyValueConfigurationElement.Key;
				}
				return array;
			}
		}

		// Token: 0x170000D9 RID: 217
		public KeyValueConfigurationElement this[string key]
		{
			get
			{
				return (KeyValueConfigurationElement)base.BaseGet(key);
			}
		}

		// Token: 0x0600030F RID: 783 RVA: 0x00008FCC File Offset: 0x000071CC
		protected override ConfigurationElement CreateNewElement()
		{
			return new KeyValueConfigurationElement();
		}

		// Token: 0x06000310 RID: 784 RVA: 0x00008FD4 File Offset: 0x000071D4
		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((KeyValueConfigurationElement)element).Key;
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000311 RID: 785 RVA: 0x00008FE4 File Offset: 0x000071E4
		protected internal override ConfigurationPropertyCollection Properties
		{
			get
			{
				if (this.properties == null)
				{
					this.properties = new ConfigurationPropertyCollection();
				}
				return this.properties;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000312 RID: 786 RVA: 0x00009004 File Offset: 0x00007204
		protected override bool ThrowOnDuplicate
		{
			get
			{
				return false;
			}
		}

		// Token: 0x040000EF RID: 239
		private ConfigurationPropertyCollection properties;
	}
}
