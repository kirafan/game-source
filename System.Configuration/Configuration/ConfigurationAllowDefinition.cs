﻿using System;

namespace System.Configuration
{
	// Token: 0x0200001D RID: 29
	public enum ConfigurationAllowDefinition
	{
		// Token: 0x04000048 RID: 72
		MachineOnly,
		// Token: 0x04000049 RID: 73
		MachineToWebRoot = 100,
		// Token: 0x0400004A RID: 74
		MachineToApplication = 200,
		// Token: 0x0400004B RID: 75
		Everywhere = 300
	}
}
