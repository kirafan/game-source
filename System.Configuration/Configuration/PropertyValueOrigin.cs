﻿using System;

namespace System.Configuration
{
	// Token: 0x02000061 RID: 97
	public enum PropertyValueOrigin
	{
		// Token: 0x0400010B RID: 267
		Default,
		// Token: 0x0400010C RID: 268
		Inherited,
		// Token: 0x0400010D RID: 269
		SetHere
	}
}
