﻿using System;

namespace System.Configuration
{
	// Token: 0x0200006A RID: 106
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class RegexStringValidatorAttribute : ConfigurationValidatorAttribute
	{
		// Token: 0x06000398 RID: 920 RVA: 0x00009D8C File Offset: 0x00007F8C
		public RegexStringValidatorAttribute(string regex)
		{
			this.regex = regex;
		}

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000399 RID: 921 RVA: 0x00009D9C File Offset: 0x00007F9C
		public string Regex
		{
			get
			{
				return this.regex;
			}
		}

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x0600039A RID: 922 RVA: 0x00009DA4 File Offset: 0x00007FA4
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				if (this.instance == null)
				{
					this.instance = new RegexStringValidator(this.regex);
				}
				return this.instance;
			}
		}

		// Token: 0x0400011D RID: 285
		private string regex;

		// Token: 0x0400011E RID: 286
		private ConfigurationValidatorBase instance;
	}
}
