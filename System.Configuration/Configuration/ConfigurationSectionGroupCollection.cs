﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;

namespace System.Configuration
{
	// Token: 0x02000039 RID: 57
	[Serializable]
	public sealed class ConfigurationSectionGroupCollection : NameObjectCollectionBase
	{
		// Token: 0x0600023E RID: 574 RVA: 0x000079E0 File Offset: 0x00005BE0
		internal ConfigurationSectionGroupCollection(Configuration config, SectionGroupInfo group) : base(StringComparer.Ordinal)
		{
			this.config = config;
			this.group = group;
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600023F RID: 575 RVA: 0x000079FC File Offset: 0x00005BFC
		public override NameObjectCollectionBase.KeysCollection Keys
		{
			get
			{
				return this.group.Groups.Keys;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000240 RID: 576 RVA: 0x00007A10 File Offset: 0x00005C10
		public override int Count
		{
			get
			{
				return this.group.Groups.Count;
			}
		}

		// Token: 0x170000A9 RID: 169
		public ConfigurationSectionGroup this[string name]
		{
			get
			{
				ConfigurationSectionGroup configurationSectionGroup = base.BaseGet(name) as ConfigurationSectionGroup;
				if (configurationSectionGroup == null)
				{
					SectionGroupInfo sectionGroupInfo = this.group.Groups[name] as SectionGroupInfo;
					if (sectionGroupInfo == null)
					{
						return null;
					}
					configurationSectionGroup = this.config.GetSectionGroupInstance(sectionGroupInfo);
					base.BaseSet(name, configurationSectionGroup);
				}
				return configurationSectionGroup;
			}
		}

		// Token: 0x170000AA RID: 170
		public ConfigurationSectionGroup this[int index]
		{
			get
			{
				return this[this.GetKey(index)];
			}
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00007A8C File Offset: 0x00005C8C
		public void Add(string name, ConfigurationSectionGroup sectionGroup)
		{
			this.config.CreateSectionGroup(this.group, name, sectionGroup);
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00007AA4 File Offset: 0x00005CA4
		public void Clear()
		{
			if (this.group.Groups != null)
			{
				foreach (object obj in this.group.Groups)
				{
					ConfigInfo configInfo = (ConfigInfo)obj;
					this.config.RemoveConfigInfo(configInfo);
				}
			}
		}

		// Token: 0x06000245 RID: 581 RVA: 0x00007B30 File Offset: 0x00005D30
		public void CopyTo(ConfigurationSectionGroup[] array, int index)
		{
			for (int i = 0; i < this.group.Groups.Count; i++)
			{
				array[i + index] = this[i];
			}
		}

		// Token: 0x06000246 RID: 582 RVA: 0x00007B6C File Offset: 0x00005D6C
		public ConfigurationSectionGroup Get(int index)
		{
			return this[index];
		}

		// Token: 0x06000247 RID: 583 RVA: 0x00007B78 File Offset: 0x00005D78
		public ConfigurationSectionGroup Get(string name)
		{
			return this[name];
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00007B84 File Offset: 0x00005D84
		public override IEnumerator GetEnumerator()
		{
			return this.group.Groups.AllKeys.GetEnumerator();
		}

		// Token: 0x06000249 RID: 585 RVA: 0x00007B9C File Offset: 0x00005D9C
		public string GetKey(int index)
		{
			return this.group.Groups.GetKey(index);
		}

		// Token: 0x0600024A RID: 586 RVA: 0x00007BB0 File Offset: 0x00005DB0
		public void Remove(string name)
		{
			SectionGroupInfo sectionGroupInfo = this.group.Groups[name] as SectionGroupInfo;
			if (sectionGroupInfo != null)
			{
				this.config.RemoveConfigInfo(sectionGroupInfo);
			}
		}

		// Token: 0x0600024B RID: 587 RVA: 0x00007BE8 File Offset: 0x00005DE8
		public void RemoveAt(int index)
		{
			SectionGroupInfo sectionGroupInfo = this.group.Groups[index] as SectionGroupInfo;
			this.config.RemoveConfigInfo(sectionGroupInfo);
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00007C18 File Offset: 0x00005E18
		[MonoTODO]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040000BD RID: 189
		private SectionGroupInfo group;

		// Token: 0x040000BE RID: 190
		private Configuration config;
	}
}
