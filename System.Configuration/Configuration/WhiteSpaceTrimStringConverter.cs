﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x02000081 RID: 129
	public sealed class WhiteSpaceTrimStringConverter : ConfigurationConverterBase
	{
		// Token: 0x06000438 RID: 1080 RVA: 0x0000BE70 File Offset: 0x0000A070
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			return ((string)data).Trim();
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x0000BE80 File Offset: 0x0000A080
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value == null)
			{
				return string.Empty;
			}
			if (!(value is string))
			{
				throw new ArgumentException("value");
			}
			return ((string)value).Trim();
		}
	}
}
