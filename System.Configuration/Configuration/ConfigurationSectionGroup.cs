﻿using System;

namespace System.Configuration
{
	// Token: 0x02000038 RID: 56
	public class ConfigurationSectionGroup
	{
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000231 RID: 561 RVA: 0x000078AC File Offset: 0x00005AAC
		private Configuration Config
		{
			get
			{
				if (this.config == null)
				{
					throw new InvalidOperationException("ConfigurationSectionGroup cannot be edited until it is added to a Configuration instance as its descendant");
				}
				return this.config;
			}
		}

		// Token: 0x06000232 RID: 562 RVA: 0x000078CC File Offset: 0x00005ACC
		internal void Initialize(Configuration config, SectionGroupInfo group)
		{
			if (this.initialized)
			{
				throw new SystemException("INTERNAL ERROR: this configuration section is being initialized twice: " + base.GetType());
			}
			this.initialized = true;
			this.config = config;
			this.group = group;
		}

		// Token: 0x06000233 RID: 563 RVA: 0x00007910 File Offset: 0x00005B10
		internal void SetName(string name)
		{
			this.name = name;
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000791C File Offset: 0x00005B1C
		[MonoTODO]
		public void ForceDeclaration(bool require)
		{
			this.require_declaration = require;
		}

		// Token: 0x06000235 RID: 565 RVA: 0x00007928 File Offset: 0x00005B28
		public void ForceDeclaration()
		{
			this.ForceDeclaration(true);
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00007934 File Offset: 0x00005B34
		public bool IsDeclared
		{
			get
			{
				return this.declared;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000237 RID: 567 RVA: 0x0000793C File Offset: 0x00005B3C
		[MonoTODO]
		public bool IsDeclarationRequired
		{
			get
			{
				return this.require_declaration;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000238 RID: 568 RVA: 0x00007944 File Offset: 0x00005B44
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000239 RID: 569 RVA: 0x0000794C File Offset: 0x00005B4C
		[MonoInternalNote("Check if this is correct")]
		public string SectionGroupName
		{
			get
			{
				return this.group.XPath;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600023A RID: 570 RVA: 0x0000795C File Offset: 0x00005B5C
		public ConfigurationSectionGroupCollection SectionGroups
		{
			get
			{
				if (this.groups == null)
				{
					this.groups = new ConfigurationSectionGroupCollection(this.Config, this.group);
				}
				return this.groups;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600023B RID: 571 RVA: 0x00007994 File Offset: 0x00005B94
		public ConfigurationSectionCollection Sections
		{
			get
			{
				if (this.sections == null)
				{
					this.sections = new ConfigurationSectionCollection(this.Config, this.group);
				}
				return this.sections;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600023C RID: 572 RVA: 0x000079CC File Offset: 0x00005BCC
		// (set) Token: 0x0600023D RID: 573 RVA: 0x000079D4 File Offset: 0x00005BD4
		public string Type
		{
			get
			{
				return this.type_name;
			}
			set
			{
				this.type_name = value;
			}
		}

		// Token: 0x040000B4 RID: 180
		private bool require_declaration;

		// Token: 0x040000B5 RID: 181
		private bool declared;

		// Token: 0x040000B6 RID: 182
		private string name;

		// Token: 0x040000B7 RID: 183
		private string type_name;

		// Token: 0x040000B8 RID: 184
		private ConfigurationSectionCollection sections;

		// Token: 0x040000B9 RID: 185
		private ConfigurationSectionGroupCollection groups;

		// Token: 0x040000BA RID: 186
		private Configuration config;

		// Token: 0x040000BB RID: 187
		private SectionGroupInfo group;

		// Token: 0x040000BC RID: 188
		private bool initialized;
	}
}
