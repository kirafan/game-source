﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace System.Configuration
{
	// Token: 0x0200007A RID: 122
	public class TimeSpanMinutesConverter : ConfigurationConverterBase
	{
		// Token: 0x0600041A RID: 1050 RVA: 0x0000B9E4 File Offset: 0x00009BE4
		public override object ConvertFrom(ITypeDescriptorContext ctx, CultureInfo ci, object data)
		{
			return TimeSpan.FromMinutes((double)long.Parse((string)data));
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x0000B9FC File Offset: 0x00009BFC
		public override object ConvertTo(ITypeDescriptorContext ctx, CultureInfo ci, object value, Type type)
		{
			if (value.GetType() != typeof(TimeSpan))
			{
				throw new ArgumentException();
			}
			return ((long)((TimeSpan)value).TotalMinutes).ToString();
		}
	}
}
