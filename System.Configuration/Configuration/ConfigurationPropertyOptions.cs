﻿using System;

namespace System.Configuration
{
	// Token: 0x02000033 RID: 51
	[Flags]
	public enum ConfigurationPropertyOptions
	{
		// Token: 0x040000A1 RID: 161
		None = 0,
		// Token: 0x040000A2 RID: 162
		IsDefaultCollection = 1,
		// Token: 0x040000A3 RID: 163
		IsRequired = 2,
		// Token: 0x040000A4 RID: 164
		IsKey = 4
	}
}
