﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x0200000F RID: 15
	[ComVisible(false)]
	public interface IInternalConfigSystem
	{
		// Token: 0x06000087 RID: 135
		object GetSection(string configKey);

		// Token: 0x06000088 RID: 136
		void RefreshConfig(string sectionName);

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000089 RID: 137
		bool SupportsUserConfig { get; }
	}
}
