﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x0200000A RID: 10
	[ComVisible(false)]
	public interface IInternalConfigConfigurationFactory
	{
		// Token: 0x06000047 RID: 71
		Configuration Create(Type typeConfigHost, params object[] hostInitConfigurationParams);

		// Token: 0x06000048 RID: 72
		string NormalizeLocationSubPath(string subPath, IConfigErrorInfo errorInfo);
	}
}
