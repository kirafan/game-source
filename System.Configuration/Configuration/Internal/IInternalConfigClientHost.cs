﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x02000009 RID: 9
	[ComVisible(false)]
	public interface IInternalConfigClientHost
	{
		// Token: 0x06000041 RID: 65
		string GetExeConfigPath();

		// Token: 0x06000042 RID: 66
		string GetLocalUserConfigPath();

		// Token: 0x06000043 RID: 67
		string GetRoamingUserConfigPath();

		// Token: 0x06000044 RID: 68
		bool IsExeConfig(string configPath);

		// Token: 0x06000045 RID: 69
		bool IsLocalUserConfig(string configPath);

		// Token: 0x06000046 RID: 70
		bool IsRoamingUserConfig(string configPath);
	}
}
