﻿using System;

namespace System.Configuration.Internal
{
	// Token: 0x02000006 RID: 6
	public interface IConfigSystem
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000032 RID: 50
		IInternalConfigHost Host { get; }

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000033 RID: 51
		IInternalConfigRoot Root { get; }

		// Token: 0x06000034 RID: 52
		void Init(Type typeConfigHost, params object[] hostInitParams);
	}
}
