﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x0200000C RID: 12
	[ComVisible(false)]
	public interface IInternalConfigRecord
	{
		// Token: 0x06000072 RID: 114
		object GetLkgSection(string configKey);

		// Token: 0x06000073 RID: 115
		object GetSection(string configKey);

		// Token: 0x06000074 RID: 116
		void RefreshSection(string configKey);

		// Token: 0x06000075 RID: 117
		void Remove();

		// Token: 0x06000076 RID: 118
		void ThrowIfInitErrors();

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000077 RID: 119
		string ConfigPath { get; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000078 RID: 120
		bool HasInitErrors { get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000079 RID: 121
		string StreamName { get; }
	}
}
