﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;

namespace System.Configuration.Internal
{
	// Token: 0x0200000B RID: 11
	[ComVisible(false)]
	public interface IInternalConfigHost
	{
		// Token: 0x06000049 RID: 73
		object CreateConfigurationContext(string configPath, string locationSubPath);

		// Token: 0x0600004A RID: 74
		object CreateDeprecatedConfigContext(string configPath);

		// Token: 0x0600004B RID: 75
		string DecryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedSection);

		// Token: 0x0600004C RID: 76
		void DeleteStream(string streamName);

		// Token: 0x0600004D RID: 77
		string EncryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedSection);

		// Token: 0x0600004E RID: 78
		string GetConfigPathFromLocationSubPath(string configPath, string locatinSubPath);

		// Token: 0x0600004F RID: 79
		Type GetConfigType(string typeName, bool throwOnError);

		// Token: 0x06000050 RID: 80
		string GetConfigTypeName(Type t);

		// Token: 0x06000051 RID: 81
		void GetRestrictedPermissions(IInternalConfigRecord configRecord, out PermissionSet permissionSet, out bool isHostReady);

		// Token: 0x06000052 RID: 82
		string GetStreamName(string configPath);

		// Token: 0x06000053 RID: 83
		string GetStreamNameForConfigSource(string streamName, string configSource);

		// Token: 0x06000054 RID: 84
		object GetStreamVersion(string streamName);

		// Token: 0x06000055 RID: 85
		IDisposable Impersonate();

		// Token: 0x06000056 RID: 86
		void Init(IInternalConfigRoot root, params object[] hostInitParams);

		// Token: 0x06000057 RID: 87
		void InitForConfiguration(ref string locationSubPath, out string configPath, out string locationConfigPath, IInternalConfigRoot root, params object[] hostInitConfigurationParams);

		// Token: 0x06000058 RID: 88
		bool IsAboveApplication(string configPath);

		// Token: 0x06000059 RID: 89
		bool IsConfigRecordRequired(string configPath);

		// Token: 0x0600005A RID: 90
		bool IsDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition);

		// Token: 0x0600005B RID: 91
		bool IsFile(string streamName);

		// Token: 0x0600005C RID: 92
		bool IsFullTrustSectionWithoutAptcaAllowed(IInternalConfigRecord configRecord);

		// Token: 0x0600005D RID: 93
		bool IsInitDelayed(IInternalConfigRecord configRecord);

		// Token: 0x0600005E RID: 94
		bool IsLocationApplicable(string configPath);

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x0600005F RID: 95
		bool IsRemote { get; }

		// Token: 0x06000060 RID: 96
		bool IsSecondaryRoot(string configPath);

		// Token: 0x06000061 RID: 97
		bool IsTrustedConfigPath(string configPath);

		// Token: 0x06000062 RID: 98
		Stream OpenStreamForRead(string streamName);

		// Token: 0x06000063 RID: 99
		Stream OpenStreamForRead(string streamName, bool assertPermissions);

		// Token: 0x06000064 RID: 100
		Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext);

		// Token: 0x06000065 RID: 101
		Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext, bool assertPermissions);

		// Token: 0x06000066 RID: 102
		bool PrefetchAll(string configPath, string streamName);

		// Token: 0x06000067 RID: 103
		bool PrefetchSection(string sectionGroupName, string sectionName);

		// Token: 0x06000068 RID: 104
		void RequireCompleteInit(IInternalConfigRecord configRecord);

		// Token: 0x06000069 RID: 105
		object StartMonitoringStreamForChanges(string streamName, StreamChangeCallback callback);

		// Token: 0x0600006A RID: 106
		void StopMonitoringStreamForChanges(string streamName, StreamChangeCallback callback);

		// Token: 0x0600006B RID: 107
		void VerifyDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition, IConfigErrorInfo errorInfo);

		// Token: 0x0600006C RID: 108
		void WriteCompleted(string streamName, bool success, object writeContext);

		// Token: 0x0600006D RID: 109
		void WriteCompleted(string streamName, bool success, object writeContext, bool assertPermissions);

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x0600006E RID: 110
		bool SupportsChangeNotifications { get; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600006F RID: 111
		bool SupportsLocation { get; }

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000070 RID: 112
		bool SupportsPath { get; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000071 RID: 113
		bool SupportsRefresh { get; }
	}
}
