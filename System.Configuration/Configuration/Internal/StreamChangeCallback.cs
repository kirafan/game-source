﻿using System;

namespace System.Configuration.Internal
{
	// Token: 0x02000083 RID: 131
	// (Invoke) Token: 0x0600043F RID: 1087
	public delegate void StreamChangeCallback(string streamName);
}
