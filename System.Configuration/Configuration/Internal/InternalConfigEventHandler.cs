﻿using System;

namespace System.Configuration.Internal
{
	// Token: 0x02000082 RID: 130
	// (Invoke) Token: 0x0600043B RID: 1083
	public delegate void InternalConfigEventHandler(object sender, InternalConfigEventArgs e);
}
