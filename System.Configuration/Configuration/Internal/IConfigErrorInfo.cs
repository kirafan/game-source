﻿using System;

namespace System.Configuration.Internal
{
	// Token: 0x02000005 RID: 5
	public interface IConfigErrorInfo
	{
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000030 RID: 48
		string Filename { get; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000031 RID: 49
		int LineNumber { get; }
	}
}
