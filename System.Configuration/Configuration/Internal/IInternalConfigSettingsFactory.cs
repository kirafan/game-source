﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x0200000E RID: 14
	[ComVisible(false)]
	public interface IInternalConfigSettingsFactory
	{
		// Token: 0x06000085 RID: 133
		void CompleteInit();

		// Token: 0x06000086 RID: 134
		void SetConfigurationSystem(IInternalConfigSystem internalConfigSystem, bool initComplete);
	}
}
