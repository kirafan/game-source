﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x02000008 RID: 8
	[ComVisible(false)]
	public interface IConfigurationManagerInternal
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000036 RID: 54
		string ApplicationConfigUri { get; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000037 RID: 55
		string ExeLocalConfigDirectory { get; }

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x06000038 RID: 56
		string ExeLocalConfigPath { get; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000039 RID: 57
		string ExeProductName { get; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600003A RID: 58
		string ExeProductVersion { get; }

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x0600003B RID: 59
		string ExeRoamingConfigDirectory { get; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600003C RID: 60
		string ExeRoamingConfigPath { get; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600003D RID: 61
		string MachineConfigPath { get; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600003E RID: 62
		bool SetConfigurationSystemInProgress { get; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x0600003F RID: 63
		bool SupportsUserConfig { get; }

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000040 RID: 64
		string UserConfigFilename { get; }
	}
}
