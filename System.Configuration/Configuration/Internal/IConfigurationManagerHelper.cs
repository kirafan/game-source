﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x02000007 RID: 7
	[ComVisible(false)]
	public interface IConfigurationManagerHelper
	{
		// Token: 0x06000035 RID: 53
		void EnsureNetConfigLoaded();
	}
}
