﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Internal
{
	// Token: 0x0200000D RID: 13
	[ComVisible(false)]
	public interface IInternalConfigRoot
	{
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x0600007A RID: 122
		// (remove) Token: 0x0600007B RID: 123
		event InternalConfigEventHandler ConfigChanged;

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x0600007C RID: 124
		// (remove) Token: 0x0600007D RID: 125
		event InternalConfigEventHandler ConfigRemoved;

		// Token: 0x0600007E RID: 126
		IInternalConfigRecord GetConfigRecord(string configPath);

		// Token: 0x0600007F RID: 127
		object GetSection(string section, string configPath);

		// Token: 0x06000080 RID: 128
		string GetUniqueConfigPath(string configPath);

		// Token: 0x06000081 RID: 129
		IInternalConfigRecord GetUniqueConfigRecord(string configPath);

		// Token: 0x06000082 RID: 130
		void Init(IInternalConfigHost host, bool isDesignTime);

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000083 RID: 131
		bool IsDesignTime { get; }

		// Token: 0x06000084 RID: 132
		void RemoveConfig(string configPath);
	}
}
