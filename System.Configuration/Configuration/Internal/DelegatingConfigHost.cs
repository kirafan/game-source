﻿using System;
using System.IO;
using System.Security;

namespace System.Configuration.Internal
{
	// Token: 0x02000004 RID: 4
	public class DelegatingConfigHost : IInternalConfigHost
	{
		// Token: 0x06000004 RID: 4 RVA: 0x00002104 File Offset: 0x00000304
		protected DelegatingConfigHost()
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000005 RID: 5 RVA: 0x0000210C File Offset: 0x0000030C
		// (set) Token: 0x06000006 RID: 6 RVA: 0x00002114 File Offset: 0x00000314
		protected IInternalConfigHost Host
		{
			get
			{
				return this.host;
			}
			set
			{
				this.host = value;
			}
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002120 File Offset: 0x00000320
		public virtual object CreateConfigurationContext(string configPath, string locationSubPath)
		{
			return this.host.CreateConfigurationContext(configPath, locationSubPath);
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002130 File Offset: 0x00000330
		public virtual object CreateDeprecatedConfigContext(string configPath)
		{
			return this.host.CreateDeprecatedConfigContext(configPath);
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002140 File Offset: 0x00000340
		public virtual string DecryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedSection)
		{
			return this.host.DecryptSection(encryptedXml, protectionProvider, protectedSection);
		}

		// Token: 0x0600000A RID: 10 RVA: 0x00002150 File Offset: 0x00000350
		public virtual void DeleteStream(string streamName)
		{
			this.host.DeleteStream(streamName);
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002160 File Offset: 0x00000360
		public virtual string EncryptSection(string encryptedXml, ProtectedConfigurationProvider protectionProvider, ProtectedConfigurationSection protectedSection)
		{
			return this.host.EncryptSection(encryptedXml, protectionProvider, protectedSection);
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002170 File Offset: 0x00000370
		public virtual string GetConfigPathFromLocationSubPath(string configPath, string locatinSubPath)
		{
			return this.host.GetConfigPathFromLocationSubPath(configPath, locatinSubPath);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x00002180 File Offset: 0x00000380
		public virtual Type GetConfigType(string typeName, bool throwOnError)
		{
			return this.host.GetConfigType(typeName, throwOnError);
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002190 File Offset: 0x00000390
		public virtual string GetConfigTypeName(Type t)
		{
			return this.host.GetConfigTypeName(t);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000021A0 File Offset: 0x000003A0
		public virtual void GetRestrictedPermissions(IInternalConfigRecord configRecord, out PermissionSet permissionSet, out bool isHostReady)
		{
			this.host.GetRestrictedPermissions(configRecord, out permissionSet, out isHostReady);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000021B0 File Offset: 0x000003B0
		public virtual string GetStreamName(string configPath)
		{
			return this.host.GetStreamName(configPath);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x000021C0 File Offset: 0x000003C0
		public virtual string GetStreamNameForConfigSource(string streamName, string configSource)
		{
			return this.host.GetStreamNameForConfigSource(streamName, configSource);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000021D0 File Offset: 0x000003D0
		public virtual object GetStreamVersion(string streamName)
		{
			return this.host.GetStreamVersion(streamName);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000021E0 File Offset: 0x000003E0
		public virtual IDisposable Impersonate()
		{
			return this.host.Impersonate();
		}

		// Token: 0x06000014 RID: 20 RVA: 0x000021F0 File Offset: 0x000003F0
		public virtual void Init(IInternalConfigRoot root, params object[] hostInitParams)
		{
			this.host.Init(root, hostInitParams);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002200 File Offset: 0x00000400
		public virtual void InitForConfiguration(ref string locationSubPath, out string configPath, out string locationConfigPath, IInternalConfigRoot root, params object[] hostInitConfigurationParams)
		{
			this.host.InitForConfiguration(ref locationSubPath, out configPath, out locationConfigPath, root, hostInitConfigurationParams);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002214 File Offset: 0x00000414
		public virtual bool IsAboveApplication(string configPath)
		{
			return this.host.IsAboveApplication(configPath);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002224 File Offset: 0x00000424
		public virtual bool IsConfigRecordRequired(string configPath)
		{
			return this.host.IsConfigRecordRequired(configPath);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002234 File Offset: 0x00000434
		public virtual bool IsDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition)
		{
			return this.host.IsDefinitionAllowed(configPath, allowDefinition, allowExeDefinition);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002244 File Offset: 0x00000444
		public virtual bool IsInitDelayed(IInternalConfigRecord configRecord)
		{
			return this.host.IsInitDelayed(configRecord);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002254 File Offset: 0x00000454
		public virtual bool IsFile(string streamName)
		{
			return this.host.IsFile(streamName);
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002264 File Offset: 0x00000464
		public virtual bool IsFullTrustSectionWithoutAptcaAllowed(IInternalConfigRecord configRecord)
		{
			return this.host.IsFullTrustSectionWithoutAptcaAllowed(configRecord);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002274 File Offset: 0x00000474
		public virtual bool IsLocationApplicable(string configPath)
		{
			return this.host.IsLocationApplicable(configPath);
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600001D RID: 29 RVA: 0x00002284 File Offset: 0x00000484
		public virtual bool IsRemote
		{
			get
			{
				return this.host.IsRemote;
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002294 File Offset: 0x00000494
		public virtual bool IsSecondaryRoot(string configPath)
		{
			return this.host.IsSecondaryRoot(configPath);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000022A4 File Offset: 0x000004A4
		public virtual bool IsTrustedConfigPath(string configPath)
		{
			return this.host.IsTrustedConfigPath(configPath);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000022B4 File Offset: 0x000004B4
		public virtual Stream OpenStreamForRead(string streamName)
		{
			return this.host.OpenStreamForRead(streamName);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000022C4 File Offset: 0x000004C4
		public virtual Stream OpenStreamForRead(string streamName, bool assertPermissions)
		{
			return this.host.OpenStreamForRead(streamName, assertPermissions);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000022D4 File Offset: 0x000004D4
		public virtual Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext)
		{
			return this.host.OpenStreamForWrite(streamName, templateStreamName, ref writeContext);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x000022E4 File Offset: 0x000004E4
		public virtual Stream OpenStreamForWrite(string streamName, string templateStreamName, ref object writeContext, bool assertPermissions)
		{
			return this.host.OpenStreamForWrite(streamName, templateStreamName, ref writeContext, assertPermissions);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000022F8 File Offset: 0x000004F8
		public virtual bool PrefetchAll(string configPath, string streamName)
		{
			return this.host.PrefetchAll(configPath, streamName);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002308 File Offset: 0x00000508
		public virtual bool PrefetchSection(string sectionGroupName, string sectionName)
		{
			return this.host.PrefetchSection(sectionGroupName, sectionName);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002318 File Offset: 0x00000518
		public virtual void RequireCompleteInit(IInternalConfigRecord configRecord)
		{
			this.host.RequireCompleteInit(configRecord);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002328 File Offset: 0x00000528
		public virtual object StartMonitoringStreamForChanges(string streamName, StreamChangeCallback callback)
		{
			return this.host.StartMonitoringStreamForChanges(streamName, callback);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002338 File Offset: 0x00000538
		public virtual void StopMonitoringStreamForChanges(string streamName, StreamChangeCallback callback)
		{
			this.host.StopMonitoringStreamForChanges(streamName, callback);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002348 File Offset: 0x00000548
		public virtual void VerifyDefinitionAllowed(string configPath, ConfigurationAllowDefinition allowDefinition, ConfigurationAllowExeDefinition allowExeDefinition, IConfigErrorInfo errorInfo)
		{
			this.host.VerifyDefinitionAllowed(configPath, allowDefinition, allowExeDefinition, errorInfo);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000235C File Offset: 0x0000055C
		public virtual void WriteCompleted(string streamName, bool success, object writeContext)
		{
			this.host.WriteCompleted(streamName, success, writeContext);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x0000236C File Offset: 0x0000056C
		public virtual void WriteCompleted(string streamName, bool success, object writeContext, bool assertPermissions)
		{
			this.host.WriteCompleted(streamName, success, writeContext, assertPermissions);
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600002C RID: 44 RVA: 0x00002380 File Offset: 0x00000580
		public virtual bool SupportsChangeNotifications
		{
			get
			{
				return this.host.SupportsChangeNotifications;
			}
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x0600002D RID: 45 RVA: 0x00002390 File Offset: 0x00000590
		public virtual bool SupportsLocation
		{
			get
			{
				return this.host.SupportsLocation;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600002E RID: 46 RVA: 0x000023A0 File Offset: 0x000005A0
		public virtual bool SupportsPath
		{
			get
			{
				return this.host.SupportsPath;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600002F RID: 47 RVA: 0x000023B0 File Offset: 0x000005B0
		public virtual bool SupportsRefresh
		{
			get
			{
				return this.host.SupportsRefresh;
			}
		}

		// Token: 0x0400001E RID: 30
		private IInternalConfigHost host;
	}
}
