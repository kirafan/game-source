﻿using System;

namespace System.Configuration
{
	// Token: 0x02000016 RID: 22
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class CallbackValidatorAttribute : ConfigurationValidatorAttribute
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x00002A1C File Offset: 0x00000C1C
		// (set) Token: 0x060000B1 RID: 177 RVA: 0x00002A24 File Offset: 0x00000C24
		public string CallbackMethodName
		{
			get
			{
				return this.callbackMethodName;
			}
			set
			{
				this.callbackMethodName = value;
				this.instance = null;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x00002A34 File Offset: 0x00000C34
		// (set) Token: 0x060000B3 RID: 179 RVA: 0x00002A3C File Offset: 0x00000C3C
		public Type Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
				this.instance = null;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000B4 RID: 180 RVA: 0x00002A4C File Offset: 0x00000C4C
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				return this.instance;
			}
		}

		// Token: 0x0400002B RID: 43
		private string callbackMethodName = string.Empty;

		// Token: 0x0400002C RID: 44
		private Type type;

		// Token: 0x0400002D RID: 45
		private ConfigurationValidatorBase instance;
	}
}
