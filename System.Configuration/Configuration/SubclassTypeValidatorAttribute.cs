﻿using System;

namespace System.Configuration
{
	// Token: 0x02000079 RID: 121
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class SubclassTypeValidatorAttribute : ConfigurationValidatorAttribute
	{
		// Token: 0x06000416 RID: 1046 RVA: 0x0000B994 File Offset: 0x00009B94
		public SubclassTypeValidatorAttribute(Type baseClass)
		{
			this.baseClass = baseClass;
		}

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000417 RID: 1047 RVA: 0x0000B9A4 File Offset: 0x00009BA4
		public Type BaseClass
		{
			get
			{
				return this.baseClass;
			}
		}

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000418 RID: 1048 RVA: 0x0000B9AC File Offset: 0x00009BAC
		public override ConfigurationValidatorBase ValidatorInstance
		{
			get
			{
				if (this.instance == null)
				{
					this.instance = new SubclassTypeValidator(this.baseClass);
				}
				return this.instance;
			}
		}

		// Token: 0x04000147 RID: 327
		private Type baseClass;

		// Token: 0x04000148 RID: 328
		private ConfigurationValidatorBase instance;
	}
}
