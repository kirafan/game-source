﻿using System;
using System.Collections.Specialized;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x02000044 RID: 68
	public sealed class DpapiProtectedConfigurationProvider : ProtectedConfigurationProvider
	{
		// Token: 0x06000286 RID: 646 RVA: 0x00008114 File Offset: 0x00006314
		[MonoNotSupported("DpapiProtectedConfigurationProvider depends on the Microsoft Data\nProtection API, and is unimplemented in Mono.  For portability's sake,\nit is suggested that you use the RsaProtectedConfigurationProvider.")]
		public override XmlNode Decrypt(XmlNode encrypted_node)
		{
			throw new NotSupportedException("DpapiProtectedConfigurationProvider depends on the Microsoft Data\nProtection API, and is unimplemented in Mono.  For portability's sake,\nit is suggested that you use the RsaProtectedConfigurationProvider.");
		}

		// Token: 0x06000287 RID: 647 RVA: 0x00008120 File Offset: 0x00006320
		[MonoNotSupported("DpapiProtectedConfigurationProvider depends on the Microsoft Data\nProtection API, and is unimplemented in Mono.  For portability's sake,\nit is suggested that you use the RsaProtectedConfigurationProvider.")]
		public override XmlNode Encrypt(XmlNode node)
		{
			throw new NotSupportedException("DpapiProtectedConfigurationProvider depends on the Microsoft Data\nProtection API, and is unimplemented in Mono.  For portability's sake,\nit is suggested that you use the RsaProtectedConfigurationProvider.");
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000812C File Offset: 0x0000632C
		[MonoTODO]
		public override void Initialize(string name, NameValueCollection configurationValues)
		{
			base.Initialize(name, configurationValues);
			string text = configurationValues["useMachineProtection"];
			if (text != null && text.ToLowerInvariant() == "true")
			{
				this.useMachineProtection = true;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000289 RID: 649 RVA: 0x00008170 File Offset: 0x00006370
		public bool UseMachineProtection
		{
			get
			{
				return this.useMachineProtection;
			}
		}

		// Token: 0x040000CF RID: 207
		private const string NotSupportedReason = "DpapiProtectedConfigurationProvider depends on the Microsoft Data\nProtection API, and is unimplemented in Mono.  For portability's sake,\nit is suggested that you use the RsaProtectedConfigurationProvider.";

		// Token: 0x040000D0 RID: 208
		private bool useMachineProtection;
	}
}
