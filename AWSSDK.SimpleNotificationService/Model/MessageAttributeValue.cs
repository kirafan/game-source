﻿using System;
using System.IO;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000035 RID: 53
	public class MessageAttributeValue
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x0600017A RID: 378 RVA: 0x00003BE0 File Offset: 0x00001DE0
		// (set) Token: 0x0600017B RID: 379 RVA: 0x00003BE8 File Offset: 0x00001DE8
		public MemoryStream BinaryValue
		{
			get
			{
				return this._binaryValue;
			}
			set
			{
				this._binaryValue = value;
			}
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00003BF1 File Offset: 0x00001DF1
		internal bool IsSetBinaryValue()
		{
			return this._binaryValue != null;
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00003BFC File Offset: 0x00001DFC
		// (set) Token: 0x0600017E RID: 382 RVA: 0x00003C04 File Offset: 0x00001E04
		public string DataType
		{
			get
			{
				return this._dataType;
			}
			set
			{
				this._dataType = value;
			}
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00003C0D File Offset: 0x00001E0D
		internal bool IsSetDataType()
		{
			return this._dataType != null;
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00003C18 File Offset: 0x00001E18
		// (set) Token: 0x06000181 RID: 385 RVA: 0x00003C20 File Offset: 0x00001E20
		public string StringValue
		{
			get
			{
				return this._stringValue;
			}
			set
			{
				this._stringValue = value;
			}
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00003C29 File Offset: 0x00001E29
		internal bool IsSetStringValue()
		{
			return this._stringValue != null;
		}

		// Token: 0x0400003B RID: 59
		private MemoryStream _binaryValue;

		// Token: 0x0400003C RID: 60
		private string _dataType;

		// Token: 0x0400003D RID: 61
		private string _stringValue;
	}
}
