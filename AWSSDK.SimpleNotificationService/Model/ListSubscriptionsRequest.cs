﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000031 RID: 49
	public class ListSubscriptionsRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x06000162 RID: 354 RVA: 0x00003388 File Offset: 0x00001588
		public ListSubscriptionsRequest()
		{
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00003AD6 File Offset: 0x00001CD6
		public ListSubscriptionsRequest(string nextToken)
		{
			this._nextToken = nextToken;
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000164 RID: 356 RVA: 0x00003AE5 File Offset: 0x00001CE5
		// (set) Token: 0x06000165 RID: 357 RVA: 0x00003AED File Offset: 0x00001CED
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00003AF6 File Offset: 0x00001CF6
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000035 RID: 53
		private string _nextToken;
	}
}
