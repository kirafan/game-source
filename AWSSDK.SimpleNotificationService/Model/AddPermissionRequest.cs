﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000007 RID: 7
	public class AddPermissionRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x06000080 RID: 128 RVA: 0x00003237 File Offset: 0x00001437
		public AddPermissionRequest()
		{
		}

		// Token: 0x06000081 RID: 129 RVA: 0x00003255 File Offset: 0x00001455
		public AddPermissionRequest(string topicArn, string label, List<string> awsAccountId, List<string> actionName)
		{
			this._topicArn = topicArn;
			this._label = label;
			this._awsAccountId = awsAccountId;
			this._actionName = actionName;
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000082 RID: 130 RVA: 0x00003290 File Offset: 0x00001490
		// (set) Token: 0x06000083 RID: 131 RVA: 0x00003298 File Offset: 0x00001498
		public List<string> ActionName
		{
			get
			{
				return this._actionName;
			}
			set
			{
				this._actionName = value;
			}
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000032A1 File Offset: 0x000014A1
		internal bool IsSetActionName()
		{
			return this._actionName != null && this._actionName.Count > 0;
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000085 RID: 133 RVA: 0x000032BB File Offset: 0x000014BB
		// (set) Token: 0x06000086 RID: 134 RVA: 0x000032C3 File Offset: 0x000014C3
		public List<string> AWSAccountId
		{
			get
			{
				return this._awsAccountId;
			}
			set
			{
				this._awsAccountId = value;
			}
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000032CC File Offset: 0x000014CC
		internal bool IsSetAWSAccountId()
		{
			return this._awsAccountId != null && this._awsAccountId.Count > 0;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000088 RID: 136 RVA: 0x000032E6 File Offset: 0x000014E6
		// (set) Token: 0x06000089 RID: 137 RVA: 0x000032EE File Offset: 0x000014EE
		public string Label
		{
			get
			{
				return this._label;
			}
			set
			{
				this._label = value;
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000032F7 File Offset: 0x000014F7
		internal bool IsSetLabel()
		{
			return this._label != null;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600008B RID: 139 RVA: 0x00003302 File Offset: 0x00001502
		// (set) Token: 0x0600008C RID: 140 RVA: 0x0000330A File Offset: 0x0000150A
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003313 File Offset: 0x00001513
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000003 RID: 3
		private List<string> _actionName = new List<string>();

		// Token: 0x04000004 RID: 4
		private List<string> _awsAccountId = new List<string>();

		// Token: 0x04000005 RID: 5
		private string _label;

		// Token: 0x04000006 RID: 6
		private string _topicArn;
	}
}
