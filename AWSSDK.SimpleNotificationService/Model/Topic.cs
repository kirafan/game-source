﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004E RID: 78
	public class Topic
	{
		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600021A RID: 538 RVA: 0x00004138 File Offset: 0x00002338
		// (set) Token: 0x0600021B RID: 539 RVA: 0x00004140 File Offset: 0x00002340
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x00004149 File Offset: 0x00002349
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x0400005F RID: 95
		private string _topicArn;
	}
}
