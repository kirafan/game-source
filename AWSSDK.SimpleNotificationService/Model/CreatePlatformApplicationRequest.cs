﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000E RID: 14
	public class CreatePlatformApplicationRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000AD RID: 173 RVA: 0x0000345B File Offset: 0x0000165B
		// (set) Token: 0x060000AE RID: 174 RVA: 0x00003463 File Offset: 0x00001663
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060000AF RID: 175 RVA: 0x0000346C File Offset: 0x0000166C
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000B0 RID: 176 RVA: 0x00003486 File Offset: 0x00001686
		// (set) Token: 0x060000B1 RID: 177 RVA: 0x0000348E File Offset: 0x0000168E
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		// Token: 0x060000B2 RID: 178 RVA: 0x00003497 File Offset: 0x00001697
		internal bool IsSetName()
		{
			return this._name != null;
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000B3 RID: 179 RVA: 0x000034A2 File Offset: 0x000016A2
		// (set) Token: 0x060000B4 RID: 180 RVA: 0x000034AA File Offset: 0x000016AA
		public string Platform
		{
			get
			{
				return this._platform;
			}
			set
			{
				this._platform = value;
			}
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x000034B3 File Offset: 0x000016B3
		internal bool IsSetPlatform()
		{
			return this._platform != null;
		}

		// Token: 0x0400000D RID: 13
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x0400000E RID: 14
		private string _name;

		// Token: 0x0400000F RID: 15
		private string _platform;
	}
}
