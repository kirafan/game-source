﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001F RID: 31
	public class GetPlatformApplicationAttributesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000FE RID: 254 RVA: 0x00003715 File Offset: 0x00001915
		// (set) Token: 0x060000FF RID: 255 RVA: 0x0000371D File Offset: 0x0000191D
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00003726 File Offset: 0x00001926
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x04000020 RID: 32
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
