﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001E RID: 30
	public class GetPlatformApplicationAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000FA RID: 250 RVA: 0x000036F9 File Offset: 0x000018F9
		// (set) Token: 0x060000FB RID: 251 RVA: 0x00003701 File Offset: 0x00001901
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x060000FC RID: 252 RVA: 0x0000370A File Offset: 0x0000190A
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x0400001F RID: 31
		private string _platformApplicationArn;
	}
}
