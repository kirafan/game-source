﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000012 RID: 18
	public class CreateTopicRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060000CC RID: 204 RVA: 0x00003388 File Offset: 0x00001588
		public CreateTopicRequest()
		{
		}

		// Token: 0x060000CD RID: 205 RVA: 0x0000359B File Offset: 0x0000179B
		public CreateTopicRequest(string name)
		{
			this._name = name;
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000CE RID: 206 RVA: 0x000035AA File Offset: 0x000017AA
		// (set) Token: 0x060000CF RID: 207 RVA: 0x000035B2 File Offset: 0x000017B2
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x000035BB File Offset: 0x000017BB
		internal bool IsSetName()
		{
			return this._name != null;
		}

		// Token: 0x04000016 RID: 22
		private string _name;
	}
}
