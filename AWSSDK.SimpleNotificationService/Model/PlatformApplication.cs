﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000039 RID: 57
	public class PlatformApplication
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x0600018F RID: 399 RVA: 0x00003C58 File Offset: 0x00001E58
		// (set) Token: 0x06000190 RID: 400 RVA: 0x00003C60 File Offset: 0x00001E60
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00003C69 File Offset: 0x00001E69
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000192 RID: 402 RVA: 0x00003C83 File Offset: 0x00001E83
		// (set) Token: 0x06000193 RID: 403 RVA: 0x00003C8B File Offset: 0x00001E8B
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x06000194 RID: 404 RVA: 0x00003C94 File Offset: 0x00001E94
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x0400003F RID: 63
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x04000040 RID: 64
		private string _platformApplicationArn;
	}
}
