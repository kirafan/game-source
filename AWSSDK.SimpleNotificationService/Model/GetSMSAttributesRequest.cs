﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000020 RID: 32
	public class GetSMSAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000102 RID: 258 RVA: 0x00003753 File Offset: 0x00001953
		// (set) Token: 0x06000103 RID: 259 RVA: 0x0000375B File Offset: 0x0000195B
		public List<string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00003764 File Offset: 0x00001964
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x04000021 RID: 33
		private List<string> _attributes = new List<string>();
	}
}
