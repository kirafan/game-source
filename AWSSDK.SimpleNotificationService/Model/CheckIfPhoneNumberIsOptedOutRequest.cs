﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000A RID: 10
	public class CheckIfPhoneNumberIsOptedOutRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000095 RID: 149 RVA: 0x0000336C File Offset: 0x0000156C
		// (set) Token: 0x06000096 RID: 150 RVA: 0x00003374 File Offset: 0x00001574
		public string PhoneNumber
		{
			get
			{
				return this._phoneNumber;
			}
			set
			{
				this._phoneNumber = value;
			}
		}

		// Token: 0x06000097 RID: 151 RVA: 0x0000337D File Offset: 0x0000157D
		internal bool IsSetPhoneNumber()
		{
			return this._phoneNumber != null;
		}

		// Token: 0x04000007 RID: 7
		private string _phoneNumber;
	}
}
