﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000018 RID: 24
	public class DeleteTopicRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060000DF RID: 223 RVA: 0x00003388 File Offset: 0x00001588
		public DeleteTopicRequest()
		{
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x0000361A File Offset: 0x0000181A
		public DeleteTopicRequest(string topicArn)
		{
			this._topicArn = topicArn;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000E1 RID: 225 RVA: 0x00003629 File Offset: 0x00001829
		// (set) Token: 0x060000E2 RID: 226 RVA: 0x00003631 File Offset: 0x00001831
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060000E3 RID: 227 RVA: 0x0000363A File Offset: 0x0000183A
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x0400001A RID: 26
		private string _topicArn;
	}
}
