﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200003C RID: 60
	public class PublishResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001B4 RID: 436 RVA: 0x00003DE1 File Offset: 0x00001FE1
		// (set) Token: 0x060001B5 RID: 437 RVA: 0x00003DE9 File Offset: 0x00001FE9
		public string MessageId
		{
			get
			{
				return this._messageId;
			}
			set
			{
				this._messageId = value;
			}
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00003DF2 File Offset: 0x00001FF2
		internal bool IsSetMessageId()
		{
			return this._messageId != null;
		}

		// Token: 0x04000048 RID: 72
		private string _messageId;
	}
}
