﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000009 RID: 9
	[Serializable]
	public class AuthorizationErrorException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x0600008F RID: 143 RVA: 0x00003326 File Offset: 0x00001526
		public AuthorizationErrorException(string message) : base(message)
		{
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0000332F File Offset: 0x0000152F
		public AuthorizationErrorException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00003339 File Offset: 0x00001539
		public AuthorizationErrorException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00003342 File Offset: 0x00001542
		public AuthorizationErrorException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003353 File Offset: 0x00001553
		public AuthorizationErrorException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003362 File Offset: 0x00001562
		protected AuthorizationErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
