﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000024 RID: 36
	public class GetTopicAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x06000113 RID: 275 RVA: 0x00003388 File Offset: 0x00001588
		public GetTopicAttributesRequest()
		{
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00003838 File Offset: 0x00001A38
		public GetTopicAttributesRequest(string topicArn)
		{
			this._topicArn = topicArn;
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00003847 File Offset: 0x00001A47
		// (set) Token: 0x06000116 RID: 278 RVA: 0x0000384F File Offset: 0x00001A4F
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00003858 File Offset: 0x00001A58
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000025 RID: 37
		private string _topicArn;
	}
}
