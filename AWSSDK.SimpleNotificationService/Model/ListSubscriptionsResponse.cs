﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000032 RID: 50
	public class ListSubscriptionsResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00003B01 File Offset: 0x00001D01
		// (set) Token: 0x06000168 RID: 360 RVA: 0x00003B09 File Offset: 0x00001D09
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00003B12 File Offset: 0x00001D12
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600016A RID: 362 RVA: 0x00003B1D File Offset: 0x00001D1D
		// (set) Token: 0x0600016B RID: 363 RVA: 0x00003B25 File Offset: 0x00001D25
		public List<Subscription> Subscriptions
		{
			get
			{
				return this._subscriptions;
			}
			set
			{
				this._subscriptions = value;
			}
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00003B2E File Offset: 0x00001D2E
		internal bool IsSetSubscriptions()
		{
			return this._subscriptions != null && this._subscriptions.Count > 0;
		}

		// Token: 0x04000036 RID: 54
		private string _nextToken;

		// Token: 0x04000037 RID: 55
		private List<Subscription> _subscriptions = new List<Subscription>();
	}
}
