﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200003B RID: 59
	public class PublishRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x0600019C RID: 412 RVA: 0x00003CB2 File Offset: 0x00001EB2
		public PublishRequest()
		{
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00003CC5 File Offset: 0x00001EC5
		public PublishRequest(string topicArn, string message)
		{
			this._topicArn = topicArn;
			this._message = message;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x00003CE6 File Offset: 0x00001EE6
		public PublishRequest(string topicArn, string message, string subject)
		{
			this._topicArn = topicArn;
			this._message = message;
			this._subject = subject;
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00003D0E File Offset: 0x00001F0E
		// (set) Token: 0x060001A0 RID: 416 RVA: 0x00003D16 File Offset: 0x00001F16
		public string Message
		{
			get
			{
				return this._message;
			}
			set
			{
				this._message = value;
			}
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x00003D1F File Offset: 0x00001F1F
		internal bool IsSetMessage()
		{
			return this._message != null;
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001A2 RID: 418 RVA: 0x00003D2A File Offset: 0x00001F2A
		// (set) Token: 0x060001A3 RID: 419 RVA: 0x00003D32 File Offset: 0x00001F32
		public Dictionary<string, MessageAttributeValue> MessageAttributes
		{
			get
			{
				return this._messageAttributes;
			}
			set
			{
				this._messageAttributes = value;
			}
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00003D3B File Offset: 0x00001F3B
		internal bool IsSetMessageAttributes()
		{
			return this._messageAttributes != null && this._messageAttributes.Count > 0;
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001A5 RID: 421 RVA: 0x00003D55 File Offset: 0x00001F55
		// (set) Token: 0x060001A6 RID: 422 RVA: 0x00003D5D File Offset: 0x00001F5D
		public string MessageStructure
		{
			get
			{
				return this._messageStructure;
			}
			set
			{
				this._messageStructure = value;
			}
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00003D66 File Offset: 0x00001F66
		internal bool IsSetMessageStructure()
		{
			return this._messageStructure != null;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001A8 RID: 424 RVA: 0x00003D71 File Offset: 0x00001F71
		// (set) Token: 0x060001A9 RID: 425 RVA: 0x00003D79 File Offset: 0x00001F79
		public string PhoneNumber
		{
			get
			{
				return this._phoneNumber;
			}
			set
			{
				this._phoneNumber = value;
			}
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00003D82 File Offset: 0x00001F82
		internal bool IsSetPhoneNumber()
		{
			return this._phoneNumber != null;
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00003D8D File Offset: 0x00001F8D
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00003D95 File Offset: 0x00001F95
		public string Subject
		{
			get
			{
				return this._subject;
			}
			set
			{
				this._subject = value;
			}
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00003D9E File Offset: 0x00001F9E
		internal bool IsSetSubject()
		{
			return this._subject != null;
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001AE RID: 430 RVA: 0x00003DA9 File Offset: 0x00001FA9
		// (set) Token: 0x060001AF RID: 431 RVA: 0x00003DB1 File Offset: 0x00001FB1
		public string TargetArn
		{
			get
			{
				return this._targetArn;
			}
			set
			{
				this._targetArn = value;
			}
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x00003DBA File Offset: 0x00001FBA
		internal bool IsSetTargetArn()
		{
			return this._targetArn != null;
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x00003DC5 File Offset: 0x00001FC5
		// (set) Token: 0x060001B2 RID: 434 RVA: 0x00003DCD File Offset: 0x00001FCD
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00003DD6 File Offset: 0x00001FD6
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000041 RID: 65
		private string _message;

		// Token: 0x04000042 RID: 66
		private Dictionary<string, MessageAttributeValue> _messageAttributes = new Dictionary<string, MessageAttributeValue>();

		// Token: 0x04000043 RID: 67
		private string _messageStructure;

		// Token: 0x04000044 RID: 68
		private string _phoneNumber;

		// Token: 0x04000045 RID: 69
		private string _subject;

		// Token: 0x04000046 RID: 70
		private string _targetArn;

		// Token: 0x04000047 RID: 71
		private string _topicArn;
	}
}
