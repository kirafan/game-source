﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005C RID: 92
	public class CreateTopicRequestMarshaller : IMarshaller<IRequest, CreateTopicRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600025B RID: 603 RVA: 0x000050EB File Offset: 0x000032EB
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((CreateTopicRequest)input);
		}

		// Token: 0x0600025C RID: 604 RVA: 0x000050FC File Offset: 0x000032FC
		public IRequest Marshall(CreateTopicRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "CreateTopic");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetName())
			{
				request.Parameters.Add("Name", StringUtils.FromString(publicRequest.Name));
			}
			return request;
		}
	}
}
