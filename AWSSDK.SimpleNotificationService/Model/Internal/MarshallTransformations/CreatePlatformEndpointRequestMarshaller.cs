﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005A RID: 90
	public class CreatePlatformEndpointRequestMarshaller : IMarshaller<IRequest, CreatePlatformEndpointRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000251 RID: 593 RVA: 0x00004D75 File Offset: 0x00002F75
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((CreatePlatformEndpointRequest)input);
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00004D84 File Offset: 0x00002F84
		public IRequest Marshall(CreatePlatformEndpointRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "CreatePlatformEndpoint");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributes())
				{
					int num = 1;
					foreach (string text in publicRequest.Attributes.Keys)
					{
						string value;
						bool flag = publicRequest.Attributes.TryGetValue(text, out value);
						request.Parameters.Add("Attributes.entry." + num + ".key", StringUtils.FromString(text));
						if (flag)
						{
							request.Parameters.Add("Attributes.entry." + num + ".value", StringUtils.FromString(value));
						}
						num++;
					}
				}
				if (publicRequest.IsSetCustomUserData())
				{
					request.Parameters.Add("CustomUserData", StringUtils.FromString(publicRequest.CustomUserData));
				}
				if (publicRequest.IsSetPlatformApplicationArn())
				{
					request.Parameters.Add("PlatformApplicationArn", StringUtils.FromString(publicRequest.PlatformApplicationArn));
				}
				if (publicRequest.IsSetToken())
				{
					request.Parameters.Add("Token", StringUtils.FromString(publicRequest.Token));
				}
			}
			return request;
		}
	}
}
