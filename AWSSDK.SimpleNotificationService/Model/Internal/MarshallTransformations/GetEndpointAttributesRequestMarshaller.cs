﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000065 RID: 101
	public class GetEndpointAttributesRequestMarshaller : IMarshaller<IRequest, GetEndpointAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000288 RID: 648 RVA: 0x00005AD0 File Offset: 0x00003CD0
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetEndpointAttributesRequest)input);
		}

		// Token: 0x06000289 RID: 649 RVA: 0x00005AE0 File Offset: 0x00003CE0
		public IRequest Marshall(GetEndpointAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "GetEndpointAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetEndpointArn())
			{
				request.Parameters.Add("EndpointArn", StringUtils.FromString(publicRequest.EndpointArn));
			}
			return request;
		}
	}
}
