﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007F RID: 127
	public class PublishResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000308 RID: 776 RVA: 0x00007CC8 File Offset: 0x00005EC8
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			PublishResponse publishResponse = new PublishResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("PublishResult", 2))
					{
						PublishResponseUnmarshaller.UnmarshallResult(context, publishResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						publishResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return publishResponse;
		}

		// Token: 0x06000309 RID: 777 RVA: 0x00007D34 File Offset: 0x00005F34
		private static void UnmarshallResult(XmlUnmarshallerContext context, PublishResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("MessageId", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.MessageId = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x0600030A RID: 778 RVA: 0x00007D94 File Offset: 0x00005F94
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("EndpointDisabled"))
			{
				return new EndpointDisabledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ParameterValueInvalid"))
			{
				return new InvalidParameterValueException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("PlatformApplicationDisabled"))
			{
				return new PlatformApplicationDisabledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600030B RID: 779 RVA: 0x00007F62 File Offset: 0x00006162
		internal static PublishResponseUnmarshaller GetInstance()
		{
			return PublishResponseUnmarshaller._instance;
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600030C RID: 780 RVA: 0x00007F62 File Offset: 0x00006162
		public static PublishResponseUnmarshaller Instance
		{
			get
			{
				return PublishResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000078 RID: 120
		private static PublishResponseUnmarshaller _instance = new PublishResponseUnmarshaller();
	}
}
