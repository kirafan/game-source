﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000084 RID: 132
	public class SetPlatformApplicationAttributesRequestMarshaller : IMarshaller<IRequest, SetPlatformApplicationAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000323 RID: 803 RVA: 0x00008503 File Offset: 0x00006703
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetPlatformApplicationAttributesRequest)input);
		}

		// Token: 0x06000324 RID: 804 RVA: 0x00008514 File Offset: 0x00006714
		public IRequest Marshall(SetPlatformApplicationAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "SetPlatformApplicationAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributes())
				{
					int num = 1;
					foreach (string text in publicRequest.Attributes.Keys)
					{
						string value;
						bool flag = publicRequest.Attributes.TryGetValue(text, out value);
						request.Parameters.Add("Attributes.entry." + num + ".key", StringUtils.FromString(text));
						if (flag)
						{
							request.Parameters.Add("Attributes.entry." + num + ".value", StringUtils.FromString(value));
						}
						num++;
					}
				}
				if (publicRequest.IsSetPlatformApplicationArn())
				{
					request.Parameters.Add("PlatformApplicationArn", StringUtils.FromString(publicRequest.PlatformApplicationArn));
				}
			}
			return request;
		}
	}
}
