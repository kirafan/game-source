﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000059 RID: 89
	public class CreatePlatformApplicationResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600024A RID: 586 RVA: 0x00004BB0 File Offset: 0x00002DB0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			CreatePlatformApplicationResponse createPlatformApplicationResponse = new CreatePlatformApplicationResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("CreatePlatformApplicationResult", 2))
					{
						CreatePlatformApplicationResponseUnmarshaller.UnmarshallResult(context, createPlatformApplicationResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						createPlatformApplicationResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return createPlatformApplicationResponse;
		}

		// Token: 0x0600024B RID: 587 RVA: 0x00004C1C File Offset: 0x00002E1C
		private static void UnmarshallResult(XmlUnmarshallerContext context, CreatePlatformApplicationResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("PlatformApplicationArn", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.PlatformApplicationArn = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x0600024C RID: 588 RVA: 0x00004C7C File Offset: 0x00002E7C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00004D62 File Offset: 0x00002F62
		internal static CreatePlatformApplicationResponseUnmarshaller GetInstance()
		{
			return CreatePlatformApplicationResponseUnmarshaller._instance;
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00004D62 File Offset: 0x00002F62
		public static CreatePlatformApplicationResponseUnmarshaller Instance
		{
			get
			{
				return CreatePlatformApplicationResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000064 RID: 100
		private static CreatePlatformApplicationResponseUnmarshaller _instance = new CreatePlatformApplicationResponseUnmarshaller();
	}
}
