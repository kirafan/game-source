﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007B RID: 123
	public class OptInPhoneNumberRequestMarshaller : IMarshaller<IRequest, OptInPhoneNumberRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002F6 RID: 758 RVA: 0x00007745 File Offset: 0x00005945
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((OptInPhoneNumberRequest)input);
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x00007754 File Offset: 0x00005954
		public IRequest Marshall(OptInPhoneNumberRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "OptInPhoneNumber");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetPhoneNumber())
			{
				request.Parameters.Add("phoneNumber", StringUtils.FromString(publicRequest.PhoneNumber));
			}
			return request;
		}
	}
}
