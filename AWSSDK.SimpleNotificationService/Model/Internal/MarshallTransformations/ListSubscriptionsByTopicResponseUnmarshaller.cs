﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000076 RID: 118
	public class ListSubscriptionsByTopicResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002DB RID: 731 RVA: 0x0000704C File Offset: 0x0000524C
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListSubscriptionsByTopicResponse listSubscriptionsByTopicResponse = new ListSubscriptionsByTopicResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListSubscriptionsByTopicResult", 2))
					{
						ListSubscriptionsByTopicResponseUnmarshaller.UnmarshallResult(context, listSubscriptionsByTopicResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listSubscriptionsByTopicResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listSubscriptionsByTopicResponse;
		}

		// Token: 0x060002DC RID: 732 RVA: 0x000070B8 File Offset: 0x000052B8
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListSubscriptionsByTopicResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("NextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
					else if (context.TestExpression("Subscriptions/member", num))
					{
						Subscription item = SubscriptionUnmarshaller.Instance.Unmarshall(context);
						response.Subscriptions.Add(item);
					}
				}
			}
		}

		// Token: 0x060002DD RID: 733 RVA: 0x00007140 File Offset: 0x00005340
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002DE RID: 734 RVA: 0x00007260 File Offset: 0x00005460
		internal static ListSubscriptionsByTopicResponseUnmarshaller GetInstance()
		{
			return ListSubscriptionsByTopicResponseUnmarshaller._instance;
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060002DF RID: 735 RVA: 0x00007260 File Offset: 0x00005460
		public static ListSubscriptionsByTopicResponseUnmarshaller Instance
		{
			get
			{
				return ListSubscriptionsByTopicResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000073 RID: 115
		private static ListSubscriptionsByTopicResponseUnmarshaller _instance = new ListSubscriptionsByTopicResponseUnmarshaller();
	}
}
