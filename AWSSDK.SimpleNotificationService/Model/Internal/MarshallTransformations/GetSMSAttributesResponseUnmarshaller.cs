﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006A RID: 106
	public class GetSMSAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600029F RID: 671 RVA: 0x000060B0 File Offset: 0x000042B0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			GetSMSAttributesResponse getSMSAttributesResponse = new GetSMSAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("GetSMSAttributesResult", 2))
					{
						GetSMSAttributesResponseUnmarshaller.UnmarshallResult(context, getSMSAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						getSMSAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return getSMSAttributesResponse;
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x0000611C File Offset: 0x0000431C
		private static void UnmarshallResult(XmlUnmarshallerContext context, GetSMSAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("attributes/entry", num))
				{
					KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
					response.Attributes.Add(item);
				}
			}
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000618C File Offset: 0x0000438C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("Throttled"))
			{
				return new ThrottledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x000062AC File Offset: 0x000044AC
		internal static GetSMSAttributesResponseUnmarshaller GetInstance()
		{
			return GetSMSAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060002A3 RID: 675 RVA: 0x000062AC File Offset: 0x000044AC
		public static GetSMSAttributesResponseUnmarshaller Instance
		{
			get
			{
				return GetSMSAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400006D RID: 109
		private static GetSMSAttributesResponseUnmarshaller _instance = new GetSMSAttributesResponseUnmarshaller();
	}
}
