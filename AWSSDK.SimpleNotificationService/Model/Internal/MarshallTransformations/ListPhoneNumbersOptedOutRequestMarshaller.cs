﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000071 RID: 113
	public class ListPhoneNumbersOptedOutRequestMarshaller : IMarshaller<IRequest, ListPhoneNumbersOptedOutRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002C4 RID: 708 RVA: 0x00006A9F File Offset: 0x00004C9F
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListPhoneNumbersOptedOutRequest)input);
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x00006AB0 File Offset: 0x00004CB0
		public IRequest Marshall(ListPhoneNumbersOptedOutRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListPhoneNumbersOptedOut");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetNextToken())
			{
				request.Parameters.Add("nextToken", StringUtils.FromString(publicRequest.NextToken));
			}
			return request;
		}
	}
}
