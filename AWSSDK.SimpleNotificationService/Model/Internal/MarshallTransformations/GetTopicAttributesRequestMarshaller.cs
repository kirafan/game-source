﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006D RID: 109
	public class GetTopicAttributesRequestMarshaller : IMarshaller<IRequest, GetTopicAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002B0 RID: 688 RVA: 0x0000654B File Offset: 0x0000474B
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetTopicAttributesRequest)input);
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000655C File Offset: 0x0000475C
		public IRequest Marshall(GetTopicAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "GetTopicAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetTopicArn())
			{
				request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
			}
			return request;
		}
	}
}
