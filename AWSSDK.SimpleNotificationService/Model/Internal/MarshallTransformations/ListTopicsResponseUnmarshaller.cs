﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007A RID: 122
	public class ListTopicsResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002EF RID: 751 RVA: 0x00007558 File Offset: 0x00005758
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListTopicsResponse listTopicsResponse = new ListTopicsResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListTopicsResult", 2))
					{
						ListTopicsResponseUnmarshaller.UnmarshallResult(context, listTopicsResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listTopicsResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listTopicsResponse;
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x000075C4 File Offset: 0x000057C4
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListTopicsResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("NextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
					else if (context.TestExpression("Topics/member", num))
					{
						Topic item = TopicUnmarshaller.Instance.Unmarshall(context);
						response.Topics.Add(item);
					}
				}
			}
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000764C File Offset: 0x0000584C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x00007732 File Offset: 0x00005932
		internal static ListTopicsResponseUnmarshaller GetInstance()
		{
			return ListTopicsResponseUnmarshaller._instance;
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060002F3 RID: 755 RVA: 0x00007732 File Offset: 0x00005932
		public static ListTopicsResponseUnmarshaller Instance
		{
			get
			{
				return ListTopicsResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000075 RID: 117
		private static ListTopicsResponseUnmarshaller _instance = new ListTopicsResponseUnmarshaller();
	}
}
