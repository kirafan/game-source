﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006C RID: 108
	public class GetSubscriptionAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002A9 RID: 681 RVA: 0x0000633C File Offset: 0x0000453C
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			GetSubscriptionAttributesResponse getSubscriptionAttributesResponse = new GetSubscriptionAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("GetSubscriptionAttributesResult", 2))
					{
						GetSubscriptionAttributesResponseUnmarshaller.UnmarshallResult(context, getSubscriptionAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						getSubscriptionAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return getSubscriptionAttributesResponse;
		}

		// Token: 0x060002AA RID: 682 RVA: 0x000063A8 File Offset: 0x000045A8
		private static void UnmarshallResult(XmlUnmarshallerContext context, GetSubscriptionAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("Attributes/entry", num))
				{
					KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
					response.Attributes.Add(item);
				}
			}
		}

		// Token: 0x060002AB RID: 683 RVA: 0x00006418 File Offset: 0x00004618
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x00006538 File Offset: 0x00004738
		internal static GetSubscriptionAttributesResponseUnmarshaller GetInstance()
		{
			return GetSubscriptionAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00006538 File Offset: 0x00004738
		public static GetSubscriptionAttributesResponseUnmarshaller Instance
		{
			get
			{
				return GetSubscriptionAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400006E RID: 110
		private static GetSubscriptionAttributesResponseUnmarshaller _instance = new GetSubscriptionAttributesResponseUnmarshaller();
	}
}
