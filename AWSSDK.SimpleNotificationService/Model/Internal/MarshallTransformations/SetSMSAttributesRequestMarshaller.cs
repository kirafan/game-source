﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000086 RID: 134
	public class SetSMSAttributesRequestMarshaller : IMarshaller<IRequest, SetSMSAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600032D RID: 813 RVA: 0x00008813 File Offset: 0x00006A13
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetSMSAttributesRequest)input);
		}

		// Token: 0x0600032E RID: 814 RVA: 0x00008824 File Offset: 0x00006A24
		public IRequest Marshall(SetSMSAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "SetSMSAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetAttributes())
			{
				int num = 1;
				foreach (string text in publicRequest.Attributes.Keys)
				{
					string value;
					bool flag = publicRequest.Attributes.TryGetValue(text, out value);
					request.Parameters.Add("attributes.entry." + num + ".key", StringUtils.FromString(text));
					if (flag)
					{
						request.Parameters.Add("attributes.entry." + num + ".value", StringUtils.FromString(value));
					}
					num++;
				}
			}
			return request;
		}
	}
}
