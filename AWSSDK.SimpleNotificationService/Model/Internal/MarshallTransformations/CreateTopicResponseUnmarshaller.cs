﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005D RID: 93
	public class CreateTopicResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600025E RID: 606 RVA: 0x00005168 File Offset: 0x00003368
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			CreateTopicResponse createTopicResponse = new CreateTopicResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("CreateTopicResult", 2))
					{
						CreateTopicResponseUnmarshaller.UnmarshallResult(context, createTopicResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						createTopicResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return createTopicResponse;
		}

		// Token: 0x0600025F RID: 607 RVA: 0x000051D4 File Offset: 0x000033D4
		private static void UnmarshallResult(XmlUnmarshallerContext context, CreateTopicResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("TopicArn", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.TopicArn = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x06000260 RID: 608 RVA: 0x00005234 File Offset: 0x00003434
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TopicLimitExceeded"))
			{
				return new TopicLimitExceededException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000261 RID: 609 RVA: 0x00005354 File Offset: 0x00003554
		internal static CreateTopicResponseUnmarshaller GetInstance()
		{
			return CreateTopicResponseUnmarshaller._instance;
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000262 RID: 610 RVA: 0x00005354 File Offset: 0x00003554
		public static CreateTopicResponseUnmarshaller Instance
		{
			get
			{
				return CreateTopicResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000066 RID: 102
		private static CreateTopicResponseUnmarshaller _instance = new CreateTopicResponseUnmarshaller();
	}
}
