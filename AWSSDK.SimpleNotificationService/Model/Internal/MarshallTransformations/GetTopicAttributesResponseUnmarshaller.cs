﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006E RID: 110
	public class GetTopicAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002B3 RID: 691 RVA: 0x000065C8 File Offset: 0x000047C8
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			GetTopicAttributesResponse getTopicAttributesResponse = new GetTopicAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("GetTopicAttributesResult", 2))
					{
						GetTopicAttributesResponseUnmarshaller.UnmarshallResult(context, getTopicAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						getTopicAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return getTopicAttributesResponse;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x00006634 File Offset: 0x00004834
		private static void UnmarshallResult(XmlUnmarshallerContext context, GetTopicAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("Attributes/entry", num))
				{
					KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
					response.Attributes.Add(item);
				}
			}
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x000066A4 File Offset: 0x000048A4
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x000067C4 File Offset: 0x000049C4
		internal static GetTopicAttributesResponseUnmarshaller GetInstance()
		{
			return GetTopicAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x000067C4 File Offset: 0x000049C4
		public static GetTopicAttributesResponseUnmarshaller Instance
		{
			get
			{
				return GetTopicAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400006F RID: 111
		private static GetTopicAttributesResponseUnmarshaller _instance = new GetTopicAttributesResponseUnmarshaller();
	}
}
