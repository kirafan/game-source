﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000081 RID: 129
	public class RemovePermissionResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000312 RID: 786 RVA: 0x00008014 File Offset: 0x00006214
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			RemovePermissionResponse removePermissionResponse = new RemovePermissionResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("RemovePermissionResult", 2))
					{
						RemovePermissionResponseUnmarshaller.UnmarshallResult(context, removePermissionResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						removePermissionResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return removePermissionResponse;
		}

		// Token: 0x06000313 RID: 787 RVA: 0x00008080 File Offset: 0x00006280
		private static void UnmarshallResult(XmlUnmarshallerContext context, RemovePermissionResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000314 RID: 788 RVA: 0x000080C0 File Offset: 0x000062C0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000315 RID: 789 RVA: 0x000081E0 File Offset: 0x000063E0
		internal static RemovePermissionResponseUnmarshaller GetInstance()
		{
			return RemovePermissionResponseUnmarshaller._instance;
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000316 RID: 790 RVA: 0x000081E0 File Offset: 0x000063E0
		public static RemovePermissionResponseUnmarshaller Instance
		{
			get
			{
				return RemovePermissionResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000079 RID: 121
		private static RemovePermissionResponseUnmarshaller _instance = new RemovePermissionResponseUnmarshaller();
	}
}
