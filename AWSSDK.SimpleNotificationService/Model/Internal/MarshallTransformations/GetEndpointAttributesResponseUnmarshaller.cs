﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000066 RID: 102
	public class GetEndpointAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600028B RID: 651 RVA: 0x00005B4C File Offset: 0x00003D4C
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			GetEndpointAttributesResponse getEndpointAttributesResponse = new GetEndpointAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("GetEndpointAttributesResult", 2))
					{
						GetEndpointAttributesResponseUnmarshaller.UnmarshallResult(context, getEndpointAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						getEndpointAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return getEndpointAttributesResponse;
		}

		// Token: 0x0600028C RID: 652 RVA: 0x00005BB8 File Offset: 0x00003DB8
		private static void UnmarshallResult(XmlUnmarshallerContext context, GetEndpointAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("Attributes/entry", num))
				{
					KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
					response.Attributes.Add(item);
				}
			}
		}

		// Token: 0x0600028D RID: 653 RVA: 0x00005C28 File Offset: 0x00003E28
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600028E RID: 654 RVA: 0x00005D48 File Offset: 0x00003F48
		internal static GetEndpointAttributesResponseUnmarshaller GetInstance()
		{
			return GetEndpointAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x0600028F RID: 655 RVA: 0x00005D48 File Offset: 0x00003F48
		public static GetEndpointAttributesResponseUnmarshaller Instance
		{
			get
			{
				return GetEndpointAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400006B RID: 107
		private static GetEndpointAttributesResponseUnmarshaller _instance = new GetEndpointAttributesResponseUnmarshaller();
	}
}
