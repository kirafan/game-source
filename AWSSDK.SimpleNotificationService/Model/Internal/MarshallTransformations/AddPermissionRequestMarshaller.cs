﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000052 RID: 82
	public class AddPermissionRequestMarshaller : IMarshaller<IRequest, AddPermissionRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000229 RID: 553 RVA: 0x0000417F File Offset: 0x0000237F
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((AddPermissionRequest)input);
		}

		// Token: 0x0600022A RID: 554 RVA: 0x00004190 File Offset: 0x00002390
		public IRequest Marshall(AddPermissionRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "AddPermission");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetActionName())
				{
					int num = 1;
					foreach (string value in publicRequest.ActionName)
					{
						request.Parameters.Add("ActionName.member." + num, StringUtils.FromString(value));
						num++;
					}
				}
				if (publicRequest.IsSetAWSAccountId())
				{
					int num2 = 1;
					foreach (string value2 in publicRequest.AWSAccountId)
					{
						request.Parameters.Add("AWSAccountId.member." + num2, StringUtils.FromString(value2));
						num2++;
					}
				}
				if (publicRequest.IsSetLabel())
				{
					request.Parameters.Add("Label", StringUtils.FromString(publicRequest.Label));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
