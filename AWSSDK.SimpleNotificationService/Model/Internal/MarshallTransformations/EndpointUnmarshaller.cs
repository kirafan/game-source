﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000064 RID: 100
	public class EndpointUnmarshaller : IUnmarshaller<Endpoint, XmlUnmarshallerContext>, IUnmarshaller<Endpoint, JsonUnmarshallerContext>
	{
		// Token: 0x06000283 RID: 643 RVA: 0x00005A04 File Offset: 0x00003C04
		public Endpoint Unmarshall(XmlUnmarshallerContext context)
		{
			Endpoint endpoint = new Endpoint();
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("Attributes/entry", num))
					{
						KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
						endpoint.Attributes.Add(item);
					}
					else if (context.TestExpression("EndpointArn", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						endpoint.EndpointArn = instance.Unmarshall(context);
					}
				}
				else if (context.IsEndElement && context.CurrentDepth < currentDepth)
				{
					return endpoint;
				}
			}
			return endpoint;
		}

		// Token: 0x06000284 RID: 644 RVA: 0x00005ABA File Offset: 0x00003CBA
		public Endpoint Unmarshall(JsonUnmarshallerContext context)
		{
			return null;
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000285 RID: 645 RVA: 0x00005ABD File Offset: 0x00003CBD
		public static EndpointUnmarshaller Instance
		{
			get
			{
				return EndpointUnmarshaller._instance;
			}
		}

		// Token: 0x0400006A RID: 106
		private static EndpointUnmarshaller _instance = new EndpointUnmarshaller();
	}
}
