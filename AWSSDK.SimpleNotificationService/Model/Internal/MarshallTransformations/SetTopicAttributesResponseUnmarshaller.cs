﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008B RID: 139
	public class SetTopicAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000344 RID: 836 RVA: 0x00008E60 File Offset: 0x00007060
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SetTopicAttributesResponse setTopicAttributesResponse = new SetTopicAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SetTopicAttributesResult", 2))
					{
						SetTopicAttributesResponseUnmarshaller.UnmarshallResult(context, setTopicAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						setTopicAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return setTopicAttributesResponse;
		}

		// Token: 0x06000345 RID: 837 RVA: 0x00008ECC File Offset: 0x000070CC
		private static void UnmarshallResult(XmlUnmarshallerContext context, SetTopicAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000346 RID: 838 RVA: 0x00008F0C File Offset: 0x0000710C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000902C File Offset: 0x0000722C
		internal static SetTopicAttributesResponseUnmarshaller GetInstance()
		{
			return SetTopicAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000348 RID: 840 RVA: 0x0000902C File Offset: 0x0000722C
		public static SetTopicAttributesResponseUnmarshaller Instance
		{
			get
			{
				return SetTopicAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007E RID: 126
		private static SetTopicAttributesResponseUnmarshaller _instance = new SetTopicAttributesResponseUnmarshaller();
	}
}
