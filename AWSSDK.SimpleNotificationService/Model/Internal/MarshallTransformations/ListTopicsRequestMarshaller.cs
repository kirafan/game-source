﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000079 RID: 121
	public class ListTopicsRequestMarshaller : IMarshaller<IRequest, ListTopicsRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002EC RID: 748 RVA: 0x000074DD File Offset: 0x000056DD
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListTopicsRequest)input);
		}

		// Token: 0x060002ED RID: 749 RVA: 0x000074EC File Offset: 0x000056EC
		public IRequest Marshall(ListTopicsRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListTopics");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetNextToken())
			{
				request.Parameters.Add("NextToken", StringUtils.FromString(publicRequest.NextToken));
			}
			return request;
		}
	}
}
