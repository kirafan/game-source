﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000089 RID: 137
	public class SetSubscriptionAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600033A RID: 826 RVA: 0x00008BC0 File Offset: 0x00006DC0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SetSubscriptionAttributesResponse setSubscriptionAttributesResponse = new SetSubscriptionAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SetSubscriptionAttributesResult", 2))
					{
						SetSubscriptionAttributesResponseUnmarshaller.UnmarshallResult(context, setSubscriptionAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						setSubscriptionAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return setSubscriptionAttributesResponse;
		}

		// Token: 0x0600033B RID: 827 RVA: 0x00008C2C File Offset: 0x00006E2C
		private static void UnmarshallResult(XmlUnmarshallerContext context, SetSubscriptionAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x0600033C RID: 828 RVA: 0x00008C6C File Offset: 0x00006E6C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600033D RID: 829 RVA: 0x00008D8C File Offset: 0x00006F8C
		internal static SetSubscriptionAttributesResponseUnmarshaller GetInstance()
		{
			return SetSubscriptionAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600033E RID: 830 RVA: 0x00008D8C File Offset: 0x00006F8C
		public static SetSubscriptionAttributesResponseUnmarshaller Instance
		{
			get
			{
				return SetSubscriptionAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007D RID: 125
		private static SetSubscriptionAttributesResponseUnmarshaller _instance = new SetSubscriptionAttributesResponseUnmarshaller();
	}
}
