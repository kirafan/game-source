﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007C RID: 124
	public class OptInPhoneNumberResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002F9 RID: 761 RVA: 0x000077C0 File Offset: 0x000059C0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			OptInPhoneNumberResponse optInPhoneNumberResponse = new OptInPhoneNumberResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("OptInPhoneNumberResult", 2))
					{
						OptInPhoneNumberResponseUnmarshaller.UnmarshallResult(context, optInPhoneNumberResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						optInPhoneNumberResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return optInPhoneNumberResponse;
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000782C File Offset: 0x00005A2C
		private static void UnmarshallResult(XmlUnmarshallerContext context, OptInPhoneNumberResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000786C File Offset: 0x00005A6C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("Throttled"))
			{
				return new ThrottledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000798C File Offset: 0x00005B8C
		internal static OptInPhoneNumberResponseUnmarshaller GetInstance()
		{
			return OptInPhoneNumberResponseUnmarshaller._instance;
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060002FD RID: 765 RVA: 0x0000798C File Offset: 0x00005B8C
		public static OptInPhoneNumberResponseUnmarshaller Instance
		{
			get
			{
				return OptInPhoneNumberResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000076 RID: 118
		private static OptInPhoneNumberResponseUnmarshaller _instance = new OptInPhoneNumberResponseUnmarshaller();
	}
}
