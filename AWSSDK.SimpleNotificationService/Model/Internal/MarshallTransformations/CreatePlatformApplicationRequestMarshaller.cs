﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000058 RID: 88
	public class CreatePlatformApplicationRequestMarshaller : IMarshaller<IRequest, CreatePlatformApplicationRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000247 RID: 583 RVA: 0x00004A5D File Offset: 0x00002C5D
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((CreatePlatformApplicationRequest)input);
		}

		// Token: 0x06000248 RID: 584 RVA: 0x00004A6C File Offset: 0x00002C6C
		public IRequest Marshall(CreatePlatformApplicationRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "CreatePlatformApplication");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributes())
				{
					int num = 1;
					foreach (string text in publicRequest.Attributes.Keys)
					{
						string value;
						bool flag = publicRequest.Attributes.TryGetValue(text, out value);
						request.Parameters.Add("Attributes.entry." + num + ".key", StringUtils.FromString(text));
						if (flag)
						{
							request.Parameters.Add("Attributes.entry." + num + ".value", StringUtils.FromString(value));
						}
						num++;
					}
				}
				if (publicRequest.IsSetName())
				{
					request.Parameters.Add("Name", StringUtils.FromString(publicRequest.Name));
				}
				if (publicRequest.IsSetPlatform())
				{
					request.Parameters.Add("Platform", StringUtils.FromString(publicRequest.Platform));
				}
			}
			return request;
		}
	}
}
