﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005E RID: 94
	public class DeleteEndpointRequestMarshaller : IMarshaller<IRequest, DeleteEndpointRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000265 RID: 613 RVA: 0x00005367 File Offset: 0x00003567
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DeleteEndpointRequest)input);
		}

		// Token: 0x06000266 RID: 614 RVA: 0x00005378 File Offset: 0x00003578
		public IRequest Marshall(DeleteEndpointRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "DeleteEndpoint");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetEndpointArn())
			{
				request.Parameters.Add("EndpointArn", StringUtils.FromString(publicRequest.EndpointArn));
			}
			return request;
		}
	}
}
