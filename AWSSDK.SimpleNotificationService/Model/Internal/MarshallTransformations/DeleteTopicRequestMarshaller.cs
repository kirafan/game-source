﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000062 RID: 98
	public class DeleteTopicRequestMarshaller : IMarshaller<IRequest, DeleteTopicRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000279 RID: 633 RVA: 0x000057A9 File Offset: 0x000039A9
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DeleteTopicRequest)input);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x000057B8 File Offset: 0x000039B8
		public IRequest Marshall(DeleteTopicRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "DeleteTopic");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetTopicArn())
			{
				request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
			}
			return request;
		}
	}
}
