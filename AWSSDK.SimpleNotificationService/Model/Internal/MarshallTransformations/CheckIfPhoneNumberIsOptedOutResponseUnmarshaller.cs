﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000055 RID: 85
	public class CheckIfPhoneNumberIsOptedOutResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000236 RID: 566 RVA: 0x00004564 File Offset: 0x00002764
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			CheckIfPhoneNumberIsOptedOutResponse checkIfPhoneNumberIsOptedOutResponse = new CheckIfPhoneNumberIsOptedOutResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("CheckIfPhoneNumberIsOptedOutResult", 2))
					{
						CheckIfPhoneNumberIsOptedOutResponseUnmarshaller.UnmarshallResult(context, checkIfPhoneNumberIsOptedOutResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						checkIfPhoneNumberIsOptedOutResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return checkIfPhoneNumberIsOptedOutResponse;
		}

		// Token: 0x06000237 RID: 567 RVA: 0x000045D0 File Offset: 0x000027D0
		private static void UnmarshallResult(XmlUnmarshallerContext context, CheckIfPhoneNumberIsOptedOutResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("isOptedOut", num))
				{
					BoolUnmarshaller instance = BoolUnmarshaller.Instance;
					response.IsOptedOut = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x06000238 RID: 568 RVA: 0x00004630 File Offset: 0x00002830
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("Throttled"))
			{
				return new ThrottledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000239 RID: 569 RVA: 0x00004750 File Offset: 0x00002950
		internal static CheckIfPhoneNumberIsOptedOutResponseUnmarshaller GetInstance()
		{
			return CheckIfPhoneNumberIsOptedOutResponseUnmarshaller._instance;
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600023A RID: 570 RVA: 0x00004750 File Offset: 0x00002950
		public static CheckIfPhoneNumberIsOptedOutResponseUnmarshaller Instance
		{
			get
			{
				return CheckIfPhoneNumberIsOptedOutResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000062 RID: 98
		private static CheckIfPhoneNumberIsOptedOutResponseUnmarshaller _instance = new CheckIfPhoneNumberIsOptedOutResponseUnmarshaller();
	}
}
