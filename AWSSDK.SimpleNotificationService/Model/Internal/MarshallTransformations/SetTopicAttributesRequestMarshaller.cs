﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008A RID: 138
	public class SetTopicAttributesRequestMarshaller : IMarshaller<IRequest, SetTopicAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000341 RID: 833 RVA: 0x00008D9F File Offset: 0x00006F9F
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetTopicAttributesRequest)input);
		}

		// Token: 0x06000342 RID: 834 RVA: 0x00008DB0 File Offset: 0x00006FB0
		public IRequest Marshall(SetTopicAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "SetTopicAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributeName())
				{
					request.Parameters.Add("AttributeName", StringUtils.FromString(publicRequest.AttributeName));
				}
				if (publicRequest.IsSetAttributeValue())
				{
					request.Parameters.Add("AttributeValue", StringUtils.FromString(publicRequest.AttributeValue));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
