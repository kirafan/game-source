﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000057 RID: 87
	public class ConfirmSubscriptionResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000240 RID: 576 RVA: 0x00004824 File Offset: 0x00002A24
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ConfirmSubscriptionResponse confirmSubscriptionResponse = new ConfirmSubscriptionResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ConfirmSubscriptionResult", 2))
					{
						ConfirmSubscriptionResponseUnmarshaller.UnmarshallResult(context, confirmSubscriptionResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						confirmSubscriptionResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return confirmSubscriptionResponse;
		}

		// Token: 0x06000241 RID: 577 RVA: 0x00004890 File Offset: 0x00002A90
		private static void UnmarshallResult(XmlUnmarshallerContext context, ConfirmSubscriptionResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("SubscriptionArn", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.SubscriptionArn = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x06000242 RID: 578 RVA: 0x000048F0 File Offset: 0x00002AF0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("SubscriptionLimitExceeded"))
			{
				return new SubscriptionLimitExceededException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00004A4A File Offset: 0x00002C4A
		internal static ConfirmSubscriptionResponseUnmarshaller GetInstance()
		{
			return ConfirmSubscriptionResponseUnmarshaller._instance;
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000244 RID: 580 RVA: 0x00004A4A File Offset: 0x00002C4A
		public static ConfirmSubscriptionResponseUnmarshaller Instance
		{
			get
			{
				return ConfirmSubscriptionResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000063 RID: 99
		private static ConfirmSubscriptionResponseUnmarshaller _instance = new ConfirmSubscriptionResponseUnmarshaller();
	}
}
