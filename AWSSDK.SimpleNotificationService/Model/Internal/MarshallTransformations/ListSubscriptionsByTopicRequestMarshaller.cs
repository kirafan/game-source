﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000075 RID: 117
	public class ListSubscriptionsByTopicRequestMarshaller : IMarshaller<IRequest, ListSubscriptionsByTopicRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002D8 RID: 728 RVA: 0x00006FAD File Offset: 0x000051AD
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListSubscriptionsByTopicRequest)input);
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x00006FBC File Offset: 0x000051BC
		public IRequest Marshall(ListSubscriptionsByTopicRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListSubscriptionsByTopic");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetNextToken())
				{
					request.Parameters.Add("NextToken", StringUtils.FromString(publicRequest.NextToken));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
