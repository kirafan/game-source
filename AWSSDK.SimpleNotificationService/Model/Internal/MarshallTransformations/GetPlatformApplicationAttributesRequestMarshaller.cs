﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000067 RID: 103
	public class GetPlatformApplicationAttributesRequestMarshaller : IMarshaller<IRequest, GetPlatformApplicationAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000292 RID: 658 RVA: 0x00005D5B File Offset: 0x00003F5B
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetPlatformApplicationAttributesRequest)input);
		}

		// Token: 0x06000293 RID: 659 RVA: 0x00005D6C File Offset: 0x00003F6C
		public IRequest Marshall(GetPlatformApplicationAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "GetPlatformApplicationAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetPlatformApplicationArn())
			{
				request.Parameters.Add("PlatformApplicationArn", StringUtils.FromString(publicRequest.PlatformApplicationArn));
			}
			return request;
		}
	}
}
