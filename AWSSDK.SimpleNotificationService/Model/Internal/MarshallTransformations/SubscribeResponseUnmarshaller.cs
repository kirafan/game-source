﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008D RID: 141
	public class SubscribeResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600034E RID: 846 RVA: 0x00009100 File Offset: 0x00007300
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SubscribeResponse subscribeResponse = new SubscribeResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SubscribeResult", 2))
					{
						SubscribeResponseUnmarshaller.UnmarshallResult(context, subscribeResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						subscribeResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return subscribeResponse;
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000916C File Offset: 0x0000736C
		private static void UnmarshallResult(XmlUnmarshallerContext context, SubscribeResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("SubscriptionArn", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.SubscriptionArn = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x06000350 RID: 848 RVA: 0x000091CC File Offset: 0x000073CC
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("SubscriptionLimitExceeded"))
			{
				return new SubscriptionLimitExceededException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x00009326 File Offset: 0x00007526
		internal static SubscribeResponseUnmarshaller GetInstance()
		{
			return SubscribeResponseUnmarshaller._instance;
		}

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000352 RID: 850 RVA: 0x00009326 File Offset: 0x00007526
		public static SubscribeResponseUnmarshaller Instance
		{
			get
			{
				return SubscribeResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007F RID: 127
		private static SubscribeResponseUnmarshaller _instance = new SubscribeResponseUnmarshaller();
	}
}
