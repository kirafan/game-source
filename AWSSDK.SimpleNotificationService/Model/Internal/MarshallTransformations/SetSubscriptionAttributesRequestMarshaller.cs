﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000088 RID: 136
	public class SetSubscriptionAttributesRequestMarshaller : IMarshaller<IRequest, SetSubscriptionAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000337 RID: 823 RVA: 0x00008AFF File Offset: 0x00006CFF
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetSubscriptionAttributesRequest)input);
		}

		// Token: 0x06000338 RID: 824 RVA: 0x00008B10 File Offset: 0x00006D10
		public IRequest Marshall(SetSubscriptionAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "SetSubscriptionAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributeName())
				{
					request.Parameters.Add("AttributeName", StringUtils.FromString(publicRequest.AttributeName));
				}
				if (publicRequest.IsSetAttributeValue())
				{
					request.Parameters.Add("AttributeValue", StringUtils.FromString(publicRequest.AttributeValue));
				}
				if (publicRequest.IsSetSubscriptionArn())
				{
					request.Parameters.Add("SubscriptionArn", StringUtils.FromString(publicRequest.SubscriptionArn));
				}
			}
			return request;
		}
	}
}
