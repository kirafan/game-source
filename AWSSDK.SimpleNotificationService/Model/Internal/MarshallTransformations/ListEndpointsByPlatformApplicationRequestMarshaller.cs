﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006F RID: 111
	public class ListEndpointsByPlatformApplicationRequestMarshaller : IMarshaller<IRequest, ListEndpointsByPlatformApplicationRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002BA RID: 698 RVA: 0x000067D7 File Offset: 0x000049D7
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListEndpointsByPlatformApplicationRequest)input);
		}

		// Token: 0x060002BB RID: 699 RVA: 0x000067E8 File Offset: 0x000049E8
		public IRequest Marshall(ListEndpointsByPlatformApplicationRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListEndpointsByPlatformApplication");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetNextToken())
				{
					request.Parameters.Add("NextToken", StringUtils.FromString(publicRequest.NextToken));
				}
				if (publicRequest.IsSetPlatformApplicationArn())
				{
					request.Parameters.Add("PlatformApplicationArn", StringUtils.FromString(publicRequest.PlatformApplicationArn));
				}
			}
			return request;
		}
	}
}
