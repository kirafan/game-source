﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000083 RID: 131
	public class SetEndpointAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600031C RID: 796 RVA: 0x00008324 File Offset: 0x00006524
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SetEndpointAttributesResponse setEndpointAttributesResponse = new SetEndpointAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SetEndpointAttributesResult", 2))
					{
						SetEndpointAttributesResponseUnmarshaller.UnmarshallResult(context, setEndpointAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						setEndpointAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return setEndpointAttributesResponse;
		}

		// Token: 0x0600031D RID: 797 RVA: 0x00008390 File Offset: 0x00006590
		private static void UnmarshallResult(XmlUnmarshallerContext context, SetEndpointAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x0600031E RID: 798 RVA: 0x000083D0 File Offset: 0x000065D0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600031F RID: 799 RVA: 0x000084F0 File Offset: 0x000066F0
		internal static SetEndpointAttributesResponseUnmarshaller GetInstance()
		{
			return SetEndpointAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000320 RID: 800 RVA: 0x000084F0 File Offset: 0x000066F0
		public static SetEndpointAttributesResponseUnmarshaller Instance
		{
			get
			{
				return SetEndpointAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007A RID: 122
		private static SetEndpointAttributesResponseUnmarshaller _instance = new SetEndpointAttributesResponseUnmarshaller();
	}
}
