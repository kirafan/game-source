﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000082 RID: 130
	public class SetEndpointAttributesRequestMarshaller : IMarshaller<IRequest, SetEndpointAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000319 RID: 793 RVA: 0x000081F3 File Offset: 0x000063F3
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetEndpointAttributesRequest)input);
		}

		// Token: 0x0600031A RID: 794 RVA: 0x00008204 File Offset: 0x00006404
		public IRequest Marshall(SetEndpointAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "SetEndpointAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAttributes())
				{
					int num = 1;
					foreach (string text in publicRequest.Attributes.Keys)
					{
						string value;
						bool flag = publicRequest.Attributes.TryGetValue(text, out value);
						request.Parameters.Add("Attributes.entry." + num + ".key", StringUtils.FromString(text));
						if (flag)
						{
							request.Parameters.Add("Attributes.entry." + num + ".value", StringUtils.FromString(value));
						}
						num++;
					}
				}
				if (publicRequest.IsSetEndpointArn())
				{
					request.Parameters.Add("EndpointArn", StringUtils.FromString(publicRequest.EndpointArn));
				}
			}
			return request;
		}
	}
}
