﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000087 RID: 135
	public class SetSMSAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000330 RID: 816 RVA: 0x00008920 File Offset: 0x00006B20
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SetSMSAttributesResponse setSMSAttributesResponse = new SetSMSAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SetSMSAttributesResult", 2))
					{
						SetSMSAttributesResponseUnmarshaller.UnmarshallResult(context, setSMSAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						setSMSAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return setSMSAttributesResponse;
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000898C File Offset: 0x00006B8C
		private static void UnmarshallResult(XmlUnmarshallerContext context, SetSMSAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000332 RID: 818 RVA: 0x000089CC File Offset: 0x00006BCC
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("Throttled"))
			{
				return new ThrottledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000333 RID: 819 RVA: 0x00008AEC File Offset: 0x00006CEC
		internal static SetSMSAttributesResponseUnmarshaller GetInstance()
		{
			return SetSMSAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x06000334 RID: 820 RVA: 0x00008AEC File Offset: 0x00006CEC
		public static SetSMSAttributesResponseUnmarshaller Instance
		{
			get
			{
				return SetSMSAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007C RID: 124
		private static SetSMSAttributesResponseUnmarshaller _instance = new SetSMSAttributesResponseUnmarshaller();
	}
}
