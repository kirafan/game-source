﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000073 RID: 115
	public class ListPlatformApplicationsRequestMarshaller : IMarshaller<IRequest, ListPlatformApplicationsRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002CE RID: 718 RVA: 0x00006D43 File Offset: 0x00004F43
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListPlatformApplicationsRequest)input);
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00006D54 File Offset: 0x00004F54
		public IRequest Marshall(ListPlatformApplicationsRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListPlatformApplications");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetNextToken())
			{
				request.Parameters.Add("NextToken", StringUtils.FromString(publicRequest.NextToken));
			}
			return request;
		}
	}
}
