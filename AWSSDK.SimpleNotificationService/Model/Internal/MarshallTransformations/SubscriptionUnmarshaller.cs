﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008E RID: 142
	public class SubscriptionUnmarshaller : IUnmarshaller<Subscription, XmlUnmarshallerContext>, IUnmarshaller<Subscription, JsonUnmarshallerContext>
	{
		// Token: 0x06000355 RID: 853 RVA: 0x0000933C File Offset: 0x0000753C
		public Subscription Unmarshall(XmlUnmarshallerContext context)
		{
			Subscription subscription = new Subscription();
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("Endpoint", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						subscription.Endpoint = instance.Unmarshall(context);
					}
					else if (context.TestExpression("Owner", num))
					{
						StringUnmarshaller instance2 = StringUnmarshaller.Instance;
						subscription.Owner = instance2.Unmarshall(context);
					}
					else if (context.TestExpression("Protocol", num))
					{
						StringUnmarshaller instance3 = StringUnmarshaller.Instance;
						subscription.Protocol = instance3.Unmarshall(context);
					}
					else if (context.TestExpression("SubscriptionArn", num))
					{
						StringUnmarshaller instance4 = StringUnmarshaller.Instance;
						subscription.SubscriptionArn = instance4.Unmarshall(context);
					}
					else if (context.TestExpression("TopicArn", num))
					{
						StringUnmarshaller instance5 = StringUnmarshaller.Instance;
						subscription.TopicArn = instance5.Unmarshall(context);
					}
				}
				else if (context.IsEndElement && context.CurrentDepth < currentDepth)
				{
					return subscription;
				}
			}
			return subscription;
		}

		// Token: 0x06000356 RID: 854 RVA: 0x00005ABA File Offset: 0x00003CBA
		public Subscription Unmarshall(JsonUnmarshallerContext context)
		{
			return null;
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000357 RID: 855 RVA: 0x0000945E File Offset: 0x0000765E
		public static SubscriptionUnmarshaller Instance
		{
			get
			{
				return SubscriptionUnmarshaller._instance;
			}
		}

		// Token: 0x04000080 RID: 128
		private static SubscriptionUnmarshaller _instance = new SubscriptionUnmarshaller();
	}
}
