﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005F RID: 95
	public class DeleteEndpointResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000268 RID: 616 RVA: 0x000053E4 File Offset: 0x000035E4
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			DeleteEndpointResponse deleteEndpointResponse = new DeleteEndpointResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("DeleteEndpointResult", 2))
					{
						DeleteEndpointResponseUnmarshaller.UnmarshallResult(context, deleteEndpointResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						deleteEndpointResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return deleteEndpointResponse;
		}

		// Token: 0x06000269 RID: 617 RVA: 0x00005450 File Offset: 0x00003650
		private static void UnmarshallResult(XmlUnmarshallerContext context, DeleteEndpointResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00005490 File Offset: 0x00003690
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600026B RID: 619 RVA: 0x00005576 File Offset: 0x00003776
		internal static DeleteEndpointResponseUnmarshaller GetInstance()
		{
			return DeleteEndpointResponseUnmarshaller._instance;
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600026C RID: 620 RVA: 0x00005576 File Offset: 0x00003776
		public static DeleteEndpointResponseUnmarshaller Instance
		{
			get
			{
				return DeleteEndpointResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000067 RID: 103
		private static DeleteEndpointResponseUnmarshaller _instance = new DeleteEndpointResponseUnmarshaller();
	}
}
