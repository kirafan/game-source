﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000085 RID: 133
	public class SetPlatformApplicationAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000326 RID: 806 RVA: 0x00008634 File Offset: 0x00006834
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			SetPlatformApplicationAttributesResponse setPlatformApplicationAttributesResponse = new SetPlatformApplicationAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("SetPlatformApplicationAttributesResult", 2))
					{
						SetPlatformApplicationAttributesResponseUnmarshaller.UnmarshallResult(context, setPlatformApplicationAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						setPlatformApplicationAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return setPlatformApplicationAttributesResponse;
		}

		// Token: 0x06000327 RID: 807 RVA: 0x000086A0 File Offset: 0x000068A0
		private static void UnmarshallResult(XmlUnmarshallerContext context, SetPlatformApplicationAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000328 RID: 808 RVA: 0x000086E0 File Offset: 0x000068E0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000329 RID: 809 RVA: 0x00008800 File Offset: 0x00006A00
		internal static SetPlatformApplicationAttributesResponseUnmarshaller GetInstance()
		{
			return SetPlatformApplicationAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600032A RID: 810 RVA: 0x00008800 File Offset: 0x00006A00
		public static SetPlatformApplicationAttributesResponseUnmarshaller Instance
		{
			get
			{
				return SetPlatformApplicationAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400007B RID: 123
		private static SetPlatformApplicationAttributesResponseUnmarshaller _instance = new SetPlatformApplicationAttributesResponseUnmarshaller();
	}
}
