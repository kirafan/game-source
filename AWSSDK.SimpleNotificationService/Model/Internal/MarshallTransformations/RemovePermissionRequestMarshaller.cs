﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000080 RID: 128
	public class RemovePermissionRequestMarshaller : IMarshaller<IRequest, RemovePermissionRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600030F RID: 783 RVA: 0x00007F75 File Offset: 0x00006175
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((RemovePermissionRequest)input);
		}

		// Token: 0x06000310 RID: 784 RVA: 0x00007F84 File Offset: 0x00006184
		public IRequest Marshall(RemovePermissionRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "RemovePermission");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetLabel())
				{
					request.Parameters.Add("Label", StringUtils.FromString(publicRequest.Label));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
