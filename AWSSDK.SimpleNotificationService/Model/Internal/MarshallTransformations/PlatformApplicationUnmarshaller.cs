﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007D RID: 125
	public class PlatformApplicationUnmarshaller : IUnmarshaller<PlatformApplication, XmlUnmarshallerContext>, IUnmarshaller<PlatformApplication, JsonUnmarshallerContext>
	{
		// Token: 0x06000300 RID: 768 RVA: 0x000079A0 File Offset: 0x00005BA0
		public PlatformApplication Unmarshall(XmlUnmarshallerContext context)
		{
			PlatformApplication platformApplication = new PlatformApplication();
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("Attributes/entry", num))
					{
						KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
						platformApplication.Attributes.Add(item);
					}
					else if (context.TestExpression("PlatformApplicationArn", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						platformApplication.PlatformApplicationArn = instance.Unmarshall(context);
					}
				}
				else if (context.IsEndElement && context.CurrentDepth < currentDepth)
				{
					return platformApplication;
				}
			}
			return platformApplication;
		}

		// Token: 0x06000301 RID: 769 RVA: 0x00005ABA File Offset: 0x00003CBA
		public PlatformApplication Unmarshall(JsonUnmarshallerContext context)
		{
			return null;
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000302 RID: 770 RVA: 0x00007A56 File Offset: 0x00005C56
		public static PlatformApplicationUnmarshaller Instance
		{
			get
			{
				return PlatformApplicationUnmarshaller._instance;
			}
		}

		// Token: 0x04000077 RID: 119
		private static PlatformApplicationUnmarshaller _instance = new PlatformApplicationUnmarshaller();
	}
}
