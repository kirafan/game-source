﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200007E RID: 126
	public class PublishRequestMarshaller : IMarshaller<IRequest, PublishRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000305 RID: 773 RVA: 0x00007A69 File Offset: 0x00005C69
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((PublishRequest)input);
		}

		// Token: 0x06000306 RID: 774 RVA: 0x00007A78 File Offset: 0x00005C78
		public IRequest Marshall(PublishRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "Publish");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetMessage())
				{
					request.Parameters.Add("Message", StringUtils.FromString(publicRequest.Message));
				}
				if (publicRequest.IsSetMessageAttributes())
				{
					int num = 1;
					foreach (string text in publicRequest.MessageAttributes.Keys)
					{
						MessageAttributeValue messageAttributeValue;
						bool flag = publicRequest.MessageAttributes.TryGetValue(text, out messageAttributeValue);
						request.Parameters.Add("MessageAttributes.entry." + num + ".Name", StringUtils.FromString(text));
						if (flag)
						{
							if (messageAttributeValue.IsSetBinaryValue())
							{
								request.Parameters.Add("MessageAttributes.entry." + num + ".Value.BinaryValue", StringUtils.FromMemoryStream(messageAttributeValue.BinaryValue));
							}
							if (messageAttributeValue.IsSetDataType())
							{
								request.Parameters.Add("MessageAttributes.entry." + num + ".Value.DataType", StringUtils.FromString(messageAttributeValue.DataType));
							}
							if (messageAttributeValue.IsSetStringValue())
							{
								request.Parameters.Add("MessageAttributes.entry." + num + ".Value.StringValue", StringUtils.FromString(messageAttributeValue.StringValue));
							}
						}
						num++;
					}
				}
				if (publicRequest.IsSetMessageStructure())
				{
					request.Parameters.Add("MessageStructure", StringUtils.FromString(publicRequest.MessageStructure));
				}
				if (publicRequest.IsSetPhoneNumber())
				{
					request.Parameters.Add("PhoneNumber", StringUtils.FromString(publicRequest.PhoneNumber));
				}
				if (publicRequest.IsSetSubject())
				{
					request.Parameters.Add("Subject", StringUtils.FromString(publicRequest.Subject));
				}
				if (publicRequest.IsSetTargetArn())
				{
					request.Parameters.Add("TargetArn", StringUtils.FromString(publicRequest.TargetArn));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
