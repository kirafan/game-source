﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000078 RID: 120
	public class ListSubscriptionsResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002E5 RID: 741 RVA: 0x000072F0 File Offset: 0x000054F0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListSubscriptionsResponse listSubscriptionsResponse = new ListSubscriptionsResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListSubscriptionsResult", 2))
					{
						ListSubscriptionsResponseUnmarshaller.UnmarshallResult(context, listSubscriptionsResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listSubscriptionsResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listSubscriptionsResponse;
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000735C File Offset: 0x0000555C
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListSubscriptionsResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("NextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
					else if (context.TestExpression("Subscriptions/member", num))
					{
						Subscription item = SubscriptionUnmarshaller.Instance.Unmarshall(context);
						response.Subscriptions.Add(item);
					}
				}
			}
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x000073E4 File Offset: 0x000055E4
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x000074CA File Offset: 0x000056CA
		internal static ListSubscriptionsResponseUnmarshaller GetInstance()
		{
			return ListSubscriptionsResponseUnmarshaller._instance;
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060002E9 RID: 745 RVA: 0x000074CA File Offset: 0x000056CA
		public static ListSubscriptionsResponseUnmarshaller Instance
		{
			get
			{
				return ListSubscriptionsResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000074 RID: 116
		private static ListSubscriptionsResponseUnmarshaller _instance = new ListSubscriptionsResponseUnmarshaller();
	}
}
