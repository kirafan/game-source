﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000054 RID: 84
	public class CheckIfPhoneNumberIsOptedOutRequestMarshaller : IMarshaller<IRequest, CheckIfPhoneNumberIsOptedOutRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000233 RID: 563 RVA: 0x000044E7 File Offset: 0x000026E7
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((CheckIfPhoneNumberIsOptedOutRequest)input);
		}

		// Token: 0x06000234 RID: 564 RVA: 0x000044F8 File Offset: 0x000026F8
		public IRequest Marshall(CheckIfPhoneNumberIsOptedOutRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "CheckIfPhoneNumberIsOptedOut");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetPhoneNumber())
			{
				request.Parameters.Add("phoneNumber", StringUtils.FromString(publicRequest.PhoneNumber));
			}
			return request;
		}
	}
}
