﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008C RID: 140
	public class SubscribeRequestMarshaller : IMarshaller<IRequest, SubscribeRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600034B RID: 843 RVA: 0x0000903F File Offset: 0x0000723F
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SubscribeRequest)input);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x00009050 File Offset: 0x00007250
		public IRequest Marshall(SubscribeRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "Subscribe");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetEndpoint())
				{
					request.Parameters.Add("Endpoint", StringUtils.FromString(publicRequest.Endpoint));
				}
				if (publicRequest.IsSetProtocol())
				{
					request.Parameters.Add("Protocol", StringUtils.FromString(publicRequest.Protocol));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
