﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000063 RID: 99
	public class DeleteTopicResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600027C RID: 636 RVA: 0x00005824 File Offset: 0x00003A24
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			DeleteTopicResponse deleteTopicResponse = new DeleteTopicResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("DeleteTopicResult", 2))
					{
						DeleteTopicResponseUnmarshaller.UnmarshallResult(context, deleteTopicResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						deleteTopicResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return deleteTopicResponse;
		}

		// Token: 0x0600027D RID: 637 RVA: 0x00005890 File Offset: 0x00003A90
		private static void UnmarshallResult(XmlUnmarshallerContext context, DeleteTopicResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x0600027E RID: 638 RVA: 0x000058D0 File Offset: 0x00003AD0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600027F RID: 639 RVA: 0x000059F0 File Offset: 0x00003BF0
		internal static DeleteTopicResponseUnmarshaller GetInstance()
		{
			return DeleteTopicResponseUnmarshaller._instance;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000280 RID: 640 RVA: 0x000059F0 File Offset: 0x00003BF0
		public static DeleteTopicResponseUnmarshaller Instance
		{
			get
			{
				return DeleteTopicResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000069 RID: 105
		private static DeleteTopicResponseUnmarshaller _instance = new DeleteTopicResponseUnmarshaller();
	}
}
