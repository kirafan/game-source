﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200008F RID: 143
	public class TopicUnmarshaller : IUnmarshaller<Topic, XmlUnmarshallerContext>, IUnmarshaller<Topic, JsonUnmarshallerContext>
	{
		// Token: 0x0600035A RID: 858 RVA: 0x00009474 File Offset: 0x00007674
		public Topic Unmarshall(XmlUnmarshallerContext context)
		{
			Topic topic = new Topic();
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("TopicArn", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						topic.TopicArn = instance.Unmarshall(context);
					}
				}
				else if (context.IsEndElement && context.CurrentDepth < currentDepth)
				{
					return topic;
				}
			}
			return topic;
		}

		// Token: 0x0600035B RID: 859 RVA: 0x00005ABA File Offset: 0x00003CBA
		public Topic Unmarshall(JsonUnmarshallerContext context)
		{
			return null;
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600035C RID: 860 RVA: 0x000094F0 File Offset: 0x000076F0
		public static TopicUnmarshaller Instance
		{
			get
			{
				return TopicUnmarshaller._instance;
			}
		}

		// Token: 0x04000081 RID: 129
		private static TopicUnmarshaller _instance = new TopicUnmarshaller();
	}
}
