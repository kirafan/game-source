﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000060 RID: 96
	public class DeletePlatformApplicationRequestMarshaller : IMarshaller<IRequest, DeletePlatformApplicationRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600026F RID: 623 RVA: 0x00005589 File Offset: 0x00003789
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DeletePlatformApplicationRequest)input);
		}

		// Token: 0x06000270 RID: 624 RVA: 0x00005598 File Offset: 0x00003798
		public IRequest Marshall(DeletePlatformApplicationRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "DeletePlatformApplication");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetPlatformApplicationArn())
			{
				request.Parameters.Add("PlatformApplicationArn", StringUtils.FromString(publicRequest.PlatformApplicationArn));
			}
			return request;
		}
	}
}
