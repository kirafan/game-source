﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000072 RID: 114
	public class ListPhoneNumbersOptedOutResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002C7 RID: 711 RVA: 0x00006B1C File Offset: 0x00004D1C
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListPhoneNumbersOptedOutResponse listPhoneNumbersOptedOutResponse = new ListPhoneNumbersOptedOutResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListPhoneNumbersOptedOutResult", 2))
					{
						ListPhoneNumbersOptedOutResponseUnmarshaller.UnmarshallResult(context, listPhoneNumbersOptedOutResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listPhoneNumbersOptedOutResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listPhoneNumbersOptedOutResponse;
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x00006B88 File Offset: 0x00004D88
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListPhoneNumbersOptedOutResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("nextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
					else if (context.TestExpression("phoneNumbers/member", num))
					{
						string item = StringUnmarshaller.Instance.Unmarshall(context);
						response.PhoneNumbers.Add(item);
					}
				}
			}
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x00006C10 File Offset: 0x00004E10
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("Throttled"))
			{
				return new ThrottledException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002CA RID: 714 RVA: 0x00006D30 File Offset: 0x00004F30
		internal static ListPhoneNumbersOptedOutResponseUnmarshaller GetInstance()
		{
			return ListPhoneNumbersOptedOutResponseUnmarshaller._instance;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060002CB RID: 715 RVA: 0x00006D30 File Offset: 0x00004F30
		public static ListPhoneNumbersOptedOutResponseUnmarshaller Instance
		{
			get
			{
				return ListPhoneNumbersOptedOutResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000071 RID: 113
		private static ListPhoneNumbersOptedOutResponseUnmarshaller _instance = new ListPhoneNumbersOptedOutResponseUnmarshaller();
	}
}
