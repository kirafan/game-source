﻿using System;
using System.Collections.Generic;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000068 RID: 104
	public class GetPlatformApplicationAttributesResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000295 RID: 661 RVA: 0x00005DD8 File Offset: 0x00003FD8
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			GetPlatformApplicationAttributesResponse getPlatformApplicationAttributesResponse = new GetPlatformApplicationAttributesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("GetPlatformApplicationAttributesResult", 2))
					{
						GetPlatformApplicationAttributesResponseUnmarshaller.UnmarshallResult(context, getPlatformApplicationAttributesResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						getPlatformApplicationAttributesResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return getPlatformApplicationAttributesResponse;
		}

		// Token: 0x06000296 RID: 662 RVA: 0x00005E44 File Offset: 0x00004044
		private static void UnmarshallResult(XmlUnmarshallerContext context, GetPlatformApplicationAttributesResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("Attributes/entry", num))
				{
					KeyValuePair<string, string> item = new KeyValueUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance).Unmarshall(context);
					response.Attributes.Add(item);
				}
			}
		}

		// Token: 0x06000297 RID: 663 RVA: 0x00005EB4 File Offset: 0x000040B4
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000298 RID: 664 RVA: 0x00005FD4 File Offset: 0x000041D4
		internal static GetPlatformApplicationAttributesResponseUnmarshaller GetInstance()
		{
			return GetPlatformApplicationAttributesResponseUnmarshaller._instance;
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x06000299 RID: 665 RVA: 0x00005FD4 File Offset: 0x000041D4
		public static GetPlatformApplicationAttributesResponseUnmarshaller Instance
		{
			get
			{
				return GetPlatformApplicationAttributesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x0400006C RID: 108
		private static GetPlatformApplicationAttributesResponseUnmarshaller _instance = new GetPlatformApplicationAttributesResponseUnmarshaller();
	}
}
