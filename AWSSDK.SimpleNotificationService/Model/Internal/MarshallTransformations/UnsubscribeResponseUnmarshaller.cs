﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000091 RID: 145
	public class UnsubscribeResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000362 RID: 866 RVA: 0x00009580 File Offset: 0x00007780
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			UnsubscribeResponse unsubscribeResponse = new UnsubscribeResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("UnsubscribeResult", 2))
					{
						UnsubscribeResponseUnmarshaller.UnmarshallResult(context, unsubscribeResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						unsubscribeResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return unsubscribeResponse;
		}

		// Token: 0x06000363 RID: 867 RVA: 0x000095EC File Offset: 0x000077EC
		private static void UnmarshallResult(XmlUnmarshallerContext context, UnsubscribeResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000962C File Offset: 0x0000782C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000974C File Offset: 0x0000794C
		internal static UnsubscribeResponseUnmarshaller GetInstance()
		{
			return UnsubscribeResponseUnmarshaller._instance;
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000366 RID: 870 RVA: 0x0000974C File Offset: 0x0000794C
		public static UnsubscribeResponseUnmarshaller Instance
		{
			get
			{
				return UnsubscribeResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000082 RID: 130
		private static UnsubscribeResponseUnmarshaller _instance = new UnsubscribeResponseUnmarshaller();
	}
}
