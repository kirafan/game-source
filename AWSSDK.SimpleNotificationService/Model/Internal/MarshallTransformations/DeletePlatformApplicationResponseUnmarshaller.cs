﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000061 RID: 97
	public class DeletePlatformApplicationResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000272 RID: 626 RVA: 0x00005604 File Offset: 0x00003804
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			DeletePlatformApplicationResponse deletePlatformApplicationResponse = new DeletePlatformApplicationResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("DeletePlatformApplicationResult", 2))
					{
						DeletePlatformApplicationResponseUnmarshaller.UnmarshallResult(context, deletePlatformApplicationResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						deletePlatformApplicationResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return deletePlatformApplicationResponse;
		}

		// Token: 0x06000273 RID: 627 RVA: 0x00005670 File Offset: 0x00003870
		private static void UnmarshallResult(XmlUnmarshallerContext context, DeletePlatformApplicationResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x06000274 RID: 628 RVA: 0x000056B0 File Offset: 0x000038B0
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000275 RID: 629 RVA: 0x00005796 File Offset: 0x00003996
		internal static DeletePlatformApplicationResponseUnmarshaller GetInstance()
		{
			return DeletePlatformApplicationResponseUnmarshaller._instance;
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000276 RID: 630 RVA: 0x00005796 File Offset: 0x00003996
		public static DeletePlatformApplicationResponseUnmarshaller Instance
		{
			get
			{
				return DeletePlatformApplicationResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000068 RID: 104
		private static DeletePlatformApplicationResponseUnmarshaller _instance = new DeletePlatformApplicationResponseUnmarshaller();
	}
}
