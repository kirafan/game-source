﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000070 RID: 112
	public class ListEndpointsByPlatformApplicationResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002BD RID: 701 RVA: 0x00006878 File Offset: 0x00004A78
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListEndpointsByPlatformApplicationResponse listEndpointsByPlatformApplicationResponse = new ListEndpointsByPlatformApplicationResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListEndpointsByPlatformApplicationResult", 2))
					{
						ListEndpointsByPlatformApplicationResponseUnmarshaller.UnmarshallResult(context, listEndpointsByPlatformApplicationResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listEndpointsByPlatformApplicationResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listEndpointsByPlatformApplicationResponse;
		}

		// Token: 0x060002BE RID: 702 RVA: 0x000068E4 File Offset: 0x00004AE4
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListEndpointsByPlatformApplicationResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("Endpoints/member", num))
					{
						Endpoint item = EndpointUnmarshaller.Instance.Unmarshall(context);
						response.Endpoints.Add(item);
					}
					else if (context.TestExpression("NextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
				}
			}
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000696C File Offset: 0x00004B6C
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x00006A8C File Offset: 0x00004C8C
		internal static ListEndpointsByPlatformApplicationResponseUnmarshaller GetInstance()
		{
			return ListEndpointsByPlatformApplicationResponseUnmarshaller._instance;
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060002C1 RID: 705 RVA: 0x00006A8C File Offset: 0x00004C8C
		public static ListEndpointsByPlatformApplicationResponseUnmarshaller Instance
		{
			get
			{
				return ListEndpointsByPlatformApplicationResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000070 RID: 112
		private static ListEndpointsByPlatformApplicationResponseUnmarshaller _instance = new ListEndpointsByPlatformApplicationResponseUnmarshaller();
	}
}
