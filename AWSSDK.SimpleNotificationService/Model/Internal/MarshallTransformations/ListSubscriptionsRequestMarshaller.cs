﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000077 RID: 119
	public class ListSubscriptionsRequestMarshaller : IMarshaller<IRequest, ListSubscriptionsRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002E2 RID: 738 RVA: 0x00007273 File Offset: 0x00005473
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ListSubscriptionsRequest)input);
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x00007284 File Offset: 0x00005484
		public IRequest Marshall(ListSubscriptionsRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ListSubscriptions");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetNextToken())
			{
				request.Parameters.Add("NextToken", StringUtils.FromString(publicRequest.NextToken));
			}
			return request;
		}
	}
}
