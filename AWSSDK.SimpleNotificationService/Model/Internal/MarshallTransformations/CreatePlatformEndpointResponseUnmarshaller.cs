﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005B RID: 91
	public class CreatePlatformEndpointResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000254 RID: 596 RVA: 0x00004EEC File Offset: 0x000030EC
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			CreatePlatformEndpointResponse createPlatformEndpointResponse = new CreatePlatformEndpointResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("CreatePlatformEndpointResult", 2))
					{
						CreatePlatformEndpointResponseUnmarshaller.UnmarshallResult(context, createPlatformEndpointResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						createPlatformEndpointResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return createPlatformEndpointResponse;
		}

		// Token: 0x06000255 RID: 597 RVA: 0x00004F58 File Offset: 0x00003158
		private static void UnmarshallResult(XmlUnmarshallerContext context, CreatePlatformEndpointResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if ((context.IsStartElement || context.IsAttribute) && context.TestExpression("EndpointArn", num))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					response.EndpointArn = instance.Unmarshall(context);
				}
			}
		}

		// Token: 0x06000256 RID: 598 RVA: 0x00004FB8 File Offset: 0x000031B8
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000257 RID: 599 RVA: 0x000050D8 File Offset: 0x000032D8
		internal static CreatePlatformEndpointResponseUnmarshaller GetInstance()
		{
			return CreatePlatformEndpointResponseUnmarshaller._instance;
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000258 RID: 600 RVA: 0x000050D8 File Offset: 0x000032D8
		public static CreatePlatformEndpointResponseUnmarshaller Instance
		{
			get
			{
				return CreatePlatformEndpointResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000065 RID: 101
		private static CreatePlatformEndpointResponseUnmarshaller _instance = new CreatePlatformEndpointResponseUnmarshaller();
	}
}
