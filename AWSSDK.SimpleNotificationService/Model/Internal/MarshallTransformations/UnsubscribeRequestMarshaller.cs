﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000090 RID: 144
	public class UnsubscribeRequestMarshaller : IMarshaller<IRequest, UnsubscribeRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600035F RID: 863 RVA: 0x00009503 File Offset: 0x00007703
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((UnsubscribeRequest)input);
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00009514 File Offset: 0x00007714
		public IRequest Marshall(UnsubscribeRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "Unsubscribe");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetSubscriptionArn())
			{
				request.Parameters.Add("SubscriptionArn", StringUtils.FromString(publicRequest.SubscriptionArn));
			}
			return request;
		}
	}
}
