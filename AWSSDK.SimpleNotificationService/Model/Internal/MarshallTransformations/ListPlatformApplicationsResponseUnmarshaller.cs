﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000074 RID: 116
	public class ListPlatformApplicationsResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x060002D1 RID: 721 RVA: 0x00006DC0 File Offset: 0x00004FC0
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ListPlatformApplicationsResponse listPlatformApplicationsResponse = new ListPlatformApplicationsResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("ListPlatformApplicationsResult", 2))
					{
						ListPlatformApplicationsResponseUnmarshaller.UnmarshallResult(context, listPlatformApplicationsResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						listPlatformApplicationsResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return listPlatformApplicationsResponse;
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x00006E2C File Offset: 0x0000502C
		private static void UnmarshallResult(XmlUnmarshallerContext context, ListPlatformApplicationsResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement || context.IsAttribute)
				{
					if (context.TestExpression("NextToken", num))
					{
						StringUnmarshaller instance = StringUnmarshaller.Instance;
						response.NextToken = instance.Unmarshall(context);
					}
					else if (context.TestExpression("PlatformApplications/member", num))
					{
						PlatformApplication item = PlatformApplicationUnmarshaller.Instance.Unmarshall(context);
						response.PlatformApplications.Add(item);
					}
				}
			}
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x00006EB4 File Offset: 0x000050B4
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x00006F9A File Offset: 0x0000519A
		internal static ListPlatformApplicationsResponseUnmarshaller GetInstance()
		{
			return ListPlatformApplicationsResponseUnmarshaller._instance;
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060002D5 RID: 725 RVA: 0x00006F9A File Offset: 0x0000519A
		public static ListPlatformApplicationsResponseUnmarshaller Instance
		{
			get
			{
				return ListPlatformApplicationsResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000072 RID: 114
		private static ListPlatformApplicationsResponseUnmarshaller _instance = new ListPlatformApplicationsResponseUnmarshaller();
	}
}
