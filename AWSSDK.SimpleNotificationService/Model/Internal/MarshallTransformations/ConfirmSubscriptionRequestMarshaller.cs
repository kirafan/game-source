﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000056 RID: 86
	public class ConfirmSubscriptionRequestMarshaller : IMarshaller<IRequest, ConfirmSubscriptionRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600023D RID: 573 RVA: 0x00004763 File Offset: 0x00002963
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((ConfirmSubscriptionRequest)input);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00004774 File Offset: 0x00002974
		public IRequest Marshall(ConfirmSubscriptionRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "ConfirmSubscription");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null)
			{
				if (publicRequest.IsSetAuthenticateOnUnsubscribe())
				{
					request.Parameters.Add("AuthenticateOnUnsubscribe", StringUtils.FromString(publicRequest.AuthenticateOnUnsubscribe));
				}
				if (publicRequest.IsSetToken())
				{
					request.Parameters.Add("Token", StringUtils.FromString(publicRequest.Token));
				}
				if (publicRequest.IsSetTopicArn())
				{
					request.Parameters.Add("TopicArn", StringUtils.FromString(publicRequest.TopicArn));
				}
			}
			return request;
		}
	}
}
