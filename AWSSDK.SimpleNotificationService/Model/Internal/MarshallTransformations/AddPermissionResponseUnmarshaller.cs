﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000053 RID: 83
	public class AddPermissionResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x0600022C RID: 556 RVA: 0x00004300 File Offset: 0x00002500
		public override AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext context)
		{
			AddPermissionResponse addPermissionResponse = new AddPermissionResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("AddPermissionResult", 2))
					{
						AddPermissionResponseUnmarshaller.UnmarshallResult(context, addPermissionResponse);
					}
					else if (context.TestExpression("ResponseMetadata", 2))
					{
						addPermissionResponse.ResponseMetadata = ResponseMetadataUnmarshaller.Instance.Unmarshall(context);
					}
				}
			}
			return addPermissionResponse;
		}

		// Token: 0x0600022D RID: 557 RVA: 0x0000436C File Offset: 0x0000256C
		private static void UnmarshallResult(XmlUnmarshallerContext context, AddPermissionResponse response)
		{
			int currentDepth = context.CurrentDepth;
			int num = currentDepth + 1;
			if (context.IsStartOfDocument)
			{
				num += 2;
			}
			while (context.ReadAtDepth(currentDepth))
			{
				if (!context.IsStartElement)
				{
					bool isAttribute = context.IsAttribute;
				}
			}
		}

		// Token: 0x0600022E RID: 558 RVA: 0x000043AC File Offset: 0x000025AC
		public override AmazonServiceException UnmarshallException(XmlUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = ErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("AuthorizationError"))
			{
				return new AuthorizationErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalError"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameter"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotFound"))
			{
				return new NotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonSimpleNotificationServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600022F RID: 559 RVA: 0x000044CC File Offset: 0x000026CC
		internal static AddPermissionResponseUnmarshaller GetInstance()
		{
			return AddPermissionResponseUnmarshaller._instance;
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000230 RID: 560 RVA: 0x000044CC File Offset: 0x000026CC
		public static AddPermissionResponseUnmarshaller Instance
		{
			get
			{
				return AddPermissionResponseUnmarshaller._instance;
			}
		}

		// Token: 0x04000061 RID: 97
		private static AddPermissionResponseUnmarshaller _instance = new AddPermissionResponseUnmarshaller();
	}
}
