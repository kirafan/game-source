﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x02000069 RID: 105
	public class GetSMSAttributesRequestMarshaller : IMarshaller<IRequest, GetSMSAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x0600029C RID: 668 RVA: 0x00005FE7 File Offset: 0x000041E7
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetSMSAttributesRequest)input);
		}

		// Token: 0x0600029D RID: 669 RVA: 0x00005FF8 File Offset: 0x000041F8
		public IRequest Marshall(GetSMSAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "GetSMSAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetAttributes())
			{
				int num = 1;
				foreach (string value in publicRequest.Attributes)
				{
					request.Parameters.Add("attributes.member." + num, StringUtils.FromString(value));
					num++;
				}
			}
			return request;
		}
	}
}
