﻿using System;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006B RID: 107
	public class GetSubscriptionAttributesRequestMarshaller : IMarshaller<IRequest, GetSubscriptionAttributesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002A6 RID: 678 RVA: 0x000062BF File Offset: 0x000044BF
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetSubscriptionAttributesRequest)input);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x000062D0 File Offset: 0x000044D0
		public IRequest Marshall(GetSubscriptionAttributesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.SimpleNotificationService");
			request.Parameters.Add("Action", "GetSubscriptionAttributes");
			request.Parameters.Add("Version", "2010-03-31");
			if (publicRequest != null && publicRequest.IsSetSubscriptionArn())
			{
				request.Parameters.Add("SubscriptionArn", StringUtils.FromString(publicRequest.SubscriptionArn));
			}
			return request;
		}
	}
}
