﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000041 RID: 65
	public class SetPlatformApplicationAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001C9 RID: 457 RVA: 0x00003EA5 File Offset: 0x000020A5
		// (set) Token: 0x060001CA RID: 458 RVA: 0x00003EAD File Offset: 0x000020AD
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060001CB RID: 459 RVA: 0x00003EB6 File Offset: 0x000020B6
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001CC RID: 460 RVA: 0x00003ED0 File Offset: 0x000020D0
		// (set) Token: 0x060001CD RID: 461 RVA: 0x00003ED8 File Offset: 0x000020D8
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x060001CE RID: 462 RVA: 0x00003EE1 File Offset: 0x000020E1
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x0400004D RID: 77
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x0400004E RID: 78
		private string _platformApplicationArn;
	}
}
