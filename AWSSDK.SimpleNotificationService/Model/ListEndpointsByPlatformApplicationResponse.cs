﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002A RID: 42
	public class ListEndpointsByPlatformApplicationResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000135 RID: 309 RVA: 0x000038D9 File Offset: 0x00001AD9
		// (set) Token: 0x06000136 RID: 310 RVA: 0x000038E1 File Offset: 0x00001AE1
		public List<Endpoint> Endpoints
		{
			get
			{
				return this._endpoints;
			}
			set
			{
				this._endpoints = value;
			}
		}

		// Token: 0x06000137 RID: 311 RVA: 0x000038EA File Offset: 0x00001AEA
		internal bool IsSetEndpoints()
		{
			return this._endpoints != null && this._endpoints.Count > 0;
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000138 RID: 312 RVA: 0x00003904 File Offset: 0x00001B04
		// (set) Token: 0x06000139 RID: 313 RVA: 0x0000390C File Offset: 0x00001B0C
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00003915 File Offset: 0x00001B15
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000029 RID: 41
		private List<Endpoint> _endpoints = new List<Endpoint>();

		// Token: 0x0400002A RID: 42
		private string _nextToken;
	}
}
