﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002E RID: 46
	public class ListPlatformApplicationsResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600014B RID: 331 RVA: 0x000039C5 File Offset: 0x00001BC5
		// (set) Token: 0x0600014C RID: 332 RVA: 0x000039CD File Offset: 0x00001BCD
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x0600014D RID: 333 RVA: 0x000039D6 File Offset: 0x00001BD6
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600014E RID: 334 RVA: 0x000039E1 File Offset: 0x00001BE1
		// (set) Token: 0x0600014F RID: 335 RVA: 0x000039E9 File Offset: 0x00001BE9
		public List<PlatformApplication> PlatformApplications
		{
			get
			{
				return this._platformApplications;
			}
			set
			{
				this._platformApplications = value;
			}
		}

		// Token: 0x06000150 RID: 336 RVA: 0x000039F2 File Offset: 0x00001BF2
		internal bool IsSetPlatformApplications()
		{
			return this._platformApplications != null && this._platformApplications.Count > 0;
		}

		// Token: 0x0400002F RID: 47
		private string _nextToken;

		// Token: 0x04000030 RID: 48
		private List<PlatformApplication> _platformApplications = new List<PlatformApplication>();
	}
}
