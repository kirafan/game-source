﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000C RID: 12
	public class ConfirmSubscriptionRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x0600009D RID: 157 RVA: 0x00003388 File Offset: 0x00001588
		public ConfirmSubscriptionRequest()
		{
		}

		// Token: 0x0600009E RID: 158 RVA: 0x000033B8 File Offset: 0x000015B8
		public ConfirmSubscriptionRequest(string topicArn, string token)
		{
			this._topicArn = topicArn;
			this._token = token;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000033CE File Offset: 0x000015CE
		public ConfirmSubscriptionRequest(string topicArn, string token, string authenticateOnUnsubscribe)
		{
			this._topicArn = topicArn;
			this._token = token;
			this._authenticateOnUnsubscribe = authenticateOnUnsubscribe;
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x000033EB File Offset: 0x000015EB
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x000033F3 File Offset: 0x000015F3
		public string AuthenticateOnUnsubscribe
		{
			get
			{
				return this._authenticateOnUnsubscribe;
			}
			set
			{
				this._authenticateOnUnsubscribe = value;
			}
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x000033FC File Offset: 0x000015FC
		internal bool IsSetAuthenticateOnUnsubscribe()
		{
			return this._authenticateOnUnsubscribe != null;
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000A3 RID: 163 RVA: 0x00003407 File Offset: 0x00001607
		// (set) Token: 0x060000A4 RID: 164 RVA: 0x0000340F File Offset: 0x0000160F
		public string Token
		{
			get
			{
				return this._token;
			}
			set
			{
				this._token = value;
			}
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003418 File Offset: 0x00001618
		internal bool IsSetToken()
		{
			return this._token != null;
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x00003423 File Offset: 0x00001623
		// (set) Token: 0x060000A7 RID: 167 RVA: 0x0000342B File Offset: 0x0000162B
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00003434 File Offset: 0x00001634
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000009 RID: 9
		private string _authenticateOnUnsubscribe;

		// Token: 0x0400000A RID: 10
		private string _token;

		// Token: 0x0400000B RID: 11
		private string _topicArn;
	}
}
