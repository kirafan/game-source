﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000029 RID: 41
	public class ListEndpointsByPlatformApplicationRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600012E RID: 302 RVA: 0x000038A1 File Offset: 0x00001AA1
		// (set) Token: 0x0600012F RID: 303 RVA: 0x000038A9 File Offset: 0x00001AA9
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000130 RID: 304 RVA: 0x000038B2 File Offset: 0x00001AB2
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000131 RID: 305 RVA: 0x000038BD File Offset: 0x00001ABD
		// (set) Token: 0x06000132 RID: 306 RVA: 0x000038C5 File Offset: 0x00001AC5
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x06000133 RID: 307 RVA: 0x000038CE File Offset: 0x00001ACE
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x04000027 RID: 39
		private string _nextToken;

		// Token: 0x04000028 RID: 40
		private string _platformApplicationArn;
	}
}
