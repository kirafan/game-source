﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000036 RID: 54
	[Serializable]
	public class NotFoundException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x06000184 RID: 388 RVA: 0x00003326 File Offset: 0x00001526
		public NotFoundException(string message) : base(message)
		{
		}

		// Token: 0x06000185 RID: 389 RVA: 0x0000332F File Offset: 0x0000152F
		public NotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00003339 File Offset: 0x00001539
		public NotFoundException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00003342 File Offset: 0x00001542
		public NotFoundException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00003353 File Offset: 0x00001553
		public NotFoundException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00003362 File Offset: 0x00001562
		protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
