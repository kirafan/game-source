﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001B RID: 27
	[Serializable]
	public class EndpointDisabledException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x060000EC RID: 236 RVA: 0x00003326 File Offset: 0x00001526
		public EndpointDisabledException(string message) : base(message)
		{
		}

		// Token: 0x060000ED RID: 237 RVA: 0x0000332F File Offset: 0x0000152F
		public EndpointDisabledException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060000EE RID: 238 RVA: 0x00003339 File Offset: 0x00001539
		public EndpointDisabledException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060000EF RID: 239 RVA: 0x00003342 File Offset: 0x00001542
		public EndpointDisabledException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x00003353 File Offset: 0x00001553
		public EndpointDisabledException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060000F1 RID: 241 RVA: 0x00003362 File Offset: 0x00001562
		protected EndpointDisabledException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
