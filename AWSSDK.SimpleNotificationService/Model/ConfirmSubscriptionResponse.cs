﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000D RID: 13
	public class ConfirmSubscriptionResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000A9 RID: 169 RVA: 0x0000343F File Offset: 0x0000163F
		// (set) Token: 0x060000AA RID: 170 RVA: 0x00003447 File Offset: 0x00001647
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00003450 File Offset: 0x00001650
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x0400000C RID: 12
		private string _subscriptionArn;
	}
}
