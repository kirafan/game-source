﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004F RID: 79
	[Serializable]
	public class TopicLimitExceededException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x0600021D RID: 541 RVA: 0x00003326 File Offset: 0x00001526
		public TopicLimitExceededException(string message) : base(message)
		{
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000332F File Offset: 0x0000152F
		public TopicLimitExceededException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600021F RID: 543 RVA: 0x00003339 File Offset: 0x00001539
		public TopicLimitExceededException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000220 RID: 544 RVA: 0x00003342 File Offset: 0x00001542
		public TopicLimitExceededException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000221 RID: 545 RVA: 0x00003353 File Offset: 0x00001553
		public TopicLimitExceededException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000222 RID: 546 RVA: 0x00003362 File Offset: 0x00001562
		protected TopicLimitExceededException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
