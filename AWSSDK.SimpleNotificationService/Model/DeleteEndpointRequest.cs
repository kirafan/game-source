﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000014 RID: 20
	public class DeleteEndpointRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x000035E2 File Offset: 0x000017E2
		// (set) Token: 0x060000D6 RID: 214 RVA: 0x000035EA File Offset: 0x000017EA
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
			set
			{
				this._endpointArn = value;
			}
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x000035F3 File Offset: 0x000017F3
		internal bool IsSetEndpointArn()
		{
			return this._endpointArn != null;
		}

		// Token: 0x04000018 RID: 24
		private string _endpointArn;
	}
}
