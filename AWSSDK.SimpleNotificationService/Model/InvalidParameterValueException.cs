﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000028 RID: 40
	[Serializable]
	public class InvalidParameterValueException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x06000128 RID: 296 RVA: 0x00003326 File Offset: 0x00001526
		public InvalidParameterValueException(string message) : base(message)
		{
		}

		// Token: 0x06000129 RID: 297 RVA: 0x0000332F File Offset: 0x0000152F
		public InvalidParameterValueException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600012A RID: 298 RVA: 0x00003339 File Offset: 0x00001539
		public InvalidParameterValueException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00003342 File Offset: 0x00001542
		public InvalidParameterValueException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00003353 File Offset: 0x00001553
		public InvalidParameterValueException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600012D RID: 301 RVA: 0x00003362 File Offset: 0x00001562
		protected InvalidParameterValueException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
