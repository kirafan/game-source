﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200003D RID: 61
	public class RemovePermissionRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060001B8 RID: 440 RVA: 0x00003388 File Offset: 0x00001588
		public RemovePermissionRequest()
		{
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00003DFD File Offset: 0x00001FFD
		public RemovePermissionRequest(string topicArn, string label)
		{
			this._topicArn = topicArn;
			this._label = label;
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00003E13 File Offset: 0x00002013
		// (set) Token: 0x060001BB RID: 443 RVA: 0x00003E1B File Offset: 0x0000201B
		public string Label
		{
			get
			{
				return this._label;
			}
			set
			{
				this._label = value;
			}
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00003E24 File Offset: 0x00002024
		internal bool IsSetLabel()
		{
			return this._label != null;
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00003E2F File Offset: 0x0000202F
		// (set) Token: 0x060001BE RID: 446 RVA: 0x00003E37 File Offset: 0x00002037
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00003E40 File Offset: 0x00002040
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000049 RID: 73
		private string _label;

		// Token: 0x0400004A RID: 74
		private string _topicArn;
	}
}
