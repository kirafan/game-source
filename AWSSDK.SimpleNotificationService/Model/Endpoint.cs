﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001A RID: 26
	public class Endpoint
	{
		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x00003645 File Offset: 0x00001845
		// (set) Token: 0x060000E6 RID: 230 RVA: 0x0000364D File Offset: 0x0000184D
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00003656 File Offset: 0x00001856
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x00003670 File Offset: 0x00001870
		// (set) Token: 0x060000E9 RID: 233 RVA: 0x00003678 File Offset: 0x00001878
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
			set
			{
				this._endpointArn = value;
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00003681 File Offset: 0x00001881
		internal bool IsSetEndpointArn()
		{
			return this._endpointArn != null;
		}

		// Token: 0x0400001B RID: 27
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x0400001C RID: 28
		private string _endpointArn;
	}
}
