﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000030 RID: 48
	public class ListSubscriptionsByTopicResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00003A7C File Offset: 0x00001C7C
		// (set) Token: 0x0600015C RID: 348 RVA: 0x00003A84 File Offset: 0x00001C84
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00003A8D File Offset: 0x00001C8D
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600015E RID: 350 RVA: 0x00003A98 File Offset: 0x00001C98
		// (set) Token: 0x0600015F RID: 351 RVA: 0x00003AA0 File Offset: 0x00001CA0
		public List<Subscription> Subscriptions
		{
			get
			{
				return this._subscriptions;
			}
			set
			{
				this._subscriptions = value;
			}
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00003AA9 File Offset: 0x00001CA9
		internal bool IsSetSubscriptions()
		{
			return this._subscriptions != null && this._subscriptions.Count > 0;
		}

		// Token: 0x04000033 RID: 51
		private string _nextToken;

		// Token: 0x04000034 RID: 52
		private List<Subscription> _subscriptions = new List<Subscription>();
	}
}
