﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000047 RID: 71
	public class SetTopicAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060001E2 RID: 482 RVA: 0x00003388 File Offset: 0x00001588
		public SetTopicAttributesRequest()
		{
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x00003FAE File Offset: 0x000021AE
		public SetTopicAttributesRequest(string topicArn, string attributeName, string attributeValue)
		{
			this._topicArn = topicArn;
			this._attributeName = attributeName;
			this._attributeValue = attributeValue;
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00003FCB File Offset: 0x000021CB
		// (set) Token: 0x060001E5 RID: 485 RVA: 0x00003FD3 File Offset: 0x000021D3
		public string AttributeName
		{
			get
			{
				return this._attributeName;
			}
			set
			{
				this._attributeName = value;
			}
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x00003FDC File Offset: 0x000021DC
		internal bool IsSetAttributeName()
		{
			return this._attributeName != null;
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001E7 RID: 487 RVA: 0x00003FE7 File Offset: 0x000021E7
		// (set) Token: 0x060001E8 RID: 488 RVA: 0x00003FEF File Offset: 0x000021EF
		public string AttributeValue
		{
			get
			{
				return this._attributeValue;
			}
			set
			{
				this._attributeValue = value;
			}
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x00003FF8 File Offset: 0x000021F8
		internal bool IsSetAttributeValue()
		{
			return this._attributeValue != null;
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001EA RID: 490 RVA: 0x00004003 File Offset: 0x00002203
		// (set) Token: 0x060001EB RID: 491 RVA: 0x0000400B File Offset: 0x0000220B
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060001EC RID: 492 RVA: 0x00004014 File Offset: 0x00002214
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000053 RID: 83
		private string _attributeName;

		// Token: 0x04000054 RID: 84
		private string _attributeValue;

		// Token: 0x04000055 RID: 85
		private string _topicArn;
	}
}
