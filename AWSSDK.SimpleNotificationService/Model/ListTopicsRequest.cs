﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000033 RID: 51
	public class ListTopicsRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x0600016E RID: 366 RVA: 0x00003388 File Offset: 0x00001588
		public ListTopicsRequest()
		{
		}

		// Token: 0x0600016F RID: 367 RVA: 0x00003B5B File Offset: 0x00001D5B
		public ListTopicsRequest(string nextToken)
		{
			this._nextToken = nextToken;
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000170 RID: 368 RVA: 0x00003B6A File Offset: 0x00001D6A
		// (set) Token: 0x06000171 RID: 369 RVA: 0x00003B72 File Offset: 0x00001D72
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000172 RID: 370 RVA: 0x00003B7B File Offset: 0x00001D7B
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000038 RID: 56
		private string _nextToken;
	}
}
