﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000025 RID: 37
	public class GetTopicAttributesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000118 RID: 280 RVA: 0x00003863 File Offset: 0x00001A63
		// (set) Token: 0x06000119 RID: 281 RVA: 0x0000386B File Offset: 0x00001A6B
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00003874 File Offset: 0x00001A74
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x04000026 RID: 38
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
