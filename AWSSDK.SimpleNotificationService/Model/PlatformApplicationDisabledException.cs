﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200003A RID: 58
	[Serializable]
	public class PlatformApplicationDisabledException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x06000196 RID: 406 RVA: 0x00003326 File Offset: 0x00001526
		public PlatformApplicationDisabledException(string message) : base(message)
		{
		}

		// Token: 0x06000197 RID: 407 RVA: 0x0000332F File Offset: 0x0000152F
		public PlatformApplicationDisabledException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000198 RID: 408 RVA: 0x00003339 File Offset: 0x00001539
		public PlatformApplicationDisabledException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00003342 File Offset: 0x00001542
		public PlatformApplicationDisabledException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00003353 File Offset: 0x00001553
		public PlatformApplicationDisabledException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00003362 File Offset: 0x00001562
		protected PlatformApplicationDisabledException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
