﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001C RID: 28
	public class GetEndpointAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000F2 RID: 242 RVA: 0x0000369F File Offset: 0x0000189F
		// (set) Token: 0x060000F3 RID: 243 RVA: 0x000036A7 File Offset: 0x000018A7
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
			set
			{
				this._endpointArn = value;
			}
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x000036B0 File Offset: 0x000018B0
		internal bool IsSetEndpointArn()
		{
			return this._endpointArn != null;
		}

		// Token: 0x0400001D RID: 29
		private string _endpointArn;
	}
}
