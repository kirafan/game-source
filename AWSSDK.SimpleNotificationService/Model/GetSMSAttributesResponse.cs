﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000021 RID: 33
	public class GetSMSAttributesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000106 RID: 262 RVA: 0x00003791 File Offset: 0x00001991
		// (set) Token: 0x06000107 RID: 263 RVA: 0x00003799 File Offset: 0x00001999
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x06000108 RID: 264 RVA: 0x000037A2 File Offset: 0x000019A2
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x04000022 RID: 34
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
