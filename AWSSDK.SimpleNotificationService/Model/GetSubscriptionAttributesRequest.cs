﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000022 RID: 34
	public class GetSubscriptionAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x0600010A RID: 266 RVA: 0x00003388 File Offset: 0x00001588
		public GetSubscriptionAttributesRequest()
		{
		}

		// Token: 0x0600010B RID: 267 RVA: 0x000037CF File Offset: 0x000019CF
		public GetSubscriptionAttributesRequest(string subscriptionArn)
		{
			this._subscriptionArn = subscriptionArn;
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600010C RID: 268 RVA: 0x000037DE File Offset: 0x000019DE
		// (set) Token: 0x0600010D RID: 269 RVA: 0x000037E6 File Offset: 0x000019E6
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x0600010E RID: 270 RVA: 0x000037EF File Offset: 0x000019EF
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x04000023 RID: 35
		private string _subscriptionArn;
	}
}
