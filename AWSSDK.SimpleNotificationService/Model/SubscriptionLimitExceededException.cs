﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004C RID: 76
	[Serializable]
	public class SubscriptionLimitExceededException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x0600020D RID: 525 RVA: 0x00003326 File Offset: 0x00001526
		public SubscriptionLimitExceededException(string message) : base(message)
		{
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0000332F File Offset: 0x0000152F
		public SubscriptionLimitExceededException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600020F RID: 527 RVA: 0x00003339 File Offset: 0x00001539
		public SubscriptionLimitExceededException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000210 RID: 528 RVA: 0x00003342 File Offset: 0x00001542
		public SubscriptionLimitExceededException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000211 RID: 529 RVA: 0x00003353 File Offset: 0x00001553
		public SubscriptionLimitExceededException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000212 RID: 530 RVA: 0x00003362 File Offset: 0x00001562
		protected SubscriptionLimitExceededException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
