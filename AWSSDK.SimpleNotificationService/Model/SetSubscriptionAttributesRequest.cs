﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000045 RID: 69
	public class SetSubscriptionAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060001D6 RID: 470 RVA: 0x00003388 File Offset: 0x00001588
		public SetSubscriptionAttributesRequest()
		{
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x00003F3D File Offset: 0x0000213D
		public SetSubscriptionAttributesRequest(string subscriptionArn, string attributeName, string attributeValue)
		{
			this._subscriptionArn = subscriptionArn;
			this._attributeName = attributeName;
			this._attributeValue = attributeValue;
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00003F5A File Offset: 0x0000215A
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00003F62 File Offset: 0x00002162
		public string AttributeName
		{
			get
			{
				return this._attributeName;
			}
			set
			{
				this._attributeName = value;
			}
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00003F6B File Offset: 0x0000216B
		internal bool IsSetAttributeName()
		{
			return this._attributeName != null;
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001DB RID: 475 RVA: 0x00003F76 File Offset: 0x00002176
		// (set) Token: 0x060001DC RID: 476 RVA: 0x00003F7E File Offset: 0x0000217E
		public string AttributeValue
		{
			get
			{
				return this._attributeValue;
			}
			set
			{
				this._attributeValue = value;
			}
		}

		// Token: 0x060001DD RID: 477 RVA: 0x00003F87 File Offset: 0x00002187
		internal bool IsSetAttributeValue()
		{
			return this._attributeValue != null;
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001DE RID: 478 RVA: 0x00003F92 File Offset: 0x00002192
		// (set) Token: 0x060001DF RID: 479 RVA: 0x00003F9A File Offset: 0x0000219A
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x00003FA3 File Offset: 0x000021A3
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x04000050 RID: 80
		private string _attributeName;

		// Token: 0x04000051 RID: 81
		private string _attributeValue;

		// Token: 0x04000052 RID: 82
		private string _subscriptionArn;
	}
}
