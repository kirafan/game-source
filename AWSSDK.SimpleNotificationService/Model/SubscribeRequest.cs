﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000049 RID: 73
	public class SubscribeRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x060001EE RID: 494 RVA: 0x00003388 File Offset: 0x00001588
		public SubscribeRequest()
		{
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0000401F File Offset: 0x0000221F
		public SubscribeRequest(string topicArn, string protocol, string endpoint)
		{
			this._topicArn = topicArn;
			this._protocol = protocol;
			this._endpoint = endpoint;
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x0000403C File Offset: 0x0000223C
		// (set) Token: 0x060001F1 RID: 497 RVA: 0x00004044 File Offset: 0x00002244
		public string Endpoint
		{
			get
			{
				return this._endpoint;
			}
			set
			{
				this._endpoint = value;
			}
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000404D File Offset: 0x0000224D
		internal bool IsSetEndpoint()
		{
			return this._endpoint != null;
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001F3 RID: 499 RVA: 0x00004058 File Offset: 0x00002258
		// (set) Token: 0x060001F4 RID: 500 RVA: 0x00004060 File Offset: 0x00002260
		public string Protocol
		{
			get
			{
				return this._protocol;
			}
			set
			{
				this._protocol = value;
			}
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x00004069 File Offset: 0x00002269
		internal bool IsSetProtocol()
		{
			return this._protocol != null;
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001F6 RID: 502 RVA: 0x00004074 File Offset: 0x00002274
		// (set) Token: 0x060001F7 RID: 503 RVA: 0x0000407C File Offset: 0x0000227C
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x00004085 File Offset: 0x00002285
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000056 RID: 86
		private string _endpoint;

		// Token: 0x04000057 RID: 87
		private string _protocol;

		// Token: 0x04000058 RID: 88
		private string _topicArn;
	}
}
