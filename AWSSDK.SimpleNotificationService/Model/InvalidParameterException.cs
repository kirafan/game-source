﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000027 RID: 39
	[Serializable]
	public class InvalidParameterException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x06000122 RID: 290 RVA: 0x00003326 File Offset: 0x00001526
		public InvalidParameterException(string message) : base(message)
		{
		}

		// Token: 0x06000123 RID: 291 RVA: 0x0000332F File Offset: 0x0000152F
		public InvalidParameterException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00003339 File Offset: 0x00001539
		public InvalidParameterException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00003342 File Offset: 0x00001542
		public InvalidParameterException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000126 RID: 294 RVA: 0x00003353 File Offset: 0x00001553
		public InvalidParameterException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000127 RID: 295 RVA: 0x00003362 File Offset: 0x00001562
		protected InvalidParameterException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
