﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000026 RID: 38
	[Serializable]
	public class InternalErrorException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x0600011C RID: 284 RVA: 0x00003326 File Offset: 0x00001526
		public InternalErrorException(string message) : base(message)
		{
		}

		// Token: 0x0600011D RID: 285 RVA: 0x0000332F File Offset: 0x0000152F
		public InternalErrorException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00003339 File Offset: 0x00001539
		public InternalErrorException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00003342 File Offset: 0x00001542
		public InternalErrorException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00003353 File Offset: 0x00001553
		public InternalErrorException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00003362 File Offset: 0x00001562
		protected InternalErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
