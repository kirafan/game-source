﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000B RID: 11
	public class CheckIfPhoneNumberIsOptedOutResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000099 RID: 153 RVA: 0x00003390 File Offset: 0x00001590
		// (set) Token: 0x0600009A RID: 154 RVA: 0x0000339D File Offset: 0x0000159D
		public bool IsOptedOut
		{
			get
			{
				return this._isOptedOut.GetValueOrDefault();
			}
			set
			{
				this._isOptedOut = new bool?(value);
			}
		}

		// Token: 0x0600009B RID: 155 RVA: 0x000033AB File Offset: 0x000015AB
		internal bool IsSetIsOptedOut()
		{
			return this._isOptedOut != null;
		}

		// Token: 0x04000008 RID: 8
		private bool? _isOptedOut;
	}
}
