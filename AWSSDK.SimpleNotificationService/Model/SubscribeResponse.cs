﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004A RID: 74
	public class SubscribeResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x00004090 File Offset: 0x00002290
		// (set) Token: 0x060001FA RID: 506 RVA: 0x00004098 File Offset: 0x00002298
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x060001FB RID: 507 RVA: 0x000040A1 File Offset: 0x000022A1
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x04000059 RID: 89
		private string _subscriptionArn;
	}
}
