﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002B RID: 43
	public class ListPhoneNumbersOptedOutRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600013C RID: 316 RVA: 0x00003933 File Offset: 0x00001B33
		// (set) Token: 0x0600013D RID: 317 RVA: 0x0000393B File Offset: 0x00001B3B
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00003944 File Offset: 0x00001B44
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x0400002B RID: 43
		private string _nextToken;
	}
}
