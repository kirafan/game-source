﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004D RID: 77
	[Serializable]
	public class ThrottledException : AmazonSimpleNotificationServiceException
	{
		// Token: 0x06000213 RID: 531 RVA: 0x00003326 File Offset: 0x00001526
		public ThrottledException(string message) : base(message)
		{
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0000332F File Offset: 0x0000152F
		public ThrottledException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000215 RID: 533 RVA: 0x00003339 File Offset: 0x00001539
		public ThrottledException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000216 RID: 534 RVA: 0x00003342 File Offset: 0x00001542
		public ThrottledException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000217 RID: 535 RVA: 0x00003353 File Offset: 0x00001553
		public ThrottledException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00003362 File Offset: 0x00001562
		protected ThrottledException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
