﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200000F RID: 15
	public class CreatePlatformApplicationResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000B7 RID: 183 RVA: 0x000034D1 File Offset: 0x000016D1
		// (set) Token: 0x060000B8 RID: 184 RVA: 0x000034D9 File Offset: 0x000016D9
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x000034E2 File Offset: 0x000016E2
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x04000010 RID: 16
		private string _platformApplicationArn;
	}
}
