﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000013 RID: 19
	public class CreateTopicResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000D1 RID: 209 RVA: 0x000035C6 File Offset: 0x000017C6
		// (set) Token: 0x060000D2 RID: 210 RVA: 0x000035CE File Offset: 0x000017CE
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x000035D7 File Offset: 0x000017D7
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000017 RID: 23
		private string _topicArn;
	}
}
