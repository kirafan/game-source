﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200003F RID: 63
	public class SetEndpointAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001C1 RID: 449 RVA: 0x00003E4B File Offset: 0x0000204B
		// (set) Token: 0x060001C2 RID: 450 RVA: 0x00003E53 File Offset: 0x00002053
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00003E5C File Offset: 0x0000205C
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x00003E76 File Offset: 0x00002076
		// (set) Token: 0x060001C5 RID: 453 RVA: 0x00003E7E File Offset: 0x0000207E
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
			set
			{
				this._endpointArn = value;
			}
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x00003E87 File Offset: 0x00002087
		internal bool IsSetEndpointArn()
		{
			return this._endpointArn != null;
		}

		// Token: 0x0400004B RID: 75
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x0400004C RID: 76
		private string _endpointArn;
	}
}
