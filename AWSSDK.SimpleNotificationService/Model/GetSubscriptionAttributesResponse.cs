﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000023 RID: 35
	public class GetSubscriptionAttributesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600010F RID: 271 RVA: 0x000037FA File Offset: 0x000019FA
		// (set) Token: 0x06000110 RID: 272 RVA: 0x00003802 File Offset: 0x00001A02
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x06000111 RID: 273 RVA: 0x0000380B File Offset: 0x00001A0B
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x04000024 RID: 36
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
