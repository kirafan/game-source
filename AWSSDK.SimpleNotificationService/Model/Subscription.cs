﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200004B RID: 75
	public class Subscription
	{
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001FE RID: 510 RVA: 0x000040AC File Offset: 0x000022AC
		// (set) Token: 0x060001FF RID: 511 RVA: 0x000040B4 File Offset: 0x000022B4
		public string Endpoint
		{
			get
			{
				return this._endpoint;
			}
			set
			{
				this._endpoint = value;
			}
		}

		// Token: 0x06000200 RID: 512 RVA: 0x000040BD File Offset: 0x000022BD
		internal bool IsSetEndpoint()
		{
			return this._endpoint != null;
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000201 RID: 513 RVA: 0x000040C8 File Offset: 0x000022C8
		// (set) Token: 0x06000202 RID: 514 RVA: 0x000040D0 File Offset: 0x000022D0
		public string Owner
		{
			get
			{
				return this._owner;
			}
			set
			{
				this._owner = value;
			}
		}

		// Token: 0x06000203 RID: 515 RVA: 0x000040D9 File Offset: 0x000022D9
		internal bool IsSetOwner()
		{
			return this._owner != null;
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000204 RID: 516 RVA: 0x000040E4 File Offset: 0x000022E4
		// (set) Token: 0x06000205 RID: 517 RVA: 0x000040EC File Offset: 0x000022EC
		public string Protocol
		{
			get
			{
				return this._protocol;
			}
			set
			{
				this._protocol = value;
			}
		}

		// Token: 0x06000206 RID: 518 RVA: 0x000040F5 File Offset: 0x000022F5
		internal bool IsSetProtocol()
		{
			return this._protocol != null;
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000207 RID: 519 RVA: 0x00004100 File Offset: 0x00002300
		// (set) Token: 0x06000208 RID: 520 RVA: 0x00004108 File Offset: 0x00002308
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x06000209 RID: 521 RVA: 0x00004111 File Offset: 0x00002311
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600020A RID: 522 RVA: 0x0000411C File Offset: 0x0000231C
		// (set) Token: 0x0600020B RID: 523 RVA: 0x00004124 File Offset: 0x00002324
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0000412D File Offset: 0x0000232D
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x0400005A RID: 90
		private string _endpoint;

		// Token: 0x0400005B RID: 91
		private string _owner;

		// Token: 0x0400005C RID: 92
		private string _protocol;

		// Token: 0x0400005D RID: 93
		private string _subscriptionArn;

		// Token: 0x0400005E RID: 94
		private string _topicArn;
	}
}
