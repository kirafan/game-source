﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000037 RID: 55
	public class OptInPhoneNumberRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600018A RID: 394 RVA: 0x00003C3C File Offset: 0x00001E3C
		// (set) Token: 0x0600018B RID: 395 RVA: 0x00003C44 File Offset: 0x00001E44
		public string PhoneNumber
		{
			get
			{
				return this._phoneNumber;
			}
			set
			{
				this._phoneNumber = value;
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00003C4D File Offset: 0x00001E4D
		internal bool IsSetPhoneNumber()
		{
			return this._phoneNumber != null;
		}

		// Token: 0x0400003E RID: 62
		private string _phoneNumber;
	}
}
