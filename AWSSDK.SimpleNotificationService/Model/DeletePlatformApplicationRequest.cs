﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000016 RID: 22
	public class DeletePlatformApplicationRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000DA RID: 218 RVA: 0x000035FE File Offset: 0x000017FE
		// (set) Token: 0x060000DB RID: 219 RVA: 0x00003606 File Offset: 0x00001806
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x060000DC RID: 220 RVA: 0x0000360F File Offset: 0x0000180F
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x04000019 RID: 25
		private string _platformApplicationArn;
	}
}
