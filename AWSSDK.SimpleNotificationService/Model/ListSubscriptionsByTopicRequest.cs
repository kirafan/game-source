﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002F RID: 47
	public class ListSubscriptionsByTopicRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x06000152 RID: 338 RVA: 0x00003388 File Offset: 0x00001588
		public ListSubscriptionsByTopicRequest()
		{
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00003A1F File Offset: 0x00001C1F
		public ListSubscriptionsByTopicRequest(string topicArn)
		{
			this._topicArn = topicArn;
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00003A2E File Offset: 0x00001C2E
		public ListSubscriptionsByTopicRequest(string topicArn, string nextToken)
		{
			this._topicArn = topicArn;
			this._nextToken = nextToken;
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00003A44 File Offset: 0x00001C44
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00003A4C File Offset: 0x00001C4C
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00003A55 File Offset: 0x00001C55
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000158 RID: 344 RVA: 0x00003A60 File Offset: 0x00001C60
		// (set) Token: 0x06000159 RID: 345 RVA: 0x00003A68 File Offset: 0x00001C68
		public string TopicArn
		{
			get
			{
				return this._topicArn;
			}
			set
			{
				this._topicArn = value;
			}
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00003A71 File Offset: 0x00001C71
		internal bool IsSetTopicArn()
		{
			return this._topicArn != null;
		}

		// Token: 0x04000031 RID: 49
		private string _nextToken;

		// Token: 0x04000032 RID: 50
		private string _topicArn;
	}
}
