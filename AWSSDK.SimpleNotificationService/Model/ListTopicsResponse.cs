﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000034 RID: 52
	public class ListTopicsResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000173 RID: 371 RVA: 0x00003B86 File Offset: 0x00001D86
		// (set) Token: 0x06000174 RID: 372 RVA: 0x00003B8E File Offset: 0x00001D8E
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000175 RID: 373 RVA: 0x00003B97 File Offset: 0x00001D97
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000176 RID: 374 RVA: 0x00003BA2 File Offset: 0x00001DA2
		// (set) Token: 0x06000177 RID: 375 RVA: 0x00003BAA File Offset: 0x00001DAA
		public List<Topic> Topics
		{
			get
			{
				return this._topics;
			}
			set
			{
				this._topics = value;
			}
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00003BB3 File Offset: 0x00001DB3
		internal bool IsSetTopics()
		{
			return this._topics != null && this._topics.Count > 0;
		}

		// Token: 0x04000039 RID: 57
		private string _nextToken;

		// Token: 0x0400003A RID: 58
		private List<Topic> _topics = new List<Topic>();
	}
}
