﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000043 RID: 67
	public class SetSMSAttributesRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x00003EFF File Offset: 0x000020FF
		// (set) Token: 0x060001D2 RID: 466 RVA: 0x00003F07 File Offset: 0x00002107
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00003F10 File Offset: 0x00002110
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x0400004F RID: 79
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
