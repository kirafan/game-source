﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002C RID: 44
	public class ListPhoneNumbersOptedOutResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000140 RID: 320 RVA: 0x0000394F File Offset: 0x00001B4F
		// (set) Token: 0x06000141 RID: 321 RVA: 0x00003957 File Offset: 0x00001B57
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00003960 File Offset: 0x00001B60
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000143 RID: 323 RVA: 0x0000396B File Offset: 0x00001B6B
		// (set) Token: 0x06000144 RID: 324 RVA: 0x00003973 File Offset: 0x00001B73
		public List<string> PhoneNumbers
		{
			get
			{
				return this._phoneNumbers;
			}
			set
			{
				this._phoneNumbers = value;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000397C File Offset: 0x00001B7C
		internal bool IsSetPhoneNumbers()
		{
			return this._phoneNumbers != null && this._phoneNumbers.Count > 0;
		}

		// Token: 0x0400002C RID: 44
		private string _nextToken;

		// Token: 0x0400002D RID: 45
		private List<string> _phoneNumbers = new List<string>();
	}
}
