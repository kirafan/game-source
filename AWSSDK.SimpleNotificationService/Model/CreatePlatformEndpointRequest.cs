﻿using System;
using System.Collections.Generic;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000010 RID: 16
	public class CreatePlatformEndpointRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000BB RID: 187 RVA: 0x000034ED File Offset: 0x000016ED
		// (set) Token: 0x060000BC RID: 188 RVA: 0x000034F5 File Offset: 0x000016F5
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060000BD RID: 189 RVA: 0x000034FE File Offset: 0x000016FE
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000BE RID: 190 RVA: 0x00003518 File Offset: 0x00001718
		// (set) Token: 0x060000BF RID: 191 RVA: 0x00003520 File Offset: 0x00001720
		public string CustomUserData
		{
			get
			{
				return this._customUserData;
			}
			set
			{
				this._customUserData = value;
			}
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00003529 File Offset: 0x00001729
		internal bool IsSetCustomUserData()
		{
			return this._customUserData != null;
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000C1 RID: 193 RVA: 0x00003534 File Offset: 0x00001734
		// (set) Token: 0x060000C2 RID: 194 RVA: 0x0000353C File Offset: 0x0000173C
		public string PlatformApplicationArn
		{
			get
			{
				return this._platformApplicationArn;
			}
			set
			{
				this._platformApplicationArn = value;
			}
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00003545 File Offset: 0x00001745
		internal bool IsSetPlatformApplicationArn()
		{
			return this._platformApplicationArn != null;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00003550 File Offset: 0x00001750
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x00003558 File Offset: 0x00001758
		public string Token
		{
			get
			{
				return this._token;
			}
			set
			{
				this._token = value;
			}
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00003561 File Offset: 0x00001761
		internal bool IsSetToken()
		{
			return this._token != null;
		}

		// Token: 0x04000011 RID: 17
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();

		// Token: 0x04000012 RID: 18
		private string _customUserData;

		// Token: 0x04000013 RID: 19
		private string _platformApplicationArn;

		// Token: 0x04000014 RID: 20
		private string _token;
	}
}
