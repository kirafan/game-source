﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200002D RID: 45
	public class ListPlatformApplicationsRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000147 RID: 327 RVA: 0x000039A9 File Offset: 0x00001BA9
		// (set) Token: 0x06000148 RID: 328 RVA: 0x000039B1 File Offset: 0x00001BB1
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000039BA File Offset: 0x00001BBA
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x0400002E RID: 46
		private string _nextToken;
	}
}
