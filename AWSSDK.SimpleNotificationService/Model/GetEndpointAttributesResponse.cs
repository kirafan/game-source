﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x0200001D RID: 29
	public class GetEndpointAttributesResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x000036BB File Offset: 0x000018BB
		// (set) Token: 0x060000F7 RID: 247 RVA: 0x000036C3 File Offset: 0x000018C3
		public Dictionary<string, string> Attributes
		{
			get
			{
				return this._attributes;
			}
			set
			{
				this._attributes = value;
			}
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x000036CC File Offset: 0x000018CC
		internal bool IsSetAttributes()
		{
			return this._attributes != null && this._attributes.Count > 0;
		}

		// Token: 0x0400001E RID: 30
		private Dictionary<string, string> _attributes = new Dictionary<string, string>();
	}
}
