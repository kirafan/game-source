﻿using System;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000050 RID: 80
	public class UnsubscribeRequest : AmazonSimpleNotificationServiceRequest
	{
		// Token: 0x06000223 RID: 547 RVA: 0x00003388 File Offset: 0x00001588
		public UnsubscribeRequest()
		{
		}

		// Token: 0x06000224 RID: 548 RVA: 0x00004154 File Offset: 0x00002354
		public UnsubscribeRequest(string subscriptionArn)
		{
			this._subscriptionArn = subscriptionArn;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000225 RID: 549 RVA: 0x00004163 File Offset: 0x00002363
		// (set) Token: 0x06000226 RID: 550 RVA: 0x0000416B File Offset: 0x0000236B
		public string SubscriptionArn
		{
			get
			{
				return this._subscriptionArn;
			}
			set
			{
				this._subscriptionArn = value;
			}
		}

		// Token: 0x06000227 RID: 551 RVA: 0x00004174 File Offset: 0x00002374
		internal bool IsSetSubscriptionArn()
		{
			return this._subscriptionArn != null;
		}

		// Token: 0x04000060 RID: 96
		private string _subscriptionArn;
	}
}
