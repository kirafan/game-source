﻿using System;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService.Model
{
	// Token: 0x02000011 RID: 17
	public class CreatePlatformEndpointResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x0000357F File Offset: 0x0000177F
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x00003587 File Offset: 0x00001787
		public string EndpointArn
		{
			get
			{
				return this._endpointArn;
			}
			set
			{
				this._endpointArn = value;
			}
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00003590 File Offset: 0x00001790
		internal bool IsSetEndpointArn()
		{
			return this._endpointArn != null;
		}

		// Token: 0x04000015 RID: 21
		private string _endpointArn;
	}
}
