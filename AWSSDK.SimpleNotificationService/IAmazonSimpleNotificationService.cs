﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;

namespace Amazon.SimpleNotificationService
{
	// Token: 0x02000003 RID: 3
	public interface IAmazonSimpleNotificationService : IDisposable, IAmazonService
	{
		// Token: 0x06000044 RID: 68
		void AddPermissionAsync(string topicArn, string label, List<string> awsAccountId, List<string> actionName, AmazonServiceCallback<AddPermissionRequest, AddPermissionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000045 RID: 69
		void AddPermissionAsync(AddPermissionRequest request, AmazonServiceCallback<AddPermissionRequest, AddPermissionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000046 RID: 70
		void CheckIfPhoneNumberIsOptedOutAsync(CheckIfPhoneNumberIsOptedOutRequest request, AmazonServiceCallback<CheckIfPhoneNumberIsOptedOutRequest, CheckIfPhoneNumberIsOptedOutResponse> callback, AsyncOptions options = null);

		// Token: 0x06000047 RID: 71
		void ConfirmSubscriptionAsync(string topicArn, string token, string authenticateOnUnsubscribe, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000048 RID: 72
		void ConfirmSubscriptionAsync(string topicArn, string token, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000049 RID: 73
		void ConfirmSubscriptionAsync(ConfirmSubscriptionRequest request, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004A RID: 74
		void CreatePlatformApplicationAsync(CreatePlatformApplicationRequest request, AmazonServiceCallback<CreatePlatformApplicationRequest, CreatePlatformApplicationResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004B RID: 75
		void CreatePlatformEndpointAsync(CreatePlatformEndpointRequest request, AmazonServiceCallback<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004C RID: 76
		void CreateTopicAsync(string name, AmazonServiceCallback<CreateTopicRequest, CreateTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004D RID: 77
		void CreateTopicAsync(CreateTopicRequest request, AmazonServiceCallback<CreateTopicRequest, CreateTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004E RID: 78
		void DeleteEndpointAsync(DeleteEndpointRequest request, AmazonServiceCallback<DeleteEndpointRequest, DeleteEndpointResponse> callback, AsyncOptions options = null);

		// Token: 0x0600004F RID: 79
		void DeletePlatformApplicationAsync(DeletePlatformApplicationRequest request, AmazonServiceCallback<DeletePlatformApplicationRequest, DeletePlatformApplicationResponse> callback, AsyncOptions options = null);

		// Token: 0x06000050 RID: 80
		void DeleteTopicAsync(string topicArn, AmazonServiceCallback<DeleteTopicRequest, DeleteTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x06000051 RID: 81
		void DeleteTopicAsync(DeleteTopicRequest request, AmazonServiceCallback<DeleteTopicRequest, DeleteTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x06000052 RID: 82
		void GetEndpointAttributesAsync(GetEndpointAttributesRequest request, AmazonServiceCallback<GetEndpointAttributesRequest, GetEndpointAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000053 RID: 83
		void GetPlatformApplicationAttributesAsync(GetPlatformApplicationAttributesRequest request, AmazonServiceCallback<GetPlatformApplicationAttributesRequest, GetPlatformApplicationAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000054 RID: 84
		void GetSMSAttributesAsync(GetSMSAttributesRequest request, AmazonServiceCallback<GetSMSAttributesRequest, GetSMSAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000055 RID: 85
		void GetSubscriptionAttributesAsync(string subscriptionArn, AmazonServiceCallback<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000056 RID: 86
		void GetSubscriptionAttributesAsync(GetSubscriptionAttributesRequest request, AmazonServiceCallback<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000057 RID: 87
		void GetTopicAttributesAsync(string topicArn, AmazonServiceCallback<GetTopicAttributesRequest, GetTopicAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000058 RID: 88
		void GetTopicAttributesAsync(GetTopicAttributesRequest request, AmazonServiceCallback<GetTopicAttributesRequest, GetTopicAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000059 RID: 89
		void ListEndpointsByPlatformApplicationAsync(ListEndpointsByPlatformApplicationRequest request, AmazonServiceCallback<ListEndpointsByPlatformApplicationRequest, ListEndpointsByPlatformApplicationResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005A RID: 90
		void ListPhoneNumbersOptedOutAsync(ListPhoneNumbersOptedOutRequest request, AmazonServiceCallback<ListPhoneNumbersOptedOutRequest, ListPhoneNumbersOptedOutResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005B RID: 91
		void ListPlatformApplicationsAsync(ListPlatformApplicationsRequest request, AmazonServiceCallback<ListPlatformApplicationsRequest, ListPlatformApplicationsResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005C RID: 92
		void ListSubscriptionsAsync(string nextToken, AmazonServiceCallback<ListSubscriptionsRequest, ListSubscriptionsResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005D RID: 93
		void ListSubscriptionsAsync(ListSubscriptionsRequest request, AmazonServiceCallback<ListSubscriptionsRequest, ListSubscriptionsResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005E RID: 94
		void ListSubscriptionsByTopicAsync(string topicArn, string nextToken, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x0600005F RID: 95
		void ListSubscriptionsByTopicAsync(string topicArn, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x06000060 RID: 96
		void ListSubscriptionsByTopicAsync(ListSubscriptionsByTopicRequest request, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null);

		// Token: 0x06000061 RID: 97
		void ListTopicsAsync(string nextToken, AmazonServiceCallback<ListTopicsRequest, ListTopicsResponse> callback, AsyncOptions options = null);

		// Token: 0x06000062 RID: 98
		void ListTopicsAsync(ListTopicsRequest request, AmazonServiceCallback<ListTopicsRequest, ListTopicsResponse> callback, AsyncOptions options = null);

		// Token: 0x06000063 RID: 99
		void OptInPhoneNumberAsync(OptInPhoneNumberRequest request, AmazonServiceCallback<OptInPhoneNumberRequest, OptInPhoneNumberResponse> callback, AsyncOptions options = null);

		// Token: 0x06000064 RID: 100
		void PublishAsync(string topicArn, string message, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null);

		// Token: 0x06000065 RID: 101
		void PublishAsync(string topicArn, string message, string subject, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null);

		// Token: 0x06000066 RID: 102
		void PublishAsync(PublishRequest request, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null);

		// Token: 0x06000067 RID: 103
		void RemovePermissionAsync(string topicArn, string label, AmazonServiceCallback<RemovePermissionRequest, RemovePermissionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000068 RID: 104
		void RemovePermissionAsync(RemovePermissionRequest request, AmazonServiceCallback<RemovePermissionRequest, RemovePermissionResponse> callback, AsyncOptions options = null);

		// Token: 0x06000069 RID: 105
		void SetEndpointAttributesAsync(SetEndpointAttributesRequest request, AmazonServiceCallback<SetEndpointAttributesRequest, SetEndpointAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006A RID: 106
		void SetPlatformApplicationAttributesAsync(SetPlatformApplicationAttributesRequest request, AmazonServiceCallback<SetPlatformApplicationAttributesRequest, SetPlatformApplicationAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006B RID: 107
		void SetSMSAttributesAsync(SetSMSAttributesRequest request, AmazonServiceCallback<SetSMSAttributesRequest, SetSMSAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006C RID: 108
		void SetSubscriptionAttributesAsync(string subscriptionArn, string attributeName, string attributeValue, AmazonServiceCallback<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006D RID: 109
		void SetSubscriptionAttributesAsync(SetSubscriptionAttributesRequest request, AmazonServiceCallback<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006E RID: 110
		void SetTopicAttributesAsync(string topicArn, string attributeName, string attributeValue, AmazonServiceCallback<SetTopicAttributesRequest, SetTopicAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x0600006F RID: 111
		void SetTopicAttributesAsync(SetTopicAttributesRequest request, AmazonServiceCallback<SetTopicAttributesRequest, SetTopicAttributesResponse> callback, AsyncOptions options = null);

		// Token: 0x06000070 RID: 112
		void SubscribeAsync(string topicArn, string protocol, string endpoint, AmazonServiceCallback<SubscribeRequest, SubscribeResponse> callback, AsyncOptions options = null);

		// Token: 0x06000071 RID: 113
		void SubscribeAsync(SubscribeRequest request, AmazonServiceCallback<SubscribeRequest, SubscribeResponse> callback, AsyncOptions options = null);

		// Token: 0x06000072 RID: 114
		void UnsubscribeAsync(string subscriptionArn, AmazonServiceCallback<UnsubscribeRequest, UnsubscribeResponse> callback, AsyncOptions options = null);

		// Token: 0x06000073 RID: 115
		void UnsubscribeAsync(UnsubscribeRequest request, AmazonServiceCallback<UnsubscribeRequest, UnsubscribeResponse> callback, AsyncOptions options = null);
	}
}
