﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Amazon.Runtime;
using ThirdParty.Json.LitJson;

namespace Amazon.SimpleNotificationService.Util
{
	// Token: 0x02000092 RID: 146
	public class Message
	{
		// Token: 0x06000369 RID: 873 RVA: 0x00003C34 File Offset: 0x00001E34
		private Message()
		{
		}

		// Token: 0x0600036A RID: 874 RVA: 0x00009760 File Offset: 0x00007960
		public static Message ParseMessage(string messageText)
		{
			Message message = new Message();
			JsonData jsonData = JsonMapper.ToObject(messageText);
			Func<string, string> func = delegate(string fieldName)
			{
				if (jsonData[fieldName] != null && jsonData[fieldName].IsString)
				{
					return (string)jsonData[fieldName];
				}
				return null;
			};
			message.MessageId = func("MessageId");
			message.MessageText = func("Message");
			message.Signature = func("Signature");
			message.SignatureVersion = func("SignatureVersion");
			message.SigningCertURL = Message.ValidateCertUrl(func("SigningCertURL"));
			message.SubscribeURL = func("SubscribeURL");
			message.Subject = func("Subject");
			message.TimestampString = func("Timestamp");
			message.Token = func("Token");
			message.TopicArn = func("TopicArn");
			message.Type = func("Type");
			message.UnsubscribeURL = func("UnsubscribeURL");
			return message;
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x0600036B RID: 875 RVA: 0x00009862 File Offset: 0x00007A62
		// (set) Token: 0x0600036C RID: 876 RVA: 0x0000986A File Offset: 0x00007A6A
		public string MessageId { get; private set; }

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600036D RID: 877 RVA: 0x00009873 File Offset: 0x00007A73
		// (set) Token: 0x0600036E RID: 878 RVA: 0x0000987B File Offset: 0x00007A7B
		public string MessageText { get; private set; }

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600036F RID: 879 RVA: 0x00009884 File Offset: 0x00007A84
		// (set) Token: 0x06000370 RID: 880 RVA: 0x0000988C File Offset: 0x00007A8C
		public string Signature { get; private set; }

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x06000371 RID: 881 RVA: 0x00009895 File Offset: 0x00007A95
		// (set) Token: 0x06000372 RID: 882 RVA: 0x0000989D File Offset: 0x00007A9D
		public string SignatureVersion { get; private set; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000373 RID: 883 RVA: 0x000098A6 File Offset: 0x00007AA6
		// (set) Token: 0x06000374 RID: 884 RVA: 0x000098AE File Offset: 0x00007AAE
		public string SigningCertURL { get; private set; }

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000375 RID: 885 RVA: 0x000098B7 File Offset: 0x00007AB7
		// (set) Token: 0x06000376 RID: 886 RVA: 0x000098BF File Offset: 0x00007ABF
		public string Subject { get; private set; }

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000377 RID: 887 RVA: 0x000098C8 File Offset: 0x00007AC8
		// (set) Token: 0x06000378 RID: 888 RVA: 0x000098D0 File Offset: 0x00007AD0
		public string SubscribeURL { get; private set; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000379 RID: 889 RVA: 0x000098D9 File Offset: 0x00007AD9
		public DateTime Timestamp
		{
			get
			{
				if (string.IsNullOrEmpty(this.TimestampString))
				{
					return DateTime.MinValue;
				}
				return DateTime.ParseExact(this.TimestampString, "yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture);
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600037A RID: 890 RVA: 0x00009903 File Offset: 0x00007B03
		// (set) Token: 0x0600037B RID: 891 RVA: 0x0000990B File Offset: 0x00007B0B
		private string TimestampString { get; set; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600037C RID: 892 RVA: 0x00009914 File Offset: 0x00007B14
		// (set) Token: 0x0600037D RID: 893 RVA: 0x0000991C File Offset: 0x00007B1C
		public string Token { get; private set; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600037E RID: 894 RVA: 0x00009925 File Offset: 0x00007B25
		// (set) Token: 0x0600037F RID: 895 RVA: 0x0000992D File Offset: 0x00007B2D
		public string TopicArn { get; private set; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000380 RID: 896 RVA: 0x00009936 File Offset: 0x00007B36
		// (set) Token: 0x06000381 RID: 897 RVA: 0x0000993E File Offset: 0x00007B3E
		public string Type { get; private set; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000382 RID: 898 RVA: 0x00009947 File Offset: 0x00007B47
		public bool IsSubscriptionType
		{
			get
			{
				return this.Type == "SubscriptionConfirmation";
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000383 RID: 899 RVA: 0x00009959 File Offset: 0x00007B59
		public bool IsUnsubscriptionType
		{
			get
			{
				return this.Type == "UnsubscribeConfirmation";
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x06000384 RID: 900 RVA: 0x0000996B File Offset: 0x00007B6B
		public bool IsNotificationType
		{
			get
			{
				return this.Type == "Notification";
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000385 RID: 901 RVA: 0x0000997D File Offset: 0x00007B7D
		// (set) Token: 0x06000386 RID: 902 RVA: 0x00009985 File Offset: 0x00007B85
		public string UnsubscribeURL { get; private set; }

		// Token: 0x06000387 RID: 903 RVA: 0x00009990 File Offset: 0x00007B90
		private static string ValidateCertUrl(string certUrl)
		{
			Uri uri = new Uri(certUrl);
			if (uri.Scheme == "https" && certUrl.EndsWith(".pem", StringComparison.Ordinal) && new Regex("^sns\\.[a-zA-Z0-9\\-]{3,}\\.amazonaws\\.com(\\.cn)?$").IsMatch(uri.Host))
			{
				return certUrl;
			}
			throw new AmazonClientException("Signing certificate url is not from a recognised source.");
		}

		// Token: 0x04000083 RID: 131
		private const int MAX_RETRIES = 3;

		// Token: 0x04000084 RID: 132
		public const string MESSAGE_TYPE_SUBSCRIPTION_CONFIRMATION = "SubscriptionConfirmation";

		// Token: 0x04000085 RID: 133
		public const string MESSAGE_TYPE_UNSUBSCRIPTION_CONFIRMATION = "UnsubscribeConfirmation";

		// Token: 0x04000086 RID: 134
		public const string MESSAGE_TYPE_NOTIFICATION = "Notification";
	}
}
