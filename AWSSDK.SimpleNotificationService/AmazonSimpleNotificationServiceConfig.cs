﻿using System;
using Amazon.Runtime;
using Amazon.Util.Internal;

namespace Amazon.SimpleNotificationService
{
	// Token: 0x02000004 RID: 4
	public class AmazonSimpleNotificationServiceConfig : ClientConfig
	{
		// Token: 0x06000074 RID: 116 RVA: 0x0000319E File Offset: 0x0000139E
		public AmazonSimpleNotificationServiceConfig()
		{
			base.AuthenticationServiceName = "sns";
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000075 RID: 117 RVA: 0x000031BC File Offset: 0x000013BC
		public override string RegionEndpointServiceName
		{
			get
			{
				return "sns";
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000076 RID: 118 RVA: 0x000031C3 File Offset: 0x000013C3
		public override string ServiceVersion
		{
			get
			{
				return "2010-03-31";
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000077 RID: 119 RVA: 0x000031CA File Offset: 0x000013CA
		public override string UserAgent
		{
			get
			{
				return this._userAgent;
			}
		}

		// Token: 0x04000001 RID: 1
		private static readonly string UserAgentString = InternalSDKUtils.BuildUserAgentString("3.3.0.17");

		// Token: 0x04000002 RID: 2
		private string _userAgent = AmazonSimpleNotificationServiceConfig.UserAgentString;
	}
}
