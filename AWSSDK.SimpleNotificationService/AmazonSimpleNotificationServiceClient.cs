﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Amazon.Auth.AccessControlPolicy;
using Amazon.Auth.AccessControlPolicy.ActionIdentifiers;
using Amazon.Runtime;
using Amazon.Runtime.Internal.Auth;
using Amazon.SimpleNotificationService.Model;
using Amazon.SimpleNotificationService.Model.Internal.MarshallTransformations;

namespace Amazon.SimpleNotificationService
{
	// Token: 0x02000002 RID: 2
	public class AmazonSimpleNotificationServiceClient : AmazonServiceClient, IAmazonSimpleNotificationService, IDisposable, IAmazonService
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private static void AddSQSPermission(Policy policy, string topicArn, string sqsQueueArn)
		{
			Statement statement = new Statement(Statement.StatementEffect.Allow);
			statement.Actions.Add(SQSActionIdentifiers.SendMessage);
			statement.Resources.Add(new Resource(sqsQueueArn));
			statement.Conditions.Add(ConditionFactory.NewSourceArnCondition(topicArn));
			statement.Principals.Add(new Principal("*"));
			policy.Statements.Add(statement);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000020B8 File Offset: 0x000002B8
		private static bool HasSQSPermission(Policy policy, string topicArn, string sqsQueueArn)
		{
			foreach (Statement statement in policy.Statements)
			{
				bool flag = false;
				using (IEnumerator<Resource> enumerator2 = statement.Resources.GetEnumerator())
				{
					while (enumerator2.MoveNext())
					{
						if (enumerator2.Current.Id.Equals(sqsQueueArn))
						{
							flag = true;
							break;
						}
					}
				}
				if (flag)
				{
					foreach (Condition condition in statement.Conditions)
					{
						if ((string.Equals(condition.Type, ConditionFactory.StringComparisonType.StringLike.ToString(), StringComparison.OrdinalIgnoreCase) || string.Equals(condition.Type, ConditionFactory.StringComparisonType.StringEquals.ToString(), StringComparison.OrdinalIgnoreCase) || string.Equals(condition.Type, ConditionFactory.ArnComparisonType.ArnEquals.ToString(), StringComparison.OrdinalIgnoreCase) || string.Equals(condition.Type, ConditionFactory.ArnComparisonType.ArnLike.ToString(), StringComparison.OrdinalIgnoreCase)) && string.Equals(condition.ConditionKey, "aws:SourceArn", StringComparison.OrdinalIgnoreCase) && condition.Values.Contains(topicArn))
						{
							return true;
						}
					}
				}
			}
			return false;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x00002260 File Offset: 0x00000460
		private static bool TopicNameMatcher(string topicArn, string topicName)
		{
			if (string.IsNullOrEmpty(topicArn))
			{
				return false;
			}
			if (string.IsNullOrEmpty(topicName))
			{
				return false;
			}
			int num = topicArn.LastIndexOf(":", StringComparison.OrdinalIgnoreCase);
			return !num.Equals(-1) && topicArn.Substring(num + 1).Equals(topicName);
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000022AC File Offset: 0x000004AC
		private static void GetNewPolicyAndStatementForTopicAttributes(Dictionary<string, string> attributes, string topicArn, string bucket, out Policy policy, out Statement statement)
		{
			if (attributes.ContainsKey("Policy") && !string.IsNullOrEmpty(attributes["Policy"]))
			{
				policy = Policy.FromJson(attributes["Policy"]);
			}
			else
			{
				policy = new Policy();
			}
			string arnPattern = string.Format(CultureInfo.InvariantCulture, "arn:aws:s3:*:*:{0}", new object[]
			{
				bucket
			});
			statement = new Statement(Statement.StatementEffect.Allow);
			statement.Actions.Add(SNSActionIdentifiers.Publish);
			statement.Resources.Add(new Resource(topicArn));
			statement.Conditions.Add(ConditionFactory.NewSourceArnCondition(arnPattern));
			statement.Principals.Add(new Principal("*"));
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002365 File Offset: 0x00000565
		public AmazonSimpleNotificationServiceClient(AWSCredentials credentials) : this(credentials, new AmazonSimpleNotificationServiceConfig())
		{
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002373 File Offset: 0x00000573
		public AmazonSimpleNotificationServiceClient(AWSCredentials credentials, RegionEndpoint region) : this(credentials, new AmazonSimpleNotificationServiceConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002388 File Offset: 0x00000588
		public AmazonSimpleNotificationServiceClient(AWSCredentials credentials, AmazonSimpleNotificationServiceConfig clientConfig) : base(credentials, clientConfig)
		{
		}

		// Token: 0x06000008 RID: 8 RVA: 0x00002392 File Offset: 0x00000592
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey) : this(awsAccessKeyId, awsSecretAccessKey, new AmazonSimpleNotificationServiceConfig())
		{
		}

		// Token: 0x06000009 RID: 9 RVA: 0x000023A1 File Offset: 0x000005A1
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey, RegionEndpoint region) : this(awsAccessKeyId, awsSecretAccessKey, new AmazonSimpleNotificationServiceConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000023B7 File Offset: 0x000005B7
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey, AmazonSimpleNotificationServiceConfig clientConfig) : base(awsAccessKeyId, awsSecretAccessKey, clientConfig)
		{
		}

		// Token: 0x0600000B RID: 11 RVA: 0x000023C2 File Offset: 0x000005C2
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken) : this(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, new AmazonSimpleNotificationServiceConfig())
		{
		}

		// Token: 0x0600000C RID: 12 RVA: 0x000023D2 File Offset: 0x000005D2
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken, RegionEndpoint region) : this(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, new AmazonSimpleNotificationServiceConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000023EA File Offset: 0x000005EA
		public AmazonSimpleNotificationServiceClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken, AmazonSimpleNotificationServiceConfig clientConfig) : base(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, clientConfig)
		{
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000023F7 File Offset: 0x000005F7
		protected override AbstractAWSSigner CreateSigner()
		{
			return new AWS4Signer();
		}

		// Token: 0x0600000F RID: 15 RVA: 0x000023FE File Offset: 0x000005FE
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002408 File Offset: 0x00000608
		public void AddPermissionAsync(string topicArn, string label, List<string> awsAccountId, List<string> actionName, AmazonServiceCallback<AddPermissionRequest, AddPermissionResponse> callback, AsyncOptions options = null)
		{
			this.AddPermissionAsync(new AddPermissionRequest
			{
				TopicArn = topicArn,
				Label = label,
				AWSAccountId = awsAccountId,
				ActionName = actionName
			}, callback, options);
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002444 File Offset: 0x00000644
		public void AddPermissionAsync(AddPermissionRequest request, AmazonServiceCallback<AddPermissionRequest, AddPermissionResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			AddPermissionRequestMarshaller marshaller = new AddPermissionRequestMarshaller();
			AddPermissionResponseUnmarshaller instance = AddPermissionResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<AddPermissionRequest, AddPermissionResponse> responseObject = new AmazonServiceResult<AddPermissionRequest, AddPermissionResponse>((AddPermissionRequest)req, (AddPermissionResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<AddPermissionRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x0000249C File Offset: 0x0000069C
		public void CheckIfPhoneNumberIsOptedOutAsync(CheckIfPhoneNumberIsOptedOutRequest request, AmazonServiceCallback<CheckIfPhoneNumberIsOptedOutRequest, CheckIfPhoneNumberIsOptedOutResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CheckIfPhoneNumberIsOptedOutRequestMarshaller marshaller = new CheckIfPhoneNumberIsOptedOutRequestMarshaller();
			CheckIfPhoneNumberIsOptedOutResponseUnmarshaller instance = CheckIfPhoneNumberIsOptedOutResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<CheckIfPhoneNumberIsOptedOutRequest, CheckIfPhoneNumberIsOptedOutResponse> responseObject = new AmazonServiceResult<CheckIfPhoneNumberIsOptedOutRequest, CheckIfPhoneNumberIsOptedOutResponse>((CheckIfPhoneNumberIsOptedOutRequest)req, (CheckIfPhoneNumberIsOptedOutResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<CheckIfPhoneNumberIsOptedOutRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000024F4 File Offset: 0x000006F4
		public void ConfirmSubscriptionAsync(string topicArn, string token, string authenticateOnUnsubscribe, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null)
		{
			this.ConfirmSubscriptionAsync(new ConfirmSubscriptionRequest
			{
				TopicArn = topicArn,
				Token = token,
				AuthenticateOnUnsubscribe = authenticateOnUnsubscribe
			}, callback, options);
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002528 File Offset: 0x00000728
		public void ConfirmSubscriptionAsync(string topicArn, string token, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null)
		{
			this.ConfirmSubscriptionAsync(new ConfirmSubscriptionRequest
			{
				TopicArn = topicArn,
				Token = token
			}, callback, options);
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002554 File Offset: 0x00000754
		public void ConfirmSubscriptionAsync(ConfirmSubscriptionRequest request, AmazonServiceCallback<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ConfirmSubscriptionRequestMarshaller marshaller = new ConfirmSubscriptionRequestMarshaller();
			ConfirmSubscriptionResponseUnmarshaller instance = ConfirmSubscriptionResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse> responseObject = new AmazonServiceResult<ConfirmSubscriptionRequest, ConfirmSubscriptionResponse>((ConfirmSubscriptionRequest)req, (ConfirmSubscriptionResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ConfirmSubscriptionRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000025AC File Offset: 0x000007AC
		public void CreatePlatformApplicationAsync(CreatePlatformApplicationRequest request, AmazonServiceCallback<CreatePlatformApplicationRequest, CreatePlatformApplicationResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CreatePlatformApplicationRequestMarshaller marshaller = new CreatePlatformApplicationRequestMarshaller();
			CreatePlatformApplicationResponseUnmarshaller instance = CreatePlatformApplicationResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<CreatePlatformApplicationRequest, CreatePlatformApplicationResponse> responseObject = new AmazonServiceResult<CreatePlatformApplicationRequest, CreatePlatformApplicationResponse>((CreatePlatformApplicationRequest)req, (CreatePlatformApplicationResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<CreatePlatformApplicationRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002604 File Offset: 0x00000804
		public void CreatePlatformEndpointAsync(CreatePlatformEndpointRequest request, AmazonServiceCallback<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CreatePlatformEndpointRequestMarshaller marshaller = new CreatePlatformEndpointRequestMarshaller();
			CreatePlatformEndpointResponseUnmarshaller instance = CreatePlatformEndpointResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse> responseObject = new AmazonServiceResult<CreatePlatformEndpointRequest, CreatePlatformEndpointResponse>((CreatePlatformEndpointRequest)req, (CreatePlatformEndpointResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<CreatePlatformEndpointRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x0000265C File Offset: 0x0000085C
		public void CreateTopicAsync(string name, AmazonServiceCallback<CreateTopicRequest, CreateTopicResponse> callback, AsyncOptions options = null)
		{
			this.CreateTopicAsync(new CreateTopicRequest
			{
				Name = name
			}, callback, options);
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002680 File Offset: 0x00000880
		public void CreateTopicAsync(CreateTopicRequest request, AmazonServiceCallback<CreateTopicRequest, CreateTopicResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CreateTopicRequestMarshaller marshaller = new CreateTopicRequestMarshaller();
			CreateTopicResponseUnmarshaller instance = CreateTopicResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<CreateTopicRequest, CreateTopicResponse> responseObject = new AmazonServiceResult<CreateTopicRequest, CreateTopicResponse>((CreateTopicRequest)req, (CreateTopicResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<CreateTopicRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600001A RID: 26 RVA: 0x000026D8 File Offset: 0x000008D8
		public void DeleteEndpointAsync(DeleteEndpointRequest request, AmazonServiceCallback<DeleteEndpointRequest, DeleteEndpointResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DeleteEndpointRequestMarshaller marshaller = new DeleteEndpointRequestMarshaller();
			DeleteEndpointResponseUnmarshaller instance = DeleteEndpointResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DeleteEndpointRequest, DeleteEndpointResponse> responseObject = new AmazonServiceResult<DeleteEndpointRequest, DeleteEndpointResponse>((DeleteEndpointRequest)req, (DeleteEndpointResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DeleteEndpointRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002730 File Offset: 0x00000930
		public void DeletePlatformApplicationAsync(DeletePlatformApplicationRequest request, AmazonServiceCallback<DeletePlatformApplicationRequest, DeletePlatformApplicationResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DeletePlatformApplicationRequestMarshaller marshaller = new DeletePlatformApplicationRequestMarshaller();
			DeletePlatformApplicationResponseUnmarshaller instance = DeletePlatformApplicationResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DeletePlatformApplicationRequest, DeletePlatformApplicationResponse> responseObject = new AmazonServiceResult<DeletePlatformApplicationRequest, DeletePlatformApplicationResponse>((DeletePlatformApplicationRequest)req, (DeletePlatformApplicationResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DeletePlatformApplicationRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002788 File Offset: 0x00000988
		public void DeleteTopicAsync(string topicArn, AmazonServiceCallback<DeleteTopicRequest, DeleteTopicResponse> callback, AsyncOptions options = null)
		{
			this.DeleteTopicAsync(new DeleteTopicRequest
			{
				TopicArn = topicArn
			}, callback, options);
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000027AC File Offset: 0x000009AC
		public void DeleteTopicAsync(DeleteTopicRequest request, AmazonServiceCallback<DeleteTopicRequest, DeleteTopicResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DeleteTopicRequestMarshaller marshaller = new DeleteTopicRequestMarshaller();
			DeleteTopicResponseUnmarshaller instance = DeleteTopicResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DeleteTopicRequest, DeleteTopicResponse> responseObject = new AmazonServiceResult<DeleteTopicRequest, DeleteTopicResponse>((DeleteTopicRequest)req, (DeleteTopicResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DeleteTopicRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002804 File Offset: 0x00000A04
		public void GetEndpointAttributesAsync(GetEndpointAttributesRequest request, AmazonServiceCallback<GetEndpointAttributesRequest, GetEndpointAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetEndpointAttributesRequestMarshaller marshaller = new GetEndpointAttributesRequestMarshaller();
			GetEndpointAttributesResponseUnmarshaller instance = GetEndpointAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetEndpointAttributesRequest, GetEndpointAttributesResponse> responseObject = new AmazonServiceResult<GetEndpointAttributesRequest, GetEndpointAttributesResponse>((GetEndpointAttributesRequest)req, (GetEndpointAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetEndpointAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000285C File Offset: 0x00000A5C
		public void GetPlatformApplicationAttributesAsync(GetPlatformApplicationAttributesRequest request, AmazonServiceCallback<GetPlatformApplicationAttributesRequest, GetPlatformApplicationAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetPlatformApplicationAttributesRequestMarshaller marshaller = new GetPlatformApplicationAttributesRequestMarshaller();
			GetPlatformApplicationAttributesResponseUnmarshaller instance = GetPlatformApplicationAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetPlatformApplicationAttributesRequest, GetPlatformApplicationAttributesResponse> responseObject = new AmazonServiceResult<GetPlatformApplicationAttributesRequest, GetPlatformApplicationAttributesResponse>((GetPlatformApplicationAttributesRequest)req, (GetPlatformApplicationAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetPlatformApplicationAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x000028B4 File Offset: 0x00000AB4
		public void GetSMSAttributesAsync(GetSMSAttributesRequest request, AmazonServiceCallback<GetSMSAttributesRequest, GetSMSAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetSMSAttributesRequestMarshaller marshaller = new GetSMSAttributesRequestMarshaller();
			GetSMSAttributesResponseUnmarshaller instance = GetSMSAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetSMSAttributesRequest, GetSMSAttributesResponse> responseObject = new AmazonServiceResult<GetSMSAttributesRequest, GetSMSAttributesResponse>((GetSMSAttributesRequest)req, (GetSMSAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetSMSAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x0000290C File Offset: 0x00000B0C
		public void GetSubscriptionAttributesAsync(string subscriptionArn, AmazonServiceCallback<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse> callback, AsyncOptions options = null)
		{
			this.GetSubscriptionAttributesAsync(new GetSubscriptionAttributesRequest
			{
				SubscriptionArn = subscriptionArn
			}, callback, options);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002930 File Offset: 0x00000B30
		public void GetSubscriptionAttributesAsync(GetSubscriptionAttributesRequest request, AmazonServiceCallback<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetSubscriptionAttributesRequestMarshaller marshaller = new GetSubscriptionAttributesRequestMarshaller();
			GetSubscriptionAttributesResponseUnmarshaller instance = GetSubscriptionAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse> responseObject = new AmazonServiceResult<GetSubscriptionAttributesRequest, GetSubscriptionAttributesResponse>((GetSubscriptionAttributesRequest)req, (GetSubscriptionAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetSubscriptionAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002988 File Offset: 0x00000B88
		public void GetTopicAttributesAsync(string topicArn, AmazonServiceCallback<GetTopicAttributesRequest, GetTopicAttributesResponse> callback, AsyncOptions options = null)
		{
			this.GetTopicAttributesAsync(new GetTopicAttributesRequest
			{
				TopicArn = topicArn
			}, callback, options);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000029AC File Offset: 0x00000BAC
		public void GetTopicAttributesAsync(GetTopicAttributesRequest request, AmazonServiceCallback<GetTopicAttributesRequest, GetTopicAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetTopicAttributesRequestMarshaller marshaller = new GetTopicAttributesRequestMarshaller();
			GetTopicAttributesResponseUnmarshaller instance = GetTopicAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetTopicAttributesRequest, GetTopicAttributesResponse> responseObject = new AmazonServiceResult<GetTopicAttributesRequest, GetTopicAttributesResponse>((GetTopicAttributesRequest)req, (GetTopicAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetTopicAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002A04 File Offset: 0x00000C04
		public void ListEndpointsByPlatformApplicationAsync(ListEndpointsByPlatformApplicationRequest request, AmazonServiceCallback<ListEndpointsByPlatformApplicationRequest, ListEndpointsByPlatformApplicationResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListEndpointsByPlatformApplicationRequestMarshaller marshaller = new ListEndpointsByPlatformApplicationRequestMarshaller();
			ListEndpointsByPlatformApplicationResponseUnmarshaller instance = ListEndpointsByPlatformApplicationResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListEndpointsByPlatformApplicationRequest, ListEndpointsByPlatformApplicationResponse> responseObject = new AmazonServiceResult<ListEndpointsByPlatformApplicationRequest, ListEndpointsByPlatformApplicationResponse>((ListEndpointsByPlatformApplicationRequest)req, (ListEndpointsByPlatformApplicationResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListEndpointsByPlatformApplicationRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x00002A5C File Offset: 0x00000C5C
		public void ListPhoneNumbersOptedOutAsync(ListPhoneNumbersOptedOutRequest request, AmazonServiceCallback<ListPhoneNumbersOptedOutRequest, ListPhoneNumbersOptedOutResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListPhoneNumbersOptedOutRequestMarshaller marshaller = new ListPhoneNumbersOptedOutRequestMarshaller();
			ListPhoneNumbersOptedOutResponseUnmarshaller instance = ListPhoneNumbersOptedOutResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListPhoneNumbersOptedOutRequest, ListPhoneNumbersOptedOutResponse> responseObject = new AmazonServiceResult<ListPhoneNumbersOptedOutRequest, ListPhoneNumbersOptedOutResponse>((ListPhoneNumbersOptedOutRequest)req, (ListPhoneNumbersOptedOutResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListPhoneNumbersOptedOutRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002AB2 File Offset: 0x00000CB2
		public void ListPlatformApplicationsAsync(AmazonServiceCallback<ListPlatformApplicationsRequest, ListPlatformApplicationsResponse> callback, AsyncOptions options = null)
		{
			this.ListPlatformApplicationsAsync(new ListPlatformApplicationsRequest(), callback, options);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00002AC4 File Offset: 0x00000CC4
		public void ListPlatformApplicationsAsync(ListPlatformApplicationsRequest request, AmazonServiceCallback<ListPlatformApplicationsRequest, ListPlatformApplicationsResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListPlatformApplicationsRequestMarshaller marshaller = new ListPlatformApplicationsRequestMarshaller();
			ListPlatformApplicationsResponseUnmarshaller instance = ListPlatformApplicationsResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListPlatformApplicationsRequest, ListPlatformApplicationsResponse> responseObject = new AmazonServiceResult<ListPlatformApplicationsRequest, ListPlatformApplicationsResponse>((ListPlatformApplicationsRequest)req, (ListPlatformApplicationsResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListPlatformApplicationsRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002B1A File Offset: 0x00000D1A
		public void ListSubscriptionsAsync(AmazonServiceCallback<ListSubscriptionsRequest, ListSubscriptionsResponse> callback, AsyncOptions options = null)
		{
			this.ListSubscriptionsAsync(new ListSubscriptionsRequest(), callback, options);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002B2C File Offset: 0x00000D2C
		public void ListSubscriptionsAsync(string nextToken, AmazonServiceCallback<ListSubscriptionsRequest, ListSubscriptionsResponse> callback, AsyncOptions options = null)
		{
			this.ListSubscriptionsAsync(new ListSubscriptionsRequest
			{
				NextToken = nextToken
			}, callback, options);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x00002B50 File Offset: 0x00000D50
		public void ListSubscriptionsAsync(ListSubscriptionsRequest request, AmazonServiceCallback<ListSubscriptionsRequest, ListSubscriptionsResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListSubscriptionsRequestMarshaller marshaller = new ListSubscriptionsRequestMarshaller();
			ListSubscriptionsResponseUnmarshaller instance = ListSubscriptionsResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListSubscriptionsRequest, ListSubscriptionsResponse> responseObject = new AmazonServiceResult<ListSubscriptionsRequest, ListSubscriptionsResponse>((ListSubscriptionsRequest)req, (ListSubscriptionsResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListSubscriptionsRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00002BA8 File Offset: 0x00000DA8
		public void ListSubscriptionsByTopicAsync(string topicArn, string nextToken, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null)
		{
			this.ListSubscriptionsByTopicAsync(new ListSubscriptionsByTopicRequest
			{
				TopicArn = topicArn,
				NextToken = nextToken
			}, callback, options);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00002BD4 File Offset: 0x00000DD4
		public void ListSubscriptionsByTopicAsync(string topicArn, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null)
		{
			this.ListSubscriptionsByTopicAsync(new ListSubscriptionsByTopicRequest
			{
				TopicArn = topicArn
			}, callback, options);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002BF8 File Offset: 0x00000DF8
		public void ListSubscriptionsByTopicAsync(ListSubscriptionsByTopicRequest request, AmazonServiceCallback<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListSubscriptionsByTopicRequestMarshaller marshaller = new ListSubscriptionsByTopicRequestMarshaller();
			ListSubscriptionsByTopicResponseUnmarshaller instance = ListSubscriptionsByTopicResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse> responseObject = new AmazonServiceResult<ListSubscriptionsByTopicRequest, ListSubscriptionsByTopicResponse>((ListSubscriptionsByTopicRequest)req, (ListSubscriptionsByTopicResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListSubscriptionsByTopicRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002C4E File Offset: 0x00000E4E
		public void ListTopicsAsync(AmazonServiceCallback<ListTopicsRequest, ListTopicsResponse> callback, AsyncOptions options = null)
		{
			this.ListTopicsAsync(new ListTopicsRequest(), callback, options);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002C60 File Offset: 0x00000E60
		public void ListTopicsAsync(string nextToken, AmazonServiceCallback<ListTopicsRequest, ListTopicsResponse> callback, AsyncOptions options = null)
		{
			this.ListTopicsAsync(new ListTopicsRequest
			{
				NextToken = nextToken
			}, callback, options);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002C84 File Offset: 0x00000E84
		public void ListTopicsAsync(ListTopicsRequest request, AmazonServiceCallback<ListTopicsRequest, ListTopicsResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListTopicsRequestMarshaller marshaller = new ListTopicsRequestMarshaller();
			ListTopicsResponseUnmarshaller instance = ListTopicsResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListTopicsRequest, ListTopicsResponse> responseObject = new AmazonServiceResult<ListTopicsRequest, ListTopicsResponse>((ListTopicsRequest)req, (ListTopicsResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListTopicsRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002CDC File Offset: 0x00000EDC
		public void OptInPhoneNumberAsync(OptInPhoneNumberRequest request, AmazonServiceCallback<OptInPhoneNumberRequest, OptInPhoneNumberResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			OptInPhoneNumberRequestMarshaller marshaller = new OptInPhoneNumberRequestMarshaller();
			OptInPhoneNumberResponseUnmarshaller instance = OptInPhoneNumberResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<OptInPhoneNumberRequest, OptInPhoneNumberResponse> responseObject = new AmazonServiceResult<OptInPhoneNumberRequest, OptInPhoneNumberResponse>((OptInPhoneNumberRequest)req, (OptInPhoneNumberResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<OptInPhoneNumberRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00002D34 File Offset: 0x00000F34
		public void PublishAsync(string topicArn, string message, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null)
		{
			this.PublishAsync(new PublishRequest
			{
				TopicArn = topicArn,
				Message = message
			}, callback, options);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002D60 File Offset: 0x00000F60
		public void PublishAsync(string topicArn, string message, string subject, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null)
		{
			this.PublishAsync(new PublishRequest
			{
				TopicArn = topicArn,
				Message = message,
				Subject = subject
			}, callback, options);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002D94 File Offset: 0x00000F94
		public void PublishAsync(PublishRequest request, AmazonServiceCallback<PublishRequest, PublishResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			PublishRequestMarshaller marshaller = new PublishRequestMarshaller();
			PublishResponseUnmarshaller instance = PublishResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<PublishRequest, PublishResponse> responseObject = new AmazonServiceResult<PublishRequest, PublishResponse>((PublishRequest)req, (PublishResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<PublishRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002DEC File Offset: 0x00000FEC
		public void RemovePermissionAsync(string topicArn, string label, AmazonServiceCallback<RemovePermissionRequest, RemovePermissionResponse> callback, AsyncOptions options = null)
		{
			this.RemovePermissionAsync(new RemovePermissionRequest
			{
				TopicArn = topicArn,
				Label = label
			}, callback, options);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002E18 File Offset: 0x00001018
		public void RemovePermissionAsync(RemovePermissionRequest request, AmazonServiceCallback<RemovePermissionRequest, RemovePermissionResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			RemovePermissionRequestMarshaller marshaller = new RemovePermissionRequestMarshaller();
			RemovePermissionResponseUnmarshaller instance = RemovePermissionResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<RemovePermissionRequest, RemovePermissionResponse> responseObject = new AmazonServiceResult<RemovePermissionRequest, RemovePermissionResponse>((RemovePermissionRequest)req, (RemovePermissionResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<RemovePermissionRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002E70 File Offset: 0x00001070
		public void SetEndpointAttributesAsync(SetEndpointAttributesRequest request, AmazonServiceCallback<SetEndpointAttributesRequest, SetEndpointAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetEndpointAttributesRequestMarshaller marshaller = new SetEndpointAttributesRequestMarshaller();
			SetEndpointAttributesResponseUnmarshaller instance = SetEndpointAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetEndpointAttributesRequest, SetEndpointAttributesResponse> responseObject = new AmazonServiceResult<SetEndpointAttributesRequest, SetEndpointAttributesResponse>((SetEndpointAttributesRequest)req, (SetEndpointAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetEndpointAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00002EC8 File Offset: 0x000010C8
		public void SetPlatformApplicationAttributesAsync(SetPlatformApplicationAttributesRequest request, AmazonServiceCallback<SetPlatformApplicationAttributesRequest, SetPlatformApplicationAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetPlatformApplicationAttributesRequestMarshaller marshaller = new SetPlatformApplicationAttributesRequestMarshaller();
			SetPlatformApplicationAttributesResponseUnmarshaller instance = SetPlatformApplicationAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetPlatformApplicationAttributesRequest, SetPlatformApplicationAttributesResponse> responseObject = new AmazonServiceResult<SetPlatformApplicationAttributesRequest, SetPlatformApplicationAttributesResponse>((SetPlatformApplicationAttributesRequest)req, (SetPlatformApplicationAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetPlatformApplicationAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002F20 File Offset: 0x00001120
		public void SetSMSAttributesAsync(SetSMSAttributesRequest request, AmazonServiceCallback<SetSMSAttributesRequest, SetSMSAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetSMSAttributesRequestMarshaller marshaller = new SetSMSAttributesRequestMarshaller();
			SetSMSAttributesResponseUnmarshaller instance = SetSMSAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetSMSAttributesRequest, SetSMSAttributesResponse> responseObject = new AmazonServiceResult<SetSMSAttributesRequest, SetSMSAttributesResponse>((SetSMSAttributesRequest)req, (SetSMSAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetSMSAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002F78 File Offset: 0x00001178
		public void SetSubscriptionAttributesAsync(string subscriptionArn, string attributeName, string attributeValue, AmazonServiceCallback<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse> callback, AsyncOptions options = null)
		{
			this.SetSubscriptionAttributesAsync(new SetSubscriptionAttributesRequest
			{
				SubscriptionArn = subscriptionArn,
				AttributeName = attributeName,
				AttributeValue = attributeValue
			}, callback, options);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00002FAC File Offset: 0x000011AC
		public void SetSubscriptionAttributesAsync(SetSubscriptionAttributesRequest request, AmazonServiceCallback<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetSubscriptionAttributesRequestMarshaller marshaller = new SetSubscriptionAttributesRequestMarshaller();
			SetSubscriptionAttributesResponseUnmarshaller instance = SetSubscriptionAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse> responseObject = new AmazonServiceResult<SetSubscriptionAttributesRequest, SetSubscriptionAttributesResponse>((SetSubscriptionAttributesRequest)req, (SetSubscriptionAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetSubscriptionAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00003004 File Offset: 0x00001204
		public void SetTopicAttributesAsync(string topicArn, string attributeName, string attributeValue, AmazonServiceCallback<SetTopicAttributesRequest, SetTopicAttributesResponse> callback, AsyncOptions options = null)
		{
			this.SetTopicAttributesAsync(new SetTopicAttributesRequest
			{
				TopicArn = topicArn,
				AttributeName = attributeName,
				AttributeValue = attributeValue
			}, callback, options);
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00003038 File Offset: 0x00001238
		public void SetTopicAttributesAsync(SetTopicAttributesRequest request, AmazonServiceCallback<SetTopicAttributesRequest, SetTopicAttributesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetTopicAttributesRequestMarshaller marshaller = new SetTopicAttributesRequestMarshaller();
			SetTopicAttributesResponseUnmarshaller instance = SetTopicAttributesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetTopicAttributesRequest, SetTopicAttributesResponse> responseObject = new AmazonServiceResult<SetTopicAttributesRequest, SetTopicAttributesResponse>((SetTopicAttributesRequest)req, (SetTopicAttributesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetTopicAttributesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003090 File Offset: 0x00001290
		public void SubscribeAsync(string topicArn, string protocol, string endpoint, AmazonServiceCallback<SubscribeRequest, SubscribeResponse> callback, AsyncOptions options = null)
		{
			this.SubscribeAsync(new SubscribeRequest
			{
				TopicArn = topicArn,
				Protocol = protocol,
				Endpoint = endpoint
			}, callback, options);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000030C4 File Offset: 0x000012C4
		public void SubscribeAsync(SubscribeRequest request, AmazonServiceCallback<SubscribeRequest, SubscribeResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SubscribeRequestMarshaller marshaller = new SubscribeRequestMarshaller();
			SubscribeResponseUnmarshaller instance = SubscribeResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SubscribeRequest, SubscribeResponse> responseObject = new AmazonServiceResult<SubscribeRequest, SubscribeResponse>((SubscribeRequest)req, (SubscribeResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SubscribeRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000311C File Offset: 0x0000131C
		public void UnsubscribeAsync(string subscriptionArn, AmazonServiceCallback<UnsubscribeRequest, UnsubscribeResponse> callback, AsyncOptions options = null)
		{
			this.UnsubscribeAsync(new UnsubscribeRequest
			{
				SubscriptionArn = subscriptionArn
			}, callback, options);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003140 File Offset: 0x00001340
		public void UnsubscribeAsync(UnsubscribeRequest request, AmazonServiceCallback<UnsubscribeRequest, UnsubscribeResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			UnsubscribeRequestMarshaller marshaller = new UnsubscribeRequestMarshaller();
			UnsubscribeResponseUnmarshaller instance = UnsubscribeResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<UnsubscribeRequest, UnsubscribeResponse> responseObject = new AmazonServiceResult<UnsubscribeRequest, UnsubscribeResponse>((UnsubscribeRequest)req, (UnsubscribeResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<UnsubscribeRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00003196 File Offset: 0x00001396
		IClientConfig IAmazonService.get_Config()
		{
			return base.Config;
		}
	}
}
