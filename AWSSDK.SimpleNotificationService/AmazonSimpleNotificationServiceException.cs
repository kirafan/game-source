﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.SimpleNotificationService
{
	// Token: 0x02000005 RID: 5
	[Serializable]
	public class AmazonSimpleNotificationServiceException : AmazonServiceException
	{
		// Token: 0x06000079 RID: 121 RVA: 0x000031E3 File Offset: 0x000013E3
		public AmazonSimpleNotificationServiceException(string message) : base(message)
		{
		}

		// Token: 0x0600007A RID: 122 RVA: 0x000031EC File Offset: 0x000013EC
		public AmazonSimpleNotificationServiceException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600007B RID: 123 RVA: 0x000031F6 File Offset: 0x000013F6
		public AmazonSimpleNotificationServiceException(Exception innerException) : base(innerException.Message, innerException)
		{
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003205 File Offset: 0x00001405
		public AmazonSimpleNotificationServiceException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00003214 File Offset: 0x00001414
		public AmazonSimpleNotificationServiceException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00003225 File Offset: 0x00001425
		protected AmazonSimpleNotificationServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
