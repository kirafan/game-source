﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE6 RID: 3046
	public class Gemlevelup
	{
		// Token: 0x040045B1 RID: 17841
		public long managedTownFacilityId;

		// Token: 0x040045B2 RID: 17842
		public int nextLevel;

		// Token: 0x040045B3 RID: 17843
		public int openState;

		// Token: 0x040045B4 RID: 17844
		public long actionTime;

		// Token: 0x040045B5 RID: 17845
		public long remainingTime;
	}
}
