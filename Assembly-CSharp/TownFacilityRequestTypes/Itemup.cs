﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE7 RID: 3047
	public class Itemup
	{
		// Token: 0x040045B6 RID: 17846
		public long managedTownFacilityId;

		// Token: 0x040045B7 RID: 17847
		public int itemNo;

		// Token: 0x040045B8 RID: 17848
		public int amount;

		// Token: 0x040045B9 RID: 17849
		public long actionTime;
	}
}
