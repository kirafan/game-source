﻿using System;
using WWWTypes;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE2 RID: 3042
	public class Buildpointsetall
	{
		// Token: 0x040045A6 RID: 17830
		public TownFacilitySetState[] townFacilitySetStates;
	}
}
