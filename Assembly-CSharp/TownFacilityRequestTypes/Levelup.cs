﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE5 RID: 3045
	public class Levelup
	{
		// Token: 0x040045AD RID: 17837
		public long managedTownFacilityId;

		// Token: 0x040045AE RID: 17838
		public int nextLevel;

		// Token: 0x040045AF RID: 17839
		public int openState;

		// Token: 0x040045B0 RID: 17840
		public long actionTime;
	}
}
