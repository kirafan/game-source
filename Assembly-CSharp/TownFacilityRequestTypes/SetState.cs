﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE9 RID: 3049
	public class SetState
	{
		// Token: 0x040045BA RID: 17850
		public long managedTownFacilityId;

		// Token: 0x040045BB RID: 17851
		public int openState;
	}
}
