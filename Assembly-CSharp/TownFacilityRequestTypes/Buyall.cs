﻿using System;
using WWWTypes;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE0 RID: 3040
	public class Buyall
	{
		// Token: 0x040045A0 RID: 17824
		public TownFacilityBuyState[] townFacilityBuyStates;
	}
}
