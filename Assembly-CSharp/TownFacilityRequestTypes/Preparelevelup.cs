﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE4 RID: 3044
	public class Preparelevelup
	{
		// Token: 0x040045A9 RID: 17833
		public long managedTownFacilityId;

		// Token: 0x040045AA RID: 17834
		public int nextLevel;

		// Token: 0x040045AB RID: 17835
		public int openState;

		// Token: 0x040045AC RID: 17836
		public long buildTime;
	}
}
