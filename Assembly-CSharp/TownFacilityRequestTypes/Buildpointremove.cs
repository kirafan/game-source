﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE3 RID: 3043
	public class Buildpointremove
	{
		// Token: 0x040045A7 RID: 17831
		public long managedTownFacilityId;

		// Token: 0x040045A8 RID: 17832
		public int openState;
	}
}
