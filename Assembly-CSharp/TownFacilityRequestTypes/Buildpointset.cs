﻿using System;

namespace TownFacilityRequestTypes
{
	// Token: 0x02000BE1 RID: 3041
	public class Buildpointset
	{
		// Token: 0x040045A1 RID: 17825
		public long managedTownFacilityId;

		// Token: 0x040045A2 RID: 17826
		public int buildPointIndex;

		// Token: 0x040045A3 RID: 17827
		public int openState;

		// Token: 0x040045A4 RID: 17828
		public long buildTime;

		// Token: 0x040045A5 RID: 17829
		public int actionNo;
	}
}
