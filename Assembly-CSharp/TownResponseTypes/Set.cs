﻿using System;
using CommonResponseTypes;

namespace TownResponseTypes
{
	// Token: 0x02000C8C RID: 3212
	public class Set : CommonResponse
	{
		// Token: 0x060040D1 RID: 16593 RVA: 0x0013FD64 File Offset: 0x0013E164
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + this.managedTownId.ToString();
		}

		// Token: 0x04004694 RID: 18068
		public long managedTownId;
	}
}
