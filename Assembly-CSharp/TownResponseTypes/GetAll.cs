﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace TownResponseTypes
{
	// Token: 0x02000C8D RID: 3213
	public class GetAll : CommonResponse
	{
		// Token: 0x060040D3 RID: 16595 RVA: 0x0013FD9C File Offset: 0x0013E19C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedTowns == null) ? string.Empty : this.managedTowns.ToString());
		}

		// Token: 0x04004695 RID: 18069
		public PlayerTown[] managedTowns;
	}
}
