﻿using System;
using MasterOrbRequestTypes;
using Meige;

// Token: 0x02000B65 RID: 2917
public static class MasterOrbRequest
{
	// Token: 0x06003E19 RID: 15897 RVA: 0x001391E8 File Offset: 0x001375E8
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E1A RID: 15898 RVA: 0x0013920C File Offset: 0x0013760C
	public static MeigewwwParam Getall(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("master_orb/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E1B RID: 15899 RVA: 0x00139230 File Offset: 0x00137630
	public static MeigewwwParam Get(Get param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("master_orb/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("masterOrbId", param.masterOrbId);
		return meigewwwParam;
	}

	// Token: 0x06003E1C RID: 15900 RVA: 0x00139264 File Offset: 0x00137664
	public static MeigewwwParam Get(string masterOrbId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("master_orb/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("masterOrbId", masterOrbId);
		return meigewwwParam;
	}
}
