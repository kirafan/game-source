﻿using System;
using Meige;
using QuestRequestTypes;

// Token: 0x02000B69 RID: 2921
public static class QuestRequest
{
	// Token: 0x06003E77 RID: 15991 RVA: 0x0013A928 File Offset: 0x00138D28
	public static MeigewwwParam Getall(Getall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", param.type);
		return meigewwwParam;
	}

	// Token: 0x06003E78 RID: 15992 RVA: 0x0013A960 File Offset: 0x00138D60
	public static MeigewwwParam Getall(int type, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/quest/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("type", type);
		return meigewwwParam;
	}
}
