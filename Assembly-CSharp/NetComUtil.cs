﻿using System;
using System.Reflection;

// Token: 0x02000354 RID: 852
public class NetComUtil
{
	// Token: 0x0600103C RID: 4156 RVA: 0x00056068 File Offset: 0x00054468
	public static string ToString(object ptarget)
	{
		string text = string.Empty;
		Type type = ptarget.GetType();
		FieldInfo[] fields = type.GetFields();
		for (int i = 0; i < fields.Length; i++)
		{
			if (i != 0)
			{
				text += ",";
			}
			if (fields[i].FieldType.IsValueType)
			{
				text += string.Format("\"{0}\":\"{1}\"", fields[i].Name, fields[i].GetValue(ptarget).ToString());
			}
			else
			{
				text += string.Format("\"{0}\":\"{1}\"", fields[i].Name, fields[i].GetValue(ptarget));
			}
		}
		return text;
	}
}
