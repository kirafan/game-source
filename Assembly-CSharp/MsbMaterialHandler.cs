﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000FC1 RID: 4033
[Serializable]
public class MsbMaterialHandler
{
	// Token: 0x06005412 RID: 21522 RVA: 0x00178777 File Offset: 0x00176B77
	public void Init(MsbHandler owner)
	{
		this.m_Work = new MsbMaterialHandler.MsbMaterialParam(this.m_Src);
	}

	// Token: 0x06005413 RID: 21523 RVA: 0x0017878A File Offset: 0x00176B8A
	public void UpdateParam()
	{
		this.m_Work.UpdateParam();
	}

	// Token: 0x06005414 RID: 21524 RVA: 0x00178797 File Offset: 0x00176B97
	public MsbMaterialHandler.MsbMaterialParam GetWork()
	{
		return this.m_Work;
	}

	// Token: 0x040054E9 RID: 21737
	public string m_Name;

	// Token: 0x040054EA RID: 21738
	public int m_RefID;

	// Token: 0x040054EB RID: 21739
	public MsbMaterialHandler.MsbMaterialParam m_Src;

	// Token: 0x040054EC RID: 21740
	private MsbMaterialHandler.MsbMaterialParam m_Work;

	// Token: 0x02000FC2 RID: 4034
	[Serializable]
	public class MsbTextureParam
	{
		// Token: 0x06005415 RID: 21525 RVA: 0x0017879F File Offset: 0x00176B9F
		public MsbTextureParam()
		{
		}

		// Token: 0x06005416 RID: 21526 RVA: 0x001787A8 File Offset: 0x00176BA8
		public MsbTextureParam(MsbMaterialHandler.MsbTextureParam src)
		{
			this.m_eType = src.m_eType;
			this.m_Layer = src.m_Layer;
			this.m_CoverageUV = src.m_CoverageUV;
			this.m_TranslationUV = src.m_TranslationUV;
			this.m_OffsetUV = src.m_OffsetUV;
			this.m_RotateUV = src.m_RotateUV;
			this.m_LayerBlendMode = src.m_LayerBlendMode;
			this.m_LayerBlendModeAlpha = src.m_LayerBlendModeAlpha;
			this.m_Name = src.m_Name;
		}

		// Token: 0x06005417 RID: 21527 RVA: 0x00178828 File Offset: 0x00176C28
		public void UpdateParam()
		{
			Quaternion q = Quaternion.Euler(new Vector3(0f, 0f, this.m_RotateUV));
			Matrix4x4 rhs = Matrix4x4.TRS(Vector3.zero, q, Vector3.one);
			float num = 1f / this.m_CoverageUV.x;
			float num2 = 1f / this.m_CoverageUV.y;
			this.m_UVMat = Matrix4x4.identity;
			this.m_UVMat *= Matrix4x4.Scale(new Vector3(num, num2, 1f));
			this.m_UVMat *= Matrix4x4.TRS(new Vector3(-0.5f * num, 0.5f * num2, 0f), Quaternion.identity, Vector3.one);
			this.m_UVMat *= rhs;
			this.m_UVMat *= Matrix4x4.TRS(new Vector3(0.5f * num, -0.5f * num2, 0f), Quaternion.identity, Vector3.one);
			Vector3 vector = new Vector3(this.m_OffsetUV.x, this.m_OffsetUV.y);
			vector = rhs.MultiplyPoint3x4(vector);
			Vector3 vector2 = new Vector3(-this.m_TranslationUV.x, -this.m_TranslationUV.y);
			vector2 = rhs.MultiplyPoint3x4(vector2);
			this.m_UVMat *= Matrix4x4.TRS(vector, Quaternion.identity, Vector3.one);
			this.m_UVMat *= Matrix4x4.TRS(vector2, Quaternion.identity, Vector3.one);
		}

		// Token: 0x06005418 RID: 21528 RVA: 0x001789CA File Offset: 0x00176DCA
		public Matrix4x4 GetMatrix()
		{
			return this.m_UVMat;
		}

		// Token: 0x040054ED RID: 21741
		public string m_Name;

		// Token: 0x040054EE RID: 21742
		public eTextureType m_eType;

		// Token: 0x040054EF RID: 21743
		public int m_Layer;

		// Token: 0x040054F0 RID: 21744
		public Vector2 m_CoverageUV;

		// Token: 0x040054F1 RID: 21745
		public Vector2 m_TranslationUV;

		// Token: 0x040054F2 RID: 21746
		public Vector2 m_OffsetUV;

		// Token: 0x040054F3 RID: 21747
		public float m_RotateUV;

		// Token: 0x040054F4 RID: 21748
		public eLayerBlendMode m_LayerBlendMode;

		// Token: 0x040054F5 RID: 21749
		public eLayerBlendMode m_LayerBlendModeAlpha;

		// Token: 0x040054F6 RID: 21750
		protected Matrix4x4 m_UVMat;
	}

	// Token: 0x02000FC3 RID: 4035
	[Serializable]
	public class MsbMaterialParam
	{
		// Token: 0x06005419 RID: 21529 RVA: 0x001789D2 File Offset: 0x00176DD2
		public MsbMaterialParam()
		{
		}

		// Token: 0x0600541A RID: 21530 RVA: 0x001789E8 File Offset: 0x00176DE8
		public MsbMaterialParam(MsbMaterialHandler.MsbMaterialParam src)
		{
			this.m_Diffuse = src.m_Diffuse;
			this.m_BlendMode = src.m_BlendMode;
			this.m_Texture = new MsbMaterialHandler.MsbTextureParam[src.m_Texture.Length];
			for (int i = 0; i < src.m_Texture.Length; i++)
			{
				this.m_Texture[i] = new MsbMaterialHandler.MsbTextureParam(src.m_Texture[i]);
			}
		}

		// Token: 0x0600541B RID: 21531 RVA: 0x00178A60 File Offset: 0x00176E60
		public int SearchTexIndex(eTextureType texType, int layerID)
		{
			int result = -1;
			for (int i = 0; i < this.m_Texture.Length; i++)
			{
				if (this.m_Texture[i].m_eType == texType && this.m_Texture[i].m_Layer == layerID)
				{
					result = i;
					break;
				}
			}
			return result;
		}

		// Token: 0x0600541C RID: 21532 RVA: 0x00178AB8 File Offset: 0x00176EB8
		public void UpdateParam()
		{
			foreach (MsbMaterialHandler.MsbTextureParam msbTextureParam in this.m_Texture)
			{
				msbTextureParam.UpdateParam();
			}
		}

		// Token: 0x040054F7 RID: 21751
		public Color m_Diffuse;

		// Token: 0x040054F8 RID: 21752
		public eBlendMode m_BlendMode;

		// Token: 0x040054F9 RID: 21753
		public MsbMaterialHandler.MsbTextureParam[] m_Texture;

		// Token: 0x040054FA RID: 21754
		public eBlendMode m_OutlineBlendMode;

		// Token: 0x040054FB RID: 21755
		public float m_AlphaTestRefValue = 0.01f;
	}
}
