﻿using System;

namespace StarApi.Models
{
	// Token: 0x02000C95 RID: 3221
	public static class StarDefine
	{
		// Token: 0x060040E2 RID: 16610 RVA: 0x0013FF67 File Offset: 0x0013E367
		public static long CalcRemainGemNum(long fremainTimes)
		{
			return (long)Math.Ceiling((double)fremainTimes / 1800000.0) * 10L;
		}

		// Token: 0x0400469A RID: 18074
		public const int SERVER_VERSION = 17071902;

		// Token: 0x0400469B RID: 18075
		public const long INVALID_MANAGED_ID = -1L;

		// Token: 0x0400469C RID: 18076
		public const int INVALID_ID = -1;

		// Token: 0x0400469D RID: 18077
		public const string REGEX_UUID = "^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$";

		// Token: 0x0400469E RID: 18078
		public const string DATETIME_WITH_TIMEZONE = "yyyy-MM-ddTHH\\:mm\\:sszzz";

		// Token: 0x0400469F RID: 18079
		public const string HEADER_SESSION_ID = "X-STAR-SESSION-ID";

		// Token: 0x040046A0 RID: 18080
		public const string CTX_ITEM_PLAYER_ID = "playerId";

		// Token: 0x040046A1 RID: 18081
		public const string HEADER_REQUEST_HASH = "X-STAR-REQUESTHASH";

		// Token: 0x040046A2 RID: 18082
		public const double CharacterUpgradeSameElementBonus = 1.1;

		// Token: 0x040046A3 RID: 18083
		public const double CharacterUpgradeBonus_Success = 1.0;

		// Token: 0x040046A4 RID: 18084
		public const double CharacterUpgradeBonus_GreatSuccess = 1.5;

		// Token: 0x040046A5 RID: 18085
		public const double CharacterUpgradeBonus_AmazingSuccess = 2.0;

		// Token: 0x040046A6 RID: 18086
		public const int CharacterUpgradeProbability_Success = 95;

		// Token: 0x040046A7 RID: 18087
		public const int CharacterUpgradeProbability_GreatSuccess = 4;

		// Token: 0x040046A8 RID: 18088
		public const int CharacterUpgradeProbability_AmazingSuccess = 1;

		// Token: 0x040046A9 RID: 18089
		public const double WeaponUpgradeBonus_Success = 1.0;

		// Token: 0x040046AA RID: 18090
		public const double WeaponUpgradeBonus_GreatSuccess = 1.5;

		// Token: 0x040046AB RID: 18091
		public const double WeaponUpgradeBonus_AmazingSuccess = 2.0;

		// Token: 0x040046AC RID: 18092
		public const int WeaponUpgradeProbability_Success = 80;

		// Token: 0x040046AD RID: 18093
		public const int WeaponUpgradeProbability_GreatSuccess = 15;

		// Token: 0x040046AE RID: 18094
		public const int WeaponUpgradeProbability_AmazingSuccess = 5;

		// Token: 0x040046AF RID: 18095
		public const int WeaponLimitExtendNum = 10;

		// Token: 0x040046B0 RID: 18096
		public const int WeaponLimitExtendAmount = 1;

		// Token: 0x040046B1 RID: 18097
		public const int WeaponLimitExtendMax = 5;

		// Token: 0x040046B2 RID: 18098
		public const int RoomObjectLimitExtendNum = 10;

		// Token: 0x040046B3 RID: 18099
		public const int RoomObjectLimitExtendAmount = 1;

		// Token: 0x040046B4 RID: 18100
		public const int RoomObjectLimitExtendMax = 5;

		// Token: 0x040046B5 RID: 18101
		public const int BattleContinueAmount = 10;

		// Token: 0x040046B6 RID: 18102
		public const long ReduceBuildTimeTownFacilityAmount = 10L;

		// Token: 0x040046B7 RID: 18103
		public const long ReduceBuildTimeTownFacilityPeriod = 30L;

		// Token: 0x040046B8 RID: 18104
		public const int StaminaAdditiveAmount = 10;
	}
}
