﻿using System;

// Token: 0x0200006E RID: 110
public enum eADVScreenAnchor
{
	// Token: 0x040001C6 RID: 454
	Middle,
	// Token: 0x040001C7 RID: 455
	Upper,
	// Token: 0x040001C8 RID: 456
	Lower
}
