﻿using System;
using System.Collections.Generic;
using GachaResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFC RID: 3068
public static class GachaResponse
{
	// Token: 0x06003F6D RID: 16237 RVA: 0x0013C5F8 File Offset: 0x0013A9F8
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F6E RID: 16238 RVA: 0x0013C610 File Offset: 0x0013AA10
	public static Draw Draw(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Draw>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F6F RID: 16239 RVA: 0x0013C628 File Offset: 0x0013AA28
	public static Stepdraw Stepdraw(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Stepdraw>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F70 RID: 16240 RVA: 0x0013C640 File Offset: 0x0013AA40
	public static GetBox GetBox(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetBox>(param, dialogType, acceptableResultCodes);
	}
}
