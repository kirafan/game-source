﻿using System;
using System.Collections.Generic;
using AppResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BF7 RID: 3063
public static class AppResponse
{
	// Token: 0x06003F56 RID: 16214 RVA: 0x0013C3D0 File Offset: 0x0013A7D0
	public static Health Health(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Health>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F57 RID: 16215 RVA: 0x0013C3E8 File Offset: 0x0013A7E8
	public static Versionget Versionget(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Versionget>(param, dialogType, acceptableResultCodes);
	}
}
