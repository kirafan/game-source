﻿using System;
using UnityEngine;

// Token: 0x02000CD7 RID: 3287
public class DifferentialAnimHandler : MonoBehaviour
{
	// Token: 0x0600412C RID: 16684 RVA: 0x0014077C File Offset: 0x0013EB7C
	private void Awake()
	{
		this.m_ThisTransform = base.transform;
		this.m_ThisAnimation = base.GetComponent<Animation>();
		this.m_ThisAnimation.enabled = false;
		this.m_AnimStat = null;
		if (this.m_ThisAnimation.clip != null)
		{
			this.UpdateAnim(this.m_ThisAnimation.clip.name, WrapMode.Loop);
		}
	}

	// Token: 0x0600412D RID: 16685 RVA: 0x001407E4 File Offset: 0x0013EBE4
	public void Sample(float normalizedTime)
	{
		if (this.m_AnimStat == null)
		{
			return;
		}
		if ((this.m_AnimStat.wrapMode == WrapMode.Loop && (int)normalizedTime > (int)this.m_AnimStat.normalizedTime) || (this.m_AnimStat.wrapMode == WrapMode.Once && normalizedTime < this.m_AnimStat.normalizedTime))
		{
			this.m_NewPosition = DifferentialAnimHandler.INITIAL_POS;
			this.m_NewEulerAngle = DifferentialAnimHandler.INITIAL_EULER;
		}
		this.m_OldEulerAngle = this.m_NewEulerAngle;
		this.m_OldPosition = this.m_NewPosition;
		this.m_AnimStat.normalizedTime = normalizedTime;
		this.m_ThisAnimation.enabled = true;
		this.m_ThisAnimation.Sample();
		this.m_ThisAnimation.enabled = false;
		this.m_NewEulerAngle = this.m_ThisTransform.localEulerAngles;
		this.m_NewPosition = this.m_ThisTransform.localPosition;
		this.m_DiffEulerAngle = this.m_NewEulerAngle - this.m_OldEulerAngle;
		this.m_DiffPosition = this.m_NewPosition - this.m_OldPosition;
	}

	// Token: 0x0600412E RID: 16686 RVA: 0x001408F6 File Offset: 0x0013ECF6
	public Animation GetAnimation()
	{
		return this.m_ThisAnimation;
	}

	// Token: 0x0600412F RID: 16687 RVA: 0x001408FE File Offset: 0x0013ECFE
	public bool IsAvailable()
	{
		return this.m_AnimStat != null;
	}

	// Token: 0x06004130 RID: 16688 RVA: 0x0014090C File Offset: 0x0013ED0C
	public Vector3 GetNewPosition()
	{
		return this.m_NewPosition;
	}

	// Token: 0x06004131 RID: 16689 RVA: 0x00140914 File Offset: 0x0013ED14
	public Vector3 GetNewEulerAngle()
	{
		return this.m_NewEulerAngle;
	}

	// Token: 0x06004132 RID: 16690 RVA: 0x0014091C File Offset: 0x0013ED1C
	public void Calculate(out Vector3 diffPos, out Vector3 diffEuler, Transform tr)
	{
		if (this.m_AnimStat == null)
		{
			diffPos = Vector3.zero;
			diffEuler = Vector3.zero;
			return;
		}
		diffEuler = this.m_DiffEulerAngle;
		diffPos = Matrix4x4.TRS(Vector3.zero, tr.rotation, Vector3.one).MultiplyPoint(this.m_DiffPosition);
	}

	// Token: 0x06004133 RID: 16691 RVA: 0x00140986 File Offset: 0x0013ED86
	public float GetAnimLength()
	{
		if (this.m_AnimStat == null)
		{
			return 0f;
		}
		return this.m_AnimStat.length;
	}

	// Token: 0x06004134 RID: 16692 RVA: 0x001409AC File Offset: 0x0013EDAC
	public void UpdateAnim(string postureAnimName, WrapMode wrapMode)
	{
		this.m_AnimStat = this.m_ThisAnimation[postureAnimName];
		if (this.m_AnimStat == null)
		{
			return;
		}
		this.m_ThisAnimation.Play(postureAnimName);
		this.m_AnimStat.wrapMode = wrapMode;
		this.m_NewPosition = DifferentialAnimHandler.INITIAL_POS;
		this.m_OldPosition = DifferentialAnimHandler.INITIAL_POS;
		this.m_DiffPosition = Vector3.zero;
		this.m_NewEulerAngle = DifferentialAnimHandler.INITIAL_EULER;
		this.m_OldEulerAngle = DifferentialAnimHandler.INITIAL_EULER;
		this.m_DiffEulerAngle = Vector3.zero;
	}

	// Token: 0x06004135 RID: 16693 RVA: 0x00140A38 File Offset: 0x0013EE38
	public void ResetFrame()
	{
		if (this.m_AnimStat == null)
		{
			return;
		}
		this.m_AnimStat.time = 0f;
		this.m_NewPosition = DifferentialAnimHandler.INITIAL_POS;
		this.m_OldPosition = DifferentialAnimHandler.INITIAL_POS;
		this.m_DiffPosition = Vector3.zero;
		this.m_NewEulerAngle = DifferentialAnimHandler.INITIAL_EULER;
		this.m_OldEulerAngle = DifferentialAnimHandler.INITIAL_EULER;
		this.m_DiffEulerAngle = Vector3.zero;
	}

	// Token: 0x0400497A RID: 18810
	private static Vector3 INITIAL_POS = Vector3.zero;

	// Token: 0x0400497B RID: 18811
	private static Vector3 INITIAL_EULER = new Vector3(270f, 0f, 0f);

	// Token: 0x0400497C RID: 18812
	private Transform m_ThisTransform;

	// Token: 0x0400497D RID: 18813
	private Animation m_ThisAnimation;

	// Token: 0x0400497E RID: 18814
	private AnimationState m_AnimStat;

	// Token: 0x0400497F RID: 18815
	private Vector3 m_OldPosition;

	// Token: 0x04004980 RID: 18816
	private Vector3 m_NewPosition;

	// Token: 0x04004981 RID: 18817
	private Vector3 m_DiffPosition;

	// Token: 0x04004982 RID: 18818
	private Vector3 m_OldEulerAngle;

	// Token: 0x04004983 RID: 18819
	private Vector3 m_NewEulerAngle;

	// Token: 0x04004984 RID: 18820
	private Vector3 m_DiffEulerAngle;
}
