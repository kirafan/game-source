﻿using System;
using System.Collections.Generic;
using InformationResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFD RID: 3069
public static class InformationResponse
{
	// Token: 0x06003F71 RID: 16241 RVA: 0x0013C658 File Offset: 0x0013AA58
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}
}
