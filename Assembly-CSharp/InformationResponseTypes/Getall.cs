﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace InformationResponseTypes
{
	// Token: 0x02000C2D RID: 3117
	public class Getall : CommonResponse
	{
		// Token: 0x06004013 RID: 16403 RVA: 0x0013DB6C File Offset: 0x0013BF6C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.informations == null) ? string.Empty : this.informations.ToString());
		}

		// Token: 0x040045F0 RID: 17904
		public Information[] informations;
	}
}
