﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using CommonResponseTypes;
using Meige;
using Newtonsoft.Json;
using Star;
using UnityEngine;
using WWWTypes;

// Token: 0x02000C06 RID: 3078
public static class ResponseCommon
{
	// Token: 0x06003FAA RID: 16298 RVA: 0x0013CBB0 File Offset: 0x0013AFB0
	public static T Func<T>(MeigewwwParam wwwParam, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.Retry, List<ResultCode> acceptableResultCodes = null) where T : CommonResponse
	{
		T t = (T)((object)null);
		if (!wwwParam.IsDone())
		{
			return t;
		}
		if (Star.SingletonMonoBehaviour<GameSystem>.Inst != null)
		{
			Star.SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
		}
		if (wwwParam.IsSystemError())
		{
			Debug.Log("www system error : " + wwwParam.GetSystemError());
			ResponseCommon.ShowDialog(wwwParam, null, ResponseCommon.DialogType.Retry);
			return (T)((object)null);
		}
		string responseJson = wwwParam.GetResponseJson();
		t = ResponseCommon.Deserialize<T>(responseJson);
		if (t == null)
		{
			APIUtility.ShowErrorWindowTitle(wwwParam, "通信エラーが発生しました。");
			return (T)((object)null);
		}
		if (Star.SingletonMonoBehaviour<GameSystem>.Inst != null)
		{
			Star.SingletonMonoBehaviour<GameSystem>.Inst.ServerTime = t.serverTime;
		}
		ResultCode result = t.GetResult();
		switch (result)
		{
		case ResultCode.INVALID_REQUEST_HASH:
		case ResultCode.INVALID_PARAMETERS:
		case ResultCode.INSUFFICIENT_PARAMETERS:
		case ResultCode.INVALID_JSON_SCHEMA:
		case ResultCode.DB_ERROR:
			break;
		default:
			switch (result + 1)
			{
			case ResultCode.SUCCESS:
			case ResultCode.TERMS_UPDATED:
				break;
			case ResultCode.OUTDATED:
				return t;
			case ResultCode.APPLYING:
			{
				string errorTitle = "アプリの更新";
				string errorMessage = result.ToMessageString();
				if (ResponseCommon.<>f__mg$cache1 == null)
				{
					ResponseCommon.<>f__mg$cache1 = new Action(APIUtility.OpenStore);
				}
				APIUtility.ShowErrorWindowOk_NoClose(errorTitle, errorMessage, ResponseCommon.<>f__mg$cache1);
				return (T)((object)null);
			}
			default:
				switch (result)
				{
				case ResultCode.PLAYER_SESSION_EXPIRED:
					APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
					return (T)((object)null);
				case ResultCode.PLAYER_SUSPENDED:
				case ResultCode.PLAYER_STOPPED:
					break;
				case ResultCode.PLAYER_DELETED:
					if (AccessSaveData.Inst != null)
					{
						AccessSaveData.Inst.Reset();
						AccessSaveData.Inst.Save();
					}
					APIUtility.ShowErrorWindowTitle(wwwParam, "アプリの更新", result.ToMessageString());
					return (T)((object)null);
				default:
					if (result == ResultCode.MAINTENANCE)
					{
						string errorTitle2 = "メンテナンス中";
						string errorMessage2 = (!string.IsNullOrEmpty(t.message)) ? t.message : result.ToMessageString();
						if (ResponseCommon.<>f__mg$cache0 == null)
						{
							ResponseCommon.<>f__mg$cache0 = new Action(ResponseCommon.ApplicationQuit);
						}
						APIUtility.ShowErrorWindowOk(errorTitle2, errorMessage2, ResponseCommon.<>f__mg$cache0);
						return (T)((object)null);
					}
					if (result != ResultCode.UNAVAILABLE)
					{
						if (result == ResultCode.OUTDATED_AB_VERSION)
						{
							APIUtility.ShowErrorWindowTitle(wwwParam, "データの更新", result.ToMessageString());
							return (T)((object)null);
						}
						if (result != ResultCode.UNKNOWN_ERROR)
						{
							if (dialogType == ResponseCommon.DialogType.None)
							{
								return t;
							}
							if (acceptableResultCodes != null && acceptableResultCodes.Contains(result))
							{
								return t;
							}
							ResponseCommon.ShowDialog(wwwParam, t.GetResult().ToMessageString(), dialogType);
							return (T)((object)null);
						}
					}
					break;
				}
				break;
			}
			break;
		}
		ResponseCommon.ShowDialog(wwwParam, result.ToMessageString(), ResponseCommon.DialogType.Title);
		return (T)((object)null);
	}

	// Token: 0x06003FAB RID: 16299 RVA: 0x0013CE3E File Offset: 0x0013B23E
	private static void ApplicationQuit()
	{
		Application.Quit();
	}

	// Token: 0x06003FAC RID: 16300 RVA: 0x0013CE48 File Offset: 0x0013B248
	public static void ShowDialog(MeigewwwParam wwwParam, string errMsg, ResponseCommon.DialogType dialogType)
	{
		switch (dialogType)
		{
		case ResponseCommon.DialogType.Ok:
			APIUtility.ShowErrorWindowOk(errMsg, null);
			break;
		case ResponseCommon.DialogType.Retry:
			APIUtility.ShowErrorWindowRetry(wwwParam, errMsg);
			break;
		case ResponseCommon.DialogType.Title:
			APIUtility.ShowErrorWindowTitle(wwwParam, errMsg);
			break;
		}
	}

	// Token: 0x06003FAD RID: 16301 RVA: 0x0013CE9C File Offset: 0x0013B29C
	public static T Deserialize<T>(string json) where T : CommonResponse
	{
		T result = (T)((object)null);
		try
		{
			result = JsonConvert.DeserializeObject<T>(json);
		}
		catch (Exception ex)
		{
			Debug.Log("Deserialize json : " + json);
			Debug.Log("Deserialize Exception : " + ex.ToString());
		}
		return result;
	}

	// Token: 0x040045C5 RID: 17861
	[CompilerGenerated]
	private static Action <>f__mg$cache0;

	// Token: 0x040045C6 RID: 17862
	[CompilerGenerated]
	private static Action <>f__mg$cache1;

	// Token: 0x02000C07 RID: 3079
	public enum DialogType
	{
		// Token: 0x040045C8 RID: 17864
		None,
		// Token: 0x040045C9 RID: 17865
		Ok,
		// Token: 0x040045CA RID: 17866
		Retry,
		// Token: 0x040045CB RID: 17867
		Title
	}
}
