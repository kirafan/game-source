﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace SupportcharacterResponseTypes
{
	// Token: 0x02000C7A RID: 3194
	public class GetAll : CommonResponse
	{
		// Token: 0x060040AD RID: 16557 RVA: 0x0013F8D4 File Offset: 0x0013DCD4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.supportCharacters == null) ? string.Empty : this.supportCharacters.ToString());
		}

		// Token: 0x04004684 RID: 18052
		public PlayerSupport[] supportCharacters;
	}
}
