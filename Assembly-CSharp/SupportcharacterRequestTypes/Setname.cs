﻿using System;

namespace SupportcharacterRequestTypes
{
	// Token: 0x02000BDA RID: 3034
	public class Setname
	{
		// Token: 0x04004599 RID: 17817
		public long managedSupportId;

		// Token: 0x0400459A RID: 17818
		public string name;
	}
}
