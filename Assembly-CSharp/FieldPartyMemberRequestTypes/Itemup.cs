﻿using System;

namespace FieldPartyMemberRequestTypes
{
	// Token: 0x02000B81 RID: 2945
	public class Itemup
	{
		// Token: 0x04004519 RID: 17689
		public long managedPartyMemberId;

		// Token: 0x0400451A RID: 17690
		public int touchItemResultNo;

		// Token: 0x0400451B RID: 17691
		public int amount;

		// Token: 0x0400451C RID: 17692
		public int flag;
	}
}
