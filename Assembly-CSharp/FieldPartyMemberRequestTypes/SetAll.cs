﻿using System;
using WWWTypes;

namespace FieldPartyMemberRequestTypes
{
	// Token: 0x02000B7C RID: 2940
	public class SetAll
	{
		// Token: 0x0400450E RID: 17678
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
