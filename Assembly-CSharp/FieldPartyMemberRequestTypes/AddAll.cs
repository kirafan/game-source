﻿using System;
using WWWTypes;

namespace FieldPartyMemberRequestTypes
{
	// Token: 0x02000B78 RID: 2936
	public class AddAll
	{
		// Token: 0x0400450A RID: 17674
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
