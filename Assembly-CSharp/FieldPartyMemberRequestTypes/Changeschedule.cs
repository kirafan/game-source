﻿using System;

namespace FieldPartyMemberRequestTypes
{
	// Token: 0x02000B7F RID: 2943
	public class Changeschedule
	{
		// Token: 0x04004511 RID: 17681
		public long managedPartyMemberId;

		// Token: 0x04004512 RID: 17682
		public int scheduleId;

		// Token: 0x04004513 RID: 17683
		public int scheduleTag;

		// Token: 0x04004514 RID: 17684
		public long managedFacilityId;

		// Token: 0x04004515 RID: 17685
		public int touchItemResultNo;

		// Token: 0x04004516 RID: 17686
		public int flag;

		// Token: 0x04004517 RID: 17687
		public string scheduleTable;
	}
}
