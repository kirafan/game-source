﻿using System;
using System.Collections.Generic;
using Meige;
using RoomObjectResponseTypes;
using WWWTypes;

// Token: 0x02000C08 RID: 3080
public static class RoomObjectResponse
{
	// Token: 0x06003FAE RID: 16302 RVA: 0x0013CEF8 File Offset: 0x0013B2F8
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FAF RID: 16303 RVA: 0x0013CF10 File Offset: 0x0013B310
	public static Buy Buy(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Buy>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB0 RID: 16304 RVA: 0x0013CF28 File Offset: 0x0013B328
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB1 RID: 16305 RVA: 0x0013CF40 File Offset: 0x0013B340
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Remove>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB2 RID: 16306 RVA: 0x0013CF58 File Offset: 0x0013B358
	public static Sale Sale(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Sale>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB3 RID: 16307 RVA: 0x0013CF70 File Offset: 0x0013B370
	public static GetList GetList(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetList>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB4 RID: 16308 RVA: 0x0013CF88 File Offset: 0x0013B388
	public static Limitadd Limitadd(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Limitadd>(param, dialogType, acceptableResultCodes);
	}
}
