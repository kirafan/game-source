﻿using System;

// Token: 0x02000F5C RID: 3932
[Serializable]
public class MabHandler
{
	// Token: 0x040050C6 RID: 20678
	public string m_Name;

	// Token: 0x040050C7 RID: 20679
	public int m_NumOfKeyframe;

	// Token: 0x040050C8 RID: 20680
	public int m_BaseFPS;

	// Token: 0x040050C9 RID: 20681
	public float m_AnimTimeBySec;

	// Token: 0x040050CA RID: 20682
	public MabAnimNodeHandler[] m_AnimNodeHandlerArray;

	// Token: 0x040050CB RID: 20683
	public MabHandler.MabAnimEvent[] m_AnimEvArray;

	// Token: 0x02000F5D RID: 3933
	[Serializable]
	public class MabAnimEvent
	{
		// Token: 0x040050CC RID: 20684
		public float m_Frame;

		// Token: 0x040050CD RID: 20685
		public int[] m_iParam = new int[4];
	}
}
