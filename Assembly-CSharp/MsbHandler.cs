﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

// Token: 0x02000FBD RID: 4029
public class MsbHandler : MonoBehaviour
{
	// Token: 0x060053EE RID: 21486 RVA: 0x00177F57 File Offset: 0x00176357
	public Transform GetTransform()
	{
		return this.m_Transform;
	}

	// Token: 0x060053EF RID: 21487 RVA: 0x00177F5F File Offset: 0x0017635F
	public Renderer[] GetRendererCache()
	{
		return this.m_rdArray;
	}

	// Token: 0x060053F0 RID: 21488 RVA: 0x00177F67 File Offset: 0x00176367
	public int GetMsbObjectHandlerNum()
	{
		return (this.m_MsbObjectHandlerArray != null) ? this.m_MsbObjectHandlerArray.Length : 0;
	}

	// Token: 0x060053F1 RID: 21489 RVA: 0x00177F82 File Offset: 0x00176382
	public int GetMsbMaterialHandlerNum()
	{
		return (this.m_MsbMaterialHandlerArray != null) ? this.m_MsbMaterialHandlerArray.Length : 0;
	}

	// Token: 0x060053F2 RID: 21490 RVA: 0x00177FA0 File Offset: 0x001763A0
	public int GetParticleEmitterNum()
	{
		if (this.m_MeigeParticleEmitterArray != null && this.m_MeigeParticleEmitterArray.Length > 0)
		{
			return this.m_MeigeParticleEmitterArray.Length;
		}
		if (this.m_MsbParticleEmitterArray != null && this.m_MsbParticleEmitterArray.Length > 0)
		{
			return this.m_MsbParticleEmitterArray.Length;
		}
		return 0;
	}

	// Token: 0x060053F3 RID: 21491 RVA: 0x00177FF2 File Offset: 0x001763F2
	public int GetMsbCameraHandlerNum()
	{
		return (this.m_MsbCameraHandlerArray != null) ? this.m_MsbCameraHandlerArray.Length : 0;
	}

	// Token: 0x060053F4 RID: 21492 RVA: 0x0017800D File Offset: 0x0017640D
	public MsbObjectHandler GetMsbObjectHandler(int i)
	{
		Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
		return this.m_MsbObjectHandlerArray[i];
	}

	// Token: 0x060053F5 RID: 21493 RVA: 0x00178038 File Offset: 0x00176438
	public MsbMaterialHandler GetMsbMaterialHandler(int i)
	{
		Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
		return this.m_MsbMaterialHandlerArray[i];
	}

	// Token: 0x060053F6 RID: 21494 RVA: 0x00178064 File Offset: 0x00176464
	public MeigeParticleEmitter GetParticleEmitter(int i)
	{
		Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
		if (this.m_MeigeParticleEmitterArray != null && this.m_MeigeParticleEmitterArray.Length > 0)
		{
			return this.m_MeigeParticleEmitterArray[i];
		}
		if (this.m_MsbParticleEmitterArray != null && this.m_MsbParticleEmitterArray.Length > 0)
		{
			return this.m_MsbParticleEmitterArray[i].m_ParticleEmitter;
		}
		return null;
	}

	// Token: 0x060053F7 RID: 21495 RVA: 0x001780DC File Offset: 0x001764DC
	public MsbCameraHandler GetMsbCameraHandler(int i)
	{
		Helper.Break(i >= 0, "Index(" + i + ") is invalidate.");
		return this.m_MsbCameraHandlerArray[i];
	}

	// Token: 0x060053F8 RID: 21496 RVA: 0x00178107 File Offset: 0x00176507
	public Animation GetAnimationTarget()
	{
		return this.m_AnimationTarget;
	}

	// Token: 0x060053F9 RID: 21497 RVA: 0x0017810F File Offset: 0x0017650F
	public MeigeAnimEventNotifier GetAnimEventNotifier()
	{
		return this.m_AnimEvNotifier;
	}

	// Token: 0x060053FA RID: 21498 RVA: 0x00178118 File Offset: 0x00176518
	public MsbObjectHandler GetMsbObjectHandlerByName(string name)
	{
		MsbObjectHandler result;
		if (!this.m_DicMsbObj.TryGetValue(name, out result))
		{
			return null;
		}
		return result;
	}

	// Token: 0x060053FB RID: 21499 RVA: 0x0017813C File Offset: 0x0017653C
	public MsbMaterialHandler GetMsbMaterialHandlerByName(string name)
	{
		MsbMaterialHandler result;
		if (!this.m_DicMsbMat.TryGetValue(name, out result))
		{
			return null;
		}
		return result;
	}

	// Token: 0x060053FC RID: 21500 RVA: 0x00178160 File Offset: 0x00176560
	public MsbHierarchyNode GetMsbHierarchyNodeByName(string name)
	{
		MsbHierarchyNode result;
		if (!this.m_DicMsbHie.TryGetValue(name, out result))
		{
			return null;
		}
		return result;
	}

	// Token: 0x060053FD RID: 21501 RVA: 0x00178183 File Offset: 0x00176583
	public void AttachCamera(int idx, Camera cam)
	{
		if (this.m_MsbCameraHandlerArray.Length > idx)
		{
			this.m_MsbCameraHandlerArray[idx].SetCamera(cam);
		}
	}

	// Token: 0x060053FE RID: 21502 RVA: 0x001781A4 File Offset: 0x001765A4
	public void DetachCamera()
	{
		for (int i = 0; i < this.m_MsbCameraHandlerArray.Length; i++)
		{
			this.DetachCamera(i);
		}
	}

	// Token: 0x060053FF RID: 21503 RVA: 0x001781D1 File Offset: 0x001765D1
	public void DetachCamera(int idx)
	{
		this.m_MsbCameraHandlerArray[idx].SetCamera(null);
	}

	// Token: 0x06005400 RID: 21504 RVA: 0x001781E4 File Offset: 0x001765E4
	public void SetDefaultVisibility()
	{
		foreach (MsbObjectHandler msbObjectHandler in this.m_MsbObjectHandlerArray)
		{
			msbObjectHandler.GetWork().m_bVisibility = msbObjectHandler.m_Src.m_bVisibility;
		}
	}

	// Token: 0x06005401 RID: 21505 RVA: 0x00178228 File Offset: 0x00176628
	private void OnDisable()
	{
		if (this.m_MsbCameraHandlerArray != null)
		{
			foreach (MsbCameraHandler msbCameraHandler in this.m_MsbCameraHandlerArray)
			{
				msbCameraHandler.UpdatePostProcessParameter(false);
			}
		}
	}

	// Token: 0x06005402 RID: 21506 RVA: 0x00178266 File Offset: 0x00176666
	private void Awake()
	{
		this.Init();
	}

	// Token: 0x06005403 RID: 21507 RVA: 0x00178270 File Offset: 0x00176670
	public void Init()
	{
		this.m_Transform = base.transform;
		this.m_DicMsbHie = new Dictionary<string, MsbHierarchyNode>();
		foreach (MsbHierarchyNode msbHierarchyNode in base.gameObject.GetComponentsInChildren<MsbHierarchyNode>())
		{
			this.m_DicMsbHie.Add(msbHierarchyNode.name, msbHierarchyNode);
			msbHierarchyNode.Init();
		}
		this.m_rdArray = base.gameObject.GetComponentsInChildren<Renderer>();
		this.m_DicMsbMat = new Dictionary<string, MsbMaterialHandler>();
		foreach (MsbMaterialHandler msbMaterialHandler in this.m_MsbMaterialHandlerArray)
		{
			msbMaterialHandler.Init(this);
			this.m_DicMsbMat.Add(msbMaterialHandler.m_Name, msbMaterialHandler);
		}
		this.m_DicMsbObj = new Dictionary<string, MsbObjectHandler>();
		foreach (MsbObjectHandler msbObjectHandler in this.m_MsbObjectHandlerArray)
		{
			msbObjectHandler.Init(this);
			this.m_DicMsbObj.Add(msbObjectHandler.m_Name, msbObjectHandler);
		}
		foreach (MsbCameraHandler msbCameraHandler in this.m_MsbCameraHandlerArray)
		{
			msbCameraHandler.Init(this);
		}
		if (this.m_MsbParticleEmitterArray != null)
		{
			foreach (MsbHandler.MsbParticleEmitter msbParticleEmitter in this.m_MsbParticleEmitterArray)
			{
				if (msbParticleEmitter.m_ParticleEmitter != null && msbParticleEmitter.m_ParentRenderer != null)
				{
					string value = msbParticleEmitter.m_ParentRenderer.sharedMaterial.name.Replace(" (Instance)", string.Empty);
					foreach (MsbMaterialHandler msbMaterialHandler2 in this.m_MsbMaterialHandlerArray)
					{
						if (msbMaterialHandler2.m_Name.Equals(value))
						{
							msbParticleEmitter.m_MsbMaterialHandler = msbMaterialHandler2;
							break;
						}
					}
					msbParticleEmitter.m_ParticleEmitter.m_Material = msbParticleEmitter.m_ParentRenderer.material;
				}
			}
		}
		if (this.m_MsbADHandlerArray != null)
		{
			foreach (MsbArticulatedDynamicsHandler msbArticulatedDynamicsHandler in this.m_MsbADHandlerArray)
			{
				msbArticulatedDynamicsHandler.Init(this);
			}
		}
	}

	// Token: 0x06005404 RID: 21508 RVA: 0x001784C0 File Offset: 0x001768C0
	private void OnDestroy()
	{
		foreach (MsbObjectHandler msbObjectHandler in this.m_MsbObjectHandlerArray)
		{
			msbObjectHandler.Destroy();
		}
	}

	// Token: 0x06005405 RID: 21509 RVA: 0x001784F4 File Offset: 0x001768F4
	private void LateUpdate()
	{
		foreach (MsbMaterialHandler msbMaterialHandler in this.m_MsbMaterialHandlerArray)
		{
			msbMaterialHandler.UpdateParam();
		}
		foreach (MsbObjectHandler msbObjectHandler in this.m_MsbObjectHandlerArray)
		{
			msbObjectHandler.UpdateParam();
		}
		foreach (MsbCameraHandler msbCameraHandler in this.m_MsbCameraHandlerArray)
		{
			msbCameraHandler.UpdateParam();
		}
		if (this.m_MsbParticleEmitterArray != null)
		{
			foreach (MsbHandler.MsbParticleEmitter msbParticleEmitter in this.m_MsbParticleEmitterArray)
			{
				if (msbParticleEmitter.m_ParticleEmitter != null && msbParticleEmitter.m_MsbMaterialHandler != null && msbParticleEmitter.m_MsbMaterialHandler.GetWork() != null)
				{
					MeigeShaderUtility.SetMeshColor(msbParticleEmitter.m_ParticleEmitter.m_Material, msbParticleEmitter.m_MsbMaterialHandler.GetWork().m_Diffuse);
				}
			}
		}
	}

	// Token: 0x06005406 RID: 21510 RVA: 0x00178608 File Offset: 0x00176A08
	private void FixedUpdate()
	{
		if (this.m_MsbADHandlerArray != null)
		{
			foreach (MsbArticulatedDynamicsHandler msbArticulatedDynamicsHandler in this.m_MsbADHandlerArray)
			{
				msbArticulatedDynamicsHandler.CalculateHierarchyMatrix();
				msbArticulatedDynamicsHandler.DoWork();
				msbArticulatedDynamicsHandler.Flush();
			}
		}
	}

	// Token: 0x040054D0 RID: 21712
	public MsbObjectHandler[] m_MsbObjectHandlerArray;

	// Token: 0x040054D1 RID: 21713
	public MsbMaterialHandler[] m_MsbMaterialHandlerArray;

	// Token: 0x040054D2 RID: 21714
	public MeigeParticleEmitter[] m_MeigeParticleEmitterArray;

	// Token: 0x040054D3 RID: 21715
	public MsbHandler.MsbParticleEmitter[] m_MsbParticleEmitterArray;

	// Token: 0x040054D4 RID: 21716
	public MsbCameraHandler[] m_MsbCameraHandlerArray;

	// Token: 0x040054D5 RID: 21717
	public MsbArticulatedDynamicsHandler[] m_MsbADHandlerArray;

	// Token: 0x040054D6 RID: 21718
	public Animation m_AnimationTarget;

	// Token: 0x040054D7 RID: 21719
	public MeigeAnimEventNotifier m_AnimEvNotifier;

	// Token: 0x040054D8 RID: 21720
	protected Renderer[] m_rdArray;

	// Token: 0x040054D9 RID: 21721
	private Dictionary<string, MsbObjectHandler> m_DicMsbObj;

	// Token: 0x040054DA RID: 21722
	private Dictionary<string, MsbMaterialHandler> m_DicMsbMat;

	// Token: 0x040054DB RID: 21723
	private Dictionary<string, MsbHierarchyNode> m_DicMsbHie;

	// Token: 0x040054DC RID: 21724
	private Transform m_Transform;

	// Token: 0x02000FBE RID: 4030
	[Serializable]
	public class MsbParticleEmitter
	{
		// Token: 0x040054DD RID: 21725
		public MeigeParticleEmitter m_ParticleEmitter;

		// Token: 0x040054DE RID: 21726
		public MsbMaterialHandler m_MsbMaterialHandler;

		// Token: 0x040054DF RID: 21727
		public Renderer m_ParentRenderer;
	}
}
