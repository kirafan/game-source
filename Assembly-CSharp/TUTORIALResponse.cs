﻿using System;
using System.Collections.Generic;
using Meige;
using TUTORIALResponseTypes;
using WWWTypes;

// Token: 0x02000C0D RID: 3085
public static class TUTORIALResponse
{
	// Token: 0x06003FC4 RID: 16324 RVA: 0x0013D108 File Offset: 0x0013B508
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC5 RID: 16325 RVA: 0x0013D120 File Offset: 0x0013B520
	public static SetParty SetParty(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<SetParty>(param, dialogType, acceptableResultCodes);
	}
}
