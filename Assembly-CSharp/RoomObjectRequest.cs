﻿using System;
using Meige;
using RoomObjectRequestTypes;

// Token: 0x02000B6A RID: 2922
public static class RoomObjectRequest
{
	// Token: 0x06003E79 RID: 15993 RVA: 0x0013A994 File Offset: 0x00138D94
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("roomObjectId", param.roomObjectId);
		meigewwwParam.Add("amount", param.amount);
		return meigewwwParam;
	}

	// Token: 0x06003E7A RID: 15994 RVA: 0x0013A9D8 File Offset: 0x00138DD8
	public static MeigewwwParam Add(string roomObjectId, string amount, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("roomObjectId", roomObjectId);
		meigewwwParam.Add("amount", amount);
		return meigewwwParam;
	}

	// Token: 0x06003E7B RID: 15995 RVA: 0x0013AA14 File Offset: 0x00138E14
	public static MeigewwwParam Buy(Buy param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/buy", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("roomObjectId", param.roomObjectId);
		meigewwwParam.Add("amount", param.amount);
		meigewwwParam.Add("tryBargain", param.tryBargain);
		return meigewwwParam;
	}

	// Token: 0x06003E7C RID: 15996 RVA: 0x0013AA70 File Offset: 0x00138E70
	public static MeigewwwParam Buy(string roomObjectId, string amount, int tryBargain, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/buy", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("roomObjectId", roomObjectId);
		meigewwwParam.Add("amount", amount);
		meigewwwParam.Add("tryBargain", tryBargain);
		return meigewwwParam;
	}

	// Token: 0x06003E7D RID: 15997 RVA: 0x0013AABC File Offset: 0x00138EBC
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E7E RID: 15998 RVA: 0x0013AAE0 File Offset: 0x00138EE0
	public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E7F RID: 15999 RVA: 0x0013AB04 File Offset: 0x00138F04
	public static MeigewwwParam Remove(Remove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomObjectId", param.managedRoomObjectId);
		return meigewwwParam;
	}

	// Token: 0x06003E80 RID: 16000 RVA: 0x0013AB3C File Offset: 0x00138F3C
	public static MeigewwwParam Remove(long managedRoomObjectId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomObjectId", managedRoomObjectId);
		return meigewwwParam;
	}

	// Token: 0x06003E81 RID: 16001 RVA: 0x0013AB70 File Offset: 0x00138F70
	public static MeigewwwParam Sale(Sale param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomObjectId", param.managedRoomObjectId);
		return meigewwwParam;
	}

	// Token: 0x06003E82 RID: 16002 RVA: 0x0013ABA4 File Offset: 0x00138FA4
	public static MeigewwwParam Sale(string managedRoomObjectId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedRoomObjectId", managedRoomObjectId);
		return meigewwwParam;
	}

	// Token: 0x06003E83 RID: 16003 RVA: 0x0013ABD4 File Offset: 0x00138FD4
	public static MeigewwwParam GetList(GetList param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("room_object/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E84 RID: 16004 RVA: 0x0013ABF8 File Offset: 0x00138FF8
	public static MeigewwwParam GetList(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("room_object/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E85 RID: 16005 RVA: 0x0013AC1C File Offset: 0x0013901C
	public static MeigewwwParam Limitadd(Limitadd param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", param.num);
		return meigewwwParam;
	}

	// Token: 0x06003E86 RID: 16006 RVA: 0x0013AC54 File Offset: 0x00139054
	public static MeigewwwParam Limitadd(int num, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/room_object/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", num);
		return meigewwwParam;
	}
}
