﻿using System;
using System.Collections.Generic;
using MasterOrbResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000C01 RID: 3073
public static class MasterOrbResponse
{
	// Token: 0x06003F7A RID: 16250 RVA: 0x0013C730 File Offset: 0x0013AB30
	public static Getall Getall(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getall>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003F7B RID: 16251 RVA: 0x0013C748 File Offset: 0x0013AB48
	public static Get Get(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Get>(param, dialogType, acceptableResultCodes);
	}
}
