﻿using System;
using System.Collections.Generic;
using Meige;
using SupportcharacterResponseTypes;
using WWWTypes;

// Token: 0x02000C0B RID: 3083
public static class SupportcharacterResponse
{
	// Token: 0x06003FC0 RID: 16320 RVA: 0x0013D0A8 File Offset: 0x0013B4A8
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC1 RID: 16321 RVA: 0x0013D0C0 File Offset: 0x0013B4C0
	public static Setname Setname(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Setname>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FC2 RID: 16322 RVA: 0x0013D0D8 File Offset: 0x0013B4D8
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}
}
