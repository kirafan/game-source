﻿using System;

namespace RoomObjectRequestTypes
{
	// Token: 0x02000BC7 RID: 3015
	public class Add
	{
		// Token: 0x04004580 RID: 17792
		public string roomObjectId;

		// Token: 0x04004581 RID: 17793
		public string amount;
	}
}
