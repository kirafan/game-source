﻿using System;

namespace RoomObjectRequestTypes
{
	// Token: 0x02000BC8 RID: 3016
	public class Buy
	{
		// Token: 0x04004582 RID: 17794
		public string roomObjectId;

		// Token: 0x04004583 RID: 17795
		public string amount;

		// Token: 0x04004584 RID: 17796
		public int tryBargain;
	}
}
