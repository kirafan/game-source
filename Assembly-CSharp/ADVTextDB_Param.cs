﻿using System;

// Token: 0x02000072 RID: 114
[Serializable]
public struct ADVTextDB_Param
{
	// Token: 0x040001DD RID: 477
	public uint m_id;

	// Token: 0x040001DE RID: 478
	public string m_charaName;

	// Token: 0x040001DF RID: 479
	public string m_text;

	// Token: 0x040001E0 RID: 480
	public string m_voiceLabel;
}
