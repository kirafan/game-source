﻿using System;
using System.Reflection;
using CommonResponseTypes;
using Meige;
using Newtonsoft.Json;
using WWWTypes;

// Token: 0x02000352 RID: 850
public class INetComHandle
{
	// Token: 0x06001030 RID: 4144 RVA: 0x00055A66 File Offset: 0x00053E66
	public void SetCallback(INetComHandle.ResponseCallbak pcallend)
	{
		this.m_CallBack = pcallend;
	}

	// Token: 0x06001031 RID: 4145 RVA: 0x00055A70 File Offset: 0x00053E70
	public void CreateComParam()
	{
		this.m_RertyNum = 3;
		this.WWWParam = new MeigewwwParam();
		if (this.Request)
		{
			this.WWWParam.Init(this.ApiName, MeigewwwParam.eRequestMethod.Post, new MeigewwwParam.Callback(this.CommonCallback));
		}
		else
		{
			this.WWWParam.Init(this.ApiName, MeigewwwParam.eRequestMethod.Get, new MeigewwwParam.Callback(this.CommonCallback));
		}
		Type type = base.GetType();
		FieldInfo[] fields = type.GetFields();
		for (int i = 0; i < fields.Length; i++)
		{
			this.WWWParam.Add(fields[i].Name, fields[i].GetValue(this));
		}
		NetworkQueueManager.Request(this.WWWParam);
	}

	// Token: 0x06001032 RID: 4146 RVA: 0x00055B24 File Offset: 0x00053F24
	public void CommonCallback(MeigewwwParam pparam)
	{
		this.m_RertyNum--;
		CommonResponse commonResponse;
		if (this.m_RertyNum > 0)
		{
			commonResponse = ResponseCommon.Func<CommonResponse>(pparam, this.m_RetryType, null);
		}
		else
		{
			commonResponse = ResponseCommon.Func<CommonResponse>(pparam, ResponseCommon.DialogType.Title, null);
		}
		if (commonResponse != null && commonResponse.GetResult() == ResultCode.SUCCESS)
		{
			string responseJson = pparam.GetResponseJson();
			this.ResponseObject = JsonConvert.DeserializeObject(responseJson, this.ResponseType);
			this.m_CallBack(this);
		}
	}

	// Token: 0x06001033 RID: 4147 RVA: 0x00055B9D File Offset: 0x00053F9D
	public bool IsComEnd()
	{
		return this.WWWParam.GetStatus() == MeigewwwParam.eStatus.Done;
	}

	// Token: 0x06001034 RID: 4148 RVA: 0x00055BAD File Offset: 0x00053FAD
	public object GetResponse()
	{
		return this.ResponseObject;
	}

	// Token: 0x06001035 RID: 4149 RVA: 0x00055BB5 File Offset: 0x00053FB5
	public void SetDummyResponse()
	{
		this.ResponseObject = (Activator.CreateInstance(this.ResponseType) as CommonResponse);
	}

	// Token: 0x06001036 RID: 4150 RVA: 0x00055BCD File Offset: 0x00053FCD
	public void DummyToCallBack()
	{
		this.m_CallBack(this);
	}

	// Token: 0x0400173C RID: 5948
	public const int RETRYE_COUNT = 3;

	// Token: 0x0400173D RID: 5949
	protected string ApiName;

	// Token: 0x0400173E RID: 5950
	protected bool Request;

	// Token: 0x0400173F RID: 5951
	protected MeigewwwParam WWWParam;

	// Token: 0x04001740 RID: 5952
	protected INetComHandle.ResponseCallbak m_CallBack;

	// Token: 0x04001741 RID: 5953
	protected Type ResponseType;

	// Token: 0x04001742 RID: 5954
	protected object ResponseObject;

	// Token: 0x04001743 RID: 5955
	protected int m_RertyNum;

	// Token: 0x04001744 RID: 5956
	protected ResponseCommon.DialogType m_RetryType = ResponseCommon.DialogType.Retry;

	// Token: 0x02000353 RID: 851
	// (Invoke) Token: 0x06001038 RID: 4152
	public delegate void ResponseCallbak(INetComHandle presponse);
}
