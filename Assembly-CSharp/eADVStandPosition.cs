﻿using System;

// Token: 0x02000070 RID: 112
public enum eADVStandPosition
{
	// Token: 0x040001CE RID: 462
	Left,
	// Token: 0x040001CF RID: 463
	CenterLeft,
	// Token: 0x040001D0 RID: 464
	Center,
	// Token: 0x040001D1 RID: 465
	CenterRight,
	// Token: 0x040001D2 RID: 466
	Right,
	// Token: 0x040001D3 RID: 467
	LeftLeft,
	// Token: 0x040001D4 RID: 468
	CenterLeftLeft,
	// Token: 0x040001D5 RID: 469
	CenterHalfLeft,
	// Token: 0x040001D6 RID: 470
	CenterHalfRight,
	// Token: 0x040001D7 RID: 471
	CenterRightRight,
	// Token: 0x040001D8 RID: 472
	RightRight,
	// Token: 0x040001D9 RID: 473
	Num
}
