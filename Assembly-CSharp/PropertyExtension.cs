﻿using System;
using System.Reflection;

// Token: 0x02000E93 RID: 3731
public static class PropertyExtension
{
	// Token: 0x06004D51 RID: 19793 RVA: 0x00158F10 File Offset: 0x00157310
	public static IPropertyAccessor ToAccessor(this PropertyInfo pi)
	{
		Type type = typeof(Func<, >).MakeGenericType(new Type[]
		{
			pi.DeclaringType,
			pi.PropertyType
		});
		Delegate @delegate = Delegate.CreateDelegate(type, pi.GetGetMethod());
		Type type2 = typeof(Action<, >).MakeGenericType(new Type[]
		{
			pi.DeclaringType,
			pi.PropertyType
		});
		Delegate delegate2 = Delegate.CreateDelegate(type2, pi.GetSetMethod());
		Type type3 = typeof(PropertyAccessor<, >).MakeGenericType(new Type[]
		{
			pi.DeclaringType,
			pi.PropertyType
		});
		return (IPropertyAccessor)Activator.CreateInstance(type3, new object[]
		{
			@delegate,
			delegate2
		});
	}
}
