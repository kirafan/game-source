﻿using System;

namespace ItemTradeRequestTypes
{
	// Token: 0x02000B92 RID: 2962
	public class Trade
	{
		// Token: 0x0400452E RID: 17710
		public int recipeId;

		// Token: 0x0400452F RID: 17711
		public int indexNo;
	}
}
