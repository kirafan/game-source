﻿using System;

namespace TownRequestTypes
{
	// Token: 0x02000BED RID: 3053
	public class Set
	{
		// Token: 0x040045BF RID: 17855
		public long managedTownId;

		// Token: 0x040045C0 RID: 17856
		public string gridData;
	}
}
