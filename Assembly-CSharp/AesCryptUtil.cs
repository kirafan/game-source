﻿using System;
using System.IO;
using System.Security.Cryptography;

// Token: 0x02000E71 RID: 3697
public class AesCryptUtil
{
	// Token: 0x17000501 RID: 1281
	// (get) Token: 0x06004C7C RID: 19580 RVA: 0x00154AEB File Offset: 0x00152EEB
	private static byte[] keyStr
	{
		get
		{
			return ProjDepend.CACK;
		}
	}

	// Token: 0x17000502 RID: 1282
	// (get) Token: 0x06004C7D RID: 19581 RVA: 0x00154AF2 File Offset: 0x00152EF2
	private static byte[] ivStr
	{
		get
		{
			return ProjDepend.CACK;
		}
	}

	// Token: 0x06004C7E RID: 19582 RVA: 0x00154AF9 File Offset: 0x00152EF9
	public static void setCipherMode(CipherMode mode)
	{
		AesCryptUtil.cipherMode = mode;
	}

	// Token: 0x06004C7F RID: 19583 RVA: 0x00154B04 File Offset: 0x00152F04
	public static string encrypt(string message)
	{
		if (AesCryptUtil.keyStr.Length <= 0)
		{
			return string.Empty;
		}
		if (AesCryptUtil.ivStr.Length <= 0)
		{
			return string.Empty;
		}
		string result = null;
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = AesCryptUtil.blockSize;
		rijndaelManaged.Key = AesCryptUtil.keyStr;
		rijndaelManaged.IV = AesCryptUtil.ivStr;
		rijndaelManaged.Mode = AesCryptUtil.cipherMode;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		try
		{
			MemoryStream memoryStream = new MemoryStream();
			using (CryptoStream cryptoStream = new CryptoStream(memoryStream, rijndaelManaged.CreateEncryptor(AesCryptUtil.keyStr, AesCryptUtil.ivStr), CryptoStreamMode.Write))
			{
				using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
				{
					streamWriter.Write(message);
					streamWriter.Close();
				}
				cryptoStream.Close();
			}
			byte[] inArray = memoryStream.ToArray();
			result = Convert.ToBase64String(inArray);
			memoryStream.Close();
		}
		catch (Exception ex)
		{
			Console.WriteLine("An error occurred: {0}", ex.Message);
		}
		finally
		{
			rijndaelManaged.Clear();
		}
		return result;
	}

	// Token: 0x06004C80 RID: 19584 RVA: 0x00154C44 File Offset: 0x00153044
	public static byte[] encrypt(byte[] message)
	{
		if (AesCryptUtil.keyStr.Length <= 0)
		{
			return new byte[0];
		}
		if (AesCryptUtil.ivStr.Length <= 0)
		{
			return new byte[0];
		}
		byte[] result = new byte[0];
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = AesCryptUtil.blockSize;
		rijndaelManaged.Key = AesCryptUtil.keyStr;
		rijndaelManaged.IV = AesCryptUtil.ivStr;
		rijndaelManaged.Mode = AesCryptUtil.cipherMode;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		try
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(AesCryptUtil.keyStr, AesCryptUtil.ivStr))
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
					{
						cryptoStream.Write(message, 0, message.Length);
						cryptoStream.FlushFinalBlock();
						result = memoryStream.ToArray();
					}
				}
			}
		}
		catch (Exception ex)
		{
			Console.WriteLine("An error occurred: {0}", ex.Message);
		}
		finally
		{
			rijndaelManaged.Clear();
		}
		return result;
	}

	// Token: 0x06004C81 RID: 19585 RVA: 0x00154D94 File Offset: 0x00153194
	public static string decrypt(string encstr)
	{
		if (AesCryptUtil.keyStr.Length <= 0)
		{
			return string.Empty;
		}
		if (AesCryptUtil.ivStr.Length <= 0)
		{
			return string.Empty;
		}
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = AesCryptUtil.blockSize;
		rijndaelManaged.Key = AesCryptUtil.keyStr;
		rijndaelManaged.IV = AesCryptUtil.ivStr;
		rijndaelManaged.Mode = AesCryptUtil.cipherMode;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		string text = string.Empty;
		try
		{
			byte[] buffer = Convert.FromBase64String(encstr);
			using (MemoryStream memoryStream = new MemoryStream(buffer))
			{
				using (CryptoStream cryptoStream = new CryptoStream(memoryStream, rijndaelManaged.CreateDecryptor(AesCryptUtil.keyStr, AesCryptUtil.ivStr), CryptoStreamMode.Read))
				{
					using (StreamReader streamReader = new StreamReader(cryptoStream))
					{
						text = streamReader.ReadToEnd();
					}
				}
			}
		}
		catch (Exception ex)
		{
			Console.WriteLine("An error occurred: {0}", ex.Message);
		}
		finally
		{
			rijndaelManaged.Clear();
		}
		text = text.TrimEnd(new char[1]);
		return text;
	}

	// Token: 0x06004C82 RID: 19586 RVA: 0x00154EEC File Offset: 0x001532EC
	public static byte[] decrypt(byte[] encstr)
	{
		if (AesCryptUtil.keyStr.Length <= 0)
		{
			return new byte[0];
		}
		if (AesCryptUtil.ivStr.Length <= 0)
		{
			return new byte[0];
		}
		RijndaelManaged rijndaelManaged = new RijndaelManaged();
		rijndaelManaged.BlockSize = AesCryptUtil.blockSize;
		rijndaelManaged.Key = AesCryptUtil.keyStr;
		rijndaelManaged.IV = AesCryptUtil.ivStr;
		rijndaelManaged.Mode = AesCryptUtil.cipherMode;
		rijndaelManaged.Padding = PaddingMode.Zeros;
		byte[] array = new byte[encstr.Length];
		try
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(AesCryptUtil.keyStr, AesCryptUtil.ivStr))
				{
					using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
					{
						cryptoStream.Write(encstr, 0, encstr.Length);
						cryptoStream.FlushFinalBlock();
						array = memoryStream.ToArray();
					}
				}
			}
		}
		catch (Exception ex)
		{
			Console.WriteLine("An error occurred: {0}", ex.Message);
		}
		finally
		{
			rijndaelManaged.Clear();
		}
		array = AesCryptUtil.bytePadding(array);
		return array;
	}

	// Token: 0x06004C83 RID: 19587 RVA: 0x00155044 File Offset: 0x00153444
	private static byte[] bytePadding(byte[] src)
	{
		if (src.Length == 0)
		{
			return src;
		}
		int num = src.Length;
		for (int i = src.Length - 1; i >= 0; i--)
		{
			if (src[i] != 0)
			{
				break;
			}
			num = i;
		}
		if (num <= 0)
		{
			return src;
		}
		byte[] array = new byte[num];
		Array.Copy(src, array, array.Length);
		return array;
	}

	// Token: 0x04004D73 RID: 19827
	private static readonly int blockSize = ProjDepend.CRYPT_AES_CBC_LENGTH;

	// Token: 0x04004D74 RID: 19828
	private static CipherMode cipherMode = CipherMode.CBC;
}
