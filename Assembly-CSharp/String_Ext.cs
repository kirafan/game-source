﻿using System;

// Token: 0x020002FA RID: 762
public static class String_Ext
{
	// Token: 0x06000EDC RID: 3804 RVA: 0x0004FDF0 File Offset: 0x0004E1F0
	public static bool RightConvertToInt(this string self, string value, out int result)
	{
		int num = self.LastIndexOf(value);
		if (num >= 0)
		{
			num++;
			string text = self.Substring(num, self.Length - num);
			if (text != null && text != string.Empty && int.TryParse(text, out result))
			{
				return true;
			}
		}
		result = 0;
		return false;
	}

	// Token: 0x06000EDD RID: 3805 RVA: 0x0004FE4A File Offset: 0x0004E24A
	public static string Coloring(this string str, string color)
	{
		return string.Format("<color={0}>{1}</color>", color, str);
	}

	// Token: 0x06000EDE RID: 3806 RVA: 0x0004FE58 File Offset: 0x0004E258
	public static string White(this string str)
	{
		return str.Coloring("white");
	}

	// Token: 0x06000EDF RID: 3807 RVA: 0x0004FE65 File Offset: 0x0004E265
	public static string Red(this string str)
	{
		return str.Coloring("red");
	}

	// Token: 0x06000EE0 RID: 3808 RVA: 0x0004FE72 File Offset: 0x0004E272
	public static string Green(this string str)
	{
		return str.Coloring("green");
	}

	// Token: 0x06000EE1 RID: 3809 RVA: 0x0004FE7F File Offset: 0x0004E27F
	public static string Blue(this string str)
	{
		return str.Coloring("blue");
	}

	// Token: 0x06000EE2 RID: 3810 RVA: 0x0004FE8C File Offset: 0x0004E28C
	public static string Yellow(this string str)
	{
		return str.Coloring("yellow");
	}

	// Token: 0x06000EE3 RID: 3811 RVA: 0x0004FE99 File Offset: 0x0004E299
	public static string Cyan(this string str)
	{
		return str.Coloring("cyan");
	}

	// Token: 0x06000EE4 RID: 3812 RVA: 0x0004FEA6 File Offset: 0x0004E2A6
	public static string Magenta(this string str)
	{
		return str.Coloring("magenta");
	}

	// Token: 0x06000EE5 RID: 3813 RVA: 0x0004FEB3 File Offset: 0x0004E2B3
	public static string Orange(this string str)
	{
		return str.Coloring("orange");
	}

	// Token: 0x06000EE6 RID: 3814 RVA: 0x0004FEC0 File Offset: 0x0004E2C0
	public static string APIColor(this string str)
	{
		return str.Coloring("orange");
	}

	// Token: 0x06000EE7 RID: 3815 RVA: 0x0004FECD File Offset: 0x0004E2CD
	public static string Resize(this string str, int size)
	{
		return string.Format("<size={0}>{1}</size>", size, str);
	}

	// Token: 0x06000EE8 RID: 3816 RVA: 0x0004FEE0 File Offset: 0x0004E2E0
	public static string Medium(this string str)
	{
		return str.Resize(11);
	}

	// Token: 0x06000EE9 RID: 3817 RVA: 0x0004FEEA File Offset: 0x0004E2EA
	public static string Small(this string str)
	{
		return str.Resize(9);
	}

	// Token: 0x06000EEA RID: 3818 RVA: 0x0004FEF4 File Offset: 0x0004E2F4
	public static string Large(this string str)
	{
		return str.Resize(16);
	}

	// Token: 0x06000EEB RID: 3819 RVA: 0x0004FEFE File Offset: 0x0004E2FE
	public static string Bold(this string str)
	{
		return string.Format("<b>{0}</b>", str);
	}

	// Token: 0x06000EEC RID: 3820 RVA: 0x0004FF0B File Offset: 0x0004E30B
	public static string Italic(this string str)
	{
		return string.Format("<i>{0}</i>", str);
	}
}
