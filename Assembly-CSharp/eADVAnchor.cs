﻿using System;

// Token: 0x0200006D RID: 109
public enum eADVAnchor
{
	// Token: 0x040001BC RID: 444
	UpperLeft,
	// Token: 0x040001BD RID: 445
	UpperCenter,
	// Token: 0x040001BE RID: 446
	UpperRight,
	// Token: 0x040001BF RID: 447
	MiddleLeft,
	// Token: 0x040001C0 RID: 448
	MiddleCenter,
	// Token: 0x040001C1 RID: 449
	MiddleRight,
	// Token: 0x040001C2 RID: 450
	LowerLeft,
	// Token: 0x040001C3 RID: 451
	LowerCenter,
	// Token: 0x040001C4 RID: 452
	LowerRight
}
