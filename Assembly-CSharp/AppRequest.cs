﻿using System;
using AppRequestTypes;
using Meige;

// Token: 0x02000B5B RID: 2907
public static class AppRequest
{
	// Token: 0x06003DD1 RID: 15825 RVA: 0x001381D8 File Offset: 0x001365D8
	public static MeigewwwParam Health(Health param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("app/health", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003DD2 RID: 15826 RVA: 0x001381FC File Offset: 0x001365FC
	public static MeigewwwParam Health(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("app/health", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003DD3 RID: 15827 RVA: 0x00138220 File Offset: 0x00136620
	public static MeigewwwParam Versionget(Versionget param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("app/version/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", param.platform);
		meigewwwParam.Add("version", param.version);
		return meigewwwParam;
	}

	// Token: 0x06003DD4 RID: 15828 RVA: 0x00138268 File Offset: 0x00136668
	public static MeigewwwParam Versionget(int platform, string version, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("app/version/get", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("platform", platform);
		meigewwwParam.Add("version", version);
		return meigewwwParam;
	}
}
