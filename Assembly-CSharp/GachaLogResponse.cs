﻿using System;
using System.Collections.Generic;
using GachaLogResponseTypes;
using Meige;
using WWWTypes;

// Token: 0x02000BFB RID: 3067
public static class GachaLogResponse
{
	// Token: 0x06003F6C RID: 16236 RVA: 0x0013C5E0 File Offset: 0x0013A9E0
	public static GetLatest GetLatest(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetLatest>(param, dialogType, acceptableResultCodes);
	}
}
