﻿using System;
using ItemTradeRequestTypes;
using Meige;

// Token: 0x02000B63 RID: 2915
public static class ItemTradeRequest
{
	// Token: 0x06003E0D RID: 15885 RVA: 0x00138FAC File Offset: 0x001373AC
	public static MeigewwwParam Getrecipe(Getrecipe param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/trade/recipe/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E0E RID: 15886 RVA: 0x00138FD0 File Offset: 0x001373D0
	public static MeigewwwParam Getrecipe(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/trade/recipe/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003E0F RID: 15887 RVA: 0x00138FF4 File Offset: 0x001373F4
	public static MeigewwwParam Trade(Trade param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/trade/trade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("recipeId", param.recipeId);
		meigewwwParam.Add("indexNo", param.indexNo);
		return meigewwwParam;
	}

	// Token: 0x06003E10 RID: 15888 RVA: 0x00139044 File Offset: 0x00137444
	public static MeigewwwParam Trade(int recipeId, int indexNo, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/item/trade/trade", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("recipeId", recipeId);
		meigewwwParam.Add("indexNo", indexNo);
		return meigewwwParam;
	}
}
