﻿using System;

// Token: 0x0200006F RID: 111
public enum eADVCharaAnchor
{
	// Token: 0x040001CA RID: 458
	Center,
	// Token: 0x040001CB RID: 459
	Face,
	// Token: 0x040001CC RID: 460
	Bottom
}
