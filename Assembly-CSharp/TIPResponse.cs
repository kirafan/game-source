﻿using System;
using System.Collections.Generic;
using Meige;
using TIPResponseTypes;
using WWWTypes;

// Token: 0x02000C0C RID: 3084
public static class TIPResponse
{
	// Token: 0x06003FC3 RID: 16323 RVA: 0x0013D0F0 File Offset: 0x0013B4F0
	public static Add Add(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Add>(param, dialogType, acceptableResultCodes);
	}
}
