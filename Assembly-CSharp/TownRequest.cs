﻿using System;
using Meige;
using TownRequestTypes;

// Token: 0x02000B71 RID: 2929
public static class TownRequest
{
	// Token: 0x06003EC5 RID: 16069 RVA: 0x0013BCF0 File Offset: 0x0013A0F0
	public static MeigewwwParam Set(Set param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownId", param.managedTownId);
		meigewwwParam.Add("gridData", param.gridData);
		return meigewwwParam;
	}

	// Token: 0x06003EC6 RID: 16070 RVA: 0x0013BD38 File Offset: 0x0013A138
	public static MeigewwwParam Set(long managedTownId, string gridData, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownId", managedTownId);
		meigewwwParam.Add("gridData", gridData);
		return meigewwwParam;
	}

	// Token: 0x06003EC7 RID: 16071 RVA: 0x0013BD78 File Offset: 0x0013A178
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", param.playerId);
		return meigewwwParam;
	}

	// Token: 0x06003EC8 RID: 16072 RVA: 0x0013BDB0 File Offset: 0x0013A1B0
	public static MeigewwwParam GetAll(long playerId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		meigewwwParam.Add("playerId", playerId);
		return meigewwwParam;
	}

	// Token: 0x06003EC9 RID: 16073 RVA: 0x0013BDE4 File Offset: 0x0013A1E4
	public static MeigewwwParam Remove(Remove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownId", param.managedTownId);
		return meigewwwParam;
	}

	// Token: 0x06003ECA RID: 16074 RVA: 0x0013BE1C File Offset: 0x0013A21C
	public static MeigewwwParam Remove(long managedTownId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownId", managedTownId);
		return meigewwwParam;
	}
}
