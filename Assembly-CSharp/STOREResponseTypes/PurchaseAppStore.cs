﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C73 RID: 3187
	public class PurchaseAppStore : CommonResponse
	{
		// Token: 0x0600409F RID: 16543 RVA: 0x0013F640 File Offset: 0x0013DA40
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.purchased == null) ? string.Empty : this.purchased.ToString());
			return str + ((this.products == null) ? string.Empty : this.products.ToString());
		}

		// Token: 0x04004677 RID: 18039
		public Player player;

		// Token: 0x04004678 RID: 18040
		public StoreProduct[] purchased;

		// Token: 0x04004679 RID: 18041
		public StoreProduct[] products;
	}
}
