﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C72 RID: 3186
	public class GetAll : CommonResponse
	{
		// Token: 0x0600409D RID: 16541 RVA: 0x0013F5CC File Offset: 0x0013D9CC
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.products == null) ? string.Empty : this.products.ToString());
			str += this.monthlyAmount.ToString();
			return str + this.availableBalance.ToString();
		}

		// Token: 0x04004674 RID: 18036
		public StoreProduct[] products;

		// Token: 0x04004675 RID: 18037
		public long monthlyAmount;

		// Token: 0x04004676 RID: 18038
		public long availableBalance;
	}
}
