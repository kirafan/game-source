﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C74 RID: 3188
	public class PurchaseGooglePlay : CommonResponse
	{
		// Token: 0x060040A1 RID: 16545 RVA: 0x0013F6D4 File Offset: 0x0013DAD4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.purchased == null) ? string.Empty : this.purchased.ToString());
			return str + ((this.products == null) ? string.Empty : this.products.ToString());
		}

		// Token: 0x0400467A RID: 18042
		public Player player;

		// Token: 0x0400467B RID: 18043
		public StoreProduct[] purchased;

		// Token: 0x0400467C RID: 18044
		public StoreProduct[] products;
	}
}
