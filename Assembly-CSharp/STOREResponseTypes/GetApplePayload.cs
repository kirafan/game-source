﻿using System;
using CommonResponseTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C76 RID: 3190
	public class GetApplePayload : CommonResponse
	{
		// Token: 0x060040A5 RID: 16549 RVA: 0x0013F7DC File Offset: 0x0013DBDC
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.payload == null) ? string.Empty : this.payload.ToString());
			str += this.monthlyAmount.ToString();
			return str + this.availableBalance.ToString();
		}

		// Token: 0x04004680 RID: 18048
		public string payload;

		// Token: 0x04004681 RID: 18049
		public long monthlyAmount;

		// Token: 0x04004682 RID: 18050
		public long availableBalance;
	}
}
