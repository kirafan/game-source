﻿using System;
using CommonResponseTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C75 RID: 3189
	public class GetGooglePayload : CommonResponse
	{
		// Token: 0x060040A3 RID: 16547 RVA: 0x0013F768 File Offset: 0x0013DB68
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.payload == null) ? string.Empty : this.payload.ToString());
			str += this.monthlyAmount.ToString();
			return str + this.availableBalance.ToString();
		}

		// Token: 0x0400467D RID: 18045
		public string payload;

		// Token: 0x0400467E RID: 18046
		public long monthlyAmount;

		// Token: 0x0400467F RID: 18047
		public long availableBalance;
	}
}
