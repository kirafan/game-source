﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace STOREResponseTypes
{
	// Token: 0x02000C77 RID: 3191
	public class GetPurchaseLog : CommonResponse
	{
		// Token: 0x060040A7 RID: 16551 RVA: 0x0013F850 File Offset: 0x0013DC50
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.purchases == null) ? string.Empty : this.purchases.ToString());
		}

		// Token: 0x04004683 RID: 18051
		public PlayerPurchase[] purchases;
	}
}
