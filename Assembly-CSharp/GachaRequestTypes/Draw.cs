﻿using System;

namespace GachaRequestTypes
{
	// Token: 0x02000B8B RID: 2955
	public class Draw
	{
		// Token: 0x04004525 RID: 17701
		public int gachaId;

		// Token: 0x04004526 RID: 17702
		public int drawType;

		// Token: 0x04004527 RID: 17703
		public int stepCode;

		// Token: 0x04004528 RID: 17704
		public bool reDraw;
	}
}
