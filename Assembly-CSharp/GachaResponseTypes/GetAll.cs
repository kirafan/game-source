﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes
{
	// Token: 0x02000C29 RID: 3113
	public class GetAll : CommonResponse
	{
		// Token: 0x0600400B RID: 16395 RVA: 0x0013D8D4 File Offset: 0x0013BCD4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.gachas == null) ? string.Empty : this.gachas.ToString());
			return str + ((this.gachaSteps == null) ? string.Empty : this.gachaSteps.ToString());
		}

		// Token: 0x040045E2 RID: 17890
		public GachaList[] gachas;

		// Token: 0x040045E3 RID: 17891
		public GachaStepList[] gachaSteps;
	}
}
