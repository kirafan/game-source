﻿using System;
using CommonResponseTypes;

namespace GachaResponseTypes
{
	// Token: 0x02000C2C RID: 3116
	public class GetBox : CommonResponse
	{
		// Token: 0x06004011 RID: 16401 RVA: 0x0013DB00 File Offset: 0x0013BF00
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.characters == null) ? string.Empty : this.characters.ToString());
			return str + ((this.pickups == null) ? string.Empty : this.pickups.ToString());
		}

		// Token: 0x040045EE RID: 17902
		public int[] characters;

		// Token: 0x040045EF RID: 17903
		public int[] pickups;
	}
}
