﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes
{
	// Token: 0x02000C2B RID: 3115
	public class Stepdraw : CommonResponse
	{
		// Token: 0x0600400F RID: 16399 RVA: 0x0013DA20 File Offset: 0x0013BE20
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.gachaResults == null) ? string.Empty : this.gachaResults.ToString());
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			return str + ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
		}

		// Token: 0x040045E9 RID: 17897
		public GachaResult[] gachaResults;

		// Token: 0x040045EA RID: 17898
		public Player player;

		// Token: 0x040045EB RID: 17899
		public PlayerItemSummary[] itemSummary;

		// Token: 0x040045EC RID: 17900
		public PlayerCharacter[] managedCharacters;

		// Token: 0x040045ED RID: 17901
		public PlayerNamedType[] managedNamedTypes;
	}
}
