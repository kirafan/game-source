﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace GachaResponseTypes
{
	// Token: 0x02000C2A RID: 3114
	public class Draw : CommonResponse
	{
		// Token: 0x0600400D RID: 16397 RVA: 0x0013D940 File Offset: 0x0013BD40
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.gachaResults == null) ? string.Empty : this.gachaResults.ToString());
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			return str + ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
		}

		// Token: 0x040045E4 RID: 17892
		public GachaResult[] gachaResults;

		// Token: 0x040045E5 RID: 17893
		public Player player;

		// Token: 0x040045E6 RID: 17894
		public PlayerItemSummary[] itemSummary;

		// Token: 0x040045E7 RID: 17895
		public PlayerCharacter[] managedCharacters;

		// Token: 0x040045E8 RID: 17896
		public PlayerNamedType[] managedNamedTypes;
	}
}
