﻿using System;
using Meige;
using TIPRequestTypes;

// Token: 0x02000B6E RID: 2926
public static class TIPRequest
{
	// Token: 0x06003EA3 RID: 16035 RVA: 0x0013B304 File Offset: 0x00139704
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/tip/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("tipId", param.tipId);
		return meigewwwParam;
	}

	// Token: 0x06003EA4 RID: 16036 RVA: 0x0013B338 File Offset: 0x00139738
	public static MeigewwwParam Add(string tipId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/tutorial/tip/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("tipId", tipId);
		return meigewwwParam;
	}
}
