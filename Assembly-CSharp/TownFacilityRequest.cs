﻿using System;
using Meige;
using TownFacilityRequestTypes;
using WWWTypes;

// Token: 0x02000B70 RID: 2928
public static class TownFacilityRequest
{
	// Token: 0x06003EA9 RID: 16041 RVA: 0x0013B440 File Offset: 0x00139840
	public static MeigewwwParam Add(Add param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("facilityId", param.facilityId);
		meigewwwParam.Add("amount", param.amount);
		return meigewwwParam;
	}

	// Token: 0x06003EAA RID: 16042 RVA: 0x0013B484 File Offset: 0x00139884
	public static MeigewwwParam Add(string facilityId, string amount, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("facilityId", facilityId);
		meigewwwParam.Add("amount", amount);
		return meigewwwParam;
	}

	// Token: 0x06003EAB RID: 16043 RVA: 0x0013B4C0 File Offset: 0x001398C0
	public static MeigewwwParam Buyall(Buyall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/buy_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("townFacilityBuyStates", param.townFacilityBuyStates);
		return meigewwwParam;
	}

	// Token: 0x06003EAC RID: 16044 RVA: 0x0013B4F4 File Offset: 0x001398F4
	public static MeigewwwParam Buyall(TownFacilityBuyState[] townFacilityBuyStates, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/buy_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("townFacilityBuyStates", townFacilityBuyStates);
		return meigewwwParam;
	}

	// Token: 0x06003EAD RID: 16045 RVA: 0x0013B524 File Offset: 0x00139924
	public static MeigewwwParam Buildpointset(Buildpointset param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("buildPointIndex", param.buildPointIndex);
		meigewwwParam.Add("openState", param.openState);
		meigewwwParam.Add("buildTime", param.buildTime);
		meigewwwParam.Add("actionNo", param.actionNo);
		return meigewwwParam;
	}

	// Token: 0x06003EAE RID: 16046 RVA: 0x0013B5B4 File Offset: 0x001399B4
	public static MeigewwwParam Buildpointset(long managedTownFacilityId, int buildPointIndex, int openState, long buildTime, int actionNo, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("buildPointIndex", buildPointIndex);
		meigewwwParam.Add("openState", openState);
		meigewwwParam.Add("buildTime", buildTime);
		meigewwwParam.Add("actionNo", actionNo);
		return meigewwwParam;
	}

	// Token: 0x06003EAF RID: 16047 RVA: 0x0013B62C File Offset: 0x00139A2C
	public static MeigewwwParam Buildpointsetall(Buildpointsetall param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("townFacilitySetStates", param.townFacilitySetStates);
		return meigewwwParam;
	}

	// Token: 0x06003EB0 RID: 16048 RVA: 0x0013B660 File Offset: 0x00139A60
	public static MeigewwwParam Buildpointsetall(TownFacilitySetState[] townFacilitySetStates, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/set_all", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("townFacilitySetStates", townFacilitySetStates);
		return meigewwwParam;
	}

	// Token: 0x06003EB1 RID: 16049 RVA: 0x0013B690 File Offset: 0x00139A90
	public static MeigewwwParam Buildpointremove(Buildpointremove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("openState", param.openState);
		return meigewwwParam;
	}

	// Token: 0x06003EB2 RID: 16050 RVA: 0x0013B6E0 File Offset: 0x00139AE0
	public static MeigewwwParam Buildpointremove(long managedTownFacilityId, int openState, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/build_point/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("openState", openState);
		return meigewwwParam;
	}

	// Token: 0x06003EB3 RID: 16051 RVA: 0x0013B724 File Offset: 0x00139B24
	public static MeigewwwParam Preparelevelup(Preparelevelup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/prepare_level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("nextLevel", param.nextLevel);
		meigewwwParam.Add("openState", param.openState);
		meigewwwParam.Add("buildTime", param.buildTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB4 RID: 16052 RVA: 0x0013B7A0 File Offset: 0x00139BA0
	public static MeigewwwParam Preparelevelup(long managedTownFacilityId, int nextLevel, int openState, long buildTime, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/prepare_level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("nextLevel", nextLevel);
		meigewwwParam.Add("openState", openState);
		meigewwwParam.Add("buildTime", buildTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB5 RID: 16053 RVA: 0x0013B808 File Offset: 0x00139C08
	public static MeigewwwParam Levelup(Levelup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("nextLevel", param.nextLevel);
		meigewwwParam.Add("openState", param.openState);
		meigewwwParam.Add("actionTime", param.actionTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB6 RID: 16054 RVA: 0x0013B884 File Offset: 0x00139C84
	public static MeigewwwParam Levelup(long managedTownFacilityId, int nextLevel, int openState, long actionTime, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("nextLevel", nextLevel);
		meigewwwParam.Add("openState", openState);
		meigewwwParam.Add("actionTime", actionTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB7 RID: 16055 RVA: 0x0013B8EC File Offset: 0x00139CEC
	public static MeigewwwParam Gemlevelup(Gemlevelup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/gem_level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("nextLevel", param.nextLevel);
		meigewwwParam.Add("openState", param.openState);
		meigewwwParam.Add("actionTime", param.actionTime);
		meigewwwParam.Add("remainingTime", param.remainingTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB8 RID: 16056 RVA: 0x0013B97C File Offset: 0x00139D7C
	public static MeigewwwParam Gemlevelup(long managedTownFacilityId, int nextLevel, int openState, long actionTime, long remainingTime, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/gem_level_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("nextLevel", nextLevel);
		meigewwwParam.Add("openState", openState);
		meigewwwParam.Add("actionTime", actionTime);
		meigewwwParam.Add("remainingTime", remainingTime);
		return meigewwwParam;
	}

	// Token: 0x06003EB9 RID: 16057 RVA: 0x0013B9F4 File Offset: 0x00139DF4
	public static MeigewwwParam Itemup(Itemup param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/item_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("itemNo", param.itemNo);
		meigewwwParam.Add("amount", param.amount);
		meigewwwParam.Add("actionTime", param.actionTime);
		return meigewwwParam;
	}

	// Token: 0x06003EBA RID: 16058 RVA: 0x0013BA70 File Offset: 0x00139E70
	public static MeigewwwParam Itemup(long managedTownFacilityId, int itemNo, int amount, long actionTime, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/item_up", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("itemNo", itemNo);
		meigewwwParam.Add("amount", amount);
		meigewwwParam.Add("actionTime", actionTime);
		return meigewwwParam;
	}

	// Token: 0x06003EBB RID: 16059 RVA: 0x0013BAD8 File Offset: 0x00139ED8
	public static MeigewwwParam GetAll(GetAll param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003EBC RID: 16060 RVA: 0x0013BAFC File Offset: 0x00139EFC
	public static MeigewwwParam GetAll(MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/get_all", MeigewwwParam.eRequestMethod.Get, callback);
		return meigewwwParam;
	}

	// Token: 0x06003EBD RID: 16061 RVA: 0x0013BB20 File Offset: 0x00139F20
	public static MeigewwwParam SetState(SetState param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/state/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		meigewwwParam.Add("openState", param.openState);
		return meigewwwParam;
	}

	// Token: 0x06003EBE RID: 16062 RVA: 0x0013BB70 File Offset: 0x00139F70
	public static MeigewwwParam SetState(long managedTownFacilityId, int openState, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/state/set", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		meigewwwParam.Add("openState", openState);
		return meigewwwParam;
	}

	// Token: 0x06003EBF RID: 16063 RVA: 0x0013BBB4 File Offset: 0x00139FB4
	public static MeigewwwParam Sale(Sale param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		return meigewwwParam;
	}

	// Token: 0x06003EC0 RID: 16064 RVA: 0x0013BBE8 File Offset: 0x00139FE8
	public static MeigewwwParam Sale(string managedTownFacilityId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/sale", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		return meigewwwParam;
	}

	// Token: 0x06003EC1 RID: 16065 RVA: 0x0013BC18 File Offset: 0x0013A018
	public static MeigewwwParam Limitadd(Limitadd param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", param.num);
		return meigewwwParam;
	}

	// Token: 0x06003EC2 RID: 16066 RVA: 0x0013BC50 File Offset: 0x0013A050
	public static MeigewwwParam Limitadd(int num, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/limit/add", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("num", num);
		return meigewwwParam;
	}

	// Token: 0x06003EC3 RID: 16067 RVA: 0x0013BC84 File Offset: 0x0013A084
	public static MeigewwwParam Remove(Remove param, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", param.managedTownFacilityId);
		return meigewwwParam;
	}

	// Token: 0x06003EC4 RID: 16068 RVA: 0x0013BCBC File Offset: 0x0013A0BC
	public static MeigewwwParam Remove(long managedTownFacilityId, MeigewwwParam.Callback callback = null)
	{
		MeigewwwParam meigewwwParam = new MeigewwwParam();
		meigewwwParam.Init("player/town_facility/remove", MeigewwwParam.eRequestMethod.Post, callback);
		meigewwwParam.Add("managedTownFacilityId", managedTownFacilityId);
		return meigewwwParam;
	}
}
