﻿using System;

namespace STORERequestTypes
{
	// Token: 0x02000BD5 RID: 3029
	public class PurchaseGooglePlay
	{
		// Token: 0x04004594 RID: 17812
		public string receipt;

		// Token: 0x04004595 RID: 17813
		public string signature;
	}
}
