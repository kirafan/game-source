﻿using System;
using UnityEngine;

// Token: 0x02000FA8 RID: 4008
public sealed class InputTouch
{
	// Token: 0x060052E9 RID: 21225 RVA: 0x00172E84 File Offset: 0x00171284
	private InputTouch()
	{
		this.THRESHOLD_SAMETOUCH_PIX = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.03f;
		this.THRESHOLD_SAMEDBLTOUCH_PIX = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.1f;
		this.THRESHOLD_FLICK = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.05f;
		this.THRESHOLD_SWITCH_HAND = (float)Screen.width * 0.25f;
	}

	// Token: 0x060052EA RID: 21226 RVA: 0x00172F5A File Offset: 0x0017135A
	public static void Create()
	{
		if (InputTouch.m_Instance == null)
		{
			InputTouch.m_Instance = new InputTouch();
		}
		InputTouch.m_Instance.Init();
	}

	// Token: 0x17000598 RID: 1432
	// (get) Token: 0x060052EB RID: 21227 RVA: 0x00172F7A File Offset: 0x0017137A
	public static InputTouch Instance
	{
		get
		{
			return InputTouch.m_Instance;
		}
	}

	// Token: 0x060052EC RID: 21228 RVA: 0x00172F84 File Offset: 0x00171384
	public void Init()
	{
		for (int i = 0; i < 3; i++)
		{
			this.m_TouchInfo[i].m_bAvailable = false;
		}
	}

	// Token: 0x060052ED RID: 21229 RVA: 0x00172FB8 File Offset: 0x001713B8
	public void Update()
	{
		int num = (3 >= Input.touchCount) ? Input.touchCount : 3;
		int i = 0;
		int num2 = 0;
		for (int j = 0; j < 3; j++)
		{
			if (this.m_DblTouchInfo[j].IsAvailable())
			{
				InputTouch.DOUBLE_TOUCH_INFO[] dblTouchInfo = this.m_DblTouchInfo;
				int num3 = j;
				dblTouchInfo[num3].m_Time = dblTouchInfo[num3].m_Time + Frame.Instance.delta;
				if (this.m_DblTouchInfo[j].m_Time >= 15f || this.m_DblTouchInfo[j].IsTrigger())
				{
					this.m_DblTouchInfo[j].Clear();
				}
			}
		}
		while (i < num)
		{
			Touch touch = Input.GetTouch(i);
			if (touch.phase != TouchPhase.Canceled)
			{
				Vector2 position = touch.position;
				int num4 = this.SearchTouchInfo(touch.fingerId);
				int num5 = this.SearchDoubleTouchInfo(touch.fingerId);
				TouchPhase phase = touch.phase;
				switch (touch.phase)
				{
				case TouchPhase.Began:
					num4 = this.GetEmptyTouchInfo();
					if (num4 >= 0)
					{
						this.m_TouchInfo[num4].m_FingerID = touch.fingerId;
						this.m_TouchInfo[num4].m_StartPos = position;
						this.m_TouchInfo[num4].m_CurrentPos = position;
						this.m_TouchInfo[num4].m_OldPos = position;
						this.m_TouchInfo[num4].m_DiffFromStart = Vector3.zero;
						if (this.m_HandIdToIdx[1] == -1 && this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft))
						{
							this.m_HandIdToIdx[1] = num4;
							this.MakeHandTouchInfo(InputTouch.eHandID.eLeft, this.m_TouchInfo[num4], position);
						}
						else if (this.m_HandIdToIdx[0] == -1)
						{
							this.m_HandIdToIdx[0] = num4;
							this.MakeHandTouchInfo(InputTouch.eHandID.eRight, this.m_TouchInfo[num4], position);
						}
					}
					num5 = this.GetEmptyDoubleTouchInfo();
					if (num5 >= 0)
					{
						this.m_DblTouchInfo[num5].Start(touch.fingerId, position);
					}
					break;
				case TouchPhase.Moved:
				case TouchPhase.Stationary:
					if (num4 >= 0)
					{
						this.UpdateTouchInfo(ref this.m_TouchInfo[num4], position, false);
						if (!this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft) && this.m_HandIdToIdx[1] == num4)
						{
							if (Mathf.Abs(position.x - this.m_HandTouchInfo[1].m_CurrentPos.x) >= this.THRESHOLD_SWITCH_HAND)
							{
								if (this.IsThisTouchOnHand(position, InputTouch.eHandID.eRight) && this.m_HandIdToIdx[0] == -1)
								{
									this.m_HandIdToIdx[0] = num4;
									this.MakeHandTouchInfo(InputTouch.eHandID.eRight, this.m_TouchInfo[num4], position);
								}
								this.m_HandIdToIdx[1] = -1;
							}
						}
						else if (!this.IsThisTouchOnHand(position, InputTouch.eHandID.eRight) && this.m_HandIdToIdx[0] == num4 && Mathf.Abs(position.x - this.m_HandTouchInfo[0].m_CurrentPos.x) >= this.THRESHOLD_SWITCH_HAND)
						{
							if (this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft) && this.m_HandIdToIdx[1] == -1)
							{
								this.m_HandIdToIdx[1] = num4;
								this.MakeHandTouchInfo(InputTouch.eHandID.eLeft, this.m_TouchInfo[num4], position);
							}
							this.m_HandIdToIdx[0] = -1;
						}
						if (this.m_HandIdToIdx[1] == num4)
						{
							this.UpdateTouchInfo(ref this.m_HandTouchInfo[1], position, false);
						}
						else if (this.m_HandIdToIdx[0] == num4)
						{
							this.UpdateTouchInfo(ref this.m_HandTouchInfo[0], position, false);
						}
					}
					break;
				case TouchPhase.Ended:
					if (num4 >= 0)
					{
						this.UpdateTouchInfo(ref this.m_TouchInfo[num4], position, true);
						if (this.m_HandIdToIdx[1] == num4)
						{
							this.UpdateTouchInfo(ref this.m_HandTouchInfo[1], position, true);
						}
						else if (this.m_HandIdToIdx[0] == num4)
						{
							this.UpdateTouchInfo(ref this.m_HandTouchInfo[0], position, true);
						}
					}
					if (num5 >= 0)
					{
						this.UpdateTouchCnt(num5, position);
					}
					break;
				}
				if (num4 >= 0)
				{
					this.m_TouchInfo[num4].m_bAvailable = true;
					this.m_TouchInfo[num4].m_RawPos = position;
					this.m_TouchInfo[num4].m_TouchPhase = phase;
					num2 |= 1 << num4;
				}
			}
			i++;
		}
		this.m_AvailableTouchCnt = 0;
		for (int k = 0; k < 3; k++)
		{
			if (this.m_TouchInfo[k].m_bAvailable)
			{
				if ((num2 & 1 << k) == 0)
				{
					this.m_TouchInfo[k].m_bAvailable = false;
				}
				this.m_AvailableTouchCnt++;
			}
		}
		if (this.m_HandIdToIdx[1] != -1 && !this.m_TouchInfo[this.m_HandIdToIdx[1]].m_bAvailable)
		{
			this.m_HandIdToIdx[1] = -1;
			this.m_HandTouchInfo[1].m_bAvailable = false;
		}
		if (this.m_HandIdToIdx[0] != -1 && !this.m_TouchInfo[this.m_HandIdToIdx[0]].m_bAvailable)
		{
			this.m_HandIdToIdx[0] = -1;
			this.m_HandTouchInfo[0].m_bAvailable = false;
		}
	}

	// Token: 0x060052EE RID: 21230 RVA: 0x00173570 File Offset: 0x00171970
	private int SearchTouchInfo(int fingerID)
	{
		int num = 0;
		foreach (InputTouch.TOUCH_INFO touch_INFO in this.m_TouchInfo)
		{
			if (touch_INFO.m_FingerID == fingerID)
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	// Token: 0x060052EF RID: 21231 RVA: 0x001735BC File Offset: 0x001719BC
	private int GetEmptyTouchInfo()
	{
		int num = 0;
		foreach (InputTouch.TOUCH_INFO touch_INFO in this.m_TouchInfo)
		{
			if (!touch_INFO.m_bAvailable)
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	// Token: 0x060052F0 RID: 21232 RVA: 0x00173608 File Offset: 0x00171A08
	private int SearchDoubleTouchInfo(int fingerID)
	{
		int num = 0;
		foreach (InputTouch.DOUBLE_TOUCH_INFO double_TOUCH_INFO in this.m_DblTouchInfo)
		{
			if (double_TOUCH_INFO.m_FingerID == fingerID)
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	// Token: 0x060052F1 RID: 21233 RVA: 0x00173654 File Offset: 0x00171A54
	private int GetEmptyDoubleTouchInfo()
	{
		int num = 0;
		foreach (InputTouch.DOUBLE_TOUCH_INFO double_TOUCH_INFO in this.m_DblTouchInfo)
		{
			if (!double_TOUCH_INFO.IsAvailable())
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	// Token: 0x060052F2 RID: 21234 RVA: 0x001736A0 File Offset: 0x00171AA0
	private void UpdateTouchCnt(int idx, Vector2 pos)
	{
		float num = MathFunc.Distance(ref pos, ref this.m_DblTouchInfo[idx].m_StartPos);
		if (num <= this.THRESHOLD_SAMEDBLTOUCH_PIX)
		{
			InputTouch.DOUBLE_TOUCH_INFO[] dblTouchInfo = this.m_DblTouchInfo;
			dblTouchInfo[idx].m_TouchCnt = dblTouchInfo[idx].m_TouchCnt + 1;
		}
	}

	// Token: 0x060052F3 RID: 21235 RVA: 0x001736EC File Offset: 0x00171AEC
	private bool IsThisTouchOnHand(Vector2 position, InputTouch.eHandID hand)
	{
		float num = (float)Screen.width / 2f;
		if (hand == InputTouch.eHandID.eLeft)
		{
			return position.x <= num;
		}
		return position.x > num;
	}

	// Token: 0x060052F4 RID: 21236 RVA: 0x00173728 File Offset: 0x00171B28
	private void MakeHandTouchInfo(InputTouch.eHandID hand, InputTouch.TOUCH_INFO ti, Vector2 curPos)
	{
		this.m_HandTouchInfo[(int)hand].m_bAvailable = ti.m_bAvailable;
		this.m_HandTouchInfo[(int)hand].m_StartPos = curPos;
		this.m_HandTouchInfo[(int)hand].m_CurrentPos = curPos;
		this.m_HandTouchInfo[(int)hand].m_OldPos = curPos;
		this.m_HandTouchInfo[(int)hand].m_DiffFromStart = Vector2.zero;
		this.m_HandTouchInfo[(int)hand].m_RawPos = ti.m_RawPos;
		this.m_HandTouchInfo[(int)hand].m_TouchPhase = TouchPhase.Began;
		this.m_HandTouchInfo[(int)hand].m_bTrigger = false;
	}

	// Token: 0x060052F5 RID: 21237 RVA: 0x001737DC File Offset: 0x00171BDC
	private void UpdateTouchInfo(ref InputTouch.TOUCH_INFO ti, Vector2 curPos, bool bPhaseEnd)
	{
		Vector2 vector = curPos - ti.m_StartPos;
		float num = MathFunc.Magnitude(ref vector);
		if (num >= this.THRESHOLD_SAMETOUCH_PIX)
		{
			ti.m_DiffFromStart.z = num;
			ti.m_DiffFromStart.x = vector.x / num;
			ti.m_DiffFromStart.y = vector.y / num;
		}
		else
		{
			ti.m_DiffFromStart = Vector3.zero;
			if (bPhaseEnd)
			{
				ti.m_bTrigger = true;
			}
		}
		ti.m_OldPos = ti.m_CurrentPos;
		ti.m_CurrentPos = ti.m_DiffFromStart;
		ti.m_CurrentPos = ti.m_CurrentPos * ti.m_DiffFromStart.z + ti.m_StartPos;
		ti.m_RawPos = curPos;
	}

	// Token: 0x060052F6 RID: 21238 RVA: 0x001738A6 File Offset: 0x00171CA6
	public bool IsAvailable(int idx)
	{
		return this.m_TouchInfo[idx].m_bAvailable;
	}

	// Token: 0x060052F7 RID: 21239 RVA: 0x001738B9 File Offset: 0x00171CB9
	public bool IsAvailable(InputTouch.eHandID tid)
	{
		return this.m_HandIdToIdx[(int)tid] != -1;
	}

	// Token: 0x060052F8 RID: 21240 RVA: 0x001738D0 File Offset: 0x00171CD0
	public InputTouch.TOUCH_INFO GetInfo(InputTouch.eHandID tid)
	{
		return this.m_HandTouchInfo[(int)tid];
	}

	// Token: 0x060052F9 RID: 21241 RVA: 0x001738E3 File Offset: 0x00171CE3
	public InputTouch.TOUCH_INFO GetInfo(int idx)
	{
		return this.m_TouchInfo[idx];
	}

	// Token: 0x060052FA RID: 21242 RVA: 0x001738F6 File Offset: 0x00171CF6
	public int GetAvailableTouchCnt()
	{
		return this.m_AvailableTouchCnt;
	}

	// Token: 0x060052FB RID: 21243 RVA: 0x00173900 File Offset: 0x00171D00
	public bool DetectTriggerOfSingleTouch(out Vector2 pos, int idx)
	{
		if (this.IsAvailable(idx))
		{
			InputTouch.TOUCH_INFO info = this.GetInfo(idx);
			if (info.m_TouchPhase == TouchPhase.Ended)
			{
				Vector2 vector = info.m_RawPos - info.m_StartPos;
				float num = MathFunc.Magnitude(ref vector);
				if (num < this.THRESHOLD_SAMETOUCH_PIX)
				{
					pos = info.m_RawPos;
					GameScreen instance = GameScreen.Instance;
					if (pos.x >= instance.screenSpace.fixedAspectPos.x && pos.x <= instance.screenSpace.fixedAspectPos.x + instance.screenSpace.fixedAspectSize.x && instance.screenSpace.size.y - pos.y >= instance.screenSpace.fixedAspectPos.y && instance.screenSpace.size.y - pos.y <= instance.screenSpace.fixedAspectPos.y + instance.screenSpace.fixedAspectSize.y)
					{
						return true;
					}
				}
			}
		}
		pos = Vector2.zero;
		return false;
	}

	// Token: 0x060052FC RID: 21244 RVA: 0x00173A47 File Offset: 0x00171E47
	public bool IsTriggerOfDoubleTouch(int idx)
	{
		return this.m_DblTouchInfo[idx].IsAvailable() && this.m_DblTouchInfo[idx].IsTrigger();
	}

	// Token: 0x060052FD RID: 21245 RVA: 0x00173A73 File Offset: 0x00171E73
	public bool DectectTriggerOfDoubleTouch(out Vector2 pos, int idx)
	{
		if (this.IsTriggerOfDoubleTouch(idx))
		{
			pos = this.m_DblTouchInfo[idx].m_StartPos;
			return true;
		}
		pos = Vector2.zero;
		return false;
	}

	// Token: 0x060052FE RID: 21246 RVA: 0x00173AA8 File Offset: 0x00171EA8
	public bool GetFlick(out Vector3 dir, out Vector3 start, out Vector3 end, int idx)
	{
		if (this.IsAvailable(idx))
		{
			InputTouch.TOUCH_INFO info = this.GetInfo(idx);
			Vector2 vector = info.m_CurrentPos - info.m_OldPos;
			float num = MathFunc.Magnitude(ref vector);
			if (num >= this.THRESHOLD_FLICK)
			{
				dir.x = vector.x;
				dir.y = vector.y;
				dir.z = num;
				start = info.m_OldPos;
				end = info.m_CurrentPos;
				return true;
			}
		}
		start = Vector3.zero;
		end = Vector3.zero;
		dir = Vector3.zero;
		return false;
	}

	// Token: 0x060052FF RID: 21247 RVA: 0x00173B5C File Offset: 0x00171F5C
	public bool GetFlick(out Vector3 dir, int idx)
	{
		Vector3 vector;
		Vector3 vector2;
		return this.GetFlick(out dir, out vector, out vector2, idx);
	}

	// Token: 0x04005429 RID: 21545
	private static InputTouch m_Instance;

	// Token: 0x0400542A RID: 21546
	private const int INVALIDATE_IDX = -1;

	// Token: 0x0400542B RID: 21547
	public const int TOUCH_MAX = 3;

	// Token: 0x0400542C RID: 21548
	private const float THRESHOLD_SAMETOUCH_RATIO = 0.03f;

	// Token: 0x0400542D RID: 21549
	private const float THRESHOLD_SAMEDBLTOUCH_RATIO = 0.1f;

	// Token: 0x0400542E RID: 21550
	private const float THRESHOLD_FLICK_RATIO = 0.05f;

	// Token: 0x0400542F RID: 21551
	public const float THRESHOLD_DBLTOUCH_FRAME = 15f;

	// Token: 0x04005430 RID: 21552
	private const float THRESHOLD_SWITCH_HAND_COEFF = 0.25f;

	// Token: 0x04005431 RID: 21553
	public float THRESHOLD_SAMETOUCH_PIX;

	// Token: 0x04005432 RID: 21554
	public float THRESHOLD_SAMEDBLTOUCH_PIX;

	// Token: 0x04005433 RID: 21555
	private float THRESHOLD_SWITCH_HAND;

	// Token: 0x04005434 RID: 21556
	public float THRESHOLD_FLICK;

	// Token: 0x04005435 RID: 21557
	private InputTouch.TOUCH_INFO[] m_TouchInfo = new InputTouch.TOUCH_INFO[3];

	// Token: 0x04005436 RID: 21558
	private InputTouch.DOUBLE_TOUCH_INFO[] m_DblTouchInfo = new InputTouch.DOUBLE_TOUCH_INFO[3];

	// Token: 0x04005437 RID: 21559
	private InputTouch.TOUCH_INFO[] m_HandTouchInfo = new InputTouch.TOUCH_INFO[2];

	// Token: 0x04005438 RID: 21560
	private int[] m_HandIdToIdx = new int[2];

	// Token: 0x04005439 RID: 21561
	private int m_AvailableTouchCnt;

	// Token: 0x02000FA9 RID: 4009
	public enum eHandID
	{
		// Token: 0x0400543B RID: 21563
		eRight,
		// Token: 0x0400543C RID: 21564
		eLeft,
		// Token: 0x0400543D RID: 21565
		eMax
	}

	// Token: 0x02000FAA RID: 4010
	public struct TOUCH_INFO
	{
		// Token: 0x0400543E RID: 21566
		public Vector2 m_StartPos;

		// Token: 0x0400543F RID: 21567
		public Vector2 m_CurrentPos;

		// Token: 0x04005440 RID: 21568
		public Vector2 m_OldPos;

		// Token: 0x04005441 RID: 21569
		public Vector3 m_DiffFromStart;

		// Token: 0x04005442 RID: 21570
		public Vector2 m_RawPos;

		// Token: 0x04005443 RID: 21571
		public TouchPhase m_TouchPhase;

		// Token: 0x04005444 RID: 21572
		public int m_FingerID;

		// Token: 0x04005445 RID: 21573
		public bool m_bAvailable;

		// Token: 0x04005446 RID: 21574
		public bool m_bTrigger;
	}

	// Token: 0x02000FAB RID: 4011
	private struct DOUBLE_TOUCH_INFO
	{
		// Token: 0x06005301 RID: 21249 RVA: 0x00173B77 File Offset: 0x00171F77
		public void Clear()
		{
			this.m_FingerID = -1;
		}

		// Token: 0x06005302 RID: 21250 RVA: 0x00173B80 File Offset: 0x00171F80
		public void Start(int fingerID, Vector2 startPos)
		{
			this.m_StartPos = startPos;
			this.m_FingerID = fingerID;
			this.m_Time = 0f;
			this.m_TouchCnt = 0;
		}

		// Token: 0x06005303 RID: 21251 RVA: 0x00173BA2 File Offset: 0x00171FA2
		public bool IsAvailable()
		{
			return this.m_FingerID != -1;
		}

		// Token: 0x06005304 RID: 21252 RVA: 0x00173BB0 File Offset: 0x00171FB0
		public bool IsTrigger()
		{
			return this.m_TouchCnt >= 2;
		}

		// Token: 0x04005447 RID: 21575
		public Vector2 m_StartPos;

		// Token: 0x04005448 RID: 21576
		public int m_FingerID;

		// Token: 0x04005449 RID: 21577
		public float m_Time;

		// Token: 0x0400544A RID: 21578
		public int m_TouchCnt;
	}
}
