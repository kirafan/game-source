﻿using System;

// Token: 0x02000E97 RID: 3735
public class FieldAccessorUtil
{
	// Token: 0x06004D59 RID: 19801 RVA: 0x00159020 File Offset: 0x00157420
	public static IPropertyAccessor GetPropertyAccessor(Type type, string propertyName)
	{
		return type.GetProperty(propertyName).ToAccessor();
	}
}
