﻿using System;

namespace ADV
{
	// Token: 0x02000068 RID: 104
	public class ADVScriptCommand
	{
		// Token: 0x060002DE RID: 734 RVA: 0x0000A2E7 File Offset: 0x000086E7
		public virtual void Wait(float m_Sec)
		{
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000A2E9 File Offset: 0x000086E9
		public virtual void GoTo(uint m_ScriptID)
		{
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000A2EB File Offset: 0x000086EB
		public virtual void PlayBGM(string m_BGMCueName, float m_FadeInSec)
		{
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000A2ED File Offset: 0x000086ED
		public virtual void PlayBGM(string m_BGMCueName)
		{
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000A2EF File Offset: 0x000086EF
		public virtual void StopBGM(float m_FadeOutSec)
		{
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000A2F1 File Offset: 0x000086F1
		public virtual void StopBGM()
		{
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000A2F3 File Offset: 0x000086F3
		public virtual void PlayVOICE(string m_ADVCharaID, uint m_CueName)
		{
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0000A2F5 File Offset: 0x000086F5
		public virtual void StopVOICE()
		{
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000A2F7 File Offset: 0x000086F7
		public virtual void WaitVOICE()
		{
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x0000A2F9 File Offset: 0x000086F9
		public virtual void PlaySE(string m_SeCueName)
		{
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000A2FB File Offset: 0x000086FB
		public virtual void StopSE()
		{
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000A2FD File Offset: 0x000086FD
		public virtual void WaitSE()
		{
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000A2FF File Offset: 0x000086FF
		public virtual void FadeIn(string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000A301 File Offset: 0x00008701
		public virtual void FadeOut(string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000A303 File Offset: 0x00008703
		public virtual void FillScreen(string m_RGBA)
		{
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000A305 File Offset: 0x00008705
		public virtual void Fade(string m_RGBAStart, string m_RGBAEnd, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000A307 File Offset: 0x00008707
		public virtual void WaitFade()
		{
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000A309 File Offset: 0x00008709
		public virtual void BGVisible(uint m_ID, string m_FileNameWithoutExt)
		{
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000A30B File Offset: 0x0000870B
		public virtual void BGScroll(uint m_ID, float m_X, float m_Y, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000A30D File Offset: 0x0000870D
		public virtual void BGColor(uint m_ID, string m_StartColor, string m_EndColor, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0000A30F File Offset: 0x0000870F
		public virtual void BGScale(uint m_ID, float m_Scale, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000A311 File Offset: 0x00008711
		public virtual void BGPointZoom(uint m_ID, float m_Scale, float m_Sec, float m_X, float m_Y, int m_CurveType)
		{
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000A313 File Offset: 0x00008713
		public virtual void Shake(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x0000A315 File Offset: 0x00008715
		public virtual void ShakeChara(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000A317 File Offset: 0x00008717
		public virtual void ShakeBG(int m_ShakeType, float m_Time)
		{
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000A319 File Offset: 0x00008719
		public virtual void ShakeChara(int m_ShakeType, float m_Time, string m_ADVCharaID)
		{
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000A31B File Offset: 0x0000871B
		public virtual void CharaShot(string m_ADVCharaID)
		{
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000A31D File Offset: 0x0000871D
		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2)
		{
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000A31F File Offset: 0x0000871F
		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3)
		{
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000A321 File Offset: 0x00008721
		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4)
		{
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000A323 File Offset: 0x00008723
		public virtual void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000A325 File Offset: 0x00008725
		public virtual void CharaShotFade(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000A327 File Offset: 0x00008727
		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, float m_Sec)
		{
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000A329 File Offset: 0x00008729
		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, float m_Sec)
		{
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000A32B File Offset: 0x0000872B
		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, float m_Sec)
		{
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000A32D File Offset: 0x0000872D
		public virtual void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5, float m_Sec)
		{
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000A32F File Offset: 0x0000872F
		public virtual void SetCharaShotPosition(int m_ADVCharaPos)
		{
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000A331 File Offset: 0x00008731
		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2)
		{
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000A333 File Offset: 0x00008733
		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3)
		{
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000A335 File Offset: 0x00008735
		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4)
		{
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000A337 File Offset: 0x00008737
		public virtual void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4, int m_ADVCharaPos5)
		{
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000A339 File Offset: 0x00008739
		public virtual void CharaOutAll()
		{
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000A33B File Offset: 0x0000873B
		public virtual void CharaOutAllFade(float m_Sec)
		{
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000A33D File Offset: 0x0000873D
		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition)
		{
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000A33F File Offset: 0x0000873F
		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000A341 File Offset: 0x00008741
		public virtual void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000A343 File Offset: 0x00008743
		public virtual void CharaOut(string m_ADVCharaID)
		{
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000A345 File Offset: 0x00008745
		public virtual void CharaOut(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000A347 File Offset: 0x00008747
		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, float m_Sec)
		{
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000A349 File Offset: 0x00008749
		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000A34B File Offset: 0x0000874B
		public virtual void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x06000311 RID: 785 RVA: 0x0000A34D File Offset: 0x0000874D
		public virtual void CharaOutFade(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06000312 RID: 786 RVA: 0x0000A34F File Offset: 0x0000874F
		public virtual void CharaOutFade(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000313 RID: 787 RVA: 0x0000A351 File Offset: 0x00008751
		public virtual void SetTarget(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0000A353 File Offset: 0x00008753
		public virtual void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
		}

		// Token: 0x06000315 RID: 789 RVA: 0x0000A355 File Offset: 0x00008755
		public virtual void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000316 RID: 790 RVA: 0x0000A357 File Offset: 0x00008757
		public virtual void CharaOutTarget(int m_EndSide, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000317 RID: 791 RVA: 0x0000A359 File Offset: 0x00008759
		public virtual void WaitCharaFade()
		{
		}

		// Token: 0x06000318 RID: 792 RVA: 0x0000A35B File Offset: 0x0000875B
		public virtual void CharaAlignment(float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000319 RID: 793 RVA: 0x0000A35D File Offset: 0x0000875D
		public virtual void CharaSwap(string m_ADVCharaID1, string m_ADVCharaID2, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000A35F File Offset: 0x0000875F
		public virtual void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x0600031B RID: 795 RVA: 0x0000A361 File Offset: 0x00008761
		public virtual void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_Xoffset, float m_Yoffset, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x0600031C RID: 796 RVA: 0x0000A363 File Offset: 0x00008763
		public virtual void WaitCharaMove()
		{
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0000A365 File Offset: 0x00008765
		public virtual void CharaMot(string m_ADVCharaID, string m_MotID)
		{
		}

		// Token: 0x0600031E RID: 798 RVA: 0x0000A367 File Offset: 0x00008767
		public virtual void WaitMotion()
		{
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0000A369 File Offset: 0x00008769
		public virtual void CharaFace(string m_ADVCharaID, int m_FaceID)
		{
		}

		// Token: 0x06000320 RID: 800 RVA: 0x0000A36B File Offset: 0x0000876B
		public virtual void CharaPose(string m_ADVCharaID, string m_PoseName)
		{
		}

		// Token: 0x06000321 RID: 801 RVA: 0x0000A36D File Offset: 0x0000876D
		public virtual void CharaEmotion(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000A36F File Offset: 0x0000876F
		public virtual void CharaEmotion(string m_ADVCharaID, string m_EmotionID, int m_EmoPosition)
		{
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000A371 File Offset: 0x00008771
		public virtual void EmotionEnd(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000A373 File Offset: 0x00008773
		public virtual void WaitEmotion(string m_ADVCharaID, string m_EmotionID)
		{
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000A375 File Offset: 0x00008775
		public virtual void CharaHighlight(string m_ADVCharaID)
		{
		}

		// Token: 0x06000326 RID: 806 RVA: 0x0000A377 File Offset: 0x00008777
		public virtual void CharaHighlightAll()
		{
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000A379 File Offset: 0x00008779
		public virtual void CharaHighlightReset(string m_ADVCharaID)
		{
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000A37B File Offset: 0x0000877B
		public virtual void CharaHighlightResetAll()
		{
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000A37D File Offset: 0x0000877D
		public virtual void CharaShading(string m_ADVCharaID)
		{
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000A37F File Offset: 0x0000877F
		public virtual void CharaShadingAll()
		{
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000A381 File Offset: 0x00008781
		public virtual void CharaHighlightTalker(string m_ADVCharaID)
		{
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000A383 File Offset: 0x00008783
		public virtual void CharaPriorityTop(string m_ADVCharaID)
		{
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000A385 File Offset: 0x00008785
		public virtual void CharaPriorityTopSet(string m_ADVCharaID)
		{
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000A387 File Offset: 0x00008787
		public virtual void CharaPriorityBottom(string m_ADVCharaID)
		{
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000A389 File Offset: 0x00008789
		public virtual void CharaPriorityBottomSet(string m_ADVCharaID)
		{
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000A38B File Offset: 0x0000878B
		public virtual void CharaPriorityReset(string m_ADVCharaID)
		{
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000A38D File Offset: 0x0000878D
		public virtual void CharaTransparency(string m_ADVCharaID, uint m_Alpha, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000332 RID: 818 RVA: 0x0000A38F File Offset: 0x0000878F
		public virtual void CharaTransparencyAll(uint m_Alpha, float m_Sec, int m_CurveType)
		{
		}

		// Token: 0x06000333 RID: 819 RVA: 0x0000A391 File Offset: 0x00008791
		public virtual void CharaTransparencyResetAll()
		{
		}

		// Token: 0x06000334 RID: 820 RVA: 0x0000A393 File Offset: 0x00008793
		public virtual void Effect(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000A395 File Offset: 0x00008795
		public virtual void Effect(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000A397 File Offset: 0x00008797
		public virtual void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06000337 RID: 823 RVA: 0x0000A399 File Offset: 0x00008799
		public virtual void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06000338 RID: 824 RVA: 0x0000A39B File Offset: 0x0000879B
		public virtual void EffectScreen(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06000339 RID: 825 RVA: 0x0000A39D File Offset: 0x0000879D
		public virtual void EffectScreen(string m_effectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000A39F File Offset: 0x0000879F
		public virtual void EffectLoop(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000A3A1 File Offset: 0x000087A1
		public virtual void EffectLoop(string m_effectID, int m_StandPosition)
		{
		}

		// Token: 0x0600033C RID: 828 RVA: 0x0000A3A3 File Offset: 0x000087A3
		public virtual void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x0600033D RID: 829 RVA: 0x0000A3A5 File Offset: 0x000087A5
		public virtual void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x0600033E RID: 830 RVA: 0x0000A3A7 File Offset: 0x000087A7
		public virtual void EffectScreenLoop(string m_effectID, int m_Anchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x0600033F RID: 831 RVA: 0x0000A3A9 File Offset: 0x000087A9
		public virtual void EffectScreenLoop(string m_effectID, int m_Anchor)
		{
		}

		// Token: 0x06000340 RID: 832 RVA: 0x0000A3AB File Offset: 0x000087AB
		public virtual void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y)
		{
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000A3AD File Offset: 0x000087AD
		public virtual void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition)
		{
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000A3AF File Offset: 0x000087AF
		public virtual void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06000343 RID: 835 RVA: 0x0000A3B1 File Offset: 0x000087B1
		public virtual void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06000344 RID: 836 RVA: 0x0000A3B3 File Offset: 0x000087B3
		public virtual void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y)
		{
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000A3B5 File Offset: 0x000087B5
		public virtual void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor)
		{
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000A3B7 File Offset: 0x000087B7
		public virtual void EffectLoopTargetEnd(string m_effectID)
		{
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000A3B9 File Offset: 0x000087B9
		public virtual void EffectEnd(string m_effectID)
		{
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000A3BB File Offset: 0x000087BB
		public virtual void WaitEffect(string m_effectID)
		{
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000A3BD File Offset: 0x000087BD
		public virtual void EffectMuteSE()
		{
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000A3BF File Offset: 0x000087BF
		public virtual void EffectMuteSEReset()
		{
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000A3C1 File Offset: 0x000087C1
		public virtual void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID, sbyte m_IgnoreParticle)
		{
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000A3C3 File Offset: 0x000087C3
		public virtual void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID)
		{
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000A3C5 File Offset: 0x000087C5
		public virtual void EffectLoopWithPreset(int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000A3C7 File Offset: 0x000087C7
		public virtual void EffectLoopWithPreset(int m_StandPosition)
		{
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000A3C9 File Offset: 0x000087C9
		public virtual void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000A3CB File Offset: 0x000087CB
		public virtual void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor)
		{
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000A3CD File Offset: 0x000087CD
		public virtual void EffectScreenLoopWithPreset(int m_Anchor, float m_X, float m_Y, float m_Rotate)
		{
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000A3CF File Offset: 0x000087CF
		public virtual void EffectScreenLoopWithPreset(int m_Anchor)
		{
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000A3D1 File Offset: 0x000087D1
		public virtual void SpriteVisible(uint m_ID, string m_FileNameWithoutExt, uint m_VisibleFlg)
		{
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000A3D3 File Offset: 0x000087D3
		public virtual void SpritePos(uint m_ID, float m_X, float m_Y)
		{
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000A3D5 File Offset: 0x000087D5
		public virtual void SpritePos(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000A3D7 File Offset: 0x000087D7
		public virtual void SpriteScale(uint m_ID, float m_X, float m_Y)
		{
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000A3D9 File Offset: 0x000087D9
		public virtual void SpriteScale(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000A3DB File Offset: 0x000087DB
		public virtual void SpriteColor(uint m_ID, string m_RGBA)
		{
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000A3DD File Offset: 0x000087DD
		public virtual void SpriteColor(uint m_ID, string m_RGBA, float m_Sec)
		{
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000A3DF File Offset: 0x000087DF
		public virtual void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos)
		{
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000A3E1 File Offset: 0x000087E1
		public virtual void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID)
		{
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000A3E3 File Offset: 0x000087E3
		public virtual void CharaTalk(uint m_TextID, int m_FaceID)
		{
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0000A3E5 File Offset: 0x000087E5
		public virtual void CharaTalk(uint m_TextID)
		{
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0000A3E7 File Offset: 0x000087E7
		public virtual void CloseTalk()
		{
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000A3E9 File Offset: 0x000087E9
		public virtual void ClearNovelText()
		{
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000A3EB File Offset: 0x000087EB
		public virtual void NovelAnchorSetting(int m_AnchorID, float m_X, float m_Y)
		{
		}

		// Token: 0x06000361 RID: 865 RVA: 0x0000A3ED File Offset: 0x000087ED
		public virtual void NovelAnchorSettingDefault()
		{
		}

		// Token: 0x06000362 RID: 866 RVA: 0x0000A3EF File Offset: 0x000087EF
		public virtual void AddNovelText(uint m_TextID)
		{
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0000A3F1 File Offset: 0x000087F1
		public virtual void NovelInsertLine()
		{
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000A3F3 File Offset: 0x000087F3
		public virtual void WarpPrepare(string m_ADVCharaID, float m_Sec)
		{
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000A3F5 File Offset: 0x000087F5
		public virtual void WarpPrepareWait()
		{
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000A3F7 File Offset: 0x000087F7
		public virtual void WarpStart(float m_Sec)
		{
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000A3F9 File Offset: 0x000087F9
		public virtual void WarpWait()
		{
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000A3FB File Offset: 0x000087FB
		public virtual void SetHDRFactor(string m_ADVCharaID, float m_Value, float m_Sec)
		{
		}
	}
}
