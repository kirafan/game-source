﻿using System;
using System.Collections.Generic;
using Meige;
using RoomResponseTypes;
using WWWTypes;

// Token: 0x02000C09 RID: 3081
public static class RoomResponse
{
	// Token: 0x06003FB5 RID: 16309 RVA: 0x0013CFA0 File Offset: 0x0013B3A0
	public static Set Set(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Set>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB6 RID: 16310 RVA: 0x0013CFB8 File Offset: 0x0013B3B8
	public static GetAll GetAll(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<GetAll>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB7 RID: 16311 RVA: 0x0013CFD0 File Offset: 0x0013B3D0
	public static Remove Remove(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Remove>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB8 RID: 16312 RVA: 0x0013CFE8 File Offset: 0x0013B3E8
	public static Setactiveroom Setactiveroom(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Setactiveroom>(param, dialogType, acceptableResultCodes);
	}

	// Token: 0x06003FB9 RID: 16313 RVA: 0x0013D000 File Offset: 0x0013B400
	public static Getactiveroom Getactiveroom(MeigewwwParam param, ResponseCommon.DialogType dialogType = ResponseCommon.DialogType.None, List<ResultCode> acceptableResultCodes = null)
	{
		return ResponseCommon.Func<Getactiveroom>(param, dialogType, acceptableResultCodes);
	}
}
