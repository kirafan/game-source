﻿using System;
using UnityEngine;

// Token: 0x020002FB RID: 763
public static class Transform_Ext
{
	// Token: 0x06000EED RID: 3821 RVA: 0x0004FF18 File Offset: 0x0004E318
	public static void positionX(this Transform self, float x)
	{
		self.position = new Vector3(x, self.position.y, self.position.z);
	}

	// Token: 0x06000EEE RID: 3822 RVA: 0x0004FF50 File Offset: 0x0004E350
	public static void positionY(this Transform self, float y)
	{
		self.position = new Vector3(self.position.x, y, self.position.z);
	}

	// Token: 0x06000EEF RID: 3823 RVA: 0x0004FF88 File Offset: 0x0004E388
	public static void positionZ(this Transform self, float z)
	{
		self.position = new Vector3(self.position.x, self.position.y, z);
	}

	// Token: 0x06000EF0 RID: 3824 RVA: 0x0004FFC0 File Offset: 0x0004E3C0
	public static void localPosX(this Transform self, float x)
	{
		self.localPosition = new Vector3(x, self.localPosition.y, self.localPosition.z);
	}

	// Token: 0x06000EF1 RID: 3825 RVA: 0x0004FFF8 File Offset: 0x0004E3F8
	public static void localPosY(this Transform self, float y)
	{
		self.localPosition = new Vector3(self.localPosition.x, y, self.localPosition.z);
	}

	// Token: 0x06000EF2 RID: 3826 RVA: 0x00050030 File Offset: 0x0004E430
	public static void localPosZ(this Transform self, float z)
	{
		self.localPosition = new Vector3(self.localPosition.x, self.localPosition.y, z);
	}

	// Token: 0x06000EF3 RID: 3827 RVA: 0x00050068 File Offset: 0x0004E468
	public static void localScaleX(this Transform self, float x)
	{
		self.localScale = new Vector3(x, self.localScale.y, self.localScale.z);
	}

	// Token: 0x06000EF4 RID: 3828 RVA: 0x000500A0 File Offset: 0x0004E4A0
	public static void localScaleY(this Transform self, float y)
	{
		self.localScale = new Vector3(self.localScale.x, y, self.localScale.z);
	}

	// Token: 0x06000EF5 RID: 3829 RVA: 0x000500D8 File Offset: 0x0004E4D8
	public static void localScaleZ(this Transform self, float z)
	{
		self.localScale = new Vector3(self.localScale.x, self.localScale.y, z);
	}

	// Token: 0x06000EF6 RID: 3830 RVA: 0x0005010D File Offset: 0x0004E50D
	public static Transform SearchParent(this Transform self)
	{
		return Transform_Ext.parentObject(self);
	}

	// Token: 0x06000EF7 RID: 3831 RVA: 0x00050115 File Offset: 0x0004E515
	public static Transform SearchParent(this Transform self, string tag)
	{
		return Transform_Ext.parentObject(self, tag);
	}

	// Token: 0x06000EF8 RID: 3832 RVA: 0x0005011E File Offset: 0x0004E51E
	private static Transform parentObject(Transform obj)
	{
		if (obj.parent == null)
		{
			return obj;
		}
		return Transform_Ext.parentObject(obj.parent);
	}

	// Token: 0x06000EF9 RID: 3833 RVA: 0x0005013E File Offset: 0x0004E53E
	private static Transform parentObject(Transform obj, string tag)
	{
		if (obj == null || obj.CompareTag(tag))
		{
			return obj;
		}
		return Transform_Ext.parentObject(obj.parent, tag);
	}
}
