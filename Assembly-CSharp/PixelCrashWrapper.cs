﻿using System;
using Meige;
using UnityEngine;

// Token: 0x02000626 RID: 1574
public class PixelCrashWrapper : MonoBehaviour
{
	// Token: 0x170001CF RID: 463
	// (get) Token: 0x06001EA5 RID: 7845 RVA: 0x000A615F File Offset: 0x000A455F
	public float rate
	{
		get
		{
			return this.m_Rate;
		}
	}

	// Token: 0x170001D0 RID: 464
	// (get) Token: 0x06001EA6 RID: 7846 RVA: 0x000A6167 File Offset: 0x000A4567
	public bool isAlive
	{
		get
		{
			return this.m_isAlive;
		}
	}

	// Token: 0x06001EA7 RID: 7847 RVA: 0x000A616F File Offset: 0x000A456F
	private void OnDestroy()
	{
		if (this.m_CrashTargetCamera != null && this.m_CrashTargetCamera.targetTexture == this.m_Texture)
		{
			this.m_CrashTargetCamera.targetTexture = null;
		}
	}

	// Token: 0x06001EA8 RID: 7848 RVA: 0x000A61A9 File Offset: 0x000A45A9
	private void SetProgressRate(float rate)
	{
		this.m_PixelCrashMaterial.SetFloat("_ProgressRate", rate);
	}

	// Token: 0x06001EA9 RID: 7849 RVA: 0x000A61BC File Offset: 0x000A45BC
	public void SetGravity()
	{
		if (this.m_Param.m_Gravity != this.m_History.m_Gravity)
		{
			this.m_PixelCrashMaterial.SetVector("_Gravity", this.m_Param.m_Gravity);
			this.m_History.m_Gravity = this.m_Param.m_Gravity;
		}
	}

	// Token: 0x06001EAA RID: 7850 RVA: 0x000A621C File Offset: 0x000A461C
	public void SetNormalGravity()
	{
		if (this.m_Param.m_NormalGravity != this.m_History.m_NormalGravity)
		{
			this.m_PixelCrashMaterial.SetVector("_NormalGravity", this.m_Param.m_NormalGravity);
			this.m_History.m_NormalGravity = this.m_Param.m_NormalGravity;
		}
	}

	// Token: 0x06001EAB RID: 7851 RVA: 0x000A627C File Offset: 0x000A467C
	public void SetFirstCommonVelocity()
	{
		if (this.m_Param.m_FirstCommonVelocity != this.m_History.m_FirstCommonVelocity)
		{
			this.m_PixelCrashMaterial.SetVector("_FirstCommonVelocity", this.m_Param.m_FirstCommonVelocity);
			this.m_History.m_FirstCommonVelocity = this.m_Param.m_FirstCommonVelocity;
		}
	}

	// Token: 0x06001EAC RID: 7852 RVA: 0x000A62DC File Offset: 0x000A46DC
	public void SetFirstNormalVelocity()
	{
		if (this.m_Param.m_FirstNormalVelocity != this.m_History.m_FirstNormalVelocity)
		{
			this.m_PixelCrashMaterial.SetVector("_FirstNormalVelocity", this.m_Param.m_FirstNormalVelocity);
			this.m_History.m_FirstNormalVelocity = this.m_Param.m_FirstNormalVelocity;
		}
	}

	// Token: 0x06001EAD RID: 7853 RVA: 0x000A633C File Offset: 0x000A473C
	public void SetType()
	{
		if (this.m_Param.m_Type != this.m_History.m_Type)
		{
			this.m_PixelCrashMaterial.SetFloat("_NormalShape", (float)this.m_Param.m_Type);
			PixelCrashWrapper.eType type = this.m_Param.m_Type;
			if (type != PixelCrashWrapper.eType.Line)
			{
				if (type == PixelCrashWrapper.eType.Circle)
				{
					this.m_PixelCrashMaterial.DisableKeyword("_NORMALSHAPE_RAY");
					this.m_PixelCrashMaterial.EnableKeyword("_NORMALSHAPE_CIRCLE");
				}
			}
			else
			{
				this.m_PixelCrashMaterial.EnableKeyword("_NORMALSHAPE_RAY");
				this.m_PixelCrashMaterial.DisableKeyword("_NORMALSHAPE_CIRCLE");
			}
			this.m_History.m_Type = this.m_Param.m_Type;
		}
	}

	// Token: 0x06001EAE RID: 7854 RVA: 0x000A6400 File Offset: 0x000A4800
	public void SetShapeArgment()
	{
		if (this.m_Param.m_ShapeArgment != this.m_History.m_ShapeArgment)
		{
			this.m_PixelCrashMaterial.SetVector("_ShapeArgment", this.m_Param.m_ShapeArgment);
			this.m_History.m_ShapeArgment = this.m_Param.m_ShapeArgment;
		}
	}

	// Token: 0x06001EAF RID: 7855 RVA: 0x000A6460 File Offset: 0x000A4860
	public void SetInvertMoveToNormalDir()
	{
		if (this.m_Param.m_InvertMoveToNormalDir != this.m_History.m_InvertMoveToNormalDir)
		{
			this.m_PixelCrashMaterial.SetFloat("_InvertMoveToNormalDir", (!this.m_Param.m_InvertMoveToNormalDir) ? 0f : 1f);
			if (this.m_Param.m_InvertMoveToNormalDir)
			{
				this.m_PixelCrashMaterial.DisableKeyword("_INVERTMOVETONORMALDIR_DISABLE");
				this.m_PixelCrashMaterial.EnableKeyword("_INVERTMOVETONORMALDIR_ENABLE");
			}
			else
			{
				this.m_PixelCrashMaterial.EnableKeyword("_INVERTMOVETONORMALDIR_DISABLE");
				this.m_PixelCrashMaterial.DisableKeyword("_INVERTMOVETONORMALDIR_ENABLE");
			}
			this.m_History.m_InvertMoveToNormalDir = this.m_Param.m_InvertMoveToNormalDir;
		}
	}

	// Token: 0x06001EB0 RID: 7856 RVA: 0x000A6524 File Offset: 0x000A4924
	public void SetMulColor()
	{
		if (this.m_Param.m_MulColor != this.m_History.m_MulColor)
		{
			this.m_PixelCrashMaterial.SetColor("_MulColor", this.m_Param.m_MulColor);
			this.m_History.m_MulColor = this.m_Param.m_MulColor;
		}
	}

	// Token: 0x06001EB1 RID: 7857 RVA: 0x000A6584 File Offset: 0x000A4984
	public void SetAddColor()
	{
		if (this.m_Param.m_AddColor != this.m_History.m_AddColor)
		{
			this.m_PixelCrashMaterial.SetColor("_AddColor", this.m_Param.m_AddColor);
			this.m_History.m_AddColor = this.m_Param.m_AddColor;
		}
	}

	// Token: 0x06001EB2 RID: 7858 RVA: 0x000A65E4 File Offset: 0x000A49E4
	public void SetBaseAddColor()
	{
		if (this.m_Param.m_BaseAddColor != this.m_History.m_BaseAddColor)
		{
			this.m_PixelCrashMaterial.SetColor("_BaseAddColor", this.m_Param.m_BaseAddColor);
			this.m_History.m_BaseAddColor = this.m_Param.m_BaseAddColor;
		}
	}

	// Token: 0x06001EB3 RID: 7859 RVA: 0x000A6644 File Offset: 0x000A4A44
	public void SetHDRFactor()
	{
		if (this.m_Param.m_HDRFactor != this.m_History.m_HDRFactor)
		{
			this.m_PixelCrashMaterial.SetFloat("_HDRFactor", this.m_Param.m_HDRFactor);
			this.m_History.m_HDRFactor = this.m_Param.m_HDRFactor;
		}
	}

	// Token: 0x06001EB4 RID: 7860 RVA: 0x000A66A0 File Offset: 0x000A4AA0
	public void SetChangeHDRFactor()
	{
		if (this.m_Param.m_ChangeHDRFactor != this.m_History.m_ChangeHDRFactor)
		{
			this.m_PixelCrashMaterial.SetFloat("_ChangeHDRFactor", this.m_Param.m_ChangeHDRFactor);
			this.m_History.m_ChangeHDRFactor = this.m_Param.m_ChangeHDRFactor;
		}
	}

	// Token: 0x06001EB5 RID: 7861 RVA: 0x000A66FC File Offset: 0x000A4AFC
	public void SetChangeThreshold()
	{
		if (this.m_Param.m_ChangeThreshold != this.m_History.m_ChangeThreshold)
		{
			this.m_PixelCrashMaterial.SetFloat("_ChangeThreshold", this.m_Param.m_ChangeThreshold);
			this.m_History.m_ChangeThreshold = this.m_Param.m_ChangeThreshold;
		}
	}

	// Token: 0x06001EB6 RID: 7862 RVA: 0x000A6758 File Offset: 0x000A4B58
	public void SetFadeThreshold()
	{
		if (this.m_Param.m_FadeThreshold != this.m_History.m_FadeThreshold)
		{
			this.m_PixelCrashMaterial.SetFloat("_FadeThreshold", this.m_Param.m_FadeThreshold);
			this.m_History.m_FadeThreshold = this.m_Param.m_FadeThreshold;
		}
	}

	// Token: 0x06001EB7 RID: 7863 RVA: 0x000A67B4 File Offset: 0x000A4BB4
	public void SetNoiseTextureParam(float addX = 0f, float addY = 0f)
	{
		if (this.m_Param.m_NoiseTextureParam != this.m_History.m_NoiseTextureParam)
		{
			this.m_PixelCrashMaterial.SetVector("_NoiseTextureParam", this.m_Param.m_NoiseTextureParam + new Vector4(addX, addX, 0f, 0f));
			this.m_History.m_NoiseTextureParam = this.m_Param.m_NoiseTextureParam;
		}
	}

	// Token: 0x06001EB8 RID: 7864 RVA: 0x000A6828 File Offset: 0x000A4C28
	private void Awake()
	{
		this.Setup();
	}

	// Token: 0x06001EB9 RID: 7865 RVA: 0x000A6834 File Offset: 0x000A4C34
	public bool Setup()
	{
		if (this.m_GO != null)
		{
			return false;
		}
		if (this.m_PixelCrashMaterialBase == null)
		{
			return false;
		}
		if (this.m_RenderCamera == null)
		{
			return false;
		}
		if (this.m_CrashTargetCamera == null)
		{
			return false;
		}
		this.m_GO = base.gameObject;
		this.m_History = new PixelCrashWrapper.Param();
		this.m_Rate = 0f;
		if (this.m_ConstSettings == null)
		{
			this.m_ConstSettings = new PixelCrashWrapper.ConstSettings();
		}
		this.CreateRenderTarget();
		this.CreateRenderObject();
		this.AttachTargetCamera();
		this.m_History.SetDirty();
		return true;
	}

	// Token: 0x06001EBA RID: 7866 RVA: 0x000A68E4 File Offset: 0x000A4CE4
	private void CreateRenderTarget()
	{
		if (this.m_Texture != null && this.m_Texture.IsCreated())
		{
			return;
		}
		this.m_Texture = new RenderTexture(Screen.width, Screen.height, this.m_ConstSettings.m_TextureDepth, RenderTextureFormat.ARGB32);
		this.m_Texture.antiAliasing = 1;
		this.m_Texture.useMipMap = false;
		this.m_Texture.autoGenerateMips = false;
		this.m_Texture.Create();
	}

	// Token: 0x06001EBB RID: 7867 RVA: 0x000A6964 File Offset: 0x000A4D64
	private void AttachTargetCamera()
	{
		if (this.m_CrashTargetCamera != null)
		{
			this.m_CrashTargetCamera.targetTexture = this.m_Texture;
		}
		MeigeShaderUtility.SetTexture(this.m_PixelCrashMaterial, this.m_Texture, eTextureType.eTextureType_Albedo, 0);
		if (this.m_DebugTexture != null)
		{
			MeigeShaderUtility.SetTexture(this.m_PixelCrashMaterial, this.m_DebugTexture, eTextureType.eTextureType_Albedo, 0);
		}
	}

	// Token: 0x06001EBC RID: 7868 RVA: 0x000A69CC File Offset: 0x000A4DCC
	private void CreateRenderObject()
	{
		this.m_PixelCrashMaterial = new Material(this.m_PixelCrashMaterialBase);
		this.m_Renderer = this.m_GO.AddComponent<MeshRenderer>();
		this.m_Renderer.material = this.m_PixelCrashMaterial;
		this.m_MeshFilter = this.m_GO.AddComponent<MeshFilter>();
		this.m_Mesh = this.m_MeshFilter.mesh;
		Vector3[] array = new Vector3[4];
		Vector2[] array2 = new Vector2[4];
		Color[] array3 = new Color[4];
		int[] array4 = new int[4];
		float num = (float)Screen.width / (float)Screen.height;
		float orthographicSize = this.m_RenderCamera.orthographicSize;
		array[0] = new Vector3(-num * orthographicSize, -orthographicSize, 0f);
		array[1] = new Vector3(num * orthographicSize, -orthographicSize, 0f);
		array[2] = new Vector3(-num * orthographicSize, orthographicSize, 0f);
		array[3] = new Vector3(num * orthographicSize, orthographicSize, 0f);
		array2[0] = new Vector2(0f, 0f);
		array2[1] = new Vector2(1f, 0f);
		array2[2] = new Vector2(0f, 1f);
		array2[3] = new Vector2(1f, 1f);
		array3[0] = new Color(1f, 1f, 1f, 1f);
		array3[1] = new Color(1f, 1f, 1f, 1f);
		array3[2] = new Color(1f, 1f, 1f, 1f);
		array3[3] = new Color(1f, 1f, 1f, 1f);
		array4[0] = 0;
		array4[1] = 1;
		array4[2] = 3;
		array4[3] = 2;
		this.m_Mesh.vertices = array;
		this.m_Mesh.uv = array2;
		this.m_Mesh.colors = array3;
		this.m_Mesh.SetIndices(array4, MeshTopology.Quads, 0);
		this.m_Renderer.enabled = this.m_isEnableRender;
	}

	// Token: 0x06001EBD RID: 7869 RVA: 0x000A6C34 File Offset: 0x000A5034
	public void SetMeshScale(float scale = 1f)
	{
		Vector3[] array = new Vector3[4];
		float num = (float)Screen.width / (float)Screen.height;
		float num2 = this.m_RenderCamera.orthographicSize * scale;
		array[0] = new Vector3(-num * num2, -num2, 0f);
		array[1] = new Vector3(num * num2, -num2, 0f);
		array[2] = new Vector3(-num * num2, num2, 0f);
		array[3] = new Vector3(num * num2, num2, 0f);
		this.m_Mesh.vertices = array;
	}

	// Token: 0x06001EBE RID: 7870 RVA: 0x000A6CDC File Offset: 0x000A50DC
	public void SetMeshColor(Color color)
	{
		Color[] colors = new Color[]
		{
			color,
			color,
			color,
			color
		};
		this.m_Mesh.colors = colors;
	}

	// Token: 0x06001EBF RID: 7871 RVA: 0x000A6D30 File Offset: 0x000A5130
	public bool Prepare(float startRate = 0f)
	{
		if (this.m_Texture == null)
		{
			return false;
		}
		if (!this.m_Texture.IsCreated())
		{
			return false;
		}
		this.AttachTargetCamera();
		if (startRate == 0f)
		{
			if (this.m_Param.m_LifeSpanSec >= 0f)
			{
				this.m_Rate = 0f;
			}
			else
			{
				this.m_Rate = 1f;
			}
		}
		else
		{
			this.m_Rate = startRate;
		}
		this.m_History.SetDirty();
		this.m_History.m_InvertMoveToNormalDir = !this.m_Param.m_InvertMoveToNormalDir;
		this.SetProgressRate(this.m_Rate);
		this.SetGravity();
		this.SetNormalGravity();
		this.SetFirstCommonVelocity();
		this.SetFirstNormalVelocity();
		this.SetType();
		this.SetShapeArgment();
		this.SetInvertMoveToNormalDir();
		this.SetMulColor();
		this.SetAddColor();
		this.SetBaseAddColor();
		this.SetHDRFactor();
		this.SetChangeHDRFactor();
		this.SetChangeThreshold();
		this.SetFadeThreshold();
		this.SetNoiseTextureParam(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
		return true;
	}

	// Token: 0x06001EC0 RID: 7872 RVA: 0x000A6E58 File Offset: 0x000A5258
	public bool Play(float startRate = 0f)
	{
		if (this.m_Texture == null)
		{
			return false;
		}
		if (!this.m_Texture.IsCreated())
		{
			return false;
		}
		if (this.m_isAlive)
		{
			return false;
		}
		this.AttachTargetCamera();
		this.m_isAlive = true;
		if (startRate == 0f)
		{
			if (this.m_Param.m_LifeSpanSec > 0f)
			{
				this.m_Rate = 0f;
			}
			else
			{
				this.m_Rate = 1f;
			}
		}
		else
		{
			this.m_Rate = startRate;
		}
		this.m_History.SetDirty();
		this.m_History.m_InvertMoveToNormalDir = !this.m_Param.m_InvertMoveToNormalDir;
		this.SetProgressRate(this.m_Rate);
		this.SetGravity();
		this.SetNormalGravity();
		this.SetFirstCommonVelocity();
		this.SetFirstNormalVelocity();
		this.SetType();
		this.SetShapeArgment();
		this.SetInvertMoveToNormalDir();
		this.SetMulColor();
		this.SetAddColor();
		this.SetBaseAddColor();
		this.SetHDRFactor();
		this.SetChangeHDRFactor();
		this.SetChangeThreshold();
		this.SetFadeThreshold();
		this.SetNoiseTextureParam(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f));
		return true;
	}

	// Token: 0x06001EC1 RID: 7873 RVA: 0x000A6F94 File Offset: 0x000A5394
	public void EnableRender(bool flg)
	{
		if (this.m_Renderer != null)
		{
			this.m_Renderer.enabled = flg;
		}
		this.m_isEnableRender = flg;
	}

	// Token: 0x06001EC2 RID: 7874 RVA: 0x000A6FBA File Offset: 0x000A53BA
	public bool IsEnableRender()
	{
		return this.m_isEnableRender;
	}

	// Token: 0x06001EC3 RID: 7875 RVA: 0x000A6FC2 File Offset: 0x000A53C2
	public bool Kill()
	{
		if (!this.m_isAlive)
		{
			return false;
		}
		this.m_isAlive = false;
		this.SetProgressRate(this.m_Rate);
		return true;
	}

	// Token: 0x06001EC4 RID: 7876 RVA: 0x000A6FE8 File Offset: 0x000A53E8
	private void Update()
	{
		if (!this.m_isAlive)
		{
			if (this.m_DebugStartFlg)
			{
				this.Play(0f);
				this.m_DebugStartFlg = false;
			}
			return;
		}
		this.m_Rate += Time.deltaTime / this.m_Param.m_LifeSpanSec;
		if ((this.m_Param.m_LifeSpanSec > 0f && this.m_Rate > 1f) || (this.m_Param.m_LifeSpanSec < 0f && this.m_Rate < 0f))
		{
			this.m_Rate = Mathf.Clamp01(this.m_Rate);
			this.Kill();
			return;
		}
		float num = this.m_Rate;
		switch (this.m_Param.m_RateMove)
		{
		case PixelCrashWrapper.eMove.Slowdown_x2:
			num = 1f - num;
			num *= num;
			num *= num;
			num = 1f - num;
			break;
		case PixelCrashWrapper.eMove.Slowdown:
			num = 1f - num;
			num *= num;
			num = 1f - num;
			break;
		case PixelCrashWrapper.eMove.Acceleration:
			num *= num;
			break;
		case PixelCrashWrapper.eMove.Acceleration_x2:
			num *= num;
			num *= num;
			break;
		}
		this.SetProgressRate(num);
	}

	// Token: 0x04002561 RID: 9569
	public PixelCrashWrapper.ConstSettings m_ConstSettings;

	// Token: 0x04002562 RID: 9570
	public Camera m_CrashTargetCamera;

	// Token: 0x04002563 RID: 9571
	public Camera m_RenderCamera;

	// Token: 0x04002564 RID: 9572
	public Material m_PixelCrashMaterialBase;

	// Token: 0x04002565 RID: 9573
	public Material m_PixelCrashMaterial;

	// Token: 0x04002566 RID: 9574
	public PixelCrashWrapper.Param m_Param;

	// Token: 0x04002567 RID: 9575
	private PixelCrashWrapper.Param m_History;

	// Token: 0x04002568 RID: 9576
	private RenderTexture m_Texture;

	// Token: 0x04002569 RID: 9577
	private Renderer m_Renderer;

	// Token: 0x0400256A RID: 9578
	private MeshFilter m_MeshFilter;

	// Token: 0x0400256B RID: 9579
	private Mesh m_Mesh;

	// Token: 0x0400256C RID: 9580
	private GameObject m_GO;

	// Token: 0x0400256D RID: 9581
	[SerializeField]
	private bool m_DebugStartFlg;

	// Token: 0x0400256E RID: 9582
	[SerializeField]
	private Texture m_DebugTexture;

	// Token: 0x0400256F RID: 9583
	private float m_Rate;

	// Token: 0x04002570 RID: 9584
	private bool m_isAlive;

	// Token: 0x04002571 RID: 9585
	private bool m_isEnableRender = true;

	// Token: 0x02000627 RID: 1575
	public enum eType
	{
		// Token: 0x04002573 RID: 9587
		Invalid = -1,
		// Token: 0x04002574 RID: 9588
		None,
		// Token: 0x04002575 RID: 9589
		Line,
		// Token: 0x04002576 RID: 9590
		Circle
	}

	// Token: 0x02000628 RID: 1576
	public enum eMove
	{
		// Token: 0x04002578 RID: 9592
		Slowdown_x2,
		// Token: 0x04002579 RID: 9593
		Slowdown,
		// Token: 0x0400257A RID: 9594
		Standard,
		// Token: 0x0400257B RID: 9595
		Acceleration,
		// Token: 0x0400257C RID: 9596
		Acceleration_x2
	}

	// Token: 0x02000629 RID: 1577
	[Serializable]
	public class ConstSettings
	{
		// Token: 0x0400257D RID: 9597
		public int m_TextureWidth = 1920;

		// Token: 0x0400257E RID: 9598
		public int m_TextureHeight = 1080;

		// Token: 0x0400257F RID: 9599
		public int m_TextureDepth;

		// Token: 0x04002580 RID: 9600
		public RenderTextureFormat m_TextureFormat = RenderTextureFormat.DefaultHDR;
	}

	// Token: 0x0200062A RID: 1578
	[Serializable]
	public class Param
	{
		// Token: 0x06001EC6 RID: 7878 RVA: 0x000A7150 File Offset: 0x000A5550
		public Param()
		{
			this.SetDefault();
		}

		// Token: 0x06001EC7 RID: 7879 RVA: 0x000A7160 File Offset: 0x000A5560
		public void SetDefault()
		{
			this.m_Type = PixelCrashWrapper.eType.Line;
			this.m_RateMove = PixelCrashWrapper.eMove.Standard;
			this.m_LifeSpanSec = 5f;
			this.m_Gravity = new Vector4(0f, 0f, 0f, 0f);
			this.m_NormalGravity = new Vector4(0f, 0f, 0f, 0f);
			this.m_FirstCommonVelocity = new Vector4(0f, 0f, 0f, 0f);
			this.m_FirstNormalVelocity = new Vector4(-0.5f, 0.4f, 0.5f, 0.8f);
			this.m_ShapeArgment = new Vector4(0.5f, 0f, 0f, 1f);
			this.m_InvertMoveToNormalDir = true;
			this.m_MulColor = new Color(1f, 1f, 1f, 1f);
			this.m_AddColor = new Color(1f, 1f, 1f, 1f);
			this.m_BaseAddColor = new Color(0f, 0f, 0f, 1f);
			this.m_HDRFactor = 1f;
			this.m_ChangeHDRFactor = 0.3f;
			this.m_ChangeThreshold = 0.1f;
			this.m_FadeThreshold = 0.2f;
			this.m_NoiseTextureParam = new Vector4(0f, 0f, 3.2785f, 3.2785f);
		}

		// Token: 0x06001EC8 RID: 7880 RVA: 0x000A72D0 File Offset: 0x000A56D0
		public void SetDirty()
		{
			this.m_Type = PixelCrashWrapper.eType.Invalid;
			this.m_RateMove = PixelCrashWrapper.eMove.Standard;
			this.m_LifeSpanSec = 0f;
			this.m_Gravity = new Vector4(-10000f, 0f, 0f, 0f);
			this.m_NormalGravity = new Vector4(-10000f, 0f, 0f, 0f);
			this.m_FirstCommonVelocity = new Vector4(-10000f, 0f, 0f, 0f);
			this.m_FirstNormalVelocity = new Vector4(-10000f, 0f, 0f, 0f);
			this.m_ShapeArgment = new Vector4(-10000f, 0f, 0f, 1f);
			this.m_InvertMoveToNormalDir = false;
			this.m_MulColor = new Color(10000f, 1f, 1f, 1f);
			this.m_AddColor = new Color(10000f, 1f, 1f, 1f);
			this.m_BaseAddColor = new Color(10000f, 0f, 0f, 1f);
			this.m_HDRFactor = -1f;
			this.m_ChangeHDRFactor = -1f;
			this.m_ChangeThreshold = -1f;
			this.m_FadeThreshold = -1f;
			this.m_NoiseTextureParam = new Vector4(10000f, 0f, 0f, 0f);
		}

		// Token: 0x04002581 RID: 9601
		public PixelCrashWrapper.eType m_Type;

		// Token: 0x04002582 RID: 9602
		public PixelCrashWrapper.eMove m_RateMove;

		// Token: 0x04002583 RID: 9603
		public float m_LifeSpanSec;

		// Token: 0x04002584 RID: 9604
		public Vector4 m_Gravity;

		// Token: 0x04002585 RID: 9605
		public Vector4 m_NormalGravity;

		// Token: 0x04002586 RID: 9606
		public Vector4 m_FirstCommonVelocity;

		// Token: 0x04002587 RID: 9607
		public Vector4 m_FirstNormalVelocity;

		// Token: 0x04002588 RID: 9608
		public Vector4 m_ShapeArgment;

		// Token: 0x04002589 RID: 9609
		public bool m_InvertMoveToNormalDir;

		// Token: 0x0400258A RID: 9610
		public Color m_MulColor;

		// Token: 0x0400258B RID: 9611
		public Color m_AddColor;

		// Token: 0x0400258C RID: 9612
		public Color m_BaseAddColor;

		// Token: 0x0400258D RID: 9613
		public float m_HDRFactor;

		// Token: 0x0400258E RID: 9614
		public float m_ChangeHDRFactor;

		// Token: 0x0400258F RID: 9615
		public float m_ChangeThreshold;

		// Token: 0x04002590 RID: 9616
		public float m_FadeThreshold;

		// Token: 0x04002591 RID: 9617
		public Vector4 m_NoiseTextureParam;
	}
}
