﻿using System;

// Token: 0x02000069 RID: 105
public enum eADVFace
{
	// Token: 0x04000198 RID: 408
	Default,
	// Token: 0x04000199 RID: 409
	Joy,
	// Token: 0x0400019A RID: 410
	Angry,
	// Token: 0x0400019B RID: 411
	Sorrow,
	// Token: 0x0400019C RID: 412
	Happy,
	// Token: 0x0400019D RID: 413
	Shy,
	// Token: 0x0400019E RID: 414
	Surprise,
	// Token: 0x0400019F RID: 415
	Unique1,
	// Token: 0x040001A0 RID: 416
	Unique2,
	// Token: 0x040001A1 RID: 417
	Unique3,
	// Token: 0x040001A2 RID: 418
	Num
}
