﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C41 RID: 3137
	public class Resumeget : CommonResponse
	{
		// Token: 0x0600403B RID: 16443 RVA: 0x0013E4B0 File Offset: 0x0013C8B0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.stamina.ToString();
			str += this.staminaMax.ToString();
			str += this.recastTime.ToString();
			return str + this.recastTimeMax.ToString();
		}

		// Token: 0x04004624 RID: 17956
		public long stamina;

		// Token: 0x04004625 RID: 17957
		public long staminaMax;

		// Token: 0x04004626 RID: 17958
		public int recastTime;

		// Token: 0x04004627 RID: 17959
		public int recastTimeMax;
	}
}
