﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C4A RID: 3146
	public class Weaponupgrade : CommonResponse
	{
		// Token: 0x0600404D RID: 16461 RVA: 0x0013E75C File Offset: 0x0013CB5C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedWeapon == null) ? string.Empty : this.managedWeapon.ToString());
			str += this.gold.ToString();
			return str + this.successType.ToString();
		}

		// Token: 0x04004630 RID: 17968
		public PlayerItemSummary[] itemSummary;

		// Token: 0x04004631 RID: 17969
		public PlayerWeapon managedWeapon;

		// Token: 0x04004632 RID: 17970
		public long gold;

		// Token: 0x04004633 RID: 17971
		public int successType;
	}
}
