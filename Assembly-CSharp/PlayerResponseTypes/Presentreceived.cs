﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C60 RID: 3168
	public class Presentreceived : CommonResponse
	{
		// Token: 0x06004079 RID: 16505 RVA: 0x0013EF88 File Offset: 0x0013D388
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.received == null) ? string.Empty : this.received.ToString());
		}

		// Token: 0x04004658 RID: 18008
		public PlayerPresent[] received;
	}
}
