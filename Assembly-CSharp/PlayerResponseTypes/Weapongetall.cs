﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C48 RID: 3144
	public class Weapongetall : CommonResponse
	{
		// Token: 0x06004049 RID: 16457 RVA: 0x0013E684 File Offset: 0x0013CA84
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
		}

		// Token: 0x0400462C RID: 17964
		public PlayerWeapon[] managedWeapons;
	}
}
