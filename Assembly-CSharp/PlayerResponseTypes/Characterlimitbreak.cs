﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C4F RID: 3151
	public class Characterlimitbreak : CommonResponse
	{
		// Token: 0x06004057 RID: 16471 RVA: 0x0013E90C File Offset: 0x0013CD0C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacter == null) ? string.Empty : this.managedCharacter.ToString());
			return str + this.gold.ToString();
		}

		// Token: 0x04004638 RID: 17976
		public PlayerItemSummary[] itemSummary;

		// Token: 0x04004639 RID: 17977
		public PlayerCharacter managedCharacter;

		// Token: 0x0400463A RID: 17978
		public long gold;
	}
}
