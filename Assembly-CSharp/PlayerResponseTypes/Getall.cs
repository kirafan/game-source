﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C3F RID: 3135
	public class Getall : CommonResponse
	{
		// Token: 0x06004037 RID: 16439 RVA: 0x0013E0F8 File Offset: 0x0013C4F8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			str += ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
			str += ((this.managedMasterOrbs == null) ? string.Empty : this.managedMasterOrbs.ToString());
			str += ((this.managedBattleParties == null) ? string.Empty : this.managedBattleParties.ToString());
			str += ((this.managedFieldPartyMembers == null) ? string.Empty : this.managedFieldPartyMembers.ToString());
			str += ((this.managedTowns == null) ? string.Empty : this.managedTowns.ToString());
			str += ((this.managedTownFacilities == null) ? string.Empty : this.managedTownFacilities.ToString());
			str += ((this.managedRooms == null) ? string.Empty : this.managedRooms.ToString());
			str += ((this.managedRoomObjects == null) ? string.Empty : this.managedRoomObjects.ToString());
			str += ((this.supportCharacters == null) ? string.Empty : this.supportCharacters.ToString());
			str += ((this.advIds == null) ? string.Empty : this.advIds.ToString());
			str += ((this.tipIds == null) ? string.Empty : this.tipIds.ToString());
			str += this.stepCode.ToString();
			str += this.missionClearCount.ToString();
			str += this.friendProposedCount.ToString();
			str += this.presentCount.ToString();
			str += this.orderReceiveId.ToString();
			str += ((this.questData == null) ? string.Empty : this.questData.ToString());
			str += this.flagPush.ToString();
			str += this.flagStamina.ToString();
			return str + this.flagUi.ToString();
		}

		// Token: 0x04004609 RID: 17929
		public Player player;

		// Token: 0x0400460A RID: 17930
		public PlayerCharacter[] managedCharacters;

		// Token: 0x0400460B RID: 17931
		public PlayerNamedType[] managedNamedTypes;

		// Token: 0x0400460C RID: 17932
		public PlayerItemSummary[] itemSummary;

		// Token: 0x0400460D RID: 17933
		public PlayerWeapon[] managedWeapons;

		// Token: 0x0400460E RID: 17934
		public PlayerMasterOrb[] managedMasterOrbs;

		// Token: 0x0400460F RID: 17935
		public PlayerBattleParty[] managedBattleParties;

		// Token: 0x04004610 RID: 17936
		public PlayerFieldPartyMember[] managedFieldPartyMembers;

		// Token: 0x04004611 RID: 17937
		public PlayerTown[] managedTowns;

		// Token: 0x04004612 RID: 17938
		public PlayerTownFacility[] managedTownFacilities;

		// Token: 0x04004613 RID: 17939
		public PlayerRoom[] managedRooms;

		// Token: 0x04004614 RID: 17940
		public PlayerRoomObject[] managedRoomObjects;

		// Token: 0x04004615 RID: 17941
		public PlayerSupport[] supportCharacters;

		// Token: 0x04004616 RID: 17942
		public int[] advIds;

		// Token: 0x04004617 RID: 17943
		public int[] tipIds;

		// Token: 0x04004618 RID: 17944
		public int stepCode;

		// Token: 0x04004619 RID: 17945
		public int missionClearCount;

		// Token: 0x0400461A RID: 17946
		public int friendProposedCount;

		// Token: 0x0400461B RID: 17947
		public int presentCount;

		// Token: 0x0400461C RID: 17948
		public long orderReceiveId;

		// Token: 0x0400461D RID: 17949
		public string questData;

		// Token: 0x0400461E RID: 17950
		public int flagPush;

		// Token: 0x0400461F RID: 17951
		public int flagStamina;

		// Token: 0x04004620 RID: 17952
		public int flagUi;
	}
}
