﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C4C RID: 3148
	public class Weaponlimitadd : CommonResponse
	{
		// Token: 0x06004051 RID: 16465 RVA: 0x0013E864 File Offset: 0x0013CC64
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004636 RID: 17974
		public Player player;
	}
}
