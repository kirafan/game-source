﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C45 RID: 3141
	public class Loginbonus : CommonResponse
	{
		// Token: 0x06004043 RID: 16451 RVA: 0x0013E590 File Offset: 0x0013C990
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.bonuses == null) ? string.Empty : this.bonuses.ToString());
		}

		// Token: 0x04004628 RID: 17960
		public Bonus[] bonuses;
	}
}
