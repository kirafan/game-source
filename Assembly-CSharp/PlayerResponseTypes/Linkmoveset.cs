﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C3D RID: 3133
	public class Linkmoveset : CommonResponse
	{
		// Token: 0x06004033 RID: 16435 RVA: 0x0013E07C File Offset: 0x0013C47C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.playerId.ToString();
			return str + ((this.accessToken == null) ? string.Empty : this.accessToken.ToString());
		}

		// Token: 0x04004607 RID: 17927
		public long playerId;

		// Token: 0x04004608 RID: 17928
		public string accessToken;
	}
}
