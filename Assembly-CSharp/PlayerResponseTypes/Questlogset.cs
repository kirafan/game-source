﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C5A RID: 3162
	public class Questlogset : CommonResponse
	{
		// Token: 0x0600406D RID: 16493 RVA: 0x0013ECE4 File Offset: 0x0013D0E4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			str += ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
			str += ((this.managedMasterOrb == null) ? string.Empty : this.managedMasterOrb.ToString());
			str += ((this.rewardFirst == null) ? string.Empty : this.rewardFirst.ToString());
			str += ((this.rewardGroup == null) ? string.Empty : this.rewardGroup.ToString());
			return str + ((this.rewardComp == null) ? string.Empty : this.rewardComp.ToString());
		}

		// Token: 0x0400464B RID: 17995
		public Player player;

		// Token: 0x0400464C RID: 17996
		public PlayerItemSummary[] itemSummary;

		// Token: 0x0400464D RID: 17997
		public PlayerCharacter[] managedCharacters;

		// Token: 0x0400464E RID: 17998
		public PlayerNamedType[] managedNamedTypes;

		// Token: 0x0400464F RID: 17999
		public PlayerMasterOrb managedMasterOrb;

		// Token: 0x04004650 RID: 18000
		public QuestReward rewardFirst;

		// Token: 0x04004651 RID: 18001
		public QuestReward rewardGroup;

		// Token: 0x04004652 RID: 18002
		public QuestReward rewardComp;
	}
}
