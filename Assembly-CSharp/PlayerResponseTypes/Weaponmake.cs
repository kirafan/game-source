﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C49 RID: 3145
	public class Weaponmake : CommonResponse
	{
		// Token: 0x0600404B RID: 16459 RVA: 0x0013E6C8 File Offset: 0x0013CAC8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			return str + ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
		}

		// Token: 0x0400462D RID: 17965
		public Player player;

		// Token: 0x0400462E RID: 17966
		public PlayerItemSummary[] itemSummary;

		// Token: 0x0400462F RID: 17967
		public PlayerWeapon[] managedWeapons;
	}
}
