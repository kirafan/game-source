﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C3C RID: 3132
	public class Moveset : CommonResponse
	{
		// Token: 0x06004031 RID: 16433 RVA: 0x0013E020 File Offset: 0x0013C420
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.playerId.ToString();
			return str + ((this.accessToken == null) ? string.Empty : this.accessToken.ToString());
		}

		// Token: 0x04004605 RID: 17925
		public long playerId;

		// Token: 0x04004606 RID: 17926
		public string accessToken;
	}
}
