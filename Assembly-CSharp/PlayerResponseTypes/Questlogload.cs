﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C5D RID: 3165
	public class Questlogload : CommonResponse
	{
		// Token: 0x06004073 RID: 16499 RVA: 0x0013EEA0 File Offset: 0x0013D2A0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.orderReceiveId.ToString();
			return str + ((this.questData == null) ? string.Empty : this.questData.ToString());
		}

		// Token: 0x04004654 RID: 18004
		public long orderReceiveId;

		// Token: 0x04004655 RID: 18005
		public string questData;
	}
}
