﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C50 RID: 3152
	public class Characterupgrade : CommonResponse
	{
		// Token: 0x06004059 RID: 16473 RVA: 0x0013E990 File Offset: 0x0013CD90
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacter == null) ? string.Empty : this.managedCharacter.ToString());
			str += this.successType.ToString();
			return str + this.gold.ToString();
		}

		// Token: 0x0400463B RID: 17979
		public PlayerItemSummary[] itemSummary;

		// Token: 0x0400463C RID: 17980
		public PlayerCharacter managedCharacter;

		// Token: 0x0400463D RID: 17981
		public int successType;

		// Token: 0x0400463E RID: 17982
		public long gold;
	}
}
