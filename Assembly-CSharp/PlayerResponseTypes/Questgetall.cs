﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C57 RID: 3159
	public class Questgetall : CommonResponse
	{
		// Token: 0x06004067 RID: 16487 RVA: 0x0013EB98 File Offset: 0x0013CF98
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.quests == null) ? string.Empty : this.quests.ToString());
			return str + ((this.eventQuests == null) ? string.Empty : this.eventQuests.ToString());
		}

		// Token: 0x04004644 RID: 17988
		public PlayerQuest[] quests;

		// Token: 0x04004645 RID: 17989
		public PlayerEventQuest[] eventQuests;
	}
}
