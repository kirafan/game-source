﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C3B RID: 3131
	public class Moveget : CommonResponse
	{
		// Token: 0x0600402F RID: 16431 RVA: 0x0013DFC4 File Offset: 0x0013C3C4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.moveCode == null) ? string.Empty : this.moveCode.ToString());
			return str + this.moveDeadline.ToString();
		}

		// Token: 0x04004603 RID: 17923
		public string moveCode;

		// Token: 0x04004604 RID: 17924
		public DateTime moveDeadline;
	}
}
