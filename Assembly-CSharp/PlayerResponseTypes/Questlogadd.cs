﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C58 RID: 3160
	public class Questlogadd : CommonResponse
	{
		// Token: 0x06004069 RID: 16489 RVA: 0x0013EC04 File Offset: 0x0013D004
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.orderReceiveId.ToString();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
		}

		// Token: 0x04004646 RID: 17990
		public long orderReceiveId;

		// Token: 0x04004647 RID: 17991
		public Player player;

		// Token: 0x04004648 RID: 17992
		public PlayerItemSummary[] itemSummary;
	}
}
