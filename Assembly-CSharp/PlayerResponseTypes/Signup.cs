﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C38 RID: 3128
	public class Signup : CommonResponse
	{
		// Token: 0x06004029 RID: 16425 RVA: 0x0013DF04 File Offset: 0x0013C304
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.playerId.ToString();
			return str + ((this.accessToken == null) ? string.Empty : this.accessToken.ToString());
		}

		// Token: 0x04004600 RID: 17920
		public long playerId;

		// Token: 0x04004601 RID: 17921
		public string accessToken;
	}
}
