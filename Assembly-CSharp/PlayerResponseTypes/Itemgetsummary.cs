﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C46 RID: 3142
	public class Itemgetsummary : CommonResponse
	{
		// Token: 0x06004045 RID: 16453 RVA: 0x0013E5D4 File Offset: 0x0013C9D4
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
		}

		// Token: 0x04004629 RID: 17961
		public PlayerItemSummary[] itemSummary;
	}
}
