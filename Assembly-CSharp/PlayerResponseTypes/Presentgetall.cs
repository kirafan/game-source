﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C5F RID: 3167
	public class Presentgetall : CommonResponse
	{
		// Token: 0x06004077 RID: 16503 RVA: 0x0013EF1C File Offset: 0x0013D31C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.presents == null) ? string.Empty : this.presents.ToString());
			return str + ((this.received == null) ? string.Empty : this.received.ToString());
		}

		// Token: 0x04004656 RID: 18006
		public PlayerPresent[] presents;

		// Token: 0x04004657 RID: 18007
		public PlayerPresent[] received;
	}
}
