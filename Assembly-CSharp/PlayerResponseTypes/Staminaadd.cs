﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C62 RID: 3170
	public class Staminaadd : CommonResponse
	{
		// Token: 0x0600407D RID: 16509 RVA: 0x0013F148 File Offset: 0x0013D548
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
		}

		// Token: 0x04004662 RID: 18018
		public Player player;

		// Token: 0x04004663 RID: 18019
		public PlayerItemSummary[] itemSummary;
	}
}
