﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C4D RID: 3149
	public class Charactergetall : CommonResponse
	{
		// Token: 0x06004053 RID: 16467 RVA: 0x0013E8A8 File Offset: 0x0013CCA8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
		}

		// Token: 0x04004637 RID: 17975
		public PlayerCharacter[] managedCharacters;
	}
}
