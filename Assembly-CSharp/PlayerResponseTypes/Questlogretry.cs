﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C59 RID: 3161
	public class Questlogretry : CommonResponse
	{
		// Token: 0x0600406B RID: 16491 RVA: 0x0013EC88 File Offset: 0x0013D088
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.orderReceiveId.ToString();
			return str + ((this.player == null) ? string.Empty : this.player.ToString());
		}

		// Token: 0x04004649 RID: 17993
		public long orderReceiveId;

		// Token: 0x0400464A RID: 17994
		public Player player;
	}
}
