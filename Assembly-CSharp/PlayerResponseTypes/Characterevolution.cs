﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C51 RID: 3153
	public class Characterevolution : CommonResponse
	{
		// Token: 0x0600405B RID: 16475 RVA: 0x0013EA2C File Offset: 0x0013CE2C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedCharacter == null) ? string.Empty : this.managedCharacter.ToString());
			return str + this.gold.ToString();
		}

		// Token: 0x0400463F RID: 17983
		public PlayerItemSummary[] itemSummary;

		// Token: 0x04004640 RID: 17984
		public PlayerCharacter managedCharacter;

		// Token: 0x04004641 RID: 17985
		public long gold;
	}
}
