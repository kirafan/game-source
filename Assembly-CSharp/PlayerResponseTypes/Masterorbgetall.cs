﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C55 RID: 3157
	public class Masterorbgetall : CommonResponse
	{
		// Token: 0x06004063 RID: 16483 RVA: 0x0013EB34 File Offset: 0x0013CF34
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedMasterOrbs == null) ? string.Empty : this.managedMasterOrbs.ToString());
		}

		// Token: 0x04004643 RID: 17987
		public PlayerMasterOrb[] managedMasterOrbs;
	}
}
