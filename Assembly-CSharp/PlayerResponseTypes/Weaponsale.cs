﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C4B RID: 3147
	public class Weaponsale : CommonResponse
	{
		// Token: 0x0600404F RID: 16463 RVA: 0x0013E7F8 File Offset: 0x0013CBF8
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
		}

		// Token: 0x04004634 RID: 17972
		public Player player;

		// Token: 0x04004635 RID: 17973
		public PlayerWeapon[] managedWeapons;
	}
}
