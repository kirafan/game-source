﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C40 RID: 3136
	public class Getbadgecount : CommonResponse
	{
		// Token: 0x06004039 RID: 16441 RVA: 0x0013E448 File Offset: 0x0013C848
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += this.missionClearCount.ToString();
			str += this.friendProposedCount.ToString();
			return str + this.presentCount.ToString();
		}

		// Token: 0x04004621 RID: 17953
		public int missionClearCount;

		// Token: 0x04004622 RID: 17954
		public int friendProposedCount;

		// Token: 0x04004623 RID: 17955
		public int presentCount;
	}
}
