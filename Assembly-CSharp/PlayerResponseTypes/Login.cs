﻿using System;
using CommonResponseTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C39 RID: 3129
	public class Login : CommonResponse
	{
		// Token: 0x0600402B RID: 16427 RVA: 0x0013DF60 File Offset: 0x0013C360
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.sessionId == null) ? string.Empty : this.sessionId.ToString());
		}

		// Token: 0x04004602 RID: 17922
		public string sessionId;
	}
}
