﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C54 RID: 3156
	public class Battlepartygetall : CommonResponse
	{
		// Token: 0x06004061 RID: 16481 RVA: 0x0013EAF0 File Offset: 0x0013CEF0
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.managedBattleParties == null) ? string.Empty : this.managedBattleParties.ToString());
		}

		// Token: 0x04004642 RID: 17986
		public PlayerBattleParty[] managedBattleParties;
	}
}
