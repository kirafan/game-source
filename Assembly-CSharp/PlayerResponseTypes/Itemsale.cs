﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C47 RID: 3143
	public class Itemsale : CommonResponse
	{
		// Token: 0x06004047 RID: 16455 RVA: 0x0013E618 File Offset: 0x0013CA18
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			return str + ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
		}

		// Token: 0x0400462A RID: 17962
		public Player player;

		// Token: 0x0400462B RID: 17963
		public PlayerItemSummary[] itemSummary;
	}
}
