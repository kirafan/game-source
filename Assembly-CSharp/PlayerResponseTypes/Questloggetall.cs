﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C5B RID: 3163
	public class Questloggetall : CommonResponse
	{
		// Token: 0x0600406F RID: 16495 RVA: 0x0013EE3C File Offset: 0x0013D23C
		public override string ToMessage()
		{
			string str = base.ToMessage();
			return str + ((this.playerQuestLogs == null) ? string.Empty : this.playerQuestLogs.ToString());
		}

		// Token: 0x04004653 RID: 18003
		public PlayerQuestLog[] playerQuestLogs;
	}
}
