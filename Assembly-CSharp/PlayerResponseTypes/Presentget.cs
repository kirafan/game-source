﻿using System;
using CommonResponseTypes;
using WWWTypes;

namespace PlayerResponseTypes
{
	// Token: 0x02000C61 RID: 3169
	public class Presentget : CommonResponse
	{
		// Token: 0x0600407B RID: 16507 RVA: 0x0013EFCC File Offset: 0x0013D3CC
		public override string ToMessage()
		{
			string str = base.ToMessage();
			str += ((this.received == null) ? string.Empty : this.received.ToString());
			str += ((this.player == null) ? string.Empty : this.player.ToString());
			str += ((this.managedCharacters == null) ? string.Empty : this.managedCharacters.ToString());
			str += ((this.managedNamedTypes == null) ? string.Empty : this.managedNamedTypes.ToString());
			str += ((this.itemSummary == null) ? string.Empty : this.itemSummary.ToString());
			str += ((this.managedWeapons == null) ? string.Empty : this.managedWeapons.ToString());
			str += ((this.managedMasterOrbs == null) ? string.Empty : this.managedMasterOrbs.ToString());
			str += ((this.managedTownFacilities == null) ? string.Empty : this.managedTownFacilities.ToString());
			return str + ((this.managedRoomObjects == null) ? string.Empty : this.managedRoomObjects.ToString());
		}

		// Token: 0x04004659 RID: 18009
		public PlayerPresent[] received;

		// Token: 0x0400465A RID: 18010
		public Player player;

		// Token: 0x0400465B RID: 18011
		public PlayerCharacter[] managedCharacters;

		// Token: 0x0400465C RID: 18012
		public PlayerNamedType[] managedNamedTypes;

		// Token: 0x0400465D RID: 18013
		public PlayerItemSummary[] itemSummary;

		// Token: 0x0400465E RID: 18014
		public PlayerWeapon[] managedWeapons;

		// Token: 0x0400465F RID: 18015
		public PlayerMasterOrb[] managedMasterOrbs;

		// Token: 0x04004660 RID: 18016
		public PlayerTownFacility[] managedTownFacilities;

		// Token: 0x04004661 RID: 18017
		public PlayerRoomObject[] managedRoomObjects;
	}
}
