﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;

// Token: 0x02000FB2 RID: 4018
public class UnicodeCSVReader
{
	// Token: 0x0600537C RID: 21372 RVA: 0x00175C97 File Offset: 0x00174097
	public UnicodeCSVReader(string path)
	{
		this.parse(path);
	}

	// Token: 0x0600537D RID: 21373 RVA: 0x00175CB8 File Offset: 0x001740B8
	public void parse(string path)
	{
		this.m_path = path;
		TextAsset textAsset = (TextAsset)Resources.Load(path, typeof(TextAsset));
		if (textAsset == null)
		{
			Debug.Log("[" + this.m_path + "] is not found");
			return;
		}
		StringReader stringReader = new StringReader(textAsset.text);
		if (stringReader == null)
		{
			Debug.Log("[" + this.m_path + "] is not readable");
			return;
		}
		this.m_strings = new ArrayList();
		string text;
		while ((text = stringReader.ReadLine()) != null)
		{
			string[] value = text.Split(this.m_Separator);
			this.m_strings.Add(value);
		}
	}

	// Token: 0x0600537E RID: 21374 RVA: 0x00175D6D File Offset: 0x0017416D
	public bool IsAvailable()
	{
		return this.m_strings != null;
	}

	// Token: 0x0600537F RID: 21375 RVA: 0x00175D7C File Offset: 0x0017417C
	public string get(int row, int col)
	{
		if (this.getRowSize() <= row)
		{
			return string.Empty;
		}
		string[] array = (string[])this.m_strings[row];
		if (array.Length <= col)
		{
			return string.Empty;
		}
		return array[col];
	}

	// Token: 0x06005380 RID: 21376 RVA: 0x00175DBF File Offset: 0x001741BF
	public int getRowSize()
	{
		return this.m_strings.Count;
	}

	// Token: 0x06005381 RID: 21377 RVA: 0x00175DCC File Offset: 0x001741CC
	public int getColumnSize(int row)
	{
		if (this.getRowSize() <= row)
		{
			return 0;
		}
		string[] array = (string[])this.m_strings[row];
		return array.Length;
	}

	// Token: 0x06005382 RID: 21378 RVA: 0x00175DFC File Offset: 0x001741FC
	public int getInt(int row, int col)
	{
		string s = this.get(row, col);
		int result = 0;
		int.TryParse(s, out result);
		return result;
	}

	// Token: 0x06005383 RID: 21379 RVA: 0x00175E20 File Offset: 0x00174220
	public float getFloat(int row, int col)
	{
		string s = this.get(row, col);
		float result = 0f;
		float.TryParse(s, out result);
		return result;
	}

	// Token: 0x06005384 RID: 21380 RVA: 0x00175E48 File Offset: 0x00174248
	public double getDouble(int row, int col)
	{
		string s = this.get(row, col);
		double result = 0.0;
		double.TryParse(s, out result);
		return result;
	}

	// Token: 0x04005461 RID: 21601
	private const char TAB_SEPARATOR = '\t';

	// Token: 0x04005462 RID: 21602
	private string m_path;

	// Token: 0x04005463 RID: 21603
	private ArrayList m_strings;

	// Token: 0x04005464 RID: 21604
	private char[] m_Separator = new char[]
	{
		'\t'
	};
}
