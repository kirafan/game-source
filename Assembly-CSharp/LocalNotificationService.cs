﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using PreviewLabs;
using UnityEngine;

// Token: 0x0200065A RID: 1626
internal class LocalNotificationService : SingletonMonoBehaviour<LocalNotificationService>
{
	// Token: 0x060020DC RID: 8412 RVA: 0x000AF51D File Offset: 0x000AD91D
	protected new void Awake()
	{
		LocalNotificationService.m_notificationIds = new List<int>();
	}

	// Token: 0x060020DD RID: 8413 RVA: 0x000AF529 File Offset: 0x000AD929
	protected void Start()
	{
	}

	// Token: 0x060020DE RID: 8414 RVA: 0x000AF52B File Offset: 0x000AD92B
	public static void RegisterForNotifications()
	{
	}

	// Token: 0x060020DF RID: 8415 RVA: 0x000AF530 File Offset: 0x000AD930
	public static void SimpleNotification(int id, TimeSpan delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "large", Color color = default(Color))
	{
		LocalNotificationService.SendNotification(id, (long)((int)delay.TotalSeconds), title, message, smallIcon, largeIcon, color, true, true, true);
	}

	// Token: 0x060020E0 RID: 8416 RVA: 0x000AF55C File Offset: 0x000AD95C
	public static void SimpleNotification(int id, int delay, string title, string message, string smallIcon = "m_icon", string largeIcon = "large", Color color = default(Color))
	{
		LocalNotificationService.SendNotification(id, (long)delay, title, message, smallIcon, largeIcon, color, true, true, true);
	}

	// Token: 0x060020E1 RID: 8417 RVA: 0x000AF584 File Offset: 0x000AD984
	public static void SendNotification(int id, long delay, string title, string message, string smallIcon, string bigIcon = "large", Color32 bgColor = default(Color32), bool sound = true, bool vibrate = true, bool lights = true)
	{
		if (!SingletonMonoBehaviour<PushNotificationService>.Instance.isRegist)
		{
			return;
		}
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.UnityNotificationManager");
		if (androidJavaClass != null)
		{
			Debug.Log("NotificationPlutin Call.");
			androidJavaClass.CallStatic("SetNotification", new object[]
			{
				id,
				delay * 1000L,
				title,
				message,
				message,
				(!sound) ? 0 : 1,
				(!vibrate) ? 0 : 1,
				(!lights) ? 0 : 1,
				bigIcon,
				smallIcon,
				(int)bgColor.r * 65536 + (int)bgColor.g * 256 + (int)bgColor.b,
				"com.amazonaws.unity.CustomUnityPlayerActivity"
			});
		}
		if (LocalNotificationService.m_notificationIds != null && !LocalNotificationService.m_notificationIds.Contains(id))
		{
			LocalNotificationService.m_notificationIds.Add(id);
		}
		LocalNotificationService.SavePrefs();
	}

	// Token: 0x060020E2 RID: 8418 RVA: 0x000AF6A0 File Offset: 0x000ADAA0
	public static void CancelNotification(int id)
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.amazonaws.unity.UnityNotificationManager");
		if (androidJavaClass != null)
		{
			androidJavaClass.CallStatic("CancelNotification", new object[]
			{
				id
			});
		}
		if (LocalNotificationService.m_notificationIds != null && LocalNotificationService.m_notificationIds.Contains(id))
		{
			LocalNotificationService.m_notificationIds.Remove(id);
			LocalNotificationService.SavePrefs();
		}
	}

	// Token: 0x060020E3 RID: 8419 RVA: 0x000AF704 File Offset: 0x000ADB04
	public static void CancellAll()
	{
		LocalNotificationService.LoadPrefs();
		if (LocalNotificationService.m_notificationIds != null)
		{
			List<int> list = new List<int>(LocalNotificationService.m_notificationIds);
			foreach (int num in list)
			{
				Debug.Log("cancel id " + num);
				LocalNotificationService.CancelNotification(num);
			}
		}
	}

	// Token: 0x060020E4 RID: 8420 RVA: 0x000AF78C File Offset: 0x000ADB8C
	private void OnApplicationPause(bool isPause)
	{
		if (isPause)
		{
		}
	}

	// Token: 0x060020E5 RID: 8421 RVA: 0x000AF79C File Offset: 0x000ADB9C
	private static void SavePrefs()
	{
		string value = LocalNotificationService.Serialize<List<int>>(LocalNotificationService.m_notificationIds);
		if (!string.IsNullOrEmpty(value))
		{
			PreviewLabs.PlayerPrefs.SetString("LocalNotificationIds", value);
		}
	}

	// Token: 0x060020E6 RID: 8422 RVA: 0x000AF7CC File Offset: 0x000ADBCC
	private static void LoadPrefs()
	{
		string @string = PreviewLabs.PlayerPrefs.GetString("LocalNotificationIds", string.Empty);
		if (!string.IsNullOrEmpty(@string))
		{
			LocalNotificationService.m_notificationIds = LocalNotificationService.Deserialize<List<int>>(@string);
		}
		if (LocalNotificationService.m_notificationIds == null)
		{
			LocalNotificationService.m_notificationIds = new List<int>();
		}
	}

	// Token: 0x060020E7 RID: 8423 RVA: 0x000AF814 File Offset: 0x000ADC14
	private static string Serialize<T>(T obj)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		MemoryStream memoryStream = new MemoryStream();
		binaryFormatter.Serialize(memoryStream, obj);
		return Convert.ToBase64String(memoryStream.GetBuffer());
	}

	// Token: 0x060020E8 RID: 8424 RVA: 0x000AF848 File Offset: 0x000ADC48
	private static T Deserialize<T>(string str)
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		MemoryStream serializationStream = new MemoryStream(Convert.FromBase64String(str));
		return (T)((object)binaryFormatter.Deserialize(serializationStream));
	}

	// Token: 0x0400271F RID: 10015
	private const string fullClassName = "com.amazonaws.unity.UnityNotificationManager";

	// Token: 0x04002720 RID: 10016
	private const string mainActivityClassName = "com.amazonaws.unity.CustomUnityPlayerActivity";

	// Token: 0x04002721 RID: 10017
	private const string PREFS_KEY = "LocalNotificationIds";

	// Token: 0x04002722 RID: 10018
	private static List<int> m_notificationIds;
}
