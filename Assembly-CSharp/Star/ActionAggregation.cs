﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000080 RID: 128
	public class ActionAggregation : MonoBehaviour
	{
		// Token: 0x060003F2 RID: 1010 RVA: 0x00013CB2 File Offset: 0x000120B2
		public void Setup()
		{
			this.m_State = eActionAggregationState.Wait;
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x00013CBB File Offset: 0x000120BB
		private void Update()
		{
			this.UpdateChildren();
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00013CC4 File Offset: 0x000120C4
		protected void UpdateChildren()
		{
			if (this.m_State == eActionAggregationState.Play)
			{
				int num = (int)(((double)Time.realtimeSinceStartup - this.m_PlayStartTime) * (double)this.m_FrameParSecond);
				if (this.m_ChildActions != null)
				{
					for (int i = 0; i < this.m_ChildActions.Length; i++)
					{
						if (this.m_ChildActions[i] != null && this.m_ChildActions[i].GetPlayStartFrame() <= num)
						{
							this.m_ChildActions[i].PlayStart();
						}
					}
				}
				if (this.m_FinishFrame <= num)
				{
					this.m_State = eActionAggregationState.Finish;
				}
			}
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x00013D58 File Offset: 0x00012158
		public void PlayStart()
		{
			if (this.m_State == eActionAggregationState.Wait || this.m_State == eActionAggregationState.Finish)
			{
				this.m_State = eActionAggregationState.Play;
				this.m_PlayStartTime = (double)Time.realtimeSinceStartup;
				this.RecoveryChildFinishToWait();
				this.UpdateChildren();
			}
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x00013D90 File Offset: 0x00012190
		public void Skip()
		{
			if (this.m_ChildActions != null)
			{
				for (int i = 0; i < this.m_ChildActions.Length; i++)
				{
					if (this.m_ChildActions[i] != null)
					{
						bool flag = true;
						if (this.m_ChildActions[i].IsExecuteEvenIfSkip())
						{
							flag = false;
							if (this.m_ChildActions[i].GetState() == eActionAggregationState.Wait)
							{
								this.m_ChildActions[i].PlayStart();
							}
						}
						if (flag)
						{
							this.m_ChildActions[i].Skip();
						}
					}
				}
			}
			this.m_State = eActionAggregationState.Finish;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x00013E20 File Offset: 0x00012220
		public virtual void RecoveryChildFinishToWait()
		{
			if (this.m_ChildActions != null)
			{
				for (int i = 0; i < this.m_ChildActions.Length; i++)
				{
					if (this.m_ChildActions[i] != null)
					{
						this.m_ChildActions[i].RecoveryFinishToWait();
					}
				}
			}
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x00013E6C File Offset: 0x0001226C
		public virtual void ForceRecoveryChildFinishToWait()
		{
			if (this.m_ChildActions != null)
			{
				for (int i = 0; i < this.m_ChildActions.Length; i++)
				{
					if (this.m_ChildActions[i] != null)
					{
						this.m_ChildActions[i].ForceRecoveryFinishToWait();
					}
				}
			}
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x00013EB7 File Offset: 0x000122B7
		protected eActionAggregationState GetState()
		{
			return this.m_State;
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x00013EBF File Offset: 0x000122BF
		public bool IsFinished()
		{
			return this.m_State == eActionAggregationState.Finish;
		}

		// Token: 0x04000228 RID: 552
		[SerializeField]
		private int m_FrameParSecond = 30;

		// Token: 0x04000229 RID: 553
		[SerializeField]
		[Tooltip("再生終了扱いになるフレーム.現状子の再生状況に関わらず一意に時間で終了判定を取る.")]
		private int m_FinishFrame;

		// Token: 0x0400022A RID: 554
		[SerializeField]
		private ActionAggregationChildAction[] m_ChildActions;

		// Token: 0x0400022B RID: 555
		private eActionAggregationState m_State;

		// Token: 0x0400022C RID: 556
		private double m_PlayStartTime;
	}
}
