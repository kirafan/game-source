﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200011C RID: 284
	[Serializable]
	public class SkillActionEvent_WeaponThrow_Parabola : SkillActionEvent_Base
	{
		// Token: 0x04000732 RID: 1842
		public string m_CallbackKey;

		// Token: 0x04000733 RID: 1843
		public bool m_IsLeftWeapon;

		// Token: 0x04000734 RID: 1844
		public bool m_IsRotationLinkAnim;

		// Token: 0x04000735 RID: 1845
		public bool m_IsAutoDetachOnArrival;

		// Token: 0x04000736 RID: 1846
		public float m_ArrivalFrame;

		// Token: 0x04000737 RID: 1847
		public float m_GravityAccel;

		// Token: 0x04000738 RID: 1848
		public eSkillActionWeaponThrowPosType m_TargetPosType;

		// Token: 0x04000739 RID: 1849
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x0400073A RID: 1850
		public Vector2 m_TargetPosOffset;
	}
}
