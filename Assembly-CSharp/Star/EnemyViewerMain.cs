﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000280 RID: 640
	public class EnemyViewerMain : MonoBehaviour
	{
		// Token: 0x06000BEE RID: 3054 RVA: 0x000461A7 File Offset: 0x000445A7
		private void Start()
		{
			this.m_ResourceIDText.text = string.Empty;
			this.SetAnimInfoText(string.Empty);
			this.m_Step = EnemyViewerMain.eStep.First;
		}

		// Token: 0x06000BEF RID: 3055 RVA: 0x000461CC File Offset: 0x000445CC
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			switch (this.m_Step)
			{
			case EnemyViewerMain.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.ApplyResourceID(0);
					this.m_Step = EnemyViewerMain.eStep.EffectPrepare;
				}
				break;
			case EnemyViewerMain.eStep.EffectPrepare:
				if (this.m_LoadingEffectIDs != null)
				{
					EffectManager effectMng = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
					for (int i = 0; i < this.m_LoadingEffectIDs.Length; i++)
					{
						string effectID = this.m_LoadingEffectIDs[i];
						string text = effectMng.CheckPackEffect(effectID);
						if (string.IsNullOrEmpty(text))
						{
							effectMng.LoadSingleEffect(effectID, false);
						}
						else
						{
							effectMng.LoadPack(text);
						}
					}
				}
				this.m_Step = EnemyViewerMain.eStep.EffectPrepare_Wait;
				break;
			case EnemyViewerMain.eStep.EffectPrepare_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
				{
					this.m_EffectIndex = 0;
					this.ApplyEffect();
					this.m_Step = EnemyViewerMain.eStep.Main;
				}
				break;
			case EnemyViewerMain.eStep.Prepare:
			{
				CharacterParam srcCharaParam = new CharacterParam();
				this.m_CharaHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(srcCharaParam, null, null, eCharaResourceType.Enemy, this.m_ResourceID, CharacterDefine.eMode.Viewer, false, CharacterDefine.eFriendType.None, "CHARA_" + this.m_ResourceID, null);
				if (this.m_CharaHndl != null)
				{
					this.m_CharaHndl.Prepare();
					this.m_Step = EnemyViewerMain.eStep.Prepare_Wait;
					Debug.LogFormat("Prepare Start >>> ResourceID:{0}".Green(), new object[]
					{
						this.m_ResourceID
					});
				}
				else
				{
					this.m_Step = EnemyViewerMain.eStep.Main;
					Debug.LogErrorFormat("Instantiate() is failed. resourceID:{0}", new object[]
					{
						this.m_ResourceID
					});
				}
				break;
			}
			case EnemyViewerMain.eStep.Prepare_Wait:
				if (!this.m_CharaHndl.IsPreparing())
				{
					this.m_AnimIndex = 0;
					this.ApplyAnim(true);
					this.m_CharaHndl.CacheTransform.position = new Vector3(0f, 0f, 0f);
					this.m_Step = EnemyViewerMain.eStep.Main;
				}
				break;
			case EnemyViewerMain.eStep.Destroy_0:
				this.SetAnimInfoText(string.Empty);
				if (this.m_CharaHndl != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_CharaHndl);
					this.m_CharaHndl = null;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.UnloadAll(false);
				this.m_Step = EnemyViewerMain.eStep.Destroy_1;
				break;
			case EnemyViewerMain.eStep.Destroy_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.IsCompleteUnloadAll())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.m_Step = EnemyViewerMain.eStep.Prepare;
				}
				break;
			}
		}

		// Token: 0x06000BF0 RID: 3056 RVA: 0x00046488 File Offset: 0x00044888
		private void LateUpdate()
		{
			EnemyViewerMain.eStep step = this.m_Step;
			if (step == EnemyViewerMain.eStep.Main)
			{
				this.UpdateAnimInfo();
			}
		}

		// Token: 0x06000BF1 RID: 3057 RVA: 0x000464B4 File Offset: 0x000448B4
		private void ToggleCharaIndex(bool isRight)
		{
			EnemyResourceListDB enemyResourceListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EnemyResourceListDB;
			int num = -1;
			for (int i = 0; i < enemyResourceListDB.m_Params.Length; i++)
			{
				if (enemyResourceListDB.m_Params[i].m_ResourceID == this.m_ResourceID)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				return;
			}
			if (isRight)
			{
				num++;
				if (num >= enemyResourceListDB.m_Params.Length)
				{
					num = 0;
				}
			}
			else
			{
				num--;
				if (num < 0)
				{
					num = enemyResourceListDB.m_Params.Length - 1;
				}
			}
			this.ApplyResourceID(num);
		}

		// Token: 0x06000BF2 RID: 3058 RVA: 0x00046554 File Offset: 0x00044954
		private void ApplyResourceID(int paramIndex)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_ResourceID = dbMng.EnemyResourceListDB.m_Params[paramIndex].m_ResourceID;
			this.m_ResourceIDText.text = this.m_ResourceID.ToString();
		}

		// Token: 0x06000BF3 RID: 3059 RVA: 0x000465A4 File Offset: 0x000449A4
		public void OnDecideApplyCharaButton()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			if (!SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EnemyResourceListDB.IsExistParam(this.m_ResourceID))
			{
				Debug.LogErrorFormat("Error!!! >>> ResourceID:{0} is not exist.", new object[]
				{
					this.m_ResourceID
				});
				return;
			}
			this.m_Step = EnemyViewerMain.eStep.Destroy_0;
		}

		// Token: 0x06000BF4 RID: 3060 RVA: 0x00046604 File Offset: 0x00044A04
		public void OnValueChangedChara()
		{
			int resourceID = 0;
			if (int.TryParse(this.m_ResourceIDText.text, out resourceID))
			{
				this.m_ResourceID = resourceID;
				this.m_ResourceIDText.text = this.m_ResourceID.ToString();
			}
		}

		// Token: 0x06000BF5 RID: 3061 RVA: 0x0004664D File Offset: 0x00044A4D
		public void OnDecideChara_L()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleCharaIndex(false);
		}

		// Token: 0x06000BF6 RID: 3062 RVA: 0x00046663 File Offset: 0x00044A63
		public void OnDecideChara_R()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleCharaIndex(true);
		}

		// Token: 0x06000BF7 RID: 3063 RVA: 0x0004667C File Offset: 0x00044A7C
		private void ToggleLocatorIndex(bool isRight)
		{
			if (isRight)
			{
				this.m_LocatorIndex++;
				if (this.m_LocatorIndex >= CharacterDefine.eLocatorIndex.Num)
				{
					this.m_LocatorIndex = CharacterDefine.eLocatorIndex.Body;
				}
			}
			else
			{
				this.m_LocatorIndex--;
				if (this.m_LocatorIndex < CharacterDefine.eLocatorIndex.Body)
				{
					this.m_LocatorIndex = CharacterDefine.eLocatorIndex.OverHead;
				}
			}
			this.ApplyLocator();
		}

		// Token: 0x06000BF8 RID: 3064 RVA: 0x000466DC File Offset: 0x00044ADC
		private void ApplyLocator()
		{
			this.m_LocatorText.text = this.m_LocatorIndex.ToString();
		}

		// Token: 0x06000BF9 RID: 3065 RVA: 0x000466FA File Offset: 0x00044AFA
		public void OnDecideLocator_L()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleLocatorIndex(false);
		}

		// Token: 0x06000BFA RID: 3066 RVA: 0x00046710 File Offset: 0x00044B10
		public void OnDecideLocator_R()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleLocatorIndex(true);
		}

		// Token: 0x06000BFB RID: 3067 RVA: 0x00046728 File Offset: 0x00044B28
		private void ToggleEffectIndex(bool isRight)
		{
			if (this.m_LoadingEffectIDs == null || this.m_LoadingEffectIDs.Length <= 0)
			{
				return;
			}
			if (isRight)
			{
				this.m_EffectIndex++;
				if (this.m_EffectIndex >= this.m_LoadingEffectIDs.Length)
				{
					this.m_EffectIndex = 0;
				}
			}
			else
			{
				this.m_EffectIndex--;
				if (this.m_EffectIndex < 0)
				{
					this.m_EffectIndex = this.m_LoadingEffectIDs.Length - 1;
				}
			}
			this.ApplyEffect();
		}

		// Token: 0x06000BFC RID: 3068 RVA: 0x000467B4 File Offset: 0x00044BB4
		private void ApplyEffect()
		{
			if (this.m_LoadingEffectIDs == null || this.m_LoadingEffectIDs.Length <= 0)
			{
				return;
			}
			this.m_EffectIDText.text = this.m_LoadingEffectIDs[this.m_EffectIndex];
			this.m_EffectID = this.m_EffectIDText.text;
		}

		// Token: 0x06000BFD RID: 3069 RVA: 0x00046804 File Offset: 0x00044C04
		public void OnDecideEffect_L()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleEffectIndex(false);
		}

		// Token: 0x06000BFE RID: 3070 RVA: 0x0004681A File Offset: 0x00044C1A
		public void OnDecideEffect_R()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleEffectIndex(true);
		}

		// Token: 0x06000BFF RID: 3071 RVA: 0x00046830 File Offset: 0x00044C30
		public void OnClickEffectPlayButton()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			if (string.IsNullOrEmpty(this.m_EffectID))
			{
				return;
			}
			Vector3 position = Vector3.zero;
			if (this.m_CharaHndl != null)
			{
				position = this.m_CharaHndl.CharaAnim.GetLocatorPos(this.m_LocatorIndex, false);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.Play(this.m_EffectID, position, Quaternion.Euler(0f, 180f, 0f), Vector3.one, null, false, 100, 1f, false);
		}

		// Token: 0x06000C00 RID: 3072 RVA: 0x000468C4 File Offset: 0x00044CC4
		private void ToggleAnimIndex(bool isRight)
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			if (isRight)
			{
				this.m_AnimIndex++;
				if (this.m_AnimIndex >= this.m_CharaHndl.CharaResource.AnimKeyForViewer.Length)
				{
					this.m_AnimIndex = 0;
				}
			}
			else
			{
				this.m_AnimIndex--;
				if (this.m_AnimIndex < 0)
				{
					this.m_AnimIndex = this.m_CharaHndl.CharaResource.AnimKeyForViewer.Length - 1;
				}
			}
			this.ApplyAnim(true);
		}

		// Token: 0x06000C01 RID: 3073 RVA: 0x00046980 File Offset: 0x00044D80
		private void ApplyAnim(bool isPlay = true)
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			this.m_AnimKeyText.text = this.m_CharaHndl.CharaResource.AnimKeyForViewer[this.m_AnimIndex];
			this.m_AnimKey = this.m_AnimKeyText.text;
			if (isPlay)
			{
				this.m_CharaHndl.CharaAnim.PlayAnim(this.m_AnimKey, WrapMode.Loop, 0f, 0f);
				this.m_CharaHndl.CharaAnim.ChangeDir(CharacterDefine.eDir.R, false);
				Debug.LogFormat("Play:{0}", new object[]
				{
					this.m_AnimKey
				});
			}
		}

		// Token: 0x06000C02 RID: 3074 RVA: 0x00046A46 File Offset: 0x00044E46
		public void OnDecideAnim_L()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(false);
		}

		// Token: 0x06000C03 RID: 3075 RVA: 0x00046A5C File Offset: 0x00044E5C
		public void OnDecideAnim_R()
		{
			if (this.m_Step != EnemyViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(true);
		}

		// Token: 0x06000C04 RID: 3076 RVA: 0x00046A74 File Offset: 0x00044E74
		private void UpdateAnimInfo()
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			int playingFrame = this.m_CharaHndl.CharaAnim.GetPlayingFrame(this.m_AnimKey, 0);
			float playingSec = this.m_CharaHndl.CharaAnim.GetPlayingSec(this.m_AnimKey, 0);
			float maxSec = this.m_CharaHndl.CharaAnim.GetMaxSec(this.m_AnimKey, 0);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Frame : ");
			stringBuilder.Append(playingFrame);
			stringBuilder.Append("\n");
			stringBuilder.Append("Sec : ");
			stringBuilder.Append(playingSec.ToString("f2"));
			stringBuilder.Append("/");
			stringBuilder.Append(maxSec.ToString("f2"));
			stringBuilder.Append("\n");
			this.SetAnimInfoText(stringBuilder.ToString());
		}

		// Token: 0x06000C05 RID: 3077 RVA: 0x00046B79 File Offset: 0x00044F79
		private void SetAnimInfoText(string txt)
		{
			this.m_AnimInfoText.text = txt;
		}

		// Token: 0x06000C06 RID: 3078 RVA: 0x00046B87 File Offset: 0x00044F87
		public void OnToggleBG(bool value)
		{
			this.m_BG.enabled = this.m_ToggleBG.isOn;
		}

		// Token: 0x06000C07 RID: 3079 RVA: 0x00046B9F File Offset: 0x00044F9F
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * value;
		}

		// Token: 0x04001487 RID: 5255
		[SerializeField]
		private SpriteRenderer m_BG;

		// Token: 0x04001488 RID: 5256
		[SerializeField]
		private InputField m_ResourceIDText;

		// Token: 0x04001489 RID: 5257
		[SerializeField]
		private Text m_LocatorText;

		// Token: 0x0400148A RID: 5258
		[SerializeField]
		private Text m_EffectIDText;

		// Token: 0x0400148B RID: 5259
		[SerializeField]
		private Text m_AnimKeyText;

		// Token: 0x0400148C RID: 5260
		[SerializeField]
		private Text m_AnimInfoText;

		// Token: 0x0400148D RID: 5261
		[SerializeField]
		private Toggle m_ToggleBG;

		// Token: 0x0400148E RID: 5262
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x0400148F RID: 5263
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x04001490 RID: 5264
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x04001491 RID: 5265
		[SerializeField]
		private string[] m_LoadingEffectIDs;

		// Token: 0x04001492 RID: 5266
		private CharacterHandler m_CharaHndl;

		// Token: 0x04001493 RID: 5267
		private int m_ResourceID = -1;

		// Token: 0x04001494 RID: 5268
		private int m_EffectIndex;

		// Token: 0x04001495 RID: 5269
		private string m_EffectID = string.Empty;

		// Token: 0x04001496 RID: 5270
		private int m_AnimIndex;

		// Token: 0x04001497 RID: 5271
		private string m_AnimKey;

		// Token: 0x04001498 RID: 5272
		private CharacterDefine.eLocatorIndex m_LocatorIndex;

		// Token: 0x04001499 RID: 5273
		private EnemyViewerMain.eStep m_Step = EnemyViewerMain.eStep.None;

		// Token: 0x02000281 RID: 641
		private enum eStep
		{
			// Token: 0x0400149B RID: 5275
			None = -1,
			// Token: 0x0400149C RID: 5276
			First,
			// Token: 0x0400149D RID: 5277
			EffectPrepare,
			// Token: 0x0400149E RID: 5278
			EffectPrepare_Wait,
			// Token: 0x0400149F RID: 5279
			Prepare,
			// Token: 0x040014A0 RID: 5280
			Prepare_Wait,
			// Token: 0x040014A1 RID: 5281
			Main,
			// Token: 0x040014A2 RID: 5282
			Destroy_0,
			// Token: 0x040014A3 RID: 5283
			Destroy_1
		}
	}
}
