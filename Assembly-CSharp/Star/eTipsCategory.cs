﻿using System;

namespace Star
{
	// Token: 0x0200023F RID: 575
	public enum eTipsCategory
	{
		// Token: 0x0400130A RID: 4874
		None = -1,
		// Token: 0x0400130B RID: 4875
		Common,
		// Token: 0x0400130C RID: 4876
		Battle,
		// Token: 0x0400130D RID: 4877
		Town,
		// Token: 0x0400130E RID: 4878
		Room,
		// Token: 0x0400130F RID: 4879
		Num
	}
}
