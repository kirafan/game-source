﻿using System;

namespace Star
{
	// Token: 0x02000235 RID: 565
	public enum eAgeType
	{
		// Token: 0x04000F74 RID: 3956
		None,
		// Token: 0x04000F75 RID: 3957
		Age15,
		// Token: 0x04000F76 RID: 3958
		Age16_19,
		// Token: 0x04000F77 RID: 3959
		Age20
	}
}
