﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using MsgPack;
using UnityEngine;

namespace Star
{
	// Token: 0x0200063A RID: 1594
	public class AccessSaveData
	{
		// Token: 0x06001F72 RID: 8050 RVA: 0x000AACDE File Offset: 0x000A90DE
		protected AccessSaveData()
		{
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06001F73 RID: 8051 RVA: 0x000AACF9 File Offset: 0x000A90F9
		public static AccessSaveData Inst
		{
			get
			{
				return AccessSaveData.instance;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06001F74 RID: 8052 RVA: 0x000AAD00 File Offset: 0x000A9100
		// (set) Token: 0x06001F75 RID: 8053 RVA: 0x000AAD0D File Offset: 0x000A910D
		public string UUID
		{
			get
			{
				return this.m_SaveData.m_UUID;
			}
			set
			{
				this.m_SaveData.m_UUID = value;
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06001F76 RID: 8054 RVA: 0x000AAD1B File Offset: 0x000A911B
		// (set) Token: 0x06001F77 RID: 8055 RVA: 0x000AAD28 File Offset: 0x000A9128
		public string AccessToken
		{
			get
			{
				return this.m_SaveData.m_AccessToken;
			}
			set
			{
				this.m_SaveData.m_AccessToken = value;
			}
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06001F78 RID: 8056 RVA: 0x000AAD36 File Offset: 0x000A9136
		// (set) Token: 0x06001F79 RID: 8057 RVA: 0x000AAD43 File Offset: 0x000A9143
		public int ConfirmedVer
		{
			get
			{
				return this.m_SaveData.m_ConfirmedVer;
			}
			set
			{
				this.m_SaveData.m_ConfirmedVer = value;
			}
		}

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x06001F7A RID: 8058 RVA: 0x000AAD51 File Offset: 0x000A9151
		// (set) Token: 0x06001F7B RID: 8059 RVA: 0x000AAD5E File Offset: 0x000A915E
		public string MyCode
		{
			get
			{
				return this.m_SaveData.m_MyCode;
			}
			set
			{
				this.m_SaveData.m_MyCode = value;
			}
		}

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x06001F7C RID: 8060 RVA: 0x000AAD6C File Offset: 0x000A916C
		// (set) Token: 0x06001F7D RID: 8061 RVA: 0x000AAD74 File Offset: 0x000A9174
		public bool SaveDataAutoDelete { get; set; }

		// Token: 0x06001F7E RID: 8062 RVA: 0x000AAD7D File Offset: 0x000A917D
		public void Reset()
		{
			if (this.m_SaveData != null)
			{
				this.m_SaveData.Reset();
			}
		}

		// Token: 0x06001F7F RID: 8063 RVA: 0x000AAD98 File Offset: 0x000A9198
		public void Save()
		{
			ObjectPacker objectPacker = new ObjectPacker();
			byte[] src = objectPacker.Pack(this.m_SaveData);
			if (File.Exists(AccessSaveData.SaveFile))
			{
				File.Copy(AccessSaveData.SaveFile, AccessSaveData.SaveFileBak, true);
			}
			try
			{
				string s;
				byte[] array;
				AccessSaveData.EncryptAes(src, AccessSaveData.EncryptKey, AccessSaveData.EncryptPasswordCount, out s, out array);
				using (FileStream fileStream = new FileStream(AccessSaveData.SaveFile, FileMode.Create, FileAccess.Write))
				{
					using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
					{
						int num = UnityEngine.Random.Range(-2147483647, int.MaxValue);
						byte b = (byte)(num & 127);
						num = ((int)((long)num & (long)((ulong)-65281)) | (65280 & (int)((short)this.m_LatestVersion) << 8));
						binaryWriter.Write(num);
						byte[] bytes = Encoding.UTF8.GetBytes(s);
						for (int i = 0; i < bytes.Length; i++)
						{
							byte[] array2 = bytes;
							int num2 = i;
							array2[num2] += 96 + (byte)i;
						}
						binaryWriter.Write((byte)(bytes.Length + (int)b));
						binaryWriter.Write(bytes);
						binaryWriter.Write(array.Length);
						binaryWriter.Write(array);
					}
				}
			}
			catch (Exception ex)
			{
			}
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x000AAF04 File Offset: 0x000A9304
		public void Load()
		{
			this.Load(AccessSaveData.SaveFile);
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x000AAF14 File Offset: 0x000A9314
		public bool Load(string path)
		{
			if (!File.Exists(path))
			{
				return false;
			}
			string pw = null;
			byte[] array = null;
			bool flag = false;
			try
			{
				using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
				{
					using (BinaryReader binaryReader = new BinaryReader(fileStream))
					{
						int num = binaryReader.ReadInt32();
						byte b = (byte)(num & 127);
						int num2 = (num & 65280) >> 8;
						if (num2 != (int)this.m_LatestVersion)
						{
							flag = true;
						}
						else
						{
							int count = (int)(binaryReader.ReadByte() - b);
							array = binaryReader.ReadBytes(count);
							for (int i = 0; i < array.Length; i++)
							{
								byte[] array2 = array;
								int num3 = i;
								array2[num3] -= 96 + (byte)i;
							}
							pw = Encoding.UTF8.GetString(array);
							count = binaryReader.ReadInt32();
							array = binaryReader.ReadBytes(count);
						}
					}
					if (!flag)
					{
						ObjectPacker objectPacker = new ObjectPacker();
						byte[] buf;
						AccessSaveData.DecryptAes(array, AccessSaveData.EncryptKey, pw, out buf);
						this.m_SaveData = objectPacker.Unpack<AccessSaveData.SaveData>(buf);
					}
				}
			}
			catch
			{
				bool flag2 = false;
				if (AccessSaveData.SaveFileBak != path)
				{
					flag2 = this.Load(AccessSaveData.SaveFileBak);
				}
				if (!flag2)
				{
					return false;
				}
			}
			if (flag)
			{
				this.DeleteAllSaveData();
				this.SaveDataAutoDelete = true;
				return true;
			}
			this.AfterLoad();
			return true;
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x000AB0AC File Offset: 0x000A94AC
		public void DeleteAllSaveData()
		{
			this.DeleteSaveData(AccessSaveData.SaveFile);
			this.DeleteSaveData(AccessSaveData.SaveFileBak);
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x000AB0C4 File Offset: 0x000A94C4
		public void DeleteSaveData(string path)
		{
			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}

		// Token: 0x06001F84 RID: 8068 RVA: 0x000AB0D7 File Offset: 0x000A94D7
		private void AfterLoad()
		{
		}

		// Token: 0x06001F85 RID: 8069 RVA: 0x000AB0DC File Offset: 0x000A94DC
		private static void EncryptAes(byte[] src, string encryptKey, int pwSize, out string pw, out byte[] dst)
		{
			pw = AccessSaveData.CreatePassword(pwSize);
			dst = null;
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				byte[] bytes = Encoding.UTF8.GetBytes(encryptKey);
				byte[] bytes2 = Encoding.UTF8.GetBytes(pw);
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(bytes, bytes2))
				{
					using (MemoryStream memoryStream = new MemoryStream())
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
						{
							cryptoStream.Write(src, 0, src.Length);
							cryptoStream.FlushFinalBlock();
							dst = memoryStream.ToArray();
						}
					}
				}
			}
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x000AB1F0 File Offset: 0x000A95F0
		private static void DecryptAes(byte[] src, string encryptKey, string pw, out byte[] dst)
		{
			dst = new byte[src.Length];
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				byte[] bytes = Encoding.UTF8.GetBytes(encryptKey);
				byte[] bytes2 = Encoding.UTF8.GetBytes(pw);
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(bytes, bytes2))
				{
					using (MemoryStream memoryStream = new MemoryStream(src))
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read))
						{
							cryptoStream.Read(dst, 0, dst.Length);
						}
					}
				}
			}
		}

		// Token: 0x06001F87 RID: 8071 RVA: 0x000AB2F4 File Offset: 0x000A96F4
		private static string CreatePassword(int count)
		{
			StringBuilder stringBuilder = new StringBuilder(count);
			for (int i = count - 1; i >= 0; i--)
			{
				char value = AccessSaveData.PasswordChars[UnityEngine.Random.Range(0, AccessSaveData.PasswordChars.Length)];
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04002612 RID: 9746
		private static readonly AccessSaveData instance = new AccessSaveData();

		// Token: 0x04002613 RID: 9747
		private static readonly string EncryptKey = "7gyPmqc54dVNB3Te6pIpd2THj2y3hjOP";

		// Token: 0x04002614 RID: 9748
		private static readonly int EncryptPasswordCount = 16;

		// Token: 0x04002615 RID: 9749
		private static readonly string FilePrefix = string.Empty;

		// Token: 0x04002616 RID: 9750
		private static readonly string SaveFile = Application.persistentDataPath + "/" + AccessSaveData.FilePrefix + "a.d";

		// Token: 0x04002617 RID: 9751
		private static readonly string SaveFileBak = Application.persistentDataPath + "/" + AccessSaveData.FilePrefix + "a.d2";

		// Token: 0x04002618 RID: 9752
		private readonly AccessSaveData.SaveDataVersion m_LatestVersion = AccessSaveData.SaveDataVersion._171101;

		// Token: 0x04002619 RID: 9753
		private AccessSaveData.SaveData m_SaveData = new AccessSaveData.SaveData();

		// Token: 0x0400261B RID: 9755
		private static readonly string PasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		// Token: 0x0200063B RID: 1595
		public enum SaveDataVersion
		{
			// Token: 0x0400261D RID: 9757
			_170404 = 17,
			// Token: 0x0400261E RID: 9758
			_170713,
			// Token: 0x0400261F RID: 9759
			_171101,
			// Token: 0x04002620 RID: 9760
			_latest
		}

		// Token: 0x0200063C RID: 1596
		public enum Amount
		{
			// Token: 0x04002622 RID: 9762
			High,
			// Token: 0x04002623 RID: 9763
			Middle,
			// Token: 0x04002624 RID: 9764
			Low
		}

		// Token: 0x0200063D RID: 1597
		private class SaveData
		{
			// Token: 0x06001F89 RID: 8073 RVA: 0x000AB3C0 File Offset: 0x000A97C0
			public SaveData()
			{
				this.Reset();
			}

			// Token: 0x06001F8A RID: 8074 RVA: 0x000AB3CE File Offset: 0x000A97CE
			public void Reset()
			{
				this.m_ConfirmedVer = 0;
				this.m_UUID = null;
				this.m_AccessToken = null;
				this.m_MyCode = null;
			}

			// Token: 0x04002625 RID: 9765
			public int m_ConfirmedVer;

			// Token: 0x04002626 RID: 9766
			public string m_UUID;

			// Token: 0x04002627 RID: 9767
			public string m_AccessToken;

			// Token: 0x04002628 RID: 9768
			public string m_MyCode;
		}
	}
}
