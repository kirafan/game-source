﻿using System;

namespace Star
{
	// Token: 0x020005F7 RID: 1527
	public class RoomComModelChange : IRoomEventCommand
	{
		// Token: 0x06001E2A RID: 7722 RVA: 0x000A24AB File Offset: 0x000A08AB
		public RoomComModelChange()
		{
			this.m_Type = eRoomRequest.ChangeModel;
			this.m_Enable = true;
		}

		// Token: 0x06001E2B RID: 7723 RVA: 0x000A24C1 File Offset: 0x000A08C1
		public void SetUpChangeModel(IRoomObjectControll pbase, long fbasemanageid, int fchangeobj)
		{
			this.m_ObjID = fchangeobj;
			this.m_ManageID = fbasemanageid;
			this.m_Target = pbase;
		}

		// Token: 0x06001E2C RID: 7724 RVA: 0x000A24D8 File Offset: 0x000A08D8
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			bool result = false;
			int step = this.m_Step;
			if (step != 0)
			{
				if (step != 1)
				{
					if (step != 2)
					{
					}
				}
				else if (!this.m_NewModel.IsPreparing())
				{
					this.m_NewModel = null;
					result = true;
					this.m_Enable = false;
					this.m_Step++;
				}
			}
			else
			{
				this.m_NewModel = pbuild.InstantiateObject(RoomObjectListUtil.CategoryIDToAccessKey(this.m_Target.ObjCategory, this.m_ObjID), this.m_Target.GetDir(), this.m_Target.BlockData.PosX, this.m_Target.BlockData.PosY, this.m_Target.FloorID, this.m_ManageID);
				this.m_Step++;
				pbuild.RemoveObject(this.m_Target);
				this.m_Target = null;
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(pbuild.GetManageID());
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
				roomComBuildScript.PlaySend();
			}
			return result;
		}

		// Token: 0x040024AD RID: 9389
		public int m_ObjID;

		// Token: 0x040024AE RID: 9390
		public IRoomObjectControll m_Target;

		// Token: 0x040024AF RID: 9391
		public IRoomObjectControll m_NewModel;

		// Token: 0x040024B0 RID: 9392
		public int m_Step;

		// Token: 0x040024B1 RID: 9393
		public long m_ManageID;
	}
}
