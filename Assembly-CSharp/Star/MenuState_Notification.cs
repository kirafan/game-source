﻿using System;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x0200043D RID: 1085
	public class MenuState_Notification : MenuState
	{
		// Token: 0x060014D6 RID: 5334 RVA: 0x0006D9CE File Offset: 0x0006BDCE
		public MenuState_Notification(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014D7 RID: 5335 RVA: 0x0006D9E6 File Offset: 0x0006BDE6
		public override int GetStateID()
		{
			return 7;
		}

		// Token: 0x060014D8 RID: 5336 RVA: 0x0006D9E9 File Offset: 0x0006BDE9
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Notification.eStep.First;
		}

		// Token: 0x060014D9 RID: 5337 RVA: 0x0006D9F2 File Offset: 0x0006BDF2
		public override void OnStateExit()
		{
		}

		// Token: 0x060014DA RID: 5338 RVA: 0x0006D9F4 File Offset: 0x0006BDF4
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014DB RID: 5339 RVA: 0x0006DA00 File Offset: 0x0006BE00
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Notification.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Notification.eStep.LoadWait;
				break;
			case MenuState_Notification.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Notification.eStep.PlayIn;
				}
				break;
			case MenuState_Notification.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Notification.eStep.Main;
				}
				break;
			case MenuState_Notification.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Notification.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Notification.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Notification.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014DC RID: 5340 RVA: 0x0006DB20 File Offset: 0x0006BF20
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuNotificationUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickDecideButton += this.OnClickDecideButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014DD RID: 5341 RVA: 0x0006DBA0 File Offset: 0x0006BFA0
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.IsDirty())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.Request_SetPushNotification(new Action(this.OnResponse_SetPushNotification));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060014DE RID: 5342 RVA: 0x0006DBEE File Offset: 0x0006BFEE
		private void OnClickDecideButton()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.IsDirty())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.Request_SetPushNotification(new Action(this.OnResponse_SetPushNotification));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060014DF RID: 5343 RVA: 0x0006DC2A File Offset: 0x0006C02A
		private void OnResponse_SetPushNotification()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060014E0 RID: 5344 RVA: 0x0006DC32 File Offset: 0x0006C032
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BC2 RID: 7106
		private MenuState_Notification.eStep m_Step = MenuState_Notification.eStep.None;

		// Token: 0x04001BC3 RID: 7107
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuNotificationUI;

		// Token: 0x04001BC4 RID: 7108
		private MenuNotificationUI m_UI;

		// Token: 0x0200043E RID: 1086
		private enum eStep
		{
			// Token: 0x04001BC6 RID: 7110
			None = -1,
			// Token: 0x04001BC7 RID: 7111
			First,
			// Token: 0x04001BC8 RID: 7112
			LoadWait,
			// Token: 0x04001BC9 RID: 7113
			PlayIn,
			// Token: 0x04001BCA RID: 7114
			Main,
			// Token: 0x04001BCB RID: 7115
			UnloadChildSceneWait
		}
	}
}
