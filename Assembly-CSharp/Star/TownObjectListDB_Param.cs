﻿using System;

namespace Star
{
	// Token: 0x0200024F RID: 591
	[Serializable]
	public struct TownObjectListDB_Param
	{
		// Token: 0x0400135C RID: 4956
		public int m_ID;

		// Token: 0x0400135D RID: 4957
		public int m_Category;

		// Token: 0x0400135E RID: 4958
		public int m_Arg;

		// Token: 0x0400135F RID: 4959
		public int m_ResourceID;

		// Token: 0x04001360 RID: 4960
		public int m_TitleType;

		// Token: 0x04001361 RID: 4961
		public int m_ElementID;

		// Token: 0x04001362 RID: 4962
		public int m_ClassID;

		// Token: 0x04001363 RID: 4963
		public string m_ObjName;

		// Token: 0x04001364 RID: 4964
		public float m_MarkerPos;

		// Token: 0x04001365 RID: 4965
		public float m_BaseSize;

		// Token: 0x04001366 RID: 4966
		public float m_OffsetX;

		// Token: 0x04001367 RID: 4967
		public float m_OffsetY;

		// Token: 0x04001368 RID: 4968
		public float m_BuildTime;

		// Token: 0x04001369 RID: 4969
		public int m_BuffParamID;

		// Token: 0x0400136A RID: 4970
		public int m_AccessID;

		// Token: 0x0400136B RID: 4971
		public int m_ResultID;

		// Token: 0x0400136C RID: 4972
		public short m_MaxNum;

		// Token: 0x0400136D RID: 4973
		public short m_MaxLevel;

		// Token: 0x0400136E RID: 4974
		public short m_EntryCharaNum;

		// Token: 0x0400136F RID: 4975
		public int m_LevelUpListID;

		// Token: 0x04001370 RID: 4976
		public string m_DetailText;

		// Token: 0x04001371 RID: 4977
		public string m_FlavorText;

		// Token: 0x04001372 RID: 4978
		public int m_ScheduleTagLink;

		// Token: 0x04001373 RID: 4979
		public sbyte m_Tutorial;
	}
}
