﻿using System;

namespace Star
{
	// Token: 0x020001C0 RID: 448
	[Serializable]
	public struct FieldItemDropListDB_Param
	{
		// Token: 0x04000ACC RID: 2764
		public int m_ID;

		// Token: 0x04000ACD RID: 2765
		public int[] m_Num;

		// Token: 0x04000ACE RID: 2766
		public byte[] m_Category;

		// Token: 0x04000ACF RID: 2767
		public int[] m_ObjectID;
	}
}
