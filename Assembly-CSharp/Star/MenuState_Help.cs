﻿using System;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000431 RID: 1073
	public class MenuState_Help : MenuState
	{
		// Token: 0x0600149C RID: 5276 RVA: 0x0006C8F3 File Offset: 0x0006ACF3
		public MenuState_Help(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x0600149D RID: 5277 RVA: 0x0006C90B File Offset: 0x0006AD0B
		public override int GetStateID()
		{
			return 5;
		}

		// Token: 0x0600149E RID: 5278 RVA: 0x0006C90E File Offset: 0x0006AD0E
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Help.eStep.First;
		}

		// Token: 0x0600149F RID: 5279 RVA: 0x0006C917 File Offset: 0x0006AD17
		public override void OnStateExit()
		{
		}

		// Token: 0x060014A0 RID: 5280 RVA: 0x0006C919 File Offset: 0x0006AD19
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014A1 RID: 5281 RVA: 0x0006C924 File Offset: 0x0006AD24
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Help.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Help.eStep.LoadWait;
				break;
			case MenuState_Help.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Help.eStep.PlayIn;
				}
				break;
			case MenuState_Help.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Help.eStep.Main;
				}
				break;
			case MenuState_Help.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Help.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Help.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Help.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014A2 RID: 5282 RVA: 0x0006CA44 File Offset: 0x0006AE44
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuHelpUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014A3 RID: 5283 RVA: 0x0006CA9E File Offset: 0x0006AE9E
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060014A4 RID: 5284 RVA: 0x0006CAAD File Offset: 0x0006AEAD
		private void OnClickButton()
		{
		}

		// Token: 0x060014A5 RID: 5285 RVA: 0x0006CAAF File Offset: 0x0006AEAF
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001B87 RID: 7047
		private MenuState_Help.eStep m_Step = MenuState_Help.eStep.None;

		// Token: 0x04001B88 RID: 7048
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuHelpUI;

		// Token: 0x04001B89 RID: 7049
		private MenuHelpUI m_UI;

		// Token: 0x02000432 RID: 1074
		private enum eStep
		{
			// Token: 0x04001B8B RID: 7051
			None = -1,
			// Token: 0x04001B8C RID: 7052
			First,
			// Token: 0x04001B8D RID: 7053
			LoadWait,
			// Token: 0x04001B8E RID: 7054
			PlayIn,
			// Token: 0x04001B8F RID: 7055
			Main,
			// Token: 0x04001B90 RID: 7056
			UnloadChildSceneWait
		}
	}
}
