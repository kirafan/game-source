﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000B3B RID: 2875
	public class UserAdvData
	{
		// Token: 0x06003C81 RID: 15489 RVA: 0x0013571E File Offset: 0x00133B1E
		public UserAdvData(int[] advIDs)
		{
			this.m_OccurredAdvList = new List<int>(advIDs);
		}

		// Token: 0x06003C82 RID: 15490 RVA: 0x00135732 File Offset: 0x00133B32
		public UserAdvData()
		{
			this.m_OccurredAdvList = new List<int>();
		}

		// Token: 0x06003C83 RID: 15491 RVA: 0x00135745 File Offset: 0x00133B45
		public List<int> GetOccurredAdvIDs()
		{
			return this.m_OccurredAdvList;
		}

		// Token: 0x06003C84 RID: 15492 RVA: 0x0013574D File Offset: 0x00133B4D
		public void SetAdvIDs(int[] advIDs)
		{
			this.m_OccurredAdvList = new List<int>(advIDs);
		}

		// Token: 0x06003C85 RID: 15493 RVA: 0x0013575B File Offset: 0x00133B5B
		public void AddOccurredAdvID(int advID)
		{
			if (!this.m_OccurredAdvList.Contains(advID))
			{
				this.m_OccurredAdvList.Add(advID);
			}
		}

		// Token: 0x06003C86 RID: 15494 RVA: 0x0013577A File Offset: 0x00133B7A
		public bool IsOccurredAdv(int advID)
		{
			return advID == -1 || this.m_OccurredAdvList.Contains(advID);
		}

		// Token: 0x04004450 RID: 17488
		private List<int> m_OccurredAdvList;
	}
}
