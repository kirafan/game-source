﻿using System;

namespace Star
{
	// Token: 0x020004B3 RID: 1203
	public enum eTownUICategory
	{
		// Token: 0x04001E3A RID: 7738
		Room,
		// Token: 0x04001E3B RID: 7739
		FreeArea,
		// Token: 0x04001E3C RID: 7740
		FreeBuf,
		// Token: 0x04001E3D RID: 7741
		Area,
		// Token: 0x04001E3E RID: 7742
		Buf,
		// Token: 0x04001E3F RID: 7743
		Content,
		// Token: 0x04001E40 RID: 7744
		ShopView,
		// Token: 0x04001E41 RID: 7745
		SummonView,
		// Token: 0x04001E42 RID: 7746
		TraningView,
		// Token: 0x04001E43 RID: 7747
		EditView,
		// Token: 0x04001E44 RID: 7748
		QuestView,
		// Token: 0x04001E45 RID: 7749
		MissionView,
		// Token: 0x04001E46 RID: 7750
		CharaTraningView
	}
}
