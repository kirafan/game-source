﻿using System;
using Star.UI;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x02000483 RID: 1155
	public class ShopState_Init : ShopState
	{
		// Token: 0x0600169C RID: 5788 RVA: 0x00076189 File Offset: 0x00074589
		public ShopState_Init(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x0600169D RID: 5789 RVA: 0x000761A1 File Offset: 0x000745A1
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600169E RID: 5790 RVA: 0x000761A4 File Offset: 0x000745A4
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_Init.eStep.First;
		}

		// Token: 0x0600169F RID: 5791 RVA: 0x000761AD File Offset: 0x000745AD
		public override void OnStateExit()
		{
		}

		// Token: 0x060016A0 RID: 5792 RVA: 0x000761AF File Offset: 0x000745AF
		public override void OnDispose()
		{
		}

		// Token: 0x060016A1 RID: 5793 RVA: 0x000761B4 File Offset: 0x000745B4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Step = ShopState_Init.eStep.Load_Wait;
				}
				break;
			case ShopState_Init.eStep.Load_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Shop, false);
					this.m_Step = ShopState_Init.eStep.Load_WaitBG;
				}
				break;
			case ShopState_Init.eStep.Load_WaitBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = ShopState_Init.eStep.SetupUI;
				}
				break;
			case ShopState_Init.eStep.SetupUI:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = ShopState_Init.eStep.End;
				}
				break;
			case ShopState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = ShopState_Init.eStep.None;
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016A2 RID: 5794 RVA: 0x00076314 File Offset: 0x00074714
		private bool SetupUI()
		{
			this.m_Owner.m_NpcUI = UIUtility.GetMenuComponent<NPCCharaDisplayUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.m_NpcUI == null)
			{
				return false;
			}
			this.m_Owner.m_NpcUI.Setup();
			return true;
		}

		// Token: 0x060016A3 RID: 5795 RVA: 0x00076372 File Offset: 0x00074772
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001D56 RID: 7510
		private ShopState_Init.eStep m_Step = ShopState_Init.eStep.None;

		// Token: 0x04001D57 RID: 7511
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.NPCCharaDisplayUI;

		// Token: 0x02000484 RID: 1156
		private enum eStep
		{
			// Token: 0x04001D59 RID: 7513
			None = -1,
			// Token: 0x04001D5A RID: 7514
			First,
			// Token: 0x04001D5B RID: 7515
			Load_Wait,
			// Token: 0x04001D5C RID: 7516
			Load_WaitBG,
			// Token: 0x04001D5D RID: 7517
			SetupUI,
			// Token: 0x04001D5E RID: 7518
			End
		}
	}
}
