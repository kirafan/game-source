﻿using System;

namespace Star
{
	// Token: 0x020004F3 RID: 1267
	public enum eXlsMissionBattleFuncType
	{
		// Token: 0x04001FC4 RID: 8132
		QuestClear,
		// Token: 0x04001FC5 RID: 8133
		EventQuestClear,
		// Token: 0x04001FC6 RID: 8134
		QuestPlay
	}
}
