﻿using System;

namespace Star
{
	// Token: 0x0200019A RID: 410
	public static class NamedFriendshipExpDB_Ext
	{
		// Token: 0x06000AFB RID: 2811 RVA: 0x00041728 File Offset: 0x0003FB28
		public static NamedFriendshipExpDB_Param GetParam(this NamedFriendshipExpDB self, int friendship, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				int num2 = num + friendship - 1;
				if (num2 >= 0 && num2 < self.m_Params.Length)
				{
					return self.m_Params[num2];
				}
			}
			return default(NamedFriendshipExpDB_Param);
		}

		// Token: 0x06000AFC RID: 2812 RVA: 0x000417AC File Offset: 0x0003FBAC
		public static int GetNextExp(this NamedFriendshipExpDB self, int friendship, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				int num2 = num + friendship - 1;
				if (num2 >= 0 && num2 < self.m_Params.Length)
				{
					return self.m_Params[num2].m_NextExp;
				}
			}
			return 0;
		}

		// Token: 0x06000AFD RID: 2813 RVA: 0x00041828 File Offset: 0x0003FC28
		public static long[] GetNextMaxExps(this NamedFriendshipExpDB self, int lv_a, int lv_b, sbyte expTableID)
		{
			int num = lv_b - lv_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(lv_a + i, expTableID);
			}
			return array;
		}

		// Token: 0x06000AFE RID: 2814 RVA: 0x0004186C File Offset: 0x0003FC6C
		public static long GetCurrentExp(this NamedFriendshipExpDB self, int currentLv, long exp, sbyte expTableID)
		{
			long num = 0L;
			for (int i = 1; i < currentLv; i++)
			{
				num += (long)self.GetNextExp(i, expTableID);
			}
			if (exp >= num)
			{
				return exp - num;
			}
			return 0L;
		}

		// Token: 0x06000AFF RID: 2815 RVA: 0x000418A8 File Offset: 0x0003FCA8
		public static int GetFriendShipMax(this NamedFriendshipExpDB self, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			int result = 0;
			if (num >= 0)
			{
				for (int j = num; j < self.m_Params.Length; j++)
				{
					if (self.m_Params[j].m_ID != (int)expTableID)
					{
						break;
					}
					result = self.m_Params[j].m_Lv;
				}
			}
			return result;
		}
	}
}
