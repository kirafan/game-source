﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002FF RID: 767
	public class CharaScheduleTimeSimulate
	{
		// Token: 0x06000F02 RID: 3842 RVA: 0x00050433 File Offset: 0x0004E833
		public void CreateTable(int fnum)
		{
			this.m_EntryCharaTable = new FieldObjHandleChara[fnum];
			this.m_EntryCharaMax = fnum;
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x00050448 File Offset: 0x0004E848
		public void AddSumilateState(FieldObjHandleChara pstate)
		{
			this.m_EntryCharaTable[this.m_EntryCharaNum] = pstate;
			this.m_EntryCharaNum++;
		}

		// Token: 0x06000F04 RID: 3844 RVA: 0x00050468 File Offset: 0x0004E868
		public void CalcSimulate(long fstarttime, long fendtime, FieldBuildMap pbuildmap, bool frefresh)
		{
			long num = (long)ScheduleTimeUtil.GetDayTimeSec(fstarttime);
			long num2 = (long)ScheduleTimeUtil.GetDayTimeSec(fendtime);
			if (num == 0L)
			{
				num -= 1L;
			}
			long num3;
			if (fstarttime != 0L)
			{
				num3 = fstarttime - num;
			}
			else
			{
				num3 = fendtime - num2;
			}
			int num4 = 0;
			int i;
			for (i = 0; i < this.m_EntryCharaNum; i++)
			{
				if (frefresh)
				{
					this.m_EntryCharaTable[i].RefleshSchedule(fendtime);
				}
				num4 += this.m_EntryCharaTable[i].GetScheduleList().GetListNum();
			}
			CharaScheduleTimeSimulate.ScheduleTimePack scheduleTimePack = new CharaScheduleTimeSimulate.ScheduleTimePack(num4);
			for (i = 0; i < this.m_EntryCharaNum; i++)
			{
				scheduleTimePack.InsertSchedule(this.m_EntryCharaTable[i].GetScheduleList(), i);
			}
			int num5 = 0;
			for (i = 0; i < num4; i++)
			{
				if ((long)scheduleTimePack.m_Table[i].m_TimeKey > num)
				{
					num5 = i;
					break;
				}
			}
			FieldBuildMapReq fieldBuildMapReq = new FieldBuildMapReq();
			i = num5;
			while (i < num4)
			{
				if ((long)scheduleTimePack.m_Table[i].m_TimeKey > num2)
				{
					break;
				}
				int timeKey = scheduleTimePack.m_Table[i].m_TimeKey;
				while (i < num4 && scheduleTimePack.m_Table[i].m_TimeKey == timeKey)
				{
					this.m_EntryCharaTable[scheduleTimePack.m_Table[i].m_CharaIndex].PlaySceheduleTag(scheduleTimePack.m_Table[i].m_TimeIndex, fieldBuildMapReq);
					i++;
				}
				if (fieldBuildMapReq.IsRequest())
				{
					pbuildmap.PlayRequest(fieldBuildMapReq, ScheduleTimeUtil.ChangeUnitTimeSecToBase(num3 + (long)timeKey));
					fieldBuildMapReq.ClearRequest();
				}
			}
			for (i = 0; i < this.m_EntryCharaNum; i++)
			{
				this.m_EntryCharaTable[i].SetScheduleTime(fendtime);
			}
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x00050644 File Offset: 0x0004EA44
		public bool CalcSimulate2(long nowTime, long nextTime, FieldBuildMap pbuildmap, bool frefresh, bool isAllSimulate = false)
		{
			for (int i = 0; i < this.m_EntryCharaNum; i++)
			{
				if (frefresh)
				{
					this.m_EntryCharaTable[i].RefleshSchedule(nextTime);
				}
			}
			int num = 0;
			for (int j = 0; j < this.m_EntryCharaNum; j++)
			{
				UserScheduleData.ListPack scheduleList = this.m_EntryCharaTable[j].GetScheduleList();
				if (this.m_EntryCharaTable[j].GetLastPlayScheduleTagTime() <= nowTime)
				{
					long num2 = (long)ScheduleTimeUtil.GetDayTimeSec(this.m_EntryCharaTable[j].GetLastPlayScheduleTagTime());
					long num3 = (long)ScheduleTimeUtil.GetDayTimeSec(nextTime);
					num3 += ((!isAllSimulate || num3 != 0L) ? 0L : 86400L);
					if (num2 == 0L)
					{
						num2 -= 1L;
					}
					long num4;
					if (nowTime != 0L)
					{
						num4 = this.m_EntryCharaTable[j].GetLastPlayScheduleTagTime() - num2;
					}
					else
					{
						num4 = nextTime - num3;
					}
					int num5 = 0;
					int i;
					for (i = 0; i < scheduleList.GetListNum(); i++)
					{
						UserScheduleData.Seg table = scheduleList.GetTable(i);
						if ((long)table.m_WakeTime > num2)
						{
							num5 = i;
							break;
						}
					}
					FieldBuildMapReq fieldBuildMapReq = new FieldBuildMapReq();
					i = num5;
					int num6 = 0;
					while (i < scheduleList.GetListNum())
					{
						if ((long)scheduleList.GetTable(i).m_WakeTime > num3)
						{
							if (i > 0 && num6 == 0)
							{
								i--;
								UserScheduleData.Seg table2 = scheduleList.GetTable(i);
								if (this.m_EntryCharaTable[j].m_PlayScheduleTag != table2.m_TagNameID || this.m_EntryCharaTable[j].m_ScheduleTagID != table2.m_TagNameID)
								{
									this.m_EntryCharaTable[j].PlaySceheduleTag(i, fieldBuildMapReq);
									pbuildmap.PlayRequest(fieldBuildMapReq, ScheduleTimeUtil.ChangeUnitTimeSecToBase(num4 + (long)scheduleList.GetTable(i).m_WakeTime));
									fieldBuildMapReq.ClearRequest();
									if (this.m_EntryCharaTable[j].m_PlayScheduleTag != this.m_EntryCharaTable[j].m_ScheduleTagID)
									{
										Debug.LogWarningFormat("スケジュール更新失敗B!!!!!!!!!\n{0}", new object[]
										{
											this.m_EntryCharaTable[j]
										});
									}
									else
									{
										this.m_EntryCharaTable[j].SetStateChangeTime(num4 + (long)scheduleList.GetTable(i).m_WakeTime);
										num6++;
									}
								}
							}
							break;
						}
						this.m_EntryCharaTable[j].PlaySceheduleTag(i, fieldBuildMapReq);
						if (fieldBuildMapReq.IsRequest())
						{
							long ftimekey = ScheduleTimeUtil.ChangeUnitTimeSecToBase(num4 + (long)scheduleList.GetTable(i).m_WakeTime);
							pbuildmap.PlayRequest(fieldBuildMapReq, ftimekey);
							fieldBuildMapReq.ClearRequest();
							if (this.m_EntryCharaTable[j].m_PlayScheduleTag != this.m_EntryCharaTable[j].m_ScheduleTagID)
							{
								Debug.LogWarningFormat("スケジュール更新失敗A!!!!!!!!!\n{0}", new object[]
								{
									this.m_EntryCharaTable[j]
								});
								i++;
								break;
							}
							this.m_EntryCharaTable[j].SetStateChangeTime(num4 + (long)scheduleList.GetTable(i).m_WakeTime);
						}
						i++;
						num6++;
					}
					this.m_EntryCharaTable[j].SetScheduleTime(nextTime);
					num += num6;
				}
			}
			return num > 0 || frefresh;
		}

		// Token: 0x06000F06 RID: 3846 RVA: 0x00050968 File Offset: 0x0004ED68
		public void SetUpPartyCom(FldComPartyScript pcomparty)
		{
			for (int i = 0; i < this.m_EntryCharaNum; i++)
			{
				pcomparty.StackSendCmd(FldComPartyScript.eCmd.ChangeSchedule, this.m_EntryCharaTable[i].m_ManageID, null);
			}
		}

		// Token: 0x040015E1 RID: 5601
		public FieldObjHandleChara[] m_EntryCharaTable;

		// Token: 0x040015E2 RID: 5602
		public int m_EntryCharaNum;

		// Token: 0x040015E3 RID: 5603
		public int m_EntryCharaMax;

		// Token: 0x02000300 RID: 768
		public struct ScheduleTimeKey
		{
			// Token: 0x040015E4 RID: 5604
			public int m_CharaIndex;

			// Token: 0x040015E5 RID: 5605
			public int m_TimeIndex;

			// Token: 0x040015E6 RID: 5606
			public int m_TimeKey;
		}

		// Token: 0x02000301 RID: 769
		public class ScheduleTimePack
		{
			// Token: 0x06000F07 RID: 3847 RVA: 0x000509A1 File Offset: 0x0004EDA1
			public ScheduleTimePack(int fkeynum)
			{
				this.m_Table = new CharaScheduleTimeSimulate.ScheduleTimeKey[fkeynum + 1];
				this.m_Max = fkeynum;
			}

			// Token: 0x06000F08 RID: 3848 RVA: 0x000509C0 File Offset: 0x0004EDC0
			private void InsertTimeKey(int findex, int fsubindex, int ftime)
			{
				int num = this.m_EntryNum;
				for (int i = 0; i < this.m_EntryNum; i++)
				{
					if (ftime < this.m_Table[i].m_TimeKey)
					{
						num = i;
						for (int j = this.m_EntryNum; j > i; j--)
						{
							this.m_Table[j] = this.m_Table[j - 1];
						}
						while (i > 0)
						{
							if (findex < this.m_Table[i].m_CharaIndex || ftime < this.m_Table[i].m_TimeKey)
							{
								for (int j = num; j > i; j--)
								{
									this.m_Table[j] = this.m_Table[j - 1];
								}
								num = i;
								break;
							}
							i--;
						}
						break;
					}
				}
				this.m_Table[num].m_CharaIndex = findex;
				this.m_Table[num].m_TimeIndex = fsubindex;
				this.m_Table[num].m_TimeKey = ftime;
				this.m_EntryNum++;
			}

			// Token: 0x06000F09 RID: 3849 RVA: 0x00050B04 File Offset: 0x0004EF04
			public void InsertSchedule(UserScheduleData.ListPack pschedule, int findex)
			{
				int listNum = pschedule.GetListNum();
				for (int i = 0; i < listNum; i++)
				{
					this.InsertTimeKey(findex, i, pschedule.GetTable(i).m_WakeTime);
				}
			}

			// Token: 0x040015E7 RID: 5607
			public CharaScheduleTimeSimulate.ScheduleTimeKey[] m_Table;

			// Token: 0x040015E8 RID: 5608
			public int m_EntryNum;

			// Token: 0x040015E9 RID: 5609
			public int m_Max;
		}
	}
}
