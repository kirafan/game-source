﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200013B RID: 315
	[Serializable]
	public class CharacterBattleParam
	{
		// Token: 0x060008C2 RID: 2242 RVA: 0x0003A07C File Offset: 0x0003847C
		public void SetupParams(CharacterHandler owner, float[] townBuffParams)
		{
			this.m_Owner = owner;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_LS = BattleLeaderSkillParser.Parse(this.m_Owner.CharaBattle.MyParty.GetLeaderSkillID(), this.m_Owner);
			this.m_TBuff = townBuffParams;
			this.m_FixedBaseHp = this.m_Owner.CharaParam.Hp;
			this.m_FixedBaseAtk = this.m_Owner.CharaParam.Atk;
			this.m_FixedBaseDef = this.m_Owner.CharaParam.Def;
			this.m_FixedBaseMDef = this.m_Owner.CharaParam.MDef;
			this.m_FixedBaseMgc = this.m_Owner.CharaParam.Mgc;
			this.m_FixedBaseSpd = this.m_Owner.CharaParam.Spd;
			this.m_FixedBaseLuck = this.m_Owner.CharaParam.Luck;
			if (this.m_Owner.NamedParam != null)
			{
				NamedListDB_Param param = dbMng.NamedListDB.GetParam(this.m_Owner.CharaParam.NamedType);
				NamedFriendshipExpDB_Param param2 = dbMng.NamedFriendshipExpDB.GetParam(this.m_Owner.NamedParam.FriendShip, param.m_FriendshipTableID);
				this.m_FixedBaseHp = EditUtility.CalcParamCorrect(this.m_FixedBaseHp, param2.m_CorrectHp);
				this.m_FixedBaseAtk = EditUtility.CalcParamCorrect(this.m_FixedBaseAtk, param2.m_CorrectAtk);
				this.m_FixedBaseDef = EditUtility.CalcParamCorrect(this.m_FixedBaseDef, param2.m_CorrectDef);
				this.m_FixedBaseMDef = EditUtility.CalcParamCorrect(this.m_FixedBaseMDef, param2.m_CorrectMDef);
				this.m_FixedBaseMgc = EditUtility.CalcParamCorrect(this.m_FixedBaseMgc, param2.m_CorrectMgc);
				this.m_FixedBaseSpd = EditUtility.CalcParamCorrect(this.m_FixedBaseSpd, param2.m_CorrectSpd);
				this.m_FixedBaseLuck = EditUtility.CalcParamCorrect(this.m_FixedBaseLuck, param2.m_CorrectLuck);
			}
			if (this.m_TBuff != null)
			{
				this.m_FixedBaseHp = EditUtility.CalcParamCorrect(this.m_FixedBaseHp, this.m_TBuff[0]);
				this.m_FixedBaseAtk = EditUtility.CalcParamCorrect(this.m_FixedBaseAtk, this.m_TBuff[1]);
				this.m_FixedBaseDef = EditUtility.CalcParamCorrect(this.m_FixedBaseDef, this.m_TBuff[3]);
				this.m_FixedBaseMDef = EditUtility.CalcParamCorrect(this.m_FixedBaseMDef, this.m_TBuff[4]);
				this.m_FixedBaseMgc = EditUtility.CalcParamCorrect(this.m_FixedBaseMgc, this.m_TBuff[2]);
				this.m_FixedBaseSpd = EditUtility.CalcParamCorrect(this.m_FixedBaseSpd, this.m_TBuff[5]);
				this.m_FixedBaseLuck = EditUtility.CalcParamCorrect(this.m_FixedBaseLuck, this.m_TBuff[6]);
			}
			if (this.m_Owner.WeaponParam != null)
			{
				this.m_FixedBaseAtk += this.m_Owner.WeaponParam.Atk;
				this.m_FixedBaseDef += this.m_Owner.WeaponParam.Def;
				this.m_FixedBaseMDef += this.m_Owner.WeaponParam.MDef;
				this.m_FixedBaseMgc += this.m_Owner.WeaponParam.Mgc;
			}
			BattleCommandParser.SetupDefaultElementCoef(this.m_Owner.CharaParam.ElementType, ref this.m_DefaultElementCoef);
			if (this.Hp == -1)
			{
				this.RecoveryFullHp();
			}
			for (int i = 0; i < 6; i++)
			{
				if (this.ResistElem[i] == null)
				{
					this.ResistElem[i] = new BattleDefine.BuffStack();
				}
			}
			for (int j = 0; j < 6; j++)
			{
				if (this.StsBuffs[j] == null)
				{
					this.StsBuffs[j] = new BattleDefine.BuffStack();
				}
			}
			for (int k = 0; k < 8; k++)
			{
				if (this.Abnmls[k] == null)
				{
					this.Abnmls[k] = new BattleDefine.StateAbnormal(this.m_Owner.CharaParam.IsRegistAbnormals[k], 0);
				}
			}
			if (this.AbnmlDisableBuff == null)
			{
				this.AbnmlDisableBuff = new BattleDefine.BuffStack();
			}
			if (this.AbnmlAddProbBuff == null)
			{
				this.AbnmlAddProbBuff = new BattleDefine.BuffStack();
			}
			for (int l = 0; l < 3; l++)
			{
				if (this.NextBuffs[l] == null)
				{
					this.NextBuffs[l] = new BattleDefine.NextBuff();
				}
			}
			if (this.WeakElemBonusBuff == null)
			{
				this.WeakElemBonusBuff = new BattleDefine.BuffStack();
			}
			if (this.HateChange == null)
			{
				this.HateChange = new BattleDefine.BuffStack();
			}
			if (this.Regene == null)
			{
				this.Regene = new BattleDefine.RegeneData(null, 0, 0);
			}
		}

		// Token: 0x060008C3 RID: 2243 RVA: 0x0003A50C File Offset: 0x0003890C
		public int FixedMaxHp()
		{
			return this.m_FixedBaseHp;
		}

		// Token: 0x060008C4 RID: 2244 RVA: 0x0003A524 File Offset: 0x00038924
		public int FixedAtk()
		{
			float num = 1f + this.StsBuffs[0].GetValue(false);
			return (int)((float)this.m_FixedBaseAtk * num);
		}

		// Token: 0x060008C5 RID: 2245 RVA: 0x0003A554 File Offset: 0x00038954
		public int FixedMgc()
		{
			float num = 1f + this.StsBuffs[1].GetValue(false);
			return (int)((float)this.m_FixedBaseMgc * num);
		}

		// Token: 0x060008C6 RID: 2246 RVA: 0x0003A584 File Offset: 0x00038984
		public int FixedDef()
		{
			float num = 1f + this.StsBuffs[2].GetValue(false);
			return (int)((float)this.m_FixedBaseDef * num);
		}

		// Token: 0x060008C7 RID: 2247 RVA: 0x0003A5B4 File Offset: 0x000389B4
		public int FixedMDef()
		{
			float num = 1f + this.StsBuffs[3].GetValue(false);
			return (int)((float)this.m_FixedBaseMDef * num);
		}

		// Token: 0x060008C8 RID: 2248 RVA: 0x0003A5E4 File Offset: 0x000389E4
		public int FixedSpd()
		{
			return this.m_FixedBaseSpd;
		}

		// Token: 0x060008C9 RID: 2249 RVA: 0x0003A5FC File Offset: 0x000389FC
		public int FixedLuck(bool ignoreStatusBufCoef = false)
		{
			float num = 1f + this.StsBuffs[5].GetValue(false);
			if (ignoreStatusBufCoef)
			{
				num = 1f;
			}
			return (int)((float)this.m_FixedBaseLuck * num);
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x0003A636 File Offset: 0x00038A36
		public int GetHp()
		{
			return this.Hp;
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x0003A63E File Offset: 0x00038A3E
		public float GetHpRatio()
		{
			return (float)this.Hp / (float)this.FixedMaxHp();
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x0003A64F File Offset: 0x00038A4F
		public void SetHp(int hp)
		{
			this.Hp = hp;
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x0003A658 File Offset: 0x00038A58
		public int CalcHp(int val)
		{
			this.Hp += val;
			this.Hp = Mathf.Clamp(this.Hp, 0, this.FixedMaxHp());
			return this.Hp;
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x0003A686 File Offset: 0x00038A86
		public int RecoveryFullHp()
		{
			this.Hp = this.FixedMaxHp();
			return this.Hp;
		}

		// Token: 0x060008CF RID: 2255 RVA: 0x0003A69C File Offset: 0x00038A9C
		public bool IsHpRange(float minRatio, float maxRatio)
		{
			int num = (int)((float)this.FixedMaxHp() * minRatio);
			int num2 = (int)((float)this.FixedMaxHp() * maxRatio);
			return this.Hp >= num && this.Hp <= num2;
		}

		// Token: 0x060008D0 RID: 2256 RVA: 0x0003A6DA File Offset: 0x00038ADA
		public float[] GetTownBuffParams()
		{
			return this.m_TBuff;
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x0003A6E2 File Offset: 0x00038AE2
		public int GetChargeCount()
		{
			return this.m_ChargeCount;
		}

		// Token: 0x060008D2 RID: 2258 RVA: 0x0003A6EA File Offset: 0x00038AEA
		public void SetChargeCount(int chargeCount)
		{
			this.m_ChargeCount = chargeCount;
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x0003A6F3 File Offset: 0x00038AF3
		public int GetChargeCountMax()
		{
			return this.m_Owner.CharaParam.ChargeCountMax;
		}

		// Token: 0x060008D4 RID: 2260 RVA: 0x0003A705 File Offset: 0x00038B05
		public int CalcChargeCount(int val)
		{
			this.m_ChargeCount = Mathf.Clamp(this.m_ChargeCount + val, 0, this.GetChargeCountMax());
			return this.m_ChargeCount;
		}

		// Token: 0x060008D5 RID: 2261 RVA: 0x0003A727 File Offset: 0x00038B27
		public bool IsChargeCountMax()
		{
			return this.m_ChargeCount >= this.GetChargeCountMax();
		}

		// Token: 0x060008D6 RID: 2262 RVA: 0x0003A73D File Offset: 0x00038B3D
		public bool IsDead()
		{
			return this.Hp <= 0 && this.DeadDetail != BattleDefine.eDeadDetail.Alive;
		}

		// Token: 0x060008D7 RID: 2263 RVA: 0x0003A75B File Offset: 0x00038B5B
		public BattleDefine.eDeadDetail GetDeadDetail()
		{
			return this.DeadDetail;
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x0003A763 File Offset: 0x00038B63
		public void SetDeadDetail(BattleDefine.eDeadDetail deadDetail)
		{
			this.DeadDetail = deadDetail;
		}

		// Token: 0x060008D9 RID: 2265 RVA: 0x0003A76C File Offset: 0x00038B6C
		public void UpdateDeadDetailIfReady()
		{
			if (this.DeadDetail == BattleDefine.eDeadDetail.Dead_Ready)
			{
				this.DeadDetail = BattleDefine.eDeadDetail.Dead_Complete;
			}
		}

		// Token: 0x060008DA RID: 2266 RVA: 0x0003A781 File Offset: 0x00038B81
		public void SetWasAttacked(bool flg)
		{
			this.m_WasAttacked = flg;
		}

		// Token: 0x060008DB RID: 2267 RVA: 0x0003A78A File Offset: 0x00038B8A
		public bool WasAttacked()
		{
			return this.m_WasAttacked;
		}

		// Token: 0x060008DC RID: 2268 RVA: 0x0003A792 File Offset: 0x00038B92
		public void SetWasAttackedOnSleeping(bool flg)
		{
			this.m_WasAttackedOnSleeping = flg;
		}

		// Token: 0x060008DD RID: 2269 RVA: 0x0003A79B File Offset: 0x00038B9B
		public bool WasAttackedOnSleeping()
		{
			return this.m_WasAttackedOnSleeping;
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x0003A7A3 File Offset: 0x00038BA3
		public void SetResistElement(eElementType elementType, eSkillTurnConsume turnConsume, int turn, float value)
		{
			this.ResistElem[(int)elementType].Add(new BattleDefine.BuffData(turnConsume, turn + 1, value));
		}

		// Token: 0x060008DF RID: 2271 RVA: 0x0003A7BD File Offset: 0x00038BBD
		public BattleDefine.BuffStack GetResistElement(eElementType elementType)
		{
			return this.ResistElem[(int)elementType];
		}

		// Token: 0x060008E0 RID: 2272 RVA: 0x0003A7C7 File Offset: 0x00038BC7
		public float GetResistElementValue(eElementType elementType)
		{
			return this.ResistElem[(int)elementType].GetValue(false);
		}

		// Token: 0x060008E1 RID: 2273 RVA: 0x0003A7D7 File Offset: 0x00038BD7
		public float GetDefaultEmentCoef(eElementType elementType)
		{
			return this.m_DefaultElementCoef[(int)elementType];
		}

		// Token: 0x060008E2 RID: 2274 RVA: 0x0003A7E1 File Offset: 0x00038BE1
		public float GetResistElementTownBuffValue(eElementType elementType)
		{
			if (this.m_TBuff != null)
			{
				return this.m_TBuff[(int)(7 + elementType)];
			}
			return 0f;
		}

		// Token: 0x060008E3 RID: 2275 RVA: 0x0003A800 File Offset: 0x00038C00
		public float FixedElementCoef(eElementType elementType)
		{
			float num = this.GetResistElementValue(elementType);
			num += this.GetResistElementTownBuffValue(elementType);
			return this.m_DefaultElementCoef[(int)elementType] - this.m_DefaultElementCoef[(int)elementType] * num;
		}

		// Token: 0x060008E4 RID: 2276 RVA: 0x0003A836 File Offset: 0x00038C36
		public void ResetResistElement(eElementType elementType)
		{
			this.ResistElem[(int)elementType].Clear();
		}

		// Token: 0x060008E5 RID: 2277 RVA: 0x0003A848 File Offset: 0x00038C48
		public void ResetResistElementAll()
		{
			for (int i = 0; i < this.ResistElem.Length; i++)
			{
				this.ResistElem[i].Clear();
			}
		}

		// Token: 0x060008E6 RID: 2278 RVA: 0x0003A87C File Offset: 0x00038C7C
		public void DecreaseTurnResistElementAll(bool isMyTurn)
		{
			for (int i = 0; i < this.ResistElem.Length; i++)
			{
				this.ResistElem[i].DecreaseTurn(isMyTurn);
			}
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x0003A8B0 File Offset: 0x00038CB0
		public int[] GetResistElementFlags()
		{
			int[] array = new int[6];
			for (int i = 0; i < array.Length; i++)
			{
				float resistElementValue = this.GetResistElementValue((eElementType)i);
				if (resistElementValue > 0f)
				{
					array[i] = 1;
				}
				else if (resistElementValue < 0f)
				{
					array[i] = -1;
				}
				else
				{
					array[i] = 0;
				}
			}
			return array;
		}

		// Token: 0x060008E8 RID: 2280 RVA: 0x0003A90D File Offset: 0x00038D0D
		public void ElementChange(eElementType elementType)
		{
			this.m_Owner.CharaParam.ElementType = elementType;
			BattleCommandParser.SetupDefaultElementCoef(this.m_Owner.CharaParam.ElementType, ref this.m_DefaultElementCoef);
		}

		// Token: 0x060008E9 RID: 2281 RVA: 0x0003A93B File Offset: 0x00038D3B
		public void SetStatusBuff(BattleDefine.eStatus status, eSkillTurnConsume turnConsume, int turn, float value)
		{
			this.StsBuffs[(int)status].Add(new BattleDefine.BuffData(turnConsume, turn + 1, value));
		}

		// Token: 0x060008EA RID: 2282 RVA: 0x0003A955 File Offset: 0x00038D55
		public BattleDefine.BuffStack GetStatusBuff(BattleDefine.eStatus status)
		{
			return this.StsBuffs[(int)status];
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x0003A95F File Offset: 0x00038D5F
		public float GetStatusBuffValue(BattleDefine.eStatus status)
		{
			return this.StsBuffs[(int)status].GetValue(status == BattleDefine.eStatus.Spd);
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x0003A972 File Offset: 0x00038D72
		public void ResetStatusBuff(BattleDefine.eStatus status)
		{
			this.StsBuffs[(int)status].Clear();
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x0003A984 File Offset: 0x00038D84
		public void ResetStatusBuffAll()
		{
			for (int i = 0; i < this.StsBuffs.Length; i++)
			{
				this.StsBuffs[i].Clear();
			}
		}

		// Token: 0x060008EE RID: 2286 RVA: 0x0003A9B8 File Offset: 0x00038DB8
		public void DecreaseTurnStatusBuffAll(bool isMyTurn)
		{
			for (int i = 0; i < this.StsBuffs.Length; i++)
			{
				this.StsBuffs[i].DecreaseTurn(isMyTurn);
			}
		}

		// Token: 0x060008EF RID: 2287 RVA: 0x0003A9EC File Offset: 0x00038DEC
		public int[] GetStatusBuffFlags()
		{
			int[] array = new int[6];
			for (int i = 0; i < array.Length; i++)
			{
				BattleDefine.eStatus eStatus = (BattleDefine.eStatus)i;
				float statusBuffValue = this.GetStatusBuffValue(eStatus);
				if (eStatus != BattleDefine.eStatus.Spd)
				{
					if (statusBuffValue > 0f)
					{
						array[i] = 1;
					}
					else if (statusBuffValue < 0f)
					{
						array[i] = -1;
					}
					else
					{
						array[i] = 0;
					}
				}
				else if (statusBuffValue == 0f)
				{
					array[i] = 0;
				}
				else if (statusBuffValue <= 1f)
				{
					array[i] = 1;
				}
				else
				{
					array[i] = -1;
				}
			}
			return array;
		}

		// Token: 0x060008F0 RID: 2288 RVA: 0x0003AA84 File Offset: 0x00038E84
		public void SetStateAbnormal(eStateAbnormal stateAbnormal, int turn)
		{
			if (!this.Abnmls[(int)stateAbnormal].IsRegist)
			{
				this.Abnmls[(int)stateAbnormal].Turn = turn + 1;
			}
			else
			{
				this.Abnmls[(int)stateAbnormal].Turn = 0;
			}
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x0003AAC8 File Offset: 0x00038EC8
		public BattleDefine.StateAbnormal GetStateAbnormal(eStateAbnormal stateAbnormal)
		{
			return this.Abnmls[(int)stateAbnormal];
		}

		// Token: 0x060008F2 RID: 2290 RVA: 0x0003AAD4 File Offset: 0x00038ED4
		public bool IsEnableStateAbnormalAny()
		{
			for (int i = 0; i < 8; i++)
			{
				if (this.Abnmls[i].IsEnable())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060008F3 RID: 2291 RVA: 0x0003AB08 File Offset: 0x00038F08
		public bool IsEnableStateAbnormal(eStateAbnormal stateAbnormal)
		{
			return this.Abnmls[(int)stateAbnormal].IsEnable();
		}

		// Token: 0x060008F4 RID: 2292 RVA: 0x0003AB17 File Offset: 0x00038F17
		public void ResetStateAbnormal(eStateAbnormal stateAbnormal)
		{
			this.Abnmls[(int)stateAbnormal].Reset();
		}

		// Token: 0x060008F5 RID: 2293 RVA: 0x0003AB28 File Offset: 0x00038F28
		public void ResetStateAbnormalAll()
		{
			for (int i = 0; i < this.Abnmls.Length; i++)
			{
				this.Abnmls[i].Reset();
			}
		}

		// Token: 0x060008F6 RID: 2294 RVA: 0x0003AB5C File Offset: 0x00038F5C
		public void DecreaseTurnStateAbnormalAll(bool isMyTurn)
		{
			for (int i = 0; i < this.Abnmls.Length; i++)
			{
				this.Abnmls[i].DecreaseTurn(isMyTurn);
			}
		}

		// Token: 0x060008F7 RID: 2295 RVA: 0x0003AB90 File Offset: 0x00038F90
		public bool CanPossibleCancelSleep()
		{
			return this.m_WasAttackedOnSleeping && this.IsEnableStateAbnormal(eStateAbnormal.Sleep);
		}

		// Token: 0x060008F8 RID: 2296 RVA: 0x0003ABAC File Offset: 0x00038FAC
		public void SetStateAbnormalDisableBuff(eSkillTurnConsume turnConsume, int turn)
		{
			this.AbnmlDisableBuff.Add(new BattleDefine.BuffData(turnConsume, turn + 1, 1f));
		}

		// Token: 0x060008F9 RID: 2297 RVA: 0x0003ABC7 File Offset: 0x00038FC7
		public BattleDefine.BuffStack GetStateAbnormalDisableBuff()
		{
			return this.AbnmlDisableBuff;
		}

		// Token: 0x060008FA RID: 2298 RVA: 0x0003ABCF File Offset: 0x00038FCF
		public bool GetStateAbnormalDisableBuffValue()
		{
			return this.AbnmlDisableBuff.GetValue(false) > 0f;
		}

		// Token: 0x060008FB RID: 2299 RVA: 0x0003ABE4 File Offset: 0x00038FE4
		public void ResetStateAbnormalDisableBuff()
		{
			this.AbnmlDisableBuff.Clear();
		}

		// Token: 0x060008FC RID: 2300 RVA: 0x0003ABF4 File Offset: 0x00038FF4
		public bool[] GetStateAbnormalFlags()
		{
			bool[] array = new bool[8];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = this.IsEnableStateAbnormal((eStateAbnormal)i);
			}
			return array;
		}

		// Token: 0x060008FD RID: 2301 RVA: 0x0003AC27 File Offset: 0x00039027
		public void SetStateAbnormalAddProbabilityBuff(eSkillTurnConsume turnConsume, int turn, float value)
		{
			this.AbnmlAddProbBuff.Add(new BattleDefine.BuffData(turnConsume, turn + 1, value));
		}

		// Token: 0x060008FE RID: 2302 RVA: 0x0003AC3E File Offset: 0x0003903E
		public BattleDefine.BuffStack GetStateAbnormalAddProbabilityBuff()
		{
			return this.AbnmlAddProbBuff;
		}

		// Token: 0x060008FF RID: 2303 RVA: 0x0003AC46 File Offset: 0x00039046
		public float GetStateAbnormalAddProbabilityBuffValue()
		{
			return this.AbnmlAddProbBuff.GetValue(false);
		}

		// Token: 0x06000900 RID: 2304 RVA: 0x0003AC54 File Offset: 0x00039054
		public void ResetStateAbnormalAddProbabilityBuff()
		{
			this.AbnmlAddProbBuff.Clear();
		}

		// Token: 0x06000901 RID: 2305 RVA: 0x0003AC61 File Offset: 0x00039061
		public void SetNextBuff(BattleDefine.eNextBuff nextBuff, float value)
		{
			this.NextBuffs[(int)nextBuff].Step = BattleDefine.NextBuff.eStep.Ready;
			this.NextBuffs[(int)nextBuff].Val = value;
		}

		// Token: 0x06000902 RID: 2306 RVA: 0x0003AC7F File Offset: 0x0003907F
		public BattleDefine.NextBuff GetNextBuff(BattleDefine.eNextBuff nextBuff)
		{
			return this.NextBuffs[(int)nextBuff];
		}

		// Token: 0x06000903 RID: 2307 RVA: 0x0003AC89 File Offset: 0x00039089
		public float GetNextBuffCoef(BattleDefine.eNextBuff nextBuff)
		{
			return this.NextBuffs[(int)nextBuff].GetCoef();
		}

		// Token: 0x06000904 RID: 2308 RVA: 0x0003AC98 File Offset: 0x00039098
		public void ResetNextBuff(BattleDefine.eNextBuff nextBuff)
		{
			this.NextBuffs[(int)nextBuff].Reset();
		}

		// Token: 0x06000905 RID: 2309 RVA: 0x0003ACA8 File Offset: 0x000390A8
		public void ResetNextBuffAll()
		{
			for (int i = 0; i < this.NextBuffs.Length; i++)
			{
				this.NextBuffs[i].Reset();
			}
		}

		// Token: 0x06000906 RID: 2310 RVA: 0x0003ACDB File Offset: 0x000390DB
		public void SetUsedNextBuffIfAvailable(BattleDefine.eNextBuff nextBuff)
		{
			this.NextBuffs[(int)nextBuff].SetUsedIfAvailable();
		}

		// Token: 0x06000907 RID: 2311 RVA: 0x0003ACEC File Offset: 0x000390EC
		public void ResetNextBuffIfUsed()
		{
			for (int i = 0; i < this.NextBuffs.Length; i++)
			{
				this.NextBuffs[i].ResetIfUsed();
			}
		}

		// Token: 0x06000908 RID: 2312 RVA: 0x0003AD1F File Offset: 0x0003911F
		public void SetWeakElementBonusBuff(eSkillTurnConsume turnConsume, int turn, float value)
		{
			this.WeakElemBonusBuff.Add(new BattleDefine.BuffData(turnConsume, turn + 1, value));
		}

		// Token: 0x06000909 RID: 2313 RVA: 0x0003AD36 File Offset: 0x00039136
		public BattleDefine.BuffStack GetWeakElementBonusBuff()
		{
			return this.WeakElemBonusBuff;
		}

		// Token: 0x0600090A RID: 2314 RVA: 0x0003AD3E File Offset: 0x0003913E
		public float GetWeakElementBonusBuffValue()
		{
			return this.WeakElemBonusBuff.GetValue(false);
		}

		// Token: 0x0600090B RID: 2315 RVA: 0x0003AD4C File Offset: 0x0003914C
		public void ResetWeakElementBonusBuff()
		{
			this.WeakElemBonusBuff.Clear();
		}

		// Token: 0x0600090C RID: 2316 RVA: 0x0003AD59 File Offset: 0x00039159
		public void SetHateChange(eSkillTurnConsume turnConsume, int turn, float value)
		{
			this.HateChange.Add(new BattleDefine.BuffData(turnConsume, turn + 1, value));
		}

		// Token: 0x0600090D RID: 2317 RVA: 0x0003AD70 File Offset: 0x00039170
		public BattleDefine.BuffStack GetHateChange()
		{
			return this.HateChange;
		}

		// Token: 0x0600090E RID: 2318 RVA: 0x0003AD78 File Offset: 0x00039178
		public float GetHateChangeValue()
		{
			return this.HateChange.GetValue(false);
		}

		// Token: 0x0600090F RID: 2319 RVA: 0x0003AD86 File Offset: 0x00039186
		public void ResetHateChange()
		{
			this.HateChange.Clear();
		}

		// Token: 0x06000910 RID: 2320 RVA: 0x0003AD93 File Offset: 0x00039193
		public void SetRegene(string ownerName, int turn, int power)
		{
			this.Regene.OwnerName = ownerName;
			this.Regene.Turn = turn;
			this.Regene.Pow = power;
		}

		// Token: 0x06000911 RID: 2321 RVA: 0x0003ADB9 File Offset: 0x000391B9
		public BattleDefine.RegeneData GetRegene()
		{
			return this.Regene;
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x0003ADC1 File Offset: 0x000391C1
		public float GetRegenePower()
		{
			return (float)this.Regene.Pow;
		}

		// Token: 0x06000913 RID: 2323 RVA: 0x0003ADCF File Offset: 0x000391CF
		public void ResetRegene()
		{
			this.Regene.Reset();
		}

		// Token: 0x06000914 RID: 2324 RVA: 0x0003ADDC File Offset: 0x000391DC
		public void SetBarrier(float cutRatio, int count)
		{
			this.Barrier = cutRatio;
			this.BarrierCount = count;
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x0003ADEC File Offset: 0x000391EC
		public float GetBarrierCutRatio()
		{
			return this.Barrier;
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x0003ADF4 File Offset: 0x000391F4
		public int GetBarrierCount()
		{
			return this.BarrierCount;
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x0003ADFC File Offset: 0x000391FC
		public void DecreaseBarrier()
		{
			this.BarrierCount--;
			if (this.BarrierCount <= 0)
			{
				this.ResetBarrier();
			}
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x0003AE1E File Offset: 0x0003921E
		public void ResetBarrier()
		{
			this.Barrier = 0f;
			this.BarrierCount = 0;
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x0003AE32 File Offset: 0x00039232
		public bool IsGuard()
		{
			return this.m_IsGuard;
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x0003AE3A File Offset: 0x0003923A
		public void GuardOn()
		{
			this.m_IsGuard = true;
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x0003AE43 File Offset: 0x00039243
		public void GuardOff()
		{
			this.m_IsGuard = false;
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x0003AE4C File Offset: 0x0003924C
		public void MemberChangeRecastFull()
		{
			this.MCRecast = (int)BattleUtility.DefVal(eBattleDefineDB.MemberChangeRecast);
		}

		// Token: 0x0600091D RID: 2333 RVA: 0x0003AE5B File Offset: 0x0003925B
		public void MemberChangeRecastZero()
		{
			this.MCRecast = 0;
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x0003AE64 File Offset: 0x00039264
		public void MemberChangeRecastDecrement()
		{
			this.MCRecast = Mathf.Max(this.MCRecast - 1, 0);
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0003AE7A File Offset: 0x0003927A
		public int GetMemberChangeRecast()
		{
			return this.MCRecast;
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x0003AE82 File Offset: 0x00039282
		public int GetMemberChangeRecastMax()
		{
			return (int)BattleUtility.DefVal(eBattleDefineDB.MemberChangeRecast);
		}

		// Token: 0x0400084B RID: 2123
		[NonSerialized]
		private CharacterHandler m_Owner;

		// Token: 0x0400084C RID: 2124
		[NonSerialized]
		private BattleLeaderSkill m_LS;

		// Token: 0x0400084D RID: 2125
		[NonSerialized]
		private float[] m_TBuff;

		// Token: 0x0400084E RID: 2126
		[NonSerialized]
		private int m_FixedBaseHp;

		// Token: 0x0400084F RID: 2127
		[NonSerialized]
		private int m_FixedBaseAtk;

		// Token: 0x04000850 RID: 2128
		[NonSerialized]
		private int m_FixedBaseMgc;

		// Token: 0x04000851 RID: 2129
		[NonSerialized]
		private int m_FixedBaseDef;

		// Token: 0x04000852 RID: 2130
		[NonSerialized]
		private int m_FixedBaseMDef;

		// Token: 0x04000853 RID: 2131
		[NonSerialized]
		private int m_FixedBaseSpd;

		// Token: 0x04000854 RID: 2132
		[NonSerialized]
		private int m_FixedBaseLuck;

		// Token: 0x04000855 RID: 2133
		[NonSerialized]
		private float[] m_DefaultElementCoef = new float[6];

		// Token: 0x04000856 RID: 2134
		[SerializeField]
		private BattleDefine.BuffStack[] ResistElem = new BattleDefine.BuffStack[6];

		// Token: 0x04000857 RID: 2135
		[SerializeField]
		private BattleDefine.BuffStack[] StsBuffs = new BattleDefine.BuffStack[6];

		// Token: 0x04000858 RID: 2136
		[SerializeField]
		private BattleDefine.StateAbnormal[] Abnmls = new BattleDefine.StateAbnormal[8];

		// Token: 0x04000859 RID: 2137
		[SerializeField]
		private BattleDefine.BuffStack AbnmlDisableBuff;

		// Token: 0x0400085A RID: 2138
		[SerializeField]
		private BattleDefine.BuffStack AbnmlAddProbBuff;

		// Token: 0x0400085B RID: 2139
		[SerializeField]
		private BattleDefine.NextBuff[] NextBuffs = new BattleDefine.NextBuff[3];

		// Token: 0x0400085C RID: 2140
		[SerializeField]
		private BattleDefine.BuffStack WeakElemBonusBuff;

		// Token: 0x0400085D RID: 2141
		[SerializeField]
		private BattleDefine.BuffStack HateChange;

		// Token: 0x0400085E RID: 2142
		[SerializeField]
		private BattleDefine.RegeneData Regene;

		// Token: 0x0400085F RID: 2143
		[SerializeField]
		private float Barrier;

		// Token: 0x04000860 RID: 2144
		[SerializeField]
		private int BarrierCount;

		// Token: 0x04000861 RID: 2145
		[SerializeField]
		private int Hp = -1;

		// Token: 0x04000862 RID: 2146
		[NonSerialized]
		private int m_ChargeCount;

		// Token: 0x04000863 RID: 2147
		[SerializeField]
		private BattleDefine.eDeadDetail DeadDetail;

		// Token: 0x04000864 RID: 2148
		[NonSerialized]
		private bool m_IsGuard;

		// Token: 0x04000865 RID: 2149
		[NonSerialized]
		private bool m_WasAttacked;

		// Token: 0x04000866 RID: 2150
		[NonSerialized]
		private bool m_WasAttackedOnSleeping;

		// Token: 0x04000867 RID: 2151
		[SerializeField]
		private int MCRecast;
	}
}
