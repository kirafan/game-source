﻿using System;

namespace Star
{
	// Token: 0x0200022B RID: 555
	public enum eTownObjectCategory
	{
		// Token: 0x04000F2E RID: 3886
		Room,
		// Token: 0x04000F2F RID: 3887
		Item,
		// Token: 0x04000F30 RID: 3888
		Money,
		// Token: 0x04000F31 RID: 3889
		Num,
		// Token: 0x04000F32 RID: 3890
		Area,
		// Token: 0x04000F33 RID: 3891
		Buf,
		// Token: 0x04000F34 RID: 3892
		Contents,
		// Token: 0x04000F35 RID: 3893
		Menu
	}
}
