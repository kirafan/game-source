﻿using System;
using FieldPartyMemberResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000349 RID: 841
	public class FieldPartyAPIAddAll : INetComHandle
	{
		// Token: 0x0600101A RID: 4122 RVA: 0x00055C2F File Offset: 0x0005402F
		public FieldPartyAPIAddAll()
		{
			this.ApiName = "player/field_party/member/add_all";
			this.Request = true;
			this.ResponseType = typeof(AddAll);
		}

		// Token: 0x0400172A RID: 5930
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
