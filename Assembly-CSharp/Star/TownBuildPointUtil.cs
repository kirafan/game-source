﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006F3 RID: 1779
	public class TownBuildPointUtil : IDataBaseResource
	{
		// Token: 0x06002343 RID: 9027 RVA: 0x000BD513 File Offset: 0x000BB913
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
			this.m_DB = (pdataobj as TownBuildPointDB);
		}

		// Token: 0x06002344 RID: 9028 RVA: 0x000BD521 File Offset: 0x000BB921
		public static void DatabaseSetUp()
		{
			if (TownBuildPointUtil.ms_BuildPointDB == null)
			{
				TownBuildPointUtil.ms_BuildPointDB = new TownBuildPointUtil();
				TownBuildPointUtil.ms_BuildPointDB.LoadResInManager("Prefab/Town/Option/TownBuildPointDB", ".asset");
			}
		}

		// Token: 0x06002345 RID: 9029 RVA: 0x000BD54B File Offset: 0x000BB94B
		public static bool IsSetUp()
		{
			return TownBuildPointUtil.ms_BuildPointDB.m_Active;
		}

		// Token: 0x06002346 RID: 9030 RVA: 0x000BD557 File Offset: 0x000BB957
		public static void DatabaseRelease()
		{
			if (TownBuildPointUtil.ms_BuildPointDB != null)
			{
				TownBuildPointUtil.ms_BuildPointDB.Release();
			}
			TownBuildPointUtil.ms_BuildPointDB = null;
		}

		// Token: 0x06002347 RID: 9031 RVA: 0x000BD574 File Offset: 0x000BB974
		public static int SearchBuildPointDB(string fname)
		{
			int num = TownBuildPointUtil.ms_BuildPointDB.m_DB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (TownBuildPointUtil.ms_BuildPointDB.m_DB.m_Params[i].m_MakeID == fname)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06002348 RID: 9032 RVA: 0x000BD5CD File Offset: 0x000BB9CD
		public static void GetBuildPointDB(out TownBuildPointDB_Param param, int findex)
		{
			param = TownBuildPointUtil.ms_BuildPointDB.m_DB.m_Params[findex];
		}

		// Token: 0x06002349 RID: 9033 RVA: 0x000BD5F0 File Offset: 0x000BB9F0
		public static void RepointBuildKey(ref TownBuildPointDB_Param param, Transform ptrs)
		{
			Vector3 localPosition = ptrs.localPosition;
			localPosition.z = (float)param.m_Layer;
			if (param.m_ObjOffsetX != 0f)
			{
				localPosition.x = param.m_ObjOffsetX;
			}
			if (param.m_ObjOffsetY != 0f)
			{
				localPosition.y = param.m_ObjOffsetY;
			}
			ptrs.localPosition = localPosition;
		}

		// Token: 0x04002A17 RID: 10775
		public static TownBuildPointUtil ms_BuildPointDB;

		// Token: 0x04002A18 RID: 10776
		private TownBuildPointDB m_DB;
	}
}
