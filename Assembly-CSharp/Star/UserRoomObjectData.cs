﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020003A9 RID: 937
	public class UserRoomObjectData
	{
		// Token: 0x060011BB RID: 4539 RVA: 0x0005D918 File Offset: 0x0005BD18
		public UserRoomObjectData(eRoomObjectCategory objCategory, int objID)
		{
			this.ObjCategory = objCategory;
			this.ObjID = objID;
			this.PlacementNum = 0;
		}

		// Token: 0x060011BC RID: 4540 RVA: 0x0005D940 File Offset: 0x0005BD40
		public UserRoomObjectData()
		{
		}

		// Token: 0x060011BD RID: 4541 RVA: 0x0005D954 File Offset: 0x0005BD54
		public void ClrToListUpUse()
		{
			for (int i = 0; i < this.m_MngIDList.Count; i++)
			{
				this.m_MngIDList[i].m_SearchUse = false;
			}
		}

		// Token: 0x060011BE RID: 4542 RVA: 0x0005D990 File Offset: 0x0005BD90
		public void SetListUpToUse(long fmanageid)
		{
			for (int i = 0; i < this.m_MngIDList.Count; i++)
			{
				if (this.m_MngIDList[i].m_ManageID == fmanageid)
				{
					this.m_MngIDList[i].m_SearchUse = true;
					break;
				}
			}
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x0005D9E8 File Offset: 0x0005BDE8
		public bool ReleaseListUpNoUse()
		{
			for (int i = this.m_MngIDList.Count - 1; i >= 0; i--)
			{
				if (!this.m_MngIDList[i].m_SearchUse)
				{
					this.m_MngIDList.RemoveAt(i);
				}
			}
			return this.m_MngIDList.Count != 0;
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x0005DA48 File Offset: 0x0005BE48
		public long GetFreeLinkMngID(bool flock = true)
		{
			long result = -1L;
			int count = this.m_MngIDList.Count;
			for (int i = 0; i < count; i++)
			{
				if (!this.m_MngIDList[i].m_LinkUse)
				{
					result = this.m_MngIDList[i].m_ManageID;
					this.m_MngIDList[i].m_LinkUse = flock;
					if (flock)
					{
						this.PlacementNum++;
					}
					break;
				}
			}
			return result;
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x0005DACC File Offset: 0x0005BECC
		public void SetFreeLinkMngID(long fmanageid)
		{
			int count = this.m_MngIDList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MngIDList[i].m_ManageID == fmanageid)
				{
					this.m_MngIDList[i].m_LinkUse = false;
					this.PlacementNum--;
					break;
				}
			}
		}

		// Token: 0x060011C2 RID: 4546 RVA: 0x0005DB34 File Offset: 0x0005BF34
		public void LockLinkMngID(long flockmngid)
		{
			int count = this.m_MngIDList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MngIDList[i].m_ManageID == flockmngid)
				{
					if (!this.m_MngIDList[i].m_LinkUse)
					{
						this.m_MngIDList[i].m_LinkUse = true;
						this.PlacementNum++;
					}
					break;
				}
			}
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x0005DBB4 File Offset: 0x0005BFB4
		public bool ReleaseLinkMngID(long fmanageid)
		{
			int count = this.m_MngIDList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MngIDList[i].m_ManageID == fmanageid)
				{
					this.m_MngIDList.RemoveAt(i);
					break;
				}
			}
			return this.m_MngIDList.Count == 0;
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x0005DC18 File Offset: 0x0005C018
		public bool IsLinkMngID(long fchkmngid)
		{
			int count = this.m_MngIDList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MngIDList[i].m_ManageID == fchkmngid)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060011C5 RID: 4549 RVA: 0x0005DC5D File Offset: 0x0005C05D
		public int HaveNum
		{
			get
			{
				return this.m_MngIDList.Count;
			}
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060011C6 RID: 4550 RVA: 0x0005DC6A File Offset: 0x0005C06A
		public int RemainNum
		{
			get
			{
				return this.m_MngIDList.Count - this.PlacementNum;
			}
		}

		// Token: 0x060011C7 RID: 4551 RVA: 0x0005DC80 File Offset: 0x0005C080
		public void SetServerData(long fmanageid, int resourceid)
		{
			UserRoomObjectData.RoomObjMngIDState roomObjMngIDState = new UserRoomObjectData.RoomObjMngIDState();
			roomObjMngIDState.m_ManageID = fmanageid;
			roomObjMngIDState.m_LinkUse = false;
			this.m_MngIDList.Add(roomObjMngIDState);
			this.ResourceID = resourceid;
		}

		// Token: 0x04001868 RID: 6248
		public int ResourceID;

		// Token: 0x04001869 RID: 6249
		public eRoomObjectCategory ObjCategory;

		// Token: 0x0400186A RID: 6250
		public int ObjID;

		// Token: 0x0400186B RID: 6251
		public int PlacementNum;

		// Token: 0x0400186C RID: 6252
		public List<UserRoomObjectData.RoomObjMngIDState> m_MngIDList = new List<UserRoomObjectData.RoomObjMngIDState>();

		// Token: 0x02000B53 RID: 2899
		public class RoomObjMngIDState
		{
			// Token: 0x040044D1 RID: 17617
			public long m_ManageID;

			// Token: 0x040044D2 RID: 17618
			public bool m_LinkUse;

			// Token: 0x040044D3 RID: 17619
			public bool m_SearchUse;
		}
	}
}
