﻿using System;

namespace Star
{
	// Token: 0x020003C7 RID: 967
	public static class GachaDefine
	{
		// Token: 0x020003C8 RID: 968
		public enum eCostType
		{
			// Token: 0x040018ED RID: 6381
			Invalid = -1,
			// Token: 0x040018EE RID: 6382
			Free,
			// Token: 0x040018EF RID: 6383
			Consume = 2
		}

		// Token: 0x020003C9 RID: 969
		public enum ePlayType
		{
			// Token: 0x040018F1 RID: 6385
			Unlimited = 1,
			// Token: 0x040018F2 RID: 6386
			Gem1,
			// Token: 0x040018F3 RID: 6387
			Gem10,
			// Token: 0x040018F4 RID: 6388
			Item
		}

		// Token: 0x020003CA RID: 970
		public enum eReturnGachaPlay
		{
			// Token: 0x040018F6 RID: 6390
			Success,
			// Token: 0x040018F7 RID: 6391
			GachaOutOfPeriod,
			// Token: 0x040018F8 RID: 6392
			GachaDrawLimit,
			// Token: 0x040018F9 RID: 6393
			GemIsShort,
			// Token: 0x040018FA RID: 6394
			UnlimitedGemIsShort,
			// Token: 0x040018FB RID: 6395
			ItemIsShort,
			// Token: 0x040018FC RID: 6396
			GachaStepDrawLimit,
			// Token: 0x040018FD RID: 6397
			Unknown
		}

		// Token: 0x020003CB RID: 971
		public enum eCheckPlay
		{
			// Token: 0x040018FF RID: 6399
			Ok,
			// Token: 0x04001900 RID: 6400
			GemIsShort,
			// Token: 0x04001901 RID: 6401
			ItemIsShort
		}
	}
}
