﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Star
{
	// Token: 0x0200037A RID: 890
	public class SystemFileWriteManager : MonoBehaviour
	{
		// Token: 0x060010F1 RID: 4337 RVA: 0x00059334 File Offset: 0x00057734
		public static SystemFileWriteManager GetManager()
		{
			if (SystemFileWriteManager.Inst == null)
			{
				GameObject gameObject = new GameObject("CashFileWrite");
				SystemFileWriteManager.Inst = gameObject.AddComponent<SystemFileWriteManager>();
				UnityEngine.Object.DontDestroyOnLoad(gameObject);
			}
			return SystemFileWriteManager.Inst;
		}

		// Token: 0x060010F2 RID: 4338 RVA: 0x00059374 File Offset: 0x00057774
		public void EntryFileWrite(string pfilename, byte[] pdata)
		{
			SystemFileWriteManager.FileWriteInfo fileWriteInfo = new SystemFileWriteManager.FileWriteInfo();
			fileWriteInfo.m_Filename = pfilename;
			fileWriteInfo.m_Data = pdata;
			this.m_List.Add(fileWriteInfo);
		}

		// Token: 0x060010F3 RID: 4339 RVA: 0x000593A4 File Offset: 0x000577A4
		private void Update()
		{
			SystemFileWriteManager.FileWriteInfo fileWriteInfo = this.m_List[0];
			if (!fileWriteInfo.UpFunc())
			{
				this.m_List.RemoveAt(0);
			}
			if (this.m_List.Count == 0)
			{
				SystemFileWriteManager.Inst = null;
				UnityEngine.Object.Destroy(base.gameObject);
			}
		}

		// Token: 0x040017DD RID: 6109
		public static SystemFileWriteManager Inst;

		// Token: 0x040017DE RID: 6110
		private List<SystemFileWriteManager.FileWriteInfo> m_List = new List<SystemFileWriteManager.FileWriteInfo>();

		// Token: 0x0200037B RID: 891
		public class FileWriteInfo
		{
			// Token: 0x060010F5 RID: 4341 RVA: 0x00059400 File Offset: 0x00057800
			public bool UpFunc()
			{
				bool result = true;
				byte step = this.m_Step;
				if (step != 0)
				{
					if (step != 1)
					{
						if (step == 2)
						{
							result = false;
						}
					}
				}
				else
				{
					this.m_File = new FileStream(this.m_Filename, FileMode.Create, FileAccess.Write, FileShare.Write, this.m_Data.Length, true);
					if (this.m_File != null)
					{
						this.m_Step += 1;
						this.m_File.BeginWrite(this.m_Data, 0, this.m_Data.Length, new AsyncCallback(this.CallbackWriteEnd), this);
					}
				}
				return result;
			}

			// Token: 0x060010F6 RID: 4342 RVA: 0x000594A0 File Offset: 0x000578A0
			private void CallbackWriteEnd(IAsyncResult pres)
			{
				this.m_File.EndWrite(pres);
				this.m_File.Close();
				this.m_File = null;
				this.m_Data = null;
				this.m_Step += 1;
			}

			// Token: 0x040017DF RID: 6111
			public string m_Filename;

			// Token: 0x040017E0 RID: 6112
			public byte[] m_Data;

			// Token: 0x040017E1 RID: 6113
			public byte m_Step;

			// Token: 0x040017E2 RID: 6114
			public FileStream m_File;
		}
	}
}
