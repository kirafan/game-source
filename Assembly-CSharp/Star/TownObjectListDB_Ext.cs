﻿using System;

namespace Star
{
	// Token: 0x020001B8 RID: 440
	public static class TownObjectListDB_Ext
	{
		// Token: 0x06000B33 RID: 2867 RVA: 0x00042A84 File Offset: 0x00040E84
		public static TownObjectListDB_Param GetParam(this TownObjectListDB self, int townObjectID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == townObjectID)
				{
					return self.m_Params[i];
				}
			}
			return default(TownObjectListDB_Param);
		}
	}
}
