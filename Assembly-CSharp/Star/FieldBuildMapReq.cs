﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000320 RID: 800
	public class FieldBuildMapReq
	{
		// Token: 0x06000F35 RID: 3893 RVA: 0x00052104 File Offset: 0x00050504
		public void CharaMoveBuildPoint(long fcharamngid, long foldpointmngid, FieldBuildMapReq.MoveBeforeCallback pbfcallback, FieldBuildMapReq.MoveResultCallback pafcallback)
		{
			FieldBuildMapReq.CCharaMoveState ccharaMoveState = new FieldBuildMapReq.CCharaMoveState();
			ccharaMoveState.m_ManageID = fcharamngid;
			ccharaMoveState.m_OldPointMngID = foldpointmngid;
			ccharaMoveState.m_BfCallback = pbfcallback;
			ccharaMoveState.m_AfCallBack = pafcallback;
			this.m_StateList.Add(ccharaMoveState);
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x00052140 File Offset: 0x00050540
		public bool IsRequest()
		{
			return this.m_StateList.Count != 0;
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x00052153 File Offset: 0x00050553
		public int GetRequestNum()
		{
			return this.m_StateList.Count;
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x00052160 File Offset: 0x00050560
		public FieldBuildMapReq.CCharaMoveState GetRequest(int findex)
		{
			return this.m_StateList[findex];
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x0005216E File Offset: 0x0005056E
		public void ClearRequest()
		{
			this.m_StateList.Clear();
		}

		// Token: 0x040016AE RID: 5806
		public List<FieldBuildMapReq.CCharaMoveState> m_StateList = new List<FieldBuildMapReq.CCharaMoveState>();

		// Token: 0x02000321 RID: 801
		// (Invoke) Token: 0x06000F3B RID: 3899
		public delegate void MoveResultCallback(eTownMoveState fup, long fnextpoint, long ftimekey);

		// Token: 0x02000322 RID: 802
		// (Invoke) Token: 0x06000F3F RID: 3903
		public delegate bool MoveBeforeCallback(FieldBuildMap pbuild, ref TownSearchResult presult);

		// Token: 0x02000323 RID: 803
		public class CCharaMoveState
		{
			// Token: 0x040016AF RID: 5807
			public long m_ManageID;

			// Token: 0x040016B0 RID: 5808
			public long m_OldPointMngID;

			// Token: 0x040016B1 RID: 5809
			public FieldBuildMapReq.MoveBeforeCallback m_BfCallback;

			// Token: 0x040016B2 RID: 5810
			public FieldBuildMapReq.MoveResultCallback m_AfCallBack;
		}
	}
}
