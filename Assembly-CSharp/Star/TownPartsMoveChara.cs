﻿using System;

namespace Star
{
	// Token: 0x020006CB RID: 1739
	public class TownPartsMoveChara : ITownPartsAction
	{
		// Token: 0x06002277 RID: 8823 RVA: 0x000B7D2B File Offset: 0x000B612B
		public TownPartsMoveChara(TownObjHandleChara phandle, TownBuilder pbuilder, long flinkbuildpoint, long fbuildpoint)
		{
			this.m_Active = true;
			this.m_Handle = phandle;
			this.m_Builder = pbuilder;
			this.m_State = TownPartsMoveChara.eState.Init;
			this.m_LinkBuildPointMngID = flinkbuildpoint;
			this.m_NextBuildPointMngID = fbuildpoint;
		}

		// Token: 0x06002278 RID: 8824 RVA: 0x000B7D60 File Offset: 0x000B6160
		public override bool PartsUpdate()
		{
			switch (this.m_State)
			{
			case TownPartsMoveChara.eState.Init:
				this.m_Handle.GetModel().PlayMotion(2);
				this.m_State = TownPartsMoveChara.eState.FadeOut;
				break;
			case TownPartsMoveChara.eState.FadeOut:
				if (!this.m_Handle.GetModel().IsPlaying(2))
				{
					this.m_State = TownPartsMoveChara.eState.ChangePoint;
					BindTownCharaMap charaBind = this.m_Builder.GetCharaBind(this.m_LinkBuildPointMngID, true);
					charaBind.ReBindGameObject(this.m_Handle);
				}
				break;
			case TownPartsMoveChara.eState.ChangePoint:
			{
				this.m_State = TownPartsMoveChara.eState.FadeIn;
				BindTownCharaMap charaBind = this.m_Builder.GetCharaBind(this.m_NextBuildPointMngID, false);
				if (charaBind != null)
				{
					charaBind.BindGameObject(this.m_Handle);
					this.m_Handle.GetModel().PlayMotion(0);
				}
				else
				{
					this.m_Active = false;
				}
				break;
			}
			case TownPartsMoveChara.eState.FadeIn:
				if (!this.m_Handle.GetModel().IsPlaying(0))
				{
					this.m_Active = false;
				}
				break;
			}
			return this.m_Active;
		}

		// Token: 0x04002940 RID: 10560
		private TownObjHandleChara m_Handle;

		// Token: 0x04002941 RID: 10561
		private TownBuilder m_Builder;

		// Token: 0x04002942 RID: 10562
		private TownPartsMoveChara.eState m_State;

		// Token: 0x04002943 RID: 10563
		private long m_LinkBuildPointMngID;

		// Token: 0x04002944 RID: 10564
		private long m_NextBuildPointMngID;

		// Token: 0x020006CC RID: 1740
		public enum eState
		{
			// Token: 0x04002946 RID: 10566
			Init,
			// Token: 0x04002947 RID: 10567
			FadeOut,
			// Token: 0x04002948 RID: 10568
			ChangePoint,
			// Token: 0x04002949 RID: 10569
			FadeIn
		}
	}
}
