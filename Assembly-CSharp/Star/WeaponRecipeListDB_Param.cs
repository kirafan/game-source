﻿using System;

namespace Star
{
	// Token: 0x02000264 RID: 612
	[Serializable]
	public struct WeaponRecipeListDB_Param
	{
		// Token: 0x040013DA RID: 5082
		public int m_ID;

		// Token: 0x040013DB RID: 5083
		public int m_WeaponID;

		// Token: 0x040013DC RID: 5084
		public int m_Amount;

		// Token: 0x040013DD RID: 5085
		public int[] m_ItemIDs;

		// Token: 0x040013DE RID: 5086
		public int[] m_ItemNums;
	}
}
