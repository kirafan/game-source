﻿using System;

namespace Star
{
	// Token: 0x020001F1 RID: 497
	[Serializable]
	public struct RetireTipsListDB_Param
	{
		// Token: 0x04000C01 RID: 3073
		public int m_ID;

		// Token: 0x04000C02 RID: 3074
		public RetireTipsListDB_Data[] m_Datas;
	}
}
