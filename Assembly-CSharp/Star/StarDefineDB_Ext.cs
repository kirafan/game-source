﻿using System;

namespace Star
{
	// Token: 0x020001B1 RID: 433
	public static class StarDefineDB_Ext
	{
		// Token: 0x06000B28 RID: 2856 RVA: 0x000426F0 File Offset: 0x00040AF0
		public static float GetValue(this StarDefineDB self, eStarDefineDB eDef)
		{
			if (eDef < (eStarDefineDB)self.m_Params.Length)
			{
				return self.m_Params[(int)eDef].m_Value;
			}
			return 0f;
		}
	}
}
