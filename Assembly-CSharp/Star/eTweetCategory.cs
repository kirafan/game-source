﻿using System;

namespace Star
{
	// Token: 0x0200025C RID: 604
	public enum eTweetCategory
	{
		// Token: 0x040013B2 RID: 5042
		None = -1,
		// Token: 0x040013B3 RID: 5043
		HomeComment,
		// Token: 0x040013B4 RID: 5044
		GreetWake,
		// Token: 0x040013B5 RID: 5045
		GreetSleep,
		// Token: 0x040013B6 RID: 5046
		GreetGoHome,
		// Token: 0x040013B7 RID: 5047
		GreetGoTown,
		// Token: 0x040013B8 RID: 5048
		GreetVisit,
		// Token: 0x040013B9 RID: 5049
		SpecialTweet
	}
}
