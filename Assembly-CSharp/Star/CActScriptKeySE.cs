﻿using System;

namespace Star
{
	// Token: 0x0200054D RID: 1357
	public class CActScriptKeySE : IRoomScriptData
	{
		// Token: 0x06001ACB RID: 6859 RVA: 0x0008EE58 File Offset: 0x0008D258
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeySE actXlsKeySE = (ActXlsKeySE)pbase;
			this.m_SE = actXlsKeySE.m_SE;
		}

		// Token: 0x04002197 RID: 8599
		public int m_SE;
	}
}
