﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200029C RID: 668
	public class RoomDebugObjectData : MonoBehaviour
	{
		// Token: 0x04001529 RID: 5417
		[SerializeField]
		public bool m_ClearRoomObject;

		// Token: 0x0400152A RID: 5418
		[SerializeField]
		public List<UserDebugData> m_UserState;

		// Token: 0x0400152B RID: 5419
		[SerializeField]
		public List<RoomDebugData> m_Room;

		// Token: 0x0400152C RID: 5420
		[SerializeField]
		public List<CharaEntryDebugData> m_CharMngID;

		// Token: 0x0400152D RID: 5421
		[SerializeField]
		public List<CharaDebugData> m_CharaTable;

		// Token: 0x0400152E RID: 5422
		[SerializeField]
		public List<TownDebugData> m_Town;

		// Token: 0x0400152F RID: 5423
		[SerializeField]
		public List<CharaDebugTraning> m_Traning;
	}
}
