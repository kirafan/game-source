﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200072F RID: 1839
	public class TownHudMessageControll
	{
		// Token: 0x06002443 RID: 9283 RVA: 0x000C2798 File Offset: 0x000C0B98
		public GameObject GetHudObject(TownResourceObjectData pobjmng, int fobjid)
		{
			GameObject nextObjectInCache = pobjmng.GetNextObjectInCache(fobjid);
			if (this.m_PriFirstHud != null)
			{
				TownCharaHud componentInChildren = this.m_PriFirstHud.GetComponentInChildren<TownCharaHud>();
				if (componentInChildren != null)
				{
					componentInChildren.CancelPlay();
				}
			}
			this.m_PriFirstHud = nextObjectInCache;
			return nextObjectInCache;
		}

		// Token: 0x06002444 RID: 9284 RVA: 0x000C27E4 File Offset: 0x000C0BE4
		public void ReleaseHudObject(TownResourceObjectData pobjmng, GameObject pobj)
		{
			if (this.m_PriFirstHud == pobj)
			{
				this.m_PriFirstHud = null;
			}
			pobjmng.FreeObjectInCache(pobj);
		}

		// Token: 0x04002B3E RID: 11070
		private GameObject m_PriFirstHud;
	}
}
