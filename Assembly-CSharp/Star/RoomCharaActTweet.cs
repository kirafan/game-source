﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005BC RID: 1468
	public class RoomCharaActTweet : IRoomCharaAct
	{
		// Token: 0x06001C6C RID: 7276 RVA: 0x0009813C File Offset: 0x0009653C
		private int CalcTweetText(CharacterHandler phandle)
		{
			int result;
			if (ScheduleDataBase.GetScheduleHolyday().GetSecToHolydayType(ScheduleTimeUtil.GetSystemTimeSec()) != 0)
			{
				result = UnityEngine.Random.Range(22000, 22004);
			}
			else
			{
				result = UnityEngine.Random.Range(21000, 21004);
			}
			return result;
		}

		// Token: 0x06001C6D RID: 7277 RVA: 0x00098188 File Offset: 0x00096588
		public void RequestTweetMode(RoomObjectCtrlChara pbase)
		{
			this.m_Timer = 4.5f;
			this.m_VoiceReqID = -1;
			pbase.PlayMotion(RoomDefine.eAnim.Idle, WrapMode.Loop);
			pbase.SetFace(8);
			if (pbase.GetUIHud() != null)
			{
				int tweetID = this.CalcTweetText(pbase.GetHandle());
				TweetListDB_Param param = pbase.GetHandle().CharaResource.TweetListDB.GetParam(tweetID);
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceReqID);
				if (param.m_VoiceCueName != null && param.m_VoiceCueName.Length > 1)
				{
					this.m_VoiceReqID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(pbase.GetHandle().CharaID).m_NamedType, param.m_VoiceCueName, false);
				}
				pbase.GetUIHud().GetCharaTweet().Open(param.m_Text);
			}
		}

		// Token: 0x06001C6E RID: 7278 RVA: 0x0009827C File Offset: 0x0009667C
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			this.m_Timer -= Time.deltaTime;
			if (this.m_VoiceReqID != -1 && !SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_VoiceReqID))
			{
				this.m_VoiceReqID = -1;
				if (this.m_Timer > 0.75f)
				{
					this.m_Timer = 0.75f;
				}
			}
			if (this.m_Timer <= 0f && this.m_VoiceReqID == -1)
			{
				if (pbase.GetUIHud() != null)
				{
					pbase.GetUIHud().GetCharaTweet().Close();
				}
				this.m_VoiceReqID = -1;
				result = false;
			}
			return result;
		}

		// Token: 0x04002344 RID: 9028
		private float m_Timer;

		// Token: 0x04002345 RID: 9029
		private int m_VoiceReqID = -1;
	}
}
