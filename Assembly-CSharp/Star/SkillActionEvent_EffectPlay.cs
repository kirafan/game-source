﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000114 RID: 276
	[Serializable]
	public class SkillActionEvent_EffectPlay : SkillActionEvent_Base
	{
		// Token: 0x04000701 RID: 1793
		public string m_EffectID;

		// Token: 0x04000702 RID: 1794
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x04000703 RID: 1795
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x04000704 RID: 1796
		public Vector2 m_TargetPosOffset;
	}
}
