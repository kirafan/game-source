﻿using System;

namespace Star
{
	// Token: 0x020001D4 RID: 468
	[Serializable]
	public struct MoviePlayListDB_Param
	{
		// Token: 0x04000B15 RID: 2837
		public string m_FileName;
	}
}
