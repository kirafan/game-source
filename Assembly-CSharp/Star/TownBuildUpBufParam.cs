﻿using System;

namespace Star
{
	// Token: 0x02000700 RID: 1792
	public class TownBuildUpBufParam : ITownEventCommand
	{
		// Token: 0x0600237B RID: 9083 RVA: 0x000BE714 File Offset: 0x000BCB14
		public TownBuildUpBufParam(int fbuildpoint, long fmanageid, int fobjid, bool finitup = true, TownBuildUpBufParam.BuildUpEvent pcallback = null)
		{
			this.m_Type = eTownRequest.BuildBuf;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_FixModel = false;
			this.m_EventCall = pcallback;
			this.m_Enable = true;
			this.m_Step = TownBuildUpBufParam.eStep.BuildQue;
			this.m_InitUp = finitup;
		}

		// Token: 0x0600237C RID: 9084 RVA: 0x000BE768 File Offset: 0x000BCB68
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (this.m_Step == TownBuildUpBufParam.eStep.BuildQue)
			{
				if (pbuilder.BuildToPoint(this, this.m_InitUp))
				{
					this.m_Step = TownBuildUpBufParam.eStep.SetUpCheck;
				}
			}
			else if (pbuilder.IsBuildMngIDToBuildUp(this.m_ManageID))
			{
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002A49 RID: 10825
		public int m_BuildPointID;

		// Token: 0x04002A4A RID: 10826
		public int m_ObjID;

		// Token: 0x04002A4B RID: 10827
		public long m_ManageID;

		// Token: 0x04002A4C RID: 10828
		public bool m_AccessLinkID;

		// Token: 0x04002A4D RID: 10829
		public bool m_FixModel;

		// Token: 0x04002A4E RID: 10830
		public TownBuildUpBufParam.eStep m_Step;

		// Token: 0x04002A4F RID: 10831
		public bool m_InitUp;

		// Token: 0x04002A50 RID: 10832
		public TownBuildUpBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000701 RID: 1793
		public enum eStep
		{
			// Token: 0x04002A52 RID: 10834
			BuildQue,
			// Token: 0x04002A53 RID: 10835
			SetUpCheck
		}

		// Token: 0x02000702 RID: 1794
		// (Invoke) Token: 0x0600237E RID: 9086
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
