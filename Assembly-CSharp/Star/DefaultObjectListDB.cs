﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000304 RID: 772
	public class DefaultObjectListDB : ScriptableObject
	{
		// Token: 0x040015F9 RID: 5625
		public DefaultObjectListDB_Param[] m_Params;
	}
}
