﻿using System;

namespace Star
{
	// Token: 0x02000202 RID: 514
	[Serializable]
	public struct SkillContentListDB_Param
	{
		// Token: 0x04000CAB RID: 3243
		public int m_ID;

		// Token: 0x04000CAC RID: 3244
		public SkillContentListDB_Data[] m_Datas;
	}
}
