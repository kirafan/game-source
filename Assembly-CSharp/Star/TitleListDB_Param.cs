﻿using System;

namespace Star
{
	// Token: 0x02000240 RID: 576
	[Serializable]
	public struct TitleListDB_Param
	{
		// Token: 0x04001310 RID: 4880
		public int m_TitleType;

		// Token: 0x04001311 RID: 4881
		public int m_Order;

		// Token: 0x04001312 RID: 4882
		public string m_ResouceBaseName;

		// Token: 0x04001313 RID: 4883
		public string m_DisplayName;

		// Token: 0x04001314 RID: 4884
		public int m_LimitBreakItemID;

		// Token: 0x04001315 RID: 4885
		public string m_Descript;

		// Token: 0x04001316 RID: 4886
		public string m_URL;
	}
}
