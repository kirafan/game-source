﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000D1 RID: 209
	[Serializable]
	public class BattleOccurEnemy
	{
		// Token: 0x06000597 RID: 1431 RVA: 0x0001CDAD File Offset: 0x0001B1AD
		public List<BattleOccurEnemy.OneWave> GetWaves()
		{
			return this.m_Waves;
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0001CDB5 File Offset: 0x0001B1B5
		public int GetWaveNum()
		{
			return (this.m_Waves == null) ? 0 : this.m_Waves.Count;
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0001CDD4 File Offset: 0x0001B1D4
		public void Setup(ref QuestListDB_Param ref_questListParam, ref QuestWaveListDB ref_questWaveListDB, ref QuestWaveRandomListDB ref_questWaveRandomListDB)
		{
			this.m_Waves = new List<BattleOccurEnemy.OneWave>();
			int waveNum = QuestListDB_Ext.GetWaveNum(ref ref_questListParam);
			for (int i = 0; i < waveNum; i++)
			{
				BattleOccurEnemy.OneWave oneWave = new BattleOccurEnemy.OneWave();
				QuestWaveListDB_Param param = ref_questWaveListDB.GetParam(ref_questListParam.m_WaveIDs[i]);
				int num = 3;
				for (int j = 0; j < num; j++)
				{
					int enemyID = -1;
					int enemyLv = -1;
					int dropID = -1;
					if (param.m_QuestRandomIDs != null && j < param.m_QuestRandomIDs.Length && param.m_QuestRandomIDs[j] != -1)
					{
						QuestWaveRandomListDB_Param param2 = ref_questWaveRandomListDB.GetParam(param.m_QuestRandomIDs[j]);
						float num2 = 0f;
						int num3 = param2.m_Num;
						for (int k = 0; k < num3; k++)
						{
							num2 += param2.m_Prob[k];
						}
						if (num2 == 0f)
						{
							num2 = 100f;
						}
						float[] array = new float[num3];
						for (int l = 0; l < num3; l++)
						{
							array[l] = param2.m_Prob[l] / num2;
						}
						float num4 = UnityEngine.Random.Range(0f, 1f);
						float num5 = 0f;
						int num6 = -1;
						for (int m = 0; m < num3; m++)
						{
							if (num4 >= num5 && num4 <= num5 + array[m])
							{
								num6 = m;
								break;
							}
							num5 += array[m];
						}
						if (num6 == -1)
						{
							num6 = 0;
						}
						enemyID = param2.m_QuestEnemyIDs[num6];
						enemyLv = param2.m_QuestEnemyLvs[num6];
						dropID = param2.m_DropIDs[num6];
					}
					else if (param.m_QuestEnemyIDs != null && j < param.m_QuestEnemyIDs.Length && param.m_QuestEnemyIDs[j] != -1)
					{
						enemyID = param.m_QuestEnemyIDs[j];
						enemyLv = param.m_QuestEnemyLvs[j];
						dropID = param.m_DropIDs[j];
					}
					BattleOccurEnemy.OneEnemy item = new BattleOccurEnemy.OneEnemy(enemyID, enemyLv, dropID);
					oneWave.m_Enemies.Add(item);
				}
				this.m_Waves.Add(oneWave);
			}
		}

		// Token: 0x04000479 RID: 1145
		[SerializeField]
		private List<BattleOccurEnemy.OneWave> m_Waves;

		// Token: 0x020000D2 RID: 210
		[Serializable]
		public class OneWave
		{
			// Token: 0x0400047A RID: 1146
			public List<BattleOccurEnemy.OneEnemy> m_Enemies = new List<BattleOccurEnemy.OneEnemy>();
		}

		// Token: 0x020000D3 RID: 211
		[Serializable]
		public class OneEnemy
		{
			// Token: 0x0600059B RID: 1435 RVA: 0x0001D017 File Offset: 0x0001B417
			public OneEnemy(int enemyID, int enemyLv, int dropID)
			{
				this.m_EnemyID = enemyID;
				this.m_EnemyLv = enemyLv;
				this.m_DropID = dropID;
			}

			// Token: 0x0400047B RID: 1147
			public int m_EnemyID;

			// Token: 0x0400047C RID: 1148
			public int m_EnemyLv;

			// Token: 0x0400047D RID: 1149
			public int m_DropID;
		}
	}
}
