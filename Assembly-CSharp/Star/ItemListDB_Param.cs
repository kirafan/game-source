﻿using System;

namespace Star
{
	// Token: 0x020001C7 RID: 455
	[Serializable]
	public struct ItemListDB_Param
	{
		// Token: 0x04000AE5 RID: 2789
		public int m_ID;

		// Token: 0x04000AE6 RID: 2790
		public string m_Name;

		// Token: 0x04000AE7 RID: 2791
		public int m_Rare;

		// Token: 0x04000AE8 RID: 2792
		public int m_SaleAmount;

		// Token: 0x04000AE9 RID: 2793
		public int m_Type;

		// Token: 0x04000AEA RID: 2794
		public int[] m_TypeArgs;

		// Token: 0x04000AEB RID: 2795
		public string m_DetailText;
	}
}
