﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x02000409 RID: 1033
	public class EditState_LimitBreak : EditState
	{
		// Token: 0x060013A2 RID: 5026 RVA: 0x00068ED0 File Offset: 0x000672D0
		public EditState_LimitBreak(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x00068EE8 File Offset: 0x000672E8
		public override int GetStateID()
		{
			return 12;
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x00068EEC File Offset: 0x000672EC
		public override void OnStateEnter()
		{
			this.m_Step = EditState_LimitBreak.eStep.First;
		}

		// Token: 0x060013A5 RID: 5029 RVA: 0x00068EF5 File Offset: 0x000672F5
		public override void OnStateExit()
		{
		}

		// Token: 0x060013A6 RID: 5030 RVA: 0x00068EF7 File Offset: 0x000672F7
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060013A7 RID: 5031 RVA: 0x00068F00 File Offset: 0x00067300
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_LimitBreak.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_LimitBreak.eStep.LoadWait;
				break;
			case EditState_LimitBreak.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_LimitBreak.eStep.PlayIn;
				}
				break;
			case EditState_LimitBreak.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_LimitBreak.eStep.Main;
				}
				break;
			case EditState_LimitBreak.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_BackButton)
					{
						this.m_NextState = 11;
					}
					else
					{
						CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton == CharaEditSceneUIBase.eButton.Decide)
						{
							this.m_NextState = 13;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_LimitBreak.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_LimitBreak.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_LimitBreak.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060013A8 RID: 5032 RVA: 0x0006905C File Offset: 0x0006745C
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<LimitBreakUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID);
			this.m_UI.OnClickButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LimitBreak);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060013A9 RID: 5033 RVA: 0x0006910A File Offset: 0x0006750A
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.m_BackButton = true;
			this.GoToMenuEnd();
		}

		// Token: 0x060013AA RID: 5034 RVA: 0x00069120 File Offset: 0x00067520
		private void OnClickButton()
		{
			CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
			if (selectButton == CharaEditSceneUIBase.eButton.Decide)
			{
				this.Request_LimitBreak(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID, this.m_UI.SelectButtonIdx, new MeigewwwParam.Callback(this.OnResponse_LimitBreak));
			}
		}

		// Token: 0x060013AB RID: 5035 RVA: 0x00069175 File Offset: 0x00067575
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060013AC RID: 5036 RVA: 0x00069194 File Offset: 0x00067594
		public void Request_LimitBreak(long charaMngID, int itemIndex, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Characterlimitbreak characterlimitbreak = new PlayerRequestTypes.Characterlimitbreak();
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
			CharacterLimitBreakListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			characterlimitbreak.managedCharacterId = charaMngID;
			characterlimitbreak.recipeId = param2.m_RecipeID;
			characterlimitbreak.itemIndex = itemIndex;
			MeigewwwParam wwwParam = PlayerRequest.Characterlimitbreak(characterlimitbreak, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013AD RID: 5037 RVA: 0x00069230 File Offset: 0x00067630
		private void OnResponse_LimitBreak(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Characterlimitbreak characterlimitbreak = PlayerResponse.Characterlimitbreak(wwwParam, ResponseCommon.DialogType.None, null);
			if (characterlimitbreak == null)
			{
				return;
			}
			ResultCode result = characterlimitbreak.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.LIMIT_BREAK_LIMIT && result != ResultCode.GOLD_IS_SHORT)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(characterlimitbreak.managedCharacter.managedCharacterId);
				EditMain.LimitBreakResultData limitBreakResultData = new EditMain.LimitBreakResultData();
				limitBreakResultData.m_CharaMngID = characterlimitbreak.managedCharacter.managedCharacterId;
				limitBreakResultData.m_BeforeMaxLv = userCharaData.Param.MaxLv;
				limitBreakResultData.m_BeforeMaxUniqueSkillLv = userCharaData.Param.UniqueSkillLearnData.SkillMaxLv;
				limitBreakResultData.m_BeforeMaxClassSkillLv = new int[userCharaData.Param.ClassSkillLearnDatas.Count];
				for (int i = 0; i < userCharaData.Param.ClassSkillLearnDatas.Count; i++)
				{
					if (userCharaData.Param.ClassSkillLearnDatas[i].SkillID != -1)
					{
						limitBreakResultData.m_BeforeMaxClassSkillLv[i] = userCharaData.Param.ClassSkillLearnDatas[i].SkillMaxLv;
					}
				}
				APIUtility.wwwToUserData(characterlimitbreak.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(characterlimitbreak.managedCharacter, userCharaData);
				userDataMng.UserData.Gold = characterlimitbreak.gold;
				limitBreakResultData.m_AfterMaxLv = userCharaData.Param.MaxLv;
				limitBreakResultData.m_AfterMaxUniqueSkillLv = userCharaData.Param.UniqueSkillLearnData.SkillMaxLv;
				limitBreakResultData.m_AfterMaxClassSkillLv = new int[userCharaData.Param.ClassSkillLearnDatas.Count];
				for (int j = 0; j < userCharaData.Param.ClassSkillLearnDatas.Count; j++)
				{
					if (userCharaData.Param.ClassSkillLearnDatas[j].SkillID != -1)
					{
						limitBreakResultData.m_AfterMaxClassSkillLv[j] = userCharaData.Param.ClassSkillLearnDatas[j].SkillMaxLv;
					}
				}
				this.m_Owner.LimitBreakResult = limitBreakResultData;
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, false);
				MissionManager.UnlockAction(eXlsMissionSeg.Edit, eXlsMissionEditFuncType.CharaLimitBreak);
				this.GoToMenuEnd();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
			}
		}

		// Token: 0x04001A9B RID: 6811
		private EditState_LimitBreak.eStep m_Step = EditState_LimitBreak.eStep.None;

		// Token: 0x04001A9C RID: 6812
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.LimitBreakUI;

		// Token: 0x04001A9D RID: 6813
		public LimitBreakUI m_UI;

		// Token: 0x04001A9E RID: 6814
		private bool m_BackButton;

		// Token: 0x0200040A RID: 1034
		private enum eStep
		{
			// Token: 0x04001AA0 RID: 6816
			None = -1,
			// Token: 0x04001AA1 RID: 6817
			First,
			// Token: 0x04001AA2 RID: 6818
			LoadWait,
			// Token: 0x04001AA3 RID: 6819
			PlayIn,
			// Token: 0x04001AA4 RID: 6820
			Main,
			// Token: 0x04001AA5 RID: 6821
			UnloadChildSceneWait
		}
	}
}
