﻿using System;

namespace Star
{
	// Token: 0x020003DA RID: 986
	public class ADVState : GameStateBase
	{
		// Token: 0x060012C0 RID: 4800 RVA: 0x000647EF File Offset: 0x00062BEF
		public ADVState(ADVMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x060012C1 RID: 4801 RVA: 0x00064805 File Offset: 0x00062C05
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x060012C2 RID: 4802 RVA: 0x00064808 File Offset: 0x00062C08
		public override void OnStateEnter()
		{
		}

		// Token: 0x060012C3 RID: 4803 RVA: 0x0006480A File Offset: 0x00062C0A
		public override void OnStateExit()
		{
		}

		// Token: 0x060012C4 RID: 4804 RVA: 0x0006480C File Offset: 0x00062C0C
		public override void OnDispose()
		{
		}

		// Token: 0x060012C5 RID: 4805 RVA: 0x0006480E File Offset: 0x00062C0E
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x060012C6 RID: 4806 RVA: 0x00064811 File Offset: 0x00062C11
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001998 RID: 6552
		protected ADVMain m_Owner;

		// Token: 0x04001999 RID: 6553
		protected int m_NextState = -1;
	}
}
