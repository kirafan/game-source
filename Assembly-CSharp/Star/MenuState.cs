﻿using System;

namespace Star
{
	// Token: 0x0200042E RID: 1070
	public class MenuState : GameStateBase
	{
		// Token: 0x0600148A RID: 5258 RVA: 0x0006C5CF File Offset: 0x0006A9CF
		public MenuState(MenuMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600148B RID: 5259 RVA: 0x0006C5E5 File Offset: 0x0006A9E5
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600148C RID: 5260 RVA: 0x0006C5E8 File Offset: 0x0006A9E8
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600148D RID: 5261 RVA: 0x0006C5EA File Offset: 0x0006A9EA
		public override void OnStateExit()
		{
		}

		// Token: 0x0600148E RID: 5262 RVA: 0x0006C5EC File Offset: 0x0006A9EC
		public override void OnDispose()
		{
		}

		// Token: 0x0600148F RID: 5263 RVA: 0x0006C5EE File Offset: 0x0006A9EE
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001490 RID: 5264 RVA: 0x0006C5F1 File Offset: 0x0006A9F1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001B7A RID: 7034
		protected MenuMain m_Owner;

		// Token: 0x04001B7B RID: 7035
		protected int m_NextState = -1;
	}
}
