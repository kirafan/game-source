﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Star
{
	// Token: 0x0200033A RID: 826
	public class BinaryReader : BinaryIO
	{
		// Token: 0x06000FD2 RID: 4050 RVA: 0x00054554 File Offset: 0x00052954
		public int Enum(Enum fkey, Type ftype)
		{
			int result = BitConverter.ToInt32(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			return result;
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x00054584 File Offset: 0x00052984
		public void Bool(ref bool fkey)
		{
			byte b = this.m_Buffer[this.m_FilePoint];
			this.m_FilePoint++;
			fkey = (b != 0);
		}

		// Token: 0x06000FD4 RID: 4052 RVA: 0x000545BC File Offset: 0x000529BC
		public void Byte(ref byte fkey)
		{
			fkey = this.m_Buffer[this.m_FilePoint];
			this.m_FilePoint++;
		}

		// Token: 0x06000FD5 RID: 4053 RVA: 0x000545DB File Offset: 0x000529DB
		public void Short(ref short fkey)
		{
			fkey = BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 2;
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x000545FE File Offset: 0x000529FE
		public void Int(ref int fkey)
		{
			fkey = BitConverter.ToInt32(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
		}

		// Token: 0x06000FD7 RID: 4055 RVA: 0x00054621 File Offset: 0x00052A21
		public void Long(ref long fkey)
		{
			fkey = BitConverter.ToInt64(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 8;
		}

		// Token: 0x06000FD8 RID: 4056 RVA: 0x00054644 File Offset: 0x00052A44
		public void Float(ref float fkey)
		{
			fkey = BitConverter.ToSingle(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
		}

		// Token: 0x06000FD9 RID: 4057 RVA: 0x00054668 File Offset: 0x00052A68
		public void String(ref string fkey)
		{
			short num = BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 2;
			if (num != 0)
			{
				byte[] array = new byte[(int)num];
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, (int)num);
				fkey = Encoding.UTF8.GetString(array);
				this.m_FilePoint += (int)num;
			}
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x000546D4 File Offset: 0x00052AD4
		public void ByteTable(ref byte[] fkey)
		{
			int num = (int)BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			fkey = new byte[num];
			Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, fkey, 0, num);
			this.m_FilePoint += num;
		}

		// Token: 0x06000FDB RID: 4059 RVA: 0x0005472C File Offset: 0x00052B2C
		public void ShortTable(ref short[] fkey)
		{
			int num = (int)BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			byte[] array = new byte[num * 2];
			Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, num * 2);
			fkey = new short[num];
			fkey.CopyTo(array, 0);
			this.m_FilePoint += 2 * num;
		}

		// Token: 0x06000FDC RID: 4060 RVA: 0x0005479C File Offset: 0x00052B9C
		public void IntTable(ref int[] fkey)
		{
			int num = (int)BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			byte[] array = new byte[num * 4];
			Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, num * 4);
			fkey = new int[num];
			fkey.CopyTo(array, 0);
			this.m_FilePoint += 4 * num;
		}

		// Token: 0x06000FDD RID: 4061 RVA: 0x00054809 File Offset: 0x00052C09
		public void SetBinary(byte[] pdat, int fsize)
		{
			this.m_Buffer = pdat;
			this.m_FilePoint = 0;
			this.m_FileSize = fsize;
			this.m_StackPoint = new int[8];
			this.m_StackNum = 0;
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x00054833 File Offset: 0x00052C33
		public void CreateBuffer(int fsize)
		{
			this.m_Buffer = new byte[fsize];
			this.m_FilePoint = 0;
			this.m_FileSize = fsize;
			this.m_StackPoint = new int[8];
			this.m_StackNum = 0;
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x00054864 File Offset: 0x00052C64
		public byte GetByte()
		{
			byte result = this.m_Buffer[this.m_FilePoint];
			this.m_FilePoint++;
			return result;
		}

		// Token: 0x06000FE0 RID: 4064 RVA: 0x00054890 File Offset: 0x00052C90
		public short GetShort()
		{
			short result = BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 2;
			return result;
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x000548C0 File Offset: 0x00052CC0
		public int GetInt()
		{
			int result = BitConverter.ToInt32(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			return result;
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x000548F0 File Offset: 0x00052CF0
		public long GetLong()
		{
			long result = BitConverter.ToInt64(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 8;
			return result;
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x00054920 File Offset: 0x00052D20
		public float GetFloat()
		{
			float result = BitConverter.ToSingle(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			return result;
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x00054950 File Offset: 0x00052D50
		public string GetString()
		{
			short num = BitConverter.ToInt16(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 2;
			string result;
			if (num != 0)
			{
				byte[] array = new byte[(int)num];
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, (int)num);
				result = Encoding.UTF8.GetString(array);
				this.m_FilePoint += (int)num;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x000549C0 File Offset: 0x00052DC0
		public byte[] GetByteTable(int flen)
		{
			byte[] array = new byte[flen];
			if (flen != 0)
			{
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, flen);
				this.m_FilePoint += flen;
			}
			return array;
		}

		// Token: 0x06000FE6 RID: 4070 RVA: 0x00054A00 File Offset: 0x00052E00
		public short[] GetShortTable(int flen)
		{
			short[] array = new short[flen];
			if (flen != 0)
			{
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, flen * 2);
				this.m_FilePoint += flen * 2;
			}
			return array;
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x00054A44 File Offset: 0x00052E44
		public int[] GetIntTable(int flen)
		{
			int[] array = new int[flen];
			if (flen != 0)
			{
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, flen * 4);
				this.m_FilePoint += flen * 4;
			}
			return array;
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x00054A85 File Offset: 0x00052E85
		public void Offset(int fslide)
		{
			this.m_FilePoint += fslide;
		}

		// Token: 0x06000FE9 RID: 4073 RVA: 0x00054A95 File Offset: 0x00052E95
		public void PushPoint(int foffset)
		{
			this.m_StackPoint[this.m_StackNum] = this.m_FilePoint;
			this.m_StackNum++;
			this.m_FilePoint = foffset;
		}

		// Token: 0x06000FEA RID: 4074 RVA: 0x00054ABF File Offset: 0x00052EBF
		public void PopPoint()
		{
			if (this.m_StackNum > 0)
			{
				this.m_StackNum--;
				this.m_FilePoint = this.m_StackPoint[this.m_StackNum];
			}
		}

		// Token: 0x06000FEB RID: 4075 RVA: 0x00054AEE File Offset: 0x00052EEE
		public void SetMarking()
		{
			this.m_MarkingPoint = this.m_FilePoint;
		}

		// Token: 0x06000FEC RID: 4076 RVA: 0x00054AFC File Offset: 0x00052EFC
		public string GetCPString()
		{
			short num = 0;
			while (this.m_Buffer[(int)num + this.m_FilePoint] != 0)
			{
				num += 1;
			}
			string result;
			if (num != 0)
			{
				byte[] array = new byte[(int)num];
				Buffer.BlockCopy(this.m_Buffer, this.m_FilePoint, array, 0, (int)num);
				result = Encoding.UTF8.GetString(array);
				this.m_FilePoint += (int)num;
			}
			else
			{
				this.m_FilePoint++;
				result = null;
			}
			return result;
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x00054B7C File Offset: 0x00052F7C
		public string GetMsgName()
		{
			int num = this.m_FilePoint;
			num += BitConverter.ToInt32(this.m_Buffer, this.m_FilePoint);
			this.m_FilePoint += 4;
			string result;
			if (num != 0)
			{
				short num2 = 0;
				while (this.m_Buffer[(int)num2 + num] != 0)
				{
					num2 += 1;
				}
				if (num2 != 0)
				{
					byte[] array = new byte[(int)num2];
					Buffer.BlockCopy(this.m_Buffer, num, array, 0, (int)num2);
					result = Encoding.UTF8.GetString(array);
				}
				else
				{
					result = null;
				}
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x00054C0A File Offset: 0x0005300A
		public bool IsEOF()
		{
			return this.m_FilePoint >= this.m_FileSize;
		}

		// Token: 0x0400170B RID: 5899
		public byte[] m_Buffer;

		// Token: 0x0400170C RID: 5900
		public int m_FilePoint;

		// Token: 0x0400170D RID: 5901
		public int m_FileSize;

		// Token: 0x0400170E RID: 5902
		public int[] m_StackPoint;

		// Token: 0x0400170F RID: 5903
		public int m_StackNum;

		// Token: 0x04001710 RID: 5904
		public int m_MarkingPoint;

		// Token: 0x0200033B RID: 827
		[StructLayout(LayoutKind.Explicit)]
		private struct ItoF
		{
			// Token: 0x04001711 RID: 5905
			[FieldOffset(0)]
			public int i;

			// Token: 0x04001712 RID: 5906
			[FieldOffset(0)]
			public float f;
		}
	}
}
