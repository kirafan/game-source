﻿using System;
using Star.UI;

namespace Star
{
	// Token: 0x0200045B RID: 1115
	public class QuestState_Init : QuestState
	{
		// Token: 0x06001598 RID: 5528 RVA: 0x000705A4 File Offset: 0x0006E9A4
		public QuestState_Init(QuestMain owner) : base(owner)
		{
			this.m_NextState = 1;
		}

		// Token: 0x06001599 RID: 5529 RVA: 0x000705BB File Offset: 0x0006E9BB
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600159A RID: 5530 RVA: 0x000705BE File Offset: 0x0006E9BE
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_Init.eStep.First;
		}

		// Token: 0x0600159B RID: 5531 RVA: 0x000705C7 File Offset: 0x0006E9C7
		public override void OnStateExit()
		{
		}

		// Token: 0x0600159C RID: 5532 RVA: 0x000705C9 File Offset: 0x0006E9C9
		public override void OnDispose()
		{
		}

		// Token: 0x0600159D RID: 5533 RVA: 0x000705CC File Offset: 0x0006E9CC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
					QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
					if (globalParam.QuestStartMode == GameGlobalParameter.eQuestStartMode.List)
					{
						bool flag = false;
						QuestManager.SelectQuest selectQuest = questMng.GetSelectQuest();
						if (globalParam.QuestFirstCleared)
						{
							globalParam.QuestFirstCleared = false;
							selectQuest = questMng.NextSelectQuest(out flag);
							if (flag)
							{
								this.m_Owner.m_ForceChapterOpenAnim = true;
							}
						}
						int eventType = selectQuest.m_EventType;
						if (eventType != -1)
						{
							this.m_Owner.BGLoad(-1, false);
							if (flag)
							{
								this.m_NextState = 2;
							}
							else
							{
								this.m_NextState = 4;
							}
						}
						else if (flag)
						{
							this.m_Owner.BGLoad(-1, false);
							this.m_NextState = 3;
						}
						else
						{
							this.m_Owner.BGLoad(selectQuest.m_ChapterID, false);
							this.m_NextState = 4;
						}
						globalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.None;
					}
					else
					{
						this.m_Owner.BGLoad(-1, false);
						this.m_NextState = 1;
					}
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(eSoundBgmListDB.BGM_QUESTSELECT))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(eSoundBgmListDB.BGM_QUESTSELECT, 1f, 0, -1, -1);
					}
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsRequestAPIWhenEnterQuestScene)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestGetAll(new Action(this.OnResponse_QuestGetAll), false);
						this.m_Step = QuestState_Init.eStep.Request_Wait;
					}
					else
					{
						this.m_Step = QuestState_Init.eStep.Load_Wait;
					}
				}
				break;
			case QuestState_Init.eStep.Load_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = QuestState_Init.eStep.LoadNpcUI;
				}
				break;
			case QuestState_Init.eStep.LoadNpcUI:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.MasterDisplayUI, true);
				this.m_Step = QuestState_Init.eStep.LoadNpcUI_Wait;
				break;
			case QuestState_Init.eStep.LoadNpcUI_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.MasterDisplayUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.MasterDisplayUI);
					this.m_Step = QuestState_Init.eStep.LoadNpcUI_Setup;
				}
				break;
			case QuestState_Init.eStep.LoadNpcUI_Setup:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = QuestState_Init.eStep.End;
				}
				break;
			case QuestState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = QuestState_Init.eStep.None;
					this.m_Owner.PlayInBGFilter();
					if (this.m_NextState != 3)
					{
						this.m_Owner.MasterUI.PlayIn();
					}
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600159E RID: 5534 RVA: 0x00070888 File Offset: 0x0006EC88
		private bool SetupUI()
		{
			this.m_Owner.MasterUI = UIUtility.GetMenuComponent<MasterDisplayUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.MasterDisplayUI].m_SceneName);
			if (this.m_Owner.MasterUI == null)
			{
				return false;
			}
			this.m_Owner.MasterUI.Setup();
			return true;
		}

		// Token: 0x0600159F RID: 5535 RVA: 0x000708E2 File Offset: 0x0006ECE2
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x060015A0 RID: 5536 RVA: 0x000708EB File Offset: 0x0006ECEB
		private void OnResponse_QuestGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestChapterGetAll(new Action(this.OnResponse_QuestChapterGetAll), false);
		}

		// Token: 0x060015A1 RID: 5537 RVA: 0x0007090A File Offset: 0x0006ED0A
		private void OnResponse_QuestChapterGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsRequestAPIWhenEnterQuestScene = false;
			this.m_Step = QuestState_Init.eStep.Load_Wait;
		}

		// Token: 0x04001C5A RID: 7258
		private QuestState_Init.eStep m_Step = QuestState_Init.eStep.None;

		// Token: 0x0200045C RID: 1116
		private enum eStep
		{
			// Token: 0x04001C5C RID: 7260
			None = -1,
			// Token: 0x04001C5D RID: 7261
			First,
			// Token: 0x04001C5E RID: 7262
			Request_Wait,
			// Token: 0x04001C5F RID: 7263
			Load_Wait,
			// Token: 0x04001C60 RID: 7264
			LoadNpcUI,
			// Token: 0x04001C61 RID: 7265
			LoadNpcUI_Wait,
			// Token: 0x04001C62 RID: 7266
			LoadNpcUI_Setup,
			// Token: 0x04001C63 RID: 7267
			End
		}
	}
}
