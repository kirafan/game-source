﻿using System;

namespace Star
{
	// Token: 0x02000545 RID: 1349
	public class CActScriptKeyBind : IRoomScriptData
	{
		// Token: 0x06001AC1 RID: 6849 RVA: 0x0008ECA0 File Offset: 0x0008D0A0
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyBind actXlsKeyBind = (ActXlsKeyBind)pbase;
			this.m_BindHrcName = actXlsKeyBind.m_BindHrcName;
			this.m_TargetName = actXlsKeyBind.m_TargetName;
			this.m_Attach = actXlsKeyBind.m_Attach;
			this.m_LinkTime = (float)actXlsKeyBind.m_LinkTime / 30f;
			this.m_CalcType = actXlsKeyBind.m_CalcType;
			this.m_CRCKey = CRC32.Calc(this.m_BindHrcName);
		}

		// Token: 0x04002174 RID: 8564
		public string m_BindHrcName;

		// Token: 0x04002175 RID: 8565
		public string m_TargetName;

		// Token: 0x04002176 RID: 8566
		public CActScriptKeyBind.eNameAttach m_Attach;

		// Token: 0x04002177 RID: 8567
		public uint m_CRCKey;

		// Token: 0x04002178 RID: 8568
		public float m_LinkTime;

		// Token: 0x04002179 RID: 8569
		public CActScriptKeyBind.eCalc m_CalcType;

		// Token: 0x02000546 RID: 1350
		public enum eNameAttach
		{
			// Token: 0x0400217B RID: 8571
			Non,
			// Token: 0x0400217C RID: 8572
			R,
			// Token: 0x0400217D RID: 8573
			L
		}

		// Token: 0x02000547 RID: 1351
		public enum eCalc
		{
			// Token: 0x0400217F RID: 8575
			Chara,
			// Token: 0x04002180 RID: 8576
			Non,
			// Token: 0x04002181 RID: 8577
			LinkOffset,
			// Token: 0x04002182 RID: 8578
			ChrTrs,
			// Token: 0x04002183 RID: 8579
			LinkOffTrs
		}
	}
}
