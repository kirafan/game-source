﻿using System;

namespace Star
{
	// Token: 0x02000213 RID: 531
	[Serializable]
	public struct SoundCueSheetDB_Param
	{
		// Token: 0x04000D38 RID: 3384
		public string m_CueSheet;

		// Token: 0x04000D39 RID: 3385
		public string m_ACB;

		// Token: 0x04000D3A RID: 3386
		public string m_AWB;
	}
}
