﻿using System;

namespace Star
{
	// Token: 0x02000092 RID: 146
	[Serializable]
	public class BattleAIPatternChangeCondition
	{
		// Token: 0x040002A8 RID: 680
		public eBattleAIPatternChangeConditionType m_Type;

		// Token: 0x040002A9 RID: 681
		public float[] m_Args;
	}
}
