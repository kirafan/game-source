﻿using System;
using FieldPartyMemberResponseTypes;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000370 RID: 880
	public class FldPartyComAllDelete : IFldNetComModule
	{
		// Token: 0x060010A7 RID: 4263 RVA: 0x00057AA5 File Offset: 0x00055EA5
		public FldPartyComAllDelete() : base(IFldNetComManager.Instance)
		{
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x00057AB2 File Offset: 0x00055EB2
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x00057AC0 File Offset: 0x00055EC0
		public bool SetUp()
		{
			FieldPartyAPIGetAll phandle = new FieldPartyAPIGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x00057AF0 File Offset: 0x00055EF0
		private void CallbackSetUp(INetComHandle phandle)
		{
			FieldPartyMemberResponseTypes.GetAll getAll = phandle.GetResponse() as FieldPartyMemberResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_Table = new long[getAll.fieldPartyMembers.Length];
				for (int i = 0; i < getAll.fieldPartyMembers.Length; i++)
				{
					this.m_Table[i] = getAll.fieldPartyMembers[i].managedPartyMemberId;
				}
				this.m_Step = FldPartyComAllDelete.eStep.DeleteUp;
			}
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x00057B58 File Offset: 0x00055F58
		public bool SetUpObjList()
		{
			RoomComAPIObjGetAll phandle = new RoomComAPIObjGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackObjSetUp), false);
			return true;
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x00057B88 File Offset: 0x00055F88
		private void CallbackObjSetUp(INetComHandle phandle)
		{
			RoomObjectResponseTypes.GetAll getAll = phandle.GetResponse() as RoomObjectResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_Table = new long[getAll.managedRoomObjects.Length];
				for (int i = 0; i < getAll.managedRoomObjects.Length; i++)
				{
					this.m_Table[i] = getAll.managedRoomObjects[i].managedRoomObjectId;
				}
				this.m_Step = FldPartyComAllDelete.eStep.DeleteObjUp;
			}
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x00057BF0 File Offset: 0x00055FF0
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case FldPartyComAllDelete.eStep.GetState:
				this.SetUp();
				this.m_Step = FldPartyComAllDelete.eStep.WaitCheck;
				break;
			case FldPartyComAllDelete.eStep.DeleteUp:
				this.m_Step = FldPartyComAllDelete.eStep.SetUpCheck;
				if (!this.DefaultDeleteManage())
				{
					this.m_Step = FldPartyComAllDelete.eStep.End;
				}
				break;
			case FldPartyComAllDelete.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x00057C6C File Offset: 0x0005606C
		public bool DefaultDeleteManage()
		{
			FieldPartyAPIRemoveAll fieldPartyAPIRemoveAll = new FieldPartyAPIRemoveAll();
			fieldPartyAPIRemoveAll.managedPartyMemberIds = new long[this.m_Table.Length];
			for (int i = 0; i < this.m_Table.Length; i++)
			{
				fieldPartyAPIRemoveAll.managedPartyMemberIds[i] = this.m_Table[i];
			}
			this.m_DelStep++;
			this.m_NetMng.SendCom(fieldPartyAPIRemoveAll, new INetComHandle.ResponseCallbak(this.CallbackDeleteUp), false);
			return true;
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x00057CE3 File Offset: 0x000560E3
		private void CallbackDeleteUp(INetComHandle phandle)
		{
			this.m_Step = FldPartyComAllDelete.eStep.DeleteUp;
		}

		// Token: 0x04001798 RID: 6040
		private FldPartyComAllDelete.eStep m_Step;

		// Token: 0x04001799 RID: 6041
		private long[] m_Table;

		// Token: 0x0400179A RID: 6042
		private int m_DelStep;

		// Token: 0x02000371 RID: 881
		public enum eStep
		{
			// Token: 0x0400179C RID: 6044
			GetState,
			// Token: 0x0400179D RID: 6045
			WaitCheck,
			// Token: 0x0400179E RID: 6046
			DeleteUp,
			// Token: 0x0400179F RID: 6047
			SetUpCheck,
			// Token: 0x040017A0 RID: 6048
			GetObjState,
			// Token: 0x040017A1 RID: 6049
			DeleteObjUp,
			// Token: 0x040017A2 RID: 6050
			End
		}
	}
}
