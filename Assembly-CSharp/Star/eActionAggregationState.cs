﻿using System;

namespace Star
{
	// Token: 0x0200007E RID: 126
	public enum eActionAggregationState
	{
		// Token: 0x04000222 RID: 546
		Wait,
		// Token: 0x04000223 RID: 547
		Play,
		// Token: 0x04000224 RID: 548
		Finish
	}
}
