﻿using System;

namespace Star
{
	// Token: 0x020000EA RID: 234
	[Serializable]
	public class BattleSystemDataForPL
	{
		// Token: 0x04000567 RID: 1383
		public CharacterBattleParam Param;

		// Token: 0x04000568 RID: 1384
		public BattleCommandData[] Cmds;
	}
}
