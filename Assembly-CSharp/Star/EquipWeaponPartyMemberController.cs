﻿using System;

namespace Star
{
	// Token: 0x020002CE RID: 718
	public abstract class EquipWeaponPartyMemberController : EquipWeaponCharacterDataController
	{
		// Token: 0x06000DF9 RID: 3577 RVA: 0x0004D460 File Offset: 0x0004B860
		public EquipWeaponPartyMemberController(eCharacterDataControllerType in_type) : base(in_type)
		{
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x0004D469 File Offset: 0x0004B869
		protected EquipWeaponPartyMemberController SetupBase(int in_partyIndex, int in_partySlotIndex)
		{
			this.SetIndices(in_partyIndex, in_partySlotIndex);
			return this;
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x0004D474 File Offset: 0x0004B874
		protected EquipWeaponCharacterDataController ApplyCharacterData(CharacterDataWrapperBase in_memberData)
		{
			this.memberData = in_memberData;
			return this;
		}

		// Token: 0x06000DFC RID: 3580
		protected abstract EquipWeaponPartyMemberController ApplyBase(int in_partyIndex, int in_partySlotIndex);

		// Token: 0x06000DFD RID: 3581 RVA: 0x0004D47E File Offset: 0x0004B87E
		private void SetIndices(int in_partyIndex, int in_partySlotIndex)
		{
			this.partyIndex = in_partyIndex;
			this.partySlotIndex = in_partySlotIndex;
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x0004D48E File Offset: 0x0004B88E
		public virtual void Refresh()
		{
			this.ApplyBase(this.partyIndex, this.partySlotIndex);
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x0004D4A3 File Offset: 0x0004B8A3
		public int GetPartyIndex()
		{
			return this.partyIndex;
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x0004D4AB File Offset: 0x0004B8AB
		public int GetPartySlotIndex()
		{
			return this.partySlotIndex;
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x0004D4B3 File Offset: 0x0004B8B3
		protected override CharacterDataWrapperBase GetDataBaseInternal()
		{
			return this.memberData;
		}

		// Token: 0x04001592 RID: 5522
		private int partyIndex;

		// Token: 0x04001593 RID: 5523
		private int partySlotIndex;

		// Token: 0x04001594 RID: 5524
		private CharacterDataWrapperBase memberData;
	}
}
