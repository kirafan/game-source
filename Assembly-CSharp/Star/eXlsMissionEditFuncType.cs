﻿using System;

namespace Star
{
	// Token: 0x020004F2 RID: 1266
	public enum eXlsMissionEditFuncType
	{
		// Token: 0x04001FBB RID: 8123
		BattlePartyEdit,
		// Token: 0x04001FBC RID: 8124
		SupportPartyEdit,
		// Token: 0x04001FBD RID: 8125
		CharaStorength,
		// Token: 0x04001FBE RID: 8126
		CharaLimitBreak,
		// Token: 0x04001FBF RID: 8127
		CharaEvelution,
		// Token: 0x04001FC0 RID: 8128
		WeaponMake,
		// Token: 0x04001FC1 RID: 8129
		WeaponStorength,
		// Token: 0x04001FC2 RID: 8130
		WeaponEquipment
	}
}
