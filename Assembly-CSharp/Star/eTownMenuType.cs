﻿using System;

namespace Star
{
	// Token: 0x0200070D RID: 1805
	public enum eTownMenuType
	{
		// Token: 0x04002A9E RID: 10910
		Quest = 1200,
		// Token: 0x04002A9F RID: 10911
		Edit,
		// Token: 0x04002AA0 RID: 10912
		Gacha,
		// Token: 0x04002AA1 RID: 10913
		Traning,
		// Token: 0x04002AA2 RID: 10914
		Shop,
		// Token: 0x04002AA3 RID: 10915
		Mission,
		// Token: 0x04002AA4 RID: 10916
		Max
	}
}
