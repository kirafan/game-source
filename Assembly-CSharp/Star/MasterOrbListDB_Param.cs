﻿using System;

namespace Star
{
	// Token: 0x020001D0 RID: 464
	[Serializable]
	public struct MasterOrbListDB_Param
	{
		// Token: 0x04000B03 RID: 2819
		public int m_ID;

		// Token: 0x04000B04 RID: 2820
		public string m_Name;

		// Token: 0x04000B05 RID: 2821
		public int m_Class;

		// Token: 0x04000B06 RID: 2822
		public sbyte m_ExpTableID;

		// Token: 0x04000B07 RID: 2823
		public int m_LimitLv;

		// Token: 0x04000B08 RID: 2824
		public int[] m_SkillIDs;

		// Token: 0x04000B09 RID: 2825
		public int[] m_SkillAavailableLvs;

		// Token: 0x04000B0A RID: 2826
		public string[] m_CueNames;
	}
}
