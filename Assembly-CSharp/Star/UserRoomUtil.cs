﻿using System;
using System.Text;
using Newtonsoft.Json;
using WWWTypes;

namespace Star
{
	// Token: 0x020003A8 RID: 936
	public class UserRoomUtil
	{
		// Token: 0x060011AA RID: 4522 RVA: 0x0005D420 File Offset: 0x0005B820
		public static UserRoomData.PlacementData GetCategoryToPlacementData(UserRoomData pbase, eRoomObjectCategory fcategory)
		{
			int placementNum = pbase.GetPlacementNum();
			for (int i = 0; i < placementNum; i++)
			{
				if (RoomObjectListUtil.GetIDToCategory(pbase.GetPlacementAt(i).m_ObjectID) == fcategory)
				{
					return pbase.GetPlacementAt(i);
				}
			}
			return null;
		}

		// Token: 0x060011AB RID: 4523 RVA: 0x0005D468 File Offset: 0x0005B868
		public static void ClearRoomData()
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			userDataMng.UserRoomListData.ClearList();
		}

		// Token: 0x060011AC RID: 4524 RVA: 0x0005D48C File Offset: 0x0005B88C
		public static void ClearRoomObjData()
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			int count = userDataMng.UserRoomObjDatas.Count;
			for (int i = count - 1; i >= 0; i--)
			{
				userDataMng.UserRoomObjDatas.RemoveAt(i);
			}
		}

		// Token: 0x060011AD RID: 4525 RVA: 0x0005D4D0 File Offset: 0x0005B8D0
		public static UserRoomData GetManageIDToUserRoomData(long fmanageid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			return userDataMng.UserRoomListData.GetRoomData(fmanageid);
		}

		// Token: 0x060011AE RID: 4526 RVA: 0x0005D4F4 File Offset: 0x0005B8F4
		public static PlayerRoomArrangement[] CreateRoomDataString(UserRoomData proom)
		{
			PlayerRoomArrangement[] array = new PlayerRoomArrangement[proom.GetPlacementNum()];
			int placementNum = proom.GetPlacementNum();
			for (int i = 0; i < placementNum; i++)
			{
				UserRoomData.PlacementData placementAt = proom.GetPlacementAt(i);
				array[i] = new PlayerRoomArrangement();
				array[i].managedRoomObjectId = placementAt.m_ManageID;
				array[i].x = placementAt.m_X;
				array[i].y = placementAt.m_Y;
				array[i].roomNo = placementAt.m_RoomNo;
				array[i].dir = (int)placementAt.m_Dir;
				array[i].roomObjectId = placementAt.m_ObjectID;
			}
			return array;
		}

		// Token: 0x060011AF RID: 4527 RVA: 0x0005D58C File Offset: 0x0005B98C
		public static RoomServerForm.RoomList BuildRoomList(RoomServerForm.DataChunk pchunk)
		{
			if (pchunk.m_DataType == 0)
			{
				string @string = Encoding.ASCII.GetString(pchunk.m_Data);
				return JsonConvert.DeserializeObject<RoomServerForm.RoomList>(@string);
			}
			return null;
		}

		// Token: 0x060011B0 RID: 4528 RVA: 0x0005D5C0 File Offset: 0x0005B9C0
		public static UserRoomObjectData GetManageIDToUserRoomObjData(long fmanageid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < userDataMng.UserRoomObjDatas.Count; i++)
			{
				if (userDataMng.UserRoomObjDatas[i].IsLinkMngID(fmanageid))
				{
					return userDataMng.UserRoomObjDatas[i];
				}
			}
			return null;
		}

		// Token: 0x060011B1 RID: 4529 RVA: 0x0005D61C File Offset: 0x0005BA1C
		public static UserRoomObjectData GetAccessIDToUserRoomObjData(int faccesskey)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < userDataMng.UserRoomObjDatas.Count; i++)
			{
				if (userDataMng.UserRoomObjDatas[i].ResourceID == faccesskey)
				{
					return userDataMng.UserRoomObjDatas[i];
				}
			}
			return null;
		}

		// Token: 0x060011B2 RID: 4530 RVA: 0x0005D675 File Offset: 0x0005BA75
		public static UserRoomObjectData GetUserRoomObjData(eRoomObjectCategory objCategory, int objID)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData(objCategory, objID);
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x0005D688 File Offset: 0x0005BA88
		public static int GetUserRoomObjHaveNum()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjHaveNum();
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x0005D699 File Offset: 0x0005BA99
		public static int GetUserRoomObjPlacementNum()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjPlacementNum();
		}

		// Token: 0x060011B5 RID: 4533 RVA: 0x0005D6AA File Offset: 0x0005BAAA
		public static int GetUserRoomObjRemainNum()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjRemainNum();
		}

		// Token: 0x060011B6 RID: 4534 RVA: 0x0005D6BC File Offset: 0x0005BABC
		public static void ReleaseCheckRoomData(long fkeycode)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng != null)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				userDataMng.UserRoomListData.ReleasePlayerFloorList(fkeycode);
			}
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x0005D700 File Offset: 0x0005BB00
		public static void ReleaseUserRoomObjData(long fmanageid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < userDataMng.UserRoomObjDatas.Count; i++)
			{
				if (userDataMng.UserRoomObjDatas[i].IsLinkMngID(fmanageid))
				{
					if (userDataMng.UserRoomObjDatas[i].ReleaseLinkMngID(fmanageid))
					{
						userDataMng.UserRoomObjDatas.RemoveAt(i);
					}
					break;
				}
			}
		}

		// Token: 0x060011B8 RID: 4536 RVA: 0x0005D774 File Offset: 0x0005BB74
		public static long GetActiveRoomMngID(long fkeycode)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			long result = -1L;
			int playerRoomListNum = userDataMng.UserRoomListData.GetPlayerRoomListNum();
			for (int i = 0; i < playerRoomListNum; i++)
			{
				UserRoomListData.PlayerFloorState playerRoomListAt = userDataMng.UserRoomListData.GetPlayerRoomListAt(i);
				if (playerRoomListAt.m_PlayerID == fkeycode)
				{
					result = playerRoomListAt.m_ActiveRoomMngID;
					break;
				}
			}
			return result;
		}

		// Token: 0x060011B9 RID: 4537 RVA: 0x0005D7D8 File Offset: 0x0005BBD8
		public static bool IsHaveSubRoom(long fplayerid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			int playerRoomListNum = userDataMng.UserRoomListData.GetPlayerRoomListNum();
			for (int i = 0; i < playerRoomListNum; i++)
			{
				UserRoomListData.PlayerFloorState playerRoomListAt = userDataMng.UserRoomListData.GetPlayerRoomListAt(i);
				if (playerRoomListAt.m_PlayerID == fplayerid)
				{
					UserRoomListData.PlayerFloorState.FloorData floorGroupData = playerRoomListAt.GetFloorGroupData(playerRoomListAt.m_ActiveRoomMngID);
					if (floorGroupData != null)
					{
						return floorGroupData.m_State.Count > 1;
					}
				}
			}
			return false;
		}

		// Token: 0x060011BA RID: 4538 RVA: 0x0005D850 File Offset: 0x0005BC50
		public static bool SetRoomChange(long fplayerid)
		{
			bool result = false;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			int playerRoomListNum = userDataMng.UserRoomListData.GetPlayerRoomListNum();
			for (int i = 0; i < playerRoomListNum; i++)
			{
				UserRoomListData.PlayerFloorState playerRoomListAt = userDataMng.UserRoomListData.GetPlayerRoomListAt(i);
				if (playerRoomListAt.m_PlayerID == fplayerid)
				{
					long activeRoomMngID = playerRoomListAt.m_ActiveRoomMngID;
					UserRoomListData.PlayerFloorState.FloorData floorGroupData = playerRoomListAt.GetFloorGroupData(activeRoomMngID);
					if (floorGroupData != null)
					{
						for (int j = 0; j < floorGroupData.m_State.Count; j++)
						{
							if (floorGroupData.m_State[j].MngID != activeRoomMngID)
							{
								playerRoomListAt.m_ActiveRoomMngID = floorGroupData.m_State[j].MngID;
								result = true;
							}
						}
					}
					break;
				}
			}
			return result;
		}
	}
}
