﻿using System;

namespace Star
{
	// Token: 0x020001A8 RID: 424
	public static class SkillExpDB_Ext
	{
		// Token: 0x06000B17 RID: 2839 RVA: 0x00042088 File Offset: 0x00040488
		public static int GetNextExp(this SkillExpDB self, int currentLv, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			int num2 = num;
			for (int j = num; j < self.m_Params.Length; j++)
			{
				if (self.m_Params[j].m_NextExp == 0U)
				{
					num2 = j;
					break;
				}
				if (self.m_Params[j].m_ID != (int)expTableID)
				{
					num2 = j - 1;
					break;
				}
			}
			if (num >= 0)
			{
				int num3 = num + currentLv - 1;
				if (num3 >= 0 && num3 <= num2)
				{
					return (int)self.m_Params[num3].m_NextExp;
				}
			}
			return 0;
		}

		// Token: 0x06000B18 RID: 2840 RVA: 0x0004215C File Offset: 0x0004055C
		public static long GetNeedRemainExp(this SkillExpDB self, int currentLv, long totalUseNum, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				long[] nextMaxExps = self.GetNextMaxExps(1, currentLv, expTableID);
				if (nextMaxExps != null)
				{
					long num2 = 0L;
					for (int j = 0; j < nextMaxExps.Length; j++)
					{
						num2 += nextMaxExps[j];
					}
					long num3 = num2 - totalUseNum;
					if (num3 < 0L)
					{
						num3 = 0L;
					}
					return num3;
				}
			}
			return 0L;
		}

		// Token: 0x06000B19 RID: 2841 RVA: 0x000421F4 File Offset: 0x000405F4
		public static long[] GetNextMaxExps(this SkillExpDB self, int lv_a, int lv_b, sbyte expTableID)
		{
			int num = lv_b - lv_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(lv_a + i, expTableID);
			}
			return array;
		}

		// Token: 0x06000B1A RID: 2842 RVA: 0x00042238 File Offset: 0x00040638
		public static int GetMaxLv(this SkillExpDB self, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				for (int j = num; j < self.m_Params.Length; j++)
				{
					if (self.m_Params[j].m_NextExp == 0U && self.m_Params[j].m_ID == (int)expTableID)
					{
						return self.m_Params[j].m_Lv;
					}
				}
			}
			return 1;
		}
	}
}
