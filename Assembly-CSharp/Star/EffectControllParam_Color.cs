﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002EF RID: 751
	[Serializable]
	public class EffectControllParam_Color : EffectControllParam
	{
		// Token: 0x06000E91 RID: 3729 RVA: 0x0004E2A0 File Offset: 0x0004C6A0
		public override void Apply(int index, EffectHandler effHndl)
		{
			if (index < 0 || index >= this.m_Args.Length)
			{
				return;
			}
			EffectControllParam_Color.Arg arg = this.m_Args[index];
			if (effHndl.m_MsbHndl != null)
			{
				for (int i = 0; i < arg.m_Indices.Length; i++)
				{
					int num = arg.m_Indices[i];
					MsbObjectHandler msbObjectHandlerByName = effHndl.m_MsbHndl.GetMsbObjectHandlerByName(this.m_TargetGoPaths[num]);
					MsbObjectHandler.MsbObjectParam work = msbObjectHandlerByName.GetWork();
					if (work != null)
					{
						work.m_MeshColor = new Color(arg.m_Color.r, arg.m_Color.g, arg.m_Color.b, work.m_MeshColor.a);
					}
				}
			}
		}

		// Token: 0x040015AF RID: 5551
		public EffectControllParam_Color.Arg[] m_Args;

		// Token: 0x020002F0 RID: 752
		[Serializable]
		public class Arg
		{
			// Token: 0x040015B0 RID: 5552
			public int[] m_Indices;

			// Token: 0x040015B1 RID: 5553
			public Color m_Color;
		}
	}
}
