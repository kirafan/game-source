﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

namespace Star
{
	// Token: 0x020005D6 RID: 1494
	public class RoomObjectModel : IRoomObjectControll
	{
		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06001D48 RID: 7496 RVA: 0x00098CFD File Offset: 0x000970FD
		public int SubKeyID
		{
			get
			{
				return this.m_SubKeyID;
			}
		}

		// Token: 0x06001D49 RID: 7497 RVA: 0x00098D08 File Offset: 0x00097108
		public void SetupObj(eRoomObjectCategory category, int objID, CharacterDefine.eDir dir, int posX, int posY, int floorID)
		{
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_Category = category;
			this.m_ObjID = objID;
			this.m_FloorID = floorID;
			this.m_DefaultSizeX = objectParam.m_SizeX;
			this.m_DefaultSizeY = objectParam.m_SizeY;
			this.m_BlockData = new RoomBlockData((float)posX, (float)posY, objectParam.m_SizeX, objectParam.m_SizeY);
			this.m_BlockData.SetLinkObject(this);
			this.m_HitActionDir.m_Mask = 0U;
			this.m_HitActionDir.m_Up = (byte)objectParam.m_SearchDown;
			this.m_HitActionDir.m_Left = (byte)objectParam.m_SearchRight;
			this.m_HitActionDir.m_Down = (byte)objectParam.m_SearchUp;
			this.m_HitActionDir.m_Right = (byte)objectParam.m_SearchLeft;
			this.m_OptionFlag = objectParam.m_Flag;
			base.SetDir(dir);
			this.m_Action = new RoomPartsActionPakage();
			this.m_RoomObjResource = new RoomObjectResModel();
			this.m_RoomObjResource.Setup(this);
			this.m_SortCompKey = 0;
			this.m_ObjGroupKey = 1;
			this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingOrderModel);
			if ((this.m_OptionFlag & 1) == 0)
			{
				this.m_ObjGroupKey = 2;
				this.UpGridState = new IRoomObjectControll.GridStateFunc(this.UpGridStateGroupUp);
				this.m_SortCompKey = 10;
			}
			this.m_MessageIDCheck = 0;
			if ((this.m_OptionFlag & 8) != 0 && objectParam.m_ObjEventArgs.Length >= 2)
			{
				this.m_MessageIDCheck = objectParam.m_ObjEventArgs[1];
			}
		}

		// Token: 0x06001D4A RID: 7498 RVA: 0x00098E88 File Offset: 0x00097288
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			if (this.m_SelectingMode != 0)
			{
				return false;
			}
			int num = gridX - this.m_BlockData.PosX;
			int num2 = gridY - this.m_BlockData.PosY;
			bool result = false;
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				switch (fdir)
				{
				case 0:
					if (num <= this.m_BlockData.SizeX && num2 == this.m_BlockData.SizeY - 1)
					{
						num = 1 << this.m_BlockData.SizeX - num;
						if (((int)this.m_HitActionDir.m_Down & num) != 0)
						{
							result = true;
						}
					}
					break;
				case 1:
					if (num == 0 && num2 <= this.m_BlockData.SizeY)
					{
						num2 = 1 << this.m_BlockData.SizeY - num2;
						if (((int)this.m_HitActionDir.m_Right & num2) != 0)
						{
							result = true;
						}
					}
					break;
				case 2:
					if (num >= -1 && num2 == 0)
					{
						num = 1 << num + 1;
						if (((int)this.m_HitActionDir.m_Up & num) != 0)
						{
							result = true;
						}
					}
					break;
				case 3:
					if (num == this.m_BlockData.SizeY - 1 && num2 >= -1)
					{
						num2 = 1 << num2 + 1;
						if (((int)this.m_HitActionDir.m_Left & num2) != 0)
						{
							result = true;
						}
					}
					break;
				}
			}
			else
			{
				switch (fdir)
				{
				case 0:
					if (num >= -1 && num2 == this.m_BlockData.SizeY - 1)
					{
						num = 1 << num + 1;
						if (((int)this.m_HitActionDir.m_Left & num) != 0)
						{
							result = true;
						}
					}
					break;
				case 1:
					if (num == 0 && num2 >= -1)
					{
						num2 = 1 << num2 + 1;
						if (((int)this.m_HitActionDir.m_Up & num2) != 0)
						{
							result = true;
						}
					}
					break;
				case 2:
					if (num <= this.m_BlockData.SizeX && num2 == 0)
					{
						num = 1 << this.m_BlockData.SizeX - num;
						if (((int)this.m_HitActionDir.m_Right & num) != 0)
						{
							result = true;
						}
					}
					break;
				case 3:
					if (num == this.m_BlockData.SizeY - 1 && num2 <= this.m_BlockData.SizeY)
					{
						num2 = 1 << this.m_BlockData.SizeY - num2;
						if (((int)this.m_HitActionDir.m_Down & num2) != 0)
						{
							result = true;
						}
					}
					break;
				}
			}
			return result;
		}

		// Token: 0x06001D4B RID: 7499 RVA: 0x00099103 File Offset: 0x00097503
		public override void UpdateObj()
		{
			this.m_Action.PakageUpdate();
		}

		// Token: 0x06001D4C RID: 7500 RVA: 0x00099110 File Offset: 0x00097510
		public override void Destroy()
		{
			if (this.m_Obj != null)
			{
				ObjectResourceManager.ObjectDestroy(this.m_Obj, true);
				this.m_Obj = null;
			}
			this.m_MabMap = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			if (this.m_DefaultConfig != null)
			{
				this.m_DefaultConfig = null;
			}
			base.Destroy();
		}

		// Token: 0x06001D4D RID: 7501 RVA: 0x00099170 File Offset: 0x00097570
		public override void SetBaseColor(Color fcolor)
		{
			this.m_BaseColor = fcolor;
			if (this.m_MsbHndl != null)
			{
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				for (int i = 0; i < msbObjectHandlerNum; i++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
					msbObjectHandler.GetWork().m_MeshColor = fcolor;
				}
			}
		}

		// Token: 0x06001D4E RID: 7502 RVA: 0x000991CC File Offset: 0x000975CC
		public void AttachModel(GameObject modelObj)
		{
			if (modelObj != null)
			{
				this.m_Obj = modelObj;
				this.CheckModelDirFunc();
				this.LinkObjectNode();
				Renderer[] componentsInChildren = modelObj.GetComponentsInChildren<Renderer>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].receiveShadows = false;
					componentsInChildren[i].shadowCastingMode = ShadowCastingMode.Off;
				}
				this.MakeColliderLink(this.m_Obj);
				base.SetDir(this.m_Dir);
				Transform meigeControllNode = RoomUtility.GetMeigeControllNode(this.m_Obj);
				this.m_MeigeAnimCtrl = meigeControllNode.gameObject.AddComponent<MeigeAnimCtrl>();
				this.m_MsbHndl = meigeControllNode.gameObject.GetComponent<MsbHandler>();
				this.m_MabMap = this.m_Obj.GetComponent<McatFileHelper>();
				Transform component = this.m_Obj.GetComponent<Transform>();
				component.rotation = Quaternion.Euler(0f, 180f, 0f);
				component.SetParent(this.m_OwnerTrs, false);
				this.m_MsbHndl.enabled = true;
				Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
				int num = 50000;
				int num2 = 0;
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				MsbObjectHandler[] array = new MsbObjectHandler[msbObjectHandlerNum];
				for (int j = 0; j < msbObjectHandlerNum; j++)
				{
					array[j] = this.m_MsbHndl.GetMsbObjectHandler(j);
					if (array[j].GetWork() != null)
					{
						int renderOrder = array[j].GetWork().m_RenderOrder;
						if (num > renderOrder)
						{
							num = renderOrder;
						}
						if (num2 < renderOrder)
						{
							num2 = renderOrder;
						}
					}
				}
				this.m_DefaultConfig = new IRoomObjectControll.ModelRenderConfig[rendererCache.Length];
				for (int k = 0; k < rendererCache.Length; k++)
				{
					for (int j = 0; j < msbObjectHandlerNum; j++)
					{
						if (rendererCache[k].name == array[j].m_Name)
						{
							if (array[j].GetWork() != null)
							{
								this.m_DefaultConfig[k].m_RenderSort = (short)(array[j].GetWork().m_RenderOrder - num + rendererCache[k].sortingOrder);
								this.m_DefaultConfig[k].m_RenderObject = rendererCache[k];
							}
							break;
						}
					}
				}
				for (int k = 0; k < rendererCache.Length; k++)
				{
					Vector3 localPosition = rendererCache[k].transform.localPosition;
					this.m_DefaultConfig[k].m_OffsetZ = (localPosition.z = (float)this.m_DefaultConfig[k].m_RenderSort * 0.1f);
					rendererCache[k].transform.localPosition = localPosition;
				}
				if ((this.m_OptionFlag & 1) == 0)
				{
					this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingNonOrderModel);
				}
				this.SetBaseColor(this.m_BaseColor);
			}
		}

		// Token: 0x06001D4F RID: 7503 RVA: 0x000994A4 File Offset: 0x000978A4
		public virtual void SetUpAnimation()
		{
		}

		// Token: 0x06001D50 RID: 7504 RVA: 0x000994A8 File Offset: 0x000978A8
		public void MakeColliderLink(GameObject ptarget)
		{
			Collider[] componentsInChildren = ptarget.GetComponentsInChildren<Collider>();
			if (componentsInChildren != null && componentsInChildren.Length > 0)
			{
				int num = componentsInChildren.Length;
				for (int i = 0; i < num; i++)
				{
					RoomHitConnect roomHitConnect = componentsInChildren[i].gameObject.AddComponent<RoomHitConnect>();
					roomHitConnect.m_Link = this;
					roomHitConnect.enabled = false;
				}
			}
		}

		// Token: 0x06001D51 RID: 7505 RVA: 0x00099500 File Offset: 0x00097900
		public void SortingOrderModel(float flayerpos, int sortingOrder)
		{
			Vector3 localPosition = this.m_OwnerTrs.localPosition;
			localPosition.z = flayerpos;
			this.m_OwnerTrs.localPosition = localPosition;
			if (this.m_MsbHndl != null)
			{
				for (int i = 0; i < this.m_DefaultConfig.Length; i++)
				{
					if (this.m_DefaultConfig[i].m_RenderObject != null)
					{
						this.m_DefaultConfig[i].m_RenderObject.sortingOrder = sortingOrder + (int)this.m_DefaultConfig[i].m_RenderSort;
					}
				}
			}
		}

		// Token: 0x06001D52 RID: 7506 RVA: 0x0009959C File Offset: 0x0009799C
		public void SortingNonOrderModel(float flayerpos, int sortingOrder)
		{
			if (this.m_HitMarkObj == null)
			{
				Vector3 localPosition = this.m_OwnerTrs.localPosition;
				localPosition.z = 0.9f;
				this.m_OwnerTrs.localPosition = localPosition;
				if (this.m_MsbHndl != null)
				{
					for (int i = 0; i < this.m_DefaultConfig.Length; i++)
					{
						if (this.m_DefaultConfig[i].m_RenderObject != null)
						{
							this.m_DefaultConfig[i].m_RenderObject.sortingOrder = sortingOrder + (int)this.m_DefaultConfig[i].m_RenderSort + -900;
						}
					}
				}
			}
			else
			{
				this.SortingOrderModel(flayerpos, sortingOrder);
			}
		}

		// Token: 0x06001D53 RID: 7507 RVA: 0x00099660 File Offset: 0x00097A60
		public override IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			IRoomObjectControll.ModelRenderConfig[] array = new IRoomObjectControll.ModelRenderConfig[this.m_DefaultConfig.Length];
			for (int i = 0; i < this.m_DefaultConfig.Length; i++)
			{
				array[i].m_RenderSort = this.m_DefaultConfig[i].m_RenderSort;
				array[i].m_OffsetZ = this.m_DefaultConfig[i].m_OffsetZ;
				array[i].m_RenderObject = this.m_DefaultConfig[i].m_RenderObject;
			}
			return array;
		}

		// Token: 0x06001D54 RID: 7508 RVA: 0x000996EC File Offset: 0x00097AEC
		public override void SetSortKey(string ptargetname, int fsortkey)
		{
			if (this.m_DefaultConfig != null)
			{
				for (int i = 0; i < this.m_DefaultConfig.Length; i++)
				{
					if (this.m_DefaultConfig[i].m_RenderObject != null && this.m_DefaultConfig[i].m_RenderObject.name == ptargetname)
					{
						this.m_DefaultConfig[i].m_RenderSort = (short)fsortkey;
						break;
					}
				}
			}
		}

		// Token: 0x06001D55 RID: 7509 RVA: 0x00099774 File Offset: 0x00097B74
		public override void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
			if (this.m_DefaultConfig != null)
			{
				for (int i = 0; i < this.m_DefaultConfig.Length; i++)
				{
					if (this.m_DefaultConfig[i].m_RenderObject != null)
					{
						this.m_DefaultConfig[i].m_RenderSort = psort[i].m_RenderSort;
						this.m_DefaultConfig[i].m_RenderObject = psort[i].m_RenderObject;
						Vector3 localPosition = this.m_DefaultConfig[i].m_RenderObject.transform.localPosition;
						this.m_DefaultConfig[i].m_OffsetZ = (localPosition.z = (float)this.m_DefaultConfig[i].m_RenderSort * 0.1f);
						this.m_DefaultConfig[i].m_RenderObject.transform.localPosition = localPosition;
					}
				}
			}
		}

		// Token: 0x06001D56 RID: 7510 RVA: 0x00099866 File Offset: 0x00097C66
		public void ClearAnimCtrl()
		{
			this.m_MeigeAnimCtrl = null;
		}

		// Token: 0x06001D57 RID: 7511 RVA: 0x0009986F File Offset: 0x00097C6F
		public void AnimCtrlOpen()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Open();
			}
		}

		// Token: 0x06001D58 RID: 7512 RVA: 0x0009988D File Offset: 0x00097C8D
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder)
		{
			if (this.m_MeigeAnimCtrl != null && meigeAnimClipHolder != null)
			{
				this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, meigeAnimClipHolder);
			}
		}

		// Token: 0x06001D59 RID: 7513 RVA: 0x000998BF File Offset: 0x00097CBF
		public void AnimCtrlClose()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Close();
			}
		}

		// Token: 0x06001D5A RID: 7514 RVA: 0x000998E0 File Offset: 0x00097CE0
		public override void PlayMotion(int fmotionid, WrapMode fmode, float fspeed)
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.SetAnimationPlaySpeed(fspeed);
				if (this.m_MabMap == null)
				{
					this.m_MeigeAnimCtrl.Play("event_001", 0f, fmode);
				}
				else
				{
					for (int i = 0; i < this.m_MabMap.m_Table.Length; i++)
					{
						if (this.m_MabMap.m_Table[i].m_AccessKey == fmotionid)
						{
							this.m_MeigeAnimCtrl.Play(this.m_MabMap.m_Table[i].m_AnimeName, 0f, fmode);
							break;
						}
					}
				}
			}
		}

		// Token: 0x06001D5B RID: 7515 RVA: 0x0009999C File Offset: 0x00097D9C
		public void PlayAnim(string actionKey, WrapMode wrapMode)
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Play(actionKey, 0f, wrapMode);
			}
		}

		// Token: 0x06001D5C RID: 7516 RVA: 0x000999C4 File Offset: 0x00097DC4
		private void CheckModelDirFunc()
		{
			bool flag = false;
			int num = 0;
			int num2 = 0;
			Renderer[] componentsInChildren = this.m_Obj.GetComponentsInChildren<Renderer>();
			if (componentsInChildren.Length >= 2)
			{
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					if (componentsInChildren[i].name.IndexOf("base_l") >= 0)
					{
						num2++;
					}
					if (componentsInChildren[i].name.IndexOf("base_r") >= 0)
					{
						num++;
					}
				}
				if (num != 0 && num2 != 0)
				{
					flag = true;
				}
			}
			this.m_UpDirFunc = new IRoomObjectControll.UpdateDirFunc(this.UpDirToScale);
			if (flag)
			{
				this.m_DirMeshGroup = new RoomObjectModel.RoomModelGroup[2];
				this.m_UpDirFunc = new IRoomObjectControll.UpdateDirFunc(this.UpDirToModelSwap);
				this.m_DirMeshGroup[0].m_GroupModel = new GameObject[num];
				this.m_DirMeshGroup[1].m_GroupModel = new GameObject[num2];
				num = 0;
				num2 = 0;
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					if (componentsInChildren[i].name.IndexOf("base_l") >= 0)
					{
						this.m_DirMeshGroup[0].m_GroupModel[num2] = componentsInChildren[i].gameObject;
						num2++;
					}
					if (componentsInChildren[i].name.IndexOf("base_r") >= 0)
					{
						this.m_DirMeshGroup[1].m_GroupModel[num] = componentsInChildren[i].gameObject;
						num++;
					}
				}
			}
		}

		// Token: 0x06001D5D RID: 7517 RVA: 0x00099B3C File Offset: 0x00097F3C
		public void UpDirToScale()
		{
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				this.m_Obj.transform.localScale = new Vector3(1f, 1f, 1f);
			}
			else
			{
				this.m_Obj.transform.localScale = new Vector3(-1f, 1f, 1f);
			}
			if (this.m_HitMarkObj != null)
			{
				this.m_HitMarkObj.transform.localScale = this.m_Obj.transform.localScale;
			}
		}

		// Token: 0x06001D5E RID: 7518 RVA: 0x00099BD4 File Offset: 0x00097FD4
		public void UpDirToModelSwap()
		{
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				this.m_DirMeshGroup[0].SetView(false);
				this.m_DirMeshGroup[1].SetView(true);
				if (this.m_HitMarkObj != null)
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(1f, 1f, 1f);
				}
			}
			else
			{
				this.m_DirMeshGroup[0].SetView(true);
				this.m_DirMeshGroup[1].SetView(false);
				if (this.m_HitMarkObj != null)
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(-1f, 1f, 1f);
				}
			}
		}

		// Token: 0x06001D5F RID: 7519 RVA: 0x00099CA4 File Offset: 0x000980A4
		public override void SetSeleting(bool flg, bool isDisable = false)
		{
			if (flg)
			{
				if (isDisable)
				{
					this.SelectingDisableOn();
				}
				else
				{
					this.SelectingEnableOn();
				}
			}
			else
			{
				this.m_SelectingMode = 0;
				RoomPartsTweenColor roomPartsTweenColor = (RoomPartsTweenColor)this.m_Action.GetAction(typeof(RoomPartsTweenColor));
				if (roomPartsTweenColor != null)
				{
					roomPartsTweenColor.SetEndStep();
				}
			}
		}

		// Token: 0x06001D60 RID: 7520 RVA: 0x00099D04 File Offset: 0x00098104
		private void SelectingEnableOn()
		{
			if (this.m_SelectingMode != 1)
			{
				this.m_SelectingMode = 1;
				RoomPartsTweenColor roomPartsTweenColor = (RoomPartsTweenColor)this.m_Action.GetAction(typeof(RoomPartsTweenColor));
				if (roomPartsTweenColor == null)
				{
					roomPartsTweenColor = new RoomPartsTweenColor(this.m_Obj, new Color(1f, 1f, 1f, 1f) * this.m_BaseColor, new Color(0.7f, 0.7f, 0.7f, 1f) * this.m_BaseColor, 0.6f);
					this.m_Action.EntryAction(roomPartsTweenColor, 0U);
				}
				else
				{
					roomPartsTweenColor.ChangeColor(new Color(1f, 1f, 1f, 1f) * this.m_BaseColor, new Color(0.7f, 0.7f, 0.7f, 1f) * this.m_BaseColor);
				}
				if (this.m_HitMarkMtl != null)
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_OK_COLOR);
				}
			}
		}

		// Token: 0x06001D61 RID: 7521 RVA: 0x00099E24 File Offset: 0x00098224
		private void SelectingDisableOn()
		{
			if (this.m_SelectingMode != 2)
			{
				this.m_SelectingMode = 2;
				RoomPartsTweenColor roomPartsTweenColor = (RoomPartsTweenColor)this.m_Action.GetAction(typeof(RoomPartsTweenColor));
				if (roomPartsTweenColor == null)
				{
					roomPartsTweenColor = new RoomPartsTweenColor(this.m_Obj, new Color(1f, 1f, 1f, 1f) * this.m_BaseColor, new Color(0.7f, 0.1f, 0.1f, 1f) * this.m_BaseColor, 0.6f);
					this.m_Action.EntryAction(roomPartsTweenColor, 0U);
				}
				else
				{
					roomPartsTweenColor.ChangeColor(new Color(1f, 1f, 1f, 1f) * this.m_BaseColor, new Color(0.7f, 0.1f, 0.1f, 1f) * this.m_BaseColor);
				}
				if (this.m_HitMarkMtl != null)
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_NG_COLOR);
				}
			}
		}

		// Token: 0x06001D62 RID: 7522 RVA: 0x00099F44 File Offset: 0x00098344
		protected void LinkObjectNode()
		{
			RoomObjectOption.RoomOptionTool roomObjectOption = RoomObjectOption.GetRoomObjectOption(RoomObjectListUtil.GetObjectParam(this.m_Category, this.m_ObjID).m_OptionID);
			if (roomObjectOption != null)
			{
				for (int i = 0; i < roomObjectOption.m_Table.Length; i++)
				{
					if (roomObjectOption.m_Table[i].m_Key == 1)
					{
						Transform transform = RoomUtility.FindTransform(this.m_Obj.transform, "LOC_" + roomObjectOption.m_Table[i].m_MakeID, 8);
						if (transform == null)
						{
							new GameObject("LOC_" + roomObjectOption.m_Table[i].m_MakeID)
							{
								transform = 
								{
									localPosition = new Vector3(roomObjectOption.m_Table[i].m_PosX, roomObjectOption.m_Table[i].m_PosY, 0f)
								}
							}.transform.SetParent(this.m_Obj.transform, false);
						}
						else
						{
							Vector3 localPosition = transform.localPosition;
							localPosition.x += roomObjectOption.m_Table[i].m_PosX;
							localPosition.y += roomObjectOption.m_Table[i].m_PosY;
							transform.localPosition = localPosition;
						}
					}
					if (roomObjectOption.m_Table[i].m_Key == 2)
					{
						Transform transform = RoomUtility.FindTransform(this.m_Obj.transform, "ROC_" + roomObjectOption.m_Table[i].m_MakeID, 8);
						if (transform == null)
						{
							GameObject gameObject = new GameObject("ROC_" + roomObjectOption.m_Table[i].m_MakeID);
							gameObject.transform.localPosition = Vector3.zero;
							gameObject.transform.SetParent(this.m_Obj.transform, false);
							new GameObject("ROC_Child_0")
							{
								transform = 
								{
									localPosition = new Vector3(roomObjectOption.m_Table[i].m_PosX, roomObjectOption.m_Table[i].m_PosY, 0f)
								}
							}.transform.SetParent(gameObject.transform, false);
						}
						else
						{
							new GameObject("ROC_Child_" + transform.transform.childCount)
							{
								transform = 
								{
									localPosition = new Vector3(roomObjectOption.m_Table[i].m_PosX, roomObjectOption.m_Table[i].m_PosY, 0f)
								}
							}.transform.SetParent(transform, false);
						}
					}
				}
			}
		}

		// Token: 0x06001D63 RID: 7523 RVA: 0x0009A218 File Offset: 0x00098618
		public override bool IsCharaCheck(RoomObjectCtrlChara pbase)
		{
			return this.m_MessageIDCheck == 0 || this.IsPopMessage(pbase.GetHandle().CharaResource.TweetListDB, this.m_MessageIDCheck);
		}

		// Token: 0x06001D64 RID: 7524 RVA: 0x0009A244 File Offset: 0x00098644
		private bool IsPopMessage(TweetListDB pdb, int fid)
		{
			for (int i = 0; i < pdb.m_Params.Length; i++)
			{
				if (pdb.m_Params[i].m_ID == fid)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001D65 RID: 7525 RVA: 0x0009A284 File Offset: 0x00098684
		protected void ReloadModel(int fobjid)
		{
		}

		// Token: 0x06001D66 RID: 7526 RVA: 0x0009A288 File Offset: 0x00098688
		public void UpGridStateGroupUp(RoomGridState pgrid, int fupstate)
		{
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				pgrid.SetGridGroup(base.BlockData.PosX, base.BlockData.PosY, this.m_DefaultSizeX, this.m_DefaultSizeY, fupstate, this.m_ObjGroupKey);
			}
			else
			{
				pgrid.SetGridGroup(base.BlockData.PosX, base.BlockData.PosY, this.m_DefaultSizeY, this.m_DefaultSizeX, fupstate, this.m_ObjGroupKey);
			}
		}

		// Token: 0x06001D67 RID: 7527 RVA: 0x0009A304 File Offset: 0x00098704
		public override void CreateHitAreaModel(bool fhitcolor)
		{
			if (this.m_Obj.GetComponent<MeshRenderer>() == null)
			{
				Vector2 blockSize = RoomUtility.GetBlockSize((float)this.m_DefaultSizeX, 0f);
				Vector2 blockSize2 = RoomUtility.GetBlockSize((float)this.m_DefaultSizeX, (float)this.m_DefaultSizeY);
				Vector2 blockSize3 = RoomUtility.GetBlockSize(0f, (float)this.m_DefaultSizeY);
				this.m_HitMarkObj = new GameObject("HitMarkObj");
				this.m_HitMarkMtl = new Material(Shader.Find("Room/GridLine"));
				if (fhitcolor)
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_OK_COLOR);
				}
				else
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_NG_COLOR);
				}
				this.m_HitMarkMtl.renderQueue = 3010;
				Mesh mesh = new Mesh();
				MeshRenderer meshRenderer = this.m_HitMarkObj.AddComponent<MeshRenderer>();
				MeshFilter meshFilter = this.m_HitMarkObj.AddComponent<MeshFilter>();
				mesh.vertices = new Vector3[]
				{
					new Vector3(0f, 0f, 0f),
					new Vector3(blockSize.x, blockSize.y, 0f),
					new Vector3(blockSize2.x, blockSize2.y, 0f),
					new Vector3(blockSize3.x, blockSize3.y, 0f)
				};
				mesh.triangles = new int[]
				{
					0,
					1,
					2,
					0,
					2,
					3
				};
				mesh.RecalculateBounds();
				meshFilter.mesh = mesh;
				meshRenderer.sortingOrder = 9990;
				meshRenderer.material = this.m_HitMarkMtl;
				this.m_HitMarkObj.transform.SetParent(base.CacheTransform, false);
				if (this.m_Dir == CharacterDefine.eDir.L)
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(1f, 1f, 1f);
				}
				else
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(-1f, 1f, 1f);
				}
				RoomPartsMove paction = new RoomPartsMove(this.m_Obj.transform, Vector3.zero, new Vector3(0f, 0.1f, 0f), 0.12f, RoomPartsMove.ePathType.LocalLinear);
				this.m_Action.EntryAction(paction, 0U);
				RoomPartsBoundScale paction2 = new RoomPartsBoundScale(this.m_Obj.transform, this.m_Obj.transform.localScale, this.m_Obj.transform.localScale * 1.2f, 0.15f);
				this.m_Action.EntryAction(paction2, 0U);
			}
		}

		// Token: 0x06001D68 RID: 7528 RVA: 0x0009A5BC File Offset: 0x000989BC
		public override void DestroyHitAreaModel()
		{
			if (this.m_HitMarkObj != null)
			{
				UnityEngine.Object.Destroy(this.m_HitMarkObj);
				this.m_HitMarkObj = null;
				UnityEngine.Object.Destroy(this.m_HitMarkMtl);
				this.m_HitMarkMtl = null;
			}
			if (this.m_Obj != null)
			{
				this.m_Obj.transform.localPosition = Vector3.zero;
			}
		}

		// Token: 0x06001D69 RID: 7529 RVA: 0x0009A624 File Offset: 0x00098A24
		private void CreateCharaHitView(byte fmask, int fposx, int fposy, int faddx, int faddy, int fpoint)
		{
			if (fmask != 0)
			{
				Transform transform = RoomUtility.FindTransform(this.m_Obj.transform, "CharaHitView" + fpoint, 4);
				GameObject gameObject;
				if (transform != null)
				{
					gameObject = transform.gameObject;
				}
				else
				{
					gameObject = new GameObject("CharaHitView" + fpoint);
					gameObject.transform.SetParent(this.m_Obj.transform, false);
				}
				int num = 0;
				int i;
				for (i = 0; i < 8; i++)
				{
					if (((int)fmask & 1 << i) != 0)
					{
						num++;
					}
				}
				Vector3[] array = new Vector3[num * 4];
				int[] array2 = new int[num * 6];
				num = 0;
				Vector2 blockSize = RoomUtility.GetBlockSize(1f, 0f);
				i = 0;
				while (i < 8 && fmask != 0)
				{
					if ((fmask & 1) != 0)
					{
						Vector2 blockSize2 = RoomUtility.GetBlockSize((float)fposx, (float)fposy);
						array[num * 4] = new Vector3(-(0f * blockSize.x + blockSize2.x), 0f * blockSize.y + blockSize2.y, 0f);
						array[num * 4 + 1] = new Vector3(-(1f * blockSize.x + blockSize2.x), 1f * blockSize.y + blockSize2.y, 0f);
						array[num * 4 + 2] = new Vector3(-(0f * blockSize.x + blockSize2.x), 2f * blockSize.y + blockSize2.y, 0f);
						array[num * 4 + 3] = new Vector3(-(-1f * blockSize.x + blockSize2.x), 1f * blockSize.y + blockSize2.y, 0f);
						array2[num * 6] = num * 4;
						array2[num * 6 + 1] = num * 4 + 1;
						array2[num * 6 + 2] = num * 4 + 3;
						array2[num * 6 + 3] = num * 4 + 1;
						array2[num * 6 + 4] = num * 4 + 2;
						array2[num * 6 + 5] = num * 4 + 3;
						num++;
					}
					fmask = (byte)(fmask >> 1);
					fposx += faddx;
					fposy += faddy;
					i++;
				}
				Color color = new Color(1f, 0.25f, 0.2f, 0.5f);
				Material material = new Material(Shader.Find("Room/GridLine"));
				material.SetColor("_Color", color);
				material.renderQueue = 3000;
				Mesh mesh = new Mesh();
				MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
				MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
				mesh.vertices = array;
				mesh.triangles = array2;
				mesh.RecalculateBounds();
				meshFilter.mesh = mesh;
				meshRenderer.sortingOrder = 0;
				meshRenderer.material = material;
			}
		}

		// Token: 0x06001D6A RID: 7530 RVA: 0x0009A934 File Offset: 0x00098D34
		private void DestroyCharaHit(int fpoint)
		{
			Transform transform = RoomUtility.FindTransform(this.m_Obj.transform, "CharaHitView" + fpoint, 4);
			if (transform != null)
			{
				UnityEngine.Object.Destroy(transform.gameObject);
			}
		}

		// Token: 0x06001D6B RID: 7531 RVA: 0x0009A97C File Offset: 0x00098D7C
		public virtual void DrawCharaHitView(bool factive)
		{
			if (factive)
			{
				this.CreateCharaHitView(this.m_HitActionDir.m_Up, -1, -1, 1, 0, 0);
				this.CreateCharaHitView(this.m_HitActionDir.m_Left, (int)this.m_BlockData.Size.x, -1, 0, 1, 1);
				this.CreateCharaHitView(this.m_HitActionDir.m_Down, (int)this.m_BlockData.Size.x, (int)this.m_BlockData.Size.y, -1, 0, 2);
				this.CreateCharaHitView(this.m_HitActionDir.m_Right, -1, (int)this.m_BlockData.Size.y, 0, -1, 3);
			}
			else
			{
				this.DestroyCharaHit(0);
				this.DestroyCharaHit(1);
				this.DestroyCharaHit(2);
				this.DestroyCharaHit(3);
			}
		}

		// Token: 0x040023C9 RID: 9161
		protected int m_SubKeyID;

		// Token: 0x040023CA RID: 9162
		protected GameObject m_Obj;

		// Token: 0x040023CB RID: 9163
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040023CC RID: 9164
		protected MsbHandler m_MsbHndl;

		// Token: 0x040023CD RID: 9165
		protected McatFileHelper m_MabMap;

		// Token: 0x040023CE RID: 9166
		protected int m_SelectingMode;

		// Token: 0x040023CF RID: 9167
		private IRoomObjectControll.ModelRenderConfig[] m_DefaultConfig;

		// Token: 0x040023D0 RID: 9168
		private Color m_BaseColor = Color.white;

		// Token: 0x040023D1 RID: 9169
		private int m_MessageIDCheck;

		// Token: 0x040023D2 RID: 9170
		private RoomPartsActionPakage m_Action;

		// Token: 0x040023D3 RID: 9171
		private RoomObjectModel.RoomModelGroup[] m_DirMeshGroup;

		// Token: 0x040023D4 RID: 9172
		protected RoomObjectModel.MarkMask m_HitActionDir;

		// Token: 0x040023D5 RID: 9173
		protected ushort m_OptionFlag;

		// Token: 0x040023D6 RID: 9174
		protected GameObject m_HitMarkObj;

		// Token: 0x040023D7 RID: 9175
		protected Material m_HitMarkMtl;

		// Token: 0x040023D8 RID: 9176
		public bool m_CharaHitView;

		// Token: 0x020005D7 RID: 1495
		public struct RoomModelGroup
		{
			// Token: 0x06001D6C RID: 7532 RVA: 0x0009AA54 File Offset: 0x00098E54
			public void SetView(bool fview)
			{
				for (int i = 0; i < this.m_GroupModel.Length; i++)
				{
					this.m_GroupModel[i].SetActive(fview);
				}
			}

			// Token: 0x040023D9 RID: 9177
			public GameObject[] m_GroupModel;
		}

		// Token: 0x020005D8 RID: 1496
		[StructLayout(LayoutKind.Explicit)]
		public struct MarkMask
		{
			// Token: 0x040023DA RID: 9178
			[FieldOffset(0)]
			public byte m_Down;

			// Token: 0x040023DB RID: 9179
			[FieldOffset(1)]
			public byte m_Right;

			// Token: 0x040023DC RID: 9180
			[FieldOffset(2)]
			public byte m_Up;

			// Token: 0x040023DD RID: 9181
			[FieldOffset(3)]
			public byte m_Left;

			// Token: 0x040023DE RID: 9182
			[FieldOffset(0)]
			public uint m_Mask;
		}
	}
}
