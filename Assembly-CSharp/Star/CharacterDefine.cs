﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000144 RID: 324
	public static class CharacterDefine
	{
		// Token: 0x04000894 RID: 2196
		public static readonly string[] EN_LOCATOR_NAMES = new string[]
		{
			"LOC_body",
			"LOC_overhead"
		};

		// Token: 0x04000895 RID: 2197
		public static readonly string[] WEAPON_ATTACH_NAMES = new string[]
		{
			"root/Hips/Spine1/Spine2/Shoulder_L/Arm_L/Fore_arm_L/Hand_L/Weapon_L/Loc_L",
			"root/Hips/Spine1/Spine2/Shoulder_R/Arm_R/Fore_arm_R/Hand_R/Weapon_R/Loc_R"
		};

		// Token: 0x04000896 RID: 2198
		public const string PL_HEAD_ROOT_NAME = "Head_root";

		// Token: 0x04000897 RID: 2199
		public const string PL_BODY_ROOT_NAME = "root";

		// Token: 0x04000898 RID: 2200
		public const string HIPS_NAME = "Hips";

		// Token: 0x04000899 RID: 2201
		public const string EN_ROOT_NAME = "root";

		// Token: 0x0400089A RID: 2202
		public const float PL_BODY_LOCATOR_OFFSET_Y = 0.5f;

		// Token: 0x0400089B RID: 2203
		public static readonly Vector3 PL_OVERHEAD_OFFSET_FROM_HEAD = new Vector3(-0.1f, 0.36f, 0f);

		// Token: 0x0400089C RID: 2204
		public const int BODY_ID_DEFAULT = 0;

		// Token: 0x0400089D RID: 2205
		public const int BODY_ID_SKIRT = 1;

		// Token: 0x0400089E RID: 2206
		public const int BODY_ID_TIGHT_SKIRT = 2;

		// Token: 0x0400089F RID: 2207
		public static readonly string[] BODY_ANIM_PREFIXS = new string[]
		{
			"common_body",
			"common_body_skirt",
			"common_body_tight"
		};

		// Token: 0x040008A0 RID: 2208
		public static readonly string[] MENU_MODE_ACT_KEYS = new string[]
		{
			"room_idle_L",
			"room_idle_R",
			"room_idle_skirt_L",
			"room_idle_skirt_R"
		};

		// Token: 0x040008A1 RID: 2209
		public const string MENU_MODE_IDLE_L_ACT_KEY = "room_idle_L";

		// Token: 0x040008A2 RID: 2210
		public const string MENU_MODE_IDLE_R_ACT_KEY = "room_idle_R";

		// Token: 0x040008A3 RID: 2211
		public const string MENU_MODE_IDLE_SKIRT_L_ACT_KEY = "room_idle_skirt_L";

		// Token: 0x040008A4 RID: 2212
		public const string MENU_MODE_IDLE_SKIRT_R_ACT_KEY = "room_idle_skirt_R";

		// Token: 0x040008A5 RID: 2213
		public static readonly int[] FACIAL_ID_DEFAULT = new int[]
		{
			0,
			45
		};

		// Token: 0x040008A6 RID: 2214
		public static readonly int[,] FACIAL_ID_BLINK = new int[,]
		{
			{
				0,
				18
			},
			{
				45,
				54
			}
		};

		// Token: 0x040008A7 RID: 2215
		public static readonly int[,] FACIAL_ID_BLINK_BATTLE = new int[,]
		{
			{
				1,
				18
			},
			{
				45,
				54
			}
		};

		// Token: 0x040008A8 RID: 2216
		public static readonly int[] FACIAL_ID_ABNORMAL_BATTLE = new int[]
		{
			6,
			48
		};

		// Token: 0x040008A9 RID: 2217
		public static readonly int[] FACIAL_ID_SLEEP_BATTLE = new int[]
		{
			18,
			54
		};

		// Token: 0x040008AA RID: 2218
		public const int FACIAL_NUM = 100;

		// Token: 0x040008AB RID: 2219
		public const string FLOATING_OBJ_NAME = "Ex_Float";

		// Token: 0x040008AC RID: 2220
		public const float FLOATIONG_FREQUENCY = 1.9f;

		// Token: 0x040008AD RID: 2221
		public const float FLOATIONG_AMPLITUDE = 0.065f;

		// Token: 0x040008AE RID: 2222
		public const string ABPATH_ANIM_PL_CMN_BTL_BODY = "anim/player/common_battle_body.muast";

		// Token: 0x02000145 RID: 325
		public enum eMode
		{
			// Token: 0x040008B0 RID: 2224
			None = -1,
			// Token: 0x040008B1 RID: 2225
			Battle,
			// Token: 0x040008B2 RID: 2226
			Room,
			// Token: 0x040008B3 RID: 2227
			Menu,
			// Token: 0x040008B4 RID: 2228
			Viewer,
			// Token: 0x040008B5 RID: 2229
			Num
		}

		// Token: 0x02000146 RID: 326
		public enum eDir
		{
			// Token: 0x040008B7 RID: 2231
			L,
			// Token: 0x040008B8 RID: 2232
			R,
			// Token: 0x040008B9 RID: 2233
			FRONT,
			// Token: 0x040008BA RID: 2234
			Num
		}

		// Token: 0x02000147 RID: 327
		public enum ePlayerModelParts
		{
			// Token: 0x040008BC RID: 2236
			Head,
			// Token: 0x040008BD RID: 2237
			Body,
			// Token: 0x040008BE RID: 2238
			Num
		}

		// Token: 0x02000148 RID: 328
		public enum eEnemyModelParts
		{
			// Token: 0x040008C0 RID: 2240
			Body,
			// Token: 0x040008C1 RID: 2241
			Num
		}

		// Token: 0x02000149 RID: 329
		public enum eLocatorIndex
		{
			// Token: 0x040008C3 RID: 2243
			Body,
			// Token: 0x040008C4 RID: 2244
			OverHead,
			// Token: 0x040008C5 RID: 2245
			Num
		}

		// Token: 0x0200014A RID: 330
		public enum eWeaponIndex
		{
			// Token: 0x040008C7 RID: 2247
			L,
			// Token: 0x040008C8 RID: 2248
			R,
			// Token: 0x040008C9 RID: 2249
			Num
		}

		// Token: 0x0200014B RID: 331
		public enum eFriendType
		{
			// Token: 0x040008CB RID: 2251
			None = -1,
			// Token: 0x040008CC RID: 2252
			Registered,
			// Token: 0x040008CD RID: 2253
			Unregistered
		}

		// Token: 0x0200014C RID: 332
		public enum eAnimEvent
		{
			// Token: 0x040008CF RID: 2255
			Facial = 1,
			// Token: 0x040008D0 RID: 2256
			Num
		}
	}
}
