﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020006CD RID: 1741
	public class TownPartsPopObject : MonoBehaviour
	{
		// Token: 0x0600227A RID: 8826 RVA: 0x000B7E6C File Offset: 0x000B626C
		public void SetPopObject(TownBuilder pbuilder, GameObject pself, ITownObjectHandler phandle, Vector3 foffset)
		{
			this.m_Object = pself;
			this.m_Builder = pbuilder;
			this.m_Offset = foffset;
			this.m_PopObject = new GameObject[5];
			Transform transform = TownUtility.FindTransform(base.transform, "Money", 4);
			if (transform != null)
			{
				this.m_PopObject[0] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "Item", 4);
			if (transform != null)
			{
				this.m_PopObject[1] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "New", 4);
			if (transform != null)
			{
				this.m_PopObject[2] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "MoneyMax", 4);
			if (transform != null)
			{
				this.m_PopObject[3] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "ItemMax", 4);
			if (transform != null)
			{
				this.m_PopObject[4] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "CountUp", 4);
			if (transform != null)
			{
				this.m_CountSheet = transform.gameObject;
				this.m_Message = this.m_CountSheet.GetComponentInChildren<Text>();
			}
			this.m_Object.transform.localPosition = this.m_Offset;
			SphereCollider componentInChildren = pself.GetComponentInChildren<SphereCollider>();
			if (componentInChildren != null)
			{
				TownObjectConnect townObjectConnect = componentInChildren.gameObject.AddComponent<TownObjectConnect>();
				townObjectConnect.m_Link = phandle;
			}
		}

		// Token: 0x0600227B RID: 8827 RVA: 0x000B7FF0 File Offset: 0x000B63F0
		public void ChangeIcon(int findex)
		{
			for (int i = 0; i < this.m_PopObject.Length; i++)
			{
				this.m_PopObject[i].SetActive(false);
			}
			this.m_CountSheet.SetActive(false);
			switch (findex)
			{
			case 0:
				this.m_PopObject[findex].SetActive(true);
				break;
			case 1:
				this.m_PopObject[findex].SetActive(true);
				this.m_CountSheet.SetActive(true);
				break;
			case 3:
			case 4:
				this.m_PopObject[findex].SetActive(true);
				break;
			}
		}

		// Token: 0x0600227C RID: 8828 RVA: 0x000B8099 File Offset: 0x000B6499
		public void DestroyRequest()
		{
			this.m_Step = 2;
		}

		// Token: 0x0600227D RID: 8829 RVA: 0x000B80A4 File Offset: 0x000B64A4
		public void Update()
		{
			byte step = this.m_Step;
			if (step != 0)
			{
				if (step != 1)
				{
					if (step == 2)
					{
						UnityEngine.Object.Destroy(this);
						this.m_Builder.ReleaseCharaMarker(this.m_Object);
						this.m_Step = 3;
					}
				}
			}
			else
			{
				this.m_Object.transform.localPosition = this.m_Offset;
				this.m_Step = 1;
			}
		}

		// Token: 0x0600227E RID: 8830 RVA: 0x000B811B File Offset: 0x000B651B
		public void SetViewActive(bool factive)
		{
			base.gameObject.SetActive(factive);
		}

		// Token: 0x0600227F RID: 8831 RVA: 0x000B812C File Offset: 0x000B652C
		public void ChangePer(int fcountup, bool fix)
		{
			if (this.m_CountSheet != null && this.m_Message != null)
			{
				if (fix)
				{
					if (this.m_PopObject[0].activeSelf)
					{
						this.ChangeIcon(3);
					}
					else if (this.m_PopObject[1].activeSelf)
					{
						this.ChangeIcon(4);
					}
					this.m_Message.text = string.Empty;
				}
				else
				{
					if (this.m_PopObject[3].activeSelf)
					{
						this.ChangeIcon(0);
					}
					else if (this.m_PopObject[4].activeSelf)
					{
						this.ChangeIcon(1);
					}
					this.m_Message.text = fcountup.ToString();
				}
			}
		}

		// Token: 0x0400294A RID: 10570
		private GameObject m_Object;

		// Token: 0x0400294B RID: 10571
		private TownBuilder m_Builder;

		// Token: 0x0400294C RID: 10572
		private Vector3 m_Offset;

		// Token: 0x0400294D RID: 10573
		private byte m_Step;

		// Token: 0x0400294E RID: 10574
		private GameObject[] m_PopObject;

		// Token: 0x0400294F RID: 10575
		private GameObject m_CountSheet;

		// Token: 0x04002950 RID: 10576
		private Text m_Message;
	}
}
