﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200069B RID: 1691
	public class TownEffectManager : MonoBehaviour
	{
		// Token: 0x060021E6 RID: 8678 RVA: 0x000B4364 File Offset: 0x000B2764
		public static TownEffectManager CreateEffectManager(GameObject plink)
		{
			return plink.AddComponent<TownEffectManager>();
		}

		// Token: 0x060021E7 RID: 8679 RVA: 0x000B436C File Offset: 0x000B276C
		public static void ReleaseEffectManager(TownEffectManager pmng)
		{
			TownEffectManager.ms_Inst = null;
		}

		// Token: 0x060021E8 RID: 8680 RVA: 0x000B4374 File Offset: 0x000B2774
		public static TownEffectPlayer CreateEffect(int feffectno)
		{
			return TownEffectManager.ms_Inst.CreateEffectNode(feffectno);
		}

		// Token: 0x060021E9 RID: 8681 RVA: 0x000B4384 File Offset: 0x000B2784
		private void Awake()
		{
			TownEffectManager.ms_Inst = this;
			this.m_ResTable = new TownEffectManager.EffectResource[10];
			this.m_ResMax = 10;
			this.m_Player = new TownEffectPlayer[10];
			this.m_PlayerMax = 10;
			this.m_PlayerNum = 0;
			this.m_WaitResTable = new int[5];
			this.m_WaitResMax = 5;
			this.m_WaitResNum = 0;
			this.m_WaitPlayerTable = new TownEffectPlayer[5];
			this.m_WaitPlayerNum = 0;
			this.m_WaitPlayerMax = 5;
		}

		// Token: 0x060021EA RID: 8682 RVA: 0x000B43FC File Offset: 0x000B27FC
		public void SetEffectList(string pfilename)
		{
			this.m_BaseHandle = ObjectResourceManager.CreateHandle(pfilename, new Action<MeigeResource.Handler>(this.CallbackHandle));
		}

		// Token: 0x060021EB RID: 8683 RVA: 0x000B4418 File Offset: 0x000B2818
		private void CallbackHandle(MeigeResource.Handler phandle)
		{
			this.m_BaseHandle = phandle;
			if (this.m_BaseHandle.IsDone)
			{
				GameObject asset = this.m_BaseHandle.GetAsset<GameObject>();
				if (asset != null)
				{
					TownEffectDataBase component = asset.GetComponent<TownEffectDataBase>();
					if (component != null)
					{
						TownEffectManager.ms_Inst.m_EffList = new TownEffectManager.EffectDataSet[component.m_List.Length];
						for (int i = 0; i < component.m_List.Length; i++)
						{
							TownEffectManager.ms_Inst.m_EffList[i].m_FileName = component.m_List[i].m_FileName;
							TownEffectManager.ms_Inst.m_EffList[i].m_Size = component.m_List[i].m_Size;
						}
					}
				}
				ObjectResourceManager.ReleaseHandle(this.m_BaseHandle);
				this.m_BaseHandle = null;
			}
		}

		// Token: 0x060021EC RID: 8684 RVA: 0x000B44F8 File Offset: 0x000B28F8
		private void Update()
		{
			if (this.m_ResUp != 0 && this.m_ResUp >= 2)
			{
				int num = this.m_WaitResTable[0];
				this.m_ResTable[num].m_Object = this.m_ResTable[num].m_Handle.GetAsset<UnityEngine.Object>();
				this.m_ResTable[num].m_ReadWait = false;
				this.m_ResTable[num].m_Active = true;
				this.m_ResUp = 0;
				for (int i = 1; i < this.m_WaitResNum; i++)
				{
					this.m_WaitResTable[i - 1] = this.m_WaitResTable[i];
				}
				this.m_WaitResNum--;
				if (this.m_WaitResNum != 0)
				{
					this.StartResLoad(this.m_WaitResTable[0]);
				}
			}
			bool flag;
			if (this.m_WaitPlayerNum != 0)
			{
				flag = false;
				for (int i = 0; i < this.m_WaitPlayerNum; i++)
				{
					if (this.m_ResTable[this.m_WaitPlayerTable[i].GetResourceNo()].m_Active)
					{
						this.m_WaitPlayerTable[i].SetUpObject(this.m_ResTable[this.m_WaitPlayerTable[i].GetResourceNo()].m_Object);
						this.m_WaitPlayerTable[i] = null;
						flag = true;
					}
				}
				if (flag)
				{
					int i = 0;
					while (i < this.m_WaitPlayerNum)
					{
						if (this.m_WaitPlayerTable[i] == null)
						{
							for (int j = i + 1; j < this.m_WaitPlayerNum; j++)
							{
								this.m_WaitPlayerTable[j - 1] = this.m_WaitPlayerTable[j];
							}
							this.m_WaitPlayerNum--;
							this.m_WaitPlayerTable[this.m_WaitPlayerNum] = null;
						}
						else
						{
							i++;
						}
					}
				}
			}
			flag = false;
			for (int i = 0; i < this.m_PlayerNum; i++)
			{
				if (!this.m_Player[i].IsActive())
				{
					UnityEngine.Object.Destroy(this.m_Player[i].gameObject);
					this.ReleaseEffRes(this.m_Player[i].GetResourceNo());
					this.m_Player[i] = null;
					flag = true;
				}
			}
			if (flag)
			{
				int i = 0;
				while (i < this.m_PlayerNum)
				{
					if (this.m_Player[i] == null)
					{
						for (int j = i + 1; j < this.m_PlayerNum; j++)
						{
							this.m_Player[j - 1] = this.m_Player[j];
						}
						this.m_PlayerNum--;
						this.m_Player[this.m_PlayerNum] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x060021ED RID: 8685 RVA: 0x000B4780 File Offset: 0x000B2B80
		private TownEffectPlayer CreateEffectNode(int feffectno)
		{
			GameObject gameObject = new GameObject("eff_" + this.m_PlayerNum);
			TownEffectPlayer townEffectPlayer = gameObject.AddComponent<TownEffectPlayer>();
			if (this.m_PlayerNum >= this.m_PlayerMax)
			{
				TownEffectPlayer[] array = new TownEffectPlayer[this.m_PlayerMax + 10];
				for (int i = 0; i < this.m_PlayerMax; i++)
				{
					array[i] = this.m_Player[i];
				}
				this.m_Player = null;
				this.m_Player = array;
				this.m_PlayerMax += 10;
			}
			this.m_Player[this.m_PlayerNum] = townEffectPlayer;
			this.m_PlayerNum++;
			int num = this.CreateEffectResource(feffectno);
			townEffectPlayer.SetResourceNo(num);
			if (this.m_ResTable[num].m_Active)
			{
				townEffectPlayer.SetUpObject(this.m_ResTable[num].m_Object);
			}
			else
			{
				this.CheckResLoad(num);
				if (this.m_WaitPlayerNum >= this.m_WaitPlayerMax)
				{
					TownEffectPlayer[] array2 = new TownEffectPlayer[this.m_WaitPlayerMax + 5];
					for (int i = 0; i < this.m_WaitPlayerMax; i++)
					{
						array2[i] = this.m_WaitPlayerTable[i];
					}
					this.m_WaitPlayerTable = null;
					this.m_WaitPlayerTable = array2;
					this.m_WaitPlayerMax += 5;
				}
				this.m_WaitPlayerTable[this.m_WaitPlayerNum] = townEffectPlayer;
				this.m_WaitPlayerNum++;
			}
			return townEffectPlayer;
		}

		// Token: 0x060021EE RID: 8686 RVA: 0x000B48EC File Offset: 0x000B2CEC
		private int CreateEffectResource(int feffno)
		{
			for (int i = 0; i < this.m_ResMax; i++)
			{
				if (this.m_ResTable[i] != null && this.m_ResTable[i].m_EffectNo == feffno)
				{
					this.m_ResTable[i].m_UseNum++;
					return i;
				}
			}
			return this.EntryResource(feffno);
		}

		// Token: 0x060021EF RID: 8687 RVA: 0x000B4950 File Offset: 0x000B2D50
		private int EntryResource(int feffno)
		{
			int num = -1;
			for (int i = 0; i < this.m_ResMax; i++)
			{
				if (this.m_ResTable[i] == null)
				{
					num = i;
					break;
				}
			}
			if (num < 0)
			{
				TownEffectManager.EffectResource[] array = new TownEffectManager.EffectResource[this.m_ResMax + 5];
				for (int i = 0; i < this.m_ResMax; i++)
				{
					array[i] = this.m_ResTable[i];
				}
				this.m_ResTable = null;
				this.m_ResTable = array;
				num = this.m_ResMax;
				this.m_ResMax += 5;
			}
			this.m_ResTable[num] = new TownEffectManager.EffectResource();
			this.m_ResTable[num].m_EffectNo = feffno;
			this.m_ResTable[num].m_UseNum = 1;
			return num;
		}

		// Token: 0x060021F0 RID: 8688 RVA: 0x000B4A10 File Offset: 0x000B2E10
		private void ReleaseEffRes(int fresno)
		{
			this.m_ResTable[fresno].m_UseNum--;
			if (this.m_ResTable[fresno].m_UseNum <= 0)
			{
				this.m_ResTable[fresno].m_Active = false;
				this.m_ResTable[fresno].m_Object = null;
				if (this.m_ResTable[fresno].m_Handle != null)
				{
					ObjectResourceManager.ReleaseHandle(this.m_ResTable[fresno].m_Handle);
				}
				this.m_ResTable[fresno] = null;
			}
		}

		// Token: 0x060021F1 RID: 8689 RVA: 0x000B4A90 File Offset: 0x000B2E90
		private void StartResLoad(int fresno)
		{
			this.m_ResUp = 1;
			this.m_ResTable[fresno].m_Handle = ObjectResourceManager.CreateHandle(this.m_EffList[this.m_ResTable[fresno].m_EffectNo].m_FileName, new Action<MeigeResource.Handler>(this.CallbackResObj));
		}

		// Token: 0x060021F2 RID: 8690 RVA: 0x000B4AE0 File Offset: 0x000B2EE0
		private void CheckResLoad(int fresno)
		{
			if (!this.m_ResTable[fresno].m_ReadWait)
			{
				if (this.m_ResUp == 0)
				{
					this.m_ResTable[fresno].m_ReadWait = true;
					this.m_WaitResTable[this.m_WaitResNum] = fresno;
					this.m_WaitResNum++;
					this.StartResLoad(fresno);
				}
				else
				{
					if (this.m_WaitResNum >= this.m_WaitResMax)
					{
						int[] array = new int[this.m_WaitResMax + 5];
						for (int i = 0; i < this.m_WaitResMax; i++)
						{
							array[i] = this.m_WaitResTable[i];
						}
						this.m_WaitResTable = null;
						this.m_WaitResTable = array;
						this.m_WaitResMax += 5;
					}
					this.m_WaitResTable[this.m_WaitResNum] = fresno;
					this.m_WaitResNum++;
				}
			}
		}

		// Token: 0x060021F3 RID: 8691 RVA: 0x000B4BB9 File Offset: 0x000B2FB9
		private void CallbackResObj(MeigeResource.Handler phandle)
		{
			this.m_ResUp = 2;
		}

		// Token: 0x04002857 RID: 10327
		private static TownEffectManager ms_Inst;

		// Token: 0x04002858 RID: 10328
		private TownEffectManager.EffectResource[] m_ResTable;

		// Token: 0x04002859 RID: 10329
		private int m_ResMax;

		// Token: 0x0400285A RID: 10330
		private TownEffectPlayer[] m_Player;

		// Token: 0x0400285B RID: 10331
		private int m_PlayerNum;

		// Token: 0x0400285C RID: 10332
		private int m_PlayerMax;

		// Token: 0x0400285D RID: 10333
		private int[] m_WaitResTable;

		// Token: 0x0400285E RID: 10334
		private int m_WaitResNum;

		// Token: 0x0400285F RID: 10335
		private int m_WaitResMax;

		// Token: 0x04002860 RID: 10336
		private byte m_ResUp;

		// Token: 0x04002861 RID: 10337
		private TownEffectPlayer[] m_WaitPlayerTable;

		// Token: 0x04002862 RID: 10338
		private int m_WaitPlayerNum;

		// Token: 0x04002863 RID: 10339
		private int m_WaitPlayerMax;

		// Token: 0x04002864 RID: 10340
		private TownEffectManager.EffectDataSet[] m_EffList;

		// Token: 0x04002865 RID: 10341
		private MeigeResource.Handler m_BaseHandle;

		// Token: 0x0200069C RID: 1692
		public class EffectResource
		{
			// Token: 0x04002866 RID: 10342
			public int m_EffectNo;

			// Token: 0x04002867 RID: 10343
			public UnityEngine.Object m_Object;

			// Token: 0x04002868 RID: 10344
			public int m_UseNum;

			// Token: 0x04002869 RID: 10345
			public bool m_Active;

			// Token: 0x0400286A RID: 10346
			public bool m_ReadWait;

			// Token: 0x0400286B RID: 10347
			public MeigeResource.Handler m_Handle;
		}

		// Token: 0x0200069D RID: 1693
		public struct EffectDataSet
		{
			// Token: 0x0400286C RID: 10348
			public string m_FileName;

			// Token: 0x0400286D RID: 10349
			public float m_Size;
		}
	}
}
