﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000625 RID: 1573
	public class RoomShadowControll
	{
		// Token: 0x06001EA2 RID: 7842 RVA: 0x000A607C File Offset: 0x000A447C
		public void SetControllTrs(Transform pshadow, Transform pnode, Transform proot, float foffset, Vector3 fsize)
		{
			this.m_ShadowHandle = pshadow;
			this.m_ShadowSize = fsize;
			this.m_ShadowSizeTrs = pnode;
			this.m_ShadowSizeKey = (pnode.position - proot.position).y * foffset;
		}

		// Token: 0x06001EA3 RID: 7843 RVA: 0x000A60C4 File Offset: 0x000A44C4
		public void UpdateShadow(Transform pnode)
		{
			if (this.m_ShadowSizeTrs != null && pnode != null)
			{
				Vector3 vector = this.m_ShadowSizeTrs.position - pnode.position;
				float d = 1f;
				if (vector.y > this.m_ShadowSizeKey)
				{
					d = 1f - (vector.y - this.m_ShadowSizeKey) / this.m_ShadowLength;
				}
				this.m_ShadowHandle.localScale = this.m_ShadowSize * d;
			}
		}

		// Token: 0x0400255C RID: 9564
		protected Transform m_ShadowHandle;

		// Token: 0x0400255D RID: 9565
		protected Vector3 m_ShadowSize = Vector3.one;

		// Token: 0x0400255E RID: 9566
		protected Transform m_ShadowSizeTrs;

		// Token: 0x0400255F RID: 9567
		public float m_ShadowSizeKey;

		// Token: 0x04002560 RID: 9568
		public float m_ShadowLength = 0.25f;
	}
}
