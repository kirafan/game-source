﻿using System;
using Star.UI;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x02000433 RID: 1075
	public class MenuState_Init : MenuState
	{
		// Token: 0x060014A6 RID: 5286 RVA: 0x0006CABC File Offset: 0x0006AEBC
		public MenuState_Init(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014A7 RID: 5287 RVA: 0x0006CAD4 File Offset: 0x0006AED4
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060014A8 RID: 5288 RVA: 0x0006CAD7 File Offset: 0x0006AED7
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Init.eStep.First;
		}

		// Token: 0x060014A9 RID: 5289 RVA: 0x0006CAE0 File Offset: 0x0006AEE0
		public override void OnStateExit()
		{
		}

		// Token: 0x060014AA RID: 5290 RVA: 0x0006CAE2 File Offset: 0x0006AEE2
		public override void OnDispose()
		{
		}

		// Token: 0x060014AB RID: 5291 RVA: 0x0006CAE4 File Offset: 0x0006AEE4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Step = MenuState_Init.eStep.Load_Wait;
				}
				break;
			case MenuState_Init.eStep.Load_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Menu, false);
					this.m_Step = MenuState_Init.eStep.Load_WaitBG;
				}
				break;
			case MenuState_Init.eStep.Load_WaitBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = MenuState_Init.eStep.SetupUI;
				}
				break;
			case MenuState_Init.eStep.SetupUI:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = MenuState_Init.eStep.End;
				}
				break;
			case MenuState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = MenuState_Init.eStep.None;
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome = false;
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart)
					{
						this.m_Owner.SetLaunchChildScene(MenuMain.eLaunchChildScene.Friend);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart = false;
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome = true;
						return 3;
					}
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart)
					{
						this.m_Owner.SetLaunchChildScene(MenuMain.eLaunchChildScene.Library);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart = false;
						if (this.m_Owner.GetLaunchChildScene() == MenuMain.eLaunchChildScene.Library)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome = true;
						}
						return 4;
					}
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam != null)
					{
						this.m_Owner.SetLaunchChildScene(MenuMain.eLaunchChildScene.LibraryEvent);
						if (this.m_Owner.GetLaunchChildScene() == MenuMain.eLaunchChildScene.LibraryEvent || this.m_Owner.GetLaunchChildScene() == MenuMain.eLaunchChildScene.Library)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome = true;
						}
						return 13;
					}
					this.m_Owner.SetLaunchChildScene(MenuMain.eLaunchChildScene.Top);
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014AC RID: 5292 RVA: 0x0006CD4C File Offset: 0x0006B14C
		private bool SetupUI()
		{
			this.m_Owner.m_NpcUI = UIUtility.GetMenuComponent<NPCCharaDisplayUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.m_NpcUI == null)
			{
				return false;
			}
			this.m_Owner.m_NpcUI.Setup();
			return true;
		}

		// Token: 0x060014AD RID: 5293 RVA: 0x0006CDAA File Offset: 0x0006B1AA
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001B91 RID: 7057
		private MenuState_Init.eStep m_Step = MenuState_Init.eStep.None;

		// Token: 0x04001B92 RID: 7058
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.NPCCharaDisplayUI;

		// Token: 0x02000434 RID: 1076
		private enum eStep
		{
			// Token: 0x04001B94 RID: 7060
			None = -1,
			// Token: 0x04001B95 RID: 7061
			First,
			// Token: 0x04001B96 RID: 7062
			Load_Wait,
			// Token: 0x04001B97 RID: 7063
			Load_WaitBG,
			// Token: 0x04001B98 RID: 7064
			SetupUI,
			// Token: 0x04001B99 RID: 7065
			End
		}
	}
}
