﻿using System;

namespace Star
{
	// Token: 0x0200062D RID: 1581
	public static class SoundDefine
	{
		// Token: 0x06001EF7 RID: 7927 RVA: 0x000A85E0 File Offset: 0x000A69E0
		// Note: this type is marked as 'beforefieldinit'.
		static SoundDefine()
		{
			string[,] array = new string[2, 3];
			array[0, 0] = "BGM_builtin";
			array[0, 1] = "BGM_builtin.acb";
			array[0, 2] = "BGM_builtin.awb";
			array[1, 0] = "System";
			array[1, 1] = "System.acb";
			array[1, 2] = string.Empty;
			SoundDefine.BUILTIN_DATAS = array;
			SoundDefine.CATEGORY_NAMES = new string[]
			{
				"BGM",
				"SE",
				"Voice"
			};
			SoundDefine.CATEGORY_BGM = SoundDefine.CATEGORY_NAMES[0];
			SoundDefine.CATEGORY_SE = SoundDefine.CATEGORY_NAMES[1];
			SoundDefine.CATEGORY_VOICE = SoundDefine.CATEGORY_NAMES[2];
		}

		// Token: 0x040025AF RID: 9647
		public const string ACF_FILE_NAME = "Star.acf";

		// Token: 0x040025B0 RID: 9648
		public const string EXT_ACF = ".acf";

		// Token: 0x040025B1 RID: 9649
		public const string EXT_ACB = ".acb";

		// Token: 0x040025B2 RID: 9650
		public const string EXT_AWB = ".awb";

		// Token: 0x040025B3 RID: 9651
		public static readonly string[,] BUILTIN_DATAS;

		// Token: 0x040025B4 RID: 9652
		public const int BGM_DEFAULT_FADE_IN_MILI_SEC = 800;

		// Token: 0x040025B5 RID: 9653
		public const int BGM_DEFAULT_FADE_OUT_MILI_SEC = 800;

		// Token: 0x040025B6 RID: 9654
		public const int CATEGORY_NUM = 3;

		// Token: 0x040025B7 RID: 9655
		public static readonly string[] CATEGORY_NAMES;

		// Token: 0x040025B8 RID: 9656
		public static readonly string CATEGORY_BGM;

		// Token: 0x040025B9 RID: 9657
		public static readonly string CATEGORY_SE;

		// Token: 0x040025BA RID: 9658
		public static readonly string CATEGORY_VOICE;

		// Token: 0x0200062E RID: 1582
		public enum eCategory
		{
			// Token: 0x040025BC RID: 9660
			BGM,
			// Token: 0x040025BD RID: 9661
			SE,
			// Token: 0x040025BE RID: 9662
			VOICE,
			// Token: 0x040025BF RID: 9663
			Num
		}

		// Token: 0x0200062F RID: 1583
		public enum eLoadStatus
		{
			// Token: 0x040025C1 RID: 9665
			None,
			// Token: 0x040025C2 RID: 9666
			Loading,
			// Token: 0x040025C3 RID: 9667
			Loaded
		}
	}
}
