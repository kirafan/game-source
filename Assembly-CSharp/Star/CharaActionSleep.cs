﻿using System;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000534 RID: 1332
	public class CharaActionSleep : ICharaRoomAction
	{
		// Token: 0x06001AA0 RID: 6816 RVA: 0x0008DD78 File Offset: 0x0008C178
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
			this.m_Step = CharaActionSleep.eSleepStep.Init;
			this.m_Target = roomObjHndl;
			this.m_Owner = pOwner.GetHandle();
			this.m_Base = pOwner;
			this.m_PartsAction = new RoomPartsActionPakage();
			this.m_AnimeMap = this.m_Base.GetAnimeMap();
			this.m_AnimePlay = this.m_Owner.CharaAnim;
			this.m_Builder = pOwner.GetBuilder();
			this.m_Target.LinkKeyLock(true);
			this.m_OverlapMessgeID = 0;
			if (psetup.m_Table.Length >= 2)
			{
				this.m_OverlapMessgeID = psetup.m_Table[1];
			}
			this.m_EventQue = false;
			this.m_FloorChg = false;
			switch (psetup.m_Table[0])
			{
			case 0:
				this.m_FloorChg = true;
				break;
			case 1:
				this.m_EventQue = true;
				this.m_FloorChg = true;
				break;
			case 2:
				this.m_Step = CharaActionSleep.eSleepStep.FloorUp;
				this.m_FloorChg = true;
				break;
			default:
				this.m_EventQue = true;
				break;
			}
			this.m_Base.SetBlink(false);
			this.m_MainOwnerPos = this.m_Base.GetGridPos();
			this.m_SleepFloor = this.m_Target.FloorID;
			this.CalcTargetTrs();
		}

		// Token: 0x06001AA1 RID: 6817 RVA: 0x0008DEB0 File Offset: 0x0008C2B0
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			bool result = true;
			switch (this.m_Step)
			{
			case CharaActionSleep.eSleepStep.FloorUp:
			{
				this.ChangeSleepFloor();
				string animeGroup = this.m_AnimeMap.GetAnimeGroup(1, this.m_Base.GetDir());
				this.m_Owner.CacheTransform.position = this.m_BindTrs.position;
				this.m_AnimePlay.PlayAnim(animeGroup, WrapMode.ClampForever, 0f, 0f);
				this.m_Target.PlayMotion((int)CRC32.Calc("lp"), WrapMode.ClampForever, 1f);
				this.m_Step = CharaActionSleep.eSleepStep.Sleep;
				break;
			}
			case CharaActionSleep.eSleepStep.Init:
				this.m_Owner.CharaAnim.PlayAnim(this.m_AnimeMap.GetAnimeKey(RoomDefine.eAnim.Idle, this.m_Base.GetDir()), WrapMode.Loop, 0f, 0f);
				this.m_Owner.CharaAnim.SetBlink(false);
				this.m_Step++;
				break;
			case CharaActionSleep.eSleepStep.HudAction:
			{
				this.ChangeSleepFloor();
				string animeGroup = this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir());
				this.m_AnimePlay.PlayAnim(animeGroup, WrapMode.ClampForever, 0f, 0f);
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, this.m_BindTrs.position, 1.1666666f, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				this.m_Target.PlayMotion((int)CRC32.Calc("st"), WrapMode.ClampForever, 1f);
				this.m_Step++;
				break;
			}
			case CharaActionSleep.eSleepStep.SleepIn:
				if (!this.m_AnimePlay.IsPlayingAnim(this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir())))
				{
					this.m_Target.PlayMotion((int)CRC32.Calc("lp"), WrapMode.ClampForever, 1f);
					this.SetBedComforterView(true);
					this.m_AnimePlay.PlayAnim(this.m_AnimeMap.GetAnimeGroup(1, this.m_Base.GetDir()), WrapMode.ClampForever, 0f, 0f);
					this.m_Step++;
					if (this.m_EventQue)
					{
						this.SetUpHudPopUp();
					}
				}
				break;
			case CharaActionSleep.eSleepStep.Sleep:
				if (!this.m_AnimePlay.IsPlayingAnim(this.m_AnimeMap.GetAnimeGroup(1, this.m_Base.GetDir())))
				{
					if (this.m_EventQue)
					{
						if (this.m_PartsAction.GetAction(typeof(RoomPartsPopUp)) == null)
						{
							string animeGroup = this.m_AnimeMap.GetAnimeGroup(2, this.m_Base.GetDir());
							this.m_AnimePlay.PlayAnim(animeGroup, WrapMode.ClampForever, 0f, 0f);
							this.m_TimeChk = 0.36666667f;
							this.m_Target.PlayMotion((int)CRC32.Calc("ed"), WrapMode.ClampForever, 1f);
							this.m_Step++;
						}
					}
					else if (this.m_Base.IsWakeUpAction())
					{
						this.m_Base.SetWakeUpAction(false);
						string animeGroup = this.m_AnimeMap.GetAnimeGroup(2, this.m_Base.GetDir());
						this.m_AnimePlay.PlayAnim(animeGroup, WrapMode.ClampForever, 0f, 0f);
						this.m_TimeChk = 0.36666667f;
						this.m_Target.PlayMotion((int)CRC32.Calc("ed"), WrapMode.ClampForever, 1f);
						this.m_Step++;
					}
				}
				break;
			case CharaActionSleep.eSleepStep.SleepMoveOut:
				this.m_TimeChk -= Time.deltaTime;
				if (this.m_TimeChk <= 0f)
				{
					RoomPartsMove paction2 = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, this.m_BackUpPos, 0.76666665f, RoomPartsMove.ePathType.Linear);
					this.m_PartsAction.EntryAction(paction2, 0U);
					this.m_Step++;
				}
				break;
			case CharaActionSleep.eSleepStep.SleepOut:
				if (!this.m_AnimePlay.IsPlayingAnim(this.m_AnimeMap.GetAnimeGroup(2, this.m_Base.GetDir())))
				{
					this.m_AnimeMap.SetMarkingGroup(RoomDefine.eAnim.Idle);
					this.m_AnimePlay.PlayAnim(this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir()), WrapMode.ClampForever, 0f, 0f);
					this.m_Step++;
					this.m_Target.LinkKeyLock(false);
					this.m_Target.SetLinkObject(this.m_Base, false);
				}
				break;
			case CharaActionSleep.eSleepStep.MainMove:
				if (this.m_FloorChg)
				{
					RoomGridState gridStatus = this.m_Builder.GetGridStatus(0);
					this.m_Base.ChangeFloor(0);
					Vector2 vector = gridStatus.CalcFreeMovePoint();
					this.m_Base.SetGridPosition((int)vector.x, (int)vector.y, 0);
				}
				else
				{
					this.m_Base.SetGridPosition((int)this.m_MainOwnerPos.x, (int)this.m_MainOwnerPos.y, 0);
				}
				this.m_Step++;
				break;
			case CharaActionSleep.eSleepStep.Wake:
				this.m_Step++;
				break;
			case CharaActionSleep.eSleepStep.End:
				result = false;
				break;
			}
			this.m_PartsAction.PakageUpdate();
			return result;
		}

		// Token: 0x06001AA2 RID: 6818 RVA: 0x0008E3E8 File Offset: 0x0008C7E8
		private void SetUpHudPopUp()
		{
			if (this.m_OverlapMessgeID > 0)
			{
				RoomCharaHud uihud = this.m_Base.GetUIHud();
				if (uihud != null && RoomActionScriptPlay.IsPopMessage(this.m_Owner.CharaResource.TweetListDB, this.m_OverlapMessgeID))
				{
					TweetListDB_Param param = this.m_Owner.CharaResource.TweetListDB.GetParam(this.m_OverlapMessgeID);
					uihud.GetCharaTweet().Open(param.m_Text);
					int fvoicelink = -1;
					if (param.m_VoiceCueName != null && param.m_VoiceCueName.Length > 1)
					{
						fvoicelink = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_Owner.CharaID).m_NamedType, param.m_VoiceCueName, false);
					}
					RoomPartsPopUp paction = new RoomPartsPopUp(uihud, 10f, fvoicelink);
					this.m_PartsAction.EntryAction(paction, 0U);
				}
			}
		}

		// Token: 0x06001AA3 RID: 6819 RVA: 0x0008E4E4 File Offset: 0x0008C8E4
		public void ChangeSleepFloor()
		{
			Vector2 pos = this.m_Target.BlockData.Pos;
			this.m_Target.SetLinkObject(this.m_Base, true);
			this.m_Base.SetDir(RoomUtility.ChangeObjectDir(this.m_Target.GetDir()));
			if (this.m_SleepFloor != this.m_Base.FloorID)
			{
				this.m_Base.ChangeFloor(this.m_SleepFloor);
			}
			if (this.m_Target.GetDir() == CharacterDefine.eDir.R)
			{
				this.m_Base.SetGridPosition((int)pos.x + 2, (int)pos.y - 1, this.m_SleepFloor);
			}
			else
			{
				this.m_Base.SetGridPosition((int)pos.x - 1, (int)pos.y + 2, this.m_SleepFloor);
			}
			this.m_AnimeMap.SetMarkingGroup(CRC32.Calc("Sleep"));
			this.m_BackUpPos = this.m_Owner.CacheTransform.position;
		}

		// Token: 0x06001AA4 RID: 6820 RVA: 0x0008E5E4 File Offset: 0x0008C9E4
		public void CalcTargetTrs()
		{
			Transform attachTransform = this.m_Target.GetAttachTransform(this.m_Base.GetGridPos());
			if (attachTransform == null)
			{
				this.m_BindTrs = this.m_Target.CacheTransform;
			}
			else
			{
				this.m_BindTrs = attachTransform;
			}
		}

		// Token: 0x06001AA5 RID: 6821 RVA: 0x0008E634 File Offset: 0x0008CA34
		private void SetBedComforterView(bool fview)
		{
			Transform transform = RoomUtility.FindTransform(this.m_Target.CacheTransform, "obj_0", 16);
			if (transform != null)
			{
				transform.gameObject.SetActive(fview);
			}
		}

		// Token: 0x06001AA6 RID: 6822 RVA: 0x0008E674 File Offset: 0x0008CA74
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			if (this.m_EventQue || !this.m_Target.IsAvailable())
			{
				if (this.m_Step < CharaActionSleep.eSleepStep.MainMove)
				{
					this.m_Target.LinkKeyLock(false);
					this.m_Target.SetLinkObject(this.m_Base, false);
				}
				if (this.m_Step < CharaActionSleep.eSleepStep.End)
				{
					this.SetBedComforterView(true);
				}
				if (this.m_FloorChg)
				{
					this.m_Base.ChangeFloor(0);
					RoomGridState gridStatus = this.m_Builder.GetGridStatus(0);
					this.m_Base.ChangeFloor(0);
					Vector2 vector = gridStatus.CalcFreeMovePoint();
					this.m_Base.SetGridPosition((int)vector.x, (int)vector.y, 0);
				}
				else
				{
					this.m_Base.SetGridPosition((int)this.m_MainOwnerPos.x, (int)this.m_MainOwnerPos.y, 0);
				}
				return true;
			}
			return false;
		}

		// Token: 0x06001AA7 RID: 6823 RVA: 0x0008E759 File Offset: 0x0008CB59
		public void Release()
		{
		}

		// Token: 0x0400210D RID: 8461
		private CharaActionSleep.eSleepStep m_Step;

		// Token: 0x0400210E RID: 8462
		private CharacterHandler m_Owner;

		// Token: 0x0400210F RID: 8463
		private RoomObjectCtrlChara m_Base;

		// Token: 0x04002110 RID: 8464
		private RoomAnimAccessMap m_AnimeMap;

		// Token: 0x04002111 RID: 8465
		private CharacterAnim m_AnimePlay;

		// Token: 0x04002112 RID: 8466
		private RoomPartsActionPakage m_PartsAction;

		// Token: 0x04002113 RID: 8467
		private RoomBuilder m_Builder;

		// Token: 0x04002114 RID: 8468
		private Transform m_BindTrs;

		// Token: 0x04002115 RID: 8469
		private IRoomObjectControll m_Target;

		// Token: 0x04002116 RID: 8470
		private Vector2 m_MainOwnerPos;

		// Token: 0x04002117 RID: 8471
		private Vector3 m_BackUpPos;

		// Token: 0x04002118 RID: 8472
		private bool m_EventQue;

		// Token: 0x04002119 RID: 8473
		private bool m_FloorChg;

		// Token: 0x0400211A RID: 8474
		private float m_TimeChk;

		// Token: 0x0400211B RID: 8475
		private int m_SleepFloor;

		// Token: 0x0400211C RID: 8476
		private int m_OverlapMessgeID;

		// Token: 0x02000535 RID: 1333
		public enum eSleepStep
		{
			// Token: 0x0400211E RID: 8478
			FloorUp,
			// Token: 0x0400211F RID: 8479
			Init,
			// Token: 0x04002120 RID: 8480
			HudAction,
			// Token: 0x04002121 RID: 8481
			SleepIn,
			// Token: 0x04002122 RID: 8482
			Sleep,
			// Token: 0x04002123 RID: 8483
			SleepMoveOut,
			// Token: 0x04002124 RID: 8484
			SleepOut,
			// Token: 0x04002125 RID: 8485
			MainMove,
			// Token: 0x04002126 RID: 8486
			Wake,
			// Token: 0x04002127 RID: 8487
			End
		}
	}
}
