﻿using System;
using System.Collections.Generic;
using Star.UI.Battle;
using UnityEngine;

namespace Star
{
	// Token: 0x0200012C RID: 300
	public class SkillActionPlayer : MonoBehaviour
	{
		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060007B5 RID: 1973 RVA: 0x0002F7C0 File Offset: 0x0002DBC0
		// (remove) Token: 0x060007B6 RID: 1974 RVA: 0x0002F7F8 File Offset: 0x0002DBF8
		private event SkillActionPlayer.UpdateRefAnimTimeDelegate UpdateRefAnimTimeCallback;

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060007B7 RID: 1975 RVA: 0x0002F82E File Offset: 0x0002DC2E
		public int PlayingKeyFrame
		{
			get
			{
				return this.m_PlayingKeyFrame;
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060007B8 RID: 1976 RVA: 0x0002F836 File Offset: 0x0002DC36
		private Transform OwnerTransform
		{
			get
			{
				return (!(this.m_Owner != null)) ? null : this.m_Owner.CacheTransform.parent;
			}
		}

		// Token: 0x060007B9 RID: 1977 RVA: 0x0002F85F File Offset: 0x0002DC5F
		public void Setup(BattleSystem battleSystem)
		{
			this.m_BattleSystem = battleSystem;
			this.m_PlayerPartyCenter = this.m_BattleSystem.CenterLocator_Player;
			this.m_EnemyPartyCenter = this.m_BattleSystem.CenterLocator_Enemy;
		}

		// Token: 0x060007BA RID: 1978 RVA: 0x0002F88C File Offset: 0x0002DC8C
		public void Destroy()
		{
			this.m_TgtCharaList = null;
			this.m_MyCharaList = null;
			if (this.m_ActiveEffectList != null)
			{
				for (int i = 0; i < this.m_ActiveEffectList.Count; i++)
				{
					SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(this.m_ActiveEffectList[i]);
					this.m_ActiveEffectList[i] = null;
				}
				this.m_ActiveEffectList.Clear();
			}
			if (this.m_ProjectileEffectList != null)
			{
				foreach (short key in this.m_ProjectileEffectList.Keys)
				{
					if (this.m_ProjectileEffectList[key] != null)
					{
						this.m_ProjectileEffectList[key].Destroy();
					}
				}
				this.m_ProjectileEffectList.Clear();
			}
			if (this.m_AttachedEffectList != null)
			{
				foreach (short key2 in this.m_AttachedEffectList.Keys)
				{
					SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(this.m_AttachedEffectList[key2].m_EffectHndl);
				}
				this.m_AttachedEffectList.Clear();
			}
			if (this.m_AttachedTrailList != null)
			{
				foreach (short key3 in this.m_AttachedTrailList.Keys)
				{
					SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(this.m_AttachedTrailList[key3]);
				}
				this.m_AttachedTrailList.Clear();
			}
			if (this.m_StackEffectSets != null)
			{
				foreach (CharacterHandler key4 in this.m_StackEffectSets.Keys)
				{
					if (this.m_StackEffectSets[key4] != null)
					{
						for (int j = 0; j < this.m_StackEffectSets[key4].Count; j++)
						{
							this.m_StackEffectSets[key4][j].Destroy();
						}
						this.m_StackEffectSets[key4].Clear();
					}
				}
				this.m_StackEffectSets.Clear();
			}
			if (this.m_PlayingEffectSets != null)
			{
				foreach (CharacterHandler key5 in this.m_PlayingEffectSets.Keys)
				{
					if (this.m_PlayingEffectSets[key5] != null)
					{
						this.m_PlayingEffectSets[key5].Destroy();
					}
				}
				this.m_PlayingEffectSets.Clear();
			}
			this.m_CallbackEnd.Clear();
			this.m_SAP = null;
			this.m_SAG = null;
			this.m_Owner = null;
			this.UpdateRefAnimTimeCallback = null;
			if (this.m_WeaponThrowDatas != null)
			{
				for (int k = 0; k < this.m_WeaponThrowDatas.Length; k++)
				{
					if (this.m_WeaponThrowDatas[k] != null)
					{
						this.m_WeaponThrowDatas[k].Clear();
					}
				}
			}
			this.m_IsPlaying = false;
		}

		// Token: 0x060007BB RID: 1979 RVA: 0x0002FC30 File Offset: 0x0002E030
		public bool IsPlaying()
		{
			return this.m_IsPlaying;
		}

		// Token: 0x060007BC RID: 1980 RVA: 0x0002FC38 File Offset: 0x0002E038
		public bool Play(SkillActionPlan sap, SkillActionGraphics sag, CharacterHandler owner, BattleCommandData command, List<CharacterHandler> tgtCharaList, List<CharacterHandler> myCharaList, SkillActionPlayer.UpdateRefAnimTimeDelegate updateRefAnimTimeCallback)
		{
			this.m_SAP = sap;
			if (this.m_SAP == null)
			{
				return false;
			}
			this.m_SAG = sag;
			if (this.m_SAG == null)
			{
				return false;
			}
			this.m_Owner = owner;
			if (this.m_Owner == null)
			{
				return false;
			}
			this.m_Command = command;
			if (this.m_Command == null)
			{
				this.m_Command = this.m_Owner.CharaBattle.ExecCommand;
			}
			this.m_BaseElementType = this.m_Owner.CharaParam.ElementType;
			this.UpdateRefAnimTimeCallback = updateRefAnimTimeCallback;
			this.m_TgtCharaList = tgtCharaList;
			this.m_MyCharaList = myCharaList;
			this.m_ActiveEffectList = new List<EffectHandler>();
			this.m_ProjectileEffectList = new Dictionary<short, SkillActionProjectile>();
			this.m_AttachedEffectList = new Dictionary<short, SkillActionPlayer.AttachedEffectData>();
			this.m_AttachedTrailList = new Dictionary<short, TrailHandler>();
			this.m_WeaponThrowDatas = new SkillActionPlayer.WeaponThrowData[2];
			for (int i = 0; i < this.m_WeaponThrowDatas.Length; i++)
			{
				this.m_WeaponThrowDatas[i] = new SkillActionPlayer.WeaponThrowData();
			}
			this.m_StackEffectSets.Clear();
			this.m_PlayingEffectSets.Clear();
			this.m_IsOverRefAnimTime = false;
			this.m_PlayingSec = 0f;
			this.m_PlayingKeyFrame = 0;
			this.m_PlayingKeyFrameOld = 0;
			for (int j = 0; j < this.m_CurrentDataIndices.Length; j++)
			{
				this.m_CurrentDataIndices[j] = 0;
			}
			bool flag = false;
			Color endColor = BattleDefine.BG_COLOR_SKILL;
			Color endColor2 = BattleDefine.CHARA_COLOR_SKILL;
			if (this.m_Owner.CharaBattle.MyParty.IsPlayerParty)
			{
				BattleCommandData.eCommandType commandType = this.m_Command.CommandType;
				if (commandType != BattleCommandData.eCommandType.Skill && commandType != BattleCommandData.eCommandType.WeaponSkill)
				{
					if (commandType == BattleCommandData.eCommandType.UniqueSkill)
					{
						if (!this.m_BattleSystem.IsInUniqueSkillScene)
						{
							flag = true;
							endColor = BattleDefine.BG_COLOR_UNIQUESKILL;
							endColor2 = BattleDefine.CHARA_COLOR_UNIQUESKILL;
						}
					}
				}
				else
				{
					flag = true;
				}
			}
			else
			{
				BattleCommandData.eCommandType commandType2 = this.m_Command.CommandType;
				if (commandType2 == BattleCommandData.eCommandType.UniqueSkill)
				{
					if (!this.m_BattleSystem.IsInUniqueSkillScene)
					{
						flag = true;
						endColor = BattleDefine.BG_COLOR_UNIQUESKILL;
						endColor2 = BattleDefine.CHARA_COLOR_UNIQUESKILL;
					}
				}
			}
			if (flag)
			{
				this.m_BattleSystem.BattleBG.ChangeColorFromCurrent(endColor, 0.18f);
				for (int k = 0; k < 3; k++)
				{
					CharacterHandler member = this.m_Owner.CharaBattle.MyParty.GetMember((BattleDefine.eJoinMember)k);
					if (member != null)
					{
						bool flag2 = true;
						if (member == this.m_Owner)
						{
							flag2 = false;
						}
						else if (this.m_MyCharaList != null)
						{
							for (int l = 0; l < this.m_MyCharaList.Count; l++)
							{
								if (member == this.m_MyCharaList[l])
								{
									flag2 = false;
									break;
								}
							}
						}
						if (flag2)
						{
							member.CharaBattle.ChangeMeshColor(Color.white, endColor2, 0.18f);
						}
						else
						{
							member.CharaBattle.ResetMeshColor();
						}
					}
					member = this.m_Owner.CharaBattle.TgtParty.GetMember((BattleDefine.eJoinMember)k);
					if (member != null)
					{
						bool flag3 = true;
						if (member == this.m_Owner)
						{
							flag3 = false;
						}
						else if (this.m_TgtCharaList != null)
						{
							for (int m = 0; m < this.m_TgtCharaList.Count; m++)
							{
								if (member == this.m_TgtCharaList[m])
								{
									flag3 = false;
									break;
								}
							}
						}
						if (flag3)
						{
							member.CharaBattle.ChangeMeshColor(Color.white, endColor2, 0.18f);
						}
						else
						{
							member.CharaBattle.ResetMeshColor();
						}
					}
				}
			}
			this.m_IsPlaying = true;
			return true;
		}

		// Token: 0x060007BD RID: 1981 RVA: 0x0003001C File Offset: 0x0002E41C
		public void ClearUpdateRefAnimTimeCallback()
		{
			this.UpdateRefAnimTimeCallback = null;
		}

		// Token: 0x060007BE RID: 1982 RVA: 0x00030028 File Offset: 0x0002E428
		public void LateUpdate()
		{
			if (!this.m_IsPlaying)
			{
				return;
			}
			if (!this.m_IsOverRefAnimTime && this.UpdateRefAnimTimeCallback != null)
			{
				SkillActionPlayer.RefAnimTime refAnimTime = this.UpdateRefAnimTimeCallback();
				if (refAnimTime.m_PlayingSec >= refAnimTime.m_MaxSec)
				{
					float num = refAnimTime.m_PlayingSec - this.m_PlayingSec;
					if (num < Time.deltaTime)
					{
						num = Time.deltaTime;
					}
					this.m_PlayingSec += num;
					this.m_IsOverRefAnimTime = true;
				}
				else
				{
					this.m_PlayingSec = refAnimTime.m_PlayingSec;
				}
			}
			else
			{
				this.m_PlayingSec += Time.deltaTime;
			}
			this.m_PlayingKeyFrameOld = this.m_PlayingKeyFrame;
			this.m_PlayingKeyFrame = (int)(this.m_PlayingSec / 0.033333335f);
			this.UpdateEventSolve();
			this.UpdateEventDamageAnim();
			this.UpdateEventDamageEffect();
			this.UpdateEventWeaponThrow_Parabola();
			this.UpdateEventWeaponVisible();
			this.UpdateEventWeaponReset();
			this.UpdateEventEffectPlay();
			this.UpdateEventEffectProjectile_Straight();
			this.UpdateEventEffectProjectile_Parabola();
			this.UpdateEventEffectProjectile_Penetrate();
			this.UpdateEventEffectAttach();
			this.UpdateEventEffectDetach();
			this.UpdateEventTrailAttach();
			this.UpdateEventTrailDetach();
			this.UpdateEventPlaySE();
			this.UpdateEventPlayVoice();
			this.UpdateEffectSet();
			for (int i = 0; i < this.m_WeaponThrowDatas.Length; i++)
			{
				if (this.m_WeaponThrowDatas[i] != null && this.m_WeaponThrowDatas[i].m_Movement != null)
				{
					int num2 = 0;
					Vector3 zero = Vector3.zero;
					if (this.m_WeaponThrowDatas[i].m_Movement.Update(out num2, out zero))
					{
						if (this.m_WeaponThrowDatas[i].m_RotationLinkAnim)
						{
							this.m_WeaponThrowDatas[i].m_RotationTarget.rotation = this.m_WeaponThrowDatas[i].m_RotationSource.rotation;
						}
					}
					else
					{
						if (this.m_WeaponThrowDatas[i].m_IsAutoDetachOnArrival)
						{
							for (int j = 0; j < this.m_WeaponThrowDatas[i].m_AttachedEffectUniqueIDs.Count; j++)
							{
								this.DetachEffect(this.m_WeaponThrowDatas[i].m_AttachedEffectUniqueIDs[j]);
							}
							this.m_WeaponThrowDatas[i].m_AttachedEffectUniqueIDs.Clear();
							for (int k = 0; k < this.m_WeaponThrowDatas[i].m_AttachedTrailUniqueIDs.Count; k++)
							{
								this.DetachTrail(this.m_WeaponThrowDatas[i].m_AttachedTrailUniqueIDs[k]);
							}
							this.m_WeaponThrowDatas[i].m_AttachedTrailUniqueIDs.Clear();
						}
						if (!string.IsNullOrEmpty(this.m_WeaponThrowDatas[i].m_CallbackKey))
						{
							this.OnArrivalProjectile(this.m_WeaponThrowDatas[i].m_CallbackKey, zero, this.m_WeaponThrowDatas[i].m_ThrowTargetCharaHndl);
						}
						this.m_WeaponThrowDatas[i].m_Movement = null;
					}
				}
			}
			if (this.m_ActiveEffectList != null)
			{
				for (int l = this.m_ActiveEffectList.Count - 1; l >= 0; l--)
				{
					EffectHandler effectHandler = this.m_ActiveEffectList[l];
					if (!effectHandler.IsPlaying())
					{
						SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(effectHandler);
						this.m_ActiveEffectList.RemoveAt(l);
					}
				}
			}
			if (this.m_ProjectileEffectList != null)
			{
				foreach (short key in this.m_ProjectileEffectList.Keys)
				{
					if (this.m_ProjectileEffectList[key] != null)
					{
						this.m_ProjectileEffectList[key].Update();
					}
				}
			}
			if (this.m_AttachedEffectList != null)
			{
				foreach (short key2 in this.m_AttachedEffectList.Keys)
				{
					if (this.m_AttachedEffectList[key2] != null && !this.m_AttachedEffectList[key2].m_IsDetached)
					{
						Transform parent = this.m_AttachedEffectList[key2].m_EffectHndl.CacheTransform.parent;
						Quaternion quaternion = (this.m_BattleSystem.MainCamera.transform.forward.z <= 0f) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
						Matrix4x4 matrix4x = default(Matrix4x4);
						matrix4x.SetTRS(new Vector3(parent.position.x, parent.position.y, this.m_AttachedEffectList[key2].m_OriginalPosZ), quaternion, parent.lossyScale);
						this.m_AttachedEffectList[key2].m_EffectHndl.CacheTransform.rotation = quaternion;
						this.m_AttachedEffectList[key2].m_EffectHndl.CacheTransform.position = new Vector3(matrix4x.m03, matrix4x.m13, matrix4x.m23);
					}
				}
			}
			this.UpdatePlayingStatus();
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x00030574 File Offset: 0x0002E974
		public void OnGUIDraw()
		{
		}

		// Token: 0x060007C0 RID: 1984 RVA: 0x00030578 File Offset: 0x0002E978
		private void UpdatePlayingStatus()
		{
			if (this.m_ActiveEffectList != null && this.m_ActiveEffectList.Count > 0)
			{
				return;
			}
			if (this.m_AttachedEffectList != null)
			{
				foreach (short key in this.m_AttachedEffectList.Keys)
				{
					if (this.m_AttachedEffectList[key] != null && this.m_AttachedEffectList[key].m_EffectHndl.IsPlaying())
					{
						return;
					}
				}
			}
			if (this.m_ProjectileEffectList != null)
			{
				foreach (short key2 in this.m_ProjectileEffectList.Keys)
				{
					if (this.m_ProjectileEffectList[key2] != null && this.m_ProjectileEffectList[key2].Movement.IsMoving)
					{
						return;
					}
				}
			}
			for (int i = 0; i < this.m_WeaponThrowDatas.Length; i++)
			{
				if (this.m_WeaponThrowDatas[i] != null && this.m_WeaponThrowDatas[i].m_Movement != null && this.m_WeaponThrowDatas[i].m_Movement.IsMoving)
				{
					return;
				}
			}
			foreach (CharacterHandler key3 in this.m_PlayingEffectSets.Keys)
			{
				if (this.m_PlayingEffectSets[key3] != null && this.m_PlayingEffectSets[key3].IsPlaying())
				{
					return;
				}
			}
			if (this.m_SAP.m_evSolve != null && this.m_SAP.m_evSolve.Count > this.m_CurrentDataIndices[0])
			{
				return;
			}
			if (this.m_SAP.m_evDamageAnim != null && this.m_SAP.m_evDamageAnim.Count > this.m_CurrentDataIndices[1])
			{
				return;
			}
			if (this.m_SAP.m_evDamageEffect != null && this.m_SAP.m_evDamageEffect.Count > this.m_CurrentDataIndices[2])
			{
				return;
			}
			if (this.m_SAG.m_evEffectPlay != null && this.m_SAG.m_evEffectPlay.Count > this.m_CurrentDataIndices[3])
			{
				return;
			}
			if (this.m_SAG.m_evEffectProjectile_Straight != null && this.m_SAG.m_evEffectProjectile_Straight.Count > this.m_CurrentDataIndices[4])
			{
				return;
			}
			if (this.m_SAG.m_evEffectProjectile_Parabola != null && this.m_SAG.m_evEffectProjectile_Parabola.Count > this.m_CurrentDataIndices[5])
			{
				return;
			}
			if (this.m_SAG.m_evEffectProjectile_Penetrate != null && this.m_SAG.m_evEffectProjectile_Penetrate.Count > this.m_CurrentDataIndices[6])
			{
				return;
			}
			if (this.m_SAG.m_evTrailAttach != null && this.m_SAG.m_evTrailAttach.Count > this.m_CurrentDataIndices[9])
			{
				return;
			}
			if (this.m_SAG.m_evTrailAttach != null && this.m_SAG.m_evTrailDetach.Count > this.m_CurrentDataIndices[10])
			{
				return;
			}
			if (this.m_SAG.m_evWeaponThrow_Parabola != null && this.m_SAG.m_evWeaponThrow_Parabola.Count > this.m_CurrentDataIndices[11])
			{
				return;
			}
			if (this.m_SAG.m_evWeaponVisible != null && this.m_SAG.m_evWeaponVisible.Count > this.m_CurrentDataIndices[12])
			{
				return;
			}
			if (this.m_SAG.m_evWeaponReset != null && this.m_SAG.m_evWeaponReset.Count > this.m_CurrentDataIndices[13])
			{
				return;
			}
			if (this.m_SAG.m_evPlaySE != null && this.m_SAG.m_evPlaySE.Count > this.m_CurrentDataIndices[14])
			{
				return;
			}
			if (this.m_SAG.m_evPlayVoice != null && this.m_SAG.m_evPlayVoice.Count > this.m_CurrentDataIndices[15])
			{
				return;
			}
			this.m_IsPlaying = false;
		}

		// Token: 0x060007C1 RID: 1985 RVA: 0x00030A14 File Offset: 0x0002EE14
		public void Skip()
		{
			if (this.m_SAP.m_evSolve != null)
			{
				for (int i = this.m_CurrentDataIndices[0]; i < this.m_SAP.m_evSolve.Count; i++)
				{
					this.UpdateEventSolve_Exec(this.m_SAP.m_evSolve[i], null, true);
				}
			}
			if (this.m_SAG.m_evEffectProjectile_Straight != null)
			{
				for (int j = 0; j < this.m_SAG.m_evEffectProjectile_Straight.Count; j++)
				{
					SkillActionEvent_EffectProjectile_Straight skillActionEvent_EffectProjectile_Straight = this.m_SAG.m_evEffectProjectile_Straight[j];
					if (!string.IsNullOrEmpty(skillActionEvent_EffectProjectile_Straight.m_CallbackKey) && this.m_SAP.m_CallbackDict != null && this.m_SAP.m_CallbackDict.ContainsKey(skillActionEvent_EffectProjectile_Straight.m_CallbackKey))
					{
						string callbackKey = skillActionEvent_EffectProjectile_Straight.m_CallbackKey;
						if (!this.m_CallbackEnd.ContainsKey(callbackKey))
						{
							SkillActionPlan.CallbackData callbackData = this.m_SAP.m_CallbackDict[skillActionEvent_EffectProjectile_Straight.m_CallbackKey];
							if (callbackData.m_evSolve != null)
							{
								for (int k = 0; k < callbackData.m_evSolve.Count; k++)
								{
									this.UpdateEventSolve_Exec(callbackData.m_evSolve[k], null, false);
								}
							}
						}
					}
				}
			}
			if (this.m_SAG.m_evEffectProjectile_Parabola != null)
			{
				for (int l = 0; l < this.m_SAG.m_evEffectProjectile_Parabola.Count; l++)
				{
					SkillActionEvent_EffectProjectile_Parabola skillActionEvent_EffectProjectile_Parabola = this.m_SAG.m_evEffectProjectile_Parabola[l];
					if (!string.IsNullOrEmpty(skillActionEvent_EffectProjectile_Parabola.m_CallbackKey) && this.m_SAP.m_CallbackDict != null && this.m_SAP.m_CallbackDict.ContainsKey(skillActionEvent_EffectProjectile_Parabola.m_CallbackKey))
					{
						string callbackKey2 = skillActionEvent_EffectProjectile_Parabola.m_CallbackKey;
						if (!this.m_CallbackEnd.ContainsKey(callbackKey2))
						{
							SkillActionPlan.CallbackData callbackData2 = this.m_SAP.m_CallbackDict[skillActionEvent_EffectProjectile_Parabola.m_CallbackKey];
							if (callbackData2.m_evSolve != null)
							{
								for (int m = 0; m < callbackData2.m_evSolve.Count; m++)
								{
									this.UpdateEventSolve_Exec(callbackData2.m_evSolve[m], null, false);
								}
							}
						}
					}
				}
			}
			if (this.m_SAG.m_evEffectProjectile_Penetrate != null)
			{
				for (int n = 0; n < this.m_SAG.m_evEffectProjectile_Penetrate.Count; n++)
				{
					SkillActionEvent_EffectProjectile_Penetrate skillActionEvent_EffectProjectile_Penetrate = this.m_SAG.m_evEffectProjectile_Penetrate[n];
					if (!string.IsNullOrEmpty(skillActionEvent_EffectProjectile_Penetrate.m_CallbackKey) && this.m_SAP.m_CallbackDict != null && this.m_SAP.m_CallbackDict.ContainsKey(skillActionEvent_EffectProjectile_Penetrate.m_CallbackKey))
					{
						List<CharacterHandler> list = new List<CharacterHandler>();
						this.GetEffectPos(skillActionEvent_EffectProjectile_Penetrate.m_TargetPosType, skillActionEvent_EffectProjectile_Penetrate.m_TargetPosLocatorType, Vector3.zero, false, list);
						for (int num = 0; num < list.Count; num++)
						{
							string key = skillActionEvent_EffectProjectile_Penetrate.m_CallbackKey + list[num].name;
							if (!this.m_CallbackEnd.ContainsKey(key))
							{
								SkillActionPlan.CallbackData callbackData3 = this.m_SAP.m_CallbackDict[skillActionEvent_EffectProjectile_Penetrate.m_CallbackKey];
								if (callbackData3.m_evSolve != null)
								{
									for (int num2 = 0; num2 < callbackData3.m_evSolve.Count; num2++)
									{
										this.UpdateEventSolve_Exec(callbackData3.m_evSolve[num2], list[num], false);
									}
								}
							}
						}
					}
				}
			}
			if (this.m_SAG.m_evWeaponThrow_Parabola != null)
			{
				for (int num3 = 0; num3 < this.m_SAG.m_evWeaponThrow_Parabola.Count; num3++)
				{
					SkillActionEvent_WeaponThrow_Parabola skillActionEvent_WeaponThrow_Parabola = this.m_SAG.m_evWeaponThrow_Parabola[num3];
					if (!string.IsNullOrEmpty(skillActionEvent_WeaponThrow_Parabola.m_CallbackKey) && this.m_SAP.m_CallbackDict != null && this.m_SAP.m_CallbackDict.ContainsKey(skillActionEvent_WeaponThrow_Parabola.m_CallbackKey))
					{
						string callbackKey3 = skillActionEvent_WeaponThrow_Parabola.m_CallbackKey;
						if (!this.m_CallbackEnd.ContainsKey(callbackKey3))
						{
							SkillActionPlan.CallbackData callbackData4 = this.m_SAP.m_CallbackDict[skillActionEvent_WeaponThrow_Parabola.m_CallbackKey];
							if (callbackData4.m_evSolve != null)
							{
								for (int num4 = 0; num4 < callbackData4.m_evSolve.Count; num4++)
								{
									this.UpdateEventSolve_Exec(callbackData4.m_evSolve[num4], null, false);
								}
							}
						}
					}
				}
			}
			this.m_IsPlaying = false;
		}

		// Token: 0x060007C2 RID: 1986 RVA: 0x00030EBC File Offset: 0x0002F2BC
		private void UpdateEventSolve()
		{
			if (this.m_SAP.m_evSolve == null)
			{
				return;
			}
			int num = 0;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAP.m_evSolve.Count; i++)
			{
				SkillActionEvent_Solve skillActionEvent_Solve = this.m_SAP.m_evSolve[i];
				if (skillActionEvent_Solve.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_Solve.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					this.UpdateEventSolve_Exec(skillActionEvent_Solve, null, false);
				}
				if (skillActionEvent_Solve.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x00030F70 File Offset: 0x0002F370
		private void UpdateEventSolve_Exec(SkillActionEvent_Solve ev, CharacterHandler targetCharaHndl, bool isSolveOnSkip)
		{
			List<SkillActionEffectSet> list = null;
			if (!BattleCommandParser.SolveCommandAt(this.m_Command, (int)ev.m_Index, new float[]
			{
				ev.m_Option_0
			}, targetCharaHndl, out list))
			{
				return;
			}
			if (!isSolveOnSkip)
			{
				if (list != null)
				{
					for (int i = 0; i < list.Count; i++)
					{
						if (!this.m_StackEffectSets.ContainsKey(list[i].TargetCharaHndl))
						{
							this.m_StackEffectSets.Add(list[i].TargetCharaHndl, new List<SkillActionEffectSet>());
						}
						this.m_StackEffectSets[list[i].TargetCharaHndl].Add(list[i]);
					}
				}
				if (this.m_Command.SolveResult.m_IsCreatedCard)
				{
					this.m_BattleSystem.BattleUI.GetOrder().SetFrames(this.m_BattleSystem.Order, 0, BattleOrderIndicator.eMoveType.Slide);
				}
			}
			bool flag = false;
			bool isCriticalHit = false;
			int num = -1;
			foreach (CharacterHandler characterHandler in this.m_Command.SolveResult.m_CharaResults.Keys)
			{
				BattleCommandSolveResult.CharaResult charaResult = this.m_Command.SolveResult.m_CharaResults[characterHandler];
				BattleCommandSolveResult.HitData hitData = charaResult.GetDamageHitDataNew();
				if (hitData == null)
				{
					hitData = charaResult.GetRecoverHitDataNew();
				}
				if (hitData != null)
				{
					if (!isSolveOnSkip)
					{
						Transform targetTransform;
						float offsetYfromTargetTransform;
						characterHandler.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out targetTransform, out offsetYfromTargetTransform);
						this.m_BattleSystem.BattleUI.DisplayDamage(hitData.m_Value, hitData.m_IsCritical, hitData.m_registHit_Or_defaultHit_Or_weakHit, charaResult.GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.RecoveryValueIsZeroBecauseUnhappy), false, targetTransform, offsetYfromTargetTransform);
						this.m_BattleSystem.UpdateUICharaStatus(characterHandler.CharaBattle.MyParty.PartyType, characterHandler.CharaBattle.Join, 2, true, false);
						this.m_BattleSystem.BattleUI.GetOrder().SlideStunFrames(this.m_BattleSystem.Order);
						if (hitData.m_Value < 0 && ev.m_IsEnableDamageEffect)
						{
							if (ev.m_DamageEffect.m_EffectType != eDmgEffectType.PL_All)
							{
								this.PlayDamageEffect(ev.m_DamageEffect, characterHandler, hitData.m_IsCritical, hitData.m_registHit_Or_defaultHit_Or_weakHit);
							}
							else
							{
								flag = true;
								if (hitData.m_IsCritical)
								{
									isCriticalHit = true;
								}
								if (hitData.m_registHit_Or_defaultHit_Or_weakHit > num)
								{
									num = hitData.m_registHit_Or_defaultHit_Or_weakHit;
								}
							}
							characterHandler.CharaBattle.RequestDamageMode();
							if (!this.m_BattleSystem.IsInUniqueSkillScene && !this.m_BattleSystem.IsRequestLastBlow)
							{
								int num2 = (!flag) ? hitData.m_registHit_Or_defaultHit_Or_weakHit : num;
								if (num2 != 1)
								{
									if (num2 != -1)
									{
										this.m_BattleSystem.BattleCamera.Shake(BattleCamera.eShakeType.Hit_Default);
									}
									else
									{
										this.m_BattleSystem.BattleCamera.Shake(BattleCamera.eShakeType.Hit_Regist);
									}
								}
								else
								{
									this.m_BattleSystem.BattleCamera.Shake(BattleCamera.eShakeType.Hit_Weak);
								}
								this.m_BattleSystem.TimeScaler.SetChangeTimeScale(0f, 0.033333335f, 1f);
							}
						}
					}
					this.m_BattleSystem.JudgeAndRequestLastBlow(this.m_Owner);
				}
				if (!isSolveOnSkip)
				{
					if (charaResult.GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.BecameStateAbnormal))
					{
						if (!characterHandler.CharaAnim.IsPlayingAnim("abnormal"))
						{
							characterHandler.CharaBattle.RequestIdleMode(0.25f, 0f, false);
							characterHandler.PlayVoice(eSoundVoiceListDB.voice_415, 1f);
						}
					}
					else if (charaResult.GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.RecoverdStateAbnormal))
					{
						if (characterHandler.CharaAnim.IsPlayingAnim("abnormal"))
						{
							characterHandler.CharaBattle.RequestIdleMode(0.25f, 0f, false);
						}
					}
					else
					{
						int autoFacialID = charaResult.GetAutoFacialID();
						if (autoFacialID != -1)
						{
							if (characterHandler != this.m_Owner)
							{
								characterHandler.CharaBattle.RequestAutoFacial(autoFacialID, 1f);
							}
							charaResult.SetAutoFacialID(-1);
						}
					}
				}
			}
			if (flag)
			{
				this.PlayDamageEffect(ev.m_DamageEffect, null, isCriticalHit, num);
			}
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x000313C0 File Offset: 0x0002F7C0
		private void UpdateEventDamageAnim()
		{
			if (this.m_SAP.m_evDamageAnim == null)
			{
				return;
			}
			int num = 1;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAP.m_evDamageAnim.Count; i++)
			{
				SkillActionEvent_DamageAnim skillActionEvent_DamageAnim = this.m_SAP.m_evDamageAnim[i];
				if (skillActionEvent_DamageAnim.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_DamageAnim.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					this.UpdateEventDamageAnim_Exec(skillActionEvent_DamageAnim, null);
				}
				if (skillActionEvent_DamageAnim.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x00031470 File Offset: 0x0002F870
		private void UpdateEventDamageAnim_Exec(SkillActionEvent_DamageAnim ev, CharacterHandler targetCharaHndl)
		{
			if (targetCharaHndl == null)
			{
				if (this.m_TgtCharaList != null)
				{
					for (int i = 0; i < this.m_TgtCharaList.Count; i++)
					{
						this.m_TgtCharaList[i].CharaBattle.RequestDamageMode();
					}
				}
			}
			else
			{
				targetCharaHndl.CharaBattle.RequestDamageMode();
			}
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x000314D8 File Offset: 0x0002F8D8
		private void UpdateEventDamageEffect()
		{
			if (this.m_SAP.m_evDamageEffect == null)
			{
				return;
			}
			int num = 2;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAP.m_evDamageEffect.Count; i++)
			{
				SkillActionEvent_DamageEffect skillActionEvent_DamageEffect = this.m_SAP.m_evDamageEffect[i];
				if (skillActionEvent_DamageEffect.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_DamageEffect.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					this.UpdateEventDamageEffect_Exec(skillActionEvent_DamageEffect, null);
				}
				if (skillActionEvent_DamageEffect.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x00031588 File Offset: 0x0002F988
		private void UpdateEventDamageEffect_Exec(SkillActionEvent_DamageEffect ev, CharacterHandler targetCharaHndl)
		{
			this.PlayDamageEffect(ev.m_DamageEffect, targetCharaHndl, false, 0);
		}

		// Token: 0x060007C8 RID: 1992 RVA: 0x0003159C File Offset: 0x0002F99C
		private void UpdateEventEffectPlay()
		{
			if (this.m_SAG.m_evEffectPlay == null)
			{
				return;
			}
			int num = 3;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evEffectPlay.Count; i++)
			{
				SkillActionEvent_EffectPlay skillActionEvent_EffectPlay = this.m_SAG.m_evEffectPlay[i];
				if (skillActionEvent_EffectPlay.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_EffectPlay.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					Vector3 offset = skillActionEvent_EffectPlay.m_TargetPosOffset * this.OwnerTransform.lossyScale.x;
					List<Vector3> effectPos = this.GetEffectPos(skillActionEvent_EffectPlay.m_TargetPosType, skillActionEvent_EffectPlay.m_TargetPosLocatorType, offset, false, null);
					for (int j = 0; j < effectPos.Count; j++)
					{
						string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_EffectPlay.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
						Vector3 position = effectPos[j];
						int sortingOrder = 1000;
						SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, effectID, ref position, ref sortingOrder);
						Quaternion rotation = (this.m_BattleSystem.MainCamera.transform.forward.z <= 0f) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
						EffectHandler effectHandler = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged(effectID, position, rotation, this.OwnerTransform, false, WrapMode.ClampForever, sortingOrder, 1f, true);
						if (effectHandler != null)
						{
							this.m_ActiveEffectList.Add(effectHandler);
						}
					}
				}
				if (skillActionEvent_EffectPlay.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007C9 RID: 1993 RVA: 0x00031780 File Offset: 0x0002FB80
		private void UpdateEventEffectProjectile_Straight()
		{
			if (this.m_SAG.m_evEffectProjectile_Straight == null)
			{
				return;
			}
			int num = 4;
			int num2 = -1;
			int i = this.m_CurrentDataIndices[num];
			while (i < this.m_SAG.m_evEffectProjectile_Straight.Count)
			{
				SkillActionEvent_EffectProjectile_Straight skillActionEvent_EffectProjectile_Straight = this.m_SAG.m_evEffectProjectile_Straight[i];
				if (skillActionEvent_EffectProjectile_Straight.m_Frame < this.m_PlayingKeyFrameOld || skillActionEvent_EffectProjectile_Straight.m_Frame > this.m_PlayingKeyFrame)
				{
					goto IL_1CB;
				}
				num2 = i;
				Vector3 offset = skillActionEvent_EffectProjectile_Straight.m_TargetPosOffset * this.OwnerTransform.lossyScale.x;
				List<CharacterHandler> out_targetCharaHndls = new List<CharacterHandler>();
				List<Vector3> effectPos = this.GetEffectPos(skillActionEvent_EffectProjectile_Straight.m_TargetPosType, skillActionEvent_EffectProjectile_Straight.m_TargetPosLocatorType, offset, false, out_targetCharaHndls);
				if (effectPos != null && effectPos.Count > 0)
				{
					int index = 0;
					string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_EffectProjectile_Straight.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
					Vector3 effectProjectileStartPos = this.GetEffectProjectileStartPos(skillActionEvent_EffectProjectile_Straight.m_StartPosType, skillActionEvent_EffectProjectile_Straight.m_StartPosLocatorType, skillActionEvent_EffectProjectile_Straight.m_StartPosOffset);
					int sortingOrder = 1000;
					SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, effectID, ref effectProjectileStartPos, ref sortingOrder);
					effectPos[index] = new Vector3(effectPos[index].x, effectPos[index].y, effectProjectileStartPos.z);
					SkillActionProjectile skillActionProjectile = new SkillActionProjectile(skillActionEvent_EffectProjectile_Straight.m_CallbackKey, effectID, sortingOrder, this.OwnerTransform, null);
					SkillActionProjectile skillActionProjectile2 = skillActionProjectile;
					skillActionProjectile2.OnArrival = (Action<string, Vector3, CharacterHandler>)Delegate.Combine(skillActionProjectile2.OnArrival, new Action<string, Vector3, CharacterHandler>(this.OnArrivalProjectile));
					skillActionProjectile.MoveStraight(false, effectProjectileStartPos, effectPos[index], skillActionEvent_EffectProjectile_Straight.m_Speed);
					this.m_ProjectileEffectList.Add(skillActionEvent_EffectProjectile_Straight.m_UniqueID, skillActionProjectile);
					goto IL_1CB;
				}
				IL_1E1:
				i++;
				continue;
				IL_1CB:
				if (skillActionEvent_EffectProjectile_Straight.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
				goto IL_1E1;
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CA RID: 1994 RVA: 0x0003199C File Offset: 0x0002FD9C
		private void UpdateEventEffectProjectile_Parabola()
		{
			if (this.m_SAG.m_evEffectProjectile_Parabola == null)
			{
				return;
			}
			int num = 5;
			int num2 = -1;
			int i = this.m_CurrentDataIndices[num];
			while (i < this.m_SAG.m_evEffectProjectile_Parabola.Count)
			{
				SkillActionEvent_EffectProjectile_Parabola skillActionEvent_EffectProjectile_Parabola = this.m_SAG.m_evEffectProjectile_Parabola[i];
				if (skillActionEvent_EffectProjectile_Parabola.m_Frame < this.m_PlayingKeyFrameOld || skillActionEvent_EffectProjectile_Parabola.m_Frame > this.m_PlayingKeyFrame)
				{
					goto IL_1D4;
				}
				num2 = i;
				Vector3 offset = skillActionEvent_EffectProjectile_Parabola.m_TargetPosOffset * this.OwnerTransform.lossyScale.x;
				List<CharacterHandler> out_targetCharaHndls = new List<CharacterHandler>();
				List<Vector3> effectPos = this.GetEffectPos(skillActionEvent_EffectProjectile_Parabola.m_TargetPosType, skillActionEvent_EffectProjectile_Parabola.m_TargetPosLocatorType, offset, false, out_targetCharaHndls);
				if (effectPos.Count > 0)
				{
					int index = 0;
					string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_EffectProjectile_Parabola.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
					Vector3 effectProjectileStartPos = this.GetEffectProjectileStartPos(skillActionEvent_EffectProjectile_Parabola.m_StartPosType, skillActionEvent_EffectProjectile_Parabola.m_StartPosLocatorType, skillActionEvent_EffectProjectile_Parabola.m_StartPosOffset);
					int sortingOrder = 1000;
					SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, effectID, ref effectProjectileStartPos, ref sortingOrder);
					effectPos[index] = new Vector3(effectPos[index].x, effectPos[index].y, effectProjectileStartPos.z);
					float arrivalSec = skillActionEvent_EffectProjectile_Parabola.m_ArrivalFrame * 0.033333335f;
					SkillActionProjectile skillActionProjectile = new SkillActionProjectile(skillActionEvent_EffectProjectile_Parabola.m_CallbackKey, effectID, sortingOrder, this.OwnerTransform, null);
					SkillActionProjectile skillActionProjectile2 = skillActionProjectile;
					skillActionProjectile2.OnArrival = (Action<string, Vector3, CharacterHandler>)Delegate.Combine(skillActionProjectile2.OnArrival, new Action<string, Vector3, CharacterHandler>(this.OnArrivalProjectile));
					skillActionProjectile.MoveParabola(false, effectProjectileStartPos, effectPos[index], arrivalSec, skillActionEvent_EffectProjectile_Parabola.m_GravityAccel);
					this.m_ProjectileEffectList.Add(skillActionEvent_EffectProjectile_Parabola.m_UniqueID, skillActionProjectile);
					goto IL_1D4;
				}
				IL_1EA:
				i++;
				continue;
				IL_1D4:
				if (skillActionEvent_EffectProjectile_Parabola.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
				goto IL_1EA;
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CB RID: 1995 RVA: 0x00031BC0 File Offset: 0x0002FFC0
		private void UpdateEventEffectProjectile_Penetrate()
		{
			if (this.m_SAG.m_evEffectProjectile_Penetrate == null)
			{
				return;
			}
			int num = 6;
			int num2 = -1;
			int i = this.m_CurrentDataIndices[num];
			while (i < this.m_SAG.m_evEffectProjectile_Penetrate.Count)
			{
				SkillActionEvent_EffectProjectile_Penetrate skillActionEvent_EffectProjectile_Penetrate = this.m_SAG.m_evEffectProjectile_Penetrate[i];
				if (skillActionEvent_EffectProjectile_Penetrate.m_Frame < this.m_PlayingKeyFrameOld || skillActionEvent_EffectProjectile_Penetrate.m_Frame > this.m_PlayingKeyFrame)
				{
					goto IL_1EA;
				}
				num2 = i;
				Vector3 offset = skillActionEvent_EffectProjectile_Penetrate.m_TargetPosOffset * this.OwnerTransform.lossyScale.x;
				List<CharacterHandler> list = new List<CharacterHandler>();
				List<Vector3> effectPos = this.GetEffectPos(skillActionEvent_EffectProjectile_Penetrate.m_TargetPosType, skillActionEvent_EffectProjectile_Penetrate.m_TargetPosLocatorType, offset, false, list);
				if (effectPos != null && effectPos.Count > 0)
				{
					string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_EffectProjectile_Penetrate.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
					Vector3 effectProjectileStartPos = this.GetEffectProjectileStartPos(skillActionEvent_EffectProjectile_Penetrate.m_StartPosType, skillActionEvent_EffectProjectile_Penetrate.m_StartPosLocatorType, skillActionEvent_EffectProjectile_Penetrate.m_StartPosOffset);
					int sortingOrder = 1000;
					SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, effectID, ref effectProjectileStartPos, ref sortingOrder);
					for (int j = 0; j < effectPos.Count; j++)
					{
						effectPos[j] = new Vector3(effectPos[j].x, effectPos[j].y, effectProjectileStartPos.z);
					}
					SkillActionProjectile skillActionProjectile = new SkillActionProjectile(skillActionEvent_EffectProjectile_Penetrate.m_CallbackKey, effectID, sortingOrder, this.OwnerTransform, list);
					SkillActionProjectile skillActionProjectile2 = skillActionProjectile;
					skillActionProjectile2.OnArrival = (Action<string, Vector3, CharacterHandler>)Delegate.Combine(skillActionProjectile2.OnArrival, new Action<string, Vector3, CharacterHandler>(this.OnArrivalProjectile));
					skillActionProjectile.MovePenetrate(false, effectProjectileStartPos, effectPos, skillActionEvent_EffectProjectile_Penetrate.m_Speed, skillActionEvent_EffectProjectile_Penetrate.m_DelaySec, skillActionEvent_EffectProjectile_Penetrate.m_AliveSec);
					this.m_ProjectileEffectList.Add(skillActionEvent_EffectProjectile_Penetrate.m_UniqueID, skillActionProjectile);
					goto IL_1EA;
				}
				IL_200:
				i++;
				continue;
				IL_1EA:
				if (skillActionEvent_EffectProjectile_Penetrate.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
				goto IL_200;
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CC RID: 1996 RVA: 0x00031DFC File Offset: 0x000301FC
		private void UpdateEventEffectAttach()
		{
			if (this.m_SAG.m_evEffectAttach == null)
			{
				return;
			}
			int num = 7;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evEffectAttach.Count; i++)
			{
				SkillActionEvent_EffectAttach skillActionEvent_EffectAttach = this.m_SAG.m_evEffectAttach[i];
				if (skillActionEvent_EffectAttach.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_EffectAttach.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_EffectAttach.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
					Transform effectAttachTo = this.GetEffectAttachTo(skillActionEvent_EffectAttach.m_AttachTo, skillActionEvent_EffectAttach.m_AttachToLocatorType);
					Vector3 position = effectAttachTo.position;
					int sortingOrder = 1000;
					SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, effectID, ref position, ref sortingOrder);
					Quaternion rotation = (this.m_BattleSystem.MainCamera.transform.forward.z <= 0f) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
					EffectHandler effectHandler = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged(effectID, skillActionEvent_EffectAttach.m_PosOffset, rotation, effectAttachTo, true, WrapMode.ClampForever, sortingOrder, 1f, false);
					if (effectHandler != null)
					{
						SkillActionPlayer.AttachedEffectData attachedEffectData = new SkillActionPlayer.AttachedEffectData();
						attachedEffectData.m_EffectHndl = effectHandler;
						attachedEffectData.m_OriginalPosZ = position.z;
						this.m_AttachedEffectList.Add(skillActionEvent_EffectAttach.m_UniqueID, attachedEffectData);
						eSkillActionEffectAttachTo attachTo = skillActionEvent_EffectAttach.m_AttachTo;
						if (attachTo != eSkillActionEffectAttachTo.WeaponLeft)
						{
							if (attachTo == eSkillActionEffectAttachTo.WeaponRight)
							{
								this.m_WeaponThrowDatas[1].m_AttachedEffectUniqueIDs.Add(skillActionEvent_EffectAttach.m_UniqueID);
							}
						}
						else
						{
							this.m_WeaponThrowDatas[0].m_AttachedEffectUniqueIDs.Add(skillActionEvent_EffectAttach.m_UniqueID);
						}
					}
				}
				if (skillActionEvent_EffectAttach.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CD RID: 1997 RVA: 0x00032018 File Offset: 0x00030418
		private void UpdateEventEffectDetach()
		{
			if (this.m_SAG.m_evEffectDetach == null)
			{
				return;
			}
			int num = 8;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evEffectDetach.Count; i++)
			{
				SkillActionEvent_EffectDetach skillActionEvent_EffectDetach = this.m_SAG.m_evEffectDetach[i];
				if (skillActionEvent_EffectDetach.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_EffectDetach.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					this.DetachEffect(skillActionEvent_EffectDetach.m_UniqueID);
				}
				if (skillActionEvent_EffectDetach.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CE RID: 1998 RVA: 0x000320CC File Offset: 0x000304CC
		private void UpdateEventTrailAttach()
		{
			if (this.m_SAG.m_evTrailAttach == null)
			{
				return;
			}
			int num = 9;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evTrailAttach.Count; i++)
			{
				SkillActionEvent_TrailAttach skillActionEvent_TrailAttach = this.m_SAG.m_evTrailAttach[i];
				if (skillActionEvent_TrailAttach.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_TrailAttach.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					string effectID = SkillActionUtility.ConvertEffectID(skillActionEvent_TrailAttach.m_EffectID, this.m_BaseElementType, this.m_SAG.m_SettingClassType, this.m_SAG.m_SettingMotionID, this.m_SAG.m_SettingGrade);
					Transform trailAttachTo = this.GetTrailAttachTo(skillActionEvent_TrailAttach.m_AttachTo, skillActionEvent_TrailAttach.m_AttachToLocatorType);
					int sortingOrder = 1000;
					TrailHandler trailHandler = SingletonMonoBehaviour<EffectManager>.Inst.BorrowTrail(effectID, skillActionEvent_TrailAttach.m_PosOffset, Quaternion.identity, trailAttachTo, true, sortingOrder);
					if (trailHandler != null)
					{
						trailHandler.Setup(this.m_BaseElementType);
						this.m_AttachedTrailList.Add(skillActionEvent_TrailAttach.m_UniqueID, trailHandler);
						eSkillActionTrailAttachTo attachTo = skillActionEvent_TrailAttach.m_AttachTo;
						if (attachTo != eSkillActionTrailAttachTo.WeaponLeft)
						{
							if (attachTo == eSkillActionTrailAttachTo.WeaponRight)
							{
								this.m_WeaponThrowDatas[1].m_AttachedTrailUniqueIDs.Add(skillActionEvent_TrailAttach.m_UniqueID);
							}
						}
						else
						{
							this.m_WeaponThrowDatas[0].m_AttachedTrailUniqueIDs.Add(skillActionEvent_TrailAttach.m_UniqueID);
						}
					}
				}
				if (skillActionEvent_TrailAttach.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007CF RID: 1999 RVA: 0x0003226C File Offset: 0x0003066C
		private void UpdateEventTrailDetach()
		{
			if (this.m_SAG.m_evTrailDetach == null)
			{
				return;
			}
			int num = 10;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evTrailDetach.Count; i++)
			{
				SkillActionEvent_TrailDetach skillActionEvent_TrailDetach = this.m_SAG.m_evTrailDetach[i];
				if (skillActionEvent_TrailDetach.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_TrailDetach.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					this.DetachTrail(skillActionEvent_TrailDetach.m_UniqueID);
				}
				if (skillActionEvent_TrailDetach.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D0 RID: 2000 RVA: 0x00032324 File Offset: 0x00030724
		private void UpdateEventWeaponThrow_Parabola()
		{
			if (this.m_SAG.m_evWeaponThrow_Parabola == null)
			{
				return;
			}
			int num = 11;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evWeaponThrow_Parabola.Count; i++)
			{
				SkillActionEvent_WeaponThrow_Parabola skillActionEvent_WeaponThrow_Parabola = this.m_SAG.m_evWeaponThrow_Parabola[i];
				if (skillActionEvent_WeaponThrow_Parabola.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_WeaponThrow_Parabola.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					int num3 = (!skillActionEvent_WeaponThrow_Parabola.m_IsLeftWeapon) ? 1 : 0;
					if (this.m_Owner.CharaAnim.WeaponModelSets[num3] != null)
					{
						Transform transform = this.m_Owner.CharaAnim.WeaponModelSets[num3].m_Transform;
						this.m_WeaponThrowDatas[num3].m_RotationTarget = transform;
						this.m_WeaponThrowDatas[num3].m_RotationSource = transform.parent;
						this.m_WeaponThrowDatas[num3].m_RotationLinkAnim = skillActionEvent_WeaponThrow_Parabola.m_IsRotationLinkAnim;
						this.m_WeaponThrowDatas[num3].m_IsAutoDetachOnArrival = skillActionEvent_WeaponThrow_Parabola.m_IsAutoDetachOnArrival;
						transform.SetParent(this.OwnerTransform, true);
						Transform handTransform = this.m_Owner.CharaAnim.GetHandTransform(skillActionEvent_WeaponThrow_Parabola.m_IsLeftWeapon);
						Vector3 position = handTransform.position;
						Vector3 offset = skillActionEvent_WeaponThrow_Parabola.m_TargetPosOffset * this.OwnerTransform.lossyScale.x;
						Vector3 weaponThrowPos = this.GetWeaponThrowPos(skillActionEvent_WeaponThrow_Parabola.m_TargetPosType, skillActionEvent_WeaponThrow_Parabola.m_TargetPosLocatorType, offset, this.m_WeaponThrowDatas[num3].m_ThrowTargetCharaHndl);
						position = new Vector3(position.x, position.y, weaponThrowPos.z);
						transform.position = position;
						float arrivalSec = skillActionEvent_WeaponThrow_Parabola.m_ArrivalFrame * 0.033333335f;
						this.m_WeaponThrowDatas[num3].m_Movement = new SkillActionMovementParabola(transform, false, position, weaponThrowPos, arrivalSec, skillActionEvent_WeaponThrow_Parabola.m_GravityAccel);
						this.m_WeaponThrowDatas[num3].m_Movement.Play();
						this.m_WeaponThrowDatas[num3].m_CallbackKey = skillActionEvent_WeaponThrow_Parabola.m_CallbackKey;
					}
				}
				if (skillActionEvent_WeaponThrow_Parabola.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D1 RID: 2001 RVA: 0x00032558 File Offset: 0x00030958
		private void UpdateEventWeaponVisible()
		{
			if (this.m_SAG.m_evWeaponVisible == null)
			{
				return;
			}
			int num = 12;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evWeaponVisible.Count; i++)
			{
				SkillActionEvent_WeaponVisible skillActionEvent_WeaponVisible = this.m_SAG.m_evWeaponVisible[i];
				if (skillActionEvent_WeaponVisible.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_WeaponVisible.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					CharacterDefine.eWeaponIndex wpnIndex = (!skillActionEvent_WeaponVisible.m_IsLeftWeapon) ? CharacterDefine.eWeaponIndex.R : CharacterDefine.eWeaponIndex.L;
					this.m_Owner.SetEnableRenderWeapon(wpnIndex, skillActionEvent_WeaponVisible.m_IsVisible, false);
					if (skillActionEvent_WeaponVisible.m_IsPosReset)
					{
						this.m_Owner.CharaAnim.ReAttachWeapon(wpnIndex);
					}
				}
				if (skillActionEvent_WeaponVisible.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D2 RID: 2002 RVA: 0x00032648 File Offset: 0x00030A48
		private void UpdateEventWeaponReset()
		{
			if (this.m_SAG.m_evWeaponReset == null)
			{
				return;
			}
			int num = 13;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evWeaponReset.Count; i++)
			{
				SkillActionEvent_WeaponReset skillActionEvent_WeaponReset = this.m_SAG.m_evWeaponReset[i];
				if (skillActionEvent_WeaponReset.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_WeaponReset.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					for (int j = 0; j < 2; j++)
					{
						this.m_Owner.CharaAnim.ReAttachWeapon((CharacterDefine.eWeaponIndex)j);
						if (this.m_WeaponThrowDatas[j] != null && this.m_WeaponThrowDatas[j].m_Movement != null)
						{
							this.m_WeaponThrowDatas[j].m_Movement = null;
						}
					}
					this.m_Owner.RevertDefaultEnableRenderWeapon();
				}
				if (skillActionEvent_WeaponReset.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D3 RID: 2003 RVA: 0x00032754 File Offset: 0x00030B54
		private void UpdateEventPlaySE()
		{
			if (this.m_SAG.m_evPlaySE == null)
			{
				return;
			}
			int num = 14;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evPlaySE.Count; i++)
			{
				SkillActionEvent_PlaySE skillActionEvent_PlaySE = this.m_SAG.m_evPlaySE[i];
				if (skillActionEvent_PlaySE.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_PlaySE.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE("Battle", skillActionEvent_PlaySE.m_CueName, 1f, 0, -1, -1);
				}
				if (skillActionEvent_PlaySE.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D4 RID: 2004 RVA: 0x00032820 File Offset: 0x00030C20
		private void UpdateEventPlayVoice()
		{
			if (this.m_SAG.m_evPlayVoice == null)
			{
				return;
			}
			int num = 15;
			int num2 = -1;
			for (int i = this.m_CurrentDataIndices[num]; i < this.m_SAG.m_evPlayVoice.Count; i++)
			{
				SkillActionEvent_PlayVoice skillActionEvent_PlayVoice = this.m_SAG.m_evPlayVoice[i];
				if (skillActionEvent_PlayVoice.m_Frame >= this.m_PlayingKeyFrameOld && skillActionEvent_PlayVoice.m_Frame <= this.m_PlayingKeyFrame)
				{
					num2 = i;
					if (string.IsNullOrEmpty(skillActionEvent_PlayVoice.m_VoiceCueName))
					{
						if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
						{
							this.m_Owner.PlayVoice(BattleDefine.VOICE_SKILLS[this.m_Command.SkillParam.m_SkillType], 1f);
						}
					}
					else
					{
						this.m_Owner.PlayVoice(skillActionEvent_PlayVoice.m_VoiceCueName, 1f);
					}
				}
				if (skillActionEvent_PlayVoice.m_Frame > this.m_PlayingKeyFrame)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				this.m_CurrentDataIndices[num] = num2 + 1;
			}
		}

		// Token: 0x060007D5 RID: 2005 RVA: 0x00032934 File Offset: 0x00030D34
		private void UpdateEffectSet()
		{
			foreach (CharacterHandler key in this.m_PlayingEffectSets.Keys)
			{
				if (this.m_PlayingEffectSets[key] != null)
				{
					this.m_PlayingEffectSets[key].Update();
				}
			}
			foreach (CharacterHandler key2 in this.m_StackEffectSets.Keys)
			{
				if (this.m_StackEffectSets[key2] != null)
				{
					if (this.m_StackEffectSets[key2].Count > 0)
					{
						if (!this.m_PlayingEffectSets.ContainsKey(key2) || !this.m_PlayingEffectSets[key2].IsPlaying())
						{
							if (this.m_StackEffectSets[key2][0] != null)
							{
								this.m_PlayingEffectSets.AddOrReplace(key2, this.m_StackEffectSets[key2][0]);
								this.m_StackEffectSets[key2].RemoveAt(0);
								base.StartCoroutine(this.m_PlayingEffectSets[key2].Play(this.m_BattleSystem.MainCamera, this.OwnerTransform, this.m_BattleSystem.BattleUI));
							}
						}
					}
				}
			}
		}

		// Token: 0x060007D6 RID: 2006 RVA: 0x00032AD4 File Offset: 0x00030ED4
		private List<Vector3> GetEffectPos(eSkillActionEffectPosType posType, eSkillActionLocatorType locatorType, Vector3 offset, bool isLocalPos, List<CharacterHandler> out_targetCharaHndls)
		{
			List<Vector3> list = new List<Vector3>();
			List<CharacterHandler> list2 = null;
			if (posType == eSkillActionEffectPosType.Self || posType == eSkillActionEffectPosType.SkillTarget_Tgt || posType == eSkillActionEffectPosType.SkillTarget_My)
			{
				if (posType != eSkillActionEffectPosType.SkillTarget_Tgt)
				{
					if (posType != eSkillActionEffectPosType.SkillTarget_My)
					{
						list2 = new List<CharacterHandler>();
						list2.Add(this.m_Owner);
					}
					else
					{
						list2 = this.m_MyCharaList;
					}
				}
				else
				{
					list2 = this.m_TgtCharaList;
				}
				if (list2 == null)
				{
					return null;
				}
				for (int i = 0; i < list2.Count; i++)
				{
					Vector3 a;
					if (locatorType != eSkillActionLocatorType.Body)
					{
						a = ((!isLocalPos) ? list2[i].CacheTransform.position : list2[i].CacheTransform.localPosition);
					}
					else
					{
						a = list2[i].CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, isLocalPos);
					}
					list.Add(a + offset);
				}
			}
			else if (posType == eSkillActionEffectPosType.PartyCenter_Tgt)
			{
				list2 = this.m_TgtCharaList;
				Vector3 a2 = (!isLocalPos) ? this.GetPartyCenter(true).position : this.GetPartyCenter(true).localPosition;
				list.Add(a2 + offset);
			}
			else if (posType == eSkillActionEffectPosType.PartyCenter_My)
			{
				list2 = this.m_MyCharaList;
				Vector3 a3 = (!isLocalPos) ? this.GetPartyCenter(false).position : this.GetPartyCenter(false).localPosition;
				list.Add(a3 + offset);
			}
			if (out_targetCharaHndls != null && list2 != null)
			{
				out_targetCharaHndls.AddRange(list2);
			}
			return list;
		}

		// Token: 0x060007D7 RID: 2007 RVA: 0x00032C68 File Offset: 0x00031068
		private Vector3 GetEffectProjectileStartPos(eSkillActionEffectProjectileStartPosType posType, eSkillActionLocatorType locatorType, Vector3 offset)
		{
			Vector3 result = Vector3.zero;
			switch (posType)
			{
			case eSkillActionEffectProjectileStartPosType.Self:
			{
				Vector3 a;
				if (locatorType != eSkillActionLocatorType.Body)
				{
					a = this.m_Owner.CacheTransform.position;
				}
				else
				{
					a = this.m_Owner.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false);
				}
				result = a + offset;
				break;
			}
			case eSkillActionEffectProjectileStartPosType.WeaponLeft:
			{
				Vector3 weaponLocatorPos = this.m_Owner.CharaAnim.GetWeaponLocatorPos(CharacterDefine.eWeaponIndex.L, (int)locatorType, false);
				result = weaponLocatorPos + offset;
				break;
			}
			case eSkillActionEffectProjectileStartPosType.WeaponRight:
			{
				Vector3 weaponLocatorPos2 = this.m_Owner.CharaAnim.GetWeaponLocatorPos(CharacterDefine.eWeaponIndex.R, (int)locatorType, false);
				result = weaponLocatorPos2 + offset;
				break;
			}
			}
			return result;
		}

		// Token: 0x060007D8 RID: 2008 RVA: 0x00032D28 File Offset: 0x00031128
		private Transform GetEffectAttachTo(eSkillActionEffectAttachTo attachTo, eSkillActionLocatorType locatorType)
		{
			Transform result = null;
			if (attachTo != eSkillActionEffectAttachTo.WeaponLeft)
			{
				if (attachTo == eSkillActionEffectAttachTo.WeaponRight)
				{
					result = this.m_Owner.CharaAnim.GetWeaponLocator(CharacterDefine.eWeaponIndex.R, (int)locatorType);
				}
			}
			else
			{
				result = this.m_Owner.CharaAnim.GetWeaponLocator(CharacterDefine.eWeaponIndex.L, (int)locatorType);
			}
			return result;
		}

		// Token: 0x060007D9 RID: 2009 RVA: 0x00032D84 File Offset: 0x00031184
		private Transform GetTrailAttachTo(eSkillActionTrailAttachTo attachTo, eSkillActionLocatorType locatorType)
		{
			Transform result = null;
			if (attachTo != eSkillActionTrailAttachTo.WeaponLeft)
			{
				if (attachTo == eSkillActionTrailAttachTo.WeaponRight)
				{
					result = this.m_Owner.CharaAnim.GetWeaponLocator(CharacterDefine.eWeaponIndex.R, (int)locatorType);
				}
			}
			else
			{
				result = this.m_Owner.CharaAnim.GetWeaponLocator(CharacterDefine.eWeaponIndex.L, (int)locatorType);
			}
			return result;
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x00032DE0 File Offset: 0x000311E0
		private Vector3 GetWeaponThrowPos(eSkillActionWeaponThrowPosType posType, eSkillActionLocatorType locatorType, Vector3 offset, CharacterHandler out_throwTargetcharaHndls)
		{
			Vector3 result = Vector3.zero;
			if (posType != eSkillActionWeaponThrowPosType.Self && posType != eSkillActionWeaponThrowPosType.SkillTarget_Tgt)
			{
				if (posType == eSkillActionWeaponThrowPosType.PartyCenter_Tgt)
				{
					result = this.GetPartyCenter(true).position + offset;
				}
			}
			else
			{
				CharacterHandler characterHandler = null;
				if (posType == eSkillActionWeaponThrowPosType.Self)
				{
					characterHandler = this.m_Owner;
				}
				else if (this.m_TgtCharaList != null && this.m_TgtCharaList[0])
				{
					characterHandler = this.m_TgtCharaList[0];
				}
				if (characterHandler != null)
				{
					Vector3 a;
					if (locatorType != eSkillActionLocatorType.Body)
					{
						a = characterHandler.CacheTransform.position;
					}
					else
					{
						a = characterHandler.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false);
					}
					result = a + offset;
				}
			}
			return result;
		}

		// Token: 0x060007DB RID: 2011 RVA: 0x00032EB4 File Offset: 0x000312B4
		private Transform GetPartyCenter(bool isTgt)
		{
			if (this.m_BattleSystem.IsInUniqueSkillScene)
			{
				Transform locator = this.m_BattleSystem.UniqueSkillScene.GetLocator(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1);
				if (locator != null)
				{
					return locator;
				}
			}
			if (isTgt)
			{
				if (this.m_Owner.CharaBattle.MyParty.IsPlayerParty)
				{
					return this.m_EnemyPartyCenter;
				}
				return this.m_PlayerPartyCenter;
			}
			else
			{
				if (this.m_Owner.CharaBattle.MyParty.IsPlayerParty)
				{
					return this.m_PlayerPartyCenter;
				}
				return this.m_EnemyPartyCenter;
			}
		}

		// Token: 0x060007DC RID: 2012 RVA: 0x00032F48 File Offset: 0x00031348
		private void DetachEffect(short uniqueID)
		{
			SkillActionPlayer.AttachedEffectData attachedEffectData = null;
			if (this.m_AttachedEffectList.TryGetValue(uniqueID, out attachedEffectData))
			{
				attachedEffectData.m_EffectHndl.CacheTransform.SetParent(this.OwnerTransform, true);
				attachedEffectData.m_IsDetached = true;
			}
		}

		// Token: 0x060007DD RID: 2013 RVA: 0x00032F88 File Offset: 0x00031388
		private void DetachTrail(short uniqueID)
		{
			TrailHandler trailHandler = null;
			if (this.m_AttachedTrailList.TryGetValue(uniqueID, out trailHandler))
			{
				trailHandler.CacheTransform.SetParent(this.OwnerTransform);
			}
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x00032FBC File Offset: 0x000313BC
		private void OnArrivalProjectile(string callbackKey, Vector3 arrivalPos, CharacterHandler targetCharaHndl)
		{
			if (!this.m_IsPlaying)
			{
				return;
			}
			if (this.m_SAP.m_CallbackDict != null && this.m_SAP.m_CallbackDict.ContainsKey(callbackKey))
			{
				string key = (!(targetCharaHndl != null)) ? callbackKey : (callbackKey + targetCharaHndl.name);
				if (this.m_CallbackEnd.ContainsKey(key))
				{
					return;
				}
				this.m_CallbackEnd.Add(key, true);
				SkillActionPlan.CallbackData callbackData = this.m_SAP.m_CallbackDict[callbackKey];
				if (callbackData.m_evSolve != null)
				{
					for (int i = 0; i < callbackData.m_evSolve.Count; i++)
					{
						if (callbackData.m_evSolve[i].m_Frame != -1)
						{
							this.UpdateEventSolve_Exec(callbackData.m_evSolve[i], targetCharaHndl, false);
						}
					}
				}
			}
		}

		// Token: 0x060007DF RID: 2015 RVA: 0x000330A0 File Offset: 0x000314A0
		private void PlayDamageEffect(SkillActionInfo_DamageEffect info, CharacterHandler targetCharaHndl, bool isCriticalHit, int registHit_Or_DefaultHit_Or_WeakHit)
		{
			string text = SkillActionUtility.ConvertDmgEffectID(info.TempEffectID, this.m_BaseElementType, info.m_Grade, info.m_EffectType);
			List<Vector3> effectPos;
			if (info.m_EffectType == eDmgEffectType.PL_All)
			{
				effectPos = this.GetEffectPos(eSkillActionEffectPosType.PartyCenter_Tgt, eSkillActionLocatorType.Body, Vector3.zero, false, null);
			}
			else
			{
				List<CharacterHandler> list = new List<CharacterHandler>();
				effectPos = this.GetEffectPos(eSkillActionEffectPosType.SkillTarget_Tgt, eSkillActionLocatorType.Body, Vector3.zero, false, list);
				if (effectPos != null && targetCharaHndl != null)
				{
					for (int i = 0; i < effectPos.Count; i++)
					{
						if (targetCharaHndl == list[i])
						{
							effectPos.Add(effectPos[i]);
							effectPos.RemoveRange(0, effectPos.Count - 1);
							break;
						}
					}
				}
			}
			if (effectPos != null)
			{
				SoundManager soundMng = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng;
				Quaternion rotation = (this.m_BattleSystem.MainCamera.transform.forward.z <= 0f) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
				for (int j = 0; j < effectPos.Count; j++)
				{
					if (!string.IsNullOrEmpty(text))
					{
						Vector3 position = effectPos[j];
						int sortingOrder = 1000;
						SkillActionUtility.CorrectEffectParam(this.m_BattleSystem.MainCamera, text, ref position, ref sortingOrder);
						EffectHandler effectHandler = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged(text, position, rotation, this.OwnerTransform, false, WrapMode.ClampForever, sortingOrder, 1f, true);
						if (effectHandler != null)
						{
							if (info.m_EffectType == eDmgEffectType.PL_Single || info.m_EffectType == eDmgEffectType.EN_Bite || info.m_EffectType == eDmgEffectType.EN_Blow || info.m_EffectType == eDmgEffectType.EN_Claw || info.m_EffectType == eDmgEffectType.EN_Slash)
							{
								effectHandler.ApplyControllParam_Visible((int)this.m_BaseElementType);
							}
							this.m_ActiveEffectList.Add(effectHandler);
							if (info.m_EffectType == eDmgEffectType.PL_Single || info.m_EffectType == eDmgEffectType.PL_All)
							{
								eSoundSeListDB cueID = eSoundSeListDB.BTL_ACT_HIT_1_SLASH;
								if (info.m_EffectType == eDmgEffectType.PL_Single && info.m_Grade < SkillActionInfo_DamageEffect.DAMAGE_EFFECT_RANGE[1])
								{
									switch (info.m_SeType)
									{
									case eDmgEffectSeType.Slash:
										cueID = eSoundSeListDB.BTL_ACT_HIT_1_SLASH;
										break;
									case eDmgEffectSeType.Blow:
										cueID = eSoundSeListDB.BTL_ACT_HIT_1_BLOW;
										break;
									case eDmgEffectSeType.Magic:
										cueID = eSoundSeListDB.BTL_ACT_HIT_1_MAGIC;
										break;
									case eDmgEffectSeType.Bite:
										cueID = eSoundSeListDB.BTL_ACT_HIT_1_BITE;
										break;
									case eDmgEffectSeType.Claw:
										cueID = eSoundSeListDB.BTL_ACT_HIT_1_SCRATCH;
										break;
									}
								}
								else
								{
									switch (info.m_SeType)
									{
									case eDmgEffectSeType.Slash:
										cueID = eSoundSeListDB.BTL_ACT_HIT_2_SLASH;
										break;
									case eDmgEffectSeType.Blow:
										cueID = eSoundSeListDB.BTL_ACT_HIT_2_BLOW;
										break;
									case eDmgEffectSeType.Magic:
										cueID = eSoundSeListDB.BTL_ACT_HIT_2_MAGIC;
										break;
									case eDmgEffectSeType.Bite:
										cueID = eSoundSeListDB.BTL_ACT_HIT_2_BITE;
										break;
									case eDmgEffectSeType.Claw:
										cueID = eSoundSeListDB.BTL_ACT_HIT_2_SCRATCH;
										break;
									}
								}
								soundMng.PlaySE(cueID, 1f, 0, -1, -1);
							}
						}
					}
					if (isCriticalHit)
					{
						soundMng.PlaySE(eSoundSeListDB.BTL_ACT_CRITICAL, 1f, 0, -1, -1);
					}
					else if (registHit_Or_DefaultHit_Or_WeakHit != 1)
					{
						if (registHit_Or_DefaultHit_Or_WeakHit == -1)
						{
							soundMng.PlaySE(eSoundSeListDB.BTL_ACT_ELEMENT_WEAK, 1f, 0, -1, -1);
						}
					}
					else
					{
						soundMng.PlaySE(eSoundSeListDB.BTL_ACT_ELEMENT_STRONG, 1f, 0, -1, -1);
					}
				}
			}
		}

		// Token: 0x04000789 RID: 1929
		private SkillActionPlan m_SAP;

		// Token: 0x0400078A RID: 1930
		private SkillActionGraphics m_SAG;

		// Token: 0x0400078B RID: 1931
		private CharacterHandler m_Owner;

		// Token: 0x0400078C RID: 1932
		private BattleCommandData m_Command;

		// Token: 0x0400078D RID: 1933
		private List<CharacterHandler> m_TgtCharaList;

		// Token: 0x0400078E RID: 1934
		private List<CharacterHandler> m_MyCharaList;

		// Token: 0x0400078F RID: 1935
		private BattleSystem m_BattleSystem;

		// Token: 0x04000790 RID: 1936
		private Transform m_PlayerPartyCenter;

		// Token: 0x04000791 RID: 1937
		private Transform m_EnemyPartyCenter;

		// Token: 0x04000792 RID: 1938
		private List<EffectHandler> m_ActiveEffectList;

		// Token: 0x04000793 RID: 1939
		private Dictionary<short, SkillActionPlayer.AttachedEffectData> m_AttachedEffectList;

		// Token: 0x04000794 RID: 1940
		private Dictionary<short, TrailHandler> m_AttachedTrailList;

		// Token: 0x04000795 RID: 1941
		private Dictionary<short, SkillActionProjectile> m_ProjectileEffectList;

		// Token: 0x04000796 RID: 1942
		private SkillActionPlayer.WeaponThrowData[] m_WeaponThrowDatas;

		// Token: 0x04000797 RID: 1943
		private Dictionary<CharacterHandler, List<SkillActionEffectSet>> m_StackEffectSets = new Dictionary<CharacterHandler, List<SkillActionEffectSet>>();

		// Token: 0x04000798 RID: 1944
		private Dictionary<CharacterHandler, SkillActionEffectSet> m_PlayingEffectSets = new Dictionary<CharacterHandler, SkillActionEffectSet>();

		// Token: 0x04000799 RID: 1945
		private eElementType m_BaseElementType = eElementType.None;

		// Token: 0x0400079A RID: 1946
		private bool m_IsOverRefAnimTime;

		// Token: 0x0400079B RID: 1947
		private float m_PlayingSec;

		// Token: 0x0400079C RID: 1948
		private int m_PlayingKeyFrame;

		// Token: 0x0400079D RID: 1949
		private int m_PlayingKeyFrameOld;

		// Token: 0x0400079E RID: 1950
		private int[] m_CurrentDataIndices = new int[16];

		// Token: 0x0400079F RID: 1951
		private Dictionary<string, bool> m_CallbackEnd = new Dictionary<string, bool>();

		// Token: 0x040007A0 RID: 1952
		private bool m_IsPlaying;

		// Token: 0x0200012D RID: 301
		private class AttachedEffectData
		{
			// Token: 0x060007E1 RID: 2017 RVA: 0x00033419 File Offset: 0x00031819
			public void Clear()
			{
				this.m_EffectHndl = null;
				this.m_IsDetached = false;
			}

			// Token: 0x040007A2 RID: 1954
			public EffectHandler m_EffectHndl;

			// Token: 0x040007A3 RID: 1955
			public float m_OriginalPosZ;

			// Token: 0x040007A4 RID: 1956
			public bool m_IsDetached;
		}

		// Token: 0x0200012E RID: 302
		private class WeaponThrowData
		{
			// Token: 0x060007E3 RID: 2019 RVA: 0x00033447 File Offset: 0x00031847
			public void Clear()
			{
				this.m_CallbackKey = null;
				this.m_ThrowTargetCharaHndl = null;
				this.m_Movement = null;
				this.m_RotationTarget = null;
				this.m_RotationSource = null;
				this.m_AttachedEffectUniqueIDs.Clear();
				this.m_AttachedTrailUniqueIDs.Clear();
			}

			// Token: 0x040007A5 RID: 1957
			public SkillActionMovementParabola m_Movement;

			// Token: 0x040007A6 RID: 1958
			public string m_CallbackKey;

			// Token: 0x040007A7 RID: 1959
			public CharacterHandler m_ThrowTargetCharaHndl;

			// Token: 0x040007A8 RID: 1960
			public bool m_RotationLinkAnim;

			// Token: 0x040007A9 RID: 1961
			public Transform m_RotationTarget;

			// Token: 0x040007AA RID: 1962
			public Transform m_RotationSource;

			// Token: 0x040007AB RID: 1963
			public bool m_IsAutoDetachOnArrival;

			// Token: 0x040007AC RID: 1964
			public List<short> m_AttachedEffectUniqueIDs = new List<short>();

			// Token: 0x040007AD RID: 1965
			public List<short> m_AttachedTrailUniqueIDs = new List<short>();
		}

		// Token: 0x0200012F RID: 303
		public struct RefAnimTime
		{
			// Token: 0x040007AE RID: 1966
			public float m_PlayingSec;

			// Token: 0x040007AF RID: 1967
			public float m_MaxSec;
		}

		// Token: 0x02000130 RID: 304
		// (Invoke) Token: 0x060007E5 RID: 2021
		public delegate SkillActionPlayer.RefAnimTime UpdateRefAnimTimeDelegate();
	}
}
