﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000658 RID: 1624
	public sealed class LoginManager
	{
		// Token: 0x1400002C RID: 44
		// (add) Token: 0x060020B0 RID: 8368 RVA: 0x000AE3A4 File Offset: 0x000AC7A4
		// (remove) Token: 0x060020B1 RID: 8369 RVA: 0x000AE3DC File Offset: 0x000AC7DC
		private event Action<MeigewwwParam, PlayerResponseTypes.Login> m_OnResponse_Login;

		// Token: 0x1400002D RID: 45
		// (add) Token: 0x060020B2 RID: 8370 RVA: 0x000AE414 File Offset: 0x000AC814
		// (remove) Token: 0x060020B3 RID: 8371 RVA: 0x000AE44C File Offset: 0x000AC84C
		private event Action<MeigewwwParam, PlayerResponseTypes.Getall> m_OnResponse_GetAll;

		// Token: 0x1400002E RID: 46
		// (add) Token: 0x060020B4 RID: 8372 RVA: 0x000AE484 File Offset: 0x000AC884
		// (remove) Token: 0x060020B5 RID: 8373 RVA: 0x000AE4BC File Offset: 0x000AC8BC
		private event Action<MeigewwwParam, PlayerResponseTypes.Signup> m_OnResponse_Signup;

		// Token: 0x1400002F RID: 47
		// (add) Token: 0x060020B6 RID: 8374 RVA: 0x000AE4F4 File Offset: 0x000AC8F4
		// (remove) Token: 0x060020B7 RID: 8375 RVA: 0x000AE52C File Offset: 0x000AC92C
		private event Action<bool> m_OnResponse_PlayerMoveGet;

		// Token: 0x14000030 RID: 48
		// (add) Token: 0x060020B8 RID: 8376 RVA: 0x000AE564 File Offset: 0x000AC964
		// (remove) Token: 0x060020B9 RID: 8377 RVA: 0x000AE59C File Offset: 0x000AC99C
		private event Action<string> m_OnResponse_PlayerMoveSet;

		// Token: 0x14000031 RID: 49
		// (add) Token: 0x060020BA RID: 8378 RVA: 0x000AE5D4 File Offset: 0x000AC9D4
		// (remove) Token: 0x060020BB RID: 8379 RVA: 0x000AE60C File Offset: 0x000ACA0C
		private event Action<MeigewwwParam, PlayerResponseTypes.Reset> m_OnResponse_PlayerReset;

		// Token: 0x14000032 RID: 50
		// (add) Token: 0x060020BC RID: 8380 RVA: 0x000AE644 File Offset: 0x000ACA44
		// (remove) Token: 0x060020BD RID: 8381 RVA: 0x000AE67C File Offset: 0x000ACA7C
		private event Action m_OnResponse_LoginBonus;

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x060020BE RID: 8382 RVA: 0x000AE6B2 File Offset: 0x000ACAB2
		public bool IsLogined
		{
			get
			{
				return this.m_IsLogined;
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x060020BF RID: 8383 RVA: 0x000AE6BA File Offset: 0x000ACABA
		public string MoveCode
		{
			get
			{
				return this.m_MoveCode;
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x060020C0 RID: 8384 RVA: 0x000AE6C2 File Offset: 0x000ACAC2
		public string MovePassward
		{
			get
			{
				return this.m_MovePassward;
			}
		}

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x060020C1 RID: 8385 RVA: 0x000AE6CA File Offset: 0x000ACACA
		public DateTime MoveDeadLine
		{
			get
			{
				return this.m_MoveDeadLine;
			}
		}

		// Token: 0x060020C2 RID: 8386 RVA: 0x000AE6D2 File Offset: 0x000ACAD2
		public void ForceResetOnReturnTitle()
		{
			this.m_IsLogined = false;
			this.m_MoveCode = null;
			this.m_MovePassward = null;
			this.m_LoginBonusSuccessCount = 0;
			this.m_LoginBonusNormal = null;
			this.m_LoginBonusEvents = null;
			this.m_LoginBonusTotal = null;
		}

		// Token: 0x060020C3 RID: 8387 RVA: 0x000AE708 File Offset: 0x000ACB08
		public bool Request_Login(Action<MeigewwwParam, PlayerResponseTypes.Login> onResponse_Login, Action<MeigewwwParam, PlayerResponseTypes.Getall> onResponse_GetAll)
		{
			if (string.IsNullOrEmpty(AccessSaveData.Inst.UUID) || string.IsNullOrEmpty(AccessSaveData.Inst.AccessToken))
			{
				return false;
			}
			this.m_OnResponse_Login = onResponse_Login;
			this.m_OnResponse_GetAll = onResponse_GetAll;
			MeigewwwParam wwwParam = PlayerRequest.Login(new PlayerRequestTypes.Login
			{
				uuid = AccessSaveData.Inst.UUID,
				accessToken = AccessSaveData.Inst.AccessToken,
				platform = Platform.Get(),
				appVersion = "1.0.3"
			}, new MeigewwwParam.Callback(this.OnResponse_Login));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x060020C4 RID: 8388 RVA: 0x000AE7B4 File Offset: 0x000ACBB4
		private void OnResponse_Login(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Login login = PlayerResponse.Login(wwwParam, ResponseCommon.DialogType.None, null);
			if (login == null)
			{
				if (this.m_OnResponse_Login != null)
				{
					this.m_OnResponse_Login(wwwParam, login);
				}
				return;
			}
			ResultCode result = login.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.PLAYER_NOT_FOUND)
				{
					if (result == ResultCode.UUID_NOT_FOUND)
					{
						AccessSaveData.Inst.Reset();
						AccessSaveData.Inst.Save();
					}
				}
			}
			else
			{
				this.m_IsLogined = true;
				NetworkQueueManager.SetSessionId(login.sessionId);
				this.Request_GetAll();
			}
			if (this.m_OnResponse_Login != null)
			{
				this.m_OnResponse_Login(wwwParam, login);
			}
		}

		// Token: 0x060020C5 RID: 8389 RVA: 0x000AE86C File Offset: 0x000ACC6C
		public void Request_GetAll()
		{
			PlayerRequestTypes.Getall param = new PlayerRequestTypes.Getall();
			MeigewwwParam wwwParam = PlayerRequest.Getall(param, new MeigewwwParam.Callback(this.OnResponse_GetAll));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020C6 RID: 8390 RVA: 0x000AE8A8 File Offset: 0x000ACCA8
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			PlayerResponseTypes.Getall getall = PlayerResponse.Getall(wwwParam, ResponseCommon.DialogType.None, null);
			if (getall == null)
			{
				if (this.m_OnResponse_GetAll != null)
				{
					this.m_OnResponse_GetAll(wwwParam, getall);
				}
				return;
			}
			ResultCode result = getall.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = inst.UserDataMng;
				inst.NotificationCtrl.SetPushFlag(NotificationController.ePush.ForUIFlag, getall.flagPush == 1);
				inst.NotificationCtrl.SetPushFlag(NotificationController.ePush.InfoFromMng, getall.flagPush == 1);
				inst.NotificationCtrl.SetPushFlag(NotificationController.ePush.StaminaFull, getall.flagStamina == 1);
				APIUtility.wwwToUserData(getall.player, userDataMng.UserData);
				APIUtility.wwwToUserData(getall.managedCharacters, userDataMng.UserCharaDatas);
				APIUtility.wwwToUserData(getall.managedNamedTypes, userDataMng.UserNamedDatas);
				APIUtility.wwwToUserData(getall.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(getall.managedWeapons, userDataMng.UserWeaponDatas);
				APIUtility.wwwToUserData(getall.managedMasterOrbs, userDataMng.UserMasterOrbDatas);
				APIUtility.wwwToUserData(getall.managedBattleParties, userDataMng.UserBattlePartyDatas);
				APIUtility.wwwToUserData(getall.managedFieldPartyMembers, userDataMng.UserFieldChara);
				APIUtility.wwwToUserData(getall.managedTownFacilities, userDataMng.UserTownObjDatas);
				APIUtility.wwwToUserData(getall.managedTowns, userDataMng.UserTownData);
				APIUtility.wwwToUserData(getall.managedRoomObjects, userDataMng.UserRoomObjDatas);
				APIUtility.wwwToUserData(getall.managedRooms, null);
				APIUtility.wwwToUserData(getall.supportCharacters, userDataMng.UserSupportPartyDatas);
				inst.QuestMng.SaveResponse = getall.questData;
				inst.QuestMng.WorkRecvID = getall.orderReceiveId;
				inst.QuestMng.WorkQuestID = BattleUtility.GetQuestIDFromSaveResponse(getall.questData);
				userDataMng.UserAdvData.SetAdvIDs(getall.advIds);
				userDataMng.NoticeFriendCount = getall.friendProposedCount;
				userDataMng.NoticePresentCount = getall.presentCount;
				userDataMng.NoticeMissionCount = getall.missionClearCount;
				inst.TutorialMng.SetTutoarialSeq((eTutorialSeq)getall.stepCode);
				inst.TutorialMng.SetTipsIDs(getall.tipIds);
				inst.GlobalParam.SelectedSupportPartyIndex = userDataMng.GetActivateSupportPartyIndex();
				AccessSaveData.Inst.MyCode = getall.player.myCode;
				AccessSaveData.Inst.Save();
			}
			if (this.m_OnResponse_GetAll != null)
			{
				this.m_OnResponse_GetAll(wwwParam, getall);
			}
		}

		// Token: 0x060020C7 RID: 8391 RVA: 0x000AEAF4 File Offset: 0x000ACEF4
		public void Request_Signup(string userName, string preCode, Action<MeigewwwParam, PlayerResponseTypes.Signup> onResponse)
		{
			this.m_OnResponse_Signup = onResponse;
			PlayerRequestTypes.Signup signup = new PlayerRequestTypes.Signup();
			AccessSaveData.Inst.UUID = APIUtility.GenerateUUID();
			signup.uuid = AccessSaveData.Inst.UUID;
			signup.platform = Platform.Get();
			signup.name = userName;
			signup.stepCode = 1;
			MeigewwwParam wwwParam = PlayerRequest.Signup(signup, new MeigewwwParam.Callback(this.OnResponse_Signup));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020C8 RID: 8392 RVA: 0x000AEB70 File Offset: 0x000ACF70
		private void OnResponse_Signup(MeigewwwParam wwwParam)
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			PlayerResponseTypes.Signup signup = PlayerResponse.Signup(wwwParam, ResponseCommon.DialogType.None, null);
			if (signup == null)
			{
				if (this.m_OnResponse_Signup != null)
				{
					this.m_OnResponse_Signup(wwwParam, signup);
				}
				return;
			}
			ResultCode result = signup.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					if (result != ResultCode.PLAYER_ALREADY_EXISTS)
					{
					}
				}
			}
			else
			{
				AccessSaveData.Inst.AccessToken = signup.accessToken;
				AccessSaveData.Inst.Save();
				inst.TutorialMng.SetTutoarialSeq(eTutorialSeq.SignupComplete_NextIsPresentGet);
			}
			if (this.m_OnResponse_Signup != null)
			{
				this.m_OnResponse_Signup(wwwParam, signup);
			}
		}

		// Token: 0x060020C9 RID: 8393 RVA: 0x000AEC24 File Offset: 0x000AD024
		public void Request_PlayerMoveGet(string movePassward, Action<bool> onResponse)
		{
			this.m_OnResponse_PlayerMoveGet = onResponse;
			PlayerRequestTypes.Moveget moveget = new PlayerRequestTypes.Moveget();
			moveget.password = movePassward;
			this.m_MovePassward = movePassward;
			MeigewwwParam wwwParam = PlayerRequest.Moveget(moveget, new MeigewwwParam.Callback(this.OnResponse_PlayerMoveGet));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020CA RID: 8394 RVA: 0x000AEC74 File Offset: 0x000AD074
		private void OnResponse_PlayerMoveGet(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Moveget moveget = PlayerResponse.Moveget(wwwParam, ResponseCommon.DialogType.None, null);
			if (moveget == null)
			{
				return;
			}
			ResultCode result = moveget.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, null);
			}
			else
			{
				this.m_MoveCode = moveget.moveCode;
				this.m_MoveDeadLine = moveget.moveDeadline;
				this.m_OnResponse_PlayerMoveGet.Call(false);
			}
		}

		// Token: 0x060020CB RID: 8395 RVA: 0x000AECD8 File Offset: 0x000AD0D8
		public void Request_PlayerMoveSet(string moveCode, string movePassward, Action<string> onResponse)
		{
			this.m_OnResponse_PlayerMoveSet = onResponse;
			PlayerRequestTypes.Moveset moveset = new PlayerRequestTypes.Moveset();
			moveset.moveCode = moveCode;
			moveset.movePassword = movePassward;
			this.m_WorkUUID = APIUtility.GenerateUUID();
			moveset.uuid = this.m_WorkUUID;
			moveset.platform = Platform.Get();
			MeigewwwParam wwwParam = PlayerRequest.Moveset(moveset, new MeigewwwParam.Callback(this.OnResponse_PlayerMoveSet));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020CC RID: 8396 RVA: 0x000AED4C File Offset: 0x000AD14C
		private void OnResponse_PlayerMoveSet(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Moveset moveset = PlayerResponse.Moveset(wwwParam, ResponseCommon.DialogType.None, null);
			if (moveset == null)
			{
				return;
			}
			ResultCode result = moveset.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				this.m_OnResponse_PlayerMoveSet.Call(result.ToMessageString());
			}
			else
			{
				AccessSaveData.Inst.UUID = this.m_WorkUUID;
				AccessSaveData.Inst.AccessToken = moveset.accessToken;
				AccessSaveData.Inst.Save();
				SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.CancelAll();
				this.m_OnResponse_PlayerMoveSet.Call(null);
			}
		}

		// Token: 0x060020CD RID: 8397 RVA: 0x000AEDDC File Offset: 0x000AD1DC
		public void Request_PlayerReset(Action<MeigewwwParam, PlayerResponseTypes.Reset> onResponse)
		{
			this.m_OnResponse_PlayerReset = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Reset(new PlayerRequestTypes.Reset
			{
				uuid = AccessSaveData.Inst.UUID,
				accessToken = AccessSaveData.Inst.AccessToken
			}, new MeigewwwParam.Callback(this.OnResponse_PlayerReset));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020CE RID: 8398 RVA: 0x000AEE40 File Offset: 0x000AD240
		private void OnResponse_PlayerReset(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Reset reset = PlayerResponse.Reset(wwwParam, ResponseCommon.DialogType.None, null);
			if (reset == null)
			{
				if (this.m_OnResponse_PlayerReset != null)
				{
					this.m_OnResponse_PlayerReset(wwwParam, reset);
				}
				return;
			}
			ResultCode result = reset.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				this.m_IsLogined = false;
				AccessSaveData.Inst.Reset();
				AccessSaveData.Inst.Save();
				LocalSaveData.Inst.ResetSave();
				SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.CancelAll();
			}
			if (this.m_OnResponse_PlayerReset != null)
			{
				this.m_OnResponse_PlayerReset(wwwParam, reset);
			}
		}

		// Token: 0x060020CF RID: 8399 RVA: 0x000AEEE0 File Offset: 0x000AD2E0
		public void Request_LoginBonus(Action onResponse, bool isConnectingIcon = true)
		{
			this.m_OnResponse_LoginBonus = onResponse;
			PlayerRequestTypes.Loginbonus param = new PlayerRequestTypes.Loginbonus();
			MeigewwwParam wwwParam = PlayerRequest.Loginbonus(param, new MeigewwwParam.Callback(this.OnResponse_LoginBonus));
			NetworkQueueManager.Request(wwwParam);
			if (isConnectingIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
		}

		// Token: 0x060020D0 RID: 8400 RVA: 0x000AEF28 File Offset: 0x000AD328
		private void OnResponse_LoginBonus(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Loginbonus loginbonus = PlayerResponse.Loginbonus(wwwParam, ResponseCommon.DialogType.None, null);
			if (loginbonus == null)
			{
				return;
			}
			ResultCode result = loginbonus.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, null);
			}
			else
			{
				this.m_LoginBonusNormal = null;
				this.m_LoginBonusEvents = null;
				this.m_LoginBonusTotal = null;
				if (loginbonus.bonuses != null && loginbonus.bonuses.Length > 0)
				{
					this.m_LoginBonusSuccessTime = loginbonus.serverTime;
					this.m_LoginBonusSuccessCount++;
					for (int i = 0; i < loginbonus.bonuses.Length; i++)
					{
						if (loginbonus.bonuses[i].type == 0)
						{
							this.m_LoginBonusNormal = new LoginBonus();
							LoginManager.wwwConvert(loginbonus.bonuses[i], this.m_LoginBonusNormal);
							break;
						}
					}
					int num = 0;
					for (int j = 0; j < loginbonus.bonuses.Length; j++)
					{
						if (loginbonus.bonuses[j].type == 1)
						{
							num++;
						}
					}
					if (num > 0)
					{
						int num2 = 0;
						this.m_LoginBonusEvents = new LoginBonus[num];
						for (int k = 0; k < loginbonus.bonuses.Length; k++)
						{
							if (loginbonus.bonuses[k].type == 1)
							{
								this.m_LoginBonusEvents[num2] = new LoginBonus();
								LoginManager.wwwConvert(loginbonus.bonuses[k], this.m_LoginBonusEvents[num2]);
								num2++;
							}
						}
					}
					for (int l = 0; l < loginbonus.bonuses.Length; l++)
					{
						if (loginbonus.bonuses[l].type == 2)
						{
							this.m_LoginBonusTotal = new LoginBonusTotal();
							LoginManager.wwwConvert(loginbonus.bonuses[l], this.m_LoginBonusTotal);
							break;
						}
					}
				}
				this.m_OnResponse_LoginBonus.Call();
			}
		}

		// Token: 0x060020D1 RID: 8401 RVA: 0x000AF108 File Offset: 0x000AD508
		private static void wwwConvert(Bonus src, LoginBonus dest)
		{
			dest.m_ID = src.bonusId;
			dest.m_DayIndex = src.dayIndex;
			dest.m_Days = new LoginBonusDay[src.bonusDays.Length];
			for (int i = 0; i < src.bonusDays.Length; i++)
			{
				LoginBonusDay loginBonusDay = new LoginBonusDay();
				loginBonusDay.m_IconID = src.bonusDays[i].iconId;
				int num = src.bonusDays[i].bonusItems.Length;
				loginBonusDay.m_Presents = new LoginBonusPresent[num];
				for (int j = 0; j < num; j++)
				{
					LoginBonusPresent loginBonusPresent = new LoginBonusPresent();
					loginBonusPresent.m_PresentType = (ePresentType)src.bonusDays[i].bonusItems[j].type;
					loginBonusPresent.m_PresentID = src.bonusDays[i].bonusItems[j].objId;
					loginBonusPresent.m_Amount = src.bonusDays[i].bonusItems[j].amount;
					loginBonusDay.m_Presents[j] = loginBonusPresent;
				}
				dest.m_Days[i] = loginBonusDay;
			}
		}

		// Token: 0x060020D2 RID: 8402 RVA: 0x000AF20C File Offset: 0x000AD60C
		private static void wwwConvert(Bonus src, LoginBonusTotal dest)
		{
			dest.m_Day = src.dayIndex;
			if (src.bonusDays.Length > 0)
			{
				int num = 0;
				int num2 = src.bonusDays[num].bonusItems.Length;
				LoginBonusPresent[] array = new LoginBonusPresent[num2];
				for (int i = 0; i < num2; i++)
				{
					array[i] = new LoginBonusPresent
					{
						m_PresentType = (ePresentType)src.bonusDays[num].bonusItems[i].type,
						m_PresentID = src.bonusDays[num].bonusItems[i].objId,
						m_Amount = src.bonusDays[num].bonusItems[i].amount
					};
				}
				dest.m_Presents = array;
			}
		}

		// Token: 0x060020D3 RID: 8403 RVA: 0x000AF2C4 File Offset: 0x000AD6C4
		public bool IsLoginBonusCall()
		{
			return this.m_LoginBonusSuccessCount <= 0 || SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Day != this.m_LoginBonusSuccessTime.Day || SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Month != this.m_LoginBonusSuccessTime.Month;
		}

		// Token: 0x060020D4 RID: 8404 RVA: 0x000AF328 File Offset: 0x000AD728
		public bool IsAvailableLoginBonus()
		{
			return this.m_LoginBonusNormal != null || this.m_LoginBonusEvents != null || this.m_LoginBonusTotal != null;
		}

		// Token: 0x060020D5 RID: 8405 RVA: 0x000AF352 File Offset: 0x000AD752
		public LoginBonus GetLoginBonusNormal()
		{
			return this.m_LoginBonusNormal;
		}

		// Token: 0x060020D6 RID: 8406 RVA: 0x000AF35A File Offset: 0x000AD75A
		public LoginBonus[] GetLoginBonusEvents()
		{
			return this.m_LoginBonusEvents;
		}

		// Token: 0x060020D7 RID: 8407 RVA: 0x000AF362 File Offset: 0x000AD762
		public LoginBonusTotal GetLoginBonusTotal()
		{
			return this.m_LoginBonusTotal;
		}

		// Token: 0x04002714 RID: 10004
		private bool m_IsLogined;

		// Token: 0x04002715 RID: 10005
		private string m_WorkUUID;

		// Token: 0x04002716 RID: 10006
		private string m_MoveCode;

		// Token: 0x04002717 RID: 10007
		private string m_MovePassward;

		// Token: 0x04002718 RID: 10008
		private DateTime m_MoveDeadLine;

		// Token: 0x04002719 RID: 10009
		private DateTime m_LoginBonusSuccessTime;

		// Token: 0x0400271A RID: 10010
		private int m_LoginBonusSuccessCount;

		// Token: 0x0400271B RID: 10011
		private LoginBonus m_LoginBonusNormal;

		// Token: 0x0400271C RID: 10012
		private LoginBonus[] m_LoginBonusEvents;

		// Token: 0x0400271D RID: 10013
		private LoginBonusTotal m_LoginBonusTotal;
	}
}
