﻿using System;

namespace Star
{
	// Token: 0x0200010F RID: 271
	public enum eDmgEffectSeType
	{
		// Token: 0x040006F2 RID: 1778
		None = -1,
		// Token: 0x040006F3 RID: 1779
		Slash,
		// Token: 0x040006F4 RID: 1780
		Blow,
		// Token: 0x040006F5 RID: 1781
		Magic,
		// Token: 0x040006F6 RID: 1782
		Bite,
		// Token: 0x040006F7 RID: 1783
		Claw
	}
}
