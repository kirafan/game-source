﻿using System;

namespace Star
{
	// Token: 0x020004C4 RID: 1220
	public class TrainingState : GameStateBase
	{
		// Token: 0x060017FA RID: 6138 RVA: 0x0007CE49 File Offset: 0x0007B249
		public TrainingState(TrainingMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x060017FB RID: 6139 RVA: 0x0007CE5F File Offset: 0x0007B25F
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x060017FC RID: 6140 RVA: 0x0007CE62 File Offset: 0x0007B262
		public override void OnStateEnter()
		{
		}

		// Token: 0x060017FD RID: 6141 RVA: 0x0007CE64 File Offset: 0x0007B264
		public override void OnStateExit()
		{
		}

		// Token: 0x060017FE RID: 6142 RVA: 0x0007CE66 File Offset: 0x0007B266
		public override void OnDispose()
		{
		}

		// Token: 0x060017FF RID: 6143 RVA: 0x0007CE68 File Offset: 0x0007B268
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001800 RID: 6144 RVA: 0x0007CE6B File Offset: 0x0007B26B
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001EC4 RID: 7876
		protected TrainingMain m_Owner;

		// Token: 0x04001EC5 RID: 7877
		protected int m_NextState = -1;
	}
}
