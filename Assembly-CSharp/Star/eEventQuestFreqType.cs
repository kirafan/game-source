﻿using System;

namespace Star
{
	// Token: 0x02000231 RID: 561
	public enum eEventQuestFreqType
	{
		// Token: 0x04000F58 RID: 3928
		None,
		// Token: 0x04000F59 RID: 3929
		EveryYear,
		// Token: 0x04000F5A RID: 3930
		Monthly,
		// Token: 0x04000F5B RID: 3931
		Weekly,
		// Token: 0x04000F5C RID: 3932
		EveryDay
	}
}
