﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200012A RID: 298
	public class SkillActionPlan : ScriptableObject, ISerializationCallbackReceiver
	{
		// Token: 0x060007B0 RID: 1968 RVA: 0x0002F3A8 File Offset: 0x0002D7A8
		public void OnBeforeSerialize()
		{
		}

		// Token: 0x060007B1 RID: 1969 RVA: 0x0002F3AC File Offset: 0x0002D7AC
		public void OnAfterDeserialize()
		{
			if (this.m_CallbackDatas != null && this.m_CallbackDatas.Count > 0)
			{
				this.m_CallbackDict = new Dictionary<string, SkillActionPlan.CallbackData>();
				for (int i = 0; i < this.m_CallbackDatas.Count; i++)
				{
					if (this.m_CallbackDict.ContainsKey(this.m_CallbackDatas[i].m_Key))
					{
						this.m_CallbackDict[this.m_CallbackDatas[i].m_Key] = this.m_CallbackDatas[i];
					}
					else
					{
						this.m_CallbackDict.Add(this.m_CallbackDatas[i].m_Key, this.m_CallbackDatas[i]);
					}
				}
			}
		}

		// Token: 0x060007B2 RID: 1970 RVA: 0x0002F474 File Offset: 0x0002D874
		public Dictionary<string, int> GetEffectIDs(eElementType elementType)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			if (this.m_evSolve != null)
			{
				for (int i = 0; i < this.m_evSolve.Count; i++)
				{
					if (this.m_evSolve[i].m_IsEnableDamageEffect)
					{
						if (this.m_evSolve[i].m_DamageEffect != null)
						{
							string text = SkillActionUtility.ConvertDmgEffectID(this.m_evSolve[i].m_DamageEffect.TempEffectID, elementType, this.m_evSolve[i].m_DamageEffect.m_Grade, this.m_evSolve[i].m_DamageEffect.m_EffectType);
							if (!string.IsNullOrEmpty(text))
							{
								if (dictionary.ContainsKey(text))
								{
									Dictionary<string, int> dictionary2;
									string key;
									(dictionary2 = dictionary)[key = text] = dictionary2[key] + 1;
								}
								else
								{
									dictionary.Add(text, 1);
								}
							}
						}
					}
				}
			}
			if (this.m_evDamageEffect != null)
			{
				for (int j = 0; j < this.m_evDamageEffect.Count; j++)
				{
					if (this.m_evDamageEffect[j].m_DamageEffect != null)
					{
						string text2 = SkillActionUtility.ConvertDmgEffectID(this.m_evDamageEffect[j].m_DamageEffect.TempEffectID, elementType, this.m_evDamageEffect[j].m_DamageEffect.m_Grade, this.m_evDamageEffect[j].m_DamageEffect.m_EffectType);
						if (!string.IsNullOrEmpty(text2))
						{
							if (dictionary.ContainsKey(text2))
							{
								Dictionary<string, int> dictionary2;
								string key2;
								(dictionary2 = dictionary)[key2 = text2] = dictionary2[key2] + 1;
							}
							else
							{
								dictionary.Add(text2, 1);
							}
						}
					}
				}
			}
			if (this.m_CallbackDatas != null)
			{
				for (int k = 0; k < this.m_CallbackDatas.Count; k++)
				{
					if (this.m_CallbackDatas[k].m_evSolve != null)
					{
						for (int l = 0; l < this.m_CallbackDatas[k].m_evSolve.Count; l++)
						{
							if (this.m_CallbackDatas[k].m_evSolve[l] != null)
							{
								string text3 = SkillActionUtility.ConvertDmgEffectID(this.m_CallbackDatas[k].m_evSolve[l].m_DamageEffect.TempEffectID, elementType, this.m_CallbackDatas[k].m_evSolve[l].m_DamageEffect.m_Grade, this.m_CallbackDatas[k].m_evSolve[l].m_DamageEffect.m_EffectType);
								if (!string.IsNullOrEmpty(text3))
								{
									if (dictionary.ContainsKey(text3))
									{
										Dictionary<string, int> dictionary2;
										string key3;
										(dictionary2 = dictionary)[key3 = text3] = dictionary2[key3] + 1;
									}
									else
									{
										dictionary.Add(text3, 1);
									}
								}
							}
						}
					}
				}
			}
			return dictionary;
		}

		// Token: 0x04000781 RID: 1921
		public string m_ID;

		// Token: 0x04000782 RID: 1922
		public List<SkillActionEvent_Solve> m_evSolve;

		// Token: 0x04000783 RID: 1923
		public List<SkillActionEvent_DamageAnim> m_evDamageAnim;

		// Token: 0x04000784 RID: 1924
		public List<SkillActionEvent_DamageEffect> m_evDamageEffect;

		// Token: 0x04000785 RID: 1925
		public List<SkillActionPlan.CallbackData> m_CallbackDatas;

		// Token: 0x04000786 RID: 1926
		public Dictionary<string, SkillActionPlan.CallbackData> m_CallbackDict;

		// Token: 0x0200012B RID: 299
		[Serializable]
		public class CallbackData
		{
			// Token: 0x04000787 RID: 1927
			public string m_Key;

			// Token: 0x04000788 RID: 1928
			public List<SkillActionEvent_Solve> m_evSolve;
		}
	}
}
