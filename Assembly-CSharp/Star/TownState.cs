﻿using System;

namespace Star
{
	// Token: 0x020004BC RID: 1212
	public class TownState : GameStateBase
	{
		// Token: 0x060017C5 RID: 6085 RVA: 0x0007BCC1 File Offset: 0x0007A0C1
		public TownState(TownMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x060017C6 RID: 6086 RVA: 0x0007BCD7 File Offset: 0x0007A0D7
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x060017C7 RID: 6087 RVA: 0x0007BCDA File Offset: 0x0007A0DA
		public override void OnStateEnter()
		{
		}

		// Token: 0x060017C8 RID: 6088 RVA: 0x0007BCDC File Offset: 0x0007A0DC
		public override void OnStateExit()
		{
		}

		// Token: 0x060017C9 RID: 6089 RVA: 0x0007BCDE File Offset: 0x0007A0DE
		public override void OnDispose()
		{
		}

		// Token: 0x060017CA RID: 6090 RVA: 0x0007BCE0 File Offset: 0x0007A0E0
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x060017CB RID: 6091 RVA: 0x0007BCE3 File Offset: 0x0007A0E3
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001E95 RID: 7829
		protected TownMain m_Owner;

		// Token: 0x04001E96 RID: 7830
		protected int m_NextState = -1;
	}
}
