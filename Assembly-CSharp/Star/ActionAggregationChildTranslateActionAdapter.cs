﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000088 RID: 136
	[RequireComponent(typeof(CanvasGroup))]
	[AddComponentMenu("ActionAggregation/ChildChildTranslate")]
	[Serializable]
	public class ActionAggregationChildTranslateActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x06000422 RID: 1058 RVA: 0x000141B0 File Offset: 0x000125B0
		public override void Update()
		{
			if (base.GetState() == eActionAggregationState.Play)
			{
				float num = 1f;
				while (this.m_Current < this.m_FixedParamList.Count)
				{
					float num2 = this.m_FixedParamList[this.m_Current - 1].m_Frame / 30f;
					float num3 = this.m_FixedParamList[this.m_Current].m_Frame / 30f;
					num = (this.m_Time - num2) / (num3 - num2);
					if (num < 1f)
					{
						break;
					}
					num = 1f;
					this.m_Current++;
				}
				if (this.m_Current >= this.m_FixedParamList.Count)
				{
					this.ApplyParam(this.m_FixedParamList.Count, 0f);
					base.SetState(eActionAggregationState.Finish);
				}
				else
				{
					this.ApplyParam(this.m_Current, num);
				}
				this.m_Time += Time.deltaTime;
			}
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x000142B4 File Offset: 0x000126B4
		private void ApplyParam(int idx, float t)
		{
			ActionAggregationChildTranslateActionAdapter.Param param = this.m_FixedParamList[idx - 1];
			ActionAggregationChildTranslateActionAdapter.Param param2;
			if (this.m_FixedParamList.Count > idx)
			{
				param2 = this.m_FixedParamList[idx];
			}
			else
			{
				param2 = param;
			}
			this.m_RectTransform.localPosition = param.m_Position + (param2.m_Position - param.m_Position) * t;
			this.m_RectTransform.localEulerAngles = new Vector3(this.m_RectTransform.localEulerAngles.x, this.m_RectTransform.localEulerAngles.y, param.m_Rotate + (param2.m_Rotate - param.m_Rotate) * t);
			this.m_RectTransform.localScale = param.m_Scale + (param2.m_Scale - param.m_Scale) * t;
			this.m_CanvasGroup.alpha = param.m_Alpha + (param2.m_Alpha - param.m_Alpha) * t;
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x000143C4 File Offset: 0x000127C4
		private void CalcRandom()
		{
			Vector2 b = default(Vector2);
			Vector3 b2 = default(Vector3);
			for (int i = 0; i < this.m_ParamList.Count; i++)
			{
				if (this.m_FixedParamList.Count <= i)
				{
					this.m_FixedParamList.Add(new ActionAggregationChildTranslateActionAdapter.Param());
				}
				ActionAggregationChildTranslateActionAdapter.Param param = this.m_ParamList[i];
				ActionAggregationChildTranslateActionAdapter.Param param2 = this.m_FixedParamList[i];
				param2.m_Frame = param.m_Frame + UnityEngine.Random.Range(-param.m_FrameRand * 0.5f, param.m_FrameRand * 0.5f);
				b.x = UnityEngine.Random.Range(-param.m_PositionRand.x * 0.5f, param.m_PositionRand.x * 0.5f);
				b.y = UnityEngine.Random.Range(-param.m_PositionRand.y * 0.5f, param.m_PositionRand.y * 0.5f);
				param2.m_Position = param.m_Position + b;
				param2.m_Rotate = param.m_Rotate + UnityEngine.Random.Range(-param.m_RotateRand * 0.5f, param.m_RotateRand * 0.5f);
				b2.x = UnityEngine.Random.Range(-param.m_ScaleRand.x * 0.5f, param.m_ScaleRand.x * 0.5f);
				b2.y = UnityEngine.Random.Range(-param.m_ScaleRand.y * 0.5f, param.m_ScaleRand.y * 0.5f);
				b2.z = UnityEngine.Random.Range(-param.m_ScaleRand.z * 0.5f, param.m_ScaleRand.z * 0.5f);
				param2.m_Scale = param.m_Scale + b2;
				param2.m_Alpha = param.m_Alpha + UnityEngine.Random.Range(-param.m_AlphaRand * 0.5f, param.m_AlphaRand * 0.5f);
			}
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x000145D0 File Offset: 0x000129D0
		public override void PlayStartExtendProcess()
		{
			this.CalcRandom();
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			if (this.m_CanvasGroup == null)
			{
				this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			}
			this.m_Current = 1;
			this.ApplyParam(this.m_Current, 0f);
			this.m_Time = 0f;
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0001465D File Offset: 0x00012A5D
		public override void Skip()
		{
			this.ApplyParam(this.m_FixedParamList.Count, 0f);
			base.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0001467C File Offset: 0x00012A7C
		public override void RecoveryFinishToWaitExtendProcess()
		{
			this.m_Current = 1;
			this.m_Time = 0f;
			this.ApplyParam(1, 0f);
		}

		// Token: 0x04000235 RID: 565
		private const float FPS = 30f;

		// Token: 0x04000236 RID: 566
		public List<ActionAggregationChildTranslateActionAdapter.Param> m_ParamList;

		// Token: 0x04000237 RID: 567
		private List<ActionAggregationChildTranslateActionAdapter.Param> m_FixedParamList = new List<ActionAggregationChildTranslateActionAdapter.Param>();

		// Token: 0x04000238 RID: 568
		private int m_Current;

		// Token: 0x04000239 RID: 569
		private float m_Time;

		// Token: 0x0400023A RID: 570
		private GameObject m_GameObject;

		// Token: 0x0400023B RID: 571
		private RectTransform m_RectTransform;

		// Token: 0x0400023C RID: 572
		private CanvasGroup m_CanvasGroup;

		// Token: 0x02000089 RID: 137
		[Serializable]
		public class Param
		{
			// Token: 0x0400023D RID: 573
			public float m_Frame;

			// Token: 0x0400023E RID: 574
			public float m_FrameRand;

			// Token: 0x0400023F RID: 575
			public Vector2 m_Position = Vector2.zero;

			// Token: 0x04000240 RID: 576
			public Vector2 m_PositionRand = Vector2.zero;

			// Token: 0x04000241 RID: 577
			public float m_Rotate;

			// Token: 0x04000242 RID: 578
			public float m_RotateRand;

			// Token: 0x04000243 RID: 579
			public Vector3 m_Scale = Vector3.one;

			// Token: 0x04000244 RID: 580
			public Vector3 m_ScaleRand = Vector3.zero;

			// Token: 0x04000245 RID: 581
			public float m_Alpha = 1f;

			// Token: 0x04000246 RID: 582
			public float m_AlphaRand;
		}
	}
}
