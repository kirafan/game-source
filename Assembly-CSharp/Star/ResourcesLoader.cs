﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E2 RID: 1250
	public class ResourcesLoader
	{
		// Token: 0x060018E4 RID: 6372 RVA: 0x00081A4C File Offset: 0x0007FE4C
		public ResourcesLoader(int simultaneouslyNum)
		{
			this.m_SimultaneouslyNum = simultaneouslyNum;
			this.m_LoadedResources = new Dictionary<string, ResourceObjectHandler>();
			this.m_LoadingResources = new Dictionary<string, ResourceObjectHandler>();
			this.m_ReserveResources = new Dictionary<string, ResourceObjectHandler>();
			this.m_RemoveKeys = new List<string>();
		}

		// Token: 0x060018E5 RID: 6373 RVA: 0x00081A9C File Offset: 0x0007FE9C
		public void Update()
		{
			if (this.m_ReserveResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text in this.m_ReserveResources.Keys)
				{
					if (this.m_LoadingResources.Count >= this.m_SimultaneouslyNum)
					{
						break;
					}
					ResourceObjectHandler resourceObjectHandler = this.m_ReserveResources[text];
					ResourceObjectHandler.eLoadType loadType = resourceObjectHandler.LoadType;
					if (loadType != ResourceObjectHandler.eLoadType.Sprite)
					{
						if (loadType != ResourceObjectHandler.eLoadType.ScriptableObject)
						{
							resourceObjectHandler.Request = Resources.LoadAsync<GameObject>(resourceObjectHandler.Path);
						}
						else
						{
							resourceObjectHandler.Request = Resources.LoadAsync<ScriptableObject>(resourceObjectHandler.Path);
						}
					}
					else
					{
						resourceObjectHandler.Request = Resources.LoadAsync<Sprite>(resourceObjectHandler.Path);
					}
					this.m_LoadingResources.Add(resourceObjectHandler.Path, resourceObjectHandler);
					this.m_RemoveKeys.Add(text);
				}
				for (int i = 0; i < this.m_RemoveKeys.Count; i++)
				{
					this.m_ReserveResources.Remove(this.m_RemoveKeys[i]);
				}
			}
			if (this.m_LoadingResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text2 in this.m_LoadingResources.Keys)
				{
					ResourceObjectHandler resourceObjectHandler2 = this.m_LoadingResources[text2];
					if (resourceObjectHandler2.Request.isDone)
					{
						if (resourceObjectHandler2.ApplyObj())
						{
							this.m_LoadedResources.Add(resourceObjectHandler2.Path, resourceObjectHandler2);
						}
						this.m_RemoveKeys.Add(text2);
					}
				}
				for (int j = 0; j < this.m_RemoveKeys.Count; j++)
				{
					this.m_LoadingResources.Remove(this.m_RemoveKeys[j]);
				}
			}
			if (this.m_LoadedResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text3 in this.m_LoadedResources.Keys)
				{
					ResourceObjectHandler resourceObjectHandler3 = this.m_LoadedResources[text3];
					if (resourceObjectHandler3.RefCount <= 0)
					{
						resourceObjectHandler3.DestroyObj();
						this.m_RemoveKeys.Add(text3);
					}
				}
				for (int k = 0; k < this.m_RemoveKeys.Count; k++)
				{
					this.m_LoadedResources.Remove(this.m_RemoveKeys[k]);
				}
			}
			if (this.m_UnloadAllFlg && this.m_ReserveResources.Count <= 0 && this.m_LoadingResources.Count <= 0)
			{
				foreach (string key in this.m_LoadedResources.Keys)
				{
					this.m_LoadedResources[key].DestroyObj();
				}
				this.m_LoadedResources.Clear();
				this.m_LoadingResources.Clear();
				this.m_ReserveResources.Clear();
				this.m_UnloadAllFlg = false;
			}
		}

		// Token: 0x060018E6 RID: 6374 RVA: 0x00081E5C File Offset: 0x0008025C
		public bool IsLoadingResources()
		{
			return this.m_LoadingResources.Count > 0 || this.m_ReserveResources.Count > 0;
		}

		// Token: 0x060018E7 RID: 6375 RVA: 0x00081E80 File Offset: 0x00080280
		public bool IsLoadedResources()
		{
			return this.m_LoadedResources.Count > 0;
		}

		// Token: 0x060018E8 RID: 6376 RVA: 0x00081E90 File Offset: 0x00080290
		public ResourceObjectHandler LoadAsyncFromResources(string path, ResourceObjectHandler.eLoadType loadType = ResourceObjectHandler.eLoadType.GameObject)
		{
			if (this.m_UnloadAllFlg)
			{
				return null;
			}
			ResourceObjectHandler resourceObjectHandler = null;
			if (this.m_LoadedResources.TryGetValue(path, out resourceObjectHandler))
			{
				resourceObjectHandler.AddRef();
				return resourceObjectHandler;
			}
			if (this.m_LoadingResources.TryGetValue(path, out resourceObjectHandler))
			{
				resourceObjectHandler.AddRef();
				return resourceObjectHandler;
			}
			if (this.m_ReserveResources.TryGetValue(path, out resourceObjectHandler))
			{
				resourceObjectHandler.AddRef();
				return resourceObjectHandler;
			}
			resourceObjectHandler = new ResourceObjectHandler(null, path, loadType);
			this.m_ReserveResources.Add(path, resourceObjectHandler);
			return resourceObjectHandler;
		}

		// Token: 0x060018E9 RID: 6377 RVA: 0x00081F18 File Offset: 0x00080318
		public void UnloadFromResources(ResourceObjectHandler unloadHndl)
		{
			if (unloadHndl == null)
			{
				return;
			}
			ResourceObjectHandler resourceObjectHandler = null;
			if (!this.m_LoadedResources.TryGetValue(unloadHndl.Path, out resourceObjectHandler) && !this.m_LoadingResources.TryGetValue(unloadHndl.Path, out resourceObjectHandler) && !this.m_ReserveResources.TryGetValue(unloadHndl.Path, out resourceObjectHandler))
			{
				return;
			}
			if (resourceObjectHandler == unloadHndl)
			{
				resourceObjectHandler.RemoveRef();
			}
		}

		// Token: 0x060018EA RID: 6378 RVA: 0x00081F8C File Offset: 0x0008038C
		public void UnloadFromResources(string path)
		{
			ResourceObjectHandler resourceObjectHandler = null;
			if (!this.m_LoadedResources.TryGetValue(path, out resourceObjectHandler) && !this.m_LoadingResources.TryGetValue(path, out resourceObjectHandler) && !this.m_ReserveResources.TryGetValue(path, out resourceObjectHandler))
			{
				return;
			}
			resourceObjectHandler.RemoveRef();
		}

		// Token: 0x060018EB RID: 6379 RVA: 0x00081FDC File Offset: 0x000803DC
		public void UnloadAllResources()
		{
			if (this.m_UnloadAllFlg)
			{
				return;
			}
			this.m_UnloadAllFlg = true;
		}

		// Token: 0x060018EC RID: 6380 RVA: 0x00081FF1 File Offset: 0x000803F1
		public bool IsCompleteUnloadAllResources()
		{
			return !this.m_UnloadAllFlg;
		}

		// Token: 0x04001F6F RID: 8047
		private Dictionary<string, ResourceObjectHandler> m_LoadedResources;

		// Token: 0x04001F70 RID: 8048
		private Dictionary<string, ResourceObjectHandler> m_LoadingResources;

		// Token: 0x04001F71 RID: 8049
		private Dictionary<string, ResourceObjectHandler> m_ReserveResources;

		// Token: 0x04001F72 RID: 8050
		private int m_SimultaneouslyNum = 1;

		// Token: 0x04001F73 RID: 8051
		private bool m_UnloadAllFlg;

		// Token: 0x04001F74 RID: 8052
		private List<string> m_RemoveKeys;
	}
}
