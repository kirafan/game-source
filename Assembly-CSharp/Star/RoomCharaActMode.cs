﻿using System;

namespace Star
{
	// Token: 0x020005C4 RID: 1476
	public class RoomCharaActMode
	{
		// Token: 0x06001CB3 RID: 7347 RVA: 0x0009880D File Offset: 0x00096C0D
		public RoomCharaActMode()
		{
			this.m_NextActionPer = new RoomCharaActMode.NextActionState[8];
		}

		// Token: 0x06001CB4 RID: 7348 RVA: 0x00098824 File Offset: 0x00096C24
		public static RoomCharaActMode.eMode GetNameToMode(string fname)
		{
			for (RoomCharaActMode.eMode eMode = RoomCharaActMode.eMode.None; eMode < RoomCharaActMode.eMode.Max; eMode++)
			{
				if (eMode.ToString() == fname)
				{
					return eMode;
				}
			}
			return RoomCharaActMode.eMode.None;
		}

		// Token: 0x06001CB5 RID: 7349 RVA: 0x00098860 File Offset: 0x00096C60
		public void ResetNextActionPer(RoomCharaActMode.eMode fmode)
		{
			RoomAcitionWakeUp nextActionParam = RoomAnimePlay.GetNextActionParam(fmode);
			int num = this.m_NextActionNum = nextActionParam.m_Table.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_NextActionPer[i].m_Type = nextActionParam.m_Table[i].m_Type;
				this.m_NextActionPer[i].m_Per = nextActionParam.m_Table[i].m_Percent;
			}
		}

		// Token: 0x04002366 RID: 9062
		public int m_NextActionNum;

		// Token: 0x04002367 RID: 9063
		public RoomCharaActMode.NextActionState[] m_NextActionPer;

		// Token: 0x020005C5 RID: 1477
		public enum eMode
		{
			// Token: 0x04002369 RID: 9065
			None = -1,
			// Token: 0x0400236A RID: 9066
			Idle,
			// Token: 0x0400236B RID: 9067
			Move,
			// Token: 0x0400236C RID: 9068
			Tweet,
			// Token: 0x0400236D RID: 9069
			Enjoy,
			// Token: 0x0400236E RID: 9070
			Rejoice,
			// Token: 0x0400236F RID: 9071
			ObjEvent,
			// Token: 0x04002370 RID: 9072
			Sleep,
			// Token: 0x04002371 RID: 9073
			RoomOut,
			// Token: 0x04002372 RID: 9074
			Max
		}

		// Token: 0x020005C6 RID: 1478
		public struct NextActionState
		{
			// Token: 0x04002373 RID: 9075
			public RoomCharaActMode.eMode m_Type;

			// Token: 0x04002374 RID: 9076
			public float m_Per;
		}
	}
}
