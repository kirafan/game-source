﻿using System;

namespace Star
{
	// Token: 0x020002B4 RID: 692
	public abstract class EvolutionResultBeforeAfterPairPartCharacterWrapper : CharacterEditResultSceneBeforeAfterPairPartBasedOutputCharaParamCharacterDataWrapper<EditMain.EvolutionResultData>
	{
		// Token: 0x06000CF7 RID: 3319 RVA: 0x00049223 File Offset: 0x00047623
		public EvolutionResultBeforeAfterPairPartCharacterWrapper(eCharacterDataType characterDataType, EditMain.EvolutionResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}
	}
}
