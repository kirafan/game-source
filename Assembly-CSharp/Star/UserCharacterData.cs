﻿using System;

namespace Star
{
	// Token: 0x02000B3D RID: 2877
	public class UserCharacterData
	{
		// Token: 0x06003C9C RID: 15516 RVA: 0x00135A2B File Offset: 0x00133E2B
		public UserCharacterData()
		{
			this.MngID = -1L;
			this.IsNew = false;
			this.Param = new CharacterParam();
		}

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x06003C9D RID: 15517 RVA: 0x00135A4D File Offset: 0x00133E4D
		// (set) Token: 0x06003C9E RID: 15518 RVA: 0x00135A55 File Offset: 0x00133E55
		public long MngID { get; set; }

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x06003C9F RID: 15519 RVA: 0x00135A5E File Offset: 0x00133E5E
		// (set) Token: 0x06003CA0 RID: 15520 RVA: 0x00135A66 File Offset: 0x00133E66
		public bool IsNew { get; set; }

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06003CA1 RID: 15521 RVA: 0x00135A6F File Offset: 0x00133E6F
		// (set) Token: 0x06003CA2 RID: 15522 RVA: 0x00135A77 File Offset: 0x00133E77
		public CharacterParam Param { get; set; }
	}
}
