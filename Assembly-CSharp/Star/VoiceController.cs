﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000638 RID: 1592
	public class VoiceController
	{
		// Token: 0x06001F61 RID: 8033 RVA: 0x000AA1CC File Offset: 0x000A85CC
		private int GetNewUniqueID()
		{
			return ++this.m_UniqueIDGenerator;
		}

		// Token: 0x06001F62 RID: 8034 RVA: 0x000AA1EA File Offset: 0x000A85EA
		public void Setup(SoundManager soundManager)
		{
			this.m_SoundManager = soundManager;
		}

		// Token: 0x06001F63 RID: 8035 RVA: 0x000AA1F4 File Offset: 0x000A85F4
		public void Release()
		{
			if (this.m_SoundManager == null)
			{
				return;
			}
			foreach (int key in this.m_SoundHndls.Keys)
			{
				this.m_SoundManager.RemoveHndl(this.m_SoundHndls[key], true);
			}
			this.m_SoundHndls.Clear();
			for (int i = 0; i < this.m_Paths.Count; i++)
			{
				this.m_SoundManager.UnloadVoiceCueSheet(this.m_Paths[i]);
			}
			this.m_Paths.Clear();
			this.m_RequestDict.Clear();
		}

		// Token: 0x06001F64 RID: 8036 RVA: 0x000AA2C8 File Offset: 0x000A86C8
		public void Update()
		{
			if (this.m_SoundManager == null)
			{
				return;
			}
			this.m_RemoveKeys_SoundHndl.Clear();
			foreach (int num in this.m_SoundHndls.Keys)
			{
				if (!this.m_SoundHndls[num].IsPrepare() && !this.m_SoundHndls[num].IsPlaying(false))
				{
					this.m_SoundManager.RemoveHndl(this.m_SoundHndls[num], true);
					this.m_RemoveKeys_SoundHndl.Add(num);
				}
			}
			for (int i = 0; i < this.m_RemoveKeys_SoundHndl.Count; i++)
			{
				this.m_SoundHndls.Remove(this.m_RemoveKeys_SoundHndl[i]);
			}
			while (this.m_Paths.Count > 4)
			{
				string cueSheet = this.m_Paths[0];
				SoundDefine.eLoadStatus cueSheetLoadStatus = this.m_SoundManager.GetCueSheetLoadStatus(cueSheet);
				if (cueSheetLoadStatus == SoundDefine.eLoadStatus.None)
				{
					this.m_Paths.RemoveAt(0);
				}
				else
				{
					if (cueSheetLoadStatus == SoundDefine.eLoadStatus.Loading)
					{
						break;
					}
					if (cueSheetLoadStatus != SoundDefine.eLoadStatus.Loaded)
					{
						break;
					}
					this.m_SoundManager.UnloadVoiceCueSheet(cueSheet);
					this.m_Paths.RemoveAt(0);
				}
			}
			this.m_RemoveKeys.Clear();
			foreach (string text in this.m_RequestDict.Keys)
			{
				if (this.m_SoundManager.GetCueSheetLoadStatus(text) == SoundDefine.eLoadStatus.Loaded)
				{
					SoundHandler soundHandler = new SoundHandler();
					VoiceController.RequestParam requestParam = this.m_RequestDict[text];
					bool flag = false;
					if (requestParam.m_NamedType != eCharaNamedType.None)
					{
						flag = this.m_SoundManager.PlayVOICE(requestParam.m_NamedType, requestParam.m_NamedVoiceCueID, soundHandler, 1f, 0, -1, -1);
					}
					else if (!string.IsNullOrEmpty(requestParam.m_CueSheet))
					{
						flag = this.m_SoundManager.PlayVOICE(requestParam.m_CueSheet, requestParam.m_CueName, soundHandler, 1f, 0, -1, -1);
					}
					this.m_RemoveKeys.Add(text);
					this.m_Paths.Add(text);
					if (flag && !this.m_SoundHndls.ContainsKey(requestParam.m_UniqueID))
					{
						this.m_SoundHndls.Add(requestParam.m_UniqueID, soundHandler);
					}
				}
			}
			for (int j = 0; j < this.m_RemoveKeys.Count; j++)
			{
				this.m_RequestDict.Remove(this.m_RemoveKeys[j]);
			}
		}

		// Token: 0x06001F65 RID: 8037 RVA: 0x000AA5CC File Offset: 0x000A89CC
		public bool IsPlaying(int requestUniqueID)
		{
			if (this.m_SoundManager == null)
			{
				return false;
			}
			if (this.m_SoundHndls.ContainsKey(requestUniqueID))
			{
				if (this.m_SoundHndls[requestUniqueID].IsPrepare())
				{
					return true;
				}
				if (this.m_SoundHndls[requestUniqueID].IsPlaying(false))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001F66 RID: 8038 RVA: 0x000AA62C File Offset: 0x000A8A2C
		public void StopRequest(int requestUniqueID)
		{
			if (this.m_SoundManager == null)
			{
				return;
			}
			if (this.m_SoundHndls.ContainsKey(requestUniqueID))
			{
				this.m_SoundHndls[requestUniqueID].Stop();
				this.m_SoundManager.RemoveHndl(this.m_SoundHndls[requestUniqueID], true);
				this.m_SoundHndls.Remove(requestUniqueID);
			}
		}

		// Token: 0x06001F67 RID: 8039 RVA: 0x000AA690 File Offset: 0x000A8A90
		public int Request(eCharaNamedType namedType, eSoundVoiceListDB cueID, bool isSaveLatestUniqueID = false)
		{
			if (this.m_SoundManager == null)
			{
				return -1;
			}
			this.StopRequestLatestUniqueID(isSaveLatestUniqueID);
			string voiceCueSheet = this.m_SoundManager.GetVoiceCueSheet(namedType);
			VoiceController.RequestParam requestParam = new VoiceController.RequestParam();
			requestParam.m_NamedType = namedType;
			requestParam.m_NamedVoiceCueID = cueID;
			requestParam.m_UniqueID = this.GetNewUniqueID();
			SoundDefine.eLoadStatus cueSheetLoadStatus = this.m_SoundManager.GetCueSheetLoadStatus(voiceCueSheet);
			if (cueSheetLoadStatus != SoundDefine.eLoadStatus.None)
			{
				if (cueSheetLoadStatus != SoundDefine.eLoadStatus.Loading)
				{
					if (cueSheetLoadStatus == SoundDefine.eLoadStatus.Loaded)
					{
						SoundHandler soundHandler = new SoundHandler();
						this.m_SoundManager.PlayVOICE(requestParam.m_NamedType, requestParam.m_NamedVoiceCueID, soundHandler, 1f, 0, -1, -1);
						if (!this.m_SoundHndls.ContainsKey(requestParam.m_UniqueID))
						{
							this.m_SoundHndls.Add(requestParam.m_UniqueID, soundHandler);
						}
					}
				}
				else
				{
					this.m_RequestDict.AddOrReplace(voiceCueSheet, requestParam);
				}
			}
			else
			{
				bool flag = this.m_SoundManager.LoadVoiceCueSheet(namedType);
				if (flag)
				{
					this.m_RequestDict.AddOrReplace(voiceCueSheet, requestParam);
				}
			}
			int uniqueID = requestParam.m_UniqueID;
			this.SaveLatestUniqueID(isSaveLatestUniqueID, uniqueID);
			return uniqueID;
		}

		// Token: 0x06001F68 RID: 8040 RVA: 0x000AA7A8 File Offset: 0x000A8BA8
		public int Request(eCharaNamedType namedType, string cueName, bool isSaveLatestUniqueID = false)
		{
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundVoiceListDB.m_Params.Length; i++)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundVoiceListDB.m_Params[i].m_CueName == cueName)
				{
					return this.Request(namedType, (eSoundVoiceListDB)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundVoiceListDB.m_Params[i].m_ID, isSaveLatestUniqueID);
				}
			}
			return -1;
		}

		// Token: 0x06001F69 RID: 8041 RVA: 0x000AA830 File Offset: 0x000A8C30
		public int Request(eSoundCueSheetDB cueSheetID, string cueName, bool isSaveLatestUniqueID = false)
		{
			if (this.m_SoundManager == null)
			{
				return -1;
			}
			SoundCueSheetDB soundCueSheetDB = this.m_SoundManager.GetSoundCueSheetDB();
			if (soundCueSheetDB == null)
			{
				return -1;
			}
			return this.Request(soundCueSheetDB.GetParam(cueSheetID).m_CueSheet, cueName, null, null, isSaveLatestUniqueID);
		}

		// Token: 0x06001F6A RID: 8042 RVA: 0x000AA880 File Offset: 0x000A8C80
		public int Request(string cueSheet, string cueName, string acbFile = null, string awbFile = null, bool isSaveLatestUniqueID = false)
		{
			if (this.m_SoundManager == null)
			{
				return -1;
			}
			if (string.IsNullOrEmpty(cueSheet))
			{
				return -1;
			}
			if (string.IsNullOrEmpty(cueName))
			{
				return -1;
			}
			this.StopRequestLatestUniqueID(isSaveLatestUniqueID);
			VoiceController.RequestParam requestParam = new VoiceController.RequestParam();
			requestParam.m_CueSheet = cueSheet;
			requestParam.m_CueName = cueName;
			requestParam.m_UniqueID = this.GetNewUniqueID();
			SoundDefine.eLoadStatus cueSheetLoadStatus = this.m_SoundManager.GetCueSheetLoadStatus(cueSheet);
			Debug.Log("loadStatus:" + cueSheetLoadStatus);
			if (cueSheetLoadStatus != SoundDefine.eLoadStatus.None)
			{
				if (cueSheetLoadStatus != SoundDefine.eLoadStatus.Loading)
				{
					if (cueSheetLoadStatus == SoundDefine.eLoadStatus.Loaded)
					{
						SoundHandler soundHandler = new SoundHandler();
						this.m_SoundManager.PlayVOICE(requestParam.m_CueSheet, requestParam.m_CueName, soundHandler, 1f, 0, -1, -1);
						if (!this.m_SoundHndls.ContainsKey(requestParam.m_UniqueID))
						{
							this.m_SoundHndls.Add(requestParam.m_UniqueID, soundHandler);
						}
					}
				}
				else
				{
					this.m_RequestDict.AddOrReplace(cueSheet, requestParam);
				}
			}
			else
			{
				bool flag = this.m_SoundManager.LoadCueSheet(cueSheet, acbFile, awbFile);
				if (!flag)
				{
					return -1;
				}
				this.m_RequestDict.AddOrReplace(cueSheet, requestParam);
			}
			int uniqueID = requestParam.m_UniqueID;
			this.SaveLatestUniqueID(isSaveLatestUniqueID, uniqueID);
			return uniqueID;
		}

		// Token: 0x06001F6B RID: 8043 RVA: 0x000AA9C0 File Offset: 0x000A8DC0
		public int RequestFlavorText(int charaID, bool isSaveLatestUniqueID = false)
		{
			if (this.m_SoundManager == null)
			{
				return -1;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			string uniqueSkillVoiceCueSheet = dbMng.SkillListDB_PL.GetParam(param.m_CharaSkillID).m_UniqueSkillVoiceCueSheet;
			string acbFile = (!string.IsNullOrEmpty(uniqueSkillVoiceCueSheet)) ? (uniqueSkillVoiceCueSheet + ".acb") : null;
			string awbFile = (!string.IsNullOrEmpty(uniqueSkillVoiceCueSheet)) ? (uniqueSkillVoiceCueSheet + ".awb") : null;
			return this.Request(uniqueSkillVoiceCueSheet, "voice_flavor", acbFile, awbFile, isSaveLatestUniqueID);
		}

		// Token: 0x06001F6C RID: 8044 RVA: 0x000AAA5C File Offset: 0x000A8E5C
		public int Request(eSoundVoiceControllListDB id, bool isSaveLatestUniqueID = false)
		{
			int num;
			int num2;
			return this.Request(id, out num, out num2, -1, -1, isSaveLatestUniqueID);
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x000AAA78 File Offset: 0x000A8E78
		public int Request(eSoundVoiceControllListDB id, out int out_requestCueSheetIndex, out int out_requestNamedIndex, int forceRequestCueSheetIndex = -1, int forceRequestNamedIndex = -1, bool isSaveLatestUniqueID = false)
		{
			this.StopRequestLatestUniqueID(isSaveLatestUniqueID);
			out_requestCueSheetIndex = -1;
			out_requestNamedIndex = -1;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			SoundVoiceControllListDB_Param param = dbMng.SoundVoiceControllListDB.GetParam(id);
			int hour = this.GetHour();
			int num = 0;
			for (int i = 0; i < param.m_Datas.Length; i++)
			{
				if (hour >= param.m_Datas[i].m_HourMin && hour < param.m_Datas[i].m_HourMax)
				{
					num = i;
					break;
				}
			}
			if (num >= param.m_Datas.Length)
			{
				return -1;
			}
			int num2 = -1;
			if (param.m_Datas[num].m_CueSheetNames.Length > 0 || forceRequestCueSheetIndex != -1)
			{
				if (forceRequestCueSheetIndex != -1)
				{
					num2 = forceRequestCueSheetIndex;
				}
				else
				{
					num2 = UnityEngine.Random.Range(0, param.m_Datas[num].m_CueSheetNames.Length);
				}
				out_requestCueSheetIndex = num2;
				out_requestNamedIndex = -1;
			}
			string cueSheet = null;
			string cueName = null;
			if (num2 >= 0)
			{
				cueSheet = param.m_Datas[num].m_CueSheetNames[num2];
				cueName = param.m_Datas[num].m_CueNames[num2];
			}
			if ((!string.IsNullOrEmpty(param.m_Datas[num].m_NamedCueName) || forceRequestNamedIndex != -1) && ((UnityEngine.Random.Range(0, 3) != 0 && forceRequestCueSheetIndex == -1) || forceRequestNamedIndex != -1))
			{
				int num3;
				if (forceRequestNamedIndex != -1)
				{
					num3 = forceRequestNamedIndex;
				}
				else
				{
					num3 = UnityEngine.Random.Range(0, dbMng.NamedListDB.m_Params.Length);
				}
				eCharaNamedType namedType = (eCharaNamedType)dbMng.NamedListDB.m_Params[num3].m_NamedType;
				cueSheet = this.m_SoundManager.GetVoiceCueSheet(namedType);
				cueName = param.m_Datas[num].m_NamedCueName;
				out_requestCueSheetIndex = -1;
				out_requestNamedIndex = num3;
				this.Request(namedType, cueName, isSaveLatestUniqueID);
			}
			return this.Request(cueSheet, cueName, null, null, isSaveLatestUniqueID);
		}

		// Token: 0x06001F6E RID: 8046 RVA: 0x000AAC79 File Offset: 0x000A9079
		private void StopRequestLatestUniqueID(bool isSaveLatestUniqueID)
		{
			if (isSaveLatestUniqueID && this.m_LatestUniqueID != -1)
			{
				this.StopRequest(this.m_LatestUniqueID);
				this.m_LatestUniqueID = -1;
			}
		}

		// Token: 0x06001F6F RID: 8047 RVA: 0x000AACA0 File Offset: 0x000A90A0
		private void SaveLatestUniqueID(bool isSaveLatestUniqueID, int uniqueID)
		{
			if (isSaveLatestUniqueID)
			{
				this.m_LatestUniqueID = uniqueID;
			}
		}

		// Token: 0x06001F70 RID: 8048 RVA: 0x000AACB0 File Offset: 0x000A90B0
		private int GetHour()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Hour;
		}

		// Token: 0x04002603 RID: 9731
		private const int CONTROLL_ACQUIRE_NUM = 4;

		// Token: 0x04002604 RID: 9732
		private const int REQUEST_CAPACITY = 32;

		// Token: 0x04002605 RID: 9733
		private SoundManager m_SoundManager;

		// Token: 0x04002606 RID: 9734
		private Dictionary<string, VoiceController.RequestParam> m_RequestDict = new Dictionary<string, VoiceController.RequestParam>(32);

		// Token: 0x04002607 RID: 9735
		private Dictionary<int, SoundHandler> m_SoundHndls = new Dictionary<int, SoundHandler>(32);

		// Token: 0x04002608 RID: 9736
		private List<string> m_Paths = new List<string>();

		// Token: 0x04002609 RID: 9737
		private List<string> m_RemoveKeys = new List<string>(32);

		// Token: 0x0400260A RID: 9738
		private List<int> m_RemoveKeys_SoundHndl = new List<int>(32);

		// Token: 0x0400260B RID: 9739
		private int m_LatestUniqueID = -1;

		// Token: 0x0400260C RID: 9740
		private int m_UniqueIDGenerator;

		// Token: 0x02000639 RID: 1593
		private class RequestParam
		{
			// Token: 0x0400260D RID: 9741
			public int m_UniqueID;

			// Token: 0x0400260E RID: 9742
			public eCharaNamedType m_NamedType = eCharaNamedType.None;

			// Token: 0x0400260F RID: 9743
			public eSoundVoiceListDB m_NamedVoiceCueID;

			// Token: 0x04002610 RID: 9744
			public string m_CueSheet;

			// Token: 0x04002611 RID: 9745
			public string m_CueName;
		}
	}
}
