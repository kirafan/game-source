﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000203 RID: 515
	public class SkillContentListDB : ScriptableObject
	{
		// Token: 0x04000CAD RID: 3245
		public SkillContentListDB_Param[] m_Params;
	}
}
