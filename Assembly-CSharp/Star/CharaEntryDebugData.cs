﻿using System;

namespace Star
{
	// Token: 0x02000298 RID: 664
	[Serializable]
	public class CharaEntryDebugData
	{
		// Token: 0x0400151A RID: 5402
		public int m_ManageID;

		// Token: 0x0400151B RID: 5403
		public int m_ScheduleID;

		// Token: 0x0400151C RID: 5404
		public int m_RoomNO;
	}
}
