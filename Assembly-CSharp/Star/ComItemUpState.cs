﻿using System;
using System.Collections.Generic;
using WWWTypes;

namespace Star
{
	// Token: 0x0200038B RID: 907
	public class ComItemUpState
	{
		// Token: 0x06001127 RID: 4391 RVA: 0x0005A0F4 File Offset: 0x000584F4
		public ComItemUpState()
		{
			this.m_List = new List<ComItemUpState.UpCategory>();
		}

		// Token: 0x06001128 RID: 4392 RVA: 0x0005A107 File Offset: 0x00058507
		public int GetNewListNum()
		{
			return this.m_List.Count;
		}

		// Token: 0x06001129 RID: 4393 RVA: 0x0005A114 File Offset: 0x00058514
		public ComItemUpState.UpCategory GetNewListAt(int findex)
		{
			return this.m_List[findex];
		}

		// Token: 0x0600112A RID: 4394 RVA: 0x0005A124 File Offset: 0x00058524
		private ComItemUpState.UpCategory SearchCategory(ComItemUpState.eUpCategory fcategory, int fitemno)
		{
			ComItemUpState.UpCategory result = null;
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].m_Category == fcategory && this.m_List[i].m_No == fitemno)
				{
					result = this.m_List[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x0600112B RID: 4395 RVA: 0x0005A190 File Offset: 0x00058590
		public void AddCategory(ComItemUpState.eUpCategory fcategory, int fitemno, long fnum)
		{
			ComItemUpState.UpCategory upCategory = this.SearchCategory(fcategory, fitemno);
			if (upCategory == null)
			{
				upCategory = new ComItemUpState.UpCategory();
			}
			upCategory.m_Category = fcategory;
			upCategory.m_No = fitemno;
			upCategory.m_Num += fnum;
			this.m_List.Add(upCategory);
		}

		// Token: 0x0600112C RID: 4396 RVA: 0x0005A1DC File Offset: 0x000585DC
		public void CalcPlayerDataNewParam(Player pcompdata)
		{
			UserData userData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData;
			if (userData.Gold != pcompdata.gold)
			{
				this.AddCategory(ComItemUpState.eUpCategory.Money, 0, pcompdata.gold - userData.Gold);
			}
			if (userData.KRRPoint.GetPoint() != pcompdata.kirara)
			{
				this.AddCategory(ComItemUpState.eUpCategory.Point, 0, (long)((int)pcompdata.kirara) - userData.KRRPoint.GetPoint());
			}
			if (userData.Stamina.GetValue() != pcompdata.stamina)
			{
				this.AddCategory(ComItemUpState.eUpCategory.Stamina, 0, (long)((int)pcompdata.stamina) - userData.Stamina.GetValue());
			}
		}

		// Token: 0x0600112D RID: 4397 RVA: 0x0005A284 File Offset: 0x00058684
		public void CalcPlayerItemNewParam(PlayerItemSummary[] pcompdata)
		{
			List<UserItemData> userItemDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserItemDatas;
			if (userItemDatas != null)
			{
				int count = userItemDatas.Count;
				int num = pcompdata.Length;
				for (int i = 0; i < num; i++)
				{
					bool flag = false;
					for (int j = 0; j < count; j++)
					{
						if (userItemDatas[j].ItemID == pcompdata[i].id)
						{
							if (pcompdata[i].amount != userItemDatas[j].ItemNum)
							{
								this.AddCategory(ComItemUpState.eUpCategory.Item, pcompdata[i].id, (long)(pcompdata[i].amount - userItemDatas[j].ItemNum));
								APIUtility.wwwToUserData(pcompdata[i], userItemDatas[j]);
							}
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						UserItemData userItemData = new UserItemData();
						APIUtility.wwwToUserData(pcompdata[i], userItemData);
						SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserItemDatas.Add(userItemData);
						this.AddCategory(ComItemUpState.eUpCategory.Item, pcompdata[i].id, (long)pcompdata[i].amount);
					}
				}
			}
		}

		// Token: 0x0600112E RID: 4398 RVA: 0x0005A394 File Offset: 0x00058794
		public void CalcNamedFriendShipNewParam(PlayerNamedType[] pcompdata)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			int num = pcompdata.Length;
			for (int i = 0; i < num; i++)
			{
				UserNamedData userNamedData = userDataMng.GetUserNamedData((eCharaNamedType)pcompdata[i].namedType);
				if (userNamedData != null && (userNamedData.FriendShip != pcompdata[i].level || userNamedData.FriendShipExp != pcompdata[i].exp))
				{
					this.AddCategory(ComItemUpState.eUpCategory.FriendShip, pcompdata[i].namedType, pcompdata[i].exp - userNamedData.FriendShipExp);
					if (userNamedData.FriendShip < pcompdata[i].level)
					{
						long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
						for (int j = 0; j < roomInCharaManageID.Length; j++)
						{
							if (roomInCharaManageID[j] != -1L && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(roomInCharaManageID[j]).Param.NamedType == (eCharaNamedType)pcompdata[i].namedType)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.AddPopParam(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(roomInCharaManageID[j]).Param.CharaID, userNamedData.FriendShip, pcompdata[i].level);
								break;
							}
						}
					}
					APIUtility.wwwToUserData(pcompdata[i], userNamedData);
				}
			}
		}

		// Token: 0x04001807 RID: 6151
		public List<ComItemUpState.UpCategory> m_List;

		// Token: 0x0200038C RID: 908
		public enum eUpCategory
		{
			// Token: 0x04001809 RID: 6153
			Item,
			// Token: 0x0400180A RID: 6154
			Money,
			// Token: 0x0400180B RID: 6155
			Point,
			// Token: 0x0400180C RID: 6156
			Stamina,
			// Token: 0x0400180D RID: 6157
			FriendShip
		}

		// Token: 0x0200038D RID: 909
		public class UpCategory
		{
			// Token: 0x0400180E RID: 6158
			public ComItemUpState.eUpCategory m_Category;

			// Token: 0x0400180F RID: 6159
			public long m_Num;

			// Token: 0x04001810 RID: 6160
			public int m_No;
		}
	}
}
