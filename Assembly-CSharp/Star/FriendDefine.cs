﻿using System;

namespace Star
{
	// Token: 0x020003BB RID: 955
	public static class FriendDefine
	{
		// Token: 0x020003BC RID: 956
		public enum eType
		{
			// Token: 0x0400189D RID: 6301
			Friend,
			// Token: 0x0400189E RID: 6302
			OpponentPropose,
			// Token: 0x0400189F RID: 6303
			SelfPropose,
			// Token: 0x040018A0 RID: 6304
			Guest
		}

		// Token: 0x020003BD RID: 957
		public enum eGetAllType
		{
			// Token: 0x040018A2 RID: 6306
			Friend = 1,
			// Token: 0x040018A3 RID: 6307
			Guest,
			// Token: 0x040018A4 RID: 6308
			Both
		}
	}
}
