﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006C9 RID: 1737
	public class TownPartsModelMaker : ITownPartsAction
	{
		// Token: 0x06002273 RID: 8819 RVA: 0x000B7A9C File Offset: 0x000B5E9C
		public TownPartsModelMaker(TownBuilder pbuilder, Transform parent, GameObject phitnode, int flayer)
		{
			this.m_Builder = pbuilder;
			this.m_Active = true;
			this.m_CalcStep = TownPartsModelMaker.eCalcStep.Init;
			this.m_Maker = this.m_Builder.GetMakerGauge();
			this.m_Maker.transform.position = parent.position;
			this.m_LayerID = flayer;
			this.m_MsbHndl = phitnode.GetComponentsInChildren<MsbHandler>();
		}

		// Token: 0x06002274 RID: 8820 RVA: 0x000B7AFF File Offset: 0x000B5EFF
		public void SetEndMark()
		{
			this.m_Active = false;
		}

		// Token: 0x06002275 RID: 8821 RVA: 0x000B7B08 File Offset: 0x000B5F08
		public override bool PartsUpdate()
		{
			TownPartsModelMaker.eCalcStep calcStep = this.m_CalcStep;
			if (calcStep != TownPartsModelMaker.eCalcStep.Init)
			{
				if (calcStep != TownPartsModelMaker.eCalcStep.Calc)
				{
					if (calcStep != TownPartsModelMaker.eCalcStep.End)
					{
					}
				}
				else
				{
					this.m_BlinkTime += Time.deltaTime;
					if (this.m_BlinkTime >= 1.5f)
					{
						this.m_BlinkTime -= 1.5f;
					}
					if (!this.m_Active)
					{
						TownMakerPoint component = this.m_Maker.GetComponent<TownMakerPoint>();
						if (component != null)
						{
							component.SetEnable(true);
						}
						this.m_Maker.SetActive(false);
						this.m_Builder.ReleaseMakerGauge(this.m_Maker);
						this.m_BlinkTime = 0f;
						this.SetBlinkModel();
						this.m_MsbHndl = null;
						this.m_CalcStep = TownPartsModelMaker.eCalcStep.End;
					}
					else
					{
						this.SetBlinkModel();
					}
				}
			}
			else
			{
				this.m_CalcStep = TownPartsModelMaker.eCalcStep.Calc;
				this.m_Maker.SetActive(true);
				TownMakerPoint component2 = this.m_Maker.GetComponent<TownMakerPoint>();
				if (component2 != null)
				{
					component2.SetEnable(true);
				}
				Renderer[] componentsInChildren = this.m_Maker.GetComponentsInChildren<Renderer>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].sortingOrder = this.m_LayerID;
				}
			}
			return this.m_Active;
		}

		// Token: 0x06002276 RID: 8822 RVA: 0x000B7C54 File Offset: 0x000B6054
		private void SetBlinkModel()
		{
			float num = this.m_BlinkTime / 1.5f;
			Color white = Color.white;
			white.r = (white.g = (white.b = 1f - (1f - (Mathf.Cos(num * 3.1415927f * 2f) * 0.5f + 0.5f)) * 0.4f));
			white.a = 1f;
			int num2 = this.m_MsbHndl.Length;
			for (int i = 0; i < num2; i++)
			{
				int msbObjectHandlerNum = this.m_MsbHndl[i].GetMsbObjectHandlerNum();
				for (int j = 0; j < msbObjectHandlerNum; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl[i].GetMsbObjectHandler(j);
					msbObjectHandler.GetWork().m_MeshColor = white;
				}
			}
		}

		// Token: 0x04002935 RID: 10549
		private TownBuilder m_Builder;

		// Token: 0x04002936 RID: 10550
		private GameObject m_Maker;

		// Token: 0x04002937 RID: 10551
		private TownPartsModelMaker.eCalcStep m_CalcStep;

		// Token: 0x04002938 RID: 10552
		private int m_LayerID;

		// Token: 0x04002939 RID: 10553
		private MsbHandler[] m_MsbHndl;

		// Token: 0x0400293A RID: 10554
		private float m_BlinkTime;

		// Token: 0x0400293B RID: 10555
		private const float BlinkTime = 1.5f;

		// Token: 0x020006CA RID: 1738
		public enum eCalcStep
		{
			// Token: 0x0400293D RID: 10557
			Init,
			// Token: 0x0400293E RID: 10558
			Calc,
			// Token: 0x0400293F RID: 10559
			End
		}
	}
}
