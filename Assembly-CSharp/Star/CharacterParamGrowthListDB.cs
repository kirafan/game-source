﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200017B RID: 379
	public class CharacterParamGrowthListDB : ScriptableObject
	{
		// Token: 0x04000A0C RID: 2572
		public CharacterParamGrowthListDB_Param[] m_Params;
	}
}
