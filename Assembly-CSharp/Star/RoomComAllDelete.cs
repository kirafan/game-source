﻿using System;
using RoomObjectResponseTypes;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x02000598 RID: 1432
	public class RoomComAllDelete : IFldNetComModule
	{
		// Token: 0x06001BBC RID: 7100 RVA: 0x00092DBE File Offset: 0x000911BE
		public RoomComAllDelete() : base(IFldNetComManager.Instance)
		{
		}

		// Token: 0x06001BBD RID: 7101 RVA: 0x00092DCB File Offset: 0x000911CB
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06001BBE RID: 7102 RVA: 0x00092DDC File Offset: 0x000911DC
		public bool SetUp()
		{
			RoomComAPIGetAll roomComAPIGetAll = new RoomComAPIGetAll();
			roomComAPIGetAll.playerId = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
			this.m_NetMng.SendCom(roomComAPIGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06001BBF RID: 7103 RVA: 0x00092E24 File Offset: 0x00091224
		private void CallbackSetUp(INetComHandle phandle)
		{
			RoomResponseTypes.GetAll getAll = phandle.GetResponse() as RoomResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_Table = new long[getAll.managedRooms.Length];
				for (int i = 0; i < getAll.managedRooms.Length; i++)
				{
					this.m_Table[i] = getAll.managedRooms[i].managedRoomId;
				}
				this.m_Step = RoomComAllDelete.eStep.DeleteUp;
			}
		}

		// Token: 0x06001BC0 RID: 7104 RVA: 0x00092E8C File Offset: 0x0009128C
		public bool SetUpObjList()
		{
			RoomComAPIObjGetAll phandle = new RoomComAPIObjGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackObjSetUp), false);
			return true;
		}

		// Token: 0x06001BC1 RID: 7105 RVA: 0x00092EBC File Offset: 0x000912BC
		private void CallbackObjSetUp(INetComHandle phandle)
		{
			RoomObjectResponseTypes.GetAll getAll = phandle.GetResponse() as RoomObjectResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_Table = new long[getAll.managedRoomObjects.Length];
				for (int i = 0; i < getAll.managedRoomObjects.Length; i++)
				{
					this.m_Table[i] = getAll.managedRoomObjects[i].managedRoomObjectId;
				}
				this.m_Step = RoomComAllDelete.eStep.DeleteObjUp;
			}
		}

		// Token: 0x06001BC2 RID: 7106 RVA: 0x00092F24 File Offset: 0x00091324
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case RoomComAllDelete.eStep.GetState:
				this.SetUp();
				this.m_Step = RoomComAllDelete.eStep.WaitCheck;
				break;
			case RoomComAllDelete.eStep.DeleteUp:
				this.m_Step = RoomComAllDelete.eStep.SetUpCheck;
				if (!this.DefaultDeleteManage())
				{
					this.m_Step = RoomComAllDelete.eStep.GetObjState;
				}
				break;
			case RoomComAllDelete.eStep.GetObjState:
				this.SetUpObjList();
				this.m_Step = RoomComAllDelete.eStep.WaitCheck;
				break;
			case RoomComAllDelete.eStep.DeleteObjUp:
				this.m_Step = RoomComAllDelete.eStep.SetUpCheck;
				if (!this.DefaultDeleteObjManage())
				{
					this.m_Step = RoomComAllDelete.eStep.End;
				}
				break;
			case RoomComAllDelete.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x06001BC3 RID: 7107 RVA: 0x00092FD8 File Offset: 0x000913D8
		public bool DefaultDeleteManage()
		{
			if (this.m_DelStep < this.m_Table.Length)
			{
				RoomComAPIRemove roomComAPIRemove = new RoomComAPIRemove();
				roomComAPIRemove.managedRoomId = this.m_Table[this.m_DelStep];
				this.m_DelStep++;
				this.m_NetMng.SendCom(roomComAPIRemove, new INetComHandle.ResponseCallbak(this.CallbackDeleteUp), false);
				return true;
			}
			return false;
		}

		// Token: 0x06001BC4 RID: 7108 RVA: 0x0009303B File Offset: 0x0009143B
		private void CallbackDeleteUp(INetComHandle phandle)
		{
			this.m_Step = RoomComAllDelete.eStep.DeleteUp;
		}

		// Token: 0x06001BC5 RID: 7109 RVA: 0x00093044 File Offset: 0x00091444
		public bool DefaultDeleteObjManage()
		{
			if (this.m_DelStep < this.m_Table.Length)
			{
				RoomComAPIObjRemove roomComAPIObjRemove = new RoomComAPIObjRemove();
				roomComAPIObjRemove.managedRoomObjectId = this.m_Table[this.m_DelStep];
				this.m_DelStep++;
				this.m_NetMng.SendCom(roomComAPIObjRemove, new INetComHandle.ResponseCallbak(this.CallbackDeleteObjUp), false);
				return true;
			}
			return false;
		}

		// Token: 0x06001BC6 RID: 7110 RVA: 0x000930A7 File Offset: 0x000914A7
		private void CallbackDeleteObjUp(INetComHandle phandle)
		{
			this.m_Step = RoomComAllDelete.eStep.DeleteObjUp;
		}

		// Token: 0x040022AA RID: 8874
		private RoomComAllDelete.eStep m_Step;

		// Token: 0x040022AB RID: 8875
		private long[] m_Table;

		// Token: 0x040022AC RID: 8876
		private int m_DelStep;

		// Token: 0x02000599 RID: 1433
		public enum eStep
		{
			// Token: 0x040022AE RID: 8878
			GetState,
			// Token: 0x040022AF RID: 8879
			WaitCheck,
			// Token: 0x040022B0 RID: 8880
			DeleteUp,
			// Token: 0x040022B1 RID: 8881
			SetUpCheck,
			// Token: 0x040022B2 RID: 8882
			GetObjState,
			// Token: 0x040022B3 RID: 8883
			DeleteObjUp,
			// Token: 0x040022B4 RID: 8884
			End
		}
	}
}
