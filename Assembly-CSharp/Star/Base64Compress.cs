﻿using System;
using System.Text;

namespace Star
{
	// Token: 0x020004DD RID: 1245
	public static class Base64Compress
	{
		// Token: 0x060018BA RID: 6330 RVA: 0x00080E1C File Offset: 0x0007F21C
		public static string Base64CompressFromString(string str)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(str);
			if (bytes == null)
			{
				return null;
			}
			byte[] array = ZlibUtil.compress(bytes, 6);
			if (array == null)
			{
				return null;
			}
			return Convert.ToBase64String(array);
		}

		// Token: 0x060018BB RID: 6331 RVA: 0x00080E58 File Offset: 0x0007F258
		public static string StringFromBase64Compress(string str)
		{
			byte[] array = Convert.FromBase64String(str);
			if (array == null)
			{
				return null;
			}
			byte[] array2 = ZlibUtil.uncompress(array);
			if (array2 == null)
			{
				return null;
			}
			return Encoding.UTF8.GetString(array2);
		}
	}
}
