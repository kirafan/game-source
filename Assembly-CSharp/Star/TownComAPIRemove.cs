﻿using System;
using TownResponseTypes;

namespace Star
{
	// Token: 0x02000689 RID: 1673
	public class TownComAPIRemove : INetComHandle
	{
		// Token: 0x060021BA RID: 8634 RVA: 0x000B39C0 File Offset: 0x000B1DC0
		public TownComAPIRemove()
		{
			this.ApiName = "player/town/remove";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x04002824 RID: 10276
		public long managedTownId;
	}
}
