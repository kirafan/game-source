﻿using System;

namespace Star
{
	// Token: 0x02000268 RID: 616
	public class WebDataListUtil
	{
		// Token: 0x06000B80 RID: 2944 RVA: 0x0004335C File Offset: 0x0004175C
		public static string GetIDToAdress(int accessId)
		{
			string url = null;
			WebDataListDB webDataListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WebDataListDB;
			if (webDataListDB != null)
			{
				int num = webDataListDB.m_Params.Length;
				for (int i = 0; i < num; i++)
				{
					if (webDataListDB.m_Params[i].m_ID == accessId)
					{
						url = webDataListDB.m_Params[i].m_WebAdress;
						break;
					}
				}
			}
			return Utility.ConvertKrrURL(url);
		}
	}
}
