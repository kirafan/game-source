﻿using System;

namespace Star
{
	// Token: 0x0200016A RID: 362
	[Serializable]
	public struct CharacterEvolutionListDB_Param
	{
		// Token: 0x040009B9 RID: 2489
		public int m_RecipeID;

		// Token: 0x040009BA RID: 2490
		public int m_RecipeType;

		// Token: 0x040009BB RID: 2491
		public int m_SrcCharaID;

		// Token: 0x040009BC RID: 2492
		public int m_DestCharaID;

		// Token: 0x040009BD RID: 2493
		public int m_Amount;

		// Token: 0x040009BE RID: 2494
		public int[] m_ItemIDs;

		// Token: 0x040009BF RID: 2495
		public int[] m_ItemNums;
	}
}
