﻿using System;
using Star.UI;

namespace Star
{
	// Token: 0x020004BF RID: 1215
	public class TownState_Init : TownState
	{
		// Token: 0x060017E3 RID: 6115 RVA: 0x0007C8DC File Offset: 0x0007ACDC
		public TownState_Init(TownMain owner) : base(owner)
		{
		}

		// Token: 0x060017E4 RID: 6116 RVA: 0x0007C8EC File Offset: 0x0007ACEC
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060017E5 RID: 6117 RVA: 0x0007C8EF File Offset: 0x0007ACEF
		public override void OnStateEnter()
		{
			this.m_Step = TownState_Init.eStep.First;
		}

		// Token: 0x060017E6 RID: 6118 RVA: 0x0007C8F8 File Offset: 0x0007ACF8
		public override void OnStateExit()
		{
		}

		// Token: 0x060017E7 RID: 6119 RVA: 0x0007C8FA File Offset: 0x0007ACFA
		public override void OnDispose()
		{
		}

		// Token: 0x060017E8 RID: 6120 RVA: 0x0007C8FC File Offset: 0x0007ACFC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case TownState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.OpenQuickIfNotDisplay(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Step = TownState_Init.eStep.HomeUIPrepare_Start;
				}
				break;
			case TownState_Init.eStep.TownUIPrepare_Start:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.TownUI, true);
				this.m_Step = TownState_Init.eStep.TownUIPrepare_Processing;
				break;
			case TownState_Init.eStep.TownUIPrepare_Processing:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.TownUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.TownUI);
					this.m_Step = TownState_Init.eStep.BuilderPrepare_Start;
				}
				break;
			case TownState_Init.eStep.HomeUIPrepare_Start:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.HomeUI, true);
				this.m_Step = TownState_Init.eStep.HomeUIPrepare_Processing;
				break;
			case TownState_Init.eStep.HomeUIPrepare_Processing:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.HomeUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.HomeUI);
					this.m_Step = TownState_Init.eStep.TownUIPrepare_Start;
				}
				break;
			case TownState_Init.eStep.BuilderPrepare_Start:
				this.m_Owner.InitializeBuilder();
				this.m_Owner.Builder.Prepare();
				this.m_Step = TownState_Init.eStep.BuilderPrepare_Processing;
				break;
			case TownState_Init.eStep.BuilderPrepare_Processing:
				if (!this.m_Owner.Builder.IsPreparing())
				{
					this.m_Step = TownState_Init.eStep.TownUIStart;
				}
				break;
			case TownState_Init.eStep.TownUIStart:
			{
				bool flag = this.m_Owner.InitializeUI();
				if (flag)
				{
					this.m_Step = TownState_Init.eStep.End;
				}
				break;
			}
			case TownState_Init.eStep.End:
				this.m_Step = TownState_Init.eStep.None;
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart)
				{
					return 2;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart = true;
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
				return 1;
			}
			return -1;
		}

		// Token: 0x060017E9 RID: 6121 RVA: 0x0007CAFF File Offset: 0x0007AEFF
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001EAF RID: 7855
		private TownState_Init.eStep m_Step = TownState_Init.eStep.None;

		// Token: 0x020004C0 RID: 1216
		private enum eStep
		{
			// Token: 0x04001EB1 RID: 7857
			None = -1,
			// Token: 0x04001EB2 RID: 7858
			First,
			// Token: 0x04001EB3 RID: 7859
			TownUIPrepare_Start,
			// Token: 0x04001EB4 RID: 7860
			TownUIPrepare_Processing,
			// Token: 0x04001EB5 RID: 7861
			HomeUIPrepare_Start,
			// Token: 0x04001EB6 RID: 7862
			HomeUIPrepare_Processing,
			// Token: 0x04001EB7 RID: 7863
			BuilderPrepare_Start,
			// Token: 0x04001EB8 RID: 7864
			BuilderPrepare_Processing,
			// Token: 0x04001EB9 RID: 7865
			TownUIStart,
			// Token: 0x04001EBA RID: 7866
			End
		}
	}
}
