﻿using System;

namespace Star
{
	// Token: 0x02000183 RID: 387
	[Serializable]
	public struct EnemyResourceListDB_Param
	{
		// Token: 0x04000AC2 RID: 2754
		public int m_ResourceID;

		// Token: 0x04000AC3 RID: 2755
		public int m_AnimID;

		// Token: 0x04000AC4 RID: 2756
		public byte m_IsBoss;

		// Token: 0x04000AC5 RID: 2757
		public string m_VoiceCueSheetName;

		// Token: 0x04000AC6 RID: 2758
		public float m_ShadowScale;

		// Token: 0x04000AC7 RID: 2759
		public float m_ShadowOffsetX;
	}
}
