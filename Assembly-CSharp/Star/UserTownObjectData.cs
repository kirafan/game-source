﻿using System;
using WWWTypes;

namespace Star
{
	// Token: 0x02000B56 RID: 2902
	public class UserTownObjectData
	{
		// Token: 0x06003D9D RID: 15773 RVA: 0x001371C6 File Offset: 0x001355C6
		public UserTownObjectData()
		{
			this.ObjID = -1;
			this.m_ManageID = -1L;
			this.Lv = 0;
			this.BuildTime = 0L;
			this.OpenState = UserTownObjectData.eOpenState.Non;
		}

		// Token: 0x06003D9E RID: 15774 RVA: 0x001371F3 File Offset: 0x001355F3
		public UserTownObjectData(int objID, long fmngid)
		{
			this.ObjID = objID;
			this.m_ManageID = fmngid;
			this.Lv = 0;
			this.BuildTime = 0L;
			this.OpenState = UserTownObjectData.eOpenState.Non;
		}

		// Token: 0x06003D9F RID: 15775 RVA: 0x00137220 File Offset: 0x00135620
		public UserTownObjectData(ref PlayerTownFacility pparam)
		{
			this.ObjID = pparam.facilityId;
			this.m_ManageID = pparam.managedTownFacilityId;
			this.Lv = pparam.level;
			this.BuildTime = pparam.buildTime;
			this.ActionTime = pparam.actionTime;
			this.OpenState = (UserTownObjectData.eOpenState)pparam.openState;
			if (this.BuildTime == 0L && this.ActionTime == 0L)
			{
				this.OpenState = UserTownObjectData.eOpenState.Open;
			}
			if ((this.OpenState == UserTownObjectData.eOpenState.OnWait || this.OpenState == UserTownObjectData.eOpenState.Wait) && this.Lv != 0)
			{
				this.Lv--;
			}
			this.BuildPoint = pparam.buildPointIndex;
		}

		// Token: 0x06003DA0 RID: 15776 RVA: 0x001372E0 File Offset: 0x001356E0
		public static void SetConnect(bool fsetup)
		{
			UserTownObjectData.ms_SetUp = fsetup;
		}

		// Token: 0x06003DA1 RID: 15777 RVA: 0x001372E8 File Offset: 0x001356E8
		public static bool IsSetUpConnect()
		{
			return UserTownObjectData.ms_SetUp;
		}

		// Token: 0x06003DA2 RID: 15778 RVA: 0x001372F0 File Offset: 0x001356F0
		public int AddChkLevel(int addLevel)
		{
			int num = this.Lv + addLevel;
			if (num > (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.ObjID).m_MaxLevel)
			{
				return this.Lv;
			}
			return num;
		}

		// Token: 0x040044E7 RID: 17639
		private static bool ms_SetUp;

		// Token: 0x040044E8 RID: 17640
		public int ObjID;

		// Token: 0x040044E9 RID: 17641
		public int Lv;

		// Token: 0x040044EA RID: 17642
		public int GroupID;

		// Token: 0x040044EB RID: 17643
		public long m_ManageID;

		// Token: 0x040044EC RID: 17644
		public long BuildTime;

		// Token: 0x040044ED RID: 17645
		public long ActionTime;

		// Token: 0x040044EE RID: 17646
		public int BuildPoint;

		// Token: 0x040044EF RID: 17647
		public UserTownObjectData.eOpenState OpenState;

		// Token: 0x040044F0 RID: 17648
		public bool m_ChangeState;

		// Token: 0x02000B57 RID: 2903
		public enum eOpenState
		{
			// Token: 0x040044F2 RID: 17650
			Non,
			// Token: 0x040044F3 RID: 17651
			Open,
			// Token: 0x040044F4 RID: 17652
			Wait,
			// Token: 0x040044F5 RID: 17653
			PreWait,
			// Token: 0x040044F6 RID: 17654
			OnWait
		}
	}
}
