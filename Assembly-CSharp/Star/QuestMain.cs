﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.BackGround;
using Star.UI.Edit;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000457 RID: 1111
	public class QuestMain : GameStateMain
	{
		// Token: 0x1700016E RID: 366
		// (get) Token: 0x06001574 RID: 5492 RVA: 0x0006FBE2 File Offset: 0x0006DFE2
		// (set) Token: 0x06001575 RID: 5493 RVA: 0x0006FBEA File Offset: 0x0006DFEA
		public bool IsQuestStartConfirm { get; set; }

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x06001576 RID: 5494 RVA: 0x0006FBF3 File Offset: 0x0006DFF3
		// (set) Token: 0x06001577 RID: 5495 RVA: 0x0006FBFB File Offset: 0x0006DFFB
		public CharaListUI CharaListUI { get; set; }

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06001578 RID: 5496 RVA: 0x0006FC04 File Offset: 0x0006E004
		// (set) Token: 0x06001579 RID: 5497 RVA: 0x0006FC0C File Offset: 0x0006E00C
		public MasterDisplayUI MasterUI { get; set; }

		// Token: 0x0600157A RID: 5498 RVA: 0x0006FC15 File Offset: 0x0006E015
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x0006FC1E File Offset: 0x0006E01E
		private void OnDestroy()
		{
			this.CharaListUI = null;
			this.MasterUI = null;
		}

		// Token: 0x0600157C RID: 5500 RVA: 0x0006FC2E File Offset: 0x0006E02E
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x0006FC38 File Offset: 0x0006E038
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			switch (nextStateID)
			{
			case 0:
				return new QuestState_Init(this);
			case 1:
				return new QuestState_QuestCategorySelect(this);
			case 2:
				return new QuestState_QuestGroupSelect(this);
			case 3:
				return new QuestState_QuestChapterSelect(this);
			case 4:
				return new QuestState_QuestSelect(this);
			case 5:
				return new QuestState_QuestConfirm(this);
			case 6:
				return new QuestState_CharaListPartyEdit(this);
			case 7:
				return new QuestState_QuestFriendSelect(this);
			default:
				if (nextStateID != 2147483646)
				{
					return null;
				}
				return new CommonState_Final(this);
			}
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x0006FCC4 File Offset: 0x0006E0C4
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
			SceneDefine.eSceneID nextTransitSceneID = base.NextTransitSceneID;
			if (nextTransitSceneID != SceneDefine.eSceneID.Battle)
			{
				if (nextTransitSceneID == SceneDefine.eSceneID.ADV)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
				QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
				QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(selectQuest.m_QuestID);
				if (questData != null)
				{
					int advID_Prev = questData.m_Param.m_AdvID_Prev;
					bool flag = false;
					if (advID_Prev != -1)
					{
						flag = true;
						if (LocalSaveData.Inst.ADVSkipOnBattle && questData.IsCleared() && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advID_Prev))
						{
							flag = false;
						}
					}
					if (flag)
					{
						base.NextTransitSceneID = SceneDefine.eSceneID.ADV;
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(advID_Prev, SceneDefine.eSceneID.Battle, false);
					}
				}
			}
			SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.MasterDisplayUI);
		}

		// Token: 0x0600157F RID: 5503 RVA: 0x0006FDCD File Offset: 0x0006E1CD
		public override bool IsCompleteDestroy()
		{
			return SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.MasterDisplayUI);
		}

		// Token: 0x06001580 RID: 5504 RVA: 0x0006FDE4 File Offset: 0x0006E1E4
		public void SavePartyData()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData = new List<UserBattlePartyData>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas.Count; i++)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData.Add((UserBattlePartyData)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[i].DeepCopy());
			}
		}

		// Token: 0x06001581 RID: 5505 RVA: 0x0006FE60 File Offset: 0x0006E260
		public void RevertPartyData()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData != null)
			{
				for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData.Count; i++)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[i] = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData[i];
				}
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData = null;
			}
		}

		// Token: 0x06001582 RID: 5506 RVA: 0x0006FEE0 File Offset: 0x0006E2E0
		public void Request_BattlePartySetAll(Action callback)
		{
			this.m_OnResultPartySetAllCallBack = callback;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			List<UserBattlePartyData> userBattlePartyDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas;
			List<int> list = new List<int>();
			for (int i = 0; i < userBattlePartyDatas.Count; i++)
			{
				int num = EditUtility.CalcPartyTotalCostAt(i);
				if (num > userDataMng.UserData.PartyCost)
				{
					list.Add(i);
				}
			}
			if (list.Count > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int j = 0; j < list.Count; j++)
				{
					if (j != 0)
					{
						stringBuilder.Append("\n");
					}
					stringBuilder.Append(userBattlePartyDatas[j].PartyName);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PartyCostOverTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PartyCostOver), stringBuilder.ToString(), new Action<int>(this.OnConfirmCostOver));
				return;
			}
			PlayerRequestTypes.Battlepartysetall battlepartysetall = new PlayerRequestTypes.Battlepartysetall();
			battlepartysetall.battlePartyMembers = new PlayerBattlePartyMember[userBattlePartyDatas.Count];
			for (int k = 0; k < battlepartysetall.battlePartyMembers.Length; k++)
			{
				UserBattlePartyData userBattlePartyData = userBattlePartyDatas[k];
				battlepartysetall.battlePartyMembers[k] = new PlayerBattlePartyMember();
				battlepartysetall.battlePartyMembers[k].managedBattlePartyId = userBattlePartyData.MngID;
				int slotNum = userBattlePartyData.GetSlotNum();
				battlepartysetall.battlePartyMembers[k].managedCharacterIds = new long[slotNum];
				battlepartysetall.battlePartyMembers[k].managedWeaponIds = new long[slotNum];
				for (int l = 0; l < slotNum; l++)
				{
					battlepartysetall.battlePartyMembers[k].managedCharacterIds[l] = userBattlePartyData.GetMemberAt(l);
					battlepartysetall.battlePartyMembers[k].managedWeaponIds[l] = userBattlePartyData.GetWeaponAt(l);
				}
			}
			MeigewwwParam wwwParam = PlayerRequest.Battlepartysetall(battlepartysetall, new MeigewwwParam.Callback(this.OnResponse_BattlePartySetAll));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06001583 RID: 5507 RVA: 0x00070103 File Offset: 0x0006E503
		private void OnConfirmCostOver(int btn)
		{
			if (btn == 0)
			{
				this.RevertPartyData();
				this.m_OnResultPartySetAllCallBack.Call();
			}
		}

		// Token: 0x06001584 RID: 5508 RVA: 0x0007011C File Offset: 0x0006E51C
		private void OnResponse_BattlePartySetAll(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Battlepartysetall battlepartysetall = PlayerResponse.Battlepartysetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetall == null)
			{
				return;
			}
			ResultCode result = battlepartysetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				globalParam.IsDirtyPartyEdit = false;
				if (globalParam.SavedPartyData != null)
				{
					for (int i = 0; i < globalParam.SavedPartyData.Count; i++)
					{
						bool flag = false;
						UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserBattlePartyData(globalParam.SavedPartyData[i].MngID);
						if (userBattlePartyData != null)
						{
							for (int j = 0; j < globalParam.SavedPartyData[i].WeaponMngIDs.Length; j++)
							{
								if (globalParam.SavedPartyData[i].WeaponMngIDs[j] != userBattlePartyData.WeaponMngIDs[j])
								{
									flag = true;
									break;
								}
							}
						}
						if (flag)
						{
							MissionManager.UnlockAction(eXlsMissionSeg.Edit, eXlsMissionEditFuncType.WeaponEquipment);
							break;
						}
					}
				}
				this.m_OnResultPartySetAllCallBack.Call();
			}
		}

		// Token: 0x06001585 RID: 5509 RVA: 0x0007023C File Offset: 0x0006E63C
		public void PlayInBGFilter()
		{
			this.m_BGFilter.PlayIn();
		}

		// Token: 0x06001586 RID: 5510 RVA: 0x00070249 File Offset: 0x0006E649
		public void PlayOutBGFilter()
		{
			this.m_BGFilter.PlayOut();
		}

		// Token: 0x06001587 RID: 5511 RVA: 0x00070256 File Offset: 0x0006E656
		public void BGLoad(int chapterId, bool fade)
		{
			if (chapterId >= 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.QuestStory_0 + chapterId - 1, fade);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Quest, fade);
			}
		}

		// Token: 0x04001C42 RID: 7234
		public const int STATE_INIT = 0;

		// Token: 0x04001C43 RID: 7235
		public const int STATE_QUEST_CATEGORY_SELECT = 1;

		// Token: 0x04001C44 RID: 7236
		public const int STATE_QUEST_GROUP_SELECT = 2;

		// Token: 0x04001C45 RID: 7237
		public const int STATE_QUEST_CHAPTER_SELECT = 3;

		// Token: 0x04001C46 RID: 7238
		public const int STATE_QUEST_SELECT = 4;

		// Token: 0x04001C47 RID: 7239
		public const int STATE_QUEST_CONFIRM = 5;

		// Token: 0x04001C48 RID: 7240
		public const int STATE_QUEST_CHARALIST_PARTY_EDIT = 6;

		// Token: 0x04001C49 RID: 7241
		public const int STATE_QUEST_FRIEND_SELECT = 7;

		// Token: 0x04001C4D RID: 7245
		[SerializeField]
		private AnimUIPlayer m_BGFilter;

		// Token: 0x04001C4E RID: 7246
		public bool m_ForceChapterOpenAnim;

		// Token: 0x04001C4F RID: 7247
		protected Action m_OnResultPartySetAllCallBack;
	}
}
