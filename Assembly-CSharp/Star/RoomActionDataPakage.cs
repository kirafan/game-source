﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Star
{
	// Token: 0x0200053B RID: 1339
	[Serializable]
	public class RoomActionDataPakage : IDataBaseResource
	{
		// Token: 0x06001AB0 RID: 6832 RVA: 0x0008E7F4 File Offset: 0x0008CBF4
		public static void ReadPakage()
		{
			if (RoomActionDataPakage.ms_Pakage == null)
			{
				RoomActionDataPakage.ms_Pakage = new RoomActionDataPakage();
				RoomActionDataPakage.ms_Pakage.LoadResInManager("Prefab/Room/Action/RoomActionData", ".bytes");
			}
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x0008E81E File Offset: 0x0008CC1E
		public static bool IsSetUp()
		{
			return RoomActionDataPakage.ms_Pakage.m_Active;
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x0008E82A File Offset: 0x0008CC2A
		public static void DataRelease()
		{
			if (RoomActionDataPakage.ms_Pakage != null)
			{
				RoomActionDataPakage.ms_Pakage.Release();
			}
			RoomActionDataPakage.ms_Pakage = null;
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x0008E848 File Offset: 0x0008CC48
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
			TextAsset textAsset = pdataobj as TextAsset;
			BinaryReader binaryReader = new BinaryReader();
			binaryReader.SetBinary(textAsset.bytes, textAsset.bytes.Length);
			RoomActionDataPakage.RoomActionDataHeader roomActionDataHeader = default(RoomActionDataPakage.RoomActionDataHeader);
			binaryReader.Int(ref roomActionDataHeader.m_Ver);
			binaryReader.Int(ref roomActionDataHeader.m_Num);
			binaryReader.Int(ref roomActionDataHeader.m_Dummy1);
			binaryReader.Int(ref roomActionDataHeader.m_Dummy2);
			int[] intTable = binaryReader.GetIntTable(roomActionDataHeader.m_Num + 1);
			this.m_TagTable = new RoomActionScriptDB[roomActionDataHeader.m_Num];
			for (int i = 0; i < roomActionDataHeader.m_Num; i++)
			{
				this.m_TagTable[i] = new RoomActionScriptDB();
				binaryReader.PushPoint(intTable[i]);
				ActXlsKeyTag actXlsKeyTag = new ActXlsKeyTag();
				actXlsKeyTag.ReadIO(binaryReader);
				RoomActionDataPakage.DataMakeToActionScript(this.m_TagTable[i], actXlsKeyTag);
				binaryReader.PopPoint();
			}
		}

		// Token: 0x06001AB4 RID: 6836 RVA: 0x0008E930 File Offset: 0x0008CD30
		public static RoomActionScriptDB GetActionScript(int faccesskey)
		{
			RoomActionScriptDB result = null;
			int num = RoomActionDataPakage.ms_Pakage.m_TagTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (RoomActionDataPakage.ms_Pakage.m_TagTable[i].m_AccessKey == faccesskey)
				{
					result = RoomActionDataPakage.ms_Pakage.m_TagTable[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06001AB5 RID: 6837 RVA: 0x0008E988 File Offset: 0x0008CD88
		private static void DataMakeToActionScript(RoomActionScriptDB pdat, ActXlsKeyTag pfile)
		{
			pdat.m_Name = pfile.m_KeyName;
			pdat.m_AccessKey = pfile.m_AccessKey;
			pdat.m_LinkDir = pfile.m_LinkKey;
			pdat.m_Table = new IRoomScriptData[pfile.m_List.Length];
			for (int i = 0; i < pfile.m_List.Length; i++)
			{
				ActXlsKeyBase actXlsKeyBase = pfile.m_List[i];
				IRoomScriptData roomScriptData = pdat.m_Table[i] = IRoomScriptData.CreateScriptData(actXlsKeyBase.m_Type);
				roomScriptData.m_Time = (float)actXlsKeyBase.m_Time / 30f;
				roomScriptData.CopyDat(actXlsKeyBase);
			}
		}

		// Token: 0x0400213E RID: 8510
		private static RoomActionDataPakage ms_Pakage;

		// Token: 0x0400213F RID: 8511
		private RoomActionScriptDB[] m_TagTable;

		// Token: 0x0200053C RID: 1340
		[StructLayout(LayoutKind.Explicit)]
		public struct RoomActionDataHeader
		{
			// Token: 0x04002140 RID: 8512
			[FieldOffset(0)]
			public int m_Ver;

			// Token: 0x04002141 RID: 8513
			[FieldOffset(4)]
			public int m_Num;

			// Token: 0x04002142 RID: 8514
			[FieldOffset(8)]
			public int m_Dummy1;

			// Token: 0x04002143 RID: 8515
			[FieldOffset(12)]
			public int m_Dummy2;
		}

		// Token: 0x0200053D RID: 1341
		[StructLayout(LayoutKind.Explicit)]
		public struct RoomActionTagHeader
		{
			// Token: 0x04002144 RID: 8516
			[FieldOffset(0)]
			public int m_KeyID;

			// Token: 0x04002145 RID: 8517
			[FieldOffset(4)]
			public int m_Size;
		}
	}
}
