﻿using System;

namespace Star
{
	// Token: 0x0200001C RID: 28
	[Serializable]
	public struct ADVMotionListDB_Param
	{
		// Token: 0x040000B5 RID: 181
		public string m_MotionID;

		// Token: 0x040000B6 RID: 182
		public ADVMotionListDB_Data[] m_Datas;
	}
}
