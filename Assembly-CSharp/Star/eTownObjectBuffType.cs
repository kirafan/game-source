﻿using System;

namespace Star
{
	// Token: 0x02000245 RID: 581
	public enum eTownObjectBuffType
	{
		// Token: 0x04001320 RID: 4896
		None = -1,
		// Token: 0x04001321 RID: 4897
		Hp,
		// Token: 0x04001322 RID: 4898
		Atk,
		// Token: 0x04001323 RID: 4899
		Mgc,
		// Token: 0x04001324 RID: 4900
		Def,
		// Token: 0x04001325 RID: 4901
		MDef,
		// Token: 0x04001326 RID: 4902
		Spd,
		// Token: 0x04001327 RID: 4903
		Luck,
		// Token: 0x04001328 RID: 4904
		ResistFire,
		// Token: 0x04001329 RID: 4905
		ResistWater,
		// Token: 0x0400132A RID: 4906
		ResistEarth,
		// Token: 0x0400132B RID: 4907
		ResistWind,
		// Token: 0x0400132C RID: 4908
		ResistMoon,
		// Token: 0x0400132D RID: 4909
		ResistSun,
		// Token: 0x0400132E RID: 4910
		Num
	}
}
