﻿using System;

namespace Star
{
	// Token: 0x020003EA RID: 1002
	public class DownloadState : GameStateBase
	{
		// Token: 0x0600130E RID: 4878 RVA: 0x00065F56 File Offset: 0x00064356
		public DownloadState(DownloadMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600130F RID: 4879 RVA: 0x00065F65 File Offset: 0x00064365
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001310 RID: 4880 RVA: 0x00065F68 File Offset: 0x00064368
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001311 RID: 4881 RVA: 0x00065F6A File Offset: 0x0006436A
		public override void OnStateExit()
		{
		}

		// Token: 0x06001312 RID: 4882 RVA: 0x00065F6C File Offset: 0x0006436C
		public override void OnDispose()
		{
		}

		// Token: 0x06001313 RID: 4883 RVA: 0x00065F6E File Offset: 0x0006436E
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001314 RID: 4884 RVA: 0x00065F71 File Offset: 0x00064371
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040019F1 RID: 6641
		protected DownloadMain m_Owner;
	}
}
