﻿using System;

namespace Star
{
	// Token: 0x020002AD RID: 685
	public abstract class CharacterEditResultSceneBeforeAfterPairPartBasedOutputCharaParamCharacterDataWrapper<ResultType> : CharacterEditResultSceneBeforeAfterPairPartCharacterDataWrapper<ResultType> where ResultType : EditMain.EditAdditiveSceneResultData
	{
		// Token: 0x06000CDF RID: 3295 RVA: 0x00049002 File Offset: 0x00047402
		public CharacterEditResultSceneBeforeAfterPairPartBasedOutputCharaParamCharacterDataWrapper(eCharacterDataType characterDataType, ResultType result) : base(characterDataType, result)
		{
			this.m_isCalculated = false;
		}

		// Token: 0x06000CE0 RID: 3296 RVA: 0x00049014 File Offset: 0x00047414
		public override int GetLv()
		{
			return this.GetCharaParamLvOnly().Lv;
		}

		// Token: 0x06000CE1 RID: 3297 RVA: 0x00049030 File Offset: 0x00047430
		public override long GetNowExp()
		{
			return this.GetCharaParamLvOnly().NowExp;
		}

		// Token: 0x06000CE2 RID: 3298 RVA: 0x0004904C File Offset: 0x0004744C
		public override long GetNowMaxExp()
		{
			return this.GetCharaParamLvOnly().NextMaxExp;
		}

		// Token: 0x06000CE3 RID: 3299 RVA: 0x00049067 File Offset: 0x00047467
		public override EditUtility.OutputCharaParam GetCharaParamLvOnly()
		{
			if (!this.m_isCalculated)
			{
				this.Calculate();
			}
			return this.m_CharaParam;
		}

		// Token: 0x06000CE4 RID: 3300 RVA: 0x00049080 File Offset: 0x00047480
		protected void Calculate()
		{
			if (!this.m_isCalculated)
			{
				this.m_CharaParam = this.ConvertResultDataToCharaParam(this.m_Result);
				this.m_isCalculated = true;
			}
		}

		// Token: 0x06000CE5 RID: 3301
		protected abstract EditUtility.OutputCharaParam ConvertResultDataToCharaParam(ResultType result);

		// Token: 0x0400154A RID: 5450
		private bool m_isCalculated;

		// Token: 0x0400154B RID: 5451
		private EditUtility.OutputCharaParam m_CharaParam;
	}
}
