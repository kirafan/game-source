﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020003B6 RID: 950
	public class TownServerForm
	{
		// Token: 0x020003B7 RID: 951
		public enum eDataType
		{
			// Token: 0x04001892 RID: 6290
			Build,
			// Token: 0x04001893 RID: 6291
			DebugPlay,
			// Token: 0x04001894 RID: 6292
			Dummy
		}

		// Token: 0x020003B8 RID: 952
		public class Build
		{
			// Token: 0x04001895 RID: 6293
			public List<UserTownData.BuildObjectUp> m_Table;
		}

		// Token: 0x020003B9 RID: 953
		public class DebugPlay
		{
			// Token: 0x04001896 RID: 6294
			public bool m_DebugPlay;

			// Token: 0x04001897 RID: 6295
			public int m_Level;

			// Token: 0x04001898 RID: 6296
			public long m_PlayTimes;
		}

		// Token: 0x020003BA RID: 954
		public class DataChunk
		{
			// Token: 0x04001899 RID: 6297
			public int m_Ver;

			// Token: 0x0400189A RID: 6298
			public int m_DataType;

			// Token: 0x0400189B RID: 6299
			public byte[] m_Data;
		}
	}
}
