﻿using System;

namespace Star
{
	// Token: 0x020002A7 RID: 679
	public class HaveBeforeAfterCharaParamCharacterEditSceneCharacterParamWrapper<SimulateParamType> : CharacterEditSceneCharacterParamWrapperExGen<SimulateParamType> where SimulateParamType : EditUtility.HaveBeforeAfterCharaParamSimulateParam
	{
		// Token: 0x06000CCD RID: 3277 RVA: 0x00048E64 File Offset: 0x00047264
		public HaveBeforeAfterCharaParamCharacterEditSceneCharacterParamWrapper(eCharacterDataType characterDataType, SimulateParamType param) : base(characterDataType, param)
		{
			this.m_Param = param;
		}

		// Token: 0x06000CCE RID: 3278 RVA: 0x00048E75 File Offset: 0x00047275
		public override int GetLv()
		{
			return this.m_Param.after.Lv;
		}

		// Token: 0x06000CCF RID: 3279 RVA: 0x00048E8C File Offset: 0x0004728C
		public override long GetNowExp()
		{
			return this.m_Param.after.NowExp;
		}

		// Token: 0x06000CD0 RID: 3280 RVA: 0x00048EA3 File Offset: 0x000472A3
		public override long GetNowMaxExp()
		{
			return this.m_Param.after.NextMaxExp;
		}

		// Token: 0x06000CD1 RID: 3281 RVA: 0x00048EBA File Offset: 0x000472BA
		public override EditUtility.OutputCharaParam GetCharaParamLvOnly()
		{
			return this.m_Param.after;
		}
	}
}
