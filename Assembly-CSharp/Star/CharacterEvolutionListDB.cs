﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200016B RID: 363
	public class CharacterEvolutionListDB : ScriptableObject
	{
		// Token: 0x040009C0 RID: 2496
		public CharacterEvolutionListDB_Param[] m_Params;
	}
}
