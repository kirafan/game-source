﻿using System;
using Star.UI;

namespace Star
{
	// Token: 0x020003D8 RID: 984
	public static class GachaUtility
	{
		// Token: 0x060012A6 RID: 4774 RVA: 0x0006425F File Offset: 0x0006265F
		public static bool IsCostInvalid(int amount)
		{
			return amount == -1;
		}

		// Token: 0x060012A7 RID: 4775 RVA: 0x0006426B File Offset: 0x0006266B
		public static bool IsCostFree(int amount)
		{
			return amount == 0;
		}

		// Token: 0x060012A8 RID: 4776 RVA: 0x00064276 File Offset: 0x00062676
		public static bool IsCostConsume(int amount)
		{
			return amount > 0;
		}

		// Token: 0x060012A9 RID: 4777 RVA: 0x00064282 File Offset: 0x00062682
		public static bool IsExistPlayItem(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_ItemID != -1 && gachaData.m_Cost_ItemAmount > 0;
		}

		// Token: 0x060012AA RID: 4778 RVA: 0x0006429F File Offset: 0x0006269F
		public static bool IsExistPlayItem(Gacha.StepGachaData gachaData)
		{
			return false;
		}

		// Token: 0x060012AB RID: 4779 RVA: 0x000642A4 File Offset: 0x000626A4
		public static bool IsEnablePlayItem(Gacha.GachaData gachaData)
		{
			if (gachaData.m_Cost_ItemID != -1 && gachaData.m_Cost_ItemAmount > 0)
			{
				int itemNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(gachaData.m_Cost_ItemID);
				if (itemNum >= gachaData.m_Cost_ItemAmount)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060012AC RID: 4780 RVA: 0x000642EE File Offset: 0x000626EE
		public static bool IsEnablePlayItem(Gacha.StepGachaData gachaData)
		{
			return false;
		}

		// Token: 0x060012AD RID: 4781 RVA: 0x000642F1 File Offset: 0x000626F1
		public static bool IsEnablePlayGem1(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_Gem1 != -1;
		}

		// Token: 0x060012AE RID: 4782 RVA: 0x00064302 File Offset: 0x00062702
		public static bool IsEnablePlayFirst10(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_First10 != -1 && gachaData.m_PlayNum_Gem10Total == 0;
		}

		// Token: 0x060012AF RID: 4783 RVA: 0x0006431E File Offset: 0x0006271E
		public static bool IsEnablePlayGem10(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_Gem10 != -1;
		}

		// Token: 0x060012B0 RID: 4784 RVA: 0x0006432F File Offset: 0x0006272F
		public static bool IsExistPlayUnlimitedGem(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_UnlimitedGem1 != -1;
		}

		// Token: 0x060012B1 RID: 4785 RVA: 0x00064340 File Offset: 0x00062740
		public static bool IsEnablePlayUnlimitedGem(Gacha.GachaData gachaData)
		{
			return gachaData.m_Cost_UnlimitedGem1 != -1 && gachaData.m_PlayNum_UnlimitedGem1Daily == 0;
		}

		// Token: 0x060012B2 RID: 4786 RVA: 0x0006435C File Offset: 0x0006275C
		public static int GetPlayNum(Gacha.GachaData gachaData)
		{
			int num = gachaData.m_PlayNum_Gem1Total + 10 * gachaData.m_PlayNum_Gem10Total + gachaData.m_PlayNum_UnlimitedGem1Total + gachaData.m_PlayNum_Item1Total;
			if (gachaData.m_Type == eGachaType.Won && gachaData.m_WonIsReset)
			{
				num %= gachaData.m_WonLimit;
			}
			return num;
		}

		// Token: 0x060012B3 RID: 4787 RVA: 0x000643A9 File Offset: 0x000627A9
		public static bool IsEnablePlayLimitedGemStep(Gacha.StepGachaData gachaData)
		{
			return gachaData.m_Steps[GachaUtility.GetCurrentStep(gachaData)].m_Cost_LimitedGem != -1;
		}

		// Token: 0x060012B4 RID: 4788 RVA: 0x000643C6 File Offset: 0x000627C6
		public static bool IsEnablePlayUnlimitedGemStep(Gacha.StepGachaData gachaData)
		{
			return gachaData.m_Steps[GachaUtility.GetCurrentStep(gachaData)].m_Cost_UnlimitedGem != -1;
		}

		// Token: 0x060012B5 RID: 4789 RVA: 0x000643E3 File Offset: 0x000627E3
		public static int GetCurrentStep(Gacha.StepGachaData gachaData)
		{
			return gachaData.m_StepCurrent % gachaData.m_StepMax;
		}

		// Token: 0x060012B6 RID: 4790 RVA: 0x000643F2 File Offset: 0x000627F2
		public static int GetMaxStep(Gacha.StepGachaData gachaData)
		{
			return gachaData.m_StepMax;
		}

		// Token: 0x060012B7 RID: 4791 RVA: 0x000643FC File Offset: 0x000627FC
		public static GachaDefine.eCheckPlay CheckPlay(Gacha.GachaData gachaData, GachaDefine.ePlayType playType, out CommonMessageWindow.eType out_windowType, out string out_messageTitle, out string out_message)
		{
			out_windowType = CommonMessageWindow.eType.OK;
			out_messageTitle = null;
			out_message = null;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			int num = int.MaxValue;
			int num2;
			if (playType == GachaDefine.ePlayType.Item)
			{
				num2 = 1;
				num = gachaData.m_Cost_ItemAmount;
			}
			else
			{
				if (playType == GachaDefine.ePlayType.Gem10)
				{
					num2 = 10;
				}
				else
				{
					num2 = 1;
				}
				if (playType == GachaDefine.ePlayType.Gem10)
				{
					bool flag = GachaUtility.IsEnablePlayFirst10(gachaData);
					if (flag)
					{
						num = gachaData.m_Cost_First10;
					}
					else
					{
						num = gachaData.m_Cost_Gem10;
					}
				}
				else if (playType == GachaDefine.ePlayType.Gem1)
				{
					num = gachaData.m_Cost_Gem1;
				}
				else if (playType == GachaDefine.ePlayType.Unlimited)
				{
					num = gachaData.m_Cost_UnlimitedGem1;
				}
			}
			switch (playType)
			{
			case GachaDefine.ePlayType.Unlimited:
				if (inst.UserDataMng.UserData.IsShortOfUnlimitedGem((long)num))
				{
					out_windowType = CommonMessageWindow.eType.YES_NO;
					out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShortTitle);
					out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShort);
					return GachaDefine.eCheckPlay.GemIsShort;
				}
				break;
			case GachaDefine.ePlayType.Gem1:
			case GachaDefine.ePlayType.Gem10:
				if (inst.UserDataMng.UserData.IsShortOfGem((long)num))
				{
					out_windowType = CommonMessageWindow.eType.YES_NO;
					out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShortTitle);
					out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShort);
					return GachaDefine.eCheckPlay.GemIsShort;
				}
				break;
			case GachaDefine.ePlayType.Item:
				if (inst.UserDataMng.GetItemNum(gachaData.m_Cost_ItemID) < num)
				{
					ItemListDB_Param param = inst.DbMng.ItemListDB.GetParam(gachaData.m_Cost_ItemID);
					out_windowType = CommonMessageWindow.eType.OK;
					out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayItemIsShortTitle);
					out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayItemIsShort, new object[]
					{
						param.m_Name
					});
					return GachaDefine.eCheckPlay.ItemIsShort;
				}
				break;
			}
			out_windowType = CommonMessageWindow.eType.YES_NO;
			out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayConfirmTitle);
			if (playType == GachaDefine.ePlayType.Item)
			{
				ItemListDB_Param param2 = inst.DbMng.ItemListDB.GetParam(gachaData.m_Cost_ItemID);
				out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayConfirmItem, new object[]
				{
					param2.m_Name,
					num,
					num2
				});
			}
			else
			{
				out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayConfirm, new object[]
				{
					num,
					num2
				});
			}
			return GachaDefine.eCheckPlay.Ok;
		}

		// Token: 0x060012B8 RID: 4792 RVA: 0x00064648 File Offset: 0x00062A48
		public static GachaDefine.eCheckPlay CheckStepPlay(Gacha.StepGachaData stepGachaData, out CommonMessageWindow.eType out_windowType, out string out_messageTitle, out string out_message)
		{
			out_windowType = CommonMessageWindow.eType.OK;
			out_messageTitle = null;
			out_message = null;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			int currentStep = GachaUtility.GetCurrentStep(stepGachaData);
			int maxStep = GachaUtility.GetMaxStep(stepGachaData);
			int num;
			bool flag;
			if (GachaUtility.IsEnablePlayUnlimitedGemStep(stepGachaData))
			{
				num = stepGachaData.m_Steps[currentStep].m_Cost_UnlimitedGem;
				flag = inst.UserDataMng.UserData.IsShortOfUnlimitedGem((long)num);
			}
			else
			{
				num = stepGachaData.m_Steps[currentStep].m_Cost_LimitedGem;
				flag = inst.UserDataMng.UserData.IsShortOfGem((long)num);
			}
			if (flag)
			{
				out_windowType = CommonMessageWindow.eType.YES_NO;
				out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShortTitle);
				out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayGemIsShort);
				return GachaDefine.eCheckPlay.GemIsShort;
			}
			out_windowType = CommonMessageWindow.eType.YES_NO;
			out_messageTitle = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayConfirmTitle);
			out_message = inst.DbMng.GetTextMessage(eText_MessageDB.GachaPlayConfirmStep, new object[]
			{
				num,
				currentStep + 1,
				maxStep
			});
			return GachaDefine.eCheckPlay.Ok;
		}
	}
}
