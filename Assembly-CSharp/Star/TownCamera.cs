﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000673 RID: 1651
	public class TownCamera : MonoBehaviour
	{
		// Token: 0x06002169 RID: 8553 RVA: 0x000B1BA8 File Offset: 0x000AFFA8
		private void Awake()
		{
			this.m_PinchEventListener = new TownPinchEvent();
			this.m_PinchState = new TownPinchState();
			this.m_Anime = new TownCameraKeyPakage();
			this.m_ThresholdMovePixel = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * this.m_ThresholdMoveRatio;
		}

		// Token: 0x0600216A RID: 8554 RVA: 0x000B1C04 File Offset: 0x000B0004
		private void Update()
		{
			if (!this.m_Anime.PakageUpdate())
			{
				this.m_PinchEventListener.UpdatePinch(this.m_PinchState);
				if (this.m_PinchState.m_Event)
				{
					this.OnPinch(this.m_PinchState);
				}
				this.UpdateMove();
			}
		}

		// Token: 0x0600216B RID: 8555 RVA: 0x000B1C54 File Offset: 0x000B0054
		public void SetIsControllable(bool flg)
		{
			this.m_IsControllable = flg;
			if (!this.m_IsControllable)
			{
				this.IsDragging = false;
				this.m_IsPinching = false;
			}
		}

		// Token: 0x0600216C RID: 8556 RVA: 0x000B1C78 File Offset: 0x000B0078
		public void PushPopBackUpData()
		{
			TownCameraBackUpKey townCameraBackUpKey = SingletonMonoBehaviour<GameSystem>.Inst.GetComponent<TownCameraBackUpKey>();
			if (townCameraBackUpKey == null)
			{
				townCameraBackUpKey = SingletonMonoBehaviour<GameSystem>.Inst.gameObject.AddComponent<TownCameraBackUpKey>();
			}
			townCameraBackUpKey.m_BackPos = base.transform.position;
			townCameraBackUpKey.m_OrthoSize = this.m_Camera.orthographicSize;
		}

		// Token: 0x0600216D RID: 8557 RVA: 0x000B1CD0 File Offset: 0x000B00D0
		public void PopBackUpData(bool fnooverwrite)
		{
			TownCameraBackUpKey component = SingletonMonoBehaviour<GameSystem>.Inst.GetComponent<TownCameraBackUpKey>();
			if (component != null)
			{
				if (!fnooverwrite)
				{
					this.m_Camera.orthographicSize = component.m_OrthoSize;
					base.transform.position = component.m_BackPos;
				}
				UnityEngine.Object.DestroyImmediate(component);
			}
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x000B1D22 File Offset: 0x000B0122
		public float GetSizeMax()
		{
			return this.m_SizeMax;
		}

		// Token: 0x0600216F RID: 8559 RVA: 0x000B1D2A File Offset: 0x000B012A
		public float GetSizePer()
		{
			return (this.m_Camera.orthographicSize - this.m_SizeMin) / (this.m_SizeMax - this.m_SizeMin);
		}

		// Token: 0x06002170 RID: 8560 RVA: 0x000B1D4C File Offset: 0x000B014C
		public void SetZoom(float size, bool force = true)
		{
			if (float.IsInfinity(size) || float.IsNaN(size))
			{
				return;
			}
			float orthographicSize;
			if (!force)
			{
				orthographicSize = Mathf.Clamp(size, this.m_SizeMin, this.m_SizeMax);
			}
			else
			{
				orthographicSize = size;
			}
			this.m_MoveRange = this.CalcMoveRange(orthographicSize);
			this.m_Camera.orthographicSize = orthographicSize;
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x06002171 RID: 8561 RVA: 0x000B1F4C File Offset: 0x000B034C
		private void OnPinch(TownPinchState listener)
		{
			if (!this.m_IsControllable)
			{
				return;
			}
			if (UIUtility.CheckPointOverUI(InputTouch.Inst.GetInfo(0).m_CurrentPos) || UIUtility.CheckPointOverUI(InputTouch.Inst.GetInfo(1).m_CurrentPos))
			{
				return;
			}
			if (listener.IsPinching)
			{
				if (!this.m_IsPinching)
				{
					this.m_IsPinching = true;
					this.m_SizeOnStartPinch = this.m_Camera.orthographicSize;
				}
				this.SetZoom(this.m_SizeOnStartPinch / listener.PinchRatio, false);
			}
			else
			{
				this.m_IsPinching = false;
			}
		}

		// Token: 0x06002172 RID: 8562 RVA: 0x000B1FF0 File Offset: 0x000B03F0
		public void SetPositionKey(Vector2 fmovepos, float ftime)
		{
			Vector3 localPosition = this.m_Transform.localPosition;
			if (ftime == 0f)
			{
				localPosition.x = fmovepos.x;
				localPosition.y = fmovepos.y;
				this.m_Transform.localPosition = localPosition;
			}
			else
			{
				Vector3 ftargetpos = localPosition;
				ftargetpos.x = fmovepos.x;
				ftargetpos.y = fmovepos.y;
				TownCameraMove paction = new TownCameraMove(this, localPosition, ftargetpos, ftime);
				this.m_Anime.EntryAction(paction, 0);
			}
		}

		// Token: 0x06002173 RID: 8563 RVA: 0x000B2078 File Offset: 0x000B0478
		public void SetSizeKey(float fsize, float ftime, bool force = false)
		{
			if (ftime == 0f)
			{
				this.SetZoom(fsize, force);
			}
			else
			{
				TownCameraSize paction = new TownCameraSize(this, this.m_Camera.orthographicSize, fsize, ftime);
				this.m_Anime.EntryAction(paction, 0);
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06002174 RID: 8564 RVA: 0x000B20BE File Offset: 0x000B04BE
		// (set) Token: 0x06002175 RID: 8565 RVA: 0x000B20C9 File Offset: 0x000B04C9
		public bool IsDragging
		{
			get
			{
				return this.m_DragState == TownCamera.eDragState.Dragging;
			}
			set
			{
				if (value)
				{
					this.m_DragState = TownCamera.eDragState.Dragging;
				}
				else
				{
					this.m_DragState = TownCamera.eDragState.None;
				}
			}
		}

		// Token: 0x06002176 RID: 8566 RVA: 0x000B20E4 File Offset: 0x000B04E4
		public void SetCameraMoveVec(Vector2 move)
		{
			this.m_vMove = move;
		}

		// Token: 0x06002177 RID: 8567 RVA: 0x000B20ED File Offset: 0x000B04ED
		public Camera GetCamera()
		{
			return this.m_Camera;
		}

		// Token: 0x06002178 RID: 8568 RVA: 0x000B20F5 File Offset: 0x000B04F5
		public void BuildMoveRange()
		{
			this.UpdateMoveRange();
		}

		// Token: 0x06002179 RID: 8569 RVA: 0x000B20FD File Offset: 0x000B04FD
		private void UpdateMoveRange()
		{
			this.m_MoveRange = this.CalcMoveRange(this.m_Camera.orthographicSize);
		}

		// Token: 0x0600217A RID: 8570 RVA: 0x000B2118 File Offset: 0x000B0518
		private Rect CalcMoveRange(float orthographicSize)
		{
			float num = orthographicSize * this.m_Camera.aspect;
			float num2 = num + num;
			float num3 = orthographicSize + orthographicSize;
			float num4 = this.m_RangeW - num2;
			if (num4 < 0f)
			{
				num4 = 0f;
			}
			float num5 = this.m_RangeH - num3;
			if (num5 < 0f)
			{
				num5 = 0f;
			}
			return new Rect(-num4 / 2f, -num5 / 2f, num4, num5);
		}

		// Token: 0x0600217B RID: 8571 RVA: 0x000B2194 File Offset: 0x000B0594
		private void UpdateMove()
		{
			if (this.m_IsControllable)
			{
				int availableTouchCnt = InputTouch.Inst.GetAvailableTouchCnt();
				TownCamera.eDragState dragState = this.m_DragState;
				if (dragState != TownCamera.eDragState.None)
				{
					if (dragState != TownCamera.eDragState.Ready)
					{
						if (dragState == TownCamera.eDragState.Dragging)
						{
							bool flag = true;
							if (availableTouchCnt == 1 && !this.m_IsPinching)
							{
								InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
								if (info.m_bAvailable && (info.m_TouchPhase == TouchPhase.Moved || info.m_TouchPhase == TouchPhase.Stationary))
								{
									flag = false;
									this.m_vMove = info.m_DiffFromPrev * info.m_DiffFromPrev.z;
									this.m_vMove.x = this.m_vMove.x * (this.m_Camera.orthographicSize * 2f / (float)Screen.width * this.m_Camera.aspect);
									this.m_vMove.y = this.m_vMove.y * (this.m_Camera.orthographicSize * 2f / (float)Screen.height);
									this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
								}
							}
							if (flag)
							{
								this.m_DragState = TownCamera.eDragState.None;
							}
						}
					}
					else
					{
						bool flag2 = true;
						if (availableTouchCnt == 1 && !this.m_IsPinching)
						{
							InputTouch.TOUCH_INFO info2 = InputTouch.Inst.GetInfo(0);
							if (info2.m_bAvailable && (info2.m_TouchPhase == TouchPhase.Moved || info2.m_TouchPhase == TouchPhase.Stationary))
							{
								flag2 = false;
								if (info2.m_DiffFromStart.z >= this.m_ThresholdMovePixel)
								{
									this.m_DragState = TownCamera.eDragState.Dragging;
								}
							}
						}
						if (flag2)
						{
							this.m_DragState = TownCamera.eDragState.None;
						}
					}
				}
				else if (availableTouchCnt == 1 && !this.m_IsPinching)
				{
					InputTouch.TOUCH_INFO info3 = InputTouch.Inst.GetInfo(0);
					if (info3.m_bAvailable && info3.m_TouchPhase == TouchPhase.Began && !UIUtility.CheckPointOverUI(info3.m_StartPos))
					{
						this.m_DragState = TownCamera.eDragState.Ready;
					}
				}
			}
			if (!this.IsDragging && !this.m_IsPinching)
			{
				this.m_vMove *= 1f - this.m_Inertia;
				if (this.m_vMove.sqrMagnitude > 0f)
				{
					this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
				}
			}
			if (this.m_IsPinching)
			{
				this.m_vMove = Vector2.zero;
			}
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x040027C3 RID: 10179
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040027C4 RID: 10180
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x040027C5 RID: 10181
		private TownPinchEvent m_PinchEventListener;

		// Token: 0x040027C6 RID: 10182
		private TownPinchState m_PinchState;

		// Token: 0x040027C7 RID: 10183
		[SerializeField]
		private float m_ThresholdMoveRatio = 0.005f;

		// Token: 0x040027C8 RID: 10184
		private float m_ThresholdMovePixel;

		// Token: 0x040027C9 RID: 10185
		private bool m_IsControllable = true;

		// Token: 0x040027CA RID: 10186
		private TownCameraKeyPakage m_Anime;

		// Token: 0x040027CB RID: 10187
		[SerializeField]
		private float m_SizeMin = 3f;

		// Token: 0x040027CC RID: 10188
		[SerializeField]
		private float m_SizeMax = 5.5f;

		// Token: 0x040027CD RID: 10189
		private float m_SizeOnStartPinch = 1f;

		// Token: 0x040027CE RID: 10190
		private bool m_IsPinching;

		// Token: 0x040027CF RID: 10191
		[SerializeField]
		private float m_Inertia = 0.04f;

		// Token: 0x040027D0 RID: 10192
		private Rect m_MoveRange;

		// Token: 0x040027D1 RID: 10193
		[SerializeField]
		public float m_RangeW = 19.2f;

		// Token: 0x040027D2 RID: 10194
		[SerializeField]
		public float m_RangeH = 14.4f;

		// Token: 0x040027D3 RID: 10195
		private Vector2 m_vMove = Vector2.zero;

		// Token: 0x040027D4 RID: 10196
		private TownCamera.eDragState m_DragState = TownCamera.eDragState.None;

		// Token: 0x02000674 RID: 1652
		private enum eDragState
		{
			// Token: 0x040027D6 RID: 10198
			None = -1,
			// Token: 0x040027D7 RID: 10199
			Ready,
			// Token: 0x040027D8 RID: 10200
			Dragging
		}
	}
}
