﻿using System;

namespace Star
{
	// Token: 0x0200018F RID: 399
	public static class CharacterLimitBreakListDB_Ext
	{
		// Token: 0x06000AE2 RID: 2786 RVA: 0x00040FD4 File Offset: 0x0003F3D4
		public static CharacterLimitBreakListDB_Param GetParam(this CharacterLimitBreakListDB self, int recipeID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_RecipeID == recipeID)
				{
					return self.m_Params[i];
				}
			}
			return default(CharacterLimitBreakListDB_Param);
		}
	}
}
