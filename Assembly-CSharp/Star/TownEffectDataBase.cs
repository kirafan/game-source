﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000699 RID: 1689
	public class TownEffectDataBase : MonoBehaviour
	{
		// Token: 0x04002853 RID: 10323
		[SerializeField]
		public TownEffectDataBase.EffectDataSet[] m_List;

		// Token: 0x0200069A RID: 1690
		[Serializable]
		public struct EffectDataSet
		{
			// Token: 0x04002854 RID: 10324
			public string m_FileName;

			// Token: 0x04002855 RID: 10325
			public uint m_AccessKey;

			// Token: 0x04002856 RID: 10326
			public float m_Size;
		}
	}
}
