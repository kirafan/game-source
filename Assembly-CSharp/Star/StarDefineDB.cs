﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000223 RID: 547
	public class StarDefineDB : ScriptableObject
	{
		// Token: 0x04000E7B RID: 3707
		public StarDefineDB_Param[] m_Params;
	}
}
