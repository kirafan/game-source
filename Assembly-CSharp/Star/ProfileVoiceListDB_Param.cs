﻿using System;

namespace Star
{
	// Token: 0x020001E0 RID: 480
	[Serializable]
	public struct ProfileVoiceListDB_Param
	{
		// Token: 0x04000B9B RID: 2971
		public string m_CueName;

		// Token: 0x04000B9C RID: 2972
		public string m_Category;

		// Token: 0x04000B9D RID: 2973
		public string m_Title;

		// Token: 0x04000B9E RID: 2974
		public int m_Friendship;
	}
}
