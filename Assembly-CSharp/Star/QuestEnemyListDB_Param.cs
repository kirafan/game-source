﻿using System;

namespace Star
{
	// Token: 0x020001E5 RID: 485
	[Serializable]
	public struct QuestEnemyListDB_Param
	{
		// Token: 0x04000BA6 RID: 2982
		public int m_ID;

		// Token: 0x04000BA7 RID: 2983
		public string m_CharaName;

		// Token: 0x04000BA8 RID: 2984
		public int m_ResourceID;

		// Token: 0x04000BA9 RID: 2985
		public int m_Element;

		// Token: 0x04000BAA RID: 2986
		public int m_InitLv;

		// Token: 0x04000BAB RID: 2987
		public int m_MaxLv;

		// Token: 0x04000BAC RID: 2988
		public int m_InitHp;

		// Token: 0x04000BAD RID: 2989
		public int m_InitAtk;

		// Token: 0x04000BAE RID: 2990
		public int m_InitMgc;

		// Token: 0x04000BAF RID: 2991
		public int m_InitDef;

		// Token: 0x04000BB0 RID: 2992
		public int m_InitMDef;

		// Token: 0x04000BB1 RID: 2993
		public int m_InitSpd;

		// Token: 0x04000BB2 RID: 2994
		public int m_InitLuck;

		// Token: 0x04000BB3 RID: 2995
		public int m_MaxHp;

		// Token: 0x04000BB4 RID: 2996
		public int m_MaxAtk;

		// Token: 0x04000BB5 RID: 2997
		public int m_MaxMgc;

		// Token: 0x04000BB6 RID: 2998
		public int m_MaxDef;

		// Token: 0x04000BB7 RID: 2999
		public int m_MaxMDef;

		// Token: 0x04000BB8 RID: 3000
		public int m_MaxSpd;

		// Token: 0x04000BB9 RID: 3001
		public int m_MaxLuck;

		// Token: 0x04000BBA RID: 3002
		public int m_AIID;

		// Token: 0x04000BBB RID: 3003
		public int[] m_SkillIDs;

		// Token: 0x04000BBC RID: 3004
		public int[] m_IsConfusionSkillIDs;

		// Token: 0x04000BBD RID: 3005
		public int m_CharageSkillID;

		// Token: 0x04000BBE RID: 3006
		public int m_CharageMax;

		// Token: 0x04000BBF RID: 3007
		public float m_StunCoef;

		// Token: 0x04000BC0 RID: 3008
		public float m_StunerMag;

		// Token: 0x04000BC1 RID: 3009
		public float m_StunerAvoid;

		// Token: 0x04000BC2 RID: 3010
		public int[] m_IsRegistAbnormals;
	}
}
