﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F9 RID: 1529
	public class RoomComWallChange : IRoomEventCommand
	{
		// Token: 0x06001E2F RID: 7727 RVA: 0x000A2654 File Offset: 0x000A0A54
		public RoomComWallChange(int fchangeobj, bool fbuyobj)
		{
			this.m_Type = eRoomRequest.ChangeWall;
			this.m_Enable = true;
			this.m_ObjID = fchangeobj;
			this.m_Flag = 0;
			this.m_ChangeResID = 0;
			this.m_ChangeMngID = -1L;
		}

		// Token: 0x06001E30 RID: 7728 RVA: 0x000A2688 File Offset: 0x000A0A88
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			bool result = false;
			switch (this.m_Step)
			{
			case 0:
			{
				UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(pbuilder.GetManageID());
				if (manageIDToUserRoomData != null)
				{
					UserRoomData.PlacementData categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Wall);
					if (categoryToPlacementData != null)
					{
						UserRoomObjectData userRoomObjectData = UserRoomUtil.GetManageIDToUserRoomObjData(categoryToPlacementData.m_ManageID);
						if (userRoomObjectData != null)
						{
							userRoomObjectData.SetFreeLinkMngID(categoryToPlacementData.m_ManageID);
						}
						userRoomObjectData = UserRoomUtil.GetUserRoomObjData(eRoomObjectCategory.Wall, this.m_ObjID);
						if (userRoomObjectData != null)
						{
							this.m_ChangeMngID = (categoryToPlacementData.m_ManageID = userRoomObjectData.GetFreeLinkMngID(true));
							this.m_ChangeResID = (categoryToPlacementData.m_ObjectID = userRoomObjectData.ResourceID);
						}
					}
				}
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(pbuilder.GetManageID());
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
				roomComBuildScript.PlaySend();
				this.m_Step++;
				break;
			}
			case 1:
			{
				IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
				this.m_NewModelMain = (RoomObjectControllCreate.SetupMdlSimple(floorManager.CreateWallNode(), 0, RoomUtility.GetWallLayer(0) - 10, 0, this.m_ChangeResID, this.m_ChangeMngID, true) as RoomObjectMdlSprite);
				this.m_NewModelMain.transform.localPosition = Vector3.one * 1000f;
				floorManager = pbuilder.GetFloorManager(1);
				this.m_NewModelSub = (RoomObjectControllCreate.SetupMdlSimple(floorManager.CreateWallNode(), 1, RoomUtility.GetWallLayer(1) - 10, 0, this.m_ChangeResID, this.m_ChangeMngID, true) as RoomObjectMdlSprite);
				this.m_NewModelSub.transform.localPosition = Vector3.one * 1000f;
				this.m_Step++;
				break;
			}
			case 2:
				if (!this.m_NewModelMain.IsPreparing())
				{
					this.m_Flag |= 1;
				}
				if (!this.m_NewModelSub.IsPreparing())
				{
					this.m_Flag |= 2;
				}
				if (this.m_Flag >= 3)
				{
					IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
					this.m_NewModelMain.transform.localPosition = floorManager.GetWall().transform.localPosition;
					this.m_NewModelMain.SetSortingOrder(0f, RoomUtility.GetWallLayer(0) + 10);
					this.m_NewModelMain.SetBaseColor(floorManager.GetBaseColor());
					this.m_NewModelMain.SetFadeIn(0.2f);
					floorManager = pbuilder.GetFloorManager(1);
					this.m_NewModelSub.transform.localPosition = floorManager.GetWall().transform.localPosition;
					this.m_NewModelSub.SetSortingOrder(0f, RoomUtility.GetWallLayer(1) + 10);
					this.m_NewModelSub.SetBaseColor(floorManager.GetBaseColor());
					this.m_NewModelSub.SetFadeIn(0.2f);
					this.m_Step++;
				}
				break;
			case 3:
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= 0.2f)
				{
					this.m_NewModelMain.SetSortingOrder(0f, RoomUtility.GetWallLayer(0));
					IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
					floorManager.ChangeWall(this.m_NewModelMain);
					this.m_NewModelMain = null;
					this.m_NewModelSub.SetSortingOrder(0f, RoomUtility.GetWallLayer(1));
					floorManager = pbuilder.GetFloorManager(1);
					floorManager.ChangeWall(this.m_NewModelSub);
					this.m_NewModelSub = null;
					this.m_Step++;
					result = true;
					this.m_Enable = false;
				}
				break;
			}
			return result;
		}

		// Token: 0x040024B3 RID: 9395
		public int m_ObjID;

		// Token: 0x040024B4 RID: 9396
		public RoomObjectMdlSprite m_NewModelMain;

		// Token: 0x040024B5 RID: 9397
		public RoomObjectMdlSprite m_NewModelSub;

		// Token: 0x040024B6 RID: 9398
		public int m_Step;

		// Token: 0x040024B7 RID: 9399
		public int m_Flag;

		// Token: 0x040024B8 RID: 9400
		public float m_Time;

		// Token: 0x040024B9 RID: 9401
		public int m_ChangeResID;

		// Token: 0x040024BA RID: 9402
		public long m_ChangeMngID;
	}
}
