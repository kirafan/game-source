﻿using System;
using System.IO;
using CriMana;
using UnityEngine;

namespace Star
{
	// Token: 0x02000513 RID: 1299
	public class MovieCanvas : MonoBehaviour
	{
		// Token: 0x06001995 RID: 6549 RVA: 0x00085507 File Offset: 0x00083907
		public void OnDestroy()
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController = null;
			}
		}

		// Token: 0x06001996 RID: 6550 RVA: 0x00085524 File Offset: 0x00083924
		public void SetFile(string movieFileNameWithoutExt)
		{
			string installPath = CRIFileInstaller.InstallPath;
			string text = movieFileNameWithoutExt.Contains(".usm") ? movieFileNameWithoutExt : (movieFileNameWithoutExt + ".usm");
			if (!string.IsNullOrEmpty(installPath))
			{
				text = Path.Combine(installPath, text).Replace("\\", "/");
			}
			if (this.m_MovieController != null)
			{
				this.m_MovieController.player.SetFile(null, text, Player.SetMode.New);
			}
		}

		// Token: 0x06001997 RID: 6551 RVA: 0x000855A0 File Offset: 0x000839A0
		public void SetLoop(bool isLoop)
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController.player.Loop(isLoop);
			}
		}

		// Token: 0x06001998 RID: 6552 RVA: 0x000855C4 File Offset: 0x000839C4
		public bool IsPlaying()
		{
			return this.m_MovieController != null && (this.m_MovieController.player.status == Player.Status.Dechead || this.m_MovieController.player.status == Player.Status.WaitPrep || this.m_MovieController.player.status == Player.Status.Prep || this.m_MovieController.player.status == Player.Status.Ready || this.m_MovieController.player.status == Player.Status.Playing);
		}

		// Token: 0x06001999 RID: 6553 RVA: 0x00085653 File Offset: 0x00083A53
		public bool IsPaused()
		{
			return this.m_MovieController != null && this.m_MovieController.player.IsPaused();
		}

		// Token: 0x0600199A RID: 6554 RVA: 0x0008567E File Offset: 0x00083A7E
		public void Play()
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController.Play();
			}
		}

		// Token: 0x0600199B RID: 6555 RVA: 0x0008569C File Offset: 0x00083A9C
		public void Stop()
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController.Stop();
			}
		}

		// Token: 0x0600199C RID: 6556 RVA: 0x000856BA File Offset: 0x00083ABA
		public void Pause()
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController.Pause(true);
			}
		}

		// Token: 0x0600199D RID: 6557 RVA: 0x000856D9 File Offset: 0x00083AD9
		public void Resume()
		{
			if (this.m_MovieController != null)
			{
				this.m_MovieController.Pause(false);
			}
		}

		// Token: 0x04002057 RID: 8279
		[SerializeField]
		private CriManaMovieControllerForUI m_MovieController;
	}
}
