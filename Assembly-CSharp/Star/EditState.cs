﻿using System;

namespace Star
{
	// Token: 0x020003F4 RID: 1012
	public class EditState : GameStateBase
	{
		// Token: 0x0600133D RID: 4925 RVA: 0x00067376 File Offset: 0x00065776
		public EditState(EditMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600133E RID: 4926 RVA: 0x0006738C File Offset: 0x0006578C
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600133F RID: 4927 RVA: 0x0006738F File Offset: 0x0006578F
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001340 RID: 4928 RVA: 0x00067391 File Offset: 0x00065791
		public override void OnStateExit()
		{
		}

		// Token: 0x06001341 RID: 4929 RVA: 0x00067393 File Offset: 0x00065793
		public override void OnDispose()
		{
		}

		// Token: 0x06001342 RID: 4930 RVA: 0x00067395 File Offset: 0x00065795
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001343 RID: 4931 RVA: 0x00067398 File Offset: 0x00065798
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001A42 RID: 6722
		protected EditMain m_Owner;

		// Token: 0x04001A43 RID: 6723
		protected int m_NextState = -1;
	}
}
