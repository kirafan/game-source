﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005E6 RID: 1510
	public static class RoomDefine
	{
		// Token: 0x04002443 RID: 9283
		public const int FLOOR_SLEEP_NO = 1;

		// Token: 0x04002444 RID: 9284
		public const int SORT_OFFSET_KEY = 10;

		// Token: 0x04002445 RID: 9285
		public const int PLACEMENT_MAX = 50;

		// Token: 0x04002446 RID: 9286
		public const int ORDER_BG = -10000;

		// Token: 0x04002447 RID: 9287
		public const int ORDER_START = 0;

		// Token: 0x04002448 RID: 9288
		public const int ORDER_GROUP = 4000;

		// Token: 0x04002449 RID: 9289
		public const int ORDER_WALL = -1000;

		// Token: 0x0400244A RID: 9290
		public const int ORDER_FLOOR = -960;

		// Token: 0x0400244B RID: 9291
		public const int ORDER_FLOOR_GROUP = -100;

		// Token: 0x0400244C RID: 9292
		public const int ORDER_NO_HIT = -900;

		// Token: 0x0400244D RID: 9293
		public const int ORDER_FOREGROUND = 10000;

		// Token: 0x0400244E RID: 9294
		public const int PIXCEL_PER_UNIT = 100;

		// Token: 0x0400244F RID: 9295
		public const float CHIP_TEX_W = 100f;

		// Token: 0x04002450 RID: 9296
		public const float CHIP_TEX_H = 50f;

		// Token: 0x04002451 RID: 9297
		public const float CHIP_TEX_W_HALF = 50f;

		// Token: 0x04002452 RID: 9298
		public const float CHIP_TEX_H_HALF = 25f;

		// Token: 0x04002453 RID: 9299
		public const int BG_LAYER_OFFSET = 200;

		// Token: 0x04002454 RID: 9300
		public const float NO_SORTOBJ_OFFSET = 0.9f;

		// Token: 0x04002455 RID: 9301
		public const byte HitGroupKeyBase = 1;

		// Token: 0x04002456 RID: 9302
		public const byte HitGroupKeyFloor = 2;

		// Token: 0x04002457 RID: 9303
		public const float GRID_PLACEMENT_Z_INTERVAL = 3f;

		// Token: 0x04002458 RID: 9304
		public const int GRID_STATUS_ANY = -1;

		// Token: 0x04002459 RID: 9305
		public const int GRID_STATUS_EMPTY = 1;

		// Token: 0x0400245A RID: 9306
		public static Color GRID_HIT_OK_COLOR = new Color(0.2f, 0.9f, 0.25f, 0.5f);

		// Token: 0x0400245B RID: 9307
		public static Color GRID_HIT_NG_COLOR = new Color(0.9f, 0.2f, 0.25f, 0.5f);

		// Token: 0x0400245C RID: 9308
		public const int CHARA_MOVE_RANGE = 4;

		// Token: 0x0400245D RID: 9309
		public static readonly float[] CHARA_IDLE_PROCESS_TIME = new float[]
		{
			1f,
			3f
		};

		// Token: 0x0400245E RID: 9310
		public const float CHARA_TWEET_PROCESS_TIME = 4.5f;

		// Token: 0x0400245F RID: 9311
		public static int ROOM_SEARCH_SIZE = 4;

		// Token: 0x04002460 RID: 9312
		public static IVector2[] ROOM_SEARCH_DIR = new IVector2[]
		{
			new IVector2(0, -1),
			new IVector2(1, 0),
			new IVector2(0, 1),
			new IVector2(-1, 0)
		};

		// Token: 0x04002461 RID: 9313
		public const float CHARA_SCALE = 1.31f;

		// Token: 0x04002462 RID: 9314
		public const float CHARA_MOVE_SPEED = 0.45f;

		// Token: 0x04002463 RID: 9315
		public const float CHARA_MOVE_TIME = 1.24226f;

		// Token: 0x04002464 RID: 9316
		public const int CHARA_INSERT_ORDER = 2;

		// Token: 0x04002465 RID: 9317
		public const int WAKE_UP_MSG = 2000;

		// Token: 0x04002466 RID: 9318
		public const int SLEEP_MSG = 2001;

		// Token: 0x04002467 RID: 9319
		public const int GO_HOME_MSG = 2002;

		// Token: 0x04002468 RID: 9320
		public const int GO_TOWN_MSG = 2003;

		// Token: 0x04002469 RID: 9321
		public const float CHARA_TOUCH_PROFILE = 0.5f;

		// Token: 0x0400246A RID: 9322
		public const float OBJ_TOUCH_EDITMODE = 1f;

		// Token: 0x020005E7 RID: 1511
		public enum eSearchDir
		{
			// Token: 0x0400246C RID: 9324
			Down,
			// Token: 0x0400246D RID: 9325
			Right,
			// Token: 0x0400246E RID: 9326
			Up,
			// Token: 0x0400246F RID: 9327
			Left
		}

		// Token: 0x020005E8 RID: 1512
		public enum eAnim
		{
			// Token: 0x04002471 RID: 9329
			Idle,
			// Token: 0x04002472 RID: 9330
			Walk,
			// Token: 0x04002473 RID: 9331
			Enjoy,
			// Token: 0x04002474 RID: 9332
			Rejoice,
			// Token: 0x04002475 RID: 9333
			Lift,
			// Token: 0x04002476 RID: 9334
			Max
		}
	}
}
