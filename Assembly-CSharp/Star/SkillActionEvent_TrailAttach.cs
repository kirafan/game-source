﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200011A RID: 282
	[Serializable]
	public class SkillActionEvent_TrailAttach : SkillActionEvent_Base
	{
		// Token: 0x0400072C RID: 1836
		public string m_EffectID;

		// Token: 0x0400072D RID: 1837
		public short m_UniqueID;

		// Token: 0x0400072E RID: 1838
		public eSkillActionTrailAttachTo m_AttachTo;

		// Token: 0x0400072F RID: 1839
		public eSkillActionLocatorType m_AttachToLocatorType;

		// Token: 0x04000730 RID: 1840
		public Vector2 m_PosOffset;
	}
}
