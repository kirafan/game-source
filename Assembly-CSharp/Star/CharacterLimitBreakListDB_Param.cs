﻿using System;

namespace Star
{
	// Token: 0x02000176 RID: 374
	[Serializable]
	public struct CharacterLimitBreakListDB_Param
	{
		// Token: 0x040009D6 RID: 2518
		public int m_RecipeID;

		// Token: 0x040009D7 RID: 2519
		public int m_Rare;

		// Token: 0x040009D8 RID: 2520
		public int m_Class;

		// Token: 0x040009D9 RID: 2521
		public int[] m_CharaMaxLvUps;

		// Token: 0x040009DA RID: 2522
		public int[] m_SkillMaxLvUps;

		// Token: 0x040009DB RID: 2523
		public int[] m_Amounts;

		// Token: 0x040009DC RID: 2524
		public int[] m_ItemIDs_1;

		// Token: 0x040009DD RID: 2525
		public int[] m_ItemNums_1;

		// Token: 0x040009DE RID: 2526
		public int[] m_ItemIDs_2;

		// Token: 0x040009DF RID: 2527
		public int[] m_ItemNums_2;

		// Token: 0x040009E0 RID: 2528
		public int[] m_ItemIDs_3;

		// Token: 0x040009E1 RID: 2529
		public int[] m_ItemNums_3;

		// Token: 0x040009E2 RID: 2530
		public int[] m_TitleItemNums;
	}
}
