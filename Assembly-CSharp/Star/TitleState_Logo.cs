﻿using System;

namespace Star
{
	// Token: 0x020004A1 RID: 1185
	public class TitleState_Logo : TitleState
	{
		// Token: 0x06001742 RID: 5954 RVA: 0x00078D9E File Offset: 0x0007719E
		public TitleState_Logo(TitleMain owner) : base(owner)
		{
		}

		// Token: 0x06001743 RID: 5955 RVA: 0x00078DAE File Offset: 0x000771AE
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001744 RID: 5956 RVA: 0x00078DB1 File Offset: 0x000771B1
		public override void OnStateEnter()
		{
			this.m_Step = TitleState_Logo.eStep.First;
		}

		// Token: 0x06001745 RID: 5957 RVA: 0x00078DBA File Offset: 0x000771BA
		public override void OnStateExit()
		{
		}

		// Token: 0x06001746 RID: 5958 RVA: 0x00078DBC File Offset: 0x000771BC
		public override void OnDispose()
		{
		}

		// Token: 0x06001747 RID: 5959 RVA: 0x00078DC0 File Offset: 0x000771C0
		public override int OnStateUpdate()
		{
			TitleState_Logo.eStep step = this.m_Step;
			if (step != TitleState_Logo.eStep.First)
			{
				if (step == TitleState_Logo.eStep.Logo_Wait)
				{
					if (!this.m_Owner.TitleUI.IsPlayingLogo())
					{
						this.m_Step = TitleState_Logo.eStep.None;
						return 2;
					}
				}
			}
			else
			{
				this.m_Owner.TitleUI.PlayInLogo();
				this.m_Step = TitleState_Logo.eStep.Logo_Wait;
			}
			return -1;
		}

		// Token: 0x04001DF3 RID: 7667
		private TitleState_Logo.eStep m_Step = TitleState_Logo.eStep.None;

		// Token: 0x020004A2 RID: 1186
		private enum eStep
		{
			// Token: 0x04001DF5 RID: 7669
			None = -1,
			// Token: 0x04001DF6 RID: 7670
			First,
			// Token: 0x04001DF7 RID: 7671
			Logo_Wait,
			// Token: 0x04001DF8 RID: 7672
			End
		}
	}
}
