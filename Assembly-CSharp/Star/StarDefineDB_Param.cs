﻿using System;

namespace Star
{
	// Token: 0x02000222 RID: 546
	[Serializable]
	public struct StarDefineDB_Param
	{
		// Token: 0x04000E79 RID: 3705
		public int m_ID;

		// Token: 0x04000E7A RID: 3706
		public float m_Value;
	}
}
