﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.Edit;
using Star.UI.Global;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000461 RID: 1121
	public class QuestState_QuestConfirm : QuestState
	{
		// Token: 0x060015B7 RID: 5559 RVA: 0x00071031 File Offset: 0x0006F431
		public QuestState_QuestConfirm(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015B8 RID: 5560 RVA: 0x00071048 File Offset: 0x0006F448
		public override int GetStateID()
		{
			return 5;
		}

		// Token: 0x060015B9 RID: 5561 RVA: 0x0007104B File Offset: 0x0006F44B
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestConfirm.eStep.First;
		}

		// Token: 0x060015BA RID: 5562 RVA: 0x00071054 File Offset: 0x0006F454
		public override void OnStateExit()
		{
		}

		// Token: 0x060015BB RID: 5563 RVA: 0x00071056 File Offset: 0x0006F456
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015BC RID: 5564 RVA: 0x00071060 File Offset: 0x0006F460
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestConfirm.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = QuestState_QuestConfirm.eStep.LoadWait;
				break;
			case QuestState_QuestConfirm.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestConfirm.eStep.PlayIn;
				}
				break;
			case QuestState_QuestConfirm.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_PARTY_EDIT, null, null);
					this.m_Step = QuestState_QuestConfirm.eStep.Main;
				}
				break;
			case QuestState_QuestConfirm.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					LocalSaveData.Inst.SelectedPartyIndex = this.m_UI.SelectPartyIndex;
					LocalSaveData.Inst.Save();
					GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
					globalParam.SelectedPartyMemberIndex = this.m_UI.SelectCharaIndex;
					globalParam.SelectedCharaMngID = this.m_UI.SelectCharaMngID;
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						PartyEditUIBase.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton != PartyEditUIBase.eButton.Chara)
						{
							if (selectButton != PartyEditUIBase.eButton.Decide)
							{
								if (this.m_Owner.IsQuestStartConfirm)
								{
									this.m_NextState = 7;
								}
								else
								{
									this.m_NextState = 4;
								}
							}
							else if (this.m_IsOutOfPeriod)
							{
								this.m_NextState = 0;
							}
							else if (this.m_Owner.IsQuestStartConfirm)
							{
								this.RequestQuestStartVoice();
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Battle;
								this.m_NextState = 2147483646;
							}
							else
							{
								this.m_NextState = 7;
							}
						}
						else
						{
							this.m_NextState = 6;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestConfirm.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestConfirm.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = QuestState_QuestConfirm.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015BD RID: 5565 RVA: 0x00071280 File Offset: 0x0006F680
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<PartyEditUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			if (this.m_Owner.IsQuestStartConfirm)
			{
				this.m_UI.Setup(LocalSaveData.Inst.SelectedPartyIndex, PartyEditUIBase.eMode.QuestStart, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestUserSupportData);
				this.m_UI.OnClickButton += this.OnClickButton_Confirm;
			}
			else
			{
				this.m_UI.Setup(LocalSaveData.Inst.SelectedPartyIndex, PartyEditUIBase.eMode.Quest, null);
				this.m_UI.OnClickButton += this.OnClickButton_Edit;
				this.m_UI.OnDecideNameChange += this.OnDecideNameChange;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Quest);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015BE RID: 5566 RVA: 0x00071394 File Offset: 0x0006F794
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.m_UI.SelectButton = PartyEditUIBase.eButton.Back;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (globalParam.IsDirtyPartyEdit)
			{
				this.m_Owner.Request_BattlePartySetAll(new Action(this.GoToMenuEnd));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060015BF RID: 5567 RVA: 0x000713EC File Offset: 0x0006F7EC
		private void OnClickButton_Edit()
		{
			if (this.m_UI.SelectButton == PartyEditUIBase.eButton.Decide)
			{
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				if (globalParam.IsDirtyPartyEdit)
				{
					this.m_Owner.Request_BattlePartySetAll(new Action(this.GoToMenuEnd));
				}
				else
				{
					this.GoToMenuEnd();
				}
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060015C0 RID: 5568 RVA: 0x00071450 File Offset: 0x0006F850
		private void OnClickButton_Confirm()
		{
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			QuestManager.SelectQuest selectQuest = questMng.GetSelectQuest();
			QuestManager.QuestData questData = questMng.GetQuestData(selectQuest.m_QuestID);
			if (questData == null)
			{
				return;
			}
			QuestManager.EventData eventData = null;
			if (questData.IsEventQuest())
			{
				eventData = questMng.GetEventData(questData.m_EventID);
			}
			long mngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[this.m_UI.SelectPartyIndex].MngID;
			switch (QuestUtility.CheckQuestLogAdd(questData, eventData, mngID))
			{
			case QuestDefine.eQuestLogAddResult.StaminaIsShort:
				SingletonMonoBehaviour<StaminaShopPlayer>.Inst.Open(1, null);
				break;
			case QuestDefine.eQuestLogAddResult.KeyItemIsShort:
			{
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				string name = dbMng.ItemListDB.GetParam(eventData.m_ExKeyItemID).m_Name;
				int itemNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(eventData.m_ExKeyItemID);
				int exKeyItemNum = eventData.m_ExKeyItemNum;
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, dbMng.GetTextMessage(eText_MessageDB.QuestConditionErrTitle), dbMng.GetTextMessage(eText_MessageDB.QuestConditionErrItemIsShort, new object[]
				{
					name,
					itemNum,
					exKeyItemNum
				}), null, null);
				break;
			}
			case QuestDefine.eQuestLogAddResult.AnyExConditionError:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.QuestConditionErrTitle, eText_MessageDB.QuestConditionErr, null);
				break;
			default:
				if (eventData != null && eventData.m_ExKeyItemID != -1)
				{
					DatabaseManager dbMng2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
					string name2 = dbMng2.ItemListDB.GetParam(eventData.m_ExKeyItemID).m_Name;
					int itemNum2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(eventData.m_ExKeyItemID);
					int exKeyItemNum2 = eventData.m_ExKeyItemNum;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, dbMng2.GetTextMessage(eText_MessageDB.QuestConditionTitle), dbMng2.GetTextMessage(eText_MessageDB.QuestCondition, new object[]
					{
						name2,
						itemNum2,
						itemNum2 - exKeyItemNum2
					}), null, new Action<int>(this.OnConfirmUseItem));
				}
				else
				{
					this.Request();
				}
				break;
			}
		}

		// Token: 0x060015C1 RID: 5569 RVA: 0x00071670 File Offset: 0x0006FA70
		private void OnConfirmUseItem(int btn)
		{
			if (btn == 0)
			{
				this.Request();
			}
		}

		// Token: 0x060015C2 RID: 5570 RVA: 0x00071680 File Offset: 0x0006FA80
		private void Request()
		{
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			QuestManager.SelectQuest selectQuest = questMng.GetSelectQuest();
			QuestManager.QuestData questData = questMng.GetQuestData(selectQuest.m_QuestID);
			if (questData == null)
			{
				return;
			}
			long mngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[this.m_UI.SelectPartyIndex].MngID;
			long friendCharaMngID = -1L;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (globalParam.QuestFriendData != null && globalParam.QuestUserSupportData != null)
			{
				friendCharaMngID = globalParam.QuestUserSupportData.MngID;
			}
			questMng.Request_QuestLogAdd(questData.IsEventQuest(), questData.m_Param.m_ID, questData.m_EventID, mngID, friendCharaMngID, new Action<QuestDefine.eReturnLogAdd, string>(this.OnResponse_QuestLogAdd));
		}

		// Token: 0x060015C3 RID: 5571 RVA: 0x0007173C File Offset: 0x0006FB3C
		private void OnResponse_QuestLogAdd(QuestDefine.eReturnLogAdd ret, string errorMessage)
		{
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			switch (ret)
			{
			case QuestDefine.eReturnLogAdd.Success:
				this.GoToMenuEnd();
				break;
			case QuestDefine.eReturnLogAdd.StaminaIsShort:
				APIUtility.ShowErrorWindowOk(errorMessage, null);
				break;
			case QuestDefine.eReturnLogAdd.QuestOutOfPerod:
			case QuestDefine.eReturnLogAdd.QuestOrderLimit:
				globalParam.ResetSelectionQuestParam();
				this.m_IsOutOfPeriod = true;
				APIUtility.ShowErrorWindowOk(errorMessage, new Action(this.OnConfirmOutOfPeriod));
				break;
			case QuestDefine.eReturnLogAdd.ItemIsShort:
				APIUtility.ShowErrorWindowOk(errorMessage, null);
				break;
			default:
				APIUtility.ShowErrorWindowOk(errorMessage, null);
				break;
			}
		}

		// Token: 0x060015C4 RID: 5572 RVA: 0x000717C6 File Offset: 0x0006FBC6
		private void OnConfirmOutOfPeriod()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060015C5 RID: 5573 RVA: 0x000717CE File Offset: 0x0006FBCE
		private void OnDecideNameChange(string partyName)
		{
			this.Request_BattlePartySetName(this.m_UI.SelectPartyIndex, partyName, new MeigewwwParam.Callback(this.OnResponse_BattlePartySetName));
		}

		// Token: 0x060015C6 RID: 5574 RVA: 0x000717EE File Offset: 0x0006FBEE
		private void GoToMenuEnd()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			}
			this.m_UI.PlayOut();
		}

		// Token: 0x060015C7 RID: 5575 RVA: 0x00071820 File Offset: 0x0006FC20
		public void Request_BattlePartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Battlepartysetname battlepartysetname = new PlayerRequestTypes.Battlepartysetname();
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			this.m_RequestPartyName = partyName;
			battlepartysetname.managedBattlePartyId = userBattlePartyData.MngID;
			battlepartysetname.name = partyName;
			MeigewwwParam wwwParam = PlayerRequest.Battlepartysetname(battlepartysetname, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060015C8 RID: 5576 RVA: 0x00071880 File Offset: 0x0006FC80
		private void OnResponse_BattlePartySetName(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Battlepartysetname battlepartysetname = PlayerResponse.Battlepartysetname(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetname == null)
			{
				return;
			}
			ResultCode result = battlepartysetname.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.NgWordError), result.ToMessageString(), null);
				}
			}
			else
			{
				UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[this.m_UI.SelectPartyIndex];
				userBattlePartyData.PartyName = this.m_RequestPartyName;
				this.m_UI.CloseNameChangeWindow();
			}
		}

		// Token: 0x060015C9 RID: 5577 RVA: 0x0007192C File Offset: 0x0006FD2C
		private void OnResponse_BattlePartySetAll(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Battlepartysetall battlepartysetall = PlayerResponse.Battlepartysetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetall == null)
			{
				return;
			}
			ResultCode result = battlepartysetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = false;
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060015CA RID: 5578 RVA: 0x00071988 File Offset: 0x0006FD88
		private void RequestQuestStartVoice()
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			UserBattlePartyData userBattlePartyData = inst.UserDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex];
			List<long> list = new List<long>();
			for (int i = 0; i < 3; i++)
			{
				long memberAt = userBattlePartyData.GetMemberAt(i);
				if (memberAt != -1L)
				{
					list.Add(memberAt);
				}
			}
			if (list.Count == 0)
			{
				return;
			}
			int index = UnityEngine.Random.Range(0, list.Count);
			UserCharacterData userCharaData = inst.UserDataMng.GetUserCharaData(list[index]);
			if (userCharaData != null)
			{
				inst.VoiceCtrl.Request(userCharaData.Param.NamedType, eSoundVoiceListDB.voice_400, false);
			}
		}

		// Token: 0x04001C79 RID: 7289
		private QuestState_QuestConfirm.eStep m_Step = QuestState_QuestConfirm.eStep.None;

		// Token: 0x04001C7A RID: 7290
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.PartyEditUI;

		// Token: 0x04001C7B RID: 7291
		public PartyEditUI m_UI;

		// Token: 0x04001C7C RID: 7292
		private string m_RequestPartyName;

		// Token: 0x04001C7D RID: 7293
		private bool m_IsOutOfPeriod;

		// Token: 0x02000462 RID: 1122
		private enum eStep
		{
			// Token: 0x04001C7F RID: 7295
			None = -1,
			// Token: 0x04001C80 RID: 7296
			First,
			// Token: 0x04001C81 RID: 7297
			LoadWait,
			// Token: 0x04001C82 RID: 7298
			PlayIn,
			// Token: 0x04001C83 RID: 7299
			Main,
			// Token: 0x04001C84 RID: 7300
			UnloadChildSceneWait
		}
	}
}
