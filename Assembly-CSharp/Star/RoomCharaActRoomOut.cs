﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005BA RID: 1466
	public class RoomCharaActRoomOut : IRoomCharaAct
	{
		// Token: 0x06001C64 RID: 7268 RVA: 0x00097F96 File Offset: 0x00096396
		public void RequestRoomOut(RoomObjectCtrlChara pbase, int factionno)
		{
			pbase.PlayMotion(RoomDefine.eAnim.Idle, WrapMode.Loop);
		}

		// Token: 0x06001C65 RID: 7269 RVA: 0x00097FA0 File Offset: 0x000963A0
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			if (!this.m_DeadUp)
			{
				pbase.CharaDelete();
			}
			this.m_DeadUp = true;
			return true;
		}

		// Token: 0x06001C66 RID: 7270 RVA: 0x00097FBB File Offset: 0x000963BB
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return false;
		}

		// Token: 0x04002342 RID: 9026
		private bool m_DeadUp;
	}
}
