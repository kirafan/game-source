﻿using System;

namespace Star
{
	// Token: 0x020002CC RID: 716
	public abstract class CharacterDataController
	{
		// Token: 0x06000DC9 RID: 3529 RVA: 0x0004D19D File Offset: 0x0004B59D
		public CharacterDataController(eCharacterDataControllerType in_type)
		{
			this.memberType = in_type;
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x0004D1AC File Offset: 0x0004B5AC
		public static bool IsExistCharacterStatic(CharacterDataController cdc)
		{
			return CharacterDataController.IsExistCharacterStatic((cdc == null) ? eCharacterDataType.Empty : cdc.GetCharacterDataType());
		}

		// Token: 0x06000DCB RID: 3531 RVA: 0x0004D1C5 File Offset: 0x0004B5C5
		public static bool IsExistCharacterStatic(eCharacterDataType characterDataType)
		{
			return characterDataType != eCharacterDataType.Empty && characterDataType != eCharacterDataType.Lock && characterDataType != eCharacterDataType.GachaDetailMessage;
		}

		// Token: 0x06000DCC RID: 3532 RVA: 0x0004D1E0 File Offset: 0x0004B5E0
		public eCharacterDataControllerType GetCharacterDataControllerType()
		{
			return this.memberType;
		}

		// Token: 0x06000DCD RID: 3533 RVA: 0x0004D1E8 File Offset: 0x0004B5E8
		public eCharacterDataType GetCharacterDataType()
		{
			return this.GetDataBaseInternal().GetCharacterDataType();
		}

		// Token: 0x06000DCE RID: 3534 RVA: 0x0004D1F5 File Offset: 0x0004B5F5
		public bool IsExistCharacter()
		{
			return CharacterDataController.IsExistCharacterStatic(this.GetCharacterDataType());
		}

		// Token: 0x06000DCF RID: 3535 RVA: 0x0004D202 File Offset: 0x0004B602
		public CharacterDataWrapperBase GetData()
		{
			return this.GetDataBaseInternal();
		}

		// Token: 0x06000DD0 RID: 3536
		protected abstract CharacterDataWrapperBase GetDataBaseInternal();

		// Token: 0x06000DD1 RID: 3537 RVA: 0x0004D20A File Offset: 0x0004B60A
		public long GetCharaMngId()
		{
			return this.GetDataBaseInternal().GetMngId();
		}

		// Token: 0x06000DD2 RID: 3538 RVA: 0x0004D217 File Offset: 0x0004B617
		public int GetCharaId()
		{
			return this.GetDataBaseInternal().GetCharaId();
		}

		// Token: 0x06000DD3 RID: 3539 RVA: 0x0004D224 File Offset: 0x0004B624
		public eTitleType GetCharaTitleType()
		{
			return this.GetDataBaseInternal().GetCharaTitleType();
		}

		// Token: 0x06000DD4 RID: 3540 RVA: 0x0004D231 File Offset: 0x0004B631
		public eCharaNamedType GetCharaNamedType()
		{
			return this.GetDataBaseInternal().GetCharaNamedType();
		}

		// Token: 0x06000DD5 RID: 3541 RVA: 0x0004D23E File Offset: 0x0004B63E
		public eClassType GetClassType()
		{
			return this.GetDataBaseInternal().GetClassType();
		}

		// Token: 0x06000DD6 RID: 3542 RVA: 0x0004D24B File Offset: 0x0004B64B
		public eElementType GetCharaElementType()
		{
			return this.GetDataBaseInternal().GetElementType();
		}

		// Token: 0x06000DD7 RID: 3543 RVA: 0x0004D258 File Offset: 0x0004B658
		public eRare GetRarity()
		{
			return this.GetDataBaseInternal().GetRarity();
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x0004D265 File Offset: 0x0004B665
		public int GetCharaCost()
		{
			return this.GetDataBaseInternal().GetCost();
		}

		// Token: 0x06000DD9 RID: 3545 RVA: 0x0004D272 File Offset: 0x0004B672
		public int GetCharaLv()
		{
			return this.GetDataBaseInternal().GetLv();
		}

		// Token: 0x06000DDA RID: 3546 RVA: 0x0004D27F File Offset: 0x0004B67F
		public int GetCharaMaxLv()
		{
			return this.GetDataBaseInternal().GetMaxLv();
		}

		// Token: 0x06000DDB RID: 3547 RVA: 0x0004D28C File Offset: 0x0004B68C
		public int GetCharaLimitBreak()
		{
			return this.GetDataBaseInternal().GetLimitBreak();
		}

		// Token: 0x06000DDC RID: 3548 RVA: 0x0004D299 File Offset: 0x0004B699
		public long GetCharaExp()
		{
			return this.GetDataBaseInternal().GetExp();
		}

		// Token: 0x06000DDD RID: 3549 RVA: 0x0004D2A6 File Offset: 0x0004B6A6
		public virtual long GetCharaNowExp()
		{
			return this.GetDataBaseInternal().GetNowExp();
		}

		// Token: 0x06000DDE RID: 3550 RVA: 0x0004D2B3 File Offset: 0x0004B6B3
		public virtual long GetCharaNowMaxExp()
		{
			return this.GetDataBaseInternal().GetNowMaxExp();
		}

		// Token: 0x06000DDF RID: 3551 RVA: 0x0004D2C0 File Offset: 0x0004B6C0
		public int GetFriendship()
		{
			return this.GetDataBaseInternal().GetFriendship();
		}

		// Token: 0x06000DE0 RID: 3552 RVA: 0x0004D2CD File Offset: 0x0004B6CD
		public int GetFriendshipMax()
		{
			return this.GetDataBaseInternal().GetFriendshipMax();
		}

		// Token: 0x06000DE1 RID: 3553 RVA: 0x0004D2DA File Offset: 0x0004B6DA
		public long GetFriendshipExp()
		{
			return this.GetDataBaseInternal().GetFriendshipExp();
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x0004D2E7 File Offset: 0x0004B6E7
		public SkillLearnData GetUniqueSkillLearnData()
		{
			if (this.GetDataBaseInternal() == null)
			{
				return null;
			}
			return this.GetDataBaseInternal().GetUniqueSkillLearnData();
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x0004D301 File Offset: 0x0004B701
		public int GetUniqueSkillLv()
		{
			if (this.GetDataBaseInternal() == null)
			{
				return 1;
			}
			return this.GetDataBaseInternal().GetUniqueSkillLv();
		}

		// Token: 0x06000DE4 RID: 3556 RVA: 0x0004D31B File Offset: 0x0004B71B
		public int GetUniqueSkillMaxLv()
		{
			if (this.GetDataBaseInternal() == null)
			{
				return 1;
			}
			return this.GetDataBaseInternal().GetUniqueSkillMaxLv();
		}

		// Token: 0x06000DE5 RID: 3557 RVA: 0x0004D335 File Offset: 0x0004B735
		public int GetClassSkillsCount()
		{
			if (this.GetDataBaseInternal() == null)
			{
				return 0;
			}
			return this.GetDataBaseInternal().GetClassSkillsCount();
		}

		// Token: 0x06000DE6 RID: 3558 RVA: 0x0004D34F File Offset: 0x0004B74F
		public SkillLearnData GetClassSkillLearnData(int index)
		{
			if (this.GetDataBaseInternal() == null)
			{
				return null;
			}
			return this.GetDataBaseInternal().GetClassSkillLearnData(index);
		}

		// Token: 0x06000DE7 RID: 3559 RVA: 0x0004D36A File Offset: 0x0004B76A
		public int[] GetClassSkillLevels()
		{
			if (this.GetDataBaseInternal() == null)
			{
				return new int[0];
			}
			return this.GetDataBaseInternal().GetClassSkillLevels();
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x0004D389 File Offset: 0x0004B789
		public int GetClassSkillLevel(int index)
		{
			if (this.GetDataBaseInternal() == null)
			{
				return 1;
			}
			return this.GetDataBaseInternal().GetClassSkillLevel(index);
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x0004D3A4 File Offset: 0x0004B7A4
		public virtual int GetCost()
		{
			return this.GetCharaCost();
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x0004D3AC File Offset: 0x0004B7AC
		public EditUtility.OutputCharaParam GetCharaParamLvOnly()
		{
			return this.GetDataBaseInternal().GetCharaParamLvOnly();
		}

		// Token: 0x04001591 RID: 5521
		protected eCharacterDataControllerType memberType;
	}
}
