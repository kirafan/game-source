﻿using System;
using System.Collections.Generic;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x020005A7 RID: 1447
	public class RoomComSetUp : IFldNetComModule
	{
		// Token: 0x06001BEC RID: 7148 RVA: 0x00093CC7 File Offset: 0x000920C7
		public RoomComSetUp() : base(IFldNetComManager.Instance)
		{
			this.m_DefaultStep = new List<RoomComSetUp.eStep>();
		}

		// Token: 0x06001BED RID: 7149 RVA: 0x00093CE0 File Offset: 0x000920E0
		public bool SetUp()
		{
			RoomComAPIGetAll roomComAPIGetAll = new RoomComAPIGetAll();
			roomComAPIGetAll.playerId = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
			this.m_NetMng.SendCom(roomComAPIGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06001BEE RID: 7150 RVA: 0x00093D28 File Offset: 0x00092128
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				int fretroom = WWWComUtil.SetRoomPlacementKey(getAll.managedRooms, true);
				this.CheckDefaultDataUp(fretroom);
				if (this.m_DefaultStep.Count == 0)
				{
					this.m_Step = RoomComSetUp.eStep.End;
				}
				else
				{
					this.m_Step = RoomComSetUp.eStep.DefStepChk;
				}
			}
		}

		// Token: 0x06001BEF RID: 7151 RVA: 0x00093D80 File Offset: 0x00092180
		public bool DefaultRoomPlace()
		{
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(-1L);
			RoomComAPINewSet roomComAPINewSet = new RoomComAPINewSet();
			roomComAPINewSet.groupId = 0;
			roomComAPINewSet.floorId = manageIDToUserRoomData.FloorID;
			roomComAPINewSet.arrangeData = UserRoomUtil.CreateRoomDataString(manageIDToUserRoomData);
			this.m_NetMng.SendCom(roomComAPINewSet, new INetComHandle.ResponseCallbak(this.CallbackDefaultRoomPlace), false);
			return true;
		}

		// Token: 0x06001BF0 RID: 7152 RVA: 0x00093DD4 File Offset: 0x000921D4
		private void CallbackDefaultRoomPlace(INetComHandle phandle)
		{
			Set set = phandle.GetResponse() as Set;
			if (set != null)
			{
				UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(-1L);
				manageIDToUserRoomData.MngID = set.managedRoomId;
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, set.managedRoomId);
				this.m_Step = RoomComSetUp.eStep.DefStepChk;
			}
		}

		// Token: 0x06001BF1 RID: 7153 RVA: 0x00093E3C File Offset: 0x0009223C
		public bool DefaultRoomActive()
		{
			RoomComAPISetActiveRoom roomComAPISetActiveRoom = new RoomComAPISetActiveRoom();
			roomComAPISetActiveRoom.managedRoomId = UserRoomUtil.GetActiveRoomMngID(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
			this.m_NetMng.SendCom(roomComAPISetActiveRoom, new INetComHandle.ResponseCallbak(this.CallbackDefaultRoomList), false);
			return true;
		}

		// Token: 0x06001BF2 RID: 7154 RVA: 0x00093E88 File Offset: 0x00092288
		private void CallbackDefaultRoomList(INetComHandle phandle)
		{
			this.m_Step = RoomComSetUp.eStep.DefStepChk;
		}

		// Token: 0x06001BF3 RID: 7155 RVA: 0x00093E94 File Offset: 0x00092294
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case RoomComSetUp.eStep.GetState:
				this.SetUp();
				this.m_Step = RoomComSetUp.eStep.WaitCheck;
				break;
			case RoomComSetUp.eStep.DefaultRoomPlace:
				this.DefaultRoomPlace();
				this.m_Step = RoomComSetUp.eStep.SetUpCheck;
				break;
			case RoomComSetUp.eStep.DefaultRoomActive:
				this.DefaultRoomActive();
				this.m_Step = RoomComSetUp.eStep.SetUpCheck;
				break;
			case RoomComSetUp.eStep.DefStepChk:
				if (this.m_DefaultStep.Count == 0)
				{
					this.m_Step = RoomComSetUp.eStep.End;
				}
				else
				{
					this.m_Step = this.m_DefaultStep[0];
					this.m_DefaultStep.RemoveAt(0);
				}
				break;
			case RoomComSetUp.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x06001BF4 RID: 7156 RVA: 0x00093F5C File Offset: 0x0009235C
		public static UserRoomData SetUpDefaultRoomKey(DefaultObjectListDB pdb, eDefCheckUpType fcheck)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserRoomData userRoomData = new UserRoomData();
			userRoomData.MngID = -1L;
			userRoomData.FloorID = 1;
			for (int i = 0; i < pdb.m_Params.Length; i++)
			{
				if (pdb.m_Params[i].m_Category == 1 && pdb.m_Params[i].m_WakeUp == (int)fcheck)
				{
					switch (pdb.m_Params[i].m_Type)
					{
					case 1:
						userRoomData.FloorID = pdb.m_Params[i].m_ObjEventArgs[0] + 1;
						break;
					case 2:
					{
						UserRoomObjectData userRoomObjData = userDataMng.GetUserRoomObjData(eRoomObjectCategory.Background, pdb.m_Params[i].m_ObjectNo);
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(0, 0, CharacterDefine.eDir.L, userRoomObjData.GetFreeLinkMngID(true), -1, userRoomObjData.ResourceID);
						userRoomData.AddPlacement(placementData);
						break;
					}
					case 3:
					{
						UserRoomObjectData userRoomObjData = userDataMng.GetUserRoomObjData(eRoomObjectCategory.Wall, pdb.m_Params[i].m_ObjectNo);
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(0, 0, CharacterDefine.eDir.L, userRoomObjData.GetFreeLinkMngID(true), -1, userRoomObjData.ResourceID);
						userRoomData.AddPlacement(placementData);
						break;
					}
					case 4:
					{
						UserRoomObjectData userRoomObjData = userDataMng.GetUserRoomObjData(eRoomObjectCategory.Floor, pdb.m_Params[i].m_ObjectNo);
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(0, 0, CharacterDefine.eDir.L, userRoomObjData.GetFreeLinkMngID(true), -1, userRoomObjData.ResourceID);
						userRoomData.AddPlacement(placementData);
						break;
					}
					case 5:
					{
						UserRoomObjectData userRoomObjData = userDataMng.GetUserRoomObjData(eRoomObjectCategory.Bedding, pdb.m_Params[i].m_ObjectNo);
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(pdb.m_Params[i].m_ObjEventArgs[1], pdb.m_Params[i].m_ObjEventArgs[2], CharacterDefine.eDir.R, userRoomObjData.GetFreeLinkMngID(true), 1, userRoomObjData.ResourceID);
						userRoomData.AddPlacement(placementData);
						break;
					}
					}
				}
			}
			return userRoomData;
		}

		// Token: 0x06001BF5 RID: 7157 RVA: 0x00094148 File Offset: 0x00092548
		private void CheckDefaultDataUp(int fretroom)
		{
			if (fretroom != 0)
			{
				DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
				if ((fretroom & 1) != 0)
				{
					UserRoomData proomdata = RoomComSetUp.SetUpDefaultRoomKey(db, eDefCheckUpType.Non);
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, 0, proomdata);
					this.m_DefaultStep.Add(RoomComSetUp.eStep.DefaultRoomPlace);
					this.m_DefaultStep.Add(RoomComSetUp.eStep.DefaultRoomActive);
				}
				else
				{
					UserRoomData proomdata = RoomComSetUp.SetUpDefaultRoomKey(db, eDefCheckUpType.Non);
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, 0, proomdata);
					this.m_DefaultStep.Add(RoomComSetUp.eStep.DefaultRoomActive);
				}
			}
		}

		// Token: 0x06001BF6 RID: 7158 RVA: 0x000941F8 File Offset: 0x000925F8
		public static bool CheckDefaultDataUp(eDefCheckUpType fcheck)
		{
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			bool result = false;
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 1 && db.m_Params[i].m_WakeUp == (int)fcheck && db.m_Params[i].m_Type == 1)
				{
					UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
					UserRoomListData.PlayerFloorState playerRoomList = userDataMng.UserRoomListData.GetPlayerRoomList(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
					result = playerRoomList.SearchFloorData(db.m_Params[i].m_ObjEventArgs[0] + 1);
					break;
				}
			}
			return result;
		}

		// Token: 0x040022E9 RID: 8937
		private RoomComSetUp.eStep m_Step;

		// Token: 0x040022EA RID: 8938
		private List<RoomComSetUp.eStep> m_DefaultStep;

		// Token: 0x040022EB RID: 8939
		private string m_DefaultRoomPlacement;

		// Token: 0x040022EC RID: 8940
		private long m_DefSetUpManageId;

		// Token: 0x020005A8 RID: 1448
		public enum eStep
		{
			// Token: 0x040022EE RID: 8942
			GetState,
			// Token: 0x040022EF RID: 8943
			WaitCheck,
			// Token: 0x040022F0 RID: 8944
			DefaultRoomPlace,
			// Token: 0x040022F1 RID: 8945
			DefaultRoomActive,
			// Token: 0x040022F2 RID: 8946
			SetUpCheck,
			// Token: 0x040022F3 RID: 8947
			DefStepChk,
			// Token: 0x040022F4 RID: 8948
			End
		}
	}
}
