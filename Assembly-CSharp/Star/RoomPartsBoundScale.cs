﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061D RID: 1565
	public class RoomPartsBoundScale : IRoomPartsAction
	{
		// Token: 0x06001E8E RID: 7822 RVA: 0x000A5601 File Offset: 0x000A3A01
		public RoomPartsBoundScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
			this.m_Active = true;
			this.m_Marker = pself;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Target = ftarget;
			this.m_Base = fbase;
		}

		// Token: 0x06001E8F RID: 7823 RVA: 0x000A5638 File Offset: 0x000A3A38
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Marker.localScale = this.m_Base;
				this.m_Active = false;
			}
			if (this.m_Active)
			{
				float num = this.m_Time / this.m_MaxTime;
				this.m_Marker.localScale = (this.m_Target - this.m_Base) * Mathf.Sin(num * 3.1415927f) + this.m_Base;
			}
			return this.m_Active;
		}

		// Token: 0x04002534 RID: 9524
		private Transform m_Marker;

		// Token: 0x04002535 RID: 9525
		private Vector3 m_Target;

		// Token: 0x04002536 RID: 9526
		private Vector3 m_Base;

		// Token: 0x04002537 RID: 9527
		private float m_Time;

		// Token: 0x04002538 RID: 9528
		private float m_MaxTime;
	}
}
