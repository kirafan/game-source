﻿using System;

namespace Star
{
	// Token: 0x020004E5 RID: 1253
	public class SimpleTimer
	{
		// Token: 0x060018FD RID: 6397 RVA: 0x00082143 File Offset: 0x00080543
		public SimpleTimer(float sec)
		{
			this.SetTime(sec);
		}

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x060018FE RID: 6398 RVA: 0x00082159 File Offset: 0x00080559
		public float Sec
		{
			get
			{
				return this.m_Time;
			}
		}

		// Token: 0x060018FF RID: 6399 RVA: 0x00082161 File Offset: 0x00080561
		public void SetTimeAndPlay(float sec)
		{
			this.m_Time = sec;
			this.m_IsStop = false;
		}

		// Token: 0x06001900 RID: 6400 RVA: 0x00082171 File Offset: 0x00080571
		public void SetTime(float sec)
		{
			this.m_Time = sec;
		}

		// Token: 0x06001901 RID: 6401 RVA: 0x0008217A File Offset: 0x0008057A
		public void Start()
		{
			this.m_IsStop = false;
		}

		// Token: 0x06001902 RID: 6402 RVA: 0x00082183 File Offset: 0x00080583
		public void Stop()
		{
			this.m_IsStop = true;
		}

		// Token: 0x06001903 RID: 6403 RVA: 0x0008218C File Offset: 0x0008058C
		public void Update(float deltaTime)
		{
			if (this.m_IsStop)
			{
				return;
			}
			this.m_Time -= deltaTime;
			if (this.m_Time < 0f)
			{
				this.m_Time = 0f;
			}
		}

		// Token: 0x04001F7E RID: 8062
		private float m_Time;

		// Token: 0x04001F7F RID: 8063
		private bool m_IsStop = true;
	}
}
