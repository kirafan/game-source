﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000DA RID: 218
	public class BattlePartyData
	{
		// Token: 0x060005D2 RID: 1490 RVA: 0x0001DB08 File Offset: 0x0001BF08
		public BattlePartyData(BattleDefine.ePartyType partyType, int[] joinMemberIndices)
		{
			this.PartyType = partyType;
			this.SingleTargetIndex = BattleDefine.eJoinMember.Join_1;
			this.PartyMngID = -1L;
			this.m_Members = new CharacterHandler[6];
			for (int i = 0; i < this.m_Members.Length; i++)
			{
				this.m_Members[i] = null;
			}
			this.m_JoinMemberIndices = joinMemberIndices;
			if (this.m_JoinMemberIndices == null || this.m_JoinMemberIndices.Length == 0)
			{
				this.m_JoinMemberIndices = new int[6];
				for (int j = 0; j < this.m_JoinMemberIndices.Length; j++)
				{
					this.m_JoinMemberIndices[j] = j;
				}
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060005D3 RID: 1491 RVA: 0x0001DBB8 File Offset: 0x0001BFB8
		// (set) Token: 0x060005D4 RID: 1492 RVA: 0x0001DBC0 File Offset: 0x0001BFC0
		public BattleDefine.ePartyType PartyType { get; private set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060005D5 RID: 1493 RVA: 0x0001DBC9 File Offset: 0x0001BFC9
		// (set) Token: 0x060005D6 RID: 1494 RVA: 0x0001DBD1 File Offset: 0x0001BFD1
		public BattleDefine.eJoinMember SingleTargetIndex { get; set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060005D7 RID: 1495 RVA: 0x0001DBDA File Offset: 0x0001BFDA
		// (set) Token: 0x060005D8 RID: 1496 RVA: 0x0001DBE2 File Offset: 0x0001BFE2
		public long PartyMngID { get; set; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060005D9 RID: 1497 RVA: 0x0001DBEB File Offset: 0x0001BFEB
		public bool IsPlayerParty
		{
			get
			{
				return this.PartyType == BattleDefine.ePartyType.Player;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060005DA RID: 1498 RVA: 0x0001DBF6 File Offset: 0x0001BFF6
		public bool IsEnemyParty
		{
			get
			{
				return this.PartyType == BattleDefine.ePartyType.Enemy;
			}
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x0001DC04 File Offset: 0x0001C004
		public void Destroy()
		{
			this.PartyType = BattleDefine.ePartyType.None;
			this.SingleTargetIndex = BattleDefine.eJoinMember.Join_1;
			for (int i = 0; i < this.m_Members.Length; i++)
			{
				if (this.m_Members[i] != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_Members[i]);
					this.m_Members[i] = null;
				}
			}
			this.m_Members = null;
			this.m_JoinMemberIndices = new int[6];
			for (int j = 0; j < this.m_JoinMemberIndices.Length; j++)
			{
				this.m_JoinMemberIndices[j] = -1;
			}
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x0001DCA0 File Offset: 0x0001C0A0
		public void SetMemberAt(int slotIndex, CharacterHandler charaHndl)
		{
			this.m_Members[slotIndex] = charaHndl;
		}

		// Token: 0x060005DD RID: 1501 RVA: 0x0001DCAC File Offset: 0x0001C0AC
		public void SetJoinMemberIndex(BattleDefine.eJoinMember join, int slotIndex)
		{
			this.m_JoinMemberIndices[(int)join] = slotIndex;
			CharacterHandler member = this.GetMember(join);
			if (member != null)
			{
				member.CharaBattle.Join = join;
			}
		}

		// Token: 0x060005DE RID: 1502 RVA: 0x0001DCE4 File Offset: 0x0001C0E4
		public BattleDefine.eJoinMember SlotIndexToJoin(int slotIndex)
		{
			for (int i = 0; i < this.m_JoinMemberIndices.Length; i++)
			{
				if (this.m_JoinMemberIndices[i] == slotIndex)
				{
					return (BattleDefine.eJoinMember)i;
				}
			}
			return BattleDefine.eJoinMember.None;
		}

		// Token: 0x060005DF RID: 1503 RVA: 0x0001DD1C File Offset: 0x0001C11C
		public CharacterHandler GetMember(BattleDefine.eJoinMember join)
		{
			int num = this.m_JoinMemberIndices[(int)join];
			if (num >= 0 && num < this.m_Members.Length)
			{
				return this.m_Members[num];
			}
			return null;
		}

		// Token: 0x060005E0 RID: 1504 RVA: 0x0001DD51 File Offset: 0x0001C151
		public CharacterHandler GetMemberAt(int slotIndex)
		{
			return this.m_Members[slotIndex];
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x0001DD5C File Offset: 0x0001C15C
		public List<CharacterHandler> GetAvailableMembers()
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			if (this.m_Members != null)
			{
				for (int i = 0; i < this.m_Members.Length; i++)
				{
					if (this.m_Members[i] != null)
					{
						list.Add(this.m_Members[i]);
					}
				}
			}
			return list;
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x0001DDB8 File Offset: 0x0001C1B8
		public List<int> GetAvailableCharaIDs()
		{
			List<int> list = new List<int>();
			if (this.m_Members != null)
			{
				for (int i = 0; i < this.m_Members.Length; i++)
				{
					if (this.m_Members[i] != null)
					{
						list.Add(this.m_Members[i].CharaParam.CharaID);
					}
				}
			}
			return list;
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x0001DE1C File Offset: 0x0001C21C
		public List<CharacterHandler> GetMembers(List<BattleDefine.eJoinMember> joins)
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			for (int i = 0; i < joins.Count; i++)
			{
				CharacterHandler member = this.GetMember(joins[i]);
				if (member != null)
				{
					list.Add(member);
				}
			}
			return list;
		}

		// Token: 0x060005E4 RID: 1508 RVA: 0x0001DE68 File Offset: 0x0001C268
		public int GetJoinCharaNum(bool isAliveOnly = false)
		{
			int num = 0;
			for (int i = 0; i < 3; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!isAliveOnly || !member.CharaBattle.Param.IsDead())
					{
						num++;
					}
				}
			}
			return num;
		}

		// Token: 0x060005E5 RID: 1509 RVA: 0x0001DEC4 File Offset: 0x0001C2C4
		public int GetStateAbnormalCountAny(bool isJoinOnly = false)
		{
			int num = 3;
			if (!isJoinOnly)
			{
				num = 6;
			}
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						if (member.CharaBattle.Param.IsEnableStateAbnormalAny())
						{
							num2++;
						}
					}
				}
			}
			return num2;
		}

		// Token: 0x060005E6 RID: 1510 RVA: 0x0001DF38 File Offset: 0x0001C338
		public int GetStateAbnormalCountNone(bool isJoinOnly = false)
		{
			int num = 3;
			if (!isJoinOnly)
			{
				num = 6;
			}
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						if (!member.CharaBattle.Param.IsEnableStateAbnormalAny())
						{
							num2++;
						}
					}
				}
			}
			return num2;
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x0001DFAC File Offset: 0x0001C3AC
		public int GetStateAbnormalCountSpecific(eStateAbnormal stateAbnormal, bool isJoinOnly = false)
		{
			int num = 3;
			if (!isJoinOnly)
			{
				num = 6;
			}
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						if (member.CharaBattle.Param.IsEnableStateAbnormal(stateAbnormal))
						{
							num2++;
						}
					}
				}
			}
			return num2;
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001E020 File Offset: 0x0001C420
		public int GetStateAbnormalCountSpecificNone(eStateAbnormal stateAbnormal, bool isJoinOnly = false)
		{
			int num = 3;
			if (!isJoinOnly)
			{
				num = 6;
			}
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!member.CharaBattle.Param.IsDead())
					{
						if (!member.CharaBattle.Param.IsEnableStateAbnormal(stateAbnormal))
						{
							num2++;
						}
					}
				}
			}
			return num2;
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001E094 File Offset: 0x0001C494
		public List<CharacterHandler> GetJoinMembers(bool isAliveOnly = false)
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			for (int i = 0; i < 3; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					if (!isAliveOnly || !member.CharaBattle.Param.IsDead())
					{
						list.Add(member);
					}
				}
			}
			return list;
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0001E0F8 File Offset: 0x0001C4F8
		public List<CharacterHandler> GetBenchMembers(bool isAliveOnly = false)
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			for (int i = 0; i < 2; i++)
			{
				CharacterHandler member = this.GetMember(BattleDefine.eJoinMember.Bench_1 + i);
				if (member != null)
				{
					if (!isAliveOnly || !member.CharaBattle.Param.IsDead())
					{
						list.Add(member);
					}
				}
			}
			return list;
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x0001E15C File Offset: 0x0001C55C
		public bool IsExistCharaID(int charaID, bool isIncludeFriend = true)
		{
			int num = 5;
			if (isIncludeFriend)
			{
				num = 6;
			}
			for (int i = 0; i < num; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null && member.CharaParam.CharaID == charaID)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0001E1AD File Offset: 0x0001C5AD
		public bool IsExistFriend()
		{
			return this.m_Members[5] != null;
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x0001E1C5 File Offset: 0x0001C5C5
		public CharacterHandler GetFriend()
		{
			return this.m_Members[5];
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x0001E1D0 File Offset: 0x0001C5D0
		public CharacterHandler FindMemberByName(string gameObjectName)
		{
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null && member.gameObject.name == gameObjectName)
				{
					return member;
				}
			}
			return null;
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0001E21C File Offset: 0x0001C61C
		public bool CanChangeJoinMember()
		{
			int num = 0;
			for (int i = 0; i < 2; i++)
			{
				CharacterHandler member = this.GetMember(BattleDefine.eJoinMember.Bench_1 + i);
				if (member != null && !member.CharaBattle.Param.IsDead())
				{
					num++;
				}
			}
			return num > 0;
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x0001E278 File Offset: 0x0001C678
		public bool ChangeJoinMember(BattleDefine.eJoinMember join, BattleDefine.eJoinMember bench)
		{
			if (join < BattleDefine.eJoinMember.Join_1 || join > BattleDefine.eJoinMember.Join_3)
			{
				return false;
			}
			if (bench < BattleDefine.eJoinMember.Bench_1 || bench > BattleDefine.eJoinMember.Bench_Friend)
			{
				return false;
			}
			int num = this.m_JoinMemberIndices[(int)join];
			this.m_JoinMemberIndices[(int)join] = this.m_JoinMemberIndices[(int)bench];
			this.m_JoinMemberIndices[(int)bench] = num;
			CharacterHandler member = this.GetMember(join);
			if (member != null)
			{
				member.CharaBattle.Join = join;
			}
			member = this.GetMember(bench);
			if (member != null)
			{
				member.CharaBattle.Join = bench;
			}
			return true;
		}

		// Token: 0x060005F1 RID: 1521 RVA: 0x0001E308 File Offset: 0x0001C708
		public void WavePreProcess(int waveIndex)
		{
			if (this.m_Members != null)
			{
				for (int i = 0; i < this.m_Members.Length; i++)
				{
					if (this.m_Members[i] != null)
					{
						this.m_Members[i].CharaBattle.WavePreProcess(waveIndex);
					}
				}
			}
			this.ResetTogetherAttackChainCoefChange();
			this.ResetAllActionCountDatas();
		}

		// Token: 0x060005F2 RID: 1522 RVA: 0x0001E36C File Offset: 0x0001C76C
		public bool CheckDeadAll()
		{
			if (this.m_Members != null)
			{
				for (int i = 0; i < 3; i++)
				{
					CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
					if (!(member == null))
					{
						if (member.IsFriendChara && !member.CharaBattle.Param.IsDead())
						{
							return false;
						}
					}
				}
				for (int j = 0; j < this.m_Members.Length; j++)
				{
					CharacterHandler characterHandler = this.m_Members[j];
					if (!(characterHandler == null))
					{
						if (!characterHandler.IsFriendChara)
						{
							if (!characterHandler.CharaBattle.Param.IsDead())
							{
								return false;
							}
						}
					}
				}
			}
			return true;
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0001E42C File Offset: 0x0001C82C
		public void TurnStartProcess(CharacterHandler turnOwner)
		{
			for (int i = 0; i < 3; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					member.CharaBattle.TurnStartProcess(turnOwner);
				}
			}
			for (int j = 0; j < 2; j++)
			{
				CharacterHandler member2 = this.GetMember(BattleDefine.eJoinMember.Bench_1 + j);
				if (member2 != null)
				{
					member2.CharaBattle.Param.MemberChangeRecastDecrement();
				}
			}
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x0001E4A4 File Offset: 0x0001C8A4
		public void TurnEndProcess(CharacterHandler turnOwner)
		{
			for (int i = 0; i < 3; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					member.CharaBattle.TurnEndProcess(turnOwner);
				}
			}
			this.ResetRequestedActionCounts();
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x0001E4EC File Offset: 0x0001C8EC
		public void ResetOnRevival()
		{
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler member = this.GetMember((BattleDefine.eJoinMember)i);
				if (member != null)
				{
					member.CharaBattle.ResetOnRevival();
				}
			}
			this.ResetTogetherAttackChainCoefChange();
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x0001E530 File Offset: 0x0001C930
		public void SetTogetherAttackChainCoefChange(float togetherAttackChainCoefChange)
		{
			if (togetherAttackChainCoefChange > this.m_TogetherAttackChainCoefChange)
			{
				this.m_TogetherAttackChainCoefChange = togetherAttackChainCoefChange;
			}
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x0001E545 File Offset: 0x0001C945
		public float GetTogetherAttackChainCoefChange()
		{
			return this.m_TogetherAttackChainCoefChange;
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x0001E54D File Offset: 0x0001C94D
		public void ResetTogetherAttackChainCoefChange()
		{
			this.m_TogetherAttackChainCoefChange = 0f;
		}

		// Token: 0x060005F9 RID: 1529 RVA: 0x0001E55A File Offset: 0x0001C95A
		public int GetActionCount(BattlePartyData.eActionCountType actionCountType)
		{
			return this.m_ActionCountDatas[(int)actionCountType].m_Count;
		}

		// Token: 0x060005FA RID: 1530 RVA: 0x0001E56D File Offset: 0x0001C96D
		public void IncrementActionCount(BattlePartyData.eActionCountType actionCountType)
		{
			BattlePartyData.ActionCountData[] actionCountDatas = this.m_ActionCountDatas;
			actionCountDatas[(int)actionCountType].m_Count = actionCountDatas[(int)actionCountType].m_Count + 1;
		}

		// Token: 0x060005FB RID: 1531 RVA: 0x0001E588 File Offset: 0x0001C988
		public void RequestResetActionCount(BattlePartyData.eActionCountType actionCountType)
		{
			this.m_ActionCountDatas[(int)actionCountType].m_RequestResetFlg = true;
		}

		// Token: 0x060005FC RID: 1532 RVA: 0x0001E59C File Offset: 0x0001C99C
		private void ResetRequestedActionCounts()
		{
			for (int i = 0; i < 4; i++)
			{
				if (this.m_ActionCountDatas[i].m_RequestResetFlg)
				{
					this.m_ActionCountDatas[i].m_Count = 0;
					this.m_ActionCountDatas[i].m_RequestResetFlg = false;
				}
			}
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x0001E5F8 File Offset: 0x0001C9F8
		public void ResetAllActionCountDatas()
		{
			for (int i = 0; i < 4; i++)
			{
				this.m_ActionCountDatas[i].m_Count = 0;
				this.m_ActionCountDatas[i].m_RequestResetFlg = false;
			}
		}

		// Token: 0x060005FE RID: 1534 RVA: 0x0001E63C File Offset: 0x0001CA3C
		public bool PlayVoice(string cueName)
		{
			List<CharacterHandler> joinMembers = this.GetJoinMembers(true);
			if (joinMembers != null && joinMembers.Count > 0)
			{
				List<int> list = new List<int>();
				for (int i = 0; i < joinMembers.Count; i++)
				{
					if (joinMembers[i].CharaResource.IsAvailableVoice)
					{
						list.Add(i);
					}
				}
				if (list.Count > 0)
				{
					int index = list[UnityEngine.Random.Range(0, list.Count)];
					joinMembers[index].PlayVoice(cueName, 1f);
					return true;
				}
			}
			return false;
		}

		// Token: 0x060005FF RID: 1535 RVA: 0x0001E6D4 File Offset: 0x0001CAD4
		public bool PlayVoice(eSoundVoiceListDB cueID)
		{
			List<CharacterHandler> joinMembers = this.GetJoinMembers(true);
			if (joinMembers != null && joinMembers.Count > 0)
			{
				List<int> list = new List<int>();
				for (int i = 0; i < joinMembers.Count; i++)
				{
					if (joinMembers[i].CharaResource.IsAvailableVoice)
					{
						list.Add(i);
					}
				}
				if (list.Count > 0)
				{
					int index = list[UnityEngine.Random.Range(0, list.Count)];
					joinMembers[index].PlayVoice(cueID, 1f);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000600 RID: 1536 RVA: 0x0001E76C File Offset: 0x0001CB6C
		public bool PrepareEffect()
		{
			string[] effectIDFromSkillAction = this.GetEffectIDFromSkillAction();
			if (effectIDFromSkillAction != null)
			{
				EffectManager effectMng = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
				for (int i = 0; i < effectIDFromSkillAction.Length; i++)
				{
					effectMng.LoadSingleEffect(effectIDFromSkillAction[i], true);
				}
				return true;
			}
			return false;
		}

		// Token: 0x06000601 RID: 1537 RVA: 0x0001E7B3 File Offset: 0x0001CBB3
		public bool IsPreparingEffect()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny();
		}

		// Token: 0x06000602 RID: 1538 RVA: 0x0001E7CC File Offset: 0x0001CBCC
		private string[] GetEffectIDFromSkillAction()
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			for (int i = 0; i < this.m_Members.Length; i++)
			{
				CharacterHandler characterHandler = this.m_Members[i];
				if (characterHandler != null && characterHandler.IsDonePreparePerfect() && !characterHandler.CharaBattle.PreparedEffectWasFromSkillAction)
				{
					Dictionary<string, int> effectIDFromSkillAction = characterHandler.CharaBattle.GetEffectIDFromSkillAction();
					foreach (string text in effectIDFromSkillAction.Keys)
					{
						if (dictionary.ContainsKey(text))
						{
							Dictionary<string, int> dictionary2;
							string key;
							(dictionary2 = dictionary)[key = text] = dictionary2[key] + effectIDFromSkillAction[text];
						}
						else
						{
							dictionary.Add(text, effectIDFromSkillAction[text]);
						}
					}
					characterHandler.CharaBattle.PreparedEffectWasFromSkillAction = true;
				}
			}
			if (dictionary.Count > 0)
			{
				string[] array = new string[dictionary.Keys.Count];
				dictionary.Keys.CopyTo(array, 0);
				return array;
			}
			return null;
		}

		// Token: 0x06000603 RID: 1539 RVA: 0x0001E8FC File Offset: 0x0001CCFC
		public int GetLeaderSkillID()
		{
			CharacterHandler characterHandler = this.m_Members[0];
			if (characterHandler != null)
			{
				return characterHandler.CharaParam.LeaderSkillID;
			}
			return -1;
		}

		// Token: 0x04000497 RID: 1175
		private CharacterHandler[] m_Members;

		// Token: 0x04000498 RID: 1176
		private int[] m_JoinMemberIndices;

		// Token: 0x0400049C RID: 1180
		private float m_TogetherAttackChainCoefChange;

		// Token: 0x0400049D RID: 1181
		private BattlePartyData.ActionCountData[] m_ActionCountDatas = new BattlePartyData.ActionCountData[4];

		// Token: 0x020000DB RID: 219
		public enum eActionCountType
		{
			// Token: 0x0400049F RID: 1183
			NormalAttackUseCount,
			// Token: 0x040004A0 RID: 1184
			SkillUseCount,
			// Token: 0x040004A1 RID: 1185
			TogetherAttackUseCount,
			// Token: 0x040004A2 RID: 1186
			MemberChangeCount,
			// Token: 0x040004A3 RID: 1187
			Num
		}

		// Token: 0x020000DC RID: 220
		public struct ActionCountData
		{
			// Token: 0x040004A4 RID: 1188
			public int m_Count;

			// Token: 0x040004A5 RID: 1189
			public bool m_RequestResetFlg;
		}
	}
}
