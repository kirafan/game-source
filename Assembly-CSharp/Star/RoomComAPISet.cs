﻿using System;
using RoomResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200058C RID: 1420
	public class RoomComAPISet : INetComHandle
	{
		// Token: 0x06001BAA RID: 7082 RVA: 0x00092907 File Offset: 0x00090D07
		public RoomComAPISet()
		{
			this.ApiName = "player/room/set";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x0400228B RID: 8843
		public long managedRoomId;

		// Token: 0x0400228C RID: 8844
		public int floorId;

		// Token: 0x0400228D RID: 8845
		public int groupId;

		// Token: 0x0400228E RID: 8846
		public PlayerRoomArrangement[] arrangeData;
	}
}
