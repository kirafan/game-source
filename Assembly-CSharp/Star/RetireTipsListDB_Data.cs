﻿using System;

namespace Star
{
	// Token: 0x020001F0 RID: 496
	[Serializable]
	public struct RetireTipsListDB_Data
	{
		// Token: 0x04000BFE RID: 3070
		public int m_ImageID;

		// Token: 0x04000BFF RID: 3071
		public string m_Title;

		// Token: 0x04000C00 RID: 3072
		public string m_Text;
	}
}
