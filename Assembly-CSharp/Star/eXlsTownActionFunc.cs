﻿using System;

namespace Star
{
	// Token: 0x0200024A RID: 586
	public enum eXlsTownActionFunc
	{
		// Token: 0x0400133F RID: 4927
		GoldUse,
		// Token: 0x04001340 RID: 4928
		AddRoom,
		// Token: 0x04001341 RID: 4929
		KRRPointLimitUp
	}
}
