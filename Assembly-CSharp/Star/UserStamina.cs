﻿using System;

namespace Star
{
	// Token: 0x02000B3F RID: 2879
	public class UserStamina
	{
		// Token: 0x06003CE5 RID: 15589 RVA: 0x00135D65 File Offset: 0x00134165
		public long GetValue()
		{
			return this.m_Value;
		}

		// Token: 0x06003CE6 RID: 15590 RVA: 0x00135D6D File Offset: 0x0013416D
		public void SetValue(long value, bool isMaxLimit = false)
		{
			this.m_Value = value;
			if (isMaxLimit && this.m_Value > this.m_ValueMax)
			{
				this.m_Value = this.m_ValueMax;
			}
		}

		// Token: 0x06003CE7 RID: 15591 RVA: 0x00135D99 File Offset: 0x00134199
		public long GetValueMax()
		{
			return this.m_ValueMax;
		}

		// Token: 0x06003CE8 RID: 15592 RVA: 0x00135DA1 File Offset: 0x001341A1
		public void SetValueMax(long valueMax)
		{
			this.m_ValueMax = valueMax;
		}

		// Token: 0x06003CE9 RID: 15593 RVA: 0x00135DAA File Offset: 0x001341AA
		public float GetRemainSec()
		{
			return this.m_RemainSec;
		}

		// Token: 0x06003CEA RID: 15594 RVA: 0x00135DB2 File Offset: 0x001341B2
		public void SetRemainSec(float remainSec)
		{
			this.m_RemainSec = remainSec;
		}

		// Token: 0x06003CEB RID: 15595 RVA: 0x00135DBB File Offset: 0x001341BB
		public void SetRemainSecMax(float remainSecMax)
		{
			this.m_RemainSecMax = remainSecMax;
		}

		// Token: 0x06003CEC RID: 15596 RVA: 0x00135DC4 File Offset: 0x001341C4
		public void UpdateStamina(float deltaSec)
		{
			if (this.m_Value >= this.m_ValueMax)
			{
				this.m_RemainSec = 0f;
				return;
			}
			this.m_RemainSec -= deltaSec;
			if (this.m_RemainSec <= 0f)
			{
				this.m_RemainSec = this.m_RemainSecMax + this.m_RemainSec;
				this.m_Value += 1L;
			}
		}

		// Token: 0x06003CED RID: 15597 RVA: 0x00135E2E File Offset: 0x0013422E
		public bool IsFull()
		{
			return this.m_Value >= this.m_ValueMax;
		}

		// Token: 0x06003CEE RID: 15598 RVA: 0x00135E44 File Offset: 0x00134244
		public TimeSpan GetTimeToCompleteRecovery()
		{
			if (this.IsFull())
			{
				return new TimeSpan(0, 0, 0);
			}
			long num = this.m_ValueMax - this.m_Value;
			long num2 = num - 1L;
			float num3;
			if (num2 <= 0L)
			{
				num3 = this.m_RemainSec;
			}
			else
			{
				num3 = (float)num2 * this.m_RemainSecMax;
				num3 += this.m_RemainSec;
			}
			TimeSpan result = new TimeSpan(0, 0, (int)num3);
			return result;
		}

		// Token: 0x04004478 RID: 17528
		private long m_Value;

		// Token: 0x04004479 RID: 17529
		private long m_ValueMax;

		// Token: 0x0400447A RID: 17530
		private float m_RemainSec;

		// Token: 0x0400447B RID: 17531
		private float m_RemainSecMax;
	}
}
