﻿using System;

namespace Star
{
	// Token: 0x02000725 RID: 1829
	public class TownBuildMoveBufParam : ITownEventCommand
	{
		// Token: 0x06002414 RID: 9236 RVA: 0x000C1AEC File Offset: 0x000BFEEC
		public TownBuildMoveBufParam(int fbackpoint, int fbuildpoint, long fmanageid, int fobjid, int fsynckey = -1, TownBuildMoveBufParam.BuildUpEvent pcallback = null)
		{
			this.m_Type = eTownRequest.ChangeBuf;
			this.m_BackBuildPoint = fbackpoint;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_EventCall = pcallback;
			this.m_Enable = true;
			this.m_SyncKey = fsynckey;
			this.m_Step = TownBuildMoveBufParam.eStep.BuildQue;
		}

		// Token: 0x06002415 RID: 9237 RVA: 0x000C1B44 File Offset: 0x000BFF44
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			TownBuildMoveBufParam.eStep step = this.m_Step;
			if (step != TownBuildMoveBufParam.eStep.BuildQue)
			{
				if (step != TownBuildMoveBufParam.eStep.SetUpCheck)
				{
					if (step == TownBuildMoveBufParam.eStep.Final)
					{
						if (pbuilder.IsBuildMngIDToBuildUp(this.m_ManageID))
						{
							if (this.m_SyncKey >= 0)
							{
								pbuilder.SetActiveSyncKey(this.m_SyncKey);
							}
							this.m_Enable = false;
						}
					}
				}
				else if (pbuilder.BuildToPoint(new TownBuildUpBufParam(this.m_BuildPointID, this.m_ManageID, this.m_ObjID, true, null)
				{
					m_AccessLinkID = true
				}, false))
				{
					this.m_Step = TownBuildMoveBufParam.eStep.Final;
				}
			}
			else
			{
				TownBuildLinkPoint buildPointTreeNode = pbuilder.GetBuildTree().GetBuildPointTreeNode(this.m_BackBuildPoint);
				if (buildPointTreeNode != null)
				{
					buildPointTreeNode.m_Pakage.BackBuildMode();
					this.m_Step = TownBuildMoveBufParam.eStep.SetUpCheck;
				}
				else
				{
					this.m_Step = TownBuildMoveBufParam.eStep.SetUpCheck;
				}
			}
			return this.m_Enable;
		}

		// Token: 0x04002B14 RID: 11028
		public int m_BackBuildPoint;

		// Token: 0x04002B15 RID: 11029
		public int m_BuildPointID;

		// Token: 0x04002B16 RID: 11030
		public int m_ObjID;

		// Token: 0x04002B17 RID: 11031
		public long m_ManageID;

		// Token: 0x04002B18 RID: 11032
		public int m_SyncKey;

		// Token: 0x04002B19 RID: 11033
		public TownBuildMoveBufParam.eStep m_Step;

		// Token: 0x04002B1A RID: 11034
		public TownBuildMoveBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000726 RID: 1830
		public enum eStep
		{
			// Token: 0x04002B1C RID: 11036
			BuildQue,
			// Token: 0x04002B1D RID: 11037
			SetUpCheck,
			// Token: 0x04002B1E RID: 11038
			Final
		}

		// Token: 0x02000727 RID: 1831
		// (Invoke) Token: 0x06002417 RID: 9239
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
