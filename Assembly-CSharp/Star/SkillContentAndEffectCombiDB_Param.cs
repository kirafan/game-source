﻿using System;

namespace Star
{
	// Token: 0x020001FF RID: 511
	[Serializable]
	public struct SkillContentAndEffectCombiDB_Param
	{
		// Token: 0x04000CA4 RID: 3236
		public byte m_ID;

		// Token: 0x04000CA5 RID: 3237
		public float[] m_Thresholds;
	}
}
