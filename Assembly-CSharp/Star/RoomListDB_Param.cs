﻿using System;

namespace Star
{
	// Token: 0x020001F3 RID: 499
	[Serializable]
	public struct RoomListDB_Param
	{
		// Token: 0x04000C04 RID: 3076
		public int m_ID;

		// Token: 0x04000C05 RID: 3077
		public int m_BgID;

		// Token: 0x04000C06 RID: 3078
		public int m_WallID;

		// Token: 0x04000C07 RID: 3079
		public int m_FloorID;
	}
}
