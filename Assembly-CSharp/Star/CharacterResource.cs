﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000157 RID: 343
	public class CharacterResource
	{
		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x060009D0 RID: 2512 RVA: 0x0003C3ED File Offset: 0x0003A7ED
		public eCharaResourceType ResourceType
		{
			get
			{
				return this.m_ResourceType;
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x060009D1 RID: 2513 RVA: 0x0003C3F5 File Offset: 0x0003A7F5
		public int ResourceID
		{
			get
			{
				return this.m_ResourceID;
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x060009D2 RID: 2514 RVA: 0x0003C3FD File Offset: 0x0003A7FD
		public bool IsBosssResource
		{
			get
			{
				return this.m_IsBossResource;
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x060009D3 RID: 2515 RVA: 0x0003C405 File Offset: 0x0003A805
		public string VoiceCueSheetName
		{
			get
			{
				return this.m_VoiceCueSheetName;
			}
		}

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060009D4 RID: 2516 RVA: 0x0003C40D File Offset: 0x0003A80D
		public bool IsAvailableVoice
		{
			get
			{
				return this.m_IsAvailableVoice;
			}
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060009D5 RID: 2517 RVA: 0x0003C415 File Offset: 0x0003A815
		public TweetListDB TweetListDB
		{
			get
			{
				return this.m_TweetListDB;
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x060009D6 RID: 2518 RVA: 0x0003C41D File Offset: 0x0003A81D
		// (set) Token: 0x060009D7 RID: 2519 RVA: 0x0003C42A File Offset: 0x0003A82A
		public string[] AnimKeyForViewer
		{
			get
			{
				return this.m_AnimKeyForViewer.ToArray();
			}
			set
			{
				this.m_AnimKeyForViewer = new List<string>(value);
			}
		}

		// Token: 0x060009D8 RID: 2520 RVA: 0x0003C438 File Offset: 0x0003A838
		public void Update()
		{
			if (this.m_IsPreparing)
			{
				this.UpdatePrepare();
			}
		}

		// Token: 0x060009D9 RID: 2521 RVA: 0x0003C44B File Offset: 0x0003A84B
		public void Setup(CharacterHandler charaHndl, eCharaResourceType resourceType, int resourceID)
		{
			this.m_Owner = charaHndl;
			this.m_ResourceType = resourceType;
			this.m_ResourceID = resourceID;
		}

		// Token: 0x060009DA RID: 2522 RVA: 0x0003C464 File Offset: 0x0003A864
		public void Destroy()
		{
			CharacterResourceManager charaResMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng;
			if (this.m_CharaModelHndl != null)
			{
				charaResMng.Unload(this.m_CharaModelHndl);
				this.m_CharaModelHndl = null;
			}
			if (this.m_WeaponModelHndl != null)
			{
				charaResMng.Unload(this.m_WeaponModelHndl);
				this.m_WeaponModelHndl = null;
			}
			if (this.m_AnimResHndlWrappers != null)
			{
				for (int i = 0; i < this.m_AnimResHndlWrappers.Length; i++)
				{
					if (this.m_AnimResHndlWrappers[i] != null)
					{
						foreach (CharaAnimResourceHndlWrapper charaAnimResourceHndlWrapper in this.m_AnimResHndlWrappers[i].Values)
						{
							charaResMng.Unload(charaAnimResourceHndlWrapper.m_ABResObjHndl);
						}
						this.m_AnimResHndlWrappers[i].Clear();
						this.m_AnimResHndlWrappers[i] = null;
					}
				}
				this.m_AnimResHndlWrappers = null;
			}
			if (this.m_FacialHndl != null)
			{
				charaResMng.Unload(this.m_FacialHndl);
				this.m_FacialHndl = null;
			}
			if (this.m_ShadowHndl != null)
			{
				charaResMng.Unload(this.m_ShadowHndl);
				this.m_ShadowHndl = null;
			}
			if (this.m_IsAvailableVoice)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadVoiceCueSheet(this.m_VoiceCueSheetName);
				this.m_VoiceCueSheetName = null;
				this.m_IsAvailableVoice = false;
			}
			this.m_TweetListDB = null;
		}

		// Token: 0x060009DB RID: 2523 RVA: 0x0003C5D4 File Offset: 0x0003A9D4
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x060009DC RID: 2524 RVA: 0x0003C5DC File Offset: 0x0003A9DC
		public bool IsDonePrepare()
		{
			return this.m_PrepareStep == CharacterResource.ePrepareStep.Done;
		}

		// Token: 0x060009DD RID: 2525 RVA: 0x0003C5F0 File Offset: 0x0003A9F0
		public void Prepare()
		{
			this.m_IsPreparing = true;
			this.m_NeedPrepareFlgs = new bool[6];
			for (int i = 0; i < this.m_NeedPrepareFlgs.Length; i++)
			{
				this.m_NeedPrepareFlgs[i] = false;
			}
			this.PrepareCharaModel();
			this.PrepareWeaponModel();
			this.PrepareVoice();
			this.m_PrepareStep = CharacterResource.ePrepareStep.Step_0;
			this.PostPrepareTweetListDB();
		}

		// Token: 0x060009DE RID: 2526 RVA: 0x0003C654 File Offset: 0x0003AA54
		private void UpdatePrepare()
		{
			CharacterResource.ePrepareStep prepareStep = this.m_PrepareStep;
			if (prepareStep != CharacterResource.ePrepareStep.Step_0)
			{
				if (prepareStep == CharacterResource.ePrepareStep.Step_1)
				{
					if (this.IsDonePrepareAnimation())
					{
						if (this.IsDonePrepareFacial())
						{
							if (this.IsDonePrepareShadow())
							{
								this.PostPrepareAnimation();
								this.PostPrepareFacial();
								this.PostPrepareShadow();
								this.m_PrepareStep = CharacterResource.ePrepareStep.Done;
								this.m_IsPreparing = false;
							}
						}
					}
				}
			}
			else if (this.IsDonePrepareCharaModel())
			{
				if (this.IsDonePrepareWeaponModel())
				{
					if (this.IsDonePrepareVoice())
					{
						this.PostPrepareCharaModel();
						this.PostPrepareWeaponModel();
						this.PostPrepareVoice();
						this.PrepareAnimation();
						this.PrepareFacial();
						this.PrepareShadow();
						this.m_PrepareStep = CharacterResource.ePrepareStep.Step_1;
					}
				}
			}
		}

		// Token: 0x060009DF RID: 2527 RVA: 0x0003C730 File Offset: 0x0003AB30
		private void PrepareCharaModel()
		{
			string modelResourcePath = CharacterUtility.GetModelResourcePath(this.m_ResourceType, this.m_ResourceID);
			if (!string.IsNullOrEmpty(modelResourcePath))
			{
				this.m_CharaModelHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.Load(modelResourcePath, new MeigeResource.Option[0]);
			}
			else
			{
				this.m_CharaModelHndl = null;
			}
			this.m_NeedPrepareFlgs[0] = true;
		}

		// Token: 0x060009E0 RID: 2528 RVA: 0x0003C78B File Offset: 0x0003AB8B
		private bool IsDonePrepareCharaModel()
		{
			return this.m_CharaModelHndl == null || (this.m_CharaModelHndl.IsDone() && !this.m_CharaModelHndl.IsError());
		}

		// Token: 0x060009E1 RID: 2529 RVA: 0x0003C7C0 File Offset: 0x0003ABC0
		private void PostPrepareCharaModel()
		{
			if (!this.m_NeedPrepareFlgs[0])
			{
				return;
			}
			GameObject[] array;
			if (this.m_ResourceType == eCharaResourceType.Player)
			{
				array = new GameObject[2];
				GameObject gameObject = this.m_CharaModelHndl.FindObjContains("_head") as GameObject;
				if (gameObject != null)
				{
					int num = 0;
					array[num] = UnityEngine.Object.Instantiate<GameObject>(gameObject, this.m_Owner.CacheTransform, false);
				}
				gameObject = (this.m_CharaModelHndl.FindObjContains("_body") as GameObject);
				if (gameObject != null)
				{
					int num = 1;
					array[num] = UnityEngine.Object.Instantiate<GameObject>(gameObject, this.m_Owner.CacheTransform, false);
				}
			}
			else
			{
				array = new GameObject[1];
				if (this.m_CharaModelHndl.Objs.Length >= 1)
				{
					GameObject original = this.m_CharaModelHndl.Objs[0] as GameObject;
					array[0] = UnityEngine.Object.Instantiate<GameObject>(original, this.m_Owner.CacheTransform, false);
				}
			}
			this.m_Owner.SetupCharaModel(array);
		}

		// Token: 0x060009E2 RID: 2530 RVA: 0x0003C8C4 File Offset: 0x0003ACC4
		private void PrepareWeaponModel()
		{
			if (this.m_Owner.Mode == CharacterDefine.eMode.Room)
			{
				this.m_WeaponModelHndl = null;
				return;
			}
			if (this.m_Owner.WeaponParam == null && this.m_Owner.Mode == CharacterDefine.eMode.Battle && this.m_Owner.CharaParam.ClassType != eClassType.None)
			{
				this.m_Owner.WeaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(this.m_Owner.WeaponParam, this.m_Owner.CharaParam.ClassType);
			}
			if (this.m_Owner.WeaponParam == null || this.m_Owner.WeaponParam.WeaponID == -1)
			{
				this.m_WeaponModelHndl = null;
				return;
			}
			string weaponModelResourcePath = CharacterUtility.GetWeaponModelResourcePath(this.m_Owner.WeaponParam.WeaponID);
			if (!string.IsNullOrEmpty(weaponModelResourcePath))
			{
				this.m_WeaponModelHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.Load(weaponModelResourcePath, new MeigeResource.Option[0]);
			}
			else
			{
				this.m_WeaponModelHndl = null;
			}
			this.m_NeedPrepareFlgs[1] = true;
		}

		// Token: 0x060009E3 RID: 2531 RVA: 0x0003C9D0 File Offset: 0x0003ADD0
		private bool IsDonePrepareWeaponModel()
		{
			return this.m_WeaponModelHndl == null || (this.m_WeaponModelHndl.IsDone() && !this.m_WeaponModelHndl.IsError());
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x0003CA04 File Offset: 0x0003AE04
		private void PostPrepareWeaponModel()
		{
			if (!this.m_NeedPrepareFlgs[1])
			{
				return;
			}
			string[] weaponModelObjectPaths = CharacterUtility.GetWeaponModelObjectPaths(this.m_Owner.WeaponParam.WeaponID);
			List<GameObject> list = new List<GameObject>();
			for (int i = 0; i < 2; i++)
			{
				GameObject gameObject = null;
				UnityEngine.Object @object = this.m_WeaponModelHndl.FindObj(weaponModelObjectPaths[i], true);
				if (@object != null)
				{
					GameObject original = @object as GameObject;
					gameObject = UnityEngine.Object.Instantiate<GameObject>(original);
				}
				else if (this.m_Owner.CharaParam.ClassType == eClassType.Alchemist && i == 0)
				{
					GameObject original2 = this.m_WeaponModelHndl.FindObj(weaponModelObjectPaths[1], true) as GameObject;
					gameObject = UnityEngine.Object.Instantiate<GameObject>(original2);
				}
				list.Add(gameObject);
				if (gameObject != null)
				{
				}
			}
			this.m_Owner.SetupWeaponModel(list.ToArray());
		}

		// Token: 0x060009E5 RID: 2533 RVA: 0x0003CAE4 File Offset: 0x0003AEE4
		private void PrepareVoice()
		{
			this.m_IsAvailableVoice = false;
			if (this.m_ResourceType == eCharaResourceType.Player)
			{
				this.m_IsBossResource = false;
				this.m_VoiceCueSheetName = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetVoiceCueSheet(this.m_Owner.CharaParam.NamedType);
			}
			else
			{
				EnemyResourceListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EnemyResourceListDB.GetParam(this.m_ResourceID);
				this.m_IsBossResource = (param.m_IsBoss == 1);
				this.m_VoiceCueSheetName = param.m_VoiceCueSheetName;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadVoiceCueSheet(this.m_VoiceCueSheetName);
			this.m_NeedPrepareFlgs[2] = true;
		}

		// Token: 0x060009E6 RID: 2534 RVA: 0x0003CB95 File Offset: 0x0003AF95
		private bool IsDonePrepareVoice()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetCueSheetLoadStatus(this.m_VoiceCueSheetName) != SoundDefine.eLoadStatus.Loading;
		}

		// Token: 0x060009E7 RID: 2535 RVA: 0x0003CBB5 File Offset: 0x0003AFB5
		private void PostPrepareVoice()
		{
			if (!this.m_NeedPrepareFlgs[2])
			{
				return;
			}
			this.m_IsAvailableVoice = (SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetCueSheetLoadStatus(this.m_VoiceCueSheetName) == SoundDefine.eLoadStatus.Loaded);
		}

		// Token: 0x060009E8 RID: 2536 RVA: 0x0003CBE4 File Offset: 0x0003AFE4
		private void PrepareAnimation()
		{
			CharacterResourceManager charaResMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng;
			this.m_AnimResHndlWrappers = new Dictionary<string, CharaAnimResourceHndlWrapper>[this.m_Owner.CharaAnim.ModelSetsNum];
			for (int i = 0; i < this.m_AnimResHndlWrappers.Length; i++)
			{
				List<AnimSettings> characterAnimSettingsFromCurrentState = CharacterUtility.GetCharacterAnimSettingsFromCurrentState(this.m_ResourceType, this.m_ResourceID, i, this.m_Owner.Mode, this.m_Owner, false);
				if (characterAnimSettingsFromCurrentState == null || characterAnimSettingsFromCurrentState.Count <= 0)
				{
					return;
				}
				this.m_AnimResHndlWrappers[i] = new Dictionary<string, CharaAnimResourceHndlWrapper>();
				for (int j = 0; j < characterAnimSettingsFromCurrentState.Count; j++)
				{
					AnimSettings animSettings = characterAnimSettingsFromCurrentState[j];
					if (animSettings.m_IsPack)
					{
						if (!this.m_AnimResHndlWrappers[i].ContainsKey(animSettings.m_ResoucePath))
						{
							ABResourceObjectHandler abresourceObjectHandler = charaResMng.Load(animSettings.m_ResoucePath, new MeigeResource.Option[0]);
							if (abresourceObjectHandler != null)
							{
								CharaAnimResourceHndlWrapper charaAnimResourceHndlWrapper = new CharaAnimResourceHndlWrapper(abresourceObjectHandler, animSettings.m_ActionKey, animSettings.m_ObjectNameWithoutExt);
								this.m_AnimResHndlWrappers[i].Add(animSettings.m_ResoucePath, charaAnimResourceHndlWrapper);
							}
						}
						else
						{
							CharaAnimResourceHndlWrapper charaAnimResourceHndlWrapper = this.m_AnimResHndlWrappers[i][animSettings.m_ResoucePath];
							charaAnimResourceHndlWrapper.m_ActKeys.Add(animSettings.m_ActionKey);
							charaAnimResourceHndlWrapper.m_Paths.Add(animSettings.m_ObjectNameWithoutExt);
						}
					}
					else if (!this.m_AnimResHndlWrappers[i].ContainsKey(animSettings.m_ResoucePath))
					{
						ABResourceObjectHandler abresourceObjectHandler2 = charaResMng.Load(animSettings.m_ResoucePath, new MeigeResource.Option[0]);
						if (abresourceObjectHandler2 != null)
						{
							CharaAnimResourceHndlWrapper value = new CharaAnimResourceHndlWrapper(abresourceObjectHandler2, animSettings.m_ActionKey, animSettings.m_ObjectNameWithoutExt);
							this.m_AnimResHndlWrappers[i].AddOrReplace(animSettings.m_ResoucePath, value);
						}
					}
				}
			}
			this.m_NeedPrepareFlgs[3] = true;
		}

		// Token: 0x060009E9 RID: 2537 RVA: 0x0003CDB8 File Offset: 0x0003B1B8
		private bool IsDonePrepareAnimation()
		{
			if (this.m_AnimResHndlWrappers == null)
			{
				return true;
			}
			for (int i = 0; i < this.m_AnimResHndlWrappers.Length; i++)
			{
				foreach (string key in this.m_AnimResHndlWrappers[i].Keys)
				{
					if (this.m_AnimResHndlWrappers[i][key] != null && this.m_AnimResHndlWrappers[i][key].m_ABResObjHndl != null && (!this.m_AnimResHndlWrappers[i][key].m_ABResObjHndl.IsDone() || this.m_AnimResHndlWrappers[i][key].m_ABResObjHndl.IsError()))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x060009EA RID: 2538 RVA: 0x0003CEAC File Offset: 0x0003B2AC
		private void PostPrepareAnimation()
		{
			if (!this.m_NeedPrepareFlgs[3])
			{
				return;
			}
			if (this.m_Owner.Mode == CharacterDefine.eMode.Viewer)
			{
				this.m_AnimKeyForViewer = new List<string>();
			}
			else
			{
				this.m_AnimKeyForViewer = null;
			}
			for (int i = 0; i < this.m_AnimResHndlWrappers.Length; i++)
			{
				this.m_Owner.SetupAnimClip_Open(i);
				foreach (CharaAnimResourceHndlWrapper charaAnimResourceHndlWrapper in this.m_AnimResHndlWrappers[i].Values)
				{
					if (charaAnimResourceHndlWrapper != null && charaAnimResourceHndlWrapper.m_ABResObjHndl != null)
					{
						for (int j = 0; j < charaAnimResourceHndlWrapper.m_ActKeys.Count; j++)
						{
							GameObject animPrefab = charaAnimResourceHndlWrapper.m_ABResObjHndl.FindObj(charaAnimResourceHndlWrapper.m_Paths[j], false) as GameObject;
							this.m_Owner.SetupAnimClip(charaAnimResourceHndlWrapper.m_ActKeys[j], animPrefab, i);
							if (this.m_AnimKeyForViewer != null && i == 0)
							{
								this.m_AnimKeyForViewer.Add(charaAnimResourceHndlWrapper.m_ActKeys[j]);
							}
						}
					}
				}
				this.m_Owner.SetupAnimClip_Close(i);
			}
		}

		// Token: 0x060009EB RID: 2539 RVA: 0x0003D000 File Offset: 0x0003B400
		private void PrepareFacial()
		{
			if (this.m_ResourceType != eCharaResourceType.Player)
			{
				this.m_FacialHndl = null;
				return;
			}
			eCharaNamedType namedType = this.m_Owner.CharaParam.NamedType;
			this.m_FacialHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.Load(CharacterUtility.GetCharacterFacialAnimSetting(namedType), new MeigeResource.Option[0]);
			this.m_NeedPrepareFlgs[4] = true;
		}

		// Token: 0x060009EC RID: 2540 RVA: 0x0003D05B File Offset: 0x0003B45B
		private bool IsDonePrepareFacial()
		{
			return this.m_FacialHndl == null || (this.m_FacialHndl.IsDone() && !this.m_FacialHndl.IsError());
		}

		// Token: 0x060009ED RID: 2541 RVA: 0x0003D08D File Offset: 0x0003B48D
		private void PostPrepareFacial()
		{
			if (!this.m_NeedPrepareFlgs[4])
			{
				return;
			}
			this.m_Owner.SetupFacialAnimClip(this.m_FacialHndl.Objs[0] as GameObject);
		}

		// Token: 0x060009EE RID: 2542 RVA: 0x0003D0BC File Offset: 0x0003B4BC
		private void PrepareShadow()
		{
			if (this.m_Owner.Mode != CharacterDefine.eMode.Battle)
			{
				this.m_ShadowHndl = null;
				return;
			}
			this.m_ShadowHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.Load(CharacterUtility.GetBattleShadowResourcePath(), new MeigeResource.Option[0]);
			this.m_NeedPrepareFlgs[5] = true;
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x0003D10A File Offset: 0x0003B50A
		private bool IsDonePrepareShadow()
		{
			return this.m_ShadowHndl == null || (this.m_ShadowHndl.IsDone() && !this.m_ShadowHndl.IsError());
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x0003D13C File Offset: 0x0003B53C
		private void PostPrepareShadow()
		{
			if (!this.m_NeedPrepareFlgs[5])
			{
				return;
			}
			GameObject original = this.m_ShadowHndl.Objs[0] as GameObject;
			GameObject modelObj = UnityEngine.Object.Instantiate<GameObject>(original);
			this.m_Owner.SetupShadow(modelObj);
		}

		// Token: 0x060009F1 RID: 2545 RVA: 0x0003D17D File Offset: 0x0003B57D
		private void PostPrepareTweetListDB()
		{
			if (this.m_Owner.Mode == CharacterDefine.eMode.Room)
			{
				this.m_TweetListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(this.m_Owner.CharaParam.NamedType);
			}
		}

		// Token: 0x04000927 RID: 2343
		private CharacterHandler m_Owner;

		// Token: 0x04000928 RID: 2344
		private ABResourceObjectHandler m_CharaModelHndl;

		// Token: 0x04000929 RID: 2345
		private ABResourceObjectHandler m_WeaponModelHndl;

		// Token: 0x0400092A RID: 2346
		private Dictionary<string, CharaAnimResourceHndlWrapper>[] m_AnimResHndlWrappers;

		// Token: 0x0400092B RID: 2347
		private ABResourceObjectHandler m_FacialHndl;

		// Token: 0x0400092C RID: 2348
		private ABResourceObjectHandler m_ShadowHndl;

		// Token: 0x0400092D RID: 2349
		private eCharaResourceType m_ResourceType;

		// Token: 0x0400092E RID: 2350
		private int m_ResourceID = -1;

		// Token: 0x0400092F RID: 2351
		private bool m_IsBossResource;

		// Token: 0x04000930 RID: 2352
		private string m_VoiceCueSheetName;

		// Token: 0x04000931 RID: 2353
		private bool m_IsAvailableVoice;

		// Token: 0x04000932 RID: 2354
		private TweetListDB m_TweetListDB;

		// Token: 0x04000933 RID: 2355
		private List<string> m_AnimKeyForViewer;

		// Token: 0x04000934 RID: 2356
		private bool m_IsPreparing;

		// Token: 0x04000935 RID: 2357
		private bool[] m_NeedPrepareFlgs;

		// Token: 0x04000936 RID: 2358
		private CharacterResource.ePrepareStep m_PrepareStep = CharacterResource.ePrepareStep.None;

		// Token: 0x02000158 RID: 344
		private enum ePrepareFlg
		{
			// Token: 0x04000938 RID: 2360
			None = -1,
			// Token: 0x04000939 RID: 2361
			CharaModel,
			// Token: 0x0400093A RID: 2362
			WeaponModel,
			// Token: 0x0400093B RID: 2363
			Voice,
			// Token: 0x0400093C RID: 2364
			Animation,
			// Token: 0x0400093D RID: 2365
			Facial,
			// Token: 0x0400093E RID: 2366
			Shadow,
			// Token: 0x0400093F RID: 2367
			Num
		}

		// Token: 0x02000159 RID: 345
		private enum ePrepareStep
		{
			// Token: 0x04000941 RID: 2369
			None = -1,
			// Token: 0x04000942 RID: 2370
			Step_0,
			// Token: 0x04000943 RID: 2371
			Step_1,
			// Token: 0x04000944 RID: 2372
			Step_2,
			// Token: 0x04000945 RID: 2373
			Done,
			// Token: 0x04000946 RID: 2374
			Num
		}
	}
}
