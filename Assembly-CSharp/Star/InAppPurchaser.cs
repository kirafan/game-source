﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;

namespace Star
{
	// Token: 0x020004C9 RID: 1225
	public class InAppPurchaser : SingletonMonoBehaviour<InAppPurchaser>, IStoreListener
	{
		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06001814 RID: 6164 RVA: 0x0007D2B9 File Offset: 0x0007B6B9
		public InAppPurchaser.eInitializeResult InitializeResult
		{
			get
			{
				if (this.m_InitializeStatus != InAppPurchaser.eInitializeResult.SUCCESS)
				{
					return this.m_InitializeStatus;
				}
				if (this.m_isRestoreFinish)
				{
					return InAppPurchaser.eInitializeResult.SUCCESS;
				}
				return InAppPurchaser.eInitializeResult.NOTYET;
			}
		}

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x06001815 RID: 6165 RVA: 0x0007D2DC File Offset: 0x0007B6DC
		public bool InitializeSuccess
		{
			get
			{
				return this.InitializeResult == InAppPurchaser.eInitializeResult.SUCCESS;
			}
		}

		// Token: 0x06001816 RID: 6166 RVA: 0x0007D2E7 File Offset: 0x0007B6E7
		public string GetErrorTitle()
		{
			return this.m_ErrorTitle;
		}

		// Token: 0x06001817 RID: 6167 RVA: 0x0007D2EF File Offset: 0x0007B6EF
		public string GetErrorMessage()
		{
			return this.m_ErrorMessage;
		}

		// Token: 0x06001818 RID: 6168 RVA: 0x0007D2F7 File Offset: 0x0007B6F7
		public PurchaseFailureReason GetErrorReason()
		{
			return this.m_ErrorReason;
		}

		// Token: 0x06001819 RID: 6169 RVA: 0x0007D300 File Offset: 0x0007B700
		public void AddProducts(List<string> productIDs)
		{
			InAppPurchaser.LOGF("AddProducts >>> productID num:{0}", new object[]
			{
				productIDs.Count
			});
			StandardPurchasingModule first = StandardPurchasingModule.Instance();
			this.m_Builder = ConfigurationBuilder.Instance(first, new IPurchasingModule[0]);
			string publicKey = Convert.ToBase64String(GooglePlayTangle.Data());
			this.m_Builder.Configure<IGooglePlayConfiguration>().SetPublicKey(publicKey);
			for (int i = 0; i < productIDs.Count; i++)
			{
				this.m_Builder.AddProduct(productIDs[i], ProductType.Consumable);
			}
			this.m_PurchaseState = InAppPurchaser.PurchaseState.None;
			this.m_InitializeStatus = InAppPurchaser.eInitializeResult.NOTYET;
			this.m_isRestoreFinish = false;
			if (Application.internetReachability == NetworkReachability.NotReachable)
			{
				this.m_InitializeStatus = InAppPurchaser.eInitializeResult.NO_CONNECTION;
			}
			else
			{
				UnityPurchasing.Initialize(this, this.m_Builder);
			}
		}

		// Token: 0x0600181A RID: 6170 RVA: 0x0007D3C4 File Offset: 0x0007B7C4
		public void Purchase(string productID, string payload)
		{
			if (this.InitializeSuccess && this.m_Controller != null)
			{
				InAppPurchaser.LOGF("Purchase Success >>> productID:{0}, payload:{1}", new object[]
				{
					productID,
					payload
				});
				this.m_PurchaseState = InAppPurchaser.PurchaseState.Processing;
				this.m_ErrorTitle = string.Empty;
				this.m_ErrorMessage = string.Empty;
				this.m_ErrorReason = PurchaseFailureReason.Unknown;
				if (Application.platform == RuntimePlatform.Android)
				{
					this.m_Controller.InitiatePurchase(productID, payload);
				}
				else
				{
					this.m_Controller.InitiatePurchase(productID);
				}
			}
			else
			{
				InAppPurchaser.LOGF("Purchase Failed >>> productID:{0}, payload:{1}", new object[]
				{
					productID,
					payload
				});
				this.m_PurchaseState = InAppPurchaser.PurchaseState.Failure;
			}
		}

		// Token: 0x0600181B RID: 6171 RVA: 0x0007D471 File Offset: 0x0007B871
		public InAppPurchaser.PurchaseState GetPurchaseState()
		{
			if (this.InitializeSuccess)
			{
				return this.m_PurchaseState;
			}
			return InAppPurchaser.PurchaseState.Failure;
		}

		// Token: 0x0600181C RID: 6172 RVA: 0x0007D486 File Offset: 0x0007B886
		public void ConfirmPurchase()
		{
			if (this.InitializeSuccess && this.m_PendingProduct != null)
			{
				this.m_Controller.ConfirmPendingPurchase(this.m_PendingProduct);
			}
			this.m_PendingProduct = null;
		}

		// Token: 0x0600181D RID: 6173 RVA: 0x0007D4B6 File Offset: 0x0007B8B6
		public bool IsRestrictedPurchase()
		{
			return false;
		}

		// Token: 0x0600181E RID: 6174 RVA: 0x0007D4BC File Offset: 0x0007B8BC
		public string GetPayload()
		{
			if (this.InitializeSuccess && this.m_PendingProduct != null)
			{
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_PendingProduct.receipt);
				return receipt.Payload;
			}
			return string.Empty;
		}

		// Token: 0x0600181F RID: 6175 RVA: 0x0007D4FC File Offset: 0x0007B8FC
		public string GetReceiptAndroid()
		{
			if (this.InitializeSuccess && this.m_PendingProduct != null)
			{
				InAppPurchaser.LOGF("m_PendingProduct.receipt:{0}", new object[]
				{
					this.m_PendingProduct.receipt
				});
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_PendingProduct.receipt);
				InAppPurchaser.LOGF("receipt.m_Payload:{0}", new object[]
				{
					receipt.Payload
				});
				InAppPurchaser.Payload payload = JsonUtility.FromJson<InAppPurchaser.Payload>(receipt.Payload);
				string json = payload.json;
				InAppPurchaser.LOG("---- GetReceiptAndroid start ----");
				InAppPurchaser.LOG(json);
				InAppPurchaser.LOG("---- GetReceiptAndroid end ----");
				byte[] bytes = Encoding.UTF8.GetBytes(json);
				return Convert.ToBase64String(bytes);
			}
			return string.Empty;
		}

		// Token: 0x06001820 RID: 6176 RVA: 0x0007D5AC File Offset: 0x0007B9AC
		public string GetSignature()
		{
			if (this.InitializeSuccess && this.m_PendingProduct != null)
			{
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_PendingProduct.receipt);
				InAppPurchaser.Payload payload = JsonUtility.FromJson<InAppPurchaser.Payload>(receipt.Payload);
				return payload.signature;
			}
			return string.Empty;
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x0007D5F8 File Offset: 0x0007B9F8
		public string GetRestorePayload(int index)
		{
			if (this.InitializeSuccess && this.m_RestoreProducts.Count > 0)
			{
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_RestoreProducts[index].receipt);
				return receipt.Payload;
			}
			return string.Empty;
		}

		// Token: 0x06001822 RID: 6178 RVA: 0x0007D644 File Offset: 0x0007BA44
		public string GetRestoreReceiptAndroid(int index)
		{
			if (this.InitializeSuccess && this.m_RestoreProducts.Count > 0)
			{
				InAppPurchaser.LOGF("m_RestoreProducts[index].receipt:{0}", new object[]
				{
					this.m_RestoreProducts[index].receipt
				});
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_RestoreProducts[index].receipt);
				InAppPurchaser.LOGF("receipt.m_Payload:{0}", new object[]
				{
					receipt.Payload
				});
				InAppPurchaser.Payload payload = JsonUtility.FromJson<InAppPurchaser.Payload>(receipt.Payload);
				string json = payload.json;
				byte[] bytes = Encoding.UTF8.GetBytes(json);
				string text = Convert.ToBase64String(bytes);
				InAppPurchaser.LOG("---- GetRestoreReceiptAndroid start ----");
				InAppPurchaser.LOG("raw");
				InAppPurchaser.LOG(json);
				InAppPurchaser.LOG("base64");
				InAppPurchaser.LOG(text);
				InAppPurchaser.LOG("---- GetRestoreReceiptAndroid end ----");
				return text;
			}
			return string.Empty;
		}

		// Token: 0x06001823 RID: 6179 RVA: 0x0007D728 File Offset: 0x0007BB28
		public string GetRestoreSignatureAndroid(int index)
		{
			if (this.InitializeSuccess && this.m_RestoreProducts.Count > 0)
			{
				InAppPurchaser.Receipt receipt = JsonUtility.FromJson<InAppPurchaser.Receipt>(this.m_RestoreProducts[index].receipt);
				InAppPurchaser.Payload payload = JsonUtility.FromJson<InAppPurchaser.Payload>(receipt.Payload);
				return payload.signature;
			}
			return string.Empty;
		}

		// Token: 0x06001824 RID: 6180 RVA: 0x0007D780 File Offset: 0x0007BB80
		public bool IsNeedRestoreProduct()
		{
			return this.InitializeSuccess && this.m_RestoreProducts.Count > 0;
		}

		// Token: 0x06001825 RID: 6181 RVA: 0x0007D7A1 File Offset: 0x0007BBA1
		public int GetRestoreReceiptCount()
		{
			if (this.InitializeSuccess)
			{
				return this.m_RestoreProducts.Count;
			}
			return 0;
		}

		// Token: 0x06001826 RID: 6182 RVA: 0x0007D7BC File Offset: 0x0007BBBC
		public void RestoreProduct(int index)
		{
			if (this.InitializeSuccess && this.m_Controller != null)
			{
				this.m_Controller.ConfirmPendingPurchase(this.m_RestoreProducts[index]);
				if (!this.m_RestoredProductIDs.Contains(this.m_RestoreProducts[index].definition.id))
				{
					this.m_RestoredProductIDs.Add(this.m_RestoreProducts[index].definition.id);
				}
			}
		}

		// Token: 0x06001827 RID: 6183 RVA: 0x0007D840 File Offset: 0x0007BC40
		public void ClearRestoreProduct()
		{
			InAppPurchaser.LOGF("ClearRestoreProduct() >>> InitializeSuccess:{0}, m_RestoredProductIDs.Count:{1}", new object[]
			{
				this.InitializeSuccess,
				this.m_RestoredProductIDs.Count
			});
			if (this.InitializeSuccess)
			{
				using (List<string>.Enumerator enumerator = this.m_RestoredProductIDs.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						string productID = enumerator.Current;
						this.m_RestoreProducts.RemoveAll((Product _product) => productID == _product.definition.id);
						InAppPurchaser.LOGF("ClearRestoreProduct() >>> restored productID:{0}", new object[]
						{
							productID
						});
					}
				}
				this.m_RestoredProductIDs.Clear();
			}
		}

		// Token: 0x06001828 RID: 6184 RVA: 0x0007D918 File Offset: 0x0007BD18
		public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
		{
			InAppPurchaser.LOG("OnInitialized() start");
			this.m_Controller = controller;
			this.m_isRestoreCall = false;
			this.m_isRestoreFinish = true;
			foreach (Product product in this.m_Controller.products.all)
			{
				if (product.hasReceipt)
				{
					this.m_RestoreProducts.Add(product);
				}
			}
			int num = 0;
			foreach (Product product2 in this.m_Controller.products.all)
			{
				if (product2.hasReceipt)
				{
					num++;
				}
			}
			InAppPurchaser.LOGF("OnInitialized() >>> hasReceiptCount{0}", new object[]
			{
				num
			});
			this.m_InitializeStatus = InAppPurchaser.eInitializeResult.SUCCESS;
			InAppPurchaser.LOG("OnInitialized() end");
		}

		// Token: 0x06001829 RID: 6185 RVA: 0x0007D9F2 File Offset: 0x0007BDF2
		public void OnInitializeFailed(InitializationFailureReason error)
		{
			InAppPurchaser.LOGF("OnInitializeFailed() >>> error:{0}", new object[]
			{
				error
			});
			this.m_InitializeStatus = InAppPurchaser.eInitializeResult.FAILURE;
		}

		// Token: 0x0600182A RID: 6186 RVA: 0x0007DA14 File Offset: 0x0007BE14
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			InAppPurchaser.LOGF("ProcessPurchase() >>> productID:{0}, isRestoreCall:{1}", new object[]
			{
				e.purchasedProduct.definition.storeSpecificId,
				this.m_isRestoreCall
			});
			if (this.m_isRestoreCall)
			{
				if (e.purchasedProduct.hasReceipt)
				{
					this.m_RestoreProducts.Add(e.purchasedProduct);
				}
			}
			else
			{
				this.m_PendingProduct = e.purchasedProduct;
			}
			this.m_PurchaseState = InAppPurchaser.PurchaseState.Success;
			return PurchaseProcessingResult.Pending;
		}

		// Token: 0x0600182B RID: 6187 RVA: 0x0007DA98 File Offset: 0x0007BE98
		public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
		{
			this.m_ErrorReason = p;
			switch (this.m_ErrorReason)
			{
			case PurchaseFailureReason.PurchasingUnavailable:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_PurchasingUnavailable_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_PurchasingUnavailable);
				break;
			case PurchaseFailureReason.ExistingPurchasePending:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_ExistingPurchasePending_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_ExistingPurchasePending);
				break;
			case PurchaseFailureReason.ProductUnavailable:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_ProductUnavailable_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_ProductUnavailable);
				break;
			case PurchaseFailureReason.SignatureInvalid:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_SignatureInvalid_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_SignatureInvalid);
				break;
			case PurchaseFailureReason.UserCancelled:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_UserCancelled_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_UserCancelled);
				break;
			case PurchaseFailureReason.PaymentDeclined:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_PaymentDeclined_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_PaymentDeclined);
				break;
			case PurchaseFailureReason.DuplicateTransaction:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_DuplicateTransaction_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_DuplicateTransaction);
				break;
			default:
				this.m_ErrorTitle = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_Unknown_Title);
				this.m_ErrorMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPFailureReason_Unknown);
				break;
			}
			InAppPurchaser.LOGF("OnPurchaseFailed() >>> reason:{0}, errormessage:{1}", new object[]
			{
				this.m_ErrorReason,
				this.m_ErrorMessage
			});
			this.m_PurchaseState = InAppPurchaser.PurchaseState.Failure;
		}

		// Token: 0x0600182C RID: 6188 RVA: 0x0007DCD0 File Offset: 0x0007C0D0
		[Conditional("____LOG_IAP")]
		private static void LOG(object o)
		{
			UnityEngine.Debug.Log("[IAP]" + o);
		}

		// Token: 0x0600182D RID: 6189 RVA: 0x0007DCE2 File Offset: 0x0007C0E2
		[Conditional("____LOG_IAP")]
		private static void LOGF(string format, params object[] args)
		{
			UnityEngine.Debug.LogFormat("[IAP]" + format, args);
		}

		// Token: 0x04001ED9 RID: 7897
		private IStoreController m_Controller;

		// Token: 0x04001EDA RID: 7898
		private ConfigurationBuilder m_Builder;

		// Token: 0x04001EDB RID: 7899
		private InAppPurchaser.eInitializeResult m_InitializeStatus;

		// Token: 0x04001EDC RID: 7900
		private InAppPurchaser.PurchaseState m_PurchaseState;

		// Token: 0x04001EDD RID: 7901
		private PurchaseFailureReason m_ErrorReason = PurchaseFailureReason.Unknown;

		// Token: 0x04001EDE RID: 7902
		private string m_ErrorTitle;

		// Token: 0x04001EDF RID: 7903
		private string m_ErrorMessage;

		// Token: 0x04001EE0 RID: 7904
		private Product m_PendingProduct;

		// Token: 0x04001EE1 RID: 7905
		private List<Product> m_RestoreProducts = new List<Product>();

		// Token: 0x04001EE2 RID: 7906
		private List<string> m_RestoredProductIDs = new List<string>();

		// Token: 0x04001EE3 RID: 7907
		private bool m_isRestoreCall;

		// Token: 0x04001EE4 RID: 7908
		private bool m_isRestoreFinish;

		// Token: 0x020004CA RID: 1226
		private class Receipt
		{
			// Token: 0x04001EE5 RID: 7909
			public string Store;

			// Token: 0x04001EE6 RID: 7910
			public string TransactionID;

			// Token: 0x04001EE7 RID: 7911
			public string Payload;
		}

		// Token: 0x020004CB RID: 1227
		private class Payload
		{
			// Token: 0x04001EE8 RID: 7912
			public string json;

			// Token: 0x04001EE9 RID: 7913
			public string signature;
		}

		// Token: 0x020004CC RID: 1228
		public enum eInitializeResult
		{
			// Token: 0x04001EEB RID: 7915
			NOTYET,
			// Token: 0x04001EEC RID: 7916
			SUCCESS,
			// Token: 0x04001EED RID: 7917
			FAILURE,
			// Token: 0x04001EEE RID: 7918
			NO_CONNECTION
		}

		// Token: 0x020004CD RID: 1229
		public enum PurchaseState
		{
			// Token: 0x04001EF0 RID: 7920
			None,
			// Token: 0x04001EF1 RID: 7921
			Processing,
			// Token: 0x04001EF2 RID: 7922
			Success,
			// Token: 0x04001EF3 RID: 7923
			Failure
		}
	}
}
