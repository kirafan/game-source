﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020005E1 RID: 1505
	public class RoomBuilder : MonoBehaviour
	{
		// Token: 0x1400001C RID: 28
		// (add) Token: 0x06001DA8 RID: 7592 RVA: 0x0009E5E4 File Offset: 0x0009C9E4
		// (remove) Token: 0x06001DA9 RID: 7593 RVA: 0x0009E61C File Offset: 0x0009CA1C
		public event Action<IMainComCommand> OnEventSend;

		// Token: 0x06001DAA RID: 7594 RVA: 0x0009E652 File Offset: 0x0009CA52
		public void SetOwner(RoomMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06001DAB RID: 7595 RVA: 0x0009E65B File Offset: 0x0009CA5B
		public bool IsEditNew
		{
			get
			{
				return this.m_IsEditNew;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06001DAC RID: 7596 RVA: 0x0009E663 File Offset: 0x0009CA63
		public bool IsBuilt
		{
			get
			{
				return this.m_IsBuilt;
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06001DAD RID: 7597 RVA: 0x0009E66B File Offset: 0x0009CA6B
		public IRoomObjectControll SelectingObj
		{
			get
			{
				return this.m_SelectingObj;
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06001DAE RID: 7598 RVA: 0x0009E673 File Offset: 0x0009CA73
		public Transform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x06001DAF RID: 7599 RVA: 0x0009E67B File Offset: 0x0009CA7B
		public long GetManageID()
		{
			return this.m_UserRoomManageID;
		}

		// Token: 0x06001DB0 RID: 7600 RVA: 0x0009E684 File Offset: 0x0009CA84
		public int GetFloorNO()
		{
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
			return manageIDToUserRoomData.FloorID;
		}

		// Token: 0x06001DB1 RID: 7601 RVA: 0x0009E6A3 File Offset: 0x0009CAA3
		public int GetTimeCategory()
		{
			return (int)this.m_TimeCategory;
		}

		// Token: 0x06001DB2 RID: 7602 RVA: 0x0009E6AC File Offset: 0x0009CAAC
		public int GetFloorFreeSpaceNum(eRoomObjectCategory fcategory, int floorno = 0)
		{
			int result = 0;
			if (this.m_GridStatus != null)
			{
				switch (fcategory)
				{
				case eRoomObjectCategory.Desk:
				case eRoomObjectCategory.Chair:
				case eRoomObjectCategory.Storage:
				case eRoomObjectCategory.Bedding:
				case eRoomObjectCategory.Appliances:
				case eRoomObjectCategory.Goods:
				case eRoomObjectCategory.Hobby:
				case eRoomObjectCategory.Carpet:
				case eRoomObjectCategory.Screen:
					result = this.m_GridStatus[floorno].m_MoveFreeSpaceNum;
					break;
				case eRoomObjectCategory.WallDecoration:
				case eRoomObjectCategory.Floor:
				case eRoomObjectCategory.Wall:
				case eRoomObjectCategory.Background:
					result = 10;
					break;
				}
			}
			return result;
		}

		// Token: 0x1400001D RID: 29
		// (add) Token: 0x06001DB3 RID: 7603 RVA: 0x0009E728 File Offset: 0x0009CB28
		// (remove) Token: 0x06001DB4 RID: 7604 RVA: 0x0009E760 File Offset: 0x0009CB60
		public event Action<IRoomObjectControll, bool> OnSelectObject;

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x06001DB5 RID: 7605 RVA: 0x0009E798 File Offset: 0x0009CB98
		// (remove) Token: 0x06001DB6 RID: 7606 RVA: 0x0009E7D0 File Offset: 0x0009CBD0
		public event Action OnUnSelectObject;

		// Token: 0x1400001F RID: 31
		// (add) Token: 0x06001DB7 RID: 7607 RVA: 0x0009E808 File Offset: 0x0009CC08
		// (remove) Token: 0x06001DB8 RID: 7608 RVA: 0x0009E840 File Offset: 0x0009CC40
		public event Action OnSelectingObjectCanPlacement;

		// Token: 0x14000020 RID: 32
		// (add) Token: 0x06001DB9 RID: 7609 RVA: 0x0009E878 File Offset: 0x0009CC78
		// (remove) Token: 0x06001DBA RID: 7610 RVA: 0x0009E8B0 File Offset: 0x0009CCB0
		public event Action OnSelectingObjectCanNotPlacement;

		// Token: 0x14000021 RID: 33
		// (add) Token: 0x06001DBB RID: 7611 RVA: 0x0009E8E8 File Offset: 0x0009CCE8
		// (remove) Token: 0x06001DBC RID: 7612 RVA: 0x0009E920 File Offset: 0x0009CD20
		public event Action OnObjectDragStart;

		// Token: 0x14000022 RID: 34
		// (add) Token: 0x06001DBD RID: 7613 RVA: 0x0009E958 File Offset: 0x0009CD58
		// (remove) Token: 0x06001DBE RID: 7614 RVA: 0x0009E990 File Offset: 0x0009CD90
		public event Action OnObjectDragEnd;

		// Token: 0x06001DBF RID: 7615 RVA: 0x0009E9C8 File Offset: 0x0009CDC8
		private void Awake()
		{
			this.m_Transform = base.gameObject.GetComponent<Transform>();
			this.m_Camera = Camera.main;
			Vector3 localPosition = this.m_Camera.transform.localPosition;
			localPosition.z = -1000f;
			this.m_Camera.transform.localPosition = localPosition;
			ObjectResourceManager.SetUpManager(base.gameObject);
			this.m_ActivePlay = false;
			this.m_ObjTouch = new IRoomObjectControll.ObjTouchState(this.m_RoomCamera);
		}

		// Token: 0x06001DC0 RID: 7616 RVA: 0x0009EA44 File Offset: 0x0009CE44
		private void Update()
		{
			switch (this.m_Mode)
			{
			case RoomBuilder.eMode.Prepare_First:
				if (RoomNetComManager.IsSetUp())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Room);
					RoomObjectOption.DatabaseSetUp();
					RoomAnimePlay.DatabaseSetUp();
					FieldMapManager.CheckWakeUpObject(base.gameObject);
					this.m_EventQue = new RoomEventQue(20);
					RoomActionDataPakage.ReadPakage();
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopSystem, true);
					this.m_TimeCheckHour = ScheduleTimeUtil.GetManageUnixTime().Hour;
					this.m_TimeCategory = 0;
					if (this.m_TimeCheckHour >= 18 || this.m_TimeCheckHour < 6)
					{
						this.m_TimeCategory = 1;
					}
					this.m_Mode = RoomBuilder.eMode.Prepare_Bg;
				}
				break;
			case RoomBuilder.eMode.Prepare_Bg:
				if (FieldMapManager.IsSetUp())
				{
					this.m_UserRoomManageID = UserRoomUtil.GetActiveRoomMngID(this.m_SelectPlayerId);
					UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
					UserRoomData.PlacementData categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Background);
					if (categoryToPlacementData != null)
					{
						this.m_Bg = RoomObjectControllCreate.SetupMdlSimple(RoomObjectControllCreate.CreateBGObject(), (int)this.m_TimeCategory, -10000, LayerMask.NameToLayer("BG"), categoryToPlacementData.m_ObjectID, categoryToPlacementData.m_ManageID, true);
						int floorNO = this.GetFloorNO();
						categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Wall);
						bool flag = categoryToPlacementData != null;
						int count = this.m_FloorCategory.Count;
						int num = 0;
						for (int i = 0; i < count; i++)
						{
							if (this.m_FloorCategory[i].m_RoomID == floorNO)
							{
								num++;
							}
						}
						this.m_GridStatus = new RoomGridState[num];
						this.m_FloorObject = new IRoomFloorManager[num];
						num = 0;
						for (int i = 0; i < count; i++)
						{
							if (this.m_FloorCategory[i].m_RoomID == floorNO)
							{
								this.m_FloorObject[num] = IRoomFloorManager.CreateFloorManager(this.m_FloorCategory[i]);
								this.m_FloorObject[num].CreateObject(base.transform, flag);
								IRoomObjectControll roomObjectControll;
								if (flag)
								{
									categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Wall);
									roomObjectControll = RoomObjectControllCreate.SetupMdlSimple(this.m_FloorObject[num].CreateWallNode(), num, -960, 0, categoryToPlacementData.m_ObjectID, categoryToPlacementData.m_ManageID, true);
									this.m_FloorObject[num].SetWall(roomObjectControll);
								}
								categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Floor);
								roomObjectControll = RoomObjectControllCreate.SetupMdlSimple(this.m_FloorObject[num].CreateFloorNode(), num, -960, 0, categoryToPlacementData.m_ObjectID, categoryToPlacementData.m_ManageID, true);
								this.m_FloorObject[num].SetFloor(roomObjectControll);
								IVector3 moveSize = this.m_FloorObject[num].GetMoveSize();
								this.m_GridStatus[num] = new RoomGridState();
								this.m_GridStatus[num].SetUp(moveSize.x, moveSize.y, moveSize.z);
								num++;
							}
						}
						if (count > 1)
						{
							this.m_RoomCamera.SetMoveRange(new Rect(-5f, -4f, 17.5f, 8f));
						}
						this.m_Mode = RoomBuilder.eMode.Prepare_Floor_Wait;
					}
					else
					{
						this.m_Mode = RoomBuilder.eMode.Prepare_Placements;
					}
				}
				break;
			case RoomBuilder.eMode.Prepare_Floor_Wait:
			{
				bool flag2 = true;
				for (int i = 0; i < this.m_FloorObject.Length; i++)
				{
					flag2 &= this.m_FloorObject[i].IsSetUp();
				}
				if (flag2)
				{
					for (int i = 0; i < this.m_FloorObject.Length; i++)
					{
						this.m_FloorObject[i].BuildUp(i);
					}
					this.m_Mode = RoomBuilder.eMode.Prepare_Placements;
				}
				break;
			}
			case RoomBuilder.eMode.Prepare_Placements:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets() && RoomObjectOption.IsSetUp())
				{
					UserRoomData manageIDToUserRoomData2 = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
					for (int j = 0; j < manageIDToUserRoomData2.GetPlacementNum(); j++)
					{
						UserRoomData.PlacementData placementAt = manageIDToUserRoomData2.GetPlacementAt(j);
						if (placementAt.m_RoomNo >= 0)
						{
							this.InstantiateObject(placementAt.m_ObjectID, placementAt.m_Dir, placementAt.m_X, placementAt.m_Y, placementAt.m_RoomNo, placementAt.m_ManageID);
						}
					}
					this.m_CharaManager = IRoomCharaManager.LinkUpManager(base.gameObject, this, this.m_SelectPlayerId, this.m_FieldPartyInSchedule);
					this.m_Mode = RoomBuilder.eMode.Prepare_Placements_Wait;
				}
				break;
			case RoomBuilder.eMode.Prepare_Placements_Wait:
				if (this.IsActiveObjSetUp())
				{
					for (int k = 0; k < this.m_ActiveRoomObjHndls.Count; k++)
					{
						IRoomObjectControll roomObjectControll2 = this.m_ActiveRoomObjHndls[k];
						roomObjectControll2.UpGridState(this.m_GridStatus[roomObjectControll2.FloorID], -1);
					}
					this.m_Mode = RoomBuilder.eMode.Prepare_Charas;
				}
				break;
			case RoomBuilder.eMode.Prepare_Charas:
				if (this.m_CharaManager.IsAcitveResource() && RoomAnimePlay.IsSetUp() && RoomActionDataPakage.IsSetUp())
				{
					Application.targetFrameRate = 30;
					this.m_CharaManager.SetUpRoomCharacter();
					this.m_Mode = RoomBuilder.eMode.Prepare_Charas_Wait;
				}
				break;
			case RoomBuilder.eMode.Prepare_Charas_Wait:
				if (this.m_CharaManager.IsBuild())
				{
					this.m_IsBuilt = true;
					this.m_ActivePlay = true;
					this.SortRoom();
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopRoom, true);
					this.m_Mode = RoomBuilder.eMode.Main;
				}
				break;
			case RoomBuilder.eMode.Main:
				this.UpdateMain();
				if (this.m_Owner != null && this.m_Owner.IsIdleStep() && this.m_Owner.RoomUI != null && this.m_Owner.RoomUI.CheckRaycast())
				{
					this.CheckTimeBuilding();
				}
				break;
			case RoomBuilder.eMode.Main_Edit:
				this.UpdateMainEdit();
				break;
			}
			this.FlushEventRequest();
		}

		// Token: 0x06001DC1 RID: 7617 RVA: 0x0009EFE3 File Offset: 0x0009D3E3
		private void LateUpdate()
		{
			if (this.m_ActivePlay)
			{
				this.SortRoom();
			}
		}

		// Token: 0x06001DC2 RID: 7618 RVA: 0x0009EFF6 File Offset: 0x0009D3F6
		public void Initialize()
		{
			this.m_ObjectCache.Initialize(this.m_Transform);
		}

		// Token: 0x06001DC3 RID: 7619 RVA: 0x0009F009 File Offset: 0x0009D409
		public void OnDestroy()
		{
			ObjectResourceManager.ReleaseCheck();
		}

		// Token: 0x06001DC4 RID: 7620 RVA: 0x0009F010 File Offset: 0x0009D410
		public void Destroy()
		{
			MissionManager.SetPopUpActive(eXlsPopupTiming.PopRoom, false);
			this.m_Bg.Destroy();
			for (int i = this.m_ActiveRoomObjHndls.Count - 1; i >= 0; i--)
			{
				this.m_ActiveRoomObjHndls[i].Destroy();
			}
			this.m_ActiveRoomObjHndls.Clear();
			this.m_CharaManager.DeleteChara();
			this.m_CharaManager.BackLinkManager();
			for (int i = 0; i < this.m_FloorObject.Length; i++)
			{
				this.m_FloorObject[i].Destroy();
			}
			for (int i = 0; i < this.m_GridStatus.Length; i++)
			{
				if (this.m_GridStatus[i] != null)
				{
					this.m_GridStatus[i] = null;
				}
			}
			RoomObjectOption.RoomObjectDatabaseRelease();
			RoomAnimePlay.DatabaseRelease();
			FieldMapManager.StoreRelease();
			this.m_GridStatus = null;
			this.m_IsBuilt = false;
			this.m_Mode = RoomBuilder.eMode.None;
			RoomActionDataPakage.DataRelease();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Room);
			this.m_ActivePlay = false;
			ObjectResourceManager.ReleaseCheck();
		}

		// Token: 0x06001DC5 RID: 7621 RVA: 0x0009F11C File Offset: 0x0009D51C
		public bool Prepare(long playerId, bool feditmode)
		{
			if (this.m_Mode != RoomBuilder.eMode.None)
			{
				return false;
			}
			Application.targetFrameRate = 60;
			if (this.m_IsBuilt)
			{
				Debug.LogWarning("Build() failed.>>> It has already been built.");
				return false;
			}
			this.m_Mode = RoomBuilder.eMode.Prepare_First;
			this.m_SelectPlayerId = playerId;
			this.m_FieldPartyInSchedule = (playerId == SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
			UserDataUtil.LinkUpUtil();
			RoomNetComManager.SetUpManager(base.gameObject, playerId);
			return true;
		}

		// Token: 0x06001DC6 RID: 7622 RVA: 0x0009F192 File Offset: 0x0009D592
		public bool IsPreparing()
		{
			return this.m_Mode >= RoomBuilder.eMode.Prepare_First && this.m_Mode < RoomBuilder.eMode.Main;
		}

		// Token: 0x06001DC7 RID: 7623 RVA: 0x0009F1AC File Offset: 0x0009D5AC
		public int GetActiveObjectNum(int floorid)
		{
			return this.m_FloorObject[floorid].GetObjectListNum();
		}

		// Token: 0x06001DC8 RID: 7624 RVA: 0x0009F1BB File Offset: 0x0009D5BB
		public IRoomObjectControll GetActiveObjectAt(int floorid, int findex)
		{
			return this.m_FloorObject[floorid].GetIndexToHandle(findex);
		}

		// Token: 0x06001DC9 RID: 7625 RVA: 0x0009F1CC File Offset: 0x0009D5CC
		public IRoomObjectControll GetDummyObject()
		{
			int count = this.m_ActiveRoomObjHndls.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_ActiveRoomObjHndls[i].ManageID == -1L)
				{
					return this.m_ActiveRoomObjHndls[i];
				}
			}
			return null;
		}

		// Token: 0x06001DCA RID: 7626 RVA: 0x0009F220 File Offset: 0x0009D620
		public void UpdateMain()
		{
			if (this.m_NewPlacementRoomObj != null)
			{
				if (!this.m_NewPlacementRoomObj.IsPreparing())
				{
					this.m_SelectingObj = this.m_NewPlacementRoomObj;
					this.MoveSelectingObj(this.m_SelectingObj, this.m_SelectingObj.BlockData.PosX, this.m_SelectingObj.BlockData.PosY, 0, (this.m_SelectingObj.ObjCategory != eRoomObjectCategory.WallDecoration) ? RoomBuilder.eEditObject.Floor : RoomBuilder.eEditObject.Wall);
					this.RequestEditMode(true);
					this.m_NewPlacementRoomObj = null;
				}
				return;
			}
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			if (info.m_bAvailable && !this.m_EventMode && !UIUtility.CheckPointOverUI(info.m_StartPos))
			{
				IRoomObjectControll roomObjectControll = null;
				switch (info.m_TouchPhase)
				{
				case TouchPhase.Began:
				{
					bool flag = false;
					Ray ray = this.m_Camera.ScreenPointToRay(info.m_StartPos);
					this.m_ObjTouch.m_NowTouch = (this.m_ObjTouch.m_TouchStart = info.m_StartPos);
					RaycastHit raycastHit;
					if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
					{
						"RoomObject"
					}) | LayerMask.GetMask(new string[]
					{
						"Character"
					})))
					{
						RoomHitConnect component = raycastHit.transform.gameObject.GetComponent<RoomHitConnect>();
						if (component != null)
						{
							this.m_ObjTouch.m_Grid = this.GetHitGridInfo(raycastHit.collider);
							roomObjectControll = component.m_Link;
							RoomUtility.IsPlacementObject(ref this.m_EditObjState, roomObjectControll.ObjCategory, roomObjectControll.FloorID);
							switch (this.m_EditObjState.m_State)
							{
							case RoomBuilder.eEditObject.Non:
								roomObjectControll = null;
								break;
							case RoomBuilder.eEditObject.Floor:
							case RoomBuilder.eEditObject.Wall:
							case RoomBuilder.eEditObject.Msgboard:
								this.m_MainRayLayerMask = LayerMask.GetMask(new string[]
								{
									"RoomObject"
								});
								flag = true;
								break;
							case RoomBuilder.eEditObject.Chara:
								flag = true;
								this.m_MainRayLayerMask = LayerMask.GetMask(new string[]
								{
									"Character"
								});
								break;
							}
						}
					}
					if (flag)
					{
						if (this.m_SelectingObj == null)
						{
							if (roomObjectControll != null)
							{
								this.m_SelectingObj = roomObjectControll;
								this.m_SelectingObj.TouchAction(eTouchStep.Begin, this.m_ObjTouch);
							}
						}
						else if (roomObjectControll != null)
						{
							if (this.m_SelectingObj != roomObjectControll)
							{
								this.m_SelectingObj.SetSeleting(false, false);
								this.m_SelectingObj = roomObjectControll;
								this.m_SelectingObj.TouchAction(eTouchStep.Begin, this.m_ObjTouch);
								this.OnUnSelectObject.Call();
							}
						}
						else
						{
							this.OnUnSelectObject.Call();
							this.m_SelectingObj.SetSeleting(false, false);
							this.m_SelectingObj = null;
						}
					}
					else
					{
						if (this.m_SelectingObj != null)
						{
							this.m_SelectingObj.SetSeleting(false, false);
							this.m_SelectingObj = null;
						}
						this.OnUnSelectObject.Call();
					}
					break;
				}
				case TouchPhase.Moved:
				case TouchPhase.Stationary:
					if (this.m_SelectingObj != null)
					{
						Ray ray = this.m_Camera.ScreenPointToRay(info.m_RawPos);
						this.m_ObjTouch.m_NowTouch = info.m_RawPos;
						RaycastHit raycastHit;
						if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, this.m_MainRayLayerMask))
						{
							if (!this.m_SelectingObj.TouchAction(eTouchStep.Drag, this.m_ObjTouch))
							{
								this.m_SelectingObj.TouchAction(eTouchStep.Cancel, this.m_ObjTouch);
								this.m_SelectingObj.SetSeleting(false, false);
								this.OnUnSelectObject.Call();
								this.m_SelectingObj = null;
							}
						}
						else if (this.m_SelectingObj.TouchAction(eTouchStep.RayCheck, this.m_ObjTouch))
						{
							this.m_SelectingObj.SetSeleting(false, false);
							this.OnUnSelectObject.Call();
							this.m_SelectingObj = null;
						}
					}
					break;
				default:
					if (this.m_SelectingObj != null && this.m_SelectingObj.TouchAction(eTouchStep.End, this.m_ObjTouch))
					{
						if (this.m_MainRayLayerMask != LayerMask.GetMask(new string[]
						{
							"Character"
						}))
						{
							if (this.m_FieldPartyInSchedule)
							{
								this.m_SelectingObj.SetSeleting(true, false);
								this.m_SaveDataOnEditStart.m_Dir = this.m_SelectingObj.GetDir();
								this.m_SaveDataOnEditStart.m_GridX = this.m_SelectingObj.BlockData.PosX;
								this.m_SaveDataOnEditStart.m_GridY = this.m_SelectingObj.BlockData.PosY;
								this.OnSelectObject.Call(this.m_SelectingObj, this.m_EditObjState.m_MenuType);
							}
						}
						else
						{
							this.OnUnSelectObject.Call();
						}
					}
					break;
				}
			}
		}

		// Token: 0x06001DCB RID: 7627 RVA: 0x0009F70C File Offset: 0x0009DB0C
		public void RequestPutAway()
		{
			if (this.m_SelectingObj == null)
			{
				Debug.LogWarningFormat("RequestPutAway() is failed. SelectingObj:{0}", new object[]
				{
					this.m_SelectingObj
				});
				return;
			}
			this.m_SelectingObj.UpGridState(this.m_GridStatus[0], 1);
			this.m_SelectingObj.AbortLinkEventObj();
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
			manageIDToUserRoomData.RemovePlacement(this.m_SelectingObj.ManageID);
			MissionManager.UnlockAction(eXlsMissionSeg.Room, eXlsMissionRoomFuncType.ObjectRemove);
			UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(this.m_SelectingObj.ManageID);
			if (manageIDToUserRoomObjData != null)
			{
				manageIDToUserRoomObjData.SetFreeLinkMngID(this.m_SelectingObj.ManageID);
			}
			if (this.m_SelectingObj.ManageID != -1L)
			{
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(this.m_UserRoomManageID);
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.DelPlacement, this.m_SelectingObj.ManageID, 0, null);
				roomComBuildScript.PlaySend();
			}
			this.RemoveObject(this.m_SelectingObj);
			this.m_SelectingObj = null;
		}

		// Token: 0x06001DCC RID: 7628 RVA: 0x0009F804 File Offset: 0x0009DC04
		public void RequestDummyPlacement(eRoomObjectCategory fobjcategory, int objID)
		{
			if (this.m_NewPlacementRoomObj != null)
			{
				return;
			}
			IVector4 vector;
			IVector2 vector2;
			this.m_FloorObject[0].GetMoveArea(out vector, out vector2, fobjcategory == eRoomObjectCategory.WallDecoration);
			int gridX = vector2.x / 2;
			int gridY = vector2.y / 2;
			if (fobjcategory == eRoomObjectCategory.WallDecoration)
			{
				gridY = vector.w;
			}
			this.m_NewPlacementRoomObj = this.InstantiateObject(RoomObjectListUtil.CategoryIDToAccessKey(fobjcategory, objID), CharacterDefine.eDir.L, gridX, gridY, 0, -1L);
		}

		// Token: 0x06001DCD RID: 7629 RVA: 0x0009F874 File Offset: 0x0009DC74
		public void RequestObjectToPlacement(eRoomObjectCategory fobjcategory, int objID)
		{
			if (this.m_NewPlacementRoomObj != null)
			{
				return;
			}
			IVector4 vector;
			IVector2 vector2;
			this.m_FloorObject[0].GetMoveArea(out vector, out vector2, fobjcategory == eRoomObjectCategory.WallDecoration);
			int gridX = vector2.x / 2;
			int gridY = vector2.y / 2;
			if (fobjcategory == eRoomObjectCategory.WallDecoration)
			{
				gridY = vector.w;
			}
			UserRoomObjectData accessIDToUserRoomObjData = UserRoomUtil.GetAccessIDToUserRoomObjData(RoomObjectListUtil.CategoryIDToAccessKey(fobjcategory, objID));
			if (accessIDToUserRoomObjData != null)
			{
				this.m_NewPlacementRoomObj = this.InstantiateObject(RoomObjectListUtil.CategoryIDToAccessKey(fobjcategory, objID), CharacterDefine.eDir.L, gridX, gridY, 0, accessIDToUserRoomObjData.GetFreeLinkMngID(true));
			}
		}

		// Token: 0x06001DCE RID: 7630 RVA: 0x0009F900 File Offset: 0x0009DD00
		public void RequestBuyObject(eRoomObjectCategory objCategory, int objID, int fnum, bool fnewplace, IFldNetComModule.CallBack pcallback)
		{
			if (this.m_NewPlacementRoomObj != null)
			{
				Debug.LogWarningFormat("RequestBuyObject() is failed. m_NewRoomObj:{0}", new object[]
				{
					this.m_NewPlacementRoomObj
				});
				return;
			}
			RoomComBuildScript roomComBuildScript = new RoomComBuildScript(this.m_UserRoomManageID);
			roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.BuyObj, (long)RoomObjectListUtil.CategoryIDToAccessKey(objCategory, objID), fnum, new IFldNetComModule.CallBack(this.CallbackNewPlaceObject));
			roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.CallEvt, 0L, 0, pcallback);
			roomComBuildScript.PlaySend();
		}

		// Token: 0x06001DCF RID: 7631 RVA: 0x0009F974 File Offset: 0x0009DD74
		private void CallbackNewPlaceObject(IFldNetComModule pmodule)
		{
			RoomComBuildScript roomComBuildScript = pmodule as RoomComBuildScript;
			this.LinkPlaceObjectToManageID(roomComBuildScript.m_ManageID);
		}

		// Token: 0x06001DD0 RID: 7632 RVA: 0x0009F994 File Offset: 0x0009DD94
		public void LinkPlaceObjectToManageID(long flinkmanageid)
		{
			IRoomObjectControll dummyObject = this.GetDummyObject();
			if (dummyObject != null)
			{
				UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(flinkmanageid);
				if (manageIDToUserRoomObjData != null)
				{
					manageIDToUserRoomObjData.LockLinkMngID(flinkmanageid);
					dummyObject.SetManageID(flinkmanageid);
				}
			}
		}

		// Token: 0x06001DD1 RID: 7633 RVA: 0x0009F9D0 File Offset: 0x0009DDD0
		public void RequestSellObject(eRoomObjectCategory objCategory, int objID, IFldNetComModule.CallBack pcallback)
		{
			long num = -1L;
			UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(objCategory, objID);
			if (userRoomObjData != null)
			{
				num = userRoomObjData.GetFreeLinkMngID(false);
			}
			if (num != -1L)
			{
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(this.m_UserRoomManageID);
				roomComBuildScript.SetManageID(num);
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.SaleObject, 0L, 0, null);
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.CallEvt, 0L, 0, pcallback);
				roomComBuildScript.PlaySend();
			}
			else
			{
				pcallback(null);
			}
		}

		// Token: 0x06001DD2 RID: 7634 RVA: 0x0009FA3C File Offset: 0x0009DE3C
		public void RequestEditMode(bool isEditNew)
		{
			if (this.m_Mode != RoomBuilder.eMode.Main)
			{
				return;
			}
			if (this.m_SelectingObj == null)
			{
				return;
			}
			RoomUtility.IsPlacementObject(ref this.m_SelectCategory, this.m_SelectingObj.ObjCategory, 0);
			this.m_FloorObject[this.m_SelectingObj.FloorID].SetGridView(this.m_SelectCategory.m_State, true);
			this.m_SelectingObj.AbortLinkEventObj();
			this.m_Mode = RoomBuilder.eMode.Main_Edit;
			this.m_IsEditNew = isEditNew;
			int posX = this.m_SelectingObj.BlockData.PosX;
			int posY = this.m_SelectingObj.BlockData.PosY;
			this.m_SelectingObj.SetGridPosition(posX, posY, 0);
			if (!isEditNew)
			{
				this.m_SelectingObj.UpGridState(this.m_GridStatus[this.m_SelectingObj.FloorID], 1);
			}
			bool fhitcolor = !this.m_GridStatus[this.m_SelectingObj.FloorID].IsAnyGridStatus(posX, posY, this.m_SelectingObj.BlockData.SizeX, this.m_SelectingObj.BlockData.SizeY, this.m_SelectingObj.ObjCategory, this.m_SelectingObj.ObjGroupKey);
			this.m_SelectingObj.CreateHitAreaModel(fhitcolor);
			this.m_IsDragging = false;
			RoomUtility.IsPlacementObject(ref this.m_EditObjState, this.m_SelectingObj.ObjCategory, this.m_SelectingObj.FloorID);
			switch (this.m_EditObjState.m_State)
			{
			case RoomBuilder.eEditObject.Non:
				this.m_EditMaskLayer = 0;
				break;
			case RoomBuilder.eEditObject.Floor:
				this.m_EditMaskLayer = LayerMask.GetMask(new string[]
				{
					"OneFloor"
				});
				break;
			case RoomBuilder.eEditObject.Wall:
				this.m_EditMaskLayer = LayerMask.GetMask(new string[]
				{
					"OneWall"
				});
				break;
			case RoomBuilder.eEditObject.Msgboard:
				this.m_EditMaskLayer = LayerMask.GetMask(new string[]
				{
					"OneFloor"
				});
				break;
			case RoomBuilder.eEditObject.Chara:
				this.m_EditMaskLayer = 0;
				break;
			}
		}

		// Token: 0x06001DD3 RID: 7635 RVA: 0x0009FC3C File Offset: 0x0009E03C
		public void RequestCancelEditMode()
		{
			this.m_FloorObject[0].SetGridView(this.m_SelectCategory.m_State, false);
			if (this.m_Mode != RoomBuilder.eMode.Main_Edit)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. mode:{0}", new object[]
				{
					this.m_Mode
				});
				return;
			}
			if (this.m_SelectingObj == null)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. SelectingObj:{0}", new object[]
				{
					this.m_SelectingObj
				});
				return;
			}
			this.m_Mode = RoomBuilder.eMode.Main;
			this.m_SelectingObj.DestroyHitAreaModel();
			if (this.m_IsEditNew)
			{
				if (this.m_SelectingObj.ManageID != -1L)
				{
					UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(this.m_SelectingObj.ManageID);
					if (manageIDToUserRoomObjData != null)
					{
						manageIDToUserRoomObjData.SetFreeLinkMngID(this.m_SelectingObj.ManageID);
					}
				}
				this.RemoveObject(this.m_SelectingObj);
			}
			else
			{
				this.m_SelectingObj.SetDir(this.m_SaveDataOnEditStart.m_Dir);
				this.m_SelectingObj.SetGridPosition(this.m_SaveDataOnEditStart.m_GridX, this.m_SaveDataOnEditStart.m_GridY, 0);
				this.m_SelectingObj.UpGridState(this.m_GridStatus[0], -1);
				this.m_CharaManager.AbortAllCharaObjEventMode(this.m_SelectingObj.BlockData.Pos, this.m_SelectingObj.BlockData.Size);
				this.m_SelectingObj.SetSeleting(false, false);
			}
			this.m_SelectingObj = null;
		}

		// Token: 0x06001DD4 RID: 7636 RVA: 0x0009FDB4 File Offset: 0x0009E1B4
		public void RequestCancelSelectObject()
		{
			if (this.m_Mode == RoomBuilder.eMode.Main_Edit)
			{
				this.RequestCancelEditMode();
			}
			else
			{
				if (this.m_SelectingObj != null)
				{
					this.m_SelectingObj.SetSeleting(false, false);
				}
				this.m_Mode = RoomBuilder.eMode.Main;
			}
			this.m_SelectingObj = null;
		}

		// Token: 0x06001DD5 RID: 7637 RVA: 0x0009FE08 File Offset: 0x0009E208
		public void RequestCancelSelectMode()
		{
			if (this.m_Mode == RoomBuilder.eMode.Main_Edit)
			{
				this.RequestCancelEditMode();
			}
			else
			{
				if (this.m_SelectingObj != null)
				{
					this.m_SelectingObj.SetDir(this.m_SaveDataOnEditStart.m_Dir);
					this.m_SelectingObj.SetGridPosition(this.m_SaveDataOnEditStart.m_GridX, this.m_SaveDataOnEditStart.m_GridY, 0);
					this.m_SelectingObj.UpGridState(this.m_GridStatus[0], -1);
					this.m_SelectingObj.SetSeleting(false, false);
				}
				this.m_Mode = RoomBuilder.eMode.Main;
			}
			this.m_SelectingObj = null;
		}

		// Token: 0x06001DD6 RID: 7638 RVA: 0x0009FEAC File Offset: 0x0009E2AC
		public void RequestFlip()
		{
			if (this.m_Mode != RoomBuilder.eMode.Main_Edit)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. mode:{0}", new object[]
				{
					this.m_Mode
				});
				return;
			}
			if (this.m_SelectingObj == null)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. SelectingObj:{0}", new object[]
				{
					this.m_SelectingObj
				});
				return;
			}
			this.m_SelectingObj.FlipDir();
			this.MoveSelectingObj(this.m_SelectingObj, this.m_SelectingObj.BlockData.PosX, this.m_SelectingObj.BlockData.PosY, this.m_SelectingObj.FloorID, (this.m_SelectingObj.ObjCategory != eRoomObjectCategory.WallDecoration) ? RoomBuilder.eEditObject.Floor : RoomBuilder.eEditObject.Wall);
		}

		// Token: 0x06001DD7 RID: 7639 RVA: 0x0009FF6C File Offset: 0x0009E36C
		public void RequestPlacement()
		{
			if (this.m_Mode != RoomBuilder.eMode.Main_Edit)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. mode:{0}", new object[]
				{
					this.m_Mode
				});
				return;
			}
			if (this.m_SelectingObj == null)
			{
				Debug.LogWarningFormat("RequestCancelEditMode() is failed. SelectingObj:{0}", new object[]
				{
					this.m_SelectingObj
				});
				return;
			}
			this.m_FloorObject[0].SetGridView(this.m_SelectCategory.m_State, false);
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
			UserRoomData.PlacementData placementManageID = manageIDToUserRoomData.GetPlacementManageID(this.m_SelectingObj.ManageID);
			if (placementManageID != null)
			{
				placementManageID.m_X = this.m_SelectingObj.BlockData.PosX;
				placementManageID.m_Y = this.m_SelectingObj.BlockData.PosY;
				placementManageID.m_Dir = this.m_SelectingObj.GetDir();
			}
			else
			{
				manageIDToUserRoomData.AddPlacement(new UserRoomData.PlacementData(this.m_SelectingObj.BlockData.PosX, this.m_SelectingObj.BlockData.PosY, this.m_SelectingObj.GetDir(), this.m_SelectingObj.ManageID, 0, RoomObjectListUtil.CategoryIDToAccessKey(this.m_SelectingObj.ObjCategory, this.m_SelectingObj.ObjID)));
			}
			this.m_SelectingObj.SetGridPosition(this.m_SelectingObj.BlockData.PosX, this.m_SelectingObj.BlockData.PosY, 0);
			this.m_SelectingObj.UpGridState(this.m_GridStatus[0], -1);
			this.m_SelectingObj.SetSeleting(false, false);
			this.m_SelectingObj.DestroyHitAreaModel();
			this.m_SelectingObj = null;
			this.m_CharaManager.ResetState();
			this.m_Mode = RoomBuilder.eMode.Main;
			MissionManager.UnlockAction(eXlsMissionSeg.Room, eXlsMissionRoomFuncType.ObjectSet);
			RoomComBuildScript roomComBuildScript = new RoomComBuildScript(this.m_UserRoomManageID);
			roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
			roomComBuildScript.PlaySend();
		}

		// Token: 0x06001DD8 RID: 7640 RVA: 0x000A014C File Offset: 0x0009E54C
		public int RequestPutAll()
		{
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
			int count = this.m_ActiveRoomObjHndls.Count;
			if (count != 0)
			{
				MissionManager.UnlockAction(eXlsMissionSeg.Room, eXlsMissionRoomFuncType.ObjectRemove);
			}
			int num = 0;
			int num2 = 0;
			int num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjRemainNum();
			int roomObjectLimit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit;
			for (int i = 0; i < count; i++)
			{
				IRoomObjectControll roomObjectControll = this.m_ActiveRoomObjHndls[num];
				if (roomObjectControll.FloorID == 0)
				{
					if (num3 < roomObjectLimit)
					{
						roomObjectControll.AbortLinkEventObj();
						manageIDToUserRoomData.RemovePlacement(roomObjectControll.ManageID);
						UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(roomObjectControll.ManageID);
						if (manageIDToUserRoomObjData != null)
						{
							manageIDToUserRoomObjData.SetFreeLinkMngID(roomObjectControll.ManageID);
						}
						if (this.m_GridStatus != null && this.m_GridStatus[roomObjectControll.FloorID] != null)
						{
							roomObjectControll.UpGridState(this.m_GridStatus[roomObjectControll.FloorID], 1);
						}
						this.RemoveObject(roomObjectControll);
						num2++;
						num3++;
					}
				}
				else
				{
					num++;
				}
			}
			this.m_Mode = RoomBuilder.eMode.Main;
			this.m_SelectingObj = null;
			RoomComBuildScript roomComBuildScript = new RoomComBuildScript(this.m_UserRoomManageID);
			roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
			roomComBuildScript.PlaySend();
			return num2;
		}

		// Token: 0x06001DD9 RID: 7641 RVA: 0x000A02A0 File Offset: 0x0009E6A0
		private IVector3 GetHitGridInfo(Collider pcol)
		{
			RoomHitObject component = pcol.gameObject.GetComponent<RoomHitObject>();
			IVector3 result;
			if (component != null)
			{
				result.x = component.m_GridX;
				result.y = component.m_GridY;
				result.z = component.m_CategoryID;
			}
			else
			{
				result.x = -1;
				result.y = -1;
				result.z = -1;
			}
			return result;
		}

		// Token: 0x06001DDA RID: 7642 RVA: 0x000A030C File Offset: 0x0009E70C
		public bool GetFloorPoint(ref IVector3 pout, Vector2 ftouchpos, Vector3 fobjpos, int floorid, RoomBuilder.eEditObject fobjtype = RoomBuilder.eEditObject.Floor)
		{
			Ray ray = this.m_Camera.ScreenPointToRay(ftouchpos);
			RaycastHit raycastHit;
			if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
			{
				"OneFloor"
			})))
			{
				IVector3 hitGridInfo = this.GetHitGridInfo(raycastHit.collider);
				if (floorid == hitGridInfo.z)
				{
					pout.x = hitGridInfo.x;
					pout.y = hitGridInfo.y;
					pout.z = hitGridInfo.z;
					return true;
				}
			}
			else
			{
				IVector3 hitGridInfo;
				hitGridInfo.x = 0;
				hitGridInfo.y = 0;
				hitGridInfo.z = 0;
				if (this.GetSelectObjToFloorCrossPoint(ref hitGridInfo, fobjpos, ray.origin, fobjtype))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001DDB RID: 7643 RVA: 0x000A03DC File Offset: 0x0009E7DC
		private bool GetSelectObjToFloorCrossPoint(ref IVector3 pout, Vector3 fobjpos, Vector3 fpos, RoomBuilder.eEditObject fobjtype)
		{
			Vector3 fstartpt = fobjpos;
			Vector3 fendpt = fpos;
			fstartpt.z = 0f;
			fendpt.z = 0f;
			return this.m_FloorObject[0].CheckCrossLine(ref pout, fstartpt, fendpt, fobjtype == RoomBuilder.eEditObject.Wall);
		}

		// Token: 0x06001DDC RID: 7644 RVA: 0x000A0424 File Offset: 0x0009E824
		public void UpdateMainEdit()
		{
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			if (!this.m_EventMode)
			{
				if (!this.m_IsDragging)
				{
					if (info.m_bAvailable)
					{
						if (info.m_TouchPhase == TouchPhase.Began && !UIUtility.CheckPointOverUI(info.m_StartPos))
						{
							IRoomObjectControll roomObjectControll = null;
							Ray ray = this.m_Camera.ScreenPointToRay(info.m_StartPos);
							RaycastHit raycastHit;
							if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
							{
								"RoomObject"
							})))
							{
								RoomHitConnect component = raycastHit.transform.gameObject.GetComponent<RoomHitConnect>();
								if (component != null)
								{
									RoomBuilder.EditObjectState editObjectState = default(RoomBuilder.EditObjectState);
									roomObjectControll = component.m_Link;
									RoomUtility.IsPlacementObject(ref editObjectState, roomObjectControll.ObjCategory, roomObjectControll.FloorID);
									switch (editObjectState.m_State)
									{
									case RoomBuilder.eEditObject.Non:
									case RoomBuilder.eEditObject.Chara:
										roomObjectControll = null;
										break;
									}
								}
							}
							if (this.m_SelectingObj == roomObjectControll)
							{
								this.m_IsDragging = true;
								this.OnObjectDragStart.Call();
							}
							else if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, this.m_EditMaskLayer))
							{
								IVector3 hitGridInfo = this.GetHitGridInfo(raycastHit.collider);
								if (hitGridInfo.z == this.m_SelectingObj.FloorID)
								{
									this.m_EditingTouchGridX = hitGridInfo.x;
									this.m_EditingTouchGridY = hitGridInfo.y;
									this.m_EditingTouchFloor = hitGridInfo.z;
									this.m_IsEditingTouchGrid = true;
								}
							}
						}
						else if (info.m_TouchPhase == TouchPhase.Ended && this.m_IsEditingTouchGrid)
						{
							this.m_IsEditingTouchGrid = false;
							Ray ray2 = Camera.main.ScreenPointToRay(info.m_StartPos);
							RaycastHit raycastHit2;
							if (Physics.Raycast(ray2.origin, ray2.direction, out raycastHit2, float.PositiveInfinity, this.m_EditMaskLayer))
							{
								IVector3 hitGridInfo = this.GetHitGridInfo(raycastHit2.collider);
								if (this.m_EditingTouchGridX == hitGridInfo.x && this.m_EditingTouchGridY == hitGridInfo.y && this.m_EditingTouchFloor == hitGridInfo.z)
								{
									this.MoveSelectingObj(this.m_SelectingObj, hitGridInfo.x, hitGridInfo.y, hitGridInfo.z, this.m_EditObjState.m_State);
								}
							}
						}
					}
				}
				else if (info.m_bAvailable)
				{
					TouchPhase touchPhase = info.m_TouchPhase;
					if (touchPhase != TouchPhase.Stationary && touchPhase != TouchPhase.Moved)
					{
						this.m_IsDragging = false;
						this.OnObjectDragEnd.Call();
					}
					else
					{
						Ray ray3 = this.m_Camera.ScreenPointToRay(info.m_RawPos);
						RaycastHit raycastHit3;
						if (Physics.Raycast(ray3.origin, ray3.direction, out raycastHit3, float.PositiveInfinity, this.m_EditMaskLayer))
						{
							IVector3 hitGridInfo = this.GetHitGridInfo(raycastHit3.collider);
							if (this.m_EditingTouchFloor == hitGridInfo.z)
							{
								this.MoveSelectingObj(this.m_SelectingObj, hitGridInfo.x, hitGridInfo.y, hitGridInfo.z, this.m_EditObjState.m_State);
							}
						}
						else
						{
							IVector3 hitGridInfo;
							hitGridInfo.x = 0;
							hitGridInfo.y = 0;
							hitGridInfo.z = 0;
							if (this.GetSelectObjToFloorCrossPoint(ref hitGridInfo, this.m_SelectingObj.CacheTransform.position, ray3.origin, this.m_EditObjState.m_State))
							{
								this.MoveSelectingObj(this.m_SelectingObj, hitGridInfo.x, hitGridInfo.y, hitGridInfo.z, this.m_EditObjState.m_State);
							}
						}
					}
				}
				else
				{
					this.m_IsDragging = false;
					this.OnObjectDragEnd.Call();
				}
			}
		}

		// Token: 0x06001DDD RID: 7645 RVA: 0x000A0814 File Offset: 0x0009EC14
		private void SortRoom()
		{
			for (int i = this.m_FloorObject.Length - 1; i >= 0; i--)
			{
				int forder = -(i * 4000);
				this.m_FloorObject[i].BuildSortingOrder(i, forder);
			}
			if (this.m_Mode == RoomBuilder.eMode.Main_Edit && this.m_SelectingObj != null)
			{
				this.m_SelectingObj.SetSortingOrder(-900f, 10000);
			}
		}

		// Token: 0x06001DDE RID: 7646 RVA: 0x000A088C File Offset: 0x0009EC8C
		public IVector3 GetFloorSize(int fno)
		{
			return this.m_FloorObject[fno].GetMoveSize();
		}

		// Token: 0x06001DDF RID: 7647 RVA: 0x000A089B File Offset: 0x0009EC9B
		public Vector2 GetFloorPos(int fno)
		{
			return this.m_FloorObject[fno].GetOffsetPos();
		}

		// Token: 0x06001DE0 RID: 7648 RVA: 0x000A08AA File Offset: 0x0009ECAA
		public RoomGridState GetGridStatus(int floorno)
		{
			return this.m_GridStatus[floorno];
		}

		// Token: 0x06001DE1 RID: 7649 RVA: 0x000A08B4 File Offset: 0x0009ECB4
		public void AttachSortObject(IRoomObjectControll pobj, int fcompkey)
		{
			this.m_FloorObject[pobj.FloorID].AttachSortObject(pobj, fcompkey);
		}

		// Token: 0x06001DE2 RID: 7650 RVA: 0x000A08CA File Offset: 0x0009ECCA
		public void DetachSortObject(IRoomObjectControll pobj)
		{
			this.m_FloorObject[pobj.FloorID].DetachObject(pobj);
		}

		// Token: 0x06001DE3 RID: 7651 RVA: 0x000A08E0 File Offset: 0x0009ECE0
		private bool IsActiveObjSetUp()
		{
			int count = this.m_ActiveRoomObjHndls.Count;
			bool flag = true;
			for (int i = 0; i < count; i++)
			{
				flag &= this.m_ActiveRoomObjHndls[i].IsAvailable();
			}
			return flag;
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x000A0924 File Offset: 0x0009ED24
		public IRoomObjectControll InstantiateObject(int fobjresid, CharacterDefine.eDir fdir, int gridX, int gridY, int floorID, long fmanageid)
		{
			IRoomObjectControll roomObjectControll = null;
			GameObject nextObjectInCache = this.m_ObjectCache.GetNextObjectInCache();
			if (nextObjectInCache != null)
			{
				nextObjectInCache.SetActive(true);
				RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(fobjresid);
				roomObjectControll = RoomObjectControllCreate.SetupHandle(nextObjectInCache, this, (eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID, fdir, gridX, gridY, floorID, fmanageid);
				roomObjectControll.SetGridPosition(gridX, gridY, floorID);
				this.m_ActiveRoomObjHndls.Add(roomObjectControll);
				this.m_FloorObject[floorID].AttachSortObject(roomObjectControll, roomObjectControll.GetSortCompKey());
				roomObjectControll.Prepare();
			}
			return roomObjectControll;
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x000A09AC File Offset: 0x0009EDAC
		public void RemoveObject(IRoomObjectControll roomObj)
		{
			if (roomObj == null)
			{
				return;
			}
			this.m_ActiveRoomObjHndls.Remove(roomObj);
			this.m_FloorObject[roomObj.FloorID].DetachObject(roomObj);
			roomObj.Destroy();
			this.m_ObjectCache.BackObject(roomObj.gameObject);
		}

		// Token: 0x06001DE6 RID: 7654 RVA: 0x000A0A00 File Offset: 0x0009EE00
		private void MoveSelectingObj(IRoomObjectControll ptarget, int gridX, int gridY, int floorID, RoomBuilder.eEditObject fobjtype = RoomBuilder.eEditObject.Non)
		{
			IVector4 vector;
			IVector2 vector2;
			this.m_FloorObject[floorID].GetMoveArea(out vector, out vector2, fobjtype == RoomBuilder.eEditObject.Wall);
			IVector2 posToSize = ptarget.GetPosToSize(gridX, gridY);
			if (fobjtype == RoomBuilder.eEditObject.Wall)
			{
				if (gridY >= vector.w)
				{
					if (gridX + posToSize.x > vector.z)
					{
						gridX = vector.z - posToSize.x;
					}
					else if (gridX < vector.x)
					{
						gridX = vector.x;
					}
					if (gridY + posToSize.y > vector2.y)
					{
						gridY = vector2.y - posToSize.y;
					}
					else if (gridY < vector.w)
					{
						gridY = vector.w;
					}
				}
				else
				{
					if (gridY + posToSize.y > vector.w)
					{
						gridY = vector.w - posToSize.y;
					}
					else if (gridY < vector.y)
					{
						gridY = vector.y;
					}
					if (gridX + posToSize.x > vector2.x)
					{
						gridX = vector2.x - posToSize.x;
					}
					else if (gridX < vector.z)
					{
						gridX = vector.z;
					}
				}
			}
			else
			{
				if (gridX + posToSize.x > vector2.x)
				{
					gridX = vector2.x - posToSize.x;
				}
				else if (gridX < vector.x)
				{
					gridX = vector.x;
				}
				if (gridY + posToSize.y > vector2.y)
				{
					gridY = vector2.y - posToSize.y;
				}
				else if (gridY < vector.y)
				{
					gridY = vector.y;
				}
			}
			if (ptarget.BlockData.PosX != gridX || ptarget.BlockData.PosY != gridY)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ROOM_ACT_WALK, 1f, 0, -1, -1);
			}
			ptarget.SetGridPosition(gridX, gridY, floorID);
			if (!this.m_GridStatus[floorID].IsAnyGridStatus(gridX, gridY, posToSize.x, posToSize.y, ptarget.ObjCategory, ptarget.ObjGroupKey))
			{
				ptarget.SetSeleting(true, false);
				this.OnSelectingObjectCanPlacement.Call();
			}
			else
			{
				ptarget.SetSeleting(true, true);
				this.OnSelectingObjectCanNotPlacement.Call();
			}
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x000A0C75 File Offset: 0x0009F075
		public GameObject GetHUDObject()
		{
			return this.m_CharaHudObject;
		}

		// Token: 0x06001DE8 RID: 7656 RVA: 0x000A0C7D File Offset: 0x0009F07D
		public IRoomFloorManager GetFloorManager(int findex)
		{
			return this.m_FloorObject[findex];
		}

		// Token: 0x06001DE9 RID: 7657 RVA: 0x000A0C87 File Offset: 0x0009F087
		public void ChangeBG(IRoomObjectControll pobj)
		{
			this.m_Bg.Destroy();
			UnityEngine.Object.Destroy(this.m_Bg.gameObject);
			this.m_Bg = pobj;
		}

		// Token: 0x06001DEA RID: 7658 RVA: 0x000A0CAB File Offset: 0x0009F0AB
		public void SendActionToMain(IMainComCommand pcmd)
		{
			if (!this.m_DebugSceneActionCut)
			{
				this.OnEventSend(pcmd);
			}
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x000A0CC4 File Offset: 0x0009F0C4
		public void DestroyObjectQue(IRoomObjectControll pobj)
		{
			RoomComCharaRelease prequest = new RoomComCharaRelease(pobj);
			this.RequestEvent(prequest);
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x000A0CE0 File Offset: 0x0009F0E0
		private void FlushEventRequest()
		{
			if (this.m_EventQue != null)
			{
				int comCommandNum = this.m_EventQue.GetComCommandNum();
				for (int i = 0; i < comCommandNum; i++)
				{
					IRoomEventCommand comCommand = this.m_EventQue.GetComCommand(i);
					comCommand.CalcRequest(this);
				}
				this.m_EventQue.Flush();
			}
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x000A0D36 File Offset: 0x0009F136
		public void RequestEvent(IRoomEventCommand prequest)
		{
			this.m_EventQue.PushComCommand(prequest);
		}

		// Token: 0x06001DEE RID: 7662 RVA: 0x000A0D44 File Offset: 0x0009F144
		public void RefreshChara(UserFieldCharaData.UpFieldCharaData[] charaMngTable)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			FieldMapManager.MemberComCheck memberComCheck = new FieldMapManager.MemberComCheck();
			userDataMng.UserFieldChara.CheckManageCharaID(charaMngTable, new UserFieldCharaData.CharaInOutEvent(memberComCheck.CallbackEvent));
			memberComCheck.SendComCheck();
			this.m_CharaManager.RefreshCharaCall();
		}

		// Token: 0x06001DEF RID: 7663 RVA: 0x000A0D8B File Offset: 0x0009F18B
		public void StopCharaManagerAction(bool fstop)
		{
			this.m_CharaManager.StopAction(fstop);
			this.m_EventMode = fstop;
		}

		// Token: 0x06001DF0 RID: 7664 RVA: 0x000A0DA0 File Offset: 0x0009F1A0
		public void ObjRequestToEditMode(IRoomObjectControll pobj)
		{
			if (this.m_FieldPartyInSchedule)
			{
				this.m_SaveDataOnEditStart.m_Dir = pobj.GetDir();
				this.m_SaveDataOnEditStart.m_GridX = pobj.BlockData.PosX;
				this.m_SaveDataOnEditStart.m_GridY = pobj.BlockData.PosY;
				this.m_SelectingObj = pobj;
				RoomComEditMode pcmd = new RoomComEditMode();
				this.SendActionToMain(pcmd);
			}
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x000A0E09 File Offset: 0x0009F209
		public void SetCameraToDebugMode(bool fmode)
		{
			this.m_RoomCamera.SetIsControllable(!fmode);
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x000A0E1C File Offset: 0x0009F21C
		private void CheckTimeBuilding()
		{
			int hour = ScheduleTimeUtil.GetManageUnixTime().Hour;
			if (this.m_TimeCheckHour != hour)
			{
				byte b = 0;
				if (hour >= 18 || hour < 6)
				{
					b = 1;
				}
				if (b != this.m_TimeCategory)
				{
					this.m_TimeCategory = b;
					UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(this.m_UserRoomManageID);
					UserRoomData.PlacementData categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Background);
					if (categoryToPlacementData != null)
					{
						RoomComBGChange prequest = new RoomComBGChange(categoryToPlacementData.m_ManageID, categoryToPlacementData.m_ObjectID);
						this.RequestEvent(prequest);
					}
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
				}
				this.m_TimeCheckHour = hour;
			}
		}

		// Token: 0x040023FD RID: 9213
		[SerializeField]
		private GameObject m_CharaHudObject;

		// Token: 0x040023FE RID: 9214
		private RoomEventQue m_EventQue;

		// Token: 0x040023FF RID: 9215
		private IRoomObjectControll m_Bg;

		// Token: 0x04002400 RID: 9216
		[SerializeField]
		public RoomGameCamera m_RoomCamera;

		// Token: 0x04002401 RID: 9217
		[SerializeField]
		public List<RoomFloorCateory> m_FloorCategory;

		// Token: 0x04002402 RID: 9218
		public IRoomFloorManager[] m_FloorObject;

		// Token: 0x04002403 RID: 9219
		[SerializeField]
		private RoomCacheObjectData m_ObjectCache;

		// Token: 0x04002404 RID: 9220
		private List<IRoomObjectControll> m_ActiveRoomObjHndls = new List<IRoomObjectControll>();

		// Token: 0x04002405 RID: 9221
		private RoomGridState[] m_GridStatus;

		// Token: 0x04002406 RID: 9222
		private long m_UserRoomManageID;

		// Token: 0x04002407 RID: 9223
		private IRoomObjectControll m_SelectingObj;

		// Token: 0x04002408 RID: 9224
		private RoomBuilder.EditObjectState m_SelectCategory;

		// Token: 0x04002409 RID: 9225
		private IRoomObjectControll m_NewPlacementRoomObj;

		// Token: 0x0400240A RID: 9226
		private bool m_IsEditNew;

		// Token: 0x0400240B RID: 9227
		private RoomBuilder.SaveDataOnEditStart m_SaveDataOnEditStart;

		// Token: 0x0400240C RID: 9228
		[HideInInspector]
		public IRoomCharaManager m_CharaManager;

		// Token: 0x0400240D RID: 9229
		private bool m_IsDragging;

		// Token: 0x0400240E RID: 9230
		private bool m_IsBuilt;

		// Token: 0x0400240F RID: 9231
		private bool m_EventMode;

		// Token: 0x04002411 RID: 9233
		public GameObject m_DebugRoomData;

		// Token: 0x04002412 RID: 9234
		public bool m_DebugSceneActionCut;

		// Token: 0x04002413 RID: 9235
		private byte m_TimeCategory;

		// Token: 0x04002414 RID: 9236
		public int m_TimeCheckHour;

		// Token: 0x04002415 RID: 9237
		private bool m_ActivePlay;

		// Token: 0x04002416 RID: 9238
		private RoomMain m_Owner;

		// Token: 0x04002417 RID: 9239
		private long m_SelectPlayerId;

		// Token: 0x04002418 RID: 9240
		private bool m_FieldPartyInSchedule;

		// Token: 0x04002419 RID: 9241
		private RoomBuilder.eMode m_Mode = RoomBuilder.eMode.None;

		// Token: 0x0400241A RID: 9242
		private Transform m_Transform;

		// Token: 0x0400241B RID: 9243
		private Camera m_Camera;

		// Token: 0x04002422 RID: 9250
		private int m_MainRayLayerMask;

		// Token: 0x04002423 RID: 9251
		private IRoomObjectControll.ObjTouchState m_ObjTouch;

		// Token: 0x04002424 RID: 9252
		private Vector2 m_TouchStart;

		// Token: 0x04002425 RID: 9253
		private int m_EditingTouchGridX;

		// Token: 0x04002426 RID: 9254
		private int m_EditingTouchGridY;

		// Token: 0x04002427 RID: 9255
		private int m_EditingTouchFloor;

		// Token: 0x04002428 RID: 9256
		private bool m_IsEditingTouchGrid;

		// Token: 0x04002429 RID: 9257
		private int m_EditMaskLayer;

		// Token: 0x0400242A RID: 9258
		private RoomBuilder.EditObjectState m_EditObjState;

		// Token: 0x020005E2 RID: 1506
		private struct SaveDataOnEditStart
		{
			// Token: 0x0400242B RID: 9259
			public int m_GridX;

			// Token: 0x0400242C RID: 9260
			public int m_GridY;

			// Token: 0x0400242D RID: 9261
			public CharacterDefine.eDir m_Dir;
		}

		// Token: 0x020005E3 RID: 1507
		public enum eMode
		{
			// Token: 0x0400242F RID: 9263
			None = -1,
			// Token: 0x04002430 RID: 9264
			Prepare_First,
			// Token: 0x04002431 RID: 9265
			Prepare_Bg,
			// Token: 0x04002432 RID: 9266
			Prepare_Floor,
			// Token: 0x04002433 RID: 9267
			Prepare_Floor_Wait,
			// Token: 0x04002434 RID: 9268
			Prepare_Placements,
			// Token: 0x04002435 RID: 9269
			Prepare_Placements_Wait,
			// Token: 0x04002436 RID: 9270
			Prepare_Charas,
			// Token: 0x04002437 RID: 9271
			Prepare_Charas_Wait,
			// Token: 0x04002438 RID: 9272
			Main,
			// Token: 0x04002439 RID: 9273
			Main_Edit,
			// Token: 0x0400243A RID: 9274
			Num
		}

		// Token: 0x020005E4 RID: 1508
		public enum eEditObject
		{
			// Token: 0x0400243C RID: 9276
			Non,
			// Token: 0x0400243D RID: 9277
			Floor,
			// Token: 0x0400243E RID: 9278
			Wall,
			// Token: 0x0400243F RID: 9279
			Msgboard,
			// Token: 0x04002440 RID: 9280
			Chara
		}

		// Token: 0x020005E5 RID: 1509
		public struct EditObjectState
		{
			// Token: 0x04002441 RID: 9281
			public RoomBuilder.eEditObject m_State;

			// Token: 0x04002442 RID: 9282
			public bool m_MenuType;
		}
	}
}
