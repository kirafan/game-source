﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000B41 RID: 2881
	public sealed class UserDataManager
	{
		// Token: 0x06003CF5 RID: 15605 RVA: 0x00135EF8 File Offset: 0x001342F8
		public UserDataManager()
		{
			this.UserData = new UserData();
			this.UserCharaDatas = new List<UserCharacterData>();
			this.UserNamedDatas = new Dictionary<eCharaNamedType, UserNamedData>();
			this.UserWeaponDatas = new List<UserWeaponData>();
			this.UserItemDatas = new List<UserItemData>();
			this.UserMasterOrbDatas = new List<UserMasterOrbData>();
			this.UserBattlePartyDatas = new List<UserBattlePartyData>();
			this.UserSupportPartyDatas = new List<UserSupportPartyData>();
			this.UserTownData = new UserTownData();
			this.UserTownObjDatas = new List<UserTownObjectData>();
			this.UserRoomListData = new UserRoomListData();
			this.UserRoomObjDatas = new List<UserRoomObjectData>();
			this.UserFieldChara = new UserFieldCharaData();
			this.UserAdvData = new UserAdvData();
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06003CF6 RID: 15606 RVA: 0x00135FA5 File Offset: 0x001343A5
		// (set) Token: 0x06003CF7 RID: 15607 RVA: 0x00135FAD File Offset: 0x001343AD
		public UserData UserData { get; private set; }

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06003CF8 RID: 15608 RVA: 0x00135FB6 File Offset: 0x001343B6
		// (set) Token: 0x06003CF9 RID: 15609 RVA: 0x00135FBE File Offset: 0x001343BE
		public List<UserCharacterData> UserCharaDatas { get; private set; }

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06003CFA RID: 15610 RVA: 0x00135FC7 File Offset: 0x001343C7
		// (set) Token: 0x06003CFB RID: 15611 RVA: 0x00135FCF File Offset: 0x001343CF
		public Dictionary<eCharaNamedType, UserNamedData> UserNamedDatas { get; private set; }

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06003CFC RID: 15612 RVA: 0x00135FD8 File Offset: 0x001343D8
		// (set) Token: 0x06003CFD RID: 15613 RVA: 0x00135FE0 File Offset: 0x001343E0
		public List<UserWeaponData> UserWeaponDatas { get; private set; }

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06003CFE RID: 15614 RVA: 0x00135FE9 File Offset: 0x001343E9
		// (set) Token: 0x06003CFF RID: 15615 RVA: 0x00135FF1 File Offset: 0x001343F1
		public List<UserItemData> UserItemDatas { get; private set; }

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06003D00 RID: 15616 RVA: 0x00135FFA File Offset: 0x001343FA
		// (set) Token: 0x06003D01 RID: 15617 RVA: 0x00136002 File Offset: 0x00134402
		public List<UserMasterOrbData> UserMasterOrbDatas { get; private set; }

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x06003D02 RID: 15618 RVA: 0x0013600B File Offset: 0x0013440B
		// (set) Token: 0x06003D03 RID: 15619 RVA: 0x00136013 File Offset: 0x00134413
		public List<UserBattlePartyData> UserBattlePartyDatas { get; private set; }

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x06003D04 RID: 15620 RVA: 0x0013601C File Offset: 0x0013441C
		// (set) Token: 0x06003D05 RID: 15621 RVA: 0x00136024 File Offset: 0x00134424
		public List<UserSupportPartyData> UserSupportPartyDatas { get; private set; }

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06003D06 RID: 15622 RVA: 0x0013602D File Offset: 0x0013442D
		// (set) Token: 0x06003D07 RID: 15623 RVA: 0x00136035 File Offset: 0x00134435
		public UserTownData UserTownData { get; private set; }

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x06003D08 RID: 15624 RVA: 0x0013603E File Offset: 0x0013443E
		// (set) Token: 0x06003D09 RID: 15625 RVA: 0x00136046 File Offset: 0x00134446
		public List<UserTownObjectData> UserTownObjDatas { get; private set; }

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x06003D0A RID: 15626 RVA: 0x0013604F File Offset: 0x0013444F
		// (set) Token: 0x06003D0B RID: 15627 RVA: 0x00136057 File Offset: 0x00134457
		public UserRoomListData UserRoomListData { get; private set; }

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06003D0C RID: 15628 RVA: 0x00136060 File Offset: 0x00134460
		// (set) Token: 0x06003D0D RID: 15629 RVA: 0x00136068 File Offset: 0x00134468
		public List<UserRoomObjectData> UserRoomObjDatas { get; private set; }

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06003D0E RID: 15630 RVA: 0x00136071 File Offset: 0x00134471
		// (set) Token: 0x06003D0F RID: 15631 RVA: 0x00136079 File Offset: 0x00134479
		public UserFieldCharaData UserFieldChara { get; private set; }

		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06003D10 RID: 15632 RVA: 0x00136082 File Offset: 0x00134482
		// (set) Token: 0x06003D11 RID: 15633 RVA: 0x0013608A File Offset: 0x0013448A
		public UserAdvData UserAdvData { get; private set; }

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06003D12 RID: 15634 RVA: 0x00136093 File Offset: 0x00134493
		// (set) Token: 0x06003D13 RID: 15635 RVA: 0x0013609B File Offset: 0x0013449B
		public int NoticeFriendCount { get; set; }

		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06003D14 RID: 15636 RVA: 0x001360A4 File Offset: 0x001344A4
		// (set) Token: 0x06003D15 RID: 15637 RVA: 0x001360AC File Offset: 0x001344AC
		public int NoticePresentCount { get; set; }

		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06003D16 RID: 15638 RVA: 0x001360B5 File Offset: 0x001344B5
		// (set) Token: 0x06003D17 RID: 15639 RVA: 0x001360BD File Offset: 0x001344BD
		public int NoticeMissionCount { get; set; }

		// Token: 0x06003D18 RID: 15640 RVA: 0x001360C6 File Offset: 0x001344C6
		public void Update()
		{
			if (this.UserData != null)
			{
				this.UserData.Update();
			}
		}

		// Token: 0x06003D19 RID: 15641 RVA: 0x001360E0 File Offset: 0x001344E0
		public bool IsAvailable()
		{
			return this.UserData != null && this.UserCharaDatas != null && this.UserNamedDatas != null && this.UserWeaponDatas != null && this.UserItemDatas != null && this.UserMasterOrbDatas != null && this.UserBattlePartyDatas != null && this.UserSupportPartyDatas != null && this.UserTownData != null && this.UserTownObjDatas != null && this.UserRoomObjDatas != null && this.UserFieldChara != null && this.UserAdvData != null;
		}

		// Token: 0x06003D1A RID: 15642 RVA: 0x00136198 File Offset: 0x00134598
		public UserCharacterData GetUserCharaData(long mngID)
		{
			for (int i = 0; i < this.UserCharaDatas.Count; i++)
			{
				if (this.UserCharaDatas[i].MngID == mngID)
				{
					return this.UserCharaDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D1B RID: 15643 RVA: 0x001361E8 File Offset: 0x001345E8
		public List<UserCharacterData> GetUserCharaDatas(List<long> mngIDs)
		{
			List<UserCharacterData> list = new List<UserCharacterData>();
			for (int i = 0; i < mngIDs.Count; i++)
			{
				UserCharacterData userCharaData = this.GetUserCharaData(mngIDs[i]);
				if (userCharaData != null)
				{
					list.Add(userCharaData);
				}
			}
			return list;
		}

		// Token: 0x06003D1C RID: 15644 RVA: 0x0013622E File Offset: 0x0013462E
		public UserNamedData GetUserNamedData(eCharaNamedType namedType)
		{
			if (!this.UserNamedDatas.ContainsKey(namedType))
			{
				return null;
			}
			return this.UserNamedDatas[namedType];
		}

		// Token: 0x06003D1D RID: 15645 RVA: 0x00136250 File Offset: 0x00134650
		public UserWeaponData GetUserWpnData(long wpnMngID)
		{
			for (int i = 0; i < this.UserWeaponDatas.Count; i++)
			{
				if (this.UserWeaponDatas[i].MngID == wpnMngID)
				{
					return this.UserWeaponDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D1E RID: 15646 RVA: 0x001362A0 File Offset: 0x001346A0
		public int GetItemNum(int itemID)
		{
			for (int i = 0; i < this.UserItemDatas.Count; i++)
			{
				if (this.UserItemDatas[i].ItemID == itemID)
				{
					return this.UserItemDatas[i].ItemNum;
				}
			}
			return 0;
		}

		// Token: 0x06003D1F RID: 15647 RVA: 0x001362F4 File Offset: 0x001346F4
		public UserMasterOrbData GetUserMasterOrbData(long masterOrbMngID)
		{
			for (int i = 0; i < this.UserMasterOrbDatas.Count; i++)
			{
				if (this.UserMasterOrbDatas[i].MngID == masterOrbMngID)
				{
					return this.UserMasterOrbDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D20 RID: 15648 RVA: 0x00136344 File Offset: 0x00134744
		public UserMasterOrbData GetEquipUserMasterOrbData()
		{
			for (int i = 0; i < this.UserMasterOrbDatas.Count; i++)
			{
				if (this.UserMasterOrbDatas[i].IsEquip)
				{
					return this.UserMasterOrbDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D21 RID: 15649 RVA: 0x00136394 File Offset: 0x00134794
		public long GetEquipUserMasterOrbMngID()
		{
			for (int i = 0; i < this.UserMasterOrbDatas.Count; i++)
			{
				if (this.UserMasterOrbDatas[i].IsEquip)
				{
					return this.UserMasterOrbDatas[i].MngID;
				}
			}
			return -1L;
		}

		// Token: 0x06003D22 RID: 15650 RVA: 0x001363E8 File Offset: 0x001347E8
		public UserBattlePartyData GetUserBattlePartyData(long partyMngID)
		{
			for (int i = 0; i < this.UserBattlePartyDatas.Count; i++)
			{
				if (this.UserBattlePartyDatas[i].MngID == partyMngID)
				{
					return this.UserBattlePartyDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D23 RID: 15651 RVA: 0x00136438 File Offset: 0x00134838
		public UserSupportPartyData GetUserSupportPartyData(long partyMngID)
		{
			for (int i = 0; i < this.UserSupportPartyDatas.Count; i++)
			{
				if (this.UserSupportPartyDatas[i].MngID == partyMngID)
				{
					return this.UserSupportPartyDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D24 RID: 15652 RVA: 0x00136488 File Offset: 0x00134888
		public int GetActivateSupportPartyIndex()
		{
			for (int i = 0; i < this.UserSupportPartyDatas.Count; i++)
			{
				if (this.UserSupportPartyDatas[i] != null && this.UserSupportPartyDatas[i].IsActive)
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x06003D25 RID: 15653 RVA: 0x001364DC File Offset: 0x001348DC
		public UserRoomObjectData GetUserRoomObjData(eRoomObjectCategory objCategory, int objID)
		{
			for (int i = 0; i < this.UserRoomObjDatas.Count; i++)
			{
				if (this.UserRoomObjDatas[i].ObjCategory == objCategory && this.UserRoomObjDatas[i].ObjID == objID)
				{
					return this.UserRoomObjDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06003D26 RID: 15654 RVA: 0x00136544 File Offset: 0x00134944
		public int GetUserRoomObjHaveNum()
		{
			int num = 0;
			for (int i = 0; i < this.UserRoomObjDatas.Count; i++)
			{
				num += this.UserRoomObjDatas[i].HaveNum;
			}
			return num;
		}

		// Token: 0x06003D27 RID: 15655 RVA: 0x00136584 File Offset: 0x00134984
		public int GetUserRoomObjPlacementNum()
		{
			int num = 0;
			for (int i = 0; i < this.UserRoomObjDatas.Count; i++)
			{
				if (RoomUtility.IsPlacementCountObj(this.UserRoomObjDatas[i].ObjCategory))
				{
					num += this.UserRoomObjDatas[i].PlacementNum;
				}
			}
			return num - 5;
		}

		// Token: 0x06003D28 RID: 15656 RVA: 0x001365E8 File Offset: 0x001349E8
		public int GetUserRoomObjRemainNum()
		{
			int num = 0;
			for (int i = 0; i < this.UserRoomObjDatas.Count; i++)
			{
				num += this.UserRoomObjDatas[i].RemainNum;
			}
			return num;
		}
	}
}
