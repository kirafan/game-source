﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020000A8 RID: 168
	public class BattleCommandSolveResult
	{
		// Token: 0x060004D9 RID: 1241 RVA: 0x0001A282 File Offset: 0x00018682
		public BattleCommandSolveResult()
		{
			this.m_CharaResults = new Dictionary<CharacterHandler, BattleCommandSolveResult.CharaResult>();
			this.m_CharaDiedWithThisCommand = new Dictionary<CharacterHandler, bool>();
		}

		// Token: 0x060004DA RID: 1242 RVA: 0x0001A2A0 File Offset: 0x000186A0
		public void Clear()
		{
			this.m_CharaResults.Clear();
			this.m_CharaDiedWithThisCommand.Clear();
			this.m_IsCreatedCard = false;
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x0001A2BF File Offset: 0x000186BF
		public BattleCommandSolveResult.CharaResult AddCharaResult_Safe(CharacterHandler hndl)
		{
			if (!this.m_CharaResults.ContainsKey(hndl))
			{
				this.m_CharaResults.Add(hndl, new BattleCommandSolveResult.CharaResult());
			}
			return this.m_CharaResults[hndl];
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x0001A2EF File Offset: 0x000186EF
		public void AddCharaDiedWithThisCommand_Safe(CharacterHandler hndl)
		{
			if (!this.m_CharaDiedWithThisCommand.ContainsKey(hndl))
			{
				this.m_CharaDiedWithThisCommand.Add(hndl, true);
			}
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x0001A310 File Offset: 0x00018710
		public int GetTotalDamageHitNum()
		{
			int num = 0;
			foreach (BattleCommandSolveResult.CharaResult charaResult in this.m_CharaResults.Values)
			{
				num += charaResult.GetDamageHitDataNum();
			}
			return num;
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x0001A378 File Offset: 0x00018778
		public int GetTotalDamageHitValue()
		{
			int num = 0;
			foreach (BattleCommandSolveResult.CharaResult charaResult in this.m_CharaResults.Values)
			{
				num += charaResult.GetTotalDamageValue();
			}
			return num;
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x0001A3E0 File Offset: 0x000187E0
		public int GetTotalRecoverHitNum()
		{
			int num = 0;
			foreach (BattleCommandSolveResult.CharaResult charaResult in this.m_CharaResults.Values)
			{
				num += charaResult.GetRecoverHitDataNum();
			}
			return num;
		}

		// Token: 0x060004E0 RID: 1248 RVA: 0x0001A448 File Offset: 0x00018848
		public int GetTotalRecoverHitValue()
		{
			int num = 0;
			foreach (BattleCommandSolveResult.CharaResult charaResult in this.m_CharaResults.Values)
			{
				num += charaResult.GetTotalRecoverValue();
			}
			return num;
		}

		// Token: 0x04000322 RID: 802
		public Dictionary<CharacterHandler, BattleCommandSolveResult.CharaResult> m_CharaResults;

		// Token: 0x04000323 RID: 803
		public Dictionary<CharacterHandler, bool> m_CharaDiedWithThisCommand;

		// Token: 0x04000324 RID: 804
		public bool m_IsCreatedCard;

		// Token: 0x020000A9 RID: 169
		public enum eSpecialReason
		{
			// Token: 0x04000326 RID: 806
			BecameStateAbnormal,
			// Token: 0x04000327 RID: 807
			MissStateAbnormal,
			// Token: 0x04000328 RID: 808
			RecoverdStateAbnormal,
			// Token: 0x04000329 RID: 809
			RecoveryValueIsZeroBecauseUnhappy,
			// Token: 0x0400032A RID: 810
			Num
		}

		// Token: 0x020000AA RID: 170
		public class HitData
		{
			// Token: 0x060004E1 RID: 1249 RVA: 0x0001A4B0 File Offset: 0x000188B0
			public HitData(int value, bool isCritical, int registHit_Or_defaultHit_Or_weakHit, BattleDefine.eFluctuation elementFluctuation)
			{
				this.m_Value = value;
				this.m_IsCritical = isCritical;
				this.m_registHit_Or_defaultHit_Or_weakHit = registHit_Or_defaultHit_Or_weakHit;
				this.m_ElementFluctuation = elementFluctuation;
			}

			// Token: 0x1700004B RID: 75
			// (get) Token: 0x060004E2 RID: 1250 RVA: 0x0001A4D5 File Offset: 0x000188D5
			// (set) Token: 0x060004E3 RID: 1251 RVA: 0x0001A4DD File Offset: 0x000188DD
			public BattleDefine.eFluctuation m_ElementFluctuation { get; set; }

			// Token: 0x0400032B RID: 811
			public int m_Value;

			// Token: 0x0400032C RID: 812
			public bool m_IsCritical;

			// Token: 0x0400032D RID: 813
			public int m_registHit_Or_defaultHit_Or_weakHit;
		}

		// Token: 0x020000AB RID: 171
		public class CharaResult
		{
			// Token: 0x060004E4 RID: 1252 RVA: 0x0001A4E6 File Offset: 0x000188E6
			public CharaResult()
			{
				this.m_DamageHitDatas = new List<BattleCommandSolveResult.HitData>();
				this.m_RecoverHitDatas = new List<BattleCommandSolveResult.HitData>();
				this.m_IsSpecialReasonFlg = new bool[4];
			}

			// Token: 0x060004E5 RID: 1253 RVA: 0x0001A517 File Offset: 0x00018917
			public void Clear()
			{
				this.m_DamageHitDatas.Clear();
				this.m_RecoverHitDatas.Clear();
				this.m_IsSpecialReasonFlg = new bool[4];
				this.m_ReadDamageHitDataIndex = 0;
				this.m_ReadRecoverHitDataIndex = 0;
				this.m_AutoFacialID = -1;
			}

			// Token: 0x060004E6 RID: 1254 RVA: 0x0001A550 File Offset: 0x00018950
			public void AddDamageHitData(BattleCommandSolveResult.HitData hitData)
			{
				this.m_DamageHitDatas.Add(hitData);
			}

			// Token: 0x060004E7 RID: 1255 RVA: 0x0001A55E File Offset: 0x0001895E
			public int GetDamageHitDataNum()
			{
				return this.m_DamageHitDatas.Count;
			}

			// Token: 0x060004E8 RID: 1256 RVA: 0x0001A56B File Offset: 0x0001896B
			public BattleCommandSolveResult.HitData GetDamageHitDataAt(int index)
			{
				return this.m_DamageHitDatas[index];
			}

			// Token: 0x060004E9 RID: 1257 RVA: 0x0001A57C File Offset: 0x0001897C
			public BattleCommandSolveResult.HitData GetDamageHitDataNew()
			{
				if (this.m_DamageHitDatas.Count > (int)this.m_ReadDamageHitDataIndex)
				{
					BattleCommandSolveResult.HitData result = this.m_DamageHitDatas[(int)this.m_ReadDamageHitDataIndex];
					this.m_ReadDamageHitDataIndex += 1;
					return result;
				}
				return null;
			}

			// Token: 0x060004EA RID: 1258 RVA: 0x0001A5C4 File Offset: 0x000189C4
			public int GetTotalDamageValue()
			{
				int num = 0;
				for (int i = 0; i < this.m_DamageHitDatas.Count; i++)
				{
					num += this.m_DamageHitDatas[i].m_Value;
				}
				return num;
			}

			// Token: 0x060004EB RID: 1259 RVA: 0x0001A604 File Offset: 0x00018A04
			public void AddRecoverHitData(BattleCommandSolveResult.HitData hitData)
			{
				this.m_RecoverHitDatas.Add(hitData);
			}

			// Token: 0x060004EC RID: 1260 RVA: 0x0001A612 File Offset: 0x00018A12
			public int GetRecoverHitDataNum()
			{
				return this.m_RecoverHitDatas.Count;
			}

			// Token: 0x060004ED RID: 1261 RVA: 0x0001A61F File Offset: 0x00018A1F
			public BattleCommandSolveResult.HitData GetRecoverHitDataAt(int index)
			{
				return this.m_RecoverHitDatas[index];
			}

			// Token: 0x060004EE RID: 1262 RVA: 0x0001A630 File Offset: 0x00018A30
			public BattleCommandSolveResult.HitData GetRecoverHitDataNew()
			{
				if (this.m_RecoverHitDatas.Count > (int)this.m_ReadRecoverHitDataIndex)
				{
					BattleCommandSolveResult.HitData result = this.m_RecoverHitDatas[(int)this.m_ReadRecoverHitDataIndex];
					this.m_ReadRecoverHitDataIndex += 1;
					return result;
				}
				return null;
			}

			// Token: 0x060004EF RID: 1263 RVA: 0x0001A678 File Offset: 0x00018A78
			public int GetTotalRecoverValue()
			{
				int num = 0;
				for (int i = 0; i < this.m_RecoverHitDatas.Count; i++)
				{
					num += this.m_RecoverHitDatas[i].m_Value;
				}
				return num;
			}

			// Token: 0x060004F0 RID: 1264 RVA: 0x0001A6B8 File Offset: 0x00018AB8
			public void SetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason reason, bool flg)
			{
				this.m_IsSpecialReasonFlg[(int)reason] = flg;
			}

			// Token: 0x060004F1 RID: 1265 RVA: 0x0001A6C3 File Offset: 0x00018AC3
			public bool GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason reason)
			{
				return this.m_IsSpecialReasonFlg[(int)reason];
			}

			// Token: 0x060004F2 RID: 1266 RVA: 0x0001A6CD File Offset: 0x00018ACD
			public void SetAutoFacialID(int facialID)
			{
				this.m_AutoFacialID = facialID;
			}

			// Token: 0x060004F3 RID: 1267 RVA: 0x0001A6D6 File Offset: 0x00018AD6
			public int GetAutoFacialID()
			{
				return this.m_AutoFacialID;
			}

			// Token: 0x0400032F RID: 815
			private List<BattleCommandSolveResult.HitData> m_DamageHitDatas;

			// Token: 0x04000330 RID: 816
			private List<BattleCommandSolveResult.HitData> m_RecoverHitDatas;

			// Token: 0x04000331 RID: 817
			private bool[] m_IsSpecialReasonFlg;

			// Token: 0x04000332 RID: 818
			private ushort m_ReadDamageHitDataIndex;

			// Token: 0x04000333 RID: 819
			private ushort m_ReadRecoverHitDataIndex;

			// Token: 0x04000334 RID: 820
			private int m_AutoFacialID = -1;
		}
	}
}
