﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D4 RID: 1236
	public sealed class InputTouch
	{
		// Token: 0x0600185F RID: 6239 RVA: 0x0007EF40 File Offset: 0x0007D340
		private InputTouch()
		{
			this.THRESHOLD_SAMETOUCH_PIX = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.001f;
			this.THRESHOLD_SAMEDBLTOUCH_PIX = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.001f;
			this.THRESHOLD_FLICK = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * 0.0005f;
			this.THRESHOLD_SWITCH_HAND = (float)Screen.width * 0.25f;
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x06001860 RID: 6240 RVA: 0x0007F016 File Offset: 0x0007D416
		public static InputTouch Inst
		{
			get
			{
				return InputTouch.m_Instance;
			}
		}

		// Token: 0x06001861 RID: 6241 RVA: 0x0007F01D File Offset: 0x0007D41D
		public static void Create()
		{
			if (InputTouch.m_Instance == null)
			{
				InputTouch.m_Instance = new InputTouch();
			}
			InputTouch.m_Instance.Init();
		}

		// Token: 0x06001862 RID: 6242 RVA: 0x0007F040 File Offset: 0x0007D440
		public void Init()
		{
			for (int i = 0; i < 3; i++)
			{
				this.m_TouchInfo[i].m_bAvailable = false;
			}
		}

		// Token: 0x06001863 RID: 6243 RVA: 0x0007F074 File Offset: 0x0007D474
		public void Update()
		{
			int num = (3 >= Input.touchCount) ? Input.touchCount : 3;
			int i = 0;
			int num2 = 0;
			for (int j = 0; j < 3; j++)
			{
				if (this.m_DblTouchInfo[j].IsAvailable())
				{
					InputTouch.DOUBLE_TOUCH_INFO[] dblTouchInfo = this.m_DblTouchInfo;
					int num3 = j;
					dblTouchInfo[num3].m_Time = dblTouchInfo[num3].m_Time + Time.deltaTime;
					if (this.m_DblTouchInfo[j].m_Time >= 10f || this.m_DblTouchInfo[j].IsTrigger())
					{
						this.m_DblTouchInfo[j].Clear();
					}
				}
			}
			while (i < num)
			{
				Touch touch = Input.GetTouch(i);
				if (touch.phase != TouchPhase.Canceled)
				{
					Vector2 position = touch.position;
					int num4 = this.SearchTouchInfo(touch.fingerId);
					int num5 = this.SearchDoubleTouchInfo(touch.fingerId);
					TouchPhase phase = touch.phase;
					switch (touch.phase)
					{
					case TouchPhase.Began:
						num4 = this.GetEmptyTouchInfo();
						if (num4 >= 0)
						{
							this.m_TouchInfo[num4].m_FingerID = touch.fingerId;
							this.m_TouchInfo[num4].m_StartPos = position;
							this.m_TouchInfo[num4].m_CurrentPos = position;
							this.m_TouchInfo[num4].m_OldPos = position;
							this.m_TouchInfo[num4].m_DiffFromStart = Vector3.zero;
							this.m_TouchInfo[num4].m_DiffFromPrev = Vector3.zero;
							if (this.m_HandIdToIdx[1] == -1 && this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft))
							{
								this.m_HandIdToIdx[1] = num4;
								this.MakeHandTouchInfo(InputTouch.eHandID.eLeft, this.m_TouchInfo[num4], position);
							}
							else if (this.m_HandIdToIdx[0] == -1)
							{
								this.m_HandIdToIdx[0] = num4;
								this.MakeHandTouchInfo(InputTouch.eHandID.eRight, this.m_TouchInfo[num4], position);
							}
						}
						num5 = this.GetEmptyDoubleTouchInfo();
						if (num5 >= 0)
						{
							this.m_DblTouchInfo[num5].Start(touch.fingerId, position);
						}
						break;
					case TouchPhase.Moved:
					case TouchPhase.Stationary:
						if (num4 >= 0)
						{
							this.UpdateTouchInfo(ref this.m_TouchInfo[num4], position, false);
							if (!this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft) && this.m_HandIdToIdx[1] == num4)
							{
								if (Mathf.Abs(position.x - this.m_HandTouchInfo[1].m_CurrentPos.x) >= this.THRESHOLD_SWITCH_HAND)
								{
									if (this.IsThisTouchOnHand(position, InputTouch.eHandID.eRight) && this.m_HandIdToIdx[0] == -1)
									{
										this.m_HandIdToIdx[0] = num4;
										this.MakeHandTouchInfo(InputTouch.eHandID.eRight, this.m_TouchInfo[num4], position);
									}
									this.m_HandIdToIdx[1] = -1;
								}
							}
							else if (!this.IsThisTouchOnHand(position, InputTouch.eHandID.eRight) && this.m_HandIdToIdx[0] == num4 && Mathf.Abs(position.x - this.m_HandTouchInfo[0].m_CurrentPos.x) >= this.THRESHOLD_SWITCH_HAND)
							{
								if (this.IsThisTouchOnHand(position, InputTouch.eHandID.eLeft) && this.m_HandIdToIdx[1] == -1)
								{
									this.m_HandIdToIdx[1] = num4;
									this.MakeHandTouchInfo(InputTouch.eHandID.eLeft, this.m_TouchInfo[num4], position);
								}
								this.m_HandIdToIdx[0] = -1;
							}
							if (this.m_HandIdToIdx[1] == num4)
							{
								this.UpdateTouchInfo(ref this.m_HandTouchInfo[1], position, false);
							}
							else if (this.m_HandIdToIdx[0] == num4)
							{
								this.UpdateTouchInfo(ref this.m_HandTouchInfo[0], position, false);
							}
						}
						break;
					case TouchPhase.Ended:
						if (num4 >= 0)
						{
							this.UpdateTouchInfo(ref this.m_TouchInfo[num4], position, true);
							if (this.m_HandIdToIdx[1] == num4)
							{
								this.UpdateTouchInfo(ref this.m_HandTouchInfo[1], position, true);
							}
							else if (this.m_HandIdToIdx[0] == num4)
							{
								this.UpdateTouchInfo(ref this.m_HandTouchInfo[0], position, true);
							}
						}
						if (num5 >= 0)
						{
							this.UpdateTouchCnt(num5, position);
						}
						break;
					}
					if (num4 >= 0)
					{
						this.m_TouchInfo[num4].m_bAvailable = true;
						this.m_TouchInfo[num4].m_RawPos = position;
						this.m_TouchInfo[num4].m_TouchPhase = phase;
						num2 |= 1 << num4;
					}
				}
				i++;
			}
			this.m_AvailableTouchCnt = 0;
			for (int k = 0; k < 3; k++)
			{
				if (this.m_TouchInfo[k].m_bAvailable)
				{
					if ((num2 & 1 << k) == 0)
					{
						this.m_TouchInfo[k].m_bAvailable = false;
					}
					this.m_AvailableTouchCnt++;
				}
			}
			if (this.m_HandIdToIdx[1] != -1 && !this.m_TouchInfo[this.m_HandIdToIdx[1]].m_bAvailable)
			{
				this.m_HandIdToIdx[1] = -1;
				this.m_HandTouchInfo[1].m_bAvailable = false;
			}
			if (this.m_HandIdToIdx[0] != -1 && !this.m_TouchInfo[this.m_HandIdToIdx[0]].m_bAvailable)
			{
				this.m_HandIdToIdx[0] = -1;
				this.m_HandTouchInfo[0].m_bAvailable = false;
			}
		}

		// Token: 0x06001864 RID: 6244 RVA: 0x0007F63C File Offset: 0x0007DA3C
		private int SearchTouchInfo(int fingerID)
		{
			int num = 0;
			foreach (InputTouch.TOUCH_INFO touch_INFO in this.m_TouchInfo)
			{
				if (touch_INFO.m_FingerID == fingerID)
				{
					return num;
				}
				num++;
			}
			return -1;
		}

		// Token: 0x06001865 RID: 6245 RVA: 0x0007F688 File Offset: 0x0007DA88
		private int GetEmptyTouchInfo()
		{
			int num = 0;
			foreach (InputTouch.TOUCH_INFO touch_INFO in this.m_TouchInfo)
			{
				if (!touch_INFO.m_bAvailable)
				{
					return num;
				}
				num++;
			}
			return -1;
		}

		// Token: 0x06001866 RID: 6246 RVA: 0x0007F6D4 File Offset: 0x0007DAD4
		private int SearchDoubleTouchInfo(int fingerID)
		{
			int num = 0;
			foreach (InputTouch.DOUBLE_TOUCH_INFO double_TOUCH_INFO in this.m_DblTouchInfo)
			{
				if (double_TOUCH_INFO.m_FingerID == fingerID)
				{
					return num;
				}
				num++;
			}
			return -1;
		}

		// Token: 0x06001867 RID: 6247 RVA: 0x0007F720 File Offset: 0x0007DB20
		private int GetEmptyDoubleTouchInfo()
		{
			int num = 0;
			foreach (InputTouch.DOUBLE_TOUCH_INFO double_TOUCH_INFO in this.m_DblTouchInfo)
			{
				if (!double_TOUCH_INFO.IsAvailable())
				{
					return num;
				}
				num++;
			}
			return -1;
		}

		// Token: 0x06001868 RID: 6248 RVA: 0x0007F76C File Offset: 0x0007DB6C
		private void UpdateTouchCnt(int idx, Vector2 pos)
		{
			float num = Vector2.Distance(pos, this.m_DblTouchInfo[idx].m_StartPos);
			if (num <= this.THRESHOLD_SAMEDBLTOUCH_PIX)
			{
				InputTouch.DOUBLE_TOUCH_INFO[] dblTouchInfo = this.m_DblTouchInfo;
				dblTouchInfo[idx].m_TouchCnt = dblTouchInfo[idx].m_TouchCnt + 1;
			}
		}

		// Token: 0x06001869 RID: 6249 RVA: 0x0007F7B8 File Offset: 0x0007DBB8
		private bool IsThisTouchOnHand(Vector2 position, InputTouch.eHandID hand)
		{
			float num = (float)Screen.width / 2f;
			if (hand == InputTouch.eHandID.eLeft)
			{
				return position.x <= num;
			}
			return position.x > num;
		}

		// Token: 0x0600186A RID: 6250 RVA: 0x0007F7F4 File Offset: 0x0007DBF4
		private void MakeHandTouchInfo(InputTouch.eHandID hand, InputTouch.TOUCH_INFO ti, Vector2 curPos)
		{
			this.m_HandTouchInfo[(int)hand].m_bAvailable = ti.m_bAvailable;
			this.m_HandTouchInfo[(int)hand].m_StartPos = curPos;
			this.m_HandTouchInfo[(int)hand].m_CurrentPos = curPos;
			this.m_HandTouchInfo[(int)hand].m_OldPos = curPos;
			this.m_HandTouchInfo[(int)hand].m_DiffFromStart = Vector3.zero;
			this.m_HandTouchInfo[(int)hand].m_DiffFromPrev = Vector3.zero;
			this.m_HandTouchInfo[(int)hand].m_RawPos = ti.m_RawPos;
			this.m_HandTouchInfo[(int)hand].m_TouchPhase = TouchPhase.Began;
			this.m_HandTouchInfo[(int)hand].m_bTrigger = false;
		}

		// Token: 0x0600186B RID: 6251 RVA: 0x0007F8B8 File Offset: 0x0007DCB8
		private void UpdateTouchInfo(ref InputTouch.TOUCH_INFO ti, Vector2 curPos, bool bPhaseEnd)
		{
			ti.m_OldPos = ti.m_CurrentPos;
			Vector2 vector = curPos - ti.m_StartPos;
			float magnitude = vector.magnitude;
			if (magnitude >= this.THRESHOLD_SAMETOUCH_PIX)
			{
				ti.m_DiffFromStart.z = magnitude;
				ti.m_DiffFromStart.x = vector.x / magnitude;
				ti.m_DiffFromStart.y = vector.y / magnitude;
			}
			else
			{
				ti.m_DiffFromStart = Vector3.zero;
				if (bPhaseEnd)
				{
					ti.m_bTrigger = true;
				}
			}
			Vector2 vector2 = curPos - ti.m_OldPos;
			float magnitude2 = vector2.magnitude;
			if (magnitude2 >= this.THRESHOLD_SAMETOUCH_PIX)
			{
				ti.m_DiffFromPrev.z = magnitude2;
				ti.m_DiffFromPrev.x = vector2.x / magnitude2;
				ti.m_DiffFromPrev.y = vector2.y / magnitude2;
			}
			else
			{
				ti.m_DiffFromPrev = Vector3.zero;
			}
			ti.m_CurrentPos = ti.m_DiffFromStart;
			ti.m_CurrentPos = ti.m_CurrentPos * ti.m_DiffFromStart.z + ti.m_StartPos;
			ti.m_RawPos = curPos;
		}

		// Token: 0x0600186C RID: 6252 RVA: 0x0007F9E7 File Offset: 0x0007DDE7
		public bool IsAvailable(int idx)
		{
			return this.m_TouchInfo[idx].m_bAvailable;
		}

		// Token: 0x0600186D RID: 6253 RVA: 0x0007F9FA File Offset: 0x0007DDFA
		public bool IsAvailable(InputTouch.eHandID tid)
		{
			return this.m_HandIdToIdx[(int)tid] != -1;
		}

		// Token: 0x0600186E RID: 6254 RVA: 0x0007FA11 File Offset: 0x0007DE11
		public InputTouch.TOUCH_INFO GetInfo(InputTouch.eHandID tid)
		{
			return this.m_HandTouchInfo[(int)tid];
		}

		// Token: 0x0600186F RID: 6255 RVA: 0x0007FA24 File Offset: 0x0007DE24
		public InputTouch.TOUCH_INFO GetInfo(int idx)
		{
			return this.m_TouchInfo[idx];
		}

		// Token: 0x06001870 RID: 6256 RVA: 0x0007FA37 File Offset: 0x0007DE37
		public int GetAvailableTouchCnt()
		{
			return this.m_AvailableTouchCnt;
		}

		// Token: 0x06001871 RID: 6257 RVA: 0x0007FA40 File Offset: 0x0007DE40
		public bool DetectTriggerOfSingleTouch(out Vector2 pos, int idx)
		{
			if (this.IsAvailable(idx))
			{
				InputTouch.TOUCH_INFO info = this.GetInfo(idx);
				if (info.m_TouchPhase == TouchPhase.Ended)
				{
					float magnitude = (info.m_RawPos - info.m_StartPos).magnitude;
					if (magnitude < this.THRESHOLD_SAMETOUCH_PIX * Screen.dpi)
					{
						pos = info.m_RawPos;
						if (pos.x >= 0f && pos.x <= (float)Screen.width && pos.y >= 0f && pos.y <= (float)Screen.height)
						{
							return true;
						}
					}
				}
			}
			pos = Vector2.zero;
			return false;
		}

		// Token: 0x06001872 RID: 6258 RVA: 0x0007FAF9 File Offset: 0x0007DEF9
		public bool IsTriggerOfDoubleTouch(int idx)
		{
			return this.m_DblTouchInfo[idx].IsAvailable() && this.m_DblTouchInfo[idx].IsTrigger();
		}

		// Token: 0x06001873 RID: 6259 RVA: 0x0007FB25 File Offset: 0x0007DF25
		public bool DectectTriggerOfDoubleTouch(out Vector2 pos, int idx)
		{
			if (this.IsTriggerOfDoubleTouch(idx))
			{
				pos = this.m_DblTouchInfo[idx].m_StartPos;
				return true;
			}
			pos = Vector2.zero;
			return false;
		}

		// Token: 0x06001874 RID: 6260 RVA: 0x0007FB58 File Offset: 0x0007DF58
		public bool GetFlick(out Vector3 dir, out Vector3 start, out Vector3 end, int idx)
		{
			if (this.IsAvailable(idx))
			{
				InputTouch.TOUCH_INFO info = this.GetInfo(idx);
				Vector2 vector = info.m_CurrentPos - info.m_OldPos;
				float magnitude = vector.magnitude;
				if (magnitude >= this.THRESHOLD_FLICK)
				{
					dir.x = vector.x;
					dir.y = vector.y;
					dir.z = magnitude;
					start = info.m_OldPos;
					end = info.m_CurrentPos;
					return true;
				}
			}
			start = Vector3.zero;
			end = Vector3.zero;
			dir = Vector3.zero;
			return false;
		}

		// Token: 0x06001875 RID: 6261 RVA: 0x0007FC0C File Offset: 0x0007E00C
		public bool GetFlick(out Vector3 dir, int idx)
		{
			Vector3 vector;
			Vector3 vector2;
			return this.GetFlick(out dir, out vector, out vector2, idx);
		}

		// Token: 0x04001F1F RID: 7967
		private static InputTouch m_Instance;

		// Token: 0x04001F20 RID: 7968
		private const int INVALIDATE_IDX = -1;

		// Token: 0x04001F21 RID: 7969
		public const int TOUCH_MAX = 3;

		// Token: 0x04001F22 RID: 7970
		private const float THRESHOLD_SAMETOUCH_RATIO = 0.001f;

		// Token: 0x04001F23 RID: 7971
		private const float THRESHOLD_SAMEDBLTOUCH_RATIO = 0.001f;

		// Token: 0x04001F24 RID: 7972
		private const float THRESHOLD_FLICK_RATIO = 0.0005f;

		// Token: 0x04001F25 RID: 7973
		private const float THRESHOLD_DBLTOUCH_FRAME = 10f;

		// Token: 0x04001F26 RID: 7974
		private const float THRESHOLD_SWITCH_HAND_COEFF = 0.25f;

		// Token: 0x04001F27 RID: 7975
		public float THRESHOLD_SAMETOUCH_PIX;

		// Token: 0x04001F28 RID: 7976
		public float THRESHOLD_SAMEDBLTOUCH_PIX;

		// Token: 0x04001F29 RID: 7977
		private float THRESHOLD_SWITCH_HAND;

		// Token: 0x04001F2A RID: 7978
		public float THRESHOLD_FLICK;

		// Token: 0x04001F2B RID: 7979
		private InputTouch.TOUCH_INFO[] m_TouchInfo = new InputTouch.TOUCH_INFO[3];

		// Token: 0x04001F2C RID: 7980
		private InputTouch.DOUBLE_TOUCH_INFO[] m_DblTouchInfo = new InputTouch.DOUBLE_TOUCH_INFO[3];

		// Token: 0x04001F2D RID: 7981
		private InputTouch.TOUCH_INFO[] m_HandTouchInfo = new InputTouch.TOUCH_INFO[2];

		// Token: 0x04001F2E RID: 7982
		private int[] m_HandIdToIdx = new int[2];

		// Token: 0x04001F2F RID: 7983
		private int m_AvailableTouchCnt;

		// Token: 0x020004D5 RID: 1237
		public enum eHandID
		{
			// Token: 0x04001F31 RID: 7985
			eRight,
			// Token: 0x04001F32 RID: 7986
			eLeft,
			// Token: 0x04001F33 RID: 7987
			eMax
		}

		// Token: 0x020004D6 RID: 1238
		public struct TOUCH_INFO
		{
			// Token: 0x04001F34 RID: 7988
			public Vector2 m_StartPos;

			// Token: 0x04001F35 RID: 7989
			public Vector2 m_CurrentPos;

			// Token: 0x04001F36 RID: 7990
			public Vector2 m_OldPos;

			// Token: 0x04001F37 RID: 7991
			public Vector3 m_DiffFromStart;

			// Token: 0x04001F38 RID: 7992
			public Vector3 m_DiffFromPrev;

			// Token: 0x04001F39 RID: 7993
			public Vector2 m_RawPos;

			// Token: 0x04001F3A RID: 7994
			public TouchPhase m_TouchPhase;

			// Token: 0x04001F3B RID: 7995
			public int m_FingerID;

			// Token: 0x04001F3C RID: 7996
			public bool m_bAvailable;

			// Token: 0x04001F3D RID: 7997
			public bool m_bTrigger;
		}

		// Token: 0x020004D7 RID: 1239
		private struct DOUBLE_TOUCH_INFO
		{
			// Token: 0x06001877 RID: 6263 RVA: 0x0007FC27 File Offset: 0x0007E027
			public void Clear()
			{
				this.m_FingerID = -1;
			}

			// Token: 0x06001878 RID: 6264 RVA: 0x0007FC30 File Offset: 0x0007E030
			public void Start(int fingerID, Vector2 startPos)
			{
				this.m_StartPos = startPos;
				this.m_FingerID = fingerID;
				this.m_Time = 0f;
				this.m_TouchCnt = 0;
			}

			// Token: 0x06001879 RID: 6265 RVA: 0x0007FC52 File Offset: 0x0007E052
			public bool IsAvailable()
			{
				return this.m_FingerID != -1;
			}

			// Token: 0x0600187A RID: 6266 RVA: 0x0007FC60 File Offset: 0x0007E060
			public bool IsTrigger()
			{
				return this.m_TouchCnt >= 2;
			}

			// Token: 0x04001F3E RID: 7998
			public Vector2 m_StartPos;

			// Token: 0x04001F3F RID: 7999
			public int m_FingerID;

			// Token: 0x04001F40 RID: 8000
			public float m_Time;

			// Token: 0x04001F41 RID: 8001
			public int m_TouchCnt;
		}
	}
}
