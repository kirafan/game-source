﻿using System;

namespace Star
{
	// Token: 0x020001E7 RID: 487
	[Serializable]
	public struct QuestListDB_Param
	{
		// Token: 0x04000BC4 RID: 3012
		public int m_ID;

		// Token: 0x04000BC5 RID: 3013
		public string m_QuestName;

		// Token: 0x04000BC6 RID: 3014
		public int m_BgID;

		// Token: 0x04000BC7 RID: 3015
		public string m_BGMCueName;

		// Token: 0x04000BC8 RID: 3016
		public string m_LastWaveBGMCueName;

		// Token: 0x04000BC9 RID: 3017
		public byte m_IsWarning;

		// Token: 0x04000BCA RID: 3018
		public byte m_IsHideElement;

		// Token: 0x04000BCB RID: 3019
		public int m_Stamina;

		// Token: 0x04000BCC RID: 3020
		public int m_RewardMoney;

		// Token: 0x04000BCD RID: 3021
		public int m_RewardUserExp;

		// Token: 0x04000BCE RID: 3022
		public int m_RewardMasterOrbExp;

		// Token: 0x04000BCF RID: 3023
		public int m_RewardCharaExp;

		// Token: 0x04000BD0 RID: 3024
		public int m_RewardFriendshipExp;

		// Token: 0x04000BD1 RID: 3025
		public byte m_IsRetryLimit;

		// Token: 0x04000BD2 RID: 3026
		public byte m_IsLoserBattle;

		// Token: 0x04000BD3 RID: 3027
		public int[] m_WaveIDs;

		// Token: 0x04000BD4 RID: 3028
		public byte m_IsAdvOnly;

		// Token: 0x04000BD5 RID: 3029
		public int m_AdvID_Prev;

		// Token: 0x04000BD6 RID: 3030
		public int m_AdvID_After;

		// Token: 0x04000BD7 RID: 3031
		public int m_AdvID_AfterBranch;

		// Token: 0x04000BD8 RID: 3032
		public int m_CpuFriendCharaID;

		// Token: 0x04000BD9 RID: 3033
		public int m_CpuFriendCharaLv;

		// Token: 0x04000BDA RID: 3034
		public int m_CpuFriendLimitBreak;

		// Token: 0x04000BDB RID: 3035
		public int m_CpuFriendSkillLv;

		// Token: 0x04000BDC RID: 3036
		public int m_CpuFriendWeaponID;

		// Token: 0x04000BDD RID: 3037
		public int m_CpuFriendWeaponLv;

		// Token: 0x04000BDE RID: 3038
		public string m_ItemDropCorrectCharaIDs;

		// Token: 0x04000BDF RID: 3039
		public int m_ItemDropProbabilityAdd;

		// Token: 0x04000BE0 RID: 3040
		public int m_StoreReview;
	}
}
