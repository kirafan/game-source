﻿using System;

namespace Star
{
	// Token: 0x020002CD RID: 717
	public abstract class EquipWeaponCharacterDataController : CharacterDataController
	{
		// Token: 0x06000DEB RID: 3563 RVA: 0x0004D3B9 File Offset: 0x0004B7B9
		public EquipWeaponCharacterDataController(eCharacterDataControllerType in_type) : base(in_type)
		{
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x0004D3C4 File Offset: 0x0004B7C4
		public virtual int GetWeaponId()
		{
			int num = this.GetWeaponIdNaked();
			if (num <= -1)
			{
				CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(base.GetCharaId());
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, (eClassType)param.m_Class);
				num = weaponParam.WeaponID;
			}
			return num;
		}

		// Token: 0x06000DED RID: 3565
		public abstract int GetWeaponIdNaked();

		// Token: 0x06000DEE RID: 3566 RVA: 0x0004D415 File Offset: 0x0004B815
		public int GetWeaponCost()
		{
			return EditUtility.CalcWeaponCostFromIDs(this.GetWeaponIdNaked());
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x0004D422 File Offset: 0x0004B822
		public WeaponListDB_Param GetWeaponListDBParam()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.GetWeaponId());
		}

		// Token: 0x06000DF0 RID: 3568
		public abstract int GetWeaponSkillID();

		// Token: 0x06000DF1 RID: 3569
		public abstract sbyte GetWeaponSkillExpTableID();

		// Token: 0x06000DF2 RID: 3570
		public abstract int GetWeaponLv();

		// Token: 0x06000DF3 RID: 3571
		public abstract int GetWeaponMaxLv();

		// Token: 0x06000DF4 RID: 3572
		public abstract long GetWeaponExp();

		// Token: 0x06000DF5 RID: 3573
		public abstract int GetWeaponSkillLv();

		// Token: 0x06000DF6 RID: 3574
		public abstract int GetWeaponSkillMaxLv();

		// Token: 0x06000DF7 RID: 3575 RVA: 0x0004D43E File Offset: 0x0004B83E
		public override int GetCost()
		{
			return base.GetCharaCost() + this.GetWeaponCost();
		}

		// Token: 0x06000DF8 RID: 3576 RVA: 0x0004D44D File Offset: 0x0004B84D
		public virtual EditUtility.OutputCharaParam GetWeaponParam()
		{
			return EditUtility.CalcWeaponParam(this.GetWeaponId(), this.GetWeaponLv());
		}
	}
}
