﻿using System;
using Star.UI;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200050A RID: 1290
	public class MissionPopupControll : MonoBehaviour
	{
		// Token: 0x0600194B RID: 6475 RVA: 0x00083644 File Offset: 0x00081A44
		public void SetUp(IMissionPakage ppakage)
		{
			GameObject gameObject = GameObject.Find("GameSystem/UICamera");
			if (gameObject != null)
			{
				Canvas component = base.gameObject.GetComponent<Canvas>();
				component.worldCamera = gameObject.GetComponent<Camera>();
			}
			Text text = UIUtility.FindHrcObject(base.transform, "TargetText", typeof(Text)) as Text;
			if (text != null)
			{
				text.text = ppakage.GetTargetMessage();
			}
			text = (UIUtility.FindHrcObject(base.transform, "CategoryText", typeof(Text)) as Text);
			if (text != null)
			{
				switch (ppakage.m_Type)
				{
				case eMissionCategory.Day:
					text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabNormal);
					break;
				case eMissionCategory.Week:
					text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabLimit);
					break;
				case eMissionCategory.Unlock:
					text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDay);
					break;
				case eMissionCategory.Event:
					text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDrop);
					break;
				}
			}
			this.m_TimeUp = 0.5f;
			this.m_SetNode = UIUtility.FindHrcTransform(base.transform, "Base");
			this.m_SetNode.localPosition = Vector3.up * 104f;
			this.m_SetNode.GetComponent<CustomButton>().AddListerner(new UnityAction(this.OnClickCallBack));
			this.m_State = MissionPopupControll.eStep.FadeIn;
		}

		// Token: 0x0600194C RID: 6476 RVA: 0x000837E0 File Offset: 0x00081BE0
		public void Update()
		{
			this.m_TimeUp -= Time.deltaTime;
			if (this.m_TimeUp <= 0f)
			{
				this.m_TimeUp = 0f;
			}
			switch (this.m_State)
			{
			case MissionPopupControll.eStep.FadeIn:
				this.m_SetNode.localPosition = this.m_TimeUp * 2f * Vector3.up * 104f - Vector3.up * 60f;
				if (this.m_TimeUp <= 0f)
				{
					this.m_TimeUp = 2.5f;
					this.m_State = MissionPopupControll.eStep.Loop;
				}
				break;
			case MissionPopupControll.eStep.FadeOut:
				this.m_SetNode.localPosition = (0.5f - this.m_TimeUp) * Vector3.up * 4f * 104f - Vector3.up * 60f;
				if (this.m_TimeUp <= 0f)
				{
					this.m_State = MissionPopupControll.eStep.End;
				}
				break;
			case MissionPopupControll.eStep.End:
				UnityEngine.Object.Destroy(base.gameObject);
				break;
			}
		}

		// Token: 0x0600194D RID: 6477 RVA: 0x0008391C File Offset: 0x00081D1C
		public bool IsEnd()
		{
			return this.m_TimeUp <= 0f && this.m_State <= MissionPopupControll.eStep.Loop;
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x0008393D File Offset: 0x00081D3D
		public void SetAutoDestroy()
		{
			this.m_TimeUp = 0.5f;
			this.m_State = MissionPopupControll.eStep.FadeOut;
		}

		// Token: 0x0600194F RID: 6479 RVA: 0x00083951 File Offset: 0x00081D51
		private void OnClickCallBack()
		{
			if (this.m_State <= MissionPopupControll.eStep.Loop)
			{
				this.SetAutoDestroy();
			}
		}

		// Token: 0x04002027 RID: 8231
		private float m_TimeUp;

		// Token: 0x04002028 RID: 8232
		private MissionPopupControll.eStep m_State;

		// Token: 0x04002029 RID: 8233
		private Transform m_SetNode;

		// Token: 0x0400202A RID: 8234
		private const float LISTUP_HI = 104f;

		// Token: 0x0400202B RID: 8235
		private const float LISTUP_POINT = 60f;

		// Token: 0x0200050B RID: 1291
		public enum eStep
		{
			// Token: 0x0400202D RID: 8237
			FadeIn,
			// Token: 0x0400202E RID: 8238
			Loop,
			// Token: 0x0400202F RID: 8239
			FadeOut,
			// Token: 0x04002030 RID: 8240
			End
		}
	}
}
