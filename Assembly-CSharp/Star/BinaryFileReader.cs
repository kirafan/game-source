﻿using System;
using System.IO;

namespace Star
{
	// Token: 0x02000335 RID: 821
	public class BinaryFileReader : BinaryReader
	{
		// Token: 0x06000FBB RID: 4027 RVA: 0x00054C28 File Offset: 0x00053028
		public bool OpenFile(string filename)
		{
			bool result;
			try
			{
				FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);
				if (fileStream == null)
				{
					result = false;
				}
				else
				{
					base.CreateBuffer((int)fileStream.Length);
					fileStream.Read(this.m_Buffer, 0, (int)fileStream.Length);
					fileStream.Close();
					result = true;
				}
			}
			catch
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x00054C98 File Offset: 0x00053098
		public void ReadFile()
		{
		}

		// Token: 0x06000FBD RID: 4029 RVA: 0x00054C9A File Offset: 0x0005309A
		public void CloseFile()
		{
			this.m_Buffer = null;
		}
	}
}
