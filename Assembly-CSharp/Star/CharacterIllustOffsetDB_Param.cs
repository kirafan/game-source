﻿using System;

namespace Star
{
	// Token: 0x02000174 RID: 372
	[Serializable]
	public struct CharacterIllustOffsetDB_Param
	{
		// Token: 0x040009CF RID: 2511
		public int m_CharaID;

		// Token: 0x040009D0 RID: 2512
		public float m_BustEyeOffsetY;

		// Token: 0x040009D1 RID: 2513
		public float m_BustSupportX;

		// Token: 0x040009D2 RID: 2514
		public float m_BustSupportY;

		// Token: 0x040009D3 RID: 2515
		public float m_BustSupportScl;

		// Token: 0x040009D4 RID: 2516
		public float[] m_GachaCutInPivotYs;
	}
}
