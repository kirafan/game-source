﻿using System;

namespace Star
{
	// Token: 0x020005B8 RID: 1464
	public class RoomCharaActObject : IRoomCharaAct
	{
		// Token: 0x06001C5D RID: 7261 RVA: 0x00097E50 File Offset: 0x00096250
		public bool RequestObjectAct(RoomObjectCtrlChara pbase, IRoomObjectControll ptarget)
		{
			if (ptarget == null)
			{
				return false;
			}
			this.m_EventAction = RoomActionCreate.CreateEventAction(ptarget.GetEventType());
			if (this.m_EventAction == null)
			{
				return false;
			}
			RoomEventParameter roomEventParameter = new RoomEventParameter();
			ptarget.GetEventParameter(roomEventParameter);
			this.m_EventAction.RequestObjEventMode(ptarget, pbase, roomEventParameter);
			return true;
		}

		// Token: 0x06001C5E RID: 7262 RVA: 0x00097EA8 File Offset: 0x000962A8
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			if (this.m_EventAction == null)
			{
				result = false;
			}
			else if (!this.m_EventAction.UpdateObjEventMode(pbase))
			{
				this.m_EventAction.Release();
				this.m_EventAction = null;
				result = false;
			}
			else if (pbase.IsIrqRequest())
			{
				if (this.m_EventAction.EventCancel(pbase, true))
				{
					this.m_EventAction.Release();
					this.m_EventAction = null;
					result = false;
				}
				else
				{
					pbase.CancelEventCommand();
				}
			}
			return result;
		}

		// Token: 0x06001C5F RID: 7263 RVA: 0x00097F30 File Offset: 0x00096330
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			if (this.m_EventAction != null)
			{
				this.m_EventAction.EventCancel(pbase, true);
				this.m_EventAction.Release();
				this.m_EventAction = null;
			}
			return true;
		}

		// Token: 0x04002341 RID: 9025
		private ICharaRoomAction m_EventAction;
	}
}
