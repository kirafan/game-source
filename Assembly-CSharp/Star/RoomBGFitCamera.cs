﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200057B RID: 1403
	public class RoomBGFitCamera : MonoBehaviour
	{
		// Token: 0x06001B34 RID: 6964 RVA: 0x0008FBD0 File Offset: 0x0008DFD0
		private void Awake()
		{
			Camera component = base.gameObject.GetComponent<Camera>();
			component.orthographic = true;
			float num = (float)Screen.height / (float)Screen.width;
			float num2 = this.m_BGWidth * this.m_PixelUnit;
			float num3 = num2 * num;
			component.orthographicSize = num3 - this.m_PixelUnit;
		}

		// Token: 0x04002229 RID: 8745
		public float m_BGWidth = 1920f;

		// Token: 0x0400222A RID: 8746
		public float m_BGHeight = 1080f;

		// Token: 0x0400222B RID: 8747
		public float m_PixelUnit = 0.01f;
	}
}
