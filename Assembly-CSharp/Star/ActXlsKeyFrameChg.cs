﻿using System;

namespace Star
{
	// Token: 0x02000574 RID: 1396
	public class ActXlsKeyFrameChg : ActXlsKeyBase
	{
		// Token: 0x06001B25 RID: 6949 RVA: 0x0008F975 File Offset: 0x0008DD75
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_Frame);
		}

		// Token: 0x04002216 RID: 8726
		public int m_Frame;
	}
}
