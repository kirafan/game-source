﻿using System;
using Star.UI.Edit;
using Star.UI.Global;

namespace Star
{
	// Token: 0x02000401 RID: 1025
	public class EditState_EditTop : EditState
	{
		// Token: 0x0600137D RID: 4989 RVA: 0x000681AB File Offset: 0x000665AB
		public EditState_EditTop(EditMain owner) : base(owner)
		{
		}

		// Token: 0x0600137E RID: 4990 RVA: 0x000681C2 File Offset: 0x000665C2
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x0600137F RID: 4991 RVA: 0x000681C5 File Offset: 0x000665C5
		public override void OnStateEnter()
		{
			this.m_Step = EditState_EditTop.eStep.First;
		}

		// Token: 0x06001380 RID: 4992 RVA: 0x000681CE File Offset: 0x000665CE
		public override void OnStateExit()
		{
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x000681D0 File Offset: 0x000665D0
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x000681DC File Offset: 0x000665DC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_EditTop.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_EditTop.eStep.LoadWait;
				break;
			case EditState_EditTop.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_EditTop.eStep.PlayIn;
				}
				break;
			case EditState_EditTop.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_EditTop.eStep.Main;
				}
				break;
			case EditState_EditTop.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_Owner.ScrollValue = this.m_UI.GetScrollValue();
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case EditTopUI.eButton.PartyEdit:
							this.m_Owner.SavePartyData();
							this.m_NextState = 2;
							break;
						case EditTopUI.eButton.SupportPartyEdit:
							this.m_NextState = 4;
							break;
						case EditTopUI.eButton.MasterEquip:
							this.m_NextState = 7;
							break;
						case EditTopUI.eButton.Upgrade:
							this.m_NextState = 8;
							break;
						case EditTopUI.eButton.LimitBreak:
							this.m_NextState = 11;
							break;
						case EditTopUI.eButton.Evolution:
							this.m_NextState = 14;
							break;
						case EditTopUI.eButton.CharaView:
							this.m_NextState = 17;
							break;
						default:
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
							this.m_NextState = 2147483646;
							break;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_EditTop.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_EditTop.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_EditTop.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x000683BC File Offset: 0x000667BC
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<EditTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.ScrollValue);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Edit);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x00068460 File Offset: 0x00066860
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Edit)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
				{
					this.m_NextState = 2147483646;
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				}
				base.OnClickBackButton(isCallFromShortCut);
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001385 RID: 4997 RVA: 0x000684D7 File Offset: 0x000668D7
		private void OnClickButton()
		{
		}

		// Token: 0x06001386 RID: 4998 RVA: 0x000684D9 File Offset: 0x000668D9
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001A6C RID: 6764
		private EditState_EditTop.eStep m_Step = EditState_EditTop.eStep.None;

		// Token: 0x04001A6D RID: 6765
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.EditTopUI;

		// Token: 0x04001A6E RID: 6766
		private EditTopUI m_UI;

		// Token: 0x02000402 RID: 1026
		private enum eStep
		{
			// Token: 0x04001A70 RID: 6768
			None = -1,
			// Token: 0x04001A71 RID: 6769
			First,
			// Token: 0x04001A72 RID: 6770
			LoadWait,
			// Token: 0x04001A73 RID: 6771
			PlayIn,
			// Token: 0x04001A74 RID: 6772
			Main,
			// Token: 0x04001A75 RID: 6773
			UnloadChildSceneWait
		}
	}
}
