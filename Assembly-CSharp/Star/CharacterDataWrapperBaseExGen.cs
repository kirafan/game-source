﻿using System;

namespace Star
{
	// Token: 0x020002A0 RID: 672
	public abstract class CharacterDataWrapperBaseExGen<DataType> : CharacterDataWrapperBase
	{
		// Token: 0x06000C8A RID: 3210 RVA: 0x0004886D File Offset: 0x00046C6D
		protected CharacterDataWrapperBaseExGen(eCharacterDataType characterDataType, DataType in_data) : base(characterDataType)
		{
			this.data = in_data;
		}

		// Token: 0x06000C8B RID: 3211 RVA: 0x0004887D File Offset: 0x00046C7D
		public DataType GetData()
		{
			return this.data;
		}

		// Token: 0x04001543 RID: 5443
		protected DataType data;
	}
}
