﻿using System;

namespace Star
{
	// Token: 0x02000308 RID: 776
	[Serializable]
	public struct ScheduleHolydayDB_Param
	{
		// Token: 0x0400160A RID: 5642
		public int m_ID;

		// Token: 0x0400160B RID: 5643
		public short m_OptionGroup;

		// Token: 0x0400160C RID: 5644
		public short m_Year;

		// Token: 0x0400160D RID: 5645
		public byte m_Month;

		// Token: 0x0400160E RID: 5646
		public byte m_Day;
	}
}
