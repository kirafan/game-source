﻿using System;

namespace Star
{
	// Token: 0x0200057A RID: 1402
	public class ActXlsKeyTag
	{
		// Token: 0x06001B31 RID: 6961 RVA: 0x0008FA90 File Offset: 0x0008DE90
		public void WriteIO(BinaryIO pio)
		{
			pio.String(ref this.m_KeyName);
			pio.Int(ref this.m_AccessKey);
			pio.Int(ref this.m_LinkKey);
			int num = 0;
			if (this.m_List != null)
			{
				num = this.m_List.Length;
			}
			pio.Int(ref num);
			for (int i = 0; i < this.m_List.Length; i++)
			{
				num = (int)this.m_List[i].m_Type;
				pio.Int(ref num);
				this.m_List[i].SerializeCode(pio);
			}
		}

		// Token: 0x06001B32 RID: 6962 RVA: 0x0008FB1C File Offset: 0x0008DF1C
		public void ReadIO(BinaryIO pio)
		{
			pio.String(ref this.m_KeyName);
			pio.Int(ref this.m_AccessKey);
			pio.Int(ref this.m_LinkKey);
			int num = 0;
			pio.Int(ref num);
			if (num != 0)
			{
				this.m_List = new ActXlsKeyBase[num];
				for (int i = 0; i < this.m_List.Length; i++)
				{
					pio.Int(ref num);
					this.m_List[i] = ActXlsKeyBase.CreateKey(num);
					this.m_List[i].SerializeCode(pio);
				}
			}
		}

		// Token: 0x04002225 RID: 8741
		public string m_KeyName;

		// Token: 0x04002226 RID: 8742
		public int m_AccessKey;

		// Token: 0x04002227 RID: 8743
		public int m_LinkKey;

		// Token: 0x04002228 RID: 8744
		public ActXlsKeyBase[] m_List;
	}
}
