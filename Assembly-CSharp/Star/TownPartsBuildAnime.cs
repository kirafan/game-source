﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006B6 RID: 1718
	public class TownPartsBuildAnime : MonoBehaviour
	{
		// Token: 0x06002242 RID: 8770 RVA: 0x000B5EA4 File Offset: 0x000B42A4
		public static TownPartsBuildAnime CreateBuildAnime(TownBuilder pbuilder, GameObject ptarget, eTownObjectType fgroupno)
		{
			Transform child = ptarget.transform.GetChild(0);
			TownPartsBuildAnime townPartsBuildAnime = child.gameObject.AddComponent<TownPartsBuildAnime>();
			townPartsBuildAnime.m_ModelIO = child.gameObject.AddComponent<TownModelIO>();
			townPartsBuildAnime.m_PlayAnime = child.gameObject.AddComponent<Animation>();
			townPartsBuildAnime.m_BaseNode = pbuilder.GetGeneralObj(fgroupno);
			townPartsBuildAnime.m_BaseNode.transform.SetParent(ptarget.transform, false);
			TownBuildAnime component = townPartsBuildAnime.m_BaseNode.GetComponent<TownBuildAnime>();
			for (int i = 0; i < component.m_Table.Length; i++)
			{
				townPartsBuildAnime.m_PlayAnime.AddClip(component.m_Table[i].m_Anime, component.m_Table[i].m_Type.ToString());
			}
			return townPartsBuildAnime;
		}

		// Token: 0x06002243 RID: 8771 RVA: 0x000B5F6E File Offset: 0x000B436E
		public void PlayMotion(TownPartsBuildAnime.eType fmode)
		{
			this.m_PlayAnime.Play(fmode.ToString());
		}

		// Token: 0x06002244 RID: 8772 RVA: 0x000B5F89 File Offset: 0x000B4389
		public bool IsPlaying(TownPartsBuildAnime.eType fmode)
		{
			return this.m_PlayAnime.IsPlaying(fmode.ToString());
		}

		// Token: 0x06002245 RID: 8773 RVA: 0x000B5FA3 File Offset: 0x000B43A3
		public void DestroyObj(TownBuilder pbuilder)
		{
			pbuilder.ReleaseCharaMarker(this.m_BaseNode);
			this.m_BaseNode = null;
		}

		// Token: 0x040028C0 RID: 10432
		private Animation m_PlayAnime;

		// Token: 0x040028C1 RID: 10433
		private GameObject m_BaseNode;

		// Token: 0x040028C2 RID: 10434
		public TownModelIO m_ModelIO;

		// Token: 0x020006B7 RID: 1719
		public enum eType
		{
			// Token: 0x040028C4 RID: 10436
			FadeIn,
			// Token: 0x040028C5 RID: 10437
			FadeOut
		}
	}
}
