﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Star
{
	// Token: 0x020004D9 RID: 1241
	public static class MenuUtility
	{
		// Token: 0x06001889 RID: 6281 RVA: 0x0007FEEC File Offset: 0x0007E2EC
		public static List<int> GetAvailableADVLibrary(eADVLibraryCategory category)
		{
			List<int> list = new List<int>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVLibraryListDB.m_Params.Length; i++)
			{
				ADVLibraryListDB_Param advlibraryListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVLibraryListDB.m_Params[i];
				if (advlibraryListDB_Param.m_Category == (int)category)
				{
					if (MenuUtility.GetAvailableADVList(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVLibraryListDB.m_Params[i].m_LibraryListID).Count > 0)
					{
						list.Add(advlibraryListDB_Param.m_LibraryListID);
					}
				}
			}
			return list;
		}

		// Token: 0x0600188A RID: 6282 RVA: 0x0007FF9C File Offset: 0x0007E39C
		public static List<int> GetAvailableADVList(int libraryID)
		{
			List<int> list = new List<int>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.m_Params.Length; i++)
			{
				ADVListDB_Param advlistDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.m_Params[i];
				if (advlistDB_Param.m_LibraryID == libraryID && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advlistDB_Param.m_AdvID))
				{
					list.Add(advlistDB_Param.m_AdvID);
				}
			}
			return list;
		}

		// Token: 0x0600188B RID: 6283 RVA: 0x00080034 File Offset: 0x0007E434
		public static List<int> GetAvailableSingleADVList(eCharaNamedType namedType)
		{
			List<int> list = new List<int>();
			ADVListDB advlistDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB;
			for (int i = 0; i < advlistDB.m_Params.Length; i++)
			{
				ADVListDB_Param advlistDB_Param = advlistDB.m_Params[i];
				int num = 0;
				bool flag = false;
				for (int j = 0; j < advlistDB_Param.m_NamedType.Length; j++)
				{
					eCharaNamedType eCharaNamedType = (eCharaNamedType)advlistDB_Param.m_NamedType[j];
					if (eCharaNamedType != eCharaNamedType.None)
					{
						num++;
						if (num > 1)
						{
							flag = false;
							break;
						}
					}
					if (eCharaNamedType == namedType)
					{
						flag = true;
					}
				}
				if (num == 1 && flag)
				{
					list.Add(advlistDB_Param.m_AdvID);
				}
			}
			return list;
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x000800F8 File Offset: 0x0007E4F8
		public static List<int> GetAvailableCrossADVList(eCharaNamedType namedType)
		{
			List<int> list = new List<int>();
			ADVListDB advlistDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB;
			for (int i = 0; i < advlistDB.m_Params.Length; i++)
			{
				ADVListDB_Param advlistDB_Param = advlistDB.m_Params[i];
				int num = 0;
				bool flag = false;
				for (int j = 0; j < advlistDB_Param.m_NamedType.Length; j++)
				{
					eCharaNamedType eCharaNamedType = (eCharaNamedType)advlistDB_Param.m_NamedType[j];
					if (eCharaNamedType != eCharaNamedType.None)
					{
						num++;
					}
					if (eCharaNamedType == namedType)
					{
						flag = true;
					}
				}
				if (num > 1 && flag)
				{
					list.Add(advlistDB_Param.m_AdvID);
				}
			}
			return list;
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x000801AC File Offset: 0x0007E5AC
		public static string GetLockTextCharaADV(int advid)
		{
			ADVListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(advid);
			StringBuilder stringBuilder = new StringBuilder();
			int num = 0;
			for (int i = 0; i < param.m_LimitNamedType.Length; i++)
			{
				eCharaNamedType eCharaNamedType = (eCharaNamedType)param.m_LimitNamedType[i];
				if (eCharaNamedType != eCharaNamedType.None)
				{
					UserNamedData userNamedData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(eCharaNamedType);
					if (userNamedData == null || userNamedData.FriendShip < param.m_LimitFriendShipLv)
					{
						if (num > 0)
						{
							stringBuilder.Append("、");
						}
						stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(eCharaNamedType).m_NickName);
						num++;
					}
				}
			}
			if (num > 0)
			{
				stringBuilder.Append("との仲良し度が");
				stringBuilder.Append(param.m_LimitFriendShipLv);
				stringBuilder.Append("以上");
				return stringBuilder.ToString();
			}
			return null;
		}
	}
}
