﻿using System;

namespace Star
{
	// Token: 0x020001B7 RID: 439
	public static class TownObjectLevelUpDB_Ext
	{
		// Token: 0x06000B31 RID: 2865 RVA: 0x000429C4 File Offset: 0x00040DC4
		public static TownObjectLevelUpDB_Param GetParam(this TownObjectLevelUpDB self, int levelUpListID, int level)
		{
			if (level <= 0)
			{
				level = 1;
			}
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == levelUpListID && self.m_Params[i].m_TargetLv == level)
				{
					return self.m_Params[i];
				}
			}
			return default(TownObjectLevelUpDB_Param);
		}

		// Token: 0x06000B32 RID: 2866 RVA: 0x00042A40 File Offset: 0x00040E40
		public static TownObjectLevelUpDB_Param GetParamByObjID(this TownObjectLevelUpDB self, int objID, int level)
		{
			int levelUpListID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_LevelUpListID;
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(levelUpListID, level);
		}
	}
}
