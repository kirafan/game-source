﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000096 RID: 150
	[Serializable]
	public class BattleAIExecData
	{
		// Token: 0x040002B0 RID: 688
		public int m_CommandIndex;

		// Token: 0x040002B1 RID: 689
		public bool m_IsCommandTargetSelectionInOrder;

		// Token: 0x040002B2 RID: 690
		public int m_Ratio;

		// Token: 0x040002B3 RID: 691
		public List<BattleAIFlag> m_AIFlags = new List<BattleAIFlag>();

		// Token: 0x040002B4 RID: 692
		public List<BattleAICommandTargetSingleCondition> m_SingleConditions = new List<BattleAICommandTargetSingleCondition>();
	}
}
