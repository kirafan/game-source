﻿using System;
using Star.UI;
using Star.UI.BackGround;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x0200042B RID: 1067
	public class MenuMain : GameStateMain
	{
		// Token: 0x17000169 RID: 361
		// (get) Token: 0x0600147E RID: 5246 RVA: 0x0006C398 File Offset: 0x0006A798
		// (set) Token: 0x0600147F RID: 5247 RVA: 0x0006C3A0 File Offset: 0x0006A7A0
		public MenuLibraryWordUI.eMode LibraryStartMode { get; set; }

		// Token: 0x06001480 RID: 5248 RVA: 0x0006C3A9 File Offset: 0x0006A7A9
		private void Start()
		{
			this.m_InstState = MenuMain.eMenuMainInstanceState.StartFunc;
			base.SetNextState(0);
			this.m_InstState = MenuMain.eMenuMainInstanceState.EndStartFunc;
		}

		// Token: 0x06001481 RID: 5249 RVA: 0x0006C3C0 File Offset: 0x0006A7C0
		private void OnDestroy()
		{
			this.m_InstState = MenuMain.eMenuMainInstanceState.DestroyFunc;
			this.m_NpcUI = null;
			this.m_InstState = MenuMain.eMenuMainInstanceState.EndDestroyFunc;
		}

		// Token: 0x06001482 RID: 5250 RVA: 0x0006C3D7 File Offset: 0x0006A7D7
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x06001483 RID: 5251 RVA: 0x0006C3E0 File Offset: 0x0006A7E0
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			switch (nextStateID)
			{
			case 0:
			{
				this.m_InstState = MenuMain.eMenuMainInstanceState.FirstInitState;
				MenuMain.eLaunchChildScene? eLaunchChildScene = MenuMain.launchChildScene;
				if (eLaunchChildScene == null)
				{
					MenuMain.launchChildScene = new MenuMain.eLaunchChildScene?(MenuMain.eLaunchChildScene.AllowChange);
				}
				this.m_InstState = MenuMain.eMenuMainInstanceState.Idle;
				return new MenuState_Init(this);
			}
			case 1:
				return new MenuState_Top(this);
			case 2:
				return new MenuState_UserInfomation(this);
			case 3:
				return new MenuState_Friend(this);
			case 4:
				return new MenuState_Library(this);
			case 5:
				return new MenuState_Help(this);
			case 6:
				return new MenuState_Option(this);
			case 7:
				return new MenuState_Notification(this);
			case 8:
			case 9:
			case 11:
				return new CommonState_Final(this);
			default:
				if (nextStateID != 2147483646)
				{
					return null;
				}
				this.m_InstState = MenuMain.eMenuMainInstanceState.CommonStateFinal;
				if (base.NextTransitSceneID != SceneDefine.eSceneID.MoviePlay && base.NextTransitSceneID != SceneDefine.eSceneID.ADV)
				{
					MenuMain.launchChildScene = null;
				}
				this.m_InstState = MenuMain.eMenuMainInstanceState.EndCommonStateFinal;
				return new CommonState_Final(this);
			case 12:
				return new MenuState_LibraryTitle(this);
			case 13:
				return new MenuState_LibraryEvent(this);
			case 14:
				return new MenuState_LibraryOriginalChara(this);
			case 15:
				return new MenuState_PlayerMoveConfiguration(this);
			case 16:
				return new MenuState_PurchaseHistory(this);
			}
		}

		// Token: 0x06001484 RID: 5252 RVA: 0x0006C51B File Offset: 0x0006A91B
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
			if (base.NextTransitSceneID == SceneDefine.eSceneID.Title)
			{
				APIUtility.ReturnTitle(false);
			}
		}

		// Token: 0x06001485 RID: 5253 RVA: 0x0006C53E File Offset: 0x0006A93E
		public MenuMain.eMenuMainInstanceState GetInstState()
		{
			return this.m_InstState;
		}

		// Token: 0x06001486 RID: 5254 RVA: 0x0006C548 File Offset: 0x0006A948
		public void SetLaunchChildScene(MenuMain.eLaunchChildScene launchScene)
		{
			MenuMain.eLaunchChildScene? eLaunchChildScene = MenuMain.launchChildScene;
			if (eLaunchChildScene == null)
			{
				return;
			}
			if (MenuMain.launchChildScene != MenuMain.eLaunchChildScene.AllowChange)
			{
				return;
			}
			MenuMain.launchChildScene = new MenuMain.eLaunchChildScene?(launchScene);
		}

		// Token: 0x06001487 RID: 5255 RVA: 0x0006C59C File Offset: 0x0006A99C
		public MenuMain.eLaunchChildScene GetLaunchChildScene()
		{
			MenuMain.eLaunchChildScene? eLaunchChildScene = MenuMain.launchChildScene;
			if (eLaunchChildScene == null)
			{
				return MenuMain.eLaunchChildScene.Invalid;
			}
			return MenuMain.launchChildScene.Value;
		}

		// Token: 0x06001488 RID: 5256 RVA: 0x0006C5CA File Offset: 0x0006A9CA
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001B55 RID: 6997
		public const int STATE_INIT = 0;

		// Token: 0x04001B56 RID: 6998
		public const int STATE_MENU_TOP = 1;

		// Token: 0x04001B57 RID: 6999
		public const int STATE_MENU_USERINFO = 2;

		// Token: 0x04001B58 RID: 7000
		public const int STATE_MENU_FRIEND = 3;

		// Token: 0x04001B59 RID: 7001
		public const int STATE_MENU_LIBRARY = 4;

		// Token: 0x04001B5A RID: 7002
		public const int STATE_MENU_HELP = 5;

		// Token: 0x04001B5B RID: 7003
		public const int STATE_MENU_OPTION = 6;

		// Token: 0x04001B5C RID: 7004
		public const int STATE_MENU_NOTIFICATION = 7;

		// Token: 0x04001B5D RID: 7005
		public const int STATE_MENU_OFFICIALSITE = 8;

		// Token: 0x04001B5E RID: 7006
		public const int STATE_MENU_TWITTER = 9;

		// Token: 0x04001B5F RID: 7007
		public const int STATE_MENU_TITLE = 11;

		// Token: 0x04001B60 RID: 7008
		public const int STATE_MENU_LIBRARY_TITLE = 12;

		// Token: 0x04001B61 RID: 7009
		public const int STATE_MENU_LIBRARY_EVENT = 13;

		// Token: 0x04001B62 RID: 7010
		public const int STATE_MENU_LIBRARY_ORIGINALCHARA = 14;

		// Token: 0x04001B63 RID: 7011
		public const int STATE_MENU_PLAYER_MOVE_CONFIGURATION = 15;

		// Token: 0x04001B64 RID: 7012
		public const int STATE_MENU_PURCHASE_HISTORY = 16;

		// Token: 0x04001B65 RID: 7013
		public NPCCharaDisplayUI m_NpcUI;

		// Token: 0x04001B67 RID: 7015
		private MenuMain.eMenuMainInstanceState m_InstState;

		// Token: 0x04001B68 RID: 7016
		private static MenuMain.eLaunchChildScene? launchChildScene;

		// Token: 0x0200042C RID: 1068
		public enum eLaunchChildScene
		{
			// Token: 0x04001B6A RID: 7018
			Invalid,
			// Token: 0x04001B6B RID: 7019
			AllowChange,
			// Token: 0x04001B6C RID: 7020
			Top,
			// Token: 0x04001B6D RID: 7021
			Friend,
			// Token: 0x04001B6E RID: 7022
			Library,
			// Token: 0x04001B6F RID: 7023
			LibraryEvent
		}

		// Token: 0x0200042D RID: 1069
		public enum eMenuMainInstanceState
		{
			// Token: 0x04001B71 RID: 7025
			Invalid,
			// Token: 0x04001B72 RID: 7026
			StartFunc,
			// Token: 0x04001B73 RID: 7027
			EndStartFunc,
			// Token: 0x04001B74 RID: 7028
			FirstInitState,
			// Token: 0x04001B75 RID: 7029
			Idle,
			// Token: 0x04001B76 RID: 7030
			CommonStateFinal,
			// Token: 0x04001B77 RID: 7031
			EndCommonStateFinal,
			// Token: 0x04001B78 RID: 7032
			DestroyFunc,
			// Token: 0x04001B79 RID: 7033
			EndDestroyFunc
		}
	}
}
