﻿using System;

namespace Star
{
	// Token: 0x0200008D RID: 141
	public enum eBattleAICommandConditionType
	{
		// Token: 0x04000252 RID: 594
		Self_HpRange,
		// Token: 0x04000253 RID: 595
		Self_StateAbnormal,
		// Token: 0x04000254 RID: 596
		Self_StatusChange,
		// Token: 0x04000255 RID: 597
		Self_ChargeCount,
		// Token: 0x04000256 RID: 598
		Self_Reserve_4,
		// Token: 0x04000257 RID: 599
		Self_Reserve_5,
		// Token: 0x04000258 RID: 600
		Self_Reserve_6,
		// Token: 0x04000259 RID: 601
		Self_Reserve_7,
		// Token: 0x0400025A RID: 602
		Self_Reserve_8,
		// Token: 0x0400025B RID: 603
		Self_Reserve_9,
		// Token: 0x0400025C RID: 604
		Self_Reserve_10,
		// Token: 0x0400025D RID: 605
		Self_Reserve_11,
		// Token: 0x0400025E RID: 606
		Self_Reserve_12,
		// Token: 0x0400025F RID: 607
		Self_Reserve_13,
		// Token: 0x04000260 RID: 608
		Self_Reserve_14,
		// Token: 0x04000261 RID: 609
		Self_Reserve_15,
		// Token: 0x04000262 RID: 610
		Self_Reserve_16,
		// Token: 0x04000263 RID: 611
		Self_Reserve_17,
		// Token: 0x04000264 RID: 612
		Self_Reserve_18,
		// Token: 0x04000265 RID: 613
		Self_Reserve_19,
		// Token: 0x04000266 RID: 614
		Self_Reserve_20,
		// Token: 0x04000267 RID: 615
		MyParty_AliveNum,
		// Token: 0x04000268 RID: 616
		MyParty_Reserve_1,
		// Token: 0x04000269 RID: 617
		MyParty_Reserve_2,
		// Token: 0x0400026A RID: 618
		MyParty_Reserve_3,
		// Token: 0x0400026B RID: 619
		MyParty_Reserve_4,
		// Token: 0x0400026C RID: 620
		MyParty_Reserve_5,
		// Token: 0x0400026D RID: 621
		MyParty_Reserve_6,
		// Token: 0x0400026E RID: 622
		MyParty_Reserve_7,
		// Token: 0x0400026F RID: 623
		MyParty_Reserve_8,
		// Token: 0x04000270 RID: 624
		MyParty_Reserve_9,
		// Token: 0x04000271 RID: 625
		MyParty_Reserve_10,
		// Token: 0x04000272 RID: 626
		MyParty_Reserve_11,
		// Token: 0x04000273 RID: 627
		MyParty_Reserve_12,
		// Token: 0x04000274 RID: 628
		MyParty_Reserve_13,
		// Token: 0x04000275 RID: 629
		MyParty_Reserve_14,
		// Token: 0x04000276 RID: 630
		MyParty_Reserve_15,
		// Token: 0x04000277 RID: 631
		MyParty_Reserve_16,
		// Token: 0x04000278 RID: 632
		MyParty_Reserve_17,
		// Token: 0x04000279 RID: 633
		MyParty_Reserve_18,
		// Token: 0x0400027A RID: 634
		MyParty_Reserve_19,
		// Token: 0x0400027B RID: 635
		MyParty_Reserve_20,
		// Token: 0x0400027C RID: 636
		TgtParty_AliveNum,
		// Token: 0x0400027D RID: 637
		TgtParty_StateAbnormalCount,
		// Token: 0x0400027E RID: 638
		TgtParty_NormalAttackUseCount,
		// Token: 0x0400027F RID: 639
		TgtParty_SkillUseCount,
		// Token: 0x04000280 RID: 640
		TgtParty_TogetherAttackUseCount,
		// Token: 0x04000281 RID: 641
		TgtParty_MemberChangeCount,
		// Token: 0x04000282 RID: 642
		TgtParty_Reserve_1,
		// Token: 0x04000283 RID: 643
		TgtParty_Reserve_2,
		// Token: 0x04000284 RID: 644
		TgtParty_Reserve_3,
		// Token: 0x04000285 RID: 645
		TgtParty_Reserve_4,
		// Token: 0x04000286 RID: 646
		TgtParty_Reserve_5,
		// Token: 0x04000287 RID: 647
		TgtParty_Reserve_6,
		// Token: 0x04000288 RID: 648
		TgtParty_Reserve_7,
		// Token: 0x04000289 RID: 649
		TgtParty_Reserve_8,
		// Token: 0x0400028A RID: 650
		TgtParty_Reserve_9,
		// Token: 0x0400028B RID: 651
		TgtParty_Reserve_10,
		// Token: 0x0400028C RID: 652
		TgtParty_Reserve_11,
		// Token: 0x0400028D RID: 653
		TgtParty_Reserve_12,
		// Token: 0x0400028E RID: 654
		TgtParty_Reserve_13,
		// Token: 0x0400028F RID: 655
		TgtParty_Reserve_14,
		// Token: 0x04000290 RID: 656
		TgtParty_Reserve_15,
		// Token: 0x04000291 RID: 657
		TgtParty_Reserve_16,
		// Token: 0x04000292 RID: 658
		TgtParty_Reserve_17,
		// Token: 0x04000293 RID: 659
		TgtParty_Reserve_18,
		// Token: 0x04000294 RID: 660
		TgtParty_Reserve_19,
		// Token: 0x04000295 RID: 661
		TgtParty_Reserve_20,
		// Token: 0x04000296 RID: 662
		Flag,
		// Token: 0x04000297 RID: 663
		Num
	}
}
