﻿using System;

namespace Star
{
	// Token: 0x0200023B RID: 571
	[Serializable]
	public struct Text_MessageDB_Param
	{
		// Token: 0x04001301 RID: 4865
		public int m_ID;

		// Token: 0x04001302 RID: 4866
		public string m_Text;
	}
}
