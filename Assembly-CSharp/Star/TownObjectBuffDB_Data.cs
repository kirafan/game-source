﻿using System;

namespace Star
{
	// Token: 0x02000242 RID: 578
	[Serializable]
	public struct TownObjectBuffDB_Data
	{
		// Token: 0x04001318 RID: 4888
		public int m_TargetConditionType;

		// Token: 0x04001319 RID: 4889
		public int m_TargetCondition;

		// Token: 0x0400131A RID: 4890
		public int m_BuffType;

		// Token: 0x0400131B RID: 4891
		public float[] m_Value;
	}
}
