﻿using System;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x02000451 RID: 1105
	public class PresentMain : GameStateMain
	{
		// Token: 0x06001552 RID: 5458 RVA: 0x0006F4F1 File Offset: 0x0006D8F1
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x06001553 RID: 5459 RVA: 0x0006F4FA File Offset: 0x0006D8FA
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x06001554 RID: 5460 RVA: 0x0006F504 File Offset: 0x0006D904
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new PresentState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new PresentState_Main(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x06001555 RID: 5461 RVA: 0x0006F54B File Offset: 0x0006D94B
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
		}

		// Token: 0x06001556 RID: 5462 RVA: 0x0006F55D File Offset: 0x0006D95D
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001C2C RID: 7212
		public const int STATE_INIT = 0;

		// Token: 0x04001C2D RID: 7213
		public const int STATE_MAIN = 1;
	}
}
