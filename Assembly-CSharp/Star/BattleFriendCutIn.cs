﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020000C2 RID: 194
	public class BattleFriendCutIn : MonoBehaviour
	{
		// Token: 0x06000525 RID: 1317 RVA: 0x0001AFD6 File Offset: 0x000193D6
		private void Start()
		{
			this.m_Camera.enabled = false;
			this.m_AdjustOrthographicCamera.enabled = false;
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x0001AFF0 File Offset: 0x000193F0
		private void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			switch (this.m_Step)
			{
			case BattleFriendCutIn.eStep.Prepare:
				if (!this.m_IsDonePrepare && this.m_Loader != null)
				{
					this.m_LoadingHndl = this.m_Loader.Load("battle/friendcutin/friendcutin.muast", new MeigeResource.Option[0]);
				}
				this.m_SpriteHndl = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustChara(this.m_CharaID);
				this.SetStep(BattleFriendCutIn.eStep.Prepare_Wait);
				break;
			case BattleFriendCutIn.eStep.Prepare_Wait:
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.SetStep(BattleFriendCutIn.eStep.Prepare_Error);
						break;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						break;
					}
				}
				if (this.m_SpriteHndl == null || this.m_SpriteHndl.IsAvailable())
				{
					GameObject gameObject = this.m_LoadingHndl.FindObj("FriendCutIn", true) as GameObject;
					GameObject gameObject2 = this.m_LoadingHndl.FindObj("MeigeAC_FriendCutIn@Take 001", true) as GameObject;
					this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), base.transform);
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(gameObject.name, false);
					this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
					Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
					component.cullingType = AnimationCullingType.AlwaysAnimate;
					this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
					MeigeAnimClipHolder componentInChildren = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
					this.m_MeigeAnimCtrl.Open();
					this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, componentInChildren);
					this.m_MeigeAnimCtrl.Close();
					int num = LayerMask.NameToLayer("Graphic2D");
					this.m_BodyObj.SetLayer(num, true);
					int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
					for (int i = 0; i < particleEmitterNum; i++)
					{
						MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(i);
						if (particleEmitter != null)
						{
							particleEmitter.SetLayer(num);
						}
					}
					if (this.m_LoadingHndl != null)
					{
						this.m_Loader.UnloadAll(false);
						this.m_LoadingHndl = null;
					}
					this.m_Texture = this.m_SpriteHndl.Obj.texture;
					this.ApplyCharaIllustTexture();
					this.m_IsDonePrepare = true;
					this.SetStep(BattleFriendCutIn.eStep.Play);
				}
				break;
			case BattleFriendCutIn.eStep.Play:
			{
				this.m_Camera.enabled = true;
				this.m_Camera.orthographicSize = 3.75f;
				this.m_AdjustOrthographicCamera.enabled = false;
				this.m_BodyObj.SetActive(true);
				int particleEmitterNum2 = this.m_MsbHndl.GetParticleEmitterNum();
				for (int j = 0; j < particleEmitterNum2; j++)
				{
					MeigeParticleEmitter particleEmitter2 = this.m_MsbHndl.GetParticleEmitter(j);
					if (particleEmitter2 != null)
					{
						particleEmitter2.Kill();
					}
				}
				this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_FRIEND_CUTIN, 1f, 0, -1, -1);
				this.SetStep(BattleFriendCutIn.eStep.Play_Wait);
				break;
			}
			case BattleFriendCutIn.eStep.Play_Wait:
				if (!this.IsPlaying())
				{
					this.m_Camera.enabled = false;
					this.m_BodyObj.SetActive(false);
					this.m_MeigeAnimCtrl.Pause();
					this.SetStep(BattleFriendCutIn.eStep.None);
				}
				break;
			}
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x0001B377 File Offset: 0x00019777
		public void Play(int charaID)
		{
			this.m_CharaID = charaID;
			this.m_Camera.enabled = false;
			if (!this.m_IsDonePrepare)
			{
				this.SetStep(BattleFriendCutIn.eStep.Prepare);
			}
			else
			{
				this.ApplyCharaIllustTexture();
				this.SetStep(BattleFriendCutIn.eStep.Play);
			}
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x0001B3B0 File Offset: 0x000197B0
		public bool IsCompletePlay()
		{
			return this.m_Step == BattleFriendCutIn.eStep.None;
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x0001B3BB File Offset: 0x000197BB
		public bool IsPrepareError()
		{
			return this.m_Step == BattleFriendCutIn.eStep.Prepare_Error;
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0001B3C8 File Offset: 0x000197C8
		public void Destroy()
		{
			this.m_IsDonePrepare = false;
			SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHndl);
			this.m_SpriteHndl = null;
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			this.m_BodyObj = null;
			this.m_Texture = null;
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x0001B41B File Offset: 0x0001981B
		private void SetStep(BattleFriendCutIn.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x0001B424 File Offset: 0x00019824
		private void ApplyCharaIllustTexture()
		{
			if (this.m_Texture != null)
			{
				MsbObjectHandler msbObjectHandlerByName = this.m_MsbHndl.GetMsbObjectHandlerByName("chara");
				if (msbObjectHandlerByName != null)
				{
					Renderer renderer = msbObjectHandlerByName.GetRenderer();
					if (renderer != null && renderer.material != null)
					{
						MeigeShaderUtility.SetTexture(renderer.material, this.m_Texture, eTextureType.eTextureType_Albedo, 0);
					}
				}
			}
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x0001B490 File Offset: 0x00019890
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x040003F1 RID: 1009
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x040003F2 RID: 1010
		private const string OBJ_CHARA_ILLUST = "chara";

		// Token: 0x040003F3 RID: 1011
		private const string PLAY_KEY = "Take 001";

		// Token: 0x040003F4 RID: 1012
		private const string RESOURCE_PATH = "battle/friendcutin/friendcutin.muast";

		// Token: 0x040003F5 RID: 1013
		private const string OBJ_MODEL_PATH = "FriendCutIn";

		// Token: 0x040003F6 RID: 1014
		private const string OBJ_ANIM_PATH = "MeigeAC_FriendCutIn@Take 001";

		// Token: 0x040003F7 RID: 1015
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040003F8 RID: 1016
		[SerializeField]
		private AdjustOrthographicCamera m_AdjustOrthographicCamera;

		// Token: 0x040003F9 RID: 1017
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040003FA RID: 1018
		private BattleFriendCutIn.eStep m_Step = BattleFriendCutIn.eStep.None;

		// Token: 0x040003FB RID: 1019
		private bool m_IsDonePrepare;

		// Token: 0x040003FC RID: 1020
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x040003FD RID: 1021
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x040003FE RID: 1022
		private SpriteHandler m_SpriteHndl;

		// Token: 0x040003FF RID: 1023
		private Transform m_BaseTransform;

		// Token: 0x04000400 RID: 1024
		private MsbHandler m_MsbHndl;

		// Token: 0x04000401 RID: 1025
		private GameObject m_BodyObj;

		// Token: 0x04000402 RID: 1026
		private Texture m_Texture;

		// Token: 0x04000403 RID: 1027
		private int m_CharaID = -1;

		// Token: 0x020000C3 RID: 195
		private enum eStep
		{
			// Token: 0x04000405 RID: 1029
			None = -1,
			// Token: 0x04000406 RID: 1030
			Prepare,
			// Token: 0x04000407 RID: 1031
			Prepare_Wait,
			// Token: 0x04000408 RID: 1032
			Prepare_Error,
			// Token: 0x04000409 RID: 1033
			Play,
			// Token: 0x0400040A RID: 1034
			Play_Wait
		}
	}
}
