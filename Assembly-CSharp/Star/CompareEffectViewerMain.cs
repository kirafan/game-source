﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000279 RID: 633
	public class CompareEffectViewerMain : MonoBehaviour
	{
		// Token: 0x06000BD6 RID: 3030 RVA: 0x0004579D File Offset: 0x00043B9D
		private void Start()
		{
			this.m_Step = CompareEffectViewerMain.eStep.First;
		}

		// Token: 0x06000BD7 RID: 3031 RVA: 0x000457A8 File Offset: 0x00043BA8
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			switch (this.m_Step)
			{
			case CompareEffectViewerMain.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_Step = CompareEffectViewerMain.eStep.Prepare;
				}
				break;
			case CompareEffectViewerMain.eStep.Prepare:
			{
				CompareEffectViewerMain.Setting setting = this.m_Settings[this.m_UseSettingsIndex];
				for (int i = 0; i < setting.m_EffectIDs.Length; i++)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadSingleEffect(setting.m_EffectIDs[i], false);
				}
				this.m_Step = CompareEffectViewerMain.eStep.Prepare_Wait;
				break;
			}
			case CompareEffectViewerMain.eStep.Prepare_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
				{
					this.m_Step = CompareEffectViewerMain.eStep.Main;
				}
				break;
			}
		}

		// Token: 0x06000BD8 RID: 3032 RVA: 0x00045890 File Offset: 0x00043C90
		public void OnDecidePlay()
		{
			if (this.m_Step != CompareEffectViewerMain.eStep.Main)
			{
				return;
			}
			CompareEffectViewerMain.Setting setting = this.m_Settings[this.m_UseSettingsIndex];
			int num = setting.m_EffectIDs.Length;
			for (int i = 0; i < num; i++)
			{
				Vector2 intervalPos = setting.m_IntervalPos;
				int columnNum = setting.m_ColumnNum;
				int num2 = i % columnNum;
				int num3 = i / columnNum;
				int num4 = Mathf.Min(columnNum, num);
				float num5 = -((float)(num4 - 1) / 2f) * intervalPos.x;
				float num6 = (float)(-(float)((num - 1) / columnNum)) * intervalPos.y / 2f;
				Vector3 position = new Vector3((float)num2 * intervalPos.x + num5, (float)num3 * intervalPos.y + num6, 0f);
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.Play(setting.m_EffectIDs[i], position, Quaternion.Euler(0f, 180f, 0f), Vector3.one, null, false, 100, 1f, false);
			}
		}

		// Token: 0x06000BD9 RID: 3033 RVA: 0x0004598A File Offset: 0x00043D8A
		public void OnToggleBG(bool value)
		{
			this.m_BG.SetActive(this.m_ToggleBG.isOn);
		}

		// Token: 0x06000BDA RID: 3034 RVA: 0x000459A2 File Offset: 0x00043DA2
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * this.m_SliderZoom.value;
		}

		// Token: 0x06000BDB RID: 3035 RVA: 0x000459CF File Offset: 0x00043DCF
		public void OnChangedTimeScale(float value)
		{
			Time.timeScale = this.m_SliderTimeScale.value;
		}

		// Token: 0x04001452 RID: 5202
		[SerializeField]
		private int m_UseSettingsIndex;

		// Token: 0x04001453 RID: 5203
		[SerializeField]
		private CompareEffectViewerMain.Setting[] m_Settings;

		// Token: 0x04001454 RID: 5204
		[SerializeField]
		private Toggle m_ToggleBG;

		// Token: 0x04001455 RID: 5205
		[SerializeField]
		private Slider m_SliderZoom;

		// Token: 0x04001456 RID: 5206
		[SerializeField]
		private Slider m_SliderTimeScale;

		// Token: 0x04001457 RID: 5207
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x04001458 RID: 5208
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04001459 RID: 5209
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x0400145A RID: 5210
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x0400145B RID: 5211
		private CompareEffectViewerMain.eStep m_Step = CompareEffectViewerMain.eStep.None;

		// Token: 0x0200027A RID: 634
		[Serializable]
		public class Setting
		{
			// Token: 0x0400145C RID: 5212
			public Vector2 m_IntervalPos = new Vector2(0.25f, 1f);

			// Token: 0x0400145D RID: 5213
			public int m_ColumnNum = 5;

			// Token: 0x0400145E RID: 5214
			public string[] m_EffectIDs;
		}

		// Token: 0x0200027B RID: 635
		private enum eStep
		{
			// Token: 0x04001460 RID: 5216
			None = -1,
			// Token: 0x04001461 RID: 5217
			First,
			// Token: 0x04001462 RID: 5218
			Prepare,
			// Token: 0x04001463 RID: 5219
			Prepare_Wait,
			// Token: 0x04001464 RID: 5220
			Main
		}
	}
}
