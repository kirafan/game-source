﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000587 RID: 1415
	public class RoomFriendCharaManager : IRoomCharaManager
	{
		// Token: 0x06001B9B RID: 7067 RVA: 0x000923D9 File Offset: 0x000907D9
		public override void SetLinkManager(RoomBuilder pbuilder)
		{
			this.m_CharaTable = new RoomCharaModels[0];
			this.m_PlayFloorID = pbuilder.GetFloorNO();
			base.SetLinkManager(pbuilder);
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x000923FC File Offset: 0x000907FC
		public override bool IsBuild()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].IsPreparing())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06001B9D RID: 7069 RVA: 0x00092444 File Offset: 0x00090844
		private void Update()
		{
			int num = this.m_CharaTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].UpdatePakage(this.m_Builder);
				}
			}
		}

		// Token: 0x06001B9E RID: 7070 RVA: 0x0009248C File Offset: 0x0009088C
		public override void DeleteChara()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].Clear(this.m_Builder);
					this.m_CharaTable[i] = null;
				}
			}
		}

		// Token: 0x06001B9F RID: 7071 RVA: 0x000924DC File Offset: 0x000908DC
		public void DeleteCharacterS(int fcharaid, bool fcutout = false)
		{
			bool flag = false;
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_CharaID == fcharaid)
				{
					this.m_CharaTable[i].Clear(this.m_Builder);
					this.m_CharaTable[i] = null;
					flag = true;
					break;
				}
			}
			if (flag)
			{
				int i = 0;
				while (i < this.m_EntryNum)
				{
					if (this.m_CharaTable[i] == null)
					{
						for (int j = i + 1; j < this.m_EntryNum; j++)
						{
							this.m_CharaTable[j - 1] = this.m_CharaTable[j];
						}
						this.m_EntryNum--;
						this.m_CharaTable[this.m_EntryNum] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x06001BA0 RID: 7072 RVA: 0x000925BC File Offset: 0x000909BC
		private void CheckRoomInManageChara(int fcharaid, int froomid, bool fcutout)
		{
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_CharaID == fcharaid)
				{
					if (this.m_CharaTable[i].m_RoomID != froomid)
					{
						if (froomid == this.m_PlayFloorID)
						{
							this.CheckEntryCharaManager(fcharaid, froomid);
						}
						else
						{
							this.DeleteCharacterS(fcharaid, fcutout);
						}
					}
					break;
				}
			}
		}

		// Token: 0x06001BA1 RID: 7073 RVA: 0x0009263C File Offset: 0x00090A3C
		public override void ResetState()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_RoomObj != null)
				{
					this.m_CharaTable[i].m_RoomObj.SetNewGridStatus(this.m_Builder.GetGridStatus(0));
					this.m_CharaTable[i].m_RoomObj.CheckExistAnyOnCurrent(0);
				}
			}
		}

		// Token: 0x06001BA2 RID: 7074 RVA: 0x000926BC File Offset: 0x00090ABC
		public override void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_RoomObj != null && this.m_CharaTable[i].m_RoomObj.FloorID == 0)
				{
					this.m_CharaTable[i].m_RoomObj.CheckExistAnyOnCurrent(0);
				}
			}
		}

		// Token: 0x06001BA3 RID: 7075 RVA: 0x00092734 File Offset: 0x00090B34
		public override void SetUpRoomCharacter()
		{
			RoomFriendFieldCharaIO component = base.gameObject.GetComponent<RoomFriendFieldCharaIO>();
			if (component != null)
			{
				int roomInCharaNum = component.m_ListUp.GetRoomInCharaNum();
				for (int i = 0; i < roomInCharaNum; i++)
				{
					this.CheckEntryCharaManager(component.m_ListUp.GetRoomInCharaAt(i).CharacterID, this.m_PlayFloorID);
				}
			}
		}

		// Token: 0x06001BA4 RID: 7076 RVA: 0x00092798 File Offset: 0x00090B98
		public RoomCharaModels CheckEntryCharaManager(int fcharaid, int froomid)
		{
			RoomCharaModels roomCharaModels = new RoomCharaModels(fcharaid, froomid, this);
			for (int i = 0; i < this.m_EntryMax; i++)
			{
				if (this.m_CharaTable[i] == null)
				{
					this.m_CharaTable[i] = roomCharaModels;
					this.m_EntryNum++;
					return roomCharaModels;
				}
			}
			this.AddRoomCharaModels(roomCharaModels);
			return roomCharaModels;
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x000927F4 File Offset: 0x00090BF4
		public void AddRoomCharaModels(RoomCharaModels pakage)
		{
			if (this.m_EntryNum >= this.m_EntryMax)
			{
				RoomCharaModels[] array = new RoomCharaModels[this.m_EntryMax + 5];
				for (int i = 0; i < this.m_EntryMax; i++)
				{
					array[i] = this.m_CharaTable[i];
				}
				this.m_CharaTable = null;
				this.m_CharaTable = array;
				this.m_EntryMax += 5;
			}
			this.m_CharaTable[this.m_EntryNum] = pakage;
			this.m_EntryNum++;
		}

		// Token: 0x04002282 RID: 8834
		[SerializeField]
		public RoomCharaModels[] m_CharaTable;

		// Token: 0x04002283 RID: 8835
		public int m_EntryNum;

		// Token: 0x04002284 RID: 8836
		public int m_EntryMax;

		// Token: 0x04002285 RID: 8837
		private int m_PlayFloorID;
	}
}
