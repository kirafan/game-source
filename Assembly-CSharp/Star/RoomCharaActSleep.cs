﻿using System;

namespace Star
{
	// Token: 0x020005BB RID: 1467
	public class RoomCharaActSleep : IRoomCharaAct
	{
		// Token: 0x06001C68 RID: 7272 RVA: 0x00097FC8 File Offset: 0x000963C8
		public bool RequestSleep(RoomObjectCtrlChara pbase, int factionno)
		{
			IRoomObjectControll freeObject = pbase.GetBuilder().GetFloorManager(1).GetFreeObject(pbase, pbase.GetEntryPoint());
			if (freeObject != null)
			{
				if (this.m_EventAction != null && this.m_EventAction.EventCancel(pbase, true))
				{
					this.m_EventAction.Release();
					this.m_EventAction = null;
				}
				this.m_EventAction = RoomActionCreate.CreateEventAction(eRoomObjectEventType.Sleep);
				if (this.m_EventAction == null)
				{
					return false;
				}
				RoomEventParameter roomEventParameter = new RoomEventParameter();
				roomEventParameter.m_Table = new int[4];
				roomEventParameter.m_Table[0] = factionno;
				this.m_EventAction.RequestObjEventMode(freeObject, pbase, roomEventParameter);
			}
			return freeObject != null;
		}

		// Token: 0x06001C69 RID: 7273 RVA: 0x00098074 File Offset: 0x00096474
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			if (this.m_EventAction == null)
			{
				result = false;
			}
			else if (!this.m_EventAction.UpdateObjEventMode(pbase))
			{
				this.m_EventAction.Release();
				this.m_EventAction = null;
				result = false;
			}
			else if (pbase.IsIrqRequest())
			{
				if (this.m_EventAction.EventCancel(pbase, true))
				{
					this.m_EventAction.Release();
					this.m_EventAction = null;
					result = false;
				}
				else
				{
					pbase.CancelEventCommand();
				}
			}
			return result;
		}

		// Token: 0x06001C6A RID: 7274 RVA: 0x000980FC File Offset: 0x000964FC
		public override bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			if (this.m_EventAction != null)
			{
				this.m_EventAction.EventCancel(pbase, true);
				this.m_EventAction.Release();
				this.m_EventAction = null;
			}
			return true;
		}

		// Token: 0x04002343 RID: 9027
		private ICharaRoomAction m_EventAction;
	}
}
