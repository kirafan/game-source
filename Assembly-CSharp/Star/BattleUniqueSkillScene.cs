﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000100 RID: 256
	public class BattleUniqueSkillScene
	{
		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600073B RID: 1851 RVA: 0x0002B8D0 File Offset: 0x00029CD0
		public List<string> UnloadCueSheetNames
		{
			get
			{
				return this.m_UnloadCueSheetNames;
			}
		}

		// Token: 0x0600073C RID: 1852 RVA: 0x0002B8D8 File Offset: 0x00029CD8
		public bool Prepare(string resourceName, BattleSystem system, Camera camera, CharacterHandler owner, List<CharacterHandler> tgtCharaHndls, CharacterHandler tgtSingleCharaHndl, List<CharacterHandler> myCharaHndls, CharacterHandler mySingleCharaHndl, bool isFadeOutStart, int togetherAttackChainCount)
		{
			if (this.m_Mode != BattleUniqueSkillScene.eMode.None)
			{
				return false;
			}
			this.m_ResourceName = resourceName;
			this.m_System = system;
			this.m_Camera = camera;
			this.m_Owner = owner;
			this.m_TgtCharaHndls = tgtCharaHndls;
			this.m_TgtSingleCharaHndl = tgtSingleCharaHndl;
			this.m_MyCharaHndls = myCharaHndls;
			this.m_MySingleCharaHndl = mySingleCharaHndl;
			this.m_IsFadeOutStart = isFadeOutStart;
			this.m_TogetherAttackChainCount = togetherAttackChainCount;
			this.m_Mode = BattleUniqueSkillScene.eMode.Prepare;
			return true;
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x0002B948 File Offset: 0x00029D48
		public bool IsCompltePrepare()
		{
			return this.m_Mode == BattleUniqueSkillScene.eMode.Prepare && this.m_PrepareStep == BattleUniqueSkillScene.ePrepareStep.End;
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x0002B961 File Offset: 0x00029D61
		public bool IsPrepareError()
		{
			return this.m_Mode == BattleUniqueSkillScene.eMode.Prepare && this.m_PrepareStep == BattleUniqueSkillScene.ePrepareStep.Prepare_Error;
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x0002B97A File Offset: 0x00029D7A
		public bool Destory()
		{
			if (!this.IsCompltePlay())
			{
				return false;
			}
			this.m_Mode = BattleUniqueSkillScene.eMode.Destroy;
			return true;
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0002B991 File Offset: 0x00029D91
		public bool IsComplteDestory()
		{
			return this.m_Mode == BattleUniqueSkillScene.eMode.Destroy && this.m_DestroyStep == BattleUniqueSkillScene.eDestroyStep.End;
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x0002B9AB File Offset: 0x00029DAB
		public bool Play()
		{
			if (!this.IsCompltePrepare())
			{
				return false;
			}
			this.m_Mode = BattleUniqueSkillScene.eMode.UpdateMain;
			return true;
		}

		// Token: 0x06000742 RID: 1858 RVA: 0x0002B9C2 File Offset: 0x00029DC2
		public bool IsCompltePlay()
		{
			return this.m_Mode == BattleUniqueSkillScene.eMode.UpdateMain && this.m_MainStep == BattleUniqueSkillScene.eMainStep.End;
		}

		// Token: 0x06000743 RID: 1859 RVA: 0x0002B9DC File Offset: 0x00029DDC
		public void Update()
		{
			if (this.m_BaseObjectHandler != null)
			{
				this.m_BaseObjectHandler.Update();
			}
			BattleUniqueSkillScene.eMode eMode = this.m_Mode;
			BattleUniqueSkillScene.eMode mode = this.m_Mode;
			if (mode != BattleUniqueSkillScene.eMode.Prepare)
			{
				if (mode != BattleUniqueSkillScene.eMode.UpdateMain)
				{
					if (mode == BattleUniqueSkillScene.eMode.Destroy)
					{
						eMode = this.Update_Destroy();
					}
				}
				else
				{
					eMode = this.Update_Main();
				}
			}
			else
			{
				eMode = this.Update_Prepare();
			}
			if (eMode != this.m_Mode)
			{
				this.m_Mode = eMode;
				BattleUniqueSkillScene.eMode mode2 = this.m_Mode;
				if (mode2 != BattleUniqueSkillScene.eMode.Prepare)
				{
					if (mode2 != BattleUniqueSkillScene.eMode.UpdateMain)
					{
						if (mode2 == BattleUniqueSkillScene.eMode.Destroy)
						{
							this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.First);
						}
					}
					else
					{
						this.SetMainStep(BattleUniqueSkillScene.eMainStep.First);
					}
				}
				else
				{
					this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.First);
				}
			}
		}

		// Token: 0x06000744 RID: 1860 RVA: 0x0002BAA8 File Offset: 0x00029EA8
		public void LateUpdate()
		{
			int currentFrame = 0;
			if (this.m_Mode == BattleUniqueSkillScene.eMode.UpdateMain && this.m_OwnerControllHandler != null)
			{
				currentFrame = (int)(this.m_OwnerControllHandler.GetPlayingSec() / 0.033333335f);
			}
			for (int i = 0; i < this.m_SoundFrameControllers.Length; i++)
			{
				if (this.m_SoundFrameControllers[i] != null)
				{
					this.m_SoundFrameControllers[i].Update(currentFrame);
				}
			}
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x0002BB1C File Offset: 0x00029F1C
		private SkillActionPlayer.RefAnimTime SkillActionPlayerUpdateRefAnimTime()
		{
			SkillActionPlayer.RefAnimTime result;
			result.m_PlayingSec = this.m_OwnerControllHandler.GetPlayingSec();
			result.m_MaxSec = this.m_OwnerControllHandler.GetMaxSec();
			return result;
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x0002BB50 File Offset: 0x00029F50
		private string GetResourcePath()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("UniqueSkill/UniqueSkill_");
			stringBuilder.Append(this.m_ResourceName);
			stringBuilder.Append(".muast");
			return stringBuilder.ToString();
		}

		// Token: 0x06000747 RID: 1863 RVA: 0x0002BB8E File Offset: 0x00029F8E
		private string GetOwnerAnimObjectPath(CharacterDefine.ePlayerModelParts parts)
		{
			if (parts == CharacterDefine.ePlayerModelParts.Head)
			{
				return "MeigeAC_owner_head@skill";
			}
			if (parts != CharacterDefine.ePlayerModelParts.Body)
			{
				return "MeigeAC_owner@skill";
			}
			return "MeigeAC_owner_body@skill";
		}

		// Token: 0x06000748 RID: 1864 RVA: 0x0002BBB3 File Offset: 0x00029FB3
		private string GetBaseModelObjectPath()
		{
			return "UniqueSkill";
		}

		// Token: 0x06000749 RID: 1865 RVA: 0x0002BBBA File Offset: 0x00029FBA
		private string GetBaseAnimObjectPath()
		{
			return "MeigeAC_UniqueSkill@Take 001";
		}

		// Token: 0x0600074A RID: 1866 RVA: 0x0002BBC4 File Offset: 0x00029FC4
		private static eSoundBgmListDB GetBgmCueID(eRare rare, eClassType classType)
		{
			if (rare != eRare.Star4)
			{
				if (rare == eRare.Star5)
				{
					switch (classType)
					{
					case eClassType.Fighter:
						return eSoundBgmListDB.BGM_5_FIGHTER;
					case eClassType.Magician:
						return eSoundBgmListDB.BGM_5_MAGICIAN;
					case eClassType.Priest:
						return eSoundBgmListDB.BGM_5_PRIEST;
					case eClassType.Knight:
						return eSoundBgmListDB.BGM_5_KNIGHT;
					case eClassType.Alchemist:
						return eSoundBgmListDB.BGM_5_ALCHMIST;
					}
				}
				return eSoundBgmListDB.None;
			}
			return eSoundBgmListDB.BGM_4_COMMON;
		}

		// Token: 0x0600074B RID: 1867 RVA: 0x0002BC2D File Offset: 0x0002A02D
		public Transform GetLocator(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			if (this.m_BaseObjectHandler != null)
			{
				return this.m_BaseObjectHandler.GetLocator(locatorType);
			}
			return null;
		}

		// Token: 0x0600074C RID: 1868 RVA: 0x0002BC50 File Offset: 0x0002A050
		public List<CharacterHandler> GetSortTargetCharaHndls()
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			if (this.m_Owner != null)
			{
				list.Add(this.m_Owner);
			}
			if (this.m_TgtCharaHndls != null)
			{
				for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
				{
					list.Add(this.m_TgtCharaHndls[i]);
				}
			}
			if (this.m_MyCharaHndls != null)
			{
				for (int j = 0; j < this.m_MyCharaHndls.Count; j++)
				{
					list.Add(this.m_MyCharaHndls[j]);
				}
			}
			return list;
		}

		// Token: 0x0600074D RID: 1869 RVA: 0x0002BCF3 File Offset: 0x0002A0F3
		public List<BattleUniqueSkillBaseObjectHandler.CharaRender> GetCharaRenderCache()
		{
			if (this.m_BaseObjectHandler != null)
			{
				return this.m_BaseObjectHandler.GetCharaRenderCache();
			}
			return null;
		}

		// Token: 0x0600074E RID: 1870 RVA: 0x0002BD13 File Offset: 0x0002A113
		public List<Renderer> GetRenderCache()
		{
			if (this.m_BaseObjectHandler != null)
			{
				return this.m_BaseObjectHandler.GetRenderCache();
			}
			return null;
		}

		// Token: 0x0600074F RID: 1871 RVA: 0x0002BD34 File Offset: 0x0002A134
		public void SetBaseObjectParticleSortingOrder(int sortingOrder)
		{
			if (this.m_BaseObjectHandler != null && this.m_BaseObjectHandler.MsbHndl != null)
			{
				int particleEmitterNum = this.m_BaseObjectHandler.MsbHndl.GetParticleEmitterNum();
				for (int i = 0; i < particleEmitterNum; i++)
				{
					this.m_BaseObjectHandler.MsbHndl.GetParticleEmitter(i).SetSortingOrder(sortingOrder);
				}
			}
		}

		// Token: 0x06000750 RID: 1872 RVA: 0x0002BDA2 File Offset: 0x0002A1A2
		private void SetDestroyStep(BattleUniqueSkillScene.eDestroyStep step)
		{
			this.m_DestroyStep = step;
		}

		// Token: 0x06000751 RID: 1873 RVA: 0x0002BDAC File Offset: 0x0002A1AC
		private BattleUniqueSkillScene.eMode Update_Destroy()
		{
			this.m_Loader.Update();
			switch (this.m_DestroyStep)
			{
			case BattleUniqueSkillScene.eDestroyStep.First:
				this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.Transit_Wait);
				break;
			case BattleUniqueSkillScene.eDestroyStep.Transit_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					if (this.m_System != null)
					{
						this.m_System.SkillActionPlayer.Destroy();
					}
					this.m_BaseObjectHandler.Pause();
					this.m_OwnerControllHandler.Pause();
					this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.Destroy_0);
				}
				break;
			case BattleUniqueSkillScene.eDestroyStep.Destroy_0:
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.Unload(this.m_LoadingHndl);
					this.m_LoadingHndl = null;
				}
				this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.Destroy_1);
				break;
			case BattleUniqueSkillScene.eDestroyStep.Destroy_1:
			{
				for (int i = 0; i < this.m_SoundFrameControllers.Length; i++)
				{
					if (this.m_SoundFrameControllers[i] != null)
					{
						this.m_SoundFrameControllers[i].Destroy(false, false);
						this.m_UnloadCueSheetNames.Add(this.m_SoundFrameControllers[i].CueSheetName);
						this.m_SoundFrameControllers[i] = null;
					}
				}
				this.m_BaseObjectHandler.Destroy();
				this.m_BaseObjectHandler = null;
				this.m_OwnerControllHandler.Destroy();
				this.m_OwnerControllHandler = null;
				this.m_Owner = null;
				this.m_TgtCharaHndls = null;
				this.m_MyCharaHndls = null;
				CharacterManager charaMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng;
				int charaNum = charaMng.GetCharaNum();
				for (int j = 0; j < charaNum; j++)
				{
					CharacterHandler charaAt = charaMng.GetCharaAt(j);
					if (charaAt != null && charaAt.CharaBattle != null)
					{
						charaAt.CharaBattle.RevertFromUniqueSkillScene();
					}
				}
				if (this.m_System != null)
				{
					this.m_System.RevertUniqueSkillSceneToSystem();
				}
				this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.Destroy_2);
				break;
			}
			case BattleUniqueSkillScene.eDestroyStep.Destroy_2:
				UnityEngine.Object.Destroy(this.m_BaseObject);
				this.m_BaseObject = null;
				UnityEngine.Object.Destroy(this.m_OwnerControllObject);
				this.m_OwnerControllObject = null;
				SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
				this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.Destroy_3);
				break;
			case BattleUniqueSkillScene.eDestroyStep.Destroy_3:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
				{
					this.SetDestroyStep(BattleUniqueSkillScene.eDestroyStep.End);
				}
				break;
			}
			return BattleUniqueSkillScene.eMode.Destroy;
		}

		// Token: 0x06000752 RID: 1874 RVA: 0x0002BFF8 File Offset: 0x0002A3F8
		private void SetMainStep(BattleUniqueSkillScene.eMainStep step)
		{
			this.m_MainStep = step;
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x0002C004 File Offset: 0x0002A404
		private BattleUniqueSkillScene.eMode Update_Main()
		{
			switch (this.m_MainStep)
			{
			case BattleUniqueSkillScene.eMainStep.First:
				this.m_OwnerControllHandler.Play();
				this.m_BaseObjectHandler.Play();
				if (this.m_System != null)
				{
					BattleCommandData uniqueSkillCommandData = this.m_Owner.CharaBattle.GetUniqueSkillCommandData();
					SkillActionPlan sap = this.m_Owner.CharaBattle.SAPs[uniqueSkillCommandData.SAP_ID];
					SkillActionGraphics sag = this.m_Owner.CharaBattle.SAGs[uniqueSkillCommandData.SAG_ID];
					List<CharacterHandler> tgtCharaList;
					List<CharacterHandler> myCharaList;
					BattleCommandParser.GetTakeCharaList(uniqueSkillCommandData, out tgtCharaList, out myCharaList);
					this.m_System.SkillActionPlayer.Play(sap, sag, this.m_Owner, null, tgtCharaList, myCharaList, new SkillActionPlayer.UpdateRefAnimTimeDelegate(this.SkillActionPlayerUpdateRefAnimTime));
					this.m_System.BattleUI.GetUniqueSkillNameWindow().SetText(uniqueSkillCommandData.SkillParam.m_SkillName, null, uniqueSkillCommandData.SkillLv);
				}
				for (int i = 0; i < this.m_SoundFrameControllers.Length; i++)
				{
					if (this.m_SoundFrameControllers[i] != null)
					{
						this.m_SoundFrameControllers[i].Play();
					}
				}
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(0f);
				if (this.m_System != null)
				{
					this.m_System.BattleUI.GetTransitFade().PlayInQuick(BattleUniqueSkillScene.FADE_COLOR);
					this.m_System.BattleUI.GetTransitFade().PlayOut();
				}
				if (this.m_IsResumeBgm)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
					this.m_IsResumeBgm = false;
				}
				this.SetMainStep(BattleUniqueSkillScene.eMainStep.Playing);
				break;
			case BattleUniqueSkillScene.eMainStep.Playing:
				if (this.m_System != null)
				{
					this.m_System.RequestSort();
				}
				if (!this.m_WasStartedLastFadeOut)
				{
					float num = this.m_OwnerControllHandler.GetMaxSec() - 0.12f;
					if (this.m_OwnerControllHandler.GetPlayingSec() >= num)
					{
						this.m_WasStartedLastFadeOut = true;
						if (this.m_System != null)
						{
							this.m_System.BattleUI.GetTransitFade().PlayIn(BattleUniqueSkillScene.FADE_COLOR);
						}
					}
				}
				if (!this.m_WasStartedLastFadeOut)
				{
					if (this.m_IsSkippable)
					{
						Vector2 vector;
						if (InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0))
						{
						}
					}
					else if (this.m_OwnerControllHandler.GetPlayingSec() >= 1f)
					{
						this.m_IsSkippable = true;
					}
				}
				if (this.m_ExecutedSkip)
				{
					this.m_WasStartedLastFadeOut = true;
					if (this.m_System != null)
					{
						this.m_System.BattleUI.GetTransitFade().PlayIn(BattleUniqueSkillScene.FADE_COLOR);
					}
					this.SetMainStep(BattleUniqueSkillScene.eMainStep.SkipWait);
				}
				else if (!this.m_OwnerControllHandler.IsPlaying())
				{
					if (!this.m_BaseObjectHandler.IsPlaying())
					{
						if (!(this.m_System != null) || this.m_System.BattleUI.GetTransitFade().IsShowComplete())
						{
							this.SetMainStep(BattleUniqueSkillScene.eMainStep.End);
						}
					}
				}
				break;
			case BattleUniqueSkillScene.eMainStep.SkipWait:
				if (!(this.m_System != null) || this.m_System.BattleUI.GetTransitFade().IsShowComplete())
				{
					if (this.m_System != null)
					{
						this.m_System.SkillActionPlayer.Skip();
					}
					this.SetMainStep(BattleUniqueSkillScene.eMainStep.End);
				}
				break;
			}
			return BattleUniqueSkillScene.eMode.UpdateMain;
		}

		// Token: 0x06000754 RID: 1876 RVA: 0x0002C397 File Offset: 0x0002A797
		private void SetPrepareStep(BattleUniqueSkillScene.ePrepareStep step)
		{
			this.m_PrepareStep = step;
		}

		// Token: 0x06000755 RID: 1877 RVA: 0x0002C3A0 File Offset: 0x0002A7A0
		private BattleUniqueSkillScene.eMode Update_Prepare()
		{
			this.m_Loader.Update();
			switch (this.m_PrepareStep)
			{
			case BattleUniqueSkillScene.ePrepareStep.First:
				if (this.m_IsFadeOutStart && this.m_System != null)
				{
					this.m_System.BattleUI.GetTransitFade().PlayIn(BattleUniqueSkillScene.FADE_COLOR);
				}
				this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.BattleSystem_PreProcess);
				break;
			case BattleUniqueSkillScene.ePrepareStep.BattleSystem_PreProcess:
				if (!(this.m_System != null) || this.m_System.BattleUI.GetTransitFade().IsShowComplete())
				{
					CharacterManager charaMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng;
					int charaNum = charaMng.GetCharaNum();
					for (int i = 0; i < charaNum; i++)
					{
						CharacterHandler charaAt = charaMng.GetCharaAt(i);
						if (charaAt != null)
						{
							if (charaAt.CharaBattle != null)
							{
								charaAt.CharaBattle.SetupForUniqueSkillScene();
								if (charaAt == this.m_Owner)
								{
									charaAt.CharaBattle.SetTogetherAttackCommand(this.m_TogetherAttackChainCount);
								}
							}
							if (charaAt.CharaAnim != null)
							{
								charaAt.CharaAnim.SetSortingOrder(0);
							}
							if (charaAt == this.m_Owner)
							{
								charaAt.SetEnableRender(true);
							}
						}
					}
					if (this.m_System != null)
					{
						this.m_System.UniqueSkillScenePreProcess();
					}
					this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.Prepare);
				}
				break;
			case BattleUniqueSkillScene.ePrepareStep.Prepare:
				this.m_LoadingHndl = this.m_Loader.Load(this.GetResourcePath(), new MeigeResource.Option[0]);
				if (this.m_LoadingHndl != null)
				{
					BattleCommandData uniqueSkillCommandData = this.m_Owner.CharaBattle.GetUniqueSkillCommandData();
					SoundFrameController soundFrameController = this.m_SoundFrameControllers[0] = new SoundFrameController(SingletonMonoBehaviour<GameSystem>.Inst.SoundMng, uniqueSkillCommandData.SkillParam.m_UniqueSkillSeCueSheet, (!string.IsNullOrEmpty(uniqueSkillCommandData.SkillParam.m_UniqueSkillSeCueSheet)) ? (uniqueSkillCommandData.SkillParam.m_UniqueSkillSeCueSheet + ".acb") : string.Empty, null);
					SingletonMonoBehaviour<GameSystem>.Inst.DbMng.UniqueSkillSoundDB.SetSoundFrameController(uniqueSkillCommandData.SkillParam.m_UniqueSkillSeCueSheet, soundFrameController);
					soundFrameController.Prepare(15);
					soundFrameController = (this.m_SoundFrameControllers[1] = new SoundFrameController(SingletonMonoBehaviour<GameSystem>.Inst.SoundMng, uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet, (!string.IsNullOrEmpty(uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet)) ? (uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet + ".acb") : string.Empty, (!string.IsNullOrEmpty(uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet)) ? (uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet + ".awb") : string.Empty));
					SingletonMonoBehaviour<GameSystem>.Inst.DbMng.UniqueSkillSoundDB.SetSoundFrameController(uniqueSkillCommandData.SkillParam.m_UniqueSkillVoiceCueSheet, soundFrameController);
					soundFrameController.Prepare(15);
					this.m_BgmCueID = BattleUniqueSkillScene.GetBgmCueID(this.m_Owner.CharaParam.RareType, this.m_Owner.CharaParam.ClassType);
					if (this.m_BgmCueID != eSoundBgmListDB.None && !SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(this.m_BgmCueID))
					{
						this.m_IsResumeBgm = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PrepareBGM(this.m_BgmCueID, 1f, 0, -1, -1);
					}
					this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.Prepare_Wait);
				}
				break;
			case BattleUniqueSkillScene.ePrepareStep.Prepare_Wait:
				if (!this.m_IsResumeBgm || SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsCompletePrepareBGM())
				{
					bool flag = true;
					for (int j = 0; j < this.m_SoundFrameControllers.Length; j++)
					{
						if (this.m_SoundFrameControllers[j] != null && !this.m_SoundFrameControllers[j].IsCompletePrepare())
						{
							flag = false;
							break;
						}
					}
					if (flag)
					{
						if (this.m_LoadingHndl != null)
						{
							if (this.m_LoadingHndl.IsError())
							{
								this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.Prepare_Error);
								break;
							}
							if (!this.m_LoadingHndl.IsDone())
							{
								break;
							}
						}
						this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.Setup);
					}
				}
				break;
			case BattleUniqueSkillScene.ePrepareStep.Setup:
			{
				this.m_OwnerControllObject = new GameObject("BattleUniqueSkillScene_OwnerControll");
				this.m_OwnerControllHandler = this.m_OwnerControllObject.AddComponent<BattleUniqueSkillOwnerControllHandler>();
				List<MeigeAnimClipHolder> list = new List<MeigeAnimClipHolder>();
				for (int k = 0; k < this.m_Owner.CharaAnim.ModelSetsNum; k++)
				{
					string ownerAnimObjectPath = this.GetOwnerAnimObjectPath((CharacterDefine.ePlayerModelParts)k);
					GameObject gameObject = this.m_LoadingHndl.FindObj(ownerAnimObjectPath, true) as GameObject;
					if (gameObject != null)
					{
						list.Add(gameObject.GetComponentInChildren<MeigeAnimClipHolder>());
					}
				}
				this.m_OwnerControllHandler.Setup(this.m_Owner, list);
				GameObject gameObject2 = this.m_LoadingHndl.FindObj(this.GetBaseModelObjectPath(), true) as GameObject;
				GameObject gameObject3 = this.m_LoadingHndl.FindObj(this.GetBaseAnimObjectPath(), true) as GameObject;
				this.m_BaseObject = UnityEngine.Object.Instantiate<GameObject>(gameObject2);
				this.m_BaseObjectHandler = this.m_BaseObject.AddComponent<BattleUniqueSkillBaseObjectHandler>();
				MeigeAnimClipHolder componentInChildren = gameObject3.GetComponentInChildren<MeigeAnimClipHolder>();
				this.m_BaseObjectHandler.Setup(gameObject2.name, componentInChildren, this.m_Owner, this.m_TgtCharaHndls, this.m_TgtSingleCharaHndl, this.m_MyCharaHndls, this.m_MySingleCharaHndl);
				this.m_BaseObjectHandler.AttachCamera(this.m_Camera);
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.Unload(this.m_LoadingHndl);
					this.m_LoadingHndl = null;
				}
				this.SetPrepareStep(BattleUniqueSkillScene.ePrepareStep.End);
				break;
			}
			case BattleUniqueSkillScene.ePrepareStep.End:
				return BattleUniqueSkillScene.eMode.UpdateMain;
			}
			return BattleUniqueSkillScene.eMode.Prepare;
		}

		// Token: 0x06000756 RID: 1878 RVA: 0x0002C98A File Offset: 0x0002AD8A
		[Conditional("APP_DEBUG")]
		private void StartStopwatchForDebugPrepare(int prepareTarget)
		{
		}

		// Token: 0x06000757 RID: 1879 RVA: 0x0002C98C File Offset: 0x0002AD8C
		[Conditional("APP_DEBUG")]
		private void StopStopwatchForDebugPrepare(int prepareTarget, string opt)
		{
		}

		// Token: 0x0400067D RID: 1661
		public static readonly Color FADE_COLOR = Color.white;

		// Token: 0x0400067E RID: 1662
		public const float FADE_TIME = 0.12f;

		// Token: 0x0400067F RID: 1663
		private BattleUniqueSkillScene.eMode m_Mode = BattleUniqueSkillScene.eMode.None;

		// Token: 0x04000680 RID: 1664
		private string m_ResourceName = string.Empty;

		// Token: 0x04000681 RID: 1665
		private BattleSystem m_System;

		// Token: 0x04000682 RID: 1666
		private Camera m_Camera;

		// Token: 0x04000683 RID: 1667
		private CharacterHandler m_Owner;

		// Token: 0x04000684 RID: 1668
		private List<CharacterHandler> m_TgtCharaHndls;

		// Token: 0x04000685 RID: 1669
		private CharacterHandler m_TgtSingleCharaHndl;

		// Token: 0x04000686 RID: 1670
		private List<CharacterHandler> m_MyCharaHndls;

		// Token: 0x04000687 RID: 1671
		private CharacterHandler m_MySingleCharaHndl;

		// Token: 0x04000688 RID: 1672
		private bool m_IsFadeOutStart;

		// Token: 0x04000689 RID: 1673
		private int m_TogetherAttackChainCount;

		// Token: 0x0400068A RID: 1674
		private GameObject m_OwnerControllObject;

		// Token: 0x0400068B RID: 1675
		private BattleUniqueSkillOwnerControllHandler m_OwnerControllHandler;

		// Token: 0x0400068C RID: 1676
		private GameObject m_BaseObject;

		// Token: 0x0400068D RID: 1677
		private BattleUniqueSkillBaseObjectHandler m_BaseObjectHandler;

		// Token: 0x0400068E RID: 1678
		private SoundFrameController[] m_SoundFrameControllers = new SoundFrameController[2];

		// Token: 0x0400068F RID: 1679
		private eSoundBgmListDB m_BgmCueID = eSoundBgmListDB.None;

		// Token: 0x04000690 RID: 1680
		private bool m_IsResumeBgm;

		// Token: 0x04000691 RID: 1681
		private List<string> m_UnloadCueSheetNames = new List<string>();

		// Token: 0x04000692 RID: 1682
		private ABResourceLoader m_Loader = new ABResourceLoader(10);

		// Token: 0x04000693 RID: 1683
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000694 RID: 1684
		private BattleUniqueSkillScene.eDestroyStep m_DestroyStep;

		// Token: 0x04000695 RID: 1685
		public const float SKIPPABLE_TIME_FROM_START = 1f;

		// Token: 0x04000696 RID: 1686
		private bool m_IsSkippable;

		// Token: 0x04000697 RID: 1687
		private bool m_ExecutedSkip;

		// Token: 0x04000698 RID: 1688
		private bool m_WasStartedLastFadeOut;

		// Token: 0x04000699 RID: 1689
		private BattleUniqueSkillScene.eMainStep m_MainStep;

		// Token: 0x0400069A RID: 1690
		private BattleUniqueSkillScene.ePrepareStep m_PrepareStep;

		// Token: 0x02000101 RID: 257
		public enum eMode
		{
			// Token: 0x0400069C RID: 1692
			None = -1,
			// Token: 0x0400069D RID: 1693
			Prepare,
			// Token: 0x0400069E RID: 1694
			UpdateMain,
			// Token: 0x0400069F RID: 1695
			Destroy
		}

		// Token: 0x02000102 RID: 258
		private enum eSoundFrameControll
		{
			// Token: 0x040006A1 RID: 1697
			SE,
			// Token: 0x040006A2 RID: 1698
			VOICE,
			// Token: 0x040006A3 RID: 1699
			Num
		}

		// Token: 0x02000103 RID: 259
		private enum eDestroyStep
		{
			// Token: 0x040006A5 RID: 1701
			None = -1,
			// Token: 0x040006A6 RID: 1702
			First,
			// Token: 0x040006A7 RID: 1703
			Transit_Wait,
			// Token: 0x040006A8 RID: 1704
			Destroy_0,
			// Token: 0x040006A9 RID: 1705
			Destroy_1,
			// Token: 0x040006AA RID: 1706
			Destroy_2,
			// Token: 0x040006AB RID: 1707
			Destroy_3,
			// Token: 0x040006AC RID: 1708
			End
		}

		// Token: 0x02000104 RID: 260
		private enum eMainStep
		{
			// Token: 0x040006AE RID: 1710
			None = -1,
			// Token: 0x040006AF RID: 1711
			First,
			// Token: 0x040006B0 RID: 1712
			Playing,
			// Token: 0x040006B1 RID: 1713
			SkipWait,
			// Token: 0x040006B2 RID: 1714
			End
		}

		// Token: 0x02000105 RID: 261
		private enum ePrepareStep
		{
			// Token: 0x040006B4 RID: 1716
			None = -1,
			// Token: 0x040006B5 RID: 1717
			First,
			// Token: 0x040006B6 RID: 1718
			BattleSystem_PreProcess,
			// Token: 0x040006B7 RID: 1719
			Prepare,
			// Token: 0x040006B8 RID: 1720
			Prepare_Wait,
			// Token: 0x040006B9 RID: 1721
			Prepare_Error,
			// Token: 0x040006BA RID: 1722
			Setup,
			// Token: 0x040006BB RID: 1723
			End
		}
	}
}
