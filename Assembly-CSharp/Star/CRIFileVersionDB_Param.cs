﻿using System;

namespace Star
{
	// Token: 0x02000168 RID: 360
	[Serializable]
	public struct CRIFileVersionDB_Param
	{
		// Token: 0x040009B1 RID: 2481
		public string m_FileName;

		// Token: 0x040009B2 RID: 2482
		public byte m_FirstDL;

		// Token: 0x040009B3 RID: 2483
		public uint m_Version;

		// Token: 0x040009B4 RID: 2484
		public byte m_IsAcb;

		// Token: 0x040009B5 RID: 2485
		public byte m_IsAwb;

		// Token: 0x040009B6 RID: 2486
		public byte m_IsAcf;

		// Token: 0x040009B7 RID: 2487
		public byte m_IsUsm;
	}
}
