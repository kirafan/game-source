﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Star
{
	// Token: 0x020000D5 RID: 213
	public class BattleOrder
	{
		// Token: 0x060005A7 RID: 1447 RVA: 0x0001D1E5 File Offset: 0x0001B5E5
		public BattleOrder(BattleTogetherGauge gauge)
		{
			this.m_TogetherData = new BattleOrder.TogetherData();
			this.m_Gauge = gauge;
			this.m_Frames = new List<BattleOrder.FrameData>();
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x0001D20A File Offset: 0x0001B60A
		public void Reset()
		{
			this.m_Frames = new List<BattleOrder.FrameData>();
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x0001D217 File Offset: 0x0001B617
		public BattleOrder.TogetherData GetTogetherData()
		{
			return this.m_TogetherData;
		}

		// Token: 0x060005AA RID: 1450 RVA: 0x0001D21F File Offset: 0x0001B61F
		public BattleTogetherGauge GetGauge()
		{
			return this.m_Gauge;
		}

		// Token: 0x060005AB RID: 1451 RVA: 0x0001D227 File Offset: 0x0001B627
		public void AddFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
		{
			this.m_Frames.Add(new BattleOrder.FrameData(frameType, owner, orderValue, cardArgs));
		}

		// Token: 0x060005AC RID: 1452 RVA: 0x0001D23E File Offset: 0x0001B63E
		public void InsertFrameData(int index, BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
		{
			this.m_Frames.Insert(index, new BattleOrder.FrameData(frameType, owner, orderValue, cardArgs));
		}

		// Token: 0x060005AD RID: 1453 RVA: 0x0001D258 File Offset: 0x0001B658
		public void RemoveFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, bool isRemoveIsFirstOnly = false)
		{
			if (!isRemoveIsFirstOnly)
			{
				for (int i = this.m_Frames.Count - 1; i >= 0; i--)
				{
					if (this.m_Frames[i].m_FrameType == frameType && this.m_Frames[i].m_Owner == owner)
					{
						this.m_Frames.RemoveAt(i);
					}
				}
			}
			else
			{
				for (int j = 0; j < this.m_Frames.Count; j++)
				{
					if (this.m_Frames[j].m_FrameType == frameType && this.m_Frames[j].m_Owner == owner)
					{
						this.m_Frames.RemoveAt(j);
						break;
					}
				}
			}
		}

		// Token: 0x060005AE RID: 1454 RVA: 0x0001D330 File Offset: 0x0001B730
		public void RemoveFrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, out int out_removeIndex, out float out_orderValue)
		{
			out_removeIndex = -1;
			out_orderValue = -1f;
			for (int i = this.m_Frames.Count - 1; i >= 0; i--)
			{
				if (this.m_Frames[i].m_FrameType == frameType && this.m_Frames[i].m_Owner == owner)
				{
					out_removeIndex = i;
					out_orderValue = this.m_Frames[i].m_OrderValue;
					this.m_Frames.RemoveAt(i);
				}
			}
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x0001D3BC File Offset: 0x0001B7BC
		public void RemoveFrameDataAt(int index)
		{
			this.m_Frames.RemoveAt(index);
		}

		// Token: 0x060005B0 RID: 1456 RVA: 0x0001D3CC File Offset: 0x0001B7CC
		public void Shuffle()
		{
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				int index = UnityEngine.Random.Range(0, this.m_Frames.Count);
				BattleOrder.FrameData value = this.m_Frames[index];
				this.m_Frames[index] = this.m_Frames[0];
				this.m_Frames[0] = value;
			}
		}

		// Token: 0x060005B1 RID: 1457 RVA: 0x0001D439 File Offset: 0x0001B839
		public void SortOrder()
		{
			List<BattleOrder.FrameData> frames = this.m_Frames;
			if (BattleOrder.<>f__mg$cache0 == null)
			{
				BattleOrder.<>f__mg$cache0 = new Comparison<BattleOrder.FrameData>(BattleOrder.CompareFrame);
			}
			Sort.StableSort<BattleOrder.FrameData>(frames, BattleOrder.<>f__mg$cache0);
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x0001D463 File Offset: 0x0001B863
		private static int CompareFrame(BattleOrder.FrameData A, BattleOrder.FrameData B)
		{
			if (A.m_OrderValue > B.m_OrderValue)
			{
				return 1;
			}
			if (A.m_OrderValue < B.m_OrderValue)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x0001D48C File Offset: 0x0001B88C
		public void UpdateOrderValue()
		{
			BattleOrder.FrameData frameDataAt = this.GetFrameDataAt(0);
			if (frameDataAt == null)
			{
				return;
			}
			float orderValue = frameDataAt.m_OrderValue;
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				this.m_Frames[i].m_OrderValue -= orderValue;
				if (this.m_Frames[i].m_OrderValue < 0f)
				{
					this.m_Frames[i].m_OrderValue = 0f;
				}
			}
		}

		// Token: 0x060005B4 RID: 1460 RVA: 0x0001D515 File Offset: 0x0001B915
		public int GetFrameNum()
		{
			return this.m_Frames.Count;
		}

		// Token: 0x060005B5 RID: 1461 RVA: 0x0001D522 File Offset: 0x0001B922
		public BattleOrder.FrameData GetFrameDataAt(int index)
		{
			if (index < 0 || index >= this.m_Frames.Count)
			{
				return null;
			}
			return this.m_Frames[index];
		}

		// Token: 0x060005B6 RID: 1462 RVA: 0x0001D54A File Offset: 0x0001B94A
		public CharacterHandler GetFrameOwnerAt(int index)
		{
			if (index < 0 || index >= this.m_Frames.Count)
			{
				return null;
			}
			return this.m_Frames[index].m_Owner;
		}

		// Token: 0x060005B7 RID: 1463 RVA: 0x0001D578 File Offset: 0x0001B978
		public BattleOrder.FrameData FindCardFrameData(CharacterHandler executor, int commandIndex)
		{
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				if (this.m_Frames[i].IsCardType && this.m_Frames[i].m_CardArgs.m_Command.Owner == executor && this.m_Frames[i].m_CardArgs.m_CommandIndexWasCreated == commandIndex)
				{
					return this.m_Frames[i];
				}
			}
			return null;
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x0001D608 File Offset: 0x0001BA08
		public void SaveFrames()
		{
			this.m_SaveFrames = new List<BattleOrder.FrameData>();
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				this.m_SaveFrames.Add(new BattleOrder.FrameData(this.m_Frames[i].m_FrameType, this.m_Frames[i].m_Owner, this.m_Frames[i].m_OrderValue, this.m_Frames[i].m_CardArgs));
			}
		}

		// Token: 0x060005B9 RID: 1465 RVA: 0x0001D690 File Offset: 0x0001BA90
		public void RevertFrames()
		{
			this.m_Frames = new List<BattleOrder.FrameData>();
			for (int i = 0; i < this.m_SaveFrames.Count; i++)
			{
				this.m_Frames.Add(new BattleOrder.FrameData(this.m_SaveFrames[i].m_FrameType, this.m_SaveFrames[i].m_Owner, this.m_SaveFrames[i].m_OrderValue, this.m_SaveFrames[i].m_CardArgs));
			}
		}

		// Token: 0x060005BA RID: 1466 RVA: 0x0001D718 File Offset: 0x0001BB18
		public int MoveTopFrameWhileFixing(float newOrderValue)
		{
			this.RevertFrames();
			int num = 0;
			for (int i = num; i < this.m_Frames.Count; i++)
			{
				if (this.m_Frames[i].m_OrderValue > newOrderValue)
				{
					break;
				}
				num = i;
			}
			num++;
			BattleOrder.FrameData frameDataAt = this.GetFrameDataAt(0);
			this.m_Frames.Insert(num, new BattleOrder.FrameData(frameDataAt.m_FrameType, frameDataAt.m_Owner, newOrderValue, frameDataAt.m_CardArgs));
			return num;
		}

		// Token: 0x060005BB RID: 1467 RVA: 0x0001D798 File Offset: 0x0001BB98
		public void SlideFrames()
		{
			this.m_Frames.RemoveAt(0);
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x0001D7A8 File Offset: 0x0001BBA8
		public bool CanBeTogehterAttack()
		{
			List<BattleDefine.eJoinMember> list = this.CalcTogetherAttackDefaultOrders();
			return list != null && list.Count > 0;
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x0001D7D4 File Offset: 0x0001BBD4
		public List<BattleDefine.eJoinMember> CalcTogetherAttackDefaultOrders()
		{
			int num = 0;
			int num2 = 0;
			return this.CalcTogetherAttackDefaultOrders(out num, out num2, true);
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x0001D7F0 File Offset: 0x0001BBF0
		public List<BattleDefine.eJoinMember> CalcTogetherAttackDefaultOrders(out int out_togetherTopIndex, out int out_togetherContiguousNum, bool includeTop = true)
		{
			out_togetherTopIndex = -1;
			out_togetherContiguousNum = 0;
			int num = (int)this.GetGauge().GetValue();
			if (num <= 0)
			{
				return null;
			}
			List<BattleDefine.eJoinMember> list = new List<BattleDefine.eJoinMember>();
			int num2 = -1;
			int num3 = 0;
			CharacterHandler y = null;
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				if (list.Count >= num)
				{
					break;
				}
				BattleOrder.FrameData frameDataAt = this.GetFrameDataAt(i);
				if (i != 0 || includeTop)
				{
					if (!frameDataAt.IsCardType || !(frameDataAt.m_Owner != null) || !frameDataAt.m_Owner.CharaBattle.MyParty.IsPlayerParty)
					{
						CharacterHandler owner = frameDataAt.m_Owner;
						if (!(owner == null))
						{
							if (!owner.CharaBattle.CanBeTogetherAttack())
							{
								break;
							}
							if (frameDataAt.IsCardType && owner.CharaBattle.MyParty.IsEnemyParty)
							{
								break;
							}
							if (num2 == -1)
							{
								num2 = i;
								y = owner;
							}
							else if (owner == y)
							{
								goto IL_147;
							}
							num3++;
							if (!list.Contains(owner.CharaBattle.Join))
							{
								list.Add(owner.CharaBattle.Join);
							}
						}
					}
				}
				IL_147:;
			}
			out_togetherTopIndex = num2;
			out_togetherContiguousNum = num3;
			return list;
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x0001D963 File Offset: 0x0001BD63
		public bool GetTogetherAttackExecutable()
		{
			return this.m_IsExecutableTogetherAttack;
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0001D96B File Offset: 0x0001BD6B
		public void SetTogetherAttackExecutable(bool isExecutableTogetherAttack)
		{
			this.m_IsExecutableTogetherAttack = isExecutableTogetherAttack;
		}

		// Token: 0x04000482 RID: 1154
		private BattleOrder.TogetherData m_TogetherData;

		// Token: 0x04000483 RID: 1155
		private BattleTogetherGauge m_Gauge;

		// Token: 0x04000484 RID: 1156
		private List<BattleOrder.FrameData> m_Frames;

		// Token: 0x04000485 RID: 1157
		private List<BattleOrder.FrameData> m_SaveFrames;

		// Token: 0x04000486 RID: 1158
		private bool m_IsExecutableTogetherAttack;

		// Token: 0x04000487 RID: 1159
		[CompilerGenerated]
		private static Comparison<BattleOrder.FrameData> <>f__mg$cache0;

		// Token: 0x020000D6 RID: 214
		public class TogetherData
		{
			// Token: 0x060005C1 RID: 1473 RVA: 0x0001D974 File Offset: 0x0001BD74
			public TogetherData()
			{
				this.m_IsAvailable = false;
				this.m_JoinOrder = null;
				this.m_Count = 0;
			}

			// Token: 0x060005C2 RID: 1474 RVA: 0x0001D991 File Offset: 0x0001BD91
			public void SetupOnTurnStart(bool executable)
			{
				this.m_IsAvailable = false;
				this.m_JoinOrder = null;
				this.m_Count = 0;
				this.SetExecutable(executable);
			}

			// Token: 0x060005C3 RID: 1475 RVA: 0x0001D9AF File Offset: 0x0001BDAF
			public bool IsAvailable()
			{
				return this.m_IsAvailable;
			}

			// Token: 0x060005C4 RID: 1476 RVA: 0x0001D9B7 File Offset: 0x0001BDB7
			public void SetAvailable(bool flg)
			{
				this.m_IsAvailable = flg;
			}

			// Token: 0x060005C5 RID: 1477 RVA: 0x0001D9C0 File Offset: 0x0001BDC0
			public bool Executable()
			{
				return this.m_Executable;
			}

			// Token: 0x060005C6 RID: 1478 RVA: 0x0001D9C8 File Offset: 0x0001BDC8
			public void SetExecutable(bool executable)
			{
				this.m_Executable = executable;
			}

			// Token: 0x060005C7 RID: 1479 RVA: 0x0001D9D1 File Offset: 0x0001BDD1
			public List<BattleDefine.eJoinMember> GetJoinOrder()
			{
				return this.m_JoinOrder;
			}

			// Token: 0x060005C8 RID: 1480 RVA: 0x0001D9D9 File Offset: 0x0001BDD9
			public void SetJoinOrder(List<BattleDefine.eJoinMember> joinOrder)
			{
				this.m_JoinOrder = joinOrder;
			}

			// Token: 0x060005C9 RID: 1481 RVA: 0x0001D9E2 File Offset: 0x0001BDE2
			public int GetCount()
			{
				return this.m_Count;
			}

			// Token: 0x060005CA RID: 1482 RVA: 0x0001D9EC File Offset: 0x0001BDEC
			public int IncreaseCount()
			{
				return ++this.m_Count;
			}

			// Token: 0x060005CB RID: 1483 RVA: 0x0001DA0A File Offset: 0x0001BE0A
			public bool IsEnd()
			{
				return this.m_Count >= this.m_JoinOrder.Count;
			}

			// Token: 0x04000488 RID: 1160
			private bool m_IsAvailable;

			// Token: 0x04000489 RID: 1161
			private bool m_Executable;

			// Token: 0x0400048A RID: 1162
			private List<BattleDefine.eJoinMember> m_JoinOrder;

			// Token: 0x0400048B RID: 1163
			private int m_Count;
		}

		// Token: 0x020000D7 RID: 215
		public enum eFrameType
		{
			// Token: 0x0400048D RID: 1165
			Chara,
			// Token: 0x0400048E RID: 1166
			Card
		}

		// Token: 0x020000D8 RID: 216
		public class CardArguments
		{
			// Token: 0x060005CC RID: 1484 RVA: 0x0001DA25 File Offset: 0x0001BE25
			public CardArguments(int aliveNum, int refID, int commandIndexWasCreated, BattleCommandData command)
			{
				this.m_AliveNum = aliveNum;
				this.m_RefID = refID;
				this.m_CommandIndexWasCreated = commandIndexWasCreated;
				this.m_Command = command;
			}

			// Token: 0x17000060 RID: 96
			// (get) Token: 0x060005CD RID: 1485 RVA: 0x0001DA4C File Offset: 0x0001BE4C
			public eSkillContentType SkillContentType
			{
				get
				{
					if (this.m_Command != null && this.m_Command.SkillContentSets != null && this.m_Command.SkillContentSets.Count > 0)
					{
						return (eSkillContentType)this.m_Command.SkillContentSets[0].m_Data.m_Type;
					}
					return eSkillContentType.Num;
				}
			}

			// Token: 0x0400048F RID: 1167
			public int m_AliveNum;

			// Token: 0x04000490 RID: 1168
			public int m_RefID;

			// Token: 0x04000491 RID: 1169
			public int m_CommandIndexWasCreated;

			// Token: 0x04000492 RID: 1170
			public BattleCommandData m_Command;
		}

		// Token: 0x020000D9 RID: 217
		public class FrameData
		{
			// Token: 0x060005CE RID: 1486 RVA: 0x0001DAA8 File Offset: 0x0001BEA8
			public FrameData(BattleOrder.eFrameType frameType, CharacterHandler owner, float orderValue, BattleOrder.CardArguments cardArgs)
			{
				this.m_FrameType = frameType;
				this.m_Owner = owner;
				this.m_OrderValue = orderValue;
				this.m_CardArgs = cardArgs;
			}

			// Token: 0x17000061 RID: 97
			// (get) Token: 0x060005CF RID: 1487 RVA: 0x0001DACD File Offset: 0x0001BECD
			public bool IsCharaType
			{
				get
				{
					return this.m_FrameType == BattleOrder.eFrameType.Chara;
				}
			}

			// Token: 0x17000062 RID: 98
			// (get) Token: 0x060005D0 RID: 1488 RVA: 0x0001DAD8 File Offset: 0x0001BED8
			public bool IsCardType
			{
				get
				{
					return this.m_FrameType != BattleOrder.eFrameType.Chara;
				}
			}

			// Token: 0x060005D1 RID: 1489 RVA: 0x0001DAE6 File Offset: 0x0001BEE6
			public bool CompareOwnerCharaOnly(CharacterHandler charaHndl)
			{
				return this.IsCharaType && this.m_Owner == charaHndl;
			}

			// Token: 0x04000493 RID: 1171
			public BattleOrder.eFrameType m_FrameType;

			// Token: 0x04000494 RID: 1172
			public CharacterHandler m_Owner;

			// Token: 0x04000495 RID: 1173
			public float m_OrderValue;

			// Token: 0x04000496 RID: 1174
			public BattleOrder.CardArguments m_CardArgs;
		}
	}
}
