﻿using System;

namespace Star
{
	// Token: 0x0200024B RID: 587
	[Serializable]
	public struct TownObjectLevelUpDB_Param
	{
		// Token: 0x04001342 RID: 4930
		public int m_ID;

		// Token: 0x04001343 RID: 4931
		public int m_TargetLv;

		// Token: 0x04001344 RID: 4932
		public int m_GoldAmount;

		// Token: 0x04001345 RID: 4933
		public int m_KiraraPoint;

		// Token: 0x04001346 RID: 4934
		public int m_UserLv;

		// Token: 0x04001347 RID: 4935
		public int m_BuildTime;

		// Token: 0x04001348 RID: 4936
		public int m_SaleGoldAmount;

		// Token: 0x04001349 RID: 4937
		public int m_SaleKiraraAmount;

		// Token: 0x0400134A RID: 4938
		public int m_WakeTime;

		// Token: 0x0400134B RID: 4939
		public int m_LimitNum;

		// Token: 0x0400134C RID: 4940
		public float[] m_Parsent;

		// Token: 0x0400134D RID: 4941
		public uint[] m_ResultNo;

		// Token: 0x0400134E RID: 4942
		public int m_ExtensionFunc;

		// Token: 0x0400134F RID: 4943
		public int m_ExtensionKey;

		// Token: 0x04001350 RID: 4944
		public int m_TownActionStepKey;

		// Token: 0x04001351 RID: 4945
		public int m_TownUseFunc;

		// Token: 0x04001352 RID: 4946
		public int m_BuildOptionFunc;

		// Token: 0x04001353 RID: 4947
		public int m_BuildOptionKey;
	}
}
