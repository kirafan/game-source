﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200017E RID: 382
	public sealed class DatabaseManager
	{
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x06000A28 RID: 2600 RVA: 0x0003F44E File Offset: 0x0003D84E
		// (set) Token: 0x06000A29 RID: 2601 RVA: 0x0003F456 File Offset: 0x0003D856
		public AssetBundleDownloadListDB ABDLDB_First { get; private set; }

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x06000A2A RID: 2602 RVA: 0x0003F45F File Offset: 0x0003D85F
		// (set) Token: 0x06000A2B RID: 2603 RVA: 0x0003F467 File Offset: 0x0003D867
		public AssetBundleDownloadListDB ABDLDB_Always { get; private set; }

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x06000A2C RID: 2604 RVA: 0x0003F470 File Offset: 0x0003D870
		// (set) Token: 0x06000A2D RID: 2605 RVA: 0x0003F478 File Offset: 0x0003D878
		public CRIFileVersionDB CRIFileVersionDB { get; private set; }

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x06000A2E RID: 2606 RVA: 0x0003F481 File Offset: 0x0003D881
		// (set) Token: 0x06000A2F RID: 2607 RVA: 0x0003F489 File Offset: 0x0003D889
		public StarDefineDB StarDefineDB { get; private set; }

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x06000A30 RID: 2608 RVA: 0x0003F492 File Offset: 0x0003D892
		// (set) Token: 0x06000A31 RID: 2609 RVA: 0x0003F49A File Offset: 0x0003D89A
		public Text_CommonDB TextCmnDB { get; private set; }

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x0003F4A3 File Offset: 0x0003D8A3
		// (set) Token: 0x06000A33 RID: 2611 RVA: 0x0003F4AB File Offset: 0x0003D8AB
		public Text_MessageDB TextMessageDB { get; private set; }

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x06000A34 RID: 2612 RVA: 0x0003F4B4 File Offset: 0x0003D8B4
		// (set) Token: 0x06000A35 RID: 2613 RVA: 0x0003F4BC File Offset: 0x0003D8BC
		public TipsDB TipsLoadingDB { get; private set; }

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x06000A36 RID: 2614 RVA: 0x0003F4C5 File Offset: 0x0003D8C5
		// (set) Token: 0x06000A37 RID: 2615 RVA: 0x0003F4CD File Offset: 0x0003D8CD
		public MoviePlayListDB MoviePlayListDB { get; private set; }

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x06000A38 RID: 2616 RVA: 0x0003F4D6 File Offset: 0x0003D8D6
		// (set) Token: 0x06000A39 RID: 2617 RVA: 0x0003F4DE File Offset: 0x0003D8DE
		public SoundCueSheetDB SoundCueSheetDB { get; private set; }

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x06000A3A RID: 2618 RVA: 0x0003F4E7 File Offset: 0x0003D8E7
		// (set) Token: 0x06000A3B RID: 2619 RVA: 0x0003F4EF File Offset: 0x0003D8EF
		public SoundSeListDB SoundSeListDB { get; private set; }

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000A3C RID: 2620 RVA: 0x0003F4F8 File Offset: 0x0003D8F8
		// (set) Token: 0x06000A3D RID: 2621 RVA: 0x0003F500 File Offset: 0x0003D900
		public SoundBgmListDB SoundBgmListDB { get; private set; }

		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000A3E RID: 2622 RVA: 0x0003F509 File Offset: 0x0003D909
		// (set) Token: 0x06000A3F RID: 2623 RVA: 0x0003F511 File Offset: 0x0003D911
		public SoundVoiceListDB SoundVoiceListDB { get; private set; }

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000A40 RID: 2624 RVA: 0x0003F51A File Offset: 0x0003D91A
		// (set) Token: 0x06000A41 RID: 2625 RVA: 0x0003F522 File Offset: 0x0003D922
		public SoundVoiceControllListDB SoundVoiceControllListDB { get; private set; }

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000A42 RID: 2626 RVA: 0x0003F52B File Offset: 0x0003D92B
		// (set) Token: 0x06000A43 RID: 2627 RVA: 0x0003F533 File Offset: 0x0003D933
		public SoundHomeBgmListDB SoundHomeBgmListDB { get; private set; }

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x06000A44 RID: 2628 RVA: 0x0003F53C File Offset: 0x0003D93C
		// (set) Token: 0x06000A45 RID: 2629 RVA: 0x0003F544 File Offset: 0x0003D944
		public CharacterListDB CharaListDB { get; private set; }

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x06000A46 RID: 2630 RVA: 0x0003F54D File Offset: 0x0003D94D
		// (set) Token: 0x06000A47 RID: 2631 RVA: 0x0003F555 File Offset: 0x0003D955
		public ClassListDB ClassListDB { get; private set; }

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x06000A48 RID: 2632 RVA: 0x0003F55E File Offset: 0x0003D95E
		// (set) Token: 0x06000A49 RID: 2633 RVA: 0x0003F566 File Offset: 0x0003D966
		public NamedListDB NamedListDB { get; private set; }

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x06000A4A RID: 2634 RVA: 0x0003F56F File Offset: 0x0003D96F
		// (set) Token: 0x06000A4B RID: 2635 RVA: 0x0003F577 File Offset: 0x0003D977
		public TitleListDB TitleListDB { get; private set; }

		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000A4C RID: 2636 RVA: 0x0003F580 File Offset: 0x0003D980
		// (set) Token: 0x06000A4D RID: 2637 RVA: 0x0003F588 File Offset: 0x0003D988
		public WeaponListDB WeaponListDB { get; private set; }

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000A4E RID: 2638 RVA: 0x0003F591 File Offset: 0x0003D991
		// (set) Token: 0x06000A4F RID: 2639 RVA: 0x0003F599 File Offset: 0x0003D999
		public WeaponRecipeListDB WeaponRecipeListDB { get; private set; }

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x06000A50 RID: 2640 RVA: 0x0003F5A2 File Offset: 0x0003D9A2
		// (set) Token: 0x06000A51 RID: 2641 RVA: 0x0003F5AA File Offset: 0x0003D9AA
		public CharacterLimitBreakListDB CharaLimitBreakListDB { get; private set; }

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x06000A52 RID: 2642 RVA: 0x0003F5B3 File Offset: 0x0003D9B3
		// (set) Token: 0x06000A53 RID: 2643 RVA: 0x0003F5BB File Offset: 0x0003D9BB
		public CharacterEvolutionListDB CharaEvolutionListDB { get; private set; }

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000A54 RID: 2644 RVA: 0x0003F5C4 File Offset: 0x0003D9C4
		// (set) Token: 0x06000A55 RID: 2645 RVA: 0x0003F5CC File Offset: 0x0003D9CC
		public LeaderSkillListDB LeaderSkillListDB { get; private set; }

		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000A56 RID: 2646 RVA: 0x0003F5D5 File Offset: 0x0003D9D5
		// (set) Token: 0x06000A57 RID: 2647 RVA: 0x0003F5DD File Offset: 0x0003D9DD
		public SkillLvCoefDB SkillLvCoefDB { get; private set; }

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000A58 RID: 2648 RVA: 0x0003F5E6 File Offset: 0x0003D9E6
		// (set) Token: 0x06000A59 RID: 2649 RVA: 0x0003F5EE File Offset: 0x0003D9EE
		public SkillContentAndEffectCombiDB SkillContentAndEffectCombiDB { get; private set; }

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x06000A5A RID: 2650 RVA: 0x0003F5F7 File Offset: 0x0003D9F7
		// (set) Token: 0x06000A5B RID: 2651 RVA: 0x0003F5FF File Offset: 0x0003D9FF
		public UniqueSkillSoundDB UniqueSkillSoundDB { get; private set; }

		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000A5C RID: 2652 RVA: 0x0003F608 File Offset: 0x0003DA08
		// (set) Token: 0x06000A5D RID: 2653 RVA: 0x0003F610 File Offset: 0x0003DA10
		public BattleDefineDB BattleDefineDB { get; private set; }

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x06000A5E RID: 2654 RVA: 0x0003F619 File Offset: 0x0003DA19
		// (set) Token: 0x06000A5F RID: 2655 RVA: 0x0003F621 File Offset: 0x0003DA21
		public GachaCutInListDB GachaCutInListDB { get; private set; }

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x06000A60 RID: 2656 RVA: 0x0003F62A File Offset: 0x0003DA2A
		// (set) Token: 0x06000A61 RID: 2657 RVA: 0x0003F632 File Offset: 0x0003DA32
		public GachaItemLabelListDB GachaItemLabelListDB { get; private set; }

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x06000A62 RID: 2658 RVA: 0x0003F63B File Offset: 0x0003DA3B
		// (set) Token: 0x06000A63 RID: 2659 RVA: 0x0003F643 File Offset: 0x0003DA43
		public CharacterFacialDB CharaFacialDB { get; private set; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x06000A64 RID: 2660 RVA: 0x0003F64C File Offset: 0x0003DA4C
		// (set) Token: 0x06000A65 RID: 2661 RVA: 0x0003F654 File Offset: 0x0003DA54
		public CharacterFlavorTextDB CharaFlavorTextDB { get; private set; }

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x06000A66 RID: 2662 RVA: 0x0003F65D File Offset: 0x0003DA5D
		// (set) Token: 0x06000A67 RID: 2663 RVA: 0x0003F665 File Offset: 0x0003DA65
		public ItemListDB ItemListDB { get; private set; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x06000A68 RID: 2664 RVA: 0x0003F66E File Offset: 0x0003DA6E
		// (set) Token: 0x06000A69 RID: 2665 RVA: 0x0003F676 File Offset: 0x0003DA76
		public MasterOrbListDB MasterOrbListDB { get; private set; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x06000A6A RID: 2666 RVA: 0x0003F67F File Offset: 0x0003DA7F
		// (set) Token: 0x06000A6B RID: 2667 RVA: 0x0003F687 File Offset: 0x0003DA87
		public QuestWaveListDB QuestWaveListDB { get; private set; }

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000A6C RID: 2668 RVA: 0x0003F690 File Offset: 0x0003DA90
		// (set) Token: 0x06000A6D RID: 2669 RVA: 0x0003F698 File Offset: 0x0003DA98
		public QuestWaveRandomListDB QuestWaveRandomListDB { get; private set; }

		// Token: 0x17000111 RID: 273
		// (get) Token: 0x06000A6E RID: 2670 RVA: 0x0003F6A1 File Offset: 0x0003DAA1
		// (set) Token: 0x06000A6F RID: 2671 RVA: 0x0003F6A9 File Offset: 0x0003DAA9
		public QuestWaveDropItemDB QuestWaveDropItemDB { get; private set; }

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x06000A70 RID: 2672 RVA: 0x0003F6B2 File Offset: 0x0003DAB2
		// (set) Token: 0x06000A71 RID: 2673 RVA: 0x0003F6BA File Offset: 0x0003DABA
		public QuestEnemyListDB QuestEnemyListDB { get; private set; }

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x06000A72 RID: 2674 RVA: 0x0003F6C3 File Offset: 0x0003DAC3
		// (set) Token: 0x06000A73 RID: 2675 RVA: 0x0003F6CB File Offset: 0x0003DACB
		public QuestADVTriggerDB QuestADVTriggerDB { get; private set; }

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000A74 RID: 2676 RVA: 0x0003F6D4 File Offset: 0x0003DAD4
		// (set) Token: 0x06000A75 RID: 2677 RVA: 0x0003F6DC File Offset: 0x0003DADC
		public EnemyResourceListDB EnemyResourceListDB { get; private set; }

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000A76 RID: 2678 RVA: 0x0003F6E5 File Offset: 0x0003DAE5
		// (set) Token: 0x06000A77 RID: 2679 RVA: 0x0003F6ED File Offset: 0x0003DAED
		public TownObjectListDB TownObjListDB { get; private set; }

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000A78 RID: 2680 RVA: 0x0003F6F6 File Offset: 0x0003DAF6
		// (set) Token: 0x06000A79 RID: 2681 RVA: 0x0003F6FE File Offset: 0x0003DAFE
		public TownShopListDB TownShopListDB { get; private set; }

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000A7A RID: 2682 RVA: 0x0003F707 File Offset: 0x0003DB07
		// (set) Token: 0x06000A7B RID: 2683 RVA: 0x0003F70F File Offset: 0x0003DB0F
		public TownObjectLevelUpDB TownObjLvUpDB { get; private set; }

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x06000A7C RID: 2684 RVA: 0x0003F718 File Offset: 0x0003DB18
		// (set) Token: 0x06000A7D RID: 2685 RVA: 0x0003F720 File Offset: 0x0003DB20
		public TownObjectBuildSubCodeDB TownObjBuildSubDB { get; private set; }

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x06000A7E RID: 2686 RVA: 0x0003F729 File Offset: 0x0003DB29
		// (set) Token: 0x06000A7F RID: 2687 RVA: 0x0003F731 File Offset: 0x0003DB31
		public TownObjectBuffDB TownObjBuffDB { get; private set; }

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000A80 RID: 2688 RVA: 0x0003F73A File Offset: 0x0003DB3A
		// (set) Token: 0x06000A81 RID: 2689 RVA: 0x0003F742 File Offset: 0x0003DB42
		public RoomListDB RoomListDB { get; private set; }

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x06000A82 RID: 2690 RVA: 0x0003F74B File Offset: 0x0003DB4B
		// (set) Token: 0x06000A83 RID: 2691 RVA: 0x0003F753 File Offset: 0x0003DB53
		public RoomObjectListDB RoomObjectListDB { get; private set; }

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x06000A84 RID: 2692 RVA: 0x0003F75C File Offset: 0x0003DB5C
		// (set) Token: 0x06000A85 RID: 2693 RVA: 0x0003F764 File Offset: 0x0003DB64
		public RoomShopListDB RoomShopListDB { get; private set; }

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x06000A86 RID: 2694 RVA: 0x0003F76D File Offset: 0x0003DB6D
		// (set) Token: 0x06000A87 RID: 2695 RVA: 0x0003F775 File Offset: 0x0003DB75
		public EffectListDB EffectListDB { get; private set; }

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000A88 RID: 2696 RVA: 0x0003F77E File Offset: 0x0003DB7E
		// (set) Token: 0x06000A89 RID: 2697 RVA: 0x0003F786 File Offset: 0x0003DB86
		public MasterRankDB MasterRankDB { get; private set; }

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000A8A RID: 2698 RVA: 0x0003F78F File Offset: 0x0003DB8F
		// (set) Token: 0x06000A8B RID: 2699 RVA: 0x0003F797 File Offset: 0x0003DB97
		public MasterOrbExpDB MasterOrbExpDB { get; private set; }

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000A8C RID: 2700 RVA: 0x0003F7A0 File Offset: 0x0003DBA0
		// (set) Token: 0x06000A8D RID: 2701 RVA: 0x0003F7A8 File Offset: 0x0003DBA8
		public CharacterExpDB CharaExpDB { get; private set; }

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000A8E RID: 2702 RVA: 0x0003F7B1 File Offset: 0x0003DBB1
		// (set) Token: 0x06000A8F RID: 2703 RVA: 0x0003F7B9 File Offset: 0x0003DBB9
		public NamedFriendshipExpDB NamedFriendshipExpDB { get; private set; }

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000A90 RID: 2704 RVA: 0x0003F7C2 File Offset: 0x0003DBC2
		// (set) Token: 0x06000A91 RID: 2705 RVA: 0x0003F7CA File Offset: 0x0003DBCA
		public WeaponExpDB WeaponExpDB { get; private set; }

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000A92 RID: 2706 RVA: 0x0003F7D3 File Offset: 0x0003DBD3
		// (set) Token: 0x06000A93 RID: 2707 RVA: 0x0003F7DB File Offset: 0x0003DBDB
		public SkillExpDB SkillExpDB { get; private set; }

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x06000A94 RID: 2708 RVA: 0x0003F7E4 File Offset: 0x0003DBE4
		// (set) Token: 0x06000A95 RID: 2709 RVA: 0x0003F7EC File Offset: 0x0003DBEC
		public CharacterParamGrowthListDB CharaParamGrowthListDB { get; private set; }

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x06000A96 RID: 2710 RVA: 0x0003F7F5 File Offset: 0x0003DBF5
		// (set) Token: 0x06000A97 RID: 2711 RVA: 0x0003F7FD File Offset: 0x0003DBFD
		public ProfileVoiceListDB ProfileVoiceListDB { get; private set; }

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000A98 RID: 2712 RVA: 0x0003F806 File Offset: 0x0003DC06
		// (set) Token: 0x06000A99 RID: 2713 RVA: 0x0003F80E File Offset: 0x0003DC0E
		public FieldItemDropListDB FieldItemDropListDB { get; private set; }

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000A9A RID: 2714 RVA: 0x0003F817 File Offset: 0x0003DC17
		// (set) Token: 0x06000A9B RID: 2715 RVA: 0x0003F81F File Offset: 0x0003DC1F
		public PopUpStateIconListDB PopUpStateIconListDB { get; private set; }

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000A9C RID: 2716 RVA: 0x0003F828 File Offset: 0x0003DC28
		// (set) Token: 0x06000A9D RID: 2717 RVA: 0x0003F830 File Offset: 0x0003DC30
		public CharacterIllustOffsetDB CharaIllustOffsetDB { get; private set; }

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000A9E RID: 2718 RVA: 0x0003F839 File Offset: 0x0003DC39
		// (set) Token: 0x06000A9F RID: 2719 RVA: 0x0003F841 File Offset: 0x0003DC41
		public ADVListDB ADVListDB { get; private set; }

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000AA0 RID: 2720 RVA: 0x0003F84A File Offset: 0x0003DC4A
		// (set) Token: 0x06000AA1 RID: 2721 RVA: 0x0003F852 File Offset: 0x0003DC52
		public ADVLibraryListDB ADVLibraryListDB { get; private set; }

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x0003F85B File Offset: 0x0003DC5B
		// (set) Token: 0x06000AA3 RID: 2723 RVA: 0x0003F863 File Offset: 0x0003DC63
		public WordLibraryListDB WordLibraryListDB { get; private set; }

		// Token: 0x1700012C RID: 300
		// (get) Token: 0x06000AA4 RID: 2724 RVA: 0x0003F86C File Offset: 0x0003DC6C
		// (set) Token: 0x06000AA5 RID: 2725 RVA: 0x0003F874 File Offset: 0x0003DC74
		public OriginalCharaLibraryListDB OriginalCharaLibraryListDB { get; private set; }

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x06000AA6 RID: 2726 RVA: 0x0003F87D File Offset: 0x0003DC7D
		// (set) Token: 0x06000AA7 RID: 2727 RVA: 0x0003F885 File Offset: 0x0003DC85
		public SceneInfoListDB SceneInfoListDB { get; private set; }

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000AA8 RID: 2728 RVA: 0x0003F88E File Offset: 0x0003DC8E
		// (set) Token: 0x06000AA9 RID: 2729 RVA: 0x0003F896 File Offset: 0x0003DC96
		public TutorialTipsListDB TutorialTipsListDB { get; private set; }

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000AAA RID: 2730 RVA: 0x0003F89F File Offset: 0x0003DC9F
		// (set) Token: 0x06000AAB RID: 2731 RVA: 0x0003F8A7 File Offset: 0x0003DCA7
		public RetireTipsListDB RetireTipsListDB { get; private set; }

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000AAC RID: 2732 RVA: 0x0003F8B0 File Offset: 0x0003DCB0
		// (set) Token: 0x06000AAD RID: 2733 RVA: 0x0003F8B8 File Offset: 0x0003DCB8
		public TutorialMessageListDB TutorialMessageListDB { get; private set; }

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000AAE RID: 2734 RVA: 0x0003F8C1 File Offset: 0x0003DCC1
		// (set) Token: 0x06000AAF RID: 2735 RVA: 0x0003F8C9 File Offset: 0x0003DCC9
		public WebDataListDB WebDataListDB { get; private set; }

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000AB0 RID: 2736 RVA: 0x0003F8D2 File Offset: 0x0003DCD2
		// (set) Token: 0x06000AB1 RID: 2737 RVA: 0x0003F8DA File Offset: 0x0003DCDA
		public Dictionary<int, BattleAIData> BattleAIDatas { get; private set; }

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000AB2 RID: 2738 RVA: 0x0003F8E3 File Offset: 0x0003DCE3
		// (set) Token: 0x06000AB3 RID: 2739 RVA: 0x0003F8EB File Offset: 0x0003DCEB
		public Dictionary<string, SkillActionGraphics> SAGs { get; private set; }

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x06000AB4 RID: 2740 RVA: 0x0003F8F4 File Offset: 0x0003DCF4
		// (set) Token: 0x06000AB5 RID: 2741 RVA: 0x0003F8FC File Offset: 0x0003DCFC
		public Dictionary<string, SkillActionPlan> SAPs { get; private set; }

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x06000AB6 RID: 2742 RVA: 0x0003F905 File Offset: 0x0003DD05
		public SkillListDB SkillListDB_PL
		{
			get
			{
				return this.SkillListDB[0];
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x06000AB7 RID: 2743 RVA: 0x0003F90F File Offset: 0x0003DD0F
		public SkillListDB SkillListDB_EN
		{
			get
			{
				return this.SkillListDB[1];
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000AB8 RID: 2744 RVA: 0x0003F919 File Offset: 0x0003DD19
		public SkillListDB SkillListDB_WPN
		{
			get
			{
				return this.SkillListDB[2];
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000AB9 RID: 2745 RVA: 0x0003F923 File Offset: 0x0003DD23
		public SkillListDB SkillListDB_MST
		{
			get
			{
				return this.SkillListDB[3];
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000ABA RID: 2746 RVA: 0x0003F92D File Offset: 0x0003DD2D
		public SkillListDB SkillListDB_CARD
		{
			get
			{
				return this.SkillListDB[4];
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000ABB RID: 2747 RVA: 0x0003F937 File Offset: 0x0003DD37
		public SkillContentListDB SkillContentListDB_PL
		{
			get
			{
				return this.SkillContentListDB[0];
			}
		}

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000ABC RID: 2748 RVA: 0x0003F941 File Offset: 0x0003DD41
		public SkillContentListDB SkillContentListDB_EN
		{
			get
			{
				return this.SkillContentListDB[1];
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000ABD RID: 2749 RVA: 0x0003F94B File Offset: 0x0003DD4B
		public SkillContentListDB SkillContentListDB_WPN
		{
			get
			{
				return this.SkillContentListDB[2];
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000ABE RID: 2750 RVA: 0x0003F955 File Offset: 0x0003DD55
		public SkillContentListDB SkillContentListDB_MST
		{
			get
			{
				return this.SkillContentListDB[3];
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000ABF RID: 2751 RVA: 0x0003F95F File Offset: 0x0003DD5F
		public SkillContentListDB SkillContentListDB_CARD
		{
			get
			{
				return this.SkillContentListDB[4];
			}
		}

		// Token: 0x06000AC0 RID: 2752 RVA: 0x0003F96C File Offset: 0x0003DD6C
		public void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			DatabaseManager.ePrepareStep prepareStep = this.m_PrepareStep;
			if (prepareStep == DatabaseManager.ePrepareStep.Preparing)
			{
				this.Preparing();
			}
		}

		// Token: 0x06000AC1 RID: 2753 RVA: 0x0003F9AC File Offset: 0x0003DDAC
		public bool Prepare()
		{
			if (this.IsPreparing())
			{
				return false;
			}
			if (this.m_Loader == null)
			{
				this.m_Loader = new ABResourceLoader(-1);
			}
			this.m_LoadingHndl = this.m_Loader.Load("database/database.muast", new MeigeResource.Option[0]);
			if (this.m_LoadingHndl != null)
			{
				this.m_PrepareStep = DatabaseManager.ePrepareStep.Preparing;
				return true;
			}
			return false;
		}

		// Token: 0x06000AC2 RID: 2754 RVA: 0x0003FA0E File Offset: 0x0003DE0E
		public bool IsPreparing()
		{
			return this.m_PrepareStep == DatabaseManager.ePrepareStep.Preparing;
		}

		// Token: 0x06000AC3 RID: 2755 RVA: 0x0003FA1C File Offset: 0x0003DE1C
		private void Preparing()
		{
			if (this.m_LoadingHndl != null && this.m_LoadingHndl.IsDone())
			{
				if (this.m_LoadingHndl.IsError())
				{
					this.m_LoadingHndl.Retry();
				}
				else
				{
					for (int i = 0; i < 78; i++)
					{
						UnityEngine.Object @object = this.m_LoadingHndl.FindObj(DatabaseManager.DB_OBJ_PATHS[i], true);
						if (@object != null)
						{
							switch (i)
							{
							case 0:
								this.ABDLDB_First = (@object as AssetBundleDownloadListDB);
								break;
							case 1:
								this.ABDLDB_Always = (@object as AssetBundleDownloadListDB);
								break;
							case 2:
								this.CRIFileVersionDB = (@object as CRIFileVersionDB);
								break;
							case 3:
								this.StarDefineDB = (@object as StarDefineDB);
								break;
							case 4:
								this.TextCmnDB = (@object as Text_CommonDB);
								this.ParseTextCommonDB();
								break;
							case 5:
								this.TextMessageDB = (@object as Text_MessageDB);
								this.ParseTextMessageDB();
								break;
							case 6:
								this.TipsLoadingDB = (@object as TipsDB);
								break;
							case 7:
								this.MoviePlayListDB = (@object as MoviePlayListDB);
								break;
							case 8:
								this.SoundCueSheetDB = (@object as SoundCueSheetDB);
								break;
							case 9:
								this.SoundSeListDB = (@object as SoundSeListDB);
								break;
							case 10:
								this.SoundBgmListDB = (@object as SoundBgmListDB);
								break;
							case 11:
								this.SoundVoiceListDB = (@object as SoundVoiceListDB);
								break;
							case 12:
								this.SoundVoiceControllListDB = (@object as SoundVoiceControllListDB);
								break;
							case 13:
								this.SoundHomeBgmListDB = (@object as SoundHomeBgmListDB);
								break;
							case 14:
								this.CharaListDB = (@object as CharacterListDB);
								break;
							case 15:
								this.ClassListDB = (@object as ClassListDB);
								break;
							case 16:
								this.NamedListDB = (@object as NamedListDB);
								break;
							case 17:
								this.TitleListDB = (@object as TitleListDB);
								break;
							case 18:
								this.WeaponListDB = (@object as WeaponListDB);
								break;
							case 19:
								this.WeaponRecipeListDB = (@object as WeaponRecipeListDB);
								break;
							case 20:
								this.CharaLimitBreakListDB = (@object as CharacterLimitBreakListDB);
								break;
							case 21:
								this.CharaEvolutionListDB = (@object as CharacterEvolutionListDB);
								break;
							case 22:
								this.LeaderSkillListDB = (@object as LeaderSkillListDB);
								break;
							case 23:
								this.SkillListDB[0] = (@object as SkillListDB);
								break;
							case 24:
								this.SkillListDB[1] = (@object as SkillListDB);
								break;
							case 25:
								this.SkillListDB[2] = (@object as SkillListDB);
								break;
							case 26:
								this.SkillListDB[3] = (@object as SkillListDB);
								break;
							case 27:
								this.SkillListDB[4] = (@object as SkillListDB);
								break;
							case 28:
								this.SkillContentListDB[0] = (@object as SkillContentListDB);
								break;
							case 29:
								this.SkillContentListDB[1] = (@object as SkillContentListDB);
								break;
							case 30:
								this.SkillContentListDB[2] = (@object as SkillContentListDB);
								break;
							case 31:
								this.SkillContentListDB[3] = (@object as SkillContentListDB);
								break;
							case 32:
								this.SkillContentListDB[4] = (@object as SkillContentListDB);
								break;
							case 33:
								this.SkillLvCoefDB = (@object as SkillLvCoefDB);
								break;
							case 34:
								this.BattleDefineDB = (@object as BattleDefineDB);
								break;
							case 35:
								this.SkillContentAndEffectCombiDB = (@object as SkillContentAndEffectCombiDB);
								break;
							case 36:
								this.UniqueSkillSoundDB = (@object as UniqueSkillSoundDB);
								break;
							case 37:
								this.GachaCutInListDB = (@object as GachaCutInListDB);
								break;
							case 38:
								this.GachaItemLabelListDB = (@object as GachaItemLabelListDB);
								break;
							case 39:
								this.CharaFacialDB = (@object as CharacterFacialDB);
								break;
							case 40:
								this.CharaFlavorTextDB = (@object as CharacterFlavorTextDB);
								break;
							case 41:
								this.ItemListDB = (@object as ItemListDB);
								break;
							case 42:
								this.MasterOrbListDB = (@object as MasterOrbListDB);
								break;
							case 43:
								this.QuestWaveListDB = (@object as QuestWaveListDB);
								break;
							case 44:
								this.QuestWaveRandomListDB = (@object as QuestWaveRandomListDB);
								break;
							case 45:
								this.QuestWaveDropItemDB = (@object as QuestWaveDropItemDB);
								break;
							case 46:
								this.QuestEnemyListDB = (@object as QuestEnemyListDB);
								break;
							case 47:
								this.QuestADVTriggerDB = (@object as QuestADVTriggerDB);
								break;
							case 48:
								this.EnemyResourceListDB = (@object as EnemyResourceListDB);
								break;
							case 49:
								this.TownObjListDB = (@object as TownObjectListDB);
								break;
							case 50:
								this.TownShopListDB = (@object as TownShopListDB);
								break;
							case 51:
								this.TownObjLvUpDB = (@object as TownObjectLevelUpDB);
								break;
							case 52:
								this.TownObjBuffDB = (@object as TownObjectBuffDB);
								break;
							case 53:
								this.RoomListDB = (@object as RoomListDB);
								break;
							case 54:
								this.RoomObjectListDB = (@object as RoomObjectListDB);
								break;
							case 55:
								this.RoomShopListDB = (@object as RoomShopListDB);
								break;
							case 56:
								this.EffectListDB = (@object as EffectListDB);
								break;
							case 57:
								this.MasterRankDB = (@object as MasterRankDB);
								break;
							case 58:
								this.MasterOrbExpDB = (@object as MasterOrbExpDB);
								break;
							case 59:
								this.CharaExpDB = (@object as CharacterExpDB);
								break;
							case 60:
								this.NamedFriendshipExpDB = (@object as NamedFriendshipExpDB);
								break;
							case 61:
								this.WeaponExpDB = (@object as WeaponExpDB);
								break;
							case 62:
								this.SkillExpDB = (@object as SkillExpDB);
								break;
							case 63:
								this.CharaParamGrowthListDB = (@object as CharacterParamGrowthListDB);
								break;
							case 64:
								this.ProfileVoiceListDB = (@object as ProfileVoiceListDB);
								break;
							case 65:
								this.FieldItemDropListDB = (@object as FieldItemDropListDB);
								break;
							case 66:
								this.PopUpStateIconListDB = (@object as PopUpStateIconListDB);
								break;
							case 67:
								this.CharaIllustOffsetDB = (@object as CharacterIllustOffsetDB);
								break;
							case 68:
								this.ADVListDB = (@object as ADVListDB);
								break;
							case 69:
								this.ADVLibraryListDB = (@object as ADVLibraryListDB);
								break;
							case 70:
								this.WordLibraryListDB = (@object as WordLibraryListDB);
								break;
							case 71:
								this.OriginalCharaLibraryListDB = (@object as OriginalCharaLibraryListDB);
								break;
							case 72:
								this.SceneInfoListDB = (@object as SceneInfoListDB);
								break;
							case 73:
								this.TutorialTipsListDB = (@object as TutorialTipsListDB);
								break;
							case 74:
								this.RetireTipsListDB = (@object as RetireTipsListDB);
								break;
							case 75:
								this.TutorialMessageListDB = (@object as TutorialMessageListDB);
								break;
							case 76:
								this.WebDataListDB = (@object as WebDataListDB);
								break;
							case 77:
								this.TownObjBuildSubDB = (@object as TownObjectBuildSubCodeDB);
								break;
							}
						}
						else
						{
							Debug.LogErrorFormat("database is not found. path:{0}", new object[]
							{
								DatabaseManager.DB_OBJ_PATHS[i]
							});
						}
					}
					if (this.IsRefDebugDB())
					{
					}
					this.TweetListDBs = new Dictionary<eCharaNamedType, TweetListDB>();
					for (int j = 0; j < this.NamedListDB.m_Params.Length; j++)
					{
						eCharaNamedType namedType = (eCharaNamedType)this.NamedListDB.m_Params[j].m_NamedType;
						string str = string.Format("{0:D4}", (int)namedType);
						string findName = "TweetList_" + str;
						TweetListDB tweetListDB = this.m_LoadingHndl.FindObj(findName, true) as TweetListDB;
						if (tweetListDB != null)
						{
							this.TweetListDBs.AddOrReplace(namedType, tweetListDB);
						}
					}
					this.BattleAIDatas = new Dictionary<int, BattleAIData>();
					this.SAGs = new Dictionary<string, SkillActionGraphics>();
					this.SAPs = new Dictionary<string, SkillActionPlan>();
					for (int k = 0; k < this.m_LoadingHndl.Objs.Length; k++)
					{
						UnityEngine.Object object2 = this.m_LoadingHndl.Objs[k];
						if (!(object2 == null))
						{
							if (object2.name.Contains("BattleAIData_"))
							{
								BattleAIData battleAIData = object2 as BattleAIData;
								if (battleAIData != null)
								{
									this.BattleAIDatas.AddOrReplace(battleAIData.m_ID, battleAIData);
								}
							}
							else if (object2.name.Contains("SAG_"))
							{
								SkillActionGraphics skillActionGraphics = object2 as SkillActionGraphics;
								if (skillActionGraphics != null)
								{
									this.SAGs.AddOrReplace(skillActionGraphics.m_ID, skillActionGraphics);
								}
							}
							else if (object2.name.Contains("SAP_"))
							{
								SkillActionPlan skillActionPlan = object2 as SkillActionPlan;
								if (skillActionPlan != null)
								{
									this.SAPs.AddOrReplace(skillActionPlan.m_ID, skillActionPlan);
								}
							}
						}
					}
					this.m_PrepareStep = DatabaseManager.ePrepareStep.DonePrepare;
					this.m_Loader.Unload(this.m_LoadingHndl);
					this.m_LoadingHndl = null;
				}
			}
		}

		// Token: 0x06000AC4 RID: 2756 RVA: 0x00040338 File Offset: 0x0003E738
		private void ParseTextCommonDB()
		{
			this.m_TextCommon = new Dictionary<eText_CommonDB, string>();
			for (int i = 0; i < this.TextCmnDB.m_Params.Length; i++)
			{
				this.m_TextCommon.Add((eText_CommonDB)this.TextCmnDB.m_Params[i].m_ID, this.TextCmnDB.m_Params[i].m_Text);
			}
		}

		// Token: 0x06000AC5 RID: 2757 RVA: 0x000403A8 File Offset: 0x0003E7A8
		private void ParseTextMessageDB()
		{
			this.m_TextMessage = new Dictionary<eText_MessageDB, string>();
			for (int i = 0; i < this.TextMessageDB.m_Params.Length; i++)
			{
				this.m_TextMessage.Add((eText_MessageDB)this.TextMessageDB.m_Params[i].m_ID, this.TextMessageDB.m_Params[i].m_Text);
			}
		}

		// Token: 0x06000AC6 RID: 2758 RVA: 0x00040418 File Offset: 0x0003E818
		public string GetTextCommon(eText_CommonDB id)
		{
			string result;
			if (this.m_TextCommon != null && this.m_TextCommon.TryGetValue(id, out result))
			{
				return result;
			}
			return string.Empty;
		}

		// Token: 0x06000AC7 RID: 2759 RVA: 0x0004044C File Offset: 0x0003E84C
		public string GetTextCommon(eText_CommonDB id, params object[] args)
		{
			string format;
			if (this.m_TextCommon != null && this.m_TextCommon.TryGetValue(id, out format))
			{
				return string.Format(format, args);
			}
			return string.Empty;
		}

		// Token: 0x06000AC8 RID: 2760 RVA: 0x00040484 File Offset: 0x0003E884
		public string GetTextMessage(eText_MessageDB id)
		{
			string result;
			if (this.m_TextMessage != null && this.m_TextMessage.TryGetValue(id, out result))
			{
				return result;
			}
			return string.Empty;
		}

		// Token: 0x06000AC9 RID: 2761 RVA: 0x000404B8 File Offset: 0x0003E8B8
		public string GetTextMessage(eText_MessageDB id, params object[] args)
		{
			string format;
			if (this.m_TextMessage != null && this.m_TextMessage.TryGetValue(id, out format))
			{
				return string.Format(format, args);
			}
			return string.Empty;
		}

		// Token: 0x06000ACA RID: 2762 RVA: 0x000404F0 File Offset: 0x0003E8F0
		public TweetListDB GetTweetListDB(eCharaNamedType namedType)
		{
			return this.TweetListDBs[namedType];
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x000404FE File Offset: 0x0003E8FE
		public BattleAIData FindBattleAIData(int id)
		{
			if (this.BattleAIDatas != null && this.BattleAIDatas.ContainsKey(id))
			{
				return this.BattleAIDatas[id];
			}
			return null;
		}

		// Token: 0x06000ACC RID: 2764 RVA: 0x0004052A File Offset: 0x0003E92A
		public SkillActionGraphics FindSAG(string sagID)
		{
			if (this.SAGs != null && this.SAGs.ContainsKey(sagID))
			{
				return this.SAGs[sagID];
			}
			return null;
		}

		// Token: 0x06000ACD RID: 2765 RVA: 0x00040556 File Offset: 0x0003E956
		public SkillActionPlan FindSAP(string sapID)
		{
			if (this.SAPs != null && this.SAPs.ContainsKey(sapID))
			{
				return this.SAPs[sapID];
			}
			return null;
		}

		// Token: 0x06000ACE RID: 2766 RVA: 0x00040584 File Offset: 0x0003E984
		private bool IsRefDebugDB()
		{
			return false;
		}

		// Token: 0x04000A11 RID: 2577
		public const int IDX_SKILL_PL = 0;

		// Token: 0x04000A12 RID: 2578
		public const int IDX_SKILL_EN = 1;

		// Token: 0x04000A13 RID: 2579
		public const int IDX_SKILL_WPN = 2;

		// Token: 0x04000A14 RID: 2580
		public const int IDX_SKILL_MST = 3;

		// Token: 0x04000A15 RID: 2581
		public const int IDX_SKILL_CARD = 4;

		// Token: 0x04000A16 RID: 2582
		public const int IDX_SKILL_NUM = 5;

		// Token: 0x04000A2E RID: 2606
		public SkillListDB[] SkillListDB = new SkillListDB[5];

		// Token: 0x04000A2F RID: 2607
		public SkillContentListDB[] SkillContentListDB = new SkillContentListDB[5];

		// Token: 0x04000A5D RID: 2653
		private Dictionary<eCharaNamedType, TweetListDB> TweetListDBs;

		// Token: 0x04000A61 RID: 2657
		private Dictionary<eText_CommonDB, string> m_TextCommon;

		// Token: 0x04000A62 RID: 2658
		private Dictionary<eText_MessageDB, string> m_TextMessage;

		// Token: 0x04000A63 RID: 2659
		private static string[] DB_OBJ_PATHS = new string[]
		{
			"AssetBundleDownloadList_First",
			"AssetBundleDownloadList_Always",
			"CRIFileVersion",
			"StarDefine",
			"Text_Common",
			"Text_Message",
			"Tips_Loading",
			"MoviePlayList",
			"SoundCueSheet",
			"SoundSeList",
			"SoundBgmList",
			"SoundVoiceList",
			"SoundVoiceControllList",
			"SoundHomeBgmList",
			"CharacterList",
			"ClassList",
			"NamedList",
			"TitleList",
			"WeaponList",
			"WeaponRecipeList",
			"CharacterLimitBreakList",
			"CharacterEvolutionList",
			"LeaderSkillList",
			"SkillList_PL",
			"SkillList_EN",
			"SkillList_WPN",
			"SkillList_MST",
			"SkillList_CARD",
			"SkillContentList_PL",
			"SkillContentList_EN",
			"SkillContentList_WPN",
			"SkillContentList_MST",
			"SkillContentList_CARD",
			"SkillLvCoef",
			"BattleDefine",
			"SkillContentAndEffectCombi",
			"UniqueSkillSound",
			"GachaCutInList",
			"GachaItemLabelList",
			"CharacterFacial",
			"CharacterFlavorText",
			"ItemList",
			"MasterOrbList",
			"QuestWaveList",
			"QuestWaveRandomList",
			"QuestWaveDropItem",
			"QuestEnemyList",
			"QuestADVTrigger",
			"EnemyResourceList",
			"TownObjectList",
			"TownShopList",
			"TownObjectLevelUp",
			"TownObjectBuff",
			"RoomList",
			"RoomObjectList",
			"RoomShopList",
			"EffectList",
			"MasterRank",
			"MasterOrbExp",
			"CharacterExp",
			"NamedFriendshipExp",
			"WeaponExp",
			"SkillExp",
			"CharacterParamGrowthList",
			"ProfileVoiceList",
			"FieldItemDropList",
			"PopUpStateIconList",
			"CharacterIllustOffset",
			"ADVList",
			"ADVLibraryList",
			"WordLibraryList",
			"OriginalCharaLibraryList",
			"SceneInfoList",
			"TutorialTipsList",
			"RetireTipsList",
			"TutorialMessageList",
			"WebDataList",
			"TownObjectBuildSubCode"
		};

		// Token: 0x04000A64 RID: 2660
		private const string RESOURCE_PATH = "database/database.muast";

		// Token: 0x04000A65 RID: 2661
		private DatabaseManager.ePrepareStep m_PrepareStep = DatabaseManager.ePrepareStep.None;

		// Token: 0x04000A66 RID: 2662
		private ABResourceLoader m_Loader;

		// Token: 0x04000A67 RID: 2663
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x0200017F RID: 383
		private enum eDatabase
		{
			// Token: 0x04000A69 RID: 2665
			ABDLDB_First,
			// Token: 0x04000A6A RID: 2666
			ABDLDB_Always,
			// Token: 0x04000A6B RID: 2667
			CRIFileVersionDB,
			// Token: 0x04000A6C RID: 2668
			StarDefineDB,
			// Token: 0x04000A6D RID: 2669
			TextCommonDB,
			// Token: 0x04000A6E RID: 2670
			TextMessageDB,
			// Token: 0x04000A6F RID: 2671
			TipsLoadingDB,
			// Token: 0x04000A70 RID: 2672
			MoviePlayListDB,
			// Token: 0x04000A71 RID: 2673
			SoundCueSheetDB,
			// Token: 0x04000A72 RID: 2674
			SoundSeListDB,
			// Token: 0x04000A73 RID: 2675
			SoundBgmListDB,
			// Token: 0x04000A74 RID: 2676
			SoundVoiceListDB,
			// Token: 0x04000A75 RID: 2677
			SoundVoiceControllListDB,
			// Token: 0x04000A76 RID: 2678
			SoundHomeBgmListDB,
			// Token: 0x04000A77 RID: 2679
			CharaListDB,
			// Token: 0x04000A78 RID: 2680
			ClassListDB,
			// Token: 0x04000A79 RID: 2681
			NamedListDB,
			// Token: 0x04000A7A RID: 2682
			TitleListDB,
			// Token: 0x04000A7B RID: 2683
			WeaponListDB,
			// Token: 0x04000A7C RID: 2684
			WeaponRecipeListDB,
			// Token: 0x04000A7D RID: 2685
			CharaLimitBreakListDB,
			// Token: 0x04000A7E RID: 2686
			CharaEvolutionListDB,
			// Token: 0x04000A7F RID: 2687
			LeaderSkillListDB,
			// Token: 0x04000A80 RID: 2688
			SkillListDB_PL,
			// Token: 0x04000A81 RID: 2689
			SkillListDB_EN,
			// Token: 0x04000A82 RID: 2690
			SkillListDB_WPN,
			// Token: 0x04000A83 RID: 2691
			SkillListDB_MST,
			// Token: 0x04000A84 RID: 2692
			SkillListDB_CARD,
			// Token: 0x04000A85 RID: 2693
			SkillContentListDB_PL,
			// Token: 0x04000A86 RID: 2694
			SkillContentListDB_EN,
			// Token: 0x04000A87 RID: 2695
			SkillContentListDB_WPN,
			// Token: 0x04000A88 RID: 2696
			SkillContentListDB_MST,
			// Token: 0x04000A89 RID: 2697
			SkillContentListDB_CARD,
			// Token: 0x04000A8A RID: 2698
			SkillLvCoefDB,
			// Token: 0x04000A8B RID: 2699
			BattleDefineDB,
			// Token: 0x04000A8C RID: 2700
			SkillContentAndEffectCombiDB,
			// Token: 0x04000A8D RID: 2701
			UniqueSkillSoundDB,
			// Token: 0x04000A8E RID: 2702
			GachaCutInListDB,
			// Token: 0x04000A8F RID: 2703
			GachaItemLabelListDB,
			// Token: 0x04000A90 RID: 2704
			CharacterFacialDB,
			// Token: 0x04000A91 RID: 2705
			CharacterFlavorTextDB,
			// Token: 0x04000A92 RID: 2706
			ItemListDB,
			// Token: 0x04000A93 RID: 2707
			MasterOrbListDB,
			// Token: 0x04000A94 RID: 2708
			QuestWaveListDB,
			// Token: 0x04000A95 RID: 2709
			QuestWaveRandomListDB,
			// Token: 0x04000A96 RID: 2710
			QuestWaveDropItemDB,
			// Token: 0x04000A97 RID: 2711
			QuestEnemyListDB,
			// Token: 0x04000A98 RID: 2712
			QuestADVTriggerDB,
			// Token: 0x04000A99 RID: 2713
			EnemyResourceListDB,
			// Token: 0x04000A9A RID: 2714
			TownObjectListDB,
			// Token: 0x04000A9B RID: 2715
			TownShopListDB,
			// Token: 0x04000A9C RID: 2716
			TownObjLvUpDB,
			// Token: 0x04000A9D RID: 2717
			TownObjBuffDB,
			// Token: 0x04000A9E RID: 2718
			RoomListDB,
			// Token: 0x04000A9F RID: 2719
			RoomObjectListDB,
			// Token: 0x04000AA0 RID: 2720
			RoomShopListDB,
			// Token: 0x04000AA1 RID: 2721
			EffectListDB,
			// Token: 0x04000AA2 RID: 2722
			MasterRankDB,
			// Token: 0x04000AA3 RID: 2723
			MasterOrbExpDB,
			// Token: 0x04000AA4 RID: 2724
			CharacterExpDB,
			// Token: 0x04000AA5 RID: 2725
			NamedFriendshipExpDB,
			// Token: 0x04000AA6 RID: 2726
			WeaponExpDB,
			// Token: 0x04000AA7 RID: 2727
			SkillExpDB,
			// Token: 0x04000AA8 RID: 2728
			CharacterParamGrowthListDB,
			// Token: 0x04000AA9 RID: 2729
			ProfileVoiceListDB,
			// Token: 0x04000AAA RID: 2730
			FieldItemDropListDB,
			// Token: 0x04000AAB RID: 2731
			PopUpStateIconListDB,
			// Token: 0x04000AAC RID: 2732
			CharaIllustOffsetDB,
			// Token: 0x04000AAD RID: 2733
			ADVListDB,
			// Token: 0x04000AAE RID: 2734
			ADVLibraryListDB,
			// Token: 0x04000AAF RID: 2735
			WordLibraryListDB,
			// Token: 0x04000AB0 RID: 2736
			OriginalCharaLibraryListDB,
			// Token: 0x04000AB1 RID: 2737
			SceneInfoListDB,
			// Token: 0x04000AB2 RID: 2738
			TutorialTipsListDB,
			// Token: 0x04000AB3 RID: 2739
			RetireTipsListDB,
			// Token: 0x04000AB4 RID: 2740
			TutorialMessageListDB,
			// Token: 0x04000AB5 RID: 2741
			WebDataListDB,
			// Token: 0x04000AB6 RID: 2742
			TownObjBuildSubDB,
			// Token: 0x04000AB7 RID: 2743
			Num
		}

		// Token: 0x02000180 RID: 384
		private enum ePrepareStep
		{
			// Token: 0x04000AB9 RID: 2745
			None = -1,
			// Token: 0x04000ABA RID: 2746
			Preparing,
			// Token: 0x04000ABB RID: 2747
			DonePrepare
		}
	}
}
