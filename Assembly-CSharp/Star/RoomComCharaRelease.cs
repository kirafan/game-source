﻿using System;

namespace Star
{
	// Token: 0x020005F5 RID: 1525
	public class RoomComCharaRelease : IRoomEventCommand
	{
		// Token: 0x06001E26 RID: 7718 RVA: 0x000A2094 File Offset: 0x000A0494
		public RoomComCharaRelease(IRoomObjectControll pobj)
		{
			this.m_Target = pobj;
			this.m_Type = eRoomRequest.CharaRelease;
			this.m_Enable = true;
		}

		// Token: 0x06001E27 RID: 7719 RVA: 0x000A20B4 File Offset: 0x000A04B4
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			bool result = false;
			this.m_Target.UpdateObj();
			if (!this.m_Target.IsActive())
			{
				this.m_Enable = false;
				this.m_Target = null;
				result = true;
			}
			return result;
		}

		// Token: 0x040024A3 RID: 9379
		public IRoomObjectControll m_Target;

		// Token: 0x040024A4 RID: 9380
		public int m_Step;
	}
}
