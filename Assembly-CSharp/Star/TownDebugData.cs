﻿using System;

namespace Star
{
	// Token: 0x02000296 RID: 662
	[Serializable]
	public class TownDebugData
	{
		// Token: 0x04001512 RID: 5394
		public bool m_BuildBuf;

		// Token: 0x04001513 RID: 5395
		public int m_SettingPoint;

		// Token: 0x04001514 RID: 5396
		public int m_SettingSubPoint;

		// Token: 0x04001515 RID: 5397
		public int m_ObjectNo;

		// Token: 0x04001516 RID: 5398
		public int m_Level;
	}
}
