﻿using System;

namespace Star
{
	// Token: 0x020002B9 RID: 697
	public class CharacterDataLocked : CharacterDataWrapperNoExist
	{
		// Token: 0x06000D13 RID: 3347 RVA: 0x00049375 File Offset: 0x00047775
		public CharacterDataLocked() : base(eCharacterDataType.Lock)
		{
		}
	}
}
