﻿using System;

namespace Star
{
	// Token: 0x020006A2 RID: 1698
	public enum eTownEventAct
	{
		// Token: 0x04002883 RID: 10371
		CompleteOpen,
		// Token: 0x04002884 RID: 10372
		OpenSelectUI,
		// Token: 0x04002885 RID: 10373
		CloseSelectUI,
		// Token: 0x04002886 RID: 10374
		ReFlesh,
		// Token: 0x04002887 RID: 10375
		ChangeObject,
		// Token: 0x04002888 RID: 10376
		ChangeModel,
		// Token: 0x04002889 RID: 10377
		StepIn,
		// Token: 0x0400288A RID: 10378
		StepOut,
		// Token: 0x0400288B RID: 10379
		StateChange,
		// Token: 0x0400288C RID: 10380
		SelectOff,
		// Token: 0x0400288D RID: 10381
		SelectTouch,
		// Token: 0x0400288E RID: 10382
		ReBuildMaker,
		// Token: 0x0400288F RID: 10383
		MovePos,
		// Token: 0x04002890 RID: 10384
		LinkCutPos,
		// Token: 0x04002891 RID: 10385
		Rebind,
		// Token: 0x04002892 RID: 10386
		MenuMode,
		// Token: 0x04002893 RID: 10387
		PlayEffect
	}
}
