﻿using System;

namespace Star
{
	// Token: 0x02000229 RID: 553
	public enum eStateAbnormal
	{
		// Token: 0x04000F1B RID: 3867
		Confusion,
		// Token: 0x04000F1C RID: 3868
		Paralysis,
		// Token: 0x04000F1D RID: 3869
		Poison,
		// Token: 0x04000F1E RID: 3870
		Bearish,
		// Token: 0x04000F1F RID: 3871
		Sleep,
		// Token: 0x04000F20 RID: 3872
		Unhappy,
		// Token: 0x04000F21 RID: 3873
		Silence,
		// Token: 0x04000F22 RID: 3874
		Isolation,
		// Token: 0x04000F23 RID: 3875
		Num
	}
}
