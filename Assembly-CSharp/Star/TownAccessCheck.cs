﻿using System;

namespace Star
{
	// Token: 0x020002FC RID: 764
	public class TownAccessCheck
	{
		// Token: 0x06000EFB RID: 3835 RVA: 0x00050170 File Offset: 0x0004E570
		public static bool IsAccessSearchBuild(ref TownAccessCheck.ChrCheckDataBase pbase, ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			bool flag = false;
			ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(AccessID);
			switch (scheduleNameTag.m_BuildMoveType)
			{
			case 0:
				flag = true;
				break;
			case 1:
				flag = (pbuild.m_ResourceID == scheduleNameTag.m_BuildMoveCode);
				break;
			case 2:
				flag = (pbuild.m_ClassID == pbase.m_ClassID);
				break;
			case 3:
				flag = (pbuild.m_ElementID == pbase.m_Element);
				break;
			case 4:
				flag = (pbuild.m_Category == eTownObjectCategory.Buf);
				break;
			case 5:
				flag |= (pbuild.m_Category == eTownObjectCategory.Money);
				flag |= (pbuild.m_Category == eTownObjectCategory.Item);
				break;
			case 6:
				flag = (pbuild.m_ClassID != eClassType.None);
				break;
			case 7:
				flag = (pbuild.m_ElementID != eElementType.None);
				break;
			}
			return flag;
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x00050250 File Offset: 0x0004E650
		public static bool IsAccessSearchBuild(ref TownAccessCheck.BuildCheckDataBase pbuild, int AccessID)
		{
			bool flag = false;
			ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(AccessID);
			switch (scheduleNameTag.m_BuildMoveType)
			{
			case 0:
				flag = true;
				break;
			case 1:
				flag = (pbuild.m_ResourceID == scheduleNameTag.m_BuildMoveCode);
				break;
			case 4:
				flag = (pbuild.m_Category == eTownObjectCategory.Buf);
				break;
			case 5:
				flag |= (pbuild.m_Category == eTownObjectCategory.Money);
				flag |= (pbuild.m_Category == eTownObjectCategory.Item);
				break;
			case 6:
				flag = (pbuild.m_ClassID != eClassType.None);
				break;
			case 7:
				flag = (pbuild.m_ElementID != eElementType.None);
				break;
			}
			return flag;
		}

		// Token: 0x020002FD RID: 765
		public class ChrCheckDataBase
		{
			// Token: 0x06000EFD RID: 3837 RVA: 0x00050310 File Offset: 0x0004E710
			public ChrCheckDataBase(long fmanageid)
			{
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				this.m_ManageID = fmanageid;
				UserCharacterData charaData = UserCharaUtil.GetCharaData(fmanageid);
				if (charaData != null)
				{
					this.m_ClassID = charaData.Param.ClassType;
					this.m_Element = charaData.Param.ElementType;
					this.m_Title = (eTitleType)dbMng.NamedListDB.GetParam(charaData.Param.NamedType).m_TitleType;
				}
			}

			// Token: 0x06000EFE RID: 3838 RVA: 0x00050388 File Offset: 0x0004E788
			public bool IsCheckToCharaClass(int ChkParam)
			{
				return this.m_ClassID == (eClassType)ChkParam;
			}

			// Token: 0x06000EFF RID: 3839 RVA: 0x00050393 File Offset: 0x0004E793
			public bool IsCheckToElement(int ChkParam)
			{
				return this.m_Element == (eElementType)ChkParam;
			}

			// Token: 0x040015D6 RID: 5590
			public long m_ManageID;

			// Token: 0x040015D7 RID: 5591
			public eTitleType m_Title;

			// Token: 0x040015D8 RID: 5592
			public eClassType m_ClassID;

			// Token: 0x040015D9 RID: 5593
			public eElementType m_Element;
		}

		// Token: 0x020002FE RID: 766
		public struct BuildCheckDataBase
		{
			// Token: 0x06000F00 RID: 3840 RVA: 0x000503A0 File Offset: 0x0004E7A0
			public void SetBuildParam(long fmanageid)
			{
				this.m_ManageID = fmanageid;
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
				if (townBuildData != null)
				{
					TownObjectListDB_Param param = dbMng.TownObjListDB.GetParam(townBuildData.m_ObjID);
					this.m_ObjID = townBuildData.m_ObjID;
					this.m_Title = (eTitleType)param.m_TitleType;
					this.m_ClassID = (eClassType)param.m_ClassID;
					this.m_ElementID = (eElementType)param.m_ElementID;
					this.m_Category = (eTownObjectCategory)param.m_Category;
					this.m_ResourceID = param.m_ResourceID;
				}
			}

			// Token: 0x040015DA RID: 5594
			public long m_ManageID;

			// Token: 0x040015DB RID: 5595
			public int m_ObjID;

			// Token: 0x040015DC RID: 5596
			public eTitleType m_Title;

			// Token: 0x040015DD RID: 5597
			public eClassType m_ClassID;

			// Token: 0x040015DE RID: 5598
			public eElementType m_ElementID;

			// Token: 0x040015DF RID: 5599
			public eTownObjectCategory m_Category;

			// Token: 0x040015E0 RID: 5600
			public int m_ResourceID;
		}
	}
}
