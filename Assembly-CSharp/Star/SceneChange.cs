﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AD4 RID: 2772
	public class SceneChange : MonoBehaviour
	{
		// Token: 0x06003A09 RID: 14857 RVA: 0x00127ED4 File Offset: 0x001262D4
		private void Start()
		{
			this.m_MeigeAnimCtrl.Open();
			this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, this.m_MeigeAnimClipHolders[0]);
			this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, this.m_MeigeAnimClipHolders[1]);
			this.m_MeigeAnimCtrl.Close();
			this.m_BodyObj.SetActive(false);
		}

		// Token: 0x06003A0A RID: 14858 RVA: 0x00127F38 File Offset: 0x00126338
		private void Update()
		{
			switch (this.m_Step)
			{
			case SceneChange.eStep.In:
				if (!this.IsPlaying())
				{
					this.m_MeigeAnimCtrl.Pause();
					this.m_Step = SceneChange.eStep.Overlay;
				}
				break;
			case SceneChange.eStep.Out:
				if (!this.IsPlaying())
				{
					this.m_BodyObj.SetActive(false);
					this.m_MeigeAnimCtrl.Pause();
					this.m_Step = SceneChange.eStep.None;
				}
				break;
			}
		}

		// Token: 0x06003A0B RID: 14859 RVA: 0x00127FC8 File Offset: 0x001263C8
		public void Play(SceneChange.eAnimType animType)
		{
			this.AdjustScale();
			this.m_BodyObj.SetActive(true);
			this.m_MeigeAnimCtrl.Play(SceneChange.ANIM_KEYS[(int)animType], 0f, WrapMode.ClampForever);
			this.m_MeigeAnimCtrl.SetAnimationPlaySpeed(1.2f);
			if (animType == SceneChange.eAnimType.In)
			{
				this.m_Step = SceneChange.eStep.In;
			}
			else
			{
				this.m_Step = SceneChange.eStep.Out;
			}
		}

		// Token: 0x06003A0C RID: 14860 RVA: 0x00128028 File Offset: 0x00126428
		public void PlayQuick(SceneChange.eAnimType animType)
		{
			this.AdjustScale();
			if (animType == SceneChange.eAnimType.In)
			{
				this.m_BodyObj.SetActive(true);
				this.m_MsbHndl.SetDefaultVisibility();
				this.m_Step = SceneChange.eStep.Overlay;
			}
			else
			{
				this.m_BodyObj.SetActive(false);
				this.m_Step = SceneChange.eStep.None;
			}
		}

		// Token: 0x06003A0D RID: 14861 RVA: 0x00128077 File Offset: 0x00126477
		public bool IsComplete()
		{
			return this.m_Step != SceneChange.eStep.In && this.m_Step != SceneChange.eStep.Out;
		}

		// Token: 0x06003A0E RID: 14862 RVA: 0x00128093 File Offset: 0x00126493
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x06003A0F RID: 14863 RVA: 0x001280B0 File Offset: 0x001264B0
		private void AdjustScale()
		{
			float num = (float)this.m_BaseHeight / (float)this.m_BaseWidth;
			float num2 = (float)Screen.height / (float)Screen.width;
			float num3 = Mathf.Max(1f, num2 / num);
			this.m_BaseTransform.localScale = new Vector3(1f * num3, 1f * num3, 1f);
		}

		// Token: 0x04004142 RID: 16706
		[SerializeField]
		private GameObject m_BodyObj;

		// Token: 0x04004143 RID: 16707
		[SerializeField]
		private MsbHandler m_MsbHndl;

		// Token: 0x04004144 RID: 16708
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04004145 RID: 16709
		[SerializeField]
		private MeigeAnimClipHolder[] m_MeigeAnimClipHolders;

		// Token: 0x04004146 RID: 16710
		[SerializeField]
		private Transform m_BaseTransform;

		// Token: 0x04004147 RID: 16711
		[SerializeField]
		private int m_BaseWidth = 1334;

		// Token: 0x04004148 RID: 16712
		[SerializeField]
		private int m_BaseHeight = 750;

		// Token: 0x04004149 RID: 16713
		private static readonly string[] ANIM_KEYS = new string[]
		{
			"st",
			"ed"
		};

		// Token: 0x0400414A RID: 16714
		private SceneChange.eStep m_Step = SceneChange.eStep.None;

		// Token: 0x02000AD5 RID: 2773
		public enum eAnimType
		{
			// Token: 0x0400414C RID: 16716
			In,
			// Token: 0x0400414D RID: 16717
			Out,
			// Token: 0x0400414E RID: 16718
			Num
		}

		// Token: 0x02000AD6 RID: 2774
		private enum eStep
		{
			// Token: 0x04004150 RID: 16720
			None = -1,
			// Token: 0x04004151 RID: 16721
			In,
			// Token: 0x04004152 RID: 16722
			Overlay,
			// Token: 0x04004153 RID: 16723
			Out
		}
	}
}
