﻿using System;

namespace Star
{
	// Token: 0x0200020D RID: 525
	[Serializable]
	public struct SkillLvCoefDB_Param
	{
		// Token: 0x04000CEF RID: 3311
		public int m_LvRangeMin;

		// Token: 0x04000CF0 RID: 3312
		public int m_LvRangeMax;

		// Token: 0x04000CF1 RID: 3313
		public float[] m_Coefs;
	}
}
