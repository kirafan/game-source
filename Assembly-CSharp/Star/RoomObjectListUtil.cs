﻿using System;

namespace Star
{
	// Token: 0x020001F8 RID: 504
	public class RoomObjectListUtil
	{
		// Token: 0x06000B5B RID: 2907 RVA: 0x0004313C File Offset: 0x0004153C
		public static int CategoryIDToAccessKey(eRoomObjectCategory category, int objID)
		{
			return (int)(category * (eRoomObjectCategory)100000 + objID);
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x00043147 File Offset: 0x00041547
		public static eRoomObjectCategory GetIDToCategory(int accessId)
		{
			return (eRoomObjectCategory)(accessId / 100000);
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x00043150 File Offset: 0x00041550
		public static RoomObjectListDB_Param GetObjectParam(eRoomObjectCategory fcategory, int fobjid)
		{
			RoomObjectListDB roomObjectListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RoomObjectListDB;
			RoomObjectListDB_Param result = roomObjectListDB.m_Params[0];
			int num = RoomObjectListUtil.CategoryIDToAccessKey(fcategory, fobjid);
			for (int i = 0; i < roomObjectListDB.m_Params.Length; i++)
			{
				if (roomObjectListDB.m_Params[i].m_DBAccessID == num)
				{
					result = roomObjectListDB.m_Params[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000B5E RID: 2910 RVA: 0x000431D0 File Offset: 0x000415D0
		public static RoomObjectListDB_Param GetAccessKeyToObjectParam(int faccesskey)
		{
			RoomObjectListDB roomObjectListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RoomObjectListDB;
			RoomObjectListDB_Param result = roomObjectListDB.m_Params[0];
			for (int i = 0; i < roomObjectListDB.m_Params.Length; i++)
			{
				if (roomObjectListDB.m_Params[i].m_DBAccessID == faccesskey)
				{
					result = roomObjectListDB.m_Params[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000B5F RID: 2911 RVA: 0x00043247 File Offset: 0x00041647
		public static int UpDataParamToResourceID(UserRoomObjectData pobj)
		{
			return RoomObjectListUtil.CategoryIDToAccessKey(pobj.ObjCategory, pobj.ObjID);
		}
	}
}
