﻿using System;

namespace Star
{
	// Token: 0x02000507 RID: 1287
	public class IMissionUIData
	{
		// Token: 0x0400201A RID: 8218
		public eMissionCategory m_Type;

		// Token: 0x0400201B RID: 8219
		public string m_TargetMessage;

		// Token: 0x0400201C RID: 8220
		public eMissionRewardCategory m_RewardType;

		// Token: 0x0400201D RID: 8221
		public long m_ManageID;

		// Token: 0x0400201E RID: 8222
		public int m_RewardID;

		// Token: 0x0400201F RID: 8223
		public int m_RewardNum;

		// Token: 0x04002020 RID: 8224
		public long m_DatabaseID;

		// Token: 0x04002021 RID: 8225
		public int m_Rate;

		// Token: 0x04002022 RID: 8226
		public int m_RateMax;

		// Token: 0x04002023 RID: 8227
		public eMissionState m_State;

		// Token: 0x04002024 RID: 8228
		public DateTime m_LimitTime;

		// Token: 0x04002025 RID: 8229
		public int m_Num;
	}
}
