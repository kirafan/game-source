﻿using System;

namespace Star
{
	// Token: 0x020001AD RID: 429
	public static class SoundHomeBgmListDB_Ext
	{
		// Token: 0x06000B24 RID: 2852 RVA: 0x00042598 File Offset: 0x00040998
		public static eSoundBgmListDB GetCueID(this SoundHomeBgmListDB self, DateTime nowTime)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				DateTime t = DateTime.Parse(self.m_Params[i].m_StartAt);
				DateTime t2 = DateTime.Parse(self.m_Params[i].m_EndAt);
				if (nowTime >= t && nowTime <= t2)
				{
					return (eSoundBgmListDB)self.m_Params[i].m_CueID;
				}
			}
			return eSoundBgmListDB.None;
		}
	}
}
