﻿using System;
using Star.UI;

namespace Star
{
	// Token: 0x020003DB RID: 987
	public class ADVState_Init : ADVState
	{
		// Token: 0x060012C7 RID: 4807 RVA: 0x00064813 File Offset: 0x00062C13
		public ADVState_Init(ADVMain owner) : base(owner)
		{
		}

		// Token: 0x060012C8 RID: 4808 RVA: 0x00064823 File Offset: 0x00062C23
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x060012C9 RID: 4809 RVA: 0x00064826 File Offset: 0x00062C26
		public override void OnStateEnter()
		{
			this.m_Step = ADVState_Init.eStep.PrepareDB;
		}

		// Token: 0x060012CA RID: 4810 RVA: 0x0006482F File Offset: 0x00062C2F
		public override void OnStateExit()
		{
		}

		// Token: 0x060012CB RID: 4811 RVA: 0x00064831 File Offset: 0x00062C31
		public override void OnDispose()
		{
		}

		// Token: 0x060012CC RID: 4812 RVA: 0x00064834 File Offset: 0x00062C34
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ADVState_Init.eStep.PrepareDB:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.OpenQuickIfNotDisplay(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					this.m_Owner.Player.PrepareDB();
					this.m_Step = ADVState_Init.eStep.PrepareDBWait;
				}
				break;
			case ADVState_Init.eStep.PrepareDBWait:
				this.m_Owner.Player.UpdatePrepareDB();
				if (this.m_Owner.Player.IsDonePrepareDB())
				{
					this.m_Step = ADVState_Init.eStep.Prepare;
				}
				break;
			case ADVState_Init.eStep.Prepare:
				this.m_Owner.Player.Prepare(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvID);
				this.m_Step = ADVState_Init.eStep.PrepareWait;
				break;
			case ADVState_Init.eStep.PrepareWait:
				this.m_Owner.Player.UpdatePrepare();
				if (this.m_Owner.Player.IsDonePrepare)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = ADVState_Init.eStep.FadeWait;
				}
				break;
			case ADVState_Init.eStep.FadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Owner.Player.Play();
					this.m_Step = ADVState_Init.eStep.End;
				}
				break;
			case ADVState_Init.eStep.End:
				this.m_Step = ADVState_Init.eStep.None;
				return 1;
			}
			return -1;
		}

		// Token: 0x060012CD RID: 4813 RVA: 0x000649B1 File Offset: 0x00062DB1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x0400199A RID: 6554
		private ADVState_Init.eStep m_Step = ADVState_Init.eStep.None;

		// Token: 0x020003DC RID: 988
		private enum eStep
		{
			// Token: 0x0400199C RID: 6556
			None = -1,
			// Token: 0x0400199D RID: 6557
			PrepareDB,
			// Token: 0x0400199E RID: 6558
			PrepareDBWait,
			// Token: 0x0400199F RID: 6559
			Prepare,
			// Token: 0x040019A0 RID: 6560
			PrepareWait,
			// Token: 0x040019A1 RID: 6561
			FadeWait,
			// Token: 0x040019A2 RID: 6562
			End
		}
	}
}
