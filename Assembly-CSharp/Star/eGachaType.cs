﻿using System;

namespace Star
{
	// Token: 0x02000233 RID: 563
	public enum eGachaType
	{
		// Token: 0x04000F6A RID: 3946
		Summon = 1,
		// Token: 0x04000F6B RID: 3947
		Pickup,
		// Token: 0x04000F6C RID: 3948
		Won,
		// Token: 0x04000F6D RID: 3949
		UGem
	}
}
