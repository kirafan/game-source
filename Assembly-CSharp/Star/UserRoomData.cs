﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B4E RID: 2894
	[Serializable]
	public class UserRoomData
	{
		// Token: 0x06003D4E RID: 15694 RVA: 0x001367FC File Offset: 0x00134BFC
		public UserRoomData()
		{
			this.MngID = -1L;
			this.FloorID = -1;
		}

		// Token: 0x06003D4F RID: 15695 RVA: 0x0013681E File Offset: 0x00134C1E
		public void AddPlacement(UserRoomData.PlacementData placementData)
		{
			this.m_Placements.Add(placementData);
		}

		// Token: 0x06003D50 RID: 15696 RVA: 0x0013682C File Offset: 0x00134C2C
		public void RemovePlacement(long fmanageid)
		{
			for (int i = 0; i < this.m_Placements.Count; i++)
			{
				if (this.m_Placements[i].m_ManageID == fmanageid)
				{
					this.m_Placements.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x06003D51 RID: 15697 RVA: 0x0013687D File Offset: 0x00134C7D
		public void ClearPlacements()
		{
			this.m_Placements.Clear();
		}

		// Token: 0x06003D52 RID: 15698 RVA: 0x0013688A File Offset: 0x00134C8A
		public int GetPlacementNum()
		{
			return this.m_Placements.Count;
		}

		// Token: 0x06003D53 RID: 15699 RVA: 0x00136897 File Offset: 0x00134C97
		public UserRoomData.PlacementData GetPlacementAt(int index)
		{
			return this.m_Placements[index];
		}

		// Token: 0x06003D54 RID: 15700 RVA: 0x001368A8 File Offset: 0x00134CA8
		public UserRoomData.PlacementData GetPlacementManageID(long fmanageid)
		{
			for (int i = 0; i < this.m_Placements.Count; i++)
			{
				if (this.m_Placements[i].m_ManageID == fmanageid)
				{
					return this.m_Placements[i];
				}
			}
			return null;
		}

		// Token: 0x06003D55 RID: 15701 RVA: 0x001368F8 File Offset: 0x00134CF8
		public UserRoomData.PlacementData GetPlacement(int x, int y, int froomNo = 0)
		{
			for (int i = 0; i < this.m_Placements.Count; i++)
			{
				if (this.m_Placements[i].m_X == x && this.m_Placements[i].m_Y == y && this.m_Placements[i].m_RoomNo == froomNo)
				{
					return this.m_Placements[i];
				}
			}
			return null;
		}

		// Token: 0x040044BF RID: 17599
		public const int Version = 0;

		// Token: 0x040044C0 RID: 17600
		public long MngID;

		// Token: 0x040044C1 RID: 17601
		public int FloorID;

		// Token: 0x040044C2 RID: 17602
		public long PlayerID;

		// Token: 0x040044C3 RID: 17603
		[SerializeField]
		private List<UserRoomData.PlacementData> m_Placements = new List<UserRoomData.PlacementData>();

		// Token: 0x02000B4F RID: 2895
		[Serializable]
		public class PlacementData
		{
			// Token: 0x06003D56 RID: 15702 RVA: 0x00136974 File Offset: 0x00134D74
			public PlacementData(int x, int y, CharacterDefine.eDir dir, long fmanageid, int roomSubID, int resourceID)
			{
				this.m_X = x;
				this.m_Y = y;
				this.m_Dir = dir;
				this.m_RoomNo = roomSubID;
				this.m_ManageID = fmanageid;
				this.m_ObjectID = resourceID;
			}

			// Token: 0x040044C4 RID: 17604
			public int m_RoomNo;

			// Token: 0x040044C5 RID: 17605
			public int m_X;

			// Token: 0x040044C6 RID: 17606
			public int m_Y;

			// Token: 0x040044C7 RID: 17607
			public CharacterDefine.eDir m_Dir;

			// Token: 0x040044C8 RID: 17608
			public int m_ObjectID;

			// Token: 0x040044C9 RID: 17609
			public long m_ManageID;
		}
	}
}
