﻿using System;

namespace Star
{
	// Token: 0x020002D1 RID: 721
	public abstract class EquipWeaponManagedPartyMemberController : EquipWeaponCharacterDataControllerExistExGen<EquipWeaponManagedPartyMemberController, UserCharacterDataWrapper>
	{
		// Token: 0x06000E07 RID: 3591 RVA: 0x0004D50C File Offset: 0x0004B90C
		public EquipWeaponManagedPartyMemberController(eCharacterDataControllerType in_type) : base(in_type)
		{
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x0004D515 File Offset: 0x0004B915
		public EquipWeaponManagedPartyMemberController Setup(int in_partyIndex, int in_partySlotIndex)
		{
			base.SetupBase(in_partyIndex, in_partySlotIndex);
			return this.Apply(in_partyIndex, in_partySlotIndex);
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x0004D528 File Offset: 0x0004B928
		private EquipWeaponManagedPartyMemberController Apply(int in_partyIndex, int in_partySlotIndex)
		{
			return (EquipWeaponManagedPartyMemberController)this.ApplyBase(in_partyIndex, in_partySlotIndex);
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x0004D537 File Offset: 0x0004B937
		protected override EquipWeaponPartyMemberController ApplyBase(int in_partyIndex, int in_partySlotIndex)
		{
			base.ApplyCharacterDataEx(this.GetManagedCharacterDataFromGlobal(in_partyIndex, in_partySlotIndex));
			this.ApplyWeaponData(this.GetManagedWeaponDataFromGlobal(in_partyIndex, in_partySlotIndex));
			return this;
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x0004D558 File Offset: 0x0004B958
		private EquipWeaponManagedPartyMemberController ApplyWeaponData(UserWeaponData in_uwd)
		{
			this.weaponData = in_uwd;
			return this;
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x0004D562 File Offset: 0x0004B962
		protected virtual CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x06000E0D RID: 3597 RVA: 0x0004D565 File Offset: 0x0004B965
		protected virtual UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return null;
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x0004D568 File Offset: 0x0004B968
		public UserWeaponData GetWeaponData()
		{
			return this.weaponData;
		}

		// Token: 0x06000E0F RID: 3599 RVA: 0x0004D570 File Offset: 0x0004B970
		public override int GetWeaponIdNaked()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.WeaponID;
			}
			return result;
		}

		// Token: 0x06000E10 RID: 3600 RVA: 0x0004D59C File Offset: 0x0004B99C
		public override int GetWeaponSkillID()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.SkillLearnData.SkillID;
			}
			return result;
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x0004D5D0 File Offset: 0x0004B9D0
		public override sbyte GetWeaponSkillExpTableID()
		{
			sbyte result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.SkillLearnData.ExpTableID;
			}
			return result;
		}

		// Token: 0x06000E12 RID: 3602 RVA: 0x0004D604 File Offset: 0x0004BA04
		public override int GetWeaponLv()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.Lv;
			}
			return result;
		}

		// Token: 0x06000E13 RID: 3603 RVA: 0x0004D630 File Offset: 0x0004BA30
		public override int GetWeaponMaxLv()
		{
			int result = -1;
			if (this.weaponData != null)
			{
				result = this.weaponData.Param.MaxLv;
			}
			return result;
		}

		// Token: 0x06000E14 RID: 3604 RVA: 0x0004D65C File Offset: 0x0004BA5C
		public override long GetWeaponExp()
		{
			return this.weaponData.Param.Exp;
		}

		// Token: 0x06000E15 RID: 3605 RVA: 0x0004D66E File Offset: 0x0004BA6E
		public override int GetWeaponSkillLv()
		{
			return this.weaponData.Param.SkillLearnData.SkillLv;
		}

		// Token: 0x06000E16 RID: 3606 RVA: 0x0004D685 File Offset: 0x0004BA85
		public override int GetWeaponSkillMaxLv()
		{
			return this.weaponData.Param.SkillLearnData.SkillMaxLv;
		}

		// Token: 0x06000E17 RID: 3607 RVA: 0x0004D69C File Offset: 0x0004BA9C
		public override EditUtility.OutputCharaParam GetWeaponParam()
		{
			return EditUtility.CalcWeaponParam(this.weaponData);
		}

		// Token: 0x04001596 RID: 5526
		protected UserWeaponData weaponData;
	}
}
