﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000677 RID: 1655
	public class TownCameraMove : ITownCameraKey
	{
		// Token: 0x06002182 RID: 8578 RVA: 0x000B278F File Offset: 0x000B0B8F
		public TownCameraMove(TownCamera ptarget, Vector3 fbasepos, Vector3 ftargetpos, float ftime)
		{
			this.m_LineType = TownCameraMove.ePathType.Linear;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BasePos = fbasepos;
			this.m_TargetPos = ftargetpos;
			this.m_Camera = ptarget;
			this.m_Active = true;
		}

		// Token: 0x06002183 RID: 8579 RVA: 0x000B27D0 File Offset: 0x000B0BD0
		public TownCameraMove(TownCamera ptarget, Vector3 fbasepos, Vector3 fpoint1, Vector3 fpoint2, Vector3 ftargetpos, float ftime)
		{
			this.m_LineType = TownCameraMove.ePathType.Bez;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BasePos = fbasepos;
			this.m_TargetPos = ftargetpos;
			this.m_Point1 = fpoint1;
			this.m_Point2 = fpoint2;
			this.m_Camera = ptarget;
			this.m_Active = true;
		}

		// Token: 0x06002184 RID: 8580 RVA: 0x000B282C File Offset: 0x000B0C2C
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Camera.transform.localPosition = this.m_TargetPos;
				this.m_Active = false;
			}
			else
			{
				float num = this.m_Time / this.m_MaxTime;
				if (this.m_LineType == TownCameraMove.ePathType.Linear)
				{
					this.m_Camera.transform.localPosition = (this.m_TargetPos - this.m_BasePos) * num + this.m_BasePos;
				}
				else
				{
					float num2 = 1f - num;
					float num3 = num2 * num2;
					float num4 = num * num;
					float d = num2 * num3;
					float d2 = num * num4;
					num3 = num3 * num * 3f;
					num4 = num4 * num2 * 3f;
					this.m_Camera.transform.localPosition = this.m_BasePos * d + this.m_Point1 * num3 + this.m_Point2 * num4 + this.m_TargetPos * d2;
				}
			}
			return this.m_Active;
		}

		// Token: 0x040027DD RID: 10205
		private Vector3 m_BasePos;

		// Token: 0x040027DE RID: 10206
		private Vector3 m_TargetPos;

		// Token: 0x040027DF RID: 10207
		private Vector3 m_Point1;

		// Token: 0x040027E0 RID: 10208
		private Vector3 m_Point2;

		// Token: 0x040027E1 RID: 10209
		private float m_Time;

		// Token: 0x040027E2 RID: 10210
		private float m_MaxTime;

		// Token: 0x040027E3 RID: 10211
		private TownCameraMove.ePathType m_LineType;

		// Token: 0x02000678 RID: 1656
		private enum ePathType
		{
			// Token: 0x040027E5 RID: 10213
			Linear,
			// Token: 0x040027E6 RID: 10214
			Bez
		}
	}
}
