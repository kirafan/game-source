﻿using System;

namespace Star
{
	// Token: 0x0200052F RID: 1327
	public class CharaActionLift : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x06001A88 RID: 6792 RVA: 0x0008D4E4 File Offset: 0x0008B8E4
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
			this.m_PartsAction = new RoomPartsActionPakage();
			base.SetUpPlayScene(roomObjHndl, pOwner, psetup, new RoomActionScriptPlay.ActionSciptCallBack(this.CallbackActionEvent), this.m_PartsAction);
			this.m_Base.SetLinkObject(this.m_Target, true);
			this.m_AnimePlay.SetBlink(false);
		}

		// Token: 0x06001A89 RID: 6793 RVA: 0x0008D538 File Offset: 0x0008B938
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			this.m_PartsAction.PakageUpdate();
			bool flag = base.PlayScene(hbase);
			if (!flag)
			{
				this.m_Base.SetLinkObject(this.m_Target, false);
			}
			return flag;
		}

		// Token: 0x06001A8A RID: 6794 RVA: 0x0008D573 File Offset: 0x0008B973
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			return true;
		}

		// Token: 0x06001A8B RID: 6795 RVA: 0x0008D576 File Offset: 0x0008B976
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			this.m_Base.SetLinkObject(this.m_Target, false);
			base.CancelPlay();
			return true;
		}

		// Token: 0x06001A8C RID: 6796 RVA: 0x0008D591 File Offset: 0x0008B991
		public void Release()
		{
			base.ReleasePlay();
			this.m_PartsAction = null;
		}
	}
}
