﻿using System;

namespace Star
{
	// Token: 0x0200054F RID: 1359
	public class CActScriptKeyProgramEvent : IRoomScriptData
	{
		// Token: 0x06001ACF RID: 6863 RVA: 0x0008EED0 File Offset: 0x0008D2D0
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyProgramEvent actXlsKeyProgramEvent = (ActXlsKeyProgramEvent)pbase;
			this.m_EventID = actXlsKeyProgramEvent.m_EventID;
			this.m_OptionKey = actXlsKeyProgramEvent.m_OptionKey;
			this.m_EvtTime = (float)actXlsKeyProgramEvent.m_EvtTime / 30f;
		}

		// Token: 0x0400219B RID: 8603
		public int m_EventID;

		// Token: 0x0400219C RID: 8604
		public int m_OptionKey;

		// Token: 0x0400219D RID: 8605
		public float m_EvtTime;
	}
}
