﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000153 RID: 339
	[Serializable]
	public class CharacterParam
	{
		// Token: 0x0600096C RID: 2412 RVA: 0x0003C02C File Offset: 0x0003A42C
		public CharacterParam()
		{
			this.MngID = -1L;
			this.AIID = -1;
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x0600096D RID: 2413 RVA: 0x0003C043 File Offset: 0x0003A443
		// (set) Token: 0x0600096E RID: 2414 RVA: 0x0003C04B File Offset: 0x0003A44B
		public long MngID { get; set; }

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x0600096F RID: 2415 RVA: 0x0003C054 File Offset: 0x0003A454
		// (set) Token: 0x06000970 RID: 2416 RVA: 0x0003C05C File Offset: 0x0003A45C
		public uint UniqueID { get; set; }

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000971 RID: 2417 RVA: 0x0003C065 File Offset: 0x0003A465
		// (set) Token: 0x06000972 RID: 2418 RVA: 0x0003C06D File Offset: 0x0003A46D
		public int CharaID { get; set; }

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000973 RID: 2419 RVA: 0x0003C076 File Offset: 0x0003A476
		// (set) Token: 0x06000974 RID: 2420 RVA: 0x0003C07E File Offset: 0x0003A47E
		public int Lv { get; set; }

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000975 RID: 2421 RVA: 0x0003C087 File Offset: 0x0003A487
		// (set) Token: 0x06000976 RID: 2422 RVA: 0x0003C08F File Offset: 0x0003A48F
		public int MaxLv { get; set; }

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000977 RID: 2423 RVA: 0x0003C098 File Offset: 0x0003A498
		// (set) Token: 0x06000978 RID: 2424 RVA: 0x0003C0A0 File Offset: 0x0003A4A0
		public long Exp { get; set; }

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000979 RID: 2425 RVA: 0x0003C0A9 File Offset: 0x0003A4A9
		// (set) Token: 0x0600097A RID: 2426 RVA: 0x0003C0B1 File Offset: 0x0003A4B1
		public int LimitBreak { get; set; }

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600097B RID: 2427 RVA: 0x0003C0BA File Offset: 0x0003A4BA
		// (set) Token: 0x0600097C RID: 2428 RVA: 0x0003C0C2 File Offset: 0x0003A4C2
		public SkillLearnData UniqueSkillLearnData { get; set; }

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x0600097D RID: 2429 RVA: 0x0003C0CB File Offset: 0x0003A4CB
		// (set) Token: 0x0600097E RID: 2430 RVA: 0x0003C0D3 File Offset: 0x0003A4D3
		public List<SkillLearnData> ClassSkillLearnDatas { get; set; }

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x0600097F RID: 2431 RVA: 0x0003C0DC File Offset: 0x0003A4DC
		// (set) Token: 0x06000980 RID: 2432 RVA: 0x0003C0E4 File Offset: 0x0003A4E4
		public eCharaNamedType NamedType { get; set; }

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000981 RID: 2433 RVA: 0x0003C0ED File Offset: 0x0003A4ED
		// (set) Token: 0x06000982 RID: 2434 RVA: 0x0003C0F5 File Offset: 0x0003A4F5
		public eRare RareType { get; set; }

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000983 RID: 2435 RVA: 0x0003C0FE File Offset: 0x0003A4FE
		// (set) Token: 0x06000984 RID: 2436 RVA: 0x0003C106 File Offset: 0x0003A506
		public eClassType ClassType { get; set; }

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000985 RID: 2437 RVA: 0x0003C10F File Offset: 0x0003A50F
		// (set) Token: 0x06000986 RID: 2438 RVA: 0x0003C117 File Offset: 0x0003A517
		public eElementType ElementType { get; set; }

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000987 RID: 2439 RVA: 0x0003C120 File Offset: 0x0003A520
		// (set) Token: 0x06000988 RID: 2440 RVA: 0x0003C128 File Offset: 0x0003A528
		public int Cost { get; set; }

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000989 RID: 2441 RVA: 0x0003C131 File Offset: 0x0003A531
		// (set) Token: 0x0600098A RID: 2442 RVA: 0x0003C139 File Offset: 0x0003A539
		public int LeaderSkillID { get; set; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x0600098B RID: 2443 RVA: 0x0003C142 File Offset: 0x0003A542
		// (set) Token: 0x0600098C RID: 2444 RVA: 0x0003C14A File Offset: 0x0003A54A
		public int Hp { get; set; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x0600098D RID: 2445 RVA: 0x0003C153 File Offset: 0x0003A553
		// (set) Token: 0x0600098E RID: 2446 RVA: 0x0003C15B File Offset: 0x0003A55B
		public int Atk { get; set; }

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x0003C164 File Offset: 0x0003A564
		// (set) Token: 0x06000990 RID: 2448 RVA: 0x0003C16C File Offset: 0x0003A56C
		public int Mgc { get; set; }

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x06000991 RID: 2449 RVA: 0x0003C175 File Offset: 0x0003A575
		// (set) Token: 0x06000992 RID: 2450 RVA: 0x0003C17D File Offset: 0x0003A57D
		public int Def { get; set; }

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x06000993 RID: 2451 RVA: 0x0003C186 File Offset: 0x0003A586
		// (set) Token: 0x06000994 RID: 2452 RVA: 0x0003C18E File Offset: 0x0003A58E
		public int MDef { get; set; }

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x06000995 RID: 2453 RVA: 0x0003C197 File Offset: 0x0003A597
		// (set) Token: 0x06000996 RID: 2454 RVA: 0x0003C19F File Offset: 0x0003A59F
		public int Spd { get; set; }

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x06000997 RID: 2455 RVA: 0x0003C1A8 File Offset: 0x0003A5A8
		// (set) Token: 0x06000998 RID: 2456 RVA: 0x0003C1B0 File Offset: 0x0003A5B0
		public int Luck { get; set; }

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x06000999 RID: 2457 RVA: 0x0003C1B9 File Offset: 0x0003A5B9
		// (set) Token: 0x0600099A RID: 2458 RVA: 0x0003C1C1 File Offset: 0x0003A5C1
		public bool[] IsRegistAbnormals { get; set; }

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x0600099B RID: 2459 RVA: 0x0003C1CA File Offset: 0x0003A5CA
		// (set) Token: 0x0600099C RID: 2460 RVA: 0x0003C1D2 File Offset: 0x0003A5D2
		public int AIID { get; set; }

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x0600099D RID: 2461 RVA: 0x0003C1DB File Offset: 0x0003A5DB
		// (set) Token: 0x0600099E RID: 2462 RVA: 0x0003C1E3 File Offset: 0x0003A5E3
		public int ChargeCountMax { get; set; }

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x0600099F RID: 2463 RVA: 0x0003C1EC File Offset: 0x0003A5EC
		// (set) Token: 0x060009A0 RID: 2464 RVA: 0x0003C1F4 File Offset: 0x0003A5F4
		public float StunCoef { get; set; }

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060009A1 RID: 2465 RVA: 0x0003C1FD File Offset: 0x0003A5FD
		// (set) Token: 0x060009A2 RID: 2466 RVA: 0x0003C205 File Offset: 0x0003A605
		public float StunerMag { get; set; }

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060009A3 RID: 2467 RVA: 0x0003C20E File Offset: 0x0003A60E
		// (set) Token: 0x060009A4 RID: 2468 RVA: 0x0003C216 File Offset: 0x0003A616
		public float StunerAvoid { get; set; }
	}
}
