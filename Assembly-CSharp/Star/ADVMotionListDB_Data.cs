﻿using System;

namespace Star
{
	// Token: 0x0200001B RID: 27
	[Serializable]
	public struct ADVMotionListDB_Data
	{
		// Token: 0x040000AF RID: 175
		public int m_Action;

		// Token: 0x040000B0 RID: 176
		public float m_X;

		// Token: 0x040000B1 RID: 177
		public float m_Y;

		// Token: 0x040000B2 RID: 178
		public float m_Duration;

		// Token: 0x040000B3 RID: 179
		public int m_EaseType;

		// Token: 0x040000B4 RID: 180
		public sbyte m_WaitEnd;
	}
}
