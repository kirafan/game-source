﻿using System;

namespace Star
{
	// Token: 0x0200067B RID: 1659
	public class TownPinchState
	{
		// Token: 0x040027F1 RID: 10225
		public bool m_Event;

		// Token: 0x040027F2 RID: 10226
		public bool IsPinching;

		// Token: 0x040027F3 RID: 10227
		public float PinchRatio;
	}
}
