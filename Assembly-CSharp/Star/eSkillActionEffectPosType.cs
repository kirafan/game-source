﻿using System;

namespace Star
{
	// Token: 0x02000108 RID: 264
	public enum eSkillActionEffectPosType
	{
		// Token: 0x040006D0 RID: 1744
		Self,
		// Token: 0x040006D1 RID: 1745
		SkillTarget_Tgt,
		// Token: 0x040006D2 RID: 1746
		SkillTarget_My,
		// Token: 0x040006D3 RID: 1747
		PartyCenter_Tgt,
		// Token: 0x040006D4 RID: 1748
		PartyCenter_My
	}
}
