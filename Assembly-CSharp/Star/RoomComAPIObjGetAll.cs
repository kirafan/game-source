﻿using System;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000591 RID: 1425
	public class RoomComAPIObjGetAll : INetComHandle
	{
		// Token: 0x06001BAF RID: 7087 RVA: 0x000929D9 File Offset: 0x00090DD9
		public RoomComAPIObjGetAll()
		{
			this.ApiName = "player/room_object/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}
	}
}
