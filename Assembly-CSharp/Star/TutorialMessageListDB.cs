﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000255 RID: 597
	public class TutorialMessageListDB : ScriptableObject
	{
		// Token: 0x0400138B RID: 5003
		public TutorialMessageListDB_Param[] m_Params;
	}
}
