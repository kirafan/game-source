﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000067 RID: 103
	public static class ADVUtility
	{
		// Token: 0x060002D9 RID: 729 RVA: 0x000100F0 File Offset: 0x0000E4F0
		public static bool IsCrossScenario(int advID)
		{
			int[] namedType = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(advID).m_NamedType;
			int num = 0;
			for (int i = 0; i < namedType.Length; i++)
			{
				if (namedType[i] != -1)
				{
					num++;
				}
			}
			return num > 1;
		}

		// Token: 0x060002DA RID: 730 RVA: 0x00010142 File Offset: 0x0000E542
		public static Vector3 GetStandPosition(eADVStandPosition standPos)
		{
			return new Vector2(ADVDefine.CHARA_POSITION[(int)standPos], 0f);
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0001015A File Offset: 0x0000E55A
		public static iTween.EaseType GetEaseType(eADVCurveType easeType)
		{
			switch (easeType)
			{
			case eADVCurveType.Linear:
				return iTween.EaseType.linear;
			case eADVCurveType.InQuad:
				return iTween.EaseType.easeInQuad;
			case eADVCurveType.OutQuad:
				return iTween.EaseType.easeOutQuad;
			case eADVCurveType.InOutQuad:
				return iTween.EaseType.easeInOutQuad;
			case eADVCurveType.InCubic:
				return iTween.EaseType.easeInCubic;
			case eADVCurveType.OutCubic:
				return iTween.EaseType.easeOutCubic;
			case eADVCurveType.InOutCubic:
				return iTween.EaseType.easeInOutCubic;
			default:
				return iTween.EaseType.linear;
			}
		}

		// Token: 0x060002DC RID: 732 RVA: 0x00010194 File Offset: 0x0000E594
		public static bool IsEmptyADVCharaID(string advCharaID)
		{
			return advCharaID == "-1" || advCharaID == "無し";
		}
	}
}
