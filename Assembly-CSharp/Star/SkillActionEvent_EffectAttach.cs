﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000118 RID: 280
	[Serializable]
	public class SkillActionEvent_EffectAttach : SkillActionEvent_Base
	{
		// Token: 0x04000726 RID: 1830
		public string m_EffectID;

		// Token: 0x04000727 RID: 1831
		public short m_UniqueID;

		// Token: 0x04000728 RID: 1832
		public eSkillActionEffectAttachTo m_AttachTo;

		// Token: 0x04000729 RID: 1833
		public eSkillActionLocatorType m_AttachToLocatorType;

		// Token: 0x0400072A RID: 1834
		public Vector2 m_PosOffset;
	}
}
