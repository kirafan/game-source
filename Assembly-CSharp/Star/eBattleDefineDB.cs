﻿using System;

namespace Star
{
	// Token: 0x02000165 RID: 357
	public enum eBattleDefineDB
	{
		// Token: 0x0400096E RID: 2414
		OrderValueBaseMin,
		// Token: 0x0400096F RID: 2415
		OrderValueBaseMax,
		// Token: 0x04000970 RID: 2416
		OrderValueDecreaseStart,
		// Token: 0x04000971 RID: 2417
		OrderValueDecreaseInterval,
		// Token: 0x04000972 RID: 2418
		OrderValueMin,
		// Token: 0x04000973 RID: 2419
		OrderValueMax,
		// Token: 0x04000974 RID: 2420
		MemberChangeRecast,
		// Token: 0x04000975 RID: 2421
		DamageRandMin,
		// Token: 0x04000976 RID: 2422
		DamageRandMax,
		// Token: 0x04000977 RID: 2423
		RecoverBonusMin,
		// Token: 0x04000978 RID: 2424
		RecoverBonusMax,
		// Token: 0x04000979 RID: 2425
		RecoverBonusInterval,
		// Token: 0x0400097A RID: 2426
		RecoverBonusPlus,
		// Token: 0x0400097B RID: 2427
		RecoverBonusSameElement,
		// Token: 0x0400097C RID: 2428
		GuardCoef,
		// Token: 0x0400097D RID: 2429
		ElementCoefRegist,
		// Token: 0x0400097E RID: 2430
		ElementCoefWeak,
		// Token: 0x0400097F RID: 2431
		ElementCoefRegistMin,
		// Token: 0x04000980 RID: 2432
		ElementCoefRegistMax,
		// Token: 0x04000981 RID: 2433
		ElementCoefDefaultMin,
		// Token: 0x04000982 RID: 2434
		ElementCoefDefaultMax,
		// Token: 0x04000983 RID: 2435
		ElementCoefWeakMin,
		// Token: 0x04000984 RID: 2436
		ElementCoefWeakMax,
		// Token: 0x04000985 RID: 2437
		CriticalProbablityMax,
		// Token: 0x04000986 RID: 2438
		CriticalStrongElementCoef,
		// Token: 0x04000987 RID: 2439
		CriticalCoef,
		// Token: 0x04000988 RID: 2440
		CriticalAdjustmentCoef,
		// Token: 0x04000989 RID: 2441
		TogetherCharge_OnAttack,
		// Token: 0x0400098A RID: 2442
		TogetherCharge_OnAttack_weak,
		// Token: 0x0400098B RID: 2443
		TogetherCharge_OnAttack_regist,
		// Token: 0x0400098C RID: 2444
		TogetherCharge_OnAttackCriticalCoef,
		// Token: 0x0400098D RID: 2445
		TogetherCharge_OnDamage,
		// Token: 0x0400098E RID: 2446
		TogetherCharge_OnDamage_weak,
		// Token: 0x0400098F RID: 2447
		TogetherCharge_OnDamage_regist,
		// Token: 0x04000990 RID: 2448
		TogetherCharge_OnStun,
		// Token: 0x04000991 RID: 2449
		TogetherAttackChainCoef_0,
		// Token: 0x04000992 RID: 2450
		TogetherAttackChainCoef_1,
		// Token: 0x04000993 RID: 2451
		TogetherAttackChainCoef_2,
		// Token: 0x04000994 RID: 2452
		StateAbnormalConfusion_Turn,
		// Token: 0x04000995 RID: 2453
		StateAbnormalParalysis_Turn,
		// Token: 0x04000996 RID: 2454
		StateAbnormalParalysis_CancelProbability,
		// Token: 0x04000997 RID: 2455
		StateAbnormalPoison_Turn,
		// Token: 0x04000998 RID: 2456
		StateAbnormalPoison_DamageRatio,
		// Token: 0x04000999 RID: 2457
		StateAbnormalBearish_Turn,
		// Token: 0x0400099A RID: 2458
		StateAbnormalSleep_Turn,
		// Token: 0x0400099B RID: 2459
		StateAbnormalSleep_LoadFactor,
		// Token: 0x0400099C RID: 2460
		StateAbnormalUnhappy_Turn,
		// Token: 0x0400099D RID: 2461
		StateAbnormalSilence_Turn,
		// Token: 0x0400099E RID: 2462
		StateAbnormalIsolation_Turn,
		// Token: 0x0400099F RID: 2463
		HateValueMin,
		// Token: 0x040009A0 RID: 2464
		HateValueMax,
		// Token: 0x040009A1 RID: 2465
		HateAIConditionCoef,
		// Token: 0x040009A2 RID: 2466
		HateValueJoin1,
		// Token: 0x040009A3 RID: 2467
		DyingHpRatio,
		// Token: 0x040009A4 RID: 2468
		RegistedFriendTurn,
		// Token: 0x040009A5 RID: 2469
		UnregistedFriendTurn,
		// Token: 0x040009A6 RID: 2470
		StunValueMax,
		// Token: 0x040009A7 RID: 2471
		StunAdditiveOrderValueRatioWhenStun,
		// Token: 0x040009A8 RID: 2472
		StunAdditiveOrderValueRatioWhenAttacked,
		// Token: 0x040009A9 RID: 2473
		StunDecreaseValueOnTurnStart,
		// Token: 0x040009AA RID: 2474
		BorderFromSkillLv_0,
		// Token: 0x040009AB RID: 2475
		BorderFromSkillLv_1,
		// Token: 0x040009AC RID: 2476
		BorderFromSkillLv_2,
		// Token: 0x040009AD RID: 2477
		DamageDiv,
		// Token: 0x040009AE RID: 2478
		Max
	}
}
