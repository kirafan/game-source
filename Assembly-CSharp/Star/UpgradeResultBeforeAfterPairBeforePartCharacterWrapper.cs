﻿using System;

namespace Star
{
	// Token: 0x020002AF RID: 687
	public class UpgradeResultBeforeAfterPairBeforePartCharacterWrapper : UpgradeResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CE7 RID: 3303 RVA: 0x000490B0 File Offset: 0x000474B0
		public UpgradeResultBeforeAfterPairBeforePartCharacterWrapper(EditMain.UpgradeResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}

		// Token: 0x06000CE8 RID: 3304 RVA: 0x000490BA File Offset: 0x000474BA
		protected override EditUtility.OutputCharaParam ConvertResultDataToCharaParam(EditMain.UpgradeResultData result)
		{
			return EditUtility.CalcCharaParamIgnoreTownBuff(this.data.Param.CharaID, this.m_Result.m_BeforeLv, this.m_Result.m_BeforeExp, base.GetUserNamedData(), null);
		}
	}
}
