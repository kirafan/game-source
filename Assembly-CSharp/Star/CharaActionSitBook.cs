﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000532 RID: 1330
	public class CharaActionSitBook : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x06001A9A RID: 6810 RVA: 0x0008DB04 File Offset: 0x0008BF04
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
			this.m_PartsAction = new RoomPartsActionPakage();
			base.SetUpPlayScene(roomObjHndl, pOwner, psetup, new RoomActionScriptPlay.ActionSciptCallBack(this.CallbackActionEvent), this.m_PartsAction);
			this.m_Target.SetLinkObject(this.m_Base, true);
			this.m_AnimePlay.SetBlink(true);
			this.m_TargetTrs = base.CalcTargetTrs();
		}

		// Token: 0x06001A9B RID: 6811 RVA: 0x0008DB64 File Offset: 0x0008BF64
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			this.m_PartsAction.PakageUpdate();
			bool flag = base.PlayScene(hbase);
			if (!flag)
			{
				this.m_Target.SetLinkObject(this.m_Base, false);
			}
			return flag;
		}

		// Token: 0x06001A9C RID: 6812 RVA: 0x0008DBA0 File Offset: 0x0008BFA0
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			int eventID = pcmd.m_EventID;
			if (eventID != 0)
			{
				if (eventID != 1)
				{
					if (eventID == 2)
					{
						Vector2 blockSize = RoomUtility.GetBlockSize(1.2f, 0f);
						Vector3 position = this.m_Target.CacheTransform.position;
						if (this.m_MarkPoint)
						{
							position.x += blockSize.x;
							this.m_Base.SetDir(CharacterDefine.eDir.L);
						}
						else
						{
							position.x -= blockSize.x;
							this.m_Base.SetDir(CharacterDefine.eDir.R);
						}
						position.y += blockSize.y;
						this.m_MoveKeyTime = pcmd.m_EvtTime;
						RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, pcmd.m_EvtTime, RoomPartsMove.ePathType.Linear);
						this.m_PartsAction.EntryAction(paction, 0U);
					}
				}
			}
			else
			{
				if (this.m_Base.trs.position.x >= this.m_Target.trs.position.x)
				{
					this.m_MarkPoint = true;
				}
				else
				{
					this.m_MarkPoint = false;
				}
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, this.m_TargetTrs.position, pcmd.m_EvtTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				this.m_MoveKeyTime = pcmd.m_EvtTime;
			}
			return true;
		}

		// Token: 0x06001A9D RID: 6813 RVA: 0x0008DD3E File Offset: 0x0008C13E
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			this.m_Target.SetLinkObject(this.m_Base, false);
			base.CancelPlay();
			return true;
		}

		// Token: 0x06001A9E RID: 6814 RVA: 0x0008DD59 File Offset: 0x0008C159
		public void Release()
		{
			base.ReleasePlay();
			this.m_PartsAction = null;
			this.m_TargetTrs = null;
		}

		// Token: 0x04002107 RID: 8455
		private Transform m_TargetTrs;

		// Token: 0x04002108 RID: 8456
		private bool m_MarkPoint;
	}
}
