﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000177 RID: 375
	public class CharacterLimitBreakListDB : ScriptableObject
	{
		// Token: 0x040009E3 RID: 2531
		public CharacterLimitBreakListDB_Param[] m_Params;
	}
}
