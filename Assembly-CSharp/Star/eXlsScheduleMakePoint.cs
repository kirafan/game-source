﻿using System;

namespace Star
{
	// Token: 0x02000312 RID: 786
	public enum eXlsScheduleMakePoint
	{
		// Token: 0x0400167F RID: 5759
		Sleep,
		// Token: 0x04001680 RID: 5760
		Room,
		// Token: 0x04001681 RID: 5761
		Content,
		// Token: 0x04001682 RID: 5762
		Buf,
		// Token: 0x04001683 RID: 5763
		Menu
	}
}
