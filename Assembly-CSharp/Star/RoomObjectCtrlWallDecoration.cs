﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005D4 RID: 1492
	public class RoomObjectCtrlWallDecoration : RoomObjectCtrlBase
	{
		// Token: 0x06001D38 RID: 7480 RVA: 0x0009D52C File Offset: 0x0009B92C
		public override void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
			this.m_BlockData.SetPos((float)fgridx, (float)fgridy);
			Vector2 blockSize = RoomUtility.GetBlockSize(1f, 1f);
			IVector3 floorSize = this.m_Builder.GetFloorSize(this.m_FloorID);
			int num = fgridx % floorSize.x;
			int num2 = fgridy % floorSize.y;
			bool flag = false;
			if (fgridx >= floorSize.x)
			{
				fgridx = floorSize.x;
				flag = true;
			}
			if (fgridy >= floorSize.y)
			{
				fgridy = floorSize.y;
				flag = false;
			}
			Vector2 vector = RoomUtility.GridToTilePos((float)fgridx, (float)fgridy);
			vector.y -= 0.25f;
			if (flag)
			{
				vector.y += blockSize.y * (float)num;
			}
			else
			{
				vector.y += blockSize.y * (float)num2;
			}
			if (this.m_OwnerTrs != null)
			{
				this.m_OwnerTrs.localPosition = new Vector3(vector.x, vector.y, 0f);
			}
			CharacterDefine.eDir eDir = CharacterDefine.eDir.L;
			if (fgridx >= floorSize.x)
			{
				eDir = CharacterDefine.eDir.R;
			}
			if (eDir != this.m_Dir)
			{
				base.SetDir(eDir);
				if (this.m_HitMarkObj != null)
				{
					if (this.m_Dir == CharacterDefine.eDir.L)
					{
						this.m_HitMarkObj.transform.localScale = new Vector3(-1f, 1f, 1f);
					}
					else
					{
						this.m_HitMarkObj.transform.localScale = new Vector3(1f, 1f, 1f);
					}
				}
			}
		}

		// Token: 0x06001D39 RID: 7481 RVA: 0x0009D6D0 File Offset: 0x0009BAD0
		public override IVector2 GetPosToSize(int fchkposx, int fchkposy)
		{
			IVector3 floorSize = this.m_Builder.GetFloorSize(this.m_FloorID);
			CharacterDefine.eDir eDir = CharacterDefine.eDir.L;
			if (fchkposx >= floorSize.x)
			{
				eDir = CharacterDefine.eDir.R;
			}
			IVector2 result = new IVector2(this.m_DefaultSizeX, this.m_DefaultSizeY);
			if (eDir != CharacterDefine.eDir.L)
			{
				result.x = this.m_DefaultSizeY;
				result.y = this.m_DefaultSizeX;
			}
			return result;
		}

		// Token: 0x06001D3A RID: 7482 RVA: 0x0009D734 File Offset: 0x0009BB34
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			if (fdir == 2 && this.m_Dir == CharacterDefine.eDir.L)
			{
				if (this.m_Builder.GetFloorSize(this.m_FloorID).y <= gridY)
				{
					return base.IsInRange(gridX, this.m_BlockData.PosY, fdir);
				}
			}
			else if (fdir == 1 && this.m_Dir == CharacterDefine.eDir.R && this.m_Builder.GetFloorSize(this.m_FloorID).x <= gridX)
			{
				return base.IsInRange(this.m_BlockData.PosX, gridY, fdir);
			}
			return false;
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x0009D7D4 File Offset: 0x0009BBD4
		public override void CreateHitAreaModel(bool fhitcolor)
		{
			if (this.m_HitMarkObj == null)
			{
				this.m_HitMarkObj = new GameObject("HitMarkObj");
				Vector2 blockSize = RoomUtility.GetBlockSize(1f, 0f);
				this.m_HitMarkMtl = new Material(Shader.Find("Room/GridLine"));
				if (fhitcolor)
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_OK_COLOR);
				}
				else
				{
					this.m_HitMarkMtl.SetColor("_Color", RoomDefine.GRID_HIT_NG_COLOR);
				}
				this.m_HitMarkMtl.renderQueue = 3010;
				Mesh mesh = new Mesh();
				MeshRenderer meshRenderer = this.m_HitMarkObj.AddComponent<MeshRenderer>();
				MeshFilter meshFilter = this.m_HitMarkObj.AddComponent<MeshFilter>();
				mesh.vertices = new Vector3[]
				{
					new Vector3(0f, 0f, 0f),
					new Vector3(-blockSize.x * (float)this.m_DefaultSizeX, blockSize.y * (float)this.m_DefaultSizeX, 0f),
					new Vector3(-blockSize.x * (float)this.m_DefaultSizeX, blockSize.y * (float)this.m_DefaultSizeY * 2f + blockSize.y * (float)this.m_DefaultSizeX, 0f),
					new Vector3(0f, blockSize.y * (float)this.m_DefaultSizeY * 2f, 0f)
				};
				mesh.triangles = new int[]
				{
					0,
					1,
					2,
					0,
					2,
					3
				};
				mesh.RecalculateBounds();
				meshFilter.mesh = mesh;
				meshRenderer.sortingOrder = 9990;
				meshRenderer.material = this.m_HitMarkMtl;
				this.m_Obj.transform.localPosition = new Vector3(0f, 0.1f, 0f);
				if (this.m_Dir == CharacterDefine.eDir.L)
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(-1f, 1f, 1f);
				}
				else
				{
					this.m_HitMarkObj.transform.localScale = new Vector3(1f, 1f, 1f);
				}
				this.m_HitMarkObj.transform.SetParent(base.transform, false);
			}
		}

		// Token: 0x06001D3C RID: 7484 RVA: 0x0009DA34 File Offset: 0x0009BE34
		public override void DestroyHitAreaModel()
		{
			if (this.m_HitMarkObj != null)
			{
				UnityEngine.Object.Destroy(this.m_HitMarkObj);
				this.m_HitMarkObj = null;
				UnityEngine.Object.Destroy(this.m_HitMarkMtl);
				this.m_HitMarkMtl = null;
			}
			this.m_Obj.transform.localPosition = Vector3.zero;
		}
	}
}
