﻿using System;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000595 RID: 1429
	public class RoomComAPIObjSale : INetComHandle
	{
		// Token: 0x06001BB3 RID: 7091 RVA: 0x00092A81 File Offset: 0x00090E81
		public RoomComAPIObjSale()
		{
			this.ApiName = "player/room_object/sale";
			this.Request = true;
			this.ResponseType = typeof(Sale);
		}

		// Token: 0x04002299 RID: 8857
		public string managedRoomObjectId;
	}
}
