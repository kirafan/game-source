﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000093 RID: 147
	[Serializable]
	public class BattleAIPatternData
	{
		// Token: 0x040002AA RID: 682
		public List<BattleAICommandData> m_CommandDatas = new List<BattleAICommandData>();
	}
}
