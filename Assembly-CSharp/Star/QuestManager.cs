﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using QuestChapterRequestTypes;
using QuestChapterResponseTypes;
using Star.Town;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000526 RID: 1318
	public sealed class QuestManager
	{
		// Token: 0x06001A23 RID: 6691 RVA: 0x00087C8D File Offset: 0x0008608D
		public void ForceResetOnReturnTitle()
		{
			this.ResetOnQuestLogSet();
			this.m_SelectQuest.Reset();
		}

		// Token: 0x06001A24 RID: 6692 RVA: 0x00087CA0 File Offset: 0x000860A0
		public void ClearQuestList()
		{
			this.m_QuestList.Clear();
		}

		// Token: 0x06001A25 RID: 6693 RVA: 0x00087CAD File Offset: 0x000860AD
		public void AddQuestListParam(QuestManager.QuestData data)
		{
			this.m_QuestList.Add(data);
		}

		// Token: 0x06001A26 RID: 6694 RVA: 0x00087CBB File Offset: 0x000860BB
		public int GetQuestListNum()
		{
			return this.m_QuestList.Count;
		}

		// Token: 0x06001A27 RID: 6695 RVA: 0x00087CC8 File Offset: 0x000860C8
		public QuestManager.QuestData GetQuestDataAt(int index)
		{
			if (index >= 0 && index < this.m_QuestList.Count)
			{
				return this.m_QuestList[index];
			}
			return null;
		}

		// Token: 0x06001A28 RID: 6696 RVA: 0x00087CF0 File Offset: 0x000860F0
		public QuestManager.QuestData GetQuestData(int questID)
		{
			for (int i = 0; i < this.m_QuestList.Count; i++)
			{
				if (this.m_QuestList[i].m_Param.m_ID == questID)
				{
					return this.m_QuestList[i];
				}
			}
			return null;
		}

		// Token: 0x06001A29 RID: 6697 RVA: 0x00087D44 File Offset: 0x00086144
		public bool IsClearQuest(int questID)
		{
			QuestManager.QuestData questData = this.GetQuestData(questID);
			return questData != null && questData.IsCleared();
		}

		// Token: 0x06001A2A RID: 6698 RVA: 0x00087D68 File Offset: 0x00086168
		public bool[] GetEnemyTypes(int questID)
		{
			QuestManager.QuestData questData = this.GetQuestData(questID);
			if (questData == null)
			{
				return null;
			}
			if (questData.m_Param.m_IsHideElement == 1)
			{
				return null;
			}
			QuestWaveListDB questWaveListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveListDB;
			QuestWaveRandomListDB questWaveRandomListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveRandomListDB;
			QuestEnemyListDB questEnemyListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestEnemyListDB;
			bool[] array = new bool[6];
			for (int i = 0; i < questData.m_Param.m_WaveIDs.Length; i++)
			{
				int waveID = questData.m_Param.m_WaveIDs[i];
				QuestWaveListDB_Param param = questWaveListDB.GetParam(waveID);
				int num = 3;
				for (int j = 0; j < num; j++)
				{
					if (param.m_QuestRandomIDs != null && j < param.m_QuestRandomIDs.Length && param.m_QuestRandomIDs[j] != -1)
					{
						QuestWaveRandomListDB_Param param2 = questWaveRandomListDB.GetParam(param.m_QuestRandomIDs[j]);
						for (int k = 0; k < param2.m_Num; k++)
						{
							int num2 = param2.m_QuestEnemyIDs[k];
							if (num2 != -1)
							{
								QuestEnemyListDB_Param param3 = questEnemyListDB.GetParam(num2);
								if (param3.m_Element != -1)
								{
									array[param3.m_Element] = true;
								}
							}
						}
					}
					else if (param.m_QuestEnemyIDs != null && j < param.m_QuestEnemyIDs.Length && param.m_QuestEnemyIDs[j] != -1)
					{
						int questEnemyID = param.m_QuestEnemyIDs[j];
						QuestEnemyListDB_Param param4 = questEnemyListDB.GetParam(questEnemyID);
						if (param4.m_Element != -1)
						{
							array[param4.m_Element] = true;
						}
					}
				}
			}
			return array;
		}

		// Token: 0x06001A2B RID: 6699 RVA: 0x00087F1C File Offset: 0x0008631C
		public List<int> GetDropItemIDs(int questID)
		{
			QuestManager.QuestData questData = this.GetQuestData(questID);
			if (questData == null)
			{
				return null;
			}
			QuestWaveListDB questWaveListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveListDB;
			QuestWaveRandomListDB questWaveRandomListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveRandomListDB;
			QuestWaveDropItemDB questWaveDropItemDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveDropItemDB;
			List<int> list = new List<int>();
			for (int i = 0; i < questData.m_Param.m_WaveIDs.Length; i++)
			{
				int waveID = questData.m_Param.m_WaveIDs[i];
				QuestWaveListDB_Param param = questWaveListDB.GetParam(waveID);
				int num = 3;
				for (int j = 0; j < num; j++)
				{
					if (param.m_QuestRandomIDs != null && j < param.m_QuestRandomIDs.Length && param.m_QuestRandomIDs[j] != -1)
					{
						QuestWaveRandomListDB_Param param2 = questWaveRandomListDB.GetParam(param.m_QuestRandomIDs[j]);
						for (int k = 0; k < param2.m_Num; k++)
						{
							int num2 = param2.m_DropIDs[k];
							if (num2 != -1)
							{
								QuestWaveDropItemDB_Param param3 = questWaveDropItemDB.GetParam(num2);
								for (int l = 0; l < param3.m_DropItemIDs.Length; l++)
								{
									int num3 = param3.m_DropItemIDs[l];
									if (num3 != -1 && !list.Contains(num3))
									{
										list.Add(num3);
									}
								}
							}
						}
					}
					else if (param.m_QuestEnemyIDs != null && j < param.m_QuestEnemyIDs.Length && param.m_QuestEnemyIDs[j] != -1)
					{
						int num4 = param.m_DropIDs[j];
						if (num4 != -1)
						{
							QuestWaveDropItemDB_Param param4 = questWaveDropItemDB.GetParam(num4);
							for (int m = 0; m < param4.m_DropItemIDs.Length; m++)
							{
								int num5 = param4.m_DropItemIDs[m];
								if (num5 != -1 && !list.Contains(num5))
								{
									list.Add(num5);
								}
							}
						}
					}
				}
			}
			return list;
		}

		// Token: 0x06001A2C RID: 6700 RVA: 0x0008812C File Offset: 0x0008652C
		private void CreateChapterDatas(QuestChapterArray[] srcs)
		{
			this.m_ChapterDatas.Clear();
			foreach (QuestChapterArray questChapterArray in srcs)
			{
				QuestManager.ChapterData chapterData = new QuestManager.ChapterData();
				QuestManager.wwwConvert(questChapterArray, chapterData);
				int num = -1;
				for (int j = 0; j < this.m_ChapterDatas.Count; j++)
				{
					if (this.m_ChapterDatas[j][0].m_ChapterID == questChapterArray.id)
					{
						num = j;
						break;
					}
				}
				if (num == -1)
				{
					num = this.m_ChapterDatas.Count;
					QuestManager.ChapterData[] item = new QuestManager.ChapterData[3];
					this.m_ChapterDatas.Add(item);
				}
				this.m_ChapterDatas[num][questChapterArray.difficulty] = chapterData;
			}
		}

		// Token: 0x06001A2D RID: 6701 RVA: 0x000881EE File Offset: 0x000865EE
		public int GetChapterNum()
		{
			return this.m_ChapterDatas.Count;
		}

		// Token: 0x06001A2E RID: 6702 RVA: 0x000881FB File Offset: 0x000865FB
		public QuestManager.ChapterData[] GetChapterDataAt(int index)
		{
			if (index >= 0 && index < this.m_ChapterDatas.Count)
			{
				return this.m_ChapterDatas[index];
			}
			return null;
		}

		// Token: 0x06001A2F RID: 6703 RVA: 0x00088223 File Offset: 0x00086623
		public QuestManager.ChapterData GetChapterDataAt(int index, QuestDefine.eDifficulty difficulty)
		{
			if (index >= 0 && index < this.m_ChapterDatas.Count)
			{
				return this.m_ChapterDatas[index][(int)difficulty];
			}
			return null;
		}

		// Token: 0x06001A30 RID: 6704 RVA: 0x00088250 File Offset: 0x00086650
		public QuestManager.ChapterData GetChapterData(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			for (int i = 0; i < this.m_ChapterDatas.Count; i++)
			{
				if (this.m_ChapterDatas[i][(int)difficulty] != null && this.m_ChapterDatas[i][(int)difficulty].m_ChapterID == chapterID)
				{
					return this.m_ChapterDatas[i][(int)difficulty];
				}
			}
			return null;
		}

		// Token: 0x06001A31 RID: 6705 RVA: 0x000882B8 File Offset: 0x000866B8
		public QuestManager.ChapterData GetChapterDataFromQuestID(int questID)
		{
			for (int i = 0; i < this.m_ChapterDatas.Count; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (this.m_ChapterDatas[i][j] != null)
					{
						for (int k = 0; k < this.m_ChapterDatas[i][j].m_QuestIDs.Length; k++)
						{
							if (this.m_ChapterDatas[i][j].m_QuestIDs[k] == questID)
							{
								return this.m_ChapterDatas[i][j];
							}
						}
					}
				}
			}
			return null;
		}

		// Token: 0x06001A32 RID: 6706 RVA: 0x0008835C File Offset: 0x0008675C
		public int ChapterIDToIdx(int chapterID)
		{
			for (int i = 0; i < this.m_ChapterDatas.Count; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					if (this.m_ChapterDatas[i][j].m_ChapterID == chapterID)
					{
						return i;
					}
				}
			}
			return -1;
		}

		// Token: 0x06001A33 RID: 6707 RVA: 0x000883B4 File Offset: 0x000867B4
		public int CalcChapterClearRatio(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			QuestManager.ChapterData chapterData = null;
			for (int i = 0; i < this.m_ChapterDatas.Count; i++)
			{
				if (this.m_ChapterDatas[i][(int)difficulty] != null && this.m_ChapterDatas[i][(int)difficulty].m_ChapterID == chapterID)
				{
					chapterData = this.m_ChapterDatas[i][(int)difficulty];
					break;
				}
			}
			if (chapterData == null)
			{
				return 0;
			}
			int num = 0;
			for (int j = 0; j < chapterData.m_QuestIDs.Length; j++)
			{
				QuestManager.QuestData questData = this.GetQuestData(chapterData.m_QuestIDs[j]);
				if (questData != null)
				{
					num = (int)(num + questData.m_ClearRank);
				}
			}
			int num2 = chapterData.m_QuestIDs.Length * 3;
			float num3 = (float)num / (float)num2 * 100f;
			return (int)num3;
		}

		// Token: 0x06001A34 RID: 6708 RVA: 0x00088480 File Offset: 0x00086880
		public int GetLastChapterIndex()
		{
			for (int i = 0; i < this.m_ChapterDatas.Count; i++)
			{
				if (!this.IsClearChapterAt(i))
				{
					return i;
				}
			}
			return this.m_ChapterDatas.Count - 1;
		}

		// Token: 0x06001A35 RID: 6709 RVA: 0x000884C4 File Offset: 0x000868C4
		public int GetLastChapterID()
		{
			int lastChapterIndex = this.GetLastChapterIndex();
			if (lastChapterIndex >= 0 && lastChapterIndex < this.m_ChapterDatas.Count)
			{
				QuestManager.ChapterData chapterData = this.m_ChapterDatas[lastChapterIndex][0];
				if (chapterData != null)
				{
					return chapterData.m_ChapterID;
				}
			}
			return 0;
		}

		// Token: 0x06001A36 RID: 6710 RVA: 0x00088510 File Offset: 0x00086910
		public int GetLastChapterQuestID()
		{
			int lastChapterIndex = this.GetLastChapterIndex();
			if (lastChapterIndex >= 0 && lastChapterIndex < this.m_ChapterDatas.Count)
			{
				QuestManager.ChapterData chapterData = this.m_ChapterDatas[lastChapterIndex][0];
				for (int i = 0; i < chapterData.m_QuestIDs.Length; i++)
				{
					QuestManager.QuestData questData = this.GetQuestData(chapterData.m_QuestIDs[i]);
					if (questData != null && !questData.IsCleared())
					{
						return questData.m_Param.m_ID;
					}
				}
			}
			return -1;
		}

		// Token: 0x06001A37 RID: 6711 RVA: 0x00088594 File Offset: 0x00086994
		public bool IsClearChapterAt(int index)
		{
			if (index >= 0 && index < this.m_ChapterDatas.Count)
			{
				QuestManager.ChapterData[] array = this.m_ChapterDatas[index];
				bool[] array2 = new bool[array.Length];
				for (int i = 0; i < array2.Length; i++)
				{
					array2[i] = true;
					if (array[i] == null)
					{
						array2[i] = false;
					}
					else
					{
						for (int j = 0; j < array[i].m_QuestIDs.Length; j++)
						{
							QuestManager.QuestData questData = this.GetQuestData(array[i].m_QuestIDs[j]);
							if (questData == null)
							{
								array2[i] = false;
								break;
							}
							if (questData.m_ClearRank == QuestDefine.eClearRank.None)
							{
								array2[i] = false;
								break;
							}
						}
					}
				}
				for (int k = 0; k < array2.Length; k++)
				{
					if (array2[k])
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06001A38 RID: 6712 RVA: 0x00088674 File Offset: 0x00086A74
		public bool IsClearChapterDifficulty(int chapterID, QuestDefine.eDifficulty difficulty)
		{
			QuestManager.ChapterData chapterData = this.GetChapterData(chapterID, difficulty);
			if (chapterData != null)
			{
				for (int i = 0; i < chapterData.m_QuestIDs.Length; i++)
				{
					QuestManager.QuestData questData = this.GetQuestData(chapterData.m_QuestIDs[i]);
					if (questData != null && questData.m_ClearRank == QuestDefine.eClearRank.None)
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}

		// Token: 0x06001A39 RID: 6713 RVA: 0x000886D0 File Offset: 0x00086AD0
		private void CreateEventGroupDatas(PlayerEventQuest[] srcs)
		{
			this.m_EventGroupDatas.Clear();
			for (int i = 0; i < srcs.Length; i++)
			{
				EventQuest eventQuest = srcs[i].eventQuest;
				QuestManager.EventData eventData = new QuestManager.EventData();
				QuestManager.wwwConvert(eventQuest, eventData);
				eventData.m_OrderTotal = srcs[i].orderTotal;
				eventData.m_OrderDaily = srcs[i].orderDaily;
				int num = -1;
				for (int j = 0; j < this.m_EventGroupDatas.Count; j++)
				{
					if (this.m_EventGroupDatas[j].m_GroupID == eventQuest.groupId)
					{
						num = j;
						break;
					}
				}
				QuestManager.EventGroupData eventGroupData;
				if (num == -1)
				{
					num = this.m_EventGroupDatas.Count;
					eventGroupData = new QuestManager.EventGroupData();
					eventGroupData.m_EventQuestType = eventQuest.eventType;
					eventGroupData.m_GroupID = eventQuest.groupId;
					eventGroupData.m_GroupName = eventQuest.groupName;
					eventGroupData.m_IsGroupSkip = (eventQuest.groupSkip == 1);
					eventGroupData.m_EventDatas = new List<QuestManager.EventData>();
					this.m_EventGroupDatas.Add(eventGroupData);
				}
				else
				{
					eventGroupData = this.m_EventGroupDatas[num];
				}
				eventGroupData.m_EventDatas.Add(eventData);
			}
		}

		// Token: 0x06001A3A RID: 6714 RVA: 0x00088804 File Offset: 0x00086C04
		public List<int> GetAvailableEventQuestTypes()
		{
			List<int> list = new List<int>();
			Dictionary<int, bool> dictionary = new Dictionary<int, bool>();
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				if (!dictionary.ContainsKey(this.m_EventGroupDatas[i].m_EventQuestType) && this.m_EventGroupDatas[i].m_EventDatas.Count != 0)
				{
					list.Add(this.m_EventGroupDatas[i].m_EventQuestType);
					dictionary.Add(this.m_EventGroupDatas[i].m_EventQuestType, true);
				}
			}
			return list;
		}

		// Token: 0x06001A3B RID: 6715 RVA: 0x000888A0 File Offset: 0x00086CA0
		public int GetEventGroupDataNum()
		{
			return this.m_EventGroupDatas.Count;
		}

		// Token: 0x06001A3C RID: 6716 RVA: 0x000888AD File Offset: 0x00086CAD
		public QuestManager.EventGroupData GetEventGroupDataAt(int index)
		{
			if (index >= 0 && index < this.m_EventGroupDatas.Count)
			{
				return this.m_EventGroupDatas[index];
			}
			return null;
		}

		// Token: 0x06001A3D RID: 6717 RVA: 0x000888D8 File Offset: 0x00086CD8
		public QuestManager.EventGroupData GetEventGroupData(int groupID)
		{
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				if (this.m_EventGroupDatas[i].m_GroupID == groupID)
				{
					return this.m_EventGroupDatas[i];
				}
			}
			return null;
		}

		// Token: 0x06001A3E RID: 6718 RVA: 0x00088928 File Offset: 0x00086D28
		public QuestManager.EventGroupData GetEventGroupDataFromEventID(int eventID)
		{
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				for (int j = 0; j < this.m_EventGroupDatas[i].m_EventDatas.Count; j++)
				{
					if (this.m_EventGroupDatas[i].m_EventDatas[j].m_EventID == eventID)
					{
						return this.m_EventGroupDatas[i];
					}
				}
			}
			return null;
		}

		// Token: 0x06001A3F RID: 6719 RVA: 0x000889A8 File Offset: 0x00086DA8
		public List<QuestManager.EventGroupData> GetEventGroupDatasFromGroupID(int eventQuestType)
		{
			List<QuestManager.EventGroupData> list = new List<QuestManager.EventGroupData>();
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				if (this.m_EventGroupDatas[i].m_EventQuestType == eventQuestType)
				{
					list.Add(this.m_EventGroupDatas[i]);
				}
			}
			return list;
		}

		// Token: 0x06001A40 RID: 6720 RVA: 0x00088A04 File Offset: 0x00086E04
		public int GetEventGroupDataNumFromGroupID(int eventQuestType)
		{
			int num = 0;
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				if (this.m_EventGroupDatas[i].m_EventQuestType == eventQuestType)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06001A41 RID: 6721 RVA: 0x00088A4C File Offset: 0x00086E4C
		public QuestManager.EventData GetEventData(int eventID)
		{
			for (int i = 0; i < this.m_EventGroupDatas.Count; i++)
			{
				for (int j = 0; j < this.m_EventGroupDatas[i].m_EventDatas.Count; j++)
				{
					if (this.m_EventGroupDatas[i].m_EventDatas[j].m_EventID == eventID)
					{
						return this.m_EventGroupDatas[i].m_EventDatas[j];
					}
				}
			}
			return null;
		}

		// Token: 0x06001A42 RID: 6722 RVA: 0x00088AD8 File Offset: 0x00086ED8
		public bool IsExistSaveResponse()
		{
			if (string.IsNullOrEmpty(this.SaveResponse))
			{
				Debug.Log("IsExistSaveResponse() false. >>> SaveResponse is null.");
				return false;
			}
			if (this.WorkRecvID == -1L)
			{
				Debug.Log("IsExistSaveResponse() false. >>> WorkRecvID is disable.");
				return false;
			}
			if (this.WorkQuestID == -1)
			{
				Debug.Log("IsExistSaveResponse() false. >>> WorkQuestID is disable.");
				return false;
			}
			BattleSystemData battleSystemData = BattleUtility.SaveResponseToBSD(this.SaveResponse);
			if (battleSystemData == null)
			{
				Debug.Log("IsExistSaveResponse() false. >>> bsd is null.");
				return false;
			}
			if (!battleSystemData.VersionCheck("1.0.3"))
			{
				Debug.Log("IsExistSaveResponse() false. >>> version.");
				return false;
			}
			if (battleSystemData.SubVer != "4")
			{
				Debug.Log("IsExistSaveResponse() false. >>> subversion.");
				return false;
			}
			return true;
		}

		// Token: 0x06001A43 RID: 6723 RVA: 0x00088B90 File Offset: 0x00086F90
		public BattleResult CreateResult(int questID, bool isAdvOnly, long partyMngID)
		{
			this.m_Result = new BattleResult();
			if (!isAdvOnly)
			{
				this.WorkTBuff = TownBuff.CalcTownBuff();
				QuestManager.QuestData questData = this.GetQuestData(questID);
				if (questData != null)
				{
					BattleUtility.CreateOccurEnemy(ref questData.m_Param, out this.WorkEnemies);
					List<int> charaIDs = this.CalcPlayerCharaIDs(ref questData.m_Param, partyMngID);
					float probAdd = BattleUtility.CalcDropItemProbAdd(charaIDs, ref questData.m_Param);
					BattleUtility.CreateScheduleDropItems(this.WorkEnemies, probAdd, out this.WorkScheduleItems);
				}
			}
			else
			{
				this.WorkTBuff = null;
				this.WorkEnemies = null;
				this.WorkScheduleItems = null;
			}
			return this.m_Result;
		}

		// Token: 0x06001A44 RID: 6724 RVA: 0x00088C28 File Offset: 0x00087028
		private List<int> CalcPlayerCharaIDs(ref QuestListDB_Param questParam, long partyMngID)
		{
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData;
			if (partyMngID == -1L)
			{
				userBattlePartyData = userDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex];
			}
			else
			{
				userBattlePartyData = userDataMng.GetUserBattlePartyData(partyMngID);
			}
			List<int> list = new List<int>();
			for (int i = 0; i < 5; i++)
			{
				long memberAt = userBattlePartyData.GetMemberAt(i);
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(memberAt);
				if (userCharaData != null)
				{
					int charaID = userCharaData.Param.CharaID;
					if (charaID != -1)
					{
						list.Add(charaID);
					}
				}
			}
			if (questParam.m_CpuFriendCharaID != -1)
			{
				list.Add(questParam.m_CpuFriendCharaID);
			}
			else if (globalParam.QuestFriendData != null && globalParam.QuestUserSupportData != null)
			{
				list.Add(globalParam.QuestUserSupportData.CharaID);
			}
			return list;
		}

		// Token: 0x06001A45 RID: 6725 RVA: 0x00088D12 File Offset: 0x00087112
		public BattleResult GetResult()
		{
			return this.m_Result;
		}

		// Token: 0x06001A46 RID: 6726 RVA: 0x00088D1A File Offset: 0x0008711A
		public QuestManager.SelectQuest GetSelectQuest()
		{
			return this.m_SelectQuest;
		}

		// Token: 0x06001A47 RID: 6727 RVA: 0x00088D22 File Offset: 0x00087122
		public QuestManager.SelectQuest NextSelectQuest(out bool out_isParentNext)
		{
			if (this.m_SelectQuest.m_EventType == -1)
			{
				return this.CalcNextQuestChapter(this.m_SelectQuest.m_QuestID, out out_isParentNext);
			}
			return this.CalcNextQuestEvent(this.m_SelectQuest.m_EventID, out out_isParentNext);
		}

		// Token: 0x06001A48 RID: 6728 RVA: 0x00088D5C File Offset: 0x0008715C
		private QuestManager.SelectQuest CalcNextQuestChapter(int questID, out bool out_isParentNext)
		{
			out_isParentNext = false;
			int chapterNum = this.GetChapterNum();
			for (int i = 0; i < chapterNum; i++)
			{
				for (int j = 0; j < 3; j++)
				{
					QuestDefine.eDifficulty difficulty = (QuestDefine.eDifficulty)j;
					QuestManager.ChapterData chapterDataAt = this.GetChapterDataAt(i, difficulty);
					if (chapterDataAt != null)
					{
						int num = chapterDataAt.m_QuestIDs.Length;
						int k = 0;
						while (k < num)
						{
							if (chapterDataAt.m_QuestIDs[k] != questID)
							{
								k++;
							}
							else
							{
								if (k + 1 < num)
								{
									this.m_SelectQuest.SetupChapter(chapterDataAt.m_ChapterID, difficulty, chapterDataAt.m_QuestIDs[k + 1]);
									return this.m_SelectQuest;
								}
								if (chapterDataAt.m_Difficulty == QuestDefine.eDifficulty.Normal && i + 1 < chapterNum)
								{
									QuestManager.ChapterData chapterDataAt2 = this.GetChapterDataAt(i + 1, difficulty);
									if (chapterDataAt2 != null)
									{
										this.m_SelectQuest.SetupChapter(chapterDataAt2.m_ChapterID, difficulty, -1);
										out_isParentNext = true;
									}
									return this.m_SelectQuest;
								}
								this.m_SelectQuest.SetupChapter(chapterDataAt.m_ChapterID, difficulty, chapterDataAt.m_QuestIDs[k]);
								return this.m_SelectQuest;
							}
						}
					}
				}
			}
			return this.m_SelectQuest;
		}

		// Token: 0x06001A49 RID: 6729 RVA: 0x00088E88 File Offset: 0x00087288
		private QuestManager.SelectQuest CalcNextQuestEvent(int eventID, out bool out_isParentNext)
		{
			out_isParentNext = false;
			int eventGroupDataNum = this.GetEventGroupDataNum();
			int num = -1;
			for (int i = 0; i < eventGroupDataNum; i++)
			{
				QuestManager.EventGroupData eventGroupDataAt = this.GetEventGroupDataAt(i);
				for (int j = 0; j < eventGroupDataAt.m_EventDatas.Count; j++)
				{
					if (eventGroupDataAt.m_EventDatas[j].m_EventID == eventID)
					{
						num = eventGroupDataAt.m_GroupID;
						break;
					}
				}
				if (num != -1)
				{
					break;
				}
			}
			List<QuestManager.EventData> list = new List<QuestManager.EventData>();
			List<QuestManager.EventGroupData> list2 = new List<QuestManager.EventGroupData>();
			for (int k = 0; k < eventGroupDataNum; k++)
			{
				QuestManager.EventGroupData eventGroupDataAt2 = this.GetEventGroupDataAt(k);
				for (int l = 0; l < eventGroupDataAt2.m_EventDatas.Count; l++)
				{
					if (eventGroupDataAt2.m_EventDatas[l].m_ExEventID == eventID)
					{
						if (eventGroupDataAt2.m_GroupID == num)
						{
							list.Add(eventGroupDataAt2.m_EventDatas[l]);
						}
						else
						{
							list2.Add(eventGroupDataAt2);
						}
					}
				}
			}
			int num2 = -1;
			int questID = -1;
			for (int m = 0; m < list.Count; m++)
			{
				if (num2 == -1 || num2 > list[m].m_EventID)
				{
					num2 = list[m].m_EventID;
					questID = list[m].m_QuestID;
				}
			}
			if (num2 != -1)
			{
				this.m_SelectQuest.SetupEvent(num, num2, questID, this.GetEventGroupData(num).m_EventQuestType);
				return this.m_SelectQuest;
			}
			int num3 = -1;
			for (int n = 0; n < list2.Count; n++)
			{
				if (num3 == -1 || num3 > list2[n].m_GroupID)
				{
					num3 = list2[n].m_GroupID;
				}
			}
			if (num3 != -1)
			{
				this.m_SelectQuest.SetupEvent(num3, -1, -1, this.GetEventGroupData(num3).m_EventQuestType);
				return this.m_SelectQuest;
			}
			return this.m_SelectQuest;
		}

		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06001A4A RID: 6730 RVA: 0x000890B0 File Offset: 0x000874B0
		// (remove) Token: 0x06001A4B RID: 6731 RVA: 0x000890E8 File Offset: 0x000874E8
		private event Action m_OnResponse_QuestGetAll;

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x06001A4C RID: 6732 RVA: 0x00089120 File Offset: 0x00087520
		// (remove) Token: 0x06001A4D RID: 6733 RVA: 0x00089158 File Offset: 0x00087558
		private event Action m_OnResponse_QuestChapterGetAll;

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x06001A4E RID: 6734 RVA: 0x00089190 File Offset: 0x00087590
		// (remove) Token: 0x06001A4F RID: 6735 RVA: 0x000891C8 File Offset: 0x000875C8
		private event Action<QuestDefine.eReturnLogAdd, string> m_OnResponse_QuestLogAdd;

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x06001A50 RID: 6736 RVA: 0x00089200 File Offset: 0x00087600
		// (remove) Token: 0x06001A51 RID: 6737 RVA: 0x00089238 File Offset: 0x00087638
		private event Action<MeigewwwParam, PlayerResponseTypes.Questlogset> m_OnResponse_QuestLogSet;

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x06001A52 RID: 6738 RVA: 0x00089270 File Offset: 0x00087670
		// (remove) Token: 0x06001A53 RID: 6739 RVA: 0x000892A8 File Offset: 0x000876A8
		private event Action<MeigewwwParam, PlayerResponseTypes.Questlogretry> m_OnResponse_QuestRetry;

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x06001A54 RID: 6740 RVA: 0x000892E0 File Offset: 0x000876E0
		// (remove) Token: 0x06001A55 RID: 6741 RVA: 0x00089318 File Offset: 0x00087718
		private event Action m_OnResponse_DeleteBSD;

		// Token: 0x06001A56 RID: 6742 RVA: 0x00089350 File Offset: 0x00087750
		public bool QuestGetAllApiTypeIsDebugFullOpen()
		{
			return false;
		}

		// Token: 0x06001A57 RID: 6743 RVA: 0x00089360 File Offset: 0x00087760
		public bool Request_QuestGetAll(Action onResponse, bool isOpenConnectingIcon = true)
		{
			this.m_OnResponse_QuestGetAll = onResponse;
			PlayerRequestTypes.Questgetall param = new PlayerRequestTypes.Questgetall();
			MeigewwwParam wwwParam = PlayerRequest.Questgetall(param, new MeigewwwParam.Callback(this.OnResponse_QuestGetAll));
			NetworkQueueManager.Request(wwwParam);
			if (isOpenConnectingIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001A58 RID: 6744 RVA: 0x000893AC File Offset: 0x000877AC
		private void OnResponse_QuestGetAll(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questgetall questgetall = PlayerResponse.Questgetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (questgetall == null)
			{
				return;
			}
			PlayerQuest[] quests = questgetall.quests;
			PlayerEventQuest[] eventQuests = questgetall.eventQuests;
			ResultCode result = questgetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.m_QuestList.Clear();
				if (quests != null)
				{
					for (int i = 0; i < quests.Length; i++)
					{
						if (quests[i].quest != null)
						{
							QuestListDB_Param param = default(QuestListDB_Param);
							QuestManager.wwwConvert(quests[i].quest, ref param);
							QuestManager.QuestData item = new QuestManager.QuestData(param, (QuestDefine.eClearRank)quests[i].clearRank, -1);
							this.m_QuestList.Add(item);
						}
					}
				}
				if (eventQuests != null)
				{
					for (int j = 0; j < eventQuests.Length; j++)
					{
						if (eventQuests[j].eventQuest.quest != null)
						{
							QuestListDB_Param param2 = default(QuestListDB_Param);
							QuestManager.wwwConvert(eventQuests[j].eventQuest.quest, ref param2);
							QuestManager.QuestData item2 = new QuestManager.QuestData(param2, (QuestDefine.eClearRank)eventQuests[j].clearRank, eventQuests[j].eventQuest.id);
							this.m_QuestList.Add(item2);
						}
					}
					this.CreateEventGroupDatas(eventQuests);
				}
				else
				{
					this.m_EventGroupDatas.Clear();
				}
				this.m_OnResponse_QuestGetAll.Call();
			}
		}

		// Token: 0x06001A59 RID: 6745 RVA: 0x00089510 File Offset: 0x00087910
		public bool Request_QuestChapterGetAll(Action onResponse, bool isOpenConnectingIcon = true)
		{
			this.m_OnResponse_QuestChapterGetAll = onResponse;
			QuestChapterRequestTypes.GetAll param = new QuestChapterRequestTypes.GetAll();
			MeigewwwParam all = QuestChapterRequest.GetAll(param, new MeigewwwParam.Callback(this.OnResponse_QuestChapterGetAll));
			NetworkQueueManager.Request(all);
			if (isOpenConnectingIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001A5A RID: 6746 RVA: 0x0008955C File Offset: 0x0008795C
		private void OnResponse_QuestChapterGetAll(MeigewwwParam wwwParam)
		{
			QuestChapterResponseTypes.GetAll all = QuestChapterResponse.GetAll(wwwParam, ResponseCommon.DialogType.None, null);
			if (all == null)
			{
				return;
			}
			ResultCode result = all.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.CreateChapterDatas(all.chapters);
				if (this.m_SelectQuest.IsInvalid())
				{
					this.m_SelectQuest.Reset();
					this.m_SelectQuest.SetupChapter(this.GetLastChapterID(), QuestDefine.eDifficulty.Normal, this.GetLastChapterQuestID());
				}
				this.m_OnResponse_QuestChapterGetAll.Call();
			}
		}

		// Token: 0x06001A5B RID: 6747 RVA: 0x000895EC File Offset: 0x000879EC
		public bool Request_QuestLogAdd(bool isEventQuest, int questID, int eventID, long battlePartyMngId, long friendCharaMngID, Action<QuestDefine.eReturnLogAdd, string> onResponse)
		{
			this.m_OnResponse_QuestLogAdd = onResponse;
			this.WorkQuestID = questID;
			this.SaveResponse = null;
			this.CreateResult(questID, false, battlePartyMngId);
			PlayerRequestTypes.Questlogadd questlogadd = new PlayerRequestTypes.Questlogadd();
			questlogadd.type = ((!isEventQuest) ? 1 : 2);
			questlogadd.questId = ((!isEventQuest) ? this.WorkQuestID : eventID);
			questlogadd.managedBattlePartyId = battlePartyMngId;
			questlogadd.supportCharacterId = friendCharaMngID;
			BattleSystemData battleSystemData = new BattleSystemData();
			battleSystemData.Ver = "1.0.3";
			battleSystemData.QuestID = this.WorkQuestID;
			battleSystemData.TBuff = this.WorkTBuff;
			battleSystemData.Enemies = this.WorkEnemies;
			battleSystemData.ScheduleItems = this.WorkScheduleItems;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (globalParam.QuestFriendData != null && globalParam.QuestUserSupportData != null)
			{
				battleSystemData.FrdType = ((globalParam.QuestFriendData.m_Type != FriendDefine.eType.Friend) ? 1 : 0);
				battleSystemData.FrdCharaID = globalParam.QuestUserSupportData.CharaID;
				battleSystemData.FrdCharaLv = globalParam.QuestUserSupportData.Lv;
				battleSystemData.FrdLB = globalParam.QuestUserSupportData.LimitBreak;
				battleSystemData.FrdUSLv = globalParam.QuestUserSupportData.UniqueSkillLv;
				battleSystemData.FrdCSLvs = globalParam.QuestUserSupportData.ClassSkillLvs;
				battleSystemData.FrdWpID = globalParam.QuestUserSupportData.WeaponID;
				battleSystemData.FrdWpLv = globalParam.QuestUserSupportData.WeaponLv;
				battleSystemData.FrdWpSLv = globalParam.QuestUserSupportData.WeaponSkillLv;
				battleSystemData.Frdship = globalParam.QuestUserSupportData.FriendShip;
			}
			battleSystemData.PtyMngID = battlePartyMngId;
			this.SaveResponse = BattleUtility.BSDToSaveResponse(battleSystemData);
			questlogadd.questData = this.SaveResponse;
			Debug.LogFormat("Request_QuestLogAdd() bsd:{0}", new object[]
			{
				battleSystemData
			});
			Debug.LogFormat("Request_QuestLogAdd() SaveResponse:{0}", new object[]
			{
				this.SaveResponse
			});
			MeigewwwParam wwwParam = PlayerRequest.Questlogadd(questlogadd, new MeigewwwParam.Callback(this.OnResponse_QuestLogAdd));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A5C RID: 6748 RVA: 0x000897EC File Offset: 0x00087BEC
		private void OnResponse_QuestLogAdd(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogadd questlogadd = PlayerResponse.Questlogadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogadd == null)
			{
				return;
			}
			ResultCode result = questlogadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.ITEM_IS_SHORT)
				{
					if (result != ResultCode.STAMINA_IS_SHORT)
					{
						if (result != ResultCode.QUEST_OUT_OF_PERIOD)
						{
							if (result != ResultCode.QUEST_ORDER_LIMIT)
							{
								APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
							}
							else
							{
								this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.QuestOrderLimit, result.ToMessageString());
							}
						}
						else
						{
							this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.QuestOutOfPerod, result.ToMessageString());
						}
					}
					else
					{
						this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.StaminaIsShort, result.ToMessageString());
					}
				}
				else
				{
					this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.ItemIsShort, result.ToMessageString());
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(questlogadd.player, userDataMng.UserData);
				APIUtility.wwwToUserData(questlogadd.itemSummary, userDataMng.UserItemDatas);
				this.WorkRecvID = questlogadd.orderReceiveId;
				this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.Success, null);
			}
		}

		// Token: 0x06001A5D RID: 6749 RVA: 0x00089900 File Offset: 0x00087D00
		public bool Request_QuestLogAddAdvOnly(bool isEventQuest, int questID, int eventID, Action<QuestDefine.eReturnLogAdd, string> onResponse)
		{
			this.m_OnResponse_QuestLogAdd = onResponse;
			this.WorkQuestID = questID;
			this.SaveResponse = null;
			this.CreateResult(questID, true, -1L);
			MeigewwwParam wwwParam = PlayerRequest.Questlogadd(new PlayerRequestTypes.Questlogadd
			{
				type = ((!isEventQuest) ? 3 : 4),
				questId = ((!isEventQuest) ? questID : eventID),
				managedBattlePartyId = -1L,
				supportCharacterId = -1L
			}, new MeigewwwParam.Callback(this.OnResponse_QuestLogAddAdvOnly));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A5E RID: 6750 RVA: 0x00089994 File Offset: 0x00087D94
		private void OnResponse_QuestLogAddAdvOnly(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogadd questlogadd = PlayerResponse.Questlogadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogadd == null)
			{
				return;
			}
			ResultCode result = questlogadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(questlogadd.player, userDataMng.UserData);
				APIUtility.wwwToUserData(questlogadd.itemSummary, userDataMng.UserItemDatas);
				this.WorkRecvID = questlogadd.orderReceiveId;
				this.m_OnResponse_QuestLogAdd.Call(QuestDefine.eReturnLogAdd.Success, null);
			}
		}

		// Token: 0x06001A5F RID: 6751 RVA: 0x00089A20 File Offset: 0x00087E20
		public bool Request_QuestSuccess(BattlePartyData battlePartyData, Action<MeigewwwParam, PlayerResponseTypes.Questlogset> onResponse)
		{
			this.m_OnResponse_QuestLogSet = onResponse;
			this.m_WorkBattlePartyMngID = battlePartyData.PartyMngID;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			PlayerRequestTypes.Questlogset questlogset = new PlayerRequestTypes.Questlogset();
			questlogset.orderReceiveId = this.WorkRecvID;
			questlogset.state = 2;
			questlogset.clearRank = (int)this.m_Result.m_ClearRank;
			this.m_sb.Length = 0;
			List<CharacterHandler> availableMembers = battlePartyData.GetAvailableMembers();
			for (int i = 0; i < availableMembers.Count; i++)
			{
				CharacterHandler characterHandler = availableMembers[i];
				if (!(characterHandler == null))
				{
					if (!characterHandler.IsFriendChara)
					{
						long[] array = new long[3];
						int num = 0;
						BattleCommandData battleCommandData = availableMembers[i].CharaBattle.GetUniqueSkillCommandData();
						if (battleCommandData != null)
						{
							array[num] = (long)battleCommandData.UseNum;
						}
						num = 1;
						for (int j = 0; j < availableMembers[i].CharaBattle.CommandNum; j++)
						{
							battleCommandData = availableMembers[i].CharaBattle.GetCommandDataAt(j);
							if (battleCommandData != null && battleCommandData.CommandType == BattleCommandData.eCommandType.Skill)
							{
								array[num] = (long)battleCommandData.UseNum;
								num++;
							}
							if (num >= array.Length)
							{
								break;
							}
						}
						if (this.m_sb.Length > 0)
						{
							this.m_sb.Append(",");
						}
						this.m_sb.Append(availableMembers[i].CharaParam.MngID);
						for (int k = 0; k < array.Length; k++)
						{
							this.m_sb.Append(":");
							this.m_sb.Append(array[k].ToString());
						}
					}
				}
			}
			questlogset.skillExps = this.m_sb.ToString();
			this.m_sb.Length = 0;
			for (int l = 0; l < availableMembers.Count; l++)
			{
				CharacterHandler characterHandler2 = availableMembers[l];
				if (!(characterHandler2 == null))
				{
					if (!characterHandler2.IsFriendChara)
					{
						if (characterHandler2.WeaponParam != null)
						{
							UserWeaponData userWpnData = userDataMng.GetUserWpnData(characterHandler2.WeaponParam.MngID);
							if (userWpnData != null)
							{
								int value = 0;
								for (int m = 0; m < availableMembers[l].CharaBattle.CommandNum; m++)
								{
									BattleCommandData commandDataAt = availableMembers[l].CharaBattle.GetCommandDataAt(m);
									if (commandDataAt != null && commandDataAt.CommandType == BattleCommandData.eCommandType.WeaponSkill)
									{
										value = commandDataAt.UseNum;
										break;
									}
								}
								if (this.m_sb.Length > 0)
								{
									this.m_sb.Append(",");
								}
								this.m_sb.Append(userWpnData.MngID);
								this.m_sb.Append(":");
								this.m_sb.Append(value);
							}
						}
					}
				}
			}
			questlogset.weaponSkillExps = this.m_sb.ToString();
			this.m_sb.Length = 0;
			for (int n = 0; n < this.m_Result.m_DropItems.Count; n++)
			{
				for (int num2 = 0; num2 < this.m_Result.m_DropItems[n].GetDropDataNum(); num2++)
				{
					BattleDropItem.DropData dropDataAt = this.m_Result.m_DropItems[n].GetDropDataAt(num2);
					if (this.m_sb.Length > 0)
					{
						this.m_sb.Append(",");
					}
					this.m_sb.Append(dropDataAt.ID);
					this.m_sb.Append(":");
					this.m_sb.Append(dropDataAt.Num);
				}
			}
			questlogset.dropItems = this.m_sb.ToString();
			this.m_sb.Length = 0;
			foreach (int num3 in this.m_Result.m_KilledEnemies.Keys)
			{
				if (this.m_sb.Length > 0)
				{
					this.m_sb.Append(",");
				}
				this.m_sb.Append(num3);
				this.m_sb.Append(":");
				this.m_sb.Append(this.m_Result.m_KilledEnemies[num3]);
			}
			questlogset.killedEnemies = this.m_sb.ToString();
			questlogset.friendUseNum = this.m_Result.m_InterruptFriendUseNum;
			questlogset.masterSkillUseNum = this.m_Result.m_MasterSkillUseNum;
			questlogset.uniqueSkillUseNum = 0;
			for (int num4 = 0; num4 < availableMembers.Count; num4++)
			{
				CharacterHandler characterHandler3 = availableMembers[num4];
				if (!(characterHandler3 == null))
				{
					BattleCommandData uniqueSkillCommandData = characterHandler3.CharaBattle.GetUniqueSkillCommandData();
					if (uniqueSkillCommandData != null && uniqueSkillCommandData.IsAvailable())
					{
						questlogset.uniqueSkillUseNum += uniqueSkillCommandData.UseNum;
					}
				}
			}
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.ADVPrologueIntro2Played_NextIsQuestLogSet)
			{
				questlogset.stepCode = 9;
			}
			MeigewwwParam wwwParam = PlayerRequest.Questlogset(questlogset, new MeigewwwParam.Callback(this.OnResponse_QuestSuccess));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A60 RID: 6752 RVA: 0x00089FF4 File Offset: 0x000883F4
		private void OnResponse_QuestSuccess(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogset questlogset = PlayerResponse.Questlogset(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogset == null)
			{
				if (this.m_OnResponse_QuestLogSet != null)
				{
					this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
				}
				return;
			}
			ResultCode result = questlogset.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				UserBattlePartyData userBattlePartyData = userDataMng.GetUserBattlePartyData(this.m_WorkBattlePartyMngID);
				UserMasterOrbData userMasterOrbData = null;
				if (questlogset.managedMasterOrb != null)
				{
					userMasterOrbData = userDataMng.GetUserMasterOrbData(questlogset.managedMasterOrb.managedMasterOrbId);
				}
				QuestManager.QuestData questData = this.GetQuestData(this.WorkQuestID);
				this.m_Result.m_RewardUserExp = (long)questData.m_Param.m_RewardUserExp;
				this.m_Result.m_RewardCharaExp = (long)questData.m_Param.m_RewardCharaExp;
				this.m_Result.m_RewardFriendshipExp = (long)questData.m_Param.m_RewardFriendshipExp;
				this.m_Result.m_RewardMasterOrbExp = (long)questData.m_Param.m_RewardMasterOrbExp;
				this.m_Result.m_RewardGold = (long)questData.m_Param.m_RewardMoney;
				if (userMasterOrbData != null)
				{
					this.m_Result.m_BeforeMasterOrbLv = userMasterOrbData.Lv;
					this.m_Result.m_BeforeMasterOrbExp = userMasterOrbData.Exp;
				}
				this.m_Result.m_BeforeUserLv = userDataMng.UserData.Lv;
				this.m_Result.m_BeforeUserExp = userDataMng.UserData.Exp;
				this.m_Result.m_BeforeGold = userDataMng.UserData.Gold;
				this.m_Result.m_CharaDatas = new BattleResult.CharaData[5];
				if (userBattlePartyData != null)
				{
					for (int i = 0; i < 5; i++)
					{
						long memberAt = userBattlePartyData.GetMemberAt(i);
						if (memberAt != -1L)
						{
							UserCharacterData userCharaData = userDataMng.GetUserCharaData(memberAt);
							if (userCharaData != null)
							{
								UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharaData.Param.NamedType);
								if (userNamedData != null)
								{
									this.m_Result.m_CharaDatas[i].m_IsAvailable = true;
									this.m_Result.m_CharaDatas[i].m_MngID = memberAt;
									this.m_Result.m_CharaDatas[i].m_BeforeLv = userCharaData.Param.Lv;
									this.m_Result.m_CharaDatas[i].m_BeforeExp = userCharaData.Param.Exp;
									this.m_Result.m_CharaDatas[i].m_BeforeFriendship = userNamedData.FriendShip;
									this.m_Result.m_CharaDatas[i].m_BeforeFriendshipExp = userNamedData.FriendShipExp;
								}
							}
						}
					}
				}
				APIUtility.wwwToUserData(questlogset.player, userDataMng.UserData);
				if (questlogset.managedCharacters != null)
				{
					for (int j = 0; j < questlogset.managedCharacters.Length; j++)
					{
						UserCharacterData userCharaData2 = userDataMng.GetUserCharaData(questlogset.managedCharacters[j].managedCharacterId);
						if (userCharaData2 != null)
						{
							APIUtility.wwwToUserData(questlogset.managedCharacters[j], userCharaData2);
						}
					}
				}
				if (questlogset.managedNamedTypes != null)
				{
					for (int k = 0; k < questlogset.managedNamedTypes.Length; k++)
					{
						UserNamedData userNamedData2 = userDataMng.GetUserNamedData((eCharaNamedType)questlogset.managedNamedTypes[k].namedType);
						if (userNamedData2 != null)
						{
							APIUtility.wwwToUserData(questlogset.managedNamedTypes[k], userNamedData2);
						}
					}
				}
				if (questlogset.managedMasterOrb != null && userMasterOrbData != null)
				{
					APIUtility.wwwToUserData(questlogset.managedMasterOrb, userMasterOrbData);
				}
				APIUtility.wwwToUserData(questlogset.itemSummary, userDataMng.UserItemDatas);
				this.m_Result.m_RewardFirst = questlogset.rewardFirst;
				this.m_Result.m_RewardGroup = questlogset.rewardGroup;
				this.m_Result.m_RewardComp = questlogset.rewardComp;
				this.m_Result.m_AfterUserLv = userDataMng.UserData.Lv;
				this.m_Result.m_AfterUserExp = userDataMng.UserData.Exp;
				this.m_Result.m_AfterGold = this.m_Result.m_BeforeGold + (long)questData.m_Param.m_RewardMoney;
				this.m_Result.m_UserNextExps = dbMng.MasterRankDB.GetNextMaxExps(this.m_Result.m_BeforeUserLv, this.m_Result.m_AfterUserLv);
				if (userMasterOrbData != null)
				{
					this.m_Result.m_AfterMasterOrbLv = userMasterOrbData.Lv;
					this.m_Result.m_AfterMasterOrbExp = userMasterOrbData.Exp;
					MasterOrbListDB_Param param = dbMng.MasterOrbListDB.GetParam(userMasterOrbData.OrbID);
					this.m_Result.m_MasterOrbNextExps = dbMng.MasterOrbExpDB.GetNextMaxExps(this.m_Result.m_BeforeMasterOrbLv, this.m_Result.m_AfterMasterOrbLv, param.m_ExpTableID);
				}
				for (int l = 0; l < this.m_Result.m_CharaDatas.Length; l++)
				{
					if (this.m_Result.m_CharaDatas[l].m_IsAvailable)
					{
						UserCharacterData userCharaData3 = userDataMng.GetUserCharaData(this.m_Result.m_CharaDatas[l].m_MngID);
						if (userCharaData3 != null)
						{
							UserNamedData userNamedData3 = userDataMng.GetUserNamedData(userCharaData3.Param.NamedType);
							if (userNamedData3 != null)
							{
								NamedListDB_Param param2 = dbMng.NamedListDB.GetParam(userNamedData3.NamedType);
								this.m_Result.m_CharaDatas[l].m_AfterLv = userCharaData3.Param.Lv;
								this.m_Result.m_CharaDatas[l].m_AfterExp = userCharaData3.Param.Exp;
								this.m_Result.m_CharaDatas[l].m_AddExp = this.m_Result.m_CharaDatas[l].m_AfterExp - this.m_Result.m_CharaDatas[l].m_BeforeExp;
								this.m_Result.m_CharaDatas[l].m_NextExps = dbMng.CharaExpDB.GetNextMaxExps(this.m_Result.m_CharaDatas[l].m_BeforeLv, this.m_Result.m_CharaDatas[l].m_AfterLv);
								this.m_Result.m_CharaDatas[l].m_AfterFriendship = userNamedData3.FriendShip;
								this.m_Result.m_CharaDatas[l].m_AfterFriendshipExp = userNamedData3.FriendShipExp;
								this.m_Result.m_CharaDatas[l].m_AddFriendshipExp = this.m_Result.m_CharaDatas[l].m_AfterFriendshipExp - this.m_Result.m_CharaDatas[l].m_BeforeFriendshipExp;
								this.m_Result.m_CharaDatas[l].m_FriendshipNextExps = dbMng.NamedFriendshipExpDB.GetNextMaxExps(this.m_Result.m_CharaDatas[l].m_BeforeFriendship, this.m_Result.m_CharaDatas[l].m_AfterFriendship, param2.m_FriendshipTableID);
							}
						}
					}
				}
				eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
				if (this.WorkQuestID == 1100010 && tutoarialSeq == eTutorialSeq.ADVPrologueIntro2Played_NextIsQuestLogSet)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.ApplyNextTutorialSeq();
				}
				this.ResetOnQuestLogSet();
				MissionManager.UnlockAction(eXlsMissionSeg.Battle, eXlsMissionBattleFuncType.QuestClear);
			}
			if (this.m_OnResponse_QuestLogSet != null)
			{
				this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
			}
		}

		// Token: 0x06001A61 RID: 6753 RVA: 0x0008A76C File Offset: 0x00088B6C
		public bool Request_QuestFailed(BattlePartyData battlePartyData, Action<MeigewwwParam, PlayerResponseTypes.Questlogset> onResponse)
		{
			this.m_OnResponse_QuestLogSet = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Questlogset(new PlayerRequestTypes.Questlogset
			{
				orderReceiveId = this.WorkRecvID,
				state = 1,
				clearRank = 0,
				skillExps = string.Empty,
				dropItems = string.Empty,
				killedEnemies = string.Empty,
				friendUseNum = 0,
				masterSkillUseNum = 0,
				uniqueSkillUseNum = 0
			}, new MeigewwwParam.Callback(this.OnResponse_QuestFailed));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A62 RID: 6754 RVA: 0x0008A800 File Offset: 0x00088C00
		private void OnResponse_QuestFailed(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogset questlogset = PlayerResponse.Questlogset(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogset == null)
			{
				if (this.m_OnResponse_QuestLogSet != null)
				{
					this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
				}
				return;
			}
			ResultCode result = questlogset.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(questlogset.player, userDataMng.UserData);
				APIUtility.wwwToUserData(questlogset.itemSummary, userDataMng.UserItemDatas);
				this.ResetOnQuestLogSet();
			}
			if (this.m_OnResponse_QuestLogSet != null)
			{
				this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
			}
		}

		// Token: 0x06001A63 RID: 6755 RVA: 0x0008A89C File Offset: 0x00088C9C
		public bool Request_QuestSuccessAdvOnly(Action<MeigewwwParam, PlayerResponseTypes.Questlogset> onResponse)
		{
			this.m_OnResponse_QuestLogSet = onResponse;
			QuestManager.QuestData questData = this.GetQuestData(this.WorkQuestID);
			if (questData.m_ClearRank == QuestDefine.eClearRank.None)
			{
				this.m_Result.m_IsFirstClear = true;
			}
			else
			{
				this.m_Result.m_IsFirstClear = false;
			}
			this.m_Result.m_QuestID = this.WorkQuestID;
			MeigewwwParam wwwParam = PlayerRequest.Questlogset(new PlayerRequestTypes.Questlogset
			{
				orderReceiveId = this.WorkRecvID,
				state = 2,
				clearRank = 3,
				skillExps = string.Empty,
				weaponSkillExps = string.Empty,
				dropItems = string.Empty,
				killedEnemies = string.Empty,
				friendUseNum = 0,
				masterSkillUseNum = 0,
				uniqueSkillUseNum = 0
			}, new MeigewwwParam.Callback(this.OnResponse_QuestSuccessAdvOnly));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A64 RID: 6756 RVA: 0x0008A980 File Offset: 0x00088D80
		private void OnResponse_QuestSuccessAdvOnly(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogset questlogset = PlayerResponse.Questlogset(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogset == null)
			{
				if (this.m_OnResponse_QuestLogSet != null)
				{
					this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
				}
				return;
			}
			ResultCode result = questlogset.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(questlogset.player, userDataMng.UserData);
				APIUtility.wwwToUserData(questlogset.itemSummary, userDataMng.UserItemDatas);
				this.m_Result.m_RewardFirst = questlogset.rewardFirst;
				this.m_Result.m_RewardGroup = questlogset.rewardGroup;
				this.m_Result.m_RewardComp = questlogset.rewardComp;
				this.ResetOnQuestLogSet();
			}
			if (this.m_OnResponse_QuestLogSet != null)
			{
				this.m_OnResponse_QuestLogSet(wwwParam, questlogset);
			}
		}

		// Token: 0x06001A65 RID: 6757 RVA: 0x0008AA4F File Offset: 0x00088E4F
		public void ResetOnQuestLogSet()
		{
			this.WorkRecvID = -1L;
			this.WorkQuestID = -1;
			this.m_WorkBattlePartyMngID = -1L;
			this.WorkTBuff = null;
			this.WorkEnemies = null;
			this.WorkScheduleItems = null;
			this.SaveResponse = null;
		}

		// Token: 0x06001A66 RID: 6758 RVA: 0x0008AA84 File Offset: 0x00088E84
		public bool Request_QuestRetry(Action<MeigewwwParam, PlayerResponseTypes.Questlogretry> onResponse)
		{
			this.m_OnResponse_QuestRetry = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Questlogretry(new PlayerRequestTypes.Questlogretry
			{
				orderReceiveId = this.WorkRecvID,
				questData = this.SaveResponse
			}, new MeigewwwParam.Callback(this.OnResponse_QuestRetry));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A67 RID: 6759 RVA: 0x0008AAE0 File Offset: 0x00088EE0
		private void OnResponse_QuestRetry(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogretry questlogretry = PlayerResponse.Questlogretry(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogretry == null)
			{
				return;
			}
			ResultCode result = questlogretry.GetResult();
			switch (result)
			{
			case ResultCode.UNLIMITED_GEM_IS_SHORT:
			case ResultCode.GEM_IS_SHORT:
				break;
			default:
				if (result == ResultCode.SUCCESS)
				{
					UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
					APIUtility.wwwToUserData(questlogretry.player, userDataMng.UserData);
					this.WorkRecvID = questlogretry.orderReceiveId;
				}
				break;
			}
			this.m_OnResponse_QuestRetry.Call(wwwParam, questlogretry);
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06001A68 RID: 6760 RVA: 0x0008AB6B File Offset: 0x00088F6B
		public bool IsRequestingSaveBSD
		{
			get
			{
				return this.m_IsRequestingSaveBSD;
			}
		}

		// Token: 0x06001A69 RID: 6761 RVA: 0x0008AB74 File Offset: 0x00088F74
		public bool Request_SaveBSD(BattleSystemData bsd, int retryCount)
		{
			bool flag = false;
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsFinishedTutorialSeq())
			{
				if (retryCount != -1)
				{
					flag = true;
					this.m_SaveBSDRetryCount = retryCount;
				}
				else if (this.m_SaveBSDRetryCount > 0 && !this.m_IsRequestingSaveBSD)
				{
					flag = true;
				}
			}
			if (!flag)
			{
				return false;
			}
			this.m_IsRequestingSaveBSD = true;
			PlayerRequestTypes.Questlogsave questlogsave = new PlayerRequestTypes.Questlogsave();
			questlogsave.orderReceiveId = this.WorkRecvID;
			if (bsd != null)
			{
				this.SaveResponse = BattleUtility.BSDToSaveResponse(bsd);
			}
			questlogsave.questData = this.SaveResponse;
			MeigewwwParam wwwParam = PlayerRequest.Questlogsave(questlogsave, new MeigewwwParam.Callback(this.OnResponse_SaveBSD));
			NetworkQueueManager.Request(wwwParam);
			return true;
		}

		// Token: 0x06001A6A RID: 6762 RVA: 0x0008AC20 File Offset: 0x00089020
		private void OnResponse_SaveBSD(MeigewwwParam wwwParam)
		{
			this.m_IsRequestingSaveBSD = false;
			this.m_SaveBSDRetryCount--;
			if (wwwParam.IsDone())
			{
				if (!wwwParam.IsSystemError())
				{
					string responseJson = wwwParam.GetResponseJson();
					PlayerResponseTypes.Questlogsave questlogsave = ResponseCommon.Deserialize<PlayerResponseTypes.Questlogsave>(responseJson);
					if (questlogsave != null)
					{
						ResultCode result = questlogsave.GetResult();
						if (result == ResultCode.SUCCESS)
						{
							this.m_SaveBSDRetryCount = 0;
						}
					}
				}
			}
		}

		// Token: 0x06001A6B RID: 6763 RVA: 0x0008AC90 File Offset: 0x00089090
		public bool Request_DeleteBSD(Action onResponse)
		{
			this.m_OnResponse_DeleteBSD = onResponse;
			PlayerRequestTypes.Questlogreset param = new PlayerRequestTypes.Questlogreset();
			MeigewwwParam wwwParam = PlayerRequest.Questlogreset(param, new MeigewwwParam.Callback(this.OnResponse_DeleteBSD));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001A6C RID: 6764 RVA: 0x0008ACD4 File Offset: 0x000890D4
		private void OnResponse_DeleteBSD(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Questlogreset questlogreset = PlayerResponse.Questlogreset(wwwParam, ResponseCommon.DialogType.None, null);
			if (questlogreset == null)
			{
				return;
			}
			ResultCode result = questlogreset.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.ResetOnQuestLogSet();
				this.m_OnResponse_DeleteBSD.Call();
			}
		}

		// Token: 0x06001A6D RID: 6765 RVA: 0x0008AD2C File Offset: 0x0008912C
		private static void wwwConvert(QuestChapterArray src, QuestManager.ChapterData dest)
		{
			dest.m_ChapterID = src.id;
			dest.m_Difficulty = (QuestDefine.eDifficulty)src.difficulty;
			dest.m_ChapterName = src.name;
			int i;
			for (i = 0; i < src.questIds.Length; i++)
			{
				if (src.questIds[i] == -1)
				{
					break;
				}
			}
			dest.m_QuestIDs = new int[i];
			Array.Copy(src.questIds, dest.m_QuestIDs, i);
		}

		// Token: 0x06001A6E RID: 6766 RVA: 0x0008ADAC File Offset: 0x000891AC
		public static void wwwConvert(Quest src, ref QuestListDB_Param ref_dest)
		{
			ref_dest.m_ID = src.id;
			ref_dest.m_QuestName = src.name;
			ref_dest.m_BgID = src.bgId;
			ref_dest.m_BGMCueName = src.bgmCue;
			ref_dest.m_LastWaveBGMCueName = src.lastBgmCue;
			ref_dest.m_IsHideElement = ((src.isHideElement != 1) ? 0 : 1);
			ref_dest.m_IsWarning = ((src.warn != 1) ? 0 : 1);
			ref_dest.m_Stamina = src.stamina;
			ref_dest.m_RewardMoney = src.rewardMoney;
			ref_dest.m_RewardUserExp = src.rewardUserExp;
			ref_dest.m_RewardMasterOrbExp = src.rewardMasterOrbExp;
			ref_dest.m_RewardCharaExp = src.rewardCharacterExp;
			ref_dest.m_RewardFriendshipExp = src.rewardFriendshipExp;
			ref_dest.m_IsRetryLimit = ((src.retryLimit != 1) ? 0 : 1);
			ref_dest.m_IsLoserBattle = ((src.loserBattle != 1) ? 0 : 1);
			ref_dest.m_WaveIDs = new int[]
			{
				src.waveId1,
				src.waveId2,
				src.waveId3,
				src.waveId4,
				src.waveId5
			};
			ref_dest.m_IsAdvOnly = ((src.advOnly != 1) ? 0 : 1);
			ref_dest.m_AdvID_Prev = src.advId1;
			ref_dest.m_AdvID_After = src.advId2;
			ref_dest.m_AdvID_AfterBranch = src.advId3;
			ref_dest.m_CpuFriendCharaID = src.cpuId;
			ref_dest.m_CpuFriendCharaLv = src.cpuLevel;
			ref_dest.m_CpuFriendLimitBreak = src.cpuLimitBreak;
			ref_dest.m_CpuFriendSkillLv = src.cpuSkillLevel;
			ref_dest.m_CpuFriendWeaponID = src.cpuWeaponId;
			ref_dest.m_CpuFriendWeaponLv = src.cpuWeaponLevel;
			ref_dest.m_ItemDropCorrectCharaIDs = src.itemDropCharacters;
			ref_dest.m_ItemDropProbabilityAdd = src.itemDropAdd;
			ref_dest.m_StoreReview = src.storeReview;
		}

		// Token: 0x06001A6F RID: 6767 RVA: 0x0008AF84 File Offset: 0x00089384
		private static void wwwConvert(EventQuest src, QuestManager.EventData dest)
		{
			dest.m_EventID = src.id;
			dest.m_QuestID = src.quest.id;
			dest.m_FreqType = (eEventQuestFreqType)src.freq;
			dest.m_StartAt = src.startAt;
			dest.m_EndAt = src.endAt;
			dest.m_ExEventID = src.exId1;
			dest.m_ExKeyItemID = src.exId2;
			dest.m_ExKeyItemNum = src.ex2Amount;
			if (string.IsNullOrEmpty(src.exId3) || src.exId3 == -1.ToString())
			{
				dest.m_ExItemIDs = null;
			}
			else
			{
				string[] array = src.exId3.Split(new char[]
				{
					';'
				});
				dest.m_ExItemIDs = new int[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					if (!int.TryParse(array[i], out dest.m_ExItemIDs[i]))
					{
						dest.m_ExItemIDs[i] = -1;
					}
				}
			}
			dest.m_ExTitleType = (eTitleType)src.exTitle;
			dest.m_ExNamedType = (eCharaNamedType)src.exName;
			dest.m_ExRare = (eRare)src.exRarity;
			dest.m_ExCost = src.exCost;
			if (src.exFire != 0 || src.exWater != 0 || src.exEarth != 0 || src.exWind != 0 || src.exMoon != 0 || src.exSun != 0)
			{
				dest.m_ExElements = new bool[6];
				dest.m_ExElements[0] = (src.exFire != 0);
				dest.m_ExElements[1] = (src.exWater != 0);
				dest.m_ExElements[2] = (src.exEarth != 0);
				dest.m_ExElements[3] = (src.exWind != 0);
				dest.m_ExElements[4] = (src.exMoon != 0);
				dest.m_ExElements[5] = (src.exSun != 0);
			}
			else
			{
				dest.m_ExElements = null;
			}
			dest.m_ExClasses = new bool[5];
			if (src.exFighter != 0 || src.exMagician != 0 || src.exPriest != 0 || src.exKnight != 0 || src.exAlchemist != 0)
			{
				dest.m_ExClasses = new bool[5];
				dest.m_ExClasses[0] = (src.exFighter != 0);
				dest.m_ExClasses[1] = (src.exMagician != 0);
				dest.m_ExClasses[2] = (src.exPriest != 0);
				dest.m_ExClasses[3] = (src.exKnight != 0);
				dest.m_ExClasses[4] = (src.exAlchemist != 0);
			}
			else
			{
				dest.m_ExClasses = null;
			}
		}

		// Token: 0x040020C7 RID: 8391
		private StringBuilder m_sb = new StringBuilder();

		// Token: 0x040020C8 RID: 8392
		private List<QuestManager.QuestData> m_QuestList = new List<QuestManager.QuestData>();

		// Token: 0x040020C9 RID: 8393
		private List<QuestManager.ChapterData[]> m_ChapterDatas = new List<QuestManager.ChapterData[]>();

		// Token: 0x040020CA RID: 8394
		private List<QuestManager.EventGroupData> m_EventGroupDatas = new List<QuestManager.EventGroupData>();

		// Token: 0x040020CB RID: 8395
		public long WorkRecvID = -1L;

		// Token: 0x040020CC RID: 8396
		public int WorkQuestID = -1;

		// Token: 0x040020CD RID: 8397
		private long m_WorkBattlePartyMngID = -1L;

		// Token: 0x040020CE RID: 8398
		public TownBuff WorkTBuff;

		// Token: 0x040020CF RID: 8399
		public BattleOccurEnemy WorkEnemies;

		// Token: 0x040020D0 RID: 8400
		public List<BattleDropItems> WorkScheduleItems;

		// Token: 0x040020D1 RID: 8401
		public string SaveResponse;

		// Token: 0x040020D2 RID: 8402
		private BattleResult m_Result;

		// Token: 0x040020D3 RID: 8403
		private QuestManager.SelectQuest m_SelectQuest = new QuestManager.SelectQuest();

		// Token: 0x040020DA RID: 8410
		private bool m_IsRequestingSaveBSD;

		// Token: 0x040020DB RID: 8411
		private int m_SaveBSDRetryCount;

		// Token: 0x02000527 RID: 1319
		public class QuestData
		{
			// Token: 0x06001A70 RID: 6768 RVA: 0x0008B24D File Offset: 0x0008964D
			public QuestData(QuestListDB_Param param, QuestDefine.eClearRank rank, int eventID)
			{
				this.m_Param = param;
				this.m_ClearRank = rank;
				this.m_EventID = eventID;
			}

			// Token: 0x06001A71 RID: 6769 RVA: 0x0008B271 File Offset: 0x00089671
			public bool IsCleared()
			{
				return this.m_ClearRank != QuestDefine.eClearRank.None;
			}

			// Token: 0x06001A72 RID: 6770 RVA: 0x0008B281 File Offset: 0x00089681
			public bool IsChapterQuest()
			{
				return this.m_EventID == -1;
			}

			// Token: 0x06001A73 RID: 6771 RVA: 0x0008B292 File Offset: 0x00089692
			public bool IsEventQuest()
			{
				return this.m_EventID != -1;
			}

			// Token: 0x06001A74 RID: 6772 RVA: 0x0008B2A4 File Offset: 0x000896A4
			public bool IsOccurredADV()
			{
				int advID_Prev = this.m_Param.m_AdvID_Prev;
				if (advID_Prev != -1 && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advID_Prev))
				{
					return false;
				}
				int advID_After = this.m_Param.m_AdvID_After;
				if (advID_After != -1 && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advID_After))
				{
					return false;
				}
				int advID_AfterBranch = this.m_Param.m_AdvID_AfterBranch;
				return advID_AfterBranch == -1 || SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advID_AfterBranch);
			}

			// Token: 0x040020DC RID: 8412
			public QuestListDB_Param m_Param;

			// Token: 0x040020DD RID: 8413
			public QuestDefine.eClearRank m_ClearRank;

			// Token: 0x040020DE RID: 8414
			public int m_EventID = -1;
		}

		// Token: 0x02000528 RID: 1320
		public class ChapterData
		{
			// Token: 0x040020DF RID: 8415
			public int m_ChapterID;

			// Token: 0x040020E0 RID: 8416
			public QuestDefine.eDifficulty m_Difficulty;

			// Token: 0x040020E1 RID: 8417
			public string m_ChapterName;

			// Token: 0x040020E2 RID: 8418
			public int[] m_QuestIDs;
		}

		// Token: 0x02000529 RID: 1321
		public class EventGroupData
		{
			// Token: 0x040020E3 RID: 8419
			public int m_EventQuestType;

			// Token: 0x040020E4 RID: 8420
			public int m_GroupID;

			// Token: 0x040020E5 RID: 8421
			public string m_GroupName;

			// Token: 0x040020E6 RID: 8422
			public bool m_IsGroupSkip;

			// Token: 0x040020E7 RID: 8423
			public List<QuestManager.EventData> m_EventDatas;
		}

		// Token: 0x0200052A RID: 1322
		public class EventData
		{
			// Token: 0x040020E8 RID: 8424
			public int m_EventID;

			// Token: 0x040020E9 RID: 8425
			public int m_QuestID;

			// Token: 0x040020EA RID: 8426
			public eEventQuestFreqType m_FreqType;

			// Token: 0x040020EB RID: 8427
			public DateTime m_StartAt;

			// Token: 0x040020EC RID: 8428
			public DateTime m_EndAt;

			// Token: 0x040020ED RID: 8429
			public int m_OrderTotal;

			// Token: 0x040020EE RID: 8430
			public int m_OrderDaily;

			// Token: 0x040020EF RID: 8431
			public int m_ExEventID;

			// Token: 0x040020F0 RID: 8432
			public int m_ExKeyItemID;

			// Token: 0x040020F1 RID: 8433
			public int m_ExKeyItemNum;

			// Token: 0x040020F2 RID: 8434
			public int[] m_ExItemIDs;

			// Token: 0x040020F3 RID: 8435
			public eTitleType m_ExTitleType;

			// Token: 0x040020F4 RID: 8436
			public eCharaNamedType m_ExNamedType;

			// Token: 0x040020F5 RID: 8437
			public eRare m_ExRare;

			// Token: 0x040020F6 RID: 8438
			public int m_ExCost;

			// Token: 0x040020F7 RID: 8439
			public bool[] m_ExElements;

			// Token: 0x040020F8 RID: 8440
			public bool[] m_ExClasses;
		}

		// Token: 0x0200052B RID: 1323
		public class SelectQuest
		{
			// Token: 0x06001A78 RID: 6776 RVA: 0x0008B357 File Offset: 0x00089757
			public SelectQuest()
			{
				this.Reset();
			}

			// Token: 0x06001A79 RID: 6777 RVA: 0x0008B365 File Offset: 0x00089765
			public bool IsInvalid()
			{
				return this.m_QuestID == -1;
			}

			// Token: 0x06001A7A RID: 6778 RVA: 0x0008B376 File Offset: 0x00089776
			public void Reset()
			{
				this.m_ChapterID = -1;
				this.m_Difficulty = QuestDefine.eDifficulty.Normal;
				this.m_GroupID = -1;
				this.m_EventID = -1;
				this.m_QuestID = -1;
				this.m_EventType = -1;
			}

			// Token: 0x06001A7B RID: 6779 RVA: 0x0008B3A2 File Offset: 0x000897A2
			public void SetupChapter(int chapterID, QuestDefine.eDifficulty difficulty, int questID)
			{
				this.m_ChapterID = chapterID;
				this.m_Difficulty = difficulty;
				this.m_QuestID = questID;
				this.m_EventType = -1;
			}

			// Token: 0x06001A7C RID: 6780 RVA: 0x0008B3C0 File Offset: 0x000897C0
			public void SetupEvent(int groupID, int eventID, int questID, int eventType)
			{
				this.m_GroupID = groupID;
				this.m_EventID = eventID;
				this.m_QuestID = questID;
				this.m_EventType = eventType;
			}

			// Token: 0x040020F9 RID: 8441
			public int m_ChapterID;

			// Token: 0x040020FA RID: 8442
			public QuestDefine.eDifficulty m_Difficulty;

			// Token: 0x040020FB RID: 8443
			public int m_GroupID;

			// Token: 0x040020FC RID: 8444
			public int m_EventID;

			// Token: 0x040020FD RID: 8445
			public int m_QuestID;

			// Token: 0x040020FE RID: 8446
			public int m_EventType;
		}
	}
}
