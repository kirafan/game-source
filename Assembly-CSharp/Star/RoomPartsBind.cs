﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061C RID: 1564
	public class RoomPartsBind : IRoomPartsAction
	{
		// Token: 0x06001E88 RID: 7816 RVA: 0x000A51FC File Offset: 0x000A35FC
		public RoomPartsBind(Transform pbase, Transform ptarget, bool freverse)
		{
			this.m_Target = ptarget;
			this.m_Base = pbase;
			this.m_Type = eRoomPartsAction.Bind;
			if (freverse)
			{
				this.m_Target.localScale = new Vector3(-1f, 1f, 1f);
			}
			else
			{
				this.m_Target.localScale = new Vector3(1f, 1f, 1f);
			}
			this.m_OffsetBase = new Vector3(0f, 0f, -0.1f);
			this.m_OffsetTarget = new Vector3(0f, 0f, -0.1f);
			this.m_Time = 1f;
			this.m_MaxTime = 1f;
			this.m_BackPos = this.m_Target.position;
			this.m_Active = true;
			this.m_CalcType = 0;
			this.m_OffsetRot = Vector3.zero;
		}

		// Token: 0x06001E89 RID: 7817 RVA: 0x000A52E4 File Offset: 0x000A36E4
		public RoomPartsBind(Transform pbase, Transform ptarget)
		{
			this.m_Target = ptarget;
			this.m_Base = pbase;
			this.m_Type = eRoomPartsAction.Bind;
			this.m_OffsetBase = new Vector3(0f, 0f, -0.1f);
			this.m_OffsetTarget = new Vector3(0f, 0f, -0.1f);
			this.m_Time = 1f;
			this.m_MaxTime = 1f;
			this.m_Active = true;
			this.m_CalcType = 0;
			this.m_BackPos = this.m_Target.position;
		}

		// Token: 0x06001E8A RID: 7818 RVA: 0x000A5375 File Offset: 0x000A3775
		public void SetOffsetAnime(Vector3 foffsetbase, Vector3 foffsettarget, float ftime)
		{
			this.m_OffsetBase = foffsetbase;
			this.m_OffsetTarget = foffsettarget;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_CalcType = 1;
			if (this.m_MaxTime == 0f)
			{
				this.m_CalcType = 0;
			}
		}

		// Token: 0x06001E8B RID: 7819 RVA: 0x000A53B5 File Offset: 0x000A37B5
		public void ReverseOffsetAnime(float ftime)
		{
			this.m_OffsetTarget = this.m_Base.position + this.m_OffsetTarget;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_CalcType = 2;
		}

		// Token: 0x06001E8C RID: 7820 RVA: 0x000A53EC File Offset: 0x000A37EC
		public void SetRotationOffset(Quaternion frot)
		{
			this.m_OffsetRot = frot.eulerAngles;
			this.m_MarkRot = this.m_Target.rotation.eulerAngles;
			this.m_RotCalc = true;
		}

		// Token: 0x06001E8D RID: 7821 RVA: 0x000A5428 File Offset: 0x000A3828
		public override bool PartsUpdate()
		{
			if (this.m_Active)
			{
				byte calcType = this.m_CalcType;
				if (calcType != 0)
				{
					if (calcType != 1)
					{
						if (calcType == 2)
						{
							this.m_Time += Time.deltaTime;
							if (this.m_Time >= this.m_MaxTime)
							{
								this.m_Time = this.m_MaxTime;
								this.m_CalcType = 0;
								this.m_Active = false;
								this.m_Target.position = this.m_BackPos;
							}
							else
							{
								float d = this.m_Time / this.m_MaxTime;
								this.m_Target.position = (this.m_BackPos - this.m_OffsetTarget) * d + this.m_OffsetTarget;
							}
						}
					}
					else
					{
						this.m_Time += Time.deltaTime;
						if (this.m_Time >= this.m_MaxTime)
						{
							this.m_Time = this.m_MaxTime;
							this.m_CalcType = 0;
						}
						float d = this.m_Time / this.m_MaxTime;
						this.m_Target.position = this.m_Base.position + (this.m_OffsetTarget - this.m_OffsetBase) * d + this.m_OffsetBase;
					}
				}
				else
				{
					this.m_Target.position = this.m_Base.position + this.m_OffsetTarget;
				}
				if (this.m_RotCalc)
				{
					Vector3 euler = this.m_Base.rotation.eulerAngles - this.m_OffsetRot + this.m_MarkRot;
					this.m_Target.rotation = Quaternion.Euler(euler);
				}
			}
			else
			{
				this.m_Target.position = this.m_BackPos;
			}
			return this.m_Active;
		}

		// Token: 0x04002529 RID: 9513
		public Transform m_Target;

		// Token: 0x0400252A RID: 9514
		public Transform m_Base;

		// Token: 0x0400252B RID: 9515
		public Vector3 m_OffsetBase;

		// Token: 0x0400252C RID: 9516
		public Vector3 m_OffsetTarget;

		// Token: 0x0400252D RID: 9517
		public Vector3 m_BackPos;

		// Token: 0x0400252E RID: 9518
		public float m_Time;

		// Token: 0x0400252F RID: 9519
		public float m_MaxTime;

		// Token: 0x04002530 RID: 9520
		public byte m_CalcType;

		// Token: 0x04002531 RID: 9521
		public Vector3 m_OffsetRot;

		// Token: 0x04002532 RID: 9522
		public Vector3 m_MarkRot;

		// Token: 0x04002533 RID: 9523
		public bool m_RotCalc;
	}
}
