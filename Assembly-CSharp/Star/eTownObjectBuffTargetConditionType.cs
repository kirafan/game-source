﻿using System;

namespace Star
{
	// Token: 0x02000246 RID: 582
	public enum eTownObjectBuffTargetConditionType
	{
		// Token: 0x04001330 RID: 4912
		None = -1,
		// Token: 0x04001331 RID: 4913
		TitleType,
		// Token: 0x04001332 RID: 4914
		ClassType,
		// Token: 0x04001333 RID: 4915
		ElementType,
		// Token: 0x04001334 RID: 4916
		Num
	}
}
