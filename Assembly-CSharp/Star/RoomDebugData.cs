﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000295 RID: 661
	[Serializable]
	public class RoomDebugData
	{
		// Token: 0x0400150C RID: 5388
		public bool m_Open;

		// Token: 0x0400150D RID: 5389
		public int m_BG;

		// Token: 0x0400150E RID: 5390
		public int m_Wall;

		// Token: 0x0400150F RID: 5391
		public int m_Floor;

		// Token: 0x04001510 RID: 5392
		public int m_RoomID;

		// Token: 0x04001511 RID: 5393
		[SerializeField]
		public List<RoomDebugObjectSetting> m_Table;
	}
}
