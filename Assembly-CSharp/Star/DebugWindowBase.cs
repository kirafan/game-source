﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000696 RID: 1686
	public class DebugWindowBase : MonoBehaviour
	{
		// Token: 0x060021CC RID: 8652 RVA: 0x000136A8 File Offset: 0x00011AA8
		public void WindowSetUp()
		{
			this.m_Window = DebugWindowBase.FindHrcTransform(base.transform, "BG");
			EventTrigger eventTrigger = base.gameObject.AddComponent<EventTrigger>();
			EventTrigger.Entry entry = new EventTrigger.Entry();
			entry.eventID = EventTriggerType.PointerDown;
			entry.callback.AddListener(delegate(BaseEventData eventData)
			{
				this.PushDown();
			});
			eventTrigger.triggers.Add(entry);
			Button button = DebugWindowBase.FindHrcObject(base.transform, "Close", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackClose));
			}
		}

		// Token: 0x060021CD RID: 8653 RVA: 0x0001374C File Offset: 0x00011B4C
		public void WindowUpdata()
		{
			int step = this.m_Step;
			if (step != 0)
			{
				if (step == 1)
				{
					if (Input.touchCount == 0)
					{
						this.m_Step = 0;
					}
					else
					{
						Vector2 vector = Input.GetTouch(0).position;
						vector = this.CalcVirtualScreenPos(vector);
						Vector2 vector2 = vector - this.m_TouchPos;
						Vector2 v = this.m_Window.transform.localPosition;
						v.x += vector2.x;
						v.y += vector2.y;
						this.m_Window.transform.localPosition = v;
						this.m_TouchPos = vector;
					}
				}
			}
		}

		// Token: 0x060021CE RID: 8654 RVA: 0x00013814 File Offset: 0x00011C14
		public void ResetWindowPos()
		{
			this.m_Window.transform.localPosition = Vector3.zero;
		}

		// Token: 0x060021CF RID: 8655 RVA: 0x0001382B File Offset: 0x00011C2B
		public void CallbackClose()
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}

		// Token: 0x060021D0 RID: 8656 RVA: 0x00013838 File Offset: 0x00011C38
		private void PushDown()
		{
			if (this.m_Step == 0)
			{
				this.m_TouchPos = Input.GetTouch(0).position;
				this.m_TouchPos = this.CalcVirtualScreenPos(this.m_TouchPos);
				Vector2 a = this.m_Window.position;
				a -= this.m_TouchPos;
				this.m_TouchOffset.x = a.x;
				this.m_TouchOffset.y = a.y;
				this.m_Step = 1;
			}
		}

		// Token: 0x060021D1 RID: 8657 RVA: 0x000138C0 File Offset: 0x00011CC0
		private Vector2 CalcVirtualScreenPos(Vector2 finput)
		{
			Vector2 result = finput;
			result.x = finput.x / (float)Screen.width * 1335f;
			result.y = finput.y / (float)Screen.height * 750f;
			return result;
		}

		// Token: 0x060021D2 RID: 8658 RVA: 0x00013908 File Offset: 0x00011D08
		public static object FindHrcObject(Transform ptarget, string findname, Type pfindkey)
		{
			Transform transform = DebugWindowBase.FindHrcTransform(ptarget, findname);
			if (transform != null)
			{
				return transform.gameObject.GetComponent(pfindkey);
			}
			return null;
		}

		// Token: 0x060021D3 RID: 8659 RVA: 0x00013938 File Offset: 0x00011D38
		public static Transform FindHrcTransform(Transform ptarget, string findname)
		{
			string[] array = findname.Split(new char[]
			{
				'/'
			});
			int num = 0;
			return DebugWindowBase.FindKeyTransform(ptarget, array, array.Length, ref num);
		}

		// Token: 0x060021D4 RID: 8660 RVA: 0x00013968 File Offset: 0x00011D68
		public static Transform FindKeyTransform(Transform ptarget, string[] findname, int flength, ref int findex)
		{
			if (ptarget.name == findname[findex])
			{
				findex++;
				if (findex >= flength)
				{
					return ptarget;
				}
			}
			int childCount = ptarget.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform transform = DebugWindowBase.FindKeyTransform(ptarget.GetChild(i), findname, flength, ref findex);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x04002847 RID: 10311
		private const float WD = 1335f;

		// Token: 0x04002848 RID: 10312
		private const float HI = 750f;

		// Token: 0x04002849 RID: 10313
		private Transform m_Window;

		// Token: 0x0400284A RID: 10314
		public int m_Step;

		// Token: 0x0400284B RID: 10315
		public Vector2 m_TouchPos;

		// Token: 0x0400284C RID: 10316
		public Vector2 m_TouchOffset;
	}
}
