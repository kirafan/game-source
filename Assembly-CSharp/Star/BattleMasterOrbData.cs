﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020000CE RID: 206
	public class BattleMasterOrbData
	{
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x0001C447 File Offset: 0x0001A847
		public int OrbID
		{
			get
			{
				return this.m_OrbID;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x0001C44F File Offset: 0x0001A84F
		public int OrbLv
		{
			get
			{
				return this.m_OrbLv;
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x0001C457 File Offset: 0x0001A857
		public int CommandNum
		{
			get
			{
				return (this.m_Commands == null) ? 0 : this.m_Commands.Count;
			}
		}

		// Token: 0x06000582 RID: 1410 RVA: 0x0001C475 File Offset: 0x0001A875
		public bool IsAvailable()
		{
			return this.m_OrbID != -1;
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x0001C488 File Offset: 0x0001A888
		public void Setup(int orbID, int orbLv)
		{
			this.m_OrbID = orbID;
			this.m_OrbLv = orbLv;
			if (this.m_OrbID != -1)
			{
				MasterOrbListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(this.m_OrbID);
				this.m_Commands = new List<BattleCommandData>();
				for (int i = 0; i < param.m_SkillAavailableLvs.Length; i++)
				{
					if (this.m_OrbLv >= param.m_SkillAavailableLvs[i])
					{
						BattleCommandData battleCommandData = new BattleCommandData();
						battleCommandData.Setup(BattleCommandData.eCommandType.MasterSkill, -1, null, param.m_SkillIDs[i], 1, 1, 0L, -1, true);
						this.m_Commands.Add(battleCommandData);
					}
				}
			}
		}

		// Token: 0x06000584 RID: 1412 RVA: 0x0001C532 File Offset: 0x0001A932
		public BattleCommandData GetCommandDataAt(int commandIndex)
		{
			if (this.m_Commands != null)
			{
				return this.m_Commands[commandIndex];
			}
			return null;
		}

		// Token: 0x04000456 RID: 1110
		private int m_OrbID = -1;

		// Token: 0x04000457 RID: 1111
		private int m_OrbLv = 1;

		// Token: 0x04000458 RID: 1112
		private List<BattleCommandData> m_Commands;
	}
}
