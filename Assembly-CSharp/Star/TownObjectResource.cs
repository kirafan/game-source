﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000715 RID: 1813
	public class TownObjectResource
	{
		// Token: 0x060023D9 RID: 9177 RVA: 0x000C0AF2 File Offset: 0x000BEEF2
		public void Update()
		{
			if (this.m_IsPreparing && this.PreparingModel())
			{
				this.m_IsPreparing = false;
			}
		}

		// Token: 0x060023DA RID: 9178 RVA: 0x000C0B11 File Offset: 0x000BEF11
		public void SetupResource(TownObjModelHandle hndl)
		{
			this.m_Hndl = hndl;
		}

		// Token: 0x060023DB RID: 9179 RVA: 0x000C0B1A File Offset: 0x000BEF1A
		public void Destroy()
		{
			if (this.m_ModelHndl != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_ModelHndl);
				this.m_ModelHndl = null;
			}
		}

		// Token: 0x060023DC RID: 9180 RVA: 0x000C0B39 File Offset: 0x000BEF39
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x060023DD RID: 9181 RVA: 0x000C0B41 File Offset: 0x000BEF41
		public void Prepare()
		{
			this.m_IsPreparing = true;
			this.PrepareModel();
		}

		// Token: 0x060023DE RID: 9182 RVA: 0x000C0B50 File Offset: 0x000BEF50
		private void PrepareModel()
		{
			string modelResourcePath = TownUtility.GetModelResourcePath(this.m_Hndl.HandleCategory, this.m_Hndl.GetResourceKey(), (int)this.m_Hndl.GetTimeKey());
			this.m_ModelHndl = ObjectResourceManager.CreateHandle(modelResourcePath);
		}

		// Token: 0x060023DF RID: 9183 RVA: 0x000C0B90 File Offset: 0x000BEF90
		private bool PreparingModel()
		{
			if (this.m_ModelHndl == null)
			{
				return true;
			}
			if (!this.m_ModelHndl.IsDone)
			{
				return false;
			}
			if (this.m_ModelHndl.IsError)
			{
				ObjectResourceManager.RetryHandle(this.m_ModelHndl);
				return false;
			}
			return true;
		}

		// Token: 0x060023E0 RID: 9184 RVA: 0x000C0BD0 File Offset: 0x000BEFD0
		public void SetAttachModel()
		{
			GameObject asset = this.m_ModelHndl.GetAsset<GameObject>();
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(asset);
			this.m_Hndl.AttachModel(gameObject);
			this.SetUpBaseAnimation(gameObject);
		}

		// Token: 0x060023E1 RID: 9185 RVA: 0x000C0C04 File Offset: 0x000BF004
		private void SetUpBaseAnimation(GameObject pobj)
		{
			MeigeAnimClipHolder[] componentsInChildren = pobj.GetComponentsInChildren<MeigeAnimClipHolder>();
			if (componentsInChildren.Length > 0)
			{
				this.m_Hndl.AnimCtrlOpen();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					this.m_Hndl.AddAnim("event_001", componentsInChildren[i]);
				}
				this.m_Hndl.AnimCtrlClose();
			}
			else
			{
				this.m_Hndl.ClearAnimCtrl();
			}
		}

		// Token: 0x04002AD5 RID: 10965
		private TownObjModelHandle m_Hndl;

		// Token: 0x04002AD6 RID: 10966
		private MeigeResource.Handler m_ModelHndl;

		// Token: 0x04002AD7 RID: 10967
		private bool m_IsPreparing;
	}
}
