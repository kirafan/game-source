﻿using System;
using System.Collections;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020000CF RID: 207
	public class BattleMasterSkillCutIn : MonoBehaviour
	{
		// Token: 0x06000586 RID: 1414 RVA: 0x0001C58B File Offset: 0x0001A98B
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x06000587 RID: 1415 RVA: 0x0001C594 File Offset: 0x0001A994
		private void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			BattleMasterSkillCutIn.eStep step = this.m_Step;
			if (step != BattleMasterSkillCutIn.eStep.Prepare_Wait)
			{
				if (step != BattleMasterSkillCutIn.eStep.Wait)
				{
					if (step == BattleMasterSkillCutIn.eStep.Play_Wait)
					{
						if (!this.m_IsRequestedVoice)
						{
							MasterOrbListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(this.m_MasterOrbID);
							SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_000, param.m_CueNames[this.m_MasterSkillIndex], false);
							this.m_IsRequestedVoice = true;
						}
						if (!this.IsPlaying())
						{
							this.m_BodyObj.SetActive(false);
							this.m_MeigeAnimCtrl.UpdateAnimation(0f);
							this.m_MeigeAnimCtrl.Pause();
							this.SetStep(BattleMasterSkillCutIn.eStep.None);
						}
					}
				}
			}
			else
			{
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.SetStep(BattleMasterSkillCutIn.eStep.Prepare_Error);
						goto IL_3FA;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						goto IL_3FA;
					}
				}
				if (this.m_MasterIllustSpriteHndl == null || this.m_MasterIllustSpriteHndl.IsAvailable())
				{
					if (this.m_MasterOrbSpriteHndl == null || this.m_MasterOrbSpriteHndl.IsAvailable())
					{
						GameObject gameObject = this.m_LoadingHndl.FindObj("MasterSkillCutIn", true) as GameObject;
						GameObject gameObject2 = this.m_LoadingHndl.FindObj("MeigeAC_MasterSkillCutIn@Take 001", true) as GameObject;
						this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), base.transform);
						string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(gameObject.name, false);
						this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
						Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
						component.cullingType = AnimationCullingType.AlwaysAnimate;
						this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
						MeigeAnimClipHolder componentInChildren = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.Open();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, componentInChildren);
						this.m_MeigeAnimCtrl.Close();
						int layer = LayerMask.NameToLayer("UI");
						this.SetLayerRecursively(this.m_BodyObj, layer);
						int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
						for (int i = 0; i < particleEmitterNum; i++)
						{
							MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(i);
							if (particleEmitter != null)
							{
								particleEmitter.SetLayer(layer);
								particleEmitter.SetSortingOrder(this.m_SortingOrder);
							}
						}
						Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
						for (int j = 0; j < rendererCache.Length; j++)
						{
							if (rendererCache[j] != null)
							{
								rendererCache[j].sortingOrder = this.m_SortingOrder;
							}
						}
						this.m_BodyObj.SetActive(false);
						MsbObjectHandler msbObjectHandlerByName = this.m_MsbHndl.GetMsbObjectHandlerByName("chara");
						if (msbObjectHandlerByName != null)
						{
							Renderer renderer = msbObjectHandlerByName.GetRenderer();
							if (renderer != null && renderer.material != null)
							{
								MeigeShaderUtility.SetTexture(renderer.material, this.m_MasterIllustSpriteHndl.Obj.texture, eTextureType.eTextureType_Albedo, 0);
							}
						}
						for (int k = 0; k < this.OBJ_ORB_ILLUSTS.Length; k++)
						{
							msbObjectHandlerByName = this.m_MsbHndl.GetMsbObjectHandlerByName(this.OBJ_ORB_ILLUSTS[k]);
							if (msbObjectHandlerByName != null)
							{
								Renderer renderer2 = msbObjectHandlerByName.GetRenderer();
								if (renderer2 != null && renderer2.material != null)
								{
									MeigeShaderUtility.SetTexture(renderer2.material, this.m_MasterOrbSpriteHndl.Obj.texture, eTextureType.eTextureType_Albedo, 0);
								}
							}
						}
						if (this.m_LoadingHndl != null)
						{
							this.m_Loader.UnloadAll(false);
							this.m_LoadingHndl = null;
						}
						this.m_IsDonePrepare = true;
						this.SetStep(BattleMasterSkillCutIn.eStep.Wait);
					}
				}
			}
			IL_3FA:
			this.UpdateCharaFade();
		}

		// Token: 0x06000588 RID: 1416 RVA: 0x0001C9A4 File Offset: 0x0001ADA4
		public void Prepare(int masterOrbID, int sortingOrder)
		{
			if (this.m_IsDonePrepare)
			{
				return;
			}
			this.m_MasterOrbID = masterOrbID;
			this.m_SortingOrder = sortingOrder;
			this.m_LoadingHndl = this.m_Loader.Load("battle/masterskillcutin/masterskillcutin.muast", new MeigeResource.Option[0]);
			this.m_MasterIllustSpriteHndl = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncMasterIllust();
			this.m_MasterOrbSpriteHndl = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncMasterOrb(masterOrbID);
			this.SetStep(BattleMasterSkillCutIn.eStep.Prepare_Wait);
		}

		// Token: 0x06000589 RID: 1417 RVA: 0x0001CA19 File Offset: 0x0001AE19
		public bool IsCompletePrepare()
		{
			return this.m_IsDonePrepare;
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x0001CA21 File Offset: 0x0001AE21
		public bool IsPrepareError()
		{
			return this.m_Step == BattleMasterSkillCutIn.eStep.Prepare_Error;
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0001CA2C File Offset: 0x0001AE2C
		public void Play(int masterSkillIndex)
		{
			if (!this.m_IsDonePrepare)
			{
				return;
			}
			this.m_MasterSkillIndex = masterSkillIndex;
			this.m_IsRequestedVoice = false;
			this.m_BodyObj.transform.localPosZ(0f);
			this.m_BodyObj.SetActive(true);
			this.m_MsbHndl.SetDefaultVisibility();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_MASTERSKILL_CUTIN, 1f, 0, -1, -1);
			this.SetStep(BattleMasterSkillCutIn.eStep.Play_Wait);
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0001CAE9 File Offset: 0x0001AEE9
		public bool IsCompletePlay()
		{
			return this.m_Step == BattleMasterSkillCutIn.eStep.None;
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0001CAF4 File Offset: 0x0001AEF4
		public void ShowChara()
		{
			this.m_BodyObj.transform.localPosZ(0f);
			this.m_BodyObj.SetActive(true);
			this.m_MsbHndl.SetDefaultVisibility();
			this.m_MeigeAnimCtrl.Pause();
			this.FadeChara(0f, 1f, 0.1f);
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0001CB4D File Offset: 0x0001AF4D
		public void HideChara()
		{
			this.m_BodyObj.SetActive(false);
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0001CB68 File Offset: 0x0001AF68
		public void Destroy()
		{
			this.m_IsDonePrepare = false;
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			this.m_BodyObj = null;
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_MasterIllustSpriteHndl);
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_MasterOrbSpriteHndl);
			}
			this.m_MasterIllustSpriteHndl = null;
			this.m_MasterOrbSpriteHndl = null;
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0001CBE0 File Offset: 0x0001AFE0
		private void SetStep(BattleMasterSkillCutIn.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0001CBEC File Offset: 0x0001AFEC
		private void SetLayerRecursively(GameObject self, int layer)
		{
			self.layer = layer;
			IEnumerator enumerator = self.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					this.SetLayerRecursively(transform.gameObject, layer);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0001CC60 File Offset: 0x0001B060
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0001CC7C File Offset: 0x0001B07C
		public void FadeChara(float startAlpha, float endAlpha, float sec)
		{
			this.m_StartAlpha = startAlpha;
			this.m_EndAlpha = endAlpha;
			this.m_FadeTime = sec;
			this.m_FadeTimer = 0f;
			this.SetCharaAlpha(this.m_StartAlpha);
			if (this.m_FadeTime <= 0f)
			{
				this.m_IsUpdateMeshColor = false;
			}
			else
			{
				this.m_IsUpdateMeshColor = true;
			}
		}

		// Token: 0x06000594 RID: 1428 RVA: 0x0001CCD8 File Offset: 0x0001B0D8
		private void UpdateCharaFade()
		{
			if (!this.m_IsUpdateMeshColor)
			{
				return;
			}
			this.m_FadeTimer += Time.deltaTime;
			float num = this.m_FadeTimer / this.m_FadeTime;
			if (num > 1f)
			{
				num = 1f;
			}
			float charaAlpha = Mathf.Lerp(this.m_StartAlpha, this.m_EndAlpha, num);
			this.SetCharaAlpha(charaAlpha);
			if (num >= 1f)
			{
				this.m_IsUpdateMeshColor = false;
			}
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x0001CD50 File Offset: 0x0001B150
		private void SetCharaAlpha(float a)
		{
			if (this.m_MsbHndl != null)
			{
				MsbObjectHandler msbObjectHandlerByName = this.m_MsbHndl.GetMsbObjectHandlerByName("chara");
				if (msbObjectHandlerByName != null)
				{
					Color meshColor = msbObjectHandlerByName.GetWork().m_MeshColor;
					meshColor.a = a;
					msbObjectHandlerByName.GetWork().m_MeshColor = meshColor;
				}
			}
		}

		// Token: 0x04000459 RID: 1113
		private const string LAYER_MASK = "UI";

		// Token: 0x0400045A RID: 1114
		private const string PLAY_KEY = "Take 001";

		// Token: 0x0400045B RID: 1115
		private const string RESOURCE_PATH = "battle/masterskillcutin/masterskillcutin.muast";

		// Token: 0x0400045C RID: 1116
		private const string OBJ_MODEL_PATH = "MasterSkillCutIn";

		// Token: 0x0400045D RID: 1117
		private const string OBJ_ANIM_PATH = "MeigeAC_MasterSkillCutIn@Take 001";

		// Token: 0x0400045E RID: 1118
		private const string OBJ_CHARA_ILLUST = "chara";

		// Token: 0x0400045F RID: 1119
		private readonly string[] OBJ_ORB_ILLUSTS = new string[]
		{
			"orb",
			"orb2"
		};

		// Token: 0x04000460 RID: 1120
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000461 RID: 1121
		private BattleMasterSkillCutIn.eStep m_Step = BattleMasterSkillCutIn.eStep.None;

		// Token: 0x04000462 RID: 1122
		private bool m_IsDonePrepare;

		// Token: 0x04000463 RID: 1123
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x04000464 RID: 1124
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04000465 RID: 1125
		private SpriteHandler m_MasterIllustSpriteHndl;

		// Token: 0x04000466 RID: 1126
		private SpriteHandler m_MasterOrbSpriteHndl;

		// Token: 0x04000467 RID: 1127
		private Transform m_BaseTransform;

		// Token: 0x04000468 RID: 1128
		private MsbHandler m_MsbHndl;

		// Token: 0x04000469 RID: 1129
		private GameObject m_BodyObj;

		// Token: 0x0400046A RID: 1130
		private int m_MasterOrbID = -1;

		// Token: 0x0400046B RID: 1131
		private int m_MasterSkillIndex;

		// Token: 0x0400046C RID: 1132
		private bool m_IsRequestedVoice;

		// Token: 0x0400046D RID: 1133
		private int m_SortingOrder;

		// Token: 0x0400046E RID: 1134
		private float m_StartAlpha;

		// Token: 0x0400046F RID: 1135
		private float m_EndAlpha;

		// Token: 0x04000470 RID: 1136
		private float m_FadeTime;

		// Token: 0x04000471 RID: 1137
		private float m_FadeTimer;

		// Token: 0x04000472 RID: 1138
		private bool m_IsUpdateMeshColor;

		// Token: 0x020000D0 RID: 208
		private enum eStep
		{
			// Token: 0x04000474 RID: 1140
			None = -1,
			// Token: 0x04000475 RID: 1141
			Prepare_Wait,
			// Token: 0x04000476 RID: 1142
			Prepare_Error,
			// Token: 0x04000477 RID: 1143
			Wait,
			// Token: 0x04000478 RID: 1144
			Play_Wait
		}
	}
}
