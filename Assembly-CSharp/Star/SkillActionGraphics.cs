﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000125 RID: 293
	public class SkillActionGraphics : ScriptableObject
	{
		// Token: 0x060007A2 RID: 1954 RVA: 0x0002E964 File Offset: 0x0002CD64
		public Dictionary<string, int> GetEffectIDs(eElementType elementType)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			if (this.m_evEffectPlay != null)
			{
				for (int i = 0; i < this.m_evEffectPlay.Count; i++)
				{
					string text = SkillActionUtility.ConvertEffectID(this.m_evEffectPlay[i].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text))
					{
						Dictionary<string, int> dictionary2;
						string key;
						(dictionary2 = dictionary)[key = text] = dictionary2[key] + 1;
					}
					else
					{
						dictionary.Add(text, 1);
					}
				}
			}
			if (this.m_evEffectProjectile_Penetrate != null)
			{
				for (int j = 0; j < this.m_evEffectProjectile_Penetrate.Count; j++)
				{
					string text2 = SkillActionUtility.ConvertEffectID(this.m_evEffectProjectile_Penetrate[j].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text2))
					{
						Dictionary<string, int> dictionary2;
						string key2;
						(dictionary2 = dictionary)[key2 = text2] = dictionary2[key2] + 1;
					}
					else
					{
						dictionary.Add(text2, 1);
					}
				}
			}
			if (this.m_evEffectProjectile_Straight != null)
			{
				for (int k = 0; k < this.m_evEffectProjectile_Straight.Count; k++)
				{
					string text3 = SkillActionUtility.ConvertEffectID(this.m_evEffectProjectile_Straight[k].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text3))
					{
						Dictionary<string, int> dictionary2;
						string key3;
						(dictionary2 = dictionary)[key3 = text3] = dictionary2[key3] + 1;
					}
					else
					{
						dictionary.Add(text3, 1);
					}
				}
			}
			if (this.m_evEffectProjectile_Parabola != null)
			{
				for (int l = 0; l < this.m_evEffectProjectile_Parabola.Count; l++)
				{
					string text4 = SkillActionUtility.ConvertEffectID(this.m_evEffectProjectile_Parabola[l].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text4))
					{
						Dictionary<string, int> dictionary2;
						string key4;
						(dictionary2 = dictionary)[key4 = text4] = dictionary2[key4] + 1;
					}
					else
					{
						dictionary.Add(text4, 1);
					}
				}
			}
			if (this.m_evEffectAttach != null)
			{
				for (int m = 0; m < this.m_evEffectAttach.Count; m++)
				{
					string text5 = SkillActionUtility.ConvertEffectID(this.m_evEffectAttach[m].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text5))
					{
						Dictionary<string, int> dictionary2;
						string key5;
						(dictionary2 = dictionary)[key5 = text5] = dictionary2[key5] + 1;
					}
					else
					{
						dictionary.Add(text5, 1);
					}
				}
			}
			return dictionary;
		}

		// Token: 0x060007A3 RID: 1955 RVA: 0x0002EC1C File Offset: 0x0002D01C
		public Dictionary<string, int> GetTrailEffectIDs(eElementType elementType)
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			if (this.m_evTrailAttach != null)
			{
				for (int i = 0; i < this.m_evTrailAttach.Count; i++)
				{
					string text = SkillActionUtility.ConvertEffectID(this.m_evTrailAttach[i].m_EffectID, elementType, this.m_SettingClassType, this.m_SettingMotionID, this.m_SettingGrade);
					if (dictionary.ContainsKey(text))
					{
						Dictionary<string, int> dictionary2;
						string key;
						(dictionary2 = dictionary)[key = text] = dictionary2[key] + 1;
					}
					else
					{
						dictionary.Add(text, 1);
					}
				}
			}
			return dictionary;
		}

		// Token: 0x04000758 RID: 1880
		public string m_ID;

		// Token: 0x04000759 RID: 1881
		public eClassType m_SettingClassType;

		// Token: 0x0400075A RID: 1882
		public sbyte m_SettingMotionID;

		// Token: 0x0400075B RID: 1883
		public byte m_SettingGrade;

		// Token: 0x0400075C RID: 1884
		public List<SkillActionEvent_EffectPlay> m_evEffectPlay;

		// Token: 0x0400075D RID: 1885
		public List<SkillActionEvent_EffectProjectile_Straight> m_evEffectProjectile_Straight;

		// Token: 0x0400075E RID: 1886
		public List<SkillActionEvent_EffectProjectile_Parabola> m_evEffectProjectile_Parabola;

		// Token: 0x0400075F RID: 1887
		public List<SkillActionEvent_EffectProjectile_Penetrate> m_evEffectProjectile_Penetrate;

		// Token: 0x04000760 RID: 1888
		public List<SkillActionEvent_EffectAttach> m_evEffectAttach;

		// Token: 0x04000761 RID: 1889
		public List<SkillActionEvent_EffectDetach> m_evEffectDetach;

		// Token: 0x04000762 RID: 1890
		public List<SkillActionEvent_TrailAttach> m_evTrailAttach;

		// Token: 0x04000763 RID: 1891
		public List<SkillActionEvent_TrailDetach> m_evTrailDetach;

		// Token: 0x04000764 RID: 1892
		public List<SkillActionEvent_WeaponThrow_Parabola> m_evWeaponThrow_Parabola;

		// Token: 0x04000765 RID: 1893
		public List<SkillActionEvent_WeaponVisible> m_evWeaponVisible;

		// Token: 0x04000766 RID: 1894
		public List<SkillActionEvent_WeaponReset> m_evWeaponReset;

		// Token: 0x04000767 RID: 1895
		public List<SkillActionEvent_PlaySE> m_evPlaySE;

		// Token: 0x04000768 RID: 1896
		public List<SkillActionEvent_PlayVoice> m_evPlayVoice;
	}
}
