﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200051F RID: 1311
	public sealed class PresentManager
	{
		// Token: 0x06001A10 RID: 6672 RVA: 0x00087214 File Offset: 0x00085614
		public List<PresentManager.PresentData> GetPresentList(ePresentType presentType = ePresentType.None)
		{
			if (presentType == ePresentType.None)
			{
				return this.m_PresentList;
			}
			List<PresentManager.PresentData> list = new List<PresentManager.PresentData>();
			for (int i = 0; i < this.m_PresentList.Count; i++)
			{
				if (this.m_PresentList[i].m_Type == presentType)
				{
					list.Add(this.m_PresentList[i]);
				}
			}
			return list;
		}

		// Token: 0x06001A11 RID: 6673 RVA: 0x0008727C File Offset: 0x0008567C
		public PresentManager.PresentData GetPresentData(long mngID)
		{
			for (int i = 0; i < this.m_PresentList.Count; i++)
			{
				if (this.m_PresentList[i].m_MngID == mngID)
				{
					return this.m_PresentList[i];
				}
			}
			return null;
		}

		// Token: 0x06001A12 RID: 6674 RVA: 0x000872CA File Offset: 0x000856CA
		public List<PresentManager.PresentData> GetHistoryList()
		{
			return this.m_HistoryList;
		}

		// Token: 0x06001A13 RID: 6675 RVA: 0x000872D4 File Offset: 0x000856D4
		public PresentManager.PresentData GetHistoryData(long mngID)
		{
			for (int i = 0; i < this.m_HistoryList.Count; i++)
			{
				if ((long)this.m_HistoryList[i].m_ID == mngID)
				{
					return this.m_HistoryList[i];
				}
			}
			return null;
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x06001A14 RID: 6676 RVA: 0x00087324 File Offset: 0x00085724
		// (remove) Token: 0x06001A15 RID: 6677 RVA: 0x0008735C File Offset: 0x0008575C
		private event Action<bool> m_OnResponse_GetPresentList;

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06001A16 RID: 6678 RVA: 0x00087394 File Offset: 0x00085794
		// (remove) Token: 0x06001A17 RID: 6679 RVA: 0x000873CC File Offset: 0x000857CC
		private event Action<bool> m_OnResponse_Get;

		// Token: 0x06001A18 RID: 6680 RVA: 0x00087402 File Offset: 0x00085802
		public List<PresentManager.PresentData> GetGotList()
		{
			return this.m_GotPresentList;
		}

		// Token: 0x06001A19 RID: 6681 RVA: 0x0008740C File Offset: 0x0008580C
		public bool Request_GetPresentList(Action<bool> onResponse)
		{
			this.m_OnResponse_GetPresentList = onResponse;
			PlayerRequestTypes.Presentgetall param = new PlayerRequestTypes.Presentgetall();
			MeigewwwParam wwwParam = PlayerRequest.Presentgetall(param, new MeigewwwParam.Callback(this.OnResponse_GetPresentList));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001A1A RID: 6682 RVA: 0x00087460 File Offset: 0x00085860
		private void OnResponse_GetPresentList(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Presentgetall presentgetall = PlayerResponse.Presentgetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (presentgetall == null)
			{
				return;
			}
			ResultCode result = presentgetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.m_PresentList.Clear();
				for (int i = 0; i < presentgetall.presents.Length; i++)
				{
					PresentManager.PresentData presentData = new PresentManager.PresentData();
					PresentManager.wwwConvert(presentgetall.presents[i], presentData, false);
					this.m_PresentList.Add(presentData);
				}
				this.m_HistoryList.Clear();
				for (int j = 0; j < presentgetall.received.Length; j++)
				{
					PresentManager.PresentData presentData2 = new PresentManager.PresentData();
					PresentManager.wwwConvert(presentgetall.received[j], presentData2, true);
					this.m_HistoryList.Add(presentData2);
				}
				this.UpdateNoticePresentCount();
				this.m_OnResponse_GetPresentList.Call(false);
			}
		}

		// Token: 0x06001A1B RID: 6683 RVA: 0x0008754C File Offset: 0x0008594C
		public bool Request_Get(List<long> presentMngIDs, Action<bool> onResponse)
		{
			this.m_OnResponse_Get = onResponse;
			PlayerRequestTypes.Presentget presentget = new PlayerRequestTypes.Presentget();
			presentget.managedPresentId = APIUtility.ArrayToStr<long>(presentMngIDs.ToArray());
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				presentget.stepCode = (int)(tutoarialSeq + 1);
			}
			MeigewwwParam wwwParam = PlayerRequest.Presentget(presentget, new MeigewwwParam.Callback(this.OnResponse_Get));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001A1C RID: 6684 RVA: 0x000875D0 File Offset: 0x000859D0
		private void OnResponse_Get(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Presentget presentget = PlayerResponse.Presentget(wwwParam, ResponseCommon.DialogType.None, null);
			if (presentget == null)
			{
				return;
			}
			ResultCode result = presentget.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(presentget.player, userDataMng.UserData);
				APIUtility.wwwToUserData(presentget.managedCharacters, userDataMng.UserCharaDatas);
				APIUtility.wwwToUserData(presentget.managedNamedTypes, userDataMng.UserNamedDatas);
				APIUtility.wwwToUserData(presentget.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(presentget.managedWeapons, userDataMng.UserWeaponDatas);
				APIUtility.wwwToUserData(presentget.managedMasterOrbs, userDataMng.UserMasterOrbDatas);
				for (int i = 0; i < presentget.received.Length; i++)
				{
					long managedPresentId = presentget.received[i].managedPresentId;
					for (int j = 0; j < this.m_PresentList.Count; j++)
					{
						if (this.m_PresentList[j].m_MngID == managedPresentId)
						{
							this.m_PresentList.RemoveAt(j);
							break;
						}
					}
				}
				this.m_GotPresentList.Clear();
				for (int k = 0; k < presentget.received.Length; k++)
				{
					PresentManager.PresentData presentData = new PresentManager.PresentData();
					PresentManager.wwwConvert(presentget.received[k], presentData, true);
					this.m_HistoryList.Insert(0, presentData);
					this.m_GotPresentList.Add(presentData);
				}
				this.UpdateNoticePresentCount();
				eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
				if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.ApplyNextTutorialSeq();
				}
				this.m_OnResponse_Get.Call(false);
			}
		}

		// Token: 0x06001A1D RID: 6685 RVA: 0x00087790 File Offset: 0x00085B90
		private static void wwwConvert(PlayerPresent src, PresentManager.PresentData dest, bool isHistory)
		{
			dest.m_MngID = src.managedPresentId;
			dest.m_Title = src.title;
			dest.m_Message = src.message;
			dest.m_CreatedDate = src.createdAt;
			dest.m_Date = src.deadlineAt;
			dest.m_ReceivedDate = src.receivedAt;
			dest.m_Type = (ePresentType)src.type;
			dest.m_ID = src.objectId;
			dest.m_Amount = src.amount;
			dest.m_AltItemID = -1;
			dest.m_AltItemAmount = 0;
			if (!string.IsNullOrEmpty(src.options))
			{
				string[] array = src.options.Split(new char[]
				{
					':'
				});
				if (array.Length >= 2)
				{
					dest.m_AltItemID = int.Parse(array[0]);
					dest.m_AltItemAmount = int.Parse(array[1]);
				}
			}
			if (isHistory)
			{
				dest.m_Date = src.receivedAt;
			}
			else
			{
				dest.m_Date = src.deadlineAt;
			}
			dest.m_IsIndefinitePeriod = src.deadlineFlag;
		}

		// Token: 0x06001A1E RID: 6686 RVA: 0x00087892 File Offset: 0x00085C92
		private void UpdateNoticePresentCount()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticePresentCount = this.GetPresentList(ePresentType.None).Count;
			}
		}

		// Token: 0x06001A1F RID: 6687 RVA: 0x000878C0 File Offset: 0x00085CC0
		public string GetPresentText(PresentManager.PresentData data)
		{
			ePresentType ePresentType = data.m_Type;
			int num = data.m_ID;
			int value = data.m_Amount;
			if (data.m_AltItemID != -1)
			{
				ePresentType = ePresentType.Item;
				num = data.m_AltItemID;
				value = data.m_AltItemAmount;
			}
			switch (ePresentType)
			{
			case ePresentType.Chara:
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(num).m_Name);
				return stringBuilder.ToString();
			}
			case ePresentType.Item:
			{
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder2.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(num).m_Name);
				stringBuilder2.Append("x");
				stringBuilder2.Append(value);
				return stringBuilder2.ToString();
			}
			case ePresentType.Weapon:
			{
				StringBuilder stringBuilder3 = new StringBuilder();
				stringBuilder3.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(num).m_WeaponName);
				stringBuilder3.Append("x");
				stringBuilder3.Append(value);
				return stringBuilder3.ToString();
			}
			case ePresentType.TownFacility:
			{
				StringBuilder stringBuilder4 = new StringBuilder();
				stringBuilder4.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num).m_ObjName);
				stringBuilder4.Append("x");
				stringBuilder4.Append(value);
				return stringBuilder4.ToString();
			}
			case ePresentType.RoomObject:
			{
				StringBuilder stringBuilder5 = new StringBuilder();
				stringBuilder5.Append(RoomObjectListUtil.GetAccessKeyToObjectParam(num).m_Name);
				stringBuilder5.Append("x");
				stringBuilder5.Append(value);
				return stringBuilder5.ToString();
			}
			case ePresentType.MasterOrb:
			{
				StringBuilder stringBuilder6 = new StringBuilder();
				stringBuilder6.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(num).m_Name);
				stringBuilder6.Append("x");
				stringBuilder6.Append(value);
				return stringBuilder6.ToString();
			}
			case ePresentType.KRRPoint:
			{
				StringBuilder stringBuilder7 = new StringBuilder();
				stringBuilder7.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.KiraraPoint));
				stringBuilder7.Append("x");
				stringBuilder7.Append(UIUtility.KPValueToString(value, false));
				return stringBuilder7.ToString();
			}
			case ePresentType.Gold:
			{
				StringBuilder stringBuilder8 = new StringBuilder();
				stringBuilder8.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gold));
				stringBuilder8.Append("x");
				stringBuilder8.Append(UIUtility.GoldValueToString(value, false));
				return stringBuilder8.ToString();
			}
			case ePresentType.UnlimitedGem:
			{
				StringBuilder stringBuilder9 = new StringBuilder();
				stringBuilder9.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem));
				stringBuilder9.Append("x");
				stringBuilder9.Append(UIUtility.GemValueToString(value, false));
				return stringBuilder9.ToString();
			}
			case ePresentType.LimitedGem:
			{
				StringBuilder stringBuilder10 = new StringBuilder();
				stringBuilder10.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem));
				stringBuilder10.Append("x");
				stringBuilder10.Append(UIUtility.GemValueToString(value, false));
				return stringBuilder10.ToString();
			}
			default:
				return string.Empty;
			}
		}

		// Token: 0x0400209D RID: 8349
		private List<PresentManager.PresentData> m_PresentList = new List<PresentManager.PresentData>();

		// Token: 0x0400209E RID: 8350
		private List<PresentManager.PresentData> m_HistoryList = new List<PresentManager.PresentData>();

		// Token: 0x040020A1 RID: 8353
		private List<PresentManager.PresentData> m_GotPresentList = new List<PresentManager.PresentData>();

		// Token: 0x02000520 RID: 1312
		public class PresentData : IComparable<PresentManager.PresentData>
		{
			// Token: 0x06001A21 RID: 6689 RVA: 0x00087BF1 File Offset: 0x00085FF1
			public int CompareTo(PresentManager.PresentData comparePart)
			{
				if (comparePart == null)
				{
					return 1;
				}
				if (this.m_CreatedDate > comparePart.m_CreatedDate)
				{
					return -1;
				}
				if (this.m_CreatedDate < comparePart.m_CreatedDate)
				{
					return 1;
				}
				return 0;
			}

			// Token: 0x040020A2 RID: 8354
			public long m_MngID;

			// Token: 0x040020A3 RID: 8355
			public string m_Title;

			// Token: 0x040020A4 RID: 8356
			public string m_Message;

			// Token: 0x040020A5 RID: 8357
			public DateTime m_Date;

			// Token: 0x040020A6 RID: 8358
			public DateTime m_CreatedDate;

			// Token: 0x040020A7 RID: 8359
			public DateTime m_ReceivedDate;

			// Token: 0x040020A8 RID: 8360
			public ePresentType m_Type;

			// Token: 0x040020A9 RID: 8361
			public int m_ID;

			// Token: 0x040020AA RID: 8362
			public int m_Amount;

			// Token: 0x040020AB RID: 8363
			public int m_AltItemID = -1;

			// Token: 0x040020AC RID: 8364
			public int m_AltItemAmount;

			// Token: 0x040020AD RID: 8365
			public bool m_IsIndefinitePeriod;
		}
	}
}
