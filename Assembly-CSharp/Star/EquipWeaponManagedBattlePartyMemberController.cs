﻿using System;

namespace Star
{
	// Token: 0x020002D3 RID: 723
	public class EquipWeaponManagedBattlePartyMemberController : EquipWeaponManagedPartyMemberControllerExGen<EquipWeaponManagedBattlePartyMemberController>
	{
		// Token: 0x06000E1A RID: 3610 RVA: 0x0004D6C3 File Offset: 0x0004BAC3
		public EquipWeaponManagedBattlePartyMemberController() : base(eCharacterDataControllerType.ManagedBattleParty)
		{
		}

		// Token: 0x06000E1B RID: 3611 RVA: 0x0004D6CC File Offset: 0x0004BACC
		protected override CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			UserCharacterData battlePartyUserCharacterData = EditUtility.GetBattlePartyUserCharacterData(in_partyIndex, in_partySlotIndex);
			if (battlePartyUserCharacterData == null)
			{
				return new CharacterDataEmpty();
			}
			return new UserCharacterDataWrapper(battlePartyUserCharacterData);
		}

		// Token: 0x06000E1C RID: 3612 RVA: 0x0004D6F3 File Offset: 0x0004BAF3
		protected override UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return EditUtility.GetBattlePartyUserWeaponData(in_partyIndex, in_partySlotIndex);
		}
	}
}
