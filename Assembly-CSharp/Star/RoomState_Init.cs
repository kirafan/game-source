﻿using System;
using Star.UI;

namespace Star
{
	// Token: 0x02000473 RID: 1139
	public class RoomState_Init : RoomState
	{
		// Token: 0x0600163C RID: 5692 RVA: 0x0007450B File Offset: 0x0007290B
		public RoomState_Init(RoomMain owner) : base(owner)
		{
		}

		// Token: 0x0600163D RID: 5693 RVA: 0x0007451B File Offset: 0x0007291B
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600163E RID: 5694 RVA: 0x0007451E File Offset: 0x0007291E
		public override void OnStateEnter()
		{
			this.m_Step = RoomState_Init.eStep.First;
		}

		// Token: 0x0600163F RID: 5695 RVA: 0x00074527 File Offset: 0x00072927
		public override void OnStateExit()
		{
		}

		// Token: 0x06001640 RID: 5696 RVA: 0x00074529 File Offset: 0x00072929
		public override void OnDispose()
		{
		}

		// Token: 0x06001641 RID: 5697 RVA: 0x0007452C File Offset: 0x0007292C
		public override int OnStateUpdate()
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			switch (this.m_Step)
			{
			case RoomState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.OpenQuickIfNotDisplay(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Step = RoomState_Init.eStep.UIPrepare_Start;
				}
				break;
			case RoomState_Init.eStep.UIPrepare_Start:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.RoomUI, true);
				this.m_Step = RoomState_Init.eStep.UIPrepare_Processing;
				break;
			case RoomState_Init.eStep.UIPrepare_Processing:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.RoomUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.RoomUI);
					this.m_Step = RoomState_Init.eStep.ShopUIPrepare_Start;
				}
				break;
			case RoomState_Init.eStep.ShopUIPrepare_Start:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.RoomShopListUI, true);
				this.m_Step = RoomState_Init.eStep.ShopUIPrepare_Processing;
				break;
			case RoomState_Init.eStep.ShopUIPrepare_Processing:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.RoomShopListUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.RoomShopListUI);
					this.m_Step = RoomState_Init.eStep.BuilderPrepare_Start;
				}
				break;
			case RoomState_Init.eStep.BuilderPrepare_Start:
			{
				this.m_Owner.InitializeBuilder();
				long num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
				this.m_Owner.PlayerID = -1L;
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomPlayerID != -1L)
				{
					num = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomPlayerID;
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomPlayerID = -1L;
					this.m_Owner.PlayerID = num;
				}
				this.m_Owner.Builder.Prepare(num, true);
				this.m_Step = RoomState_Init.eStep.BuilderPrepare_Processing;
				break;
			}
			case RoomState_Init.eStep.BuilderPrepare_Processing:
				if (!this.m_Owner.Builder.IsPreparing())
				{
					this.m_Step = RoomState_Init.eStep.UIStart;
				}
				break;
			case RoomState_Init.eStep.UIStart:
			{
				bool flag = this.m_Owner.InitializeUI();
				if (flag)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = RoomState_Init.eStep.End;
				}
				break;
			}
			case RoomState_Init.eStep.End:
				this.m_Owner.SetupUIOnRoomStart();
				this.m_Step = RoomState_Init.eStep.None;
				return 1;
			}
			return -1;
		}

		// Token: 0x06001642 RID: 5698 RVA: 0x00074797 File Offset: 0x00072B97
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001CE5 RID: 7397
		private RoomState_Init.eStep m_Step = RoomState_Init.eStep.None;

		// Token: 0x02000474 RID: 1140
		private enum eStep
		{
			// Token: 0x04001CE7 RID: 7399
			None = -1,
			// Token: 0x04001CE8 RID: 7400
			First,
			// Token: 0x04001CE9 RID: 7401
			UIPrepare_Start,
			// Token: 0x04001CEA RID: 7402
			UIPrepare_Processing,
			// Token: 0x04001CEB RID: 7403
			ShopUIPrepare_Start,
			// Token: 0x04001CEC RID: 7404
			ShopUIPrepare_Processing,
			// Token: 0x04001CED RID: 7405
			BuilderPrepare_Start,
			// Token: 0x04001CEE RID: 7406
			BuilderPrepare_Processing,
			// Token: 0x04001CEF RID: 7407
			UIStart,
			// Token: 0x04001CF0 RID: 7408
			End
		}
	}
}
