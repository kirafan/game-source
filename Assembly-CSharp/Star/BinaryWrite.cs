﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Star
{
	// Token: 0x0200033C RID: 828
	public class BinaryWrite : BinaryIO
	{
		// Token: 0x06000FF0 RID: 4080 RVA: 0x00054E86 File Offset: 0x00053286
		public void PushPoint()
		{
			this.m_MarkPoint = this.m_FileSize;
			this.m_StackPoint[this.m_StackNum] = this.m_FileSize;
			this.m_StackNum++;
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x00054EB5 File Offset: 0x000532B5
		public void PopPoint()
		{
			if (this.m_StackNum > 0)
			{
				this.m_StackNum--;
				this.m_MarkPoint = this.m_StackPoint[this.m_StackNum];
			}
		}

		// Token: 0x06000FF2 RID: 4082 RVA: 0x00054EE4 File Offset: 0x000532E4
		public int OffsetPoint()
		{
			return this.m_FileSize - this.m_MarkPoint;
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x00054EF3 File Offset: 0x000532F3
		public int FilePoint()
		{
			return this.m_FileSize;
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x00054EFC File Offset: 0x000532FC
		public int Enum(Enum fkey, Type ftype)
		{
			int num = (int)fkey;
			this.SetInt(num, -1);
			return num;
		}

		// Token: 0x06000FF5 RID: 4085 RVA: 0x00054F1C File Offset: 0x0005331C
		public void Bool(ref bool fkey)
		{
			byte fdat = (!fkey) ? 0 : 1;
			this.SetByte(fdat, -1);
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x00054F41 File Offset: 0x00053341
		public void Byte(ref byte fkey)
		{
			this.SetByte(fkey, -1);
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x00054F4D File Offset: 0x0005334D
		public void Short(ref short fkey)
		{
			this.SetShort(fkey, -1);
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x00054F59 File Offset: 0x00053359
		public void Int(ref int fkey)
		{
			this.SetInt(fkey, -1);
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x00054F65 File Offset: 0x00053365
		public void Long(ref long fkey)
		{
			this.SetLong(fkey, -1);
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x00054F71 File Offset: 0x00053371
		public void Float(ref float fkey)
		{
			this.SetFloat(fkey, -1);
		}

		// Token: 0x06000FFB RID: 4091 RVA: 0x00054F7D File Offset: 0x0005337D
		public void String(ref string fkey)
		{
			this.SetString(fkey);
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x00054F88 File Offset: 0x00053388
		public void ByteTable(ref byte[] fkey)
		{
			this.SetInt(fkey.Length, -1);
			this.SetByteTable(fkey, -1);
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x00054FA0 File Offset: 0x000533A0
		public void ShortTable(ref short[] fkey)
		{
			this.SetInt(fkey.Length, -1);
			this.SetShortTable(fkey, -1);
		}

		// Token: 0x06000FFE RID: 4094 RVA: 0x00054FB8 File Offset: 0x000533B8
		public void IntTable(ref int[] fkey)
		{
			this.SetInt(fkey.Length, -1);
			this.SetIntTable(fkey, -1);
		}

		// Token: 0x06000FFF RID: 4095 RVA: 0x00054FD0 File Offset: 0x000533D0
		public int SetByte(byte fdat, int faccessid = -1)
		{
			int num;
			if (faccessid == -1)
			{
				num = this.m_FileSize;
				BinaryWrite.ByteWriteData byteWriteData = new BinaryWrite.ByteWriteData();
				byteWriteData.m_Type = BinaryWrite.eData.Byte;
				byteWriteData.m_FilePoint = this.m_FileSize;
				byteWriteData.m_Data = fdat;
				this.m_FileSize++;
				this.m_Table.Add(byteWriteData);
			}
			else
			{
				num = faccessid;
				BinaryWrite.ByteWriteData byteWriteData = (BinaryWrite.ByteWriteData)this.FindData(num);
				if (byteWriteData != null)
				{
					byteWriteData.m_Data = fdat;
				}
			}
			return num;
		}

		// Token: 0x06001000 RID: 4096 RVA: 0x00055048 File Offset: 0x00053448
		public int SetShort(short fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.ShortWriteData shortWriteData = new BinaryWrite.ShortWriteData();
				shortWriteData.m_Type = BinaryWrite.eData.Short;
				shortWriteData.m_FilePoint = this.m_FileSize;
				shortWriteData.m_Data = fdat;
				this.m_FileSize += 2;
				this.m_Table.Add(shortWriteData);
			}
			else
			{
				num = faccessid;
				BinaryWrite.ShortWriteData shortWriteData = (BinaryWrite.ShortWriteData)this.FindData(num);
				if (shortWriteData != null)
				{
					shortWriteData.m_Data = fdat;
				}
			}
			return num;
		}

		// Token: 0x06001001 RID: 4097 RVA: 0x000550C0 File Offset: 0x000534C0
		public int SetInt(int fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			BinaryWrite.IntWriteData intWriteData = new BinaryWrite.IntWriteData();
			if (faccessid == -1)
			{
				intWriteData.m_Type = BinaryWrite.eData.Int;
				intWriteData.m_FilePoint = this.m_FileSize;
				intWriteData.m_Data = fdat;
				this.m_Table.Add(intWriteData);
				this.m_FileSize += 4;
			}
			else
			{
				num = faccessid;
				intWriteData = (BinaryWrite.IntWriteData)this.FindData(num);
				if (intWriteData != null)
				{
					intWriteData.m_Data = fdat;
				}
			}
			return num;
		}

		// Token: 0x06001002 RID: 4098 RVA: 0x00055138 File Offset: 0x00053538
		public int SetLong(long fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.LongWriteData longWriteData = new BinaryWrite.LongWriteData();
				longWriteData.m_Type = BinaryWrite.eData.Long;
				longWriteData.m_FilePoint = this.m_FileSize;
				longWriteData.m_Data = fdat;
				this.m_Table.Add(longWriteData);
				this.m_FileSize += 8;
			}
			else
			{
				num = faccessid;
				BinaryWrite.LongWriteData longWriteData = (BinaryWrite.LongWriteData)this.FindData(num);
				if (longWriteData != null)
				{
					longWriteData.m_Data = fdat;
				}
			}
			return num;
		}

		// Token: 0x06001003 RID: 4099 RVA: 0x000551B0 File Offset: 0x000535B0
		public int SetFloat(float fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.FloatWriteData floatWriteData = new BinaryWrite.FloatWriteData();
				floatWriteData.m_Type = BinaryWrite.eData.Float;
				floatWriteData.m_FilePoint = this.m_FileSize;
				floatWriteData.m_Data = fdat;
				this.m_Table.Add(floatWriteData);
				this.m_FileSize += 4;
			}
			else
			{
				num = faccessid;
				BinaryWrite.FloatWriteData floatWriteData = (BinaryWrite.FloatWriteData)this.FindData(num);
				if (floatWriteData != null)
				{
					floatWriteData.m_Data = fdat;
				}
			}
			return num;
		}

		// Token: 0x06001004 RID: 4100 RVA: 0x00055228 File Offset: 0x00053628
		public int SetString(string fname)
		{
			int fileSize = this.m_FileSize;
			if (fname != null)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(fname);
				BinaryWrite.ShortWriteData shortWriteData = new BinaryWrite.ShortWriteData();
				shortWriteData.m_Type = BinaryWrite.eData.Short;
				shortWriteData.m_FilePoint = this.m_FileSize;
				shortWriteData.m_Data = (short)bytes.Length;
				this.m_Table.Add(shortWriteData);
				this.m_FileSize += 2;
				if (fname.Length != 0)
				{
					BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
					tableWriteData.m_Type = BinaryWrite.eData.Table;
					tableWriteData.m_FilePoint = this.m_FileSize;
					tableWriteData.m_Data = bytes;
					this.m_Table.Add(tableWriteData);
					this.m_FileSize += tableWriteData.m_Data.Length;
				}
			}
			else
			{
				BinaryWrite.ShortWriteData shortWriteData = new BinaryWrite.ShortWriteData();
				shortWriteData.m_Type = BinaryWrite.eData.Short;
				shortWriteData.m_FilePoint = this.m_FileSize;
				shortWriteData.m_Data = 0;
				this.m_Table.Add(shortWriteData);
				this.m_FileSize += 2;
			}
			return fileSize;
		}

		// Token: 0x06001005 RID: 4101 RVA: 0x00055318 File Offset: 0x00053718
		public int SetByteTable(byte[] fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
				tableWriteData.m_Type = BinaryWrite.eData.Table;
				tableWriteData.m_Data = new byte[fdat.Length];
				tableWriteData.m_FilePoint = this.m_FileSize;
				Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length);
				this.m_Table.Add(tableWriteData);
				this.m_FileSize += tableWriteData.m_Data.Length;
			}
			else
			{
				num = faccessid;
				BinaryWrite.TableWriteData tableWriteData = (BinaryWrite.TableWriteData)this.FindData(num);
				if (tableWriteData != null)
				{
					Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length * 2);
				}
			}
			return num;
		}

		// Token: 0x06001006 RID: 4102 RVA: 0x000553BC File Offset: 0x000537BC
		public int SetShortTable(short[] fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
				tableWriteData.m_Type = BinaryWrite.eData.Table;
				tableWriteData.m_Data = new byte[fdat.Length * 2];
				tableWriteData.m_FilePoint = this.m_FileSize;
				Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length * 2);
				this.m_Table.Add(tableWriteData);
				this.m_FileSize += tableWriteData.m_Data.Length;
			}
			else
			{
				num = faccessid;
				BinaryWrite.TableWriteData tableWriteData = (BinaryWrite.TableWriteData)this.FindData(num);
				if (tableWriteData != null)
				{
					Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length * 2);
				}
			}
			return num;
		}

		// Token: 0x06001007 RID: 4103 RVA: 0x00055464 File Offset: 0x00053864
		public int SetIntTable(int[] fdat, int faccessid = -1)
		{
			int num = this.m_FileSize;
			if (faccessid == -1)
			{
				BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
				tableWriteData.m_Type = BinaryWrite.eData.Table;
				tableWriteData.m_FilePoint = this.m_FileSize;
				tableWriteData.m_Data = new byte[fdat.Length * 4];
				Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length * 4);
				this.m_Table.Add(tableWriteData);
				this.m_FileSize += tableWriteData.m_Data.Length;
			}
			else
			{
				num = faccessid;
				BinaryWrite.TableWriteData tableWriteData = (BinaryWrite.TableWriteData)this.FindData(num);
				if (tableWriteData != null)
				{
					Buffer.BlockCopy(fdat, 0, tableWriteData.m_Data, 0, fdat.Length * 4);
				}
			}
			return num;
		}

		// Token: 0x06001008 RID: 4104 RVA: 0x0005550C File Offset: 0x0005390C
		public void Offset(int fslide)
		{
			BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
			tableWriteData.m_Type = BinaryWrite.eData.Table;
			tableWriteData.m_FilePoint = this.m_FileSize;
			tableWriteData.m_Data = new byte[fslide];
			this.m_Table.Add(tableWriteData);
			this.m_FileSize += fslide;
		}

		// Token: 0x06001009 RID: 4105 RVA: 0x00055558 File Offset: 0x00053958
		public void Align(int falign)
		{
			int num = (int)(((long)(this.m_FileSize + (falign - 1)) & ((long)(falign - 1) ^ (long)((ulong)-1))) - (long)this.m_FileSize);
			if (num != 0)
			{
				BinaryWrite.TableWriteData tableWriteData = new BinaryWrite.TableWriteData();
				tableWriteData.m_Type = BinaryWrite.eData.Table;
				tableWriteData.m_FilePoint = this.m_FileSize;
				tableWriteData.m_Data = new byte[num];
				this.m_Table.Add(tableWriteData);
				this.m_FileSize += num;
			}
		}

		// Token: 0x0600100A RID: 4106 RVA: 0x000555C8 File Offset: 0x000539C8
		private BinaryWrite.IWriteData FindData(int fkey)
		{
			for (int i = 0; i < this.m_Table.Count; i++)
			{
				if (this.m_Table[i].m_FilePoint == fkey)
				{
					return this.m_Table[i];
				}
			}
			return null;
		}

		// Token: 0x0600100B RID: 4107 RVA: 0x00055618 File Offset: 0x00053A18
		public byte[] PackToByte()
		{
			int count = this.m_Table.Count;
			int num = 0;
			for (int i = 0; i < count; i++)
			{
				BinaryWrite.IWriteData writeData = this.m_Table[i];
				switch (writeData.m_Type)
				{
				case BinaryWrite.eData.Byte:
					num++;
					break;
				case BinaryWrite.eData.Short:
					num += 2;
					break;
				case BinaryWrite.eData.Int:
					num += 4;
					break;
				case BinaryWrite.eData.Long:
					num += 8;
					break;
				case BinaryWrite.eData.Float:
					num += 4;
					break;
				case BinaryWrite.eData.Table:
				{
					BinaryWrite.TableWriteData tableWriteData = (BinaryWrite.TableWriteData)writeData;
					num += tableWriteData.m_Data.Length;
					break;
				}
				}
			}
			byte[] array = new byte[num];
			num = 0;
			for (int i = 0; i < count; i++)
			{
				BinaryWrite.IWriteData writeData = this.m_Table[i];
				switch (writeData.m_Type)
				{
				case BinaryWrite.eData.Byte:
				{
					BinaryWrite.ByteWriteData byteWriteData = (BinaryWrite.ByteWriteData)writeData;
					Buffer.BlockCopy(BitConverter.GetBytes((short)byteWriteData.m_Data), 0, array, num, 1);
					num++;
					break;
				}
				case BinaryWrite.eData.Short:
				{
					BinaryWrite.ShortWriteData shortWriteData = (BinaryWrite.ShortWriteData)writeData;
					Buffer.BlockCopy(BitConverter.GetBytes(shortWriteData.m_Data), 0, array, num, 2);
					num += 2;
					break;
				}
				case BinaryWrite.eData.Int:
				{
					BinaryWrite.IntWriteData intWriteData = (BinaryWrite.IntWriteData)writeData;
					Buffer.BlockCopy(BitConverter.GetBytes(intWriteData.m_Data), 0, array, num, 4);
					num += 4;
					break;
				}
				case BinaryWrite.eData.Long:
				{
					BinaryWrite.LongWriteData longWriteData = (BinaryWrite.LongWriteData)writeData;
					Buffer.BlockCopy(BitConverter.GetBytes(longWriteData.m_Data), 0, array, num, 8);
					num += 8;
					break;
				}
				case BinaryWrite.eData.Float:
				{
					BinaryWrite.FloatWriteData floatWriteData = (BinaryWrite.FloatWriteData)writeData;
					Buffer.BlockCopy(BitConverter.GetBytes(floatWriteData.m_Data), 0, array, num, 4);
					num += 4;
					break;
				}
				case BinaryWrite.eData.Table:
				{
					BinaryWrite.TableWriteData tableWriteData2 = (BinaryWrite.TableWriteData)writeData;
					Buffer.BlockCopy(tableWriteData2.m_Data, 0, array, num, tableWriteData2.m_Data.Length);
					num += tableWriteData2.m_Data.Length;
					break;
				}
				}
			}
			return array;
		}

		// Token: 0x04001713 RID: 5907
		public List<BinaryWrite.IWriteData> m_Table = new List<BinaryWrite.IWriteData>();

		// Token: 0x04001714 RID: 5908
		public int m_FileSize;

		// Token: 0x04001715 RID: 5909
		public int[] m_StackPoint = new int[8];

		// Token: 0x04001716 RID: 5910
		public int m_StackNum;

		// Token: 0x04001717 RID: 5911
		public int m_MarkPoint;

		// Token: 0x0200033D RID: 829
		public enum eData
		{
			// Token: 0x04001719 RID: 5913
			Byte,
			// Token: 0x0400171A RID: 5914
			Short,
			// Token: 0x0400171B RID: 5915
			Int,
			// Token: 0x0400171C RID: 5916
			Long,
			// Token: 0x0400171D RID: 5917
			Float,
			// Token: 0x0400171E RID: 5918
			String,
			// Token: 0x0400171F RID: 5919
			Table
		}

		// Token: 0x0200033E RID: 830
		public class IWriteData
		{
			// Token: 0x04001720 RID: 5920
			public BinaryWrite.eData m_Type;

			// Token: 0x04001721 RID: 5921
			public int m_FilePoint;
		}

		// Token: 0x0200033F RID: 831
		public class TableWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001722 RID: 5922
			public byte[] m_Data;
		}

		// Token: 0x02000340 RID: 832
		public class ByteWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001723 RID: 5923
			public byte m_Data;
		}

		// Token: 0x02000341 RID: 833
		public class ShortWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001724 RID: 5924
			public short m_Data;
		}

		// Token: 0x02000342 RID: 834
		public class IntWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001725 RID: 5925
			public int m_Data;
		}

		// Token: 0x02000343 RID: 835
		public class LongWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001726 RID: 5926
			public long m_Data;
		}

		// Token: 0x02000344 RID: 836
		public class FloatWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001727 RID: 5927
			public float m_Data;
		}

		// Token: 0x02000345 RID: 837
		public class StringWriteData : BinaryWrite.IWriteData
		{
			// Token: 0x04001728 RID: 5928
			public string m_Data;
		}
	}
}
