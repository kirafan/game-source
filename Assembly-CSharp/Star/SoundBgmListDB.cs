﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000211 RID: 529
	public class SoundBgmListDB : ScriptableObject
	{
		// Token: 0x04000D1E RID: 3358
		public SoundBgmListDB_Param[] m_Params;
	}
}
