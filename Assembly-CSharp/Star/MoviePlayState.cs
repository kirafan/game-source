﻿using System;

namespace Star
{
	// Token: 0x0200044C RID: 1100
	public class MoviePlayState : GameStateBase
	{
		// Token: 0x06001539 RID: 5433 RVA: 0x0006F02F File Offset: 0x0006D42F
		public MoviePlayState(MoviePlayMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600153A RID: 5434 RVA: 0x0006F045 File Offset: 0x0006D445
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600153B RID: 5435 RVA: 0x0006F048 File Offset: 0x0006D448
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600153C RID: 5436 RVA: 0x0006F04A File Offset: 0x0006D44A
		public override void OnStateExit()
		{
		}

		// Token: 0x0600153D RID: 5437 RVA: 0x0006F04C File Offset: 0x0006D44C
		public override void OnDispose()
		{
		}

		// Token: 0x0600153E RID: 5438 RVA: 0x0006F04E File Offset: 0x0006D44E
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0600153F RID: 5439 RVA: 0x0006F051 File Offset: 0x0006D451
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001C17 RID: 7191
		protected MoviePlayMain m_Owner;

		// Token: 0x04001C18 RID: 7192
		protected int m_NextState = -1;
	}
}
