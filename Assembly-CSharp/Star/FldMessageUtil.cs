﻿using System;

namespace Star
{
	// Token: 0x0200038F RID: 911
	public class FldMessageUtil
	{
		// Token: 0x06001137 RID: 4407 RVA: 0x0005A57D File Offset: 0x0005897D
		public static string GetMessage(int fkeyid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownItemUpToItem + fkeyid);
		}
	}
}
