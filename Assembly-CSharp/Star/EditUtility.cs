﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using Star.Town;
using Star.UI;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x020002BA RID: 698
	public static class EditUtility
	{
		// Token: 0x06000D14 RID: 3348 RVA: 0x00049380 File Offset: 0x00047780
		public static EditUtility.SimulateAddExpParam SimulateCharaLv(int charaID, int maxLv, long currentExp, long addExp)
		{
			EditUtility.SimulateAddExpParam simulateAddExpParam = new EditUtility.SimulateAddExpParam();
			EditUtility.SimulateCharaLv(charaID, maxLv, currentExp, addExp, out simulateAddExpParam.afterLv, out simulateAddExpParam.afterExp, out simulateAddExpParam.getExp);
			return simulateAddExpParam;
		}

		// Token: 0x06000D15 RID: 3349 RVA: 0x000493AF File Offset: 0x000477AF
		public static void SimulateCharaLv(int charaID, int maxLv, long currentExp, long addExp, out int out_afterLv, out long out_afterExp, out long out_getExp)
		{
			EditUtility.SimulateCharaLv(charaID, maxLv, currentExp + addExp, out out_afterLv, out out_afterExp);
			out_getExp = out_afterExp - currentExp;
		}

		// Token: 0x06000D16 RID: 3350 RVA: 0x000493C8 File Offset: 0x000477C8
		public static EditUtility.SimulateCharaLevelParam SimulateCharaLv(int charaID, int maxLv, long work_RemainExp)
		{
			EditUtility.SimulateCharaLevelParam simulateCharaLevelParam = new EditUtility.SimulateCharaLevelParam();
			EditUtility.SimulateCharaLv(charaID, maxLv, work_RemainExp, out simulateCharaLevelParam.afterLv, out simulateCharaLevelParam.afterExp);
			return simulateCharaLevelParam;
		}

		// Token: 0x06000D17 RID: 3351 RVA: 0x000493F0 File Offset: 0x000477F0
		public static void SimulateCharaLv(int charaID, int maxLv, long work_RemainExp, out int out_afterLv, out long out_afterExp)
		{
			CharacterExpDB charaExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaExpDB;
			out_afterLv = 1;
			out_afterExp = 0L;
			while (work_RemainExp > 0L && out_afterLv < maxLv)
			{
				int nextExp = charaExpDB.GetNextExp(out_afterLv);
				if (nextExp <= 0)
				{
					break;
				}
				if (work_RemainExp < (long)nextExp)
				{
					out_afterExp += work_RemainExp;
					work_RemainExp = 0L;
				}
				else
				{
					out_afterLv++;
					long num = (long)nextExp;
					if (num > work_RemainExp)
					{
						num = work_RemainExp;
					}
					out_afterExp += num;
					work_RemainExp -= num;
				}
			}
		}

		// Token: 0x06000D18 RID: 3352 RVA: 0x00049478 File Offset: 0x00047878
		public static int SimulateCharaLvOnly(int charaID, int maxLv, long work_RemainExp)
		{
			int result;
			long num;
			EditUtility.SimulateCharaLv(charaID, maxLv, work_RemainExp, out result, out num);
			return result;
		}

		// Token: 0x06000D19 RID: 3353 RVA: 0x00049494 File Offset: 0x00047894
		public static long GetCharaNowExp(int currentLv, long exp)
		{
			CharacterExpDB charaExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaExpDB;
			int num = 0;
			for (int i = 1; i < currentLv; i++)
			{
				num += charaExpDB.GetNextExp(i);
			}
			if (exp >= (long)num)
			{
				return exp - (long)num;
			}
			Debug.Log(string.Concat(new object[]
			{
				"GetCharaNowExp() is error.  exp:",
				exp,
				", tmp:",
				num
			}));
			return 0L;
		}

		// Token: 0x06000D1A RID: 3354 RVA: 0x00049510 File Offset: 0x00047910
		public static long GetCharaNextMaxExp(int lv)
		{
			CharacterExpDB charaExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaExpDB;
			return (long)charaExpDB.GetNextExp(lv);
		}

		// Token: 0x06000D1B RID: 3355 RVA: 0x00049538 File Offset: 0x00047938
		public static int CalcPartyTotalCostAt(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			return EditUtility.CalcPartyTotalCost(userBattlePartyData);
		}

		// Token: 0x06000D1C RID: 3356 RVA: 0x00049564 File Offset: 0x00047964
		public static int CalcPartyTotalCost(long partyMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.GetUserBattlePartyData(partyMngID);
			return EditUtility.CalcPartyTotalCost(userBattlePartyData);
		}

		// Token: 0x06000D1D RID: 3357 RVA: 0x0004958C File Offset: 0x0004798C
		public static int CalcPartyTotalCost(UserBattlePartyData userBattlePartyData)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			if (userBattlePartyData == null)
			{
				return 0;
			}
			int num = 0;
			int slotNum = userBattlePartyData.GetSlotNum();
			for (int i = 0; i < slotNum; i++)
			{
				long memberAt = userBattlePartyData.GetMemberAt(i);
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(memberAt);
				if (userCharaData != null)
				{
					num += userCharaData.Param.Cost;
				}
				long weaponAt = userBattlePartyData.GetWeaponAt(i);
				UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponAt);
				if (userWpnData != null)
				{
					num += userWpnData.Param.Cost;
				}
			}
			return num;
		}

		// Token: 0x06000D1E RID: 3358 RVA: 0x0004961C File Offset: 0x00047A1C
		public static int CalcPartyTotalCostFromMngIDs(List<long> charaMngIDs, List<long> weaponMngIDs)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			List<int> list = null;
			if (charaMngIDs != null)
			{
				list = new List<int>();
				for (int i = 0; i < charaMngIDs.Count; i++)
				{
					UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngIDs[i]);
					if (userCharaData != null)
					{
						list.Add(userCharaData.Param.CharaID);
					}
				}
			}
			List<int> weaponIDs = null;
			if (weaponMngIDs != null)
			{
				weaponIDs = new List<int>();
				for (int j = 0; j < weaponMngIDs.Count; j++)
				{
					UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponMngIDs[j]);
					if (userWpnData != null)
					{
						list.Add(userWpnData.Param.WeaponID);
					}
				}
			}
			return EditUtility.CalcPartyTotalCostFromIDs(list, weaponIDs);
		}

		// Token: 0x06000D1F RID: 3359 RVA: 0x000496DC File Offset: 0x00047ADC
		public static int CalcPartyTotalCostFromIDs(List<int> charaIDs, List<int> weaponIDs)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			int num = 0;
			if (charaIDs != null)
			{
				for (int i = 0; i < charaIDs.Count; i++)
				{
					if (charaIDs[i] != -1)
					{
						num += dbMng.CharaListDB.GetParam(charaIDs[i]).m_Cost;
					}
				}
			}
			if (weaponIDs != null)
			{
				for (int j = 0; j < weaponIDs.Count; j++)
				{
					if (weaponIDs[j] != -1)
					{
						num += dbMng.WeaponListDB.GetParam(weaponIDs[j]).m_Cost;
					}
				}
			}
			return num;
		}

		// Token: 0x06000D20 RID: 3360 RVA: 0x0004978C File Offset: 0x00047B8C
		public static int CalcCharaCostFromIDs(int charaID, int weaponID)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			int num = 0;
			if (charaID != -1)
			{
				num += dbMng.CharaListDB.GetParam(charaID).m_Cost;
			}
			return num + EditUtility.CalcWeaponCostFromIDs(weaponID);
		}

		// Token: 0x06000D21 RID: 3361 RVA: 0x000497D0 File Offset: 0x00047BD0
		public static int CalcWeaponCostFromIDs(int weaponID)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (weaponID != -1)
			{
				return dbMng.WeaponListDB.GetParam(weaponID).m_Cost;
			}
			return 0;
		}

		// Token: 0x06000D22 RID: 3362 RVA: 0x00049805 File Offset: 0x00047C05
		public static int CalcCharaCostFromUserSupportData(UserSupportData userSupportData, bool appliedWeapon = false)
		{
			return EditUtility.CalcCharaCostFromIDs(userSupportData.CharaID, userSupportData.WeaponID);
		}

		// Token: 0x06000D23 RID: 3363 RVA: 0x00049818 File Offset: 0x00047C18
		public static int CalcCharaCostFromUserSupportData(UserSupportDataWrapper userSupportData, bool appliedWeapon = false)
		{
			int weaponID = (!appliedWeapon) ? -1 : userSupportData.GetWeaponId();
			return EditUtility.CalcCharaCostFromIDs(userSupportData.GetCharaId(), weaponID);
		}

		// Token: 0x06000D24 RID: 3364 RVA: 0x00049844 File Offset: 0x00047C44
		public static int CalcMaxLv(int charaID)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			return EditUtility.CalcMaxLv(ref param);
		}

		// Token: 0x06000D25 RID: 3365 RVA: 0x00049870 File Offset: 0x00047C70
		public static int CalcMaxLv(ref CharacterListDB_Param charaListParam)
		{
			int num = EditUtility.CalcTotalLvUpValuByLimitBreak(charaListParam.m_CharaID);
			return charaListParam.m_InitLimitLv + num;
		}

		// Token: 0x06000D26 RID: 3366 RVA: 0x00049894 File Offset: 0x00047C94
		public static int CalcMaxLvFromLimitBreak(int charaID, int limitBreak)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			CharacterLimitBreakListDB_Param param2 = dbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			int num = limitBreak;
			if (num >= param2.m_CharaMaxLvUps.Length)
			{
				num = param2.m_CharaMaxLvUps.Length;
			}
			int num2 = 0;
			for (int i = 0; i < num; i++)
			{
				num2 += param2.m_CharaMaxLvUps[i];
			}
			return param.m_InitLimitLv + num2;
		}

		// Token: 0x06000D27 RID: 3367 RVA: 0x0004991C File Offset: 0x00047D1C
		private static EditUtility.OutputCharaParam CalcCaharaParam(int currentLv, long exp, EditUtility.BasedCharaParamParams basedParams)
		{
			if (basedParams == null)
			{
				return default(EditUtility.OutputCharaParam);
			}
			return EditUtility.CalcCaharaParam(currentLv, exp, basedParams.lvBaseCharaParam, basedParams.weaponParam, basedParams.friendShipPercentageParam, basedParams.townBuffPercentageParam);
		}

		// Token: 0x06000D28 RID: 3368 RVA: 0x00049958 File Offset: 0x00047D58
		private static EditUtility.OutputCharaParam CalcCaharaParam(int currentLv, long exp, EditUtility.OutputCharaParam lvBaseCharaParam, EditUtility.OutputCharaParam weaponParam, EditUtility.OutputPercentageParam friendShipPercentageParam, EditUtility.OutputPercentageParam townBuffPercentageParam)
		{
			EditUtility.OutputCharaParam result = EditUtility.CalcCaharaParam(lvBaseCharaParam, weaponParam, friendShipPercentageParam, townBuffPercentageParam);
			result.NowExp = EditUtility.GetCharaNowExp(currentLv, exp);
			result.NextMaxExp = EditUtility.GetCharaNextMaxExp(currentLv);
			return result;
		}

		// Token: 0x06000D29 RID: 3369 RVA: 0x00049990 File Offset: 0x00047D90
		public static EditUtility.OutputCharaParam CalcCaharaParam(EditUtility.OutputCharaParam lvBaseCharaParam, EditUtility.OutputCharaParam weaponParam, EditUtility.OutputPercentageParam friendShipPercentageParam, EditUtility.OutputPercentageParam townBuffPercentageParam)
		{
			EditUtility.OutputCharaParam result = lvBaseCharaParam;
			result.Hp = EditUtility.CalcParamCorrect(result.Hp, friendShipPercentageParam.Hp);
			result.Atk = EditUtility.CalcParamCorrect(result.Atk, friendShipPercentageParam.Atk);
			result.Def = EditUtility.CalcParamCorrect(result.Def, friendShipPercentageParam.Def);
			result.MDef = EditUtility.CalcParamCorrect(result.MDef, friendShipPercentageParam.MDef);
			result.Mgc = EditUtility.CalcParamCorrect(result.Mgc, friendShipPercentageParam.Mgc);
			result.Spd = EditUtility.CalcParamCorrect(result.Spd, friendShipPercentageParam.Spd);
			result.Luck = EditUtility.CalcParamCorrect(result.Luck, friendShipPercentageParam.Luck);
			result.Hp = EditUtility.CalcParamCorrect(result.Hp, townBuffPercentageParam.Hp);
			result.Atk = EditUtility.CalcParamCorrect(result.Atk, townBuffPercentageParam.Atk);
			result.Def = EditUtility.CalcParamCorrect(result.Def, townBuffPercentageParam.Def);
			result.MDef = EditUtility.CalcParamCorrect(result.MDef, townBuffPercentageParam.MDef);
			result.Mgc = EditUtility.CalcParamCorrect(result.Mgc, townBuffPercentageParam.Mgc);
			result.Spd = EditUtility.CalcParamCorrect(result.Spd, townBuffPercentageParam.Spd);
			result.Luck = EditUtility.CalcParamCorrect(result.Luck, townBuffPercentageParam.Luck);
			result.Atk += weaponParam.Atk;
			result.Mgc += weaponParam.Mgc;
			result.Def += weaponParam.Def;
			result.MDef += weaponParam.MDef;
			return result;
		}

		// Token: 0x06000D2A RID: 3370 RVA: 0x00049B60 File Offset: 0x00047F60
		public static EditUtility.OutputCharaParam CalcCharaParamIgnoreTownBuff(int charaID, int currentLv, long exp, UserNamedData userNamedData, UserWeaponData userWeaponData)
		{
			EditUtility.OutputCharaParam lvBaseCharaParam = EditUtility.CalcCharaParamLevelOnly(charaID, currentLv);
			EditUtility.OutputCharaParam weaponParam = EditUtility.CalcWeaponParam(userWeaponData);
			EditUtility.OutputPercentageParam friendShipPercentageParam = default(EditUtility.OutputPercentageParam);
			if (userNamedData != null)
			{
				friendShipPercentageParam = EditUtility.GetFriendshipIncreaseRate(charaID, userNamedData.FriendShip);
			}
			EditUtility.OutputCharaParam result = EditUtility.CalcCaharaParam(lvBaseCharaParam, weaponParam, friendShipPercentageParam, default(EditUtility.OutputPercentageParam));
			result.NowExp = EditUtility.GetCharaNowExp(result.Lv, exp);
			result.NextMaxExp = EditUtility.GetCharaNextMaxExp(result.Lv);
			return result;
		}

		// Token: 0x06000D2B RID: 3371 RVA: 0x00049BD4 File Offset: 0x00047FD4
		public static EditUtility.OutputCharaParam CalcCharaParam(UserCharacterData userCharaData, UserWeaponData userWeaponData)
		{
			return EditUtility.CalcCharaParam(userCharaData, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(userCharaData.Param.NamedType), userWeaponData);
		}

		// Token: 0x06000D2C RID: 3372 RVA: 0x00049BF7 File Offset: 0x00047FF7
		public static EditUtility.OutputCharaParam CalcCharaParam(UserCharacterData userCharaData, UserNamedData userNamedData, UserWeaponData userWeaponData)
		{
			return EditUtility.CalcCharaParam(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharaData, userWeaponData), true, true, true);
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x00049C0D File Offset: 0x0004800D
		public static EditUtility.OutputCharaParam CalcCharaParam(UserSupportData userSupportData)
		{
			return EditUtility.CalcCharaParam(new EquipWeaponUnmanagedSupportPartyMemberController().Setup(userSupportData), true, true, true);
		}

		// Token: 0x06000D2E RID: 3374 RVA: 0x00049C24 File Offset: 0x00048024
		public static EditUtility.OutputCharaParam CalcCharaParam(EquipWeaponCharacterDataController partyMember, bool appliedFriendship = true, bool appliedTownBuff = true, bool appliedWeapon = true)
		{
			if (partyMember == null)
			{
				Debug.LogError("Invalid Character Data.");
				return default(EditUtility.OutputCharaParam);
			}
			return EditUtility.CalcCaharaParam(partyMember.GetCharaLv(), partyMember.GetCharaExp(), EditUtility.GetBasedCharaParamParams(partyMember, appliedFriendship, appliedTownBuff, appliedWeapon));
		}

		// Token: 0x06000D2F RID: 3375 RVA: 0x00049C68 File Offset: 0x00048068
		public static EditUtility.OutputCharaParam CalcCharaParamNoWeapon(CharacterDataController cdc, bool appliedFriendship = true, bool appliedTownBuff = true)
		{
			if (cdc == null)
			{
				Debug.LogError("Invalid Character Data.");
				return default(EditUtility.OutputCharaParam);
			}
			return EditUtility.CalcCaharaParam(cdc.GetCharaLv(), cdc.GetCharaExp(), EditUtility.GetBasedCharaParamParamsNoWeapon(cdc, appliedFriendship, appliedTownBuff));
		}

		// Token: 0x06000D30 RID: 3376 RVA: 0x00049CA8 File Offset: 0x000480A8
		private static EditUtility.BasedCharaParamParams GetBasedCharaParamParams(EquipWeaponCharacterDataController partyMember, bool appliedFriendship = true, bool appliedTownBuff = true, bool appliedWeapon = true)
		{
			if (partyMember == null)
			{
				Debug.LogError("Invalid Character Data.");
				return null;
			}
			switch (partyMember.GetCharacterDataControllerType())
			{
			case eCharacterDataControllerType.ManagedBattleParty:
			case eCharacterDataControllerType.ManagedSupportParty:
			case eCharacterDataControllerType.UnmanagedSupportParty:
			case eCharacterDataControllerType.UnmanagedUserCharacterData:
			{
				EditUtility.BasedCharaParamParams basedCharaParamParamsNoWeapon = EditUtility.GetBasedCharaParamParamsNoWeapon(partyMember, appliedFriendship, appliedTownBuff);
				if (basedCharaParamParamsNoWeapon == null)
				{
					return null;
				}
				if (appliedWeapon)
				{
					basedCharaParamParamsNoWeapon.weaponParam = partyMember.GetWeaponParam();
				}
				else
				{
					basedCharaParamParamsNoWeapon.weaponParam = default(EditUtility.OutputCharaParam);
				}
				return basedCharaParamParamsNoWeapon;
			}
			default:
				return null;
			}
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x00049D28 File Offset: 0x00048128
		private static EditUtility.BasedCharaParamParams GetBasedCharaParamParamsNoWeapon(CharacterDataController cdc, bool appliedFriendship = true, bool appliedTownBuff = true)
		{
			if (cdc == null)
			{
				Debug.LogError("Invalid Character Data.");
				return null;
			}
			switch (cdc.GetCharacterDataControllerType())
			{
			case eCharacterDataControllerType.ManagedBattleParty:
			case eCharacterDataControllerType.ManagedSupportParty:
			case eCharacterDataControllerType.UnmanagedSupportParty:
			case eCharacterDataControllerType.UnmanagedUserCharacterData:
			case eCharacterDataControllerType.UnmanagedCharacterDataWrapper:
			case eCharacterDataControllerType.AfterLimitBreakParam:
			{
				EditUtility.BasedCharaParamParams basedCharaParamParams = new EditUtility.BasedCharaParamParams();
				basedCharaParamParams.lvBaseCharaParam = cdc.GetCharaParamLvOnly();
				if (appliedFriendship)
				{
					basedCharaParamParams.friendShipPercentageParam = EditUtility.GetFriendshipIncreaseRate(cdc);
				}
				else
				{
					basedCharaParamParams.friendShipPercentageParam = default(EditUtility.OutputPercentageParam);
				}
				if (appliedTownBuff)
				{
					basedCharaParamParams.townBuffPercentageParam = EditUtility.GetTownBuffIncreaseRate(cdc);
				}
				else
				{
					basedCharaParamParams.townBuffPercentageParam = default(EditUtility.OutputPercentageParam);
				}
				return basedCharaParamParams;
			}
			}
			return null;
		}

		// Token: 0x06000D32 RID: 3378 RVA: 0x00049DDC File Offset: 0x000481DC
		public static EditUtility.OutputCharaParam CalcCharaParamLevelOnly(CharacterDataController cdc)
		{
			return EditUtility.CalcCharaParamLevelOnly(cdc.GetCharaId(), cdc.GetCharaLv());
		}

		// Token: 0x06000D33 RID: 3379 RVA: 0x00049DEF File Offset: 0x000481EF
		public static EditUtility.OutputCharaParam CalcCharaParamLevelOnly(long charaMngID)
		{
			return EditUtility.CalcCharaParamLevelOnly(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID));
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x00049E06 File Offset: 0x00048206
		public static EditUtility.OutputCharaParam CalcCharaParamLevelOnly(UserCharacterData userCharacterData)
		{
			return EditUtility.CalcCharaParamLevelOnly(userCharacterData.Param.CharaID, userCharacterData.Param.Lv);
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x00049E23 File Offset: 0x00048223
		public static EditUtility.OutputCharaParam CalcCharaParamLevelOnly(UserSupportData userSupportData)
		{
			return EditUtility.CalcCharaParamLevelOnly(userSupportData.CharaID, userSupportData.Lv);
		}

		// Token: 0x06000D36 RID: 3382 RVA: 0x00049E38 File Offset: 0x00048238
		public static EditUtility.OutputCharaParam CalcCharaParamLevelOnly(int charaID, int currentLv)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			EditUtility.OutputCharaParam result = default(EditUtility.OutputCharaParam);
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			result.Lv = currentLv;
			CharacterParamGrowthListDB charaParamGrowthListDB = dbMng.CharaParamGrowthListDB;
			result.Hp = charaParamGrowthListDB.CalcHp(param.m_InitHp, currentLv, param.m_GrowthTableID);
			result.Atk = charaParamGrowthListDB.CalcAtk(param.m_InitAtk, currentLv, param.m_GrowthTableID);
			result.Mgc = charaParamGrowthListDB.CalcMgc(param.m_InitMgc, currentLv, param.m_GrowthTableID);
			result.Def = charaParamGrowthListDB.CalcDef(param.m_InitDef, currentLv, param.m_GrowthTableID);
			result.MDef = charaParamGrowthListDB.CalcMDef(param.m_InitMDef, currentLv, param.m_GrowthTableID);
			result.Spd = charaParamGrowthListDB.CalcSpd(param.m_InitSpd, currentLv, param.m_GrowthTableID);
			result.Luck = charaParamGrowthListDB.CalcLuck(param.m_InitLuck, currentLv, param.m_GrowthTableID);
			return result;
		}

		// Token: 0x06000D37 RID: 3383 RVA: 0x00049F3C File Offset: 0x0004833C
		public static int CalcParamCorrect(int val, float correct)
		{
			float num = 1f + correct * 0.01f;
			return (int)Mathf.Ceil((float)val * num);
		}

		// Token: 0x06000D38 RID: 3384 RVA: 0x00049F64 File Offset: 0x00048364
		public static int CalcParamCorrectLv(int initVal, int maxVal, int initLv, int maxLv, int currentLv)
		{
			float num = 1f;
			if (maxLv > 1)
			{
				num = 1f - (float)(maxLv - currentLv) / (float)(maxLv - initLv);
			}
			return initVal + (int)Mathf.Ceil((float)(maxVal - initVal) * num);
		}

		// Token: 0x06000D39 RID: 3385 RVA: 0x00049F9F File Offset: 0x0004839F
		public static EditUtility.OutputPercentageParam GetFriendshipIncreaseRate(CharacterDataController partyMember)
		{
			return EditUtility.GetFriendshipIncreaseRate(partyMember.GetCharaId(), partyMember.GetFriendship());
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x00049FB4 File Offset: 0x000483B4
		public static EditUtility.OutputPercentageParam GetFriendshipIncreaseRate(int charaID, int friendShip)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			EditUtility.OutputPercentageParam result = default(EditUtility.OutputPercentageParam);
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			NamedListDB_Param param2 = dbMng.NamedListDB.GetParam((eCharaNamedType)param.m_NamedType);
			NamedFriendshipExpDB_Param param3 = dbMng.NamedFriendshipExpDB.GetParam(friendShip, param2.m_FriendshipTableID);
			result.Hp = param3.m_CorrectHp;
			result.Atk = param3.m_CorrectAtk;
			result.Def = param3.m_CorrectDef;
			result.MDef = param3.m_CorrectMDef;
			result.Mgc = param3.m_CorrectMgc;
			result.Spd = param3.m_CorrectSpd;
			result.Luck = param3.m_CorrectLuck;
			return result;
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x0004A06C File Offset: 0x0004846C
		public static EditUtility.OutputPercentageParam GetTownBuffIncreaseRate(CharacterDataController partyMember)
		{
			return EditUtility.GetTownBuffIncreaseRate(partyMember.GetCharaId());
		}

		// Token: 0x06000D3C RID: 3388 RVA: 0x0004A07C File Offset: 0x0004847C
		public static EditUtility.OutputPercentageParam GetTownBuffIncreaseRate(int charaID)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
			NamedListDB_Param param2 = dbMng.NamedListDB.GetParam((eCharaNamedType)param.m_NamedType);
			float[] statusBuff = TownBuff.CalcTownBuff().GetStatusBuff((eTitleType)param2.m_TitleType, (eClassType)param.m_Class, (eElementType)param.m_Element);
			return new EditUtility.OutputPercentageParam
			{
				Hp = statusBuff[0],
				Atk = statusBuff[1],
				Def = statusBuff[3],
				MDef = statusBuff[4],
				Mgc = statusBuff[2],
				Spd = statusBuff[5]
			};
		}

		// Token: 0x06000D3D RID: 3389 RVA: 0x0004A11C File Offset: 0x0004851C
		public static bool IsFriendShipMax(int charaID, int friendShip)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			return EditUtility.IsFriendShipMax((eCharaNamedType)dbMng.CharaListDB.GetParam(charaID).m_NamedType, friendShip);
		}

		// Token: 0x06000D3E RID: 3390 RVA: 0x0004A14E File Offset: 0x0004854E
		public static bool IsFriendShipMax(UserNamedData userNamedData)
		{
			return EditUtility.IsFriendShipMax(userNamedData.NamedType, userNamedData.FriendShip);
		}

		// Token: 0x06000D3F RID: 3391 RVA: 0x0004A161 File Offset: 0x00048561
		public static bool IsFriendshipMax(EquipWeaponCharacterDataController partyMember)
		{
			return EditUtility.IsFriendShipMax(partyMember.GetCharaNamedType(), partyMember.GetFriendship());
		}

		// Token: 0x06000D40 RID: 3392 RVA: 0x0004A174 File Offset: 0x00048574
		public static bool IsFriendShipMax(eCharaNamedType namedType, int friendShip)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			sbyte friendshipTableID = dbMng.NamedListDB.GetFriendshipTableID(namedType);
			return EditUtility.IsFriendshipMax(friendshipTableID, friendShip);
		}

		// Token: 0x06000D41 RID: 3393 RVA: 0x0004A1A0 File Offset: 0x000485A0
		public static bool IsFriendshipMax(sbyte friendShipTableID, int friendShip)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			int friendShipMax = dbMng.NamedFriendshipExpDB.GetFriendShipMax(friendShipTableID);
			return friendShip >= friendShipMax;
		}

		// Token: 0x06000D42 RID: 3394 RVA: 0x0004A1D0 File Offset: 0x000485D0
		public static void SimulateFriendship(eCharaNamedType namedType, int maxFriendship, int currentFriendship, long currentExp, long addExp, out int afterFriendship, out long afterExp, out long[] nextMaxExps)
		{
			NamedFriendshipExpDB namedFriendshipExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB;
			NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(namedType);
			afterFriendship = currentFriendship;
			afterExp = currentExp;
			nextMaxExps = null;
			int friendship = currentFriendship;
			long num = currentExp + addExp;
			while (addExp > 0L)
			{
				int nextExp = namedFriendshipExpDB.GetNextExp(friendship, param.m_FriendshipTableID);
				if (num < (long)nextExp)
				{
					afterExp = num;
					break;
				}
				afterFriendship = Mathf.Clamp(afterFriendship + 1, 1, maxFriendship);
				num -= (long)nextExp;
				friendship = afterFriendship;
			}
			if (maxFriendship <= afterFriendship)
			{
				afterExp = 0L;
			}
			int num2 = afterFriendship - currentFriendship + 1;
			nextMaxExps = new long[num2];
			for (int i = 0; i < num2; i++)
			{
				nextMaxExps[i] = (long)namedFriendshipExpDB.GetNextExp(currentFriendship + i, param.m_FriendshipTableID);
			}
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x0004A2C0 File Offset: 0x000486C0
		public static List<long> GetHaveCharaList(eCharaNamedType namedType)
		{
			List<long> list = new List<long>();
			List<UserCharacterData> userCharaDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas;
			for (int i = 0; i < userCharaDatas.Count; i++)
			{
				CharacterParam param = userCharaDatas[i].Param;
				if (param.NamedType == namedType)
				{
					list.Add(param.MngID);
				}
			}
			return list;
		}

		// Token: 0x06000D44 RID: 3396 RVA: 0x0004A320 File Offset: 0x00048720
		public static int GetHaveNamedToCharaID(eCharaNamedType namedType)
		{
			int num = -1;
			int num2 = 0;
			List<UserCharacterData> userCharaDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas;
			for (int i = 0; i < userCharaDatas.Count; i++)
			{
				if (userCharaDatas[i].Param.NamedType == namedType && (userCharaDatas[i].Param.RareType > (eRare)num2 || (userCharaDatas[i].Param.RareType == (eRare)num2 && (num == -1 || num > userCharaDatas[i].Param.CharaID))))
				{
					num = userCharaDatas[i].Param.CharaID;
					num2 = (int)userCharaDatas[i].Param.RareType;
				}
			}
			return num;
		}

		// Token: 0x06000D45 RID: 3397 RVA: 0x0004A3E0 File Offset: 0x000487E0
		public static List<int> GetHaveNamedToCharaIDList(eCharaNamedType namedType)
		{
			List<long> haveCharaList = EditUtility.GetHaveCharaList(namedType);
			List<int> list = new List<int>();
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < haveCharaList.Count; i++)
			{
				list.Add(userDataMng.GetUserCharaData(haveCharaList[i]).Param.CharaID);
			}
			list.Sort();
			return list;
		}

		// Token: 0x06000D46 RID: 3398 RVA: 0x0004A440 File Offset: 0x00048840
		public static UserCharacterData GetManagedPartyUserCharacterData(EquipWeaponManagedPartyMemberController managedPartyMember)
		{
			int partyIndex = managedPartyMember.GetPartyIndex();
			int partySlotIndex = managedPartyMember.GetPartySlotIndex();
			if (managedPartyMember.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedBattleParty)
			{
				return EditUtility.GetBattlePartyUserCharacterData(partyIndex, partySlotIndex);
			}
			if (managedPartyMember.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedSupportParty)
			{
				return EditUtility.GetSupportPartyUserCharacterData(partyIndex, partySlotIndex);
			}
			return null;
		}

		// Token: 0x06000D47 RID: 3399 RVA: 0x0004A484 File Offset: 0x00048884
		public static UserWeaponData GetManagedPartyUserWeaponData(EquipWeaponManagedPartyMemberController managedPartyMember)
		{
			int partyIndex = managedPartyMember.GetPartyIndex();
			int partySlotIndex = managedPartyMember.GetPartySlotIndex();
			if (managedPartyMember.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedBattleParty)
			{
				return EditUtility.GetBattlePartyUserWeaponData(partyIndex, partySlotIndex);
			}
			if (managedPartyMember.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedSupportParty)
			{
				return EditUtility.GetSupportPartyUserWeaponData(partyIndex, partySlotIndex);
			}
			return null;
		}

		// Token: 0x06000D48 RID: 3400 RVA: 0x0004A4C8 File Offset: 0x000488C8
		public static UserCharacterData GetBattlePartyUserCharacterData(int partyIndex, int partySlotIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData battlePartyUserBattlePartyData = EditUtility.GetBattlePartyUserBattlePartyData(partyIndex);
			if (battlePartyUserBattlePartyData == null)
			{
				Debug.LogError("Invalid BattlePartyData");
				return null;
			}
			long memberAt = battlePartyUserBattlePartyData.GetMemberAt(partySlotIndex);
			return userDataMng.GetUserCharaData(memberAt);
		}

		// Token: 0x06000D49 RID: 3401 RVA: 0x0004A508 File Offset: 0x00048908
		public static UserWeaponData GetBattlePartyUserWeaponData(int partyIndex, int partySlotIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData battlePartyUserBattlePartyData = EditUtility.GetBattlePartyUserBattlePartyData(partyIndex);
			if (battlePartyUserBattlePartyData == null)
			{
				Debug.LogError("Invalid BattlePartyData");
				return null;
			}
			long weaponAt = battlePartyUserBattlePartyData.GetWeaponAt(partySlotIndex);
			return userDataMng.GetUserWpnData(weaponAt);
		}

		// Token: 0x06000D4A RID: 3402 RVA: 0x0004A548 File Offset: 0x00048948
		public static UserBattlePartyData GetBattlePartyUserBattlePartyData(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			if (userDataMng == null)
			{
				Debug.LogError("Invalid UserDataMng");
				return null;
			}
			if (userDataMng.UserBattlePartyDatas == null)
			{
				Debug.LogError("Invalid UserDataMng");
				return null;
			}
			if (partyIndex < 0)
			{
				Debug.LogError("Invalid PartyIndex");
				return null;
			}
			if (userDataMng.UserBattlePartyDatas.Count <= partyIndex)
			{
				Debug.LogError("Invalid PartyIndex");
				return null;
			}
			return userDataMng.UserBattlePartyDatas[partyIndex];
		}

		// Token: 0x06000D4B RID: 3403 RVA: 0x0004A5C8 File Offset: 0x000489C8
		public static bool IsEmptyBattleParty(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			List<long> availableMemberMngIDs = userBattlePartyData.GetAvailableMemberMngIDs();
			return availableMemberMngIDs.Count == 0;
		}

		// Token: 0x06000D4C RID: 3404 RVA: 0x0004A604 File Offset: 0x00048A04
		public static bool IsAvailableParty(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			if (userBattlePartyData.GetMemberAt(0) == -1L && userBattlePartyData.GetMemberAt(1) == -1L && userBattlePartyData.GetMemberAt(2) == -1L)
			{
				return false;
			}
			int num = EditUtility.CalcPartyTotalCostAt(partyIndex);
			return num <= userDataMng.UserData.PartyCost;
		}

		// Token: 0x06000D4D RID: 3405 RVA: 0x0004A670 File Offset: 0x00048A70
		public static bool CanBeRemoveFromParty(int partyIndex, int memberIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			return userBattlePartyData.GetMemberAt(memberIndex) != -1L;
		}

		// Token: 0x06000D4E RID: 3406 RVA: 0x0004A6A8 File Offset: 0x00048AA8
		public static bool IsSameNamedOnChange(int partyIndex, int memberIndex, long changeCharaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(changeCharaMngID);
			if (userCharaData == null)
			{
				return false;
			}
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			int slotNum = userBattlePartyData.GetSlotNum();
			for (int i = 0; i < slotNum; i++)
			{
				if (i != memberIndex)
				{
					long memberAt = userBattlePartyData.GetMemberAt(i);
					if (memberAt != -1L && userDataMng.GetUserCharaData(memberAt).Param.NamedType == userCharaData.Param.NamedType)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000D4F RID: 3407 RVA: 0x0004A73C File Offset: 0x00048B3C
		public static void AssignPartyMember(int partyIndex, int memberIndex, long assignCharaMngID)
		{
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			int memberIdx = userBattlePartyData.GetMemberIdx(assignCharaMngID);
			if (memberIdx == -1)
			{
				userBattlePartyData.SetMemberAt(memberIndex, assignCharaMngID);
				long weaponAt = userBattlePartyData.GetWeaponAt(memberIndex);
				if (!EditUtility.CanEquipWeapon(weaponAt, assignCharaMngID))
				{
					userBattlePartyData.EmptyWeaponAt(memberIndex);
				}
			}
			else
			{
				long weaponAt2 = userBattlePartyData.GetWeaponAt(memberIdx);
				long memberAt = userBattlePartyData.GetMemberAt(memberIndex);
				long weaponAt3 = userBattlePartyData.GetWeaponAt(memberIndex);
				userBattlePartyData.SetMemberAt(memberIndex, assignCharaMngID);
				userBattlePartyData.SetMemberAt(memberIdx, memberAt);
				userBattlePartyData.SetWeaponAt(memberIndex, weaponAt2);
				userBattlePartyData.SetWeaponAt(memberIdx, weaponAt3);
			}
		}

		// Token: 0x06000D50 RID: 3408 RVA: 0x0004A7D8 File Offset: 0x00048BD8
		public static void RemovePartyMember(int partyIndex, int memberIndex)
		{
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			userBattlePartyData.EmptyMemberAt(memberIndex);
			userBattlePartyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D51 RID: 3409 RVA: 0x0004A80C File Offset: 0x00048C0C
		public static void AssignPartyWeapon(int partyIndex, int memberIndex, long assignWeaponMngID)
		{
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			userBattlePartyData.SetWeaponAt(memberIndex, assignWeaponMngID);
		}

		// Token: 0x06000D52 RID: 3410 RVA: 0x0004A837 File Offset: 0x00048C37
		public static void AssignPartyWeapon(UserBattlePartyData partyData, int memberIndex, long assignWeaponMngID)
		{
			partyData.SetWeaponAt(memberIndex, assignWeaponMngID);
		}

		// Token: 0x06000D53 RID: 3411 RVA: 0x0004A844 File Offset: 0x00048C44
		public static void RemovePartyWeapon(int partyIndex, int memberIndex)
		{
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			userBattlePartyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D54 RID: 3412 RVA: 0x0004A86E File Offset: 0x00048C6E
		public static void RemovePartyWeapon(UserBattlePartyData partyData, int memberIndex)
		{
			partyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x0004A878 File Offset: 0x00048C78
		public static int CheckPartyChara(int partyIndex, long charaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			for (int i = 0; i < userBattlePartyData.CharaMngIDs.Length; i++)
			{
				if (userBattlePartyData.CharaMngIDs[i] == charaMngID)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x0004A8C8 File Offset: 0x00048CC8
		public static bool CheckPartyContainTitle(int partyIndex, eTitleType title)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			if (userBattlePartyData == null)
			{
				return false;
			}
			CharacterListDB charaListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB;
			NamedListDB namedListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB;
			List<long> availableMemberMngIDs = userBattlePartyData.GetAvailableMemberMngIDs();
			for (int i = 0; i < availableMemberMngIDs.Count; i++)
			{
				int charaID = userDataMng.GetUserCharaData(availableMemberMngIDs[i]).Param.CharaID;
				if (title == namedListDB.GetTitleType((eCharaNamedType)charaListDB.GetParam(charaID).m_NamedType))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D57 RID: 3415 RVA: 0x0004A974 File Offset: 0x00048D74
		public static bool CheckPartyContainWeapon(int partyIndex, long weaponMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			if (userBattlePartyData == null)
			{
				return false;
			}
			int slotNum = userBattlePartyData.GetSlotNum();
			for (int i = 0; i < slotNum; i++)
			{
				long weaponAt = userBattlePartyData.GetWeaponAt(i);
				if (weaponMngID == weaponAt)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D58 RID: 3416 RVA: 0x0004A9D0 File Offset: 0x00048DD0
		public static bool CheckPartyContainWeapon(long weaponMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < userDataMng.UserBattlePartyDatas.Count; i++)
			{
				bool flag = EditUtility.CheckPartyContainWeapon(i, weaponMngID);
				if (flag)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D59 RID: 3417 RVA: 0x0004AA15 File Offset: 0x00048E15
		public static List<EditUtility.EquipWeaponCharacterReferenceData> GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas()
		{
			return EditUtility.GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(SingletonMonoBehaviour<GameSystem>.Inst.UISettings);
		}

		// Token: 0x06000D5A RID: 3418 RVA: 0x0004AA28 File Offset: 0x00048E28
		public static List<EditUtility.EquipWeaponCharacterReferenceData> GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(UISettings UISettings)
		{
			UISettings uisettings = SingletonMonoBehaviour<GameSystem>.Inst.UISettings;
			return EditUtility.GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Class), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Rare), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Element), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Title), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.StatusPriority));
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x0004AA70 File Offset: 0x00048E70
		public static List<EditUtility.EquipWeaponCharacterReferenceData> GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles, bool[] bPriorityStates)
		{
			EditUtility.eState priorityState = EditUtility.eState.Hp;
			bool[] filterFlagsWithAll = SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.StatusPriority);
			if (filterFlagsWithAll != null)
			{
				for (int i = 0; i < filterFlagsWithAll.Length; i++)
				{
					if (filterFlagsWithAll[i])
					{
						priorityState = (EditUtility.eState)i;
						break;
					}
				}
			}
			return EditUtility.GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(bClassTypes, bRareTypes, bElementTypes, bTitles, priorityState);
		}

		// Token: 0x06000D5C RID: 3420 RVA: 0x0004AAC4 File Offset: 0x00048EC4
		public static List<EditUtility.EquipWeaponCharacterReferenceData> GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles, EditUtility.eState priorityState)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			List<UserCharacterData> userCharacterData = EditUtility.FilterUserCharacterDatas(bClassTypes, bRareTypes, bElementTypes, bTitles);
			List<UserCharacterData>[] array = EditUtility.SplitUserCharacterDataForClass(userCharacterData);
			List<KeyValuePair<UserCharacterData, float>>[] array2 = new List<KeyValuePair<UserCharacterData, float>>[5];
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i] = EditUtility.SortUserCharacterDataCostParStateParameter(array[i], priorityState);
			}
			List<UserWeaponData>[] array3 = EditUtility.SplitUserWeaponDataForClassFromUserData();
			List<KeyValuePair<UserWeaponData, float>>[] array4 = new List<KeyValuePair<UserWeaponData, float>>[5];
			for (int j = 0; j < array4.Length; j++)
			{
				array4[j] = EditUtility.SortUserWeaponDataCostParStateParameter(array3[j], priorityState);
			}
			int num = 0;
			List<EditUtility.EquipWeaponCharacterReferenceData> list = new List<EditUtility.EquipWeaponCharacterReferenceData>();
			List<KeyValuePair<UserCharacterData, float>> list2 = new List<KeyValuePair<UserCharacterData, float>>();
			List<KeyValuePair<UserWeaponData, float>> list3 = new List<KeyValuePair<UserWeaponData, float>>();
			for (int k = 0; k < 5; k++)
			{
				int num2 = -1;
				int num3 = -1;
				for (int l = 0; l < 5; l++)
				{
					if (0 < array2[l].Count)
					{
						if (num + array2[l][0].Key.Param.Cost < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.PartyCost)
						{
							int num4 = -1;
							if (array4[l] != null)
							{
								for (int m = 0; m < array4[l].Count; m++)
								{
									if (num + array2[l][0].Key.Param.Cost + array4[l][m].Key.Param.Cost < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.PartyCost)
									{
										float value = array4[l][m].Value;
										float num5 = -1f;
										if (0 <= num4)
										{
											num5 = array4[l][num4].Value;
										}
										if (num5 < value)
										{
											num4 = m;
										}
									}
								}
							}
							float num6 = 0f;
							if (0 <= num4)
							{
								num6 += array4[l][num4].Value;
							}
							num6 += array2[l][0].Value;
							float num7 = 0f;
							if (0 <= num3)
							{
								num7 += array4[num2][num3].Value;
							}
							if (0 <= num2)
							{
								num7 += array2[num2][0].Value;
							}
							if (num7 < num6)
							{
								num2 = l;
								num3 = num4;
							}
						}
						else
						{
							array2[l].RemoveAt(0);
							l--;
						}
					}
				}
				if (0 <= num2)
				{
					long weaponMngId = -1L;
					if (0 <= num3)
					{
						weaponMngId = array4[num2][num3].Key.MngID;
					}
					list.Add(new EditUtility.EquipWeaponCharacterReferenceData
					{
						m_CharaMngId = array2[num2][0].Key.MngID,
						m_WeaponMngId = weaponMngId
					});
					num += array2[num2][0].Key.Param.Cost;
					list2.Add(array2[num2][0]);
					if (0 <= num3)
					{
						num += array4[num2][num3].Key.Param.Cost;
						list3.Add(array4[num2][num3]);
						array4[num2].RemoveAt(num3);
					}
					eCharaNamedType namedType = array2[num2][0].Key.Param.NamedType;
					for (int n = 0; n < 5; n++)
					{
						for (int num8 = 0; num8 < array2[n].Count; num8++)
						{
							if (array2[n][num8].Key.Param.NamedType == namedType)
							{
								array2[n].RemoveAt(num8);
								num8--;
							}
						}
					}
				}
			}
			return list;
		}

		// Token: 0x06000D5D RID: 3421 RVA: 0x0004AEE4 File Offset: 0x000492E4
		public static List<UserCharacterData>[] SplitUserCharacterDataForClass(List<UserCharacterData> userCharacterData)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			List<UserCharacterData>[] array = new List<UserCharacterData>[5];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new List<UserCharacterData>();
			}
			NamedListDB namedListDB = dbMng.NamedListDB;
			for (int j = 0; j < userCharacterData.Count; j++)
			{
				array[(int)userCharacterData[j].Param.ClassType].Add(userCharacterData[j]);
			}
			return array;
		}

		// Token: 0x06000D5E RID: 3422 RVA: 0x0004AF64 File Offset: 0x00049364
		public static List<UserCharacterData> FilterUserCharacterDatas(UISettings UISettings)
		{
			UISettings uisettings = SingletonMonoBehaviour<GameSystem>.Inst.UISettings;
			return EditUtility.FilterUserCharacterDatas(uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Class), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Rare), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Element), uisettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.Recommend, UISettings.eFilterCategory.Title));
		}

		// Token: 0x06000D5F RID: 3423 RVA: 0x0004AFA1 File Offset: 0x000493A1
		public static List<UserCharacterData> FilterUserCharacterDatas(bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles)
		{
			return EditUtility.FilterUserCharacterDatas(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas, bClassTypes, bRareTypes, bElementTypes, bTitles);
		}

		// Token: 0x06000D60 RID: 3424 RVA: 0x0004AFBC File Offset: 0x000493BC
		public static List<UserCharacterData> FilterUserCharacterDatas(List<UserCharacterData> userCharacterData, bool[] bClassTypes, bool[] bRareTypes, bool[] bElementTypes, bool[] bTitles)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (bClassTypes.Length < 5)
			{
				return null;
			}
			if (bRareTypes.Length < 5)
			{
				return null;
			}
			if (bElementTypes.Length < 6)
			{
				return null;
			}
			int num = new eTitleTypeConverter().Setup().HowManyTitles();
			if (bTitles.Length < num)
			{
				return null;
			}
			List<UserCharacterData> userCharaDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas;
			List<UserCharacterData> list = new List<UserCharacterData>();
			NamedListDB namedListDB = dbMng.NamedListDB;
			for (int i = 0; i < userCharaDatas.Count; i++)
			{
				if (bRareTypes[(int)userCharaDatas[i].Param.RareType])
				{
					if (bElementTypes[(int)userCharaDatas[i].Param.ElementType])
					{
						if (bTitles[namedListDB.GetParam(userCharaDatas[i].Param.NamedType).m_TitleType])
						{
							if (bClassTypes[(int)userCharaDatas[i].Param.ClassType])
							{
								list.Add(userCharaDatas[i]);
							}
						}
					}
				}
			}
			return list;
		}

		// Token: 0x06000D61 RID: 3425 RVA: 0x0004B0E4 File Offset: 0x000494E4
		public static List<KeyValuePair<UserCharacterData, float>> SortUserCharacterDataCostParStateParameter(List<UserCharacterData> ucds, EditUtility.eState priorityState)
		{
			List<KeyValuePair<UserCharacterData, float>> list = new List<KeyValuePair<UserCharacterData, float>>();
			for (int i = 0; i < ucds.Count; i++)
			{
				list.Add(new KeyValuePair<UserCharacterData, float>(ucds[i], (float)EditUtility.GetUserCharacterDataStateParameter(ucds[i], priorityState)));
			}
			for (int j = 0; j < list.Count; j++)
			{
				for (int k = j + 1; k < list.Count; k++)
				{
					if (list[j].Value < list[k].Value)
					{
						KeyValuePair<UserCharacterData, float> value = list[j];
						list[j] = list[k];
						list[k] = value;
					}
				}
			}
			return list;
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x0004B1A4 File Offset: 0x000495A4
		public static List<KeyValuePair<UserWeaponData, float>> SortUserWeaponDataCostParStateParameter(List<UserWeaponData> uwds, EditUtility.eState priorityState)
		{
			List<KeyValuePair<UserWeaponData, float>> list = new List<KeyValuePair<UserWeaponData, float>>();
			for (int i = 0; i < uwds.Count; i++)
			{
				list.Add(new KeyValuePair<UserWeaponData, float>(uwds[i], (float)EditUtility.GetUserCharacterDataStateParameter(uwds[i], priorityState)));
			}
			for (int j = 0; j < list.Count; j++)
			{
				for (int k = j + 1; k < list.Count; k++)
				{
					if (list[j].Value < list[k].Value)
					{
						KeyValuePair<UserWeaponData, float> value = list[j];
						list[j] = list[k];
						list[k] = value;
					}
				}
			}
			return list;
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x0004B264 File Offset: 0x00049664
		private static int GetUserCharacterDataStateParameter(UserCharacterData userCharacterData, EditUtility.eState state)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharacterData.Param.NamedType);
			UserWeaponData userWeaponData = null;
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(userCharacterData, userNamedData, userWeaponData);
			switch (state)
			{
			case EditUtility.eState.Hp:
				return outputCharaParam.Hp;
			case EditUtility.eState.Atk:
				return outputCharaParam.Atk;
			case EditUtility.eState.Mgc:
				return outputCharaParam.Mgc;
			case EditUtility.eState.Def:
				return outputCharaParam.Def;
			case EditUtility.eState.MDef:
				return outputCharaParam.MDef;
			case EditUtility.eState.Spd:
				return outputCharaParam.Spd;
			default:
				return -1;
			}
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x0004B2F0 File Offset: 0x000496F0
		private static int GetUserCharacterDataStateParameter(UserWeaponData userWeaponData, EditUtility.eState state)
		{
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcWeaponParam(userWeaponData);
			switch (state)
			{
			case EditUtility.eState.Hp:
				return outputCharaParam.Hp;
			case EditUtility.eState.Atk:
				return outputCharaParam.Atk;
			case EditUtility.eState.Mgc:
				return outputCharaParam.Mgc;
			case EditUtility.eState.Def:
				return outputCharaParam.Def;
			case EditUtility.eState.MDef:
				return outputCharaParam.MDef;
			case EditUtility.eState.Spd:
				return outputCharaParam.Spd;
			default:
				return -1;
			}
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x0004B358 File Offset: 0x00049758
		public static int CheckSupportPartyChara(int partyIndex, long charaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData userSupportPartyData = userDataMng.UserSupportPartyDatas[partyIndex];
			for (int i = 0; i < userSupportPartyData.CharaMngIDs.Count; i++)
			{
				if (userSupportPartyData.CharaMngIDs[i] == charaMngID)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000D66 RID: 3430 RVA: 0x0004B3B0 File Offset: 0x000497B0
		public static UserCharacterData GetSupportPartyUserCharacterData(int partyIndex, int partySlotIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData supportPartyUserSupportPartyData = EditUtility.GetSupportPartyUserSupportPartyData(partyIndex);
			if (supportPartyUserSupportPartyData == null)
			{
				Debug.LogError("Invalid SupportPartyData");
				return null;
			}
			long memberAt = supportPartyUserSupportPartyData.GetMemberAt(partySlotIndex);
			return userDataMng.GetUserCharaData(memberAt);
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x0004B3F0 File Offset: 0x000497F0
		public static UserWeaponData GetSupportPartyUserWeaponData(int partyIndex, int partySlotIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData supportPartyUserSupportPartyData = EditUtility.GetSupportPartyUserSupportPartyData(partyIndex);
			if (supportPartyUserSupportPartyData == null)
			{
				Debug.LogError("Invalid SupportPartyData");
				return null;
			}
			long weaponAt = supportPartyUserSupportPartyData.GetWeaponAt(partySlotIndex);
			return userDataMng.GetUserWpnData(weaponAt);
		}

		// Token: 0x06000D68 RID: 3432 RVA: 0x0004B430 File Offset: 0x00049830
		public static UserSupportPartyData GetSupportPartyUserSupportPartyData(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			if (userDataMng == null)
			{
				Debug.LogError("Invalid UserDataMng");
				return null;
			}
			if (userDataMng.UserSupportPartyDatas == null)
			{
				Debug.LogError("Invalid UserDataMng");
				return null;
			}
			if (partyIndex < 0)
			{
				Debug.LogError("Invalid PartyIndex");
				return null;
			}
			if (userDataMng.UserSupportPartyDatas.Count <= partyIndex)
			{
				Debug.LogError("Invalid PartyIndex");
				return null;
			}
			return userDataMng.UserSupportPartyDatas[partyIndex];
		}

		// Token: 0x06000D69 RID: 3433 RVA: 0x0004B4B0 File Offset: 0x000498B0
		public static bool IsEmptySupportParty(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData userSupportPartyData = userDataMng.UserSupportPartyDatas[partyIndex];
			for (int i = 0; i < userDataMng.UserData.SupportLimit; i++)
			{
				if (userSupportPartyData.GetMemberAt(i) != -1L)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000D6A RID: 3434 RVA: 0x0004B504 File Offset: 0x00049904
		public static void ActivateSupportParty(int partyIndex)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			for (int i = 0; i < userDataMng.UserSupportPartyDatas.Count; i++)
			{
				if (userDataMng.UserSupportPartyDatas[i] != null)
				{
					userDataMng.UserSupportPartyDatas[i].IsActive = (i == partyIndex);
				}
			}
		}

		// Token: 0x06000D6B RID: 3435 RVA: 0x0004B560 File Offset: 0x00049960
		public static bool CanBeAssignToSupportParty(int partyIndex, int memberIndex, long assignCharaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData userSupportPartyData = userDataMng.UserSupportPartyDatas[partyIndex];
			if (userSupportPartyData == null)
			{
				return false;
			}
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(assignCharaMngID);
			if (userCharaData == null)
			{
				return false;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			eTitleType titleType = (eTitleType)dbMng.NamedListDB.GetParam(userCharaData.Param.NamedType).m_TitleType;
			for (int i = 0; i < userSupportPartyData.CharaMngIDs.Count; i++)
			{
				if (i != memberIndex)
				{
					UserCharacterData userCharaData2 = userDataMng.GetUserCharaData(userSupportPartyData.CharaMngIDs[i]);
					if (userCharaData2 != null)
					{
						if (dbMng.NamedListDB.GetParam(userCharaData2.Param.NamedType).m_TitleType == (int)titleType)
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		// Token: 0x06000D6C RID: 3436 RVA: 0x0004B644 File Offset: 0x00049A44
		public static void AssignSupportPartyMember(int partyIndex, int memberIndex, long assignCharaMngID)
		{
			UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
			userSupportPartyData.SetMemberAt(memberIndex, assignCharaMngID);
			long weaponAt = userSupportPartyData.GetWeaponAt(memberIndex);
			if (!EditUtility.CanEquipWeapon(weaponAt, assignCharaMngID))
			{
				userSupportPartyData.EmptyWeaponAt(memberIndex);
			}
		}

		// Token: 0x06000D6D RID: 3437 RVA: 0x0004B68C File Offset: 0x00049A8C
		public static void RemoveSupportPartyMember(int partyIndex, int memberIndex)
		{
			UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
			userSupportPartyData.EmptyMemberAt(memberIndex);
			userSupportPartyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x0004B6C0 File Offset: 0x00049AC0
		public static void AssignSupportPartyWeapon(int partyIndex, int memberIndex, long assignWeaponMngID)
		{
			UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
			userSupportPartyData.SetWeaponAt(memberIndex, assignWeaponMngID);
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x0004B6EB File Offset: 0x00049AEB
		public static void AssignSupportPartyWeapon(UserSupportPartyData supportPartyData, int memberIndex, long assignWeaponMngID)
		{
			supportPartyData.SetWeaponAt(memberIndex, assignWeaponMngID);
		}

		// Token: 0x06000D70 RID: 3440 RVA: 0x0004B6F8 File Offset: 0x00049AF8
		public static void RemoveSupportPartyWeapon(int partyIndex, int memberIndex)
		{
			UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
			userSupportPartyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D71 RID: 3441 RVA: 0x0004B722 File Offset: 0x00049B22
		public static void RemoveSupportPartyWeapon(UserSupportPartyData supportPartyData, int memberIndex)
		{
			supportPartyData.EmptyWeaponAt(memberIndex);
		}

		// Token: 0x06000D72 RID: 3442 RVA: 0x0004B72C File Offset: 0x00049B2C
		public static int CalcSkillLvFromTotalUseNum(int maxLv, long totalUseNum, sbyte expTableID)
		{
			SkillExpDB skillExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB;
			int num = 1;
			long num2 = totalUseNum;
			while (num2 > 0L)
			{
				int nextExp = skillExpDB.GetNextExp(num, expTableID);
				if (num2 >= (long)nextExp)
				{
					num2 -= (long)nextExp;
					num++;
				}
				else
				{
					num2 = 0L;
				}
			}
			if (num > maxLv)
			{
				num = maxLv;
			}
			return num;
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x0004B788 File Offset: 0x00049B88
		public static int GetSkillNowRemainExp(int currentLv, long totalUseNum, sbyte expTableID)
		{
			SkillExpDB skillExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB;
			long[] nextMaxExps = skillExpDB.GetNextMaxExps(1, currentLv, expTableID);
			if (nextMaxExps == null)
			{
				Debug.LogWarning("exps is null.");
				return 0;
			}
			long num = 0L;
			for (int i = 0; i < nextMaxExps.Length; i++)
			{
				num += nextMaxExps[i];
			}
			return (int)(num - totalUseNum);
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x0004B7E4 File Offset: 0x00049BE4
		public static bool CanUpgrade(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			return userCharaData.Param.Lv < userCharaData.Param.MaxLv;
		}

		// Token: 0x06000D75 RID: 3445 RVA: 0x0004B820 File Offset: 0x00049C20
		public static List<ItemListDB_Param> GetUpgradeItemParams(eElementType elementType)
		{
			List<ItemListDB_Param> list = new List<ItemListDB_Param>();
			ItemListDB itemListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB;
			for (int i = 0; i < itemListDB.m_Params.Length; i++)
			{
				ItemListDB_Param item = itemListDB.m_Params[i];
				if (item.m_Type == 0 && item.m_TypeArgs[0] == (int)elementType)
				{
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x06000D76 RID: 3446 RVA: 0x0004B894 File Offset: 0x00049C94
		public static EditUtility.SimulateUpgradeParam SimulateUpgrade(long charaMngID, SelectItemPanel.UseItemInfos itemInfos)
		{
			EditUtility.SimulateUpgradeParam simulateUpgradeParam = new EditUtility.SimulateUpgradeParam();
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			simulateUpgradeParam.nowUserCharaData = userCharaData;
			EditUtility.SimulateUpgrade(simulateUpgradeParam.nowUserCharaData, itemInfos, out simulateUpgradeParam.amount, out simulateUpgradeParam.exp, out simulateUpgradeParam.before, out simulateUpgradeParam.after);
			return simulateUpgradeParam;
		}

		// Token: 0x06000D77 RID: 3447 RVA: 0x0004B8E7 File Offset: 0x00049CE7
		public static bool SimulateUpgrade(long charaMngID, SelectItemPanel selectItem, out int out_amount, out long out_exp, out EditUtility.OutputCharaParam out_beforeParam, out EditUtility.OutputCharaParam out_afterParam)
		{
			return EditUtility.SimulateUpgrade(charaMngID, selectItem.GetUseItemInfos(), out out_amount, out out_exp, out out_beforeParam, out out_afterParam);
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x0004B8FC File Offset: 0x00049CFC
		public static bool SimulateUpgrade(long charaMngID, SelectItemPanel.UseItemInfos itemInfos, out int out_amount, out long out_exp, out EditUtility.OutputCharaParam out_beforeParam, out EditUtility.OutputCharaParam out_afterParam)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			return EditUtility.SimulateUpgrade(userCharaData, itemInfos, out out_amount, out out_exp, out out_beforeParam, out out_afterParam);
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x0004B92C File Offset: 0x00049D2C
		public static bool SimulateUpgrade(UserCharacterData userCharaData, SelectItemPanel.UseItemInfos itemInfos, out int out_amount, out long out_exp, out EditUtility.OutputCharaParam out_beforeParam, out EditUtility.OutputCharaParam out_afterParam)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharaData.Param.NamedType);
			UserWeaponData userWeaponData = null;
			float value = dbMng.StarDefineDB.GetValue(eStarDefineDB.UpgradeSameElementBonus);
			out_exp = 0L;
			out_amount = 0;
			out_beforeParam = default(EditUtility.OutputCharaParam);
			out_afterParam = default(EditUtility.OutputCharaParam);
			if (itemInfos != null)
			{
				ItemListDB itemListDB = dbMng.ItemListDB;
				CharacterExpDB charaExpDB = dbMng.CharaExpDB;
				for (int i = 0; i < itemInfos.HowManyAdapters(); i++)
				{
					SelectItemPanel.UseItemInfo info = itemInfos.GetInfo(i);
					if (info.GetId() != -1)
					{
						ItemListDB_Param param = itemListDB.GetParam(info.GetId());
						float num = (userCharaData.Param.ElementType != (eElementType)param.m_TypeArgs[0]) ? 1f : value;
						out_exp += (long)((ulong)((uint)((float)(param.m_TypeArgs[1] * info.GetNum()) * num)));
						int upgradeAmount = charaExpDB.GetUpgradeAmount(userCharaData.Param.Lv);
						out_amount += upgradeAmount * info.GetNum();
					}
				}
			}
			out_beforeParam.Lv = userCharaData.Param.Lv;
			out_beforeParam.NowExp = EditUtility.GetCharaNowExp(out_beforeParam.Lv, userCharaData.Param.Exp);
			out_beforeParam.NextMaxExp = EditUtility.GetCharaNextMaxExp(out_beforeParam.Lv);
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParamIgnoreTownBuff(userCharaData.Param.CharaID, out_beforeParam.Lv, userCharaData.Param.Exp, userNamedData, userWeaponData);
			out_beforeParam.Hp = outputCharaParam.Hp;
			out_beforeParam.Atk = outputCharaParam.Atk;
			out_beforeParam.Mgc = outputCharaParam.Mgc;
			out_beforeParam.Def = outputCharaParam.Def;
			out_beforeParam.MDef = outputCharaParam.MDef;
			out_beforeParam.Spd = outputCharaParam.Spd;
			out_beforeParam.Luck = outputCharaParam.Luck;
			int lv;
			long exp;
			long num2;
			EditUtility.SimulateCharaLv(userCharaData.Param.CharaID, userCharaData.Param.MaxLv, userCharaData.Param.Exp, out_exp, out lv, out exp, out num2);
			out_exp = num2;
			out_afterParam.Lv = lv;
			out_afterParam.NowExp = EditUtility.GetCharaNowExp(out_afterParam.Lv, exp);
			out_afterParam.NextMaxExp = EditUtility.GetCharaNextMaxExp(lv);
			EditUtility.OutputCharaParam outputCharaParam2 = EditUtility.CalcCharaParamIgnoreTownBuff(userCharaData.Param.CharaID, out_afterParam.Lv, exp, userNamedData, userWeaponData);
			out_afterParam.Hp = outputCharaParam2.Hp;
			out_afterParam.Atk = outputCharaParam2.Atk;
			out_afterParam.Mgc = outputCharaParam2.Mgc;
			out_afterParam.Def = outputCharaParam2.Def;
			out_afterParam.MDef = outputCharaParam2.MDef;
			out_afterParam.Spd = outputCharaParam2.Spd;
			out_afterParam.Luck = outputCharaParam2.Luck;
			return true;
		}

		// Token: 0x06000D7A RID: 3450 RVA: 0x0004BBF4 File Offset: 0x00049FF4
		public static bool IsMaxLimitBreak(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
			int num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID).m_CharaMaxLvUps.Length;
			return userCharaData.Param.LimitBreak >= num;
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x0004BC6C File Offset: 0x0004A06C
		public static bool CanLimitBreak(long charaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
			CharacterLimitBreakListDB_Param param2 = dbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			int num = param2.m_CharaMaxLvUps.Length;
			if (userCharaData.Param.LimitBreak >= num)
			{
				return false;
			}
			int limitBreak = userCharaData.Param.LimitBreak;
			if (userDataMng.UserData.IsShortOfGold((long)param2.m_Amounts[limitBreak]))
			{
				return false;
			}
			int num2 = param2.m_ItemIDs_1[limitBreak];
			if (num2 != -1 && param2.m_ItemNums_1[limitBreak] > 0 && param2.m_ItemNums_1[limitBreak] <= userDataMng.GetItemNum(num2))
			{
				return true;
			}
			num2 = param2.m_ItemIDs_2[limitBreak];
			if (num2 != -1 && param2.m_ItemNums_2[limitBreak] > 0 && param2.m_ItemNums_2[limitBreak] <= userDataMng.GetItemNum(num2))
			{
				return true;
			}
			num2 = param2.m_ItemIDs_3[limitBreak];
			if (num2 != -1 && param2.m_ItemNums_3[limitBreak] > 0 && param2.m_ItemNums_3[limitBreak] <= userDataMng.GetItemNum(num2))
			{
				return true;
			}
			eTitleType titleType = dbMng.NamedListDB.GetTitleType(userCharaData.Param.NamedType);
			num2 = dbMng.TitleListDB.GetParam(titleType).m_LimitBreakItemID;
			return num2 != -1 && param2.m_TitleItemNums[limitBreak] > 0 && param2.m_TitleItemNums[limitBreak] <= userDataMng.GetItemNum(num2);
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x0004BE24 File Offset: 0x0004A224
		public static EditUtility.SimulateLimitBreakParam SimulateLimitBreak(long charaMngID)
		{
			EditUtility.SimulateLimitBreakParam simulateLimitBreakParam = new EditUtility.SimulateLimitBreakParam();
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			simulateLimitBreakParam.nowUserCharaData = userCharaData;
			EditUtility.SimulateLimitBreak(simulateLimitBreakParam.nowUserCharaData, out simulateLimitBreakParam.beforeMaxLv, out simulateLimitBreakParam.afterMaxLv, out simulateLimitBreakParam.beforeUniqueSkillMaxLv, out simulateLimitBreakParam.afterUniqueSkillMaxLv, out simulateLimitBreakParam.beforeClassSkillMaxLvs, out simulateLimitBreakParam.afterClassSkillMaxLvs);
			return simulateLimitBreakParam;
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x0004BE84 File Offset: 0x0004A284
		public static bool SimulateLimitBreak(long charaMngID, out int out_beforeMaxLv, out int out_afterMaxLv, out int out_beforeUniqueSkillMaxLv, out int out_afterUniqueSkillMaxLv, out int[] out_beforeClassSkillMaxLvs, out int[] out_afterClassSkillMaxLvs)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			return EditUtility.SimulateLimitBreak(userCharaData, out out_beforeMaxLv, out out_afterMaxLv, out out_beforeUniqueSkillMaxLv, out out_afterUniqueSkillMaxLv, out out_beforeClassSkillMaxLvs, out out_afterClassSkillMaxLvs);
		}

		// Token: 0x06000D7E RID: 3454 RVA: 0x0004BEB4 File Offset: 0x0004A2B4
		public static bool SimulateLimitBreak(UserCharacterData userCharacterData, out int out_beforeMaxLv, out int out_afterMaxLv, out int out_beforeUniqueSkillMaxLv, out int out_afterUniqueSkillMaxLv, out int[] out_beforeClassSkillMaxLvs, out int[] out_afterClassSkillMaxLvs)
		{
			if (userCharacterData == null)
			{
				out_beforeMaxLv = -1;
				out_afterMaxLv = -1;
				out_beforeUniqueSkillMaxLv = -1;
				out_afterUniqueSkillMaxLv = -1;
				out_beforeClassSkillMaxLvs = null;
				out_afterClassSkillMaxLvs = null;
				return false;
			}
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharacterData.Param.CharaID);
			CharacterLimitBreakListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			if (userCharacterData.Param.LimitBreak >= param2.m_CharaMaxLvUps.Length)
			{
				out_beforeMaxLv = -1;
				out_afterMaxLv = -1;
				out_beforeUniqueSkillMaxLv = -1;
				out_afterUniqueSkillMaxLv = -1;
				out_beforeClassSkillMaxLvs = null;
				out_afterClassSkillMaxLvs = null;
				return false;
			}
			out_beforeMaxLv = userCharacterData.Param.MaxLv;
			out_afterMaxLv = userCharacterData.Param.MaxLv + param2.m_CharaMaxLvUps[userCharacterData.Param.LimitBreak];
			int num = param2.m_SkillMaxLvUps[userCharacterData.Param.LimitBreak];
			out_beforeUniqueSkillMaxLv = userCharacterData.Param.UniqueSkillLearnData.SkillMaxLv;
			out_afterUniqueSkillMaxLv = userCharacterData.Param.UniqueSkillLearnData.SkillMaxLv + num;
			out_beforeClassSkillMaxLvs = new int[userCharacterData.Param.ClassSkillLearnDatas.Count];
			for (int i = 0; i < out_beforeClassSkillMaxLvs.Length; i++)
			{
				out_beforeClassSkillMaxLvs[i] = userCharacterData.Param.ClassSkillLearnDatas[i].SkillMaxLv;
			}
			out_afterClassSkillMaxLvs = new int[userCharacterData.Param.ClassSkillLearnDatas.Count];
			for (int j = 0; j < out_afterClassSkillMaxLvs.Length; j++)
			{
				out_afterClassSkillMaxLvs[j] = userCharacterData.Param.ClassSkillLearnDatas[j].SkillMaxLv + num;
			}
			return true;
		}

		// Token: 0x06000D7F RID: 3455 RVA: 0x0004C050 File Offset: 0x0004A450
		public static int CalcTotalLvUpValuByLimitBreak(int charaID)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			CharacterLimitBreakListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			int num = 0;
			for (int i = 0; i < param2.m_CharaMaxLvUps.Length; i++)
			{
				num += param2.m_CharaMaxLvUps[i];
			}
			return num;
		}

		// Token: 0x06000D80 RID: 3456 RVA: 0x0004C0B8 File Offset: 0x0004A4B8
		public static int CalcTotalSkillLvUpValuByLimitBreak(int charaID)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			CharacterLimitBreakListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			int num = 0;
			for (int i = 0; i < param2.m_SkillMaxLvUps.Length; i++)
			{
				num += param2.m_SkillMaxLvUps[i];
			}
			return num;
		}

		// Token: 0x06000D81 RID: 3457 RVA: 0x0004C120 File Offset: 0x0004A520
		public static int GetSkillMaxLv(int charaID, int lb)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			CharacterLimitBreakListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			int num = 0;
			for (int i = 0; i < lb; i++)
			{
				num += param2.m_SkillMaxLvUps[i];
			}
			return param.m_SkillLimitLv + num;
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x0004C188 File Offset: 0x0004A588
		public static bool IsExistEvolution(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			return userCharaData != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaEvolutionListDB.IsExistParam(userCharaData.Param.CharaID);
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x0004C1D0 File Offset: 0x0004A5D0
		public static bool IsHaveEvolutionChara(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			if (userCharaData == null)
			{
				return false;
			}
			CharacterEvolutionListDB_Param characterEvolutionListDB_Param;
			if (!SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaEvolutionListDB.GetParamBySrcCharaID(userCharaData.Param.CharaID, out characterEvolutionListDB_Param))
			{
				return false;
			}
			List<UserCharacterData> userCharaDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas;
			for (int i = 0; i < userCharaDatas.Count; i++)
			{
				if (userCharaDatas[i].Param.CharaID == characterEvolutionListDB_Param.m_DestCharaID)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D84 RID: 3460 RVA: 0x0004C268 File Offset: 0x0004A668
		public static bool CanEvolutionListup(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			return userCharaData != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaEvolutionListDB.IsExistParam(userCharaData.Param.CharaID);
		}

		// Token: 0x06000D85 RID: 3461 RVA: 0x0004C2B8 File Offset: 0x0004A6B8
		public static bool CanEvolution(long charaMngID, bool isSpecify)
		{
			List<EditUtility.eEvolutionFailureReason> out_reasons = new List<EditUtility.eEvolutionFailureReason>();
			return EditUtility.CanEvolution(charaMngID, isSpecify, out_reasons);
		}

		// Token: 0x06000D86 RID: 3462 RVA: 0x0004C2D4 File Offset: 0x0004A6D4
		public static bool CanEvolution(long charaMngID, bool isSpecify, List<EditUtility.eEvolutionFailureReason> out_reasons)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			if (userCharaData == null)
			{
				return false;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (!dbMng.CharaEvolutionListDB.IsExistParam(userCharaData.Param.CharaID))
			{
				out_reasons.Add(EditUtility.eEvolutionFailureReason.RecipeNotFound);
			}
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
			int num = dbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID).m_CharaMaxLvUps.Length;
			if (userCharaData.Param.LimitBreak < num)
			{
				out_reasons.Add(EditUtility.eEvolutionFailureReason.LimitBreakFail);
			}
			if (userCharaData.Param.Lv < userCharaData.Param.MaxLv)
			{
				out_reasons.Add(EditUtility.eEvolutionFailureReason.CharaLvFail);
			}
			CharacterEvolutionListDB_Param characterEvolutionListDB_Param;
			bool flag;
			if (isSpecify)
			{
				flag = dbMng.CharaEvolutionListDB.GetSpecifyMaterialParamBySrcCharaID(userCharaData.Param.CharaID, out characterEvolutionListDB_Param);
			}
			else
			{
				flag = dbMng.CharaEvolutionListDB.GetNormalMaterialParamBySrcCharaID(userCharaData.Param.CharaID, out characterEvolutionListDB_Param);
			}
			if (flag)
			{
				for (int i = 0; i < characterEvolutionListDB_Param.m_ItemIDs.Length; i++)
				{
					if (characterEvolutionListDB_Param.m_ItemIDs[i] != -1 && userDataMng.GetItemNum(characterEvolutionListDB_Param.m_ItemIDs[i]) < characterEvolutionListDB_Param.m_ItemNums[i])
					{
						out_reasons.Add(EditUtility.eEvolutionFailureReason.MaterialFail);
						break;
					}
				}
				if (userDataMng.UserData.IsShortOfGold((long)characterEvolutionListDB_Param.m_Amount))
				{
					out_reasons.Add(EditUtility.eEvolutionFailureReason.AmountFail);
				}
			}
			return out_reasons.Count <= 0;
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x0004C468 File Offset: 0x0004A868
		public static EditUtility.SimulateEvolutionParam SimulateEvolution(long charaMngID)
		{
			EditUtility.SimulateEvolutionParam simulateEvolutionParam = new EditUtility.SimulateEvolutionParam();
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			simulateEvolutionParam.nowUserCharaData = userDataMng.GetUserCharaData(charaMngID);
			if (simulateEvolutionParam.nowUserCharaData == null)
			{
				return null;
			}
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(simulateEvolutionParam.nowUserCharaData.Param.CharaID);
			CharacterEvolutionListDB_Param characterEvolutionListDB_Param;
			dbMng.CharaEvolutionListDB.GetParamBySrcCharaID(simulateEvolutionParam.nowUserCharaData.Param.CharaID, out characterEvolutionListDB_Param);
			simulateEvolutionParam.afterCharaId = characterEvolutionListDB_Param.m_DestCharaID;
			CharacterListDB_Param param2 = dbMng.CharaListDB.GetParam(characterEvolutionListDB_Param.m_DestCharaID);
			simulateEvolutionParam.before = EditUtility.CalcCharaParamIgnoreTownBuff(param.m_CharaID, simulateEvolutionParam.nowUserCharaData.Param.Lv, simulateEvolutionParam.nowUserCharaData.Param.Exp, null, null);
			simulateEvolutionParam.after = EditUtility.CalcCharaParamIgnoreTownBuff(param2.m_CharaID, simulateEvolutionParam.nowUserCharaData.Param.Lv, simulateEvolutionParam.nowUserCharaData.Param.Exp, null, null);
			simulateEvolutionParam.afterMaxLv = param2.m_InitLimitLv;
			simulateEvolutionParam.afterLimitBreak = 0;
			return simulateEvolutionParam;
		}

		// Token: 0x06000D88 RID: 3464 RVA: 0x0004C584 File Offset: 0x0004A984
		public static string GetEvolutionFailString(long charaMngID, bool isSpecify)
		{
			List<EditUtility.eEvolutionFailureReason> list = new List<EditUtility.eEvolutionFailureReason>();
			EditUtility.CanEvolution(charaMngID, isSpecify, list);
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < list.Count; i++)
			{
				if (i > 0)
				{
					stringBuilder.Append("\n");
				}
				switch (list[i])
				{
				case EditUtility.eEvolutionFailureReason.RecipeNotFound:
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedRecipeNotFound));
					break;
				case EditUtility.eEvolutionFailureReason.LimitBreakFail:
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedLimitBreak));
					break;
				case EditUtility.eEvolutionFailureReason.CharaLvFail:
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedCharaLv));
					break;
				case EditUtility.eEvolutionFailureReason.MaterialFail:
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedMaterial));
					break;
				case EditUtility.eEvolutionFailureReason.AmountFail:
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailedAmount));
					break;
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D89 RID: 3465 RVA: 0x0004C69D File Offset: 0x0004AA9D
		public static List<UserWeaponData>[] SplitUserWeaponDataForClassFromUserData()
		{
			return EditUtility.SplitUserWeaponDataForClass(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas);
		}

		// Token: 0x06000D8A RID: 3466 RVA: 0x0004C6B4 File Offset: 0x0004AAB4
		public static List<UserWeaponData>[] SplitUserWeaponDataForClass(List<UserWeaponData> userWeaponData)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			List<UserWeaponData>[] array = new List<UserWeaponData>[5];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = new List<UserWeaponData>();
			}
			NamedListDB namedListDB = dbMng.NamedListDB;
			for (int j = 0; j < userWeaponData.Count; j++)
			{
				int classType = dbMng.WeaponListDB.GetParam(userWeaponData[j].Param.WeaponID).m_ClassType;
				array[classType].Add(userWeaponData[j]);
			}
			return array;
		}

		// Token: 0x06000D8B RID: 3467 RVA: 0x0004C74B File Offset: 0x0004AB4B
		public static List<UserWeaponData> GetUserWeaponDataRefineClassFromUserData(eClassType classType)
		{
			return EditUtility.GetUserWeaponDataRefineClass(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas, classType);
		}

		// Token: 0x06000D8C RID: 3468 RVA: 0x0004C762 File Offset: 0x0004AB62
		public static List<UserWeaponData> GetUserWeaponDataRefineClass(List<UserWeaponData> userWeaponDatas, eClassType classType)
		{
			return EditUtility.SplitUserWeaponDataForClass(userWeaponDatas)[(int)classType];
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x0004C76C File Offset: 0x0004AB6C
		public static List<ItemListDB_Param> GetWeaponUpgradeItemParams()
		{
			List<ItemListDB_Param> list = new List<ItemListDB_Param>();
			ItemListDB itemListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB;
			for (int i = 0; i < itemListDB.m_Params.Length; i++)
			{
				ItemListDB_Param item = itemListDB.m_Params[i];
				if (item.m_Type == 3)
				{
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x0004C7D0 File Offset: 0x0004ABD0
		public static EditUtility.eWeaponCreateResult CanCreateWeapon(int weaponID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			WeaponRecipeListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.GetParam(weaponID);
			if ((long)param.m_Amount > userDataMng.UserData.Gold)
			{
				return EditUtility.eWeaponCreateResult.NotEnoughMoney;
			}
			for (int i = 0; i < param.m_ItemIDs.Length; i++)
			{
				int itemNum = userDataMng.GetItemNum(param.m_ItemIDs[i]);
				if (param.m_ItemNums[i] > itemNum)
				{
					return EditUtility.eWeaponCreateResult.NotEnoughItem;
				}
			}
			return EditUtility.eWeaponCreateResult.OK;
		}

		// Token: 0x06000D8F RID: 3471 RVA: 0x0004C854 File Offset: 0x0004AC54
		public static int CalcWeaponNum(int weaponID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			int num = 0;
			int count = userDataMng.UserWeaponDatas.Count;
			for (int i = 0; i < count; i++)
			{
				if (userDataMng.UserWeaponDatas[i].Param.WeaponID == weaponID)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06000D90 RID: 3472 RVA: 0x0004C8B0 File Offset: 0x0004ACB0
		public static bool CanEquipWeapon(long weaponMngID, long charaMngID)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponMngID);
			if (userWpnData == null)
			{
				return false;
			}
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			return userCharaData != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID).m_ClassType == (int)userCharaData.Param.ClassType;
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x0004C924 File Offset: 0x0004AD24
		public static bool CheckDefaultWeapon(long weaponMngID)
		{
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(weaponMngID);
			return EditUtility.CheckDefaultWeaponNaked(userWpnData.Param.WeaponID);
		}

		// Token: 0x06000D92 RID: 3474 RVA: 0x0004C954 File Offset: 0x0004AD54
		public static bool CheckDefaultWeaponNaked(int weaponID)
		{
			ClassListDB classListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ClassListDB;
			for (int i = 0; i < classListDB.m_Params.Length; i++)
			{
				if (classListDB.m_Params[i].m_DefaultWeaponID == weaponID)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D93 RID: 3475 RVA: 0x0004C9A4 File Offset: 0x0004ADA4
		public static bool IsMaxLevelWeapon(long weaponMngID)
		{
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(weaponMngID);
			return userWpnData.Param.Lv >= userWpnData.Param.MaxLv;
		}

		// Token: 0x06000D94 RID: 3476 RVA: 0x0004C9E0 File Offset: 0x0004ADE0
		public static bool SimulateWeaponUpgrade(long weaponMngID, SelectItemPanel selectItem, out int out_amount, out long out_exp, out EditUtility.OutputCharaParam out_beforeParam, out EditUtility.OutputCharaParam out_afterParam)
		{
			out_exp = 0L;
			out_amount = 0;
			out_beforeParam = default(EditUtility.OutputCharaParam);
			out_afterParam = default(EditUtility.OutputCharaParam);
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponMngID);
			if (userWpnData == null)
			{
				return false;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			WeaponExpDB weaponExpDB = dbMng.WeaponExpDB;
			WeaponListDB_Param param = dbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID);
			float value = dbMng.StarDefineDB.GetValue(eStarDefineDB.UpgradeWeaponMaterialBonus);
			if (selectItem != null)
			{
				ItemListDB itemListDB = dbMng.ItemListDB;
				for (int i = 0; i < selectItem.GetItemsLimit(); i++)
				{
					int useItemId = selectItem.GetUseItemId(i);
					if (useItemId != -1)
					{
						int useItemNumIndex = selectItem.GetUseItemNumIndex(i);
						ItemListDB_Param param2 = itemListDB.GetParam(useItemId);
						bool flag = dbMng.WeaponListDB.IsBonusItem(param.m_ID, useItemId);
						float num = (!flag) ? 1f : value;
						out_exp += (long)((ulong)((uint)((float)(param2.m_TypeArgs[0] * useItemNumIndex) * num)));
						int upgradeAmount = weaponExpDB.GetUpgradeAmount(userWpnData.Param.Lv, param.m_ExpTableID);
						out_amount += upgradeAmount * useItemNumIndex;
					}
				}
			}
			out_beforeParam.Lv = userWpnData.Param.Lv;
			out_beforeParam.NowExp = EditUtility.GetWeaponNowExp(userWpnData.Param.WeaponID, out_beforeParam.Lv, userWpnData.Param.Exp);
			out_beforeParam.NextMaxExp = (long)weaponExpDB.GetNextExp(out_beforeParam.Lv, param.m_ExpTableID);
			out_beforeParam.Hp = 0;
			out_beforeParam.Atk = userWpnData.Param.Atk;
			out_beforeParam.Mgc = userWpnData.Param.Mgc;
			out_beforeParam.Def = userWpnData.Param.Def;
			out_beforeParam.MDef = userWpnData.Param.MDef;
			out_beforeParam.Spd = 0;
			out_beforeParam.Luck = 0;
			int num2;
			long exp;
			long num3;
			EditUtility.SimulateWeaponLv(userWpnData.Param.WeaponID, userWpnData.Param.MaxLv, userWpnData.Param.Exp, out_exp, out num2, out exp, out num3);
			out_exp = num3;
			out_afterParam.Lv = num2;
			out_afterParam.NowExp = EditUtility.GetWeaponNowExp(userWpnData.Param.WeaponID, out_afterParam.Lv, exp);
			out_afterParam.NextMaxExp = (long)weaponExpDB.GetNextExp(num2, param.m_ExpTableID);
			out_afterParam.Hp = 0;
			out_afterParam.Atk = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, num2);
			out_afterParam.Mgc = EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, num2);
			out_afterParam.Def = EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, num2);
			out_afterParam.MDef = EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, num2);
			out_afterParam.Spd = 0;
			out_afterParam.Luck = 0;
			return true;
		}

		// Token: 0x06000D95 RID: 3477 RVA: 0x0004CCEC File Offset: 0x0004B0EC
		public static void SimulateWeaponLv(int weaponID, int maxLv, long currentExp, long addExp, out int out_afterLv, out long out_afterExp, out long out_getExp)
		{
			sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_ExpTableID;
			WeaponExpDB weaponExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB;
			long num = currentExp + addExp;
			out_afterLv = 1;
			out_afterExp = 0L;
			while (num > 0L && out_afterLv < maxLv)
			{
				int nextExp = weaponExpDB.GetNextExp(out_afterLv, expTableID);
				if (nextExp <= 0)
				{
					break;
				}
				if (num < (long)nextExp)
				{
					out_afterExp += num;
					num = 0L;
				}
				else
				{
					out_afterLv++;
					long num2 = (long)nextExp;
					if (num2 > num)
					{
						num2 = num;
					}
					out_afterExp += num2;
					num -= num2;
				}
			}
			out_getExp = out_afterExp - currentExp;
		}

		// Token: 0x06000D96 RID: 3478 RVA: 0x0004CDAC File Offset: 0x0004B1AC
		public static long GetWeaponNowExp(int weaponID, int currentLv, long exp)
		{
			WeaponExpDB weaponExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB;
			sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_ExpTableID;
			int num = 0;
			for (int i = 1; i < currentLv; i++)
			{
				num += weaponExpDB.GetNextExp(i, expTableID);
			}
			if (exp >= (long)num)
			{
				return exp - (long)num;
			}
			return 0L;
		}

		// Token: 0x06000D97 RID: 3479 RVA: 0x0004CE1C File Offset: 0x0004B21C
		public static EditUtility.OutputCharaParam CalcWeaponParam(EquipWeaponCharacterDataController partyMember)
		{
			if (partyMember == null)
			{
				return default(EditUtility.OutputCharaParam);
			}
			return partyMember.GetWeaponParam();
		}

		// Token: 0x06000D98 RID: 3480 RVA: 0x0004CE40 File Offset: 0x0004B240
		public static EditUtility.OutputCharaParam CalcWeaponParam(int weaponID, int weaponLv)
		{
			EditUtility.OutputCharaParam result = default(EditUtility.OutputCharaParam);
			if (weaponID != -1 && !EditUtility.CheckDefaultWeaponNaked(weaponID))
			{
				WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID);
				result.Atk += EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, weaponLv);
				result.Mgc += EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, weaponLv);
				result.Def += EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, weaponLv);
				result.MDef += EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, weaponLv);
			}
			return result;
		}

		// Token: 0x06000D99 RID: 3481 RVA: 0x0004CF28 File Offset: 0x0004B328
		public static EditUtility.OutputCharaParam CalcWeaponParam(UserWeaponData userWeaponData)
		{
			EditUtility.OutputCharaParam result = default(EditUtility.OutputCharaParam);
			if (userWeaponData != null)
			{
				result.Atk += userWeaponData.Param.Atk;
				result.Mgc += userWeaponData.Param.Mgc;
				result.Def += userWeaponData.Param.Def;
				result.MDef += userWeaponData.Param.MDef;
			}
			return result;
		}

		// Token: 0x020002BB RID: 699
		public enum eState
		{
			// Token: 0x0400154D RID: 5453
			Hp,
			// Token: 0x0400154E RID: 5454
			Atk,
			// Token: 0x0400154F RID: 5455
			Mgc,
			// Token: 0x04001550 RID: 5456
			Def,
			// Token: 0x04001551 RID: 5457
			MDef,
			// Token: 0x04001552 RID: 5458
			Spd,
			// Token: 0x04001553 RID: 5459
			Num
		}

		// Token: 0x020002BC RID: 700
		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct OutputCharaParam
		{
			// Token: 0x17000141 RID: 321
			// (get) Token: 0x06000D9A RID: 3482 RVA: 0x0004CFA8 File Offset: 0x0004B3A8
			// (set) Token: 0x06000D9B RID: 3483 RVA: 0x0004CFB0 File Offset: 0x0004B3B0
			public int Lv { get; set; }

			// Token: 0x17000142 RID: 322
			// (get) Token: 0x06000D9C RID: 3484 RVA: 0x0004CFB9 File Offset: 0x0004B3B9
			// (set) Token: 0x06000D9D RID: 3485 RVA: 0x0004CFC1 File Offset: 0x0004B3C1
			public long NowExp { get; set; }

			// Token: 0x17000143 RID: 323
			// (get) Token: 0x06000D9E RID: 3486 RVA: 0x0004CFCA File Offset: 0x0004B3CA
			// (set) Token: 0x06000D9F RID: 3487 RVA: 0x0004CFD2 File Offset: 0x0004B3D2
			public long NextMaxExp { get; set; }

			// Token: 0x17000144 RID: 324
			// (get) Token: 0x06000DA0 RID: 3488 RVA: 0x0004CFDB File Offset: 0x0004B3DB
			// (set) Token: 0x06000DA1 RID: 3489 RVA: 0x0004CFE3 File Offset: 0x0004B3E3
			public int Hp { get; set; }

			// Token: 0x17000145 RID: 325
			// (get) Token: 0x06000DA2 RID: 3490 RVA: 0x0004CFEC File Offset: 0x0004B3EC
			// (set) Token: 0x06000DA3 RID: 3491 RVA: 0x0004CFF4 File Offset: 0x0004B3F4
			public int Atk { get; set; }

			// Token: 0x17000146 RID: 326
			// (get) Token: 0x06000DA4 RID: 3492 RVA: 0x0004CFFD File Offset: 0x0004B3FD
			// (set) Token: 0x06000DA5 RID: 3493 RVA: 0x0004D005 File Offset: 0x0004B405
			public int Mgc { get; set; }

			// Token: 0x17000147 RID: 327
			// (get) Token: 0x06000DA6 RID: 3494 RVA: 0x0004D00E File Offset: 0x0004B40E
			// (set) Token: 0x06000DA7 RID: 3495 RVA: 0x0004D016 File Offset: 0x0004B416
			public int Def { get; set; }

			// Token: 0x17000148 RID: 328
			// (get) Token: 0x06000DA8 RID: 3496 RVA: 0x0004D01F File Offset: 0x0004B41F
			// (set) Token: 0x06000DA9 RID: 3497 RVA: 0x0004D027 File Offset: 0x0004B427
			public int MDef { get; set; }

			// Token: 0x17000149 RID: 329
			// (get) Token: 0x06000DAA RID: 3498 RVA: 0x0004D030 File Offset: 0x0004B430
			// (set) Token: 0x06000DAB RID: 3499 RVA: 0x0004D038 File Offset: 0x0004B438
			public int Spd { get; set; }

			// Token: 0x1700014A RID: 330
			// (get) Token: 0x06000DAC RID: 3500 RVA: 0x0004D041 File Offset: 0x0004B441
			// (set) Token: 0x06000DAD RID: 3501 RVA: 0x0004D049 File Offset: 0x0004B449
			public int Luck { get; set; }

			// Token: 0x06000DAE RID: 3502 RVA: 0x0004D052 File Offset: 0x0004B452
			public int[] ToIntsForCharaStatus()
			{
				return new int[]
				{
					this.Hp,
					this.Atk,
					this.Mgc,
					this.Def,
					this.MDef,
					this.Spd
				};
			}
		}

		// Token: 0x020002BD RID: 701
		[StructLayout(LayoutKind.Sequential, Size = 1)]
		public struct OutputPercentageParam
		{
			// Token: 0x1700014B RID: 331
			// (get) Token: 0x06000DAF RID: 3503 RVA: 0x0004D090 File Offset: 0x0004B490
			// (set) Token: 0x06000DB0 RID: 3504 RVA: 0x0004D098 File Offset: 0x0004B498
			public float Hp { get; set; }

			// Token: 0x1700014C RID: 332
			// (get) Token: 0x06000DB1 RID: 3505 RVA: 0x0004D0A1 File Offset: 0x0004B4A1
			// (set) Token: 0x06000DB2 RID: 3506 RVA: 0x0004D0A9 File Offset: 0x0004B4A9
			public float Atk { get; set; }

			// Token: 0x1700014D RID: 333
			// (get) Token: 0x06000DB3 RID: 3507 RVA: 0x0004D0B2 File Offset: 0x0004B4B2
			// (set) Token: 0x06000DB4 RID: 3508 RVA: 0x0004D0BA File Offset: 0x0004B4BA
			public float Mgc { get; set; }

			// Token: 0x1700014E RID: 334
			// (get) Token: 0x06000DB5 RID: 3509 RVA: 0x0004D0C3 File Offset: 0x0004B4C3
			// (set) Token: 0x06000DB6 RID: 3510 RVA: 0x0004D0CB File Offset: 0x0004B4CB
			public float Def { get; set; }

			// Token: 0x1700014F RID: 335
			// (get) Token: 0x06000DB7 RID: 3511 RVA: 0x0004D0D4 File Offset: 0x0004B4D4
			// (set) Token: 0x06000DB8 RID: 3512 RVA: 0x0004D0DC File Offset: 0x0004B4DC
			public float MDef { get; set; }

			// Token: 0x17000150 RID: 336
			// (get) Token: 0x06000DB9 RID: 3513 RVA: 0x0004D0E5 File Offset: 0x0004B4E5
			// (set) Token: 0x06000DBA RID: 3514 RVA: 0x0004D0ED File Offset: 0x0004B4ED
			public float Spd { get; set; }

			// Token: 0x17000151 RID: 337
			// (get) Token: 0x06000DBB RID: 3515 RVA: 0x0004D0F6 File Offset: 0x0004B4F6
			// (set) Token: 0x06000DBC RID: 3516 RVA: 0x0004D0FE File Offset: 0x0004B4FE
			public float Luck { get; set; }

			// Token: 0x06000DBD RID: 3517 RVA: 0x0004D107 File Offset: 0x0004B507
			public float[] ToFloatsForCharaStatusPercent()
			{
				return new float[]
				{
					this.Hp,
					this.Atk,
					this.Mgc,
					this.Def,
					this.MDef,
					this.Spd
				};
			}
		}

		// Token: 0x020002BE RID: 702
		public class SimulateParam
		{
		}

		// Token: 0x020002BF RID: 703
		public class SimulateCharaLevelParamBase : EditUtility.SimulateParam
		{
			// Token: 0x04001565 RID: 5477
			public int afterLv;

			// Token: 0x04001566 RID: 5478
			public long afterExp;
		}

		// Token: 0x020002C0 RID: 704
		public class SimulateCharaLevelParam : EditUtility.SimulateCharaLevelParamBase
		{
		}

		// Token: 0x020002C1 RID: 705
		public class SimulateAddExpParam : EditUtility.SimulateCharaLevelParamBase
		{
			// Token: 0x04001567 RID: 5479
			public long getExp;
		}

		// Token: 0x020002C2 RID: 706
		private class BasedCharaParamParams
		{
			// Token: 0x04001568 RID: 5480
			public EditUtility.OutputCharaParam lvBaseCharaParam;

			// Token: 0x04001569 RID: 5481
			public EditUtility.OutputCharaParam weaponParam;

			// Token: 0x0400156A RID: 5482
			public EditUtility.OutputPercentageParam friendShipPercentageParam;

			// Token: 0x0400156B RID: 5483
			public EditUtility.OutputPercentageParam townBuffPercentageParam;
		}

		// Token: 0x020002C3 RID: 707
		public class EquipWeaponCharacterReferenceData
		{
			// Token: 0x0400156C RID: 5484
			public long m_CharaMngId;

			// Token: 0x0400156D RID: 5485
			public long m_WeaponMngId;
		}

		// Token: 0x020002C4 RID: 708
		public class CharacterEditSimulateParam : EditUtility.SimulateParam
		{
			// Token: 0x0400156E RID: 5486
			public UserCharacterData nowUserCharaData;
		}

		// Token: 0x020002C5 RID: 709
		public class HaveBeforeAfterCharaParamSimulateParam : EditUtility.CharacterEditSimulateParam
		{
			// Token: 0x0400156F RID: 5487
			public EditUtility.OutputCharaParam before;

			// Token: 0x04001570 RID: 5488
			public EditUtility.OutputCharaParam after;
		}

		// Token: 0x020002C6 RID: 710
		public class SimulateUpgradeParam : EditUtility.HaveBeforeAfterCharaParamSimulateParam
		{
			// Token: 0x04001571 RID: 5489
			public int amount;

			// Token: 0x04001572 RID: 5490
			public long exp;
		}

		// Token: 0x020002C7 RID: 711
		public class SimulateLimitBreakParam : EditUtility.CharacterEditSimulateParam
		{
			// Token: 0x04001573 RID: 5491
			public int beforeMaxLv;

			// Token: 0x04001574 RID: 5492
			public int afterMaxLv;

			// Token: 0x04001575 RID: 5493
			public int beforeUniqueSkillMaxLv;

			// Token: 0x04001576 RID: 5494
			public int afterUniqueSkillMaxLv;

			// Token: 0x04001577 RID: 5495
			public int[] beforeClassSkillMaxLvs;

			// Token: 0x04001578 RID: 5496
			public int[] afterClassSkillMaxLvs;
		}

		// Token: 0x020002C8 RID: 712
		public enum eEvolutionFailureReason
		{
			// Token: 0x0400157A RID: 5498
			RecipeNotFound,
			// Token: 0x0400157B RID: 5499
			LimitBreakFail,
			// Token: 0x0400157C RID: 5500
			CharaLvFail,
			// Token: 0x0400157D RID: 5501
			MaterialFail,
			// Token: 0x0400157E RID: 5502
			AmountFail
		}

		// Token: 0x020002C9 RID: 713
		public class SimulateEvolutionParam : EditUtility.HaveBeforeAfterCharaParamSimulateParam
		{
			// Token: 0x0400157F RID: 5503
			public int afterCharaId;

			// Token: 0x04001580 RID: 5504
			public int afterMaxLv;

			// Token: 0x04001581 RID: 5505
			public int afterLimitBreak;
		}

		// Token: 0x020002CA RID: 714
		public enum eWeaponCreateResult
		{
			// Token: 0x04001583 RID: 5507
			OK,
			// Token: 0x04001584 RID: 5508
			NotEnoughMoney,
			// Token: 0x04001585 RID: 5509
			NotEnoughItem
		}
	}
}
