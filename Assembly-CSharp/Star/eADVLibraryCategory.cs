﻿using System;

namespace Star
{
	// Token: 0x0200015F RID: 351
	public enum eADVLibraryCategory
	{
		// Token: 0x04000952 RID: 2386
		None,
		// Token: 0x04000953 RID: 2387
		Story,
		// Token: 0x04000954 RID: 2388
		Event,
		// Token: 0x04000955 RID: 2389
		Num
	}
}
