﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000398 RID: 920
	public class ScheduleSetUpDataBase : IDataBaseResource
	{
		// Token: 0x06001153 RID: 4435 RVA: 0x0005AD18 File Offset: 0x00059118
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
			this.m_Database = (pdataobj as ScheduleSetupDB);
			this.m_CategoryDB = new ScheduleSetUpDataBase.ScheduleCategory[2];
			this.m_CategoryDB[0] = new ScheduleSetUpDataBase.ScheduleCategory();
			this.m_CategoryDB[1] = new ScheduleSetUpDataBase.ScheduleCategory();
			int num = -1;
			int num2 = -1;
			for (int i = 0; i < this.m_Database.m_Params.Length; i++)
			{
				if (this.m_Database.m_Params[i].m_Week != 0)
				{
					if ((int)this.m_Database.m_Params[i].m_BuildPriority != num)
					{
						this.m_CategoryDB[0].m_ListUpNum++;
					}
					num = (int)this.m_Database.m_Params[i].m_BuildPriority;
				}
				if (this.m_Database.m_Params[i].m_Holyday != 0)
				{
					if ((int)this.m_Database.m_Params[i].m_BuildPriority != num2)
					{
						this.m_CategoryDB[1].m_ListUpNum++;
					}
					num2 = (int)this.m_Database.m_Params[i].m_BuildPriority;
				}
			}
			for (int i = 0; i < this.m_CategoryDB.Length; i++)
			{
				this.m_CategoryDB[i].m_Pack = new ScheduleSetUpDataBase.ScheudlePackBase[this.m_CategoryDB[i].m_ListUpNum];
			}
			ScheduleSetUpDataBase.ScheduleUpElement item = default(ScheduleSetUpDataBase.ScheduleUpElement);
			num = -1;
			num2 = -1;
			List<ScheduleSetUpDataBase.ScheduleUpElement> list = new List<ScheduleSetUpDataBase.ScheduleUpElement>();
			List<ScheduleSetUpDataBase.ScheduleUpElement> list2 = new List<ScheduleSetUpDataBase.ScheduleUpElement>();
			int num3 = -1;
			int num4 = -1;
			for (int i = 0; i < this.m_Database.m_Params.Length; i++)
			{
				if (this.m_Database.m_Params[i].m_Week != 0)
				{
					if ((int)this.m_Database.m_Params[i].m_BuildPriority != num)
					{
						if (num3 >= 0)
						{
							this.m_CategoryDB[0].m_Pack[num3] = ScheduleSetUpDataBase.ScheudlePackBase.Create(num);
							this.m_CategoryDB[0].m_Pack[num3].m_List = list.ToArray();
							list.Clear();
						}
						num3++;
					}
					item = default(ScheduleSetUpDataBase.ScheduleUpElement);
					item.SetUp(ref this.m_Database.m_Params[i]);
					list.Add(item);
					num = (int)this.m_Database.m_Params[i].m_BuildPriority;
				}
				if (this.m_Database.m_Params[i].m_Holyday != 0)
				{
					if ((int)this.m_Database.m_Params[i].m_BuildPriority != num2)
					{
						if (num4 >= 0)
						{
							this.m_CategoryDB[1].m_Pack[num4] = ScheduleSetUpDataBase.ScheudlePackBase.Create(num2);
							this.m_CategoryDB[1].m_Pack[num4].m_List = list2.ToArray();
							list2.Clear();
						}
						num4++;
					}
					item = default(ScheduleSetUpDataBase.ScheduleUpElement);
					item.SetUp(ref this.m_Database.m_Params[i]);
					list2.Add(item);
					num2 = (int)this.m_Database.m_Params[i].m_BuildPriority;
				}
			}
			this.m_CategoryDB[0].m_Pack[num3] = ScheduleSetUpDataBase.ScheudlePackBase.Create(num);
			this.m_CategoryDB[0].m_Pack[num3].m_List = list.ToArray();
			list.Clear();
			this.m_CategoryDB[1].m_Pack[num4] = ScheduleSetUpDataBase.ScheudlePackBase.Create(num2);
			this.m_CategoryDB[1].m_Pack[num4].m_List = list2.ToArray();
			list2.Clear();
		}

		// Token: 0x06001154 RID: 4436 RVA: 0x0005B09C File Offset: 0x0005949C
		private static UserScheduleData.eType ChangeXlsKeyToType(int fmakewark)
		{
			UserScheduleData.eType result = UserScheduleData.eType.Non;
			switch (fmakewark)
			{
			case 0:
				result = UserScheduleData.eType.Sleep;
				break;
			case 1:
				result = UserScheduleData.eType.Room;
				break;
			case 2:
				result = UserScheduleData.eType.Office;
				break;
			case 3:
				result = UserScheduleData.eType.Town;
				break;
			case 4:
				result = UserScheduleData.eType.System;
				break;
			}
			return result;
		}

		// Token: 0x06001155 RID: 4437 RVA: 0x0005B0F0 File Offset: 0x000594F0
		private bool CheckTownBuildToPoint(int fmakewark, ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid)
		{
			bool result = false;
			switch (fmakewark)
			{
			case 0:
			case 1:
			case 4:
				result = true;
				break;
			case 2:
			{
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 0)
					{
						result = true;
						break;
					}
				}
				break;
			}
			}
			return result;
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x0005B18C File Offset: 0x0005958C
		public void CreateList(long fmanageid, long fsettime, ref UserScheduleData.ListPack pbase, bool fixroom)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserCharacterData charaData = UserCharaUtil.GetCharaData(fmanageid);
			if (charaData != null)
			{
				int secToHolydayType = ScheduleDataBase.GetScheduleHolyday().GetSecToHolydayType(fsettime);
				ScheduleSetUpDataBase.ScheduleUpParam scheduleUpParam = new ScheduleSetUpDataBase.ScheduleUpParam(fmanageid);
				ScheduleSetUpDataBase.ScheduleCategory scheduleCategory = this.m_CategoryDB[secToHolydayType];
				TownAccessCheck.ChrCheckDataBase chrCheckDataBase = new TownAccessCheck.ChrCheckDataBase(fmanageid);
				for (int i = 0; i < scheduleCategory.m_ListUpNum; i++)
				{
					scheduleCategory.m_Pack[i].ListUp(ref chrCheckDataBase, scheduleUpParam);
				}
				pbase.m_SettingTime = fsettime * 1000L * 1000L * 10L;
				pbase.m_ChangeStateTime = fsettime * 1000L * 1000L * 10L;
				pbase.m_Table = scheduleUpParam.CreateSheduleTable(fixroom);
			}
		}

		// Token: 0x06001157 RID: 4439 RVA: 0x0005B248 File Offset: 0x00059648
		public int ChangeFailTag(int fscheduletag, eTitleType ftitle)
		{
			bool flag = false;
			ushort num = 0;
			ushort num2 = 0;
			for (int i = 0; i < this.m_Database.m_Params.Length; i++)
			{
				if (this.m_Database.m_Params[i].m_ScheduleTagSelf == fscheduletag)
				{
					num = this.m_Database.m_Params[i].m_Week;
					num2 = this.m_Database.m_Params[i].m_Holyday;
					flag = true;
					break;
				}
			}
			if (flag)
			{
				for (int i = 0; i < this.m_Database.m_Params.Length; i++)
				{
					if (this.m_Database.m_Params[i].m_Content == (int)ftitle && this.m_Database.m_Params[i].m_Week == num && this.m_Database.m_Params[i].m_Holyday == num2)
					{
						fscheduletag = this.m_Database.m_Params[i].m_ScheduleTagOther;
						break;
					}
				}
			}
			return fscheduletag;
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x0005B364 File Offset: 0x00059764
		public void ChangeSegState(UserScheduleData.Seg pseg, int fchg)
		{
			int num = this.m_Database.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_Database.m_Params[i].m_ScheduleTagSelf == fchg)
				{
					pseg.m_Type = ScheduleSetUpDataBase.ChangeXlsKeyToType((int)this.m_Database.m_Params[i].m_MakePoint);
					break;
				}
			}
			pseg.m_TagNameID = fchg;
		}

		// Token: 0x0400181E RID: 6174
		private const int TIME_HOUR_TO_SEC = 3600;

		// Token: 0x0400181F RID: 6175
		public const int ROOM_TAG = 1;

		// Token: 0x04001820 RID: 6176
		private ScheduleSetupDB m_Database;

		// Token: 0x04001821 RID: 6177
		private ScheduleSetUpDataBase.ScheduleCategory[] m_CategoryDB;

		// Token: 0x02000399 RID: 921
		public struct ScheduleUpElement
		{
			// Token: 0x06001159 RID: 4441 RVA: 0x0005B3DC File Offset: 0x000597DC
			public void SetUp(ref ScheduleSetupDB_Param pbase)
			{
				this.m_ID = pbase.m_ID;
				this.m_BuildPriority = pbase.m_BuildPriority;
				this.m_ScheduleTagSelf = pbase.m_ScheduleTagSelf;
				this.m_ScheduleTagOther = pbase.m_ScheduleTagOther;
				this.m_Content = (eTitleType)pbase.m_Content;
				this.m_MakePoint = pbase.m_MakePoint;
				this.m_Week = pbase.m_Week;
				this.m_Holyday = pbase.m_Holyday;
				this.m_BuildType = (eXlsScheduleMakeType)pbase.m_BuildType;
				this.m_StartTime = pbase.m_StartTime;
				this.m_EndTime = pbase.m_EndTime;
				this.m_StartLife = pbase.m_StartLife;
				this.m_EndLife = pbase.m_EndLife;
				this.m_BuildUpPer = pbase.m_BuildUpPer;
			}

			// Token: 0x04001822 RID: 6178
			public int m_ID;

			// Token: 0x04001823 RID: 6179
			public ushort m_BuildPriority;

			// Token: 0x04001824 RID: 6180
			public int m_ScheduleTagSelf;

			// Token: 0x04001825 RID: 6181
			public int m_ScheduleTagOther;

			// Token: 0x04001826 RID: 6182
			public eTitleType m_Content;

			// Token: 0x04001827 RID: 6183
			public ushort m_MakePoint;

			// Token: 0x04001828 RID: 6184
			public ushort m_Week;

			// Token: 0x04001829 RID: 6185
			public ushort m_Holyday;

			// Token: 0x0400182A RID: 6186
			public eXlsScheduleMakeType m_BuildType;

			// Token: 0x0400182B RID: 6187
			public int m_StartTime;

			// Token: 0x0400182C RID: 6188
			public int m_EndTime;

			// Token: 0x0400182D RID: 6189
			public int m_StartLife;

			// Token: 0x0400182E RID: 6190
			public int m_EndLife;

			// Token: 0x0400182F RID: 6191
			public short m_BuildUpPer;
		}

		// Token: 0x0200039A RID: 922
		public class ScheudlePackBase
		{
			// Token: 0x0600115B RID: 4443 RVA: 0x0005B49C File Offset: 0x0005989C
			public static ScheduleSetUpDataBase.ScheudlePackBase Create(int fkey)
			{
				ScheduleSetUpDataBase.ScheudlePackBase result;
				if (fkey != 1)
				{
					if (fkey != 2)
					{
						result = new ScheduleSetUpDataBase.ScheudlePackNormal();
					}
					else
					{
						result = new ScheduleSetUpDataBase.ScheudlePackBuff();
					}
				}
				else
				{
					result = new ScheduleSetUpDataBase.ScheudlePackContent();
				}
				return result;
			}

			// Token: 0x0600115C RID: 4444 RVA: 0x0005B4E2 File Offset: 0x000598E2
			public virtual void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
			}

			// Token: 0x04001830 RID: 6192
			public ScheduleSetUpDataBase.ScheduleUpElement[] m_List;
		}

		// Token: 0x0200039B RID: 923
		public class ScheudlePackNormal : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x0600115E RID: 4446 RVA: 0x0005B4EC File Offset: 0x000598EC
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
				int num = this.m_List.Length;
				for (int i = 0; i < num; i++)
				{
					int num2 = UnityEngine.Random.Range(0, 100);
					if (num2 <= (int)this.m_List[i].m_BuildUpPer)
					{
						eXlsScheduleMakeType buildType = this.m_List[i].m_BuildType;
						if (buildType != eXlsScheduleMakeType.Fix)
						{
							if (buildType != eXlsScheduleMakeType.Derive)
							{
								if (buildType == eXlsScheduleMakeType.Free)
								{
									psetup.CreateSceduleFreeSeg(ref this.m_List[i], true);
								}
							}
							else
							{
								psetup.CreateSceduleDeriveSeg(ref this.m_List[i], true);
							}
						}
						else
						{
							psetup.CreateSceduleFixSeg(ref this.m_List[i], true);
						}
					}
				}
			}
		}

		// Token: 0x0200039C RID: 924
		public class ScheudlePackContent : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06001160 RID: 4448 RVA: 0x0005B5B0 File Offset: 0x000599B0
			private void CheckTownBuildToContent(ref TownAccessCheck.ChrCheckDataBase pchrparam, List<ScheduleSetUpDataBase.ScheudlePackContent.Up> flist)
			{
				TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 536870912)
					{
						buildCheckDataBase.SetBuildParam(buildObjectDataAt.m_ManageID);
						ScheduleSetUpDataBase.ScheudlePackContent.Up up = new ScheduleSetUpDataBase.ScheudlePackContent.Up(i, 1f, buildCheckDataBase.m_Title);
						if (pchrparam.m_Title == buildCheckDataBase.m_Title)
						{
							up.m_Per = 2f;
						}
						flist.Add(up);
					}
				}
			}

			// Token: 0x06001161 RID: 4449 RVA: 0x0005B660 File Offset: 0x00059A60
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
				List<ScheduleSetUpDataBase.ScheudlePackContent.Up> list = new List<ScheduleSetUpDataBase.ScheudlePackContent.Up>();
				this.CheckTownBuildToContent(ref pchrparam, list);
				int num = list.Count;
				if (num != 0)
				{
					float num2 = 0f;
					for (int i = 0; i < num; i++)
					{
						num2 += list[i].m_Per;
					}
					for (int i = 0; i < num; i++)
					{
						list[i].m_Per /= num2;
					}
					float num3 = UnityEngine.Random.Range(0f, 1f);
					eTitleType eTitleType = eTitleType.None;
					for (int i = 0; i < num; i++)
					{
						num3 -= list[i].m_Per;
						if (num3 <= 0f)
						{
							eTitleType = list[i].m_Title;
							break;
						}
					}
					num = this.m_List.Length;
					eTitleType eTitleType2 = eTitleType.None;
					for (int i = 0; i < num; i++)
					{
						if (eTitleType == this.m_List[i].m_Content && UnityEngine.Random.Range(0, 100) <= (int)this.m_List[i].m_BuildUpPer)
						{
							eXlsScheduleMakeType buildType = this.m_List[i].m_BuildType;
							if (buildType != eXlsScheduleMakeType.Fix)
							{
								if (buildType != eXlsScheduleMakeType.Derive)
								{
									if (buildType == eXlsScheduleMakeType.Free)
									{
										psetup.CreateSceduleFreeSeg(ref this.m_List[i], eTitleType == pchrparam.m_Title);
									}
								}
								else if (eTitleType2 == this.m_List[i].m_Content)
								{
									psetup.CreateSceduleDeriveSeg(ref this.m_List[i], eTitleType == pchrparam.m_Title);
								}
							}
							else
							{
								psetup.CreateSceduleFixSeg(ref this.m_List[i], eTitleType == pchrparam.m_Title);
							}
							eTitleType2 = eTitleType;
						}
					}
				}
			}

			// Token: 0x0200039D RID: 925
			public class Up
			{
				// Token: 0x06001162 RID: 4450 RVA: 0x0005B830 File Offset: 0x00059C30
				public Up(int fpoint, float fper, eTitleType ftitle)
				{
					this.m_Index = fpoint;
					this.m_Per = fper;
					this.m_Title = ftitle;
				}

				// Token: 0x04001831 RID: 6193
				public float m_Per;

				// Token: 0x04001832 RID: 6194
				public int m_Index;

				// Token: 0x04001833 RID: 6195
				public eTitleType m_Title;
			}
		}

		// Token: 0x0200039E RID: 926
		public class ScheudlePackBuff : ScheduleSetUpDataBase.ScheudlePackBase
		{
			// Token: 0x06001164 RID: 4452 RVA: 0x0005B858 File Offset: 0x00059C58
			private bool CheckTownBuffToPoint(ref TownAccessCheck.ChrCheckDataBase pchrparam, int ftagnameid)
			{
				bool result = false;
				TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && buildObjectDataAt.m_IsOpen && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 1073741824)
					{
						buildCheckDataBase.SetBuildParam(buildObjectDataAt.m_ManageID);
						if (TownAccessCheck.IsAccessSearchBuild(ref pchrparam, ref buildCheckDataBase, ftagnameid))
						{
							result = true;
							break;
						}
					}
				}
				return result;
			}

			// Token: 0x06001165 RID: 4453 RVA: 0x0005B8F4 File Offset: 0x00059CF4
			public override void ListUp(ref TownAccessCheck.ChrCheckDataBase pchrparam, ScheduleSetUpDataBase.ScheduleUpParam psetup)
			{
				int num = this.m_List.Length;
				int num2 = 3;
				for (int i = 0; i < num; i++)
				{
					if (num2 > 0 && this.CheckTownBuffToPoint(ref pchrparam, this.m_List[i].m_ScheduleTagSelf) && UnityEngine.Random.Range(0, 100) <= (int)this.m_List[i].m_BuildUpPer)
					{
						eXlsScheduleMakeType buildType = this.m_List[i].m_BuildType;
						if (buildType != eXlsScheduleMakeType.Fix)
						{
							if (buildType != eXlsScheduleMakeType.Derive)
							{
								if (buildType == eXlsScheduleMakeType.Free)
								{
									psetup.CreateSceduleFreeSeg(ref this.m_List[i], true);
								}
							}
							else
							{
								psetup.CreateSceduleDeriveSeg(ref this.m_List[i], true);
							}
						}
						else
						{
							psetup.CreateSceduleFixSeg(ref this.m_List[i], true);
						}
						num2--;
					}
				}
			}
		}

		// Token: 0x0200039F RID: 927
		public class ScheduleCategory
		{
			// Token: 0x04001834 RID: 6196
			public int m_ListUpNum;

			// Token: 0x04001835 RID: 6197
			public ScheduleSetUpDataBase.ScheudlePackBase[] m_Pack;
		}

		// Token: 0x020003A0 RID: 928
		public class Seg
		{
			// Token: 0x04001836 RID: 6198
			public int m_Type;

			// Token: 0x04001837 RID: 6199
			public int m_WakeTime;

			// Token: 0x04001838 RID: 6200
			public int m_UseTime;

			// Token: 0x04001839 RID: 6201
			public int m_TagNameID;

			// Token: 0x0400183A RID: 6202
			public int m_ID;

			// Token: 0x0400183B RID: 6203
			public int m_Priority;
		}

		// Token: 0x020003A1 RID: 929
		public class ScheduleUpParam
		{
			// Token: 0x06001168 RID: 4456 RVA: 0x0005B9E7 File Offset: 0x00059DE7
			public ScheduleUpParam(long fmanageid)
			{
				this.m_Mask = new byte[25];
				this.m_List = new List<ScheduleSetUpDataBase.Seg>();
			}

			// Token: 0x06001169 RID: 4457 RVA: 0x0005BA08 File Offset: 0x00059E08
			public int GetFreeHour()
			{
				for (int i = 0; i < 24; i++)
				{
					if (this.m_Mask[i] == 0)
					{
						return i;
					}
				}
				return -1;
			}

			// Token: 0x0600116A RID: 4458 RVA: 0x0005BA38 File Offset: 0x00059E38
			public int GetFreeHour(int fstart, int fend)
			{
				for (int i = fstart; i <= fend; i++)
				{
					if (this.m_Mask[i] == 0)
					{
						return i;
					}
				}
				return -1;
			}

			// Token: 0x0600116B RID: 4459 RVA: 0x0005BA68 File Offset: 0x00059E68
			public void AddSeg(ScheduleSetUpDataBase.Seg pseg)
			{
				if (pseg.m_WakeTime + pseg.m_UseTime >= 24)
				{
					pseg.m_UseTime = 24 - pseg.m_WakeTime;
				}
				for (int i = 0; i < pseg.m_UseTime; i++)
				{
					this.m_Mask[pseg.m_WakeTime + i] = byte.MaxValue;
				}
				this.m_List.Add(pseg);
			}

			// Token: 0x0600116C RID: 4460 RVA: 0x0005BACF File Offset: 0x00059ECF
			public ScheduleSetUpDataBase.Seg GetLastSeg()
			{
				if (this.m_List.Count != 0)
				{
					return this.m_List[this.m_List.Count - 1];
				}
				return null;
			}

			// Token: 0x0600116D RID: 4461 RVA: 0x0005BAFC File Offset: 0x00059EFC
			private void SortTable()
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					ScheduleSetUpDataBase.Seg seg = this.m_List[i];
					for (int j = i + 1; j < count; j++)
					{
						if (seg.m_WakeTime > this.m_List[j].m_WakeTime)
						{
							seg = this.m_List[j];
							this.m_List[j] = this.m_List[i];
							this.m_List[i] = seg;
						}
					}
				}
			}

			// Token: 0x0600116E RID: 4462 RVA: 0x0005BB98 File Offset: 0x00059F98
			private void ReactRoomTag()
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_TagNameID > 1)
					{
						this.m_List[i].m_TagNameID = 1;
					}
				}
			}

			// Token: 0x0600116F RID: 4463 RVA: 0x0005BBEC File Offset: 0x00059FEC
			private void MargeSeg()
			{
				int num = this.m_List.Count - 1;
				int i = 0;
				while (i < num)
				{
					if (this.m_List[i].m_TagNameID == this.m_List[i + 1].m_TagNameID)
					{
						this.m_List[i].m_UseTime += this.m_List[i + 1].m_UseTime;
						this.m_List.RemoveAt(i + 1);
						num = this.m_List.Count - 1;
					}
					else
					{
						i++;
					}
				}
			}

			// Token: 0x06001170 RID: 4464 RVA: 0x0005BC90 File Offset: 0x0005A090
			private void SetBlankSeg()
			{
				ScheduleSetUpDataBase.Seg seg = null;
				for (int i = 0; i < 24; i++)
				{
					if (this.m_Mask[i] == 0)
					{
						if (seg == null)
						{
							seg = new ScheduleSetUpDataBase.Seg();
							seg.m_WakeTime = i;
							seg.m_UseTime = 1;
							seg.m_TagNameID = 1;
							seg.m_Priority = 4;
							seg.m_ID = -1;
							seg.m_Type = 1;
						}
						else
						{
							seg.m_UseTime++;
						}
						this.m_Mask[i] = byte.MaxValue;
					}
					else if (seg != null)
					{
						this.m_List.Add(seg);
						seg = null;
					}
				}
			}

			// Token: 0x06001171 RID: 4465 RVA: 0x0005BD30 File Offset: 0x0005A130
			public UserScheduleData.Seg[] CreateSheduleTable(bool fixroom)
			{
				this.SetBlankSeg();
				this.SortTable();
				int count = this.m_List.Count;
				for (int i = 0; i < count - 1; i++)
				{
					if (this.m_List[i].m_WakeTime + this.m_List[i].m_UseTime > this.m_List[i + 1].m_WakeTime)
					{
						if (this.m_List[i].m_Priority < this.m_List[i + 1].m_Priority)
						{
							this.m_List[i + 1].m_WakeTime = this.m_List[i].m_WakeTime + this.m_List[i].m_UseTime;
						}
						else
						{
							this.m_List[i].m_UseTime = this.m_List[i + 1].m_WakeTime - this.m_List[i].m_WakeTime;
						}
					}
				}
				for (int i = 0; i < 24; i++)
				{
					this.m_Mask[i] = 0;
				}
				for (int i = 0; i < count; i++)
				{
					for (int j = 0; j < this.m_List[i].m_UseTime; j++)
					{
						this.m_Mask[this.m_List[i].m_WakeTime + j] = byte.MaxValue;
					}
				}
				this.SetBlankSeg();
				this.SortTable();
				if (fixroom)
				{
					this.ReactRoomTag();
				}
				this.MargeSeg();
				int num = this.m_List.Count + 1;
				count = this.m_List.Count;
				UserScheduleData.Seg[] array = new UserScheduleData.Seg[num];
				for (int i = 0; i < count; i++)
				{
					array[i] = new UserScheduleData.Seg();
					array[i].m_Type = ScheduleSetUpDataBase.ChangeXlsKeyToType(this.m_List[i].m_Type);
					array[i].m_WakeTime = this.m_List[i].m_WakeTime * 3600;
					array[i].m_UseTime = this.m_List[i].m_UseTime * 3600;
					array[i].m_TagNameID = this.m_List[i].m_TagNameID;
				}
				array[num - 1] = new UserScheduleData.Seg();
				array[num - 1].m_Type = UserScheduleData.eType.End;
				array[num - 1].m_WakeTime = 86400;
				array[num - 1].m_UseTime = 0;
				array[num - 1].m_TagNameID = 0;
				return array;
			}

			// Token: 0x06001172 RID: 4466 RVA: 0x0005BFBC File Offset: 0x0005A3BC
			public void CreateSceduleFixSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
				ScheduleSetUpDataBase.Seg seg = new ScheduleSetUpDataBase.Seg();
				seg.m_Type = (int)pseg.m_MakePoint;
				seg.m_TagNameID = ((!fusetag) ? pseg.m_ScheduleTagOther : pseg.m_ScheduleTagSelf);
				seg.m_Priority = (int)pseg.m_BuildPriority;
				seg.m_ID = pseg.m_ID;
				switch (pseg.m_BuildPriority)
				{
				case 0:
					if (pseg.m_StartTime >= 0 && pseg.m_EndTime >= 0)
					{
						seg.m_WakeTime = pseg.m_StartTime;
						seg.m_UseTime = pseg.m_EndTime - pseg.m_StartTime;
					}
					else if (pseg.m_StartTime >= 0)
					{
						seg.m_WakeTime = pseg.m_StartTime;
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
					}
					else
					{
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
						seg.m_WakeTime = pseg.m_EndTime - seg.m_UseTime;
					}
					this.AddSeg(seg);
					break;
				case 1:
					seg.m_WakeTime = UnityEngine.Random.Range(pseg.m_StartTime, pseg.m_EndTime + 1);
					seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
					this.AddSeg(seg);
					break;
				case 2:
					seg.m_WakeTime = UnityEngine.Random.Range(pseg.m_StartTime, pseg.m_EndTime + 1);
					seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
					this.AddSeg(seg);
					break;
				case 3:
					seg.m_WakeTime = this.GetFreeHour(pseg.m_StartTime, pseg.m_EndTime);
					if (seg.m_WakeTime != -1)
					{
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
						this.AddSeg(seg);
					}
					break;
				}
			}

			// Token: 0x06001173 RID: 4467 RVA: 0x0005C1A4 File Offset: 0x0005A5A4
			public void CreateSceduleDeriveSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
				ScheduleSetUpDataBase.Seg lastSeg = this.GetLastSeg();
				if (lastSeg != null)
				{
					ScheduleSetUpDataBase.Seg seg = new ScheduleSetUpDataBase.Seg();
					seg.m_Type = (int)pseg.m_MakePoint;
					seg.m_TagNameID = ((!fusetag) ? pseg.m_ScheduleTagOther : pseg.m_ScheduleTagSelf);
					seg.m_Priority = (int)pseg.m_BuildPriority;
					seg.m_ID = pseg.m_ID;
					seg.m_WakeTime = lastSeg.m_WakeTime + lastSeg.m_UseTime;
					switch (pseg.m_BuildPriority)
					{
					case 0:
						seg.m_UseTime = pseg.m_EndTime - pseg.m_StartTime;
						break;
					case 1:
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
						break;
					case 2:
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
						break;
					case 3:
						seg.m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1);
						break;
					}
					this.AddSeg(seg);
				}
			}

			// Token: 0x06001174 RID: 4468 RVA: 0x0005C2B4 File Offset: 0x0005A6B4
			public void CreateSceduleFreeSeg(ref ScheduleSetUpDataBase.ScheduleUpElement pseg, bool fusetag = true)
			{
				int freeHour = this.GetFreeHour();
				if (freeHour >= 0)
				{
					this.AddSeg(new ScheduleSetUpDataBase.Seg
					{
						m_Type = (int)pseg.m_MakePoint,
						m_Priority = (int)pseg.m_BuildPriority,
						m_ID = pseg.m_ID,
						m_TagNameID = ((!fusetag) ? pseg.m_ScheduleTagOther : pseg.m_ScheduleTagSelf),
						m_WakeTime = freeHour,
						m_UseTime = UnityEngine.Random.Range(pseg.m_StartLife, pseg.m_EndLife + 1)
					});
				}
			}

			// Token: 0x0400183C RID: 6204
			public List<ScheduleSetUpDataBase.Seg> m_List;

			// Token: 0x0400183D RID: 6205
			public byte[] m_Mask;
		}
	}
}
