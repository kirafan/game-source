﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Star
{
	// Token: 0x02000634 RID: 1588
	public sealed class SoundManager
	{
		// Token: 0x06001F0A RID: 7946 RVA: 0x000A8D6C File Offset: 0x000A716C
		public void Update()
		{
			for (int i = this.m_ActiveSoundObjs.Count - 1; i >= 0; i--)
			{
				SoundObject soundObject = this.m_ActiveSoundObjs[i];
				soundObject.Update();
				if (soundObject.GetStatus() == SoundObject.eStatus.Removed && soundObject.Ref <= 0)
				{
					this.SinkSoundObject(soundObject);
				}
			}
		}

		// Token: 0x06001F0B RID: 7947 RVA: 0x000A8DCC File Offset: 0x000A71CC
		public void Destroy()
		{
			this.m_SoundCueSheetDB = null;
			this.m_SoundBgmListDB = null;
			this.m_SoundSeListDB = null;
			this.m_SoundVoiceListDB = null;
			this.m_NamedListDB = null;
			this.m_LocalSaveData = null;
			this.m_SoundBgmIndices = null;
			this.m_SoundSeIndices = null;
			this.m_SoundVoiceIndices = null;
			for (int i = 0; i < this.m_InactiveSoundObjs.Count; i++)
			{
				if (this.m_InactiveSoundObjs[i] != null)
				{
					this.m_InactiveSoundObjs[i].Destroy();
					this.m_InactiveSoundObjs[i] = null;
				}
			}
			this.m_InactiveSoundObjs.Clear();
			for (int j = 0; j < this.m_ActiveSoundObjs.Count; j++)
			{
				if (this.m_ActiveSoundObjs[j] != null)
				{
					this.m_ActiveSoundObjs[j].Destroy();
					this.m_ActiveSoundObjs[j] = null;
				}
			}
			this.m_ActiveSoundObjs.Clear();
			this.m_VoiceCueSheetRefCounts.Clear();
		}

		// Token: 0x06001F0C RID: 7948 RVA: 0x000A8ED0 File Offset: 0x000A72D0
		public void Setup(SoundCueSheetDB soundCueSheetDB, SoundSeListDB soundSeListDB, SoundBgmListDB soundBgmListDB, SoundVoiceListDB soundVoiceListDB, NamedListDB namedListDB)
		{
			this.m_InactiveSoundObjs.Clear();
			for (int i = 0; i < 32; i++)
			{
				SoundObject item = new SoundObject();
				this.m_InactiveSoundObjs.Add(item);
			}
			this.SetDatabase(soundCueSheetDB, soundSeListDB, soundBgmListDB, soundVoiceListDB, namedListDB);
		}

		// Token: 0x06001F0D RID: 7949 RVA: 0x000A8F1C File Offset: 0x000A731C
		public void ApplySaveData(LocalSaveData localSaveData)
		{
			this.m_LocalSaveData = localSaveData;
			if (this.m_LocalSaveData != null)
			{
				this._SetCategoryVolume(SoundDefine.eCategory.SE, this.m_LocalSaveData.SeVolume);
				this._SetCategoryVolume(SoundDefine.eCategory.BGM, this.m_LocalSaveData.BgmVolume);
				this._SetCategoryVolume(SoundDefine.eCategory.VOICE, this.m_LocalSaveData.VoiceVolume);
			}
		}

		// Token: 0x06001F0E RID: 7950 RVA: 0x000A8F74 File Offset: 0x000A7374
		public void RegisterInstalledAcf()
		{
			string installPath = CRIFileInstaller.InstallPath;
			string text = Path.Combine(installPath, "Star.acf").Replace("\\", "/");
			if (File.Exists(text))
			{
				CriAtomEx.UnregisterAcf();
				CriAtomEx.RegisterAcf(null, text);
			}
			Debug.LogFormat("RegisterInstalledAcf() acfPath:{0}, isExists:{1}", new object[]
			{
				text,
				File.Exists(text)
			});
		}

		// Token: 0x06001F0F RID: 7951 RVA: 0x000A8FDC File Offset: 0x000A73DC
		public void SetDatabase(SoundCueSheetDB soundCueSheetDB, SoundSeListDB soundSeListDB, SoundBgmListDB soundBgmListDB, SoundVoiceListDB soundVoiceListDB, NamedListDB namedListDB)
		{
			this.m_SoundCueSheetDB = soundCueSheetDB;
			this.m_SoundBgmListDB = soundBgmListDB;
			this.m_SoundBgmIndices = new Dictionary<eSoundBgmListDB, int>();
			for (int i = 0; i < this.m_SoundBgmListDB.m_Params.Length; i++)
			{
				this.m_SoundBgmIndices.Add((eSoundBgmListDB)this.m_SoundBgmListDB.m_Params[i].m_ID, i);
			}
			this.m_SoundSeListDB = soundSeListDB;
			try
			{
				this.m_SoundSeIndices = new Dictionary<eSoundSeListDB, int>();
				for (int j = 0; j < this.m_SoundSeListDB.m_Params.Length; j++)
				{
					this.m_SoundSeIndices.Add((eSoundSeListDB)this.m_SoundSeListDB.m_Params[j].m_ID, j);
				}
			}
			catch (Exception message)
			{
				Debug.LogError(message);
			}
			this.m_SoundVoiceListDB = soundVoiceListDB;
			this.m_SoundVoiceIndices = new Dictionary<eSoundVoiceListDB, int>();
			for (int k = 0; k < this.m_SoundVoiceListDB.m_Params.Length; k++)
			{
				this.m_SoundVoiceIndices.Add((eSoundVoiceListDB)this.m_SoundVoiceListDB.m_Params[k].m_ID, k);
			}
			this.m_NamedListDB = namedListDB;
		}

		// Token: 0x06001F10 RID: 7952 RVA: 0x000A9110 File Offset: 0x000A7510
		public SoundCueSheetDB GetSoundCueSheetDB()
		{
			return this.m_SoundCueSheetDB;
		}

		// Token: 0x06001F11 RID: 7953 RVA: 0x000A9118 File Offset: 0x000A7518
		public bool LoadCueSheet(string cueSheet, string acbFile, string awbFile)
		{
			if (string.IsNullOrEmpty(cueSheet) || this.m_CueSheets.Contains(cueSheet))
			{
				return false;
			}
			if (string.IsNullOrEmpty(acbFile) && string.IsNullOrEmpty(awbFile))
			{
				SoundCueSheetDB_Param param = this.m_SoundCueSheetDB.GetParam(cueSheet);
				acbFile = param.m_ACB;
				awbFile = param.m_AWB;
			}
			if (string.IsNullOrEmpty(acbFile))
			{
				return false;
			}
			bool flag = false;
			for (int i = 0; i < SoundDefine.BUILTIN_DATAS.GetLength(0); i++)
			{
				if (cueSheet == SoundDefine.BUILTIN_DATAS[i, 0])
				{
					flag = true;
					break;
				}
			}
			string text = null;
			if (!flag)
			{
				text = CRIFileInstaller.InstallPath;
			}
			if (!string.IsNullOrEmpty(text))
			{
				acbFile = Path.Combine(text, acbFile).Replace("\\", "/");
				if (!string.IsNullOrEmpty(awbFile))
				{
					awbFile = Path.Combine(text, awbFile).Replace("\\", "/");
				}
			}
			CriAtomCueSheet criAtomCueSheet = CriAtom.AddCueSheet(cueSheet, acbFile, awbFile, null);
			if (criAtomCueSheet != null && criAtomCueSheet.acb != null)
			{
				this.m_CueSheets.Add(cueSheet);
				return true;
			}
			CriAtom.RemoveCueSheet(cueSheet);
			return false;
		}

		// Token: 0x06001F12 RID: 7954 RVA: 0x000A9250 File Offset: 0x000A7650
		public bool LoadCueSheet(eSoundCueSheetDB cueSheetID)
		{
			if (this.m_SoundCueSheetDB == null)
			{
				return false;
			}
			SoundCueSheetDB_Param param = this.m_SoundCueSheetDB.GetParam(cueSheetID);
			return this.LoadCueSheet(param.m_CueSheet, param.m_ACB, param.m_AWB);
		}

		// Token: 0x06001F13 RID: 7955 RVA: 0x000A9298 File Offset: 0x000A7698
		public bool LoadVoiceCueSheet(eCharaNamedType namedType)
		{
			if (this.m_NamedListDB == null)
			{
				return false;
			}
			if (namedType == eCharaNamedType.None)
			{
				return false;
			}
			string voiceCueSheet = this.GetVoiceCueSheet(namedType);
			return this.LoadVoiceCueSheet(voiceCueSheet);
		}

		// Token: 0x06001F14 RID: 7956 RVA: 0x000A92D0 File Offset: 0x000A76D0
		public bool LoadVoiceCueSheet(string cueSheet)
		{
			if (string.IsNullOrEmpty(cueSheet))
			{
				return false;
			}
			int num = 0;
			if (this.m_VoiceCueSheetRefCounts.TryGetValue(cueSheet, out num))
			{
				num++;
				this.m_VoiceCueSheetRefCounts[cueSheet] = num;
				return false;
			}
			this.m_VoiceCueSheetRefCounts.Add(cueSheet, 1);
			string acbFile = cueSheet + ".acb";
			string awbFile = cueSheet + ".awb";
			return this.LoadCueSheet(cueSheet, acbFile, awbFile);
		}

		// Token: 0x06001F15 RID: 7957 RVA: 0x000A9340 File Offset: 0x000A7740
		public bool IsLoadingCueSheets()
		{
			return CriAtom.CueSheetsAreLoading;
		}

		// Token: 0x06001F16 RID: 7958 RVA: 0x000A9348 File Offset: 0x000A7748
		public SoundDefine.eLoadStatus GetCueSheetLoadStatus(eSoundCueSheetDB cueSheetID)
		{
			CriAtomCueSheet cueSheet = CriAtom.GetCueSheet(this.m_SoundCueSheetDB.GetParam(cueSheetID).m_CueSheet);
			if (cueSheet == null)
			{
				return SoundDefine.eLoadStatus.None;
			}
			if (cueSheet.IsLoading)
			{
				return SoundDefine.eLoadStatus.Loading;
			}
			return SoundDefine.eLoadStatus.Loaded;
		}

		// Token: 0x06001F17 RID: 7959 RVA: 0x000A9388 File Offset: 0x000A7788
		public SoundDefine.eLoadStatus GetCueSheetLoadStatus(string cueSheet)
		{
			CriAtomCueSheet cueSheet2 = CriAtom.GetCueSheet(cueSheet);
			if (cueSheet2 == null)
			{
				return SoundDefine.eLoadStatus.None;
			}
			if (cueSheet2.IsLoading)
			{
				return SoundDefine.eLoadStatus.Loading;
			}
			return SoundDefine.eLoadStatus.Loaded;
		}

		// Token: 0x06001F18 RID: 7960 RVA: 0x000A93B4 File Offset: 0x000A77B4
		public SoundDefine.eLoadStatus GetVoiceCueSheetLoadStatus(eCharaNamedType namedType)
		{
			string voiceCueSheet = this.GetVoiceCueSheet(namedType);
			return this.GetCueSheetLoadStatus(voiceCueSheet);
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x000A93D0 File Offset: 0x000A77D0
		public int GetTrackNum(string cueSheet, string cueName)
		{
			CriAtomExAcb acb = CriAtom.GetAcb(cueSheet);
			if (acb != null)
			{
				CriAtomEx.CueInfo cueInfo2;
				bool cueInfo = acb.GetCueInfo(cueName, out cueInfo2);
				if (cueInfo)
				{
					return (int)cueInfo2.numTracks;
				}
			}
			return 0;
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x000A9404 File Offset: 0x000A7804
		public int GetTrackNum(eCharaNamedType namedType, string cueName)
		{
			if (this.m_NamedListDB == null)
			{
				return 0;
			}
			if (namedType == eCharaNamedType.None)
			{
				return 0;
			}
			string voiceCueSheet = this.GetVoiceCueSheet(namedType);
			return this.GetTrackNum(voiceCueSheet, cueName);
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x000A943D File Offset: 0x000A783D
		public bool UnloadCueSheet(string cueSheet)
		{
			if (!string.IsNullOrEmpty(cueSheet) && this.m_CueSheets.Contains(cueSheet))
			{
				CriAtom.RemoveCueSheet(cueSheet);
				this.m_CueSheets.Remove(cueSheet);
				return true;
			}
			return false;
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x000A9474 File Offset: 0x000A7874
		public bool UnloadCueSheet(eSoundCueSheetDB cueSheetID)
		{
			return !(this.m_SoundCueSheetDB == null) && this.UnloadCueSheet(this.m_SoundCueSheetDB.GetParam(cueSheetID).m_CueSheet);
		}

		// Token: 0x06001F1D RID: 7965 RVA: 0x000A94B0 File Offset: 0x000A78B0
		public bool UnloadVoiceCueSheet(eCharaNamedType namedType)
		{
			if (this.m_NamedListDB == null)
			{
				return false;
			}
			if (namedType == eCharaNamedType.None)
			{
				return false;
			}
			string voiceCueSheet = this.GetVoiceCueSheet(namedType);
			return this.UnloadVoiceCueSheet(voiceCueSheet);
		}

		// Token: 0x06001F1E RID: 7966 RVA: 0x000A94E8 File Offset: 0x000A78E8
		public bool UnloadVoiceCueSheet(string cueSheet)
		{
			int num = 0;
			if (this.m_VoiceCueSheetRefCounts.TryGetValue(cueSheet, out num))
			{
				num--;
				this.m_VoiceCueSheetRefCounts[cueSheet] = num;
			}
			if (num <= 0)
			{
				this.m_VoiceCueSheetRefCounts.Remove(cueSheet);
				return this.UnloadCueSheet(cueSheet);
			}
			return false;
		}

		// Token: 0x06001F1F RID: 7967 RVA: 0x000A9538 File Offset: 0x000A7938
		public void ForceResetOnReturnTitle()
		{
			for (int i = this.m_ActiveSoundObjs.Count - 1; i >= 0; i--)
			{
				SoundObject soundObject = this.m_ActiveSoundObjs[i];
				soundObject.Stop();
				this.SinkSoundObject(soundObject);
			}
			this.m_ActiveSoundObjs.Clear();
			for (int j = this.m_CueSheets.Count - 1; j >= 0; j--)
			{
				this.UnloadCueSheet(this.m_CueSheets[j]);
			}
			this.m_VoiceCueSheetRefCounts.Clear();
		}

		// Token: 0x06001F20 RID: 7968 RVA: 0x000A95C8 File Offset: 0x000A79C8
		private SoundObject ScoopSoundObject()
		{
			if (this.m_InactiveSoundObjs.Count > 0)
			{
				SoundObject soundObject = this.m_InactiveSoundObjs[0];
				this.m_ActiveSoundObjs.Add(soundObject);
				this.m_InactiveSoundObjs.Remove(soundObject);
				return soundObject;
			}
			return null;
		}

		// Token: 0x06001F21 RID: 7969 RVA: 0x000A960F File Offset: 0x000A7A0F
		private void SinkSoundObject(SoundObject so)
		{
			if (so != null && this.m_ActiveSoundObjs.Remove(so))
			{
				this.m_InactiveSoundObjs.Add(so);
			}
		}

		// Token: 0x06001F22 RID: 7970 RVA: 0x000A9634 File Offset: 0x000A7A34
		public void RemoveHndl(SoundHandler hndl, bool stopIfStatusIsNotRemoved = true)
		{
			if (hndl != null)
			{
				hndl.OnMngDestruct(stopIfStatusIsNotRemoved);
			}
		}

		// Token: 0x06001F23 RID: 7971 RVA: 0x000A9644 File Offset: 0x000A7A44
		private bool RequestPrepare(string cueSheet, string cueName, SoundHandler hndl, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec)
		{
			if (string.IsNullOrEmpty(cueSheet))
			{
				return false;
			}
			if (string.IsNullOrEmpty(cueName))
			{
				return false;
			}
			if (hndl == null)
			{
				return false;
			}
			SoundObject soundObject = this.ScoopSoundObject();
			if (soundObject == null)
			{
				return false;
			}
			if (!soundObject.Prepare(cueSheet, cueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec))
			{
				this.SinkSoundObject(soundObject);
				return false;
			}
			hndl.OnMngConstruct(soundObject);
			return true;
		}

		// Token: 0x06001F24 RID: 7972 RVA: 0x000A96AC File Offset: 0x000A7AAC
		private bool RequestPlay(string cueSheet, string cueName, SoundHandler hndl, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec)
		{
			if (string.IsNullOrEmpty(cueSheet))
			{
				return false;
			}
			if (string.IsNullOrEmpty(cueName))
			{
				return false;
			}
			SoundObject soundObject = this.ScoopSoundObject();
			if (soundObject == null)
			{
				return false;
			}
			if (!soundObject.Play(cueSheet, cueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec))
			{
				this.SinkSoundObject(soundObject);
				return false;
			}
			if (hndl != null)
			{
				hndl.OnMngConstruct(soundObject);
			}
			return true;
		}

		// Token: 0x06001F25 RID: 7973 RVA: 0x000A9710 File Offset: 0x000A7B10
		public bool PrepareBGM(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_BgmHandler != null)
			{
				this.RemoveHndl(this.m_BgmHandler, false);
			}
			return this.RequestPrepare(cueSheet, cueName, this.m_BgmHandler, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F26 RID: 7974 RVA: 0x000A9740 File Offset: 0x000A7B40
		public bool PrepareBGM(eSoundBgmListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundBgmIndices != null)
			{
				SoundBgmListDB_Param param = this.m_SoundBgmListDB.GetParam(cueID, this.m_SoundBgmIndices);
				return this.PrepareBGM(this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)param.m_CueSheetID).m_CueSheet, param.m_CueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F27 RID: 7975 RVA: 0x000A979A File Offset: 0x000A7B9A
		public bool PlayBGM(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_BgmHandler != null)
			{
				this.RemoveHndl(this.m_BgmHandler, false);
			}
			return this.RequestPlay(cueSheet, cueName, this.m_BgmHandler, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F28 RID: 7976 RVA: 0x000A97CC File Offset: 0x000A7BCC
		public bool PlayBGM(eSoundBgmListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundBgmIndices != null)
			{
				SoundBgmListDB_Param param = this.m_SoundBgmListDB.GetParam(cueID, this.m_SoundBgmIndices);
				return this.PlayBGM(this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)param.m_CueSheetID).m_CueSheet, param.m_CueName, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x000A9826 File Offset: 0x000A7C26
		public bool IsCompletePrepareBGM()
		{
			return this.m_BgmHandler != null && this.m_BgmHandler.IsCompletePrepare();
		}

		// Token: 0x06001F2A RID: 7978 RVA: 0x000A9840 File Offset: 0x000A7C40
		public bool StopBGM()
		{
			return this.m_BgmHandler != null && this.m_BgmHandler.Stop();
		}

		// Token: 0x06001F2B RID: 7979 RVA: 0x000A985A File Offset: 0x000A7C5A
		public bool SuspendBGM()
		{
			return this.m_BgmHandler != null && this.m_BgmHandler.Suspend();
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x000A9874 File Offset: 0x000A7C74
		public bool ResumeBGM()
		{
			return this.m_BgmHandler != null && this.m_BgmHandler.Resume();
		}

		// Token: 0x06001F2D RID: 7981 RVA: 0x000A9890 File Offset: 0x000A7C90
		public bool IsPlayingBGM(eSoundBgmListDB cueID)
		{
			if (this.m_SoundBgmIndices != null)
			{
				SoundBgmListDB_Param param = this.m_SoundBgmListDB.GetParam(cueID, this.m_SoundBgmIndices);
				return this.IsPlayingBGM(this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)param.m_CueSheetID).m_CueSheet, param.m_CueName);
			}
			return false;
		}

		// Token: 0x06001F2E RID: 7982 RVA: 0x000A98E4 File Offset: 0x000A7CE4
		public bool IsPlayingBGM(string cueSheet, string cueName)
		{
			return this.m_BgmHandler != null && this.m_BgmHandler.GetCueSheet() == cueSheet && this.m_BgmHandler.GetCueName() == cueName && this.m_BgmHandler.IsPlaying(false);
		}

		// Token: 0x06001F2F RID: 7983 RVA: 0x000A993C File Offset: 0x000A7D3C
		public bool PrepareSE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.RequestPrepare(cueSheet, cueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F30 RID: 7984 RVA: 0x000A9950 File Offset: 0x000A7D50
		public bool PrepareSE(eSoundSeListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundSeIndices != null)
			{
				SoundSeListDB_Param param = this.m_SoundSeListDB.GetParam(cueID, this.m_SoundSeIndices);
				return this.PrepareSE(this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)param.m_CueSheetID).m_CueSheet, param.m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x000A99AC File Offset: 0x000A7DAC
		public bool PlaySE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.RequestPlay(cueSheet, cueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F32 RID: 7986 RVA: 0x000A99BF File Offset: 0x000A7DBF
		public bool PlaySE(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.PlaySE(cueSheet, cueName, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F33 RID: 7987 RVA: 0x000A99D4 File Offset: 0x000A7DD4
		public bool PlaySE(eSoundSeListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundSeIndices != null)
			{
				SoundSeListDB_Param param = this.m_SoundSeListDB.GetParam(cueID, this.m_SoundSeIndices);
				return this.PlaySE(this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)param.m_CueSheetID).m_CueSheet, param.m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F34 RID: 7988 RVA: 0x000A9A30 File Offset: 0x000A7E30
		public bool PlaySE(eSoundSeListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.PlaySE(cueID, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F35 RID: 7989 RVA: 0x000A9A40 File Offset: 0x000A7E40
		public bool PrepareVOICE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.RequestPrepare(cueSheet, cueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F36 RID: 7990 RVA: 0x000A9A54 File Offset: 0x000A7E54
		public bool PrepareVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundVoiceIndices != null && namedType != eCharaNamedType.None)
			{
				string voiceCueSheet = this.GetVoiceCueSheet(namedType);
				return this.PrepareVOICE(voiceCueSheet, this.m_SoundVoiceListDB.GetParam(cueID, this.m_SoundVoiceIndices).m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F37 RID: 7991 RVA: 0x000A9AA8 File Offset: 0x000A7EA8
		public bool PlayVOICE(string cueSheet, string cueName, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.RequestPlay(cueSheet, cueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F38 RID: 7992 RVA: 0x000A9AC8 File Offset: 0x000A7EC8
		public bool PlayVOICE(string cueSheet, string cueName, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.PlayVOICE(cueSheet, cueName, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F39 RID: 7993 RVA: 0x000A9ADC File Offset: 0x000A7EDC
		public bool PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, SoundHandler hndl, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			if (this.m_SoundVoiceIndices != null && namedType != eCharaNamedType.None)
			{
				string voiceCueSheet = this.GetVoiceCueSheet(namedType);
				return this.PlayVOICE(voiceCueSheet, this.m_SoundVoiceListDB.GetParam(cueID, this.m_SoundVoiceIndices).m_CueName, hndl, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
			}
			return false;
		}

		// Token: 0x06001F3A RID: 7994 RVA: 0x000A9B2F File Offset: 0x000A7F2F
		public bool PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cueID, float volume = 1f, int startTimeMiliSec = 0, int fadeInMiliSec = -1, int fadeOutMiliSec = -1)
		{
			return this.PlayVOICE(namedType, cueID, null, volume, startTimeMiliSec, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x06001F3B RID: 7995 RVA: 0x000A9B44 File Offset: 0x000A7F44
		public string GetVoiceCueSheet(eCharaNamedType namedType)
		{
			if (namedType != eCharaNamedType.None)
			{
				return "Voice_" + this.m_NamedListDB.GetParam(namedType).m_ResouceBaseName;
			}
			return null;
		}

		// Token: 0x06001F3C RID: 7996 RVA: 0x000A9B78 File Offset: 0x000A7F78
		public void SetCategoryVolume(SoundDefine.eCategory category, float volume, bool isIgnoreSave = false)
		{
			volume = this._SetCategoryVolume(category, volume);
			if (!isIgnoreSave && this.m_LocalSaveData != null)
			{
				if (category != SoundDefine.eCategory.SE)
				{
					if (category != SoundDefine.eCategory.BGM)
					{
						if (category == SoundDefine.eCategory.VOICE)
						{
							this.m_LocalSaveData.VoiceVolume = volume;
						}
					}
					else
					{
						this.m_LocalSaveData.BgmVolume = volume;
					}
				}
				else
				{
					this.m_LocalSaveData.SeVolume = volume;
				}
			}
		}

		// Token: 0x06001F3D RID: 7997 RVA: 0x000A9BEC File Offset: 0x000A7FEC
		private float _SetCategoryVolume(SoundDefine.eCategory category, float volume)
		{
			volume = Mathf.Clamp(volume, 0f, 1f);
			CriAtom.SetCategoryVolume(SoundDefine.CATEGORY_NAMES[(int)category], volume);
			return volume;
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x000A9C0E File Offset: 0x000A800E
		public float GetCategoryVolume(SoundDefine.eCategory category)
		{
			return CriAtom.GetCategoryVolume(SoundDefine.CATEGORY_NAMES[(int)category]);
		}

		// Token: 0x040025E3 RID: 9699
		private SoundCueSheetDB m_SoundCueSheetDB;

		// Token: 0x040025E4 RID: 9700
		private SoundBgmListDB m_SoundBgmListDB;

		// Token: 0x040025E5 RID: 9701
		private Dictionary<eSoundBgmListDB, int> m_SoundBgmIndices;

		// Token: 0x040025E6 RID: 9702
		private SoundSeListDB m_SoundSeListDB;

		// Token: 0x040025E7 RID: 9703
		private Dictionary<eSoundSeListDB, int> m_SoundSeIndices;

		// Token: 0x040025E8 RID: 9704
		private SoundVoiceListDB m_SoundVoiceListDB;

		// Token: 0x040025E9 RID: 9705
		private Dictionary<eSoundVoiceListDB, int> m_SoundVoiceIndices;

		// Token: 0x040025EA RID: 9706
		private NamedListDB m_NamedListDB;

		// Token: 0x040025EB RID: 9707
		private LocalSaveData m_LocalSaveData;

		// Token: 0x040025EC RID: 9708
		private List<string> m_CueSheets = new List<string>();

		// Token: 0x040025ED RID: 9709
		private const int SOUND_OBJ_POOL_NUM = 32;

		// Token: 0x040025EE RID: 9710
		private List<SoundObject> m_InactiveSoundObjs = new List<SoundObject>(32);

		// Token: 0x040025EF RID: 9711
		private List<SoundObject> m_ActiveSoundObjs = new List<SoundObject>(32);

		// Token: 0x040025F0 RID: 9712
		private SoundHandler m_BgmHandler = new SoundHandler();

		// Token: 0x040025F1 RID: 9713
		private Dictionary<string, int> m_VoiceCueSheetRefCounts = new Dictionary<string, int>();
	}
}
