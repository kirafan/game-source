﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020003F7 RID: 1015
	public class EditState_CharaListLimitBreak : EditState
	{
		// Token: 0x0600134D RID: 4941 RVA: 0x000675FB File Offset: 0x000659FB
		public EditState_CharaListLimitBreak(EditMain owner) : base(owner)
		{
		}

		// Token: 0x0600134E RID: 4942 RVA: 0x00067613 File Offset: 0x00065A13
		public override int GetStateID()
		{
			return 11;
		}

		// Token: 0x0600134F RID: 4943 RVA: 0x00067617 File Offset: 0x00065A17
		public override void OnStateEnter()
		{
			this.m_Step = EditState_CharaListLimitBreak.eStep.First;
		}

		// Token: 0x06001350 RID: 4944 RVA: 0x00067620 File Offset: 0x00065A20
		public override void OnStateExit()
		{
		}

		// Token: 0x06001351 RID: 4945 RVA: 0x00067622 File Offset: 0x00065A22
		public override void OnDispose()
		{
		}

		// Token: 0x06001352 RID: 4946 RVA: 0x00067624 File Offset: 0x00065A24
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_CharaListLimitBreak.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = EditState_CharaListLimitBreak.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = EditState_CharaListLimitBreak.eStep.LoadWait;
				}
				break;
			case EditState_CharaListLimitBreak.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_CharaListLimitBreak.eStep.PlayIn;
				}
				break;
			case EditState_CharaListLimitBreak.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_CHARA_LIMITBREAK, null, null);
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, true);
					this.m_Step = EditState_CharaListLimitBreak.eStep.Main;
				}
				break;
			case EditState_CharaListLimitBreak.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
						if (selectButton != CharaListUI.eButton.Chara)
						{
							this.m_NextState = 1;
						}
						else
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID = this.m_Owner.CharaListUI.SelectCharaMngID;
							this.m_NextState = 12;
						}
					}
					this.m_Step = EditState_CharaListLimitBreak.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001353 RID: 4947 RVA: 0x000677A0 File Offset: 0x00065BA0
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.m_Owner.CharaListUI.SetLimitBreakMode();
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x06001354 RID: 4948 RVA: 0x0006783A File Offset: 0x00065C3A
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x06001355 RID: 4949 RVA: 0x00067849 File Offset: 0x00065C49
		private void GoToMenuEnd()
		{
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x04001A4C RID: 6732
		private EditState_CharaListLimitBreak.eStep m_Step = EditState_CharaListLimitBreak.eStep.None;

		// Token: 0x04001A4D RID: 6733
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x020003F8 RID: 1016
		private enum eStep
		{
			// Token: 0x04001A4F RID: 6735
			None = -1,
			// Token: 0x04001A50 RID: 6736
			First,
			// Token: 0x04001A51 RID: 6737
			LoadWait,
			// Token: 0x04001A52 RID: 6738
			PlayIn,
			// Token: 0x04001A53 RID: 6739
			Main
		}
	}
}
