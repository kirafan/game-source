﻿using System;

namespace Star
{
	// Token: 0x02000299 RID: 665
	[Serializable]
	public class UserDebugData
	{
		// Token: 0x0400151D RID: 5405
		public UserDebugData.eData m_Category;

		// Token: 0x0400151E RID: 5406
		public int m_Num;

		// Token: 0x0200029A RID: 666
		public enum eData
		{
			// Token: 0x04001520 RID: 5408
			Level,
			// Token: 0x04001521 RID: 5409
			KiraraPoint,
			// Token: 0x04001522 RID: 5410
			Money,
			// Token: 0x04001523 RID: 5411
			Item
		}
	}
}
