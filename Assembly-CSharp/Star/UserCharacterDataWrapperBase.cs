﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002A1 RID: 673
	public abstract class UserCharacterDataWrapperBase : CharacterDataWrapperBaseExGen<UserCharacterData>
	{
		// Token: 0x06000C8C RID: 3212 RVA: 0x00048885 File Offset: 0x00046C85
		public UserCharacterDataWrapperBase(eCharacterDataType characterDataType, UserCharacterData in_data) : base(characterDataType, in_data)
		{
		}

		// Token: 0x06000C8D RID: 3213 RVA: 0x0004888F File Offset: 0x00046C8F
		public override long GetMngId()
		{
			return this.data.MngID;
		}

		// Token: 0x06000C8E RID: 3214 RVA: 0x0004889C File Offset: 0x00046C9C
		public override int GetCharaId()
		{
			return this.data.Param.CharaID;
		}

		// Token: 0x06000C8F RID: 3215 RVA: 0x000488AE File Offset: 0x00046CAE
		public override eCharaNamedType GetCharaNamedType()
		{
			return this.data.Param.NamedType;
		}

		// Token: 0x06000C90 RID: 3216 RVA: 0x000488C0 File Offset: 0x00046CC0
		public override eClassType GetClassType()
		{
			return this.data.Param.ClassType;
		}

		// Token: 0x06000C91 RID: 3217 RVA: 0x000488D2 File Offset: 0x00046CD2
		public override eElementType GetElementType()
		{
			return this.data.Param.ElementType;
		}

		// Token: 0x06000C92 RID: 3218 RVA: 0x000488E4 File Offset: 0x00046CE4
		public override eRare GetRarity()
		{
			return this.data.Param.RareType;
		}

		// Token: 0x06000C93 RID: 3219 RVA: 0x000488F6 File Offset: 0x00046CF6
		public override int GetCost()
		{
			return this.data.Param.Cost;
		}

		// Token: 0x06000C94 RID: 3220 RVA: 0x00048908 File Offset: 0x00046D08
		public override int GetLv()
		{
			return this.data.Param.Lv;
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x0004891A File Offset: 0x00046D1A
		public override int GetMaxLv()
		{
			return this.data.Param.MaxLv;
		}

		// Token: 0x06000C96 RID: 3222 RVA: 0x0004892C File Offset: 0x00046D2C
		public override long GetExp()
		{
			return this.data.Param.Exp;
		}

		// Token: 0x06000C97 RID: 3223 RVA: 0x0004893E File Offset: 0x00046D3E
		public override int GetLimitBreak()
		{
			return this.data.Param.LimitBreak;
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x00048950 File Offset: 0x00046D50
		public override int GetFriendship()
		{
			return this.GetUserNamedData().FriendShip;
		}

		// Token: 0x06000C99 RID: 3225 RVA: 0x0004895D File Offset: 0x00046D5D
		public override int GetFriendshipMax()
		{
			return this.GetUserNamedData().FriendShipMax;
		}

		// Token: 0x06000C9A RID: 3226 RVA: 0x0004896A File Offset: 0x00046D6A
		public override long GetFriendshipExp()
		{
			return this.GetUserNamedData().FriendShipExp;
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x00048977 File Offset: 0x00046D77
		public override SkillLearnData GetUniqueSkillLearnData()
		{
			return this.data.Param.UniqueSkillLearnData;
		}

		// Token: 0x06000C9C RID: 3228 RVA: 0x00048989 File Offset: 0x00046D89
		public override List<SkillLearnData> GetClassSkillLearnDatas()
		{
			return this.data.Param.ClassSkillLearnDatas;
		}

		// Token: 0x06000C9D RID: 3229 RVA: 0x0004899B File Offset: 0x00046D9B
		public override int GetHpLvOnly()
		{
			return this.data.Param.Hp;
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x000489AD File Offset: 0x00046DAD
		public override int GetAtkLvOnly()
		{
			return this.data.Param.Atk;
		}

		// Token: 0x06000C9F RID: 3231 RVA: 0x000489BF File Offset: 0x00046DBF
		public override int GetDefLvOnly()
		{
			return this.data.Param.Def;
		}

		// Token: 0x06000CA0 RID: 3232 RVA: 0x000489D1 File Offset: 0x00046DD1
		public override int GetMDefLvOnly()
		{
			return this.data.Param.MDef;
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x000489E3 File Offset: 0x00046DE3
		public override int GetMgcLvOnly()
		{
			return this.data.Param.Mgc;
		}

		// Token: 0x06000CA2 RID: 3234 RVA: 0x000489F5 File Offset: 0x00046DF5
		public override int GetSpdLvOnly()
		{
			return this.data.Param.Spd;
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x00048A07 File Offset: 0x00046E07
		public override int GetLuckLvOnly()
		{
			return this.data.Param.Luck;
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x00048A19 File Offset: 0x00046E19
		public UserNamedData GetUserNamedData()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(this.GetCharaNamedType());
		}
	}
}
