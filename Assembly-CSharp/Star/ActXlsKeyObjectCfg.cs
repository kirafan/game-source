﻿using System;

namespace Star
{
	// Token: 0x02000579 RID: 1401
	public class ActXlsKeyObjectCfg : ActXlsKeyBase
	{
		// Token: 0x06001B2F RID: 6959 RVA: 0x0008FA5A File Offset: 0x0008DE5A
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_OffsetLayer);
			pio.Float(ref this.m_OffsetZ);
			pio.String(ref this.m_TargetName);
		}

		// Token: 0x04002222 RID: 8738
		public int m_OffsetLayer;

		// Token: 0x04002223 RID: 8739
		public float m_OffsetZ;

		// Token: 0x04002224 RID: 8740
		public string m_TargetName;
	}
}
