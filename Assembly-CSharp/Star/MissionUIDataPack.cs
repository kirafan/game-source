﻿using System;

namespace Star
{
	// Token: 0x02000508 RID: 1288
	public class MissionUIDataPack
	{
		// Token: 0x06001946 RID: 6470 RVA: 0x000835BE File Offset: 0x000819BE
		public int GetListNum()
		{
			return this.m_Table.Length;
		}

		// Token: 0x06001947 RID: 6471 RVA: 0x000835C8 File Offset: 0x000819C8
		public IMissionUIData GetTable(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x04002026 RID: 8230
		public IMissionUIData[] m_Table;
	}
}
