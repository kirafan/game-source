﻿using System;

namespace Star
{
	// Token: 0x020002D0 RID: 720
	public abstract class EquipWeaponCharacterDataControllerExistExGen<ThisType, WrapperType> : EquipWeaponCharacterDataControllerExist where ThisType : EquipWeaponCharacterDataControllerExistExGen<ThisType, WrapperType> where WrapperType : CharacterDataWrapperBase
	{
		// Token: 0x06000E03 RID: 3587 RVA: 0x0004D4C4 File Offset: 0x0004B8C4
		public EquipWeaponCharacterDataControllerExistExGen(eCharacterDataControllerType in_type) : base(in_type)
		{
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x0004D4CD File Offset: 0x0004B8CD
		protected ThisType ApplyCharacterDataEx(CharacterDataWrapperBase in_memberDataEx)
		{
			this.memberData_Ex = (in_memberDataEx as WrapperType);
			return (ThisType)((object)base.ApplyCharacterData(in_memberDataEx));
		}

		// Token: 0x06000E05 RID: 3589 RVA: 0x0004D4EC File Offset: 0x0004B8EC
		public WrapperType GetDataEx()
		{
			if (this.memberData_Ex == null)
			{
			}
			return this.memberData_Ex;
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x0004D504 File Offset: 0x0004B904
		protected WrapperType GetDataInternal()
		{
			return this.memberData_Ex;
		}

		// Token: 0x04001595 RID: 5525
		private WrapperType memberData_Ex;
	}
}
