﻿using System;
using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200029D RID: 669
	public class WebViewDebugMain : MonoBehaviour
	{
		// Token: 0x06000C5D RID: 3165 RVA: 0x00048404 File Offset: 0x00046804
		private void Start()
		{
			this.m_UI.Setup();
			for (int i = 0; i < this.m_TabInformations.Length; i++)
			{
				this.m_UI.AddTab(this.m_TabInformations[i]);
			}
			this.OnChangeMargin();
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x0004844E File Offset: 0x0004684E
		private void LoadURL()
		{
			if (this.m_UI == null)
			{
				return;
			}
			if (this.m_TabInformations == null)
			{
				return;
			}
			this.m_UI.Open();
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x00048479 File Offset: 0x00046879
		private int GetSliderValue(Slider slider)
		{
			if (slider == null)
			{
				return 0;
			}
			return (int)slider.value;
		}

		// Token: 0x06000C60 RID: 3168 RVA: 0x00048490 File Offset: 0x00046890
		private void UpdateSliderText(Slider slider)
		{
			if (slider == null)
			{
				return;
			}
			Transform transform = slider.gameObject.transform.FindChild("Text");
			if (transform == null)
			{
				return;
			}
			GameObject gameObject = transform.gameObject;
			if (gameObject == null)
			{
				return;
			}
			Text component = gameObject.GetComponent<Text>();
			if (component == null)
			{
				return;
			}
			component.text = slider.gameObject.name + " : " + slider.value.ToString() + "px.";
		}

		// Token: 0x06000C61 RID: 3169 RVA: 0x0004852C File Offset: 0x0004692C
		public void OnClickOnOffButton()
		{
			if (!this.m_UI.IsOpenWindow())
			{
				this.LoadURL();
				if (this.m_OnOffButtonText != null)
				{
					this.m_OnOffButtonText.text = "Off";
				}
			}
			else
			{
				this.m_UI.Close();
				if (this.m_OnOffButtonText != null)
				{
					this.m_OnOffButtonText.text = "On";
				}
			}
		}

		// Token: 0x06000C62 RID: 3170 RVA: 0x000485A1 File Offset: 0x000469A1
		public void OnClickHomeButton()
		{
			if (this.m_UI.IsOpenWindow())
			{
				this.LoadURL();
			}
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x000485BC File Offset: 0x000469BC
		public void OnChangeMargin()
		{
			if (this.m_UI == null)
			{
				return;
			}
			this.UpdateSliderText(this.m_MarginLeft);
			this.UpdateSliderText(this.m_MarginTop);
			this.UpdateSliderText(this.m_MarginRight);
			this.UpdateSliderText(this.m_MarginBottom);
			this.m_UI.SetViewMargin(this.GetSliderValue(this.m_MarginLeft), this.GetSliderValue(this.m_MarginTop), this.GetSliderValue(this.m_MarginRight), this.GetSliderValue(this.m_MarginBottom));
		}

		// Token: 0x04001530 RID: 5424
		[SerializeField]
		private WebViewWindowUI m_UI;

		// Token: 0x04001531 RID: 5425
		[SerializeField]
		private Text m_OnOffButtonText;

		// Token: 0x04001532 RID: 5426
		[SerializeField]
		private WebViewWindowTabInformation[] m_TabInformations;

		// Token: 0x04001533 RID: 5427
		[SerializeField]
		private Slider m_MarginLeft;

		// Token: 0x04001534 RID: 5428
		[SerializeField]
		private Slider m_MarginTop;

		// Token: 0x04001535 RID: 5429
		[SerializeField]
		private Slider m_MarginRight;

		// Token: 0x04001536 RID: 5430
		[SerializeField]
		private Slider m_MarginBottom;
	}
}
