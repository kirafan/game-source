﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020000C7 RID: 199
	public class BattleKiraraJumpScene
	{
		// Token: 0x0600055B RID: 1371 RVA: 0x0001B9DE File Offset: 0x00019DDE
		public bool Prepare(BattleSystem system, Camera camera, List<CharacterHandler> charaHndls)
		{
			if (this.m_Mode != BattleKiraraJumpScene.eMode.None)
			{
				return false;
			}
			this.m_System = system;
			this.m_Camera = camera;
			this.m_CharaHndls = charaHndls;
			this.m_Mode = BattleKiraraJumpScene.eMode.Prepare;
			return true;
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x0001BA0B File Offset: 0x00019E0B
		public bool IsCompltePrepare()
		{
			return this.m_Mode == BattleKiraraJumpScene.eMode.Prepare && this.m_PrepareStep == BattleKiraraJumpScene.ePrepareStep.End;
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x0001BA24 File Offset: 0x00019E24
		public bool IsPrepareError()
		{
			return this.m_Mode == BattleKiraraJumpScene.eMode.Prepare && this.m_PrepareStep == BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare_Error;
		}

		// Token: 0x0600055E RID: 1374 RVA: 0x0001BA3D File Offset: 0x00019E3D
		public bool Destory()
		{
			if (!this.IsCompltePlay())
			{
				return false;
			}
			this.m_Mode = BattleKiraraJumpScene.eMode.Destroy;
			return true;
		}

		// Token: 0x0600055F RID: 1375 RVA: 0x0001BA54 File Offset: 0x00019E54
		public bool IsComplteDestory()
		{
			return this.m_Mode == BattleKiraraJumpScene.eMode.Destroy && this.m_DestroyStep == BattleKiraraJumpScene.eDestroyStep.End;
		}

		// Token: 0x06000560 RID: 1376 RVA: 0x0001BA6E File Offset: 0x00019E6E
		public bool Play()
		{
			if (!this.IsCompltePrepare())
			{
				return false;
			}
			this.m_Mode = BattleKiraraJumpScene.eMode.UpdateMain;
			return true;
		}

		// Token: 0x06000561 RID: 1377 RVA: 0x0001BA85 File Offset: 0x00019E85
		public bool IsCompltePlay()
		{
			return this.m_Mode == BattleKiraraJumpScene.eMode.UpdateMain && this.m_MainStep == BattleKiraraJumpScene.eMainStep.End;
		}

		// Token: 0x06000562 RID: 1378 RVA: 0x0001BAA0 File Offset: 0x00019EA0
		public void Update()
		{
			BattleKiraraJumpScene.eMode eMode = this.m_Mode;
			BattleKiraraJumpScene.eMode mode = this.m_Mode;
			if (mode != BattleKiraraJumpScene.eMode.Prepare)
			{
				if (mode != BattleKiraraJumpScene.eMode.UpdateMain)
				{
					if (mode == BattleKiraraJumpScene.eMode.Destroy)
					{
						eMode = this.Update_Destroy();
					}
				}
				else
				{
					eMode = this.Update_Main();
				}
			}
			else
			{
				eMode = this.Update_Prepare();
			}
			if (eMode != this.m_Mode)
			{
				this.m_Mode = eMode;
				BattleKiraraJumpScene.eMode mode2 = this.m_Mode;
				if (mode2 != BattleKiraraJumpScene.eMode.Prepare)
				{
					if (mode2 != BattleKiraraJumpScene.eMode.UpdateMain)
					{
						if (mode2 == BattleKiraraJumpScene.eMode.Destroy)
						{
							this.SetDestroyStep(BattleKiraraJumpScene.eDestroyStep.First);
						}
					}
					else
					{
						this.SetMainStep(BattleKiraraJumpScene.eMainStep.First);
					}
				}
				else
				{
					this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.First);
				}
			}
		}

		// Token: 0x06000563 RID: 1379 RVA: 0x0001BB4F File Offset: 0x00019F4F
		public List<CharacterHandler> GetSortTargetCharaHndls()
		{
			return this.m_CharaHndls;
		}

		// Token: 0x06000564 RID: 1380 RVA: 0x0001BB57 File Offset: 0x00019F57
		public Renderer[] GetBaseObjectRenderers()
		{
			if (this.m_KiraraJumpObjectHandler != null && this.m_KiraraJumpObjectHandler.MsbHndl != null)
			{
				return this.m_KiraraJumpObjectHandler.MsbHndl.GetRendererCache();
			}
			return null;
		}

		// Token: 0x06000565 RID: 1381 RVA: 0x0001BB94 File Offset: 0x00019F94
		public void SetBaseObjectSortingOrder(int sortingOrder)
		{
			if (this.m_KiraraJumpObjectHandler != null && this.m_KiraraJumpObjectHandler.MsbHndl != null)
			{
				int particleEmitterNum = this.m_KiraraJumpObjectHandler.MsbHndl.GetParticleEmitterNum();
				for (int i = 0; i < particleEmitterNum; i++)
				{
					this.m_KiraraJumpObjectHandler.MsbHndl.GetParticleEmitter(i).SetSortingOrder(sortingOrder);
				}
			}
		}

		// Token: 0x06000566 RID: 1382 RVA: 0x0001BC02 File Offset: 0x0001A002
		private void SetDestroyStep(BattleKiraraJumpScene.eDestroyStep step)
		{
			this.m_DestroyStep = step;
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x0001BC0C File Offset: 0x0001A00C
		private BattleKiraraJumpScene.eMode Update_Destroy()
		{
			this.m_Loader.Update();
			switch (this.m_DestroyStep)
			{
			case BattleKiraraJumpScene.eDestroyStep.First:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(0f);
				if (this.m_System != null)
				{
					this.m_System.BattleUI.GetTransitFade().PlayInQuick(this.FADE_COLOR);
				}
				this.m_KiraraJumpObjectHandler.Pause();
				this.SetDestroyStep(BattleKiraraJumpScene.eDestroyStep.Destroy_0);
				break;
			case BattleKiraraJumpScene.eDestroyStep.Destroy_0:
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.UnloadAll(false);
					this.m_LoadingHndl = null;
				}
				this.SetDestroyStep(BattleKiraraJumpScene.eDestroyStep.Destroy_1);
				break;
			case BattleKiraraJumpScene.eDestroyStep.Destroy_1:
				if (this.m_Loader.IsCompleteUnloadAll())
				{
					this.m_KiraraJumpObjectHandler.Destroy();
					this.m_KiraraJumpObjectHandler = null;
					this.m_CharaHndls = null;
					CharacterManager charaMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng;
					int charaNum = charaMng.GetCharaNum();
					for (int i = 0; i < charaNum; i++)
					{
						CharacterHandler charaAt = charaMng.GetCharaAt(i);
						if (charaAt != null && charaAt.CharaBattle != null)
						{
							charaAt.CharaBattle.RevertFromKiraraJumpScene();
						}
					}
					if (this.m_System != null)
					{
						this.m_System.RevertKiraraJumpSceneToSystem();
					}
					UnityEngine.Object.Destroy(this.m_KiraraJumpObject);
					this.m_KiraraJumpObject = null;
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.SetDestroyStep(BattleKiraraJumpScene.eDestroyStep.Destroy_2);
				}
				break;
			case BattleKiraraJumpScene.eDestroyStep.Destroy_2:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
				{
					this.SetDestroyStep(BattleKiraraJumpScene.eDestroyStep.End);
				}
				break;
			}
			return BattleKiraraJumpScene.eMode.Destroy;
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0001BDB6 File Offset: 0x0001A1B6
		private void SetMainStep(BattleKiraraJumpScene.eMainStep step)
		{
			this.m_MainStep = step;
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0001BDC0 File Offset: 0x0001A1C0
		private BattleKiraraJumpScene.eMode Update_Main()
		{
			BattleKiraraJumpScene.eMainStep mainStep = this.m_MainStep;
			if (mainStep != BattleKiraraJumpScene.eMainStep.First)
			{
				if (mainStep != BattleKiraraJumpScene.eMainStep.Playing)
				{
					if (mainStep != BattleKiraraJumpScene.eMainStep.End)
					{
					}
				}
				else
				{
					this.m_System.RequestSort();
					if (!this.m_WasStartedLastFadeOut)
					{
						float num = this.m_CharaHndls[0].CharaAnim.GetMaxSec("kirarajump_0", 0) - 0.2f;
						if (this.m_CharaHndls[0].CharaAnim.GetPlayingSec("kirarajump_0", 0) >= num)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(this.FADE_COLOR, 0.2f, null);
							this.m_WasStartedLastFadeOut = true;
						}
					}
					if (!this.m_KiraraJumpObjectHandler.IsPlaying())
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
						{
							this.SetMainStep(BattleKiraraJumpScene.eMainStep.End);
						}
					}
				}
			}
			else
			{
				this.m_KiraraJumpObjectHandler.Play();
				if (this.m_System != null)
				{
					this.m_System.BattleUI.FinishTogetherCutIn();
				}
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.2f, null);
				this.SetMainStep(BattleKiraraJumpScene.eMainStep.Playing);
			}
			return BattleKiraraJumpScene.eMode.UpdateMain;
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0001BEFC File Offset: 0x0001A2FC
		private void SetPrepareStep(BattleKiraraJumpScene.ePrepareStep step)
		{
			this.m_PrepareStep = step;
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0001BF08 File Offset: 0x0001A308
		private BattleKiraraJumpScene.eMode Update_Prepare()
		{
			this.m_Loader.Update();
			switch (this.m_PrepareStep)
			{
			case BattleKiraraJumpScene.ePrepareStep.First:
				this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.BattleSystem_PreProcess);
				break;
			case BattleKiraraJumpScene.ePrepareStep.BattleSystem_PreProcess:
			{
				CharacterManager charaMng = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng;
				int charaNum = charaMng.GetCharaNum();
				for (int i = 0; i < charaNum; i++)
				{
					CharacterHandler charaAt = charaMng.GetCharaAt(i);
					if (charaAt != null)
					{
						if (charaAt.CharaBattle != null)
						{
							charaAt.CharaBattle.SetupForKiraraJumpScene();
						}
						if (charaAt.CharaAnim != null)
						{
							charaAt.CharaAnim.SetSortingOrder(0);
						}
						if (!this.m_CharaHndls.Contains(charaAt))
						{
							charaAt.SetEnableRender(false);
						}
					}
				}
				if (this.m_System != null)
				{
					this.m_System.KiraraJumpScenePreProcess();
				}
				this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare);
				break;
			}
			case BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare:
				this.m_LoadingHndl = this.m_Loader.Load("battle/kiraraJump/kiraraJump.muast", new MeigeResource.Option[0]);
				if (this.m_LoadingHndl == null)
				{
				}
				this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare_Wait);
				break;
			case BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare_Wait:
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.KiraraJumpPrepare_Error);
						break;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						break;
					}
				}
				this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.Setup);
				break;
			case BattleKiraraJumpScene.ePrepareStep.Setup:
			{
				GameObject gameObject = this.m_LoadingHndl.FindObj("KiraraJump", true) as GameObject;
				GameObject gameObject2 = this.m_LoadingHndl.FindObj("MeigeAC_KiraraJump@Take 001", true) as GameObject;
				this.m_KiraraJumpObject = UnityEngine.Object.Instantiate<GameObject>(gameObject);
				this.m_KiraraJumpObjectHandler = this.m_KiraraJumpObject.AddComponent<BattleKiraraJumpObjectHandler>();
				MeigeAnimClipHolder componentInChildren = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
				this.m_KiraraJumpObjectHandler.Setup(gameObject.name, componentInChildren, this.m_CharaHndls);
				this.m_KiraraJumpObjectHandler.AttachCamera(this.m_Camera);
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.UnloadAll(false);
					this.m_LoadingHndl = null;
				}
				this.SetPrepareStep(BattleKiraraJumpScene.ePrepareStep.End);
				break;
			}
			case BattleKiraraJumpScene.ePrepareStep.End:
				if (this.m_Loader.IsCompleteUnloadAll())
				{
					return BattleKiraraJumpScene.eMode.UpdateMain;
				}
				break;
			}
			return BattleKiraraJumpScene.eMode.Prepare;
		}

		// Token: 0x04000421 RID: 1057
		private const string RESOURCE_PATH = "battle/kiraraJump/kiraraJump.muast";

		// Token: 0x04000422 RID: 1058
		private const string OBJ_MODEL_PATH = "KiraraJump";

		// Token: 0x04000423 RID: 1059
		private const string OBJ_ANIM_PATH = "MeigeAC_KiraraJump@Take 001";

		// Token: 0x04000424 RID: 1060
		private BattleKiraraJumpScene.eMode m_Mode = BattleKiraraJumpScene.eMode.None;

		// Token: 0x04000425 RID: 1061
		private readonly Color FADE_COLOR = Color.white;

		// Token: 0x04000426 RID: 1062
		private const float FADE_TIME = 0.2f;

		// Token: 0x04000427 RID: 1063
		private BattleSystem m_System;

		// Token: 0x04000428 RID: 1064
		private Camera m_Camera;

		// Token: 0x04000429 RID: 1065
		private List<CharacterHandler> m_CharaHndls;

		// Token: 0x0400042A RID: 1066
		private GameObject m_KiraraJumpObject;

		// Token: 0x0400042B RID: 1067
		private BattleKiraraJumpObjectHandler m_KiraraJumpObjectHandler;

		// Token: 0x0400042C RID: 1068
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x0400042D RID: 1069
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x0400042E RID: 1070
		private BattleKiraraJumpScene.eDestroyStep m_DestroyStep;

		// Token: 0x0400042F RID: 1071
		private const string ANIM_KEY = "kirarajump_0";

		// Token: 0x04000430 RID: 1072
		private const float FADE_SEC = 0.2f;

		// Token: 0x04000431 RID: 1073
		private const float LAST_FADEOUT_START_TIME = 0.2f;

		// Token: 0x04000432 RID: 1074
		private bool m_WasStartedLastFadeOut;

		// Token: 0x04000433 RID: 1075
		private BattleKiraraJumpScene.eMainStep m_MainStep;

		// Token: 0x04000434 RID: 1076
		private BattleKiraraJumpScene.ePrepareStep m_PrepareStep;

		// Token: 0x020000C8 RID: 200
		public enum eMode
		{
			// Token: 0x04000436 RID: 1078
			None = -1,
			// Token: 0x04000437 RID: 1079
			Prepare,
			// Token: 0x04000438 RID: 1080
			UpdateMain,
			// Token: 0x04000439 RID: 1081
			Destroy
		}

		// Token: 0x020000C9 RID: 201
		private enum eDestroyStep
		{
			// Token: 0x0400043B RID: 1083
			None = -1,
			// Token: 0x0400043C RID: 1084
			First,
			// Token: 0x0400043D RID: 1085
			Destroy_0,
			// Token: 0x0400043E RID: 1086
			Destroy_1,
			// Token: 0x0400043F RID: 1087
			Destroy_2,
			// Token: 0x04000440 RID: 1088
			End
		}

		// Token: 0x020000CA RID: 202
		private enum eMainStep
		{
			// Token: 0x04000442 RID: 1090
			None = -1,
			// Token: 0x04000443 RID: 1091
			First,
			// Token: 0x04000444 RID: 1092
			Playing,
			// Token: 0x04000445 RID: 1093
			End
		}

		// Token: 0x020000CB RID: 203
		private enum ePrepareStep
		{
			// Token: 0x04000447 RID: 1095
			None = -1,
			// Token: 0x04000448 RID: 1096
			First,
			// Token: 0x04000449 RID: 1097
			BattleSystem_PreProcess,
			// Token: 0x0400044A RID: 1098
			KiraraJumpPrepare,
			// Token: 0x0400044B RID: 1099
			KiraraJumpPrepare_Wait,
			// Token: 0x0400044C RID: 1100
			KiraraJumpPrepare_Error,
			// Token: 0x0400044D RID: 1101
			Setup,
			// Token: 0x0400044E RID: 1102
			End
		}
	}
}
