﻿using System;

namespace Star
{
	// Token: 0x0200046E RID: 1134
	public class RoomComUI : IMainComCommand
	{
		// Token: 0x060015F9 RID: 5625 RVA: 0x0007266C File Offset: 0x00070A6C
		public override int GetHashCode()
		{
			return 2;
		}

		// Token: 0x04001CB4 RID: 7348
		public eRoomUICategory m_OpenUI;

		// Token: 0x04001CB5 RID: 7349
		public long m_ManageID;

		// Token: 0x04001CB6 RID: 7350
		public int m_CharaID;

		// Token: 0x04001CB7 RID: 7351
		public IRoomObjectControll m_SelectHandle;
	}
}
