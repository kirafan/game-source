﻿using System;

namespace Star
{
	// Token: 0x0200030E RID: 782
	[Serializable]
	public struct ScheduleSetupDB_Param
	{
		// Token: 0x04001632 RID: 5682
		public int m_ID;

		// Token: 0x04001633 RID: 5683
		public ushort m_BuildPriority;

		// Token: 0x04001634 RID: 5684
		public int m_ScheduleTagSelf;

		// Token: 0x04001635 RID: 5685
		public int m_ScheduleTagOther;

		// Token: 0x04001636 RID: 5686
		public int m_Content;

		// Token: 0x04001637 RID: 5687
		public ushort m_MakePoint;

		// Token: 0x04001638 RID: 5688
		public ushort m_Week;

		// Token: 0x04001639 RID: 5689
		public ushort m_Holyday;

		// Token: 0x0400163A RID: 5690
		public ushort m_BuildType;

		// Token: 0x0400163B RID: 5691
		public int m_StartTime;

		// Token: 0x0400163C RID: 5692
		public int m_EndTime;

		// Token: 0x0400163D RID: 5693
		public int m_StartLife;

		// Token: 0x0400163E RID: 5694
		public int m_EndLife;

		// Token: 0x0400163F RID: 5695
		public short m_BuildUpPer;
	}
}
