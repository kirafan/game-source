﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200023E RID: 574
	public class TipsDB : ScriptableObject
	{
		// Token: 0x04001308 RID: 4872
		public TipsDB_Param[] m_Params;
	}
}
