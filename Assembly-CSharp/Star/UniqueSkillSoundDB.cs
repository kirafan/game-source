﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200025F RID: 607
	public class UniqueSkillSoundDB : ScriptableObject
	{
		// Token: 0x040013BE RID: 5054
		public UniqueSkillSoundDB_Param[] m_Params;
	}
}
