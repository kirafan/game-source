﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000309 RID: 777
	public class ScheduleHolydayDB : ScriptableObject
	{
		// Token: 0x0400160F RID: 5647
		public ScheduleHolydayDB_Param[] m_Params;
	}
}
