﻿using System;
using System.Collections;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x0200065C RID: 1628
	public class NotificationController
	{
		// Token: 0x060020EF RID: 8431 RVA: 0x000AF94E File Offset: 0x000ADD4E
		public NotificationController()
		{
			this.PushFlags = new bool[3];
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x060020F0 RID: 8432 RVA: 0x000AF962 File Offset: 0x000ADD62
		// (set) Token: 0x060020F1 RID: 8433 RVA: 0x000AF96A File Offset: 0x000ADD6A
		public bool[] PushFlags { get; set; }

		// Token: 0x14000033 RID: 51
		// (add) Token: 0x060020F2 RID: 8434 RVA: 0x000AF974 File Offset: 0x000ADD74
		// (remove) Token: 0x060020F3 RID: 8435 RVA: 0x000AF9AC File Offset: 0x000ADDAC
		private event Action m_OnResponse_SetPushNotification;

		// Token: 0x060020F4 RID: 8436 RVA: 0x000AF9E4 File Offset: 0x000ADDE4
		public void Save()
		{
			this.m_SavedPushFlags = new bool[this.PushFlags.Length];
			for (int i = 0; i < this.m_SavedPushFlags.Length; i++)
			{
				this.m_SavedPushFlags[i] = this.PushFlags[i];
			}
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x000AFA30 File Offset: 0x000ADE30
		public void RestoreFromSave()
		{
			if (this.m_SavedPushFlags == null)
			{
				return;
			}
			this.PushFlags = new bool[this.m_SavedPushFlags.Length];
			for (int i = 0; i < this.PushFlags.Length; i++)
			{
				this.PushFlags[i] = this.m_SavedPushFlags[i];
			}
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x000AFA88 File Offset: 0x000ADE88
		public bool IsDirty()
		{
			if (this.m_SavedPushFlags == null || this.PushFlags == null)
			{
				return false;
			}
			if (this.m_SavedPushFlags.Length != this.PushFlags.Length)
			{
				return false;
			}
			for (int i = 0; i < this.m_SavedPushFlags.Length; i++)
			{
				if (this.m_SavedPushFlags[i] != this.PushFlags[i])
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060020F7 RID: 8439 RVA: 0x000AFAF8 File Offset: 0x000ADEF8
		public bool GetPushFlag(NotificationController.ePush type)
		{
			return this.PushFlags != null && type < (NotificationController.ePush)this.PushFlags.Length && this.PushFlags[(int)type];
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x000AFB30 File Offset: 0x000ADF30
		public void SetPushFlag(NotificationController.ePush type, bool isOn)
		{
			if (this.PushFlags != null && type < (NotificationController.ePush)this.PushFlags.Length)
			{
				this.PushFlags[(int)type] = isOn;
			}
		}

		// Token: 0x060020F9 RID: 8441 RVA: 0x000AFB64 File Offset: 0x000ADF64
		public void SetPushFlagsAll(bool isOn)
		{
			if (this.PushFlags != null)
			{
				for (int i = 0; i < this.PushFlags.Length; i++)
				{
					this.PushFlags[i] = isOn;
				}
			}
		}

		// Token: 0x060020FA RID: 8442 RVA: 0x000AFBA0 File Offset: 0x000ADFA0
		public void Request_SetPushNotification(Action response)
		{
			this.m_OnResponse_SetPushNotification = response;
			MeigewwwParam wwwParam = PlayerRequest.Setpushnotification(new PlayerRequestTypes.Setpushnotification
			{
				flag3 = ((!this.GetPushFlag(NotificationController.ePush.ForUIFlag)) ? 0 : 1),
				flag1 = ((!this.GetPushFlag(NotificationController.ePush.InfoFromMng)) ? 0 : 1),
				flag2 = ((!this.GetPushFlag(NotificationController.ePush.StaminaFull)) ? 0 : 1)
			}, new MeigewwwParam.Callback(this.OnResponse_SetPushNotification));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060020FB RID: 8443 RVA: 0x000AFC30 File Offset: 0x000AE030
		public void OnResponse_SetPushNotification(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Setpushnotification setpushnotification = PlayerResponse.Setpushnotification(wwwParam, ResponseCommon.DialogType.None, null);
			if (setpushnotification == null)
			{
				return;
			}
			ResultCode result = setpushnotification.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				this.m_OnResponse_SetPushNotification.Call();
				this.m_OnResponse_SetPushNotification = null;
			}
			else
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToString());
			}
		}

		// Token: 0x060020FC RID: 8444 RVA: 0x000AFC84 File Offset: 0x000AE084
		public void OnAwake()
		{
			this.Cancel_StaminaFull();
		}

		// Token: 0x060020FD RID: 8445 RVA: 0x000AFC8C File Offset: 0x000AE08C
		public void OnQuit()
		{
		}

		// Token: 0x060020FE RID: 8446 RVA: 0x000AFC90 File Offset: 0x000AE090
		public void LocalPush_StaminaFull()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				if (this.m_Coroutine_StaminaFull != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.StopCoroutine(this.m_Coroutine_StaminaFull);
					this.m_Coroutine_StaminaFull = null;
				}
				this.m_Coroutine_StaminaFull = SingletonMonoBehaviour<GameSystem>.Inst.StartCoroutine(this.StaminaFullProc());
			}
		}

		// Token: 0x060020FF RID: 8447 RVA: 0x000AFCE8 File Offset: 0x000AE0E8
		private IEnumerator StaminaFullProc()
		{
			this.Cancel_StaminaFull();
			yield return new WaitForSeconds(0.5f);
			this.Push_StaminaFull();
			yield break;
		}

		// Token: 0x06002100 RID: 8448 RVA: 0x000AFD03 File Offset: 0x000AE103
		public void CancelAll()
		{
			LocalNotificationService.CancellAll();
		}

		// Token: 0x06002101 RID: 8449 RVA: 0x000AFD0C File Offset: 0x000AE10C
		public void Push_StaminaFull()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng != null)
			{
				GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
				bool pushFlag = this.GetPushFlag(NotificationController.ePush.InfoFromMng);
				if (pushFlag && !inst.UserDataMng.UserData.Stamina.IsFull())
				{
					TimeSpan timeToCompleteRecovery = inst.UserDataMng.UserData.Stamina.GetTimeToCompleteRecovery();
					string textMessage = inst.DbMng.GetTextMessage(eText_MessageDB.PushNotifiStaminaFullTitle);
					string textMessage2 = inst.DbMng.GetTextMessage(eText_MessageDB.PushNotifiStaminaFull);
					LocalNotificationService.SimpleNotification(100, timeToCompleteRecovery, textMessage, textMessage2, "m_icon", "large", default(Color));
				}
			}
		}

		// Token: 0x06002102 RID: 8450 RVA: 0x000AFDBE File Offset: 0x000AE1BE
		public void Cancel_StaminaFull()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng != null)
			{
				LocalNotificationService.CancelNotification(100);
			}
		}

		// Token: 0x06002103 RID: 8451 RVA: 0x000AFDE6 File Offset: 0x000AE1E6
		public void LocalPush_Test()
		{
		}

		// Token: 0x04002725 RID: 10021
		public const int ID_StaminaFull = 100;

		// Token: 0x04002727 RID: 10023
		private bool[] m_SavedPushFlags;

		// Token: 0x04002729 RID: 10025
		private Coroutine m_Coroutine_StaminaFull;

		// Token: 0x0200065D RID: 1629
		public enum ePush
		{
			// Token: 0x0400272B RID: 10027
			ForUIFlag,
			// Token: 0x0400272C RID: 10028
			InfoFromMng,
			// Token: 0x0400272D RID: 10029
			StaminaFull,
			// Token: 0x0400272E RID: 10030
			Num
		}
	}
}
