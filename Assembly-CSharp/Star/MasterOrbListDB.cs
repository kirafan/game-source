﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001D1 RID: 465
	public class MasterOrbListDB : ScriptableObject
	{
		// Token: 0x04000B0B RID: 2827
		public MasterOrbListDB_Param[] m_Params;
	}
}
