﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000607 RID: 1543
	[Serializable]
	public class RoomCacheObjectData
	{
		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06001E4F RID: 7759 RVA: 0x000A378D File Offset: 0x000A1B8D
		public int CacheSize
		{
			get
			{
				return this.m_CacheSize;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06001E50 RID: 7760 RVA: 0x000A3795 File Offset: 0x000A1B95
		public GameObject[] CacheObjects
		{
			get
			{
				return this.m_Objects;
			}
		}

		// Token: 0x06001E51 RID: 7761 RVA: 0x000A37A0 File Offset: 0x000A1BA0
		public void Initialize(Transform parent)
		{
			this.m_Objects = new GameObject[this.m_CacheSize];
			this.m_Parent = parent;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				this.m_Objects[i] = new GameObject();
				this.m_Objects[i].SetActive(false);
				this.m_Objects[i].name = "RoomObject" + i;
				this.m_Objects[i].transform.SetParent(this.m_Parent, false);
			}
		}

		// Token: 0x06001E52 RID: 7762 RVA: 0x000A3830 File Offset: 0x000A1C30
		public void Destroy()
		{
			if (this.m_Objects != null)
			{
				for (int i = 0; i < this.m_Objects.Length; i++)
				{
					UnityEngine.Object.Destroy(this.m_Objects[i]);
					this.m_Objects[i] = null;
				}
				this.m_Objects = null;
			}
			this.m_CacheSize = 0;
		}

		// Token: 0x06001E53 RID: 7763 RVA: 0x000A3888 File Offset: 0x000A1C88
		public GameObject GetNextObjectInCache()
		{
			GameObject gameObject = null;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				if (!this.m_Objects[i].activeSelf)
				{
					gameObject = this.m_Objects[i];
					break;
				}
			}
			if (gameObject == null)
			{
				GameObject[] array = new GameObject[this.m_CacheSize + 10];
				for (int j = 0; j < this.m_CacheSize; j++)
				{
					array[j] = this.m_Objects[j];
				}
				for (int j = this.m_CacheSize; j < this.m_CacheSize + 10; j++)
				{
					array[j] = new GameObject();
					array[j].SetActive(false);
					array[j].name = "RoomObject" + j;
					array[j].transform.SetParent(this.m_Parent, false);
				}
				this.m_Objects = null;
				this.m_Objects = array;
				gameObject = this.m_Objects[this.m_CacheSize];
				this.m_CacheSize += 10;
			}
			return gameObject;
		}

		// Token: 0x06001E54 RID: 7764 RVA: 0x000A3993 File Offset: 0x000A1D93
		public void BackObject(GameObject pobj)
		{
			pobj.SetActive(false);
		}

		// Token: 0x040024DD RID: 9437
		[SerializeField]
		private int m_CacheSize = 50;

		// Token: 0x040024DE RID: 9438
		private GameObject[] m_Objects;

		// Token: 0x040024DF RID: 9439
		private Transform m_Parent;
	}
}
