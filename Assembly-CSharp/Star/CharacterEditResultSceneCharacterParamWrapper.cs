﻿using System;

namespace Star
{
	// Token: 0x020002AB RID: 683
	public class CharacterEditResultSceneCharacterParamWrapper : UserCharacterDataWrapperBase
	{
		// Token: 0x06000CDD RID: 3293 RVA: 0x00048FCE File Offset: 0x000473CE
		public CharacterEditResultSceneCharacterParamWrapper(eCharacterDataType characterDataType, EditMain.EditAdditiveSceneResultData result) : base(characterDataType, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(result.m_CharaMngID))
		{
		}
	}
}
