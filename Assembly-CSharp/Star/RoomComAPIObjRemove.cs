﻿using System;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000593 RID: 1427
	public class RoomComAPIObjRemove : INetComHandle
	{
		// Token: 0x06001BB1 RID: 7089 RVA: 0x00092A2D File Offset: 0x00090E2D
		public RoomComAPIObjRemove()
		{
			this.ApiName = "player/room_object/remove";
			this.Request = true;
			this.ResponseType = typeof(Remove);
		}

		// Token: 0x04002296 RID: 8854
		public long managedRoomObjectId;
	}
}
