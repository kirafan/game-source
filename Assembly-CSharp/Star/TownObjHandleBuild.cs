﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006D8 RID: 1752
	public class TownObjHandleBuild : TownObjModelHandle
	{
		// Token: 0x060022A9 RID: 8873 RVA: 0x000B9AB8 File Offset: 0x000B7EB8
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_MakerPos = param.m_MarkerPos;
			this.m_BaseSize = param.m_BaseSize;
			this.m_Offset.x = param.m_OffsetX;
			this.m_Offset.y = param.m_OffsetY;
			this.m_Offset.z = 1f;
			this.m_ResourceID = param.m_ResourceID * 100;
			if ((param.m_Arg & 1) == 0 && townBuildData.m_Lv != 0)
			{
				this.m_ResourceID += townBuildData.m_Lv - 1;
			}
			this.m_GaugeUp = false;
			this.m_ModelMarkUp = false;
			this.m_ItemType = 0;
			bool building = true;
			if (param.m_Category == 1)
			{
				this.m_ItemType = 1;
			}
			this.m_Category = (eTownObjectCategory)param.m_Category;
			if (UserTownUtil.IsBuildMarks(objID) && townBuildData != null && !townBuildData.m_IsOpen)
			{
				this.m_GaugeUp = true;
				building = false;
				if (finitbuild)
				{
					this.m_InitGaugeUp = true;
				}
			}
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(fmanageid, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(building);
			}
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			this.m_BindCtrl = this.m_Builder.GetCharaBind(this.m_ManageID, true);
			this.m_BindCtrl.SetLinkBindKey(this.m_Transform, this.m_LayerID, this.m_MakerPos);
			FieldMapManager.EntryBuildState(fmanageid, new FeedBackResultCallback(this.CallbackBufBuild));
		}

		// Token: 0x060022AA RID: 8874 RVA: 0x000B9C50 File Offset: 0x000B8050
		protected override bool PrepareMain()
		{
			if (!base.PrepareMain())
			{
				return false;
			}
			this.m_Obj.transform.localPosition = this.m_Offset;
			base.MakeColliderLink(this.m_Obj);
			if (this.m_GaugeUp)
			{
				TownPartsBuildGauge paction = new TownPartsBuildGauge(this.m_Builder, this.m_Transform, this.m_ManageID, this.m_LayerID + 5, (int)this.m_TimeKey, this.m_InitGaugeUp, false);
				TownPartsBuildGauge townPartsBuildGauge = this.m_Action.GetAction(typeof(TownPartsBuildGauge)) as TownPartsBuildGauge;
				if (townPartsBuildGauge != null)
				{
				}
				this.m_Action.EntryAction(paction, 0);
				base.SetRenderFlg(this.m_Obj, false);
				this.m_ModelMarkUp = true;
				if (!this.m_InitGaugeUp)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_BUILD_START, 1f, 0, -1, -1);
				}
			}
			this.m_Obj.transform.localScale = Vector3.one * this.m_BaseSize;
			return true;
		}

		// Token: 0x060022AB RID: 8875 RVA: 0x000B9D4F File Offset: 0x000B814F
		public bool IsCompleteBuildEvt()
		{
			if (!this.m_GaugeUp)
			{
				return false;
			}
			if (UserTownUtil.IsManageIDToPreComplete(this.m_ManageID))
			{
				this.m_GaugeUp = false;
				return true;
			}
			return false;
		}

		// Token: 0x060022AC RID: 8876 RVA: 0x000B9D78 File Offset: 0x000B8178
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleBuild.eState.Init:
				if (this.m_IsPreparing && this.PrepareMain())
				{
					this.m_State = TownObjHandleBuild.eState.Main;
				}
				break;
			case TownObjHandleBuild.eState.Main:
			{
				if (this.m_ModelMarkUp && UserTownUtil.IsManageIDToPreComplete(this.m_ManageID))
				{
					this.m_ModelMarkUp = false;
				}
				if (this.m_ReBindPos)
				{
					this.m_BindCtrl.CalcCharaPopIconPos(this.m_ManageID, this.m_LayerID, this.m_MakerPos);
					this.m_ReBindPos = false;
				}
				FieldObjHandleBuild buildState = FieldMapManager.GetBuildState(this.m_ManageID);
				if (buildState != null && buildState.CalcDropItemListNum(ref this.m_ItemUpInfo) && this.m_ItemUpInfo.m_CalcNum != 0)
				{
					if (!this.m_ItemUp)
					{
						Vector3 zero = Vector3.zero;
						zero.y += this.m_MakerPos;
						GameObject generalObj = this.m_Builder.GetGeneralObj(eTownObjectType.BUILD_ITEM_POP);
						generalObj.transform.SetParent(base.transform, false);
						generalObj.transform.localEulerAngles = new Vector3(0f, 180f);
						TownUtility.SetRenderLayer(generalObj, this.m_LayerID + 1);
						this.m_ItemPopModel = generalObj.AddComponent<TownPartsPopObject>();
						this.m_ItemPopModel.SetPopObject(this.m_Builder, generalObj, this, zero);
						this.m_ItemPopModel.ChangeIcon((int)this.m_ItemType);
						this.m_ItemPopModel.ChangePer(this.m_ItemUpInfo.m_CalcNum, this.m_ItemUpInfo.m_Max);
					}
					else
					{
						this.m_ItemPopModel.ChangePer(this.m_ItemUpInfo.m_CalcNum, this.m_ItemUpInfo.m_Max);
					}
					this.m_ItemUp = true;
				}
				break;
			}
			case TownObjHandleBuild.eState.EndStart:
				this.m_State = TownObjHandleBuild.eState.End;
				break;
			case TownObjHandleBuild.eState.End:
				base.Destroy();
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x060022AD RID: 8877 RVA: 0x000B9F68 File Offset: 0x000B8368
		public override void DestroyRequest()
		{
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(this.m_ManageID, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(false);
				buildMngIDTreeNode.ChangeManageID(-1L);
			}
			this.m_State = TownObjHandleBuild.eState.EndStart;
			this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
			if (this.m_ItemPopModel != null)
			{
				this.m_ItemPopModel.DestroyRequest();
				this.m_ItemPopModel = null;
			}
			ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsBuildGauge));
			if (action != null)
			{
				TownPartsBuildGauge townPartsBuildGauge = (TownPartsBuildGauge)action;
				townPartsBuildGauge.SetTerm();
			}
		}

		// Token: 0x060022AE RID: 8878 RVA: 0x000BA020 File Offset: 0x000B8420
		public override void Destroy()
		{
			this.m_State = TownObjHandleBuild.eState.End;
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			if (this.m_Action != null)
			{
				this.m_Action.Release();
				this.m_Action = null;
			}
			if (this.m_ItemPopModel != null)
			{
				this.m_ItemPopModel.DestroyRequest();
				this.m_ItemPopModel = null;
			}
			base.Destroy();
		}

		// Token: 0x060022AF RID: 8879 RVA: 0x000BA088 File Offset: 0x000B8488
		private void ReSetUpHandle(int objID, long fmanageid)
		{
			base.SetupHandle(objID, this.m_BuildAccessID, fmanageid, this.m_LayerID, this.m_TimeKey, false);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_MakerPos = param.m_MarkerPos;
			this.m_BaseSize = param.m_BaseSize;
			this.m_Offset.x = param.m_OffsetX;
			this.m_Offset.y = param.m_OffsetY;
			this.m_Offset.z = 1f;
			this.m_ResourceID = param.m_ResourceID * 100;
			if ((param.m_Arg & 1) == 0)
			{
				UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
				if (townBuildData.m_Lv != 0)
				{
					this.m_ResourceID += townBuildData.m_Lv - 1;
				}
			}
			this.m_Category = (eTownObjectCategory)param.m_Category;
			this.m_TownObjResource.SetupResource(this);
			this.m_TownObjResource.Prepare();
			this.m_IsPreparing = true;
			this.m_IsAvailable = false;
			this.m_State = TownObjHandleBuild.eState.Init;
			this.m_BindCtrl.SetLinkBindKey(this.m_Transform, this.m_LayerID, this.m_MakerPos);
			this.m_BindCtrl.ChangeManageID(fmanageid);
		}

		// Token: 0x060022B0 RID: 8880 RVA: 0x000BA1BC File Offset: 0x000B85BC
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			switch (pact.m_Type)
			{
			case eTownEventAct.OpenSelectUI:
			{
				TownPartsMaker paction = new TownPartsMaker(this.m_Builder, this.m_Transform, this.m_Obj, this.m_LayerID + 10);
				this.m_Action.EntryAction(paction, 0);
				break;
			}
			case eTownEventAct.CloseSelectUI:
			{
				ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsMaker));
				if (action != null)
				{
					TownPartsMaker townPartsMaker = (TownPartsMaker)action;
					townPartsMaker.SetEndMark();
				}
				break;
			}
			case eTownEventAct.ReFlesh:
				this.m_ReBindPos = true;
				break;
			case eTownEventAct.ChangeObject:
			{
				TownHandleActionChangeObj townHandleActionChangeObj = (TownHandleActionChangeObj)pact;
				TownPartsChangeModel paction2 = new TownPartsChangeModel(this.m_Obj, this, townHandleActionChangeObj.m_ObjectID, townHandleActionChangeObj.m_ManageID);
				this.m_Action.EntryAction(paction2, 0);
				break;
			}
			case eTownEventAct.ChangeModel:
			{
				TownHandleActionModelObj townHandleActionModelObj = (TownHandleActionModelObj)pact;
				base.DesModel();
				this.ReSetUpHandle(townHandleActionModelObj.m_ObjectID, townHandleActionModelObj.m_ManageID);
				break;
			}
			case eTownEventAct.StepIn:
				this.m_Obj.transform.localScale = Vector3.one * this.m_BaseSize;
				break;
			case eTownEventAct.SelectOff:
			{
				TownPartsModelBlink townPartsModelBlink = (TownPartsModelBlink)this.m_Action.GetAction(typeof(TownPartsModelBlink));
				if (townPartsModelBlink != null)
				{
					townPartsModelBlink.SetEndMark();
				}
				break;
			}
			case eTownEventAct.SelectTouch:
			{
				TownHandleActionTouchSelect townHandleActionTouchSelect = pact as TownHandleActionTouchSelect;
				TownPartsModelBlink townPartsModelBlink2 = (TownPartsModelBlink)this.m_Action.GetAction(typeof(TownPartsModelBlink));
				if (townHandleActionTouchSelect.m_Select)
				{
					if (townPartsModelBlink2 == null)
					{
						townPartsModelBlink2 = new TownPartsModelBlink(this.m_Obj, 0.3f, 0.7f, 1.5f);
						this.m_Action.EntryAction(townPartsModelBlink2, 0);
					}
				}
				else if (townPartsModelBlink2 != null)
				{
					townPartsModelBlink2.SetEndMark();
				}
				break;
			}
			case eTownEventAct.ReBuildMaker:
			{
				TownPartsBuildGauge paction3 = new TownPartsBuildGauge(this.m_Builder, this.m_Transform, this.m_ManageID, this.m_LayerID + 5, (int)this.m_TimeKey, false, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_BUILD_START, 1f, 0, -1, -1);
				this.m_Action.EntryAction(paction3, 0);
				base.SetRenderFlg(this.m_Obj, false);
				this.m_GaugeUp = true;
				break;
			}
			case eTownEventAct.MenuMode:
			{
				TownHandleActionMenuMode townHandleActionMenuMode = (TownHandleActionMenuMode)pact;
				this.m_BindCtrl.SetViewActive(townHandleActionMenuMode.m_Mode);
				if (this.m_ItemPopModel != null)
				{
					this.m_ItemPopModel.SetViewActive(townHandleActionMenuMode.m_Mode);
				}
				break;
			}
			}
			return result;
		}

		// Token: 0x060022B1 RID: 8881 RVA: 0x000BA46C File Offset: 0x000B886C
		public void SetCompleteGauge()
		{
			ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsBuildGauge));
			if (action != null)
			{
				TownPartsBuildGauge townPartsBuildGauge = (TownPartsBuildGauge)action;
				townPartsBuildGauge.SetComplete();
				base.SetRenderFlg(this.m_Obj, true);
			}
			TownPartsModelBlink townPartsModelBlink = (TownPartsModelBlink)this.m_Action.GetAction(typeof(TownPartsModelBlink));
			if (townPartsModelBlink != null)
			{
				townPartsModelBlink.SetEndMark();
			}
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(this.m_ManageID, -1);
			buildMngIDTreeNode.SetBuilding(true);
		}

		// Token: 0x060022B2 RID: 8882 RVA: 0x000BA4F8 File Offset: 0x000B88F8
		public void CallbackItemUpResult(IFldNetComModule phandle)
		{
			FldComBuildScript fldComBuildScript = phandle as FldComBuildScript;
			ComItemUpState upItemState = fldComBuildScript.GetUpItemState();
			int newListNum = upItemState.GetNewListNum();
			Vector3 position = this.m_Obj.transform.position;
			position.y += this.m_MakerPos;
			int num = 0;
			int[] array = new int[4];
			for (int i = 0; i < 4; i++)
			{
				array[i] = 0;
			}
			for (int i = 0; i < newListNum; i++)
			{
				ComItemUpState.UpCategory newListAt = upItemState.GetNewListAt(i);
				switch (newListAt.m_Category)
				{
				case ComItemUpState.eUpCategory.Item:
				{
					if ((num & 1) == 0)
					{
						TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(1);
						townEffectPlayer.SetTrs(position + new Vector3(0f, 0.2f, 0f), Quaternion.identity, Vector3.one, this.m_LayerID + 30);
						num |= 1;
					}
					MainComResultItem pevent = new MainComResultItem(newListAt.m_No, (int)newListAt.m_Num);
					this.m_Builder.StackEventQue(pevent);
					break;
				}
				case ComItemUpState.eUpCategory.Money:
					if ((num & 2) == 0)
					{
						TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(0);
						townEffectPlayer.SetTrs(position + new Vector3(0f, 0.2f, 0f), Quaternion.identity, Vector3.one, this.m_LayerID + 30);
						num |= 2;
					}
					array[1] += (int)newListAt.m_Num;
					break;
				case ComItemUpState.eUpCategory.Point:
					if ((num & 8) == 0)
					{
						num |= 8;
					}
					array[3] += (int)newListAt.m_Num;
					break;
				case ComItemUpState.eUpCategory.Stamina:
					if ((num & 4) == 0)
					{
						num |= 4;
					}
					array[2] += (int)newListAt.m_Num;
					break;
				}
			}
			if (num != 0)
			{
				if ((num & 2) != 0)
				{
					MainComResultPlayerItem pevent2 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.Money, (long)array[1]);
					this.m_Builder.StackEventQue(pevent2);
				}
				if ((num & 4) != 0)
				{
					MainComResultPlayerItem pevent2 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.Stamina, (long)array[2]);
					this.m_Builder.StackEventQue(pevent2);
				}
				if ((num & 8) != 0)
				{
					MainComResultPlayerItem pevent2 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.KRRPoint, (long)array[3]);
					this.m_Builder.StackEventQue(pevent2);
				}
			}
		}

		// Token: 0x060022B3 RID: 8883 RVA: 0x000BA744 File Offset: 0x000B8B44
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate != ITownObjectHandler.eTouchState.Begin)
			{
				if (fstate != ITownObjectHandler.eTouchState.Cancel)
				{
					if (fstate == ITownObjectHandler.eTouchState.End)
					{
						bool flag = true;
						FieldObjHandleBuild buildState = FieldMapManager.GetBuildState(this.m_ManageID);
						if (buildState != null && this.m_ItemUp)
						{
							buildState.SendDropItemCom(new IFldNetComModule.CallBack(this.CallbackItemUpResult));
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_CHARA_ICON_SELECT, 1f, 0, -1, -1);
							if (this.m_ItemPopModel != null)
							{
								this.m_ItemPopModel.DestroyRequest();
								this.m_ItemPopModel = null;
							}
							this.m_ItemUp = false;
							flag = false;
						}
						if (flag)
						{
							if (this.m_Category == eTownObjectCategory.Room)
							{
								pbuilder.StackEventQue(new TownComUI(true)
								{
									m_SelectHandle = this,
									m_OpenUI = eTownUICategory.Room,
									m_BuildPoint = base.GetBuildAccessID()
								});
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
							}
							else if (this.IsCompleteBuildEvt())
							{
								pbuilder.StackEventQue(new TownComComplete(TownUtility.eResCategory.Buf)
								{
									m_BuildPoint = base.GetBuildAccessID(),
									m_ManageID = base.GetManageID()
								});
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_BUILD_COMPLETE, 1f, 0, -1, -1);
								pbuilder.StackEventQue(new TownComUI(true)
								{
									m_SelectHandle = this,
									m_OpenUI = eTownUICategory.Buf,
									m_BuildPoint = base.GetBuildAccessID()
								});
								this.SetCompleteGauge();
							}
							else
							{
								pbuilder.StackEventQue(new TownComUI(true)
								{
									m_SelectHandle = this,
									m_BuildPoint = base.GetBuildAccessID(),
									m_OpenUI = eTownUICategory.Buf
								});
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
							}
						}
						TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
						if (townPartsTouchScale != null)
						{
							townPartsTouchScale.SetStepOut();
						}
					}
				}
				else
				{
					TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
					if (townPartsTouchScale != null)
					{
						townPartsTouchScale.SetStepOut();
					}
				}
			}
			else
			{
				TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
				if (townPartsTouchScale == null)
				{
					townPartsTouchScale = new TownPartsTouchScale(this.m_Obj.transform, Vector3.one * this.m_BaseSize, Vector3.one * this.m_BaseSize * 1.1f, 0.1f);
					this.m_Action.EntryAction(townPartsTouchScale, 0);
				}
				townPartsTouchScale.SetStepIn();
			}
			return true;
		}

		// Token: 0x060022B4 RID: 8884 RVA: 0x000BA9E0 File Offset: 0x000B8DE0
		public void CallbackBufBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
			switch (ftype)
			{
			case eCallBackType.MoveBuild:
			{
				int fsynckey = this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				TownBuildMoveBufParam prequest = new TownBuildMoveBufParam(this.m_BuildAccessID, pbase.m_BuildPoint, this.m_ManageID, this.m_ObjID, fsynckey, null);
				this.m_Builder.BuildRequest(prequest);
				break;
			}
			case eCallBackType.SwapBuild:
			{
				int fsynckey2 = this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				TownBuildMoveBufParam prequest2 = new TownBuildMoveBufParam(this.m_BuildAccessID, pbase.m_BuildPoint, this.m_ManageID, this.m_ObjID, fsynckey2, null);
				this.m_Builder.BuildRequest(prequest2);
				this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				break;
			}
			case eCallBackType.RemoveBuild:
			{
				TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(this.m_ManageID, -1);
				if (buildMngIDTreeNode != null)
				{
					buildMngIDTreeNode.m_Pakage.BackBuildMode();
				}
				break;
			}
			case eCallBackType.CompBuild:
				this.SetCompleteGauge();
				MissionManager.UnlockAction(eXlsMissionSeg.Town, eXlsMissionTownFuncType.BufBuild);
				break;
			case eCallBackType.LevelUp:
				this.CheckModelChange();
				break;
			}
		}

		// Token: 0x060022B5 RID: 8885 RVA: 0x000BAB2C File Offset: 0x000B8F2C
		private void CheckModelChange()
		{
			if ((SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID).m_Arg & 1) == 0)
			{
				TownPartsFrameEvent townPartsFrameEvent = new TownPartsFrameEvent();
				townPartsFrameEvent.Stack(0, new TownPartsFrameEvent.CallEvent(this.CallChangeModel), 0);
				townPartsFrameEvent.Stack(0, new TownPartsFrameEvent.CallEvent(this.CallChangeModel), 1);
				townPartsFrameEvent.Stack(43, new TownPartsFrameEvent.CallEvent(this.CallChangeModel), 2);
				townPartsFrameEvent.Play();
				this.m_Action.EntryAction(townPartsFrameEvent, 0);
			}
		}

		// Token: 0x060022B6 RID: 8886 RVA: 0x000BABB8 File Offset: 0x000B8FB8
		private bool CallChangeModel(int fkeyid)
		{
			bool result = true;
			if (fkeyid != 0)
			{
				if (fkeyid != 1)
				{
					if (fkeyid == 2)
					{
						base.DesModel();
						this.ReSetUpHandle(this.m_ObjID, this.m_ManageID);
					}
				}
				else if (this.m_Obj != null)
				{
					TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(3);
					townEffectPlayer.SetTrs(this.m_Obj.transform.position, Quaternion.identity, Vector3.one, this.m_LayerID + 30);
				}
			}
			else
			{
				TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID);
				UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(this.m_ManageID);
				this.m_ResourceID = param.m_ResourceID * 100;
				if (townBuildData.m_Lv != 0)
				{
					this.m_ResourceID += townBuildData.m_Lv - 1;
				}
			}
			return result;
		}

		// Token: 0x0400297D RID: 10621
		private bool m_GaugeUp;

		// Token: 0x0400297E RID: 10622
		private bool m_InitGaugeUp;

		// Token: 0x0400297F RID: 10623
		private bool m_ModelMarkUp;

		// Token: 0x04002980 RID: 10624
		private float m_MakerPos;

		// Token: 0x04002981 RID: 10625
		private float m_BaseSize;

		// Token: 0x04002982 RID: 10626
		private Vector3 m_Offset;

		// Token: 0x04002983 RID: 10627
		private eTownObjectCategory m_Category;

		// Token: 0x04002984 RID: 10628
		private bool m_ItemUp;

		// Token: 0x04002985 RID: 10629
		private TownPartsPopObject m_ItemPopModel;

		// Token: 0x04002986 RID: 10630
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x04002987 RID: 10631
		private FieldObjDropItem.DropItemInfo m_ItemUpInfo;

		// Token: 0x04002988 RID: 10632
		private bool m_ReBindPos;

		// Token: 0x04002989 RID: 10633
		private byte m_ItemType;

		// Token: 0x0400298A RID: 10634
		public TownObjHandleBuild.eState m_State;

		// Token: 0x020006D9 RID: 1753
		public enum eState
		{
			// Token: 0x0400298C RID: 10636
			Init,
			// Token: 0x0400298D RID: 10637
			Main,
			// Token: 0x0400298E RID: 10638
			Sleep,
			// Token: 0x0400298F RID: 10639
			EndStart,
			// Token: 0x04002990 RID: 10640
			End
		}
	}
}
