﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000265 RID: 613
	public class WeaponRecipeListDB : ScriptableObject
	{
		// Token: 0x040013DF RID: 5087
		public WeaponRecipeListDB_Param[] m_Params;
	}
}
