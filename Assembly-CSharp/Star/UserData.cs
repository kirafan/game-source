﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B3E RID: 2878
	public class UserData
	{
		// Token: 0x06003CA3 RID: 15523 RVA: 0x00135A80 File Offset: 0x00133E80
		public UserData()
		{
			this.ID = -1L;
			this.Name = string.Empty;
			this.Comment = string.Empty;
			this.Stamina = new UserStamina();
			this.KRRPoint = new UserKRRPoint();
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06003CA4 RID: 15524 RVA: 0x00135ABC File Offset: 0x00133EBC
		// (set) Token: 0x06003CA5 RID: 15525 RVA: 0x00135AC4 File Offset: 0x00133EC4
		public long ID { get; set; }

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06003CA6 RID: 15526 RVA: 0x00135ACD File Offset: 0x00133ECD
		// (set) Token: 0x06003CA7 RID: 15527 RVA: 0x00135AD5 File Offset: 0x00133ED5
		public string Name { get; set; }

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06003CA8 RID: 15528 RVA: 0x00135ADE File Offset: 0x00133EDE
		// (set) Token: 0x06003CA9 RID: 15529 RVA: 0x00135AE6 File Offset: 0x00133EE6
		public string Comment { get; set; }

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x06003CAA RID: 15530 RVA: 0x00135AEF File Offset: 0x00133EEF
		// (set) Token: 0x06003CAB RID: 15531 RVA: 0x00135AF7 File Offset: 0x00133EF7
		public eAgeType Age { get; set; }

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x06003CAC RID: 15532 RVA: 0x00135B00 File Offset: 0x00133F00
		// (set) Token: 0x06003CAD RID: 15533 RVA: 0x00135B08 File Offset: 0x00133F08
		public string MyCode { get; set; }

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06003CAE RID: 15534 RVA: 0x00135B11 File Offset: 0x00133F11
		// (set) Token: 0x06003CAF RID: 15535 RVA: 0x00135B19 File Offset: 0x00133F19
		public int Lv { get; set; }

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x06003CB0 RID: 15536 RVA: 0x00135B22 File Offset: 0x00133F22
		// (set) Token: 0x06003CB1 RID: 15537 RVA: 0x00135B2A File Offset: 0x00133F2A
		public long LvExp { get; set; }

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x06003CB2 RID: 15538 RVA: 0x00135B33 File Offset: 0x00133F33
		// (set) Token: 0x06003CB3 RID: 15539 RVA: 0x00135B3B File Offset: 0x00133F3B
		public long Exp { get; set; }

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x06003CB4 RID: 15540 RVA: 0x00135B44 File Offset: 0x00133F44
		// (set) Token: 0x06003CB5 RID: 15541 RVA: 0x00135B4C File Offset: 0x00133F4C
		public long Gold { get; set; }

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x06003CB6 RID: 15542 RVA: 0x00135B55 File Offset: 0x00133F55
		// (set) Token: 0x06003CB7 RID: 15543 RVA: 0x00135B5D File Offset: 0x00133F5D
		public long UnlimitedGem { get; set; }

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06003CB8 RID: 15544 RVA: 0x00135B66 File Offset: 0x00133F66
		// (set) Token: 0x06003CB9 RID: 15545 RVA: 0x00135B6E File Offset: 0x00133F6E
		public long LimitedGem { get; set; }

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06003CBA RID: 15546 RVA: 0x00135B77 File Offset: 0x00133F77
		public long Gem
		{
			get
			{
				return this.UnlimitedGem + this.LimitedGem;
			}
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06003CBB RID: 15547 RVA: 0x00135B86 File Offset: 0x00133F86
		// (set) Token: 0x06003CBC RID: 15548 RVA: 0x00135B8E File Offset: 0x00133F8E
		public long LotteryTicket { get; set; }

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06003CBD RID: 15549 RVA: 0x00135B97 File Offset: 0x00133F97
		// (set) Token: 0x06003CBE RID: 15550 RVA: 0x00135B9F File Offset: 0x00133F9F
		public int PartyCost { get; set; }

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06003CBF RID: 15551 RVA: 0x00135BA8 File Offset: 0x00133FA8
		// (set) Token: 0x06003CC0 RID: 15552 RVA: 0x00135BB0 File Offset: 0x00133FB0
		public UserStamina Stamina { get; set; }

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06003CC1 RID: 15553 RVA: 0x00135BB9 File Offset: 0x00133FB9
		// (set) Token: 0x06003CC2 RID: 15554 RVA: 0x00135BC1 File Offset: 0x00133FC1
		public UserKRRPoint KRRPoint { get; set; }

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06003CC3 RID: 15555 RVA: 0x00135BCA File Offset: 0x00133FCA
		// (set) Token: 0x06003CC4 RID: 15556 RVA: 0x00135BD2 File Offset: 0x00133FD2
		public int WeaponLimit { get; set; }

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06003CC5 RID: 15557 RVA: 0x00135BDB File Offset: 0x00133FDB
		// (set) Token: 0x06003CC6 RID: 15558 RVA: 0x00135BE3 File Offset: 0x00133FE3
		public int WeaponLimitCount { get; set; }

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x06003CC7 RID: 15559 RVA: 0x00135BEC File Offset: 0x00133FEC
		// (set) Token: 0x06003CC8 RID: 15560 RVA: 0x00135BF4 File Offset: 0x00133FF4
		public int FacilityLimit { get; set; }

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x06003CC9 RID: 15561 RVA: 0x00135BFD File Offset: 0x00133FFD
		// (set) Token: 0x06003CCA RID: 15562 RVA: 0x00135C05 File Offset: 0x00134005
		public int FacilityLimitCount { get; set; }

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x06003CCB RID: 15563 RVA: 0x00135C0E File Offset: 0x0013400E
		// (set) Token: 0x06003CCC RID: 15564 RVA: 0x00135C16 File Offset: 0x00134016
		public int RoomObjectLimit { get; set; }

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x06003CCD RID: 15565 RVA: 0x00135C1F File Offset: 0x0013401F
		// (set) Token: 0x06003CCE RID: 15566 RVA: 0x00135C27 File Offset: 0x00134027
		public int RoomObjectLimitCount { get; set; }

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x06003CCF RID: 15567 RVA: 0x00135C30 File Offset: 0x00134030
		// (set) Token: 0x06003CD0 RID: 15568 RVA: 0x00135C38 File Offset: 0x00134038
		public int CharacterLimit { get; set; }

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x06003CD1 RID: 15569 RVA: 0x00135C41 File Offset: 0x00134041
		// (set) Token: 0x06003CD2 RID: 15570 RVA: 0x00135C49 File Offset: 0x00134049
		public int ItemLimit { get; set; }

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x06003CD3 RID: 15571 RVA: 0x00135C52 File Offset: 0x00134052
		// (set) Token: 0x06003CD4 RID: 15572 RVA: 0x00135C5A File Offset: 0x0013405A
		public int FriendLimit { get; set; }

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06003CD5 RID: 15573 RVA: 0x00135C63 File Offset: 0x00134063
		// (set) Token: 0x06003CD6 RID: 15574 RVA: 0x00135C6B File Offset: 0x0013406B
		public int SupportLimit { get; set; }

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06003CD7 RID: 15575 RVA: 0x00135C74 File Offset: 0x00134074
		// (set) Token: 0x06003CD8 RID: 15576 RVA: 0x00135C7C File Offset: 0x0013407C
		public int LoginCount { get; set; }

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06003CD9 RID: 15577 RVA: 0x00135C85 File Offset: 0x00134085
		// (set) Token: 0x06003CDA RID: 15578 RVA: 0x00135C8D File Offset: 0x0013408D
		public DateTime LastLoginAt { get; set; }

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06003CDB RID: 15579 RVA: 0x00135C96 File Offset: 0x00134096
		// (set) Token: 0x06003CDC RID: 15580 RVA: 0x00135C9E File Offset: 0x0013409E
		public int LoginDays { get; set; }

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06003CDD RID: 15581 RVA: 0x00135CA7 File Offset: 0x001340A7
		// (set) Token: 0x06003CDE RID: 15582 RVA: 0x00135CAF File Offset: 0x001340AF
		public int ContinuousDays { get; set; }

		// Token: 0x06003CDF RID: 15583 RVA: 0x00135CB8 File Offset: 0x001340B8
		public void Update()
		{
			if (this.Stamina != null)
			{
				this.Stamina.UpdateStamina(Time.unscaledDeltaTime);
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RefreshBadgeTimeSec > 0f)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RefreshBadgeTimeSec -= Time.deltaTime;
			}
		}

		// Token: 0x06003CE0 RID: 15584 RVA: 0x00135D14 File Offset: 0x00134114
		public bool IsShortOfGold(long amount)
		{
			return this.Gold < amount;
		}

		// Token: 0x06003CE1 RID: 15585 RVA: 0x00135D25 File Offset: 0x00134125
		public bool IsShortOfGem(long amount)
		{
			return this.Gem < amount;
		}

		// Token: 0x06003CE2 RID: 15586 RVA: 0x00135D36 File Offset: 0x00134136
		public bool IsShortOfUnlimitedGem(long amount)
		{
			return this.UnlimitedGem < amount;
		}

		// Token: 0x06003CE3 RID: 15587 RVA: 0x00135D47 File Offset: 0x00134147
		public bool IsShortOfKP(long amount)
		{
			return this.KRRPoint.GetPoint() < amount;
		}

		// Token: 0x04004458 RID: 17496
		public const int USERNAME_MAX = 10;

		// Token: 0x04004459 RID: 17497
		public const int USERCOMMENT_MAX = 31;

		// Token: 0x0400445A RID: 17498
		public const int USERCOMMENT_RETURN = 15;
	}
}
