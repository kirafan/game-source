﻿using System;
using System.Collections.Generic;
using Meige;
using TIPRequestTypes;
using TIPResponseTypes;
using TUTORIALRequestTypes;
using TUTORIALResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200073A RID: 1850
	public sealed class TutorialManager
	{
		// Token: 0x14000035 RID: 53
		// (add) Token: 0x06002458 RID: 9304 RVA: 0x000C2DC4 File Offset: 0x000C11C4
		// (remove) Token: 0x06002459 RID: 9305 RVA: 0x000C2DFC File Offset: 0x000C11FC
		private event Action OnCompleteTutorialTipsWindow;

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x0600245A RID: 9306 RVA: 0x000C2E32 File Offset: 0x000C1232
		// (set) Token: 0x0600245B RID: 9307 RVA: 0x000C2E35 File Offset: 0x000C1235
		public bool IsSkip
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x0600245C RID: 9308 RVA: 0x000C2E37 File Offset: 0x000C1237
		public bool IsFinishedTutorialSeq()
		{
			return this.IsSkip || this.GetTutoarialSeq() == eTutorialSeq.Finish;
		}

		// Token: 0x0600245D RID: 9309 RVA: 0x000C2E55 File Offset: 0x000C1255
		public eTutorialSeq GetTutoarialSeq()
		{
			if (this.IsSkip)
			{
				return eTutorialSeq.Finish;
			}
			return this.m_TutorialSeq;
		}

		// Token: 0x0600245E RID: 9310 RVA: 0x000C2E6A File Offset: 0x000C126A
		public void SetTutoarialSeq(eTutorialSeq tutorialSeq)
		{
			this.m_TutorialSeq = tutorialSeq;
		}

		// Token: 0x0600245F RID: 9311 RVA: 0x000C2E73 File Offset: 0x000C1273
		public void SetTutoarialSeq(int tutorialSeq)
		{
			this.m_TutorialSeq = (eTutorialSeq)tutorialSeq;
		}

		// Token: 0x06002460 RID: 9312 RVA: 0x000C2E7C File Offset: 0x000C127C
		public eTutorialSeq ApplyNextTutorialSeq()
		{
			eTutorialSeq tutorialSeq = this.m_TutorialSeq + 1;
			this.m_TutorialSeq = tutorialSeq;
			return this.m_TutorialSeq;
		}

		// Token: 0x06002461 RID: 9313 RVA: 0x000C2E9F File Offset: 0x000C129F
		public List<eTutorialTipsListDB> GetOccurredTipsIDs()
		{
			return this.m_OccurredTipsIDs;
		}

		// Token: 0x06002462 RID: 9314 RVA: 0x000C2EA8 File Offset: 0x000C12A8
		public void SetTipsIDs(int[] tipsIDs)
		{
			this.m_OccurredTipsIDs = new List<eTutorialTipsListDB>();
			for (int i = 0; i < tipsIDs.Length; i++)
			{
				this.m_OccurredTipsIDs.Add((eTutorialTipsListDB)tipsIDs[i]);
			}
		}

		// Token: 0x06002463 RID: 9315 RVA: 0x000C2EE2 File Offset: 0x000C12E2
		public void AddOccurredTipsID(eTutorialTipsListDB tipsID)
		{
			if (!this.m_OccurredTipsIDs.Contains(tipsID))
			{
				this.m_OccurredTipsIDs.Add(tipsID);
			}
		}

		// Token: 0x06002464 RID: 9316 RVA: 0x000C2F01 File Offset: 0x000C1301
		public bool IsOccurredTips(eTutorialTipsListDB tipsID)
		{
			return this.IsSkip || this.m_OccurredTipsIDs.Contains(tipsID);
		}

		// Token: 0x06002465 RID: 9317 RVA: 0x000C2F1C File Offset: 0x000C131C
		public bool OpenTipsWindowIfNotOccurred(eTutorialTipsListDB tipsID, Action onComplete = null, Action onOpend = null)
		{
			if (this.IsOccurredTips(tipsID))
			{
				return false;
			}
			this.OpenTipsWindow(tipsID, onComplete, onOpend);
			return true;
		}

		// Token: 0x06002466 RID: 9318 RVA: 0x000C2F38 File Offset: 0x000C1338
		public void OpenTipsWindow(eTutorialTipsListDB tipsID, Action onComplete = null, Action onOpend = null)
		{
			this.m_CurrentTutorialTipsID = tipsID;
			this.OnCompleteTutorialTipsWindow = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialTipsUI.Open(tipsID);
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialTipsUI.SetOnClickCloseCallback(new Action(this.OnCompleteTutorialTipsUI));
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialTipsUI.SetOnOpendCallback(onOpend);
		}

		// Token: 0x06002467 RID: 9319 RVA: 0x000C2F90 File Offset: 0x000C1390
		private void OnCompleteTutorialTipsUI()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TutorialTipsListDB.GetParam(this.m_CurrentTutorialTipsID).m_IsAPI != 0)
			{
				this.Request_TutorialTipsAdd();
			}
			else
			{
				this.OnCompleteTutorialTipsWindow.Call();
			}
		}

		// Token: 0x06002468 RID: 9320 RVA: 0x000C2FDC File Offset: 0x000C13DC
		private void Request_TutorialTipsAdd()
		{
			TIPRequestTypes.Add add = new TIPRequestTypes.Add();
			TIPRequestTypes.Add add2 = add;
			int currentTutorialTipsID = (int)this.m_CurrentTutorialTipsID;
			add2.tipId = currentTutorialTipsID.ToString();
			MeigewwwParam wwwParam = TIPRequest.Add(add, new MeigewwwParam.Callback(this.OnResponse_TutorialTipsAdd));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06002469 RID: 9321 RVA: 0x000C3034 File Offset: 0x000C1434
		private void OnResponse_TutorialTipsAdd(MeigewwwParam wwwParam)
		{
			TIPResponseTypes.Add add = TIPResponse.Add(wwwParam, ResponseCommon.DialogType.None, null);
			if (add == null)
			{
				return;
			}
			if (add.GetResult() == ResultCode.SUCCESS)
			{
				this.AddOccurredTipsID(this.m_CurrentTutorialTipsID);
			}
			this.OnCompleteTutorialTipsWindow.Call();
		}

		// Token: 0x14000036 RID: 54
		// (add) Token: 0x0600246A RID: 9322 RVA: 0x000C3078 File Offset: 0x000C1478
		// (remove) Token: 0x0600246B RID: 9323 RVA: 0x000C30B0 File Offset: 0x000C14B0
		private event Action m_OnComplete_TutorialFinish;

		// Token: 0x0600246C RID: 9324 RVA: 0x000C30E8 File Offset: 0x000C14E8
		public void Request_TutorialFinish(Action onResponse)
		{
			this.m_OnComplete_TutorialFinish = onResponse;
			MeigewwwParam wwwParam = TUTORIALRequest.Add(new TUTORIALRequestTypes.Add
			{
				stepCode = -1
			}, new MeigewwwParam.Callback(this.OnResponse_TutorialFinish));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x0600246D RID: 9325 RVA: 0x000C3134 File Offset: 0x000C1534
		private void OnResponse_TutorialFinish(MeigewwwParam wwwParam)
		{
			TUTORIALResponseTypes.Add add = TUTORIALResponse.Add(wwwParam, ResponseCommon.DialogType.None, null);
			if (add == null)
			{
				return;
			}
			if (add.GetResult() == ResultCode.SUCCESS)
			{
				this.SetTutoarialSeq(eTutorialSeq.Finish);
				if (AppsFlyersHandler.Instance != null)
				{
					AppsFlyersHandler.Instance.TrackEvent_TutorialClear();
				}
			}
			this.m_OnComplete_TutorialFinish.Call();
		}

		// Token: 0x14000037 RID: 55
		// (add) Token: 0x0600246E RID: 9326 RVA: 0x000C318C File Offset: 0x000C158C
		// (remove) Token: 0x0600246F RID: 9327 RVA: 0x000C31C4 File Offset: 0x000C15C4
		private event Action m_OnComplete_TutorialPartySet;

		// Token: 0x06002470 RID: 9328 RVA: 0x000C31FC File Offset: 0x000C15FC
		public void Request_TutorialPartySet(Action onResponse)
		{
			this.m_OnComplete_TutorialPartySet = onResponse;
			MeigewwwParam wwwParam = TUTORIALRequest.SetParty(new TUTORIALRequestTypes.SetParty
			{
				stepCode = 5
			}, new MeigewwwParam.Callback(this.OnResponse_TutorialPartySet));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06002471 RID: 9329 RVA: 0x000C3248 File Offset: 0x000C1648
		private void OnResponse_TutorialPartySet(MeigewwwParam wwwParam)
		{
			TUTORIALResponseTypes.SetParty setParty = TUTORIALResponse.SetParty(wwwParam, ResponseCommon.DialogType.None, null);
			if (setParty == null)
			{
				return;
			}
			if (setParty.GetResult() == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(setParty.managedBattleParties, userDataMng.UserBattlePartyDatas);
				APIUtility.wwwToUserData(setParty.managedFieldPartyMembers, userDataMng.UserFieldChara);
				this.SetTutoarialSeq(eTutorialSeq.GachaDecided_NextIsADVGachaAfterPlay);
			}
			this.m_OnComplete_TutorialPartySet.Call();
		}

		// Token: 0x14000038 RID: 56
		// (add) Token: 0x06002472 RID: 9330 RVA: 0x000C32B4 File Offset: 0x000C16B4
		// (remove) Token: 0x06002473 RID: 9331 RVA: 0x000C32EC File Offset: 0x000C16EC
		private event Action m_OnComplete_TutorialQuest;

		// Token: 0x06002474 RID: 9332 RVA: 0x000C3322 File Offset: 0x000C1722
		public void Request_TutorialQuest(Action onComplete)
		{
			this.m_OnComplete_TutorialQuest = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestGetAll(new Action(this.OnResponse_QuestGetAll), true);
		}

		// Token: 0x06002475 RID: 9333 RVA: 0x000C3348 File Offset: 0x000C1748
		private void OnResponse_QuestGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestChapterGetAll(new Action(this.OnResponse_QuestChapterGetAll), true);
		}

		// Token: 0x06002476 RID: 9334 RVA: 0x000C3368 File Offset: 0x000C1768
		private void OnResponse_QuestChapterGetAll()
		{
			QuestManager.ChapterData chapterDataFromQuestID = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetChapterDataFromQuestID(1100010);
			QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
			selectQuest.Reset();
			selectQuest.SetupChapter(chapterDataFromQuestID.m_ChapterID, chapterDataFromQuestID.m_Difficulty, 1100010);
			LocalSaveData.Inst.SelectedPartyIndex = 0;
			LocalSaveData.Inst.Save();
			long mngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex].MngID;
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestLogAdd(false, 1100010, -1, mngID, -1L, new Action<QuestDefine.eReturnLogAdd, string>(this.OnResponse_QuestLogAdd));
		}

		// Token: 0x06002477 RID: 9335 RVA: 0x000C3416 File Offset: 0x000C1816
		private void OnResponse_QuestLogAdd(QuestDefine.eReturnLogAdd ret, string errorMessage)
		{
			if (ret == QuestDefine.eReturnLogAdd.Success)
			{
				this.m_OnComplete_TutorialQuest.Call();
			}
		}

		// Token: 0x06002478 RID: 9336 RVA: 0x000C3438 File Offset: 0x000C1838
		public TutorialManager.eRoomTutorialStep GetRoomTutorialStep()
		{
			return this.m_RoomTutorialStep;
		}

		// Token: 0x06002479 RID: 9337 RVA: 0x000C3440 File Offset: 0x000C1840
		public void SetRoomTutorialStep(TutorialManager.eRoomTutorialStep step)
		{
			this.m_RoomTutorialStep = step;
		}

		// Token: 0x0600247A RID: 9338 RVA: 0x000C3449 File Offset: 0x000C1849
		public bool IsRoomTutorial()
		{
			return this.m_RoomTutorialStep != TutorialManager.eRoomTutorialStep.None;
		}

		// Token: 0x0600247B RID: 9339 RVA: 0x000C3458 File Offset: 0x000C1858
		public TutorialManager.eRoomTutorialStep CalcRoomTutorialStep()
		{
			if (this.IsSkip)
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.None;
				return this.m_RoomTutorialStep;
			}
			if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_001))
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.FirstTips;
			}
			else if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjPlacementNum() == 4 && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_003))
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.Buy;
			}
			else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_002))
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.MoveTips;
			}
			else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_003))
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.EndTips;
			}
			else
			{
				this.m_RoomTutorialStep = TutorialManager.eRoomTutorialStep.None;
			}
			return this.m_RoomTutorialStep;
		}

		// Token: 0x0600247C RID: 9340 RVA: 0x000C3534 File Offset: 0x000C1934
		public TutorialManager.eTownTutorialStep GetTownTutorialStep()
		{
			return this.m_TownTutorialStep;
		}

		// Token: 0x0600247D RID: 9341 RVA: 0x000C353C File Offset: 0x000C193C
		public void SetTownTutorialStep(TutorialManager.eTownTutorialStep step)
		{
			this.m_TownTutorialStep = step;
		}

		// Token: 0x0600247E RID: 9342 RVA: 0x000C3545 File Offset: 0x000C1945
		public bool IsTownTutorial()
		{
			return this.m_TownTutorialStep != TutorialManager.eTownTutorialStep.None;
		}

		// Token: 0x0600247F RID: 9343 RVA: 0x000C3554 File Offset: 0x000C1954
		public TutorialManager.eTownTutorialStep CalcTownTutorialStep()
		{
			if (this.IsSkip)
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.None;
				return this.m_TownTutorialStep;
			}
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_001))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.FirstTips;
			}
			else if (userTownData.GetBuildNumByCategory(eTownObjectCategory.Contents) <= 0 && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_004))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.BuildContent;
			}
			else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_002))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.AfterContentTips;
			}
			else if (userTownData.GetBuildNumByCategory(eTownObjectCategory.Item) <= 0 && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.BuildWeapon;
			}
			else if (userTownData.GetBuildNumByCategory(eTownObjectCategory.Money) <= 0 && this.IsBuilding() && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.WaitBuildWeapon;
			}
			else if (userTownData.GetBuildNumByCategory(eTownObjectCategory.Money) <= 0 && this.IsNotAllOpen() && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.CompleteWeapon;
			}
			else if (userTownData.GetBuildNumByCategory(eTownObjectCategory.Money) <= 0 && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.BuildMoney;
			}
			else if (this.IsBuilding() && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.WaitBuildMoney;
			}
			else if (this.IsNotAllOpen() && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.CompleteMoney;
			}
			else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_003))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.AfterBuffTips;
			}
			else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Town_004))
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.EndTips;
			}
			else
			{
				this.m_TownTutorialStep = TutorialManager.eTownTutorialStep.None;
			}
			return this.m_TownTutorialStep;
		}

		// Token: 0x06002480 RID: 9344 RVA: 0x000C37A0 File Offset: 0x000C1BA0
		private bool IsBuilding()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				if (UserTownUtil.GetBuildTimeMs(buildObjectDataAt.m_ManageID) <= 0L && !buildObjectDataAt.m_IsOpen)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002481 RID: 9345 RVA: 0x000C37FC File Offset: 0x000C1BFC
		private bool IsNotAllOpen()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				if (!buildObjectDataAt.m_IsOpen)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x04002B5B RID: 11099
		private eTutorialSeq m_TutorialSeq = eTutorialSeq.Finish;

		// Token: 0x04002B5C RID: 11100
		private List<eTutorialTipsListDB> m_OccurredTipsIDs = new List<eTutorialTipsListDB>();

		// Token: 0x04002B5D RID: 11101
		private eTutorialTipsListDB m_CurrentTutorialTipsID;

		// Token: 0x04002B5F RID: 11103
		private TutorialManager.eTownTutorialStep m_TownTutorialStep;

		// Token: 0x04002B60 RID: 11104
		private TutorialManager.eRoomTutorialStep m_RoomTutorialStep;

		// Token: 0x04002B61 RID: 11105
		private const int DEFAULT_ROOMOBJ_NUM = 4;

		// Token: 0x0200073B RID: 1851
		public enum eTownTutorialStep
		{
			// Token: 0x04002B66 RID: 11110
			None,
			// Token: 0x04002B67 RID: 11111
			FirstTips,
			// Token: 0x04002B68 RID: 11112
			BuildContent,
			// Token: 0x04002B69 RID: 11113
			WaitBuildContent,
			// Token: 0x04002B6A RID: 11114
			AfterContentTips,
			// Token: 0x04002B6B RID: 11115
			BuildWeapon,
			// Token: 0x04002B6C RID: 11116
			WaitBuildWeapon,
			// Token: 0x04002B6D RID: 11117
			CompleteWeapon,
			// Token: 0x04002B6E RID: 11118
			BuildMoney,
			// Token: 0x04002B6F RID: 11119
			WaitBuildMoney,
			// Token: 0x04002B70 RID: 11120
			CompleteMoney,
			// Token: 0x04002B71 RID: 11121
			AfterBuffTips,
			// Token: 0x04002B72 RID: 11122
			EndTips
		}

		// Token: 0x0200073C RID: 1852
		public enum eRoomTutorialStep
		{
			// Token: 0x04002B74 RID: 11124
			None,
			// Token: 0x04002B75 RID: 11125
			FirstTips,
			// Token: 0x04002B76 RID: 11126
			Buy,
			// Token: 0x04002B77 RID: 11127
			MoveTips,
			// Token: 0x04002B78 RID: 11128
			EndTips
		}
	}
}
