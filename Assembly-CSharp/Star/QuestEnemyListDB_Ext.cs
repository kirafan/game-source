﻿using System;

namespace Star
{
	// Token: 0x0200019E RID: 414
	public static class QuestEnemyListDB_Ext
	{
		// Token: 0x06000B07 RID: 2823 RVA: 0x00041B40 File Offset: 0x0003FF40
		public static QuestEnemyListDB_Param GetParam(this QuestEnemyListDB self, int questEnemyID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == questEnemyID)
				{
					return self.m_Params[i];
				}
			}
			return default(QuestEnemyListDB_Param);
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x00041B98 File Offset: 0x0003FF98
		public static int GetSkillIDNum(ref QuestEnemyListDB_Param questEnemyParam)
		{
			int num = 0;
			for (int i = 0; i < questEnemyParam.m_SkillIDs.Length; i++)
			{
				if (questEnemyParam.m_SkillIDs[i] <= -1)
				{
					break;
				}
				num++;
			}
			return num;
		}
	}
}
