﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020006DA RID: 1754
	public class TownObjHandleChara : ITownObjectHandler
	{
		// Token: 0x060022B8 RID: 8888 RVA: 0x000BACA8 File Offset: 0x000B90A8
		public TownCharaModel GetModel()
		{
			return this.m_CharaIcons;
		}

		// Token: 0x060022B9 RID: 8889 RVA: 0x000BACB0 File Offset: 0x000B90B0
		public void SetUpChara(long fmngid)
		{
			this.m_State = TownObjHandleChara.eState.Init;
			this.m_ManageID = fmngid;
			this.m_CharaID = UserCharaUtil.GetCharaParam(fmngid).m_CharaID;
			this.m_LinkAccessMngID = TownDefine.ROOM_ACCESS_MNG_ID;
			this.m_RenderObject = this.m_Builder.GetGeneralObj(eTownObjectType.CHARA_MODEL);
			this.m_CharaIcons = this.m_RenderObject.AddComponent<TownCharaModel>();
			this.m_CharaIcons.SetCharaModel(this, this.m_ManageID, 0.325f, Vector3.up * 0.75f * 0.5f);
			this.m_RenderObject.transform.SetParent(this.m_Transform, false);
			this.Prepare();
			this.m_CharaIcons.PlayMotion(0);
			this.m_RenderObject.SetActive(true);
			this.m_BindManageID = -1L;
		}

		// Token: 0x060022BA RID: 8890 RVA: 0x000BAD7C File Offset: 0x000B917C
		public void BindBuildPoint(bool frebind = false)
		{
			if (frebind)
			{
				BindTownCharaMap charaBind = this.m_Builder.GetCharaBind(this.m_LinkAccessMngID, true);
				charaBind.BindGameObject(this);
				this.m_CharaIcons.PlayMotion(0);
				this.m_BindManageID = -1L;
			}
			else
			{
				TownBuildTree buildTree = this.m_Builder.GetBuildTree();
				TownBuildLinkPoint buildMngIDTreeNode = buildTree.GetBuildMngIDTreeNode(this.m_LinkAccessMngID, -1);
				if (buildMngIDTreeNode != null && buildMngIDTreeNode.IsBuilding())
				{
					BindTownCharaMap charaBind = this.m_Builder.GetCharaBind(this.m_LinkAccessMngID, true);
					charaBind.BindGameObject(this);
				}
				else
				{
					this.m_Action.EntryAction(new TownPartsMoveChara(this, this.m_Builder, this.m_LinkAccessMngID, TownDefine.ROOM_ACCESS_MNG_ID), 0);
					this.m_LinkAccessMngID = TownDefine.ROOM_ACCESS_MNG_ID;
				}
			}
		}

		// Token: 0x060022BB RID: 8891 RVA: 0x000BAE3C File Offset: 0x000B923C
		protected override bool PrepareMain()
		{
			bool result = false;
			int buildUpStep = this.m_BuildUpStep;
			if (buildUpStep != 0)
			{
				if (buildUpStep == 1)
				{
					if (base.PrepareMain())
					{
						result = true;
						this.m_State = TownObjHandleChara.eState.Main;
					}
				}
			}
			else
			{
				this.m_LinkAccessMngID = TownDefine.ROOM_ACCESS_MNG_ID;
				FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
				charaState.LinkFeedBackCall(new FeedBackCharaCallBack(this.FeedBackCharaCallBack));
				this.m_LinkAccessMngID = charaState.GetStayBuildMngID();
				this.m_Action.EntryAction(new TownPartsMoveChara(this, this.m_Builder, TownDefine.ROOM_ACCESS_MNG_ID, this.m_LinkAccessMngID), 0);
				this.ClrCharaHaveItem(charaState.GetScheduleTagItemLevel());
				this.m_BuildUpStep++;
			}
			return result;
		}

		// Token: 0x060022BC RID: 8892 RVA: 0x000BAEF8 File Offset: 0x000B92F8
		public override void DestroyRequest()
		{
			Vector3 position = this.m_Transform.position;
			this.m_Transform.SetParent(null, true);
			this.m_Transform.position = position;
			this.m_State = TownObjHandleChara.eState.EndStart;
		}

		// Token: 0x060022BD RID: 8893 RVA: 0x000BAF31 File Offset: 0x000B9331
		public override void Destroy()
		{
			if (this.m_RenderObject != null)
			{
				this.m_Builder.ReleaseCharaMarker(this.m_RenderObject);
				this.m_RenderObject = null;
			}
			this.m_CharaIcons = null;
			base.Destroy();
		}

		// Token: 0x060022BE RID: 8894 RVA: 0x000BAF6C File Offset: 0x000B936C
		private void Update()
		{
			this.m_Action.PakageUpdate();
			switch (this.m_State)
			{
			case TownObjHandleChara.eState.Init:
				if (this.m_IsPreparing)
				{
					this.PrepareMain();
					return;
				}
				break;
			case TownObjHandleChara.eState.Main:
			{
				FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
				if (charaState != null)
				{
					if (charaState.IsTouchItemNew())
					{
						if (!this.m_ScheduleItemNew)
						{
							this.SetCharaHaveItem(charaState.GetTouchItemNewLevel());
							this.m_ScheduleItemNew = true;
						}
					}
					else if (this.m_ScheduleItemNew)
					{
						this.ClrCharaHaveItem(charaState.GetScheduleTagItemLevel());
						this.m_ScheduleItemNew = false;
					}
				}
				break;
			}
			case TownObjHandleChara.eState.Sleep:
				if (this.m_RebindWait)
				{
					this.m_RebindWait = false;
					this.m_State = TownObjHandleChara.eState.Main;
					this.BindBuildPoint(true);
				}
				break;
			case TownObjHandleChara.eState.EndStart:
			{
				BindTownCharaMap charaBind = this.m_Builder.GetCharaBind(this.m_BindManageID, true);
				if (charaBind != null)
				{
					charaBind.ReBindGameObject(this);
				}
				this.m_CharaIcons.PlayMotion(2);
				this.m_State = TownObjHandleChara.eState.End;
				break;
			}
			case TownObjHandleChara.eState.End:
				if (!this.m_CharaIcons.IsPlaying(2))
				{
					this.Destroy();
					UnityEngine.Object.Destroy(base.gameObject);
				}
				break;
			}
		}

		// Token: 0x060022BF RID: 8895 RVA: 0x000BB0AC File Offset: 0x000B94AC
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			if (pact.m_Type == eTownEventAct.MovePos)
			{
				TownHandleActionMovePos townHandleActionMovePos = (TownHandleActionMovePos)pact;
				this.MovePosition(townHandleActionMovePos.m_MovePos, townHandleActionMovePos.m_ManageID, townHandleActionMovePos.m_LayerID);
			}
			else if (pact.m_Type == eTownEventAct.LinkCutPos)
			{
				TownHandleActionLinkCut townHandleActionLinkCut = (TownHandleActionLinkCut)pact;
				this.BindFade(null, townHandleActionLinkCut.m_LayerID);
			}
			else if (pact.m_Type == eTownEventAct.Rebind)
			{
				this.m_RebindWait = true;
			}
			return result;
		}

		// Token: 0x060022C0 RID: 8896 RVA: 0x000BB127 File Offset: 0x000B9527
		public override void SetEnableRender(bool flg)
		{
			if (this.m_CharaIcons != null)
			{
				this.m_CharaIcons.SetRenderActive(flg);
			}
			this.m_IsEnableRender = flg;
		}

		// Token: 0x060022C1 RID: 8897 RVA: 0x000BB150 File Offset: 0x000B9550
		private void MovePosition(Vector3 ftarget, long faccessid, int flayerid)
		{
			if (this.m_BindManageID != faccessid)
			{
				TownPartsTweenPos townPartsTweenPos = (TownPartsTweenPos)this.m_Action.GetAction(typeof(TownPartsTweenPos));
				if (townPartsTweenPos != null)
				{
					townPartsTweenPos.ChangePos(ftarget, 0.01f);
				}
				else
				{
					this.m_Transform.localPosition = ftarget;
				}
			}
			else
			{
				TownPartsTweenPos townPartsTweenPos = (TownPartsTweenPos)this.m_Action.GetAction(typeof(TownPartsTweenPos));
				if (townPartsTweenPos != null)
				{
					townPartsTweenPos.ChangePos(ftarget, 0.2f);
				}
				else
				{
					this.m_Action.EntryAction(new TownPartsTweenPos(this.m_Transform, ftarget, 0.2f), 0);
				}
			}
			this.m_LayerID = flayerid + 10;
			this.m_CharaIcons.ChangeLayer(flayerid + 10);
			this.m_BindManageID = faccessid;
		}

		// Token: 0x060022C2 RID: 8898 RVA: 0x000BB21B File Offset: 0x000B961B
		private void BindFade(Transform parent, int flayerid)
		{
			if (parent == null)
			{
				this.m_CharaIcons.PlayMotion(2);
				this.m_State = TownObjHandleChara.eState.Sleep;
			}
			this.m_LayerID = flayerid + 10;
			this.m_CharaIcons.ChangeLayer(flayerid + 10);
		}

		// Token: 0x060022C3 RID: 8899 RVA: 0x000BB258 File Offset: 0x000B9658
		public void FeedBackCharaCallBack(eCallBackType ftype, FieldObjHandleChara pbase)
		{
			if (ftype != eCallBackType.ScheduleChange)
			{
				if (ftype != eCallBackType.MoveBuild)
				{
					if (ftype == eCallBackType.Event)
					{
						this.m_PopUpMessageID = pbase.GetScheduleToPopMessageID();
					}
				}
				else if (this.m_LinkAccessMngID != pbase.GetStayBuildMngID())
				{
					this.m_Action.EntryAction(new TownPartsMoveChara(this, this.m_Builder, this.m_LinkAccessMngID, pbase.GetStayBuildMngID()), 0);
					this.m_LinkAccessMngID = pbase.GetStayBuildMngID();
				}
			}
		}

		// Token: 0x060022C4 RID: 8900 RVA: 0x000BB2DC File Offset: 0x000B96DC
		private void CallbackTouchItemEvt(IFldNetComModule phandle)
		{
			List<TownPartsGetItem> list = new List<TownPartsGetItem>();
			FldComPartyScript fldComPartyScript = phandle as FldComPartyScript;
			ComItemUpState upItemState = fldComPartyScript.GetUpItemState();
			int newListNum = upItemState.GetNewListNum();
			if (newListNum != 0)
			{
				MissionManager.UnlockAction(eXlsMissionSeg.Town, eXlsMissionTownFuncType.CharaTouchItem);
				for (int i = 0; i < newListNum; i++)
				{
					ComItemUpState.UpCategory newListAt = upItemState.GetNewListAt(i);
					switch (newListAt.m_Category)
					{
					case ComItemUpState.eUpCategory.Item:
					{
						MainComResultItem pevent = new MainComResultItem(newListAt.m_No, (int)newListAt.m_Num);
						this.m_Builder.StackEventQue(pevent);
						break;
					}
					case ComItemUpState.eUpCategory.Money:
					{
						MainComResultPlayerItem pevent2 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.Money, newListAt.m_Num);
						this.m_Builder.StackEventQue(pevent2);
						break;
					}
					case ComItemUpState.eUpCategory.Point:
					{
						TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(2);
						townEffectPlayer.SetTrs(this.m_RenderObject.transform.position, Quaternion.identity, Vector3.one, this.m_LayerID + 20);
						MainComResultPlayerItem pevent3 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.KRRPoint, newListAt.m_Num);
						this.m_Builder.StackEventQue(pevent3);
						break;
					}
					case ComItemUpState.eUpCategory.Stamina:
					{
						MainComResultPlayerItem pevent4 = new MainComResultPlayerItem(MainComResultPlayerItem.eCategory.Stamina, newListAt.m_Num);
						this.m_Builder.StackEventQue(pevent4);
						break;
					}
					}
					TownPartsGetItem townPartsGetItem = new TownPartsGetItem(this.m_Builder, newListAt.m_Category, (int)upItemState.GetNewListAt(i).m_Num, this.m_Transform.position - Vector3.up * 0.2f * (float)i, 0.2f * (float)i);
					list.Add(townPartsGetItem);
					this.m_Action.EntryAction(townPartsGetItem, 1 + i);
				}
			}
			else
			{
				TownEffectPlayer townEffectPlayer2 = TownEffectManager.CreateEffect(2);
				townEffectPlayer2.SetTrs(this.m_RenderObject.transform.position, Quaternion.identity, Vector3.one, this.m_LayerID + 20);
			}
			FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
			if (charaState.IsPopUpKeySpMode())
			{
				TownComCharaPopup townComCharaPopup = new TownComCharaPopup();
				townComCharaPopup.m_CharaID = this.m_CharaID;
				townComCharaPopup.m_ManageID = this.m_ManageID;
				townComCharaPopup.m_Position = base.CacheTransform.position;
				if (this.m_PopUpMessageID != 0)
				{
					townComCharaPopup.m_MessageID = charaState.GetScheduleToPopMessageID();
				}
				else
				{
					townComCharaPopup.m_MessageID = this.m_PopUpMessageID;
					this.m_PopUpMessageID = 0;
				}
				townComCharaPopup.m_SpecialNo = 0;
				townComCharaPopup.m_TownObjectID = charaState.GetStayToTownObjectID();
				townComCharaPopup.m_MessageID = charaState.GetTagDropMessageID();
				townComCharaPopup.m_SpecialNo = 1;
				townComCharaPopup.m_PartsGetItemList = list;
				this.m_Builder.StackEventQue(townComCharaPopup);
			}
			charaState.ClearDropItemState();
		}

		// Token: 0x060022C5 RID: 8901 RVA: 0x000BB58C File Offset: 0x000B998C
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
				if (charaState.IsTouchItemNew())
				{
					charaState.CalcScheduleChangeTagDrop(new IFldNetComModule.CallBack(this.CallbackTouchItemEvt));
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_EVENT_ICON_SELECT, 1f, 0, -1, -1);
					charaState.SetScheduleChangeTagUp();
				}
				else if ((TownPartsCharaHud)this.m_Action.GetAction(typeof(TownPartsCharaHud)) == null)
				{
					TownPartsCharaHud paction = new TownPartsCharaHud(this.m_Builder, this.m_Transform, new Vector3(-0.3f, 0.3f, 0f), this.m_CharaID, charaState.GetScheduleToPopMessageID());
					this.m_Action.EntryAction(paction, 0);
				}
			}
			return true;
		}

		// Token: 0x060022C6 RID: 8902 RVA: 0x000BB653 File Offset: 0x000B9A53
		public void SetCharaHaveItem(int flevel)
		{
			this.m_CharaIcons.ChangeFrame(2 + flevel);
		}

		// Token: 0x060022C7 RID: 8903 RVA: 0x000BB663 File Offset: 0x000B9A63
		public void ClrCharaHaveItem(int flevel)
		{
			this.m_CharaIcons.ChangeFrame(flevel);
		}

		// Token: 0x04002991 RID: 10641
		public int m_CharaID;

		// Token: 0x04002992 RID: 10642
		private TownCharaModel m_CharaIcons;

		// Token: 0x04002993 RID: 10643
		private GameObject m_RenderObject;

		// Token: 0x04002994 RID: 10644
		private long m_LinkAccessMngID;

		// Token: 0x04002995 RID: 10645
		private int m_BuildUpStep;

		// Token: 0x04002996 RID: 10646
		private int m_PopUpMessageID;

		// Token: 0x04002997 RID: 10647
		private long m_BindManageID;

		// Token: 0x04002998 RID: 10648
		private bool m_ScheduleItemNew;

		// Token: 0x04002999 RID: 10649
		private bool m_RebindWait;

		// Token: 0x0400299A RID: 10650
		private TownObjHandleChara.eState m_State;

		// Token: 0x020006DB RID: 1755
		private enum eState
		{
			// Token: 0x0400299C RID: 10652
			Init,
			// Token: 0x0400299D RID: 10653
			Main,
			// Token: 0x0400299E RID: 10654
			Sleep,
			// Token: 0x0400299F RID: 10655
			EndStart,
			// Token: 0x040029A0 RID: 10656
			End
		}

		// Token: 0x020006DC RID: 1756
		private enum eCharaUI
		{
			// Token: 0x040029A2 RID: 10658
			Non,
			// Token: 0x040029A3 RID: 10659
			PopUp,
			// Token: 0x040029A4 RID: 10660
			Traning,
			// Token: 0x040029A5 RID: 10661
			Item,
			// Token: 0x040029A6 RID: 10662
			Money
		}
	}
}
