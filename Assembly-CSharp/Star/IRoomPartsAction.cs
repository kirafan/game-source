﻿using System;

namespace Star
{
	// Token: 0x020005FD RID: 1533
	public class IRoomPartsAction
	{
		// Token: 0x06001E34 RID: 7732 RVA: 0x000A2A1D File Offset: 0x000A0E1D
		public virtual bool PartsUpdate()
		{
			return false;
		}

		// Token: 0x06001E35 RID: 7733 RVA: 0x000A2A20 File Offset: 0x000A0E20
		public virtual void PartsCancel()
		{
		}

		// Token: 0x040024C6 RID: 9414
		public eRoomPartsAction m_Type;

		// Token: 0x040024C7 RID: 9415
		public uint m_UID;

		// Token: 0x040024C8 RID: 9416
		public bool m_Active;
	}
}
