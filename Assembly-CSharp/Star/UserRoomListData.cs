﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000B50 RID: 2896
	[Serializable]
	public class UserRoomListData
	{
		// Token: 0x06003D58 RID: 15704 RVA: 0x001369BC File Offset: 0x00134DBC
		public void CheckEntryFloor(long fplayerid, int fgroupno, UserRoomData proomdata)
		{
			bool flag = false;
			int count = this.m_PlayerFloorList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_PlayerFloorList[i].m_PlayerID == fplayerid)
				{
					this.m_PlayerFloorList[i].CheckFloorData(fgroupno, proomdata);
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				UserRoomListData.PlayerFloorState playerFloorState = new UserRoomListData.PlayerFloorState();
				playerFloorState.m_PlayerID = fplayerid;
				playerFloorState.m_ActiveRoomMngID = proomdata.MngID;
				this.m_PlayerFloorList.Add(playerFloorState);
				playerFloorState.CheckFloorData(fgroupno, proomdata);
			}
		}

		// Token: 0x06003D59 RID: 15705 RVA: 0x00136A50 File Offset: 0x00134E50
		public void ChangePlayerFloorActive(long fplayerid, long fmanageid)
		{
			int count = this.m_PlayerFloorList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_PlayerFloorList[i].m_PlayerID == fplayerid)
				{
					this.m_PlayerFloorList[i].m_ActiveRoomMngID = fmanageid;
					break;
				}
			}
		}

		// Token: 0x06003D5A RID: 15706 RVA: 0x00136AAC File Offset: 0x00134EAC
		public void ReleasePlayerFloorList(long fplayerid)
		{
			int count = this.m_PlayerFloorList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_PlayerFloorList[i].m_PlayerID == fplayerid)
				{
					this.m_PlayerFloorList.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x06003D5B RID: 15707 RVA: 0x00136B00 File Offset: 0x00134F00
		public UserRoomListData.PlayerFloorState GetPlayerRoomList(long fplayerid)
		{
			int count = this.m_PlayerFloorList.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_PlayerFloorList[i].m_PlayerID == fplayerid)
				{
					return this.m_PlayerFloorList[i];
				}
			}
			return null;
		}

		// Token: 0x06003D5C RID: 15708 RVA: 0x00136B50 File Offset: 0x00134F50
		public UserRoomData GetRoomData(long fmanageid)
		{
			int count = this.m_PlayerFloorList.Count;
			for (int i = 0; i < count; i++)
			{
				int count2 = this.m_PlayerFloorList[i].m_RoomList.Count;
				for (int j = 0; j < count2; j++)
				{
					if (this.m_PlayerFloorList[i].m_RoomList[j].MngID == fmanageid)
					{
						return this.m_PlayerFloorList[i].m_RoomList[j];
					}
				}
			}
			return null;
		}

		// Token: 0x06003D5D RID: 15709 RVA: 0x00136BDF File Offset: 0x00134FDF
		public int GetPlayerRoomListNum()
		{
			return this.m_PlayerFloorList.Count;
		}

		// Token: 0x06003D5E RID: 15710 RVA: 0x00136BEC File Offset: 0x00134FEC
		public UserRoomListData.PlayerFloorState GetPlayerRoomListAt(int findex)
		{
			return this.m_PlayerFloorList[findex];
		}

		// Token: 0x06003D5F RID: 15711 RVA: 0x00136BFA File Offset: 0x00134FFA
		public void ClearList()
		{
			this.m_PlayerFloorList.Clear();
		}

		// Token: 0x06003D60 RID: 15712 RVA: 0x00136C07 File Offset: 0x00135007
		public bool IsSetUpActive()
		{
			return this.m_PlayerFloorList.Count == 0;
		}

		// Token: 0x040044CA RID: 17610
		private List<UserRoomListData.PlayerFloorState> m_PlayerFloorList = new List<UserRoomListData.PlayerFloorState>();

		// Token: 0x02000B51 RID: 2897
		[Serializable]
		public class PlayerFloorState
		{
			// Token: 0x06003D61 RID: 15713 RVA: 0x00136C17 File Offset: 0x00135017
			public PlayerFloorState()
			{
				this.m_List = new List<UserRoomListData.PlayerFloorState.FloorData>();
				this.m_RoomList = new List<UserRoomData>();
			}

			// Token: 0x06003D62 RID: 15714 RVA: 0x00136C38 File Offset: 0x00135038
			public void CheckFloorData(int fgroupno, UserRoomData proomdata)
			{
				bool flag = false;
				bool flag2 = false;
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_GroupID == fgroupno)
					{
						flag2 = this.m_List[i].CheckFloorState(proomdata);
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					UserRoomListData.PlayerFloorState.FloorData floorData = new UserRoomListData.PlayerFloorState.FloorData();
					floorData.m_GroupID = fgroupno;
					this.m_List.Add(floorData);
					floorData.CheckFloorState(proomdata);
				}
				if (!flag2)
				{
					this.m_RoomList.Add(proomdata);
				}
			}

			// Token: 0x06003D63 RID: 15715 RVA: 0x00136CD8 File Offset: 0x001350D8
			public UserRoomListData.PlayerFloorState.FloorData GetFloorGroupData(long fmanageid)
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					int count2 = this.m_List[i].m_State.Count;
					for (int j = 0; j < count2; j++)
					{
						if (this.m_List[i].m_State[j].MngID == fmanageid)
						{
							return this.m_List[i];
						}
					}
				}
				return null;
			}

			// Token: 0x06003D64 RID: 15716 RVA: 0x00136D5C File Offset: 0x0013515C
			public bool SearchFloorData(int floorid)
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					int count2 = this.m_List[i].m_State.Count;
					for (int j = 0; j < count2; j++)
					{
						if (this.m_List[i].m_State[j].FloorID == floorid)
						{
							return true;
						}
					}
				}
				return false;
			}

			// Token: 0x06003D65 RID: 15717 RVA: 0x00136DD5 File Offset: 0x001351D5
			public int GetRoomNum()
			{
				return this.m_RoomList.Count;
			}

			// Token: 0x06003D66 RID: 15718 RVA: 0x00136DE2 File Offset: 0x001351E2
			public UserRoomData GetRoomData(int findex)
			{
				return this.m_RoomList[findex];
			}

			// Token: 0x040044CB RID: 17611
			public long m_PlayerID;

			// Token: 0x040044CC RID: 17612
			public long m_ActiveRoomMngID;

			// Token: 0x040044CD RID: 17613
			public List<UserRoomListData.PlayerFloorState.FloorData> m_List;

			// Token: 0x040044CE RID: 17614
			public List<UserRoomData> m_RoomList;

			// Token: 0x02000B52 RID: 2898
			[Serializable]
			public class FloorData
			{
				// Token: 0x06003D67 RID: 15719 RVA: 0x00136DF0 File Offset: 0x001351F0
				public FloorData()
				{
					this.m_State = new List<UserRoomData>();
				}

				// Token: 0x06003D68 RID: 15720 RVA: 0x00136E04 File Offset: 0x00135204
				public bool CheckFloorState(UserRoomData proomdata)
				{
					bool flag = false;
					int count = this.m_State.Count;
					for (int i = 0; i < count; i++)
					{
						if (this.m_State[i].FloorID == proomdata.FloorID && this.m_State[i].MngID == proomdata.MngID)
						{
							this.m_State[i] = proomdata;
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						this.m_State.Add(proomdata);
					}
					return flag;
				}

				// Token: 0x040044CF RID: 17615
				public int m_GroupID;

				// Token: 0x040044D0 RID: 17616
				public List<UserRoomData> m_State;
			}
		}
	}
}
