﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000220 RID: 544
	public class SoundVoiceListDB : ScriptableObject
	{
		// Token: 0x04000E59 RID: 3673
		public SoundVoiceListDB_Param[] m_Params;
	}
}
