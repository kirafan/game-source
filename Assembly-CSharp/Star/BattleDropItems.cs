﻿using System;

namespace Star
{
	// Token: 0x020000BC RID: 188
	[Serializable]
	public class BattleDropItems
	{
		// Token: 0x040003E5 RID: 997
		public BattleDropItem[] Items;
	}
}
