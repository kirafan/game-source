﻿using System;

namespace Star
{
	// Token: 0x0200030D RID: 781
	public enum eXlsBuildMoveCheckType
	{
		// Token: 0x0400162A RID: 5674
		Normal,
		// Token: 0x0400162B RID: 5675
		ResourceID,
		// Token: 0x0400162C RID: 5676
		SelfClass,
		// Token: 0x0400162D RID: 5677
		SelfElement,
		// Token: 0x0400162E RID: 5678
		StrengBuild,
		// Token: 0x0400162F RID: 5679
		ProductBuild,
		// Token: 0x04001630 RID: 5680
		ClassBuild,
		// Token: 0x04001631 RID: 5681
		ElementBuild
	}
}
