﻿using System;

namespace Star
{
	// Token: 0x020005B3 RID: 1459
	public class IRoomCharaAct
	{
		// Token: 0x06001C4D RID: 7245 RVA: 0x00097974 File Offset: 0x00095D74
		public virtual bool UpdateMode(RoomObjectCtrlChara hbase)
		{
			return false;
		}

		// Token: 0x06001C4E RID: 7246 RVA: 0x00097978 File Offset: 0x00095D78
		public virtual bool IsMoveCost(RoomObjectCtrlChara hbase)
		{
			bool result = false;
			if (hbase.GetAStar().GetCost(hbase.BlockData.Pos) != 1)
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06001C4F RID: 7247 RVA: 0x000979A6 File Offset: 0x00095DA6
		public virtual bool CancelMode(RoomObjectCtrlChara pbase, bool fover)
		{
			return true;
		}
	}
}
