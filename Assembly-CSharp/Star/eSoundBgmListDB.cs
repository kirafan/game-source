﻿using System;

namespace Star
{
	// Token: 0x0200020F RID: 527
	public enum eSoundBgmListDB
	{
		// Token: 0x04000CF4 RID: 3316
		None = -1,
		// Token: 0x04000CF5 RID: 3317
		BGM_TITLE,
		// Token: 0x04000CF6 RID: 3318
		BGM_TOWN_1 = 10000,
		// Token: 0x04000CF7 RID: 3319
		BGM_TOWN_2,
		// Token: 0x04000CF8 RID: 3320
		BGM_TOWN_3,
		// Token: 0x04000CF9 RID: 3321
		BGM_QUESTSELECT,
		// Token: 0x04000CFA RID: 3322
		BGM_GACHA,
		// Token: 0x04000CFB RID: 3323
		BGM_GACHAPLAY,
		// Token: 0x04000CFC RID: 3324
		BGM_BATTLE_1,
		// Token: 0x04000CFD RID: 3325
		BGM_BATTLE_2,
		// Token: 0x04000CFE RID: 3326
		BGM_BATTLE_3,
		// Token: 0x04000CFF RID: 3327
		BGM_BATTLE_4,
		// Token: 0x04000D00 RID: 3328
		BGM_BATTLE_5,
		// Token: 0x04000D01 RID: 3329
		BGM_BATTLE_WIN,
		// Token: 0x04000D02 RID: 3330
		BGM_ADV_1 = 20012,
		// Token: 0x04000D03 RID: 3331
		BGM_ADV_2,
		// Token: 0x04000D04 RID: 3332
		BGM_ADV_3,
		// Token: 0x04000D05 RID: 3333
		BGM_ADV_4,
		// Token: 0x04000D06 RID: 3334
		BGM_ADV_5,
		// Token: 0x04000D07 RID: 3335
		BGM_ADV_6,
		// Token: 0x04000D08 RID: 3336
		BGM_ADV_7,
		// Token: 0x04000D09 RID: 3337
		BGM_ADV_8,
		// Token: 0x04000D0A RID: 3338
		BGM_ADV_9,
		// Token: 0x04000D0B RID: 3339
		BGM_ADV_10,
		// Token: 0x04000D0C RID: 3340
		BGM_ADV_11,
		// Token: 0x04000D0D RID: 3341
		BGM_ADV_12,
		// Token: 0x04000D0E RID: 3342
		BGM_ADV_13,
		// Token: 0x04000D0F RID: 3343
		BGM_ADV_14,
		// Token: 0x04000D10 RID: 3344
		BGM_ADV_15,
		// Token: 0x04000D11 RID: 3345
		BGM_ADV_16,
		// Token: 0x04000D12 RID: 3346
		BGM_ADV_17,
		// Token: 0x04000D13 RID: 3347
		BGM_ADV_18,
		// Token: 0x04000D14 RID: 3348
		BGM_5_FIGHTER = 30000,
		// Token: 0x04000D15 RID: 3349
		BGM_5_PRIEST,
		// Token: 0x04000D16 RID: 3350
		BGM_5_KNIGHT,
		// Token: 0x04000D17 RID: 3351
		BGM_5_MAGICIAN,
		// Token: 0x04000D18 RID: 3352
		BGM_5_ALCHMIST,
		// Token: 0x04000D19 RID: 3353
		BGM_4_COMMON,
		// Token: 0x04000D1A RID: 3354
		Max
	}
}
