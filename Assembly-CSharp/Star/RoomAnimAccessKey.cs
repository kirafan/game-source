﻿using System;

namespace Star
{
	// Token: 0x020005FE RID: 1534
	public struct RoomAnimAccessKey
	{
		// Token: 0x040024C9 RID: 9417
		public uint m_AccessKey;

		// Token: 0x040024CA RID: 9418
		public string[] m_KeyName;
	}
}
