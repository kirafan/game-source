﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000737 RID: 1847
	public class TownResourceDownload : MonoBehaviour
	{
		// Token: 0x06002451 RID: 9297 RVA: 0x000C2B9C File Offset: 0x000C0F9C
		private void Awake()
		{
			Debug.Log("Town PreLoad Start = 0.0f");
		}

		// Token: 0x06002452 RID: 9298 RVA: 0x000C2BA8 File Offset: 0x000C0FA8
		private void Start()
		{
			int count = this.m_ListUp.Count;
			for (int i = 0; i < count; i++)
			{
				this.m_ListUp[i].m_Handle = MeigeResourceManager.LoadHandler(this.m_ListUp[i].m_Filename, new MeigeResource.Option[]
				{
					MeigeResource.Option.DownloadOnly
				});
			}
		}

		// Token: 0x06002453 RID: 9299 RVA: 0x000C2C04 File Offset: 0x000C1004
		private void Update()
		{
			this.m_DebugTime += Time.deltaTime;
			int count = this.m_ListUp.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_ListUp[i].m_Handle != null)
				{
					if (this.m_ListUp[i].m_Handle.IsDone)
					{
						MeigeResourceManager.UnloadHandler(this.m_ListUp[i].m_Handle, false);
						this.m_ListUp[i].m_Handle = null;
						this.m_BuildUpNum++;
					}
					else if (this.m_ListUp[i].m_Handle.IsError)
					{
						MeigeResourceManager.RetryHandler(this.m_ListUp[i].m_Handle);
					}
				}
			}
			if (this.m_BuildUpNum >= this.m_ListUp.Count)
			{
				Debug.Log("Town PreLoad End = " + this.m_DebugTime);
			}
		}

		// Token: 0x06002454 RID: 9300 RVA: 0x000C2D10 File Offset: 0x000C1110
		public void AddResource(string ffilename)
		{
			ffilename += ".muast";
			bool flag = false;
			for (int i = 0; i < this.m_ListUp.Count; i++)
			{
				if (this.m_ListUp[i].m_Filename == ffilename)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				TownResourceDownload.ResDownloadState resDownloadState = new TownResourceDownload.ResDownloadState();
				resDownloadState.m_Filename = ffilename;
				this.m_ListUp.Add(resDownloadState);
			}
		}

		// Token: 0x06002455 RID: 9301 RVA: 0x000C2D8A File Offset: 0x000C118A
		public bool IsDownloadEnd()
		{
			return this.m_BuildUpNum >= this.m_ListUp.Count;
		}

		// Token: 0x04002B4E RID: 11086
		private List<TownResourceDownload.ResDownloadState> m_ListUp = new List<TownResourceDownload.ResDownloadState>();

		// Token: 0x04002B4F RID: 11087
		private int m_BuildUpNum;

		// Token: 0x04002B50 RID: 11088
		private float m_DebugTime;

		// Token: 0x02000738 RID: 1848
		public class ResDownloadState
		{
			// Token: 0x04002B51 RID: 11089
			public MeigeResource.Handler m_Handle;

			// Token: 0x04002B52 RID: 11090
			public string m_Filename;
		}
	}
}
