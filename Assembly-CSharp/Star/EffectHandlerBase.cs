﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002F2 RID: 754
	public abstract class EffectHandlerBase : MonoBehaviour
	{
		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000EAC RID: 3756 RVA: 0x0004E36B File Offset: 0x0004C76B
		public string EffectID
		{
			get
			{
				return this.m_EffectID;
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000EAD RID: 3757 RVA: 0x0004E373 File Offset: 0x0004C773
		public Transform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x06000EAE RID: 3758
		public abstract void OnDestroyToCache();

		// Token: 0x040015C1 RID: 5569
		public string m_EffectID;

		// Token: 0x040015C2 RID: 5570
		public Transform m_Transform;
	}
}
