﻿using System;
using System.Reflection;
using UnityEngine;

namespace Star
{
	// Token: 0x020006EF RID: 1775
	public class ITownObjectHandler : MonoBehaviour
	{
		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06002324 RID: 8996 RVA: 0x000B84E1 File Offset: 0x000B68E1
		// (set) Token: 0x06002323 RID: 8995 RVA: 0x000B84DF File Offset: 0x000B68DF
		public virtual bool isUnderConstruction
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06002325 RID: 8997 RVA: 0x000B84E4 File Offset: 0x000B68E4
		public Transform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06002326 RID: 8998 RVA: 0x000B84EC File Offset: 0x000B68EC
		public bool IsEnableRender
		{
			get
			{
				return this.m_IsEnableRender;
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06002327 RID: 8999 RVA: 0x000B84F4 File Offset: 0x000B68F4
		public TownUtility.eResCategory HandleCategory
		{
			get
			{
				return this.m_HandleCategory;
			}
		}

		// Token: 0x06002328 RID: 9000 RVA: 0x000B84FC File Offset: 0x000B68FC
		public int GetBuildAccessID()
		{
			return this.m_BuildAccessID;
		}

		// Token: 0x06002329 RID: 9001 RVA: 0x000B8504 File Offset: 0x000B6904
		public long GetManageID()
		{
			return this.m_ManageID;
		}

		// Token: 0x0600232A RID: 9002 RVA: 0x000B850C File Offset: 0x000B690C
		public int GetObjID()
		{
			return this.m_ObjID;
		}

		// Token: 0x0600232B RID: 9003 RVA: 0x000B8514 File Offset: 0x000B6914
		public static ITownObjectHandler CreateHandler(GameObject pobj, TownUtility.eResCategory fcategory, TownBuilder pbuilder)
		{
			ITownObjectHandler townObjectHandler = null;
			switch (fcategory)
			{
			case TownUtility.eResCategory.Field:
				pobj.name = "TownField";
				townObjectHandler = pobj.AddComponent<TownObjHandleField>();
				break;
			case TownUtility.eResCategory.Room:
			case TownUtility.eResCategory.Buf:
				townObjectHandler = pobj.AddComponent<TownObjHandleBuild>();
				break;
			case TownUtility.eResCategory.Area:
				townObjectHandler = pobj.AddComponent<TownObjHandleArea>();
				break;
			case TownUtility.eResCategory.AreaFree:
				townObjectHandler = pobj.AddComponent<TownObjHandleFree>();
				break;
			case TownUtility.eResCategory.BufFree:
				townObjectHandler = pobj.AddComponent<TownObjHandleBufFree>();
				break;
			case TownUtility.eResCategory.MenuBuf:
				townObjectHandler = pobj.AddComponent<TownObjHandleMenu>();
				break;
			case TownUtility.eResCategory.Content:
				townObjectHandler = pobj.AddComponent<TownObjHandleContent>();
				break;
			case TownUtility.eResCategory.Chara:
				townObjectHandler = pobj.AddComponent<TownObjHandleChara>();
				break;
			case TownUtility.eResCategory.MenuChara:
				townObjectHandler = pobj.AddComponent<TownObjHandleMenuChara>();
				break;
			}
			townObjectHandler.m_Transform = pobj.GetComponent<Transform>();
			townObjectHandler.m_Builder = pbuilder;
			townObjectHandler.m_Action = new TownPartsActionPakage();
			townObjectHandler.m_HandleCategory = fcategory;
			return townObjectHandler;
		}

		// Token: 0x0600232C RID: 9004 RVA: 0x000B85F4 File Offset: 0x000B69F4
		public static void CopyHandle(GameObject pobj, ITownObjectHandler phandle)
		{
			Type type = phandle.GetType();
			Component obj = pobj.AddComponent(type);
			FieldInfo[] fields = type.GetFields();
			for (int i = 0; i < fields.Length; i++)
			{
				fields[i].SetValue(obj, fields[i].GetValue(phandle));
			}
		}

		// Token: 0x0600232D RID: 9005 RVA: 0x000B863D File Offset: 0x000B6A3D
		public void SetCallback(TownBuildUpBufParam.BuildUpEvent pcallback)
		{
			this.m_Callback = pcallback;
		}

		// Token: 0x0600232E RID: 9006 RVA: 0x000B8646 File Offset: 0x000B6A46
		private void Awake()
		{
			this.m_Transform = base.GetComponent<Transform>();
		}

		// Token: 0x0600232F RID: 9007 RVA: 0x000B8654 File Offset: 0x000B6A54
		private void Update()
		{
			if (this.m_IsPreparing)
			{
				this.PrepareMain();
				return;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x06002330 RID: 9008 RVA: 0x000B8674 File Offset: 0x000B6A74
		public bool IsAvailable()
		{
			return this.m_IsAvailable;
		}

		// Token: 0x06002331 RID: 9009 RVA: 0x000B867C File Offset: 0x000B6A7C
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x000B8684 File Offset: 0x000B6A84
		public virtual void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			this.m_ObjID = objID;
			this.m_TimeKey = ftimekey;
			this.m_BuildAccessID = buildPointIndex;
			this.m_LayerID = sortingOrder;
			this.m_InitUp = finitbuild;
			this.m_ManageID = fmanageid;
		}

		// Token: 0x06002333 RID: 9011 RVA: 0x000B86B3 File Offset: 0x000B6AB3
		public virtual void Destroy()
		{
			this.m_Transform = null;
			this.m_Builder = null;
			this.m_IsAvailable = false;
			UnityEngine.Object.Destroy(this);
		}

		// Token: 0x06002334 RID: 9012 RVA: 0x000B86D0 File Offset: 0x000B6AD0
		public virtual void Prepare()
		{
			if (this.m_IsAvailable)
			{
				Debug.Log("TownObjectHandler.Prepare() is failed. m_IsAvailable is true.");
				return;
			}
			this.m_IsPreparing = true;
		}

		// Token: 0x06002335 RID: 9013 RVA: 0x000B86EF File Offset: 0x000B6AEF
		protected virtual bool PrepareMain()
		{
			this.m_IsPreparing = false;
			this.m_IsAvailable = true;
			return true;
		}

		// Token: 0x06002336 RID: 9014 RVA: 0x000B8700 File Offset: 0x000B6B00
		public virtual void SetEnableRender(bool flg)
		{
			this.m_IsEnableRender = flg;
		}

		// Token: 0x06002337 RID: 9015 RVA: 0x000B870C File Offset: 0x000B6B0C
		public void SetLayer(int layerNo)
		{
			Transform[] componentsInChildren = base.gameObject.GetComponentsInChildren<Transform>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].gameObject.layer = layerNo;
			}
		}

		// Token: 0x06002338 RID: 9016 RVA: 0x000B8747 File Offset: 0x000B6B47
		public virtual void DestroyRequest()
		{
		}

		// Token: 0x06002339 RID: 9017 RVA: 0x000B874C File Offset: 0x000B6B4C
		public void MakeColliderLink(GameObject ptarget)
		{
			Collider[] componentsInChildren = ptarget.GetComponentsInChildren<Collider>();
			if (componentsInChildren != null && componentsInChildren.Length > 0)
			{
				int num = componentsInChildren.Length;
				for (int i = 0; i < num; i++)
				{
					TownObjectConnect townObjectConnect = componentsInChildren[i].gameObject.AddComponent<TownObjectConnect>();
					townObjectConnect.m_Link = this;
					townObjectConnect.enabled = false;
				}
			}
		}

		// Token: 0x0600233A RID: 9018 RVA: 0x000B87A1 File Offset: 0x000B6BA1
		public byte GetTimeKey()
		{
			return this.m_TimeKey;
		}

		// Token: 0x0600233B RID: 9019 RVA: 0x000B87A9 File Offset: 0x000B6BA9
		public int GetResourceKey()
		{
			return this.m_ResourceID;
		}

		// Token: 0x0600233C RID: 9020 RVA: 0x000B87B1 File Offset: 0x000B6BB1
		public virtual bool RecvEvent(ITownHandleAction pact)
		{
			return false;
		}

		// Token: 0x0600233D RID: 9021 RVA: 0x000B87B4 File Offset: 0x000B6BB4
		public void LinkHitToNode(GameObject ptarget)
		{
			if (ptarget != null)
			{
				TownObjectConnect townObjectConnect = ptarget.GetComponent<TownObjectConnect>();
				if (townObjectConnect == null)
				{
					townObjectConnect = ptarget.AddComponent<TownObjectConnect>();
				}
				townObjectConnect.m_Link = this;
				townObjectConnect.enabled = false;
			}
		}

		// Token: 0x0600233E RID: 9022 RVA: 0x000B87F8 File Offset: 0x000B6BF8
		public static void LinkHitMesh(Transform ptrs)
		{
			MeshFilter component = ptrs.gameObject.GetComponent<MeshFilter>();
			if (component != null)
			{
				ptrs.gameObject.layer = LayerMask.NameToLayer("TownObject");
				MeshCollider meshCollider = ptrs.gameObject.AddComponent<MeshCollider>();
				meshCollider.sharedMesh = component.mesh;
			}
		}

		// Token: 0x0600233F RID: 9023 RVA: 0x000B884C File Offset: 0x000B6C4C
		public void SetColliderActive(GameObject ptarget, bool factive)
		{
			Collider[] componentsInChildren = ptarget.GetComponentsInChildren<Collider>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].enabled = factive;
			}
		}

		// Token: 0x06002340 RID: 9024 RVA: 0x000B887D File Offset: 0x000B6C7D
		public virtual bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			return false;
		}

		// Token: 0x040029F9 RID: 10745
		protected Transform m_Transform;

		// Token: 0x040029FA RID: 10746
		protected TownBuilder m_Builder;

		// Token: 0x040029FB RID: 10747
		protected bool m_IsAvailable;

		// Token: 0x040029FC RID: 10748
		protected bool m_IsPreparing;

		// Token: 0x040029FD RID: 10749
		protected bool m_IsEnableRender = true;

		// Token: 0x040029FE RID: 10750
		protected bool m_InitUp = true;

		// Token: 0x040029FF RID: 10751
		protected TownUtility.eResCategory m_HandleCategory;

		// Token: 0x04002A00 RID: 10752
		protected int m_ResourceID;

		// Token: 0x04002A01 RID: 10753
		protected int m_BuildAccessID;

		// Token: 0x04002A02 RID: 10754
		protected int m_ObjID;

		// Token: 0x04002A03 RID: 10755
		protected int m_LayerID;

		// Token: 0x04002A04 RID: 10756
		protected byte m_TimeKey;

		// Token: 0x04002A05 RID: 10757
		protected TownPartsActionPakage m_Action;

		// Token: 0x04002A06 RID: 10758
		protected TownBuildUpBufParam.BuildUpEvent m_Callback;

		// Token: 0x04002A07 RID: 10759
		protected long m_ManageID;

		// Token: 0x020006F0 RID: 1776
		public enum eTouchState
		{
			// Token: 0x04002A09 RID: 10761
			Begin,
			// Token: 0x04002A0A RID: 10762
			Move,
			// Token: 0x04002A0B RID: 10763
			End,
			// Token: 0x04002A0C RID: 10764
			Cancel
		}
	}
}
