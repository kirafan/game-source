﻿using System;
using TownResponseTypes;

namespace Star
{
	// Token: 0x02000686 RID: 1670
	public class TownComAPIGetAll : INetComHandle
	{
		// Token: 0x060021B7 RID: 8631 RVA: 0x000B3942 File Offset: 0x000B1D42
		public TownComAPIGetAll()
		{
			this.ApiName = "player/town/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}

		// Token: 0x04002820 RID: 10272
		public long playerId;
	}
}
