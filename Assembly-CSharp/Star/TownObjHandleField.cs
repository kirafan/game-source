﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020006DF RID: 1759
	public class TownObjHandleField : TownObjModelHandle
	{
		// Token: 0x060022D4 RID: 8916 RVA: 0x000BBF9E File Offset: 0x000BA39E
		private void Update()
		{
			if (this.m_IsPreparing)
			{
				this.PrepareMain();
				return;
			}
		}

		// Token: 0x060022D5 RID: 8917 RVA: 0x000BBFB3 File Offset: 0x000BA3B3
		public override void Destroy()
		{
			if (this.m_BuildKey != null)
			{
				this.m_BuildKey.Release();
				this.m_BuildKey = null;
			}
			base.Destroy();
		}

		// Token: 0x060022D6 RID: 8918 RVA: 0x000BBFD8 File Offset: 0x000BA3D8
		private void OnDestroy()
		{
			this.m_BuildKey = null;
		}

		// Token: 0x060022D7 RID: 8919 RVA: 0x000BBFE4 File Offset: 0x000BA3E4
		protected override bool PrepareMain()
		{
			if (!base.PrepareMain())
			{
				return false;
			}
			TownBuildTree buildTree = this.m_Builder.GetBuildTree();
			this.m_BuildKey = new TownObjHandleField.CFieldPointSetUp();
			TownObjHandleField.MakeBuildPoint(this.m_Transform, this.m_BuildKey, 6);
			int sceneLevel = this.m_Builder.GetSceneLevel();
			int num = this.m_BuildKey.GetBuildNum();
			for (int i = 0; i < num; i++)
			{
				TownObjHandleField.CFieldBuildGroup buildPoint = this.m_BuildKey.GetBuildPoint(i);
				TownObjHandleField.CFieldBuildUpKey cfieldBuildUpKey = buildPoint.GetRootNode();
				TownBuildPakage pakage = cfieldBuildUpKey.m_Pakage;
				TownObjHandleFree townObjHandleFree = (TownObjHandleFree)ITownObjectHandler.CreateHandler(cfieldBuildUpKey.m_Object, TownUtility.eResCategory.AreaFree, this.m_Builder);
				townObjHandleFree.SetupHandle(0, pakage.m_Index, -1L, pakage.m_Layer, this.m_TimeKey, true);
				townObjHandleFree.SetBuildUpCallBack(new TownObjHandleFree.CallbackBuildUp(this.CallBackFreeArea), pakage);
				townObjHandleFree.Prepare();
				pakage.SetLinkHandle(townObjHandleFree);
				if (cfieldBuildUpKey.m_WakeUpKey > sceneLevel)
				{
					cfieldBuildUpKey.m_Object.SetActive(false);
				}
				TownHitNodeState townHitNodeState = cfieldBuildUpKey.m_Object.AddComponent<TownHitNodeState>();
				townHitNodeState.CreateHitTable(buildPoint.m_Hit.Count);
				for (int j = 0; j < buildPoint.m_Hit.Count; j++)
				{
					townHitNodeState.AddHitNode(buildPoint.m_Hit[j].m_Model, buildPoint.m_Hit[j].m_Node, buildPoint.m_Hit[j].m_Type, buildPoint.m_Hit[j].m_AccessKey);
				}
				for (int j = 0; j < buildPoint.m_Loc.Count; j++)
				{
					townHitNodeState.AddLocNode(buildPoint.m_Loc[j].m_Trs, buildPoint.m_Loc[j].m_Type, buildPoint.m_Loc[j].m_AccessKey);
				}
			}
			num = this.m_BuildKey.GetMenuNum();
			for (int i = 0; i < num; i++)
			{
				TownObjHandleField.CFieldBuildUpKey cfieldBuildUpKey = this.m_BuildKey.GetMenuPoint(i);
				TownBuildPakage pakage = cfieldBuildUpKey.m_Pakage;
				buildTree.AddBuildPoint(new TownBuildLinkPoint(eBuildPointCategory.Menu)
				{
					m_Object = cfieldBuildUpKey.m_Object,
					m_Pakage = cfieldBuildUpKey.m_Pakage,
					m_AccessID = pakage.m_Index
				});
			}
			return true;
		}

		// Token: 0x060022D8 RID: 8920 RVA: 0x000BC244 File Offset: 0x000BA644
		private void CallBackFreeArea(TownBuildPakage pakage)
		{
			TownBuildTree buildTree = this.m_Builder.GetBuildTree();
			buildTree.AddBuildPoint(new TownBuildLinkPoint(eBuildPointCategory.Area)
			{
				m_Object = pakage.gameObject,
				m_Pakage = pakage,
				m_AccessID = pakage.m_Index
			});
		}

		// Token: 0x060022D9 RID: 8921 RVA: 0x000BC28C File Offset: 0x000BA68C
		public static void MakeBuildPoint(Transform ptrs, TownObjHandleField.CFieldPointSetUp pup, int fcnt)
		{
			if (fcnt >= 0)
			{
				int num = TownBuildPointUtil.SearchBuildPointDB(ptrs.name);
				if (num >= 0)
				{
					TownBuildPointDB_Param townBuildPointDB_Param;
					TownBuildPointUtil.GetBuildPointDB(out townBuildPointDB_Param, num);
					switch (townBuildPointDB_Param.m_AttachType)
					{
					case 0:
					{
						TownBuildPakage townBuildPakage = ptrs.gameObject.AddComponent<TownBuildPakage>();
						townBuildPakage.m_Index = TownUtility.MakeBuildAreaID(0, 0);
						townBuildPakage.m_Layer = townBuildPointDB_Param.m_Layer * 10;
						TownBuildPointUtil.RepointBuildKey(ref townBuildPointDB_Param, ptrs);
						pup.AddMenuPoint(new TownObjHandleField.CFieldBuildUpKey(ptrs.gameObject, townBuildPakage, eTownOption.Non, townBuildPointDB_Param.m_WakeUpKey));
						break;
					}
					case 1:
					{
						TownBuildPakage townBuildPakage = ptrs.gameObject.AddComponent<TownBuildPakage>();
						townBuildPakage.m_Index = TownUtility.MakeMenuPointID(townBuildPointDB_Param.m_AccessKey);
						townBuildPakage.m_Layer = townBuildPointDB_Param.m_Layer * 10;
						TownBuildPointUtil.RepointBuildKey(ref townBuildPointDB_Param, ptrs);
						pup.AddMenuPoint(new TownObjHandleField.CFieldBuildUpKey(ptrs.gameObject, townBuildPakage, eTownOption.System, townBuildPointDB_Param.m_WakeUpKey));
						break;
					}
					case 2:
					{
						TownBuildPakage townBuildPakage = ptrs.gameObject.AddComponent<TownBuildPakage>();
						townBuildPakage.m_Index = TownUtility.MakeBuildAreaID(townBuildPointDB_Param.m_AccessKey, 0);
						townBuildPakage.m_Layer = townBuildPointDB_Param.m_Layer * 10;
						TownBuildPointUtil.RepointBuildKey(ref townBuildPointDB_Param, ptrs);
						pup.AddBuildPoint(new TownObjHandleField.CFieldBuildUpKey(ptrs.gameObject, townBuildPakage, eTownOption.Area, townBuildPointDB_Param.m_WakeUpKey), townBuildPointDB_Param.m_Group);
						break;
					}
					case 3:
					case 4:
						pup.AddHitNode(ptrs, townBuildPointDB_Param.m_Group, townBuildPointDB_Param.m_AccessKey);
						break;
					case 5:
					case 6:
						ITownObjectHandler.LinkHitMesh(ptrs);
						pup.AddHitModel(ptrs, (eTownOption)townBuildPointDB_Param.m_AttachType, townBuildPointDB_Param.m_Group, townBuildPointDB_Param.m_AccessKey);
						break;
					case 7:
						pup.AddGroupEffectPoint(ptrs, (eTownOption)townBuildPointDB_Param.m_AttachType, townBuildPointDB_Param.m_Group, townBuildPointDB_Param.m_AccessKey);
						break;
					}
				}
				int childCount = ptrs.childCount;
				for (int i = 0; i < childCount; i++)
				{
					TownObjHandleField.MakeBuildPoint(ptrs.GetChild(i), pup, fcnt - 1);
				}
			}
		}

		// Token: 0x060022DA RID: 8922 RVA: 0x000BC480 File Offset: 0x000BA880
		public void OpenArea()
		{
			int sceneLevel = this.m_Builder.GetSceneLevel();
			int buildNum = this.m_BuildKey.GetBuildNum();
			for (int i = 0; i < buildNum; i++)
			{
				TownObjHandleField.CFieldBuildGroup buildPoint = this.m_BuildKey.GetBuildPoint(i);
				TownObjHandleField.CFieldBuildUpKey rootNode = buildPoint.GetRootNode();
				if (rootNode.m_WakeUpKey <= sceneLevel)
				{
					rootNode.m_Object.SetActive(true);
				}
			}
		}

		// Token: 0x040029B5 RID: 10677
		private TownObjHandleField.CFieldPointSetUp m_BuildKey;

		// Token: 0x020006E0 RID: 1760
		public enum eBuildType
		{
			// Token: 0x040029B7 RID: 10679
			Area,
			// Token: 0x040029B8 RID: 10680
			Menu,
			// Token: 0x040029B9 RID: 10681
			Home
		}

		// Token: 0x020006E1 RID: 1761
		public class CFieldBuildUpKey
		{
			// Token: 0x060022DB RID: 8923 RVA: 0x000BC4E5 File Offset: 0x000BA8E5
			public CFieldBuildUpKey(GameObject pobj, TownBuildPakage pakage, eTownOption ftype, int fkey)
			{
				this.m_Object = pobj;
				this.m_Pakage = pakage;
				this.m_Type = ftype;
				this.m_WakeUpKey = fkey;
			}

			// Token: 0x040029BA RID: 10682
			public GameObject m_Object;

			// Token: 0x040029BB RID: 10683
			public TownBuildPakage m_Pakage;

			// Token: 0x040029BC RID: 10684
			public int m_No;

			// Token: 0x040029BD RID: 10685
			public eTownOption m_Type;

			// Token: 0x040029BE RID: 10686
			public int m_WakeUpKey;
		}

		// Token: 0x020006E2 RID: 1762
		public class CFieldBuildHitKey
		{
			// Token: 0x060022DC RID: 8924 RVA: 0x000BC50A File Offset: 0x000BA90A
			public CFieldBuildHitKey(eTownOption ftype, int fkey)
			{
				this.m_Type = ftype;
				this.m_AccessKey = fkey;
			}

			// Token: 0x060022DD RID: 8925 RVA: 0x000BC520 File Offset: 0x000BA920
			public CFieldBuildHitKey(int fkey)
			{
				this.m_AccessKey = fkey;
			}

			// Token: 0x040029BF RID: 10687
			public GameObject m_Node;

			// Token: 0x040029C0 RID: 10688
			public GameObject m_Model;

			// Token: 0x040029C1 RID: 10689
			public eTownOption m_Type;

			// Token: 0x040029C2 RID: 10690
			public int m_AccessKey;
		}

		// Token: 0x020006E3 RID: 1763
		public class CFieldLocatorKey
		{
			// Token: 0x060022DE RID: 8926 RVA: 0x000BC52F File Offset: 0x000BA92F
			public CFieldLocatorKey(Transform pobj, eTownOption ftype, int fkey)
			{
				this.m_Trs = pobj;
				this.m_Type = ftype;
				this.m_AccessKey = fkey;
			}

			// Token: 0x040029C3 RID: 10691
			public Transform m_Trs;

			// Token: 0x040029C4 RID: 10692
			public eTownOption m_Type;

			// Token: 0x040029C5 RID: 10693
			public int m_AccessKey;
		}

		// Token: 0x020006E4 RID: 1764
		public class CFieldBuildGroup
		{
			// Token: 0x060022DF RID: 8927 RVA: 0x000BC54C File Offset: 0x000BA94C
			public CFieldBuildGroup(int fgroupno)
			{
				this.m_GroupNo = fgroupno;
				this.m_Node = new List<TownObjHandleField.CFieldBuildUpKey>();
				this.m_Hit = new List<TownObjHandleField.CFieldBuildHitKey>();
				this.m_Loc = new List<TownObjHandleField.CFieldLocatorKey>();
			}

			// Token: 0x060022E0 RID: 8928 RVA: 0x000BC57C File Offset: 0x000BA97C
			public TownObjHandleField.CFieldBuildUpKey GetRootNode()
			{
				int count = this.m_Node.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_Node[i].m_Type == eTownOption.Area)
					{
						return this.m_Node[i];
					}
				}
				return null;
			}

			// Token: 0x060022E1 RID: 8929 RVA: 0x000BC5CC File Offset: 0x000BA9CC
			public void CheckLinkHitNode(Transform ptrs, int fkey)
			{
				bool flag = false;
				int count = this.m_Hit.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_Hit[i].m_AccessKey == fkey)
					{
						this.m_Hit[i].m_Node = ptrs.gameObject;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildHitKey cfieldBuildHitKey = new TownObjHandleField.CFieldBuildHitKey(fkey);
					cfieldBuildHitKey.m_Node = ptrs.gameObject;
					this.m_Hit.Add(cfieldBuildHitKey);
				}
			}

			// Token: 0x060022E2 RID: 8930 RVA: 0x000BC654 File Offset: 0x000BAA54
			public void CheckLinkHitModel(Transform ptrs, eTownOption ftype, int fkey)
			{
				bool flag = false;
				int count = this.m_Hit.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_Hit[i].m_AccessKey == fkey)
					{
						this.m_Hit[i].m_Model = ptrs.gameObject;
						this.m_Hit[i].m_Type = ftype;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildHitKey cfieldBuildHitKey = new TownObjHandleField.CFieldBuildHitKey(ftype, fkey);
					cfieldBuildHitKey.m_Model = ptrs.gameObject;
					this.m_Hit.Add(cfieldBuildHitKey);
				}
			}

			// Token: 0x060022E3 RID: 8931 RVA: 0x000BC6F0 File Offset: 0x000BAAF0
			public void EntryLocaterNode(Transform ptrs, eTownOption ftype, int fkey)
			{
				TownObjHandleField.CFieldLocatorKey item = new TownObjHandleField.CFieldLocatorKey(ptrs, ftype, fkey);
				this.m_Loc.Add(item);
			}

			// Token: 0x040029C6 RID: 10694
			public int m_GroupNo;

			// Token: 0x040029C7 RID: 10695
			public List<TownObjHandleField.CFieldBuildUpKey> m_Node;

			// Token: 0x040029C8 RID: 10696
			public List<TownObjHandleField.CFieldBuildHitKey> m_Hit;

			// Token: 0x040029C9 RID: 10697
			public List<TownObjHandleField.CFieldLocatorKey> m_Loc;
		}

		// Token: 0x020006E5 RID: 1765
		public class CFieldPointSetUp
		{
			// Token: 0x060022E4 RID: 8932 RVA: 0x000BC712 File Offset: 0x000BAB12
			public CFieldPointSetUp()
			{
				this.m_BuildNode = new List<TownObjHandleField.CFieldBuildGroup>();
				this.m_MenuNode = new List<TownObjHandleField.CFieldBuildUpKey>();
				this.m_LocNode = new List<TownObjHandleField.CFieldLocatorKey>();
			}

			// Token: 0x060022E5 RID: 8933 RVA: 0x000BC73C File Offset: 0x000BAB3C
			public void Release()
			{
				if (this.m_BuildNode != null)
				{
					this.m_BuildNode.Clear();
					this.m_BuildNode = null;
				}
				if (this.m_MenuNode != null)
				{
					this.m_MenuNode.Clear();
					this.m_MenuNode = null;
				}
				if (this.m_LocNode != null)
				{
					this.m_LocNode.Clear();
					this.m_LocNode = null;
				}
			}

			// Token: 0x060022E6 RID: 8934 RVA: 0x000BC7A0 File Offset: 0x000BABA0
			public int GetBuildNum()
			{
				return this.m_BuildNode.Count;
			}

			// Token: 0x060022E7 RID: 8935 RVA: 0x000BC7B0 File Offset: 0x000BABB0
			public void AddBuildPoint(TownObjHandleField.CFieldBuildUpKey ppoint, int fgroupno)
			{
				int count = this.m_BuildNode.Count;
				bool flag = false;
				for (int i = 0; i < count; i++)
				{
					if (this.m_BuildNode[i].m_GroupNo == fgroupno)
					{
						flag = true;
						this.m_BuildNode[i].m_Node.Add(ppoint);
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildGroup cfieldBuildGroup = new TownObjHandleField.CFieldBuildGroup(fgroupno);
					cfieldBuildGroup.m_Node.Add(ppoint);
					this.m_BuildNode.Add(cfieldBuildGroup);
				}
			}

			// Token: 0x060022E8 RID: 8936 RVA: 0x000BC837 File Offset: 0x000BAC37
			public TownObjHandleField.CFieldBuildGroup GetBuildPoint(int findex)
			{
				return this.m_BuildNode[findex];
			}

			// Token: 0x060022E9 RID: 8937 RVA: 0x000BC845 File Offset: 0x000BAC45
			public int GetMenuNum()
			{
				return this.m_MenuNode.Count;
			}

			// Token: 0x060022EA RID: 8938 RVA: 0x000BC852 File Offset: 0x000BAC52
			public void AddMenuPoint(TownObjHandleField.CFieldBuildUpKey ppoint)
			{
				this.m_MenuNode.Add(ppoint);
			}

			// Token: 0x060022EB RID: 8939 RVA: 0x000BC860 File Offset: 0x000BAC60
			public TownObjHandleField.CFieldBuildUpKey GetMenuPoint(int findex)
			{
				return this.m_MenuNode[findex];
			}

			// Token: 0x060022EC RID: 8940 RVA: 0x000BC870 File Offset: 0x000BAC70
			public void AddHitNode(Transform ptrs, int fgroupno, int flinkid)
			{
				int count = this.m_BuildNode.Count;
				bool flag = false;
				for (int i = 0; i < count; i++)
				{
					if (this.m_BuildNode[i].m_GroupNo == fgroupno)
					{
						flag = true;
						this.m_BuildNode[i].CheckLinkHitNode(ptrs, flinkid);
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildGroup cfieldBuildGroup = new TownObjHandleField.CFieldBuildGroup(fgroupno);
					cfieldBuildGroup.CheckLinkHitNode(ptrs, flinkid);
					this.m_BuildNode.Add(cfieldBuildGroup);
				}
			}

			// Token: 0x060022ED RID: 8941 RVA: 0x000BC8F0 File Offset: 0x000BACF0
			public void AddHitModel(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
				int count = this.m_BuildNode.Count;
				bool flag = false;
				for (int i = 0; i < count; i++)
				{
					if (this.m_BuildNode[i].m_GroupNo == fgroupno)
					{
						flag = true;
						this.m_BuildNode[i].CheckLinkHitModel(ptrs, ftype, flinkid);
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildGroup cfieldBuildGroup = new TownObjHandleField.CFieldBuildGroup(fgroupno);
					cfieldBuildGroup.CheckLinkHitModel(ptrs, ftype, flinkid);
					this.m_BuildNode.Add(cfieldBuildGroup);
				}
			}

			// Token: 0x060022EE RID: 8942 RVA: 0x000BC974 File Offset: 0x000BAD74
			public void AddGroupEffectPoint(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
				int count = this.m_BuildNode.Count;
				bool flag = false;
				for (int i = 0; i < count; i++)
				{
					if (this.m_BuildNode[i].m_GroupNo == fgroupno)
					{
						flag = true;
						this.m_BuildNode[i].EntryLocaterNode(ptrs, ftype, flinkid);
						break;
					}
				}
				if (!flag)
				{
					TownObjHandleField.CFieldBuildGroup cfieldBuildGroup = new TownObjHandleField.CFieldBuildGroup(fgroupno);
					cfieldBuildGroup.EntryLocaterNode(ptrs, ftype, flinkid);
					this.m_BuildNode.Add(cfieldBuildGroup);
				}
			}

			// Token: 0x060022EF RID: 8943 RVA: 0x000BC9F8 File Offset: 0x000BADF8
			public void AddEffectPoint(Transform ptrs, eTownOption ftype, int fgroupno, int flinkid)
			{
				TownObjHandleField.CFieldLocatorKey item = new TownObjHandleField.CFieldLocatorKey(ptrs, ftype, flinkid);
				this.m_LocNode.Add(item);
			}

			// Token: 0x040029CA RID: 10698
			public List<TownObjHandleField.CFieldBuildGroup> m_BuildNode;

			// Token: 0x040029CB RID: 10699
			public List<TownObjHandleField.CFieldBuildUpKey> m_MenuNode;

			// Token: 0x040029CC RID: 10700
			public List<TownObjHandleField.CFieldLocatorKey> m_LocNode;
		}
	}
}
