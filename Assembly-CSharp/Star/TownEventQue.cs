﻿using System;

namespace Star
{
	// Token: 0x0200072B RID: 1835
	public class TownEventQue
	{
		// Token: 0x06002433 RID: 9267 RVA: 0x000C2456 File Offset: 0x000C0856
		public TownEventQue(int ftablenum)
		{
			this.m_Table = new ITownEventCommand[ftablenum];
			this.m_Num = 0;
			this.m_Max = ftablenum;
		}

		// Token: 0x06002434 RID: 9268 RVA: 0x000C2478 File Offset: 0x000C0878
		public void PushComCommand(ITownEventCommand pcmd)
		{
			if (this.m_Num < this.m_Max)
			{
				this.m_Table[this.m_Num] = pcmd;
				this.m_Num++;
			}
		}

		// Token: 0x06002435 RID: 9269 RVA: 0x000C24A7 File Offset: 0x000C08A7
		public int GetComCommandNum()
		{
			return this.m_Num;
		}

		// Token: 0x06002436 RID: 9270 RVA: 0x000C24AF File Offset: 0x000C08AF
		public ITownEventCommand GetComCommand(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x06002437 RID: 9271 RVA: 0x000C24BC File Offset: 0x000C08BC
		public void Flush()
		{
			int num = 0;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (!this.m_Table[i].m_Enable)
				{
					this.m_Table[i] = null;
					num++;
				}
			}
			if (num != 0)
			{
				int i = 0;
				while (i < this.m_Num)
				{
					if (this.m_Table[i] == null)
					{
						for (int j = i + 1; j < this.m_Num; j++)
						{
							this.m_Table[j - 1] = this.m_Table[j];
						}
						this.m_Num--;
						this.m_Table[this.m_Num] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x04002B31 RID: 11057
		public int m_Num;

		// Token: 0x04002B32 RID: 11058
		public int m_Max;

		// Token: 0x04002B33 RID: 11059
		public ITownEventCommand[] m_Table;
	}
}
