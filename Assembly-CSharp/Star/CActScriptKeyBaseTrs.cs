﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000553 RID: 1363
	public class CActScriptKeyBaseTrs : IRoomScriptData
	{
		// Token: 0x06001AD5 RID: 6869 RVA: 0x0008EF98 File Offset: 0x0008D398
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyBaseTrs actXlsKeyBaseTrs = (ActXlsKeyBaseTrs)pbase;
			this.m_TargetName = actXlsKeyBaseTrs.m_TargetName;
			this.m_Pos = actXlsKeyBaseTrs.m_Pos;
			this.m_Rot = actXlsKeyBaseTrs.m_Rot;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x040021A7 RID: 8615
		public string m_TargetName;

		// Token: 0x040021A8 RID: 8616
		public uint m_CRCKey;

		// Token: 0x040021A9 RID: 8617
		public Vector3 m_Pos;

		// Token: 0x040021AA RID: 8618
		public float m_Rot;
	}
}
