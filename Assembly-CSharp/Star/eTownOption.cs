﻿using System;

namespace Star
{
	// Token: 0x0200070F RID: 1807
	public enum eTownOption
	{
		// Token: 0x04002AB2 RID: 10930
		Non,
		// Token: 0x04002AB3 RID: 10931
		System,
		// Token: 0x04002AB4 RID: 10932
		Area,
		// Token: 0x04002AB5 RID: 10933
		Buf,
		// Token: 0x04002AB6 RID: 10934
		Content,
		// Token: 0x04002AB7 RID: 10935
		HitContent,
		// Token: 0x04002AB8 RID: 10936
		HitBuf,
		// Token: 0x04002AB9 RID: 10937
		Point
	}
}
