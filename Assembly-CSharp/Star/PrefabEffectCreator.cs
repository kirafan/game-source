﻿using System;
using System.Collections;
using UnityEngine;

namespace Star
{
	// Token: 0x0200028A RID: 650
	public class PrefabEffectCreator : MonoBehaviour
	{
		// Token: 0x06000C2A RID: 3114 RVA: 0x000472B8 File Offset: 0x000456B8
		private void Update()
		{
			PrefabEffectCreator.eStep step = this.m_Step;
			if (step != PrefabEffectCreator.eStep.Setup)
			{
				if (step != PrefabEffectCreator.eStep.Main)
				{
				}
			}
			else
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_PrefabModel);
				this.m_EffectHandler = gameObject.GetComponent<EffectHandler>();
				if (this.m_EffectHandler == null)
				{
					this.m_EffectHandler = gameObject.AddComponent<EffectHandler>();
				}
				gameObject.SetActive(false);
				this.m_EffectHandler.Setup();
				this.m_Step = PrefabEffectCreator.eStep.Main;
			}
		}

		// Token: 0x06000C2B RID: 3115 RVA: 0x00047338 File Offset: 0x00045738
		public bool Setup()
		{
			if (this.m_PrefabModel == null)
			{
				Debug.LogErrorFormat("PrerabEffectCreator >>> PrefabModel is null.", new object[0]);
				return false;
			}
			if (this.m_PrefabAnim == null)
			{
				Debug.LogErrorFormat("PrerabEffectCreator >>> PrefabAnim is null.", new object[0]);
				return false;
			}
			this.m_Step = PrefabEffectCreator.eStep.Setup;
			return true;
		}

		// Token: 0x06000C2C RID: 3116 RVA: 0x00047394 File Offset: 0x00045794
		public IEnumerator PlayOneShot(Vector3 pos, Transform parent)
		{
			if (this.m_Step != PrefabEffectCreator.eStep.Main)
			{
				yield return null;
			}
			if (this.m_EffectHandler != null)
			{
				yield return null;
			}
			if (this.m_PlayOneShotDelay > 0f)
			{
				yield return new WaitForSeconds(this.m_PlayOneShotDelay);
			}
			if (this.m_EffectHandler != null)
			{
				this.m_EffectHandler.gameObject.SetActive(true);
				this.m_EffectHandler.Play(pos, Quaternion.Euler(0f, 180f, 0f), Vector3.one, parent, true, WrapMode.ClampForever, this.m_AnimSpeed);
			}
			yield break;
		}

		// Token: 0x06000C2D RID: 3117 RVA: 0x000473C0 File Offset: 0x000457C0
		public IEnumerator PlayLoop(Vector3 pos, Transform parent)
		{
			if (this.m_Step != PrefabEffectCreator.eStep.Main)
			{
				yield return null;
			}
			if (this.m_EffectHandler != null)
			{
				yield return null;
			}
			if (this.m_EffectHandler != null)
			{
				this.m_EffectHandler.gameObject.SetActive(true);
				this.m_EffectHandler.Play(pos, Quaternion.Euler(0f, 180f, 0f), Vector3.one, parent, true, WrapMode.Loop, this.m_AnimSpeed);
			}
			yield break;
		}

		// Token: 0x040014D3 RID: 5331
		[SerializeField]
		private GameObject m_PrefabModel;

		// Token: 0x040014D4 RID: 5332
		[SerializeField]
		private MeigeAnimClipHolder m_PrefabAnim;

		// Token: 0x040014D5 RID: 5333
		[SerializeField]
		private float m_AnimSpeed = 1f;

		// Token: 0x040014D6 RID: 5334
		[SerializeField]
		private float m_PlayOneShotDelay;

		// Token: 0x040014D7 RID: 5335
		private PrefabEffectCreator.eStep m_Step;

		// Token: 0x040014D8 RID: 5336
		private EffectHandler m_EffectHandler;

		// Token: 0x0200028B RID: 651
		private enum eStep
		{
			// Token: 0x040014DA RID: 5338
			None,
			// Token: 0x040014DB RID: 5339
			Setup,
			// Token: 0x040014DC RID: 5340
			Main
		}
	}
}
