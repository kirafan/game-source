﻿using System;

namespace Star
{
	// Token: 0x020006FB RID: 1787
	public enum eBuildPointCategory
	{
		// Token: 0x04002A35 RID: 10805
		Area,
		// Token: 0x04002A36 RID: 10806
		Contents,
		// Token: 0x04002A37 RID: 10807
		Buf,
		// Token: 0x04002A38 RID: 10808
		Menu
	}
}
