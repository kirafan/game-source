﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200028F RID: 655
	public class SoundDebugMain : MonoBehaviour
	{
		// Token: 0x06000C3B RID: 3131 RVA: 0x0004796C File Offset: 0x00045D6C
		private void Start()
		{
			this.m_Step = SoundDebugMain.eStep.First;
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x00047978 File Offset: 0x00045D78
		private void Update()
		{
			SoundDebugMain.eStep step = this.m_Step;
			if (step != SoundDebugMain.eStep.First)
			{
				if (step != SoundDebugMain.eStep.Prepare_Wait)
				{
					if (step != SoundDebugMain.eStep.Main)
					{
					}
				}
				else if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets())
				{
					this.m_Step = SoundDebugMain.eStep.Main;
				}
			}
			else if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetDatabase(this.m_SoundCueSheetDB, this.m_SoundSeListDB, this.m_SoundBgmListDB, this.m_SoundVoiceListDB, this.m_NamedListDB);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.BGM, 1f, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.SE, 1f, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.VOICE, 1f, true);
				this.m_DebugParam[0] = new SoundDebugMain.DebugParam();
				this.m_DebugParam[1] = new SoundDebugMain.DebugParam();
				this.m_DebugParam[2] = new SoundDebugMain.DebugParam();
				this.ToggleSound(SoundDefine.eCategory.BGM, true);
				this.ToggleSound(SoundDefine.eCategory.SE, true);
				this.m_Step = SoundDebugMain.eStep.Prepare_Wait;
			}
		}

		// Token: 0x06000C3D RID: 3133 RVA: 0x00047AA8 File Offset: 0x00045EA8
		private void ToggleVoiceCueSheet(bool isRight)
		{
			NamedListDB namedListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB;
			eCharaNamedType namedType = (eCharaNamedType)((this.m_VoiceNamedDbIndex < 0) ? -1 : namedListDB.m_Params[this.m_VoiceNamedDbIndex].m_NamedType);
			int num = namedListDB.m_Params.Length;
			if (isRight)
			{
				this.m_VoiceNamedDbIndex++;
				if (this.m_VoiceNamedDbIndex >= num)
				{
					this.m_VoiceNamedDbIndex = 0;
				}
			}
			else
			{
				this.m_VoiceNamedDbIndex--;
				if (this.m_VoiceNamedDbIndex < 0)
				{
					this.m_VoiceNamedDbIndex = num - 1;
				}
			}
			this.m_VOICE_CueSheetNameText.text = namedListDB.m_Params[this.m_VoiceNamedDbIndex].m_NickName;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadVoiceCueSheet(namedType);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadVoiceCueSheet((eCharaNamedType)namedListDB.m_Params[this.m_VoiceNamedDbIndex].m_NamedType);
		}

		// Token: 0x06000C3E RID: 3134 RVA: 0x00047BA0 File Offset: 0x00045FA0
		private void ToggleSound(SoundDefine.eCategory category, bool isRight)
		{
			SoundDebugMain.DebugParam debugParam = this.m_DebugParam[(int)category];
			int index = debugParam.m_Index;
			int num = 0;
			if (category != SoundDefine.eCategory.BGM)
			{
				if (category != SoundDefine.eCategory.SE)
				{
					if (category == SoundDefine.eCategory.VOICE)
					{
						num = this.m_SoundVoiceListDB.m_Params.Length;
					}
				}
				else
				{
					num = this.m_SoundSeListDB.m_Params.Length;
				}
			}
			else
			{
				num = this.m_SoundBgmListDB.m_Params.Length;
			}
			if (isRight)
			{
				debugParam.m_Index++;
				if (debugParam.m_Index >= num)
				{
					debugParam.m_Index = 0;
				}
			}
			else
			{
				debugParam.m_Index--;
				if (debugParam.m_Index < 0)
				{
					debugParam.m_Index = num - 1;
				}
			}
			int index2 = debugParam.m_Index;
			if (category != SoundDefine.eCategory.BGM)
			{
				if (category != SoundDefine.eCategory.SE)
				{
					if (category == SoundDefine.eCategory.VOICE)
					{
						this.m_VOICE_CueNameText.text = this.m_SoundVoiceListDB.m_Params[index2].m_CueName;
					}
				}
				else
				{
					if (index < 0 || this.m_SoundSeListDB.m_Params[index].m_CueSheetID != this.m_SoundSeListDB.m_Params[index2].m_CueSheetID)
					{
						if (index >= 0)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet((eSoundCueSheetDB)this.m_SoundSeListDB.m_Params[index].m_CueSheetID);
						}
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet((eSoundCueSheetDB)this.m_SoundSeListDB.m_Params[index2].m_CueSheetID);
						this.m_Step = SoundDebugMain.eStep.Prepare_Wait;
					}
					this.m_SE_CueNameText.text = this.m_SoundSeListDB.m_Params[index2].m_CueName;
				}
			}
			else
			{
				if (index < 0 || this.m_SoundBgmListDB.m_Params[index].m_CueSheetID != this.m_SoundBgmListDB.m_Params[index2].m_CueSheetID)
				{
					if (index >= 0)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet((eSoundCueSheetDB)this.m_SoundBgmListDB.m_Params[index].m_CueSheetID);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet((eSoundCueSheetDB)this.m_SoundBgmListDB.m_Params[index2].m_CueSheetID);
					this.m_Step = SoundDebugMain.eStep.Prepare_Wait;
				}
				this.m_BGM_CueNameText.text = this.m_SoundBgmListDB.m_Params[index2].m_CueName;
			}
		}

		// Token: 0x06000C3F RID: 3135 RVA: 0x00047E18 File Offset: 0x00046218
		public void OnDecideBGM_Play()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			string cueSheet = this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)this.m_SoundBgmListDB.m_Params[this.m_DebugParam[0].m_Index].m_CueSheetID).m_CueSheet;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(cueSheet, this.m_SoundBgmListDB.m_Params[this.m_DebugParam[0].m_Index].m_CueName, 1f, 0, -1, -1);
		}

		// Token: 0x06000C40 RID: 3136 RVA: 0x00047EA3 File Offset: 0x000462A3
		public void OnDecideBGM_L()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.BGM, false);
		}

		// Token: 0x06000C41 RID: 3137 RVA: 0x00047EBA File Offset: 0x000462BA
		public void OnDecideBGM_R()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.BGM, true);
		}

		// Token: 0x06000C42 RID: 3138 RVA: 0x00047ED4 File Offset: 0x000462D4
		public void OnChangedBGMVolume(float value)
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			float value2 = this.m_BGM_Slider.value;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.BGM, value2, true);
		}

		// Token: 0x06000C43 RID: 3139 RVA: 0x00047F0C File Offset: 0x0004630C
		public void OnDecideSE_Play()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			if (this.m_SeHndl.IsPlaying(false))
			{
				this.m_SeHndl.Stop();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SeHndl, true);
			string cueSheet = this.m_SoundCueSheetDB.GetParam((eSoundCueSheetDB)this.m_SoundSeListDB.m_Params[this.m_DebugParam[1].m_Index].m_CueSheetID).m_CueSheet;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(cueSheet, this.m_SoundSeListDB.m_Params[this.m_DebugParam[1].m_Index].m_CueName, this.m_SeHndl, 1f, 0, -1, -1);
		}

		// Token: 0x06000C44 RID: 3140 RVA: 0x00047FD0 File Offset: 0x000463D0
		public void OnDecideSE_L()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.SE, false);
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x00047FE7 File Offset: 0x000463E7
		public void OnDecideSE_R()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.SE, true);
		}

		// Token: 0x06000C46 RID: 3142 RVA: 0x00048000 File Offset: 0x00046400
		public void OnChangedSEVolume(float value)
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			float value2 = this.m_SE_Slider.value;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.SE, value2, true);
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x00048038 File Offset: 0x00046438
		public void OnDecideVOICECueSheet_L()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleVoiceCueSheet(false);
		}

		// Token: 0x06000C48 RID: 3144 RVA: 0x0004804E File Offset: 0x0004644E
		public void OnDecideVOICECueSheet_R()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleVoiceCueSheet(true);
		}

		// Token: 0x06000C49 RID: 3145 RVA: 0x00048064 File Offset: 0x00046464
		public void OnDecideVOICE_Play()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			if (this.m_VoiceHndl.IsPlaying(false))
			{
				this.m_VoiceHndl.Stop();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_VoiceHndl, true);
			eCharaNamedType eCharaNamedType = (eCharaNamedType)((this.m_VoiceNamedDbIndex < 0) ? -1 : SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.m_Params[this.m_VoiceNamedDbIndex].m_NamedType);
			if (eCharaNamedType != eCharaNamedType.None)
			{
				eSoundVoiceListDB id = (eSoundVoiceListDB)this.m_SoundVoiceListDB.m_Params[this.m_DebugParam[2].m_Index].m_ID;
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(eCharaNamedType, id, this.m_VoiceHndl, 1f, 0, -1, -1);
			}
		}

		// Token: 0x06000C4A RID: 3146 RVA: 0x00048132 File Offset: 0x00046532
		public void OnDecideVOICE_L()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.VOICE, false);
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x00048149 File Offset: 0x00046549
		public void OnDecideVOICE_R()
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleSound(SoundDefine.eCategory.VOICE, true);
		}

		// Token: 0x06000C4C RID: 3148 RVA: 0x00048160 File Offset: 0x00046560
		public void OnChangedVOICEVolume(float value)
		{
			if (this.m_Step != SoundDebugMain.eStep.Main)
			{
				return;
			}
			float value2 = this.m_VOICE_Slider.value;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(SoundDefine.eCategory.VOICE, value2, true);
		}

		// Token: 0x040014EB RID: 5355
		[SerializeField]
		private Text m_BGM_CueNameText;

		// Token: 0x040014EC RID: 5356
		[SerializeField]
		private Slider m_BGM_Slider;

		// Token: 0x040014ED RID: 5357
		[SerializeField]
		private Text m_SE_CueNameText;

		// Token: 0x040014EE RID: 5358
		[SerializeField]
		private Slider m_SE_Slider;

		// Token: 0x040014EF RID: 5359
		[SerializeField]
		private Text m_VOICE_CueSheetNameText;

		// Token: 0x040014F0 RID: 5360
		[SerializeField]
		private Text m_VOICE_CueNameText;

		// Token: 0x040014F1 RID: 5361
		[SerializeField]
		private Slider m_VOICE_Slider;

		// Token: 0x040014F2 RID: 5362
		[SerializeField]
		private SoundCueSheetDB m_SoundCueSheetDB;

		// Token: 0x040014F3 RID: 5363
		[SerializeField]
		private SoundBgmListDB m_SoundBgmListDB;

		// Token: 0x040014F4 RID: 5364
		[SerializeField]
		private SoundSeListDB m_SoundSeListDB;

		// Token: 0x040014F5 RID: 5365
		[SerializeField]
		private SoundVoiceListDB m_SoundVoiceListDB;

		// Token: 0x040014F6 RID: 5366
		[SerializeField]
		private NamedListDB m_NamedListDB;

		// Token: 0x040014F7 RID: 5367
		private SoundHandler m_SeHndl = new SoundHandler();

		// Token: 0x040014F8 RID: 5368
		private SoundHandler m_VoiceHndl = new SoundHandler();

		// Token: 0x040014F9 RID: 5369
		private int m_VoiceNamedDbIndex = -1;

		// Token: 0x040014FA RID: 5370
		private SoundDebugMain.DebugParam[] m_DebugParam = new SoundDebugMain.DebugParam[3];

		// Token: 0x040014FB RID: 5371
		private SoundDebugMain.eStep m_Step = SoundDebugMain.eStep.None;

		// Token: 0x02000290 RID: 656
		private class DebugParam
		{
			// Token: 0x06000C4D RID: 3149 RVA: 0x00048198 File Offset: 0x00046598
			public DebugParam()
			{
				this.m_Index = -1;
			}

			// Token: 0x040014FC RID: 5372
			public int m_Index;
		}

		// Token: 0x02000291 RID: 657
		private enum eStep
		{
			// Token: 0x040014FE RID: 5374
			None = -1,
			// Token: 0x040014FF RID: 5375
			First,
			// Token: 0x04001500 RID: 5376
			Prepare,
			// Token: 0x04001501 RID: 5377
			Prepare_Wait,
			// Token: 0x04001502 RID: 5378
			CueSheetPrepare,
			// Token: 0x04001503 RID: 5379
			CueSheetPrepare_Wait,
			// Token: 0x04001504 RID: 5380
			Main,
			// Token: 0x04001505 RID: 5381
			Destroy_0,
			// Token: 0x04001506 RID: 5382
			Destroy_1
		}
	}
}
