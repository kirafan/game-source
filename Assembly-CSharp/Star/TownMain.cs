﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using Star.UI;
using Star.UI.Global;
using Star.UI.Home;
using Star.UI.Town;
using TownFacilityRequestTypes;
using TownFacilityResponseTypes;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020004B5 RID: 1205
	public class TownMain : GameStateMain
	{
		// Token: 0x1700017C RID: 380
		// (get) Token: 0x0600177B RID: 6011 RVA: 0x00079645 File Offset: 0x00077A45
		// (set) Token: 0x0600177C RID: 6012 RVA: 0x0007964D File Offset: 0x00077A4D
		public bool IsDoneInfoAPI { get; set; }

		// Token: 0x0600177D RID: 6013 RVA: 0x00079656 File Offset: 0x00077A56
		public bool IsIdleStep()
		{
			return this.m_Step == TownMain.eStep.Idle;
		}

		// Token: 0x0600177E RID: 6014 RVA: 0x00079661 File Offset: 0x00077A61
		public bool IsHomeStep()
		{
			return this.m_Step == TownMain.eStep.Home;
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x0600177F RID: 6015 RVA: 0x0007966C File Offset: 0x00077A6C
		public TownBuilder Builder
		{
			get
			{
				return this.m_Builder;
			}
		}

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x06001780 RID: 6016 RVA: 0x00079674 File Offset: 0x00077A74
		public TownUI TownUI
		{
			get
			{
				return this.m_TownUI;
			}
		}

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x06001781 RID: 6017 RVA: 0x0007967C File Offset: 0x00077A7C
		public HomeUI HomeUI
		{
			get
			{
				return this.m_HomeUI;
			}
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06001782 RID: 6018 RVA: 0x00079684 File Offset: 0x00077A84
		// (set) Token: 0x06001783 RID: 6019 RVA: 0x0007968C File Offset: 0x00077A8C
		public TownMain.eTransit Transit
		{
			get
			{
				return this.m_Transit;
			}
			set
			{
				this.m_Transit = value;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x06001784 RID: 6020 RVA: 0x00079695 File Offset: 0x00077A95
		// (set) Token: 0x06001785 RID: 6021 RVA: 0x0007969D File Offset: 0x00077A9D
		public TownMain.BuildRequestData BuildReqData
		{
			get
			{
				return this.m_BuildRequestData;
			}
			set
			{
				this.m_BuildRequestData = value;
			}
		}

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06001786 RID: 6022 RVA: 0x000796A8 File Offset: 0x00077AA8
		// (remove) Token: 0x06001787 RID: 6023 RVA: 0x000796E0 File Offset: 0x00077AE0
		private event Action OnTransit;

		// Token: 0x06001788 RID: 6024 RVA: 0x00079716 File Offset: 0x00077B16
		public void SetTransitCallBack(Action onTransit)
		{
			this.OnTransit = null;
			this.OnTransit += onTransit;
		}

		// Token: 0x06001789 RID: 6025 RVA: 0x00079728 File Offset: 0x00077B28
		private void Start()
		{
			this.m_ComEvent = new TownComManager(256);
			this.m_GameCamera.BuildMoveRange();
			if (this.m_StartUp.m_Ena)
			{
				this.m_GameCamera.SetPositionKey(this.m_StartUp.m_Pos, 0f);
				this.m_GameCamera.SetSizeKey(this.m_StartUp.m_Size, 0f, false);
			}
			this.m_GameCamera.PopBackUpData(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart);
			base.SetNextState(0);
		}

		// Token: 0x0600178A RID: 6026 RVA: 0x000797B8 File Offset: 0x00077BB8
		public override void Destroy()
		{
			base.Destroy();
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.Clear();
			this.Builder.Destroy();
			this.TownUI.Destroy();
			this.HomeUI.Destroy();
		}

		// Token: 0x0600178B RID: 6027 RVA: 0x000797F0 File Offset: 0x00077BF0
		public override bool IsCompleteDestroy()
		{
			return base.IsCompleteDestroy();
		}

		// Token: 0x0600178C RID: 6028 RVA: 0x000797F8 File Offset: 0x00077BF8
		public void DestroyReq()
		{
			this.m_GameCamera.PushPopBackUpData();
		}

		// Token: 0x0600178D RID: 6029 RVA: 0x00079808 File Offset: 0x00077C08
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			switch (nextStateID)
			{
			case 0:
				return new TownState_Init(this);
			case 1:
				return new TownState_Main(this);
			case 2:
				return new TownState_Home(this);
			default:
				if (nextStateID != 2147483646)
				{
					return null;
				}
				return new CommonState_Final(this);
			}
		}

		// Token: 0x0600178E RID: 6030 RVA: 0x0007985C File Offset: 0x00077C5C
		public void RefreshTutorialStep()
		{
			if (this.m_TownUI == null)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.CalcTownTutorialStep();
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.RightBottom);
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(false);
			switch (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep())
			{
			case TutorialManager.eTownTutorialStep.None:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(true);
				this.m_TownUI.SetMainGroupButtonInteractable(true);
				this.SetCameraControllable(true);
				break;
			case TutorialManager.eTownTutorialStep.FirstTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Town_001, new Action(this.RefreshTutorialStep), null);
				break;
			case TutorialManager.eTownTutorialStep.BuildContent:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				Vector3 position = GameObject.Find("LOC_AREA_00_3/Content2").transform.position;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowWorldPosition(position);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.TOWN_BUILDCONTENT);
				break;
			}
			case TutorialManager.eTownTutorialStep.WaitBuildContent:
			case TutorialManager.eTownTutorialStep.WaitBuildWeapon:
			case TutorialManager.eTownTutorialStep.WaitBuildMoney:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				break;
			case TutorialManager.eTownTutorialStep.AfterContentTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Town_002, new Action(this.RefreshTutorialStep), null);
				break;
			case TutorialManager.eTownTutorialStep.BuildWeapon:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				Vector3 position2 = GameObject.Find("LOC_AREA_00_3/LOC_07").transform.position;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowWorldPosition(position2);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.TOWN_BUILDWEAPON);
				break;
			}
			case TutorialManager.eTownTutorialStep.CompleteWeapon:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				Vector3 position3 = GameObject.Find("LOC_AREA_00_3/LOC_07").transform.position;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowWorldPosition(position3);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.TOWN_BUILDWEAPONCOMPLETE);
				break;
			}
			case TutorialManager.eTownTutorialStep.BuildMoney:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				Vector3 position4 = GameObject.Find("LOC_AREA_00_3/LOC_06").transform.position;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowWorldPosition(position4);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.TOWN_BUILDWEAPON);
				break;
			}
			case TutorialManager.eTownTutorialStep.CompleteMoney:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_TownUI.SetMainGroupButtonInteractable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				Vector3 position5 = GameObject.Find("LOC_AREA_00_3/LOC_06").transform.position;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowWorldPosition(position5);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.TOWN_BUILDWEAPONCOMPLETE);
				break;
			}
			case TutorialManager.eTownTutorialStep.AfterBuffTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Town_003, new Action(this.RefreshTutorialStep), null);
				break;
			case TutorialManager.eTownTutorialStep.EndTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Town_004, new Action(this.RefreshTutorialStep), null);
				break;
			}
		}

		// Token: 0x0600178F RID: 6031 RVA: 0x00079C41 File Offset: 0x00078041
		public void InitializeBuilder()
		{
			this.m_Builder.Initialize();
			this.m_Builder.SetOwner(this);
			this.m_Builder.OnEventSend += this.OnComEvent;
		}

		// Token: 0x06001790 RID: 6032 RVA: 0x00079C71 File Offset: 0x00078071
		public void OnComEvent(IMainComCommand pcmd)
		{
			this.m_ComEvent.PushComCommand(pcmd);
		}

		// Token: 0x06001791 RID: 6033 RVA: 0x00079C80 File Offset: 0x00078080
		public void FlushCommand()
		{
			int comCommandNum = this.m_ComEvent.GetComCommandNum();
			for (int i = 0; i < comCommandNum; i++)
			{
				IMainComCommand comCommand = this.m_ComEvent.GetComCommand(i);
				switch (comCommand.GetHashCode())
				{
				case 0:
				{
					MainComResultItem mainComResultItem = (MainComResultItem)comCommand;
					this.m_ItemStackList.Add(new TownMain.ItemStack(mainComResultItem.m_ResultNo, mainComResultItem.m_AddNum));
					break;
				}
				case 1:
					this.m_TownUI.UpdateKRRPoint();
					break;
				case 2:
				{
					TownComUI townComUI = (TownComUI)comCommand;
					if (townComUI.m_IsOpen)
					{
						int num = -1;
						long mngID = -1L;
						int num2 = -1;
						townComUI.GetBuildInfomation(out num, out mngID, out num2);
						this.m_BuildRequestData.m_BuildPoint = num;
						this.m_BuildRequestData.m_MngID = mngID;
						this.m_BuildRequestData.m_ObjID = -1;
						switch (townComUI.m_OpenUI)
						{
						case eTownUICategory.Room:
							if (this.m_NowMode == TownMain.eMode.Home)
							{
								this.OnClickRoomButtonCallBack();
							}
							else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.m_TownUI.OpenSelectWindow();
							}
							break;
						case eTownUICategory.FreeArea:
							if (this.m_NowMode == TownMain.eMode.Town)
							{
								if (this.m_TownUI.IsSelect)
								{
									this.ChangeStep(TownMain.eStep.Idle);
								}
								else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildContent)
								{
									int num3 = 2;
									if (num == num3)
									{
										SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
										SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(false);
										this.SelectFreeArea();
									}
								}
								else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
								{
									this.SelectFreeArea();
								}
							}
							break;
						case eTownUICategory.FreeBuf:
							if (this.m_NowMode == TownMain.eMode.Town)
							{
								if (this.m_TownUI.IsSelect)
								{
									this.ChangeStep(TownMain.eStep.Idle);
								}
								else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildWeapon || SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildMoney)
								{
									GameObject gameObject;
									if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildWeapon)
									{
										gameObject = GameObject.Find("LOC_AREA_00_3/LOC_07");
									}
									else
									{
										gameObject = GameObject.Find("LOC_AREA_00_3/LOC_06");
									}
									int buildAccessID = gameObject.GetComponent<TownObjHandleBufFree>().GetBuildAccessID();
									if (num == buildAccessID)
									{
										SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
										SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(false);
										this.SelectFreeBuff();
									}
								}
								else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
								{
									this.SelectFreeBuff();
								}
							}
							break;
						case eTownUICategory.Area:
							if (this.m_NowMode == TownMain.eMode.Town && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.ChangeStep(TownMain.eStep.Idle);
							}
							break;
						case eTownUICategory.Buf:
							if (this.m_NowMode == TownMain.eMode.Town && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.m_TownUI.OpenSelectWindow();
							}
							break;
						case eTownUICategory.Content:
							if (this.m_NowMode == TownMain.eMode.Town && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.m_TownUI.OpenSelectWindow();
							}
							break;
						case eTownUICategory.ShopView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.ChangeSceneShop();
							}
							break;
						case eTownUICategory.SummonView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.ChangeSceneGacha();
							}
							break;
						case eTownUICategory.TraningView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								if (!LocalSaveData.Inst.OccurredTrainingPrepare)
								{
									this.ChangeSceneTraining();
								}
								else
								{
									SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG, 1f, 0, -1, -1);
								}
							}
							break;
						case eTownUICategory.EditView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.ChangeSceneEdit();
							}
							break;
						case eTownUICategory.QuestView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
							{
								this.ChangeSceneQuest();
							}
							break;
						case eTownUICategory.MissionView:
							if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial() && this.m_NowMode == TownMain.eMode.Town)
							{
								this.OpenMissionBoard();
							}
							break;
						}
					}
					else if (this.m_TownUI.IsSelect && !this.m_TownUI.IsActive)
					{
						this.m_TownUI.OpenMainGroup();
					}
					break;
				}
				case 3:
				{
					TownComBuild townComBuild = (TownComBuild)comCommand;
					if (townComBuild.m_BuildType == eTownCommand.BuildArea)
					{
						this.DecideBuildArea(townComBuild);
					}
					else
					{
						this.DecideBuildPoint(townComBuild);
					}
					break;
				}
				case 4:
				{
					TownComComplete townComComplete = (TownComComplete)comCommand;
					FieldMapManager.ActiveBuildObject(townComComplete.m_ManageID, false, null);
					if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
					{
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(false);
						if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.CompleteMoney)
						{
							int roomInCharaNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaNum();
							for (int j = 0; j < roomInCharaNum; j++)
							{
								UserFieldCharaData.RoomInCharaData roomInCharaAt = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaAt(j);
								roomInCharaAt.ReserveReCreateSchedule();
							}
						}
						this.RefreshTutorialStep();
					}
					break;
				}
				case 5:
				{
					TownComCharaPopup townComCharaPopup = (TownComCharaPopup)comCommand;
					eCharaNamedType namedType = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(townComCharaPopup.m_ManageID).Param.NamedType;
					this.m_TownUI.OpenCharaTweetSpecial(townComCharaPopup.m_ManageID, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(namedType).GetParam(eTweetCategory.SpecialTweet).m_ID, townComCharaPopup.m_PartsGetItemList);
					break;
				}
				case 6:
				{
					TownComMoveDecision townComMoveDecision = (TownComMoveDecision)comCommand;
					this.m_MoveBaseBuildPoint = townComMoveDecision.m_BaseBuildPoint;
					this.m_MoveTargetBuildPoint = townComMoveDecision.m_TargetBuildPoint;
					this.m_MoveTargetManageID = townComMoveDecision.m_ChangeMngID;
					this.ChangeStep(TownMain.eStep.MovePointDecide);
					break;
				}
				case 7:
				{
					TownComAddBuildComEnd townComAddBuildComEnd = (TownComAddBuildComEnd)comCommand;
					if (townComAddBuildComEnd != null && townComAddBuildComEnd.m_BuildUpParamList != null)
					{
						for (int k = 0; k < townComAddBuildComEnd.m_BuildUpParamList.Count; k++)
						{
							this.m_TownUI.OnAddBuildComEnd(townComAddBuildComEnd.m_BuildUpParamList[k].m_ManageID);
						}
						if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildContent)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.SetTownTutorialStep(TutorialManager.eTownTutorialStep.WaitBuildContent);
						}
						else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildWeapon)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.SetTownTutorialStep(TutorialManager.eTownTutorialStep.WaitBuildWeapon);
						}
						else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildMoney)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.SetTownTutorialStep(TutorialManager.eTownTutorialStep.WaitBuildMoney);
						}
					}
					break;
				}
				}
			}
			this.m_ComEvent.Flush();
			if (this.m_ItemStackList.Count > 0)
			{
				this.ShowGetItems(this.m_ItemStackList, 1f);
				this.m_ItemStackList.Clear();
			}
		}

		// Token: 0x06001792 RID: 6034 RVA: 0x0007A3D0 File Offset: 0x000787D0
		public void UpdateEditing()
		{
			this.FlushCommand();
			this.UpdateStep();
			this.m_Builder.SetMenuActive(this.m_NowMode == TownMain.eMode.Town && this.m_TownUI.IsActive);
			this.m_Builder.SetMainMode(this.m_NowMode);
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial() && (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildContent || SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildMoney || SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildWeapon))
			{
				this.m_TutorialWaitBuildContent += Time.deltaTime;
				if (this.m_TutorialWaitBuildContent > 3f)
				{
					bool flag = false;
					if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildContent)
					{
						flag = (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildNumByCategory(eTownObjectCategory.Contents) > 0);
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildMoney)
					{
						flag = (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildNumByCategory(eTownObjectCategory.Money) > 0);
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.WaitBuildWeapon)
					{
						flag = (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildNumByCategory(eTownObjectCategory.Item) > 0);
					}
					if (flag)
					{
						this.m_TutorialWaitBuildContent = 0f;
						this.RefreshTutorialStep();
					}
				}
			}
		}

		// Token: 0x06001793 RID: 6035 RVA: 0x0007A543 File Offset: 0x00078943
		private void OnConfirmADVUnlock()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.ADV;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvToNextSceneID = SceneDefine.eSceneID.Town;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart = false;
		}

		// Token: 0x06001794 RID: 6036 RVA: 0x0007A578 File Offset: 0x00078978
		private void UpdateStep()
		{
			TownMain.eStep step = this.m_Step;
			if (step != TownMain.eStep.Home && step != TownMain.eStep.Idle)
			{
				if (step == TownMain.eStep.Mission)
				{
					if (!MissionManager.Inst.IsOpenUI())
					{
						this.m_TownUI.SetEnableInput(true);
						this.m_HomeUI.SetEnableInput(true);
						if (this.m_NowMode == TownMain.eMode.Home)
						{
							this.ChangeStep(TownMain.eStep.Home);
						}
						else
						{
							this.ChangeStep(TownMain.eStep.Idle);
						}
					}
				}
			}
			else
			{
				if (!SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.IsOpen())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackPop();
				}
				if (!SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.IsOpen())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackADVPop(new Action(this.OnConfirmADVUnlock));
				}
			}
		}

		// Token: 0x06001795 RID: 6037 RVA: 0x0007A648 File Offset: 0x00078A48
		private void ChangeStep(TownMain.eStep step)
		{
			this.m_Step = step;
			switch (step)
			{
			case TownMain.eStep.Home:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Town, eSceneInfo.Home);
				this.m_TownUI.SetEnableObjInput(false);
				this.m_TownUI.SetDarkBG(false);
				this.SetCameraControllable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				break;
			case TownMain.eStep.Idle:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Town);
				this.m_TownUI.SetEnableObjInput(true);
				this.m_TownUI.SetDarkBG(false);
				this.SetCameraControllable(true);
				this.m_TownUI.OpenMainGroup();
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				break;
			case TownMain.eStep.BuildPointSelect:
				this.m_TownUI.SetEnableObjInput(true);
				this.m_TownUI.SetDarkBG(false);
				this.SetCameraControllable(true);
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_BuildRequestData.m_ObjID).m_Category == 4)
				{
					this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.FreeArea, this.m_BuildRequestData.m_ObjID, 0L, 0);
				}
				else
				{
					this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.FreeBufPoint, this.m_BuildRequestData.m_ObjID, 0L, 0);
				}
				this.m_TownUI.OpenBuildPointGroup();
				break;
			case TownMain.eStep.BuildPointSelectDecide:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.m_Builder.ClearSelectUI();
				this.ChangeStep(TownMain.eStep.ExecuteBuild);
				break;
			case TownMain.eStep.BuildPointSelectCancel:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.m_BuildRequestData.Clear();
				this.m_Builder.ClearSelectUI();
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			case TownMain.eStep.BuildObjectSelect:
				this.m_TownUI.SetEnableObjInput(false);
				this.m_TownUI.SetDarkBG(true);
				this.SetCameraControllable(false);
				this.m_BuildRequestData.m_MngID = -1L;
				this.m_BuildRequestData.m_ObjID = -1;
				this.m_TownUI.OpenBuildTopWindow();
				break;
			case TownMain.eStep.ExecuteBuild:
			{
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.BuildTownObject(this.m_BuildRequestData);
				if (this.m_BuildRequestData.m_MngID == -1L)
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial() && SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_BuildRequestData.m_ObjID).m_Category == 4)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold -= (long)TownUtility.GetTownObjectLevelUpConfirm(this.m_BuildRequestData.m_ObjID, 1, TownDefine.eTownLevelUpConditionCategory.Gold);
						SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.AddPoint(-TownUtility.GetTownObjectLevelUpConfirm(this.m_BuildRequestData.m_ObjID, 1, TownDefine.eTownLevelUpConditionCategory.Kirara));
					}
					FieldMapManager.BuildUpOptionCode(this.m_BuildRequestData.m_ObjID, 1);
					SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_003, "voice_212", true);
				}
				TownBuildLinkPoint buildPointTreeNode = this.m_Builder.m_ActPakage.m_BuildTrees.GetBuildPointTreeNode(this.m_BuildRequestData.m_BuildPoint);
				if (buildPointTreeNode != null && buildPointTreeNode.m_Object != null && buildPointTreeNode.m_Type == eBuildPointCategory.Area)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(buildPointTreeNode.m_Object, true);
				}
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			}
			case TownMain.eStep.ExecuteLevelUp:
			{
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				UserTownData.BuildObjectData buildObjectDataAtBuildPointIndex = userDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(this.m_BuildRequestData.m_BuildPoint);
				bool flag = UserTownUtil.IsBuildMarks(buildObjectDataAtBuildPointIndex.m_ObjID);
				int num = buildObjectDataAtBuildPointIndex.m_Lv;
				if (!flag)
				{
					buildObjectDataAtBuildPointIndex.AddLevel(1);
					num = buildObjectDataAtBuildPointIndex.m_Lv;
				}
				else
				{
					num = buildObjectDataAtBuildPointIndex.AddChkLevel(1);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold -= (long)TownUtility.GetTownObjectLevelUpConfirm(buildObjectDataAtBuildPointIndex.m_ObjID, num, TownDefine.eTownLevelUpConditionCategory.Gold);
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.AddPoint(-TownUtility.GetTownObjectLevelUpConfirm(buildObjectDataAtBuildPointIndex.m_ObjID, num, TownDefine.eTownLevelUpConditionCategory.Kirara));
				FieldMapManager.BuildUpOptionCode(buildObjectDataAtBuildPointIndex.m_ObjID, num);
				FieldMapManager.ChangeTownBuildLevelWait(this.m_BuildRequestData.m_BuildPoint, buildObjectDataAtBuildPointIndex.m_ManageID, !flag);
				if (flag)
				{
					this.m_Builder.SendHandleAction(ITownHandleAction.CreateAction(eTownEventAct.ReBuildMaker), buildObjectDataAtBuildPointIndex.m_ManageID);
				}
				this.m_Builder.SendHandleAction(ITownHandleAction.CreateAction(eTownEventAct.SelectOff), buildObjectDataAtBuildPointIndex.m_ManageID);
				Vector3 fpos = new Vector3(0f, -0.5f, -50f);
				Vector3 fsize = Vector3.one * 1.5f;
				TownHandleActionEffectPlay pact = new TownHandleActionEffectPlay(5, fpos, fsize);
				this.m_Builder.SendHandleAction(pact, buildObjectDataAtBuildPointIndex.m_ManageID);
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			}
			case TownMain.eStep.ExecuteQuick:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				if (UserTownUtil.GetRemainingTime(this.m_BuildRequestData.m_MngID, -1) <= 0L)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildQuickResultErrTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildQuickResultErrText), null, null);
					this.m_Builder.SendHandleAction(ITownHandleAction.CreateAction(eTownEventAct.SelectOff), this.m_BuildRequestData.m_MngID);
					this.ChangeStep(TownMain.eStep.Idle);
				}
				else
				{
					FieldMapManager.ActiveBuildObject(this.m_BuildRequestData.m_MngID, true, delegate(bool fsuccess)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildQuickResultTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildQuickResultText), null, null);
					});
					this.m_Builder.SendHandleAction(ITownHandleAction.CreateAction(eTownEventAct.SelectOff), this.m_BuildRequestData.m_MngID);
					this.ChangeStep(TownMain.eStep.Idle);
				}
				break;
			case TownMain.eStep.ExecuteStore:
				this.m_TownUI.SetEnableObjInput(false);
				this.m_TownUI.SetDarkBG(true);
				if (UserTownUtil.GetTownObjStockNum() < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimit)
				{
					this.SetCameraControllable(false);
					this.m_Builder.SendHandleAction(ITownHandleAction.CreateAction(eTownEventAct.SelectOff), this.m_BuildRequestData.m_MngID);
					FieldMapManager.RemoveTownBuildObject(this.m_BuildRequestData.m_MngID, this.m_BuildRequestData.m_BuildPoint, delegate(bool fsuccess)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildStockTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildStockResultText), null, null);
					});
				}
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			case TownMain.eStep.ExecuteSell:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.Request_Sale(this.m_BuildRequestData.m_MngID, new MeigewwwParam.Callback(this.OnResponse_Sale));
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			case TownMain.eStep.MovePointSelect:
				this.m_TownUI.SetEnableObjInput(true);
				this.m_TownUI.SetDarkBG(false);
				this.SetCameraControllable(true);
				this.m_TownUI.OpenMovePointGroup();
				break;
			case TownMain.eStep.MovePointDecide:
			{
				this.m_TownUI.SetEnableObjInput(false);
				this.m_TownUI.SetDarkBG(true);
				this.SetCameraControllable(false);
				TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_BuildRequestData.m_ObjID);
				string text = string.Empty;
				switch (param.m_Category)
				{
				case 1:
				case 2:
				case 5:
					if (this.m_MoveTargetManageID == -1L)
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildObjMoveText);
					}
					else
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildObjSwapText);
					}
					break;
				case 4:
				case 6:
					if (this.m_MoveTargetManageID == -1L)
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildAreaMoveText);
					}
					else
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildAreaSwapText);
					}
					break;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildMoveTitle), text, null, new Action<int>(this.OnConfirmMoveCallBack));
				break;
			}
			case TownMain.eStep.MovePointCancel:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.m_BuildRequestData.Clear();
				this.m_Builder.ClearSelectUI();
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			case TownMain.eStep.ExecuteMoveObj:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				switch (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_BuildRequestData.m_ObjID).m_Category)
				{
				case 1:
				case 2:
				case 5:
					if (this.m_MoveTargetManageID == -1L)
					{
						FieldMapManager.BuildMovePoint(this.m_MoveBaseBuildPoint, this.m_MoveTargetBuildPoint);
					}
					else
					{
						FieldMapManager.BuildSwapPoint(this.m_MoveBaseBuildPoint, this.m_MoveTargetBuildPoint);
					}
					break;
				case 4:
				case 6:
					if (this.m_MoveTargetManageID == -1L)
					{
						FieldMapManager.AreaMoveBuild(this.m_MoveBaseBuildPoint, this.m_MoveTargetBuildPoint);
					}
					else
					{
						FieldMapManager.AreaSwapBuild(this.m_MoveBaseBuildPoint, this.m_MoveTargetBuildPoint);
					}
					break;
				}
				this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.Close, -1, 0L, 0);
				this.ChangeStep(TownMain.eStep.Idle);
				break;
			case TownMain.eStep.Mission:
				this.m_TownUI.SetEnableObjInput(false);
				this.SetCameraControllable(false);
				this.m_TownUI.SetEnableInput(false);
				this.m_HomeUI.SetEnableInput(false);
				MissionManager.Inst.OpenUI();
				break;
			case TownMain.eStep.Transit:
				this.m_TownUI.SetEnableInput(false);
				this.m_HomeUI.SetEnableInput(false);
				this.OnTransit.Call();
				break;
			}
		}

		// Token: 0x06001796 RID: 6038 RVA: 0x0007B014 File Offset: 0x00079414
		private void SetCameraControllable(bool flg)
		{
			this.m_GameCamera.SetIsControllable(!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial() && flg);
		}

		// Token: 0x06001797 RID: 6039 RVA: 0x0007B039 File Offset: 0x00079439
		public void SetEnableInputObj(bool flg)
		{
			this.m_TownUI.SetEnableObjInput(flg);
			this.SetCameraControllable(flg);
		}

		// Token: 0x06001798 RID: 6040 RVA: 0x0007B050 File Offset: 0x00079450
		public void SelectFreeBuff()
		{
			if (this.m_BuildRequestData.m_MngID != -1L)
			{
				this.m_TownUI.OpenPlacementWindow(this.m_BuildRequestData.m_MngID);
			}
			else if (this.m_BuildRequestData.m_ObjID != -1)
			{
				this.m_TownUI.OpenBuildWindow(this.m_BuildRequestData.m_ObjID);
			}
			else
			{
				this.m_TownUI.OpenBuildTopWindow();
			}
		}

		// Token: 0x06001799 RID: 6041 RVA: 0x0007B0C4 File Offset: 0x000794C4
		public void SelectFreeArea()
		{
			if (this.m_BuildRequestData.m_MngID != -1L)
			{
				this.m_TownUI.OpenPlacementWindow(this.m_BuildRequestData.m_MngID);
			}
			else if (this.m_BuildRequestData.m_ObjID != -1)
			{
				this.m_TownUI.OpenBuildWindow(this.m_BuildRequestData.m_ObjID);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentCheckTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentCheckText), null, new Action<int>(this.OnConfirmBuildContentArea));
			}
		}

		// Token: 0x0600179A RID: 6042 RVA: 0x0007B16F File Offset: 0x0007956F
		private void OnConfirmBuildContentArea(int btn)
		{
			if (btn == 0)
			{
				this.m_TownUI.OpenBuildAreaWindow(false);
			}
			else
			{
				this.RefreshTutorialStep();
			}
		}

		// Token: 0x0600179B RID: 6043 RVA: 0x0007B18E File Offset: 0x0007958E
		public void ExecuteBuild()
		{
			this.ChangeStep(TownMain.eStep.ExecuteBuild);
		}

		// Token: 0x0600179C RID: 6044 RVA: 0x0007B197 File Offset: 0x00079597
		public void ExecuteLevelUp()
		{
			this.ChangeStep(TownMain.eStep.ExecuteLevelUp);
		}

		// Token: 0x0600179D RID: 6045 RVA: 0x0007B1A1 File Offset: 0x000795A1
		public void ExecuteQuick()
		{
			this.ChangeStep(TownMain.eStep.ExecuteQuick);
		}

		// Token: 0x0600179E RID: 6046 RVA: 0x0007B1AB File Offset: 0x000795AB
		public void ExecuteSell()
		{
			this.ChangeStep(TownMain.eStep.ExecuteSell);
		}

		// Token: 0x0600179F RID: 6047 RVA: 0x0007B1B8 File Offset: 0x000795B8
		public void StartPointSelect()
		{
			switch (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_BuildRequestData.m_ObjID).m_Category)
			{
			case 1:
			case 2:
			case 5:
				this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.MaskBufPoint, this.m_BuildRequestData.m_ObjID, this.m_BuildRequestData.m_MngID, this.m_BuildRequestData.m_BuildPoint);
				break;
			case 4:
				this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.MaskArea, this.m_BuildRequestData.m_ObjID, this.m_BuildRequestData.m_MngID, this.m_BuildRequestData.m_BuildPoint);
				break;
			case 6:
				this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.MaskArea, this.m_BuildRequestData.m_ObjID, this.m_BuildRequestData.m_MngID, this.m_BuildRequestData.m_BuildPoint);
				break;
			}
		}

		// Token: 0x060017A0 RID: 6048 RVA: 0x0007B2A8 File Offset: 0x000796A8
		public void ExecuteMoveObj()
		{
			this.ChangeStep(TownMain.eStep.ExecuteMoveObj);
		}

		// Token: 0x060017A1 RID: 6049 RVA: 0x0007B2B2 File Offset: 0x000796B2
		public void ExecuteStore()
		{
			this.ChangeStep(TownMain.eStep.ExecuteStore);
		}

		// Token: 0x060017A2 RID: 6050 RVA: 0x0007B2BC File Offset: 0x000796BC
		private void BuildTownObject(TownMain.BuildRequestData data)
		{
			this.m_Builder.SetBuildPointsEnable(TownBuilder.eSelectType.Close, -1, 0L, 0);
			int objID = data.m_ObjID;
			int buildPoint = data.m_BuildPoint;
			long mngID = data.m_MngID;
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserTownData userTownData = userDataMng.UserTownData;
			switch (param.m_Category)
			{
			case 1:
			case 2:
			case 5:
				FieldMapManager.AddTownBuildObject(buildPoint, objID, mngID);
				break;
			case 4:
			case 6:
				if (!FieldMapManager.IsPointToBuilding(buildPoint))
				{
					FieldMapManager.AddTownBuildObject(buildPoint, objID, mngID);
				}
				else
				{
					FieldMapManager.ChangeTownBuildObject(buildPoint, objID, mngID);
				}
				break;
			}
		}

		// Token: 0x060017A3 RID: 6051 RVA: 0x0007B37A File Offset: 0x0007977A
		private void DecideBuildPoint(TownComBuild pcmd)
		{
			this.m_BuildRequestData.m_BuildPoint = pcmd.m_BuildPoint;
			this.ChangeStep(TownMain.eStep.BuildPointSelectDecide);
		}

		// Token: 0x060017A4 RID: 6052 RVA: 0x0007B394 File Offset: 0x00079794
		private void DecideBuildArea(TownComBuild pcmd)
		{
			this.m_BuildRequestData.m_BuildPoint = pcmd.m_BuildPoint;
			this.ChangeStep(TownMain.eStep.BuildPointSelectDecide);
		}

		// Token: 0x060017A5 RID: 6053 RVA: 0x0007B3B0 File Offset: 0x000797B0
		public bool InitializeUI()
		{
			this.m_TownUI = UIUtility.GetMenuComponent<TownUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.TownUI].m_SceneName);
			this.m_HomeUI = UIUtility.GetMenuComponent<HomeUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.HomeUI].m_SceneName);
			if (this.m_TownUI == null || this.m_HomeUI == null)
			{
				return false;
			}
			this.m_TownUI.Setup(this);
			this.m_TownUI.OnRoomIn += this.OnClickRoomButtonCallBack;
			this.m_TownUI.OnExecuteBuild += this.ExecuteBuild;
			this.m_TownUI.OnExecuteLevelUp += this.ExecuteLevelUp;
			this.m_TownUI.OnExecuteSell += this.ExecuteSell;
			this.m_TownUI.OnExecuteQuick += this.ExecuteQuick;
			this.m_TownUI.OnExecuteStore += this.ExecuteStore;
			this.m_TownUI.OnClickBuildPointCancelButton += this.OnClickBuildPointCancelButton;
			this.m_TownUI.OnClickMovePointCancelButton += this.OnClickMovePointCancelButton;
			this.m_TownUI.OnClosedTweetWindow += this.OnClosedCharaTweetWindow;
			this.m_TownUI.OnDecideLiveCharas += this.OnDecideLiveChara;
			this.m_TownUI.SetEnableInput(false);
			this.m_HomeUI.Setup();
			this.m_HomeUI.OnClickPartyButton += this.ChangeSceneEdit;
			this.m_HomeUI.OnClickMissionButton += this.OpenMissionBoard;
			this.m_HomeUI.OnClickPresentButton += this.OpenPresentWindow;
			this.m_HomeUI.SetEnableInput(false);
			this.m_IsInitilalizedUI = true;
			if (this.m_NowMode == TownMain.eMode.Home)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.SetCameraHomePosition(0f);
				this.ChangeStep(TownMain.eStep.Home);
				this.m_HomeUI.gameObject.SetActive(true);
				this.m_HomeUI.PlayIn();
				this.m_TownUI.gameObject.SetActive(false);
				this.SetCameraControllable(false);
			}
			else if (this.m_NowMode == TownMain.eMode.Town)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.m_GameCamera.SetPositionKey(this.m_StartUp.m_Pos, 0f);
				this.m_GameCamera.SetSizeKey(this.m_TownStartSize, 0f, false);
				this.ChangeStep(TownMain.eStep.Idle);
				this.m_TownUI.gameObject.SetActive(true);
				this.m_TownUI.PlayIn();
				this.m_HomeUI.gameObject.SetActive(false);
				this.SetCameraControllable(true);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			return true;
		}

		// Token: 0x060017A6 RID: 6054 RVA: 0x0007B680 File Offset: 0x00079A80
		public void StartMode(TownMain.eMode mode)
		{
			if (this.m_NowMode == mode)
			{
				return;
			}
			float num = this.m_TownStartTransitTime;
			if (this.m_NowMode == TownMain.eMode.None)
			{
				num = 0f;
			}
			this.m_NowMode = mode;
			if (this.m_NowMode == TownMain.eMode.Home)
			{
				this.SetCameraHomePosition(num);
				this.ChangeStep(TownMain.eStep.Home);
				this.m_HomeUI.gameObject.SetActive(true);
				this.m_HomeUI.PlayIn();
				this.m_TownUI.gameObject.SetActive(false);
				this.SetCameraControllable(false);
				this.m_TownUI.SetEnableInput(false);
				this.m_HomeUI.SetEnableInput(true);
			}
			else
			{
				this.m_TownUI.SetEnableInput(true);
				this.m_HomeUI.SetEnableInput(false);
				this.m_TownUI.gameObject.SetActive(true);
				this.m_TownUI.PlayIn();
				this.m_HomeUI.gameObject.SetActive(false);
				this.SetCameraControllable(true);
				this.m_GameCamera.SetPositionKey(this.m_StartUp.m_Pos, num);
				this.m_GameCamera.SetSizeKey(this.m_TownStartSize, num, false);
				this.ChangeStep(TownMain.eStep.Idle);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
		}

		// Token: 0x060017A7 RID: 6055 RVA: 0x0007B7B7 File Offset: 0x00079BB7
		public void ReleaseInputBlock()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
		}

		// Token: 0x060017A8 RID: 6056 RVA: 0x0007B7D0 File Offset: 0x00079BD0
		public void EndMode(bool changeMode = false)
		{
			if (this.m_IsInitilalizedUI)
			{
				if (this.m_NowMode == TownMain.eMode.Town)
				{
					this.m_TownUI.PlayOut();
					this.SetCameraControllable(false);
					if (changeMode)
					{
						this.SetCameraHomePosition(this.m_TownStartTransitTime);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
					}
				}
				else
				{
					this.m_HomeUI.PlayOut();
					this.SetCameraControllable(true);
					if (changeMode)
					{
						this.m_GameCamera.SetSizeKey(this.m_TownStartSize, this.m_TownStartTransitTime, false);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
					}
				}
			}
			if (changeMode)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			}
		}

		// Token: 0x060017A9 RID: 6057 RVA: 0x0007B888 File Offset: 0x00079C88
		public void SetCameraHomePosition(float time)
		{
			float num = 750f;
			float num2 = num * (float)Screen.width / (float)Screen.height;
			float num3 = 1334f / num2;
			this.m_GameCamera.SetSizeKey(this.m_StartUp.m_Size * num3, time, true);
			this.m_GameCamera.SetPositionKey(this.m_StartUp.m_Pos, time);
			this.m_GameCamera.SetCameraMoveVec(Vector2.zero);
		}

		// Token: 0x060017AA RID: 6058 RVA: 0x0007B8F4 File Offset: 0x00079CF4
		private void OnClickBuildButton()
		{
			this.ChangeStep(TownMain.eStep.BuildObjectSelect);
		}

		// Token: 0x060017AB RID: 6059 RVA: 0x0007B8FD File Offset: 0x00079CFD
		private void OnClickBuildPointCancelButton()
		{
			this.ChangeStep(TownMain.eStep.BuildPointSelectCancel);
		}

		// Token: 0x060017AC RID: 6060 RVA: 0x0007B906 File Offset: 0x00079D06
		private void OnClickMovePointCancelButton()
		{
			this.ChangeStep(TownMain.eStep.MovePointCancel);
		}

		// Token: 0x060017AD RID: 6061 RVA: 0x0007B910 File Offset: 0x00079D10
		private void OnConfirmMoveCallBack(int btn)
		{
			if (btn == 0)
			{
				this.ChangeStep(TownMain.eStep.ExecuteMoveObj);
			}
			else
			{
				this.ChangeStep(TownMain.eStep.MovePointCancel);
			}
		}

		// Token: 0x060017AE RID: 6062 RVA: 0x0007B92D File Offset: 0x00079D2D
		private void OnConfirmSwapArea(int btn)
		{
			if (btn == 0)
			{
				this.ChangeStep(TownMain.eStep.ExecuteBuild);
			}
			else
			{
				this.ChangeStep(TownMain.eStep.Idle);
			}
		}

		// Token: 0x060017AF RID: 6063 RVA: 0x0007B948 File Offset: 0x00079D48
		private void OnConfirmStore(int btn)
		{
			if (btn == 0)
			{
				this.ChangeStep(TownMain.eStep.ExecuteStore);
			}
			else
			{
				this.m_TownUI.OpenSelectWindow();
			}
		}

		// Token: 0x060017B0 RID: 6064 RVA: 0x0007B968 File Offset: 0x00079D68
		private void OnDecideLiveChara(long[] charaMngIDs)
		{
			this.SetCameraControllable(true);
			if (charaMngIDs != null)
			{
				UserFieldCharaData.UpFieldCharaData[] charaMngTable = UserFieldCharaData.CreateSetUpData(charaMngIDs);
				this.m_Builder.RefreshIcon(charaMngTable);
			}
			else
			{
				this.m_BuildRequestData.m_ObjID = -1;
			}
		}

		// Token: 0x060017B1 RID: 6065 RVA: 0x0007B9A8 File Offset: 0x00079DA8
		private void ShowGetItems(List<TownMain.ItemStack> list, float delay)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < list.Count; i++)
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(list[i].m_ItemID).m_Name);
				stringBuilder.Append("x");
				stringBuilder.Append(list[i].m_Num);
				if (i != list.Count - 1)
				{
					stringBuilder.Append("\n");
				}
			}
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownGetProductTitle), stringBuilder.ToString(), null, delay, null);
		}

		// Token: 0x060017B2 RID: 6066 RVA: 0x0007BA67 File Offset: 0x00079E67
		private void OnClosedCharaTweetWindow()
		{
			this.SetCameraControllable(true);
		}

		// Token: 0x060017B3 RID: 6067 RVA: 0x0007BA70 File Offset: 0x00079E70
		public void OnClickTownButtonCallBack()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Town;
		}

		// Token: 0x060017B4 RID: 6068 RVA: 0x0007BA81 File Offset: 0x00079E81
		public void OnClickRoomButtonCallBack()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Room;
		}

		// Token: 0x060017B5 RID: 6069 RVA: 0x0007BA92 File Offset: 0x00079E92
		public void OnClickTrainingIcon()
		{
			this.ChangeSceneTraining();
		}

		// Token: 0x060017B6 RID: 6070 RVA: 0x0007BA9A File Offset: 0x00079E9A
		public void ChangeSceneQuest()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Quest;
		}

		// Token: 0x060017B7 RID: 6071 RVA: 0x0007BAAB File Offset: 0x00079EAB
		public void ChangeSceneShop()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Shop;
		}

		// Token: 0x060017B8 RID: 6072 RVA: 0x0007BABC File Offset: 0x00079EBC
		public void ChangeSceneEdit()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Edit;
		}

		// Token: 0x060017B9 RID: 6073 RVA: 0x0007BACD File Offset: 0x00079ECD
		public void ChangeSceneGacha()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Gacha;
		}

		// Token: 0x060017BA RID: 6074 RVA: 0x0007BADE File Offset: 0x00079EDE
		public void ChangeSceneTraining()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Training;
		}

		// Token: 0x060017BB RID: 6075 RVA: 0x0007BAEF File Offset: 0x00079EEF
		public void OpenMissionBoard()
		{
			this.ChangeStep(TownMain.eStep.Mission);
		}

		// Token: 0x060017BC RID: 6076 RVA: 0x0007BAF9 File Offset: 0x00079EF9
		public void OpenPresentWindow()
		{
			this.ChangeStep(TownMain.eStep.Transit);
			this.m_Transit = TownMain.eTransit.Present;
		}

		// Token: 0x060017BD RID: 6077 RVA: 0x0007BB0C File Offset: 0x00079F0C
		public void Request_Sale(long mngID, MeigewwwParam.Callback callback)
		{
			TownFacilityRequestTypes.Sale sale = new TownFacilityRequestTypes.Sale();
			long[] array = new long[]
			{
				mngID
			};
			sale.managedTownFacilityId = APIUtility.ArrayToStr<long>(array);
			MeigewwwParam wwwParam = TownFacilityRequest.Sale(sale, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060017BE RID: 6078 RVA: 0x0007BB54 File Offset: 0x00079F54
		protected void OnResponse_Sale(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			TownFacilityResponseTypes.Sale sale = TownFacilityResponse.Sale(wwwParam, ResponseCommon.DialogType.None, null);
			if (sale == null)
			{
				return;
			}
			ResultCode result = sale.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(sale.player, userDataMng.UserData);
				APIUtility.wwwToUserData(sale.managedTownFacilities, userDataMng.UserTownObjDatas);
			}
		}

		// Token: 0x04001E4B RID: 7755
		public const int STATE_INIT = 0;

		// Token: 0x04001E4C RID: 7756
		public const int STATE_MAIN = 1;

		// Token: 0x04001E4D RID: 7757
		public const int STATE_HOME = 2;

		// Token: 0x04001E4E RID: 7758
		[SerializeField]
		private TownCamera m_GameCamera;

		// Token: 0x04001E4F RID: 7759
		[SerializeField]
		private TownBuilder m_Builder;

		// Token: 0x04001E50 RID: 7760
		private TownUI m_TownUI;

		// Token: 0x04001E51 RID: 7761
		private HomeUI m_HomeUI;

		// Token: 0x04001E52 RID: 7762
		[SerializeField]
		private TownMain.StartUpTownInfo m_StartUp = new TownMain.StartUpTownInfo();

		// Token: 0x04001E53 RID: 7763
		[SerializeField]
		private float m_TownStartSize = 2.5f;

		// Token: 0x04001E54 RID: 7764
		[SerializeField]
		private float m_TownStartTransitTime = 0.2f;

		// Token: 0x04001E55 RID: 7765
		private TownMain.eMode m_NowMode;

		// Token: 0x04001E56 RID: 7766
		private bool m_IsInitilalizedUI;

		// Token: 0x04001E58 RID: 7768
		private TownMain.eStep m_Step = TownMain.eStep.Idle;

		// Token: 0x04001E59 RID: 7769
		private TownMain.eTransit m_Transit;

		// Token: 0x04001E5A RID: 7770
		private TownMain.BuildRequestData m_BuildRequestData = new TownMain.BuildRequestData();

		// Token: 0x04001E5B RID: 7771
		private int m_MoveBaseBuildPoint;

		// Token: 0x04001E5C RID: 7772
		private int m_MoveTargetBuildPoint;

		// Token: 0x04001E5D RID: 7773
		private long m_MoveTargetManageID = -1L;

		// Token: 0x04001E5E RID: 7774
		private List<TownMain.ItemStack> m_ItemStackList = new List<TownMain.ItemStack>();

		// Token: 0x04001E5F RID: 7775
		private const float TUTORIALBUILDWAIT = 3f;

		// Token: 0x04001E60 RID: 7776
		private float m_TutorialWaitBuildContent;

		// Token: 0x04001E61 RID: 7777
		private const string TUTORIAL_CONTENT_POINT = "LOC_AREA_00_3/Content2";

		// Token: 0x04001E62 RID: 7778
		private const string TUTORIAL_WEAPON_POINT = "LOC_AREA_00_3/LOC_07";

		// Token: 0x04001E63 RID: 7779
		private const string TUTORIAL_MONEY_POINT = "LOC_AREA_00_3/LOC_06";

		// Token: 0x04001E64 RID: 7780
		private const int TUTORIAL_CONTENT_IDX = 2;

		// Token: 0x04001E65 RID: 7781
		private TownComManager m_ComEvent;

		// Token: 0x020004B6 RID: 1206
		[Serializable]
		private class StartUpTownInfo
		{
			// Token: 0x04001E69 RID: 7785
			public Vector2 m_Pos = new Vector2(-1.98f, 0.33f);

			// Token: 0x04001E6A RID: 7786
			public float m_Size = 1.75f;

			// Token: 0x04001E6B RID: 7787
			public bool m_Ena;
		}

		// Token: 0x020004B7 RID: 1207
		public enum eMode
		{
			// Token: 0x04001E6D RID: 7789
			None,
			// Token: 0x04001E6E RID: 7790
			Home,
			// Token: 0x04001E6F RID: 7791
			Town
		}

		// Token: 0x020004B8 RID: 1208
		private enum eStep
		{
			// Token: 0x04001E71 RID: 7793
			Home,
			// Token: 0x04001E72 RID: 7794
			Idle,
			// Token: 0x04001E73 RID: 7795
			BuildPointSelect,
			// Token: 0x04001E74 RID: 7796
			BuildPointSelectDecide,
			// Token: 0x04001E75 RID: 7797
			BuildPointSelectCancel,
			// Token: 0x04001E76 RID: 7798
			BuildObjectSelect,
			// Token: 0x04001E77 RID: 7799
			BuildObjectSelectArea,
			// Token: 0x04001E78 RID: 7800
			BuildObjectSelectBuff,
			// Token: 0x04001E79 RID: 7801
			ExecuteBuild,
			// Token: 0x04001E7A RID: 7802
			ExecuteLevelUp,
			// Token: 0x04001E7B RID: 7803
			ExecuteQuick,
			// Token: 0x04001E7C RID: 7804
			ExecuteStore,
			// Token: 0x04001E7D RID: 7805
			ExecuteSell,
			// Token: 0x04001E7E RID: 7806
			MovePointSelect,
			// Token: 0x04001E7F RID: 7807
			MovePointDecide,
			// Token: 0x04001E80 RID: 7808
			MovePointCancel,
			// Token: 0x04001E81 RID: 7809
			ExecuteMoveObj,
			// Token: 0x04001E82 RID: 7810
			Mission,
			// Token: 0x04001E83 RID: 7811
			Transit
		}

		// Token: 0x020004B9 RID: 1209
		public enum eTransit
		{
			// Token: 0x04001E85 RID: 7813
			None,
			// Token: 0x04001E86 RID: 7814
			Home,
			// Token: 0x04001E87 RID: 7815
			Town,
			// Token: 0x04001E88 RID: 7816
			Quest,
			// Token: 0x04001E89 RID: 7817
			Edit,
			// Token: 0x04001E8A RID: 7818
			Room,
			// Token: 0x04001E8B RID: 7819
			Gacha,
			// Token: 0x04001E8C RID: 7820
			Shop,
			// Token: 0x04001E8D RID: 7821
			Training,
			// Token: 0x04001E8E RID: 7822
			ADV,
			// Token: 0x04001E8F RID: 7823
			Present
		}

		// Token: 0x020004BA RID: 1210
		public class BuildRequestData
		{
			// Token: 0x060017C3 RID: 6083 RVA: 0x0007BC93 File Offset: 0x0007A093
			public void Clear()
			{
				this.m_ObjID = -1;
				this.m_BuildPoint = -1;
				this.m_MngID = -1L;
			}

			// Token: 0x04001E90 RID: 7824
			public int m_ObjID = -1;

			// Token: 0x04001E91 RID: 7825
			public int m_BuildPoint = -1;

			// Token: 0x04001E92 RID: 7826
			public long m_MngID = -1L;
		}

		// Token: 0x020004BB RID: 1211
		public class ItemStack
		{
			// Token: 0x060017C4 RID: 6084 RVA: 0x0007BCAB File Offset: 0x0007A0AB
			public ItemStack(int id, int num)
			{
				this.m_ItemID = id;
				this.m_Num = num;
			}

			// Token: 0x04001E93 RID: 7827
			public int m_ItemID;

			// Token: 0x04001E94 RID: 7828
			public int m_Num;
		}
	}
}
