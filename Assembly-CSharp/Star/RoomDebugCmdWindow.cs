﻿using System;
using System.Runtime.CompilerServices;
using Meige;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020005AD RID: 1453
	public class RoomDebugCmdWindow : DebugWindowBase
	{
		// Token: 0x06001C10 RID: 7184 RVA: 0x00095004 File Offset: 0x00093404
		public static void OpenCheck()
		{
			if (RoomDebugCmdWindow.ms_Handle == null)
			{
				string resourceName = "Prefab/UI/RoomDebugCmdWindow.muast";
				if (RoomDebugCmdWindow.<>f__mg$cache0 == null)
				{
					RoomDebugCmdWindow.<>f__mg$cache0 = new Action<MeigeResource.Handler>(RoomDebugCmdWindow.CallbackWindowMake);
				}
				MeigeResourceManager.LoadHandler(resourceName, RoomDebugCmdWindow.<>f__mg$cache0, new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
			}
			RoomDebugCmdWindow.ms_ResetPos = true;
		}

		// Token: 0x06001C11 RID: 7185 RVA: 0x00095054 File Offset: 0x00093454
		private static void CallbackWindowMake(MeigeResource.Handler phandle)
		{
			RoomDebugCmdWindow.ms_Handle = phandle;
			GameObject gameObject = phandle.GetAsset<GameObject>();
			gameObject = UnityEngine.Object.Instantiate<GameObject>(gameObject);
			gameObject.AddComponent<RoomDebugCmdWindow>();
		}

		// Token: 0x06001C12 RID: 7186 RVA: 0x0009507C File Offset: 0x0009347C
		private void Start()
		{
			base.WindowSetUp();
			Button button = DebugWindowBase.FindHrcObject(base.transform, "NextButton", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackNextButton));
			}
			this.m_TabCenter = DebugWindowBase.FindHrcTransform(base.transform, "TabCenter");
			this.m_TabForm = new DebugWindowForm[3];
			Transform transform = DebugWindowBase.FindHrcTransform(base.transform, "ObjForm");
			if (transform != null)
			{
				this.m_TabForm[0] = transform.gameObject.AddComponent<RoomObjDebugForm>();
			}
			transform = DebugWindowBase.FindHrcTransform(base.transform, "CharaForm");
			if (transform != null)
			{
				this.m_TabForm[1] = transform.gameObject.AddComponent<RoomCharaDebugForm>();
				transform.gameObject.SetActive(false);
			}
			transform = DebugWindowBase.FindHrcTransform(base.transform, "RoomForm");
			if (transform != null)
			{
				this.m_TabForm[2] = transform.gameObject.AddComponent<RoomFloorDebugForm>();
				transform.gameObject.SetActive(false);
			}
		}

		// Token: 0x06001C13 RID: 7187 RVA: 0x0009519C File Offset: 0x0009359C
		private void Update()
		{
			base.WindowUpdata();
			if (RoomDebugCmdWindow.ms_ResetPos)
			{
				base.ResetWindowPos();
				RoomDebugCmdWindow.ms_ResetPos = false;
			}
		}

		// Token: 0x06001C14 RID: 7188 RVA: 0x000951BC File Offset: 0x000935BC
		public void CallbackNextButton()
		{
			this.m_TabSelect++;
			if (this.m_TabSelect >= this.m_TabForm.Length)
			{
				this.m_TabSelect = 0;
			}
			for (int i = 0; i < this.m_TabForm.Length; i++)
			{
				if (this.m_TabForm[i] != null)
				{
					this.m_TabForm[i].SetActive(i == this.m_TabSelect);
				}
			}
			if (this.m_TabForm[this.m_TabSelect] != null)
			{
				this.m_TabForm[this.m_TabSelect].m_Form.localPosition = this.m_TabCenter.localPosition;
			}
		}

		// Token: 0x06001C15 RID: 7189 RVA: 0x0009526D File Offset: 0x0009366D
		private void OnDestroy()
		{
			if (RoomDebugCmdWindow.ms_Handle != null)
			{
				if (SingletonMonoBehaviour<MeigeResourceManager>.Instance != null)
				{
					MeigeResourceManager.UnloadHandler(RoomDebugCmdWindow.ms_Handle, false);
				}
				RoomDebugCmdWindow.ms_Handle = null;
			}
		}

		// Token: 0x0400230A RID: 8970
		private static MeigeResource.Handler ms_Handle;

		// Token: 0x0400230B RID: 8971
		private static bool ms_ResetPos;

		// Token: 0x0400230C RID: 8972
		private int m_TabSelect;

		// Token: 0x0400230D RID: 8973
		private Transform m_TabCenter;

		// Token: 0x0400230E RID: 8974
		private DebugWindowForm[] m_TabForm;

		// Token: 0x0400230F RID: 8975
		[CompilerGenerated]
		private static Action<MeigeResource.Handler> <>f__mg$cache0;
	}
}
