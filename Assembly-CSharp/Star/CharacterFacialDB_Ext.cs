﻿using System;

namespace Star
{
	// Token: 0x0200018C RID: 396
	public static class CharacterFacialDB_Ext
	{
		// Token: 0x06000ADF RID: 2783 RVA: 0x00040E78 File Offset: 0x0003F278
		public static int GetOverrideFacialID(this CharacterFacialDB self, int overrideSetID, eCharaNamedType namedType)
		{
			int result = 0;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == overrideSetID)
				{
					for (int j = 0; j < self.m_Params[i].m_Datas.Length; j++)
					{
						if (self.m_Params[i].m_Datas[j].m_TargetNamed == (int)namedType)
						{
							result = self.m_Params[i].m_Datas[j].m_FacialID;
							break;
						}
					}
					break;
				}
			}
			return result;
		}
	}
}
