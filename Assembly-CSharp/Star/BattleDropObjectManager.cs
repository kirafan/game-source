﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000C0 RID: 192
	public sealed class BattleDropObjectManager
	{
		// Token: 0x0600051D RID: 1309 RVA: 0x0001AD64 File Offset: 0x00019164
		public void Setup(Transform cacheRoot)
		{
			this.m_Root = cacheRoot;
			this.m_Chaces.Clear();
			for (int i = 0; i < 1; i++)
			{
				BattleDropObjectManager.eDropObjType eDropObjType = (BattleDropObjectManager.eDropObjType)i;
				int num = BattleDropObjectManager.CHACE_NUMS[i];
				BattleDropObjectHandler[] array = new BattleDropObjectHandler[num];
				for (int j = 0; j < num; j++)
				{
					if (eDropObjType == BattleDropObjectManager.eDropObjType.TreasureBox)
					{
						array[j] = new BattleTreasureBoxHandler();
					}
				}
				this.m_Chaces.Add(eDropObjType, array);
			}
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x0001ADE8 File Offset: 0x000191E8
		public void Destroy()
		{
			for (int i = 0; i < this.m_ActiveHndls.Count; i++)
			{
				if (this.m_ActiveHndls[i] != null)
				{
					this.m_ActiveHndls[i].Stop();
					this.m_ActiveHndls[i] = null;
				}
			}
			this.m_ActiveHndls.Clear();
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x0001AE4C File Offset: 0x0001924C
		public void Update()
		{
			for (int i = this.m_ActiveHndls.Count - 1; i >= 0; i--)
			{
				if (this.m_ActiveHndls[i] != null)
				{
					this.m_ActiveHndls[i].Update();
					if (!this.m_ActiveHndls[i].IsPlaying())
					{
						this.m_ActiveHndls.RemoveAt(i);
					}
				}
			}
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x0001AEBC File Offset: 0x000192BC
		private BattleDropObjectHandler ScoopCache(BattleDropObjectManager.eDropObjType type)
		{
			BattleDropObjectHandler[] array = this.m_Chaces[type];
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] != null && !array[i].IsPlaying())
				{
					return array[i];
				}
			}
			return null;
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x0001AF04 File Offset: 0x00019304
		public void PlayTreasureBox(BattleDropItem dropItem, Vector3 startPos, Vector3 targetPos, Transform parent, int sortingOrder, Action<BattleDropItem> OnCompleteFunc)
		{
			BattleDropObjectHandler battleDropObjectHandler = this.ScoopCache(BattleDropObjectManager.eDropObjType.TreasureBox);
			if (battleDropObjectHandler != null)
			{
				BattleTreasureBoxHandler battleTreasureBoxHandler = battleDropObjectHandler as BattleTreasureBoxHandler;
				if (battleTreasureBoxHandler != null)
				{
					battleTreasureBoxHandler.SetParameters(dropItem, startPos, targetPos, OnCompleteFunc);
					battleTreasureBoxHandler.Play(this.m_Root, sortingOrder);
					this.m_ActiveHndls.Add(battleTreasureBoxHandler);
				}
			}
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x0001AF54 File Offset: 0x00019354
		public bool IsPlayingTreasureBox()
		{
			for (int i = 0; i < this.m_ActiveHndls.Count; i++)
			{
				BattleTreasureBoxHandler battleTreasureBoxHandler = this.m_ActiveHndls[i] as BattleTreasureBoxHandler;
				if (battleTreasureBoxHandler != null && battleTreasureBoxHandler.IsPlaying())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x040003E9 RID: 1001
		private static readonly int[] CHACE_NUMS = new int[]
		{
			3
		};

		// Token: 0x040003EA RID: 1002
		private Transform m_Root;

		// Token: 0x040003EB RID: 1003
		private List<BattleDropObjectHandler> m_ActiveHndls = new List<BattleDropObjectHandler>();

		// Token: 0x040003EC RID: 1004
		private Dictionary<BattleDropObjectManager.eDropObjType, BattleDropObjectHandler[]> m_Chaces = new Dictionary<BattleDropObjectManager.eDropObjType, BattleDropObjectHandler[]>();

		// Token: 0x020000C1 RID: 193
		public enum eDropObjType
		{
			// Token: 0x040003EE RID: 1006
			None = -1,
			// Token: 0x040003EF RID: 1007
			TreasureBox,
			// Token: 0x040003F0 RID: 1008
			Num
		}
	}
}
