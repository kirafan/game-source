﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Star
{
	// Token: 0x02000284 RID: 644
	public class MasukoDebugMain : MonoBehaviour
	{
		// Token: 0x06000C11 RID: 3089 RVA: 0x00046C90 File Offset: 0x00045090
		private void Start()
		{
			this.m_Step = MasukoDebugMain.eStep.Step_0;
		}

		// Token: 0x06000C12 RID: 3090 RVA: 0x00046C9C File Offset: 0x0004509C
		private void Update()
		{
			switch (this.m_Step)
			{
			case MasukoDebugMain.eStep.Step_0:
				if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
				}
				this.m_Step = MasukoDebugMain.eStep.Step_1;
				break;
			case MasukoDebugMain.eStep.Step_2:
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadPack("btl_always");
				this.m_Stopwatch = new Stopwatch();
				this.m_Stopwatch.Start();
				UnityEngine.Debug.LogFormat(" >>> Start.".Cyan(), new object[0]);
				this.m_Step = MasukoDebugMain.eStep.Step_3;
				break;
			case MasukoDebugMain.eStep.Step_3:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
				{
					this.m_Stopwatch.Stop();
					UnityEngine.Debug.LogFormat(" >>> 経過時間:{0}ms ({1}s)".Cyan(), new object[]
					{
						this.m_Stopwatch.ElapsedMilliseconds,
						this.m_Stopwatch.Elapsed
					});
					this.m_Step = MasukoDebugMain.eStep.Step_1;
				}
				break;
			}
		}

		// Token: 0x06000C13 RID: 3091 RVA: 0x00046DB2 File Offset: 0x000451B2
		private void OnGUI()
		{
			if (this.m_Step == MasukoDebugMain.eStep.Step_1 && GUI.Button(new Rect(20f, 20f, 200f, 50f), "Start"))
			{
				this.m_Step = MasukoDebugMain.eStep.Step_2;
			}
		}

		// Token: 0x040014AB RID: 5291
		private Stopwatch m_Stopwatch;

		// Token: 0x040014AC RID: 5292
		private MasukoDebugMain.eStep m_Step = MasukoDebugMain.eStep.None;

		// Token: 0x02000285 RID: 645
		private enum eStep
		{
			// Token: 0x040014AE RID: 5294
			None = -1,
			// Token: 0x040014AF RID: 5295
			Step_0,
			// Token: 0x040014B0 RID: 5296
			Step_1,
			// Token: 0x040014B1 RID: 5297
			Step_2,
			// Token: 0x040014B2 RID: 5298
			Step_3,
			// Token: 0x040014B3 RID: 5299
			Step_4
		}
	}
}
