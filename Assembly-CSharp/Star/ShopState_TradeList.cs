﻿using System;
using System.Collections.Generic;
using ItemTradeResponseTypes;
using Meige;
using Star.UI;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x02000487 RID: 1159
	public class ShopState_TradeList : ShopState
	{
		// Token: 0x060016AE RID: 5806 RVA: 0x00076660 File Offset: 0x00074A60
		public ShopState_TradeList(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016AF RID: 5807 RVA: 0x00076678 File Offset: 0x00074A78
		public override int GetStateID()
		{
			return 3;
		}

		// Token: 0x060016B0 RID: 5808 RVA: 0x0007667B File Offset: 0x00074A7B
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_TradeList.eStep.First;
		}

		// Token: 0x060016B1 RID: 5809 RVA: 0x00076684 File Offset: 0x00074A84
		public override void OnStateExit()
		{
		}

		// Token: 0x060016B2 RID: 5810 RVA: 0x00076686 File Offset: 0x00074A86
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016B3 RID: 5811 RVA: 0x00076690 File Offset: 0x00074A90
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_TradeList.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_TradeList.eStep.LoadWait;
				break;
			case ShopState_TradeList.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeList.eStep.PlayIn;
				}
				break;
			case ShopState_TradeList.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_TradeList.eStep.Main;
				}
				break;
			case ShopState_TradeList.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 2;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeList.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_TradeList.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_TradeList.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016B4 RID: 5812 RVA: 0x000767B4 File Offset: 0x00074BB4
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopTradeListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			List<ShopManager.TradeData> tradeList = SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.GetTradeList(this.m_Owner.IsLimitList);
			this.m_UI.Setup(this.m_Owner.IsLimitList, tradeList);
			this.m_UI.OnClickExecuteTradeButton += this.OnClickExecuteTradeButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			if (this.m_Owner.IsLimitList)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LimitTrade);
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_007, "voice_210", true);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.NormalTrade);
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_007, "voice_208", true);
			}
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060016B5 RID: 5813 RVA: 0x000768D1 File Offset: 0x00074CD1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060016B6 RID: 5814 RVA: 0x000768E0 File Offset: 0x00074CE0
		private void OnClickButton()
		{
		}

		// Token: 0x060016B7 RID: 5815 RVA: 0x000768E2 File Offset: 0x00074CE2
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060016B8 RID: 5816 RVA: 0x000768FE File Offset: 0x00074CFE
		private void OnClickExecuteTradeButtonCallBack(int recipeID, int idx)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_Trade(recipeID, idx, new Action<MeigewwwParam, Trade>(this.OnResponse_Trade));
		}

		// Token: 0x060016B9 RID: 5817 RVA: 0x00076920 File Offset: 0x00074D20
		private void OnResponse_Trade(MeigewwwParam wwwParam, Trade param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.WEAPON_LIMIT)
				{
					if (result != ResultCode.BUY_OUT_OF_PERIOD)
					{
						APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					}
					else
					{
						this.m_Owner.IsTradeListReset = true;
						APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					}
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
			}
			else
			{
				List<ShopManager.TradeData> tradeList = SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.GetTradeList(this.m_Owner.IsLimitList);
				this.m_UI.Refresh(tradeList);
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_007, "voice_212", false);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTradeTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTradeResult), null, null);
			}
		}

		// Token: 0x04001D69 RID: 7529
		private ShopState_TradeList.eStep m_Step = ShopState_TradeList.eStep.None;

		// Token: 0x04001D6A RID: 7530
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopTradeListUI;

		// Token: 0x04001D6B RID: 7531
		private ShopTradeListUI m_UI;

		// Token: 0x02000488 RID: 1160
		private enum eStep
		{
			// Token: 0x04001D6D RID: 7533
			None = -1,
			// Token: 0x04001D6E RID: 7534
			First,
			// Token: 0x04001D6F RID: 7535
			LoadStart,
			// Token: 0x04001D70 RID: 7536
			LoadWait,
			// Token: 0x04001D71 RID: 7537
			PlayIn,
			// Token: 0x04001D72 RID: 7538
			Main,
			// Token: 0x04001D73 RID: 7539
			UnloadChildSceneWait
		}
	}
}
