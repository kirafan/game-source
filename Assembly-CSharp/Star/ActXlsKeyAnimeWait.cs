﻿using System;

namespace Star
{
	// Token: 0x02000565 RID: 1381
	public class ActXlsKeyAnimeWait : ActXlsKeyBase
	{
		// Token: 0x06001B07 RID: 6919 RVA: 0x0008F57F File Offset: 0x0008D97F
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Float(ref this.m_OffsetTime);
		}

		// Token: 0x040021EF RID: 8687
		public float m_OffsetTime;
	}
}
