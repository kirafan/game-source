﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x0200015A RID: 346
	public class CharaAnimResourceHndlWrapper
	{
		// Token: 0x060009F2 RID: 2546 RVA: 0x0003D1B5 File Offset: 0x0003B5B5
		public CharaAnimResourceHndlWrapper()
		{
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x0003D1C0 File Offset: 0x0003B5C0
		public CharaAnimResourceHndlWrapper(ABResourceObjectHandler abResObjHndl, string actKey, string path)
		{
			this.m_ABResObjHndl = abResObjHndl;
			this.m_ActKeys = new List<string>
			{
				actKey
			};
			this.m_Paths = new List<string>
			{
				path
			};
		}

		// Token: 0x04000947 RID: 2375
		public ABResourceObjectHandler m_ABResObjHndl;

		// Token: 0x04000948 RID: 2376
		public List<string> m_ActKeys;

		// Token: 0x04000949 RID: 2377
		public List<string> m_Paths;
	}
}
