﻿using System;

namespace Star
{
	// Token: 0x020002B8 RID: 696
	public class CharacterDataEmpty : CharacterDataWrapperNoExist
	{
		// Token: 0x06000D12 RID: 3346 RVA: 0x0004936C File Offset: 0x0004776C
		public CharacterDataEmpty() : base(eCharacterDataType.Empty)
		{
		}
	}
}
