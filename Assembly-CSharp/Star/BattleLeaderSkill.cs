﻿using System;

namespace Star
{
	// Token: 0x020000CC RID: 204
	public class BattleLeaderSkill
	{
		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600056D RID: 1389 RVA: 0x0001C14E File Offset: 0x0001A54E
		// (set) Token: 0x0600056E RID: 1390 RVA: 0x0001C156 File Offset: 0x0001A556
		public float StatusHp
		{
			get
			{
				return this.m_StatusHp;
			}
			set
			{
				this.m_StatusHp = value;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600056F RID: 1391 RVA: 0x0001C15F File Offset: 0x0001A55F
		// (set) Token: 0x06000570 RID: 1392 RVA: 0x0001C167 File Offset: 0x0001A567
		public float StatusAtk
		{
			get
			{
				return this.m_StatusAtk;
			}
			set
			{
				this.m_StatusAtk = value;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x06000571 RID: 1393 RVA: 0x0001C170 File Offset: 0x0001A570
		// (set) Token: 0x06000572 RID: 1394 RVA: 0x0001C178 File Offset: 0x0001A578
		public float StatusMgc
		{
			get
			{
				return this.m_StatusMgc;
			}
			set
			{
				this.m_StatusMgc = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000573 RID: 1395 RVA: 0x0001C181 File Offset: 0x0001A581
		// (set) Token: 0x06000574 RID: 1396 RVA: 0x0001C189 File Offset: 0x0001A589
		public float StatusDef
		{
			get
			{
				return this.m_StatusDef;
			}
			set
			{
				this.m_StatusDef = value;
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x0001C192 File Offset: 0x0001A592
		// (set) Token: 0x06000576 RID: 1398 RVA: 0x0001C19A File Offset: 0x0001A59A
		public float StatusMDef
		{
			get
			{
				return this.m_StatusMDef;
			}
			set
			{
				this.m_StatusMDef = value;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000577 RID: 1399 RVA: 0x0001C1A3 File Offset: 0x0001A5A3
		// (set) Token: 0x06000578 RID: 1400 RVA: 0x0001C1AB File Offset: 0x0001A5AB
		public float StatusSpd
		{
			get
			{
				return this.m_StatusSpd;
			}
			set
			{
				this.m_StatusSpd = value;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000579 RID: 1401 RVA: 0x0001C1B4 File Offset: 0x0001A5B4
		// (set) Token: 0x0600057A RID: 1402 RVA: 0x0001C1BC File Offset: 0x0001A5BC
		public float StatusLuck
		{
			get
			{
				return this.m_StatusLuck;
			}
			set
			{
				this.m_StatusLuck = value;
			}
		}

		// Token: 0x0400044F RID: 1103
		private float m_StatusHp;

		// Token: 0x04000450 RID: 1104
		private float m_StatusAtk;

		// Token: 0x04000451 RID: 1105
		private float m_StatusMgc;

		// Token: 0x04000452 RID: 1106
		private float m_StatusDef;

		// Token: 0x04000453 RID: 1107
		private float m_StatusMDef;

		// Token: 0x04000454 RID: 1108
		private float m_StatusSpd;

		// Token: 0x04000455 RID: 1109
		private float m_StatusLuck;
	}
}
