﻿using System;
using Meige;

namespace Star
{
	// Token: 0x0200015B RID: 347
	public sealed class CharacterResourceManager
	{
		// Token: 0x060009F4 RID: 2548 RVA: 0x0003D204 File Offset: 0x0003B604
		public CharacterResourceManager()
		{
			this.m_ABResLoader = new ABResourceLoader(64);
			for (int i = 0; i < this.RESIDENT_PATH_LIST.Length; i++)
			{
				this.m_ABResLoader.AddIgnoreUnloadPathList(this.RESIDENT_PATH_LIST[i]);
			}
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x0003D264 File Offset: 0x0003B664
		public void Update()
		{
			this.m_ABResLoader.Update();
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x0003D271 File Offset: 0x0003B671
		public bool IsAvailable()
		{
			return this.m_ABResLoader != null;
		}

		// Token: 0x060009F7 RID: 2551 RVA: 0x0003D281 File Offset: 0x0003B681
		public bool IsLoadingAny()
		{
			return this.m_ABResLoader.IsLoading();
		}

		// Token: 0x060009F8 RID: 2552 RVA: 0x0003D296 File Offset: 0x0003B696
		public bool IsError()
		{
			return this.m_ABResLoader.IsError();
		}

		// Token: 0x060009F9 RID: 2553 RVA: 0x0003D2AC File Offset: 0x0003B6AC
		public ABResourceObjectHandler Load(string path, params MeigeResource.Option[] options)
		{
			return this.m_ABResLoader.Load(path, options);
		}

		// Token: 0x060009FA RID: 2554 RVA: 0x0003D2C8 File Offset: 0x0003B6C8
		public void Unload(string path)
		{
			this.m_ABResLoader.Unload(path);
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x0003D2D6 File Offset: 0x0003B6D6
		public void Unload(ABResourceObjectHandler unloadHndl)
		{
			this.m_ABResLoader.Unload(unloadHndl);
		}

		// Token: 0x060009FC RID: 2556 RVA: 0x0003D2E4 File Offset: 0x0003B6E4
		public void UnloadAll(bool isUnloadResident = false)
		{
			this.m_ABResLoader.UnloadAll(isUnloadResident);
		}

		// Token: 0x060009FD RID: 2557 RVA: 0x0003D2F2 File Offset: 0x0003B6F2
		public bool IsCompleteUnloadAll()
		{
			return this.m_ABResLoader.IsCompleteUnloadAll();
		}

		// Token: 0x0400094A RID: 2378
		private readonly string[] RESIDENT_PATH_LIST = new string[]
		{
			"anim/player/common_battle_body.muast"
		};

		// Token: 0x0400094B RID: 2379
		private const int SIMULTANEOUSLY_LOAD_NUM = 64;

		// Token: 0x0400094C RID: 2380
		public ABResourceLoader m_ABResLoader;
	}
}
