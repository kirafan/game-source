﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000F0 RID: 240
	public class BattleTitleCutIn : MonoBehaviour
	{
		// Token: 0x060006E2 RID: 1762 RVA: 0x00029111 File Offset: 0x00027511
		private void Start()
		{
			this.m_Camera.enabled = false;
			this.m_AdjustOrthographicCamera.enabled = false;
		}

		// Token: 0x060006E3 RID: 1763 RVA: 0x0002912C File Offset: 0x0002752C
		private void Update()
		{
			BattleTitleCutIn.eStep step = this.m_Step;
			if (step != BattleTitleCutIn.eStep.Play)
			{
				if (step == BattleTitleCutIn.eStep.Play_Wait)
				{
					if (!(this.m_EffectHndl != null) || !this.m_EffectHndl.IsPlaying())
					{
						this.m_Camera.enabled = false;
						this.m_AdjustOrthographicCamera.enabled = false;
						this.Destroy();
						this.SetStep(BattleTitleCutIn.eStep.None);
					}
				}
			}
			else
			{
				this.m_EffectHndl = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged(this.EFFECT_IDS[(int)this.m_Type], this.m_Offsets[(int)this.m_Type], Quaternion.Euler(0f, 180f, 0f), base.gameObject.transform, true, WrapMode.ClampForever, 0, 1f, true);
				if (this.m_EffectHndl != null)
				{
					this.m_Camera.enabled = true;
					this.m_AdjustOrthographicCamera.enabled = true;
					int layerNo = LayerMask.NameToLayer("Graphic2D");
					this.m_EffectHndl.SetLayer(layerNo, true);
					this.m_EffectHndl.SetScale(this.m_Scales[(int)this.m_Type]);
					this.SetStep(BattleTitleCutIn.eStep.Play_Wait);
				}
				else
				{
					this.SetStep(BattleTitleCutIn.eStep.None);
				}
			}
		}

		// Token: 0x060006E4 RID: 1764 RVA: 0x00029273 File Offset: 0x00027673
		public void Play(BattleTitleCutIn.eType type)
		{
			this.m_Type = type;
			this.m_Camera.enabled = false;
			this.SetStep(BattleTitleCutIn.eStep.Play);
		}

		// Token: 0x060006E5 RID: 1765 RVA: 0x0002928F File Offset: 0x0002768F
		public bool IsCompletePlay()
		{
			return this.m_Step == BattleTitleCutIn.eStep.None;
		}

		// Token: 0x060006E6 RID: 1766 RVA: 0x0002929A File Offset: 0x0002769A
		public void Destroy()
		{
			if (this.m_EffectHndl != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_EffectHndl);
				this.m_EffectHndl = null;
			}
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x000292C9 File Offset: 0x000276C9
		private void SetStep(BattleTitleCutIn.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x040005F0 RID: 1520
		private BattleTitleCutIn.eType m_Type;

		// Token: 0x040005F1 RID: 1521
		private readonly string[] EFFECT_IDS = new string[]
		{
			"ef_btl_quest_start",
			"ef_btl_quest_clear",
			"ef_btl_quest_failed"
		};

		// Token: 0x040005F2 RID: 1522
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x040005F3 RID: 1523
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040005F4 RID: 1524
		[SerializeField]
		private AdjustOrthographicCamera m_AdjustOrthographicCamera;

		// Token: 0x040005F5 RID: 1525
		[SerializeField]
		private float[] m_Scales = new float[]
		{
			2.5f,
			2.8f,
			2.8f
		};

		// Token: 0x040005F6 RID: 1526
		[SerializeField]
		private Vector3[] m_Offsets = new Vector3[]
		{
			new Vector3(0f, 0.5f, 0f),
			new Vector3(0f, 0.1f, 0f),
			new Vector3(0f, 0.1f, 0f)
		};

		// Token: 0x040005F7 RID: 1527
		private BattleTitleCutIn.eStep m_Step = BattleTitleCutIn.eStep.None;

		// Token: 0x040005F8 RID: 1528
		private EffectHandler m_EffectHndl;

		// Token: 0x020000F1 RID: 241
		public enum eType
		{
			// Token: 0x040005FA RID: 1530
			Start,
			// Token: 0x040005FB RID: 1531
			Clear,
			// Token: 0x040005FC RID: 1532
			Failed,
			// Token: 0x040005FD RID: 1533
			Num
		}

		// Token: 0x020000F2 RID: 242
		private enum eStep
		{
			// Token: 0x040005FF RID: 1535
			None = -1,
			// Token: 0x04000600 RID: 1536
			Play,
			// Token: 0x04000601 RID: 1537
			Play_Wait
		}
	}
}
