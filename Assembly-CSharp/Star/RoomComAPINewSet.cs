﻿using System;
using RoomResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200058D RID: 1421
	public class RoomComAPINewSet : INetComHandle
	{
		// Token: 0x06001BAB RID: 7083 RVA: 0x00092931 File Offset: 0x00090D31
		public RoomComAPINewSet()
		{
			this.ApiName = "player/room/set";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x0400228F RID: 8847
		public int floorId;

		// Token: 0x04002290 RID: 8848
		public int groupId;

		// Token: 0x04002291 RID: 8849
		public PlayerRoomArrangement[] arrangeData;
	}
}
