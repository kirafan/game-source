﻿using System;

namespace Star
{
	// Token: 0x020001EB RID: 491
	[Serializable]
	public struct QuestWaveListDB_Param
	{
		// Token: 0x04000BE7 RID: 3047
		public int m_ID;

		// Token: 0x04000BE8 RID: 3048
		public int[] m_QuestEnemyIDs;

		// Token: 0x04000BE9 RID: 3049
		public int[] m_QuestRandomIDs;

		// Token: 0x04000BEA RID: 3050
		public int[] m_QuestEnemyLvs;

		// Token: 0x04000BEB RID: 3051
		public int[] m_DropIDs;
	}
}
