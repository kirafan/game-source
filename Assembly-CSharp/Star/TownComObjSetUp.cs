﻿using System;
using System.Collections.Generic;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000681 RID: 1665
	public class TownComObjSetUp : IFldNetComModule
	{
		// Token: 0x060021A5 RID: 8613 RVA: 0x000B3162 File Offset: 0x000B1562
		public TownComObjSetUp() : base(IFldNetComManager.Instance)
		{
			this.m_Step = TownComObjSetUp.eStep.GetState;
		}

		// Token: 0x060021A6 RID: 8614 RVA: 0x000B3178 File Offset: 0x000B1578
		public bool SetUp()
		{
			TownComAPIObjGetAll phandle = new TownComAPIObjGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x060021A7 RID: 8615 RVA: 0x000B31A8 File Offset: 0x000B15A8
		public bool DefaultObjAdd()
		{
			if (this.m_SendQue != null && this.m_SendQue.Count != 0)
			{
				TownComObjSetUp.DefaultSetUpQue defaultSetUpQue = this.m_SendQue[0];
				defaultSetUpQue.SendUp(this.m_NetMng);
				return true;
			}
			return false;
		}

		// Token: 0x060021A8 RID: 8616 RVA: 0x000B31EC File Offset: 0x000B15EC
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case TownComObjSetUp.eStep.GetState:
				this.SetUp();
				this.m_Step = TownComObjSetUp.eStep.WaitCheck;
				break;
			case TownComObjSetUp.eStep.DefaultObjUp:
				if (this.DefaultObjAdd())
				{
					this.m_Step = TownComObjSetUp.eStep.SetUpCheck;
				}
				else
				{
					this.m_Step = TownComObjSetUp.eStep.End;
				}
				break;
			case TownComObjSetUp.eStep.SetUpCheck:
				if (this.m_SendQue[0].m_ListUp)
				{
					this.m_SendQue.RemoveAt(0);
					if (this.DefaultObjAdd())
					{
						this.m_Step = TownComObjSetUp.eStep.SetUpCheck;
					}
					else
					{
						this.m_Step = TownComObjSetUp.eStep.End;
					}
				}
				break;
			case TownComObjSetUp.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x060021A9 RID: 8617 RVA: 0x000B32AC File Offset: 0x000B16AC
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				WWWComUtil.SetTownObjectListKey(getAll.managedTownFacilities, true);
				if (this.CheckDefaultSetUpData(getAll))
				{
					this.m_Step = TownComObjSetUp.eStep.End;
				}
				else
				{
					this.m_Step = TownComObjSetUp.eStep.DefaultObjUp;
				}
			}
		}

		// Token: 0x060021AA RID: 8618 RVA: 0x000B32F8 File Offset: 0x000B16F8
		private bool CheckDefaultSetUpData(GetAll pres)
		{
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			bool result = true;
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 0 && db.m_Params[i].m_WakeUp == 0)
				{
					int num = 1;
					for (int j = 0; j < pres.managedTownFacilities.Length; j++)
					{
						if (db.m_Params[i].m_ObjectNo == pres.managedTownFacilities[j].facilityId)
						{
							num = 0;
							break;
						}
					}
					if (num != 0)
					{
						if (this.m_SendQue == null)
						{
							this.m_SendQue = new List<TownComObjSetUp.DefaultSetUpQue>();
						}
						if (db.m_Params[i].m_Type == 0)
						{
							this.m_SendQue.Add(new TownComObjSetUp.DefaultSetUpQue(db.m_Params[i].m_ObjectNo, false));
						}
						else
						{
							this.m_SendQue.Add(new TownComObjSetUp.DefaultSetUpQue(db.m_Params[i].m_ObjectNo, true));
						}
						result = false;
					}
				}
			}
			return result;
		}

		// Token: 0x0400280A RID: 10250
		private TownComObjSetUp.eStep m_Step;

		// Token: 0x0400280B RID: 10251
		private List<TownComObjSetUp.DefaultSetUpQue> m_SendQue;

		// Token: 0x02000682 RID: 1666
		public enum eStep
		{
			// Token: 0x0400280D RID: 10253
			GetState,
			// Token: 0x0400280E RID: 10254
			WaitCheck,
			// Token: 0x0400280F RID: 10255
			DefaultObjUp,
			// Token: 0x04002810 RID: 10256
			SetUpCheck,
			// Token: 0x04002811 RID: 10257
			End
		}

		// Token: 0x02000683 RID: 1667
		public class DefaultSetUpQue
		{
			// Token: 0x060021AB RID: 8619 RVA: 0x000B341D File Offset: 0x000B181D
			public DefaultSetUpQue(int fobjid, bool ftype)
			{
				this.m_ObjID = fobjid;
				this.m_Type = ftype;
			}

			// Token: 0x060021AC RID: 8620 RVA: 0x000B3434 File Offset: 0x000B1834
			public void SendUp(IFldNetComManager pmanager)
			{
				this.m_Manager = pmanager;
				pmanager.SendCom(new TownComAPIObjAdd
				{
					facilityId = this.m_ObjID.ToString(),
					amount = "1"
				}, new INetComHandle.ResponseCallbak(this.CallbackDefaultAdd), false);
			}

			// Token: 0x060021AD RID: 8621 RVA: 0x000B3484 File Offset: 0x000B1884
			public void CallbackDefaultAdd(INetComHandle phandle)
			{
				Add add = phandle.GetResponse() as Add;
				if (long.TryParse(add.managedTownFacilityIds, out this.m_ManageID))
				{
					UserTownObjectData userTownObjectData = new UserTownObjectData(this.m_ObjID, this.m_ManageID);
					if (this.m_Type)
					{
						userTownObjectData.BuildPoint = TownUtility.MakeMenuPointID(this.m_ObjID);
					}
					userTownObjectData.OpenState = UserTownObjectData.eOpenState.Open;
					userTownObjectData.Lv = 1;
					UserDataUtil.TownObjData.Add(userTownObjectData);
					TownComAPIObjbuildPointSet townComAPIObjbuildPointSet = new TownComAPIObjbuildPointSet();
					townComAPIObjbuildPointSet.managedTownFacilityId = this.m_ManageID;
					townComAPIObjbuildPointSet.buildPointIndex = userTownObjectData.BuildPoint;
					townComAPIObjbuildPointSet.openState = (int)userTownObjectData.OpenState;
					this.m_Manager.SendCom(townComAPIObjbuildPointSet, new INetComHandle.ResponseCallbak(this.CallbackDefaultParamUp), false);
				}
				else
				{
					UserTownObjectData.SetConnect(true);
					this.m_ListUp = true;
				}
			}

			// Token: 0x060021AE RID: 8622 RVA: 0x000B3550 File Offset: 0x000B1950
			public void CallbackDefaultParamUp(INetComHandle phandle)
			{
				UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_ManageID);
				TownComAPIObjLeveUp townComAPIObjLeveUp = new TownComAPIObjLeveUp();
				townComAPIObjLeveUp.managedTownFacilityId = this.m_ManageID;
				townComAPIObjLeveUp.nextLevel = townObjData.Lv;
				townComAPIObjLeveUp.openState = (int)townObjData.OpenState;
				this.m_Manager.SendCom(townComAPIObjLeveUp, new INetComHandle.ResponseCallbak(this.CallbackDefaultLevelUp), false);
			}

			// Token: 0x060021AF RID: 8623 RVA: 0x000B35AC File Offset: 0x000B19AC
			public void CallbackDefaultLevelUp(INetComHandle phandle)
			{
				this.m_ListUp = true;
			}

			// Token: 0x04002812 RID: 10258
			public long m_ManageID;

			// Token: 0x04002813 RID: 10259
			public int m_ObjID;

			// Token: 0x04002814 RID: 10260
			public bool m_ListUp;

			// Token: 0x04002815 RID: 10261
			public bool m_Type;

			// Token: 0x04002816 RID: 10262
			private IFldNetComManager m_Manager;
		}
	}
}
