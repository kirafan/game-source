﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001E1 RID: 481
	public class ProfileVoiceListDB : ScriptableObject
	{
		// Token: 0x04000B9F RID: 2975
		public ProfileVoiceListDB_Param[] m_Params;
	}
}
