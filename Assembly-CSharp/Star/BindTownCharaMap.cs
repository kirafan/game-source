﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006A1 RID: 1697
	public class BindTownCharaMap : ITownEventCommand
	{
		// Token: 0x06002207 RID: 8711 RVA: 0x000B50B4 File Offset: 0x000B34B4
		public BindTownCharaMap(long fmngid, TownBuilder pbuiler)
		{
			this.m_ManageID = fmngid;
			this.m_Builder = pbuiler;
			this.m_ViewAct = true;
		}

		// Token: 0x06002208 RID: 8712 RVA: 0x000B50D1 File Offset: 0x000B34D1
		public void SetLinkBindKey(Transform parent, int flayerid, float foffset)
		{
			this.m_BasePos = parent.position;
			this.m_LayerID = flayerid;
			this.m_OffsetPos = foffset;
		}

		// Token: 0x06002209 RID: 8713 RVA: 0x000B50ED File Offset: 0x000B34ED
		public void ChangeManageID(long fmngid)
		{
			this.m_ManageID = fmngid;
			this.m_ViewAct = true;
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x000B5100 File Offset: 0x000B3500
		public void CalcCharaPopIconPos(long fmanageid, int flayerid, float foffset)
		{
			TownHandleActionMovePos townHandleActionMovePos = new TownHandleActionMovePos(fmanageid, flayerid);
			townHandleActionMovePos.m_MovePos = this.m_BasePos;
			TownHandleActionMovePos townHandleActionMovePos2 = townHandleActionMovePos;
			townHandleActionMovePos2.m_MovePos.y = townHandleActionMovePos2.m_MovePos.y + foffset;
			TownHandleActionMovePos townHandleActionMovePos3 = townHandleActionMovePos;
			townHandleActionMovePos3.m_MovePos.x = townHandleActionMovePos3.m_MovePos.x + (float)(this.m_BindObjNum - 1) * 0.75f * 0.5f;
			TownHandleActionMovePos townHandleActionMovePos4 = townHandleActionMovePos;
			townHandleActionMovePos4.m_MovePos.z = townHandleActionMovePos4.m_MovePos.z - (float)(flayerid / 10 + 1);
			for (int i = 0; i < this.m_BindObjNum; i++)
			{
				this.m_BindObject[i].RecvEvent(townHandleActionMovePos);
				TownHandleActionMovePos townHandleActionMovePos5 = townHandleActionMovePos;
				townHandleActionMovePos5.m_MovePos.x = townHandleActionMovePos5.m_MovePos.x - 0.75f;
			}
		}

		// Token: 0x0600220B RID: 8715 RVA: 0x000B51B4 File Offset: 0x000B35B4
		public void SetViewActive(bool factive)
		{
			this.m_ViewAct = factive;
			for (int i = 0; i < this.m_BindObjNum; i++)
			{
				if (this.m_BindObject[i] != null)
				{
					this.m_BindObject[i].SetEnableRender(factive);
				}
			}
		}

		// Token: 0x0600220C RID: 8716 RVA: 0x000B5200 File Offset: 0x000B3600
		public void BindGameObject(ITownObjectHandler pobj)
		{
			bool flag = false;
			for (int i = 0; i < this.m_BindObjNum; i++)
			{
				if (this.m_BindObject[i] == pobj)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				if (this.m_BindObjNum >= this.m_BindObjMax)
				{
					ITownObjectHandler[] array = new ITownObjectHandler[this.m_BindObjMax + 2];
					for (int i = 0; i < this.m_BindObjMax; i++)
					{
						array[i] = this.m_BindObject[i];
					}
					this.m_BindObject = null;
					this.m_BindObject = array;
					this.m_BindObjMax += 2;
				}
				pobj.SetEnableRender(this.m_ViewAct);
				this.m_BindObject[this.m_BindObjNum] = pobj;
				this.m_BindObjNum++;
				if (!this.m_Enable)
				{
					this.m_Builder.EventRequest(this);
					this.m_Enable = true;
				}
			}
		}

		// Token: 0x0600220D RID: 8717 RVA: 0x000B52EC File Offset: 0x000B36EC
		public void ReBindGameObject(ITownObjectHandler pobj)
		{
			bool flag = false;
			for (int i = 0; i < this.m_BindObjNum; i++)
			{
				if (this.m_BindObject[i] == pobj)
				{
					this.m_BindObject[i] = null;
					flag = true;
					break;
				}
			}
			if (flag)
			{
				int i = 0;
				while (i < this.m_BindObjNum)
				{
					if (this.m_BindObject[i] == null)
					{
						for (int j = i + 1; j < this.m_BindObjNum; j++)
						{
							this.m_BindObject[j - 1] = this.m_BindObject[j];
						}
						this.m_BindObjNum--;
						this.m_BindObject[this.m_BindObjNum] = null;
					}
					else
					{
						i++;
					}
				}
				if (!this.m_Enable)
				{
					this.m_Builder.EventRequest(this);
					this.m_Enable = true;
				}
			}
		}

		// Token: 0x0600220E RID: 8718 RVA: 0x000B53D0 File Offset: 0x000B37D0
		public int BackLinkCut(TownBuilder pbuilder, long fmanageid, int flayerid, int fsynckey = -1)
		{
			int num = -1;
			if (this.m_BindObjNum != 0)
			{
				num = pbuilder.CreateSyncKey(fsynckey);
				TownEventActBackCharaBind townEventActBackCharaBind = new TownEventActBackCharaBind(fmanageid, num);
				TownHandleActionLinkCut pact = new TownHandleActionLinkCut(null, fmanageid, flayerid);
				for (int i = 0; i < this.m_BindObjNum; i++)
				{
					this.m_BindObject[i].RecvEvent(pact);
					townEventActBackCharaBind.AddChara(this.m_BindObject[i]);
					this.m_BindObject[i] = null;
				}
				pbuilder.EventRequest(townEventActBackCharaBind);
				this.m_BindObjNum = 0;
			}
			return num;
		}

		// Token: 0x0600220F RID: 8719 RVA: 0x000B5450 File Offset: 0x000B3850
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			this.CalcCharaPopIconPos(this.m_ManageID, this.m_LayerID, this.m_OffsetPos);
			this.m_Enable = false;
			return false;
		}

		// Token: 0x04002879 RID: 10361
		private ITownObjectHandler[] m_BindObject;

		// Token: 0x0400287A RID: 10362
		private int m_BindObjNum;

		// Token: 0x0400287B RID: 10363
		private int m_BindObjMax;

		// Token: 0x0400287C RID: 10364
		private bool m_ViewAct;

		// Token: 0x0400287D RID: 10365
		private TownBuilder m_Builder;

		// Token: 0x0400287E RID: 10366
		private Vector3 m_BasePos;

		// Token: 0x0400287F RID: 10367
		private int m_LayerID;

		// Token: 0x04002880 RID: 10368
		private float m_OffsetPos;

		// Token: 0x04002881 RID: 10369
		public long m_ManageID;
	}
}
