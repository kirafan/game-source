﻿using System;

namespace Star
{
	// Token: 0x02000555 RID: 1365
	public class CActScriptKeyRotAnime : IRoomScriptData
	{
		// Token: 0x06001AD9 RID: 6873 RVA: 0x0008F050 File Offset: 0x0008D450
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyRotAnime actXlsKeyRotAnime = (ActXlsKeyRotAnime)pbase;
			this.m_TargetName = actXlsKeyRotAnime.m_TargetName;
			this.m_Rot = actXlsKeyRotAnime.m_Rot;
			this.m_Frame = (float)actXlsKeyRotAnime.m_Times / 30f;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x040021AF RID: 8623
		public string m_TargetName;

		// Token: 0x040021B0 RID: 8624
		public uint m_CRCKey;

		// Token: 0x040021B1 RID: 8625
		public float m_Rot;

		// Token: 0x040021B2 RID: 8626
		public float m_Frame;
	}
}
