﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020005B0 RID: 1456
	public class RoomObjDebugForm : DebugWindowForm
	{
		// Token: 0x06001C1E RID: 7198 RVA: 0x000954F4 File Offset: 0x000938F4
		public void Awake()
		{
			this.m_Form = base.transform;
			GameObject gameObject = GameObject.Find("RoomBuilder");
			if (gameObject != null)
			{
				this.m_Builder = gameObject.GetComponent<RoomBuilder>();
			}
			Button button = DebugWindowBase.FindHrcObject(base.transform, "ObjSelect", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackNextObj));
			}
			this.m_DebugView = (DebugWindowBase.FindHrcObject(base.transform, "CharaHitView", typeof(Toggle)) as Toggle);
			if (this.m_DebugView != null)
			{
				this.m_DebugView.onValueChanged.AddListener(new UnityAction<bool>(this.CallbackViewChange));
			}
			this.m_ObjectName = (DebugWindowBase.FindHrcObject(base.transform, "ObjInfo/Names", typeof(Text)) as Text);
			this.m_ObjectIDNum = (DebugWindowBase.FindHrcObject(base.transform, "ObjNumber/Names", typeof(Text)) as Text);
			this.m_ObjectXSize = (DebugWindowBase.FindHrcObject(base.transform, "ObjSize/XSize/Size", typeof(Text)) as Text);
			this.m_ObjectYSize = (DebugWindowBase.FindHrcObject(base.transform, "ObjSize/YSize/Size", typeof(Text)) as Text);
			this.m_SelectNo = -1;
			if (this.m_Builder != null)
			{
				IRoomFloorManager floorManager = this.m_Builder.GetFloorManager(0);
				if (floorManager.GetObjectListNum() != 0)
				{
					this.m_SelectNo = 0;
				}
				this.SetUpObjectView();
			}
		}

		// Token: 0x06001C1F RID: 7199 RVA: 0x00095698 File Offset: 0x00093A98
		public void CallbackNextObj()
		{
			if (this.m_Builder != null)
			{
				IRoomFloorManager floorManager = this.m_Builder.GetFloorManager(0);
				if (floorManager.GetObjectListNum() != 0)
				{
					this.m_SelectNo++;
					if (this.m_SelectNo >= floorManager.GetObjectListNum())
					{
						this.m_SelectNo = 0;
					}
					IRoomObjectControll indexToHandle = floorManager.GetIndexToHandle(this.m_SelectNo);
					if (indexToHandle != null && this.m_DebugView != null)
					{
						RoomObjectModel roomObjectModel = indexToHandle as RoomObjectModel;
						if (roomObjectModel != null)
						{
							this.m_DebugView.isOn = roomObjectModel.m_CharaHitView;
						}
					}
				}
				else
				{
					this.m_SelectNo = -1;
				}
				this.SetUpObjectView();
			}
		}

		// Token: 0x06001C20 RID: 7200 RVA: 0x00095754 File Offset: 0x00093B54
		public void CallbackViewChange(bool factive)
		{
			if (this.m_Builder != null && this.m_SelectNo >= 0)
			{
				IRoomFloorManager floorManager = this.m_Builder.GetFloorManager(0);
				if (this.m_SelectNo >= floorManager.GetObjectListNum())
				{
					this.m_SelectNo = 0;
				}
				IRoomObjectControll indexToHandle = floorManager.GetIndexToHandle(this.m_SelectNo);
				if (indexToHandle != null)
				{
					RoomObjectModel roomObjectModel = indexToHandle as RoomObjectModel;
					if (roomObjectModel != null)
					{
						roomObjectModel.DrawCharaHitView(factive);
					}
				}
			}
		}

		// Token: 0x06001C21 RID: 7201 RVA: 0x000957D6 File Offset: 0x00093BD6
		private void Update()
		{
			this.SetUpObjectView();
		}

		// Token: 0x06001C22 RID: 7202 RVA: 0x000957E0 File Offset: 0x00093BE0
		private void SetUpObjectView()
		{
			if (this.m_Builder != null && this.m_SelectNo >= 0)
			{
				IRoomFloorManager floorManager = this.m_Builder.GetFloorManager(0);
				if (this.m_SelectNo >= floorManager.GetObjectListNum())
				{
					this.m_SelectNo = 0;
				}
				IRoomObjectControll indexToHandle = floorManager.GetIndexToHandle(this.m_SelectNo);
				if (indexToHandle != null)
				{
					if (this.m_ObjectName != null)
					{
						this.m_ObjectName.text = indexToHandle.ObjCategory.ToString();
					}
					if (this.m_ObjectIDNum != null)
					{
						this.m_ObjectIDNum.text = indexToHandle.ObjID.ToString();
					}
					if (this.m_ObjectXSize != null)
					{
						this.m_ObjectXSize.text = indexToHandle.BlockData.SizeX.ToString();
					}
					if (this.m_ObjectYSize != null)
					{
						this.m_ObjectYSize.text = indexToHandle.BlockData.SizeY.ToString();
					}
				}
			}
		}

		// Token: 0x06001C23 RID: 7203 RVA: 0x00095914 File Offset: 0x00093D14
		public override void SetActive(bool factive)
		{
			this.m_Form.gameObject.SetActive(factive);
		}

		// Token: 0x04002313 RID: 8979
		private RoomBuilder m_Builder;

		// Token: 0x04002314 RID: 8980
		private int m_SelectNo;

		// Token: 0x04002315 RID: 8981
		private Toggle m_DebugView;

		// Token: 0x04002316 RID: 8982
		private Text m_ObjectName;

		// Token: 0x04002317 RID: 8983
		private Text m_ObjectIDNum;

		// Token: 0x04002318 RID: 8984
		private Text m_ObjectXSize;

		// Token: 0x04002319 RID: 8985
		private Text m_ObjectYSize;
	}
}
