﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020006C0 RID: 1728
	public class TownPartsFrameEvent : ITownPartsAction
	{
		// Token: 0x06002257 RID: 8791 RVA: 0x000B6A36 File Offset: 0x000B4E36
		public TownPartsFrameEvent()
		{
			this.m_Active = true;
			this.m_Que = new List<TownPartsFrameEvent.StackQue>();
		}

		// Token: 0x06002258 RID: 8792 RVA: 0x000B6A50 File Offset: 0x000B4E50
		public void Stack(int frame, TownPartsFrameEvent.CallEvent pcallback, int fkeyid)
		{
			TownPartsFrameEvent.StackQue stackQue = new TownPartsFrameEvent.StackQue();
			stackQue.m_Time = (float)frame / 30f;
			stackQue.m_Callback = pcallback;
			stackQue.m_KeyCode = fkeyid;
			this.m_Que.Add(stackQue);
		}

		// Token: 0x06002259 RID: 8793 RVA: 0x000B6A8C File Offset: 0x000B4E8C
		public void Play()
		{
			int num = this.m_PlayMax = this.m_Que.Count;
			for (int i = 0; i < num; i++)
			{
				for (int j = 1; j < num - i; j++)
				{
					if (this.m_Que[j - 1].m_Time > this.m_Que[j].m_Time)
					{
						TownPartsFrameEvent.StackQue value = this.m_Que[j];
						this.m_Que[j] = this.m_Que[j - 1];
						this.m_Que[j - 1] = value;
					}
				}
			}
		}

		// Token: 0x0600225A RID: 8794 RVA: 0x000B6B38 File Offset: 0x000B4F38
		public override bool PartsUpdate()
		{
			bool flag = true;
			bool flag2 = this.m_PlayIndex < this.m_PlayMax;
			float times = this.m_Times;
			while (flag2)
			{
				if (times >= this.m_Que[this.m_PlayIndex].m_Time)
				{
					flag = this.m_Que[this.m_PlayIndex].m_Callback(this.m_Que[this.m_PlayIndex].m_KeyCode);
					if (flag)
					{
						this.m_PlayIndex++;
						if (this.m_PlayIndex >= this.m_PlayMax)
						{
							flag2 = false;
						}
					}
					else
					{
						flag = true;
						flag2 = false;
					}
				}
				else
				{
					flag2 = false;
				}
			}
			if (flag)
			{
				this.m_Times += Time.deltaTime;
			}
			if (this.m_PlayIndex >= this.m_PlayMax)
			{
				this.m_Active = false;
			}
			return this.m_Active;
		}

		// Token: 0x04002909 RID: 10505
		private List<TownPartsFrameEvent.StackQue> m_Que;

		// Token: 0x0400290A RID: 10506
		private float m_Times;

		// Token: 0x0400290B RID: 10507
		private int m_PlayIndex;

		// Token: 0x0400290C RID: 10508
		private int m_PlayMax;

		// Token: 0x020006C1 RID: 1729
		// (Invoke) Token: 0x0600225C RID: 8796
		public delegate bool CallEvent(int fkeycode);

		// Token: 0x020006C2 RID: 1730
		public class StackQue
		{
			// Token: 0x0400290D RID: 10509
			public float m_Time;

			// Token: 0x0400290E RID: 10510
			public TownPartsFrameEvent.CallEvent m_Callback;

			// Token: 0x0400290F RID: 10511
			public int m_KeyCode;
		}
	}
}
