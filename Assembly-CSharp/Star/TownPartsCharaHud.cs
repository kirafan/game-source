﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006BF RID: 1727
	public class TownPartsCharaHud : ITownPartsAction
	{
		// Token: 0x06002255 RID: 8789 RVA: 0x000B6974 File Offset: 0x000B4D74
		public TownPartsCharaHud(TownBuilder pbuilder, Transform parent, Vector3 foffset, int fcharaid, int fmassageid)
		{
			this.m_Builder = pbuilder;
			this.m_HudObject = this.m_Builder.GetCharaHudObj();
			this.m_HudObject.transform.SetParent(parent, false);
			this.m_HudObject.transform.localPosition = foffset;
			this.m_Active = true;
			this.m_PartsLink = this.m_HudObject.AddComponent<TownCharaHud>();
			this.m_PartsLink.SetMessageID(fmassageid, fcharaid);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.TOWN_CHARA_ICON_TAP, 1f, 0, -1, -1);
		}

		// Token: 0x06002256 RID: 8790 RVA: 0x000B6A06 File Offset: 0x000B4E06
		public override bool PartsUpdate()
		{
			if (!this.m_PartsLink.IsPlaying())
			{
				this.m_Builder.ReleaseCharaHudObj(this.m_HudObject);
				this.m_Active = false;
			}
			return this.m_Active;
		}

		// Token: 0x04002902 RID: 10498
		private TownBuilder m_Builder;

		// Token: 0x04002903 RID: 10499
		private GameObject m_HudObject;

		// Token: 0x04002904 RID: 10500
		private TownCharaHud m_PartsLink;

		// Token: 0x04002905 RID: 10501
		private AnimationState[] m_AnimeTable;

		// Token: 0x04002906 RID: 10502
		private int m_VoicePlayID;

		// Token: 0x04002907 RID: 10503
		private int m_Step;

		// Token: 0x04002908 RID: 10504
		private float m_TimeUp;
	}
}
