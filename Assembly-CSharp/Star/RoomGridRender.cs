﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200060C RID: 1548
	public class RoomGridRender : MonoBehaviour
	{
		// Token: 0x06001E5E RID: 7774 RVA: 0x000A3B0C File Offset: 0x000A1F0C
		public static GameObject CreateGridTile(int fsizex, int fsizey)
		{
			GameObject gameObject = new GameObject("Grid");
			RoomGridRender roomGridRender = gameObject.AddComponent<RoomGridRender>();
			roomGridRender.CreateModel(fsizex, fsizey);
			return gameObject;
		}

		// Token: 0x06001E5F RID: 7775 RVA: 0x000A3B34 File Offset: 0x000A1F34
		public void CreateModel(int fsizex, int fsizey)
		{
			this.m_GridLine = new Mesh();
			this.m_Material = new Material(Shader.Find("Room/GridLine"));
			Vector3[] array = new Vector3[(fsizex + 1) * (fsizey + 1)];
			Color[] array2 = new Color[(fsizex + 1) * (fsizey + 1)];
			int num = 0;
			for (int i = 0; i <= fsizey; i++)
			{
				for (int j = 0; j <= fsizex; j++)
				{
					array[num] = RoomUtility.GridToTilePos((float)j, (float)i);
					array2[num] = Color.white;
					num++;
				}
			}
			int[] array3 = new int[(fsizex + 1) * 2 * fsizey + (fsizey + 1) * 2 * fsizex];
			num = 0;
			for (int i = 0; i <= fsizey; i++)
			{
				for (int j = 0; j < fsizex; j++)
				{
					array3[num] = j + i * (fsizex + 1);
					array3[num + 1] = j + 1 + i * (fsizex + 1);
					num += 2;
				}
			}
			for (int i = 0; i <= fsizex; i++)
			{
				for (int j = 0; j < fsizey; j++)
				{
					array3[num] = j * (fsizex + 1) + i;
					array3[num + 1] = (j + 1) * (fsizex + 1) + i;
					num += 2;
				}
			}
			this.m_GridLine.vertices = array;
			this.m_GridLine.colors = array2;
			this.m_GridLine.SetIndices(array3, MeshTopology.Lines, 0);
			MeshFilter meshFilter = base.gameObject.AddComponent<MeshFilter>();
			meshFilter.mesh = this.m_GridLine;
			MeshRenderer meshRenderer = base.gameObject.AddComponent<MeshRenderer>();
			meshRenderer.material = this.m_Material;
		}

		// Token: 0x040024EC RID: 9452
		private Mesh m_GridLine;

		// Token: 0x040024ED RID: 9453
		private Material m_Material;

		// Token: 0x040024EE RID: 9454
		private MeshRenderer m_Render;
	}
}
