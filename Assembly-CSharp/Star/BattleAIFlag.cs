﻿using System;

namespace Star
{
	// Token: 0x02000097 RID: 151
	[Serializable]
	public class BattleAIFlag
	{
		// Token: 0x040002B5 RID: 693
		public int m_ID;

		// Token: 0x040002B6 RID: 694
		public bool m_IsFlag;
	}
}
