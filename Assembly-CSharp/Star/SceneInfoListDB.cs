﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001FC RID: 508
	public class SceneInfoListDB : ScriptableObject
	{
		// Token: 0x04000C4D RID: 3149
		public SceneInfoListDB_Param[] m_Params;
	}
}
