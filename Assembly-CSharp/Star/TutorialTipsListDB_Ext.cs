﻿using System;

namespace Star
{
	// Token: 0x020001BA RID: 442
	public static class TutorialTipsListDB_Ext
	{
		// Token: 0x06000B35 RID: 2869 RVA: 0x00042B34 File Offset: 0x00040F34
		public static TutorialTipsListDB_Param GetParam(this TutorialTipsListDB self, eTutorialTipsListDB id)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)id)
				{
					return self.m_Params[i];
				}
			}
			return default(TutorialTipsListDB_Param);
		}
	}
}
