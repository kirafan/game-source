﻿using System;

namespace Star
{
	// Token: 0x02000562 RID: 1378
	public class ActXlsKeyBase
	{
		// Token: 0x06001B00 RID: 6912 RVA: 0x0008F2B8 File Offset: 0x0008D6B8
		public virtual void SerializeCode(BinaryIO pio)
		{
			this.m_Type = (eRoomScriptCommand)pio.Enum(this.m_Type, typeof(eRoomScriptCommand));
			pio.Int(ref this.m_Time);
		}

		// Token: 0x06001B01 RID: 6913 RVA: 0x0008F2E8 File Offset: 0x0008D6E8
		public static ActXlsKeyBase CreateKey(int ftype)
		{
			Type typeFromHandle = typeof(ActXlsKeyWait);
			switch (ftype)
			{
			case 0:
				typeFromHandle = typeof(ActXlsKeyPlayAnim);
				break;
			case 1:
				typeFromHandle = typeof(ActXlsKeyWait);
				break;
			case 2:
				typeFromHandle = typeof(ActXlsKeyAnimeWait);
				break;
			case 3:
				typeFromHandle = typeof(ActXlsKeyBind);
				break;
			case 4:
				typeFromHandle = typeof(ActXlsKeyLinkCut);
				break;
			case 5:
				typeFromHandle = typeof(ActXlsKeyViewMode);
				break;
			case 6:
				typeFromHandle = typeof(ActXlsKeyMove);
				break;
			case 7:
				typeFromHandle = typeof(ActXlsKeyPopUp);
				break;
			case 8:
				typeFromHandle = typeof(ActXlsKeySE);
				break;
			case 9:
				typeFromHandle = typeof(ActXlsKeyJump);
				break;
			case 11:
				typeFromHandle = typeof(ActXlsKeyProgramEvent);
				break;
			case 12:
				typeFromHandle = typeof(ActXlsKeyObjectDir);
				break;
			case 13:
				typeFromHandle = typeof(ActXlsKeyEffect);
				break;
			case 14:
				typeFromHandle = typeof(ActXlsKeyBaseTrs);
				break;
			case 15:
				typeFromHandle = typeof(ActXlsKeyPosAnime);
				break;
			case 16:
				typeFromHandle = typeof(ActXlsKeyRotAnime);
				break;
			case 17:
				typeFromHandle = typeof(ActXlsKeyBindRot);
				break;
			case 18:
				typeFromHandle = typeof(ActXlsKeyFrameChg);
				break;
			case 19:
				typeFromHandle = typeof(ActXlsKeyCharaViewMode);
				break;
			case 20:
				typeFromHandle = typeof(ActXlsKeyObjPlayAnim);
				break;
			case 21:
				typeFromHandle = typeof(ActXlsKeyCharaCfg);
				break;
			case 22:
				typeFromHandle = typeof(ActXlsKeyCueSe);
				break;
			case 23:
				typeFromHandle = typeof(ActXlsKeyObjectCfg);
				break;
			}
			ActXlsKeyBase actXlsKeyBase = (ActXlsKeyBase)Activator.CreateInstance(typeFromHandle);
			actXlsKeyBase.m_Type = (eRoomScriptCommand)ftype;
			return actXlsKeyBase;
		}

		// Token: 0x040021E7 RID: 8679
		public eRoomScriptCommand m_Type;

		// Token: 0x040021E8 RID: 8680
		public int m_Time;
	}
}
