﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006AF RID: 1711
	public class TownMenuActMission : ITownMenuAct
	{
		// Token: 0x06002223 RID: 8739 RVA: 0x000B5607 File Offset: 0x000B3A07
		public void SetUp(TownBuilder pbuild)
		{
			this.m_Builder = pbuild;
		}

		// Token: 0x06002224 RID: 8740 RVA: 0x000B5610 File Offset: 0x000B3A10
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
			if (MissionManager.GetNewListUpNum() != 0)
			{
				if (this.m_MenuIcon == null)
				{
					this.m_MenuIcon = this.m_Builder.GetGeneralObj(eTownObjectType.BUILD_ITEM_POP);
					if (this.m_MenuIcon != null)
					{
						this.m_MenuIcon.transform.localEulerAngles = new Vector3(0f, 180f, 0f);
						this.m_MenuIcon.transform.SetParent(pmenu.CacheTransform, false);
						this.m_IconUp = this.m_MenuIcon.AddComponent<TownPartsPopObject>();
						this.m_IconUp.SetPopObject(this.m_Builder, this.m_MenuIcon, pmenu, Vector3.up * pmenu.m_MakerPos);
						this.m_IconUp.ChangeIcon(2);
						this.m_IconUp.ChangePer(MissionManager.GetNewListUpNum(), false);
					}
				}
			}
			else if (this.m_MenuIcon != null)
			{
				this.m_Builder.ReleaseCharaMarker(this.m_MenuIcon);
				this.m_MenuIcon = null;
				this.m_IconUp = null;
			}
		}

		// Token: 0x040028A5 RID: 10405
		private TownBuilder m_Builder;

		// Token: 0x040028A6 RID: 10406
		private GameObject m_MenuIcon;

		// Token: 0x040028A7 RID: 10407
		private TownPartsPopObject m_IconUp;
	}
}
