﻿using System;
using System.Collections.Generic;
using WWWTypes;

namespace Star
{
	// Token: 0x020000DE RID: 222
	public class BattleResult
	{
		// Token: 0x06000609 RID: 1545 RVA: 0x0001EB03 File Offset: 0x0001CF03
		public BattleResult()
		{
			this.m_Status = BattleResult.eStatus.None;
			this.m_KilledEnemies = new Dictionary<int, int>();
			this.m_DropItems = new List<BattleDropItem>();
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x0001EB28 File Offset: 0x0001CF28
		public void AddKilledEnemy(int enemyID)
		{
			if (this.m_KilledEnemies.ContainsKey(enemyID))
			{
				Dictionary<int, int> killedEnemies;
				(killedEnemies = this.m_KilledEnemies)[enemyID] = killedEnemies[enemyID] + 1;
			}
			else
			{
				this.m_KilledEnemies.Add(enemyID, 1);
			}
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x0001EB74 File Offset: 0x0001CF74
		public void SetupDropItems(List<BattleDropItems> scheduleDropItems, int waveIndex)
		{
			this.m_DropItems.Clear();
			if (waveIndex > 0)
			{
				for (int i = 0; i < waveIndex; i++)
				{
					for (int j = 0; j < scheduleDropItems[i].Items.Length; j++)
					{
						if (scheduleDropItems[i].Items[j].IsDrop())
						{
							this.AddItem(scheduleDropItems[i].Items[j]);
						}
					}
				}
			}
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x0001EBF0 File Offset: 0x0001CFF0
		public void AddItem(BattleDropItem item)
		{
			this.m_DropItems.Add(item);
		}

		// Token: 0x0600060D RID: 1549 RVA: 0x0001EC00 File Offset: 0x0001D000
		public int GetTreasureBoxNum(int tbIndex)
		{
			int num = 0;
			for (int i = 0; i < this.m_DropItems.Count; i++)
			{
				if (tbIndex == this.m_DropItems[i].GetTreasureBoxIndex())
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x040004A8 RID: 1192
		public int m_QuestID;

		// Token: 0x040004A9 RID: 1193
		public BattleResult.eStatus m_Status;

		// Token: 0x040004AA RID: 1194
		public bool m_IsFirstClear;

		// Token: 0x040004AB RID: 1195
		public bool m_IsLoserClear;

		// Token: 0x040004AC RID: 1196
		public QuestDefine.eClearRank m_ClearRank;

		// Token: 0x040004AD RID: 1197
		public int m_ContinueCount;

		// Token: 0x040004AE RID: 1198
		public int m_DeadCount;

		// Token: 0x040004AF RID: 1199
		public int m_InterruptFriendUseNum;

		// Token: 0x040004B0 RID: 1200
		public int m_MasterSkillUseNum;

		// Token: 0x040004B1 RID: 1201
		public Dictionary<int, int> m_KilledEnemies;

		// Token: 0x040004B2 RID: 1202
		public List<BattleDropItem> m_DropItems;

		// Token: 0x040004B3 RID: 1203
		public long m_RewardUserExp;

		// Token: 0x040004B4 RID: 1204
		public long[] m_UserNextExps;

		// Token: 0x040004B5 RID: 1205
		public int m_BeforeUserLv;

		// Token: 0x040004B6 RID: 1206
		public long m_BeforeUserExp;

		// Token: 0x040004B7 RID: 1207
		public int m_AfterUserLv;

		// Token: 0x040004B8 RID: 1208
		public long m_AfterUserExp;

		// Token: 0x040004B9 RID: 1209
		public long m_RewardMasterOrbExp;

		// Token: 0x040004BA RID: 1210
		public long[] m_MasterOrbNextExps;

		// Token: 0x040004BB RID: 1211
		public int m_BeforeMasterOrbLv;

		// Token: 0x040004BC RID: 1212
		public long m_BeforeMasterOrbExp;

		// Token: 0x040004BD RID: 1213
		public int m_AfterMasterOrbLv;

		// Token: 0x040004BE RID: 1214
		public long m_AfterMasterOrbExp;

		// Token: 0x040004BF RID: 1215
		public long m_RewardCharaExp;

		// Token: 0x040004C0 RID: 1216
		public long m_RewardFriendshipExp;

		// Token: 0x040004C1 RID: 1217
		public BattleResult.CharaData[] m_CharaDatas;

		// Token: 0x040004C2 RID: 1218
		public long m_RewardGold;

		// Token: 0x040004C3 RID: 1219
		public long m_BeforeGold;

		// Token: 0x040004C4 RID: 1220
		public long m_AfterGold;

		// Token: 0x040004C5 RID: 1221
		public QuestReward m_RewardFirst;

		// Token: 0x040004C6 RID: 1222
		public QuestReward m_RewardGroup;

		// Token: 0x040004C7 RID: 1223
		public QuestReward m_RewardComp;

		// Token: 0x020000DF RID: 223
		public enum eStatus
		{
			// Token: 0x040004C9 RID: 1225
			None = -1,
			// Token: 0x040004CA RID: 1226
			Retire,
			// Token: 0x040004CB RID: 1227
			GameOver,
			// Token: 0x040004CC RID: 1228
			Clear
		}

		// Token: 0x020000E0 RID: 224
		public struct CharaData
		{
			// Token: 0x040004CD RID: 1229
			public bool m_IsAvailable;

			// Token: 0x040004CE RID: 1230
			public long m_MngID;

			// Token: 0x040004CF RID: 1231
			public long m_AddExp;

			// Token: 0x040004D0 RID: 1232
			public long[] m_NextExps;

			// Token: 0x040004D1 RID: 1233
			public int m_BeforeLv;

			// Token: 0x040004D2 RID: 1234
			public long m_BeforeExp;

			// Token: 0x040004D3 RID: 1235
			public int m_AfterLv;

			// Token: 0x040004D4 RID: 1236
			public long m_AfterExp;

			// Token: 0x040004D5 RID: 1237
			public long m_AddFriendshipExp;

			// Token: 0x040004D6 RID: 1238
			public long[] m_FriendshipNextExps;

			// Token: 0x040004D7 RID: 1239
			public int m_BeforeFriendship;

			// Token: 0x040004D8 RID: 1240
			public long m_BeforeFriendshipExp;

			// Token: 0x040004D9 RID: 1241
			public int m_AfterFriendship;

			// Token: 0x040004DA RID: 1242
			public long m_AfterFriendshipExp;
		}
	}
}
