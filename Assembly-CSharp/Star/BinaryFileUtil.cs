﻿using System;
using System.Runtime.InteropServices;

namespace Star
{
	// Token: 0x02000336 RID: 822
	public class BinaryFileUtil
	{
		// Token: 0x06000FBF RID: 4031 RVA: 0x00054CAC File Offset: 0x000530AC
		public static ushort MakeCipherCode()
		{
			Random random = new Random();
			BinaryFileUtil.TCipherKey tcipherKey = default(BinaryFileUtil.TCipherKey);
			tcipherKey.m_key0 = (byte)(random.Next(253) + 1);
			tcipherKey.m_key1 = (byte)(random.Next(253) + 1);
			return tcipherKey.m_code;
		}

		// Token: 0x06000FC0 RID: 4032 RVA: 0x00054CF8 File Offset: 0x000530F8
		public static ushort DecodeCode(byte[] pchk, int fsize, ushort fcode, int foffset)
		{
			BinaryFileUtil.TCipherKey tcipherKey = default(BinaryFileUtil.TCipherKey);
			BinaryFileUtil.TCipherKey tcipherKey2 = default(BinaryFileUtil.TCipherKey);
			tcipherKey.m_code = fcode;
			int i = 0;
			tcipherKey2.m_code = 0;
			while (i < fsize)
			{
				byte b = pchk[i + foffset];
				byte b2;
				if ((i & 1) != 0)
				{
					b2 = tcipherKey.m_key1;
				}
				else
				{
					b2 = tcipherKey.m_key0;
				}
				if ((i & 2) != 0)
				{
					b -= b2;
				}
				else
				{
					b -= b2 * 2;
				}
				if ((i & 1) != 0)
				{
					tcipherKey2.m_key1 += b;
				}
				else
				{
					tcipherKey2.m_key0 += b;
				}
				pchk[i + foffset] = b;
				i++;
			}
			return tcipherKey2.m_code;
		}

		// Token: 0x06000FC1 RID: 4033 RVA: 0x00054DB8 File Offset: 0x000531B8
		public static ushort CipherCode(byte[] pchk, int fsize, ushort fcode, int foffset)
		{
			BinaryFileUtil.TCipherKey tcipherKey = default(BinaryFileUtil.TCipherKey);
			BinaryFileUtil.TCipherKey tcipherKey2 = default(BinaryFileUtil.TCipherKey);
			tcipherKey.m_code = fcode;
			int i = 0;
			tcipherKey2.m_code = 0;
			while (i < fsize)
			{
				byte b = pchk[i + foffset];
				byte b2;
				if ((i & 1) != 0)
				{
					b2 = tcipherKey.m_key1;
					tcipherKey2.m_key1 += b;
				}
				else
				{
					b2 = tcipherKey.m_key0;
					tcipherKey2.m_key0 += b;
				}
				if ((i & 2) != 0)
				{
					pchk[i + foffset] = b + b2;
				}
				else
				{
					pchk[i + foffset] = b + b2 * 2;
				}
				i++;
			}
			return tcipherKey2.m_code;
		}

		// Token: 0x02000337 RID: 823
		[StructLayout(LayoutKind.Explicit)]
		public struct TCipherKey
		{
			// Token: 0x04001707 RID: 5895
			[FieldOffset(0)]
			public ushort m_code;

			// Token: 0x04001708 RID: 5896
			[FieldOffset(0)]
			public byte m_key0;

			// Token: 0x04001709 RID: 5897
			[FieldOffset(1)]
			public byte m_key1;
		}
	}
}
