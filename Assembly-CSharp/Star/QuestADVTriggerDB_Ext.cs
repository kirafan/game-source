﻿using System;

namespace Star
{
	// Token: 0x0200019D RID: 413
	public static class QuestADVTriggerDB_Ext
	{
		// Token: 0x06000B06 RID: 2822 RVA: 0x00041AD8 File Offset: 0x0003FED8
		public static int FindAdvID(this QuestADVTriggerDB self, eQuestADVTriggerType triggerType, int triggerID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_TriggerType == (int)triggerType && self.m_Params[i].m_TriggerID == triggerID)
				{
					return self.m_Params[i].m_AdvID;
				}
			}
			return -1;
		}
	}
}
