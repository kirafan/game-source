﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020000F8 RID: 248
	public class BattleUniqueSkillBaseObjectHandler : MonoBehaviour
	{
		// Token: 0x17000081 RID: 129
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x0002984A File Offset: 0x00027C4A
		public MsbHandler MsbHndl
		{
			get
			{
				return this.m_MsbHndl;
			}
		}

		// Token: 0x06000702 RID: 1794 RVA: 0x00029854 File Offset: 0x00027C54
		public void Setup(string resourceName, MeigeAnimClipHolder meigeAnimClipHolder, CharacterHandler owner, List<CharacterHandler> tgtCharaHndls, CharacterHandler tgtSingleCharaHndl, List<CharacterHandler> myCharaHndls, CharacterHandler mySingleCharaHndl)
		{
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(resourceName, false);
			this.m_BaseTransform = base.transform.Find(name);
			if (this.m_BaseTransform == null)
			{
				return;
			}
			this.m_Anim = this.m_BaseTransform.gameObject.GetComponent<Animation>();
			this.m_Anim.cullingType = AnimationCullingType.AlwaysAnimate;
			this.m_MeigeAnimCtrl = this.m_BaseTransform.gameObject.AddComponent<MeigeAnimCtrl>();
			this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
			this.m_MeigeAnimCtrl.Open();
			this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, meigeAnimClipHolder);
			this.m_MeigeAnimCtrl.Close();
			this.m_MeigeAnimEvNotify = this.m_BaseTransform.gameObject.GetComponent<MeigeAnimEventNotifier>();
			if (this.m_MeigeAnimEvNotify != null)
			{
				this.m_MeigeAnimEvNotify.AddNotify(new MeigeAnimEventNotifier.OnEvent(this.OnMeigeAnimEvent));
			}
			for (int i = 0; i < 6; i++)
			{
				BattleUniqueSkillBaseObjectHandler.LocatorStatus locatorStatus = new BattleUniqueSkillBaseObjectHandler.LocatorStatus();
				locatorStatus.m_IsOn = false;
				locatorStatus.m_CharaHndl = null;
				this.m_CharaHndlsInLocator.Add((BattleUniqueSkillBaseObjectHandler.eLocatorType)i, locatorStatus);
			}
			this.m_AssociatedLocatorObjs = new List<MsbObjectHandler>[6];
			this.m_AssociatedEmmiters = new List<MeigeParticleEmitter>[6];
			for (int j = 0; j < 6; j++)
			{
				this.m_AssociatedLocatorObjs[j] = new List<MsbObjectHandler>();
				this.m_AssociatedEmmiters[j] = new List<MeigeParticleEmitter>();
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				for (int k = 0; k < msbObjectHandlerNum; k++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(k);
					if (msbObjectHandler != null && msbObjectHandler.m_Name.Contains(this.ASSOCIATED_LOCATOR_MESH_NAMES[j]))
					{
						this.m_AssociatedLocatorObjs[j].Add(msbObjectHandler);
					}
				}
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int l = 0; l < particleEmitterNum; l++)
				{
					MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(l);
					if (particleEmitter != null && particleEmitter.name.Contains(this.ASSOCIATED_LOCATOR_MESH_NAMES[j]))
					{
						this.m_AssociatedEmmiters[j].Add(particleEmitter);
					}
				}
			}
			this.m_RenderCache = new List<Renderer>();
			Transform[] componentsInChildren = base.gameObject.GetComponentsInChildren<Transform>();
			this.m_CharaRenderCache = new List<BattleUniqueSkillBaseObjectHandler.CharaRender>();
			for (int m = 1; m <= 3; m++)
			{
				string value = string.Format("eff_body_{0}_root", m);
				BattleUniqueSkillBaseObjectHandler.CharaRender charaRender = null;
				for (int n = 0; n < componentsInChildren.Length; n++)
				{
					if (componentsInChildren[n].name.Length >= 15)
					{
						string text = componentsInChildren[n].name.Substring(0, 15);
						if (text.Contains(value))
						{
							charaRender = new BattleUniqueSkillBaseObjectHandler.CharaRender();
							charaRender.m_Transform = componentsInChildren[n];
							charaRender.m_RenderCache = new List<Renderer>();
							break;
						}
					}
				}
				this.m_CharaRenderCache.Add(charaRender);
			}
			Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
			if (rendererCache != null)
			{
				foreach (Renderer renderer in rendererCache)
				{
					int num2 = -1;
					for (int num3 = 1; num3 <= 3; num3++)
					{
						if (this.m_CharaRenderCache[num3 - 1] != null)
						{
							if (renderer.name.Length >= 11)
							{
								string value2 = string.Format("eff_head_{0}_", num3);
								string value3 = string.Format("eff_body_{0}_", num3);
								string text2 = renderer.name.Substring(0, 11);
								if (text2.Contains(value2) || text2.Contains(value3))
								{
									num2 = num3 - 1;
									break;
								}
							}
						}
					}
					if (num2 != -1)
					{
						this.m_CharaRenderCache[num2].m_RenderCache.Add(renderer);
					}
					else
					{
						this.m_RenderCache.Add(renderer);
					}
				}
			}
			this.m_Owner = owner;
			this.SetCharaParent(this.m_Owner, this.m_BaseTransform);
			this.m_TgtCharaHndls = tgtCharaHndls;
			for (int num4 = 0; num4 < this.m_TgtCharaHndls.Count; num4++)
			{
				this.SetCharaParent(this.m_TgtCharaHndls[num4], this.m_BaseTransform);
				this.m_TgtCharaHndls[num4].SetEnableRender(false);
			}
			this.m_TgtSingleCharaHndl = tgtSingleCharaHndl;
			this.m_MyCharaHndls = myCharaHndls;
			for (int num5 = 0; num5 < this.m_MyCharaHndls.Count; num5++)
			{
				this.SetCharaParent(this.m_MyCharaHndls[num5], this.m_BaseTransform);
				if (this.m_MyCharaHndls[num5] != this.m_Owner)
				{
					this.m_MyCharaHndls[num5].SetEnableRender(false);
				}
			}
			this.m_MySingleCharaHndl = mySingleCharaHndl;
		}

		// Token: 0x06000703 RID: 1795 RVA: 0x00029D50 File Offset: 0x00028150
		public void Destroy()
		{
			this.DetachAllCharas();
			this.DetachCamera();
			this.m_AssociatedLocatorObjs = null;
			this.m_AssociatedEmmiters = null;
			this.m_CharaHndlsInLocator.Clear();
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			if (this.m_MeigeAnimEvNotify != null)
			{
				this.m_MeigeAnimEvNotify.ClearNotify();
				this.m_MeigeAnimEvNotify = null;
			}
			this.m_MsbHndl = null;
		}

		// Token: 0x06000704 RID: 1796 RVA: 0x00029DBA File Offset: 0x000281BA
		public void Update()
		{
			this.UpdateVisiblity();
		}

		// Token: 0x06000705 RID: 1797 RVA: 0x00029DC2 File Offset: 0x000281C2
		public List<BattleUniqueSkillBaseObjectHandler.CharaRender> GetCharaRenderCache()
		{
			return this.m_CharaRenderCache;
		}

		// Token: 0x06000706 RID: 1798 RVA: 0x00029DCA File Offset: 0x000281CA
		public List<Renderer> GetRenderCache()
		{
			return this.m_RenderCache;
		}

		// Token: 0x06000707 RID: 1799 RVA: 0x00029DD4 File Offset: 0x000281D4
		public void DetachAllCharas()
		{
			if (this.m_Owner != null)
			{
				this.m_Owner.CacheTransform.SetParent(null, false);
				this.m_Owner = null;
			}
			if (this.m_TgtCharaHndls != null)
			{
				for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
				{
					this.m_TgtCharaHndls[i].CacheTransform.SetParent(null, false);
				}
				this.m_TgtCharaHndls = null;
			}
			if (this.m_MyCharaHndls != null)
			{
				for (int j = 0; j < this.m_MyCharaHndls.Count; j++)
				{
					this.m_MyCharaHndls[j].CacheTransform.SetParent(null, false);
				}
				this.m_MyCharaHndls = null;
			}
		}

		// Token: 0x06000708 RID: 1800 RVA: 0x00029E98 File Offset: 0x00028298
		public Transform GetLocator(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			switch (locatorType)
			{
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0:
				return this.m_BaseTransform.Find("loc_TGT_0");
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1:
				return this.m_BaseTransform.Find("loc_TGT_1");
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_2:
				return this.m_BaseTransform.Find("loc_TGT_2");
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_0:
				return this.m_BaseTransform.Find("loc_MY_0");
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_1:
				return this.m_BaseTransform.Find("loc_MY_1");
			case BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_2:
				return this.m_BaseTransform.Find("loc_MY_2");
			default:
				return null;
			}
		}

		// Token: 0x06000709 RID: 1801 RVA: 0x00029F2F File Offset: 0x0002832F
		private void SetLocatorStatus(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType, bool isOn, CharacterHandler charaHndl)
		{
			this.m_CharaHndlsInLocator[locatorType].m_IsOn = isOn;
			this.m_CharaHndlsInLocator[locatorType].m_CharaHndl = charaHndl;
		}

		// Token: 0x0600070A RID: 1802 RVA: 0x00029F55 File Offset: 0x00028355
		private bool IsLocatorStatus(BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType)
		{
			return this.m_CharaHndlsInLocator[locatorType].m_IsOn;
		}

		// Token: 0x0600070B RID: 1803 RVA: 0x00029F68 File Offset: 0x00028368
		public void AttachCamera(Camera camera)
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.AttachCamera(0, camera);
			camera.nearClipPlane = 0.1f;
			camera.farClipPlane = 200f;
		}

		// Token: 0x0600070C RID: 1804 RVA: 0x00029F9F File Offset: 0x0002839F
		public void DetachCamera()
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.DetachCamera();
		}

		// Token: 0x0600070D RID: 1805 RVA: 0x00029FC0 File Offset: 0x000283C0
		public void Play()
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
		}

		// Token: 0x0600070E RID: 1806 RVA: 0x0002A037 File Offset: 0x00028437
		public void Pause()
		{
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x0600070F RID: 1807 RVA: 0x0002A058 File Offset: 0x00028458
		public bool IsPlaying()
		{
			WrapMode wrapMode = this.m_MeigeAnimCtrl.m_WrapMode;
			if (wrapMode != WrapMode.ClampForever)
			{
				if (this.m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
				{
					return true;
				}
			}
			else if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000710 RID: 1808 RVA: 0x0002A0B4 File Offset: 0x000284B4
		private void UpdateVisiblity()
		{
			if (this.m_AssociatedEmmiters != null)
			{
				for (int i = 0; i < this.m_AssociatedEmmiters.Length; i++)
				{
					bool flag = this.IsLocatorStatus((BattleUniqueSkillBaseObjectHandler.eLocatorType)i);
					List<MeigeParticleEmitter> list = this.m_AssociatedEmmiters[i];
					for (int j = 0; j < list.Count; j++)
					{
						if (!flag)
						{
							list[j].Kill();
						}
					}
				}
			}
			if (this.m_AssociatedLocatorObjs != null)
			{
				for (int k = 0; k < this.m_AssociatedLocatorObjs.Length; k++)
				{
					bool flag2 = this.IsLocatorStatus((BattleUniqueSkillBaseObjectHandler.eLocatorType)k);
					List<MsbObjectHandler> list2 = this.m_AssociatedLocatorObjs[k];
					for (int l = 0; l < list2.Count; l++)
					{
						list2[l].GetWork().m_bVisibility = (list2[l].GetWork().m_bVisibility && flag2);
					}
				}
			}
		}

		// Token: 0x06000711 RID: 1809 RVA: 0x0002A1B0 File Offset: 0x000285B0
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
			BattleUniqueSkillBaseObjectHandler.eAnimEvent eAnimEvent = (BattleUniqueSkillBaseObjectHandler.eAnimEvent)ev.m_iParam[0];
			switch (eAnimEvent)
			{
			case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_TgtSingle_Change:
				this.OnMeigeAnimEvent_MeshColor_TgtSingle_Change(ev.m_iParam[1], ev.m_iParam[2]);
				break;
			case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_TgtSingle_Revert:
				this.OnMeigeAnimEvent_MeshColor_TgtSingle_Revert(ev.m_iParam[1]);
				break;
			case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_TgtAll_Change:
				this.OnMeigeAnimEvent_MeshColor_TgtAll_Change(ev.m_iParam[1], ev.m_iParam[2]);
				break;
			case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_TgtAll_Revert:
				this.OnMeigeAnimEvent_MeshColor_TgtAll_Revert(ev.m_iParam[1]);
				break;
			default:
				switch (eAnimEvent)
				{
				case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_MySingle_Change:
					this.OnMeigeAnimEvent_MeshColor_MySingle_Change(ev.m_iParam[1], ev.m_iParam[2]);
					break;
				case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_MySingle_Revert:
					this.OnMeigeAnimEvent_MeshColor_MySingle_Revert(ev.m_iParam[1]);
					break;
				case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_MyAll_Change:
					this.OnMeigeAnimEvent_MeshColor_MyAll_Change(ev.m_iParam[1], ev.m_iParam[2]);
					break;
				case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_MyAll_Revert:
					this.OnMeigeAnimEvent_MeshColor_MyAll_Revert(ev.m_iParam[1]);
					break;
				default:
					switch (eAnimEvent)
					{
					case BattleUniqueSkillBaseObjectHandler.eAnimEvent.TgtSingle_OnOff:
						this.OnMeigeAnimEvent_TgtSingle_OnOff(ev.m_iParam[1]);
						break;
					case BattleUniqueSkillBaseObjectHandler.eAnimEvent.TgtAll_OnOff:
						this.OnMeigeAnimEvent_TgtAll_OnOff(ev.m_iParam[1]);
						break;
					case BattleUniqueSkillBaseObjectHandler.eAnimEvent.Tgt_OnOff:
						this.OnMeigeAnimEvent_Tgt_OnOff(ev.m_iParam[1], ev.m_iParam[2]);
						break;
					default:
						switch (eAnimEvent)
						{
						case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MySingle_OnOff:
							this.OnMeigeAnimEvent_MySingle_OnOff(ev.m_iParam[1], ev.m_iParam[2]);
							break;
						case BattleUniqueSkillBaseObjectHandler.eAnimEvent.MyAll_OnOff:
							this.OnMeigeAnimEvent_MyAll_OnOff(ev.m_iParam[1], ev.m_iParam[2]);
							break;
						case BattleUniqueSkillBaseObjectHandler.eAnimEvent.My_OnOff:
							this.OnMeigeAnimEvent_My_OnOff(ev.m_iParam[1], ev.m_iParam[2]);
							break;
						default:
							switch (eAnimEvent)
							{
							case BattleUniqueSkillBaseObjectHandler.eAnimEvent.ShadowVisible_Owner:
								this.OnMeigeAnimEvent_ShadowVisible_Owner(ev.m_iParam[1]);
								break;
							case BattleUniqueSkillBaseObjectHandler.eAnimEvent.ShadowVisible_TgtAll:
								this.OnMeigeAnimEvent_ShadowVisible_TgtAll(ev.m_iParam[1]);
								break;
							case BattleUniqueSkillBaseObjectHandler.eAnimEvent.ShadowVisible_MyAll:
								this.OnMeigeAnimEvent_ShadowVisible_MyAll(ev.m_iParam[1]);
								break;
							default:
								if (eAnimEvent != BattleUniqueSkillBaseObjectHandler.eAnimEvent.FadeIn)
								{
									if (eAnimEvent != BattleUniqueSkillBaseObjectHandler.eAnimEvent.FadeOut)
									{
										if (eAnimEvent != BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_Self_Change)
										{
											if (eAnimEvent != BattleUniqueSkillBaseObjectHandler.eAnimEvent.MeshColor_Self_Revert)
											{
												if (eAnimEvent != BattleUniqueSkillBaseObjectHandler.eAnimEvent.DamageAnim)
												{
													if (eAnimEvent == BattleUniqueSkillBaseObjectHandler.eAnimEvent.WeaponVisible)
													{
														this.OnMeigeAnimEvent_WeaponVisible(ev.m_iParam[1], ev.m_iParam[2], ev.m_iParam[3]);
													}
												}
												else
												{
													this.OnMeigeAnimEvent_DamageAnim(ev.m_iParam[1]);
												}
											}
											else
											{
												this.OnMeigeAnimEvent_MeshColor_Self_Revert(ev.m_iParam[1]);
											}
										}
										else
										{
											this.OnMeigeAnimEvent_MeshColor_Self_Change(ev.m_iParam[1], ev.m_iParam[2]);
										}
									}
									else
									{
										this.OnMeigeAnimEvent_FadeOut(ev.m_iParam[1], ev.m_iParam[2]);
									}
								}
								else
								{
									this.OnMeigeAnimEvent_FadeIn(ev.m_iParam[1], ev.m_iParam[2]);
								}
								break;
							}
							break;
						}
						break;
					}
					break;
				}
				break;
			}
		}

		// Token: 0x06000712 RID: 1810 RVA: 0x0002A4A4 File Offset: 0x000288A4
		private void OnMeigeAnimEvent_FadeIn(int miliSec, int colorType)
		{
			float time = (float)miliSec / 1000f;
			Color color;
			if (colorType != 0)
			{
				if (colorType != 1)
				{
					color = Color.white;
				}
				else
				{
					color = Color.white;
				}
			}
			else
			{
				color = Color.black;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(color, time, null);
		}

		// Token: 0x06000713 RID: 1811 RVA: 0x0002A500 File Offset: 0x00028900
		private void OnMeigeAnimEvent_FadeOut(int miliSec, int colorType)
		{
			float time = (float)miliSec / 1000f;
			Color color;
			if (colorType != 0)
			{
				if (colorType != 1)
				{
					color = Color.white;
				}
				else
				{
					color = Color.white;
				}
			}
			else
			{
				color = Color.black;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(color, time, null);
		}

		// Token: 0x06000714 RID: 1812 RVA: 0x0002A55C File Offset: 0x0002895C
		private void OnMeigeAnimEvent_TgtSingle_OnOff(int onOff)
		{
			if (this.m_TgtSingleCharaHndl == null)
			{
				return;
			}
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				this.m_TgtCharaHndls[i].SetEnableRender(false);
			}
			bool flag = onOff != 0;
			CharacterHandler tgtSingleCharaHndl = this.m_TgtSingleCharaHndl;
			BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0;
			if (flag)
			{
				Transform locator = this.GetLocator(eLocatorType);
				this.SetCharaParent(tgtSingleCharaHndl, locator);
				this.SetLocatorStatus(eLocatorType, true, tgtSingleCharaHndl);
				tgtSingleCharaHndl.SetEnableRender(true);
				tgtSingleCharaHndl.CharaBattle.RequestIdleMode(0.3f, 0f, false);
			}
			else
			{
				this.SetCharaParent(tgtSingleCharaHndl, this.m_BaseTransform);
				if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == tgtSingleCharaHndl)
				{
					this.SetLocatorStatus(eLocatorType, false, null);
				}
				tgtSingleCharaHndl.SetEnableRender(false);
			}
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x0002A63C File Offset: 0x00028A3C
		private void OnMeigeAnimEvent_TgtAll_OnOff(int onOff)
		{
			bool flag = onOff != 0;
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_TgtCharaHndls[i];
				BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.None;
				BattleDefine.eJoinMember join = characterHandler.CharaBattle.Join;
				if (join != BattleDefine.eJoinMember.Join_1)
				{
					if (join != BattleDefine.eJoinMember.Join_2)
					{
						if (join == BattleDefine.eJoinMember.Join_3)
						{
							eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_2;
						}
					}
					else
					{
						eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1;
					}
				}
				else
				{
					eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0;
				}
				Transform locator = this.GetLocator(eLocatorType);
				if (!(locator == null))
				{
					this.SetCharaParent(characterHandler, locator);
					if (flag)
					{
						characterHandler.SetEnableRender(true);
						characterHandler.CharaBattle.RequestIdleMode(0.3f, 0f, false);
						this.SetLocatorStatus(eLocatorType, true, characterHandler);
					}
					else
					{
						characterHandler.SetEnableRender(false);
						if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == characterHandler)
						{
							this.SetLocatorStatus(eLocatorType, false, null);
						}
					}
				}
			}
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x0002A744 File Offset: 0x00028B44
		private void OnMeigeAnimEvent_Tgt_OnOff(int targetLocator, int onOff)
		{
			BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.None;
			if (targetLocator != 0)
			{
				if (targetLocator != 1)
				{
					if (targetLocator == 2)
					{
						eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_2;
					}
				}
				else
				{
					eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1;
				}
			}
			else
			{
				eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0;
			}
			Transform locator = this.GetLocator(eLocatorType);
			if (locator == null)
			{
				return;
			}
			CharacterHandler characterHandler = null;
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				if (this.m_TgtCharaHndls[i].CharaBattle.Join == (BattleDefine.eJoinMember)targetLocator)
				{
					characterHandler = this.m_TgtCharaHndls[i];
					break;
				}
			}
			if (characterHandler == null)
			{
				return;
			}
			this.SetCharaParent(characterHandler, locator);
			bool flag = onOff != 0;
			if (flag)
			{
				characterHandler.SetEnableRender(true);
				characterHandler.CharaBattle.RequestIdleMode(0.3f, 0f, false);
				this.SetLocatorStatus(eLocatorType, true, characterHandler);
			}
			else
			{
				characterHandler.SetEnableRender(false);
				if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == characterHandler)
				{
					this.SetLocatorStatus(eLocatorType, false, null);
				}
			}
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x0002A864 File Offset: 0x00028C64
		private void OnMeigeAnimEvent_MySingle_OnOff(int onOff, int fixOwnerRootBoneTransform)
		{
			if (this.m_MySingleCharaHndl == null)
			{
				return;
			}
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				this.m_MyCharaHndls[i].SetEnableRender(false);
			}
			bool flag = onOff != 0;
			bool flag2 = fixOwnerRootBoneTransform != 0;
			CharacterHandler mySingleCharaHndl = this.m_MySingleCharaHndl;
			bool flag3 = mySingleCharaHndl == this.m_Owner;
			BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_0;
			if (flag)
			{
				if (flag3 && !flag2)
				{
					this.SetLocatorStatus(eLocatorType, true, mySingleCharaHndl);
				}
				else
				{
					Transform locator = this.GetLocator(eLocatorType);
					this.SetCharaParent(mySingleCharaHndl, locator);
					this.SetLocatorStatus(eLocatorType, true, mySingleCharaHndl);
				}
				if (flag3)
				{
					if (flag2)
					{
					}
				}
				else
				{
					mySingleCharaHndl.SetEnableRender(true);
					mySingleCharaHndl.CharaBattle.RequestIdleMode(0.3f, 0f, false);
				}
			}
			else
			{
				this.SetCharaParent(mySingleCharaHndl, this.m_BaseTransform);
				if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == mySingleCharaHndl)
				{
					this.SetLocatorStatus(eLocatorType, false, null);
				}
				if (flag3)
				{
					mySingleCharaHndl.CharaBattle.SetFixRootBoneTransform(false);
				}
				else
				{
					mySingleCharaHndl.SetEnableRender(false);
				}
			}
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x0002A9AC File Offset: 0x00028DAC
		private void OnMeigeAnimEvent_MyAll_OnOff(int onOff, int fixOwnerRootBoneTransform)
		{
			bool flag = onOff != 0;
			bool flag2 = fixOwnerRootBoneTransform != 0;
			int num = 0;
			CharacterHandler characterHandler = null;
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				characterHandler = this.m_MyCharaHndls[i];
				bool flag3 = characterHandler == this.m_Owner;
				if (flag)
				{
					BattleUniqueSkillBaseObjectHandler.eLocatorType locatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.None;
					if (!flag2)
					{
						if (!flag3)
						{
							locatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_1 + num;
							num++;
						}
					}
					else
					{
						BattleDefine.eJoinMember join = characterHandler.CharaBattle.Join;
						if (join != BattleDefine.eJoinMember.Join_1)
						{
							if (join != BattleDefine.eJoinMember.Join_2)
							{
								if (join == BattleDefine.eJoinMember.Join_3)
								{
									locatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_2;
								}
							}
							else
							{
								locatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_1;
							}
						}
						else
						{
							locatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_0;
						}
					}
					if (flag3 && !flag2)
					{
						this.SetLocatorStatus(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_0, true, characterHandler);
					}
					else
					{
						Transform locator = this.GetLocator(locatorType);
						if (locator == null)
						{
							goto IL_1CA;
						}
						this.SetCharaParent(characterHandler, locator);
						this.SetLocatorStatus(locatorType, true, characterHandler);
					}
					if (flag3)
					{
						if (flag2)
						{
							characterHandler.CharaBattle.SetFixRootBoneTransform(true);
						}
					}
					else
					{
						characterHandler.SetEnableRender(true);
						characterHandler.CharaBattle.RequestIdleMode(0.3f, 0f, false);
					}
				}
				else
				{
					this.SetCharaParent(characterHandler, this.m_BaseTransform);
					foreach (BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType in this.m_CharaHndlsInLocator.Keys)
					{
						if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == characterHandler)
						{
							this.SetLocatorStatus(eLocatorType, false, null);
							break;
						}
					}
					if (flag3)
					{
						characterHandler.CharaBattle.SetFixRootBoneTransform(false);
					}
					else
					{
						characterHandler.SetEnableRender(false);
					}
				}
				IL_1CA:;
			}
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x0002ABAC File Offset: 0x00028FAC
		private void OnMeigeAnimEvent_My_OnOff(int targetLocator, int onOff)
		{
			BattleUniqueSkillBaseObjectHandler.eLocatorType eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.None;
			if (targetLocator != 0)
			{
				if (targetLocator != 1)
				{
					if (targetLocator == 2)
					{
						eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_2;
					}
				}
				else
				{
					eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_1;
				}
			}
			else
			{
				eLocatorType = BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_MY_0;
			}
			Transform locator = this.GetLocator(eLocatorType);
			if (locator == null)
			{
				return;
			}
			CharacterHandler characterHandler = null;
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				if (this.m_MyCharaHndls[i].CharaBattle.Join == (BattleDefine.eJoinMember)targetLocator)
				{
					characterHandler = this.m_MyCharaHndls[i];
					break;
				}
			}
			if (characterHandler == null)
			{
				return;
			}
			this.SetCharaParent(characterHandler, locator);
			bool flag = onOff != 0;
			if (flag)
			{
				characterHandler.SetEnableRender(true);
				characterHandler.CharaBattle.RequestIdleMode(0.3f, 0f, false);
				this.SetLocatorStatus(eLocatorType, true, characterHandler);
			}
			else
			{
				characterHandler.SetEnableRender(false);
				if (this.m_CharaHndlsInLocator[eLocatorType].m_CharaHndl == characterHandler)
				{
					this.SetLocatorStatus(eLocatorType, false, null);
				}
			}
		}

		// Token: 0x0600071A RID: 1818 RVA: 0x0002ACCC File Offset: 0x000290CC
		private void OnMeigeAnimEvent_DamageAnim(int targetLocator)
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			switch (targetLocator)
			{
			case 0:
			{
				BattleUniqueSkillBaseObjectHandler.LocatorStatus locatorStatus = null;
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0, out locatorStatus))
				{
					list.Add(locatorStatus.m_CharaHndl);
				}
				break;
			}
			case 1:
			{
				BattleUniqueSkillBaseObjectHandler.LocatorStatus locatorStatus2 = null;
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1, out locatorStatus2))
				{
					list.Add(locatorStatus2.m_CharaHndl);
				}
				break;
			}
			case 2:
			{
				BattleUniqueSkillBaseObjectHandler.LocatorStatus locatorStatus3 = null;
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_2, out locatorStatus3))
				{
					list.Add(locatorStatus3.m_CharaHndl);
				}
				break;
			}
			case 3:
			{
				BattleUniqueSkillBaseObjectHandler.LocatorStatus locatorStatus4 = null;
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_0, out locatorStatus4))
				{
					list.Add(locatorStatus4.m_CharaHndl);
				}
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_1, out locatorStatus4))
				{
					list.Add(locatorStatus4.m_CharaHndl);
				}
				if (this.m_CharaHndlsInLocator.TryGetValue(BattleUniqueSkillBaseObjectHandler.eLocatorType.LOC_TGT_2, out locatorStatus4))
				{
					list.Add(locatorStatus4.m_CharaHndl);
				}
				break;
			}
			}
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i] != null)
				{
					list[i].CharaBattle.RequestDamageMode();
				}
			}
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x0002AE14 File Offset: 0x00029214
		private void OnMeigeAnimEvent_WeaponVisible(int onOff, int option0, int option1)
		{
			bool flag = onOff != 0;
			if (option0 != 1)
			{
				if (option0 != 2)
				{
					this.m_Owner.SetEnableRenderWeapon(flag);
				}
				else if (this.m_Owner.CharaParam.ClassType == eClassType.Knight)
				{
					this.m_Owner.SetEnableRenderWeapon(CharacterDefine.eWeaponIndex.L, false, true);
				}
			}
			else if (this.m_Owner.CharaParam.ClassType == eClassType.Alchemist || this.m_Owner.CharaParam.ClassType == eClassType.Knight)
			{
				if (option1 == 0)
				{
					this.m_Owner.SetEnableRenderWeapon(CharacterDefine.eWeaponIndex.L, flag, true);
				}
				else
				{
					this.m_Owner.SetEnableRenderWeapon(CharacterDefine.eWeaponIndex.R, flag, true);
				}
			}
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x0002AED4 File Offset: 0x000292D4
		private void OnMeigeAnimEvent_MeshColor_TgtSingle_Change(int miliSec, int colorType)
		{
			if (this.m_TgtSingleCharaHndl == null)
			{
				return;
			}
			float sec = (float)miliSec / 1000f;
			CharacterHandler tgtSingleCharaHndl = this.m_TgtSingleCharaHndl;
			Color currentMeshColor = tgtSingleCharaHndl.CharaAnim.GetCurrentMeshColor();
			Color meshColorChangeType = this.GetMeshColorChangeType(colorType);
			tgtSingleCharaHndl.CharaAnim.ChangeMeshColor(currentMeshColor, meshColorChangeType, sec);
		}

		// Token: 0x0600071D RID: 1821 RVA: 0x0002AF28 File Offset: 0x00029328
		private void OnMeigeAnimEvent_MeshColor_TgtSingle_Revert(int miliSec)
		{
			if (this.m_TgtSingleCharaHndl == null)
			{
				return;
			}
			float sec = (float)miliSec / 1000f;
			CharacterHandler tgtSingleCharaHndl = this.m_TgtSingleCharaHndl;
			Color currentMeshColor = tgtSingleCharaHndl.CharaAnim.GetCurrentMeshColor();
			Color white = Color.white;
			tgtSingleCharaHndl.CharaAnim.ChangeMeshColor(currentMeshColor, white, sec);
		}

		// Token: 0x0600071E RID: 1822 RVA: 0x0002AF78 File Offset: 0x00029378
		private void OnMeigeAnimEvent_MeshColor_TgtAll_Change(int miliSec, int colorType)
		{
			float sec = (float)miliSec / 1000f;
			Color meshColorChangeType = this.GetMeshColorChangeType(colorType);
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_TgtCharaHndls[i];
				Color currentMeshColor = characterHandler.CharaAnim.GetCurrentMeshColor();
				characterHandler.CharaAnim.ChangeMeshColor(currentMeshColor, meshColorChangeType, sec);
			}
		}

		// Token: 0x0600071F RID: 1823 RVA: 0x0002AFDC File Offset: 0x000293DC
		private void OnMeigeAnimEvent_MeshColor_TgtAll_Revert(int miliSec)
		{
			float sec = (float)miliSec / 1000f;
			Color white = Color.white;
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_TgtCharaHndls[i];
				Color currentMeshColor = characterHandler.CharaAnim.GetCurrentMeshColor();
				characterHandler.CharaAnim.ChangeMeshColor(currentMeshColor, white, sec);
			}
		}

		// Token: 0x06000720 RID: 1824 RVA: 0x0002B040 File Offset: 0x00029440
		private void OnMeigeAnimEvent_MeshColor_MySingle_Change(int miliSec, int colorType)
		{
			if (this.m_MySingleCharaHndl == null)
			{
				return;
			}
			float sec = (float)miliSec / 1000f;
			CharacterHandler mySingleCharaHndl = this.m_MySingleCharaHndl;
			Color currentMeshColor = mySingleCharaHndl.CharaAnim.GetCurrentMeshColor();
			Color meshColorChangeType = this.GetMeshColorChangeType(colorType);
			mySingleCharaHndl.CharaAnim.ChangeMeshColor(currentMeshColor, meshColorChangeType, sec);
		}

		// Token: 0x06000721 RID: 1825 RVA: 0x0002B094 File Offset: 0x00029494
		private void OnMeigeAnimEvent_MeshColor_MySingle_Revert(int miliSec)
		{
			if (this.m_MySingleCharaHndl == null)
			{
				return;
			}
			float sec = (float)miliSec / 1000f;
			CharacterHandler mySingleCharaHndl = this.m_MySingleCharaHndl;
			Color currentMeshColor = mySingleCharaHndl.CharaAnim.GetCurrentMeshColor();
			Color white = Color.white;
			mySingleCharaHndl.CharaAnim.ChangeMeshColor(currentMeshColor, white, sec);
		}

		// Token: 0x06000722 RID: 1826 RVA: 0x0002B0E4 File Offset: 0x000294E4
		private void OnMeigeAnimEvent_MeshColor_MyAll_Change(int miliSec, int colorType)
		{
			float sec = (float)miliSec / 1000f;
			Color meshColorChangeType = this.GetMeshColorChangeType(colorType);
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_MyCharaHndls[i];
				Color currentMeshColor = characterHandler.CharaAnim.GetCurrentMeshColor();
				characterHandler.CharaAnim.ChangeMeshColor(currentMeshColor, meshColorChangeType, sec);
			}
		}

		// Token: 0x06000723 RID: 1827 RVA: 0x0002B148 File Offset: 0x00029548
		private void OnMeigeAnimEvent_MeshColor_MyAll_Revert(int miliSec)
		{
			float sec = (float)miliSec / 1000f;
			Color white = Color.white;
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_MyCharaHndls[i];
				Color currentMeshColor = characterHandler.CharaAnim.GetCurrentMeshColor();
				characterHandler.CharaAnim.ChangeMeshColor(currentMeshColor, white, sec);
			}
		}

		// Token: 0x06000724 RID: 1828 RVA: 0x0002B1AC File Offset: 0x000295AC
		private void OnMeigeAnimEvent_MeshColor_Self_Change(int miliSec, int colorType)
		{
			float sec = (float)miliSec / 1000f;
			CharacterHandler owner = this.m_Owner;
			Color currentMeshColor = owner.CharaAnim.GetCurrentMeshColor();
			Color meshColorChangeType = this.GetMeshColorChangeType(colorType);
			owner.CharaAnim.ChangeMeshColor(currentMeshColor, meshColorChangeType, sec);
		}

		// Token: 0x06000725 RID: 1829 RVA: 0x0002B1EC File Offset: 0x000295EC
		private void OnMeigeAnimEvent_MeshColor_Self_Revert(int miliSec)
		{
			float sec = (float)miliSec / 1000f;
			CharacterHandler owner = this.m_Owner;
			Color currentMeshColor = owner.CharaAnim.GetCurrentMeshColor();
			Color white = Color.white;
			owner.CharaAnim.ChangeMeshColor(currentMeshColor, white, sec);
		}

		// Token: 0x06000726 RID: 1830 RVA: 0x0002B229 File Offset: 0x00029629
		private Color GetMeshColorChangeType(int colorType)
		{
			if (colorType != 0)
			{
				return Color.black;
			}
			return Color.black;
		}

		// Token: 0x06000727 RID: 1831 RVA: 0x0002B244 File Offset: 0x00029644
		private void OnMeigeAnimEvent_ShadowVisible_Owner(int onOff)
		{
			bool enableRenderShadow = onOff != 0;
			CharacterHandler owner = this.m_Owner;
			owner.SetEnableRenderShadow(enableRenderShadow);
		}

		// Token: 0x06000728 RID: 1832 RVA: 0x0002B270 File Offset: 0x00029670
		private void OnMeigeAnimEvent_ShadowVisible_TgtAll(int onOff)
		{
			bool enableRenderShadow = onOff != 0;
			for (int i = 0; i < this.m_TgtCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_TgtCharaHndls[i];
				characterHandler.SetEnableRenderShadow(enableRenderShadow);
			}
		}

		// Token: 0x06000729 RID: 1833 RVA: 0x0002B2BC File Offset: 0x000296BC
		private void OnMeigeAnimEvent_ShadowVisible_MyAll(int onOff)
		{
			bool enableRenderShadow = onOff != 0;
			for (int i = 0; i < this.m_MyCharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_MyCharaHndls[i];
				characterHandler.SetEnableRenderShadow(enableRenderShadow);
			}
		}

		// Token: 0x0600072A RID: 1834 RVA: 0x0002B307 File Offset: 0x00029707
		private void SetCharaParent(CharacterHandler charaHndl, Transform parent)
		{
			if (charaHndl != null)
			{
				charaHndl.CacheTransform.SetParent(parent, false);
				charaHndl.CacheTransform.localScale = Vector3.one;
				charaHndl.CacheTransform.localPosition = Vector3.zero;
			}
		}

		// Token: 0x0400062A RID: 1578
		private readonly string[] ASSOCIATED_LOCATOR_MESH_NAMES = new string[]
		{
			"_loc_TGT_0_",
			"_loc_TGT_1_",
			"_loc_TGT_2_",
			"_loc_MY_0_",
			"_loc_MY_1_",
			"_loc_MY_2_"
		};

		// Token: 0x0400062B RID: 1579
		private const string LOCATOR_TGT_0 = "loc_TGT_0";

		// Token: 0x0400062C RID: 1580
		private const string LOCATOR_TGT_1 = "loc_TGT_1";

		// Token: 0x0400062D RID: 1581
		private const string LOCATOR_TGT_2 = "loc_TGT_2";

		// Token: 0x0400062E RID: 1582
		private const string LOCATOR_MY_0 = "loc_MY_0";

		// Token: 0x0400062F RID: 1583
		private const string LOCATOR_MY_1 = "loc_MY_1";

		// Token: 0x04000630 RID: 1584
		private const string LOCATOR_MY_2 = "loc_MY_2";

		// Token: 0x04000631 RID: 1585
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04000632 RID: 1586
		private const float IDLE_BLEND_SEC = 0.3f;

		// Token: 0x04000633 RID: 1587
		private const float NEAR_CLIP = 0.1f;

		// Token: 0x04000634 RID: 1588
		private const float FAR_CLIP = 200f;

		// Token: 0x04000635 RID: 1589
		private Transform m_BaseTransform;

		// Token: 0x04000636 RID: 1590
		private Animation m_Anim;

		// Token: 0x04000637 RID: 1591
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04000638 RID: 1592
		private MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x04000639 RID: 1593
		private MsbHandler m_MsbHndl;

		// Token: 0x0400063A RID: 1594
		private CharacterHandler m_Owner;

		// Token: 0x0400063B RID: 1595
		private List<CharacterHandler> m_TgtCharaHndls;

		// Token: 0x0400063C RID: 1596
		private CharacterHandler m_TgtSingleCharaHndl;

		// Token: 0x0400063D RID: 1597
		private List<CharacterHandler> m_MyCharaHndls;

		// Token: 0x0400063E RID: 1598
		private CharacterHandler m_MySingleCharaHndl;

		// Token: 0x0400063F RID: 1599
		private Dictionary<BattleUniqueSkillBaseObjectHandler.eLocatorType, BattleUniqueSkillBaseObjectHandler.LocatorStatus> m_CharaHndlsInLocator = new Dictionary<BattleUniqueSkillBaseObjectHandler.eLocatorType, BattleUniqueSkillBaseObjectHandler.LocatorStatus>();

		// Token: 0x04000640 RID: 1600
		private List<MsbObjectHandler>[] m_AssociatedLocatorObjs;

		// Token: 0x04000641 RID: 1601
		private List<MeigeParticleEmitter>[] m_AssociatedEmmiters;

		// Token: 0x04000642 RID: 1602
		private const int CHECK_CHARA_MESH_NUM = 3;

		// Token: 0x04000643 RID: 1603
		private List<BattleUniqueSkillBaseObjectHandler.CharaRender> m_CharaRenderCache;

		// Token: 0x04000644 RID: 1604
		private List<Renderer> m_RenderCache;

		// Token: 0x020000F9 RID: 249
		public enum eLocatorType
		{
			// Token: 0x04000646 RID: 1606
			None = -1,
			// Token: 0x04000647 RID: 1607
			LOC_TGT_0,
			// Token: 0x04000648 RID: 1608
			LOC_TGT_1,
			// Token: 0x04000649 RID: 1609
			LOC_TGT_2,
			// Token: 0x0400064A RID: 1610
			LOC_MY_0,
			// Token: 0x0400064B RID: 1611
			LOC_MY_1,
			// Token: 0x0400064C RID: 1612
			LOC_MY_2,
			// Token: 0x0400064D RID: 1613
			Num
		}

		// Token: 0x020000FA RID: 250
		public enum eAnimEvent
		{
			// Token: 0x0400064F RID: 1615
			FadeIn = 101,
			// Token: 0x04000650 RID: 1616
			FadeOut,
			// Token: 0x04000651 RID: 1617
			TgtSingle_OnOff = 110,
			// Token: 0x04000652 RID: 1618
			TgtAll_OnOff,
			// Token: 0x04000653 RID: 1619
			Tgt_OnOff,
			// Token: 0x04000654 RID: 1620
			MySingle_OnOff = 120,
			// Token: 0x04000655 RID: 1621
			MyAll_OnOff,
			// Token: 0x04000656 RID: 1622
			My_OnOff,
			// Token: 0x04000657 RID: 1623
			DamageAnim = 130,
			// Token: 0x04000658 RID: 1624
			WeaponVisible = 140,
			// Token: 0x04000659 RID: 1625
			MeshColor_TgtSingle_Change = 150,
			// Token: 0x0400065A RID: 1626
			MeshColor_TgtSingle_Revert,
			// Token: 0x0400065B RID: 1627
			MeshColor_TgtAll_Change,
			// Token: 0x0400065C RID: 1628
			MeshColor_TgtAll_Revert,
			// Token: 0x0400065D RID: 1629
			MeshColor_MySingle_Change = 160,
			// Token: 0x0400065E RID: 1630
			MeshColor_MySingle_Revert,
			// Token: 0x0400065F RID: 1631
			MeshColor_MyAll_Change,
			// Token: 0x04000660 RID: 1632
			MeshColor_MyAll_Revert,
			// Token: 0x04000661 RID: 1633
			MeshColor_Self_Change = 170,
			// Token: 0x04000662 RID: 1634
			MeshColor_Self_Revert,
			// Token: 0x04000663 RID: 1635
			ShadowVisible_Owner = 180,
			// Token: 0x04000664 RID: 1636
			ShadowVisible_TgtAll,
			// Token: 0x04000665 RID: 1637
			ShadowVisible_MyAll
		}

		// Token: 0x020000FB RID: 251
		private class LocatorStatus
		{
			// Token: 0x04000666 RID: 1638
			public bool m_IsOn;

			// Token: 0x04000667 RID: 1639
			public CharacterHandler m_CharaHndl;
		}

		// Token: 0x020000FC RID: 252
		public class CharaRender
		{
			// Token: 0x04000668 RID: 1640
			public List<Renderer> m_RenderCache;

			// Token: 0x04000669 RID: 1641
			public Transform m_Transform;
		}
	}
}
