﻿using System;

namespace Star
{
	// Token: 0x02000178 RID: 376
	[Serializable]
	public struct CharacterListDB_Param
	{
		// Token: 0x040009E4 RID: 2532
		public int m_CharaID;

		// Token: 0x040009E5 RID: 2533
		public int m_ResourceID;

		// Token: 0x040009E6 RID: 2534
		public int m_HeadID;

		// Token: 0x040009E7 RID: 2535
		public int m_BodyID;

		// Token: 0x040009E8 RID: 2536
		public string m_Name;

		// Token: 0x040009E9 RID: 2537
		public int m_NamedType;

		// Token: 0x040009EA RID: 2538
		public int m_Rare;

		// Token: 0x040009EB RID: 2539
		public int m_Class;

		// Token: 0x040009EC RID: 2540
		public int m_Element;

		// Token: 0x040009ED RID: 2541
		public int m_Cost;

		// Token: 0x040009EE RID: 2542
		public sbyte m_GrowthTableID;

		// Token: 0x040009EF RID: 2543
		public int m_InitLv;

		// Token: 0x040009F0 RID: 2544
		public int m_InitLimitLv;

		// Token: 0x040009F1 RID: 2545
		public int m_InitHp;

		// Token: 0x040009F2 RID: 2546
		public int m_InitAtk;

		// Token: 0x040009F3 RID: 2547
		public int m_InitMgc;

		// Token: 0x040009F4 RID: 2548
		public int m_InitDef;

		// Token: 0x040009F5 RID: 2549
		public int m_InitMDef;

		// Token: 0x040009F6 RID: 2550
		public int m_InitSpd;

		// Token: 0x040009F7 RID: 2551
		public int m_InitLuck;

		// Token: 0x040009F8 RID: 2552
		public int m_LeaderSkillID;

		// Token: 0x040009F9 RID: 2553
		public int m_SkillLimitLv;

		// Token: 0x040009FA RID: 2554
		public int m_CharaSkillID;

		// Token: 0x040009FB RID: 2555
		public sbyte m_CharaSkillExpTableID;

		// Token: 0x040009FC RID: 2556
		public int[] m_ClassSkillIDs;

		// Token: 0x040009FD RID: 2557
		public sbyte[] m_ClassSkillExpTableIDs;

		// Token: 0x040009FE RID: 2558
		public float m_StunCoef;

		// Token: 0x040009FF RID: 2559
		public int m_AltItemID;

		// Token: 0x04000A00 RID: 2560
		public int m_AltItemAmount;

		// Token: 0x04000A01 RID: 2561
		public byte m_FULLOPEN;

		// Token: 0x04000A02 RID: 2562
		public int m_LimitBreakRecipeID;
	}
}
