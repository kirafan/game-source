﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Global;
using Star.UI.Menu;
using WWWTypes;

namespace Star
{
	// Token: 0x02000449 RID: 1097
	public class MenuState_UserInfomation : MenuState
	{
		// Token: 0x0600151F RID: 5407 RVA: 0x0006EC20 File Offset: 0x0006D020
		public MenuState_UserInfomation(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x06001520 RID: 5408 RVA: 0x0006EC38 File Offset: 0x0006D038
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x06001521 RID: 5409 RVA: 0x0006EC3B File Offset: 0x0006D03B
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_UserInfomation.eStep.First;
		}

		// Token: 0x06001522 RID: 5410 RVA: 0x0006EC44 File Offset: 0x0006D044
		public override void OnStateExit()
		{
		}

		// Token: 0x06001523 RID: 5411 RVA: 0x0006EC46 File Offset: 0x0006D046
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001524 RID: 5412 RVA: 0x0006EC50 File Offset: 0x0006D050
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_UserInfomation.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_UserInfomation.eStep.LoadWait;
				break;
			case MenuState_UserInfomation.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_UserInfomation.eStep.PlayIn;
				}
				break;
			case MenuState_UserInfomation.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_UserInfomation.eStep.Main;
				}
				break;
			case MenuState_UserInfomation.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_UserInfomation.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_UserInfomation.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_UserInfomation.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001525 RID: 5413 RVA: 0x0006ED70 File Offset: 0x0006D170
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuUserInfomationUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.UserData);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001526 RID: 5414 RVA: 0x0006EE07 File Offset: 0x0006D207
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (this.m_UI.m_IsDirtyUserData)
			{
				this.SendPlayerNameSet();
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001527 RID: 5415 RVA: 0x0006EE31 File Offset: 0x0006D231
		private void OnClickButton()
		{
		}

		// Token: 0x06001528 RID: 5416 RVA: 0x0006EE33 File Offset: 0x0006D233
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x06001529 RID: 5417 RVA: 0x0006EE50 File Offset: 0x0006D250
		private void SendPlayerNameSet()
		{
			MeigewwwParam wwwParam = PlayerRequest.Set(new PlayerRequestTypes.Set
			{
				name = this.m_UI.GetInputUserName(),
				comment = this.m_UI.GetInputCommentName()
			}, new MeigewwwParam.Callback(this.OnResponsePlayerRequest));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x0600152A RID: 5418 RVA: 0x0006EEB0 File Offset: 0x0006D2B0
		public void OnResponsePlayerRequest(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Set set = PlayerResponse.Set(wwwParam, ResponseCommon.DialogType.None, null);
			if (set == null)
			{
				return;
			}
			ResultCode result = set.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					this.m_UI.Refresh();
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.NgWordError), result.ToMessageString(), null);
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Name = this.m_UI.GetInputUserName();
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Comment = this.m_UI.GetInputCommentName();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x04001C07 RID: 7175
		private MenuState_UserInfomation.eStep m_Step = MenuState_UserInfomation.eStep.None;

		// Token: 0x04001C08 RID: 7176
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuUserInfomationUI;

		// Token: 0x04001C09 RID: 7177
		private MenuUserInfomationUI m_UI;

		// Token: 0x0200044A RID: 1098
		private enum eStep
		{
			// Token: 0x04001C0B RID: 7179
			None = -1,
			// Token: 0x04001C0C RID: 7180
			First,
			// Token: 0x04001C0D RID: 7181
			LoadWait,
			// Token: 0x04001C0E RID: 7182
			PlayIn,
			// Token: 0x04001C0F RID: 7183
			Main,
			// Token: 0x04001C10 RID: 7184
			UnloadChildSceneWait
		}
	}
}
