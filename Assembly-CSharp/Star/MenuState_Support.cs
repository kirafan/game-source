﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000445 RID: 1093
	public class MenuState_Support : MenuState
	{
		// Token: 0x0600150A RID: 5386 RVA: 0x0006E68D File Offset: 0x0006CA8D
		public MenuState_Support(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x0600150B RID: 5387 RVA: 0x0006E6A5 File Offset: 0x0006CAA5
		public override int GetStateID()
		{
			return 5;
		}

		// Token: 0x0600150C RID: 5388 RVA: 0x0006E6A8 File Offset: 0x0006CAA8
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Support.eStep.First;
		}

		// Token: 0x0600150D RID: 5389 RVA: 0x0006E6B1 File Offset: 0x0006CAB1
		public override void OnStateExit()
		{
		}

		// Token: 0x0600150E RID: 5390 RVA: 0x0006E6B3 File Offset: 0x0006CAB3
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600150F RID: 5391 RVA: 0x0006E6BC File Offset: 0x0006CABC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Support.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Support.eStep.LoadWait;
				break;
			case MenuState_Support.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Support.eStep.PlayIn;
				}
				break;
			case MenuState_Support.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Support.eStep.Main;
				}
				break;
			case MenuState_Support.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_IsOnClickPurchaseHistory)
					{
						this.m_NextState = 16;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Support.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Support.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Support.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001510 RID: 5392 RVA: 0x0006E7F4 File Offset: 0x0006CBF4
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuSupportUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_IsOnClickPurchaseHistory = false;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Support);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.m_OnClickPurchaseHistory = new Action(this.OnClickPurchaseHistory);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001511 RID: 5393 RVA: 0x0006E8A9 File Offset: 0x0006CCA9
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
		}

		// Token: 0x06001512 RID: 5394 RVA: 0x0006E8C7 File Offset: 0x0006CCC7
		private void OnClickButton()
		{
		}

		// Token: 0x06001513 RID: 5395 RVA: 0x0006E8C9 File Offset: 0x0006CCC9
		protected void OnClickPurchaseHistory()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
			this.m_IsOnClickPurchaseHistory = true;
		}

		// Token: 0x06001514 RID: 5396 RVA: 0x0006E8E7 File Offset: 0x0006CCE7
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BF2 RID: 7154
		private MenuState_Support.eStep m_Step = MenuState_Support.eStep.None;

		// Token: 0x04001BF3 RID: 7155
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuSupportUI;

		// Token: 0x04001BF4 RID: 7156
		private MenuSupportUI m_UI;

		// Token: 0x04001BF5 RID: 7157
		private bool m_IsOnClickPurchaseHistory;

		// Token: 0x02000446 RID: 1094
		private enum eStep
		{
			// Token: 0x04001BF7 RID: 7159
			None = -1,
			// Token: 0x04001BF8 RID: 7160
			First,
			// Token: 0x04001BF9 RID: 7161
			LoadWait,
			// Token: 0x04001BFA RID: 7162
			PlayIn,
			// Token: 0x04001BFB RID: 7163
			Main,
			// Token: 0x04001BFC RID: 7164
			UnloadChildSceneWait
		}
	}
}
