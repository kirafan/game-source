﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020005AE RID: 1454
	public class RoomFloorDebugForm : DebugWindowForm
	{
		// Token: 0x06001C17 RID: 7191 RVA: 0x000952A4 File Offset: 0x000936A4
		public void Awake()
		{
			this.m_Form = base.transform;
			GameObject gameObject = GameObject.Find("RoomBuilder");
			if (gameObject != null)
			{
				this.m_Builder = gameObject.GetComponent<RoomBuilder>();
			}
			Toggle toggle = DebugWindowBase.FindHrcObject(base.transform, "GridView", typeof(Toggle)) as Toggle;
			if (toggle != null)
			{
				toggle.onValueChanged.AddListener(new UnityAction<bool>(this.ViewChange));
			}
			this.m_FreeGrid = (DebugWindowBase.FindHrcObject(base.transform, "FreeGrid/Names", typeof(Text)) as Text);
			if (this.m_Builder != null)
			{
				this.SetUpFloorView();
			}
		}

		// Token: 0x06001C18 RID: 7192 RVA: 0x0009535F File Offset: 0x0009375F
		private void Update()
		{
			this.SetUpFloorView();
		}

		// Token: 0x06001C19 RID: 7193 RVA: 0x00095368 File Offset: 0x00093768
		private void SetUpFloorView()
		{
			if (this.m_Builder != null)
			{
				RoomGridState gridStatus = this.m_Builder.GetGridStatus(0);
				if (gridStatus != null && this.m_FreeGrid != null)
				{
					this.m_FreeGrid.text = gridStatus.m_MoveFreeSpaceNum.ToString();
				}
			}
		}

		// Token: 0x06001C1A RID: 7194 RVA: 0x000953C8 File Offset: 0x000937C8
		private void ViewChange(bool factive)
		{
			if (this.m_Builder != null)
			{
				IRoomFloorManager floorManager = this.m_Builder.GetFloorManager(0);
				if (floorManager != null && floorManager.GetRoot() != null)
				{
					Transform transform = RoomUtility.FindTransform(floorManager.GetRoot().transform, "HitTileGroup", 10);
					if (transform != null)
					{
						Renderer[] componentsInChildren = transform.GetComponentsInChildren<Renderer>();
						for (int i = 0; i < componentsInChildren.Length; i++)
						{
							if (componentsInChildren[i].enabled)
							{
								componentsInChildren[i].enabled = false;
							}
							else
							{
								componentsInChildren[i].enabled = true;
							}
						}
					}
					transform = RoomUtility.FindTransform(floorManager.GetRoot().transform, "HitWallGroup", 10);
					if (transform != null)
					{
						Renderer[] componentsInChildren = transform.GetComponentsInChildren<Renderer>();
						for (int i = 0; i < componentsInChildren.Length; i++)
						{
							if (componentsInChildren[i].enabled)
							{
								componentsInChildren[i].enabled = false;
							}
							else
							{
								componentsInChildren[i].enabled = true;
							}
						}
					}
				}
			}
		}

		// Token: 0x06001C1B RID: 7195 RVA: 0x000954D0 File Offset: 0x000938D0
		public override void SetActive(bool factive)
		{
			this.m_Form.gameObject.SetActive(factive);
		}

		// Token: 0x04002310 RID: 8976
		private RoomBuilder m_Builder;

		// Token: 0x04002311 RID: 8977
		private Text m_FreeGrid;
	}
}
