﻿using System;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x02000407 RID: 1031
	public class EditState_Init : EditState
	{
		// Token: 0x0600139B RID: 5019 RVA: 0x00068D5B File Offset: 0x0006715B
		public EditState_Init(EditMain owner) : base(owner)
		{
		}

		// Token: 0x0600139C RID: 5020 RVA: 0x00068D6B File Offset: 0x0006716B
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600139D RID: 5021 RVA: 0x00068D6E File Offset: 0x0006716E
		public override void OnStateEnter()
		{
			this.m_Step = EditState_Init.eStep.First;
		}

		// Token: 0x0600139E RID: 5022 RVA: 0x00068D77 File Offset: 0x00067177
		public override void OnStateExit()
		{
		}

		// Token: 0x0600139F RID: 5023 RVA: 0x00068D79 File Offset: 0x00067179
		public override void OnDispose()
		{
		}

		// Token: 0x060013A0 RID: 5024 RVA: 0x00068D7C File Offset: 0x0006717C
		public override int OnStateUpdate()
		{
			EditState_Init.eStep step = this.m_Step;
			if (step != EditState_Init.eStep.First)
			{
				if (step != EditState_Init.eStep.Load_Wait)
				{
					if (step == EditState_Init.eStep.End)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
						{
							this.m_Step = EditState_Init.eStep.None;
							if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuUpgradeStart)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuUpgradeStart = false;
								return 8;
							}
							return 1;
						}
					}
				}
				else if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets())
					{
						SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
						this.m_Step = EditState_Init.eStep.End;
					}
				}
			}
			else if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Common, false);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Mix);
				eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
				if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
				}
				this.m_Step = EditState_Init.eStep.Load_Wait;
			}
			return -1;
		}

		// Token: 0x060013A1 RID: 5025 RVA: 0x00068EC7 File Offset: 0x000672C7
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001A95 RID: 6805
		private EditState_Init.eStep m_Step = EditState_Init.eStep.None;

		// Token: 0x02000408 RID: 1032
		private enum eStep
		{
			// Token: 0x04001A97 RID: 6807
			None = -1,
			// Token: 0x04001A98 RID: 6808
			First,
			// Token: 0x04001A99 RID: 6809
			Load_Wait,
			// Token: 0x04001A9A RID: 6810
			End
		}
	}
}
