﻿using System;

namespace Star
{
	// Token: 0x02000636 RID: 1590
	public class SoundObject
	{
		// Token: 0x06001F4D RID: 8013 RVA: 0x000A9DE0 File Offset: 0x000A81E0
		public SoundObject()
		{
			this.m_Player = new CriAtomExPlayer();
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06001F4E RID: 8014 RVA: 0x000A9E0C File Offset: 0x000A820C
		public string CueSeet
		{
			get
			{
				return this.m_CueSheet;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06001F4F RID: 8015 RVA: 0x000A9E14 File Offset: 0x000A8214
		public string CueName
		{
			get
			{
				return this.m_CueName;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06001F50 RID: 8016 RVA: 0x000A9E1C File Offset: 0x000A821C
		// (set) Token: 0x06001F51 RID: 8017 RVA: 0x000A9E24 File Offset: 0x000A8224
		public byte Ref { get; private set; }

		// Token: 0x06001F52 RID: 8018 RVA: 0x000A9E2D File Offset: 0x000A822D
		public void AddRef()
		{
			this.Ref = 1;
		}

		// Token: 0x06001F53 RID: 8019 RVA: 0x000A9E36 File Offset: 0x000A8236
		public void RemoveRef()
		{
			this.Ref = 0;
		}

		// Token: 0x06001F54 RID: 8020 RVA: 0x000A9E3F File Offset: 0x000A823F
		public void Destroy()
		{
			if (this.m_Player != null)
			{
				this.m_Player.Dispose();
				this.m_Player = null;
			}
		}

		// Token: 0x06001F55 RID: 8021 RVA: 0x000A9E5E File Offset: 0x000A825E
		public void Update()
		{
			if (this.m_Player == null)
			{
				return;
			}
			if (this.m_NeedToPlayerUpdateAll)
			{
				this.m_Player.Update(this.m_Playback);
				this.m_NeedToPlayerUpdateAll = false;
			}
		}

		// Token: 0x06001F56 RID: 8022 RVA: 0x000A9E90 File Offset: 0x000A8290
		public SoundObject.eStatus GetStatus()
		{
			if (this.m_Player != null)
			{
				CriAtomExPlayback.Status status = this.m_Playback.GetStatus();
				if (status == CriAtomExPlayback.Status.Prep)
				{
					return SoundObject.eStatus.Prepare;
				}
				if (status == CriAtomExPlayback.Status.Playing)
				{
					return SoundObject.eStatus.Playing;
				}
				if (status == CriAtomExPlayback.Status.Removed)
				{
					return SoundObject.eStatus.Removed;
				}
			}
			return SoundObject.eStatus.None;
		}

		// Token: 0x06001F57 RID: 8023 RVA: 0x000A9ED5 File Offset: 0x000A82D5
		public bool IsCompletePrepare()
		{
			return this.m_Player != null && this.m_IsPrepared && this.GetStatus() == SoundObject.eStatus.Playing;
		}

		// Token: 0x06001F58 RID: 8024 RVA: 0x000A9EFC File Offset: 0x000A82FC
		public bool IsPaused()
		{
			return this.m_Player != null && this.m_Playback.IsPaused();
		}

		// Token: 0x06001F59 RID: 8025 RVA: 0x000A9F16 File Offset: 0x000A8316
		public bool IsPrepared()
		{
			return this.m_IsPrepared;
		}

		// Token: 0x06001F5A RID: 8026 RVA: 0x000A9F20 File Offset: 0x000A8320
		public bool Play(string cueSheet, string cueName, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_CueSheet = cueSheet;
			this.m_CueName = cueName;
			this.m_Volume = volume;
			this.m_StartTimeMiliSec = startTimeMiliSec;
			this.m_FadeInMiliSec = fadeInMiliSec;
			this.m_FadeOutMiliSec = fadeOutMiliSec;
			this.m_Player.ResetParameters();
			this.m_Player.SetVolume(volume);
			this.m_Player.SetStartTime((long)startTimeMiliSec);
			if (fadeInMiliSec >= 0)
			{
				this.m_Player.SetEnvelopeAttackTime((float)fadeInMiliSec);
			}
			if (fadeOutMiliSec >= 0)
			{
				this.m_Player.SetEnvelopeReleaseTime((float)fadeOutMiliSec);
			}
			this.m_Player.SetCue(CriAtom.GetAcb(this.m_CueSheet), this.m_CueName);
			this.m_Playback = this.m_Player.Start();
			this.m_Player.Update(this.m_Playback);
			this.m_IsPrepared = false;
			return true;
		}

		// Token: 0x06001F5B RID: 8027 RVA: 0x000A9FFE File Offset: 0x000A83FE
		public bool Play()
		{
			return this.Play(this.m_CueSheet, this.m_CueName, this.m_Volume, this.m_StartTimeMiliSec, this.m_FadeInMiliSec, this.m_FadeOutMiliSec);
		}

		// Token: 0x06001F5C RID: 8028 RVA: 0x000AA02C File Offset: 0x000A842C
		public bool Prepare(string cueSheet, string cueName, float volume, int startTimeMiliSec, int fadeInMiliSec, int fadeOutMiliSec)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_CueSheet = cueSheet;
			this.m_CueName = cueName;
			this.m_Volume = volume;
			this.m_StartTimeMiliSec = startTimeMiliSec;
			this.m_Player.ResetParameters();
			this.m_Player.SetVolume(volume);
			this.m_Player.SetStartTime((long)startTimeMiliSec);
			if (fadeInMiliSec >= 0)
			{
				this.m_Player.SetEnvelopeAttackTime((float)fadeInMiliSec);
			}
			if (fadeOutMiliSec >= 0)
			{
				this.m_Player.SetEnvelopeReleaseTime((float)fadeOutMiliSec);
			}
			this.m_Player.SetCue(CriAtom.GetAcb(this.m_CueSheet), this.m_CueName);
			this.m_Playback = this.m_Player.Prepare();
			this.m_Player.Update(this.m_Playback);
			this.m_IsPrepared = true;
			return true;
		}

		// Token: 0x06001F5D RID: 8029 RVA: 0x000AA0FA File Offset: 0x000A84FA
		public bool Stop()
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_Playback.Stop();
			return true;
		}

		// Token: 0x06001F5E RID: 8030 RVA: 0x000AA115 File Offset: 0x000A8515
		public bool Suspend()
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_Playback.Pause();
			return true;
		}

		// Token: 0x06001F5F RID: 8031 RVA: 0x000AA130 File Offset: 0x000A8530
		public bool Resume()
		{
			if (this.m_Player == null)
			{
				return false;
			}
			if (this.m_IsPrepared)
			{
				this.m_Playback.Resume(CriAtomEx.ResumeMode.PreparedPlayback);
				this.m_IsPrepared = false;
			}
			else
			{
				this.m_Playback.Resume(CriAtomEx.ResumeMode.PausedPlayback);
			}
			return true;
		}

		// Token: 0x040025F3 RID: 9715
		private CriAtomExPlayer m_Player;

		// Token: 0x040025F4 RID: 9716
		private CriAtomExPlayback m_Playback;

		// Token: 0x040025F5 RID: 9717
		private bool m_NeedToPlayerUpdateAll;

		// Token: 0x040025F6 RID: 9718
		private string m_CueSheet;

		// Token: 0x040025F7 RID: 9719
		private string m_CueName;

		// Token: 0x040025F8 RID: 9720
		private bool m_IsPrepared;

		// Token: 0x040025F9 RID: 9721
		private float m_Volume = 1f;

		// Token: 0x040025FA RID: 9722
		private int m_StartTimeMiliSec;

		// Token: 0x040025FB RID: 9723
		private int m_FadeInMiliSec = -1;

		// Token: 0x040025FC RID: 9724
		private int m_FadeOutMiliSec = -1;

		// Token: 0x02000637 RID: 1591
		public enum eStatus
		{
			// Token: 0x040025FF RID: 9727
			None = -1,
			// Token: 0x04002600 RID: 9728
			Prepare = 1,
			// Token: 0x04002601 RID: 9729
			Playing,
			// Token: 0x04002602 RID: 9730
			Removed
		}
	}
}
