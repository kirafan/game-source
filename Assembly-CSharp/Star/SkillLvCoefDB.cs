﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200020E RID: 526
	public class SkillLvCoefDB : ScriptableObject
	{
		// Token: 0x04000CF2 RID: 3314
		public SkillLvCoefDB_Param[] m_Params;
	}
}
