﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000730 RID: 1840
	public class TownModelIO : MonoBehaviour
	{
		// Token: 0x06002446 RID: 9286 RVA: 0x000C2824 File Offset: 0x000C0C24
		private void Awake()
		{
			this.m_BackColor = Color.white;
			MsbHandler[] componentsInChildren = base.gameObject.GetComponentsInChildren<MsbHandler>();
			if (componentsInChildren.Length >= 1)
			{
				this.m_MsbHandle = new TownModelIO.MsbModelHandle[componentsInChildren.Length];
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					this.m_MsbHandle[i].m_Handle = componentsInChildren[i];
					this.m_MsbHandle[i].m_Num = componentsInChildren[i].GetMsbObjectHandlerNum();
					this.m_MsbHandle[i].m_Color = new Color[this.m_MsbHandle[i].m_Num];
					for (int j = 0; j < this.m_MsbHandle[i].m_Num; j++)
					{
						MsbObjectHandler msbObjectHandler = componentsInChildren[i].GetMsbObjectHandler(j);
						this.m_MsbHandle[i].m_Color[j] = msbObjectHandler.GetWork().m_MeshColor;
					}
				}
			}
			else
			{
				Renderer[] componentsInChildren2 = base.gameObject.GetComponentsInChildren<Renderer>();
				if (componentsInChildren2.Length >= 1)
				{
					this.m_ModelHandle = new TownModelIO.ModelHandle[componentsInChildren2.Length];
					for (int i = 0; i < componentsInChildren2.Length; i++)
					{
						this.m_ModelHandle[i].m_Handle = componentsInChildren2[i];
					}
				}
			}
		}

		// Token: 0x06002447 RID: 9287 RVA: 0x000C2970 File Offset: 0x000C0D70
		private void Update()
		{
			if (this.m_BackColor != this.ModelColor)
			{
				if (this.m_MsbHandle != null)
				{
					this.UpMsbModelColor(this.ModelColor);
				}
				if (this.m_ModelHandle != null)
				{
					this.UpModelColor(this.ModelColor);
				}
				this.m_BackColor = this.ModelColor;
			}
		}

		// Token: 0x06002448 RID: 9288 RVA: 0x000C29D0 File Offset: 0x000C0DD0
		private void UpMsbModelColor(Color fcol)
		{
			int num = this.m_MsbHandle.Length;
			for (int i = 0; i < num; i++)
			{
				int num2 = this.m_MsbHandle[i].m_Num;
				for (int j = 0; j < num2; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHandle[i].m_Handle.GetMsbObjectHandler(j);
					msbObjectHandler.GetWork().m_MeshColor = this.m_MsbHandle[i].m_Color[j] * fcol;
				}
			}
		}

		// Token: 0x06002449 RID: 9289 RVA: 0x000C2A64 File Offset: 0x000C0E64
		private void UpModelColor(Color fcol)
		{
			int num = this.m_ModelHandle.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_ModelHandle[i].m_Handle.material.SetColor("_Color", fcol);
			}
		}

		// Token: 0x04002B3F RID: 11071
		public Color ModelColor = Color.white;

		// Token: 0x04002B40 RID: 11072
		public Vector2 ModelUvOffset = Vector2.zero;

		// Token: 0x04002B41 RID: 11073
		private Color m_BackColor;

		// Token: 0x04002B42 RID: 11074
		private TownModelIO.MsbModelHandle[] m_MsbHandle;

		// Token: 0x04002B43 RID: 11075
		private TownModelIO.ModelHandle[] m_ModelHandle;

		// Token: 0x02000731 RID: 1841
		private struct MsbModelHandle
		{
			// Token: 0x04002B44 RID: 11076
			public MsbHandler m_Handle;

			// Token: 0x04002B45 RID: 11077
			public int m_Num;

			// Token: 0x04002B46 RID: 11078
			public Color[] m_Color;
		}

		// Token: 0x02000732 RID: 1842
		private struct ModelHandle
		{
			// Token: 0x04002B47 RID: 11079
			public Renderer m_Handle;
		}
	}
}
