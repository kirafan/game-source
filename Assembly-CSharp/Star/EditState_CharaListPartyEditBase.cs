﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x020003FA RID: 1018
	public class EditState_CharaListPartyEditBase : EditState
	{
		// Token: 0x0600135A RID: 4954 RVA: 0x0006785B File Offset: 0x00065C5B
		public EditState_CharaListPartyEditBase(EditMain owner) : base(owner)
		{
		}

		// Token: 0x0600135B RID: 4955 RVA: 0x00067873 File Offset: 0x00065C73
		public override int GetStateID()
		{
			return 3;
		}

		// Token: 0x0600135C RID: 4956 RVA: 0x00067876 File Offset: 0x00065C76
		public override void OnStateEnter()
		{
			this.m_Step = EditState_CharaListPartyEditBase.eStep.First;
		}

		// Token: 0x0600135D RID: 4957 RVA: 0x0006787F File Offset: 0x00065C7F
		public virtual int GetDefaultNextSceneState()
		{
			return 2;
		}

		// Token: 0x0600135E RID: 4958 RVA: 0x00067882 File Offset: 0x00065C82
		public override void OnStateExit()
		{
		}

		// Token: 0x0600135F RID: 4959 RVA: 0x00067884 File Offset: 0x00065C84
		public override void OnDispose()
		{
		}

		// Token: 0x06001360 RID: 4960 RVA: 0x00067888 File Offset: 0x00065C88
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_CharaListPartyEditBase.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = EditState_CharaListPartyEditBase.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = EditState_CharaListPartyEditBase.eStep.LoadWait;
				}
				break;
			case EditState_CharaListPartyEditBase.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_CharaListPartyEditBase.eStep.PlayIn;
				}
				break;
			case EditState_CharaListPartyEditBase.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_CharaListPartyEditBase.eStep.Main;
				}
				break;
			case EditState_CharaListPartyEditBase.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.OnStateUpdateMainOnClickButton();
						this.m_NextState = this.GetDefaultNextSceneState();
					}
					this.m_Step = EditState_CharaListPartyEditBase.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001361 RID: 4961 RVA: 0x000679A4 File Offset: 0x00065DA4
		protected virtual void OnStateUpdateMainOnClickButton()
		{
		}

		// Token: 0x06001362 RID: 4962 RVA: 0x000679A8 File Offset: 0x00065DA8
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.SetListEditMode();
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharaList);
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x06001363 RID: 4963 RVA: 0x00067A4A File Offset: 0x00065E4A
		protected virtual void SetListEditMode()
		{
			this.m_Owner.CharaListUI.SetPartyEditMode(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex);
		}

		// Token: 0x06001364 RID: 4964 RVA: 0x00067A78 File Offset: 0x00065E78
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (isCallFromShortCut && globalParam.IsDirtyPartyEdit)
			{
				this.m_Owner.Request_BattlePartySetAll(new Action(this.GoToMenuEnd));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001365 RID: 4965 RVA: 0x00067ACA File Offset: 0x00065ECA
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x06001366 RID: 4966 RVA: 0x00067AEC File Offset: 0x00065EEC
		private void OnResponse_BattlePartySetAll(MeigewwwParam wwwParam)
		{
			Battlepartysetall battlepartysetall = PlayerResponse.Battlepartysetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetall == null)
			{
				return;
			}
			ResultCode result = battlepartysetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = false;
				this.GoToMenuEnd();
			}
		}

		// Token: 0x04001A54 RID: 6740
		private EditState_CharaListPartyEditBase.eStep m_Step = EditState_CharaListPartyEditBase.eStep.None;

		// Token: 0x04001A55 RID: 6741
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x020003FB RID: 1019
		private enum eStep
		{
			// Token: 0x04001A57 RID: 6743
			None = -1,
			// Token: 0x04001A58 RID: 6744
			First,
			// Token: 0x04001A59 RID: 6745
			LoadWait,
			// Token: 0x04001A5A RID: 6746
			PlayIn,
			// Token: 0x04001A5B RID: 6747
			Main
		}
	}
}
