﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005CA RID: 1482
	public class RoomObjectControllCreate
	{
		// Token: 0x06001CC3 RID: 7363 RVA: 0x00098B34 File Offset: 0x00096F34
		public static RoomObjectModel CreateObjectModel(GameObject pobj, eRoomObjectCategory category)
		{
			RoomObjectModel result;
			switch (category)
			{
			case eRoomObjectCategory.Chair:
				result = pobj.AddComponent<RoomObjectCtrlChair>();
				break;
			default:
				if (category != eRoomObjectCategory.WallDecoration)
				{
					result = pobj.AddComponent<RoomObjectCtrlBase>();
				}
				else
				{
					result = pobj.AddComponent<RoomObjectCtrlWallDecoration>();
				}
				break;
			case eRoomObjectCategory.Bedding:
				result = pobj.AddComponent<RoomObjectCtrlBedding>();
				break;
			}
			return result;
		}

		// Token: 0x06001CC4 RID: 7364 RVA: 0x00098B94 File Offset: 0x00096F94
		public static RoomObjectSprite CreateObjectSprite(GameObject pobj, eRoomObjectCategory category, int objID)
		{
			return pobj.AddComponent<RoomObjectSprite>();
		}

		// Token: 0x06001CC5 RID: 7365 RVA: 0x00098BAC File Offset: 0x00096FAC
		public static RoomObjectMdlSprite CreateObjectMdlSprite(GameObject pobj, eRoomObjectCategory category, int objID)
		{
			return pobj.AddComponent<RoomObjectMdlSprite>();
		}

		// Token: 0x06001CC6 RID: 7366 RVA: 0x00098BC4 File Offset: 0x00096FC4
		public static IRoomObjectControll CreateCharaObject(GameObject pobj)
		{
			return pobj.AddComponent<RoomObjectCtrlChara>();
		}

		// Token: 0x06001CC7 RID: 7367 RVA: 0x00098BDC File Offset: 0x00096FDC
		public static IRoomObjectControll SetupSimple(GameObject pobj, eRoomObjectCategory category, int objID, int fsubkey, CharacterDefine.eDir dir, int posX, int posY, int defaultSizeX, int defaultSizeY)
		{
			RoomObjectSprite roomObjectSprite = RoomObjectControllCreate.CreateObjectSprite(pobj, category, objID);
			roomObjectSprite.SetupSimple(category, objID, fsubkey, dir, posX, posY, defaultSizeX, defaultSizeY);
			roomObjectSprite.Prepare();
			return roomObjectSprite;
		}

		// Token: 0x06001CC8 RID: 7368 RVA: 0x00098C0C File Offset: 0x0009700C
		public static IRoomObjectControll SetupMdlSimple(GameObject pobj, int fsubkey, int flayerid, int frenderlayer, int fresourceid, long fmanageid, bool finit)
		{
			RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(fresourceid);
			RoomObjectMdlSprite roomObjectMdlSprite = RoomObjectControllCreate.CreateObjectMdlSprite(pobj, (eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
			roomObjectMdlSprite.SetManageID(fmanageid);
			roomObjectMdlSprite.SetupSimple((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID, fsubkey, flayerid, frenderlayer, finit);
			roomObjectMdlSprite.Prepare();
			return roomObjectMdlSprite;
		}

		// Token: 0x06001CC9 RID: 7369 RVA: 0x00098C60 File Offset: 0x00097060
		public static IRoomObjectControll SetupHandle(GameObject pobj, RoomBuilder pbuilder, eRoomObjectCategory category, int objID, CharacterDefine.eDir dir, int posX, int posY, int floorID, long fmanageID)
		{
			RoomObjectModel roomObjectModel = RoomObjectControllCreate.CreateObjectModel(pobj, category);
			roomObjectModel.SetManageID(fmanageID);
			roomObjectModel.SetupObj(category, objID, dir, posX, posY, floorID);
			roomObjectModel.LinkObjectHandle(pbuilder);
			return roomObjectModel;
		}

		// Token: 0x06001CCA RID: 7370 RVA: 0x00098C98 File Offset: 0x00097098
		public static GameObject CreateBGObject()
		{
			return new GameObject("RoomBG")
			{
				transform = 
				{
					localScale = Vector3.one * 2f,
					localPosition = new Vector3(0f, 0f, 900f)
				}
			};
		}
	}
}
