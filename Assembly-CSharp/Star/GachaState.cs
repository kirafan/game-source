﻿using System;

namespace Star
{
	// Token: 0x0200041C RID: 1052
	public class GachaState : GameStateBase
	{
		// Token: 0x06001421 RID: 5153 RVA: 0x0006B6A8 File Offset: 0x00069AA8
		public GachaState(GachaMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001422 RID: 5154 RVA: 0x0006B6BE File Offset: 0x00069ABE
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001423 RID: 5155 RVA: 0x0006B6C1 File Offset: 0x00069AC1
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001424 RID: 5156 RVA: 0x0006B6C3 File Offset: 0x00069AC3
		public override void OnStateExit()
		{
		}

		// Token: 0x06001425 RID: 5157 RVA: 0x0006B6C5 File Offset: 0x00069AC5
		public override void OnDispose()
		{
		}

		// Token: 0x06001426 RID: 5158 RVA: 0x0006B6C7 File Offset: 0x00069AC7
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001427 RID: 5159 RVA: 0x0006B6CA File Offset: 0x00069ACA
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001B19 RID: 6937
		protected GachaMain m_Owner;

		// Token: 0x04001B1A RID: 6938
		protected int m_NextState = -1;
	}
}
