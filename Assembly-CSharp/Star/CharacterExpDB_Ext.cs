﻿using System;

namespace Star
{
	// Token: 0x0200018B RID: 395
	public static class CharacterExpDB_Ext
	{
		// Token: 0x06000ADC RID: 2780 RVA: 0x00040DE8 File Offset: 0x0003F1E8
		public static int GetNextExp(this CharacterExpDB self, int currentLv)
		{
			if (currentLv > self.m_Params.Length)
			{
				return 0;
			}
			return self.m_Params[currentLv - 1].m_NextExp;
		}

		// Token: 0x06000ADD RID: 2781 RVA: 0x00040E10 File Offset: 0x0003F210
		public static long[] GetNextMaxExps(this CharacterExpDB self, int lv_a, int lv_b)
		{
			int num = lv_b - lv_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(lv_a + i);
			}
			return array;
		}

		// Token: 0x06000ADE RID: 2782 RVA: 0x00040E53 File Offset: 0x0003F253
		public static int GetUpgradeAmount(this CharacterExpDB self, int currentLv)
		{
			if (currentLv > self.m_Params.Length)
			{
				return 0;
			}
			return self.m_Params[currentLv - 1].m_UpgradeAmount;
		}
	}
}
