﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000132 RID: 306
	public static class SkillActionUtility
	{
		// Token: 0x060007F0 RID: 2032 RVA: 0x000336E8 File Offset: 0x00031AE8
		public static string ConvertEffectID(string srcEffectID, eElementType elementType, eClassType classType, sbyte motionID, byte grade)
		{
			if (!srcEffectID.Contains("ef_btl_"))
			{
				return srcEffectID;
			}
			if (srcEffectID.Contains("_attack_"))
			{
				string[] array = srcEffectID.Split(new char[]
				{
					'_'
				});
				string arg = array[array.Length - 1];
				return string.Format("ef_btl_{0}_attack_{1}_{2:D2}", SkillActionUtility.ConvertEffectID_class(classType), SkillActionUtility.ConvertEffectID_element(elementType), arg);
			}
			if (srcEffectID.Contains("_skill_"))
			{
				string[] array2 = srcEffectID.Split(new char[]
				{
					'_'
				});
				string text = array2[array2.Length - 1];
				return string.Format("ef_btl_{0}_skill_{1:D2}_{2}_{3:D2}_{4:D2}", new object[]
				{
					SkillActionUtility.ConvertEffectID_class(classType),
					motionID,
					SkillActionUtility.ConvertEffectID_element(elementType),
					grade,
					text
				});
			}
			return srcEffectID;
		}

		// Token: 0x060007F1 RID: 2033 RVA: 0x000337B0 File Offset: 0x00031BB0
		public static string ConvertDmgEffectID(string srcEffectID, eElementType elementType, byte grade, eDmgEffectType dmgEffectType)
		{
			if (srcEffectID.Contains("_dmg_enemy_"))
			{
				return string.Format("ef_btl_dmg_enemy_attack_{0}_{1:D2}", SkillActionUtility.ConvertEffectID_dmgEnemy(dmgEffectType), grade);
			}
			if (!srcEffectID.Contains("_dmg_"))
			{
				return srcEffectID;
			}
			if (srcEffectID.Contains("_single_"))
			{
				return string.Format("ef_btl_dmg_single_{0:D2}", grade);
			}
			return string.Format("ef_btl_dmg_all_{0}_{1:D2}", SkillActionUtility.ConvertEffectID_element(elementType), grade);
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x00033830 File Offset: 0x00031C30
		public static string ConvertEffectID_class(eClassType classType)
		{
			switch (classType)
			{
			case eClassType.Fighter:
				return "fighter";
			case eClassType.Magician:
				return "magician";
			case eClassType.Priest:
				return "priest";
			case eClassType.Knight:
				return "knight";
			case eClassType.Alchemist:
				return "alchemist";
			default:
				return "fighter";
			}
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x00033880 File Offset: 0x00031C80
		public static string ConvertEffectID_element(eElementType elementType)
		{
			switch (elementType)
			{
			case eElementType.Fire:
				return "fire";
			case eElementType.Water:
				return "water";
			case eElementType.Earth:
				return "earth";
			case eElementType.Wind:
				return "wind";
			case eElementType.Moon:
				return "moon";
			case eElementType.Sun:
				return "sun";
			default:
				return "fire";
			}
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x000338D9 File Offset: 0x00031CD9
		public static string ConvertEffectID_dmgEnemy(eDmgEffectType dmgEnemyEffectType)
		{
			switch (dmgEnemyEffectType)
			{
			case eDmgEffectType.EN_Slash:
				return "slash";
			case eDmgEffectType.EN_Blow:
				return "blow";
			case eDmgEffectType.EN_Bite:
				return "bite";
			case eDmgEffectType.EN_Claw:
				return "claw";
			default:
				return "slash";
			}
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x00033918 File Offset: 0x00031D18
		public static void CorrectEffectParam(Camera camera, string effectID, ref Vector3 ref_pos, ref int ref_sortingOrder)
		{
			Transform transform = camera.transform;
			Vector3 vector = transform.TransformDirection(Vector3.forward);
			if (effectID.Contains("_dmg_"))
			{
				ref_pos.z = transform.position.z + vector.z * 10f + vector.z * camera.nearClipPlane;
				ref_sortingOrder = 1500;
			}
			else
			{
				ref_pos.z = transform.position.z + vector.z * 20f + vector.z * camera.nearClipPlane;
				ref_sortingOrder = 1000;
			}
		}

		// Token: 0x040007B8 RID: 1976
		public const string EFFECT_ID_FORMAT_ATTACK = "ef_btl_{0}_attack_{1}_{2:D2}";

		// Token: 0x040007B9 RID: 1977
		public const string EFFECT_ID_FORMAT_SKILL = "ef_btl_{0}_skill_{1:D2}_{2}_{3:D2}_{4:D2}";

		// Token: 0x040007BA RID: 1978
		public const string EFFECT_ID_FORMAT_DAMAGE_SINGLE = "ef_btl_dmg_single_{0:D2}";

		// Token: 0x040007BB RID: 1979
		public const string EFFECT_ID_FORMAT_DAMAGE_ALL = "ef_btl_dmg_all_{0}_{1:D2}";

		// Token: 0x040007BC RID: 1980
		public const string EFFECT_ID_FORMAT_DAMAGE_EN = "ef_btl_dmg_enemy_attack_{0}_{1:D2}";

		// Token: 0x040007BD RID: 1981
		public const string EFFECT_DMG_EN_KEY = "_dmg_enemy_";

		// Token: 0x040007BE RID: 1982
		public const string EFFECT_DMG_KEY = "_dmg_";
	}
}
