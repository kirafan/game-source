﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using MsgPack;
using UnityEngine;

namespace Star
{
	// Token: 0x0200064D RID: 1613
	public class LocalSaveData
	{
		// Token: 0x0600206B RID: 8299 RVA: 0x000AD823 File Offset: 0x000ABC23
		protected LocalSaveData()
		{
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x0600206C RID: 8300 RVA: 0x000AD83E File Offset: 0x000ABC3E
		public static LocalSaveData Inst
		{
			get
			{
				return LocalSaveData.instance;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x0600206D RID: 8301 RVA: 0x000AD845 File Offset: 0x000ABC45
		// (set) Token: 0x0600206E RID: 8302 RVA: 0x000AD84D File Offset: 0x000ABC4D
		public bool SaveDataAutoDelete { get; set; }

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x0600206F RID: 8303 RVA: 0x000AD856 File Offset: 0x000ABC56
		// (set) Token: 0x06002070 RID: 8304 RVA: 0x000AD863 File Offset: 0x000ABC63
		public float BgmVolume
		{
			get
			{
				return this.m_SaveData.m_BgmVolume;
			}
			set
			{
				this.m_SaveData.m_BgmVolume = value;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06002071 RID: 8305 RVA: 0x000AD871 File Offset: 0x000ABC71
		// (set) Token: 0x06002072 RID: 8306 RVA: 0x000AD87E File Offset: 0x000ABC7E
		public float SeVolume
		{
			get
			{
				return this.m_SaveData.m_SeVolume;
			}
			set
			{
				this.m_SaveData.m_SeVolume = value;
			}
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06002073 RID: 8307 RVA: 0x000AD88C File Offset: 0x000ABC8C
		// (set) Token: 0x06002074 RID: 8308 RVA: 0x000AD899 File Offset: 0x000ABC99
		public float VoiceVolume
		{
			get
			{
				return this.m_SaveData.m_VoiceVolume;
			}
			set
			{
				this.m_SaveData.m_VoiceVolume = value;
			}
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x06002075 RID: 8309 RVA: 0x000AD8A7 File Offset: 0x000ABCA7
		// (set) Token: 0x06002076 RID: 8310 RVA: 0x000AD8B4 File Offset: 0x000ABCB4
		public LocalSaveData.eAmount BattleEffect
		{
			get
			{
				return this.m_SaveData.m_BattleEffect;
			}
			set
			{
				this.m_SaveData.m_BattleEffect = value;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06002077 RID: 8311 RVA: 0x000AD8C2 File Offset: 0x000ABCC2
		// (set) Token: 0x06002078 RID: 8312 RVA: 0x000AD8CF File Offset: 0x000ABCCF
		public bool BattleAutoMode
		{
			get
			{
				return this.m_SaveData.m_BattleAutoMode;
			}
			set
			{
				this.m_SaveData.m_BattleAutoMode = value;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06002079 RID: 8313 RVA: 0x000AD8DD File Offset: 0x000ABCDD
		// (set) Token: 0x0600207A RID: 8314 RVA: 0x000AD8EA File Offset: 0x000ABCEA
		public bool BattleSkipTogetherAttackInput
		{
			get
			{
				return this.m_SaveData.m_BattleSkipTogetherAttackInput;
			}
			set
			{
				this.m_SaveData.m_BattleSkipTogetherAttackInput = value;
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x0600207B RID: 8315 RVA: 0x000AD8F8 File Offset: 0x000ABCF8
		// (set) Token: 0x0600207C RID: 8316 RVA: 0x000AD905 File Offset: 0x000ABD05
		public bool BattleSkipUniqueSkill
		{
			get
			{
				return this.m_SaveData.m_BattleSkipUniqueSkill;
			}
			set
			{
				this.m_SaveData.m_BattleSkipUniqueSkill = value;
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x0600207D RID: 8317 RVA: 0x000AD913 File Offset: 0x000ABD13
		// (set) Token: 0x0600207E RID: 8318 RVA: 0x000AD920 File Offset: 0x000ABD20
		public bool ADVSkipOnBattle
		{
			get
			{
				return this.m_SaveData.m_ADVSkipOnBattle;
			}
			set
			{
				this.m_SaveData.m_ADVSkipOnBattle = value;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x0600207F RID: 8319 RVA: 0x000AD92E File Offset: 0x000ABD2E
		// (set) Token: 0x06002080 RID: 8320 RVA: 0x000AD93B File Offset: 0x000ABD3B
		public bool TownSkipGreet
		{
			get
			{
				return this.m_SaveData.m_RoomSkipGreet;
			}
			set
			{
				this.m_SaveData.m_RoomSkipGreet = value;
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06002081 RID: 8321 RVA: 0x000AD949 File Offset: 0x000ABD49
		// (set) Token: 0x06002082 RID: 8322 RVA: 0x000AD956 File Offset: 0x000ABD56
		public string StoreReviewAppVersion
		{
			get
			{
				return this.m_SaveData.m_StoreReviewAppVersion;
			}
			set
			{
				this.m_SaveData.m_StoreReviewAppVersion = value;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06002083 RID: 8323 RVA: 0x000AD964 File Offset: 0x000ABD64
		// (set) Token: 0x06002084 RID: 8324 RVA: 0x000AD971 File Offset: 0x000ABD71
		public int PlayedOpenChapterId
		{
			get
			{
				return this.m_SaveData.m_PlayedOpenChapterId;
			}
			set
			{
				this.m_SaveData.m_PlayedOpenChapterId = value;
			}
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06002085 RID: 8325 RVA: 0x000AD97F File Offset: 0x000ABD7F
		// (set) Token: 0x06002086 RID: 8326 RVA: 0x000AD98C File Offset: 0x000ABD8C
		public int SelectedPartyIndex
		{
			get
			{
				return this.m_SaveData.m_SelectedPartyIndex;
			}
			set
			{
				this.m_SaveData.m_SelectedPartyIndex = value;
			}
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06002087 RID: 8327 RVA: 0x000AD99A File Offset: 0x000ABD9A
		// (set) Token: 0x06002088 RID: 8328 RVA: 0x000AD9A7 File Offset: 0x000ABDA7
		public int RetireTipsIdx
		{
			get
			{
				return this.m_SaveData.m_RetireTipsIdx;
			}
			set
			{
				this.m_SaveData.m_RetireTipsIdx = value;
			}
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06002089 RID: 8329 RVA: 0x000AD9B5 File Offset: 0x000ABDB5
		// (set) Token: 0x0600208A RID: 8330 RVA: 0x000AD9C2 File Offset: 0x000ABDC2
		public bool OccurredTrainingPrepare
		{
			get
			{
				return this.m_SaveData.m_OccurredTrainingPrepare;
			}
			set
			{
				this.m_SaveData.m_OccurredTrainingPrepare = value;
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x0600208B RID: 8331 RVA: 0x000AD9D0 File Offset: 0x000ABDD0
		// (set) Token: 0x0600208C RID: 8332 RVA: 0x000AD9DD File Offset: 0x000ABDDD
		public bool Agreement
		{
			get
			{
				return this.m_SaveData.m_Agreement;
			}
			set
			{
				this.m_SaveData.m_Agreement = value;
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x0600208D RID: 8333 RVA: 0x000AD9EB File Offset: 0x000ABDEB
		// (set) Token: 0x0600208E RID: 8334 RVA: 0x000AD9F8 File Offset: 0x000ABDF8
		public List<LocalSaveData.UISortParam> UISortParams
		{
			get
			{
				return this.m_SaveData.m_UISortParams;
			}
			set
			{
				this.m_SaveData.m_UISortParams = value;
			}
		}

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x0600208F RID: 8335 RVA: 0x000ADA06 File Offset: 0x000ABE06
		// (set) Token: 0x06002090 RID: 8336 RVA: 0x000ADA13 File Offset: 0x000ABE13
		public List<LocalSaveData.UIFilterParam> UIFilterParams
		{
			get
			{
				return this.m_SaveData.m_UIFilterParams;
			}
			set
			{
				this.m_SaveData.m_UIFilterParams = value;
			}
		}

		// Token: 0x06002091 RID: 8337 RVA: 0x000ADA21 File Offset: 0x000ABE21
		public float GetBattleSpeedScale(LocalSaveData.eBattleSpeedLv type)
		{
			return LocalSaveData.BATTLE_SPEED_LV_SCALE[this.GetBattleSpeedLv(type)];
		}

		// Token: 0x06002092 RID: 8338 RVA: 0x000ADA30 File Offset: 0x000ABE30
		public int GetBattleSpeedLv(LocalSaveData.eBattleSpeedLv type)
		{
			return this.m_SaveData.m_BattleSpeedLvs[(int)type];
		}

		// Token: 0x06002093 RID: 8339 RVA: 0x000ADA3F File Offset: 0x000ABE3F
		public void SetBattleSpeedLv(LocalSaveData.eBattleSpeedLv type, int lv)
		{
			lv %= 2;
			this.m_SaveData.m_BattleSpeedLvs[(int)type] = lv;
		}

		// Token: 0x06002094 RID: 8340 RVA: 0x000ADA54 File Offset: 0x000ABE54
		public int ToggleBattleSpeedLv(LocalSaveData.eBattleSpeedLv type)
		{
			int lv = (this.m_SaveData.m_BattleSpeedLvs[(int)type] + 1) % 2;
			this.SetBattleSpeedLv(type, lv);
			return this.m_SaveData.m_BattleSpeedLvs[(int)type];
		}

		// Token: 0x06002095 RID: 8341 RVA: 0x000ADA88 File Offset: 0x000ABE88
		public void Save()
		{
			ObjectPacker objectPacker = new ObjectPacker();
			byte[] src = objectPacker.Pack(this.m_SaveData);
			if (File.Exists(LocalSaveData.SaveFile))
			{
				File.Copy(LocalSaveData.SaveFile, LocalSaveData.SaveFileBak, true);
			}
			try
			{
				string s;
				byte[] array;
				LocalSaveData.EncryptAes(src, LocalSaveData.EncryptKey, LocalSaveData.EncryptPasswordCount, out s, out array);
				using (FileStream fileStream = new FileStream(LocalSaveData.SaveFile, FileMode.Create, FileAccess.Write))
				{
					using (BinaryWriter binaryWriter = new BinaryWriter(fileStream))
					{
						int num = UnityEngine.Random.Range(-2147483647, int.MaxValue);
						byte b = (byte)(num & 127);
						num = ((int)((long)num & (long)((ulong)-65281)) | (65280 & (int)((short)this.m_LatestVersion) << 8));
						binaryWriter.Write(num);
						byte[] bytes = Encoding.UTF8.GetBytes(s);
						for (int i = 0; i < bytes.Length; i++)
						{
							byte[] array2 = bytes;
							int num2 = i;
							array2[num2] += 96 + (byte)i;
						}
						binaryWriter.Write((byte)(bytes.Length + (int)b));
						binaryWriter.Write(bytes);
						binaryWriter.Write(array.Length);
						binaryWriter.Write(array);
					}
				}
			}
			catch (Exception ex)
			{
			}
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x000ADBF4 File Offset: 0x000ABFF4
		public void Load()
		{
			this.Load(LocalSaveData.SaveFile);
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x000ADC04 File Offset: 0x000AC004
		public bool Load(string path)
		{
			if (!File.Exists(path))
			{
				return false;
			}
			string pw = null;
			byte[] array = null;
			bool flag = false;
			try
			{
				using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
				{
					using (BinaryReader binaryReader = new BinaryReader(fileStream))
					{
						int num = binaryReader.ReadInt32();
						byte b = (byte)(num & 127);
						int num2 = (num & 65280) >> 8;
						if (num2 != (int)this.m_LatestVersion)
						{
							flag = true;
						}
						else
						{
							int count = (int)(binaryReader.ReadByte() - b);
							array = binaryReader.ReadBytes(count);
							for (int i = 0; i < array.Length; i++)
							{
								byte[] array2 = array;
								int num3 = i;
								array2[num3] -= 96 + (byte)i;
							}
							pw = Encoding.UTF8.GetString(array);
							count = binaryReader.ReadInt32();
							array = binaryReader.ReadBytes(count);
						}
					}
					if (!flag)
					{
						ObjectPacker objectPacker = new ObjectPacker();
						byte[] buf;
						LocalSaveData.DecryptAes(array, LocalSaveData.EncryptKey, pw, out buf);
						this.m_SaveData = objectPacker.Unpack<LocalSaveData.SaveData>(buf);
					}
				}
			}
			catch
			{
				bool flag2 = false;
				if (LocalSaveData.SaveFileBak != path)
				{
					flag2 = this.Load(LocalSaveData.SaveFileBak);
				}
				if (!flag2)
				{
					return false;
				}
			}
			if (flag)
			{
				this.DeleteAllSaveData();
				this.SaveDataAutoDelete = true;
				return true;
			}
			this.AfterLoad();
			return true;
		}

		// Token: 0x06002098 RID: 8344 RVA: 0x000ADD9C File Offset: 0x000AC19C
		public void DeleteAllSaveData()
		{
			this.DeleteSaveData(LocalSaveData.SaveFile);
			this.DeleteSaveData(LocalSaveData.SaveFileBak);
		}

		// Token: 0x06002099 RID: 8345 RVA: 0x000ADDB4 File Offset: 0x000AC1B4
		public void DeleteSaveData(string path)
		{
			if (File.Exists(path))
			{
				File.Delete(path);
			}
		}

		// Token: 0x0600209A RID: 8346 RVA: 0x000ADDC7 File Offset: 0x000AC1C7
		public void ResetSave()
		{
			this.m_SaveData.Reset();
			this.Save();
		}

		// Token: 0x0600209B RID: 8347 RVA: 0x000ADDDA File Offset: 0x000AC1DA
		private void AfterLoad()
		{
		}

		// Token: 0x0600209C RID: 8348 RVA: 0x000ADDDC File Offset: 0x000AC1DC
		private static void EncryptAes(byte[] src, string encryptKey, int pwSize, out string pw, out byte[] dst)
		{
			pw = LocalSaveData.CreatePassword(pwSize);
			dst = null;
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				byte[] bytes = Encoding.UTF8.GetBytes(encryptKey);
				byte[] bytes2 = Encoding.UTF8.GetBytes(pw);
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(bytes, bytes2))
				{
					using (MemoryStream memoryStream = new MemoryStream())
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Write))
						{
							cryptoStream.Write(src, 0, src.Length);
							cryptoStream.FlushFinalBlock();
							dst = memoryStream.ToArray();
						}
					}
				}
			}
		}

		// Token: 0x0600209D RID: 8349 RVA: 0x000ADEF0 File Offset: 0x000AC2F0
		private static void DecryptAes(byte[] src, string encryptKey, string pw, out byte[] dst)
		{
			dst = new byte[src.Length];
			using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
			{
				rijndaelManaged.Padding = PaddingMode.PKCS7;
				rijndaelManaged.Mode = CipherMode.CBC;
				rijndaelManaged.KeySize = 256;
				rijndaelManaged.BlockSize = 128;
				byte[] bytes = Encoding.UTF8.GetBytes(encryptKey);
				byte[] bytes2 = Encoding.UTF8.GetBytes(pw);
				using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(bytes, bytes2))
				{
					using (MemoryStream memoryStream = new MemoryStream(src))
					{
						using (CryptoStream cryptoStream = new CryptoStream(memoryStream, cryptoTransform, CryptoStreamMode.Read))
						{
							cryptoStream.Read(dst, 0, dst.Length);
						}
					}
				}
			}
		}

		// Token: 0x0600209E RID: 8350 RVA: 0x000ADFF4 File Offset: 0x000AC3F4
		private static string CreatePassword(int count)
		{
			StringBuilder stringBuilder = new StringBuilder(count);
			for (int i = count - 1; i >= 0; i--)
			{
				char value = LocalSaveData.PasswordChars[UnityEngine.Random.Range(0, LocalSaveData.PasswordChars.Length)];
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040026C4 RID: 9924
		private static readonly LocalSaveData instance = new LocalSaveData();

		// Token: 0x040026C5 RID: 9925
		private static readonly string EncryptKey = "3xgLmcc62dNLJ3Te6phdzf2TFj85yhxR";

		// Token: 0x040026C6 RID: 9926
		private static readonly int EncryptPasswordCount = 16;

		// Token: 0x040026C7 RID: 9927
		private static readonly string FilePrefix = string.Empty;

		// Token: 0x040026C8 RID: 9928
		private static readonly string SaveFile = Application.persistentDataPath + "/" + LocalSaveData.FilePrefix + "s.d";

		// Token: 0x040026C9 RID: 9929
		private static readonly string SaveFileBak = Application.persistentDataPath + "/" + LocalSaveData.FilePrefix + "s.d2";

		// Token: 0x040026CA RID: 9930
		private readonly LocalSaveData.SaveDataVersion m_LatestVersion = LocalSaveData.SaveDataVersion._171130;

		// Token: 0x040026CB RID: 9931
		public const int BATTLE_SPEED_LV_NUM = 2;

		// Token: 0x040026CC RID: 9932
		public static float[] BATTLE_SPEED_LV_SCALE = new float[]
		{
			1f,
			1.8f
		};

		// Token: 0x040026CD RID: 9933
		private LocalSaveData.SaveData m_SaveData = new LocalSaveData.SaveData();

		// Token: 0x040026CF RID: 9935
		private static readonly string PasswordChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		// Token: 0x0200064E RID: 1614
		public enum SaveDataVersion
		{
			// Token: 0x040026D1 RID: 9937
			_170203 = 17,
			// Token: 0x040026D2 RID: 9938
			_170209,
			// Token: 0x040026D3 RID: 9939
			_170307,
			// Token: 0x040026D4 RID: 9940
			_170311,
			// Token: 0x040026D5 RID: 9941
			_170331,
			// Token: 0x040026D6 RID: 9942
			_170404,
			// Token: 0x040026D7 RID: 9943
			_170420,
			// Token: 0x040026D8 RID: 9944
			_170608,
			// Token: 0x040026D9 RID: 9945
			_170805,
			// Token: 0x040026DA RID: 9946
			_170905,
			// Token: 0x040026DB RID: 9947
			_170906,
			// Token: 0x040026DC RID: 9948
			_170914,
			// Token: 0x040026DD RID: 9949
			_171027,
			// Token: 0x040026DE RID: 9950
			_171028,
			// Token: 0x040026DF RID: 9951
			_171111,
			// Token: 0x040026E0 RID: 9952
			_171122,
			// Token: 0x040026E1 RID: 9953
			_171130,
			// Token: 0x040026E2 RID: 9954
			_latest
		}

		// Token: 0x0200064F RID: 1615
		public enum eAmount
		{
			// Token: 0x040026E4 RID: 9956
			High,
			// Token: 0x040026E5 RID: 9957
			Middle,
			// Token: 0x040026E6 RID: 9958
			Low
		}

		// Token: 0x02000650 RID: 1616
		public enum eBattleSpeedLv
		{
			// Token: 0x040026E8 RID: 9960
			PL,
			// Token: 0x040026E9 RID: 9961
			EN,
			// Token: 0x040026EA RID: 9962
			UniqueSkill,
			// Token: 0x040026EB RID: 9963
			Num
		}

		// Token: 0x02000651 RID: 1617
		public class UISortParam
		{
			// Token: 0x060020A0 RID: 8352 RVA: 0x000AE0DB File Offset: 0x000AC4DB
			public UISortParam()
			{
				this.m_Value = 0;
				this.m_OrderType = 0;
			}

			// Token: 0x060020A1 RID: 8353 RVA: 0x000AE0F1 File Offset: 0x000AC4F1
			public void Clear()
			{
				this.m_Value = 0;
				this.m_OrderType = 0;
			}

			// Token: 0x040026EC RID: 9964
			public ushort m_Value;

			// Token: 0x040026ED RID: 9965
			public ushort m_OrderType;
		}

		// Token: 0x02000652 RID: 1618
		public class UIFilterParam
		{
			// Token: 0x060020A2 RID: 8354 RVA: 0x000AE104 File Offset: 0x000AC504
			public UIFilterParam(long flg)
			{
				this.m_BitFlgs = new long[12];
				for (int i = 0; i < this.m_BitFlgs.Length; i++)
				{
					this.m_BitFlgs[i] = flg;
				}
			}

			// Token: 0x060020A3 RID: 8355 RVA: 0x000AE148 File Offset: 0x000AC548
			public UIFilterParam()
			{
				this.m_BitFlgs = new long[12];
				for (int i = 0; i < this.m_BitFlgs.Length; i++)
				{
					this.m_BitFlgs[i] = 0L;
				}
			}

			// Token: 0x060020A4 RID: 8356 RVA: 0x000AE18C File Offset: 0x000AC58C
			public void Clear()
			{
				for (int i = 0; i < this.m_BitFlgs.Length; i++)
				{
					this.m_BitFlgs[i] = 0L;
				}
			}

			// Token: 0x060020A5 RID: 8357 RVA: 0x000AE1BC File Offset: 0x000AC5BC
			public bool HasFlag(int typeIndex, int bitIndex)
			{
				return typeIndex < 0 || typeIndex >= this.m_BitFlgs.Length || (this.m_BitFlgs[typeIndex] & 1L << (bitIndex & 31)) != 0L;
			}

			// Token: 0x060020A6 RID: 8358 RVA: 0x000AE1F0 File Offset: 0x000AC5F0
			public void SetFlag(int typeIndex, int bitIndex, bool isOn)
			{
				if (typeIndex >= 0 && typeIndex < this.m_BitFlgs.Length)
				{
					if (isOn)
					{
						this.m_BitFlgs[typeIndex] |= (long)(1UL << (bitIndex & 31));
					}
					else
					{
						this.m_BitFlgs[typeIndex] &= ~(1L << (bitIndex & 31));
					}
				}
			}

			// Token: 0x060020A7 RID: 8359 RVA: 0x000AE24C File Offset: 0x000AC64C
			public bool ToggleFlg(int typeIndex, int bitIndex)
			{
				this.SetFlag(typeIndex, bitIndex, !this.HasFlag(typeIndex, bitIndex));
				return this.HasFlag(typeIndex, bitIndex);
			}

			// Token: 0x060020A8 RID: 8360 RVA: 0x000AE269 File Offset: 0x000AC669
			public int GetTypeNum()
			{
				return this.m_BitFlgs.Length;
			}

			// Token: 0x040026EE RID: 9966
			public const int FILTER_TYPE_MAX = 12;

			// Token: 0x040026EF RID: 9967
			private long[] m_BitFlgs;
		}

		// Token: 0x02000653 RID: 1619
		public class SaveData
		{
			// Token: 0x060020A9 RID: 8361 RVA: 0x000AE274 File Offset: 0x000AC674
			public SaveData()
			{
				this.Reset();
			}

			// Token: 0x060020AA RID: 8362 RVA: 0x000AE2DC File Offset: 0x000AC6DC
			public void Reset()
			{
				this.m_BgmVolume = 1f;
				this.m_SeVolume = 1f;
				this.m_VoiceVolume = 1f;
				this.m_BattleEffect = LocalSaveData.eAmount.High;
				this.m_BattleAutoMode = false;
				for (int i = 0; i < this.m_BattleSpeedLvs.Length; i++)
				{
					this.m_BattleSpeedLvs[i] = 0;
				}
				this.m_BattleSkipTogetherAttackInput = true;
				this.m_BattleSkipUniqueSkill = false;
				this.m_ADVSkipOnBattle = true;
				this.m_RoomSkipGreet = false;
				this.m_PlayedOpenChapterId = 0;
				this.m_SelectedPartyIndex = 0;
				this.m_RetireTipsIdx = -1;
				this.m_OccurredTrainingPrepare = false;
				this.m_Agreement = false;
			}

			// Token: 0x040026F0 RID: 9968
			public float m_BgmVolume = 1f;

			// Token: 0x040026F1 RID: 9969
			public float m_SeVolume = 1f;

			// Token: 0x040026F2 RID: 9970
			public float m_VoiceVolume = 1f;

			// Token: 0x040026F3 RID: 9971
			public LocalSaveData.eAmount m_BattleEffect;

			// Token: 0x040026F4 RID: 9972
			public int[] m_BattleSpeedLvs = new int[3];

			// Token: 0x040026F5 RID: 9973
			public bool m_BattleAutoMode;

			// Token: 0x040026F6 RID: 9974
			public bool m_BattleSkipTogetherAttackInput;

			// Token: 0x040026F7 RID: 9975
			public bool m_BattleSkipUniqueSkill;

			// Token: 0x040026F8 RID: 9976
			public bool m_ADVSkipOnBattle;

			// Token: 0x040026F9 RID: 9977
			public bool m_RoomSkipGreet;

			// Token: 0x040026FA RID: 9978
			public string m_StoreReviewAppVersion = "0.0.0";

			// Token: 0x040026FB RID: 9979
			public int m_PlayedOpenChapterId;

			// Token: 0x040026FC RID: 9980
			public int m_SelectedPartyIndex;

			// Token: 0x040026FD RID: 9981
			public int m_RetireTipsIdx = -1;

			// Token: 0x040026FE RID: 9982
			public bool m_OccurredTrainingPrepare;

			// Token: 0x040026FF RID: 9983
			public bool m_Agreement;

			// Token: 0x04002700 RID: 9984
			public byte[] reserve = new byte[96];

			// Token: 0x04002701 RID: 9985
			public List<LocalSaveData.UISortParam> m_UISortParams;

			// Token: 0x04002702 RID: 9986
			public List<LocalSaveData.UIFilterParam> m_UIFilterParams;
		}
	}
}
