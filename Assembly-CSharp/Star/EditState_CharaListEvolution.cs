﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020003F5 RID: 1013
	public class EditState_CharaListEvolution : EditState
	{
		// Token: 0x06001344 RID: 4932 RVA: 0x0006739A File Offset: 0x0006579A
		public EditState_CharaListEvolution(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001345 RID: 4933 RVA: 0x000673B2 File Offset: 0x000657B2
		public override int GetStateID()
		{
			return 14;
		}

		// Token: 0x06001346 RID: 4934 RVA: 0x000673B6 File Offset: 0x000657B6
		public override void OnStateEnter()
		{
			this.m_Step = EditState_CharaListEvolution.eStep.First;
		}

		// Token: 0x06001347 RID: 4935 RVA: 0x000673BF File Offset: 0x000657BF
		public override void OnStateExit()
		{
		}

		// Token: 0x06001348 RID: 4936 RVA: 0x000673C1 File Offset: 0x000657C1
		public override void OnDispose()
		{
		}

		// Token: 0x06001349 RID: 4937 RVA: 0x000673C4 File Offset: 0x000657C4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_CharaListEvolution.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = EditState_CharaListEvolution.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = EditState_CharaListEvolution.eStep.LoadWait;
				}
				break;
			case EditState_CharaListEvolution.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_CharaListEvolution.eStep.PlayIn;
				}
				break;
			case EditState_CharaListEvolution.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_CHARA_EVOLUTION, null, null);
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, true);
					this.m_Step = EditState_CharaListEvolution.eStep.Main;
				}
				break;
			case EditState_CharaListEvolution.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
						if (selectButton != CharaListUI.eButton.Chara)
						{
							this.m_NextState = 1;
						}
						else
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID = this.m_Owner.CharaListUI.SelectCharaMngID;
							this.m_NextState = 15;
						}
					}
					this.m_Step = EditState_CharaListEvolution.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600134A RID: 4938 RVA: 0x00067540 File Offset: 0x00065940
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.m_Owner.CharaListUI.SetEvolutionMode();
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x0600134B RID: 4939 RVA: 0x000675DA File Offset: 0x000659DA
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x0600134C RID: 4940 RVA: 0x000675E9 File Offset: 0x000659E9
		private void GoToMenuEnd()
		{
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x04001A44 RID: 6724
		private EditState_CharaListEvolution.eStep m_Step = EditState_CharaListEvolution.eStep.None;

		// Token: 0x04001A45 RID: 6725
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x020003F6 RID: 1014
		private enum eStep
		{
			// Token: 0x04001A47 RID: 6727
			None = -1,
			// Token: 0x04001A48 RID: 6728
			First,
			// Token: 0x04001A49 RID: 6729
			LoadWait,
			// Token: 0x04001A4A RID: 6730
			PlayIn,
			// Token: 0x04001A4B RID: 6731
			Main
		}
	}
}
