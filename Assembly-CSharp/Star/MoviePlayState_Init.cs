﻿using System;
using Star.UI;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x0200044D RID: 1101
	public class MoviePlayState_Init : MoviePlayState
	{
		// Token: 0x06001540 RID: 5440 RVA: 0x0006F053 File Offset: 0x0006D453
		public MoviePlayState_Init(MoviePlayMain owner) : base(owner)
		{
		}

		// Token: 0x06001541 RID: 5441 RVA: 0x0006F063 File Offset: 0x0006D463
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001542 RID: 5442 RVA: 0x0006F066 File Offset: 0x0006D466
		public override void OnStateEnter()
		{
			this.m_Step = MoviePlayState_Init.eStep.First;
		}

		// Token: 0x06001543 RID: 5443 RVA: 0x0006F06F File Offset: 0x0006D46F
		public override void OnStateExit()
		{
		}

		// Token: 0x06001544 RID: 5444 RVA: 0x0006F071 File Offset: 0x0006D471
		public override void OnDispose()
		{
		}

		// Token: 0x06001545 RID: 5445 RVA: 0x0006F074 File Offset: 0x0006D474
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MoviePlayState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
					this.m_Owner.MoviePlayUI.Setup(MoviePlayUI.eSkipMode.Disable);
					this.m_Owner.MoviePlayFileName = inst.GlobalParam.MoviePlayFileNameWithoutExt;
					if (string.IsNullOrEmpty(this.m_Owner.MoviePlayFileName))
					{
						this.m_Owner.MoviePlayFileName = "mv_prologue";
					}
					CRIFileInstaller crifileInstaller = inst.CRIFileInstaller;
					if (crifileInstaller.CheckVersionUpdate(inst.DbMng.CRIFileVersionDB, this.m_Owner.MoviePlayFileName))
					{
						this.m_Step = MoviePlayState_Init.eStep.DownloadCRI;
					}
					else
					{
						this.m_Step = MoviePlayState_Init.eStep.Transit;
					}
				}
				break;
			case MoviePlayState_Init.eStep.DownloadCRI:
				Screen.sleepTimeout = -1;
				SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller.Install(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CRIFileVersionDB, false, this.m_Owner.MoviePlayFileName);
				this.m_Step = MoviePlayState_Init.eStep.DownloadCRI_Wait;
				break;
			case MoviePlayState_Init.eStep.DownloadCRI_Wait:
			{
				CRIFileInstaller crifileInstaller2 = SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller;
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.UpdateProgress(crifileInstaller2.Ratio);
				if (!crifileInstaller2.IsInstalling)
				{
					if (!this.m_IsErrorWindow)
					{
						if (crifileInstaller2.IsError)
						{
							string textMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErr);
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErrTitle), textMessage, null, new Action<int>(this.OnConfirmCRIDownloadError));
							this.m_IsErrorWindow = true;
						}
						else
						{
							this.m_Step = MoviePlayState_Init.eStep.Transit;
						}
					}
				}
				break;
			}
			case MoviePlayState_Init.eStep.Transit:
				Screen.sleepTimeout = -2;
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
				this.m_Step = MoviePlayState_Init.eStep.Transit_Wait;
				break;
			case MoviePlayState_Init.eStep.Transit_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = MoviePlayState_Init.eStep.End;
				}
				break;
			case MoviePlayState_Init.eStep.End:
				this.m_Step = MoviePlayState_Init.eStep.None;
				return 1;
			}
			return -1;
		}

		// Token: 0x06001546 RID: 5446 RVA: 0x0006F298 File Offset: 0x0006D698
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x06001547 RID: 5447 RVA: 0x0006F2A1 File Offset: 0x0006D6A1
		private void OnConfirmCRIDownloadError(int answer)
		{
			this.m_IsErrorWindow = false;
			SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller.RetryInstall();
		}

		// Token: 0x04001C19 RID: 7193
		private MoviePlayState_Init.eStep m_Step = MoviePlayState_Init.eStep.None;

		// Token: 0x04001C1A RID: 7194
		private bool m_IsErrorWindow;

		// Token: 0x0200044E RID: 1102
		private enum eStep
		{
			// Token: 0x04001C1C RID: 7196
			None = -1,
			// Token: 0x04001C1D RID: 7197
			First,
			// Token: 0x04001C1E RID: 7198
			DownloadCRI,
			// Token: 0x04001C1F RID: 7199
			DownloadCRI_Wait,
			// Token: 0x04001C20 RID: 7200
			Transit,
			// Token: 0x04001C21 RID: 7201
			Transit_Wait,
			// Token: 0x04001C22 RID: 7202
			End
		}
	}
}
