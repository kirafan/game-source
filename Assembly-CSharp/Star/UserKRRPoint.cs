﻿using System;

namespace Star
{
	// Token: 0x02000B40 RID: 2880
	public class UserKRRPoint
	{
		// Token: 0x06003CF0 RID: 15600 RVA: 0x00135EC5 File Offset: 0x001342C5
		public long GetPoint()
		{
			return this.m_Point;
		}

		// Token: 0x06003CF1 RID: 15601 RVA: 0x00135ECD File Offset: 0x001342CD
		public void SetPoint(long value)
		{
			this.m_Point = value;
		}

		// Token: 0x06003CF2 RID: 15602 RVA: 0x00135ED6 File Offset: 0x001342D6
		public void AddPoint(int value)
		{
			this.m_Point += (long)value;
		}

		// Token: 0x06003CF3 RID: 15603 RVA: 0x00135EE7 File Offset: 0x001342E7
		public long GetLimit()
		{
			return this.m_Limit;
		}

		// Token: 0x06003CF4 RID: 15604 RVA: 0x00135EEF File Offset: 0x001342EF
		public void SetLimit(long value)
		{
			this.m_Limit = value;
		}

		// Token: 0x0400447C RID: 17532
		private long m_Point;

		// Token: 0x0400447D RID: 17533
		private long m_Limit = 1000000L;
	}
}
