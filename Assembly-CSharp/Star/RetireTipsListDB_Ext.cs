﻿using System;

namespace Star
{
	// Token: 0x020001A3 RID: 419
	public static class RetireTipsListDB_Ext
	{
		// Token: 0x06000B0E RID: 2830 RVA: 0x00041D78 File Offset: 0x00040178
		public static RetireTipsListDB_Param GetParam(this RetireTipsListDB self, eRetireTipsListDB id)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)id)
				{
					return self.m_Params[i];
				}
			}
			return default(RetireTipsListDB_Param);
		}
	}
}
