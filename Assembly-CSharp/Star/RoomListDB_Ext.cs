﻿using System;

namespace Star
{
	// Token: 0x020001A4 RID: 420
	public static class RoomListDB_Ext
	{
		// Token: 0x06000B0F RID: 2831 RVA: 0x00041DD0 File Offset: 0x000401D0
		public static RoomListDB_Param GetParam(this RoomListDB self, int roomID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == roomID)
				{
					return self.m_Params[i];
				}
			}
			return default(RoomListDB_Param);
		}
	}
}
