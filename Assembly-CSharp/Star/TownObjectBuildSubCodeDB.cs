﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000248 RID: 584
	public class TownObjectBuildSubCodeDB : ScriptableObject
	{
		// Token: 0x0400133A RID: 4922
		public TownObjectBuildSubCodeDB_Param[] m_Params;
	}
}
