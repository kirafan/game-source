﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200045F RID: 1119
	public class QuestState_QuestChapterSelect : QuestState
	{
		// Token: 0x060015AC RID: 5548 RVA: 0x00070C8A File Offset: 0x0006F08A
		public QuestState_QuestChapterSelect(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015AD RID: 5549 RVA: 0x00070CA1 File Offset: 0x0006F0A1
		public override int GetStateID()
		{
			return 3;
		}

		// Token: 0x060015AE RID: 5550 RVA: 0x00070CA4 File Offset: 0x0006F0A4
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestChapterSelect.eStep.First;
		}

		// Token: 0x060015AF RID: 5551 RVA: 0x00070CAD File Offset: 0x0006F0AD
		public override void OnStateExit()
		{
		}

		// Token: 0x060015B0 RID: 5552 RVA: 0x00070CAF File Offset: 0x0006F0AF
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015B1 RID: 5553 RVA: 0x00070CB8 File Offset: 0x0006F0B8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestChapterSelect.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Quest, true);
				this.m_Step = QuestState_QuestChapterSelect.eStep.LoadWait;
				break;
			case QuestState_QuestChapterSelect.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID) && !SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestChapterSelect.eStep.PlayIn;
				}
				break;
			case QuestState_QuestChapterSelect.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = QuestState_QuestChapterSelect.eStep.Main;
				}
				break;
			case QuestState_QuestChapterSelect.eStep.Main:
				if (this.m_UI.IsIdle())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackADVPop(new Action(this.OnConfirmADVUnlock));
				}
				if (this.m_UI.IsMenuEnd())
				{
					if (!this.m_IsTransitADV)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
						{
							this.m_NextState = 2147483646;
						}
						else
						{
							QuestChapterSelectUI.eButton selectButton = this.m_UI.SelectButton;
							if (selectButton != QuestChapterSelectUI.eButton.Chapter)
							{
								this.m_NextState = 4;
							}
							else
							{
								QuestManager.ChapterData chapterDataAt = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetChapterDataAt(this.m_UI.SelectChapterIdx, QuestDefine.eDifficulty.Normal);
								SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest().SetupChapter(chapterDataAt.m_ChapterID, QuestDefine.eDifficulty.Normal, chapterDataAt.m_QuestIDs[chapterDataAt.m_QuestIDs.Length - 1]);
								this.m_NextState = 4;
							}
						}
					}
					this.m_UI.Destroy();
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestChapterSelect.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestChapterSelect.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = QuestState_QuestChapterSelect.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015B2 RID: 5554 RVA: 0x00070EB0 File Offset: 0x0006F2B0
		private void OnConfirmADVUnlock()
		{
			this.GoToMenuEnd();
			this.m_NextState = 2147483646;
			this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			globalParam.SetAdvParam(globalParam.AdvID, SceneDefine.eSceneID.Quest, false);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.List;
			this.m_IsTransitADV = true;
		}

		// Token: 0x060015B3 RID: 5555 RVA: 0x00070F0C File Offset: 0x0006F30C
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<QuestChapterSelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			bool playOpenAnim = false;
			int chapterID = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest().m_ChapterID;
			if (this.m_Owner.m_ForceChapterOpenAnim || LocalSaveData.Inst.PlayedOpenChapterId < chapterID)
			{
				playOpenAnim = true;
				LocalSaveData.Inst.PlayedOpenChapterId = chapterID;
				LocalSaveData.Inst.Save();
			}
			this.m_Owner.m_ForceChapterOpenAnim = false;
			this.m_UI.Setup(chapterID, playOpenAnim);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Quest);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015B4 RID: 5556 RVA: 0x00070FFE File Offset: 0x0006F3FE
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (isCallFromShortCut)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			}
			this.GoToMenuEnd();
		}

		// Token: 0x060015B5 RID: 5557 RVA: 0x00071022 File Offset: 0x0006F422
		private void OnClickButton()
		{
		}

		// Token: 0x060015B6 RID: 5558 RVA: 0x00071024 File Offset: 0x0006F424
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001C6E RID: 7278
		private QuestState_QuestChapterSelect.eStep m_Step = QuestState_QuestChapterSelect.eStep.None;

		// Token: 0x04001C6F RID: 7279
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.QuestChapterSelectUI;

		// Token: 0x04001C70 RID: 7280
		public QuestChapterSelectUI m_UI;

		// Token: 0x04001C71 RID: 7281
		private bool m_IsTransitADV;

		// Token: 0x02000460 RID: 1120
		private enum eStep
		{
			// Token: 0x04001C73 RID: 7283
			None = -1,
			// Token: 0x04001C74 RID: 7284
			First,
			// Token: 0x04001C75 RID: 7285
			LoadWait,
			// Token: 0x04001C76 RID: 7286
			PlayIn,
			// Token: 0x04001C77 RID: 7287
			Main,
			// Token: 0x04001C78 RID: 7288
			UnloadChildSceneWait
		}
	}
}
