﻿using System;

namespace Star
{
	// Token: 0x02000350 RID: 848
	public class IFldNetComModule
	{
		// Token: 0x06001028 RID: 4136 RVA: 0x0005603E File Offset: 0x0005443E
		public IFldNetComModule(IFldNetComManager pmanager)
		{
			this.m_NetMng = pmanager;
			this.m_Callback = null;
		}

		// Token: 0x06001029 RID: 4137 RVA: 0x00056054 File Offset: 0x00054454
		public void EntryCallback(IFldNetComModule.CallBack pcallback)
		{
			this.m_Callback = pcallback;
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x0005605D File Offset: 0x0005445D
		public virtual bool UpFunc()
		{
			return true;
		}

		// Token: 0x0400173A RID: 5946
		protected IFldNetComManager m_NetMng;

		// Token: 0x0400173B RID: 5947
		public IFldNetComModule.CallBack m_Callback;

		// Token: 0x02000351 RID: 849
		// (Invoke) Token: 0x0600102C RID: 4140
		public delegate void CallBack(IFldNetComModule pmodule);
	}
}
