﻿using System;

namespace Star
{
	// Token: 0x020004B2 RID: 1202
	public class TownComMoveDecision : IMainComCommand
	{
		// Token: 0x06001776 RID: 6006 RVA: 0x000795B0 File Offset: 0x000779B0
		public override int GetHashCode()
		{
			return 6;
		}

		// Token: 0x04001E35 RID: 7733
		public int m_BaseBuildPoint;

		// Token: 0x04001E36 RID: 7734
		public long m_BaseMngID;

		// Token: 0x04001E37 RID: 7735
		public int m_TargetBuildPoint;

		// Token: 0x04001E38 RID: 7736
		public long m_ChangeMngID;
	}
}
