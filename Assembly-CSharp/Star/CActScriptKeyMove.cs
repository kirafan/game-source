﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200054A RID: 1354
	public class CActScriptKeyMove : IRoomScriptData
	{
		// Token: 0x06001AC7 RID: 6855 RVA: 0x0008EDB8 File Offset: 0x0008D1B8
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyMove actXlsKeyMove = (ActXlsKeyMove)pbase;
			this.m_Func = actXlsKeyMove.m_Func;
			this.m_Speed = actXlsKeyMove.m_Speed;
			this.m_MoveTime = (float)actXlsKeyMove.m_MoveTime / 30f;
			this.m_Offset = actXlsKeyMove.m_Offset;
		}

		// Token: 0x0400218A RID: 8586
		public CActScriptKeyMove.eMoveType m_Func;

		// Token: 0x0400218B RID: 8587
		public float m_Speed;

		// Token: 0x0400218C RID: 8588
		public float m_MoveTime;

		// Token: 0x0400218D RID: 8589
		public Vector3 m_Offset;

		// Token: 0x0200054B RID: 1355
		public enum eMoveType
		{
			// Token: 0x0400218F RID: 8591
			TargetTime,
			// Token: 0x04002190 RID: 8592
			TargetSpeed,
			// Token: 0x04002191 RID: 8593
			BaseTime,
			// Token: 0x04002192 RID: 8594
			BaseSpeed,
			// Token: 0x04002193 RID: 8595
			BasePos,
			// Token: 0x04002194 RID: 8596
			LocTarget
		}
	}
}
