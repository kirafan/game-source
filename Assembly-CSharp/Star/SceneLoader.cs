﻿using System;
using System.Diagnostics;
using System.Text;
using Meige;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Star
{
	// Token: 0x02000665 RID: 1637
	public class SceneLoader : SingletonMonoBehaviour<SceneLoader>
	{
		// Token: 0x06002118 RID: 8472 RVA: 0x000B070C File Offset: 0x000AEB0C
		protected override void Awake()
		{
			if (base.CheckInstance())
			{
				this.m_ChildList = new SceneLoader.OperationInfo[61];
				this.m_sb = new StringBuilder();
				SceneManager.activeSceneChanged += this.OnActiveSceneChanged;
				SceneManager.sceneLoaded += this.OnSceneLoaded;
				SceneManager.sceneUnloaded += this.OnSceneUnloaded;
			}
		}

		// Token: 0x06002119 RID: 8473 RVA: 0x000B0770 File Offset: 0x000AEB70
		public void Update()
		{
			if (this.m_MainSceneHandler != null && this.m_MainSceneHandler.IsError)
			{
				MeigeResourceManager.RetryHandler(this.m_MainSceneHandler);
			}
			for (int i = 0; i < this.m_ChildList.Length; i++)
			{
				if (this.m_ChildList[i] != null && this.m_ChildList[i].m_SceneHandler != null && this.m_ChildList[i].m_SceneHandler.IsError)
				{
					MeigeResourceManager.RetryHandler(this.m_ChildList[i].m_SceneHandler);
				}
			}
		}

		// Token: 0x0600211A RID: 8474 RVA: 0x000B0804 File Offset: 0x000AEC04
		public void ForceTransitTitleScene()
		{
			for (int i = 0; i < this.m_ChildList.Length; i++)
			{
				if (this.m_ChildList[i] != null)
				{
					if (this.m_ChildList[i].m_SceneHandler != null)
					{
						MeigeResourceManager.UnloadHandler(this.m_ChildList[i].m_SceneHandler, false);
						this.m_ChildList[i].m_SceneHandler = null;
					}
					if (this.m_ChildList[i].m_Op != null)
					{
						this.m_ChildList[i].m_Op = null;
					}
				}
				this.m_ChildList[i] = null;
			}
			this.TransitSceneAsync(SceneDefine.eSceneID.Title);
		}

		// Token: 0x0600211B RID: 8475 RVA: 0x000B089C File Offset: 0x000AEC9C
		public void TransitSceneAsync(SceneDefine.eSceneID sceneID)
		{
			if (this.m_MainSceneHandler != null)
			{
				MeigeResourceManager.UnloadHandler(this.m_MainSceneHandler, false);
				this.m_MainSceneHandler = null;
			}
			if (SceneDefine.SCENE_INFOS[sceneID].m_IsAssetBundle)
			{
				this.m_sb.Length = 0;
				this.m_sb.Append("Scene/");
				this.m_sb.Append(SceneDefine.SCENE_INFOS[sceneID].m_SceneName);
				this.m_sb.Append(".muast");
				this.m_MainSceneHandler = MeigeResourceManager.LoadHandler(this.m_sb.ToString(), SceneDefine.SCENE_INFOS[sceneID].m_SceneName, new MeigeResource.Option[]
				{
					MeigeResource.Option.Scene
				});
			}
			else
			{
				SceneManager.LoadSceneAsync(SceneDefine.SCENE_INFOS[sceneID].m_SceneName, LoadSceneMode.Single);
			}
		}

		// Token: 0x0600211C RID: 8476 RVA: 0x000B0980 File Offset: 0x000AED80
		public void AdditiveChildSceneAsync(SceneDefine.eChildSceneID childSceneID, bool allowSceneActivation = true)
		{
			if (this.m_ChildList[(int)childSceneID] == null)
			{
				this.m_ChildList[(int)childSceneID] = new SceneLoader.OperationInfo();
			}
			SceneLoader.OperationInfo operationInfo = this.m_ChildList[(int)childSceneID];
			if (SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_IsAssetBundle)
			{
				this.m_sb.Length = 0;
				this.m_sb.Append("Scene/Child/");
				this.m_sb.Append(SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName);
				this.m_sb.Append(".muast");
				MeigeResource.Option[] options;
				if (allowSceneActivation)
				{
					options = new MeigeResource.Option[]
					{
						MeigeResource.Option.SceneAdditive
					};
				}
				else
				{
					options = new MeigeResource.Option[]
					{
						MeigeResource.Option.SceneAdditive,
						MeigeResource.Option.SceneActivationFalse
					};
				}
				operationInfo.m_SceneHandler = MeigeResourceManager.LoadHandler(this.m_sb.ToString(), SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName, options);
				operationInfo.m_Op = null;
			}
			else
			{
				AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName, LoadSceneMode.Additive);
				asyncOperation.allowSceneActivation = allowSceneActivation;
				operationInfo.m_Op = asyncOperation;
				operationInfo.m_SceneHandler = null;
			}
		}

		// Token: 0x0600211D RID: 8477 RVA: 0x000B0AA8 File Offset: 0x000AEEA8
		public bool IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID childSceneID)
		{
			SceneLoader.OperationInfo operationInfo = this.m_ChildList[(int)childSceneID];
			if (operationInfo != null)
			{
				if (operationInfo.m_SceneHandler != null)
				{
					return operationInfo.m_SceneHandler.IsDone;
				}
				if (operationInfo.m_Op != null)
				{
					if (!operationInfo.m_Op.allowSceneActivation)
					{
						return operationInfo.m_Op.progress >= 0.9f;
					}
					return operationInfo.m_Op.isDone;
				}
			}
			return false;
		}

		// Token: 0x0600211E RID: 8478 RVA: 0x000B0B24 File Offset: 0x000AEF24
		public void ActivationChildScene(SceneDefine.eChildSceneID childSceneID)
		{
			SceneLoader.OperationInfo operationInfo = this.m_ChildList[(int)childSceneID];
			if (operationInfo != null)
			{
				if (operationInfo.m_SceneHandler != null)
				{
					if (!operationInfo.m_SceneHandler.asyncOperation.allowSceneActivation)
					{
						operationInfo.m_SceneHandler.asyncOperation.allowSceneActivation = true;
					}
				}
				else if (operationInfo.m_Op != null && !operationInfo.m_Op.allowSceneActivation)
				{
					operationInfo.m_Op.allowSceneActivation = true;
					operationInfo.m_Op = null;
				}
			}
		}

		// Token: 0x0600211F RID: 8479 RVA: 0x000B0BA4 File Offset: 0x000AEFA4
		public void UnloadChildSceneAsync(SceneDefine.eChildSceneID childSceneID)
		{
			SceneLoader.OperationInfo operationInfo = this.m_ChildList[(int)childSceneID];
			if (operationInfo != null)
			{
				if (operationInfo.m_SceneHandler != null)
				{
					MeigeResourceManager.UnloadHandler(operationInfo.m_SceneHandler, false);
				}
				else
				{
					operationInfo.m_Op = SceneManager.UnloadSceneAsync(SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName);
				}
			}
		}

		// Token: 0x06002120 RID: 8480 RVA: 0x000B0BFC File Offset: 0x000AEFFC
		public bool IsCompleteUnloadChildScene(SceneDefine.eChildSceneID childSceneID)
		{
			SceneLoader.OperationInfo operationInfo = this.m_ChildList[(int)childSceneID];
			if (operationInfo != null)
			{
				if (operationInfo.m_SceneHandler != null)
				{
					if (operationInfo.m_SceneHandler.IsUnloadDone)
					{
						operationInfo.m_SceneHandler = null;
						return true;
					}
					return false;
				}
				else if (operationInfo.m_Op != null)
				{
					if (operationInfo.m_Op.isDone)
					{
						operationInfo.m_Op = null;
						return true;
					}
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002121 RID: 8481 RVA: 0x000B0C68 File Offset: 0x000AF068
		public Scene GetScene(SceneDefine.eSceneID sceneID)
		{
			return SceneManager.GetSceneByName(SceneDefine.SCENE_INFOS[sceneID].m_SceneName);
		}

		// Token: 0x06002122 RID: 8482 RVA: 0x000B0C90 File Offset: 0x000AF090
		public Scene GetChildScene(SceneDefine.eChildSceneID childSceneID)
		{
			return SceneManager.GetSceneByName(SceneDefine.CHILD_SCENE_INFOS[childSceneID].m_SceneName);
		}

		// Token: 0x06002123 RID: 8483 RVA: 0x000B0CB5 File Offset: 0x000AF0B5
		private void OnActiveSceneChanged(Scene preChangedScene, Scene postChangedScene)
		{
		}

		// Token: 0x06002124 RID: 8484 RVA: 0x000B0CB7 File Offset: 0x000AF0B7
		private void OnSceneLoaded(Scene loadedScene, LoadSceneMode mode)
		{
		}

		// Token: 0x06002125 RID: 8485 RVA: 0x000B0CB9 File Offset: 0x000AF0B9
		private void OnSceneUnloaded(Scene unloaded)
		{
		}

		// Token: 0x06002126 RID: 8486 RVA: 0x000B0CBB File Offset: 0x000AF0BB
		[Conditional("APP_DEBUG")]
		private void StartStopwatch(string key)
		{
		}

		// Token: 0x06002127 RID: 8487 RVA: 0x000B0CBD File Offset: 0x000AF0BD
		[Conditional("APP_DEBUG")]
		private void StopStopwatch(string key)
		{
		}

		// Token: 0x04002794 RID: 10132
		private MeigeResource.Handler m_MainSceneHandler;

		// Token: 0x04002795 RID: 10133
		private SceneLoader.OperationInfo[] m_ChildList;

		// Token: 0x04002796 RID: 10134
		private StringBuilder m_sb;

		// Token: 0x02000666 RID: 1638
		private class OperationInfo
		{
			// Token: 0x04002797 RID: 10135
			public MeigeResource.Handler m_SceneHandler;

			// Token: 0x04002798 RID: 10136
			public AsyncOperation m_Op;
		}
	}
}
