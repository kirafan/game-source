﻿using System;

namespace Star
{
	// Token: 0x02000502 RID: 1282
	public class IMissionPakage
	{
		// Token: 0x0600192D RID: 6445 RVA: 0x00082D8C File Offset: 0x0008118C
		public virtual string GetTargetMessage()
		{
			if (!string.IsNullOrEmpty(this.m_TargetMessage) && this.m_TargetMessage.Contains("{0}"))
			{
				return string.Format(this.m_TargetMessage, this.m_RateBase);
			}
			return this.m_TargetMessage;
		}

		// Token: 0x04001FF2 RID: 8178
		public eMissionCategory m_Type;

		// Token: 0x04001FF3 RID: 8179
		public eXlsMissionSeg m_ActionCategory;

		// Token: 0x04001FF4 RID: 8180
		public int m_ExtensionScriptID;

		// Token: 0x04001FF5 RID: 8181
		public string m_TargetMessage;

		// Token: 0x04001FF6 RID: 8182
		public long m_KeyManageID;

		// Token: 0x04001FF7 RID: 8183
		public long m_ListUpID;

		// Token: 0x04001FF8 RID: 8184
		public long m_SubCodeID;

		// Token: 0x04001FF9 RID: 8185
		public eMissionState m_State;

		// Token: 0x04001FFA RID: 8186
		public eMissionRewardCategory m_RewardCategory;

		// Token: 0x04001FFB RID: 8187
		public int m_RewardNo;

		// Token: 0x04001FFC RID: 8188
		public int m_RewardNum;

		// Token: 0x04001FFD RID: 8189
		public int m_Rate;

		// Token: 0x04001FFE RID: 8190
		public int m_RateBase;

		// Token: 0x04001FFF RID: 8191
		public DateTime m_LimitTime;

		// Token: 0x04002000 RID: 8192
		public ushort m_PopupTimingNo;
	}
}
