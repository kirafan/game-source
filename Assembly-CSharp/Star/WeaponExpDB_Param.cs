﻿using System;

namespace Star
{
	// Token: 0x02000260 RID: 608
	[Serializable]
	public struct WeaponExpDB_Param
	{
		// Token: 0x040013BF RID: 5055
		public int m_ID;

		// Token: 0x040013C0 RID: 5056
		public int m_Lv;

		// Token: 0x040013C1 RID: 5057
		public int m_NextExp;

		// Token: 0x040013C2 RID: 5058
		public int m_UpgradeAmount;
	}
}
