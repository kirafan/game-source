﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006FC RID: 1788
	public class TownBuildLinkPoint
	{
		// Token: 0x06002368 RID: 9064 RVA: 0x000BE36E File Offset: 0x000BC76E
		public TownBuildLinkPoint(eBuildPointCategory ftype)
		{
			this.m_Type = ftype;
			this.m_Build = false;
			this.m_AccessID = 0;
			this.m_AccessMngID = -1L;
			this.m_Object = null;
			this.m_Pakage = null;
		}

		// Token: 0x06002369 RID: 9065 RVA: 0x000BE3A1 File Offset: 0x000BC7A1
		public void ChangeManageID(long fmanageid)
		{
			this.m_AccessMngID = fmanageid;
		}

		// Token: 0x0600236A RID: 9066 RVA: 0x000BE3AA File Offset: 0x000BC7AA
		public void SetBuilding(bool fset)
		{
			this.m_Build = fset;
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x000BE3B3 File Offset: 0x000BC7B3
		public bool IsBuilding()
		{
			return this.m_Build;
		}

		// Token: 0x0600236C RID: 9068 RVA: 0x000BE3BB File Offset: 0x000BC7BB
		public eBuildPointCategory GetCategory()
		{
			return this.m_Type;
		}

		// Token: 0x0600236D RID: 9069 RVA: 0x000BE3C3 File Offset: 0x000BC7C3
		public int GetAccessID()
		{
			return this.m_AccessID;
		}

		// Token: 0x0600236E RID: 9070 RVA: 0x000BE3CB File Offset: 0x000BC7CB
		public void Release()
		{
			this.m_Pakage.Release();
			UnityEngine.Object.Destroy(this.m_Pakage);
			this.m_Pakage = null;
			this.m_Object = null;
		}

		// Token: 0x04002A39 RID: 10809
		public eBuildPointCategory m_Type;

		// Token: 0x04002A3A RID: 10810
		public int m_AccessID;

		// Token: 0x04002A3B RID: 10811
		public long m_AccessMngID;

		// Token: 0x04002A3C RID: 10812
		public bool m_Build;

		// Token: 0x04002A3D RID: 10813
		public GameObject m_Object;

		// Token: 0x04002A3E RID: 10814
		public TownBuildPakage m_Pakage;
	}
}
