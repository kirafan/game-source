﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000127 RID: 295
	public class SkillActionMovementStraight : SkillActionMovementBase
	{
		// Token: 0x060007A9 RID: 1961 RVA: 0x0002ECF8 File Offset: 0x0002D0F8
		public SkillActionMovementStraight(Transform moveObj, bool isLocalPos, Vector3 startPos, Vector3 targetPos, float speed) : base(moveObj, isLocalPos)
		{
			this.m_StartPos = startPos;
			this.m_TargetPos = targetPos;
			this.m_Speed = speed;
			Vector2 a = this.m_StartPos;
			Vector2 b = this.m_TargetPos;
			float num = Vector2.Distance(a, b);
			this.m_ArrivalSec = num / this.m_Speed;
			this.m_t = 0f;
			this.m_IsArrival = false;
			if (isLocalPos)
			{
				this.m_MoveObj.localPosition = this.m_StartPos;
			}
			else
			{
				this.m_MoveObj.position = this.m_StartPos;
			}
		}

		// Token: 0x060007AA RID: 1962 RVA: 0x0002ED94 File Offset: 0x0002D194
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			out_arrivalIndex = -1;
			out_arrivalPos = Vector3.zero;
			if (!this.m_IsMoving)
			{
				return false;
			}
			if (this.m_t < this.m_ArrivalSec)
			{
				this.m_t = Mathf.Min(this.m_t + Time.deltaTime, this.m_ArrivalSec);
				float t = this.m_t / this.m_ArrivalSec;
				Vector3 vector = Vector3.Lerp(this.m_StartPos, this.m_TargetPos, t);
				if (this.m_IsLocalPos)
				{
					this.m_MoveObj.localPosition = vector;
				}
				else
				{
					this.m_MoveObj.position = vector;
				}
			}
			if (this.m_t >= this.m_ArrivalSec)
			{
				this.m_IsMoving = false;
				if (!this.m_IsArrival)
				{
					out_arrivalIndex = 0;
					out_arrivalPos = this.m_TargetPos;
					this.m_IsArrival = true;
				}
			}
			return this.m_IsMoving;
		}

		// Token: 0x0400076C RID: 1900
		private Vector3 m_StartPos;

		// Token: 0x0400076D RID: 1901
		private Vector3 m_TargetPos;

		// Token: 0x0400076E RID: 1902
		private float m_Speed;

		// Token: 0x0400076F RID: 1903
		private float m_ArrivalSec;

		// Token: 0x04000770 RID: 1904
		private bool m_IsArrival;

		// Token: 0x04000771 RID: 1905
		private float m_t;
	}
}
