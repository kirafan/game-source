﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000250 RID: 592
	public class TownObjectListDB : ScriptableObject
	{
		// Token: 0x04001374 RID: 4980
		public TownObjectListDB_Param[] m_Params;
	}
}
