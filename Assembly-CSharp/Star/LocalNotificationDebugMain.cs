﻿using System;
using Star.UI;
using UnityEngine;

namespace Star
{
	// Token: 0x02000282 RID: 642
	public class LocalNotificationDebugMain : MonoBehaviour
	{
		// Token: 0x06000C09 RID: 3081 RVA: 0x00046BCA File Offset: 0x00044FCA
		private void Start()
		{
		}

		// Token: 0x06000C0A RID: 3082 RVA: 0x00046BCC File Offset: 0x00044FCC
		private void RegisterLocalNotification()
		{
			if (this.m_Controller == null)
			{
				return;
			}
			LocalNotificationController.SimpleNotification(int.Parse(this.m_NoticeID.GetString()), int.Parse(this.m_ReceiveSecond.GetString()), this.m_NoticeTitle.GetString(), this.m_NoticeMessage.GetString(), "m_icon", "true", default(Color));
		}

		// Token: 0x06000C0B RID: 3083 RVA: 0x00046C39 File Offset: 0x00045039
		private void UnregisterLocalNotification()
		{
			if (this.m_Controller == null)
			{
				return;
			}
			LocalNotificationController.UnregisterNotification(int.Parse(this.m_NoticeID.GetString()));
		}

		// Token: 0x06000C0C RID: 3084 RVA: 0x00046C62 File Offset: 0x00045062
		public void OnClickCheckAgreeButton()
		{
			LocalNotificationController.RegisterForNotifications();
		}

		// Token: 0x06000C0D RID: 3085 RVA: 0x00046C69 File Offset: 0x00045069
		public void OnClickRegisterButton()
		{
			this.RegisterLocalNotification();
		}

		// Token: 0x06000C0E RID: 3086 RVA: 0x00046C71 File Offset: 0x00045071
		public void OnClickUnregisterButton()
		{
			this.UnregisterLocalNotification();
		}

		// Token: 0x040014A4 RID: 5284
		[SerializeField]
		private LocalNotificationController m_Controller;

		// Token: 0x040014A5 RID: 5285
		[SerializeField]
		private InputFieldController m_NoticeID;

		// Token: 0x040014A6 RID: 5286
		[SerializeField]
		private InputFieldController m_ReceiveSecond;

		// Token: 0x040014A7 RID: 5287
		[SerializeField]
		private InputFieldController m_NoticeTitle;

		// Token: 0x040014A8 RID: 5288
		[SerializeField]
		private InputFieldController m_NoticeMessage;
	}
}
