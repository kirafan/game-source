﻿using System;

namespace Star
{
	// Token: 0x02000120 RID: 288
	[Serializable]
	public class SkillActionEvent_PlayVoice : SkillActionEvent_Base
	{
		// Token: 0x0400073F RID: 1855
		public string m_VoiceCueName;
	}
}
