﻿using System;
using Star.UI.Window;
using UnityEngine;

namespace Star
{
	// Token: 0x02000668 RID: 1640
	public class StoreReviewController : MonoBehaviour
	{
		// Token: 0x0600212C RID: 8492 RVA: 0x000B0CDC File Offset: 0x000AF0DC
		private void Update()
		{
			if (this.m_IsOpenWindow && this.m_IsNativeOpen)
			{
				this.m_WorkTimer += Time.deltaTime;
				if (this.m_WorkTimer >= 3f)
				{
					this.m_IsOpenWindow = false;
				}
			}
		}

		// Token: 0x0600212D RID: 8493 RVA: 0x000B0D28 File Offset: 0x000AF128
		public bool CheckConditionFull(int prevLv, int currentLv, int questID)
		{
			return this.CheckCondition_ReviewVersion() && (this.CheckCondition_UserLv(prevLv, currentLv) || this.CheckCondition_Quest(questID));
		}

		// Token: 0x0600212E RID: 8494 RVA: 0x000B0D51 File Offset: 0x000AF151
		public bool CheckCondition_ReviewVersion()
		{
			return LocalSaveData.Inst.StoreReviewAppVersion != "1.0.3";
		}

		// Token: 0x0600212F RID: 8495 RVA: 0x000B0D70 File Offset: 0x000AF170
		public bool CheckCondition_UserLv(int prevLv, int currentLv)
		{
			if (currentLv <= prevLv)
			{
				return false;
			}
			MasterRankDB masterRankDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterRankDB;
			for (int i = prevLv; i <= currentLv; i++)
			{
				if (masterRankDB.GetParam(i).m_StoreReview == 1)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002130 RID: 8496 RVA: 0x000B0DC0 File Offset: 0x000AF1C0
		public bool CheckCondition_Quest(int questID)
		{
			QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(questID);
			return questData != null && questData.m_Param.m_StoreReview == 1;
		}

		// Token: 0x06002131 RID: 8497 RVA: 0x000B0DF8 File Offset: 0x000AF1F8
		public bool Review()
		{
			this.m_IsOpenWindow = true;
			this.m_WorkTimer = 0f;
			if (this.IsNativeCall())
			{
				this.m_IsNativeOpen = true;
				NativeReviewRequest.RequestReview();
				this.SaveStoreReviewAppVersion();
				return true;
			}
			this.m_IsNativeOpen = false;
			this.m_Window.Open();
			return false;
		}

		// Token: 0x06002132 RID: 8498 RVA: 0x000B0E49 File Offset: 0x000AF249
		public bool IsOpenWindow()
		{
			return this.m_IsOpenWindow;
		}

		// Token: 0x06002133 RID: 8499 RVA: 0x000B0E51 File Offset: 0x000AF251
		public void OnWindowYes()
		{
			this.OpenStoreReviewURL();
			this.m_IsOpenWindow = false;
			this.m_Window.Close();
		}

		// Token: 0x06002134 RID: 8500 RVA: 0x000B0E6B File Offset: 0x000AF26B
		public void OnWindowNo()
		{
			this.m_IsOpenWindow = false;
			this.m_Window.Close();
		}

		// Token: 0x06002135 RID: 8501 RVA: 0x000B0E7F File Offset: 0x000AF27F
		private bool IsNativeCall()
		{
			return false;
		}

		// Token: 0x06002136 RID: 8502 RVA: 0x000B0E82 File Offset: 0x000AF282
		private void OpenStoreReviewURL()
		{
			Application.OpenURL(WebDataListUtil.GetIDToAdress(1015));
		}

		// Token: 0x06002137 RID: 8503 RVA: 0x000B0E93 File Offset: 0x000AF293
		private void SaveStoreReviewAppVersion()
		{
			LocalSaveData.Inst.StoreReviewAppVersion = "1.0.3";
			LocalSaveData.Inst.Save();
		}

		// Token: 0x04002799 RID: 10137
		[SerializeField]
		private CustomWindow m_Window;

		// Token: 0x0400279A RID: 10138
		private bool m_IsOpenWindow;

		// Token: 0x0400279B RID: 10139
		private bool m_IsNativeOpen;

		// Token: 0x0400279C RID: 10140
		private float m_WorkTimer;

		// Token: 0x0400279D RID: 10141
		private const int STORE_REIVEW_ON = 1;
	}
}
