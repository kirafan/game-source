﻿using System;

namespace Star
{
	// Token: 0x02000B54 RID: 2900
	public class UserSupportData
	{
		// Token: 0x06003D6A RID: 15722 RVA: 0x00136E90 File Offset: 0x00135290
		public UserSupportData()
		{
			this.MngID = -1L;
			this.CharaID = -1;
		}

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x06003D6B RID: 15723 RVA: 0x00136EA7 File Offset: 0x001352A7
		// (set) Token: 0x06003D6C RID: 15724 RVA: 0x00136EAF File Offset: 0x001352AF
		public long MngID { get; set; }

		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x06003D6D RID: 15725 RVA: 0x00136EB8 File Offset: 0x001352B8
		// (set) Token: 0x06003D6E RID: 15726 RVA: 0x00136EC0 File Offset: 0x001352C0
		public int CharaID { get; set; }

		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x06003D6F RID: 15727 RVA: 0x00136EC9 File Offset: 0x001352C9
		// (set) Token: 0x06003D70 RID: 15728 RVA: 0x00136ED1 File Offset: 0x001352D1
		public int Lv { get; set; }

		// Token: 0x170003C8 RID: 968
		// (get) Token: 0x06003D71 RID: 15729 RVA: 0x00136EDA File Offset: 0x001352DA
		// (set) Token: 0x06003D72 RID: 15730 RVA: 0x00136EE2 File Offset: 0x001352E2
		public int MaxLv { get; set; }

		// Token: 0x170003C9 RID: 969
		// (get) Token: 0x06003D73 RID: 15731 RVA: 0x00136EEB File Offset: 0x001352EB
		// (set) Token: 0x06003D74 RID: 15732 RVA: 0x00136EF3 File Offset: 0x001352F3
		public long Exp { get; set; }

		// Token: 0x170003CA RID: 970
		// (get) Token: 0x06003D75 RID: 15733 RVA: 0x00136EFC File Offset: 0x001352FC
		// (set) Token: 0x06003D76 RID: 15734 RVA: 0x00136F04 File Offset: 0x00135304
		public int LimitBreak { get; set; }

		// Token: 0x170003CB RID: 971
		// (get) Token: 0x06003D77 RID: 15735 RVA: 0x00136F0D File Offset: 0x0013530D
		// (set) Token: 0x06003D78 RID: 15736 RVA: 0x00136F15 File Offset: 0x00135315
		public int UniqueSkillLv { get; set; }

		// Token: 0x170003CC RID: 972
		// (get) Token: 0x06003D79 RID: 15737 RVA: 0x00136F1E File Offset: 0x0013531E
		// (set) Token: 0x06003D7A RID: 15738 RVA: 0x00136F26 File Offset: 0x00135326
		public int[] ClassSkillLvs { get; set; }

		// Token: 0x170003CD RID: 973
		// (get) Token: 0x06003D7B RID: 15739 RVA: 0x00136F2F File Offset: 0x0013532F
		// (set) Token: 0x06003D7C RID: 15740 RVA: 0x00136F37 File Offset: 0x00135337
		public int FriendShip { get; set; }

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x06003D7D RID: 15741 RVA: 0x00136F40 File Offset: 0x00135340
		// (set) Token: 0x06003D7E RID: 15742 RVA: 0x00136F48 File Offset: 0x00135348
		public long FriendShipExp { get; set; }

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x06003D7F RID: 15743 RVA: 0x00136F51 File Offset: 0x00135351
		// (set) Token: 0x06003D80 RID: 15744 RVA: 0x00136F59 File Offset: 0x00135359
		public int WeaponID { get; set; }

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x06003D81 RID: 15745 RVA: 0x00136F62 File Offset: 0x00135362
		// (set) Token: 0x06003D82 RID: 15746 RVA: 0x00136F6A File Offset: 0x0013536A
		public int WeaponLv { get; set; }

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x06003D83 RID: 15747 RVA: 0x00136F73 File Offset: 0x00135373
		// (set) Token: 0x06003D84 RID: 15748 RVA: 0x00136F7B File Offset: 0x0013537B
		public int WeaponSkillLv { get; set; }

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x06003D85 RID: 15749 RVA: 0x00136F84 File Offset: 0x00135384
		// (set) Token: 0x06003D86 RID: 15750 RVA: 0x00136F8C File Offset: 0x0013538C
		public long WeaponSkillExp { get; set; }
	}
}
