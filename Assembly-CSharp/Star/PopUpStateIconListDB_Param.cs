﻿using System;

namespace Star
{
	// Token: 0x020001DC RID: 476
	[Serializable]
	public struct PopUpStateIconListDB_Param
	{
		// Token: 0x04000B35 RID: 2869
		public int m_PopUpType;

		// Token: 0x04000B36 RID: 2870
		public int m_StateIconType;

		// Token: 0x04000B37 RID: 2871
		public string[] m_Text;
	}
}
