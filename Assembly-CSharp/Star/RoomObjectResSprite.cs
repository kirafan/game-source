﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020005DD RID: 1501
	public class RoomObjectResSprite : IRoomObjectResource
	{
		// Token: 0x06001D87 RID: 7559 RVA: 0x0009E248 File Offset: 0x0009C648
		public override void UpdateRes()
		{
			if (this.m_IsPreparing)
			{
				RoomObjectResSprite.ePrepareStep prepareStep = this.m_PrepareStep;
				if (prepareStep == RoomObjectResSprite.ePrepareStep.Sprite)
				{
					if (this.PreparingSprite())
					{
						this.m_PrepareStep = RoomObjectResSprite.ePrepareStep.None;
						this.m_IsPreparing = false;
					}
				}
			}
		}

		// Token: 0x06001D88 RID: 7560 RVA: 0x0009E290 File Offset: 0x0009C690
		public override void Setup(IRoomObjectControll hndl)
		{
			this.m_Owner = (RoomObjectSprite)hndl;
		}

		// Token: 0x06001D89 RID: 7561 RVA: 0x0009E29E File Offset: 0x0009C69E
		public override void Destroy()
		{
			if (this.m_SpriteHndl != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_SpriteHndl);
				this.m_SpriteHndl = null;
			}
		}

		// Token: 0x06001D8A RID: 7562 RVA: 0x0009E2C0 File Offset: 0x0009C6C0
		public override void Prepare()
		{
			this.m_IsPreparing = true;
			this.m_PrepareStep = RoomObjectResSprite.ePrepareStep.Sprite;
			string spriteResourcePath = RoomUtility.GetSpriteResourcePath(this.m_Owner.ObjCategory, this.m_Owner.ObjID, this.m_Owner.SubKeyID, this.m_Owner.GetDir());
			this.m_SpriteHndl = ObjectResourceManager.CreateHandle(spriteResourcePath);
		}

		// Token: 0x06001D8B RID: 7563 RVA: 0x0009E31C File Offset: 0x0009C71C
		private bool PreparingSprite()
		{
			if (this.m_SpriteHndl == null)
			{
				return true;
			}
			if (!this.m_SpriteHndl.IsDone)
			{
				return false;
			}
			if (this.m_SpriteHndl.IsError)
			{
				ObjectResourceManager.RetryHandle(this.m_SpriteHndl);
				return false;
			}
			Sprite asset = this.m_SpriteHndl.GetAsset<Sprite>();
			this.m_Owner.AttachSprite(asset);
			this.m_Owner.SetUpObjectKey(null);
			return true;
		}

		// Token: 0x040023E8 RID: 9192
		private RoomObjectSprite m_Owner;

		// Token: 0x040023E9 RID: 9193
		private MeigeResource.Handler m_SpriteHndl;

		// Token: 0x040023EA RID: 9194
		private RoomObjectResSprite.ePrepareStep m_PrepareStep = RoomObjectResSprite.ePrepareStep.None;

		// Token: 0x020005DE RID: 1502
		private enum ePrepareStep
		{
			// Token: 0x040023EC RID: 9196
			None = -1,
			// Token: 0x040023ED RID: 9197
			Sprite,
			// Token: 0x040023EE RID: 9198
			Num
		}
	}
}
