﻿using System;

namespace Star
{
	// Token: 0x02000600 RID: 1536
	public struct RoomWakeUpKey
	{
		// Token: 0x040024CD RID: 9421
		public RoomCharaActMode.eMode m_Type;

		// Token: 0x040024CE RID: 9422
		public float m_Percent;
	}
}
