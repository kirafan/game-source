﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200013A RID: 314
	public class CharacterBattle
	{
		// Token: 0x0600083D RID: 2109 RVA: 0x0003614C File Offset: 0x0003454C
		public CharacterBattle()
		{
			this.Join = BattleDefine.eJoinMember.None;
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x0600083E RID: 2110 RVA: 0x00036205 File Offset: 0x00034605
		// (set) Token: 0x0600083F RID: 2111 RVA: 0x0003620D File Offset: 0x0003460D
		public BattleSystem System { get; set; }

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000840 RID: 2112 RVA: 0x00036216 File Offset: 0x00034616
		// (set) Token: 0x06000841 RID: 2113 RVA: 0x0003621E File Offset: 0x0003461E
		public BattlePartyData MyParty { get; set; }

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x00036227 File Offset: 0x00034627
		// (set) Token: 0x06000843 RID: 2115 RVA: 0x0003622F File Offset: 0x0003462F
		public BattlePartyData TgtParty { get; set; }

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000844 RID: 2116 RVA: 0x00036238 File Offset: 0x00034638
		// (set) Token: 0x06000845 RID: 2117 RVA: 0x00036240 File Offset: 0x00034640
		public BattleDefine.eJoinMember Join { get; set; }

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x00036249 File Offset: 0x00034649
		// (set) Token: 0x06000847 RID: 2119 RVA: 0x00036251 File Offset: 0x00034651
		public string Name { get; set; }

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000848 RID: 2120 RVA: 0x0003625C File Offset: 0x0003465C
		// (remove) Token: 0x06000849 RID: 2121 RVA: 0x00036294 File Offset: 0x00034694
		public event Action<CharacterHandler> OnEndDeadAnimation;

		// Token: 0x0600084A RID: 2122 RVA: 0x000362CC File Offset: 0x000346CC
		public void Update()
		{
			this.m_Timer.Update(Time.deltaTime);
			switch (this.m_Mode)
			{
			case CharacterBattle.eMode.Idle:
				this.UpdateIdleMode();
				break;
			case CharacterBattle.eMode.Damage:
				this.UpdateDamageMode();
				break;
			case CharacterBattle.eMode.Dead:
				this.UpdateDeadMode();
				break;
			case CharacterBattle.eMode.Command:
				this.UpdateCommandMode();
				break;
			case CharacterBattle.eMode.Win:
				this.UpdateWinMode();
				break;
			case CharacterBattle.eMode.In:
				this.UpdateInMode();
				break;
			case CharacterBattle.eMode.Out:
				this.UpdateOutMode();
				break;
			}
			if (this.m_StunOccurEffectHandler != null && !this.m_StunOccurEffectHandler.IsPlaying())
			{
				this.DetachStunOccurEffect();
			}
		}

		// Token: 0x0600084B RID: 2123 RVA: 0x0003638B File Offset: 0x0003478B
		public void LateUpdate()
		{
			this.UpdateAutoFacial();
			this.UpdateFixRootBoneTransform();
			this.UpdateShadow();
		}

		// Token: 0x0600084C RID: 2124 RVA: 0x000363A0 File Offset: 0x000347A0
		public void Setup(CharacterHandler charaHndl, bool setupParameterOnly)
		{
			this.m_Owner = charaHndl;
			if (!this.m_SetupedParameter)
			{
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				this.m_BattleWinID = dbMng.NamedListDB.GetParam(this.m_Owner.CharaParam.NamedType).m_BattleWinID;
				this.SetupAI();
				this.SetupSkillActionData();
				this.m_SetupedParameter = true;
			}
			if (!setupParameterOnly && !this.m_SetupedResource)
			{
				this.SetupShadow();
				this.SetupCollision();
				this.m_SetupedResource = true;
			}
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x0003642C File Offset: 0x0003482C
		public void Destroy()
		{
			this.m_Owner = null;
			this.m_ExecCommand = null;
			if (this.m_Commands != null)
			{
				for (int i = 0; i < this.m_Commands.Count; i++)
				{
					this.m_Commands[i].Clear();
				}
				this.m_Commands.Clear();
				this.m_Commands = null;
			}
			this.m_BattleAIData = null;
			if (this.m_SAPs != null)
			{
				this.m_SAPs.Clear();
				this.m_SAPs = null;
			}
			if (this.m_SAGs != null)
			{
				this.m_SAGs.Clear();
				this.m_SAGs = null;
			}
			this.m_SkillActionPlayer = null;
			this.DetachDeadEffectHandler();
			this.DetachStunOccurEffect();
			this.DetachStunEffect();
			SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_SmokeEffectHandler);
			this.m_SmokeEffectHandler = null;
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x00036508 File Offset: 0x00034908
		private void SetupCollision()
		{
			SphereCollider sphereCollider = this.m_Owner.TouchCollider as SphereCollider;
			if (sphereCollider == null)
			{
				this.m_Owner.TouchCollider = this.m_Owner.gameObject.AddComponent<SphereCollider>();
				sphereCollider = (this.m_Owner.TouchCollider as SphereCollider);
			}
			sphereCollider.center = this.m_Owner.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, true);
			sphereCollider.radius = 0.36f;
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600084F RID: 2127 RVA: 0x00036581 File Offset: 0x00034981
		public Dictionary<string, SkillActionPlan> SAPs
		{
			get
			{
				return this.m_SAPs;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000850 RID: 2128 RVA: 0x00036589 File Offset: 0x00034989
		public Dictionary<string, SkillActionGraphics> SAGs
		{
			get
			{
				return this.m_SAGs;
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000851 RID: 2129 RVA: 0x00036591 File Offset: 0x00034991
		// (set) Token: 0x06000852 RID: 2130 RVA: 0x00036599 File Offset: 0x00034999
		public bool PreparedEffectWasFromSkillAction { get; set; }

		// Token: 0x06000853 RID: 2131 RVA: 0x000365A4 File Offset: 0x000349A4
		private void SetupSkillActionData()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_SAGs = new Dictionary<string, SkillActionGraphics>();
			string[] array = CharacterUtility.GetSAGIDs(this.m_Owner.CharaParam, this.m_Owner.WeaponParam, this.m_Owner.IsUserControll);
			for (int i = 0; i < array.Length; i++)
			{
				SkillActionGraphics skillActionGraphics = dbMng.FindSAG(array[i]);
				if (skillActionGraphics != null)
				{
					this.m_SAGs.AddOrReplace(skillActionGraphics.m_ID, skillActionGraphics);
				}
			}
			this.m_SAPs = new Dictionary<string, SkillActionPlan>();
			array = CharacterUtility.GetSAPIDs(this.m_Owner.CharaParam, this.m_Owner.WeaponParam, this.m_Owner.IsUserControll);
			for (int j = 0; j < array.Length; j++)
			{
				SkillActionPlan skillActionPlan = dbMng.FindSAP(array[j]);
				if (skillActionPlan != null)
				{
					this.m_SAPs.AddOrReplace(skillActionPlan.m_ID, skillActionPlan);
				}
			}
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x000366A4 File Offset: 0x00034AA4
		public Dictionary<string, int> GetEffectIDFromSkillAction()
		{
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
			if (this.m_SAGs != null)
			{
				foreach (string key in this.m_SAGs.Keys)
				{
					Dictionary<string, int> effectIDs = this.m_SAGs[key].GetEffectIDs(this.m_Owner.CharaParam.ElementType);
					foreach (string text in effectIDs.Keys)
					{
						if (dictionary.ContainsKey(text))
						{
							Dictionary<string, int> dictionary2;
							string key2;
							(dictionary2 = dictionary)[key2 = text] = dictionary2[key2] + effectIDs[text];
						}
						else
						{
							dictionary.Add(text, effectIDs[text]);
						}
					}
				}
			}
			if (this.m_SAPs != null)
			{
				foreach (string key3 in this.m_SAPs.Keys)
				{
					Dictionary<string, int> effectIDs2 = this.m_SAPs[key3].GetEffectIDs(this.m_Owner.CharaParam.ElementType);
					foreach (string text2 in effectIDs2.Keys)
					{
						if (dictionary.ContainsKey(text2))
						{
							Dictionary<string, int> dictionary2;
							string key4;
							(dictionary2 = dictionary)[key4 = text2] = dictionary2[key4] + effectIDs2[text2];
						}
						else
						{
							dictionary.Add(text2, effectIDs2[text2]);
						}
					}
				}
			}
			return dictionary;
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000855 RID: 2133 RVA: 0x000368C0 File Offset: 0x00034CC0
		public CharacterBattleParam Param
		{
			get
			{
				return this.m_Param;
			}
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x000368C8 File Offset: 0x00034CC8
		public void SetupParams(float[] townBuffParams)
		{
			this.m_Param = new CharacterBattleParam();
			this.m_Param.SetupParams(this.m_Owner, townBuffParams);
			this.SetupCommandDatas(null);
			this.ResetOldSingleTargetIndices();
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x000368F4 File Offset: 0x00034CF4
		public void SetupParams(BattleSystemDataForPL bsdForPL, float[] townBuffParams)
		{
			this.m_Param = bsdForPL.Param;
			this.m_Param.SetupParams(this.m_Owner, townBuffParams);
			this.SetupCommandDatas(bsdForPL.Cmds);
			this.ResetOldSingleTargetIndices();
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x00036928 File Offset: 0x00034D28
		public void ResetAllParameter()
		{
			this.m_Param.SetDeadDetail(BattleDefine.eDeadDetail.Alive);
			this.m_Param.RecoveryFullHp();
			this.m_Param.ResetResistElementAll();
			this.m_Param.ResetStatusBuffAll();
			this.m_Param.ResetNextBuffAll();
			this.m_Param.ResetStateAbnormalAll();
			this.m_Param.ResetStateAbnormalDisableBuff();
			this.m_Param.ResetStateAbnormalAddProbabilityBuff();
			this.m_Param.ResetWeakElementBonusBuff();
			this.m_Param.ResetHateChange();
			this.m_Param.ResetRegene();
			this.m_Param.ResetBarrier();
			this.m_Param.GuardOff();
			this.m_Param.MemberChangeRecastZero();
			this.RecastZero(false);
			this.ResetStun();
			this.ResetStuner();
			this.m_Param.SetWasAttacked(false);
			this.m_Param.SetWasAttackedOnSleeping(false);
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x000369FC File Offset: 0x00034DFC
		private void ResetOnDead()
		{
			this.m_Param.ResetResistElementAll();
			this.m_Param.ResetStatusBuffAll();
			this.m_Param.ResetNextBuffAll();
			this.m_Param.ResetStateAbnormalAll();
			this.m_Param.ResetStateAbnormalDisableBuff();
			this.m_Param.ResetStateAbnormalAddProbabilityBuff();
			this.m_Param.ResetWeakElementBonusBuff();
			this.m_Param.ResetHateChange();
			this.m_Param.ResetHateChange();
			this.m_Param.ResetBarrier();
			this.m_Param.GuardOff();
			this.m_Param.MemberChangeRecastZero();
			this.RecastZero(false);
			this.ResetStun();
			this.ResetStuner();
			this.m_Param.SetWasAttacked(false);
			this.m_Param.SetWasAttackedOnSleeping(false);
		}

		// Token: 0x0600085A RID: 2138 RVA: 0x00036AB8 File Offset: 0x00034EB8
		public void ResetOnRevival()
		{
			this.ResetAllParameter();
		}

		// Token: 0x0600085B RID: 2139 RVA: 0x00036AC0 File Offset: 0x00034EC0
		public void WavePreProcess(int waveIndex)
		{
			this.m_Param.GuardOff();
			this.ResetStun();
			this.ResetStuner();
		}

		// Token: 0x0600085C RID: 2140 RVA: 0x00036ADC File Offset: 0x00034EDC
		public void TurnStartProcess(CharacterHandler turnOwner)
		{
			if (this.m_Param.IsDead())
			{
				return;
			}
			bool flag = turnOwner == this.m_Owner;
			if (flag)
			{
				this.m_Param.GuardOff();
				this.CalcStunValue(BattleCommandParser.CalcStunDecreaseValueOnTurnStart());
			}
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x00036B24 File Offset: 0x00034F24
		public void TurnEndProcess(CharacterHandler turnOwner)
		{
			this.m_Param.UpdateDeadDetailIfReady();
			if (this.m_Param.IsDead())
			{
				return;
			}
			bool flag = turnOwner == this.m_Owner;
			bool flag2 = this.m_Param.IsEnableStateAbnormalAny();
			this.m_Param.DecreaseTurnResistElementAll(flag);
			this.m_Param.DecreaseTurnStatusBuffAll(flag);
			this.m_Param.DecreaseTurnStateAbnormalAll(flag);
			this.m_Param.GetStateAbnormalDisableBuff().DecreaseTurn(flag);
			this.m_Param.GetStateAbnormalAddProbabilityBuff().DecreaseTurn(flag);
			if (this.m_Param.CanPossibleCancelSleep())
			{
				this.m_Param.ResetStateAbnormal(eStateAbnormal.Sleep);
			}
			if (this.m_Param.WasAttacked())
			{
				this.m_Param.DecreaseBarrier();
			}
			this.m_Param.ResetNextBuffIfUsed();
			this.m_Param.GetWeakElementBonusBuff().DecreaseTurn(flag);
			this.m_Param.GetHateChange().DecreaseTurn(flag);
			this.m_Param.GetRegene().DecreaseTurn(flag);
			this.ResetStuner();
			this.RecastDecrement();
			bool flag3 = false;
			if (flag)
			{
				flag3 = this.ResetStunIfStun();
				if (this.m_ExecCommand != null)
				{
					eSkillTargetType mainSkillTargetType = this.m_ExecCommand.MainSkillTargetType;
					if (mainSkillTargetType != eSkillTargetType.TgtSingle)
					{
						if (mainSkillTargetType == eSkillTargetType.MySingle)
						{
							this.m_OldSingleTargetIndices[0] = this.MyParty.SingleTargetIndex;
						}
					}
					else
					{
						this.m_OldSingleTargetIndices[1] = this.MyParty.SingleTargetIndex;
					}
					if (this.m_ExecCommand == this.GetUniqueSkillCommandData())
					{
						this.m_Param.SetChargeCount(0);
					}
					else
					{
						this.m_Param.CalcChargeCount(1);
					}
					this.m_ExecCommand.RecastFull();
				}
				if (this.IsEnableAI())
				{
					this.ApplyAIWriteFlag();
				}
			}
			if (this.m_ExecCommand != null)
			{
				this.m_ExecCommand.SolveResult.Clear();
				this.m_ExecCommand = null;
			}
			this.m_Param.SetWasAttacked(false);
			this.m_Param.SetWasAttackedOnSleeping(false);
			if (!this.m_Param.IsEnableStateAbnormalAny() && !this.IsStun() && (flag2 || flag3))
			{
				this.RequestIdleMode(0.25f, 0f, false);
			}
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x00036D54 File Offset: 0x00035154
		public sbyte[] GetFlagForUIStatusIcon()
		{
			sbyte[] array = new sbyte[30];
			if (this.IsStun())
			{
				array[0] = 1;
			}
			else
			{
				array[0] = 0;
			}
			int[] statusBuffFlags = this.m_Param.GetStatusBuffFlags();
			for (int i = 0; i < statusBuffFlags.Length; i++)
			{
				array[1 + i] = (sbyte)statusBuffFlags[i];
			}
			bool[] stateAbnormalFlags = this.m_Param.GetStateAbnormalFlags();
			for (int j = 0; j < stateAbnormalFlags.Length; j++)
			{
				array[7 + j] = ((!stateAbnormalFlags[j]) ? 0 : 1);
			}
			int[] resistElementFlags = this.m_Param.GetResistElementFlags();
			for (int k = 0; k < resistElementFlags.Length; k++)
			{
				array[15 + k] = (sbyte)resistElementFlags[k];
			}
			float stateAbnormalAddProbabilityBuffValue = this.m_Param.GetStateAbnormalAddProbabilityBuffValue();
			if (stateAbnormalAddProbabilityBuffValue > 0f)
			{
				array[21] = 1;
			}
			else if (stateAbnormalAddProbabilityBuffValue < 0f)
			{
				array[21] = -1;
			}
			else
			{
				array[21] = 0;
			}
			bool stateAbnormalDisableBuffValue = this.m_Param.GetStateAbnormalDisableBuffValue();
			if (stateAbnormalDisableBuffValue)
			{
				array[22] = 1;
			}
			else
			{
				array[22] = 0;
			}
			float weakElementBonusBuffValue = this.m_Param.GetWeakElementBonusBuffValue();
			if (weakElementBonusBuffValue > 0f)
			{
				array[23] = 1;
			}
			else
			{
				array[23] = 0;
			}
			float nextBuffCoef = this.m_Param.GetNextBuffCoef(BattleDefine.eNextBuff.Attack);
			if (nextBuffCoef > 0f)
			{
				array[24] = 1;
			}
			else
			{
				array[24] = 0;
			}
			float nextBuffCoef2 = this.m_Param.GetNextBuffCoef(BattleDefine.eNextBuff.Magic);
			if (nextBuffCoef2 > 0f)
			{
				array[25] = 1;
			}
			else
			{
				array[25] = 0;
			}
			float nextBuffCoef3 = this.m_Param.GetNextBuffCoef(BattleDefine.eNextBuff.Critical);
			if (nextBuffCoef3 > 0f)
			{
				array[26] = 1;
			}
			else
			{
				array[26] = 0;
			}
			float barrierCutRatio = this.m_Param.GetBarrierCutRatio();
			if (barrierCutRatio > 0f)
			{
				array[27] = 1;
			}
			else
			{
				array[27] = 0;
			}
			float hateChangeValue = this.m_Param.GetHateChangeValue();
			if (hateChangeValue > 0f)
			{
				array[28] = 1;
			}
			else if (hateChangeValue < 0f)
			{
				array[28] = -1;
			}
			else
			{
				array[28] = 0;
			}
			if (this.m_Param.GetRegene().IsEnable())
			{
				array[29] = 1;
			}
			else
			{
				array[29] = 0;
			}
			return array;
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x00036FA8 File Offset: 0x000353A8
		public bool CanBeTogetherAttack()
		{
			return this.MyParty.IsPlayerParty && !this.m_Param.IsEnableStateAbnormal(eStateAbnormal.Paralysis) && !this.m_Param.IsEnableStateAbnormal(eStateAbnormal.Sleep) && !this.m_Param.IsEnableStateAbnormal(eStateAbnormal.Silence) && !this.IsStun() && !this.m_Param.IsDead();
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000860 RID: 2144 RVA: 0x00037020 File Offset: 0x00035420
		public int CommandNum
		{
			get
			{
				return (this.m_Commands == null) ? 0 : this.m_Commands.Count;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000861 RID: 2145 RVA: 0x0003703E File Offset: 0x0003543E
		public BattleCommandData ExecCommand
		{
			get
			{
				return this.m_ExecCommand;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000862 RID: 2146 RVA: 0x00037046 File Offset: 0x00035446
		public int TogetherAttackChainCount
		{
			get
			{
				return this.m_TogetherAttackChainCount;
			}
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x00037050 File Offset: 0x00035450
		private void SetupCommandDatas(BattleCommandData[] pool)
		{
			this.m_Commands = new List<BattleCommandData>();
			if (this.m_Owner.IsUserControll)
			{
				int num = 0;
				BattleCommandData battleCommandData = new BattleCommandData();
				battleCommandData.Setup(BattleCommandData.eCommandType.NormalAttack, num++, this.m_Owner, 0, 1, 1, 0L, -1, true);
				this.m_Commands.Add(battleCommandData);
				for (int i = 0; i < this.m_Owner.CharaParam.ClassSkillLearnDatas.Count; i++)
				{
					if (pool != null && i < pool.Length)
					{
						battleCommandData = pool[i];
					}
					else
					{
						battleCommandData = new BattleCommandData();
					}
					battleCommandData.Setup(BattleCommandData.eCommandType.Skill, num++, this.m_Owner, this.m_Owner.CharaParam.ClassSkillLearnDatas[i].SkillID, this.m_Owner.CharaParam.ClassSkillLearnDatas[i].SkillLv, this.m_Owner.CharaParam.ClassSkillLearnDatas[i].SkillMaxLv, this.m_Owner.CharaParam.ClassSkillLearnDatas[i].TotalUseNum, this.m_Owner.CharaParam.ClassSkillLearnDatas[i].ExpTableID, true);
					this.m_Commands.Add(battleCommandData);
				}
				if (this.m_Owner.WeaponParam != null && this.m_Owner.WeaponParam.SkillLearnData.SkillID != -1)
				{
					if (pool != null && this.m_Commands.Count - 1 < pool.Length)
					{
						battleCommandData = pool[this.m_Commands.Count - 1];
					}
					else
					{
						battleCommandData = new BattleCommandData();
					}
					battleCommandData.Setup(BattleCommandData.eCommandType.WeaponSkill, num++, this.m_Owner, this.m_Owner.WeaponParam.SkillLearnData.SkillID, this.m_Owner.WeaponParam.SkillLearnData.SkillLv, this.m_Owner.WeaponParam.SkillLearnData.SkillMaxLv, this.m_Owner.WeaponParam.SkillLearnData.TotalUseNum, this.m_Owner.WeaponParam.SkillLearnData.ExpTableID, true);
					this.m_Commands.Add(battleCommandData);
				}
				if (pool != null && this.m_Commands.Count - 1 < pool.Length)
				{
					this.m_UniqueSkillCommand = pool[this.m_Commands.Count - 1];
				}
				else
				{
					this.m_UniqueSkillCommand = new BattleCommandData();
				}
				this.m_UniqueSkillCommand.Setup(BattleCommandData.eCommandType.UniqueSkill, -1, this.m_Owner, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillID, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillLv, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillMaxLv, this.m_Owner.CharaParam.UniqueSkillLearnData.TotalUseNum, this.m_Owner.CharaParam.UniqueSkillLearnData.ExpTableID, false);
			}
			else
			{
				for (int j = 0; j < this.m_Owner.CharaParam.ClassSkillLearnDatas.Count; j++)
				{
					BattleCommandData battleCommandData = new BattleCommandData();
					battleCommandData.Setup(BattleCommandData.eCommandType.Skill, j, this.m_Owner, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].SkillID, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].SkillLv, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].SkillMaxLv, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].TotalUseNum, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].ExpTableID, this.m_Owner.CharaParam.ClassSkillLearnDatas[j].IsConfusionSkillID);
					this.m_Commands.Add(battleCommandData);
				}
				this.m_UniqueSkillCommand = new BattleCommandData();
				this.m_UniqueSkillCommand.Setup(BattleCommandData.eCommandType.UniqueSkill, -1, this.m_Owner, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillID, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillLv, this.m_Owner.CharaParam.UniqueSkillLearnData.SkillMaxLv, this.m_Owner.CharaParam.UniqueSkillLearnData.TotalUseNum, this.m_Owner.CharaParam.UniqueSkillLearnData.ExpTableID, this.m_Owner.CharaParam.UniqueSkillLearnData.IsConfusionSkillID);
			}
		}

		// Token: 0x06000864 RID: 2148 RVA: 0x000374C0 File Offset: 0x000358C0
		public List<BattleCommandData> GetCommandDatas()
		{
			return this.m_Commands;
		}

		// Token: 0x06000865 RID: 2149 RVA: 0x000374C8 File Offset: 0x000358C8
		public BattleCommandData GetCommandDataAt(int commandIndex)
		{
			return this.m_Commands[commandIndex];
		}

		// Token: 0x06000866 RID: 2150 RVA: 0x000374D8 File Offset: 0x000358D8
		public List<BattleCommandData> GetUniqueSkillCommandDatas()
		{
			return new List<BattleCommandData>
			{
				this.m_UniqueSkillCommand
			};
		}

		// Token: 0x06000867 RID: 2151 RVA: 0x000374F8 File Offset: 0x000358F8
		public BattleCommandData GetUniqueSkillCommandData()
		{
			return this.m_UniqueSkillCommand;
		}

		// Token: 0x06000868 RID: 2152 RVA: 0x00037500 File Offset: 0x00035900
		public bool CanBeExecCommand(int commandIndex)
		{
			BattleCommandData battleCommandData = this.m_Commands[commandIndex];
			switch (battleCommandData.CommandType)
			{
			case BattleCommandData.eCommandType.Skill:
			case BattleCommandData.eCommandType.WeaponSkill:
			case BattleCommandData.eCommandType.UniqueSkill:
				if (this.m_Param.IsEnableStateAbnormal(eStateAbnormal.Silence))
				{
					return false;
				}
				break;
			}
			return battleCommandData.RecastVal <= 0;
		}

		// Token: 0x06000869 RID: 2153 RVA: 0x00037574 File Offset: 0x00035974
		public void RecastFull()
		{
			for (int i = 0; i < this.m_Commands.Count; i++)
			{
				this.m_Commands[i].RecastFull();
			}
		}

		// Token: 0x0600086A RID: 2154 RVA: 0x000375B0 File Offset: 0x000359B0
		public void RecastZero(bool recastRecoveredFlg = false)
		{
			for (int i = 0; i < this.m_Commands.Count; i++)
			{
				this.m_Commands[i].RecastZero(recastRecoveredFlg);
			}
		}

		// Token: 0x0600086B RID: 2155 RVA: 0x000375EC File Offset: 0x000359EC
		public void RecastDecrement()
		{
			for (int i = 0; i < this.m_Commands.Count; i++)
			{
				this.m_Commands[i].CalcRecast(-1);
			}
		}

		// Token: 0x0600086C RID: 2156 RVA: 0x00037628 File Offset: 0x00035A28
		public void RecastChange(float ratio)
		{
			for (int i = 0; i < this.m_Commands.Count; i++)
			{
				this.m_Commands[i].CalcRecast((int)((float)this.m_Commands[i].RecastMax * ratio));
			}
		}

		// Token: 0x0600086D RID: 2157 RVA: 0x00037678 File Offset: 0x00035A78
		public bool IsStun()
		{
			return this.m_IsStun;
		}

		// Token: 0x0600086E RID: 2158 RVA: 0x00037680 File Offset: 0x00035A80
		public float GetStunRatio()
		{
			return this.m_StunValue / BattleUtility.DefVal(eBattleDefineDB.StunValueMax);
		}

		// Token: 0x0600086F RID: 2159 RVA: 0x00037690 File Offset: 0x00035A90
		public float GetStunValue()
		{
			return this.m_StunValue;
		}

		// Token: 0x06000870 RID: 2160 RVA: 0x00037698 File Offset: 0x00035A98
		public float CalcStunValue(float value)
		{
			this.m_StunValue += value;
			this.m_StunValue = Mathf.Clamp(this.m_StunValue, 0f, BattleUtility.DefVal(eBattleDefineDB.StunValueMax));
			return this.m_StunValue;
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x000376CB File Offset: 0x00035ACB
		public bool JudgeAndApplyStun()
		{
			if (!this.m_IsStun && this.GetStunRatio() >= 1f)
			{
				this.m_IsStun = true;
				return true;
			}
			return false;
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x000376F2 File Offset: 0x00035AF2
		public bool ResetStunIfStun()
		{
			if (this.m_IsStun)
			{
				this.ResetStun();
				return true;
			}
			return false;
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x00037708 File Offset: 0x00035B08
		public void ResetStun()
		{
			this.m_IsStun = false;
			this.m_StunValue = 0f;
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x0003771C File Offset: 0x00035B1C
		public bool IsStunerJudged()
		{
			return this.m_IsStunerJudged;
		}

		// Token: 0x06000875 RID: 2165 RVA: 0x00037724 File Offset: 0x00035B24
		public void SetIsStunerJudged(bool isStunerJudged)
		{
			this.m_IsStunerJudged = isStunerJudged;
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x0003772D File Offset: 0x00035B2D
		public bool IsStunerApply()
		{
			return this.m_IsStunerApply;
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x00037735 File Offset: 0x00035B35
		public void SetIsStunerApply(bool isStunerApply)
		{
			this.m_IsStunerApply = isStunerApply;
		}

		// Token: 0x06000878 RID: 2168 RVA: 0x0003773E File Offset: 0x00035B3E
		public void ResetStuner()
		{
			this.m_IsStunerJudged = false;
			this.m_IsStunerApply = false;
		}

		// Token: 0x06000879 RID: 2169 RVA: 0x00037750 File Offset: 0x00035B50
		public void AttachStunOccurEffect()
		{
			if (this.m_IsInUniqueSkillScene)
			{
				return;
			}
			int sortingOrder = this.m_Owner.CharaAnim.GetSortingOrder() + 1;
			if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
			{
				Transform cacheTransform = this.m_Owner.CacheTransform;
				Vector3 pl_STUN_OFFSET = BattleDefine.PL_STUN_OFFSET;
				this.m_StunOccurEffectHandler = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged("ef_btl_stun_occur", pl_STUN_OFFSET, Quaternion.Euler(0f, 180f, 0f), cacheTransform, true, WrapMode.ClampForever, sortingOrder, 1f, false);
			}
			else
			{
				Transform locator = this.m_Owner.CharaAnim.GetLocator(CharacterDefine.eLocatorIndex.OverHead);
				Vector3 zero = Vector3.zero;
				this.m_StunOccurEffectHandler = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged("ef_btl_stun_occur", zero, Quaternion.Euler(0f, 0f, 0f), locator, true, WrapMode.ClampForever, sortingOrder, 1f, false);
			}
			if (this.m_StunOccurEffectHandler != null)
			{
				if (this.m_Owner.CharaAnim.Dir != CharacterDefine.eDir.L)
				{
					this.m_StunOccurEffectHandler.CacheTransform.localScale = new Vector3(-1f, 1f, 1f);
				}
				else
				{
					this.m_StunOccurEffectHandler.CacheTransform.localScale = Vector3.one;
				}
				Transform transform = this.m_StunOccurEffectHandler.m_MsbHndl.transform.Find("pl_tarai");
				Transform transform2 = this.m_StunOccurEffectHandler.m_MsbHndl.transform.Find("en_book");
				if (transform != null && transform2 != null)
				{
					transform.gameObject.SetActive(this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player);
					transform2.gameObject.SetActive(this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Enemy);
				}
			}
		}

		// Token: 0x0600087A RID: 2170 RVA: 0x00037928 File Offset: 0x00035D28
		private void DetachStunOccurEffect()
		{
			if (this.m_StunOccurEffectHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_StunOccurEffectHandler);
				this.m_StunOccurEffectHandler = null;
			}
		}

		// Token: 0x0600087B RID: 2171 RVA: 0x00037958 File Offset: 0x00035D58
		private void AttachStunEffect()
		{
			if (this.m_IsInUniqueSkillScene)
			{
				return;
			}
			if (this.m_StunEffectHandler == null)
			{
				int sortingOrder = this.m_Owner.CharaAnim.GetSortingOrder() + 1;
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					Vector3 pl_STUN_OFFSET = BattleDefine.PL_STUN_OFFSET;
					this.m_StunEffectHandler = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged("ef_btl_stun", pl_STUN_OFFSET, Quaternion.Euler(0f, 180f, 0f), this.m_Owner.CacheTransform, true, WrapMode.Loop, sortingOrder, 1f, false);
				}
				else
				{
					Transform locator = this.m_Owner.CharaAnim.GetLocator(CharacterDefine.eLocatorIndex.OverHead);
					Vector3 zero = Vector3.zero;
					this.m_StunEffectHandler = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged("ef_btl_stun", zero, Quaternion.Euler(0f, 0f, 0f), locator, true, WrapMode.Loop, sortingOrder, 1f, false);
				}
				if (this.m_Owner.CharaAnim.Dir == CharacterDefine.eDir.L)
				{
					this.m_StunEffectHandler.CacheTransform.localScaleX(1f);
				}
				else
				{
					this.m_StunEffectHandler.CacheTransform.localScaleX(-1f);
				}
			}
		}

		// Token: 0x0600087C RID: 2172 RVA: 0x00037A8D File Offset: 0x00035E8D
		private void DetachStunEffect()
		{
			if (this.m_StunEffectHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_StunEffectHandler);
				this.m_StunEffectHandler = null;
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600087D RID: 2173 RVA: 0x00037ABC File Offset: 0x00035EBC
		public BattleAIData BattleAIData
		{
			get
			{
				return this.m_BattleAIData;
			}
		}

		// Token: 0x0600087E RID: 2174 RVA: 0x00037AC4 File Offset: 0x00035EC4
		private void SetupAI()
		{
			this.m_BattleAIData = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FindBattleAIData(this.m_Owner.CharaParam.AIID);
		}

		// Token: 0x0600087F RID: 2175 RVA: 0x00037AEB File Offset: 0x00035EEB
		public bool IsEnableAI()
		{
			return this.m_Owner.CharaParam.AIID > -1;
		}

		// Token: 0x06000880 RID: 2176 RVA: 0x00037B06 File Offset: 0x00035F06
		public void AddAIPatternChangeHitIndex(int patternChangeHitIndex)
		{
			if (!this.m_AIPatternChangeHitIndices.ContainsKey(patternChangeHitIndex))
			{
				this.m_AIPatternChangeHitIndices.Add(patternChangeHitIndex, true);
			}
		}

		// Token: 0x06000881 RID: 2177 RVA: 0x00037B26 File Offset: 0x00035F26
		public bool IsAIPatternChangeHitIndex(int patternChangeHitIndex)
		{
			return this.m_AIPatternChangeHitIndices.ContainsKey(patternChangeHitIndex);
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x00037B34 File Offset: 0x00035F34
		public void SetAIPatternIndex(int patternIndex)
		{
			this.m_AIPatternIndex = patternIndex;
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x00037B3D File Offset: 0x00035F3D
		public int GetAIPatternIndex()
		{
			return this.m_AIPatternIndex;
		}

		// Token: 0x06000884 RID: 2180 RVA: 0x00037B45 File Offset: 0x00035F45
		public List<BattleAIFlag> GetAIFlags()
		{
			return this.m_AIFlags;
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x00037B50 File Offset: 0x00035F50
		public bool GetAIFlag(int flagID)
		{
			for (int i = 0; i < this.m_AIFlags.Count; i++)
			{
				if (this.m_AIFlags[i].m_ID == flagID)
				{
					return this.m_AIFlags[i].m_IsFlag;
				}
			}
			return false;
		}

		// Token: 0x06000886 RID: 2182 RVA: 0x00037BA4 File Offset: 0x00035FA4
		public bool JudgeAIFlags(List<BattleAIFlag> condFlags)
		{
			for (int i = 0; i < condFlags.Count; i++)
			{
				bool aiflag = this.GetAIFlag(condFlags[i].m_ID);
				if (aiflag != condFlags[i].m_IsFlag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000887 RID: 2183 RVA: 0x00037BF0 File Offset: 0x00035FF0
		public void SetAIWriteFlags(List<BattleAIFlag> aiWriteFlags)
		{
			this.m_AIWriteFlags = aiWriteFlags;
		}

		// Token: 0x06000888 RID: 2184 RVA: 0x00037BF9 File Offset: 0x00035FF9
		public List<BattleAIFlag> GetAIWriteFlags()
		{
			return this.m_AIWriteFlags;
		}

		// Token: 0x06000889 RID: 2185 RVA: 0x00037C04 File Offset: 0x00036004
		public void ApplyAIWriteFlag()
		{
			if (this.m_AIWriteFlags == null)
			{
				return;
			}
			for (int i = 0; i < this.m_AIWriteFlags.Count; i++)
			{
				int id = this.m_AIWriteFlags[i].m_ID;
				bool isFlag = this.m_AIWriteFlags[i].m_IsFlag;
				int num = -1;
				for (int j = 0; j < this.m_AIFlags.Count; j++)
				{
					if (this.m_AIFlags[j].m_ID == id)
					{
						num = j;
						break;
					}
				}
				if (num == -1)
				{
					BattleAIFlag battleAIFlag = new BattleAIFlag();
					battleAIFlag.m_ID = id;
					battleAIFlag.m_IsFlag = isFlag;
					this.m_AIFlags.Add(battleAIFlag);
				}
				else
				{
					this.m_AIFlags[num].m_ID = id;
					this.m_AIFlags[num].m_IsFlag = isFlag;
				}
			}
		}

		// Token: 0x0600088A RID: 2186 RVA: 0x00037CF4 File Offset: 0x000360F4
		[Conditional("APP_DEBUG")]
		public void LogAIFlags()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[AI] フラグ状態 :  ");
			for (int i = 0; i < this.m_AIFlags.Count; i++)
			{
				if (i != 0)
				{
					stringBuilder.Append(",");
				}
				stringBuilder.Append(this.m_AIFlags[i].m_ID);
				stringBuilder.Append(":");
				stringBuilder.Append((!this.m_AIFlags[i].m_IsFlag) ? "OFF" : "ON");
			}
		}

		// Token: 0x0600088B RID: 2187 RVA: 0x00037D94 File Offset: 0x00036194
		[Conditional("APP_DEBUG")]
		public void LogAIWriteFlags()
		{
			if (this.m_AIWriteFlags == null)
			{
				return;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[AI] 今回書き込みフラグ :  ");
			for (int i = 0; i < this.m_AIWriteFlags.Count; i++)
			{
				if (i != 0)
				{
					stringBuilder.Append(",");
				}
				stringBuilder.Append(this.m_AIWriteFlags[i].m_ID);
				stringBuilder.Append(":");
				stringBuilder.Append((!this.m_AIWriteFlags[i].m_IsFlag) ? "OFF" : "ON");
			}
		}

		// Token: 0x0600088C RID: 2188 RVA: 0x00037E40 File Offset: 0x00036240
		public int GetAIExecedNum(int patternIndex, int conditionIndex)
		{
			string aiexecedKey = CharacterBattle.GetAIExecedKey(patternIndex, conditionIndex);
			if (this.m_AIExecedNums.ContainsKey(aiexecedKey))
			{
				return this.m_AIExecedNums[aiexecedKey];
			}
			return 0;
		}

		// Token: 0x0600088D RID: 2189 RVA: 0x00037E74 File Offset: 0x00036274
		public void SetAIExecedNum(int patternIndex, int conditionIndex, int num)
		{
			string aiexecedKey = CharacterBattle.GetAIExecedKey(patternIndex, conditionIndex);
			this.m_AIExecedNums.AddOrReplace(aiexecedKey, num);
		}

		// Token: 0x0600088E RID: 2190 RVA: 0x00037E98 File Offset: 0x00036298
		public void IncrementAIExecedNum(int patternIndex, int conditionIndex)
		{
			string aiexecedKey = CharacterBattle.GetAIExecedKey(patternIndex, conditionIndex);
			if (this.m_AIExecedNums.ContainsKey(aiexecedKey))
			{
				Dictionary<string, int> aiexecedNums;
				string key;
				(aiexecedNums = this.m_AIExecedNums)[key = aiexecedKey] = aiexecedNums[key] + 1;
			}
			else
			{
				this.m_AIExecedNums.Add(aiexecedKey, 1);
			}
		}

		// Token: 0x0600088F RID: 2191 RVA: 0x00037EE9 File Offset: 0x000362E9
		private static string GetAIExecedKey(int patternIndex, int condtionIndex)
		{
			return string.Format("{0}_{1}", patternIndex, condtionIndex);
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000890 RID: 2192 RVA: 0x00037F01 File Offset: 0x00036301
		// (set) Token: 0x06000891 RID: 2193 RVA: 0x00037F09 File Offset: 0x00036309
		public bool IsMoveTo { get; set; }

		// Token: 0x06000892 RID: 2194 RVA: 0x00037F14 File Offset: 0x00036314
		public Vector3 GetMoveTargetPosOnAttack()
		{
			Vector3 vector = this.m_Owner.CacheTransform.localPosition;
			if (this.m_Owner.CharaAnim.Dir == CharacterDefine.eDir.R)
			{
				vector += new Vector3(0.5f, -0.001f, 0f);
			}
			else
			{
				vector -= new Vector3(0.5f, -0.001f, 0f);
			}
			return vector;
		}

		// Token: 0x06000893 RID: 2195 RVA: 0x00037F84 File Offset: 0x00036384
		public void MoveToPosition(Vector3 startPos, Vector3 endPos, float speed)
		{
			this.IsMoveTo = true;
			this.m_Owner.CacheTransform.localPosition = startPos;
			iTween.MoveTo(this.m_Owner.gameObject, iTween.Hash(new object[]
			{
				"x",
				endPos.x,
				"y",
				endPos.y,
				"z",
				endPos.z,
				"islocal",
				true,
				"speed",
				speed,
				"easetype",
				iTween.EaseType.linear,
				"oncompletetarget",
				this.m_Owner.gameObject,
				"oncomplete",
				"OnCompleteMoveTo"
			}));
		}

		// Token: 0x06000894 RID: 2196 RVA: 0x0003806C File Offset: 0x0003646C
		public void MoveToPositionTime(Vector3 startPos, Vector3 endPos, float time, float delay)
		{
			this.IsMoveTo = true;
			this.m_Owner.CacheTransform.localPosition = startPos;
			iTween.MoveTo(this.m_Owner.gameObject, iTween.Hash(new object[]
			{
				"x",
				endPos.x,
				"y",
				endPos.y,
				"z",
				endPos.z,
				"islocal",
				true,
				"time",
				time,
				"delay",
				delay,
				"easetype",
				iTween.EaseType.linear,
				"oncompletetarget",
				this.m_Owner.gameObject,
				"oncomplete",
				"OnCompleteMoveTo"
			}));
		}

		// Token: 0x06000895 RID: 2197 RVA: 0x00038168 File Offset: 0x00036568
		public void MoveToPositionFromCurrent(Vector3 endPos, float speed)
		{
			this.IsMoveTo = true;
			iTween.MoveTo(this.m_Owner.gameObject, iTween.Hash(new object[]
			{
				"x",
				endPos.x,
				"y",
				endPos.y,
				"z",
				endPos.z,
				"islocal",
				true,
				"speed",
				speed,
				"easetype",
				iTween.EaseType.linear,
				"oncompletetarget",
				this.m_Owner.gameObject,
				"oncomplete",
				"OnCompleteMoveTo"
			}));
		}

		// Token: 0x06000896 RID: 2198 RVA: 0x0003823E File Offset: 0x0003663E
		public BattleDefine.eJoinMember GetOldSingleTargetIndex(BattleDefine.ePartyType partyType)
		{
			return this.m_OldSingleTargetIndices[(int)partyType];
		}

		// Token: 0x06000897 RID: 2199 RVA: 0x00038248 File Offset: 0x00036648
		public BattleDefine.eJoinMember GetOldSingleTargetIndex(eSkillTargetType skillTargetType)
		{
			if (skillTargetType == eSkillTargetType.TgtSingle)
			{
				return this.m_OldSingleTargetIndices[1];
			}
			if (skillTargetType != eSkillTargetType.MySingle)
			{
				return BattleDefine.eJoinMember.None;
			}
			return this.m_OldSingleTargetIndices[0];
		}

		// Token: 0x06000898 RID: 2200 RVA: 0x00038270 File Offset: 0x00036670
		private void ResetOldSingleTargetIndices()
		{
			for (int i = 0; i < this.m_OldSingleTargetIndices.Length; i++)
			{
				this.m_OldSingleTargetIndices[i] = BattleDefine.eJoinMember.None;
			}
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x0003829F File Offset: 0x0003669F
		public void ChangeMeshColor(Color startColor, Color endColor, float sec)
		{
			this.m_Owner.CharaAnim.ChangeMeshColor(startColor, endColor, sec);
			if (this.m_StunEffectHandler != null)
			{
				this.m_StunEffectHandler.ChangeMeshColor(startColor, endColor, sec);
			}
		}

		// Token: 0x0600089A RID: 2202 RVA: 0x000382D3 File Offset: 0x000366D3
		public void ResetMeshColor()
		{
			this.m_Owner.CharaAnim.ResetMeshColor();
			if (this.m_StunEffectHandler != null)
			{
				this.m_StunEffectHandler.ResetMeshColor();
			}
		}

		// Token: 0x0600089B RID: 2203 RVA: 0x00038304 File Offset: 0x00036704
		public void SetFixRootBoneTransform(bool flg)
		{
			this.m_IsFixRootBoneTransform = flg;
			if (this.m_IsFixRootBoneTransform && this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
					{
						Transform transform = null;
						if (this.m_Owner.CharaAnim.ModelSets[0] != null)
						{
							transform = this.m_Owner.CharaAnim.ModelSets[0].m_RootTransform;
						}
						Transform transform2 = null;
						if (this.m_Owner.CharaAnim.ModelSets[1] != null)
						{
							transform2 = this.m_Owner.CharaAnim.ModelSets[1].m_RootTransform;
						}
						if (transform != null && transform2 != null)
						{
							transform2.localPosition = Vector3.zero;
							Transform transform3 = transform2.Find("Hips/Spine1/Spine2/Neck/Head");
							if (transform3 != null)
							{
								transform.localPosition = transform.InverseTransformPoint(transform3.position);
							}
						}
					}
					else if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_RootTransform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_RootTransform.localPosition = Vector3.zero;
					}
				}
			}
		}

		// Token: 0x0600089C RID: 2204 RVA: 0x00038480 File Offset: 0x00036880
		private void UpdateFixRootBoneTransform()
		{
			if (this.m_IsFixRootBoneTransform && this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
					{
						Transform transform = null;
						if (this.m_Owner.CharaAnim.ModelSets[0] != null)
						{
							transform = this.m_Owner.CharaAnim.ModelSets[0].m_RootTransform;
						}
						Transform transform2 = null;
						if (this.m_Owner.CharaAnim.ModelSets[1] != null)
						{
							transform2 = this.m_Owner.CharaAnim.ModelSets[1].m_RootTransform;
						}
						if (transform != null && transform2 != null)
						{
							transform2.localPosition = Vector3.zero;
							Transform transform3 = transform2.Find("Hips/Spine1/Spine2/Neck/Head");
							if (transform3 != null)
							{
								transform.position = transform3.position;
							}
						}
					}
					else if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_RootTransform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_RootTransform.localPosition = Vector3.zero;
					}
				}
			}
		}

		// Token: 0x0600089D RID: 2205 RVA: 0x000385ED File Offset: 0x000369ED
		public void PlayCharaTouchVoice()
		{
			if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
			{
				this.m_Owner.PlayVoice(eSoundVoiceListDB.voice_420, 1f);
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600089E RID: 2206 RVA: 0x00038619 File Offset: 0x00036A19
		public CharacterBattle.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x0600089F RID: 2207 RVA: 0x00038624 File Offset: 0x00036A24
		public void RequestIdleMode(float blendSec = 0f, float delaySec = 0f, bool isStunScaleAnim = false)
		{
			this.m_Mode = CharacterBattle.eMode.Idle;
			this.m_UpdateIdleStep = CharacterBattle.eUpdateIdleStep.ExecAnimation;
			this.m_BlendSec = blendSec;
			this.m_DelaySec = delaySec;
			this.m_IdleIsStunScaleAnim = isStunScaleAnim;
			if (this.Param.IsEnableStateAbnormalAny() || this.IsStun())
			{
				this.m_Owner.CharaAnim.SetBlink(false);
			}
			else
			{
				this.m_Owner.CharaAnim.SetBlink(true);
			}
		}

		// Token: 0x060008A0 RID: 2208 RVA: 0x00038698 File Offset: 0x00036A98
		private void UpdateIdleMode()
		{
			CharacterBattle.eUpdateIdleStep updateIdleStep = this.m_UpdateIdleStep;
			if (updateIdleStep != CharacterBattle.eUpdateIdleStep.ExecAnimation)
			{
				if (updateIdleStep != CharacterBattle.eUpdateIdleStep.ExecAnimation_Wait)
				{
				}
			}
			else
			{
				this.m_Owner.SetEnableRenderWeapon(true);
				if (this.Param.IsEnableStateAbnormalAny() || this.IsStun())
				{
					if (!this.m_Owner.CharaAnim.IsPlayingAnim("abnormal"))
					{
						this.m_Owner.CharaAnim.PlayAnim("abnormal", WrapMode.Loop, this.m_BlendSec, this.m_DelaySec);
					}
					if (this.Param.IsEnableStateAbnormal(eStateAbnormal.Sleep))
					{
						this.m_Owner.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_SLEEP_BATTLE[(int)this.m_Owner.CharaAnim.Dir]);
					}
					else
					{
						this.m_Owner.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_ABNORMAL_BATTLE[(int)this.m_Owner.CharaAnim.Dir]);
					}
				}
				else
				{
					if (!this.m_Owner.CharaAnim.IsPlayingAnim("idle"))
					{
						this.m_Owner.CharaAnim.PlayAnim("idle", WrapMode.Loop, this.m_BlendSec, this.m_DelaySec);
					}
					this.m_Owner.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_BLINK_BATTLE[0, 0]);
				}
				if (this.IsStun())
				{
					this.AttachStunEffect();
					if (this.m_IdleIsStunScaleAnim)
					{
						this.m_Owner.ResetScale();
						iTween.Stop(this.m_Owner.gameObject);
						iTween.PunchScale(this.m_Owner.gameObject, iTween.Hash(new object[]
						{
							"easeType",
							iTween.EaseType.linear,
							"y",
							-0.8f,
							"time",
							0.5f
						}));
					}
				}
				else
				{
					this.DetachStunEffect();
				}
				this.m_UpdateIdleStep = CharacterBattle.eUpdateIdleStep.ExecAnimation_Wait;
			}
		}

		// Token: 0x060008A1 RID: 2209 RVA: 0x0003888A File Offset: 0x00036C8A
		public void RequestDamageMode()
		{
			this.m_Mode = CharacterBattle.eMode.Damage;
			this.m_UpdateDamageStep = CharacterBattle.eUpdateDamageStep.ExecAnimation;
			this.m_Owner.CharaAnim.SetBlink(false);
		}

		// Token: 0x060008A2 RID: 2210 RVA: 0x000388AC File Offset: 0x00036CAC
		private void UpdateDamageMode()
		{
			CharacterBattle.eUpdateDamageStep updateDamageStep = this.m_UpdateDamageStep;
			if (updateDamageStep != CharacterBattle.eUpdateDamageStep.ExecAnimation)
			{
				if (updateDamageStep != CharacterBattle.eUpdateDamageStep.ExecAnimation_Wait)
				{
					if (updateDamageStep == CharacterBattle.eUpdateDamageStep.End)
					{
						this.RequestIdleMode(0.2f, 0f, false);
					}
				}
				else if (!this.m_Owner.CharaAnim.IsPlayingAnim("damage"))
				{
					this.m_UpdateDamageStep = CharacterBattle.eUpdateDamageStep.End;
				}
			}
			else
			{
				this.m_Owner.CharaAnim.PlayAnim("damage", WrapMode.ClampForever, 0f, 0f);
				float hpRatio = this.Param.GetHpRatio();
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					for (int i = BattleDefine.DAMAGE_VOICE_THRESHOLD.Length - 1; i >= 0; i--)
					{
						if (hpRatio <= BattleDefine.DAMAGE_VOICE_THRESHOLD[i] || i == 0)
						{
							this.m_Owner.PlayVoice(BattleDefine.VOICE_DAMAGES[i], 1f);
							break;
						}
					}
				}
				else
				{
					for (int j = BattleDefine.DAMAGE_VOICE_THRESHOLD.Length - 1; j >= 0; j--)
					{
						if (hpRatio <= BattleDefine.DAMAGE_VOICE_THRESHOLD[j] || j == 0)
						{
							int length = BattleDefine.EN_VOICE_DAMAGES.GetLength(1);
							string[] array = new string[length];
							for (int k = 0; k < length; k++)
							{
								array[k] = BattleDefine.EN_VOICE_DAMAGES[j, k];
							}
							this.m_Owner.PlayVoiceRandom(array, 1f);
							break;
						}
					}
				}
				this.m_UpdateDamageStep = CharacterBattle.eUpdateDamageStep.ExecAnimation_Wait;
			}
		}

		// Token: 0x060008A3 RID: 2211 RVA: 0x00038A33 File Offset: 0x00036E33
		public void RequestDeadMode()
		{
			this.m_Mode = CharacterBattle.eMode.Dead;
			this.m_UpdateDeadStep = CharacterBattle.eUpdateDeadStep.ExecAnimation;
			this.ResetOnDead();
			this.m_DeadScaleRatio = 0f;
			this.m_Owner.CharaAnim.SetBlink(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008A4 RID: 2212 RVA: 0x00038A6C File Offset: 0x00036E6C
		private void UpdateDeadMode()
		{
			CharacterBattle.eUpdateDeadStep updateDeadStep = this.m_UpdateDeadStep;
			if (updateDeadStep != CharacterBattle.eUpdateDeadStep.ExecAnimation)
			{
				if (updateDeadStep != CharacterBattle.eUpdateDeadStep.ExecAnimation_Wait)
				{
					if (updateDeadStep != CharacterBattle.eUpdateDeadStep.End)
					{
					}
				}
				else
				{
					if (!this.m_Owner.CharaResource.IsBosssResource)
					{
						if (this.m_Owner.CharaAnim.GetPlayingFrame("dead", 0) >= 12)
						{
							this.m_DeadScaleRatio += Time.deltaTime * (1f / (8f / (float)Application.targetFrameRate));
						}
						this.m_DeadScaleRatio = Mathf.Clamp(this.m_DeadScaleRatio, 0f, 1f);
						float deadScaleRatio = this.m_DeadScaleRatio;
						float x = Mathf.Lerp(1f, 0f, deadScaleRatio);
						float y = Mathf.Lerp(1f, 1.2f, deadScaleRatio);
						this.m_Owner.CacheTransform.localScale = new Vector3(x, y, this.m_Owner.CacheTransform.localScale.z);
					}
					if (!this.m_Owner.CharaAnim.IsPlayingAnim("dead") && (this.m_DeadEffectHandler == null || (this.m_DeadEffectHandler != null && !this.m_DeadEffectHandler.IsPlaying())))
					{
						this.DetachDeadEffectHandler();
						this.m_Owner.SetEnableRender(false);
						this.m_Owner.ResetScale();
						this.OnEndDeadAnimation.Call(this.m_Owner);
						this.m_UpdateDeadStep = CharacterBattle.eUpdateDeadStep.End;
					}
				}
			}
			else
			{
				this.m_Owner.CharaAnim.PlayAnim("dead", WrapMode.ClampForever, 0f, 0f);
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					this.m_Owner.PlayVoice(eSoundVoiceListDB.voice_416, 1f);
				}
				else
				{
					this.m_Owner.PlayVoice("voice_418", 1f);
				}
				int sortingOrder = this.m_Owner.CharaAnim.GetSortingOrder() + 1;
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.Play("ef_btl_common_dead", this.m_Owner.CacheTransform.localPosition, Quaternion.Euler(0f, 180f, 0f), Vector3.one, this.m_Owner.CacheTransform.parent, true, sortingOrder, 1f, false);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ACT_DOWN, 1f, 0, -1, -1);
				}
				else if (this.m_Owner.CharaResource.IsBosssResource)
				{
					Transform locator = this.m_Owner.CharaAnim.GetLocator(CharacterDefine.eLocatorIndex.Body);
					SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.Play("ef_btl_boss_dead", locator.position, Quaternion.Euler(0f, 180f, 0f), Vector3.one, this.m_Owner.CacheTransform.parent, false, sortingOrder, 1f, true);
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ENEMY_DEAD, 1f, 0, -1, -1);
				}
				this.m_UpdateDeadStep = CharacterBattle.eUpdateDeadStep.ExecAnimation_Wait;
			}
		}

		// Token: 0x060008A5 RID: 2213 RVA: 0x00038D96 File Offset: 0x00037196
		public bool IsPlayingDeadAnim()
		{
			return this.m_Mode == CharacterBattle.eMode.Dead && this.m_UpdateDeadStep != CharacterBattle.eUpdateDeadStep.End;
		}

		// Token: 0x060008A6 RID: 2214 RVA: 0x00038DB3 File Offset: 0x000371B3
		private void DetachDeadEffectHandler()
		{
			if (this.m_DeadEffectHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_DeadEffectHandler);
				this.m_DeadEffectHandler = null;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060008A7 RID: 2215 RVA: 0x00038DE2 File Offset: 0x000371E2
		public int BattleWinID
		{
			get
			{
				return this.m_BattleWinID;
			}
		}

		// Token: 0x060008A8 RID: 2216 RVA: 0x00038DEA File Offset: 0x000371EA
		public void RequestWinMode(bool isStartFromLoop = false)
		{
			this.m_Mode = CharacterBattle.eMode.Win;
			this.m_BattleWinIsStartFromLoop = isStartFromLoop;
			this.m_UpdateWinStep = CharacterBattle.eUpdateWinStep.ExecAnimation;
			this.m_Owner.CharaAnim.SetBlink(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008A9 RID: 2217 RVA: 0x00038E18 File Offset: 0x00037218
		private void UpdateWinMode()
		{
			CharacterBattle.eUpdateWinStep updateWinStep = this.m_UpdateWinStep;
			if (updateWinStep != CharacterBattle.eUpdateWinStep.ExecAnimation)
			{
				if (updateWinStep != CharacterBattle.eUpdateWinStep.ExecAnimation_Wait)
				{
					if (updateWinStep != CharacterBattle.eUpdateWinStep.End)
					{
					}
				}
				else if (!this.m_Owner.CharaAnim.IsPlayingAnim(this.m_BattleWinActKey))
				{
					this.m_Owner.CharaAnim.PlayAnim("win_lp_" + this.m_BattleWinID, WrapMode.Loop, 0f, 0f);
					this.m_UpdateWinStep = CharacterBattle.eUpdateWinStep.End;
				}
			}
			else
			{
				this.m_Owner.SetEnableRenderWeapon(false);
				this.m_BattleWinActKey = "win_st_" + this.m_BattleWinID;
				if (!this.m_BattleWinIsStartFromLoop)
				{
					this.m_Owner.CharaAnim.PlayAnim(this.m_BattleWinActKey, WrapMode.ClampForever, 0f, 0f);
					this.m_UpdateWinStep = CharacterBattle.eUpdateWinStep.ExecAnimation_Wait;
				}
				else
				{
					this.m_Owner.CharaAnim.PlayAnim("win_lp_" + this.m_BattleWinID, WrapMode.Loop, 0f, 0f);
					this.m_UpdateWinStep = CharacterBattle.eUpdateWinStep.End;
				}
			}
		}

		// Token: 0x060008AA RID: 2218 RVA: 0x00038F3C File Offset: 0x0003733C
		public bool IsPlayingWinAnim()
		{
			return this.m_Mode == CharacterBattle.eMode.Win && this.m_UpdateWinStep != CharacterBattle.eUpdateWinStep.End;
		}

		// Token: 0x060008AB RID: 2219 RVA: 0x00038F5C File Offset: 0x0003735C
		public BattleCommandData RequestCommandMode(SkillActionPlayer skillActionPlayer, int commandIndex, bool isUseUniqueSkill, int togetherAttackChainCount)
		{
			this.m_Mode = CharacterBattle.eMode.Command;
			if (!isUseUniqueSkill)
			{
				this.m_ExecCommand = this.m_Commands[commandIndex];
			}
			else
			{
				this.m_ExecCommand = this.m_UniqueSkillCommand;
			}
			this.m_TogetherAttackChainCount = togetherAttackChainCount;
			this.m_SkillActionPlayer = skillActionPlayer;
			this.m_UpdateCommandStep = CharacterBattle.eUpdateCommandStep.ExecAnimation;
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_IsEndCommandPlayingAnim = false;
			return this.m_ExecCommand;
		}

		// Token: 0x060008AC RID: 2220 RVA: 0x00038FD0 File Offset: 0x000373D0
		private void UpdateCommandMode()
		{
			CharacterBattle.eUpdateCommandStep updateCommandStep = this.m_UpdateCommandStep;
			if (updateCommandStep != CharacterBattle.eUpdateCommandStep.ExecAnimation)
			{
				if (updateCommandStep != CharacterBattle.eUpdateCommandStep.ExecAnimation_Wait)
				{
					if (updateCommandStep == CharacterBattle.eUpdateCommandStep.End)
					{
						this.RequestIdleMode(0.25f, 0f, false);
						this.m_UpdateCommandStep = CharacterBattle.eUpdateCommandStep.None;
					}
				}
				else
				{
					if (!this.m_IsEndCommandPlayingAnim && !this.m_Owner.CharaAnim.IsPlayingAnim(this.m_ActionKey))
					{
						this.m_IsEndCommandPlayingAnim = true;
						this.m_Owner.CharaAnim.PlayAnim("idle", WrapMode.Loop, 0.5f, 0f);
						this.m_SkillActionPlayer.ClearUpdateRefAnimTimeCallback();
					}
					if (this.m_IsEndCommandPlayingAnim && !this.m_SkillActionPlayer.IsPlaying())
					{
						this.m_UpdateCommandStep = CharacterBattle.eUpdateCommandStep.End;
					}
				}
			}
			else
			{
				this.m_Owner.SetEnableRenderWeapon(true);
				this.m_ActionKey = this.m_ExecCommand.ActionKey;
				this.m_Owner.CharaAnim.PlayAnim(this.m_ActionKey, WrapMode.ClampForever, 0f, 0f);
				SkillActionPlan sap = null;
				try
				{
					sap = this.SAPs[this.m_ExecCommand.SAP_ID];
				}
				catch (Exception ex)
				{
					UnityEngine.Debug.LogErrorFormat("CHARA_ID:{0} >>> SAP_ID:{1} is not found. exception:{2}", new object[]
					{
						this.m_Owner.CharaParam.CharaID,
						this.m_ExecCommand.SAP_ID,
						ex
					});
				}
				SkillActionGraphics sag = null;
				try
				{
					sag = this.SAGs[this.m_ExecCommand.SAG_ID];
				}
				catch (Exception ex2)
				{
					UnityEngine.Debug.LogErrorFormat("CHARA_ID:{0} >>> SAG_ID:{1} is not found. exception:{2}", new object[]
					{
						this.m_Owner.CharaParam.CharaID,
						this.m_ExecCommand.SAG_ID,
						ex2
					});
				}
				List<CharacterHandler> tgtCharaList;
				List<CharacterHandler> myCharaList;
				BattleCommandParser.GetTakeCharaList(this.m_ExecCommand, out tgtCharaList, out myCharaList);
				this.m_SkillActionPlayer.Play(sap, sag, this.m_Owner, null, tgtCharaList, myCharaList, new SkillActionPlayer.UpdateRefAnimTimeDelegate(this.SkillActionPlayerUpdateRefAnimTime));
				this.m_UpdateCommandStep = CharacterBattle.eUpdateCommandStep.ExecAnimation_Wait;
			}
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x000391F4 File Offset: 0x000375F4
		private SkillActionPlayer.RefAnimTime SkillActionPlayerUpdateRefAnimTime()
		{
			SkillActionPlayer.RefAnimTime result;
			result.m_PlayingSec = this.m_Owner.CharaAnim.GetPlayingSec(this.m_ActionKey, 0);
			result.m_MaxSec = this.m_Owner.CharaAnim.GetMaxSec(this.m_ActionKey, 0);
			return result;
		}

		// Token: 0x060008AE RID: 2222 RVA: 0x0003923E File Offset: 0x0003763E
		public void RequestInMode(float delaySec = 0f)
		{
			this.m_Mode = CharacterBattle.eMode.In;
			this.m_UpdateInStep = CharacterBattle.eUpdateInStep.ExecAnimation;
			this.m_DelaySec = delaySec;
			this.m_Owner.CharaAnim.SetBlink(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008AF RID: 2223 RVA: 0x0003926C File Offset: 0x0003766C
		private void UpdateInMode()
		{
			CharacterBattle.eUpdateInStep updateInStep = this.m_UpdateInStep;
			if (updateInStep != CharacterBattle.eUpdateInStep.ExecAnimation)
			{
				if (updateInStep != CharacterBattle.eUpdateInStep.ExecAnimation_Wait)
				{
					if (updateInStep == CharacterBattle.eUpdateInStep.End)
					{
						this.RequestIdleMode(0.25f, 0f, false);
					}
				}
				else if (!this.m_Owner.CharaAnim.IsPlayingAnim("battle_in"))
				{
					this.m_UpdateInStep = CharacterBattle.eUpdateInStep.End;
				}
			}
			else
			{
				this.m_DelaySec -= Time.deltaTime;
				if (this.m_DelaySec <= 0f)
				{
					this.m_Owner.SetEnableRenderWeapon(false);
					this.m_Owner.CharaAnim.PlayAnim("battle_in", WrapMode.ClampForever, 0.1f, 0f);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ACT_CHANGE_IN, 1f, 0, -1, -1);
					this.m_UpdateInStep = CharacterBattle.eUpdateInStep.ExecAnimation_Wait;
				}
			}
		}

		// Token: 0x060008B0 RID: 2224 RVA: 0x00039351 File Offset: 0x00037751
		public void RequestOutMode(Vector3 toPos, float delaySec = 0f, bool isToFront = false)
		{
			this.m_Mode = CharacterBattle.eMode.Out;
			this.m_UpdateOutStep = CharacterBattle.eUpdateOutStep.ExecAnimation;
			this.m_DelaySec = delaySec;
			this.m_OutToPos = toPos;
			this.m_IsToFront = isToFront;
			this.m_Owner.CharaAnim.SetBlink(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008B1 RID: 2225 RVA: 0x00039390 File Offset: 0x00037790
		private void UpdateOutMode()
		{
			CharacterBattle.eUpdateOutStep updateOutStep = this.m_UpdateOutStep;
			if (updateOutStep != CharacterBattle.eUpdateOutStep.ExecAnimation)
			{
				if (updateOutStep != CharacterBattle.eUpdateOutStep.ExecAnimation_Wait)
				{
					if (updateOutStep != CharacterBattle.eUpdateOutStep.End)
					{
					}
				}
				else if (!this.IsMoveTo)
				{
					if (this.m_SmokeEffectHandler != null)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_SmokeEffectHandler);
						this.m_SmokeEffectHandler = null;
					}
					this.m_Owner.CharaAnim.ChangeDir((this.m_Owner.CharaAnim.Dir != CharacterDefine.eDir.L) ? CharacterDefine.eDir.L : CharacterDefine.eDir.R, false);
					this.m_UpdateOutStep = CharacterBattle.eUpdateOutStep.End;
				}
			}
			else
			{
				this.m_DelaySec -= Time.deltaTime;
				if (this.m_DelaySec <= 0f)
				{
					this.m_Owner.SetEnableRenderWeapon(false);
					this.m_Owner.CharaAnim.ChangeDir((this.m_Owner.CharaAnim.Dir != CharacterDefine.eDir.L) ? CharacterDefine.eDir.L : CharacterDefine.eDir.R, false);
					if (!this.m_IsToFront)
					{
						this.m_Owner.CharaAnim.PlayAnim("battle_out", WrapMode.Loop, 0f, 0f);
					}
					else
					{
						this.m_Owner.CharaAnim.PlayAnim("battle_run", WrapMode.Loop, 0f, 0f);
					}
					int sortingOrder = this.m_Owner.CharaAnim.GetSortingOrder() + 1;
					this.m_SmokeEffectHandler = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged("ef_btl_common_smoke", Vector3.zero, Quaternion.Euler(0f, 180f, 0f), this.m_Owner.CacheTransform, true, WrapMode.Loop, sortingOrder, 1f, false);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ACT_CHANGE_OUT, 1f, 0, -1, -1);
					this.MoveToPositionFromCurrent(this.m_OutToPos, 4.2f);
					this.m_UpdateOutStep = CharacterBattle.eUpdateOutStep.ExecAnimation_Wait;
				}
			}
		}

		// Token: 0x060008B2 RID: 2226 RVA: 0x00039577 File Offset: 0x00037977
		public bool IsArrivalOutToPos()
		{
			return this.m_Mode == CharacterBattle.eMode.Out && this.m_UpdateOutStep == CharacterBattle.eUpdateOutStep.End;
		}

		// Token: 0x060008B3 RID: 2227 RVA: 0x00039594 File Offset: 0x00037994
		public void RequestAutoFacial(int facialID, float time = 1f)
		{
			UnityEngine.Debug.LogFormat("RequestAutoFacial() >>> facialID:{0}, time:{1}", new object[]
			{
				facialID,
				time
			});
			this.m_AutoFacialID = facialID;
			this.m_AutoFacialTimer = time;
			this.m_AutoFacialSaveFacialID = this.m_Owner.CharaAnim.GetFacialID();
			this.m_AutoFacialSaveIsEnableBlink = this.m_Owner.CharaAnim.IsEnableBlink();
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_Owner.CharaAnim.SetFacial(this.m_AutoFacialID);
		}

		// Token: 0x060008B4 RID: 2228 RVA: 0x00039624 File Offset: 0x00037A24
		private void UpdateAutoFacial()
		{
			if (this.m_AutoFacialID != -1)
			{
				this.m_AutoFacialTimer -= Time.deltaTime;
				if (this.m_AutoFacialTimer <= 0f)
				{
					this.ResetAutoFacial();
				}
			}
		}

		// Token: 0x060008B5 RID: 2229 RVA: 0x0003965C File Offset: 0x00037A5C
		private void ResetAutoFacial()
		{
			if (this.m_AutoFacialSaveIsEnableBlink)
			{
				this.m_Owner.CharaAnim.SetBlink(true);
			}
			else if (this.m_AutoFacialSaveFacialID != -1)
			{
				this.m_Owner.CharaAnim.SetFacial(this.m_AutoFacialSaveFacialID);
			}
			this.m_AutoFacialTimer = 0f;
			this.m_AutoFacialID = -1;
			this.m_AutoFacialSaveFacialID = -1;
			this.m_AutoFacialSaveIsEnableBlink = false;
		}

		// Token: 0x060008B6 RID: 2230 RVA: 0x000396CC File Offset: 0x00037ACC
		public bool IsAutoFacialing()
		{
			return this.m_AutoFacialID != -1;
		}

		// Token: 0x060008B7 RID: 2231 RVA: 0x000396E0 File Offset: 0x00037AE0
		public void SetupForKiraraJumpScene()
		{
			this.m_Mode = CharacterBattle.eMode.None;
			if (this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_Transform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 0f, 0f);
					}
				}
			}
			this.m_Owner.CharaAnim.Pause();
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_Owner.CharaAnim.SetEnableFloatingObj(false);
			this.m_Owner.SetEnableRenderWeapon(false);
			this.m_Owner.SetEnableRenderShadow(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008B8 RID: 2232 RVA: 0x000397E8 File Offset: 0x00037BE8
		public void RevertFromKiraraJumpScene()
		{
			this.m_Mode = CharacterBattle.eMode.None;
			if (this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_Transform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 180f, 0f);
					}
				}
			}
			this.SetFixRootBoneTransform(false);
			this.m_Owner.CharaAnim.Pause();
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_Owner.CharaBattle.ResetMeshColor();
			this.m_Owner.CharaAnim.SetEnableFloatingObj(true);
			this.m_Owner.SetEnableRenderWeapon(true);
			this.m_Owner.SetEnableRenderShadow(true);
		}

		// Token: 0x060008B9 RID: 2233 RVA: 0x000398FE File Offset: 0x00037CFE
		public void RequestKiraraJump(int index)
		{
			this.m_Owner.CharaAnim.PlayAnim("kirarajump_" + index, WrapMode.ClampForever, 0f, 0f);
		}

		// Token: 0x060008BA RID: 2234 RVA: 0x0003992B File Offset: 0x00037D2B
		public BattleCommandData SetTogetherAttackCommand(int togetherAttackChainCount)
		{
			this.m_ExecCommand = this.m_UniqueSkillCommand;
			this.m_TogetherAttackChainCount = togetherAttackChainCount;
			return this.m_ExecCommand;
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x00039948 File Offset: 0x00037D48
		public void SetupForUniqueSkillScene()
		{
			this.m_IsInUniqueSkillScene = true;
			this.m_Mode = CharacterBattle.eMode.None;
			if (this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_Transform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 0f, 0f);
					}
					this.m_Owner.CharaAnim.SetupShadowTransform(true);
				}
			}
			this.m_Owner.CharaAnim.Pause();
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_Owner.CharaAnim.SetEnableFloatingObj(false);
			this.m_Owner.SetEnableRenderShadow(false);
			this.m_Owner.SetEnableRender(false);
			this.DetachStunEffect();
		}

		// Token: 0x060008BC RID: 2236 RVA: 0x00039A68 File Offset: 0x00037E68
		public void RevertFromUniqueSkillScene()
		{
			this.m_IsInUniqueSkillScene = false;
			this.m_Mode = CharacterBattle.eMode.None;
			if (this.m_Owner.CharaAnim.ModelSets != null)
			{
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_Transform != null)
					{
						this.m_Owner.CharaAnim.ModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 180f, 0f);
					}
					this.m_Owner.CharaAnim.SetupShadowTransform(false);
				}
			}
			this.SetFixRootBoneTransform(false);
			this.m_Owner.CharaAnim.Pause();
			this.m_Owner.CharaAnim.SetBlink(false);
			this.m_Owner.CharaBattle.ResetMeshColor();
			this.m_Owner.CharaAnim.SetEnableFloatingObj(true);
			this.m_Owner.RevertDefaultEnableRenderWeapon();
			this.m_Owner.SetEnableRenderShadow(true);
		}

		// Token: 0x060008BD RID: 2237 RVA: 0x00039B98 File Offset: 0x00037F98
		private void SetupShadow()
		{
			this.m_ShadowTransform = this.m_Owner.CharaAnim.Shadow.transform;
			if (this.m_Owner.CharaAnim.ModelSets != null)
			{
				if (this.m_Owner.CharaAnim.ModelSets.Length > 1)
				{
					this.m_ShadowRefTransform = this.m_Owner.CharaAnim.ModelSets[1].m_RootTransform.FindChild("Hips");
				}
				else
				{
					this.m_ShadowRefTransform = this.m_Owner.CharaAnim.ModelSets[0].m_RootTransform.FindChild("Hips");
				}
			}
			if (this.m_ShadowRefTransform != null)
			{
				this.m_ShadowScaleBaseHeight = this.m_ShadowRefTransform.position.y - this.m_Owner.CacheTransform.position.y;
			}
			if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Enemy)
			{
				EnemyResourceListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EnemyResourceListDB.GetParam(this.m_Owner.CharaResource.ResourceID);
				this.m_ShadowScaleMax = param.m_ShadowScale;
				this.m_ShadowBaseOffsetX = param.m_ShadowOffsetX;
			}
		}

		// Token: 0x060008BE RID: 2238 RVA: 0x00039CD8 File Offset: 0x000380D8
		private void UpdateShadow()
		{
			if (this.m_ShadowRefTransform != null && this.m_ShadowTransform != null)
			{
				float num = this.m_ShadowBaseOffsetX * this.m_ShadowTransform.parent.lossyScale.x;
				if (this.m_Owner.CharaAnim.IsPlayingAnim("idle") && this.m_Owner.CharaParam.ClassType != eClassType.None)
				{
					num += BattleDefine.SHADOW_OFFSET_BY_CLASS[(int)this.m_Owner.CharaParam.ClassType];
				}
				this.m_ShadowOffsetX = Mathf.Lerp(this.m_ShadowOffsetX, num, 0.05f);
				this.m_ShadowTransform.positionX(this.m_ShadowRefTransform.position.x + this.m_ShadowOffsetX);
				if (this.System != null && this.System.MainCameraTransform != null)
				{
					Vector3 vector = this.System.MainCameraTransform.TransformDirection(Vector3.forward);
					this.m_ShadowTransform.positionZ(this.m_ShadowRefTransform.position.z + vector.z * 0.3f);
				}
				float num2 = this.m_ShadowRefTransform.position.y - this.m_Owner.CacheTransform.position.y;
				float num3 = num2 / this.m_ShadowScaleBaseHeight;
				float min = BattleDefine.SHADOW_SCALE_RANGE[0];
				float shadowScaleMax = this.m_ShadowScaleMax;
				float num4 = (num3 <= 0f) ? 1f : (shadowScaleMax / num3);
				num4 = Mathf.Clamp(num4, min, shadowScaleMax);
				this.m_ShadowTransform.localScale = new Vector3(num4, num4, 1f);
			}
		}

		// Token: 0x060008BF RID: 2239 RVA: 0x00039EA8 File Offset: 0x000382A8
		public void SetupForResult(bool enableRender, Vector3 pos, float scale)
		{
			this.m_Owner.SetEnableRender(enableRender);
			if (this.m_Owner.CharaAnim.Shadow != null)
			{
				this.m_Owner.CharaAnim.Shadow.SetActive(false);
			}
			this.m_Owner.CharaAnim.SetSortingOrder(1);
			this.m_Owner.CacheTransform.SetParent(null);
			this.m_Owner.CacheTransform.position = pos;
			this.m_Owner.CacheTransform.localScale = new Vector3(scale, scale, scale);
			this.DetachStunEffect();
		}

		// Token: 0x060008C0 RID: 2240 RVA: 0x00039F44 File Offset: 0x00038344
		public void SetStencilParamForResult(int idx)
		{
			if (this.m_MaterialList == null)
			{
				this.m_MaterialList = this.m_Owner.CharaAnim.GetMaterials();
				if (this.m_MaterialList != null)
				{
					idx = Mathf.Clamp(idx, 0, 255);
					for (int i = 0; i < this.m_MaterialList.Count; i++)
					{
						MeigeShaderUtility.SetOutlineStencilID(this.m_MaterialList[i], (byte)(255 - idx));
						MeigeShaderUtility.SetOutlineStencilCompare(this.m_MaterialList[i], eCompareFunc.Equal);
						MeigeShaderUtility.SetOutlineStencilOp(this.m_MaterialList[i], eStencilOp.Keep);
						MeigeShaderUtility.SetStencilID(this.m_MaterialList[i], (byte)(255 - idx));
						MeigeShaderUtility.SetStencilCompare(this.m_MaterialList[i], eCompareFunc.Equal);
						MeigeShaderUtility.SetStencilOp(this.m_MaterialList[i], eStencilOp.Keep);
					}
				}
			}
		}

		// Token: 0x04000802 RID: 2050
		private bool m_SetupedParameter;

		// Token: 0x04000803 RID: 2051
		private bool m_SetupedResource;

		// Token: 0x04000804 RID: 2052
		private CharacterHandler m_Owner;

		// Token: 0x04000805 RID: 2053
		private CharacterBattleParam m_Param;

		// Token: 0x04000806 RID: 2054
		private BattleAIData m_BattleAIData;

		// Token: 0x04000807 RID: 2055
		private Dictionary<string, SkillActionPlan> m_SAPs;

		// Token: 0x04000808 RID: 2056
		private Dictionary<string, SkillActionGraphics> m_SAGs;

		// Token: 0x0400080E RID: 2062
		private BattleDefine.eJoinMember[] m_OldSingleTargetIndices = new BattleDefine.eJoinMember[2];

		// Token: 0x0400080F RID: 2063
		private SkillActionPlayer m_SkillActionPlayer;

		// Token: 0x04000810 RID: 2064
		private string m_ActionKey;

		// Token: 0x04000811 RID: 2065
		private bool m_IsInUniqueSkillScene;

		// Token: 0x04000812 RID: 2066
		private float m_DelaySec;

		// Token: 0x04000813 RID: 2067
		private float m_BlendSec;

		// Token: 0x04000814 RID: 2068
		private bool m_IsFixRootBoneTransform;

		// Token: 0x04000815 RID: 2069
		private SimpleTimer m_Timer = new SimpleTimer(0f);

		// Token: 0x04000818 RID: 2072
		private List<BattleCommandData> m_Commands;

		// Token: 0x04000819 RID: 2073
		private BattleCommandData m_UniqueSkillCommand;

		// Token: 0x0400081A RID: 2074
		private BattleCommandData m_ExecCommand;

		// Token: 0x0400081B RID: 2075
		private int m_TogetherAttackChainCount;

		// Token: 0x0400081C RID: 2076
		private bool m_IsStun;

		// Token: 0x0400081D RID: 2077
		private float m_StunValue;

		// Token: 0x0400081E RID: 2078
		private bool m_IsStunerJudged;

		// Token: 0x0400081F RID: 2079
		private bool m_IsStunerApply;

		// Token: 0x04000820 RID: 2080
		private EffectHandler m_StunOccurEffectHandler;

		// Token: 0x04000821 RID: 2081
		private EffectHandler m_StunEffectHandler;

		// Token: 0x04000822 RID: 2082
		private int m_AIPatternIndex;

		// Token: 0x04000823 RID: 2083
		private Dictionary<int, bool> m_AIPatternChangeHitIndices = new Dictionary<int, bool>();

		// Token: 0x04000824 RID: 2084
		private List<BattleAIFlag> m_AIFlags = new List<BattleAIFlag>();

		// Token: 0x04000825 RID: 2085
		private List<BattleAIFlag> m_AIWriteFlags;

		// Token: 0x04000826 RID: 2086
		private Dictionary<string, int> m_AIExecedNums = new Dictionary<string, int>();

		// Token: 0x04000828 RID: 2088
		private CharacterBattle.eMode m_Mode;

		// Token: 0x04000829 RID: 2089
		private CharacterBattle.eUpdateIdleStep m_UpdateIdleStep = CharacterBattle.eUpdateIdleStep.None;

		// Token: 0x0400082A RID: 2090
		private bool m_IdleIsStunScaleAnim;

		// Token: 0x0400082B RID: 2091
		public const float DEFAULT_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x0400082C RID: 2092
		private CharacterBattle.eUpdateDamageStep m_UpdateDamageStep = CharacterBattle.eUpdateDamageStep.None;

		// Token: 0x0400082D RID: 2093
		private const float DAMAGE_TO_IDLE_BLEND_SEC = 0.2f;

		// Token: 0x0400082E RID: 2094
		private CharacterBattle.eUpdateDeadStep m_UpdateDeadStep = CharacterBattle.eUpdateDeadStep.None;

		// Token: 0x0400082F RID: 2095
		private EffectHandler m_DeadEffectHandler;

		// Token: 0x04000830 RID: 2096
		private const int DEAD_SCALE_TIMING_FRAME = 12;

		// Token: 0x04000831 RID: 2097
		private const int DEAD_SCALE_FRAME = 8;

		// Token: 0x04000832 RID: 2098
		private float m_DeadScaleRatio;

		// Token: 0x04000833 RID: 2099
		private CharacterBattle.eUpdateWinStep m_UpdateWinStep = CharacterBattle.eUpdateWinStep.None;

		// Token: 0x04000834 RID: 2100
		private bool m_BattleWinIsStartFromLoop;

		// Token: 0x04000835 RID: 2101
		private string m_BattleWinActKey;

		// Token: 0x04000836 RID: 2102
		private int m_BattleWinID;

		// Token: 0x04000837 RID: 2103
		private CharacterBattle.eUpdateCommandStep m_UpdateCommandStep = CharacterBattle.eUpdateCommandStep.None;

		// Token: 0x04000838 RID: 2104
		private bool m_IsEndCommandPlayingAnim;

		// Token: 0x04000839 RID: 2105
		private CharacterBattle.eUpdateInStep m_UpdateInStep = CharacterBattle.eUpdateInStep.None;

		// Token: 0x0400083A RID: 2106
		private const float IN_TO_IDLE_BLEND_SEC = 0.25f;

		// Token: 0x0400083B RID: 2107
		private CharacterBattle.eUpdateOutStep m_UpdateOutStep = CharacterBattle.eUpdateOutStep.None;

		// Token: 0x0400083C RID: 2108
		private Vector3 m_OutToPos = Vector3.zero;

		// Token: 0x0400083D RID: 2109
		private EffectHandler m_SmokeEffectHandler;

		// Token: 0x0400083E RID: 2110
		private bool m_IsToFront;

		// Token: 0x0400083F RID: 2111
		private const float OUT_SPEED = 4.2f;

		// Token: 0x04000840 RID: 2112
		private int m_AutoFacialID = -1;

		// Token: 0x04000841 RID: 2113
		private float m_AutoFacialTimer;

		// Token: 0x04000842 RID: 2114
		private int m_AutoFacialSaveFacialID = -1;

		// Token: 0x04000843 RID: 2115
		private bool m_AutoFacialSaveIsEnableBlink;

		// Token: 0x04000844 RID: 2116
		private Transform m_ShadowTransform;

		// Token: 0x04000845 RID: 2117
		private Transform m_ShadowRefTransform;

		// Token: 0x04000846 RID: 2118
		private float m_ShadowOffsetX;

		// Token: 0x04000847 RID: 2119
		private float m_ShadowBaseOffsetX;

		// Token: 0x04000848 RID: 2120
		private float m_ShadowScaleBaseHeight = 0.25f;

		// Token: 0x04000849 RID: 2121
		private float m_ShadowScaleMax = BattleDefine.SHADOW_SCALE_RANGE[1];

		// Token: 0x0400084A RID: 2122
		private List<Material> m_MaterialList;

		// Token: 0x0200013C RID: 316
		public enum eMode
		{
			// Token: 0x04000869 RID: 2153
			None = -1,
			// Token: 0x0400086A RID: 2154
			Idle,
			// Token: 0x0400086B RID: 2155
			Damage,
			// Token: 0x0400086C RID: 2156
			Dead,
			// Token: 0x0400086D RID: 2157
			Command,
			// Token: 0x0400086E RID: 2158
			Win,
			// Token: 0x0400086F RID: 2159
			In,
			// Token: 0x04000870 RID: 2160
			Out
		}

		// Token: 0x0200013D RID: 317
		private enum eUpdateIdleStep
		{
			// Token: 0x04000872 RID: 2162
			None = -1,
			// Token: 0x04000873 RID: 2163
			ExecAnimation,
			// Token: 0x04000874 RID: 2164
			ExecAnimation_Wait,
			// Token: 0x04000875 RID: 2165
			End
		}

		// Token: 0x0200013E RID: 318
		private enum eUpdateDamageStep
		{
			// Token: 0x04000877 RID: 2167
			None = -1,
			// Token: 0x04000878 RID: 2168
			ExecAnimation,
			// Token: 0x04000879 RID: 2169
			ExecAnimation_Wait,
			// Token: 0x0400087A RID: 2170
			End
		}

		// Token: 0x0200013F RID: 319
		private enum eUpdateDeadStep
		{
			// Token: 0x0400087C RID: 2172
			None = -1,
			// Token: 0x0400087D RID: 2173
			ExecAnimation,
			// Token: 0x0400087E RID: 2174
			ExecAnimation_Wait,
			// Token: 0x0400087F RID: 2175
			End
		}

		// Token: 0x02000140 RID: 320
		private enum eUpdateWinStep
		{
			// Token: 0x04000881 RID: 2177
			None = -1,
			// Token: 0x04000882 RID: 2178
			ExecAnimation,
			// Token: 0x04000883 RID: 2179
			ExecAnimation_Wait,
			// Token: 0x04000884 RID: 2180
			End
		}

		// Token: 0x02000141 RID: 321
		private enum eUpdateCommandStep
		{
			// Token: 0x04000886 RID: 2182
			None = -1,
			// Token: 0x04000887 RID: 2183
			ExecAnimation,
			// Token: 0x04000888 RID: 2184
			ExecAnimation_Wait,
			// Token: 0x04000889 RID: 2185
			End
		}

		// Token: 0x02000142 RID: 322
		private enum eUpdateInStep
		{
			// Token: 0x0400088B RID: 2187
			None = -1,
			// Token: 0x0400088C RID: 2188
			ExecAnimation,
			// Token: 0x0400088D RID: 2189
			ExecAnimation_Wait,
			// Token: 0x0400088E RID: 2190
			End
		}

		// Token: 0x02000143 RID: 323
		private enum eUpdateOutStep
		{
			// Token: 0x04000890 RID: 2192
			None = -1,
			// Token: 0x04000891 RID: 2193
			ExecAnimation,
			// Token: 0x04000892 RID: 2194
			ExecAnimation_Wait,
			// Token: 0x04000893 RID: 2195
			End
		}
	}
}
