﻿using System;

namespace Star
{
	// Token: 0x02000710 RID: 1808
	public enum eTownEffect
	{
		// Token: 0x04002ABB RID: 10939
		BuildMoney,
		// Token: 0x04002ABC RID: 10940
		BuildItem,
		// Token: 0x04002ABD RID: 10941
		TouchKRR,
		// Token: 0x04002ABE RID: 10942
		BuildLevelUp,
		// Token: 0x04002ABF RID: 10943
		AreaCreate,
		// Token: 0x04002AC0 RID: 10944
		ContentChange
	}
}
