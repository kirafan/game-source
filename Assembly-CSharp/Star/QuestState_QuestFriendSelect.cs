﻿using System;
using Star.UI;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000463 RID: 1123
	public class QuestState_QuestFriendSelect : QuestState
	{
		// Token: 0x060015CB RID: 5579 RVA: 0x00071A3C File Offset: 0x0006FE3C
		public QuestState_QuestFriendSelect(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015CC RID: 5580 RVA: 0x00071A53 File Offset: 0x0006FE53
		public override int GetStateID()
		{
			return 7;
		}

		// Token: 0x060015CD RID: 5581 RVA: 0x00071A56 File Offset: 0x0006FE56
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestFriendSelect.eStep.First;
		}

		// Token: 0x060015CE RID: 5582 RVA: 0x00071A5F File Offset: 0x0006FE5F
		public override void OnStateExit()
		{
		}

		// Token: 0x060015CF RID: 5583 RVA: 0x00071A61 File Offset: 0x0006FE61
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015D0 RID: 5584 RVA: 0x00071A6C File Offset: 0x0006FE6C
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestFriendSelect.eStep.First:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.GetFriendListNum(FriendDefine.eType.Guest) == 0)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_GetAll(FriendDefine.eGetAllType.Both, new Action<bool>(this.OnResponse_GetAll));
					this.m_Step = QuestState_QuestFriendSelect.eStep.Request_Wait;
				}
				else
				{
					this.m_Step = QuestState_QuestFriendSelect.eStep.LoadStart;
				}
				break;
			case QuestState_QuestFriendSelect.eStep.LoadStart:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = QuestState_QuestFriendSelect.eStep.LoadWait;
				break;
			case QuestState_QuestFriendSelect.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestFriendSelect.eStep.PlayIn;
				}
				break;
			case QuestState_QuestFriendSelect.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_QUEST, null, null);
					this.m_Step = QuestState_QuestFriendSelect.eStep.Main;
				}
				break;
			case QuestState_QuestFriendSelect.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						QuestFriendSelectUI.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton != QuestFriendSelectUI.eButton.Friend)
						{
							this.m_Owner.SavePartyData();
							this.m_Owner.IsQuestStartConfirm = false;
							this.m_NextState = 5;
						}
						else
						{
							this.m_Owner.IsQuestStartConfirm = true;
							this.m_NextState = 5;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestFriendSelect.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestFriendSelect.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.m_Step = QuestState_QuestFriendSelect.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015D1 RID: 5585 RVA: 0x00071C50 File Offset: 0x00070050
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<QuestFriendSelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickButton += this.OnClickButtonCallback;
			this.m_UI.OnClickReloadButton += this.OnClickReloadButtonCallback;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015D2 RID: 5586 RVA: 0x00071CF4 File Offset: 0x000700F4
		private void OnClickButtonCallback()
		{
			this.GoToMenuEnd();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestFriendData = this.m_UI.SelectFriendData;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestUserSupportData = this.m_UI.SelectSupport;
		}

		// Token: 0x060015D3 RID: 5587 RVA: 0x00071D30 File Offset: 0x00070130
		private void OnClickReloadButtonCallback()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_GetAll(FriendDefine.eGetAllType.Both, new Action<bool>(this.OnReload));
		}

		// Token: 0x060015D4 RID: 5588 RVA: 0x00071D4F File Offset: 0x0007014F
		private void OnReload(bool flg)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.QuestFriendListReloadTitle, eText_MessageDB.QuestFriendListReloadMessage, null);
			this.m_UI.Refresh();
		}

		// Token: 0x060015D5 RID: 5589 RVA: 0x00071D77 File Offset: 0x00070177
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			}
			this.GoToMenuEnd();
		}

		// Token: 0x060015D6 RID: 5590 RVA: 0x00071DA9 File Offset: 0x000701A9
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x060015D7 RID: 5591 RVA: 0x00071DB6 File Offset: 0x000701B6
		private void OnResponse_GetAll(bool isError)
		{
			this.m_Step = QuestState_QuestFriendSelect.eStep.LoadStart;
		}

		// Token: 0x04001C85 RID: 7301
		private QuestState_QuestFriendSelect.eStep m_Step = QuestState_QuestFriendSelect.eStep.None;

		// Token: 0x04001C86 RID: 7302
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.QuestFriendSelectUI;

		// Token: 0x04001C87 RID: 7303
		public QuestFriendSelectUI m_UI;

		// Token: 0x02000464 RID: 1124
		private enum eStep
		{
			// Token: 0x04001C89 RID: 7305
			None = -1,
			// Token: 0x04001C8A RID: 7306
			First,
			// Token: 0x04001C8B RID: 7307
			Request_Wait,
			// Token: 0x04001C8C RID: 7308
			LoadStart,
			// Token: 0x04001C8D RID: 7309
			LoadWait,
			// Token: 0x04001C8E RID: 7310
			PlayIn,
			// Token: 0x04001C8F RID: 7311
			Main,
			// Token: 0x04001C90 RID: 7312
			UnloadChildSceneWait
		}
	}
}
