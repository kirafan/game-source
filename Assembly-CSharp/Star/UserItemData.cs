﻿using System;

namespace Star
{
	// Token: 0x02000B4B RID: 2891
	public class UserItemData
	{
		// Token: 0x06003D32 RID: 15666 RVA: 0x001366A8 File Offset: 0x00134AA8
		public UserItemData()
		{
			this.ItemID = -1;
			this.ItemNum = 0;
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06003D33 RID: 15667 RVA: 0x001366BE File Offset: 0x00134ABE
		// (set) Token: 0x06003D34 RID: 15668 RVA: 0x001366C6 File Offset: 0x00134AC6
		public int ItemID { get; set; }

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06003D35 RID: 15669 RVA: 0x001366CF File Offset: 0x00134ACF
		// (set) Token: 0x06003D36 RID: 15670 RVA: 0x001366D7 File Offset: 0x00134AD7
		public int ItemNum { get; set; }
	}
}
