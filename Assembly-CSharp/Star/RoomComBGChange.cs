﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F4 RID: 1524
	public class RoomComBGChange : IRoomEventCommand
	{
		// Token: 0x06001E23 RID: 7715 RVA: 0x000A1E33 File Offset: 0x000A0233
		public RoomComBGChange(int fchangeobj, bool fbuyobj)
		{
			this.m_Type = eRoomRequest.ChangeBG;
			this.m_Enable = true;
			this.m_ObjID = fchangeobj;
			this.m_ChangeResID = 0;
			this.m_ChangeMngID = -1L;
			this.m_Step = 0;
		}

		// Token: 0x06001E24 RID: 7716 RVA: 0x000A1E66 File Offset: 0x000A0266
		public RoomComBGChange(long fmanageid, int fresourceid)
		{
			this.m_Type = eRoomRequest.ChangeBG;
			this.m_Enable = true;
			this.m_ChangeResID = fresourceid;
			this.m_ChangeMngID = fmanageid;
			this.m_Step = 1;
		}

		// Token: 0x06001E25 RID: 7717 RVA: 0x000A1E94 File Offset: 0x000A0294
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			bool result = false;
			switch (this.m_Step)
			{
			case 0:
			{
				UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(pbuilder.GetManageID());
				if (manageIDToUserRoomData != null)
				{
					UserRoomData.PlacementData categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Background);
					if (categoryToPlacementData != null)
					{
						UserRoomObjectData userRoomObjectData = UserRoomUtil.GetManageIDToUserRoomObjData(categoryToPlacementData.m_ManageID);
						if (userRoomObjectData != null)
						{
							userRoomObjectData.SetFreeLinkMngID(categoryToPlacementData.m_ManageID);
						}
						userRoomObjectData = UserRoomUtil.GetUserRoomObjData(eRoomObjectCategory.Background, this.m_ObjID);
						if (userRoomObjectData != null)
						{
							this.m_ChangeMngID = (categoryToPlacementData.m_ManageID = userRoomObjectData.GetFreeLinkMngID(true));
							this.m_ChangeResID = (categoryToPlacementData.m_ObjectID = userRoomObjectData.ResourceID);
						}
					}
				}
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(pbuilder.GetManageID());
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
				roomComBuildScript.PlaySend();
				this.m_Step++;
				break;
			}
			case 1:
				this.m_NewModel = (RoomObjectControllCreate.SetupMdlSimple(RoomObjectControllCreate.CreateBGObject(), pbuilder.GetTimeCategory(), -10010, LayerMask.NameToLayer("BG"), this.m_ChangeResID, this.m_ChangeMngID, false) as RoomObjectMdlSprite);
				this.m_Step++;
				break;
			case 2:
				if (!this.m_NewModel.IsPreparing())
				{
					this.m_NewModel.SetSortingOrder(0f, -9990);
					this.m_Step++;
					this.m_NewModel.SetFadeIn(0.3f);
					this.m_Time = 0f;
				}
				break;
			case 3:
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= 0.3f)
				{
					this.m_NewModel.SetSortingOrder(0f, -10000);
					pbuilder.ChangeBG(this.m_NewModel);
					this.m_NewModel = null;
					this.m_Step++;
					result = true;
					this.m_Enable = false;
				}
				break;
			}
			return result;
		}

		// Token: 0x0400249D RID: 9373
		public int m_ObjID;

		// Token: 0x0400249E RID: 9374
		public RoomObjectMdlSprite m_NewModel;

		// Token: 0x0400249F RID: 9375
		public int m_Step;

		// Token: 0x040024A0 RID: 9376
		public int m_ChangeResID;

		// Token: 0x040024A1 RID: 9377
		public long m_ChangeMngID;

		// Token: 0x040024A2 RID: 9378
		public float m_Time;
	}
}
