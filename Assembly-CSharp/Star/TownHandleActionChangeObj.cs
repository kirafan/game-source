﻿using System;

namespace Star
{
	// Token: 0x020006A6 RID: 1702
	public class TownHandleActionChangeObj : ITownHandleAction
	{
		// Token: 0x06002215 RID: 8725 RVA: 0x000B54EC File Offset: 0x000B38EC
		public TownHandleActionChangeObj(int fobjid, long fmanageid)
		{
			this.m_Type = eTownEventAct.ChangeObject;
			this.m_ObjectID = fobjid;
			this.m_ManageID = fmanageid;
		}

		// Token: 0x04002899 RID: 10393
		public int m_ObjectID;

		// Token: 0x0400289A RID: 10394
		public long m_ManageID;
	}
}
