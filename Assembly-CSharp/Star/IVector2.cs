﻿using System;

namespace Star
{
	// Token: 0x02000608 RID: 1544
	public struct IVector2
	{
		// Token: 0x06001E55 RID: 7765 RVA: 0x000A399C File Offset: 0x000A1D9C
		public IVector2(int fx, int fy)
		{
			this.x = fx;
			this.y = fy;
		}

		// Token: 0x040024E0 RID: 9440
		public int x;

		// Token: 0x040024E1 RID: 9441
		public int y;
	}
}
