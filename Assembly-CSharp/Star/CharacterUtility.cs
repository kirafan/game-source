﻿using System;
using System.Collections.Generic;
using System.Text;
using Star.Town;

namespace Star
{
	// Token: 0x0200015C RID: 348
	public static class CharacterUtility
	{
		// Token: 0x060009FE RID: 2558 RVA: 0x0003D300 File Offset: 0x0003B700
		public static void SetupCharacterParam(CharacterParam destParam, long mngID, int charaID, int currentLv, int maxLv, long exp, int limitBreak)
		{
			destParam.MngID = mngID;
			destParam.CharaID = charaID;
			destParam.Lv = currentLv;
			destParam.MaxLv = maxLv;
			destParam.Exp = exp;
			destParam.LimitBreak = limitBreak;
			if (destParam.ClassSkillLearnDatas == null)
			{
				destParam.ClassSkillLearnDatas = new List<SkillLearnData>();
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(destParam.CharaID);
			destParam.NamedType = (eCharaNamedType)param.m_NamedType;
			destParam.RareType = (eRare)param.m_Rare;
			destParam.ClassType = (eClassType)param.m_Class;
			destParam.ElementType = (eElementType)param.m_Element;
			destParam.Cost = param.m_Cost;
			destParam.LeaderSkillID = param.m_LeaderSkillID;
			destParam.Hp = dbMng.CharaParamGrowthListDB.CalcHp(param.m_InitHp, currentLv, param.m_GrowthTableID);
			destParam.Atk = dbMng.CharaParamGrowthListDB.CalcAtk(param.m_InitAtk, currentLv, param.m_GrowthTableID);
			destParam.Mgc = dbMng.CharaParamGrowthListDB.CalcMgc(param.m_InitMgc, currentLv, param.m_GrowthTableID);
			destParam.Def = dbMng.CharaParamGrowthListDB.CalcDef(param.m_InitDef, currentLv, param.m_GrowthTableID);
			destParam.MDef = dbMng.CharaParamGrowthListDB.CalcMDef(param.m_InitMDef, currentLv, param.m_GrowthTableID);
			destParam.Spd = dbMng.CharaParamGrowthListDB.CalcSpd(param.m_InitSpd, currentLv, param.m_GrowthTableID);
			destParam.Luck = dbMng.CharaParamGrowthListDB.CalcLuck(param.m_InitLuck, currentLv, param.m_GrowthTableID);
			destParam.IsRegistAbnormals = new bool[8];
			destParam.ChargeCountMax = 1;
			destParam.StunCoef = param.m_StunCoef;
			destParam.StunerMag = 1f;
			destParam.StunerAvoid = 0f;
			destParam.AIID = -1;
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x0003D4D8 File Offset: 0x0003B8D8
		public static void SetupCharacterParamFromQuestEnemyParam(CharacterParam destParam, ref QuestEnemyListDB_Param questEnemyParam, int currentLv)
		{
			destParam.CharaID = questEnemyParam.m_ID;
			destParam.Lv = currentLv;
			destParam.MaxLv = questEnemyParam.m_MaxLv;
			destParam.Exp = 0L;
			destParam.LimitBreak = 0;
			if (destParam.ClassSkillLearnDatas == null)
			{
				destParam.ClassSkillLearnDatas = new List<SkillLearnData>();
			}
			destParam.NamedType = eCharaNamedType.None;
			destParam.RareType = eRare.Star1;
			destParam.ClassType = eClassType.None;
			destParam.ElementType = (eElementType)questEnemyParam.m_Element;
			destParam.Cost = 0;
			destParam.LeaderSkillID = -1;
			destParam.Hp = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitHp, questEnemyParam.m_MaxHp, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.Atk = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitAtk, questEnemyParam.m_MaxAtk, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.Mgc = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitMgc, questEnemyParam.m_MaxMgc, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.Def = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitDef, questEnemyParam.m_MaxDef, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.MDef = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitMDef, questEnemyParam.m_MaxMDef, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.Spd = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitSpd, questEnemyParam.m_MaxSpd, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.Luck = EditUtility.CalcParamCorrectLv(questEnemyParam.m_InitLuck, questEnemyParam.m_MaxLuck, questEnemyParam.m_InitLv, questEnemyParam.m_MaxLv, currentLv);
			destParam.IsRegistAbnormals = new bool[8];
			for (int i = 0; i < destParam.IsRegistAbnormals.Length; i++)
			{
				if (questEnemyParam.m_IsRegistAbnormals.Length > i)
				{
					destParam.IsRegistAbnormals[i] = ((questEnemyParam.m_IsRegistAbnormals[i] != 1) ? false : true);
				}
			}
			int skillIDNum = QuestEnemyListDB_Ext.GetSkillIDNum(ref questEnemyParam);
			for (int j = 0; j < skillIDNum; j++)
			{
				destParam.ClassSkillLearnDatas.Add(new SkillLearnData(questEnemyParam.m_SkillIDs[j], 1, 1, 0L, -1, questEnemyParam.m_IsConfusionSkillIDs[j] == 1));
			}
			destParam.UniqueSkillLearnData = new SkillLearnData(questEnemyParam.m_CharageSkillID, 1, 1, 0L, -1, false);
			destParam.ChargeCountMax = questEnemyParam.m_CharageMax;
			destParam.StunCoef = questEnemyParam.m_StunCoef;
			destParam.StunerMag = questEnemyParam.m_StunerMag;
			destParam.StunerAvoid = questEnemyParam.m_StunerAvoid;
			destParam.AIID = questEnemyParam.m_AIID;
		}

		// Token: 0x06000A00 RID: 2560 RVA: 0x0003D738 File Offset: 0x0003BB38
		public static void SetupWeaponParam(WeaponParam destParam, long mngID, int weaponID, int currentLv, long exp, int weaponSkillLv, long weaponSkillExp)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			destParam.MngID = mngID;
			destParam.WeaponID = weaponID;
			WeaponListDB_Param param = dbMng.WeaponListDB.GetParam(destParam.WeaponID);
			destParam.Lv = currentLv;
			destParam.MaxLv = param.m_LimitLv;
			destParam.Exp = exp;
			destParam.Cost = param.m_Cost;
			destParam.Atk = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, currentLv);
			destParam.Mgc = EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, currentLv);
			destParam.Def = EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, currentLv);
			destParam.MDef = EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, currentLv);
			destParam.SkillLearnData = new SkillLearnData(param.m_SkillID, weaponSkillLv, dbMng.SkillExpDB.GetMaxLv(param.m_SkillExpTableID), weaponSkillExp, param.m_SkillExpTableID, true);
		}

		// Token: 0x06000A01 RID: 2561 RVA: 0x0003D854 File Offset: 0x0003BC54
		public static void SetupDefaultWeaponParam(WeaponParam destParam, eClassType classType)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			destParam.MngID = -1L;
			destParam.WeaponID = dbMng.ClassListDB.GetParam(classType).m_DefaultWeaponID;
			WeaponListDB_Param param = dbMng.WeaponListDB.GetParam(destParam.WeaponID);
			destParam.Lv = 1;
			destParam.MaxLv = 1;
			destParam.Exp = 0L;
			destParam.Cost = param.m_Cost;
			destParam.Atk = param.m_InitAtk;
			destParam.Mgc = param.m_InitMgc;
			destParam.Def = param.m_InitDef;
			destParam.MDef = param.m_InitMDef;
			destParam.SkillLearnData = new SkillLearnData(param.m_SkillID, 1, 1, 0L, -1, true);
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x0003D910 File Offset: 0x0003BD10
		public static void SetupNamedParam(NamedParam destParam, int friendShip, eCharaNamedType namedType)
		{
			sbyte friendshipTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetFriendshipTableID(namedType);
			CharacterUtility.SetupNamedParam(destParam, friendShip, friendshipTableID);
		}

		// Token: 0x06000A03 RID: 2563 RVA: 0x0003D93B File Offset: 0x0003BD3B
		public static void SetupNamedParam(NamedParam destParam, int friendShip, sbyte friendShipTableID)
		{
			destParam.FriendShip = friendShip;
			destParam.FriendShipMax = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB.GetFriendShipMax(friendShipTableID);
		}

		// Token: 0x06000A04 RID: 2564 RVA: 0x0003D95F File Offset: 0x0003BD5F
		public static eTitleType GetTitleType(eCharaNamedType namedType)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(namedType);
		}

		// Token: 0x06000A05 RID: 2565 RVA: 0x0003D978 File Offset: 0x0003BD78
		public static string GetModelResourcePath(eCharaResourceType resourceType, int resourceID)
		{
			string result = null;
			StringBuilder stringBuilder = new StringBuilder();
			if (resourceType != eCharaResourceType.Player)
			{
				if (resourceType == eCharaResourceType.Enemy)
				{
					stringBuilder.Append("model/enemy/model_en_");
					stringBuilder.Append(resourceID);
					stringBuilder.Append(".muast");
					result = stringBuilder.ToString();
				}
			}
			else
			{
				stringBuilder.Append("model/player/model_pl_");
				stringBuilder.Append(resourceID);
				stringBuilder.Append(".muast");
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x0003D9F8 File Offset: 0x0003BDF8
		public static string GetWeaponModelResourcePath(int weaponID)
		{
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID);
			int num = param.m_ResourceID_L;
			if (num == -1)
			{
				num = param.m_ResourceID_R;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("model/weapon/wpn_");
			stringBuilder.Append(num);
			stringBuilder.Append(".muast");
			return stringBuilder.ToString();
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x0003DA60 File Offset: 0x0003BE60
		public static string[] GetWeaponModelObjectPaths(int weaponID)
		{
			string[] array = new string[2];
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID);
			StringBuilder stringBuilder = new StringBuilder();
			if (param.m_ResourceID_L != -1)
			{
				stringBuilder.Append("WPN_");
				stringBuilder.Append(param.m_ResourceID_L);
				stringBuilder.Append("_L");
				array[0] = stringBuilder.ToString();
			}
			else
			{
				array[0] = string.Empty;
			}
			if (param.m_ResourceID_R != -1)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append("WPN_");
				stringBuilder.Append(param.m_ResourceID_R);
				stringBuilder.Append("_R");
				array[1] = stringBuilder.ToString();
			}
			else
			{
				array[1] = string.Empty;
			}
			return array;
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x0003DB2C File Offset: 0x0003BF2C
		public static List<AnimSettings> GetCharacterAnimSettingsFromCurrentState(eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterDefine.eMode mode, CharacterHandler charaHndl, bool isViewerSetting = false)
		{
			List<AnimSettings> list = new List<AnimSettings>();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder stringBuilder2 = new StringBuilder();
			CharacterParam charaParam = charaHndl.CharaParam;
			switch (mode)
			{
			case CharacterDefine.eMode.Battle:
				if (resourceType != eCharaResourceType.Player)
				{
					if (resourceType == eCharaResourceType.Enemy)
					{
						EnemyResourceListDB_Param param = dbMng.EnemyResourceListDB.GetParam(resourceID);
						stringBuilder2.Length = 0;
						stringBuilder2.Append("anim/enemy/common_en_");
						stringBuilder2.Append(param.m_AnimID);
						stringBuilder2.Append(".muast");
						string resoucePath = stringBuilder2.ToString();
						stringBuilder2.Length = 0;
						stringBuilder2.Append("meigeac_common_en_");
						stringBuilder2.Append(param.m_AnimID);
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@idle");
						list.Add(new AnimSettings("idle", resoucePath, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@abnormal");
						list.Add(new AnimSettings("abnormal", resoucePath, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@damage");
						list.Add(new AnimSettings("damage", resoucePath, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@dead");
						list.Add(new AnimSettings("dead", resoucePath, stringBuilder.ToString()));
						for (int i = 0; i <= 1; i++)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@skill_");
							stringBuilder.Append(i);
							list.Add(new AnimSettings("skill_" + i.ToString(), resoucePath, stringBuilder.ToString()));
						}
						if (param.m_IsBoss == 1)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@charge_skill");
							list.Add(new AnimSettings("charge_skill", resoucePath, stringBuilder.ToString()));
						}
					}
				}
				else
				{
					CharacterListDB_Param param2 = dbMng.CharaListDB.GetParam(charaParam.CharaID);
					ClassListDB_Param param3 = dbMng.ClassListDB.GetParam(charaParam.ClassType);
					NamedListDB_Param param4 = dbMng.NamedListDB.GetParam(charaParam.NamedType);
					if (partsIndex != 0)
					{
						if (partsIndex == 1)
						{
							stringBuilder2.Length = 0;
							stringBuilder2.Append("anim/player/class_");
							stringBuilder2.Append(param3.m_ResouceBaseName.ToLower());
							stringBuilder2.Append("_body");
							stringBuilder2.Append(".muast");
							string resoucePath2 = stringBuilder2.ToString();
							stringBuilder2.Length = 0;
							stringBuilder2.Append("meigeac_");
							stringBuilder2.Append(param3.m_ResouceBaseName.ToLower());
							stringBuilder2.Append("_body");
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@idle");
							list.Add(new AnimSettings("idle", resoucePath2, stringBuilder.ToString()));
							if (charaParam.ClassType == eClassType.Magician)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@abnormal");
								list.Add(new AnimSettings("abnormal", resoucePath2, stringBuilder.ToString()));
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@damage");
								list.Add(new AnimSettings("damage", resoucePath2, stringBuilder.ToString()));
							}
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@attack");
							list.Add(new AnimSettings("attack", resoucePath2, stringBuilder.ToString()));
							for (int j = 1; j <= 3; j++)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@class_skill_");
								stringBuilder.Append(j);
								list.Add(new AnimSettings("class_skill_" + j, resoucePath2, stringBuilder.ToString()));
							}
							stringBuilder2.Length = 0;
							stringBuilder2.Append("anim/player/common_battle_body.muast");
							resoucePath2 = stringBuilder2.ToString();
							stringBuilder2.Length = 0;
							stringBuilder2.Append("meigeac_common_body");
							if (charaParam.ClassType != eClassType.Magician)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@damage");
								list.Add(new AnimSettings("damage", resoucePath2, stringBuilder.ToString()));
							}
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@dead");
							list.Add(new AnimSettings("dead", resoucePath2, stringBuilder.ToString()));
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@battle_in");
							list.Add(new AnimSettings("battle_in", resoucePath2, stringBuilder.ToString()));
							stringBuilder.Length = 0;
							if (param2.m_BodyID == 2)
							{
								stringBuilder.Append("meigeac_");
								stringBuilder.Append(CharacterDefine.BODY_ANIM_PREFIXS[2].ToLower());
							}
							else
							{
								stringBuilder.Append(stringBuilder2.ToString());
							}
							stringBuilder.Append("@battle_out");
							list.Add(new AnimSettings("battle_out", resoucePath2, stringBuilder.ToString()));
							stringBuilder.Length = 0;
							if (param2.m_BodyID == 2)
							{
								stringBuilder.Append("meigeac_");
								stringBuilder.Append(CharacterDefine.BODY_ANIM_PREFIXS[2].ToLower());
							}
							else
							{
								stringBuilder.Append(stringBuilder2.ToString());
							}
							stringBuilder.Append("@battle_run");
							list.Add(new AnimSettings("battle_run", resoucePath2, stringBuilder.ToString()));
							int[] array;
							if (isViewerSetting)
							{
								array = new int[]
								{
									0,
									1,
									2,
									3,
									4
								};
							}
							else
							{
								array = new int[]
								{
									param4.m_BattleWinID
								};
							}
							for (int k = 0; k < array.Length; k++)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@win_st_");
								stringBuilder.Append(array[k]);
								list.Add(new AnimSettings("win_st_" + array[k], resoucePath2, stringBuilder.ToString()));
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@win_lp_");
								stringBuilder.Append(array[k]);
								list.Add(new AnimSettings("win_lp_" + array[k], resoucePath2, stringBuilder.ToString()));
							}
							if (charaParam.ClassType != eClassType.Magician)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@abnormal");
								list.Add(new AnimSettings("abnormal", resoucePath2, stringBuilder.ToString()));
							}
							for (int l = 0; l < 3; l++)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@kirarajump_");
								stringBuilder.Append(l);
								list.Add(new AnimSettings("kirarajump_" + l, resoucePath2, stringBuilder.ToString()));
							}
						}
					}
					else
					{
						stringBuilder2.Length = 0;
						stringBuilder2.Append("anim/player/class_");
						stringBuilder2.Append(param3.m_ResouceBaseName.ToLower());
						stringBuilder2.Append("_head_");
						stringBuilder2.Append(param2.m_HeadID);
						stringBuilder2.Append(".muast");
						string resoucePath3 = stringBuilder2.ToString();
						stringBuilder2.Length = 0;
						stringBuilder2.Append("meigeac_");
						stringBuilder2.Append(param3.m_ResouceBaseName.ToLower());
						stringBuilder2.Append("_head_");
						stringBuilder2.Append(param2.m_HeadID);
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@idle");
						list.Add(new AnimSettings("idle", resoucePath3, stringBuilder.ToString()));
						if (charaParam.ClassType == eClassType.Magician)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@abnormal");
							list.Add(new AnimSettings("abnormal", resoucePath3, stringBuilder.ToString()));
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@damage");
							list.Add(new AnimSettings("damage", resoucePath3, stringBuilder.ToString()));
						}
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@attack");
						list.Add(new AnimSettings("attack", resoucePath3, stringBuilder.ToString()));
						for (int m = 1; m <= 3; m++)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@class_skill_");
							stringBuilder.Append(m);
							list.Add(new AnimSettings("class_skill_" + m, resoucePath3, stringBuilder.ToString()));
						}
						stringBuilder2.Length = 0;
						stringBuilder2.Append("anim/player/common_battle_head_");
						stringBuilder2.Append(param2.m_HeadID);
						stringBuilder2.Append(".muast");
						resoucePath3 = stringBuilder2.ToString();
						stringBuilder2.Length = 0;
						stringBuilder2.Append("meigeac_common_head_");
						stringBuilder2.Append(param2.m_HeadID);
						if (charaParam.ClassType != eClassType.Magician)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@damage");
							list.Add(new AnimSettings("damage", resoucePath3, stringBuilder.ToString()));
						}
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@dead");
						list.Add(new AnimSettings("dead", resoucePath3, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@battle_run");
						list.Add(new AnimSettings("battle_run", resoucePath3, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@battle_in");
						list.Add(new AnimSettings("battle_in", resoucePath3, stringBuilder.ToString()));
						stringBuilder.Length = 0;
						stringBuilder.Append(stringBuilder2.ToString());
						stringBuilder.Append("@battle_out");
						list.Add(new AnimSettings("battle_out", resoucePath3, stringBuilder.ToString()));
						int[] array2;
						if (isViewerSetting)
						{
							array2 = new int[]
							{
								0,
								1,
								2,
								3,
								4
							};
						}
						else
						{
							array2 = new int[]
							{
								param4.m_BattleWinID
							};
						}
						for (int n = 0; n < array2.Length; n++)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@win_st_");
							stringBuilder.Append(array2[n]);
							list.Add(new AnimSettings("win_st_" + array2[n], resoucePath3, stringBuilder.ToString()));
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@win_lp_");
							stringBuilder.Append(array2[n]);
							list.Add(new AnimSettings("win_lp_" + array2[n], resoucePath3, stringBuilder.ToString()));
						}
						if (charaParam.ClassType != eClassType.Magician)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@abnormal");
							list.Add(new AnimSettings("abnormal", resoucePath3, stringBuilder.ToString()));
						}
						for (int num = 0; num < 3; num++)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@kirarajump_");
							stringBuilder.Append(num);
							list.Add(new AnimSettings("kirarajump_" + num, resoucePath3, stringBuilder.ToString()));
						}
					}
				}
				break;
			case CharacterDefine.eMode.Room:
				RoomUtility.GetRoomCharacterAnimSettings(list, resourceType, resourceID, partsIndex, charaParam);
				break;
			case CharacterDefine.eMode.Menu:
				if (resourceType != eCharaResourceType.Player)
				{
					if (resourceType != eCharaResourceType.Enemy)
					{
					}
				}
				else
				{
					CharacterListDB_Param param5 = dbMng.CharaListDB.GetParam(charaParam.CharaID);
					if (partsIndex != 0)
					{
						if (partsIndex == 1)
						{
							stringBuilder2.Length = 0;
							stringBuilder2.Append("anim/player/common_menu_body");
							stringBuilder2.Append(".muast");
							string resoucePath4 = stringBuilder2.ToString();
							stringBuilder2.Length = 0;
							stringBuilder2.Append("meigeac_common_body");
							for (int num2 = 0; num2 < CharacterDefine.MENU_MODE_ACT_KEYS.Length; num2++)
							{
								stringBuilder.Length = 0;
								stringBuilder.Append(stringBuilder2.ToString());
								stringBuilder.Append("@");
								stringBuilder.Append(CharacterDefine.MENU_MODE_ACT_KEYS[num2]);
								list.Add(new AnimSettings(CharacterDefine.MENU_MODE_ACT_KEYS[num2], resoucePath4, stringBuilder.ToString()));
							}
						}
					}
					else
					{
						stringBuilder2.Length = 0;
						stringBuilder2.Append("anim/player/common_menu_head_");
						stringBuilder2.Append(param5.m_HeadID);
						stringBuilder2.Append(".muast");
						string resoucePath5 = stringBuilder2.ToString();
						stringBuilder2.Length = 0;
						stringBuilder2.Append("meigeac_common_head_");
						stringBuilder2.Append(param5.m_HeadID);
						for (int num3 = 0; num3 < CharacterDefine.MENU_MODE_ACT_KEYS.Length; num3++)
						{
							stringBuilder.Length = 0;
							stringBuilder.Append(stringBuilder2.ToString());
							stringBuilder.Append("@");
							stringBuilder.Append(CharacterDefine.MENU_MODE_ACT_KEYS[num3]);
							list.Add(new AnimSettings(CharacterDefine.MENU_MODE_ACT_KEYS[num3], resoucePath5, stringBuilder.ToString()));
						}
					}
				}
				break;
			case CharacterDefine.eMode.Viewer:
				list.AddRange(CharacterUtility.GetCharacterAnimSettingsFromCurrentState(resourceType, resourceID, partsIndex, CharacterDefine.eMode.Battle, charaHndl, true));
				break;
			}
			return list;
		}

		// Token: 0x06000A09 RID: 2569 RVA: 0x0003E9D8 File Offset: 0x0003CDD8
		public static string GetCharacterFacialAnimSetting(eCharaNamedType namedType)
		{
			NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(namedType);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("anim/player/meigeac_");
			stringBuilder.Append(param.m_ResouceBaseName);
			stringBuilder.Append("@facial");
			stringBuilder.Append(".muast");
			return stringBuilder.ToString();
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x0003EA3C File Offset: 0x0003CE3C
		public static string ConvertMeigeFbx2PrefabObjPath(string prefabRootName, bool isIncludePrefabRootName = false)
		{
			StringBuilder stringBuilder = new StringBuilder();
			string value = prefabRootName.Replace("(Clone)", string.Empty);
			if (isIncludePrefabRootName)
			{
				stringBuilder.Append(value);
				stringBuilder.Append("/MeshRoot_");
				stringBuilder.Append(value);
				stringBuilder.Append("/");
				stringBuilder.Append(value);
				stringBuilder.Append("(Clone)");
			}
			else
			{
				stringBuilder.Append("MeshRoot_");
				stringBuilder.Append(value);
				stringBuilder.Append("/");
				stringBuilder.Append(value);
				stringBuilder.Append("(Clone)");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000A0B RID: 2571 RVA: 0x0003EAE1 File Offset: 0x0003CEE1
		public static string GetBattleShadowResourcePath()
		{
			return "model/shadow/shadow_battle.muast";
		}

		// Token: 0x06000A0C RID: 2572 RVA: 0x0003EAE8 File Offset: 0x0003CEE8
		public static string GetRoomShadowResourcePath()
		{
			return "model/shadow/shadow_room.muast";
		}

		// Token: 0x06000A0D RID: 2573 RVA: 0x0003EAF0 File Offset: 0x0003CEF0
		public static string[] GetSAPIDs(CharacterParam charaParam, WeaponParam wpnParam, bool isUserControll)
		{
			List<string> list = new List<string>();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			SkillListDB skillListDB_CARD = dbMng.SkillListDB_CARD;
			if (isUserControll)
			{
				SkillListDB skillListDB_PL = dbMng.SkillListDB_PL;
				SkillListDB skillListDB_WPN = dbMng.SkillListDB_WPN;
				SkillContentListDB skillContentListDB_PL = dbMng.SkillContentListDB_PL;
				SkillContentListDB skillContentListDB_WPN = dbMng.SkillContentListDB_WPN;
				string skillActionPlanID;
				if (charaParam.ClassType != eClassType.None)
				{
					skillActionPlanID = skillListDB_PL.GetSkillActionPlanID(dbMng.ClassListDB.GetParam(charaParam.ClassType).m_NormalAttackSkillID);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
				}
				for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++)
				{
					int skillID = charaParam.ClassSkillLearnDatas[i].SkillID;
					skillActionPlanID = skillListDB_PL.GetSkillActionPlanID(skillID);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
					List<int> refIdsOnCardType = skillContentListDB_PL.GetRefIdsOnCardType(skillID);
					for (int j = 0; j < refIdsOnCardType.Count; j++)
					{
						skillActionPlanID = skillListDB_CARD.GetSkillActionPlanID(refIdsOnCardType[j]);
						if (!string.IsNullOrEmpty(skillActionPlanID))
						{
							list.Add(skillActionPlanID);
						}
					}
				}
				int skillID2 = charaParam.UniqueSkillLearnData.SkillID;
				skillActionPlanID = skillListDB_PL.GetSkillActionPlanID(skillID2);
				if (!string.IsNullOrEmpty(skillActionPlanID))
				{
					list.Add(skillActionPlanID);
				}
				List<int> refIdsOnCardType2 = skillContentListDB_PL.GetRefIdsOnCardType(skillID2);
				for (int k = 0; k < refIdsOnCardType2.Count; k++)
				{
					skillActionPlanID = skillListDB_CARD.GetSkillActionPlanID(refIdsOnCardType2[k]);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
				}
				if (wpnParam != null && wpnParam.WeaponID != -1)
				{
					int skillID3 = dbMng.WeaponListDB.GetParam(wpnParam.WeaponID).m_SkillID;
					skillActionPlanID = skillListDB_WPN.GetSkillActionPlanID(skillID3);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
					List<int> refIdsOnCardType3 = skillContentListDB_WPN.GetRefIdsOnCardType(skillID3);
					for (int l = 0; l < refIdsOnCardType3.Count; l++)
					{
						skillActionPlanID = skillListDB_CARD.GetSkillActionPlanID(refIdsOnCardType3[l]);
						if (!string.IsNullOrEmpty(skillActionPlanID))
						{
							list.Add(skillActionPlanID);
						}
					}
				}
			}
			else
			{
				SkillListDB skillListDB_EN = dbMng.SkillListDB_EN;
				SkillContentListDB skillContentListDB_EN = dbMng.SkillContentListDB_EN;
				int skillID4 = charaParam.UniqueSkillLearnData.SkillID;
				string skillActionPlanID = skillListDB_EN.GetSkillActionPlanID(skillID4);
				if (!string.IsNullOrEmpty(skillActionPlanID))
				{
					list.Add(skillActionPlanID);
				}
				List<int> refIdsOnCardType4 = skillContentListDB_EN.GetRefIdsOnCardType(skillID4);
				for (int m = 0; m < refIdsOnCardType4.Count; m++)
				{
					skillActionPlanID = skillListDB_CARD.GetSkillActionPlanID(refIdsOnCardType4[m]);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
				}
				for (int n = 0; n < charaParam.ClassSkillLearnDatas.Count; n++)
				{
					int skillID5 = charaParam.ClassSkillLearnDatas[n].SkillID;
					skillActionPlanID = skillListDB_EN.GetSkillActionPlanID(skillID5);
					if (!string.IsNullOrEmpty(skillActionPlanID))
					{
						list.Add(skillActionPlanID);
					}
					List<int> refIdsOnCardType5 = skillContentListDB_EN.GetRefIdsOnCardType(skillID5);
					for (int num = 0; num < refIdsOnCardType5.Count; num++)
					{
						skillActionPlanID = skillListDB_CARD.GetSkillActionPlanID(refIdsOnCardType5[num]);
						if (!string.IsNullOrEmpty(skillActionPlanID))
						{
							list.Add(skillActionPlanID);
						}
					}
				}
			}
			HashSet<string> hashSet = new HashSet<string>(list);
			string[] array = new string[hashSet.Count];
			hashSet.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06000A0E RID: 2574 RVA: 0x0003EE5C File Offset: 0x0003D25C
		public static string[] GetSAGIDs(CharacterParam charaParam, WeaponParam wpnParam, bool isUserControll)
		{
			List<string> list = new List<string>();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			SkillListDB skillListDB_CARD = dbMng.SkillListDB_CARD;
			if (isUserControll)
			{
				SkillListDB skillListDB_PL = dbMng.SkillListDB_PL;
				SkillListDB skillListDB_WPN = dbMng.SkillListDB_WPN;
				SkillContentListDB skillContentListDB_PL = dbMng.SkillContentListDB_PL;
				SkillContentListDB skillContentListDB_WPN = dbMng.SkillContentListDB_WPN;
				string skillActionGraphicsID;
				if (charaParam.ClassType != eClassType.None)
				{
					skillActionGraphicsID = skillListDB_PL.GetSkillActionGraphicsID(dbMng.ClassListDB.GetParam(charaParam.ClassType).m_NormalAttackSkillID);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
				}
				for (int i = 0; i < charaParam.ClassSkillLearnDatas.Count; i++)
				{
					int skillID = charaParam.ClassSkillLearnDatas[i].SkillID;
					skillActionGraphicsID = skillListDB_PL.GetSkillActionGraphicsID(skillID);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
					List<int> refIdsOnCardType = skillContentListDB_PL.GetRefIdsOnCardType(skillID);
					for (int j = 0; j < refIdsOnCardType.Count; j++)
					{
						skillActionGraphicsID = skillListDB_CARD.GetSkillActionGraphicsID(refIdsOnCardType[j]);
						if (!string.IsNullOrEmpty(skillActionGraphicsID))
						{
							list.Add(skillActionGraphicsID);
						}
					}
				}
				int skillID2 = charaParam.UniqueSkillLearnData.SkillID;
				skillActionGraphicsID = skillListDB_PL.GetSkillActionGraphicsID(skillID2);
				if (!string.IsNullOrEmpty(skillActionGraphicsID))
				{
					list.Add(skillActionGraphicsID);
				}
				List<int> refIdsOnCardType2 = skillContentListDB_PL.GetRefIdsOnCardType(skillID2);
				for (int k = 0; k < refIdsOnCardType2.Count; k++)
				{
					skillActionGraphicsID = skillListDB_CARD.GetSkillActionGraphicsID(refIdsOnCardType2[k]);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
				}
				if (wpnParam != null && wpnParam.WeaponID != -1)
				{
					int skillID3 = dbMng.WeaponListDB.GetParam(wpnParam.WeaponID).m_SkillID;
					skillActionGraphicsID = skillListDB_WPN.GetSkillActionGraphicsID(skillID3);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
					List<int> refIdsOnCardType3 = skillContentListDB_WPN.GetRefIdsOnCardType(skillID3);
					for (int l = 0; l < refIdsOnCardType3.Count; l++)
					{
						skillActionGraphicsID = skillListDB_CARD.GetSkillActionGraphicsID(refIdsOnCardType3[l]);
						if (!string.IsNullOrEmpty(skillActionGraphicsID))
						{
							list.Add(skillActionGraphicsID);
						}
					}
				}
			}
			else
			{
				SkillListDB skillListDB_EN = dbMng.SkillListDB_EN;
				SkillContentListDB skillContentListDB_EN = dbMng.SkillContentListDB_EN;
				int skillID4 = charaParam.UniqueSkillLearnData.SkillID;
				string skillActionGraphicsID = skillListDB_EN.GetSkillActionGraphicsID(skillID4);
				if (!string.IsNullOrEmpty(skillActionGraphicsID))
				{
					list.Add(skillActionGraphicsID);
				}
				List<int> refIdsOnCardType4 = skillContentListDB_EN.GetRefIdsOnCardType(skillID4);
				for (int m = 0; m < refIdsOnCardType4.Count; m++)
				{
					skillActionGraphicsID = skillListDB_CARD.GetSkillActionGraphicsID(refIdsOnCardType4[m]);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
				}
				for (int n = 0; n < charaParam.ClassSkillLearnDatas.Count; n++)
				{
					int skillID5 = charaParam.ClassSkillLearnDatas[n].SkillID;
					skillActionGraphicsID = skillListDB_EN.GetSkillActionGraphicsID(skillID5);
					if (!string.IsNullOrEmpty(skillActionGraphicsID))
					{
						list.Add(skillActionGraphicsID);
					}
					List<int> refIdsOnCardType5 = skillContentListDB_EN.GetRefIdsOnCardType(skillID5);
					for (int num = 0; num < refIdsOnCardType5.Count; num++)
					{
						skillActionGraphicsID = skillListDB_CARD.GetSkillActionGraphicsID(refIdsOnCardType5[num]);
						if (!string.IsNullOrEmpty(skillActionGraphicsID))
						{
							list.Add(skillActionGraphicsID);
						}
					}
				}
			}
			HashSet<string> hashSet = new HashSet<string>(list);
			string[] array = new string[hashSet.Count];
			hashSet.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06000A0F RID: 2575 RVA: 0x0003F1C7 File Offset: 0x0003D5C7
		public static string GetSkillActionPlanResourcePath()
		{
			return "battle/sap/sap.muast";
		}

		// Token: 0x06000A10 RID: 2576 RVA: 0x0003F1CE File Offset: 0x0003D5CE
		public static string GetSkillActionPlanObjectPath(string sapID)
		{
			return "SAP_" + sapID;
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0003F1DB File Offset: 0x0003D5DB
		public static string GetSkillActionGraphicsResourcePath()
		{
			return "battle/sag/sag.muast";
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x0003F1E2 File Offset: 0x0003D5E2
		public static string GetSkillActionGraphicsObjectPath(string sagID)
		{
			return "SAG_" + sagID;
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x0003F1EF File Offset: 0x0003D5EF
		public static string GetBattleAIDataResourcePath()
		{
			return "database/battleai/battleai.muast";
		}

		// Token: 0x06000A14 RID: 2580 RVA: 0x0003F1F6 File Offset: 0x0003D5F6
		public static string GetBattleAIDataObjectPath(int AIID)
		{
			return "BattleAIData_" + AIID;
		}

		// Token: 0x06000A15 RID: 2581 RVA: 0x0003F208 File Offset: 0x0003D608
		public static string GetTweetListDBResourcePath(eCharaNamedType namedType)
		{
			return "Data/Database/TweetList_" + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetResourceBaseName(namedType);
		}

		// Token: 0x06000A16 RID: 2582 RVA: 0x0003F22C File Offset: 0x0003D62C
		public static float[] CalcCharaParamApplyTownBuff(TownBuff srcTownBuff, CharacterParam srcCharaParam)
		{
			float[] array = new float[13];
			List<TownBuff.BuffParam> buffParam = srcTownBuff.GetBuffParam(CharacterUtility.GetTitleType(srcCharaParam.NamedType), srcCharaParam.ClassType, srcCharaParam.ElementType);
			if (buffParam != null)
			{
				for (int i = 0; i < buffParam.Count; i++)
				{
					array[(int)buffParam[i].m_Type] += CharacterUtility.CalcTownBuff(buffParam[i].m_Type, buffParam[i].m_Val, srcCharaParam);
				}
			}
			return array;
		}

		// Token: 0x06000A17 RID: 2583 RVA: 0x0003F2B4 File Offset: 0x0003D6B4
		public static float[] CalcCharaParamApplyTownBuff(float[] srcTownBuffValues, CharacterParam srcCharaParam)
		{
			float[] array = new float[13];
			for (int i = 0; i < srcTownBuffValues.Length; i++)
			{
				array[i] = CharacterUtility.CalcTownBuff((eTownObjectBuffType)i, srcTownBuffValues[i], srcCharaParam);
			}
			return array;
		}

		// Token: 0x06000A18 RID: 2584 RVA: 0x0003F2EC File Offset: 0x0003D6EC
		public static float CalcTownBuff(eTownObjectBuffType type, float value, CharacterParam srcCharaParam)
		{
			float result = 0f;
			float num = value * 0.01f;
			switch (type)
			{
			case eTownObjectBuffType.Hp:
				result = (float)srcCharaParam.Hp * num;
				break;
			case eTownObjectBuffType.Atk:
				result = (float)srcCharaParam.Atk * num;
				break;
			case eTownObjectBuffType.Mgc:
				result = (float)srcCharaParam.Mgc * num;
				break;
			case eTownObjectBuffType.Def:
				result = (float)srcCharaParam.Def * num;
				break;
			case eTownObjectBuffType.MDef:
				result = (float)srcCharaParam.MDef * num;
				break;
			case eTownObjectBuffType.Spd:
				result = (float)srcCharaParam.Spd * num;
				break;
			case eTownObjectBuffType.Luck:
				result = (float)srcCharaParam.Luck * num;
				break;
			case eTownObjectBuffType.ResistFire:
			case eTownObjectBuffType.ResistWater:
			case eTownObjectBuffType.ResistEarth:
			case eTownObjectBuffType.ResistWind:
			case eTownObjectBuffType.ResistMoon:
			case eTownObjectBuffType.ResistSun:
				result = num;
				break;
			}
			return result;
		}
	}
}
