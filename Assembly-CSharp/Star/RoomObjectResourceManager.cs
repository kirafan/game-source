﻿using System;

namespace Star
{
	// Token: 0x020005F0 RID: 1520
	public sealed class RoomObjectResourceManager
	{
		// Token: 0x06001E01 RID: 7681 RVA: 0x000A1578 File Offset: 0x0009F978
		public RoomObjectResourceManager()
		{
			ResourceObjectHandler.eLoadType[] array = new ResourceObjectHandler.eLoadType[3];
			array[0] = ResourceObjectHandler.eLoadType.Sprite;
			this.LOAD_TYPES = array;
			base..ctor();
			this.m_Loaders = new ResourcesLoader[3];
			for (int i = 0; i < 3; i++)
			{
				this.m_Loaders[i] = new ResourcesLoader(this.SIMULTANEOUSLY_LOAD_NUMS[i]);
			}
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x000A15E8 File Offset: 0x0009F9E8
		public void Update()
		{
			for (int i = 0; i < 3; i++)
			{
				this.m_Loaders[i].Update();
			}
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x000A1614 File Offset: 0x0009FA14
		public bool IsAvailable()
		{
			return true;
		}

		// Token: 0x06001E04 RID: 7684 RVA: 0x000A1618 File Offset: 0x0009FA18
		public bool IsLoadingAny()
		{
			for (int i = 0; i < 3; i++)
			{
				if (this.m_Loaders[i].IsLoadingResources())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001E05 RID: 7685 RVA: 0x000A164C File Offset: 0x0009FA4C
		public ResourceObjectHandler LoadAsyncFromResources(RoomObjectResourceManager.eType type, string path)
		{
			return this.m_Loaders[(int)type].LoadAsyncFromResources(path, this.LOAD_TYPES[(int)type]);
		}

		// Token: 0x06001E06 RID: 7686 RVA: 0x000A1664 File Offset: 0x0009FA64
		public void UnloadFromResources(RoomObjectResourceManager.eType type, string path)
		{
			this.m_Loaders[(int)type].UnloadFromResources(path);
		}

		// Token: 0x06001E07 RID: 7687 RVA: 0x000A1674 File Offset: 0x0009FA74
		public void UnloadAllResources(RoomObjectResourceManager.eType type)
		{
			this.m_Loaders[(int)type].UnloadAllResources();
		}

		// Token: 0x06001E08 RID: 7688 RVA: 0x000A1683 File Offset: 0x0009FA83
		public bool IsCompleteUnloadAllResources(RoomObjectResourceManager.eType type)
		{
			return this.m_Loaders[(int)type].IsCompleteUnloadAllResources();
		}

		// Token: 0x06001E09 RID: 7689 RVA: 0x000A1694 File Offset: 0x0009FA94
		public void UnloadAllResources()
		{
			for (int i = 0; i < this.m_Loaders.Length; i++)
			{
				this.m_Loaders[i].UnloadAllResources();
			}
		}

		// Token: 0x06001E0A RID: 7690 RVA: 0x000A16C8 File Offset: 0x0009FAC8
		public bool IsCompleteUnloadAllResources()
		{
			for (int i = 0; i < this.m_Loaders.Length; i++)
			{
				if (!this.m_Loaders[i].IsCompleteUnloadAllResources())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04002492 RID: 9362
		public const int TYPE_NUM = 3;

		// Token: 0x04002493 RID: 9363
		private readonly int[] SIMULTANEOUSLY_LOAD_NUMS = new int[]
		{
			10,
			10,
			10
		};

		// Token: 0x04002494 RID: 9364
		private readonly ResourceObjectHandler.eLoadType[] LOAD_TYPES;

		// Token: 0x04002495 RID: 9365
		private ResourcesLoader[] m_Loaders;

		// Token: 0x020005F1 RID: 1521
		public enum eType
		{
			// Token: 0x04002497 RID: 9367
			Sprite,
			// Token: 0x04002498 RID: 9368
			Model,
			// Token: 0x04002499 RID: 9369
			Anim,
			// Token: 0x0400249A RID: 9370
			Num
		}
	}
}
