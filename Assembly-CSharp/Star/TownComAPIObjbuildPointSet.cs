﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x0200068B RID: 1675
	public class TownComAPIObjbuildPointSet : INetComHandle
	{
		// Token: 0x060021BC RID: 8636 RVA: 0x000B3A14 File Offset: 0x000B1E14
		public TownComAPIObjbuildPointSet()
		{
			this.ApiName = "player/town_facility/build_point/set";
			this.Request = true;
			this.ResponseType = typeof(Buildpointset);
		}

		// Token: 0x04002825 RID: 10277
		public long managedTownFacilityId;

		// Token: 0x04002826 RID: 10278
		public int buildPointIndex;

		// Token: 0x04002827 RID: 10279
		public int openState;

		// Token: 0x04002828 RID: 10280
		public long buildTime;

		// Token: 0x04002829 RID: 10281
		public int actionNo;
	}
}
