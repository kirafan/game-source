﻿using System;

namespace Star
{
	// Token: 0x020006FE RID: 1790
	public class TownBuildUpAreaParam : ITownEventCommand
	{
		// Token: 0x06002379 RID: 9081 RVA: 0x000BE698 File Offset: 0x000BCA98
		public TownBuildUpAreaParam(int fbuildpoint, long fmanageid, int fobjid)
		{
			this.m_Type = eTownRequest.BuildArea;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_Step = TownBuildUpAreaParam.eStep.BuildQue;
		}

		// Token: 0x0600237A RID: 9082 RVA: 0x000BE6C4 File Offset: 0x000BCAC4
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (this.m_Step == TownBuildUpAreaParam.eStep.BuildQue)
			{
				if (pbuilder.BuildToArea(this))
				{
					this.m_Step = TownBuildUpAreaParam.eStep.SetUpCheck;
				}
			}
			else if (pbuilder.IsBuildMngIDToBuildUp(this.m_ManageID))
			{
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002A42 RID: 10818
		public int m_BuildPointID;

		// Token: 0x04002A43 RID: 10819
		public int m_ObjID;

		// Token: 0x04002A44 RID: 10820
		public long m_ManageID;

		// Token: 0x04002A45 RID: 10821
		public TownBuildUpAreaParam.eStep m_Step;

		// Token: 0x020006FF RID: 1791
		public enum eStep
		{
			// Token: 0x04002A47 RID: 10823
			BuildQue,
			// Token: 0x04002A48 RID: 10824
			SetUpCheck
		}
	}
}
