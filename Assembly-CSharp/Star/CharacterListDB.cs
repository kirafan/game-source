﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000179 RID: 377
	public class CharacterListDB : ScriptableObject
	{
		// Token: 0x04000A03 RID: 2563
		public CharacterListDB_Param[] m_Params;
	}
}
