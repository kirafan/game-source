﻿using System;

namespace Star
{
	// Token: 0x0200022A RID: 554
	public enum eItemType
	{
		// Token: 0x04000F25 RID: 3877
		Upgrade,
		// Token: 0x04000F26 RID: 3878
		Evolution,
		// Token: 0x04000F27 RID: 3879
		LimitBreak,
		// Token: 0x04000F28 RID: 3880
		Weapon,
		// Token: 0x04000F29 RID: 3881
		QuestKey,
		// Token: 0x04000F2A RID: 3882
		GachaTicket,
		// Token: 0x04000F2B RID: 3883
		TradeItem,
		// Token: 0x04000F2C RID: 3884
		Stamina
	}
}
