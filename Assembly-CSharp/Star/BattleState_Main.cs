﻿using System;

namespace Star
{
	// Token: 0x020003E3 RID: 995
	public class BattleState_Main : BattleState
	{
		// Token: 0x060012EB RID: 4843 RVA: 0x000651CC File Offset: 0x000635CC
		public BattleState_Main(BattleMain owner) : base(owner)
		{
		}

		// Token: 0x060012EC RID: 4844 RVA: 0x000651DC File Offset: 0x000635DC
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x060012ED RID: 4845 RVA: 0x000651DF File Offset: 0x000635DF
		public override void OnStateEnter()
		{
			this.m_Step = BattleState_Main.eStep.First;
		}

		// Token: 0x060012EE RID: 4846 RVA: 0x000651E8 File Offset: 0x000635E8
		public override void OnStateExit()
		{
		}

		// Token: 0x060012EF RID: 4847 RVA: 0x000651EA File Offset: 0x000635EA
		public override void OnDispose()
		{
		}

		// Token: 0x060012F0 RID: 4848 RVA: 0x000651EC File Offset: 0x000635EC
		public override int OnStateUpdate()
		{
			BattleState_Main.eStep step = this.m_Step;
			if (step != BattleState_Main.eStep.First)
			{
				if (step == BattleState_Main.eStep.Main)
				{
					if (this.m_Owner.System.IsBattleFinished())
					{
						this.m_Step = BattleState_Main.eStep.None;
						return 2;
					}
				}
			}
			else if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.m_Owner.System.SystemAwake();
				this.m_Step = BattleState_Main.eStep.Main;
			}
			return -1;
		}

		// Token: 0x040019C1 RID: 6593
		private BattleState_Main.eStep m_Step = BattleState_Main.eStep.None;

		// Token: 0x020003E4 RID: 996
		private enum eStep
		{
			// Token: 0x040019C3 RID: 6595
			None = -1,
			// Token: 0x040019C4 RID: 6596
			First,
			// Token: 0x040019C5 RID: 6597
			Main
		}
	}
}
