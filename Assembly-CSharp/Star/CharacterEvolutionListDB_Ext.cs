﻿using System;

namespace Star
{
	// Token: 0x0200018A RID: 394
	public static class CharacterEvolutionListDB_Ext
	{
		// Token: 0x06000AD8 RID: 2776 RVA: 0x00040C5C File Offset: 0x0003F05C
		public static bool GetParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_SrcCharaID == srcCharaID)
				{
					result = self.m_Params[i];
					return true;
				}
			}
			result = default(CharacterEvolutionListDB_Param);
			return false;
		}

		// Token: 0x06000AD9 RID: 2777 RVA: 0x00040CBC File Offset: 0x0003F0BC
		public static bool GetNormalMaterialParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_SrcCharaID == srcCharaID && self.m_Params[i].m_RecipeType == 0)
				{
					result = self.m_Params[i];
					return true;
				}
			}
			result = default(CharacterEvolutionListDB_Param);
			return false;
		}

		// Token: 0x06000ADA RID: 2778 RVA: 0x00040D30 File Offset: 0x0003F130
		public static bool GetSpecifyMaterialParamBySrcCharaID(this CharacterEvolutionListDB self, int srcCharaID, out CharacterEvolutionListDB_Param result)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_SrcCharaID == srcCharaID && self.m_Params[i].m_RecipeType == 1)
				{
					result = self.m_Params[i];
					return true;
				}
			}
			result = default(CharacterEvolutionListDB_Param);
			return false;
		}

		// Token: 0x06000ADB RID: 2779 RVA: 0x00040DA8 File Offset: 0x0003F1A8
		public static bool IsExistParam(this CharacterEvolutionListDB self, int srcCharaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_SrcCharaID == srcCharaID)
				{
					return true;
				}
			}
			return false;
		}
	}
}
