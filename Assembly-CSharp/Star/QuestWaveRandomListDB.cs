﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001EE RID: 494
	public class QuestWaveRandomListDB : ScriptableObject
	{
		// Token: 0x04000BF4 RID: 3060
		public QuestWaveRandomListDB_Param[] m_Params;
	}
}
