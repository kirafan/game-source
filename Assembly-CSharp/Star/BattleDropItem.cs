﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000BD RID: 189
	[Serializable]
	public class BattleDropItem
	{
		// Token: 0x0600050F RID: 1295 RVA: 0x0001ABC3 File Offset: 0x00018FC3
		public bool IsDrop()
		{
			return this.GetDropDataNum() > 0;
		}

		// Token: 0x06000510 RID: 1296 RVA: 0x0001ABD4 File Offset: 0x00018FD4
		public int GetDropDataNum()
		{
			return this.Datas.Count;
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x0001ABE1 File Offset: 0x00018FE1
		public BattleDropItem.DropData GetDropDataAt(int index)
		{
			return this.Datas[index];
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x0001ABF0 File Offset: 0x00018FF0
		public void Setup(ref QuestWaveDropItemDB_Param ref_param, float probAdd)
		{
			for (int i = 0; i < ref_param.m_DropItemIDs.Length; i++)
			{
				int num = ref_param.m_DropItemIDs[i];
				if (num != -1)
				{
					float num2 = UnityEngine.Random.Range(0f, 100f);
					float num3 = ref_param.m_DropProbabilitys[i] + probAdd;
					if (num2 <= num3)
					{
						BattleDropItem.DropData dropData = new BattleDropItem.DropData();
						dropData.ID = ref_param.m_DropItemIDs[i];
						dropData.Num = Mathf.Max(1, ref_param.m_DropItemNums[i]);
						this.Datas.Add(dropData);
					}
				}
			}
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x0001AC88 File Offset: 0x00019088
		public int GetTreasureBoxIndex()
		{
			int num = -1;
			for (int i = 0; i < this.Datas.Count; i++)
			{
				int num2 = BattleDropItem.ConvertItemIdToTreasureBoxIndex(this.Datas[i].ID);
				if (num2 > num)
				{
					num = num2;
				}
			}
			return num;
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x0001ACD4 File Offset: 0x000190D4
		public static int ConvertItemIdToTreasureBoxIndex(int itemID)
		{
			if (itemID == -1)
			{
				return -1;
			}
			return BattleDropItem.ConvertRareToTreasureBoxIndex((eRare)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID).m_Rare);
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x0001AD0C File Offset: 0x0001910C
		public static int ConvertRareToTreasureBoxIndex(eRare rare)
		{
			switch (rare)
			{
			case eRare.Star1:
			case eRare.Star2:
				return 0;
			case eRare.Star3:
			case eRare.Star4:
				return 1;
			case eRare.Star5:
				return 2;
			default:
				return -1;
			}
		}

		// Token: 0x040003E6 RID: 998
		[SerializeField]
		private List<BattleDropItem.DropData> Datas = new List<BattleDropItem.DropData>();

		// Token: 0x020000BE RID: 190
		[Serializable]
		public class DropData
		{
			// Token: 0x040003E7 RID: 999
			public int ID;

			// Token: 0x040003E8 RID: 1000
			public int Num;
		}
	}
}
