﻿using System;

namespace Star
{
	// Token: 0x0200018D RID: 397
	public static class CharacterFlavorTextDB_Ext
	{
		// Token: 0x06000AE0 RID: 2784 RVA: 0x00040F28 File Offset: 0x0003F328
		public static string GetFlavorText(this CharacterFlavorTextDB self, int charaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CharaID == charaID)
				{
					return self.m_Params[i].m_FlavorText;
				}
			}
			return string.Empty;
		}
	}
}
