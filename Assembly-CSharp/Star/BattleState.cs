﻿using System;

namespace Star
{
	// Token: 0x020003E0 RID: 992
	public class BattleState : GameStateBase
	{
		// Token: 0x060012DF RID: 4831 RVA: 0x00064F60 File Offset: 0x00063360
		public BattleState(BattleMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x060012E0 RID: 4832 RVA: 0x00064F6F File Offset: 0x0006336F
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x060012E1 RID: 4833 RVA: 0x00064F72 File Offset: 0x00063372
		public override void OnStateEnter()
		{
		}

		// Token: 0x060012E2 RID: 4834 RVA: 0x00064F74 File Offset: 0x00063374
		public override void OnStateExit()
		{
		}

		// Token: 0x060012E3 RID: 4835 RVA: 0x00064F76 File Offset: 0x00063376
		public override void OnDispose()
		{
		}

		// Token: 0x060012E4 RID: 4836 RVA: 0x00064F78 File Offset: 0x00063378
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x060012E5 RID: 4837 RVA: 0x00064F7B File Offset: 0x0006337B
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040019B7 RID: 6583
		protected BattleMain m_Owner;
	}
}
