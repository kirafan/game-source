﻿using System;
using Star.UI;
using Star.UI.LoginBonus;

namespace Star
{
	// Token: 0x02000429 RID: 1065
	public class LoginBonusState_Main : LoginBonusState
	{
		// Token: 0x06001476 RID: 5238 RVA: 0x0006C1EC File Offset: 0x0006A5EC
		public LoginBonusState_Main(LoginBonusMain owner) : base(owner)
		{
		}

		// Token: 0x06001477 RID: 5239 RVA: 0x0006C204 File Offset: 0x0006A604
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001478 RID: 5240 RVA: 0x0006C207 File Offset: 0x0006A607
		public override void OnStateEnter()
		{
			this.m_Step = LoginBonusState_Main.eStep.First;
		}

		// Token: 0x06001479 RID: 5241 RVA: 0x0006C210 File Offset: 0x0006A610
		public override void OnStateExit()
		{
		}

		// Token: 0x0600147A RID: 5242 RVA: 0x0006C212 File Offset: 0x0006A612
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600147B RID: 5243 RVA: 0x0006C21C File Offset: 0x0006A61C
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case LoginBonusState_Main.eStep.First:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = LoginBonusState_Main.eStep.LoadWait;
				}
				break;
			case LoginBonusState_Main.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = LoginBonusState_Main.eStep.PlayIn;
				}
				break;
			case LoginBonusState_Main.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = LoginBonusState_Main.eStep.Main;
				}
				break;
			case LoginBonusState_Main.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_Step = LoginBonusState_Main.eStep.UnloadChildScene;
				}
				break;
			case LoginBonusState_Main.eStep.UnloadChildScene:
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				this.m_Step = LoginBonusState_Main.eStep.UnloadChildSceneWait;
				break;
			case LoginBonusState_Main.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = LoginBonusState_Main.eStep.None;
					SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Town);
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600147C RID: 5244 RVA: 0x0006C34C File Offset: 0x0006A74C
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<LoginBonusUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			return !(this.m_UI == null);
		}

		// Token: 0x04001B4A RID: 6986
		private LoginBonusState_Main.eStep m_Step = LoginBonusState_Main.eStep.None;

		// Token: 0x04001B4B RID: 6987
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.LoginBonusUI;

		// Token: 0x04001B4C RID: 6988
		private LoginBonusUI m_UI;

		// Token: 0x0200042A RID: 1066
		private enum eStep
		{
			// Token: 0x04001B4E RID: 6990
			None = -1,
			// Token: 0x04001B4F RID: 6991
			First,
			// Token: 0x04001B50 RID: 6992
			LoadWait,
			// Token: 0x04001B51 RID: 6993
			PlayIn,
			// Token: 0x04001B52 RID: 6994
			Main,
			// Token: 0x04001B53 RID: 6995
			UnloadChildScene,
			// Token: 0x04001B54 RID: 6996
			UnloadChildSceneWait
		}
	}
}
