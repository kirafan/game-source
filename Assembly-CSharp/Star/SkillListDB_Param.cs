﻿using System;

namespace Star
{
	// Token: 0x02000209 RID: 521
	[Serializable]
	public struct SkillListDB_Param
	{
		// Token: 0x04000CD6 RID: 3286
		public int m_ID;

		// Token: 0x04000CD7 RID: 3287
		public string m_SkillName;

		// Token: 0x04000CD8 RID: 3288
		public string m_SkillDetail;

		// Token: 0x04000CD9 RID: 3289
		public int m_SkillType;

		// Token: 0x04000CDA RID: 3290
		public string m_UniqueSkillScene;

		// Token: 0x04000CDB RID: 3291
		public string m_UniqueSkillVoiceCueSheet;

		// Token: 0x04000CDC RID: 3292
		public string m_UniqueSkillSeCueSheet;

		// Token: 0x04000CDD RID: 3293
		public string m_SAP;

		// Token: 0x04000CDE RID: 3294
		public string m_SAG;

		// Token: 0x04000CDF RID: 3295
		public int[] m_Recasts;

		// Token: 0x04000CE0 RID: 3296
		public float[] m_LoadFactors;
	}
}
