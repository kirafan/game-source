﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x0200040D RID: 1037
	public class EditState_MasterEquip : EditState
	{
		// Token: 0x060013B6 RID: 5046 RVA: 0x0006988F File Offset: 0x00067C8F
		public EditState_MasterEquip(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013B7 RID: 5047 RVA: 0x000698AF File Offset: 0x00067CAF
		public override int GetStateID()
		{
			return 7;
		}

		// Token: 0x060013B8 RID: 5048 RVA: 0x000698B2 File Offset: 0x00067CB2
		public override void OnStateEnter()
		{
			this.m_Step = EditState_MasterEquip.eStep.First;
		}

		// Token: 0x060013B9 RID: 5049 RVA: 0x000698BB File Offset: 0x00067CBB
		public override void OnStateExit()
		{
		}

		// Token: 0x060013BA RID: 5050 RVA: 0x000698BD File Offset: 0x00067CBD
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060013BB RID: 5051 RVA: 0x000698C8 File Offset: 0x00067CC8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_MasterEquip.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_MasterEquip.eStep.LoadWait;
				break;
			case EditState_MasterEquip.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_MasterEquip.eStep.PlayIn;
				}
				break;
			case EditState_MasterEquip.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_MasterEquip.eStep.Main;
				}
				break;
			case EditState_MasterEquip.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_MasterEquip.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_MasterEquip.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_MasterEquip.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060013BC RID: 5052 RVA: 0x000699E8 File Offset: 0x00067DE8
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MasterEquipUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.MasterEquipUI].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.MasterEquip);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060013BD RID: 5053 RVA: 0x00069A6C File Offset: 0x00067E6C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetEquipUserMasterOrbMngID() != this.m_UI.CurrentEquipMasterOrbMngID)
			{
				this.Request_MasterOrbSet(this.m_UI.CurrentEquipMasterOrbMngID, new MeigewwwParam.Callback(this.OnResponse_MasterOrbSet));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060013BE RID: 5054 RVA: 0x00069AC7 File Offset: 0x00067EC7
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060013BF RID: 5055 RVA: 0x00069AE4 File Offset: 0x00067EE4
		public void Request_MasterOrbSet(long euipMasterOrbMngID, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Masterorbset masterorbset = new PlayerRequestTypes.Masterorbset();
			masterorbset.managedMasterOrbId = euipMasterOrbMngID;
			this.m_RequestEuipMasterOrbMngID = euipMasterOrbMngID;
			MeigewwwParam wwwParam = PlayerRequest.Masterorbset(masterorbset, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013C0 RID: 5056 RVA: 0x00069B24 File Offset: 0x00067F24
		private void OnResponse_MasterOrbSet(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Masterorbset masterorbset = PlayerResponse.Masterorbset(wwwParam, ResponseCommon.DialogType.None, null);
			if (masterorbset == null)
			{
				return;
			}
			ResultCode result = masterorbset.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				for (int i = 0; i < userDataMng.UserMasterOrbDatas.Count; i++)
				{
					userDataMng.UserMasterOrbDatas[i].IsEquip = (userDataMng.UserMasterOrbDatas[i].MngID == this.m_RequestEuipMasterOrbMngID);
				}
				this.GoToMenuEnd();
			}
		}

		// Token: 0x04001ABB RID: 6843
		private EditState_MasterEquip.eStep m_Step = EditState_MasterEquip.eStep.None;

		// Token: 0x04001ABC RID: 6844
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MasterEquipUI;

		// Token: 0x04001ABD RID: 6845
		public MasterEquipUI m_UI;

		// Token: 0x04001ABE RID: 6846
		private long m_RequestEuipMasterOrbMngID = -1L;

		// Token: 0x0200040E RID: 1038
		private enum eStep
		{
			// Token: 0x04001AC0 RID: 6848
			None = -1,
			// Token: 0x04001AC1 RID: 6849
			First,
			// Token: 0x04001AC2 RID: 6850
			LoadWait,
			// Token: 0x04001AC3 RID: 6851
			PlayIn,
			// Token: 0x04001AC4 RID: 6852
			Main,
			// Token: 0x04001AC5 RID: 6853
			UnloadChildSceneWait
		}
	}
}
