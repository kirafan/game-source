﻿using System;

namespace Star
{
	// Token: 0x02000428 RID: 1064
	public class LoginBonusState : GameStateBase
	{
		// Token: 0x0600146F RID: 5231 RVA: 0x0006C1CF File Offset: 0x0006A5CF
		public LoginBonusState(LoginBonusMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001470 RID: 5232 RVA: 0x0006C1DE File Offset: 0x0006A5DE
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001471 RID: 5233 RVA: 0x0006C1E1 File Offset: 0x0006A5E1
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001472 RID: 5234 RVA: 0x0006C1E3 File Offset: 0x0006A5E3
		public override void OnStateExit()
		{
		}

		// Token: 0x06001473 RID: 5235 RVA: 0x0006C1E5 File Offset: 0x0006A5E5
		public override void OnDispose()
		{
		}

		// Token: 0x06001474 RID: 5236 RVA: 0x0006C1E7 File Offset: 0x0006A5E7
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001475 RID: 5237 RVA: 0x0006C1EA File Offset: 0x0006A5EA
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001B49 RID: 6985
		protected LoginBonusMain m_Owner;
	}
}
