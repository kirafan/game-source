﻿using System;

namespace Star
{
	// Token: 0x020002E3 RID: 739
	public class EquipWeaponManagedSupportPartyController : EquipWeaponManagedPartyControllerExGen<EquipWeaponManagedSupportPartyController, EquipWeaponManagedSupportPartyMemberController>
	{
		// Token: 0x06000E62 RID: 3682 RVA: 0x0004DD4D File Offset: 0x0004C14D
		public EquipWeaponManagedSupportPartyController() : base(eCharacterDataControllerType.ManagedSupportParty)
		{
		}

		// Token: 0x06000E63 RID: 3683 RVA: 0x0004DD56 File Offset: 0x0004C156
		public override EquipWeaponManagedSupportPartyController Setup(int partyIndex)
		{
			return this.SetupEx(partyIndex);
		}

		// Token: 0x06000E64 RID: 3684 RVA: 0x0004DD5F File Offset: 0x0004C15F
		public override EquipWeaponManagedSupportPartyController SetupEx(int partyIndex)
		{
			base.SetupEx(partyIndex);
			return this.SetupProcess(partyIndex);
		}

		// Token: 0x06000E65 RID: 3685 RVA: 0x0004DD70 File Offset: 0x0004C170
		protected EquipWeaponManagedSupportPartyController SetupProcess(int partyIndex)
		{
			int supportLimit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.SupportLimit;
			this.partyMembers = new EquipWeaponPartyMemberController[8];
			this.enablePartyMembers = new EquipWeaponManagedSupportPartyMemberController[supportLimit];
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserSupportPartyData userSupportPartyData = userDataMng.UserSupportPartyDatas[partyIndex];
			this.m_MngID = userSupportPartyData.MngID;
			this.m_Name = userSupportPartyData.PartyName;
			for (int i = 0; i < supportLimit; i++)
			{
				this.enablePartyMembers[i] = new EquipWeaponManagedSupportPartyMemberController().SetupEx(this.m_PartyIndex, i);
				this.partyMembers[i] = this.enablePartyMembers[i];
			}
			for (int j = supportLimit; j < 8; j++)
			{
				this.partyMembers[j] = new EquipWeaponManagedSupportPartyMemberController().Setup(this.m_PartyIndex, j);
			}
			return this;
		}

		// Token: 0x040015A3 RID: 5539
		public const int PARTYPANEL_CHARA_ALL = 8;
	}
}
