﻿using System;

namespace Star
{
	// Token: 0x020002DF RID: 735
	public class EquipWeaponPartyController
	{
		// Token: 0x06000E49 RID: 3657 RVA: 0x0004DAD3 File Offset: 0x0004BED3
		public EquipWeaponPartyController(eCharacterDataControllerType memberType)
		{
		}

		// Token: 0x06000E4A RID: 3658 RVA: 0x0004DADB File Offset: 0x0004BEDB
		protected virtual EquipWeaponPartyController SetupBase(int partyIndex)
		{
			this.m_PartyIndex = partyIndex;
			return this;
		}

		// Token: 0x06000E4B RID: 3659 RVA: 0x0004DAE5 File Offset: 0x0004BEE5
		public eCharacterDataControllerType GetMemberType()
		{
			return this.m_MemberType;
		}

		// Token: 0x06000E4C RID: 3660 RVA: 0x0004DAED File Offset: 0x0004BEED
		public int HowManyPartyMemberAll()
		{
			return this.partyMembers.Length;
		}

		// Token: 0x06000E4D RID: 3661 RVA: 0x0004DAF7 File Offset: 0x0004BEF7
		public virtual int HowManyPartyMemberEnable()
		{
			return -1;
		}

		// Token: 0x06000E4E RID: 3662 RVA: 0x0004DAFA File Offset: 0x0004BEFA
		public long GetMngId()
		{
			return this.m_MngID;
		}

		// Token: 0x06000E4F RID: 3663 RVA: 0x0004DB02 File Offset: 0x0004BF02
		public int GetPartyIndex()
		{
			return this.m_PartyIndex;
		}

		// Token: 0x06000E50 RID: 3664 RVA: 0x0004DB0A File Offset: 0x0004BF0A
		public string GetPartyName()
		{
			return this.m_Name;
		}

		// Token: 0x06000E51 RID: 3665 RVA: 0x0004DB14 File Offset: 0x0004BF14
		public virtual int GetPartyCost()
		{
			int num = 0;
			for (int i = 0; i < this.partyMembers.Length; i++)
			{
				EquipWeaponPartyMemberController equipWeaponPartyMemberController = this.partyMembers[i];
				if (equipWeaponPartyMemberController != null)
				{
					num += equipWeaponPartyMemberController.GetCost();
				}
			}
			return num;
		}

		// Token: 0x06000E52 RID: 3666 RVA: 0x0004DB55 File Offset: 0x0004BF55
		public EquipWeaponPartyMemberController GetMember(int index)
		{
			if (index < 0)
			{
				return null;
			}
			return this.partyMembers[index];
		}

		// Token: 0x06000E53 RID: 3667 RVA: 0x0004DB68 File Offset: 0x0004BF68
		public void SetPartyName(string partyName)
		{
			this.m_Name = partyName;
		}

		// Token: 0x0400159B RID: 5531
		protected EquipWeaponPartyMemberController[] partyMembers;

		// Token: 0x0400159C RID: 5532
		protected eCharacterDataControllerType m_MemberType;

		// Token: 0x0400159D RID: 5533
		protected int m_PartyIndex;

		// Token: 0x0400159E RID: 5534
		protected long m_MngID;

		// Token: 0x0400159F RID: 5535
		protected string m_Name;
	}
}
