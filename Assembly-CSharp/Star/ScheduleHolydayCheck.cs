﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000395 RID: 917
	public class ScheduleHolydayCheck : IDataBaseResource
	{
		// Token: 0x06001147 RID: 4423 RVA: 0x0005A809 File Offset: 0x00058C09
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
			this.m_Database = (pdataobj as ScheduleHolydayDB);
		}

		// Token: 0x06001148 RID: 4424 RVA: 0x0005A818 File Offset: 0x00058C18
		public int GetSecToHolydayType(long ftimesec)
		{
			int result = 0;
			DateTime dateTime = ScheduleTimeUtil.ChangeBaseTimeToDateTime(ftimesec * 1000L * 1000L * 10L);
			int num = this.m_Database.m_Params.Length;
			bool flag = false;
			int year = dateTime.Year;
			int month = dateTime.Month;
			int day = dateTime.Day;
			for (int i = 0; i < num; i++)
			{
				if ((int)this.m_Database.m_Params[i].m_Year == year && (int)this.m_Database.m_Params[i].m_Month == month && (int)this.m_Database.m_Params[i].m_Day == day)
				{
					result = 1;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				switch (dateTime.DayOfWeek)
				{
				case DayOfWeek.Sunday:
				case DayOfWeek.Saturday:
					result = 1;
					break;
				}
			}
			return result;
		}

		// Token: 0x0400181B RID: 6171
		private ScheduleHolydayDB m_Database;
	}
}
