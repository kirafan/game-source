﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x0200058E RID: 1422
	public class RoomComAPIRemove : INetComHandle
	{
		// Token: 0x06001BAC RID: 7084 RVA: 0x0009295B File Offset: 0x00090D5B
		public RoomComAPIRemove()
		{
			this.ApiName = "player/room/remove";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x04002292 RID: 8850
		public long managedRoomId;
	}
}
