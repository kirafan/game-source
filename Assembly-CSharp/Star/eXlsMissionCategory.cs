﻿using System;

namespace Star
{
	// Token: 0x020004EF RID: 1263
	public enum eXlsMissionCategory
	{
		// Token: 0x04001FA8 RID: 8104
		Days,
		// Token: 0x04001FA9 RID: 8105
		Week,
		// Token: 0x04001FAA RID: 8106
		Unlock,
		// Token: 0x04001FAB RID: 8107
		Event,
		// Token: 0x04001FAC RID: 8108
		CharaUp
	}
}
