﻿using System;

namespace Star
{
	// Token: 0x0200047C RID: 1148
	public class ShopState : GameStateBase
	{
		// Token: 0x06001670 RID: 5744 RVA: 0x00075382 File Offset: 0x00073782
		public ShopState(ShopMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001671 RID: 5745 RVA: 0x00075398 File Offset: 0x00073798
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001672 RID: 5746 RVA: 0x0007539B File Offset: 0x0007379B
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001673 RID: 5747 RVA: 0x0007539D File Offset: 0x0007379D
		public override void OnStateExit()
		{
		}

		// Token: 0x06001674 RID: 5748 RVA: 0x0007539F File Offset: 0x0007379F
		public override void OnDispose()
		{
		}

		// Token: 0x06001675 RID: 5749 RVA: 0x000753A1 File Offset: 0x000737A1
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001676 RID: 5750 RVA: 0x000753A4 File Offset: 0x000737A4
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001D31 RID: 7473
		protected ShopMain m_Owner;

		// Token: 0x04001D32 RID: 7474
		protected int m_NextState = -1;
	}
}
