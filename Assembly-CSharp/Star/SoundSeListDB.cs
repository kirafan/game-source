﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000219 RID: 537
	public class SoundSeListDB : ScriptableObject
	{
		// Token: 0x04000DF7 RID: 3575
		public SoundSeListDB_Param[] m_Params;
	}
}
