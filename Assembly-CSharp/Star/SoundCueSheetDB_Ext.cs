﻿using System;

namespace Star
{
	// Token: 0x020001AC RID: 428
	public static class SoundCueSheetDB_Ext
	{
		// Token: 0x06000B21 RID: 2849 RVA: 0x00042514 File Offset: 0x00040914
		public static SoundCueSheetDB_Param GetParam(this SoundCueSheetDB self, string cueSheet)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CueSheet == cueSheet)
				{
					return self.m_Params[i];
				}
			}
			return default(SoundCueSheetDB_Param);
		}

		// Token: 0x06000B22 RID: 2850 RVA: 0x00042571 File Offset: 0x00040971
		public static SoundCueSheetDB_Param GetParam(this SoundCueSheetDB self, eSoundCueSheetDB cueSheetID)
		{
			return self.m_Params[(int)cueSheetID];
		}

		// Token: 0x06000B23 RID: 2851 RVA: 0x00042584 File Offset: 0x00040984
		public static string GetQueSheet(this SoundCueSheetDB self, eSoundCueSheetDB cueSheetID)
		{
			return self.m_Params[(int)cueSheetID].m_CueSheet;
		}
	}
}
