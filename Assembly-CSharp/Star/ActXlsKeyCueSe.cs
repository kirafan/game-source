﻿using System;

namespace Star
{
	// Token: 0x02000578 RID: 1400
	public class ActXlsKeyCueSe : ActXlsKeyBase
	{
		// Token: 0x06001B2D RID: 6957 RVA: 0x0008FA31 File Offset: 0x0008DE31
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_CueName);
			pio.Float(ref this.m_Volume);
		}

		// Token: 0x04002220 RID: 8736
		public string m_CueName;

		// Token: 0x04002221 RID: 8737
		public float m_Volume;
	}
}
