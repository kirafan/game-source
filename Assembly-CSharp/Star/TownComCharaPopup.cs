﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020004AF RID: 1199
	public class TownComCharaPopup : IMainComCommand
	{
		// Token: 0x0600176D RID: 5997 RVA: 0x000794FC File Offset: 0x000778FC
		public override int GetHashCode()
		{
			return 5;
		}

		// Token: 0x04001E27 RID: 7719
		public int m_CharaID;

		// Token: 0x04001E28 RID: 7720
		public long m_ManageID;

		// Token: 0x04001E29 RID: 7721
		public int m_MessageID;

		// Token: 0x04001E2A RID: 7722
		public Vector3 m_Position;

		// Token: 0x04001E2B RID: 7723
		public int m_TownObjectID;

		// Token: 0x04001E2C RID: 7724
		public int m_SpecialNo;

		// Token: 0x04001E2D RID: 7725
		public List<TownPartsGetItem> m_PartsGetItemList;
	}
}
