﻿using System;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000594 RID: 1428
	public class RoomComAPIObjBuy : INetComHandle
	{
		// Token: 0x06001BB2 RID: 7090 RVA: 0x00092A57 File Offset: 0x00090E57
		public RoomComAPIObjBuy()
		{
			this.ApiName = "player/room_object/buy";
			this.Request = true;
			this.ResponseType = typeof(Buy);
		}

		// Token: 0x04002297 RID: 8855
		public string roomObjectId;

		// Token: 0x04002298 RID: 8856
		public string amount;
	}
}
