﻿using System;

namespace Star
{
	// Token: 0x0200011D RID: 285
	[Serializable]
	public class SkillActionEvent_WeaponVisible : SkillActionEvent_Base
	{
		// Token: 0x0400073B RID: 1851
		public bool m_IsLeftWeapon;

		// Token: 0x0400073C RID: 1852
		public bool m_IsVisible;

		// Token: 0x0400073D RID: 1853
		public bool m_IsPosReset;
	}
}
