﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200066B RID: 1643
	public class WebViewController : MonoBehaviour
	{
		// Token: 0x06002143 RID: 8515 RVA: 0x000B0F2D File Offset: 0x000AF32D
		public bool IsActiveWebViewObj()
		{
			return this.m_WebViewObject != null;
		}

		// Token: 0x06002144 RID: 8516 RVA: 0x000B0F3C File Offset: 0x000AF33C
		private Rect TranslateCMRectToMargin(RectTransform target, Camera uiCamera)
		{
			Vector2 vector = uiCamera.WorldToScreenPoint(target.position);
			vector.y = (float)uiCamera.pixelHeight - vector.y;
			Vector2 sizeDelta = target.sizeDelta;
			float num = (float)Screen.width / 1334f;
			sizeDelta.x *= num;
			sizeDelta.y *= num;
			Rect result = default(Rect);
			result.xMin = vector.x - sizeDelta.x / 2f;
			result.xMax = (float)Screen.width - (result.xMin + sizeDelta.x);
			result.yMin = vector.y - sizeDelta.y / 2f;
			result.yMax = (float)Screen.height - (result.yMin + sizeDelta.y);
			return result;
		}

		// Token: 0x06002145 RID: 8517 RVA: 0x000B1020 File Offset: 0x000AF420
		public void LoadURL(string url, RectTransform rect, Action<string> callback = null)
		{
			Rect rect2 = this.TranslateCMRectToMargin(rect, SingletonMonoBehaviour<GameSystem>.Inst.UICamera);
			this.LoadURL(url, (int)rect2.xMin, (int)rect2.yMin, (int)rect2.xMax, (int)rect2.yMax, callback);
		}

		// Token: 0x06002146 RID: 8518 RVA: 0x000B1068 File Offset: 0x000AF468
		public void LoadURL(string url, int marginLeft, int marginTop, int marginRight, int marginBottom, Action<string> callback = null)
		{
			if (!this.CreateAndOldDestroyWebViewObject())
			{
				return;
			}
			this.m_URL = url;
			this.m_Margin = default(Rect);
			this.m_Margin.xMin = (float)marginLeft;
			this.m_Margin.yMin = (float)marginTop;
			this.m_Margin.xMax = (float)marginRight;
			this.m_Margin.yMax = (float)marginBottom;
			this.m_CallBack = callback;
			this.m_WebViewObject.Init(delegate(string msg)
			{
				Debug.Log(string.Format("CallFromJS[{0}]", msg));
				if (callback != null)
				{
					callback(msg);
				}
			}, false, this.UserAgent(), delegate(string msg)
			{
				Debug.Log(string.Format("CallOnError[{0}]", msg));
			}, delegate(string msg)
			{
				Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
			}, true);
			this.SetMargins(marginLeft, marginTop, marginRight, marginBottom);
			this.SetVisibility(true);
			this.Load(url);
		}

		// Token: 0x06002147 RID: 8519 RVA: 0x000B115A File Offset: 0x000AF55A
		private void Load(string url)
		{
			if (this.m_WebViewObject == null)
			{
				return;
			}
			this.m_WebViewObject.LoadURL(url.Replace(" ", "%20"));
			this.SetVisibility(true);
		}

		// Token: 0x06002148 RID: 8520 RVA: 0x000B1190 File Offset: 0x000AF590
		private void LateUpdate()
		{
			if (this.m_WebViewObject != null && SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.IsActive)
				{
					if (this.m_SystemVisible)
					{
						this.SetSystemVisibility(false);
					}
				}
				else if (!this.m_SystemVisible)
				{
					this.SetSystemVisibility(true);
				}
			}
		}

		// Token: 0x06002149 RID: 8521 RVA: 0x000B120C File Offset: 0x000AF60C
		private void SetSystemVisibility(bool flg)
		{
			if (this.m_WebViewObject != null)
			{
				if (flg && !this.m_SystemVisible)
				{
					this.m_SystemVisible = flg;
					this.CreateAndOldDestroyWebViewObject();
					this.LoadURL(this.m_URL, (int)this.m_Margin.xMin, (int)this.m_Margin.yMin, (int)this.m_Margin.xMax, (int)this.m_Margin.yMax, this.m_CallBack);
				}
				else if (!flg && this.m_SystemVisible)
				{
					this.m_SystemVisible = flg;
					if (this.m_WebViewObject != null)
					{
						this.m_WebViewObject.SetVisibility(this.m_SystemVisible && this.m_Visibility);
					}
				}
			}
		}

		// Token: 0x0600214A RID: 8522 RVA: 0x000B12D8 File Offset: 0x000AF6D8
		public void SetVisibility(bool flg)
		{
			if (this.m_WebViewObject != null)
			{
				this.m_Visibility = flg;
				this.m_WebViewObject.SetVisibility(this.m_SystemVisible && this.m_Visibility);
			}
		}

		// Token: 0x0600214B RID: 8523 RVA: 0x000B1311 File Offset: 0x000AF711
		public void SetMargins(WebViewMargins margins)
		{
			this.SetMargins(margins.m_Left, margins.m_Top, margins.m_Right, margins.m_Bottom);
		}

		// Token: 0x0600214C RID: 8524 RVA: 0x000B1331 File Offset: 0x000AF731
		public void SetMargins(int left, int top, int right, int bottom)
		{
			if (this.m_WebViewObject == null)
			{
				return;
			}
			this.m_WebViewObject.SetMargins(left, top, right, bottom);
		}

		// Token: 0x0600214D RID: 8525 RVA: 0x000B1355 File Offset: 0x000AF755
		private bool CreateAndOldDestroyWebViewObject()
		{
			this.DestroyWebViewObject();
			return this.AcquireWebViewObject();
		}

		// Token: 0x0600214E RID: 8526 RVA: 0x000B1364 File Offset: 0x000AF764
		private WebViewObject CreateWebViewObject()
		{
			GameObject gameObject = new GameObject("WebViewObject");
			if (gameObject != null)
			{
				return gameObject.AddComponent<WebViewObject>();
			}
			return null;
		}

		// Token: 0x0600214F RID: 8527 RVA: 0x000B1390 File Offset: 0x000AF790
		public bool DestroyWebViewObject()
		{
			if (this.m_WebViewObject == null)
			{
				return true;
			}
			if (this.m_WebViewObject.gameObject != null)
			{
				UnityEngine.Object.Destroy(this.m_WebViewObject.gameObject);
			}
			else
			{
				UnityEngine.Object.Destroy(this.m_WebViewObject);
			}
			this.m_WebViewObject = null;
			return true;
		}

		// Token: 0x06002150 RID: 8528 RVA: 0x000B13F0 File Offset: 0x000AF7F0
		public Rect MarginToRect(WebViewMargins margins)
		{
			return new Rect
			{
				xMin = (float)margins.m_Left,
				yMin = (float)(-(float)(Screen.height - margins.m_Bottom)),
				xMax = (float)(Screen.width - margins.m_Right),
				yMax = (float)(-(float)margins.m_Top)
			};
		}

		// Token: 0x06002151 RID: 8529 RVA: 0x000B144C File Offset: 0x000AF84C
		private bool AcquireWebViewObject()
		{
			if (this.m_WebViewObject != null)
			{
				return true;
			}
			this.m_WebViewObject = this.CreateWebViewObject();
			return this.m_WebViewObject != null;
		}

		// Token: 0x06002152 RID: 8530 RVA: 0x000B147C File Offset: 0x000AF87C
		private string UserAgent()
		{
			string str = string.Empty;
			str += "Android/";
			return str + SystemInfo.deviceModel;
		}

		// Token: 0x040027A7 RID: 10151
		private WebViewObject m_WebViewObject;

		// Token: 0x040027A8 RID: 10152
		private bool m_Visibility;

		// Token: 0x040027A9 RID: 10153
		private bool m_SystemVisible = true;

		// Token: 0x040027AA RID: 10154
		private string m_URL;

		// Token: 0x040027AB RID: 10155
		private Rect m_Margin;

		// Token: 0x040027AC RID: 10156
		private Action<string> m_CallBack;
	}
}
