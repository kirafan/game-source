﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B4 RID: 1460
	public class RoomCharaActEnjoy : IRoomCharaAct
	{
		// Token: 0x06001C51 RID: 7249 RVA: 0x000979B1 File Offset: 0x00095DB1
		public void RequestEnjoyMode(RoomObjectCtrlChara pbase)
		{
			pbase.PlayMotion(RoomDefine.eAnim.Enjoy, WrapMode.ClampForever);
		}

		// Token: 0x06001C52 RID: 7250 RVA: 0x000979BC File Offset: 0x00095DBC
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			if (!pbase.IsPlayingMotion(RoomDefine.eAnim.Enjoy))
			{
				result = false;
			}
			return result;
		}
	}
}
