﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004D8 RID: 1240
	public class PinchEventListener : MonoBehaviour
	{
		// Token: 0x17000186 RID: 390
		// (get) Token: 0x0600187C RID: 6268 RVA: 0x0007FC76 File Offset: 0x0007E076
		// (set) Token: 0x0600187D RID: 6269 RVA: 0x0007FC7E File Offset: 0x0007E07E
		public float InitialPinchDist { get; set; }

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x0600187E RID: 6270 RVA: 0x0007FC87 File Offset: 0x0007E087
		// (set) Token: 0x0600187F RID: 6271 RVA: 0x0007FC8F File Offset: 0x0007E08F
		public bool IsPinching { get; set; }

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06001880 RID: 6272 RVA: 0x0007FC98 File Offset: 0x0007E098
		// (set) Token: 0x06001881 RID: 6273 RVA: 0x0007FCA0 File Offset: 0x0007E0A0
		public Vector2 FocusedPos { get; set; }

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06001882 RID: 6274 RVA: 0x0007FCA9 File Offset: 0x0007E0A9
		// (set) Token: 0x06001883 RID: 6275 RVA: 0x0007FCB1 File Offset: 0x0007E0B1
		public float PinchRatio { get; set; }

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x06001884 RID: 6276 RVA: 0x0007FCBC File Offset: 0x0007E0BC
		// (remove) Token: 0x06001885 RID: 6277 RVA: 0x0007FCF4 File Offset: 0x0007E0F4
		public event Action<PinchEventListener> OnPinch;

		// Token: 0x06001886 RID: 6278 RVA: 0x0007FD2C File Offset: 0x0007E12C
		private void Update()
		{
			if (Input.touchCount == 2)
			{
				if (!this.IsPinching)
				{
					this.InitialPinchDist = this.CalcPinchDist();
					this.m_CurrentPinchDist = this.InitialPinchDist;
					this.PinchRatio = 1f;
					this.FocusedPos = this.CalcFocusPos();
					this.IsPinching = true;
					if (this.OnPinch != null)
					{
						this.OnPinch(this);
					}
				}
				else
				{
					this.m_OldPinchRatio = this.PinchRatio;
					this.m_CurrentPinchDist = this.CalcPinchDist();
					this.PinchRatio = this.m_CurrentPinchDist / this.InitialPinchDist;
					if (this.m_OldPinchRatio != this.m_CurrentPinchDist && this.OnPinch != null)
					{
						this.OnPinch(this);
					}
				}
			}
			else if (this.IsPinching)
			{
				this.m_CurrentPinchDist = 1f;
				this.InitialPinchDist = 1f;
				this.PinchRatio = -1f;
				this.IsPinching = false;
				if (this.OnPinch != null)
				{
					this.OnPinch(this);
				}
			}
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x0007FE44 File Offset: 0x0007E244
		private Vector2 CalcFocusPos()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position + position2) * 0.5f;
			}
			return Input.mousePosition;
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x0007FE9C File Offset: 0x0007E29C
		private float CalcPinchDist()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position - position2).magnitude;
			}
			return 0f;
		}

		// Token: 0x04001F42 RID: 8002
		private float m_CurrentPinchDist;

		// Token: 0x04001F43 RID: 8003
		private float m_OldPinchRatio;
	}
}
