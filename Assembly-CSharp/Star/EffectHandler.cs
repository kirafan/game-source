﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002F1 RID: 753
	public class EffectHandler : EffectHandlerBase
	{
		// Token: 0x06000E94 RID: 3732 RVA: 0x0004E383 File Offset: 0x0004C783
		private void LateUpdate()
		{
			this.UpdateMeshColor();
		}

		// Token: 0x06000E95 RID: 3733 RVA: 0x0004E38B File Offset: 0x0004C78B
		public override void OnDestroyToCache()
		{
			this.EmitterKill();
			this.Stop();
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x0004E39C File Offset: 0x0004C79C
		public bool IsPlaying()
		{
			WrapMode wrapMode = this.m_MeigeAnimCtrl.m_WrapMode;
			if (wrapMode != WrapMode.ClampForever)
			{
				if (this.m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
				{
					return true;
				}
			}
			else if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
			{
				return true;
			}
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				if (this.m_MsbHndl.GetParticleEmitter(i).isAlive)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x0004E42C File Offset: 0x0004C82C
		public void Setup()
		{
			if (this.m_MeigeAnimCtrl != null && this.m_MeigeAnimClipHolder != null && this.m_MsbHndl != null)
			{
				this.m_MeigeAnimCtrl.Open();
				this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, this.m_MeigeAnimClipHolder);
				this.m_MeigeAnimCtrl.Close();
			}
			Renderer[] componentsInChildren = base.gameObject.GetComponentsInChildren<Renderer>();
			if (componentsInChildren != null)
			{
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].enabled = false;
				}
			}
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x0004E4D0 File Offset: 0x0004C8D0
		public void Play(Vector3 position, Quaternion rotation, Vector3 localScale, Transform parent, bool isLocalPosition, WrapMode wrapMode, float animationPlaySpeed)
		{
			if (parent != null)
			{
				this.m_Transform.SetParent(parent, false);
			}
			if (isLocalPosition)
			{
				this.m_Transform.localPosition = position;
			}
			else
			{
				this.m_Transform.position = position;
			}
			if (isLocalPosition)
			{
				this.m_Transform.localRotation = rotation;
			}
			else
			{
				this.m_Transform.rotation = rotation;
			}
			this.m_Transform.localScale = localScale;
			this.m_MsbHndl.SetDefaultVisibility();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.ResetMeshColor();
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, wrapMode);
			this.m_MeigeAnimCtrl.SetAnimationPlaySpeed(animationPlaySpeed);
		}

		// Token: 0x06000E99 RID: 3737 RVA: 0x0004E5B1 File Offset: 0x0004C9B1
		public void SetScale(float scale)
		{
			this.m_Transform.localScale = new Vector3(scale, scale, scale);
		}

		// Token: 0x06000E9A RID: 3738 RVA: 0x0004E5C6 File Offset: 0x0004C9C6
		public void SetScale(Vector3 scale)
		{
			this.m_Transform.localScale = scale;
		}

		// Token: 0x06000E9B RID: 3739 RVA: 0x0004E5D4 File Offset: 0x0004C9D4
		public void SetSortingOrder(int sortingOrder)
		{
			if (this.m_MsbHndl != null)
			{
				Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
				for (int i = 0; i < rendererCache.Length; i++)
				{
					rendererCache[i].sortingOrder = sortingOrder;
				}
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int j = 0; j < particleEmitterNum; j++)
				{
					this.m_MsbHndl.GetParticleEmitter(j).SetSortingOrder(sortingOrder);
				}
			}
		}

		// Token: 0x06000E9C RID: 3740 RVA: 0x0004E64C File Offset: 0x0004CA4C
		public void SetLayer(int layerNo, bool needSetChildrens = true)
		{
			this.m_Transform.gameObject.SetLayer(layerNo, needSetChildrens);
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).SetLayer(layerNo);
			}
		}

		// Token: 0x06000E9D RID: 3741 RVA: 0x0004E69C File Offset: 0x0004CA9C
		public void Stop()
		{
			this.m_MeigeAnimCtrl.Pause();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Activate(false);
			}
		}

		// Token: 0x06000E9E RID: 3742 RVA: 0x0004E6E4 File Offset: 0x0004CAE4
		public float GetPlayingSec()
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler("Take 001");
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime * this.m_MeigeAnimCtrl.m_NormalizedPlayTime;
			}
			return 0f;
		}

		// Token: 0x06000E9F RID: 3743 RVA: 0x0004E720 File Offset: 0x0004CB20
		public float GetMaxSec()
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler("Take 001");
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime;
			}
			return 0f;
		}

		// Token: 0x06000EA0 RID: 3744 RVA: 0x0004E750 File Offset: 0x0004CB50
		public void DisableOtherThanParticles(float fadeSec)
		{
			this.m_MeigeAnimCtrl.Pause();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Activate(false);
			}
			this.FadeIn(fadeSec, false);
		}

		// Token: 0x06000EA1 RID: 3745 RVA: 0x0004E7A0 File Offset: 0x0004CBA0
		public void EmitterKill()
		{
			if (this.m_MsbHndl != null)
			{
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int i = 0; i < particleEmitterNum; i++)
				{
					this.m_MsbHndl.GetParticleEmitter(i).Kill();
				}
			}
		}

		// Token: 0x06000EA2 RID: 3746 RVA: 0x0004E7EE File Offset: 0x0004CBEE
		public void ApplyControllParam_Visible(int index)
		{
			if (this.m_ControllParam_Visible != null)
			{
				this.m_ControllParam_Visible.Apply(index, this);
			}
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x0004E808 File Offset: 0x0004CC08
		public void ApplyControllParam_Color(int index)
		{
			if (this.m_ControllParam_Color != null)
			{
				this.m_ControllParam_Color.Apply(index, this);
			}
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x0004E824 File Offset: 0x0004CC24
		public void ChangeMeshColor(Color startColor, Color endColor, float sec)
		{
			this.m_FadeType = -1;
			this.m_StartColor = startColor;
			this.m_EndColor = endColor;
			this.m_MeshColorTime = sec;
			this.m_MeshColorTimer = 0f;
			Color meshColor = this.m_StartColor;
			if (sec <= 0f)
			{
				meshColor = this.m_EndColor;
			}
			this.SetMeshColor(meshColor);
			if (this.m_MeshColorTime <= 0f)
			{
				this.m_IsUpdateMeshColor = false;
			}
			else
			{
				this.m_IsUpdateMeshColor = true;
			}
		}

		// Token: 0x06000EA5 RID: 3749 RVA: 0x0004E89B File Offset: 0x0004CC9B
		public void FadeIn(float sec, bool isCancelOnFadeComplete)
		{
			this.m_FadeType = 0;
			this.m_MeshColorTime = sec;
			this.m_MeshColorTimer = 0f;
			this.m_IsUpdateMeshColor = true;
			this.m_IsCancelOnFadeComplete = isCancelOnFadeComplete;
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x0004E8C4 File Offset: 0x0004CCC4
		public void FadeOut(float sec, bool isCancelOnFadeComplete)
		{
			this.m_FadeType = 1;
			this.m_MeshColorTime = sec;
			this.m_MeshColorTimer = 0f;
			this.m_IsUpdateMeshColor = true;
			this.m_IsCancelOnFadeComplete = isCancelOnFadeComplete;
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x0004E8F0 File Offset: 0x0004CCF0
		private void UpdateMeshColor()
		{
			if (!this.m_IsUpdateMeshColor)
			{
				return;
			}
			this.m_MeshColorTimer += Time.deltaTime;
			float num = this.m_MeshColorTimer / this.m_MeshColorTime;
			if (num > 1f)
			{
				num = 1f;
			}
			if (this.m_FadeType != -1)
			{
				float meshColorA = (this.m_FadeType != 0) ? Mathf.Lerp(0f, 1f, num) : Mathf.Lerp(1f, 0f, num);
				this.SetMeshColorA(meshColorA);
				if (num >= 1f && this.m_IsCancelOnFadeComplete)
				{
					this.m_IsUpdateMeshColor = false;
				}
			}
			else
			{
				Color meshColor = Color.Lerp(this.m_StartColor, this.m_EndColor, num);
				this.SetMeshColor(meshColor);
				if (num >= 1f)
				{
					this.m_IsUpdateMeshColor = false;
				}
			}
			if (num >= 1f && this.m_IsCancelOnFadeComplete)
			{
				this.m_IsUpdateMeshColor = false;
			}
		}

		// Token: 0x06000EA8 RID: 3752 RVA: 0x0004E9E9 File Offset: 0x0004CDE9
		public void ResetMeshColor()
		{
			this.m_FadeType = -1;
			this.m_IsUpdateMeshColor = false;
			this.m_IsCancelOnFadeComplete = false;
			this.SetMeshColor(Color.white);
		}

		// Token: 0x06000EA9 RID: 3753 RVA: 0x0004EA0C File Offset: 0x0004CE0C
		private void SetMeshColor(Color color)
		{
			int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
			for (int i = 0; i < msbObjectHandlerNum; i++)
			{
				MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
				MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
				if (work != null)
				{
					work.m_MeshColor = color;
				}
			}
		}

		// Token: 0x06000EAA RID: 3754 RVA: 0x0004EA58 File Offset: 0x0004CE58
		private void SetMeshColorA(float a)
		{
			this.m_CurrentAlpha = a;
			int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
			for (int i = 0; i < msbObjectHandlerNum; i++)
			{
				MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
				MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
				if (work != null)
				{
					work.m_MeshColor.a = this.m_CurrentAlpha;
				}
			}
		}

		// Token: 0x040015B2 RID: 5554
		private const string PLAY_KEY = "Take 001";

		// Token: 0x040015B3 RID: 5555
		public Animation m_Anim;

		// Token: 0x040015B4 RID: 5556
		public MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040015B5 RID: 5557
		public MeigeAnimClipHolder m_MeigeAnimClipHolder;

		// Token: 0x040015B6 RID: 5558
		public MsbHandler m_MsbHndl;

		// Token: 0x040015B7 RID: 5559
		public EffectControllParam_Visible m_ControllParam_Visible;

		// Token: 0x040015B8 RID: 5560
		public EffectControllParam_Color m_ControllParam_Color;

		// Token: 0x040015B9 RID: 5561
		private Color m_StartColor;

		// Token: 0x040015BA RID: 5562
		private Color m_EndColor;

		// Token: 0x040015BB RID: 5563
		private float m_MeshColorTime;

		// Token: 0x040015BC RID: 5564
		private float m_MeshColorTimer;

		// Token: 0x040015BD RID: 5565
		private float m_CurrentAlpha;

		// Token: 0x040015BE RID: 5566
		private bool m_IsUpdateMeshColor;

		// Token: 0x040015BF RID: 5567
		private bool m_IsCancelOnFadeComplete;

		// Token: 0x040015C0 RID: 5568
		private int m_FadeType;
	}
}
