﻿using System;

namespace Star
{
	// Token: 0x020002B5 RID: 693
	public class EvolutionResultBeforeAfterPairBeforePartCharacterWrapper : EvolutionResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CF8 RID: 3320 RVA: 0x0004922D File Offset: 0x0004762D
		public EvolutionResultBeforeAfterPairBeforePartCharacterWrapper(EditMain.EvolutionResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}

		// Token: 0x06000CF9 RID: 3321 RVA: 0x00049237 File Offset: 0x00047637
		public override int GetCharaId()
		{
			if (this.m_Result == null)
			{
				return base.GetCharaId();
			}
			return this.m_Result.m_SrcCharaID;
		}

		// Token: 0x06000CFA RID: 3322 RVA: 0x00049258 File Offset: 0x00047658
		public override int GetMaxLv()
		{
			return this.GetCharaParamLvOnly().Lv;
		}

		// Token: 0x06000CFB RID: 3323 RVA: 0x00049273 File Offset: 0x00047673
		public override int GetLimitBreak()
		{
			return 4;
		}

		// Token: 0x06000CFC RID: 3324 RVA: 0x00049276 File Offset: 0x00047676
		protected override EditUtility.OutputCharaParam ConvertResultDataToCharaParam(EditMain.EvolutionResultData result)
		{
			return EditUtility.CalcCharaParamIgnoreTownBuff(this.m_Result.m_SrcCharaID, this.data.Param.Lv, this.data.Param.Exp, base.GetUserNamedData(), null);
		}
	}
}
