﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200070B RID: 1803
	public class TownCharaManager
	{
		// Token: 0x060023C8 RID: 9160 RVA: 0x000C0756 File Offset: 0x000BEB56
		public TownCharaManager(TownBuilder pbuilder)
		{
			this.m_Table = new TownCharaManager.TownCharaInfo[10];
			this.m_Builder = pbuilder;
		}

		// Token: 0x060023C9 RID: 9161 RVA: 0x000C0774 File Offset: 0x000BEB74
		public void AddCharacter(long fid, bool frepos = true, bool fdebug = false)
		{
			for (int i = 0; i < this.m_Table.Length; i++)
			{
				if (this.m_Table[i] == null)
				{
					TownCharaManager.TownCharaInfo townCharaInfo = this.m_Table[i] = new TownCharaManager.TownCharaInfo();
					townCharaInfo.m_MngID = fid;
					townCharaInfo.m_Object = new GameObject("Chara_" + fid);
					townCharaInfo.m_Handle = (TownObjHandleChara)ITownObjectHandler.CreateHandler(townCharaInfo.m_Object, TownUtility.eResCategory.Chara, this.m_Builder);
					townCharaInfo.m_Handle.SetUpChara(fid);
					if (frepos)
					{
						townCharaInfo.m_Handle.BindBuildPoint(false);
					}
					break;
				}
			}
		}

		// Token: 0x060023CA RID: 9162 RVA: 0x000C081C File Offset: 0x000BEC1C
		public void Release()
		{
			for (int i = 0; i < this.m_Table.Length; i++)
			{
				if (this.m_Table[i] != null)
				{
					this.m_Table[i].m_Handle.Destroy();
					this.m_Table[i].m_Handle = null;
				}
			}
			this.m_Table = null;
		}

		// Token: 0x060023CB RID: 9163 RVA: 0x000C0878 File Offset: 0x000BEC78
		public void DeleteCharacter(long faccessid)
		{
			for (int i = 0; i < this.m_Table.Length; i++)
			{
				if (this.m_Table[i] != null && this.m_Table[i].m_MngID == faccessid)
				{
					this.m_Table[i].m_Handle.DestroyRequest();
					this.m_Table[i] = null;
					break;
				}
			}
		}

		// Token: 0x060023CC RID: 9164 RVA: 0x000C08DE File Offset: 0x000BECDE
		public void CallbackCharaState(eCharaBuildUp ftype, long fmanageid, int froomid)
		{
			if (ftype != eCharaBuildUp.Add)
			{
				if (ftype == eCharaBuildUp.Del)
				{
					this.DeleteCharacter(fmanageid);
				}
			}
			else
			{
				this.AddCharacter(fmanageid, false, false);
			}
		}

		// Token: 0x060023CD RID: 9165 RVA: 0x000C090C File Offset: 0x000BED0C
		public void RefreshBindPos()
		{
			for (int i = 0; i < this.m_Table.Length; i++)
			{
				if (this.m_Table[i] != null)
				{
					this.m_Table[i].m_Handle.BindBuildPoint(false);
				}
			}
		}

		// Token: 0x04002A98 RID: 10904
		public TownCharaManager.TownCharaInfo[] m_Table;

		// Token: 0x04002A99 RID: 10905
		public TownBuilder m_Builder;

		// Token: 0x0200070C RID: 1804
		public class TownCharaInfo
		{
			// Token: 0x04002A9A RID: 10906
			public TownObjHandleChara m_Handle;

			// Token: 0x04002A9B RID: 10907
			public GameObject m_Object;

			// Token: 0x04002A9C RID: 10908
			public long m_MngID;
		}
	}
}
