﻿using System;
using Star.UI.Room;

namespace Star
{
	// Token: 0x0200046A RID: 1130
	public class RoomComActionPopup : IMainComCommand
	{
		// Token: 0x060015F0 RID: 5616 RVA: 0x000725BC File Offset: 0x000709BC
		public override int GetHashCode()
		{
			return 3;
		}

		// Token: 0x04001CAC RID: 7340
		public long m_CharaManageID;

		// Token: 0x04001CAD RID: 7341
		public RoomGreet.eCategory m_CategoryID;
	}
}
