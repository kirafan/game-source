﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000208 RID: 520
	public class SkillExpDB : ScriptableObject
	{
		// Token: 0x04000CD5 RID: 3285
		public SkillExpDB_Param[] m_Params;
	}
}
