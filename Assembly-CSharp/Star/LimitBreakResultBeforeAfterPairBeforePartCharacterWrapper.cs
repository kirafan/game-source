﻿using System;

namespace Star
{
	// Token: 0x020002B2 RID: 690
	public class LimitBreakResultBeforeAfterPairBeforePartCharacterWrapper : LimitBreakResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CED RID: 3309 RVA: 0x00049167 File Offset: 0x00047567
		public LimitBreakResultBeforeAfterPairBeforePartCharacterWrapper(EditMain.LimitBreakResultData result) : base(eCharacterDataType.AfterLimitBreakParam, result)
		{
		}

		// Token: 0x06000CEE RID: 3310 RVA: 0x00049171 File Offset: 0x00047571
		public override int GetMaxLv()
		{
			if (this.m_Result == null)
			{
				return base.GetMaxLv();
			}
			return this.m_Result.m_BeforeMaxLv;
		}

		// Token: 0x06000CEF RID: 3311 RVA: 0x00049190 File Offset: 0x00047590
		public override int GetLimitBreak()
		{
			if (this.m_Result == null)
			{
				return base.GetLimitBreak();
			}
			return base.GetLimitBreak() - 1;
		}

		// Token: 0x06000CF0 RID: 3312 RVA: 0x000491AC File Offset: 0x000475AC
		public override int GetUniqueSkillMaxLv()
		{
			return this.m_Result.m_BeforeMaxUniqueSkillLv;
		}

		// Token: 0x06000CF1 RID: 3313 RVA: 0x000491B9 File Offset: 0x000475B9
		public override int[] GetClassSkillLevels()
		{
			return this.m_Result.m_BeforeMaxClassSkillLv;
		}
	}
}
