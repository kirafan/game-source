﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020006BA RID: 1722
	[RequireComponent(typeof(CanvasRenderer))]
	[RequireComponent(typeof(RectTransform))]
	public class TownPartsCanvasRender : Graphic
	{
		// Token: 0x0600224D RID: 8781 RVA: 0x000B65E8 File Offset: 0x000B49E8
		public void SetMesh(Vector3[] pvert, Vector2[] puv, int[] pindex, Color fcolor, Texture ptex)
		{
			this.m_TriNum = pindex.Length / 3;
			this.m_Index = new int[this.m_TriNum * 3];
			for (int i = 0; i < this.m_TriNum * 3; i++)
			{
				this.m_Index[i] = pindex[i];
			}
			this.color = fcolor;
			this.m_VertNum = pvert.Length;
			this.m_SetTex = ptex;
			this.m_Vertex = new Vector3[this.m_VertNum];
			this.m_UV = new Vector2[this.m_VertNum];
			for (int i = 0; i < this.m_VertNum; i++)
			{
				this.m_Vertex[i] = pvert[i];
				this.m_UV[i] = puv[i];
			}
		}

		// Token: 0x0600224E RID: 8782 RVA: 0x000B66C3 File Offset: 0x000B4AC3
		public void SetColor(Color fcol)
		{
			this.color = fcol;
		}

		// Token: 0x0600224F RID: 8783 RVA: 0x000B66CC File Offset: 0x000B4ACC
		protected override void OnPopulateMesh(VertexHelper vbo)
		{
			UIVertex simpleVert = UIVertex.simpleVert;
			vbo.Clear();
			for (int i = 0; i < this.m_VertNum; i++)
			{
				simpleVert.position = this.m_Vertex[i];
				simpleVert.color = this.color;
				simpleVert.uv0 = this.m_UV[i];
				vbo.AddVert(simpleVert);
			}
			for (int i = 0; i < this.m_TriNum; i++)
			{
				vbo.AddTriangle(this.m_Index[i * 3], this.m_Index[i * 3 + 1], this.m_Index[i * 3 + 2]);
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06002250 RID: 8784 RVA: 0x000B6784 File Offset: 0x000B4B84
		public override Texture mainTexture
		{
			get
			{
				return this.m_SetTex;
			}
		}

		// Token: 0x040028E7 RID: 10471
		public Vector3[] m_Vertex;

		// Token: 0x040028E8 RID: 10472
		public Vector2[] m_UV;

		// Token: 0x040028E9 RID: 10473
		public int[] m_Index;

		// Token: 0x040028EA RID: 10474
		public Texture m_SetTex;

		// Token: 0x040028EB RID: 10475
		public int m_VertNum;

		// Token: 0x040028EC RID: 10476
		public int m_TriNum;
	}
}
