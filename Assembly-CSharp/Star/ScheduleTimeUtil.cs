﻿using System;

namespace Star
{
	// Token: 0x02000375 RID: 885
	public static class ScheduleTimeUtil
	{
		// Token: 0x060010D3 RID: 4307 RVA: 0x00058F1C File Offset: 0x0005731C
		private static long GetUniversalTime()
		{
			return (SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.ToUniversalTime().Ticks - ScheduleTimeUtil.UnitBaseTime.Ticks) / 10000000L;
		}

		// Token: 0x060010D4 RID: 4308 RVA: 0x00058F58 File Offset: 0x00057358
		private static long GetLocalTimeBase()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Ticks - ScheduleTimeUtil.UnitBaseTime.Ticks;
		}

		// Token: 0x060010D5 RID: 4309 RVA: 0x00058F88 File Offset: 0x00057388
		private static long GetDebugTimeBase()
		{
			long ticks = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Ticks;
			if (!ScheduleTimeUtil.m_MainDebug)
			{
				ScheduleTimeUtil.m_DebugDeltaTime = ScheduleTimeUtil.GetLocalTimeBase();
				ScheduleTimeUtil.m_MainDebug = true;
			}
			long num = 0L;
			if (ScheduleTimeUtil.ms_BackTimeKey != 0L)
			{
				num = ticks - ScheduleTimeUtil.ms_BackTimeKey;
			}
			ScheduleTimeUtil.ms_BackTimeKey = ticks;
			ScheduleTimeUtil.m_DebugDeltaTime += (long)((float)num * ScheduleTimeUtil.m_DebugSpeed);
			return ScheduleTimeUtil.m_DebugDeltaTime;
		}

		// Token: 0x060010D6 RID: 4310 RVA: 0x00058FF8 File Offset: 0x000573F8
		public static long GetSystemTimeSec()
		{
			return ScheduleTimeUtil.m_SettingTimeBase / 10000000L;
		}

		// Token: 0x060010D7 RID: 4311 RVA: 0x00059006 File Offset: 0x00057406
		public static long GetSystemTimeMs()
		{
			return ScheduleTimeUtil.m_SettingTimeBase / 10000L;
		}

		// Token: 0x060010D8 RID: 4312 RVA: 0x00059014 File Offset: 0x00057414
		public static long GetSystemTimeBase()
		{
			if (ScheduleTimeUtil.m_SettingTimeBase == 0L)
			{
				ScheduleTimeUtil.m_SettingTimeBase = ScheduleTimeUtil.GetLocalTimeBase();
			}
			return ScheduleTimeUtil.m_SettingTimeBase;
		}

		// Token: 0x060010D9 RID: 4313 RVA: 0x00059034 File Offset: 0x00057434
		public static int GetTimeCalcMode()
		{
			int result = 0;
			if (ScheduleTimeUtil.m_DebugSetUp)
			{
				result = 1;
			}
			return result;
		}

		// Token: 0x060010DA RID: 4314 RVA: 0x00059050 File Offset: 0x00057450
		public static void SetMarkingTime()
		{
			long settingTimeBase;
			if (ScheduleTimeUtil.m_DebugSetUp)
			{
				settingTimeBase = ScheduleTimeUtil.GetDebugTimeBase();
			}
			else
			{
				settingTimeBase = ScheduleTimeUtil.GetLocalTimeBase();
			}
			ScheduleTimeUtil.m_SettingTimeBase = settingTimeBase;
		}

		// Token: 0x060010DB RID: 4315 RVA: 0x0005907E File Offset: 0x0005747E
		public static void SetDebugSpeed(float fspeed)
		{
			ScheduleTimeUtil.m_DebugSpeed = fspeed;
		}

		// Token: 0x060010DC RID: 4316 RVA: 0x00059088 File Offset: 0x00057488
		public static void SetDebugAddTime(int fday, int fhour, int fminute)
		{
			long num = (long)(fminute * 60);
			num += (long)(fhour * 60 * 60);
			num += (long)(fday * 24 * 60 * 60);
			ScheduleTimeUtil.m_DebugDeltaTime += num * 10000000L;
		}

		// Token: 0x060010DD RID: 4317 RVA: 0x000590C8 File Offset: 0x000574C8
		public static void ResetBaseTime(int fday, int fhour, int fminute)
		{
			DateTime serverTime = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime;
			DateTime dateTime = new DateTime(serverTime.Year, serverTime.Month, serverTime.Day, fhour, fminute, 0, DateTimeKind.Local);
			ScheduleTimeUtil.m_SettingTimeBase = (ScheduleTimeUtil.m_DebugDeltaTime = dateTime.Ticks - ScheduleTimeUtil.UnitBaseTime.Ticks);
			ScheduleTimeUtil.m_MainDebug = true;
		}

		// Token: 0x060010DE RID: 4318 RVA: 0x00059126 File Offset: 0x00057526
		public static int GetDayTimeMinute(long ftime)
		{
			return (int)(ftime % 86400L) / 60;
		}

		// Token: 0x060010DF RID: 4319 RVA: 0x00059134 File Offset: 0x00057534
		public static int GetDayTimeSec(long ftime)
		{
			return (int)(ftime % 86400L);
		}

		// Token: 0x060010E0 RID: 4320 RVA: 0x0005913F File Offset: 0x0005753F
		public static long GetDayKey(long ftime)
		{
			return ftime / 86400L;
		}

		// Token: 0x060010E1 RID: 4321 RVA: 0x00059149 File Offset: 0x00057549
		public static int ChangeUnitTimeMinute(int fday, int fhour, int fminute)
		{
			return fday * 60 * 24 + fhour * 60 + fminute;
		}

		// Token: 0x060010E2 RID: 4322 RVA: 0x00059159 File Offset: 0x00057559
		public static int ChangeUnitTimeSec(int fday, int fhour, int fminute)
		{
			return fday * 3600 * 24 + fhour * 3600 + fminute * 60;
		}

		// Token: 0x060010E3 RID: 4323 RVA: 0x00059172 File Offset: 0x00057572
		public static long ChangeUnitTimeSecToBase(long fsec)
		{
			return fsec * 1000L * 1000L * 10L;
		}

		// Token: 0x060010E4 RID: 4324 RVA: 0x00059188 File Offset: 0x00057588
		public static DateTime ChangeBaseTimeToDateTime(long fbasetime)
		{
			return DateTime.FromBinary(fbasetime + ScheduleTimeUtil.UnitBaseTime.Ticks);
		}

		// Token: 0x060010E5 RID: 4325 RVA: 0x000591AC File Offset: 0x000575AC
		public static long DebugUnitTimeSec(int fmonth, int fday, int fhour, int fminute, int fsec)
		{
			DateTime now = DateTime.Now;
			if (fmonth == 0)
			{
				fmonth = now.Month;
			}
			if (fday == 0)
			{
				fday = now.Day;
			}
			DateTime dateTime = new DateTime(now.Year, now.Month, now.Day, fhour, fminute, fsec, DateTimeKind.Local);
			return (dateTime.Ticks - ScheduleTimeUtil.UnitBaseTime.Ticks) / 10000000L;
		}

		// Token: 0x060010E6 RID: 4326 RVA: 0x0005921C File Offset: 0x0005761C
		public static DateTime GetManageUnixTime()
		{
			return DateTime.FromBinary(ScheduleTimeUtil.m_SettingTimeBase + ScheduleTimeUtil.UnitBaseTime.Ticks);
		}

		// Token: 0x060010E7 RID: 4327 RVA: 0x00059244 File Offset: 0x00057644
		public static DateTime GetManageUniversalTime()
		{
			return DateTime.FromBinary(ScheduleTimeUtil.m_SettingTimeBase + ScheduleTimeUtil.UnitBaseTime.Ticks);
		}

		// Token: 0x060010E8 RID: 4328 RVA: 0x0005926C File Offset: 0x0005766C
		public static DateTime GetMsTimeToDateTime(long ftimes)
		{
			return DateTime.FromBinary(ftimes * 1000L * 10L + ScheduleTimeUtil.UnitBaseTime.Ticks);
		}

		// Token: 0x060010E9 RID: 4329 RVA: 0x0005929A File Offset: 0x0005769A
		public static void SetDebugMode(bool fena)
		{
			ScheduleTimeUtil.m_DebugSetUp = fena;
		}

		// Token: 0x040017C0 RID: 6080
		public static readonly DateTime UnitBaseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		// Token: 0x040017C1 RID: 6081
		public const long BASE_TIME_SEC = 10000000L;

		// Token: 0x040017C2 RID: 6082
		public const long BASE_TIME_MS = 10000L;

		// Token: 0x040017C3 RID: 6083
		public const int TIME_HOUR = 3600;

		// Token: 0x040017C4 RID: 6084
		public static long m_DebugDeltaTime;

		// Token: 0x040017C5 RID: 6085
		public static long ms_BackTimeKey;

		// Token: 0x040017C6 RID: 6086
		public static long m_SettingTimeBase;

		// Token: 0x040017C7 RID: 6087
		public static bool m_MainDebug = false;

		// Token: 0x040017C8 RID: 6088
		public static bool m_DebugSetUp;

		// Token: 0x040017C9 RID: 6089
		public static float m_DebugSpeed = 1f;
	}
}
