﻿using System;

namespace Star
{
	// Token: 0x020001DF RID: 479
	public enum eStateIconType
	{
		// Token: 0x04000B6D RID: 2925
		None,
		// Token: 0x04000B6E RID: 2926
		Confusion,
		// Token: 0x04000B6F RID: 2927
		Paralysis,
		// Token: 0x04000B70 RID: 2928
		Poison,
		// Token: 0x04000B71 RID: 2929
		Bearish,
		// Token: 0x04000B72 RID: 2930
		Sleep,
		// Token: 0x04000B73 RID: 2931
		Unhappy,
		// Token: 0x04000B74 RID: 2932
		Silence,
		// Token: 0x04000B75 RID: 2933
		Isolation,
		// Token: 0x04000B76 RID: 2934
		Regene,
		// Token: 0x04000B77 RID: 2935
		StatusAtkUp,
		// Token: 0x04000B78 RID: 2936
		StatusMgcUp,
		// Token: 0x04000B79 RID: 2937
		StatusDefUp,
		// Token: 0x04000B7A RID: 2938
		StatusMDefUp,
		// Token: 0x04000B7B RID: 2939
		StatusSpdUp,
		// Token: 0x04000B7C RID: 2940
		StatusLuckUp,
		// Token: 0x04000B7D RID: 2941
		StatusAtkDown,
		// Token: 0x04000B7E RID: 2942
		StatusMgcDown,
		// Token: 0x04000B7F RID: 2943
		StatusDefDown,
		// Token: 0x04000B80 RID: 2944
		StatusMDefDown,
		// Token: 0x04000B81 RID: 2945
		StatusSpdDown,
		// Token: 0x04000B82 RID: 2946
		StatusLuckDown,
		// Token: 0x04000B83 RID: 2947
		ElementFireUp,
		// Token: 0x04000B84 RID: 2948
		ElementWindUp,
		// Token: 0x04000B85 RID: 2949
		ElementEarthUp,
		// Token: 0x04000B86 RID: 2950
		ElementWaterUp,
		// Token: 0x04000B87 RID: 2951
		ElementMoonUp,
		// Token: 0x04000B88 RID: 2952
		ElementSunUp,
		// Token: 0x04000B89 RID: 2953
		ElementFireDown,
		// Token: 0x04000B8A RID: 2954
		ElementWindDown,
		// Token: 0x04000B8B RID: 2955
		ElementEarthDown,
		// Token: 0x04000B8C RID: 2956
		ElementWaterDown,
		// Token: 0x04000B8D RID: 2957
		ElementMoonDown,
		// Token: 0x04000B8E RID: 2958
		ElementSunDown,
		// Token: 0x04000B8F RID: 2959
		StateAbnormalDisable,
		// Token: 0x04000B90 RID: 2960
		StateAbnormalAdditionalProbabilityUp,
		// Token: 0x04000B91 RID: 2961
		StateAbnormalAdditionalProbabilityDown,
		// Token: 0x04000B92 RID: 2962
		WeakElementBonus,
		// Token: 0x04000B93 RID: 2963
		NextAtkUp,
		// Token: 0x04000B94 RID: 2964
		NextMgcUp,
		// Token: 0x04000B95 RID: 2965
		NextCritical,
		// Token: 0x04000B96 RID: 2966
		Barrier,
		// Token: 0x04000B97 RID: 2967
		ChainBonusUp,
		// Token: 0x04000B98 RID: 2968
		HateUp,
		// Token: 0x04000B99 RID: 2969
		HateDown,
		// Token: 0x04000B9A RID: 2970
		Num
	}
}
