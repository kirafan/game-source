﻿using System;

namespace Star
{
	// Token: 0x02000421 RID: 1057
	public class GachaPlayMain : GameStateMain
	{
		// Token: 0x17000164 RID: 356
		// (get) Token: 0x06001442 RID: 5186 RVA: 0x0006BE6C File Offset: 0x0006A26C
		// (set) Token: 0x06001443 RID: 5187 RVA: 0x0006BE74 File Offset: 0x0006A274
		public GachaPlayScene GachaPlayScene
		{
			get
			{
				return this.m_GachaPlayScene;
			}
			set
			{
				this.m_GachaPlayScene = value;
			}
		}

		// Token: 0x06001444 RID: 5188 RVA: 0x0006BE7D File Offset: 0x0006A27D
		private void Start()
		{
			base.SetNextState(1);
		}

		// Token: 0x06001445 RID: 5189 RVA: 0x0006BE86 File Offset: 0x0006A286
		private void OnDestroy()
		{
			this.m_GachaPlayScene = null;
		}

		// Token: 0x06001446 RID: 5190 RVA: 0x0006BE8F File Offset: 0x0006A28F
		protected override void Update()
		{
			if (this.m_GachaPlayScene != null)
			{
				this.m_GachaPlayScene.Update();
			}
			base.Update();
		}

		// Token: 0x06001447 RID: 5191 RVA: 0x0006BEB0 File Offset: 0x0006A2B0
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 1)
			{
				return new GachaPlayState_Main(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x06001448 RID: 5192 RVA: 0x0006BEEA File Offset: 0x0006A2EA
		public override void Destroy()
		{
			if (this.m_GachaPlayScene != null)
			{
				this.m_GachaPlayScene.Destory();
			}
		}

		// Token: 0x06001449 RID: 5193 RVA: 0x0006BF03 File Offset: 0x0006A303
		public override bool IsCompleteDestroy()
		{
			return this.m_GachaPlayScene == null || this.m_GachaPlayScene.IsCompleteDestory();
		}

		// Token: 0x04001B32 RID: 6962
		public const int STATE_MAIN = 1;

		// Token: 0x04001B33 RID: 6963
		private GachaPlayScene m_GachaPlayScene;
	}
}
