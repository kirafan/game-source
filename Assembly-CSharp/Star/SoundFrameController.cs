﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Star
{
	// Token: 0x02000630 RID: 1584
	public class SoundFrameController
	{
		// Token: 0x06001EF8 RID: 7928 RVA: 0x000A868E File Offset: 0x000A6A8E
		public SoundFrameController(SoundManager soundManager, string cueSheetName, string acbFile, string awbFile)
		{
			this.m_SoundManager = soundManager;
			this.m_CueSheetName = cueSheetName;
			this.m_AcbFile = acbFile;
			this.m_AwbFile = awbFile;
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06001EF9 RID: 7929 RVA: 0x000A86C5 File Offset: 0x000A6AC5
		public string CueSheetName
		{
			get
			{
				return this.m_CueSheetName;
			}
		}

		// Token: 0x06001EFA RID: 7930 RVA: 0x000A86D0 File Offset: 0x000A6AD0
		public void Destroy(bool stopIfStatusIsNotRemoved = true, bool isUnloadCueSheet = true)
		{
			this.m_Status = SoundFrameController.eStatus.None;
			this.m_IsPreparing = false;
			for (int i = 0; i < this.m_RequestParams.Count; i++)
			{
				if (this.m_RequestParams[i].m_Hndl != null)
				{
					this.m_SoundManager.RemoveHndl(this.m_RequestParams[i].m_Hndl, stopIfStatusIsNotRemoved);
					this.m_RequestParams[i].m_Hndl = null;
				}
			}
			this.m_RequestParams.Clear();
			if (isUnloadCueSheet)
			{
				this.m_SoundManager.UnloadCueSheet(this.m_CueSheetName);
			}
		}

		// Token: 0x06001EFB RID: 7931 RVA: 0x000A8770 File Offset: 0x000A6B70
		public void Update(int currentFrame)
		{
			if (this.m_SoundManager == null)
			{
				return;
			}
			this.UpdatePreparingCues();
			switch (this.m_Status)
			{
			case SoundFrameController.eStatus.Preparing_CueSheet:
			{
				SoundDefine.eLoadStatus cueSheetLoadStatus = this.m_SoundManager.GetCueSheetLoadStatus(this.m_CueSheetName);
				if (cueSheetLoadStatus != SoundDefine.eLoadStatus.Loading)
				{
					int num = this.FirstPrepareCues();
					if (num > 0)
					{
						this.m_Status = SoundFrameController.eStatus.Preparing_FirstStream;
					}
					else
					{
						this.m_Status = SoundFrameController.eStatus.Stop;
						this.m_IsPreparing = false;
					}
				}
				break;
			}
			case SoundFrameController.eStatus.Preparing_FirstStream:
			{
				bool flag = true;
				for (int i = 0; i < this.m_RequestParams.Count; i++)
				{
					if (this.m_RequestParams[i].m_Status == SoundFrameController.eRequestParamStatus.Preparing)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.m_Status = SoundFrameController.eStatus.Stop;
					this.m_IsPreparing = false;
				}
				break;
			}
			case SoundFrameController.eStatus.Play:
			{
				this.PrepareCues(currentFrame);
				int j = 0;
				while (j < this.m_RequestParams.Count)
				{
					SoundFrameController.RequestParam requestParam = this.m_RequestParams[j];
					if (requestParam.m_Status != SoundFrameController.eRequestParamStatus.Played)
					{
						goto IL_16D;
					}
					if (requestParam.m_LoopReset)
					{
						if (!requestParam.m_LoopResetCheck)
						{
							if (currentFrame > requestParam.m_PlayFrame)
							{
								requestParam.m_LoopResetCheck = true;
								goto IL_231;
							}
						}
						else
						{
							if (currentFrame > requestParam.m_PlayFrame)
							{
								goto IL_231;
							}
							requestParam.m_LoopResetCheck = false;
							requestParam.m_Status = SoundFrameController.eRequestParamStatus.CompletePrepare;
						}
						goto IL_16D;
					}
					IL_231:
					j++;
					continue;
					IL_16D:
					if (requestParam.m_Status != SoundFrameController.eRequestParamStatus.Played && currentFrame >= requestParam.m_PlayFrame)
					{
						if (requestParam.m_Hndl != null)
						{
							requestParam.m_Hndl.Resume();
						}
						else if (requestParam.m_IsVoice)
						{
							this.m_SoundManager.PlayVOICE(this.m_CueSheetName, requestParam.m_CueName, 1f, 0, -1, -1);
						}
						else if (!string.IsNullOrEmpty(requestParam.m_CueName))
						{
							this.m_SoundManager.PlaySE(this.m_CueSheetName, requestParam.m_CueName, 1f, 0, -1, -1);
						}
						else
						{
							this.m_SoundManager.PlaySE(requestParam.m_CueID, 1f, 0, -1, -1);
						}
						requestParam.m_Status = SoundFrameController.eRequestParamStatus.Played;
						goto IL_231;
					}
					goto IL_231;
				}
				break;
			}
			}
		}

		// Token: 0x06001EFC RID: 7932 RVA: 0x000A89CC File Offset: 0x000A6DCC
		public void AddRequestParam(int frame, string cueName, bool isVoice, bool loopReset)
		{
			SoundFrameController.RequestParam item = new SoundFrameController.RequestParam(frame, cueName, isVoice, loopReset);
			this.m_RequestParams.Add(item);
		}

		// Token: 0x06001EFD RID: 7933 RVA: 0x000A89F0 File Offset: 0x000A6DF0
		public void AddRequestParam(int frame, eSoundSeListDB cueID, bool loopReset)
		{
			SoundFrameController.RequestParam item = new SoundFrameController.RequestParam(frame, cueID, loopReset);
			this.m_RequestParams.Add(item);
		}

		// Token: 0x06001EFE RID: 7934 RVA: 0x000A8A14 File Offset: 0x000A6E14
		public void Prepare(int prepareStartFrame = 15)
		{
			this.m_PrepareStartFrame = prepareStartFrame;
			List<SoundFrameController.RequestParam> requestParams = this.m_RequestParams;
			if (SoundFrameController.<>f__mg$cache0 == null)
			{
				SoundFrameController.<>f__mg$cache0 = new Comparison<SoundFrameController.RequestParam>(SoundFrameController.CompareRequestParam);
			}
			requestParams.Sort(SoundFrameController.<>f__mg$cache0);
			bool flag = this.m_SoundManager.LoadCueSheet(this.m_CueSheetName, this.m_AcbFile, this.m_AwbFile);
			if (flag)
			{
				this.m_Status = SoundFrameController.eStatus.Preparing_CueSheet;
				this.m_IsPreparing = true;
			}
			else
			{
				this.m_Status = SoundFrameController.eStatus.Error;
				this.m_IsPreparing = false;
			}
		}

		// Token: 0x06001EFF RID: 7935 RVA: 0x000A8A95 File Offset: 0x000A6E95
		public void PrepareQuick(int prepareStartFrame = 15)
		{
			this.m_PrepareStartFrame = prepareStartFrame;
			List<SoundFrameController.RequestParam> requestParams = this.m_RequestParams;
			if (SoundFrameController.<>f__mg$cache1 == null)
			{
				SoundFrameController.<>f__mg$cache1 = new Comparison<SoundFrameController.RequestParam>(SoundFrameController.CompareRequestParam);
			}
			requestParams.Sort(SoundFrameController.<>f__mg$cache1);
			this.m_Status = SoundFrameController.eStatus.Stop;
			this.m_IsPreparing = false;
		}

		// Token: 0x06001F00 RID: 7936 RVA: 0x000A8AD4 File Offset: 0x000A6ED4
		public bool IsCompletePrepare()
		{
			return !this.m_IsPreparing;
		}

		// Token: 0x06001F01 RID: 7937 RVA: 0x000A8ADF File Offset: 0x000A6EDF
		public bool Play()
		{
			if (!this.IsCompletePrepare())
			{
				return false;
			}
			this.m_Status = SoundFrameController.eStatus.Play;
			return true;
		}

		// Token: 0x06001F02 RID: 7938 RVA: 0x000A8AF6 File Offset: 0x000A6EF6
		private static int CompareRequestParam(SoundFrameController.RequestParam A, SoundFrameController.RequestParam B)
		{
			if (A.m_PlayFrame > B.m_PlayFrame)
			{
				return 1;
			}
			if (A.m_PlayFrame < B.m_PlayFrame)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06001F03 RID: 7939 RVA: 0x000A8B1F File Offset: 0x000A6F1F
		private int FirstPrepareCues()
		{
			return this.PrepareCues(0);
		}

		// Token: 0x06001F04 RID: 7940 RVA: 0x000A8B28 File Offset: 0x000A6F28
		private int PrepareCues(int currentFrame)
		{
			int num = 0;
			for (int i = 0; i < this.m_RequestParams.Count; i++)
			{
				if (this.m_RequestParams[i].m_IsVoice)
				{
					if (this.m_RequestParams[i].m_Status == SoundFrameController.eRequestParamStatus.None)
					{
						if (currentFrame >= this.m_RequestParams[i].m_PlayFrame - this.m_PrepareStartFrame)
						{
							bool flag = this.PrepareCue(this.m_RequestParams[i]);
							if (flag)
							{
								num++;
							}
						}
					}
				}
			}
			return num;
		}

		// Token: 0x06001F05 RID: 7941 RVA: 0x000A8BC8 File Offset: 0x000A6FC8
		private bool PrepareCue(SoundFrameController.RequestParam reqParam)
		{
			if (!reqParam.m_IsVoice)
			{
				return false;
			}
			reqParam.m_Hndl = new SoundHandler();
			bool flag = this.m_SoundManager.PrepareVOICE(this.m_CueSheetName, reqParam.m_CueName, reqParam.m_Hndl, 1f, 0, -1, -1);
			if (flag)
			{
				reqParam.m_Status = SoundFrameController.eRequestParamStatus.Preparing;
				return true;
			}
			reqParam.m_Status = SoundFrameController.eRequestParamStatus.None;
			return false;
		}

		// Token: 0x06001F06 RID: 7942 RVA: 0x000A8C2C File Offset: 0x000A702C
		private void UpdatePreparingCues()
		{
			for (int i = 0; i < this.m_RequestParams.Count; i++)
			{
				SoundFrameController.eRequestParamStatus status = this.m_RequestParams[i].m_Status;
				if (status == SoundFrameController.eRequestParamStatus.Preparing)
				{
					if (this.m_RequestParams[i].m_Hndl != null && this.m_RequestParams[i].m_Hndl.IsCompletePrepare())
					{
						this.m_RequestParams[i].m_Status = SoundFrameController.eRequestParamStatus.CompletePrepare;
					}
				}
			}
		}

		// Token: 0x040025C4 RID: 9668
		private const int DEFAULT_PREPARE_START_FRAME = 15;

		// Token: 0x040025C5 RID: 9669
		private int m_PrepareStartFrame;

		// Token: 0x040025C6 RID: 9670
		private SoundManager m_SoundManager;

		// Token: 0x040025C7 RID: 9671
		private string m_CueSheetName;

		// Token: 0x040025C8 RID: 9672
		private string m_AcbFile;

		// Token: 0x040025C9 RID: 9673
		private string m_AwbFile;

		// Token: 0x040025CA RID: 9674
		private List<SoundFrameController.RequestParam> m_RequestParams = new List<SoundFrameController.RequestParam>();

		// Token: 0x040025CB RID: 9675
		private SoundFrameController.eStatus m_Status = SoundFrameController.eStatus.None;

		// Token: 0x040025CC RID: 9676
		private bool m_IsPreparing;

		// Token: 0x040025CD RID: 9677
		[CompilerGenerated]
		private static Comparison<SoundFrameController.RequestParam> <>f__mg$cache0;

		// Token: 0x040025CE RID: 9678
		[CompilerGenerated]
		private static Comparison<SoundFrameController.RequestParam> <>f__mg$cache1;

		// Token: 0x02000631 RID: 1585
		private enum eRequestParamStatus
		{
			// Token: 0x040025D0 RID: 9680
			None,
			// Token: 0x040025D1 RID: 9681
			Preparing,
			// Token: 0x040025D2 RID: 9682
			CompletePrepare,
			// Token: 0x040025D3 RID: 9683
			Played
		}

		// Token: 0x02000632 RID: 1586
		private class RequestParam
		{
			// Token: 0x06001F07 RID: 7943 RVA: 0x000A8CBB File Offset: 0x000A70BB
			public RequestParam(int frame, string cueName, bool isVoice, bool loopReset)
			{
				this.m_PlayFrame = frame;
				this.m_CueName = cueName;
				this.m_IsVoice = isVoice;
				this.m_LoopReset = loopReset;
			}

			// Token: 0x06001F08 RID: 7944 RVA: 0x000A8CEB File Offset: 0x000A70EB
			public RequestParam(int frame, eSoundSeListDB cueID, bool loopReset)
			{
				this.m_PlayFrame = frame;
				this.m_CueID = cueID;
				this.m_IsVoice = false;
				this.m_LoopReset = loopReset;
			}

			// Token: 0x040025D4 RID: 9684
			public int m_PlayFrame;

			// Token: 0x040025D5 RID: 9685
			public string m_CueName;

			// Token: 0x040025D6 RID: 9686
			public eSoundSeListDB m_CueID = eSoundSeListDB.Max;

			// Token: 0x040025D7 RID: 9687
			public SoundFrameController.eRequestParamStatus m_Status;

			// Token: 0x040025D8 RID: 9688
			public bool m_LoopReset;

			// Token: 0x040025D9 RID: 9689
			public bool m_LoopResetCheck;

			// Token: 0x040025DA RID: 9690
			public bool m_IsVoice;

			// Token: 0x040025DB RID: 9691
			public SoundHandler m_Hndl;
		}

		// Token: 0x02000633 RID: 1587
		private enum eStatus
		{
			// Token: 0x040025DD RID: 9693
			None = -1,
			// Token: 0x040025DE RID: 9694
			Preparing_CueSheet,
			// Token: 0x040025DF RID: 9695
			Preparing_FirstStream,
			// Token: 0x040025E0 RID: 9696
			Stop,
			// Token: 0x040025E1 RID: 9697
			Play,
			// Token: 0x040025E2 RID: 9698
			Error
		}
	}
}
