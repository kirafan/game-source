﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000B55 RID: 2901
	public class UserSupportPartyData
	{
		// Token: 0x06003D87 RID: 15751 RVA: 0x00136F95 File Offset: 0x00135395
		public UserSupportPartyData()
		{
			this.MngID = -1L;
			this.CharaMngIDs = new List<long>();
			this.WeaponMngIDs = new List<long>();
		}

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06003D88 RID: 15752 RVA: 0x00136FBB File Offset: 0x001353BB
		// (set) Token: 0x06003D89 RID: 15753 RVA: 0x00136FC3 File Offset: 0x001353C3
		public long MngID { get; set; }

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06003D8A RID: 15754 RVA: 0x00136FCC File Offset: 0x001353CC
		// (set) Token: 0x06003D8B RID: 15755 RVA: 0x00136FD4 File Offset: 0x001353D4
		public string PartyName { get; set; }

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06003D8C RID: 15756 RVA: 0x00136FDD File Offset: 0x001353DD
		// (set) Token: 0x06003D8D RID: 15757 RVA: 0x00136FE5 File Offset: 0x001353E5
		public List<long> CharaMngIDs { get; set; }

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06003D8E RID: 15758 RVA: 0x00136FEE File Offset: 0x001353EE
		// (set) Token: 0x06003D8F RID: 15759 RVA: 0x00136FF6 File Offset: 0x001353F6
		public List<long> WeaponMngIDs { get; set; }

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06003D90 RID: 15760 RVA: 0x00136FFF File Offset: 0x001353FF
		// (set) Token: 0x06003D91 RID: 15761 RVA: 0x00137007 File Offset: 0x00135407
		public bool IsActive { get; set; }

		// Token: 0x06003D92 RID: 15762 RVA: 0x00137010 File Offset: 0x00135410
		public int GetSlotNum()
		{
			return this.CharaMngIDs.Count;
		}

		// Token: 0x06003D93 RID: 15763 RVA: 0x0013701D File Offset: 0x0013541D
		public long GetMemberAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Count)
			{
				return this.CharaMngIDs[slotIndex];
			}
			return -1L;
		}

		// Token: 0x06003D94 RID: 15764 RVA: 0x00137046 File Offset: 0x00135446
		public void SetMemberAt(int slotIndex, long charaMngID)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Count)
			{
				this.CharaMngIDs[slotIndex] = charaMngID;
			}
		}

		// Token: 0x06003D95 RID: 15765 RVA: 0x0013706D File Offset: 0x0013546D
		public void SetMembers(long[] charaMngIDs)
		{
			this.CharaMngIDs = new List<long>(charaMngIDs);
		}

		// Token: 0x06003D96 RID: 15766 RVA: 0x0013707B File Offset: 0x0013547B
		public void EmptyMemberAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Count)
			{
				this.CharaMngIDs[slotIndex] = -1L;
			}
		}

		// Token: 0x06003D97 RID: 15767 RVA: 0x001370A4 File Offset: 0x001354A4
		public void EmptyMember(long charaMngID)
		{
			for (int i = 0; i < this.CharaMngIDs.Count; i++)
			{
				if (this.CharaMngIDs[i] == charaMngID)
				{
					this.CharaMngIDs[i] = -1L;
					break;
				}
			}
		}

		// Token: 0x06003D98 RID: 15768 RVA: 0x001370F2 File Offset: 0x001354F2
		public long GetWeaponAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Count)
			{
				return this.WeaponMngIDs[slotIndex];
			}
			return -1L;
		}

		// Token: 0x06003D99 RID: 15769 RVA: 0x0013711B File Offset: 0x0013551B
		public void SetWeaponAt(int slotIndex, long weaponMngID)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Count)
			{
				this.WeaponMngIDs[slotIndex] = weaponMngID;
			}
		}

		// Token: 0x06003D9A RID: 15770 RVA: 0x00137142 File Offset: 0x00135542
		public void SetWeapons(long[] weaponMngIDs)
		{
			this.WeaponMngIDs = new List<long>(weaponMngIDs);
		}

		// Token: 0x06003D9B RID: 15771 RVA: 0x00137150 File Offset: 0x00135550
		public void EmptyWeaponAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Count)
			{
				this.WeaponMngIDs[slotIndex] = -1L;
			}
		}

		// Token: 0x06003D9C RID: 15772 RVA: 0x00137178 File Offset: 0x00135578
		public void EmptyWeapon(long weaponMngID)
		{
			for (int i = 0; i < this.WeaponMngIDs.Count; i++)
			{
				if (this.WeaponMngIDs[i] == weaponMngID)
				{
					this.WeaponMngIDs[i] = -1L;
					break;
				}
			}
		}
	}
}
