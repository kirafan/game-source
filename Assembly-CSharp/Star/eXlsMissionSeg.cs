﻿using System;

namespace Star
{
	// Token: 0x020004ED RID: 1261
	public enum eXlsMissionSeg
	{
		// Token: 0x04001F9E RID: 8094
		System,
		// Token: 0x04001F9F RID: 8095
		Battle,
		// Token: 0x04001FA0 RID: 8096
		Town,
		// Token: 0x04001FA1 RID: 8097
		Room,
		// Token: 0x04001FA2 RID: 8098
		Edit,
		// Token: 0x04001FA3 RID: 8099
		Chara
	}
}
