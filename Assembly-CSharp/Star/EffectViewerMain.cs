﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200027E RID: 638
	public class EffectViewerMain : MonoBehaviour
	{
		// Token: 0x06000BE3 RID: 3043 RVA: 0x00045C2D File Offset: 0x0004402D
		private void Start()
		{
			this.m_ProfileStatus = new ProfileStatus();
			this.m_Step = EffectViewerMain.eStep.First;
		}

		// Token: 0x06000BE4 RID: 3044 RVA: 0x00045C44 File Offset: 0x00044044
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			if (Input.GetKeyDown(KeyCode.P))
			{
				this.m_IsDisplayProfileStatus = !this.m_IsDisplayProfileStatus;
			}
			switch (this.m_Step)
			{
			case EffectViewerMain.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_PackNames = new Dictionary<string, bool>();
					EffectListDB effectListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EffectListDB;
					for (int i = 0; i < effectListDB.m_Params.Length; i++)
					{
						this.m_PackNames.AddOrReplace(effectListDB.m_Params[i].m_PackName, true);
					}
					foreach (string packName in this.m_PackNames.Keys)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadPack(packName);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Battle);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.ADV);
					this.ApplyEffectID(0);
					this.m_Step = EffectViewerMain.eStep.First_Wait;
				}
				break;
			case EffectViewerMain.eStep.First_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
				{
					this.m_Step = EffectViewerMain.eStep.Main;
				}
				break;
			case EffectViewerMain.eStep.Prepare:
			{
				EffectManager effectMng = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
				if (string.IsNullOrEmpty(effectMng.CheckPackEffect(this.m_EffectID)))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadSingleEffect(this.m_EffectID, false);
				}
				this.m_Step = EffectViewerMain.eStep.Prepare_Wait;
				break;
			}
			case EffectViewerMain.eStep.Prepare_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
				{
					this.m_EffectHndl = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged(this.m_EffectID, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), null, false, WrapMode.Loop, 100, 1f, true);
					this.m_Step = EffectViewerMain.eStep.Main;
				}
				break;
			case EffectViewerMain.eStep.Destroy_0:
			{
				string effectID = (!(this.m_EffectHndl != null)) ? string.Empty : this.m_EffectHndl.EffectID;
				EffectManager effectMng2 = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
				effectMng2.DestroyToCache(this.m_EffectHndl);
				this.m_EffectHndl = null;
				if (string.IsNullOrEmpty(effectMng2.CheckPackEffect(effectID)))
				{
					effectMng2.UnloadSingleEffect(effectID);
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
				}
				this.m_Step = EffectViewerMain.eStep.Prepare;
				break;
			}
			}
			this.m_ProfileStatus.Profile();
		}

		// Token: 0x06000BE5 RID: 3045 RVA: 0x00045F0C File Offset: 0x0004430C
		private void ToggleEffectIndex(bool isRight)
		{
			EffectListDB effectListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EffectListDB;
			int num = -1;
			for (int i = 0; i < effectListDB.m_Params.Length; i++)
			{
				if (effectListDB.m_Params[i].m_EffectID == this.m_EffectID)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				return;
			}
			if (isRight)
			{
				num++;
				if (num >= effectListDB.m_Params.Length)
				{
					num = 0;
				}
			}
			else
			{
				num--;
				if (num < 0)
				{
					num = effectListDB.m_Params.Length - 1;
				}
			}
			this.ApplyEffectID(num);
		}

		// Token: 0x06000BE6 RID: 3046 RVA: 0x00045FB0 File Offset: 0x000443B0
		private void ApplyEffectID(int paramIndex)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_EffectID = dbMng.EffectListDB.m_Params[paramIndex].m_EffectID;
			this.m_EffectIDText.text = this.m_EffectID.ToString();
		}

		// Token: 0x06000BE7 RID: 3047 RVA: 0x00045FFC File Offset: 0x000443FC
		public void OnDecideApplyButton()
		{
			if (this.m_Step != EffectViewerMain.eStep.Main)
			{
				return;
			}
			bool flag = false;
			EffectListDB effectListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.EffectListDB;
			for (int i = 0; i < effectListDB.m_Params.Length; i++)
			{
				if (effectListDB.m_Params[i].m_EffectID == this.m_EffectID)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				Debug.LogErrorFormat("Error!!! >>> EffectID:{0} is not exist.", new object[]
				{
					this.m_EffectID
				});
				return;
			}
			this.m_Step = EffectViewerMain.eStep.Destroy_0;
		}

		// Token: 0x06000BE8 RID: 3048 RVA: 0x00046090 File Offset: 0x00044490
		public void OnValueChangedEffect()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_EffectID = this.m_EffectIDText.text;
			bool flag = false;
			EffectListDB effectListDB = dbMng.EffectListDB;
			for (int i = 0; i < effectListDB.m_Params.Length; i++)
			{
				if (effectListDB.m_Params[i].m_EffectID == this.m_EffectID)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
			}
		}

		// Token: 0x06000BE9 RID: 3049 RVA: 0x00046109 File Offset: 0x00044509
		public void OnDecideEffect_L()
		{
			if (this.m_Step != EffectViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleEffectIndex(false);
		}

		// Token: 0x06000BEA RID: 3050 RVA: 0x0004611F File Offset: 0x0004451F
		public void OnDecideEffect_R()
		{
			if (this.m_Step != EffectViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleEffectIndex(true);
		}

		// Token: 0x06000BEB RID: 3051 RVA: 0x00046135 File Offset: 0x00044535
		public void OnToggleBG(bool value)
		{
			this.m_BG.SetActive(this.m_ToggleBG.isOn);
		}

		// Token: 0x06000BEC RID: 3052 RVA: 0x0004614D File Offset: 0x0004454D
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * value;
		}

		// Token: 0x04001473 RID: 5235
		private Dictionary<string, bool> m_PackNames;

		// Token: 0x04001474 RID: 5236
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x04001475 RID: 5237
		[SerializeField]
		private InputField m_EffectIDText;

		// Token: 0x04001476 RID: 5238
		[SerializeField]
		private Toggle m_ToggleBG;

		// Token: 0x04001477 RID: 5239
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04001478 RID: 5240
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x04001479 RID: 5241
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x0400147A RID: 5242
		private EffectHandler m_EffectHndl;

		// Token: 0x0400147B RID: 5243
		private string m_EffectID;

		// Token: 0x0400147C RID: 5244
		public bool m_IsDisplayProfileStatus;

		// Token: 0x0400147D RID: 5245
		private ProfileStatus m_ProfileStatus;

		// Token: 0x0400147E RID: 5246
		private EffectViewerMain.eStep m_Step = EffectViewerMain.eStep.None;

		// Token: 0x0200027F RID: 639
		private enum eStep
		{
			// Token: 0x04001480 RID: 5248
			None = -1,
			// Token: 0x04001481 RID: 5249
			First,
			// Token: 0x04001482 RID: 5250
			First_Wait,
			// Token: 0x04001483 RID: 5251
			Prepare,
			// Token: 0x04001484 RID: 5252
			Prepare_Wait,
			// Token: 0x04001485 RID: 5253
			Main,
			// Token: 0x04001486 RID: 5254
			Destroy_0
		}
	}
}
