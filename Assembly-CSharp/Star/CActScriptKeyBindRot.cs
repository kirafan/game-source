﻿using System;

namespace Star
{
	// Token: 0x02000556 RID: 1366
	public class CActScriptKeyBindRot : IRoomScriptData
	{
		// Token: 0x06001ADB RID: 6875 RVA: 0x0008F0B0 File Offset: 0x0008D4B0
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyBindRot actXlsKeyBindRot = (ActXlsKeyBindRot)pbase;
			this.m_TargetName = actXlsKeyBindRot.m_TargetName;
			this.m_OnOff = actXlsKeyBindRot.m_OnOff;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x040021B3 RID: 8627
		public string m_TargetName;

		// Token: 0x040021B4 RID: 8628
		public uint m_CRCKey;

		// Token: 0x040021B5 RID: 8629
		public bool m_OnOff;
	}
}
