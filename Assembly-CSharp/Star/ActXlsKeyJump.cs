﻿using System;

namespace Star
{
	// Token: 0x0200056C RID: 1388
	public class ActXlsKeyJump : ActXlsKeyBase
	{
		// Token: 0x06001B15 RID: 6933 RVA: 0x0008F73F File Offset: 0x0008DB3F
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_Randam);
			pio.Int(ref this.m_Offset);
			pio.Bool(ref this.m_FrameReset);
		}

		// Token: 0x04002201 RID: 8705
		public int m_Randam;

		// Token: 0x04002202 RID: 8706
		public int m_Offset;

		// Token: 0x04002203 RID: 8707
		public bool m_FrameReset;
	}
}
