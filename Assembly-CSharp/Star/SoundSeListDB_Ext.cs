﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020001AE RID: 430
	public static class SoundSeListDB_Ext
	{
		// Token: 0x06000B25 RID: 2853 RVA: 0x00042618 File Offset: 0x00040A18
		public static SoundSeListDB_Param GetParam(this SoundSeListDB self, eSoundSeListDB cueID, Dictionary<eSoundSeListDB, int> indices)
		{
			if (indices != null)
			{
				int num = 0;
				if (indices.TryGetValue(cueID, out num))
				{
					return self.m_Params[num];
				}
			}
			return default(SoundSeListDB_Param);
		}
	}
}
