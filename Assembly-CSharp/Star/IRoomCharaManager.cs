﻿using System;
using Meige;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000580 RID: 1408
	public class IRoomCharaManager : MonoBehaviour
	{
		// Token: 0x06001B50 RID: 6992 RVA: 0x000907F0 File Offset: 0x0008EBF0
		public static IRoomCharaManager LinkUpManager(GameObject plink, RoomBuilder pbuilder, long fplayerid, bool fmode)
		{
			IRoomCharaManager roomCharaManager;
			if (fmode)
			{
				roomCharaManager = plink.AddComponent<RoomCharaManager>();
			}
			else
			{
				roomCharaManager = plink.AddComponent<RoomFriendCharaManager>();
			}
			roomCharaManager.SetLinkManager(pbuilder);
			return roomCharaManager;
		}

		// Token: 0x06001B51 RID: 6993 RVA: 0x0009081E File Offset: 0x0008EC1E
		public virtual void SetLinkManager(RoomBuilder pbuilder)
		{
			this.m_Builder = pbuilder;
			RoomActionDataPakage.ReadPakage();
			this.m_ShadowHndl = ObjectResourceManager.CreateHandle(RoomUtility.GetRoomShadowResourcePath());
		}

		// Token: 0x06001B52 RID: 6994 RVA: 0x0009083C File Offset: 0x0008EC3C
		public bool IsAcitveResource()
		{
			bool result = true;
			if (!this.m_ShadowHndl.IsDone)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06001B53 RID: 6995 RVA: 0x0009085E File Offset: 0x0008EC5E
		public MeigeResource.Handler GetShadowResource()
		{
			return this.m_ShadowHndl;
		}

		// Token: 0x06001B54 RID: 6996 RVA: 0x00090866 File Offset: 0x0008EC66
		public virtual void BackLinkManager()
		{
			if (this.m_ShadowHndl != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_ShadowHndl);
				this.m_ShadowHndl = null;
			}
		}

		// Token: 0x06001B55 RID: 6997 RVA: 0x00090885 File Offset: 0x0008EC85
		public virtual bool IsBuild()
		{
			return true;
		}

		// Token: 0x06001B56 RID: 6998 RVA: 0x00090888 File Offset: 0x0008EC88
		public virtual void ResetState()
		{
		}

		// Token: 0x06001B57 RID: 6999 RVA: 0x0009088A File Offset: 0x0008EC8A
		public virtual void AbortAllCharaObjEventMode(Vector2 fsetpos, Vector2 frect)
		{
		}

		// Token: 0x06001B58 RID: 7000 RVA: 0x0009088C File Offset: 0x0008EC8C
		public virtual void SetUpRoomCharacter()
		{
		}

		// Token: 0x06001B59 RID: 7001 RVA: 0x0009088E File Offset: 0x0008EC8E
		public virtual void DeleteChara()
		{
		}

		// Token: 0x06001B5A RID: 7002 RVA: 0x00090890 File Offset: 0x0008EC90
		public virtual void DeleteCharacter(long fmanageid, bool fcutout = false)
		{
		}

		// Token: 0x06001B5B RID: 7003 RVA: 0x00090892 File Offset: 0x0008EC92
		public virtual void RefreshCharaCall()
		{
			FieldMapManager.RefreshManageChara(null);
		}

		// Token: 0x06001B5C RID: 7004 RVA: 0x0009089A File Offset: 0x0008EC9A
		public void StopAction(bool fstop)
		{
		}

		// Token: 0x06001B5D RID: 7005 RVA: 0x0009089C File Offset: 0x0008EC9C
		public bool CheckAction(RoomGreet.eCategory fcategory, long fmanageid)
		{
			RoomComActionPopup roomComActionPopup = new RoomComActionPopup();
			roomComActionPopup.m_CharaManageID = fmanageid;
			roomComActionPopup.m_CategoryID = fcategory;
			this.m_Builder.SendActionToMain(roomComActionPopup);
			return true;
		}

		// Token: 0x0400224C RID: 8780
		public RoomBuilder m_Builder;

		// Token: 0x0400224D RID: 8781
		private MeigeResource.Handler m_ShadowHndl;
	}
}
