﻿using System;

namespace Star
{
	// Token: 0x02000515 RID: 1301
	public class MovieVolumeInformation
	{
		// Token: 0x0600199E RID: 6558 RVA: 0x000856F8 File Offset: 0x00083AF8
		public MovieVolumeInformation(float volume)
		{
			this.m_Volume = volume;
		}

		// Token: 0x0400205B RID: 8283
		public float m_Volume;
	}
}
