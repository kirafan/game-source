﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F6 RID: 1526
	public class RoomComFloorChange : IRoomEventCommand
	{
		// Token: 0x06001E28 RID: 7720 RVA: 0x000A20EF File Offset: 0x000A04EF
		public RoomComFloorChange(int fmodelno, bool fbuyobj)
		{
			this.m_Type = eRoomRequest.ChangeFloor;
			this.m_Enable = true;
			this.m_ObjID = fmodelno;
			this.m_ChangeResID = 0;
			this.m_ChangeMngID = -1L;
		}

		// Token: 0x06001E29 RID: 7721 RVA: 0x000A211C File Offset: 0x000A051C
		public override bool CalcRequest(RoomBuilder pbuilder)
		{
			bool result = false;
			switch (this.m_Step)
			{
			case 0:
			{
				UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(pbuilder.GetManageID());
				if (manageIDToUserRoomData != null)
				{
					UserRoomData.PlacementData categoryToPlacementData = UserRoomUtil.GetCategoryToPlacementData(manageIDToUserRoomData, eRoomObjectCategory.Floor);
					if (categoryToPlacementData != null)
					{
						UserRoomObjectData userRoomObjectData = UserRoomUtil.GetManageIDToUserRoomObjData(categoryToPlacementData.m_ManageID);
						if (userRoomObjectData != null)
						{
							userRoomObjectData.SetFreeLinkMngID(categoryToPlacementData.m_ManageID);
						}
						userRoomObjectData = UserRoomUtil.GetUserRoomObjData(eRoomObjectCategory.Floor, this.m_ObjID);
						if (userRoomObjectData != null)
						{
							this.m_ChangeMngID = (categoryToPlacementData.m_ManageID = userRoomObjectData.GetFreeLinkMngID(true));
							this.m_ChangeResID = (categoryToPlacementData.m_ObjectID = userRoomObjectData.ResourceID);
						}
					}
				}
				RoomComBuildScript roomComBuildScript = new RoomComBuildScript(pbuilder.GetManageID());
				roomComBuildScript.StackSendCmd(RoomComBuildScript.eCmd.ChangePlacement, 0L, 0, null);
				roomComBuildScript.PlaySend();
				this.m_Step++;
				break;
			}
			case 1:
			{
				IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
				this.m_NewModelMain = (RoomObjectControllCreate.SetupMdlSimple(floorManager.CreateFloorNode(), 0, RoomUtility.GetFloorLayer(0) - 10, 0, this.m_ChangeResID, this.m_ChangeMngID, false) as RoomObjectMdlSprite);
				this.m_NewModelMain.transform.localPosition = Vector3.one * 1000f;
				floorManager = pbuilder.GetFloorManager(1);
				this.m_NewModelSub = (RoomObjectControllCreate.SetupMdlSimple(floorManager.CreateFloorNode(), 1, RoomUtility.GetFloorLayer(1) - 10, 0, this.m_ChangeResID, this.m_ChangeMngID, false) as RoomObjectMdlSprite);
				this.m_NewModelSub.transform.localPosition = Vector3.one * 1000f;
				this.m_Step++;
				break;
			}
			case 2:
				if (!this.m_NewModelMain.IsPreparing())
				{
					this.m_Flag |= 1;
				}
				if (!this.m_NewModelSub.IsPreparing())
				{
					this.m_Flag |= 2;
				}
				if (this.m_Flag >= 3)
				{
					IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
					this.m_NewModelMain.transform.localPosition = floorManager.GetFloor().transform.localPosition;
					this.m_NewModelMain.SetSortingOrder(0f, RoomUtility.GetFloorLayer(0) + 10);
					this.m_NewModelMain.SetBaseColor(floorManager.GetBaseColor());
					this.m_NewModelMain.SetFadeIn(0.2f);
					floorManager = pbuilder.GetFloorManager(1);
					this.m_NewModelSub.transform.localPosition = floorManager.GetFloor().transform.localPosition;
					this.m_NewModelSub.SetSortingOrder(0f, RoomUtility.GetFloorLayer(1) + 10);
					this.m_NewModelSub.SetBaseColor(floorManager.GetBaseColor());
					this.m_NewModelSub.SetFadeIn(0.2f);
					this.m_Step++;
				}
				break;
			case 3:
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= 0.2f)
				{
					this.m_NewModelMain.SetSortingOrder(0f, RoomUtility.GetFloorLayer(0));
					IRoomFloorManager floorManager = pbuilder.GetFloorManager(0);
					floorManager.ChangeFloor(this.m_NewModelMain);
					this.m_NewModelMain = null;
					this.m_NewModelSub.SetSortingOrder(0f, RoomUtility.GetFloorLayer(1));
					floorManager = pbuilder.GetFloorManager(1);
					floorManager.ChangeFloor(this.m_NewModelSub);
					this.m_NewModelSub = null;
					this.m_Step++;
					result = true;
					this.m_Enable = false;
				}
				break;
			case 4:
				result = true;
				break;
			}
			return result;
		}

		// Token: 0x040024A5 RID: 9381
		public int m_ObjID;

		// Token: 0x040024A6 RID: 9382
		public RoomObjectMdlSprite m_NewModelMain;

		// Token: 0x040024A7 RID: 9383
		public RoomObjectMdlSprite m_NewModelSub;

		// Token: 0x040024A8 RID: 9384
		public int m_Step;

		// Token: 0x040024A9 RID: 9385
		public int m_Flag;

		// Token: 0x040024AA RID: 9386
		public float m_Time;

		// Token: 0x040024AB RID: 9387
		public int m_ChangeResID;

		// Token: 0x040024AC RID: 9388
		public long m_ChangeMngID;
	}
}
