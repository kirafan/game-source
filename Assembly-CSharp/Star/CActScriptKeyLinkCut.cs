﻿using System;

namespace Star
{
	// Token: 0x02000548 RID: 1352
	public class CActScriptKeyLinkCut : IRoomScriptData
	{
		// Token: 0x06001AC3 RID: 6851 RVA: 0x0008ED18 File Offset: 0x0008D118
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyLinkCut actXlsKeyLinkCut = (ActXlsKeyLinkCut)pbase;
			this.m_BindHrcName = actXlsKeyLinkCut.m_BindHrcName;
			this.m_CRCKey = CRC32.Calc(this.m_BindHrcName);
			this.m_LinkTime = (float)actXlsKeyLinkCut.m_LinkTime / 30f;
		}

		// Token: 0x04002184 RID: 8580
		public string m_BindHrcName;

		// Token: 0x04002185 RID: 8581
		public uint m_CRCKey;

		// Token: 0x04002186 RID: 8582
		public float m_LinkTime;
	}
}
