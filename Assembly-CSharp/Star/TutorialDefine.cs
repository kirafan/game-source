﻿using System;

namespace Star
{
	// Token: 0x02000739 RID: 1849
	public static class TutorialDefine
	{
		// Token: 0x04002B53 RID: 11091
		public const int GACHA_ID = 1;

		// Token: 0x04002B54 RID: 11092
		public const int QUEST_ID = 1100010;

		// Token: 0x04002B55 RID: 11093
		public const int ADV_ID_GACHA_PREV = 1000000;

		// Token: 0x04002B56 RID: 11094
		public const int ADV_ID_GACHA_AFTER = 1000001;

		// Token: 0x04002B57 RID: 11095
		public const int ADV_ID_PROLOGUE_INTRO_1 = 1000002;

		// Token: 0x04002B58 RID: 11096
		public const int ADV_ID_PROLOGUE_INTRO_2 = 1000003;

		// Token: 0x04002B59 RID: 11097
		public const int ADV_ID_PROLOGUE_END = 1000004;

		// Token: 0x04002B5A RID: 11098
		public const int MASTER_ORB_ID = 1;
	}
}
