﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001D9 RID: 473
	public class NamedListDB : ScriptableObject
	{
		// Token: 0x04000B2D RID: 2861
		public NamedListDB_Param[] m_Params;
	}
}
