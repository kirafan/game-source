﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200050E RID: 1294
	public class MissionDummyServerApi : IMissionServerApi
	{
		// Token: 0x06001964 RID: 6500 RVA: 0x000839B8 File Offset: 0x00081DB8
		public static IMissionServerApi CreateDummyServer()
		{
			MissionDummyServerApi missionDummyServerApi = SingletonMonoBehaviour<GameSystem>.Inst.GetComponent<MissionDummyServerApi>();
			if (missionDummyServerApi == null)
			{
				missionDummyServerApi = SingletonMonoBehaviour<GameSystem>.Inst.gameObject.AddComponent<MissionDummyServerApi>();
				missionDummyServerApi.m_DataHandle = MeigeResourceManager.LoadHandler("Prefab/MissionData.muast", new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
			}
			return missionDummyServerApi;
		}

		// Token: 0x06001965 RID: 6501 RVA: 0x00083A08 File Offset: 0x00081E08
		public override List<IMissionPakage> GetMissionList()
		{
			return this.m_List;
		}

		// Token: 0x06001966 RID: 6502 RVA: 0x00083A10 File Offset: 0x00081E10
		public override bool IsPreComp()
		{
			return this.m_NewCompList.Count != 0;
		}

		// Token: 0x06001967 RID: 6503 RVA: 0x00083A24 File Offset: 0x00081E24
		public override IMissionPakage GetPreCompMission(PopupMaskFunc fmask)
		{
			long num = this.m_NewCompList[0];
			this.m_NewCompList.RemoveAt(0);
			IMissionPakage missionPakage = null;
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].m_KeyManageID == num)
				{
					missionPakage = this.m_List[i];
					missionPakage.m_State = eMissionState.PreView;
					break;
				}
			}
			return missionPakage;
		}

		// Token: 0x06001968 RID: 6504 RVA: 0x00083A9C File Offset: 0x00081E9C
		private void SetUpDummyData()
		{
			long num = 36000000000L;
			long ticks = ScheduleTimeUtil.GetManageUniversalTime().Ticks;
			MissionDataDB dataList = this.m_DataList;
			for (int i = 0; i < dataList.m_Params.Length; i++)
			{
				if (dataList.m_Params[i].m_ListUpNo < 0)
				{
					IMissionPakage missionPakage = new IMissionPakage();
					switch (dataList.m_Params[i].m_Category)
					{
					case 0:
						missionPakage.m_Type = eMissionCategory.Day;
						break;
					case 1:
						missionPakage.m_Type = eMissionCategory.Week;
						break;
					case 2:
						missionPakage.m_Type = eMissionCategory.Unlock;
						break;
					case 3:
						missionPakage.m_Type = eMissionCategory.Event;
						break;
					}
					missionPakage.m_KeyManageID = (long)(i + 1);
					missionPakage.m_ListUpID = (long)dataList.m_Params[i].m_ID;
					missionPakage.m_ActionCategory = (eXlsMissionSeg)dataList.m_Params[i].m_ActionNo;
					missionPakage.m_ExtensionScriptID = dataList.m_Params[i].m_ExtentionScriptID;
					missionPakage.m_RateBase = dataList.m_Params[i].m_ExtentionFunKey;
					missionPakage.m_TargetMessage = dataList.m_Params[i].m_TargetMessage;
					long ticks2 = ticks + (long)dataList.m_Params[i].m_LimitTime * num;
					missionPakage.m_LimitTime = new DateTime(ticks2, DateTimeKind.Local);
					switch (dataList.m_Params[i].m_RewardType)
					{
					case 0:
						missionPakage.m_RewardCategory = eMissionRewardCategory.Money;
						break;
					case 1:
						missionPakage.m_RewardCategory = eMissionRewardCategory.Money;
						break;
					case 2:
						missionPakage.m_RewardCategory = eMissionRewardCategory.Item;
						break;
					case 6:
						missionPakage.m_RewardCategory = eMissionRewardCategory.Gem;
						break;
					}
					missionPakage.m_RewardNo = dataList.m_Params[i].m_RewardID;
					missionPakage.m_RewardNum = dataList.m_Params[i].m_RewardNum;
					this.m_List.Add(missionPakage);
				}
			}
		}

		// Token: 0x06001969 RID: 6505 RVA: 0x00083CBA File Offset: 0x000820BA
		public override bool IsListUp()
		{
			return true;
		}

		// Token: 0x0600196A RID: 6506 RVA: 0x00083CC0 File Offset: 0x000820C0
		public override void UnlockAction(eXlsMissionSeg fcategory, int factionno)
		{
			IMissionFunction missionFunction = null;
			switch (fcategory)
			{
			case eXlsMissionSeg.System:
				missionFunction = new MissionFuncSystem();
				break;
			case eXlsMissionSeg.Battle:
				missionFunction = new MissionFuncBattle();
				break;
			case eXlsMissionSeg.Town:
				missionFunction = new MissionFuncTown();
				break;
			case eXlsMissionSeg.Room:
				missionFunction = new MissionFuncRoom();
				break;
			case eXlsMissionSeg.Edit:
				missionFunction = new MissionFuncRoom();
				break;
			}
			if (missionFunction != null)
			{
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_ActionCategory == fcategory && this.m_List[i].m_ExtensionScriptID == factionno && this.m_List[i].m_State == eMissionState.Non && missionFunction.CheckUnlock(this.m_List[i], this.m_List))
					{
						this.m_List[i].m_State = eMissionState.PreComp;
						this.m_NewCompList.Add(this.m_List[i].m_KeyManageID);
					}
				}
			}
		}

		// Token: 0x0600196B RID: 6507 RVA: 0x00083DD8 File Offset: 0x000821D8
		public override void CompleteMission(long fcompmanageid)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].m_KeyManageID == fcompmanageid)
				{
					this.m_List[i].m_State = eMissionState.Comp;
					break;
				}
			}
		}

		// Token: 0x0600196C RID: 6508 RVA: 0x00083E34 File Offset: 0x00082234
		public void Update()
		{
			if (this.m_DataList == null)
			{
				if (this.m_DataHandle.IsDone)
				{
					this.m_DataList = this.m_DataHandle.GetAsset<MissionDataDB>();
					this.SetUpDummyData();
				}
			}
			else
			{
				this.SystemDataUpCheck();
			}
		}

		// Token: 0x0600196D RID: 6509 RVA: 0x00083E84 File Offset: 0x00082284
		private void SystemDataUpCheck()
		{
			this.m_SystemCheckTime -= Time.deltaTime;
			if (this.m_SystemCheckTime < 0f)
			{
				this.m_SystemCheckTime = 0.5f;
				IMissionFunction missionFunction = new MissionFuncSystem();
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_ActionCategory == eXlsMissionSeg.System && this.m_List[i].m_Rate < this.m_List[i].m_RateBase && missionFunction.CheckUnlock(this.m_List[i], this.m_List))
					{
						this.m_NewCompList.Add(this.m_List[i].m_KeyManageID);
					}
				}
			}
		}

		// Token: 0x0600196E RID: 6510 RVA: 0x00083F58 File Offset: 0x00082358
		public void CallbackApiRecv(MeigewwwParam wwwParam)
		{
		}

		// Token: 0x04002031 RID: 8241
		private List<IMissionPakage> m_List = new List<IMissionPakage>();

		// Token: 0x04002032 RID: 8242
		private List<long> m_NewCompList = new List<long>();

		// Token: 0x04002033 RID: 8243
		private const float SYSTEM_CHECK_TIME = 0.5f;

		// Token: 0x04002034 RID: 8244
		private float m_SystemCheckTime;

		// Token: 0x04002035 RID: 8245
		private MeigeResource.Handler m_DataHandle;

		// Token: 0x04002036 RID: 8246
		private MissionDataDB m_DataList;
	}
}
