﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Star
{
	// Token: 0x02000189 RID: 393
	public static class CRIFileVersionDB_Ext
	{
		// Token: 0x06000AD6 RID: 2774 RVA: 0x00040A50 File Offset: 0x0003EE50
		public static List<string> GetPaths(this CRIFileVersionDB self, int index, out string out_fileNameKeyWithoutExt, out uint out_version)
		{
			StringBuilder stringBuilder = new StringBuilder();
			List<string> list = new List<string>();
			if (self.m_Params[index].m_IsAcb != 0)
			{
				stringBuilder.Append(self.m_Params[index].m_FileName);
				stringBuilder.Append(".acb");
				list.Add(stringBuilder.ToString());
			}
			if (self.m_Params[index].m_IsAwb != 0)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(self.m_Params[index].m_FileName);
				stringBuilder.Append(".awb");
				list.Add(stringBuilder.ToString());
			}
			if (self.m_Params[index].m_IsAcf != 0)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(self.m_Params[index].m_FileName);
				stringBuilder.Append(".acf");
				list.Add(stringBuilder.ToString());
			}
			if (self.m_Params[index].m_IsUsm != 0)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(self.m_Params[index].m_FileName);
				stringBuilder.Append(".usm");
				list.Add(stringBuilder.ToString());
			}
			out_fileNameKeyWithoutExt = self.m_Params[index].m_FileName;
			out_version = self.m_Params[index].m_Version;
			return list;
		}

		// Token: 0x06000AD7 RID: 2775 RVA: 0x00040BC0 File Offset: 0x0003EFC0
		public static string GetAcf(this CRIFileVersionDB self, out string out_fileNameKeyWithoutExt, out uint out_version)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_IsAcf != 0)
				{
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(self.m_Params[i].m_FileName);
					stringBuilder.Append(".acf");
					out_fileNameKeyWithoutExt = self.m_Params[i].m_FileName;
					out_version = self.m_Params[i].m_Version;
					return stringBuilder.ToString();
				}
			}
			out_fileNameKeyWithoutExt = null;
			out_version = 0U;
			return null;
		}
	}
}
