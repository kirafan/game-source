﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200020A RID: 522
	public class SkillListDB : ScriptableObject
	{
		// Token: 0x04000CE1 RID: 3297
		public SkillListDB_Param[] m_Params;
	}
}
