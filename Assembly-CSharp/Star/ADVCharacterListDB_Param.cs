﻿using System;

namespace Star
{
	// Token: 0x0200000C RID: 12
	[Serializable]
	public struct ADVCharacterListDB_Param
	{
		// Token: 0x0400005B RID: 91
		public string m_ADVCharaID;

		// Token: 0x0400005C RID: 92
		public string m_ResourceBaseName;

		// Token: 0x0400005D RID: 93
		public string m_DisplayName;

		// Token: 0x0400005E RID: 94
		public int m_NamedType;

		// Token: 0x0400005F RID: 95
		public string m_CueSheet;

		// Token: 0x04000060 RID: 96
		public ADVCharacterListDB_Data[] m_Datas;
	}
}
