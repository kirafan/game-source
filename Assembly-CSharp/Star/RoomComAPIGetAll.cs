﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x02000589 RID: 1417
	public class RoomComAPIGetAll : INetComHandle
	{
		// Token: 0x06001BA7 RID: 7079 RVA: 0x00092882 File Offset: 0x00090C82
		public RoomComAPIGetAll()
		{
			this.ApiName = "player/room/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}

		// Token: 0x04002287 RID: 8839
		public long playerId;
	}
}
