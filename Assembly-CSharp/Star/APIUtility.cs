﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using Star.UI;
using Star.UI.WeaponList;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000B59 RID: 2905
	public static class APIUtility
	{
		// Token: 0x06003DA9 RID: 15785 RVA: 0x00137375 File Offset: 0x00135775
		public static void ShowErrorWindowNone(string errorMessage)
		{
			APIUtility.ShowErrorWindowNone("通信エラー", errorMessage);
		}

		// Token: 0x06003DAA RID: 15786 RVA: 0x00137382 File Offset: 0x00135782
		public static void ShowErrorWindowNone(string errorTitle, string errorMessage)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.None, errorTitle, errorMessage, null, null);
		}

		// Token: 0x06003DAB RID: 15787 RVA: 0x00137398 File Offset: 0x00135798
		public static void ShowErrorWindowOk(string errorMessage, Action action)
		{
			APIUtility.ShowErrorWindowOk("通信エラー", errorMessage, action);
		}

		// Token: 0x06003DAC RID: 15788 RVA: 0x001373A8 File Offset: 0x001357A8
		public static void ShowErrorWindowOk(string errorTitle, string errorMessage, Action action)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, errorTitle, errorMessage, null, delegate(int answer)
			{
				action.Call();
			});
		}

		// Token: 0x06003DAD RID: 15789 RVA: 0x001373F0 File Offset: 0x001357F0
		public static void ShowErrorWindowOk_NoClose(string errorTitle, string errorMessage, Action action)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK_NoClose, errorTitle, errorMessage, null, delegate(int answer)
			{
				action.Call();
			});
		}

		// Token: 0x06003DAE RID: 15790 RVA: 0x00137438 File Offset: 0x00135838
		public static void ShowErrorWindowRetry(MeigewwwParam wwwParam, string errorMessage)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (wwwParam.IsSystemError())
			{
				stringBuilder.Append("通信エラーが発生しました。");
			}
			else if (string.IsNullOrEmpty(errorMessage))
			{
				stringBuilder.Append("通信エラーが発生しました。");
			}
			else
			{
				stringBuilder.Append(errorMessage);
			}
			stringBuilder.Append("\n");
			stringBuilder.Append("リトライします。");
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, "通信エラー", stringBuilder.ToString(), null, delegate(int answer)
			{
				APIUtility.RetryAPI(wwwParam);
			});
		}

		// Token: 0x06003DAF RID: 15791 RVA: 0x001374DD File Offset: 0x001358DD
		public static void ShowErrorWindowTitle(MeigewwwParam wwwParam, string errorMessage)
		{
			APIUtility.ShowErrorWindowTitle(wwwParam, "通信エラー", errorMessage);
		}

		// Token: 0x06003DB0 RID: 15792 RVA: 0x001374EC File Offset: 0x001358EC
		public static void ShowErrorWindowTitle(MeigewwwParam wwwParam, string errorTitle, string errorMessage)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (wwwParam != null && wwwParam.IsSystemError())
			{
				stringBuilder.Append("通信エラーが発生しました。");
			}
			else if (string.IsNullOrEmpty(errorMessage))
			{
				stringBuilder.Append("通信エラーが発生しました。");
			}
			else
			{
				stringBuilder.Append(errorMessage);
			}
			stringBuilder.Append("\n");
			stringBuilder.Append("タイトルに戻ります。");
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, errorTitle, stringBuilder.ToString(), null, delegate(int answer)
			{
				APIUtility.ReturnTitle(true);
			});
		}

		// Token: 0x06003DB1 RID: 15793 RVA: 0x00137592 File Offset: 0x00135992
		public static void RetryAPI(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06003DB2 RID: 15794 RVA: 0x001375AC File Offset: 0x001359AC
		public static void ReturnTitle(bool isForceTransitTitleScene = true)
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			inst.GlobalParam.Reset();
			inst.CharaMng.DestroyCharaAll();
			inst.CharaResMng.UnloadAll(true);
			inst.TownResMng.UnloadAllResources();
			inst.RoomResMng.UnloadAllResources();
			inst.EffectMng.ForceResetOnReturnTitle();
			inst.SpriteMng.ForceResetOnReturnTitle();
			inst.SoundMng.ForceResetOnReturnTitle();
			inst.MovieMng.ForceResetOnReturnTitle();
			inst.StoreMng.ForceResetOnReturnTitle();
			inst.QuestMng.ForceResetOnReturnTitle();
			inst.MissionMng.ForceResetOnReturnTitle();
			inst.LoginManager.ForceResetOnReturnTitle();
			inst.FadeMng.SetFadeRatio(1f);
			inst.FadeMng.SetColor(Color.white);
			inst.GlobalUI.Close();
			inst.GlobalUI.ResetSelectedShortCut();
			inst.ConnectingIcon.Close();
			inst.LoadingUI.Abort();
			inst.TutorialTipsUI.Abort();
			inst.TutorialMessage.Clear();
			inst.BackGroundManager.Clear();
			inst.InputBlock.Clear();
			inst.GlobalParam.IsRequestedReturnTitle = true;
			SingletonMonoBehaviour<GemShopPlayer>.Inst.ForceHide();
			SingletonMonoBehaviour<StaminaShopPlayer>.Inst.ForceHide();
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.ForceHide();
			SingletonMonoBehaviour<WeaponListPlayer>.Inst.ForceHide();
			SingletonMonoBehaviour<RewardPlayer>.Inst.ForceHide();
			SingletonMonoBehaviour<SupportEditPlayer>.Inst.ForceHide();
			inst.OverlayUIMng.Clear();
			inst.WebView.Hide();
			Screen.sleepTimeout = -1;
			Time.timeScale = 1f;
			Application.targetFrameRate = 30;
			if (isForceTransitTitleScene)
			{
				SingletonMonoBehaviour<SceneLoader>.Inst.ForceTransitTitleScene();
			}
		}

		// Token: 0x06003DB3 RID: 15795 RVA: 0x0013774C File Offset: 0x00135B4C
		public static void OpenStore()
		{
			string text = WebDataListUtil.GetIDToAdress(1013);
			if (text == null)
			{
				text = "https://play.google.com/store/apps/details?id=com.aniplex.kirarafantasia";
			}
			Application.OpenURL(text);
		}

		// Token: 0x06003DB4 RID: 15796 RVA: 0x00137778 File Offset: 0x00135B78
		public static void ApplyingOn(string applyUrl, string applyAssetUrl)
		{
			Version.IsApplying = true;
			ProjDepend.SERVER_APPLYING_URL = applyUrl;
			AssetServerSettings.m_assetServerReleaseURL = applyAssetUrl;
			NetworkQueueManager.SetRelease(true);
		}

		// Token: 0x06003DB5 RID: 15797 RVA: 0x00137794 File Offset: 0x00135B94
		public static string GenerateUUID()
		{
			return Guid.NewGuid().ToString();
		}

		// Token: 0x06003DB6 RID: 15798 RVA: 0x001377B4 File Offset: 0x00135BB4
		public static bool IsNeedSignup()
		{
			return string.IsNullOrEmpty(AccessSaveData.Inst.UUID);
		}

		// Token: 0x06003DB7 RID: 15799 RVA: 0x001377D0 File Offset: 0x00135BD0
		public static string ArrayToStr<T>(T[] array)
		{
			if (array != null)
			{
				string text = string.Empty;
				for (int i = 0; i < array.Length; i++)
				{
					text += ((i != 0) ? ("," + array[i].ToString()) : array[i].ToString());
				}
				return text;
			}
			return null;
		}

		// Token: 0x06003DB8 RID: 15800 RVA: 0x00137844 File Offset: 0x00135C44
		public static void wwwToUserData(Player src, UserData dest)
		{
			dest.ID = src.id;
			dest.Name = src.name;
			dest.Comment = src.comment;
			dest.Age = (eAgeType)src.age;
			dest.MyCode = src.myCode;
			dest.Lv = src.level;
			dest.LvExp = src.levelExp;
			dest.Exp = src.totalExp;
			dest.Gold = src.gold;
			dest.UnlimitedGem = src.unlimitedGem;
			dest.LimitedGem = src.limitedGem;
			dest.LotteryTicket = src.lotteryTicket;
			dest.PartyCost = src.partyCost;
			dest.KRRPoint.SetPoint(src.kirara);
			dest.KRRPoint.SetLimit(src.kiraraLimit);
			dest.WeaponLimit = src.weaponLimit;
			dest.WeaponLimitCount = src.weaponLimitCount;
			dest.FacilityLimit = src.facilityLimit;
			dest.FacilityLimitCount = src.facilityLimitCount;
			dest.RoomObjectLimit = src.roomObjectLimit;
			dest.RoomObjectLimitCount = src.roomObjectLimitCount;
			dest.CharacterLimit = src.characterLimit;
			dest.ItemLimit = src.itemLimit;
			dest.FriendLimit = src.friendLimit;
			dest.SupportLimit = src.supportLimit;
			dest.LoginCount = src.loginCount;
			dest.LastLoginAt = src.lastLoginAt;
			dest.LoginDays = src.loginDays;
			dest.ContinuousDays = src.continuousDays;
			dest.Stamina.SetValueMax(src.staminaMax);
			dest.Stamina.SetValue(src.stamina, false);
			dest.Stamina.SetRemainSecMax((float)src.recastTimeMax);
			dest.Stamina.SetRemainSec((float)src.recastTime);
			SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.LocalPush_StaminaFull();
		}

		// Token: 0x06003DB9 RID: 15801 RVA: 0x00137A10 File Offset: 0x00135E10
		public static void wwwToUserData(PlayerCharacter src, UserCharacterData dest)
		{
			dest.MngID = src.managedCharacterId;
			dest.IsNew = (src.shown == 0);
			if (dest.Param == null)
			{
				dest.Param = new CharacterParam();
			}
			CharacterUtility.SetupCharacterParam(dest.Param, src.managedCharacterId, src.characterId, src.level, src.levelLimit, src.exp, src.levelBreak);
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(dest.Param.CharaID);
			dest.Param.UniqueSkillLearnData = new SkillLearnData(param.m_CharaSkillID, src.skillLevel1, src.skillLevelLimit1, src.skillExp1, param.m_CharaSkillExpTableID, true);
			if (dest.Param.ClassSkillLearnDatas != null)
			{
				dest.Param.ClassSkillLearnDatas = new List<SkillLearnData>();
			}
			dest.Param.ClassSkillLearnDatas.Clear();
			int num = 0;
			if (num < param.m_ClassSkillIDs.Length)
			{
				dest.Param.ClassSkillLearnDatas.Add(new SkillLearnData(param.m_ClassSkillIDs[num], src.skillLevel2, src.skillLevelLimit2, src.skillExp2, param.m_ClassSkillExpTableIDs[num], true));
			}
			num = 1;
			if (num < param.m_ClassSkillIDs.Length)
			{
				dest.Param.ClassSkillLearnDatas.Add(new SkillLearnData(param.m_ClassSkillIDs[num], src.skillLevel3, src.skillLevelLimit3, src.skillExp3, param.m_ClassSkillExpTableIDs[num], true));
			}
		}

		// Token: 0x06003DBA RID: 15802 RVA: 0x00137B94 File Offset: 0x00135F94
		public static void wwwToUserData(PlayerCharacter[] src, List<UserCharacterData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserCharacterData userCharacterData = new UserCharacterData();
				APIUtility.wwwToUserData(src[i], userCharacterData);
				dest.Add(userCharacterData);
			}
		}

		// Token: 0x06003DBB RID: 15803 RVA: 0x00137BD4 File Offset: 0x00135FD4
		public static void wwwToUserData(SupportCharacter src, UserSupportData dest)
		{
			dest.MngID = src.managedCharacterId;
			dest.CharaID = src.characterId;
			dest.Lv = src.level;
			dest.MaxLv = EditUtility.CalcMaxLvFromLimitBreak(src.characterId, src.levelBreak);
			dest.Exp = src.exp;
			dest.LimitBreak = src.levelBreak;
			dest.UniqueSkillLv = src.skillLevel1;
			dest.ClassSkillLvs = new int[]
			{
				src.skillLevel2,
				src.skillLevel3
			};
			dest.FriendShip = src.namedLevel;
			dest.FriendShipExp = src.namedExp;
			dest.WeaponID = src.weaponId;
			dest.WeaponLv = src.weaponLevel;
			dest.WeaponSkillLv = src.weaponSkillLevel;
			dest.WeaponSkillExp = src.weaponSkillExp;
		}

		// Token: 0x06003DBC RID: 15804 RVA: 0x00137CA8 File Offset: 0x001360A8
		public static void wwwToUserData(SupportCharacter[] src, List<UserSupportData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserSupportData userSupportData = new UserSupportData();
				APIUtility.wwwToUserData(src[i], userSupportData);
				dest.Add(userSupportData);
			}
		}

		// Token: 0x06003DBD RID: 15805 RVA: 0x00137CE8 File Offset: 0x001360E8
		public static void wwwToUserData(PlayerNamedType src, UserNamedData dest)
		{
			dest.MngID = src.managedNamedTypeId;
			dest.NamedType = (eCharaNamedType)src.namedType;
			dest.FriendShip = src.level;
			dest.FriendShipExp = src.exp;
			NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(dest.NamedType);
			dest.FriendShipMax = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB.GetFriendShipMax(param.m_FriendshipTableID);
		}

		// Token: 0x06003DBE RID: 15806 RVA: 0x00137D64 File Offset: 0x00136164
		public static void wwwToUserData(PlayerNamedType[] src, Dictionary<eCharaNamedType, UserNamedData> dest)
		{
			if (src == null || dest == null)
			{
				return;
			}
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserNamedData userNamedData = new UserNamedData();
				APIUtility.wwwToUserData(src[i], userNamedData);
				dest.Add((eCharaNamedType)src[i].namedType, userNamedData);
			}
		}

		// Token: 0x06003DBF RID: 15807 RVA: 0x00137DB8 File Offset: 0x001361B8
		public static void wwwToUserData(PlayerWeapon src, UserWeaponData dest)
		{
			dest.MngID = src.managedWeaponId;
			if (dest.Param == null)
			{
				dest.Param = new WeaponParam();
			}
			CharacterUtility.SetupWeaponParam(dest.Param, src.managedWeaponId, src.weaponId, src.level, src.exp, src.skillLevel, src.skillExp);
		}

		// Token: 0x06003DC0 RID: 15808 RVA: 0x00137E18 File Offset: 0x00136218
		public static void wwwToUserData(PlayerWeapon[] src, List<UserWeaponData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserWeaponData userWeaponData = new UserWeaponData();
				APIUtility.wwwToUserData(src[i], userWeaponData);
				dest.Add(userWeaponData);
			}
		}

		// Token: 0x06003DC1 RID: 15809 RVA: 0x00137E55 File Offset: 0x00136255
		public static void wwwToUserData(PlayerItemSummary src, UserItemData dest)
		{
			dest.ItemID = src.id;
			dest.ItemNum = src.amount;
		}

		// Token: 0x06003DC2 RID: 15810 RVA: 0x00137E70 File Offset: 0x00136270
		public static void wwwToUserData(PlayerItemSummary[] src, List<UserItemData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserItemData userItemData = new UserItemData();
				APIUtility.wwwToUserData(src[i], userItemData);
				dest.Add(userItemData);
			}
		}

		// Token: 0x06003DC3 RID: 15811 RVA: 0x00137EB0 File Offset: 0x001362B0
		public static void wwwToUserData(PlayerMasterOrb src, UserMasterOrbData dest)
		{
			dest.MngID = src.managedMasterOrbId;
			dest.OrbID = src.masterOrbId;
			dest.Lv = src.level;
			dest.Exp = src.exp;
			dest.IsEquip = (src.equipment == 1);
		}

		// Token: 0x06003DC4 RID: 15812 RVA: 0x00137EFC File Offset: 0x001362FC
		public static void wwwToUserData(PlayerMasterOrb[] src, List<UserMasterOrbData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserMasterOrbData userMasterOrbData = new UserMasterOrbData();
				APIUtility.wwwToUserData(src[i], userMasterOrbData);
				dest.Add(userMasterOrbData);
			}
		}

		// Token: 0x06003DC5 RID: 15813 RVA: 0x00137F3C File Offset: 0x0013633C
		public static void wwwToUserData(PlayerBattleParty src, UserBattlePartyData dest)
		{
			dest.MngID = src.managedBattlePartyId;
			dest.PartyName = src.name;
			dest.ClearSlotAll();
			for (int i = 0; i < src.managedCharacterIds.Length; i++)
			{
				dest.SetMemberAt(i, src.managedCharacterIds[i]);
			}
			for (int j = 0; j < src.managedWeaponIds.Length; j++)
			{
				dest.SetWeaponAt(j, src.managedWeaponIds[j]);
			}
		}

		// Token: 0x06003DC6 RID: 15814 RVA: 0x00137FB8 File Offset: 0x001363B8
		public static void wwwToUserData(PlayerBattleParty[] src, List<UserBattlePartyData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserBattlePartyData userBattlePartyData = new UserBattlePartyData();
				APIUtility.wwwToUserData(src[i], userBattlePartyData);
				dest.Add(userBattlePartyData);
			}
		}

		// Token: 0x06003DC7 RID: 15815 RVA: 0x00137FF8 File Offset: 0x001363F8
		public static void wwwToUserData(PlayerSupport src, UserSupportPartyData dest)
		{
			dest.MngID = src.managedSupportId;
			dest.PartyName = src.name;
			dest.IsActive = (src.active == 1);
			dest.CharaMngIDs.Clear();
			for (int i = 0; i < src.managedCharacterIds.Length; i++)
			{
				dest.CharaMngIDs.Add(src.managedCharacterIds[i]);
			}
			dest.WeaponMngIDs.Clear();
			for (int j = 0; j < src.managedWeaponIds.Length; j++)
			{
				dest.WeaponMngIDs.Add(src.managedWeaponIds[j]);
			}
		}

		// Token: 0x06003DC8 RID: 15816 RVA: 0x0013809C File Offset: 0x0013649C
		public static void wwwToUserData(PlayerSupport[] src, List<UserSupportPartyData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				UserSupportPartyData userSupportPartyData = new UserSupportPartyData();
				APIUtility.wwwToUserData(src[i], userSupportPartyData);
				dest.Add(userSupportPartyData);
			}
		}

		// Token: 0x06003DC9 RID: 15817 RVA: 0x001380D9 File Offset: 0x001364D9
		public static void wwwToUserData(PlayerTown[] src, UserTownData dest)
		{
			WWWComUtil.SetTownBuildListKey(src, true);
		}

		// Token: 0x06003DCA RID: 15818 RVA: 0x001380E2 File Offset: 0x001364E2
		public static void wwwToUserData(PlayerTownFacility[] src, List<UserTownObjectData> dest)
		{
			WWWComUtil.SetTownObjectListKey(src, true);
		}

		// Token: 0x06003DCB RID: 15819 RVA: 0x001380EB File Offset: 0x001364EB
		public static void wwwToUserData(PlayerRoom[] src, List<UserRoomData> dest)
		{
			WWWComUtil.SetRoomPlacementKey(src, true);
		}

		// Token: 0x06003DCC RID: 15820 RVA: 0x001380F5 File Offset: 0x001364F5
		public static void wwwToUserData(PlayerRoomObject[] src, List<UserRoomObjectData> dest)
		{
			WWWComUtil.SetRoomObjectList(src, true, false);
		}

		// Token: 0x06003DCD RID: 15821 RVA: 0x001380FF File Offset: 0x001364FF
		public static void wwwToUserData(PlayerFieldPartyMember[] src, UserFieldCharaData dest)
		{
			WWWComUtil.SetFieldPartyListKey(src, true);
		}

		// Token: 0x040044F9 RID: 17657
		public const string ERR_TITLE = "通信エラー";

		// Token: 0x040044FA RID: 17658
		public const string ERR_TITLE_MAINTENANCE = "メンテナンス中";

		// Token: 0x040044FB RID: 17659
		public const string ERR_TITLE_PLAYER_DELETED = "削除済みのユーザー";

		// Token: 0x040044FC RID: 17660
		public const string ERR_TITLE_NOT_UUID = "引き継ぎが行われました。";

		// Token: 0x040044FD RID: 17661
		public const string ERR_TITLE_OUTDATED = "アプリの更新";

		// Token: 0x040044FE RID: 17662
		public const string ERR_TITLE_AB_OUTDATED = "データの更新";

		// Token: 0x040044FF RID: 17663
		public const string ERR_TITLE_PLAYER_SESSION_EXPIRED = "日付の更新";

		// Token: 0x04004500 RID: 17664
		public const string ERR_MSG_SYSTEM_ERROR = "通信エラーが発生しました。";

		// Token: 0x04004501 RID: 17665
		public const string ERR_MSG_RETURN_TITLE = "タイトルに戻ります。";

		// Token: 0x04004502 RID: 17666
		public const string ERR_MSG_RETRY = "リトライします。";
	}
}
