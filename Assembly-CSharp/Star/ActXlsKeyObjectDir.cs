﻿using System;

namespace Star
{
	// Token: 0x0200056E RID: 1390
	public class ActXlsKeyObjectDir : ActXlsKeyBase
	{
		// Token: 0x06001B19 RID: 6937 RVA: 0x0008F7AC File Offset: 0x0008DBAC
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			int func = (int)this.m_Func;
			pio.Int(ref func);
			this.m_Func = (CActScriptKeyObjectDir.eDirFunc)func;
		}

		// Token: 0x04002207 RID: 8711
		public CActScriptKeyObjectDir.eDirFunc m_Func;
	}
}
