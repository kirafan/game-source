﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020006C3 RID: 1731
	public class TownPartsGetItem : ITownPartsAction
	{
		// Token: 0x06002260 RID: 8800 RVA: 0x000B6C2C File Offset: 0x000B502C
		public TownPartsGetItem(TownBuilder pbuilder, ComItemUpState.eUpCategory fcategory, int fnum, Vector3 fpos, float fdelaytime)
		{
			this.m_Builder = pbuilder;
			this.m_Object = this.m_Builder.GetGeneralObj(eTownObjectType.GET_ITEM);
			this.m_Object.transform.position = fpos;
			this.m_Object.SetActive(false);
			this.m_Active = true;
			this.m_Time = fdelaytime;
			this.m_Message = this.m_Object.GetComponentsInChildren<Text>();
			this.m_TitleImage = this.m_Object.GetComponentInChildren<Image>();
			TownResourceBind component = this.m_Object.GetComponent<TownResourceBind>();
			this.IsEnableUpdate = true;
			string text = string.Empty;
			switch (fcategory)
			{
			case ComItemUpState.eUpCategory.Item:
				text = FldMessageUtil.GetMessage(0);
				this.m_TitleImage.sprite = null;
				this.m_TitleImage.enabled = false;
				break;
			case ComItemUpState.eUpCategory.Money:
				text = FldMessageUtil.GetMessage(1);
				this.m_TitleImage.sprite = null;
				this.m_TitleImage.enabled = false;
				break;
			case ComItemUpState.eUpCategory.Point:
				text = FldMessageUtil.GetMessage(3);
				this.m_TitleImage.enabled = true;
				this.m_TitleImage.sprite = (component.m_Table[11].m_Res as Sprite);
				break;
			case ComItemUpState.eUpCategory.Stamina:
				text = FldMessageUtil.GetMessage(2);
				this.m_TitleImage.enabled = true;
				this.m_TitleImage.sprite = (component.m_Table[10].m_Res as Sprite);
				break;
			case ComItemUpState.eUpCategory.FriendShip:
				text = FldMessageUtil.GetMessage(4);
				this.m_TitleImage.enabled = true;
				this.m_TitleImage.sprite = (component.m_Table[12].m_Res as Sprite);
				break;
			}
			for (int i = 0; i < this.m_Message.Length; i++)
			{
				this.m_Message[i].text = text;
				Color color = this.m_Message[i].color;
				color.a = 0f;
				this.m_Message[i].color = color;
				this.m_Message[i].enabled = false;
			}
			this.m_TitleImage.color = new Color(this.m_TitleImage.color.r, this.m_TitleImage.color.g, this.m_TitleImage.color.b, 0f);
			this.m_BasePos = fpos;
			int j = fnum;
			int num;
			if (fnum != 0)
			{
				num = 0;
				while (j > 0)
				{
					j /= 10;
					num++;
				}
			}
			else
			{
				num = 1;
			}
			Vector3 fpos2 = Vector3.right * 18f * (float)num;
			this.m_NumberNum = num;
			this.m_Number = new TownPartsGetItem.NumberState[num];
			if (fnum != 0)
			{
				num = 0;
				j = fnum;
				while (j > 0)
				{
					this.m_Number[num].SetUpNumber(this.m_Object.transform, component.m_Table[j % 10].m_Res as Sprite, j % 10, fpos2, fdelaytime + (float)num * 0.09f);
					j /= 10;
					num++;
					fpos2.x -= 18f;
				}
			}
			else
			{
				this.m_Number[0].SetUpNumber(this.m_Object.transform, component.m_Table[0].m_Res as Sprite, 0, fpos2, fdelaytime);
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06002261 RID: 8801 RVA: 0x000B6FA6 File Offset: 0x000B53A6
		// (set) Token: 0x06002262 RID: 8802 RVA: 0x000B6FAE File Offset: 0x000B53AE
		public bool IsEnableUpdate { get; set; }

		// Token: 0x06002263 RID: 8803 RVA: 0x000B6FB8 File Offset: 0x000B53B8
		public override bool PartsUpdate()
		{
			if (!this.IsEnableUpdate)
			{
				return true;
			}
			switch (this.m_Step)
			{
			case 0:
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					this.m_Object.SetActive(true);
					this.m_Step++;
					this.m_Time = 0.15f;
				}
				break;
			case 1:
				this.SetFontFade(1f - this.m_Time / 0.15f);
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					this.SetFontFade(1f);
					this.m_Step++;
					this.m_Time = 0.8f;
				}
				else
				{
					this.SetFontFade(1f - this.m_Time / 0.15f);
				}
				break;
			case 2:
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					for (int i = 0; i < this.m_NumberNum; i++)
					{
						this.m_Number[i].SetFadeOut();
					}
					this.m_Step++;
					this.m_Time = 1f;
				}
				break;
			case 3:
				this.m_Object.transform.position = Vector3.up * (1f - this.m_Time) + this.m_BasePos;
				if (this.m_Time < 0.5f)
				{
					this.SetFontFade(this.m_Time * 2f);
				}
				this.m_Time -= Time.deltaTime;
				if (this.m_Time <= 0f)
				{
					this.m_Builder.ReleaseCharaMarker(this.m_Object);
					this.m_Object = null;
					this.m_Active = false;
				}
				break;
			}
			for (int i = 0; i < this.m_NumberNum; i++)
			{
				this.m_Number[i].Update(Time.deltaTime);
			}
			return this.m_Active;
		}

		// Token: 0x06002264 RID: 8804 RVA: 0x000B71F4 File Offset: 0x000B55F4
		private void SetFontFade(float falpha)
		{
			Color color;
			for (int i = 0; i < this.m_Message.Length; i++)
			{
				color = this.m_Message[i].color;
				color.a = falpha;
				this.m_Message[i].color = color;
			}
			color = this.m_TitleImage.color;
			color.a = falpha;
			this.m_TitleImage.color = color;
		}

		// Token: 0x04002910 RID: 10512
		private TownBuilder m_Builder;

		// Token: 0x04002911 RID: 10513
		private GameObject m_Object;

		// Token: 0x04002912 RID: 10514
		private int m_Step;

		// Token: 0x04002913 RID: 10515
		private float m_Time;

		// Token: 0x04002914 RID: 10516
		private Text[] m_Message;

		// Token: 0x04002915 RID: 10517
		private Image m_TitleImage;

		// Token: 0x04002916 RID: 10518
		private Vector3 m_BasePos;

		// Token: 0x04002917 RID: 10519
		private Vector3 m_TargetPos;

		// Token: 0x04002919 RID: 10521
		private TownPartsGetItem.NumberState[] m_Number;

		// Token: 0x0400291A RID: 10522
		private int m_NumberNum;

		// Token: 0x020006C4 RID: 1732
		public struct NumberState
		{
			// Token: 0x06002265 RID: 8805 RVA: 0x000B7260 File Offset: 0x000B5660
			public void SetUpNumber(Transform parent, Sprite psrite, int fno, Vector3 fpos, float fdelay)
			{
				this.m_Time = fdelay;
				this.m_Number = new GameObject(fno.ToString());
				this.m_Number.transform.SetParent(parent, false);
				this.m_Number.transform.localPosition = fpos;
				this.m_BasePos = fpos;
				this.CreateRender(psrite);
			}

			// Token: 0x06002266 RID: 8806 RVA: 0x000B72C0 File Offset: 0x000B56C0
			public void Update(float faddtime)
			{
				switch (this.m_Step)
				{
				case 0:
					this.m_Time -= faddtime;
					if (this.m_Time <= 0f)
					{
						this.m_Time = 0.4f;
						this.m_Step++;
					}
					break;
				case 1:
					this.m_Time -= faddtime;
					if (this.m_Time <= 0f)
					{
						this.m_Time = 0f;
						this.m_Step = 2;
					}
					this.m_Number.transform.localPosition = Vector3.up * Mathf.Sin(this.m_Time / 0.4f * 3.1415927f) * 8f + this.m_BasePos;
					if (this.m_Time < 0.3f)
					{
						this.SetFade(1f);
					}
					else
					{
						this.SetFade(10f * (0.4f - this.m_Time));
					}
					break;
				case 3:
					this.m_Time -= faddtime;
					if (this.m_Time < 0.5f)
					{
						this.SetFade(this.m_Time * 2f);
					}
					break;
				}
			}

			// Token: 0x06002267 RID: 8807 RVA: 0x000B7418 File Offset: 0x000B5818
			public void SetFadeOut()
			{
				this.m_Step = 3;
				this.m_Time = 1f;
			}

			// Token: 0x06002268 RID: 8808 RVA: 0x000B742C File Offset: 0x000B582C
			private void SetFade(float falpha)
			{
				Color white = Color.white;
				white.a = falpha;
				this.m_Render.SetColor(white);
			}

			// Token: 0x06002269 RID: 8809 RVA: 0x000B7454 File Offset: 0x000B5854
			private void CreateRender(Sprite psprite)
			{
				Color white = Color.white;
				white.a = 0f;
				float x = (psprite.textureRect.x + psprite.textureRect.width) / (float)psprite.texture.width;
				float x2 = psprite.textureRect.x / (float)psprite.texture.width;
				float y = (psprite.textureRect.y + psprite.textureRect.height) / (float)psprite.texture.height;
				float y2 = psprite.textureRect.y / (float)psprite.texture.height;
				(this.m_Render = this.m_Number.AddComponent<TownPartsCanvasRender>()).SetMesh(new Vector3[]
				{
					new Vector3(-9f, 0f, 0f),
					new Vector3(9f, 0f, 0f),
					new Vector3(-9f, -20f, 0f),
					new Vector3(9f, -20f, 0f)
				}, new Vector2[]
				{
					new Vector2(x2, y),
					new Vector2(x, y),
					new Vector2(x2, y2),
					new Vector2(x, y2)
				}, new int[]
				{
					0,
					1,
					2,
					1,
					3,
					2
				}, white, psprite.texture);
			}

			// Token: 0x0400291B RID: 10523
			public int m_Step;

			// Token: 0x0400291C RID: 10524
			public float m_Time;

			// Token: 0x0400291D RID: 10525
			public GameObject m_Number;

			// Token: 0x0400291E RID: 10526
			public Vector3 m_BasePos;

			// Token: 0x0400291F RID: 10527
			public TownPartsCanvasRender m_Render;
		}
	}
}
