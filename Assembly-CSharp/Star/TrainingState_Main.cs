﻿using System;
using Star.UI.Global;
using Star.UI.Training;

namespace Star
{
	// Token: 0x020004C7 RID: 1223
	public class TrainingState_Main : TrainingState
	{
		// Token: 0x06001809 RID: 6153 RVA: 0x0007D05F File Offset: 0x0007B45F
		public TrainingState_Main(TrainingMain owner) : base(owner)
		{
		}

		// Token: 0x0600180A RID: 6154 RVA: 0x0007D077 File Offset: 0x0007B477
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x0600180B RID: 6155 RVA: 0x0007D07A File Offset: 0x0007B47A
		public override void OnStateEnter()
		{
			this.m_Step = TrainingState_Main.eStep.First;
		}

		// Token: 0x0600180C RID: 6156 RVA: 0x0007D083 File Offset: 0x0007B483
		public override void OnStateExit()
		{
		}

		// Token: 0x0600180D RID: 6157 RVA: 0x0007D085 File Offset: 0x0007B485
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600180E RID: 6158 RVA: 0x0007D090 File Offset: 0x0007B490
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case TrainingState_Main.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = TrainingState_Main.eStep.LoadWait;
				break;
			case TrainingState_Main.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = TrainingState_Main.eStep.PlayIn;
				}
				break;
			case TrainingState_Main.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = TrainingState_Main.eStep.Main;
				}
				break;
			case TrainingState_Main.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						this.m_NextState = 2147483646;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = TrainingState_Main.eStep.UnloadChildSceneWait;
				}
				break;
			case TrainingState_Main.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					LocalSaveData.Inst.OccurredTrainingPrepare = true;
					LocalSaveData.Inst.Save();
					this.m_Step = TrainingState_Main.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600180F RID: 6159 RVA: 0x0007D1D4 File Offset: 0x0007B5D4
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<TrainingUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.m_NpcUI);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Training);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001810 RID: 6160 RVA: 0x0007D276 File Offset: 0x0007B676
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x06001811 RID: 6161 RVA: 0x0007D285 File Offset: 0x0007B685
		private void OnClickButton()
		{
		}

		// Token: 0x06001812 RID: 6162 RVA: 0x0007D287 File Offset: 0x0007B687
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001ECF RID: 7887
		private TrainingState_Main.eStep m_Step = TrainingState_Main.eStep.None;

		// Token: 0x04001ED0 RID: 7888
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.TrainingUI;

		// Token: 0x04001ED1 RID: 7889
		private TrainingUI m_UI;

		// Token: 0x020004C8 RID: 1224
		private enum eStep
		{
			// Token: 0x04001ED3 RID: 7891
			None = -1,
			// Token: 0x04001ED4 RID: 7892
			First,
			// Token: 0x04001ED5 RID: 7893
			LoadWait,
			// Token: 0x04001ED6 RID: 7894
			PlayIn,
			// Token: 0x04001ED7 RID: 7895
			Main,
			// Token: 0x04001ED8 RID: 7896
			UnloadChildSceneWait
		}
	}
}
