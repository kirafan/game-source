﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B9 RID: 1465
	public class RoomCharaActRejoice : IRoomCharaAct
	{
		// Token: 0x06001C61 RID: 7265 RVA: 0x00097F66 File Offset: 0x00096366
		public void RequestRejoiceMode(RoomObjectCtrlChara pbase)
		{
			pbase.PlayMotion(RoomDefine.eAnim.Rejoice, WrapMode.ClampForever);
		}

		// Token: 0x06001C62 RID: 7266 RVA: 0x00097F70 File Offset: 0x00096370
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			if (!pbase.IsPlayingMotion(RoomDefine.eAnim.Rejoice))
			{
				result = false;
			}
			return result;
		}
	}
}
