﻿using System;

namespace Star
{
	// Token: 0x0200052E RID: 1326
	public class CharaActionDummy : ICharaRoomAction
	{
		// Token: 0x06001A83 RID: 6787 RVA: 0x0008B80E File Offset: 0x00089C0E
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
		}

		// Token: 0x06001A84 RID: 6788 RVA: 0x0008B810 File Offset: 0x00089C10
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			return false;
		}

		// Token: 0x06001A85 RID: 6789 RVA: 0x0008B813 File Offset: 0x00089C13
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			return true;
		}

		// Token: 0x06001A86 RID: 6790 RVA: 0x0008B816 File Offset: 0x00089C16
		public void Release()
		{
		}
	}
}
