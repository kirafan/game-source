﻿using System;

namespace Star
{
	// Token: 0x02000232 RID: 562
	public enum ePresentType
	{
		// Token: 0x04000F5E RID: 3934
		None,
		// Token: 0x04000F5F RID: 3935
		Chara,
		// Token: 0x04000F60 RID: 3936
		Item,
		// Token: 0x04000F61 RID: 3937
		Weapon,
		// Token: 0x04000F62 RID: 3938
		TownFacility,
		// Token: 0x04000F63 RID: 3939
		RoomObject,
		// Token: 0x04000F64 RID: 3940
		MasterOrb,
		// Token: 0x04000F65 RID: 3941
		KRRPoint,
		// Token: 0x04000F66 RID: 3942
		Gold,
		// Token: 0x04000F67 RID: 3943
		UnlimitedGem,
		// Token: 0x04000F68 RID: 3944
		LimitedGem
	}
}
