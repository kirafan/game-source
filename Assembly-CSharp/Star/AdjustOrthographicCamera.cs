﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000133 RID: 307
	public class AdjustOrthographicCamera : MonoBehaviour
	{
		// Token: 0x060007F7 RID: 2039 RVA: 0x000339F0 File Offset: 0x00031DF0
		private void Start()
		{
			this.m_Camera = base.GetComponent<Camera>();
			if (this.m_Camera != null)
			{
				this.AdjustOrthographic();
			}
			else
			{
				UnityEngine.Object.Destroy(base.gameObject.GetComponent<AdjustOrthographicCamera>());
			}
			if (this.m_IsDestroyOnStart)
			{
				UnityEngine.Object.Destroy(base.gameObject.GetComponent<AdjustOrthographicCamera>());
			}
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x00033A50 File Offset: 0x00031E50
		private void Update()
		{
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x00033A54 File Offset: 0x00031E54
		public void AdjustOrthographic()
		{
			this.m_Camera.orthographic = true;
			this.m_Camera.orthographicSize = (float)Screen.height / 2f / this.m_BasePixelPerUnit;
			float num = this.m_BaseHeight / this.m_BaseWidth;
			float num2 = (float)Screen.height / (float)Screen.width;
			if (num > num2)
			{
				float num3 = this.m_BaseHeight / (float)Screen.height;
				this.m_Camera.orthographicSize *= num3;
			}
			else
			{
				float num4 = this.m_BaseWidth / (float)Screen.width;
				this.m_Camera.orthographicSize *= num4;
			}
		}

		// Token: 0x040007BF RID: 1983
		[SerializeField]
		private float m_BaseWidth = 1920f;

		// Token: 0x040007C0 RID: 1984
		[SerializeField]
		private float m_BaseHeight = 1080f;

		// Token: 0x040007C1 RID: 1985
		[SerializeField]
		private float m_BasePixelPerUnit = 100f;

		// Token: 0x040007C2 RID: 1986
		[SerializeField]
		private bool m_IsDestroyOnStart = true;

		// Token: 0x040007C3 RID: 1987
		private Camera m_Camera;
	}
}
