﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Star
{
	// Token: 0x02000293 RID: 659
	public class AnimeFileEdit
	{
		// Token: 0x06000C53 RID: 3155 RVA: 0x000482C0 File Offset: 0x000466C0
		public static List<AnimSettings> AnimationListUp(string fapppath, string filepath, string fmaskname, string fkeyname, int fsubno)
		{
			List<AnimSettings> list = new List<AnimSettings>();
			string str = string.Format(filepath, fsubno);
			string text = string.Format(fmaskname, fsubno);
			DirectoryInfo directoryInfo = new DirectoryInfo(fapppath + str);
			FileInfo[] files = directoryInfo.GetFiles("*.prefab");
			int num = files.Length;
			for (int i = 0; i < num; i++)
			{
				if (files[i].Name.IndexOf(fkeyname) >= 0 && files[i].Name.IndexOf(text) >= 0)
				{
					string text2 = files[i].FullName.Replace("\\", "/");
					text2 = text2.Replace(fapppath, string.Empty);
					string text3 = files[i].Name.Replace(text, string.Empty);
					string actionKey = text3.Replace(".prefab", string.Empty);
					list.Add(new AnimSettings(actionKey, text2.Replace(".prefab", string.Empty)));
				}
			}
			return list;
		}
	}
}
