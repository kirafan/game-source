﻿using System;

namespace Star
{
	// Token: 0x020004F7 RID: 1271
	public enum eXlsPopupTiming
	{
		// Token: 0x04001FD6 RID: 8150
		PopNon,
		// Token: 0x04001FD7 RID: 8151
		PopSystem,
		// Token: 0x04001FD8 RID: 8152
		PopRoom,
		// Token: 0x04001FD9 RID: 8153
		PopTown,
		// Token: 0x04001FDA RID: 8154
		PopEdit,
		// Token: 0x04001FDB RID: 8155
		PopBattle,
		// Token: 0x04001FDC RID: 8156
		PopChara,
		// Token: 0x04001FDD RID: 8157
		PopPlayer
	}
}
