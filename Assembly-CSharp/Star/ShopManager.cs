﻿using System;
using System.Collections.Generic;
using ItemTradeRequestTypes;
using ItemTradeResponseTypes;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using RoomObjectRequestTypes;
using RoomObjectResponseTypes;
using Star.UI;
using TownFacilityRequestTypes;
using TownFacilityResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200062B RID: 1579
	public sealed class ShopManager
	{
		// Token: 0x06001ECA RID: 7882 RVA: 0x000A745A File Offset: 0x000A585A
		public int GetTradeListNum()
		{
			return this.m_TradeList.Count;
		}

		// Token: 0x06001ECB RID: 7883 RVA: 0x000A7467 File Offset: 0x000A5867
		public ShopManager.TradeData GetTradeDataAt(int index)
		{
			if (index >= 0 && index < this.m_TradeList.Count)
			{
				return this.m_TradeList[index];
			}
			return null;
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x000A7490 File Offset: 0x000A5890
		public ShopManager.TradeData GetTradeData(long id)
		{
			for (int i = 0; i < this.m_TradeList.Count; i++)
			{
				if ((long)this.m_TradeList[i].m_ID == id)
				{
					return this.m_TradeList[i];
				}
			}
			return null;
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x000A74E0 File Offset: 0x000A58E0
		public List<ShopManager.TradeData> GetTradeList(bool isLimited)
		{
			List<ShopManager.TradeData> list = new List<ShopManager.TradeData>();
			for (int i = 0; i < this.m_TradeList.Count; i++)
			{
				if (this.m_TradeList[i].m_IsLimited == isLimited)
				{
					list.Add(this.m_TradeList[i]);
				}
			}
			return list;
		}

		// Token: 0x14000023 RID: 35
		// (add) Token: 0x06001ECE RID: 7886 RVA: 0x000A753C File Offset: 0x000A593C
		// (remove) Token: 0x06001ECF RID: 7887 RVA: 0x000A7574 File Offset: 0x000A5974
		private event Action<MeigewwwParam, ItemTradeResponseTypes.Getrecipe> m_OnResponse_GetTradeRecipe;

		// Token: 0x14000024 RID: 36
		// (add) Token: 0x06001ED0 RID: 7888 RVA: 0x000A75AC File Offset: 0x000A59AC
		// (remove) Token: 0x06001ED1 RID: 7889 RVA: 0x000A75E4 File Offset: 0x000A59E4
		private event Action<MeigewwwParam, ItemTradeResponseTypes.Trade> m_OnResponse_Trade;

		// Token: 0x14000025 RID: 37
		// (add) Token: 0x06001ED2 RID: 7890 RVA: 0x000A761C File Offset: 0x000A5A1C
		// (remove) Token: 0x06001ED3 RID: 7891 RVA: 0x000A7654 File Offset: 0x000A5A54
		private event Action<MeigewwwParam, PlayerResponseTypes.Itemsale> m_OnResponse_ItemSale;

		// Token: 0x14000026 RID: 38
		// (add) Token: 0x06001ED4 RID: 7892 RVA: 0x000A768C File Offset: 0x000A5A8C
		// (remove) Token: 0x06001ED5 RID: 7893 RVA: 0x000A76C4 File Offset: 0x000A5AC4
		private event Action<MeigewwwParam, PlayerResponseTypes.Weaponmake> m_OnResponse_WeaponMake;

		// Token: 0x14000027 RID: 39
		// (add) Token: 0x06001ED6 RID: 7894 RVA: 0x000A76FC File Offset: 0x000A5AFC
		// (remove) Token: 0x06001ED7 RID: 7895 RVA: 0x000A7734 File Offset: 0x000A5B34
		private event Action<MeigewwwParam, PlayerResponseTypes.Weaponupgrade> m_OnResponse_WeaponUpgrade;

		// Token: 0x14000028 RID: 40
		// (add) Token: 0x06001ED8 RID: 7896 RVA: 0x000A776C File Offset: 0x000A5B6C
		// (remove) Token: 0x06001ED9 RID: 7897 RVA: 0x000A77A4 File Offset: 0x000A5BA4
		private event Action<MeigewwwParam, PlayerResponseTypes.Weaponsale> m_OnResponse_WeaponSale;

		// Token: 0x14000029 RID: 41
		// (add) Token: 0x06001EDA RID: 7898 RVA: 0x000A77DC File Offset: 0x000A5BDC
		// (remove) Token: 0x06001EDB RID: 7899 RVA: 0x000A7814 File Offset: 0x000A5C14
		private event Action<MeigewwwParam, PlayerResponseTypes.Weaponlimitadd> m_OnResponse_WeaponLimitAdd;

		// Token: 0x1400002A RID: 42
		// (add) Token: 0x06001EDC RID: 7900 RVA: 0x000A784C File Offset: 0x000A5C4C
		// (remove) Token: 0x06001EDD RID: 7901 RVA: 0x000A7884 File Offset: 0x000A5C84
		private event Action<MeigewwwParam, RoomObjectResponseTypes.Limitadd> m_OnResponse_RoomObjectLimitAdd;

		// Token: 0x1400002B RID: 43
		// (add) Token: 0x06001EDE RID: 7902 RVA: 0x000A78BC File Offset: 0x000A5CBC
		// (remove) Token: 0x06001EDF RID: 7903 RVA: 0x000A78F4 File Offset: 0x000A5CF4
		private event Action<MeigewwwParam, TownFacilityResponseTypes.Limitadd> m_OnResponse_TownFacilityLimitAdd;

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06001EE0 RID: 7904 RVA: 0x000A792A File Offset: 0x000A5D2A
		public int WeaponUpgradeSuccessType
		{
			get
			{
				return this.m_WeaponUpgradeSuccessType;
			}
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x000A7934 File Offset: 0x000A5D34
		public bool Request_GetTradeRecipe(Action<MeigewwwParam, ItemTradeResponseTypes.Getrecipe> onResponse)
		{
			this.m_OnResponse_GetTradeRecipe = onResponse;
			ItemTradeRequestTypes.Getrecipe param = new ItemTradeRequestTypes.Getrecipe();
			MeigewwwParam wwwParam = ItemTradeRequest.Getrecipe(param, new MeigewwwParam.Callback(this.OnResponse_GetTradeRecipe));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x000A7988 File Offset: 0x000A5D88
		private void OnResponse_GetTradeRecipe(MeigewwwParam wwwParam)
		{
			ItemTradeResponseTypes.Getrecipe getrecipe = ItemTradeResponse.Getrecipe(wwwParam, ResponseCommon.DialogType.None, null);
			if (getrecipe == null)
			{
				if (this.m_OnResponse_GetTradeRecipe != null)
				{
					this.m_OnResponse_GetTradeRecipe(wwwParam, getrecipe);
				}
				return;
			}
			ResultCode result = getrecipe.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				this.m_TradeList.Clear();
				for (int i = 0; i < getrecipe.itemTradeRecipes.Length; i++)
				{
					ItemTradeRecipe itemTradeRecipe = getrecipe.itemTradeRecipes[i];
					ShopManager.TradeData tradeData = new ShopManager.TradeData();
					tradeData.m_ID = itemTradeRecipe.id;
					tradeData.m_IsLimited = (itemTradeRecipe.limitedFlag == 1);
					tradeData.m_DstType = (ePresentType)itemTradeRecipe.dstType;
					tradeData.m_DstID = itemTradeRecipe.dstId;
					tradeData.m_DstAmount = itemTradeRecipe.dstAmount;
					tradeData.m_AltType = (ePresentType)itemTradeRecipe.altType;
					tradeData.m_AltID = itemTradeRecipe.altId;
					tradeData.m_AltAmount = itemTradeRecipe.altAmount;
					tradeData.m_SrcTypes = new ePresentType[]
					{
						(ePresentType)itemTradeRecipe.srcType1,
						(ePresentType)itemTradeRecipe.srcType2
					};
					tradeData.m_SrcIDs = new int[]
					{
						itemTradeRecipe.srcId1,
						itemTradeRecipe.srcId2
					};
					tradeData.m_SrcAmounts = new int[]
					{
						itemTradeRecipe.srcAmount1,
						itemTradeRecipe.srcAmount2
					};
					tradeData.m_StartAt = itemTradeRecipe.startAt;
					tradeData.m_EndAt = itemTradeRecipe.endAt;
					tradeData.m_LimitCount = itemTradeRecipe.limitCount;
					tradeData.m_ResetFlag = itemTradeRecipe.resetFlag;
					tradeData.m_TotalTradeCount = itemTradeRecipe.totalTradeCount;
					tradeData.m_MonthlyTradeCount = itemTradeRecipe.monthlyTradeCount;
					this.m_TradeList.Add(tradeData);
				}
			}
			if (this.m_OnResponse_GetTradeRecipe != null)
			{
				this.m_OnResponse_GetTradeRecipe(wwwParam, getrecipe);
			}
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x000A7B48 File Offset: 0x000A5F48
		public bool Request_Trade(int recipeID, int index, Action<MeigewwwParam, ItemTradeResponseTypes.Trade> onResponse)
		{
			this.m_OnResponse_Trade = onResponse;
			MeigewwwParam wwwParam = ItemTradeRequest.Trade(new ItemTradeRequestTypes.Trade
			{
				recipeId = recipeID,
				indexNo = index
			}, new MeigewwwParam.Callback(this.OnResponse_Trade));
			NetworkQueueManager.Request(wwwParam);
			this.m_SelectedRecipeID = recipeID;
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x000A7BB0 File Offset: 0x000A5FB0
		private void OnResponse_Trade(MeigewwwParam wwwParam)
		{
			ItemTradeResponseTypes.Trade trade = ItemTradeResponse.Trade(wwwParam, ResponseCommon.DialogType.None, null);
			if (trade == null)
			{
				if (this.m_OnResponse_Trade != null)
				{
					this.m_OnResponse_Trade(wwwParam, trade);
				}
				return;
			}
			ResultCode result = trade.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.WEAPON_LIMIT)
				{
					if (result != ResultCode.BUY_OUT_OF_PERIOD)
					{
					}
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(trade.player, userDataMng.UserData);
				APIUtility.wwwToUserData(trade.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(trade.managedWeapons, userDataMng.UserWeaponDatas);
				APIUtility.wwwToUserData(trade.managedCharacters, userDataMng.UserCharaDatas);
				APIUtility.wwwToUserData(trade.managedNamedTypes, userDataMng.UserNamedDatas);
				ShopManager.TradeData tradeData = this.GetTradeData((long)this.m_SelectedRecipeID);
				tradeData.m_TotalTradeCount = trade.totalTradeCount;
				tradeData.m_MonthlyTradeCount = trade.monthlyTradeCount;
			}
			if (this.m_OnResponse_Trade != null)
			{
				this.m_OnResponse_Trade(wwwParam, trade);
			}
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x000A7CC0 File Offset: 0x000A60C0
		public bool Request_ItemSale(int itemID, int amount, Action<MeigewwwParam, PlayerResponseTypes.Itemsale> onResponse)
		{
			return this.Request_ItemSale(new List<int>
			{
				itemID
			}, new List<int>
			{
				amount
			}, onResponse);
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x000A7CF0 File Offset: 0x000A60F0
		public bool Request_ItemSale(List<int> itemIDs, List<int> amounts, Action<MeigewwwParam, PlayerResponseTypes.Itemsale> onResponse)
		{
			this.m_OnResponse_ItemSale = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Itemsale(new PlayerRequestTypes.Itemsale
			{
				itemId = APIUtility.ArrayToStr<int>(itemIDs.ToArray()),
				amount = APIUtility.ArrayToStr<int>(amounts.ToArray())
			}, new MeigewwwParam.Callback(this.OnResponse_ItemSale));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x000A7D68 File Offset: 0x000A6168
		private void OnResponse_ItemSale(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Itemsale itemsale = PlayerResponse.Itemsale(wwwParam, ResponseCommon.DialogType.None, null);
			if (itemsale == null)
			{
				if (this.m_OnResponse_ItemSale != null)
				{
					this.m_OnResponse_ItemSale(wwwParam, itemsale);
				}
				return;
			}
			ResultCode result = itemsale.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(itemsale.player, userDataMng.UserData);
				APIUtility.wwwToUserData(itemsale.itemSummary, userDataMng.UserItemDatas);
			}
			if (this.m_OnResponse_ItemSale != null)
			{
				this.m_OnResponse_ItemSale(wwwParam, itemsale);
			}
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x000A7E00 File Offset: 0x000A6200
		public bool Request_WeaponMake(int recipeID, Action<MeigewwwParam, PlayerResponseTypes.Weaponmake> onResponse)
		{
			this.m_OnResponse_WeaponMake = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Weaponmake(new PlayerRequestTypes.Weaponmake
			{
				recipeId = recipeID
			}, new MeigewwwParam.Callback(this.OnResponse_WeaponMake));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EE9 RID: 7913 RVA: 0x000A7E5C File Offset: 0x000A625C
		private void OnResponse_WeaponMake(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Weaponmake weaponmake = PlayerResponse.Weaponmake(wwwParam, ResponseCommon.DialogType.None, null);
			if (weaponmake == null)
			{
				if (this.m_OnResponse_WeaponMake != null)
				{
					this.m_OnResponse_WeaponMake(wwwParam, weaponmake);
				}
				return;
			}
			ResultCode result = weaponmake.GetResult();
			if (result != ResultCode.GOLD_IS_SHORT)
			{
				if (result != ResultCode.ITEM_IS_SHORT)
				{
					if (result != ResultCode.SUCCESS)
					{
						if (result != ResultCode.WEAPON_LIMIT)
						{
						}
					}
					else
					{
						UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
						APIUtility.wwwToUserData(weaponmake.player, userDataMng.UserData);
						APIUtility.wwwToUserData(weaponmake.managedWeapons, userDataMng.UserWeaponDatas);
						APIUtility.wwwToUserData(weaponmake.itemSummary, userDataMng.UserItemDatas);
					}
				}
			}
			if (this.m_OnResponse_WeaponMake != null)
			{
				this.m_OnResponse_WeaponMake(wwwParam, weaponmake);
			}
		}

		// Token: 0x06001EEA RID: 7914 RVA: 0x000A7F34 File Offset: 0x000A6334
		public bool Request_WeaponUpgrade(long weaponMngID, List<int> itemIDs, List<int> amounts, Action<MeigewwwParam, PlayerResponseTypes.Weaponupgrade> onResponse)
		{
			this.m_OnResponse_WeaponUpgrade = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Weaponupgrade(new PlayerRequestTypes.Weaponupgrade
			{
				managedWeaponId = weaponMngID,
				itemId = APIUtility.ArrayToStr<int>(itemIDs.ToArray()),
				amount = APIUtility.ArrayToStr<int>(amounts.ToArray())
			}, new MeigewwwParam.Callback(this.OnResponse_WeaponUpgrade));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EEB RID: 7915 RVA: 0x000A7FB4 File Offset: 0x000A63B4
		private void OnResponse_WeaponUpgrade(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Weaponupgrade weaponupgrade = PlayerResponse.Weaponupgrade(wwwParam, ResponseCommon.DialogType.None, null);
			if (weaponupgrade == null)
			{
				if (this.m_OnResponse_WeaponUpgrade != null)
				{
					this.m_OnResponse_WeaponUpgrade(wwwParam, weaponupgrade);
				}
				return;
			}
			ResultCode result = weaponupgrade.GetResult();
			if (result != ResultCode.GOLD_IS_SHORT)
			{
				if (result != ResultCode.ITEM_IS_SHORT)
				{
					if (result == ResultCode.SUCCESS)
					{
						UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
						userDataMng.UserData.Gold = weaponupgrade.gold;
						UserWeaponData userWpnData = userDataMng.GetUserWpnData(weaponupgrade.managedWeapon.managedWeaponId);
						if (userWpnData != null)
						{
							APIUtility.wwwToUserData(weaponupgrade.managedWeapon, userWpnData);
						}
						APIUtility.wwwToUserData(weaponupgrade.itemSummary, userDataMng.UserItemDatas);
						this.m_WeaponUpgradeSuccessType = weaponupgrade.successType;
					}
				}
			}
			if (this.m_OnResponse_WeaponUpgrade != null)
			{
				this.m_OnResponse_WeaponUpgrade(wwwParam, weaponupgrade);
			}
		}

		// Token: 0x06001EEC RID: 7916 RVA: 0x000A809C File Offset: 0x000A649C
		public bool Request_WeaponSale(List<long> weaponMngIDs, Action<MeigewwwParam, PlayerResponseTypes.Weaponsale> onResponse)
		{
			this.m_OnResponse_WeaponSale = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Weaponsale(new PlayerRequestTypes.Weaponsale
			{
				managedWeaponId = APIUtility.ArrayToStr<long>(weaponMngIDs.ToArray())
			}, new MeigewwwParam.Callback(this.OnResponse_WeaponSale));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EED RID: 7917 RVA: 0x000A8100 File Offset: 0x000A6500
		private void OnResponse_WeaponSale(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Weaponsale weaponsale = PlayerResponse.Weaponsale(wwwParam, ResponseCommon.DialogType.None, null);
			if (weaponsale == null)
			{
				if (this.m_OnResponse_WeaponSale != null)
				{
					this.m_OnResponse_WeaponSale(wwwParam, weaponsale);
				}
				return;
			}
			ResultCode result = weaponsale.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(weaponsale.player, userDataMng.UserData);
				APIUtility.wwwToUserData(weaponsale.managedWeapons, userDataMng.UserWeaponDatas);
			}
			if (this.m_OnResponse_WeaponSale != null)
			{
				this.m_OnResponse_WeaponSale(wwwParam, weaponsale);
			}
		}

		// Token: 0x06001EEE RID: 7918 RVA: 0x000A8198 File Offset: 0x000A6598
		public bool Request_WeaponLimitAdd(Action<MeigewwwParam, PlayerResponseTypes.Weaponlimitadd> onResponse)
		{
			this.m_OnResponse_WeaponLimitAdd = onResponse;
			MeigewwwParam wwwParam = PlayerRequest.Weaponlimitadd(new PlayerRequestTypes.Weaponlimitadd
			{
				num = 1
			}, new MeigewwwParam.Callback(this.OnResponse_WeaponLimitAdd));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EEF RID: 7919 RVA: 0x000A81F4 File Offset: 0x000A65F4
		private void OnResponse_WeaponLimitAdd(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Weaponlimitadd weaponlimitadd = PlayerResponse.Weaponlimitadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (weaponlimitadd == null)
			{
				if (this.m_OnResponse_WeaponLimitAdd != null)
				{
					this.m_OnResponse_WeaponLimitAdd(wwwParam, weaponlimitadd);
				}
				return;
			}
			ResultCode result = weaponlimitadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.WEAPON_EXT_LIMIT)
				{
					if (result != ResultCode.GEM_IS_SHORT)
					{
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.WeaponLimitExtendTitle, eText_MessageDB.WeaponLimitExtendErr, null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(weaponlimitadd.player, userDataMng.UserData);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.WeaponLimitExtendTitle, eText_MessageDB.WeaponLimitExtendResult, null);
			}
			if (this.m_OnResponse_WeaponLimitAdd != null)
			{
				this.m_OnResponse_WeaponLimitAdd(wwwParam, weaponlimitadd);
			}
		}

		// Token: 0x06001EF0 RID: 7920 RVA: 0x000A82D0 File Offset: 0x000A66D0
		public bool Request_RoomObjectLimitAdd(Action<MeigewwwParam, RoomObjectResponseTypes.Limitadd> onResponse)
		{
			this.m_OnResponse_RoomObjectLimitAdd = onResponse;
			MeigewwwParam wwwParam = RoomObjectRequest.Limitadd(new RoomObjectRequestTypes.Limitadd
			{
				num = 1
			}, new MeigewwwParam.Callback(this.OnResponse_RoomObjectLimitAdd));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EF1 RID: 7921 RVA: 0x000A832C File Offset: 0x000A672C
		private void OnResponse_RoomObjectLimitAdd(MeigewwwParam wwwParam)
		{
			RoomObjectResponseTypes.Limitadd limitadd = RoomObjectResponse.Limitadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (limitadd == null)
			{
				if (this.m_OnResponse_RoomObjectLimitAdd != null)
				{
					this.m_OnResponse_RoomObjectLimitAdd(wwwParam, limitadd);
				}
				return;
			}
			ResultCode result = limitadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.ROOM_OBJECT_EXT_LIMIT)
				{
					if (result != ResultCode.GEM_IS_SHORT)
					{
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomLimitExtendTitle, eText_MessageDB.RoomLimitExtendErr, null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(limitadd.player, userDataMng.UserData);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomLimitExtendTitle, eText_MessageDB.RoomLimitExtendResult, null);
			}
			if (this.m_OnResponse_RoomObjectLimitAdd != null)
			{
				this.m_OnResponse_RoomObjectLimitAdd(wwwParam, limitadd);
			}
		}

		// Token: 0x06001EF2 RID: 7922 RVA: 0x000A8408 File Offset: 0x000A6808
		public bool Request_TownFacilityLimitAdd(Action<MeigewwwParam, TownFacilityResponseTypes.Limitadd> onResponse)
		{
			this.m_OnResponse_TownFacilityLimitAdd = onResponse;
			MeigewwwParam wwwParam = TownFacilityRequest.Limitadd(new TownFacilityRequestTypes.Limitadd
			{
				num = 1
			}, new MeigewwwParam.Callback(this.OnResponse_TownFacilityLimitAdd));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001EF3 RID: 7923 RVA: 0x000A8464 File Offset: 0x000A6864
		private void OnResponse_TownFacilityLimitAdd(MeigewwwParam wwwParam)
		{
			TownFacilityResponseTypes.Limitadd limitadd = TownFacilityResponse.Limitadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (limitadd == null)
			{
				if (this.m_OnResponse_TownFacilityLimitAdd != null)
				{
					this.m_OnResponse_TownFacilityLimitAdd(wwwParam, limitadd);
				}
				return;
			}
			ResultCode result = limitadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.FACILITY_EXT_LIMIT)
				{
					if (result != ResultCode.GEM_IS_SHORT)
					{
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.FacilityLimitExtendTitle, eText_MessageDB.FacilityLimitExtendErr, null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(limitadd.player, userDataMng.UserData);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.FacilityLimitExtendTitle, eText_MessageDB.FacilityLimitExtendResult, null);
			}
			if (this.m_OnResponse_TownFacilityLimitAdd != null)
			{
				this.m_OnResponse_TownFacilityLimitAdd(wwwParam, limitadd);
			}
		}

		// Token: 0x04002592 RID: 9618
		private List<ShopManager.TradeData> m_TradeList = new List<ShopManager.TradeData>();

		// Token: 0x04002593 RID: 9619
		private int m_SelectedRecipeID = -1;

		// Token: 0x0400259D RID: 9629
		private int m_WeaponUpgradeSuccessType;

		// Token: 0x0200062C RID: 1580
		public class TradeData
		{
			// Token: 0x170001D2 RID: 466
			// (get) Token: 0x06001EF5 RID: 7925 RVA: 0x000A8547 File Offset: 0x000A6947
			public int RemainNum
			{
				get
				{
					return this.m_LimitCount - this.m_TotalTradeCount;
				}
			}

			// Token: 0x170001D3 RID: 467
			// (get) Token: 0x06001EF6 RID: 7926 RVA: 0x000A8558 File Offset: 0x000A6958
			public TimeSpan DeadLineSpan
			{
				get
				{
					TimeSpan timeSpan = this.m_EndAt.Subtract(SingletonMonoBehaviour<GameSystem>.Inst.ServerTime);
					if (this.m_ResetFlag != 0)
					{
						DateTime serverTime = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime;
						DateTime d = new DateTime(serverTime.Year, serverTime.Month, 1, 23, 59, 59);
						d = d.AddMonths(1);
						d = d.AddDays(-1.0);
						TimeSpan timeSpan2 = d - serverTime;
						if (timeSpan > timeSpan2)
						{
							timeSpan = timeSpan2;
						}
					}
					return timeSpan;
				}
			}

			// Token: 0x0400259E RID: 9630
			public int m_ID;

			// Token: 0x0400259F RID: 9631
			public bool m_IsLimited;

			// Token: 0x040025A0 RID: 9632
			public ePresentType m_DstType;

			// Token: 0x040025A1 RID: 9633
			public int m_DstID;

			// Token: 0x040025A2 RID: 9634
			public int m_DstAmount;

			// Token: 0x040025A3 RID: 9635
			public ePresentType m_AltType;

			// Token: 0x040025A4 RID: 9636
			public int m_AltID;

			// Token: 0x040025A5 RID: 9637
			public int m_AltAmount;

			// Token: 0x040025A6 RID: 9638
			public ePresentType[] m_SrcTypes;

			// Token: 0x040025A7 RID: 9639
			public int[] m_SrcIDs;

			// Token: 0x040025A8 RID: 9640
			public int[] m_SrcAmounts;

			// Token: 0x040025A9 RID: 9641
			public DateTime m_StartAt;

			// Token: 0x040025AA RID: 9642
			public DateTime m_EndAt;

			// Token: 0x040025AB RID: 9643
			public int m_LimitCount;

			// Token: 0x040025AC RID: 9644
			public int m_ResetFlag;

			// Token: 0x040025AD RID: 9645
			public int m_TotalTradeCount;

			// Token: 0x040025AE RID: 9646
			public int m_MonthlyTradeCount;
		}
	}
}
