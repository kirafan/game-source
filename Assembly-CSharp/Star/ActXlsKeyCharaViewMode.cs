﻿using System;

namespace Star
{
	// Token: 0x02000575 RID: 1397
	public class ActXlsKeyCharaViewMode : ActXlsKeyBase
	{
		// Token: 0x06001B27 RID: 6951 RVA: 0x0008F992 File Offset: 0x0008DD92
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Bool(ref this.m_View);
			pio.Int(ref this.m_EntryTagID);
		}

		// Token: 0x04002217 RID: 8727
		public string m_TargetName;

		// Token: 0x04002218 RID: 8728
		public bool m_View;

		// Token: 0x04002219 RID: 8729
		public int m_EntryTagID;
	}
}
