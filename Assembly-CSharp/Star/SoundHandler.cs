﻿using System;

namespace Star
{
	// Token: 0x02000635 RID: 1589
	public class SoundHandler
	{
		// Token: 0x06001F40 RID: 8000 RVA: 0x000A9C24 File Offset: 0x000A8024
		public void OnMngConstruct(SoundObject so)
		{
			if (so != null)
			{
				this.m_SoundObject = so;
				this.m_SoundObject.AddRef();
			}
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x000A9C40 File Offset: 0x000A8040
		public void OnMngDestruct(bool stopIfStatusIsNotRemoved)
		{
			if (this.m_SoundObject != null)
			{
				if (stopIfStatusIsNotRemoved && this.m_SoundObject.GetStatus() != SoundObject.eStatus.Removed)
				{
					this.m_SoundObject.Stop();
				}
				this.m_SoundObject.RemoveRef();
				this.m_SoundObject = null;
			}
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x000A9C8D File Offset: 0x000A808D
		public bool IsAvailable()
		{
			return this.m_SoundObject != null;
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x000A9C9D File Offset: 0x000A809D
		public bool IsCompletePrepare()
		{
			return this.m_SoundObject != null && this.m_SoundObject.IsCompletePrepare();
		}

		// Token: 0x06001F44 RID: 8004 RVA: 0x000A9CB7 File Offset: 0x000A80B7
		public bool IsPaused()
		{
			return this.m_SoundObject != null && this.m_SoundObject.IsPaused();
		}

		// Token: 0x06001F45 RID: 8005 RVA: 0x000A9CD1 File Offset: 0x000A80D1
		public bool IsPrepare()
		{
			return this.m_SoundObject != null && this.m_SoundObject.GetStatus() == SoundObject.eStatus.Prepare;
		}

		// Token: 0x06001F46 RID: 8006 RVA: 0x000A9CF2 File Offset: 0x000A80F2
		public bool IsPlaying(bool isIgnorePaused = false)
		{
			return this.m_SoundObject != null && this.m_SoundObject.GetStatus() == SoundObject.eStatus.Playing && (isIgnorePaused || !this.m_SoundObject.IsPaused());
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x000A9D28 File Offset: 0x000A8128
		public bool Play()
		{
			if (this.m_SoundObject == null)
			{
				return false;
			}
			if (this.m_SoundObject.IsPrepared())
			{
				return this.m_SoundObject.Resume();
			}
			return this.m_SoundObject.Play();
		}

		// Token: 0x06001F48 RID: 8008 RVA: 0x000A9D5E File Offset: 0x000A815E
		public bool Stop()
		{
			return this.m_SoundObject != null && this.m_SoundObject.Stop();
		}

		// Token: 0x06001F49 RID: 8009 RVA: 0x000A9D78 File Offset: 0x000A8178
		public bool Suspend()
		{
			return this.m_SoundObject != null && this.m_SoundObject.Suspend();
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x000A9D92 File Offset: 0x000A8192
		public bool Resume()
		{
			if (this.m_SoundObject != null)
			{
				this.m_SoundObject.Resume();
			}
			return false;
		}

		// Token: 0x06001F4B RID: 8011 RVA: 0x000A9DAC File Offset: 0x000A81AC
		public string GetCueSheet()
		{
			if (this.m_SoundObject != null)
			{
				return this.m_SoundObject.CueSeet;
			}
			return null;
		}

		// Token: 0x06001F4C RID: 8012 RVA: 0x000A9DC6 File Offset: 0x000A81C6
		public string GetCueName()
		{
			if (this.m_SoundObject != null)
			{
				return this.m_SoundObject.CueName;
			}
			return null;
		}

		// Token: 0x040025F2 RID: 9714
		private SoundObject m_SoundObject;
	}
}
