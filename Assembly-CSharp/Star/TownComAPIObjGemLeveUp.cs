﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x0200068F RID: 1679
	public class TownComAPIObjGemLeveUp : INetComHandle
	{
		// Token: 0x060021C0 RID: 8640 RVA: 0x000B3ABC File Offset: 0x000B1EBC
		public TownComAPIObjGemLeveUp()
		{
			this.ApiName = "player/town_facility/gem_level_up";
			this.Request = true;
			this.ResponseType = typeof(Gemlevelup);
		}

		// Token: 0x04002833 RID: 10291
		public long managedTownFacilityId;

		// Token: 0x04002834 RID: 10292
		public int nextLevel;

		// Token: 0x04002835 RID: 10293
		public int openState;

		// Token: 0x04002836 RID: 10294
		public long actionTime;

		// Token: 0x04002837 RID: 10295
		public long remainingTime;
	}
}
