﻿using System;
using System.Collections;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000479 RID: 1145
	public class MixWpnUpgradeEffectScene : MonoBehaviour
	{
		// Token: 0x06001657 RID: 5719 RVA: 0x00074D9C File Offset: 0x0007319C
		private void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			MixWpnUpgradeEffectScene.eStep step = this.m_Step;
			if (step != MixWpnUpgradeEffectScene.eStep.Prepare_Wait)
			{
				if (step != MixWpnUpgradeEffectScene.eStep.Wait)
				{
					if (step == MixWpnUpgradeEffectScene.eStep.Play_Wait)
					{
						if (!this.IsPlaying())
						{
							this.m_MeigeAnimCtrl.UpdateAnimation(0f);
							this.m_MeigeAnimCtrl.Pause();
							this.m_BodyObj.SetActive(false);
							this.SetStep(MixWpnUpgradeEffectScene.eStep.None);
						}
					}
				}
			}
			else
			{
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.SetStep(MixWpnUpgradeEffectScene.eStep.Prepare_Error);
						return;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						return;
					}
				}
				GameObject gameObject = this.m_LoadingHndl.FindObj("Mix_WpnUpgrade", true) as GameObject;
				GameObject gameObject2 = this.m_LoadingHndl.FindObj("MeigeAC_Mix_WpnUpgrade@Take 001", true) as GameObject;
				if (gameObject != null && gameObject2 != null)
				{
					this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), base.transform);
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(gameObject.name, false);
					this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
					Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
					component.cullingType = AnimationCullingType.AlwaysAnimate;
					this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
					MeigeAnimClipHolder componentInChildren = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
					this.m_MeigeAnimCtrl.Open();
					this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, componentInChildren);
					this.m_MeigeAnimCtrl.Close();
					int layer = LayerMask.NameToLayer("UI");
					this.SetLayerRecursively(this.m_BodyObj, layer);
					int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
					for (int i = 0; i < particleEmitterNum; i++)
					{
						MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(i);
						if (particleEmitter != null)
						{
							particleEmitter.SetLayer(layer);
							particleEmitter.SetSortingOrder(this.m_SortingOrder);
						}
					}
					Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
					if (rendererCache != null)
					{
						for (int j = 0; j < rendererCache.Length; j++)
						{
							rendererCache[j].sortingOrder = this.m_SortingOrder;
						}
					}
					this.m_BodyObj.SetActive(false);
				}
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.UnloadAll(false);
					this.m_LoadingHndl = null;
				}
				this.m_IsDonePrepare = true;
				this.SetStep(MixWpnUpgradeEffectScene.eStep.Wait);
			}
		}

		// Token: 0x06001658 RID: 5720 RVA: 0x00075044 File Offset: 0x00073444
		public void Prepare(int sortingOrder)
		{
			if (this.m_IsDonePrepare)
			{
				return;
			}
			this.m_SortingOrder = sortingOrder;
			this.m_LoadingHndl = this.m_Loader.Load("mix/mix_wpnupgrade.muast", new MeigeResource.Option[0]);
			this.SetStep(MixWpnUpgradeEffectScene.eStep.Prepare_Wait);
		}

		// Token: 0x06001659 RID: 5721 RVA: 0x0007507C File Offset: 0x0007347C
		public bool IsCompletePrepare()
		{
			return this.m_IsDonePrepare;
		}

		// Token: 0x0600165A RID: 5722 RVA: 0x00075084 File Offset: 0x00073484
		public bool IsPrepareError()
		{
			return this.m_Step == MixWpnUpgradeEffectScene.eStep.Prepare_Error;
		}

		// Token: 0x0600165B RID: 5723 RVA: 0x00075090 File Offset: 0x00073490
		public void Play()
		{
			if (!this.m_IsDonePrepare)
			{
				return;
			}
			this.m_BodyObj.transform.localPosZ(0f);
			this.m_BodyObj.SetActive(true);
			this.m_BodyObj.transform.localPosition = Vector3.zero;
			this.m_BodyObj.transform.localScale = Vector3.one * 100f;
			this.m_MsbHndl.SetDefaultVisibility();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
			this.SetStep(MixWpnUpgradeEffectScene.eStep.Play_Wait);
		}

		// Token: 0x0600165C RID: 5724 RVA: 0x00075156 File Offset: 0x00073556
		public bool IsCompletePlay()
		{
			return this.m_Step == MixWpnUpgradeEffectScene.eStep.None;
		}

		// Token: 0x0600165D RID: 5725 RVA: 0x00075161 File Offset: 0x00073561
		public void Destroy()
		{
			this.m_IsDonePrepare = false;
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			this.m_BodyObj = null;
		}

		// Token: 0x0600165E RID: 5726 RVA: 0x00075186 File Offset: 0x00073586
		private void SetStep(MixWpnUpgradeEffectScene.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x0600165F RID: 5727 RVA: 0x00075190 File Offset: 0x00073590
		private void SetLayerRecursively(GameObject self, int layer)
		{
			self.layer = layer;
			IEnumerator enumerator = self.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					this.SetLayerRecursively(transform.gameObject, layer);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x06001660 RID: 5728 RVA: 0x00075204 File Offset: 0x00073604
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x04001D0B RID: 7435
		private const string LAYER_MASK = "UI";

		// Token: 0x04001D0C RID: 7436
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04001D0D RID: 7437
		private const string RESOURCE_PATH = "mix/mix_wpnupgrade.muast";

		// Token: 0x04001D0E RID: 7438
		private const string OBJ_MODEL_PATH = "Mix_WpnUpgrade";

		// Token: 0x04001D0F RID: 7439
		private const string OBJ_ANIM_PATH = "MeigeAC_Mix_WpnUpgrade@Take 001";

		// Token: 0x04001D10 RID: 7440
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04001D11 RID: 7441
		private MixWpnUpgradeEffectScene.eStep m_Step = MixWpnUpgradeEffectScene.eStep.None;

		// Token: 0x04001D12 RID: 7442
		private bool m_IsDonePrepare;

		// Token: 0x04001D13 RID: 7443
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x04001D14 RID: 7444
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04001D15 RID: 7445
		private Transform m_BaseTransform;

		// Token: 0x04001D16 RID: 7446
		private MsbHandler m_MsbHndl;

		// Token: 0x04001D17 RID: 7447
		private GameObject m_BodyObj;

		// Token: 0x04001D18 RID: 7448
		private int m_SortingOrder;

		// Token: 0x0200047A RID: 1146
		private enum eStep
		{
			// Token: 0x04001D1A RID: 7450
			None = -1,
			// Token: 0x04001D1B RID: 7451
			Prepare_Wait,
			// Token: 0x04001D1C RID: 7452
			Prepare_Error,
			// Token: 0x04001D1D RID: 7453
			Wait,
			// Token: 0x04001D1E RID: 7454
			Play_Wait
		}
	}
}
