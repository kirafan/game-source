﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001FA RID: 506
	public class RoomShopListDB : ScriptableObject
	{
		// Token: 0x04000C49 RID: 3145
		public RoomShopListDB_Param[] m_Params;
	}
}
