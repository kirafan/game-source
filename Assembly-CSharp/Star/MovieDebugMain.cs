﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000286 RID: 646
	public class MovieDebugMain : MonoBehaviour
	{
		// Token: 0x06000C15 RID: 3093 RVA: 0x00046E45 File Offset: 0x00045245
		private void Start()
		{
			this.m_Step = MovieDebugMain.eStep.First;
			this.AcquireManager();
			this.Start2();
		}

		// Token: 0x06000C16 RID: 3094 RVA: 0x00046E5B File Offset: 0x0004525B
		protected virtual void Initialize()
		{
			this.GetManager().Initialize();
			this.m_DebugParam[0] = new MovieDebugMain.DebugParam();
			if (this.m_MovieNameText != null)
			{
				this.ToggleMovie(true);
			}
			else
			{
				this.m_Step = MovieDebugMain.eStep.Main;
			}
		}

		// Token: 0x06000C17 RID: 3095 RVA: 0x00046E99 File Offset: 0x00045299
		protected virtual void Prepare_Wait()
		{
			if (!this.m_MovieHandle.IsCompletePrepare())
			{
				return;
			}
			this.m_Step = MovieDebugMain.eStep.Main;
		}

		// Token: 0x06000C18 RID: 3096 RVA: 0x00046EB4 File Offset: 0x000452B4
		private void Update()
		{
			MovieDebugMain.eStep step = this.m_Step;
			if (step != MovieDebugMain.eStep.First)
			{
				if (step != MovieDebugMain.eStep.Prepare_Wait)
				{
					if (step != MovieDebugMain.eStep.Main)
					{
					}
				}
				else
				{
					this.Prepare_Wait();
				}
			}
			else if (this.m_PrepareStep == MovieDebugMain.ePrepareStep.End)
			{
				this.Initialize();
			}
			this.Update2();
			MovieDebugMain.eStep nowViewStep = this.m_NowViewStep;
			if (nowViewStep != MovieDebugMain.eStep.Prepare_Wait)
			{
				if (nowViewStep == MovieDebugMain.eStep.Main)
				{
					if (this.m_PrepareStep == MovieDebugMain.ePrepareStep.End)
					{
						if (!string.IsNullOrEmpty(this.m_NowViewMovie))
						{
							this.GetManager().Prepare(this.m_NowViewHandle, this.m_NowViewMovie, new MovieVolumeInformation(this.m_Volume), false, true, this.m_MovieTarget, null);
							this.m_NowViewMovie = null;
							this.m_NowViewStep = MovieDebugMain.eStep.Prepare_Wait;
						}
					}
				}
			}
			else
			{
				if (!this.m_NowViewHandle.IsCompletePrepare())
				{
					return;
				}
				this.m_NowViewHandle.Play();
				this.m_NowViewStep = MovieDebugMain.eStep.Main;
			}
		}

		// Token: 0x06000C19 RID: 3097 RVA: 0x00046FB6 File Offset: 0x000453B6
		private void ToggleMovie(bool isRight)
		{
			this.ToggleMovie((!isRight) ? -1 : 1);
		}

		// Token: 0x06000C1A RID: 3098 RVA: 0x00046FCC File Offset: 0x000453CC
		private void ToggleMovie(int moveIndex)
		{
			MovieDebugMain.DebugParam debugParam = this.m_DebugParam[0];
			int index = debugParam.m_Index;
			int dictionarySize = this.GetManager().GetDictionarySize();
			debugParam.m_Index += moveIndex;
			if (debugParam.m_Index >= dictionarySize)
			{
				debugParam.m_Index = 0;
			}
			if (debugParam.m_Index < 0)
			{
				debugParam.m_Index = dictionarySize - 1;
			}
			int i = debugParam.m_Index;
			this.GetManager().RemoveHndl(this.m_MovieHandle, true);
			bool flag = this.GetManager().Prepare(this.m_MovieHandle, this.GetManager().GetMoviePath(i), new MovieVolumeInformation(this.m_Volume), true, true, this.m_MovieTarget, null);
			if (flag)
			{
				this.m_Step = MovieDebugMain.eStep.Prepare_Wait;
			}
			else
			{
				debugParam.m_Index = index;
				i = index;
			}
			if (this.m_MovieNameText != null)
			{
				this.m_MovieNameText.text = this.GetManager().GetMoviePath(i);
			}
		}

		// Token: 0x06000C1B RID: 3099 RVA: 0x000470BB File Offset: 0x000454BB
		public void OnClickPlay()
		{
			if (this.m_Step != MovieDebugMain.eStep.Main)
			{
				return;
			}
			this.GetManager().Play(this.m_MovieHandle, this.GetManager().GetMoviePath(this.m_DebugParam[0].m_Index), null, false, false, null, null);
		}

		// Token: 0x06000C1C RID: 3100 RVA: 0x000470FC File Offset: 0x000454FC
		public void PlayStop(MovieObjectHandler hndl = null, string moviePath = "Movie_opening.usm", MovieVolumeInformation volume = null, bool loop = false, bool additiveMode = false, Graphic image = null, Material material = null)
		{
			if (this.m_Step != MovieDebugMain.eStep.Main)
			{
				return;
			}
			if (hndl.IsPlaying(false))
			{
				this.GetManager().RemoveHndl(hndl, true);
			}
			else
			{
				this.GetManager().Play(hndl, moviePath, volume, loop, additiveMode, image, material);
			}
		}

		// Token: 0x06000C1D RID: 3101 RVA: 0x0004714B File Offset: 0x0004554B
		public void OnClickLeft()
		{
			if (this.m_Step != MovieDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleMovie(false);
		}

		// Token: 0x06000C1E RID: 3102 RVA: 0x00047161 File Offset: 0x00045561
		public void OnClickRight()
		{
			if (this.m_Step != MovieDebugMain.eStep.Main)
			{
				return;
			}
			this.ToggleMovie(true);
		}

		// Token: 0x06000C1F RID: 3103 RVA: 0x00047178 File Offset: 0x00045578
		public void OnChangedVolume(float value)
		{
			if (this.m_Step != MovieDebugMain.eStep.Main)
			{
				return;
			}
			float value2 = this.m_SoundSlider.value;
			this.m_Volume = value2;
			if (this.m_MovieHandle != null)
			{
				this.m_MovieHandle.SetVolume(value2);
			}
		}

		// Token: 0x06000C20 RID: 3104 RVA: 0x000471BD File Offset: 0x000455BD
		protected MovieManager GetManager()
		{
			this.AcquireManager();
			return this.MovieMng;
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x06000C21 RID: 3105 RVA: 0x000471CC File Offset: 0x000455CC
		private MovieManager MovieMng
		{
			get
			{
				return this.m_MovieManager;
			}
		}

		// Token: 0x06000C22 RID: 3106 RVA: 0x000471D4 File Offset: 0x000455D4
		protected bool AcquireManager()
		{
			if (this.m_MovieManager != null)
			{
				return true;
			}
			this.m_MovieManager = new MovieManager();
			if (this.m_MovieManager != null)
			{
			}
			return this.m_MovieManager != null;
		}

		// Token: 0x06000C23 RID: 3107 RVA: 0x00047205 File Offset: 0x00045605
		private void Start2()
		{
			this.m_PrepareStep = MovieDebugMain.ePrepareStep.GameSystemPrepare;
		}

		// Token: 0x06000C24 RID: 3108 RVA: 0x0004720E File Offset: 0x0004560E
		private void Update2()
		{
			this.GetManager().Update();
			this.UpdatePrepare();
		}

		// Token: 0x06000C25 RID: 3109 RVA: 0x00047224 File Offset: 0x00045624
		private void UpdatePrepare()
		{
			MovieDebugMain.ePrepareStep prepareStep = this.m_PrepareStep;
			if (prepareStep != MovieDebugMain.ePrepareStep.GameSystemPrepare)
			{
				if (prepareStep == MovieDebugMain.ePrepareStep.GameSystemPreparing)
				{
					this.GetManager().Setup(LocalSaveData.Inst);
					this.m_PrepareStep = MovieDebugMain.ePrepareStep.End;
				}
			}
			else
			{
				this.m_PrepareStep = MovieDebugMain.ePrepareStep.GameSystemPreparing;
			}
		}

		// Token: 0x06000C26 RID: 3110 RVA: 0x00047273 File Offset: 0x00045673
		protected void OnDestroy()
		{
			if (this.GetManager() != null)
			{
				this.GetManager().Destroy();
			}
		}

		// Token: 0x06000C27 RID: 3111 RVA: 0x0004728B File Offset: 0x0004568B
		public void OnDestroyTest()
		{
			this.OnDestroy();
		}

		// Token: 0x040014B4 RID: 5300
		[SerializeField]
		private Graphic m_MovieTarget;

		// Token: 0x040014B5 RID: 5301
		[SerializeField]
		private string m_NowViewMovie;

		// Token: 0x040014B6 RID: 5302
		[SerializeField]
		private Text m_MovieNameText;

		// Token: 0x040014B7 RID: 5303
		[SerializeField]
		private Slider m_SoundSlider;

		// Token: 0x040014B8 RID: 5304
		private MovieObjectHandler m_NowViewHandle = new MovieObjectHandler();

		// Token: 0x040014B9 RID: 5305
		private MovieObjectHandler m_MovieHandle = new MovieObjectHandler();

		// Token: 0x040014BA RID: 5306
		private float m_Volume = 1f;

		// Token: 0x040014BB RID: 5307
		private MovieDebugMain.DebugParam[] m_DebugParam = new MovieDebugMain.DebugParam[1];

		// Token: 0x040014BC RID: 5308
		private MovieDebugMain.eStep m_Step = MovieDebugMain.eStep.None;

		// Token: 0x040014BD RID: 5309
		private MovieDebugMain.eStep m_NowViewStep = MovieDebugMain.eStep.Main;

		// Token: 0x040014BE RID: 5310
		private MovieManager m_MovieManager;

		// Token: 0x040014BF RID: 5311
		private MovieDebugMain.ePrepareStep m_PrepareStep = MovieDebugMain.ePrepareStep.None;

		// Token: 0x02000287 RID: 647
		private class DebugParam
		{
			// Token: 0x06000C28 RID: 3112 RVA: 0x00047293 File Offset: 0x00045693
			public DebugParam()
			{
				this.m_Index = -1;
			}

			// Token: 0x040014C0 RID: 5312
			public int m_Index;
		}

		// Token: 0x02000288 RID: 648
		private enum eStep
		{
			// Token: 0x040014C2 RID: 5314
			None = -1,
			// Token: 0x040014C3 RID: 5315
			First,
			// Token: 0x040014C4 RID: 5316
			Prepare,
			// Token: 0x040014C5 RID: 5317
			Prepare_Wait,
			// Token: 0x040014C6 RID: 5318
			CueSheetPrepare,
			// Token: 0x040014C7 RID: 5319
			CueSheetPrepare_Wait,
			// Token: 0x040014C8 RID: 5320
			Main,
			// Token: 0x040014C9 RID: 5321
			Destroy_0,
			// Token: 0x040014CA RID: 5322
			Destroy_1
		}

		// Token: 0x02000289 RID: 649
		private enum ePrepareStep
		{
			// Token: 0x040014CC RID: 5324
			None = -1,
			// Token: 0x040014CD RID: 5325
			Start,
			// Token: 0x040014CE RID: 5326
			Login,
			// Token: 0x040014CF RID: 5327
			Login_Wait,
			// Token: 0x040014D0 RID: 5328
			GameSystemPrepare,
			// Token: 0x040014D1 RID: 5329
			GameSystemPreparing,
			// Token: 0x040014D2 RID: 5330
			End
		}
	}
}
