﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B6 RID: 1462
	public class RoomCharaActMove : IRoomCharaAct
	{
		// Token: 0x06001C57 RID: 7255 RVA: 0x00097A68 File Offset: 0x00095E68
		public override bool IsMoveCost(RoomObjectCtrlChara pbase)
		{
			int cost = pbase.GetAStar().GetCost(this.m_MoveBase);
			int cost2 = pbase.GetAStar().GetCost(this.m_MoveTarget);
			return cost != 1 || cost2 != 1;
		}

		// Token: 0x06001C58 RID: 7256 RVA: 0x00097AB0 File Offset: 0x00095EB0
		public bool RequestMoveMode(RoomObjectCtrlChara pbase, Vector2 fendPos)
		{
			Vector2 pos = pbase.BlockData.Pos;
			this.m_Paths = pbase.GetAStar().Find(pos, fendPos);
			if (this.m_Paths != null)
			{
				this.m_PathIndex = 0;
				this.m_MoveTime = 1.24226f;
				this.m_MoveBase = this.m_Paths[0];
				if (this.m_Paths.Count > 1)
				{
					this.m_MoveTarget = this.m_Paths[1];
				}
				else
				{
					this.m_MoveTarget = this.m_MoveBase;
				}
				pbase.PlayMotion(RoomDefine.eAnim.Walk, WrapMode.Loop);
				pbase.SetBlink(true);
				return true;
			}
			return false;
		}

		// Token: 0x06001C59 RID: 7257 RVA: 0x00097B54 File Offset: 0x00095F54
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool flag = false;
			int i = 4;
			this.m_MoveTime -= Time.deltaTime;
			bool result = true;
			while (i > 0)
			{
				if (this.m_Paths == null || this.m_PathIndex >= this.m_Paths.Count - 1)
				{
					flag = true;
				}
				if (flag)
				{
					pbase.SetBlink(false);
					result = false;
					break;
				}
				pbase.GetHandle().CacheTransform.localPosition.z = 0f;
				bool flag2 = false;
				if (this.m_MoveTime <= 0f)
				{
					this.m_MoveTime += 1.24226f;
					Vector3 localPosition = RoomUtility.GridToTilePos(this.m_MoveTarget.x, this.m_MoveTarget.y);
					localPosition.z = 0f;
					pbase.GetHandle().CacheTransform.localPosition = localPosition;
					pbase.BlockData.SetPos(this.m_MoveTarget.x, this.m_MoveTarget.y);
					this.m_PathIndex++;
					if (pbase.IsIrqRequest())
					{
						flag2 = true;
					}
					else if (this.m_PathIndex + 1 < this.m_Paths.Count)
					{
						this.m_MoveBase = this.m_MoveTarget;
						this.m_MoveTarget = this.m_Paths[this.m_PathIndex + 1];
						int cost = pbase.GetAStar().GetCost(this.m_Paths[this.m_PathIndex + 1]);
						if (cost != 1)
						{
							flag2 = true;
						}
					}
				}
				else
				{
					float d = (1.24226f - this.m_MoveTime) / 1.24226f;
					Vector2 vector = (this.m_MoveTarget - this.m_MoveBase) * d + this.m_MoveBase;
					Vector3 localPosition = RoomUtility.GridToTilePos(vector.x, vector.y);
					localPosition.z = 0f;
					pbase.GetHandle().CacheTransform.localPosition = localPosition;
					pbase.BlockData.SetPos(vector.x, vector.y);
					i = 0;
				}
				if (flag2)
				{
					pbase.SetBlink(false);
					result = false;
					break;
				}
				if (this.m_MoveTarget.x > this.m_MoveBase.x || this.m_MoveTarget.y < this.m_MoveBase.y)
				{
					if (pbase.GetDir() != CharacterDefine.eDir.R)
					{
						pbase.SetBlink(true);
						pbase.SetDir(CharacterDefine.eDir.R);
						pbase.PlayMotion(RoomDefine.eAnim.Walk, WrapMode.Loop);
					}
				}
				else if (pbase.GetDir() != CharacterDefine.eDir.L)
				{
					pbase.SetBlink(true);
					pbase.SetDir(CharacterDefine.eDir.L);
					pbase.PlayMotion(RoomDefine.eAnim.Walk, WrapMode.Loop);
				}
				i--;
			}
			return result;
		}

		// Token: 0x0400233C RID: 9020
		private List<Vector2> m_Paths;

		// Token: 0x0400233D RID: 9021
		private int m_PathIndex;

		// Token: 0x0400233E RID: 9022
		private Vector2 m_MoveBase;

		// Token: 0x0400233F RID: 9023
		private Vector2 m_MoveTarget;

		// Token: 0x04002340 RID: 9024
		private float m_MoveTime;
	}
}
