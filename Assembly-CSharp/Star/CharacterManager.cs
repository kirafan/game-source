﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200014F RID: 335
	public sealed class CharacterManager
	{
		// Token: 0x06000958 RID: 2392 RVA: 0x0003BBC1 File Offset: 0x00039FC1
		public CharacterManager()
		{
			this.m_CharaList = new List<CharacterHandler>();
		}

		// Token: 0x06000959 RID: 2393 RVA: 0x0003BBD4 File Offset: 0x00039FD4
		public void Update()
		{
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x0003BBD6 File Offset: 0x00039FD6
		public bool IsAvailable()
		{
			return this.m_CharaList != null;
		}

		// Token: 0x0600095B RID: 2395 RVA: 0x0003BBE6 File Offset: 0x00039FE6
		public int GetCharaNum()
		{
			return this.m_CharaList.Count;
		}

		// Token: 0x0600095C RID: 2396 RVA: 0x0003BBF3 File Offset: 0x00039FF3
		public CharacterHandler GetCharaAt(int index)
		{
			if (index >= this.m_CharaList.Count || index < 0)
			{
				return null;
			}
			return this.m_CharaList[index];
		}

		// Token: 0x0600095D RID: 2397 RVA: 0x0003BC1C File Offset: 0x0003A01C
		public CharacterHandler FindChara(uint uniqueID)
		{
			for (int i = 0; i < this.m_CharaList.Count; i++)
			{
				if (this.m_CharaList[i].UniqueID == uniqueID)
				{
					return this.m_CharaList[i];
				}
			}
			return null;
		}

		// Token: 0x0600095E RID: 2398 RVA: 0x0003BC6A File Offset: 0x0003A06A
		public List<CharacterHandler> GetCharas()
		{
			return this.m_CharaList;
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x0003BC74 File Offset: 0x0003A074
		public void DestroyCharaAt(int index)
		{
			if (index >= 0 && index < this.m_CharaList.Count)
			{
				if (this.m_CharaList[index] != null)
				{
					this.m_CharaList[index].Destroy();
					if (this.m_CharaList[index].gameObject != null)
					{
						UnityEngine.Object.Destroy(this.m_CharaList[index].gameObject);
					}
				}
				this.m_CharaList.RemoveAt(index);
			}
		}

		// Token: 0x06000960 RID: 2400 RVA: 0x0003BCFF File Offset: 0x0003A0FF
		public void DestroyChara(CharacterHandler charaHndl)
		{
			if (charaHndl == null)
			{
				return;
			}
			charaHndl.Destroy();
			UnityEngine.Object.Destroy(charaHndl.gameObject);
			this.m_CharaList.Remove(charaHndl);
		}

		// Token: 0x06000961 RID: 2401 RVA: 0x0003BD2C File Offset: 0x0003A12C
		public void DestroyCharaAll()
		{
			while (this.m_CharaList.Count > 0)
			{
				this.DestroyCharaAt(0);
			}
		}

		// Token: 0x06000962 RID: 2402 RVA: 0x0003BD4C File Offset: 0x0003A14C
		public CharacterHandler InstantiateCharacter(CharacterParam srcCharaParam, NamedParam srcNamedParam, WeaponParam srcWeaponParam, eCharaResourceType resourceType, int resourceID, CharacterDefine.eMode mode, bool isUserControll, CharacterDefine.eFriendType friendType, string name = "", Transform parent = null)
		{
			CharacterHandler characterHandler = this._InstantiateCharacter(name, parent);
			if (characterHandler != null)
			{
				characterHandler.SetupInInstantiate(srcCharaParam, srcNamedParam, srcWeaponParam, resourceType, resourceID, this.GetNewUniqueID(), mode, isUserControll, friendType);
				this.m_CharaList.Add(characterHandler);
			}
			return characterHandler;
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x0003BD98 File Offset: 0x0003A198
		private CharacterHandler _InstantiateCharacter(string name, Transform parent)
		{
			GameObject gameObject = new GameObject();
			if (gameObject == null)
			{
				return null;
			}
			CharacterHandler characterHandler = gameObject.AddComponent<CharacterHandler>();
			if (characterHandler == null)
			{
				return null;
			}
			if (string.IsNullOrEmpty(name))
			{
				gameObject.name = "CHARACTER";
			}
			else
			{
				gameObject.name = name;
			}
			characterHandler.CacheTransform.SetParent(parent, false);
			return characterHandler;
		}

		// Token: 0x06000964 RID: 2404 RVA: 0x0003BE00 File Offset: 0x0003A200
		private uint GetNewUniqueID()
		{
			return this.m_UniqueIDGenerator++;
		}

		// Token: 0x040008E9 RID: 2281
		private List<CharacterHandler> m_CharaList;

		// Token: 0x040008EA RID: 2282
		private uint m_UniqueIDGenerator;
	}
}
