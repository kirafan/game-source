﻿using System;

namespace Star
{
	// Token: 0x020001A5 RID: 421
	public static class SceneInfoListDB_Ext
	{
		// Token: 0x06000B10 RID: 2832 RVA: 0x00041E28 File Offset: 0x00040228
		public static SceneInfoListDB_Param GetParam(this SceneInfoListDB self, eSceneInfo sceneInfo)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_SceneID == (int)sceneInfo)
				{
					return self.m_Params[i];
				}
			}
			return default(SceneInfoListDB_Param);
		}
	}
}
