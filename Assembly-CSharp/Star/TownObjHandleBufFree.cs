﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006D6 RID: 1750
	public class TownObjHandleBufFree : ITownObjectHandler
	{
		// Token: 0x060022A3 RID: 8867 RVA: 0x000B942D File Offset: 0x000B782D
		public void SetupLink(int buildPointIndex, GameObject phitnode)
		{
			this.m_ObjID = -1;
			this.m_ManageID = -1L;
			this.m_BuildAccessID = buildPointIndex;
			this.m_HitNode = phitnode;
			base.LinkHitToNode(this.m_HitNode);
			this.m_IsAvailable = true;
		}

		// Token: 0x060022A4 RID: 8868 RVA: 0x000B9460 File Offset: 0x000B7860
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleBufFree.eState.Init:
				this.m_State = TownObjHandleBufFree.eState.Main;
				break;
			case TownObjHandleBufFree.eState.Sleep:
			{
				this.m_State = TownObjHandleBufFree.eState.Main;
				ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsMaker));
				if (action != null)
				{
					action.m_Active = false;
				}
				base.enabled = false;
				break;
			}
			case TownObjHandleBufFree.eState.WakeUp:
				this.m_HitNode.SetActive(true);
				this.m_State = TownObjHandleBufFree.eState.Main;
				break;
			case TownObjHandleBufFree.eState.EndStart:
				this.m_State = TownObjHandleBufFree.eState.End;
				break;
			case TownObjHandleBufFree.eState.End:
				base.Destroy();
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x060022A5 RID: 8869 RVA: 0x000B9518 File Offset: 0x000B7918
		public override void DestroyRequest()
		{
			this.m_State = TownObjHandleBufFree.eState.EndStart;
			this.m_HitNode.SetActive(false);
		}

		// Token: 0x060022A6 RID: 8870 RVA: 0x000B9530 File Offset: 0x000B7930
		public override bool RecvEvent(ITownHandleAction pact)
		{
			switch (pact.m_Type)
			{
			case eTownEventAct.OpenSelectUI:
			{
				TownPartsMaker paction = new TownPartsMaker(this.m_Builder, this.m_Transform, this.m_HitNode, this.m_LayerID + 10);
				this.m_Action.EntryAction(paction, 0);
				break;
			}
			case eTownEventAct.CloseSelectUI:
			{
				ITownPartsAction action = this.m_Action.GetAction(typeof(TownPartsMaker));
				if (action != null)
				{
					TownPartsMaker townPartsMaker = (TownPartsMaker)action;
					townPartsMaker.SetEndMark();
				}
				break;
			}
			case eTownEventAct.StepIn:
				this.m_State = TownObjHandleBufFree.eState.WakeUp;
				base.enabled = true;
				break;
			case eTownEventAct.StepOut:
				this.m_State = TownObjHandleBufFree.eState.Sleep;
				this.m_HitNode.SetActive(false);
				break;
			case eTownEventAct.StateChange:
				this.m_HitNode.SetActive(false);
				break;
			}
			return false;
		}

		// Token: 0x060022A7 RID: 8871 RVA: 0x000B9618 File Offset: 0x000B7A18
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				pbuilder.StackEventQue(new TownComUI(true)
				{
					m_SelectHandle = this,
					m_OpenUI = eTownUICategory.FreeBuf,
					m_BuildPoint = this.m_BuildAccessID
				});
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
			}
			return true;
		}

		// Token: 0x04002974 RID: 10612
		private GameObject m_HitNode;

		// Token: 0x04002975 RID: 10613
		public TownObjHandleBufFree.eState m_State;

		// Token: 0x020006D7 RID: 1751
		public enum eState
		{
			// Token: 0x04002977 RID: 10615
			Init,
			// Token: 0x04002978 RID: 10616
			Main,
			// Token: 0x04002979 RID: 10617
			Sleep,
			// Token: 0x0400297A RID: 10618
			WakeUp,
			// Token: 0x0400297B RID: 10619
			EndStart,
			// Token: 0x0400297C RID: 10620
			End
		}
	}
}
