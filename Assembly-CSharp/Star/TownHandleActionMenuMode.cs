﻿using System;

namespace Star
{
	// Token: 0x020006A9 RID: 1705
	public class TownHandleActionMenuMode : ITownHandleAction
	{
		// Token: 0x06002218 RID: 8728 RVA: 0x000B554C File Offset: 0x000B394C
		public TownHandleActionMenuMode(bool fmode)
		{
			this.m_Mode = fmode;
			this.m_Type = eTownEventAct.MenuMode;
		}

		// Token: 0x040028A0 RID: 10400
		public bool m_Mode;
	}
}
