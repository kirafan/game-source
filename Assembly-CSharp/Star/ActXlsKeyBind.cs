﻿using System;

namespace Star
{
	// Token: 0x02000566 RID: 1382
	public class ActXlsKeyBind : ActXlsKeyBase
	{
		// Token: 0x06001B09 RID: 6921 RVA: 0x0008F59C File Offset: 0x0008D99C
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_BindHrcName);
			pio.String(ref this.m_TargetName);
			pio.Int(ref this.m_LinkTime);
			this.m_Attach = (CActScriptKeyBind.eNameAttach)pio.Enum(this.m_Attach, typeof(CActScriptKeyBind.eNameAttach));
			this.m_CalcType = (CActScriptKeyBind.eCalc)pio.Enum(this.m_CalcType, typeof(CActScriptKeyBind.eCalc));
		}

		// Token: 0x040021F0 RID: 8688
		public string m_BindHrcName;

		// Token: 0x040021F1 RID: 8689
		public string m_TargetName;

		// Token: 0x040021F2 RID: 8690
		public CActScriptKeyBind.eNameAttach m_Attach;

		// Token: 0x040021F3 RID: 8691
		public int m_LinkTime;

		// Token: 0x040021F4 RID: 8692
		public CActScriptKeyBind.eCalc m_CalcType;
	}
}
