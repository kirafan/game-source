﻿using System;
using TownFacilityResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000692 RID: 1682
	public class TownComAPIObjBuyAll : INetComHandle
	{
		// Token: 0x060021C3 RID: 8643 RVA: 0x000B3B3A File Offset: 0x000B1F3A
		public TownComAPIObjBuyAll()
		{
			this.ApiName = "player/town_facility/buy_all";
			this.Request = true;
			this.ResponseType = typeof(Buyall);
		}

		// Token: 0x0400283E RID: 10302
		public TownFacilityBuyState[] townFacilityBuyStates;
	}
}
