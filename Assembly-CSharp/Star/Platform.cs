﻿using System;

namespace Star
{
	// Token: 0x0200065E RID: 1630
	public class Platform
	{
		// Token: 0x06002105 RID: 8453 RVA: 0x000AFE94 File Offset: 0x000AE294
		public static Platform.Type GetEnum()
		{
			return Platform.Type.Android;
		}

		// Token: 0x06002106 RID: 8454 RVA: 0x000AFEA6 File Offset: 0x000AE2A6
		public static int Get()
		{
			return (int)Platform.GetEnum();
		}

		// Token: 0x0200065F RID: 1631
		public enum Type
		{
			// Token: 0x04002730 RID: 10032
			Other,
			// Token: 0x04002731 RID: 10033
			iOS,
			// Token: 0x04002732 RID: 10034
			Android
		}
	}
}
