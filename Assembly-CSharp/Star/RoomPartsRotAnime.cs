﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000621 RID: 1569
	public class RoomPartsRotAnime : IRoomPartsAction
	{
		// Token: 0x06001E96 RID: 7830 RVA: 0x000A5A9D File Offset: 0x000A3E9D
		public RoomPartsRotAnime(Transform ptrs, Quaternion fbaserot, Quaternion ftargetrot, float ftime)
		{
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BaseRot = fbaserot;
			this.m_TargetRot = ftargetrot;
			this.m_Target = ptrs;
			this.m_Active = true;
		}

		// Token: 0x06001E97 RID: 7831 RVA: 0x000A5AD4 File Offset: 0x000A3ED4
		public override bool PartsUpdate()
		{
			if (this.m_Active)
			{
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Target.localRotation = this.m_TargetRot;
					this.m_Active = false;
				}
				else
				{
					float t = this.m_Time / this.m_MaxTime;
					this.m_Target.localRotation = Quaternion.Slerp(this.m_BaseRot, this.m_TargetRot, t);
				}
			}
			return this.m_Active;
		}

		// Token: 0x04002549 RID: 9545
		private Transform m_Target;

		// Token: 0x0400254A RID: 9546
		private Quaternion m_BaseRot;

		// Token: 0x0400254B RID: 9547
		private Quaternion m_TargetRot;

		// Token: 0x0400254C RID: 9548
		private float m_Time;

		// Token: 0x0400254D RID: 9549
		private float m_MaxTime;
	}
}
