﻿using System;
using Star.UI;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000437 RID: 1079
	public class MenuState_LibraryEvent : MenuState
	{
		// Token: 0x060014B8 RID: 5304 RVA: 0x0006D1F4 File Offset: 0x0006B5F4
		public MenuState_LibraryEvent(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014B9 RID: 5305 RVA: 0x0006D20C File Offset: 0x0006B60C
		public override int GetStateID()
		{
			return 13;
		}

		// Token: 0x060014BA RID: 5306 RVA: 0x0006D210 File Offset: 0x0006B610
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_LibraryEvent.eStep.First;
		}

		// Token: 0x060014BB RID: 5307 RVA: 0x0006D219 File Offset: 0x0006B619
		public override void OnStateExit()
		{
		}

		// Token: 0x060014BC RID: 5308 RVA: 0x0006D21B File Offset: 0x0006B61B
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014BD RID: 5309 RVA: 0x0006D224 File Offset: 0x0006B624
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_LibraryEvent.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_LibraryEvent.eStep.LoadWait;
				break;
			case MenuState_LibraryEvent.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryEvent.eStep.PlayIn;
				}
				break;
			case MenuState_LibraryEvent.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Owner.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Training);
					this.m_Step = MenuState_LibraryEvent.eStep.Main;
				}
				break;
			case MenuState_LibraryEvent.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam = null;
						this.m_NextState = 2147483646;
					}
					else if (this.m_UI.SelectADVID != -1)
					{
						if (string.IsNullOrEmpty(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(this.m_UI.SelectADVID).m_ScriptName))
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetMoviePlayParam(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(this.m_UI.SelectADVID).m_MovieFileName, SceneDefine.eSceneID.Menu);
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.MoviePlay;
							this.m_NextState = 2147483646;
						}
						else
						{
							GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
							globalParam.SetAdvParam(this.m_UI.SelectADVID, SceneDefine.eSceneID.Menu, false);
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
							this.m_NextState = 2147483646;
						}
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam = null;
						this.m_NextState = 4;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryEvent.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_LibraryEvent.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_LibraryEvent.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014BE RID: 5310 RVA: 0x0006D448 File Offset: 0x0006B848
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuLibraryEventUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.m_NpcUI);
			this.m_UI.OnTransit += this.OnTransit;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LibraryADV);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014BF RID: 5311 RVA: 0x0006D504 File Offset: 0x0006B904
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (isCallFromShortCut)
			{
				this.GoToMenuEnd();
			}
			else
			{
				bool flag = this.m_UI.ExecuteBack();
				if (flag)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
					this.GoToMenuEnd();
				}
			}
		}

		// Token: 0x060014C0 RID: 5312 RVA: 0x0006D550 File Offset: 0x0006B950
		private void OnTransit()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060014C1 RID: 5313 RVA: 0x0006D558 File Offset: 0x0006B958
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BA4 RID: 7076
		private MenuState_LibraryEvent.eStep m_Step = MenuState_LibraryEvent.eStep.None;

		// Token: 0x04001BA5 RID: 7077
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuLibraryEventUI;

		// Token: 0x04001BA6 RID: 7078
		private MenuLibraryEventUI m_UI;

		// Token: 0x02000438 RID: 1080
		private enum eStep
		{
			// Token: 0x04001BA8 RID: 7080
			None = -1,
			// Token: 0x04001BA9 RID: 7081
			First,
			// Token: 0x04001BAA RID: 7082
			LoadWait,
			// Token: 0x04001BAB RID: 7083
			PlayIn,
			// Token: 0x04001BAC RID: 7084
			Main,
			// Token: 0x04001BAD RID: 7085
			UnloadChildSceneWait
		}
	}
}
