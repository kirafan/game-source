﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000AD2 RID: 2770
	public class Fade : MonoBehaviour
	{
		// Token: 0x060039EA RID: 14826 RVA: 0x0012752A File Offset: 0x0012592A
		private void Start()
		{
			this.Init();
		}

		// Token: 0x060039EB RID: 14827 RVA: 0x00127534 File Offset: 0x00125934
		private void Init()
		{
			if (this.m_IsDoneInit)
			{
				return;
			}
			if (this.m_Fade == null)
			{
				this.m_Fade = base.GetComponent<Image>();
			}
			this.m_IsFading = false;
			this.SetAlphaToObj(0f);
			this.m_IsDoneInit = true;
		}

		// Token: 0x060039EC RID: 14828 RVA: 0x00127584 File Offset: 0x00125984
		private void SetAlphaToObj(float a)
		{
			this.SetColor(new Color(this.m_Fade.color.r, this.m_Fade.color.g, this.m_Fade.color.b, a));
		}

		// Token: 0x060039ED RID: 14829 RVA: 0x001275D6 File Offset: 0x001259D6
		public bool IsEnableRender()
		{
			return this.m_Fade.enabled;
		}

		// Token: 0x060039EE RID: 14830 RVA: 0x001275EB File Offset: 0x001259EB
		public bool IsComplete()
		{
			return !this.m_IsFading;
		}

		// Token: 0x060039EF RID: 14831 RVA: 0x001275F6 File Offset: 0x001259F6
		public void SetFadeRatio(float ratio)
		{
			if (!this.m_IsDoneInit)
			{
				this.Init();
			}
			this.m_Ratio = ratio;
			this.SetAlphaToObj(this.m_Ratio);
		}

		// Token: 0x060039F0 RID: 14832 RVA: 0x0012761C File Offset: 0x00125A1C
		public void SetColor(Color color)
		{
			this.m_Fade.color = color;
			if (color.a > 0f)
			{
				this.m_Fade.enabled = true;
			}
			else
			{
				this.m_Fade.enabled = false;
			}
		}

		// Token: 0x060039F1 RID: 14833 RVA: 0x00127658 File Offset: 0x00125A58
		public Color GetColor()
		{
			return this.m_Fade.color;
		}

		// Token: 0x060039F2 RID: 14834 RVA: 0x00127665 File Offset: 0x00125A65
		public Coroutine FadeOut(float time, Action action)
		{
			base.StopAllCoroutines();
			return base.StartCoroutine(this.FadeoutCoroutine(time, action));
		}

		// Token: 0x060039F3 RID: 14835 RVA: 0x0012767B File Offset: 0x00125A7B
		public Coroutine FadeOut(float time)
		{
			return this.FadeOut(time, null);
		}

		// Token: 0x060039F4 RID: 14836 RVA: 0x00127685 File Offset: 0x00125A85
		public Coroutine FadeIn(float time, Action action)
		{
			base.StopAllCoroutines();
			return base.StartCoroutine(this.FadeinCoroutine(time, action));
		}

		// Token: 0x060039F5 RID: 14837 RVA: 0x0012769B File Offset: 0x00125A9B
		public Coroutine FadeIn(float time)
		{
			return this.FadeIn(time, null);
		}

		// Token: 0x060039F6 RID: 14838 RVA: 0x001276A8 File Offset: 0x00125AA8
		private IEnumerator FadeinCoroutine(float time, Action action)
		{
			this.m_IsFading = true;
			float endTime = Time.timeSinceLevelLoad + time * this.m_Ratio;
			WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
			while (Time.timeSinceLevelLoad <= endTime)
			{
				if (time != 0f)
				{
					this.m_Ratio = (endTime - Time.timeSinceLevelLoad) / time;
				}
				else
				{
					this.m_Ratio = 0f;
				}
				this.SetAlphaToObj(this.m_Ratio);
				yield return endFrame;
			}
			this.m_IsFading = false;
			this.m_Ratio = 0f;
			this.SetAlphaToObj(this.m_Ratio);
			if (action != null)
			{
				action();
			}
			yield break;
		}

		// Token: 0x060039F7 RID: 14839 RVA: 0x001276D4 File Offset: 0x00125AD4
		private IEnumerator FadeoutCoroutine(float time, Action action)
		{
			this.m_IsFading = true;
			float endTime = Time.timeSinceLevelLoad + time * (1f - this.m_Ratio);
			WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
			while (Time.timeSinceLevelLoad <= endTime)
			{
				if (time != 0f)
				{
					this.m_Ratio = 1f - (endTime - Time.timeSinceLevelLoad) / time;
				}
				else
				{
					this.m_Ratio = 1f;
				}
				this.SetAlphaToObj(this.m_Ratio);
				yield return endFrame;
			}
			this.m_IsFading = false;
			this.m_Ratio = 1f;
			this.SetAlphaToObj(this.m_Ratio);
			if (action != null)
			{
				action();
			}
			yield break;
		}

		// Token: 0x060039F8 RID: 14840 RVA: 0x001276FD File Offset: 0x00125AFD
		private void OnValidate()
		{
			this.Init();
			this.SetAlphaToObj(this.m_Ratio);
		}

		// Token: 0x04004137 RID: 16695
		[SerializeField]
		[Range(0f, 1f)]
		private float m_Ratio;

		// Token: 0x04004138 RID: 16696
		private Image m_Fade;

		// Token: 0x04004139 RID: 16697
		private bool m_IsFading;

		// Token: 0x0400413A RID: 16698
		private bool m_IsDoneInit;
	}
}
