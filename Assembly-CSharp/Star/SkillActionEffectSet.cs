﻿using System;
using System.Collections;
using System.Collections.Generic;
using Star.UI.Battle;
using UnityEngine;

namespace Star
{
	// Token: 0x02000122 RID: 290
	public class SkillActionEffectSet
	{
		// Token: 0x06000779 RID: 1913 RVA: 0x0002CF38 File Offset: 0x0002B338
		public SkillActionEffectSet(CharacterHandler targetCharaHndl)
		{
			string[,] array = new string[6, 4];
			array[0, 0] = "ef_btl_fire_up_line";
			array[0, 1] = "ef_btl_fire_up_option_0";
			array[0, 2] = "ef_btl_up_ring";
			array[0, 3] = "ef_btl_up_option_1";
			array[1, 0] = "ef_btl_water_up_line";
			array[1, 1] = "ef_btl_water_up_option_0";
			array[1, 2] = "ef_btl_up_ring";
			array[1, 3] = "ef_btl_up_option_1";
			array[2, 0] = "ef_btl_earth_up_line";
			array[2, 1] = "ef_btl_earth_up_option_0";
			array[2, 2] = "ef_btl_up_ring";
			array[2, 3] = "ef_btl_up_option_1";
			array[3, 0] = "ef_btl_wind_up_line";
			array[3, 1] = "ef_btl_wind_up_option_0";
			array[3, 2] = "ef_btl_up_ring";
			array[3, 3] = "ef_btl_up_option_1";
			array[4, 0] = "ef_btl_moon_up_line";
			array[4, 1] = "ef_btl_moon_up_option_0";
			array[4, 2] = "ef_btl_up_ring";
			array[4, 3] = "ef_btl_up_option_1";
			array[5, 0] = "ef_btl_sun_up_line";
			array[5, 1] = "ef_btl_sun_up_option_0";
			array[5, 2] = "ef_btl_up_ring";
			array[5, 3] = "ef_btl_up_option_1";
			this.EffectID_ElementUp = array;
			string[,] array2 = new string[6, 3];
			array2[0, 0] = "ef_btl_debuff_line";
			array2[0, 1] = "ef_btl_debuff_ring";
			array2[0, 2] = "ef_btl_down";
			array2[1, 0] = "ef_btl_debuff_line";
			array2[1, 1] = "ef_btl_debuff_ring";
			array2[1, 2] = "ef_btl_down";
			array2[2, 0] = "ef_btl_debuff_line";
			array2[2, 1] = "ef_btl_debuff_ring";
			array2[2, 2] = "ef_btl_down";
			array2[3, 0] = "ef_btl_debuff_line";
			array2[3, 1] = "ef_btl_debuff_ring";
			array2[3, 2] = "ef_btl_down";
			array2[4, 0] = "ef_btl_debuff_line";
			array2[4, 1] = "ef_btl_debuff_ring";
			array2[4, 2] = "ef_btl_down";
			array2[5, 0] = "ef_btl_debuff_line";
			array2[5, 1] = "ef_btl_debuff_ring";
			array2[5, 2] = "ef_btl_down";
			this.EffectID_ElementDown = array2;
			this.m_OverlayNum = 1;
			base..ctor();
			this.m_TargetCharaHndl = targetCharaHndl;
			this.m_EffectTo = null;
			if (this.m_TargetCharaHndl.CharaAnim.ModelSets.Length > 1)
			{
				this.m_EffectTo = this.m_TargetCharaHndl.CharaAnim.ModelSets[1].m_RootTransform;
			}
			if (this.m_EffectTo == null)
			{
				this.m_EffectTo = this.m_TargetCharaHndl.CacheTransform;
			}
			this.m_ActiveEffectList = new List<EffectHandler>();
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x0600077A RID: 1914 RVA: 0x0002D1FE File Offset: 0x0002B5FE
		public CharacterHandler TargetCharaHndl
		{
			get
			{
				return this.m_TargetCharaHndl;
			}
		}

		// Token: 0x0600077B RID: 1915 RVA: 0x0002D208 File Offset: 0x0002B608
		public void Destroy()
		{
			if (this.m_ActiveEffectList != null)
			{
				for (int i = 0; i < this.m_ActiveEffectList.Count; i++)
				{
					SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(this.m_ActiveEffectList[i]);
					this.m_ActiveEffectList[i] = null;
				}
				this.m_ActiveEffectList.Clear();
			}
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x0002D26C File Offset: 0x0002B66C
		public void Update()
		{
			if (this.m_ActiveEffectList != null)
			{
				for (int i = this.m_ActiveEffectList.Count - 1; i >= 0; i--)
				{
					EffectHandler effectHandler = this.m_ActiveEffectList[i];
					if (!effectHandler.IsPlaying())
					{
						SingletonMonoBehaviour<EffectManager>.Inst.DestroyToCache(effectHandler);
						this.m_ActiveEffectList.RemoveAt(i);
					}
				}
			}
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x0002D2D4 File Offset: 0x0002B6D4
		public SkillActionEffectSet SetupTo_Recover(int opt_power)
		{
			SkillContentAndEffectCombiDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.Recover);
			int grade = SkillActionEffectSet.CalcGrade((float)opt_power, param.m_Thresholds);
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			this.m_EffectDatas = this.CreateEffectDatas_Recover(grade);
			return this;
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x0002D324 File Offset: 0x0002B724
		public SkillActionEffectSet SetupTo_StatusChange_Up(float[] opt_values)
		{
			SkillContentAndEffectCombiDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.StatusChange_Up);
			float num = Mathf.Abs(opt_values[0]);
			for (int i = 1; i < opt_values.Length; i++)
			{
				if (i != 4 && i != 5)
				{
					float num2 = Mathf.Abs(opt_values[i]);
					if (num < num2)
					{
						num = num2;
					}
				}
			}
			int num3 = 0;
			float num4 = num;
			for (int j = 2; j >= 0; j--)
			{
				if (num4 >= Mathf.Abs(param.m_Thresholds[j]))
				{
					num3 = j;
					break;
				}
			}
			int num5 = 0;
			float num6 = Mathf.Abs(opt_values[5]);
			if (num6 != 0f)
			{
				for (int k = 5; k >= 3; k--)
				{
					if (num6 >= Mathf.Abs(param.m_Thresholds[k]))
					{
						num5 = k - 3;
						break;
					}
				}
			}
			int num7 = 0;
			float num8 = Mathf.Abs(opt_values[4]);
			if (num8 != 0f)
			{
				for (int l = 8; l >= 6; l--)
				{
					if (num8 <= Mathf.Abs(param.m_Thresholds[l]))
					{
						num7 = l - 6;
						break;
					}
				}
			}
			num3 = Mathf.Max(num3, num5);
			num3 = Mathf.Max(num3, num7);
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			for (int m = 0; m < opt_values.Length; m++)
			{
				if (opt_values[m] != 0f)
				{
					int grade = 0;
					float num9 = Mathf.Abs(opt_values[m]);
					if (m == 5)
					{
						grade = num5;
					}
					else if (m == 4)
					{
						grade = num7;
					}
					else
					{
						for (int n = 2; n >= 0; n--)
						{
							if (num9 >= Mathf.Abs(param.m_Thresholds[n]))
							{
								grade = n;
								break;
							}
						}
					}
					this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.StatusAtkUp + m, grade, false));
				}
			}
			this.m_OverlayNum = num3 + 1;
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x0600077F RID: 1919 RVA: 0x0002D540 File Offset: 0x0002B940
		public SkillActionEffectSet SetupTo_StatusChange_Down(float[] opt_values)
		{
			SkillContentAndEffectCombiDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.StatusChange_Down);
			float num = Mathf.Abs(opt_values[0]);
			for (int i = 1; i < opt_values.Length; i++)
			{
				if (i != 4 && i != 5)
				{
					float num2 = Mathf.Abs(opt_values[i]);
					if (num < num2)
					{
						num = num2;
					}
				}
			}
			int num3 = 0;
			float num4 = num;
			for (int j = 2; j >= 0; j--)
			{
				if (num4 >= Mathf.Abs(param.m_Thresholds[j]))
				{
					num3 = j;
					break;
				}
			}
			int num5 = 0;
			float num6 = Mathf.Abs(opt_values[5]);
			if (num6 != 0f)
			{
				for (int k = 5; k >= 3; k--)
				{
					if (num6 >= Mathf.Abs(param.m_Thresholds[k]))
					{
						num5 = k - 3;
						break;
					}
				}
			}
			int num7 = 0;
			float num8 = Mathf.Abs(opt_values[4]);
			if (num8 != 0f)
			{
				for (int l = 8; l >= 6; l--)
				{
					if (num8 >= Mathf.Abs(param.m_Thresholds[l]))
					{
						num7 = l - 6;
						break;
					}
				}
			}
			num3 = Mathf.Max(num3, num5);
			num3 = Mathf.Max(num3, num7);
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			for (int m = 0; m < opt_values.Length; m++)
			{
				if (opt_values[m] != 0f)
				{
					int grade = 0;
					float num9 = Mathf.Abs(opt_values[m]);
					if (m == 5)
					{
						grade = num5;
					}
					else if (m == 4)
					{
						grade = num7;
					}
					else
					{
						for (int n = 2; n >= 0; n--)
						{
							if (num9 >= Mathf.Abs(param.m_Thresholds[n]))
							{
								grade = n;
								break;
							}
						}
					}
					this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.StatusAtkDown + m, grade, false));
				}
			}
			this.m_OverlayNum = num3 + 1;
			this.m_EffectDatas = this.CreateEffectDatas_Debuff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000780 RID: 1920 RVA: 0x0002D75C File Offset: 0x0002BB5C
		public SkillActionEffectSet SetupTo_Abnormal(eStateAbnormal opt_stateAbnormal)
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.Confusion + (int)opt_stateAbnormal, -1, false));
			this.m_EffectDatas = this.CreateEffectDatas_Abnormal(opt_stateAbnormal);
			return this;
		}

		// Token: 0x06000781 RID: 1921 RVA: 0x0002D793 File Offset: 0x0002BB93
		public SkillActionEffectSet SetupTo_AbnormalMiss()
		{
			this.m_OverlayNum = 0;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.None, -1, true));
			this.m_EffectDatas = null;
			return this;
		}

		// Token: 0x06000782 RID: 1922 RVA: 0x0002D7C2 File Offset: 0x0002BBC2
		public SkillActionEffectSet SetupTo_AbnormalRecover()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			this.m_EffectDatas = this.CreateEffectDatas_AbnormalRecover(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000783 RID: 1923 RVA: 0x0002D7E5 File Offset: 0x0002BBE5
		public SkillActionEffectSet SetupTo_AbnormalAdditionalProbability(bool opt_isUp)
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			if (opt_isUp)
			{
				this.m_EffectDatas = this.CreateEffectDatas_Abnormal(eStateAbnormal.Num);
			}
			else
			{
				this.m_EffectDatas = this.CreateEffectDatas_AbnormalRecover(this.m_OverlayNum);
			}
			return this;
		}

		// Token: 0x06000784 RID: 1924 RVA: 0x0002D820 File Offset: 0x0002BC20
		public SkillActionEffectSet SetupTo_ElementResist_Up(eElementType opt_elementType, float opt_value)
		{
			int num = SkillActionEffectSet.CalcGrade(opt_value, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.ElementResist_FireUp + (int)opt_elementType).m_Thresholds);
			this.m_OverlayNum = num + 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.ElementFireUp + (int)opt_elementType, num, false));
			this.m_EffectDatas = this.CreateEffectDatas_ElementUp(this.m_OverlayNum, opt_elementType);
			return this;
		}

		// Token: 0x06000785 RID: 1925 RVA: 0x0002D894 File Offset: 0x0002BC94
		public SkillActionEffectSet SetupTo_ElementResist_Down(eElementType opt_elementType, float opt_value)
		{
			int num = SkillActionEffectSet.CalcGrade(opt_value, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.ElementResist_FireDown + (int)opt_elementType).m_Thresholds);
			this.m_OverlayNum = num + 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.ElementFireDown + (int)opt_elementType, num, false));
			this.m_EffectDatas = this.CreateEffectDatas_ElementDown(this.m_OverlayNum, opt_elementType);
			return this;
		}

		// Token: 0x06000786 RID: 1926 RVA: 0x0002D906 File Offset: 0x0002BD06
		public SkillActionEffectSet SetupTo_ElementChange(eElementType opt_elementType)
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			this.m_EffectDatas = this.CreateEffectDatas_ElementUp(this.m_OverlayNum, opt_elementType);
			return this;
		}

		// Token: 0x06000787 RID: 1927 RVA: 0x0002D92C File Offset: 0x0002BD2C
		public SkillActionEffectSet SetupTo_WeakElementBonus(float opt_value)
		{
			int grade = SkillActionEffectSet.CalcGrade(opt_value, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.WeakElementBonus).m_Thresholds);
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.WeakElementBonus, grade, false));
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000788 RID: 1928 RVA: 0x0002D998 File Offset: 0x0002BD98
		public SkillActionEffectSet SetupTo_NextAttackUp(bool opt_isAtk, float opt_value)
		{
			int num = SkillActionEffectSet.CalcGrade(opt_value, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.NextAttackUp).m_Thresholds);
			this.m_OverlayNum = num + 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			if (opt_isAtk)
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.NextAtkUp, num, false));
			}
			else
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.NextMgcUp, num, false));
			}
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000789 RID: 1929 RVA: 0x0002DA24 File Offset: 0x0002BE24
		public SkillActionEffectSet SetupTo_NextAttackCritical()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.NextCritical, -1, false));
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x0600078A RID: 1930 RVA: 0x0002DA5F File Offset: 0x0002BE5F
		public SkillActionEffectSet SetupTo_Barrier()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.Barrier, -1, false));
			this.m_EffectDatas = this.CreateEffectDatas_Barrier();
			return this;
		}

		// Token: 0x0600078B RID: 1931 RVA: 0x0002DA94 File Offset: 0x0002BE94
		public SkillActionEffectSet SetupTo_RecastChange(bool opt_isUp)
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			if (opt_isUp)
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.RecastUp, -1, false));
				this.m_EffectDatas = this.CreateEffectDatas_Debuff(this.m_OverlayNum);
			}
			else
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.RecastDown, -1, false));
				this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			}
			return this;
		}

		// Token: 0x0600078C RID: 1932 RVA: 0x0002DB0B File Offset: 0x0002BF0B
		public SkillActionEffectSet SetupTo_KiraraJumpGaugeChange()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x0600078D RID: 1933 RVA: 0x0002DB30 File Offset: 0x0002BF30
		public SkillActionEffectSet SetupTo_HateChange(bool opt_isUp, float opt_value)
		{
			int grade = SkillActionEffectSet.CalcGrade(opt_value, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentAndEffectCombiDB.GetParam(eSkillContentAndEffectCombiDB.HateChange).m_Thresholds);
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			if (opt_isUp)
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.HateUp, grade, false));
				this.m_EffectDatas = this.CreateEffectDatas_HateUp();
			}
			else
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.HateDown, grade, false));
				this.m_EffectDatas = this.CreateEffectDatas_HateDown();
			}
			return this;
		}

		// Token: 0x0600078E RID: 1934 RVA: 0x0002DBC0 File Offset: 0x0002BFC0
		public SkillActionEffectSet SetupTo_ChargeChange(bool opt_isUp)
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			if (opt_isUp)
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.ChargeCountUp, -1, false));
				this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			}
			else
			{
				this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.ChargeCountDown, -1, false));
				this.m_EffectDatas = this.CreateEffectDatas_Debuff(this.m_OverlayNum);
			}
			return this;
		}

		// Token: 0x0600078F RID: 1935 RVA: 0x0002DC37 File Offset: 0x0002C037
		public SkillActionEffectSet SetupTo_ChainCoefChange()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.ChainBonusUp, -1, false));
			this.m_EffectDatas = this.CreateEffectDatas_Buff(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000790 RID: 1936 RVA: 0x0002DC72 File Offset: 0x0002C072
		public SkillActionEffectSet SetupTo_StunRecover()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = null;
			this.m_EffectDatas = this.CreateEffectDatas_AbnormalRecover(this.m_OverlayNum);
			return this;
		}

		// Token: 0x06000791 RID: 1937 RVA: 0x0002DC95 File Offset: 0x0002C095
		public SkillActionEffectSet SetupTo_Regene()
		{
			this.m_OverlayNum = 1;
			this.m_IconDatas = new List<SkillActionEffectSet.IconData>();
			this.m_IconDatas.Add(new SkillActionEffectSet.IconData(ePopUpStateIconType.Regene, -1, false));
			this.m_EffectDatas = this.CreateEffectDatas_Recover(0);
			return this;
		}

		// Token: 0x06000792 RID: 1938 RVA: 0x0002DCCC File Offset: 0x0002C0CC
		private static int CalcGrade(float src, float[] thresholds)
		{
			src = Mathf.Abs(src);
			for (int i = thresholds.Length - 1; i >= 0; i--)
			{
				if (src >= Mathf.Abs(thresholds[i]))
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x06000793 RID: 1939 RVA: 0x0002DD08 File Offset: 0x0002C108
		public IEnumerator Play(Camera camera, Transform parent, BattleUI battleUI)
		{
			this.m_UI = battleUI;
			this.m_IsDoneEffectDatas = false;
			Transform uiPopupTo;
			float popupOffsetYfromTargetTransform;
			this.m_TargetCharaHndl.CharaAnim.GetLocatorPos(CharacterDefine.eLocatorIndex.Body, false, out uiPopupTo, out popupOffsetYfromTargetTransform);
			Transform camTransform = camera.transform;
			for (;;)
			{
				bool isComplete = true;
				if (this.m_EffectDatas != null)
				{
					for (int i = 0; i < this.m_EffectDatas.Count; i++)
					{
						SkillActionEffectSet.EffectData effectData = this.m_EffectDatas[i];
						if (!effectData.m_IsPlayed)
						{
							effectData.m_DelaySec -= Time.deltaTime;
							if (effectData.m_DelaySec <= 0f)
							{
								Vector3 vector = this.m_EffectTo.position;
								vector += new Vector3(effectData.m_Offset.x * parent.lossyScale.x, effectData.m_Offset.y * parent.lossyScale.x, 0f);
								int sortingOrder = 1000;
								SkillActionUtility.CorrectEffectParam(camera, effectData.m_EffectID, ref vector, ref sortingOrder);
								Vector3 b = camTransform.TransformDirection(0f, 0f, effectData.m_Offset.z);
								vector += b;
								Quaternion rotation = (camTransform.forward.z <= 0f) ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f);
								EffectHandler effectHandler = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged(effectData.m_EffectID, vector, rotation, parent, false, WrapMode.ClampForever, sortingOrder, effectData.m_AnimSpeed, true);
								if (effectHandler != null)
								{
									effectHandler.SetScale(effectData.m_Scale);
									if (effectData.m_ArgIndexCtrlParam_Visible != -1)
									{
										effectHandler.ApplyControllParam_Visible(effectData.m_ArgIndexCtrlParam_Visible);
									}
									if (effectData.m_ArgIndexCtrlParam_Color != -1)
									{
										effectHandler.ApplyControllParam_Color(effectData.m_ArgIndexCtrlParam_Color);
									}
									this.m_ActiveEffectList.Add(effectHandler);
								}
								if (effectData.m_IsPopupIcon && this.m_IconDatas != null && this.m_IconDatas.Count > 0)
								{
									battleUI.PopUpIcon(this.m_IconDatas, uiPopupTo, popupOffsetYfromTargetTransform);
								}
								effectData.m_IsPlayed = true;
							}
						}
						if (!effectData.m_IsPlayed)
						{
							isComplete = false;
						}
					}
				}
				else if (this.m_IconDatas != null && this.m_IconDatas.Count > 0)
				{
					battleUI.PopUpIcon(this.m_IconDatas, uiPopupTo, popupOffsetYfromTargetTransform);
				}
				if (isComplete)
				{
					break;
				}
				yield return null;
			}
			this.m_IsDoneEffectDatas = true;
			yield break;
			yield break;
		}

		// Token: 0x06000794 RID: 1940 RVA: 0x0002DD38 File Offset: 0x0002C138
		public bool IsPlaying()
		{
			if (!this.m_IsDoneEffectDatas)
			{
				return true;
			}
			if (this.m_ActiveEffectList != null)
			{
				for (int i = 0; i < this.m_ActiveEffectList.Count; i++)
				{
					if (this.m_ActiveEffectList[i].IsPlaying())
					{
						return true;
					}
				}
			}
			return this.m_UI != null && this.m_UI.IsPlayingPopUpIcon();
		}

		// Token: 0x06000795 RID: 1941 RVA: 0x0002DDB8 File Offset: 0x0002C1B8
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Buff(int overlayNum)
		{
			List<SkillActionEffectSet.EffectData> list = new List<SkillActionEffectSet.EffectData>();
			list.Add(new SkillActionEffectSet.EffectData("ef_btl_buff_line", 0.6f, 0f, Vector3.zero, 0.8f, false, -1, -1));
			overlayNum = Mathf.Clamp(overlayNum, 1, 3);
			for (int i = 0; i < overlayNum; i++)
			{
				if (i != 0)
				{
					if (i != 1)
					{
						if (i == 2)
						{
							list.Add(new SkillActionEffectSet.EffectData("ef_btl_buff_ring", 0.65f, 0.5f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, false, -1, -1));
						}
					}
					else
					{
						list.Add(new SkillActionEffectSet.EffectData("ef_btl_buff_ring", 0.65f, 0.3f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, false, -1, -1));
					}
				}
				else
				{
					list.Add(new SkillActionEffectSet.EffectData("ef_btl_buff_ring", 0.65f, 0.1f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, true, -1, -1));
				}
			}
			return list;
		}

		// Token: 0x06000796 RID: 1942 RVA: 0x0002DEE4 File Offset: 0x0002C2E4
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Debuff(int overlayNum)
		{
			List<SkillActionEffectSet.EffectData> list = new List<SkillActionEffectSet.EffectData>();
			list.Add(new SkillActionEffectSet.EffectData("ef_btl_debuff_line", 0.6f, 0f, Vector3.zero, 0.8f, false, -1, -1));
			overlayNum = Mathf.Clamp(overlayNum, 1, 3);
			for (int i = 0; i < overlayNum; i++)
			{
				if (i != 0)
				{
					if (i != 1)
					{
						if (i == 2)
						{
							list.Add(new SkillActionEffectSet.EffectData("ef_btl_debuff_ring", 0.65f, 0.5f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, false, -1, -1));
						}
					}
					else
					{
						list.Add(new SkillActionEffectSet.EffectData("ef_btl_debuff_ring", 0.65f, 0.3f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, false, -1, -1));
					}
				}
				else
				{
					list.Add(new SkillActionEffectSet.EffectData("ef_btl_debuff_ring", 0.65f, 0.1f, new Vector3(0f, -0.03f, -1.5f * (float)i), 0.8f, true, -1, -1));
				}
			}
			return list;
		}

		// Token: 0x06000797 RID: 1943 RVA: 0x0002E010 File Offset: 0x0002C410
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Recover(int grade)
		{
			return new List<SkillActionEffectSet.EffectData>
			{
				new SkillActionEffectSet.EffectData(string.Format("ef_btl_recover_{0:D2}", grade), 1f, 0f, Vector3.zero, 1f, true, -1, -1)
			};
		}

		// Token: 0x06000798 RID: 1944 RVA: 0x0002E058 File Offset: 0x0002C458
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_ElementUp(int overlayNum, eElementType elementType)
		{
			List<SkillActionEffectSet.EffectData> list = new List<SkillActionEffectSet.EffectData>();
			if (elementType != eElementType.None)
			{
				list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 0], 0.8f, 0f, Vector3.zero, 1f, false, -1, -1));
				list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 1], 0.8f, 0f, Vector3.zero, 1f, false, -1, -1));
				overlayNum = Mathf.Clamp(overlayNum, 1, 3);
				for (int i = 0; i < overlayNum; i++)
				{
					if (i != 0)
					{
						if (i != 1)
						{
							if (i == 2)
							{
								list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 2], 1.2f, 0.3f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, false, -1, (int)elementType));
							}
						}
						else
						{
							list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 2], 1.2f, 0.15f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, false, -1, (int)elementType));
						}
					}
					else
					{
						list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 2], 1.2f, 0f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, true, -1, (int)elementType));
					}
				}
				list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementUp[(int)elementType, 3], 1f, 0.4f, Vector3.zero, 1f, false, (int)elementType, -1));
			}
			return list;
		}

		// Token: 0x06000799 RID: 1945 RVA: 0x0002E20C File Offset: 0x0002C60C
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_ElementDown(int overlayNum, eElementType elementType)
		{
			List<SkillActionEffectSet.EffectData> list = new List<SkillActionEffectSet.EffectData>();
			if (elementType != eElementType.None)
			{
				list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementDown[(int)elementType, 0], 0.8f, 0f, Vector3.zero, 1f, false, -1, -1));
				overlayNum = Mathf.Clamp(overlayNum, 1, 3);
				for (int i = 0; i < overlayNum; i++)
				{
					if (i != 0)
					{
						if (i != 1)
						{
							if (i == 2)
							{
								list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementDown[(int)elementType, 1], 1.2f, 0.3f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, false, -1, -1));
							}
						}
						else
						{
							list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementDown[(int)elementType, 1], 1.2f, 0.15f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, false, -1, -1));
						}
					}
					else
					{
						list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementDown[(int)elementType, 1], 1.2f, 0f, new Vector3(0f, 0f, -1.5f * (float)i), 1f, true, -1, -1));
					}
				}
				list.Add(new SkillActionEffectSet.EffectData(this.EffectID_ElementDown[(int)elementType, 2], 1f, 0.4f, Vector3.zero, 1f, false, (int)elementType, -1));
			}
			return list;
		}

		// Token: 0x0600079A RID: 1946 RVA: 0x0002E390 File Offset: 0x0002C790
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Abnormal(eStateAbnormal stateAbnormal)
		{
			List<SkillActionEffectSet.EffectData> list = new List<SkillActionEffectSet.EffectData>();
			list.Add(new SkillActionEffectSet.EffectData("ef_btl_abnormal", 1f, 0f, Vector3.zero, 1f, true, -1, -1));
			if (stateAbnormal < eStateAbnormal.Num)
			{
				list.Add(new SkillActionEffectSet.EffectData("ef_btl_abnormal_symbol", 1f, 0f, new Vector3(0f, 0.14f, 0f), 1f, false, (int)stateAbnormal, -1));
			}
			return list;
		}

		// Token: 0x0600079B RID: 1947 RVA: 0x0002E408 File Offset: 0x0002C808
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_AbnormalRecover(int overlayNum)
		{
			return new List<SkillActionEffectSet.EffectData>
			{
				new SkillActionEffectSet.EffectData("ef_btl_abnormal_recover", 1.15f, 0f, Vector3.zero, 1f, true, -1, -1)
			};
		}

		// Token: 0x0600079C RID: 1948 RVA: 0x0002E444 File Offset: 0x0002C844
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_Barrier()
		{
			return new List<SkillActionEffectSet.EffectData>
			{
				new SkillActionEffectSet.EffectData("ef_btl_barrier", 1.15f, 0f, new Vector3(0f, 0.15f, 0f), 1f, true, -1, -1)
			};
		}

		// Token: 0x0600079D RID: 1949 RVA: 0x0002E490 File Offset: 0x0002C890
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_HateUp()
		{
			return new List<SkillActionEffectSet.EffectData>
			{
				new SkillActionEffectSet.EffectData("ef_btl_hate_up", 1.75f, 0f, new Vector3(0f, 0.3f, 0f), 1f, true, -1, -1)
			};
		}

		// Token: 0x0600079E RID: 1950 RVA: 0x0002E4DC File Offset: 0x0002C8DC
		private List<SkillActionEffectSet.EffectData> CreateEffectDatas_HateDown()
		{
			return new List<SkillActionEffectSet.EffectData>
			{
				new SkillActionEffectSet.EffectData("ef_btl_hate_down", 1.75f, 0f, new Vector3(0f, 0.3f, 0f), 1f, true, -1, -1)
			};
		}

		// Token: 0x04000741 RID: 1857
		private const float EFFECT_PLAY_OVERLAY_OFFSET_Z = -1.5f;

		// Token: 0x04000742 RID: 1858
		private readonly string[,] EffectID_ElementUp;

		// Token: 0x04000743 RID: 1859
		private readonly string[,] EffectID_ElementDown;

		// Token: 0x04000744 RID: 1860
		private List<SkillActionEffectSet.EffectData> m_EffectDatas;

		// Token: 0x04000745 RID: 1861
		private List<SkillActionEffectSet.IconData> m_IconDatas;

		// Token: 0x04000746 RID: 1862
		private CharacterHandler m_TargetCharaHndl;

		// Token: 0x04000747 RID: 1863
		private Transform m_EffectTo;

		// Token: 0x04000748 RID: 1864
		private int m_OverlayNum;

		// Token: 0x04000749 RID: 1865
		private bool m_IsDoneEffectDatas;

		// Token: 0x0400074A RID: 1866
		private List<EffectHandler> m_ActiveEffectList;

		// Token: 0x0400074B RID: 1867
		private BattleUI m_UI;

		// Token: 0x02000123 RID: 291
		private class EffectData
		{
			// Token: 0x0600079F RID: 1951 RVA: 0x0002E528 File Offset: 0x0002C928
			public EffectData(string effectID, float animSpeed, float delaySec, Vector3 offset, float scale, bool isPopupIcon, int argIndexCtrlParam_Visible = -1, int argIndexCtrlParam_Color = -1)
			{
				this.m_IsPlayed = false;
				this.m_EffectID = effectID;
				this.m_AnimSpeed = animSpeed;
				this.m_DelaySec = delaySec;
				this.m_Offset = offset;
				this.m_Scale = scale;
				this.m_IsPopupIcon = isPopupIcon;
				this.m_ArgIndexCtrlParam_Visible = argIndexCtrlParam_Visible;
				this.m_ArgIndexCtrlParam_Color = argIndexCtrlParam_Color;
			}

			// Token: 0x0400074C RID: 1868
			public bool m_IsPlayed;

			// Token: 0x0400074D RID: 1869
			public string m_EffectID;

			// Token: 0x0400074E RID: 1870
			public float m_AnimSpeed = 1f;

			// Token: 0x0400074F RID: 1871
			public float m_DelaySec;

			// Token: 0x04000750 RID: 1872
			public Vector3 m_Offset = Vector3.zero;

			// Token: 0x04000751 RID: 1873
			public float m_Scale = 1f;

			// Token: 0x04000752 RID: 1874
			public bool m_IsPopupIcon;

			// Token: 0x04000753 RID: 1875
			public int m_ArgIndexCtrlParam_Visible = -1;

			// Token: 0x04000754 RID: 1876
			public int m_ArgIndexCtrlParam_Color = -1;
		}

		// Token: 0x02000124 RID: 292
		public class IconData
		{
			// Token: 0x060007A0 RID: 1952 RVA: 0x0002E5AE File Offset: 0x0002C9AE
			public IconData(ePopUpStateIconType iconType, int grade = -1, bool isMiss = false)
			{
				this.m_IconType = iconType;
				this.m_Grade = grade;
				this.m_IsMiss = isMiss;
			}

			// Token: 0x04000755 RID: 1877
			public ePopUpStateIconType m_IconType;

			// Token: 0x04000756 RID: 1878
			public int m_Grade;

			// Token: 0x04000757 RID: 1879
			public bool m_IsMiss;
		}
	}
}
