﻿using System;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x0200041B RID: 1051
	public class GachaMain : GameStateMain
	{
		// Token: 0x0600141C RID: 5148 RVA: 0x0006B639 File Offset: 0x00069A39
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x0600141D RID: 5149 RVA: 0x0006B642 File Offset: 0x00069A42
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600141E RID: 5150 RVA: 0x0006B64C File Offset: 0x00069A4C
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new GachaState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new GachaState_Main(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x0600141F RID: 5151 RVA: 0x0006B693 File Offset: 0x00069A93
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
		}

		// Token: 0x06001420 RID: 5152 RVA: 0x0006B6A5 File Offset: 0x00069AA5
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001B17 RID: 6935
		public const int STATE_INIT = 0;

		// Token: 0x04001B18 RID: 6936
		public const int STATE_MAIN = 1;
	}
}
