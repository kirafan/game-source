﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020004A3 RID: 1187
	public class TitleState_Title : TitleState
	{
		// Token: 0x06001748 RID: 5960 RVA: 0x00078E26 File Offset: 0x00077226
		public TitleState_Title(TitleMain owner) : base(owner)
		{
		}

		// Token: 0x06001749 RID: 5961 RVA: 0x00078E36 File Offset: 0x00077236
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x0600174A RID: 5962 RVA: 0x00078E39 File Offset: 0x00077239
		public override void OnStateEnter()
		{
			this.m_Step = TitleState_Title.eStep.First;
		}

		// Token: 0x0600174B RID: 5963 RVA: 0x00078E42 File Offset: 0x00077242
		public override void OnStateExit()
		{
		}

		// Token: 0x0600174C RID: 5964 RVA: 0x00078E44 File Offset: 0x00077244
		public override void OnDispose()
		{
		}

		// Token: 0x0600174D RID: 5965 RVA: 0x00078E48 File Offset: 0x00077248
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case TitleState_Title.eStep.First:
				this.m_Owner.TitleUI.OnClickGameStartButton += this.OnClickGameStartButton;
				this.m_Owner.TitleUI.OnClickPlayerResetButton += this.OnClickPlayerResetButton;
				this.m_Owner.TitleUI.OnClickPlayerMoveButton += this.OnClickPlayerMoveButton;
				this.m_Owner.TitleUI.OnClickCacheClearButton += this.OnClickCacheClearButton;
				this.m_Owner.TitleUI.OnSuccessPlayerMoveCallback += this.OnPlayerMoveCallback;
				this.m_IsPreparedBgm = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PrepareBGM(eSoundBgmListDB.BGM_TITLE, 1f, 0, -1, -1);
				this.m_Step = TitleState_Title.eStep.Prepare_Wait;
				break;
			case TitleState_Title.eStep.Prepare_Wait:
				if (!this.m_IsPreparedBgm || SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsCompletePrepareBGM())
				{
					this.m_Owner.TitleUI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, -1f, null);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.ResumeBGM();
					this.m_Step = TitleState_Title.eStep.Title_Wait;
				}
				break;
			case TitleState_Title.eStep.Title_Wait:
				this.m_Owner.TitleUI.SetGoToNextSheetActive(true);
				this.UpdateAndroidBackKey();
				break;
			case TitleState_Title.eStep.Agreement:
				this.m_Step = TitleState_Title.eStep.Agreement_Wait;
				this.m_Owner.TitleUI.OpenAgreement(new Action<bool>(this.OnCloseAgreement));
				break;
			case TitleState_Title.eStep.Login:
				SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_Login(new Action<MeigewwwParam, Login>(this.OnResponse_Login), new Action<MeigewwwParam, Getall>(this.OnResponse_GetAll));
				this.m_Step = TitleState_Title.eStep.Login_Wait;
				break;
			case TitleState_Title.eStep.QuestGetAll:
				SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestGetAll(new Action(this.OnResponse_QuestGetAll), true);
				this.m_Step = TitleState_Title.eStep.QuestGetAll_Wait;
				break;
			case TitleState_Title.eStep.Mission:
				SingletonMonoBehaviour<GameSystem>.Inst.MissionMng.Prepare();
				this.m_Step = TitleState_Title.eStep.Mission_Wait;
				break;
			case TitleState_Title.eStep.Mission_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.MissionMng.IsDonePrepare())
				{
					this.m_Step = TitleState_Title.eStep.Restore;
				}
				break;
			case TitleState_Title.eStep.Restore:
				SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.Request_InitializeProducts(new Action(this.OnCompleteInitProducts));
				this.m_Step = TitleState_Title.eStep.Restore_Wait;
				break;
			case TitleState_Title.eStep.TransitNextScene:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsOverlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				}
				this.m_Step = TitleState_Title.eStep.TransitNextScene_Wait;
				break;
			case TitleState_Title.eStep.TransitNextScene_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = TitleState_Title.eStep.None;
					if (this.m_IsRequestReturnTitle)
					{
						APIUtility.ReturnTitle(true);
					}
					else
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Download);
					}
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600174E RID: 5966 RVA: 0x0007915C File Offset: 0x0007755C
		public void OnClickGameStartButton()
		{
			bool flag = false;
			if (APIUtility.IsNeedSignup() && LocalSaveData.Inst != null && !LocalSaveData.Inst.Agreement)
			{
				flag = true;
			}
			if (flag)
			{
				this.m_Step = TitleState_Title.eStep.Agreement;
			}
			else if (APIUtility.IsNeedSignup())
			{
				this.TransitNextSceneStart();
			}
			else
			{
				this.m_Step = TitleState_Title.eStep.Login;
			}
		}

		// Token: 0x0600174F RID: 5967 RVA: 0x000791BE File Offset: 0x000775BE
		public void OnCloseAgreement(bool isYes)
		{
			if (isYes)
			{
				LocalSaveData.Inst.Agreement = true;
				LocalSaveData.Inst.Save();
				this.TransitNextSceneStart();
			}
			else
			{
				this.m_Step = TitleState_Title.eStep.Title_Wait;
			}
		}

		// Token: 0x06001750 RID: 5968 RVA: 0x000791F0 File Offset: 0x000775F0
		private void OnResponse_Login(MeigewwwParam wwwParam, Login param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.PLAYER_NOT_FOUND)
				{
					if (result != ResultCode.UUID_NOT_FOUND)
					{
						APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
						this.m_Step = TitleState_Title.eStep.Title_Wait;
					}
					else
					{
						APIUtility.ShowErrorWindowOk("引き継ぎが行われました。", result.ToMessageString(), null);
						this.m_Step = TitleState_Title.eStep.Title_Wait;
					}
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					this.m_Step = TitleState_Title.eStep.Title_Wait;
				}
			}
		}

		// Token: 0x06001751 RID: 5969 RVA: 0x00079280 File Offset: 0x00077680
		private void OnResponse_GetAll(MeigewwwParam wwwParam, Getall param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowTitle(wwwParam, result.ToMessageString());
				this.m_Step = TitleState_Title.eStep.Title_Wait;
			}
			else
			{
				this.m_Step = TitleState_Title.eStep.QuestGetAll;
			}
		}

		// Token: 0x06001752 RID: 5970 RVA: 0x000792CA File Offset: 0x000776CA
		private void OnResponse_QuestGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestChapterGetAll(new Action(this.OnResponse_QuestChapterGetAll), true);
		}

		// Token: 0x06001753 RID: 5971 RVA: 0x000792E9 File Offset: 0x000776E9
		private void OnResponse_QuestChapterGetAll()
		{
			this.m_Step = TitleState_Title.eStep.Mission;
		}

		// Token: 0x06001754 RID: 5972 RVA: 0x000792F3 File Offset: 0x000776F3
		private void OnCompleteInitProducts()
		{
			this.TransitNextSceneStart();
		}

		// Token: 0x06001755 RID: 5973 RVA: 0x000792FB File Offset: 0x000776FB
		private void OnClickPlayerResetButton()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_PlayerReset(new Action<MeigewwwParam, Reset>(this.OnResponse_PlayerReset));
		}

		// Token: 0x06001756 RID: 5974 RVA: 0x00079318 File Offset: 0x00077718
		private void OnResponse_PlayerReset(MeigewwwParam wwwParam, Reset param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				this.m_Step = TitleState_Title.eStep.Title_Wait;
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerResetTitle, eText_MessageDB.PlayerReset, new Action<int>(this.OnClosedConfirm));
			}
		}

		// Token: 0x06001757 RID: 5975 RVA: 0x0007937B File Offset: 0x0007777B
		private void OnClickPlayerMoveButton(string moveCode, string moveCodePassward)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_PlayerMoveSet(moveCode, moveCodePassward, new Action<string>(this.OnResponse_PlayerMoveSet));
		}

		// Token: 0x06001758 RID: 5976 RVA: 0x0007939A File Offset: 0x0007779A
		private void OnResponse_PlayerMoveSet(string errorMessage)
		{
			if (string.IsNullOrEmpty(errorMessage))
			{
				this.OnPlayerMoveCallback();
			}
			else
			{
				APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveErrTitle), errorMessage, null);
				this.m_Step = TitleState_Title.eStep.Title_Wait;
			}
		}

		// Token: 0x06001759 RID: 5977 RVA: 0x000793D1 File Offset: 0x000777D1
		private void OnPlayerMoveCallback()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerMoveTitle, eText_MessageDB.PlayerMove, new Action<int>(this.OnClosedConfirm));
		}

		// Token: 0x0600175A RID: 5978 RVA: 0x000793F3 File Offset: 0x000777F3
		private void OnClickCacheClearButton()
		{
			MeigeResourceManager.AssetbundleCleanCache();
			SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller.CleanCache();
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerCacheClearTitle, eText_MessageDB.PlayerCacheClear, new Action<int>(this.OnClosedConfirm));
		}

		// Token: 0x0600175B RID: 5979 RVA: 0x0007942A File Offset: 0x0007782A
		private void OnClosedConfirm(int answer)
		{
			this.m_IsRequestReturnTitle = true;
			this.TransitNextSceneStart();
		}

		// Token: 0x0600175C RID: 5980 RVA: 0x00079439 File Offset: 0x00077839
		private void TransitNextSceneStart()
		{
			this.m_Step = TitleState_Title.eStep.TransitNextScene;
		}

		// Token: 0x0600175D RID: 5981 RVA: 0x00079444 File Offset: 0x00077844
		private void UpdateAndroidBackKey()
		{
			if (UIUtility.GetInputAndroidBackKey(this.m_Owner.TitleUI.GetCacheClearButton()))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.AppQuitConfirmTitle, eText_MessageDB.AppQuitConfirmMessage, new Action<int>(this.OnConfirmAppQuit));
			}
		}

		// Token: 0x0600175E RID: 5982 RVA: 0x00079491 File Offset: 0x00077891
		private void OnConfirmAppQuit(int answer)
		{
			if (answer == 0)
			{
				Application.Quit();
			}
		}

		// Token: 0x04001DF9 RID: 7673
		private TitleState_Title.eStep m_Step = TitleState_Title.eStep.None;

		// Token: 0x04001DFA RID: 7674
		private bool m_IsRequestReturnTitle;

		// Token: 0x04001DFB RID: 7675
		private bool m_IsPreparedBgm;

		// Token: 0x020004A4 RID: 1188
		private enum eStep
		{
			// Token: 0x04001DFD RID: 7677
			None = -1,
			// Token: 0x04001DFE RID: 7678
			First,
			// Token: 0x04001DFF RID: 7679
			Prepare_Wait,
			// Token: 0x04001E00 RID: 7680
			Title_Wait,
			// Token: 0x04001E01 RID: 7681
			Agreement,
			// Token: 0x04001E02 RID: 7682
			Agreement_Wait,
			// Token: 0x04001E03 RID: 7683
			Login,
			// Token: 0x04001E04 RID: 7684
			Login_Wait,
			// Token: 0x04001E05 RID: 7685
			QuestGetAll,
			// Token: 0x04001E06 RID: 7686
			QuestGetAll_Wait,
			// Token: 0x04001E07 RID: 7687
			Mission,
			// Token: 0x04001E08 RID: 7688
			Mission_Wait,
			// Token: 0x04001E09 RID: 7689
			Restore,
			// Token: 0x04001E0A RID: 7690
			Restore_Wait,
			// Token: 0x04001E0B RID: 7691
			TransitNextScene,
			// Token: 0x04001E0C RID: 7692
			TransitNextScene_Wait
		}
	}
}
