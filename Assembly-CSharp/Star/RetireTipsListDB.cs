﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001F2 RID: 498
	public class RetireTipsListDB : ScriptableObject
	{
		// Token: 0x04000C03 RID: 3075
		public RetireTipsListDB_Param[] m_Params;
	}
}
