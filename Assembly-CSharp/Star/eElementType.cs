﻿using System;

namespace Star
{
	// Token: 0x02000228 RID: 552
	public enum eElementType
	{
		// Token: 0x04000F12 RID: 3858
		None = -1,
		// Token: 0x04000F13 RID: 3859
		Fire,
		// Token: 0x04000F14 RID: 3860
		Water,
		// Token: 0x04000F15 RID: 3861
		Earth,
		// Token: 0x04000F16 RID: 3862
		Wind,
		// Token: 0x04000F17 RID: 3863
		Moon,
		// Token: 0x04000F18 RID: 3864
		Sun,
		// Token: 0x04000F19 RID: 3865
		Num
	}
}
