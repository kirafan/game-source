﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000FF RID: 255
	public class BattleUniqueSkillOwnerControllHandler : MonoBehaviour
	{
		// Token: 0x06000733 RID: 1843 RVA: 0x0002B5AC File Offset: 0x000299AC
		public void Setup(CharacterHandler owner, List<MeigeAnimClipHolder> meigeAnimClipHolders)
		{
			this.m_Owner = owner;
			this.m_MeigeAnimCtrl = base.gameObject.AddComponent<MeigeAnimCtrl>();
			this.m_MeigeAnimCtrl.Init();
			this.m_ClipNames = new List<string>();
			this.m_MeigeAnimCtrl.Open();
			for (int i = 0; i < meigeAnimClipHolders.Count; i++)
			{
				if (this.m_Owner.CharaAnim.ModelSets.Length > i && this.m_Owner.CharaAnim.ModelSets[i].m_MsbHndl != null)
				{
					this.m_MeigeAnimCtrl.AddClip(this.m_Owner.CharaAnim.ModelSets[i].m_MsbHndl, meigeAnimClipHolders[i]);
					this.m_ClipNames.Add(meigeAnimClipHolders[i].m_UnityAnimClip.name);
				}
			}
			this.m_MeigeAnimCtrl.Close();
		}

		// Token: 0x06000734 RID: 1844 RVA: 0x0002B694 File Offset: 0x00029A94
		public void Destroy()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Pause();
				for (int i = 0; i < this.m_Owner.CharaAnim.ModelSets.Length; i++)
				{
					if (this.m_Owner.CharaAnim.ModelSets[i] != null && this.m_Owner.CharaAnim.ModelSets[i].m_MsbHndl != null)
					{
						Animation animationTarget = this.m_Owner.CharaAnim.ModelSets[i].m_MsbHndl.GetAnimationTarget();
						if (animationTarget != null)
						{
							animationTarget.Stop(this.m_ClipNames[i]);
							animationTarget.RemoveClip(this.m_ClipNames[i]);
						}
					}
				}
				this.m_MeigeAnimCtrl = null;
			}
		}

		// Token: 0x06000735 RID: 1845 RVA: 0x0002B76D File Offset: 0x00029B6D
		public void Play()
		{
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			this.m_MeigeAnimCtrl.Play("skill", 0f, WrapMode.ClampForever);
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x0002B797 File Offset: 0x00029B97
		public void Pause()
		{
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x06000737 RID: 1847 RVA: 0x0002B7B8 File Offset: 0x00029BB8
		public bool IsPlaying()
		{
			WrapMode wrapMode = this.m_MeigeAnimCtrl.m_WrapMode;
			if (wrapMode != WrapMode.ClampForever)
			{
				if (this.m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
				{
					return true;
				}
			}
			else if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000738 RID: 1848 RVA: 0x0002B814 File Offset: 0x00029C14
		public float GetPlayingSec()
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler("skill");
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime * this.m_MeigeAnimCtrl.m_NormalizedPlayTime;
			}
			return 0f;
		}

		// Token: 0x06000739 RID: 1849 RVA: 0x0002B850 File Offset: 0x00029C50
		public float GetMaxSec()
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler("skill");
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime;
			}
			return 0f;
		}

		// Token: 0x04000679 RID: 1657
		private const string PLAY_KEY = "skill";

		// Token: 0x0400067A RID: 1658
		private CharacterHandler m_Owner;

		// Token: 0x0400067B RID: 1659
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400067C RID: 1660
		private List<string> m_ClipNames;
	}
}
