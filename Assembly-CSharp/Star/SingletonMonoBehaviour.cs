﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E6 RID: 1254
	public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
	{
		// Token: 0x17000198 RID: 408
		// (get) Token: 0x06001905 RID: 6405 RVA: 0x000481B8 File Offset: 0x000465B8
		public static T Inst
		{
			get
			{
				if (SingletonMonoBehaviour<T>.instance == null)
				{
					SingletonMonoBehaviour<T>.instance = (T)((object)UnityEngine.Object.FindObjectOfType(typeof(T)));
					if (SingletonMonoBehaviour<T>.instance != null)
					{
					}
				}
				return SingletonMonoBehaviour<T>.instance;
			}
		}

		// Token: 0x06001906 RID: 6406 RVA: 0x00048212 File Offset: 0x00046612
		protected virtual void Awake()
		{
			this.CheckInstance();
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x0004821C File Offset: 0x0004661C
		protected bool CheckInstance()
		{
			if (this == SingletonMonoBehaviour<T>.Inst)
			{
				if (this.m_IsDontDestroyOnLoad)
				{
					UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
				}
				return true;
			}
			UnityEngine.Object.Destroy(base.gameObject);
			UnityEngine.Object.Destroy(this);
			return false;
		}

		// Token: 0x06001908 RID: 6408 RVA: 0x00048268 File Offset: 0x00046668
		protected virtual void OnDestroy()
		{
			if (SingletonMonoBehaviour<T>.instance == this)
			{
				SingletonMonoBehaviour<T>.instance = (T)((object)null);
			}
		}

		// Token: 0x04001F80 RID: 8064
		[SerializeField]
		private bool m_IsDontDestroyOnLoad = true;

		// Token: 0x04001F81 RID: 8065
		private static T instance;
	}
}
