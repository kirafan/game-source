﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200066F RID: 1647
	[Serializable]
	public class TownResourceObjectData
	{
		// Token: 0x06002160 RID: 8544 RVA: 0x000B1874 File Offset: 0x000AFC74
		public void Initialize(string pfilename)
		{
			this.m_Objects = new TownResourceObjectData.TCacheObjectState[4];
			this.m_CacheSize = 4;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				this.m_Objects[i] = new TownResourceObjectData.TCacheObjectState();
			}
			this.m_Handle = ObjectResourceManager.CreateHandle(pfilename, new Action<MeigeResource.Handler>(this.CallbackResObj));
		}

		// Token: 0x06002161 RID: 8545 RVA: 0x000B18D0 File Offset: 0x000AFCD0
		private void CallbackResObj(MeigeResource.Handler phandle)
		{
			this.m_Handle = phandle;
			if (this.m_Handle.IsDone)
			{
				GameObject asset = this.m_Handle.GetAsset<GameObject>();
				if (asset != null)
				{
					this.m_ObjectList = asset.GetComponent<TownObjectDataBase>();
				}
			}
		}

		// Token: 0x06002162 RID: 8546 RVA: 0x000B1918 File Offset: 0x000AFD18
		public void Destroy()
		{
			if (this.m_Objects != null)
			{
				for (int i = 0; i < this.m_Objects.Length; i++)
				{
					if (this.m_Objects[i].m_Object != null)
					{
						UnityEngine.Object.Destroy(this.m_Objects[i].m_Object);
					}
					this.m_Objects[i].m_Object = null;
				}
				this.m_Objects = null;
			}
			this.m_CacheSize = 0;
			if (this.m_Handle != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_Handle);
				this.m_Handle = null;
			}
		}

		// Token: 0x06002163 RID: 8547 RVA: 0x000B19AC File Offset: 0x000AFDAC
		public GameObject GetNextObjectInCache(int fmodelno)
		{
			int num = -1;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				if (!this.m_Objects[i].m_Active)
				{
					num = i;
					break;
				}
			}
			if (num < 0)
			{
				TownResourceObjectData.TCacheObjectState[] array = new TownResourceObjectData.TCacheObjectState[this.m_CacheSize + 4];
				for (int j = 0; j < this.m_CacheSize; j++)
				{
					array[j] = this.m_Objects[j];
				}
				for (int j = this.m_CacheSize; j < this.m_CacheSize + 4; j++)
				{
					array[j] = new TownResourceObjectData.TCacheObjectState();
				}
				this.m_Objects = null;
				this.m_Objects = array;
				num = this.m_CacheSize;
				this.m_CacheSize += 4;
			}
			GameObject result = this.m_Objects[num].m_Object = UnityEngine.Object.Instantiate<GameObject>(this.m_ObjectList.m_List[fmodelno]);
			this.m_Objects[num].m_Active = true;
			return result;
		}

		// Token: 0x06002164 RID: 8548 RVA: 0x000B1AA8 File Offset: 0x000AFEA8
		public void FreeObjectInCache(GameObject pobj)
		{
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				if (this.m_Objects[i].m_Object == pobj)
				{
					this.m_Objects[i].m_Active = false;
					UnityEngine.Object.Destroy(this.m_Objects[i].m_Object);
					this.m_Objects[i].m_Object = null;
					break;
				}
			}
		}

		// Token: 0x040027B6 RID: 10166
		private TownResourceObjectData.TCacheObjectState[] m_Objects;

		// Token: 0x040027B7 RID: 10167
		private int m_CacheSize;

		// Token: 0x040027B8 RID: 10168
		private TownObjectDataBase m_ObjectList;

		// Token: 0x040027B9 RID: 10169
		private MeigeResource.Handler m_Handle;

		// Token: 0x02000670 RID: 1648
		private class TCacheObjectState
		{
			// Token: 0x040027BA RID: 10170
			public GameObject m_Object;

			// Token: 0x040027BB RID: 10171
			public bool m_Active;
		}
	}
}
