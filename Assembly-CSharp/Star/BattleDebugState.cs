﻿using System;

namespace Star
{
	// Token: 0x02000272 RID: 626
	public class BattleDebugState : GameStateBase
	{
		// Token: 0x06000B9C RID: 2972 RVA: 0x00043CB8 File Offset: 0x000420B8
		public BattleDebugState(BattleDebugMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06000B9D RID: 2973 RVA: 0x00043CC7 File Offset: 0x000420C7
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06000B9E RID: 2974 RVA: 0x00043CCA File Offset: 0x000420CA
		public override void OnStateEnter()
		{
		}

		// Token: 0x06000B9F RID: 2975 RVA: 0x00043CCC File Offset: 0x000420CC
		public override void OnStateExit()
		{
		}

		// Token: 0x06000BA0 RID: 2976 RVA: 0x00043CCE File Offset: 0x000420CE
		public override void OnDispose()
		{
			this.m_Owner = null;
		}

		// Token: 0x06000BA1 RID: 2977 RVA: 0x00043CD7 File Offset: 0x000420D7
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x00043CD9 File Offset: 0x000420D9
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0400140F RID: 5135
		protected BattleDebugMain m_Owner;
	}
}
