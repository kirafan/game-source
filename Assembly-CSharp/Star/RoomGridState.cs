﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005EC RID: 1516
	public class RoomGridState
	{
		// Token: 0x06001DF7 RID: 7671 RVA: 0x000A0FC0 File Offset: 0x0009F3C0
		public void SetUp(int fgridx, int fgridy, int fgridz)
		{
			this.m_SizeX = fgridx;
			this.m_SizeY = fgridy;
			this.m_SizeZ = fgridz;
			this.m_Max = (fgridx + fgridz) * (fgridy * fgridz);
			this.m_GridStatus = new int[fgridx + fgridz, fgridy + fgridz];
			this.m_GroupStatus = new byte[fgridx + fgridz, fgridy + fgridz];
			for (int i = 0; i < fgridy + fgridz; i++)
			{
				for (int j = 0; j < fgridx + fgridz; j++)
				{
					this.m_GridStatus[j, i] = 1;
					this.m_GroupStatus[j, i] = 0;
				}
			}
			this.m_MoveFreeSpaceNum = this.m_SizeX * this.m_SizeY;
			this.m_SubMoveFreeSpaceNum = this.m_SizeX * this.m_SizeZ + (this.m_SizeY + this.m_SizeZ);
		}

		// Token: 0x06001DF8 RID: 7672 RVA: 0x000A1089 File Offset: 0x0009F489
		public void Destroy()
		{
			this.m_GridStatus = null;
		}

		// Token: 0x06001DF9 RID: 7673 RVA: 0x000A1092 File Offset: 0x0009F492
		public int[,] GetGridMap()
		{
			return this.m_GridStatus;
		}

		// Token: 0x06001DFA RID: 7674 RVA: 0x000A109C File Offset: 0x0009F49C
		public Vector2 CalcFreeMovePoint()
		{
			Vector2 zero = Vector2.zero;
			int num2;
			int num = num2 = UnityEngine.Random.Range(0, this.m_SizeX);
			int num4;
			int num3 = num4 = UnityEngine.Random.Range(0, this.m_SizeY);
			int num5 = 0;
			bool flag = true;
			bool flag2 = false;
			while (flag)
			{
				if (this.m_GridStatus[num, num3] == 1)
				{
					zero.x = (float)num;
					zero.y = (float)num3;
					break;
				}
				switch (num5)
				{
				case 0:
					num++;
					if (num >= this.m_SizeX)
					{
						num = num2;
						num5++;
					}
					break;
				case 1:
					num--;
					if (num < 0)
					{
						num = num2;
						if (flag2)
						{
							num5 = 3;
						}
						else
						{
							num5 = 2;
						}
					}
					break;
				case 2:
					num3++;
					if (num3 >= this.m_SizeY)
					{
						num3 = num4;
						flag2 = true;
						num5++;
					}
					else
					{
						num5 = 0;
					}
					break;
				case 3:
					num3--;
					if (num3 < 0)
					{
						num3 = num4;
						num5++;
					}
					else
					{
						num5 = 0;
					}
					break;
				case 4:
					num = (num2 = UnityEngine.Random.Range(0, this.m_SizeX - 1));
					num3 = (num4 = UnityEngine.Random.Range(0, this.m_SizeY - 1));
					num5 = 0;
					flag = false;
					break;
				}
			}
			return zero;
		}

		// Token: 0x06001DFB RID: 7675 RVA: 0x000A11EC File Offset: 0x0009F5EC
		public void SetGridStatus(int gridX, int gridY, int sizeX, int sizeY, int status, byte fgroupkey)
		{
			gridX = Mathf.Clamp(gridX, 0, this.m_SizeX + this.m_SizeZ - 1);
			gridY = Mathf.Clamp(gridY, 0, this.m_SizeY + this.m_SizeZ - 1);
			byte b;
			byte b2;
			if (status == -1)
			{
				b = byte.MaxValue;
				b2 = fgroupkey;
			}
			else
			{
				b = ~fgroupkey;
				b2 = 0;
			}
			int num = gridX + sizeX;
			int num2 = gridY + sizeY;
			if (num > this.m_SizeX + this.m_SizeZ)
			{
				num = this.m_SizeX + this.m_SizeZ;
			}
			if (num2 > this.m_SizeY + this.m_SizeZ)
			{
				num2 = this.m_SizeY + this.m_SizeZ;
			}
			for (int i = gridY; i < num2; i++)
			{
				for (int j = gridX; j < num; j++)
				{
					this.m_GridStatus[j, i] = status;
					this.m_GroupStatus[j, i] = ((this.m_GroupStatus[j, i] & b) | b2);
				}
			}
			if (gridX < this.m_SizeX && gridY < this.m_SizeY)
			{
				if (status == 1)
				{
					this.m_MoveFreeSpaceNum += (num2 - gridY) * (num - gridX);
				}
				else
				{
					this.m_MoveFreeSpaceNum -= (num2 - gridY) * (num - gridX);
				}
			}
			else if (status == 1)
			{
				this.m_SubMoveFreeSpaceNum += (num2 - gridY) * (num - gridX);
			}
			else
			{
				this.m_SubMoveFreeSpaceNum -= (num2 - gridY) * (num - gridX);
			}
		}

		// Token: 0x06001DFC RID: 7676 RVA: 0x000A1378 File Offset: 0x0009F778
		public void SetGridGroup(int gridX, int gridY, int sizeX, int sizeY, int status, byte fgroupkey)
		{
			gridX = Mathf.Clamp(gridX, 0, this.m_SizeX + this.m_SizeZ - 1);
			gridY = Mathf.Clamp(gridY, 0, this.m_SizeY + this.m_SizeZ - 1);
			int num = gridX + sizeX;
			int num2 = gridY + sizeY;
			byte b;
			byte b2;
			if (status == -1)
			{
				b = byte.MaxValue;
				b2 = fgroupkey;
			}
			else
			{
				b = ~fgroupkey;
				b2 = 0;
			}
			if (num > this.m_SizeX + this.m_SizeZ)
			{
				num = this.m_SizeX + this.m_SizeZ;
			}
			if (num2 > this.m_SizeY + this.m_SizeZ)
			{
				num2 = this.m_SizeY + this.m_SizeZ;
			}
			for (int i = gridY; i < num2; i++)
			{
				for (int j = gridX; j < num; j++)
				{
					this.m_GroupStatus[j, i] = ((this.m_GroupStatus[j, i] & b) | b2);
				}
			}
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x000A146C File Offset: 0x0009F86C
		public bool IsAnyGridStatus(int gridX, int gridY, int sizeX, int sizeY, eRoomObjectCategory fobjcategory, byte fobjgroup)
		{
			gridX = Mathf.Clamp(gridX, 0, this.m_SizeX + this.m_SizeZ - 1);
			gridY = Mathf.Clamp(gridY, 0, this.m_SizeY + this.m_SizeZ - 1);
			int num = gridX + sizeX;
			int num2 = gridY + sizeY;
			bool flag = false;
			if (fobjcategory == eRoomObjectCategory.WallDecoration)
			{
				if (num > this.m_SizeX + this.m_SizeZ)
				{
					flag = true;
				}
				if (num2 > this.m_SizeY + this.m_SizeZ)
				{
					flag = true;
				}
			}
			else
			{
				if (num > this.m_SizeX)
				{
					flag = true;
				}
				if (num2 > this.m_SizeY)
				{
					flag = true;
				}
			}
			if (!flag)
			{
				for (int i = gridY; i < num2; i++)
				{
					for (int j = gridX; j < num; j++)
					{
						if ((this.m_GroupStatus[j, i] & fobjgroup) != 0)
						{
							return true;
						}
					}
				}
			}
			return flag;
		}

		// Token: 0x04002485 RID: 9349
		public int[,] m_GridStatus;

		// Token: 0x04002486 RID: 9350
		public byte[,] m_GroupStatus;

		// Token: 0x04002487 RID: 9351
		public int m_SizeX;

		// Token: 0x04002488 RID: 9352
		public int m_SizeY;

		// Token: 0x04002489 RID: 9353
		public int m_SizeZ;

		// Token: 0x0400248A RID: 9354
		public int m_Max;

		// Token: 0x0400248B RID: 9355
		public int m_MoveFreeSpaceNum;

		// Token: 0x0400248C RID: 9356
		public int m_SubMoveFreeSpaceNum;

		// Token: 0x020005ED RID: 1517
		public struct GridState
		{
			// Token: 0x0400248D RID: 9357
			public int m_Status;
		}
	}
}
