﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000426 RID: 1062
	public class GameStateMain : MonoBehaviour
	{
		// Token: 0x17000165 RID: 357
		// (get) Token: 0x06001460 RID: 5216 RVA: 0x00043B57 File Offset: 0x00041F57
		public int PrevStateID
		{
			get
			{
				return this.m_PrevStateID;
			}
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06001461 RID: 5217 RVA: 0x00043B5F File Offset: 0x00041F5F
		public int NextStateID
		{
			get
			{
				return this.m_NextStateID;
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06001462 RID: 5218 RVA: 0x00043B67 File Offset: 0x00041F67
		// (set) Token: 0x06001463 RID: 5219 RVA: 0x00043B6F File Offset: 0x00041F6F
		public SceneDefine.eSceneID NextTransitSceneID
		{
			get
			{
				return this.m_NextTransitSceneID;
			}
			set
			{
				this.m_NextTransitSceneID = value;
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06001464 RID: 5220 RVA: 0x00043B78 File Offset: 0x00041F78
		public GameStateBase CurrentState
		{
			get
			{
				return this.m_CurrentState;
			}
		}

		// Token: 0x06001465 RID: 5221 RVA: 0x00043B80 File Offset: 0x00041F80
		protected virtual void Update()
		{
			if (this.m_NextStateID != -1)
			{
				if (this.m_CurrentState != null)
				{
					this.m_PrevStateID = this.m_CurrentState.GetStateID();
					this.m_CurrentState.OnStateExit();
					this.m_CurrentState.OnDispose();
					this.m_CurrentState = null;
				}
				this.m_CurrentState = this.ChangeState(this.m_NextStateID);
				if (this.m_CurrentState != null)
				{
					this.m_CurrentState.OnStateEnter();
				}
				this.m_NextStateID = -1;
			}
			if (this.m_CurrentState != null)
			{
				this.m_NextStateID = this.m_CurrentState.OnStateUpdate();
			}
		}

		// Token: 0x06001466 RID: 5222 RVA: 0x00043C1D File Offset: 0x0004201D
		private void OnDestroy()
		{
			if (this.m_CurrentState != null)
			{
				this.m_CurrentState.OnDispose();
				this.m_CurrentState = null;
			}
		}

		// Token: 0x06001467 RID: 5223 RVA: 0x00043C3C File Offset: 0x0004203C
		public void SetNextState(int nextStateID)
		{
			this.m_NextStateID = nextStateID;
		}

		// Token: 0x06001468 RID: 5224 RVA: 0x00043C45 File Offset: 0x00042045
		public virtual GameStateBase ChangeState(int stateID)
		{
			return null;
		}

		// Token: 0x06001469 RID: 5225 RVA: 0x00043C48 File Offset: 0x00042048
		public virtual void Destroy()
		{
		}

		// Token: 0x0600146A RID: 5226 RVA: 0x00043C4A File Offset: 0x0004204A
		public virtual bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001B43 RID: 6979
		public const int COMMON_STATE_FINAL = 2147483646;

		// Token: 0x04001B44 RID: 6980
		protected int m_PrevStateID = -1;

		// Token: 0x04001B45 RID: 6981
		protected int m_NextStateID = -1;

		// Token: 0x04001B46 RID: 6982
		private SceneDefine.eSceneID m_NextTransitSceneID = SceneDefine.eSceneID.None;

		// Token: 0x04001B47 RID: 6983
		protected GameStateBase m_CurrentState;
	}
}
