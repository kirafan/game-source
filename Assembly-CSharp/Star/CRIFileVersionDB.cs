﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000169 RID: 361
	public class CRIFileVersionDB : ScriptableObject
	{
		// Token: 0x040009B8 RID: 2488
		public CRIFileVersionDB_Param[] m_Params;
	}
}
