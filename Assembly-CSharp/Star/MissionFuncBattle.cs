﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004F9 RID: 1273
	public class MissionFuncBattle : IMissionFunction
	{
		// Token: 0x06001921 RID: 6433 RVA: 0x000827FC File Offset: 0x00080BFC
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			eXlsMissionBattleFuncType extensionScriptID = (eXlsMissionBattleFuncType)pparam.m_ExtensionScriptID;
			if (extensionScriptID != eXlsMissionBattleFuncType.QuestClear)
			{
				if (extensionScriptID != eXlsMissionBattleFuncType.EventQuestClear)
				{
					if (extensionScriptID == eXlsMissionBattleFuncType.QuestPlay)
					{
						pparam.m_Rate++;
						result = (pparam.m_Rate >= pparam.m_RateBase);
					}
				}
				else
				{
					pparam.m_Rate++;
					result = (pparam.m_Rate >= pparam.m_RateBase);
				}
			}
			else
			{
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
			}
			return result;
		}
	}
}
