﻿using System;

namespace Star
{
	// Token: 0x0200001E RID: 30
	public enum eADVMotAction
	{
		// Token: 0x040000B9 RID: 185
		None,
		// Token: 0x040000BA RID: 186
		Move,
		// Token: 0x040000BB RID: 187
		Scale
	}
}
