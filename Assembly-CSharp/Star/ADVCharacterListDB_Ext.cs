﻿using System;

namespace Star
{
	// Token: 0x02000185 RID: 389
	public static class ADVCharacterListDB_Ext
	{
		// Token: 0x06000AD2 RID: 2770 RVA: 0x00040874 File Offset: 0x0003EC74
		public static ADVCharacterListDB_Param GetParam(this ADVCharacterListDB self, string ADVCharaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ADVCharaID == ADVCharaID)
				{
					return self.m_Params[i];
				}
			}
			return new ADVCharacterListDB_Param
			{
				m_ADVCharaID = string.Empty,
				m_ResourceBaseName = string.Empty
			};
		}
	}
}
