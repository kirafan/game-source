﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005BE RID: 1470
	public class IRoomObjectControll : MonoBehaviour
	{
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06001C70 RID: 7280 RVA: 0x00098349 File Offset: 0x00096749
		public Transform trs
		{
			get
			{
				return this.m_OwnerTrs;
			}
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06001C71 RID: 7281 RVA: 0x00098351 File Offset: 0x00096751
		public Transform CacheTransform
		{
			get
			{
				return this.m_OwnerTrs;
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06001C72 RID: 7282 RVA: 0x00098359 File Offset: 0x00096759
		public RoomBlockData BlockData
		{
			get
			{
				return this.m_BlockData;
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06001C73 RID: 7283 RVA: 0x00098361 File Offset: 0x00096761
		public int FloorID
		{
			get
			{
				return this.m_FloorID;
			}
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06001C74 RID: 7284 RVA: 0x00098369 File Offset: 0x00096769
		public eRoomObjectCategory ObjCategory
		{
			get
			{
				return this.m_Category;
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06001C75 RID: 7285 RVA: 0x00098371 File Offset: 0x00096771
		public byte ObjGroupKey
		{
			get
			{
				return this.m_ObjGroupKey;
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06001C76 RID: 7286 RVA: 0x00098379 File Offset: 0x00096779
		public int ObjID
		{
			get
			{
				return this.m_ObjID;
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06001C77 RID: 7287 RVA: 0x00098381 File Offset: 0x00096781
		public long ManageID
		{
			get
			{
				return this.m_ManageID;
			}
		}

		// Token: 0x06001C78 RID: 7288 RVA: 0x00098389 File Offset: 0x00096789
		public bool IsActive()
		{
			return this.m_Active;
		}

		// Token: 0x06001C79 RID: 7289 RVA: 0x00098391 File Offset: 0x00096791
		public RoomBuilder GetBuilder()
		{
			return this.m_Builder;
		}

		// Token: 0x06001C7A RID: 7290 RVA: 0x00098399 File Offset: 0x00096799
		public void SetManageID(long fmanageid)
		{
			this.m_ManageID = fmanageid;
		}

		// Token: 0x06001C7B RID: 7291 RVA: 0x000983A2 File Offset: 0x000967A2
		public int GetSortCompKey()
		{
			return this.m_SortCompKey;
		}

		// Token: 0x06001C7C RID: 7292 RVA: 0x000983AA File Offset: 0x000967AA
		public void LinkObjectHandle(RoomBuilder pbuilder)
		{
			this.m_Builder = pbuilder;
		}

		// Token: 0x06001C7D RID: 7293 RVA: 0x000983B3 File Offset: 0x000967B3
		public virtual void ReleaseObj()
		{
			this.m_BlockData = null;
			this.m_OwnerTrs = null;
		}

		// Token: 0x06001C7E RID: 7294 RVA: 0x000983C4 File Offset: 0x000967C4
		protected void Awake()
		{
			this.m_OwnerTrs = base.GetComponent<Transform>();
			this.m_UpDirFunc = new IRoomObjectControll.UpdateDirFunc(this.BaseUpDirFunc);
			this.UpGridState = new IRoomObjectControll.GridStateFunc(this.GridStateBase);
			this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingOrderBase);
		}

		// Token: 0x06001C7F RID: 7295 RVA: 0x00098414 File Offset: 0x00096814
		protected void Update()
		{
			if (this.m_RoomObjResource != null)
			{
				this.m_RoomObjResource.UpdateRes();
			}
			if (!this.m_IsPreparing)
			{
				this.UpdateObj();
				return;
			}
			if (this.m_RoomObjResource.IsPreparing())
			{
				return;
			}
			this.m_IsPreparing = false;
			this.m_IsAvailable = true;
		}

		// Token: 0x06001C80 RID: 7296 RVA: 0x00098468 File Offset: 0x00096868
		public bool IsAvailable()
		{
			return this.m_IsAvailable;
		}

		// Token: 0x06001C81 RID: 7297 RVA: 0x00098470 File Offset: 0x00096870
		public void Prepare()
		{
			if (this.m_IsAvailable)
			{
				return;
			}
			this.m_IsPreparing = true;
			this.m_RoomObjResource.Prepare();
		}

		// Token: 0x06001C82 RID: 7298 RVA: 0x00098490 File Offset: 0x00096890
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x06001C83 RID: 7299 RVA: 0x00098498 File Offset: 0x00096898
		public virtual void SetUpObjectKey(GameObject hbase)
		{
		}

		// Token: 0x06001C84 RID: 7300 RVA: 0x0009849C File Offset: 0x0009689C
		public void SetLayerPos(float fposz)
		{
			Vector3 localPosition = this.m_OwnerTrs.localPosition;
			localPosition.z = fposz;
			this.m_OwnerTrs.localPosition = localPosition;
		}

		// Token: 0x06001C85 RID: 7301 RVA: 0x000984C9 File Offset: 0x000968C9
		public virtual bool IsAction()
		{
			return false;
		}

		// Token: 0x06001C86 RID: 7302 RVA: 0x000984CC File Offset: 0x000968CC
		public virtual void LinkKeyLock(bool flock)
		{
		}

		// Token: 0x06001C87 RID: 7303 RVA: 0x000984CE File Offset: 0x000968CE
		public virtual void SetAttachLink(bool flock)
		{
		}

		// Token: 0x06001C88 RID: 7304 RVA: 0x000984D0 File Offset: 0x000968D0
		public virtual Transform GetAttachTransform(Vector2 flinkpos)
		{
			return this.m_OwnerTrs;
		}

		// Token: 0x06001C89 RID: 7305 RVA: 0x000984D8 File Offset: 0x000968D8
		public virtual bool IsInRange(int gridX, int gridY, int fdir)
		{
			return false;
		}

		// Token: 0x06001C8A RID: 7306 RVA: 0x000984DB File Offset: 0x000968DB
		public virtual void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
		}

		// Token: 0x06001C8B RID: 7307 RVA: 0x000984DD File Offset: 0x000968DD
		public virtual void SetActionSendObj(IRoomObjectControll plink)
		{
		}

		// Token: 0x06001C8C RID: 7308 RVA: 0x000984DF File Offset: 0x000968DF
		public virtual void LinkObjectParam(int forder, float fposz)
		{
		}

		// Token: 0x06001C8D RID: 7309 RVA: 0x000984E4 File Offset: 0x000968E4
		public virtual void SetGridPosition(int fgridx, int fgridy, int floorID)
		{
			this.m_BlockData.SetPos((float)fgridx, (float)fgridy);
			Vector2 floorPos = this.m_Builder.GetFloorPos(floorID);
			Vector2 vector = RoomUtility.GridToTilePos((float)fgridx, (float)fgridy);
			vector.y -= 0.25f;
			if (this.m_OwnerTrs != null)
			{
				this.m_OwnerTrs.localPosition = new Vector3(vector.x + floorPos.x, vector.y + floorPos.y, 0f);
			}
		}

		// Token: 0x06001C8E RID: 7310 RVA: 0x0009856E File Offset: 0x0009696E
		public virtual void UpdateObj()
		{
		}

		// Token: 0x06001C8F RID: 7311 RVA: 0x00098570 File Offset: 0x00096970
		public eRoomObjectEventType GetEventType()
		{
			return (eRoomObjectEventType)RoomObjectListUtil.GetObjectParam(this.m_Category, this.m_ObjID).m_ObjEventType;
		}

		// Token: 0x06001C90 RID: 7312 RVA: 0x00098598 File Offset: 0x00096998
		public void GetEventParameter(RoomEventParameter pparam)
		{
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(this.m_Category, this.m_ObjID);
			pparam.m_Type = (eRoomObjectEventType)objectParam.m_ObjEventType;
			pparam.m_Table = objectParam.m_ObjEventArgs;
		}

		// Token: 0x06001C91 RID: 7313 RVA: 0x000985D1 File Offset: 0x000969D1
		public CharacterDefine.eDir GetDir()
		{
			return this.m_Dir;
		}

		// Token: 0x06001C92 RID: 7314 RVA: 0x000985D9 File Offset: 0x000969D9
		public CharacterDefine.eDir FlipDir()
		{
			this.SetDir((this.m_Dir != CharacterDefine.eDir.L) ? CharacterDefine.eDir.L : CharacterDefine.eDir.R);
			return this.m_Dir;
		}

		// Token: 0x06001C93 RID: 7315 RVA: 0x000985FC File Offset: 0x000969FC
		public void SetDir(CharacterDefine.eDir dir)
		{
			this.m_Dir = dir;
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				this.m_BlockData.SizeX = this.m_DefaultSizeX;
				this.m_BlockData.SizeY = this.m_DefaultSizeY;
			}
			else
			{
				this.m_BlockData.SizeX = this.m_DefaultSizeY;
				this.m_BlockData.SizeY = this.m_DefaultSizeX;
			}
			this.m_UpDirFunc();
		}

		// Token: 0x06001C94 RID: 7316 RVA: 0x0009866F File Offset: 0x00096A6F
		private void BaseUpDirFunc()
		{
		}

		// Token: 0x06001C95 RID: 7317 RVA: 0x00098674 File Offset: 0x00096A74
		public virtual IVector2 GetPosToSize(int fchkposx, int fchkposy)
		{
			IVector2 result = new IVector2(this.m_DefaultSizeX, this.m_DefaultSizeY);
			if (this.m_Dir != CharacterDefine.eDir.L)
			{
				result.x = this.m_DefaultSizeY;
				result.y = this.m_DefaultSizeX;
			}
			return result;
		}

		// Token: 0x06001C96 RID: 7318 RVA: 0x000986BA File Offset: 0x00096ABA
		public virtual void Destroy()
		{
			if (this.m_RoomObjResource != null)
			{
				this.m_RoomObjResource.Destroy();
				this.m_RoomObjResource = null;
			}
			this.m_IsAvailable = false;
			UnityEngine.Object.Destroy(this);
		}

		// Token: 0x06001C97 RID: 7319 RVA: 0x000986E6 File Offset: 0x00096AE6
		public virtual void SetBaseColor(Color fcolor)
		{
		}

		// Token: 0x06001C98 RID: 7320 RVA: 0x000986E8 File Offset: 0x00096AE8
		public virtual void SetSeleting(bool flg, bool isDisable = false)
		{
		}

		// Token: 0x06001C99 RID: 7321 RVA: 0x000986EC File Offset: 0x00096AEC
		public void SetLayer(int layerNo)
		{
			Transform[] componentsInChildren = base.gameObject.GetComponentsInChildren<Transform>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].gameObject.layer = layerNo;
			}
		}

		// Token: 0x06001C9A RID: 7322 RVA: 0x00098727 File Offset: 0x00096B27
		public virtual void PlayMotion(int fmotionid, WrapMode fmode, float fspeed)
		{
		}

		// Token: 0x06001C9B RID: 7323 RVA: 0x00098729 File Offset: 0x00096B29
		public virtual bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			return true;
		}

		// Token: 0x06001C9C RID: 7324 RVA: 0x0009872C File Offset: 0x00096B2C
		public void GridStateBase(RoomGridState pgrid, int fupstate)
		{
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				pgrid.SetGridStatus(this.BlockData.PosX, this.BlockData.PosY, this.m_DefaultSizeX, this.m_DefaultSizeY, fupstate, this.m_ObjGroupKey);
			}
			else
			{
				pgrid.SetGridStatus(this.BlockData.PosX, this.BlockData.PosY, this.m_DefaultSizeY, this.m_DefaultSizeX, fupstate, this.m_ObjGroupKey);
			}
		}

		// Token: 0x06001C9D RID: 7325 RVA: 0x000987A8 File Offset: 0x00096BA8
		public void SortingOrderBase(float flayerpos, int sortingOrder)
		{
			Vector3 localPosition = this.m_OwnerTrs.localPosition;
			localPosition.z = flayerpos;
			this.m_OwnerTrs.localPosition = localPosition;
		}

		// Token: 0x06001C9E RID: 7326 RVA: 0x000987D5 File Offset: 0x00096BD5
		public virtual bool IsCharaCheck(RoomObjectCtrlChara pbase)
		{
			return false;
		}

		// Token: 0x06001C9F RID: 7327 RVA: 0x000987D8 File Offset: 0x00096BD8
		public virtual void CreateHitAreaModel(bool fhitcolor)
		{
		}

		// Token: 0x06001CA0 RID: 7328 RVA: 0x000987DA File Offset: 0x00096BDA
		public virtual void DestroyHitAreaModel()
		{
		}

		// Token: 0x06001CA1 RID: 7329 RVA: 0x000987DC File Offset: 0x00096BDC
		public virtual void AbortLinkEventObj()
		{
		}

		// Token: 0x06001CA2 RID: 7330 RVA: 0x000987DE File Offset: 0x00096BDE
		public virtual IRoomObjectControll.ModelRenderConfig[] GetSortKey()
		{
			return null;
		}

		// Token: 0x06001CA3 RID: 7331 RVA: 0x000987E1 File Offset: 0x00096BE1
		public virtual void SetSortKey(string ptargetname, int fsortkey)
		{
		}

		// Token: 0x06001CA4 RID: 7332 RVA: 0x000987E3 File Offset: 0x00096BE3
		public virtual void SetSortKey(IRoomObjectControll.ModelRenderConfig[] psort)
		{
		}

		// Token: 0x0400234C RID: 9036
		protected Transform m_OwnerTrs;

		// Token: 0x0400234D RID: 9037
		protected RoomBlockData m_BlockData;

		// Token: 0x0400234E RID: 9038
		protected RoomBuilder m_Builder;

		// Token: 0x0400234F RID: 9039
		protected int m_FloorID;

		// Token: 0x04002350 RID: 9040
		protected long m_ManageID;

		// Token: 0x04002351 RID: 9041
		protected int m_SortCompKey;

		// Token: 0x04002352 RID: 9042
		protected IRoomObjectResource m_RoomObjResource;

		// Token: 0x04002353 RID: 9043
		protected bool m_IsAvailable;

		// Token: 0x04002354 RID: 9044
		protected bool m_IsPreparing;

		// Token: 0x04002355 RID: 9045
		protected eRoomObjectCategory m_Category;

		// Token: 0x04002356 RID: 9046
		protected byte m_ObjGroupKey;

		// Token: 0x04002357 RID: 9047
		protected int m_ObjID;

		// Token: 0x04002358 RID: 9048
		protected CharacterDefine.eDir m_Dir;

		// Token: 0x04002359 RID: 9049
		protected int m_DefaultSizeX = 1;

		// Token: 0x0400235A RID: 9050
		protected int m_DefaultSizeY = 1;

		// Token: 0x0400235B RID: 9051
		protected bool m_Active = true;

		// Token: 0x0400235C RID: 9052
		protected IRoomObjectControll.UpdateDirFunc m_UpDirFunc;

		// Token: 0x0400235D RID: 9053
		public IRoomObjectControll.GridStateFunc UpGridState;

		// Token: 0x0400235E RID: 9054
		public IRoomObjectControll.SortingOrderFunc SetSortingOrder;

		// Token: 0x020005BF RID: 1471
		public class ObjTouchState
		{
			// Token: 0x06001CA5 RID: 7333 RVA: 0x000987E5 File Offset: 0x00096BE5
			public ObjTouchState(RoomGameCamera pcamera)
			{
				this.m_Camera = pcamera;
			}

			// Token: 0x06001CA6 RID: 7334 RVA: 0x000987F4 File Offset: 0x00096BF4
			public bool IsMoveSlide()
			{
				return this.m_Camera.IsCheckTouchSlide(this.m_NowTouch, this.m_TouchStart);
			}

			// Token: 0x0400235F RID: 9055
			public IVector3 m_Grid;

			// Token: 0x04002360 RID: 9056
			public Vector2 m_TouchStart;

			// Token: 0x04002361 RID: 9057
			public Vector2 m_NowTouch;

			// Token: 0x04002362 RID: 9058
			public RoomGameCamera m_Camera;
		}

		// Token: 0x020005C0 RID: 1472
		public struct ModelRenderConfig
		{
			// Token: 0x04002363 RID: 9059
			public short m_RenderSort;

			// Token: 0x04002364 RID: 9060
			public float m_OffsetZ;

			// Token: 0x04002365 RID: 9061
			public Renderer m_RenderObject;
		}

		// Token: 0x020005C1 RID: 1473
		// (Invoke) Token: 0x06001CA8 RID: 7336
		protected delegate void UpdateDirFunc();

		// Token: 0x020005C2 RID: 1474
		// (Invoke) Token: 0x06001CAC RID: 7340
		public delegate void GridStateFunc(RoomGridState pgrid, int fupstate);

		// Token: 0x020005C3 RID: 1475
		// (Invoke) Token: 0x06001CB0 RID: 7344
		public delegate void SortingOrderFunc(float flayerpos, int sortid);
	}
}
