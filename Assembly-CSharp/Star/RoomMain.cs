﻿using System;
using System.Collections.Generic;
using Meige;
using RoomObjectRequestTypes;
using RoomObjectResponseTypes;
using Star.UI;
using Star.UI.Global;
using Star.UI.Room;
using UnityEngine;
using UnityEngine.Events;
using WWWTypes;

namespace Star
{
	// Token: 0x0200046F RID: 1135
	public class RoomMain : GameStateMain
	{
		// Token: 0x060015FB RID: 5627 RVA: 0x0007269C File Offset: 0x00070A9C
		public bool IsIdleStep()
		{
			return this.m_Step == RoomMain.eStep.Idle;
		}

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x060015FC RID: 5628 RVA: 0x000726A7 File Offset: 0x00070AA7
		public RoomBuilder Builder
		{
			get
			{
				return this.m_Builder;
			}
		}

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x060015FD RID: 5629 RVA: 0x000726AF File Offset: 0x00070AAF
		public RoomUI RoomUI
		{
			get
			{
				return this.m_UI;
			}
		}

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x060015FE RID: 5630 RVA: 0x000726B7 File Offset: 0x00070AB7
		public RoomShopListUI ShopUI
		{
			get
			{
				return this.m_ShopUI;
			}
		}

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x060015FF RID: 5631 RVA: 0x000726BF File Offset: 0x00070ABF
		public SceneDefine.eSceneID NextSceneID
		{
			get
			{
				return this.m_NextSceneID;
			}
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x06001601 RID: 5633 RVA: 0x000726D0 File Offset: 0x00070AD0
		// (set) Token: 0x06001600 RID: 5632 RVA: 0x000726C7 File Offset: 0x00070AC7
		public long PlayerID
		{
			get
			{
				return this.m_PlayerID;
			}
			set
			{
				this.m_PlayerID = value;
			}
		}

		// Token: 0x06001602 RID: 5634 RVA: 0x000726D8 File Offset: 0x00070AD8
		private void Start()
		{
			this.m_GameCamera.SetMoveRange(new Rect(-5f, -4f, 10f, 8f));
			base.SetNextState(0);
			this.m_ComEvent = new RoomComManager(16);
		}

		// Token: 0x06001603 RID: 5635 RVA: 0x00072712 File Offset: 0x00070B12
		public override void Destroy()
		{
			base.Destroy();
			this.RoomUI.Destroy();
			this.Builder.Destroy();
			SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.UnloadAll(false);
		}

		// Token: 0x06001604 RID: 5636 RVA: 0x00072740 File Offset: 0x00070B40
		public override bool IsCompleteDestroy()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.IsCompleteUnloadAll() && base.IsCompleteDestroy();
		}

		// Token: 0x06001605 RID: 5637 RVA: 0x00072760 File Offset: 0x00070B60
		public void RefreshTutorialStep()
		{
			if (this.m_UI == null)
			{
				return;
			}
			if (this.m_PlayerID != -1L)
			{
				return;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetRoomTutorialStep() == TutorialManager.eRoomTutorialStep.Buy)
			{
				this.m_RoomObjShopList.MarkRefresh();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.CalcRoomTutorialStep();
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.RightBottom);
			switch (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetRoomTutorialStep())
			{
			case TutorialManager.eRoomTutorialStep.None:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(true);
				break;
			case TutorialManager.eRoomTutorialStep.FirstTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Room_001, new Action(this.RefreshTutorialStep), null);
				break;
			case TutorialManager.eRoomTutorialStep.Buy:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetInteractable(false);
				this.m_UI.RefreshTutorialStep();
				break;
			case TutorialManager.eRoomTutorialStep.MoveTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Room_002, new Action(this.RefreshTutorialStep), null);
				break;
			case TutorialManager.eRoomTutorialStep.EndTips:
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindow(eTutorialTipsListDB.Room_003, new Action(this.RefreshTutorialStep), null);
				break;
			}
		}

		// Token: 0x06001606 RID: 5638 RVA: 0x000728A4 File Offset: 0x00070CA4
		protected override void Update()
		{
			base.Update();
			this.FlushCommand();
			if (this.m_UI != null)
			{
				for (int i = 0; i < this.m_GreetDataStackList.Count; i++)
				{
					Debug.Log(string.Concat(new object[]
					{
						"RequestGreet",
						this.m_GreetDataStackList[i].m_CharaMngID,
						" Category=",
						this.m_GreetDataStackList[i].m_Category
					}));
					this.m_UI.RequestGreet(this.m_GreetDataStackList[i].m_CharaMngID, this.m_GreetDataStackList[i].m_Category);
				}
				this.m_GreetDataStackList.Clear();
				this.m_UI.OpenGreet();
				this.UpdateStep();
			}
		}

		// Token: 0x06001607 RID: 5639 RVA: 0x00072985 File Offset: 0x00070D85
		public void OnComEvent(IMainComCommand pcmd)
		{
			this.m_ComEvent.PushComCommand(pcmd);
		}

		// Token: 0x06001608 RID: 5640 RVA: 0x00072994 File Offset: 0x00070D94
		public void FlushCommand()
		{
			int comCommandNum = this.m_ComEvent.GetComCommandNum();
			for (int i = 0; i < comCommandNum; i++)
			{
				IMainComCommand comCommand = this.m_ComEvent.GetComCommand(i);
				int hashCode = comCommand.GetHashCode();
				if (hashCode != 2)
				{
					if (hashCode != 3)
					{
						if (hashCode == 5)
						{
							this.ChangeStep(RoomMain.eStep.MovePlaceSelect);
						}
					}
					else
					{
						RoomComActionPopup roomComActionPopup = (RoomComActionPopup)comCommand;
						RoomMain.GreetData item = new RoomMain.GreetData((long)((int)roomComActionPopup.m_CharaManageID), roomComActionPopup.m_CategoryID);
						this.m_GreetDataStackList.Add(item);
					}
				}
				else if (!this.m_UI.IsUIHide())
				{
					RoomComUI roomComUI = (RoomComUI)comCommand;
					eRoomUICategory openUI = roomComUI.m_OpenUI;
					if (openUI != eRoomUICategory.CharaTouch)
					{
						if (openUI == eRoomUICategory.CharaView)
						{
							SingletonMonoBehaviour<CharaDetailPlayer>.Inst.OpenProfileOnly(roomComUI.m_CharaID);
						}
					}
					else
					{
						SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(roomComUI.m_ManageID, true);
					}
				}
			}
			this.m_ComEvent.Flush();
		}

		// Token: 0x06001609 RID: 5641 RVA: 0x00072A9C File Offset: 0x00070E9C
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new RoomState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new RoomState_Main(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x0600160A RID: 5642 RVA: 0x00072AE4 File Offset: 0x00070EE4
		private void UpdateStep()
		{
			RoomMain.eStep step = this.m_Step;
			switch (step)
			{
			case RoomMain.eStep.Idle:
				break;
			case RoomMain.eStep.Selecting:
				switch (this.m_UI.GetRoomObjSelectWindow().SelectButton)
				{
				case RoomObjSelectWindow.eButton.Info:
					this.ChangeStep(RoomMain.eStep.ObjectInfo);
					break;
				case RoomObjSelectWindow.eButton.Move:
					this.ChangeStep(RoomMain.eStep.MovePlaceSelect);
					break;
				case RoomObjSelectWindow.eButton.PutAway:
					this.ChangeStep(RoomMain.eStep.StoreConfirm);
					break;
				case RoomObjSelectWindow.eButton.Placement:
					this.m_Builder.RequestPlacement();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				case RoomObjSelectWindow.eButton.Buy:
					this.ChangeStep(RoomMain.eStep.BuyConfirm);
					break;
				case RoomObjSelectWindow.eButton.Flip:
					this.m_Builder.RequestFlip();
					break;
				case RoomObjSelectWindow.eButton.Cancel:
					this.m_Builder.RequestCancelEditMode();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				}
				this.m_UI.GetRoomObjSelectWindow().ResetSelectButton();
				break;
			case RoomMain.eStep.ObjectInfo:
				if (this.m_UI.GetObjInfoWindow().IsDonePlayOut)
				{
					this.ChangeStep(RoomMain.eStep.Idle);
				}
				break;
			case RoomMain.eStep.MovePlaceSelect:
				switch (this.m_UI.GetRoomObjSelectWindow().SelectButton)
				{
				case RoomObjSelectWindow.eButton.Move:
					this.ChangeStep(RoomMain.eStep.MovePlaceSelect);
					break;
				case RoomObjSelectWindow.eButton.PutAway:
					this.ChangeStep(RoomMain.eStep.StoreConfirm);
					break;
				case RoomObjSelectWindow.eButton.Placement:
					this.m_Builder.RequestPlacement();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				case RoomObjSelectWindow.eButton.Buy:
					this.ChangeStep(RoomMain.eStep.BuyConfirm);
					break;
				case RoomObjSelectWindow.eButton.Flip:
					this.m_Builder.RequestFlip();
					break;
				case RoomObjSelectWindow.eButton.Cancel:
					this.m_Builder.RequestCancelEditMode();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				}
				this.m_UI.GetRoomObjSelectWindow().ResetSelectButton();
				break;
			default:
				if (step == RoomMain.eStep.ShopPrepareWait)
				{
					if (this.m_RoomObjShopList.IsAvailable)
					{
						if (!RoomUtility.IsPlacementCountObj(this.m_Category))
						{
							this.ChangeStep(RoomMain.eStep.BuyConfirm);
						}
						else
						{
							this.ChangeStep(RoomMain.eStep.BuyPlaceSelect);
						}
					}
				}
				break;
			case RoomMain.eStep.PutPlaceSelect:
				switch (this.m_UI.GetRoomObjSelectWindow().SelectButton)
				{
				case RoomObjSelectWindow.eButton.Move:
					this.ChangeStep(RoomMain.eStep.MovePlaceSelect);
					break;
				case RoomObjSelectWindow.eButton.PutAway:
					this.ChangeStep(RoomMain.eStep.StoreConfirm);
					break;
				case RoomObjSelectWindow.eButton.Placement:
				{
					RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(this.m_Category, this.m_ObjID);
					if (this.m_Builder.GetFloorFreeSpaceNum(this.m_Category, 0) <= objectParam.m_SizeX * objectParam.m_SizeY)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomConfirmSizeOverTitle, eText_MessageDB.RoomConfirmSizeOver, null);
					}
					else
					{
						this.m_Builder.RequestPlacement();
						this.ChangeStep(RoomMain.eStep.Idle);
					}
					break;
				}
				case RoomObjSelectWindow.eButton.Buy:
					this.ChangeStep(RoomMain.eStep.BuyConfirm);
					break;
				case RoomObjSelectWindow.eButton.Flip:
					this.m_Builder.RequestFlip();
					break;
				case RoomObjSelectWindow.eButton.Cancel:
					this.m_Builder.RequestCancelEditMode();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				}
				this.m_UI.GetRoomObjSelectWindow().ResetSelectButton();
				break;
			case RoomMain.eStep.BuyPlaceSelect:
				switch (this.m_UI.GetRoomObjSelectWindow().SelectButton)
				{
				case RoomObjSelectWindow.eButton.Move:
					this.ChangeStep(RoomMain.eStep.MovePlaceSelect);
					break;
				case RoomObjSelectWindow.eButton.PutAway:
					this.ChangeStep(RoomMain.eStep.StoreConfirm);
					break;
				case RoomObjSelectWindow.eButton.Placement:
					this.m_Builder.RequestPlacement();
					this.ChangeStep(RoomMain.eStep.Idle);
					break;
				case RoomObjSelectWindow.eButton.Buy:
					this.ChangeStep(RoomMain.eStep.BuyConfirm);
					break;
				case RoomObjSelectWindow.eButton.Flip:
					this.m_Builder.RequestFlip();
					break;
				case RoomObjSelectWindow.eButton.Cancel:
					this.m_Builder.RequestCancelEditMode();
					this.m_UI.OnClickShopButtonCallBack();
					break;
				}
				this.m_UI.GetRoomObjSelectWindow().ResetSelectButton();
				break;
			case RoomMain.eStep.ObjList:
				if (this.m_ShopUI.IsMenuEnd())
				{
					if (this.m_ShopUI.Transit == RoomShopListUI.eTransit.Room)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam != null)
						{
							if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_IsBuy)
							{
								this.m_Category = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_Category;
								this.m_ObjID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_RoomObjID;
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = null;
								if (!RoomUtility.IsPlacementCountObj(this.m_Category))
								{
									this.ChangeStep(RoomMain.eStep.BuyConfirm);
								}
								else
								{
									this.ChangeStep(RoomMain.eStep.BuyPlaceSelect);
								}
							}
							else
							{
								this.m_Category = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_Category;
								this.m_ObjID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_RoomObjID;
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = null;
								if (this.m_Category == eRoomObjectCategory.Bedding && this.m_Builder.SelectingObj != null && this.m_Builder.SelectingObj.ObjCategory == eRoomObjectCategory.Bedding)
								{
									this.SwapBedding();
									this.m_Builder.RequestCancelSelectObject();
									this.ChangeStep(RoomMain.eStep.Idle);
								}
								else
								{
									this.ObjPlacement();
								}
							}
						}
					}
					else
					{
						this.ChangeStep(RoomMain.eStep.Idle);
					}
				}
				break;
			}
		}

		// Token: 0x0600160B RID: 5643 RVA: 0x00073054 File Offset: 0x00071454
		private void ChangeStep(RoomMain.eStep step)
		{
			this.m_GameCamera.SetIsControllable(false);
			this.m_Step = step;
			this.m_UI.SetEnableGreet(false);
			switch (step)
			{
			case RoomMain.eStep.Idle:
				this.m_UI.SetEnableGreet(true);
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				this.m_Builder.RequestCancelSelectObject();
				this.m_UI.CloseSelectWindow();
				this.m_UI.OpenMainWindow();
				break;
			case RoomMain.eStep.Selecting:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				this.m_UI.OpenSelectWindowInfo(this.m_Category, this.m_ObjID);
				break;
			case RoomMain.eStep.ObjectInfo:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(false);
				this.m_UI.OpenObjInfoWindow(this.m_Builder.SelectingObj.ObjCategory, this.m_Builder.SelectingObj.ObjID);
				break;
			case RoomMain.eStep.MovePlaceSelect:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				this.m_Builder.RequestEditMode(false);
				this.m_UI.OpenSelectWindowEdit(this.m_Builder.SelectingObj.ObjCategory, this.m_Builder.SelectingObj.ObjID);
				break;
			case RoomMain.eStep.StoreConfirm:
				this.m_UI.CloseSelectWindow();
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				this.m_UI.OpenObjConfirmWindow2(this.m_Builder.SelectingObj.ObjCategory, this.m_Builder.SelectingObj.ObjID, RoomObjConfirmWindow.eMode.Store, new UnityAction(this.OnClosedWindow_ConfirmPutAway));
				break;
			case RoomMain.eStep.PutPlaceSelect:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				this.m_UI.OpenSelectWindowEdit(this.m_Category, this.m_ObjID);
				break;
			case RoomMain.eStep.BuyPlaceSelect:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				this.m_Builder.RequestDummyPlacement(this.m_Category, this.m_ObjID);
				this.m_UI.OpenSelectWindowBuy(this.m_Category, this.m_ObjID);
				this.m_UI.SetEnablePlacementButton(false);
				this.m_UI.SetEnableObjInput(true);
				break;
			case RoomMain.eStep.BuyConfirm:
				this.m_UI.CloseSelectWindow();
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(false);
				this.m_UI.OpenObjBuyWindow(this.m_RoomObjShopList, this.m_Category, this.m_ObjID, new UnityAction(this.OnConfirmBuyPlace));
				break;
			case RoomMain.eStep.BuyPlaceRequest:
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				this.m_RoomObjShopList.ExecuteBuy(this.m_Category, this.m_ObjID, this.m_ObjNum, new Action<long>(this.OnResponseBuyPlaceRoomObject));
				break;
			case RoomMain.eStep.BuyNumSelectRequest:
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				this.m_RoomObjShopList.ExecuteBuy(this.m_Category, this.m_ObjID, this.m_ObjNum, new Action<long>(this.OnResponseBuyNumSelectRoomObject));
				break;
			case RoomMain.eStep.BuyCompleteConfirm:
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(false);
				switch (this.m_Category)
				{
				case eRoomObjectCategory.Floor:
					this.SwapFloorModel(true);
					break;
				case eRoomObjectCategory.Wall:
					this.SwapWallModel(true);
					break;
				case eRoomObjectCategory.Background:
					this.SwapBackground(true);
					break;
				default:
					this.m_Builder.RequestPlacement();
					break;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_PURCHASE, 1f, 0, -1, -1);
				this.m_UI.OpenObjConfirmWindow(this.m_Category, this.m_ObjID, RoomObjConfirmWindow.eMode.BuyAndPlacement, new UnityAction(this.OnResultBuy));
				this.m_Builder.RequestCancelSelectObject();
				break;
			case RoomMain.eStep.ObjList:
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(false);
				break;
			case RoomMain.eStep.BeddingSelect:
			{
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				RoomComSelectFloorObj prequest = new RoomComSelectFloorObj(1, true);
				this.m_Builder.RequestEvent(prequest);
				this.m_UI.OpenObjSelectGroup();
				break;
			}
			case RoomMain.eStep.BeddingSelectBuy:
			{
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(true);
				this.m_GameCamera.SetIsControllable(true);
				RoomComSelectFloorObj prequest2 = new RoomComSelectFloorObj(1, true);
				this.m_Builder.RequestEvent(prequest2);
				this.m_UI.OpenObjSelectGroup();
				break;
			}
			case RoomMain.eStep.SellRequest:
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				this.Request_Sale(this.m_Category, this.m_ObjID, this.m_ObjNum, new MeigewwwParam.Callback(this.OnResponse_Sale));
				break;
			case RoomMain.eStep.SellCompleteConfirm:
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_SELL, 1f, 0, -1, -1);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, "アイテムの売却", "売却しました", null, null);
				this.ChangeStep(RoomMain.eStep.Idle);
				break;
			case RoomMain.eStep.Schedule:
			{
				this.m_UI.SetEnableInput(true);
				this.m_UI.SetEnableObjInput(false);
				this.m_GameCamera.SetIsControllable(false);
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				this.m_UI.OpenScheduleWindow(0, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID());
				break;
			}
			case RoomMain.eStep.StoreAllConfirm:
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.RoomConfirmStoreAllTitle, eText_MessageDB.RoomConfirmStoreAll, new Action<int>(this.OnConfirmStoreAll));
				break;
			case RoomMain.eStep.Transit:
				this.m_UI.SetEnableInput(false);
				this.m_UI.SetEnableObjInput(false);
				break;
			case RoomMain.eStep.ShopStart:
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_IsBuy)
				{
					this.m_Category = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_Category;
					this.m_ObjID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_RoomObjID;
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = null;
					this.m_RoomObjShopList.Open();
					this.ChangeStep(RoomMain.eStep.ShopPrepareWait);
				}
				else
				{
					this.m_Category = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_Category;
					this.m_ObjID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam.m_RoomObjID;
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = null;
					this.ObjPlacement();
				}
				break;
			}
		}

		// Token: 0x0600160C RID: 5644 RVA: 0x000737E4 File Offset: 0x00071BE4
		private void ObjPlacement()
		{
			switch (this.m_Category)
			{
			case eRoomObjectCategory.Floor:
				this.SwapFloorModel(false);
				this.ChangeStep(RoomMain.eStep.Idle);
				break;
			case eRoomObjectCategory.Wall:
				this.SwapWallModel(false);
				this.ChangeStep(RoomMain.eStep.Idle);
				break;
			case eRoomObjectCategory.Background:
				this.SwapBackground(false);
				this.ChangeStep(RoomMain.eStep.Idle);
				break;
			default:
				this.m_Builder.RequestObjectToPlacement(this.m_Category, this.m_ObjID);
				this.ChangeStep(RoomMain.eStep.PutPlaceSelect);
				break;
			}
		}

		// Token: 0x0600160D RID: 5645 RVA: 0x00073870 File Offset: 0x00071C70
		public void InitializeBuilder()
		{
			this.m_Builder.Initialize();
			this.m_Builder.SetOwner(this);
			this.m_Builder.OnEventSend += this.OnComEvent;
			this.m_Builder.OnSelectObject += this.OnSelectObject;
			this.m_Builder.OnUnSelectObject += this.OnUnSelectObject;
			this.m_Builder.OnSelectingObjectCanPlacement += this.OnSelectingObjectCanPlacement;
			this.m_Builder.OnSelectingObjectCanNotPlacement += this.OnSelectingObjectCanNotPlacement;
			this.m_Builder.OnObjectDragStart += this.OnObjectDragStart;
			this.m_Builder.OnObjectDragEnd += this.OnObjectDragEnd;
		}

		// Token: 0x0600160E RID: 5646 RVA: 0x00073938 File Offset: 0x00071D38
		public void OnSelectObject(IRoomObjectControll roomObj, bool ftype)
		{
			this.m_UI.EndUIHide();
			if (ftype)
			{
				if (roomObj.ObjCategory == eRoomObjectCategory.Bedding)
				{
					RoomMain.eStep step = this.m_Step;
					if (step != RoomMain.eStep.Idle)
					{
						if (step != RoomMain.eStep.BeddingSelect)
						{
							if (step == RoomMain.eStep.BeddingSelectBuy)
							{
								this.m_ObjManageID = roomObj.ManageID;
								this.ChangeStep(RoomMain.eStep.BuyConfirm);
							}
						}
						else
						{
							this.m_UI.CloseObjSelectGroup();
							this.m_UI.OpenMainWindow();
							this.m_ObjManageID = roomObj.ManageID;
							this.SwapBedding();
							this.m_Builder.RequestCancelSelectObject();
							this.m_Step = RoomMain.eStep.Idle;
						}
					}
					else
					{
						this.m_ObjManageID = roomObj.ManageID;
						this.ChangeStep(RoomMain.eStep.ObjList);
						this.m_ShopUI.SetMode(RoomObjListWindow.eMode.HaveBedding);
						this.m_ShopUI.PlayIn();
					}
				}
			}
			else if (this.m_Step == RoomMain.eStep.Idle)
			{
				if (roomObj.ObjCategory == eRoomObjectCategory.Msgboard)
				{
					this.m_UI.OpenSelectWindowMission();
				}
				else
				{
					this.m_Category = roomObj.ObjCategory;
					this.m_ObjManageID = roomObj.ManageID;
					this.m_ObjID = roomObj.ObjID;
					this.ChangeStep(RoomMain.eStep.Selecting);
				}
			}
		}

		// Token: 0x0600160F RID: 5647 RVA: 0x00073A63 File Offset: 0x00071E63
		public void OnUnSelectObject()
		{
			if (this.m_Step == RoomMain.eStep.Selecting || this.m_Step == RoomMain.eStep.MovePlaceSelect || this.m_Step == RoomMain.eStep.PutPlaceSelect || this.m_Step == RoomMain.eStep.BuyPlaceSelect)
			{
				this.ChangeStep(RoomMain.eStep.Idle);
			}
		}

		// Token: 0x06001610 RID: 5648 RVA: 0x00073A9C File Offset: 0x00071E9C
		public void OnSelectingObjectCanPlacement()
		{
			this.m_UI.SetEnablePlacementButton(true);
		}

		// Token: 0x06001611 RID: 5649 RVA: 0x00073AAA File Offset: 0x00071EAA
		public void OnSelectingObjectCanNotPlacement()
		{
			this.m_UI.SetEnablePlacementButton(false);
		}

		// Token: 0x06001612 RID: 5650 RVA: 0x00073AB8 File Offset: 0x00071EB8
		public void OnObjectDragStart()
		{
			this.m_GameCamera.SetIsControllable(false);
		}

		// Token: 0x06001613 RID: 5651 RVA: 0x00073AC6 File Offset: 0x00071EC6
		public void OnObjectDragEnd()
		{
			this.m_GameCamera.SetIsControllable(true);
		}

		// Token: 0x06001614 RID: 5652 RVA: 0x00073AD4 File Offset: 0x00071ED4
		public bool InitializeUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<RoomUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.RoomUI].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_ShopUI = UIUtility.GetMenuComponent<RoomShopListUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.RoomShopListUI].m_SceneName);
			if (this.m_ShopUI == null)
			{
				return false;
			}
			this.m_UI.Setup(this, this.m_ShopUI, this.m_PlayerID);
			this.m_ShopUI.Setup(this.m_RoomObjShopList);
			this.m_UI.OnClickTownButton += this.OnClickBackButton;
			this.m_UI.OnClickBackButton_Selecting += this.OnClickBackButton_Selecting;
			this.m_UI.OnCloseGreet += this.OnCloseGreet;
			this.m_UI.OnCancelSelectBed += this.OnCancelSelectBed;
			this.m_UI.OnDecideLiveCharas += this.OnDecideLiveChara;
			this.m_UI.OnClickScheduleButton += this.OnClickScheduleButton;
			this.m_UI.OnClosedScheduleWindow += this.OnCloseSchedule;
			this.m_UI.OnClickStoreAllButton += this.OnClickStoreAll;
			this.m_UI.OnClickRoomChangeButton += this.OnClickRoomChangeCallBack;
			this.m_UI.OnClickFriendRoomButton += this.OnClickFriendRoomCallBack;
			this.m_UI.OnOpenObjList += this.OnOpenObjListCallBack;
			this.m_UI.SetEnableInput(false);
			this.m_UI.PlayIn();
			if (!UserRoomUtil.IsHaveSubRoom(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Room);
			}
			else if (UserRoomUtil.GetManageIDToUserRoomData(this.m_Builder.GetManageID()).FloorID == 1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.RoomMain);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.RoomSub);
			}
			return true;
		}

		// Token: 0x06001615 RID: 5653 RVA: 0x00073CFC File Offset: 0x000720FC
		public void SetupUIOnRoomStart()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam != null)
			{
				this.ChangeStep(RoomMain.eStep.ShopStart);
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.Idle);
			}
		}

		// Token: 0x06001616 RID: 5654 RVA: 0x00073D26 File Offset: 0x00072126
		public void OnOpenObjListCallBack()
		{
			this.ChangeStep(RoomMain.eStep.ObjList);
		}

		// Token: 0x06001617 RID: 5655 RVA: 0x00073D30 File Offset: 0x00072130
		private void OnClosedWindow_ConfirmPutAway()
		{
			this.m_GameCamera.SetIsControllable(true);
			if (this.m_UI.GetObjConfirmWindow().LastPressedFooterButtonID == 0)
			{
				if (UserRoomUtil.GetUserRoomObjRemainNum() < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit)
				{
					this.m_Builder.RequestPutAway();
					this.m_Builder.RequestCancelSelectObject();
					this.ChangeStep(RoomMain.eStep.Idle);
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomConfirmStoreOverTitle, eText_MessageDB.RoomConfirmStoreOver, new Action<int>(this.OnConfirmStoreAll));
					this.ChangeStep(RoomMain.eStep.Selecting);
				}
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.Selecting);
			}
		}

		// Token: 0x06001618 RID: 5656 RVA: 0x00073DD8 File Offset: 0x000721D8
		public void OnConfirmBuyPlace()
		{
			if (this.m_UI.GetObjBuyWindow().SelectButton == RoomObjBuyWindow.eButton.Yes)
			{
				RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(this.m_Category, this.m_ObjID);
				if (this.m_Builder.GetFloorFreeSpaceNum(this.m_Category, 0) <= objectParam.m_SizeX * objectParam.m_SizeY)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomConfirmSizeOverTitle, eText_MessageDB.RoomConfirmSizeOver, new Action<int>(this.OnConfirmSizeOver));
				}
				else
				{
					this.m_ObjNum = 1;
					this.ChangeStep(RoomMain.eStep.BuyPlaceRequest);
				}
			}
			else
			{
				this.m_Builder.RequestCancelEditMode();
				this.m_Builder.RequestCancelSelectObject();
				this.m_UI.OnClickShopButtonCallBack();
			}
		}

		// Token: 0x06001619 RID: 5657 RVA: 0x00073E92 File Offset: 0x00072292
		private void OnConfirmSizeOver(int btn)
		{
			this.m_Builder.RequestCancelEditMode();
			this.m_Builder.RequestCancelSelectObject();
			this.m_UI.OnClickShopButtonCallBack();
		}

		// Token: 0x0600161A RID: 5658 RVA: 0x00073EB5 File Offset: 0x000722B5
		private void OnResponseBuyPlaceRoomObject(long mngObjID)
		{
			if (mngObjID != -1L)
			{
				this.m_Builder.LinkPlaceObjectToManageID(mngObjID);
				this.ChangeStep(RoomMain.eStep.BuyCompleteConfirm);
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.Idle);
			}
		}

		// Token: 0x0600161B RID: 5659 RVA: 0x00073EDF File Offset: 0x000722DF
		private void OnResponseBuyNumSelectRoomObject(long mngObjID)
		{
			if (mngObjID != -1L)
			{
				this.ChangeStep(RoomMain.eStep.BuyCompleteConfirm);
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.Idle);
			}
		}

		// Token: 0x0600161C RID: 5660 RVA: 0x00073F00 File Offset: 0x00072300
		public void OnResultBuy()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				this.RefreshTutorialStep();
				this.ChangeStep(RoomMain.eStep.Idle);
			}
			else if (this.m_UI.GetObjConfirmWindow().LastPressedFooterButtonID == 0)
			{
				this.m_UI.OnClickShopButtonCallBack();
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.BuyPlaceSelect);
			}
		}

		// Token: 0x0600161D RID: 5661 RVA: 0x00073F5F File Offset: 0x0007235F
		private void OnCancelSelectBed()
		{
			this.ChangeStep(RoomMain.eStep.Idle);
		}

		// Token: 0x0600161E RID: 5662 RVA: 0x00073F68 File Offset: 0x00072368
		private void SwapBedding()
		{
			Debug.Log(string.Concat(new object[]
			{
				"SwapBedding Category=",
				this.m_Category,
				" m_ObjID=",
				this.m_ObjID,
				" SelectingObj",
				this.m_Builder.SelectingObj
			}));
			UserRoomObjectData userRoomObjData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData(this.m_Category, this.m_ObjID);
			UserRoomListData.PlayerFloorState playerRoomList = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.GetPlayerRoomList(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
			if (playerRoomList != null)
			{
				int roomNum = playerRoomList.GetRoomNum();
				for (int i = 0; i < roomNum; i++)
				{
					UserRoomData.PlacementData placementManageID = playerRoomList.GetRoomData(i).GetPlacementManageID(this.m_ObjManageID);
					if (placementManageID != null)
					{
						UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(placementManageID.m_ManageID);
						if (manageIDToUserRoomObjData != null)
						{
							manageIDToUserRoomObjData.SetFreeLinkMngID(placementManageID.m_ManageID);
						}
						placementManageID.m_ObjectID = userRoomObjData.ResourceID;
						placementManageID.m_ManageID = userRoomObjData.GetFreeLinkMngID(true);
						this.m_ObjManageID = placementManageID.m_ManageID;
						break;
					}
				}
			}
			RoomComModelChange roomComModelChange = new RoomComModelChange();
			roomComModelChange.SetUpChangeModel(this.m_Builder.SelectingObj, this.m_ObjManageID, this.m_ObjID);
			this.m_Builder.RequestCancelSelectObject();
			this.m_Builder.RequestEvent(roomComModelChange);
		}

		// Token: 0x0600161F RID: 5663 RVA: 0x000740D0 File Offset: 0x000724D0
		private void SwapBackground(bool fsetup)
		{
			RoomComBGChange prequest = new RoomComBGChange(this.m_ObjID, fsetup);
			this.m_Builder.RequestEvent(prequest);
		}

		// Token: 0x06001620 RID: 5664 RVA: 0x000740F8 File Offset: 0x000724F8
		private void SwapFloorModel(bool fsetup)
		{
			RoomComFloorChange prequest = new RoomComFloorChange(this.m_ObjID, fsetup);
			this.m_Builder.RequestEvent(prequest);
		}

		// Token: 0x06001621 RID: 5665 RVA: 0x00074120 File Offset: 0x00072520
		private void SwapWallModel(bool fsetup)
		{
			RoomComWallChange prequest = new RoomComWallChange(this.m_ObjID, fsetup);
			this.m_Builder.RequestEvent(prequest);
		}

		// Token: 0x06001622 RID: 5666 RVA: 0x00074146 File Offset: 0x00072546
		private void OnClosedADVWindow(int advID)
		{
			this.m_UI.SetEnableInput(false);
			this.m_NextSceneID = SceneDefine.eSceneID.ADV;
			this.ChangeStep(RoomMain.eStep.Transit);
		}

		// Token: 0x06001623 RID: 5667 RVA: 0x00074163 File Offset: 0x00072563
		private void OnClickBackButton()
		{
			this.m_NextSceneID = SceneDefine.eSceneID.Town;
			this.ChangeStep(RoomMain.eStep.Transit);
		}

		// Token: 0x06001624 RID: 5668 RVA: 0x00074174 File Offset: 0x00072574
		private void OnClickHomeButton()
		{
			this.m_UI.SetEnableInput(false);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart = true;
			this.m_NextSceneID = SceneDefine.eSceneID.Town;
			this.ChangeStep(RoomMain.eStep.Transit);
		}

		// Token: 0x06001625 RID: 5669 RVA: 0x000741A1 File Offset: 0x000725A1
		private void OnClickBackButton_Selecting()
		{
			this.m_UI.CloseSelectWindow();
			this.m_UI.OpenMainWindow();
		}

		// Token: 0x06001626 RID: 5670 RVA: 0x000741B9 File Offset: 0x000725B9
		private void OnCloseGreet()
		{
		}

		// Token: 0x06001627 RID: 5671 RVA: 0x000741BB File Offset: 0x000725BB
		private void OnClickScheduleButton()
		{
			this.ChangeStep(RoomMain.eStep.Schedule);
		}

		// Token: 0x06001628 RID: 5672 RVA: 0x000741C8 File Offset: 0x000725C8
		private void OnDecideLiveChara(long[] charaMngIDs)
		{
			if (charaMngIDs != null)
			{
				UserFieldCharaData.UpFieldCharaData[] charaMngTable = UserFieldCharaData.CreateSetUpData(charaMngIDs);
				this.m_Builder.RefreshChara(charaMngTable);
			}
		}

		// Token: 0x06001629 RID: 5673 RVA: 0x000741EE File Offset: 0x000725EE
		private void OnCloseSchedule()
		{
			this.m_GameCamera.SetIsControllable(true);
			this.ChangeStep(RoomMain.eStep.Idle);
		}

		// Token: 0x0600162A RID: 5674 RVA: 0x00074203 File Offset: 0x00072603
		private void OnClickStoreAll()
		{
			this.ChangeStep(RoomMain.eStep.StoreAllConfirm);
		}

		// Token: 0x0600162B RID: 5675 RVA: 0x00074210 File Offset: 0x00072610
		private void OnConfirmStoreAll(int select)
		{
			if (select == 0)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit <= UserRoomUtil.GetUserRoomObjRemainNum())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomConfirmStoreAllTitle, eText_MessageDB.RoomConfirmStoreAllFailedAll, null);
				}
				else
				{
					if (UserRoomUtil.GetUserRoomObjPlacementNum() > SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit - UserRoomUtil.GetUserRoomObjRemainNum())
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomConfirmStoreAllTitle, eText_MessageDB.RoomConfirmStoreAllFailed, null);
					}
					this.ExecuteStoreAll();
				}
				this.ChangeStep(RoomMain.eStep.Idle);
			}
			else
			{
				this.ChangeStep(RoomMain.eStep.Idle);
			}
		}

		// Token: 0x0600162C RID: 5676 RVA: 0x000742B9 File Offset: 0x000726B9
		public void ExecuteStoreAll()
		{
			this.m_Builder.RequestPutAll();
		}

		// Token: 0x0600162D RID: 5677 RVA: 0x000742C8 File Offset: 0x000726C8
		private void OnClickRoomChangeCallBack()
		{
			if (UserRoomUtil.GetManageIDToUserRoomData(this.m_Builder.GetManageID()).FloorID == 1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.RoomConfirmRoomChangeTitle, eText_MessageDB.RoomConfirmRoomChangeSub, new Action<int>(this.OnConfirmRoomChange));
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.RoomConfirmRoomChangeTitle, eText_MessageDB.RoomConfirmRoomChangeMain, new Action<int>(this.OnConfirmRoomChange));
			}
		}

		// Token: 0x0600162E RID: 5678 RVA: 0x00074341 File Offset: 0x00072741
		private void OnClickFriendRoomCallBack()
		{
			this.ExecuteRoomChange();
		}

		// Token: 0x0600162F RID: 5679 RVA: 0x00074349 File Offset: 0x00072749
		private void OnConfirmRoomChange(int btn)
		{
			if (btn == 0 && UserRoomUtil.SetRoomChange(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID))
			{
				this.ExecuteRoomChange();
			}
		}

		// Token: 0x06001630 RID: 5680 RVA: 0x00074375 File Offset: 0x00072775
		private void ExecuteRoomChange()
		{
			this.m_NextSceneID = SceneDefine.eSceneID.Room;
			base.NextTransitSceneID = SceneDefine.eSceneID.Room;
			this.RoomUI.PlayOut();
			this.ChangeStep(RoomMain.eStep.Transit);
		}

		// Token: 0x06001631 RID: 5681 RVA: 0x00074398 File Offset: 0x00072798
		public RoomObjShopList GetShopObjList()
		{
			return this.m_RoomObjShopList;
		}

		// Token: 0x06001632 RID: 5682 RVA: 0x000743A0 File Offset: 0x000727A0
		public void Request_Sale(eRoomObjectCategory category, int objID, int objNum, MeigewwwParam.Callback callback)
		{
			RoomObjectRequestTypes.Sale sale = new RoomObjectRequestTypes.Sale();
			UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(category, objID);
			List<long> list = new List<long>();
			for (int i = 0; i < userRoomObjData.HaveNum; i++)
			{
				if (!userRoomObjData.m_MngIDList[i].m_LinkUse)
				{
					list.Add(userRoomObjData.m_MngIDList[i].m_ManageID);
					if (list.Count >= objNum)
					{
						break;
					}
				}
			}
			sale.managedRoomObjectId = APIUtility.ArrayToStr<long>(list.ToArray());
			MeigewwwParam wwwParam = RoomObjectRequest.Sale(sale, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06001633 RID: 5683 RVA: 0x00074448 File Offset: 0x00072848
		private void OnResponse_Sale(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			RoomObjectResponseTypes.Sale sale = RoomObjectResponse.Sale(wwwParam, ResponseCommon.DialogType.None, null);
			if (sale == null)
			{
				return;
			}
			ResultCode result = sale.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(sale.player, userDataMng.UserData);
				APIUtility.wwwToUserData(sale.managedRoomObjects, userDataMng.UserRoomObjDatas);
				this.ChangeStep(RoomMain.eStep.SellCompleteConfirm);
			}
		}

		// Token: 0x04001CB8 RID: 7352
		public const int STATE_INIT = 0;

		// Token: 0x04001CB9 RID: 7353
		public const int STATE_MAIN = 1;

		// Token: 0x04001CBA RID: 7354
		private RoomMain.eStep m_Step;

		// Token: 0x04001CBB RID: 7355
		[SerializeField]
		private RoomGameCamera m_GameCamera;

		// Token: 0x04001CBC RID: 7356
		[SerializeField]
		private RoomBuilder m_Builder;

		// Token: 0x04001CBD RID: 7357
		private RoomUI m_UI;

		// Token: 0x04001CBE RID: 7358
		private RoomShopListUI m_ShopUI;

		// Token: 0x04001CBF RID: 7359
		private SceneDefine.eSceneID m_NextSceneID = SceneDefine.eSceneID.None;

		// Token: 0x04001CC0 RID: 7360
		private eRoomObjectCategory m_Category;

		// Token: 0x04001CC1 RID: 7361
		private int m_ObjID;

		// Token: 0x04001CC2 RID: 7362
		private long m_ObjManageID;

		// Token: 0x04001CC3 RID: 7363
		private int m_ObjNum;

		// Token: 0x04001CC4 RID: 7364
		private List<RoomMain.GreetData> m_GreetDataStackList = new List<RoomMain.GreetData>();

		// Token: 0x04001CC5 RID: 7365
		private long m_PlayerID = -1L;

		// Token: 0x04001CC6 RID: 7366
		private RoomObjShopList m_RoomObjShopList = new RoomObjShopList();

		// Token: 0x04001CC7 RID: 7367
		private RoomComManager m_ComEvent;

		// Token: 0x02000470 RID: 1136
		private enum eStep
		{
			// Token: 0x04001CC9 RID: 7369
			Idle,
			// Token: 0x04001CCA RID: 7370
			Selecting,
			// Token: 0x04001CCB RID: 7371
			ObjectInfo,
			// Token: 0x04001CCC RID: 7372
			MovePlaceSelect,
			// Token: 0x04001CCD RID: 7373
			StoreConfirm,
			// Token: 0x04001CCE RID: 7374
			PutPlaceSelect,
			// Token: 0x04001CCF RID: 7375
			BuyPlaceSelect,
			// Token: 0x04001CD0 RID: 7376
			BuyConfirm,
			// Token: 0x04001CD1 RID: 7377
			BuyStoreConfirm,
			// Token: 0x04001CD2 RID: 7378
			BuyPlaceRequest,
			// Token: 0x04001CD3 RID: 7379
			BuyNumSelectRequest,
			// Token: 0x04001CD4 RID: 7380
			BuyCompleteConfirm,
			// Token: 0x04001CD5 RID: 7381
			ObjList,
			// Token: 0x04001CD6 RID: 7382
			BeddingSelect,
			// Token: 0x04001CD7 RID: 7383
			BeddingSelectBuy,
			// Token: 0x04001CD8 RID: 7384
			BeddingBuyConfirm,
			// Token: 0x04001CD9 RID: 7385
			SellConfirm,
			// Token: 0x04001CDA RID: 7386
			SellRequest,
			// Token: 0x04001CDB RID: 7387
			SellCompleteConfirm,
			// Token: 0x04001CDC RID: 7388
			Schedule,
			// Token: 0x04001CDD RID: 7389
			StoreAllConfirm,
			// Token: 0x04001CDE RID: 7390
			Transit,
			// Token: 0x04001CDF RID: 7391
			ShopStart,
			// Token: 0x04001CE0 RID: 7392
			ShopPrepareWait
		}

		// Token: 0x02000471 RID: 1137
		private class GreetData
		{
			// Token: 0x06001634 RID: 5684 RVA: 0x000744D1 File Offset: 0x000728D1
			public GreetData(long charaMngID, RoomGreet.eCategory category)
			{
				this.m_CharaMngID = charaMngID;
				this.m_Category = category;
			}

			// Token: 0x04001CE1 RID: 7393
			public long m_CharaMngID;

			// Token: 0x04001CE2 RID: 7394
			public RoomGreet.eCategory m_Category;
		}
	}
}
