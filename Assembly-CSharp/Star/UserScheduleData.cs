﻿using System;

namespace Star
{
	// Token: 0x02000B42 RID: 2882
	public class UserScheduleData
	{
		// Token: 0x02000B43 RID: 2883
		public enum eType
		{
			// Token: 0x04004490 RID: 17552
			Non,
			// Token: 0x04004491 RID: 17553
			Sleep,
			// Token: 0x04004492 RID: 17554
			Room,
			// Token: 0x04004493 RID: 17555
			Office,
			// Token: 0x04004494 RID: 17556
			Town,
			// Token: 0x04004495 RID: 17557
			System,
			// Token: 0x04004496 RID: 17558
			End
		}

		// Token: 0x02000B44 RID: 2884
		public enum eDropCategory
		{
			// Token: 0x04004498 RID: 17560
			Money,
			// Token: 0x04004499 RID: 17561
			Item,
			// Token: 0x0400449A RID: 17562
			KRRPoint,
			// Token: 0x0400449B RID: 17563
			Stamina,
			// Token: 0x0400449C RID: 17564
			FriendShip
		}

		// Token: 0x02000B45 RID: 2885
		public class DropState
		{
			// Token: 0x0400449D RID: 17565
			public UserScheduleData.eDropCategory m_Category;

			// Token: 0x0400449E RID: 17566
			public int m_CategoryNo;

			// Token: 0x0400449F RID: 17567
			public int m_Num;
		}

		// Token: 0x02000B46 RID: 2886
		public class Seg
		{
			// Token: 0x040044A0 RID: 17568
			public UserScheduleData.eType m_Type;

			// Token: 0x040044A1 RID: 17569
			public int m_WakeTime;

			// Token: 0x040044A2 RID: 17570
			public int m_UseTime;

			// Token: 0x040044A3 RID: 17571
			public int m_TagNameID;
		}

		// Token: 0x02000B47 RID: 2887
		public class ListPack
		{
			// Token: 0x06003D2D RID: 15661 RVA: 0x00136648 File Offset: 0x00134A48
			public int GetListNum()
			{
				return this.m_Table.Length;
			}

			// Token: 0x06003D2E RID: 15662 RVA: 0x00136652 File Offset: 0x00134A52
			public UserScheduleData.Seg GetTable(int findex)
			{
				return this.m_Table[findex];
			}

			// Token: 0x040044A4 RID: 17572
			public UserScheduleData.Seg[] m_Table;

			// Token: 0x040044A5 RID: 17573
			public long m_SettingTime;

			// Token: 0x040044A6 RID: 17574
			public long m_ChangeStateTime;
		}

		// Token: 0x02000B48 RID: 2888
		public class Play
		{
			// Token: 0x06003D2F RID: 15663 RVA: 0x0013665C File Offset: 0x00134A5C
			public Play()
			{
				this.m_DropItemID = -1;
				this.m_DropMagKey = 0f;
			}

			// Token: 0x06003D30 RID: 15664 RVA: 0x00136676 File Offset: 0x00134A76
			public void StackList(UserScheduleData.ListPack plist)
			{
				if (this.m_DayListUp == null)
				{
					this.m_DayListUp = new UserScheduleData.ListPack[2];
				}
				this.m_DayListUp[1] = this.m_DayListUp[0];
				this.m_DayListUp[0] = plist;
			}

			// Token: 0x040044A7 RID: 17575
			public UserScheduleData.ListPack[] m_DayListUp;

			// Token: 0x040044A8 RID: 17576
			public long m_PlayTime;

			// Token: 0x040044A9 RID: 17577
			public int m_Version;

			// Token: 0x040044AA RID: 17578
			public int m_DropItemID;

			// Token: 0x040044AB RID: 17579
			public float m_DropMagKey;
		}
	}
}
