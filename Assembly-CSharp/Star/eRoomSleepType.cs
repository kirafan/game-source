﻿using System;

namespace Star
{
	// Token: 0x02000533 RID: 1331
	public enum eRoomSleepType
	{
		// Token: 0x0400210A RID: 8458
		Loop,
		// Token: 0x0400210B RID: 8459
		OneShot,
		// Token: 0x0400210C RID: 8460
		SetUpSleep
	}
}
