﻿using System;
using FieldPartyMemberResponseTypes;

namespace Star
{
	// Token: 0x020005A3 RID: 1443
	public class RoomComPlayerFieldParty : IFldNetComModule
	{
		// Token: 0x06001BE4 RID: 7140 RVA: 0x00093AA0 File Offset: 0x00091EA0
		public RoomComPlayerFieldParty(long fplayerid, RoomFriendFieldCharaIO pio) : base(IFldNetComManager.Instance)
		{
			this.m_PlayerId = fplayerid;
			this.m_Step = RoomComPlayerFieldParty.eStep.GetState;
			this.m_IO = pio;
		}

		// Token: 0x06001BE5 RID: 7141 RVA: 0x00093AC4 File Offset: 0x00091EC4
		public bool SetUp()
		{
			RoomComAPIFriendFieldPartyGetAll roomComAPIFriendFieldPartyGetAll = new RoomComAPIFriendFieldPartyGetAll();
			roomComAPIFriendFieldPartyGetAll.playerId = this.m_PlayerId;
			this.m_NetMng.SendCom(roomComAPIFriendFieldPartyGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06001BE6 RID: 7142 RVA: 0x00093B00 File Offset: 0x00091F00
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				this.m_IO.m_ListUp = new UserFieldCharaData();
				if (getAll.fieldPartyMembers.Length != 0)
				{
					for (int i = 0; i < getAll.fieldPartyMembers.Length; i++)
					{
						if (!this.m_IO.m_ListUp.IsEntryFldChara(getAll.fieldPartyMembers[i].managedCharacterId))
						{
							this.m_IO.m_ListUp.AddRoomInChara(getAll.fieldPartyMembers[i]);
						}
					}
				}
				this.m_Step = RoomComPlayerFieldParty.eStep.End;
			}
		}

		// Token: 0x06001BE7 RID: 7143 RVA: 0x00093B98 File Offset: 0x00091F98
		public override bool UpFunc()
		{
			bool result = true;
			RoomComPlayerFieldParty.eStep step = this.m_Step;
			if (step != RoomComPlayerFieldParty.eStep.GetState)
			{
				if (step != RoomComPlayerFieldParty.eStep.WaitCheck)
				{
					if (step == RoomComPlayerFieldParty.eStep.End)
					{
						result = false;
					}
				}
			}
			else
			{
				this.SetUp();
				this.m_Step = RoomComPlayerFieldParty.eStep.WaitCheck;
			}
			return result;
		}

		// Token: 0x040022DC RID: 8924
		private RoomComPlayerFieldParty.eStep m_Step;

		// Token: 0x040022DD RID: 8925
		private long m_PlayerId;

		// Token: 0x040022DE RID: 8926
		private RoomFriendFieldCharaIO m_IO;

		// Token: 0x020005A4 RID: 1444
		public enum eStep
		{
			// Token: 0x040022E0 RID: 8928
			GetState,
			// Token: 0x040022E1 RID: 8929
			WaitCheck,
			// Token: 0x040022E2 RID: 8930
			End
		}
	}
}
