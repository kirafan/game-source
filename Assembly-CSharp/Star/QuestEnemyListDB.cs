﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001E6 RID: 486
	public class QuestEnemyListDB : ScriptableObject
	{
		// Token: 0x04000BC3 RID: 3011
		public QuestEnemyListDB_Param[] m_Params;
	}
}
