﻿using System;

namespace Star
{
	// Token: 0x02000517 RID: 1303
	public class MovieObjectHandler
	{
		// Token: 0x060019B0 RID: 6576 RVA: 0x00085BC8 File Offset: 0x00083FC8
		public void OnMngConstruct(MovieObject o)
		{
			if (o != null)
			{
				this.m_Object = o;
				this.m_Object.AddRef();
				this.m_Object.Constructed();
			}
		}

		// Token: 0x060019B1 RID: 6577 RVA: 0x00085BF0 File Offset: 0x00083FF0
		public void OnMngDestruct(bool stopIfStatusIsNotRemoved)
		{
			if (this.m_Object != null)
			{
				this.m_Object.CallRemoved();
				if (stopIfStatusIsNotRemoved && this.m_Object.GetStatus() != MovieObject.eStatus.Removed)
				{
					this.m_Object.Stop();
				}
				this.m_Object.RemoveRef();
				this.m_Object = null;
			}
		}

		// Token: 0x060019B2 RID: 6578 RVA: 0x00085C48 File Offset: 0x00084048
		public bool IsAvailable()
		{
			return this.m_Object != null;
		}

		// Token: 0x060019B3 RID: 6579 RVA: 0x00085C58 File Offset: 0x00084058
		public bool IsCompletePrepare()
		{
			return this.m_Object != null && this.m_Object.IsCompletePrepare();
		}

		// Token: 0x060019B4 RID: 6580 RVA: 0x00085C72 File Offset: 0x00084072
		public bool IsPaused()
		{
			return this.m_Object != null && this.m_Object.IsPaused();
		}

		// Token: 0x060019B5 RID: 6581 RVA: 0x00085C8C File Offset: 0x0008408C
		public bool IsPlaying(bool isIgnorePaused = false)
		{
			return this.m_Object != null && this.m_Object.GetStatus() == MovieObject.eStatus.Playing && (isIgnorePaused || !this.m_Object.IsPaused());
		}

		// Token: 0x060019B6 RID: 6582 RVA: 0x00085CC2 File Offset: 0x000840C2
		public bool Play()
		{
			return this.m_Object != null && this.m_Object.Play(null, null, false, false, null, null);
		}

		// Token: 0x060019B7 RID: 6583 RVA: 0x00085CE2 File Offset: 0x000840E2
		public bool Stop()
		{
			return this.m_Object != null && this.m_Object.Stop();
		}

		// Token: 0x060019B8 RID: 6584 RVA: 0x00085CFC File Offset: 0x000840FC
		public bool Suspend()
		{
			return this.m_Object != null && this.m_Object.Suspend();
		}

		// Token: 0x060019B9 RID: 6585 RVA: 0x00085D16 File Offset: 0x00084116
		public bool Resume()
		{
			if (this.m_Object != null)
			{
				this.m_Object.Resume();
			}
			return false;
		}

		// Token: 0x060019BA RID: 6586 RVA: 0x00085D30 File Offset: 0x00084130
		public bool SetVolume(float volume)
		{
			if (this.m_Object == null)
			{
				return false;
			}
			this.m_Object.SetVolume(volume);
			return true;
		}

		// Token: 0x04002062 RID: 8290
		private MovieObject m_Object;
	}
}
