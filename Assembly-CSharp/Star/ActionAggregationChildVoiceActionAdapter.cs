﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200008A RID: 138
	[AddComponentMenu("ActionAggregation/Voice")]
	[Serializable]
	public class ActionAggregationChildVoiceActionAdapter : ActionAggregationChildActionAdapter
	{
		// Token: 0x0600042A RID: 1066 RVA: 0x000146F1 File Offset: 0x00012AF1
		public override void Update()
		{
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x000146F4 File Offset: 0x00012AF4
		public override void PlayStartExtendProcess()
		{
			if (APIUtility.IsNeedSignup())
			{
				this.m_RequestCueSheetIndex = 0;
				this.m_RequestNamedIndex = -1;
			}
			if (this.m_RequestCueSheetIndex != -1 || this.m_RequestNamedIndex != -1)
			{
				int num;
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleCall, out num, out num, this.m_RequestCueSheetIndex, this.m_RequestNamedIndex, false);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleCall, out this.m_RequestCueSheetIndex, out this.m_RequestNamedIndex, -1, -1, false);
			}
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x00014778 File Offset: 0x00012B78
		public override void Skip()
		{
			base.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x00014781 File Offset: 0x00012B81
		public void SetRequestData(int requestCueSheetIndex, int requestNamedIndex)
		{
			this.m_RequestCueSheetIndex = requestCueSheetIndex;
			this.m_RequestNamedIndex = requestNamedIndex;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00014791 File Offset: 0x00012B91
		public int GetRequestCueSheetIndex()
		{
			return this.m_RequestCueSheetIndex;
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x00014799 File Offset: 0x00012B99
		public int GetRequestNamedIndex()
		{
			return this.m_RequestNamedIndex;
		}

		// Token: 0x04000247 RID: 583
		[SerializeField]
		[Tooltip("タイトルボイス：リクエストしたパラメータ")]
		private int m_RequestCueSheetIndex = -1;

		// Token: 0x04000248 RID: 584
		[SerializeField]
		[Tooltip("タイトルボイス：リクエストしたパラメータ")]
		private int m_RequestNamedIndex = -1;
	}
}
