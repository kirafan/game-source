﻿using System;
using System.Collections;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000417 RID: 1047
	public class MixEffectScene : MonoBehaviour
	{
		// Token: 0x06001409 RID: 5129 RVA: 0x0006ADFD File Offset: 0x000691FD
		private void Start()
		{
			this.m_Camera.enabled = false;
		}

		// Token: 0x0600140A RID: 5130 RVA: 0x0006AE0B File Offset: 0x0006920B
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x0600140B RID: 5131 RVA: 0x0006AE14 File Offset: 0x00069214
		private void Update()
		{
			this.m_Loader.Update();
			MixEffectScene.eStep step = this.m_Step;
			if (step != MixEffectScene.eStep.Prepare_Wait)
			{
				if (step != MixEffectScene.eStep.PlayEffect_Wait)
				{
					if (step != MixEffectScene.eStep.PlayLoop_Wait)
					{
					}
				}
				else if (!this.IsPlaying())
				{
					this.SetStep(MixEffectScene.eStep.None);
				}
			}
			else
			{
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.m_Step = MixEffectScene.eStep.Prepare_Error;
						return;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						return;
					}
				}
				if (this.m_SpriteHndl == null || this.m_SpriteHndl.IsAvailable())
				{
					if (!this.m_IsDonePrepareFirst)
					{
						GameObject gameObject = this.m_LoadingHndl.FindObj(MixEffectScene.MODEL_OBJECT_PATHS[(int)this.m_MixType], true) as GameObject;
						if (gameObject != null)
						{
							this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, Vector3.zero, Quaternion.Euler(0f, 0f, 0f), base.transform);
						}
						MeigeAnimClipHolder[] array = new MeigeAnimClipHolder[2];
						for (int i = 0; i < array.Length; i++)
						{
							GameObject gameObject2 = this.m_LoadingHndl.FindObj(MixEffectScene.ANIM_OBJECT_PATHS[(int)this.m_MixType, i], true) as GameObject;
							if (gameObject2 != null)
							{
								array[i] = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
							}
						}
						string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(gameObject.name, false);
						this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
						Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
						component.cullingType = AnimationCullingType.AlwaysAnimate;
						this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
						this.m_MeigeAnimCtrl.Open();
						for (int j = 0; j < array.Length; j++)
						{
							this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, array[j]);
						}
						this.m_MeigeAnimCtrl.Close();
						int layer = LayerMask.NameToLayer("Graphic2D");
						this.SetLayerRecursively(this.m_BodyObj, layer);
						int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
						for (int k = 0; k < particleEmitterNum; k++)
						{
							MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(k);
							if (particleEmitter != null)
							{
								particleEmitter.SetLayer(layer);
							}
						}
						if (this.m_LoadingHndl != null)
						{
							this.m_Loader.Unload(this.m_LoadingHndl);
							this.m_LoadingHndl = null;
						}
					}
					this.m_IsDonePrepareFirst = true;
					if (this.m_SpriteHndl != null && this.m_SpriteHndl.Obj != null)
					{
						this.m_Texture = this.m_SpriteHndl.Obj.texture;
					}
					this.ApplyCharaIllustTexture();
					this.SetStep(MixEffectScene.eStep.None);
				}
			}
		}

		// Token: 0x0600140C RID: 5132 RVA: 0x0006B0F0 File Offset: 0x000694F0
		public void Prepare(int charaID)
		{
			int charaID2 = this.m_CharaID;
			this.m_CharaID = charaID;
			this.m_Camera.enabled = false;
			if (this.m_LoadingHndl == null)
			{
				this.m_LoadingHndl = this.m_Loader.Load(MixEffectScene.RESOURCE_PATHS[(int)this.m_MixType], new MeigeResource.Option[0]);
			}
			if (this.m_CharaID != charaID2)
			{
				this.m_SpriteHndl = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustChara(this.m_CharaID);
			}
			this.SetStep(MixEffectScene.eStep.Prepare_Wait);
		}

		// Token: 0x0600140D RID: 5133 RVA: 0x0006B173 File Offset: 0x00069573
		public bool IsCompletePrepare()
		{
			return this.m_Step < MixEffectScene.eStep.Prepare || this.m_Step > MixEffectScene.eStep.Prepare_Wait;
		}

		// Token: 0x0600140E RID: 5134 RVA: 0x0006B193 File Offset: 0x00069593
		public bool IsPrepareError()
		{
			return this.m_Step == MixEffectScene.eStep.Prepare_Error;
		}

		// Token: 0x0600140F RID: 5135 RVA: 0x0006B1A0 File Offset: 0x000695A0
		public void Play(MixEffectScene.eAnimType animType)
		{
			this.m_Camera.enabled = true;
			this.m_BodyObj.SetActive(true);
			this.m_MsbHndl.AttachCamera(0, this.m_Camera);
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play(MixEffectScene.ANIM_KEYS[(int)animType], 0f, (animType != MixEffectScene.eAnimType.Loop) ? WrapMode.ClampForever : WrapMode.Loop);
			if (animType == MixEffectScene.eAnimType.Effect)
			{
				this.m_SoundHndl = new SoundHandler();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(MixEffectScene.CUE_IDS[(int)this.m_MixType], this.m_SoundHndl, 1f, 0, -1, -1);
			}
			this.SetStep(MixEffectScene.eStep.PlayEffect_Wait);
		}

		// Token: 0x06001410 RID: 5136 RVA: 0x0006B26E File Offset: 0x0006966E
		public bool IsCompletePlay()
		{
			return (this.m_Step < MixEffectScene.eStep.PlayEffect || this.m_Step > MixEffectScene.eStep.PlayEffect_Wait) && (this.m_Step < MixEffectScene.eStep.PlayLoop || this.m_Step > MixEffectScene.eStep.PlayLoop_Wait);
		}

		// Token: 0x06001411 RID: 5137 RVA: 0x0006B2A9 File Offset: 0x000696A9
		public void OnSkip()
		{
			if (this.m_SoundHndl != null)
			{
				this.m_SoundHndl.Stop();
			}
		}

		// Token: 0x06001412 RID: 5138 RVA: 0x0006B2C4 File Offset: 0x000696C4
		public float GetPlayingSec(MixEffectScene.eAnimType animType)
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler(MixEffectScene.ANIM_KEYS[(int)animType]);
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime * this.m_MeigeAnimCtrl.m_NormalizedPlayTime;
			}
			return 0f;
		}

		// Token: 0x06001413 RID: 5139 RVA: 0x0006B304 File Offset: 0x00069704
		public float GetMaxSec(MixEffectScene.eAnimType animType)
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler(MixEffectScene.ANIM_KEYS[(int)animType]);
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime;
			}
			return 0f;
		}

		// Token: 0x06001414 RID: 5140 RVA: 0x0006B338 File Offset: 0x00069738
		public void Destroy()
		{
			this.m_IsDonePrepareFirst = false;
			if (this.m_MsbHndl != null)
			{
				this.m_MsbHndl.DetachCamera();
			}
			if (this.m_LoadingHndl != null)
			{
				this.m_Loader.Unload(this.m_LoadingHndl);
				this.m_LoadingHndl = null;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHndl);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHndl, true);
			}
			this.m_SpriteHndl = null;
			this.m_SoundHndl = null;
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			this.m_BodyObj = null;
			this.m_Texture = null;
		}

		// Token: 0x06001415 RID: 5141 RVA: 0x0006B3F7 File Offset: 0x000697F7
		public void Suspend()
		{
			this.m_Camera.enabled = false;
			this.m_BodyObj.SetActive(false);
			this.m_MsbHndl.DetachCamera();
			this.SetStep(MixEffectScene.eStep.None);
		}

		// Token: 0x06001416 RID: 5142 RVA: 0x0006B423 File Offset: 0x00069823
		private void SetStep(MixEffectScene.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x06001417 RID: 5143 RVA: 0x0006B42C File Offset: 0x0006982C
		private void SetLayerRecursively(GameObject self, int layer)
		{
			self.layer = layer;
			IEnumerator enumerator = self.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					this.SetLayerRecursively(transform.gameObject, layer);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x06001418 RID: 5144 RVA: 0x0006B4A0 File Offset: 0x000698A0
		private void ApplyCharaIllustTexture()
		{
			if (this.m_Texture != null)
			{
				MsbObjectHandler msbObjectHandlerByName = this.m_MsbHndl.GetMsbObjectHandlerByName(MixEffectScene.OBJ_CHARA_ILLUSTS[(int)this.m_MixType]);
				if (msbObjectHandlerByName != null)
				{
					Renderer renderer = msbObjectHandlerByName.GetRenderer();
					if (renderer != null && renderer.material != null)
					{
						MeigeShaderUtility.SetTexture(renderer.material, this.m_Texture, eTextureType.eTextureType_Albedo, 0);
					}
				}
			}
		}

		// Token: 0x06001419 RID: 5145 RVA: 0x0006B513 File Offset: 0x00069913
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x0600141A RID: 5146 RVA: 0x0006B530 File Offset: 0x00069930
		// Note: this type is marked as 'beforefieldinit'.
		static MixEffectScene()
		{
			string[,] array = new string[3, 2];
			array[0, 0] = "MeigeAC_Mix_Upgrade@effect";
			array[0, 1] = "MeigeAC_Mix_Upgrade@loop";
			array[1, 0] = "MeigeAC_Mix_LimitBreak@effect";
			array[1, 1] = "MeigeAC_Mix_LimitBreak@loop";
			array[2, 0] = "MeigeAC_Mix_Evolution@effect";
			array[2, 1] = "MeigeAC_Mix_Evolution@loop";
			MixEffectScene.ANIM_OBJECT_PATHS = array;
			MixEffectScene.CUE_IDS = new eSoundSeListDB[]
			{
				eSoundSeListDB.MIX_UPGRADE,
				eSoundSeListDB.MIX_LIMITBREAK,
				eSoundSeListDB.MIX_EVOLUTION
			};
		}

		// Token: 0x04001AF0 RID: 6896
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x04001AF1 RID: 6897
		private static readonly string[] OBJ_CHARA_ILLUSTS = new string[]
		{
			"Mix_card",
			"Mix_card",
			"Mix_card"
		};

		// Token: 0x04001AF2 RID: 6898
		private static readonly string[] ANIM_KEYS = new string[]
		{
			"effect",
			"loop"
		};

		// Token: 0x04001AF3 RID: 6899
		private static readonly string[] RESOURCE_PATHS = new string[]
		{
			"mix/mix_upgrade.muast",
			"mix/mix_limitbreak.muast",
			"mix/mix_evolution.muast"
		};

		// Token: 0x04001AF4 RID: 6900
		private static readonly string[] MODEL_OBJECT_PATHS = new string[]
		{
			"Mix_Upgrade",
			"Mix_LimitBreak",
			"Mix_Evolution"
		};

		// Token: 0x04001AF5 RID: 6901
		private static readonly string[,] ANIM_OBJECT_PATHS;

		// Token: 0x04001AF6 RID: 6902
		private static readonly eSoundSeListDB[] CUE_IDS;

		// Token: 0x04001AF7 RID: 6903
		[SerializeField]
		private MixEffectScene.eMixType m_MixType;

		// Token: 0x04001AF8 RID: 6904
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04001AF9 RID: 6905
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04001AFA RID: 6906
		private MixEffectScene.eStep m_Step = MixEffectScene.eStep.None;

		// Token: 0x04001AFB RID: 6907
		private bool m_IsDonePrepareFirst;

		// Token: 0x04001AFC RID: 6908
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x04001AFD RID: 6909
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04001AFE RID: 6910
		private SpriteHandler m_SpriteHndl;

		// Token: 0x04001AFF RID: 6911
		private Transform m_BaseTransform;

		// Token: 0x04001B00 RID: 6912
		private MsbHandler m_MsbHndl;

		// Token: 0x04001B01 RID: 6913
		private GameObject m_BodyObj;

		// Token: 0x04001B02 RID: 6914
		private Texture m_Texture;

		// Token: 0x04001B03 RID: 6915
		private int m_CharaID = -1;

		// Token: 0x04001B04 RID: 6916
		private SoundHandler m_SoundHndl;

		// Token: 0x02000418 RID: 1048
		public enum eMixType
		{
			// Token: 0x04001B06 RID: 6918
			Upgrade,
			// Token: 0x04001B07 RID: 6919
			LimitBreak,
			// Token: 0x04001B08 RID: 6920
			Evolution,
			// Token: 0x04001B09 RID: 6921
			Num
		}

		// Token: 0x02000419 RID: 1049
		public enum eAnimType
		{
			// Token: 0x04001B0B RID: 6923
			Effect,
			// Token: 0x04001B0C RID: 6924
			Loop,
			// Token: 0x04001B0D RID: 6925
			Num
		}

		// Token: 0x0200041A RID: 1050
		private enum eStep
		{
			// Token: 0x04001B0F RID: 6927
			None = -1,
			// Token: 0x04001B10 RID: 6928
			Prepare,
			// Token: 0x04001B11 RID: 6929
			Prepare_Wait,
			// Token: 0x04001B12 RID: 6930
			Prepare_Error,
			// Token: 0x04001B13 RID: 6931
			PlayEffect,
			// Token: 0x04001B14 RID: 6932
			PlayEffect_Wait,
			// Token: 0x04001B15 RID: 6933
			PlayLoop,
			// Token: 0x04001B16 RID: 6934
			PlayLoop_Wait
		}
	}
}
