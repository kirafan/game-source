﻿using System;

namespace Star
{
	// Token: 0x020002AA RID: 682
	public class AfterEvolutionParamWrapper : HaveBeforeAfterCharaParamCharacterEditSceneCharacterParamWrapper<EditUtility.SimulateEvolutionParam>
	{
		// Token: 0x06000CD9 RID: 3289 RVA: 0x00048F67 File Offset: 0x00047367
		public AfterEvolutionParamWrapper(EditUtility.SimulateEvolutionParam simulateParam) : base(eCharacterDataType.AfterEvolutionParam, simulateParam)
		{
		}

		// Token: 0x06000CDA RID: 3290 RVA: 0x00048F71 File Offset: 0x00047371
		public override int GetCharaId()
		{
			if (this.m_Param == null)
			{
				return base.GetCharaId();
			}
			return this.m_Param.afterCharaId;
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x00048F90 File Offset: 0x00047390
		public override int GetMaxLv()
		{
			if (this.m_Param == null)
			{
				return base.GetMaxLv();
			}
			return this.m_Param.afterMaxLv;
		}

		// Token: 0x06000CDC RID: 3292 RVA: 0x00048FAF File Offset: 0x000473AF
		public override int GetLimitBreak()
		{
			if (this.m_Param == null)
			{
				return base.GetLimitBreak();
			}
			return this.m_Param.afterLimitBreak;
		}
	}
}
