﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000372 RID: 882
	public class IObjectResourceHandle
	{
		// Token: 0x060010B0 RID: 4272 RVA: 0x00057CEC File Offset: 0x000560EC
		public IObjectResourceHandle()
		{
			this.m_UseNum = 1;
			this.m_Handle = null;
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x00057D02 File Offset: 0x00056102
		public bool IsDone()
		{
			return this.m_Active;
		}

		// Token: 0x060010B2 RID: 4274 RVA: 0x00057D0A File Offset: 0x0005610A
		public void Release()
		{
			this.Obj = null;
			this.m_Active = false;
			if (this.m_Handle != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_Handle);
				this.m_Handle = null;
			}
		}

		// Token: 0x060010B3 RID: 4275 RVA: 0x00057D37 File Offset: 0x00056137
		public virtual void SetUpResource(UnityEngine.Object pres)
		{
		}

		// Token: 0x040017A3 RID: 6051
		public bool m_Active;

		// Token: 0x040017A4 RID: 6052
		public bool m_ReadWait;

		// Token: 0x040017A5 RID: 6053
		public bool m_Web;

		// Token: 0x040017A6 RID: 6054
		public int m_MappingNo;

		// Token: 0x040017A7 RID: 6055
		public int m_UseNum;

		// Token: 0x040017A8 RID: 6056
		public uint m_UID;

		// Token: 0x040017A9 RID: 6057
		public string m_FileName;

		// Token: 0x040017AA RID: 6058
		public string m_FileExt;

		// Token: 0x040017AB RID: 6059
		public UnityEngine.Object Obj;

		// Token: 0x040017AC RID: 6060
		public MeigeResource.Handler m_Handle;
	}
}
