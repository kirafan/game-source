﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x0200048D RID: 1165
	public class ShopState_WeaponCreate : ShopState
	{
		// Token: 0x060016D0 RID: 5840 RVA: 0x00077087 File Offset: 0x00075487
		public ShopState_WeaponCreate(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016D1 RID: 5841 RVA: 0x0007709F File Offset: 0x0007549F
		public override int GetStateID()
		{
			return 11;
		}

		// Token: 0x060016D2 RID: 5842 RVA: 0x000770A3 File Offset: 0x000754A3
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponCreate.eStep.First;
		}

		// Token: 0x060016D3 RID: 5843 RVA: 0x000770AC File Offset: 0x000754AC
		public override void OnStateExit()
		{
		}

		// Token: 0x060016D4 RID: 5844 RVA: 0x000770AE File Offset: 0x000754AE
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016D5 RID: 5845 RVA: 0x000770B8 File Offset: 0x000754B8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponCreate.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponCreate.eStep.LoadWait;
				break;
			case ShopState_WeaponCreate.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponCreate.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponCreate.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_WPN_CREATE, null, null);
					this.m_Step = ShopState_WeaponCreate.eStep.Main;
				}
				break;
			case ShopState_WeaponCreate.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI.Destroy();
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 10;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponCreate.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponCreate.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_WeaponCreate.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016D6 RID: 5846 RVA: 0x000771FC File Offset: 0x000755FC
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopWeaponCreateUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickExecuteButton += this.OnClickExecuteButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponCreate);
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_208", true);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060016D7 RID: 5847 RVA: 0x000772B3 File Offset: 0x000756B3
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060016D8 RID: 5848 RVA: 0x000772C2 File Offset: 0x000756C2
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060016D9 RID: 5849 RVA: 0x000772DE File Offset: 0x000756DE
		private void OnClickExecuteButton(int recipeID)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_WeaponMake(recipeID, new Action<MeigewwwParam, Weaponmake>(this.OnResponse_WeaponMake));
		}

		// Token: 0x060016DA RID: 5850 RVA: 0x00077300 File Offset: 0x00075700
		private void OnResponse_WeaponMake(MeigewwwParam wwwParam, Weaponmake param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.GOLD_IS_SHORT && result != ResultCode.ITEM_IS_SHORT)
			{
				if (result != ResultCode.SUCCESS)
				{
					if (result != ResultCode.WEAPON_LIMIT)
					{
						APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					}
					else
					{
						APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					}
				}
				else
				{
					this.m_UI.StartCreateEffect();
				}
			}
			else
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
		}

		// Token: 0x04001D8B RID: 7563
		private ShopState_WeaponCreate.eStep m_Step = ShopState_WeaponCreate.eStep.None;

		// Token: 0x04001D8C RID: 7564
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopWeaponCreateUI;

		// Token: 0x04001D8D RID: 7565
		private ShopWeaponCreateUI m_UI;

		// Token: 0x0200048E RID: 1166
		private enum eStep
		{
			// Token: 0x04001D8F RID: 7567
			None = -1,
			// Token: 0x04001D90 RID: 7568
			First,
			// Token: 0x04001D91 RID: 7569
			LoadStart,
			// Token: 0x04001D92 RID: 7570
			LoadWait,
			// Token: 0x04001D93 RID: 7571
			PlayIn,
			// Token: 0x04001D94 RID: 7572
			Main,
			// Token: 0x04001D95 RID: 7573
			UnloadChildSceneWait
		}
	}
}
