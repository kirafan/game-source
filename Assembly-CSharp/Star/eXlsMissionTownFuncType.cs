﻿using System;

namespace Star
{
	// Token: 0x020004F4 RID: 1268
	public enum eXlsMissionTownFuncType
	{
		// Token: 0x04001FC8 RID: 8136
		Building,
		// Token: 0x04001FC9 RID: 8137
		Move,
		// Token: 0x04001FCA RID: 8138
		Remove,
		// Token: 0x04001FCB RID: 8139
		CharaTouchItem,
		// Token: 0x04001FCC RID: 8140
		ContentBuild,
		// Token: 0x04001FCD RID: 8141
		BufBuild,
		// Token: 0x04001FCE RID: 8142
		BuildTouchItem
	}
}
