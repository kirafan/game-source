﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000B3C RID: 2876
	[Serializable]
	public class UserBattlePartyData
	{
		// Token: 0x06003C87 RID: 15495 RVA: 0x00135791 File Offset: 0x00133B91
		public UserBattlePartyData()
		{
			this.MngID = -1L;
			this.ClearSlotAll();
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06003C88 RID: 15496 RVA: 0x001357A7 File Offset: 0x00133BA7
		// (set) Token: 0x06003C89 RID: 15497 RVA: 0x001357AF File Offset: 0x00133BAF
		public long MngID { get; set; }

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06003C8A RID: 15498 RVA: 0x001357B8 File Offset: 0x00133BB8
		// (set) Token: 0x06003C8B RID: 15499 RVA: 0x001357C0 File Offset: 0x00133BC0
		public string PartyName { get; set; }

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06003C8C RID: 15500 RVA: 0x001357C9 File Offset: 0x00133BC9
		// (set) Token: 0x06003C8D RID: 15501 RVA: 0x001357D1 File Offset: 0x00133BD1
		public long[] CharaMngIDs { get; set; }

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06003C8E RID: 15502 RVA: 0x001357DA File Offset: 0x00133BDA
		// (set) Token: 0x06003C8F RID: 15503 RVA: 0x001357E2 File Offset: 0x00133BE2
		public long[] WeaponMngIDs { get; set; }

		// Token: 0x06003C90 RID: 15504 RVA: 0x001357EB File Offset: 0x00133BEB
		public int GetSlotNum()
		{
			return this.CharaMngIDs.Length;
		}

		// Token: 0x06003C91 RID: 15505 RVA: 0x001357F8 File Offset: 0x00133BF8
		public void ClearSlotAll()
		{
			int num = 5;
			this.CharaMngIDs = new long[num];
			for (int i = 0; i < num; i++)
			{
				this.CharaMngIDs[i] = -1L;
			}
			this.WeaponMngIDs = new long[num];
			for (int j = 0; j < num; j++)
			{
				this.WeaponMngIDs[j] = -1L;
			}
		}

		// Token: 0x06003C92 RID: 15506 RVA: 0x00135857 File Offset: 0x00133C57
		public long GetMemberAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Length)
			{
				return this.CharaMngIDs[slotIndex];
			}
			return -1L;
		}

		// Token: 0x06003C93 RID: 15507 RVA: 0x0013587C File Offset: 0x00133C7C
		public int GetMemberIdx(long charaMngID)
		{
			for (int i = 0; i < this.CharaMngIDs.Length; i++)
			{
				if (this.CharaMngIDs[i] == charaMngID)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06003C94 RID: 15508 RVA: 0x001358B3 File Offset: 0x00133CB3
		public void SetMemberAt(int slotIndex, long charaMngID)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Length)
			{
				this.CharaMngIDs[slotIndex] = charaMngID;
			}
		}

		// Token: 0x06003C95 RID: 15509 RVA: 0x001358D3 File Offset: 0x00133CD3
		public void EmptyMemberAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.CharaMngIDs.Length)
			{
				this.CharaMngIDs[slotIndex] = -1L;
			}
		}

		// Token: 0x06003C96 RID: 15510 RVA: 0x001358F4 File Offset: 0x00133CF4
		public void EmptyMember(long charaMngID)
		{
			for (int i = 0; i < this.CharaMngIDs.Length; i++)
			{
				if (this.CharaMngIDs[i] == charaMngID)
				{
					this.CharaMngIDs[i] = -1L;
					break;
				}
			}
		}

		// Token: 0x06003C97 RID: 15511 RVA: 0x00135938 File Offset: 0x00133D38
		public List<long> GetAvailableMemberMngIDs()
		{
			List<long> list = new List<long>();
			for (int i = 0; i < this.CharaMngIDs.Length; i++)
			{
				if (this.CharaMngIDs[i] != -1L)
				{
					list.Add(this.CharaMngIDs[i]);
				}
			}
			return list;
		}

		// Token: 0x06003C98 RID: 15512 RVA: 0x00135982 File Offset: 0x00133D82
		public long GetWeaponAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Length)
			{
				return this.WeaponMngIDs[slotIndex];
			}
			return -1L;
		}

		// Token: 0x06003C99 RID: 15513 RVA: 0x001359A4 File Offset: 0x00133DA4
		public void SetWeaponAt(int slotIndex, long weaponMngID)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Length)
			{
				this.WeaponMngIDs[slotIndex] = weaponMngID;
			}
		}

		// Token: 0x06003C9A RID: 15514 RVA: 0x001359C4 File Offset: 0x00133DC4
		public void EmptyWeaponAt(int slotIndex)
		{
			if (slotIndex >= 0 && slotIndex < this.WeaponMngIDs.Length)
			{
				this.WeaponMngIDs[slotIndex] = -1L;
			}
		}

		// Token: 0x06003C9B RID: 15515 RVA: 0x001359E8 File Offset: 0x00133DE8
		public void EmptyWeapon(long weaponMngID)
		{
			for (int i = 0; i < this.WeaponMngIDs.Length; i++)
			{
				if (this.WeaponMngIDs[i] == weaponMngID)
				{
					this.WeaponMngIDs[i] = -1L;
					break;
				}
			}
		}
	}
}
