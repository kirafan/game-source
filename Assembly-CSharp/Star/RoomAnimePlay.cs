﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000602 RID: 1538
	public static class RoomAnimePlay
	{
		// Token: 0x06001E3D RID: 7741 RVA: 0x000A2B9C File Offset: 0x000A0F9C
		public static void DatabaseSetUp()
		{
			if (RoomAnimePlay.ms_RoomAnimeTagDB == null)
			{
				RoomAnimePlay.ms_RoomAnimeTagDB = new RoomAnimePlay.RoomAnimePlayTagList();
				RoomAnimePlay.ms_RoomAnimeTagDB.LoadResInManager("Prefab/Room/Action/RoomDataAnimeTag", ".bytes");
			}
		}

		// Token: 0x06001E3E RID: 7742 RVA: 0x000A2BC6 File Offset: 0x000A0FC6
		public static bool IsSetUp()
		{
			return RoomAnimePlay.ms_RoomAnimeTagDB.m_Active;
		}

		// Token: 0x06001E3F RID: 7743 RVA: 0x000A2BD2 File Offset: 0x000A0FD2
		public static void DatabaseRelease()
		{
			if (RoomAnimePlay.ms_RoomAnimeTagDB != null)
			{
				RoomAnimePlay.ms_RoomAnimeTagDB.Release();
			}
			RoomAnimePlay.ms_RoomAnimeTagDB = null;
		}

		// Token: 0x06001E40 RID: 7744 RVA: 0x000A2BEE File Offset: 0x000A0FEE
		public static void GetRoomCharacterAnimSettings(List<AnimSettings> settings, int partsIndex, int foptioncode, int fkey)
		{
			RoomAnimePlay.ms_RoomAnimeTagDB.m_AnimeTag.CreateSetting(settings, partsIndex, foptioncode, fkey);
		}

		// Token: 0x06001E41 RID: 7745 RVA: 0x000A2C04 File Offset: 0x000A1004
		public static void BodyAnimeUp(CharacterHandler pchara, MeigeResource.Handler phandle, int fkeycode)
		{
			if (phandle.IsDone)
			{
				GameObject asset = phandle.GetAsset<GameObject>();
				pchara.SetupAnimClip_Open(1);
				McatFileHelper component = asset.GetComponent<McatFileHelper>();
				for (int i = 0; i < component.m_Table.Length; i++)
				{
					pchara.SetupAnimClip(component.m_Table[i].m_AnimeName, component.m_Table[i].m_MabFile, 1);
				}
				pchara.SetupAnimClip_Close(1);
			}
		}

		// Token: 0x06001E42 RID: 7746 RVA: 0x000A2C7C File Offset: 0x000A107C
		public static void HeadAnimeUp(CharacterHandler pchara, MeigeResource.Handler phandle, int fkeycode)
		{
			if (phandle.IsDone)
			{
				GameObject asset = phandle.GetAsset<GameObject>();
				pchara.SetupAnimClip_Open(0);
				McatFileHelper component = asset.GetComponent<McatFileHelper>();
				for (int i = 0; i < component.m_Table.Length; i++)
				{
					pchara.SetupAnimClip(component.m_Table[i].m_AnimeName, component.m_Table[i].m_MabFile, 0);
				}
				pchara.SetupAnimClip_Close(0);
			}
		}

		// Token: 0x06001E43 RID: 7747 RVA: 0x000A2CF4 File Offset: 0x000A10F4
		public static RoomAnimAccessMap CreateAnimeMap(int fkey)
		{
			RoomAnimAccessMap roomAnimAccessMap = new RoomAnimAccessMap();
			RoomAnimePlay.XlsRoomAnimeTagData animeTag = RoomAnimePlay.ms_RoomAnimeTagDB.m_AnimeTag;
			string[] array = new string[]
			{
				"_L",
				"_R"
			};
			int count = animeTag.m_AnimeAccess.Count;
			roomAnimAccessMap.m_BodyMap = new RoomAnimAccessKey[count];
			int num = 0;
			for (int i = 0; i < count; i++)
			{
				RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess xlsAnimaAccess = animeTag.m_AnimeAccess[i];
				bool flag = true;
				if (xlsAnimaAccess.m_Type != 0 && ((int)xlsAnimaAccess.m_Type & fkey) == 0)
				{
					flag = false;
				}
				if (flag)
				{
					roomAnimAccessMap.m_BodyMap[num].m_AccessKey = xlsAnimaAccess.m_KeyCode;
					int num2 = 2;
					if (xlsAnimaAccess.m_LR == 0)
					{
						num2 = 1;
					}
					switch (xlsAnimaAccess.m_Num)
					{
					case 1:
					case 2:
					{
						roomAnimAccessMap.m_BodyMap[num].m_KeyName = new string[2];
						int j;
						for (j = 0; j < num2; j++)
						{
							roomAnimAccessMap.m_BodyMap[num].m_KeyName[j] = string.Format("{0}{1}", xlsAnimaAccess.m_BaseName, array[j]);
						}
						if (j < 2)
						{
							roomAnimAccessMap.m_BodyMap[num].m_KeyName[1] = roomAnimAccessMap.m_BodyMap[num].m_KeyName[0];
						}
						break;
					}
					case 3:
						roomAnimAccessMap.m_BodyMap[num].m_KeyName = new string[6];
						for (int j = 0; j < num2; j++)
						{
							roomAnimAccessMap.m_BodyMap[num].m_KeyName[j] = string.Format("{0}_st{1}", xlsAnimaAccess.m_BaseName, array[j]);
							roomAnimAccessMap.m_BodyMap[num].m_KeyName[2 + j] = string.Format("{0}_lp{1}", xlsAnimaAccess.m_BaseName, array[j]);
							roomAnimAccessMap.m_BodyMap[num].m_KeyName[4 + j] = string.Format("{0}_ed{1}", xlsAnimaAccess.m_BaseName, array[j]);
						}
						break;
					}
					num++;
				}
			}
			return roomAnimAccessMap;
		}

		// Token: 0x06001E44 RID: 7748 RVA: 0x000A2F28 File Offset: 0x000A1328
		public static RoomAcitionWakeUp GetNextActionParam(RoomCharaActMode.eMode fmode)
		{
			RoomAcitionWakeUp result = null;
			RoomAnimePlay.XlsRoomAnimeTagData animeTag = RoomAnimePlay.ms_RoomAnimeTagDB.m_AnimeTag;
			int count = animeTag.m_ActionAccess.Count;
			for (int i = 0; i < count; i++)
			{
				if (animeTag.m_ActionAccess[i].m_Key == fmode)
				{
					result = animeTag.m_ActionAccess[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x040024D1 RID: 9425
		public static RoomAnimePlay.RoomAnimePlayTagList ms_RoomAnimeTagDB;

		// Token: 0x02000603 RID: 1539
		public class BuildUpSettings
		{
			// Token: 0x06001E45 RID: 7749 RVA: 0x000A2F8A File Offset: 0x000A138A
			public BuildUpSettings(string fkeyname, string filename, uint fcategory)
			{
				this.m_AnimationClipPath = filename;
				this.m_ActionKey = fkeyname;
				this.m_Category = fcategory;
			}

			// Token: 0x040024D2 RID: 9426
			public string m_AnimationClipPath;

			// Token: 0x040024D3 RID: 9427
			public string m_ActionKey;

			// Token: 0x040024D4 RID: 9428
			public uint m_Category;
		}

		// Token: 0x02000604 RID: 1540
		public class XlsRoomAnimeTagData : IXlsBinForm
		{
			// Token: 0x06001E47 RID: 7751 RVA: 0x000A2FC8 File Offset: 0x000A13C8
			public override void SheetDataTag(string psheetname, int ftablenum, BinaryReader pfiles)
			{
				if (psheetname == "animedata")
				{
					for (int i = 0; i < ftablenum; i++)
					{
						RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess xlsAnimaAccess = new RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess();
						xlsAnimaAccess.m_KeyCode = CRC32.Calc(pfiles.GetMsgName());
						xlsAnimaAccess.m_LR = pfiles.GetShort();
						xlsAnimaAccess.m_Type = pfiles.GetShort();
						xlsAnimaAccess.m_Num = pfiles.GetInt();
						xlsAnimaAccess.m_BaseName = pfiles.GetMsgName();
						this.m_AnimeAccess.Add(xlsAnimaAccess);
					}
				}
				else if (psheetname == "Action")
				{
					string[] array = new string[4];
					float[] array2 = new float[4];
					for (int i = 0; i < ftablenum; i++)
					{
						RoomAcitionWakeUp roomAcitionWakeUp = new RoomAcitionWakeUp();
						roomAcitionWakeUp.m_Key = RoomCharaActMode.GetNameToMode(pfiles.GetMsgName());
						int num = 0;
						array2[0] = pfiles.GetFloat();
						array[0] = pfiles.GetMsgName();
						array2[1] = pfiles.GetFloat();
						array[1] = pfiles.GetMsgName();
						array2[2] = pfiles.GetFloat();
						array[2] = pfiles.GetMsgName();
						array2[3] = pfiles.GetFloat();
						array[3] = pfiles.GetMsgName();
						if (array2[0] > 0f)
						{
							num++;
						}
						if (array2[1] > 0f)
						{
							num++;
						}
						if (array2[2] > 0f)
						{
							num++;
						}
						if (array2[3] > 0f)
						{
							num++;
						}
						roomAcitionWakeUp.m_Table = new RoomWakeUpKey[num];
						if (array2[0] > 0f)
						{
							roomAcitionWakeUp.m_Table[0].m_Percent = array2[0];
							roomAcitionWakeUp.m_Table[0].m_Type = RoomCharaActMode.GetNameToMode(array[0]);
						}
						if (array2[1] > 0f)
						{
							roomAcitionWakeUp.m_Table[1].m_Percent = array2[1];
							roomAcitionWakeUp.m_Table[1].m_Type = RoomCharaActMode.GetNameToMode(array[1]);
						}
						if (array2[2] > 0f)
						{
							roomAcitionWakeUp.m_Table[2].m_Percent = array2[2];
							roomAcitionWakeUp.m_Table[2].m_Type = RoomCharaActMode.GetNameToMode(array[2]);
						}
						if (array2[3] > 0f)
						{
							roomAcitionWakeUp.m_Table[3].m_Percent = array2[3];
							roomAcitionWakeUp.m_Table[3].m_Type = RoomCharaActMode.GetNameToMode(array[3]);
						}
						this.m_ActionAccess.Add(roomAcitionWakeUp);
					}
				}
			}

			// Token: 0x06001E48 RID: 7752 RVA: 0x000A3240 File Offset: 0x000A1640
			public void CreateSetting(List<AnimSettings> settings, int partsIndex, int foptioncode, int fkey)
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (partsIndex != 0)
				{
					if (partsIndex != 1)
					{
					}
					stringBuilder.Append("anim/player/common_body");
					stringBuilder.Append(".muast");
				}
				else
				{
					stringBuilder.Append("anim/player/common_head_");
					stringBuilder.Append(foptioncode);
					stringBuilder.Append(".muast");
				}
				string resoucePath = stringBuilder.ToString();
				stringBuilder.Length = 0;
				if (partsIndex != 0)
				{
					if (partsIndex != 1)
					{
					}
					stringBuilder.Append("MeigeAC_Common_body");
				}
				else
				{
					stringBuilder.Append("meigeac_common_head_");
					stringBuilder.Append(foptioncode);
				}
				string[] array = new string[]
				{
					"_L",
					"_R"
				};
				int count = this.m_AnimeAccess.Count;
				for (int i = 0; i < count; i++)
				{
					RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess xlsAnimaAccess = this.m_AnimeAccess[i];
					bool flag = true;
					if (xlsAnimaAccess.m_Type != 0 && ((int)xlsAnimaAccess.m_Type & fkey) == 0)
					{
						flag = false;
					}
					if (flag)
					{
						int num = 2;
						if (xlsAnimaAccess.m_LR == 0)
						{
							num = 1;
						}
						switch (xlsAnimaAccess.m_Num)
						{
						case 1:
						case 2:
							for (int j = 0; j < num; j++)
							{
								string text = string.Format("{0}{1}", xlsAnimaAccess.m_BaseName, array[j]);
								string objectNameWithoutExt = string.Format("{0}@{1}", stringBuilder.ToString(), text);
								settings.Add(new AnimSettings(text, resoucePath, objectNameWithoutExt));
							}
							break;
						case 3:
							for (int j = 0; j < num; j++)
							{
								string text = string.Format("{0}_st{1}", xlsAnimaAccess.m_BaseName, array[j]);
								string objectNameWithoutExt = string.Format("{0}@{1}", stringBuilder.ToString(), text);
								settings.Add(new AnimSettings(text, resoucePath, objectNameWithoutExt));
								text = string.Format("{0}_lp{1}", xlsAnimaAccess.m_BaseName, array[j]);
								objectNameWithoutExt = string.Format("{0}@{1}", stringBuilder.ToString(), text);
								settings.Add(new AnimSettings(text, resoucePath, objectNameWithoutExt));
								text = string.Format("{0}_ed{1}", xlsAnimaAccess.m_BaseName, array[j]);
								objectNameWithoutExt = string.Format("{0}@{1}", stringBuilder.ToString(), text);
								settings.Add(new AnimSettings(text, resoucePath, objectNameWithoutExt));
							}
							break;
						}
					}
				}
			}

			// Token: 0x06001E49 RID: 7753 RVA: 0x000A34CC File Offset: 0x000A18CC
			public void CreateDummySetting(List<AnimSettings> settings, int partsIndex, int foptioncode)
			{
				string arg;
				if (partsIndex != 0)
				{
					if (partsIndex != 1)
					{
					}
					arg = string.Format("Anim/Player/MeigeAC_Common_body", new object[0]);
				}
				else
				{
					arg = string.Format("Anim/Player/MeigeAC_Common_head_{0}", foptioncode);
				}
				string resoucePath = string.Format("{0}@{1}", arg, "room_dummy");
				settings.Add(new AnimSettings("room_dummy", resoucePath));
			}

			// Token: 0x06001E4A RID: 7754 RVA: 0x000A353C File Offset: 0x000A193C
			public void CreateDatbaseSetting(List<RoomAnimePlay.BuildUpSettings> settings, int partsIndex, int foptioncode, int fkey)
			{
				string arg;
				if (partsIndex != 0)
				{
					if (partsIndex != 1)
					{
					}
					arg = string.Format("Common_body", new object[0]);
				}
				else
				{
					arg = string.Format("Common_head_{0}", foptioncode);
				}
				string[] array = new string[]
				{
					"_L",
					"_R"
				};
				int count = this.m_AnimeAccess.Count;
				for (int i = 0; i < count; i++)
				{
					RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess xlsAnimaAccess = this.m_AnimeAccess[i];
					int num = 2;
					if (xlsAnimaAccess.m_LR == 0)
					{
						num = 1;
					}
					switch (xlsAnimaAccess.m_Num)
					{
					case 1:
					case 2:
						for (int j = 0; j < num; j++)
						{
							string text = string.Format("{0}{1}", xlsAnimaAccess.m_BaseName, array[j]);
							string filename = string.Format("{0}@{1}", arg, text);
							settings.Add(new RoomAnimePlay.BuildUpSettings(text, filename, xlsAnimaAccess.m_KeyCode));
						}
						break;
					case 3:
						for (int j = 0; j < num; j++)
						{
							string text = string.Format("{0}_st{1}", xlsAnimaAccess.m_BaseName, array[j]);
							string filename = string.Format("{0}@{1}", arg, text);
							settings.Add(new RoomAnimePlay.BuildUpSettings(text, filename, xlsAnimaAccess.m_KeyCode));
							text = string.Format("{0}_lp{1}", xlsAnimaAccess.m_BaseName, array[j]);
							filename = string.Format("{0}@{1}", arg, text);
							settings.Add(new RoomAnimePlay.BuildUpSettings(text, filename, xlsAnimaAccess.m_KeyCode));
							text = string.Format("{0}_ed{1}", xlsAnimaAccess.m_BaseName, array[j]);
							filename = string.Format("{0}@{1}", arg, text);
							settings.Add(new RoomAnimePlay.BuildUpSettings(text, filename, xlsAnimaAccess.m_KeyCode));
						}
						break;
					}
				}
			}

			// Token: 0x040024D5 RID: 9429
			public List<RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess> m_AnimeAccess = new List<RoomAnimePlay.XlsRoomAnimeTagData.XlsAnimaAccess>();

			// Token: 0x040024D6 RID: 9430
			public List<RoomAcitionWakeUp> m_ActionAccess = new List<RoomAcitionWakeUp>();

			// Token: 0x02000605 RID: 1541
			public class XlsAnimaAccess
			{
				// Token: 0x040024D7 RID: 9431
				public uint m_KeyCode;

				// Token: 0x040024D8 RID: 9432
				public short m_LR;

				// Token: 0x040024D9 RID: 9433
				public short m_Type;

				// Token: 0x040024DA RID: 9434
				public int m_Num;

				// Token: 0x040024DB RID: 9435
				public string m_BaseName;
			}
		}

		// Token: 0x02000606 RID: 1542
		public class RoomAnimePlayTagList : IDataBaseResource
		{
			// Token: 0x06001E4D RID: 7757 RVA: 0x000A3738 File Offset: 0x000A1B38
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
				TextAsset textAsset = pdataobj as TextAsset;
				BinaryReader binaryReader = new BinaryReader();
				this.m_AnimeTag = new RoomAnimePlay.XlsRoomAnimeTagData();
				binaryReader.SetBinary(textAsset.bytes, textAsset.bytes.Length);
				this.m_AnimeTag.RealBinFile(binaryReader);
			}

			// Token: 0x040024DC RID: 9436
			public RoomAnimePlay.XlsRoomAnimeTagData m_AnimeTag;
		}
	}
}
