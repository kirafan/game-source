﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000263 RID: 611
	public class WeaponListDB : ScriptableObject
	{
		// Token: 0x040013D9 RID: 5081
		public WeaponListDB_Param[] m_Params;
	}
}
