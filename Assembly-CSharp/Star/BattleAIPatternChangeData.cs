﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000091 RID: 145
	[Serializable]
	public class BattleAIPatternChangeData
	{
		// Token: 0x040002A6 RID: 678
		public List<BattleAIPatternChangeCondition> m_Conditions = new List<BattleAIPatternChangeCondition>();

		// Token: 0x040002A7 RID: 679
		public int m_ChangeToPatternIndex;
	}
}
