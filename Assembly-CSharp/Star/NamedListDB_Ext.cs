﻿using System;

namespace Star
{
	// Token: 0x0200019B RID: 411
	public static class NamedListDB_Ext
	{
		// Token: 0x06000B00 RID: 2816 RVA: 0x00041944 File Offset: 0x0003FD44
		public static bool IsExist(this NamedListDB self, eCharaNamedType namedType)
		{
			if (namedType == eCharaNamedType.None)
			{
				return false;
			}
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_NamedType == (int)namedType)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000B01 RID: 2817 RVA: 0x00041990 File Offset: 0x0003FD90
		public static NamedListDB_Param GetParam(this NamedListDB self, eCharaNamedType namedType)
		{
			if (namedType == eCharaNamedType.None)
			{
				return default(NamedListDB_Param);
			}
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_NamedType == (int)namedType)
				{
					return self.m_Params[i];
				}
			}
			return default(NamedListDB_Param);
		}

		// Token: 0x06000B02 RID: 2818 RVA: 0x000419FC File Offset: 0x0003FDFC
		public static eTitleType GetTitleType(this NamedListDB self, eCharaNamedType namedType)
		{
			if (namedType == eCharaNamedType.None)
			{
				return eTitleType.None;
			}
			return (eTitleType)self.GetParam(namedType).m_TitleType;
		}

		// Token: 0x06000B03 RID: 2819 RVA: 0x00041A24 File Offset: 0x0003FE24
		public static sbyte GetFriendshipTableID(this NamedListDB self, eCharaNamedType namedType)
		{
			if (namedType == eCharaNamedType.None)
			{
				return 0;
			}
			return self.GetParam(namedType).m_FriendshipTableID;
		}

		// Token: 0x06000B04 RID: 2820 RVA: 0x00041A4C File Offset: 0x0003FE4C
		public static string GetResourceBaseName(this NamedListDB self, eCharaNamedType namedType)
		{
			if (namedType == eCharaNamedType.None)
			{
				return string.Empty;
			}
			return self.GetParam(namedType).m_ResouceBaseName;
		}
	}
}
