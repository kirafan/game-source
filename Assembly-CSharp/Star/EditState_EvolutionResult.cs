﻿using System;
using Star.UI.BackGround;
using Star.UI.Edit;
using UnityEngine;

namespace Star
{
	// Token: 0x02000405 RID: 1029
	public class EditState_EvolutionResult : EditState
	{
		// Token: 0x06001393 RID: 5011 RVA: 0x0006896A File Offset: 0x00066D6A
		public EditState_EvolutionResult(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001394 RID: 5012 RVA: 0x00068982 File Offset: 0x00066D82
		public override int GetStateID()
		{
			return 16;
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x00068986 File Offset: 0x00066D86
		public override void OnStateEnter()
		{
			this.m_MixEffectScene = this.m_Owner.MixEffectScenes[2];
			this.m_Step = EditState_EvolutionResult.eStep.StartFadeOut;
		}

		// Token: 0x06001396 RID: 5014 RVA: 0x000689A2 File Offset: 0x00066DA2
		public override void OnStateExit()
		{
		}

		// Token: 0x06001397 RID: 5015 RVA: 0x000689A4 File Offset: 0x00066DA4
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001398 RID: 5016 RVA: 0x000689B0 File Offset: 0x00066DB0
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_EvolutionResult.eStep.StartFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_EvolutionResult.eStep.StartFadeWait;
				break;
			case EditState_EvolutionResult.eStep.StartFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = EditState_EvolutionResult.eStep.First;
				}
				break;
			case EditState_EvolutionResult.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_EvolutionResult.eStep.ChildSceneLoadWait;
				break;
			case EditState_EvolutionResult.eStep.ChildSceneLoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_Owner.EvolutionResult.m_SrcCharaID);
					this.m_MixEffectScene.Prepare(param.m_CharaID);
					this.m_Step = EditState_EvolutionResult.eStep.EffectLoadWait;
				}
				break;
			case EditState_EvolutionResult.eStep.EffectLoadWait:
				if (this.m_MixEffectScene.IsPrepareError())
				{
					this.m_Step = EditState_EvolutionResult.eStep.PrepareError;
				}
				else if (this.m_MixEffectScene.IsCompletePrepare())
				{
					this.m_Step = EditState_EvolutionResult.eStep.Setup;
				}
				break;
			case EditState_EvolutionResult.eStep.Setup:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Hide(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(-1f, null);
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Effect);
					this.m_Step = EditState_EvolutionResult.eStep.Main_0;
				}
				break;
			case EditState_EvolutionResult.eStep.Main_0:
			{
				Vector2 vector;
				bool flag = InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0);
				if (flag || this.m_MixEffectScene.IsCompletePlay())
				{
					this.m_Step = EditState_EvolutionResult.eStep.Main_1;
					if (flag)
					{
						this.m_MixEffectScene.OnSkip();
					}
				}
				break;
			}
			case EditState_EvolutionResult.eStep.Main_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_UI.PlayIn();
					this.m_MixEffectScene.Play(MixEffectScene.eAnimType.Loop);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.2f, null);
					this.m_Step = EditState_EvolutionResult.eStep.Main_2;
				}
				break;
			case EditState_EvolutionResult.eStep.Main_2:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_NextState = 14;
					this.m_Step = EditState_EvolutionResult.eStep.EndFadeOut;
				}
				break;
			case EditState_EvolutionResult.eStep.EndFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(0.2f, null);
				this.m_Step = EditState_EvolutionResult.eStep.EndFadeWait;
				break;
			case EditState_EvolutionResult.eStep.EndFadeWait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_EvolutionResult.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_EvolutionResult.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_MixEffectScene.Suspend();
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Show(UIBackGroundManager.eBackGroundOrderID.Low);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(0.2f, null);
					this.m_Step = EditState_EvolutionResult.eStep.None;
					return this.m_NextState;
				}
				break;
			case EditState_EvolutionResult.eStep.PrepareError:
				APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
				this.m_Step = EditState_EvolutionResult.eStep.PrepareErrorWait;
				break;
			}
			return -1;
		}

		// Token: 0x06001399 RID: 5017 RVA: 0x00068CF8 File Offset: 0x000670F8
		public bool SetupUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<EvolutionResultUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.EvolutionResult);
			return true;
		}

		// Token: 0x0600139A RID: 5018 RVA: 0x00068D52 File Offset: 0x00067152
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001A80 RID: 6784
		private EditState_EvolutionResult.eStep m_Step = EditState_EvolutionResult.eStep.None;

		// Token: 0x04001A81 RID: 6785
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.EvolutionResultUI;

		// Token: 0x04001A82 RID: 6786
		public EvolutionResultUI m_UI;

		// Token: 0x04001A83 RID: 6787
		public MixEffectScene m_MixEffectScene;

		// Token: 0x04001A84 RID: 6788
		private const float FADE_SEC = 0.2f;

		// Token: 0x02000406 RID: 1030
		private enum eStep
		{
			// Token: 0x04001A86 RID: 6790
			None = -1,
			// Token: 0x04001A87 RID: 6791
			StartFadeOut,
			// Token: 0x04001A88 RID: 6792
			StartFadeWait,
			// Token: 0x04001A89 RID: 6793
			First,
			// Token: 0x04001A8A RID: 6794
			ChildSceneLoadWait,
			// Token: 0x04001A8B RID: 6795
			EffectLoadWait,
			// Token: 0x04001A8C RID: 6796
			Setup,
			// Token: 0x04001A8D RID: 6797
			Main_0,
			// Token: 0x04001A8E RID: 6798
			Main_1,
			// Token: 0x04001A8F RID: 6799
			Main_2,
			// Token: 0x04001A90 RID: 6800
			EndFadeOut,
			// Token: 0x04001A91 RID: 6801
			EndFadeWait,
			// Token: 0x04001A92 RID: 6802
			UnloadChildSceneWait,
			// Token: 0x04001A93 RID: 6803
			PrepareError,
			// Token: 0x04001A94 RID: 6804
			PrepareErrorWait
		}
	}
}
