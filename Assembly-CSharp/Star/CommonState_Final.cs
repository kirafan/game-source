﻿using System;
using Star.UI;
using Star.UI.Global;

namespace Star
{
	// Token: 0x020003E7 RID: 999
	public class CommonState_Final : GameStateBase
	{
		// Token: 0x060012FD RID: 4861 RVA: 0x00065CFA File Offset: 0x000640FA
		public CommonState_Final(GameStateMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x060012FE RID: 4862 RVA: 0x00065D10 File Offset: 0x00064110
		public override int GetStateID()
		{
			return 2147483646;
		}

		// Token: 0x060012FF RID: 4863 RVA: 0x00065D17 File Offset: 0x00064117
		public override void OnStateEnter()
		{
			this.m_Step = CommonState_Final.eStep.First;
		}

		// Token: 0x06001300 RID: 4864 RVA: 0x00065D20 File Offset: 0x00064120
		public override void OnStateExit()
		{
		}

		// Token: 0x06001301 RID: 4865 RVA: 0x00065D22 File Offset: 0x00064122
		public override void OnDispose()
		{
		}

		// Token: 0x06001302 RID: 4866 RVA: 0x00065D24 File Offset: 0x00064124
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case CommonState_Final.eStep.First:
			{
				eTipsCategory tipCategory = eTipsCategory.None;
				if (this.m_Owner.NextTransitSceneID == SceneDefine.eSceneID.Battle)
				{
					tipCategory = eTipsCategory.Battle;
				}
				else if (this.m_Owner.NextTransitSceneID == SceneDefine.eSceneID.Town || SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Home)
				{
					tipCategory = eTipsCategory.Town;
				}
				else if (this.m_Owner.NextTransitSceneID == SceneDefine.eSceneID.Room || SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Room)
				{
					tipCategory = eTipsCategory.Room;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, tipCategory);
				this.m_Step = CommonState_Final.eStep.Step_0;
				break;
			}
			case CommonState_Final.eStep.Step_0:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.HideSceneInfo();
					this.m_Owner.Destroy();
					this.m_Step = CommonState_Final.eStep.Step_1;
				}
				break;
			case CommonState_Final.eStep.Step_1:
				if (this.m_Owner.IsCompleteDestroy())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.m_Step = CommonState_Final.eStep.Step_2;
				}
				break;
			case CommonState_Final.eStep.Step_2:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ExecShortCut())
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(this.m_Owner.NextTransitSceneID);
					}
					this.m_Step = CommonState_Final.eStep.None;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001303 RID: 4867 RVA: 0x00065EA4 File Offset: 0x000642A4
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x040019E5 RID: 6629
		private CommonState_Final.eStep m_Step = CommonState_Final.eStep.None;

		// Token: 0x040019E6 RID: 6630
		private GameStateMain m_Owner;

		// Token: 0x020003E8 RID: 1000
		private enum eStep
		{
			// Token: 0x040019E8 RID: 6632
			None = -1,
			// Token: 0x040019E9 RID: 6633
			First,
			// Token: 0x040019EA RID: 6634
			Step_0,
			// Token: 0x040019EB RID: 6635
			Step_1,
			// Token: 0x040019EC RID: 6636
			Step_2
		}
	}
}
