﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001D3 RID: 467
	public class MasterRankDB : ScriptableObject
	{
		// Token: 0x04000B14 RID: 2836
		public MasterRankDB_Param[] m_Params;
	}
}
