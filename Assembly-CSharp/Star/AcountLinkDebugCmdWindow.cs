﻿using System;
using System.Runtime.CompilerServices;
using Meige;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200007D RID: 125
	public class AcountLinkDebugCmdWindow : DebugWindowBase
	{
		// Token: 0x060003DF RID: 991 RVA: 0x000139E4 File Offset: 0x00011DE4
		public static void OpenCheck()
		{
			if (AcountLinkDebugCmdWindow.ms_Handle == null)
			{
				string resourceName = "Prefab/UI/AcountLinkDebugCmdWindow.muast";
				if (AcountLinkDebugCmdWindow.<>f__mg$cache0 == null)
				{
					AcountLinkDebugCmdWindow.<>f__mg$cache0 = new Action<MeigeResource.Handler>(AcountLinkDebugCmdWindow.CallbackWindowMake);
				}
				MeigeResourceManager.LoadHandler(resourceName, AcountLinkDebugCmdWindow.<>f__mg$cache0, new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
			}
			AcountLinkDebugCmdWindow.ms_ResetPos = true;
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x00013A34 File Offset: 0x00011E34
		private static void CallbackWindowMake(MeigeResource.Handler phandle)
		{
			AcountLinkDebugCmdWindow.ms_Handle = phandle;
			GameObject gameObject = phandle.GetAsset<GameObject>();
			gameObject = UnityEngine.Object.Instantiate<GameObject>(gameObject);
			gameObject.AddComponent<AcountLinkDebugCmdWindow>();
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00013A5C File Offset: 0x00011E5C
		private void Start()
		{
			base.WindowSetUp();
			AccountLinker.CheckCreate();
			Button button = DebugWindowBase.FindHrcObject(base.transform, "AcountLink", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackAcountLink));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "LinkMove", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackLinkMove));
			}
			this.m_AcountName = (DebugWindowBase.FindHrcObject(base.transform, "AcountText/Text", typeof(Text)) as Text);
			this.m_AcountState = (DebugWindowBase.FindHrcObject(base.transform, "AcountState/Text", typeof(Text)) as Text);
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x00013B44 File Offset: 0x00011F44
		private void Update()
		{
			base.WindowUpdata();
			if (AcountLinkDebugCmdWindow.ms_ResetPos)
			{
				base.ResetWindowPos();
				AcountLinkDebugCmdWindow.ms_ResetPos = false;
			}
			if (this.m_TimeStep && IAcountController.IsSetup())
			{
				if (this.m_AcountName != null)
				{
					this.m_AcountName.text = IAcountController.GetUserName();
				}
				if (this.m_AcountState != null)
				{
					this.m_AcountState.text = IAcountController.GetState().ToString();
				}
			}
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00013BD2 File Offset: 0x00011FD2
		public void CallbackAcountLink()
		{
			this.m_TimeStep = true;
			AccountLinker.SetAcountLinkData();
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00013BE0 File Offset: 0x00011FE0
		public void CallbackLinkMove()
		{
			this.m_TimeStep = true;
			AccountLinker.BuildUpAcountLinkPlayer();
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x00013BEE File Offset: 0x00011FEE
		private void OnDestroy()
		{
			if (AcountLinkDebugCmdWindow.ms_Handle != null)
			{
				if (SingletonMonoBehaviour<MeigeResourceManager>.Instance != null)
				{
					MeigeResourceManager.UnloadHandler(AcountLinkDebugCmdWindow.ms_Handle, false);
				}
				AcountLinkDebugCmdWindow.ms_Handle = null;
			}
		}

		// Token: 0x0400021A RID: 538
		private static MeigeResource.Handler ms_Handle;

		// Token: 0x0400021B RID: 539
		private static bool ms_ResetPos;

		// Token: 0x0400021C RID: 540
		private bool m_TimeStep;

		// Token: 0x0400021D RID: 541
		private Text m_TimeUp;

		// Token: 0x0400021E RID: 542
		private Text m_AcountName;

		// Token: 0x0400021F RID: 543
		private Text m_AcountState;

		// Token: 0x04000220 RID: 544
		[CompilerGenerated]
		private static Action<MeigeResource.Handler> <>f__mg$cache0;
	}
}
