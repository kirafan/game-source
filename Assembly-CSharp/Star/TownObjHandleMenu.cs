﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006E9 RID: 1769
	public class TownObjHandleMenu : TownObjModelHandle
	{
		// Token: 0x060022FE RID: 8958 RVA: 0x000BCCFC File Offset: 0x000BB0FC
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			TownObjectListDB_Param townObjectListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.m_Params[0];
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.m_Params.Length; i++)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.m_Params[i].m_ResourceID == objID)
				{
					townObjectListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.m_Params[i];
					break;
				}
			}
			this.m_MakerPos = townObjectListDB_Param.m_MarkerPos;
			this.m_BaseSize = townObjectListDB_Param.m_BaseSize;
			this.m_Offset.x = townObjectListDB_Param.m_OffsetX;
			this.m_Offset.y = townObjectListDB_Param.m_OffsetY;
			this.m_Offset.z = 1f;
			this.m_ResourceID = townObjectListDB_Param.m_ResourceID * 100;
			this.m_EventActive = true;
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(fmanageid, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(true);
			}
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			this.m_BindCtrl = this.m_Builder.GetCharaBind(this.m_ManageID, true);
			this.m_BindCtrl.SetLinkBindKey(this.m_Transform, this.m_LayerID, this.m_MakerPos);
		}

		// Token: 0x060022FF RID: 8959 RVA: 0x000BCE64 File Offset: 0x000BB264
		protected override bool PrepareMain()
		{
			if (!base.PrepareMain())
			{
				return false;
			}
			this.m_Obj.transform.localPosition = this.m_Offset;
			this.m_Obj.transform.localScale = Vector3.one * this.m_BaseSize;
			base.MakeColliderLink(this.m_Obj);
			if (!this.m_EventActive)
			{
				this.SetModelToNoActive();
			}
			if (TownUtility.GetMenuPointEnum(this.m_BuildAccessID) == eTownMenuType.Traning)
			{
				GameObject menuIcon = this.m_Builder.GetMenuIcon(eTownMenuType.Traning);
				if (menuIcon != null)
				{
					GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(menuIcon);
					gameObject.transform.position = this.m_Transform.position + Vector3.up * this.m_MakerPos;
					gameObject.transform.SetParent(this.m_Transform, true);
				}
			}
			else
			{
				this.m_MenuOptionTask = TownMenuActCreate.Create(TownUtility.GetMenuPointEnum(this.m_BuildAccessID), this.m_Builder);
			}
			return true;
		}

		// Token: 0x06002300 RID: 8960 RVA: 0x000BCF69 File Offset: 0x000BB369
		public bool IsCompleteBuildEvt()
		{
			return false;
		}

		// Token: 0x06002301 RID: 8961 RVA: 0x000BCF6C File Offset: 0x000BB36C
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleMenu.eState.Init:
				if (this.m_IsPreparing && this.PrepareMain())
				{
					this.m_State = TownObjHandleMenu.eState.Main;
				}
				break;
			case TownObjHandleMenu.eState.Main:
				if (this.m_BindPos)
				{
					this.m_BindCtrl.CalcCharaPopIconPos(this.m_ManageID, this.m_LayerID, this.m_MakerPos);
					this.m_BindPos = false;
				}
				if (this.m_MenuOptionTask != null)
				{
					this.m_MenuOptionTask.UpdateMenu(this);
				}
				break;
			case TownObjHandleMenu.eState.EndStart:
				this.m_State = TownObjHandleMenu.eState.End;
				break;
			case TownObjHandleMenu.eState.End:
				if (this.m_Action.IsEmpty())
				{
					base.Destroy();
				}
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x06002302 RID: 8962 RVA: 0x000BD043 File Offset: 0x000BB443
		public override void Destroy()
		{
			this.m_Action = null;
			base.Destroy();
		}

		// Token: 0x06002303 RID: 8963 RVA: 0x000BD054 File Offset: 0x000BB454
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			eTownEventAct type = pact.m_Type;
			if (type != eTownEventAct.ReFlesh)
			{
				if (type == eTownEventAct.MenuMode)
				{
					TownHandleActionMenuMode townHandleActionMenuMode = (TownHandleActionMenuMode)pact;
					this.m_BindCtrl.SetViewActive(townHandleActionMenuMode.m_Mode);
				}
			}
			else
			{
				this.m_BindPos = true;
			}
			return result;
		}

		// Token: 0x06002304 RID: 8964 RVA: 0x000BD0A8 File Offset: 0x000BB4A8
		private void SetModelToNoActive()
		{
			MsbHandler[] componentsInChildren = this.m_Obj.GetComponentsInChildren<MsbHandler>();
			Color white = Color.white;
			white.r = (white.g = (white.b = 0.5f));
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				int msbObjectHandlerNum = componentsInChildren[i].GetMsbObjectHandlerNum();
				for (int j = 0; j < msbObjectHandlerNum; j++)
				{
					MsbObjectHandler msbObjectHandler = componentsInChildren[i].GetMsbObjectHandler(j);
					msbObjectHandler.GetWork().m_MeshColor = white;
				}
			}
		}

		// Token: 0x06002305 RID: 8965 RVA: 0x000BD134 File Offset: 0x000BB534
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				TownComUI townComUI = new TownComUI(true);
				townComUI.m_SelectHandle = this;
				townComUI.m_OpenUI = eTownUICategory.FreeBuf;
				townComUI.m_BuildPoint = this.m_BuildAccessID;
				switch (TownUtility.GetMenuPointEnum(this.m_BuildAccessID))
				{
				case eTownMenuType.Quest:
					townComUI.m_OpenUI = eTownUICategory.QuestView;
					break;
				case eTownMenuType.Edit:
					townComUI.m_OpenUI = eTownUICategory.EditView;
					break;
				case eTownMenuType.Gacha:
					townComUI.m_OpenUI = eTownUICategory.SummonView;
					break;
				case eTownMenuType.Traning:
					townComUI.m_OpenUI = eTownUICategory.TraningView;
					break;
				case eTownMenuType.Shop:
					townComUI.m_OpenUI = eTownUICategory.ShopView;
					break;
				case eTownMenuType.Mission:
					townComUI.m_OpenUI = eTownUICategory.MissionView;
					break;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
				pbuilder.StackEventQue(townComUI);
			}
			return true;
		}

		// Token: 0x040029D9 RID: 10713
		public float m_MakerPos;

		// Token: 0x040029DA RID: 10714
		private float m_BaseSize;

		// Token: 0x040029DB RID: 10715
		private Vector3 m_Offset;

		// Token: 0x040029DC RID: 10716
		public bool m_EventActive;

		// Token: 0x040029DD RID: 10717
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x040029DE RID: 10718
		private bool m_BindPos;

		// Token: 0x040029DF RID: 10719
		private ITownMenuAct m_MenuOptionTask;

		// Token: 0x040029E0 RID: 10720
		public TownObjHandleMenu.eState m_State;

		// Token: 0x020006EA RID: 1770
		public enum eState
		{
			// Token: 0x040029E2 RID: 10722
			Init,
			// Token: 0x040029E3 RID: 10723
			Main,
			// Token: 0x040029E4 RID: 10724
			Sleep,
			// Token: 0x040029E5 RID: 10725
			EndStart,
			// Token: 0x040029E6 RID: 10726
			End
		}
	}
}
