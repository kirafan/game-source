﻿using System;

namespace Star
{
	// Token: 0x020002A9 RID: 681
	public class AfterLimitBreakParamWrapper : CharacterEditSceneCharacterParamWrapperExGen<EditUtility.SimulateLimitBreakParam>
	{
		// Token: 0x06000CD3 RID: 3283 RVA: 0x00048ED6 File Offset: 0x000472D6
		public AfterLimitBreakParamWrapper(EditUtility.SimulateLimitBreakParam simulateParam) : base(eCharacterDataType.AfterLimitBreakParam, simulateParam)
		{
		}

		// Token: 0x06000CD4 RID: 3284 RVA: 0x00048EE0 File Offset: 0x000472E0
		public override int GetMaxLv()
		{
			if (this.m_Param == null)
			{
				return base.GetMaxLv();
			}
			return this.m_Param.afterMaxLv;
		}

		// Token: 0x06000CD5 RID: 3285 RVA: 0x00048EFF File Offset: 0x000472FF
		public override int GetLimitBreak()
		{
			if (this.m_Param == null)
			{
				return base.GetLimitBreak();
			}
			return base.GetLimitBreak() + 1;
		}

		// Token: 0x06000CD6 RID: 3286 RVA: 0x00048F1B File Offset: 0x0004731B
		public override int GetUniqueSkillMaxLv()
		{
			return this.m_Param.afterUniqueSkillMaxLv;
		}

		// Token: 0x06000CD7 RID: 3287 RVA: 0x00048F28 File Offset: 0x00047328
		public override int[] GetClassSkillLevels()
		{
			return this.m_Param.afterClassSkillMaxLvs;
		}

		// Token: 0x06000CD8 RID: 3288 RVA: 0x00048F38 File Offset: 0x00047338
		public override int GetClassSkillLevel(int index)
		{
			int[] classSkillLevels = this.GetClassSkillLevels();
			if (classSkillLevels == null || index < 0 || classSkillLevels.Length <= index)
			{
				return 1;
			}
			return classSkillLevels[index];
		}
	}
}
