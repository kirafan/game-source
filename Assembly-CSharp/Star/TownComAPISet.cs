﻿using System;
using TownResponseTypes;

namespace Star
{
	// Token: 0x02000687 RID: 1671
	public class TownComAPISet : INetComHandle
	{
		// Token: 0x060021B8 RID: 8632 RVA: 0x000B396C File Offset: 0x000B1D6C
		public TownComAPISet()
		{
			this.ApiName = "player/town/set";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x04002821 RID: 10273
		public long managedTownId;

		// Token: 0x04002822 RID: 10274
		public string gridData;
	}
}
