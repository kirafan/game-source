﻿using System;

namespace Star
{
	// Token: 0x0200053E RID: 1342
	public enum eRoomScriptCommand
	{
		// Token: 0x04002147 RID: 8519
		AnimePlay,
		// Token: 0x04002148 RID: 8520
		Wait,
		// Token: 0x04002149 RID: 8521
		AnimeWait,
		// Token: 0x0400214A RID: 8522
		Bind,
		// Token: 0x0400214B RID: 8523
		NonBind,
		// Token: 0x0400214C RID: 8524
		ViewMode,
		// Token: 0x0400214D RID: 8525
		Move,
		// Token: 0x0400214E RID: 8526
		PopupMsg,
		// Token: 0x0400214F RID: 8527
		SePlay,
		// Token: 0x04002150 RID: 8528
		Jump,
		// Token: 0x04002151 RID: 8529
		FaceAnime,
		// Token: 0x04002152 RID: 8530
		ProgramEvent,
		// Token: 0x04002153 RID: 8531
		ObjectDir,
		// Token: 0x04002154 RID: 8532
		Effect,
		// Token: 0x04002155 RID: 8533
		BaseTrs,
		// Token: 0x04002156 RID: 8534
		PosAnime,
		// Token: 0x04002157 RID: 8535
		RotAnime,
		// Token: 0x04002158 RID: 8536
		BindRot,
		// Token: 0x04002159 RID: 8537
		FrameChange,
		// Token: 0x0400215A RID: 8538
		CharaViewMode,
		// Token: 0x0400215B RID: 8539
		ObjAnime,
		// Token: 0x0400215C RID: 8540
		CharaCfg,
		// Token: 0x0400215D RID: 8541
		CueSe,
		// Token: 0x0400215E RID: 8542
		ObjCfg
	}
}
