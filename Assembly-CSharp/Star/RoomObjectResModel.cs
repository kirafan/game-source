﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020005DC RID: 1500
	public class RoomObjectResModel : IRoomObjectResource
	{
		// Token: 0x06001D80 RID: 7552 RVA: 0x0009E051 File Offset: 0x0009C451
		public override void UpdateRes()
		{
			if (this.m_IsPreparing && this.PreparingModel())
			{
				this.m_IsPreparing = false;
			}
		}

		// Token: 0x06001D81 RID: 7553 RVA: 0x0009E070 File Offset: 0x0009C470
		public override void Setup(IRoomObjectControll hndl)
		{
			this.m_Owner = (RoomObjectModel)hndl;
		}

		// Token: 0x06001D82 RID: 7554 RVA: 0x0009E07E File Offset: 0x0009C47E
		public override void Destroy()
		{
			if (this.m_ModelHndl != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_ModelHndl);
				this.m_ModelHndl = null;
			}
		}

		// Token: 0x06001D83 RID: 7555 RVA: 0x0009E0A0 File Offset: 0x0009C4A0
		public override void Prepare()
		{
			this.m_IsPreparing = true;
			string modelResourcePath = RoomUtility.GetModelResourcePath(this.m_Owner.ObjCategory, this.m_Owner.ObjID);
			this.m_ModelHndl = ObjectResourceManager.CreateHandle(modelResourcePath);
		}

		// Token: 0x06001D84 RID: 7556 RVA: 0x0009E0DC File Offset: 0x0009C4DC
		private bool PreparingModel()
		{
			if (this.m_ModelHndl == null)
			{
				return true;
			}
			if (!this.m_ModelHndl.IsDone)
			{
				return false;
			}
			if (this.m_ModelHndl.IsError)
			{
				ObjectResourceManager.RetryHandle(this.m_ModelHndl);
				return false;
			}
			GameObject asset = this.m_ModelHndl.GetAsset<GameObject>();
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(asset);
			this.m_Owner.AttachModel(gameObject);
			this.m_Owner.SetUpObjectKey(gameObject);
			this.SetUpBaseAnimation(gameObject);
			return true;
		}

		// Token: 0x06001D85 RID: 7557 RVA: 0x0009E158 File Offset: 0x0009C558
		private void SetUpBaseAnimation(GameObject pobj)
		{
			MeigeAnimClipHolder[] componentsInChildren = pobj.GetComponentsInChildren<MeigeAnimClipHolder>();
			McatFileHelper componentInChildren = pobj.GetComponentInChildren<McatFileHelper>();
			if (componentsInChildren.Length > 0 || componentInChildren != null)
			{
				this.m_Owner.AnimCtrlOpen();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					this.m_Owner.AddAnim("event_001", componentsInChildren[i]);
				}
				if (componentInChildren != null)
				{
					for (int i = 0; i < componentInChildren.m_Table.Length; i++)
					{
						this.m_Owner.AddAnim(componentInChildren.m_Table[i].m_AnimeName, componentInChildren.m_Table[i].m_MabFile.GetComponent<MeigeAnimClipHolder>());
					}
				}
				this.m_Owner.AnimCtrlClose();
			}
			else
			{
				this.m_Owner.ClearAnimCtrl();
			}
			this.m_Owner.SetUpAnimation();
		}

		// Token: 0x040023E6 RID: 9190
		private RoomObjectModel m_Owner;

		// Token: 0x040023E7 RID: 9191
		private MeigeResource.Handler m_ModelHndl;
	}
}
