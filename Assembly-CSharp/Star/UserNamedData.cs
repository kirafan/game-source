﻿using System;

namespace Star
{
	// Token: 0x02000B4D RID: 2893
	public class UserNamedData
	{
		// Token: 0x06003D42 RID: 15682 RVA: 0x0013674C File Offset: 0x00134B4C
		public UserNamedData(long mngID, eCharaNamedType namedType, int friendship, int friendShipMax, uint friendshipExp)
		{
			this.MngID = mngID;
			this.NamedType = namedType;
			this.FriendShip = friendship;
			this.FriendShipMax = friendShipMax;
			this.FriendShipExp = (long)((ulong)friendshipExp);
		}

		// Token: 0x06003D43 RID: 15683 RVA: 0x0013677A File Offset: 0x00134B7A
		public UserNamedData()
		{
			this.MngID = -1L;
			this.NamedType = eCharaNamedType.None;
			this.FriendShip = 0;
			this.FriendShipMax = 0;
			this.FriendShipExp = 0L;
		}

		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x06003D44 RID: 15684 RVA: 0x001367A7 File Offset: 0x00134BA7
		// (set) Token: 0x06003D45 RID: 15685 RVA: 0x001367AF File Offset: 0x00134BAF
		public long MngID { get; set; }

		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x06003D46 RID: 15686 RVA: 0x001367B8 File Offset: 0x00134BB8
		// (set) Token: 0x06003D47 RID: 15687 RVA: 0x001367C0 File Offset: 0x00134BC0
		public eCharaNamedType NamedType { get; set; }

		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x06003D48 RID: 15688 RVA: 0x001367C9 File Offset: 0x00134BC9
		// (set) Token: 0x06003D49 RID: 15689 RVA: 0x001367D1 File Offset: 0x00134BD1
		public int FriendShip { get; set; }

		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x06003D4A RID: 15690 RVA: 0x001367DA File Offset: 0x00134BDA
		// (set) Token: 0x06003D4B RID: 15691 RVA: 0x001367E2 File Offset: 0x00134BE2
		public int FriendShipMax { get; set; }

		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x06003D4C RID: 15692 RVA: 0x001367EB File Offset: 0x00134BEB
		// (set) Token: 0x06003D4D RID: 15693 RVA: 0x001367F3 File Offset: 0x00134BF3
		public long FriendShipExp { get; set; }
	}
}
