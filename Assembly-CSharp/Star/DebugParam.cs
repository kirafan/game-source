﻿using System;

namespace Star
{
	// Token: 0x0200027D RID: 637
	public class DebugParam
	{
		// Token: 0x04001468 RID: 5224
		public bool m_ShortcutToResult;

		// Token: 0x04001469 RID: 5225
		public int m_BattleDamageToPlayerParty;

		// Token: 0x0400146A RID: 5226
		public int m_BattleDamageToEnemyParty;

		// Token: 0x0400146B RID: 5227
		public bool m_BattleAbnormalToPlayerParty;

		// Token: 0x0400146C RID: 5228
		public bool m_BattleAbnormalToEnemyParty;

		// Token: 0x0400146D RID: 5229
		public bool m_BattleShortcutStunToPlayerParty;

		// Token: 0x0400146E RID: 5230
		public bool m_BattleShortcutStunToEnemyParty;

		// Token: 0x0400146F RID: 5231
		public bool m_IsRecastNone;

		// Token: 0x04001470 RID: 5232
		public bool m_BackToTitle;

		// Token: 0x04001471 RID: 5233
		public int m_AnimFrameSkipLv;

		// Token: 0x04001472 RID: 5234
		public bool m_EnableAllADV;
	}
}
