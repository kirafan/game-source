﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001D7 RID: 471
	public class NamedFriendshipExpDB : ScriptableObject
	{
		// Token: 0x04000B21 RID: 2849
		public NamedFriendshipExpDB_Param[] m_Params;
	}
}
