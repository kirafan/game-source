﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200038E RID: 910
	public class FldDefaultObjectUtil : IDataBaseResource
	{
		// Token: 0x06001131 RID: 4401 RVA: 0x0005A509 File Offset: 0x00058909
		public override void SetUpResource(UnityEngine.Object pdataobj)
		{
			this.m_DB = (pdataobj as DefaultObjectListDB);
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x0005A517 File Offset: 0x00058917
		public static void DatabaseSetUp()
		{
			if (FldDefaultObjectUtil.ms_FldDefaultObjDB == null)
			{
				FldDefaultObjectUtil.ms_FldDefaultObjDB = new FldDefaultObjectUtil();
				FldDefaultObjectUtil.ms_FldDefaultObjDB.LoadResInManager("Prefab/FldDB/DefaultObjectList", ".asset");
			}
		}

		// Token: 0x06001133 RID: 4403 RVA: 0x0005A541 File Offset: 0x00058941
		public static bool IsSetUp()
		{
			return FldDefaultObjectUtil.ms_FldDefaultObjDB.m_Active;
		}

		// Token: 0x06001134 RID: 4404 RVA: 0x0005A54D File Offset: 0x0005894D
		public static void DatabaseRelease()
		{
			if (FldDefaultObjectUtil.ms_FldDefaultObjDB != null)
			{
				FldDefaultObjectUtil.ms_FldDefaultObjDB.Release();
			}
			FldDefaultObjectUtil.ms_FldDefaultObjDB = null;
		}

		// Token: 0x06001135 RID: 4405 RVA: 0x0005A569 File Offset: 0x00058969
		public static DefaultObjectListDB GetDB()
		{
			return FldDefaultObjectUtil.ms_FldDefaultObjDB.m_DB;
		}

		// Token: 0x04001811 RID: 6161
		public static FldDefaultObjectUtil ms_FldDefaultObjDB;

		// Token: 0x04001812 RID: 6162
		private DefaultObjectListDB m_DB;
	}
}
