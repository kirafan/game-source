﻿using System;

namespace Star
{
	// Token: 0x02000294 RID: 660
	[Serializable]
	public class RoomDebugObjectSetting
	{
		// Token: 0x04001507 RID: 5383
		public int m_X;

		// Token: 0x04001508 RID: 5384
		public int m_Y;

		// Token: 0x04001509 RID: 5385
		public eRoomObjectCategory m_Category;

		// Token: 0x0400150A RID: 5386
		public CharacterDefine.eDir m_Dir;

		// Token: 0x0400150B RID: 5387
		public int m_SetNo;
	}
}
