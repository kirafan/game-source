﻿using System;

namespace Star
{
	// Token: 0x020001D2 RID: 466
	[Serializable]
	public struct MasterRankDB_Param
	{
		// Token: 0x04000B0C RID: 2828
		public int m_Rank;

		// Token: 0x04000B0D RID: 2829
		public int m_NextExp;

		// Token: 0x04000B0E RID: 2830
		public int m_Stamina;

		// Token: 0x04000B0F RID: 2831
		public int m_FriendLimit;

		// Token: 0x04000B10 RID: 2832
		public int m_SupportLimit;

		// Token: 0x04000B11 RID: 2833
		public int m_BattlePartyCost;

		// Token: 0x04000B12 RID: 2834
		public int m_WeaponLimit;

		// Token: 0x04000B13 RID: 2835
		public int m_StoreReview;
	}
}
