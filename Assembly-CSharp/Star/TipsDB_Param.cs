﻿using System;

namespace Star
{
	// Token: 0x0200023D RID: 573
	[Serializable]
	public struct TipsDB_Param
	{
		// Token: 0x04001304 RID: 4868
		public int m_ID;

		// Token: 0x04001305 RID: 4869
		public int m_Category;

		// Token: 0x04001306 RID: 4870
		public string m_Title;

		// Token: 0x04001307 RID: 4871
		public string m_Text;
	}
}
