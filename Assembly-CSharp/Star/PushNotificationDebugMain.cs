﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200028E RID: 654
	public class PushNotificationDebugMain : MonoBehaviour
	{
		// Token: 0x06000C35 RID: 3125 RVA: 0x0004782D File Offset: 0x00045C2D
		private void Start()
		{
		}

		// Token: 0x06000C36 RID: 3126 RVA: 0x0004782F File Offset: 0x00045C2F
		private void RegisterPushNotification()
		{
			if (this.m_Controller == null)
			{
				return;
			}
			this.m_Controller.RegisterDevice();
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x0004784E File Offset: 0x00045C4E
		private void UnregisterPushNotification()
		{
			if (this.m_Controller == null)
			{
				return;
			}
			this.m_Controller.UnregisterDevice();
		}

		// Token: 0x06000C38 RID: 3128 RVA: 0x00047870 File Offset: 0x00045C70
		public void OnClickRegisterUnregisterButton()
		{
			if (this.m_Window == null)
			{
				return;
			}
			if (!this.m_Window.activeSelf)
			{
				this.m_Window.SetActive(true);
				this.RegisterPushNotification();
				if (this.m_RegisterUnregisterButtonText != null)
				{
					this.m_RegisterUnregisterButtonText.text = "Unregister";
				}
			}
			else
			{
				this.m_Window.SetActive(false);
				this.UnregisterPushNotification();
				if (this.m_RegisterUnregisterButtonText != null)
				{
					this.m_RegisterUnregisterButtonText.text = "Register";
				}
			}
		}

		// Token: 0x06000C39 RID: 3129 RVA: 0x0004790A File Offset: 0x00045D0A
		public void OnClickUpdateLog()
		{
			if (this.m_Log == null)
			{
				return;
			}
			this.m_Log.text = this.m_Controller.GetLog();
		}

		// Token: 0x040014E7 RID: 5351
		[SerializeField]
		private PushNotificationController m_Controller;

		// Token: 0x040014E8 RID: 5352
		[SerializeField]
		private GameObject m_Window;

		// Token: 0x040014E9 RID: 5353
		[SerializeField]
		private Text m_RegisterUnregisterButtonText;

		// Token: 0x040014EA RID: 5354
		[SerializeField]
		private Text m_Log;
	}
}
