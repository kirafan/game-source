﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000275 RID: 629
	public class CharaViewerMain : MonoBehaviour
	{
		// Token: 0x06000BA9 RID: 2985 RVA: 0x00043E3D File Offset: 0x0004223D
		private void Start()
		{
			this.m_ProfileStatus = new ProfileStatus();
			this.m_CharaIDText.text = string.Empty;
			this.m_WeaponIDText.text = string.Empty;
			this.SetAnimInfoText(string.Empty);
			this.m_Step = CharaViewerMain.eStep.First;
		}

		// Token: 0x06000BAA RID: 2986 RVA: 0x00043E7C File Offset: 0x0004227C
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			if (Input.GetKeyDown(KeyCode.P))
			{
				this.m_IsDisplayProfileStatus = !this.m_IsDisplayProfileStatus;
			}
			switch (this.m_Step)
			{
			case CharaViewerMain.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.ApplyCharaID(0);
					this.m_Step = CharaViewerMain.eStep.Main;
				}
				break;
			case CharaViewerMain.eStep.Prepare:
			{
				CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaID);
				WeaponListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.m_WeaponID);
				CharacterParam characterParam = new CharacterParam();
				CharacterUtility.SetupCharacterParam(characterParam, 0L, param.m_CharaID, 1, 99, 0L, 0);
				characterParam.UniqueSkillLearnData = new SkillLearnData(param.m_CharaSkillID, 1, 10, 0L, param.m_CharaSkillExpTableID, true);
				characterParam.ClassSkillLearnDatas = new List<SkillLearnData>();
				for (int i = 0; i < param.m_ClassSkillIDs.Length; i++)
				{
					characterParam.ClassSkillLearnDatas.Add(new SkillLearnData(param.m_ClassSkillIDs[i], 1, 10, 0L, param.m_ClassSkillExpTableIDs[i], true));
				}
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupWeaponParam(weaponParam, 1L, this.m_WeaponID, 1, 0L, 1, 0L);
				this.m_CharaHndl = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, null, weaponParam, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Viewer, true, CharacterDefine.eFriendType.None, "CHARA_" + param.m_CharaID, null);
				if (this.m_CharaHndl != null)
				{
					this.m_CharaHndl.SetEnableRenderWeapon(this.m_IsWeaponRender);
					this.m_CharaHndl.Prepare();
					this.m_Step = CharaViewerMain.eStep.Prepare_Wait;
					Debug.LogFormat("Prepare Start >>> CharaResourceID:{0}, WeaponResourceID_L:{1}, WeaponResourceID_R:{2}".Green(), new object[]
					{
						param.m_ResourceID,
						param2.m_ResourceID_L,
						param2.m_ResourceID_R
					});
				}
				else
				{
					this.m_Step = CharaViewerMain.eStep.Main;
				}
				break;
			}
			case CharaViewerMain.eStep.Prepare_Wait:
				if (!this.m_CharaHndl.IsPreparing())
				{
					for (int j = 0; j < this.m_CharaHndl.CharaAnim.ModelSets.Length; j++)
					{
						if (this.m_CharaHndl.CharaAnim.ModelSets[j] != null && this.m_CharaHndl.CharaAnim.ModelSets[j].m_MeigeAnimCtrl != null)
						{
							this.m_CharaHndl.CharaAnim.ModelSets[j].m_MeigeAnimCtrl.m_bFixedDeltaTime = true;
						}
					}
					this.m_AnimIndex = 0;
					this.ApplyAnim(true);
					this.ApplyFacial();
					this.m_CharaHndl.CacheTransform.position = new Vector3(0f, 0f, 0f);
					this.m_Step = CharaViewerMain.eStep.Main;
				}
				break;
			case CharaViewerMain.eStep.Destroy_0:
				this.SetAnimInfoText(string.Empty);
				if (this.m_CharaHndl != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_CharaHndl);
					this.m_CharaHndl = null;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.UnloadAll(false);
				this.m_Step = CharaViewerMain.eStep.Destroy_1;
				break;
			case CharaViewerMain.eStep.Destroy_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.IsCompleteUnloadAll())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
					this.m_Step = CharaViewerMain.eStep.Prepare;
				}
				break;
			}
			this.m_ProfileStatus.Profile();
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x00044224 File Offset: 0x00042624
		private void LateUpdate()
		{
			CharaViewerMain.eStep step = this.m_Step;
			if (step == CharaViewerMain.eStep.Main)
			{
				this.UpdateAnimInfo();
			}
		}

		// Token: 0x06000BAC RID: 2988 RVA: 0x00044250 File Offset: 0x00042650
		private void ToggleCharaIndex(bool isRight)
		{
			CharacterListDB charaListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB;
			int num = -1;
			for (int i = 0; i < charaListDB.m_Params.Length; i++)
			{
				if (charaListDB.m_Params[i].m_CharaID == this.m_CharaID)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				return;
			}
			if (isRight)
			{
				num++;
				if (num >= charaListDB.m_Params.Length)
				{
					num = 0;
				}
			}
			else
			{
				num--;
				if (num < 0)
				{
					num = charaListDB.m_Params.Length - 1;
				}
			}
			this.ApplyCharaID(num);
		}

		// Token: 0x06000BAD RID: 2989 RVA: 0x000442F0 File Offset: 0x000426F0
		private void ApplyCharaID(int paramIndex)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_CharaID = dbMng.CharaListDB.m_Params[paramIndex].m_CharaID;
			this.m_CharaIDText.text = this.m_CharaID.ToString();
			if (dbMng.CharaListDB.IsExistParam(this.m_CharaID))
			{
				CharacterListDB_Param param = dbMng.CharaListDB.GetParam(this.m_CharaID);
				this.m_WeaponIDs.Clear();
				for (int i = 0; i < dbMng.WeaponListDB.m_Params.Length; i++)
				{
					if (dbMng.WeaponListDB.m_Params[i].m_ClassType == param.m_Class)
					{
						this.m_WeaponIDs.Add(dbMng.WeaponListDB.m_Params[i].m_ID);
					}
				}
				this.ApplyWeaponID(0);
			}
		}

		// Token: 0x06000BAE RID: 2990 RVA: 0x000443DC File Offset: 0x000427DC
		private void ToggleWeaponIndex(bool isRight)
		{
			int num = -1;
			for (int i = 0; i < this.m_WeaponIDs.Count; i++)
			{
				if (this.m_WeaponIDs[i] == this.m_WeaponID)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				return;
			}
			if (isRight)
			{
				num++;
				if (num >= this.m_WeaponIDs.Count)
				{
					num = 0;
				}
			}
			else
			{
				num--;
				if (num < 0)
				{
					num = this.m_WeaponIDs.Count - 1;
				}
			}
			this.ApplyWeaponID(num);
		}

		// Token: 0x06000BAF RID: 2991 RVA: 0x0004446F File Offset: 0x0004286F
		private void ApplyWeaponID(int paramIndex)
		{
			this.m_WeaponID = this.m_WeaponIDs[paramIndex];
			this.m_WeaponIDText.text = this.m_WeaponID.ToString();
		}

		// Token: 0x06000BB0 RID: 2992 RVA: 0x000444A0 File Offset: 0x000428A0
		public void OnDecideApplyCharaButton()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			if (!SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.IsExistParam(this.m_CharaID))
			{
				Debug.LogFormat("CharaID:{0} is not exist.", new object[]
				{
					this.m_CharaID
				});
				return;
			}
			if (!SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.IsExistParam(this.m_WeaponID))
			{
				this.m_WeaponID = this.m_WeaponIDs[0];
				this.m_WeaponIDText.text = this.m_WeaponID.ToString();
			}
			this.m_Step = CharaViewerMain.eStep.Destroy_0;
		}

		// Token: 0x06000BB1 RID: 2993 RVA: 0x0004454C File Offset: 0x0004294C
		public void OnValueChangedChara()
		{
			int charaID = 0;
			if (int.TryParse(this.m_CharaIDText.text, out charaID))
			{
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				this.m_CharaID = charaID;
				this.m_CharaIDText.text = this.m_CharaID.ToString();
				if (dbMng.CharaListDB.IsExistParam(this.m_CharaID))
				{
					CharacterListDB_Param param = dbMng.CharaListDB.GetParam(this.m_CharaID);
					this.m_WeaponIDs.Clear();
					for (int i = 0; i < dbMng.WeaponListDB.m_Params.Length; i++)
					{
						if (dbMng.WeaponListDB.m_Params[i].m_ClassType == param.m_Class)
						{
							this.m_WeaponIDs.Add(dbMng.WeaponListDB.m_Params[i].m_ID);
						}
					}
					this.ApplyWeaponID(0);
				}
			}
		}

		// Token: 0x06000BB2 RID: 2994 RVA: 0x0004463B File Offset: 0x00042A3B
		public void OnDecideChara_L()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleCharaIndex(false);
		}

		// Token: 0x06000BB3 RID: 2995 RVA: 0x00044651 File Offset: 0x00042A51
		public void OnDecideChara_R()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleCharaIndex(true);
		}

		// Token: 0x06000BB4 RID: 2996 RVA: 0x00044668 File Offset: 0x00042A68
		public void OnValueChangedWeapon()
		{
			int weaponID = 0;
			if (int.TryParse(this.m_WeaponIDText.text, out weaponID))
			{
				this.m_WeaponID = weaponID;
				this.m_WeaponIDText.text = this.m_WeaponID.ToString();
			}
		}

		// Token: 0x06000BB5 RID: 2997 RVA: 0x000446B1 File Offset: 0x00042AB1
		public void OnDecideWeapon_L()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleWeaponIndex(false);
		}

		// Token: 0x06000BB6 RID: 2998 RVA: 0x000446C7 File Offset: 0x00042AC7
		public void OnDecideWeapon_R()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleWeaponIndex(true);
		}

		// Token: 0x06000BB7 RID: 2999 RVA: 0x000446E0 File Offset: 0x00042AE0
		private void ToggleAnimIndex(bool isRight)
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			if (isRight)
			{
				this.m_AnimIndex++;
				if (this.m_AnimIndex >= this.m_CharaHndl.CharaResource.AnimKeyForViewer.Length)
				{
					this.m_AnimIndex = 0;
				}
			}
			else
			{
				this.m_AnimIndex--;
				if (this.m_AnimIndex < 0)
				{
					this.m_AnimIndex = this.m_CharaHndl.CharaResource.AnimKeyForViewer.Length - 1;
				}
			}
			this.ApplyAnim(true);
		}

		// Token: 0x06000BB8 RID: 3000 RVA: 0x0004479C File Offset: 0x00042B9C
		private void ApplyAnim(bool isPlay = true)
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			this.m_AnimKeyText.text = this.m_CharaHndl.CharaResource.AnimKeyForViewer[this.m_AnimIndex];
			this.m_AnimKey = this.m_AnimKeyText.text;
			if (isPlay)
			{
				this.m_CharaHndl.CharaAnim.PlayAnim(this.m_AnimKey, WrapMode.Loop, 0f, 0f);
				string[] array = this.m_AnimKey.Split(new char[]
				{
					'_'
				});
				if (array[array.Length - 1] == "R")
				{
					this.m_CharaHndl.CharaAnim.ChangeDir(CharacterDefine.eDir.R, false);
				}
				else
				{
					this.m_CharaHndl.CharaAnim.ChangeDir(CharacterDefine.eDir.L, false);
				}
				bool flag = false;
				if (!this.m_IsForceFacial)
				{
					for (int i = 0; i < this.BLINK_ANIM_KEY_LIST.Length; i++)
					{
						if (this.BLINK_ANIM_KEY_LIST[i] == this.m_AnimKey)
						{
							flag = true;
							break;
						}
					}
					this.m_CharaHndl.CharaAnim.SetBlink(flag);
					if (!flag && this.m_AnimKey == "abnormal")
					{
						this.m_CharaHndl.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_ABNORMAL_BATTLE[(int)this.m_CharaHndl.CharaAnim.Dir]);
					}
				}
				Debug.LogFormat("Play:{0}, isBlink:{1}", new object[]
				{
					this.m_AnimKey,
					flag
				});
			}
		}

		// Token: 0x06000BB9 RID: 3001 RVA: 0x00044946 File Offset: 0x00042D46
		public void OnDecideAnim_L()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(false);
		}

		// Token: 0x06000BBA RID: 3002 RVA: 0x0004495C File Offset: 0x00042D5C
		public void OnDecideAnim_R()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(true);
		}

		// Token: 0x06000BBB RID: 3003 RVA: 0x00044974 File Offset: 0x00042D74
		private void ToggleFacialIndex(bool isRight)
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			if (!this.m_IsForceFacial)
			{
				return;
			}
			if (isRight)
			{
				this.m_FacialIndex++;
				if (this.m_FacialIndex >= 100)
				{
					this.m_FacialIndex = 0;
				}
			}
			else
			{
				this.m_FacialIndex--;
				if (this.m_FacialIndex < 0)
				{
					this.m_FacialIndex = 99;
				}
			}
			this.ApplyFacial();
		}

		// Token: 0x06000BBC RID: 3004 RVA: 0x00044A18 File Offset: 0x00042E18
		private void ApplyFacial()
		{
			if (this.m_CharaHndl == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaAnim == null)
			{
				return;
			}
			if (this.m_CharaHndl.CharaResource == null)
			{
				return;
			}
			if (!this.m_IsForceFacial)
			{
				return;
			}
			this.m_FacialIDText.text = this.m_FacialIndex.ToString();
			this.m_CharaHndl.CharaAnim.SetFacial(this.m_FacialIndex);
			this.m_CharaHndl.CharaAnim.SetBlink(false);
		}

		// Token: 0x06000BBD RID: 3005 RVA: 0x00044AA8 File Offset: 0x00042EA8
		public void OnDecideFacial_L()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleFacialIndex(false);
		}

		// Token: 0x06000BBE RID: 3006 RVA: 0x00044ABE File Offset: 0x00042EBE
		public void OnDecideFacial_R()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleFacialIndex(true);
		}

		// Token: 0x06000BBF RID: 3007 RVA: 0x00044AD4 File Offset: 0x00042ED4
		public void OnToggleForceFacial(bool value)
		{
			this.m_IsForceFacial = this.m_ToggleForceFacial.isOn;
			if (!(this.m_CharaHndl != null) || this.m_CharaHndl.CharaAnim == null || this.m_CharaHndl.CharaResource != null)
			{
			}
			if (this.m_IsForceFacial)
			{
				this.ApplyFacial();
			}
			else
			{
				this.ApplyAnim(true);
			}
		}

		// Token: 0x06000BC0 RID: 3008 RVA: 0x00044B40 File Offset: 0x00042F40
		private void UpdateAnimInfo()
		{
			StringBuilder stringBuilder = new StringBuilder();
			int value = 0;
			float num = 0f;
			float num2 = 0f;
			if (this.m_CharaHndl != null && this.m_CharaHndl.CharaAnim != null && this.m_CharaHndl.CharaResource != null)
			{
				value = this.m_CharaHndl.CharaAnim.GetPlayingFrame(this.m_AnimKey, 0);
				num = this.m_CharaHndl.CharaAnim.GetPlayingSec(this.m_AnimKey, 0);
				num2 = this.m_CharaHndl.CharaAnim.GetMaxSec(this.m_AnimKey, 0);
			}
			stringBuilder.Append("Frame : ");
			stringBuilder.Append(value);
			stringBuilder.Append("\n");
			stringBuilder.Append("Sec : ");
			stringBuilder.Append(num.ToString("f4"));
			stringBuilder.Append("/");
			stringBuilder.Append(num2.ToString("f4"));
			stringBuilder.Append("\n");
			if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.IsExistParam(this.m_CharaID))
			{
				CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaID);
				stringBuilder.Append("CharaResourceID:");
				stringBuilder.Append(param.m_ResourceID);
				stringBuilder.Append("\n");
				stringBuilder.Append("Class:");
				stringBuilder.Append((eClassType)param.m_Class);
				stringBuilder.Append("\n");
				stringBuilder.Append(param.m_Name);
				stringBuilder.Append("\n");
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.IsExistParam(this.m_CharaID))
			{
				WeaponListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.m_WeaponID);
				stringBuilder.Append("WeaponResourceID:");
				stringBuilder.Append(param2.m_ResourceID_L);
				stringBuilder.Append(",");
				stringBuilder.Append(param2.m_ResourceID_R);
				stringBuilder.Append("\n");
				stringBuilder.Append(param2.m_WeaponName);
				stringBuilder.Append("\n");
			}
			this.SetAnimInfoText(stringBuilder.ToString());
		}

		// Token: 0x06000BC1 RID: 3009 RVA: 0x00044D8B File Offset: 0x0004318B
		private void SetAnimInfoText(string txt)
		{
			this.m_AnimInfoText.text = txt;
		}

		// Token: 0x06000BC2 RID: 3010 RVA: 0x00044D99 File Offset: 0x00043199
		public void OnToggleWeapon(bool value)
		{
			this.m_IsWeaponRender = this.m_ToggleWeapon.isOn;
			if (this.m_CharaHndl != null)
			{
				this.m_CharaHndl.SetEnableRenderWeapon(this.m_IsWeaponRender);
			}
		}

		// Token: 0x06000BC3 RID: 3011 RVA: 0x00044DCE File Offset: 0x000431CE
		public void OnToggleBG(bool value)
		{
			this.m_BG.SetActive(this.m_ToggleBG.isOn);
		}

		// Token: 0x06000BC4 RID: 3012 RVA: 0x00044DE6 File Offset: 0x000431E6
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * value;
		}

		// Token: 0x06000BC5 RID: 3013 RVA: 0x00044E09 File Offset: 0x00043209
		public void OnDecideTitleButton()
		{
			if (this.m_Step != CharaViewerMain.eStep.Main)
			{
				return;
			}
			APIUtility.ReturnTitle(true);
		}

		// Token: 0x04001415 RID: 5141
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x04001416 RID: 5142
		[SerializeField]
		private InputField m_CharaIDText;

		// Token: 0x04001417 RID: 5143
		[SerializeField]
		private InputField m_WeaponIDText;

		// Token: 0x04001418 RID: 5144
		[SerializeField]
		private Text m_AnimKeyText;

		// Token: 0x04001419 RID: 5145
		[SerializeField]
		private Text m_AnimInfoText;

		// Token: 0x0400141A RID: 5146
		[SerializeField]
		private Text m_FacialIDText;

		// Token: 0x0400141B RID: 5147
		[SerializeField]
		private Toggle m_ToggleForceFacial;

		// Token: 0x0400141C RID: 5148
		[SerializeField]
		private Toggle m_ToggleWeapon;

		// Token: 0x0400141D RID: 5149
		[SerializeField]
		private Toggle m_ToggleBG;

		// Token: 0x0400141E RID: 5150
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x0400141F RID: 5151
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x04001420 RID: 5152
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x04001421 RID: 5153
		private readonly string[] BLINK_ANIM_KEY_LIST = new string[]
		{
			"idle",
			"room_idle_L",
			"room_idle_R",
			"room_idle_skirt_L",
			"room_idle_skirt_R",
			"room_walk_L",
			"room_walk_R",
			"room_sit_st_L",
			"room_sit_lp_L",
			"room_sit_ed_L",
			"room_sit_st_R",
			"room_sit_lp_R",
			"room_sit_ed_R"
		};

		// Token: 0x04001422 RID: 5154
		private CharacterHandler m_CharaHndl;

		// Token: 0x04001423 RID: 5155
		private int m_CharaID = -1;

		// Token: 0x04001424 RID: 5156
		private int m_WeaponID = -1;

		// Token: 0x04001425 RID: 5157
		private int m_AnimIndex;

		// Token: 0x04001426 RID: 5158
		private List<int> m_WeaponIDs = new List<int>();

		// Token: 0x04001427 RID: 5159
		private bool m_IsWeaponRender = true;

		// Token: 0x04001428 RID: 5160
		private string m_AnimKey;

		// Token: 0x04001429 RID: 5161
		private bool m_IsForceFacial;

		// Token: 0x0400142A RID: 5162
		private int m_FacialIndex;

		// Token: 0x0400142B RID: 5163
		public bool m_IsDisplayProfileStatus;

		// Token: 0x0400142C RID: 5164
		private ProfileStatus m_ProfileStatus;

		// Token: 0x0400142D RID: 5165
		private CharaViewerMain.eStep m_Step = CharaViewerMain.eStep.None;

		// Token: 0x02000276 RID: 630
		private enum eStep
		{
			// Token: 0x0400142F RID: 5167
			None = -1,
			// Token: 0x04001430 RID: 5168
			First,
			// Token: 0x04001431 RID: 5169
			Prepare,
			// Token: 0x04001432 RID: 5170
			Prepare_Wait,
			// Token: 0x04001433 RID: 5171
			Main,
			// Token: 0x04001434 RID: 5172
			Destroy_0,
			// Token: 0x04001435 RID: 5173
			Destroy_1
		}
	}
}
