﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B1 RID: 1457
	public class IRoomFloorManager
	{
		// Token: 0x06001C25 RID: 7205 RVA: 0x0009593A File Offset: 0x00093D3A
		public Color GetBaseColor()
		{
			return this.m_BasePer;
		}

		// Token: 0x06001C26 RID: 7206 RVA: 0x00095944 File Offset: 0x00093D44
		public static IRoomFloorManager CreateFloorManager(RoomFloorCateory pcategory)
		{
			IRoomFloorManager roomFloorManager = null;
			RoomFloorCateory.eCategory type = pcategory.m_Type;
			if (type != RoomFloorCateory.eCategory.Main)
			{
				if (type == RoomFloorCateory.eCategory.Sleep)
				{
					roomFloorManager = new IRoomFloorManager();
					roomFloorManager.m_BasePer = new Color(0.6f, 0.6f, 0.7f);
				}
			}
			else
			{
				roomFloorManager = new IRoomFloorManager();
				roomFloorManager.m_BasePer = Color.white;
			}
			roomFloorManager.m_Type = pcategory.m_Type;
			roomFloorManager.m_PosX = pcategory.m_PosX;
			roomFloorManager.m_PosY = pcategory.m_PosY;
			roomFloorManager.m_SizeX = pcategory.m_SizeX;
			roomFloorManager.m_SizeY = pcategory.m_SizeY;
			roomFloorManager.m_SizeZ = pcategory.m_SizeZ;
			roomFloorManager.m_RoomOffset = pcategory.m_RoomOffset;
			roomFloorManager.m_WallOffset = pcategory.m_WallOffset;
			return roomFloorManager;
		}

		// Token: 0x06001C27 RID: 7207 RVA: 0x00095A08 File Offset: 0x00093E08
		public GameObject GetRoot()
		{
			return this.m_Root;
		}

		// Token: 0x06001C28 RID: 7208 RVA: 0x00095A10 File Offset: 0x00093E10
		public void CreateObject(Transform pparent, bool fmakewall)
		{
			this.SetUpList();
			this.m_Root = new GameObject("Floor" + this.m_Type);
			this.m_Root.transform.SetParent(pparent);
			RoomFloorDebugInspect roomFloorDebugInspect = this.m_Root.AddComponent<RoomFloorDebugInspect>();
			roomFloorDebugInspect.m_LinkFloor = this;
		}

		// Token: 0x06001C29 RID: 7209 RVA: 0x00095A68 File Offset: 0x00093E68
		public void GetMoveArea(out IVector4 prect, out IVector2 psize, bool fcategory)
		{
			prect.x = 0;
			prect.y = 0;
			prect.z = this.m_SizeX;
			prect.w = this.m_SizeY;
			psize.x = this.m_SizeX;
			psize.y = this.m_SizeY;
			if (fcategory)
			{
				psize.x += this.m_SizeZ;
				psize.y += this.m_SizeZ;
				prect.x = 0;
				prect.y = 0;
				prect.z = this.m_SizeX;
				prect.w = this.m_SizeY;
			}
		}

		// Token: 0x06001C2A RID: 7210 RVA: 0x00095B08 File Offset: 0x00093F08
		public IVector3 GetMoveSize()
		{
			IVector3 result;
			result.x = this.m_SizeX;
			result.y = this.m_SizeY;
			result.z = this.m_SizeZ;
			return result;
		}

		// Token: 0x06001C2B RID: 7211 RVA: 0x00095B3D File Offset: 0x00093F3D
		public Vector2 GetOffsetPos()
		{
			return this.m_BuildPosOffset;
		}

		// Token: 0x06001C2C RID: 7212 RVA: 0x00095B48 File Offset: 0x00093F48
		public GameObject CreateFloorNode()
		{
			GameObject gameObject = new GameObject("RoomFloor" + this.m_Type);
			gameObject.transform.SetParent(this.m_Root.transform, false);
			return gameObject;
		}

		// Token: 0x06001C2D RID: 7213 RVA: 0x00095B88 File Offset: 0x00093F88
		public void SetFloor(IRoomObjectControll pobj)
		{
			this.m_Floor = (pobj as RoomObjectMdlSprite);
			this.m_Floor.SetBaseColor(this.m_BasePer);
		}

		// Token: 0x06001C2E RID: 7214 RVA: 0x00095BA7 File Offset: 0x00093FA7
		public IRoomObjectControll GetFloor()
		{
			return this.m_Floor;
		}

		// Token: 0x06001C2F RID: 7215 RVA: 0x00095BB0 File Offset: 0x00093FB0
		public void ChangeFloor(IRoomObjectControll pobj)
		{
			if (this.m_Floor != null)
			{
				pobj.transform.localPosition = this.m_Floor.transform.localPosition;
				this.m_Floor.Destroy();
				UnityEngine.Object.Destroy(this.m_Floor.gameObject);
				this.m_Floor = null;
			}
			this.m_Floor = (pobj as RoomObjectMdlSprite);
			this.m_Floor.SetBaseColor(this.m_BasePer);
		}

		// Token: 0x06001C30 RID: 7216 RVA: 0x00095C28 File Offset: 0x00094028
		public GameObject CreateWallNode()
		{
			GameObject gameObject = new GameObject("RoomWall" + this.m_Type);
			gameObject.transform.SetParent(this.m_Root.transform, false);
			return gameObject;
		}

		// Token: 0x06001C31 RID: 7217 RVA: 0x00095C68 File Offset: 0x00094068
		public void SetWall(IRoomObjectControll pobj)
		{
			this.m_Wall = (pobj as RoomObjectMdlSprite);
			this.m_Wall.SetBaseColor(this.m_BasePer);
		}

		// Token: 0x06001C32 RID: 7218 RVA: 0x00095C87 File Offset: 0x00094087
		public IRoomObjectControll GetWall()
		{
			return this.m_Wall;
		}

		// Token: 0x06001C33 RID: 7219 RVA: 0x00095C90 File Offset: 0x00094090
		public void ChangeWall(IRoomObjectControll pobj)
		{
			if (this.m_Wall != null)
			{
				pobj.transform.localPosition = this.m_Wall.transform.localPosition;
				this.m_Wall.Destroy();
				UnityEngine.Object.Destroy(this.m_Wall.gameObject);
			}
			this.m_Wall = (pobj as RoomObjectMdlSprite);
			this.m_Wall.SetBaseColor(this.m_BasePer);
		}

		// Token: 0x06001C34 RID: 7220 RVA: 0x00095D04 File Offset: 0x00094104
		public bool IsSetUp()
		{
			bool flag = this.m_Floor.IsAvailable();
			if (this.m_Wall != null)
			{
				flag &= this.m_Wall.IsAvailable();
			}
			return flag;
		}

		// Token: 0x06001C35 RID: 7221 RVA: 0x00095D40 File Offset: 0x00094140
		public void BuildUp(int fgroupno)
		{
			Vector2 zero = Vector2.zero;
			Vector2 blockSize = RoomUtility.GetBlockSize((float)this.m_SizeX, 0f);
			Vector2 blockSize2 = RoomUtility.GetBlockSize((float)this.m_SizeX, (float)this.m_SizeY);
			Vector2 blockSize3 = RoomUtility.GetBlockSize(0f, (float)this.m_SizeY);
			Vector2 v;
			v.y = (blockSize2.y + zero.y) * 0.5f;
			v.x = (blockSize3.x + blockSize.x) * 0.5f;
			float y = RoomUtility.GetBlockSize(1f, 0f).y;
			this.m_BuildPosOffset = Vector2.zero;
			Vector3 localPosition = Vector3.zero;
			localPosition.x += this.m_PosX;
			localPosition.y += this.m_PosY;
			this.m_Root.transform.localPosition = localPosition;
			this.m_Floor.SetSortingOrder(0f, RoomUtility.GetFloorLayer(fgroupno));
			localPosition = v;
			localPosition.x += this.m_RoomOffset.x;
			localPosition.y += this.m_RoomOffset.y - y;
			this.m_Floor.gameObject.transform.localPosition = localPosition;
			if (this.m_Wall != null)
			{
				localPosition = Vector3.zero;
				localPosition.x += this.m_WallOffset.x + v.x + this.m_RoomOffset.x;
				localPosition.y += this.m_WallOffset.y + v.y + this.m_RoomOffset.y - y;
				this.m_Wall.gameObject.transform.localPosition = localPosition;
				this.m_Wall.GetComponent<IRoomObjectControll>().SetSortingOrder(0f, RoomUtility.GetWallLayer(fgroupno));
			}
			this.m_BuildPosOffset.x = this.m_PosX;
			this.m_BuildPosOffset.y = this.m_PosY;
			Vector2 buildPosOffset = this.m_BuildPosOffset;
			buildPosOffset.x -= this.m_PosX;
			buildPosOffset.y -= this.m_PosY;
			this.TilingFloor(buildPosOffset);
		}

		// Token: 0x06001C36 RID: 7222 RVA: 0x00095F9C File Offset: 0x0009439C
		public void SetSortOrder(float forderz)
		{
			this.m_Floor.transform.localPosZ(forderz);
			this.m_HitTileGroup.transform.localPosZ(forderz);
			if (this.m_Wall != null)
			{
				this.m_Wall.transform.localPosZ(forderz + 3f);
				this.m_HitWallGroup.transform.localPosZ(forderz + 3f);
			}
		}

		// Token: 0x06001C37 RID: 7223 RVA: 0x0009600C File Offset: 0x0009440C
		public void Destroy()
		{
			if (this.m_Wall != null)
			{
				this.m_Wall.Destroy();
			}
			if (this.m_Floor != null)
			{
				this.m_Floor.Destroy();
			}
			this.m_TilingFloors.Clear();
			this.m_Wall = null;
			this.m_Floor = null;
			this.m_Root = null;
		}

		// Token: 0x06001C38 RID: 7224 RVA: 0x00096074 File Offset: 0x00094474
		public void SetGridView(RoomBuilder.eEditObject fedit, bool fview)
		{
			if (fedit != RoomBuilder.eEditObject.Floor)
			{
				if (fedit == RoomBuilder.eEditObject.Wall)
				{
					if (this.m_HitWallGroup)
					{
						Renderer[] componentsInChildren = this.m_HitWallGroup.GetComponentsInChildren<Renderer>();
						for (int i = 0; i < componentsInChildren.Length; i++)
						{
							componentsInChildren[i].enabled = fview;
						}
					}
				}
			}
			else if (this.m_HitTileGroup)
			{
				Renderer[] componentsInChildren2 = this.m_HitTileGroup.GetComponentsInChildren<Renderer>();
				for (int j = 0; j < componentsInChildren2.Length; j++)
				{
					componentsInChildren2[j].enabled = fview;
				}
			}
		}

		// Token: 0x06001C39 RID: 7225 RVA: 0x00096110 File Offset: 0x00094510
		public void TilingFloor(Vector2 foffset)
		{
			this.m_HitTileGroup = new GameObject("HitTileGroup");
			this.m_HitTileGroup.transform.SetParent(this.m_Root.transform, false);
			this.m_RenderMaterial = new Material(Shader.Find("Room/GridLine"));
			Vector2 blockSize = RoomUtility.GetBlockSize(1f, 0f);
			this.m_HitTileLine = new Vector3[]
			{
				new Vector3(0f, 0f, 0f),
				new Vector3(blockSize.x * (float)this.m_SizeX, blockSize.y * (float)this.m_SizeX, 0f),
				new Vector3(blockSize.x * (float)this.m_SizeX - blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeX + blockSize.y * (float)this.m_SizeY, 0f),
				new Vector3(-blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeY, 0f),
				new Vector3(0f, 0f, 0f)
			};
			this.m_HitTileGrid = new Vector2[]
			{
				new Vector2(0f, 0f),
				new Vector2((float)this.m_SizeX, 0f),
				new Vector2((float)this.m_SizeX, (float)this.m_SizeY),
				new Vector2(0f, (float)this.m_SizeY),
				new Vector2(0f, 0f)
			};
			for (int i = 0; i < this.m_SizeY; i++)
			{
				for (int j = 0; j < this.m_SizeX; j++)
				{
					GameObject gameObject = new GameObject(string.Format("F_{0}_{1}", j, i));
					this.BuildFloorCollider(gameObject, foffset, j, i);
					gameObject.transform.SetParent(this.m_HitTileGroup.transform, false);
					this.m_TilingFloors.Add(gameObject);
				}
			}
			if (this.m_Wall != null)
			{
				float num = blockSize.y * 2f * (float)this.m_SizeZ;
				this.m_HitWallLine = new Vector3[]
				{
					new Vector3(-blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeY, 0f),
					new Vector3(blockSize.x * (float)this.m_SizeX - blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeX + blockSize.y * (float)this.m_SizeY, 0f),
					new Vector3(blockSize.x * (float)this.m_SizeX, blockSize.y * (float)this.m_SizeX, 0f),
					new Vector3(blockSize.x * (float)this.m_SizeX, blockSize.y * (float)this.m_SizeX + num, 0f),
					new Vector3(blockSize.x * (float)this.m_SizeX - blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeX + blockSize.y * (float)this.m_SizeY + num, 0f),
					new Vector3(-blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeY + num, 0f),
					new Vector3(-blockSize.x * (float)this.m_SizeY, blockSize.y * (float)this.m_SizeY, 0f)
				};
				this.m_HitWallGrid = new Vector2[]
				{
					new Vector2(0f, (float)this.m_SizeY),
					new Vector2((float)this.m_SizeX, (float)this.m_SizeY),
					new Vector2((float)this.m_SizeX, 0f),
					new Vector3((float)(this.m_SizeX + this.m_SizeZ), 0f),
					new Vector3((float)(this.m_SizeX + this.m_SizeZ), (float)(this.m_SizeY + this.m_SizeZ)),
					new Vector3(0f, (float)(this.m_SizeY + this.m_SizeZ)),
					new Vector2(0f, (float)this.m_SizeY)
				};
				this.m_HitWallGroup = new GameObject("HitWallGroup");
				this.m_HitWallGroup.transform.SetParent(this.m_Root.transform, false);
				for (int j = 0; j < this.m_SizeX; j++)
				{
					for (int i = this.m_SizeY; i < this.m_SizeY + this.m_SizeZ; i++)
					{
						GameObject gameObject = new GameObject(string.Format("W_{0}_{1}", j, i));
						this.BuildWallCollider(gameObject, true, foffset, j, i);
						gameObject.transform.SetParent(this.m_HitWallGroup.transform, false);
						this.m_TilingFloors.Add(gameObject);
					}
				}
				for (int i = this.m_SizeY - 1; i >= 0; i--)
				{
					for (int j = this.m_SizeX; j < this.m_SizeX + this.m_SizeZ; j++)
					{
						GameObject gameObject = new GameObject(string.Format("W_{0}_{1}", j, i));
						this.BuildWallCollider(gameObject, false, foffset, j, i);
						gameObject.transform.SetParent(this.m_HitWallGroup.transform, false);
						this.m_TilingFloors.Add(gameObject);
					}
				}
			}
		}

		// Token: 0x06001C3A RID: 7226 RVA: 0x000967B4 File Offset: 0x00094BB4
		private void BuildFloorCollider(GameObject pobj, Vector2 foffset, int fpointx, int fpointy)
		{
			Vector3[] array = new Vector3[4];
			Color32[] array2 = new Color32[4];
			int[] array3 = new int[6];
			Vector2 blockSize = RoomUtility.GetBlockSize(1f, 0f);
			MeshCollider meshCollider = pobj.AddComponent<MeshCollider>();
			Mesh mesh = new Mesh();
			pobj.layer = LayerMask.NameToLayer("OneFloor");
			RoomHitObject roomHitObject = pobj.AddComponent<RoomHitObject>();
			roomHitObject.SetGridInfo(fpointx, fpointy, (int)this.m_Type);
			array[0] = new Vector3(0f, blockSize.y, -1f);
			array[1] = new Vector3(-blockSize.x, 0f, -1f);
			array[2] = new Vector3(blockSize.x, 0f, -1f);
			array[3] = new Vector3(0f, -blockSize.y, -1f);
			array2[0] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[1] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[2] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[3] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array3[0] = 0;
			array3[1] = 2;
			array3[2] = 1;
			array3[3] = 1;
			array3[4] = 2;
			array3[5] = 3;
			mesh.vertices = array;
			mesh.SetIndices(array3, MeshTopology.Triangles, 0);
			mesh.colors32 = array2;
			mesh.RecalculateBounds();
			meshCollider.sharedMesh = mesh;
			Vector2 vector = RoomUtility.GridToTilePos((float)fpointx, (float)fpointy);
			vector += foffset;
			pobj.transform.position = vector;
			int[] array4 = new int[5];
			mesh = new Mesh();
			array4[0] = 0;
			array4[1] = 1;
			array4[2] = 3;
			array4[3] = 2;
			array4[4] = 0;
			MeshFilter meshFilter = pobj.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = pobj.AddComponent<MeshRenderer>();
			mesh.vertices = array;
			mesh.colors32 = array2;
			mesh.SetIndices(array4, MeshTopology.LineStrip, 0);
			meshFilter.mesh = mesh;
			meshRenderer.material = this.m_RenderMaterial;
			meshRenderer.enabled = false;
		}

		// Token: 0x06001C3B RID: 7227 RVA: 0x00096A14 File Offset: 0x00094E14
		private void BuildWallCollider(GameObject pobj, bool fside, Vector2 foffset, int fpointx, int fpointy)
		{
			Vector3[] array = new Vector3[4];
			Color32[] array2 = new Color32[4];
			int[] array3 = new int[6];
			Vector2 blockSize = RoomUtility.GetBlockSize(1f, 0f);
			pobj.layer = LayerMask.NameToLayer("OneWall");
			MeshCollider meshCollider = pobj.AddComponent<MeshCollider>();
			Mesh mesh = new Mesh();
			RoomHitObject roomHitObject = pobj.AddComponent<RoomHitObject>();
			roomHitObject.SetGridInfo(fpointx, fpointy, (int)this.m_Type);
			Vector2 vector;
			if (fside)
			{
				vector = RoomUtility.GridToTilePos((float)fpointx, (float)this.m_SizeY);
				vector.y += blockSize.y * 2f * (float)(fpointy - this.m_SizeY);
				array[0] = new Vector3(0f, -blockSize.y + 2f * blockSize.y, -1f);
				array[1] = new Vector3(blockSize.x, 2f * blockSize.y, -1f);
				array[2] = new Vector3(0f, -blockSize.y, -1f);
				array[3] = new Vector3(blockSize.x, 0f, -1f);
				array3[0] = 0;
				array3[1] = 1;
				array3[2] = 2;
				array3[3] = 1;
				array3[4] = 3;
				array3[5] = 2;
			}
			else
			{
				vector = RoomUtility.GridToTilePos((float)this.m_SizeX, (float)fpointy);
				vector.y += blockSize.y * 2f * (float)(fpointx - this.m_SizeX);
				array[0] = new Vector3(0f, -blockSize.y + 2f * blockSize.y, -1f);
				array[1] = new Vector3(-blockSize.x, 2f * blockSize.y, -1f);
				array[2] = new Vector3(0f, -blockSize.y, -1f);
				array[3] = new Vector3(-blockSize.x, 0f, -1f);
				array3[0] = 0;
				array3[1] = 2;
				array3[2] = 1;
				array3[3] = 1;
				array3[4] = 2;
				array3[5] = 3;
			}
			vector += foffset;
			pobj.transform.position = vector;
			array2[0] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[1] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[2] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			array2[3] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			mesh.vertices = array;
			mesh.SetIndices(array3, MeshTopology.Triangles, 0);
			mesh.RecalculateBounds();
			meshCollider.sharedMesh = mesh;
			int[] array4 = new int[5];
			mesh = new Mesh();
			array4[0] = 0;
			array4[1] = 1;
			array4[2] = 3;
			array4[3] = 2;
			array4[4] = 0;
			MeshFilter meshFilter = pobj.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = pobj.AddComponent<MeshRenderer>();
			mesh.vertices = array;
			mesh.colors32 = array2;
			mesh.SetIndices(array4, MeshTopology.LineStrip, 0);
			meshFilter.mesh = mesh;
			meshRenderer.material = this.m_RenderMaterial;
			meshRenderer.enabled = false;
		}

		// Token: 0x06001C3C RID: 7228 RVA: 0x00096DBC File Offset: 0x000951BC
		public bool CheckLineCross(Vector3 fstpt1, Vector3 fedpt1, Vector3 fstpt2, Vector3 fedpt2)
		{
			float num = (fstpt2.x - fedpt2.x) * (fstpt1.y - fstpt2.y) + (fstpt2.y - fedpt2.y) * (fstpt2.x - fstpt1.x);
			float num2 = (fstpt2.x - fedpt2.x) * (fedpt1.y - fstpt2.y) + (fstpt2.y - fedpt2.y) * (fstpt2.x - fedpt1.x);
			float num3 = (fstpt1.x - fedpt1.x) * (fstpt2.y - fstpt1.y) + (fstpt1.y - fedpt1.y) * (fstpt1.x - fstpt2.x);
			float num4 = (fstpt1.x - fedpt1.x) * (fedpt2.y - fstpt1.y) + (fstpt1.y - fedpt1.y) * (fstpt1.x - fedpt2.x);
			return num3 * num4 < 0f && num * num2 < 0f;
		}

		// Token: 0x06001C3D RID: 7229 RVA: 0x00096EE4 File Offset: 0x000952E4
		public void CalcLineToPerpendicular(ref Vector3 pans, Vector3 fpoint, Vector3 fstpt, Vector3 fedpt)
		{
			Vector3 vector = fedpt - fstpt;
			Vector3 vector2 = fpoint - fstpt;
			float num = (vector.x * vector2.x + vector.y * vector2.y) / (vector.x * vector.x + vector.y * vector.y);
			pans.x = fstpt.x + vector.x * num;
			pans.y = fstpt.y + vector.y * num;
			pans.z = num;
		}

		// Token: 0x06001C3E RID: 7230 RVA: 0x00096F78 File Offset: 0x00095378
		public Vector2 CalcGridPos(float fpoint, Vector2 fst, Vector2 fend)
		{
			Vector2 zero = Vector2.zero;
			zero.x = (fend.x - fst.x) * fpoint + fst.x;
			zero.y = (fend.y - fst.y) * fpoint + fst.y;
			return zero;
		}

		// Token: 0x06001C3F RID: 7231 RVA: 0x00096FCC File Offset: 0x000953CC
		public bool CheckCrossLine(ref IVector3 pout, Vector3 fstartpt, Vector3 fendpt, bool fcategory)
		{
			Vector3 zero = Vector3.zero;
			fstartpt.y += 0.35f;
			fendpt.y += 0.35f;
			if (fcategory)
			{
				int num = this.m_HitWallLine.Length - 1;
				fstartpt -= this.m_HitTileGroup.transform.position;
				fendpt -= this.m_HitTileGroup.transform.position;
				for (int i = 0; i < num; i++)
				{
					if (this.CheckLineCross(fstartpt, fendpt, this.m_HitWallLine[i], this.m_HitWallLine[i + 1]))
					{
						this.CalcLineToPerpendicular(ref zero, fendpt, this.m_HitWallLine[i], this.m_HitWallLine[i + 1]);
						Vector2 vector = this.CalcGridPos(zero.z, this.m_HitWallGrid[i], this.m_HitWallGrid[i + 1]);
						pout.x = (int)vector.x;
						pout.y = (int)vector.y;
						pout.z = 0;
						return true;
					}
				}
			}
			else
			{
				int num = this.m_HitTileLine.Length - 1;
				fstartpt -= this.m_HitTileGroup.transform.position;
				fendpt -= this.m_HitTileGroup.transform.position;
				for (int i = 0; i < num; i++)
				{
					if (this.CheckLineCross(fstartpt, fendpt, this.m_HitTileLine[i], this.m_HitTileLine[i + 1]))
					{
						this.CalcLineToPerpendicular(ref zero, fendpt, this.m_HitTileLine[i], this.m_HitTileLine[i + 1]);
						Vector2 vector = this.CalcGridPos(zero.z, this.m_HitTileGrid[i], this.m_HitTileGrid[i + 1]);
						pout.x = (int)vector.x;
						pout.y = (int)vector.y;
						pout.z = 0;
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06001C40 RID: 7232 RVA: 0x00097216 File Offset: 0x00095616
		public int GetObjectListNum()
		{
			return this.m_EntryNum;
		}

		// Token: 0x06001C41 RID: 7233 RVA: 0x0009721E File Offset: 0x0009561E
		public IRoomObjectControll GetIndexToHandle(int findex)
		{
			return this.m_List[findex].m_Object;
		}

		// Token: 0x06001C42 RID: 7234 RVA: 0x0009722D File Offset: 0x0009562D
		public void SetUpList()
		{
			this.m_List = new IRoomFloorManager.RoomSortObject[10];
			this.m_MaxList = 10;
		}

		// Token: 0x06001C43 RID: 7235 RVA: 0x00097244 File Offset: 0x00095644
		public void AttachSortObject(IRoomObjectControll pobj, int fcompkey)
		{
			bool flag = false;
			int num = 0;
			for (int i = 0; i < this.m_MaxList; i++)
			{
				if (this.m_List[i] == null)
				{
					num = i;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				IRoomFloorManager.RoomSortObject[] array = new IRoomFloorManager.RoomSortObject[this.m_MaxList + 10];
				for (int i = 0; i < this.m_MaxList; i++)
				{
					array[i] = this.m_List[i];
				}
				this.m_List = null;
				this.m_List = array;
				this.m_MaxList += 10;
				num = this.m_EntryNum;
			}
			this.m_List[num] = new IRoomFloorManager.RoomSortObject(pobj);
			this.m_List[num].m_CompKey = fcompkey;
			pobj.SetBaseColor(this.m_BasePer);
			this.m_EntryNum++;
		}

		// Token: 0x06001C44 RID: 7236 RVA: 0x00097314 File Offset: 0x00095714
		public void DetachObject(IRoomObjectControll pobj)
		{
			int num = -1;
			for (int i = 0; i < this.m_MaxList; i++)
			{
				if (this.m_List[i] != null && this.m_List[i].m_Object == pobj)
				{
					this.m_List[i] = null;
					num = i;
					break;
				}
			}
			if (num != -1)
			{
				for (int i = num + 1; i < this.m_EntryNum; i++)
				{
					this.m_List[i - 1] = this.m_List[i];
				}
				this.m_EntryNum--;
				this.m_List[this.m_EntryNum] = null;
			}
		}

		// Token: 0x06001C45 RID: 7237 RVA: 0x000973C0 File Offset: 0x000957C0
		private void ListSort()
		{
			int i;
			for (i = 0; i < this.m_EntryNum; i++)
			{
				this.m_List[i].UpdateIsoBounds();
			}
			for (i = 0; i < this.m_EntryNum; i++)
			{
				IRoomFloorManager.RoomSortObject roomSortObject = this.m_List[i];
				for (int j = i + 1; j < this.m_EntryNum; j++)
				{
					if (roomSortObject.m_Key > this.m_List[j].m_Key)
					{
						roomSortObject = this.m_List[j];
						this.m_List[j] = this.m_List[i];
						this.m_List[i] = roomSortObject;
					}
					else if (roomSortObject.m_Key == this.m_List[j].m_Key && roomSortObject.m_CompKey > this.m_List[j].m_CompKey)
					{
						roomSortObject = this.m_List[j];
						this.m_List[j] = this.m_List[i];
						this.m_List[i] = roomSortObject;
					}
				}
			}
			i = 0;
			int num = 0;
			while (i < this.m_EntryNum)
			{
				IRoomFloorManager.RoomSortObject roomSortObject = this.m_List[i];
				if (!roomSortObject.m_Block.IsLinkObject())
				{
					bool flag = false;
					for (int j = i + 1; j < this.m_EntryNum; j++)
					{
						if (!this.m_List[j].m_Block.IsLinkObject())
						{
							int isoBoxCross = RoomIso.GetIsoBoxCross(roomSortObject.m_Box, this.m_List[j].m_Box);
							if (isoBoxCross != 0)
							{
								RoomBlockData frontBlockCross = RoomIso.GetFrontBlockCross(roomSortObject.m_Block, this.m_List[j].m_Block, isoBoxCross);
								if (frontBlockCross != null && roomSortObject.m_Block != frontBlockCross)
								{
									roomSortObject = this.m_List[j];
									this.m_List[j] = this.m_List[i];
									this.m_List[i] = roomSortObject;
									flag = true;
								}
							}
						}
						if (flag)
						{
							break;
						}
					}
					if (!flag)
					{
						i++;
						num = 0;
					}
					else
					{
						num++;
						if (num + i >= this.m_EntryNum)
						{
							i++;
							num = 0;
						}
					}
				}
				else
				{
					num = 0;
					i++;
				}
			}
		}

		// Token: 0x06001C46 RID: 7238 RVA: 0x000975D8 File Offset: 0x000959D8
		public void BuildSortingOrder(int flayergroup, int forder)
		{
			this.ListSort();
			float num = (float)(4000 * flayergroup / 10);
			for (int i = this.m_EntryNum - 1; i >= 0; i--)
			{
				forder += 10;
				RoomBlockData block = this.m_List[i].m_Block;
				if (!block.IsLinkObject() && block.RoomObject != null)
				{
					block.RoomObject.SetSortingOrder(num, forder);
					block.RoomObject.LinkObjectParam(forder, num);
				}
				num -= 3f;
			}
			num = (float)(4000 * flayergroup / 10);
			num += 3f;
			this.SetSortOrder(num);
		}

		// Token: 0x06001C47 RID: 7239 RVA: 0x00097682 File Offset: 0x00095A82
		public IRoomObjectControll GetObject(int findex)
		{
			return this.m_List[findex].m_Object;
		}

		// Token: 0x06001C48 RID: 7240 RVA: 0x00097694 File Offset: 0x00095A94
		public int GetFreeNum()
		{
			int num = 0;
			for (int i = 0; i < this.m_EntryNum; i++)
			{
				if (this.m_List[i].m_Object.IsAction())
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06001C49 RID: 7241 RVA: 0x000976D8 File Offset: 0x00095AD8
		public IRoomObjectControll GetFreeObject(RoomObjectCtrlChara pchara, int fkeyindex = -1)
		{
			if (fkeyindex < 0)
			{
				for (int i = 0; i < this.m_EntryNum; i++)
				{
					if (this.m_List[i].m_Object.IsAction())
					{
						return this.m_List[i].m_Object;
					}
				}
			}
			else if (fkeyindex < this.m_EntryNum && this.m_List[fkeyindex].m_Object.IsAction())
			{
				return this.m_List[fkeyindex].m_Object;
			}
			return null;
		}

		// Token: 0x0400231A RID: 8986
		private const int FLOOR_WALL_BLOCK_X = 0;

		// Token: 0x0400231B RID: 8987
		private const int FLOOR_WALL_BLOCK_Y = 0;

		// Token: 0x0400231C RID: 8988
		public RoomFloorCateory.eCategory m_Type;

		// Token: 0x0400231D RID: 8989
		public float m_PosX;

		// Token: 0x0400231E RID: 8990
		public float m_PosY;

		// Token: 0x0400231F RID: 8991
		public int m_SizeX;

		// Token: 0x04002320 RID: 8992
		public int m_SizeY;

		// Token: 0x04002321 RID: 8993
		public int m_SizeZ;

		// Token: 0x04002322 RID: 8994
		public Vector2 m_RoomOffset;

		// Token: 0x04002323 RID: 8995
		public Vector2 m_WallOffset;

		// Token: 0x04002324 RID: 8996
		private GameObject m_Root;

		// Token: 0x04002325 RID: 8997
		private Vector2 m_BuildPosOffset;

		// Token: 0x04002326 RID: 8998
		private RoomObjectMdlSprite m_Floor;

		// Token: 0x04002327 RID: 8999
		private RoomObjectMdlSprite m_Wall;

		// Token: 0x04002328 RID: 9000
		private Material m_RenderMaterial;

		// Token: 0x04002329 RID: 9001
		private GameObject m_HitTileGroup;

		// Token: 0x0400232A RID: 9002
		private GameObject m_HitWallGroup;

		// Token: 0x0400232B RID: 9003
		private Vector3[] m_HitTileLine;

		// Token: 0x0400232C RID: 9004
		private Vector2[] m_HitTileGrid;

		// Token: 0x0400232D RID: 9005
		private Vector3[] m_HitWallLine;

		// Token: 0x0400232E RID: 9006
		private Vector2[] m_HitWallGrid;

		// Token: 0x0400232F RID: 9007
		private List<GameObject> m_TilingFloors = new List<GameObject>();

		// Token: 0x04002330 RID: 9008
		private Color m_BasePer;

		// Token: 0x04002331 RID: 9009
		public IRoomFloorManager.RoomSortObject[] m_List;

		// Token: 0x04002332 RID: 9010
		public int m_MaxList;

		// Token: 0x04002333 RID: 9011
		public int m_EntryNum;

		// Token: 0x020005B2 RID: 1458
		public class RoomSortObject
		{
			// Token: 0x06001C4A RID: 7242 RVA: 0x00097760 File Offset: 0x00095B60
			public RoomSortObject(IRoomObjectControll pobj)
			{
				this.m_Object = pobj;
				this.m_NameVert = default(RoomIso.IsoNamedVerts);
				this.m_Vert = default(RoomIso.IsoVerts);
				this.m_Block = pobj.BlockData;
				this.m_Box = new RoomIso.IsoBounds();
				this.UpdateIsoBounds();
			}

			// Token: 0x06001C4B RID: 7243 RVA: 0x000977B8 File Offset: 0x00095BB8
			public void UpdateIsoBounds()
			{
				float key = 10000f;
				if (!this.m_Block.IsLinkObject())
				{
					key = 0.70710677f * (float)this.m_Block.PosX + 0.70710677f * (float)this.m_Block.PosY;
				}
				this.m_Key = key;
				this.m_NameVert.Make(this.m_Block.PosW, this.m_Block.SizeW);
				RoomIso.SpaceToIso(ref this.m_Vert.leftDown, ref this.m_NameVert.leftDown);
				RoomIso.SpaceToIso(ref this.m_Vert.rightDown, ref this.m_NameVert.rightDown);
				RoomIso.SpaceToIso(ref this.m_Vert.backDown, ref this.m_NameVert.backDown);
				RoomIso.SpaceToIso(ref this.m_Vert.frontDown, ref this.m_NameVert.frontDown);
				RoomIso.SpaceToIso(ref this.m_Vert.leftUp, ref this.m_NameVert.leftUp);
				RoomIso.SpaceToIso(ref this.m_Vert.rightUp, ref this.m_NameVert.rightUp);
				RoomIso.SpaceToIso(ref this.m_Vert.backUp, ref this.m_NameVert.backUp);
				RoomIso.SpaceToIso(ref this.m_Vert.frontUp, ref this.m_NameVert.frontUp);
				this.m_Box.Set(this.m_Vert.frontDown.x, this.m_Vert.backUp.x, this.m_Vert.frontDown.y, this.m_Vert.backUp.y, this.m_Vert.leftDown.h, this.m_Vert.rightDown.h);
			}

			// Token: 0x04002334 RID: 9012
			public RoomBlockData m_Block;

			// Token: 0x04002335 RID: 9013
			public RoomIso.IsoBounds m_Box;

			// Token: 0x04002336 RID: 9014
			public RoomIso.IsoNamedVerts m_NameVert;

			// Token: 0x04002337 RID: 9015
			public RoomIso.IsoVerts m_Vert;

			// Token: 0x04002338 RID: 9016
			public float m_Key;

			// Token: 0x04002339 RID: 9017
			public int m_CompKey;

			// Token: 0x0400233A RID: 9018
			public IRoomObjectControll m_Object;
		}
	}
}
