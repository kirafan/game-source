﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200034F RID: 847
	public class IFldNetComManager : MonoBehaviour
	{
		// Token: 0x06001021 RID: 4129 RVA: 0x00055D33 File Offset: 0x00054133
		public static void SetDebugMode(bool fset)
		{
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x00055D35 File Offset: 0x00054135
		protected void AwakeCom()
		{
			IFldNetComManager.Instance = this;
			this.m_ComTable = new INetComHandle[10];
			this.m_ComMax = 10;
			this.m_ModuleTable = new IFldNetComModule[10];
			this.m_ModMax = 10;
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x00055D67 File Offset: 0x00054167
		protected void DestroyCom()
		{
			IFldNetComManager.Instance = null;
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x00055D70 File Offset: 0x00054170
		protected bool UpCom()
		{
			if (this.m_ComNum != 0 && this.m_ComTable[0].IsComEnd())
			{
				this.m_ComTable[0] = null;
				for (int i = 1; i < this.m_ComNum; i++)
				{
					this.m_ComTable[i - 1] = this.m_ComTable[i];
				}
				this.m_ComNum--;
				this.m_ComTable[this.m_ComNum] = null;
				if (this.m_ComNum != 0)
				{
					this.m_ComTable[0].CreateComParam();
				}
				else if (this.m_OpenUI)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
					this.m_OpenUI = false;
				}
			}
			return this.m_ComNum != 0;
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x00055E34 File Offset: 0x00054234
		protected bool UpModule()
		{
			bool flag = false;
			for (int i = 0; i < this.m_ModNum; i++)
			{
				if (!this.m_ModuleTable[i].UpFunc())
				{
					this.m_ModuleTable[i] = null;
					flag = true;
				}
			}
			if (flag)
			{
				int i = 0;
				while (i < this.m_ModNum)
				{
					if (this.m_ModuleTable[i] == null)
					{
						for (int j = i + 1; j < this.m_ModNum; j++)
						{
							this.m_ModuleTable[j - 1] = this.m_ModuleTable[j];
						}
						this.m_ModNum--;
						this.m_ModuleTable[this.m_ModNum] = null;
					}
					else
					{
						i++;
					}
				}
			}
			return this.m_ModNum != 0;
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x00055EFC File Offset: 0x000542FC
		public void SendCom(INetComHandle phandle, INetComHandle.ResponseCallbak pcallback, bool fopenui = false)
		{
			if (this.m_ComNum >= this.m_ComMax)
			{
				INetComHandle[] array = new INetComHandle[this.m_ComMax + 5];
				for (int i = 0; i < this.m_ComMax; i++)
				{
					array[i] = this.m_ComTable[i];
				}
				this.m_ComTable = null;
				this.m_ComTable = array;
				this.m_ComMax += 5;
			}
			phandle.SetCallback(pcallback);
			this.m_ComTable[this.m_ComNum] = phandle;
			if (this.m_ComNum == 0)
			{
				phandle.CreateComParam();
				if (fopenui)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
					this.m_OpenUI = true;
				}
			}
			this.m_ComNum++;
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x00055FB8 File Offset: 0x000543B8
		public void AddModule(IFldNetComModule pmodule)
		{
			if (this.m_ModNum >= this.m_ModMax)
			{
				IFldNetComModule[] array = new IFldNetComModule[this.m_ModMax + 5];
				for (int i = 0; i < this.m_ModMax; i++)
				{
					array[i] = this.m_ModuleTable[i];
				}
				this.m_ModuleTable = null;
				this.m_ModuleTable = array;
				this.m_ModMax += 5;
			}
			this.m_ModuleTable[this.m_ModNum] = pmodule;
			this.m_ModNum++;
		}

		// Token: 0x04001732 RID: 5938
		private INetComHandle[] m_ComTable;

		// Token: 0x04001733 RID: 5939
		protected int m_ComNum;

		// Token: 0x04001734 RID: 5940
		private int m_ComMax;

		// Token: 0x04001735 RID: 5941
		private IFldNetComModule[] m_ModuleTable;

		// Token: 0x04001736 RID: 5942
		protected int m_ModNum;

		// Token: 0x04001737 RID: 5943
		private int m_ModMax;

		// Token: 0x04001738 RID: 5944
		private bool m_OpenUI;

		// Token: 0x04001739 RID: 5945
		public static IFldNetComManager Instance;
	}
}
