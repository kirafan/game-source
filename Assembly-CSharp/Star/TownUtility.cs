﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000718 RID: 1816
	public static class TownUtility
	{
		// Token: 0x060023EC RID: 9196 RVA: 0x000C0DF3 File Offset: 0x000BF1F3
		public static string GetResourceNoName(int objID, int flevel, int ftimes = 0)
		{
			return string.Format("{0:0000}{1:00}", objID, flevel - 1);
		}

		// Token: 0x060023ED RID: 9197 RVA: 0x000C0E10 File Offset: 0x000BF210
		public static string GetModelResourcePath(TownUtility.eResCategory ftype, int objID, int ftime)
		{
			switch (ftype)
			{
			case TownUtility.eResCategory.Field:
				return string.Format("Prefab/Town/Field/BG_town_{0:00}_{1}", objID, ftime);
			case TownUtility.eResCategory.Room:
			case TownUtility.eResCategory.Buf:
			case TownUtility.eResCategory.MenuBuf:
			case TownUtility.eResCategory.Content:
				return string.Format("Prefab/Town/Building/BLD_{0:000000}_{1}", objID, ftime);
			case TownUtility.eResCategory.Area:
				return string.Format("Prefab/Town/Area/AREA_{0:000000}_{1}", objID, 0);
			case TownUtility.eResCategory.AreaFree:
				return string.Format("Prefab/Town/Area/AREA_{0:000000}_{1}", objID, 0);
			}
			return string.Format("Prefab/Town/Option/AreaSelectPoint", new object[0]);
		}

		// Token: 0x060023EE RID: 9198 RVA: 0x000C0ED0 File Offset: 0x000BF2D0
		public static bool IsModelResourcePath(TownUtility.eResCategory ftype, int objID)
		{
			bool result = false;
			switch (ftype)
			{
			case TownUtility.eResCategory.Field:
				return true;
			case TownUtility.eResCategory.Room:
			case TownUtility.eResCategory.Buf:
			case TownUtility.eResCategory.MenuBuf:
			case TownUtility.eResCategory.Content:
				return true;
			case TownUtility.eResCategory.Area:
				return result;
			case TownUtility.eResCategory.AreaFree:
				return false;
			}
			result = false;
			return result;
		}

		// Token: 0x060023EF RID: 9199 RVA: 0x000C0F2C File Offset: 0x000BF32C
		public static int GetTownObjectLevelUpConfirm(int objID, int targetLv, TownDefine.eTownLevelUpConditionCategory category)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			TownObjectLevelUpDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, targetLv);
			int num = 0;
			switch (category)
			{
			case TownDefine.eTownLevelUpConditionCategory.Gold:
				num = param2.m_GoldAmount;
				num += TownUtility.CalcUsePlayerResource(category, param2.m_TownUseFunc, param2.m_TownActionStepKey);
				break;
			case TownDefine.eTownLevelUpConditionCategory.Kirara:
				num = param2.m_KiraraPoint;
				num += TownUtility.CalcUsePlayerResource(category, param2.m_TownUseFunc, param2.m_TownActionStepKey);
				break;
			case TownDefine.eTownLevelUpConditionCategory.UserLv:
				num = param2.m_UserLv;
				break;
			case TownDefine.eTownLevelUpConditionCategory.BuildTime:
				num = param2.m_BuildTime;
				num += TownUtility.CalcUsePlayerResource(category, param2.m_TownUseFunc, param2.m_TownActionStepKey);
				break;
			}
			return num;
		}

		// Token: 0x060023F0 RID: 9200 RVA: 0x000C1004 File Offset: 0x000BF404
		private static int CalcUsePlayerResource(TownDefine.eTownLevelUpConditionCategory category, int funckey, int fsubkey)
		{
			if (funckey != 0 && funckey == 1)
			{
				int num = 0;
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int num2 = userTownData.GetBuildObjectDataNum();
				for (int i = 0; i < num2; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 0)
					{
						num++;
					}
				}
				TownObjectBuildSubCodeDB townObjBuildSubDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjBuildSubDB;
				num2 = townObjBuildSubDB.m_Params.Length;
				for (int i = 0; i < num2; i++)
				{
					if (townObjBuildSubDB.m_Params[i].m_ID == num + fsubkey)
					{
						if (category != TownDefine.eTownLevelUpConditionCategory.Gold)
						{
							if (category != TownDefine.eTownLevelUpConditionCategory.Kirara)
							{
							}
						}
						else if (townObjBuildSubDB.m_Params[i].m_ExtentionScriptID == 0)
						{
							return townObjBuildSubDB.m_Params[i].m_ExtentionFunKey;
						}
					}
				}
			}
			return 0;
		}

		// Token: 0x060023F1 RID: 9201 RVA: 0x000C1104 File Offset: 0x000BF504
		public static int GetBuildObjNum(int townObjID)
		{
			int num = 0;
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				if (buildObjectDataAt.m_ObjID == townObjID)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x060023F2 RID: 9202 RVA: 0x000C1157 File Offset: 0x000BF557
		public static int SyncFixKey(int fbaseid)
		{
			return fbaseid | 1073741824;
		}

		// Token: 0x060023F3 RID: 9203 RVA: 0x000C1160 File Offset: 0x000BF560
		public static int MakeBuildAreaID(int fbaseid, int fsubid = 0)
		{
			return (fbaseid & 4095) | fsubid << 16 | 0;
		}

		// Token: 0x060023F4 RID: 9204 RVA: 0x000C1170 File Offset: 0x000BF570
		public static int GetBuildAreaID(int fbuildpoint)
		{
			return fbuildpoint & 4095;
		}

		// Token: 0x060023F5 RID: 9205 RVA: 0x000C1179 File Offset: 0x000BF579
		public static int ChangeBuildAreaID(int fbuildpoint, int fareaid)
		{
			return (int)((long)fbuildpoint & (long)((ulong)-4096)) | fareaid;
		}

		// Token: 0x060023F6 RID: 9206 RVA: 0x000C1187 File Offset: 0x000BF587
		public static int MakeBuildPointID(int fbaseid, int fsubid)
		{
			return (fbaseid & 4095) | fsubid << 16 | 1073741824;
		}

		// Token: 0x060023F7 RID: 9207 RVA: 0x000C119B File Offset: 0x000BF59B
		public static int MakeBuildContentID(int fbaseid)
		{
			return (fbaseid & 4095) | 536870912;
		}

		// Token: 0x060023F8 RID: 9208 RVA: 0x000C11AA File Offset: 0x000BF5AA
		public static int MakeMenuPointID(int fbaseid)
		{
			return fbaseid | 268435456;
		}

		// Token: 0x060023F9 RID: 9209 RVA: 0x000C11B3 File Offset: 0x000BF5B3
		public static eTownMenuType GetMenuPointEnum(int fbaseid)
		{
			return (eTownMenuType)(fbaseid & 4095);
		}

		// Token: 0x060023FA RID: 9210 RVA: 0x000C11BC File Offset: 0x000BF5BC
		public static int GetPointToBuildType(int fpoint)
		{
			return fpoint & 1879048192;
		}

		// Token: 0x060023FB RID: 9211 RVA: 0x000C11C8 File Offset: 0x000BF5C8
		public static eTownObjectCategory GetResourceCategory(int fid)
		{
			return (eTownObjectCategory)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fid).m_Category;
		}

		// Token: 0x060023FC RID: 9212 RVA: 0x000C11F4 File Offset: 0x000BF5F4
		public static Transform FindTransform(Transform ptarget, string findname, int fchkcall)
		{
			if (ptarget.name == findname)
			{
				return ptarget;
			}
			if (fchkcall <= 0)
			{
				return null;
			}
			fchkcall--;
			int childCount = ptarget.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform transform = TownUtility.FindTransform(ptarget.GetChild(i), findname, fchkcall);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x060023FD RID: 9213 RVA: 0x000C125C File Offset: 0x000BF65C
		public static int CalcAreaToContentID(int objID)
		{
			TownObjectListDB townObjListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB;
			TownObjectListDB_Param param = townObjListDB.GetParam(objID);
			int id = param.m_ID;
			int num = townObjListDB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (townObjListDB.m_Params[i].m_TitleType == param.m_TitleType && townObjListDB.m_Params[i].m_Category == 6)
				{
					id = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.m_Params[i].m_ID;
					break;
				}
			}
			return id;
		}

		// Token: 0x060023FE RID: 9214 RVA: 0x000C1300 File Offset: 0x000BF700
		public static int GetObjectModelLinkLevel(UserTownData.BuildObjectData pbuildobj, int fobjid)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid);
			int result = 1;
			if ((param.m_Arg & 1) == 0 && pbuildobj.m_Lv != 0)
			{
				result = pbuildobj.m_Lv + 1;
			}
			return result;
		}

		// Token: 0x060023FF RID: 9215 RVA: 0x000C1348 File Offset: 0x000BF748
		public static void SetRenderLayer(GameObject pobj, int flayer)
		{
			Renderer[] componentsInChildren = pobj.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].sortingOrder = flayer;
			}
		}

		// Token: 0x06002400 RID: 9216 RVA: 0x000C137C File Offset: 0x000BF77C
		public static eSoundBgmListDB GetTownBgmID()
		{
			eSoundBgmListDB cueID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundHomeBgmListDB.GetCueID(SingletonMonoBehaviour<GameSystem>.Inst.ServerTime);
			if (cueID != eSoundBgmListDB.None)
			{
				return cueID;
			}
			ScheduleTimeUtil.SetMarkingTime();
			int hour = ScheduleTimeUtil.GetManageUnixTime().Hour;
			if (hour >= 18 || hour < 6)
			{
				return eSoundBgmListDB.BGM_TOWN_3;
			}
			if (hour >= 6 && hour < 13)
			{
				return eSoundBgmListDB.BGM_TOWN_1;
			}
			return eSoundBgmListDB.BGM_TOWN_2;
		}

		// Token: 0x02000719 RID: 1817
		public enum eResCategory
		{
			// Token: 0x04002AE1 RID: 10977
			Field,
			// Token: 0x04002AE2 RID: 10978
			Room,
			// Token: 0x04002AE3 RID: 10979
			Area,
			// Token: 0x04002AE4 RID: 10980
			Buf,
			// Token: 0x04002AE5 RID: 10981
			AreaFree,
			// Token: 0x04002AE6 RID: 10982
			BufFree,
			// Token: 0x04002AE7 RID: 10983
			MenuBuf,
			// Token: 0x04002AE8 RID: 10984
			Content,
			// Token: 0x04002AE9 RID: 10985
			Chara,
			// Token: 0x04002AEA RID: 10986
			MenuChara
		}
	}
}
