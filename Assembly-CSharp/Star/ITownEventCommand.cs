﻿using System;

namespace Star
{
	// Token: 0x0200071B RID: 1819
	public class ITownEventCommand
	{
		// Token: 0x06002402 RID: 9218 RVA: 0x000B50B1 File Offset: 0x000B34B1
		public virtual bool CalcRequest(TownBuilder pbuild)
		{
			return true;
		}

		// Token: 0x04002AF1 RID: 10993
		public eTownRequest m_Type;

		// Token: 0x04002AF2 RID: 10994
		public bool m_Enable;
	}
}
