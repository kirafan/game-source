﻿using System;

namespace Star
{
	// Token: 0x02000303 RID: 771
	[Serializable]
	public struct DefaultObjectListDB_Param
	{
		// Token: 0x040015F3 RID: 5619
		public int m_Category;

		// Token: 0x040015F4 RID: 5620
		public int m_Type;

		// Token: 0x040015F5 RID: 5621
		public int m_ObjectNo;

		// Token: 0x040015F6 RID: 5622
		public int m_Num;

		// Token: 0x040015F7 RID: 5623
		public int[] m_ObjEventArgs;

		// Token: 0x040015F8 RID: 5624
		public int m_WakeUp;
	}
}
