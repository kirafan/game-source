﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using STORERequestTypes;
using STOREResponseTypes;
using UnityEngine;
using UnityEngine.Purchasing;
using WWWTypes;

namespace Star
{
	// Token: 0x020004D0 RID: 1232
	public sealed class StoreManager
	{
		// Token: 0x17000184 RID: 388
		// (get) Token: 0x06001831 RID: 6193 RVA: 0x0007DD52 File Offset: 0x0007C152
		private InAppPurchaser IAP
		{
			get
			{
				if (this.m_InAppPurchaser == null)
				{
					this.m_InAppPurchaser = SingletonMonoBehaviour<InAppPurchaser>.Inst;
				}
				return this.m_InAppPurchaser;
			}
		}

		// Token: 0x06001832 RID: 6194 RVA: 0x0007DD76 File Offset: 0x0007C176
		public long GetMonthlyAmount()
		{
			return this.m_MonthlyAmount;
		}

		// Token: 0x06001833 RID: 6195 RVA: 0x0007DD7E File Offset: 0x0007C17E
		public long GetAvailableBalance()
		{
			return this.m_AvailableBalance;
		}

		// Token: 0x06001834 RID: 6196 RVA: 0x0007DD88 File Offset: 0x0007C188
		public int GetMonthlyPurchaseAmount()
		{
			eAgeType age = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Age;
			if (age == eAgeType.Age15)
			{
				return (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.MonthlyPurchaseAmount_Under15);
			}
			if (age != eAgeType.Age16_19)
			{
				return -1;
			}
			return (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.MonthlyPurchaseAmount_From16To19);
		}

		// Token: 0x06001835 RID: 6197 RVA: 0x0007DDEE File Offset: 0x0007C1EE
		public List<StoreManager.Product> GetProducts()
		{
			return this.m_Products;
		}

		// Token: 0x06001836 RID: 6198 RVA: 0x0007DDF8 File Offset: 0x0007C1F8
		public StoreManager.Product GetProduct(int id)
		{
			if (this.m_Products != null)
			{
				for (int i = 0; i < this.m_Products.Count; i++)
				{
					if (this.m_Products[i].m_ID == id)
					{
						return this.m_Products[i];
					}
				}
			}
			return null;
		}

		// Token: 0x06001837 RID: 6199 RVA: 0x0007DE51 File Offset: 0x0007C251
		public List<StoreManager.PurchaseLog> GetPurchaseLogs()
		{
			return this.m_PurchaseLogs;
		}

		// Token: 0x06001838 RID: 6200 RVA: 0x0007DE59 File Offset: 0x0007C259
		public bool IsAvailableProduct(StoreManager.Product product)
		{
			return this.m_AvailableBalance == -1L || this.m_AvailableBalance >= product.m_Price;
		}

		// Token: 0x06001839 RID: 6201 RVA: 0x0007DE7E File Offset: 0x0007C27E
		public void ForceResetOnReturnTitle()
		{
			this.m_Step = StoreManager.eStep.None;
		}

		// Token: 0x0600183A RID: 6202 RVA: 0x0007DE88 File Offset: 0x0007C288
		public void Update()
		{
			switch (this.m_Step)
			{
			case StoreManager.eStep.InitializeProducts_Wait:
				if (this.IAP.InitializeResult != InAppPurchaser.eInitializeResult.NOTYET)
				{
					StoreManager.LOGF("InitializeProducts_Wait >>> IAP.InitializeResult:{0}", new object[]
					{
						this.IAP.InitializeResult
					});
					InAppPurchaser.eInitializeResult initializeResult = this.IAP.InitializeResult;
					if (initializeResult != InAppPurchaser.eInitializeResult.SUCCESS)
					{
						if (initializeResult == InAppPurchaser.eInitializeResult.FAILURE || initializeResult == InAppPurchaser.eInitializeResult.NO_CONNECTION)
						{
							this.OnInitializeProducts(true);
						}
					}
					else if (this.IAP.IsNeedRestoreProduct())
					{
						this.m_Step = StoreManager.eStep.RestoreFirst;
					}
					else
					{
						this.OnInitializeProducts(false);
					}
				}
				break;
			case StoreManager.eStep.RestoreFirst:
				StoreManager.LOG("RestoreFirst");
				this.m_RestoreIndex = 0;
				this.m_Step = StoreManager.eStep.RestoreAPI;
				break;
			case StoreManager.eStep.RestoreAPI:
			{
				StoreManager.LOG("RestoreAPI");
				string restoreReceiptAndroid = this.IAP.GetRestoreReceiptAndroid(this.m_RestoreIndex);
				string restoreSignatureAndroid = this.IAP.GetRestoreSignatureAndroid(this.m_RestoreIndex);
				this.m_Step = StoreManager.eStep.RestoreAPI_Wait;
				this.Request_SendReceipt(restoreReceiptAndroid, restoreSignatureAndroid, new Action<ResultCode>(this.OnCompleteSendReceiptByRestore));
				break;
			}
			case StoreManager.eStep.RestoreFinish:
				StoreManager.LOGF("RestoreFinish >>> restoreIndex:{0}, restoreRecipeCount:{1}", new object[]
				{
					this.m_RestoreIndex,
					this.IAP.GetRestoreReceiptCount()
				});
				this.m_RestoreIndex++;
				if (this.IAP.GetRestoreReceiptCount() <= this.m_RestoreIndex)
				{
					this.IAP.ClearRestoreProduct();
					this.OnInitializeProducts(false);
				}
				else
				{
					this.m_Step = StoreManager.eStep.RestoreAPI;
				}
				break;
			case StoreManager.eStep.PurchaseIAP_Wait:
			{
				InAppPurchaser.PurchaseState purchaseState = this.IAP.GetPurchaseState();
				StoreManager.LOGF("Purchase_Wait >>> purchaseState:{0}", new object[]
				{
					purchaseState
				});
				if (purchaseState != InAppPurchaser.PurchaseState.Processing)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
					if (purchaseState == InAppPurchaser.PurchaseState.Success)
					{
						StoreManager.LOG("Purchase_Wait >>> SUCCESS");
						this.m_Step = StoreManager.eStep.PurchaseSendReceiptAPI_Wait;
						string receiptAndroid = this.IAP.GetReceiptAndroid();
						string signature = this.IAP.GetSignature();
						this.Request_SendReceipt(receiptAndroid, signature, new Action<ResultCode>(this.OnCompleteSendReceiptByPurchase));
					}
					else
					{
						PurchaseFailureReason errorReason = this.IAP.GetErrorReason();
						string errorTitle = this.IAP.GetErrorTitle();
						string errorMessage = this.IAP.GetErrorMessage();
						StoreManager.LOGF("Purchase_Wait >>> FAILED... reason:{0}", new object[]
						{
							errorReason
						});
						this.m_Step = StoreManager.eStep.None;
						APIUtility.ShowErrorWindowOk(errorTitle, errorMessage, null);
					}
				}
				break;
			}
			}
		}

		// Token: 0x0600183B RID: 6203 RVA: 0x0007E134 File Offset: 0x0007C534
		private void OnInitializeProducts(bool isError)
		{
			StoreManager.LOGF("OnInitializeProducts() >>> isError:{0}", new object[]
			{
				isError
			});
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			this.m_Step = StoreManager.eStep.None;
			if (isError)
			{
				this.m_Products.Clear();
			}
			this.m_OnCompleteInitializeProduct.Call();
		}

		// Token: 0x0600183C RID: 6204 RVA: 0x0007E18C File Offset: 0x0007C58C
		private void OnCompleteSendReceiptByRestore(ResultCode resultCode)
		{
			StoreManager.LOGF("OnCompleteSendReceiptByRestore() >>> resultCode:{0}, restoreIndex:{1}", new object[]
			{
				resultCode,
				this.m_RestoreIndex
			});
			switch (resultCode)
			{
			case ResultCode.APP_STORE_ERROR:
				break;
			case ResultCode.GOOGLE_PLAY_ERROR:
			case ResultCode.STORE_ALREADY_PURCHASED:
				this.IAP.RestoreProduct(this.m_RestoreIndex);
				break;
			case ResultCode.APP_STORE_INVALID_RECEIPT:
				break;
			case ResultCode.GOOGLE_PLAY_INVALID_STATUS:
				break;
			case ResultCode.STORE_ITEM_NOT_FOUND:
				break;
			default:
				if (resultCode == ResultCode.SUCCESS)
				{
					this.IAP.RestoreProduct(this.m_RestoreIndex);
				}
				break;
			}
			this.m_Step = StoreManager.eStep.RestoreFinish;
		}

		// Token: 0x0600183D RID: 6205 RVA: 0x0007E238 File Offset: 0x0007C638
		private void OnCompleteSendReceiptByPurchase(ResultCode resultCode)
		{
			StoreManager.LOGF("OnCompleteSendReceiptByPurchase() >>> resultCode:{0}", new object[]
			{
				resultCode
			});
			if (resultCode != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(resultCode.ToMessageString(), null);
			}
			else
			{
				this.IAP.ConfirmPurchase();
				this.m_OnCompletePurchase.Call(this.m_RequestedProduct);
			}
			this.m_Step = StoreManager.eStep.None;
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x0600183E RID: 6206 RVA: 0x0007E2A4 File Offset: 0x0007C6A4
		// (remove) Token: 0x0600183F RID: 6207 RVA: 0x0007E2DC File Offset: 0x0007C6DC
		private event Action m_OnCompleteSetAge;

		// Token: 0x06001840 RID: 6208 RVA: 0x0007E312 File Offset: 0x0007C712
		public bool IsNeedCheckAge()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Age == eAgeType.None;
		}

		// Token: 0x06001841 RID: 6209 RVA: 0x0007E330 File Offset: 0x0007C730
		public void Request_SetAge(eAgeType ageType, Action onComplete)
		{
			StoreManager.LOGF("Request_SetAge() >>> ageType:{0}", new object[]
			{
				ageType
			});
			this.m_OnCompleteSetAge = onComplete;
			this.m_RequestedAgeType = ageType;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			MeigewwwParam wwwParam = PlayerRequest.Setage(new PlayerRequestTypes.Setage
			{
				age = (int)ageType
			}, new MeigewwwParam.Callback(this.OnResponse_SetAge));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001842 RID: 6210 RVA: 0x0007E39C File Offset: 0x0007C79C
		private void OnResponse_SetAge(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			PlayerResponseTypes.Setage setage = PlayerResponse.Setage(wwwParam, ResponseCommon.DialogType.None, null);
			if (setage == null)
			{
				StoreManager.LOGF("OnResponse_SetAge() >>> error.", new object[0]);
				return;
			}
			ResultCode result = setage.GetResult();
			StoreManager.LOGF("OnResponse_SetAge() >>> resultCode:{0}", new object[]
			{
				result
			});
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Age = this.m_RequestedAgeType;
				this.m_RequestedAgeType = eAgeType.None;
				this.m_OnCompleteSetAge.Call();
			}
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06001843 RID: 6211 RVA: 0x0007E448 File Offset: 0x0007C848
		// (remove) Token: 0x06001844 RID: 6212 RVA: 0x0007E480 File Offset: 0x0007C880
		private event Action m_OnCompleteInitializeProduct;

		// Token: 0x06001845 RID: 6213 RVA: 0x0007E4B8 File Offset: 0x0007C8B8
		public void Request_InitializeProducts(Action onComplete)
		{
			StoreManager.LOG("Request_InitializeProducts()");
			this.m_OnCompleteInitializeProduct = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			STORERequestTypes.GetAll getAll = new STORERequestTypes.GetAll();
			getAll.platform = Platform.Get();
			getAll.env = 1;
			StoreManager.LOGF("GetAll platform:{0}, env:{1}", new object[]
			{
				getAll.platform,
				getAll.env
			});
			MeigewwwParam all = STORERequest.GetAll(getAll, new MeigewwwParam.Callback(this.OnResponse_StoreGetAll));
			NetworkQueueManager.Request(all);
		}

		// Token: 0x06001846 RID: 6214 RVA: 0x0007E544 File Offset: 0x0007C944
		private void OnResponse_StoreGetAll(MeigewwwParam wwwParam)
		{
			STOREResponseTypes.GetAll all = STOREResponse.GetAll(wwwParam, ResponseCommon.DialogType.None, null);
			if (all == null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				StoreManager.LOGF("OnResponse_StoreGetAll() >>> error.", new object[0]);
				return;
			}
			ResultCode result = all.GetResult();
			StoreManager.LOGF("OnResponse_StoreGetAll() >>> resultCode:{0}", new object[]
			{
				result
			});
			if (result != ResultCode.SUCCESS)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.m_MonthlyAmount = all.monthlyAmount;
				this.m_AvailableBalance = all.availableBalance;
				List<string> list = new List<string>();
				this.m_Products.Clear();
				for (int i = 0; i < all.products.Length; i++)
				{
					StoreManager.Product product = new StoreManager.Product();
					StoreManager.wwwConvert(all.products[i], product);
					this.m_Products.Add(product);
					list.Add(product.m_ProductID);
				}
				List<StoreManager.Product> products = this.m_Products;
				if (StoreManager.<>f__mg$cache0 == null)
				{
					StoreManager.<>f__mg$cache0 = new Comparison<StoreManager.Product>(StoreManager.CompareSortProduct);
				}
				Sort.StableSort<StoreManager.Product>(products, StoreManager.<>f__mg$cache0);
				StoreManager.LOGF("monthlyAmount:{0}, availableBalance:{1}, productIDs.Count:{2}", new object[]
				{
					this.m_MonthlyAmount,
					this.m_AvailableBalance,
					list.Count
				});
				this.m_Step = StoreManager.eStep.InitializeProducts_Wait;
				this.IAP.AddProducts(list);
			}
		}

		// Token: 0x06001847 RID: 6215 RVA: 0x0007E6B8 File Offset: 0x0007CAB8
		private static void wwwConvert(StoreProduct src, StoreManager.Product dest)
		{
			dest.m_ID = src.id;
			dest.m_ProductID = src.sku;
			dest.m_Name = src.name;
			dest.m_Desc = src.description;
			dest.m_Price = src.price;
			dest.m_UnlimitedGem = (long)src.amount1;
			dest.m_LimitedGem = (long)src.amount2;
			dest.m_Limit = src.limitNum;
			dest.m_LinkID = src.linkId;
			dest.m_uiPriority = src.uiPriority;
			dest.m_uiType = (StoreDefine.eUIType)src.uiType;
		}

		// Token: 0x06001848 RID: 6216 RVA: 0x0007E74B File Offset: 0x0007CB4B
		private static int CompareSortProduct(StoreManager.Product A, StoreManager.Product B)
		{
			if (A.m_uiPriority > B.m_uiPriority)
			{
				return 1;
			}
			if (A.m_uiPriority < B.m_uiPriority)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x06001849 RID: 6217 RVA: 0x0007E774 File Offset: 0x0007CB74
		// (remove) Token: 0x0600184A RID: 6218 RVA: 0x0007E7AC File Offset: 0x0007CBAC
		private event Action<StoreManager.Product> m_OnCompletePurchase;

		// Token: 0x0600184B RID: 6219 RVA: 0x0007E7E4 File Offset: 0x0007CBE4
		public bool Purchase(int id, Action<StoreManager.Product> onComplete)
		{
			this.m_OnCompletePurchase = onComplete;
			this.m_RequestedProduct = null;
			for (int i = 0; i < this.m_Products.Count; i++)
			{
				if (this.m_Products[i].m_ID == id)
				{
					this.m_RequestedProduct = this.m_Products[i];
					break;
				}
			}
			if (this.m_RequestedProduct == null)
			{
				return false;
			}
			StoreManager.LOGF("Purchase() >>> m_ProductID:{0}", new object[]
			{
				this.m_RequestedProduct.m_ProductID
			});
			this.Request_GetPayload(id, new Action<string>(this.OnCompleteGetPayload));
			return true;
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x0007E888 File Offset: 0x0007CC88
		private void OnCompleteGetPayload(string payload)
		{
			StoreManager.LOGF("OnCompleteGetPayload() >>> payload:{0}", new object[]
			{
				payload
			});
			if (!string.IsNullOrEmpty(payload))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				this.IAP.Purchase(this.m_RequestedProduct.m_ProductID, payload);
				this.m_Step = StoreManager.eStep.PurchaseIAP_Wait;
			}
			else
			{
				APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPGetPayloadFailed_Title), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPGetPayloadFailed), null);
			}
		}

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x0600184D RID: 6221 RVA: 0x0007E914 File Offset: 0x0007CD14
		// (remove) Token: 0x0600184E RID: 6222 RVA: 0x0007E94C File Offset: 0x0007CD4C
		private event Action<string> m_OnCompleteGetPayload;

		// Token: 0x0600184F RID: 6223 RVA: 0x0007E984 File Offset: 0x0007CD84
		private void Request_GetPayload(int id, Action<string> onComplete)
		{
			StoreManager.LOGF("Request_GetPayload() id:{0}", new object[]
			{
				id
			});
			this.m_OnCompleteGetPayload = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			MeigewwwParam googlePayload = STORERequest.GetGooglePayload(new STORERequestTypes.GetGooglePayload
			{
				id = id
			}, new MeigewwwParam.Callback(this.OnResponse_GetPayload));
			NetworkQueueManager.Request(googlePayload);
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x0007E9E8 File Offset: 0x0007CDE8
		private void OnResponse_GetPayload(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			STOREResponseTypes.GetGooglePayload googlePayload = STOREResponse.GetGooglePayload(wwwParam, ResponseCommon.DialogType.None, null);
			if (googlePayload == null)
			{
				StoreManager.LOG("OnResponse_GetPayload() >>> error.");
				return;
			}
			ResultCode result = googlePayload.GetResult();
			StoreManager.LOGF("OnResponse_GetPayload() >>> resultCode:{0}", new object[]
			{
				result
			});
			if (result != ResultCode.SUCCESS && result != ResultCode.PURCHASE_GEM_OVERFLOW)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnCloseErrorWindow_OnResponse_GetPayload));
			}
			else
			{
				StoreManager.LOGF("OnResponse_GetPayload() >>> payload:{0}", new object[]
				{
					googlePayload.payload
				});
				this.m_MonthlyAmount = googlePayload.monthlyAmount;
				this.m_AvailableBalance = googlePayload.availableBalance;
				if (result == ResultCode.SUCCESS)
				{
					this.m_OnCompleteGetPayload.Call(googlePayload.payload);
				}
				else
				{
					string text = UIUtility.MoneyValueToString(this.m_MonthlyAmount);
					string text2 = UIUtility.MoneyValueToString(this.GetMonthlyPurchaseAmount());
					string textMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPLimit, new object[]
					{
						result.ToMessageString(),
						text,
						text2
					});
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.IAPLimitTitle), textMessage, new Action(this.OnCloseErrorWindow_OnResponse_GetPayload));
				}
			}
		}

		// Token: 0x06001851 RID: 6225 RVA: 0x0007EB2E File Offset: 0x0007CF2E
		private void OnCloseErrorWindow_OnResponse_GetPayload()
		{
			this.m_OnCompleteGetPayload.Call(null);
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x06001852 RID: 6226 RVA: 0x0007EB3C File Offset: 0x0007CF3C
		// (remove) Token: 0x06001853 RID: 6227 RVA: 0x0007EB74 File Offset: 0x0007CF74
		private event Action<ResultCode> m_OnCompleteSendReceipt;

		// Token: 0x06001854 RID: 6228 RVA: 0x0007EBAC File Offset: 0x0007CFAC
		private void Request_SendReceipt(string receipt, string signature, Action<ResultCode> onComplete)
		{
			StoreManager.LOGF("Request_SendReceipt() >>> receipt:{0}, signature:{1}", new object[]
			{
				receipt,
				signature
			});
			this.m_OnCompleteSendReceipt = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			MeigewwwParam wwwParam = STORERequest.PurchaseGooglePlay(new STORERequestTypes.PurchaseGooglePlay
			{
				receipt = receipt,
				signature = signature
			}, new MeigewwwParam.Callback(this.OnResponse_SendReceipt));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001855 RID: 6229 RVA: 0x0007EC14 File Offset: 0x0007D014
		private void OnResponse_SendReceipt(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			STOREResponseTypes.PurchaseGooglePlay purchaseGooglePlay = STOREResponse.PurchaseGooglePlay(wwwParam, ResponseCommon.DialogType.None, null);
			if (purchaseGooglePlay == null)
			{
				StoreManager.LOG("OnResponse_SendReceipt() >>> error.");
				return;
			}
			ResultCode result = purchaseGooglePlay.GetResult();
			StoreManager.LOGF("OnResponse_SendReceipt() >>> resultCode:{0}", new object[]
			{
				result
			});
			if (purchaseGooglePlay.player != null)
			{
				APIUtility.wwwToUserData(purchaseGooglePlay.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
			}
			if (result != ResultCode.SUCCESS)
			{
				this.m_OnCompleteSendReceipt.Call(result);
			}
			else
			{
				if (AppsFlyersHandler.Instance != null && purchaseGooglePlay.purchased != null)
				{
					for (int i = 0; i < purchaseGooglePlay.purchased.Length; i++)
					{
						AppsFlyersHandler.Instance.TrackEvent_Purchase(purchaseGooglePlay.purchased[i].price, purchaseGooglePlay.purchased[i].sku, 0, "JPY");
					}
				}
				this.m_Products.Clear();
				for (int j = 0; j < purchaseGooglePlay.products.Length; j++)
				{
					StoreManager.Product product = new StoreManager.Product();
					StoreManager.wwwConvert(purchaseGooglePlay.products[j], product);
					this.m_Products.Add(product);
				}
				List<StoreManager.Product> products = this.m_Products;
				if (StoreManager.<>f__mg$cache1 == null)
				{
					StoreManager.<>f__mg$cache1 = new Comparison<StoreManager.Product>(StoreManager.CompareSortProduct);
				}
				Sort.StableSort<StoreManager.Product>(products, StoreManager.<>f__mg$cache1);
				this.m_OnCompleteSendReceipt.Call(result);
			}
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x06001856 RID: 6230 RVA: 0x0007ED8C File Offset: 0x0007D18C
		// (remove) Token: 0x06001857 RID: 6231 RVA: 0x0007EDC4 File Offset: 0x0007D1C4
		private event Action m_OnCompleteGetPurchaseLog;

		// Token: 0x06001858 RID: 6232 RVA: 0x0007EDFC File Offset: 0x0007D1FC
		public void Request_PurchaseLog(Action onComplete)
		{
			this.m_OnCompleteGetPurchaseLog = onComplete;
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			STORERequestTypes.GetPurchaseLog param = new STORERequestTypes.GetPurchaseLog();
			MeigewwwParam purchaseLog = STORERequest.GetPurchaseLog(param, new MeigewwwParam.Callback(this.OnResponse_GetPurchaseLog));
			NetworkQueueManager.Request(purchaseLog);
		}

		// Token: 0x06001859 RID: 6233 RVA: 0x0007EE40 File Offset: 0x0007D240
		private void OnResponse_GetPurchaseLog(MeigewwwParam wwwParam)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
			STOREResponseTypes.GetPurchaseLog purchaseLog = STOREResponse.GetPurchaseLog(wwwParam, ResponseCommon.DialogType.None, null);
			if (purchaseLog == null)
			{
				return;
			}
			ResultCode result = purchaseLog.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
			}
			else
			{
				this.m_PurchaseLogs.Clear();
				for (int i = 0; i < purchaseLog.purchases.Length; i++)
				{
					StoreManager.PurchaseLog purchaseLog2 = new StoreManager.PurchaseLog();
					StoreManager.wwwConvert(purchaseLog.purchases[i], purchaseLog2);
					this.m_PurchaseLogs.Add(purchaseLog2);
				}
				this.m_OnCompleteGetPurchaseLog.Call();
			}
		}

		// Token: 0x0600185A RID: 6234 RVA: 0x0007EEE3 File Offset: 0x0007D2E3
		private static void wwwConvert(PlayerPurchase src, StoreManager.PurchaseLog dest)
		{
			dest.m_Name = src.name;
			dest.m_Price = src.price;
			dest.m_Date = src.purchasedAt;
		}

		// Token: 0x0600185B RID: 6235 RVA: 0x0007EF09 File Offset: 0x0007D309
		[Conditional("____LOG_STORE")]
		private static void LOG(object o)
		{
			UnityEngine.Debug.Log("[STORE]" + o);
		}

		// Token: 0x0600185C RID: 6236 RVA: 0x0007EF1B File Offset: 0x0007D31B
		[Conditional("____LOG_STORE")]
		private static void LOGF(string format, params object[] args)
		{
			UnityEngine.Debug.LogFormat("[STORE]" + format, args);
		}

		// Token: 0x04001EF7 RID: 7927
		private long m_MonthlyAmount;

		// Token: 0x04001EF8 RID: 7928
		private long m_AvailableBalance;

		// Token: 0x04001EF9 RID: 7929
		private List<StoreManager.Product> m_Products = new List<StoreManager.Product>();

		// Token: 0x04001EFA RID: 7930
		private List<StoreManager.PurchaseLog> m_PurchaseLogs = new List<StoreManager.PurchaseLog>();

		// Token: 0x04001EFB RID: 7931
		private StoreManager.eStep m_Step = StoreManager.eStep.None;

		// Token: 0x04001EFC RID: 7932
		private int m_RestoreIndex;

		// Token: 0x04001EFD RID: 7933
		private InAppPurchaser m_InAppPurchaser;

		// Token: 0x04001EFF RID: 7935
		private eAgeType m_RequestedAgeType;

		// Token: 0x04001F01 RID: 7937
		private StoreManager.Product m_RequestedProduct;

		// Token: 0x04001F06 RID: 7942
		[CompilerGenerated]
		private static Comparison<StoreManager.Product> <>f__mg$cache0;

		// Token: 0x04001F07 RID: 7943
		[CompilerGenerated]
		private static Comparison<StoreManager.Product> <>f__mg$cache1;

		// Token: 0x020004D1 RID: 1233
		public class Product
		{
			// Token: 0x04001F08 RID: 7944
			public int m_ID;

			// Token: 0x04001F09 RID: 7945
			public string m_ProductID;

			// Token: 0x04001F0A RID: 7946
			public string m_Name;

			// Token: 0x04001F0B RID: 7947
			public string m_Desc;

			// Token: 0x04001F0C RID: 7948
			public long m_Price;

			// Token: 0x04001F0D RID: 7949
			public long m_UnlimitedGem;

			// Token: 0x04001F0E RID: 7950
			public long m_LimitedGem;

			// Token: 0x04001F0F RID: 7951
			public int m_Limit;

			// Token: 0x04001F10 RID: 7952
			public int m_LinkID;

			// Token: 0x04001F11 RID: 7953
			public int m_uiPriority;

			// Token: 0x04001F12 RID: 7954
			public StoreDefine.eUIType m_uiType;
		}

		// Token: 0x020004D2 RID: 1234
		public class PurchaseLog
		{
			// Token: 0x04001F13 RID: 7955
			public string m_Name;

			// Token: 0x04001F14 RID: 7956
			public long m_Price;

			// Token: 0x04001F15 RID: 7957
			public DateTime m_Date;
		}

		// Token: 0x020004D3 RID: 1235
		private enum eStep
		{
			// Token: 0x04001F17 RID: 7959
			None = -1,
			// Token: 0x04001F18 RID: 7960
			InitializeProducts_Wait,
			// Token: 0x04001F19 RID: 7961
			RestoreFirst,
			// Token: 0x04001F1A RID: 7962
			RestoreAPI,
			// Token: 0x04001F1B RID: 7963
			RestoreAPI_Wait,
			// Token: 0x04001F1C RID: 7964
			RestoreFinish,
			// Token: 0x04001F1D RID: 7965
			PurchaseIAP_Wait,
			// Token: 0x04001F1E RID: 7966
			PurchaseSendReceiptAPI_Wait
		}
	}
}
