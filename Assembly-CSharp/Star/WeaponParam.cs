﻿using System;

namespace Star
{
	// Token: 0x02000155 RID: 341
	[Serializable]
	public class WeaponParam
	{
		// Token: 0x060009B2 RID: 2482 RVA: 0x0003C2BA File Offset: 0x0003A6BA
		public WeaponParam()
		{
			this.MngID = -1L;
			this.WeaponID = -1;
			this.Lv = 1;
			this.MaxLv = 1;
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060009B3 RID: 2483 RVA: 0x0003C2DF File Offset: 0x0003A6DF
		// (set) Token: 0x060009B4 RID: 2484 RVA: 0x0003C2E7 File Offset: 0x0003A6E7
		public long MngID { get; set; }

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060009B5 RID: 2485 RVA: 0x0003C2F0 File Offset: 0x0003A6F0
		// (set) Token: 0x060009B6 RID: 2486 RVA: 0x0003C2F8 File Offset: 0x0003A6F8
		public int WeaponID { get; set; }

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060009B7 RID: 2487 RVA: 0x0003C301 File Offset: 0x0003A701
		// (set) Token: 0x060009B8 RID: 2488 RVA: 0x0003C309 File Offset: 0x0003A709
		public int Lv { get; set; }

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060009B9 RID: 2489 RVA: 0x0003C312 File Offset: 0x0003A712
		// (set) Token: 0x060009BA RID: 2490 RVA: 0x0003C31A File Offset: 0x0003A71A
		public int MaxLv { get; set; }

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060009BB RID: 2491 RVA: 0x0003C323 File Offset: 0x0003A723
		// (set) Token: 0x060009BC RID: 2492 RVA: 0x0003C32B File Offset: 0x0003A72B
		public long Exp { get; set; }

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060009BD RID: 2493 RVA: 0x0003C334 File Offset: 0x0003A734
		// (set) Token: 0x060009BE RID: 2494 RVA: 0x0003C33C File Offset: 0x0003A73C
		public int Atk { get; set; }

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x060009BF RID: 2495 RVA: 0x0003C345 File Offset: 0x0003A745
		// (set) Token: 0x060009C0 RID: 2496 RVA: 0x0003C34D File Offset: 0x0003A74D
		public int Mgc { get; set; }

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x060009C1 RID: 2497 RVA: 0x0003C356 File Offset: 0x0003A756
		// (set) Token: 0x060009C2 RID: 2498 RVA: 0x0003C35E File Offset: 0x0003A75E
		public int Def { get; set; }

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x060009C3 RID: 2499 RVA: 0x0003C367 File Offset: 0x0003A767
		// (set) Token: 0x060009C4 RID: 2500 RVA: 0x0003C36F File Offset: 0x0003A76F
		public int MDef { get; set; }

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x060009C5 RID: 2501 RVA: 0x0003C378 File Offset: 0x0003A778
		// (set) Token: 0x060009C6 RID: 2502 RVA: 0x0003C380 File Offset: 0x0003A780
		public int Cost { get; set; }

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x060009C7 RID: 2503 RVA: 0x0003C389 File Offset: 0x0003A789
		// (set) Token: 0x060009C8 RID: 2504 RVA: 0x0003C391 File Offset: 0x0003A791
		public SkillLearnData SkillLearnData { get; set; }
	}
}
