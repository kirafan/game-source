﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000011 RID: 17
	public class ADVDefine : MonoBehaviour
	{
		// Token: 0x04000079 RID: 121
		public const float DEFAULT_FADE_SEC = 1f;

		// Token: 0x0400007A RID: 122
		public static readonly float[] CHARA_POSITION = new float[]
		{
			-444f,
			-222f,
			0f,
			222f,
			444f,
			-555f,
			-333f,
			-111f,
			111f,
			333f,
			555f
		};
	}
}
