﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200026D RID: 621
	public class ADVDebugCharaSelectPanel : MonoBehaviour
	{
		// Token: 0x06000B86 RID: 2950 RVA: 0x0004357C File Offset: 0x0004197C
		public string GetADVCharaID()
		{
			return this.m_ADVCharaIDInput.text;
		}

		// Token: 0x06000B87 RID: 2951 RVA: 0x00043589 File Offset: 0x00041989
		public void ChangeStandPosition(int add)
		{
			this.m_StandPosition += add;
			if (this.m_StandPosition >= eADVStandPosition.Num)
			{
				this.m_StandPosition = eADVStandPosition.Left;
			}
			else if (this.m_StandPosition < eADVStandPosition.Left)
			{
				this.m_StandPosition = eADVStandPosition.RightRight;
			}
		}

		// Token: 0x06000B88 RID: 2952 RVA: 0x000435C6 File Offset: 0x000419C6
		public void ChangeFace(int add)
		{
			this.m_Face += add;
			if (this.m_Face >= eADVFace.Num)
			{
				this.m_Face = eADVFace.Default;
			}
			else if (this.m_Face < eADVFace.Default)
			{
				this.m_Face = eADVFace.Unique3;
			}
		}

		// Token: 0x06000B89 RID: 2953 RVA: 0x00043603 File Offset: 0x00041A03
		private void LateUpdate()
		{
			this.m_StandPositionText.text = this.m_StandPosition.ToString();
			this.m_FaceText.text = this.m_Face.ToString();
		}

		// Token: 0x040013F3 RID: 5107
		public InputField m_ADVCharaIDInput;

		// Token: 0x040013F4 RID: 5108
		public Text m_StandPositionText;

		// Token: 0x040013F5 RID: 5109
		public Text m_FaceText;

		// Token: 0x040013F6 RID: 5110
		public eADVStandPosition m_StandPosition;

		// Token: 0x040013F7 RID: 5111
		public eADVFace m_Face;
	}
}
