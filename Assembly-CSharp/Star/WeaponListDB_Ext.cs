﻿using System;

namespace Star
{
	// Token: 0x020001BE RID: 446
	public static class WeaponListDB_Ext
	{
		// Token: 0x06000B3E RID: 2878 RVA: 0x00042EB4 File Offset: 0x000412B4
		public static WeaponListDB_Param GetParam(this WeaponListDB self, int weaponID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == weaponID)
				{
					return self.m_Params[i];
				}
			}
			return default(WeaponListDB_Param);
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x00042F0C File Offset: 0x0004130C
		public static bool IsExistParam(this WeaponListDB self, int weaponID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == weaponID)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000B40 RID: 2880 RVA: 0x00042F4C File Offset: 0x0004134C
		public static bool IsBonusItem(this WeaponListDB self, int weaponID, int itemID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == weaponID)
				{
					for (int j = 0; j < self.m_Params[i].m_UpgradeBonusMaterialItemIDs.Length; j++)
					{
						if (self.m_Params[i].m_UpgradeBonusMaterialItemIDs[j] == itemID)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
	}
}
