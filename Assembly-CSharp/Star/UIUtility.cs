﻿using System;
using System.Collections.Generic;
using System.Text;
using Star.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000B29 RID: 2857
	public static class UIUtility
	{
		// Token: 0x06003BD8 RID: 15320 RVA: 0x00131780 File Offset: 0x0012FB80
		public static bool CheckPointOverUI(Vector2 pos)
		{
			bool result = false;
			if (EventSystem.current != null)
			{
				PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
				pointerEventData.position = pos;
				List<RaycastResult> list = new List<RaycastResult>();
				EventSystem.current.RaycastAll(pointerEventData, list);
				result = (list.Count > 0);
			}
			return result;
		}

		// Token: 0x06003BD9 RID: 15321 RVA: 0x001317D0 File Offset: 0x0012FBD0
		public static bool CheckPointerOverTarget(Vector2 pos, GameObject go)
		{
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = pos;
			List<RaycastResult> list = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list);
			for (int i = 0; i < list.Count; i++)
			{
				if (list[i].gameObject == go)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003BDA RID: 15322 RVA: 0x00131838 File Offset: 0x0012FC38
		public static Vector2 GetScaledScreenSize(CanvasScaler canvasScaler)
		{
			float num = (float)Screen.width / (float)Screen.height;
			Vector2 result = Vector2.zero;
			if (canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight)
			{
				Vector2 a = new Vector2(canvasScaler.referenceResolution.x, canvasScaler.referenceResolution.x / num);
				Vector2 b = new Vector2(canvasScaler.referenceResolution.y * num, canvasScaler.referenceResolution.y);
				result = Vector2.Lerp(a, b, canvasScaler.matchWidthOrHeight);
				if (canvasScaler.referenceResolution.x >= canvasScaler.referenceResolution.y || Screen.width > Screen.height)
				{
				}
			}
			return result;
		}

		// Token: 0x06003BDB RID: 15323 RVA: 0x001318F4 File Offset: 0x0012FCF4
		public static Vector2 RawScreenPositionToScaledScreenPosition(CanvasScaler canvasScaler, Vector2 rawPos)
		{
			Vector2 scaledScreenSize = UIUtility.GetScaledScreenSize(canvasScaler);
			Vector2 zero = Vector2.zero;
			if (canvasScaler.uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize)
			{
				if (canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.Expand)
				{
					zero.x = scaledScreenSize.x / (float)Screen.width * rawPos.x;
					zero.y = scaledScreenSize.y / (float)Screen.height * rawPos.y;
				}
				else if (canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight)
				{
					if (canvasScaler.referenceResolution.x >= canvasScaler.referenceResolution.y || Screen.width > Screen.height)
					{
					}
					zero.x = scaledScreenSize.x / (float)Screen.width * rawPos.x;
					zero.y = scaledScreenSize.y / (float)Screen.height * rawPos.y;
				}
			}
			return zero;
		}

		// Token: 0x06003BDC RID: 15324 RVA: 0x001319DC File Offset: 0x0012FDDC
		public static T GetMenuComponent<T>(string menuName) where T : MenuUIBase
		{
			GameObject gameObject = GameObject.Find(menuName);
			if (gameObject == null)
			{
				return (T)((object)null);
			}
			T component = gameObject.GetComponent<T>();
			if (component == null)
			{
				return (T)((object)null);
			}
			return component;
		}

		// Token: 0x06003BDD RID: 15325 RVA: 0x00131A23 File Offset: 0x0012FE23
		public static int GetFriendListMax()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FriendLimit;
		}

		// Token: 0x06003BDE RID: 15326 RVA: 0x00131A39 File Offset: 0x0012FE39
		public static int GetMasterMaxLv()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterRankDB.m_Params.Length;
		}

		// Token: 0x06003BDF RID: 15327 RVA: 0x00131A54 File Offset: 0x0012FE54
		public static string CharaCurrentLvValueToString(int lv, UIUtility.eLvColorChange lvCol)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(lv.ToString());
			if (lvCol != UIUtility.eLvColorChange.None)
			{
				if (lvCol != UIUtility.eLvColorChange.Increase)
				{
					if (lvCol == UIUtility.eLvColorChange.Decrease)
					{
						stringBuilder = UIUtility.GetDecreaseString(stringBuilder);
					}
				}
				else
				{
					stringBuilder = UIUtility.GetIncreaseString(stringBuilder);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003BE0 RID: 15328 RVA: 0x00131AB7 File Offset: 0x0012FEB7
		public static string CharaCurrentLvValueToString(int lv, int increase = 0)
		{
			if (increase > 0)
			{
				return UIUtility.CharaCurrentLvValueToString(lv, UIUtility.eLvColorChange.Increase);
			}
			if (increase < 0)
			{
				return UIUtility.CharaCurrentLvValueToString(lv, UIUtility.eLvColorChange.Decrease);
			}
			return UIUtility.CharaCurrentLvValueToString(lv, UIUtility.eLvColorChange.None);
		}

		// Token: 0x06003BE1 RID: 15329 RVA: 0x00131AE0 File Offset: 0x0012FEE0
		public static string CharaLvValueToString(int lv, int maxLv, UIUtility.eLvColorChange lvCol, UIUtility.eLvColorChange maxLvCol)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(lv.ToString());
			if (lvCol != UIUtility.eLvColorChange.None)
			{
				if (lvCol != UIUtility.eLvColorChange.Increase)
				{
					if (lvCol == UIUtility.eLvColorChange.Decrease)
					{
						stringBuilder = UIUtility.GetDecreaseString(stringBuilder);
					}
				}
				else
				{
					stringBuilder = UIUtility.GetIncreaseString(stringBuilder);
				}
			}
			stringBuilder.Append(" / ");
			StringBuilder stringBuilder2 = new StringBuilder();
			stringBuilder2.Append(maxLv.ToString());
			if (maxLvCol != UIUtility.eLvColorChange.None)
			{
				if (maxLvCol != UIUtility.eLvColorChange.Increase)
				{
					if (maxLvCol == UIUtility.eLvColorChange.Decrease)
					{
						stringBuilder2 = UIUtility.GetDecreaseString(stringBuilder2);
					}
				}
				else
				{
					stringBuilder2 = UIUtility.GetIncreaseString(stringBuilder2);
				}
			}
			stringBuilder.Append(stringBuilder2.ToString());
			return stringBuilder.ToString();
		}

		// Token: 0x06003BE2 RID: 15330 RVA: 0x00131BAC File Offset: 0x0012FFAC
		public static string CharaLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0)
		{
			UIUtility.eLvColorChange lvCol;
			if (increase > 0)
			{
				lvCol = UIUtility.eLvColorChange.Increase;
			}
			else if (increase < 0)
			{
				lvCol = UIUtility.eLvColorChange.Decrease;
			}
			else
			{
				lvCol = UIUtility.eLvColorChange.None;
			}
			UIUtility.eLvColorChange maxLvCol;
			if (increaseMaxLv > 0)
			{
				maxLvCol = UIUtility.eLvColorChange.Increase;
			}
			else if (increaseMaxLv < 0)
			{
				maxLvCol = UIUtility.eLvColorChange.Decrease;
			}
			else
			{
				maxLvCol = UIUtility.eLvColorChange.None;
			}
			return UIUtility.CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
		}

		// Token: 0x06003BE3 RID: 15331 RVA: 0x00131C02 File Offset: 0x00130002
		public static string WeaponCurrentLvValueToString(int lv, UIUtility.eLvColorChange lvCol)
		{
			return UIUtility.CharaCurrentLvValueToString(lv, lvCol);
		}

		// Token: 0x06003BE4 RID: 15332 RVA: 0x00131C0B File Offset: 0x0013000B
		public static string WeaponCurrentLvValueToString(int lv, int increase = 0)
		{
			return UIUtility.CharaCurrentLvValueToString(lv, increase);
		}

		// Token: 0x06003BE5 RID: 15333 RVA: 0x00131C14 File Offset: 0x00130014
		public static string WeaponLvValueToString(int lv, int maxLv, UIUtility.eLvColorChange lvCol, UIUtility.eLvColorChange maxLvCol)
		{
			return UIUtility.CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
		}

		// Token: 0x06003BE6 RID: 15334 RVA: 0x00131C1F File Offset: 0x0013001F
		public static string WeaponLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0)
		{
			return UIUtility.CharaLvValueToString(lv, maxLv, increase, increaseMaxLv);
		}

		// Token: 0x06003BE7 RID: 15335 RVA: 0x00131C2A File Offset: 0x0013002A
		public static string SkillCurrentLvValueToString(int lv, UIUtility.eLvColorChange lvCol)
		{
			return UIUtility.CharaCurrentLvValueToString(lv, lvCol);
		}

		// Token: 0x06003BE8 RID: 15336 RVA: 0x00131C33 File Offset: 0x00130033
		public static string SkillCurrentLvValueToString(int lv, int increase = 0)
		{
			return UIUtility.CharaCurrentLvValueToString(lv, increase);
		}

		// Token: 0x06003BE9 RID: 15337 RVA: 0x00131C3C File Offset: 0x0013003C
		public static string SkillLvValueToString(int lv, int maxLv, UIUtility.eLvColorChange lvCol, UIUtility.eLvColorChange maxLvCol)
		{
			return UIUtility.CharaLvValueToString(lv, maxLv, lvCol, maxLvCol);
		}

		// Token: 0x06003BEA RID: 15338 RVA: 0x00131C47 File Offset: 0x00130047
		public static string SkillLvValueToString(int lv, int maxLv, int increase = 0, int increaseMaxLv = 0)
		{
			return UIUtility.CharaLvValueToString(lv, maxLv, increase, increaseMaxLv);
		}

		// Token: 0x06003BEB RID: 15339 RVA: 0x00131C54 File Offset: 0x00130054
		public static string ItemHaveNumToString(int value, bool countStop = true)
		{
			if (countStop && value > 999)
			{
				value = 999;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BEC RID: 15340 RVA: 0x00131CA4 File Offset: 0x001300A4
		public static string GoldValueToString(int value, bool countStop = true)
		{
			if (countStop && value > 9999999)
			{
				value = 9999999;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BED RID: 15341 RVA: 0x00131CF4 File Offset: 0x001300F4
		public static string GoldValueToString(long value, bool countStop = true)
		{
			if (countStop && value > 9999999L)
			{
				value = 9999999L;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BEE RID: 15342 RVA: 0x00131D44 File Offset: 0x00130144
		public static string GemValueToString(int value, bool countStop = true)
		{
			if (countStop && value > 9999999)
			{
				value = 9999999;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BEF RID: 15343 RVA: 0x00131D94 File Offset: 0x00130194
		public static string GemValueToString(long value, bool countStop = true)
		{
			if (countStop && value > 9999999L)
			{
				value = 9999999L;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BF0 RID: 15344 RVA: 0x00131DE4 File Offset: 0x001301E4
		public static string KPValueToString(int value, bool countStop = true)
		{
			if (countStop)
			{
				int num = (int)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetLimit();
				if (value >= num)
				{
					value = num;
					return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
				}
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BF1 RID: 15345 RVA: 0x00131E44 File Offset: 0x00130244
		public static string KPValueToString(long value, bool countStop = true)
		{
			if (countStop)
			{
				long limit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetLimit();
				if (value >= limit)
				{
					value = limit;
					return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
				}
			}
			return value.ToString("#,0");
		}

		// Token: 0x06003BF2 RID: 15346 RVA: 0x00131EA4 File Offset: 0x001302A4
		public static string StaminaValueToString(int value, bool countStop = true)
		{
			if (countStop && value > 9999)
			{
				value = 9999;
				return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
			}
			return value.ToString();
		}

		// Token: 0x06003BF3 RID: 15347 RVA: 0x00131EF4 File Offset: 0x001302F4
		public static string StaminaValueToString(long value, bool countStop = true)
		{
			if (countStop)
			{
				if (value > 9999L)
				{
					value = 9999L;
					return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
				}
				if (value >= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax())
				{
					return "<color=#FFBE41>" + value.ToString("#,0") + "</color>";
				}
			}
			return value.ToString();
		}

		// Token: 0x06003BF4 RID: 15348 RVA: 0x00131F7F File Offset: 0x0013037F
		public static string MoneyValueToString(int value)
		{
			return value.ToString("#,0");
		}

		// Token: 0x06003BF5 RID: 15349 RVA: 0x00131F8D File Offset: 0x0013038D
		public static string MoneyValueToString(long value)
		{
			return value.ToString("#,0");
		}

		// Token: 0x06003BF6 RID: 15350 RVA: 0x00131F9B File Offset: 0x0013039B
		public static StringBuilder GetNotEnoughString(StringBuilder sb)
		{
			sb.Insert(0, "<color=red>");
			sb.Append("</color>");
			return sb;
		}

		// Token: 0x06003BF7 RID: 15351 RVA: 0x00131FB8 File Offset: 0x001303B8
		public static string GetNeedGoldString(int amount)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)amount))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(UIUtility.GoldValueToString(amount, true));
				return UIUtility.GetNotEnoughString(stringBuilder).ToString();
			}
			return UIUtility.GoldValueToString(amount, true);
		}

		// Token: 0x06003BF8 RID: 15352 RVA: 0x00132008 File Offset: 0x00130408
		public static string GetNeedKPString(int amount)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfKP((long)amount))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(UIUtility.KPValueToString(amount, false));
				return UIUtility.GetNotEnoughString(stringBuilder).ToString();
			}
			return UIUtility.KPValueToString(amount, false);
		}

		// Token: 0x06003BF9 RID: 15353 RVA: 0x00132058 File Offset: 0x00130458
		public static string GetNeedGemString(int amount)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)amount))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(UIUtility.GemValueToString(amount, true));
				return UIUtility.GetNotEnoughString(stringBuilder).ToString();
			}
			return UIUtility.GemValueToString(amount, true);
		}

		// Token: 0x06003BFA RID: 15354 RVA: 0x001320A7 File Offset: 0x001304A7
		public static StringBuilder GetIncreaseString(StringBuilder sb)
		{
			sb.Insert(0, "<color=#00C882>");
			sb.Append("</color>");
			return sb;
		}

		// Token: 0x06003BFB RID: 15355 RVA: 0x001320C3 File Offset: 0x001304C3
		public static StringBuilder GetDecreaseString(StringBuilder sb)
		{
			sb.Insert(0, "<color=#FF4141>");
			sb.Append("</color>");
			return sb;
		}

		// Token: 0x06003BFC RID: 15356 RVA: 0x001320DF File Offset: 0x001304DF
		public static StringBuilder GetCountStopString(StringBuilder sb)
		{
			sb.Insert(0, "<color=#FFBE41>");
			sb.Append("</color>");
			return sb;
		}

		// Token: 0x06003BFD RID: 15357 RVA: 0x001320FC File Offset: 0x001304FC
		public static string GetLastLoginString(DateTime lastTime)
		{
			StringBuilder stringBuilder = new StringBuilder();
			TimeSpan timeSpan = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime - lastTime;
			if (timeSpan.Days > 999)
			{
				stringBuilder.Append(999);
				stringBuilder.Append("日前");
			}
			else if (timeSpan.Days > 0)
			{
				stringBuilder.Append(timeSpan.Days.ToString());
				stringBuilder.Append("日前");
			}
			else if (timeSpan.Hours > 0)
			{
				stringBuilder.Append(timeSpan.Hours.ToString());
				stringBuilder.Append("時間前");
			}
			else
			{
				stringBuilder.Append(timeSpan.Minutes.ToString());
				stringBuilder.Append("分前");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003BFE RID: 15358 RVA: 0x001321F4 File Offset: 0x001305F4
		public static string GetSpanString(TimeSpan span)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (span.Days <= 999)
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
				if (span.Days > 0)
				{
					stringBuilder.Append(span.Days.ToString().PadLeft(3));
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
				}
				else if (span.Hours > 0)
				{
					stringBuilder.Append(span.Hours.ToString().PadLeft(2));
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
				}
				else if (span.Minutes > 0)
				{
					stringBuilder.Append(span.Minutes.ToString().PadLeft(3));
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
				}
				else
				{
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003BFF RID: 15359 RVA: 0x00132348 File Offset: 0x00130748
		public static string GetTradeLimitSpanString(TimeSpan span)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
			if (span.Days > 0)
			{
				stringBuilder.Append(span.Days.ToString().PadLeft(3));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
			}
			else if (span.Hours > 0)
			{
				stringBuilder.Append(span.Hours.ToString().PadLeft(2));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
			}
			else if (span.Minutes > 0)
			{
				stringBuilder.Append(span.Minutes.ToString().PadLeft(3));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
			}
			else
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003C00 RID: 15360 RVA: 0x00132484 File Offset: 0x00130884
		public static string GetMissionLimitSpanString(TimeSpan span)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeRemain));
			if (span.Days > 0)
			{
				stringBuilder.Append(span.Days.ToString().PadLeft(3));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeDay));
			}
			else if (span.Hours > 0)
			{
				stringBuilder.Append(span.Hours.ToString().PadLeft(2));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeHour));
			}
			else if (span.Minutes > 0)
			{
				stringBuilder.Append(span.Minutes.ToString().PadLeft(3));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMinute));
			}
			else
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTimeMin));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003C01 RID: 15361 RVA: 0x001325C0 File Offset: 0x001309C0
		public static string GetDayOfWeek(DayOfWeek dayofweek)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonSunday + (int)dayofweek);
		}

		// Token: 0x06003C02 RID: 15362 RVA: 0x001325D8 File Offset: 0x001309D8
		public static void GetRemainTimeToTimes(out int fday, out int fhour, out int fminute, out int fsec, int ftimes)
		{
			fday = ftimes / 86400;
			ftimes -= fday * 3600 * 24;
			fhour = ftimes / 3600;
			ftimes -= fhour * 3600;
			fminute = ftimes / 60;
			ftimes -= fminute * 60;
			fsec = ftimes;
		}

		// Token: 0x06003C03 RID: 15363 RVA: 0x0013262C File Offset: 0x00130A2C
		public static string GetBuildTimeString(int sec)
		{
			int num;
			int num2;
			int num3;
			int num4;
			UIUtility.GetRemainTimeToTimes(out num, out num2, out num3, out num4, sec);
			if (num > 0 || num2 > 0)
			{
				return string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainHour, new object[]
				{
					num * 24 + num2
				}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, new object[]
				{
					num3
				}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, new object[]
				{
					num4
				}));
			}
			if (num3 > 0)
			{
				return string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, new object[]
				{
					num3
				}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainMinute, new object[]
				{
					num4
				}));
			}
			return string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildRemainSec, new object[]
			{
				num4
			}), new object[0]);
		}

		// Token: 0x06003C04 RID: 15364 RVA: 0x0013274C File Offset: 0x00130B4C
		public static Color GetIconColor(eElementType elementType)
		{
			switch (elementType + 1)
			{
			case eElementType.Fire:
				return UIDefine.NONE_ELEMENT_COLOR;
			case eElementType.Water:
				return UIDefine.FIRE_COLOR;
			case eElementType.Earth:
				return UIDefine.WATER_COLOR;
			case eElementType.Wind:
				return UIDefine.EARTH_COLOR;
			case eElementType.Moon:
				return UIDefine.WIND_COLOR;
			case eElementType.Sun:
				return UIDefine.MOON_COLOR;
			case eElementType.Num:
				return UIDefine.SUN_COLOR;
			default:
				return Color.white;
			}
		}

		// Token: 0x06003C05 RID: 15365 RVA: 0x001327B4 File Offset: 0x00130BB4
		public static int ElementEnumToSortIdx(eElementType elementType)
		{
			if (elementType == eElementType.None)
			{
				return -1;
			}
			for (int i = 0; i < UIDefine.ELEMENT_SORT.Length; i++)
			{
				if (UIDefine.ELEMENT_SORT[i] == elementType)
				{
					return i;
				}
			}
			Debug.LogError("Unknown Element");
			return -1;
		}

		// Token: 0x06003C06 RID: 15366 RVA: 0x001327FC File Offset: 0x00130BFC
		public static eElementType ElementSortIdxToEnum(int sortIdx)
		{
			if (sortIdx >= 0 && sortIdx < UIDefine.ELEMENT_SORT.Length)
			{
				return UIDefine.ELEMENT_SORT[sortIdx];
			}
			Debug.LogError("Invalid Idx");
			return eElementType.None;
		}

		// Token: 0x06003C07 RID: 15367 RVA: 0x00132828 File Offset: 0x00130C28
		public static object FindHrcObject(Transform ptarget, string findname, Type pfindkey)
		{
			Transform transform = UIUtility.FindHrcTransform(ptarget, findname);
			if (transform != null)
			{
				return transform.gameObject.GetComponent(pfindkey);
			}
			return null;
		}

		// Token: 0x06003C08 RID: 15368 RVA: 0x00132858 File Offset: 0x00130C58
		public static Transform FindHrcTransform(Transform ptarget, string findname)
		{
			string[] array = findname.Split(new char[]
			{
				'/'
			});
			int num = 0;
			return UIUtility.FindKeyTransform(ptarget, array, array.Length, ref num);
		}

		// Token: 0x06003C09 RID: 15369 RVA: 0x00132888 File Offset: 0x00130C88
		public static Transform FindKeyTransform(Transform ptarget, string[] findname, int flength, ref int findex)
		{
			if (ptarget.name == findname[findex])
			{
				findex++;
				if (findex >= flength)
				{
					return ptarget;
				}
			}
			int childCount = ptarget.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform transform = UIUtility.FindKeyTransform(ptarget.GetChild(i), findname, flength, ref findex);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x06003C0A RID: 15370 RVA: 0x001328F4 File Offset: 0x00130CF4
		public static void AttachCanvasForChangeRenderOrder(GameObject obj, int sortOrder, bool addGraphicsRaycaster)
		{
			Canvas canvas = obj.GetComponent<Canvas>();
			if (canvas == null)
			{
				canvas = obj.AddComponent<Canvas>();
			}
			canvas.overrideSorting = true;
			canvas.sortingOrder = sortOrder;
			if (addGraphicsRaycaster)
			{
				GraphicRaycaster component = obj.GetComponent<GraphicRaycaster>();
				if (component == null)
				{
					obj.AddComponent<GraphicRaycaster>();
				}
			}
		}

		// Token: 0x06003C0B RID: 15371 RVA: 0x0013294C File Offset: 0x00130D4C
		public static void DetachCanvasForChangeRenderOrder(GameObject obj, bool removeGraphicsRaycaster)
		{
			if (removeGraphicsRaycaster)
			{
				GraphicRaycaster component = obj.GetComponent<GraphicRaycaster>();
				if (component != null)
				{
					obj.RemoveComponent<GraphicRaycaster>();
				}
			}
			Canvas component2 = obj.GetComponent<Canvas>();
			if (component2 != null)
			{
				obj.RemoveComponent<Canvas>();
			}
		}

		// Token: 0x06003C0C RID: 15372 RVA: 0x00132994 File Offset: 0x00130D94
		public static void PlayVoiceScheduleChara(eSoundVoiceListDB voice)
		{
			List<eCharaNamedType> list = new List<eCharaNamedType>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID().Length; i++)
			{
				long num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID()[i];
				if (num != -1L)
				{
					list.Add(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(num).Param.NamedType);
				}
			}
			if (list.Count > 0)
			{
				eCharaNamedType namedType = list[UnityEngine.Random.Range(0, list.Count)];
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(namedType, voice, true);
			}
		}

		// Token: 0x06003C0D RID: 15373 RVA: 0x00132A40 File Offset: 0x00130E40
		public static void UpdateAndroidBackKey(CustomButton customButton, Func<bool> checkFunc = null)
		{
			if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape))
			{
				if (InputTouch.Inst.GetAvailableTouchCnt() > 0)
				{
					return;
				}
				if (checkFunc != null && !checkFunc())
				{
					return;
				}
				if (customButton == null)
				{
					return;
				}
				if (!customButton.gameObject.activeInHierarchy)
				{
					return;
				}
				if (!customButton.IsEnableInput)
				{
					return;
				}
				if (!customButton.Interactable)
				{
					return;
				}
				if (!Utility.CheckTapUI(customButton.gameObject))
				{
					return;
				}
				Debug.Log("Android BackKey input." + customButton.gameObject);
				customButton.ExecuteClick();
			}
		}

		// Token: 0x06003C0E RID: 15374 RVA: 0x00132AEC File Offset: 0x00130EEC
		public static bool GetInputAndroidBackKey(CustomButton customButton = null)
		{
			if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape) && InputTouch.Inst.GetAvailableTouchCnt() <= 0)
			{
				if (customButton != null)
				{
					if (!customButton.gameObject.activeSelf)
					{
						return false;
					}
					if (!customButton.IsEnableInput)
					{
						return false;
					}
					if (!customButton.Interactable)
					{
						return false;
					}
					if (!Utility.CheckTapUI(customButton.gameObject))
					{
						return false;
					}
				}
				return true;
			}
			return false;
		}

		// Token: 0x02000B2A RID: 2858
		public enum eLvColorChange
		{
			// Token: 0x040043D6 RID: 17366
			None,
			// Token: 0x040043D7 RID: 17367
			Increase,
			// Token: 0x040043D8 RID: 17368
			Decrease
		}
	}
}
