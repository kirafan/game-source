﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000661 RID: 1633
	public static class SceneDefine
	{
		// Token: 0x0400273C RID: 10044
		public const string SCENE_DIR = "Scene/";

		// Token: 0x0400273D RID: 10045
		public static Dictionary<SceneDefine.eSceneID, SceneDefine.SceneInfo> SCENE_INFOS = new Dictionary<SceneDefine.eSceneID, SceneDefine.SceneInfo>
		{
			{
				SceneDefine.eSceneID.Title,
				new SceneDefine.SceneInfo("Title", false)
			},
			{
				SceneDefine.eSceneID.Download,
				new SceneDefine.SceneInfo("Download", false)
			},
			{
				SceneDefine.eSceneID.Signup,
				new SceneDefine.SceneInfo("Signup", false)
			},
			{
				SceneDefine.eSceneID.Town,
				new SceneDefine.SceneInfo("Town", true)
			},
			{
				SceneDefine.eSceneID.Room,
				new SceneDefine.SceneInfo("Room", true)
			},
			{
				SceneDefine.eSceneID.Quest,
				new SceneDefine.SceneInfo("Quest", true)
			},
			{
				SceneDefine.eSceneID.Edit,
				new SceneDefine.SceneInfo("Edit", true)
			},
			{
				SceneDefine.eSceneID.Battle,
				new SceneDefine.SceneInfo("Battle", true)
			},
			{
				SceneDefine.eSceneID.ADV,
				new SceneDefine.SceneInfo("ADV", true)
			},
			{
				SceneDefine.eSceneID.Training,
				new SceneDefine.SceneInfo("Training", true)
			},
			{
				SceneDefine.eSceneID.Gacha,
				new SceneDefine.SceneInfo("Gacha", true)
			},
			{
				SceneDefine.eSceneID.GachaPlay,
				new SceneDefine.SceneInfo("GachaPlay", true)
			},
			{
				SceneDefine.eSceneID.Shop,
				new SceneDefine.SceneInfo("Shop", true)
			},
			{
				SceneDefine.eSceneID.Menu,
				new SceneDefine.SceneInfo("Menu", true)
			},
			{
				SceneDefine.eSceneID.Present,
				new SceneDefine.SceneInfo("Present", true)
			},
			{
				SceneDefine.eSceneID.MoviePlay,
				new SceneDefine.SceneInfo("MoviePlay", true)
			}
		};

		// Token: 0x0400273E RID: 10046
		public const string CHILD_SCENE_DIR = "Scene/Child/";

		// Token: 0x0400273F RID: 10047
		public static Dictionary<SceneDefine.eChildSceneID, SceneDefine.SceneInfo> CHILD_SCENE_INFOS = new Dictionary<SceneDefine.eChildSceneID, SceneDefine.SceneInfo>
		{
			{
				SceneDefine.eChildSceneID.QuestCategorySelectUI,
				new SceneDefine.SceneInfo("Quest_QuestCategorySelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.QuestGroupSelectUI,
				new SceneDefine.SceneInfo("Quest_QuestGroupSelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.QuestChapterSelectUI,
				new SceneDefine.SceneInfo("Quest_QuestChapterSelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.QuestSelectUI,
				new SceneDefine.SceneInfo("Quest_QuestSelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.QuestFriendSelectUI,
				new SceneDefine.SceneInfo("Quest_QuestFriendSelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.BattleResultUI,
				new SceneDefine.SceneInfo("Battle_BattleResultUI", true)
			},
			{
				SceneDefine.eChildSceneID.EditTopUI,
				new SceneDefine.SceneInfo("Edit_EditTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.PartyEditUI,
				new SceneDefine.SceneInfo("Edit_PartyEditUI", true)
			},
			{
				SceneDefine.eChildSceneID.SupportEditUI,
				new SceneDefine.SceneInfo("Edit_SupportEditUI", true)
			},
			{
				SceneDefine.eChildSceneID.MasterEquipUI,
				new SceneDefine.SceneInfo("Edit_MasterEquipUI", true)
			},
			{
				SceneDefine.eChildSceneID.SkillEditUI,
				new SceneDefine.SceneInfo("Edit_SkillEditUI", true)
			},
			{
				SceneDefine.eChildSceneID.UpgradeUI,
				new SceneDefine.SceneInfo("Edit_UpgradeUI", true)
			},
			{
				SceneDefine.eChildSceneID.UpgradeResultUI,
				new SceneDefine.SceneInfo("Edit_UpgradeResultUI", true)
			},
			{
				SceneDefine.eChildSceneID.LimitBreakUI,
				new SceneDefine.SceneInfo("Edit_LimitBreakUI", true)
			},
			{
				SceneDefine.eChildSceneID.LimitBreakResultUI,
				new SceneDefine.SceneInfo("Edit_LimitBreakResultUI", true)
			},
			{
				SceneDefine.eChildSceneID.EvolutionUI,
				new SceneDefine.SceneInfo("Edit_EvolutionUI", true)
			},
			{
				SceneDefine.eChildSceneID.EvolutionResultUI,
				new SceneDefine.SceneInfo("Edit_EvolutionResultUI", true)
			},
			{
				SceneDefine.eChildSceneID.CharaListUI,
				new SceneDefine.SceneInfo("Edit_CharaListUI", true)
			},
			{
				SceneDefine.eChildSceneID.CharaDetailUI,
				new SceneDefine.SceneInfo("Edit_CharaDetailUI", true)
			},
			{
				SceneDefine.eChildSceneID.WeaponListUI,
				new SceneDefine.SceneInfo("Edit_WeaponListUI", true)
			},
			{
				SceneDefine.eChildSceneID.HomeUI,
				new SceneDefine.SceneInfo("Town_HomeUI", true)
			},
			{
				SceneDefine.eChildSceneID.TownUI,
				new SceneDefine.SceneInfo("Town_TownUI", true)
			},
			{
				SceneDefine.eChildSceneID.RoomUI,
				new SceneDefine.SceneInfo("Room_RoomUI", true)
			},
			{
				SceneDefine.eChildSceneID.TrainingUI,
				new SceneDefine.SceneInfo("Training_TrainingUI", true)
			},
			{
				SceneDefine.eChildSceneID.MissionUI,
				new SceneDefine.SceneInfo("Mission_MissionUI", true)
			},
			{
				SceneDefine.eChildSceneID.GachaPlayUI,
				new SceneDefine.SceneInfo("Gacha_GachaPlayUI", true)
			},
			{
				SceneDefine.eChildSceneID.GachaSelectUI,
				new SceneDefine.SceneInfo("Gacha_GachaSelectUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopTopUI,
				new SceneDefine.SceneInfo("Shop_ShopTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopTradeTopUI,
				new SceneDefine.SceneInfo("Shop_ShopTradeTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopTradeListUI,
				new SceneDefine.SceneInfo("Shop_ShopTradeListUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopTradeSaleListUI,
				new SceneDefine.SceneInfo("Shop_ShopTradeSaleListUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopWeaponTopUI,
				new SceneDefine.SceneInfo("Shop_ShopWeaponTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopWeaponCreateUI,
				new SceneDefine.SceneInfo("Shop_ShopWeaponCreateUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopWeaponUpgradeUI,
				new SceneDefine.SceneInfo("Shop_ShopWeaponUpgradeUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopWeaponSaleUI,
				new SceneDefine.SceneInfo("Shop_ShopWeaponSaleUI", true)
			},
			{
				SceneDefine.eChildSceneID.ShopBuildTopUI,
				new SceneDefine.SceneInfo("Shop_ShopBuildTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.RoomShopListUI,
				new SceneDefine.SceneInfo("RoomShopListUI", true)
			},
			{
				SceneDefine.eChildSceneID.NPCCharaDisplayUI,
				new SceneDefine.SceneInfo("NPCCharaDisplayUI", true)
			},
			{
				SceneDefine.eChildSceneID.MasterDisplayUI,
				new SceneDefine.SceneInfo("MasterDisplayUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuTopUI,
				new SceneDefine.SceneInfo("Menu_MenuTopUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuUserInfomationUI,
				new SceneDefine.SceneInfo("Menu_UserInfomationUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuFriendUI,
				new SceneDefine.SceneInfo("Menu_FriendUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuLibraryUI,
				new SceneDefine.SceneInfo("Menu_LibraryUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuOptionUI,
				new SceneDefine.SceneInfo("Menu_OptionUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuNotificationUI,
				new SceneDefine.SceneInfo("Menu_NotificationUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuSupportUI,
				new SceneDefine.SceneInfo("Menu_SupportUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuLibraryWordUI,
				new SceneDefine.SceneInfo("Menu_LibraryWordUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuLibraryEventUI,
				new SceneDefine.SceneInfo("Menu_LibraryEventUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuLibraryOriginalCharaUI,
				new SceneDefine.SceneInfo("Menu_LibraryOriginalCharaUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuPlayerMoveConfiguration,
				new SceneDefine.SceneInfo("Menu_PlayerMoveConfigurationUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuPurchaseHistory,
				new SceneDefine.SceneInfo("Menu_PurchaseHistoryUI", true)
			},
			{
				SceneDefine.eChildSceneID.MenuHelpUI,
				new SceneDefine.SceneInfo("Menu_HelpUI", true)
			},
			{
				SceneDefine.eChildSceneID.PresentUI,
				new SceneDefine.SceneInfo("Present_PresentUI", true)
			},
			{
				SceneDefine.eChildSceneID.StaminaShopUI,
				new SceneDefine.SceneInfo("StaminaShopUI", true)
			},
			{
				SceneDefine.eChildSceneID.GemShopUI,
				new SceneDefine.SceneInfo("GemShopUI", true)
			},
			{
				SceneDefine.eChildSceneID.RewardUI,
				new SceneDefine.SceneInfo("RewardUI", true)
			},
			{
				SceneDefine.eChildSceneID.LoginBonusUI,
				new SceneDefine.SceneInfo("LoginBonus_LoginBonusUI", true)
			},
			{
				SceneDefine.eChildSceneID.FilterUI,
				new SceneDefine.SceneInfo("Filter_FilterUI", true)
			},
			{
				SceneDefine.eChildSceneID.ScrollFilterUI,
				new SceneDefine.SceneInfo("Filter_ScrollFilterUI", true)
			},
			{
				SceneDefine.eChildSceneID.SortUI,
				new SceneDefine.SceneInfo("Sort_SortUI", true)
			},
			{
				SceneDefine.eChildSceneID.InfoUI,
				new SceneDefine.SceneInfo("InfoUI", true)
			}
		};

		// Token: 0x02000662 RID: 1634
		public struct SceneInfo
		{
			// Token: 0x06002116 RID: 8470 RVA: 0x000B06F2 File Offset: 0x000AEAF2
			public SceneInfo(string sceneName, bool isAssetBundle = true)
			{
				this.m_SceneName = sceneName;
				this.m_IsAssetBundle = isAssetBundle;
			}

			// Token: 0x04002740 RID: 10048
			public string m_SceneName;

			// Token: 0x04002741 RID: 10049
			public bool m_IsAssetBundle;
		}

		// Token: 0x02000663 RID: 1635
		public enum eSceneID
		{
			// Token: 0x04002743 RID: 10051
			None = -1,
			// Token: 0x04002744 RID: 10052
			Title,
			// Token: 0x04002745 RID: 10053
			Download,
			// Token: 0x04002746 RID: 10054
			Signup,
			// Token: 0x04002747 RID: 10055
			Town,
			// Token: 0x04002748 RID: 10056
			Room,
			// Token: 0x04002749 RID: 10057
			Quest,
			// Token: 0x0400274A RID: 10058
			Edit,
			// Token: 0x0400274B RID: 10059
			Battle,
			// Token: 0x0400274C RID: 10060
			ADV,
			// Token: 0x0400274D RID: 10061
			Training,
			// Token: 0x0400274E RID: 10062
			Gacha,
			// Token: 0x0400274F RID: 10063
			GachaPlay,
			// Token: 0x04002750 RID: 10064
			Shop,
			// Token: 0x04002751 RID: 10065
			Menu,
			// Token: 0x04002752 RID: 10066
			Present,
			// Token: 0x04002753 RID: 10067
			MoviePlay,
			// Token: 0x04002754 RID: 10068
			Num
		}

		// Token: 0x02000664 RID: 1636
		public enum eChildSceneID
		{
			// Token: 0x04002756 RID: 10070
			QuestCategorySelectUI,
			// Token: 0x04002757 RID: 10071
			QuestGroupSelectUI,
			// Token: 0x04002758 RID: 10072
			QuestChapterSelectUI,
			// Token: 0x04002759 RID: 10073
			QuestSelectUI,
			// Token: 0x0400275A RID: 10074
			QuestFriendSelectUI,
			// Token: 0x0400275B RID: 10075
			BattleResultUI,
			// Token: 0x0400275C RID: 10076
			EditTopUI,
			// Token: 0x0400275D RID: 10077
			PartyEditUI,
			// Token: 0x0400275E RID: 10078
			SupportEditUI,
			// Token: 0x0400275F RID: 10079
			MasterEquipUI,
			// Token: 0x04002760 RID: 10080
			SkillEditUI,
			// Token: 0x04002761 RID: 10081
			UpgradeUI,
			// Token: 0x04002762 RID: 10082
			UpgradeResultUI,
			// Token: 0x04002763 RID: 10083
			LimitBreakUI,
			// Token: 0x04002764 RID: 10084
			LimitBreakResultUI,
			// Token: 0x04002765 RID: 10085
			EvolutionUI,
			// Token: 0x04002766 RID: 10086
			EvolutionResultUI,
			// Token: 0x04002767 RID: 10087
			CharaListUI,
			// Token: 0x04002768 RID: 10088
			CharaDetailUI,
			// Token: 0x04002769 RID: 10089
			WeaponListUI,
			// Token: 0x0400276A RID: 10090
			HomeUI,
			// Token: 0x0400276B RID: 10091
			TownUI,
			// Token: 0x0400276C RID: 10092
			RoomUI,
			// Token: 0x0400276D RID: 10093
			TrainingUI,
			// Token: 0x0400276E RID: 10094
			MissionUI,
			// Token: 0x0400276F RID: 10095
			GachaPlayUI,
			// Token: 0x04002770 RID: 10096
			GachaSelectUI,
			// Token: 0x04002771 RID: 10097
			ShopTopUI,
			// Token: 0x04002772 RID: 10098
			ShopTradeTopUI,
			// Token: 0x04002773 RID: 10099
			ShopTradeListUI,
			// Token: 0x04002774 RID: 10100
			ShopTradeSaleListUI,
			// Token: 0x04002775 RID: 10101
			ShopWeaponTopUI,
			// Token: 0x04002776 RID: 10102
			ShopWeaponCreateUI,
			// Token: 0x04002777 RID: 10103
			ShopWeaponUpgradeUI,
			// Token: 0x04002778 RID: 10104
			ShopWeaponSaleUI,
			// Token: 0x04002779 RID: 10105
			ShopBuildTopUI,
			// Token: 0x0400277A RID: 10106
			RoomShopListUI,
			// Token: 0x0400277B RID: 10107
			NPCCharaDisplayUI,
			// Token: 0x0400277C RID: 10108
			MasterDisplayUI,
			// Token: 0x0400277D RID: 10109
			MenuTopUI,
			// Token: 0x0400277E RID: 10110
			MenuUserInfomationUI,
			// Token: 0x0400277F RID: 10111
			MenuFriendUI,
			// Token: 0x04002780 RID: 10112
			MenuLibraryUI,
			// Token: 0x04002781 RID: 10113
			MenuOptionUI,
			// Token: 0x04002782 RID: 10114
			MenuNotificationUI,
			// Token: 0x04002783 RID: 10115
			MenuSupportUI,
			// Token: 0x04002784 RID: 10116
			MenuLibraryWordUI,
			// Token: 0x04002785 RID: 10117
			MenuLibraryEventUI,
			// Token: 0x04002786 RID: 10118
			MenuLibraryOriginalCharaUI,
			// Token: 0x04002787 RID: 10119
			MenuPlayerMoveConfiguration,
			// Token: 0x04002788 RID: 10120
			MenuPurchaseHistory,
			// Token: 0x04002789 RID: 10121
			MenuHelpUI,
			// Token: 0x0400278A RID: 10122
			PresentUI,
			// Token: 0x0400278B RID: 10123
			StaminaShopUI,
			// Token: 0x0400278C RID: 10124
			GemShopUI,
			// Token: 0x0400278D RID: 10125
			RewardUI,
			// Token: 0x0400278E RID: 10126
			LoginBonusUI,
			// Token: 0x0400278F RID: 10127
			FilterUI,
			// Token: 0x04002790 RID: 10128
			ScrollFilterUI,
			// Token: 0x04002791 RID: 10129
			SortUI,
			// Token: 0x04002792 RID: 10130
			InfoUI,
			// Token: 0x04002793 RID: 10131
			Num
		}
	}
}
