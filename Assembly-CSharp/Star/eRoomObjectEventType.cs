﻿using System;

namespace Star
{
	// Token: 0x020001F7 RID: 503
	public enum eRoomObjectEventType
	{
		// Token: 0x04000C28 RID: 3112
		None = -1,
		// Token: 0x04000C29 RID: 3113
		Lift,
		// Token: 0x04000C2A RID: 3114
		Sit,
		// Token: 0x04000C2B RID: 3115
		Sleep,
		// Token: 0x04000C2C RID: 3116
		PlayAnime,
		// Token: 0x04000C2D RID: 3117
		TriggerChange,
		// Token: 0x04000C2E RID: 3118
		SitBook,
		// Token: 0x04000C2F RID: 3119
		WallLook,
		// Token: 0x04000C30 RID: 3120
		PCPlay,
		// Token: 0x04000C31 RID: 3121
		Water,
		// Token: 0x04000C32 RID: 3122
		Machine,
		// Token: 0x04000C33 RID: 3123
		Coffee,
		// Token: 0x04000C34 RID: 3124
		TeacherDesk,
		// Token: 0x04000C35 RID: 3125
		WhiteBoard,
		// Token: 0x04000C36 RID: 3126
		Humidifier,
		// Token: 0x04000C37 RID: 3127
		StandLight,
		// Token: 0x04000C38 RID: 3128
		FishBowl,
		// Token: 0x04000C39 RID: 3129
		DeskSit,
		// Token: 0x04000C3A RID: 3130
		MFP,
		// Token: 0x04000C3B RID: 3131
		WaterSever,
		// Token: 0x04000C3C RID: 3132
		PraWater,
		// Token: 0x04000C3D RID: 3133
		BookShelf,
		// Token: 0x04000C3E RID: 3134
		Killme_A,
		// Token: 0x04000C3F RID: 3135
		XmasCake_A,
		// Token: 0x04000C40 RID: 3136
		XmasTree_A,
		// Token: 0x04000C41 RID: 3137
		PreDeskSit
	}
}
