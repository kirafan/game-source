﻿using System;

namespace Star
{
	// Token: 0x0200049E RID: 1182
	public class TitleState : GameStateBase
	{
		// Token: 0x06001732 RID: 5938 RVA: 0x000789FF File Offset: 0x00076DFF
		public TitleState(TitleMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001733 RID: 5939 RVA: 0x00078A0E File Offset: 0x00076E0E
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x00078A11 File Offset: 0x00076E11
		public override void OnStateEnter()
		{
		}

		// Token: 0x06001735 RID: 5941 RVA: 0x00078A13 File Offset: 0x00076E13
		public override void OnStateExit()
		{
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x00078A15 File Offset: 0x00076E15
		public override void OnDispose()
		{
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x00078A17 File Offset: 0x00076E17
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x00078A1A File Offset: 0x00076E1A
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001DE5 RID: 7653
		protected TitleMain m_Owner;
	}
}
