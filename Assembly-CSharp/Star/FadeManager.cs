﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000643 RID: 1603
	public class FadeManager : SingletonMonoBehaviour<FadeManager>
	{
		// Token: 0x06001FBF RID: 8127 RVA: 0x000ABEBD File Offset: 0x000AA2BD
		public void SetColor(Color color)
		{
			this.m_CurrentColor = color;
			this.m_Fade.SetColor(color);
		}

		// Token: 0x06001FC0 RID: 8128 RVA: 0x000ABED2 File Offset: 0x000AA2D2
		public Color GetColor()
		{
			return this.m_Fade.GetColor();
		}

		// Token: 0x06001FC1 RID: 8129 RVA: 0x000ABEDF File Offset: 0x000AA2DF
		public void FadeIn(float time = -1f, Action onCompleteFunc = null)
		{
			this.FadeIn(Color.black, time, onCompleteFunc);
		}

		// Token: 0x06001FC2 RID: 8130 RVA: 0x000ABEEE File Offset: 0x000AA2EE
		public void FadeInFromCurrentColor(float time = -1f, Action onCompleteFunc = null)
		{
			this.FadeIn(this.m_CurrentColor, time, onCompleteFunc);
		}

		// Token: 0x06001FC3 RID: 8131 RVA: 0x000ABEFE File Offset: 0x000AA2FE
		public void FadeIn(Color color, float time = -1f, Action onCompleteFunc = null)
		{
			if (time < 0f)
			{
				time = this.m_DefaultFadeInTime;
			}
			this.SetColor(color);
			this.m_Fade.FadeIn(time, onCompleteFunc);
			this.m_BackGround.SetActive(true);
		}

		// Token: 0x06001FC4 RID: 8132 RVA: 0x000ABF34 File Offset: 0x000AA334
		public void FadeOut(float time = -1f, Action onCompleteFunc = null)
		{
			this.FadeOut(Color.black, time, onCompleteFunc);
		}

		// Token: 0x06001FC5 RID: 8133 RVA: 0x000ABF43 File Offset: 0x000AA343
		public void FadeOutFromCurrentColor(float time = -1f, Action onCompleteFunc = null)
		{
			this.FadeOut(this.m_CurrentColor, time, onCompleteFunc);
		}

		// Token: 0x06001FC6 RID: 8134 RVA: 0x000ABF53 File Offset: 0x000AA353
		public void FadeOut(Color color, float time = -1f, Action onCompleteFunc = null)
		{
			if (time < 0f)
			{
				time = this.m_DefaultFadeOutTime;
			}
			this.SetColor(color);
			this.m_Fade.FadeOut(time, onCompleteFunc);
			this.m_BackGround.SetActive(true);
		}

		// Token: 0x06001FC7 RID: 8135 RVA: 0x000ABF89 File Offset: 0x000AA389
		private void Update()
		{
			if (!this.IsEnableRender())
			{
				this.m_BackGround.SetActive(false);
			}
		}

		// Token: 0x06001FC8 RID: 8136 RVA: 0x000ABFA2 File Offset: 0x000AA3A2
		public bool IsEnableRender()
		{
			return this.m_Fade.IsEnableRender();
		}

		// Token: 0x06001FC9 RID: 8137 RVA: 0x000ABFAF File Offset: 0x000AA3AF
		public bool IsComplete()
		{
			return this.m_Fade.IsComplete();
		}

		// Token: 0x06001FCA RID: 8138 RVA: 0x000ABFBC File Offset: 0x000AA3BC
		public void SetFadeRatio(float ratio)
		{
			this.m_Fade.SetFadeRatio(ratio);
		}

		// Token: 0x0400263A RID: 9786
		[SerializeField]
		private Fade m_Fade;

		// Token: 0x0400263B RID: 9787
		[SerializeField]
		private float m_DefaultFadeOutTime = 0.25f;

		// Token: 0x0400263C RID: 9788
		[SerializeField]
		private float m_DefaultFadeInTime = 0.25f;

		// Token: 0x0400263D RID: 9789
		[SerializeField]
		private GameObject m_BackGround;

		// Token: 0x0400263E RID: 9790
		private Color m_CurrentColor = Color.black;
	}
}
