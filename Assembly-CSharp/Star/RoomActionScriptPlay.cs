﻿using System;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x0200055E RID: 1374
	public class RoomActionScriptPlay
	{
		// Token: 0x06001AEA RID: 6890 RVA: 0x0008B820 File Offset: 0x00089C20
		public void SetUpPlayScene(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, int fscriptno, RoomActionScriptPlay.ActionSciptCallBack pcallback, RoomPartsActionPakage ppartsplay)
		{
			this.m_Target = roomObjHndl;
			this.m_Base = pOwner;
			this.m_Owner = pOwner.GetHandle();
			this.m_BaseMdlConfig = this.m_Target.GetSortKey();
			this.m_ChrMdlConfig = this.m_Base.GetSortKey();
			this.m_BackUpPos = pOwner.CacheTransform.position;
			this.m_ActionScript = RoomActionDataPakage.GetActionScript(fscriptno);
			this.m_PlayIndex = 0;
			this.m_PlayTime = 0f;
			this.m_OverlapMessgeID = 0;
			this.m_AnimeMap = this.m_Base.GetAnimeMap();
			this.m_AnimePlay = this.m_Owner.CharaAnim;
			if (this.m_ActionScript != null && this.m_ActionScript.m_Table != null)
			{
				this.m_PlayTableMax = this.m_ActionScript.m_Table.Length;
			}
			else
			{
				this.m_ActionScript = null;
			}
			this.m_LocalObject = new RoomActionScriptPlay.LocalObjectState[6];
			this.m_LinkObject = new RoomActionScriptPlay.LocalLinkObjectState[2];
			this.m_TrsObject = new RoomActionScriptPlay.LocalLinkObjectState[6];
			this.m_PartsAction = ppartsplay;
			this.m_Callback = pcallback;
		}

		// Token: 0x06001AEB RID: 6891 RVA: 0x0008B930 File Offset: 0x00089D30
		public void SetUpPlayScene(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup, RoomActionScriptPlay.ActionSciptCallBack pcallback, RoomPartsActionPakage ppartsplay)
		{
			this.m_Target = roomObjHndl;
			this.m_Base = pOwner;
			this.m_Owner = pOwner.GetHandle();
			this.m_BaseMdlConfig = this.m_Target.GetSortKey();
			this.m_ChrMdlConfig = this.m_Base.GetSortKey();
			this.m_BackUpPos = pOwner.CacheTransform.position;
			this.m_ActionScript = RoomActionDataPakage.GetActionScript(psetup.m_Table[0]);
			this.m_PlayIndex = 0;
			this.m_PlayTime = 0f;
			this.m_OverlapMessgeID = 0;
			if (psetup.m_Table.Length >= 2)
			{
				this.m_OverlapMessgeID = psetup.m_Table[1];
			}
			this.m_AnimeMap = this.m_Base.GetAnimeMap();
			this.m_AnimePlay = this.m_Owner.CharaAnim;
			if (this.m_ActionScript != null && this.m_ActionScript.m_Table != null)
			{
				this.m_PlayTableMax = this.m_ActionScript.m_Table.Length;
			}
			else
			{
				this.m_ActionScript = null;
			}
			this.m_LocalObject = new RoomActionScriptPlay.LocalObjectState[6];
			this.m_LinkObject = new RoomActionScriptPlay.LocalLinkObjectState[2];
			this.m_TrsObject = new RoomActionScriptPlay.LocalLinkObjectState[6];
			this.m_PartsAction = ppartsplay;
			this.m_Callback = pcallback;
		}

		// Token: 0x06001AEC RID: 6892 RVA: 0x0008BA64 File Offset: 0x00089E64
		public bool PlayScene(RoomObjectCtrlChara hbase)
		{
			bool result = true;
			this.m_MoveKeyTime -= Time.deltaTime;
			if (this.m_MoveKeyTime < 0f)
			{
				this.m_MoveKeyTime = 0f;
			}
			if (this.m_ActionScript != null)
			{
				float num = this.m_PlayTime;
				bool flag = true;
				bool flag2 = this.m_PlayIndex < this.m_PlayTableMax;
				while (flag2)
				{
					if (num < this.m_ActionScript.m_Table[this.m_PlayIndex].m_Time)
					{
						break;
					}
					switch (this.m_ActionScript.m_Table[this.m_PlayIndex].m_Command)
					{
					case eRoomScriptCommand.AnimePlay:
					{
						CActScriptKeyPlayAnim cactScriptKeyPlayAnim = (CActScriptKeyPlayAnim)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.m_AnimeStep = cactScriptKeyPlayAnim.m_PlayAnimNo;
						this.m_AnimeMap.SetMarkingGroup(cactScriptKeyPlayAnim.m_Category);
						string animeGroup = this.m_AnimeMap.GetAnimeGroup(this.m_AnimeStep, this.m_Base.GetDir());
						this.m_AnimePlay.PlayAnim(animeGroup, cactScriptKeyPlayAnim.m_Loop, 0f, 0f);
						this.m_AnimeTime = this.m_AnimePlay.GetMaxSec(animeGroup, 0);
						break;
					}
					case eRoomScriptCommand.Wait:
					{
						CActScriptKeyWait cactScriptKeyWait = (CActScriptKeyWait)this.m_ActionScript.m_Table[this.m_PlayIndex];
						if (cactScriptKeyWait.m_WaitType == CActScriptKeyWait.eWait.Move)
						{
							if (this.m_MoveKeyTime > 0f)
							{
								flag2 = false;
								flag = false;
							}
						}
						else if (cactScriptKeyWait.m_WaitType == CActScriptKeyWait.eWait.Popup && this.m_PartsAction.GetAction(typeof(RoomPartsPopUp)) != null)
						{
							flag2 = false;
							flag = false;
						}
						break;
					}
					case eRoomScriptCommand.AnimeWait:
					{
						CActScriptKeyAnimeWait cactScriptKeyAnimeWait = (CActScriptKeyAnimeWait)this.m_ActionScript.m_Table[this.m_PlayIndex];
						string animeGroup = this.m_AnimeMap.GetAnimeGroup(this.m_AnimeStep, this.m_Base.GetDir());
						float num2 = this.m_AnimeTime + cactScriptKeyAnimeWait.m_OffsetTime;
						if (this.m_AnimePlay.GetPlayingSec(animeGroup, 0) < num2)
						{
							flag2 = false;
							flag = false;
						}
						break;
					}
					case eRoomScriptCommand.Bind:
					{
						CActScriptKeyBind cactScriptKeyBind = (CActScriptKeyBind)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.MakeCalcBind(ref cactScriptKeyBind);
						break;
					}
					case eRoomScriptCommand.NonBind:
					{
						CActScriptKeyLinkCut cactScriptKeyLinkCut = (CActScriptKeyLinkCut)this.m_ActionScript.m_Table[this.m_PlayIndex];
						IRoomPartsAction action = this.m_PartsAction.GetAction(cactScriptKeyLinkCut.m_CRCKey);
						if (action != null)
						{
							if (cactScriptKeyLinkCut.m_LinkTime == 0f)
							{
								action.m_Active = false;
							}
							else
							{
								((RoomPartsBind)action).ReverseOffsetAnime(cactScriptKeyLinkCut.m_LinkTime);
							}
							this.SetLinkTargetObject(null, cactScriptKeyLinkCut.m_CRCKey, false);
						}
						break;
					}
					case eRoomScriptCommand.ViewMode:
					{
						CActScriptKeyViewMode cactScriptKeyViewMode = (CActScriptKeyViewMode)this.m_ActionScript.m_Table[this.m_PlayIndex];
						Transform transform = RoomUtility.FindStrTransform(this.m_Target.CacheTransform, cactScriptKeyViewMode.m_TargetName, 100);
						if (transform != null)
						{
							transform.gameObject.SetActive(cactScriptKeyViewMode.m_View);
							this.SetViewTargetObject(transform, cactScriptKeyViewMode.m_CRCKey, cactScriptKeyViewMode.m_View, false);
						}
						break;
					}
					case eRoomScriptCommand.Move:
					{
						CActScriptKeyMove cactScriptKeyMove = (CActScriptKeyMove)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.MakeMove(ref cactScriptKeyMove);
						break;
					}
					case eRoomScriptCommand.PopupMsg:
					{
						CActScriptKeyPopUp cactScriptKeyPopUp = (CActScriptKeyPopUp)this.m_ActionScript.m_Table[this.m_PlayIndex];
						int num3 = cactScriptKeyPopUp.m_PopUpID;
						if (this.m_OverlapMessgeID > 0)
						{
							num3 = this.m_OverlapMessgeID;
						}
						RoomCharaHud uihud = this.m_Base.GetUIHud();
						if (uihud != null && RoomActionScriptPlay.IsPopMessage(this.m_Owner.CharaResource.TweetListDB, num3))
						{
							TweetListDB_Param param = this.m_Owner.CharaResource.TweetListDB.GetParam(num3);
							uihud.GetCharaTweet().Open(param.m_Text);
							int fvoicelink = -1;
							if (param.m_VoiceCueName != null && param.m_VoiceCueName.Length > 1)
							{
								fvoicelink = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_Owner.CharaID).m_NamedType, param.m_VoiceCueName, false);
							}
							RoomPartsPopUp paction = new RoomPartsPopUp(uihud, cactScriptKeyPopUp.m_PopUpTime, fvoicelink);
							this.m_PartsAction.EntryAction(paction, 0U);
						}
						break;
					}
					case eRoomScriptCommand.SePlay:
					{
						CActScriptKeySE cactScriptKeySE = (CActScriptKeySE)this.m_ActionScript.m_Table[this.m_PlayIndex];
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE((eSoundSeListDB)cactScriptKeySE.m_SE, 1f, 0, -1, -1);
						break;
					}
					case eRoomScriptCommand.Jump:
					{
						CActScriptKeyJump cactScriptKeyJump = (CActScriptKeyJump)this.m_ActionScript.m_Table[this.m_PlayIndex];
						int num4 = UnityEngine.Random.Range(0, 100);
						if (cactScriptKeyJump.m_Randam <= num4)
						{
							this.m_PlayIndex += cactScriptKeyJump.m_Offset - 1;
							if (cactScriptKeyJump.m_FrameReset)
							{
								num = (this.m_PlayTime = this.m_ActionScript.m_Table[this.m_PlayIndex + 1].m_Time);
							}
						}
						break;
					}
					case eRoomScriptCommand.ProgramEvent:
						if (this.m_Callback != null && !this.m_Callback((CActScriptKeyProgramEvent)this.m_ActionScript.m_Table[this.m_PlayIndex]))
						{
							flag2 = false;
							flag = false;
						}
						break;
					case eRoomScriptCommand.ObjectDir:
					{
						CActScriptKeyObjectDir cactScriptKeyObjectDir = (CActScriptKeyObjectDir)this.m_ActionScript.m_Table[this.m_PlayIndex];
						switch (cactScriptKeyObjectDir.m_Func)
						{
						case CActScriptKeyObjectDir.eDirFunc.TargetDir:
							if (this.m_Base.trs.position.x >= this.m_Target.trs.position.x)
							{
								this.m_Base.SetDir(CharacterDefine.eDir.L);
							}
							else
							{
								this.m_Base.SetDir(CharacterDefine.eDir.R);
							}
							break;
						case CActScriptKeyObjectDir.eDirFunc.TargetRevDir:
							if (this.m_Base.trs.position.x >= this.m_Target.trs.position.x)
							{
								this.m_Base.SetDir(CharacterDefine.eDir.R);
							}
							else
							{
								this.m_Base.SetDir(CharacterDefine.eDir.L);
							}
							break;
						case CActScriptKeyObjectDir.eDirFunc.Left:
							this.m_Base.SetDir(CharacterDefine.eDir.L);
							break;
						case CActScriptKeyObjectDir.eDirFunc.Right:
							this.m_Base.SetDir(CharacterDefine.eDir.R);
							break;
						}
						break;
					}
					case eRoomScriptCommand.BaseTrs:
					{
						CActScriptKeyBaseTrs cactScriptKeyBaseTrs = (CActScriptKeyBaseTrs)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.SetUpObjectTrs(ref cactScriptKeyBaseTrs);
						break;
					}
					case eRoomScriptCommand.PosAnime:
					{
						CActScriptKeyPosAnime cactScriptKeyPosAnime = (CActScriptKeyPosAnime)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.SetUpObjectPosAnime(ref cactScriptKeyPosAnime);
						break;
					}
					case eRoomScriptCommand.RotAnime:
					{
						CActScriptKeyRotAnime cactScriptKeyRotAnime = (CActScriptKeyRotAnime)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.SetUpObjectRotAnime(ref cactScriptKeyRotAnime);
						break;
					}
					case eRoomScriptCommand.BindRot:
					{
						CActScriptKeyBindRot cactScriptKeyBindRot = (CActScriptKeyBindRot)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.SetUpObjectBindRot(ref cactScriptKeyBindRot);
						break;
					}
					case eRoomScriptCommand.FrameChange:
					{
						CActScriptKeyFrameChg cactScriptKeyFrameChg = (CActScriptKeyFrameChg)this.m_ActionScript.m_Table[this.m_PlayIndex];
						num = (this.m_PlayTime = cactScriptKeyFrameChg.m_Frame);
						break;
					}
					case eRoomScriptCommand.CharaViewMode:
					{
						CActScriptKeyCharaViewMode cactScriptKeyCharaViewMode = (CActScriptKeyCharaViewMode)this.m_ActionScript.m_Table[this.m_PlayIndex];
						Transform transform2 = RoomUtility.FindStrTransform(this.m_Base.CacheTransform, cactScriptKeyCharaViewMode.m_TargetName, 100);
						if (transform2 != null)
						{
							transform2.gameObject.SetActive(cactScriptKeyCharaViewMode.m_View);
							this.SetViewTargetObject(transform2, cactScriptKeyCharaViewMode.m_CRCKey, cactScriptKeyCharaViewMode.m_View, true);
						}
						break;
					}
					case eRoomScriptCommand.ObjAnime:
					{
						CActScriptKeyObjPlayAnim cactScriptKeyObjPlayAnim = (CActScriptKeyObjPlayAnim)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.m_Target.PlayMotion(cactScriptKeyObjPlayAnim.m_AnimeKey, cactScriptKeyObjPlayAnim.m_Loop, 1f);
						break;
					}
					case eRoomScriptCommand.CharaCfg:
					{
						CActScriptKeyCharaCfg cactScriptKeyCharaCfg = (CActScriptKeyCharaCfg)this.m_ActionScript.m_Table[this.m_PlayIndex];
						this.m_Base.SetOffsetZ(cactScriptKeyCharaCfg.m_OffsetZ);
						this.m_Base.SetSortKey(null, cactScriptKeyCharaCfg.m_OffsetLayer);
						break;
					}
					case eRoomScriptCommand.CueSe:
					{
						CActScriptKeyCueSe cactScriptKeyCueSe = (CActScriptKeyCueSe)this.m_ActionScript.m_Table[this.m_PlayIndex];
						CriAtomExAcb acb = CriAtom.GetAcb("Room");
						if (acb != null && acb.Exists(cactScriptKeyCueSe.m_CueName))
						{
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE("Room", cactScriptKeyCueSe.m_CueName, cactScriptKeyCueSe.m_Volume, 0, -1, -1);
						}
						break;
					}
					case eRoomScriptCommand.ObjCfg:
					{
						CActScriptKeyObjCfg cactScriptKeyObjCfg = (CActScriptKeyObjCfg)this.m_ActionScript.m_Table[this.m_PlayIndex];
						Transform transform3 = RoomUtility.FindStrTransform(this.m_Target.transform, cactScriptKeyObjCfg.m_TargetName, 100);
						if (transform3 != null)
						{
							Vector3 localPosition = transform3.localPosition;
							localPosition.z = cactScriptKeyObjCfg.m_OffsetZ;
							transform3.localPosition = localPosition;
						}
						this.m_Target.SetSortKey(cactScriptKeyObjCfg.m_TargetName, cactScriptKeyObjCfg.m_OffsetLayer);
						break;
					}
					}
					if (flag)
					{
						this.m_PlayIndex++;
						if (this.m_PlayIndex >= this.m_PlayTableMax)
						{
							flag2 = false;
						}
					}
				}
				if (flag)
				{
					this.m_PlayTime += Time.deltaTime;
				}
				if (this.m_PlayIndex >= this.m_PlayTableMax)
				{
					result = false;
					if (this.m_ChrMdlConfig != null)
					{
						this.m_Base.SetSortKey(this.m_ChrMdlConfig);
						this.m_ChrMdlConfig = null;
					}
					if (this.m_BaseMdlConfig != null)
					{
						this.m_Target.SetSortKey(this.m_BaseMdlConfig);
						this.m_BaseMdlConfig = null;
					}
				}
			}
			else
			{
				result = false;
				if (this.m_ChrMdlConfig != null)
				{
					this.m_Base.SetSortKey(this.m_ChrMdlConfig);
					this.m_ChrMdlConfig = null;
				}
				if (this.m_BaseMdlConfig != null)
				{
					this.m_Target.SetSortKey(this.m_BaseMdlConfig);
					this.m_BaseMdlConfig = null;
				}
			}
			return result;
		}

		// Token: 0x06001AED RID: 6893 RVA: 0x0008C4C4 File Offset: 0x0008A8C4
		private void MakeMove(ref CActScriptKeyMove pmovedat)
		{
			switch (pmovedat.m_Func)
			{
			case CActScriptKeyMove.eMoveType.TargetTime:
			{
				this.m_AnimeMap.SetMarkingGroup(RoomDefine.eAnim.Walk);
				Vector3 position = this.m_Target.CacheTransform.position;
				if (this.m_Base.trs.position.x > this.m_Target.trs.position.x)
				{
					position.x += pmovedat.m_Offset.x;
					position.y += pmovedat.m_Offset.y;
					this.m_Base.SetDir(CharacterDefine.eDir.L);
				}
				else
				{
					position.x -= pmovedat.m_Offset.x;
					position.y += pmovedat.m_Offset.y;
					this.m_Base.SetDir(CharacterDefine.eDir.R);
				}
				this.m_AnimePlay.PlayAnim(this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir()), WrapMode.Loop, 0f, 0f);
				this.m_MoveKeyTime = pmovedat.m_MoveTime;
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, this.m_MoveKeyTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 10U);
				break;
			}
			case CActScriptKeyMove.eMoveType.TargetSpeed:
			{
				this.m_AnimeMap.SetMarkingGroup(RoomDefine.eAnim.Walk);
				this.m_AnimePlay.SetBlink(true);
				Vector3 position = this.m_Target.CacheTransform.position;
				if (this.m_Base.trs.position.x > this.m_Target.trs.position.x)
				{
					position.x += pmovedat.m_Offset.x;
					position.y += pmovedat.m_Offset.y;
					this.m_Base.SetDir(CharacterDefine.eDir.L);
				}
				else
				{
					position.x -= pmovedat.m_Offset.x;
					position.y += pmovedat.m_Offset.y;
					this.m_Base.SetDir(CharacterDefine.eDir.R);
				}
				this.m_AnimePlay.PlayAnim(this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir()), WrapMode.Loop, 0f, 0f);
				this.m_MoveKeyTime = RoomUtility.GetCharaMoveTime(position, this.m_Owner.CacheTransform.position);
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, this.m_MoveKeyTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				break;
			}
			case CActScriptKeyMove.eMoveType.BaseTime:
			{
				Vector3 position = this.m_Owner.CacheTransform.position;
				position.x += pmovedat.m_Offset.x;
				position.y += pmovedat.m_Offset.y;
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, pmovedat.m_MoveTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				break;
			}
			case CActScriptKeyMove.eMoveType.BaseSpeed:
			{
				Vector3 position = this.m_Owner.CacheTransform.position;
				position.x += pmovedat.m_Offset.x;
				position.y += pmovedat.m_Offset.y;
				this.m_MoveKeyTime = RoomUtility.GetCharaMoveTime(position, this.m_Owner.CacheTransform.position);
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, this.m_MoveKeyTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				break;
			}
			case CActScriptKeyMove.eMoveType.BasePos:
			{
				Vector3 position = this.m_Owner.CacheTransform.position;
				this.m_AnimeMap.SetMarkingGroup(RoomDefine.eAnim.Walk);
				this.m_AnimePlay.SetBlink(true);
				if (position.x >= this.m_BackUpPos.x)
				{
					this.m_Base.SetDir(CharacterDefine.eDir.L);
				}
				else
				{
					this.m_Base.SetDir(CharacterDefine.eDir.R);
				}
				this.m_AnimePlay.PlayAnim(this.m_AnimeMap.GetAnimeGroup(0, this.m_Base.GetDir()), WrapMode.Loop, 0f, 0f);
				this.m_MoveKeyTime = RoomUtility.GetCharaMoveTime(this.m_BackUpPos, this.m_Owner.CacheTransform.position);
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, this.m_BackUpPos, this.m_MoveKeyTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				break;
			}
			case CActScriptKeyMove.eMoveType.LocTarget:
			{
				Transform transform = this.CalcTargetTrs();
				this.m_MoveKeyTime = pmovedat.m_MoveTime;
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, transform.position, this.m_MoveKeyTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				break;
			}
			}
		}

		// Token: 0x06001AEE RID: 6894 RVA: 0x0008CA14 File Offset: 0x0008AE14
		private void MakeCalcBind(ref CActScriptKeyBind pbinddat)
		{
			Transform transform = RoomUtility.FindStrTransform(this.m_Owner.CacheTransform, this.GetOwnerToTargetName(pbinddat.m_BindHrcName, pbinddat.m_Attach), 100);
			if (transform != null)
			{
				Transform transform2 = RoomUtility.FindStrTransform(this.m_Target.CacheTransform, pbinddat.m_TargetName, 20);
				if (transform2 != null)
				{
					RoomPartsBind roomPartsBind = null;
					switch (pbinddat.m_CalcType)
					{
					case CActScriptKeyBind.eCalc.Chara:
						roomPartsBind = new RoomPartsBind(transform, transform2, RoomUtility.CalcObjectChildDir(this.m_Target, this.m_Base));
						break;
					case CActScriptKeyBind.eCalc.Non:
						roomPartsBind = new RoomPartsBind(transform, transform2);
						break;
					case CActScriptKeyBind.eCalc.LinkOffset:
					{
						roomPartsBind = new RoomPartsBind(transform, transform2);
						Vector3 vector = transform2.position - transform.position;
						roomPartsBind.SetOffsetAnime(vector, vector, 0f);
						break;
					}
					case CActScriptKeyBind.eCalc.ChrTrs:
						roomPartsBind = new RoomPartsBind(transform, transform2, RoomUtility.CalcObjectChildDir(this.m_Target, this.m_Base));
						roomPartsBind.SetRotationOffset(transform.rotation);
						break;
					case CActScriptKeyBind.eCalc.LinkOffTrs:
					{
						roomPartsBind = new RoomPartsBind(transform, transform2);
						Vector3 vector = transform2.position - transform.position;
						roomPartsBind.SetOffsetAnime(vector, vector, 0f);
						roomPartsBind.SetRotationOffset(transform.rotation);
						break;
					}
					}
					if (roomPartsBind != null)
					{
						this.m_PartsAction.EntryAction(roomPartsBind, pbinddat.m_CRCKey);
						this.SetLinkTargetObject(transform2, pbinddat.m_CRCKey, true);
					}
				}
			}
		}

		// Token: 0x06001AEF RID: 6895 RVA: 0x0008CB88 File Offset: 0x0008AF88
		public void SetUpObjectTrs(ref CActScriptKeyBaseTrs pbasetrs)
		{
			Transform transform = RoomUtility.FindStrTransform(this.m_Target.CacheTransform, pbasetrs.m_TargetName, 20);
			if (transform != null)
			{
				this.SetTrsTargetObject(transform, pbasetrs.m_CRCKey, true);
				Vector3 localPosition = transform.localPosition;
				Vector3 eulerAngles = transform.localRotation.eulerAngles;
				Vector3 position = this.m_Owner.CacheTransform.position;
				Vector3 position2 = this.m_Target.CacheTransform.position;
				if (position.x >= position2.x)
				{
					localPosition.x = pbasetrs.m_Pos.x;
					localPosition.y = pbasetrs.m_Pos.y;
					transform.localRotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, pbasetrs.m_Rot);
				}
				else
				{
					localPosition.x = -pbasetrs.m_Pos.x;
					localPosition.y = pbasetrs.m_Pos.y;
					transform.localRotation = Quaternion.Euler(eulerAngles.x, eulerAngles.y, -pbasetrs.m_Rot);
				}
				transform.localPosition = localPosition;
			}
		}

		// Token: 0x06001AF0 RID: 6896 RVA: 0x0008CCB2 File Offset: 0x0008B0B2
		public void SetUpObjectPosAnime(ref CActScriptKeyPosAnime pposanime)
		{
		}

		// Token: 0x06001AF1 RID: 6897 RVA: 0x0008CCB4 File Offset: 0x0008B0B4
		public void SetUpObjectRotAnime(ref CActScriptKeyRotAnime protanime)
		{
			Transform transform = RoomUtility.FindStrTransform(this.m_Target.CacheTransform, protanime.m_TargetName, 20);
			if (transform != null)
			{
				this.SetTrsTargetObject(transform, protanime.m_CRCKey, true);
				Vector3 zero = Vector3.zero;
				Vector3 position = this.m_Owner.CacheTransform.position;
				Vector3 position2 = this.m_Target.CacheTransform.position;
				CharacterDefine.eDir dir = this.m_Target.GetDir();
				if (this.m_Target.BlockData.SizeX == 1 && this.m_Target.BlockData.SizeY == 1)
				{
					if (position.x >= position2.x)
					{
						if (dir == CharacterDefine.eDir.R)
						{
							zero.z = -protanime.m_Rot;
						}
						else
						{
							zero.z = protanime.m_Rot;
						}
					}
					else if (dir == CharacterDefine.eDir.R)
					{
						zero.z = protanime.m_Rot;
					}
					else
					{
						zero.z = -protanime.m_Rot;
					}
				}
				else
				{
					zero.z = protanime.m_Rot;
				}
				RoomPartsRotAnime paction = new RoomPartsRotAnime(transform, transform.localRotation, Quaternion.Euler(zero), protanime.m_Frame);
				this.m_PartsAction.EntryAction(paction, 0U);
			}
		}

		// Token: 0x06001AF2 RID: 6898 RVA: 0x0008CDFE File Offset: 0x0008B1FE
		public void SetUpObjectBindRot(ref CActScriptKeyBindRot pbindrot)
		{
		}

		// Token: 0x06001AF3 RID: 6899 RVA: 0x0008CE00 File Offset: 0x0008B200
		public string GetOwnerToTargetName(string fbasename, CActScriptKeyBind.eNameAttach fgetchange)
		{
			string result;
			if (fgetchange != CActScriptKeyBind.eNameAttach.R)
			{
				if (fgetchange != CActScriptKeyBind.eNameAttach.L)
				{
					result = fbasename;
				}
				else if (this.m_Base.GetDir() == CharacterDefine.eDir.R)
				{
					result = fbasename + "R";
				}
				else
				{
					result = fbasename + "L";
				}
			}
			else if (this.m_Base.GetDir() != CharacterDefine.eDir.R)
			{
				result = fbasename + "R";
			}
			else
			{
				result = fbasename + "L";
			}
			return result;
		}

		// Token: 0x06001AF4 RID: 6900 RVA: 0x0008CE90 File Offset: 0x0008B290
		public void SetViewTargetObject(Transform pentry, uint fuid, bool fentry, bool fbase)
		{
			int num = this.m_LocalObject.Length;
			if (fentry)
			{
				for (int i = 0; i < num; i++)
				{
					if (!this.m_LocalObject[i].m_Active)
					{
						this.m_LocalObject[i].m_AccessID = fuid;
						this.m_LocalObject[i].m_Active = true;
						this.m_LocalObject[i].m_Object = pentry;
						this.m_LocalObject[i].m_BaseUp = fbase;
						break;
					}
				}
			}
			else
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_LocalObject[i].m_AccessID == fuid)
					{
						this.m_LocalObject[i].m_AccessID = 0U;
						this.m_LocalObject[i].m_Active = false;
						this.m_LocalObject[i].m_Object = null;
						break;
					}
				}
			}
		}

		// Token: 0x06001AF5 RID: 6901 RVA: 0x0008CF8C File Offset: 0x0008B38C
		public void SetLinkTargetObject(Transform pentry, uint fuid, bool factive)
		{
			int num = this.m_LinkObject.Length;
			if (factive)
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_LinkObject[i].m_AccessID == fuid)
					{
						return;
					}
				}
				for (int i = 0; i < num; i++)
				{
					if (this.m_LinkObject[i].m_AccessID == 0U)
					{
						this.m_LinkObject[i].m_AccessID = fuid;
						this.m_LinkObject[i].m_BackPos = pentry.localPosition;
						this.m_LinkObject[i].m_BackRot = pentry.localRotation;
						this.m_LinkObject[i].m_Object = pentry;
						break;
					}
				}
			}
			else
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_LinkObject[i].m_AccessID == fuid)
					{
						this.m_LinkObject[i].m_AccessID = 0U;
						this.m_LinkObject[i].m_Object = null;
						break;
					}
				}
			}
		}

		// Token: 0x06001AF6 RID: 6902 RVA: 0x0008D0A8 File Offset: 0x0008B4A8
		public void SetTrsTargetObject(Transform pentry, uint fuid, bool factive)
		{
			int num = this.m_TrsObject.Length;
			if (factive)
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_TrsObject[i].m_AccessID == fuid)
					{
						return;
					}
				}
				for (int i = 0; i < num; i++)
				{
					if (this.m_TrsObject[i].m_AccessID == 0U)
					{
						this.m_TrsObject[i].m_AccessID = fuid;
						this.m_TrsObject[i].m_BackPos = pentry.localPosition;
						this.m_TrsObject[i].m_BackRot = pentry.localRotation;
						this.m_TrsObject[i].m_Object = pentry;
						break;
					}
				}
			}
			else
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_TrsObject[i].m_AccessID == fuid)
					{
						this.m_TrsObject[i].m_AccessID = 0U;
						this.m_TrsObject[i].m_Object = null;
						break;
					}
				}
			}
		}

		// Token: 0x06001AF7 RID: 6903 RVA: 0x0008D1C4 File Offset: 0x0008B5C4
		public void CancelPlay()
		{
			int num = this.m_LocalObject.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_LocalObject[i].m_Object != null)
				{
					this.m_LocalObject[i].m_Object.gameObject.SetActive(this.m_LocalObject[i].m_BaseUp);
				}
			}
			num = this.m_TrsObject.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_TrsObject[i].m_Object != null)
				{
					this.m_TrsObject[i].m_Object.localPosition = this.m_TrsObject[i].m_BackPos;
					this.m_TrsObject[i].m_Object.localRotation = this.m_TrsObject[i].m_BackRot;
					this.m_TrsObject[i].m_Object = null;
				}
			}
			num = this.m_LinkObject.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_LinkObject[i].m_Object != null)
				{
					this.m_LinkObject[i].m_Object.localPosition = this.m_LinkObject[i].m_BackPos;
					this.m_LinkObject[i].m_Object.localRotation = this.m_LinkObject[i].m_BackRot;
					this.m_LinkObject[i].m_Object = null;
				}
			}
			this.m_Owner.CacheTransform.position = this.m_BackUpPos;
			if (this.m_ChrMdlConfig != null)
			{
				this.m_Base.SetSortKey(this.m_ChrMdlConfig);
				this.m_ChrMdlConfig = null;
			}
			if (this.m_BaseMdlConfig != null)
			{
				this.m_Target.SetSortKey(this.m_BaseMdlConfig);
				this.m_BaseMdlConfig = null;
			}
		}

		// Token: 0x06001AF8 RID: 6904 RVA: 0x0008D3C0 File Offset: 0x0008B7C0
		protected Transform CalcTargetTrs()
		{
			Vector2 gridPos = this.m_Base.GetGridPos();
			Transform transform = this.m_Target.GetAttachTransform(gridPos);
			if (transform == null)
			{
				transform = this.m_Target.CacheTransform;
			}
			return transform;
		}

		// Token: 0x06001AF9 RID: 6905 RVA: 0x0008D400 File Offset: 0x0008B800
		public static bool IsPopMessage(TweetListDB pdb, int fid)
		{
			for (int i = 0; i < pdb.m_Params.Length; i++)
			{
				if (pdb.m_Params[i].m_ID == fid)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001AFA RID: 6906 RVA: 0x0008D440 File Offset: 0x0008B840
		public void ReleasePlay()
		{
			int num = this.m_LocalObject.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_LocalObject[i].m_Object = null;
			}
			num = this.m_LinkObject.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_LinkObject[i].m_Object = null;
			}
			this.m_LocalObject = null;
			this.m_LinkObject = null;
			this.m_ActionScript = null;
			if (this.m_PartsAction != null)
			{
				this.m_PartsAction.Release();
			}
			this.m_PartsAction = null;
		}

		// Token: 0x040021CA RID: 8650
		protected RoomActionScriptDB m_ActionScript;

		// Token: 0x040021CB RID: 8651
		protected int m_PlayIndex;

		// Token: 0x040021CC RID: 8652
		protected int m_PlayTableMax;

		// Token: 0x040021CD RID: 8653
		protected float m_PlayTime;

		// Token: 0x040021CE RID: 8654
		protected CharacterHandler m_Owner;

		// Token: 0x040021CF RID: 8655
		protected RoomAnimAccessMap m_AnimeMap;

		// Token: 0x040021D0 RID: 8656
		protected CharacterAnim m_AnimePlay;

		// Token: 0x040021D1 RID: 8657
		protected IRoomObjectControll m_Target;

		// Token: 0x040021D2 RID: 8658
		protected RoomObjectCtrlChara m_Base;

		// Token: 0x040021D3 RID: 8659
		protected int m_AnimeStep;

		// Token: 0x040021D4 RID: 8660
		protected float m_AnimeTime;

		// Token: 0x040021D5 RID: 8661
		protected RoomPartsActionPakage m_PartsAction;

		// Token: 0x040021D6 RID: 8662
		protected float m_MoveKeyTime;

		// Token: 0x040021D7 RID: 8663
		protected Vector3 m_BackUpPos;

		// Token: 0x040021D8 RID: 8664
		protected int m_OverlapMessgeID;

		// Token: 0x040021D9 RID: 8665
		protected RoomActionScriptPlay.LocalObjectState[] m_LocalObject;

		// Token: 0x040021DA RID: 8666
		protected RoomActionScriptPlay.LocalLinkObjectState[] m_LinkObject;

		// Token: 0x040021DB RID: 8667
		protected RoomActionScriptPlay.LocalLinkObjectState[] m_TrsObject;

		// Token: 0x040021DC RID: 8668
		protected IRoomObjectControll.ModelRenderConfig[] m_BaseMdlConfig;

		// Token: 0x040021DD RID: 8669
		protected IRoomObjectControll.ModelRenderConfig[] m_ChrMdlConfig;

		// Token: 0x040021DE RID: 8670
		public RoomActionScriptPlay.ActionSciptCallBack m_Callback;

		// Token: 0x0200055F RID: 1375
		public struct LocalObjectState
		{
			// Token: 0x040021DF RID: 8671
			public Transform m_Object;

			// Token: 0x040021E0 RID: 8672
			public uint m_AccessID;

			// Token: 0x040021E1 RID: 8673
			public bool m_Active;

			// Token: 0x040021E2 RID: 8674
			public bool m_BaseUp;
		}

		// Token: 0x02000560 RID: 1376
		public struct LocalLinkObjectState
		{
			// Token: 0x040021E3 RID: 8675
			public Transform m_Object;

			// Token: 0x040021E4 RID: 8676
			public uint m_AccessID;

			// Token: 0x040021E5 RID: 8677
			public Vector3 m_BackPos;

			// Token: 0x040021E6 RID: 8678
			public Quaternion m_BackRot;
		}

		// Token: 0x02000561 RID: 1377
		// (Invoke) Token: 0x06001AFC RID: 6908
		public delegate bool ActionSciptCallBack(CActScriptKeyProgramEvent pcmd);
	}
}
