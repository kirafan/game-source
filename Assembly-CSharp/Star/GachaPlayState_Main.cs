﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000423 RID: 1059
	public class GachaPlayState_Main : GachaPlayState
	{
		// Token: 0x06001451 RID: 5201 RVA: 0x0006BF40 File Offset: 0x0006A340
		public GachaPlayState_Main(GachaPlayMain owner) : base(owner)
		{
		}

		// Token: 0x06001452 RID: 5202 RVA: 0x0006BF50 File Offset: 0x0006A350
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001453 RID: 5203 RVA: 0x0006BF53 File Offset: 0x0006A353
		public override void OnStateEnter()
		{
			this.m_Step = GachaPlayState_Main.eStep.First;
		}

		// Token: 0x06001454 RID: 5204 RVA: 0x0006BF5C File Offset: 0x0006A35C
		public override void OnStateExit()
		{
		}

		// Token: 0x06001455 RID: 5205 RVA: 0x0006BF5E File Offset: 0x0006A35E
		public override void OnDispose()
		{
		}

		// Token: 0x06001456 RID: 5206 RVA: 0x0006BF60 File Offset: 0x0006A360
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case GachaPlayState_Main.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_Step = GachaPlayState_Main.eStep.Prepare;
					eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
					if (tutoarialSeq == eTutorialSeq.GachaPlayed_NextIsGachaDecide)
					{
						List<Gacha.Result> results = SingletonMonoBehaviour<GameSystem>.Inst.Gacha.GetResults();
						if (results == null || results.Count == 0)
						{
							this.m_Step = GachaPlayState_Main.eStep.GachaLogRequest;
						}
					}
				}
				break;
			case GachaPlayState_Main.eStep.GachaLogRequest:
				SingletonMonoBehaviour<GameSystem>.Inst.Gacha.Request_GachaLogGetLatest(new Action(this.OnResponse_GachaLogGetLatest), false);
				this.m_Step = GachaPlayState_Main.eStep.GachaLogRequest_Wait;
				break;
			case GachaPlayState_Main.eStep.Prepare:
			{
				List<Gacha.Result> gachaResults;
				if (this.m_IsStartResult)
				{
					gachaResults = SingletonMonoBehaviour<GameSystem>.Inst.Gacha.GetGachaLog();
				}
				else
				{
					gachaResults = SingletonMonoBehaviour<GameSystem>.Inst.Gacha.GetResults();
				}
				this.m_Owner.GachaPlayScene = new GachaPlayScene();
				this.m_Owner.GachaPlayScene.Prepare(gachaResults, Camera.main, this.m_IsStartResult);
				this.m_Step = GachaPlayState_Main.eStep.Prepare_Wait;
				break;
			}
			case GachaPlayState_Main.eStep.Prepare_Wait:
				if (this.m_Owner.GachaPlayScene.IsCompletePrepare())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = GachaPlayState_Main.eStep.Play;
				}
				break;
			case GachaPlayState_Main.eStep.Play:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Owner.GachaPlayScene.Play();
					this.m_Step = GachaPlayState_Main.eStep.Main;
				}
				break;
			case GachaPlayState_Main.eStep.Main:
				if (this.m_Owner.GachaPlayScene.IsCompletePlay())
				{
					eTutorialSeq tutoarialSeq2 = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
					if (tutoarialSeq2 == eTutorialSeq.GachaDecided_NextIsADVGachaAfterPlay)
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000001, SceneDefine.eSceneID.ADV, false);
					}
					else
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Gacha;
					}
					return 2147483646;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001457 RID: 5207 RVA: 0x0006C17C File Offset: 0x0006A57C
		private void OnResponse_GachaLogGetLatest()
		{
			this.m_IsStartResult = true;
			this.m_Step = GachaPlayState_Main.eStep.Prepare;
		}

		// Token: 0x04001B35 RID: 6965
		private GachaPlayState_Main.eStep m_Step = GachaPlayState_Main.eStep.None;

		// Token: 0x04001B36 RID: 6966
		private bool m_IsStartResult;

		// Token: 0x02000424 RID: 1060
		private enum eStep
		{
			// Token: 0x04001B38 RID: 6968
			None = -1,
			// Token: 0x04001B39 RID: 6969
			First,
			// Token: 0x04001B3A RID: 6970
			GachaLogRequest,
			// Token: 0x04001B3B RID: 6971
			GachaLogRequest_Wait,
			// Token: 0x04001B3C RID: 6972
			Prepare,
			// Token: 0x04001B3D RID: 6973
			Prepare_Wait,
			// Token: 0x04001B3E RID: 6974
			Play,
			// Token: 0x04001B3F RID: 6975
			Main,
			// Token: 0x04001B40 RID: 6976
			Destory,
			// Token: 0x04001B41 RID: 6977
			Destory_Wait,
			// Token: 0x04001B42 RID: 6978
			End
		}
	}
}
