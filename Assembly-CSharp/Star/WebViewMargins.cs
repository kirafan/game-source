﻿using System;

namespace Star
{
	// Token: 0x0200066A RID: 1642
	public class WebViewMargins
	{
		// Token: 0x06002140 RID: 8512 RVA: 0x000B0EF1 File Offset: 0x000AF2F1
		public WebViewMargins(int left, int top, int right, int bottom)
		{
			this.m_Left = left;
			this.m_Top = top;
			this.m_Right = right;
			this.m_Bottom = bottom;
		}

		// Token: 0x06002141 RID: 8513 RVA: 0x000B0F16 File Offset: 0x000AF316
		public WebViewMargins()
		{
		}

		// Token: 0x040027A3 RID: 10147
		public int m_Left;

		// Token: 0x040027A4 RID: 10148
		public int m_Top;

		// Token: 0x040027A5 RID: 10149
		public int m_Right;

		// Token: 0x040027A6 RID: 10150
		public int m_Bottom;
	}
}
