﻿using System;
using System.Collections;
using UnityEngine;

namespace Star
{
	// Token: 0x020006B0 RID: 1712
	public class TownMenuActTraning : ITownMenuAct
	{
		// Token: 0x06002226 RID: 8742 RVA: 0x000B5729 File Offset: 0x000B3B29
		public void SetUp(TownBuilder pbuild)
		{
			this.m_Builder = pbuild;
			this.m_CharaTable = new TownMenuActTraning.TraningCharaIcon[2];
			this.m_CharaEntryNum = 0;
			this.m_CharaEntryMax = 2;
		}

		// Token: 0x06002227 RID: 8743 RVA: 0x000B574C File Offset: 0x000B3B4C
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
			this.UpdateCharaIcon(pmenu);
		}

		// Token: 0x06002228 RID: 8744 RVA: 0x000B5758 File Offset: 0x000B3B58
		private void EntryChara(long fmanageid, TownObjHandleMenu pmenu)
		{
			if (this.m_CharaEntryNum >= this.m_CharaEntryMax)
			{
				TownMenuActTraning.TraningCharaIcon[] array = new TownMenuActTraning.TraningCharaIcon[this.m_CharaEntryMax + 2];
				for (int i = 0; i < this.m_CharaEntryMax; i++)
				{
					array[i] = this.m_CharaTable[i];
				}
				this.m_CharaTable = array;
				this.m_CharaEntryMax += 2;
			}
			this.m_CharaTable[this.m_CharaEntryNum] = new TownMenuActTraning.TraningCharaIcon();
			this.m_CharaTable[this.m_CharaEntryNum].SetUpModel(fmanageid, this.m_Builder, pmenu);
			this.m_CharaEntryNum++;
		}

		// Token: 0x06002229 RID: 8745 RVA: 0x000B57F8 File Offset: 0x000B3BF8
		private void ReleaseCharaChk()
		{
			for (int i = 0; i < this.m_CharaEntryNum; i++)
			{
				if (!this.m_CharaTable[i].m_Search)
				{
					this.m_CharaTable[i].SetReleaseStep();
				}
			}
		}

		// Token: 0x0600222A RID: 8746 RVA: 0x000B583C File Offset: 0x000B3C3C
		private void UpdateCharaIcon(TownObjHandleMenu pmenu)
		{
			bool flag = false;
			Vector3 zero = Vector3.zero;
			zero.x = (float)(this.m_CharaEntryNum - 1) * 0.75f * 0.33f;
			zero.y = -0.2475f;
			zero.z = 1f;
			for (int i = 0; i < this.m_CharaEntryNum; i++)
			{
				this.m_CharaTable[i].SetPoint(zero);
				if (!this.m_CharaTable[i].UpdateModel(this.m_Builder, pmenu))
				{
					this.m_CharaTable[i] = null;
					flag = true;
				}
				zero.x += 0.1875f;
			}
			if (flag)
			{
				int i = 0;
				while (i < this.m_CharaEntryNum)
				{
					if (this.m_CharaTable[i] == null)
					{
						for (int j = i + 1; j < this.m_CharaEntryNum; j++)
						{
							this.m_CharaTable[j - 1] = this.m_CharaTable[j];
						}
						this.m_CharaEntryNum--;
						this.m_CharaTable[this.m_CharaEntryNum] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x040028A8 RID: 10408
		private GameObject m_MenuIcon;

		// Token: 0x040028A9 RID: 10409
		private TownBuilder m_Builder;

		// Token: 0x040028AA RID: 10410
		private TownMenuActTraning.TownIconModel m_IconUp;

		// Token: 0x040028AB RID: 10411
		public TownMenuActTraning.TraningCharaIcon[] m_CharaTable;

		// Token: 0x040028AC RID: 10412
		public int m_CharaEntryNum;

		// Token: 0x040028AD RID: 10413
		public int m_CharaEntryMax;

		// Token: 0x020006B1 RID: 1713
		public class TownIconModel
		{
			// Token: 0x0600222C RID: 8748 RVA: 0x000B5960 File Offset: 0x000B3D60
			public void SetIconModel(GameObject pobj)
			{
				int num = 0;
				this.m_PlayAnime = pobj.GetComponentInChildren<Animation>();
				this.m_AnimeTable = new AnimationState[this.m_PlayAnime.GetClipCount()];
				IEnumerator enumerator = this.m_PlayAnime.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						AnimationState animationState = (AnimationState)obj;
						this.m_AnimeTable[num] = animationState;
						num++;
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				this.m_Render = pobj.GetComponentInChildren<MeshRenderer>();
				this.m_PlayAnimeNo = -1;
			}

			// Token: 0x0600222D RID: 8749 RVA: 0x000B5A04 File Offset: 0x000B3E04
			public void PlayMotion(int findex)
			{
				this.m_PlayAnime.Play(this.m_AnimeTable[findex].name);
				this.m_PlayAnimeNo = findex;
			}

			// Token: 0x0600222E RID: 8750 RVA: 0x000B5A26 File Offset: 0x000B3E26
			public bool IsPlaying(int findex)
			{
				return this.m_PlayAnime.IsPlaying(this.m_AnimeTable[findex].name);
			}

			// Token: 0x0600222F RID: 8751 RVA: 0x000B5A40 File Offset: 0x000B3E40
			public void ChangeLayer(int flayerid)
			{
				if (this.m_Render != null)
				{
					this.m_Render.sortingOrder = flayerid;
				}
			}

			// Token: 0x040028AE RID: 10414
			private Animation m_PlayAnime;

			// Token: 0x040028AF RID: 10415
			private AnimationState[] m_AnimeTable;

			// Token: 0x040028B0 RID: 10416
			private MeshRenderer m_Render;

			// Token: 0x040028B1 RID: 10417
			public int m_PlayAnimeNo;
		}

		// Token: 0x020006B2 RID: 1714
		public class TraningCharaIcon
		{
			// Token: 0x06002231 RID: 8753 RVA: 0x000B5A68 File Offset: 0x000B3E68
			public void SetUpModel(long fmanageid, TownBuilder pbuilder, TownObjHandleMenu phandle)
			{
				this.m_ManageID = fmanageid;
				this.m_Search = true;
				this.m_Step = 0;
				this.m_Object = new GameObject("MenuChara");
				this.m_Model = (TownObjHandleMenuChara)ITownObjectHandler.CreateHandler(this.m_Object, TownUtility.eResCategory.MenuChara, pbuilder);
				this.m_Model.SetUpChara(fmanageid, 0.33f, TownObjHandleMenuChara.eMenuType.Traning);
				this.m_Object.transform.SetParent(phandle.CacheTransform, false);
			}

			// Token: 0x06002232 RID: 8754 RVA: 0x000B5ADC File Offset: 0x000B3EDC
			public bool UpdateModel(TownBuilder pbuilder, TownObjHandleMenu pmenu)
			{
				bool result = true;
				if (this.m_Step == 0)
				{
					if (this.m_Model.IsAvailable())
					{
						this.m_Model.GetModel().ChangeLayer(11);
						this.m_Step = 1;
					}
				}
				else if (this.m_Step == 2)
				{
					this.m_Model.DestroyRequest();
					this.m_Model = null;
					this.m_Object = null;
					result = false;
				}
				return result;
			}

			// Token: 0x06002233 RID: 8755 RVA: 0x000B5B4C File Offset: 0x000B3F4C
			public void SetPoint(Vector3 fpos)
			{
				this.m_Object.transform.localPosition = fpos;
			}

			// Token: 0x06002234 RID: 8756 RVA: 0x000B5B5F File Offset: 0x000B3F5F
			public void SetReleaseStep()
			{
				this.m_Step = 2;
			}

			// Token: 0x040028B2 RID: 10418
			public bool m_Search;

			// Token: 0x040028B3 RID: 10419
			public long m_ManageID;

			// Token: 0x040028B4 RID: 10420
			private GameObject m_Object;

			// Token: 0x040028B5 RID: 10421
			private TownObjHandleMenuChara m_Model;

			// Token: 0x040028B6 RID: 10422
			public int m_Step;
		}
	}
}
