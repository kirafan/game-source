﻿using System;

namespace Star
{
	// Token: 0x02000201 RID: 513
	[Serializable]
	public struct SkillContentListDB_Data
	{
		// Token: 0x04000CA7 RID: 3239
		public sbyte m_SkillLvCoef;

		// Token: 0x04000CA8 RID: 3240
		public int m_Target;

		// Token: 0x04000CA9 RID: 3241
		public int m_Type;

		// Token: 0x04000CAA RID: 3242
		public float[] m_Args;
	}
}
