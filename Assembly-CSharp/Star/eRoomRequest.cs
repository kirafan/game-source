﻿using System;

namespace Star
{
	// Token: 0x020005FA RID: 1530
	public enum eRoomRequest
	{
		// Token: 0x040024BC RID: 9404
		ChangeModel,
		// Token: 0x040024BD RID: 9405
		ChangeWall,
		// Token: 0x040024BE RID: 9406
		ChangeFloor,
		// Token: 0x040024BF RID: 9407
		ChangeBG,
		// Token: 0x040024C0 RID: 9408
		CharaRelease
	}
}
