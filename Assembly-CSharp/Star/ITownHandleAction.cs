﻿using System;

namespace Star
{
	// Token: 0x020006A3 RID: 1699
	public class ITownHandleAction
	{
		// Token: 0x06002211 RID: 8721 RVA: 0x000B547C File Offset: 0x000B387C
		public static ITownHandleAction CreateHandleAction(ITownObjectHandler phandle, eTownEventAct faction)
		{
			return new TownHandleAction
			{
				m_Type = faction,
				m_AccessID = phandle.GetBuildAccessID(),
				m_Target = phandle
			};
		}

		// Token: 0x06002212 RID: 8722 RVA: 0x000B54AC File Offset: 0x000B38AC
		public static ITownHandleAction CreateAction(eTownEventAct faction)
		{
			return new ITownHandleAction
			{
				m_Type = faction
			};
		}

		// Token: 0x04002894 RID: 10388
		public eTownEventAct m_Type;

		// Token: 0x04002895 RID: 10389
		public int m_AccessID;
	}
}
