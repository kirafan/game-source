﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E8 RID: 1256
	public class TransformShaker : MonoBehaviour
	{
		// Token: 0x0600190B RID: 6411 RVA: 0x000822A9 File Offset: 0x000806A9
		public void Play(float decay, float intensity, Transform refTransform)
		{
			this.m_IsShaking = true;
			this.m_Decay = decay;
			this.m_Intensity = intensity;
			this.m_WorkIntensity = this.m_Intensity;
			this.m_Transform = refTransform;
			this.m_OriginPos = this.m_Transform.localPosition;
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x000822E4 File Offset: 0x000806E4
		public void Stop()
		{
			this.m_IsShaking = false;
		}

		// Token: 0x0600190D RID: 6413 RVA: 0x000822F0 File Offset: 0x000806F0
		public void Update()
		{
			if (this.m_IsShaking)
			{
				this.m_Transform.localPosition = this.m_OriginPos + UnityEngine.Random.insideUnitSphere * this.m_Intensity;
				this.m_WorkIntensity -= this.m_Decay * Time.deltaTime;
				if (this.m_WorkIntensity <= 0f)
				{
					this.m_IsShaking = false;
				}
			}
		}

		// Token: 0x04001F82 RID: 8066
		private float m_Decay;

		// Token: 0x04001F83 RID: 8067
		private float m_Intensity;

		// Token: 0x04001F84 RID: 8068
		private float m_WorkIntensity;

		// Token: 0x04001F85 RID: 8069
		private Transform m_Transform;

		// Token: 0x04001F86 RID: 8070
		private Vector3 m_OriginPos;

		// Token: 0x04001F87 RID: 8071
		private bool m_IsShaking;
	}
}
