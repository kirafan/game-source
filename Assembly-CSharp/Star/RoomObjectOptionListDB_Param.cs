﻿using System;

namespace Star
{
	// Token: 0x02000619 RID: 1561
	[Serializable]
	public struct RoomObjectOptionListDB_Param
	{
		// Token: 0x04002517 RID: 9495
		public uint m_Group;

		// Token: 0x04002518 RID: 9496
		public ushort m_AttachType;

		// Token: 0x04002519 RID: 9497
		public ushort m_ActiveNum;

		// Token: 0x0400251A RID: 9498
		public byte m_MakeID0;

		// Token: 0x0400251B RID: 9499
		public byte m_MakeID1;

		// Token: 0x0400251C RID: 9500
		public byte m_MakeID2;

		// Token: 0x0400251D RID: 9501
		public byte m_MakeID3;

		// Token: 0x0400251E RID: 9502
		public float m_ObjOffsetX0;

		// Token: 0x0400251F RID: 9503
		public float m_ObjOffsetY0;

		// Token: 0x04002520 RID: 9504
		public float m_ObjOffsetX1;

		// Token: 0x04002521 RID: 9505
		public float m_ObjOffsetY1;

		// Token: 0x04002522 RID: 9506
		public float m_ObjOffsetX2;

		// Token: 0x04002523 RID: 9507
		public float m_ObjOffsetY2;

		// Token: 0x04002524 RID: 9508
		public float m_ObjOffsetX3;

		// Token: 0x04002525 RID: 9509
		public float m_ObjOffsetY3;
	}
}
