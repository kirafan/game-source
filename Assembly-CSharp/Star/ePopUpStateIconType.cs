﻿using System;

namespace Star
{
	// Token: 0x020001DE RID: 478
	public enum ePopUpStateIconType
	{
		// Token: 0x04000B3A RID: 2874
		None,
		// Token: 0x04000B3B RID: 2875
		Confusion,
		// Token: 0x04000B3C RID: 2876
		Paralysis,
		// Token: 0x04000B3D RID: 2877
		Poison,
		// Token: 0x04000B3E RID: 2878
		Bearish,
		// Token: 0x04000B3F RID: 2879
		Sleep,
		// Token: 0x04000B40 RID: 2880
		Unhappy,
		// Token: 0x04000B41 RID: 2881
		Silence,
		// Token: 0x04000B42 RID: 2882
		Isolation,
		// Token: 0x04000B43 RID: 2883
		StatusAtkUp,
		// Token: 0x04000B44 RID: 2884
		StatusMgcUp,
		// Token: 0x04000B45 RID: 2885
		StatusDefUp,
		// Token: 0x04000B46 RID: 2886
		StatusMDefUp,
		// Token: 0x04000B47 RID: 2887
		StatusSpdUp,
		// Token: 0x04000B48 RID: 2888
		StatusLuckUp,
		// Token: 0x04000B49 RID: 2889
		StatusAtkDown,
		// Token: 0x04000B4A RID: 2890
		StatusMgcDown,
		// Token: 0x04000B4B RID: 2891
		StatusDefDown,
		// Token: 0x04000B4C RID: 2892
		StatusMDefDown,
		// Token: 0x04000B4D RID: 2893
		StatusSpdDown,
		// Token: 0x04000B4E RID: 2894
		StatusLuckDown,
		// Token: 0x04000B4F RID: 2895
		ElementFireUp,
		// Token: 0x04000B50 RID: 2896
		ElementWaterUp,
		// Token: 0x04000B51 RID: 2897
		ElementEarthUp,
		// Token: 0x04000B52 RID: 2898
		ElementWindUp,
		// Token: 0x04000B53 RID: 2899
		ElementMoonUp,
		// Token: 0x04000B54 RID: 2900
		ElementSunUp,
		// Token: 0x04000B55 RID: 2901
		ElementFireDown,
		// Token: 0x04000B56 RID: 2902
		ElementWaterDown,
		// Token: 0x04000B57 RID: 2903
		ElementEarthDown,
		// Token: 0x04000B58 RID: 2904
		ElementWindDown,
		// Token: 0x04000B59 RID: 2905
		ElementMoonDown,
		// Token: 0x04000B5A RID: 2906
		ElementSunDown,
		// Token: 0x04000B5B RID: 2907
		StateAbnormalDisable,
		// Token: 0x04000B5C RID: 2908
		StateAbnormalAdditionalProbabilityUp,
		// Token: 0x04000B5D RID: 2909
		StateAbnormalAdditionalProbabilityDown,
		// Token: 0x04000B5E RID: 2910
		WeakElementBonus,
		// Token: 0x04000B5F RID: 2911
		NextAtkUp,
		// Token: 0x04000B60 RID: 2912
		NextMgcUp,
		// Token: 0x04000B61 RID: 2913
		NextCritical,
		// Token: 0x04000B62 RID: 2914
		Barrier,
		// Token: 0x04000B63 RID: 2915
		ChainBonusUp,
		// Token: 0x04000B64 RID: 2916
		RecastUp,
		// Token: 0x04000B65 RID: 2917
		RecastDown,
		// Token: 0x04000B66 RID: 2918
		HateUp,
		// Token: 0x04000B67 RID: 2919
		HateDown,
		// Token: 0x04000B68 RID: 2920
		ChargeCountUp,
		// Token: 0x04000B69 RID: 2921
		ChargeCountDown,
		// Token: 0x04000B6A RID: 2922
		Regene,
		// Token: 0x04000B6B RID: 2923
		Num
	}
}
