﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000571 RID: 1393
	public class ActXlsKeyPosAnime : ActXlsKeyBase
	{
		// Token: 0x06001B1F RID: 6943 RVA: 0x0008F8B0 File Offset: 0x0008DCB0
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Int(ref this.m_Times);
			pio.Float(ref this.m_Pos.x);
			pio.Float(ref this.m_Pos.y);
			pio.Float(ref this.m_Pos.z);
		}

		// Token: 0x0400220E RID: 8718
		public string m_TargetName;

		// Token: 0x0400220F RID: 8719
		public int m_Times;

		// Token: 0x04002210 RID: 8720
		public Vector3 m_Pos;
	}
}
