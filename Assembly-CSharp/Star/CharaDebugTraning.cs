﻿using System;

namespace Star
{
	// Token: 0x0200029B RID: 667
	[Serializable]
	public class CharaDebugTraning
	{
		// Token: 0x04001524 RID: 5412
		public int m_ManageID;

		// Token: 0x04001525 RID: 5413
		public int m_TraningID;

		// Token: 0x04001526 RID: 5414
		public int m_Hour;

		// Token: 0x04001527 RID: 5415
		public int m_Minute;

		// Token: 0x04001528 RID: 5416
		public int m_Sec;
	}
}
