﻿using System;

namespace Star
{
	// Token: 0x02000572 RID: 1394
	public class ActXlsKeyRotAnime : ActXlsKeyBase
	{
		// Token: 0x06001B21 RID: 6945 RVA: 0x0008F917 File Offset: 0x0008DD17
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Int(ref this.m_Times);
			pio.Float(ref this.m_Rot);
		}

		// Token: 0x04002211 RID: 8721
		public string m_TargetName;

		// Token: 0x04002212 RID: 8722
		public int m_Times;

		// Token: 0x04002213 RID: 8723
		public float m_Rot;
	}
}
