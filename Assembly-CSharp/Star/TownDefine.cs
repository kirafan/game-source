﻿using System;

namespace Star
{
	// Token: 0x02000711 RID: 1809
	public static class TownDefine
	{
		// Token: 0x060023CF RID: 9167 RVA: 0x000C095C File Offset: 0x000BED5C
		public static void CalcRoomManageID()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				if (userTownData.GetBuildObjectDataAt(i).m_BuildPointIndex == 0)
				{
					TownDefine.ROOM_ACCESS_MNG_ID = userTownData.GetBuildObjectDataAt(i).m_ManageID;
					break;
				}
			}
		}

		// Token: 0x04002AC1 RID: 10945
		public const int BUILD_LEVEL_MODEL_CUT = 1;

		// Token: 0x04002AC2 RID: 10946
		public const int AREA_CTXID = 0;

		// Token: 0x04002AC3 RID: 10947
		public const int MENU_BUFID = 268435456;

		// Token: 0x04002AC4 RID: 10948
		public const int AREA_CONTENTID = 536870912;

		// Token: 0x04002AC5 RID: 10949
		public const int AREA_BUFID = 1073741824;

		// Token: 0x04002AC6 RID: 10950
		public const int TYPE_MASKID = 1879048192;

		// Token: 0x04002AC7 RID: 10951
		public const float CHARA_ICON_SIZE = 0.75f;

		// Token: 0x04002AC8 RID: 10952
		public const int LAYER_OFFSET = 10;

		// Token: 0x04002AC9 RID: 10953
		public static long ROOM_ACCESS_MNG_ID = 1L;

		// Token: 0x04002ACA RID: 10954
		public const int AREA_OBJID = 1;

		// Token: 0x02000712 RID: 1810
		public enum eTownLevelUpConditionCategory
		{
			// Token: 0x04002ACC RID: 10956
			Gold,
			// Token: 0x04002ACD RID: 10957
			Kirara,
			// Token: 0x04002ACE RID: 10958
			UserLv,
			// Token: 0x04002ACF RID: 10959
			BuildTime
		}
	}
}
