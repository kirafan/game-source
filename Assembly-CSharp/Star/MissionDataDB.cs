﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004EC RID: 1260
	public class MissionDataDB : ScriptableObject
	{
		// Token: 0x04001F9C RID: 8092
		public MissionDataDB_Param[] m_Params;
	}
}
