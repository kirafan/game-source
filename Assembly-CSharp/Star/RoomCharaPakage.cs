﻿using System;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000584 RID: 1412
	public class RoomCharaPakage
	{
		// Token: 0x06001B83 RID: 7043 RVA: 0x0009189C File Offset: 0x0008FC9C
		public RoomCharaPakage(long fmngid, int froomid, RoomCharaManager pmanager)
		{
			this.m_Manager = pmanager;
			this.m_Handle = null;
			this.m_ManageID = fmngid;
			this.m_RoomID = froomid;
			this.m_IsSchedule = false;
			this.m_RoomObj = null;
			this.m_State = RoomCharaPakage.eState.Non;
			this.m_FloorID = 0;
			this.m_StackQue = new RoomCharaPakage.StackActCommand[(int)this.STACK_QUE_MAX];
			this.m_StackQueNum = 0;
			FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
			this.m_Named = eCharaNamedType.None;
			if (charaState != null)
			{
				charaState.LinkFeedBackCall(new FeedBackCharaCallBack(this.FeedBackCharaCallBack));
				this.m_IsSchedule = true;
			}
			this.m_Named = (eCharaNamedType)UserCharaUtil.GetCharaParam(this.m_ManageID).m_NamedType;
		}

		// Token: 0x06001B84 RID: 7044 RVA: 0x00091951 File Offset: 0x0008FD51
		public bool IsScheduleChara()
		{
			return this.m_IsSchedule;
		}

		// Token: 0x06001B85 RID: 7045 RVA: 0x00091959 File Offset: 0x0008FD59
		public bool IsRoomIn()
		{
			return this.m_State != RoomCharaPakage.eState.Non;
		}

		// Token: 0x06001B86 RID: 7046 RVA: 0x00091967 File Offset: 0x0008FD67
		public void AttachObjectHandle(CharacterHandler fhandle, RoomObjectCtrlChara proomobj)
		{
			this.m_Handle = fhandle;
			this.m_RoomObj = proomobj;
			this.m_State = RoomCharaPakage.eState.SetUpWait;
		}

		// Token: 0x06001B87 RID: 7047 RVA: 0x00091980 File Offset: 0x0008FD80
		public void SetUpRoomCharaObject(CharacterParam fsetup, int fresid, Transform parent)
		{
			CharacterHandler characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(fsetup, null, null, eCharaResourceType.Player, fresid, CharacterDefine.eMode.Room, true, CharacterDefine.eFriendType.None, "CHARA_" + this.m_ManageID, parent);
			if (characterHandler != null)
			{
				characterHandler.Prepare();
				characterHandler.CacheTransform.localPosition = new Vector3(-10000f, -10000f, -10000f);
				RoomObjectCtrlChara roomObjectCtrlChara = (RoomObjectCtrlChara)RoomObjectControllCreate.CreateCharaObject(characterHandler.gameObject);
				roomObjectCtrlChara.SetManageID(this.m_ManageID);
				this.AttachObjectHandle(characterHandler, roomObjectCtrlChara);
			}
		}

		// Token: 0x06001B88 RID: 7048 RVA: 0x00091A14 File Offset: 0x0008FE14
		public bool IsPreparing()
		{
			bool result = false;
			if (this.m_Handle != null)
			{
				result = this.m_Handle.IsPreparing();
			}
			return result;
		}

		// Token: 0x06001B89 RID: 7049 RVA: 0x00091A44 File Offset: 0x0008FE44
		public void UpdatePakage(RoomBuilder pbuilder)
		{
			switch (this.m_State)
			{
			case RoomCharaPakage.eState.Non:
				if (this.IsStackActCmd())
				{
					RoomCharaPakage.StackActCommand stackQue = this.GetStackQue();
					this.m_State = stackQue.m_StackState;
				}
				break;
			case RoomCharaPakage.eState.SetUp:
				this.SetUpRoomCharaObject(UserCharaUtil.GetCharaData(this.m_ManageID).Param, UserCharaUtil.GetCharaParam(this.m_ManageID).m_ResourceID, pbuilder.CacheTransform);
				this.m_State = RoomCharaPakage.eState.SetUpWait;
				break;
			case RoomCharaPakage.eState.SetUpWait:
				if (!this.m_Handle.IsPreparing())
				{
					this.BuildLink(pbuilder);
				}
				break;
			case RoomCharaPakage.eState.Update:
			case RoomCharaPakage.eState.WakeUp:
				if (this.IsStackActCmd())
				{
					RoomCharaPakage.StackActCommand stackQue = this.GetStackQue();
					this.CheckStateChange(stackQue.m_StackState, stackQue);
					this.SetNextQue();
				}
				break;
			case RoomCharaPakage.eState.Sleep:
				if (this.IsStackActCmd())
				{
					RoomCharaPakage.StackActCommand stackQue = this.GetStackQue();
					this.CheckStateChange(stackQue.m_StackState, stackQue);
					this.SetNextQue();
					if (this.m_State != RoomCharaPakage.eState.Sleep)
					{
						this.m_Manager.ClrSleepFloor(this.m_ManageID, true);
					}
				}
				break;
			case RoomCharaPakage.eState.End:
				if (this.m_RoomObj != null)
				{
					this.m_RoomObj.SetObjectLinkCut();
					RoomCharaPakage.StackActCommand stackQue = this.GetStackQue();
					if (stackQue != null)
					{
						this.m_State = stackQue.m_StackState;
						if (stackQue.m_SetUpCmd != null)
						{
							this.m_RoomObj.RequestIrqCommand(stackQue.m_SetUpCmd);
						}
					}
					this.m_RoomObj.UpdateObj();
					if (this.m_RoomObj != null)
					{
						this.m_RoomObj.ReleaseEntry();
					}
				}
				this.m_RoomObj = null;
				this.m_State = RoomCharaPakage.eState.Non;
				break;
			}
		}

		// Token: 0x06001B8A RID: 7050 RVA: 0x00091BFC File Offset: 0x0008FFFC
		private void CheckStateChange(RoomCharaPakage.eState fstate, RoomCharaPakage.StackActCommand pque)
		{
			switch (fstate)
			{
			case RoomCharaPakage.eState.Sleep:
				this.m_Manager.CheckAction(RoomGreet.eCategory.Sleep, this.m_ManageID);
				break;
			}
			if (pque.m_SetUpCmd != null && this.m_RoomObj != null)
			{
				this.m_RoomObj.RequestIrqCommand(pque.m_SetUpCmd);
			}
			this.m_State = fstate;
		}

		// Token: 0x06001B8B RID: 7051 RVA: 0x00091C9C File Offset: 0x0009009C
		public bool IsScheduleOfRoom()
		{
			bool result = false;
			FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
			if (charaState != null)
			{
				switch (charaState.GetStayScene())
				{
				case eCharaStay.Room:
				case eCharaStay.Sleep:
					result = true;
					break;
				case eCharaStay.Content:
				case eCharaStay.Buf:
				case eCharaStay.Menu:
					if (charaState.GetStayBuildMngID() == -1L)
					{
						result = true;
					}
					break;
				}
			}
			return result;
		}

		// Token: 0x06001B8C RID: 7052 RVA: 0x00091D00 File Offset: 0x00090100
		public void Clear(RoomBuilder pbuilder)
		{
			if (this.m_Manager != null && this.m_State == RoomCharaPakage.eState.Sleep)
			{
				this.m_Manager.ClrSleepFloor(this.m_ManageID, true);
			}
			if (this.m_RoomObj != null)
			{
				this.m_RoomObj.AbortObjEventMode();
				this.m_RoomObj.ReleaseObj();
			}
			if (this.m_Handle != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_Handle);
			}
			this.m_Handle = null;
			this.m_RoomObj = null;
			this.m_State = RoomCharaPakage.eState.Non;
		}

		// Token: 0x06001B8D RID: 7053 RVA: 0x00091DA0 File Offset: 0x000901A0
		public void BuildLink(RoomBuilder pbuilder)
		{
			if (this.m_RoomObj != null)
			{
				this.m_RoomObj.LinkManager(pbuilder, this.m_Manager);
				this.m_RoomObj.Setup(this.m_Handle);
				pbuilder.AttachSortObject(this.m_RoomObj, (int)this.m_Named);
				this.AttachCharaHud(pbuilder.GetHUDObject());
				this.SetFreePosition(pbuilder.GetGridStatus(this.m_FloorID), pbuilder);
				if (this.m_IsSchedule)
				{
					FieldObjHandleChara charaState = FieldMapManager.GetCharaState(this.m_ManageID);
					if (charaState.GetStayScene() == eCharaStay.Sleep)
					{
						this.m_Manager.SetSleepFloor(this.m_ManageID, true);
						this.m_State = RoomCharaPakage.eState.Sleep;
						this.m_RoomObj.RequestSleep(2);
					}
					else
					{
						this.m_RoomObj.RequestIdleMode(0f);
						this.m_State = RoomCharaPakage.eState.Update;
					}
				}
				else
				{
					this.m_State = RoomCharaPakage.eState.Update;
				}
				RoomCharaPakage.StackActCommand stackQue = this.GetStackQue();
				if (stackQue != null)
				{
					if (stackQue.m_SetUpCmd != null)
					{
						this.m_RoomObj.RequestIrqCommand(stackQue.m_SetUpCmd);
					}
					this.SetNextQue();
				}
			}
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x00091EB4 File Offset: 0x000902B4
		private void SetFreePosition(RoomGridState pgrid, RoomBuilder pbuilder)
		{
			if (this.m_RoomObj != null)
			{
				Vector2 vector = pgrid.CalcFreeMovePoint();
				this.m_RoomObj.Initialize((int)vector.x, (int)vector.y);
				Vector2 vector2 = RoomUtility.GridToTilePos(vector.x, vector.y);
				this.m_RoomObj.SetCurrentPos(new Vector3(vector2.x, vector2.y, 0f), (int)vector.x, (int)vector.y);
				this.m_RoomObj.RequestIdleMode(1f);
			}
		}

		// Token: 0x06001B8F RID: 7055 RVA: 0x00091F4C File Offset: 0x0009034C
		public void AttachCharaHud(GameObject phudobj)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(phudobj);
			gameObject.SetActive(true);
			this.m_RoomObj.AttachHud(gameObject.GetComponent<RoomCharaHud>());
		}

		// Token: 0x06001B90 RID: 7056 RVA: 0x00091F78 File Offset: 0x00090378
		public void FeedBackCharaCallBack(eCallBackType ftype, FieldObjHandleChara punit)
		{
			if (ftype == eCallBackType.MoveBuild)
			{
				switch (punit.GetStayScene())
				{
				case eCharaStay.Room:
					if (this.m_State == RoomCharaPakage.eState.Sleep)
					{
						if (this.m_RoomObj != null)
						{
							this.m_RoomObj.SetWakeUpAction(true);
						}
						this.StackActCmd(RoomCharaPakage.eState.WakeUp, RoomActionCommand.eActionCode.Non, 0, false);
						this.m_Manager.CheckAction(RoomGreet.eCategory.WakeUp, this.m_ManageID);
					}
					else if (this.m_State == RoomCharaPakage.eState.Non)
					{
						this.StackActCmd(RoomCharaPakage.eState.SetUp, RoomActionCommand.eActionCode.Non, 0, false);
						this.m_Manager.CheckAction(RoomGreet.eCategory.GoHome, this.m_ManageID);
						this.m_Manager.AddScheduleCharacter(this.m_ManageID);
					}
					break;
				case eCharaStay.Sleep:
					if (this.m_State != RoomCharaPakage.eState.Sleep)
					{
						if (this.m_State == RoomCharaPakage.eState.Non)
						{
							this.StackActCmd(RoomCharaPakage.eState.SetUp, RoomActionCommand.eActionCode.Non, 0, false);
							this.m_Manager.CheckAction(RoomGreet.eCategory.GoHome, this.m_ManageID);
							this.m_Manager.AddScheduleCharacter(this.m_ManageID);
						}
						if (this.m_RoomObj != null)
						{
							this.m_RoomObj.SetWakeUpAction(false);
						}
						this.m_Manager.SetSleepFloor(this.m_ManageID, true);
						this.StackActCmd(RoomCharaPakage.eState.Sleep, RoomActionCommand.eActionCode.Sleep, 0, false);
					}
					break;
				case eCharaStay.Content:
					if (this.m_State == RoomCharaPakage.eState.Sleep)
					{
						if (this.m_RoomObj != null)
						{
							this.m_RoomObj.SetWakeUpAction(true);
						}
						this.StackActCmd(RoomCharaPakage.eState.WakeUp, RoomActionCommand.eActionCode.Non, 0, false);
						this.m_Manager.CheckAction(RoomGreet.eCategory.WakeUp, this.m_ManageID);
					}
					if (this.m_State != RoomCharaPakage.eState.Non)
					{
						this.StackActCmd(RoomCharaPakage.eState.End, RoomActionCommand.eActionCode.Erase, 2003, true);
						this.m_Manager.CheckAction(RoomGreet.eCategory.GoTown, this.m_ManageID);
					}
					break;
				case eCharaStay.Buf:
					if (this.m_State == RoomCharaPakage.eState.Sleep)
					{
						if (this.m_RoomObj != null)
						{
							this.m_RoomObj.SetWakeUpAction(true);
						}
						this.StackActCmd(RoomCharaPakage.eState.WakeUp, RoomActionCommand.eActionCode.Non, 0, false);
						this.m_Manager.CheckAction(RoomGreet.eCategory.WakeUp, this.m_ManageID);
					}
					if (this.m_State != RoomCharaPakage.eState.Non)
					{
						this.StackActCmd(RoomCharaPakage.eState.End, RoomActionCommand.eActionCode.Erase, 2003, true);
						this.m_Manager.CheckAction(RoomGreet.eCategory.GoTown, this.m_ManageID);
					}
					break;
				case eCharaStay.Menu:
					if (this.m_State == RoomCharaPakage.eState.Sleep)
					{
						if (this.m_RoomObj != null)
						{
							this.m_RoomObj.SetWakeUpAction(true);
						}
						this.StackActCmd(RoomCharaPakage.eState.WakeUp, RoomActionCommand.eActionCode.Non, 0, false);
						this.m_Manager.CheckAction(RoomGreet.eCategory.WakeUp, this.m_ManageID);
					}
					if (this.m_State != RoomCharaPakage.eState.Non)
					{
						this.StackActCmd(RoomCharaPakage.eState.End, RoomActionCommand.eActionCode.Erase, 2003, true);
						this.m_Manager.CheckAction(RoomGreet.eCategory.GoTown, this.m_ManageID);
					}
					break;
				}
			}
		}

		// Token: 0x06001B91 RID: 7057 RVA: 0x00092223 File Offset: 0x00090623
		public bool SetSleep()
		{
			if (this.m_Manager.CheckAction(RoomGreet.eCategory.Sleep, this.m_ManageID))
			{
				this.m_Manager.SetSleepFloor(this.m_ManageID, true);
				this.StackActCmd(RoomCharaPakage.eState.Sleep, RoomActionCommand.eActionCode.Sleep, 0, false);
				return true;
			}
			return false;
		}

		// Token: 0x06001B92 RID: 7058 RVA: 0x0009225C File Offset: 0x0009065C
		public void SetWakeUp()
		{
			if (this.m_RoomObj != null)
			{
				this.m_RoomObj.SetWakeUpAction(true);
			}
		}

		// Token: 0x06001B93 RID: 7059 RVA: 0x0009227C File Offset: 0x0009067C
		public void SetUpInRoom(Transform parent, bool faction)
		{
			this.SetUpRoomCharaObject(UserCharaUtil.GetCharaData(this.m_ManageID).Param, UserCharaUtil.GetCharaParam(this.m_ManageID).m_ResourceID, parent);
		}

		// Token: 0x06001B94 RID: 7060 RVA: 0x000922B3 File Offset: 0x000906B3
		public void DropOutRoom()
		{
			this.StackActCmd(RoomCharaPakage.eState.End, RoomActionCommand.eActionCode.Erase, 2003, true);
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x000922C4 File Offset: 0x000906C4
		private void StackActCmd(RoomCharaPakage.eState fstate, RoomActionCommand.eActionCode fcmd, int fkey, bool flock = false)
		{
			if (this.m_StackQueNum < this.STACK_QUE_MAX)
			{
				RoomActionCommand roomActionCommand = null;
				if (fcmd != RoomActionCommand.eActionCode.Non)
				{
					roomActionCommand = new RoomActionCommand();
					roomActionCommand.m_Command = fcmd;
					roomActionCommand.m_ParamI1 = fkey;
					roomActionCommand.m_Lock = flock;
				}
				this.m_StackQue[(int)this.m_StackQueNum] = new RoomCharaPakage.StackActCommand();
				this.m_StackQue[(int)this.m_StackQueNum].m_StackState = fstate;
				this.m_StackQue[(int)this.m_StackQueNum].m_SetUpCmd = roomActionCommand;
				this.m_StackQueNum += 1;
			}
		}

		// Token: 0x06001B96 RID: 7062 RVA: 0x0009234D File Offset: 0x0009074D
		private bool IsStackActCmd()
		{
			return this.m_StackQueNum != 0;
		}

		// Token: 0x06001B97 RID: 7063 RVA: 0x0009235B File Offset: 0x0009075B
		private RoomCharaPakage.StackActCommand GetStackQue()
		{
			return this.m_StackQue[0];
		}

		// Token: 0x06001B98 RID: 7064 RVA: 0x00092368 File Offset: 0x00090768
		private void SetNextQue()
		{
			this.m_StackQue[0].m_SetUpCmd = null;
			for (int i = 1; i < (int)this.m_StackQueNum; i++)
			{
				this.m_StackQue[i - 1] = this.m_StackQue[i];
			}
			this.m_StackQueNum -= 1;
			this.m_StackQue[(int)this.m_StackQueNum] = null;
		}

		// Token: 0x0400226A RID: 8810
		public long m_ManageID;

		// Token: 0x0400226B RID: 8811
		public int m_RoomID;

		// Token: 0x0400226C RID: 8812
		public int m_AccessKey;

		// Token: 0x0400226D RID: 8813
		public eCharaNamedType m_Named;

		// Token: 0x0400226E RID: 8814
		private bool m_IsSchedule;

		// Token: 0x0400226F RID: 8815
		private int m_FloorID;

		// Token: 0x04002270 RID: 8816
		public CharacterHandler m_Handle;

		// Token: 0x04002271 RID: 8817
		public RoomObjectCtrlChara m_RoomObj;

		// Token: 0x04002272 RID: 8818
		private RoomCharaManager m_Manager;

		// Token: 0x04002273 RID: 8819
		private RoomCharaPakage.StackActCommand[] m_StackQue;

		// Token: 0x04002274 RID: 8820
		private short m_StackQueNum;

		// Token: 0x04002275 RID: 8821
		private short STACK_QUE_MAX = 4;

		// Token: 0x04002276 RID: 8822
		private RoomCharaPakage.eState m_State;

		// Token: 0x04002277 RID: 8823
		private RoomCharaPakage.eState m_SleepKick;

		// Token: 0x02000585 RID: 1413
		private enum eState
		{
			// Token: 0x04002279 RID: 8825
			Non,
			// Token: 0x0400227A RID: 8826
			SetUp,
			// Token: 0x0400227B RID: 8827
			SetUpWait,
			// Token: 0x0400227C RID: 8828
			Update,
			// Token: 0x0400227D RID: 8829
			Sleep,
			// Token: 0x0400227E RID: 8830
			WakeUp,
			// Token: 0x0400227F RID: 8831
			End
		}

		// Token: 0x02000586 RID: 1414
		private class StackActCommand
		{
			// Token: 0x04002280 RID: 8832
			public RoomCharaPakage.eState m_StackState;

			// Token: 0x04002281 RID: 8833
			public RoomActionCommand m_SetUpCmd;
		}
	}
}
