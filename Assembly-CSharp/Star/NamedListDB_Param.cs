﻿using System;

namespace Star
{
	// Token: 0x020001D8 RID: 472
	[Serializable]
	public struct NamedListDB_Param
	{
		// Token: 0x04000B22 RID: 2850
		public int m_NamedType;

		// Token: 0x04000B23 RID: 2851
		public int m_TitleType;

		// Token: 0x04000B24 RID: 2852
		public string m_ResouceBaseName;

		// Token: 0x04000B25 RID: 2853
		public string m_NickName;

		// Token: 0x04000B26 RID: 2854
		public string m_FullName;

		// Token: 0x04000B27 RID: 2855
		public int m_BattleWinID;

		// Token: 0x04000B28 RID: 2856
		public sbyte m_FriendshipTableID;

		// Token: 0x04000B29 RID: 2857
		public sbyte m_PersonalityType;

		// Token: 0x04000B2A RID: 2858
		public string m_ProfileText;

		// Token: 0x04000B2B RID: 2859
		public string m_CVText;

		// Token: 0x04000B2C RID: 2860
		public int m_DropItemKey;
	}
}
