﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000182 RID: 386
	public class EffectListDB : ScriptableObject
	{
		// Token: 0x04000AC1 RID: 2753
		public EffectListDB_Param[] m_Params;
	}
}
