﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020003F9 RID: 1017
	public class EditState_CharaListPartyEdit : EditState_CharaListPartyEditBase
	{
		// Token: 0x06001356 RID: 4950 RVA: 0x00067B47 File Offset: 0x00065F47
		public EditState_CharaListPartyEdit(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001357 RID: 4951 RVA: 0x00067B50 File Offset: 0x00065F50
		public override int GetDefaultNextSceneState()
		{
			return 2;
		}

		// Token: 0x06001358 RID: 4952 RVA: 0x00067B54 File Offset: 0x00065F54
		protected override void OnStateUpdateMainOnClickButton()
		{
			CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
			if (selectButton != CharaListUI.eButton.Remove)
			{
				if (selectButton == CharaListUI.eButton.Chara)
				{
					EditUtility.AssignPartyMember(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex, this.m_Owner.CharaListUI.SelectCharaMngID);
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
				}
			}
			else
			{
				EditUtility.RemovePartyMember(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
			}
		}

		// Token: 0x06001359 RID: 4953 RVA: 0x00067BFF File Offset: 0x00065FFF
		protected override void SetListEditMode()
		{
			this.m_Owner.CharaListUI.SetPartyEditMode(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex);
		}
	}
}
