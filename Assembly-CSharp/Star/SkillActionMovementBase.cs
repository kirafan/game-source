﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000126 RID: 294
	public class SkillActionMovementBase
	{
		// Token: 0x060007A4 RID: 1956 RVA: 0x0002ECB0 File Offset: 0x0002D0B0
		public SkillActionMovementBase(Transform moveObj, bool isLocalPos)
		{
			this.m_MoveObj = moveObj;
			this.m_IsLocalPos = isLocalPos;
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060007A5 RID: 1957 RVA: 0x0002ECC6 File Offset: 0x0002D0C6
		public bool IsMoving
		{
			get
			{
				return this.m_IsMoving;
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060007A6 RID: 1958 RVA: 0x0002ECCE File Offset: 0x0002D0CE
		public Transform MoveObj
		{
			get
			{
				return this.m_MoveObj;
			}
		}

		// Token: 0x060007A7 RID: 1959 RVA: 0x0002ECD6 File Offset: 0x0002D0D6
		public void Play()
		{
			this.m_IsMoving = true;
		}

		// Token: 0x060007A8 RID: 1960 RVA: 0x0002ECDF File Offset: 0x0002D0DF
		public virtual bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			out_arrivalIndex = -1;
			out_arrivalPos = Vector3.zero;
			return this.m_IsMoving;
		}

		// Token: 0x04000769 RID: 1897
		protected bool m_IsMoving;

		// Token: 0x0400076A RID: 1898
		protected Transform m_MoveObj;

		// Token: 0x0400076B RID: 1899
		protected bool m_IsLocalPos;
	}
}
