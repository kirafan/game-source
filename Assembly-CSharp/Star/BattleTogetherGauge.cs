﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000F3 RID: 243
	[Serializable]
	public class BattleTogetherGauge
	{
		// Token: 0x060006E8 RID: 1768 RVA: 0x000292D2 File Offset: 0x000276D2
		public BattleTogetherGauge()
		{
			this.Val = 0f;
		}

		// Token: 0x060006E9 RID: 1769 RVA: 0x000292E5 File Offset: 0x000276E5
		public float GetValue()
		{
			return this.Val;
		}

		// Token: 0x060006EA RID: 1770 RVA: 0x000292ED File Offset: 0x000276ED
		public float CalcGauge(float value)
		{
			this.Val = Mathf.Clamp(this.Val + value + 1E-06f, 0f, 3f);
			return this.Val;
		}

		// Token: 0x060006EB RID: 1771 RVA: 0x00029318 File Offset: 0x00027718
		public void ResetGauge()
		{
			this.Val = 0f;
		}

		// Token: 0x04000602 RID: 1538
		[SerializeField]
		private float Val;
	}
}
