﻿using System;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x0200043F RID: 1087
	public class MenuState_Option : MenuState
	{
		// Token: 0x060014E1 RID: 5345 RVA: 0x0006DC4E File Offset: 0x0006C04E
		public MenuState_Option(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014E2 RID: 5346 RVA: 0x0006DC66 File Offset: 0x0006C066
		public override int GetStateID()
		{
			return 6;
		}

		// Token: 0x060014E3 RID: 5347 RVA: 0x0006DC69 File Offset: 0x0006C069
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Option.eStep.First;
		}

		// Token: 0x060014E4 RID: 5348 RVA: 0x0006DC72 File Offset: 0x0006C072
		public override void OnStateExit()
		{
		}

		// Token: 0x060014E5 RID: 5349 RVA: 0x0006DC74 File Offset: 0x0006C074
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014E6 RID: 5350 RVA: 0x0006DC80 File Offset: 0x0006C080
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Option.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Option.eStep.LoadWait;
				break;
			case MenuState_Option.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Option.eStep.PlayIn;
				}
				break;
			case MenuState_Option.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Option.eStep.Main;
				}
				break;
			case MenuState_Option.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Option.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Option.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Option.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014E7 RID: 5351 RVA: 0x0006DDA0 File Offset: 0x0006C1A0
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuOptionUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014E8 RID: 5352 RVA: 0x0006DE09 File Offset: 0x0006C209
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
		}

		// Token: 0x060014E9 RID: 5353 RVA: 0x0006DE27 File Offset: 0x0006C227
		private void OnClickButton()
		{
		}

		// Token: 0x060014EA RID: 5354 RVA: 0x0006DE29 File Offset: 0x0006C229
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BCC RID: 7116
		private MenuState_Option.eStep m_Step = MenuState_Option.eStep.None;

		// Token: 0x04001BCD RID: 7117
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuOptionUI;

		// Token: 0x04001BCE RID: 7118
		private MenuOptionUI m_UI;

		// Token: 0x02000440 RID: 1088
		private enum eStep
		{
			// Token: 0x04001BD0 RID: 7120
			None = -1,
			// Token: 0x04001BD1 RID: 7121
			First,
			// Token: 0x04001BD2 RID: 7122
			LoadWait,
			// Token: 0x04001BD3 RID: 7123
			PlayIn,
			// Token: 0x04001BD4 RID: 7124
			Main,
			// Token: 0x04001BD5 RID: 7125
			UnloadChildSceneWait
		}
	}
}
