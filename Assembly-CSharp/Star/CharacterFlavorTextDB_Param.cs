﻿using System;

namespace Star
{
	// Token: 0x02000172 RID: 370
	[Serializable]
	public struct CharacterFlavorTextDB_Param
	{
		// Token: 0x040009CC RID: 2508
		public int m_CharaID;

		// Token: 0x040009CD RID: 2509
		public string m_FlavorText;
	}
}
