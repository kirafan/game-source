﻿using System;

namespace Star
{
	// Token: 0x0200055B RID: 1371
	public class CActScriptKeyCueSe : IRoomScriptData
	{
		// Token: 0x06001AE5 RID: 6885 RVA: 0x0008F21C File Offset: 0x0008D61C
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyCueSe actXlsKeyCueSe = (ActXlsKeyCueSe)pbase;
			this.m_CueName = actXlsKeyCueSe.m_CueName;
			this.m_Volume = actXlsKeyCueSe.m_Volume;
		}

		// Token: 0x040021C0 RID: 8640
		public string m_CueName;

		// Token: 0x040021C1 RID: 8641
		public float m_Volume;
	}
}
