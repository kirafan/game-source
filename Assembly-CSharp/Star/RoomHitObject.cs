﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005EF RID: 1519
	public class RoomHitObject : MonoBehaviour
	{
		// Token: 0x06001E00 RID: 7680 RVA: 0x000A155E File Offset: 0x0009F95E
		public void SetGridInfo(int fpointx, int fpointy, int fcategoryid)
		{
			this.m_GridX = fpointx;
			this.m_GridY = fpointy;
			this.m_CategoryID = fcategoryid;
		}

		// Token: 0x0400248F RID: 9359
		public int m_GridX;

		// Token: 0x04002490 RID: 9360
		public int m_GridY;

		// Token: 0x04002491 RID: 9361
		public int m_CategoryID;
	}
}
