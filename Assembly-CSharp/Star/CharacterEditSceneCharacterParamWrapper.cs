﻿using System;

namespace Star
{
	// Token: 0x020002A5 RID: 677
	public class CharacterEditSceneCharacterParamWrapper : UserCharacterDataWrapperBase
	{
		// Token: 0x06000CCB RID: 3275 RVA: 0x00048E3F File Offset: 0x0004723F
		public CharacterEditSceneCharacterParamWrapper(eCharacterDataType characterDataType, EditUtility.CharacterEditSimulateParam param) : base(characterDataType, param.nowUserCharaData)
		{
		}
	}
}
