﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000531 RID: 1329
	public class CharaActionSit : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x06001A94 RID: 6804 RVA: 0x0008D8E8 File Offset: 0x0008BCE8
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
			this.m_PartsAction = new RoomPartsActionPakage();
			base.SetUpPlayScene(roomObjHndl, pOwner, psetup, new RoomActionScriptPlay.ActionSciptCallBack(this.CallbackActionEvent), this.m_PartsAction);
			this.m_Target.SetLinkObject(this.m_Base, true);
			this.m_AnimePlay.SetBlink(true);
			this.m_TargetTrs = base.CalcTargetTrs();
		}

		// Token: 0x06001A95 RID: 6805 RVA: 0x0008D948 File Offset: 0x0008BD48
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			this.m_PartsAction.PakageUpdate();
			bool flag = base.PlayScene(hbase);
			if (!flag)
			{
				this.m_Target.SetLinkObject(this.m_Base, false);
			}
			return flag;
		}

		// Token: 0x06001A96 RID: 6806 RVA: 0x0008D984 File Offset: 0x0008BD84
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			bool result = true;
			int eventID = pcmd.m_EventID;
			if (eventID != 0)
			{
				if (eventID != 1)
				{
					if (eventID == 2)
					{
						Vector3 position = this.m_Owner.CacheTransform.position;
						Vector3 backUpPos = this.m_BackUpPos;
						position.y += 0.2f;
						backUpPos.y += 0.2f;
						RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position, backUpPos, this.m_BackUpPos, pcmd.m_EvtTime);
						this.m_PartsAction.EntryAction(paction, 0U);
					}
				}
			}
			else
			{
				Vector3 position2 = this.m_Owner.CacheTransform.position;
				Vector3 position3 = this.m_TargetTrs.position;
				position2.y += 0.2f;
				position3.y += 0.2f;
				RoomPartsMove paction2 = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, position2, position3, this.m_TargetTrs.position, pcmd.m_EvtTime);
				this.m_PartsAction.EntryAction(paction2, 0U);
			}
			return result;
		}

		// Token: 0x06001A97 RID: 6807 RVA: 0x0008DAC9 File Offset: 0x0008BEC9
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			this.m_Target.SetLinkObject(this.m_Base, false);
			base.CancelPlay();
			return true;
		}

		// Token: 0x06001A98 RID: 6808 RVA: 0x0008DAE4 File Offset: 0x0008BEE4
		public void Release()
		{
			base.ReleasePlay();
			this.m_PartsAction = null;
			this.m_TargetTrs = null;
		}

		// Token: 0x04002106 RID: 8454
		private Transform m_TargetTrs;
	}
}
