﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000184 RID: 388
	public class EnemyResourceListDB : ScriptableObject
	{
		// Token: 0x04000AC8 RID: 2760
		public EnemyResourceListDB_Param[] m_Params;
	}
}
