﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000175 RID: 373
	public class CharacterIllustOffsetDB : ScriptableObject
	{
		// Token: 0x040009D5 RID: 2517
		public CharacterIllustOffsetDB_Param[] m_Params;
	}
}
