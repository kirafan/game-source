﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000066 RID: 102
	public class ADVTextTagArgDB : ScriptableObject
	{
		// Token: 0x04000196 RID: 406
		public ADVTextTagArgDB_Param[] m_Params;
	}
}
