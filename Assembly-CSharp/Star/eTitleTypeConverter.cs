﻿using System;

namespace Star
{
	// Token: 0x020004EA RID: 1258
	public class eTitleTypeConverter
	{
		// Token: 0x06001918 RID: 6424 RVA: 0x00082740 File Offset: 0x00080B40
		public eTitleTypeConverter Setup()
		{
			this.m_DB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB;
			this.m_Table = this.m_DB.CalcSortedTitleID();
			return this;
		}

		// Token: 0x06001919 RID: 6425 RVA: 0x00082769 File Offset: 0x00080B69
		public int HowManyTitles()
		{
			return this.m_Table.Length;
		}

		// Token: 0x0600191A RID: 6426 RVA: 0x00082773 File Offset: 0x00080B73
		public eTitleType[] GetOrderTitleTypes()
		{
			return this.m_Table;
		}

		// Token: 0x0600191B RID: 6427 RVA: 0x0008277B File Offset: 0x00080B7B
		public eTitleType ConvertFromOrderToTitleType(int order)
		{
			return this.m_Table[order];
		}

		// Token: 0x0600191C RID: 6428 RVA: 0x00082788 File Offset: 0x00080B88
		public string ConvertFromOrderToTitleName(int order)
		{
			eTitleType titleType = this.ConvertFromOrderToTitleType(order);
			return this.m_DB.GetParam(titleType).m_DisplayName;
		}

		// Token: 0x0600191D RID: 6429 RVA: 0x000827B4 File Offset: 0x00080BB4
		public string[] GetOrderTitles()
		{
			string[] array = new string[this.HowManyTitles()];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = this.ConvertFromOrderToTitleName(i);
			}
			return array;
		}

		// Token: 0x04001F88 RID: 8072
		private TitleListDB m_DB;

		// Token: 0x04001F89 RID: 8073
		private eTitleType[] m_Table;
	}
}
