﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000691 RID: 1681
	public class TownComAPIObjAdd : INetComHandle
	{
		// Token: 0x060021C2 RID: 8642 RVA: 0x000B3B10 File Offset: 0x000B1F10
		public TownComAPIObjAdd()
		{
			this.ApiName = "player/town_facility/add";
			this.Request = true;
			this.ResponseType = typeof(Add);
		}

		// Token: 0x0400283C RID: 10300
		public string facilityId;

		// Token: 0x0400283D RID: 10301
		public string amount;
	}
}
