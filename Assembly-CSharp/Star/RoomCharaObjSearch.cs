﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005C8 RID: 1480
	public class RoomCharaObjSearch
	{
		// Token: 0x06001CBF RID: 7359 RVA: 0x0009897A File Offset: 0x00096D7A
		public RoomCharaObjSearch(int fblock)
		{
			this.m_SearchNum = 0;
			this.m_SearchMax = fblock;
			this.m_SearchTable = new RoomCharaObjSearch.SearchObjectHandle[this.m_SearchMax];
			this.m_SearchIndex = 0;
		}

		// Token: 0x06001CC0 RID: 7360 RVA: 0x000989A8 File Offset: 0x00096DA8
		public void ClearSearchInfo()
		{
			for (int i = 0; i < this.m_SearchNum; i++)
			{
				this.m_SearchTable[i].m_Handle = null;
			}
			this.m_SearchNum = 0;
			this.m_SearchIndex = 0;
		}

		// Token: 0x06001CC1 RID: 7361 RVA: 0x000989EC File Offset: 0x00096DEC
		public IRoomObjectControll CalcObjectEventFromCurrentPos(RoomBuilder pbuilder, RoomObjectCtrlChara pbase, int floorid)
		{
			this.ClearSearchInfo();
			int activeObjectNum = pbuilder.GetActiveObjectNum(floorid);
			for (int i = 0; i < activeObjectNum; i++)
			{
				IRoomObjectControll activeObjectAt = pbuilder.GetActiveObjectAt(floorid, i);
				if (activeObjectAt != null)
				{
					for (int j = 0; j < RoomDefine.ROOM_SEARCH_SIZE; j++)
					{
						int gridX = (int)(pbase.BlockData.PosSpaceX + (float)RoomDefine.ROOM_SEARCH_DIR[j].x);
						int gridY = (int)(pbase.BlockData.PosSpaceY + (float)RoomDefine.ROOM_SEARCH_DIR[j].y);
						if (activeObjectAt.IsInRange(gridX, gridY, j))
						{
							if (!activeObjectAt.IsPreparing() && activeObjectAt.IsAction() && activeObjectAt.IsCharaCheck(pbase) && this.m_SearchNum < this.m_SearchMax)
							{
								this.m_SearchTable[this.m_SearchNum].m_Handle = activeObjectAt;
								this.m_SearchNum++;
							}
							break;
						}
					}
				}
			}
			if (this.m_SearchNum > 0)
			{
				this.m_SearchIndex = UnityEngine.Random.Range(0, this.m_SearchNum);
				return this.m_SearchTable[this.m_SearchIndex].m_Handle;
			}
			return null;
		}

		// Token: 0x04002379 RID: 9081
		private RoomCharaObjSearch.SearchObjectHandle[] m_SearchTable;

		// Token: 0x0400237A RID: 9082
		private int m_SearchIndex;

		// Token: 0x0400237B RID: 9083
		private int m_SearchNum;

		// Token: 0x0400237C RID: 9084
		private int m_SearchMax;

		// Token: 0x020005C9 RID: 1481
		public struct SearchObjectHandle
		{
			// Token: 0x0400237D RID: 9085
			public IRoomObjectControll m_Handle;
		}
	}
}
