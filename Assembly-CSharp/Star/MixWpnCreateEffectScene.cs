﻿using System;
using System.Collections;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000477 RID: 1143
	public class MixWpnCreateEffectScene : MonoBehaviour
	{
		// Token: 0x0600164C RID: 5708 RVA: 0x000748FC File Offset: 0x00072CFC
		private void Update()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			MixWpnCreateEffectScene.eStep step = this.m_Step;
			if (step != MixWpnCreateEffectScene.eStep.Prepare_Wait)
			{
				if (step != MixWpnCreateEffectScene.eStep.Wait)
				{
					if (step == MixWpnCreateEffectScene.eStep.Play_Wait)
					{
						if (!this.IsPlaying())
						{
							this.m_BodyObj.SetActive(false);
							this.m_MeigeAnimCtrl.UpdateAnimation(0f);
							this.m_MeigeAnimCtrl.Pause();
							this.SetStep(MixWpnCreateEffectScene.eStep.None);
						}
					}
				}
			}
			else
			{
				if (this.m_LoadingHndl != null)
				{
					if (this.m_LoadingHndl.IsError())
					{
						this.SetStep(MixWpnCreateEffectScene.eStep.Prepare_Error);
						return;
					}
					if (!this.m_LoadingHndl.IsDone())
					{
						return;
					}
				}
				GameObject gameObject = this.m_LoadingHndl.FindObj("Mix_WpnCreate", true) as GameObject;
				GameObject gameObject2 = this.m_LoadingHndl.FindObj("MeigeAC_Mix_WpnCreate@Take 001", true) as GameObject;
				if (gameObject != null && gameObject2 != null)
				{
					this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(gameObject, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), base.transform);
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(gameObject.name, false);
					this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
					Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
					component.cullingType = AnimationCullingType.AlwaysAnimate;
					this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
					MeigeAnimClipHolder componentInChildren = gameObject2.GetComponentInChildren<MeigeAnimClipHolder>();
					this.m_MeigeAnimCtrl.Open();
					this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, componentInChildren);
					this.m_MeigeAnimCtrl.Close();
					int layer = LayerMask.NameToLayer("UI");
					this.SetLayerRecursively(this.m_BodyObj, layer);
					int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
					for (int i = 0; i < particleEmitterNum; i++)
					{
						MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(i);
						if (particleEmitter != null)
						{
							particleEmitter.SetLayer(layer);
							particleEmitter.SetSortingOrder(this.m_SortingOrder);
						}
					}
					Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
					if (rendererCache != null)
					{
						for (int j = 0; j < rendererCache.Length; j++)
						{
							rendererCache[j].sortingOrder = this.m_SortingOrder;
						}
					}
					this.m_BodyObj.SetActive(false);
				}
				if (this.m_LoadingHndl != null)
				{
					this.m_Loader.UnloadAll(false);
					this.m_LoadingHndl = null;
				}
				this.m_IsDonePrepare = true;
				this.SetStep(MixWpnCreateEffectScene.eStep.Wait);
			}
		}

		// Token: 0x0600164D RID: 5709 RVA: 0x00074BA4 File Offset: 0x00072FA4
		public void Prepare(int sortingOrder)
		{
			if (this.m_IsDonePrepare)
			{
				return;
			}
			this.m_SortingOrder = sortingOrder;
			this.m_LoadingHndl = this.m_Loader.Load("mix/mix_wpncreate.muast", new MeigeResource.Option[0]);
			this.SetStep(MixWpnCreateEffectScene.eStep.Prepare_Wait);
		}

		// Token: 0x0600164E RID: 5710 RVA: 0x00074BDC File Offset: 0x00072FDC
		public bool IsCompletePrepare()
		{
			return this.m_IsDonePrepare;
		}

		// Token: 0x0600164F RID: 5711 RVA: 0x00074BE4 File Offset: 0x00072FE4
		public bool IsPrepareError()
		{
			return this.m_Step == MixWpnCreateEffectScene.eStep.Prepare_Error;
		}

		// Token: 0x06001650 RID: 5712 RVA: 0x00074BF0 File Offset: 0x00072FF0
		public void Play()
		{
			if (!this.m_IsDonePrepare)
			{
				return;
			}
			this.m_BodyObj.transform.localPosZ(0f);
			this.m_BodyObj.SetActive(true);
			this.m_BodyObj.transform.localPosition = Vector3.zero;
			this.m_BodyObj.transform.localScale = Vector3.one * 100f;
			this.m_MsbHndl.SetDefaultVisibility();
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
			this.SetStep(MixWpnCreateEffectScene.eStep.Play_Wait);
		}

		// Token: 0x06001651 RID: 5713 RVA: 0x00074CB6 File Offset: 0x000730B6
		public bool IsCompletePlay()
		{
			return this.m_Step == MixWpnCreateEffectScene.eStep.None;
		}

		// Token: 0x06001652 RID: 5714 RVA: 0x00074CC1 File Offset: 0x000730C1
		public void Destroy()
		{
			this.m_IsDonePrepare = false;
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			this.m_BodyObj = null;
		}

		// Token: 0x06001653 RID: 5715 RVA: 0x00074CE6 File Offset: 0x000730E6
		private void SetStep(MixWpnCreateEffectScene.eStep step)
		{
			this.m_Step = step;
		}

		// Token: 0x06001654 RID: 5716 RVA: 0x00074CF0 File Offset: 0x000730F0
		private void SetLayerRecursively(GameObject self, int layer)
		{
			self.layer = layer;
			IEnumerator enumerator = self.transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					Transform transform = (Transform)obj;
					this.SetLayerRecursively(transform.gameObject, layer);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		// Token: 0x06001655 RID: 5717 RVA: 0x00074D64 File Offset: 0x00073164
		private bool IsPlaying()
		{
			return this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f;
		}

		// Token: 0x04001CF7 RID: 7415
		private const string LAYER_MASK = "UI";

		// Token: 0x04001CF8 RID: 7416
		private const string PLAY_KEY = "Take 001";

		// Token: 0x04001CF9 RID: 7417
		private const string RESOURCE_PATH = "mix/mix_wpncreate.muast";

		// Token: 0x04001CFA RID: 7418
		private const string OBJ_MODEL_PATH = "Mix_WpnCreate";

		// Token: 0x04001CFB RID: 7419
		private const string OBJ_ANIM_PATH = "MeigeAC_Mix_WpnCreate@Take 001";

		// Token: 0x04001CFC RID: 7420
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04001CFD RID: 7421
		private MixWpnCreateEffectScene.eStep m_Step = MixWpnCreateEffectScene.eStep.None;

		// Token: 0x04001CFE RID: 7422
		private bool m_IsDonePrepare;

		// Token: 0x04001CFF RID: 7423
		private ABResourceLoader m_Loader = new ABResourceLoader(-1);

		// Token: 0x04001D00 RID: 7424
		private ABResourceObjectHandler m_LoadingHndl;

		// Token: 0x04001D01 RID: 7425
		private Transform m_BaseTransform;

		// Token: 0x04001D02 RID: 7426
		private MsbHandler m_MsbHndl;

		// Token: 0x04001D03 RID: 7427
		private GameObject m_BodyObj;

		// Token: 0x04001D04 RID: 7428
		private int m_SortingOrder;

		// Token: 0x02000478 RID: 1144
		private enum eStep
		{
			// Token: 0x04001D06 RID: 7430
			None = -1,
			// Token: 0x04001D07 RID: 7431
			Prepare_Wait,
			// Token: 0x04001D08 RID: 7432
			Prepare_Error,
			// Token: 0x04001D09 RID: 7433
			Wait,
			// Token: 0x04001D0A RID: 7434
			Play_Wait
		}
	}
}
