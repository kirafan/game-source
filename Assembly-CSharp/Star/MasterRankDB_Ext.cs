﻿using System;

namespace Star
{
	// Token: 0x02000199 RID: 409
	public static class MasterRankDB_Ext
	{
		// Token: 0x06000AF7 RID: 2807 RVA: 0x0004163C File Offset: 0x0003FA3C
		public static MasterRankDB_Param GetParam(this MasterRankDB self, int currentRank)
		{
			if (currentRank >= 1 && currentRank <= self.m_Params.Length)
			{
				return self.m_Params[currentRank - 1];
			}
			return default(MasterRankDB_Param);
		}

		// Token: 0x06000AF8 RID: 2808 RVA: 0x0004167B File Offset: 0x0003FA7B
		public static int GetNextExp(this MasterRankDB self, int currentRank)
		{
			if (currentRank >= 1 && currentRank <= self.m_Params.Length)
			{
				return self.m_Params[currentRank - 1].m_NextExp;
			}
			return 0;
		}

		// Token: 0x06000AF9 RID: 2809 RVA: 0x000416A8 File Offset: 0x0003FAA8
		public static long[] GetNextMaxExps(this MasterRankDB self, int rank_a, int rank_b)
		{
			int num = rank_b - rank_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(rank_a + i);
			}
			return array;
		}

		// Token: 0x06000AFA RID: 2810 RVA: 0x000416EC File Offset: 0x0003FAEC
		public static long GetCurrentExp(this MasterRankDB self, int currentLv, long exp)
		{
			long num = 0L;
			for (int i = 1; i < currentLv; i++)
			{
				num += (long)self.GetNextExp(i);
			}
			if (exp >= num)
			{
				return exp - num;
			}
			return 0L;
		}
	}
}
