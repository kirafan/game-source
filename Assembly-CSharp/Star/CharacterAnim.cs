﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000136 RID: 310
	public class CharacterAnim
	{
		// Token: 0x06000806 RID: 2054 RVA: 0x00034390 File Offset: 0x00032790
		public CharacterAnim(CharacterHandler charaHndl)
		{
			this.m_Owner = charaHndl;
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000807 RID: 2055 RVA: 0x00034420 File Offset: 0x00032820
		public CharacterAnim.ModelSet[] ModelSets
		{
			get
			{
				return this.m_ModelSets;
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000808 RID: 2056 RVA: 0x00034428 File Offset: 0x00032828
		public int ModelSetsNum
		{
			get
			{
				return (this.m_ModelSets == null) ? 0 : this.m_ModelSets.Length;
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000809 RID: 2057 RVA: 0x00034443 File Offset: 0x00032843
		public CharacterAnim.ModelSet[] WeaponModelSets
		{
			get
			{
				return this.m_WeaponModelSets;
			}
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x0600080A RID: 2058 RVA: 0x0003444B File Offset: 0x0003284B
		public int WeaponModelSetsNum
		{
			get
			{
				return (this.m_WeaponModelSets == null) ? 0 : this.m_ModelSets.Length;
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600080B RID: 2059 RVA: 0x00034466 File Offset: 0x00032866
		public GameObject Shadow
		{
			get
			{
				return this.m_Shadow;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x0600080C RID: 2060 RVA: 0x0003446E File Offset: 0x0003286E
		public CharacterDefine.eDir Dir
		{
			get
			{
				return this.m_Dir;
			}
		}

		// Token: 0x0600080D RID: 2061 RVA: 0x00034476 File Offset: 0x00032876
		public void Update()
		{
			this.UpdateBlink();
			this.UpdateMeshColor();
		}

		// Token: 0x0600080E RID: 2062 RVA: 0x00034484 File Offset: 0x00032884
		public void LateUpdate()
		{
			this.UpdateFloatingObj();
		}

		// Token: 0x0600080F RID: 2063 RVA: 0x0003448C File Offset: 0x0003288C
		public void Setup()
		{
		}

		// Token: 0x06000810 RID: 2064 RVA: 0x00034490 File Offset: 0x00032890
		public void AttachCharaModel(GameObject[] modelObjs)
		{
			int num = modelObjs.Length;
			this.m_ModelSets = new CharacterAnim.ModelSet[num];
			for (int i = 0; i < num; i++)
			{
				if (modelObjs[i] != null)
				{
					this.m_ModelSets[i] = new CharacterAnim.ModelSet();
					this.m_ModelSets[i].m_Obj = modelObjs[i];
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_ModelSets[i].m_Obj.name, false);
					Transform transform = this.m_ModelSets[i].m_Obj.transform.Find(name);
					GameObject gameObject = transform.gameObject;
					this.m_ModelSets[i].m_Anim = transform.GetComponent<Animation>();
					this.m_ModelSets[i].m_Anim.cullingType = AnimationCullingType.AlwaysAnimate;
					this.m_ModelSets[i].m_MeigeAnimCtrl = gameObject.AddComponent<MeigeAnimCtrl>();
					this.m_ModelSets[i].m_MsbHndl = gameObject.GetComponent<MsbHandler>();
					this.m_ModelSets[i].m_MeigeAnimEvNotify = gameObject.GetComponent<MeigeAnimEventNotifier>();
					if (this.m_ModelSets[i].m_MeigeAnimEvNotify != null)
					{
						this.m_ModelSets[i].m_MeigeAnimEvNotify.AddNotify(new MeigeAnimEventNotifier.OnEvent(this.OnMeigeAnimEvent));
					}
					this.m_ModelSets[i].m_Transform = this.m_ModelSets[i].m_Obj.GetComponent<Transform>();
					this.m_ModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 180f, 0f);
					if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
					{
						if (i == 0)
						{
							this.m_ModelSets[i].m_RootTransform = transform.Find("Head_root");
						}
						else
						{
							this.m_ModelSets[i].m_RootTransform = transform.Find("root");
						}
					}
					else
					{
						this.m_ModelSets[i].m_RootTransform = transform.Find("root");
					}
				}
				else
				{
					this.m_ModelSets[i] = null;
				}
			}
			this.SetupFloatingObj();
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x00034684 File Offset: 0x00032A84
		public void AttachWeaponModel(GameObject[] modelObjs)
		{
			int num = modelObjs.Length;
			this.m_WeaponModelSets = new CharacterAnim.ModelSet[num];
			for (int i = 0; i < num; i++)
			{
				GameObject gameObject = modelObjs[i];
				if (gameObject != null)
				{
					this.m_WeaponModelSets[i] = new CharacterAnim.ModelSet();
					this.m_WeaponModelSets[i].m_Obj = gameObject;
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_WeaponModelSets[i].m_Obj.name, false);
					Transform transform = this.m_WeaponModelSets[i].m_Obj.transform.Find(name);
					this.m_WeaponModelSets[i].m_MsbHndl = transform.gameObject.GetComponent<MsbHandler>();
					this.m_WeaponModelSets[i].m_Transform = this.m_WeaponModelSets[i].m_Obj.GetComponent<Transform>();
					this.m_WeaponModelSets[i].m_Transform.rotation = Quaternion.Euler(0f, 0f, 0f);
					Transform handTransform = this.GetHandTransform(i == 0);
					if (handTransform != null)
					{
						this.m_WeaponModelSets[i].m_Transform.SetParent(handTransform, false);
					}
					for (int j = 0; j < transform.childCount; j++)
					{
						Transform child = transform.GetChild(j);
						if (child.name.Contains("WPN_"))
						{
							this.m_WeaponModelSets[i].m_RootTransform = child;
							break;
						}
					}
				}
				else
				{
					this.m_WeaponModelSets[i] = null;
				}
			}
		}

		// Token: 0x06000812 RID: 2066 RVA: 0x00034804 File Offset: 0x00032C04
		public void AttachShadow(GameObject modelObj)
		{
			this.m_Shadow = modelObj;
			Transform component = this.m_Shadow.GetComponent<Transform>();
			component.SetParent(this.m_Owner.CacheTransform, false);
			this.SetupShadowTransform(false);
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_Shadow.name, false);
			Transform transform = this.m_Shadow.transform.Find(name);
			this.m_ShadowMsbHndl = transform.gameObject.GetComponent<MsbHandler>();
		}

		// Token: 0x06000813 RID: 2067 RVA: 0x00034874 File Offset: 0x00032C74
		public void SetupShadowTransform(bool isReverse)
		{
			if (this.m_Shadow == null)
			{
				return;
			}
			Transform component = this.m_Shadow.GetComponent<Transform>();
			if (!isReverse)
			{
				component.rotation = Quaternion.Euler(0f, 180f, 0f);
				component.localPosition = new Vector3(0f, 0f, 0.3f);
			}
			else
			{
				component.rotation = Quaternion.Euler(0f, 0f, 0f);
				component.localPosition = new Vector3(0f, 0f, -0.3f);
			}
		}

		// Token: 0x06000814 RID: 2068 RVA: 0x00034914 File Offset: 0x00032D14
		public void Destroy()
		{
			if (this.m_FacialClipHolder != null)
			{
				this.m_FacialClipHolder = null;
			}
			if (this.m_Shadow != null)
			{
				UnityEngine.Object.Destroy(this.m_Shadow);
				this.m_Shadow = null;
			}
			if (this.m_ModelSets != null)
			{
				for (int i = 0; i < this.m_ModelSets.Length; i++)
				{
					if (this.m_ModelSets[i] != null)
					{
						this.m_ModelSets[i].Destroy();
						this.m_ModelSets[i] = null;
					}
				}
				this.m_ModelSets = null;
			}
			if (this.m_WeaponModelSets != null)
			{
				for (int j = 0; j < this.m_WeaponModelSets.Length; j++)
				{
					if (this.m_WeaponModelSets[j] != null)
					{
						this.m_WeaponModelSets[j].Destroy();
						this.m_WeaponModelSets[j] = null;
					}
				}
				this.m_WeaponModelSets = null;
			}
		}

		// Token: 0x06000815 RID: 2069 RVA: 0x000349F8 File Offset: 0x00032DF8
		public void ReAttachWeapon(CharacterDefine.eWeaponIndex wpnIndex)
		{
			if (this.m_WeaponModelSets == null)
			{
				return;
			}
			CharacterAnim.ModelSet modelSet = this.m_WeaponModelSets[(int)wpnIndex];
			if (modelSet == null)
			{
				return;
			}
			modelSet.m_Transform.localPosition = Vector3.zero;
			modelSet.m_Transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
			Transform handTransform = this.GetHandTransform(wpnIndex == CharacterDefine.eWeaponIndex.L);
			if (handTransform != null)
			{
				modelSet.m_Transform.SetParent(handTransform, false);
			}
		}

		// Token: 0x06000816 RID: 2070 RVA: 0x00034A74 File Offset: 0x00032E74
		public Transform GetHandTransform(bool isLeft)
		{
			if (this.m_ModelSets == null)
			{
				return null;
			}
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_ModelSets[1].m_Obj.name, false);
			Transform transform = this.m_ModelSets[1].m_Obj.transform.Find(name);
			return transform.Find(CharacterDefine.WEAPON_ATTACH_NAMES[(!isLeft) ? 1 : 0]);
		}

		// Token: 0x06000817 RID: 2071 RVA: 0x00034ADC File Offset: 0x00032EDC
		public void AnimCtrlOpen(int partsIndex)
		{
			if (this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.Open();
			}
		}

		// Token: 0x06000818 RID: 2072 RVA: 0x00034B2C File Offset: 0x00032F2C
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder, int partsIndex)
		{
			if (meigeAnimClipHolder != null && this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.AddClip(this.m_ModelSets[partsIndex].m_MsbHndl, meigeAnimClipHolder);
			}
		}

		// Token: 0x06000819 RID: 2073 RVA: 0x00034B98 File Offset: 0x00032F98
		public void AnimCtrlClose(int partsIndex)
		{
			if (this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.Close();
			}
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x00034BE8 File Offset: 0x00032FE8
		public void PlayAnim(string actionKey, WrapMode wrapMode, float blendSec = 0f, float offsetTime = 0f)
		{
			if (string.IsNullOrEmpty(actionKey))
			{
				return;
			}
			if (this.m_ModelSets == null)
			{
				return;
			}
			for (int i = 0; i < this.m_ModelSets.Length; i++)
			{
				if (this.m_ModelSets[i] != null && this.m_ModelSets[i].m_Anim != null)
				{
					this.m_ModelSets[i].m_MeigeAnimCtrl.Play(actionKey, blendSec, wrapMode);
					this.m_ModelSets[i].m_MeigeAnimCtrl.m_AnimationPlayTime = offsetTime;
				}
			}
			this.m_LastActionKey = actionKey;
			this.m_LastWrapMode = wrapMode;
		}

		// Token: 0x0600081B RID: 2075 RVA: 0x00034C84 File Offset: 0x00033084
		public int GetPlayingFrame(string actionKey, int partsIndex = 0)
		{
			if (this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.GetReferenceAnimHandler(actionKey);
				if (referenceAnimHandler != null)
				{
					string name = referenceAnimHandler.m_ClipTargetHandlerList[0].m_Clip.m_UnityAnimClip.name;
					AnimationState animationState = this.m_ModelSets[partsIndex].m_Anim[name];
					float num = 1f / animationState.clip.frameRate;
					float playingSec = this.GetPlayingSec(actionKey, partsIndex);
					return (int)(playingSec / num);
				}
			}
			return 0;
		}

		// Token: 0x0600081C RID: 2076 RVA: 0x00034D34 File Offset: 0x00033134
		public float GetPlayingSec(string actionKey, int partsIndex = 0)
		{
			if (this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.GetReferenceAnimHandler(actionKey);
				if (referenceAnimHandler != null)
				{
					return referenceAnimHandler.m_AnimationTime * this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.m_NormalizedPlayTime;
				}
			}
			return 0f;
		}

		// Token: 0x0600081D RID: 2077 RVA: 0x00034DAC File Offset: 0x000331AC
		public float GetMaxSec(string actionKey, int partsIndex = 0)
		{
			if (this.m_ModelSets != null && this.m_ModelSets[partsIndex] != null && this.m_ModelSets[partsIndex].m_MeigeAnimCtrl != null)
			{
				MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_ModelSets[partsIndex].m_MeigeAnimCtrl.GetReferenceAnimHandler(actionKey);
				if (referenceAnimHandler != null)
				{
					return referenceAnimHandler.m_AnimationTime;
				}
			}
			return 0f;
		}

		// Token: 0x0600081E RID: 2078 RVA: 0x00034E10 File Offset: 0x00033210
		public bool IsPlayingAnim(string actionKey)
		{
			if (this.m_LastActionKey != actionKey)
			{
				return false;
			}
			if (this.m_ModelSets == null)
			{
				return false;
			}
			for (int i = 0; i < this.m_ModelSets.Length; i++)
			{
				if (this.m_ModelSets[i] != null && this.m_ModelSets[i].m_MeigeAnimCtrl != null)
				{
					WrapMode wrapMode = this.m_ModelSets[i].m_MeigeAnimCtrl.m_WrapMode;
					if (wrapMode != WrapMode.ClampForever)
					{
						if (this.m_ModelSets[i].m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
						{
							return true;
						}
					}
					else if (this.m_ModelSets[i].m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600081F RID: 2079 RVA: 0x00034EDC File Offset: 0x000332DC
		public void Pause()
		{
			if (this.m_ModelSets == null)
			{
				return;
			}
			for (int i = 0; i < this.m_ModelSets.Length; i++)
			{
				if (this.m_ModelSets[i] != null && this.m_ModelSets[i].m_MeigeAnimCtrl != null)
				{
					this.m_ModelSets[i].m_MeigeAnimCtrl.Pause();
				}
			}
		}

		// Token: 0x06000820 RID: 2080 RVA: 0x00034F48 File Offset: 0x00033348
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
			int num = ev.m_iParam[0];
			if (num == 1)
			{
				this.SetFacial(ev.m_iParam[1], ev.m_iParam[2]);
			}
		}

		// Token: 0x06000821 RID: 2081 RVA: 0x00034F8A File Offset: 0x0003338A
		public void AddFacialAnim(MeigeAnimClipHolder meigeAnimClipHolder)
		{
			this.m_FacialClipHolder = meigeAnimClipHolder;
		}

		// Token: 0x06000822 RID: 2082 RVA: 0x00034F93 File Offset: 0x00033393
		public int GetFacialID()
		{
			return this.m_CurrentFacialID;
		}

		// Token: 0x06000823 RID: 2083 RVA: 0x00034F9C File Offset: 0x0003339C
		public void SetFacial(int facialID, int overrideSetID)
		{
			if (overrideSetID > 0)
			{
				int overrideFacialID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaFacialDB.GetOverrideFacialID(overrideSetID, this.m_Owner.CharaParam.NamedType);
				if (overrideFacialID > 0)
				{
					facialID = overrideFacialID;
				}
			}
			this.SetFacial(facialID);
		}

		// Token: 0x06000824 RID: 2084 RVA: 0x00034FE8 File Offset: 0x000333E8
		public void SetFacial(int facialID)
		{
			this.m_CurrentFacialID = facialID;
			if (this.m_FacialClipHolder == null)
			{
				return;
			}
			if (this.m_ModelSets == null)
			{
				return;
			}
			MsbHandler msbHndl = this.m_ModelSets[0].m_MsbHndl;
			int num = this.m_FacialClipHolder.m_MeigeAnimClip.m_AnimNodeHandlerArray.Length;
			for (int i = 0; i < num; i++)
			{
				MabAnimNodeHandler mabAnimNodeHandler = this.m_FacialClipHolder.m_MeigeAnimClip.m_AnimNodeHandlerArray[i];
				if (mabAnimNodeHandler.m_Target.m_TargetType == eAnimTargetType.eAnimTarget_MeshVisibility)
				{
					MsbObjectHandler msbObjectHandlerByName = msbHndl.GetMsbObjectHandlerByName(mabAnimNodeHandler.m_Target.m_TargetName);
					if (msbObjectHandlerByName != null)
					{
						int num2 = 0;
						int num3 = 0;
						int num4 = 0;
						for (int j = 0; j < mabAnimNodeHandler.m_Curves[num3].m_ComponentCurves[num4].m_KeyDatas.Length; j++)
						{
							int num5 = (int)mabAnimNodeHandler.m_Curves[num3].m_ComponentCurves[num4].m_KeyDatas[j].m_Frame;
							if (num5 <= facialID)
							{
								num2 = j;
								if (facialID == num5)
								{
									break;
								}
							}
						}
						msbObjectHandlerByName.GetWork().m_bVisibility = ((int)mabAnimNodeHandler.m_Curves[num3].m_ComponentCurves[num4].m_KeyDatas[num2].m_Value == 1);
						Renderer renderer = msbObjectHandlerByName.GetRenderer();
						if (renderer != null)
						{
							if (renderer.enabled != msbObjectHandlerByName.GetWork().m_bVisibility)
							{
								renderer.enabled = msbObjectHandlerByName.GetWork().m_bVisibility;
							}
						}
					}
				}
			}
		}

		// Token: 0x06000825 RID: 2085 RVA: 0x00035180 File Offset: 0x00033580
		public void ResetFacial()
		{
			CharacterDefine.eDir dir = this.m_Dir;
			if (dir == CharacterDefine.eDir.L || dir == CharacterDefine.eDir.R)
			{
				this.SetFacial(CharacterDefine.FACIAL_ID_DEFAULT[(int)this.m_Dir]);
			}
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x000351BD File Offset: 0x000335BD
		public bool IsEnableBlink()
		{
			return this.m_IsEnableBlink;
		}

		// Token: 0x06000827 RID: 2087 RVA: 0x000351C8 File Offset: 0x000335C8
		public void SetBlink(bool isEnableBlink)
		{
			this.m_IsEnableBlink = isEnableBlink;
			if (this.m_IsEnableBlink)
			{
				this.m_BlinkDir = this.Dir;
				int num = (this.Dir != CharacterDefine.eDir.L) ? 1 : 0;
				if (this.m_Owner.Mode == CharacterDefine.eMode.Battle)
				{
					this.SetFacial(CharacterDefine.FACIAL_ID_BLINK_BATTLE[num, 0]);
				}
				else
				{
					this.SetFacial(CharacterDefine.FACIAL_ID_BLINK[num, 0]);
				}
				this.m_BlinkCount = 0;
				this.m_BlinkOpen = true;
				this.m_BlinkType = CharacterAnim.eBlinkType.SLOW;
				float num2 = UnityEngine.Random.Range(0f, 100f);
				for (int i = 0; i < 3; i++)
				{
					if (num2 >= this.BLINK_TYPE_PROBABLITY[i, 0] && num2 <= this.BLINK_TYPE_PROBABLITY[i, 1])
					{
						this.m_BlinkType = (CharacterAnim.eBlinkType)i;
						break;
					}
				}
				this.m_BlinkTimer = UnityEngine.Random.Range(this.BLINK_OPEN_TIME_RANGE[0], this.BLINK_OPEN_TIME_RANGE[1]);
			}
		}

		// Token: 0x06000828 RID: 2088 RVA: 0x000352C4 File Offset: 0x000336C4
		private void UpdateBlink()
		{
			if (!this.m_IsEnableBlink)
			{
				return;
			}
			if (this.m_BlinkDir != this.Dir)
			{
				this.SetBlink(true);
			}
			this.m_BlinkTimer -= Time.deltaTime;
			if (this.m_BlinkTimer <= 0f)
			{
				int num = (this.Dir != CharacterDefine.eDir.L) ? 1 : 0;
				this.m_BlinkCount++;
				if (this.m_BlinkOpen)
				{
					if (this.m_Owner.Mode == CharacterDefine.eMode.Battle)
					{
						this.SetFacial(CharacterDefine.FACIAL_ID_BLINK_BATTLE[num, 1]);
					}
					else
					{
						this.SetFacial(CharacterDefine.FACIAL_ID_BLINK[num, 1]);
					}
					this.m_BlinkOpen = false;
					this.m_BlinkTimer = this.BLINK_CLOSE_TIME[(int)this.m_BlinkType];
				}
				else
				{
					if (this.m_Owner.Mode == CharacterDefine.eMode.Battle)
					{
						this.SetFacial(CharacterDefine.FACIAL_ID_BLINK_BATTLE[num, 0]);
					}
					else
					{
						this.SetFacial(CharacterDefine.FACIAL_ID_BLINK[num, 0]);
					}
					this.m_BlinkOpen = true;
					if (this.m_BlinkType == CharacterAnim.eBlinkType.TWO && this.m_BlinkCount <= 2)
					{
						this.m_BlinkType = CharacterAnim.eBlinkType.TWO;
						this.m_BlinkTimer = this.BLINK_CLOSE_TIME[2];
					}
					else
					{
						float num2 = UnityEngine.Random.Range(0f, 100f);
						for (int i = 0; i < 3; i++)
						{
							if (num2 >= this.BLINK_TYPE_PROBABLITY[i, 0] && num2 <= this.BLINK_TYPE_PROBABLITY[i, 1])
							{
								this.m_BlinkType = (CharacterAnim.eBlinkType)i;
								break;
							}
						}
						this.m_BlinkTimer = UnityEngine.Random.Range(this.BLINK_OPEN_TIME_RANGE[0], this.BLINK_OPEN_TIME_RANGE[1]);
						this.m_BlinkCount = 0;
					}
				}
			}
		}

		// Token: 0x06000829 RID: 2089 RVA: 0x00035484 File Offset: 0x00033884
		private void SetupFloatingObj()
		{
			if (this.m_ModelSets != null && this.m_ModelSets.Length > 1 && this.m_ModelSets[1] != null)
			{
				this.m_FloatingSets = new List<CharacterAnim.FloatingSet>();
				Transform refTransform = this.m_ModelSets[1].m_RootTransform.FindChild("Hips");
				string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_ModelSets[1].m_Obj.name, false);
				Transform transform = this.m_ModelSets[1].m_Obj.transform.Find(name);
				for (int i = 0; i < transform.childCount; i++)
				{
					Transform child = transform.GetChild(i);
					if (child.name.Contains("Ex_Float"))
					{
						CharacterAnim.FloatingSet floatingSet = new CharacterAnim.FloatingSet();
						floatingSet.m_Transform = child;
						floatingSet.m_RefTransform = refTransform;
						floatingSet.m_BaseOffset = floatingSet.m_Transform.localPosition;
						floatingSet.m_Float = UnityEngine.Random.Range(0f, 360f) + this.m_Owner.UniqueID % 360U;
						this.m_FloatingSets.Add(floatingSet);
						if (this.m_Owner.Mode != CharacterDefine.eMode.Room)
						{
							floatingSet.m_Transform.gameObject.SetActive(true);
						}
						else
						{
							floatingSet.m_Transform.gameObject.SetActive(false);
						}
					}
				}
			}
		}

		// Token: 0x0600082A RID: 2090 RVA: 0x000355E0 File Offset: 0x000339E0
		public void SetEnableFloatingObj(bool flg)
		{
			if (this.m_FloatingSets != null)
			{
				for (int i = 0; i < this.m_FloatingSets.Count; i++)
				{
					this.m_FloatingSets[i].m_Transform.gameObject.SetActive(flg);
				}
			}
		}

		// Token: 0x0600082B RID: 2091 RVA: 0x00035630 File Offset: 0x00033A30
		private void UpdateFloatingObj()
		{
			if (this.m_FloatingSets != null)
			{
				for (int i = 0; i < this.m_FloatingSets.Count; i++)
				{
					CharacterAnim.FloatingSet floatingSet = this.m_FloatingSets[i];
					floatingSet.m_Float += Time.deltaTime;
					Vector3 localPosition;
					localPosition.x = floatingSet.m_RefTransform.localPosition.x + floatingSet.m_BaseOffset.x;
					localPosition.y = floatingSet.m_BaseOffset.y + Mathf.Sin(floatingSet.m_Float * 1.9f) * 0.065f;
					localPosition.z = floatingSet.m_RefTransform.localPosition.z + floatingSet.m_BaseOffset.z;
					floatingSet.m_Transform.localPosition = localPosition;
					if (this.m_Dir == CharacterDefine.eDir.L)
					{
						floatingSet.m_Transform.localScaleX(1f);
					}
					else
					{
						floatingSet.m_Transform.localScaleX(-1f);
					}
				}
			}
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x00035738 File Offset: 0x00033B38
		public void ChangeMeshColor(Color startColor, Color endColor, float sec)
		{
			this.m_StartColor = startColor;
			this.m_EndColor = endColor;
			this.m_MeshColorTime = sec;
			this.m_MeshColorTimer = 0f;
			Color meshColor = this.m_StartColor;
			if (sec <= 0f)
			{
				meshColor = this.m_EndColor;
			}
			this.SetMeshColor(meshColor);
			if (this.m_MeshColorTime <= 0f)
			{
				this.m_IsUpdateMeshColor = false;
			}
			else
			{
				this.m_IsUpdateMeshColor = true;
			}
		}

		// Token: 0x0600082D RID: 2093 RVA: 0x000357A8 File Offset: 0x00033BA8
		public void ResetMeshColor()
		{
			this.m_IsUpdateMeshColor = false;
			this.SetMeshColor(Color.white);
		}

		// Token: 0x0600082E RID: 2094 RVA: 0x000357BC File Offset: 0x00033BBC
		private void UpdateMeshColor()
		{
			if (!this.m_IsUpdateMeshColor)
			{
				return;
			}
			this.m_MeshColorTimer += Time.deltaTime;
			float num = this.m_MeshColorTimer / this.m_MeshColorTime;
			if (num > 1f)
			{
				num = 1f;
			}
			Color meshColor = Color.Lerp(this.m_StartColor, this.m_EndColor, num);
			this.SetMeshColor(meshColor);
			if (num >= 1f)
			{
				this.m_IsUpdateMeshColor = false;
			}
		}

		// Token: 0x0600082F RID: 2095 RVA: 0x00035834 File Offset: 0x00033C34
		private void SetMeshColor(Color color)
		{
			this.m_CurrentColor = color;
			if (this.m_ModelSets != null)
			{
				for (int i = 0; i < this.m_ModelSets.Length; i++)
				{
					if (this.m_ModelSets[i] != null && this.m_ModelSets[i].m_MsbHndl != null)
					{
						int msbObjectHandlerNum = this.m_ModelSets[i].m_MsbHndl.GetMsbObjectHandlerNum();
						for (int j = 0; j < msbObjectHandlerNum; j++)
						{
							MsbObjectHandler msbObjectHandler = this.m_ModelSets[i].m_MsbHndl.GetMsbObjectHandler(j);
							MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
							if (work != null)
							{
								work.m_MeshColor = new Color(color.r, color.g, color.b, work.m_MeshColor.a);
								MsbObjectHandler.MsbObjectParam src = msbObjectHandler.m_Src;
								if (src != null)
								{
									Color color2 = src.m_OutlineColor * (color / 1f);
									work.m_OutlineColor = new Color(color2.r, color2.g, color2.b, work.m_OutlineColor.a);
								}
							}
						}
					}
				}
			}
			if (this.m_WeaponModelSets != null)
			{
				for (int k = 0; k < this.m_WeaponModelSets.Length; k++)
				{
					if (this.m_WeaponModelSets[k] != null && this.m_WeaponModelSets[k].m_MsbHndl != null)
					{
						int msbObjectHandlerNum2 = this.m_WeaponModelSets[k].m_MsbHndl.GetMsbObjectHandlerNum();
						for (int l = 0; l < msbObjectHandlerNum2; l++)
						{
							MsbObjectHandler msbObjectHandler2 = this.m_WeaponModelSets[k].m_MsbHndl.GetMsbObjectHandler(l);
							MsbObjectHandler.MsbObjectParam work2 = msbObjectHandler2.GetWork();
							if (work2 != null)
							{
								work2.m_MeshColor = new Color(color.r, color.g, color.b, work2.m_MeshColor.a);
								MsbObjectHandler.MsbObjectParam src2 = msbObjectHandler2.m_Src;
								if (src2 != null)
								{
									Color color3 = src2.m_OutlineColor * (color / 1f);
									work2.m_OutlineColor = new Color(color3.r, color3.g, color3.b, work2.m_OutlineColor.a);
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06000830 RID: 2096 RVA: 0x00035A80 File Offset: 0x00033E80
		public Color GetCurrentMeshColor()
		{
			return this.m_CurrentColor;
		}

		// Token: 0x06000831 RID: 2097 RVA: 0x00035A88 File Offset: 0x00033E88
		public void ChangeDir(CharacterDefine.eDir dir, bool isAnimChange = false)
		{
			this.m_Dir = dir;
			if (isAnimChange && this.IsPlayingAnim(this.m_LastActionKey))
			{
				int num = this.m_LastActionKey.LastIndexOf("_L");
				if (num >= 0 && this.m_Dir == CharacterDefine.eDir.R)
				{
					this.PlayAnim(this.m_LastActionKey.Substring(0, num) + "_R", this.m_LastWrapMode, 0f, 0f);
				}
				else
				{
					num = this.m_LastActionKey.LastIndexOf("_R");
					if (num >= 0 && this.m_Dir == CharacterDefine.eDir.L)
					{
						this.PlayAnim(this.m_LastActionKey.Substring(0, num) + "_L", this.m_LastWrapMode, 0f, 0f);
					}
				}
			}
		}

		// Token: 0x06000832 RID: 2098 RVA: 0x00035B5C File Offset: 0x00033F5C
		public void SetSortingOrder(int sortingOrder)
		{
			if (this.m_ModelSets != null)
			{
				for (int i = 0; i < this.m_ModelSets.Length; i++)
				{
					if (this.m_ModelSets[i] != null)
					{
						Renderer[] rendererCache = this.m_ModelSets[i].m_MsbHndl.GetRendererCache();
						for (int j = 0; j < rendererCache.Length; j++)
						{
							rendererCache[j].sortingOrder = sortingOrder;
						}
					}
				}
			}
			if (this.m_ShadowMsbHndl != null)
			{
				Renderer[] rendererCache2 = this.m_ShadowMsbHndl.GetRendererCache();
				if (rendererCache2 != null)
				{
					for (int k = 0; k < rendererCache2.Length; k++)
					{
						rendererCache2[k].sortingOrder = sortingOrder;
					}
				}
			}
		}

		// Token: 0x06000833 RID: 2099 RVA: 0x00035C10 File Offset: 0x00034010
		public int GetSortingOrder()
		{
			if (this.m_ModelSets != null)
			{
				for (int i = 0; i < this.m_ModelSets.Length; i++)
				{
					if (this.m_ModelSets[i] != null)
					{
						Renderer[] rendererCache = this.m_ModelSets[i].m_MsbHndl.GetRendererCache();
						if (rendererCache.Length > 0)
						{
							return rendererCache[0].sortingOrder;
						}
					}
				}
			}
			return 0;
		}

		// Token: 0x06000834 RID: 2100 RVA: 0x00035C74 File Offset: 0x00034074
		public Transform GetLocator(CharacterDefine.eLocatorIndex locatorIndex)
		{
			if (this.m_ModelSets != null)
			{
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					if (locatorIndex == CharacterDefine.eLocatorIndex.Body)
					{
						return this.m_ModelSets[1].m_RootTransform;
					}
					if (locatorIndex == CharacterDefine.eLocatorIndex.OverHead)
					{
						return this.m_ModelSets[0].m_RootTransform;
					}
				}
				else
				{
					string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_ModelSets[0].m_Transform.name, false);
					Transform transform = this.m_ModelSets[0].m_Transform.Find(name);
					if (transform != null)
					{
						Transform transform2 = transform.Find(CharacterDefine.EN_LOCATOR_NAMES[(int)locatorIndex]);
						if (transform2 == null)
						{
							transform2 = transform.Find((new string[]
							{
								"root/Hips/LOC_body",
								"root/Hips/LOC_overhead"
							})[(int)locatorIndex]);
						}
						return transform2;
					}
				}
			}
			return null;
		}

		// Token: 0x06000835 RID: 2101 RVA: 0x00035D4C File Offset: 0x0003414C
		public Vector3 GetLocatorPos(CharacterDefine.eLocatorIndex locatorIndex, bool isLocalPosition = false)
		{
			Transform transform;
			float num;
			return this.GetLocatorPos(locatorIndex, isLocalPosition, out transform, out num);
		}

		// Token: 0x06000836 RID: 2102 RVA: 0x00035D68 File Offset: 0x00034168
		public Vector3 GetLocatorPos(CharacterDefine.eLocatorIndex locatorIndex, bool isLocalPosition, out Transform out_locator, out float out_offsetYfromLocator)
		{
			Vector3 vector = Vector3.zero;
			out_locator = null;
			out_offsetYfromLocator = 0f;
			if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
			{
				if (locatorIndex != CharacterDefine.eLocatorIndex.Body)
				{
					if (locatorIndex == CharacterDefine.eLocatorIndex.OverHead)
					{
						out_locator = null;
						if (this.m_ModelSets != null && this.m_ModelSets.Length > 0)
						{
							out_locator = this.m_ModelSets[0].m_RootTransform;
						}
						if (out_locator == null)
						{
							out_locator = this.m_Owner.CacheTransform;
						}
						if (out_locator != null)
						{
							if (isLocalPosition)
							{
								vector = out_locator.localPosition;
							}
							else
							{
								vector = out_locator.position;
							}
							Vector3 vector2 = CharacterDefine.PL_OVERHEAD_OFFSET_FROM_HEAD;
							if (!isLocalPosition)
							{
								vector2 *= out_locator.parent.lossyScale.x;
							}
							out_offsetYfromLocator = vector2.y;
							vector += vector2;
						}
					}
				}
				else
				{
					out_locator = null;
					if (this.m_ModelSets != null && this.m_ModelSets.Length > 1)
					{
						out_locator = this.m_ModelSets[1].m_RootTransform;
					}
					if (out_locator == null)
					{
						out_locator = this.m_Owner.CacheTransform;
					}
					if (out_locator != null)
					{
						if (isLocalPosition)
						{
							vector = out_locator.localPosition;
						}
						else
						{
							vector = out_locator.position;
						}
						out_offsetYfromLocator = 0.5f;
						if (!isLocalPosition)
						{
							out_offsetYfromLocator *= out_locator.parent.lossyScale.x;
						}
						vector.y += out_offsetYfromLocator;
					}
				}
			}
			else
			{
				out_locator = this.GetLocator(locatorIndex);
				if (out_locator != null)
				{
					if (isLocalPosition)
					{
						vector = out_locator.localPosition;
					}
					else
					{
						vector = out_locator.position;
					}
				}
			}
			return vector;
		}

		// Token: 0x06000837 RID: 2103 RVA: 0x00035F40 File Offset: 0x00034340
		public Transform GetWeaponLocator(CharacterDefine.eWeaponIndex weaponIndex, int locatorIndex)
		{
			if (this.m_WeaponModelSets == null)
			{
				return null;
			}
			CharacterAnim.ModelSet modelSet = this.m_WeaponModelSets[(int)weaponIndex];
			if (modelSet == null)
			{
				return null;
			}
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(modelSet.m_Obj.name, false);
			Transform transform = modelSet.m_Obj.transform.Find(name);
			if (!(transform != null))
			{
				return null;
			}
			Transform transform2 = transform.Find("LOC_" + locatorIndex);
			if (transform2 != null)
			{
				return transform2;
			}
			return transform;
		}

		// Token: 0x06000838 RID: 2104 RVA: 0x00035FC4 File Offset: 0x000343C4
		public Vector3 GetWeaponLocatorPos(CharacterDefine.eWeaponIndex weaponIndex, int locatorIndex, bool isLocalPosition = false)
		{
			Vector3 result = Vector3.zero;
			Transform weaponLocator = this.GetWeaponLocator(weaponIndex, locatorIndex);
			if (weaponLocator != null)
			{
				if (isLocalPosition)
				{
					result = weaponLocator.localPosition;
				}
				else
				{
					result = weaponLocator.position;
				}
			}
			return result;
		}

		// Token: 0x06000839 RID: 2105 RVA: 0x00036008 File Offset: 0x00034408
		public List<Material> GetMaterials()
		{
			if (this.m_ModelSets == null)
			{
				return null;
			}
			List<Material> list = null;
			for (int i = 0; i < this.m_ModelSets.Length; i++)
			{
				if (this.m_ModelSets[i] != null && this.m_ModelSets[i].m_MsbHndl != null)
				{
					Renderer[] componentsInChildren = this.m_ModelSets[i].m_MsbHndl.GetComponentsInChildren<Renderer>(true);
					if (componentsInChildren != null)
					{
						for (int j = 0; j < componentsInChildren.Length; j++)
						{
							if (list == null)
							{
								list = new List<Material>();
							}
							if (!list.Contains(componentsInChildren[j].material))
							{
								list.Add(componentsInChildren[j].material);
							}
						}
					}
				}
			}
			return list;
		}

		// Token: 0x040007D8 RID: 2008
		private CharacterHandler m_Owner;

		// Token: 0x040007D9 RID: 2009
		private CharacterAnim.ModelSet[] m_ModelSets;

		// Token: 0x040007DA RID: 2010
		private CharacterAnim.ModelSet[] m_WeaponModelSets;

		// Token: 0x040007DB RID: 2011
		public MeigeAnimClipHolder m_FacialClipHolder;

		// Token: 0x040007DC RID: 2012
		private GameObject m_Shadow;

		// Token: 0x040007DD RID: 2013
		public MsbHandler m_ShadowMsbHndl;

		// Token: 0x040007DE RID: 2014
		private CharacterDefine.eDir m_Dir = CharacterDefine.eDir.R;

		// Token: 0x040007DF RID: 2015
		private string m_LastActionKey = string.Empty;

		// Token: 0x040007E0 RID: 2016
		private WrapMode m_LastWrapMode;

		// Token: 0x040007E1 RID: 2017
		private int m_CurrentFacialID = -1;

		// Token: 0x040007E2 RID: 2018
		private bool m_IsEnableBlink;

		// Token: 0x040007E3 RID: 2019
		private bool m_BlinkOpen = true;

		// Token: 0x040007E4 RID: 2020
		private int m_BlinkCount;

		// Token: 0x040007E5 RID: 2021
		private float m_BlinkTimer;

		// Token: 0x040007E6 RID: 2022
		private CharacterDefine.eDir m_BlinkDir;

		// Token: 0x040007E7 RID: 2023
		private CharacterAnim.eBlinkType m_BlinkType;

		// Token: 0x040007E8 RID: 2024
		private readonly float[,] BLINK_TYPE_PROBABLITY = new float[,]
		{
			{
				0f,
				60f
			},
			{
				60f,
				80f
			},
			{
				80f,
				100f
			}
		};

		// Token: 0x040007E9 RID: 2025
		private readonly float[] BLINK_CLOSE_TIME = new float[]
		{
			0.13f,
			0.1f,
			0.06f
		};

		// Token: 0x040007EA RID: 2026
		private readonly float[] BLINK_OPEN_TIME_RANGE = new float[]
		{
			3.5f,
			6.5f
		};

		// Token: 0x040007EB RID: 2027
		private List<CharacterAnim.FloatingSet> m_FloatingSets;

		// Token: 0x040007EC RID: 2028
		private Color m_StartColor;

		// Token: 0x040007ED RID: 2029
		private Color m_EndColor;

		// Token: 0x040007EE RID: 2030
		private Color m_CurrentColor = Color.white;

		// Token: 0x040007EF RID: 2031
		private float m_MeshColorTime;

		// Token: 0x040007F0 RID: 2032
		private float m_MeshColorTimer;

		// Token: 0x040007F1 RID: 2033
		private bool m_IsUpdateMeshColor;

		// Token: 0x02000137 RID: 311
		public class ModelSet
		{
			// Token: 0x0600083B RID: 2107 RVA: 0x000360C8 File Offset: 0x000344C8
			public void Destroy()
			{
				if (this.m_Obj != null)
				{
					UnityEngine.Object.Destroy(this.m_Obj);
					this.m_Obj = null;
				}
				this.m_Transform = null;
				this.m_RootTransform = null;
				this.m_Anim = null;
				this.m_MeigeAnimCtrl = null;
				this.m_MsbHndl = null;
				if (this.m_MeigeAnimEvNotify != null)
				{
					this.m_MeigeAnimEvNotify.ClearNotify();
				}
			}

			// Token: 0x040007F2 RID: 2034
			public GameObject m_Obj;

			// Token: 0x040007F3 RID: 2035
			public Transform m_Transform;

			// Token: 0x040007F4 RID: 2036
			public Transform m_RootTransform;

			// Token: 0x040007F5 RID: 2037
			public Animation m_Anim;

			// Token: 0x040007F6 RID: 2038
			public MeigeAnimCtrl m_MeigeAnimCtrl;

			// Token: 0x040007F7 RID: 2039
			public MeigeAnimEventNotifier m_MeigeAnimEvNotify;

			// Token: 0x040007F8 RID: 2040
			public MsbHandler m_MsbHndl;
		}

		// Token: 0x02000138 RID: 312
		private enum eBlinkType
		{
			// Token: 0x040007FA RID: 2042
			SLOW,
			// Token: 0x040007FB RID: 2043
			FAST,
			// Token: 0x040007FC RID: 2044
			TWO,
			// Token: 0x040007FD RID: 2045
			Num
		}

		// Token: 0x02000139 RID: 313
		public class FloatingSet
		{
			// Token: 0x040007FE RID: 2046
			public Transform m_Transform;

			// Token: 0x040007FF RID: 2047
			public Transform m_RefTransform;

			// Token: 0x04000800 RID: 2048
			public Vector3 m_BaseOffset = Vector3.zero;

			// Token: 0x04000801 RID: 2049
			public float m_Float;
		}
	}
}
