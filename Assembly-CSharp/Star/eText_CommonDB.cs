﻿using System;

namespace Star
{
	// Token: 0x02000237 RID: 567
	public enum eText_CommonDB
	{
		// Token: 0x04000F86 RID: 3974
		GameTitle,
		// Token: 0x04000F87 RID: 3975
		CommonType = 10,
		// Token: 0x04000F88 RID: 3976
		CommonType_Character,
		// Token: 0x04000F89 RID: 3977
		CommonType_Item,
		// Token: 0x04000F8A RID: 3978
		CommonType_Weapon,
		// Token: 0x04000F8B RID: 3979
		CommonType_TownFacility,
		// Token: 0x04000F8C RID: 3980
		CommonType_RoomObject,
		// Token: 0x04000F8D RID: 3981
		CommonType_MasterOrb,
		// Token: 0x04000F8E RID: 3982
		CommonType_Kirara,
		// Token: 0x04000F8F RID: 3983
		CommonType_Gold,
		// Token: 0x04000F90 RID: 3984
		CommonType_Gem,
		// Token: 0x04000F91 RID: 3985
		Gold = 100,
		// Token: 0x04000F92 RID: 3986
		Gem,
		// Token: 0x04000F93 RID: 3987
		GemStoneUnlimited,
		// Token: 0x04000F94 RID: 3988
		GemStoneLimit,
		// Token: 0x04000F95 RID: 3989
		KiraraPoint,
		// Token: 0x04000F96 RID: 3990
		Lv = 200,
		// Token: 0x04000F97 RID: 3991
		Exp,
		// Token: 0x04000F98 RID: 3992
		NextExp,
		// Token: 0x04000F99 RID: 3993
		Friendship,
		// Token: 0x04000F9A RID: 3994
		LevelText,
		// Token: 0x04000F9B RID: 3995
		Rare,
		// Token: 0x04000F9C RID: 3996
		ContentTitle,
		// Token: 0x04000F9D RID: 3997
		SkillLvUpRemain,
		// Token: 0x04000F9E RID: 3998
		SkillNoExist,
		// Token: 0x04000F9F RID: 3999
		CommonOK = 1000,
		// Token: 0x04000FA0 RID: 4000
		CommonYES,
		// Token: 0x04000FA1 RID: 4001
		CommonNo,
		// Token: 0x04000FA2 RID: 4002
		CommonDecide,
		// Token: 0x04000FA3 RID: 4003
		CommonCancel,
		// Token: 0x04000FA4 RID: 4004
		CommonClose,
		// Token: 0x04000FA5 RID: 4005
		CommonRetry,
		// Token: 0x04000FA6 RID: 4006
		CommonGoNext,
		// Token: 0x04000FA7 RID: 4007
		CommonGoBack,
		// Token: 0x04000FA8 RID: 4008
		CommonDefault,
		// Token: 0x04000FA9 RID: 4009
		CommonFilter,
		// Token: 0x04000FAA RID: 4010
		CommonSort,
		// Token: 0x04000FAB RID: 4011
		CommonAll,
		// Token: 0x04000FAC RID: 4012
		CommonEtc,
		// Token: 0x04000FAD RID: 4013
		CommonSkip,
		// Token: 0x04000FAE RID: 4014
		CommonNum,
		// Token: 0x04000FAF RID: 4015
		CommonTicketNum,
		// Token: 0x04000FB0 RID: 4016
		CommonTimeRemain,
		// Token: 0x04000FB1 RID: 4017
		CommonTimeDay,
		// Token: 0x04000FB2 RID: 4018
		CommonTimeHour,
		// Token: 0x04000FB3 RID: 4019
		CommonTimeMinute,
		// Token: 0x04000FB4 RID: 4020
		CommonTimeMin,
		// Token: 0x04000FB5 RID: 4021
		CommonSunday = 1030,
		// Token: 0x04000FB6 RID: 4022
		CommonMonday,
		// Token: 0x04000FB7 RID: 4023
		CommonTuesday,
		// Token: 0x04000FB8 RID: 4024
		CommonWednesday,
		// Token: 0x04000FB9 RID: 4025
		CommonThursday,
		// Token: 0x04000FBA RID: 4026
		CommonFriday,
		// Token: 0x04000FBB RID: 4027
		CommonSaturday,
		// Token: 0x04000FBC RID: 4028
		SortOrderASC = 1200,
		// Token: 0x04000FBD RID: 4029
		SortOrderDESC,
		// Token: 0x04000FBE RID: 4030
		RecommendParty,
		// Token: 0x04000FBF RID: 4031
		RecommendPriority,
		// Token: 0x04000FC0 RID: 4032
		FilterHeaderChara = 1210,
		// Token: 0x04000FC1 RID: 4033
		FilterHeaderWeapon,
		// Token: 0x04000FC2 RID: 4034
		FilterHeaderPresent,
		// Token: 0x04000FC3 RID: 4035
		FilterHeaderSupport,
		// Token: 0x04000FC4 RID: 4036
		FilterTitleSelectTitle,
		// Token: 0x04000FC5 RID: 4037
		FilterHeaderSelectTitle,
		// Token: 0x04000FC6 RID: 4038
		FilterRecommendParty = 1220,
		// Token: 0x04000FC7 RID: 4039
		FilterRandom = 1225,
		// Token: 0x04000FC8 RID: 4040
		FilterDeadline = 1230,
		// Token: 0x04000FC9 RID: 4041
		FilterPresentType,
		// Token: 0x04000FCA RID: 4042
		RecommendStatusPriority = 1240,
		// Token: 0x04000FCB RID: 4043
		SortOrderLv = 1300,
		// Token: 0x04000FCC RID: 4044
		SortOrderRare,
		// Token: 0x04000FCD RID: 4045
		SortOrderCost,
		// Token: 0x04000FCE RID: 4046
		SortOrderTitle,
		// Token: 0x04000FCF RID: 4047
		SortOrderHaveNum = 1204,
		// Token: 0x04000FD0 RID: 4048
		SortOrderGetTime = 1304,
		// Token: 0x04000FD1 RID: 4049
		SortOrderHp,
		// Token: 0x04000FD2 RID: 4050
		SortOrderAtk,
		// Token: 0x04000FD3 RID: 4051
		SortOrderMgc,
		// Token: 0x04000FD4 RID: 4052
		SortOrderDef,
		// Token: 0x04000FD5 RID: 4053
		SortOrderMDef,
		// Token: 0x04000FD6 RID: 4054
		SortOrderSpd,
		// Token: 0x04000FD7 RID: 4055
		Status = 2000,
		// Token: 0x04000FD8 RID: 4056
		StatusHp,
		// Token: 0x04000FD9 RID: 4057
		StatusAtk,
		// Token: 0x04000FDA RID: 4058
		StatusMgc,
		// Token: 0x04000FDB RID: 4059
		StatusDef,
		// Token: 0x04000FDC RID: 4060
		StatusMDef,
		// Token: 0x04000FDD RID: 4061
		StatusSpd,
		// Token: 0x04000FDE RID: 4062
		StatusLuck,
		// Token: 0x04000FDF RID: 4063
		CharaCost = 2010,
		// Token: 0x04000FE0 RID: 4064
		Class = 2100,
		// Token: 0x04000FE1 RID: 4065
		ClassFighter,
		// Token: 0x04000FE2 RID: 4066
		ClassMagician,
		// Token: 0x04000FE3 RID: 4067
		ClassPriest,
		// Token: 0x04000FE4 RID: 4068
		ClassKnight,
		// Token: 0x04000FE5 RID: 4069
		ClassAlchemist,
		// Token: 0x04000FE6 RID: 4070
		Element = 2200,
		// Token: 0x04000FE7 RID: 4071
		ElementFire,
		// Token: 0x04000FE8 RID: 4072
		ElementWater,
		// Token: 0x04000FE9 RID: 4073
		ElementEarth,
		// Token: 0x04000FEA RID: 4074
		ElementWind,
		// Token: 0x04000FEB RID: 4075
		ElementMoon,
		// Token: 0x04000FEC RID: 4076
		ElementSun,
		// Token: 0x04000FED RID: 4077
		DownloadCaption = 9999,
		// Token: 0x04000FEE RID: 4078
		GlobalUI,
		// Token: 0x04000FEF RID: 4079
		TotalLoginTitle,
		// Token: 0x04000FF0 RID: 4080
		TotalLoginText,
		// Token: 0x04000FF1 RID: 4081
		LoginBonusText,
		// Token: 0x04000FF2 RID: 4082
		Info = 14000,
		// Token: 0x04000FF3 RID: 4083
		InfoUpdate,
		// Token: 0x04000FF4 RID: 4084
		InfoEvent,
		// Token: 0x04000FF5 RID: 4085
		InfoBug,
		// Token: 0x04000FF6 RID: 4086
		WVGem0Title,
		// Token: 0x04000FF7 RID: 4087
		WVGem1Title,
		// Token: 0x04000FF8 RID: 4088
		WVRightTitle,
		// Token: 0x04000FF9 RID: 4089
		WVAgreementTitle,
		// Token: 0x04000FFA RID: 4090
		ItemDetail = 14100,
		// Token: 0x04000FFB RID: 4091
		StaminaShop = 15000,
		// Token: 0x04000FFC RID: 4092
		StaminaShopDetail,
		// Token: 0x04000FFD RID: 4093
		StaminaShopHaveNum,
		// Token: 0x04000FFE RID: 4094
		StaminaShopRecipeDetail,
		// Token: 0x04000FFF RID: 4095
		StaminaShopConfirm = 15010,
		// Token: 0x04001000 RID: 4096
		StaminaShopConfirmSub,
		// Token: 0x04001001 RID: 4097
		StaminaShopBefore,
		// Token: 0x04001002 RID: 4098
		StaminaShopAfter,
		// Token: 0x04001003 RID: 4099
		StaminaShopValue,
		// Token: 0x04001004 RID: 4100
		GemShopUI = 16000,
		// Token: 0x04001005 RID: 4101
		GemShopPlanTitle,
		// Token: 0x04001006 RID: 4102
		GemShopAmount,
		// Token: 0x04001007 RID: 4103
		GemShopURLButton0,
		// Token: 0x04001008 RID: 4104
		GemShopURLButton1,
		// Token: 0x04001009 RID: 4105
		GemShopHaveNum,
		// Token: 0x0400100A RID: 4106
		GemShopExecuteButton,
		// Token: 0x0400100B RID: 4107
		GemShopAgeConfirm = 16010,
		// Token: 0x0400100C RID: 4108
		GemShopAgeConfirmDetail,
		// Token: 0x0400100D RID: 4109
		GemShopAgeConfirmHeader,
		// Token: 0x0400100E RID: 4110
		GemShopAgeConfirmAge0 = 16020,
		// Token: 0x0400100F RID: 4111
		GemShopAgeConfirmAge1,
		// Token: 0x04001010 RID: 4112
		GemShopAgeConfirmAge2,
		// Token: 0x04001011 RID: 4113
		GemShopAgeConfirmLimit0 = 16030,
		// Token: 0x04001012 RID: 4114
		GemShopAgeConfirmLimit1,
		// Token: 0x04001013 RID: 4115
		GemShopAgeConfirmLimit2,
		// Token: 0x04001014 RID: 4116
		PresentTitle = 20100,
		// Token: 0x04001015 RID: 4117
		PresentDescript,
		// Token: 0x04001016 RID: 4118
		PresentHistory,
		// Token: 0x04001017 RID: 4119
		PresentGetAll,
		// Token: 0x04001018 RID: 4120
		PresentReturn,
		// Token: 0x04001019 RID: 4121
		PresentGet = 20200,
		// Token: 0x0400101A RID: 4122
		PresentDeadLine,
		// Token: 0x0400101B RID: 4123
		PresentGetDate,
		// Token: 0x0400101C RID: 4124
		PresentDate,
		// Token: 0x0400101D RID: 4125
		PresentNoExistDeadLine,
		// Token: 0x0400101E RID: 4126
		PresentFilterExistDeadLine = 20300,
		// Token: 0x0400101F RID: 4127
		PresentFilterNotExistDeadLine,
		// Token: 0x04001020 RID: 4128
		PresentGetResult = 20400,
		// Token: 0x04001021 RID: 4129
		PresentGetResultDescript,
		// Token: 0x04001022 RID: 4130
		TownMenuBuild = 31000,
		// Token: 0x04001023 RID: 4131
		TownSelectMenuObjName = 31100,
		// Token: 0x04001024 RID: 4132
		TownSelectMenuRoomIn,
		// Token: 0x04001025 RID: 4133
		TownSelectMenuSchedule,
		// Token: 0x04001026 RID: 4134
		TownSelectMenuInfo,
		// Token: 0x04001027 RID: 4135
		TownSelectMenuLevelUp,
		// Token: 0x04001028 RID: 4136
		TownSelectMenuQuick,
		// Token: 0x04001029 RID: 4137
		TownSelectMenuStore,
		// Token: 0x0400102A RID: 4138
		TownSelectMenuMove,
		// Token: 0x0400102B RID: 4139
		TownSelectMenuSwap,
		// Token: 0x0400102C RID: 4140
		TownConfirmBuildTitle = 31200,
		// Token: 0x0400102D RID: 4141
		TownConfirmBuildText,
		// Token: 0x0400102E RID: 4142
		TownConfirmPlacementTitle,
		// Token: 0x0400102F RID: 4143
		TownConfirmPlacementText,
		// Token: 0x04001030 RID: 4144
		TownConfirmQuickTitle,
		// Token: 0x04001031 RID: 4145
		TownConfirmQuickText,
		// Token: 0x04001032 RID: 4146
		TownConfirmLvupTitle,
		// Token: 0x04001033 RID: 4147
		TownConfirmLvupText,
		// Token: 0x04001034 RID: 4148
		TownConfirmSellTitle,
		// Token: 0x04001035 RID: 4149
		TownConfirmSellText,
		// Token: 0x04001036 RID: 4150
		TownConfirmContentPlacementTitle,
		// Token: 0x04001037 RID: 4151
		TownConfirmContentPlacementText,
		// Token: 0x04001038 RID: 4152
		TownConfirmContentSwapTitle,
		// Token: 0x04001039 RID: 4153
		TownConfirmContentSwapText,
		// Token: 0x0400103A RID: 4154
		TownBuildContentCheckTitle = 31250,
		// Token: 0x0400103B RID: 4155
		TownBuildContentCheckText,
		// Token: 0x0400103C RID: 4156
		TownBuildContentCheckButton,
		// Token: 0x0400103D RID: 4157
		TownBuildContentTitle,
		// Token: 0x0400103E RID: 4158
		TownBuildContentQuestion,
		// Token: 0x0400103F RID: 4159
		TownBuildContentSwapTitle,
		// Token: 0x04001040 RID: 4160
		TownBuildContentSwapQuestion,
		// Token: 0x04001041 RID: 4161
		TownBuildStockTitle = 31260,
		// Token: 0x04001042 RID: 4162
		TownBuildStockText,
		// Token: 0x04001043 RID: 4163
		TownBuildStockSubText,
		// Token: 0x04001044 RID: 4164
		TownBuildStockResultText,
		// Token: 0x04001045 RID: 4165
		TownBuildQuickResultTitle,
		// Token: 0x04001046 RID: 4166
		TownBuildQuickResultText,
		// Token: 0x04001047 RID: 4167
		TownBuildQuickResultErrTitle,
		// Token: 0x04001048 RID: 4168
		TownBuildQuickResultErrText,
		// Token: 0x04001049 RID: 4169
		TownBuildMoveTitle,
		// Token: 0x0400104A RID: 4170
		TownBuildAreaMoveText,
		// Token: 0x0400104B RID: 4171
		TownBuildAreaSwapText,
		// Token: 0x0400104C RID: 4172
		TownBuildObjMoveText,
		// Token: 0x0400104D RID: 4173
		TownBuildObjSwapText,
		// Token: 0x0400104E RID: 4174
		TownQuickFinalConfirmTitle,
		// Token: 0x0400104F RID: 4175
		TownQuickFinalConfirm,
		// Token: 0x04001050 RID: 4176
		TownBuildInfoTitle = 31220,
		// Token: 0x04001051 RID: 4177
		TownBuildInfoBuffTarget,
		// Token: 0x04001052 RID: 4178
		TownBuildInfoGold,
		// Token: 0x04001053 RID: 4179
		TownBuildInfoItem,
		// Token: 0x04001054 RID: 4180
		TownBuildInfoRoom,
		// Token: 0x04001055 RID: 4181
		TownBuildLevelUpConditionNone = 31299,
		// Token: 0x04001056 RID: 4182
		TownBuildLevelUpConditionUserLv,
		// Token: 0x04001057 RID: 4183
		TownBuildLevelUpConditionChapter,
		// Token: 0x04001058 RID: 4184
		TownBuildRemainDays = 31411,
		// Token: 0x04001059 RID: 4185
		TownBuildRemainHour,
		// Token: 0x0400105A RID: 4186
		TownBuildRemainMinute,
		// Token: 0x0400105B RID: 4187
		TownBuildRemainSec,
		// Token: 0x0400105C RID: 4188
		TownBuildTopTitle = 31500,
		// Token: 0x0400105D RID: 4189
		TownBuildTopCategorySelect,
		// Token: 0x0400105E RID: 4190
		TownBuildTopProduct,
		// Token: 0x0400105F RID: 4191
		TownBuildTopBuff,
		// Token: 0x04001060 RID: 4192
		TownBuildTopWarehouse,
		// Token: 0x04001061 RID: 4193
		TownMenuProductTitle = 31510,
		// Token: 0x04001062 RID: 4194
		TownMenuProductMessage,
		// Token: 0x04001063 RID: 4195
		TownMenuBuffTitle,
		// Token: 0x04001064 RID: 4196
		TownMenuBuffMessage,
		// Token: 0x04001065 RID: 4197
		TownMenuWarehouseTitle,
		// Token: 0x04001066 RID: 4198
		TownMenuWarehouseMessage,
		// Token: 0x04001067 RID: 4199
		TownMenuBuildEffectTitle,
		// Token: 0x04001068 RID: 4200
		TownMenuWarehouseStockNum,
		// Token: 0x04001069 RID: 4201
		TownBuffWindowNone = 33000,
		// Token: 0x0400106A RID: 4202
		TownBuffWindowCategoryProduction = 33010,
		// Token: 0x0400106B RID: 4203
		TownBuffWindowCategoryClass,
		// Token: 0x0400106C RID: 4204
		TownBuffWindowCategoryElement,
		// Token: 0x0400106D RID: 4205
		TownBuffWindowCategoryContent,
		// Token: 0x0400106E RID: 4206
		TownBuffWindowGold = 33020,
		// Token: 0x0400106F RID: 4207
		TownBuffWindowMinute,
		// Token: 0x04001070 RID: 4208
		TownBuffWindowClassFighter = 33030,
		// Token: 0x04001071 RID: 4209
		TownBuffWindowClassMagician,
		// Token: 0x04001072 RID: 4210
		TownBuffWindowClassPriest,
		// Token: 0x04001073 RID: 4211
		TownBuffWindowClassKnight,
		// Token: 0x04001074 RID: 4212
		TownBuffWindowClassAlchemist,
		// Token: 0x04001075 RID: 4213
		TownBuffWindowElementFire = 33040,
		// Token: 0x04001076 RID: 4214
		TownBuffWindowElementWater,
		// Token: 0x04001077 RID: 4215
		TownBuffWindowElementEarth,
		// Token: 0x04001078 RID: 4216
		TownBuffWindowElementWind,
		// Token: 0x04001079 RID: 4217
		TownBuffWindowElementMoon,
		// Token: 0x0400107A RID: 4218
		TownBuffWindowElementSun,
		// Token: 0x0400107B RID: 4219
		TownBuffWindowContentUpgrade = 33050,
		// Token: 0x0400107C RID: 4220
		TownItemUpToItem = 39000,
		// Token: 0x0400107D RID: 4221
		TownItemUpToMoney,
		// Token: 0x0400107E RID: 4222
		TownItemUpToStamina,
		// Token: 0x0400107F RID: 4223
		TownItemUpToKRRPoint,
		// Token: 0x04001080 RID: 4224
		TownItemUpToFriendShip,
		// Token: 0x04001081 RID: 4225
		TownGetProductTitle = 39010,
		// Token: 0x04001082 RID: 4226
		RoomObjectCategoryDesk = 40000,
		// Token: 0x04001083 RID: 4227
		RoomObjectCategoryChair,
		// Token: 0x04001084 RID: 4228
		RoomObjectCategoryStorage,
		// Token: 0x04001085 RID: 4229
		RoomObjectCategoryBedding,
		// Token: 0x04001086 RID: 4230
		RoomObjectCategoryAppliances,
		// Token: 0x04001087 RID: 4231
		RoomObjectCategoryGoods,
		// Token: 0x04001088 RID: 4232
		RoomObjectCategoryHobby,
		// Token: 0x04001089 RID: 4233
		RoomObjectCategoryWallDecoration,
		// Token: 0x0400108A RID: 4234
		RoomObjectCategoryCarpet,
		// Token: 0x0400108B RID: 4235
		RoomObjectCategoryScreen,
		// Token: 0x0400108C RID: 4236
		RoomObjectCategoryFloor,
		// Token: 0x0400108D RID: 4237
		RoomObjectCategoryWall,
		// Token: 0x0400108E RID: 4238
		RoomObjectCategoryBackground,
		// Token: 0x0400108F RID: 4239
		RoomSelectMenuMove = 41100,
		// Token: 0x04001090 RID: 4240
		RoomSelectMenuStore,
		// Token: 0x04001091 RID: 4241
		RoomSelectMenuPlacement,
		// Token: 0x04001092 RID: 4242
		RoomSelectMenuFlip,
		// Token: 0x04001093 RID: 4243
		RoomSelectMenuBuy,
		// Token: 0x04001094 RID: 4244
		RoomChangeBedding,
		// Token: 0x04001095 RID: 4245
		RoomObjListBuy = 41110,
		// Token: 0x04001096 RID: 4246
		RoomObjListPlacement,
		// Token: 0x04001097 RID: 4247
		RoomObjListSell,
		// Token: 0x04001098 RID: 4248
		RoomStore = 41500,
		// Token: 0x04001099 RID: 4249
		RoomShop,
		// Token: 0x0400109A RID: 4250
		RoomBuyTitle = 41600,
		// Token: 0x0400109B RID: 4251
		RoomBuyMessage,
		// Token: 0x0400109C RID: 4252
		RoomSellTitle,
		// Token: 0x0400109D RID: 4253
		RoomSellMessage,
		// Token: 0x0400109E RID: 4254
		RoomBuyResultTitle = 41610,
		// Token: 0x0400109F RID: 4255
		RoomBuyResultText,
		// Token: 0x040010A0 RID: 4256
		RoomBuyResultPlacement,
		// Token: 0x040010A1 RID: 4257
		RoomBuyResultNextPlacement,
		// Token: 0x040010A2 RID: 4258
		RoomStoreResultTitle,
		// Token: 0x040010A3 RID: 4259
		RoomStoreResultText,
		// Token: 0x040010A4 RID: 4260
		RoomMainRoom = 42000,
		// Token: 0x040010A5 RID: 4261
		RoomSubRoom,
		// Token: 0x040010A6 RID: 4262
		RoomSchdeduleRoomInChara,
		// Token: 0x040010A7 RID: 4263
		BattleWave = 50000,
		// Token: 0x040010A8 RID: 4264
		BattleMenuButton,
		// Token: 0x040010A9 RID: 4265
		BattleAutoButton,
		// Token: 0x040010AA RID: 4266
		BattleMiss = 50010,
		// Token: 0x040010AB RID: 4267
		BattleMasterSkillTitle = 51000,
		// Token: 0x040010AC RID: 4268
		BattleMasterSkillConfirmTitle,
		// Token: 0x040010AD RID: 4269
		BattleSkillConfirm,
		// Token: 0x040010AE RID: 4270
		BattleSkillTargetTitle,
		// Token: 0x040010AF RID: 4271
		BattleMenuTitle,
		// Token: 0x040010B0 RID: 4272
		BattleElementType,
		// Token: 0x040010B1 RID: 4273
		BattleHelp,
		// Token: 0x040010B2 RID: 4274
		BattleOption,
		// Token: 0x040010B3 RID: 4275
		BattleTownBuff = 51012,
		// Token: 0x040010B4 RID: 4276
		BattleRetire = 51008,
		// Token: 0x040010B5 RID: 4277
		BattleMenuDescriptAbnormalTitle,
		// Token: 0x040010B6 RID: 4278
		BattleMenuDescriptBuffTitle,
		// Token: 0x040010B7 RID: 4279
		BattleMasterNoOrb,
		// Token: 0x040010B8 RID: 4280
		BattleTogetherDescript = 51100,
		// Token: 0x040010B9 RID: 4281
		BattleFriendInterruptDescript,
		// Token: 0x040010BA RID: 4282
		BattleNoticeConfusion = 53000,
		// Token: 0x040010BB RID: 4283
		BattleNoticeParalysis,
		// Token: 0x040010BC RID: 4284
		BattleNoticeSleepOn,
		// Token: 0x040010BD RID: 4285
		BattleNoticeSleepOff_Single,
		// Token: 0x040010BE RID: 4286
		BattleNoticeSleepOff_Multiple,
		// Token: 0x040010BF RID: 4287
		BattleNoticeSilence,
		// Token: 0x040010C0 RID: 4288
		BattleNoticeIsolation,
		// Token: 0x040010C1 RID: 4289
		BattleNoticeExecutedMemberChange,
		// Token: 0x040010C2 RID: 4290
		BattleNoticeFriendCantUseMemberChange,
		// Token: 0x040010C3 RID: 4291
		BattleNoticeCantTogetherAttack,
		// Token: 0x040010C4 RID: 4292
		BattleNoticeStun,
		// Token: 0x040010C5 RID: 4293
		StateAbnormalConfusion = 55000,
		// Token: 0x040010C6 RID: 4294
		StateAbnormalParalysis,
		// Token: 0x040010C7 RID: 4295
		StateAbnormalPoison,
		// Token: 0x040010C8 RID: 4296
		StateAbnormalBearish,
		// Token: 0x040010C9 RID: 4297
		StateAbnormalSleep,
		// Token: 0x040010CA RID: 4298
		StateAbnormalUnhappy,
		// Token: 0x040010CB RID: 4299
		StateAbnormalSilence,
		// Token: 0x040010CC RID: 4300
		StateAbnormalIsolation,
		// Token: 0x040010CD RID: 4301
		BuffAtkUp,
		// Token: 0x040010CE RID: 4302
		BuffMgcUp,
		// Token: 0x040010CF RID: 4303
		BuffDefUp,
		// Token: 0x040010D0 RID: 4304
		BuffMDefUp,
		// Token: 0x040010D1 RID: 4305
		BuffSpdUp,
		// Token: 0x040010D2 RID: 4306
		BuffLuckUp,
		// Token: 0x040010D3 RID: 4307
		BuffAtkDown,
		// Token: 0x040010D4 RID: 4308
		BuffMgcDown,
		// Token: 0x040010D5 RID: 4309
		BuffDefDown,
		// Token: 0x040010D6 RID: 4310
		BuffMDefDown,
		// Token: 0x040010D7 RID: 4311
		BuffSpdDown,
		// Token: 0x040010D8 RID: 4312
		BuffLuckDown,
		// Token: 0x040010D9 RID: 4313
		BuffElementFireUp,
		// Token: 0x040010DA RID: 4314
		BuffElementWaterUp,
		// Token: 0x040010DB RID: 4315
		BuffElementEarthUp,
		// Token: 0x040010DC RID: 4316
		BuffElementWindUp,
		// Token: 0x040010DD RID: 4317
		BuffElementMoonUp,
		// Token: 0x040010DE RID: 4318
		BuffElementSunUp,
		// Token: 0x040010DF RID: 4319
		BuffElementFireDown,
		// Token: 0x040010E0 RID: 4320
		BuffElementWaterDown,
		// Token: 0x040010E1 RID: 4321
		BuffElementEarthDown,
		// Token: 0x040010E2 RID: 4322
		BuffElementWindDown,
		// Token: 0x040010E3 RID: 4323
		BuffElementMoonDown,
		// Token: 0x040010E4 RID: 4324
		BuffElementSunDown,
		// Token: 0x040010E5 RID: 4325
		BuffStateAbnormalDisable,
		// Token: 0x040010E6 RID: 4326
		BuffStateAbnormalAdditionalProbabilityUp,
		// Token: 0x040010E7 RID: 4327
		BuffStateAbnormalAdditionalProbabilityDown,
		// Token: 0x040010E8 RID: 4328
		BuffWeakElementBonus,
		// Token: 0x040010E9 RID: 4329
		BuffNextAtkUp,
		// Token: 0x040010EA RID: 4330
		BuffNextMgcUp,
		// Token: 0x040010EB RID: 4331
		BuffNextCritical,
		// Token: 0x040010EC RID: 4332
		BuffBarrier,
		// Token: 0x040010ED RID: 4333
		BuffChainBonusUp,
		// Token: 0x040010EE RID: 4334
		BuffHateUp,
		// Token: 0x040010EF RID: 4335
		BuffHateDown,
		// Token: 0x040010F0 RID: 4336
		BuffRegene,
		// Token: 0x040010F1 RID: 4337
		BattleResult = 60000,
		// Token: 0x040010F2 RID: 4338
		BattleResultRank,
		// Token: 0x040010F3 RID: 4339
		BattleResultDead0,
		// Token: 0x040010F4 RID: 4340
		BattleResultDead1,
		// Token: 0x040010F5 RID: 4341
		BattleResultDead2,
		// Token: 0x040010F6 RID: 4342
		BattleResultDead3,
		// Token: 0x040010F7 RID: 4343
		BattleResultRetry,
		// Token: 0x040010F8 RID: 4344
		BattleResultCharaExp = 61000,
		// Token: 0x040010F9 RID: 4345
		BattleResultPartyRewardExp,
		// Token: 0x040010FA RID: 4346
		BattleResultCharaRewardExp,
		// Token: 0x040010FB RID: 4347
		BattleResultCharaFriendship,
		// Token: 0x040010FC RID: 4348
		BattleResultPartyRewardFriendshipExp,
		// Token: 0x040010FD RID: 4349
		BattleResultCharaRewardFriendshipExp,
		// Token: 0x040010FE RID: 4350
		BattleResultRewardItem = 62000,
		// Token: 0x040010FF RID: 4351
		BattleResultRewardItemDescript,
		// Token: 0x04001100 RID: 4352
		BattleResultNext,
		// Token: 0x04001101 RID: 4353
		BattleFriendProposeTitle = 62010,
		// Token: 0x04001102 RID: 4354
		BattleFriendProposeText,
		// Token: 0x04001103 RID: 4355
		BattleFriendProposeYes,
		// Token: 0x04001104 RID: 4356
		BattleFriendProposeNo,
		// Token: 0x04001105 RID: 4357
		RewardFirstClear = 63000,
		// Token: 0x04001106 RID: 4358
		RewardFirstClearMessage,
		// Token: 0x04001107 RID: 4359
		RewardGroupClear,
		// Token: 0x04001108 RID: 4360
		RewardGroupClearMessage,
		// Token: 0x04001109 RID: 4361
		RewardCompleteClear,
		// Token: 0x0400110A RID: 4362
		RewardCompleteClearMessage,
		// Token: 0x0400110B RID: 4363
		RewardADV,
		// Token: 0x0400110C RID: 4364
		RewardADVMessage,
		// Token: 0x0400110D RID: 4365
		Edit = 90000,
		// Token: 0x0400110E RID: 4366
		EditTop = 91000,
		// Token: 0x0400110F RID: 4367
		PartyEdit = 92000,
		// Token: 0x04001110 RID: 4368
		PartyEditOpenPartyDetailButton,
		// Token: 0x04001111 RID: 4369
		PartyEditEmpty0,
		// Token: 0x04001112 RID: 4370
		PartyEditCheckResetWindowTitle,
		// Token: 0x04001113 RID: 4371
		PartyEditCheckResetWindowMessage,
		// Token: 0x04001114 RID: 4372
		PartyEditNotHaveClassWeaponMessage,
		// Token: 0x04001115 RID: 4373
		PartyEditQuestStartPanelScenarioSkip,
		// Token: 0x04001116 RID: 4374
		PartyEditNotHaveClassWeaponTitle,
		// Token: 0x04001117 RID: 4375
		PartyEditEditPartyNameWindow = 92100,
		// Token: 0x04001118 RID: 4376
		PartyEditEditPartyNameWindowTitle,
		// Token: 0x04001119 RID: 4377
		PartyEditEditPartyNameWindowMessage,
		// Token: 0x0400111A RID: 4378
		PartyEditPartyDetailWindow = 92200,
		// Token: 0x0400111B RID: 4379
		PartyEditPartyDetailWindowTitle,
		// Token: 0x0400111C RID: 4380
		SupportEdit = 93000,
		// Token: 0x0400111D RID: 4381
		SupportEditMessage,
		// Token: 0x0400111E RID: 4382
		SupportEditResetButton,
		// Token: 0x0400111F RID: 4383
		SupportEditCharacterListCostHeadText,
		// Token: 0x04001120 RID: 4384
		SupportEditEmpty0,
		// Token: 0x04001121 RID: 4385
		SupportEditEmpty1,
		// Token: 0x04001122 RID: 4386
		SupportEditFriendSupportMessage,
		// Token: 0x04001123 RID: 4387
		SupportEditEditPartyNameWindow = 93100,
		// Token: 0x04001124 RID: 4388
		SupportEditEditPartyNameWindowTitle,
		// Token: 0x04001125 RID: 4389
		SupportEditEditPartyNameWindowMessage,
		// Token: 0x04001126 RID: 4390
		SupportEditPartyDetailWindow = 93200,
		// Token: 0x04001127 RID: 4391
		SupportEditPartyDetailWindowTitle,
		// Token: 0x04001128 RID: 4392
		CharacterList = 94000,
		// Token: 0x04001129 RID: 4393
		CharacterListMessage,
		// Token: 0x0400112A RID: 4394
		CharacterListSortButton,
		// Token: 0x0400112B RID: 4395
		CharacterListFilterButton,
		// Token: 0x0400112C RID: 4396
		CharacterListHaveCharactersText,
		// Token: 0x0400112D RID: 4397
		Profile = 95000,
		// Token: 0x0400112E RID: 4398
		ProfileViewRadioButtonIllustButton,
		// Token: 0x0400112F RID: 4399
		ProfileViewRadioButtonSDButton,
		// Token: 0x04001130 RID: 4400
		ProfileProfileTabButtonStateButton,
		// Token: 0x04001131 RID: 4401
		ProfileProfileTabButtonProfileButton,
		// Token: 0x04001132 RID: 4402
		ProfileProfileTabButtonVoiceButton,
		// Token: 0x04001133 RID: 4403
		ProfileState = 95100,
		// Token: 0x04001134 RID: 4404
		ProfileStateFriendshipTitle,
		// Token: 0x04001135 RID: 4405
		ProfileStateFriendshipNextText,
		// Token: 0x04001136 RID: 4406
		ProfileStateUniqueSkillTitle,
		// Token: 0x04001137 RID: 4407
		ProfileStateGeneralSkillTitle,
		// Token: 0x04001138 RID: 4408
		ProfileStateEquipWeaponTitle,
		// Token: 0x04001139 RID: 4409
		ProfileStateEquipWeaponNoEquipButton,
		// Token: 0x0400113A RID: 4410
		ProfileStateCostText,
		// Token: 0x0400113B RID: 4411
		ProfileStateFriendshipMessage,
		// Token: 0x0400113C RID: 4412
		ProfileStateScheduleDrop1000,
		// Token: 0x0400113D RID: 4413
		ProfileStateScheduleDrop2000,
		// Token: 0x0400113E RID: 4414
		ProfileStateScheduleDrop3000,
		// Token: 0x0400113F RID: 4415
		ProfileStateEquipWeaponNoEquipMessage,
		// Token: 0x04001140 RID: 4416
		ProfileProfile = 95200,
		// Token: 0x04001141 RID: 4417
		ProfileProfileAppearanceTitleTitle,
		// Token: 0x04001142 RID: 4418
		ProfileProfileProfileTitle,
		// Token: 0x04001143 RID: 4419
		ProfileProfileCharacterVoiceTitle,
		// Token: 0x04001144 RID: 4420
		ProfileProfileCharacterVoiceTextHead,
		// Token: 0x04001145 RID: 4421
		ProfileVoice = 95300,
		// Token: 0x04001146 RID: 4422
		ProfileVoiceReleaseCondition,
		// Token: 0x04001147 RID: 4423
		ProfileStateDetailPercent = 95400,
		// Token: 0x04001148 RID: 4424
		ProfileStateDetailWindowTitle,
		// Token: 0x04001149 RID: 4425
		CharacterUpgrade = 96000,
		// Token: 0x0400114A RID: 4426
		CharacterUpgradeGetExpText,
		// Token: 0x0400114B RID: 4427
		CharacterUpgradeNeedMoneyText,
		// Token: 0x0400114C RID: 4428
		CharacterUpgradeResetButton,
		// Token: 0x0400114D RID: 4429
		CharacterLimitBreak = 96100,
		// Token: 0x0400114E RID: 4430
		CharacterLimitBreakTitle,
		// Token: 0x0400114F RID: 4431
		CharacterLimitBreakMessage,
		// Token: 0x04001150 RID: 4432
		CharacterLimitBreakNeedText,
		// Token: 0x04001151 RID: 4433
		CharacterLimitBreakHaveText,
		// Token: 0x04001152 RID: 4434
		CharacterLimitBreakSkillMaxLevelText,
		// Token: 0x04001153 RID: 4435
		CharacterLimitBreakAttentionMessage,
		// Token: 0x04001154 RID: 4436
		CharacterEvolve = 96200,
		// Token: 0x04001155 RID: 4437
		CharacterEvolveMessageTitle,
		// Token: 0x04001156 RID: 4438
		CharacterEvolveMessageRow1,
		// Token: 0x04001157 RID: 4439
		CharacterEvolveMessageRow2,
		// Token: 0x04001158 RID: 4440
		CharacterEvolveNormalEvolveTitle,
		// Token: 0x04001159 RID: 4441
		CharacterEvolveUniqueEvolveTitle,
		// Token: 0x0400115A RID: 4442
		CharacterEvolveEvoluveButton,
		// Token: 0x0400115B RID: 4443
		CharacterEvolveCannotText,
		// Token: 0x0400115C RID: 4444
		CharacterEvolveDetailButton,
		// Token: 0x0400115D RID: 4445
		CharacterEvolveNeedText,
		// Token: 0x0400115E RID: 4446
		CharacterEvolveHaveText,
		// Token: 0x0400115F RID: 4447
		CharacterEvolveNeedMoneyText,
		// Token: 0x04001160 RID: 4448
		CharacterEvolveFailedTitle,
		// Token: 0x04001161 RID: 4449
		CharacterEvolveFailed,
		// Token: 0x04001162 RID: 4450
		CharacterEvolveFailedRecipeNotFound,
		// Token: 0x04001163 RID: 4451
		CharacterEvolveFailedLimitBreak,
		// Token: 0x04001164 RID: 4452
		CharacterEvolveFailedCharaLv,
		// Token: 0x04001165 RID: 4453
		CharacterEvolveFailedMaterial,
		// Token: 0x04001166 RID: 4454
		CharacterEvolveFailedAmount,
		// Token: 0x04001167 RID: 4455
		CharacterResult = 97000,
		// Token: 0x04001168 RID: 4456
		MasterEquipSkillLimit = 98000,
		// Token: 0x04001169 RID: 4457
		WeaponParam = 99000,
		// Token: 0x0400116A RID: 4458
		WeaponEquipClass,
		// Token: 0x0400116B RID: 4459
		WeaponEquipExecute,
		// Token: 0x0400116C RID: 4460
		WeaponEquipTotalCost,
		// Token: 0x0400116D RID: 4461
		Quest = 100000,
		// Token: 0x0400116E RID: 4462
		QuestFriendSelectReload,
		// Token: 0x0400116F RID: 4463
		QuestFriendEmpty,
		// Token: 0x04001170 RID: 4464
		QuestCostStamina = 100010,
		// Token: 0x04001171 RID: 4465
		QuestCostItem,
		// Token: 0x04001172 RID: 4466
		QuestDropItem = 100020,
		// Token: 0x04001173 RID: 4467
		ADV = 110000,
		// Token: 0x04001174 RID: 4468
		ADVSkipWindowHeaderMessage,
		// Token: 0x04001175 RID: 4469
		ADVShowAllTextButton,
		// Token: 0x04001176 RID: 4470
		ADVSkipButton,
		// Token: 0x04001177 RID: 4471
		ADVSkipCancelButton,
		// Token: 0x04001178 RID: 4472
		ADVMenuHideButton,
		// Token: 0x04001179 RID: 4473
		ADVMenuLogButton,
		// Token: 0x0400117A RID: 4474
		ADVMenuAutoButton,
		// Token: 0x0400117B RID: 4475
		ADVMenuSkipButton,
		// Token: 0x0400117C RID: 4476
		ADVTalkWindowAutoButton,
		// Token: 0x0400117D RID: 4477
		GachaPlay = 120000,
		// Token: 0x0400117E RID: 4478
		GachaPlayResultWindowTitle,
		// Token: 0x0400117F RID: 4479
		GachaPlayResultWindowCloseButton,
		// Token: 0x04001180 RID: 4480
		GachaPlayResultWindowTalkMessage,
		// Token: 0x04001181 RID: 4481
		GachaPlayResultWindowTalkMessageRare,
		// Token: 0x04001182 RID: 4482
		GachaSelect = 125000,
		// Token: 0x04001183 RID: 4483
		GachaSelectInfoWebView,
		// Token: 0x04001184 RID: 4484
		GachaSelectInfoCharaList,
		// Token: 0x04001185 RID: 4485
		GachaSelectPeriod,
		// Token: 0x04001186 RID: 4486
		GachaSelectPeriodNoExist,
		// Token: 0x04001187 RID: 4487
		GachaSelectFree,
		// Token: 0x04001188 RID: 4488
		MissionTitle = 129999,
		// Token: 0x04001189 RID: 4489
		MissionTabNormal,
		// Token: 0x0400118A RID: 4490
		MissionTabLimit,
		// Token: 0x0400118B RID: 4491
		MissionTabDay,
		// Token: 0x0400118C RID: 4492
		MissionTabDrop,
		// Token: 0x0400118D RID: 4493
		MissionLimitTimeHour,
		// Token: 0x0400118E RID: 4494
		MissionLimitTimeMinute,
		// Token: 0x0400118F RID: 4495
		MissionLimitTimeSec,
		// Token: 0x04001190 RID: 4496
		MissionLimitTimeOver,
		// Token: 0x04001191 RID: 4497
		MissionButtonAllGet,
		// Token: 0x04001192 RID: 4498
		MissionButtonExchange,
		// Token: 0x04001193 RID: 4499
		MissionButtonChancel,
		// Token: 0x04001194 RID: 4500
		MissionButtonAdopt,
		// Token: 0x04001195 RID: 4501
		MissionButtonNoAdopt,
		// Token: 0x04001196 RID: 4502
		MissionLimitTimeDay,
		// Token: 0x04001197 RID: 4503
		MissionRewardNum,
		// Token: 0x04001198 RID: 4504
		MissionLimitTitle,
		// Token: 0x04001199 RID: 4505
		MissionTitleItemExchange = 130100,
		// Token: 0x0400119A RID: 4506
		MissionTitleExchangeComplete,
		// Token: 0x0400119B RID: 4507
		MissionTitleMissionComplete,
		// Token: 0x0400119C RID: 4508
		MissionTitleFriendshipLevelup,
		// Token: 0x0400119D RID: 4509
		MissionTitlePlayerInfomation,
		// Token: 0x0400119E RID: 4510
		MissionFriendshipLevelupMessage = 130200,
		// Token: 0x0400119F RID: 4511
		MissionQuestionMissionListen,
		// Token: 0x040011A0 RID: 4512
		MissionSegMsgLevel = 130300,
		// Token: 0x040011A1 RID: 4513
		MissionSegMsgUserName,
		// Token: 0x040011A2 RID: 4514
		MissionSegMsgLogIn,
		// Token: 0x040011A3 RID: 4515
		MissionSegMsgFriendEntry,
		// Token: 0x040011A4 RID: 4516
		MissionSegMsgHaveNum,
		// Token: 0x040011A5 RID: 4517
		MissionSegMsgHaveFix,
		// Token: 0x040011A6 RID: 4518
		MenuButtonUserInfomation = 140000,
		// Token: 0x040011A7 RID: 4519
		MenuButtonFriend,
		// Token: 0x040011A8 RID: 4520
		MenuButtonLibrary,
		// Token: 0x040011A9 RID: 4521
		MenuButtonHelp,
		// Token: 0x040011AA RID: 4522
		MenuButtonOption,
		// Token: 0x040011AB RID: 4523
		MenuButtonNontification,
		// Token: 0x040011AC RID: 4524
		MenuButtonOfficailSite,
		// Token: 0x040011AD RID: 4525
		MenuButtonTwitter,
		// Token: 0x040011AE RID: 4526
		MenuButtonSupport,
		// Token: 0x040011AF RID: 4527
		MenuButtonTitle,
		// Token: 0x040011B0 RID: 4528
		MenuButtonPlayerMove,
		// Token: 0x040011B1 RID: 4529
		MenuUserInfoNames = 140100,
		// Token: 0x040011B2 RID: 4530
		MenuUserInfoComment,
		// Token: 0x040011B3 RID: 4531
		MenuUserInfoUserCode,
		// Token: 0x040011B4 RID: 4532
		MenuUserInfoLimitGem,
		// Token: 0x040011B5 RID: 4533
		MenuUserInfoUnLimitGem,
		// Token: 0x040011B6 RID: 4534
		MenuUserInfoAllGem,
		// Token: 0x040011B7 RID: 4535
		MenuUserInfoNamesTitle,
		// Token: 0x040011B8 RID: 4536
		MenuUserInfoGemTitle,
		// Token: 0x040011B9 RID: 4537
		MenuUserInfoCopy,
		// Token: 0x040011BA RID: 4538
		MenuUserInfoNameChangeTitle = 140110,
		// Token: 0x040011BB RID: 4539
		MenuUserInfoNameChangeMessage,
		// Token: 0x040011BC RID: 4540
		MenuUserInfoCommentChangeTitle,
		// Token: 0x040011BD RID: 4541
		MenuUserInfoCommentChangeMessage,
		// Token: 0x040011BE RID: 4542
		MenuFriendWindowList = 140200,
		// Token: 0x040011BF RID: 4543
		MenuFriendWindowRequest,
		// Token: 0x040011C0 RID: 4544
		MenuFriendWindowFind,
		// Token: 0x040011C1 RID: 4545
		MenuFriendWindowInvitation,
		// Token: 0x040011C2 RID: 4546
		MenuFriendWindowNumTitle,
		// Token: 0x040011C3 RID: 4547
		MenuFriendWindowRankTitle,
		// Token: 0x040011C4 RID: 4548
		MenuFriendWindowFinalLoginTitle,
		// Token: 0x040011C5 RID: 4549
		MenuFriendWindowOKTitle,
		// Token: 0x040011C6 RID: 4550
		MenuFriendWindowNGTitle,
		// Token: 0x040011C7 RID: 4551
		MenuFriendWindowCancelTitle,
		// Token: 0x040011C8 RID: 4552
		MenuFriendWindowRoomInTitle,
		// Token: 0x040011C9 RID: 4553
		MenuFriendWindowSupportEditTitle,
		// Token: 0x040011CA RID: 4554
		MenuFriendWindowFriendCutTitle,
		// Token: 0x040011CB RID: 4555
		MenuFriendWindowFindTitle,
		// Token: 0x040011CC RID: 4556
		MenuFriendWindowIDCopyTitle,
		// Token: 0x040011CD RID: 4557
		MenuFriendWindowApplicationTitle,
		// Token: 0x040011CE RID: 4558
		MenuFriendWindowFindMessage,
		// Token: 0x040011CF RID: 4559
		MenuFriendWindowMyCode,
		// Token: 0x040011D0 RID: 4560
		MenuFriendWindowEmpty = 140250,
		// Token: 0x040011D1 RID: 4561
		MenuFriendWindowRequestEmpty,
		// Token: 0x040011D2 RID: 4562
		MenuFriendWindowSearchEmpty,
		// Token: 0x040011D3 RID: 4563
		MenuLibraryWindowArtwork = 140300,
		// Token: 0x040011D4 RID: 4564
		MenuLibraryWindowGlossary,
		// Token: 0x040011D5 RID: 4565
		MenuLibraryWindowViewing,
		// Token: 0x040011D6 RID: 4566
		MenuLibraryWindowEvent,
		// Token: 0x040011D7 RID: 4567
		MenuLibraryWindowMainScenario,
		// Token: 0x040011D8 RID: 4568
		MenuLibraryWindowEventScenario,
		// Token: 0x040011D9 RID: 4569
		MenuLibraryTitleURL = 140310,
		// Token: 0x040011DA RID: 4570
		MenuOptionWindowSound = 140400,
		// Token: 0x040011DB RID: 4571
		MenuOptionWindowBattle,
		// Token: 0x040011DC RID: 4572
		MenuOptionWindowTown,
		// Token: 0x040011DD RID: 4573
		MenuOptionWindowSoundBGM,
		// Token: 0x040011DE RID: 4574
		MenuOptionWindowSoundSE,
		// Token: 0x040011DF RID: 4575
		MenuOptionWindowSoundVoice,
		// Token: 0x040011E0 RID: 4576
		MenuOptionWindowBattlePlayerSpeed = 140410,
		// Token: 0x040011E1 RID: 4577
		MenuOptionWindowBattleEnemySpeed,
		// Token: 0x040011E2 RID: 4578
		MenuOptionWindowBattleTogetherSpeed,
		// Token: 0x040011E3 RID: 4579
		MenuOptionWindowBattleSkipTogetherAttackInput,
		// Token: 0x040011E4 RID: 4580
		MenuOptionWindowSkipGreet = 140420,
		// Token: 0x040011E5 RID: 4581
		MenuNotificationAll = 140500,
		// Token: 0x040011E6 RID: 4582
		MenuNotificationManagement,
		// Token: 0x040011E7 RID: 4583
		MenuNotificationStamina,
		// Token: 0x040011E8 RID: 4584
		MenuNotificationTown,
		// Token: 0x040011E9 RID: 4585
		MenuNotificationRoom,
		// Token: 0x040011EA RID: 4586
		MenuOtherTermOfService = 140600,
		// Token: 0x040011EB RID: 4587
		MenuOtherQuestion,
		// Token: 0x040011EC RID: 4588
		MenuOtherRightsExpression,
		// Token: 0x040011ED RID: 4589
		MenuOtherFundSettlement,
		// Token: 0x040011EE RID: 4590
		MenuOtherPurchaseHistory,
		// Token: 0x040011EF RID: 4591
		MenuOtherSpecificFinancial,
		// Token: 0x040011F0 RID: 4592
		MenuOtherPolicy,
		// Token: 0x040011F1 RID: 4593
		MenuPlayerMove = 140700,
		// Token: 0x040011F2 RID: 4594
		MenuPlayerMoveDescript,
		// Token: 0x040011F3 RID: 4595
		MenuPlayerMoveConfigure = 140710,
		// Token: 0x040011F4 RID: 4596
		MenuPlayerMoveConfigureDescript,
		// Token: 0x040011F5 RID: 4597
		MenuPlayerMoveConfigureDeadline,
		// Token: 0x040011F6 RID: 4598
		MenuPlayerMoveConfigureDeadlineValue,
		// Token: 0x040011F7 RID: 4599
		MenuADVGacha = 140800,
		// Token: 0x040011F8 RID: 4600
		MenuADVSingle,
		// Token: 0x040011F9 RID: 4601
		MenuADVCross,
		// Token: 0x040011FA RID: 4602
		MenuPlayerMoveSetting = 140900,
		// Token: 0x040011FB RID: 4603
		MenuPlayerMoveSetting_Android,
		// Token: 0x040011FC RID: 4604
		MenuPlayerMoveSetting_iOS,
		// Token: 0x040011FD RID: 4605
		MenuPlayerMoveSetting_App,
		// Token: 0x040011FE RID: 4606
		TitleLaunch = 150000,
		// Token: 0x040011FF RID: 4607
		TitleLaunchNotice,
		// Token: 0x04001200 RID: 4608
		TitleTitleMain = 151000,
		// Token: 0x04001201 RID: 4609
		TitleTitleMainUIVersionHeadText,
		// Token: 0x04001202 RID: 4610
		TitleTitleMainUIIDHeadText,
		// Token: 0x04001203 RID: 4611
		TitleTitleMainUICacheClearButton,
		// Token: 0x04001204 RID: 4612
		TitleTitleMainUIInheritanceButton,
		// Token: 0x04001205 RID: 4613
		TitleTitleMainUIDataClearButton,
		// Token: 0x04001206 RID: 4614
		TitleTitleMainAgree = 151100,
		// Token: 0x04001207 RID: 4615
		TitleTitleMainAgreeTitle,
		// Token: 0x04001208 RID: 4616
		TitleTitleMainAgreeLargeMessage,
		// Token: 0x04001209 RID: 4617
		TitleTitleMainAgreeMessage,
		// Token: 0x0400120A RID: 4618
		TitleTitleMainAgreeOpenAgreeButton,
		// Token: 0x0400120B RID: 4619
		TitleTitleMainAgreeAgreeButton,
		// Token: 0x0400120C RID: 4620
		TitleTitleMainAgreeNoAgreeButton,
		// Token: 0x0400120D RID: 4621
		TitleTitleMainCacheClear = 151200,
		// Token: 0x0400120E RID: 4622
		TitleTitleMainCacheClearTitle,
		// Token: 0x0400120F RID: 4623
		TitleTitleMainCacheClearMessage,
		// Token: 0x04001210 RID: 4624
		TitleTitleMainInheritance = 151300,
		// Token: 0x04001211 RID: 4625
		TitleTitleMainInheritanceTitle,
		// Token: 0x04001212 RID: 4626
		TitleTitleMainInheritanceIDText,
		// Token: 0x04001213 RID: 4627
		TitleTitleMainInheritancePasswordText,
		// Token: 0x04001214 RID: 4628
		TitleTitleMainInheritanceMessage,
		// Token: 0x04001215 RID: 4629
		TitleTitleMainInheritanceExecuteButton,
		// Token: 0x04001216 RID: 4630
		TitleTitleMainDataClear = 151400,
		// Token: 0x04001217 RID: 4631
		TitleTitleMainDataClearTitle,
		// Token: 0x04001218 RID: 4632
		TitleTitleMainDataClearMessageFrontCount,
		// Token: 0x04001219 RID: 4633
		TitleTitleMainDataClearMessageCount,
		// Token: 0x0400121A RID: 4634
		TitleTitleMainDataClearMessageAfterCount,
		// Token: 0x0400121B RID: 4635
		TitleTitleMainDataClearButton,
		// Token: 0x0400121C RID: 4636
		TitleTitleMainDataClearFinalTitle,
		// Token: 0x0400121D RID: 4637
		TitleTitleMainDataClearFinalMessage,
		// Token: 0x0400121E RID: 4638
		TitlePlayerMoveSelect = 151500,
		// Token: 0x0400121F RID: 4639
		TitlePlayerMoveSelect_Android,
		// Token: 0x04001220 RID: 4640
		TitlePlayerMoveSelect_iOS,
		// Token: 0x04001221 RID: 4641
		TitlePlayerMoveSelect_App,
		// Token: 0x04001222 RID: 4642
		Signup = 160000,
		// Token: 0x04001223 RID: 4643
		SignupTitle,
		// Token: 0x04001224 RID: 4644
		SignupMessage,
		// Token: 0x04001225 RID: 4645
		ShopTradeItemTypeUpgrade = 170001,
		// Token: 0x04001226 RID: 4646
		ShopTradeItemTypeEvolution,
		// Token: 0x04001227 RID: 4647
		ShopTradeItemTypeWeapon,
		// Token: 0x04001228 RID: 4648
		ShopTradeItemTypeEtc,
		// Token: 0x04001229 RID: 4649
		ShopTradeItemTypeLimitBreak,
		// Token: 0x0400122A RID: 4650
		ShopTradeItemTypeWeaponItem,
		// Token: 0x0400122B RID: 4651
		ShopTradeSrcExt = 170010,
		// Token: 0x0400122C RID: 4652
		ShopItemSale,
		// Token: 0x0400122D RID: 4653
		ShopWeaponSaleSelectRelease = 170020,
		// Token: 0x0400122E RID: 4654
		ShopWeaponSaleExec,
		// Token: 0x0400122F RID: 4655
		TrainingPrepare = 180001,
		// Token: 0x04001230 RID: 4656
		StoreReviewTitle = 190000,
		// Token: 0x04001231 RID: 4657
		StoreReviewMessage,
		// Token: 0x04001232 RID: 4658
		StoreReviewYes,
		// Token: 0x04001233 RID: 4659
		StoreReviewNo,
		// Token: 0x04001234 RID: 4660
		Max
	}
}
