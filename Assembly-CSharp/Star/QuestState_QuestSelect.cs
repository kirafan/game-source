﻿using System;
using Star.UI.Global;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000467 RID: 1127
	public class QuestState_QuestSelect : QuestState
	{
		// Token: 0x060015E2 RID: 5602 RVA: 0x000720D2 File Offset: 0x000704D2
		public QuestState_QuestSelect(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015E3 RID: 5603 RVA: 0x000720E9 File Offset: 0x000704E9
		public override int GetStateID()
		{
			return 4;
		}

		// Token: 0x060015E4 RID: 5604 RVA: 0x000720EC File Offset: 0x000704EC
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestSelect.eStep.First;
		}

		// Token: 0x060015E5 RID: 5605 RVA: 0x000720F5 File Offset: 0x000704F5
		public override void OnStateExit()
		{
		}

		// Token: 0x060015E6 RID: 5606 RVA: 0x000720F7 File Offset: 0x000704F7
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015E7 RID: 5607 RVA: 0x00072100 File Offset: 0x00070500
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestSelect.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = QuestState_QuestSelect.eStep.LoadWait;
				break;
			case QuestState_QuestSelect.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestSelect.eStep.PlayIn;
				}
				break;
			case QuestState_QuestSelect.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Owner.MasterUI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackADVPop(new Action(this.OnConfirmADVUnlock));
					this.m_Step = QuestState_QuestSelect.eStep.Main;
				}
				break;
			case QuestState_QuestSelect.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestSelect.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestSelect.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = QuestState_QuestSelect.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015E8 RID: 5608 RVA: 0x00072220 File Offset: 0x00070620
		private void OnConfirmADVUnlock()
		{
			this.GoToMenuEnd();
			this.m_NextState = 2147483646;
			this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			globalParam.SetAdvParam(globalParam.AdvID, SceneDefine.eSceneID.Quest, false);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.List;
		}

		// Token: 0x060015E9 RID: 5609 RVA: 0x00072274 File Offset: 0x00070674
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<QuestSelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			globalParam.QuestFriendData = null;
			globalParam.QuestUserSupportData = null;
			this.m_UI.Setup();
			this.m_UI.OnClickQuestButton += this.OnClickQuestButton;
			this.m_UI.OnClickChapterButton += this.OnClickChapterButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Quest);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015EA RID: 5610 RVA: 0x00072351 File Offset: 0x00070751
		private void OnClickChapterButton()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060015EB RID: 5611 RVA: 0x00072359 File Offset: 0x00070759
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060015EC RID: 5612 RVA: 0x00072368 File Offset: 0x00070768
		private void GoToMenuEnd()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
			{
				this.m_NextState = 2147483646;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			}
			else
			{
				QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
				QuestSelectUI.eButton selectButton = this.m_UI.SelectButton;
				if (selectButton != QuestSelectUI.eButton.Chapter)
				{
					if (selectButton != QuestSelectUI.eButton.Quest)
					{
						QuestManager.SelectQuest selectQuest = questMng.GetSelectQuest();
						if (selectQuest.m_EventType == -1)
						{
							this.m_NextState = 1;
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
						}
						else
						{
							this.m_NextState = 2;
						}
					}
					else
					{
						this.m_Owner.MasterUI.PlayOut();
						this.m_Owner.IsQuestStartConfirm = false;
						QuestManager.SelectQuest selectQuest2 = questMng.GetSelectQuest();
						if (selectQuest2.m_EventType == -1)
						{
							selectQuest2.m_Difficulty = this.m_UI.SelectDifficulty;
							selectQuest2.m_QuestID = this.m_UI.SelectQuestID;
						}
						else
						{
							selectQuest2.m_EventID = this.m_UI.SelectEventID;
							selectQuest2.m_QuestID = this.m_UI.SelectQuestID;
						}
						if (this.m_IsAdvOnly)
						{
							this.m_NextState = 2147483646;
						}
						else
						{
							this.m_Owner.SavePartyData();
							this.m_NextState = 5;
						}
					}
				}
				else
				{
					this.m_Owner.MasterUI.PlayOut();
					this.m_NextState = 3;
				}
			}
			this.m_UI.PlayOut();
		}

		// Token: 0x060015ED RID: 5613 RVA: 0x000724DC File Offset: 0x000708DC
		private void OnClickQuestButton()
		{
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			QuestManager.QuestData questData = questMng.GetQuestData(this.m_UI.SelectQuestID);
			if (questData != null && questData.m_Param.m_IsAdvOnly == 1)
			{
				this.m_IsAdvOnly = true;
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				globalParam.SetAdvParam(questData.m_Param.m_AdvID_Prev, SceneDefine.eSceneID.Quest, true);
				this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
				questMng.Request_QuestLogAddAdvOnly(questData.IsEventQuest(), this.m_UI.SelectQuestID, this.m_UI.SelectEventID, new Action<QuestDefine.eReturnLogAdd, string>(this.OnResponse_QuestLogAddAdvOnly));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060015EE RID: 5614 RVA: 0x00072588 File Offset: 0x00070988
		private void OnResponse_QuestLogAddAdvOnly(QuestDefine.eReturnLogAdd ret, string errorMessage)
		{
			if (ret != QuestDefine.eReturnLogAdd.Success)
			{
				APIUtility.ShowErrorWindowOk(errorMessage, null);
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x04001C9C RID: 7324
		private QuestState_QuestSelect.eStep m_Step = QuestState_QuestSelect.eStep.None;

		// Token: 0x04001C9D RID: 7325
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.QuestSelectUI;

		// Token: 0x04001C9E RID: 7326
		private QuestSelectUI m_UI;

		// Token: 0x04001C9F RID: 7327
		private bool m_IsAdvOnly;

		// Token: 0x02000468 RID: 1128
		private enum eStep
		{
			// Token: 0x04001CA1 RID: 7329
			None = -1,
			// Token: 0x04001CA2 RID: 7330
			First,
			// Token: 0x04001CA3 RID: 7331
			LoadWait,
			// Token: 0x04001CA4 RID: 7332
			PlayIn,
			// Token: 0x04001CA5 RID: 7333
			Main,
			// Token: 0x04001CA6 RID: 7334
			UnloadChildSceneWait
		}
	}
}
