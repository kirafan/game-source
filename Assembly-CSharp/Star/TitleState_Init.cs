﻿using System;
using AppRequestTypes;
using AppResponseTypes;
using Meige;
using Star.UI;
using WWWTypes;

namespace Star
{
	// Token: 0x0200049F RID: 1183
	public class TitleState_Init : TitleState
	{
		// Token: 0x06001739 RID: 5945 RVA: 0x00078A1C File Offset: 0x00076E1C
		public TitleState_Init(TitleMain owner) : base(owner)
		{
		}

		// Token: 0x0600173A RID: 5946 RVA: 0x00078A2C File Offset: 0x00076E2C
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600173B RID: 5947 RVA: 0x00078A2F File Offset: 0x00076E2F
		public override void OnStateEnter()
		{
			this.m_Step = TitleState_Init.eStep.First;
		}

		// Token: 0x0600173C RID: 5948 RVA: 0x00078A38 File Offset: 0x00076E38
		public override void OnStateExit()
		{
		}

		// Token: 0x0600173D RID: 5949 RVA: 0x00078A3A File Offset: 0x00076E3A
		public override void OnDispose()
		{
		}

		// Token: 0x0600173E RID: 5950 RVA: 0x00078A3C File Offset: 0x00076E3C
		public override int OnStateUpdate()
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			switch (this.m_Step)
			{
			case TitleState_Init.eStep.First:
				if (!(inst == null) && inst.IsAvailable())
				{
					if (inst.GlobalParam.IsRequestedReturnTitle)
					{
						inst.SoundMng.StopBGM();
						inst.GlobalParam.IsRequestedReturnTitle = false;
						inst.RequesetUnloadUnusedAssets(true);
						inst.LoadingUI.Abort();
						this.m_Step = TitleState_Init.eStep.FromReturnTitle_Wait;
					}
					else
					{
						this.m_Step = TitleState_Init.eStep.VersionGet;
					}
				}
				break;
			case TitleState_Init.eStep.FromReturnTitle_Wait:
				if (inst.IsCompleteUnloadUnusedAssets())
				{
					this.m_Step = TitleState_Init.eStep.VersionGet;
				}
				break;
			case TitleState_Init.eStep.VersionGet:
				this.m_Step = TitleState_Init.eStep.VersionGet_Wait;
				inst.LoadingUI.Open(LoadingUI.eDisplayMode.NowLoadingOnly, eTipsCategory.None);
				inst.LoadingUI.UpdateProgress(0f);
				this.Request_VersionGet(new MeigewwwParam.Callback(this.OnResponse_VersionGet));
				break;
			case TitleState_Init.eStep.SystemPrepare0:
				this.m_Step = TitleState_Init.eStep.SystemPrepare0_Wait;
				inst.PrepareNetwork();
				inst.LoadingUI.UpdateProgress(0.25f);
				break;
			case TitleState_Init.eStep.SystemPrepare0_Wait:
				if (inst.IsCompletePrepareNetowrk())
				{
					this.m_Step = TitleState_Init.eStep.SystemPrepare1_Wait;
					inst.PrepareAssetBundles();
					inst.LoadingUI.UpdateProgress(0.4f);
				}
				break;
			case TitleState_Init.eStep.SystemPrepare1_Wait:
				if (inst.IsCompletePrepareAssetBundles())
				{
					this.m_Step = TitleState_Init.eStep.SystemPrepare2_Wait;
					inst.PrepareDatabase();
					inst.LoadingUI.UpdateProgress(0.55f);
				}
				break;
			case TitleState_Init.eStep.SystemPrepare2_Wait:
				if (inst.IsCompletePrepareDatabase())
				{
					this.m_Step = TitleState_Init.eStep.SystemPrepare3_Wait;
					inst.CRIFileInstaller.InstallAcf(inst.DbMng.CRIFileVersionDB);
					inst.LoadingUI.UpdateProgress(0.7f);
				}
				break;
			case TitleState_Init.eStep.SystemPrepare3_Wait:
			{
				CRIFileInstaller crifileInstaller = inst.CRIFileInstaller;
				if (!crifileInstaller.IsInstalling)
				{
					if (crifileInstaller.IsError)
					{
						crifileInstaller.RetryInstall();
					}
					else
					{
						inst.PrepareSound(true);
						inst.LoadingUI.UpdateProgress(0.85f);
						this.m_Step = TitleState_Init.eStep.SystemPrepare4_Wait;
					}
				}
				break;
			}
			case TitleState_Init.eStep.SystemPrepare4_Wait:
				if (inst.IsCompletePrepareSound())
				{
					inst.LoadingUI.UpdateProgress(1f);
					inst.LoadingUI.Close();
					this.m_Step = TitleState_Init.eStep.None;
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600173F RID: 5951 RVA: 0x00078CA1 File Offset: 0x000770A1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x06001740 RID: 5952 RVA: 0x00078CAC File Offset: 0x000770AC
		private void Request_VersionGet(MeigewwwParam.Callback callback)
		{
			if (MeigeWWW.HttpHeaderCommon.ContainsKey("X-STAR-AB"))
			{
				MeigeWWW.HttpHeaderCommon.Remove("X-STAR-AB");
			}
			AppRequestTypes.Versionget versionget = new AppRequestTypes.Versionget();
			versionget.platform = Platform.Get();
			versionget.version = "1.0.3";
			NetworkQueueManager.SetAppVersion("1.0.3");
			MeigewwwParam wwwParam = AppRequest.Versionget(versionget, callback);
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06001741 RID: 5953 RVA: 0x00078D14 File Offset: 0x00077114
		private void OnResponse_VersionGet(MeigewwwParam wwwParam)
		{
			AppResponseTypes.Versionget versionget = AppResponse.Versionget(wwwParam, ResponseCommon.DialogType.None, null);
			if (versionget == null)
			{
				return;
			}
			ResultCode result = versionget.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result == ResultCode.APPLYING)
				{
					APIUtility.ApplyingOn(versionget.applyUrl, versionget.applys3Url);
				}
			}
			else
			{
				Version.ABVersion = versionget.abVer;
				MeigeWWW.HttpHeaderCommon.AddOrReplace("X-STAR-AB", Version.ABVersion.ToString());
			}
			this.m_Step = TitleState_Init.eStep.SystemPrepare0;
		}

		// Token: 0x04001DE6 RID: 7654
		private TitleState_Init.eStep m_Step = TitleState_Init.eStep.None;

		// Token: 0x020004A0 RID: 1184
		private enum eStep
		{
			// Token: 0x04001DE8 RID: 7656
			None = -1,
			// Token: 0x04001DE9 RID: 7657
			First,
			// Token: 0x04001DEA RID: 7658
			FromReturnTitle_Wait,
			// Token: 0x04001DEB RID: 7659
			VersionGet,
			// Token: 0x04001DEC RID: 7660
			VersionGet_Wait,
			// Token: 0x04001DED RID: 7661
			SystemPrepare0,
			// Token: 0x04001DEE RID: 7662
			SystemPrepare0_Wait,
			// Token: 0x04001DEF RID: 7663
			SystemPrepare1_Wait,
			// Token: 0x04001DF0 RID: 7664
			SystemPrepare2_Wait,
			// Token: 0x04001DF1 RID: 7665
			SystemPrepare3_Wait,
			// Token: 0x04001DF2 RID: 7666
			SystemPrepare4_Wait
		}
	}
}
