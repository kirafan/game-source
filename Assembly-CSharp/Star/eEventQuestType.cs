﻿using System;

namespace Star
{
	// Token: 0x02000230 RID: 560
	public enum eEventQuestType
	{
		// Token: 0x04000F53 RID: 3923
		DayOfTheWeek,
		// Token: 0x04000F54 RID: 3924
		Challenge,
		// Token: 0x04000F55 RID: 3925
		Blank1,
		// Token: 0x04000F56 RID: 3926
		Blank2
	}
}
