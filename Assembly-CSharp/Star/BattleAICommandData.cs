﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000094 RID: 148
	[Serializable]
	public class BattleAICommandData
	{
		// Token: 0x040002AB RID: 683
		public List<BattleAICommandCondition> m_Conditions = new List<BattleAICommandCondition>();

		// Token: 0x040002AC RID: 684
		public List<BattleAIExecData> m_ExecDatas = new List<BattleAIExecData>();

		// Token: 0x040002AD RID: 685
		public int m_ExecNum;
	}
}
