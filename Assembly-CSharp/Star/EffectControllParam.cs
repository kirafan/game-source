﻿using System;

namespace Star
{
	// Token: 0x020002EC RID: 748
	[Serializable]
	public abstract class EffectControllParam
	{
		// Token: 0x06000E8C RID: 3724
		public abstract void Apply(int index, EffectHandler effHndl);

		// Token: 0x040015AB RID: 5547
		public string[] m_TargetGoPaths;
	}
}
