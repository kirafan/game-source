﻿using System;

namespace Star
{
	// Token: 0x020005F8 RID: 1528
	public class RoomComSelectFloorObj : IRoomEventCommand
	{
		// Token: 0x06001E2D RID: 7725 RVA: 0x000A25E1 File Offset: 0x000A09E1
		public RoomComSelectFloorObj(int floorID, bool fselect)
		{
			this.m_Enable = true;
		}

		// Token: 0x06001E2E RID: 7726 RVA: 0x000A25F0 File Offset: 0x000A09F0
		public override bool CalcRequest(RoomBuilder pbuild)
		{
			bool result = false;
			int step = this.m_Step;
			if (step != 0)
			{
				if (step != 1)
				{
					if (step != 2)
					{
					}
				}
				else
				{
					result = true;
					this.m_Enable = false;
					this.m_Step++;
				}
			}
			else
			{
				this.m_Step++;
			}
			return result;
		}

		// Token: 0x040024B2 RID: 9394
		private int m_Step;
	}
}
