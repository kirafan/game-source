﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000559 RID: 1369
	public class CActScriptKeyObjPlayAnim : IRoomScriptData
	{
		// Token: 0x06001AE1 RID: 6881 RVA: 0x0008F180 File Offset: 0x0008D580
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyObjPlayAnim actXlsKeyObjPlayAnim = (ActXlsKeyObjPlayAnim)pbase;
			this.m_PlayAnimNo = actXlsKeyObjPlayAnim.m_PlayAnimNo;
			this.m_AnimeKey = actXlsKeyObjPlayAnim.m_AnimeKey;
			this.m_AnimeName = actXlsKeyObjPlayAnim.m_AnmName;
			this.m_Loop = ((!actXlsKeyObjPlayAnim.m_Loop) ? WrapMode.ClampForever : WrapMode.Loop);
		}

		// Token: 0x040021BA RID: 8634
		public int m_PlayAnimNo;

		// Token: 0x040021BB RID: 8635
		public int m_AnimeKey;

		// Token: 0x040021BC RID: 8636
		public string m_AnimeName;

		// Token: 0x040021BD RID: 8637
		public WrapMode m_Loop;
	}
}
