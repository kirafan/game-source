﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.BackGround;
using Star.UI.Edit;
using WWWTypes;

namespace Star
{
	// Token: 0x020003EF RID: 1007
	public class EditMain : GameStateMain
	{
		// Token: 0x17000162 RID: 354
		// (get) Token: 0x06001328 RID: 4904 RVA: 0x00066CAB File Offset: 0x000650AB
		// (set) Token: 0x06001329 RID: 4905 RVA: 0x00066CB3 File Offset: 0x000650B3
		public CharaListUI CharaListUI { get; set; }

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x0600132A RID: 4906 RVA: 0x00066CBC File Offset: 0x000650BC
		// (set) Token: 0x0600132B RID: 4907 RVA: 0x00066CC4 File Offset: 0x000650C4
		public float ScrollValue { get; set; }

		// Token: 0x0600132C RID: 4908 RVA: 0x00066CCD File Offset: 0x000650CD
		private void Start()
		{
			this.ScrollValue = 1f;
			base.SetNextState(0);
		}

		// Token: 0x0600132D RID: 4909 RVA: 0x00066CE4 File Offset: 0x000650E4
		private void OnDestroy()
		{
			this.CharaListUI = null;
			for (int i = 0; i < this.MixEffectScenes.Length; i++)
			{
				this.MixEffectScenes[i] = null;
			}
		}

		// Token: 0x0600132E RID: 4910 RVA: 0x00066D1A File Offset: 0x0006511A
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600132F RID: 4911 RVA: 0x00066D24 File Offset: 0x00065124
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			switch (nextStateID)
			{
			case 0:
				return new EditState_Init(this);
			case 1:
				return new EditState_EditTop(this);
			case 2:
				return new EditState_PartyEdit(this);
			case 3:
				return new EditState_CharaListPartyEdit(this);
			case 4:
				return new EditState_SupportEdit(this);
			case 5:
				return new EditState_CharaListSupportEdit(this);
			default:
				if (nextStateID != 2147483646)
				{
					return null;
				}
				return new CommonState_Final(this);
			case 7:
				return new EditState_MasterEquip(this);
			case 8:
				return new EditState_CharaListUpgrade(this);
			case 9:
				return new EditState_Upgrade(this);
			case 10:
				return new EditState_UpgradeResult(this);
			case 11:
				return new EditState_CharaListLimitBreak(this);
			case 12:
				return new EditState_LimitBreak(this);
			case 13:
				return new EditState_LimitBreakResult(this);
			case 14:
				return new EditState_CharaListEvolution(this);
			case 15:
				return new EditState_Evolution(this);
			case 16:
				return new EditState_EvolutionResult(this);
			case 17:
				return new EditState_CharaListView(this);
			}
		}

		// Token: 0x06001330 RID: 4912 RVA: 0x00066E18 File Offset: 0x00065218
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Mix);
			for (int i = 0; i < this.MixEffectScenes.Length; i++)
			{
				this.MixEffectScenes[i].Destroy();
			}
		}

		// Token: 0x06001331 RID: 4913 RVA: 0x00066E6D File Offset: 0x0006526D
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x06001332 RID: 4914 RVA: 0x00066E70 File Offset: 0x00065270
		public void SavePartyData()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData = new List<UserBattlePartyData>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas.Count; i++)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData.Add((UserBattlePartyData)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[i].DeepCopy());
			}
		}

		// Token: 0x06001333 RID: 4915 RVA: 0x00066EEC File Offset: 0x000652EC
		public void RevertPartyData()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData != null)
			{
				for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData.Count; i++)
				{
					this.RevertBattlePartyData(i);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData = null;
			}
		}

		// Token: 0x06001334 RID: 4916 RVA: 0x00066F4C File Offset: 0x0006534C
		private void RevertBattlePartyData(List<int> partyIndeces)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData != null)
			{
				for (int i = 0; i < partyIndeces.Count; i++)
				{
					this.RevertBattlePartyData(partyIndeces[i]);
				}
			}
		}

		// Token: 0x06001335 RID: 4917 RVA: 0x00066F91 File Offset: 0x00065391
		private void RevertBattlePartyData(int partyIndex)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex] = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SavedPartyData[partyIndex];
		}

		// Token: 0x06001336 RID: 4918 RVA: 0x00066FC0 File Offset: 0x000653C0
		public void Request_BattlePartySetAll(Action callback)
		{
			this.m_OnResultPartySetAllCallBack = callback;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			List<UserBattlePartyData> userBattlePartyDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas;
			this.overIdxs = new List<int>();
			for (int i = 0; i < userBattlePartyDatas.Count; i++)
			{
				int num = EditUtility.CalcPartyTotalCostAt(i);
				if (num > userDataMng.UserData.PartyCost)
				{
					this.overIdxs.Add(i);
				}
			}
			if (this.overIdxs.Count > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int j = 0; j < this.overIdxs.Count; j++)
				{
					if (j != 0)
					{
						stringBuilder.Append("\n");
					}
					stringBuilder.Append(userBattlePartyDatas[this.overIdxs[j]].PartyName);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PartyCostOverTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PartyCostOver), stringBuilder.ToString(), new Action<int>(this.OnConfirmCostOver));
				return;
			}
			PlayerRequestTypes.Battlepartysetall battlepartysetall = new PlayerRequestTypes.Battlepartysetall();
			battlepartysetall.battlePartyMembers = new PlayerBattlePartyMember[userBattlePartyDatas.Count];
			for (int k = 0; k < battlepartysetall.battlePartyMembers.Length; k++)
			{
				UserBattlePartyData userBattlePartyData = userBattlePartyDatas[k];
				battlepartysetall.battlePartyMembers[k] = new PlayerBattlePartyMember();
				battlepartysetall.battlePartyMembers[k].managedBattlePartyId = userBattlePartyData.MngID;
				int slotNum = userBattlePartyData.GetSlotNum();
				battlepartysetall.battlePartyMembers[k].managedCharacterIds = new long[slotNum];
				battlepartysetall.battlePartyMembers[k].managedWeaponIds = new long[slotNum];
				for (int l = 0; l < slotNum; l++)
				{
					battlepartysetall.battlePartyMembers[k].managedCharacterIds[l] = userBattlePartyData.GetMemberAt(l);
					battlepartysetall.battlePartyMembers[k].managedWeaponIds[l] = userBattlePartyData.GetWeaponAt(l);
				}
			}
			MeigewwwParam wwwParam = PlayerRequest.Battlepartysetall(battlepartysetall, new MeigewwwParam.Callback(this.OnResponse_BattlePartySetAll));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06001337 RID: 4919 RVA: 0x00067200 File Offset: 0x00065600
		private void OnConfirmCostOver(int btn)
		{
			if (btn == 0)
			{
				this.RevertBattlePartyData(this.overIdxs);
				this.Request_BattlePartySetAll(this.m_OnResultPartySetAllCallBack);
			}
		}

		// Token: 0x06001338 RID: 4920 RVA: 0x00067220 File Offset: 0x00065620
		private void OnResponse_BattlePartySetAll(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Battlepartysetall battlepartysetall = PlayerResponse.Battlepartysetall(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetall == null)
			{
				return;
			}
			ResultCode result = battlepartysetall.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				globalParam.IsDirtyPartyEdit = false;
				if (globalParam.SavedPartyData != null)
				{
					for (int i = 0; i < globalParam.SavedPartyData.Count; i++)
					{
						bool flag = false;
						UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserBattlePartyData(globalParam.SavedPartyData[i].MngID);
						if (userBattlePartyData != null)
						{
							for (int j = 0; j < globalParam.SavedPartyData[i].WeaponMngIDs.Length; j++)
							{
								if (globalParam.SavedPartyData[i].WeaponMngIDs[j] != userBattlePartyData.WeaponMngIDs[j])
								{
									flag = true;
									break;
								}
							}
						}
						if (flag)
						{
							MissionManager.UnlockAction(eXlsMissionSeg.Edit, eXlsMissionEditFuncType.WeaponEquipment);
							break;
						}
					}
				}
				this.m_OnResultPartySetAllCallBack.Call();
			}
		}

		// Token: 0x04001A1A RID: 6682
		public const int STATE_INIT = 0;

		// Token: 0x04001A1B RID: 6683
		public const int STATE_EDIT_TOP = 1;

		// Token: 0x04001A1C RID: 6684
		public const int STATE_PARTY_EDIT = 2;

		// Token: 0x04001A1D RID: 6685
		public const int STATE_CHARALIST_PARTY_EDIT = 3;

		// Token: 0x04001A1E RID: 6686
		public const int STATE_SUPPORT_EDIT = 4;

		// Token: 0x04001A1F RID: 6687
		public const int STATE_CHARALIST_SUPPORT_EDIT = 5;

		// Token: 0x04001A20 RID: 6688
		public const int STATE_WEAPONLIST_EQUIP = 6;

		// Token: 0x04001A21 RID: 6689
		public const int STATE_MASTER_EQUIP = 7;

		// Token: 0x04001A22 RID: 6690
		public const int STATE_CHARALIST_UPGRADE = 8;

		// Token: 0x04001A23 RID: 6691
		public const int STATE_UPGRADE = 9;

		// Token: 0x04001A24 RID: 6692
		public const int STATE_UPGRADE_RESULT = 10;

		// Token: 0x04001A25 RID: 6693
		public const int STATE_CHARALIST_LIMITBREAK = 11;

		// Token: 0x04001A26 RID: 6694
		public const int STATE_LIMITBREAK = 12;

		// Token: 0x04001A27 RID: 6695
		public const int STATE_LIMITBREAK_RESULT = 13;

		// Token: 0x04001A28 RID: 6696
		public const int STATE_CHARALIST_EVOLUTION = 14;

		// Token: 0x04001A29 RID: 6697
		public const int STATE_EVOLUTION = 15;

		// Token: 0x04001A2A RID: 6698
		public const int STATE_EVOLUTION_RESULT = 16;

		// Token: 0x04001A2B RID: 6699
		public const int STATE_CHARALIST_VIEW = 17;

		// Token: 0x04001A2C RID: 6700
		public EditMain.UpgradeResultData UpgradeResult;

		// Token: 0x04001A2D RID: 6701
		public EditMain.LimitBreakResultData LimitBreakResult;

		// Token: 0x04001A2E RID: 6702
		public EditMain.EvolutionResultData EvolutionResult;

		// Token: 0x04001A30 RID: 6704
		public MixEffectScene[] MixEffectScenes;

		// Token: 0x04001A32 RID: 6706
		protected Action m_OnResultPartySetAllCallBack;

		// Token: 0x04001A33 RID: 6707
		private List<int> overIdxs = new List<int>();

		// Token: 0x020003F0 RID: 1008
		public class EditAdditiveSceneResultData
		{
			// Token: 0x04001A34 RID: 6708
			public long m_CharaMngID = -1L;
		}

		// Token: 0x020003F1 RID: 1009
		public class UpgradeResultData : EditMain.EditAdditiveSceneResultData
		{
			// Token: 0x04001A35 RID: 6709
			public int m_SuccessType;

			// Token: 0x04001A36 RID: 6710
			public int m_BeforeLv;

			// Token: 0x04001A37 RID: 6711
			public long m_BeforeExp;

			// Token: 0x04001A38 RID: 6712
			public int m_AfterLv;

			// Token: 0x04001A39 RID: 6713
			public long m_AfterExp;
		}

		// Token: 0x020003F2 RID: 1010
		public class LimitBreakResultData : EditMain.EditAdditiveSceneResultData
		{
			// Token: 0x04001A3A RID: 6714
			public int m_BeforeMaxLv;

			// Token: 0x04001A3B RID: 6715
			public int m_BeforeMaxUniqueSkillLv;

			// Token: 0x04001A3C RID: 6716
			public int[] m_BeforeMaxClassSkillLv;

			// Token: 0x04001A3D RID: 6717
			public int m_AfterMaxLv;

			// Token: 0x04001A3E RID: 6718
			public int m_AfterMaxUniqueSkillLv;

			// Token: 0x04001A3F RID: 6719
			public int[] m_AfterMaxClassSkillLv;
		}

		// Token: 0x020003F3 RID: 1011
		public class EvolutionResultData : EditMain.EditAdditiveSceneResultData
		{
			// Token: 0x04001A40 RID: 6720
			public int m_SrcCharaID = -1;

			// Token: 0x04001A41 RID: 6721
			public int m_DestCharaID = -1;
		}
	}
}
