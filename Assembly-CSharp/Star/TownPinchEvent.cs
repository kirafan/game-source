﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200067C RID: 1660
	public class TownPinchEvent
	{
		// Token: 0x1700024E RID: 590
		// (get) Token: 0x0600218A RID: 8586 RVA: 0x000B2AF6 File Offset: 0x000B0EF6
		// (set) Token: 0x0600218B RID: 8587 RVA: 0x000B2AFE File Offset: 0x000B0EFE
		public float InitialPinchDist { get; set; }

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x0600218C RID: 8588 RVA: 0x000B2B07 File Offset: 0x000B0F07
		// (set) Token: 0x0600218D RID: 8589 RVA: 0x000B2B0F File Offset: 0x000B0F0F
		public bool IsPinching { get; set; }

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x0600218E RID: 8590 RVA: 0x000B2B18 File Offset: 0x000B0F18
		// (set) Token: 0x0600218F RID: 8591 RVA: 0x000B2B20 File Offset: 0x000B0F20
		public Vector2 FocusedPos { get; set; }

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06002190 RID: 8592 RVA: 0x000B2B29 File Offset: 0x000B0F29
		// (set) Token: 0x06002191 RID: 8593 RVA: 0x000B2B31 File Offset: 0x000B0F31
		public float PinchRatio { get; set; }

		// Token: 0x06002192 RID: 8594 RVA: 0x000B2B3C File Offset: 0x000B0F3C
		public void UpdatePinch(TownPinchState pstate)
		{
			pstate.m_Event = false;
			if (Input.touchCount == 2)
			{
				if (!this.IsPinching)
				{
					this.InitialPinchDist = this.CalcPinchDist();
					this.m_CurrentPinchDist = this.InitialPinchDist;
					this.PinchRatio = 1f;
					this.FocusedPos = this.CalcFocusPos();
					this.IsPinching = true;
					pstate.m_Event = true;
					pstate.IsPinching = this.IsPinching;
					pstate.PinchRatio = this.PinchRatio;
				}
				else
				{
					this.m_OldPinchRatio = this.PinchRatio;
					this.m_CurrentPinchDist = this.CalcPinchDist();
					this.PinchRatio = this.m_CurrentPinchDist / this.InitialPinchDist;
					if (this.m_OldPinchRatio != this.m_CurrentPinchDist)
					{
						pstate.m_Event = true;
						pstate.IsPinching = this.IsPinching;
						pstate.PinchRatio = this.PinchRatio;
					}
				}
			}
			else if (this.IsPinching)
			{
				this.m_CurrentPinchDist = 1f;
				this.InitialPinchDist = 1f;
				this.PinchRatio = -1f;
				this.IsPinching = false;
				pstate.m_Event = true;
				pstate.IsPinching = this.IsPinching;
				pstate.PinchRatio = this.PinchRatio;
			}
		}

		// Token: 0x06002193 RID: 8595 RVA: 0x000B2C74 File Offset: 0x000B1074
		private Vector2 CalcFocusPos()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position + position2) * 0.5f;
			}
			return Input.mousePosition;
		}

		// Token: 0x06002194 RID: 8596 RVA: 0x000B2CCC File Offset: 0x000B10CC
		private float CalcPinchDist()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position - position2).magnitude;
			}
			return 0f;
		}

		// Token: 0x040027F4 RID: 10228
		private float m_CurrentPinchDist;

		// Token: 0x040027F5 RID: 10229
		private float m_OldPinchRatio;
	}
}
