﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000EF RID: 239
	public class BattleTimeScaler
	{
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060006DC RID: 1756 RVA: 0x00028EF0 File Offset: 0x000272F0
		// (set) Token: 0x060006DD RID: 1757 RVA: 0x00028EF8 File Offset: 0x000272F8
		public bool IsDirty { get; set; }

		// Token: 0x060006DE RID: 1758 RVA: 0x00028F04 File Offset: 0x00027304
		public void Update()
		{
			if (this.m_BeforeTime == -1f)
			{
				this.m_BeforeTime = Time.realtimeSinceStartup;
			}
			float num = Time.realtimeSinceStartup - this.m_BeforeTime;
			this.m_BeforeTime = Time.realtimeSinceStartup;
			if (this.m_IsChangeTimeScale)
			{
				this.m_ChangeTimeScaleSec -= num;
				if (this.m_ChangeTimeScaleSec < 0f)
				{
					this.m_ChangeTimeScaleSec = 0f;
				}
				if (this.m_ChangeTimeScaleSecRevert > 0f && this.m_ChangeTimeScaleSec <= this.m_ChangeTimeScaleSecRevert)
				{
					float num2 = (this.m_ChangeTimeScaleSecRevert - this.m_ChangeTimeScaleSec) / this.m_ChangeTimeScaleSecRevert;
					num2 = Mathf.Clamp(num2, 0f, 1f);
					this.m_ChangeTimeScale = Mathf.Lerp(this.m_ChangeTimeScaleInit, 1f, num2);
				}
				if (this.m_ChangeTimeScaleSec <= 0f)
				{
					this.m_ChangeTimeScale = 1f;
					this.m_IsChangeTimeScale = false;
				}
			}
			Time.timeScale = this.m_BaseTimeScale * this.m_ChangeTimeScale;
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x0002900A File Offset: 0x0002740A
		public void SetBaseTimeScale(float baseTimeScale)
		{
			this.m_BaseTimeScale = baseTimeScale;
		}

		// Token: 0x060006E0 RID: 1760 RVA: 0x00029014 File Offset: 0x00027414
		public void SetChangeTimeScale(float changeTimeScale, float changeTimeScaleSec, float changeTimeScaleSecRevertRatio = 1f)
		{
			this.m_ChangeTimeScaleInit = changeTimeScale;
			this.m_ChangeTimeScale = changeTimeScale;
			this.m_ChangeTimeScaleSec = changeTimeScaleSec;
			this.m_ChangeTimeScaleSecRevert = changeTimeScaleSec - this.m_ChangeTimeScaleSec * changeTimeScaleSecRevertRatio;
			this.m_IsChangeTimeScale = true;
		}

		// Token: 0x040005E8 RID: 1512
		private float m_BeforeTime = -1f;

		// Token: 0x040005E9 RID: 1513
		private float m_BaseTimeScale = 1f;

		// Token: 0x040005EA RID: 1514
		private float m_ChangeTimeScale = 1f;

		// Token: 0x040005EB RID: 1515
		private float m_ChangeTimeScaleInit = 1f;

		// Token: 0x040005EC RID: 1516
		private float m_ChangeTimeScaleSec;

		// Token: 0x040005ED RID: 1517
		private float m_ChangeTimeScaleSecRevert;

		// Token: 0x040005EE RID: 1518
		private bool m_IsChangeTimeScale;
	}
}
