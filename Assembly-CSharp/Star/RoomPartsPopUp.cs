﻿using System;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000620 RID: 1568
	public class RoomPartsPopUp : IRoomPartsAction
	{
		// Token: 0x06001E93 RID: 7827 RVA: 0x000A5958 File Offset: 0x000A3D58
		public RoomPartsPopUp(RoomCharaHud popup, float ftime, int fvoicelink = -1)
		{
			this.m_PopUp = popup;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Active = true;
			this.m_VoiceLink = fvoicelink;
		}

		// Token: 0x06001E94 RID: 7828 RVA: 0x000A5988 File Offset: 0x000A3D88
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_VoiceLink != -1 && !SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_VoiceLink))
			{
				this.m_VoiceLink = -1;
				if (this.m_Time < this.m_MaxTime - 0.75f)
				{
					this.m_Time = this.m_MaxTime - 0.75f;
				}
			}
			if (this.m_Time >= this.m_MaxTime && this.m_VoiceLink == -1)
			{
				this.m_PopUp.GetCharaTweet().Close();
				this.m_PopUp = null;
				this.m_Active = false;
			}
			return this.m_Active;
		}

		// Token: 0x06001E95 RID: 7829 RVA: 0x000A5A40 File Offset: 0x000A3E40
		public override void PartsCancel()
		{
			if (this.m_PopUp != null)
			{
				this.m_PopUp.GetCharaTweet().Close();
				this.m_PopUp = null;
			}
			if (this.m_VoiceLink != -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceLink);
				this.m_VoiceLink = -1;
			}
		}

		// Token: 0x04002545 RID: 9541
		public RoomCharaHud m_PopUp;

		// Token: 0x04002546 RID: 9542
		public float m_Time;

		// Token: 0x04002547 RID: 9543
		public float m_MaxTime;

		// Token: 0x04002548 RID: 9544
		public int m_VoiceLink;
	}
}
