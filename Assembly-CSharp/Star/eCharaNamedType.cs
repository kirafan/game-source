﻿using System;

namespace Star
{
	// Token: 0x02000224 RID: 548
	public enum eCharaNamedType
	{
		// Token: 0x04000E7D RID: 3709
		None = -1,
		// Token: 0x04000E7E RID: 3710
		Named_0000,
		// Token: 0x04000E7F RID: 3711
		Named_0001,
		// Token: 0x04000E80 RID: 3712
		Named_0002,
		// Token: 0x04000E81 RID: 3713
		Named_0003,
		// Token: 0x04000E82 RID: 3714
		Named_0004,
		// Token: 0x04000E83 RID: 3715
		Named_0005,
		// Token: 0x04000E84 RID: 3716
		Named_0006,
		// Token: 0x04000E85 RID: 3717
		Named_0007,
		// Token: 0x04000E86 RID: 3718
		Named_0008,
		// Token: 0x04000E87 RID: 3719
		Named_0009,
		// Token: 0x04000E88 RID: 3720
		Named_0010,
		// Token: 0x04000E89 RID: 3721
		Named_0011,
		// Token: 0x04000E8A RID: 3722
		Named_0012,
		// Token: 0x04000E8B RID: 3723
		Named_0013,
		// Token: 0x04000E8C RID: 3724
		Named_0014,
		// Token: 0x04000E8D RID: 3725
		Named_0015,
		// Token: 0x04000E8E RID: 3726
		Named_0016,
		// Token: 0x04000E8F RID: 3727
		Named_0017,
		// Token: 0x04000E90 RID: 3728
		Named_0018,
		// Token: 0x04000E91 RID: 3729
		Named_0019,
		// Token: 0x04000E92 RID: 3730
		Named_0020,
		// Token: 0x04000E93 RID: 3731
		Named_0021,
		// Token: 0x04000E94 RID: 3732
		Named_0022,
		// Token: 0x04000E95 RID: 3733
		Named_0023,
		// Token: 0x04000E96 RID: 3734
		Named_0024,
		// Token: 0x04000E97 RID: 3735
		Named_0025,
		// Token: 0x04000E98 RID: 3736
		Named_0026,
		// Token: 0x04000E99 RID: 3737
		Named_0027,
		// Token: 0x04000E9A RID: 3738
		Named_0028,
		// Token: 0x04000E9B RID: 3739
		Named_0029,
		// Token: 0x04000E9C RID: 3740
		Named_0030,
		// Token: 0x04000E9D RID: 3741
		Named_0031,
		// Token: 0x04000E9E RID: 3742
		Named_0032,
		// Token: 0x04000E9F RID: 3743
		Named_0033,
		// Token: 0x04000EA0 RID: 3744
		Named_0034,
		// Token: 0x04000EA1 RID: 3745
		Named_0035,
		// Token: 0x04000EA2 RID: 3746
		Named_0036,
		// Token: 0x04000EA3 RID: 3747
		Named_0037,
		// Token: 0x04000EA4 RID: 3748
		Named_0038,
		// Token: 0x04000EA5 RID: 3749
		Named_0039,
		// Token: 0x04000EA6 RID: 3750
		Named_0040,
		// Token: 0x04000EA7 RID: 3751
		Named_0041,
		// Token: 0x04000EA8 RID: 3752
		Named_0042,
		// Token: 0x04000EA9 RID: 3753
		Named_0043,
		// Token: 0x04000EAA RID: 3754
		Named_0044,
		// Token: 0x04000EAB RID: 3755
		Named_0045,
		// Token: 0x04000EAC RID: 3756
		Named_0046,
		// Token: 0x04000EAD RID: 3757
		Named_0047,
		// Token: 0x04000EAE RID: 3758
		Named_0048,
		// Token: 0x04000EAF RID: 3759
		Named_0049,
		// Token: 0x04000EB0 RID: 3760
		Named_0050,
		// Token: 0x04000EB1 RID: 3761
		Named_0051,
		// Token: 0x04000EB2 RID: 3762
		Named_0052,
		// Token: 0x04000EB3 RID: 3763
		Named_0053,
		// Token: 0x04000EB4 RID: 3764
		Named_0054,
		// Token: 0x04000EB5 RID: 3765
		Named_0055,
		// Token: 0x04000EB6 RID: 3766
		Named_0056,
		// Token: 0x04000EB7 RID: 3767
		Named_0057,
		// Token: 0x04000EB8 RID: 3768
		Named_0058,
		// Token: 0x04000EB9 RID: 3769
		Named_0059,
		// Token: 0x04000EBA RID: 3770
		Named_0060,
		// Token: 0x04000EBB RID: 3771
		Named_0061,
		// Token: 0x04000EBC RID: 3772
		Named_0062,
		// Token: 0x04000EBD RID: 3773
		Named_0063,
		// Token: 0x04000EBE RID: 3774
		Named_0064,
		// Token: 0x04000EBF RID: 3775
		Named_0065,
		// Token: 0x04000EC0 RID: 3776
		Named_0066,
		// Token: 0x04000EC1 RID: 3777
		Named_0067,
		// Token: 0x04000EC2 RID: 3778
		Named_0068,
		// Token: 0x04000EC3 RID: 3779
		Named_0069,
		// Token: 0x04000EC4 RID: 3780
		Named_0070,
		// Token: 0x04000EC5 RID: 3781
		Named_0071,
		// Token: 0x04000EC6 RID: 3782
		Named_0072,
		// Token: 0x04000EC7 RID: 3783
		Named_0073,
		// Token: 0x04000EC8 RID: 3784
		Named_0074,
		// Token: 0x04000EC9 RID: 3785
		Named_0075,
		// Token: 0x04000ECA RID: 3786
		Named_0076,
		// Token: 0x04000ECB RID: 3787
		Named_0077,
		// Token: 0x04000ECC RID: 3788
		Named_0078,
		// Token: 0x04000ECD RID: 3789
		Named_0079,
		// Token: 0x04000ECE RID: 3790
		Named_0080,
		// Token: 0x04000ECF RID: 3791
		Named_0081,
		// Token: 0x04000ED0 RID: 3792
		Named_0082,
		// Token: 0x04000ED1 RID: 3793
		Named_0083,
		// Token: 0x04000ED2 RID: 3794
		Named_0084,
		// Token: 0x04000ED3 RID: 3795
		Named_0085,
		// Token: 0x04000ED4 RID: 3796
		Named_0086,
		// Token: 0x04000ED5 RID: 3797
		Named_0087,
		// Token: 0x04000ED6 RID: 3798
		Named_0088,
		// Token: 0x04000ED7 RID: 3799
		Named_0089,
		// Token: 0x04000ED8 RID: 3800
		Named_0090,
		// Token: 0x04000ED9 RID: 3801
		Named_0091,
		// Token: 0x04000EDA RID: 3802
		Named_0092,
		// Token: 0x04000EDB RID: 3803
		Named_0093,
		// Token: 0x04000EDC RID: 3804
		Named_0094,
		// Token: 0x04000EDD RID: 3805
		Named_0095,
		// Token: 0x04000EDE RID: 3806
		Named_0096,
		// Token: 0x04000EDF RID: 3807
		Named_0097,
		// Token: 0x04000EE0 RID: 3808
		Named_0098,
		// Token: 0x04000EE1 RID: 3809
		Named_0099,
		// Token: 0x04000EE2 RID: 3810
		Named_0100,
		// Token: 0x04000EE3 RID: 3811
		Named_0101,
		// Token: 0x04000EE4 RID: 3812
		Named_0102,
		// Token: 0x04000EE5 RID: 3813
		Named_0103,
		// Token: 0x04000EE6 RID: 3814
		Named_0104,
		// Token: 0x04000EE7 RID: 3815
		Named_0105,
		// Token: 0x04000EE8 RID: 3816
		Named_0106,
		// Token: 0x04000EE9 RID: 3817
		Named_0107,
		// Token: 0x04000EEA RID: 3818
		Named_0108,
		// Token: 0x04000EEB RID: 3819
		Named_0109
	}
}
