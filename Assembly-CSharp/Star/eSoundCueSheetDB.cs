﻿using System;

namespace Star
{
	// Token: 0x02000212 RID: 530
	public enum eSoundCueSheetDB
	{
		// Token: 0x04000D20 RID: 3360
		BGM_builtin,
		// Token: 0x04000D21 RID: 3361
		BGM,
		// Token: 0x04000D22 RID: 3362
		BGM_adv,
		// Token: 0x04000D23 RID: 3363
		BGM_uniqueSkill,
		// Token: 0x04000D24 RID: 3364
		System,
		// Token: 0x04000D25 RID: 3365
		Battle,
		// Token: 0x04000D26 RID: 3366
		Town,
		// Token: 0x04000D27 RID: 3367
		Room,
		// Token: 0x04000D28 RID: 3368
		ADV,
		// Token: 0x04000D29 RID: 3369
		Gacha,
		// Token: 0x04000D2A RID: 3370
		Mix,
		// Token: 0x04000D2B RID: 3371
		Voice_Original_000,
		// Token: 0x04000D2C RID: 3372
		Voice_Original_001,
		// Token: 0x04000D2D RID: 3373
		Voice_Original_002,
		// Token: 0x04000D2E RID: 3374
		Voice_Original_003,
		// Token: 0x04000D2F RID: 3375
		Voice_Original_004,
		// Token: 0x04000D30 RID: 3376
		Voice_Original_005,
		// Token: 0x04000D31 RID: 3377
		Voice_Original_006,
		// Token: 0x04000D32 RID: 3378
		Voice_Original_007,
		// Token: 0x04000D33 RID: 3379
		Voice_Original_008,
		// Token: 0x04000D34 RID: 3380
		Voice_Original_009,
		// Token: 0x04000D35 RID: 3381
		Voice_Original_010,
		// Token: 0x04000D36 RID: 3382
		Voice_Original_011,
		// Token: 0x04000D37 RID: 3383
		Max
	}
}
