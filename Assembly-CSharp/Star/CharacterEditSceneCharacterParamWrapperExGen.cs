﻿using System;

namespace Star
{
	// Token: 0x020002A6 RID: 678
	public class CharacterEditSceneCharacterParamWrapperExGen<SimulateParamType> : CharacterEditSceneCharacterParamWrapper where SimulateParamType : EditUtility.CharacterEditSimulateParam
	{
		// Token: 0x06000CCC RID: 3276 RVA: 0x00048E4E File Offset: 0x0004724E
		public CharacterEditSceneCharacterParamWrapperExGen(eCharacterDataType characterDataType, SimulateParamType param) : base(characterDataType, param)
		{
			this.m_Param = param;
		}

		// Token: 0x04001548 RID: 5448
		protected SimulateParamType m_Param;
	}
}
