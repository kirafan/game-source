﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000085 RID: 133
	public abstract class PlayInterfaceMonoBehaviour : MonoBehaviour
	{
		// Token: 0x06000416 RID: 1046
		public abstract void Play();

		// Token: 0x06000417 RID: 1047
		public abstract void Stop();
	}
}
