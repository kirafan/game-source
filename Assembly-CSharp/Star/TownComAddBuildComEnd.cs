﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004AC RID: 1196
	public class TownComAddBuildComEnd : IMainComCommand
	{
		// Token: 0x06001768 RID: 5992 RVA: 0x000794D0 File Offset: 0x000778D0
		public TownComAddBuildComEnd(List<FldComBuildScript.BuildUpParam> list)
		{
			this.m_BuildUpParamList = list;
		}

		// Token: 0x06001769 RID: 5993 RVA: 0x000794DF File Offset: 0x000778DF
		public override int GetHashCode()
		{
			return 7;
		}

		// Token: 0x04001E20 RID: 7712
		public List<FldComBuildScript.BuildUpParam> m_BuildUpParamList;
	}
}
