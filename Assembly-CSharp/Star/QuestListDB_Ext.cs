﻿using System;

namespace Star
{
	// Token: 0x0200019F RID: 415
	public static class QuestListDB_Ext
	{
		// Token: 0x06000B09 RID: 2825 RVA: 0x00041BD8 File Offset: 0x0003FFD8
		public static QuestListDB_Param GetParam(this QuestListDB self, int questID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == questID)
				{
					return self.m_Params[i];
				}
			}
			return default(QuestListDB_Param);
		}

		// Token: 0x06000B0A RID: 2826 RVA: 0x00041C30 File Offset: 0x00040030
		public static int GetWaveNum(ref QuestListDB_Param questParam)
		{
			int num = 0;
			for (int i = 0; i < questParam.m_WaveIDs.Length; i++)
			{
				if (questParam.m_WaveIDs[i] <= -1)
				{
					break;
				}
				num++;
			}
			return num;
		}
	}
}
