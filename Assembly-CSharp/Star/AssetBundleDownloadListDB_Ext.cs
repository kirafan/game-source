﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Star
{
	// Token: 0x02000188 RID: 392
	public static class AssetBundleDownloadListDB_Ext
	{
		// Token: 0x06000AD5 RID: 2773 RVA: 0x0004099C File Offset: 0x0003ED9C
		public static List<string> GetPaths(this AssetBundleDownloadListDB self)
		{
			StringBuilder stringBuilder = new StringBuilder();
			List<string> list = new List<string>();
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				for (int j = 0; j < self.m_Params[i].m_Paths.Length; j++)
				{
					stringBuilder.Length = 0;
					stringBuilder.Append(self.m_Params[i].m_Paths[j]);
					if (!self.m_Params[i].m_Paths[j].Contains(".muast"))
					{
						stringBuilder.Append(".muast");
					}
					list.Add(stringBuilder.ToString());
				}
			}
			return list;
		}
	}
}
