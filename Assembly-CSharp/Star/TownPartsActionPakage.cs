﻿using System;

namespace Star
{
	// Token: 0x020006B4 RID: 1716
	public class TownPartsActionPakage
	{
		// Token: 0x06002238 RID: 8760 RVA: 0x000B5B75 File Offset: 0x000B3F75
		public TownPartsActionPakage()
		{
			this.m_Table = new ITownPartsAction[8];
		}

		// Token: 0x06002239 RID: 8761 RVA: 0x000B5B89 File Offset: 0x000B3F89
		public bool IsEmpty()
		{
			return this.m_Num == 0;
		}

		// Token: 0x0600223A RID: 8762 RVA: 0x000B5B94 File Offset: 0x000B3F94
		public void EntryAction(ITownPartsAction paction, int fuid = 0)
		{
			Type type = paction.GetType();
			bool flag = false;
			if (fuid == 0)
			{
				for (int i = 0; i < this.m_Num; i++)
				{
					if (this.m_Table[i].GetType() == type)
					{
						this.m_Table[i] = null;
						this.m_Table[i] = paction;
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				if (this.m_Num < 8)
				{
					this.m_Table[this.m_Num] = paction;
					paction.m_UID = fuid;
					this.m_Num++;
				}
				else
				{
					paction = null;
				}
			}
		}

		// Token: 0x0600223B RID: 8763 RVA: 0x000B5C30 File Offset: 0x000B4030
		public ITownPartsAction GetAction(int fuid)
		{
			ITownPartsAction result = null;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].m_UID == fuid)
				{
					result = this.m_Table[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x0600223C RID: 8764 RVA: 0x000B5C78 File Offset: 0x000B4078
		public ITownPartsAction GetAction(Type ftype)
		{
			ITownPartsAction result = null;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].GetType() == ftype)
				{
					result = this.m_Table[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x0600223D RID: 8765 RVA: 0x000B5CC0 File Offset: 0x000B40C0
		public void PakageUpdate()
		{
			int num = 0;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (!this.m_Table[i].PartsUpdate())
				{
					this.m_Table[i] = null;
					num++;
				}
			}
			if (num != 0)
			{
				int i = 0;
				while (i < this.m_Num)
				{
					if (this.m_Table[i] == null)
					{
						for (int j = i + 1; j < this.m_Num; j++)
						{
							this.m_Table[j - 1] = this.m_Table[j];
						}
						this.m_Num--;
						this.m_Table[this.m_Num] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x0600223E RID: 8766 RVA: 0x000B5D7C File Offset: 0x000B417C
		public void Release()
		{
			for (int i = 0; i < 8; i++)
			{
				if (this.m_Table[i] != null)
				{
					this.m_Table[i].Destory();
				}
				this.m_Table[i] = null;
			}
			this.m_Table = null;
		}

		// Token: 0x040028B9 RID: 10425
		public int m_Num;

		// Token: 0x040028BA RID: 10426
		public ITownPartsAction[] m_Table;
	}
}
