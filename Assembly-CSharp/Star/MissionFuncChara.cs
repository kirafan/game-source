﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004FA RID: 1274
	public class MissionFuncChara : IMissionFunction
	{
		// Token: 0x06001923 RID: 6435 RVA: 0x000828A4 File Offset: 0x00080CA4
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			eXlsMissionCharaFuncType extensionScriptID = (eXlsMissionCharaFuncType)pparam.m_ExtensionScriptID;
			if (extensionScriptID == eXlsMissionCharaFuncType.FriendShip)
			{
				UserNamedData userNamedData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData((eCharaNamedType)pparam.m_SubCodeID);
				if (userNamedData != null)
				{
					pparam.m_Rate = userNamedData.FriendShip;
					result = (pparam.m_Rate >= pparam.m_RateBase);
				}
			}
			return result;
		}
	}
}
