﻿using System;

namespace Star
{
	// Token: 0x0200018E RID: 398
	public static class CharacterIllustOffsetDB_Ext
	{
		// Token: 0x06000AE1 RID: 2785 RVA: 0x00040F7C File Offset: 0x0003F37C
		public static CharacterIllustOffsetDB_Param GetParam(this CharacterIllustOffsetDB self, int charaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CharaID == charaID)
				{
					return self.m_Params[i];
				}
			}
			return default(CharacterIllustOffsetDB_Param);
		}
	}
}
