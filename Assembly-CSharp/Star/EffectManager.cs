﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020002F3 RID: 755
	public sealed class EffectManager : SingletonMonoBehaviour<EffectManager>
	{
		// Token: 0x06000EB0 RID: 3760 RVA: 0x0004EB4F File Offset: 0x0004CF4F
		protected override void Awake()
		{
			base.Awake();
			this.m_Loader = new ABResourceLoader(100);
		}

		// Token: 0x06000EB1 RID: 3761 RVA: 0x0004EB64 File Offset: 0x0004CF64
		private void Update()
		{
			this.UpdateLoadEffect();
			for (int i = this.m_ActiveEffectList.Count - 1; i >= 0; i--)
			{
				EffectHandler effectHandler = this.m_ActiveEffectList[i];
				if (!effectHandler.IsPlaying())
				{
					this.DestroyToCache(effectHandler);
					this.m_ActiveEffectList.RemoveAt(i);
				}
			}
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x0004EBC0 File Offset: 0x0004CFC0
		private void LateUpdate()
		{
			this.UpdateSfc();
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x0004EBC8 File Offset: 0x0004CFC8
		public bool IsAvailable()
		{
			return !(this.m_EffectListDB == null);
		}

		// Token: 0x06000EB4 RID: 3764 RVA: 0x0004EBE0 File Offset: 0x0004CFE0
		public void ForceResetOnReturnTitle()
		{
			foreach (EffectHandler effectHandler in this.m_SfcDict.Keys)
			{
				if (effectHandler != null)
				{
					this.m_SfcDict[effectHandler].Destroy(true, true);
				}
			}
			this.m_SfcDict.Clear();
			this.UnloadEffectAll(null);
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x0004EC6C File Offset: 0x0004D06C
		public void SetupFromDB(EffectListDB effectListDB)
		{
			this.m_EffectListDB = effectListDB;
			this.m_PackDatas.Clear();
			this.m_DBIndices.Clear();
			int num = this.m_EffectListDB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				string packName = this.m_EffectListDB.m_Params[i].m_PackName;
				if (!string.IsNullOrEmpty(packName))
				{
					if (!this.m_PackDatas.ContainsKey(packName))
					{
						this.m_PackDatas.Add(packName, new EffectManager.PackEffectData());
					}
					this.m_PackDatas[packName].m_EffectIDs.Add(this.m_EffectListDB.m_Params[i].m_EffectID);
				}
				this.m_DBIndices.Add(this.m_EffectListDB.m_Params[i].m_EffectID, i);
			}
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x0004ED48 File Offset: 0x0004D148
		public bool GetEffectListDBParam(string effectID, out EffectListDB_Param out_param)
		{
			int num = 0;
			if (this.m_DBIndices.TryGetValue(effectID, out num))
			{
				out_param = this.m_EffectListDB.m_Params[num];
				return true;
			}
			out_param = default(EffectListDB_Param);
			return false;
		}

		// Token: 0x06000EB7 RID: 3767 RVA: 0x0004ED90 File Offset: 0x0004D190
		public string CheckPackEffect(string effectID)
		{
			int num = 0;
			if (this.m_DBIndices.TryGetValue(effectID, out num) && !string.IsNullOrEmpty(this.m_EffectListDB.m_Params[num].m_PackName))
			{
				return this.m_EffectListDB.m_Params[num].m_PackName;
			}
			return null;
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0004EDEA File Offset: 0x0004D1EA
		public void SetupSound(SoundManager soundMng)
		{
			this.m_SoundManager = soundMng;
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x0004EDF4 File Offset: 0x0004D1F4
		private void PlaySfc(EffectHandler effect)
		{
			int index = this.m_DBIndices[effect.EffectID];
			if (this.m_EffectListDB.IsExistSoundSetting(index) && this.m_SoundManager != null)
			{
				SoundFrameController soundFrameController = new SoundFrameController(this.m_SoundManager, null, null, null);
				this.m_EffectListDB.SetupSoundFrameController(index, soundFrameController);
				soundFrameController.PrepareQuick(15);
				soundFrameController.Play();
				this.m_SfcDict.AddOrReplace(effect, soundFrameController);
			}
		}

		// Token: 0x06000EBA RID: 3770 RVA: 0x0004EE68 File Offset: 0x0004D268
		private void UpdateSfc()
		{
			this.m_RemoveSfcKeys.Clear();
			foreach (EffectHandler effectHandler in this.m_SfcDict.Keys)
			{
				if (effectHandler != null)
				{
					if (effectHandler.IsPlaying())
					{
						int currentFrame = (int)(effectHandler.GetPlayingSec() / 0.033333335f);
						this.m_SfcDict[effectHandler].Update(currentFrame);
					}
					else
					{
						this.m_SfcDict[effectHandler].Destroy(true, true);
						this.m_RemoveSfcKeys.Add(effectHandler);
					}
				}
			}
			for (int i = 0; i < this.m_RemoveSfcKeys.Count; i++)
			{
				this.m_SfcDict.Remove(this.m_RemoveSfcKeys[i]);
			}
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x0004EF5C File Offset: 0x0004D35C
		private void UpdateLoadEffect()
		{
			this.m_Loader.Update();
			foreach (string text in this.m_LoadingHndls_Single.Keys)
			{
				ABResourceObjectHandler abresourceObjectHandler = this.m_LoadingHndls_Single[text];
				if (abresourceObjectHandler != null)
				{
					if (abresourceObjectHandler.IsDone())
					{
						if (abresourceObjectHandler.IsError())
						{
							UnityEngine.Debug.LogFormat("abHndl is error :{0}", new object[]
							{
								text
							});
						}
						else
						{
							this.CreateCacheFromAB(abresourceObjectHandler, text);
						}
						this.m_Loader.Unload(abresourceObjectHandler);
						this.m_RemoveKeys.Add(text);
					}
				}
				else
				{
					this.m_RemoveKeys.Add(text);
				}
			}
			if (this.m_RemoveKeys.Count > 0)
			{
				for (int i = 0; i < this.m_RemoveKeys.Count; i++)
				{
					this.m_LoadingHndls_Single.Remove(this.m_RemoveKeys[i]);
				}
				this.m_RemoveKeys.Clear();
			}
			this.m_RemoveKeys.Clear();
			foreach (string text2 in this.m_LoadingHndls_Pack.Keys)
			{
				ABResourceObjectHandler abresourceObjectHandler2 = this.m_LoadingHndls_Pack[text2];
				if (abresourceObjectHandler2 != null)
				{
					if (abresourceObjectHandler2.IsDone())
					{
						if (abresourceObjectHandler2.IsError())
						{
							UnityEngine.Debug.LogFormat("abHndl is error :{0}", new object[]
							{
								text2
							});
						}
						else
						{
							EffectManager.PackEffectData packEffectData = this.m_PackDatas[text2];
							for (int j = 0; j < packEffectData.m_EffectIDs.Count; j++)
							{
								this.CreateCacheFromAB(abresourceObjectHandler2, packEffectData.m_EffectIDs[j]);
							}
							this.m_LoadedPacks.Add(text2, true);
						}
						this.m_Loader.Unload(abresourceObjectHandler2);
						this.m_RemoveKeys.Add(text2);
					}
				}
				else
				{
					this.m_RemoveKeys.Add(text2);
				}
			}
			if (this.m_RemoveKeys.Count > 0)
			{
				for (int k = 0; k < this.m_RemoveKeys.Count; k++)
				{
					this.m_LoadingHndls_Pack.Remove(this.m_RemoveKeys[k]);
				}
				this.m_RemoveKeys.Clear();
			}
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x0004F1F8 File Offset: 0x0004D5F8
		private void CreateCacheFromAB(ABResourceObjectHandler hndl, string key)
		{
			GameObject gameObject = hndl.FindObj(key, true) as GameObject;
			if (!(gameObject != null) || !this.CreateCache(key, gameObject))
			{
			}
		}

		// Token: 0x06000EBD RID: 3773 RVA: 0x0004F22C File Offset: 0x0004D62C
		private string GetSingleEffectResourcePath(string effectID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("effect/");
			this.m_sb.Append(effectID);
			this.m_sb.Append(".muast");
			return this.m_sb.ToString();
		}

		// Token: 0x06000EBE RID: 3774 RVA: 0x0004F280 File Offset: 0x0004D680
		private string GetPackEffectResourcePath(string packName)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("effect/");
			this.m_sb.Append(packName);
			this.m_sb.Append(".muast");
			return this.m_sb.ToString();
		}

		// Token: 0x06000EBF RID: 3775 RVA: 0x0004F2D3 File Offset: 0x0004D6D3
		public bool IsLoadingAny()
		{
			return this.m_LoadingHndls_Single.Count > 0 || this.m_LoadingHndls_Pack.Count > 0;
		}

		// Token: 0x06000EC0 RID: 3776 RVA: 0x0004F2FC File Offset: 0x0004D6FC
		public bool LoadPack(string packName)
		{
			if (this.m_LoadingHndls_Pack.ContainsKey(packName))
			{
				return false;
			}
			if (this.m_LoadedPacks.ContainsKey(packName))
			{
				return false;
			}
			if (!this.m_PackDatas.ContainsKey(packName))
			{
				return false;
			}
			ABResourceObjectHandler abresourceObjectHandler = this.m_Loader.Load(this.GetPackEffectResourcePath(packName), new MeigeResource.Option[0]);
			if (abresourceObjectHandler != null)
			{
				this.m_LoadingHndls_Pack.Add(packName, abresourceObjectHandler);
				return true;
			}
			return false;
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x0004F378 File Offset: 0x0004D778
		public bool UnloadPack(string packName)
		{
			if (!this.m_PackDatas.ContainsKey(packName))
			{
				return false;
			}
			EffectManager.PackEffectData packEffectData = this.m_PackDatas[packName];
			for (int i = 0; i < packEffectData.m_EffectIDs.Count; i++)
			{
				string text = packEffectData.m_EffectIDs[i];
				this.ActiveToCache(text);
				if (this.m_Caches.ContainsKey(text))
				{
					this.m_Caches[text].Destroy();
					this.m_Caches.Remove(text);
				}
				this.m_Loader.Unload(this.GetPackEffectResourcePath(packName));
			}
			this.m_LoadedPacks.Remove(packName);
			return true;
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x0004F424 File Offset: 0x0004D824
		public bool LoadSingleEffect(string effectID, bool isPackLoadIfBelongingToPack = false)
		{
			if (isPackLoadIfBelongingToPack)
			{
				string text = this.CheckPackEffect(effectID);
				if (!string.IsNullOrEmpty(text))
				{
					return this.LoadPack(text);
				}
			}
			if (this.m_LoadingHndls_Single.ContainsKey(effectID))
			{
				return false;
			}
			if (this.m_Caches.ContainsKey(effectID))
			{
				return false;
			}
			if (!this.m_DBIndices.ContainsKey(effectID))
			{
				return false;
			}
			ABResourceObjectHandler abresourceObjectHandler = this.m_Loader.Load(this.GetSingleEffectResourcePath(effectID), new MeigeResource.Option[0]);
			if (abresourceObjectHandler != null)
			{
				this.m_LoadingHndls_Single.Add(effectID, abresourceObjectHandler);
				return true;
			}
			return false;
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x0004F4C0 File Offset: 0x0004D8C0
		public void UnloadSingleEffect(string effectID)
		{
			string value = this.CheckPackEffect(effectID);
			if (!string.IsNullOrEmpty(value))
			{
				return;
			}
			this.ActiveToCache(effectID);
			if (this.m_Caches.ContainsKey(effectID))
			{
				this.m_Caches[effectID].Destroy();
				this.m_Caches.Remove(effectID);
				this.m_Loader.Unload(this.GetSingleEffectResourcePath(effectID));
			}
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x0004F52C File Offset: 0x0004D92C
		private void ActiveToCache(string effectID)
		{
			for (int i = this.m_ActiveEffectList.Count - 1; i >= 0; i--)
			{
				EffectHandler effectHandler = this.m_ActiveEffectList[i];
				if (!(effectHandler.EffectID != effectID))
				{
					this.DestroyToCache(effectHandler);
					this.m_ActiveEffectList.RemoveAt(i);
				}
			}
		}

		// Token: 0x06000EC5 RID: 3781 RVA: 0x0004F590 File Offset: 0x0004D990
		public void UnloadEffectAll(string[] ignorePackNames)
		{
			List<string> list = new List<string>();
			foreach (string text in this.m_LoadedPacks.Keys)
			{
				bool flag = true;
				if (ignorePackNames != null)
				{
					for (int i = 0; i < ignorePackNames.Length; i++)
					{
						if (text == ignorePackNames[i])
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					list.Add(text);
				}
			}
			for (int j = 0; j < list.Count; j++)
			{
				this.UnloadPack(list[j]);
			}
			List<string> list2 = new List<string>();
			foreach (string text2 in this.m_Caches.Keys)
			{
				bool flag2 = true;
				if (ignorePackNames != null)
				{
					string text3 = this.CheckPackEffect(text2);
					if (!string.IsNullOrEmpty(text3))
					{
						for (int k = 0; k < ignorePackNames.Length; k++)
						{
							if (text3 == ignorePackNames[k])
							{
								flag2 = false;
								break;
							}
						}
					}
				}
				if (flag2)
				{
					list2.Add(text2);
				}
			}
			for (int l = 0; l < list2.Count; l++)
			{
				this.UnloadSingleEffect(list2[l]);
			}
		}

		// Token: 0x06000EC6 RID: 3782 RVA: 0x0004F73C File Offset: 0x0004DB3C
		private bool CreateCache(string effectID, GameObject modelPrefab)
		{
			if (modelPrefab == null)
			{
				return false;
			}
			if (this.m_Caches.ContainsKey(effectID))
			{
				return false;
			}
			int num = this.m_DBIndices[effectID];
			int instanceMax = this.m_EffectListDB.m_Params[num].m_InstanceMax;
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(modelPrefab);
			CacheObjectData cacheObjectData = new CacheObjectData();
			cacheObjectData.OnInstantiateObject += this.OnInitializeObj;
			cacheObjectData.Initialize(gameObject, instanceMax, this.m_CacheRoot);
			cacheObjectData.OnInstantiateObject -= this.OnInitializeObj;
			this.m_Caches.Add(effectID, cacheObjectData);
			for (int i = 0; i < cacheObjectData.CacheObjects.Length; i++)
			{
				this.m_ActiveCachedInstances.Add(cacheObjectData.CacheObjects[i], true);
			}
			UnityEngine.Object.Destroy(gameObject);
			return true;
		}

		// Token: 0x06000EC7 RID: 3783 RVA: 0x0004F814 File Offset: 0x0004DC14
		private void OnInitializeObj(GameObject obj)
		{
			EffectHandler component = obj.GetComponent<EffectHandler>();
			if (component != null)
			{
				component.Setup();
			}
			else
			{
				TrailHandler component2 = obj.GetComponent<TrailHandler>();
				if (component2 != null)
				{
				}
			}
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x0004F858 File Offset: 0x0004DC58
		public void DestroyToCache(EffectHandlerBase objToDestroy)
		{
			if (objToDestroy == null)
			{
				return;
			}
			objToDestroy.OnDestroyToCache();
			GameObject gameObject = objToDestroy.gameObject;
			if (this.m_ActiveCachedInstances.ContainsKey(gameObject))
			{
				gameObject.SetActive(false);
				gameObject.transform.SetParent(this.m_CacheRoot, false);
				this.m_ActiveCachedInstances[gameObject] = false;
			}
			else
			{
				UnityEngine.Object.Destroy(gameObject);
			}
		}

		// Token: 0x06000EC9 RID: 3785 RVA: 0x0004F8C4 File Offset: 0x0004DCC4
		public void Play(string effectID, Vector3 position, Quaternion rotation, Vector3 localScale, Transform parent, bool isLocalPosition, int sortingOrder = 0, float animationPlaySpeed = 1f, bool isSoundPlay = false)
		{
			if (!this.m_Caches.ContainsKey(effectID))
			{
				return;
			}
			CacheObjectData cacheObjectData = this.m_Caches[effectID];
			GameObject nextObjectInCache = cacheObjectData.GetNextObjectInCache();
			if (nextObjectInCache == null)
			{
				return;
			}
			nextObjectInCache.SetActive(true);
			this.m_ActiveCachedInstances[nextObjectInCache] = true;
			EffectHandler component = nextObjectInCache.GetComponent<EffectHandler>();
			if (component != null)
			{
				component.Play(position, rotation, localScale, parent, isLocalPosition, WrapMode.ClampForever, animationPlaySpeed);
				component.SetSortingOrder(sortingOrder);
				this.m_ActiveEffectList.Add(component);
				if (isSoundPlay)
				{
					this.PlaySfc(component);
				}
			}
		}

		// Token: 0x06000ECA RID: 3786 RVA: 0x0004F960 File Offset: 0x0004DD60
		public EffectHandler PlayUnManaged(string effectID, Vector3 position, Quaternion rotation, Transform parent, bool isLocalPosition, WrapMode wrapMode = WrapMode.ClampForever, int sortingOrder = 0, float animationPlaySpeed = 1f, bool isSoundPlay = false)
		{
			if (!this.m_Caches.ContainsKey(effectID))
			{
				return null;
			}
			CacheObjectData cacheObjectData = this.m_Caches[effectID];
			GameObject nextObjectInCache = cacheObjectData.GetNextObjectInCache();
			if (nextObjectInCache == null)
			{
				return null;
			}
			nextObjectInCache.SetActive(true);
			this.m_ActiveCachedInstances[nextObjectInCache] = true;
			EffectHandler component = nextObjectInCache.GetComponent<EffectHandler>();
			if (component != null)
			{
				component.Play(position, rotation, Vector3.one, parent, isLocalPosition, wrapMode, animationPlaySpeed);
				component.SetSortingOrder(sortingOrder);
				if (isSoundPlay)
				{
					this.PlaySfc(component);
				}
			}
			return component;
		}

		// Token: 0x06000ECB RID: 3787 RVA: 0x0004F9F8 File Offset: 0x0004DDF8
		public TrailHandler BorrowTrail(string effectID, Vector3 position, Quaternion rotation, Transform parent, bool isLocalPosition, int sortingOrder = 0)
		{
			if (!this.m_Caches.ContainsKey(effectID))
			{
				return null;
			}
			CacheObjectData cacheObjectData = this.m_Caches[effectID];
			if (cacheObjectData == null)
			{
				return null;
			}
			GameObject nextObjectInCache = cacheObjectData.GetNextObjectInCache();
			if (nextObjectInCache == null)
			{
				return null;
			}
			nextObjectInCache.SetActive(true);
			this.m_ActiveCachedInstances[nextObjectInCache] = true;
			TrailHandler component = nextObjectInCache.GetComponent<TrailHandler>();
			if (component != null)
			{
				if (parent != null)
				{
					component.CacheTransform.SetParent(parent, false);
				}
				if (isLocalPosition)
				{
					component.CacheTransform.localPosition = position;
				}
				else
				{
					component.CacheTransform.position = position;
				}
				component.CacheTransform.localRotation = rotation;
				component.SetSortingOrder(sortingOrder);
			}
			return component;
		}

		// Token: 0x06000ECC RID: 3788 RVA: 0x0004FABD File Offset: 0x0004DEBD
		[Conditional("EFFECT_MNG_DEBUG")]
		private void StartStopwatchForDebugPrepare(string key, bool isPack)
		{
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x0004FABF File Offset: 0x0004DEBF
		[Conditional("EFFECT_MNG_DEBUG")]
		private void StopStopwatchForDebugPrepare(string key, string header_msg, bool isPack)
		{
		}

		// Token: 0x040015C3 RID: 5571
		[SerializeField]
		private Transform m_CacheRoot;

		// Token: 0x040015C4 RID: 5572
		private Dictionary<string, bool> m_LoadedPacks = new Dictionary<string, bool>();

		// Token: 0x040015C5 RID: 5573
		private Dictionary<string, CacheObjectData> m_Caches = new Dictionary<string, CacheObjectData>();

		// Token: 0x040015C6 RID: 5574
		private Dictionary<GameObject, bool> m_ActiveCachedInstances = new Dictionary<GameObject, bool>();

		// Token: 0x040015C7 RID: 5575
		private List<EffectHandler> m_ActiveEffectList = new List<EffectHandler>();

		// Token: 0x040015C8 RID: 5576
		private EffectListDB m_EffectListDB;

		// Token: 0x040015C9 RID: 5577
		private Dictionary<string, EffectManager.PackEffectData> m_PackDatas = new Dictionary<string, EffectManager.PackEffectData>();

		// Token: 0x040015CA RID: 5578
		private Dictionary<string, int> m_DBIndices = new Dictionary<string, int>();

		// Token: 0x040015CB RID: 5579
		private SoundManager m_SoundManager;

		// Token: 0x040015CC RID: 5580
		private Dictionary<EffectHandler, SoundFrameController> m_SfcDict = new Dictionary<EffectHandler, SoundFrameController>();

		// Token: 0x040015CD RID: 5581
		private List<EffectHandler> m_RemoveSfcKeys = new List<EffectHandler>();

		// Token: 0x040015CE RID: 5582
		private const int SIMULTANEOUSLY_LOAD_NUM = 100;

		// Token: 0x040015CF RID: 5583
		private ABResourceLoader m_Loader;

		// Token: 0x040015D0 RID: 5584
		private Dictionary<string, ABResourceObjectHandler> m_LoadingHndls_Single = new Dictionary<string, ABResourceObjectHandler>();

		// Token: 0x040015D1 RID: 5585
		private Dictionary<string, ABResourceObjectHandler> m_LoadingHndls_Pack = new Dictionary<string, ABResourceObjectHandler>();

		// Token: 0x040015D2 RID: 5586
		private List<string> m_RemoveKeys = new List<string>();

		// Token: 0x040015D3 RID: 5587
		private StringBuilder m_sb = new StringBuilder();

		// Token: 0x020002F4 RID: 756
		public class PackEffectData
		{
			// Token: 0x040015D4 RID: 5588
			public List<string> m_EffectIDs = new List<string>();
		}
	}
}
