﻿using System;

namespace Star
{
	// Token: 0x0200055D RID: 1373
	public class RoomActionScriptDB
	{
		// Token: 0x040021C6 RID: 8646
		public string m_Name;

		// Token: 0x040021C7 RID: 8647
		public int m_AccessKey;

		// Token: 0x040021C8 RID: 8648
		public int m_LinkDir;

		// Token: 0x040021C9 RID: 8649
		public IRoomScriptData[] m_Table;
	}
}
