﻿using System;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200026B RID: 619
	public class DebugADVMain : MonoBehaviour
	{
		// Token: 0x06000B83 RID: 2947 RVA: 0x000433EF File Offset: 0x000417EF
		private void Start()
		{
		}

		// Token: 0x06000B84 RID: 2948 RVA: 0x000433F4 File Offset: 0x000417F4
		private void Update()
		{
			switch (this.m_State)
			{
			case DebugADVMain.eState.PrepareDB:
				if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_AdvPlayer.PrepareDB();
					this.m_State = DebugADVMain.eState.WaitPrepareDB;
				}
				break;
			case DebugADVMain.eState.WaitPrepareDB:
				this.m_AdvPlayer.UpdatePrepareDB();
				if (this.m_AdvPlayer.IsDonePrepareDB())
				{
					this.m_State = DebugADVMain.eState.Prepare;
				}
				break;
			case DebugADVMain.eState.Prepare:
				if (this.m_TestCategory)
				{
					this.m_AdvPlayer.Prepare(eADVCategory.Test, this.m_ADVID.ToString(), this.m_ADVID.ToString());
				}
				else
				{
					this.m_AdvPlayer.Prepare(this.m_ADVID);
				}
				this.m_State = DebugADVMain.eState.WaitPrepare;
				break;
			case DebugADVMain.eState.WaitPrepare:
				this.m_AdvPlayer.UpdatePrepare();
				if (this.m_AdvPlayer.IsDonePrepare)
				{
					this.m_AdvPlayer.Play();
					this.m_State = DebugADVMain.eState.Update;
				}
				break;
			case DebugADVMain.eState.Update:
				if (this.m_AdvPlayer.IsEnd)
				{
					this.m_State = DebugADVMain.eState.End;
				}
				break;
			case DebugADVMain.eState.End:
				this.m_AdvPlayer.Destroy();
				this.m_State = DebugADVMain.eState.WaitEnd;
				break;
			case DebugADVMain.eState.WaitEnd:
				if (this.m_AdvPlayer.UpdateDestroy())
				{
					Resources.UnloadUnusedAssets();
					this.m_State = DebugADVMain.eState.None;
				}
				break;
			}
		}

		// Token: 0x040013E6 RID: 5094
		private DebugADVMain.eState m_State = DebugADVMain.eState.PrepareDB;

		// Token: 0x040013E7 RID: 5095
		[SerializeField]
		private bool m_TestCategory;

		// Token: 0x040013E8 RID: 5096
		[SerializeField]
		private int m_ADVID;

		// Token: 0x040013E9 RID: 5097
		[SerializeField]
		private ADVPlayer m_AdvPlayer;

		// Token: 0x0200026C RID: 620
		private enum eState
		{
			// Token: 0x040013EB RID: 5099
			None,
			// Token: 0x040013EC RID: 5100
			PrepareDB,
			// Token: 0x040013ED RID: 5101
			WaitPrepareDB,
			// Token: 0x040013EE RID: 5102
			Prepare,
			// Token: 0x040013EF RID: 5103
			WaitPrepare,
			// Token: 0x040013F0 RID: 5104
			Update,
			// Token: 0x040013F1 RID: 5105
			End,
			// Token: 0x040013F2 RID: 5106
			WaitEnd
		}
	}
}
