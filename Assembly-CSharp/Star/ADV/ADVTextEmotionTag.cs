﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000055 RID: 85
	public class ADVTextEmotionTag : ADVTextEmotionTagBase
	{
		// Token: 0x060002BD RID: 701 RVA: 0x0000FB87 File Offset: 0x0000DF87
		public ADVTextEmotionTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0000FB94 File Offset: 0x0000DF94
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int num2 = tag.IndexOf(",") - num;
				string charaID = tag.Substring(num, num2);
				tag.Remove(num, num2);
				num = tag.IndexOf(",") + 1;
				num2 = tag.IndexOf(">") - num;
				string emotionID = tag.Substring(num, num2);
				tag.Remove(num, num2);
				this.m_Player.PlayEmotion(charaID, emotionID, eADVEmotionPosition.CharaDefault);
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
