﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000052 RID: 82
	public class ADVTextWaitMotionTag : ADVTextWaitMotionTagBase
	{
		// Token: 0x060002B8 RID: 696 RVA: 0x0000FB14 File Offset: 0x0000DF14
		public ADVTextWaitMotionTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000FB20 File Offset: 0x0000DF20
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				this.m_Player.WaitMotion();
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
