﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000059 RID: 89
	public class ADVTextPoseTagEnd : ADVTextPoseTagBase
	{
		// Token: 0x060002C4 RID: 708 RVA: 0x0000FD59 File Offset: 0x0000E159
		public ADVTextPoseTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x0000FD64 File Offset: 0x0000E164
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
