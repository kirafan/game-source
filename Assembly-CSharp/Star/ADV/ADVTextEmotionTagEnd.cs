﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000056 RID: 86
	public class ADVTextEmotionTagEnd : ADVTextEmotionTagBase
	{
		// Token: 0x060002BF RID: 703 RVA: 0x0000FC3B File Offset: 0x0000E03B
		public ADVTextEmotionTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0000FC46 File Offset: 0x0000E046
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
