﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000061 RID: 97
	public class ADVTextPageTag : ADVTextWaitTagBase
	{
		// Token: 0x060002D1 RID: 721 RVA: 0x0000FF8D File Offset: 0x0000E38D
		public ADVTextPageTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x0000FF98 File Offset: 0x0000E398
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			base.DeleteTag(ref text, ref idx, tag.Length);
			if (execTag && !this.m_IsEndTag)
			{
				text = text.Substring(idx);
				if (text[0] == '\n')
				{
					text = text.Substring(1);
				}
				idx = 0;
				result = true;
			}
			return result;
		}
	}
}
