﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Meige;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x02000074 RID: 116
	public class ScriptPlayer
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600036B RID: 875 RVA: 0x000101C4 File Offset: 0x0000E5C4
		public bool IsDonePrepare
		{
			get
			{
				return this.m_IsDonePrepare;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600036C RID: 876 RVA: 0x000101CC File Offset: 0x0000E5CC
		public bool IsEnd
		{
			get
			{
				return this.m_IsEnd;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600036D RID: 877 RVA: 0x000101D4 File Offset: 0x0000E5D4
		public ADVTextDB TextDB
		{
			get
			{
				return this.m_TextDB;
			}
		}

		// Token: 0x1700002D RID: 45
		// (set) Token: 0x0600036E RID: 878 RVA: 0x000101DC File Offset: 0x0000E5DC
		public ADVPlayer Player
		{
			set
			{
				this.m_Player = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600036F RID: 879 RVA: 0x000101E5 File Offset: 0x0000E5E5
		public int NowCommandIdx
		{
			get
			{
				return this.m_NowCommandIdx;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000370 RID: 880 RVA: 0x000101ED File Offset: 0x0000E5ED
		public List<ScriptPlayer.ADVCommand> CommandList
		{
			get
			{
				return this.m_CommandList;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000371 RID: 881 RVA: 0x000101F5 File Offset: 0x0000E5F5
		public bool IsError
		{
			get
			{
				return this.m_IsError;
			}
		}

		// Token: 0x06000372 RID: 882 RVA: 0x00010200 File Offset: 0x0000E600
		public void Prepare(eADVCategory category, string scriptName, string textDBName)
		{
			StringBuilder stringBuilder = new StringBuilder();
			string value = string.Empty;
			switch (category)
			{
			case eADVCategory.Story:
				value = "adv/script/story/";
				textDBName = "story_" + textDBName;
				scriptName = "story_" + scriptName;
				break;
			case eADVCategory.Event:
				value = "adv/script/event/";
				textDBName = "event_" + textDBName;
				scriptName = "event_" + scriptName;
				break;
			case eADVCategory.Chara:
				value = "adv/script/chara/";
				textDBName = "charaevent_" + textDBName;
				scriptName = "charaevent_" + scriptName;
				break;
			case eADVCategory.Cross:
				value = "adv/script/chara/";
				textDBName = "crossevent_" + textDBName;
				scriptName = "crossevent_" + scriptName;
				break;
			}
			switch (category)
			{
			case eADVCategory.Story:
			case eADVCategory.Event:
			case eADVCategory.Chara:
			case eADVCategory.Cross:
				stringBuilder.Length = 0;
				stringBuilder.Append(value);
				stringBuilder.Append("advscript_");
				stringBuilder.Append(scriptName);
				stringBuilder.Append(".muast");
				this.m_ScriptPath = stringBuilder.ToString();
				stringBuilder.Length = 0;
				stringBuilder.Append(value);
				stringBuilder.Append("advscripttext_");
				stringBuilder.Append(textDBName);
				stringBuilder.Append(".muast");
				this.m_TextDBPath = stringBuilder.ToString();
				break;
			}
			this.PrepareWithPath(category, this.m_ScriptPath, this.m_TextDBPath);
		}

		// Token: 0x06000373 RID: 883 RVA: 0x00010378 File Offset: 0x0000E778
		private void PrepareWithPath(eADVCategory category, string scriptPath, string textDBPath)
		{
			this.m_ScriptPath = scriptPath;
			this.m_TextDBPath = textDBPath;
			this.m_ADVCommandInherited = new ADVScriptCommandInherited();
			this.m_ADVCommandInherited.Setup(this.m_Player);
			this.m_IsDonePrepare = false;
			switch (category)
			{
			case eADVCategory.Story:
			case eADVCategory.Event:
			case eADVCategory.Chara:
			case eADVCategory.Cross:
				this.m_Loader = new ABResourceLoader(-1);
				this.m_HndlScript = this.m_Loader.Load(scriptPath, new MeigeResource.Option[0]);
				this.m_HndlTextDB = this.m_Loader.Load(textDBPath, new MeigeResource.Option[0]);
				this.m_TextDB = null;
				this.m_ScriptDB = null;
				break;
			}
		}

		// Token: 0x06000374 RID: 884 RVA: 0x00010424 File Offset: 0x0000E824
		public bool UpdatePrepare()
		{
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			if (this.m_TextDB == null && this.m_HndlTextDB != null)
			{
				if (!this.m_HndlTextDB.IsDone())
				{
					return false;
				}
				if (this.m_HndlTextDB.IsError())
				{
					APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
					this.m_HndlTextDB = null;
					this.m_IsError = true;
					return false;
				}
				this.m_TextDB = (ADVTextDB)this.m_HndlTextDB.Objs[0];
				if (this.m_TextDB == null)
				{
					Debug.LogError(" ADVTextDB not found : path=" + this.m_TextDBPath);
				}
			}
			if (this.m_ScriptDB == null && this.m_HndlTextDB != null)
			{
				if (!this.m_HndlScript.IsDone())
				{
					return false;
				}
				if (this.m_HndlScript.IsError())
				{
					APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
					this.m_HndlScript = null;
					this.m_IsError = true;
					return false;
				}
				this.m_ScriptDB = (ScriptData)this.m_HndlScript.Objs[0];
				if (this.m_ScriptDB == null)
				{
					Debug.LogError(" ScriptDB not found : path=" + this.m_ScriptPath);
				}
				this.ParseScript(-1);
			}
			this.m_IsDonePrepare = true;
			return true;
		}

		// Token: 0x06000375 RID: 885 RVA: 0x00010598 File Offset: 0x0000E998
		public void ParseScript(int scriptId = -1)
		{
			int num = this.m_ScriptDB.m_Params.Length;
			int num2 = -1;
			if (scriptId == -1)
			{
				num2 = 0;
			}
			else
			{
				for (int i = 0; i < num; i++)
				{
					if (this.m_ScriptDB.m_Params[i].ScriptID == scriptId)
					{
						num2 = i;
						break;
					}
				}
			}
			if (num2 < 0)
			{
				Debug.LogWarning("scriptID:" + scriptId + " not found ");
			}
			else
			{
				this.SetCommandList(this.m_ScriptDB.m_Params[num2]);
			}
		}

		// Token: 0x06000376 RID: 886 RVA: 0x0001063C File Offset: 0x0000EA3C
		private void SetCommandList(ScriptData_Param script)
		{
			this.m_CommandList = new List<ScriptPlayer.ADVCommand>();
			this.m_Player.ClearNeedStandPic();
			this.m_Player.ClearNeedSprite();
			this.m_Player.ClearNeedEffect();
			int num = script.FuncParam.Length;
			for (int i = 0; i < num; i++)
			{
				ScriptPlayer.ADVCommand advcommand = new ScriptPlayer.ADVCommand();
				ScriptData_FuncParam scriptData_FuncParam = script.FuncParam[i];
				advcommand.param.FuncName = scriptData_FuncParam.funcName;
				advcommand.param.ArgNum = scriptData_FuncParam.argNum;
				advcommand.param.argParam = new ScriptPlayer.ADVCommand.ArgParam[6];
				advcommand.param.argParam[0].ArgType = scriptData_FuncParam.argType1;
				advcommand.param.argParam[0].value = scriptData_FuncParam.m_value1;
				advcommand.param.argParam[1].ArgType = scriptData_FuncParam.argType2;
				advcommand.param.argParam[1].value = scriptData_FuncParam.m_value2;
				advcommand.param.argParam[2].ArgType = scriptData_FuncParam.argType3;
				advcommand.param.argParam[2].value = scriptData_FuncParam.m_value3;
				advcommand.param.argParam[3].ArgType = scriptData_FuncParam.argType4;
				advcommand.param.argParam[3].value = scriptData_FuncParam.m_value4;
				advcommand.param.argParam[4].ArgType = scriptData_FuncParam.argType5;
				advcommand.param.argParam[4].value = scriptData_FuncParam.m_value5;
				advcommand.param.argParam[5].ArgType = scriptData_FuncParam.argType6;
				advcommand.param.argParam[5].value = scriptData_FuncParam.m_value6;
				this.m_CommandList.Add(advcommand);
				if (advcommand.param.FuncName == "PlayVOICE")
				{
					this.m_Player.SoundManager.LoadVOICECharaID(advcommand.param.argParam[0].value);
				}
				else if (advcommand.param.FuncName == "CharaShot" || advcommand.param.FuncName == "SetTarget")
				{
					for (int j = 0; j < advcommand.param.ArgNum; j++)
					{
						this.m_Player.AddNeedStandPic(i, advcommand.param.argParam[j].value, null);
					}
				}
				else if (advcommand.param.FuncName == "CharaShotFade")
				{
					for (int k = 0; k < advcommand.param.ArgNum - 1; k++)
					{
						this.m_Player.AddNeedStandPic(i, advcommand.param.argParam[k].value, null);
					}
				}
				else if (advcommand.param.FuncName == "CharaIn" || advcommand.param.FuncName == "CharaInFade")
				{
					this.m_Player.AddNeedStandPic(i, advcommand.param.argParam[0].value, null);
				}
				else if (advcommand.param.FuncName == "CharaPose")
				{
					this.m_Player.AddNeedStandPic(i, advcommand.param.argParam[0].value, advcommand.param.argParam[1].value);
				}
				else if (advcommand.param.FuncName == "Effect" || advcommand.param.FuncName == "EffectChara" || advcommand.param.FuncName == "EffectScreen" || advcommand.param.FuncName == "EffectLoop" || advcommand.param.FuncName == "EffectCharaLoop" || advcommand.param.FuncName == "EffectScreenLoop")
				{
					this.m_Player.AddNeedEffect(advcommand.param.argParam[0].value);
				}
				else if (advcommand.param.FuncName == "EffectLoopTarget" || advcommand.param.FuncName == "EffectCharaLoopTarget" || advcommand.param.FuncName == "EffectScreenLoopTarget")
				{
					this.m_Player.AddNeedEffect(advcommand.param.argParam[0].value);
					this.m_Player.AddNeedEffect(advcommand.param.argParam[1].value);
				}
				else if (advcommand.param.FuncName == "PresetEffectLoop")
				{
					this.m_Player.AddNeedEffect(advcommand.param.argParam[0].value);
					this.m_Player.AddNeedEffect(advcommand.param.argParam[1].value);
					this.m_Player.AddNeedEffect(advcommand.param.argParam[2].value);
				}
				else if (advcommand.param.FuncName == "CharaEmotion")
				{
					this.m_Player.AddNeedEmo(advcommand.param.argParam[1].value);
				}
				else if (advcommand.param.FuncName == "SpriteVisible")
				{
					this.m_Player.AddNeedSprite(advcommand.param.argParam[1].value, int.Parse(advcommand.param.argParam[0].value), null);
				}
				else if (advcommand.param.FuncName == "BGVisible")
				{
					this.m_Player.BackGroundManager.Load(advcommand.param.argParam[1].value);
				}
				else if (advcommand.param.FuncName == "CharaTalk" || advcommand.param.FuncName == "AddNovelText")
				{
					if (advcommand.param.FuncName == "CharaTalk" && advcommand.param.ArgNum >= 3)
					{
						this.m_Player.AddNeedEmo(advcommand.param.argParam[2].value);
					}
					string voiceLabel = this.m_TextDB.m_Params[int.Parse(advcommand.param.argParam[0].value)].m_voiceLabel;
					if (!string.IsNullOrEmpty(voiceLabel) && voiceLabel != "-1")
					{
						string talkerADVCharaID = this.m_Player.GetTalkerADVCharaID(uint.Parse(advcommand.param.argParam[0].value));
						this.m_Player.SoundManager.LoadVOICECharaID(talkerADVCharaID);
					}
					string text = this.m_TextDB.m_Params[int.Parse(advcommand.param.argParam[0].value)].m_text;
					while (text.Length > 0)
					{
						int commandsCount = this.m_Player.GetCommandsCount();
						int num2 = text.IndexOf("<");
						string text2 = null;
						if (num2 < 0)
						{
							break;
						}
						for (int l = 0; l < commandsCount; l++)
						{
							text2 = this.m_Player.GetTagCommand((ADVParser.EventType)l);
							if (num2 != text.IndexOf("<" + text2))
							{
								text2 = null;
							}
							else if (text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_emo) || text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_pose) || text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_voice) || text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_pose))
							{
								l = commandsCount;
							}
							else
							{
								text2 = null;
							}
						}
						if (text2 == null)
						{
							text = text.Substring(1);
						}
						else
						{
							int num3 = text.IndexOf(">", num2) + 1;
							string text3 = text.Substring(num2, num3 - num2);
							int num4 = text3.IndexOf("=") + 1;
							int num5 = text3.IndexOf(",");
							string text4 = text3.Substring(num4, num5 - num4);
							num4 = num5 + 1;
							num5 = text3.IndexOf(">");
							string text5 = text3.Substring(num4, num5 - num4);
							if (text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_emo))
							{
								this.m_Player.AddNeedEmo(text5);
							}
							else if (text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_pose))
							{
								this.m_Player.AddNeedStandPic(i, text4, text5);
							}
							else if (text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_voice))
							{
								this.m_Player.SoundManager.LoadVOICECharaID(text4);
							}
							else if (text2 == this.m_Player.GetTagCommand(ADVParser.EventType.event_pose))
							{
								this.m_Player.AddNeedStandPic(i, text4, text5);
							}
							text = text.Substring(num3);
						}
					}
				}
			}
			this.m_Player.DestroyAllNotNeedStandPics();
			this.m_Player.UnloadAllNotNeedEffect();
			this.m_Player.UnLoadAllNotNeedSprite();
			SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
			this.m_Player.CreateAllNeedStandPics();
			this.m_Player.LoadRecentNeedStandPics();
			this.m_Player.LoadAllNeedEffect();
			this.m_Player.LoadAllNeedSprite();
		}

		// Token: 0x06000377 RID: 887 RVA: 0x000110A6 File Offset: 0x0000F4A6
		public void Destroy()
		{
			this.m_Loader.UnloadAll(false);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x000110B4 File Offset: 0x0000F4B4
		public bool UpdateDestroy()
		{
			if (this.m_Loader != null && !this.m_Loader.IsCompleteUnloadAll())
			{
				this.m_Loader.Update();
				return this.m_Loader.IsCompleteUnloadAll();
			}
			return true;
		}

		// Token: 0x06000379 RID: 889 RVA: 0x000110E9 File Offset: 0x0000F4E9
		public void Play(int startCommandIdx = 0)
		{
			this.m_NowCommandIdx = startCommandIdx;
			this.m_IsEnd = false;
		}

		// Token: 0x0600037A RID: 890 RVA: 0x000110FC File Offset: 0x0000F4FC
		public void DoCommand()
		{
			if (this.m_NowCommandIdx >= this.m_CommandList.Count)
			{
				return;
			}
			ScriptPlayer.ADVCommand advcommand = this.m_CommandList[this.m_NowCommandIdx];
			this.m_NowCommandIdx++;
			if (this.m_NowCommandIdx >= this.m_CommandList.Count)
			{
				this.m_IsEnd = true;
			}
			int argNum = advcommand.param.ArgNum;
			if (argNum == 0)
			{
				MethodInfo method = this.m_ADVCommandInherited.GetType().GetMethod(advcommand.param.FuncName, new Type[0]);
				if (method != null)
				{
					method.Invoke(this.m_ADVCommandInherited, null);
				}
			}
			else
			{
				object[] array = new object[argNum];
				Type[] array2 = new Type[argNum];
				for (int i = 0; i < argNum; i++)
				{
					object obj = null;
					switch (advcommand.param.argParam[i].ArgType)
					{
					case 1:
						array2[i] = typeof(sbyte);
						obj = Convert.ToSByte(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(sbyte));
						break;
					case 2:
						array2[i] = typeof(short);
						obj = Convert.ToInt16(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(short));
						break;
					case 3:
						array2[i] = typeof(int);
						obj = Convert.ToInt32(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(int));
						break;
					case 4:
						array2[i] = typeof(long);
						obj = Convert.ToInt64(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(long));
						break;
					case 5:
						array2[i] = typeof(byte);
						obj = Convert.ToByte(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(byte));
						break;
					case 6:
						array2[i] = typeof(ushort);
						obj = Convert.ToUInt16(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(ushort));
						break;
					case 7:
						array2[i] = typeof(uint);
						obj = Convert.ToUInt32(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(uint));
						break;
					case 8:
						array2[i] = typeof(ulong);
						obj = Convert.ToUInt64(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(ulong));
						break;
					case 9:
						array2[i] = typeof(float);
						obj = Convert.ToSingle(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(float));
						break;
					case 10:
						array2[i] = typeof(double);
						obj = Convert.ToDouble(advcommand.param.argParam[i].value);
						Convert.ChangeType(obj, typeof(double));
						break;
					case 11:
					case 12:
					case 13:
						array2[i] = typeof(string);
						obj = advcommand.param.argParam[i].value;
						break;
					}
					array[i] = obj;
				}
				MethodInfo method2 = this.m_ADVCommandInherited.GetType().GetMethod(advcommand.param.FuncName, array2);
				if (method2 != null)
				{
					method2.Invoke(this.m_ADVCommandInherited, array);
				}
			}
		}

		// Token: 0x0600037B RID: 891 RVA: 0x00011574 File Offset: 0x0000F974
		public void Goto(int scriptId)
		{
			this.ParseScript(scriptId);
		}

		// Token: 0x0600037C RID: 892 RVA: 0x00011580 File Offset: 0x0000F980
		public List<int> GetSkipLogTextList()
		{
			List<int> list = new List<int>();
			list.Clear();
			for (int i = 0; i < this.m_ScriptDB.m_Params.Length; i++)
			{
				ScriptData_Param scriptData_Param = this.m_ScriptDB.m_Params[i];
				int num = scriptData_Param.FuncParam.Length;
				for (int j = 0; j < num; j++)
				{
					ScriptData_FuncParam scriptData_FuncParam = scriptData_Param.FuncParam[j];
					if (scriptData_FuncParam.funcName == "CharaTalk" || scriptData_FuncParam.funcName == "AddNovelText")
					{
						int item = int.Parse(scriptData_FuncParam.m_value1);
						list.Add(item);
					}
				}
			}
			return list;
		}

		// Token: 0x040001E2 RID: 482
		private List<ScriptPlayer.ADVCommand> m_CommandList;

		// Token: 0x040001E3 RID: 483
		private int m_NowCommandIdx;

		// Token: 0x040001E4 RID: 484
		private ADVScriptCommandInherited m_ADVCommandInherited;

		// Token: 0x040001E5 RID: 485
		private bool m_IsEnd;

		// Token: 0x040001E6 RID: 486
		private string m_ScriptPath;

		// Token: 0x040001E7 RID: 487
		private string m_TextDBPath;

		// Token: 0x040001E8 RID: 488
		private bool m_IsDonePrepare;

		// Token: 0x040001E9 RID: 489
		private bool m_IsError;

		// Token: 0x040001EA RID: 490
		private ABResourceLoader m_Loader;

		// Token: 0x040001EB RID: 491
		private ScriptData m_ScriptDB;

		// Token: 0x040001EC RID: 492
		private ABResourceObjectHandler m_HndlScript;

		// Token: 0x040001ED RID: 493
		private ADVTextDB m_TextDB;

		// Token: 0x040001EE RID: 494
		private ABResourceObjectHandler m_HndlTextDB;

		// Token: 0x040001EF RID: 495
		private ADVPlayer m_Player;

		// Token: 0x02000075 RID: 117
		public class ADVCommand
		{
			// Token: 0x040001F0 RID: 496
			public ScriptPlayer.ADVCommand.Param param = new ScriptPlayer.ADVCommand.Param();

			// Token: 0x02000076 RID: 118
			public class Param
			{
				// Token: 0x040001F1 RID: 497
				public string FuncName;

				// Token: 0x040001F2 RID: 498
				public int ArgNum;

				// Token: 0x040001F3 RID: 499
				public ScriptPlayer.ADVCommand.ArgParam[] argParam;
			}

			// Token: 0x02000077 RID: 119
			public struct ArgParam
			{
				// Token: 0x040001F4 RID: 500
				public int ArgType;

				// Token: 0x040001F5 RID: 501
				public string value;
			}
		}
	}
}
