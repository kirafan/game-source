﻿using System;

namespace Star.ADV
{
	// Token: 0x02000043 RID: 67
	public class ADVTextSeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x0600029F RID: 671 RVA: 0x0000F65A File Offset: 0x0000DA5A
		protected ADVTextSeTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_se, isEndTag)
		{
		}
	}
}
