﻿using System;

namespace Star.ADV
{
	// Token: 0x0200004E RID: 78
	public class ADVTextMotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002B2 RID: 690 RVA: 0x0000FA2C File Offset: 0x0000DE2C
		protected ADVTextMotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_motion, isEndTag)
		{
		}
	}
}
