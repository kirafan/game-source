﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200003F RID: 63
	public class ADVTextStrongTagEnd : ADVTextStrongTagBase
	{
		// Token: 0x06000298 RID: 664 RVA: 0x0000F4F7 File Offset: 0x0000D8F7
		public ADVTextStrongTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x06000299 RID: 665 RVA: 0x0000F504 File Offset: 0x0000D904
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			string text2 = "</size>";
			text = text.Insert(idx, text2);
			idx += text2.Length;
			return true;
		}
	}
}
