﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200005B RID: 91
	public class ADVTextFaceTag : ADVTextFaceTagBase
	{
		// Token: 0x060002C7 RID: 711 RVA: 0x0000FD82 File Offset: 0x0000E182
		public ADVTextFaceTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x0000FD90 File Offset: 0x0000E190
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int num2 = tag.IndexOf(",") - num;
				string text2 = tag.Substring(num, num2);
				tag.Remove(num, num2);
				num = tag.IndexOf(",") + 1;
				num2 = tag.IndexOf(">") - num;
				string advtextTagArgRefName = tag.Substring(num, num2);
				ADVStandPic standPic = this.m_Player.StandPicManager.GetStandPic(text2);
				if (standPic == null)
				{
					Debug.LogError("指定されたキャラクタが存在しません. キャラクタ：" + text2.ToString());
				}
				else
				{
					standPic.ApplyFace((eADVFace)this.m_Player.GetADVTextTagParam(advtextTagArgRefName).m_EnumID);
				}
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
