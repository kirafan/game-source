﻿using System;

namespace Star.ADV
{
	// Token: 0x02000040 RID: 64
	public class ADVTextRubyTagBase : ADVTextRangeTagBase
	{
		// Token: 0x0600029A RID: 666 RVA: 0x0000F53E File Offset: 0x0000D93E
		protected ADVTextRubyTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_ruby, isEndTag)
		{
		}
	}
}
