﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000050 RID: 80
	public class ADVTextMotionTagEnd : ADVTextMotionTagBase
	{
		// Token: 0x060002B5 RID: 693 RVA: 0x0000FAEB File Offset: 0x0000DEEB
		public ADVTextMotionTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000FAF6 File Offset: 0x0000DEF6
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
