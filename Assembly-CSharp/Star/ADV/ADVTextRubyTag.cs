﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000041 RID: 65
	public class ADVTextRubyTag : ADVTextRubyTagBase
	{
		// Token: 0x0600029B RID: 667 RVA: 0x0000F54B File Offset: 0x0000D94B
		public ADVTextRubyTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000F558 File Offset: 0x0000D958
		public override bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			int num = tag.IndexOf("=") + 1;
			int length = tag.IndexOf(">") - num;
			string text2 = tag.Substring(num, length);
			string text3 = text.Substring(0, idx);
			string text4 = text.Substring(idx, text.Length - idx);
			int num2 = text4.IndexOf("</ruby>");
			if (num2 == -1)
			{
				Debug.Log("not found </ruby>");
			}
			else
			{
				List<ADVParser.RubyData> list = new List<ADVParser.RubyData>();
				string text5 = this.m_ParentParser.ParseAll(text3, false, false, null, ref list);
				Text obj;
				Vector2 start = calcTextObj.CalcLastPosition(text5, out obj);
				text5 = this.m_ParentParser.ParseAll(text.Substring(0, idx + num2), false, false, null, ref list);
				Vector2 end = calcTextObj.CalcLastPosition(text5, out obj);
				ADVParser.RubyData rubyData = new ADVParser.RubyData();
				rubyData.Set(text2, start, end, obj);
				rubyList.Add(rubyData);
			}
			return false;
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000F645 File Offset: 0x0000DA45
		public override bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return calcTextObj != null;
		}
	}
}
