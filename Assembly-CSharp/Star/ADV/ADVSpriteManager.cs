﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000030 RID: 48
	public class ADVSpriteManager : MonoBehaviour
	{
		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000228 RID: 552 RVA: 0x0000CCDE File Offset: 0x0000B0DE
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000229 RID: 553 RVA: 0x0000CCE6 File Offset: 0x0000B0E6
		public bool IsDonePrepare
		{
			get
			{
				return this.m_IsDonePrepare;
			}
		}

		// Token: 0x0600022A RID: 554 RVA: 0x0000CCEE File Offset: 0x0000B0EE
		public void Prepare(ADVPlayer player)
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_NeedList = new List<int>();
			this.m_ImageList = new List<ADVSpriteManager.ImageInfo>();
			this.m_InstList = new List<ADVSprite>();
			this.m_ImageListId = -1;
		}

		// Token: 0x0600022B RID: 555 RVA: 0x0000CD24 File Offset: 0x0000B124
		public void ClearNeedList()
		{
			this.m_NeedList.Clear();
		}

		// Token: 0x0600022C RID: 556 RVA: 0x0000CD31 File Offset: 0x0000B131
		public int AddNeedList(string fileName, int instId = -1, Image destination = null)
		{
			instId = this.AddADVSprite(fileName, instId, destination);
			if (0 <= instId)
			{
				this.m_NeedList.Add(instId);
				return instId;
			}
			return -1;
		}

		// Token: 0x0600022D RID: 557 RVA: 0x0000CD54 File Offset: 0x0000B154
		public int AddADVSprite(string fileName, int instId = -1, Image destination = null)
		{
			ADVSpriteManager.ImageInfo imageInfo = this.GetImageInfoFromFileName(fileName);
			if (imageInfo == null)
			{
				imageInfo = new ADVSpriteManager.ImageInfo(++this.m_ImageListId, fileName);
				this.m_ImageList.Add(imageInfo);
			}
			if (imageInfo != null)
			{
				ADVSprite advsprite = this.GetADVSpriteFromInstID(instId);
				if (advsprite == null)
				{
					if (instId < 0)
					{
						instId = this.GetNotRegisteredInstIdMin();
					}
					advsprite = UnityEngine.Object.Instantiate<ADVSprite>(this.m_Prefab, this.m_RectTransform, false);
					this.m_InstList.Add(advsprite);
				}
				advsprite.Initialize(instId, imageInfo.m_ImageID, null);
				return advsprite.m_InstID;
			}
			return -1;
		}

		// Token: 0x0600022E RID: 558 RVA: 0x0000CDF4 File Offset: 0x0000B1F4
		public void UnLoadAllNotNeedSprite()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_InstList[i].SetImageEnabled(false);
			}
			List<ADVSprite> list = new List<ADVSprite>();
			list.Clear();
			for (int j = 0; j < this.m_InstList.Count; j++)
			{
				bool flag = false;
				for (int k = 0; k < this.m_NeedList.Count; k++)
				{
					if (this.m_InstList[j].m_InstID == this.m_NeedList[k])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					this.m_InstList[j].Destroy();
					this.m_InstList[j] = null;
					list.Add(this.m_InstList[j]);
				}
			}
			for (int l = 0; l < list.Count; l++)
			{
				this.m_InstList.Remove(list[l]);
			}
			list.Clear();
			List<int> list2 = new List<int>();
			for (int m = 0; m < this.m_InstList.Count; m++)
			{
				list2.Add(this.m_InstList[m].m_ImageID);
			}
			for (int n = 0; n < list2.Count; n++)
			{
				for (int num = n + 1; num < list2.Count; num++)
				{
					if (list2[n] == list2[num])
					{
						list2.RemoveAt(num);
						num--;
					}
				}
			}
			for (int num2 = 0; num2 < this.m_ImageList.Count; num2++)
			{
				bool flag2 = false;
				for (int num3 = 0; num3 < list2.Count; num3++)
				{
					if (this.m_ImageList[num2].m_ImageID == list2[num3])
					{
						flag2 = true;
						num3 = list2.Count;
					}
				}
				if (!flag2)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_ImageList[num2].m_SpriteHandler);
					this.m_ImageList[num2].m_SpriteHandler = null;
					this.m_ImageList.RemoveAt(num2);
					num2--;
				}
			}
		}

		// Token: 0x0600022F RID: 559 RVA: 0x0000D068 File Offset: 0x0000B468
		public void LoadAllNeedSprite()
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				this.Load(this.m_NeedList[i]);
			}
		}

		// Token: 0x06000230 RID: 560 RVA: 0x0000D0A4 File Offset: 0x0000B4A4
		public int Load(string fileName, int instId = -1, Image destination = null)
		{
			instId = this.AddADVSprite(fileName, instId, destination);
			if (0 <= instId)
			{
				return this.Load(instId);
			}
			return -1;
		}

		// Token: 0x06000231 RID: 561 RVA: 0x0000D0C4 File Offset: 0x0000B4C4
		private int Load(int instId)
		{
			ADVSprite advspriteFromInstID = this.GetADVSpriteFromInstID(instId);
			if (advspriteFromInstID == null)
			{
				return -1;
			}
			if (advspriteFromInstID.IsAvailable())
			{
				return advspriteFromInstID.m_InstID;
			}
			ADVSpriteManager.ImageInfo imageInfoFromImageID = this.GetImageInfoFromImageID(advspriteFromInstID.m_ImageID);
			if (imageInfoFromImageID == null)
			{
				return -1;
			}
			if (imageInfoFromImageID.m_SpriteHandler == null)
			{
				imageInfoFromImageID.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVSprite(imageInfoFromImageID.m_FileName);
			}
			this.m_IsDonePrepare = false;
			return instId;
		}

		// Token: 0x06000232 RID: 562 RVA: 0x0000D13C File Offset: 0x0000B53C
		public void UpdateLoad()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (!this.m_InstList[i].IsAvailable())
				{
					ADVSpriteManager.ImageInfo imageInfoFromImageID = this.GetImageInfoFromImageID(this.m_InstList[i].m_ImageID);
					if (imageInfoFromImageID != null)
					{
						if (imageInfoFromImageID.m_SpriteHandler != null)
						{
							if (!imageInfoFromImageID.m_SpriteHandler.IsAvailable())
							{
								return;
							}
							this.m_InstList[i].SetSprite(imageInfoFromImageID.m_SpriteHandler.Obj, this.RectTransform);
						}
					}
				}
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x06000233 RID: 563 RVA: 0x0000D1F2 File Offset: 0x0000B5F2
		private void Update()
		{
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000D1F4 File Offset: 0x0000B5F4
		public void Destroy()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_InstList[i].Destroy();
				this.m_InstList[i] = null;
			}
			this.m_InstList.Clear();
			for (int j = 0; j < this.m_ImageList.Count; j++)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_ImageList[j].m_SpriteHandler);
				this.m_ImageList[j].m_SpriteHandler = null;
				this.m_ImageList.RemoveAt(j);
				j--;
			}
			this.m_ImageList.Clear();
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000D2B0 File Offset: 0x0000B6B0
		private ADVSpriteManager.ImageInfo GetImageInfoFromImageID(int imageId)
		{
			for (int i = 0; i < this.m_ImageList.Count; i++)
			{
				if (this.m_ImageList[i].m_FileName != null && this.m_ImageList[i].m_ImageID == imageId)
				{
					return this.m_ImageList[i];
				}
			}
			return null;
		}

		// Token: 0x06000236 RID: 566 RVA: 0x0000D314 File Offset: 0x0000B714
		private ADVSpriteManager.ImageInfo GetImageInfoFromFileName(string fileName)
		{
			for (int i = 0; i < this.m_ImageList.Count; i++)
			{
				if (this.m_ImageList[i].m_FileName != null && this.m_ImageList[i].m_FileName == fileName)
				{
					return this.m_ImageList[i];
				}
			}
			return null;
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000D380 File Offset: 0x0000B780
		private ADVSprite GetADVSpriteFromInstID(int instId)
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].m_InstID == instId)
				{
					return this.m_InstList[i];
				}
			}
			return null;
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000D3D0 File Offset: 0x0000B7D0
		private int GetADVSpriteIndexFromInstID(int instId)
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].m_InstID == instId)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000D414 File Offset: 0x0000B814
		private int GetNotRegisteredInstIdMin()
		{
			for (int i = 0; i < 1000000; i++)
			{
				int num = -1;
				for (int j = 0; j < this.m_InstList.Count; j++)
				{
					if (this.m_InstList[j].m_InstID == i)
					{
						num = j;
						j = this.m_InstList.Count;
					}
				}
				if (num < 0)
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000D484 File Offset: 0x0000B884
		public void EnableRender(uint id, bool flag)
		{
			ADVSprite advspriteFromInstID = this.GetADVSpriteFromInstID((int)id);
			if (advspriteFromInstID == null)
			{
				this.OutputNotFoundError(MethodBase.GetCurrentMethod().Name, (int)id);
				return;
			}
			advspriteFromInstID.SetImageEnabled(flag);
		}

		// Token: 0x0600023B RID: 571 RVA: 0x0000D4C0 File Offset: 0x0000B8C0
		public void Move(uint id, Vector2 pos, float sec = 0f)
		{
			ADVSprite advspriteFromInstID = this.GetADVSpriteFromInstID((int)id);
			if (advspriteFromInstID == null)
			{
				this.OutputNotFoundError(MethodBase.GetCurrentMethod().Name, (int)id);
				return;
			}
			advspriteFromInstID.Move(pos, sec);
		}

		// Token: 0x0600023C RID: 572 RVA: 0x0000D4FC File Offset: 0x0000B8FC
		public void Scale(uint id, Vector2 scale, float sec = 0f)
		{
			ADVSprite advspriteFromInstID = this.GetADVSpriteFromInstID((int)id);
			if (advspriteFromInstID == null)
			{
				this.OutputNotFoundError(MethodBase.GetCurrentMethod().Name, (int)id);
				return;
			}
			advspriteFromInstID.Scale(scale, sec);
		}

		// Token: 0x0600023D RID: 573 RVA: 0x0000D538 File Offset: 0x0000B938
		public void SetColor(uint id, Color color, float sec = 0f)
		{
			ADVSprite advspriteFromInstID = this.GetADVSpriteFromInstID((int)id);
			if (advspriteFromInstID == null)
			{
				this.OutputNotFoundError(MethodBase.GetCurrentMethod().Name, (int)id);
				return;
			}
			advspriteFromInstID.SetColor(color, sec);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x0000D573 File Offset: 0x0000B973
		protected void OutputNotFoundError(string functionName, int id)
		{
			Debug.LogError(string.Concat(new string[]
			{
				"[",
				functionName,
				"] ADVスプライトID : ",
				id.ToString(),
				" が見つかりません.コマンドは実行されませんでした."
			}));
		}

		// Token: 0x04000153 RID: 339
		private List<int> m_NeedList;

		// Token: 0x04000154 RID: 340
		private List<ADVSpriteManager.ImageInfo> m_ImageList;

		// Token: 0x04000155 RID: 341
		private int m_ImageListId = -1;

		// Token: 0x04000156 RID: 342
		private List<ADVSprite> m_InstList;

		// Token: 0x04000157 RID: 343
		private bool m_IsDonePrepare;

		// Token: 0x04000158 RID: 344
		[SerializeField]
		private ADVSprite m_Prefab;

		// Token: 0x04000159 RID: 345
		private RectTransform m_RectTransform;

		// Token: 0x02000031 RID: 49
		private class ImageInfo
		{
			// Token: 0x0600023F RID: 575 RVA: 0x0000D5B1 File Offset: 0x0000B9B1
			public ImageInfo(int imageId, string fileName)
			{
				this.m_ImageID = imageId;
				this.m_FileName = fileName;
			}

			// Token: 0x0400015A RID: 346
			public int m_ImageID;

			// Token: 0x0400015B RID: 347
			public string m_FileName;

			// Token: 0x0400015C RID: 348
			public SpriteHandler m_SpriteHandler;
		}
	}
}
