﻿using System;
using System.Collections.Generic;
using System.Text;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000021 RID: 33
	public class ADVParser
	{
		// Token: 0x060000D1 RID: 209 RVA: 0x00007107 File Offset: 0x00005507
		public string GetTagCommand(ADVParser.EventType eventType)
		{
			return this.m_TagCommand[(int)eventType];
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00007111 File Offset: 0x00005511
		public int GetCommandsCount()
		{
			return this.m_TagCommand.Length;
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x0000711B File Offset: 0x0000551B
		public void Setup(ADVPlayer player, ADVTalkCustomWindow talkCustomWindow)
		{
			this.m_Player = player;
			this.m_TalkCustomWindow = talkCustomWindow;
			this.m_IsTapWaitWithTap = false;
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00007134 File Offset: 0x00005534
		public string ParseAll(string text, bool execTag, bool execBreak, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			int i = 0;
			List<ADVParser.EventType> list = new List<ADVParser.EventType>();
			bool flag = false;
			string result = text;
			while (i < text.Length)
			{
				result = this.Parse(ref text, ref i, ref list, execTag, execBreak, out flag, calcTextObj, ref rubyList);
			}
			return result;
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00007174 File Offset: 0x00005574
		public string Parse(ref string text, ref int idx, ref List<ADVParser.EventType> evList, bool execTag, bool execBreak, out bool breakFlag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			breakFlag = false;
			if (idx < 0)
			{
				Debug.LogError(string.Concat(new object[]
				{
					"error ParseText",
					text,
					" , idx=",
					idx
				}));
				return text;
			}
			if (idx + 1 > text.Length)
			{
				Debug.LogError(string.Concat(new object[]
				{
					"error ParseText",
					text,
					" , idx=",
					idx
				}));
				return text;
			}
			char c = text.Substring(0, idx + 1)[idx];
			if (c != '<')
			{
				idx++;
			}
			else
			{
				while (c == '<')
				{
					int num = idx;
					char c2 = ' ';
					while (c2 != '>')
					{
						if (idx >= text.Length)
						{
							Debug.LogError("not Found EndTag.");
							break;
						}
						c2 = text.Substring(0, idx + 1)[idx];
						idx++;
					}
					int length = idx - num;
					string tag = text.Substring(num, length);
					breakFlag = this.AnalyzeTag(ref text, ref idx, ref evList, tag, execTag, calcTextObj, ref rubyList);
					if (idx > 0 && idx < text.Length)
					{
						c = text[idx];
					}
					else
					{
						c = ' ';
					}
					if (execBreak && breakFlag)
					{
						break;
					}
				}
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(text.Substring(0, idx));
			for (int i = evList.Count - 1; i >= 0; i--)
			{
				if (evList[i] == ADVParser.EventType.event_color)
				{
					stringBuilder.Append("</color>");
				}
				else if (evList[i] == ADVParser.EventType.event_strong)
				{
					stringBuilder.Append("</size>");
				}
				else if (evList[i] == ADVParser.EventType.event_size)
				{
					stringBuilder.Append("</size>");
				}
				else if (evList[i] == ADVParser.EventType.event_bold)
				{
					stringBuilder.Append("</b>");
				}
				else if (evList[i] == ADVParser.EventType.event_italic)
				{
					stringBuilder.Append("</i>");
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x000073C8 File Offset: 0x000057C8
		private bool AnalyzeTag(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			ADVTextTag advtextTag = this.ConvertTagStringToADVTextTag(tag);
			if (advtextTag == null)
			{
			}
			if (advtextTag.GetTagType() == ADVTextTag.eTagType.RangeEnd)
			{
				evList.Remove(advtextTag.GetEventType());
			}
			advtextTag.SetTapWaitWithTapFlag(this.m_IsTapWaitWithTap);
			return advtextTag.Analyze(ref text, ref idx, ref evList, tag, execTag, calcTextObj, ref rubyList);
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x0000741C File Offset: 0x0000581C
		private ADVTextTag ConvertTagStringToADVTextTag(string tagString)
		{
			for (int i = 1; i < this.m_TagCommand.Length; i++)
			{
				int num = tagString.IndexOf(this.m_TagCommand[i]);
				bool flag = false;
				bool flag2 = false;
				if (num == 1)
				{
					flag = true;
					flag2 = false;
				}
				else if (tagString.Substring(0, 2) == "</" && num == 2)
				{
					flag = true;
					flag2 = true;
				}
				if (flag)
				{
					switch (i)
					{
					case 1:
						return (!flag2) ? new ADVTextSeTag(this.m_Player, this) : new ADVTextSeTagEnd(this.m_Player, this);
					case 2:
						return (!flag2) ? new ADVTextVoiceTag(this.m_Player, this) : new ADVTextVoiceTagEnd(this.m_Player, this);
					case 3:
						return (!flag2) ? new ADVTextMotionTag(this.m_Player, this) : new ADVTextMotionTagEnd(this.m_Player, this);
					case 4:
						return (!flag2) ? new ADVTextWaitMotionTag(this.m_Player, this) : new ADVTextWaitMotionTagEnd(this.m_Player, this);
					case 5:
						return new ADVTextMoveTag(this.m_Player, this);
					case 6:
						return new ADVTextWaitMoveTag(this.m_Player, this);
					case 7:
						return (!flag2) ? new ADVTextEmotionTag(this.m_Player, this) : new ADVTextEmotionTagEnd(this.m_Player, this);
					case 8:
						return (!flag2) ? new ADVTextFaceTag(this.m_Player, this) : new ADVTextFaceTagEnd(this.m_Player, this);
					case 9:
						return (!flag2) ? new ADVTextPoseTag(this.m_Player, this) : new ADVTextPoseTagEnd(this.m_Player, this);
					case 10:
						return (!flag2) ? new ADVTextShakeTag(this.m_Player, this) : new ADVTextShakeTagEnd(this.m_Player, this);
					case 13:
						return (!flag2) ? new ADVTextStrongTag(this.m_Player, this) : new ADVTextStrongTagEnd(this.m_Player, this);
					case 14:
						return (!flag2) ? new ADVTextRubyTag(this.m_Player, this) : new ADVTextRubyTagEnd(this.m_Player, this);
					case 15:
						return (!flag2) ? new ADVTextWaitTag(this.m_Player, this) : new ADVTextWaitTagEnd(this.m_Player, this);
					case 18:
						return new ADVTextPageTag(this.m_Player, this);
					case 19:
						return new ADVTextShiftTag(this.m_Player, this);
					}
					return new ADVTextOtherTag(this.m_Player, this, (ADVParser.EventType)i, flag2);
				}
			}
			return null;
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x000076BC File Offset: 0x00005ABC
		private void DeleteTag(ref string text, ref int idx, int charNum)
		{
			text = text.Remove(idx - charNum, charNum);
			idx -= charNum;
			if (idx > text.Length)
			{
				idx = text.Length;
			}
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x000076E9 File Offset: 0x00005AE9
		public ADVPlayer GetADVPlayer()
		{
			return this.m_Player;
		}

		// Token: 0x060000DA RID: 218 RVA: 0x000076F1 File Offset: 0x00005AF1
		public ADVTalkCustomWindow GetTalkWindow()
		{
			return this.m_TalkCustomWindow;
		}

		// Token: 0x060000DB RID: 219 RVA: 0x000076F9 File Offset: 0x00005AF9
		public void SetTapWaitWithTapFlag(bool flag)
		{
			this.m_IsTapWaitWithTap = flag;
		}

		// Token: 0x040000D2 RID: 210
		private ADVPlayer m_Player;

		// Token: 0x040000D3 RID: 211
		private ADVTalkCustomWindow m_TalkCustomWindow;

		// Token: 0x040000D4 RID: 212
		private bool m_IsTapWaitWithTap;

		// Token: 0x040000D5 RID: 213
		private string[] m_TagCommand = new string[]
		{
			string.Empty,
			"se",
			"voice",
			"mot",
			"waitmot",
			"mov",
			"waitmov",
			"emo",
			"face",
			"pose",
			"shake",
			"color",
			"size",
			"strong",
			"ruby",
			"wait",
			"b",
			"i",
			"page",
			"shift"
		};

		// Token: 0x02000022 RID: 34
		public enum EventType
		{
			// Token: 0x040000D7 RID: 215
			event_none,
			// Token: 0x040000D8 RID: 216
			event_se,
			// Token: 0x040000D9 RID: 217
			event_voice,
			// Token: 0x040000DA RID: 218
			event_motion,
			// Token: 0x040000DB RID: 219
			event_wait_motion,
			// Token: 0x040000DC RID: 220
			event_move,
			// Token: 0x040000DD RID: 221
			event_wait_move,
			// Token: 0x040000DE RID: 222
			event_emo,
			// Token: 0x040000DF RID: 223
			event_face,
			// Token: 0x040000E0 RID: 224
			event_pose,
			// Token: 0x040000E1 RID: 225
			event_shake,
			// Token: 0x040000E2 RID: 226
			event_color,
			// Token: 0x040000E3 RID: 227
			event_size,
			// Token: 0x040000E4 RID: 228
			event_strong,
			// Token: 0x040000E5 RID: 229
			event_ruby,
			// Token: 0x040000E6 RID: 230
			event_wait,
			// Token: 0x040000E7 RID: 231
			event_bold,
			// Token: 0x040000E8 RID: 232
			event_italic,
			// Token: 0x040000E9 RID: 233
			event_page,
			// Token: 0x040000EA RID: 234
			event_shift,
			// Token: 0x040000EB RID: 235
			event_Max
		}

		// Token: 0x02000023 RID: 35
		public class TagInformation
		{
		}

		// Token: 0x02000024 RID: 36
		public class RubyData
		{
			// Token: 0x060000DE RID: 222 RVA: 0x00007712 File Offset: 0x00005B12
			public void Set(string text, Vector2 start, Vector2 end, Text obj)
			{
				this.m_Text = text;
				this.m_StartPos = start;
				this.m_EndPos = end;
				this.m_TextObj = obj;
			}

			// Token: 0x040000EC RID: 236
			public string m_Text;

			// Token: 0x040000ED RID: 237
			public Vector2 m_StartPos;

			// Token: 0x040000EE RID: 238
			public Vector2 m_EndPos;

			// Token: 0x040000EF RID: 239
			public Text m_TextObj;
		}
	}
}
