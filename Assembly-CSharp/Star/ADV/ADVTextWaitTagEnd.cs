﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200004B RID: 75
	public class ADVTextWaitTagEnd : ADVTextWaitTagBase
	{
		// Token: 0x060002AC RID: 684 RVA: 0x0000F8A1 File Offset: 0x0000DCA1
		public ADVTextWaitTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002AD RID: 685 RVA: 0x0000F8AC File Offset: 0x0000DCAC
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
