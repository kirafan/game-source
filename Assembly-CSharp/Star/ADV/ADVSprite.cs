﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x0200002F RID: 47
	public class ADVSprite : MonoBehaviour
	{
		// Token: 0x0600021D RID: 541 RVA: 0x0000C893 File Offset: 0x0000AC93
		public ADVSprite()
		{
			this.m_InstID = -1;
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000C8AC File Offset: 0x0000ACAC
		public bool Initialize(int instId, int imageId, Image image = null)
		{
			this.m_InstID = instId;
			this.m_ImageID = imageId;
			if (this.m_Image != null && image == null)
			{
				image = this.m_Image;
			}
			this.m_Image = image;
			this.m_IsAvailable = false;
			return true;
		}

		// Token: 0x0600021F RID: 543 RVA: 0x0000C8FB File Offset: 0x0000ACFB
		public void Destroy()
		{
			UnityEngine.Object.Destroy(this.m_Image.gameObject);
			this.m_Image = null;
		}

		// Token: 0x06000220 RID: 544 RVA: 0x0000C914 File Offset: 0x0000AD14
		public void SetSprite(Sprite sprite, RectTransform rectTransform)
		{
			if (this.m_Image == null)
			{
				this.m_Image = base.gameObject.GetComponent<Image>();
			}
			if (this.m_Image == null)
			{
				this.m_Image = base.gameObject.AddComponent<Image>();
			}
			if (this.m_Image == null)
			{
				return;
			}
			this.m_Image.sprite = sprite;
			this.m_Image.rectTransform.pivot = new Vector2(0.5f, 0.5f);
			this.m_Image.rectTransform.anchoredPosition = new Vector2(0f, 0f);
			this.SetImageEnabled(false);
			this.m_IsAvailable = true;
		}

		// Token: 0x06000221 RID: 545 RVA: 0x0000C9CF File Offset: 0x0000ADCF
		public bool IsAvailable()
		{
			return this.m_IsAvailable;
		}

		// Token: 0x06000222 RID: 546 RVA: 0x0000C9D7 File Offset: 0x0000ADD7
		public void SetImageEnabled(bool enabled)
		{
			if (this.m_Image != null)
			{
				this.m_Image.enabled = enabled;
			}
		}

		// Token: 0x06000223 RID: 547 RVA: 0x0000C9F8 File Offset: 0x0000ADF8
		public void Move(Vector2 pos, float sec = 0f)
		{
			GameObject gameObject = this.m_Image.gameObject;
			RectTransform rectTransform = this.m_Image.rectTransform;
			if (sec <= 0f)
			{
				rectTransform.localPosition = pos;
			}
			else
			{
				iTween.Stop(gameObject, "move");
				iTween.MoveTo(gameObject, new Hashtable
				{
					{
						"isLocal",
						true
					},
					{
						"x",
						pos.x
					},
					{
						"y",
						pos.y
					},
					{
						"time",
						sec
					},
					{
						"easeType",
						iTween.EaseType.linear
					},
					{
						"delay",
						0.1f
					}
				});
			}
		}

		// Token: 0x06000224 RID: 548 RVA: 0x0000CAC8 File Offset: 0x0000AEC8
		public void Scale(Vector2 scale, float sec = 0f)
		{
			GameObject gameObject = this.m_Image.gameObject;
			RectTransform rectTransform = this.m_Image.rectTransform;
			if (sec <= 0f)
			{
				rectTransform.localScale = scale;
			}
			else
			{
				iTween.Stop(gameObject, "scale");
				iTween.ScaleTo(gameObject, new Hashtable
				{
					{
						"isLocal",
						true
					},
					{
						"x",
						scale.x
					},
					{
						"y",
						scale.y
					},
					{
						"time",
						sec
					},
					{
						"easeType",
						iTween.EaseType.linear
					},
					{
						"delay",
						0.1f
					}
				});
			}
		}

		// Token: 0x06000225 RID: 549 RVA: 0x0000CB98 File Offset: 0x0000AF98
		public void SetColor(Color color, float sec = 0f)
		{
			GameObject gameObject = this.m_Image.gameObject;
			RectTransform rectTransform = this.m_Image.rectTransform;
			if (sec <= 0f)
			{
				this.m_Image.color = color;
			}
			else
			{
				ADVSprite advsprite = this.m_Image.GetComponent<ADVSprite>();
				if (advsprite == null)
				{
					advsprite = this.m_Image.gameObject.AddComponent<ADVSprite>();
				}
				advsprite.m_ColorTweenStart = this.m_Image.color;
				advsprite.m_ColorTweenEnd = color;
				iTween.Stop(gameObject, "value");
				iTween.ValueTo(gameObject, new Hashtable
				{
					{
						"from",
						0f
					},
					{
						"to",
						1f
					},
					{
						"time",
						sec
					},
					{
						"easeType",
						iTween.EaseType.linear
					},
					{
						"onupdate",
						"OnUpdateColor"
					},
					{
						"onupdatetarget",
						gameObject
					},
					{
						"delay",
						0.1f
					}
				});
			}
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000CCB0 File Offset: 0x0000B0B0
		public void OnUpdateColor(float value)
		{
			base.GetComponent<Image>().color = Color.Lerp(this.m_ColorTweenStart, this.m_ColorTweenEnd, value);
		}

		// Token: 0x0400014D RID: 333
		public int m_InstID = -1;

		// Token: 0x0400014E RID: 334
		public int m_ImageID;

		// Token: 0x0400014F RID: 335
		private Image m_Image;

		// Token: 0x04000150 RID: 336
		private bool m_IsAvailable;

		// Token: 0x04000151 RID: 337
		public Color m_ColorTweenStart;

		// Token: 0x04000152 RID: 338
		public Color m_ColorTweenEnd;
	}
}
