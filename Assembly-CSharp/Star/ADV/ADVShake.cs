﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200002B RID: 43
	public class ADVShake : MonoBehaviour
	{
		// Token: 0x060001FD RID: 509 RVA: 0x0000BDCC File Offset: 0x0000A1CC
		private void Awake()
		{
			this.UpdateTransform();
		}

		// Token: 0x060001FE RID: 510 RVA: 0x0000BDD4 File Offset: 0x0000A1D4
		private void Update()
		{
			if (this.m_IsShake)
			{
				this.m_ShakeTimeCount += Time.deltaTime;
				if (this.m_ShakeTimeCount >= this.m_ShakeMaxTime)
				{
					this.m_IsShake = false;
					this.m_NeedMoveToNext = false;
					this.m_ShakeTimeCount = 0f;
					this.ReturnToOrig();
				}
			}
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000BE30 File Offset: 0x0000A230
		public void Shake(ADVPlayer player, bool isAllShake, bool isThisCharaShake, ADVShake.eShakeType xType, float xRadius, ADVShake.eShakeType yType, float yRadius, float cycle, float time)
		{
			this.m_Player = player;
			this.m_IsAllShake = isAllShake;
			this.m_IsThisCharaShake = isThisCharaShake;
			this.m_IsShake = true;
			this.m_Transform.localPosition = this.m_OrigPos;
			this.m_ShakeData[0].m_Type = xType;
			this.m_ShakeData[0].m_Radius = xRadius;
			this.m_ShakeData[1].m_Type = yType;
			this.m_ShakeData[1].m_Radius = yRadius;
			this.m_Cycle = cycle;
			this.m_ShakeTimeCount = 0f;
			this.m_ShakeMaxTime = time;
			this.m_MoveCount = 0;
			this.MoveToNextPosition();
		}

		// Token: 0x06000200 RID: 512 RVA: 0x0000BEDE File Offset: 0x0000A2DE
		public void RestartShake()
		{
			this.MoveToNextPosition();
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000BEE8 File Offset: 0x0000A2E8
		private void MoveToNextPosition()
		{
			if (!this.m_IsShake)
			{
				return;
			}
			this.m_MoveCount++;
			Vector3 zero = Vector3.zero;
			for (int i = 0; i < this.m_ShakeData.Length; i++)
			{
				float num = 0f;
				ADVShake.eShakeType type = this.m_ShakeData[i].m_Type;
				if (type != ADVShake.eShakeType.None)
				{
					if (type != ADVShake.eShakeType.Round)
					{
						if (type == ADVShake.eShakeType.Random)
						{
							if (this.m_IsAllShake)
							{
								if (this.m_Player == null)
								{
									return;
								}
								num = this.m_Player.GetShakeAllRandomParameter(i);
							}
							else if (this.m_IsThisCharaShake)
							{
								if (this.m_Player == null)
								{
									return;
								}
								num = this.m_Player.GetCharaRandomParameter(i);
							}
							else
							{
								num = this.m_ShakeData[i].m_Radius * (UnityEngine.Random.value * 2f - 1f);
							}
						}
					}
					else
					{
						num = this.m_ShakeData[i].m_Radius * (float)((this.m_MoveCount / 2 * 2 - this.m_MoveCount) * 2 + 1);
					}
				}
				else
				{
					num = 0f;
				}
				if (i == 0)
				{
					zero.x = this.m_OrigPos.x + num;
				}
				else
				{
					zero.y = this.m_OrigPos.y + num;
				}
			}
			this.m_NeedMoveToNext = false;
			this.MoveToNextPosition(zero);
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000C068 File Offset: 0x0000A468
		private void MoveToNextPosition(Vector2 pos)
		{
			if (!this.m_IsShake)
			{
				return;
			}
			iTween.Stop(this.m_GameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", pos.x);
			hashtable.Add("y", pos.y);
			hashtable.Add("time", this.m_Cycle);
			hashtable.Add("easeType", iTween.EaseType.linear);
			hashtable.Add("delay", 0.0001f);
			hashtable.Add("oncomplete", "OnComplete");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x0000C140 File Offset: 0x0000A540
		private void OnComplete()
		{
			this.m_NeedMoveToNext = true;
			if (this.m_IsAllShake)
			{
				if (this.m_Player == null)
				{
					return;
				}
				this.m_Player.MoveToNextPosition();
			}
			else if (this.m_IsThisCharaShake)
			{
				if (this.m_Player == null)
				{
					return;
				}
				this.m_Player.MoveToNextPositionChara();
			}
			else
			{
				this.MoveToNextPosition();
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0000C1B4 File Offset: 0x0000A5B4
		private void ReturnToOrig()
		{
			iTween.Stop(this.m_GameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", this.m_OrigPos.x);
			hashtable.Add("y", this.m_OrigPos.y);
			hashtable.Add("time", this.m_Cycle);
			hashtable.Add("easeType", iTween.EaseType.easeInOutCubic);
			hashtable.Add("delay", 0.001f);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000C266 File Offset: 0x0000A666
		public ADVShake.eShakeType GetShakeTypeX()
		{
			return this.m_ShakeData[0].m_Type;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0000C279 File Offset: 0x0000A679
		public ADVShake.eShakeType GetShakeTypeY()
		{
			return this.m_ShakeData[1].m_Type;
		}

		// Token: 0x06000207 RID: 519 RVA: 0x0000C28C File Offset: 0x0000A68C
		public float GetRadiusX()
		{
			return this.m_ShakeData[0].m_Radius;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0000C29F File Offset: 0x0000A69F
		public float GetRadiusY()
		{
			return this.m_ShakeData[1].m_Radius;
		}

		// Token: 0x06000209 RID: 521 RVA: 0x0000C2B2 File Offset: 0x0000A6B2
		public bool IsNeedMoveToNext()
		{
			return this.m_NeedMoveToNext;
		}

		// Token: 0x0600020A RID: 522 RVA: 0x0000C2BA File Offset: 0x0000A6BA
		public void CancelSyncShakeAll()
		{
			this.m_IsAllShake = false;
		}

		// Token: 0x0600020B RID: 523 RVA: 0x0000C2C3 File Offset: 0x0000A6C3
		public void StopShake()
		{
			this.m_IsShake = false;
			this.m_NeedMoveToNext = false;
			this.m_ShakeTimeCount = 0f;
			this.ReturnToOrig();
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0000C2E4 File Offset: 0x0000A6E4
		public void UpdateTransform()
		{
			this.m_GameObject = base.gameObject;
			this.m_Transform = base.transform;
			this.m_OrigPos = this.m_Transform.localPosition;
		}

		// Token: 0x04000135 RID: 309
		private Vector3 m_OrigPos = Vector3.zero;

		// Token: 0x04000136 RID: 310
		private bool m_IsShake;

		// Token: 0x04000137 RID: 311
		private ADVShake.ShakeData[] m_ShakeData = new ADVShake.ShakeData[2];

		// Token: 0x04000138 RID: 312
		private float m_ShakeTimeCount;

		// Token: 0x04000139 RID: 313
		private float m_ShakeMaxTime;

		// Token: 0x0400013A RID: 314
		private float m_Cycle;

		// Token: 0x0400013B RID: 315
		private int m_MoveCount;

		// Token: 0x0400013C RID: 316
		private GameObject m_GameObject;

		// Token: 0x0400013D RID: 317
		private Transform m_Transform;

		// Token: 0x0400013E RID: 318
		private ADVPlayer m_Player;

		// Token: 0x0400013F RID: 319
		private bool m_IsAllShake;

		// Token: 0x04000140 RID: 320
		private bool m_IsThisCharaShake;

		// Token: 0x04000141 RID: 321
		private bool m_NeedMoveToNext;

		// Token: 0x0200002C RID: 44
		private struct ShakeData
		{
			// Token: 0x04000142 RID: 322
			public ADVShake.eShakeType m_Type;

			// Token: 0x04000143 RID: 323
			public float m_Radius;
		}

		// Token: 0x0200002D RID: 45
		public enum eShakeType
		{
			// Token: 0x04000145 RID: 325
			None,
			// Token: 0x04000146 RID: 326
			Round,
			// Token: 0x04000147 RID: 327
			Random
		}
	}
}
