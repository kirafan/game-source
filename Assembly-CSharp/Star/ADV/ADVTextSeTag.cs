﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000044 RID: 68
	public class ADVTextSeTag : ADVTextSeTagBase
	{
		// Token: 0x060002A0 RID: 672 RVA: 0x0000F666 File Offset: 0x0000DA66
		public ADVTextSeTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000F674 File Offset: 0x0000DA74
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int length = tag.IndexOf(">") - num;
				string cueName = tag.Substring(num, length);
				this.m_Player.SoundManager.PlaySE(cueName);
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
