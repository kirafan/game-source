﻿using System;

namespace Star.ADV
{
	// Token: 0x02000051 RID: 81
	public class ADVTextWaitMotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002B7 RID: 695 RVA: 0x0000FB08 File Offset: 0x0000DF08
		protected ADVTextWaitMotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_wait_motion, isEndTag)
		{
		}
	}
}
