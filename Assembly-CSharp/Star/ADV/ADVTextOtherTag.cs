﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000064 RID: 100
	public class ADVTextOtherTag : ADVTextStandardRangeTagBase
	{
		// Token: 0x060002D6 RID: 726 RVA: 0x000100A5 File Offset: 0x0000E4A5
		public ADVTextOtherTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag) : base(player, parentParser, eventType, (!isEndTag) ? ADVTextTag.eTagType.RangeBegin : ADVTextTag.eTagType.RangeEnd)
		{
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x000100C0 File Offset: 0x0000E4C0
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (!this.m_IsEndTag)
			{
				evList.Add(this.m_EventType);
			}
			return result;
		}
	}
}
