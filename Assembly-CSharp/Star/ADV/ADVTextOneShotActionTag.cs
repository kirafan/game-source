﻿using System;

namespace Star.ADV
{
	// Token: 0x02000039 RID: 57
	public class ADVTextOneShotActionTag : ADVTextTag
	{
		// Token: 0x0600028D RID: 653 RVA: 0x0000F429 File Offset: 0x0000D829
		protected ADVTextOneShotActionTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isActiveExecTab) : base(player, parentParser, eventType, ADVTextTag.eTagType.OneShotAction)
		{
		}
	}
}
