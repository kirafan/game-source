﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200003D RID: 61
	public class ADVTextStrongTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06000293 RID: 659 RVA: 0x0000F494 File Offset: 0x0000D894
		protected ADVTextStrongTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_strong, isEndTag)
		{
		}

		// Token: 0x06000294 RID: 660 RVA: 0x0000F4A1 File Offset: 0x0000D8A1
		public override bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return true;
		}

		// Token: 0x06000295 RID: 661 RVA: 0x0000F4A4 File Offset: 0x0000D8A4
		public override void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
		}
	}
}
