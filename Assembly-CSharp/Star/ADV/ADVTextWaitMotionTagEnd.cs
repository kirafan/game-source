﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000053 RID: 83
	public class ADVTextWaitMotionTagEnd : ADVTextWaitMotionTagBase
	{
		// Token: 0x060002BA RID: 698 RVA: 0x0000FB5E File Offset: 0x0000DF5E
		public ADVTextWaitMotionTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0000FB69 File Offset: 0x0000DF69
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
