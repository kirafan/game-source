﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000032 RID: 50
	public class ADVStandPic : MonoBehaviour
	{
		// Token: 0x06000241 RID: 577 RVA: 0x0000D648 File Offset: 0x0000BA48
		public void ChangeMaterial(Material mat)
		{
			if (this.m_BodyImage != null)
			{
				this.m_BodyImage.material = mat;
			}
			if (this.m_FaceImage != null)
			{
				this.m_FaceImage.material = mat;
			}
		}

		// Token: 0x06000242 RID: 578 RVA: 0x0000D684 File Offset: 0x0000BA84
		public void SetHDRFactor(float hdrFactor, float sec)
		{
			if (sec <= 0f)
			{
				this.m_CurrentHDRFactor = hdrFactor;
				this.SetHDRFactor(hdrFactor);
			}
			else
			{
				this.m_GoalHDRFactor = hdrFactor;
				this.m_HDRFactorSpeed = (this.m_GoalHDRFactor - this.m_CurrentHDRFactor) / sec;
			}
		}

		// Token: 0x06000243 RID: 579 RVA: 0x0000D6C0 File Offset: 0x0000BAC0
		public void SetHDRFactor(float hdrFactor)
		{
			if (this.m_BodyImage != null && this.m_BodyImage.material != null)
			{
				this.m_BodyImage.material.SetFloat("_HDRFactor", hdrFactor);
			}
			if (this.m_FaceImage != null && this.m_BodyImage.material != null)
			{
				this.m_FaceImage.material.SetFloat("_HDRFactor", hdrFactor);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000244 RID: 580 RVA: 0x0000D747 File Offset: 0x0000BB47
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000245 RID: 581 RVA: 0x0000D74F File Offset: 0x0000BB4F
		public GameObject CacheGameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000246 RID: 582 RVA: 0x0000D757 File Offset: 0x0000BB57
		// (set) Token: 0x06000247 RID: 583 RVA: 0x0000D75F File Offset: 0x0000BB5F
		public string ADVCharaID
		{
			get
			{
				return this.m_ADVCharaID;
			}
			set
			{
				this.m_ADVCharaID = value;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000248 RID: 584 RVA: 0x0000D768 File Offset: 0x0000BB68
		public ADVStandPic.eState State
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000249 RID: 585 RVA: 0x0000D770 File Offset: 0x0000BB70
		public int CurrentPoseIdx
		{
			get
			{
				return this.m_CurrentPoseIdx;
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600024A RID: 586 RVA: 0x0000D778 File Offset: 0x0000BB78
		public bool IsPlayingMotion
		{
			get
			{
				return this.m_MotionID != "なし";
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600024B RID: 587 RVA: 0x0000D78A File Offset: 0x0000BB8A
		public bool IsMoving
		{
			get
			{
				return this.m_IsMoving;
			}
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0000D794 File Offset: 0x0000BB94
		public float GetBodySpriteWidth()
		{
			if (this.m_BodyImage.sprite == null)
			{
				return 1f;
			}
			return this.m_BodyImage.sprite.rect.width;
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0000D7D8 File Offset: 0x0000BBD8
		public float GetBodySpriteHeight()
		{
			if (this.m_BodyImage.sprite == null)
			{
				return 1f;
			}
			return this.m_BodyImage.sprite.rect.height;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000D81C File Offset: 0x0000BC1C
		public void Setup(StandPicManager manager)
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_RectTransform.localScale = Vector3.one;
			this.m_OwnerManager = manager;
			if (this.m_FaceImage != null)
			{
				this.m_FaceImage.gameObject.SetActive(false);
			}
			this.m_PoseSpriteHandlers = new SpriteHandler[manager.Player.GetADVCharacterParam(this.m_ADVCharaID).m_Datas.Length];
		}

		// Token: 0x0600024F RID: 591 RVA: 0x0000D8A0 File Offset: 0x0000BCA0
		public bool IsAvailable()
		{
			for (int i = 0; i < this.m_PoseSpriteHandlers.Length; i++)
			{
				if (this.m_PoseSpriteHandlers[i] != null && !this.m_PoseSpriteHandlers[i].IsAvailable())
				{
					return false;
				}
			}
			for (int j = 0; j < this.m_FacePatternList.Count; j++)
			{
				if (!this.m_FacePatternList[j].IsAvailable())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000250 RID: 592 RVA: 0x0000D91C File Offset: 0x0000BD1C
		public void UnloadSprites()
		{
			this.m_BodyImage.sprite = null;
			this.m_FaceImage.sprite = null;
			for (int i = 0; i < this.m_PoseSpriteHandlers.Length; i++)
			{
				if (this.m_PoseSpriteHandlers[i] != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_PoseSpriteHandlers[i]);
					this.m_PoseSpriteHandlers[i] = null;
				}
			}
			for (int j = 0; j < this.m_FacePatternList.Count; j++)
			{
				if (this.m_FacePatternList[j] != null)
				{
					this.m_FacePatternList[j].UnLoadAll();
					this.m_FacePatternList[j] = null;
				}
			}
		}

		// Token: 0x06000251 RID: 593 RVA: 0x0000D9D4 File Offset: 0x0000BDD4
		public int GetLoadedPoseNum()
		{
			int num = 0;
			for (int i = 0; i < this.m_PoseSpriteHandlers.Length; i++)
			{
				if (this.m_PoseSpriteHandlers[i] != null)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06000252 RID: 594 RVA: 0x0000DA10 File Offset: 0x0000BE10
		public void LoadPose(string poseName)
		{
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			int num = -1;
			if (string.IsNullOrEmpty(poseName))
			{
				num = 0;
			}
			else
			{
				for (int i = 0; i < advcharacterParam.m_Datas.Length; i++)
				{
					if (advcharacterParam.m_Datas[i].m_PoseName == poseName)
					{
						num = i;
						break;
					}
				}
			}
			if (num != -1)
			{
				this.LoadPose(num);
			}
		}

		// Token: 0x06000253 RID: 595 RVA: 0x0000DA9C File Offset: 0x0000BE9C
		public void LoadPose(int idx)
		{
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			if ((int)advcharacterParam.m_Datas[idx].m_DummyStandPic == 0)
			{
				if (this.m_PoseSpriteHandlers[idx] != null)
				{
					return;
				}
				this.m_PoseSpriteHandlers[idx] = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVStandPic(advcharacterParam.m_ResourceBaseName, advcharacterParam.m_Datas[idx].m_PoseID);
			}
			int facePattern = advcharacterParam.m_Datas[idx].m_FacePattern;
			if (facePattern != -1)
			{
				for (int i = 0; i < this.m_FacePatternList.Count; i++)
				{
					if (this.m_FacePatternList[i].GetPatternID() == facePattern)
					{
						return;
					}
				}
				ADVStandPic.FacePatternData facePatternData = new ADVStandPic.FacePatternData();
				facePatternData.Load(advcharacterParam.m_ResourceBaseName, facePattern);
				this.m_FacePatternList.Add(facePatternData);
			}
		}

		// Token: 0x06000254 RID: 596 RVA: 0x0000DB8C File Offset: 0x0000BF8C
		public void UnloadPose(string poseName)
		{
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			int num = -1;
			int num2 = -1;
			if (string.IsNullOrEmpty(poseName))
			{
				num = 0;
			}
			else
			{
				for (int i = 0; i < advcharacterParam.m_Datas.Length; i++)
				{
					if (advcharacterParam.m_Datas[i].m_PoseName == poseName)
					{
						num = i;
						break;
					}
				}
			}
			if (num != -1)
			{
				if (this.m_PoseSpriteHandlers[num] == null)
				{
					return;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_PoseSpriteHandlers[num]);
				this.m_PoseSpriteHandlers[num] = null;
				num2 = advcharacterParam.m_Datas[num].m_FacePattern;
			}
			if (num2 != -1)
			{
				List<ADVStandPic.FacePatternData> list = new List<ADVStandPic.FacePatternData>();
				for (int j = 0; j < this.m_FacePatternList.Count; j++)
				{
					if (this.m_FacePatternList[j].GetPatternID() == num2)
					{
						this.m_FacePatternList[j].UnLoadAll();
						list.Add(this.m_FacePatternList[j]);
					}
				}
				for (int k = 0; k < list.Count; k++)
				{
					this.m_FacePatternList.Remove(list[k]);
				}
			}
		}

		// Token: 0x06000255 RID: 597 RVA: 0x0000DCF0 File Offset: 0x0000C0F0
		public void ApplyPose(string poseName)
		{
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			int poseIdx = -1;
			for (int i = 0; i < advcharacterParam.m_Datas.Length; i++)
			{
				if (advcharacterParam.m_Datas[i].m_PoseName == poseName)
				{
					poseIdx = i;
					break;
				}
			}
			this.ApplyPose(poseIdx);
		}

		// Token: 0x06000256 RID: 598 RVA: 0x0000DD5B File Offset: 0x0000C15B
		public void ApplyPose(int poseIdx)
		{
			if (this.m_CurrentPoseIdx == poseIdx)
			{
				return;
			}
			this.m_NextPoseIdx = poseIdx;
			this.UpdateApplyPose();
		}

		// Token: 0x06000257 RID: 599 RVA: 0x0000DD78 File Offset: 0x0000C178
		public void ApplyFace(eADVFace face)
		{
			if (this.m_FaceImage == null)
			{
				return;
			}
			if (this.m_CurrentFacePatternData == null)
			{
				return;
			}
			this.m_FaceImage.sprite = this.m_CurrentFacePatternData.GetFaceSprite(face);
		}

		// Token: 0x06000258 RID: 600 RVA: 0x0000DDB0 File Offset: 0x0000C1B0
		public bool UpdateApplyPose()
		{
			if (this.m_NextPoseIdx == -1)
			{
				return true;
			}
			if (!this.IsAvailable())
			{
				return false;
			}
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			if (this.m_CurrentPoseIdx != -1)
			{
				this.m_OwnerManager.UnUse(this.m_ADVCharaID, advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_PoseName);
			}
			this.m_CurrentPoseIdx = -1;
			SpriteHandler spriteHandler = this.m_PoseSpriteHandlers[this.m_NextPoseIdx];
			if ((int)advcharacterParam.m_Datas[this.m_NextPoseIdx].m_DummyStandPic != 0)
			{
				this.m_BodyImage.sprite = null;
			}
			else
			{
				if (spriteHandler == null)
				{
					Debug.LogWarning(string.Concat(new string[]
					{
						"ポーズの読み込みが始まっていません charaid = ",
						advcharacterParam.m_ADVCharaID,
						" ポーズID：",
						this.m_NextPoseIdx.ToString(),
						" 読み込みを開始します(ロード待ちが発生します)"
					}));
					this.m_OwnerManager.ForceLoadPic(this.m_ADVCharaID, advcharacterParam.m_Datas[this.m_NextPoseIdx].m_PoseName);
					return false;
				}
				if (this.m_PoseSpriteHandlers[this.m_NextPoseIdx].Obj == null)
				{
					Debug.LogWarning("ロード直後です\u3000読み込みを待ちます(ロード待ちが発生します)");
					return false;
				}
				this.m_BodyImage.sprite = this.m_PoseSpriteHandlers[this.m_NextPoseIdx].Obj;
			}
			this.m_CurrentPoseIdx = this.m_NextPoseIdx;
			RectTransform rectTransform = this.m_BodyImage.rectTransform;
			rectTransform.anchoredPosition = new Vector2(advcharacterParam.m_Datas[this.m_NextPoseIdx].m_OffsetX, advcharacterParam.m_Datas[this.m_NextPoseIdx].m_OffsetY);
			this.m_RectTransform.sizeDelta = new Vector2(this.GetBodySpriteWidth(), this.GetBodySpriteHeight() + advcharacterParam.m_Datas[this.m_NextPoseIdx].m_OffsetY);
			int facePattern = advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePattern;
			if (facePattern != -1)
			{
				for (int i = 0; i < this.m_FacePatternList.Count; i++)
				{
					if (facePattern == this.m_FacePatternList[i].GetPatternID())
					{
						this.m_CurrentFacePatternData = this.m_FacePatternList[i];
					}
				}
				if (this.m_FaceImage != null)
				{
					this.m_FaceImage.gameObject.SetActive(true);
					LayoutRebuilder.ForceRebuildLayoutImmediate(rectTransform);
					this.ApplyFace(eADVFace.Default);
					if (this.m_FaceImage.sprite != null)
					{
						this.m_FaceImage.rectTransform.pivot = new Vector2(advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePivotX, advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePivotY);
						if (this.GetBodySpriteWidth() < this.m_FaceImage.sprite.rect.width)
						{
							this.m_RectTransform.sizeDelta = new Vector2(this.m_FaceImage.sprite.rect.width, this.m_FaceImage.sprite.rect.height + advcharacterParam.m_Datas[this.m_NextPoseIdx].m_OffsetY * (1f - advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePivotY));
							this.m_FaceImage.rectTransform.localPosX(-this.GetBodySpriteWidth() * 0.5f + advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePicX);
							this.m_FaceImage.rectTransform.localPosY(this.m_BodyImage.GetComponent<RectTransform>().localPosition.y + (this.GetBodySpriteHeight() * 0.5f - advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePicY));
						}
						else
						{
							this.m_FaceImage.rectTransform.localPosX(-this.m_RectTransform.rect.width * 0.5f + advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePicX);
							this.m_FaceImage.rectTransform.localPosY(this.m_RectTransform.rect.height * 1f - advcharacterParam.m_Datas[this.m_NextPoseIdx].m_FacePicY);
						}
					}
				}
			}
			else
			{
				this.m_FaceImage.gameObject.SetActive(false);
			}
			this.m_NextPoseIdx = -1;
			return true;
		}

		// Token: 0x06000259 RID: 601 RVA: 0x0000E260 File Offset: 0x0000C660
		public bool IsDoneApplyPose()
		{
			return this.m_NextPoseIdx == -1;
		}

		// Token: 0x0600025A RID: 602 RVA: 0x0000E26C File Offset: 0x0000C66C
		private void Update()
		{
			switch (this.m_State)
			{
			case ADVStandPic.eState.In:
				this.m_FadeRate += Time.deltaTime / this.m_FadeDuration;
				if (this.m_FadeRate >= 1f)
				{
					this.m_FadeRate = 1f;
					this.m_State = ADVStandPic.eState.Wait;
				}
				this.UpdateFinalColor();
				break;
			case ADVStandPic.eState.Out:
				this.m_FadeRate -= Time.deltaTime / this.m_FadeDuration;
				if (this.m_FadeRate <= 0f)
				{
					this.m_FadeRate = 0f;
					this.SetEnableRender(false);
				}
				this.UpdateFinalColor();
				break;
			}
			if (this.m_HDRFactorSpeed != 0f)
			{
				float currentHDRFactor = this.m_CurrentHDRFactor;
				this.m_CurrentHDRFactor += this.m_HDRFactorSpeed * Time.deltaTime;
				if ((currentHDRFactor < this.m_GoalHDRFactor && this.m_CurrentHDRFactor > this.m_GoalHDRFactor) || (currentHDRFactor > this.m_GoalHDRFactor && this.m_CurrentHDRFactor < this.m_GoalHDRFactor))
				{
					this.m_CurrentHDRFactor = this.m_GoalHDRFactor;
					this.m_HDRFactorSpeed = 0f;
				}
				this.SetHDRFactor(this.m_CurrentHDRFactor);
			}
		}

		// Token: 0x0600025B RID: 603 RVA: 0x0000E3C0 File Offset: 0x0000C7C0
		public void SetEnableRender(bool flg)
		{
			this.m_GameObject.SetActive(flg);
			if (flg && this.m_State == ADVStandPic.eState.Hide)
			{
				this.ApplyPose(0);
				this.m_State = ADVStandPic.eState.Wait;
			}
			else if (!flg)
			{
				ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
				this.m_State = ADVStandPic.eState.Hide;
				this.m_OwnerManager.RemovePicList(this);
				this.m_OwnerManager.UnUse(this.m_ADVCharaID, advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_PoseName);
				this.m_CurrentPoseIdx = -1;
			}
			this.m_FadeRate = 1f;
			this.UpdateFinalColor();
		}

		// Token: 0x0600025C RID: 604 RVA: 0x0000E46D File Offset: 0x0000C86D
		public bool IsActive()
		{
			return this.m_State == ADVStandPic.eState.In || this.m_State == ADVStandPic.eState.Wait;
		}

		// Token: 0x0600025D RID: 605 RVA: 0x0000E487 File Offset: 0x0000C887
		public void HighlightTalker()
		{
			if (this.m_HighlightState == ADVStandPic.eHighlightState.None)
			{
				this.m_HighlightState = ADVStandPic.eHighlightState.Talker;
				this.UpdateFinalColor();
			}
		}

		// Token: 0x0600025E RID: 606 RVA: 0x0000E4A1 File Offset: 0x0000C8A1
		public void HighlightTalkerReset()
		{
			if (this.m_HighlightState == ADVStandPic.eHighlightState.Talker)
			{
				this.m_HighlightState = ADVStandPic.eHighlightState.None;
				this.UpdateFinalColor();
			}
		}

		// Token: 0x0600025F RID: 607 RVA: 0x0000E4BC File Offset: 0x0000C8BC
		public void Highlight()
		{
			this.m_HighlightState = ADVStandPic.eHighlightState.Command;
			this.UpdateFinalColor();
		}

		// Token: 0x06000260 RID: 608 RVA: 0x0000E4CB File Offset: 0x0000C8CB
		public void HighlightDefault()
		{
			if (this.m_HighlightState != ADVStandPic.eHighlightState.Talker)
			{
				this.m_HighlightState = ADVStandPic.eHighlightState.None;
				this.UpdateFinalColor();
			}
		}

		// Token: 0x06000261 RID: 609 RVA: 0x0000E4E6 File Offset: 0x0000C8E6
		public void Shading()
		{
			this.m_HighlightState = ADVStandPic.eHighlightState.Shader;
			this.UpdateFinalColor();
		}

		// Token: 0x06000262 RID: 610 RVA: 0x0000E4F8 File Offset: 0x0000C8F8
		public void UpdateFinalColor()
		{
			Color white = Color.white;
			switch (this.m_HighlightState)
			{
			case ADVStandPic.eHighlightState.None:
				white = new Color(0.5f, 0.5f, 0.5f, 1f);
				break;
			case ADVStandPic.eHighlightState.Talker:
				white = new Color(1f, 1f, 1f, 1f);
				break;
			case ADVStandPic.eHighlightState.Command:
				white = new Color(1f, 1f, 1f, 1f);
				break;
			case ADVStandPic.eHighlightState.Shader:
				white = new Color(0.5f, 0.5f, 0.5f, 1f);
				break;
			}
			white.a = white.a * this.m_FadeRate * this.m_AlphaRate;
			this.SetColor(white);
		}

		// Token: 0x06000263 RID: 611 RVA: 0x0000E5D0 File Offset: 0x0000C9D0
		public void SetPriorityState(ADVStandPic.ePriorityState state)
		{
			this.m_PriorityStateState = state;
		}

		// Token: 0x06000264 RID: 612 RVA: 0x0000E5D9 File Offset: 0x0000C9D9
		public void SetColor(Color color)
		{
			this.m_BodyImage.color = color;
			if (this.m_FaceImage != null)
			{
				this.m_FaceImage.color = color;
			}
		}

		// Token: 0x06000265 RID: 613 RVA: 0x0000E604 File Offset: 0x0000CA04
		public Color GetColor()
		{
			return this.m_BodyImage.color;
		}

		// Token: 0x06000266 RID: 614 RVA: 0x0000E611 File Offset: 0x0000CA11
		public void SetAnchoredPosition(Vector2 position)
		{
			this.StopMotion();
			this.m_RectTransform.anchoredPosition = position;
			this.m_OrigPosition = this.m_RectTransform.localPosition;
		}

		// Token: 0x06000267 RID: 615 RVA: 0x0000E63B File Offset: 0x0000CA3B
		public void SetPositionZ(float z)
		{
			this.m_FaceImage.GetComponent<RectTransform>().localPosZ(z - 1f);
			this.m_BodyImage.GetComponent<RectTransform>().localPosZ(z);
		}

		// Token: 0x06000268 RID: 616 RVA: 0x0000E665 File Offset: 0x0000CA65
		public ADVStandPic.ePriorityState GetPriorityState()
		{
			return this.m_PriorityStateState;
		}

		// Token: 0x06000269 RID: 617 RVA: 0x0000E670 File Offset: 0x0000CA70
		public Vector2 GetCharaPosition()
		{
			Vector2 origPosition = this.m_OrigPosition;
			origPosition.y += this.m_RectTransform.rect.height * 0.5f;
			return origPosition;
		}

		// Token: 0x0600026A RID: 618 RVA: 0x0000E6AC File Offset: 0x0000CAAC
		public Vector2 GetCharaFacePosition()
		{
			Vector2 origPosition = this.m_OrigPosition;
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			eADVCharaListPicType faceReferenceImageType = (eADVCharaListPicType)advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FaceReferenceImageType;
			if (faceReferenceImageType != eADVCharaListPicType.StandPic)
			{
				if (faceReferenceImageType == eADVCharaListPicType.Face)
				{
					origPosition.x -= this.m_RectTransform.rect.width * 0.5f;
					origPosition.y += this.m_RectTransform.rect.height;
					origPosition.x += this.m_FaceImage.rectTransform.anchoredPosition.x;
					origPosition.y += this.m_FaceImage.rectTransform.anchoredPosition.y;
					origPosition.x -= this.m_FaceImage.sprite.rect.width * advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FacePivotX;
					origPosition.y += this.m_FaceImage.sprite.rect.height * (1f - advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FacePivotY);
				}
			}
			else
			{
				origPosition.x += advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_OffsetX;
				origPosition.y += advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_OffsetY;
				origPosition.x -= this.GetBodySpriteWidth() * 0.5f;
				origPosition.y += this.GetBodySpriteHeight();
			}
			origPosition.x += advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FaceX;
			origPosition.y -= advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FaceY;
			return origPosition;
		}

		// Token: 0x0600026B RID: 619 RVA: 0x0000E8EC File Offset: 0x0000CCEC
		public Vector2 GetCharaFootPosition()
		{
			Vector2 origPosition = this.m_OrigPosition;
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			origPosition.x += advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FootOffsetX;
			origPosition.y += advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_FootOffsetY;
			return origPosition;
		}

		// Token: 0x0600026C RID: 620 RVA: 0x0000E964 File Offset: 0x0000CD64
		public Vector2 GetEmoOffset()
		{
			ADVCharacterListDB_Param advcharacterParam = this.m_OwnerManager.Player.GetADVCharacterParam(this.m_ADVCharaID);
			return new Vector2(advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_EmoOffsetX, advcharacterParam.m_Datas[this.m_CurrentPoseIdx].m_EmoOffsetY);
		}

		// Token: 0x0600026D RID: 621 RVA: 0x0000E9BB File Offset: 0x0000CDBB
		public bool IsFading()
		{
			return this.m_State == ADVStandPic.eState.In || this.m_State == ADVStandPic.eState.Out;
		}

		// Token: 0x0600026E RID: 622 RVA: 0x0000E9D5 File Offset: 0x0000CDD5
		public void FadeIn(float sec)
		{
			if (this.IsActive())
			{
				return;
			}
			this.ApplyPose(0);
			this.m_FadeRate = 0f;
			this.m_FadeDuration = sec;
			this.m_GameObject.SetActive(true);
			this.m_State = ADVStandPic.eState.In;
			this.UpdateFinalColor();
		}

		// Token: 0x0600026F RID: 623 RVA: 0x0000EA15 File Offset: 0x0000CE15
		public void FadeOut(float sec)
		{
			if (!this.IsActive())
			{
				return;
			}
			this.m_FadeRate = 1f;
			this.m_FadeDuration = sec;
			this.m_State = ADVStandPic.eState.Out;
			this.UpdateFinalColor();
		}

		// Token: 0x06000270 RID: 624 RVA: 0x0000EA42 File Offset: 0x0000CE42
		public void PlayMotion(string motion)
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			this.StopMotion();
			this.m_MotionID = motion;
			this.m_MotActionIdx = 0;
			this.DoMotAction();
		}

		// Token: 0x06000271 RID: 625 RVA: 0x0000EA6C File Offset: 0x0000CE6C
		public void StopMotion()
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			iTween.Stop(this.m_GameObject);
			iTween.Init(this.m_GameObject);
			this.m_RectTransform.localPosition = new Vector3(this.m_OrigPosition.x, this.m_OrigPosition.y, this.m_RectTransform.localPosition.z);
			this.m_RectTransform.localScale = Vector3.one;
			this.m_MotionID = "なし";
			this.m_MotActionIdx = 0;
			this.m_IsMoving = false;
		}

		// Token: 0x06000272 RID: 626 RVA: 0x0000EAFD File Offset: 0x0000CEFD
		public void StopAlpha()
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			iTween.Stop(this.m_GameObject, "value");
			this.m_AlphaRate = this.m_OrigAlpha;
		}

		// Token: 0x06000273 RID: 627 RVA: 0x0000EB28 File Offset: 0x0000CF28
		public void DoMotAction()
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			ADVMotionListDB_Param advmotionParam = this.m_OwnerManager.Player.GetADVMotionParam(this.m_MotionID);
			if (this.m_MotActionIdx >= advmotionParam.m_Datas.Length)
			{
				this.StopMotion();
				return;
			}
			while (this.m_MotActionIdx < advmotionParam.m_Datas.Length)
			{
				ADVMotionListDB_Data advmotionListDB_Data = advmotionParam.m_Datas[this.m_MotActionIdx];
				eADVMotAction action = (eADVMotAction)advmotionListDB_Data.m_Action;
				if (action != eADVMotAction.Move)
				{
					if (action == eADVMotAction.Scale)
					{
						Hashtable hashtable = new Hashtable();
						hashtable.Add("isLocal", true);
						hashtable.Add("x", advmotionListDB_Data.m_X);
						hashtable.Add("y", advmotionListDB_Data.m_Y);
						hashtable.Add("time", advmotionListDB_Data.m_Duration);
						hashtable.Add("delay", 0.0001f);
						hashtable.Add("easeType", this.GetEaseType((eADVCurveType)advmotionListDB_Data.m_EaseType));
						if ((int)advmotionListDB_Data.m_WaitEnd != 0 || this.m_MotActionIdx >= advmotionParam.m_Datas.Length - 1)
						{
							hashtable.Add("oncomplete", "OnCompleteMotAction");
							hashtable.Add("oncompletetarget", this.m_GameObject);
						}
						iTween.ScaleTo(this.m_GameObject, hashtable);
					}
				}
				else
				{
					Hashtable hashtable2 = new Hashtable();
					hashtable2.Add("isLocal", true);
					hashtable2.Add("x", this.m_OrigPosition.x + advmotionListDB_Data.m_X);
					hashtable2.Add("y", this.m_OrigPosition.y + advmotionListDB_Data.m_Y);
					hashtable2.Add("z", this.m_RectTransform.localPosition.z);
					hashtable2.Add("time", advmotionListDB_Data.m_Duration);
					hashtable2.Add("delay", 0.0001f);
					hashtable2.Add("easeType", this.GetEaseType((eADVCurveType)advmotionListDB_Data.m_EaseType));
					if ((int)advmotionListDB_Data.m_WaitEnd != 0 || this.m_MotActionIdx >= advmotionParam.m_Datas.Length - 1)
					{
						hashtable2.Add("oncomplete", "OnCompleteMotAction");
						hashtable2.Add("oncompletetarget", this.m_GameObject);
					}
					iTween.MoveTo(this.m_GameObject, hashtable2);
				}
				this.m_MotActionIdx++;
				if ((int)advmotionListDB_Data.m_WaitEnd != 0)
				{
					return;
				}
			}
		}

		// Token: 0x06000274 RID: 628 RVA: 0x0000EDE7 File Offset: 0x0000D1E7
		public iTween.EaseType GetEaseType(eADVCurveType easeType)
		{
			switch (easeType)
			{
			case eADVCurveType.Linear:
				return iTween.EaseType.linear;
			case eADVCurveType.InQuad:
				return iTween.EaseType.easeInQuad;
			case eADVCurveType.OutQuad:
				return iTween.EaseType.easeOutQuad;
			case eADVCurveType.InOutQuad:
				return iTween.EaseType.easeInOutQuad;
			case eADVCurveType.InCubic:
				return iTween.EaseType.easeInCubic;
			case eADVCurveType.OutCubic:
				return iTween.EaseType.easeOutCubic;
			case eADVCurveType.InOutCubic:
				return iTween.EaseType.easeInOutCubic;
			default:
				return iTween.EaseType.linear;
			}
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000EE21 File Offset: 0x0000D221
		public void OnCompleteMotAction()
		{
			iTween.Stop(this.m_GameObject);
			this.DoMotAction();
		}

		// Token: 0x06000276 RID: 630 RVA: 0x0000EE34 File Offset: 0x0000D234
		public void SetPosition(Vector2 pos)
		{
			this.m_OrigPosition = pos;
			this.StopMotion();
		}

		// Token: 0x06000277 RID: 631 RVA: 0x0000EE44 File Offset: 0x0000D244
		public void MoveTo(Vector2 pos, float duration, eADVCurveType easeType, bool isCharaOut)
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			this.StopMotion();
			this.m_OrigPosition = pos;
			this.m_isCharaOutMove = isCharaOut;
			this.m_IsMoving = true;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", this.m_OrigPosition.x);
			hashtable.Add("y", this.m_OrigPosition.y);
			hashtable.Add("z", this.m_RectTransform.localPosition.z);
			hashtable.Add("time", duration);
			hashtable.Add("delay", 0.1f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(easeType));
			hashtable.Add("oncomplete", "OnCompleteMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000278 RID: 632 RVA: 0x0000EF54 File Offset: 0x0000D354
		public void Move(Vector2 pos, float duration, eADVCurveType easeType, bool isCharaOut)
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			this.StopMotion();
			this.m_isCharaOutMove = isCharaOut;
			this.m_OrigPosition += pos;
			this.m_IsMoving = true;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", this.m_OrigPosition.x);
			hashtable.Add("y", this.m_OrigPosition.y);
			hashtable.Add("z", this.m_RectTransform.localPosition.z);
			hashtable.Add("time", duration);
			hashtable.Add("delay", 0.1f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(easeType));
			hashtable.Add("oncomplete", "OnCompleteMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000279 RID: 633 RVA: 0x0000F070 File Offset: 0x0000D470
		public void AlphaTo(uint alpha, float duration, eADVCurveType easeType)
		{
			if (this.m_State == ADVStandPic.eState.Hide)
			{
				return;
			}
			this.StopAlpha();
			this.m_OrigAlpha = alpha / 100f;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", this.m_AlphaRate);
			hashtable.Add("to", this.m_OrigAlpha);
			hashtable.Add("time", duration);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(easeType));
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "OnCompleteInAlpha");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x0600027A RID: 634 RVA: 0x0000F15A File Offset: 0x0000D55A
		public void OnUpdateAlpha(float value)
		{
			this.m_AlphaRate = value;
			this.UpdateFinalColor();
		}

		// Token: 0x0600027B RID: 635 RVA: 0x0000F169 File Offset: 0x0000D569
		public void OnCompleteMove()
		{
			this.m_IsMoving = false;
			if (this.m_isCharaOutMove)
			{
				this.SetEnableRender(false);
			}
		}

		// Token: 0x0600027C RID: 636 RVA: 0x0000F184 File Offset: 0x0000D584
		public void OnCompleteInAlpha()
		{
			this.UpdateFinalColor();
		}

		// Token: 0x0400015D RID: 349
		private string m_ADVCharaID;

		// Token: 0x0400015E RID: 350
		[SerializeField]
		private Image m_BodyImage;

		// Token: 0x0400015F RID: 351
		[SerializeField]
		private Image m_FaceImage;

		// Token: 0x04000160 RID: 352
		private SpriteHandler[] m_PoseSpriteHandlers;

		// Token: 0x04000161 RID: 353
		private ADVStandPic.eHighlightState m_HighlightState;

		// Token: 0x04000162 RID: 354
		private ADVStandPic.ePriorityState m_PriorityStateState = ADVStandPic.ePriorityState.Normal;

		// Token: 0x04000163 RID: 355
		private const string SHADER_PROPERTY_NAME_HDRFACTOR = "_HDRFactor";

		// Token: 0x04000164 RID: 356
		private float m_CurrentHDRFactor = 1f;

		// Token: 0x04000165 RID: 357
		private float m_GoalHDRFactor = 1f;

		// Token: 0x04000166 RID: 358
		private float m_HDRFactorSpeed;

		// Token: 0x04000167 RID: 359
		private List<ADVStandPic.FacePatternData> m_FacePatternList = new List<ADVStandPic.FacePatternData>();

		// Token: 0x04000168 RID: 360
		private ADVStandPic.FacePatternData m_CurrentFacePatternData;

		// Token: 0x04000169 RID: 361
		private ADVStandPic.eState m_State;

		// Token: 0x0400016A RID: 362
		private int m_CurrentPoseIdx = -1;

		// Token: 0x0400016B RID: 363
		private int m_NextPoseIdx = -1;

		// Token: 0x0400016C RID: 364
		private string m_MotionID = "なし";

		// Token: 0x0400016D RID: 365
		private int m_MotActionIdx;

		// Token: 0x0400016E RID: 366
		private Vector2 m_OrigPosition = Vector2.zero;

		// Token: 0x0400016F RID: 367
		private float m_OrigAlpha = 1f;

		// Token: 0x04000170 RID: 368
		private bool m_IsMoving;

		// Token: 0x04000171 RID: 369
		private float m_FadeDuration;

		// Token: 0x04000172 RID: 370
		private float m_FadeRate = 1f;

		// Token: 0x04000173 RID: 371
		private float m_AlphaRate = 1f;

		// Token: 0x04000174 RID: 372
		private StandPicManager m_OwnerManager;

		// Token: 0x04000175 RID: 373
		private RectTransform m_RectTransform;

		// Token: 0x04000176 RID: 374
		private GameObject m_GameObject;

		// Token: 0x04000177 RID: 375
		private bool m_isCharaOutMove;

		// Token: 0x02000033 RID: 51
		public enum eHighlightState
		{
			// Token: 0x04000179 RID: 377
			None,
			// Token: 0x0400017A RID: 378
			Talker,
			// Token: 0x0400017B RID: 379
			Command,
			// Token: 0x0400017C RID: 380
			Shader
		}

		// Token: 0x02000034 RID: 52
		public enum ePriorityState
		{
			// Token: 0x0400017E RID: 382
			Error,
			// Token: 0x0400017F RID: 383
			Normal,
			// Token: 0x04000180 RID: 384
			TopSet,
			// Token: 0x04000181 RID: 385
			BottomSet
		}

		// Token: 0x02000035 RID: 53
		private class FacePatternData
		{
			// Token: 0x0600027E RID: 638 RVA: 0x0000F1A4 File Offset: 0x0000D5A4
			public void Load(string resourceBaseName, int patternID)
			{
				this.m_PatternID = patternID;
				for (int i = 0; i < 10; i++)
				{
					if (this.m_SpriteHandlers[i] != null)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandlers[i]);
					}
					SpriteHandler[] spriteHandlers = this.m_SpriteHandlers;
					int num = i;
					SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
					eADVFace eADVFace = (eADVFace)i;
					spriteHandlers[num] = spriteMng.LoadAsyncADVFace(resourceBaseName, patternID, eADVFace.ToString());
				}
			}

			// Token: 0x0600027F RID: 639 RVA: 0x0000F218 File Offset: 0x0000D618
			public void UnLoadAll()
			{
				for (int i = 0; i < 10; i++)
				{
					if (this.m_SpriteHandlers[i] != null)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandlers[i]);
					}
				}
			}

			// Token: 0x06000280 RID: 640 RVA: 0x0000F25C File Offset: 0x0000D65C
			public bool IsAvailable()
			{
				for (int i = 0; i < this.m_SpriteHandlers.Length; i++)
				{
					if (this.m_SpriteHandlers[i] == null)
					{
						if (!this.m_SpriteHandlers[i].IsAvailable())
						{
							return false;
						}
					}
				}
				return true;
			}

			// Token: 0x06000281 RID: 641 RVA: 0x0000F2A9 File Offset: 0x0000D6A9
			public int GetPatternID()
			{
				return this.m_PatternID;
			}

			// Token: 0x06000282 RID: 642 RVA: 0x0000F2B1 File Offset: 0x0000D6B1
			public Sprite GetFaceSprite(eADVFace face)
			{
				return this.m_SpriteHandlers[(int)face].Obj;
			}

			// Token: 0x04000182 RID: 386
			private int m_PatternID;

			// Token: 0x04000183 RID: 387
			private SpriteHandler[] m_SpriteHandlers = new SpriteHandler[10];
		}

		// Token: 0x02000036 RID: 54
		public enum eState
		{
			// Token: 0x04000185 RID: 389
			Hide,
			// Token: 0x04000186 RID: 390
			In,
			// Token: 0x04000187 RID: 391
			Wait,
			// Token: 0x04000188 RID: 392
			Out
		}
	}
}
