﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200004D RID: 77
	public class ADVTextWaitMoveTag : ADVTextRangeTagBase
	{
		// Token: 0x060002B0 RID: 688 RVA: 0x0000F9D3 File Offset: 0x0000DDD3
		public ADVTextWaitMoveTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, ADVParser.EventType.event_move, false)
		{
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000F9E0 File Offset: 0x0000DDE0
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				this.m_Player.WaitCharaMove(base.ParseTag(tag)[0]);
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
