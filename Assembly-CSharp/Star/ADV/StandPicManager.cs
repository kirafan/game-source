﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x02000078 RID: 120
	public class StandPicManager : MonoBehaviour
	{
		// Token: 0x06000380 RID: 896 RVA: 0x00011687 File Offset: 0x0000FA87
		private void OnDestroy()
		{
			this.m_PicMaterialBase = null;
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00011690 File Offset: 0x0000FA90
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000382 RID: 898 RVA: 0x00011698 File Offset: 0x0000FA98
		public bool IsPlayingMotion
		{
			get
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					if (this.m_PicList[i] != null && this.m_PicList[i].IsPlayingMotion)
					{
						return true;
					}
				}
				return false;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000383 RID: 899 RVA: 0x000116F4 File Offset: 0x0000FAF4
		public bool IsMoving
		{
			get
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					if (this.m_PicList[i] != null && this.m_PicList[i].IsMoving)
					{
						return true;
					}
				}
				return false;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000384 RID: 900 RVA: 0x0001174D File Offset: 0x0000FB4D
		public ADVPlayer Player
		{
			get
			{
				return this.m_Player;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000385 RID: 901 RVA: 0x00011755 File Offset: 0x0000FB55
		public RectTransform CanvasRectTransform
		{
			get
			{
				return this.m_CanvasRectTransform;
			}
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0001175D File Offset: 0x0000FB5D
		public void Prepare(ADVPlayer player)
		{
			this.m_Player = player;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_NeedList = new List<StandPicManager.StandPicLoadParam>();
			this.m_InstList = new List<ADVStandPic>();
			this.m_PicList = new List<ADVStandPic>();
		}

		// Token: 0x06000387 RID: 903 RVA: 0x00011793 File Offset: 0x0000FB93
		public void ClearNeedList()
		{
			this.m_NeedList.Clear();
		}

		// Token: 0x06000388 RID: 904 RVA: 0x000117A0 File Offset: 0x0000FBA0
		public void AddNeedList(int startIdx, string advCharaID, string poseName)
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].m_AdvCharaID == advCharaID && this.m_NeedList[i].m_PoseName == poseName)
				{
					this.m_NeedList[i].AddUseIdx(startIdx);
					Debug.Log(string.Concat(new object[]
					{
						"AddNeedList(AddUseIdx): startIdx = ",
						startIdx,
						" : advCharaID = ",
						advCharaID,
						" : poseName = ",
						poseName
					}));
					return;
				}
			}
			Debug.Log(string.Concat(new object[]
			{
				"AddNeedList : startIdx = ",
				startIdx,
				" : advCharaID = ",
				advCharaID,
				" : poseName = ",
				poseName
			}));
			StandPicManager.StandPicLoadParam standPicLoadParam = new StandPicManager.StandPicLoadParam(advCharaID, poseName);
			standPicLoadParam.AddUseIdx(startIdx);
			this.m_NeedList.Add(standPicLoadParam);
		}

		// Token: 0x06000389 RID: 905 RVA: 0x000118A4 File Offset: 0x0000FCA4
		public void DestroyAllNotNeedStandPic()
		{
			Debug.Log("DestroyAllNotNeedStandPic");
			this.HideAllChara();
			List<ADVStandPic> list = new List<ADVStandPic>();
			list.Clear();
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < this.m_NeedList.Count; j++)
				{
					if (this.m_InstList[i].ADVCharaID == this.m_NeedList[j].m_AdvCharaID)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					this.m_InstList[i].UnloadSprites();
					UnityEngine.Object.Destroy(this.m_InstList[i].CacheGameObject);
					list.Add(this.m_InstList[i]);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				this.m_InstList.Remove(list[k]);
				list[k] = null;
			}
			list.Clear();
		}

		// Token: 0x0600038A RID: 906 RVA: 0x000119B8 File Offset: 0x0000FDB8
		private void CreateStandPic(string advCharaID)
		{
			int num = -1;
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].ADVCharaID == advCharaID)
				{
					num = i;
					break;
				}
			}
			if (num == -1)
			{
				Debug.Log("CreateNeedStandPic : " + advCharaID);
				ADVStandPic advstandPic = UnityEngine.Object.Instantiate<ADVStandPic>(this.m_StandPicPrefab, this.m_RectTransform);
				advstandPic.ADVCharaID = advCharaID;
				advstandPic.Setup(this);
				if (this.m_PicMaterialBase != null)
				{
					Material material = UnityEngine.Object.Instantiate<Material>(this.m_PicMaterialBase);
					if (material != null)
					{
						advstandPic.ChangeMaterial(material);
					}
				}
				this.m_InstList.Add(advstandPic);
				num = this.m_InstList.Count - 1;
				for (int j = 0; j < this.m_InstList.Count; j++)
				{
					this.m_InstList[j].SetPositionZ((float)(-(float)this.m_InstList[j].RectTransform.GetSiblingIndex()) * 100f);
				}
			}
		}

		// Token: 0x0600038B RID: 907 RVA: 0x00011AD8 File Offset: 0x0000FED8
		public void CreateAllNeedStandPic()
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				this.CreateStandPic(this.m_NeedList[i].m_AdvCharaID);
			}
		}

		// Token: 0x0600038C RID: 908 RVA: 0x00011B18 File Offset: 0x0000FF18
		public void UnUse(string advCharaID, string poseName)
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].m_AdvCharaID == advCharaID && this.m_NeedList[i].m_PoseName == poseName && this.m_NeedList[i].m_UseCount > 0)
				{
					this.m_NeedList[i].m_UseCount--;
					this.m_NeedList[i].m_UnuseIdx = this.m_Player.GetCurrentCommandIdx();
					Debug.Log(string.Concat(new object[]
					{
						"Unuse ",
						advCharaID,
						" pose",
						this.m_NeedList[i].m_PoseName,
						" UseCount = ",
						this.m_NeedList[i].m_UseCount,
						" idx",
						this.m_Player.GetCurrentCommandIdx()
					}));
					this.LoadRecentPic();
					break;
				}
			}
		}

		// Token: 0x0600038D RID: 909 RVA: 0x00011C44 File Offset: 0x00010044
		public void UnloadUnusePose()
		{
			int num = -1;
			StandPicManager.StandPicLoadParam standPicLoadParam = null;
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].m_IsLoaded)
				{
					if (this.m_NeedList[i].m_UseCount <= 0)
					{
						if (num == -1 || this.m_NeedList[i].m_UnuseIdx < num)
						{
							standPicLoadParam = this.m_NeedList[i];
							num = this.m_NeedList[i].m_UnuseIdx;
						}
					}
				}
			}
			if (standPicLoadParam != null)
			{
				this.GetStandPic(standPicLoadParam.m_AdvCharaID).UnloadPose(standPicLoadParam.m_PoseName);
			}
		}

		// Token: 0x0600038E RID: 910 RVA: 0x00011D04 File Offset: 0x00010104
		public void LoadRecentPicMax()
		{
			for (int i = 0; i < this.m_PosePicMax; i++)
			{
				int num = this.UsingPicNum();
				int num2 = this.LoadedPicNum();
				if (num >= num2 || num2 < this.m_PosePicMax)
				{
					break;
				}
				this.UnloadUnusePose();
			}
			for (int j = 0; j < this.m_PosePicMax; j++)
			{
				if (this.LoadedPicNum() < this.m_PosePicMax)
				{
					this.LoadRecentPic();
				}
			}
		}

		// Token: 0x0600038F RID: 911 RVA: 0x00011D88 File Offset: 0x00010188
		private void LoadRecentPic()
		{
			StandPicManager.StandPicLoadParam recentNeedStandPic = this.GetRecentNeedStandPic();
			if (recentNeedStandPic != null)
			{
				this.LoadStandPic(recentNeedStandPic);
				recentNeedStandPic.RemoveRecentUseIdx();
			}
		}

		// Token: 0x06000390 RID: 912 RVA: 0x00011DB0 File Offset: 0x000101B0
		public void ForceLoadPic(string advCharaID, string poseName)
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].m_AdvCharaID == advCharaID && this.m_NeedList[i].m_PoseName == poseName)
				{
					this.LoadStandPic(this.m_NeedList[i]);
					this.m_NeedList[i].RemoveRecentUseIdx();
					return;
				}
			}
			this.m_PosePicMax++;
			Debug.LogError("ForceLoadPic:advCharaID = " + advCharaID + " pose = " + poseName);
		}

		// Token: 0x06000391 RID: 913 RVA: 0x00011E5C File Offset: 0x0001025C
		private int UsingPicNum()
		{
			int num = 0;
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].m_UseCount > 0)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06000392 RID: 914 RVA: 0x00011EA4 File Offset: 0x000102A4
		private int LoadedPicNum()
		{
			int num = 0;
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				num += this.m_InstList[i].GetLoadedPoseNum();
			}
			return num;
		}

		// Token: 0x06000393 RID: 915 RVA: 0x00011EE4 File Offset: 0x000102E4
		public void LoadFirstNeedStandPics()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_InstList[i].UnloadSprites();
			}
			for (int j = 0; j < 5; j++)
			{
				StandPicManager.StandPicLoadParam recentNeedStandPic = this.GetRecentNeedStandPic();
				if (recentNeedStandPic == null)
				{
					break;
				}
				this.LoadStandPic(recentNeedStandPic);
				recentNeedStandPic.RemoveRecentUseIdx();
			}
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00011F54 File Offset: 0x00010354
		public void LoadFirstLoadStandPics(int currentIdx)
		{
			int num = this.m_PosePicMax - this.LoadedPicNum();
			for (int i = 0; i < num; i++)
			{
				this.LoadRecentPic();
			}
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00011F88 File Offset: 0x00010388
		private StandPicManager.StandPicLoadParam GetRecentNeedStandPic()
		{
			int num = -1;
			StandPicManager.StandPicLoadParam result = null;
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				if (this.m_NeedList[i].GetUseIdxListNum() > 0)
				{
					int recentIdx = this.m_NeedList[i].GetRecentIdx();
					if (recentIdx != -1)
					{
						if (num == -1)
						{
							num = recentIdx;
							result = this.m_NeedList[i];
						}
						else if (recentIdx < num)
						{
							num = recentIdx;
							result = this.m_NeedList[i];
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06000396 RID: 918 RVA: 0x00012028 File Offset: 0x00010428
		private void LoadStandPic(StandPicManager.StandPicLoadParam param)
		{
			if (param == null)
			{
				return;
			}
			Debug.Log(string.Concat(new object[]
			{
				"LoadStandPic  :  advCharaID = ",
				param.m_AdvCharaID,
				" idx = ",
				param.GetRecentIdx(),
				" : poseName = ",
				param.m_PoseName
			}));
			if (param.m_UseCount <= 0)
			{
				param.m_IsLoaded = true;
				this.GetStandPic(param.m_AdvCharaID).LoadPose(param.m_PoseName);
			}
			param.m_UseCount++;
		}

		// Token: 0x06000397 RID: 919 RVA: 0x000120BC File Offset: 0x000104BC
		public bool IsLoading()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (!this.m_InstList[i].IsAvailable())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000398 RID: 920 RVA: 0x00012100 File Offset: 0x00010500
		public bool IsDoneApplyPose()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (!this.m_InstList[i].IsDoneApplyPose())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00012144 File Offset: 0x00010544
		public bool IsFading()
		{
			for (int i = 0; i < this.m_PicList.Count; i++)
			{
				if (this.m_PicList[i].IsFading())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600039A RID: 922 RVA: 0x00012188 File Offset: 0x00010588
		private void Update()
		{
			if (this.m_InstList != null)
			{
				for (int i = 0; i < this.m_InstList.Count; i++)
				{
					this.m_InstList[i].UpdateApplyPose();
				}
			}
		}

		// Token: 0x0600039B RID: 923 RVA: 0x000121D0 File Offset: 0x000105D0
		public void Destroy()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_InstList[i].UnloadSprites();
				UnityEngine.Object.Destroy(this.m_InstList[i].CacheGameObject);
				this.m_InstList[i] = null;
			}
			this.m_InstList.Clear();
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00012238 File Offset: 0x00010638
		public List<ADVStandPic> GetStandPicList()
		{
			return this.m_InstList;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x00012240 File Offset: 0x00010640
		public ADVStandPic GetStandPic(string advCharaID)
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].ADVCharaID == advCharaID)
				{
					return this.m_InstList[i];
				}
			}
			return null;
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00012294 File Offset: 0x00010694
		public void HideChara(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic != null)
			{
				standPic.SetEnableRender(false);
				this.m_PicList.Remove(standPic);
			}
		}

		// Token: 0x0600039F RID: 927 RVA: 0x000122C9 File Offset: 0x000106C9
		public void HideAllChara()
		{
			while (0 < this.m_PicList.Count)
			{
				this.m_PicList[0].SetEnableRender(false);
			}
			this.m_PicList.Clear();
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00012300 File Offset: 0x00010700
		public void HideAllCharaFade(float sec)
		{
			for (int i = 0; i < this.m_PicList.Count; i++)
			{
				this.m_PicList[i].FadeOut(sec);
			}
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x0001233B File Offset: 0x0001073B
		public void RemovePicList(ADVStandPic standPic)
		{
			this.m_PicList.Remove(standPic);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0001234C File Offset: 0x0001074C
		public void SetCharaEnableRender(string advCharaID, bool flg, float fadesec = 0f)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			if (flg)
			{
				if (!standPic.IsActive())
				{
					if (fadesec > 0f)
					{
						standPic.FadeIn(fadesec);
					}
					else
					{
						standPic.SetEnableRender(true);
					}
					this.m_PicList.Add(standPic);
				}
			}
			else if (standPic.IsActive())
			{
				if (fadesec > 0f)
				{
					standPic.FadeOut(fadesec);
				}
				else
				{
					standPic.SetEnableRender(false);
				}
				this.m_PicList.Remove(standPic);
			}
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x000123E4 File Offset: 0x000107E4
		public void SetChara(string advCharaID, eADVStandPosition standPos, float xOffset, float sec, eADVCurveType curveType)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			Vector3 vector = ADVUtility.GetStandPosition(standPos) + new Vector3(xOffset, 0f, 0f);
			if (sec > 0f)
			{
				standPic.MoveTo(vector, sec, curveType, false);
			}
			else
			{
				standPic.SetAnchoredPosition(vector + new Vector3(xOffset, 0f, 0f));
			}
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00012468 File Offset: 0x00010868
		public void SetCharaScreenSide(string advCharaID, eADVSidePosition side, float xOffset, float sec, eADVCurveType curveType, bool charaOut = false)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			Vector2 vector;
			if (side == eADVSidePosition.LeftSide)
			{
				vector = new Vector2(-this.m_RectTransform.rect.width * 0.5f - standPic.RectTransform.rect.width * 0.5f + xOffset, 0f);
			}
			else
			{
				vector = new Vector2(this.m_RectTransform.rect.width * 0.5f + standPic.RectTransform.rect.width * 0.5f + xOffset, 0f);
			}
			if (sec > 0f)
			{
				standPic.MoveTo(vector, sec, curveType, true);
			}
			else
			{
				standPic.SetAnchoredPosition(vector);
				if (charaOut)
				{
					this.SetCharaEnableRender(advCharaID, false, 0f);
				}
			}
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00012554 File Offset: 0x00010954
		public void CharaAlignment(float m_Sec, eADVCurveType curveType)
		{
			List<ADVStandPic> list = new List<ADVStandPic>();
			for (int i = 0; i < this.m_PicList.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < list.Count; j++)
				{
					if (list[j].GetCharaPosition().x > this.m_PicList[i].GetCharaPosition().x)
					{
						list.Insert(j, this.m_PicList[i]);
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					list.Add(this.m_PicList[i]);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				list[k].MoveTo(new Vector3(-this.m_RectTransform.rect.width * 0.5f + (float)(k + 1) * this.m_RectTransform.rect.width / (float)(list.Count + 1), 0f), m_Sec, curveType, false);
			}
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0001267C File Offset: 0x00010A7C
		public Vector2 GetCharaPosition(string advCharaID)
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].ADVCharaID == advCharaID)
				{
					Vector2 result = new Vector3(0f, 0f);
					result.x = this.m_InstList[i].RectTransform.anchoredPosition.x;
					result.y = this.m_InstList[i].RectTransform.anchoredPosition.y;
					return result;
				}
			}
			return new Vector3(0f, 0f, 0f);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x0001273C File Offset: 0x00010B3C
		public Vector2 GetCharaEmoPosition(string charaID, eADVEmotionPosition position)
		{
			Vector2 vector = new Vector2(0f, 0f);
			Vector2 emoOffset = new Vector2(0f, 0f);
			if (position == eADVEmotionPosition.CharaBottom)
			{
				for (int i = 0; i < this.m_InstList.Count; i++)
				{
					if (this.m_InstList[i].ADVCharaID == charaID)
					{
						vector = this.m_InstList[i].GetCharaFootPosition();
						vector.y -= this.RectTransform.anchoredPosition.y;
						break;
					}
				}
			}
			else
			{
				for (int j = 0; j < this.m_InstList.Count; j++)
				{
					if (this.m_InstList[j].ADVCharaID == charaID)
					{
						vector = this.m_InstList[j].GetCharaFacePosition();
						emoOffset = this.m_InstList[j].GetEmoOffset();
						break;
					}
				}
				if (position != eADVEmotionPosition.CharaFace)
				{
					if (position != eADVEmotionPosition.CharaFaceRight)
					{
						if (position == eADVEmotionPosition.CharaFaceLeft)
						{
							vector.x -= 1f * emoOffset.x;
							vector.y += 1f * emoOffset.y;
						}
					}
					else
					{
						vector.x += 1f * emoOffset.x;
						vector.y += 1f * emoOffset.y;
					}
				}
				else
				{
					vector.y += 1f * emoOffset.y;
				}
			}
			vector = this.ConvertPositionTo3D(vector);
			return vector;
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x0001290D File Offset: 0x00010D0D
		public void SetAsLastSibling(string advCharaID)
		{
			this.SetAsLastSibling(advCharaID, true);
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x00012918 File Offset: 0x00010D18
		protected void SetAsLastSibling(string advCharaID, bool isExecNormalOnly)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			ADVStandPic.ePriorityState priorityState = standPic.GetPriorityState();
			if (priorityState != ADVStandPic.ePriorityState.TopSet)
			{
				if (priorityState != ADVStandPic.ePriorityState.Normal)
				{
					if (priorityState == ADVStandPic.ePriorityState.BottomSet)
					{
						if (!isExecNormalOnly)
						{
							this.SetAsBottomSetGroupLastSibling(advCharaID);
						}
					}
				}
				else
				{
					this.SetAsNormalGroupLastSibling(advCharaID);
				}
			}
			else if (!isExecNormalOnly)
			{
				this.SetAsTopSetGroupLastSibling(advCharaID);
			}
		}

		// Token: 0x060003AA RID: 938 RVA: 0x0001298C File Offset: 0x00010D8C
		protected void SetAsFirstSibling(string advCharaID, bool isExecNormalOnly)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			ADVStandPic.ePriorityState priorityState = standPic.GetPriorityState();
			if (priorityState != ADVStandPic.ePriorityState.TopSet)
			{
				if (priorityState != ADVStandPic.ePriorityState.Normal)
				{
					if (priorityState == ADVStandPic.ePriorityState.BottomSet)
					{
						if (!isExecNormalOnly)
						{
							this.SetAsBottomSetGroupFirstSibling(advCharaID);
						}
					}
				}
				else
				{
					this.SetAsNormalGroupFirstSibling(advCharaID);
				}
			}
			else if (!isExecNormalOnly)
			{
				this.SetAsTopSetGroupFirstSibling(advCharaID);
			}
		}

		// Token: 0x060003AB RID: 939 RVA: 0x000129FF File Offset: 0x00010DFF
		public void CharaPriorityTop(string advCharaID)
		{
			this.SetAsLastSibling(advCharaID, false);
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00012A09 File Offset: 0x00010E09
		public void CharaPriorityBottom(string advCharaID)
		{
			this.SetAsFirstSibling(advCharaID, false);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00012A14 File Offset: 0x00010E14
		public void SetAsTopSetGroupLastSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.TopSet);
			this.InsertSiblingIndex(this.m_InstList.Count, standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00012A58 File Offset: 0x00010E58
		public void SetAsTopSetGroupFirstSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.TopSet);
			this.InsertSiblingIndex(this.GetPriorityTopSetGroupTailIndex(), standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00012A94 File Offset: 0x00010E94
		public void SetAsNormalGroupLastSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.Normal);
			int num = this.GetPriorityNormalGroupHeadIndex();
			if (num < standPic.RectTransform.GetSiblingIndex())
			{
				num++;
			}
			this.InsertSiblingIndex(num, standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x00012AE8 File Offset: 0x00010EE8
		public void SetAsNormalGroupFirstSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.Normal);
			int num = this.GetPriorityNormalGroupTailIndex();
			if (standPic.RectTransform.GetSiblingIndex() < num)
			{
				num--;
			}
			this.InsertSiblingIndex(num, standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x00012B3C File Offset: 0x00010F3C
		public void SetAsBottomSetGroupLastSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.BottomSet);
			this.InsertSiblingIndex(this.GetPriorityBottomSetGroupHeadIndex(), standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x00012B78 File Offset: 0x00010F78
		public void SetAsBottomSetGroupFirstSibling(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetPriorityState(ADVStandPic.ePriorityState.BottomSet);
			this.InsertSiblingIndex(0, standPic);
			this.RefreshPositionZ();
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x00012BB0 File Offset: 0x00010FB0
		public void SetHDRFactor(string advCharaID, float hdrFactor)
		{
			if (advCharaID == null)
			{
				return;
			}
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.SetHDRFactor(hdrFactor);
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x00012BE0 File Offset: 0x00010FE0
		private void InsertSiblingIndex(int index, ADVStandPic pic)
		{
			if (pic == null)
			{
				return;
			}
			if (pic.RectTransform.GetSiblingIndex() < index)
			{
				this.InsertSiblingIndexFront(index, pic);
			}
			else if (index < pic.RectTransform.GetSiblingIndex())
			{
				this.InsertSiblingIndexBack(index, pic);
			}
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x00012C36 File Offset: 0x00011036
		private void InsertSiblingIndexFront(int index, ADVStandPic pic)
		{
			if (pic == null)
			{
				return;
			}
			pic.RectTransform.SetSiblingIndex(index);
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x00012C51 File Offset: 0x00011051
		private void InsertSiblingIndexBack(int index, ADVStandPic pic)
		{
			if (pic == null)
			{
				return;
			}
			pic.RectTransform.SetSiblingIndex(index);
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x00012C6C File Offset: 0x0001106C
		public void CharaPriorityReset(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			ADVStandPic.ePriorityState priorityState = this.GetPriorityState(standPic);
			this.SetPriorityState(standPic, ADVStandPic.ePriorityState.Normal);
			if (priorityState != ADVStandPic.ePriorityState.TopSet)
			{
				if (priorityState == ADVStandPic.ePriorityState.BottomSet)
				{
					this.CharaPriorityBottom(advCharaID);
				}
			}
			else
			{
				this.CharaPriorityTop(advCharaID);
			}
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x00012CCC File Offset: 0x000110CC
		private void RefreshPositionZ()
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				int siblingIndex = this.m_InstList[i].RectTransform.GetSiblingIndex();
				this.m_InstList[i].SetPositionZ(-(float)siblingIndex * 100f);
			}
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x00012D28 File Offset: 0x00011128
		private int GetPriorityTopSetGroupTailIndex()
		{
			int num = this.m_InstList.Count;
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].GetPriorityState() == ADVStandPic.ePriorityState.TopSet)
				{
					int siblingIndex = this.m_InstList[i].RectTransform.GetSiblingIndex();
					if (siblingIndex < num)
					{
						num = siblingIndex;
					}
				}
			}
			return num;
		}

		// Token: 0x060003BA RID: 954 RVA: 0x00012D95 File Offset: 0x00011195
		private int GetPriorityNormalGroupHeadIndex()
		{
			return this.GetPriorityTopSetGroupTailIndex() - 1;
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00012D9F File Offset: 0x0001119F
		private int GetPriorityNormalGroupTailIndex()
		{
			return this.GetPriorityBottomSetGroupHeadIndex() + 1;
		}

		// Token: 0x060003BC RID: 956 RVA: 0x00012DAC File Offset: 0x000111AC
		private int GetPriorityBottomSetGroupHeadIndex()
		{
			int num = -1;
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].GetPriorityState() == ADVStandPic.ePriorityState.BottomSet)
				{
					int siblingIndex = this.m_InstList[i].RectTransform.GetSiblingIndex();
					if (num < siblingIndex)
					{
						num = siblingIndex;
					}
				}
			}
			return num;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x00012E0F File Offset: 0x0001120F
		public void SetPriorityState(string advCharaID, ADVStandPic.ePriorityState state)
		{
			this.SetPriorityState(this.GetStandPic(advCharaID), state);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x00012E1F File Offset: 0x0001121F
		public void SetPriorityState(ADVStandPic pic, ADVStandPic.ePriorityState state)
		{
			if (pic == null)
			{
				return;
			}
			pic.SetPriorityState(state);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x00012E35 File Offset: 0x00011235
		public ADVStandPic.ePriorityState GetPriorityState(string advCharaID)
		{
			return this.GetPriorityState(this.GetStandPic(advCharaID));
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x00012E44 File Offset: 0x00011244
		public ADVStandPic.ePriorityState GetPriorityState(ADVStandPic pic)
		{
			if (pic == null)
			{
				return ADVStandPic.ePriorityState.Error;
			}
			return pic.GetPriorityState();
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x00012E5C File Offset: 0x0001125C
		public void HighlightTalker(string advCharaID = null)
		{
			this.HighlightTalkerResetAll();
			if (advCharaID != null)
			{
				this.AddNextTalkTalkerHighlightCharacter(advCharaID);
			}
			for (int i = 0; i < this.m_NextTalkTalkerHighlightCharacters.Count; i++)
			{
				ADVStandPic standPic = this.GetStandPic(this.m_NextTalkTalkerHighlightCharacters[i]);
				if (standPic != null)
				{
					standPic.HighlightTalker();
				}
			}
			this.m_NextTalkTalkerHighlightCharacters.Clear();
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x00012EC8 File Offset: 0x000112C8
		public void HighlightTalkerResetAll()
		{
			if (this.m_PicList != null)
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					this.m_PicList[i].HighlightTalkerReset();
				}
			}
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x00012F0D File Offset: 0x0001130D
		public void AddNextTalkTalkerHighlightCharacter(string advCharaID)
		{
			this.m_NextTalkTalkerHighlightCharacters.Add(advCharaID);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x00012F1C File Offset: 0x0001131C
		public void Highlight(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.Highlight();
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x00012F44 File Offset: 0x00011344
		public void HighlightAll()
		{
			if (this.m_PicList != null)
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					this.Highlight(this.m_PicList[i].ADVCharaID);
				}
			}
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00012F90 File Offset: 0x00011390
		public void HighlightDefault(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.HighlightDefault();
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x00012FB8 File Offset: 0x000113B8
		public void HighlightDefaultAll()
		{
			if (this.m_PicList != null)
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					this.HighlightDefault(this.m_PicList[i].ADVCharaID);
				}
			}
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00013004 File Offset: 0x00011404
		public void Shading(string advCharaID)
		{
			ADVStandPic standPic = this.GetStandPic(advCharaID);
			if (standPic == null)
			{
				return;
			}
			standPic.Shading();
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x0001302C File Offset: 0x0001142C
		public void ShadingAll()
		{
			if (this.m_PicList != null)
			{
				for (int i = 0; i < this.m_PicList.Count; i++)
				{
					this.Shading(this.m_PicList[i].ADVCharaID);
				}
			}
		}

		// Token: 0x060003CA RID: 970 RVA: 0x00013078 File Offset: 0x00011478
		public void AlphaToAll(uint alpha, float duration, eADVCurveType easeType)
		{
			for (int i = 0; i < this.m_PicList.Count; i++)
			{
				this.m_PicList[i].AlphaTo(alpha, duration, easeType);
			}
		}

		// Token: 0x060003CB RID: 971 RVA: 0x000130B8 File Offset: 0x000114B8
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			pos.y += this.m_RectTransform.localPosition.y;
			pos.x /= this.m_CanvasRectTransform.sizeDelta.y;
			pos.y /= this.m_CanvasRectTransform.sizeDelta.y;
			return pos;
		}

		// Token: 0x060003CC RID: 972 RVA: 0x00013130 File Offset: 0x00011530
		public bool IsMovingChara(string charaId)
		{
			for (int i = 0; i < this.m_PicList.Count; i++)
			{
				if (this.m_PicList[i] != null && this.m_PicList[i].ADVCharaID == charaId)
				{
					return this.m_PicList[i].IsMoving;
				}
			}
			return false;
		}

		// Token: 0x040001F6 RID: 502
		public const int STAND_PIC_FIRST_NUM = 5;

		// Token: 0x040001F7 RID: 503
		public const int STAND_PIC_POSE_MAX = 15;

		// Token: 0x040001F8 RID: 504
		public const float ZSPACE = 100f;

		// Token: 0x040001F9 RID: 505
		private List<StandPicManager.StandPicLoadParam> m_NeedList;

		// Token: 0x040001FA RID: 506
		[SerializeField]
		private ADVStandPic m_StandPicPrefab;

		// Token: 0x040001FB RID: 507
		private List<ADVStandPic> m_InstList;

		// Token: 0x040001FC RID: 508
		private List<ADVStandPic> m_PicList = new List<ADVStandPic>();

		// Token: 0x040001FD RID: 509
		[SerializeField]
		private RectTransform m_CanvasRectTransform;

		// Token: 0x040001FE RID: 510
		[SerializeField]
		private Material m_PicMaterialBase;

		// Token: 0x040001FF RID: 511
		private int m_PosePicMax = 15;

		// Token: 0x04000200 RID: 512
		private RectTransform m_RectTransform;

		// Token: 0x04000201 RID: 513
		private ADVPlayer m_Player;

		// Token: 0x04000202 RID: 514
		private List<string> m_NextTalkTalkerHighlightCharacters = new List<string>();

		// Token: 0x02000079 RID: 121
		private class StandPicLoadParam
		{
			// Token: 0x060003CD RID: 973 RVA: 0x0001319F File Offset: 0x0001159F
			public StandPicLoadParam(string advCharaId, string poseName)
			{
				this.m_AdvCharaID = advCharaId;
				this.m_PoseName = poseName;
				this.m_UseIdxList = new List<int>();
			}

			// Token: 0x060003CE RID: 974 RVA: 0x000131C7 File Offset: 0x000115C7
			public void AddUseIdx(int useStartIdx)
			{
				if (!this.m_UseIdxList.Contains(useStartIdx))
				{
					this.m_UseIdxList.Add(useStartIdx);
					this.m_UseIdxList.Sort();
				}
			}

			// Token: 0x060003CF RID: 975 RVA: 0x000131F1 File Offset: 0x000115F1
			public void RemoveRecentUseIdx()
			{
				this.m_UseIdxList.RemoveAt(0);
			}

			// Token: 0x060003D0 RID: 976 RVA: 0x000131FF File Offset: 0x000115FF
			public int GetUseIdxListNum()
			{
				return this.m_UseIdxList.Count;
			}

			// Token: 0x060003D1 RID: 977 RVA: 0x0001320C File Offset: 0x0001160C
			public int GetRecentIdx()
			{
				if (this.m_UseIdxList.Count > 0)
				{
					return this.m_UseIdxList[0];
				}
				return -1;
			}

			// Token: 0x04000203 RID: 515
			public string m_AdvCharaID;

			// Token: 0x04000204 RID: 516
			public string m_PoseName;

			// Token: 0x04000205 RID: 517
			private List<int> m_UseIdxList;

			// Token: 0x04000206 RID: 518
			public bool m_IsLoaded;

			// Token: 0x04000207 RID: 519
			public int m_UseCount;

			// Token: 0x04000208 RID: 520
			public int m_UnuseIdx = -1;
		}
	}
}
