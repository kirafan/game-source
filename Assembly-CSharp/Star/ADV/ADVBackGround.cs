﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000008 RID: 8
	public class ADVBackGround : MonoBehaviour
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000049 RID: 73 RVA: 0x00003C03 File Offset: 0x00002003
		public bool IsEmpty
		{
			get
			{
				return this.m_Image.sprite == null;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600004A RID: 74 RVA: 0x00003C16 File Offset: 0x00002016
		// (set) Token: 0x0600004B RID: 75 RVA: 0x00003C1E File Offset: 0x0000201E
		public uint ID
		{
			get
			{
				return this.m_ID;
			}
			set
			{
				this.m_ID = value;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00003C27 File Offset: 0x00002027
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00003C2F File Offset: 0x0000202F
		public void Setup(string filename = null)
		{
			this.m_Image.sprite = null;
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00003C58 File Offset: 0x00002058
		public void Initialize()
		{
			this.Setup(null);
			this.SetEnableRender(false);
			this.RectTransform.localPosZ(0f);
			this.RectTransform.sizeDelta = new Vector2(0f, 0f);
			this.RectTransform.localScale = Vector2.one;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00003CB2 File Offset: 0x000020B2
		private void OnDestroy()
		{
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00003CB4 File Offset: 0x000020B4
		public void Destroy()
		{
			this.m_Image.sprite = null;
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00003CC4 File Offset: 0x000020C4
		public void MoveTo(Vector2 pos, float sec, eADVCurveType curveType)
		{
			RectTransform component = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector3 vector = new Vector3(pos.x, 375f + pos.y, 0f) + new Vector3(((this.m_RectTransform.anchorMin.x + this.m_RectTransform.anchorMax.x) * 0.5f - component.pivot.x) * component.rect.width, ((this.m_RectTransform.anchorMin.y + this.m_RectTransform.anchorMax.y) * 0.5f - component.pivot.y) * component.rect.height);
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", sec);
			hashtable.Add("delay", 0.1f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(curveType));
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00003E3C File Offset: 0x0000223C
		public void ScaleTo(float scale, float sec, eADVCurveType curveType)
		{
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", scale);
			hashtable.Add("y", scale);
			hashtable.Add("time", sec);
			hashtable.Add("delay", 0.1f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(curveType));
			iTween.ScaleTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00003ECC File Offset: 0x000022CC
		public void Zoom(float scale, float sec, float x, float y, eADVCurveType curveType)
		{
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", scale);
			hashtable.Add("y", scale);
			hashtable.Add("time", sec);
			hashtable.Add("delay", 0.1f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(curveType));
			iTween.ScaleTo(this.m_GameObject, hashtable);
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("isLocal", true);
			hashtable2.Add("x", (this.m_RectTransform.rect.width * 0.5f - x) * scale);
			hashtable2.Add("y", -(this.m_RectTransform.rect.height * 0.5f - y) * scale);
			hashtable2.Add("time", sec);
			hashtable2.Add("delay", 0.1f);
			hashtable2.Add("easeType", ADVUtility.GetEaseType(curveType));
			iTween.MoveTo(this.m_GameObject, hashtable2);
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00004018 File Offset: 0x00002418
		public void ChangeColor(Color startColor, Color endColor, float sec, eADVCurveType curveType)
		{
			this.m_StartColor = startColor;
			this.m_EndColor = endColor;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", 0f);
			hashtable.Add("to", 1f);
			hashtable.Add("time", sec);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", ADVUtility.GetEaseType(curveType));
			hashtable.Add("onupdate", "OnUpdateColorChange");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06000055 RID: 85 RVA: 0x000040D0 File Offset: 0x000024D0
		public void SetSprite(Sprite sprite)
		{
			this.m_Image.sprite = sprite;
			this.m_RectTransform.pivot = new Vector2(0.5f, 0.5f);
			this.m_RectTransform.anchoredPosition = new Vector2(0f, 0f);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x0000411D File Offset: 0x0000251D
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06000057 RID: 87 RVA: 0x0000412B File Offset: 0x0000252B
		public void OnUpdateColorChange(float value)
		{
			this.m_Image.color = this.m_StartColor * (1f - value) + this.m_EndColor * value;
		}

		// Token: 0x0400003C RID: 60
		private const float ORIG_ASPECT_RATIO = 1.3333334f;

		// Token: 0x0400003D RID: 61
		[SerializeField]
		[Tooltip("表示の実体.")]
		private Image m_Image;

		// Token: 0x0400003E RID: 62
		private uint m_ID;

		// Token: 0x0400003F RID: 63
		private Color m_StartColor;

		// Token: 0x04000040 RID: 64
		private Color m_EndColor;

		// Token: 0x04000041 RID: 65
		private GameObject m_GameObject;

		// Token: 0x04000042 RID: 66
		private RectTransform m_RectTransform;
	}
}
