﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000045 RID: 69
	public class ADVTextSeTagEnd : ADVTextSeTagBase
	{
		// Token: 0x060002A2 RID: 674 RVA: 0x0000F6DC File Offset: 0x0000DADC
		public ADVTextSeTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000F6E7 File Offset: 0x0000DAE7
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
