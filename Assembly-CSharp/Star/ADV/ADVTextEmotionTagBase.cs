﻿using System;

namespace Star.ADV
{
	// Token: 0x02000054 RID: 84
	public class ADVTextEmotionTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002BC RID: 700 RVA: 0x0000FB7B File Offset: 0x0000DF7B
		protected ADVTextEmotionTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_emo, isEndTag)
		{
		}
	}
}
