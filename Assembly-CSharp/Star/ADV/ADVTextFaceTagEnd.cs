﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200005C RID: 92
	public class ADVTextFaceTagEnd : ADVTextFaceTagBase
	{
		// Token: 0x060002C9 RID: 713 RVA: 0x0000FE77 File Offset: 0x0000E277
		public ADVTextFaceTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002CA RID: 714 RVA: 0x0000FE82 File Offset: 0x0000E282
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return false;
		}
	}
}
