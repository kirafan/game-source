﻿using System;

namespace Star.ADV
{
	// Token: 0x02000060 RID: 96
	public class ADVTextPageTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002D0 RID: 720 RVA: 0x0000FF80 File Offset: 0x0000E380
		protected ADVTextPageTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_wait, isEndTag)
		{
		}
	}
}
