﻿using System;

namespace Star.ADV
{
	// Token: 0x0200003C RID: 60
	public class ADVTextOriginalRangeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06000292 RID: 658 RVA: 0x0000F487 File Offset: 0x0000D887
		protected ADVTextOriginalRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag) : base(player, parentParser, eventType, isEndTag)
		{
		}
	}
}
