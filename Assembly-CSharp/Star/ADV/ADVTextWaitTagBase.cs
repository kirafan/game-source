﻿using System;

namespace Star.ADV
{
	// Token: 0x02000049 RID: 73
	public class ADVTextWaitTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002A9 RID: 681 RVA: 0x0000F813 File Offset: 0x0000DC13
		protected ADVTextWaitTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_wait, isEndTag)
		{
		}
	}
}
