﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200004A RID: 74
	public class ADVTextWaitTag : ADVTextWaitTagBase
	{
		// Token: 0x060002AA RID: 682 RVA: 0x0000F820 File Offset: 0x0000DC20
		public ADVTextWaitTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000F82C File Offset: 0x0000DC2C
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int length = tag.IndexOf(">") - num;
				string value = tag.Substring(num, length);
				float wait = Convert.ToSingle(value);
				this.m_ParentParser.GetTalkWindow().SetWait(wait);
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
