﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200004C RID: 76
	public class ADVTextMoveTag : ADVTextRangeTagBase
	{
		// Token: 0x060002AE RID: 686 RVA: 0x0000F8BE File Offset: 0x0000DCBE
		public ADVTextMoveTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, ADVParser.EventType.event_move, false)
		{
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000F8CC File Offset: 0x0000DCCC
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			List<string> list = base.ParseTag(tag);
			ADVStandPic standPic = this.m_Player.StandPicManager.GetStandPic(list[0]);
			if (standPic != null)
			{
				if (this.m_IsTapWaitWithTap)
				{
					standPic.StopMotion();
					standPic.SetPosition(ADVUtility.GetStandPosition((eADVStandPosition)this.m_Player.GetADVTextTagParam(list[1]).m_EnumID));
					result = false;
				}
				else if (execTag && !this.m_IsEndTag)
				{
					standPic.MoveTo(ADVUtility.GetStandPosition((eADVStandPosition)this.m_Player.GetADVTextTagParam(list[1]).m_EnumID), float.Parse(list[2]), (eADVCurveType)this.m_Player.GetADVTextTagParam(list[3]).m_EnumID, false);
					result = true;
				}
				else if (!this.m_IsEndTag)
				{
					result = false;
				}
				base.DeleteTag(ref text, ref idx, tag.Length);
			}
			return result;
		}
	}
}
