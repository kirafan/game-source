﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x02000009 RID: 9
	public class ADVBackGroundManager : MonoBehaviour
	{
		// Token: 0x06000059 RID: 89 RVA: 0x00004163 File Offset: 0x00002563
		private void Awake()
		{
			this.Setup();
		}

		// Token: 0x0600005A RID: 90 RVA: 0x0000416C File Offset: 0x0000256C
		public void Setup()
		{
			this.m_SpriteHandlerFileNamePairs = new List<ADVBackGroundManager.SpriteHandlerFileNamePair>();
			this.m_Insts = new ADVBackGround[4];
			for (int i = 0; i < 4; i++)
			{
				this.m_Insts[i] = UnityEngine.Object.Instantiate<ADVBackGround>(this.m_Prefab, this.m_ShakeBGParent);
				this.m_Insts[i].Initialize();
			}
		}

		// Token: 0x0600005B RID: 91 RVA: 0x000041C8 File Offset: 0x000025C8
		private void OnDestroy()
		{
			for (int i = 0; i < this.m_Insts.Length; i++)
			{
				if (this.m_Insts[i] != null)
				{
					this.m_Insts[i].Destroy();
					this.m_Insts[i] = null;
				}
			}
			this.m_Insts = null;
			for (int j = 0; j < this.m_SpriteHandlerFileNamePairs.Count; j++)
			{
				if (this.m_SpriteHandlerFileNamePairs[j] != null && this.m_SpriteHandlerFileNamePairs[j].m_Handler != null)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst != null)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandlerFileNamePairs[j].m_Handler);
					}
					this.m_SpriteHandlerFileNamePairs[j].m_Handler = null;
				}
			}
			this.m_SpriteHandlerFileNamePairs.Clear();
			this.m_SpriteHandlerFileNamePairs = null;
		}

		// Token: 0x0600005C RID: 92 RVA: 0x000042B8 File Offset: 0x000026B8
		public void Load(string fileName)
		{
			SpriteHandler handler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVBackGround(fileName);
			this.m_SpriteHandlerFileNamePairs.Add(new ADVBackGroundManager.SpriteHandlerFileNamePair(handler, fileName));
		}

		// Token: 0x0600005D RID: 93 RVA: 0x000042E8 File Offset: 0x000026E8
		public void Visible(uint id, string fileName)
		{
			ADVBackGround advbackGround = this.GetActiveMatchIdInstance(id);
			if (advbackGround == null)
			{
				advbackGround = this.GetEmptyInstance();
				advbackGround.ID = id;
			}
			if (advbackGround == null)
			{
				return;
			}
			advbackGround.Initialize();
			SpriteHandler spriteHandler = this.GetSpriteHandler(fileName);
			if (spriteHandler == null)
			{
				return;
			}
			advbackGround.SetSprite(spriteHandler.Obj);
			advbackGround.SetEnableRender(true);
			for (int i = 0; i < this.m_Insts.Length; i++)
			{
				if (!this.m_Insts[i].IsEmpty)
				{
					this.m_Insts[i].RectTransform.SetSiblingIndex((int)this.m_Insts[i].ID);
				}
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x0000439C File Offset: 0x0000279C
		public SpriteHandler GetSpriteHandler(string fileName)
		{
			for (int i = 0; i < this.m_SpriteHandlerFileNamePairs.Count; i++)
			{
				if (this.m_SpriteHandlerFileNamePairs[i].m_FileName == fileName)
				{
					return this.m_SpriteHandlerFileNamePairs[i].m_Handler;
				}
			}
			return null;
		}

		// Token: 0x0600005F RID: 95 RVA: 0x000043F4 File Offset: 0x000027F4
		public ADVBackGround GetEmptyInstance()
		{
			for (int i = 0; i < this.m_Insts.Length; i++)
			{
				if (this.m_Insts[i].IsEmpty)
				{
					return this.m_Insts[i];
				}
			}
			return null;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00004438 File Offset: 0x00002838
		public ADVBackGround GetActiveMatchIdInstance(uint id)
		{
			for (int i = 0; i < this.m_Insts.Length; i++)
			{
				if (!this.m_Insts[i].IsEmpty)
				{
					if (this.m_Insts[i].ID == id)
					{
						return this.m_Insts[i];
					}
				}
			}
			return null;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00004494 File Offset: 0x00002894
		public bool IsDonePrepare()
		{
			for (int i = 0; i < this.m_SpriteHandlerFileNamePairs.Count; i++)
			{
				if (this.m_SpriteHandlerFileNamePairs[i] != null)
				{
					if (this.m_SpriteHandlerFileNamePairs[i].m_Handler != null)
					{
						if (!this.m_SpriteHandlerFileNamePairs[i].m_Handler.IsAvailable())
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		// Token: 0x04000043 RID: 67
		[SerializeField]
		[Tooltip("子をぶら下げる親.")]
		private RectTransform m_ShakeBGParent;

		// Token: 0x04000044 RID: 68
		[SerializeField]
		[Tooltip("子の元となるPrefab.")]
		private ADVBackGround m_Prefab;

		// Token: 0x04000045 RID: 69
		private const int BG_MAX = 4;

		// Token: 0x04000046 RID: 70
		private List<ADVBackGroundManager.SpriteHandlerFileNamePair> m_SpriteHandlerFileNamePairs;

		// Token: 0x04000047 RID: 71
		private ADVBackGround[] m_Insts;

		// Token: 0x0200000A RID: 10
		public class SpriteHandlerFileNamePair
		{
			// Token: 0x06000062 RID: 98 RVA: 0x0000450C File Offset: 0x0000290C
			public SpriteHandlerFileNamePair(SpriteHandler handler, string fileName)
			{
				this.m_Handler = handler;
				this.m_FileName = fileName;
			}

			// Token: 0x04000048 RID: 72
			public SpriteHandler m_Handler;

			// Token: 0x04000049 RID: 73
			public string m_FileName;
		}
	}
}
