﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000048 RID: 72
	public class ADVTextVoiceTagEnd : ADVTextVoiceTagBase
	{
		// Token: 0x060002A7 RID: 679 RVA: 0x0000F7F6 File Offset: 0x0000DBF6
		public ADVTextVoiceTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000F801 File Offset: 0x0000DC01
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
