﻿using System;

namespace Star.ADV
{
	// Token: 0x0200005A RID: 90
	public class ADVTextFaceTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002C6 RID: 710 RVA: 0x0000FD76 File Offset: 0x0000E176
		protected ADVTextFaceTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_face, isEndTag)
		{
		}
	}
}
