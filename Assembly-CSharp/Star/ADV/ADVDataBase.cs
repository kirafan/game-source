﻿using System;
using System.Collections.Generic;
using Meige;

namespace Star.ADV
{
	// Token: 0x0200000F RID: 15
	public class ADVDataBase
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000065 RID: 101 RVA: 0x0000455E File Offset: 0x0000295E
		public bool IsDoneLoad
		{
			get
			{
				return this.m_IsDoneLoad;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00004566 File Offset: 0x00002966
		public bool IsError
		{
			get
			{
				return this.m_IsError;
			}
		}

		// Token: 0x06000067 RID: 103 RVA: 0x0000456E File Offset: 0x0000296E
		public void Init()
		{
			this.m_CharaDBIndices = new Dictionary<string, int>();
			this.m_MotionDBIndices = new Dictionary<string, int>();
			this.m_EmotionDBIndices = new Dictionary<string, int>();
			this.m_TextTagDBIndices = new Dictionary<string, int>();
			this.m_IsDoneLoad = false;
		}

		// Token: 0x06000068 RID: 104 RVA: 0x000045A4 File Offset: 0x000029A4
		public void Prepare()
		{
			this.m_Loader = new ABResourceLoader(-1);
			ABResourceObjectHandler abresourceObjectHandler = this.m_Loader.Load("adv/advdatabase/advdatabase.muast", new MeigeResource.Option[0]);
			if (abresourceObjectHandler != null)
			{
				this.m_ResourceHandler = abresourceObjectHandler;
			}
		}

		// Token: 0x06000069 RID: 105 RVA: 0x000045E8 File Offset: 0x000029E8
		public bool UpdatePrepare()
		{
			if (this.IsDoneLoad)
			{
				return true;
			}
			if (this.m_Loader != null)
			{
				this.m_Loader.Update();
			}
			if (this.m_ResourceHandler != null)
			{
				if (!this.m_ResourceHandler.IsDone())
				{
					return false;
				}
				if (this.m_ResourceHandler.IsError())
				{
					APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
					this.m_ResourceHandler = null;
					this.m_IsError = true;
					return false;
				}
				this.m_CharaList = (ADVCharacterListDB)this.m_ResourceHandler.FindObj(this.OBJ_NAME[0], true);
				this.m_EmotionList = (ADVEmotionListDB)this.m_ResourceHandler.FindObj(this.OBJ_NAME[1], true);
				this.m_MotionList = (ADVMotionListDB)this.m_ResourceHandler.FindObj(this.OBJ_NAME[2], true);
				this.m_TextTagArgList = (ADVTextTagArgDB)this.m_ResourceHandler.FindObj(this.OBJ_NAME[3], true);
			}
			for (int i = 0; i < this.m_CharaList.m_Params.Length; i++)
			{
				this.m_CharaDBIndices.Add(this.m_CharaList.m_Params[i].m_ADVCharaID, i);
			}
			for (int j = 0; j < this.m_MotionList.m_Params.Length; j++)
			{
				this.m_MotionDBIndices.Add(this.m_MotionList.m_Params[j].m_MotionID, j);
			}
			for (int k = 0; k < this.m_EmotionList.m_Params.Length; k++)
			{
				this.m_EmotionDBIndices.Add(this.m_EmotionList.m_Params[k].m_EmotionID, k);
			}
			for (int l = 0; l < this.m_TextTagArgList.m_Params.Length; l++)
			{
				this.m_TextTagDBIndices.Add(this.m_TextTagArgList.m_Params[l].m_ReferenceName, l);
			}
			this.m_IsDoneLoad = true;
			return true;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x000047EE File Offset: 0x00002BEE
		public void Destroy()
		{
			this.m_CharaList = null;
			this.m_EmotionList = null;
			this.m_MotionList = null;
			this.m_TextTagArgList = null;
			this.m_Loader.UnloadAll(false);
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00004818 File Offset: 0x00002C18
		public bool UpdateDestroy()
		{
			if (this.m_Loader != null && !this.m_Loader.IsCompleteUnloadAll())
			{
				this.m_Loader.Update();
				return this.m_Loader.IsCompleteUnloadAll();
			}
			return true;
		}

		// Token: 0x04000065 RID: 101
		private ABResourceLoader m_Loader;

		// Token: 0x04000066 RID: 102
		private ABResourceObjectHandler m_ResourceHandler;

		// Token: 0x04000067 RID: 103
		private const string RESOURCE_PATH = "adv/advdatabase/advdatabase.muast";

		// Token: 0x04000068 RID: 104
		private readonly string[] OBJ_NAME = new string[]
		{
			"ADVCharacterList",
			"ADVEmotionList",
			"ADVMotionList",
			"ADVTextTagArg"
		};

		// Token: 0x04000069 RID: 105
		public ADVCharacterListDB m_CharaList;

		// Token: 0x0400006A RID: 106
		public ADVEmotionListDB m_EmotionList;

		// Token: 0x0400006B RID: 107
		public ADVMotionListDB m_MotionList;

		// Token: 0x0400006C RID: 108
		public ADVTextTagArgDB m_TextTagArgList;

		// Token: 0x0400006D RID: 109
		public Dictionary<string, int> m_CharaDBIndices;

		// Token: 0x0400006E RID: 110
		public Dictionary<string, int> m_MotionDBIndices;

		// Token: 0x0400006F RID: 111
		public Dictionary<string, int> m_EmotionDBIndices;

		// Token: 0x04000070 RID: 112
		public Dictionary<string, int> m_TextTagDBIndices;

		// Token: 0x04000071 RID: 113
		public bool m_IsDoneLoad;

		// Token: 0x04000072 RID: 114
		private bool m_IsError;

		// Token: 0x02000010 RID: 16
		private enum eADVDatabase
		{
			// Token: 0x04000074 RID: 116
			CharaList,
			// Token: 0x04000075 RID: 117
			EmotionList,
			// Token: 0x04000076 RID: 118
			MotionList,
			// Token: 0x04000077 RID: 119
			TextTagArg,
			// Token: 0x04000078 RID: 120
			Num
		}
	}
}
