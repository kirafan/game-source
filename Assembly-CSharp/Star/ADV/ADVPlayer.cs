﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000025 RID: 37
	public class ADVPlayer : MonoBehaviour
	{
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x000077CC File Offset: 0x00005BCC
		public PixelCrashWrapper pixelCrashWrapper
		{
			get
			{
				return this.m_PixelCrashWrapper;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x000077DD File Offset: 0x00005BDD
		// (set) Token: 0x060000E1 RID: 225 RVA: 0x000077D4 File Offset: 0x00005BD4
		private bool isWaitPixelCrashPrepare { get; set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x000077EE File Offset: 0x00005BEE
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x000077E5 File Offset: 0x00005BE5
		private bool isWaitPixelCrash { get; set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000E6 RID: 230 RVA: 0x000077FF File Offset: 0x00005BFF
		// (set) Token: 0x060000E5 RID: 229 RVA: 0x000077F6 File Offset: 0x00005BF6
		private bool isPlayingPixelCrash { get; set; }

		// Token: 0x060000E7 RID: 231 RVA: 0x00007807 File Offset: 0x00005C07
		public void WaitPixelCrashPrepare()
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			this.isWaitPixelCrashPrepare = true;
			this.BreakFlag = true;
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00007829 File Offset: 0x00005C29
		public void WaitPixelCrash()
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			this.isWaitPixelCrash = true;
			this.BreakFlag = true;
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x0000784C File Offset: 0x00005C4C
		public void StartPixelCrashPrepare(float sec)
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			this.m_PixelCrashPrepareTime = 0f;
			this.m_PixelCrashPrepareTimeMax = sec;
			this.m_PixelCrashWrapper.EnableRender(true);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UICamera != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled = false;
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x000078B0 File Offset: 0x00005CB0
		public void StartPixelCrash(float sec)
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			this.isPlayingPixelCrash = true;
			this.m_PixelCrashWrapper.m_Param.m_LifeSpanSec = sec;
			this.m_PixelCrashWrapper.Play(0f);
			this.m_PixelCrashWrapper.EnableRender(true);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UICamera != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled = false;
			}
		}

		// Token: 0x060000EB RID: 235 RVA: 0x0000792C File Offset: 0x00005D2C
		public void AddPicForPixelCrash(ADVStandPic pic)
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			this.m_StandPicForPixelCrash.Add(pic);
			pic.gameObject.SetLayer(LayerMask.NameToLayer("SaveArea"), true);
			pic.transform.SetParent(this.m_SaveAreasParent.transform, false);
			this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.r = 0f;
			this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.g = 0f;
			this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.b = 0f;
			this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.a = 1f;
			this.m_PixelCrashWrapper.SetBaseAddColor();
			this.m_PixelCrashWrapper.Prepare(0f);
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00007A08 File Offset: 0x00005E08
		public void ClearPicForPixelCrash()
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return;
			}
			for (int i = 0; i < this.m_StandPicForPixelCrash.Count; i++)
			{
				this.m_StandPicForPixelCrash[i].gameObject.SetLayer(LayerMask.NameToLayer("UI"), true);
				this.m_StandPicForPixelCrash[i].transform.SetParent(this.m_StandPicManager.transform, false);
			}
			this.m_StandPicForPixelCrash.Clear();
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00007A94 File Offset: 0x00005E94
		public bool UpdatePixelCrashAndCheckProgress()
		{
			if (this.m_PixelCrashWrapper == null)
			{
				return true;
			}
			if (this.m_PixelCrashPrepareTimeMax > 0f)
			{
				this.m_PixelCrashPrepareTime += Time.deltaTime / this.m_PixelCrashPrepareTimeMax;
				if (this.m_PixelCrashPrepareTime >= 1f)
				{
					this.m_PixelCrashPrepareTime = 1f;
					this.m_PixelCrashPrepareTimeMax = 0f;
				}
				for (int i = 0; i < this.m_StandPicForPixelCrash.Count; i++)
				{
					this.m_StandPicForPixelCrash[i].SetColor(Color.Lerp(this.m_StandPicForPixelCrash[i].GetColor(), Color.white, Mathf.Min(1f, this.m_PixelCrashPrepareTime * 1.5f)));
				}
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.r = this.m_PixelCrashPrepareTime * this.PixelCrashBaseColor.r;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.g = this.m_PixelCrashPrepareTime * this.PixelCrashBaseColor.g;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.b = this.m_PixelCrashPrepareTime * this.PixelCrashBaseColor.b;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.a = 1f;
				this.m_PixelCrashWrapper.SetBaseAddColor();
			}
			if (this.isPlayingPixelCrash && !this.m_PixelCrashWrapper.isAlive)
			{
				for (int j = 0; j < this.m_StandPicForPixelCrash.Count; j++)
				{
					this.m_StandPicForPixelCrash[j].SetEnableRender(false);
				}
				this.ClearPicForPixelCrash();
				this.m_PixelCrashWrapper.EnableRender(false);
				if (SingletonMonoBehaviour<GameSystem>.Inst.UICamera != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled = false;
				}
				this.isPlayingPixelCrash = false;
			}
			if (this.isWaitPixelCrashPrepare)
			{
				if (this.m_PixelCrashPrepareTime < 1f)
				{
					return false;
				}
				this.isWaitPixelCrashPrepare = false;
			}
			if (this.isWaitPixelCrash)
			{
				if (this.m_PixelCrashWrapper.isAlive)
				{
					return false;
				}
				this.isWaitPixelCrash = false;
			}
			return true;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000EE RID: 238 RVA: 0x00007CDE File Offset: 0x000060DE
		public bool IsDonePrepare
		{
			get
			{
				return this.m_IsDonePrepare;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000EF RID: 239 RVA: 0x00007CE6 File Offset: 0x000060E6
		public bool IsPlay
		{
			get
			{
				return this.m_IsPlay;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000F0 RID: 240 RVA: 0x00007CEE File Offset: 0x000060EE
		public bool IsEnd
		{
			get
			{
				return this.m_IsEnd;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x00007CF6 File Offset: 0x000060F6
		// (set) Token: 0x060000F2 RID: 242 RVA: 0x00007CFE File Offset: 0x000060FE
		public bool IsEnableAuto
		{
			get
			{
				return this.m_IsAuto;
			}
			set
			{
				this.EnableAuto(value, true);
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000F3 RID: 243 RVA: 0x00007D08 File Offset: 0x00006108
		public StandPicManager StandPicManager
		{
			get
			{
				return this.m_StandPicManager;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x00007D10 File Offset: 0x00006110
		public ADVEffectManager ADVEffectManager
		{
			get
			{
				return this.m_ADVEffectManager;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000F5 RID: 245 RVA: 0x00007D18 File Offset: 0x00006118
		public ADVSpriteManager SpriteManager
		{
			get
			{
				return this.m_SpriteManager;
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000F6 RID: 246 RVA: 0x00007D20 File Offset: 0x00006120
		public ADVBackGroundManager BackGroundManager
		{
			get
			{
				return this.m_BackGroundManager;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00007D28 File Offset: 0x00006128
		public ADVSoundManager SoundManager
		{
			get
			{
				return this.m_SoundManager;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00007D30 File Offset: 0x00006130
		public ScriptPlayer ScriptPlayer
		{
			get
			{
				return this.m_ScriptPlayer;
			}
		}

		// Token: 0x1700001F RID: 31
		// (set) Token: 0x060000F9 RID: 249 RVA: 0x00007D38 File Offset: 0x00006138
		public bool BreakFlag
		{
			set
			{
				this.m_BreakFlag = value;
			}
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00007D41 File Offset: 0x00006141
		public string GetTagCommand(ADVParser.EventType eventType)
		{
			if (this.m_Parser == null)
			{
				return null;
			}
			return this.m_Parser.GetTagCommand(eventType);
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00007D5C File Offset: 0x0000615C
		public int GetCommandsCount()
		{
			if (this.m_Parser == null)
			{
				return -1;
			}
			return this.m_Parser.GetCommandsCount();
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000FC RID: 252 RVA: 0x00007D76 File Offset: 0x00006176
		public int AdvID
		{
			get
			{
				return this.m_AdvID;
			}
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00007D7E File Offset: 0x0000617E
		private void OnDestroy()
		{
			this.m_PixelCrashWrapperPrefab = null;
			if (this.m_PixelCrashWrapper != null)
			{
				Helper.DestroyAll(this.m_PixelCrashWrapper.gameObject);
				this.m_PixelCrashWrapper = null;
			}
			this.ResetCamera();
			this.m_UI = null;
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00007DBC File Offset: 0x000061BC
		private void ResetCamera()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.UICamera != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled = true;
				}
				if (SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.enabled = false;
				}
			}
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00007E23 File Offset: 0x00006223
		public void PrepareDB()
		{
			this.m_DB = new ADVDataBase();
			this.m_DB.Init();
			this.m_DB.Prepare();
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00007E46 File Offset: 0x00006246
		public bool IsDonePrepareDB()
		{
			return this.m_DB.IsDoneLoad;
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00007E54 File Offset: 0x00006254
		public void Prepare(int advID)
		{
			this.m_AdvID = advID;
			ADVListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(this.m_AdvID);
			string scriptName = param.m_ScriptName;
			string scriptTextName = param.m_ScriptTextName;
			this.Prepare((eADVCategory)param.m_Category, scriptName, scriptTextName);
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00007EA2 File Offset: 0x000062A2
		public void Prepare(eADVCategory category, string scriptName, string textName)
		{
			this.Prepare();
			this.m_ScriptPlayer = new ScriptPlayer();
			this.m_ScriptPlayer.Player = this;
			this.m_ScriptPlayer.Prepare(category, scriptName, textName);
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00007ED0 File Offset: 0x000062D0
		public void Prepare()
		{
			this.m_UI.Setup(this.m_Parser);
			this.m_Parser.Setup(this, this.m_UI.talkWindow.Window.GetComponent<ADVTalkCustomWindow>());
			this.m_UI.fade.Set(Color.black);
			this.m_StandPicManager.Prepare(this);
			this.m_ADVEffectManager.Prepare(this);
			this.m_SpriteManager.Prepare(this);
			this.m_SoundManager.Prepare(this);
			this.m_IsDonePrepare = false;
			this.EnableAuto(false, false);
			this.m_WaitFade = false;
			this.m_WaitCharaFade = false;
			this.m_WaitMotion = false;
			this.m_WaitMove = false;
			this.m_WaitSE = false;
			this.m_WaitVOICE = false;
			this.m_WaitEffectId = null;
			this.m_WaitMoveCharaId = null;
			this.m_BreakFlag = false;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x00007FA4 File Offset: 0x000063A4
		public ADVCharacterListDB_Param GetADVCharacterParam(string ADVCharaID)
		{
			if (ADVCharaID == null || !this.m_DB.m_CharaDBIndices.ContainsKey(ADVCharaID))
			{
				return default(ADVCharacterListDB_Param);
			}
			return this.m_DB.m_CharaList.m_Params[this.m_DB.m_CharaDBIndices[ADVCharaID]];
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00008004 File Offset: 0x00006404
		public ADVMotionListDB_Param GetADVMotionParam(string ADVMotionID)
		{
			if (!this.m_DB.m_MotionDBIndices.ContainsKey(ADVMotionID))
			{
				return default(ADVMotionListDB_Param);
			}
			return this.m_DB.m_MotionList.m_Params[this.m_DB.m_MotionDBIndices[ADVMotionID]];
		}

		// Token: 0x06000106 RID: 262 RVA: 0x0000805C File Offset: 0x0000645C
		public ADVEmotionListDB_Param GetADVEmotionParam(string ADVEmotionID)
		{
			if (!this.m_DB.m_EmotionDBIndices.ContainsKey(ADVEmotionID))
			{
				return default(ADVEmotionListDB_Param);
			}
			return this.m_DB.m_EmotionList.m_Params[this.m_DB.m_EmotionDBIndices[ADVEmotionID]];
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000080B4 File Offset: 0x000064B4
		public ADVTextTagArgDB_Param GetADVTextTagParam(string ADVTextTagArgRefName)
		{
			if (!this.m_DB.m_TextTagDBIndices.ContainsKey(ADVTextTagArgRefName))
			{
				return default(ADVTextTagArgDB_Param);
			}
			return this.m_DB.m_TextTagArgList.m_Params[this.m_DB.m_TextTagDBIndices[ADVTextTagArgRefName]];
		}

		// Token: 0x06000108 RID: 264 RVA: 0x0000810C File Offset: 0x0000650C
		public ADVCharacterListDB_Data GetADVCharacterListDBData(string ADVCharaID, string poseName)
		{
			ADVCharacterListDB_Param advcharacterParam = this.GetADVCharacterParam(ADVCharaID);
			for (int i = 0; i < advcharacterParam.m_Datas.Length; i++)
			{
				if (advcharacterParam.m_Datas[i].m_PoseName == poseName)
				{
					return advcharacterParam.m_Datas[i];
				}
			}
			return default(ADVCharacterListDB_Data);
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00008174 File Offset: 0x00006574
		public int GetCurrentCommandIdx()
		{
			if (this.m_ScriptPlayer != null)
			{
				return this.m_ScriptPlayer.NowCommandIdx;
			}
			return -1;
		}

		// Token: 0x0600010A RID: 266 RVA: 0x0000818E File Offset: 0x0000658E
		public void ClearNeedStandPic()
		{
			this.m_StandPicManager.ClearNeedList();
		}

		// Token: 0x0600010B RID: 267 RVA: 0x0000819C File Offset: 0x0000659C
		public void AddNeedStandPic(int idx, string charaID, string poseName = null)
		{
			if (ADVUtility.IsEmptyADVCharaID(charaID))
			{
				return;
			}
			for (int i = 0; i < this.m_DB.m_CharaList.m_Params.Length; i++)
			{
				if (this.m_DB.m_CharaList.m_Params[i].m_ADVCharaID == charaID)
				{
					this.m_IsDonePrepare = false;
					if (string.IsNullOrEmpty(poseName))
					{
						poseName = this.m_DB.m_CharaList.m_Params[i].m_Datas[0].m_PoseName;
					}
					this.m_StandPicManager.AddNeedList(idx, charaID, poseName);
					return;
				}
			}
			Debug.Log("AddNeedStandPic() Invalid ADVCharaID : " + charaID);
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00008257 File Offset: 0x00006657
		public void DestroyAllNotNeedStandPics()
		{
			this.m_StandPicManager.DestroyAllNotNeedStandPic();
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00008264 File Offset: 0x00006664
		public void CreateAllNeedStandPics()
		{
			this.m_StandPicManager.CreateAllNeedStandPic();
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00008271 File Offset: 0x00006671
		public void LoadRecentNeedStandPics()
		{
			this.m_StandPicManager.LoadFirstNeedStandPics();
		}

		// Token: 0x0600010F RID: 271 RVA: 0x0000827E File Offset: 0x0000667E
		public void ClearNeedEffect()
		{
			this.m_ADVEffectManager.ClearNeedList();
		}

		// Token: 0x06000110 RID: 272 RVA: 0x0000828B File Offset: 0x0000668B
		public void AddNeedEffect(string effectID)
		{
			this.m_IsDonePrepare = false;
			this.m_ADVEffectManager.AddNeedList(effectID);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x000082A0 File Offset: 0x000066A0
		public void AddNeedEmo(string emotionID)
		{
			this.m_IsDonePrepare = false;
			this.m_ADVEffectManager.AddNeedListByEmotionID(emotionID);
		}

		// Token: 0x06000112 RID: 274 RVA: 0x000082B5 File Offset: 0x000066B5
		public void UnloadAllNotNeedEffect()
		{
			this.m_ADVEffectManager.UnloadAllNotNeedEffect();
		}

		// Token: 0x06000113 RID: 275 RVA: 0x000082C2 File Offset: 0x000066C2
		public void LoadAllNeedEffect()
		{
			this.m_ADVEffectManager.LoadAllNeedEffect();
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000082CF File Offset: 0x000066CF
		public void ClearNeedSprite()
		{
			this.m_SpriteManager.ClearNeedList();
		}

		// Token: 0x06000115 RID: 277 RVA: 0x000082DC File Offset: 0x000066DC
		public void AddNeedSprite(string fileName, int instId = -1, Image destination = null)
		{
			this.m_IsDonePrepare = false;
			this.m_SpriteManager.AddNeedList(fileName, instId, null);
		}

		// Token: 0x06000116 RID: 278 RVA: 0x000082F4 File Offset: 0x000066F4
		public void UnLoadAllNotNeedSprite()
		{
			this.m_SpriteManager.UnLoadAllNotNeedSprite();
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00008301 File Offset: 0x00006701
		public void LoadAllNeedSprite()
		{
			this.m_SpriteManager.LoadAllNeedSprite();
		}

		// Token: 0x06000118 RID: 280 RVA: 0x0000830E File Offset: 0x0000670E
		public void UpdatePrepareDB()
		{
			if (!this.m_DB.IsError)
			{
				this.m_DB.UpdatePrepare();
			}
		}

		// Token: 0x06000119 RID: 281 RVA: 0x00008334 File Offset: 0x00006734
		public void UpdatePrepare()
		{
			if (this.m_IsDonePrepare)
			{
				return;
			}
			if (this.m_ScriptPlayer != null && this.m_ScriptPlayer.IsError)
			{
				return;
			}
			this.m_SpriteManager.UpdateLoad();
			if (this.m_ScriptPlayer != null)
			{
				this.m_ScriptPlayer.UpdatePrepare();
			}
			if (this.m_StandPicManager.IsLoading())
			{
				return;
			}
			if (this.m_ScriptPlayer != null && !this.m_ScriptPlayer.IsDonePrepare)
			{
				return;
			}
			if (!this.m_ADVEffectManager.IsDonePrepare())
			{
				return;
			}
			if (!this.m_SpriteManager.IsDonePrepare)
			{
				return;
			}
			if (!this.m_BackGroundManager.IsDonePrepare())
			{
				return;
			}
			if (!this.m_SoundManager.IsDonePrepare())
			{
				return;
			}
			if (this.m_ScriptPlayer != null)
			{
				for (int i = 0; i < this.m_ScriptPlayer.CommandList.Count; i++)
				{
					if (this.m_ScriptPlayer.CommandList[i].param.FuncName.Contains("Warp"))
					{
						if (this.m_PixelCrashWrapper == null)
						{
							this.m_PixelCrashWrapper = UnityEngine.Object.Instantiate<PixelCrashWrapper>(this.m_PixelCrashWrapperPrefab);
						}
						this.m_PixelCrashWrapper.enabled = true;
						this.m_PixelCrashWrapper.m_RenderCamera = Camera.main;
						this.m_PixelCrashWrapper.m_CrashTargetCamera = SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera;
						this.m_PixelCrashWrapper.EnableRender(false);
						this.m_PixelCrashWrapper.Setup();
						this.m_PixelCrashWrapper.m_Param.m_HDRFactor = 1f;
						this.m_PixelCrashWrapper.m_Param.m_ChangeHDRFactor = 0.75f;
						this.m_PixelCrashWrapper.m_Param.m_BaseAddColor = new Color(0f, 0f, 0f, 1f);
						this.m_PixelCrashWrapper.m_Param.m_ShapeArgment = new Vector4(0.5f, 0f, 0f, 1f);
						this.m_PixelCrashWrapper.Prepare(0f);
						SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.orthographicSize = 0.5f;
						SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.enabled = true;
						break;
					}
				}
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x0600011A RID: 282 RVA: 0x0000857C File Offset: 0x0000697C
		public void Play()
		{
			if (!this.m_IsDonePrepare)
			{
				return;
			}
			this.m_IsPlay = true;
			if (this.m_ScriptPlayer != null)
			{
				this.m_StandPicManager.LoadFirstLoadStandPics(this.m_ScriptPlayer.NowCommandIdx);
			}
			if (this.m_ScriptPlayer != null)
			{
				this.m_ScriptPlayer.Play(0);
			}
		}

		// Token: 0x0600011B RID: 283 RVA: 0x000085D4 File Offset: 0x000069D4
		private void Update()
		{
			this.m_SoundManager.Update();
			if (this.m_IsPlay)
			{
				if (!this.m_IsDonePrepare)
				{
					this.UpdatePrepare();
					if (this.m_IsDonePrepare)
					{
						this.Play();
					}
				}
				else
				{
					this.UpdateWork();
					if (this.m_IsAuto && this.m_NovelTextState == ADVPlayer.eNovelTextState.TapWait && this.m_UI.TextMode == ADVUI.eTextMode.Novel)
					{
						this.m_AutoWaitCount += Time.deltaTime;
						if (this.m_AutoWait < this.m_AutoWaitCount)
						{
							this.FlushText();
							this.m_AutoWaitCount = 0f;
						}
					}
					if (this.m_UI != null && this.m_UI.talkWindow != null)
					{
						this.m_UI.talkWindow.ManualUpdate2();
					}
				}
			}
		}

		// Token: 0x0600011C RID: 284 RVA: 0x000086B8 File Offset: 0x00006AB8
		private void UpdateWork()
		{
			if (this.m_WaitFade)
			{
				if (this.m_UI.fade.IsFade)
				{
					return;
				}
				this.m_WaitFade = false;
			}
			if (this.m_WaitCharaFade)
			{
				if (this.m_StandPicManager.IsFading())
				{
					return;
				}
				this.m_WaitCharaFade = false;
			}
			if (this.m_WaitMotion)
			{
				if (this.m_StandPicManager.IsPlayingMotion)
				{
					return;
				}
				this.m_WaitMotion = false;
			}
			if (this.m_WaitMove)
			{
				if (this.m_StandPicManager.IsMoving)
				{
					return;
				}
				this.m_WaitMove = false;
			}
			if (this.m_WaitVOICE)
			{
				if (this.m_SoundManager.IsPlayingVOICE())
				{
					return;
				}
				this.m_WaitVOICE = false;
			}
			if (this.m_WaitEffectId != null)
			{
				if (!this.ADVEffectManager.IsDonePlayingAllFromEffectIdIgnoreLoop(this.m_WaitEffectId))
				{
					return;
				}
				this.m_WaitEffectId = null;
			}
			if (this.m_WaitMoveCharaId != null)
			{
				if (this.m_StandPicManager.IsMovingChara(this.m_WaitMoveCharaId))
				{
					return;
				}
				this.m_WaitMoveCharaId = null;
			}
			if (this.m_WaitSE)
			{
				if (this.m_SoundManager.IsPlayingSE())
				{
					return;
				}
				this.m_WaitSE = false;
			}
			if (!this.UpdatePixelCrashAndCheckProgress())
			{
				return;
			}
			if (this.m_WaitTimer > 0f)
			{
				this.m_WaitTimer -= Time.deltaTime;
				if (this.m_WaitTimer < 0f)
				{
					this.SetWait(0f);
				}
				return;
			}
			if (!this.m_StandPicManager.IsDoneApplyPose())
			{
				return;
			}
			if (this.m_UI.TextMode == ADVUI.eTextMode.TalkWindow)
			{
				if (this.m_UI.talkWindow.TextState != ADVTalkCustomWindow.eTextState.None)
				{
					ADVTalkCustomWindow.eTextState textState = this.m_UI.talkWindow.TextState;
					if (textState != ADVTalkCustomWindow.eTextState.StandBy)
					{
						if (textState == ADVTalkCustomWindow.eTextState.Progress)
						{
							this.m_UI.talkWindow.ManualUpdate();
						}
					}
					else
					{
						this.m_UI.talkWindow.StartDisplayText();
						this.m_NovelTextState = ADVPlayer.eNovelTextState.Progress;
					}
					return;
				}
			}
			else if (this.m_NovelTextState != ADVPlayer.eNovelTextState.None)
			{
				if (this.m_NovelTextState == ADVPlayer.eNovelTextState.Progress)
				{
					int i;
					if (this.m_TextProgressPerSec <= 0f || this.m_StrongTextFlag)
					{
						i = this.m_TalkText.Length;
					}
					else
					{
						this.m_TextCountTimer = (float)this.m_TextCount + (this.m_TextCountTimer - (float)((int)this.m_TextCountTimer));
						if (this.m_TextProgressPerSec > 0f)
						{
							this.m_TextCountTimer += Time.deltaTime * this.m_TextProgressPerSec;
						}
						i = (int)this.m_TextCountTimer;
						if (i > this.m_TalkText.Length)
						{
							i = this.m_TalkText.Length;
						}
					}
					if (i < 0)
					{
						i = 0;
					}
					while (i > this.m_TextCount)
					{
						bool flag = false;
						this.m_RubyList.Clear();
						string text = this.m_Parser.Parse(ref this.m_TalkText, ref this.m_TextCount, ref this.m_msgEventList, true, true, out flag, this.m_UI.novel.LastCaption.CalcTextObj, ref this.m_RubyList);
						this.m_UI.novel.SetText(text);
						for (int j = 0; j < this.m_RubyList.Count; j++)
						{
							this.SetRubyNovel(this.m_RubyList[j]);
						}
						if (this.m_TextProgressPerSec <= 0f || this.m_StrongTextFlag)
						{
							i = this.m_TalkText.Length;
						}
						else if (i > this.m_TalkText.Length)
						{
							i = this.m_TalkText.Length;
						}
						if (i < 0)
						{
							i = 0;
						}
						if (this.m_UI.TextMode == ADVUI.eTextMode.Novel && this.m_TextCount >= this.m_TalkText.Length)
						{
							this.m_NovelTextState = ADVPlayer.eNovelTextState.TapWait;
							break;
						}
						if (flag && this.m_TextProgressPerSec > 0f)
						{
							this.m_TextCountTimer = (float)this.m_TextCount;
							break;
						}
					}
				}
				return;
			}
			this.m_BreakFlag = false;
			while (!this.m_BreakFlag)
			{
				if (this.m_ScriptPlayer != null)
				{
					if (this.m_ScriptPlayer.IsEnd)
					{
						this.m_IsEnd = true;
						this.ResetCamera();
						return;
					}
					this.m_ScriptPlayer.DoCommand();
				}
			}
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00008B40 File Offset: 0x00006F40
		public void Destroy()
		{
			this.m_DB.Destroy();
			this.m_ADVEffectManager.DestroyAllEffect();
			this.m_StandPicManager.Destroy();
			if (this.m_ScriptPlayer != null)
			{
				this.m_ScriptPlayer.Destroy();
			}
			this.m_SpriteManager.Destroy();
			this.m_SoundManager.UnloadCueSheet();
			this.m_UI.Destroy();
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00008BA5 File Offset: 0x00006FA5
		public bool UpdateDestroy()
		{
			return this.m_DB.UpdateDestroy() && this.m_ScriptPlayer.UpdateDestroy();
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00008BC8 File Offset: 0x00006FC8
		public void Tap()
		{
			if (!this.m_IsPlay || this.m_IsEnd)
			{
				return;
			}
			if (!this.m_ShowUI)
			{
				this.ToggleShowUI();
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ADV_TEXT, 1f, 0, -1, -1);
			this.m_UI.talkWindow.OnClickScreenPenProcess();
			if (this.m_UI.TextMode == ADVUI.eTextMode.Novel)
			{
				if (this.m_IsAuto)
				{
					this.EnableAuto(false, true);
					return;
				}
				ADVPlayer.eNovelTextState novelTextState = this.m_NovelTextState;
				if (novelTextState != ADVPlayer.eNovelTextState.Progress)
				{
					if (novelTextState == ADVPlayer.eNovelTextState.TapWait)
					{
						if (this.m_UI.systemPanel.IsMenuOpen())
						{
							this.m_UI.systemPanel.Toggle();
						}
						this.m_Parser.SetTapWaitWithTapFlag(false);
						this.FlushText();
					}
				}
				else
				{
					this.m_Parser.SetTapWaitWithTapFlag(true);
					this.SkipTextProgress();
					this.m_NovelTextState = ADVPlayer.eNovelTextState.TapWait;
				}
			}
			else
			{
				bool isEnableAuto = this.m_UI.talkWindow.IsEnableAuto;
				this.m_UI.talkWindow.OnClickScreenTalkWindowProcess();
				if (isEnableAuto && !this.m_UI.talkWindow.IsEnableAuto)
				{
					this.m_IsAuto = false;
					this.m_AutoWaitCount = 0f;
					this.m_UI.EnableMenuPanelAutoAnimation(false, true);
				}
				ADVTalkCustomWindow.eTextState textState = this.m_UI.talkWindow.TextState;
				if (textState != ADVTalkCustomWindow.eTextState.TapWait)
				{
					if (textState == ADVTalkCustomWindow.eTextState.None)
					{
						if (this.m_UI.systemPanel.IsMenuOpen())
						{
							this.m_UI.systemPanel.Toggle();
						}
						this.m_NovelTextState = ADVPlayer.eNovelTextState.None;
					}
				}
				else
				{
					this.m_NovelTextState = ADVPlayer.eNovelTextState.TapWait;
				}
			}
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00008D90 File Offset: 0x00007190
		private void ParseTalker(string charaName, out string talkerCharaID, out string talkerName)
		{
			talkerCharaID = null;
			talkerName = string.Empty;
			int num = charaName.IndexOf('$');
			if (num != -1)
			{
				talkerCharaID = charaName.Substring(num + 1);
				talkerName = charaName.Remove(num);
			}
			else
			{
				talkerCharaID = charaName;
				ADVCharacterListDB_Param advcharacterParam = this.GetADVCharacterParam(talkerCharaID);
				if (advcharacterParam.m_DisplayName != null)
				{
					talkerName = advcharacterParam.m_DisplayName;
				}
				else
				{
					talkerCharaID = null;
					talkerName = charaName;
				}
			}
		}

		// Token: 0x06000121 RID: 289 RVA: 0x00008DFC File Offset: 0x000071FC
		private void FlushText()
		{
			this.AddBackLogText();
			this.m_NovelTextState = ADVPlayer.eNovelTextState.None;
			if (this.m_UI.TextMode == ADVUI.eTextMode.Novel)
			{
				this.m_TalkText = string.Empty;
				this.m_TalkerText = string.Empty;
				this.m_TalkerCharaID = string.Empty;
			}
		}

		// Token: 0x06000122 RID: 290 RVA: 0x00008E50 File Offset: 0x00007250
		public void AddBackLogText()
		{
			List<ADVParser.RubyData> list = new List<ADVParser.RubyData>();
			List<ADVParser.RubyData> rubyList = this.m_UI.talkWindow.GetRubyList();
			for (int i = 0; i < rubyList.Count; i++)
			{
				list.Add(rubyList[i]);
			}
			string text;
			if (this.m_UI.TextMode == ADVUI.eTextMode.Novel)
			{
				text = this.m_TalkText;
			}
			else
			{
				text = this.m_UI.talkWindow.GetNowDisplayMessage();
			}
			this.AddBackLogText(this.m_TalkerCharaID, this.m_TalkerText, text, list);
		}

		// Token: 0x06000123 RID: 291 RVA: 0x00008EDA File Offset: 0x000072DA
		private void AddBackLogText(string charaID, string talker, string text, List<ADVParser.RubyData> rubyList)
		{
			this.m_UI.AddBackLogText(charaID, talker, text, rubyList);
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00008EEC File Offset: 0x000072EC
		public void CreateSkipLogText()
		{
			List<int> skipLogTextList = this.m_ScriptPlayer.GetSkipLogTextList();
			for (int i = 0; i < skipLogTextList.Count; i++)
			{
				string text = string.Empty;
				string charaName = string.Empty;
				string empty = string.Empty;
				string empty2 = string.Empty;
				for (int j = 0; j < this.m_ScriptPlayer.TextDB.m_Params.Length; j++)
				{
					if ((ulong)this.m_ScriptPlayer.TextDB.m_Params[j].m_id == (ulong)((long)skipLogTextList[i]))
					{
						text = this.m_ScriptPlayer.TextDB.m_Params[j].m_text;
						charaName = this.m_ScriptPlayer.TextDB.m_Params[j].m_charaName;
						break;
					}
				}
				this.ParseTalker(charaName, out empty2, out empty);
				List<ADVParser.RubyData> rubyList = new List<ADVParser.RubyData>();
				string text2 = this.m_Parser.ParseAll(text, false, false, this.m_UI.GetBackLogCalcText(), ref rubyList);
				this.m_UI.AddSkipLogText(empty2, empty, text2, rubyList);
			}
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00009008 File Offset: 0x00007408
		public void SkipScript()
		{
			this.m_IsEnd = true;
			this.m_IsPlay = false;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UICamera != null && !SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UICamera.enabled = true;
			}
		}

		// Token: 0x06000126 RID: 294 RVA: 0x0000905C File Offset: 0x0000745C
		public void EnableAuto(bool flag, bool anim = false)
		{
			Debug.Log("EnableAuto" + flag);
			this.m_IsAuto = flag;
			this.m_AutoWaitCount = 0f;
			if (this.m_UI != null)
			{
				this.m_UI.EnableAuto(flag, anim);
			}
		}

		// Token: 0x06000127 RID: 295 RVA: 0x000090AE File Offset: 0x000074AE
		public void ToggleShowUI()
		{
			this.m_ShowUI = !this.m_ShowUI;
			this.m_UI.IsEnableRender = this.m_ShowUI;
			this.EnableAuto(false, false);
		}

		// Token: 0x06000128 RID: 296 RVA: 0x000090D8 File Offset: 0x000074D8
		public void SetWait(float time)
		{
			this.m_WaitTimer = time;
		}

		// Token: 0x06000129 RID: 297 RVA: 0x000090E1 File Offset: 0x000074E1
		public void GoTo(int scriptId)
		{
			this.m_ScriptPlayer.Goto(scriptId);
		}

		// Token: 0x0600012A RID: 298 RVA: 0x000090EF File Offset: 0x000074EF
		public void FillScreen(Color rgba)
		{
			this.m_UI.fade.Set(rgba);
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00009102 File Offset: 0x00007502
		public void Fade(Color rgba, float time)
		{
			this.m_UI.fade.Fade(rgba, time);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00009116 File Offset: 0x00007516
		public void WaitFade()
		{
			this.m_WaitFade = true;
		}

		// Token: 0x0600012D RID: 301 RVA: 0x0000911F File Offset: 0x0000751F
		public void WaitCharaFade()
		{
			this.m_WaitCharaFade = true;
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00009128 File Offset: 0x00007528
		public void WaitMotion()
		{
			this.m_WaitMotion = true;
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00009131 File Offset: 0x00007531
		public void WaitMove()
		{
			this.m_WaitMove = true;
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0000913A File Offset: 0x0000753A
		public void WaitSE()
		{
			this.m_WaitSE = true;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00009143 File Offset: 0x00007543
		public void WaitVOICE()
		{
			this.m_WaitVOICE = true;
		}

		// Token: 0x06000132 RID: 306 RVA: 0x0000914C File Offset: 0x0000754C
		public void WaitEffect(string effectId)
		{
			this.m_WaitEffectId = effectId;
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00009158 File Offset: 0x00007558
		public void WaitEmotion(string emotionId)
		{
			this.m_WaitEffectId = this.GetADVEmotionParam(emotionId).m_EffectID;
		}

		// Token: 0x06000134 RID: 308 RVA: 0x0000917A File Offset: 0x0000757A
		public void WaitCharaMove(string charaId)
		{
			this.m_WaitMoveCharaId = charaId;
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00009184 File Offset: 0x00007584
		public string GetTalkerADVCharaID(uint m_TextID)
		{
			int num = this.m_ScriptPlayer.TextDB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_ScriptPlayer.TextDB.m_Params[i].m_id == m_TextID)
				{
					this.m_TextCountTimer = 0f;
					this.m_TextCount = 0;
					this.m_TalkText = this.m_ScriptPlayer.TextDB.m_Params[i].m_text;
					string charaName = this.m_ScriptPlayer.TextDB.m_Params[i].m_charaName;
					this.m_TalkerCharaID = charaName;
					this.m_TalkerText = charaName;
					this.ParseTalker(charaName, out this.m_TalkerCharaID, out this.m_TalkerText);
					return this.m_TalkerCharaID;
				}
			}
			return null;
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000924F File Offset: 0x0000764F
		public void CharaHighlight(string m_ADVCharaID)
		{
			this.m_StandPicManager.Highlight(m_ADVCharaID);
		}

		// Token: 0x06000137 RID: 311 RVA: 0x0000925D File Offset: 0x0000765D
		public void CharaHighlightAll()
		{
			this.m_StandPicManager.HighlightAll();
		}

		// Token: 0x06000138 RID: 312 RVA: 0x0000926A File Offset: 0x0000766A
		public void CharaHighlightDefault(string m_ADVCharaID)
		{
			this.m_StandPicManager.HighlightDefault(m_ADVCharaID);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00009278 File Offset: 0x00007678
		public void CharaHighlightDefaultAll()
		{
			this.m_StandPicManager.HighlightDefaultAll();
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00009285 File Offset: 0x00007685
		public void CharaShading(string m_ADVCharaID)
		{
			this.m_StandPicManager.Shading(m_ADVCharaID);
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00009293 File Offset: 0x00007693
		public void CharaShadingAll()
		{
			this.m_StandPicManager.ShadingAll();
		}

		// Token: 0x0600013C RID: 316 RVA: 0x000092A0 File Offset: 0x000076A0
		public void CharaHighlightTalker(string m_ADVCharaID)
		{
			this.m_StandPicManager.AddNextTalkTalkerHighlightCharacter(m_ADVCharaID);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000092AE File Offset: 0x000076AE
		public void CharaHighlightTalkerResetAll()
		{
			this.m_StandPicManager.HighlightTalkerResetAll();
		}

		// Token: 0x0600013E RID: 318 RVA: 0x000092BC File Offset: 0x000076BC
		public void CharaTalk(uint m_TextID)
		{
			int num = this.m_ScriptPlayer.TextDB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_ScriptPlayer.TextDB.m_Params[i].m_id == m_TextID)
				{
					this.m_TextCountTimer = 0f;
					this.m_TextCount = 0;
					this.m_TalkText = this.m_ScriptPlayer.TextDB.m_Params[i].m_text;
					string charaName = this.m_ScriptPlayer.TextDB.m_Params[i].m_charaName;
					this.m_TalkerCharaID = charaName;
					this.m_TalkerText = charaName;
					this.ParseTalker(charaName, out this.m_TalkerCharaID, out this.m_TalkerText);
					ADVCharacterListDB_Param advcharacterParam = this.GetADVCharacterParam(this.m_TalkerCharaID);
					if (advcharacterParam.m_NamedType != -1)
					{
						this.m_SoundManager.PlayVOICE((eCharaNamedType)advcharacterParam.m_NamedType, this.m_ScriptPlayer.TextDB.m_Params[i].m_voiceLabel);
					}
					else if (!string.IsNullOrEmpty(advcharacterParam.m_CueSheet))
					{
						this.m_SoundManager.PlayVOICE(advcharacterParam.m_CueSheet, this.m_ScriptPlayer.TextDB.m_Params[i].m_voiceLabel);
					}
					this.m_StandPicManager.SetAsLastSibling(this.m_TalkerCharaID);
					this.m_StandPicManager.HighlightTalker(this.m_TalkerCharaID);
					this.m_UI.SetTextMode(ADVUI.eTextMode.TalkWindow);
					this.m_UI.talkWindow.SetTalker(this.m_TalkerText);
					this.m_UI.talkWindow.SetText(this.m_TalkText, ADVTalkCustomWindow.eTextState.Start);
					this.m_UI.talkWindow.Open();
					this.m_NovelTextState = ADVPlayer.eNovelTextState.Progress;
					return;
				}
			}
		}

		// Token: 0x0600013F RID: 319 RVA: 0x0000947C File Offset: 0x0000787C
		public void OpenTalk()
		{
			this.m_UI.talkWindow.Open();
		}

		// Token: 0x06000140 RID: 320 RVA: 0x0000948E File Offset: 0x0000788E
		public void CloseTalk()
		{
			this.m_UI.talkWindow.Close();
		}

		// Token: 0x06000141 RID: 321 RVA: 0x000094A0 File Offset: 0x000078A0
		public void AddNovelText(uint m_TextID)
		{
			int num = this.m_ScriptPlayer.TextDB.m_Params.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_ScriptPlayer.TextDB.m_Params[i].m_id == m_TextID)
				{
					this.m_TextCountTimer = 0f;
					this.m_TextCount = 0;
					this.m_TalkText = this.m_ScriptPlayer.TextDB.m_Params[i].m_text;
					string charaName = this.m_ScriptPlayer.TextDB.m_Params[i].m_charaName;
					this.m_TalkerCharaID = charaName;
					this.m_TalkerText = charaName;
					this.ParseTalker(charaName, out this.m_TalkerCharaID, out this.m_TalkerText);
					this.m_StandPicManager.SetAsLastSibling(this.m_TalkerCharaID);
					this.m_UI.novel.Open();
					this.m_UI.SetTextMode(ADVUI.eTextMode.Novel);
					this.m_UI.novel.AddText();
					string text = this.m_ScriptPlayer.TextDB.m_Params[i].m_text;
					this.m_RubyList.Clear();
					string text2 = this.m_Parser.ParseAll(text, false, false, null, ref this.m_RubyList);
					this.m_UI.novel.LastCaption.CalcSize(text2);
					this.m_NovelTextState = ADVPlayer.eNovelTextState.Progress;
					return;
				}
			}
		}

		// Token: 0x06000142 RID: 322 RVA: 0x000095FE File Offset: 0x000079FE
		public void NovelInsertLine()
		{
			this.m_UI.novel.AddText();
		}

		// Token: 0x06000143 RID: 323 RVA: 0x00009611 File Offset: 0x00007A11
		public void ClearNovelText()
		{
			this.m_UI.novel.ClearText();
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00009623 File Offset: 0x00007A23
		public void SetNovelAnchor(eADVAnchor anchor, Vector2 pos)
		{
			this.m_UI.novel.SetAnchorPosition(anchor, pos);
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00009637 File Offset: 0x00007A37
		public void SetNovelAnchorDefault()
		{
			this.m_UI.novel.SetAnchorPositionDefault();
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00009649 File Offset: 0x00007A49
		public void CloseNovel()
		{
			this.m_UI.novel.Close();
		}

		// Token: 0x06000147 RID: 327 RVA: 0x0000965C File Offset: 0x00007A5C
		private void SkipTextProgress()
		{
			if (this.m_UI.TextMode == ADVUI.eTextMode.TalkWindow)
			{
				if (this.m_UI.talkWindow.TextState == ADVTalkCustomWindow.eTextState.Progress)
				{
					this.m_UI.talkWindow.SkipTextProgress();
				}
			}
			else
			{
				while (this.m_TextCount < this.m_TalkText.Length)
				{
					bool flag = false;
					this.m_RubyList.Clear();
					if (this.m_UI.TextMode != ADVUI.eTextMode.TalkWindow)
					{
						string text = this.m_Parser.Parse(ref this.m_TalkText, ref this.m_TextCount, ref this.m_msgEventList, true, true, out flag, this.m_UI.novel.LastCaption.CalcTextObj, ref this.m_RubyList);
						this.m_UI.novel.SetText(text);
						for (int i = 0; i < this.m_RubyList.Count; i++)
						{
							this.SetRubyNovel(this.m_RubyList[i]);
						}
					}
				}
			}
			this.SetWait(0f);
			this.m_NovelTextState = ADVPlayer.eNovelTextState.TapWait;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x0000976F File Offset: 0x00007B6F
		public void SetRuby(ADVParser.RubyData rubyData)
		{
			this.m_UI.talkWindow.AddRuby(rubyData);
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00009782 File Offset: 0x00007B82
		public void SetRubyNovel(ADVParser.RubyData rubyData)
		{
			this.m_UI.novel.LastCaption.AddRuby(rubyData);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x0000979C File Offset: 0x00007B9C
		public void PlayEffect(string effectID, eADVStandPosition standPosition, float offsetX, float offsetY, float rotate, bool loop = false)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			vector.y += this.StandPicManager.CanvasRectTransform.GetComponent<CanvasScaler>().referenceResolution.y * 0.5f;
			this.m_ADVEffectManager.Play(effectID, this.m_StandPicManager.ConvertPositionTo3D(ADVUtility.GetStandPosition(standPosition) + new Vector3(vector.x, vector.y + this.m_StandPicManager.RectTransform.anchoredPosition.y)), Quaternion.Euler(0f, 180f, rotate), loop);
		}

		// Token: 0x0600014B RID: 331 RVA: 0x0000984C File Offset: 0x00007C4C
		public void EffectLoopTarget(string effectID, string afterEffectID, eADVStandPosition standPosition, float offsetX, float offsetY, float rotate)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			vector.y += this.StandPicManager.CanvasRectTransform.GetComponent<CanvasScaler>().referenceResolution.y * 0.5f;
			Vector3 position = this.m_StandPicManager.ConvertPositionTo3D(ADVUtility.GetStandPosition(standPosition) + new Vector3(vector.x, vector.y + this.m_StandPicManager.RectTransform.anchoredPosition.y));
			Quaternion rotation = Quaternion.Euler(0f, 180f, rotate);
			this.m_ADVEffectManager.PlayAndReserveNextEffect(new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Normal, -1, effectID, position, rotation, WrapMode.ClampForever), new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Normal, -1, afterEffectID, position, rotation, WrapMode.Loop));
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00009914 File Offset: 0x00007D14
		public void PlayEffectPreset(eADVStandPosition standPosition, float offsetX, float offsetY, float rotate)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			vector.y += this.StandPicManager.CanvasRectTransform.GetComponent<CanvasScaler>().referenceResolution.y * 0.5f;
			this.m_ADVEffectManager.PlayPreset(this.m_StandPicManager.ConvertPositionTo3D(ADVUtility.GetStandPosition(standPosition) + new Vector3(vector.x, vector.y + this.m_StandPicManager.RectTransform.anchoredPosition.y)), Quaternion.Euler(0f, 180f, rotate));
		}

		// Token: 0x0600014D RID: 333 RVA: 0x000099C0 File Offset: 0x00007DC0
		public void PlayEffect(string effectID, string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate, bool loop = false)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			if (charaAnchor != eADVCharaAnchor.Face)
			{
				if (charaAnchor != eADVCharaAnchor.Center)
				{
					if (charaAnchor == eADVCharaAnchor.Bottom)
					{
						vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFootPosition();
					}
				}
				else
				{
					vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaPosition();
				}
			}
			else
			{
				vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFacePosition();
			}
			this.m_ADVEffectManager.Play(effectID, this.m_StandPicManager.ConvertPositionTo3D(vector), Quaternion.Euler(0f, 180f, rotate), loop);
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00009A74 File Offset: 0x00007E74
		public void EffectCharaLoopTarget(string effectID, string afterEffectID, string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			if (charaAnchor != eADVCharaAnchor.Face)
			{
				if (charaAnchor != eADVCharaAnchor.Center)
				{
					if (charaAnchor == eADVCharaAnchor.Bottom)
					{
						vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFootPosition();
					}
				}
				else
				{
					vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaPosition();
				}
			}
			else
			{
				vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFacePosition();
			}
			Vector3 position = this.m_StandPicManager.ConvertPositionTo3D(vector);
			Quaternion rotation = Quaternion.Euler(0f, 180f, rotate);
			this.m_ADVEffectManager.PlayAndReserveNextEffect(new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Normal, -1, effectID, position, rotation, WrapMode.ClampForever), new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Normal, -1, afterEffectID, position, rotation, WrapMode.Loop));
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00009B40 File Offset: 0x00007F40
		public void PlayEffectPreset(string ADVCharaID, eADVCharaAnchor charaAnchor, float offsetX, float offsetY, float rotate)
		{
			Vector2 vector = new Vector2(offsetX, offsetY);
			if (charaAnchor != eADVCharaAnchor.Face)
			{
				if (charaAnchor != eADVCharaAnchor.Center)
				{
					if (charaAnchor == eADVCharaAnchor.Bottom)
					{
						vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFootPosition();
					}
				}
				else
				{
					vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaPosition();
				}
			}
			else
			{
				vector += this.m_StandPicManager.GetStandPic(ADVCharaID).GetCharaFacePosition();
			}
			this.m_ADVEffectManager.PlayPreset(this.m_StandPicManager.ConvertPositionTo3D(vector), Quaternion.Euler(0f, 180f, rotate));
		}

		// Token: 0x06000150 RID: 336 RVA: 0x00009BF0 File Offset: 0x00007FF0
		public void PlayEffectScreen(string effectID, eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate, bool loop = false)
		{
			Vector2 pos = new Vector2(offsetX, offsetY);
			if (screenAnchor != eADVScreenAnchor.Middle)
			{
				if (screenAnchor != eADVScreenAnchor.Lower)
				{
					if (screenAnchor == eADVScreenAnchor.Upper)
					{
						pos.y += this.m_UI.RectTransform.rect.height * 0.5f;
					}
				}
				else
				{
					pos.y -= this.m_UI.RectTransform.rect.height * 0.5f;
				}
			}
			this.m_ADVEffectManager.PlayScreen(effectID, this.m_UI.ConvertPositionTo3D(pos), Quaternion.Euler(0f, 180f, rotate), loop);
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00009CB4 File Offset: 0x000080B4
		public void EffectScreenLoopTarget(string effectID, string afterEffectID, eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate, bool loop = false)
		{
			Vector2 pos = new Vector2(offsetX, offsetY);
			if (screenAnchor != eADVScreenAnchor.Middle)
			{
				if (screenAnchor != eADVScreenAnchor.Lower)
				{
					if (screenAnchor == eADVScreenAnchor.Upper)
					{
						pos.y += this.m_UI.RectTransform.rect.height * 0.5f;
					}
				}
				else
				{
					pos.y -= this.m_UI.RectTransform.rect.height * 0.5f;
				}
			}
			Vector3 position = this.m_UI.ConvertPositionTo3D(pos);
			Quaternion rotation = Quaternion.Euler(0f, 180f, rotate);
			this.m_ADVEffectManager.PlayAndReserveNextEffect(new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Screen, -1, effectID, position, rotation, WrapMode.ClampForever), new ADVEffectManager.PlayProcessArgument(ADVEffectManager.ePlayType.Screen, -1, afterEffectID, position, rotation, WrapMode.Loop));
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00009D90 File Offset: 0x00008190
		public void PlayEffectScreenPreset(eADVScreenAnchor screenAnchor, float offsetX, float offsetY, float rotate)
		{
			Vector2 pos = new Vector2(offsetX, offsetY);
			if (screenAnchor != eADVScreenAnchor.Middle)
			{
				if (screenAnchor != eADVScreenAnchor.Lower)
				{
					if (screenAnchor == eADVScreenAnchor.Upper)
					{
						pos.y += this.m_UI.RectTransform.rect.height * 0.5f;
					}
				}
				else
				{
					pos.y -= this.m_UI.RectTransform.rect.height * 0.5f;
				}
			}
			this.m_ADVEffectManager.PlayPresetScreen(this.m_UI.ConvertPositionTo3D(pos), Quaternion.Euler(0f, 180f, rotate));
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00009E50 File Offset: 0x00008250
		public void PlayEmotion(string charaID, string emotionID, eADVEmotionPosition position)
		{
			if (position == eADVEmotionPosition.CharaDefault)
			{
				position = (eADVEmotionPosition)this.GetADVEmotionParam(emotionID).m_DefaultPosition;
			}
			Vector3 position2 = this.m_StandPicManager.GetCharaEmoPosition(charaID, position);
			position2.z = 0f;
			bool loop = false;
			if ((int)this.GetADVEmotionParam(emotionID).m_Loop > 0)
			{
				loop = true;
			}
			this.m_ADVEffectManager.PlayEmo(emotionID, position2, Quaternion.Euler(0f, 180f, 0f), loop);
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00009ECF File Offset: 0x000082CF
		public void ShakeAll(eADVShakeType type, float time)
		{
			this.UpdateShakeAllRandomParameter();
			this.ShakeChara(type, time, true);
			this.ShakeBG(type, time, true);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00009EEC File Offset: 0x000082EC
		public void ShakeChara(eADVShakeType type, float time, bool isAllShake = false)
		{
			this.UpdateCharaRandomParameter();
			if (!isAllShake)
			{
				this.m_ShakeBG.CancelSyncShakeAll();
			}
			for (int i = 0; i < this.m_OnlyCharaShakes.Count; i++)
			{
				this.m_OnlyCharaShakes[i].StopShake();
			}
			this.m_OnlyCharaShakes.Clear();
			this.Shake(this.m_ShakeChara, isAllShake, true, type, time);
			this.Shake(this.m_ShakeSpr, isAllShake, true, type, time);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00009F68 File Offset: 0x00008368
		public void ShakeOnlyChara(ADVShake target, eADVShakeType type, float time)
		{
			this.UpdateCharaRandomParameter();
			this.m_ShakeBG.CancelSyncShakeAll();
			this.m_ShakeChara.StopShake();
			this.m_ShakeSpr.StopShake();
			this.m_OnlyCharaShakes.Add(target);
			this.Shake(target, false, false, type, time);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00009FA8 File Offset: 0x000083A8
		public void ShakeBG(eADVShakeType type, float time, bool isAllShake = false)
		{
			if (!isAllShake)
			{
				this.m_ShakeChara.CancelSyncShakeAll();
				this.m_ShakeSpr.CancelSyncShakeAll();
			}
			this.Shake(this.m_ShakeBG, isAllShake, false, type, time);
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00009FD8 File Offset: 0x000083D8
		public void Shake(ADVShake target, bool isAllShake, bool isTargetCharaShake, eADVShakeType type, float time)
		{
			switch (type)
			{
			case eADVShakeType.RandomS:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Random, 2f, ADVShake.eShakeType.Random, 2f, 0.01f, time);
				break;
			case eADVShakeType.RandomM:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Random, 7f, ADVShake.eShakeType.Random, 7f, 0.01f, time);
				break;
			case eADVShakeType.RandomL:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Random, 20f, ADVShake.eShakeType.Random, 20f, 0.01f, time);
				break;
			case eADVShakeType.HorizontalS:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Round, 2f, ADVShake.eShakeType.None, 0f, 0.02f, time);
				break;
			case eADVShakeType.HorizontalM:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Round, 7f, ADVShake.eShakeType.None, 0f, 0.02f, time);
				break;
			case eADVShakeType.HorizontalL:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Round, 20f, ADVShake.eShakeType.None, 0f, 0.02f, time);
				break;
			case eADVShakeType.VerticalS:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.None, 0f, ADVShake.eShakeType.Round, 2f, 0.02f, time);
				break;
			case eADVShakeType.VerticalM:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.None, 0f, ADVShake.eShakeType.Round, 7f, 0.02f, time);
				break;
			case eADVShakeType.VerticalL:
				target.Shake(this, isAllShake, isTargetCharaShake, ADVShake.eShakeType.Round, 20f, ADVShake.eShakeType.Round, 0f, 0.02f, time);
				break;
			}
		}

		// Token: 0x06000159 RID: 345 RVA: 0x0000A140 File Offset: 0x00008540
		public void MoveToNextPosition()
		{
			if (!this.m_ShakeBG.IsNeedMoveToNext())
			{
				return;
			}
			if (!this.m_ShakeChara.IsNeedMoveToNext())
			{
				return;
			}
			if (!this.m_ShakeSpr.IsNeedMoveToNext())
			{
				return;
			}
			this.UpdateShakeAllRandomParameter();
			this.m_ShakeBG.RestartShake();
			this.m_ShakeChara.RestartShake();
			this.m_ShakeSpr.RestartShake();
		}

		// Token: 0x0600015A RID: 346 RVA: 0x0000A1A7 File Offset: 0x000085A7
		public void MoveToNextPositionChara()
		{
			if (!this.m_ShakeChara.IsNeedMoveToNext())
			{
				return;
			}
			if (!this.m_ShakeSpr.IsNeedMoveToNext())
			{
				return;
			}
			this.UpdateCharaRandomParameter();
			this.m_ShakeChara.RestartShake();
			this.m_ShakeSpr.RestartShake();
		}

		// Token: 0x0600015B RID: 347 RVA: 0x0000A1E7 File Offset: 0x000085E7
		private void UpdateShakeAllRandomParameter()
		{
			this.UpdateShakeAllRandomParameterX();
			this.UpdateShakeAllRandomParameterY();
		}

		// Token: 0x0600015C RID: 348 RVA: 0x0000A1F5 File Offset: 0x000085F5
		private void UpdateShakeAllRandomParameterX()
		{
			this.m_ShakeAllRandomParamX = UnityEngine.Random.value * 2f - 1f;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x0000A20E File Offset: 0x0000860E
		private void UpdateShakeAllRandomParameterY()
		{
			this.m_ShakeAllRandomParamY = UnityEngine.Random.value * 2f - 1f;
		}

		// Token: 0x0600015E RID: 350 RVA: 0x0000A227 File Offset: 0x00008627
		private void UpdateCharaRandomParameter()
		{
			this.UpdateCharaRandomParameterX();
			this.UpdateCharaRandomParameterY();
		}

		// Token: 0x0600015F RID: 351 RVA: 0x0000A235 File Offset: 0x00008635
		private void UpdateCharaRandomParameterX()
		{
			this.m_CharaRandomParamX = UnityEngine.Random.value * 2f - 1f;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x0000A24E File Offset: 0x0000864E
		private void UpdateCharaRandomParameterY()
		{
			this.m_CharaRandomParamY = UnityEngine.Random.value * 2f - 1f;
		}

		// Token: 0x06000161 RID: 353 RVA: 0x0000A267 File Offset: 0x00008667
		public float GetShakeAllRandomParameter(int i)
		{
			if (i == 0)
			{
				return this.GetShakeAllRandomParameterX();
			}
			if (i == 1)
			{
				return this.GetShakeAllRandomParameterY();
			}
			Debug.LogError(string.Empty);
			return 0f;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x0000A293 File Offset: 0x00008693
		public float GetShakeAllRandomParameterX()
		{
			return this.m_ShakeAllRandomParamX;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x0000A29B File Offset: 0x0000869B
		public float GetShakeAllRandomParameterY()
		{
			return this.m_ShakeAllRandomParamY;
		}

		// Token: 0x06000164 RID: 356 RVA: 0x0000A2A3 File Offset: 0x000086A3
		public float GetCharaRandomParameter(int i)
		{
			if (i == 0)
			{
				return this.GetCharaRandomParameterX();
			}
			if (i == 1)
			{
				return this.GetCharaRandomParameterY();
			}
			Debug.LogError(string.Empty);
			return 0f;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x0000A2CF File Offset: 0x000086CF
		public float GetCharaRandomParameterX()
		{
			return this.m_CharaRandomParamX;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x0000A2D7 File Offset: 0x000086D7
		public float GetCharaRandomParameterY()
		{
			return this.m_CharaRandomParamY;
		}

		// Token: 0x040000F0 RID: 240
		[SerializeField]
		private CanvasScaler m_CanvasScaler;

		// Token: 0x040000F1 RID: 241
		private float m_WaitTimer;

		// Token: 0x040000F2 RID: 242
		private bool m_WaitFade;

		// Token: 0x040000F3 RID: 243
		private bool m_WaitCharaFade;

		// Token: 0x040000F4 RID: 244
		private bool m_WaitMotion;

		// Token: 0x040000F5 RID: 245
		private bool m_WaitMove;

		// Token: 0x040000F6 RID: 246
		private bool m_WaitSE;

		// Token: 0x040000F7 RID: 247
		private bool m_WaitVOICE;

		// Token: 0x040000F8 RID: 248
		private string m_WaitEffectId;

		// Token: 0x040000F9 RID: 249
		private string m_WaitMoveCharaId;

		// Token: 0x040000FA RID: 250
		[SerializeField]
		private float m_TextProgressPerSec = 50f;

		// Token: 0x040000FB RID: 251
		[SerializeField]
		private float m_AutoWait = 1f;

		// Token: 0x040000FC RID: 252
		private ADVPlayer.eNovelTextState m_NovelTextState;

		// Token: 0x040000FD RID: 253
		private string m_TalkerText;

		// Token: 0x040000FE RID: 254
		private string m_TalkText;

		// Token: 0x040000FF RID: 255
		private string m_TalkerCharaID;

		// Token: 0x04000100 RID: 256
		private int m_TextCount;

		// Token: 0x04000101 RID: 257
		private float m_TextCountTimer;

		// Token: 0x04000102 RID: 258
		private bool m_IsAuto;

		// Token: 0x04000103 RID: 259
		private float m_AutoWaitCount;

		// Token: 0x04000104 RID: 260
		private bool m_ShowUI = true;

		// Token: 0x04000105 RID: 261
		private List<ADVParser.EventType> m_msgEventList = new List<ADVParser.EventType>();

		// Token: 0x04000106 RID: 262
		private ADVParser m_Parser = new ADVParser();

		// Token: 0x04000107 RID: 263
		[SerializeField]
		private StandPicManager m_StandPicManager;

		// Token: 0x04000108 RID: 264
		[SerializeField]
		private ADVEffectManager m_ADVEffectManager;

		// Token: 0x04000109 RID: 265
		[SerializeField]
		private ADVSpriteManager m_SpriteManager;

		// Token: 0x0400010A RID: 266
		[SerializeField]
		private ADVBackGroundManager m_BackGroundManager;

		// Token: 0x0400010B RID: 267
		private ADVSoundManager m_SoundManager = new ADVSoundManager();

		// Token: 0x0400010C RID: 268
		private ScriptPlayer m_ScriptPlayer;

		// Token: 0x0400010D RID: 269
		[SerializeField]
		private ADVShake m_ShakeEffect;

		// Token: 0x0400010E RID: 270
		[SerializeField]
		private ADVShake m_ShakeChara;

		// Token: 0x0400010F RID: 271
		[SerializeField]
		private ADVShake m_ShakeSpr;

		// Token: 0x04000110 RID: 272
		[SerializeField]
		private ADVShake m_ShakeBG;

		// Token: 0x04000111 RID: 273
		[SerializeField]
		private ADVUI m_UI;

		// Token: 0x04000112 RID: 274
		[SerializeField]
		private GameObject m_SaveAreasParent;

		// Token: 0x04000113 RID: 275
		[SerializeField]
		private PixelCrashWrapper m_PixelCrashWrapperPrefab;

		// Token: 0x04000114 RID: 276
		private PixelCrashWrapper m_PixelCrashWrapper;

		// Token: 0x04000115 RID: 277
		private readonly Color PixelCrashBaseColor = new Color(0.125f, 0.125f, 0.125f, 1f);

		// Token: 0x04000116 RID: 278
		private float m_PixelCrashPrepareTime;

		// Token: 0x04000117 RID: 279
		private float m_PixelCrashPrepareTimeMax;

		// Token: 0x0400011B RID: 283
		private List<ADVStandPic> m_StandPicForPixelCrash = new List<ADVStandPic>();

		// Token: 0x0400011C RID: 284
		private List<ADVParser.RubyData> m_RubyList = new List<ADVParser.RubyData>();

		// Token: 0x0400011D RID: 285
		private bool m_IsDonePrepare;

		// Token: 0x0400011E RID: 286
		private bool m_IsPlay;

		// Token: 0x0400011F RID: 287
		private bool m_IsEnd;

		// Token: 0x04000120 RID: 288
		private float m_ShakeAllRandomParamX;

		// Token: 0x04000121 RID: 289
		private float m_ShakeAllRandomParamY;

		// Token: 0x04000122 RID: 290
		private float m_CharaRandomParamX;

		// Token: 0x04000123 RID: 291
		private float m_CharaRandomParamY;

		// Token: 0x04000124 RID: 292
		private List<ADVShake> m_OnlyCharaShakes = new List<ADVShake>();

		// Token: 0x04000125 RID: 293
		private ADVDataBase m_DB;

		// Token: 0x04000126 RID: 294
		private bool m_BreakFlag;

		// Token: 0x04000127 RID: 295
		private bool m_StrongTextFlag;

		// Token: 0x04000128 RID: 296
		private int m_AdvID = -1;

		// Token: 0x02000026 RID: 38
		private enum eNovelTextState
		{
			// Token: 0x0400012A RID: 298
			None,
			// Token: 0x0400012B RID: 299
			Progress,
			// Token: 0x0400012C RID: 300
			TapWait
		}
	}
}
