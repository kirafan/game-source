﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200005E RID: 94
	public class ADVTextShakeTag : ADVTextShakeTagBase
	{
		// Token: 0x060002CC RID: 716 RVA: 0x0000FE92 File Offset: 0x0000E292
		public ADVTextShakeTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002CD RID: 717 RVA: 0x0000FEA0 File Offset: 0x0000E2A0
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (this.m_EventType == ADVParser.EventType.event_shake)
			{
				if (execTag && !this.m_IsEndTag)
				{
					int num = tag.IndexOf("=") + 1;
					int num2 = tag.IndexOf(",") - num;
					string advtextTagArgRefName = tag.Substring(num, num2);
					tag.Remove(num, num2);
					num = tag.IndexOf(",") + 1;
					num2 = tag.IndexOf(">") - num;
					string value = tag.Substring(num, num2);
					this.m_Player.ShakeAll((eADVShakeType)this.m_Player.GetADVTextTagParam(advtextTagArgRefName).m_EnumID, Convert.ToSingle(value));
					result = true;
				}
				base.DeleteTag(ref text, ref idx, tag.Length);
			}
			return result;
		}
	}
}
