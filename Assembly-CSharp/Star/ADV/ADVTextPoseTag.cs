﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x02000058 RID: 88
	public class ADVTextPoseTag : ADVTextPoseTagBase
	{
		// Token: 0x060002C2 RID: 706 RVA: 0x0000FC65 File Offset: 0x0000E065
		public ADVTextPoseTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x0000FC70 File Offset: 0x0000E070
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int num2 = tag.IndexOf(",") - num;
				string text2 = tag.Substring(num, num2);
				tag.Remove(num, num2);
				num = tag.IndexOf(",") + 1;
				num2 = tag.IndexOf(">") - num;
				string poseName = tag.Substring(num, num2);
				ADVStandPic standPic = this.m_Player.StandPicManager.GetStandPic(text2);
				if (standPic == null)
				{
					Debug.LogError("指定されたキャラクタが存在しません. キャラクタ：" + text2.ToString());
				}
				else
				{
					standPic.ApplyPose(this.m_Player.GetADVCharacterListDBData(text2, poseName).m_PoseID);
				}
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
