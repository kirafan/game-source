﻿using System;
using ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x02000027 RID: 39
	public class ADVScriptCommandInherited : ADVScriptCommand
	{
		// Token: 0x06000168 RID: 360 RVA: 0x0000A414 File Offset: 0x00008814
		public void Setup(ADVPlayer player)
		{
			this.m_Player = player;
			this.m_StandPicManager = this.m_Player.StandPicManager;
			this.m_SpriteManager = this.m_Player.SpriteManager;
			this.m_BackGroundManager = this.m_Player.BackGroundManager;
			this.m_SoundManager = this.m_Player.SoundManager;
		}

		// Token: 0x06000169 RID: 361 RVA: 0x0000A46C File Offset: 0x0000886C
		public override void Wait(float m_Sec)
		{
			this.m_Player.SetWait(m_Sec);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x0600016A RID: 362 RVA: 0x0000A486 File Offset: 0x00008886
		public override void GoTo(uint m_ScriptID)
		{
			this.m_Player.GoTo((int)m_ScriptID);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x0600016B RID: 363 RVA: 0x0000A4A0 File Offset: 0x000088A0
		public override void PlayBGM(string m_BGMCueName, float m_FadeInSec)
		{
			float num = -1f;
			for (int i = this.m_Player.ScriptPlayer.NowCommandIdx; i < this.m_Player.ScriptPlayer.CommandList.Count; i++)
			{
				if (this.m_Player.ScriptPlayer.CommandList[i].param.FuncName == "StopBGM")
				{
					if (this.m_Player.ScriptPlayer.CommandList[i].param.ArgNum >= 2)
					{
						num = Convert.ToSingle(this.m_Player.ScriptPlayer.CommandList[i].param.argParam[1].value);
					}
					break;
				}
			}
			int fadeInMiliSec = (int)(m_FadeInSec * 1000f);
			int fadeOutMiliSec;
			if (num >= 0f)
			{
				fadeOutMiliSec = (int)(num * 1000f);
			}
			else
			{
				fadeOutMiliSec = -1;
			}
			string cueSheet = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM_adv).m_CueSheet;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(cueSheet, m_BGMCueName, 1f, 0, fadeInMiliSec, fadeOutMiliSec);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x0000A5D4 File Offset: 0x000089D4
		public override void PlayBGM(string m_BGMCueName)
		{
			float num = -1f;
			for (int i = this.m_Player.ScriptPlayer.NowCommandIdx; i < this.m_Player.ScriptPlayer.CommandList.Count; i++)
			{
				if (this.m_Player.ScriptPlayer.CommandList[i].param.FuncName == "StopBGM")
				{
					if (this.m_Player.ScriptPlayer.CommandList[i].param.ArgNum >= 1)
					{
						num = Convert.ToSingle(this.m_Player.ScriptPlayer.CommandList[i].param.argParam[0].value);
					}
					break;
				}
			}
			int fadeOutMiliSec;
			if (num >= 0f)
			{
				fadeOutMiliSec = (int)(num * 1000f);
			}
			else
			{
				fadeOutMiliSec = -1;
			}
			string cueSheet = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.BGM_adv).m_CueSheet;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(cueSheet, m_BGMCueName, 1f, 0, -1, fadeOutMiliSec);
		}

		// Token: 0x0600016D RID: 365 RVA: 0x0000A6FB File Offset: 0x00008AFB
		public override void StopBGM(float m_FadeOutSec)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000A70D File Offset: 0x00008B0D
		public override void StopBGM()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
		}

		// Token: 0x0600016F RID: 367 RVA: 0x0000A720 File Offset: 0x00008B20
		public override void PlayVOICE(string m_ADVCharaID, uint m_CueID)
		{
			this.m_SoundManager.PlayVOICE((eCharaNamedType)this.m_Player.GetADVCharacterParam(m_ADVCharaID).m_NamedType, (eSoundVoiceListDB)m_CueID);
		}

		// Token: 0x06000170 RID: 368 RVA: 0x0000A74D File Offset: 0x00008B4D
		public override void StopVOICE()
		{
			this.m_SoundManager.StopAllVOICE();
		}

		// Token: 0x06000171 RID: 369 RVA: 0x0000A75A File Offset: 0x00008B5A
		public override void WaitVOICE()
		{
			this.m_Player.WaitVOICE();
		}

		// Token: 0x06000172 RID: 370 RVA: 0x0000A767 File Offset: 0x00008B67
		public override void PlaySE(string m_CueID)
		{
			this.m_SoundManager.PlaySE(m_CueID);
		}

		// Token: 0x06000173 RID: 371 RVA: 0x0000A775 File Offset: 0x00008B75
		public override void StopSE()
		{
			this.m_SoundManager.StopAllSE();
		}

		// Token: 0x06000174 RID: 372 RVA: 0x0000A782 File Offset: 0x00008B82
		public override void WaitSE()
		{
			this.m_Player.WaitSE();
		}

		// Token: 0x06000175 RID: 373 RVA: 0x0000A790 File Offset: 0x00008B90
		public override void FadeIn(string m_RGBA, float m_Sec)
		{
			Color rgba;
			ColorUtility.TryParseHtmlString(m_RGBA, out rgba);
			rgba.a = 1f;
			Color rgba2 = new Color(rgba.r, rgba.g, rgba.b, 0f);
			this.m_Player.FillScreen(rgba);
			this.m_Player.Fade(rgba2, m_Sec);
		}

		// Token: 0x06000176 RID: 374 RVA: 0x0000A7EC File Offset: 0x00008BEC
		public override void FadeOut(string m_RGBA, float m_Sec)
		{
			Color rgba;
			ColorUtility.TryParseHtmlString(m_RGBA, out rgba);
			rgba.a = 0f;
			Color rgba2 = new Color(rgba.r, rgba.g, rgba.b, 1f);
			this.m_Player.FillScreen(rgba);
			this.m_Player.Fade(rgba2, m_Sec);
			this.CharaHighlightResetAll();
			this.m_Player.CharaHighlightTalkerResetAll();
		}

		// Token: 0x06000177 RID: 375 RVA: 0x0000A85C File Offset: 0x00008C5C
		public override void FillScreen(string m_RGBA)
		{
			Color rgba;
			ColorUtility.TryParseHtmlString(m_RGBA, out rgba);
			this.m_Player.FillScreen(rgba);
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000A880 File Offset: 0x00008C80
		public override void Fade(string m_RGBAStart, string m_RGBAEnd, float m_Sec, int m_CurveType)
		{
			Color rgba;
			ColorUtility.TryParseHtmlString(m_RGBAStart, out rgba);
			Color rgba2;
			ColorUtility.TryParseHtmlString(m_RGBAEnd, out rgba2);
			this.m_Player.FillScreen(rgba);
			this.m_Player.Fade(rgba2, m_Sec);
		}

		// Token: 0x06000179 RID: 377 RVA: 0x0000A8B8 File Offset: 0x00008CB8
		public override void WaitFade()
		{
			this.m_Player.WaitFade();
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x0000A8D1 File Offset: 0x00008CD1
		public override void BGVisible(uint m_ID, string m_FileNameWithoutExt)
		{
			this.m_BackGroundManager.Visible(m_ID, m_FileNameWithoutExt);
		}

		// Token: 0x0600017B RID: 379 RVA: 0x0000A8E0 File Offset: 0x00008CE0
		public override void BGScroll(uint m_ID, float m_X, float m_Y, float m_Sec, int m_CurveType)
		{
			this.m_BackGroundManager.GetActiveMatchIdInstance(m_ID).MoveTo(new Vector2(m_X, m_Y), m_Sec, (eADVCurveType)m_CurveType);
		}

		// Token: 0x0600017C RID: 380 RVA: 0x0000A8FE File Offset: 0x00008CFE
		public override void BGScale(uint m_ID, float m_Scale, float m_Sec, int m_CurveType)
		{
			this.m_BackGroundManager.GetActiveMatchIdInstance(m_ID).ScaleTo(m_Scale, m_Sec, (eADVCurveType)m_CurveType);
		}

		// Token: 0x0600017D RID: 381 RVA: 0x0000A915 File Offset: 0x00008D15
		public override void BGPointZoom(uint m_ID, float m_Scale, float m_Sec, float m_X, float m_Y, int m_CurveType)
		{
			this.m_BackGroundManager.GetActiveMatchIdInstance(m_ID).Zoom(m_Scale, m_Sec, m_X, m_Y, (eADVCurveType)m_CurveType);
		}

		// Token: 0x0600017E RID: 382 RVA: 0x0000A930 File Offset: 0x00008D30
		public override void BGColor(uint m_ID, string m_StartColor, string m_EndColor, float m_Sec, int m_CurveType)
		{
			Color startColor;
			ColorUtility.TryParseHtmlString(m_StartColor, out startColor);
			Color endColor;
			ColorUtility.TryParseHtmlString(m_EndColor, out endColor);
			this.m_BackGroundManager.GetActiveMatchIdInstance(m_ID).ChangeColor(startColor, endColor, m_Sec, (eADVCurveType)m_CurveType);
		}

		// Token: 0x0600017F RID: 383 RVA: 0x0000A966 File Offset: 0x00008D66
		public override void Shake(int m_ShakeType, float m_Time)
		{
			this.m_Player.ShakeAll((eADVShakeType)m_ShakeType, m_Time);
		}

		// Token: 0x06000180 RID: 384 RVA: 0x0000A975 File Offset: 0x00008D75
		public override void ShakeChara(int m_ShakeType, float m_Time)
		{
			this.m_Player.ShakeChara((eADVShakeType)m_ShakeType, m_Time, false);
		}

		// Token: 0x06000181 RID: 385 RVA: 0x0000A985 File Offset: 0x00008D85
		public override void ShakeBG(int m_ShakeType, float m_Time)
		{
			this.m_Player.ShakeBG((eADVShakeType)m_ShakeType, m_Time, false);
		}

		// Token: 0x06000182 RID: 386 RVA: 0x0000A998 File Offset: 0x00008D98
		public override void ShakeChara(int m_ShakeType, float m_Time, string m_ADVCharaID)
		{
			ADVStandPic advstandPic = null;
			GameObject gameObject = null;
			ADVShake advshake = null;
			if (m_ADVCharaID != null)
			{
				advstandPic = this.m_StandPicManager.GetStandPic(m_ADVCharaID);
			}
			if (advstandPic != null)
			{
				gameObject = advstandPic.gameObject;
			}
			if (gameObject != null)
			{
				advshake = gameObject.GetComponent<ADVShake>();
				if (advshake == null)
				{
					advshake = gameObject.AddComponent<ADVShake>();
				}
			}
			if (advshake != null)
			{
				advshake.UpdateTransform();
				this.m_Player.ShakeOnlyChara(advshake, (eADVShakeType)m_ShakeType, m_Time);
			}
		}

		// Token: 0x06000183 RID: 387 RVA: 0x0000AA21 File Offset: 0x00008E21
		public override void CharaShot(string m_ADVCharaID)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID
			}), 0f);
		}

		// Token: 0x06000184 RID: 388 RVA: 0x0000AA3D File Offset: 0x00008E3D
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2
			}), 0f);
		}

		// Token: 0x06000185 RID: 389 RVA: 0x0000AA5D File Offset: 0x00008E5D
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3
			}), 0f);
		}

		// Token: 0x06000186 RID: 390 RVA: 0x0000AA81 File Offset: 0x00008E81
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3,
				m_ADVCharaID4
			}), 0f);
		}

		// Token: 0x06000187 RID: 391 RVA: 0x0000AAAA File Offset: 0x00008EAA
		public override void CharaShot(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3,
				m_ADVCharaID4,
				m_ADVCharaID5
			}), 0f);
		}

		// Token: 0x06000188 RID: 392 RVA: 0x0000AAD8 File Offset: 0x00008ED8
		public override void CharaShotFade(string m_ADVCharaID, float m_Sec)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID
			}), m_Sec);
		}

		// Token: 0x06000189 RID: 393 RVA: 0x0000AAF0 File Offset: 0x00008EF0
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, float m_Sec)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2
			}), m_Sec);
		}

		// Token: 0x0600018A RID: 394 RVA: 0x0000AB0C File Offset: 0x00008F0C
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, float m_Sec)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3
			}), m_Sec);
		}

		// Token: 0x0600018B RID: 395 RVA: 0x0000AB2D File Offset: 0x00008F2D
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, float m_Sec)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3,
				m_ADVCharaID4
			}), m_Sec);
		}

		// Token: 0x0600018C RID: 396 RVA: 0x0000AB53 File Offset: 0x00008F53
		public override void CharaShotFade(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5, float m_Sec)
		{
			this.CharaShotProcess(new ADVScriptCommandInherited.CharaIDs(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3,
				m_ADVCharaID4,
				m_ADVCharaID5
			}), m_Sec);
		}

		// Token: 0x0600018D RID: 397 RVA: 0x0000AB7E File Offset: 0x00008F7E
		public override void SetCharaShotPosition(int m_ADVCharaPos)
		{
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[]
			{
				(eADVStandPosition)m_ADVCharaPos
			});
		}

		// Token: 0x0600018E RID: 398 RVA: 0x0000AB95 File Offset: 0x00008F95
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2)
		{
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[]
			{
				(eADVStandPosition)m_ADVCharaPos,
				(eADVStandPosition)m_ADVCharaPos2
			});
		}

		// Token: 0x0600018F RID: 399 RVA: 0x0000ABB0 File Offset: 0x00008FB0
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3)
		{
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[]
			{
				(eADVStandPosition)m_ADVCharaPos,
				(eADVStandPosition)m_ADVCharaPos2,
				(eADVStandPosition)m_ADVCharaPos3
			});
		}

		// Token: 0x06000190 RID: 400 RVA: 0x0000ABCF File Offset: 0x00008FCF
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4)
		{
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[]
			{
				(eADVStandPosition)m_ADVCharaPos,
				(eADVStandPosition)m_ADVCharaPos2,
				(eADVStandPosition)m_ADVCharaPos3,
				(eADVStandPosition)m_ADVCharaPos4
			});
		}

		// Token: 0x06000191 RID: 401 RVA: 0x0000ABF3 File Offset: 0x00008FF3
		public override void SetCharaShotPosition(int m_ADVCharaPos, int m_ADVCharaPos2, int m_ADVCharaPos3, int m_ADVCharaPos4, int m_ADVCharaPos5)
		{
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[]
			{
				(eADVStandPosition)m_ADVCharaPos,
				(eADVStandPosition)m_ADVCharaPos2,
				(eADVStandPosition)m_ADVCharaPos3,
				(eADVStandPosition)m_ADVCharaPos4,
				(eADVStandPosition)m_ADVCharaPos5
			});
		}

		// Token: 0x06000192 RID: 402 RVA: 0x0000AC1C File Offset: 0x0000901C
		protected void CharaShotProcess(ADVScriptCommandInherited.CharaIDs charaId, float m_FadeSec = 0f)
		{
			this.m_StandPicManager.HideAllChara();
			if (this.positions == null)
			{
				this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[0]);
			}
			string[] array = new string[11];
			for (int i = 0; i < 5; i++)
			{
				array[(int)this.positions.value[i]] = charaId.value[i];
			}
			this.positions = new ADVScriptCommandInherited.CharaPositions(new eADVStandPosition[0]);
			for (int j = 0; j < array.Length; j++)
			{
				this.m_StandPicManager.SetCharaEnableRender(array[j], true, m_FadeSec);
				this.m_StandPicManager.SetChara(array[j], (eADVStandPosition)j, 0f, 0f, eADVCurveType.Linear);
			}
			for (int k = 0; k < array.Length; k++)
			{
				ADVStandPic standPic = this.m_StandPicManager.GetStandPic(array[k]);
				if (standPic != null)
				{
					standPic.SetPriorityState(ADVStandPic.ePriorityState.Normal);
				}
			}
			int num = 2;
			int num2 = 7;
			int num3 = num;
			while (0 <= num3)
			{
				this.m_StandPicManager.SetAsLastSibling(array[num2 + num3 + 1]);
				this.m_StandPicManager.SetAsLastSibling(array[num2 - num3]);
				this.m_StandPicManager.SetAsLastSibling(array[num + num3]);
				this.m_StandPicManager.SetAsLastSibling(array[num - num3]);
				num3--;
			}
			this.CharaHighlightResetAll();
			this.m_Player.BreakFlag = true;
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x06000193 RID: 403 RVA: 0x0000AD97 File Offset: 0x00009197
		public override void CharaOutAll()
		{
			this.m_StandPicManager.HideAllChara();
			this.CharaHighlightResetAll();
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x06000194 RID: 404 RVA: 0x0000ADC5 File Offset: 0x000091C5
		public override void CharaOutAllFade(float m_Sec)
		{
			this.m_StandPicManager.HideAllCharaFade(m_Sec);
			this.CharaHighlightResetAll();
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x06000195 RID: 405 RVA: 0x0000ADF4 File Offset: 0x000091F4
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition)
		{
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, true, 0f);
			this.m_StandPicManager.SetChara(m_ADVCharaID, (eADVStandPosition)m_StandPosition, 0f, 0f, eADVCurveType.Linear);
		}

		// Token: 0x06000196 RID: 406 RVA: 0x0000AE20 File Offset: 0x00009220
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
			this.CharaIn(m_ADVCharaID, m_StandPosition, m_StartSide, m_Sec, m_CurveType, 0f);
		}

		// Token: 0x06000197 RID: 407 RVA: 0x0000AE34 File Offset: 0x00009234
		public override void CharaIn(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, true, 0f);
			this.m_StandPicManager.SetCharaScreenSide(m_ADVCharaID, (eADVSidePosition)m_StartSide, 0f, 0f, eADVCurveType.Linear, false);
			this.m_StandPicManager.SetChara(m_ADVCharaID, (eADVStandPosition)m_StandPosition, m_XOffset, m_Sec, (eADVCurveType)m_CurveType);
			this.CharaHighlightReset(m_ADVCharaID);
		}

		// Token: 0x06000198 RID: 408 RVA: 0x0000AE86 File Offset: 0x00009286
		public override void CharaOut(string m_ADVCharaID)
		{
			this.CharaHighlightReset(m_ADVCharaID);
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, false, 0f);
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x06000199 RID: 409 RVA: 0x0000AEBC File Offset: 0x000092BC
		public override void CharaOut(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, true, 0f);
			this.m_StandPicManager.SetCharaScreenSide(m_ADVCharaID, (eADVSidePosition)m_EndSide, 0f, m_Sec, (eADVCurveType)m_CurveType, false);
			this.CharaHighlightReset(m_ADVCharaID);
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x0600019A RID: 410 RVA: 0x0000AF13 File Offset: 0x00009313
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, float m_Sec)
		{
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, true, m_Sec);
			this.m_StandPicManager.SetChara(m_ADVCharaID, (eADVStandPosition)m_StandPosition, 0f, 0f, eADVCurveType.Linear);
			this.CharaHighlightReset(m_ADVCharaID);
		}

		// Token: 0x0600019B RID: 411 RVA: 0x0000AF42 File Offset: 0x00009342
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType)
		{
			this.CharaInFade(m_ADVCharaID, m_StandPosition, m_StartSide, m_Sec, m_CurveType, 0f);
		}

		// Token: 0x0600019C RID: 412 RVA: 0x0000AF58 File Offset: 0x00009358
		public override void CharaInFade(string m_ADVCharaID, int m_StandPosition, int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, false, 0f);
			this.m_StandPicManager.SetCharaScreenSide(m_ADVCharaID, (eADVSidePosition)m_StartSide, 0f, 0f, eADVCurveType.Linear, false);
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, true, m_Sec);
			this.m_StandPicManager.SetChara(m_ADVCharaID, (eADVStandPosition)m_StandPosition, m_XOffset, m_Sec, (eADVCurveType)m_CurveType);
			this.CharaHighlightReset(m_ADVCharaID);
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000AFB9 File Offset: 0x000093B9
		public override void CharaOutFade(string m_ADVCharaID, float m_Sec)
		{
			this.CharaHighlightReset(m_ADVCharaID);
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, false, m_Sec);
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000AFEC File Offset: 0x000093EC
		public override void CharaOutFade(string m_ADVCharaID, int m_EndSide, float m_Sec, int m_CurveType)
		{
			this.CharaHighlightReset(m_ADVCharaID);
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_StandPicManager.SetCharaEnableRender(m_ADVCharaID, false, m_Sec);
			this.m_StandPicManager.SetCharaScreenSide(m_ADVCharaID, (eADVSidePosition)m_EndSide, 0f, m_Sec, (eADVCurveType)m_CurveType, false);
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x0600019F RID: 415 RVA: 0x0000B03F File Offset: 0x0000943F
		public override void SetTarget(string m_ADVCharaID, string m_ADVCharaID2, string m_ADVCharaID3, string m_ADVCharaID4, string m_ADVCharaID5)
		{
			this.SetTargetProcess(new string[]
			{
				m_ADVCharaID,
				m_ADVCharaID2,
				m_ADVCharaID3,
				m_ADVCharaID4,
				m_ADVCharaID5
			});
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x0000B064 File Offset: 0x00009464
		protected void SetTargetProcess(params string[] advCharaIds)
		{
			for (int i = 0; i < this.m_TargetADVCharaIDs.Length; i++)
			{
				if (i < advCharaIds.Length)
				{
					this.m_TargetADVCharaIDs[i] = advCharaIds[i];
				}
				else
				{
					this.m_TargetADVCharaIDs[i] = "-1";
				}
			}
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x0000B0B0 File Offset: 0x000094B0
		public override void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType, float m_XOffset)
		{
			float num = 0f;
			ADVStandPic advstandPic = null;
			if (m_StartSide == 0)
			{
				for (int i = this.m_TargetADVCharaIDs.Length - 1; i >= 0; i--)
				{
					if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[i]))
					{
						advstandPic = this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[i]);
						advstandPic.SetEnableRender(true);
						LayoutRebuilder.ForceRebuildLayoutImmediate(advstandPic.RectTransform);
						num = this.m_StandPicManager.RectTransform.rect.width * 0.5f + ADVDefine.CHARA_POSITION[i] + m_XOffset + advstandPic.RectTransform.rect.width * 0.5f;
						break;
					}
				}
			}
			else
			{
				for (int j = 0; j < this.m_TargetADVCharaIDs.Length; j++)
				{
					if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[j]))
					{
						advstandPic = this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[j]);
						this.m_StandPicManager.SetCharaEnableRender(advstandPic.ADVCharaID, true, 0f);
						LayoutRebuilder.ForceRebuildLayoutImmediate(advstandPic.RectTransform);
						num = -(this.m_StandPicManager.RectTransform.rect.width - (this.m_StandPicManager.RectTransform.rect.width * 0.5f + ADVDefine.CHARA_POSITION[j] + m_XOffset - advstandPic.RectTransform.rect.width * 0.5f));
						break;
					}
				}
			}
			if (advstandPic == null)
			{
				return;
			}
			for (int k = 0; k < this.m_TargetADVCharaIDs.Length; k++)
			{
				if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[k]))
				{
					this.m_StandPicManager.SetCharaEnableRender(this.m_TargetADVCharaIDs[k], true, 0f);
					this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[k]).SetAnchoredPosition(new Vector3(ADVDefine.CHARA_POSITION[k] + m_XOffset - num, 0f));
					this.m_StandPicManager.SetChara(this.m_TargetADVCharaIDs[k], (eADVStandPosition)k, m_XOffset, m_Sec, (eADVCurveType)m_CurveType);
				}
			}
			this.CharaHighlightResetAll();
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x0000B2EC File Offset: 0x000096EC
		public override void CharaInTarget(int m_StartSide, float m_Sec, int m_CurveType)
		{
			this.CharaInTarget(m_StartSide, m_Sec, m_CurveType, 0f);
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x0000B2FC File Offset: 0x000096FC
		public override void CharaOutTarget(int m_EndSide, float m_Sec, int m_CurveType)
		{
			float x = 0f;
			ADVStandPic advstandPic = null;
			if (m_EndSide == 0)
			{
				for (int i = this.m_TargetADVCharaIDs.Length - 1; i >= 0; i--)
				{
					if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[i]))
					{
						advstandPic = this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[i]);
						advstandPic.SetEnableRender(true);
						LayoutRebuilder.ForceRebuildLayoutImmediate(advstandPic.RectTransform);
						x = -(this.m_StandPicManager.RectTransform.rect.width * 0.5f + advstandPic.RectTransform.anchoredPosition.x + advstandPic.RectTransform.rect.width * 0.5f);
						break;
					}
				}
			}
			else
			{
				for (int j = 0; j < this.m_TargetADVCharaIDs.Length; j++)
				{
					if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[j]))
					{
						advstandPic = this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[j]);
						advstandPic.SetEnableRender(true);
						LayoutRebuilder.ForceRebuildLayoutImmediate(advstandPic.RectTransform);
						x = this.m_StandPicManager.RectTransform.rect.width - (this.m_StandPicManager.RectTransform.rect.width * 0.5f + advstandPic.RectTransform.anchoredPosition.x - advstandPic.RectTransform.rect.width * 0.5f);
						break;
					}
				}
			}
			if (advstandPic == null)
			{
				return;
			}
			for (int k = 0; k < this.m_TargetADVCharaIDs.Length; k++)
			{
				if (!ADVUtility.IsEmptyADVCharaID(this.m_TargetADVCharaIDs[k]))
				{
					this.m_StandPicManager.SetCharaEnableRender(this.m_TargetADVCharaIDs[k], true, 0f);
					this.m_StandPicManager.GetStandPic(this.m_TargetADVCharaIDs[k]).Move(new Vector3(x, 0f), m_Sec, (eADVCurveType)m_CurveType, true);
				}
			}
			this.CharaHighlightResetAll();
			this.m_Player.CharaHighlightTalkerResetAll();
			this.m_Player.StandPicManager.LoadRecentPicMax();
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x0000B533 File Offset: 0x00009933
		public override void CharaAlignment(float m_Sec, int m_CurveType)
		{
			this.m_StandPicManager.CharaAlignment(m_Sec, (eADVCurveType)m_CurveType);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000B544 File Offset: 0x00009944
		public override void CharaSwap(string m_ADVCharaID1, string m_ADVCharaID2, float m_Sec, int m_CurveType)
		{
			Vector3 v = this.m_StandPicManager.GetCharaPosition(m_ADVCharaID1);
			this.m_StandPicManager.GetStandPic(m_ADVCharaID1).MoveTo(this.m_StandPicManager.GetCharaPosition(m_ADVCharaID2), m_Sec, (eADVCurveType)m_CurveType, false);
			this.m_StandPicManager.GetStandPic(m_ADVCharaID2).MoveTo(v, m_Sec, (eADVCurveType)m_CurveType, false);
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x0000B59F File Offset: 0x0000999F
		public override void WaitCharaFade()
		{
			this.m_Player.WaitCharaFade();
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x0000B5B8 File Offset: 0x000099B8
		public override void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_XOffset, float m_Sec, int m_CurveType)
		{
			ADVStandPic standPic = this.m_StandPicManager.GetStandPic(m_ADVCharaID);
			if (standPic != null)
			{
				standPic.MoveTo(ADVUtility.GetStandPosition((eADVStandPosition)m_StandPosition) + new Vector3(m_XOffset, 0f, 0f), m_Sec, (eADVCurveType)m_CurveType, false);
			}
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x0000B60C File Offset: 0x00009A0C
		public override void CharaMove(string m_ADVCharaID, int m_StandPosition, float m_XOffset, float m_YOffset, float m_Sec, int m_CurveType)
		{
			ADVStandPic standPic = this.m_StandPicManager.GetStandPic(m_ADVCharaID);
			if (standPic != null)
			{
				standPic.MoveTo(ADVUtility.GetStandPosition((eADVStandPosition)m_StandPosition) + new Vector3(m_XOffset, m_YOffset, 0f), m_Sec, (eADVCurveType)m_CurveType, false);
			}
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x0000B65A File Offset: 0x00009A5A
		public override void WaitCharaMove()
		{
			this.m_Player.WaitMove();
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001AA RID: 426 RVA: 0x0000B673 File Offset: 0x00009A73
		public override void CharaMot(string m_ADVCharaID, string m_MotID)
		{
			this.m_StandPicManager.GetStandPic(m_ADVCharaID).PlayMotion(m_MotID);
		}

		// Token: 0x060001AB RID: 427 RVA: 0x0000B687 File Offset: 0x00009A87
		public override void WaitMotion()
		{
			this.m_Player.WaitMotion();
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001AC RID: 428 RVA: 0x0000B6A0 File Offset: 0x00009AA0
		public override void CharaFace(string m_ADVCharaID, int m_FaceID)
		{
			this.m_StandPicManager.GetStandPic(m_ADVCharaID).ApplyFace((eADVFace)m_FaceID);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x0000B6B4 File Offset: 0x00009AB4
		public override void CharaPose(string m_ADVCharaID, string m_PoseName)
		{
			this.m_StandPicManager.GetStandPic(m_ADVCharaID).ApplyPose(m_PoseName);
		}

		// Token: 0x060001AE RID: 430 RVA: 0x0000B6C8 File Offset: 0x00009AC8
		public override void CharaEmotion(string m_ADVCharaID, string m_EmotionID)
		{
			this.m_Player.PlayEmotion(m_ADVCharaID, m_EmotionID, eADVEmotionPosition.CharaDefault);
		}

		// Token: 0x060001AF RID: 431 RVA: 0x0000B6D8 File Offset: 0x00009AD8
		public override void CharaEmotion(string m_ADVCharaID, string m_EmotionID, int m_Position)
		{
			this.m_Player.PlayEmotion(m_ADVCharaID, m_EmotionID, (eADVEmotionPosition)m_Position);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x0000B6E8 File Offset: 0x00009AE8
		public override void EmotionEnd(string m_ADVCharaID, string m_EmotionID)
		{
			this.m_Player.ADVEffectManager.StopEmo(m_EmotionID);
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x0000B6FB File Offset: 0x00009AFB
		public override void WaitEmotion(string m_ADVCharaID, string m_EmotionID)
		{
			this.m_Player.WaitEmotion(m_EmotionID);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x0000B715 File Offset: 0x00009B15
		public override void CharaHighlight(string m_ADVCharaID)
		{
			this.m_Player.CharaHighlight(m_ADVCharaID);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x0000B723 File Offset: 0x00009B23
		public override void CharaHighlightAll()
		{
			this.m_Player.CharaHighlightAll();
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x0000B730 File Offset: 0x00009B30
		public override void CharaHighlightReset(string m_ADVCharaID)
		{
			this.m_Player.CharaHighlightDefault(m_ADVCharaID);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x0000B73E File Offset: 0x00009B3E
		public override void CharaHighlightResetAll()
		{
			this.m_Player.CharaHighlightDefaultAll();
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x0000B74B File Offset: 0x00009B4B
		public override void CharaShading(string m_ADVCharaID)
		{
			this.m_Player.CharaShading(m_ADVCharaID);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x0000B759 File Offset: 0x00009B59
		public override void CharaShadingAll()
		{
			this.m_Player.CharaShadingAll();
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x0000B766 File Offset: 0x00009B66
		public override void CharaHighlightTalker(string m_ADVCharaID)
		{
			this.m_Player.CharaHighlightTalker(m_ADVCharaID);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x0000B774 File Offset: 0x00009B74
		public override void CharaPriorityTop(string m_ADVCharaID)
		{
			this.m_StandPicManager.CharaPriorityTop(m_ADVCharaID);
		}

		// Token: 0x060001BA RID: 442 RVA: 0x0000B782 File Offset: 0x00009B82
		public override void CharaPriorityTopSet(string m_ADVCharaID)
		{
			this.m_StandPicManager.SetAsTopSetGroupLastSibling(m_ADVCharaID);
		}

		// Token: 0x060001BB RID: 443 RVA: 0x0000B790 File Offset: 0x00009B90
		public override void CharaPriorityBottom(string m_ADVCharaID)
		{
			this.m_StandPicManager.CharaPriorityBottom(m_ADVCharaID);
		}

		// Token: 0x060001BC RID: 444 RVA: 0x0000B79E File Offset: 0x00009B9E
		public override void CharaPriorityBottomSet(string m_ADVCharaID)
		{
			this.m_StandPicManager.SetAsBottomSetGroupFirstSibling(m_ADVCharaID);
		}

		// Token: 0x060001BD RID: 445 RVA: 0x0000B7AC File Offset: 0x00009BAC
		public override void CharaPriorityReset(string m_ADVCharaID)
		{
			this.m_StandPicManager.CharaPriorityReset(m_ADVCharaID);
		}

		// Token: 0x060001BE RID: 446 RVA: 0x0000B7BC File Offset: 0x00009BBC
		public override void CharaTransparency(string m_ADVCharaID, uint m_Alpha, float m_Sec, int m_CurveType)
		{
			ADVStandPic standPic = this.m_StandPicManager.GetStandPic(m_ADVCharaID);
			if (standPic != null)
			{
				standPic.AlphaTo(m_Alpha, m_Sec, (eADVCurveType)m_CurveType);
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x0000B7EC File Offset: 0x00009BEC
		public override void CharaTransparencyAll(uint m_Alpha, float m_Sec, int m_CurveType)
		{
			this.m_StandPicManager.AlphaToAll(m_Alpha, m_Sec, (eADVCurveType)m_CurveType);
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x0000B7FC File Offset: 0x00009BFC
		public override void CharaTransparencyResetAll()
		{
			this.CharaTransparencyAll(100U, 0f, 0);
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x0000B80C File Offset: 0x00009C0C
		public override void Effect(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffect(m_effectID, (eADVStandPosition)m_StandPosition, m_X, m_Y, m_Rotate, false);
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x0000B821 File Offset: 0x00009C21
		public override void Effect(string m_effectID, int m_StandPosition)
		{
			this.Effect(m_effectID, m_StandPosition, 0f, 0f, 0f);
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x0000B83A File Offset: 0x00009C3A
		public override void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffect(m_effectID, m_ADVCharaID, (eADVCharaAnchor)m_CharaAnchor, m_X, m_Y, m_Rotate, false);
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x0000B851 File Offset: 0x00009C51
		public override void EffectChara(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
			this.EffectChara(m_effectID, m_ADVCharaID, m_CharaAnchor, 0f, 0f, 0f);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x0000B86B File Offset: 0x00009C6B
		public override void EffectScreen(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffectScreen(m_effectID, (eADVScreenAnchor)m_ScreenAnchor, m_X, m_Y, m_Rotate, false);
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x0000B880 File Offset: 0x00009C80
		public override void EffectScreen(string m_effectID, int m_ScreenAnchor)
		{
			this.EffectScreen(m_effectID, m_ScreenAnchor, 0f, 0f, 0f);
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000B899 File Offset: 0x00009C99
		public override void EffectLoop(string m_effectID, int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffect(m_effectID, (eADVStandPosition)m_StandPosition, m_X, m_Y, m_Rotate, true);
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x0000B8AE File Offset: 0x00009CAE
		public override void EffectLoop(string m_effectID, int m_StandPosition)
		{
			this.EffectLoop(m_effectID, m_StandPosition, 0f, 0f, 0f);
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x0000B8C7 File Offset: 0x00009CC7
		public override void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffect(m_effectID, m_ADVCharaID, (eADVCharaAnchor)m_CharaAnchor, m_X, m_Y, m_Rotate, true);
		}

		// Token: 0x060001CA RID: 458 RVA: 0x0000B8DE File Offset: 0x00009CDE
		public override void EffectCharaLoop(string m_effectID, string m_ADVCharaID, int m_CharaAnchor)
		{
			this.EffectCharaLoop(m_effectID, m_ADVCharaID, m_CharaAnchor, 0f, 0f, 0f);
		}

		// Token: 0x060001CB RID: 459 RVA: 0x0000B8F8 File Offset: 0x00009CF8
		public override void EffectScreenLoop(string m_effectID, int m_ScreenAnchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffectScreen(m_effectID, (eADVScreenAnchor)m_ScreenAnchor, m_X, m_Y, m_Rotate, true);
		}

		// Token: 0x060001CC RID: 460 RVA: 0x0000B90D File Offset: 0x00009D0D
		public override void EffectScreenLoop(string m_effectID, int m_ScreenAnchor)
		{
			this.EffectScreenLoop(m_effectID, m_ScreenAnchor, 0f, 0f, 0f);
		}

		// Token: 0x060001CD RID: 461 RVA: 0x0000B926 File Offset: 0x00009D26
		public override void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition, float m_X, float m_Y)
		{
			this.m_Player.EffectLoopTarget(m_effectID, m_afterEffectID, (eADVStandPosition)m_StandPosition, m_X, m_Y, 0f);
		}

		// Token: 0x060001CE RID: 462 RVA: 0x0000B93F File Offset: 0x00009D3F
		public override void EffectLoopTarget(string m_effectID, string m_afterEffectID, int m_StandPosition)
		{
			this.EffectLoopTarget(m_effectID, m_afterEffectID, m_StandPosition, 0f, 0f);
		}

		// Token: 0x060001CF RID: 463 RVA: 0x0000B954 File Offset: 0x00009D54
		public override void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y)
		{
			this.m_Player.EffectCharaLoopTarget(m_effectID, m_afterEffectID, m_ADVCharaID, (eADVCharaAnchor)m_CharaAnchor, m_X, m_Y, 0f);
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x0000B96F File Offset: 0x00009D6F
		public override void EffectCharaLoopTarget(string m_effectID, string m_afterEffectID, string m_ADVCharaID, int m_CharaAnchor)
		{
			this.EffectCharaLoopTarget(m_effectID, m_afterEffectID, m_ADVCharaID, m_CharaAnchor, 0f, 0f);
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x0000B986 File Offset: 0x00009D86
		public override void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor, float m_X, float m_Y)
		{
			this.m_Player.EffectScreenLoopTarget(m_effectID, m_afterEffectID, (eADVScreenAnchor)m_ScreenAnchor, m_X, m_Y, 0f, false);
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x0000B9A0 File Offset: 0x00009DA0
		public override void EffectScreenLoopTarget(string m_effectID, string m_afterEffectID, int m_ScreenAnchor)
		{
			this.EffectScreenLoopTarget(m_effectID, m_afterEffectID, m_ScreenAnchor, 0f, 0f);
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x0000B9B5 File Offset: 0x00009DB5
		public override void EffectLoopTargetEnd(string m_effectID)
		{
			this.m_Player.ADVEffectManager.LoopEnd(m_effectID);
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x0000B9C8 File Offset: 0x00009DC8
		public override void EffectEnd(string m_effectID)
		{
			this.m_Player.ADVEffectManager.Stop(m_effectID);
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x0000B9DB File Offset: 0x00009DDB
		public override void WaitEffect(string m_effectID)
		{
			this.m_Player.WaitEffect(m_effectID);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x0000B9F5 File Offset: 0x00009DF5
		public override void EffectMuteSE()
		{
			this.m_Player.ADVEffectManager.SetEnablePlaySE(false);
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x0000BA08 File Offset: 0x00009E08
		public override void EffectMuteSEReset()
		{
			this.m_Player.ADVEffectManager.SetEnablePlaySE(true);
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x0000BA1B File Offset: 0x00009E1B
		public override void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID, sbyte m_IgnoreParticle)
		{
			this.m_Player.ADVEffectManager.PresetEffectLoop(m_stEffectID, m_lpEffectID, m_EdEffectID, (int)m_IgnoreParticle != 1);
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x0000BA39 File Offset: 0x00009E39
		public override void PresetEffectLoop(string m_stEffectID, string m_lpEffectID, string m_EdEffectID)
		{
			this.PresetEffectLoop(m_stEffectID, m_lpEffectID, m_EdEffectID, 0);
		}

		// Token: 0x060001DA RID: 474 RVA: 0x0000BA45 File Offset: 0x00009E45
		public override void EffectLoopWithPreset(int m_StandPosition, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffectPreset((eADVStandPosition)m_StandPosition, m_X, m_Y, m_Rotate);
		}

		// Token: 0x060001DB RID: 475 RVA: 0x0000BA57 File Offset: 0x00009E57
		public override void EffectLoopWithPreset(int m_StandPosition)
		{
			this.EffectScreenLoopWithPreset(m_StandPosition, 0f, 0f, 0f);
		}

		// Token: 0x060001DC RID: 476 RVA: 0x0000BA6F File Offset: 0x00009E6F
		public override void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffectPreset(m_ADVCharaID, (eADVCharaAnchor)m_CharaAnchor, m_X, m_Y, m_Rotate);
		}

		// Token: 0x060001DD RID: 477 RVA: 0x0000BA83 File Offset: 0x00009E83
		public override void EffectCharaLoopWithPreset(string m_ADVCharaID, int m_CharaAnchor)
		{
			this.EffectCharaLoopWithPreset(m_ADVCharaID, m_CharaAnchor);
		}

		// Token: 0x060001DE RID: 478 RVA: 0x0000BA8D File Offset: 0x00009E8D
		public override void EffectScreenLoopWithPreset(int m_Anchor, float m_X, float m_Y, float m_Rotate)
		{
			this.m_Player.PlayEffectScreenPreset((eADVScreenAnchor)m_Anchor, m_X, m_Y, m_Rotate);
		}

		// Token: 0x060001DF RID: 479 RVA: 0x0000BA9F File Offset: 0x00009E9F
		public override void EffectScreenLoopWithPreset(int m_Anchor)
		{
			this.EffectScreenLoopWithPreset(m_Anchor, 0f, 0f, 0f);
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x0000BAB7 File Offset: 0x00009EB7
		public override void SpriteVisible(uint m_ID, string m_FileNameWithoutExt, uint m_VisibleFlg)
		{
			this.m_SpriteManager.EnableRender(m_ID, m_VisibleFlg > 0U);
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x0000BAC9 File Offset: 0x00009EC9
		public override void SpritePos(uint m_ID, float m_X, float m_Y)
		{
			this.m_SpriteManager.Move(m_ID, new Vector2(m_X, m_Y), 0f);
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x0000BAE3 File Offset: 0x00009EE3
		public override void SpritePos(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
			this.m_SpriteManager.Move(m_ID, new Vector2(m_X, m_Y), m_Sec);
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x0000BAFA File Offset: 0x00009EFA
		public override void SpriteScale(uint m_ID, float m_X, float m_Y)
		{
			this.m_SpriteManager.Scale(m_ID, new Vector2(m_X, m_Y), 0f);
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x0000BB14 File Offset: 0x00009F14
		public override void SpriteScale(uint m_ID, float m_X, float m_Y, float m_Sec)
		{
			this.m_SpriteManager.Scale(m_ID, new Vector2(m_X, m_Y), m_Sec);
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x0000BB2C File Offset: 0x00009F2C
		public override void SpriteColor(uint m_ID, string m_RGBA)
		{
			Color color;
			ColorUtility.TryParseHtmlString(m_RGBA, out color);
			this.m_SpriteManager.SetColor(m_ID, color, 0f);
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x0000BB54 File Offset: 0x00009F54
		public override void SpriteColor(uint m_ID, string m_RGBA, float m_Sec)
		{
			Color color;
			ColorUtility.TryParseHtmlString(m_RGBA, out color);
			this.m_SpriteManager.SetColor(m_ID, color, m_Sec);
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0000BB78 File Offset: 0x00009F78
		public override void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID, int m_EmotionPos)
		{
			string talkerADVCharaID = this.m_Player.GetTalkerADVCharaID(m_TextID);
			if (talkerADVCharaID != null)
			{
				this.m_StandPicManager.GetStandPic(talkerADVCharaID).ApplyFace((eADVFace)m_FaceID);
				this.m_Player.PlayEmotion(talkerADVCharaID, m_EmotionID, (eADVEmotionPosition)m_EmotionPos);
			}
			this.CharaTalk(m_TextID);
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x0000BBC5 File Offset: 0x00009FC5
		public override void CharaTalk(uint m_TextID, int m_FaceID, string m_EmotionID)
		{
			this.CharaTalk(m_TextID, m_FaceID, m_EmotionID, 0);
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x0000BBD4 File Offset: 0x00009FD4
		public override void CharaTalk(uint m_TextID, int m_FaceID)
		{
			string talkerADVCharaID = this.m_Player.GetTalkerADVCharaID(m_TextID);
			if (talkerADVCharaID != null)
			{
				this.m_StandPicManager.GetStandPic(talkerADVCharaID).ApplyFace((eADVFace)m_FaceID);
			}
			this.CharaTalk(m_TextID);
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0000BC12 File Offset: 0x0000A012
		public override void CharaTalk(uint m_TextID)
		{
			this.m_Player.CharaTalk(m_TextID);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0000BC2C File Offset: 0x0000A02C
		public override void CloseTalk()
		{
			this.m_Player.CloseTalk();
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000BC39 File Offset: 0x0000A039
		public override void ClearNovelText()
		{
			this.m_Player.ClearNovelText();
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0000BC46 File Offset: 0x0000A046
		public override void NovelAnchorSetting(int m_AnchorID, float m_X, float m_Y)
		{
			this.m_Player.SetNovelAnchor((eADVAnchor)m_AnchorID, new Vector2(m_X, m_Y));
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000BC5B File Offset: 0x0000A05B
		public override void NovelAnchorSettingDefault()
		{
			this.m_Player.SetNovelAnchorDefault();
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0000BC68 File Offset: 0x0000A068
		public override void AddNovelText(uint m_TextID)
		{
			this.m_Player.AddNovelText(m_TextID);
			this.m_Player.BreakFlag = true;
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0000BC82 File Offset: 0x0000A082
		public override void NovelInsertLine()
		{
			this.m_Player.NovelInsertLine();
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x0000BC8F File Offset: 0x0000A08F
		public override void WarpPrepare(string m_ADVCharaID, float m_Sec)
		{
			this.m_Player.AddPicForPixelCrash(this.m_StandPicManager.GetStandPic(m_ADVCharaID));
			this.m_Player.StartPixelCrashPrepare(m_Sec);
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000BCB4 File Offset: 0x0000A0B4
		public override void WarpPrepareWait()
		{
			this.m_Player.WaitPixelCrashPrepare();
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0000BCC1 File Offset: 0x0000A0C1
		public override void WarpStart(float m_Sec)
		{
			this.m_Player.StartPixelCrash(m_Sec);
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000BCCF File Offset: 0x0000A0CF
		public override void WarpWait()
		{
			this.m_Player.WaitPixelCrash();
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0000BCDC File Offset: 0x0000A0DC
		public override void SetHDRFactor(string m_ADVCharaID, float m_Value, float m_Sec)
		{
			this.m_Player.StandPicManager.GetStandPic(m_ADVCharaID).SetHDRFactor(m_Value, m_Sec);
		}

		// Token: 0x0400012D RID: 301
		private ADVPlayer m_Player;

		// Token: 0x0400012E RID: 302
		private StandPicManager m_StandPicManager;

		// Token: 0x0400012F RID: 303
		private ADVSpriteManager m_SpriteManager;

		// Token: 0x04000130 RID: 304
		private ADVBackGroundManager m_BackGroundManager;

		// Token: 0x04000131 RID: 305
		private ADVSoundManager m_SoundManager;

		// Token: 0x04000132 RID: 306
		private string[] m_TargetADVCharaIDs = new string[11];

		// Token: 0x04000133 RID: 307
		protected ADVScriptCommandInherited.CharaPositions positions;

		// Token: 0x02000028 RID: 40
		public class VariableArrayBase<vType>
		{
			// Token: 0x060001F6 RID: 502 RVA: 0x0000BCF8 File Offset: 0x0000A0F8
			public VariableArrayBase(params vType[] in_params)
			{
				this.value = new vType[11];
				for (int i = 0; i < 11; i++)
				{
					if (i < in_params.Length)
					{
						this.value[i] = in_params[i];
					}
					else
					{
						this.value[i] = this.GetDefaultValue(i);
					}
				}
			}

			// Token: 0x060001F7 RID: 503 RVA: 0x0000BD60 File Offset: 0x0000A160
			protected virtual vType GetDefaultValue(int index)
			{
				return default(vType);
			}

			// Token: 0x04000134 RID: 308
			public vType[] value;
		}

		// Token: 0x02000029 RID: 41
		public class CharaIDs : ADVScriptCommandInherited.VariableArrayBase<string>
		{
			// Token: 0x060001F8 RID: 504 RVA: 0x0000BD76 File Offset: 0x0000A176
			public CharaIDs(params string[] in_params) : base(in_params)
			{
			}

			// Token: 0x060001F9 RID: 505 RVA: 0x0000BD7F File Offset: 0x0000A17F
			protected override string GetDefaultValue(int index)
			{
				return string.Empty;
			}
		}

		// Token: 0x0200002A RID: 42
		public class CharaPositions : ADVScriptCommandInherited.VariableArrayBase<eADVStandPosition>
		{
			// Token: 0x060001FA RID: 506 RVA: 0x0000BD86 File Offset: 0x0000A186
			public CharaPositions(params eADVStandPosition[] in_params) : base(in_params)
			{
			}

			// Token: 0x060001FB RID: 507 RVA: 0x0000BD90 File Offset: 0x0000A190
			protected override eADVStandPosition GetDefaultValue(int index)
			{
				int num = index;
				if (11 < num)
				{
					num -= 10;
				}
				return (eADVStandPosition)num;
			}
		}
	}
}
