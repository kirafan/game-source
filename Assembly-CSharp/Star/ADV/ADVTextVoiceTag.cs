﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000047 RID: 71
	public class ADVTextVoiceTag : ADVTextVoiceTagBase
	{
		// Token: 0x060002A5 RID: 677 RVA: 0x0000F705 File Offset: 0x0000DB05
		public ADVTextVoiceTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000F710 File Offset: 0x0000DB10
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int num2 = tag.IndexOf(",") - num;
				string advcharaID = tag.Substring(num, num2);
				tag.Remove(num, num2);
				num = tag.IndexOf(",") + 1;
				num2 = tag.IndexOf(">") - num;
				string text2 = tag.Substring(num, num2);
				ADVCharacterListDB_Param advcharacterParam = this.m_Player.GetADVCharacterParam(advcharaID);
				eCharaNamedType namedType = (eCharaNamedType)advcharacterParam.m_NamedType;
				if (namedType == eCharaNamedType.None)
				{
					this.m_Player.SoundManager.PlayVOICE(advcharacterParam.m_CueSheet, text2);
				}
				else if (!string.IsNullOrEmpty(text2))
				{
					this.m_Player.SoundManager.PlayVOICE(namedType, text2);
				}
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
