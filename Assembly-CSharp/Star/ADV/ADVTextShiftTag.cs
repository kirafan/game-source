﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000063 RID: 99
	public class ADVTextShiftTag : ADVTextWaitTagBase
	{
		// Token: 0x060002D4 RID: 724 RVA: 0x0000FFFF File Offset: 0x0000E3FF
		public ADVTextShiftTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x0001000C File Offset: 0x0000E40C
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			base.DeleteTag(ref text, ref idx, tag.Length);
			if (execTag && !this.m_IsEndTag)
			{
				int num = text.IndexOf('\n');
				if (num == -1)
				{
					text = text.Substring(idx);
					if (text[0] == '\n')
					{
						text = text.Substring(1);
					}
					idx = 0;
				}
				else
				{
					text = text.Substring(num);
					if (text[0] == '\n')
					{
						text = text.Substring(1);
					}
					idx -= num;
				}
				result = true;
			}
			return result;
		}
	}
}
