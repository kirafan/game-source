﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x0200002E RID: 46
	public class ADVSoundManager
	{
		// Token: 0x0600020E RID: 526 RVA: 0x0000C344 File Offset: 0x0000A744
		public void Prepare(ADVPlayer player)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Release();
			this.m_Player = player;
			bool flag = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.BGM_adv);
			if (flag)
			{
				this.m_LoadedCueSheetList.Add(eSoundCueSheetDB.BGM_adv);
			}
			flag = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.ADV);
			if (flag)
			{
				this.m_LoadedCueSheetList.Add(eSoundCueSheetDB.ADV);
			}
		}

		// Token: 0x0600020F RID: 527 RVA: 0x0000C3AD File Offset: 0x0000A7AD
		public bool IsDonePrepare()
		{
			return !SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets();
		}

		// Token: 0x06000210 RID: 528 RVA: 0x0000C3C8 File Offset: 0x0000A7C8
		public void Update()
		{
			SoundManager soundMng = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng;
			List<SoundHandler> list = new List<SoundHandler>();
			for (int i = 0; i < this.m_SEList.Count; i++)
			{
				if (!this.m_SEList[i].IsPlaying(false))
				{
					soundMng.RemoveHndl(this.m_SEList[i], true);
					list.Add(this.m_SEList[i]);
				}
			}
			while (list.Count > 0)
			{
				this.m_SEList.Remove(list[0]);
				list.RemoveAt(0);
			}
			list.Clear();
			for (int j = 0; j < this.m_VOICEList.Count; j++)
			{
				if (!this.m_VOICEList[j].IsPlaying(false))
				{
					soundMng.RemoveHndl(this.m_VOICEList[j], true);
					list.Add(this.m_VOICEList[j]);
				}
			}
			while (list.Count > 0)
			{
				this.m_VOICEList.Remove(list[0]);
				list.RemoveAt(0);
			}
		}

		// Token: 0x06000211 RID: 529 RVA: 0x0000C4F4 File Offset: 0x0000A8F4
		public void UnloadCueSheet()
		{
			SoundManager soundMng = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng;
			for (int i = 0; i < this.m_SEList.Count; i++)
			{
				soundMng.RemoveHndl(this.m_SEList[i], true);
			}
			this.m_SEList.Clear();
			for (int j = 0; j < this.m_VOICEList.Count; j++)
			{
				soundMng.RemoveHndl(this.m_VOICEList[j], true);
			}
			this.m_VOICEList.Clear();
			soundMng.UnloadCueSheet(eSoundCueSheetDB.BGM_adv);
			for (int k = 0; k < this.m_LoadedCueSheetList.Count; k++)
			{
				soundMng.UnloadCueSheet(this.m_LoadedCueSheetList[k]);
			}
			for (int l = 0; l < this.m_LoadedVOICECueSheetList.Count; l++)
			{
				soundMng.UnloadVoiceCueSheet(this.m_LoadedVOICECueSheetList[l]);
			}
		}

		// Token: 0x06000212 RID: 530 RVA: 0x0000C5EC File Offset: 0x0000A9EC
		public void PlaySE(string cueName)
		{
			if (this.m_LoadedCueSheetList.IndexOf(eSoundCueSheetDB.ADV) >= 0)
			{
				string cueSheet = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SoundCueSheetDB.GetParam(eSoundCueSheetDB.ADV).m_CueSheet;
				SoundHandler soundHandler = new SoundHandler();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(cueSheet, cueName, soundHandler, 1f, 0, -1, -1);
				this.m_SEList.Add(soundHandler);
			}
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0000C658 File Offset: 0x0000AA58
		public void StopAllSE()
		{
			for (int i = 0; i < this.m_SEList.Count; i++)
			{
				this.m_SEList[i].Stop();
			}
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0000C693 File Offset: 0x0000AA93
		public bool IsPlayingSE()
		{
			return this.m_SEList.Count > 0;
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000C6A4 File Offset: 0x0000AAA4
		public void LoadVOICECharaID(string advCharaID)
		{
			Debug.Log("LoadVoice : " + advCharaID);
			if (this.m_Player.GetADVCharacterParam(advCharaID).m_NamedType != -1)
			{
				eCharaNamedType namedType = (eCharaNamedType)this.m_Player.GetADVCharacterParam(advCharaID).m_NamedType;
				if (namedType != eCharaNamedType.None)
				{
					this.LoadVOICECueSheet(namedType);
				}
			}
			else
			{
				string cueSheet = this.m_Player.GetADVCharacterParam(advCharaID).m_CueSheet;
				if (!string.IsNullOrEmpty(cueSheet))
				{
					this.LoadVOICECueSheet(cueSheet);
				}
			}
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0000C72C File Offset: 0x0000AB2C
		private void LoadVOICECueSheet(eCharaNamedType namedType)
		{
			string voiceCueSheet = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetVoiceCueSheet(namedType);
			this.LoadVOICECueSheet(voiceCueSheet);
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0000C751 File Offset: 0x0000AB51
		private void LoadVOICECueSheet(string cueSheet)
		{
			if (!this.m_LoadedVOICECueSheetList.Contains(cueSheet))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadVoiceCueSheet(cueSheet);
				this.m_LoadedVOICECueSheetList.Add(cueSheet);
			}
		}

		// Token: 0x06000218 RID: 536 RVA: 0x0000C784 File Offset: 0x0000AB84
		public void PlayVOICE(eCharaNamedType namedType, eSoundVoiceListDB cue)
		{
			SoundHandler soundHandler = new SoundHandler();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(namedType, cue, soundHandler, 1f, 0, -1, -1);
			this.m_VOICEList.Add(soundHandler);
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0000C7C0 File Offset: 0x0000ABC0
		public void PlayVOICE(eCharaNamedType namedType, string cue)
		{
			SoundHandler soundHandler = new SoundHandler();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetVoiceCueSheet(namedType), cue, soundHandler, 1f, 0, -1, -1);
			this.m_VOICEList.Add(soundHandler);
		}

		// Token: 0x0600021A RID: 538 RVA: 0x0000C80C File Offset: 0x0000AC0C
		public void PlayVOICE(string cueSheet, string cue)
		{
			SoundHandler soundHandler = new SoundHandler();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(cueSheet, cue, soundHandler, 1f, 0, -1, -1);
			this.m_VOICEList.Add(soundHandler);
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000C848 File Offset: 0x0000AC48
		public void StopAllVOICE()
		{
			for (int i = 0; i < this.m_SEList.Count; i++)
			{
				this.m_VOICEList[i].Stop();
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0000C883 File Offset: 0x0000AC83
		public bool IsPlayingVOICE()
		{
			return this.m_VOICEList.Count > 0;
		}

		// Token: 0x04000148 RID: 328
		private List<SoundHandler> m_VOICEList = new List<SoundHandler>();

		// Token: 0x04000149 RID: 329
		private List<SoundHandler> m_SEList = new List<SoundHandler>();

		// Token: 0x0400014A RID: 330
		private List<string> m_LoadedVOICECueSheetList = new List<string>();

		// Token: 0x0400014B RID: 331
		private List<eSoundCueSheetDB> m_LoadedCueSheetList = new List<eSoundCueSheetDB>();

		// Token: 0x0400014C RID: 332
		private ADVPlayer m_Player;
	}
}
