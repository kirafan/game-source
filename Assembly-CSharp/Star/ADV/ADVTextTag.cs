﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x02000037 RID: 55
	public class ADVTextTag
	{
		// Token: 0x06000283 RID: 643 RVA: 0x0000F2C0 File Offset: 0x0000D6C0
		protected ADVTextTag(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType)
		{
			this.m_Player = player;
			this.m_ParentParser = parentParser;
			this.m_EventType = eventType;
			this.m_TagType = tagType;
			this.m_IsTapWaitWithTap = false;
		}

		// Token: 0x06000284 RID: 644 RVA: 0x0000F2EC File Offset: 0x0000D6EC
		public virtual bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (this.IsExecuteAnalyzeGeneral(ref text, ref idx, ref evList, tag, execTag, calcTextObj, ref rubyList))
			{
				result = this.AnalyzeProcess(ref text, ref idx, ref evList, tag, execTag, calcTextObj, ref rubyList);
			}
			this.AnalyzeEndProcess(ref text, ref idx, ref evList, tag, execTag, calcTextObj, ref rubyList);
			return result;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000F335 File Offset: 0x0000D735
		public virtual bool IsExecuteAnalyzeGeneral(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return execTag;
		}

		// Token: 0x06000286 RID: 646 RVA: 0x0000F339 File Offset: 0x0000D739
		public virtual bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			return false;
		}

		// Token: 0x06000287 RID: 647 RVA: 0x0000F33C File Offset: 0x0000D73C
		public virtual void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			this.DeleteTag(ref text, ref idx, tag.Length);
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000F350 File Offset: 0x0000D750
		protected List<string> ParseTag(string tag)
		{
			List<string> list = new List<string>();
			string text = tag;
			string text2 = "=";
			int num;
			while (0 <= (num = text.IndexOf(text2)) || text2 != ">")
			{
				if (num < 0)
				{
					text2 = ">";
				}
				else
				{
					if (text2 != "=")
					{
						list.Add(text.Substring(0, num));
					}
					text = text.Remove(0, num + 1);
					if (text2 == "=")
					{
						text2 = ",";
					}
				}
			}
			return list;
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000F3E3 File Offset: 0x0000D7E3
		protected void DeleteTag(ref string text, ref int idx, int charNum)
		{
			text = text.Remove(idx - charNum, charNum);
			idx -= charNum;
			if (idx > text.Length)
			{
				idx = text.Length;
			}
		}

		// Token: 0x0600028A RID: 650 RVA: 0x0000F410 File Offset: 0x0000D810
		public void SetTapWaitWithTapFlag(bool flag)
		{
			this.m_IsTapWaitWithTap = flag;
		}

		// Token: 0x0600028B RID: 651 RVA: 0x0000F419 File Offset: 0x0000D819
		public ADVParser.EventType GetEventType()
		{
			return this.m_EventType;
		}

		// Token: 0x0600028C RID: 652 RVA: 0x0000F421 File Offset: 0x0000D821
		public ADVTextTag.eTagType GetTagType()
		{
			return this.m_TagType;
		}

		// Token: 0x04000189 RID: 393
		protected ADVPlayer m_Player;

		// Token: 0x0400018A RID: 394
		protected ADVParser m_ParentParser;

		// Token: 0x0400018B RID: 395
		protected ADVParser.EventType m_EventType;

		// Token: 0x0400018C RID: 396
		protected ADVTextTag.eTagType m_TagType;

		// Token: 0x0400018D RID: 397
		protected bool m_IsTapWaitWithTap;

		// Token: 0x02000038 RID: 56
		public enum eTagType
		{
			// Token: 0x0400018F RID: 399
			Error,
			// Token: 0x04000190 RID: 400
			OneShotAction,
			// Token: 0x04000191 RID: 401
			RangeBegin,
			// Token: 0x04000192 RID: 402
			RangeEnd
		}
	}
}
