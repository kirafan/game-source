﻿using System;

namespace Star.ADV
{
	// Token: 0x0200003A RID: 58
	public class ADVTextRangeTagBase : ADVTextTag
	{
		// Token: 0x0600028E RID: 654 RVA: 0x0000F435 File Offset: 0x0000D835
		protected ADVTextRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType) : base(player, parentParser, eventType, tagType)
		{
			this.m_IsEndTag = (tagType == ADVTextTag.eTagType.RangeEnd);
		}

		// Token: 0x0600028F RID: 655 RVA: 0x0000F457 File Offset: 0x0000D857
		protected ADVTextRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, bool isEndTag) : base(player, parentParser, eventType, (!isEndTag) ? ADVTextTag.eTagType.RangeBegin : ADVTextTag.eTagType.RangeEnd)
		{
			this.m_IsEndTag = isEndTag;
		}

		// Token: 0x04000193 RID: 403
		protected bool m_IsEndTag;
	}
}
