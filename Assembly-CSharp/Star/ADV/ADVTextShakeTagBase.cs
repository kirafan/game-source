﻿using System;

namespace Star.ADV
{
	// Token: 0x0200005D RID: 93
	public class ADVTextShakeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002CB RID: 715 RVA: 0x0000FE85 File Offset: 0x0000E285
		protected ADVTextShakeTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_shake, isEndTag)
		{
		}
	}
}
