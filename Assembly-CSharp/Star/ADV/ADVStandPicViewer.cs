﻿using System;
using System.Collections.Generic;
using Star.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Star.ADV
{
	// Token: 0x0200026F RID: 623
	public class ADVStandPicViewer : MonoBehaviour
	{
		// Token: 0x06000B92 RID: 2962 RVA: 0x00043708 File Offset: 0x00041B08
		private void Update()
		{
			this.m_DebugMenu.alpha = this.m_Slider.value;
			switch (this.m_State)
			{
			case ADVStandPicViewer.eState.PrepareDB:
				if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.NowLoadingOnly, eTipsCategory.None);
					this.m_Player.PrepareDB();
					this.m_State = ADVStandPicViewer.eState.WaitPrepareDB;
				}
				break;
			case ADVStandPicViewer.eState.WaitPrepareDB:
				this.m_Player.UpdatePrepareDB();
				if (this.m_Player.IsDonePrepareDB())
				{
					this.m_State = ADVStandPicViewer.eState.Prepare;
				}
				break;
			case ADVStandPicViewer.eState.Prepare:
				this.m_Player.Prepare();
				this.m_Player.BackGroundManager.Load("BG_Adv_Com_01_01");
				this.m_State = ADVStandPicViewer.eState.WaitPrepare;
				break;
			case ADVStandPicViewer.eState.WaitPrepare:
				this.m_Player.UpdatePrepare();
				if (this.m_Player.IsDonePrepare)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Player.BackGroundManager.Visible(0U, "BG_Adv_Com_01_01");
					this.m_Player.Fade(new Color(0f, 0f, 0f, 0f), 0f);
					this.m_State = ADVStandPicViewer.eState.None;
				}
				break;
			case ADVStandPicViewer.eState.UpdateCharaLoad:
				if (this.m_Player.StandPicManager.IsDoneApplyPose())
				{
					for (int i = 0; i < this.m_CharaPanels.Length; i++)
					{
						if (!string.IsNullOrEmpty(this.m_CharaPanels[i].GetADVCharaID()))
						{
							this.m_Player.StandPicManager.GetStandPic(this.m_CharaPanels[i].GetADVCharaID()).ApplyFace(this.m_CharaPanels[i].m_Face);
							this.m_Player.StandPicManager.SetChara(this.m_CharaPanels[i].GetADVCharaID(), this.m_CharaPanels[i].m_StandPosition, 0f, 0f, eADVCurveType.Linear);
						}
					}
					this.m_State = ADVStandPicViewer.eState.None;
				}
				break;
			case ADVStandPicViewer.eState.UpdateEmo:
				if (this.m_Player.ADVEffectManager.IsDonePrepare())
				{
					List<ADVStandPic> standPicList = this.m_Player.StandPicManager.GetStandPicList();
					for (int j = 0; j < standPicList.Count; j++)
					{
						this.m_Player.PlayEmotion(standPicList[j].ADVCharaID, this.m_EmoPanel.GetEmoID(), this.m_EmoPanel.GetEmoPosition());
					}
					this.m_State = ADVStandPicViewer.eState.None;
				}
				break;
			}
		}

		// Token: 0x06000B93 RID: 2963 RVA: 0x00043994 File Offset: 0x00041D94
		public void CharaLoad()
		{
			if (this.m_State != ADVStandPicViewer.eState.None)
			{
				Debug.LogWarning("m_State:" + this.m_State);
				return;
			}
			this.m_Player.StandPicManager.ClearNeedList();
			this.m_Player.StandPicManager.DestroyAllNotNeedStandPic();
			for (int i = 0; i < this.m_CharaPanels.Length; i++)
			{
				if (!string.IsNullOrEmpty(this.m_CharaPanels[i].GetADVCharaID()))
				{
					this.m_Player.AddNeedStandPic(0, this.m_CharaPanels[i].GetADVCharaID(), null);
				}
			}
			this.m_Player.StandPicManager.CreateAllNeedStandPic();
			for (int j = 0; j < this.m_CharaPanels.Length; j++)
			{
				if (!string.IsNullOrEmpty(this.m_CharaPanels[j].GetADVCharaID()))
				{
					this.m_Player.StandPicManager.GetStandPic(this.m_CharaPanels[j].GetADVCharaID()).ApplyPose(0);
				}
			}
			this.m_State = ADVStandPicViewer.eState.UpdateCharaLoad;
		}

		// Token: 0x06000B94 RID: 2964 RVA: 0x00043A9C File Offset: 0x00041E9C
		public void PlayEmo()
		{
			if (this.m_State != ADVStandPicViewer.eState.None)
			{
				Debug.LogWarning("m_State:" + this.m_State);
				return;
			}
			this.m_Player.ClearNeedEffect();
			this.m_Player.AddNeedEmo(this.m_EmoPanel.GetEmoID());
			this.m_Player.UnloadAllNotNeedEffect();
			this.m_Player.LoadAllNeedEffect();
			this.m_State = ADVStandPicViewer.eState.UpdateEmo;
		}

		// Token: 0x06000B95 RID: 2965 RVA: 0x00043B0D File Offset: 0x00041F0D
		public void ToggleTextWindow()
		{
			if (this.m_ToggleWindow.isOn)
			{
				this.m_Player.OpenTalk();
			}
			else
			{
				this.m_Player.CloseTalk();
			}
		}

		// Token: 0x040013FE RID: 5118
		public ADVPlayer m_Player;

		// Token: 0x040013FF RID: 5119
		public CanvasGroup m_DebugMenu;

		// Token: 0x04001400 RID: 5120
		public Slider m_Slider;

		// Token: 0x04001401 RID: 5121
		public ADVDebugCharaSelectPanel[] m_CharaPanels;

		// Token: 0x04001402 RID: 5122
		public ADVDebugEmotionPanel m_EmoPanel;

		// Token: 0x04001403 RID: 5123
		public Toggle m_ToggleWindow;

		// Token: 0x04001404 RID: 5124
		private ADVStandPicViewer.eState m_State = ADVStandPicViewer.eState.PrepareDB;

		// Token: 0x02000270 RID: 624
		public enum eState
		{
			// Token: 0x04001406 RID: 5126
			None,
			// Token: 0x04001407 RID: 5127
			PrepareDB,
			// Token: 0x04001408 RID: 5128
			WaitPrepareDB,
			// Token: 0x04001409 RID: 5129
			Prepare,
			// Token: 0x0400140A RID: 5130
			WaitPrepare,
			// Token: 0x0400140B RID: 5131
			UpdateCharaLoad,
			// Token: 0x0400140C RID: 5132
			UpdateEmo
		}
	}
}
