﻿using System;

namespace Star.ADV
{
	// Token: 0x02000042 RID: 66
	public class ADVTextRubyTagEnd : ADVTextRubyTagBase
	{
		// Token: 0x0600029E RID: 670 RVA: 0x0000F64F File Offset: 0x0000DA4F
		public ADVTextRubyTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}
	}
}
