﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200004F RID: 79
	public class ADVTextMotionTag : ADVTextMotionTagBase
	{
		// Token: 0x060002B3 RID: 691 RVA: 0x0000FA38 File Offset: 0x0000DE38
		public ADVTextMotionTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000FA44 File Offset: 0x0000DE44
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			bool result = false;
			if (execTag && !this.m_IsEndTag)
			{
				int num = tag.IndexOf("=") + 1;
				int num2 = tag.IndexOf(",") - num;
				string advCharaID = tag.Substring(num, num2);
				tag.Remove(num, num2);
				num = tag.IndexOf(",") + 1;
				num2 = tag.IndexOf(">") - num;
				string motion = tag.Substring(num, num2);
				this.m_Player.StandPicManager.GetStandPic(advCharaID).PlayMotion(motion);
				result = true;
			}
			base.DeleteTag(ref text, ref idx, tag.Length);
			return result;
		}
	}
}
