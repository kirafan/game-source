﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.ADV
{
	// Token: 0x02000012 RID: 18
	public class ADVEffectManager : MonoBehaviour
	{
		// Token: 0x0600006F RID: 111 RVA: 0x0000487D File Offset: 0x00002C7D
		public void SetEnablePlaySE(bool flg)
		{
			this.m_IsPlaySE = flg;
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00004886 File Offset: 0x00002C86
		public void Prepare(ADVPlayer player)
		{
			this.m_NeedList = new List<string>();
			this.m_LoadedList = new List<string>();
			this.m_Player = player;
			this.m_EffectHandlerList = new List<ADVEffectManager.ADVEffectHandlerWrapper>();
			this.m_WaitInfoList = new List<ADVEffectManager.WaitPrecedingEffectInformation>();
		}

		// Token: 0x06000071 RID: 113 RVA: 0x000048BB File Offset: 0x00002CBB
		public bool IsDonePrepare()
		{
			return !SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny();
		}

		// Token: 0x06000072 RID: 114 RVA: 0x000048D4 File Offset: 0x00002CD4
		private void Update()
		{
			if (this.m_EffectHandlerList != null)
			{
				for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
				{
					if ((this.m_EffectHandlerList[i].GetPlayState() == ADVEffectManager.eEffectPlayState.Play || this.m_EffectHandlerList[i].GetPlayState() == ADVEffectManager.eEffectPlayState.PlayLoop) && !this.m_EffectHandlerList[i].IsEffectPlaying())
					{
						this.m_EffectHandlerList[i].Remove();
					}
				}
			}
			if (this.m_WaitInfoList != null)
			{
				for (int j = 0; j < this.m_WaitInfoList.Count; j++)
				{
					if (this.m_WaitInfoList[j].GetPrecedingEffectState() == ADVEffectManager.eEffectPlayState.Done && (this.m_WaitInfoList[j].GetPrecedingUniqueId() == -1 || this.GetEffectPlayState(this.m_WaitInfoList[j].GetPrecedingUniqueId()) == ADVEffectManager.eEffectPlayState.Done))
					{
						this.PlayProcess(this.m_WaitInfoList[j].GetPlayType(), this.m_WaitInfoList[j].GetEffectUniqueId(), this.m_WaitInfoList[j].GetEffectID(), this.m_WaitInfoList[j].GetPosition(), this.m_WaitInfoList[j].GetRotation(), this.m_WaitInfoList[j].GetPlayMode());
						this.m_WaitInfoList.RemoveAt(j);
						j--;
					}
				}
			}
			if (this.m_Preset != null && this.m_Preset.m_Step < 2 && this.m_EffectHandlerList != null)
			{
				for (int k = 0; k < this.m_EffectHandlerList.Count; k++)
				{
					if (this.m_EffectHandlerList[k].GetPlayState() == ADVEffectManager.eEffectPlayState.Play || this.m_EffectHandlerList[k].GetPlayState() == ADVEffectManager.eEffectPlayState.Done)
					{
						if (this.m_Preset.m_ignoreParticle)
						{
							if (this.m_EffectHandlerList[k].GetEffectHandler().GetPlayingSec() >= this.m_EffectHandlerList[k].GetEffectHandler().GetMaxSec())
							{
								if (this.m_Preset.m_Step == 0 && this.m_EffectHandlerList[k].GetEffectId() == this.m_Preset.st)
								{
									this.m_EffectHandlerList[k].GetEffectHandler().DisableOtherThanParticles(0f);
									this.PlayProcess(this.m_EffectHandlerList[k].GetPlayType(), this.m_Preset.lp, this.m_Preset.pos, this.m_Preset.rot, this.m_Preset.isLoop);
									this.m_Preset.m_Step++;
								}
								else if (this.m_Preset.m_Step == 1 && this.m_EffectHandlerList[k].GetEffectId() == this.m_Preset.lp)
								{
									this.m_EffectHandlerList[k].GetEffectHandler().DisableOtherThanParticles(0f);
									this.PlayProcess(this.m_EffectHandlerList[k].GetPlayType(), this.m_Preset.ed, this.m_Preset.pos, this.m_Preset.rot, false);
									this.m_Preset.m_Step++;
								}
							}
						}
						else if (!this.m_EffectHandlerList[k].IsEffectPlaying() || this.m_EffectHandlerList[k].GetPlayState() == ADVEffectManager.eEffectPlayState.Done)
						{
							if (this.m_Preset.m_Step == 0 && this.m_EffectHandlerList[k].GetEffectId() == this.m_Preset.st)
							{
								this.m_EffectHandlerList[k].GetEffectHandler().DisableOtherThanParticles(0f);
								this.PlayProcess(this.m_EffectHandlerList[k].GetPlayType(), this.m_Preset.lp, this.m_Preset.pos, this.m_Preset.rot, this.m_Preset.isLoop);
								this.m_Preset.m_Step++;
							}
							else if (this.m_Preset.m_Step == 1 && this.m_EffectHandlerList[k].GetEffectId() == this.m_Preset.lp)
							{
								this.m_EffectHandlerList[k].GetEffectHandler().DisableOtherThanParticles(0f);
								this.PlayProcess(this.m_EffectHandlerList[k].GetPlayType(), this.m_Preset.ed, this.m_Preset.pos, this.m_Preset.rot, false);
								this.m_Preset.m_Step++;
							}
						}
					}
				}
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00004DC8 File Offset: 0x000031C8
		private void LateUpdate()
		{
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00004DCA File Offset: 0x000031CA
		public void ClearNeedList()
		{
			this.m_NeedList.Clear();
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00004DD7 File Offset: 0x000031D7
		public void AddNeedList(string effectID)
		{
			this.m_NeedList.Add(effectID);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00004DE5 File Offset: 0x000031E5
		public void AddNeedListByEmotionID(string emotionID)
		{
			this.m_NeedList.Add(this.GetEffectIDByEmotionID(emotionID));
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00004DFC File Offset: 0x000031FC
		public void UnloadAllNotNeedEffect()
		{
			this.StopAllEffect();
			EffectManager effectMng = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
			List<string> list = new List<string>();
			list.Clear();
			for (int i = 0; i < this.m_LoadedList.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < this.m_NeedList.Count; j++)
				{
					if (this.m_LoadedList[i] == this.m_NeedList[j])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					effectMng.UnloadSingleEffect(this.m_LoadedList[i]);
					list.Add(this.m_LoadedList[i]);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				this.m_LoadedList.Remove(list[k]);
			}
			list.Clear();
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00004EF0 File Offset: 0x000032F0
		public void LoadAllNeedEffect()
		{
			for (int i = 0; i < this.m_NeedList.Count; i++)
			{
				this.LoadEffect(this.m_NeedList[i]);
			}
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00004F2C File Offset: 0x0000332C
		public void LoadEffect(string effectID)
		{
			for (int i = 0; i < this.m_LoadedList.Count; i++)
			{
				if (this.m_LoadedList[i] == effectID)
				{
					return;
				}
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadSingleEffect(effectID, true))
			{
				this.m_LoadedList.Add(effectID);
			}
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00004F90 File Offset: 0x00003390
		public void DestroyAllEffect()
		{
			this.StopAllEffect();
			EffectManager effectMng = SingletonMonoBehaviour<GameSystem>.Inst.EffectMng;
			for (int i = 0; i < this.m_LoadedList.Count; i++)
			{
				effectMng.UnloadSingleEffect(this.m_LoadedList[i]);
			}
			effectMng.UnloadPack("ef_ADV_emo");
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00004FE8 File Offset: 0x000033E8
		private string GetEffectIDByEmotionID(string emotionID)
		{
			return this.m_Player.GetADVEmotionParam(emotionID).m_EffectID;
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00005009 File Offset: 0x00003409
		public void Play(string effectID, Vector3 position, Quaternion rotation, bool loop = false)
		{
			this.PlayProcess(ADVEffectManager.ePlayType.Normal, effectID, position, rotation, loop);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00005018 File Offset: 0x00003418
		public void PlayScreen(string effectID, Vector3 position, Quaternion rotation, bool loop = false)
		{
			this.PlayProcess(ADVEffectManager.ePlayType.Screen, effectID, position, rotation, loop);
		}

		// Token: 0x0600007E RID: 126 RVA: 0x00005027 File Offset: 0x00003427
		public void PlayEmo(string emotionID, Vector3 position, Quaternion rotation, bool loop = false)
		{
			this.PlayProcess(ADVEffectManager.ePlayType.Emo, this.GetEffectIDByEmotionID(emotionID), position, rotation, loop);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000503C File Offset: 0x0000343C
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, bool loop = false)
		{
			if (loop)
			{
				return this.PlayLoopProcess(playType, effectID, position, rotation);
			}
			return this.PlayNormalProcess(playType, effectID, position, rotation);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000505C File Offset: 0x0000345C
		private ADVEffectManager.ADVEffectHandlerWrapper PlayNormalProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation)
		{
			return this.PlayProcess(playType, effectID, position, rotation, WrapMode.ClampForever);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x0000506A File Offset: 0x0000346A
		private ADVEffectManager.ADVEffectHandlerWrapper PlayLoopProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation)
		{
			return this.PlayProcess(playType, effectID, position, rotation, WrapMode.Loop);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00005078 File Offset: 0x00003478
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever)
		{
			return this.PlayProcess(playType, -1, effectID, position, rotation, playMode);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00005088 File Offset: 0x00003488
		public void PlayAndReserveNextEffect(ADVEffectManager.ePlayType playType, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode, ADVEffectManager.ePlayType nextPlayType, string nextEffectID, WrapMode nextPlayMode)
		{
			this.PlayAndReserveNextEffect(new ADVEffectManager.PlayProcessArgument(playType, -1, effectID, position, rotation, playMode), new ADVEffectManager.PlayProcessArgument(nextPlayType, -1, nextEffectID, position, rotation, nextPlayMode));
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000050AC File Offset: 0x000034AC
		public void PlayAndReserveNextEffect(ADVEffectManager.PlayProcessArgument argument, ADVEffectManager.PlayProcessArgument nextEffectArgument)
		{
			this.AddWaitPrecedingEffectInformation(new ADVEffectManager.WaitPrecedingEffectInformation(this.PlayProcess(argument), -1, nextEffectArgument));
		}

		// Token: 0x06000085 RID: 133 RVA: 0x000050C2 File Offset: 0x000034C2
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever)
		{
			return this.PlayProcess(new ADVEffectManager.PlayProcessArgument(playType, effectUniqueId, effectID, position, rotation, playMode));
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000050D8 File Offset: 0x000034D8
		private ADVEffectManager.ADVEffectHandlerWrapper PlayProcess(ADVEffectManager.PlayProcessArgument argument)
		{
			ADVEffectManager.eEffectPlayState eEffectPlayState = ADVEffectManager.eEffectPlayState.None;
			if (-1 < argument.GetEffectUniqueId())
			{
				eEffectPlayState = this.GetEffectPlayState(argument.GetEffectUniqueId());
			}
			if (eEffectPlayState != ADVEffectManager.eEffectPlayState.None)
			{
				if (eEffectPlayState == ADVEffectManager.eEffectPlayState.Play || eEffectPlayState == ADVEffectManager.eEffectPlayState.PlayLoop)
				{
					this.Stop(this.GetEffect(argument.GetEffectUniqueId()).GetEffectId());
				}
				if (eEffectPlayState == ADVEffectManager.eEffectPlayState.Done)
				{
					this.ReleaseRemovedEffect(argument.GetEffectUniqueId());
				}
			}
			Vector3 position = argument.GetPosition();
			position.x *= 1f / base.transform.localScale.x;
			position.y *= 1f / base.transform.localScale.y;
			ADVEffectManager.ADVEffectHandlerWrapper adveffectHandlerWrapper = new ADVEffectManager.ADVEffectHandlerWrapper(argument.GetPlayType(), argument.GetEffectUniqueId(), SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.PlayUnManaged(argument.GetEffectID(), position, argument.GetRotation(), base.transform, true, argument.GetPlayMode(), 1, 1f, this.m_IsPlaySE));
			this.m_EffectHandlerList.Add(adveffectHandlerWrapper);
			if (argument.GetPlayType() == ADVEffectManager.ePlayType.Screen)
			{
				float num = 0.18740629f;
				adveffectHandlerWrapper.GetEffectTransform().localScale = new Vector3(num, num, num);
			}
			return adveffectHandlerWrapper;
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00005218 File Offset: 0x00003618
		public void Stop(string effectID)
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetEffectId() == effectID)
				{
					this.m_EffectHandlerList[i].Remove();
				}
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0000526E File Offset: 0x0000366E
		public void StopEmo(string emotionID)
		{
			this.Stop(this.GetEffectIDByEmotionID(emotionID));
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00005280 File Offset: 0x00003680
		public void LoopEnd(string effectID)
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetEffectId() == effectID)
				{
					this.m_EffectHandlerList[i].LoopEnd();
				}
			}
			if (this.m_Preset != null && this.m_Preset.lp == effectID)
			{
				this.m_Preset.isLoop = false;
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x00005304 File Offset: 0x00003704
		public void StopAllEffect()
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				this.m_EffectHandlerList[i].Remove();
			}
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00005340 File Offset: 0x00003740
		public void ReleaseRemovedEffect(int effectUniqueId)
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetUniqueId() == effectUniqueId && this.m_EffectHandlerList[i].GetPlayState() == ADVEffectManager.eEffectPlayState.Done)
				{
					this.m_EffectHandlerList.RemoveAt(i);
					i--;
				}
			}
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000053A8 File Offset: 0x000037A8
		public void ReleaseRemovedEffectAll()
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetPlayState() == ADVEffectManager.eEffectPlayState.Done)
				{
					this.m_EffectHandlerList.RemoveAt(i);
					i--;
				}
			}
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000053F8 File Offset: 0x000037F8
		private List<ADVEffectManager.ADVEffectHandlerWrapper> FilterEffects(ADVEffectManager.eEffectPlayState playState, string effectID)
		{
			List<ADVEffectManager.ADVEffectHandlerWrapper> list = new List<ADVEffectManager.ADVEffectHandlerWrapper>();
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetEffectId() == effectID && this.m_EffectHandlerList[i].GetPlayState() == playState)
				{
					list.Add(this.m_EffectHandlerList[i]);
				}
			}
			return list;
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00005470 File Offset: 0x00003870
		public void AddWaitPrecedingEffectInformation(int precedingUniqueId, ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever)
		{
			this.AddWaitPrecedingEffectInformation(new ADVEffectManager.WaitPrecedingEffectInformation(null, precedingUniqueId, playType, effectUniqueId, effectID, position, rotation, playMode));
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00005494 File Offset: 0x00003894
		protected void AddWaitPrecedingEffectInformation(ADVEffectManager.WaitPrecedingEffectInformation waitPrecedingEffectInfo)
		{
			this.m_WaitInfoList.Add(waitPrecedingEffectInfo);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x000054A4 File Offset: 0x000038A4
		public ADVEffectManager.ADVEffectHandlerWrapper GetEffect(int effectUniqueId)
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetUniqueId() == effectUniqueId)
				{
					return this.m_EffectHandlerList[i];
				}
			}
			return null;
		}

		// Token: 0x06000091 RID: 145 RVA: 0x000054F4 File Offset: 0x000038F4
		public ADVEffectManager.eEffectPlayState GetEffectPlayState(int effectUniqueId)
		{
			for (int i = 0; i < this.m_EffectHandlerList.Count; i++)
			{
				if (this.m_EffectHandlerList[i].GetUniqueId() == effectUniqueId)
				{
					return this.m_EffectHandlerList[i].GetPlayState();
				}
			}
			return ADVEffectManager.eEffectPlayState.None;
		}

		// Token: 0x06000092 RID: 146 RVA: 0x00005547 File Offset: 0x00003947
		public bool IsDonePlaying(int effectUniqueId)
		{
			return this.GetEffectPlayState(effectUniqueId) == ADVEffectManager.eEffectPlayState.Done;
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00005553 File Offset: 0x00003953
		public bool IsDonePlayingAllFromEffectIdIgnoreLoop(string effectID)
		{
			return 0 >= this.FilterEffects(ADVEffectManager.eEffectPlayState.Play, effectID).Count && 0 < this.FilterEffects(ADVEffectManager.eEffectPlayState.Done, effectID).Count;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00005580 File Offset: 0x00003980
		public bool IsDonePlayingAllFromEmotionIdIgnoreLoop(string emotionID)
		{
			return this.IsDonePlayingAllFromEffectIdIgnoreLoop(this.GetEffectIDByEmotionID(emotionID));
		}

		// Token: 0x06000095 RID: 149 RVA: 0x0000558F File Offset: 0x0000398F
		public void PresetEffectLoop(string st, string lp, string ed, bool ignoreParticle)
		{
			this.m_Preset = new ADVEffectManager.EffectPreset();
			this.m_Preset.st = st;
			this.m_Preset.lp = lp;
			this.m_Preset.ed = ed;
			this.m_Preset.m_ignoreParticle = ignoreParticle;
		}

		// Token: 0x06000096 RID: 150 RVA: 0x000055D0 File Offset: 0x000039D0
		public void PlayPreset(Vector3 position, Quaternion rotation)
		{
			this.m_Preset.pos = position;
			this.m_Preset.rot = rotation;
			this.m_Preset.m_Step = 0;
			this.m_Preset.isLoop = true;
			this.PlayProcess(ADVEffectManager.ePlayType.Normal, this.m_Preset.st, position, rotation, false);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00005624 File Offset: 0x00003A24
		public void PlayPresetScreen(Vector3 position, Quaternion rotation)
		{
			this.m_Preset.pos = position;
			this.m_Preset.rot = rotation;
			this.m_Preset.m_Step = 0;
			this.m_Preset.isLoop = true;
			this.PlayProcess(ADVEffectManager.ePlayType.Screen, this.m_Preset.st, position, rotation, false);
		}

		// Token: 0x0400007B RID: 123
		private const float SCREENEFFECT_SIZE = 1334f;

		// Token: 0x0400007C RID: 124
		private const float CAMERA_ORTHOGRAPHIC_SIZE = 0.5f;

		// Token: 0x0400007D RID: 125
		public const int DEFAULT_UNIQUE_EFFECT_ID = -1;

		// Token: 0x0400007E RID: 126
		private ADVPlayer m_Player;

		// Token: 0x0400007F RID: 127
		private List<string> m_NeedList;

		// Token: 0x04000080 RID: 128
		private List<string> m_LoadedList;

		// Token: 0x04000081 RID: 129
		private List<ADVEffectManager.ADVEffectHandlerWrapper> m_EffectHandlerList;

		// Token: 0x04000082 RID: 130
		private List<ADVEffectManager.WaitPrecedingEffectInformation> m_WaitInfoList;

		// Token: 0x04000083 RID: 131
		private bool m_IsPlaySE = true;

		// Token: 0x04000084 RID: 132
		private ADVEffectManager.EffectPreset m_Preset;

		// Token: 0x02000013 RID: 19
		public enum ePlayType
		{
			// Token: 0x04000086 RID: 134
			Error,
			// Token: 0x04000087 RID: 135
			Normal,
			// Token: 0x04000088 RID: 136
			Screen,
			// Token: 0x04000089 RID: 137
			Emo
		}

		// Token: 0x02000014 RID: 20
		public enum eEffectPlayState
		{
			// Token: 0x0400008B RID: 139
			Error,
			// Token: 0x0400008C RID: 140
			None,
			// Token: 0x0400008D RID: 141
			Play,
			// Token: 0x0400008E RID: 142
			PlayLoop,
			// Token: 0x0400008F RID: 143
			Done
		}

		// Token: 0x02000015 RID: 21
		public class ADVEffectHandlerWrapper
		{
			// Token: 0x06000098 RID: 152 RVA: 0x00005677 File Offset: 0x00003A77
			public ADVEffectHandlerWrapper(ADVEffectManager.ePlayType playType, int uniqueId, EffectHandler effectHandler)
			{
				this.m_PlayType = playType;
				this.m_UniqueId = uniqueId;
				this.m_EffectHandler = effectHandler;
				this.m_PlayState = ((effectHandler.m_MeigeAnimCtrl.m_WrapMode != WrapMode.Loop) ? ADVEffectManager.eEffectPlayState.Play : ADVEffectManager.eEffectPlayState.PlayLoop);
			}

			// Token: 0x06000099 RID: 153 RVA: 0x000056B2 File Offset: 0x00003AB2
			public void Remove()
			{
				this.DestroyEffectHandler();
				this.m_PlayState = ADVEffectManager.eEffectPlayState.Done;
			}

			// Token: 0x0600009A RID: 154 RVA: 0x000056C1 File Offset: 0x00003AC1
			public void DestroyEffectHandler()
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_EffectHandler);
			}

			// Token: 0x0600009B RID: 155 RVA: 0x000056D8 File Offset: 0x00003AD8
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return this.m_PlayType;
			}

			// Token: 0x0600009C RID: 156 RVA: 0x000056E0 File Offset: 0x00003AE0
			public int GetUniqueId()
			{
				return this.m_UniqueId;
			}

			// Token: 0x0600009D RID: 157 RVA: 0x000056E8 File Offset: 0x00003AE8
			public string GetEffectId()
			{
				return this.m_EffectHandler.EffectID;
			}

			// Token: 0x0600009E RID: 158 RVA: 0x000056F5 File Offset: 0x00003AF5
			public EffectHandler GetEffectHandler()
			{
				return this.m_EffectHandler;
			}

			// Token: 0x0600009F RID: 159 RVA: 0x000056FD File Offset: 0x00003AFD
			public Transform GetEffectTransform()
			{
				return this.m_EffectHandler.GetComponent<Transform>();
			}

			// Token: 0x060000A0 RID: 160 RVA: 0x0000570A File Offset: 0x00003B0A
			public bool IsEffectPlaying()
			{
				return this.m_EffectHandler.IsPlaying();
			}

			// Token: 0x060000A1 RID: 161 RVA: 0x00005717 File Offset: 0x00003B17
			public ADVEffectManager.eEffectPlayState GetPlayState()
			{
				return this.m_PlayState;
			}

			// Token: 0x060000A2 RID: 162 RVA: 0x00005720 File Offset: 0x00003B20
			public void LoopEnd()
			{
				this.GetEffectHandler().m_MeigeAnimCtrl.m_AnimationPlayTime = this.GetEffectHandler().GetMaxSec() * this.GetEffectHandler().m_MeigeAnimCtrl.m_NormalizedPlayTime;
				this.GetEffectHandler().m_MeigeAnimCtrl.m_WrapMode = WrapMode.ClampForever;
				if (this.GetPlayState() == ADVEffectManager.eEffectPlayState.PlayLoop)
				{
					this.m_PlayState = ADVEffectManager.eEffectPlayState.Play;
				}
			}

			// Token: 0x04000090 RID: 144
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x04000091 RID: 145
			private int m_UniqueId;

			// Token: 0x04000092 RID: 146
			private ADVEffectManager.eEffectPlayState m_PlayState;

			// Token: 0x04000093 RID: 147
			private EffectHandler m_EffectHandler;
		}

		// Token: 0x02000016 RID: 22
		public class WaitPrecedingEffectInformation
		{
			// Token: 0x060000A3 RID: 163 RVA: 0x00005780 File Offset: 0x00003B80
			public WaitPrecedingEffectInformation(ADVEffectManager.ADVEffectHandlerWrapper precedingInstance, int precedingUniqueId, ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever)
			{
				this.m_PrecedingInstance = precedingInstance;
				this.m_PrecedingUniqueId = precedingUniqueId;
				this.m_PlayType = playType;
				this.m_EffectUniqueId = effectUniqueId;
				this.m_EffectID = effectID;
				this.m_Position = position;
				this.m_Rotation = rotation;
				this.m_PlayMode = playMode;
			}

			// Token: 0x060000A4 RID: 164 RVA: 0x000057D0 File Offset: 0x00003BD0
			public WaitPrecedingEffectInformation(ADVEffectManager.ADVEffectHandlerWrapper precedingInstance, int precedingUniqueId, ADVEffectManager.PlayProcessArgument argument)
			{
				this.m_PrecedingInstance = precedingInstance;
				this.m_PrecedingUniqueId = precedingUniqueId;
				this.m_PlayType = argument.GetPlayType();
				this.m_EffectUniqueId = argument.GetEffectUniqueId();
				this.m_EffectID = argument.GetEffectID();
				this.m_Position = argument.GetPosition();
				this.m_Rotation = argument.GetRotation();
				this.m_PlayMode = argument.GetPlayMode();
			}

			// Token: 0x060000A5 RID: 165 RVA: 0x00005839 File Offset: 0x00003C39
			public ADVEffectManager.eEffectPlayState GetPrecedingEffectState()
			{
				return (this.m_PrecedingInstance == null) ? ADVEffectManager.eEffectPlayState.Done : this.m_PrecedingInstance.GetPlayState();
			}

			// Token: 0x060000A6 RID: 166 RVA: 0x00005857 File Offset: 0x00003C57
			public int GetPrecedingUniqueId()
			{
				return this.m_PrecedingUniqueId;
			}

			// Token: 0x060000A7 RID: 167 RVA: 0x0000585F File Offset: 0x00003C5F
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return this.m_PlayType;
			}

			// Token: 0x060000A8 RID: 168 RVA: 0x00005867 File Offset: 0x00003C67
			public int GetEffectUniqueId()
			{
				return this.m_EffectUniqueId;
			}

			// Token: 0x060000A9 RID: 169 RVA: 0x0000586F File Offset: 0x00003C6F
			public string GetEffectID()
			{
				return this.m_EffectID;
			}

			// Token: 0x060000AA RID: 170 RVA: 0x00005877 File Offset: 0x00003C77
			public Vector3 GetPosition()
			{
				return this.m_Position;
			}

			// Token: 0x060000AB RID: 171 RVA: 0x0000587F File Offset: 0x00003C7F
			public Quaternion GetRotation()
			{
				return this.m_Rotation;
			}

			// Token: 0x060000AC RID: 172 RVA: 0x00005887 File Offset: 0x00003C87
			public WrapMode GetPlayMode()
			{
				return this.m_PlayMode;
			}

			// Token: 0x04000094 RID: 148
			private ADVEffectManager.ADVEffectHandlerWrapper m_PrecedingInstance;

			// Token: 0x04000095 RID: 149
			private int m_PrecedingUniqueId;

			// Token: 0x04000096 RID: 150
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x04000097 RID: 151
			private int m_EffectUniqueId;

			// Token: 0x04000098 RID: 152
			private string m_EffectID;

			// Token: 0x04000099 RID: 153
			private Vector3 m_Position;

			// Token: 0x0400009A RID: 154
			private Quaternion m_Rotation;

			// Token: 0x0400009B RID: 155
			private WrapMode m_PlayMode;
		}

		// Token: 0x02000017 RID: 23
		public class PlayProcessArgument
		{
			// Token: 0x060000AD RID: 173 RVA: 0x0000588F File Offset: 0x00003C8F
			public PlayProcessArgument(ADVEffectManager.ePlayType playType, int effectUniqueId, string effectID, Vector3 position, Quaternion rotation, WrapMode playMode = WrapMode.ClampForever)
			{
				this.m_PlayType = playType;
				this.m_EffectUniqueId = effectUniqueId;
				this.m_EffectID = effectID;
				this.m_Position = position;
				this.m_Rotation = rotation;
				this.m_PlayMode = playMode;
			}

			// Token: 0x060000AE RID: 174 RVA: 0x000058C4 File Offset: 0x00003CC4
			public ADVEffectManager.ePlayType GetPlayType()
			{
				return this.m_PlayType;
			}

			// Token: 0x060000AF RID: 175 RVA: 0x000058CC File Offset: 0x00003CCC
			public int GetEffectUniqueId()
			{
				return this.m_EffectUniqueId;
			}

			// Token: 0x060000B0 RID: 176 RVA: 0x000058D4 File Offset: 0x00003CD4
			public string GetEffectID()
			{
				return this.m_EffectID;
			}

			// Token: 0x060000B1 RID: 177 RVA: 0x000058DC File Offset: 0x00003CDC
			public Vector3 GetPosition()
			{
				return this.m_Position;
			}

			// Token: 0x060000B2 RID: 178 RVA: 0x000058E4 File Offset: 0x00003CE4
			public Quaternion GetRotation()
			{
				return this.m_Rotation;
			}

			// Token: 0x060000B3 RID: 179 RVA: 0x000058EC File Offset: 0x00003CEC
			public WrapMode GetPlayMode()
			{
				return this.m_PlayMode;
			}

			// Token: 0x0400009C RID: 156
			private ADVEffectManager.ePlayType m_PlayType;

			// Token: 0x0400009D RID: 157
			private int m_EffectUniqueId;

			// Token: 0x0400009E RID: 158
			private string m_EffectID;

			// Token: 0x0400009F RID: 159
			private Vector3 m_Position;

			// Token: 0x040000A0 RID: 160
			private Quaternion m_Rotation;

			// Token: 0x040000A1 RID: 161
			private WrapMode m_PlayMode;
		}

		// Token: 0x02000018 RID: 24
		public class EffectPreset
		{
			// Token: 0x040000A2 RID: 162
			public string st;

			// Token: 0x040000A3 RID: 163
			public string lp;

			// Token: 0x040000A4 RID: 164
			public string ed;

			// Token: 0x040000A5 RID: 165
			public bool m_ignoreParticle;

			// Token: 0x040000A6 RID: 166
			public Vector3 pos;

			// Token: 0x040000A7 RID: 167
			public Quaternion rot;

			// Token: 0x040000A8 RID: 168
			public bool isLoop;

			// Token: 0x040000A9 RID: 169
			public int m_Step;
		}
	}
}
