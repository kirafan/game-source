﻿using System;

namespace Star.ADV
{
	// Token: 0x02000062 RID: 98
	public class ADVTextShiftTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002D3 RID: 723 RVA: 0x0000FFF2 File Offset: 0x0000E3F2
		protected ADVTextShiftTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_wait, isEndTag)
		{
		}
	}
}
