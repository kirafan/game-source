﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200005F RID: 95
	public class ADVTextShakeTagEnd : ADVTextShakeTagBase
	{
		// Token: 0x060002CE RID: 718 RVA: 0x0000FF63 File Offset: 0x0000E363
		public ADVTextShakeTagEnd(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, true)
		{
		}

		// Token: 0x060002CF RID: 719 RVA: 0x0000FF6E File Offset: 0x0000E36E
		public override bool Analyze(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			return false;
		}
	}
}
