﻿using System;

namespace Star.ADV
{
	// Token: 0x02000046 RID: 70
	public class ADVTextVoiceTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002A4 RID: 676 RVA: 0x0000F6F9 File Offset: 0x0000DAF9
		protected ADVTextVoiceTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_voice, isEndTag)
		{
		}
	}
}
