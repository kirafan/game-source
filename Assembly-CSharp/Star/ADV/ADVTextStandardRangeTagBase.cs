﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200003B RID: 59
	public class ADVTextStandardRangeTagBase : ADVTextRangeTagBase
	{
		// Token: 0x06000290 RID: 656 RVA: 0x0000F478 File Offset: 0x0000D878
		protected ADVTextStandardRangeTagBase(ADVPlayer player, ADVParser parentParser, ADVParser.EventType eventType, ADVTextTag.eTagType tagType) : base(player, parentParser, eventType, tagType)
		{
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000F485 File Offset: 0x0000D885
		public override void AnalyzeEndProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
		}
	}
}
