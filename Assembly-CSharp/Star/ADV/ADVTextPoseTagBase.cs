﻿using System;

namespace Star.ADV
{
	// Token: 0x02000057 RID: 87
	public class ADVTextPoseTagBase : ADVTextRangeTagBase
	{
		// Token: 0x060002C1 RID: 705 RVA: 0x0000FC58 File Offset: 0x0000E058
		protected ADVTextPoseTagBase(ADVPlayer player, ADVParser parentParser, bool isEndTag) : base(player, parentParser, ADVParser.EventType.event_pose, isEndTag)
		{
		}
	}
}
