﻿using System;
using System.Collections.Generic;
using Star.UI.ADV;

namespace Star.ADV
{
	// Token: 0x0200003E RID: 62
	public class ADVTextStrongTag : ADVTextStrongTagBase
	{
		// Token: 0x06000296 RID: 662 RVA: 0x0000F4A6 File Offset: 0x0000D8A6
		public ADVTextStrongTag(ADVPlayer player, ADVParser parentParser) : base(player, parentParser, false)
		{
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000F4B4 File Offset: 0x0000D8B4
		public override bool AnalyzeProcess(ref string text, ref int idx, ref List<ADVParser.EventType> evList, string tag, bool execTag, ADVCalcText calcTextObj, ref List<ADVParser.RubyData> rubyList)
		{
			base.DeleteTag(ref text, ref idx, tag.Length);
			string text2 = "<size=72>";
			text = text.Insert(idx, text2);
			idx += text2.Length;
			evList.Add(ADVParser.EventType.event_strong);
			return true;
		}
	}
}
