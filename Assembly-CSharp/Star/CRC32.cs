﻿using System;
using System.Text;

namespace Star
{
	// Token: 0x020005F3 RID: 1523
	public static class CRC32
	{
		// Token: 0x06001E1F RID: 7711 RVA: 0x000A1D04 File Offset: 0x000A0104
		private static void BuildCRC32Table()
		{
			CRC32.crcTable = new uint[256];
			for (uint num = 0U; num < 256U; num += 1U)
			{
				uint num2 = num;
				for (uint num3 = 0U; num3 < 8U; num3 += 1U)
				{
					num2 = (uint)(((num2 & 1U) != 0U) ? (18446744073402876704UL ^ (ulong)(num2 >> 1)) : ((ulong)(num2 >> 1)));
				}
				CRC32.crcTable[(int)((UIntPtr)num)] = num2;
			}
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x000A1D74 File Offset: 0x000A0174
		public static uint Calc(byte[] buf)
		{
			if (CRC32.crcTable == null)
			{
				CRC32.BuildCRC32Table();
			}
			uint num = uint.MaxValue;
			uint num2 = 0U;
			while ((ulong)num2 < (ulong)((long)buf.Length))
			{
				num = (CRC32.crcTable[(int)((UIntPtr)((num ^ (uint)buf[(int)((UIntPtr)num2)]) & 255U))] ^ num >> 8);
				num2 += 1U;
			}
			return (uint)((ulong)num ^ ulong.MaxValue);
		}

		// Token: 0x06001E21 RID: 7713 RVA: 0x000A1DC8 File Offset: 0x000A01C8
		public static uint Calc(string fname)
		{
			if (CRC32.crcTable == null)
			{
				CRC32.BuildCRC32Table();
			}
			byte[] bytes = Encoding.ASCII.GetBytes(fname);
			uint num = uint.MaxValue;
			uint num2 = 0U;
			while ((ulong)num2 < (ulong)((long)bytes.Length))
			{
				num = (CRC32.crcTable[(int)((UIntPtr)((num ^ (uint)bytes[(int)((UIntPtr)num2)]) & 255U))] ^ num >> 8);
				num2 += 1U;
			}
			return (uint)((ulong)num ^ ulong.MaxValue);
		}

		// Token: 0x0400249B RID: 9371
		private const int TABLE_LENGTH = 256;

		// Token: 0x0400249C RID: 9372
		private static uint[] crcTable;
	}
}
