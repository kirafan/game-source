﻿using System;

namespace Star
{
	// Token: 0x020002D4 RID: 724
	public class EquipWeaponManagedSupportPartyMemberController : EquipWeaponManagedPartyMemberControllerExGen<EquipWeaponManagedSupportPartyMemberController>
	{
		// Token: 0x06000E1D RID: 3613 RVA: 0x0004D6FC File Offset: 0x0004BAFC
		public EquipWeaponManagedSupportPartyMemberController() : base(eCharacterDataControllerType.ManagedSupportParty)
		{
		}

		// Token: 0x06000E1E RID: 3614 RVA: 0x0004D708 File Offset: 0x0004BB08
		protected override CharacterDataWrapperBase GetManagedCharacterDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			UserCharacterData supportPartyUserCharacterData = EditUtility.GetSupportPartyUserCharacterData(in_partyIndex, in_partySlotIndex);
			if (supportPartyUserCharacterData != null)
			{
				return new UserCharacterDataWrapper(supportPartyUserCharacterData);
			}
			if (in_partySlotIndex < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.SupportLimit)
			{
				return new CharacterDataEmpty();
			}
			return new CharacterDataLocked();
		}

		// Token: 0x06000E1F RID: 3615 RVA: 0x0004D74F File Offset: 0x0004BB4F
		protected override UserWeaponData GetManagedWeaponDataFromGlobal(int in_partyIndex, int in_partySlotIndex)
		{
			return EditUtility.GetSupportPartyUserWeaponData(in_partyIndex, in_partySlotIndex);
		}
	}
}
