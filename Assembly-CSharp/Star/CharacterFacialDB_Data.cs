﻿using System;

namespace Star
{
	// Token: 0x0200016F RID: 367
	[Serializable]
	public struct CharacterFacialDB_Data
	{
		// Token: 0x040009C7 RID: 2503
		public int m_TargetNamed;

		// Token: 0x040009C8 RID: 2504
		public int m_FacialID;
	}
}
