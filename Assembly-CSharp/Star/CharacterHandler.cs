﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200014D RID: 333
	public class CharacterHandler : MonoBehaviour
	{
		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000923 RID: 2339 RVA: 0x0003AFFC File Offset: 0x000393FC
		public Transform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000924 RID: 2340 RVA: 0x0003B004 File Offset: 0x00039404
		// (set) Token: 0x06000925 RID: 2341 RVA: 0x0003B00C File Offset: 0x0003940C
		public Collider TouchCollider
		{
			get
			{
				return this.m_TouchCollider;
			}
			set
			{
				this.m_TouchCollider = value;
			}
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000926 RID: 2342 RVA: 0x0003B015 File Offset: 0x00039415
		public CharacterResource CharaResource
		{
			get
			{
				return this.m_CharaResource;
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x06000927 RID: 2343 RVA: 0x0003B01D File Offset: 0x0003941D
		public CharacterBattle CharaBattle
		{
			get
			{
				return this.m_CharaBattle;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000928 RID: 2344 RVA: 0x0003B025 File Offset: 0x00039425
		public CharacterMenu CharaMenu
		{
			get
			{
				return this.m_CharaMenu;
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000929 RID: 2345 RVA: 0x0003B02D File Offset: 0x0003942D
		public CharacterAnim CharaAnim
		{
			get
			{
				return this.m_CharaAnim;
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x0600092A RID: 2346 RVA: 0x0003B035 File Offset: 0x00039435
		public CharacterParam CharaParam
		{
			get
			{
				return this.m_CharaParam;
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0003B03D File Offset: 0x0003943D
		public int CharaID
		{
			get
			{
				return this.m_CharaParam.CharaID;
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x0600092C RID: 2348 RVA: 0x0003B04A File Offset: 0x0003944A
		public uint UniqueID
		{
			get
			{
				return this.m_CharaParam.UniqueID;
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x0600092D RID: 2349 RVA: 0x0003B057 File Offset: 0x00039457
		public BattleAIData BattleAIData
		{
			get
			{
				return this.m_CharaBattle.BattleAIData;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x0600092E RID: 2350 RVA: 0x0003B064 File Offset: 0x00039464
		public NamedParam NamedParam
		{
			get
			{
				return this.m_NamedParam;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x0600092F RID: 2351 RVA: 0x0003B06C File Offset: 0x0003946C
		// (set) Token: 0x06000930 RID: 2352 RVA: 0x0003B074 File Offset: 0x00039474
		public WeaponParam WeaponParam
		{
			get
			{
				return this.m_WeaponParam;
			}
			set
			{
				this.m_WeaponParam = value;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000931 RID: 2353 RVA: 0x0003B07D File Offset: 0x0003947D
		public CharacterDefine.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000932 RID: 2354 RVA: 0x0003B085 File Offset: 0x00039485
		public bool IsUserControll
		{
			get
			{
				return this.m_IsUserControll;
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x06000933 RID: 2355 RVA: 0x0003B08D File Offset: 0x0003948D
		public CharacterDefine.eFriendType FriendType
		{
			get
			{
				return this.m_FriendType;
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000934 RID: 2356 RVA: 0x0003B095 File Offset: 0x00039495
		public bool IsFriendChara
		{
			get
			{
				return this.m_FriendType != CharacterDefine.eFriendType.None;
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000935 RID: 2357 RVA: 0x0003B0A3 File Offset: 0x000394A3
		public bool IsEnableRender
		{
			get
			{
				return this.m_IsEnableRender;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000936 RID: 2358 RVA: 0x0003B0AB File Offset: 0x000394AB
		public bool[] IsEnableRenderWeapon
		{
			get
			{
				return this.m_IsEnableRenderWeapons;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000937 RID: 2359 RVA: 0x0003B0B3 File Offset: 0x000394B3
		public bool IsEnableRenderShadow
		{
			get
			{
				return this.m_IsEnableRenderShadow;
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000938 RID: 2360 RVA: 0x0003B0BB File Offset: 0x000394BB
		public SoundHandler VoiceSoundHndl
		{
			get
			{
				return this.m_VoiceSoundHndl;
			}
		}

		// Token: 0x06000939 RID: 2361 RVA: 0x0003B0C4 File Offset: 0x000394C4
		private void Awake()
		{
			this.m_Transform = base.GetComponent<Transform>();
			base.gameObject.layer = LayerMask.NameToLayer("Character");
			this.m_TouchCollider = base.gameObject.AddComponent<SphereCollider>();
			SphereCollider sphereCollider = this.m_TouchCollider as SphereCollider;
			if (sphereCollider != null)
			{
				sphereCollider.center = new Vector3(0f, 0.38f, 0f);
				sphereCollider.radius = 0.38f;
			}
		}

		// Token: 0x0600093A RID: 2362 RVA: 0x0003B140 File Offset: 0x00039540
		private void Update()
		{
			if (this.m_CharaResource != null)
			{
				this.m_CharaResource.Update();
			}
			if (this.m_IsPreparing)
			{
				this.PrepareMain();
				return;
			}
			CharacterDefine.eMode mode = this.m_Mode;
			if (mode != CharacterDefine.eMode.Battle)
			{
				if (mode == CharacterDefine.eMode.Menu)
				{
					if (this.m_CharaMenu != null)
					{
						this.m_CharaMenu.Update();
					}
				}
			}
			else if (this.m_CharaBattle != null)
			{
				this.m_CharaBattle.Update();
			}
			if (this.m_CharaAnim != null)
			{
				this.m_CharaAnim.Update();
			}
		}

		// Token: 0x0600093B RID: 2363 RVA: 0x0003B1DC File Offset: 0x000395DC
		private void LateUpdate()
		{
			CharacterDefine.eMode mode = this.m_Mode;
			if (mode == CharacterDefine.eMode.Battle)
			{
				if (this.m_CharaBattle != null)
				{
					this.m_CharaBattle.LateUpdate();
				}
			}
			if (this.m_CharaAnim != null)
			{
				this.m_CharaAnim.LateUpdate();
			}
		}

		// Token: 0x0600093C RID: 2364 RVA: 0x0003B22C File Offset: 0x0003962C
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x0600093D RID: 2365 RVA: 0x0003B234 File Offset: 0x00039634
		public bool IsDonePreparePerfect()
		{
			return !this.IsPreparing() && this.m_CharaResource != null && this.m_CharaResource.IsDonePrepare();
		}

		// Token: 0x0600093E RID: 2366 RVA: 0x0003B260 File Offset: 0x00039660
		public void SetupInInstantiate(CharacterParam srcCharaParam, NamedParam srcNamedParam, WeaponParam srcWeaponParam, eCharaResourceType resourceType, int resurceID, uint uniqueID, CharacterDefine.eMode mode, bool isUserControll, CharacterDefine.eFriendType friendType)
		{
			this.m_Mode = mode;
			this.m_IsUserControll = isUserControll;
			this.m_FriendType = friendType;
			if (srcCharaParam != null)
			{
				this.m_CharaParam = (CharacterParam)srcCharaParam.DeepCopy();
			}
			else
			{
				this.m_CharaParam = null;
			}
			if (srcNamedParam != null)
			{
				this.m_NamedParam = (NamedParam)srcNamedParam.DeepCopy();
			}
			else
			{
				this.m_NamedParam = null;
			}
			if (srcWeaponParam != null)
			{
				this.m_WeaponParam = (WeaponParam)srcWeaponParam.DeepCopy();
			}
			else
			{
				this.m_WeaponParam = null;
			}
			this.m_CharaResource = new CharacterResource();
			this.m_CharaResource.Setup(this, resourceType, resurceID);
		}

		// Token: 0x0600093F RID: 2367 RVA: 0x0003B30C File Offset: 0x0003970C
		public void Destroy()
		{
			if (this.m_CharaMenu != null)
			{
				this.m_CharaMenu.Destroy();
			}
			if (this.m_CharaBattle != null)
			{
				this.m_CharaBattle.Destroy();
			}
			if (this.m_CharaAnim != null)
			{
				this.m_CharaAnim.Destroy();
			}
			if (this.m_CharaResource != null)
			{
				this.m_CharaResource.Destroy();
			}
			if (this.m_VoiceSoundHndl != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_VoiceSoundHndl, true);
				this.m_VoiceSoundHndl = null;
			}
		}

		// Token: 0x06000940 RID: 2368 RVA: 0x0003B399 File Offset: 0x00039799
		public void Prepare()
		{
			this.m_IsPreparing = true;
			this.m_PrepareWithoutResourceSetup = CharacterHandler.ePrepareWithoutResourceSetup.None;
			this.m_CharaResource.Prepare();
		}

		// Token: 0x06000941 RID: 2369 RVA: 0x0003B3B4 File Offset: 0x000397B4
		public void PrepareWithoutResourceSetup(bool isFirstPrepare)
		{
			if (this.m_Mode != CharacterDefine.eMode.Battle)
			{
				return;
			}
			this.m_IsPreparing = true;
			if (isFirstPrepare)
			{
				this.m_PrepareWithoutResourceSetup = CharacterHandler.ePrepareWithoutResourceSetup.First;
			}
			else
			{
				this.m_PrepareWithoutResourceSetup = CharacterHandler.ePrepareWithoutResourceSetup.Second;
			}
			CharacterHandler.ePrepareWithoutResourceSetup prepareWithoutResourceSetup = this.m_PrepareWithoutResourceSetup;
			if (prepareWithoutResourceSetup != CharacterHandler.ePrepareWithoutResourceSetup.First)
			{
				if (prepareWithoutResourceSetup == CharacterHandler.ePrepareWithoutResourceSetup.Second)
				{
					this.m_CharaResource.Prepare();
				}
			}
			else
			{
				CharacterDefine.eMode mode = this.m_Mode;
				if (mode == CharacterDefine.eMode.Battle)
				{
					if (this.m_CharaAnim == null)
					{
						this.m_CharaAnim = new CharacterAnim(this);
					}
					if (this.m_CharaBattle == null)
					{
						this.m_CharaBattle = new CharacterBattle();
					}
					this.m_CharaBattle.Setup(this, true);
				}
				this.m_IsPreparing = false;
			}
		}

		// Token: 0x06000942 RID: 2370 RVA: 0x0003B474 File Offset: 0x00039874
		private void PrepareMain()
		{
			if (this.m_CharaResource.IsPreparing())
			{
				return;
			}
			CharacterDefine.eMode mode = this.m_Mode;
			if (mode != CharacterDefine.eMode.Battle)
			{
				if (mode == CharacterDefine.eMode.Menu)
				{
					if (this.m_CharaMenu == null)
					{
						this.m_CharaMenu = new CharacterMenu();
					}
					this.m_CharaMenu.Setup(this);
				}
			}
			else
			{
				if (this.m_CharaBattle == null)
				{
					this.m_CharaBattle = new CharacterBattle();
				}
				this.m_CharaBattle.Setup(this, false);
			}
			this.m_IsPreparing = false;
		}

		// Token: 0x06000943 RID: 2371 RVA: 0x0003B501 File Offset: 0x00039901
		public void SetupCharaModel(GameObject[] modelObjs)
		{
			if (this.m_CharaAnim == null)
			{
				this.m_CharaAnim = new CharacterAnim(this);
			}
			this.m_CharaAnim.Setup();
			this.m_CharaAnim.AttachCharaModel(modelObjs);
			this.SetEnableRender(this.m_IsEnableRender);
		}

		// Token: 0x06000944 RID: 2372 RVA: 0x0003B540 File Offset: 0x00039940
		public void SetupWeaponModel(GameObject[] modelObjs)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			this.m_CharaAnim.AttachWeaponModel(modelObjs);
			for (int i = 0; i < 2; i++)
			{
				bool flg = false;
				if (i < modelObjs.Length && modelObjs[i] != null)
				{
					flg = this.m_IsEnableRenderWeapons[i];
					if (this.CharaParam.ClassType == eClassType.Alchemist && i == 0)
					{
						flg = false;
					}
				}
				this.SetEnableRenderWeapon((CharacterDefine.eWeaponIndex)i, flg, false);
			}
		}

		// Token: 0x06000945 RID: 2373 RVA: 0x0003B5BB File Offset: 0x000399BB
		public void SetupAnimClip_Open(int partsIndex)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			this.m_CharaAnim.AnimCtrlOpen(partsIndex);
		}

		// Token: 0x06000946 RID: 2374 RVA: 0x0003B5D8 File Offset: 0x000399D8
		public void SetupAnimClip(string actionKey, GameObject animPrefab, int partsIndex)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			if (animPrefab == null)
			{
				return;
			}
			MeigeAnimClipHolder componentInChildren = animPrefab.GetComponentInChildren<MeigeAnimClipHolder>();
			this.m_CharaAnim.AddAnim(actionKey, componentInChildren, partsIndex);
		}

		// Token: 0x06000947 RID: 2375 RVA: 0x0003B613 File Offset: 0x00039A13
		public void SetupAnimClip_Close(int partsIndex)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			this.m_CharaAnim.AnimCtrlClose(partsIndex);
		}

		// Token: 0x06000948 RID: 2376 RVA: 0x0003B630 File Offset: 0x00039A30
		public void SetupFacialAnimClip(GameObject animPrefab)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			if (animPrefab == null)
			{
				return;
			}
			MeigeAnimClipHolder componentInChildren = animPrefab.GetComponentInChildren<MeigeAnimClipHolder>();
			this.m_CharaAnim.AddFacialAnim(componentInChildren);
		}

		// Token: 0x06000949 RID: 2377 RVA: 0x0003B669 File Offset: 0x00039A69
		public void SetupShadow(GameObject modelObj)
		{
			if (this.m_CharaAnim == null)
			{
				return;
			}
			if (modelObj == null)
			{
				return;
			}
			this.m_CharaAnim.AttachShadow(modelObj);
		}

		// Token: 0x0600094A RID: 2378 RVA: 0x0003B690 File Offset: 0x00039A90
		public void SetEnableRender(bool flg)
		{
			this.m_IsEnableRender = flg;
			if (this.m_CharaAnim != null)
			{
				if (this.m_CharaAnim.ModelSets != null)
				{
					for (int i = 0; i < this.m_CharaAnim.ModelSets.Length; i++)
					{
						if (this.m_CharaAnim.ModelSets[i] != null && this.m_CharaAnim.ModelSets[i].m_Obj != null)
						{
							this.m_CharaAnim.ModelSets[i].m_Obj.SetActive(flg);
						}
					}
				}
				if (this.m_CharaAnim.Shadow != null)
				{
					this.m_CharaAnim.Shadow.SetActive(this.m_IsEnableRenderShadow && flg);
				}
			}
		}

		// Token: 0x0600094B RID: 2379 RVA: 0x0003B75C File Offset: 0x00039B5C
		public void SetEnableRenderWeapon(bool flg)
		{
			this.m_IsEnableRenderWeapon = flg;
			if (this.m_CharaAnim != null && this.m_CharaAnim.WeaponModelSets != null)
			{
				for (int i = 0; i < 2; i++)
				{
					if (this.m_CharaAnim.WeaponModelSets[i] != null && this.m_CharaAnim.WeaponModelSets[i].m_Obj != null)
					{
						this.m_CharaAnim.WeaponModelSets[i].m_Obj.SetActive(this.m_IsEnableRenderWeapon && this.m_IsEnableRenderWeapons[i]);
					}
				}
			}
		}

		// Token: 0x0600094C RID: 2380 RVA: 0x0003B7FC File Offset: 0x00039BFC
		public void SetEnableRenderWeapon(CharacterDefine.eWeaponIndex wpnIndex, bool flg, bool isIgnoreMasterEnable = false)
		{
			this.m_IsEnableRenderWeapons[(int)wpnIndex] = flg;
			if (this.m_CharaAnim != null && this.m_CharaAnim.WeaponModelSets != null && this.m_CharaAnim.WeaponModelSets[(int)wpnIndex] != null && this.m_CharaAnim.WeaponModelSets[(int)wpnIndex].m_Obj != null)
			{
				if (!isIgnoreMasterEnable)
				{
					this.m_CharaAnim.WeaponModelSets[(int)wpnIndex].m_Obj.SetActive(this.m_IsEnableRenderWeapon && this.m_IsEnableRenderWeapons[(int)wpnIndex]);
				}
				else
				{
					this.m_CharaAnim.WeaponModelSets[(int)wpnIndex].m_Obj.SetActive(this.m_IsEnableRenderWeapons[(int)wpnIndex]);
				}
			}
		}

		// Token: 0x0600094D RID: 2381 RVA: 0x0003B8B8 File Offset: 0x00039CB8
		public void RevertDefaultEnableRenderWeapon()
		{
			this.m_IsEnableRenderWeapon = true;
			for (int i = 0; i < 2; i++)
			{
				if (i == 0 && this.CharaParam.ClassType == eClassType.Alchemist)
				{
					this.SetEnableRenderWeapon((CharacterDefine.eWeaponIndex)i, false, false);
				}
				else
				{
					this.SetEnableRenderWeapon((CharacterDefine.eWeaponIndex)i, true, false);
				}
			}
		}

		// Token: 0x0600094E RID: 2382 RVA: 0x0003B90C File Offset: 0x00039D0C
		public void SetEnableRenderShadow(bool flg)
		{
			this.m_IsEnableRenderShadow = flg;
			if (this.m_CharaAnim != null && this.m_CharaAnim.Shadow != null)
			{
				this.m_CharaAnim.Shadow.SetActive(this.m_IsEnableRenderShadow);
			}
		}

		// Token: 0x0600094F RID: 2383 RVA: 0x0003B94C File Offset: 0x00039D4C
		public string GetNickName()
		{
			if (this.m_CharaParam != null && this.m_CharaParam.NamedType != eCharaNamedType.None)
			{
				return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(this.m_CharaParam.NamedType).m_NickName;
			}
			if (this.m_CharaBattle != null)
			{
				return this.m_CharaBattle.Name;
			}
			return string.Empty;
		}

		// Token: 0x06000950 RID: 2384 RVA: 0x0003B9B9 File Offset: 0x00039DB9
		public void ResetScale()
		{
			this.CacheTransform.localScale = new Vector3(1f, 1f, 1f);
		}

		// Token: 0x06000951 RID: 2385 RVA: 0x0003B9DC File Offset: 0x00039DDC
		public void PlayVoice(string cueName, float volume = 1f)
		{
			if (this.m_CharaResource != null && this.m_CharaResource.IsAvailableVoice)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_VoiceSoundHndl, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(this.m_CharaResource.VoiceCueSheetName, cueName, this.m_VoiceSoundHndl, volume, 0, -1, -1);
			}
		}

		// Token: 0x06000952 RID: 2386 RVA: 0x0003BA40 File Offset: 0x00039E40
		public void PlayVoice(eSoundVoiceListDB cueID, float volume = 1f)
		{
			if (this.m_CharaResource != null && this.m_CharaResource.IsAvailableVoice)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_VoiceSoundHndl, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayVOICE(this.m_CharaParam.NamedType, cueID, this.m_VoiceSoundHndl, volume, 0, -1, -1);
			}
		}

		// Token: 0x06000953 RID: 2387 RVA: 0x0003BAA4 File Offset: 0x00039EA4
		public void PlayVoiceRandom(eSoundVoiceListDB[] cueIDs, float volume = 1f)
		{
			if (this.m_CharaResource != null && this.m_CharaResource.IsAvailableVoice && cueIDs != null)
			{
				int num = UnityEngine.Random.Range(0, cueIDs.Length);
				this.PlayVoice(cueIDs[num], volume);
			}
		}

		// Token: 0x06000954 RID: 2388 RVA: 0x0003BAE8 File Offset: 0x00039EE8
		public void PlayVoiceRandom(string[] cueNames, float volume = 1f)
		{
			if (this.m_CharaResource != null && this.m_CharaResource.IsAvailableVoice && cueNames != null)
			{
				int num = UnityEngine.Random.Range(0, cueNames.Length);
				this.PlayVoice(cueNames[num], volume);
			}
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x0003BB2A File Offset: 0x00039F2A
		public bool IsPlayingVoice()
		{
			return this.m_VoiceSoundHndl != null && this.m_VoiceSoundHndl.IsPlaying(false);
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x0003BB4B File Offset: 0x00039F4B
		private void OnCompleteMoveTo()
		{
			if (this.m_CharaBattle != null)
			{
				this.m_CharaBattle.IsMoveTo = false;
			}
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x0003BB64 File Offset: 0x00039F64
		private void OnCompleteStunScale()
		{
			this.CacheTransform.localScaleY(1f);
			iTween.PunchScale(base.gameObject, iTween.Hash(new object[]
			{
				"y",
				1f,
				"time",
				0.16666667f
			}));
		}

		// Token: 0x040008D1 RID: 2257
		private Transform m_Transform;

		// Token: 0x040008D2 RID: 2258
		private Collider m_TouchCollider;

		// Token: 0x040008D3 RID: 2259
		private CharacterParam m_CharaParam;

		// Token: 0x040008D4 RID: 2260
		private NamedParam m_NamedParam;

		// Token: 0x040008D5 RID: 2261
		private CharacterResource m_CharaResource;

		// Token: 0x040008D6 RID: 2262
		private CharacterBattle m_CharaBattle;

		// Token: 0x040008D7 RID: 2263
		private CharacterMenu m_CharaMenu;

		// Token: 0x040008D8 RID: 2264
		private CharacterAnim m_CharaAnim;

		// Token: 0x040008D9 RID: 2265
		private WeaponParam m_WeaponParam;

		// Token: 0x040008DA RID: 2266
		private CharacterDefine.eMode m_Mode = CharacterDefine.eMode.None;

		// Token: 0x040008DB RID: 2267
		private bool m_IsUserControll;

		// Token: 0x040008DC RID: 2268
		public CharacterDefine.eFriendType m_FriendType = CharacterDefine.eFriendType.None;

		// Token: 0x040008DD RID: 2269
		private CharacterHandler.ePrepareWithoutResourceSetup m_PrepareWithoutResourceSetup = CharacterHandler.ePrepareWithoutResourceSetup.None;

		// Token: 0x040008DE RID: 2270
		private bool m_IsPreparing;

		// Token: 0x040008DF RID: 2271
		private bool m_IsEnableRender = true;

		// Token: 0x040008E0 RID: 2272
		private bool m_IsEnableRenderWeapon = true;

		// Token: 0x040008E1 RID: 2273
		private bool[] m_IsEnableRenderWeapons = new bool[]
		{
			true,
			true
		};

		// Token: 0x040008E2 RID: 2274
		private bool m_IsEnableRenderShadow = true;

		// Token: 0x040008E3 RID: 2275
		private SoundHandler m_VoiceSoundHndl = new SoundHandler();

		// Token: 0x0200014E RID: 334
		public enum ePrepareWithoutResourceSetup
		{
			// Token: 0x040008E5 RID: 2277
			None = -1,
			// Token: 0x040008E6 RID: 2278
			First,
			// Token: 0x040008E7 RID: 2279
			Second,
			// Token: 0x040008E8 RID: 2280
			Done
		}
	}
}
