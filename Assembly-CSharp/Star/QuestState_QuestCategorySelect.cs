﻿using System;
using Star.UI.Global;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x0200045D RID: 1117
	public class QuestState_QuestCategorySelect : QuestState
	{
		// Token: 0x060015A2 RID: 5538 RVA: 0x00070923 File Offset: 0x0006ED23
		public QuestState_QuestCategorySelect(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015A3 RID: 5539 RVA: 0x00070933 File Offset: 0x0006ED33
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x060015A4 RID: 5540 RVA: 0x00070936 File Offset: 0x0006ED36
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestCategorySelect.eStep.First;
		}

		// Token: 0x060015A5 RID: 5541 RVA: 0x0007093F File Offset: 0x0006ED3F
		public override void OnStateExit()
		{
		}

		// Token: 0x060015A6 RID: 5542 RVA: 0x00070941 File Offset: 0x0006ED41
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015A7 RID: 5543 RVA: 0x0007094C File Offset: 0x0006ED4C
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestCategorySelect.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = QuestState_QuestCategorySelect.eStep.LoadWait;
				break;
			case QuestState_QuestCategorySelect.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestCategorySelect.eStep.PlayIn;
				}
				break;
			case QuestState_QuestCategorySelect.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = QuestState_QuestCategorySelect.eStep.Main;
				}
				break;
			case QuestState_QuestCategorySelect.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_UI.IsSelectQuestCategory)
					{
						QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
						selectQuest.m_EventType = this.m_UI.SelectEventType;
						selectQuest.m_QuestID = -1;
						if (this.m_UI.IsSelectChapterQuest)
						{
							if (SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetLastChapterID() > LocalSaveData.Inst.PlayedOpenChapterId)
							{
								this.m_NextState = 3;
							}
							else
							{
								this.m_NextState = 4;
							}
						}
						else
						{
							int num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestADVTriggerDB.FindAdvID(eQuestADVTriggerType.EventQuestType, this.m_UI.SelectEventType);
							if (num != -1 && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(num))
							{
								num = -1;
							}
							if (num != -1)
							{
								GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
								globalParam.SetAdvParam(num, SceneDefine.eSceneID.Quest, false);
								globalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.None;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
								this.m_NextState = 2147483646;
							}
							else
							{
								this.m_NextState = 2;
							}
						}
					}
					else
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						this.m_NextState = 2147483646;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestCategorySelect.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestCategorySelect.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = QuestState_QuestCategorySelect.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015A8 RID: 5544 RVA: 0x00070B8C File Offset: 0x0006EF8C
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<QuestCategorySelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Quest);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015A9 RID: 5545 RVA: 0x00070C24 File Offset: 0x0006F024
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Quest)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060015AA RID: 5546 RVA: 0x00070C7B File Offset: 0x0006F07B
		private void OnClickButton()
		{
		}

		// Token: 0x060015AB RID: 5547 RVA: 0x00070C7D File Offset: 0x0006F07D
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001C64 RID: 7268
		private QuestState_QuestCategorySelect.eStep m_Step = QuestState_QuestCategorySelect.eStep.None;

		// Token: 0x04001C65 RID: 7269
		private SceneDefine.eChildSceneID m_ChildSceneID;

		// Token: 0x04001C66 RID: 7270
		private QuestCategorySelectUI m_UI;

		// Token: 0x0200045E RID: 1118
		private enum eStep
		{
			// Token: 0x04001C68 RID: 7272
			None = -1,
			// Token: 0x04001C69 RID: 7273
			First,
			// Token: 0x04001C6A RID: 7274
			LoadWait,
			// Token: 0x04001C6B RID: 7275
			PlayIn,
			// Token: 0x04001C6C RID: 7276
			Main,
			// Token: 0x04001C6D RID: 7277
			UnloadChildSceneWait
		}
	}
}
