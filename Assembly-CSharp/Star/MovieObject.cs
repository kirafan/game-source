﻿using System;
using CriMana;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000518 RID: 1304
	public class MovieObject
	{
		// Token: 0x060019BB RID: 6587 RVA: 0x00085D4D File Offset: 0x0008414D
		public MovieObject()
		{
			this.m_Player = new Player();
			this.isMaterialAvailable = false;
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x060019BC RID: 6588 RVA: 0x00085D72 File Offset: 0x00084172
		// (set) Token: 0x060019BD RID: 6589 RVA: 0x00085D7A File Offset: 0x0008417A
		public byte Ref { get; private set; }

		// Token: 0x060019BE RID: 6590 RVA: 0x00085D83 File Offset: 0x00084183
		public void AddRef()
		{
			this.Ref = 1;
		}

		// Token: 0x060019BF RID: 6591 RVA: 0x00085D8C File Offset: 0x0008418C
		public void RemoveRef()
		{
			this.Ref = 0;
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x060019C0 RID: 6592 RVA: 0x00085D95 File Offset: 0x00084195
		// (set) Token: 0x060019C1 RID: 6593 RVA: 0x00085D9D File Offset: 0x0008419D
		public bool isMaterialAvailable { get; private set; }

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x060019C2 RID: 6594 RVA: 0x00085DA6 File Offset: 0x000841A6
		// (set) Token: 0x060019C3 RID: 6595 RVA: 0x00085DAE File Offset: 0x000841AE
		public Material material
		{
			get
			{
				return this._material;
			}
			set
			{
				if (value != this._material)
				{
					if (this.materialOwn)
					{
						this.materialOwn = false;
					}
					this._material = value;
					this.isMaterialAvailable = false;
				}
			}
		}

		// Token: 0x060019C4 RID: 6596 RVA: 0x00085DE1 File Offset: 0x000841E1
		public void CallRemoved()
		{
			this.m_IsRemoved = true;
		}

		// Token: 0x060019C5 RID: 6597 RVA: 0x00085DEA File Offset: 0x000841EA
		public void Constructed()
		{
			this.m_IsRemoved = false;
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x00085DF3 File Offset: 0x000841F3
		public bool IsRemoved()
		{
			return this.m_IsRemoved;
		}

		// Token: 0x060019C7 RID: 6599 RVA: 0x00085DFC File Offset: 0x000841FC
		public void Destroy()
		{
			if (this.m_Player != null)
			{
				if (this.target != null)
				{
					this.target.material = this.originalMaterial;
				}
				if (!this.useOriginalMaterial && this.target != null)
				{
					this.target.enabled = false;
				}
				this.originalMaterial = null;
				this.m_Player.Dispose();
				this.m_Player = null;
				this.material = null;
			}
		}

		// Token: 0x060019C8 RID: 6600 RVA: 0x00085E80 File Offset: 0x00084280
		public void Update()
		{
			if (this.m_Player == null)
			{
				return;
			}
			if (this.m_IsPrepareYet && this.m_Player.status == Player.Status.Stop)
			{
				this.m_Player.Prepare();
				this.m_IsPrepared = true;
			}
			if (this.m_IsPlayed && this.m_Player.status == Player.Status.Ready)
			{
				this.Play(null, null, false, false, null, null);
			}
			this.m_Player.Update();
			bool flag;
			if (this.m_Player.isFrameAvailable)
			{
				flag = this.m_Player.UpdateMaterial(this.material);
				if (flag)
				{
					this.OnMaterialUpdated();
				}
			}
			else
			{
				flag = false;
			}
			if (this.isMaterialAvailable != flag)
			{
				this.isMaterialAvailable = flag;
				this.OnMaterialAvailableChanged();
			}
			if (this.renderMode == MovieObject.MovieRenderMode.Always)
			{
				this.RenderMovie();
			}
			if (this.renderMode == MovieObject.MovieRenderMode.OnVisibility)
			{
			}
		}

		// Token: 0x060019C9 RID: 6601 RVA: 0x00085F68 File Offset: 0x00084368
		public MovieObject.eStatus GetStatus()
		{
			if (this.m_Player != null)
			{
				switch (this.m_Player.status)
				{
				case Player.Status.Stop:
					return MovieObject.eStatus.Removed;
				case Player.Status.Prep:
					return MovieObject.eStatus.Prepare;
				case Player.Status.Ready:
					return MovieObject.eStatus.Playing;
				case Player.Status.Playing:
					return MovieObject.eStatus.Playing;
				case Player.Status.PlayEnd:
					return MovieObject.eStatus.Removed;
				}
			}
			return MovieObject.eStatus.None;
		}

		// Token: 0x060019CA RID: 6602 RVA: 0x00085FBE File Offset: 0x000843BE
		public Player.Status GetStatusNative()
		{
			if (this.m_Player != null)
			{
				return this.m_Player.status;
			}
			return Player.Status.Error;
		}

		// Token: 0x060019CB RID: 6603 RVA: 0x00085FD8 File Offset: 0x000843D8
		private void CreateMaterial()
		{
			this._material = new Material(Shader.Find("VertexLit"));
			this._material.name = "CriMana-MovieMaterial";
			this.materialOwn = true;
		}

		// Token: 0x060019CC RID: 6604 RVA: 0x00086008 File Offset: 0x00084408
		private MovieObjectPrepareMethodArgument CreateCanvasInformation()
		{
			if (this.m_PrepareArgument != null)
			{
				return this.m_PrepareArgument;
			}
			MovieObjectPrepareMethodArgument result = null;
			Canvas canvas = null;
			CanvasScaler canvasScaler = null;
			GraphicRaycaster x = null;
			GameObject gameObject = new GameObject("Canvas");
			if (gameObject != null)
			{
				canvas = gameObject.AddComponent<Canvas>();
			}
			if (canvas != null)
			{
				canvas.sortingOrder = 1;
				canvas.renderMode = RenderMode.ScreenSpaceOverlay;
				canvasScaler = gameObject.AddComponent<CanvasScaler>();
			}
			if (canvasScaler != null)
			{
				canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
				canvasScaler.referenceResolution = new Vector2(0f, 0f);
				canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
				x = gameObject.AddComponent<GraphicRaycaster>();
			}
			if (x != null)
			{
				result = new MovieObjectPrepareMethodArgument(null, null, false, false, this.CreateImage(canvas), null, canvas, canvasScaler, this.m_PrepareArgument == null);
			}
			return result;
		}

		// Token: 0x060019CD RID: 6605 RVA: 0x000860D4 File Offset: 0x000844D4
		private Image CreateImage(Canvas canvas)
		{
			GameObject gameObject = null;
			CanvasRenderer x = null;
			Image image = null;
			if (canvas != null)
			{
				gameObject = new GameObject("Movie");
			}
			if (gameObject != null)
			{
				x = gameObject.AddComponent<CanvasRenderer>();
			}
			if (x != null)
			{
				image = gameObject.AddComponent<Image>();
			}
			if (image != null)
			{
				image.transform.SetParent(canvas.transform);
			}
			return image;
		}

		// Token: 0x060019CE RID: 6606 RVA: 0x00086144 File Offset: 0x00084544
		private Image CreateImage()
		{
			Image result = null;
			if (this.m_PrepareArgument != null)
			{
				result = this.CreateImage(this.m_PrepareArgument.m_Canvas);
			}
			return result;
		}

		// Token: 0x060019CF RID: 6607 RVA: 0x00086171 File Offset: 0x00084571
		public void RenderMovie()
		{
			this.m_Player.OnWillRenderObject(null);
		}

		// Token: 0x060019D0 RID: 6608 RVA: 0x0008617F File Offset: 0x0008457F
		private void OnWillRenderObject()
		{
			if (this.renderMode == MovieObject.MovieRenderMode.OnVisibility)
			{
				this.RenderMovie();
			}
		}

		// Token: 0x060019D1 RID: 6609 RVA: 0x00086194 File Offset: 0x00084594
		private void OnMaterialAvailableChanged()
		{
			if (this.isMaterialAvailable)
			{
				this.target.material = this.material;
				this.target.enabled = true;
			}
			else
			{
				this.target.material = this.originalMaterial;
				if (!this.useOriginalMaterial)
				{
					this.target.enabled = false;
				}
			}
			Image component = this.target.gameObject.GetComponent<Image>();
			if (component != null)
			{
				if (this.isMaterialAvailable)
				{
					component.sprite = null;
				}
				else
				{
					component.sprite = this.originalSprite;
				}
			}
		}

		// Token: 0x060019D2 RID: 6610 RVA: 0x00086236 File Offset: 0x00084636
		private void OnMaterialUpdated()
		{
		}

		// Token: 0x060019D3 RID: 6611 RVA: 0x00086238 File Offset: 0x00084638
		public bool IsPaused()
		{
			return this.m_Player != null && this.m_Player.IsPaused();
		}

		// Token: 0x060019D4 RID: 6612 RVA: 0x00086252 File Offset: 0x00084652
		public bool IsCompletePrepare()
		{
			return this.m_Player != null && this.m_IsPrepared && this.GetStatus() == MovieObject.eStatus.Playing;
		}

		// Token: 0x060019D5 RID: 6613 RVA: 0x00086279 File Offset: 0x00084679
		public bool IsPrepared()
		{
			return this.m_IsPrepared;
		}

		// Token: 0x060019D6 RID: 6614 RVA: 0x00086284 File Offset: 0x00084684
		public bool Play(string moviePath = null, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic target = null, Material material = null)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			if (volume != null)
			{
				this.m_Player.SetVolume(volume.m_Volume);
			}
			this.m_IsPlayed = false;
			bool flag = this.IsPrepared();
			if (this.m_Player.movieInfo == null)
			{
				flag = false;
			}
			if ((this.m_Player.status == Player.Status.Stop || this.m_Player.status == Player.Status.Ready || this.m_Player.status == Player.Status.PlayEnd) && !flag)
			{
				this.m_IsPlayed = true;
				this.Prepare(moviePath, volume, loop, alphaMovie, target, material);
			}
			bool flag2 = true;
			if (this.m_Player.status == Player.Status.Stop || this.m_Player.status == Player.Status.Ready || this.m_Player.status == Player.Status.PlayEnd)
			{
				if (this.m_PrepareArgument != null)
				{
					this.m_PrepareArgument.m_Canvas.sortingOrder = 100;
					if (this.m_PrepareArgument != null)
					{
						target = this.m_PrepareArgument.m_Target;
						if (target != null)
						{
							this.m_PrepareArgument.m_Scaler.referenceResolution = new Vector2(this.m_Player.movieInfo.width, this.m_Player.movieInfo.height);
							target.rectTransform.sizeDelta = new Vector2(this.m_PrepareArgument.m_Scaler.referenceResolution.x, this.m_PrepareArgument.m_Scaler.referenceResolution.y);
						}
					}
					target.rectTransform.anchoredPosition = Vector3.zero;
				}
				this.m_Player.Start();
			}
			else
			{
				flag2 = false;
			}
			if (!flag2)
			{
				return true;
			}
			this.m_IsPrepared = false;
			return true;
		}

		// Token: 0x060019D7 RID: 6615 RVA: 0x0008644C File Offset: 0x0008484C
		public bool Prepare()
		{
			return this.Prepare(null, null, false, false, null, null);
		}

		// Token: 0x060019D8 RID: 6616 RVA: 0x0008645C File Offset: 0x0008485C
		public bool Prepare(string moviePath, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic target = null, Material material = null)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_IsPrepareYet = false;
			if (this.m_Player.status == Player.Status.Ready)
			{
				this.m_Player.Stop();
			}
			if (material != null)
			{
				this._material = material;
			}
			else
			{
				this.CreateMaterial();
			}
			if (!string.IsNullOrEmpty(moviePath))
			{
				this.m_Player.SetFile(null, moviePath, Player.SetMode.New);
			}
			this.m_Player.Loop(loop);
			this.m_Player.additiveMode = alphaMovie;
			if (target == null)
			{
				MovieObjectPrepareMethodArgument prepareArgument = this.CreateCanvasInformation();
				this.m_PrepareArgument = prepareArgument;
				if (this.m_PrepareArgument != null)
				{
					target = this.m_PrepareArgument.m_Target;
				}
			}
			if (target == null)
			{
				return false;
			}
			this.target = target;
			Image component = target.gameObject.GetComponent<Image>();
			if (component != null)
			{
				this.originalSprite = component.sprite;
			}
			this.originalMaterial = target.material;
			this.useOriginalMaterial = false;
			if (!this.useOriginalMaterial)
			{
				target.enabled = false;
			}
			if (this.m_Player.status != Player.Status.Stop && this.m_Player.status != Player.Status.PlayEnd)
			{
				this.m_IsPrepareYet = true;
				return true;
			}
			if (volume != null)
			{
				this.m_Volume = volume.m_Volume;
			}
			this.m_Player.Prepare();
			this.m_IsPrepared = true;
			return true;
		}

		// Token: 0x060019D9 RID: 6617 RVA: 0x000865D1 File Offset: 0x000849D1
		public bool Stop()
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_IsPlayed = false;
			this.m_Player.Stop();
			if (this.isMaterialAvailable)
			{
				this.isMaterialAvailable = false;
				this.OnMaterialAvailableChanged();
			}
			return true;
		}

		// Token: 0x060019DA RID: 6618 RVA: 0x0008660B File Offset: 0x00084A0B
		public bool Suspend()
		{
			return this.Pause(true);
		}

		// Token: 0x060019DB RID: 6619 RVA: 0x00086614 File Offset: 0x00084A14
		public bool Pause(bool sw)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_Player.Pause(sw);
			return false;
		}

		// Token: 0x060019DC RID: 6620 RVA: 0x00086630 File Offset: 0x00084A30
		public bool Resume()
		{
			if (this.m_Player == null)
			{
				return false;
			}
			if (this.m_IsPrepared)
			{
				this.m_Player.Start();
				this.m_IsPrepared = false;
			}
			else
			{
				this.m_Player.Pause(false);
			}
			return true;
		}

		// Token: 0x060019DD RID: 6621 RVA: 0x0008666E File Offset: 0x00084A6E
		public bool SetVolume(float volume)
		{
			if (this.m_Player == null)
			{
				return false;
			}
			this.m_Volume = volume;
			this.m_Player.SetVolume(this.m_Volume);
			return true;
		}

		// Token: 0x04002063 RID: 8291
		private Player m_Player;

		// Token: 0x04002065 RID: 8293
		private bool m_IsPrepared;

		// Token: 0x04002066 RID: 8294
		private float m_Volume = 1f;

		// Token: 0x04002068 RID: 8296
		public MovieObject.MovieRenderMode renderMode;

		// Token: 0x04002069 RID: 8297
		public Graphic target;

		// Token: 0x0400206A RID: 8298
		public bool useOriginalMaterial;

		// Token: 0x0400206B RID: 8299
		private Material _material;

		// Token: 0x0400206C RID: 8300
		private bool materialOwn;

		// Token: 0x0400206D RID: 8301
		private Material originalMaterial;

		// Token: 0x0400206E RID: 8302
		private Sprite originalSprite;

		// Token: 0x0400206F RID: 8303
		private MovieObjectPrepareMethodArgument m_PrepareArgument;

		// Token: 0x04002070 RID: 8304
		private bool m_IsPrepareYet;

		// Token: 0x04002071 RID: 8305
		private bool m_IsPlayed;

		// Token: 0x04002072 RID: 8306
		private bool m_IsRemoved;

		// Token: 0x02000519 RID: 1305
		public enum eStatus
		{
			// Token: 0x04002074 RID: 8308
			None = -1,
			// Token: 0x04002075 RID: 8309
			Prepare = 1,
			// Token: 0x04002076 RID: 8310
			Playing,
			// Token: 0x04002077 RID: 8311
			Removed
		}

		// Token: 0x0200051A RID: 1306
		public enum MovieRenderMode
		{
			// Token: 0x04002079 RID: 8313
			Always,
			// Token: 0x0400207A RID: 8314
			OnVisibility,
			// Token: 0x0400207B RID: 8315
			Never
		}
	}
}
