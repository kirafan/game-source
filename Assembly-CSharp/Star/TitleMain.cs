﻿using System;
using Meige;
using Star.UI.Title;
using UnityEngine;

namespace Star
{
	// Token: 0x0200049D RID: 1181
	public class TitleMain : GameStateMain
	{
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x0600172D RID: 5933 RVA: 0x00078976 File Offset: 0x00076D76
		public TitleUI TitleUI
		{
			get
			{
				return this.m_UI;
			}
		}

		// Token: 0x0600172E RID: 5934 RVA: 0x0007897E File Offset: 0x00076D7E
		private void Start()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsRequestedReturnTitle)
			{
				MeigeResourceManager.UnloadHandlerAll();
			}
			this.m_UI.Setup();
			base.SetNextState(0);
		}

		// Token: 0x0600172F RID: 5935 RVA: 0x000789AB File Offset: 0x00076DAB
		private void OnDestroy()
		{
			this.m_UI = null;
		}

		// Token: 0x06001730 RID: 5936 RVA: 0x000789B4 File Offset: 0x00076DB4
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x000789BC File Offset: 0x00076DBC
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new TitleState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new TitleState_Logo(this);
			}
			if (nextStateID != 2)
			{
				return null;
			}
			return new TitleState_Title(this);
		}

		// Token: 0x04001DE1 RID: 7649
		public const int STATE_INIT = 0;

		// Token: 0x04001DE2 RID: 7650
		public const int STATE_LOGO = 1;

		// Token: 0x04001DE3 RID: 7651
		public const int STATE_TITLE = 2;

		// Token: 0x04001DE4 RID: 7652
		[SerializeField]
		private TitleUI m_UI;
	}
}
