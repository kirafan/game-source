﻿using System;

namespace Star
{
	// Token: 0x0200019C RID: 412
	public static class PopUpStateIconListDB_Ext
	{
		// Token: 0x06000B05 RID: 2821 RVA: 0x00041A78 File Offset: 0x0003FE78
		public static PopUpStateIconListDB_Param GetParam(this PopUpStateIconListDB self, ePopUpStateIconType popupType)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_PopUpType == (int)popupType)
				{
					return self.m_Params[i];
				}
			}
			return self.m_Params[0];
		}
	}
}
