﻿using System;

namespace Star
{
	// Token: 0x020002E1 RID: 737
	public class EquipWeaponManagedPartyControllerExGen<ThisType, EnablePartyMemberType> : EquipWeaponManagedPartyController where ThisType : EquipWeaponManagedPartyControllerExGen<ThisType, EnablePartyMemberType> where EnablePartyMemberType : EquipWeaponManagedPartyMemberController
	{
		// Token: 0x06000E55 RID: 3669 RVA: 0x0004DB7A File Offset: 0x0004BF7A
		public EquipWeaponManagedPartyControllerExGen(eCharacterDataControllerType memberType) : base(memberType)
		{
		}

		// Token: 0x06000E56 RID: 3670 RVA: 0x0004DB83 File Offset: 0x0004BF83
		public virtual ThisType Setup(int partyIndex)
		{
			return this.SetupEx(partyIndex);
		}

		// Token: 0x06000E57 RID: 3671 RVA: 0x0004DB8C File Offset: 0x0004BF8C
		public virtual ThisType SetupEx(int partyIndex)
		{
			this.SetupBase(partyIndex);
			return (ThisType)((object)this);
		}

		// Token: 0x06000E58 RID: 3672 RVA: 0x0004DB9C File Offset: 0x0004BF9C
		public EnablePartyMemberType GetMemberEx(int index)
		{
			return this.enablePartyMembers[index];
		}

		// Token: 0x06000E59 RID: 3673 RVA: 0x0004DBAA File Offset: 0x0004BFAA
		public override int HowManyPartyMemberEnable()
		{
			return this.enablePartyMembers.Length;
		}

		// Token: 0x040015A0 RID: 5536
		protected EnablePartyMemberType[] enablePartyMembers;
	}
}
