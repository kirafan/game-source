﻿using System;

namespace Star
{
	// Token: 0x02000573 RID: 1395
	public class ActXlsKeyBindRot : ActXlsKeyBase
	{
		// Token: 0x06001B23 RID: 6947 RVA: 0x0008F94C File Offset: 0x0008DD4C
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Bool(ref this.m_OnOff);
		}

		// Token: 0x04002214 RID: 8724
		public string m_TargetName;

		// Token: 0x04002215 RID: 8725
		public bool m_OnOff;
	}
}
