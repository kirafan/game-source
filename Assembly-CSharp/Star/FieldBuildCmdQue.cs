﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x0200031A RID: 794
	public class FieldBuildCmdQue
	{
		// Token: 0x06000F14 RID: 3860 RVA: 0x00050BE9 File Offset: 0x0004EFE9
		public void AddComQue(FieldBuilCmdBase pque, Action<bool> pcallback = null)
		{
			pque.m_Callback = pcallback;
			this.m_List.Add(pque);
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x00050C00 File Offset: 0x0004F000
		public FieldBuilCmdBase GetQue(Type ftype)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].GetType() == ftype)
				{
					return this.m_List[i];
				}
			}
			return null;
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00050C50 File Offset: 0x0004F050
		public void RemoveQue(Type ftype)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].GetType() == ftype)
				{
					this.m_List.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x0400169B RID: 5787
		private List<FieldBuilCmdBase> m_List = new List<FieldBuilCmdBase>();
	}
}
