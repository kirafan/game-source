﻿using System;

namespace Star
{
	// Token: 0x02000254 RID: 596
	[Serializable]
	public struct TutorialMessageListDB_Param
	{
		// Token: 0x04001388 RID: 5000
		public int m_ID;

		// Token: 0x04001389 RID: 5001
		public int m_Position;

		// Token: 0x0400138A RID: 5002
		public string m_Text;
	}
}
