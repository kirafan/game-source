﻿using System;
using ADVRequestTypes;
using ADVResponseTypes;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x02000007 RID: 7
	public class ADVAPI
	{
		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000044 RID: 68 RVA: 0x00003A18 File Offset: 0x00001E18
		// (remove) Token: 0x06000045 RID: 69 RVA: 0x00003A50 File Offset: 0x00001E50
		private event Action m_OnResponse_AdvAdd;

		// Token: 0x06000046 RID: 70 RVA: 0x00003A88 File Offset: 0x00001E88
		public void Request_AdvAdd(int advID, Action callback, bool isOpenConnectingIcon = true)
		{
			ADVRequestTypes.Add add = new ADVRequestTypes.Add();
			this.m_AdvID = advID;
			this.m_OnResponse_AdvAdd = callback;
			add.advId = advID.ToString();
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			switch (tutoarialSeq)
			{
			case eTutorialSeq.PresentGot_NextIsADVGachaPrevPlay:
			case eTutorialSeq.GachaDecided_NextIsADVGachaAfterPlay:
			case eTutorialSeq.ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play:
			case eTutorialSeq.ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play:
			case eTutorialSeq.QuestLogSeted_NextIsADVPrologueEnd:
				add.stepCode = (int)(tutoarialSeq + 1);
				break;
			}
			MeigewwwParam wwwParam = ADVRequest.Add(add, new MeigewwwParam.Callback(this.OnResponse_AdvAdd));
			NetworkQueueManager.Request(wwwParam);
			if (isOpenConnectingIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00003B38 File Offset: 0x00001F38
		private void OnResponse_AdvAdd(MeigewwwParam wwwParam)
		{
			ADVResponseTypes.Add add = ADVResponse.Add(wwwParam, ResponseCommon.DialogType.None, null);
			if (add == null)
			{
				return;
			}
			ResultCode result = add.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.AddOccurredAdvID(this.m_AdvID);
				switch (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq())
				{
				case eTutorialSeq.PresentGot_NextIsADVGachaPrevPlay:
				case eTutorialSeq.GachaDecided_NextIsADVGachaAfterPlay:
				case eTutorialSeq.ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play:
				case eTutorialSeq.ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play:
				case eTutorialSeq.QuestLogSeted_NextIsADVPrologueEnd:
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.ApplyNextTutorialSeq();
					break;
				}
				this.m_OnResponse_AdvAdd.Call();
				this.m_OnResponse_AdvAdd = null;
			}
		}

		// Token: 0x0400003A RID: 58
		private int m_AdvID = -1;
	}
}
