﻿using System;
using Star.UI.Download;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x020003E9 RID: 1001
	public class DownloadMain : GameStateMain
	{
		// Token: 0x1700015E RID: 350
		// (get) Token: 0x06001305 RID: 4869 RVA: 0x00065EAE File Offset: 0x000642AE
		public DownloadUI DownloadUI
		{
			get
			{
				return this.m_DownloadUI;
			}
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x06001306 RID: 4870 RVA: 0x00065EB6 File Offset: 0x000642B6
		// (set) Token: 0x06001307 RID: 4871 RVA: 0x00065EBE File Offset: 0x000642BE
		public MovieCanvas MovieCanvas
		{
			get
			{
				return this.m_MovieCanvas;
			}
			set
			{
				this.m_MovieCanvas = value;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x06001308 RID: 4872 RVA: 0x00065EC7 File Offset: 0x000642C7
		public MoviePlayUI MoviePlayUI
		{
			get
			{
				return this.m_MoviePlayUI;
			}
		}

		// Token: 0x06001309 RID: 4873 RVA: 0x00065ECF File Offset: 0x000642CF
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x0600130A RID: 4874 RVA: 0x00065ED8 File Offset: 0x000642D8
		private void OnDestroy()
		{
			this.m_DownloadUI = null;
			this.m_MoviePlayUI = null;
		}

		// Token: 0x0600130B RID: 4875 RVA: 0x00065EE8 File Offset: 0x000642E8
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600130C RID: 4876 RVA: 0x00065EF0 File Offset: 0x000642F0
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID != 0)
			{
				return null;
			}
			return new DownloadState_Main(this);
		}

		// Token: 0x0600130D RID: 4877 RVA: 0x00065F17 File Offset: 0x00064317
		public void TouchDisplayUIOnPlayingMovie()
		{
			if (this.MovieCanvas != null && this.DownloadUI != null && this.MovieCanvas.IsPlaying())
			{
				this.DownloadUI.TouchDisplay();
			}
		}

		// Token: 0x040019ED RID: 6637
		public const int STATE_MAIN = 0;

		// Token: 0x040019EE RID: 6638
		[SerializeField]
		private DownloadUI m_DownloadUI;

		// Token: 0x040019EF RID: 6639
		[SerializeField]
		private MoviePlayUI m_MoviePlayUI;

		// Token: 0x040019F0 RID: 6640
		[SerializeField]
		private MovieCanvas m_MovieCanvas;
	}
}
