﻿using System;
using Star.UI.MoviePlay;

namespace Star
{
	// Token: 0x0200044F RID: 1103
	public class MoviePlayState_Play : MoviePlayState
	{
		// Token: 0x06001548 RID: 5448 RVA: 0x0006F2B9 File Offset: 0x0006D6B9
		public MoviePlayState_Play(MoviePlayMain owner) : base(owner)
		{
		}

		// Token: 0x06001549 RID: 5449 RVA: 0x0006F2D4 File Offset: 0x0006D6D4
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600154A RID: 5450 RVA: 0x0006F2D7 File Offset: 0x0006D6D7
		public override void OnStateEnter()
		{
			this.m_Step = MoviePlayState_Play.eStep.Play;
		}

		// Token: 0x0600154B RID: 5451 RVA: 0x0006F2E0 File Offset: 0x0006D6E0
		public override void OnStateExit()
		{
		}

		// Token: 0x0600154C RID: 5452 RVA: 0x0006F2E2 File Offset: 0x0006D6E2
		public override void OnDispose()
		{
		}

		// Token: 0x0600154D RID: 5453 RVA: 0x0006F2E4 File Offset: 0x0006D6E4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MoviePlayState_Play.eStep.Play:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_Owner.MoviePlayUI.SetSkipMode(MoviePlayUI.eSkipMode.Tap);
					this.m_Owner.MoviePlayUI.SetExecSkip(new Action(this.OnExecSkipMovie));
					this.m_Owner.MovieCanvas.SetFile(this.m_Owner.MoviePlayFileName);
					this.m_Owner.MovieCanvas.SetLoop(false);
					this.m_Owner.MovieCanvas.Play();
					this.m_Step = MoviePlayState_Play.eStep.Playing;
				}
				break;
			case MoviePlayState_Play.eStep.Playing:
			{
				bool flag = false;
				if (!this.m_Owner.MovieCanvas.IsPaused() && !this.m_Owner.MovieCanvas.IsPlaying())
				{
					flag = true;
				}
				if (this.m_RequestedMovieSkip)
				{
					flag = true;
				}
				if (flag)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(this.m_Owner.AdvID))
					{
						this.m_Step = MoviePlayState_Play.eStep.End;
					}
					else
					{
						this.m_AdvAPI.Request_AdvAdd(this.m_Owner.AdvID, new Action(this.OnResponse_AdvAdd), true);
						this.m_Step = MoviePlayState_Play.eStep.Request_Wait;
					}
				}
				break;
			}
			case MoviePlayState_Play.eStep.End:
			{
				this.m_NextState = 2147483646;
				this.m_Step = MoviePlayState_Play.eStep.None;
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MoviePlayToNextSceneID != SceneDefine.eSceneID.None)
				{
					this.m_Owner.NextTransitSceneID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MoviePlayToNextSceneID;
				}
				eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
				if (tutoarialSeq == eTutorialSeq.ADVPrologueEndPlayed)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart = true;
					this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
				}
				return this.m_NextState;
			}
			}
			return -1;
		}

		// Token: 0x0600154E RID: 5454 RVA: 0x0006F4CE File Offset: 0x0006D8CE
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x0600154F RID: 5455 RVA: 0x0006F4D7 File Offset: 0x0006D8D7
		private void OnExecSkipMovie()
		{
			this.m_RequestedMovieSkip = true;
		}

		// Token: 0x06001550 RID: 5456 RVA: 0x0006F4E0 File Offset: 0x0006D8E0
		private void OnResponse_AdvAdd()
		{
			this.m_Step = MoviePlayState_Play.eStep.End;
		}

		// Token: 0x04001C23 RID: 7203
		private MoviePlayState_Play.eStep m_Step = MoviePlayState_Play.eStep.None;

		// Token: 0x04001C24 RID: 7204
		private bool m_RequestedMovieSkip;

		// Token: 0x04001C25 RID: 7205
		private ADVAPI m_AdvAPI = new ADVAPI();

		// Token: 0x02000450 RID: 1104
		private enum eStep
		{
			// Token: 0x04001C27 RID: 7207
			None = -1,
			// Token: 0x04001C28 RID: 7208
			Play,
			// Token: 0x04001C29 RID: 7209
			Playing,
			// Token: 0x04001C2A RID: 7210
			Request_Wait,
			// Token: 0x04001C2B RID: 7211
			End
		}
	}
}
