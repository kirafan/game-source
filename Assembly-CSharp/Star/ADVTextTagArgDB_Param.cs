﻿using System;

namespace Star
{
	// Token: 0x02000065 RID: 101
	[Serializable]
	public struct ADVTextTagArgDB_Param
	{
		// Token: 0x04000194 RID: 404
		public string m_ReferenceName;

		// Token: 0x04000195 RID: 405
		public int m_EnumID;
	}
}
