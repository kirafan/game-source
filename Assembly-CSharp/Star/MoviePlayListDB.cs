﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001D5 RID: 469
	public class MoviePlayListDB : ScriptableObject
	{
		// Token: 0x04000B16 RID: 2838
		public MoviePlayListDB_Param[] m_Params;
	}
}
