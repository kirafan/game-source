﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x0200068D RID: 1677
	public class TownComAPIObjPreLeveUp : INetComHandle
	{
		// Token: 0x060021BE RID: 8638 RVA: 0x000B3A68 File Offset: 0x000B1E68
		public TownComAPIObjPreLeveUp()
		{
			this.ApiName = "player/town_facility/prepare_level_up";
			this.Request = true;
			this.ResponseType = typeof(Preparelevelup);
		}

		// Token: 0x0400282B RID: 10283
		public long managedTownFacilityId;

		// Token: 0x0400282C RID: 10284
		public int nextLevel;

		// Token: 0x0400282D RID: 10285
		public int openState;

		// Token: 0x0400282E RID: 10286
		public long buildTime;
	}
}
