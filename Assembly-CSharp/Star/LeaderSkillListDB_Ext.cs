﻿using System;

namespace Star
{
	// Token: 0x02000196 RID: 406
	public static class LeaderSkillListDB_Ext
	{
		// Token: 0x06000AF2 RID: 2802 RVA: 0x00041490 File Offset: 0x0003F890
		public static LeaderSkillListDB_Param GetParam(this LeaderSkillListDB self, int skillID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == skillID)
				{
					return self.m_Params[i];
				}
			}
			return default(LeaderSkillListDB_Param);
		}
	}
}
