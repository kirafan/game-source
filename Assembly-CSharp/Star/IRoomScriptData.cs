﻿using System;

namespace Star
{
	// Token: 0x0200053F RID: 1343
	public class IRoomScriptData
	{
		// Token: 0x06001AB7 RID: 6839 RVA: 0x0008EA28 File Offset: 0x0008CE28
		public static IRoomScriptData CreateScriptData(eRoomScriptCommand ftype)
		{
			IRoomScriptData roomScriptData = (IRoomScriptData)Activator.CreateInstance(IRoomScriptData.ms_SciptUpTable[(int)ftype]);
			roomScriptData.m_Command = ftype;
			return roomScriptData;
		}

		// Token: 0x06001AB8 RID: 6840 RVA: 0x0008EA4F File Offset: 0x0008CE4F
		public virtual void CopyDat(ActXlsKeyBase pbase)
		{
			this.m_Time = (float)pbase.m_Time / 30f;
		}

		// Token: 0x0400215F RID: 8543
		public eRoomScriptCommand m_Command;

		// Token: 0x04002160 RID: 8544
		public float m_Time;

		// Token: 0x04002161 RID: 8545
		public static Type[] ms_SciptUpTable = new Type[]
		{
			typeof(CActScriptKeyPlayAnim),
			typeof(CActScriptKeyWait),
			typeof(CActScriptKeyAnimeWait),
			typeof(CActScriptKeyBind),
			typeof(CActScriptKeyLinkCut),
			typeof(CActScriptKeyViewMode),
			typeof(CActScriptKeyMove),
			typeof(CActScriptKeyPopUp),
			typeof(CActScriptKeySE),
			typeof(CActScriptKeyJump),
			typeof(IRoomScriptData),
			typeof(CActScriptKeyProgramEvent),
			typeof(CActScriptKeyObjectDir),
			typeof(CActScriptKeyEffect),
			typeof(CActScriptKeyBaseTrs),
			typeof(CActScriptKeyPosAnime),
			typeof(CActScriptKeyRotAnime),
			typeof(CActScriptKeyBindRot),
			typeof(CActScriptKeyFrameChg),
			typeof(CActScriptKeyCharaViewMode),
			typeof(CActScriptKeyObjPlayAnim),
			typeof(CActScriptKeyCharaCfg),
			typeof(CActScriptKeyCueSe),
			typeof(CActScriptKeyObjCfg)
		};
	}
}
