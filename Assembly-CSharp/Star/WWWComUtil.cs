﻿using System;
using WWWTypes;

namespace Star
{
	// Token: 0x020003AF RID: 943
	public class WWWComUtil
	{
		// Token: 0x06001212 RID: 4626 RVA: 0x0005F454 File Offset: 0x0005D854
		public static void SetTownBuildListKey(PlayerTown[] plist, bool fdataclear)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			if (fdataclear)
			{
				userTownData.ClearData(-1L, 0L);
			}
			if (plist.Length != 0)
			{
				for (int i = 0; i < plist.Length; i++)
				{
					TownServerForm.DataChunk dataChunk = UserTownUtil.GetDataChunk(plist[i].gridData);
					if (dataChunk.m_DataType == 0)
					{
						TownServerForm.Build build = UserTownUtil.BuildTownBuildList(dataChunk);
						userTownData.ClearData(plist[i].managedTownId, 0L);
						for (int j = 0; j < build.m_Table.Count; j++)
						{
							long actionTime = build.m_Table[j].m_ActionTime;
							for (int k = 0; k < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas.Count; k++)
							{
								if (build.m_Table[j].m_ManageID == SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas[k].m_ManageID)
								{
									actionTime = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas[k].ActionTime;
									break;
								}
							}
							UserTownData.BuildObjectData buildObject = UserTownUtil.CreateUserTownBuildData(build.m_Table[j].m_ManageID, build.m_Table[j].m_BuildPointIndex, build.m_Table[j].m_ObjID, build.m_Table[j].m_IsOpen, build.m_Table[j].m_BuildingMs, actionTime);
							userTownData.AddBuildObjectData(buildObject);
						}
					}
					else if (dataChunk.m_DataType == 1)
					{
						TownServerForm.DebugPlay debugPlay = UserTownUtil.BuildTownDebugCode(dataChunk);
						if (debugPlay.m_DebugPlay)
						{
							userTownData.Level = debugPlay.m_Level;
							userTownData.DebugMngID = plist[i].managedTownId;
						}
					}
				}
			}
		}

		// Token: 0x06001213 RID: 4627 RVA: 0x0005F628 File Offset: 0x0005DA28
		public static void SetTownObjectListKey(PlayerTownFacility[] plist, bool fdataclear)
		{
			if (fdataclear)
			{
				UserTownUtil.ClearTownObjData();
			}
			if (plist.Length != 0)
			{
				UserTownObjectData.SetConnect(true);
			}
			for (int i = 0; i < plist.Length; i++)
			{
				UserTownObjectData padd = new UserTownObjectData(ref plist[i]);
				UserTownUtil.AddTownObjData(padd);
			}
		}

		// Token: 0x06001214 RID: 4628 RVA: 0x0005F678 File Offset: 0x0005DA78
		public static int SetRoomPlacementKey(PlayerRoom[] plist, bool fdataclear)
		{
			int result = 0;
			if (fdataclear)
			{
				UserRoomUtil.ClearRoomData();
			}
			if (plist.Length == 0)
			{
				result = 1;
			}
			else
			{
				for (int i = 0; i < plist.Length; i++)
				{
					UserRoomData userRoomData = UserRoomUtil.GetManageIDToUserRoomData(plist[i].managedRoomId);
					if (userRoomData == null)
					{
						userRoomData = new UserRoomData();
						userRoomData.MngID = plist[i].managedRoomId;
					}
					userRoomData.ClearPlacements();
					userRoomData.PlayerID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
					userRoomData.FloorID = plist[i].floorId;
					if (userRoomData.FloorID <= 0)
					{
						userRoomData.FloorID = 1;
					}
					for (int j = 0; j < plist[i].arrangeData.Length; j++)
					{
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(plist[i].arrangeData[j].x, plist[i].arrangeData[j].y, (CharacterDefine.eDir)plist[i].arrangeData[j].dir, plist[i].arrangeData[j].managedRoomObjectId, plist[i].arrangeData[j].roomNo, plist[i].arrangeData[j].roomObjectId);
						UserRoomObjectData manageIDToUserRoomObjData = UserRoomUtil.GetManageIDToUserRoomObjData(plist[i].arrangeData[j].managedRoomObjectId);
						if (manageIDToUserRoomObjData != null)
						{
							if (manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Background || manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Wall || manageIDToUserRoomObjData.ObjCategory == eRoomObjectCategory.Floor)
							{
								placementData.m_RoomNo = -1;
								placementData.m_ObjectID = manageIDToUserRoomObjData.ResourceID;
							}
							manageIDToUserRoomObjData.LockLinkMngID(plist[i].arrangeData[j].managedRoomObjectId);
						}
						userRoomData.AddPlacement(placementData);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, plist[i].groupId, userRoomData);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, plist[0].managedRoomId);
			}
			return result;
		}

		// Token: 0x06001215 RID: 4629 RVA: 0x0005F878 File Offset: 0x0005DC78
		public static void SetFriendRoomPlacementKey(long fplayerid, PlayerRoom[] plist)
		{
			if (plist.Length != 0)
			{
				for (int i = 0; i < plist.Length; i++)
				{
					UserRoomData userRoomData = UserRoomUtil.GetManageIDToUserRoomData(plist[i].managedRoomId);
					if (userRoomData == null)
					{
						userRoomData = new UserRoomData();
						userRoomData.MngID = plist[i].managedRoomId;
					}
					userRoomData.ClearPlacements();
					userRoomData.PlayerID = fplayerid;
					userRoomData.FloorID = plist[i].floorId;
					if (userRoomData.FloorID <= 0)
					{
						userRoomData.FloorID = 1;
					}
					for (int j = 0; j < plist[i].arrangeData.Length; j++)
					{
						UserRoomData.PlacementData placementData = new UserRoomData.PlacementData(plist[i].arrangeData[j].x, plist[i].arrangeData[j].y, (CharacterDefine.eDir)plist[i].arrangeData[j].dir, plist[i].arrangeData[j].managedRoomObjectId, plist[i].arrangeData[j].roomNo, plist[i].arrangeData[j].roomObjectId);
						if (RoomObjectListUtil.GetIDToCategory(plist[i].arrangeData[j].roomObjectId) == eRoomObjectCategory.Background || RoomObjectListUtil.GetIDToCategory(plist[i].arrangeData[j].roomObjectId) == eRoomObjectCategory.Wall || RoomObjectListUtil.GetIDToCategory(plist[i].arrangeData[j].roomObjectId) == eRoomObjectCategory.Floor)
						{
							placementData.m_RoomNo = -1;
						}
						userRoomData.AddPlacement(placementData);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(fplayerid, plist[i].groupId, userRoomData);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.ChangePlayerFloorActive(fplayerid, plist[0].managedRoomId);
			}
		}

		// Token: 0x06001216 RID: 4630 RVA: 0x0005FA0C File Offset: 0x0005DE0C
		public static void SetRoomObjectList(PlayerRoomObject[] psetuplist, bool fdataclear, bool fsetupmarge = false)
		{
			if (fdataclear)
			{
				UserRoomUtil.ClearRoomObjData();
			}
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			if (!fsetupmarge)
			{
				int num = psetuplist.Length;
				for (int i = 0; i < num; i++)
				{
					UserRoomObjectData userRoomObjectData = UserRoomUtil.GetAccessIDToUserRoomObjData(psetuplist[i].roomObjectId);
					if (userRoomObjectData == null)
					{
						RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(psetuplist[i].roomObjectId);
						userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
						userDataMng.UserRoomObjDatas.Add(userRoomObjectData);
					}
					userRoomObjectData.SetServerData(psetuplist[i].managedRoomObjectId, psetuplist[i].roomObjectId);
				}
			}
			else
			{
				int num = userDataMng.UserRoomObjDatas.Count;
				for (int i = 0; i < num; i++)
				{
					userDataMng.UserRoomObjDatas[i].ClrToListUpUse();
				}
				num = psetuplist.Length;
				for (int i = 0; i < num; i++)
				{
					UserRoomObjectData userRoomObjectData = UserRoomUtil.GetManageIDToUserRoomObjData(psetuplist[i].managedRoomObjectId);
					if (userRoomObjectData == null)
					{
						RoomObjectListDB_Param accessKeyToObjectParam2 = RoomObjectListUtil.GetAccessKeyToObjectParam(psetuplist[i].roomObjectId);
						userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam2.m_Category, accessKeyToObjectParam2.m_ID);
						userDataMng.UserRoomObjDatas.Add(userRoomObjectData);
						userRoomObjectData.SetServerData(psetuplist[i].managedRoomObjectId, psetuplist[i].roomObjectId);
					}
					userRoomObjectData.SetListUpToUse(psetuplist[i].managedRoomObjectId);
				}
				num = userDataMng.UserRoomObjDatas.Count;
				for (int i = num - 1; i >= 0; i--)
				{
					if (!userDataMng.UserRoomObjDatas[i].ReleaseListUpNoUse())
					{
						userDataMng.UserRoomObjDatas.RemoveAt(i);
					}
				}
			}
		}

		// Token: 0x06001217 RID: 4631 RVA: 0x0005FB98 File Offset: 0x0005DF98
		public static void SetFieldPartyListKey(PlayerFieldPartyMember[] plist, bool fdataclear)
		{
			UserFieldCharaData userFieldChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara;
			if (fdataclear)
			{
				userFieldChara.ClearAll(-1L);
				userFieldChara.UpManageID(1L);
			}
			if (plist.Length != 0)
			{
				for (int i = 0; i < plist.Length; i++)
				{
					if (!userFieldChara.IsEntryFldChara(plist[i].managedCharacterId))
					{
						userFieldChara.AddRoomInChara(plist[i]);
					}
				}
			}
		}
	}
}
