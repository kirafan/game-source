﻿using System;
using GachaResponseTypes;
using Meige;
using Star.UI.BackGround;
using WWWTypes;

namespace Star
{
	// Token: 0x0200041D RID: 1053
	public class GachaState_Init : GachaState
	{
		// Token: 0x06001428 RID: 5160 RVA: 0x0006B6CC File Offset: 0x00069ACC
		public GachaState_Init(GachaMain owner) : base(owner)
		{
		}

		// Token: 0x06001429 RID: 5161 RVA: 0x0006B6DC File Offset: 0x00069ADC
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x0600142A RID: 5162 RVA: 0x0006B6DF File Offset: 0x00069ADF
		public override void OnStateEnter()
		{
			this.m_Step = GachaState_Init.eStep.First;
		}

		// Token: 0x0600142B RID: 5163 RVA: 0x0006B6E8 File Offset: 0x00069AE8
		public override void OnStateExit()
		{
		}

		// Token: 0x0600142C RID: 5164 RVA: 0x0006B6EA File Offset: 0x00069AEA
		public override void OnDispose()
		{
		}

		// Token: 0x0600142D RID: 5165 RVA: 0x0006B6EC File Offset: 0x00069AEC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case GachaState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Summon, false);
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(eSoundBgmListDB.BGM_GACHA))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(eSoundBgmListDB.BGM_GACHA, 1f, 0, -1, -1);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.Gacha.Request_GachaGetAll(new Action<MeigewwwParam, GetAll>(this.OnResponse_GachaGetAll), false);
					this.m_Step = GachaState_Init.eStep.Request_Wait;
				}
				break;
			case GachaState_Init.eStep.LoadWait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = GachaState_Init.eStep.End;
				}
				break;
			case GachaState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = GachaState_Init.eStep.None;
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600142E RID: 5166 RVA: 0x0006B80C File Offset: 0x00069C0C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x0600142F RID: 5167 RVA: 0x0006B818 File Offset: 0x00069C18
		private void OnResponse_GachaGetAll(MeigewwwParam wwwParam, GetAll param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				this.m_Step = GachaState_Init.eStep.LoadWait;
			}
		}

		// Token: 0x04001B1B RID: 6939
		private GachaState_Init.eStep m_Step = GachaState_Init.eStep.None;

		// Token: 0x0200041E RID: 1054
		private enum eStep
		{
			// Token: 0x04001B1D RID: 6941
			None = -1,
			// Token: 0x04001B1E RID: 6942
			First,
			// Token: 0x04001B1F RID: 6943
			Request_Wait,
			// Token: 0x04001B20 RID: 6944
			LoadWait,
			// Token: 0x04001B21 RID: 6945
			End
		}
	}
}
