﻿using System;

namespace Star
{
	// Token: 0x0200025A RID: 602
	[Serializable]
	public struct TweetListDB_Param
	{
		// Token: 0x040013AC RID: 5036
		public int m_ID;

		// Token: 0x040013AD RID: 5037
		public string m_Text;

		// Token: 0x040013AE RID: 5038
		public string m_VoiceCueName;

		// Token: 0x040013AF RID: 5039
		public int m_Category;
	}
}
