﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200071C RID: 1820
	public class TownBuildAnime : MonoBehaviour
	{
		// Token: 0x04002AF3 RID: 10995
		[SerializeField]
		public TownBuildAnime.AnimeTools[] m_Table;

		// Token: 0x0200071D RID: 1821
		[Serializable]
		public struct AnimeTools
		{
			// Token: 0x04002AF4 RID: 10996
			public TownPartsBuildAnime.eType m_Type;

			// Token: 0x04002AF5 RID: 10997
			public AnimationClip m_Anime;
		}
	}
}
