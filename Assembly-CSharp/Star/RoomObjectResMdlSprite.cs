﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020005DB RID: 1499
	public class RoomObjectResMdlSprite : IRoomObjectResource
	{
		// Token: 0x06001D7A RID: 7546 RVA: 0x0009DF2C File Offset: 0x0009C32C
		public override void UpdateRes()
		{
			if (this.m_IsPreparing && this.PreparingSprite())
			{
				this.m_IsPreparing = false;
			}
		}

		// Token: 0x06001D7B RID: 7547 RVA: 0x0009DF4B File Offset: 0x0009C34B
		public override void Setup(IRoomObjectControll hndl)
		{
			this.m_Owner = (RoomObjectMdlSprite)hndl;
		}

		// Token: 0x06001D7C RID: 7548 RVA: 0x0009DF59 File Offset: 0x0009C359
		public override void Destroy()
		{
			if (this.m_SpriteHndl != null)
			{
				ObjectResourceManager.ReleaseHandle(this.m_SpriteHndl);
				this.m_SpriteHndl = null;
			}
			this.m_Owner = null;
		}

		// Token: 0x06001D7D RID: 7549 RVA: 0x0009DF80 File Offset: 0x0009C380
		public override void Prepare()
		{
			this.m_IsPreparing = true;
			string spriteResourcePath = RoomUtility.GetSpriteResourcePath(this.m_Owner.ObjCategory, this.m_Owner.ObjID, this.m_Owner.SubKeyID, this.m_Owner.GetDir());
			this.m_SpriteHndl = ObjectResourceManager.CreateHandle(spriteResourcePath);
		}

		// Token: 0x06001D7E RID: 7550 RVA: 0x0009DFD4 File Offset: 0x0009C3D4
		private bool PreparingSprite()
		{
			if (this.m_SpriteHndl == null)
			{
				return true;
			}
			if (!this.m_SpriteHndl.IsDone)
			{
				return false;
			}
			if (this.m_SpriteHndl.IsError)
			{
				ObjectResourceManager.RetryHandle(this.m_SpriteHndl);
				return false;
			}
			GameObject asset = this.m_SpriteHndl.GetAsset<GameObject>();
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(asset);
			this.m_Owner.AttachSprite(gameObject);
			this.m_Owner.SetUpObjectKey(gameObject);
			return true;
		}

		// Token: 0x040023E4 RID: 9188
		private RoomObjectMdlSprite m_Owner;

		// Token: 0x040023E5 RID: 9189
		private MeigeResource.Handler m_SpriteHndl;
	}
}
