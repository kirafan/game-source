﻿using System;

namespace Star
{
	// Token: 0x020002B0 RID: 688
	public class UpgradeResultBeforeAfterPairAfterPartCharacterWrapper : UpgradeResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CE9 RID: 3305 RVA: 0x000490EE File Offset: 0x000474EE
		public UpgradeResultBeforeAfterPairAfterPartCharacterWrapper(EditMain.UpgradeResultData result) : base(eCharacterDataType.AfterUpgradeParam, result)
		{
		}

		// Token: 0x06000CEA RID: 3306 RVA: 0x000490F8 File Offset: 0x000474F8
		protected override EditUtility.OutputCharaParam ConvertResultDataToCharaParam(EditMain.UpgradeResultData result)
		{
			return EditUtility.CalcCharaParamIgnoreTownBuff(this.data.Param.CharaID, this.m_Result.m_AfterLv, this.m_Result.m_AfterExp, base.GetUserNamedData(), null);
		}
	}
}
