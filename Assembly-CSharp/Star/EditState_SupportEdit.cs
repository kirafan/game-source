﻿using System;
using System.Collections.Generic;
using Meige;
using Star.UI.Edit;
using Star.UI.Global;
using SupportcharacterRequestTypes;
using SupportcharacterResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000412 RID: 1042
	public class EditState_SupportEdit : EditState_PartyEditBase<SupportEditUI>
	{
		// Token: 0x060013E4 RID: 5092 RVA: 0x0006A121 File Offset: 0x00068521
		public EditState_SupportEdit(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013E5 RID: 5093 RVA: 0x0006A12A File Offset: 0x0006852A
		public override SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.SupportEditUI;
		}

		// Token: 0x060013E6 RID: 5094 RVA: 0x0006A12D File Offset: 0x0006852D
		public override int GetCharaListSceneId()
		{
			return 5;
		}

		// Token: 0x060013E7 RID: 5095 RVA: 0x0006A130 File Offset: 0x00068530
		public override int GetSelectedPartyIndex()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex;
		}

		// Token: 0x060013E8 RID: 5096 RVA: 0x0006A141 File Offset: 0x00068541
		public override void SetSelectedPartyIndex(int index)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex = index;
		}

		// Token: 0x060013E9 RID: 5097 RVA: 0x0006A153 File Offset: 0x00068553
		public override void SetSelectedPartyMemberIndex(int index)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyMemberIndex = index;
		}

		// Token: 0x060013EA RID: 5098 RVA: 0x0006A165 File Offset: 0x00068565
		public override void SetSelectedCharaMngId(long charaMngId)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportCharaMngID = charaMngId;
		}

		// Token: 0x060013EB RID: 5099 RVA: 0x0006A177 File Offset: 0x00068577
		protected override void OpenTutorialTipsWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_SUPPORT_EDIT, null, null);
		}

		// Token: 0x060013EC RID: 5100 RVA: 0x0006A190 File Offset: 0x00068590
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (globalParam.SelectedSupportPartyIndex != this.m_UI.SelectPartyIndex)
			{
				globalParam.IsDirtyPartyEdit = true;
			}
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x060013ED RID: 5101 RVA: 0x0006A1CC File Offset: 0x000685CC
		protected override void SetSceneInfo()
		{
			base.SetSceneInfo();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.SupportEdit);
		}

		// Token: 0x060013EE RID: 5102 RVA: 0x0006A1E6 File Offset: 0x000685E6
		protected override void RequestSetPartyAll()
		{
			this.Request_SupportPartySetAll(this.m_UI.SelectPartyIndex, new MeigewwwParam.Callback(this.OnResponse_SupportPartySetAll));
		}

		// Token: 0x060013EF RID: 5103 RVA: 0x0006A205 File Offset: 0x00068605
		protected override void RequestSetPartyName(string partyName)
		{
			this.Request_SupportPartySetName(this.m_UI.SelectPartyIndex, partyName, new MeigewwwParam.Callback(this.OnResponse_SupportPartySetName));
		}

		// Token: 0x060013F0 RID: 5104 RVA: 0x0006A228 File Offset: 0x00068628
		public void Request_SupportPartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
			SupportcharacterRequestTypes.Setname setname = new SupportcharacterRequestTypes.Setname();
			UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[partyIndex];
			this.m_RequestedPartyName = partyName;
			setname.managedSupportId = userSupportPartyData.MngID;
			setname.name = partyName;
			MeigewwwParam wwwParam = SupportcharacterRequest.Setname(setname, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013F1 RID: 5105 RVA: 0x0006A288 File Offset: 0x00068688
		protected void OnResponse_SupportPartySetName(MeigewwwParam wwwParam)
		{
			SupportcharacterResponseTypes.Setname setname = SupportcharacterResponse.Setname(wwwParam, ResponseCommon.DialogType.None, null);
			if (setname == null)
			{
				return;
			}
			ResultCode result = setname.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.NgWordError), result.ToMessageString(), null);
				}
			}
			else
			{
				UserSupportPartyData userSupportPartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[this.m_UI.SelectPartyIndex];
				userSupportPartyData.PartyName = this.m_RequestedPartyName;
				this.m_UI.CloseNameChangeWindow();
			}
		}

		// Token: 0x060013F2 RID: 5106 RVA: 0x0006A334 File Offset: 0x00068734
		public void Request_SupportPartySetAll(int activateSupportPatyIndex, MeigewwwParam.Callback callback)
		{
			EditUtility.ActivateSupportParty(activateSupportPatyIndex);
			SupportcharacterRequestTypes.Set set = new SupportcharacterRequestTypes.Set();
			List<UserSupportPartyData> userSupportPartyDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas;
			set.supportCharacters = new PlayerSupport[userSupportPartyDatas.Count];
			for (int i = 0; i < set.supportCharacters.Length; i++)
			{
				UserSupportPartyData userSupportPartyData = userSupportPartyDatas[i];
				set.supportCharacters[i] = new PlayerSupport();
				set.supportCharacters[i].managedSupportId = userSupportPartyData.MngID;
				set.supportCharacters[i].name = userSupportPartyData.PartyName;
				set.supportCharacters[i].active = ((!userSupportPartyData.IsActive) ? 0 : 1);
				int count = userSupportPartyData.CharaMngIDs.Count;
				set.supportCharacters[i].managedCharacterIds = new long[count];
				set.supportCharacters[i].managedWeaponIds = new long[count];
				for (int j = 0; j < count; j++)
				{
					set.supportCharacters[i].managedCharacterIds[j] = userSupportPartyData.CharaMngIDs[j];
					set.supportCharacters[i].managedWeaponIds[j] = userSupportPartyData.WeaponMngIDs[j];
				}
			}
			MeigewwwParam wwwParam = SupportcharacterRequest.Set(set, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013F3 RID: 5107 RVA: 0x0006A488 File Offset: 0x00068888
		private void OnResponse_SupportPartySetAll(MeigewwwParam wwwParam)
		{
			SupportcharacterResponseTypes.Set set = SupportcharacterResponse.Set(wwwParam, ResponseCommon.DialogType.None, null);
			if (set == null)
			{
				return;
			}
			ResultCode result = set.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.SUPPORT_CHAR_LIMIT)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
			}
			else
			{
				base.GoToMenuEnd();
			}
		}
	}
}
