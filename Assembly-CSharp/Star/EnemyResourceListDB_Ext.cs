﻿using System;

namespace Star
{
	// Token: 0x02000194 RID: 404
	public static class EnemyResourceListDB_Ext
	{
		// Token: 0x06000AEF RID: 2799 RVA: 0x000413A0 File Offset: 0x0003F7A0
		public static EnemyResourceListDB_Param GetParam(this EnemyResourceListDB self, int resourceID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ResourceID == resourceID)
				{
					return self.m_Params[i];
				}
			}
			return default(EnemyResourceListDB_Param);
		}

		// Token: 0x06000AF0 RID: 2800 RVA: 0x000413F8 File Offset: 0x0003F7F8
		public static bool IsExistParam(this EnemyResourceListDB self, int resourceID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ResourceID == resourceID)
				{
					return true;
				}
			}
			return false;
		}
	}
}
