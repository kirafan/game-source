﻿using System;

namespace Star
{
	// Token: 0x020003B0 RID: 944
	public class IXlsBinForm
	{
		// Token: 0x06001219 RID: 4633 RVA: 0x0005FC0C File Offset: 0x0005E00C
		public void RealBinFile(BinaryReader pfiles)
		{
			pfiles.GetInt();
			pfiles.GetInt();
			pfiles.GetInt();
			int @int = pfiles.GetInt();
			int filePoint = pfiles.m_FilePoint;
			int[] intTable = pfiles.GetIntTable(@int);
			for (int i = 0; i < @int; i++)
			{
				pfiles.PushPoint(intTable[i] + filePoint);
				this.SheetHeaderData(pfiles);
				pfiles.PopPoint();
			}
		}

		// Token: 0x0600121A RID: 4634 RVA: 0x0005FC70 File Offset: 0x0005E070
		public virtual void SheetHeaderData(BinaryReader pfiles)
		{
			int filePoint = pfiles.m_FilePoint;
			pfiles.GetInt();
			int @int = pfiles.GetInt();
			int int2 = pfiles.GetInt();
			int int3 = pfiles.GetInt();
			pfiles.PushPoint(filePoint + int3);
			int int4 = pfiles.GetInt();
			pfiles.PushPoint(filePoint + int3 + int4);
			string cpstring = pfiles.GetCPString();
			pfiles.PopPoint();
			pfiles.PopPoint();
			pfiles.PushPoint(int2 + filePoint);
			this.SheetDataTag(cpstring, @int, pfiles);
			pfiles.PopPoint();
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x0005FCEA File Offset: 0x0005E0EA
		public virtual void SheetDataTag(string psheetname, int ftablenums, BinaryReader pfiles)
		{
		}
	}
}
