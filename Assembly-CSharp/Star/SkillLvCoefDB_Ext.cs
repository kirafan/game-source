﻿using System;

namespace Star
{
	// Token: 0x020001AA RID: 426
	public static class SkillLvCoefDB_Ext
	{
		// Token: 0x06000B1E RID: 2846 RVA: 0x000423F0 File Offset: 0x000407F0
		public static float CalcValueFromSkillLv(this SkillLvCoefDB self, int currentLv, int tableID, float srcValue)
		{
			for (int i = 1; i < currentLv; i++)
			{
				for (int j = self.m_Params.Length - 1; j >= 0; j--)
				{
					if (currentLv >= self.m_Params[j].m_LvRangeMin && currentLv <= self.m_Params[j].m_LvRangeMax)
					{
						return srcValue * self.m_Params[j].m_Coefs[tableID];
					}
				}
			}
			return srcValue;
		}
	}
}
