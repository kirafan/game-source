﻿using System;

namespace Star
{
	// Token: 0x02000187 RID: 391
	public static class ADVListDB_Ext
	{
		// Token: 0x06000AD4 RID: 2772 RVA: 0x00040944 File Offset: 0x0003ED44
		public static ADVListDB_Param GetParam(this ADVListDB self, int advID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_AdvID == advID)
				{
					return self.m_Params[i];
				}
			}
			return default(ADVListDB_Param);
		}
	}
}
