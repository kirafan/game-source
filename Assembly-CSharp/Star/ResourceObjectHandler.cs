﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004E3 RID: 1251
	public class ResourceObjectHandler
	{
		// Token: 0x060018ED RID: 6381 RVA: 0x00081FFC File Offset: 0x000803FC
		public ResourceObjectHandler(ResourceRequest request, string path, ResourceObjectHandler.eLoadType loadType)
		{
			this.Request = request;
			this.Path = path;
			this.LoadType = loadType;
			this.RefCount = 1;
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x060018EE RID: 6382 RVA: 0x00082020 File Offset: 0x00080420
		// (set) Token: 0x060018EF RID: 6383 RVA: 0x00082028 File Offset: 0x00080428
		public ResourceObjectHandler.eLoadType LoadType { get; private set; }

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x060018F0 RID: 6384 RVA: 0x00082031 File Offset: 0x00080431
		// (set) Token: 0x060018F1 RID: 6385 RVA: 0x00082039 File Offset: 0x00080439
		public UnityEngine.Object Obj { get; set; }

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x060018F2 RID: 6386 RVA: 0x00082042 File Offset: 0x00080442
		// (set) Token: 0x060018F3 RID: 6387 RVA: 0x0008204A File Offset: 0x0008044A
		public ResourceRequest Request { get; set; }

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x060018F4 RID: 6388 RVA: 0x00082053 File Offset: 0x00080453
		// (set) Token: 0x060018F5 RID: 6389 RVA: 0x0008205B File Offset: 0x0008045B
		public string Path { get; private set; }

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x060018F6 RID: 6390 RVA: 0x00082064 File Offset: 0x00080464
		// (set) Token: 0x060018F7 RID: 6391 RVA: 0x0008206C File Offset: 0x0008046C
		public int RefCount { get; private set; }

		// Token: 0x060018F8 RID: 6392 RVA: 0x00082078 File Offset: 0x00080478
		public int AddRef()
		{
			return ++this.RefCount;
		}

		// Token: 0x060018F9 RID: 6393 RVA: 0x00082098 File Offset: 0x00080498
		public int RemoveRef()
		{
			return --this.RefCount;
		}

		// Token: 0x060018FA RID: 6394 RVA: 0x000820B6 File Offset: 0x000804B6
		public bool IsDone()
		{
			return this.Request == null && this.Obj != null;
		}

		// Token: 0x060018FB RID: 6395 RVA: 0x000820D8 File Offset: 0x000804D8
		public bool ApplyObj()
		{
			if (this.Request == null || !this.Request.isDone)
			{
				return false;
			}
			if (this.Request.asset == null)
			{
				return false;
			}
			this.Obj = this.Request.asset;
			this.Request = null;
			return true;
		}

		// Token: 0x060018FC RID: 6396 RVA: 0x00082133 File Offset: 0x00080533
		public void DestroyObj()
		{
			this.Obj = null;
			this.Request = null;
		}

		// Token: 0x020004E4 RID: 1252
		public enum eLoadType
		{
			// Token: 0x04001F7B RID: 8059
			GameObject,
			// Token: 0x04001F7C RID: 8060
			Sprite,
			// Token: 0x04001F7D RID: 8061
			ScriptableObject
		}
	}
}
