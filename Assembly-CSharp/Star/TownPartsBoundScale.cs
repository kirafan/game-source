﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006B5 RID: 1717
	public class TownPartsBoundScale : ITownPartsAction
	{
		// Token: 0x0600223F RID: 8767 RVA: 0x000B5DC5 File Offset: 0x000B41C5
		public TownPartsBoundScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
			this.m_Active = true;
			this.m_Marker = pself;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Target = ftarget;
			this.m_Base = fbase;
		}

		// Token: 0x06002240 RID: 8768 RVA: 0x000B5DFC File Offset: 0x000B41FC
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Marker.localScale = this.m_Base;
				this.m_Active = false;
			}
			if (this.m_Active)
			{
				float num = this.m_Time / this.m_MaxTime;
				this.m_Marker.localScale = (this.m_Target - this.m_Base) * Mathf.Sin(num * 3.1415927f) + this.m_Base;
			}
			return this.m_Active;
		}

		// Token: 0x040028BB RID: 10427
		private Transform m_Marker;

		// Token: 0x040028BC RID: 10428
		private Vector3 m_Target;

		// Token: 0x040028BD RID: 10429
		private Vector3 m_Base;

		// Token: 0x040028BE RID: 10430
		private float m_Time;

		// Token: 0x040028BF RID: 10431
		private float m_MaxTime;
	}
}
