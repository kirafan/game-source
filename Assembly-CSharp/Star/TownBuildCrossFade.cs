﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200071E RID: 1822
	public class TownBuildCrossFade : MonoBehaviour
	{
		// Token: 0x06002405 RID: 9221 RVA: 0x000C1404 File Offset: 0x000BF804
		public static TownBuildCrossFade CreateFade(TownBuilder pbuilder)
		{
			GameObject gameObject = new GameObject("FadeModels");
			TownBuildCrossFade townBuildCrossFade = gameObject.AddComponent<TownBuildCrossFade>();
			townBuildCrossFade.SetUp(pbuilder);
			return townBuildCrossFade;
		}

		// Token: 0x06002406 RID: 9222 RVA: 0x000C142B File Offset: 0x000BF82B
		private void OnDestroy()
		{
		}

		// Token: 0x06002407 RID: 9223 RVA: 0x000C1430 File Offset: 0x000BF830
		public void CreateCaptureCamera()
		{
			this.m_CaptureCamera = new GameObject("Capture_Camera");
			Camera camera = this.m_CaptureCamera.AddComponent<Camera>();
			camera.orthographic = true;
			camera.orthographicSize = this.m_RenderCamera.orthographicSize;
			camera.cullingMask = LayerMask.GetMask(new string[]
			{
				"FldFilter"
			});
			camera.depth = this.m_RenderCamera.depth + 1f;
			camera.nearClipPlane = 0f;
			camera.farClipPlane = 100f;
			camera.clearFlags = CameraClearFlags.Nothing;
			this.m_CaptureCamera.transform.SetParent(base.transform, false);
			this.m_RenderTarget.filterMode = FilterMode.Point;
			GameObject gameObject = new GameObject("CaptureRenderObj");
			gameObject.layer = LayerMask.NameToLayer("FldFilter");
			gameObject.transform.localPosition = new Vector3(0f, 0f, 10f);
			this.m_FadeMtl = new Material(Shader.Find("Town/FadeSprite"));
			this.m_FadeMtl.SetTexture("_MainTex", this.m_RenderTarget);
			Mesh mesh = new Mesh();
			MeshRenderer meshRenderer = gameObject.AddComponent<MeshRenderer>();
			MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
			mesh.vertices = new Vector3[]
			{
				new Vector3(-1f, 1f, 0f),
				new Vector3(1f, 1f, 0f),
				new Vector3(-1f, -1f, 0f),
				new Vector3(1f, -1f, 0f)
			};
			mesh.uv = new Vector2[]
			{
				new Vector2(0f, 1f),
				new Vector2(1f, 1f),
				new Vector2(0f, 0f),
				new Vector2(1f, 0f)
			};
			mesh.triangles = new int[]
			{
				0,
				1,
				2,
				2,
				1,
				3
			};
			mesh.RecalculateBounds();
			meshFilter.mesh = mesh;
			meshRenderer.material = this.m_FadeMtl;
			gameObject.transform.SetParent(base.transform, false);
		}

		// Token: 0x06002408 RID: 9224 RVA: 0x000C16A6 File Offset: 0x000BFAA6
		public void SetUp(TownBuilder pbuilder)
		{
			this.m_Builder = pbuilder;
			this.m_RenderCamera = pbuilder.m_GameCamera.GetCamera();
		}

		// Token: 0x06002409 RID: 9225 RVA: 0x000C16C0 File Offset: 0x000BFAC0
		private void Update()
		{
			switch (this.m_Step)
			{
			case 0:
				this.m_Builder.PreLoadResource();
				this.m_Step++;
				break;
			case 1:
				if (this.m_Builder.IsEndPreLoadResource())
				{
					this.m_RenderTarget = new RenderTexture(Screen.width, Screen.height, 0, RenderTextureFormat.RGB565);
					this.m_RenderTarget.Create();
					this.m_RenderDepth = new RenderTexture(Screen.width, Screen.height, 16, RenderTextureFormat.Depth);
					this.m_RenderDepth.Create();
					this.m_RenderSync = this.m_RenderCamera.gameObject.AddComponent<TownRenderSync>();
					this.m_RenderSync.SetLinkRender(this.m_RenderCamera, this.m_RenderTarget, this.m_RenderDepth);
					this.m_Step++;
				}
				break;
			case 2:
				if (this.m_RenderSync.m_Sync)
				{
					UnityEngine.Object.Destroy(this.m_RenderSync);
					this.m_RenderSync = null;
					this.m_RenderDepth.Release();
					UnityEngine.Object.Destroy(this.m_RenderDepth);
					this.m_RenderDepth = null;
					this.CreateCaptureCamera();
					this.m_Builder.RebuildObject();
					this.m_Step++;
				}
				break;
			case 3:
				if (this.m_Builder.IsSetUpRebuild())
				{
					this.m_Step++;
				}
				break;
			case 4:
				this.m_FadeTime += Time.deltaTime;
				if (this.m_FadeTime >= 0.25f)
				{
					this.m_RenderTarget.Release();
					UnityEngine.Object.Destroy(this.m_RenderTarget);
					UnityEngine.Object.Destroy(this.m_FadeMtl);
					this.m_RenderTarget = null;
					this.m_FadeMtl = null;
					UnityEngine.Object.Destroy(base.gameObject);
				}
				else
				{
					Color white = Color.white;
					white.a = 1f - this.m_FadeTime * 4f;
					this.m_FadeMtl.SetColor("_Color", white);
				}
				break;
			}
		}

		// Token: 0x04002AF6 RID: 10998
		private int m_Step;

		// Token: 0x04002AF7 RID: 10999
		private Camera m_RenderCamera;

		// Token: 0x04002AF8 RID: 11000
		private GameObject m_CaptureCamera;

		// Token: 0x04002AF9 RID: 11001
		private Material m_FadeMtl;

		// Token: 0x04002AFA RID: 11002
		private RenderTexture m_RenderTarget;

		// Token: 0x04002AFB RID: 11003
		private RenderTexture m_RenderDepth;

		// Token: 0x04002AFC RID: 11004
		private TownBuilder m_Builder;

		// Token: 0x04002AFD RID: 11005
		private TownRenderSync m_RenderSync;

		// Token: 0x04002AFE RID: 11006
		private float m_FadeTime;
	}
}
