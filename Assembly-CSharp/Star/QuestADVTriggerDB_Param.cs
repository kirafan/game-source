﻿using System;

namespace Star
{
	// Token: 0x020001E2 RID: 482
	[Serializable]
	public struct QuestADVTriggerDB_Param
	{
		// Token: 0x04000BA0 RID: 2976
		public int m_TriggerType;

		// Token: 0x04000BA1 RID: 2977
		public int m_TriggerID;

		// Token: 0x04000BA2 RID: 2978
		public int m_AdvID;
	}
}
