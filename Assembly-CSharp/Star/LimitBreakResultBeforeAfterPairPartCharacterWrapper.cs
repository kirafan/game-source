﻿using System;

namespace Star
{
	// Token: 0x020002B1 RID: 689
	public class LimitBreakResultBeforeAfterPairPartCharacterWrapper : CharacterEditResultSceneBeforeAfterPairPartCharacterDataWrapper<EditMain.LimitBreakResultData>
	{
		// Token: 0x06000CEB RID: 3307 RVA: 0x0004912C File Offset: 0x0004752C
		public LimitBreakResultBeforeAfterPairPartCharacterWrapper(eCharacterDataType characterDataType, EditMain.LimitBreakResultData result) : base(eCharacterDataType.AfterLimitBreakParam, result)
		{
		}

		// Token: 0x06000CEC RID: 3308 RVA: 0x00049138 File Offset: 0x00047538
		public override int GetClassSkillLevel(int index)
		{
			int[] classSkillLevels = this.GetClassSkillLevels();
			if (classSkillLevels == null || index < 0 || classSkillLevels.Length <= index)
			{
				return 1;
			}
			return classSkillLevels[index];
		}
	}
}
