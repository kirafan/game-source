﻿using System;

namespace Star
{
	// Token: 0x02000313 RID: 787
	public enum eXlsScheduleMakeType
	{
		// Token: 0x04001685 RID: 5765
		Fix,
		// Token: 0x04001686 RID: 5766
		Derive,
		// Token: 0x04001687 RID: 5767
		Free,
		// Token: 0x04001688 RID: 5768
		Per
	}
}
