﻿using System;

namespace Star
{
	// Token: 0x020005C7 RID: 1479
	public class RoomCharaEventReq
	{
		// Token: 0x06001CB7 RID: 7351 RVA: 0x000988E2 File Offset: 0x00096CE2
		public bool IsIrqRequest()
		{
			return this.m_IrqRequest;
		}

		// Token: 0x06001CB8 RID: 7352 RVA: 0x000988EA File Offset: 0x00096CEA
		public RoomActionCommand GetCommand()
		{
			return this.m_ActiveEventCmd;
		}

		// Token: 0x06001CB9 RID: 7353 RVA: 0x000988F2 File Offset: 0x00096CF2
		public void ClrCommand()
		{
			this.m_ActiveEventCmd = null;
			this.m_CommandSend = false;
		}

		// Token: 0x06001CBA RID: 7354 RVA: 0x00098902 File Offset: 0x00096D02
		public void ClearReq()
		{
			this.m_IrqRequest = false;
		}

		// Token: 0x06001CBB RID: 7355 RVA: 0x0009890B File Offset: 0x00096D0B
		public bool IsDebugMode()
		{
			return this.m_DebugStop;
		}

		// Token: 0x06001CBC RID: 7356 RVA: 0x00098913 File Offset: 0x00096D13
		public void CancelEventCommand()
		{
			if (this.m_ActiveEventCmd == null || !this.m_ActiveEventCmd.m_Lock)
			{
				this.m_CommandSend = false;
				this.m_ActiveEventCmd = null;
				this.m_IrqRequest = false;
			}
		}

		// Token: 0x06001CBD RID: 7357 RVA: 0x00098945 File Offset: 0x00096D45
		public void RequestDebugCommand(RoomActionCommand pcmd)
		{
			this.m_ActiveEventCmd = pcmd;
			this.m_CommandSend = true;
			this.m_IrqRequest = true;
			this.m_DebugStop = true;
		}

		// Token: 0x06001CBE RID: 7358 RVA: 0x00098963 File Offset: 0x00096D63
		public void RequestIrqCommand(RoomActionCommand pcmd)
		{
			this.m_ActiveEventCmd = pcmd;
			this.m_CommandSend = true;
			this.m_IrqRequest = true;
		}

		// Token: 0x04002375 RID: 9077
		public bool m_CommandSend;

		// Token: 0x04002376 RID: 9078
		public bool m_IrqRequest;

		// Token: 0x04002377 RID: 9079
		public bool m_DebugStop;

		// Token: 0x04002378 RID: 9080
		public RoomActionCommand m_ActiveEventCmd;
	}
}
