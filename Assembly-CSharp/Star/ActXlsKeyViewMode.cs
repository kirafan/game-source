﻿using System;

namespace Star
{
	// Token: 0x02000568 RID: 1384
	public class ActXlsKeyViewMode : ActXlsKeyBase
	{
		// Token: 0x06001B0D RID: 6925 RVA: 0x0008F647 File Offset: 0x0008DA47
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Bool(ref this.m_View);
			pio.Int(ref this.m_EntryTagID);
		}

		// Token: 0x040021F7 RID: 8695
		public string m_TargetName;

		// Token: 0x040021F8 RID: 8696
		public bool m_View;

		// Token: 0x040021F9 RID: 8697
		public int m_EntryTagID;
	}
}
