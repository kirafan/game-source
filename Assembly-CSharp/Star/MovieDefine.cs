﻿using System;

namespace Star
{
	// Token: 0x02000514 RID: 1300
	public static class MovieDefine
	{
		// Token: 0x04002058 RID: 8280
		public const string EXT_USM = ".usm";

		// Token: 0x04002059 RID: 8281
		public const string MV_OPENING = "mv_op";

		// Token: 0x0400205A RID: 8282
		public const string MV_PROLOGUE = "mv_prologue";
	}
}
