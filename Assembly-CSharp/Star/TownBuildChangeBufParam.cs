﻿using System;

namespace Star
{
	// Token: 0x02000720 RID: 1824
	public class TownBuildChangeBufParam : ITownEventCommand
	{
		// Token: 0x0600240C RID: 9228 RVA: 0x000C1940 File Offset: 0x000BFD40
		public TownBuildChangeBufParam(int fbuildpoint, long fmanageid, int fobjid, TownBuildChangeBufParam.BuildUpEvent pcallback = null)
		{
			this.m_Type = eTownRequest.ChangeBuf;
			this.m_BuildPointID = fbuildpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_EventCall = pcallback;
			this.m_Enable = true;
			this.m_Step = TownBuildChangeBufParam.eStep.BuildQue;
			this.m_InitUp = false;
		}

		// Token: 0x0600240D RID: 9229 RVA: 0x000C198C File Offset: 0x000BFD8C
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (this.m_Step == TownBuildChangeBufParam.eStep.BuildQue)
			{
				TownBuildUpBufParam pbuildup = new TownBuildUpBufParam(this.m_BuildPointID, this.m_ManageID, this.m_ObjID, true, null);
				if (pbuilder.BuildToPoint(pbuildup, false))
				{
					this.m_Step = TownBuildChangeBufParam.eStep.SetUpCheck;
				}
			}
			else if (pbuilder.IsBuildMngIDToBuildUp(this.m_ManageID))
			{
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002B02 RID: 11010
		public int m_BuildPointID;

		// Token: 0x04002B03 RID: 11011
		public int m_ObjID;

		// Token: 0x04002B04 RID: 11012
		public long m_ManageID;

		// Token: 0x04002B05 RID: 11013
		public TownBuildChangeBufParam.eStep m_Step;

		// Token: 0x04002B06 RID: 11014
		public bool m_InitUp;

		// Token: 0x04002B07 RID: 11015
		public TownBuildChangeBufParam.BuildUpEvent m_EventCall;

		// Token: 0x02000721 RID: 1825
		public enum eStep
		{
			// Token: 0x04002B09 RID: 11017
			BuildQue,
			// Token: 0x04002B0A RID: 11018
			SetUpCheck
		}

		// Token: 0x02000722 RID: 1826
		// (Invoke) Token: 0x0600240F RID: 9231
		public delegate void BuildUpEvent(ITownObjectHandler hhandle);
	}
}
