﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020006EE RID: 1774
	public class TownObjModelHandle : ITownObjectHandler
	{
		// Token: 0x06002312 RID: 8978 RVA: 0x000B967B File Offset: 0x000B7A7B
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			this.m_SortingOrder = sortingOrder;
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			if (this.m_TownObjResource == null)
			{
				this.m_TownObjResource = new TownObjectResource();
			}
			this.m_TownObjResource.SetupResource(this);
		}

		// Token: 0x06002313 RID: 8979 RVA: 0x000B96B6 File Offset: 0x000B7AB6
		public override void SetEnableRender(bool flg)
		{
			this.m_IsEnableRender = flg;
			if (this.m_Obj != null)
			{
				this.m_Obj.SetActive(flg);
			}
		}

		// Token: 0x06002314 RID: 8980 RVA: 0x000B96DC File Offset: 0x000B7ADC
		public override void Prepare()
		{
			if (this.m_IsAvailable)
			{
				Debug.Log("TownObjectHandler.Prepare() is failed. m_IsAvailable is true.");
				return;
			}
			this.m_TownObjResource.Prepare();
			this.m_IsPreparing = true;
		}

		// Token: 0x06002315 RID: 8981 RVA: 0x000B9708 File Offset: 0x000B7B08
		protected override bool PrepareMain()
		{
			this.m_TownObjResource.Update();
			if (this.m_TownObjResource.IsPreparing())
			{
				return false;
			}
			this.m_TownObjResource.SetAttachModel();
			this.m_Obj.SetActive(true);
			this.m_IsPreparing = false;
			this.m_IsAvailable = true;
			this.SetUpAutoPlayAnime();
			if (this.m_Callback != null)
			{
				this.m_Callback(this);
			}
			return true;
		}

		// Token: 0x06002316 RID: 8982 RVA: 0x000B9778 File Offset: 0x000B7B78
		public override void Destroy()
		{
			if (this.m_Obj != null)
			{
				ObjectResourceManager.ObjectDestroy(this.m_Obj, true);
				this.m_Obj = null;
			}
			if (this.m_TownObjResource != null)
			{
				this.m_TownObjResource.Destroy();
				this.m_TownObjResource = null;
			}
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
			base.Destroy();
		}

		// Token: 0x06002317 RID: 8983 RVA: 0x000B97DC File Offset: 0x000B7BDC
		public void DesModel()
		{
			if (this.m_TownObjResource != null)
			{
				this.m_TownObjResource.Destroy();
				this.m_TownObjResource = null;
			}
			if (this.m_Obj != null)
			{
				UnityEngine.Object.Destroy(this.m_Obj);
				this.m_Obj = null;
			}
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
		}

		// Token: 0x06002318 RID: 8984 RVA: 0x000B9838 File Offset: 0x000B7C38
		public void AttachModel(GameObject modelObj)
		{
			if (modelObj != null)
			{
				this.m_Obj = modelObj;
				Transform component = this.m_Obj.GetComponent<Transform>();
				this.m_MsbHndl = this.m_Obj.GetComponentInChildren<MsbHandler>();
				this.m_MeigeAnimCtrl = this.m_MsbHndl.gameObject.AddComponent<MeigeAnimCtrl>();
				component.localRotation = Quaternion.Euler(0f, 180f, 0f);
				this.m_Obj.SetActive(false);
				this.OnBuildUpModel(component);
				component.position = this.m_Transform.position;
				component.SetParent(this.m_Transform, true);
				MeigeParticleEmitter[] componentsInChildren = this.m_Obj.GetComponentsInChildren<MeigeParticleEmitter>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].SetSortingOrder(this.m_SortingOrder + 5);
				}
				this.SetSortingOrder(this.m_SortingOrder);
			}
		}

		// Token: 0x06002319 RID: 8985 RVA: 0x000B9912 File Offset: 0x000B7D12
		public virtual void OnBuildUpModel(Transform ptrs)
		{
			ptrs.position = this.m_Transform.position;
			ptrs.SetParent(this.m_Transform, true);
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x000B9932 File Offset: 0x000B7D32
		public void AnimCtrlOpen()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Open();
			}
		}

		// Token: 0x0600231B RID: 8987 RVA: 0x000B9950 File Offset: 0x000B7D50
		public void AddAnim(string actionKey, MeigeAnimClipHolder meigeAnimClipHolder)
		{
			if (this.m_MeigeAnimCtrl != null && meigeAnimClipHolder != null)
			{
				this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, meigeAnimClipHolder);
			}
		}

		// Token: 0x0600231C RID: 8988 RVA: 0x000B9982 File Offset: 0x000B7D82
		public void AnimCtrlClose()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Close();
			}
		}

		// Token: 0x0600231D RID: 8989 RVA: 0x000B99A0 File Offset: 0x000B7DA0
		public void ClearAnimCtrl()
		{
			this.m_MeigeAnimCtrl = null;
		}

		// Token: 0x0600231E RID: 8990 RVA: 0x000B99A9 File Offset: 0x000B7DA9
		public void SetUpAutoPlayAnime()
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.SetAnimationPlaySpeed(1f);
				this.m_MeigeAnimCtrl.Play("event_001", 0f, WrapMode.Loop);
			}
		}

		// Token: 0x0600231F RID: 8991 RVA: 0x000B99E2 File Offset: 0x000B7DE2
		public void PlayAnim(string actionKey, WrapMode wrapMode)
		{
			if (this.m_MeigeAnimCtrl != null)
			{
				this.m_MeigeAnimCtrl.Play(actionKey, 0f, wrapMode);
			}
		}

		// Token: 0x06002320 RID: 8992 RVA: 0x000B9A08 File Offset: 0x000B7E08
		public void SetSortingOrder(int sortingOrder)
		{
			if (this.m_MsbHndl != null)
			{
				Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
				if (rendererCache != null)
				{
					for (int i = 0; i < rendererCache.Length; i++)
					{
						rendererCache[i].sortingOrder = sortingOrder;
					}
				}
			}
		}

		// Token: 0x06002321 RID: 8993 RVA: 0x000B9A58 File Offset: 0x000B7E58
		protected void SetRenderFlg(GameObject ptarget, bool flag)
		{
			MsbHandler[] componentsInChildren = ptarget.GetComponentsInChildren<MsbHandler>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].enabled = flag;
			}
			Renderer[] componentsInChildren2 = ptarget.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren2.Length; i++)
			{
				componentsInChildren2[i].enabled = flag;
			}
		}

		// Token: 0x040029F4 RID: 10740
		protected TownObjectResource m_TownObjResource;

		// Token: 0x040029F5 RID: 10741
		protected GameObject m_Obj;

		// Token: 0x040029F6 RID: 10742
		protected int m_SortingOrder;

		// Token: 0x040029F7 RID: 10743
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040029F8 RID: 10744
		protected MsbHandler m_MsbHndl;
	}
}
