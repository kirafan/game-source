﻿using System;
using Star.UI.Global;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x02000491 RID: 1169
	public class ShopState_WeaponSaleList : ShopState
	{
		// Token: 0x060016E7 RID: 5863 RVA: 0x00077783 File Offset: 0x00075B83
		public ShopState_WeaponSaleList(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016E8 RID: 5864 RVA: 0x0007779B File Offset: 0x00075B9B
		public override int GetStateID()
		{
			return 14;
		}

		// Token: 0x060016E9 RID: 5865 RVA: 0x0007779F File Offset: 0x00075B9F
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponSaleList.eStep.First;
		}

		// Token: 0x060016EA RID: 5866 RVA: 0x000777A8 File Offset: 0x00075BA8
		public override void OnStateExit()
		{
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x000777AA File Offset: 0x00075BAA
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016EC RID: 5868 RVA: 0x000777B4 File Offset: 0x00075BB4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponSaleList.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponSaleList.eStep.LoadWait;
				break;
			case ShopState_WeaponSaleList.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponSaleList.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponSaleList.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_WeaponSaleList.eStep.Main;
				}
				break;
			case ShopState_WeaponSaleList.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_UI.SelectButton == WeaponListUI.eButton.Sale)
					{
						this.m_Owner.TargetWeaponMngID = this.m_UI.SelectedWeaponMngID;
						this.m_NextState = 14;
					}
					else
					{
						this.m_NextState = 10;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponSaleList.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponSaleList.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_WeaponSaleList.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016ED RID: 5869 RVA: 0x0007790C File Offset: 0x00075D0C
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<WeaponListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.SetupSale();
			this.m_UI.OnClickButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponSale);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060016EE RID: 5870 RVA: 0x000779AB File Offset: 0x00075DAB
		protected void OnClickButtonCallBack()
		{
			if (this.m_UI.SelectButton == WeaponListUI.eButton.Sale)
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060016EF RID: 5871 RVA: 0x000779C4 File Offset: 0x00075DC4
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060016F0 RID: 5872 RVA: 0x000779D3 File Offset: 0x00075DD3
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001DA5 RID: 7589
		private ShopState_WeaponSaleList.eStep m_Step = ShopState_WeaponSaleList.eStep.None;

		// Token: 0x04001DA6 RID: 7590
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.WeaponListUI;

		// Token: 0x04001DA7 RID: 7591
		private WeaponListUI m_UI;

		// Token: 0x02000492 RID: 1170
		private enum eStep
		{
			// Token: 0x04001DA9 RID: 7593
			None = -1,
			// Token: 0x04001DAA RID: 7594
			First,
			// Token: 0x04001DAB RID: 7595
			LoadStart,
			// Token: 0x04001DAC RID: 7596
			LoadWait,
			// Token: 0x04001DAD RID: 7597
			PlayIn,
			// Token: 0x04001DAE RID: 7598
			Main,
			// Token: 0x04001DAF RID: 7599
			UnloadChildSceneWait
		}
	}
}
