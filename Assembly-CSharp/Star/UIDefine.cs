﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000B1A RID: 2842
	public static class UIDefine
	{
		// Token: 0x04004353 RID: 17235
		public static readonly int[] SORT_ORDER = new int[]
		{
			-100,
			0,
			100,
			200,
			700,
			800,
			900,
			1000,
			1300,
			1100,
			1200,
			1400,
			1500,
			300,
			400,
			500,
			600,
			1200,
			1099
		};

		// Token: 0x04004354 RID: 17236
		public const int STAMINA_DISPMAX = 9999;

		// Token: 0x04004355 RID: 17237
		public const int GEM_DISPMAX = 9999999;

		// Token: 0x04004356 RID: 17238
		public const int GOLD_DISPMAX = 9999999;

		// Token: 0x04004357 RID: 17239
		public const int ITEM_HAVENUM_DISPMAX = 999;

		// Token: 0x04004358 RID: 17240
		public const int CHARA_LV_DIGIT = 3;

		// Token: 0x04004359 RID: 17241
		public const int CHARA_FRIENDSHIP_LV_DIGIT = 2;

		// Token: 0x0400435A RID: 17242
		public const int NEXT_EXP_DIGIT = 9;

		// Token: 0x0400435B RID: 17243
		public const int CHARA_COST_DIGIT = 3;

		// Token: 0x0400435C RID: 17244
		public const int CHARA_STATUS_DIGIT = 6;

		// Token: 0x0400435D RID: 17245
		public const int SKILL_LV_DIGIT = 3;

		// Token: 0x0400435E RID: 17246
		public const int WEAPON_LV_DIGIT = 3;

		// Token: 0x0400435F RID: 17247
		public static readonly Color NONE_ELEMENT_COLOR = new Color(0.5019608f, 0.5019608f, 0.5019608f, 1f);

		// Token: 0x04004360 RID: 17248
		public static readonly Color FIRE_COLOR = new Color(1f, 0.34117648f, 0.019607844f, 1f);

		// Token: 0x04004361 RID: 17249
		public static readonly Color WATER_COLOR = new Color(0.28235295f, 0.73333335f, 0.8745098f, 1f);

		// Token: 0x04004362 RID: 17250
		public static readonly Color EARTH_COLOR = new Color(0.9607843f, 0.5411765f, 0.043137256f, 1f);

		// Token: 0x04004363 RID: 17251
		public static readonly Color WIND_COLOR = new Color(0.36078432f, 0.78431374f, 0.33333334f, 1f);

		// Token: 0x04004364 RID: 17252
		public static readonly Color MOON_COLOR = new Color(0.8784314f, 0.44313726f, 0.79607844f, 1f);

		// Token: 0x04004365 RID: 17253
		public static readonly Color SUN_COLOR = new Color(0.89411765f, 0.80784315f, 0.17254902f, 1f);

		// Token: 0x04004366 RID: 17254
		public static readonly eElementType[] ELEMENT_SORT = new eElementType[]
		{
			eElementType.Fire,
			eElementType.Wind,
			eElementType.Earth,
			eElementType.Water,
			eElementType.Moon,
			eElementType.Sun
		};

		// Token: 0x04004367 RID: 17255
		public static readonly Color CANT_COLOR = new Color(0.39215687f, 0.5882353f, 1f, 1f);

		// Token: 0x02000B1B RID: 2843
		public enum eSortOrderTypeID
		{
			// Token: 0x04004369 RID: 17257
			Inherited = -1,
			// Token: 0x0400436A RID: 17258
			BackGround_Low,
			// Token: 0x0400436B RID: 17259
			UI,
			// Token: 0x0400436C RID: 17260
			BackGround_OverlayUI0,
			// Token: 0x0400436D RID: 17261
			OverlayUI0,
			// Token: 0x0400436E RID: 17262
			GlobalUI,
			// Token: 0x0400436F RID: 17263
			Fade,
			// Token: 0x04004370 RID: 17264
			Loading,
			// Token: 0x04004371 RID: 17265
			UniqueWindow,
			// Token: 0x04004372 RID: 17266
			CmnWindow,
			// Token: 0x04004373 RID: 17267
			TutorialMessage,
			// Token: 0x04004374 RID: 17268
			TutorialMessageOverlay,
			// Token: 0x04004375 RID: 17269
			Connecting,
			// Token: 0x04004376 RID: 17270
			TouchEffect,
			// Token: 0x04004377 RID: 17271
			BackGround_OverlayUI1,
			// Token: 0x04004378 RID: 17272
			OverlayUI1,
			// Token: 0x04004379 RID: 17273
			BackGround_OverlayUI2,
			// Token: 0x0400437A RID: 17274
			OverlayUI2,
			// Token: 0x0400437B RID: 17275
			WebViewUI,
			// Token: 0x0400437C RID: 17276
			InfoUI,
			// Token: 0x0400437D RID: 17277
			Num
		}
	}
}
