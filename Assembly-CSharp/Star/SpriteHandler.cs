﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x02000006 RID: 6
	public class SpriteHandler
	{
		// Token: 0x0600002E RID: 46 RVA: 0x0000377F File Offset: 0x00001B7F
		public SpriteHandler(MeigeResource.Handler hndl, string path)
		{
			this.Hndl = hndl;
			this.Path = path;
			this.RefCount = 1;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600002F RID: 47 RVA: 0x0000379C File Offset: 0x00001B9C
		// (set) Token: 0x06000030 RID: 48 RVA: 0x000037A4 File Offset: 0x00001BA4
		public bool IsDummySprite { get; set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000031 RID: 49 RVA: 0x000037AD File Offset: 0x00001BAD
		// (set) Token: 0x06000032 RID: 50 RVA: 0x000037B5 File Offset: 0x00001BB5
		public Sprite Obj { get; set; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000033 RID: 51 RVA: 0x000037BE File Offset: 0x00001BBE
		// (set) Token: 0x06000034 RID: 52 RVA: 0x000037C6 File Offset: 0x00001BC6
		public int DummyWidth { get; set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000035 RID: 53 RVA: 0x000037CF File Offset: 0x00001BCF
		// (set) Token: 0x06000036 RID: 54 RVA: 0x000037D7 File Offset: 0x00001BD7
		public int DummyHeight { get; set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000037 RID: 55 RVA: 0x000037E0 File Offset: 0x00001BE0
		// (set) Token: 0x06000038 RID: 56 RVA: 0x000037E8 File Offset: 0x00001BE8
		public MeigeResource.Handler Hndl { get; set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000039 RID: 57 RVA: 0x000037F1 File Offset: 0x00001BF1
		// (set) Token: 0x0600003A RID: 58 RVA: 0x000037F9 File Offset: 0x00001BF9
		public string Path { get; private set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00003802 File Offset: 0x00001C02
		// (set) Token: 0x0600003C RID: 60 RVA: 0x0000380A File Offset: 0x00001C0A
		public int RefCount { get; private set; }

		// Token: 0x0600003D RID: 61 RVA: 0x00003814 File Offset: 0x00001C14
		public int AddRef()
		{
			return ++this.RefCount;
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00003834 File Offset: 0x00001C34
		public int RemoveRef()
		{
			return --this.RefCount;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003852 File Offset: 0x00001C52
		public void ResetRef()
		{
			this.RefCount = 0;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x0000385B File Offset: 0x00001C5B
		public bool IsAvailable()
		{
			return this.Hndl == null && this.Obj != null;
		}

		// Token: 0x06000041 RID: 65 RVA: 0x0000387C File Offset: 0x00001C7C
		public bool ApplyObj()
		{
			if (this.Hndl != null && this.Hndl.IsDone)
			{
				if (!this.Hndl.IsError)
				{
					this.Obj = null;
					if (this.Hndl.assets != null)
					{
						for (int i = 0; i < this.Hndl.assets.Length; i++)
						{
							if (this.Hndl.assets[i] is Sprite)
							{
								this.Obj = (this.Hndl.assets[i] as Sprite);
								break;
							}
						}
					}
				}
				this.IsDummySprite = false;
				if (this.Hndl.IsError || this.Obj == null)
				{
					this.IsDummySprite = true;
					this.Obj = Sprite.Create(new Texture2D(this.DummyWidth, this.DummyHeight), new Rect(0f, 0f, (float)this.DummyWidth, (float)this.DummyHeight), new Vector2(0.5f, 0.5f));
					if (this.Hndl.IsError)
					{
						MeigeResourceManager.ClearError(this.Hndl.resourceName);
					}
				}
				MeigeResourceManager.UnloadHandler(this.Hndl, false);
				this.Hndl = null;
				return true;
			}
			return false;
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000039CA File Offset: 0x00001DCA
		public void DestroyObj()
		{
			if (!this.IsDummySprite)
			{
				Resources.UnloadAsset(this.Obj);
			}
			this.Obj = null;
			if (this.Hndl != null)
			{
				MeigeResourceManager.UnloadHandler(this.Hndl, false);
				this.Hndl = null;
			}
		}
	}
}
