﻿using System;

namespace Star
{
	// Token: 0x02000195 RID: 405
	public static class ItemListDB_Ext
	{
		// Token: 0x06000AF1 RID: 2801 RVA: 0x00041438 File Offset: 0x0003F838
		public static ItemListDB_Param GetParam(this ItemListDB self, int itemID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == itemID)
				{
					return self.m_Params[i];
				}
			}
			return default(ItemListDB_Param);
		}
	}
}
