﻿using System;
using TownFacilityResponseTypes;
using TownResponseTypes;

namespace Star
{
	// Token: 0x0200067D RID: 1661
	public class TownComAllDelete : IFldNetComModule
	{
		// Token: 0x06002195 RID: 8597 RVA: 0x000B2D1B File Offset: 0x000B111B
		public TownComAllDelete() : base(IFldNetComManager.Instance)
		{
		}

		// Token: 0x06002196 RID: 8598 RVA: 0x000B2D28 File Offset: 0x000B1128
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06002197 RID: 8599 RVA: 0x000B2D38 File Offset: 0x000B1138
		public bool SetUp()
		{
			TownComAPIGetAll townComAPIGetAll = new TownComAPIGetAll();
			townComAPIGetAll.playerId = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
			this.m_NetMng.SendCom(townComAPIGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06002198 RID: 8600 RVA: 0x000B2D80 File Offset: 0x000B1180
		private void CallbackSetUp(INetComHandle phandle)
		{
			TownResponseTypes.GetAll getAll = phandle.GetResponse() as TownResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_DelStep = 0;
				this.m_Table = new long[getAll.managedTowns.Length];
				for (int i = 0; i < getAll.managedTowns.Length; i++)
				{
					this.m_Table[i] = getAll.managedTowns[i].managedTownId;
				}
				this.m_Step = TownComAllDelete.eStep.DeleteUp;
			}
		}

		// Token: 0x06002199 RID: 8601 RVA: 0x000B2DF0 File Offset: 0x000B11F0
		public bool SetUpObjList()
		{
			TownComAPIObjGetAll phandle = new TownComAPIObjGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackObjSetUp), false);
			return true;
		}

		// Token: 0x0600219A RID: 8602 RVA: 0x000B2E20 File Offset: 0x000B1220
		private void CallbackObjSetUp(INetComHandle phandle)
		{
			TownFacilityResponseTypes.GetAll getAll = phandle.GetResponse() as TownFacilityResponseTypes.GetAll;
			if (getAll != null)
			{
				this.m_DelStep = 0;
				this.m_Table = new long[getAll.managedTownFacilities.Length];
				for (int i = 0; i < getAll.managedTownFacilities.Length; i++)
				{
					this.m_Table[i] = getAll.managedTownFacilities[i].managedTownFacilityId;
				}
				this.m_Step = TownComAllDelete.eStep.DeleteObjUp;
			}
		}

		// Token: 0x0600219B RID: 8603 RVA: 0x000B2E90 File Offset: 0x000B1290
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case TownComAllDelete.eStep.GetState:
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				this.SetUp();
				this.m_Step = TownComAllDelete.eStep.WaitCheck;
				break;
			case TownComAllDelete.eStep.DeleteUp:
				this.m_Step = TownComAllDelete.eStep.SetUpCheck;
				if (!this.DefaultDeleteManage())
				{
					this.m_Step = TownComAllDelete.eStep.GetObjState;
				}
				break;
			case TownComAllDelete.eStep.GetObjState:
				this.SetUpObjList();
				this.m_Step = TownComAllDelete.eStep.WaitCheck;
				break;
			case TownComAllDelete.eStep.DeleteObjUp:
				this.m_Step = TownComAllDelete.eStep.SetUpCheck;
				if (!this.DefaultDeleteObjManage())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
					this.m_Step = TownComAllDelete.eStep.End;
				}
				break;
			case TownComAllDelete.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x0600219C RID: 8604 RVA: 0x000B2F60 File Offset: 0x000B1360
		public bool DefaultDeleteManage()
		{
			if (this.m_DelStep < this.m_Table.Length)
			{
				TownComAPIRemove townComAPIRemove = new TownComAPIRemove();
				townComAPIRemove.managedTownId = this.m_Table[this.m_DelStep];
				this.m_DelStep++;
				this.m_NetMng.SendCom(townComAPIRemove, new INetComHandle.ResponseCallbak(this.CallbackDeleteUp), false);
				return true;
			}
			return false;
		}

		// Token: 0x0600219D RID: 8605 RVA: 0x000B2FC3 File Offset: 0x000B13C3
		private void CallbackDeleteUp(INetComHandle phandle)
		{
			this.m_Step = TownComAllDelete.eStep.DeleteUp;
		}

		// Token: 0x0600219E RID: 8606 RVA: 0x000B2FCC File Offset: 0x000B13CC
		public bool DefaultDeleteObjManage()
		{
			if (this.m_DelStep < this.m_Table.Length)
			{
				TownComAPIObjRemove townComAPIObjRemove = new TownComAPIObjRemove();
				townComAPIObjRemove.managedTownFacilityId = this.m_Table[this.m_DelStep];
				this.m_DelStep++;
				this.m_NetMng.SendCom(townComAPIObjRemove, new INetComHandle.ResponseCallbak(this.CallbackDeleteObjUp), false);
				return true;
			}
			return false;
		}

		// Token: 0x0600219F RID: 8607 RVA: 0x000B302F File Offset: 0x000B142F
		private void CallbackDeleteObjUp(INetComHandle phandle)
		{
			this.m_Step = TownComAllDelete.eStep.DeleteObjUp;
		}

		// Token: 0x040027FA RID: 10234
		private TownComAllDelete.eStep m_Step;

		// Token: 0x040027FB RID: 10235
		private long[] m_Table;

		// Token: 0x040027FC RID: 10236
		private int m_DelStep;

		// Token: 0x0200067E RID: 1662
		public enum eStep
		{
			// Token: 0x040027FE RID: 10238
			GetState,
			// Token: 0x040027FF RID: 10239
			WaitCheck,
			// Token: 0x04002800 RID: 10240
			DeleteUp,
			// Token: 0x04002801 RID: 10241
			SetUpCheck,
			// Token: 0x04002802 RID: 10242
			GetObjState,
			// Token: 0x04002803 RID: 10243
			DeleteObjUp,
			// Token: 0x04002804 RID: 10244
			End
		}
	}
}
