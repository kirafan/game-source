﻿using System;

namespace Star
{
	// Token: 0x020000C4 RID: 196
	public class BattleInputData
	{
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x0600052F RID: 1327 RVA: 0x0001B4B2 File Offset: 0x000198B2
		// (set) Token: 0x06000530 RID: 1328 RVA: 0x0001B4BA File Offset: 0x000198BA
		public BattleInputData.eResult Result { get; private set; }

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000531 RID: 1329 RVA: 0x0001B4C3 File Offset: 0x000198C3
		// (set) Token: 0x06000532 RID: 1330 RVA: 0x0001B4CB File Offset: 0x000198CB
		public bool IsCancel { get; set; }

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000533 RID: 1331 RVA: 0x0001B4D4 File Offset: 0x000198D4
		// (set) Token: 0x06000534 RID: 1332 RVA: 0x0001B4DC File Offset: 0x000198DC
		public int SelectCommandIndex { get; set; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000535 RID: 1333 RVA: 0x0001B4E5 File Offset: 0x000198E5
		// (set) Token: 0x06000536 RID: 1334 RVA: 0x0001B4ED File Offset: 0x000198ED
		public bool SelectIsUniqueSkill { get; set; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000537 RID: 1335 RVA: 0x0001B4F6 File Offset: 0x000198F6
		// (set) Token: 0x06000538 RID: 1336 RVA: 0x0001B4FE File Offset: 0x000198FE
		public int SelectBenchIndex { get; private set; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000539 RID: 1337 RVA: 0x0001B507 File Offset: 0x00019907
		// (set) Token: 0x0600053A RID: 1338 RVA: 0x0001B50F File Offset: 0x0001990F
		public int SelectMasterSkillIndex { get; private set; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600053B RID: 1339 RVA: 0x0001B518 File Offset: 0x00019918
		// (set) Token: 0x0600053C RID: 1340 RVA: 0x0001B520 File Offset: 0x00019920
		public BattleDefine.eJoinMember SelectSingleTargetIndex { get; private set; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600053D RID: 1341 RVA: 0x0001B529 File Offset: 0x00019929
		// (set) Token: 0x0600053E RID: 1342 RVA: 0x0001B531 File Offset: 0x00019931
		public BattleDefine.eJoinMember InterruptJoin { get; private set; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600053F RID: 1343 RVA: 0x0001B53A File Offset: 0x0001993A
		// (set) Token: 0x06000540 RID: 1344 RVA: 0x0001B542 File Offset: 0x00019942
		public bool IsDecideInterruptJoin { get; private set; }

		// Token: 0x06000541 RID: 1345 RVA: 0x0001B54C File Offset: 0x0001994C
		public void Reset()
		{
			this.Result = BattleInputData.eResult.None;
			this.IsCancel = false;
			this.SelectCommandIndex = -1;
			this.SelectIsUniqueSkill = false;
			this.SelectBenchIndex = -1;
			this.SelectMasterSkillIndex = -1;
			this.SelectSingleTargetIndex = BattleDefine.eJoinMember.None;
			this.InterruptJoin = BattleDefine.eJoinMember.None;
			this.IsDecideInterruptJoin = false;
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0001B598 File Offset: 0x00019998
		public void InputCommand_Decide(bool selectIsUniqueSkill = false)
		{
			this.Result = BattleInputData.eResult.Command;
			this.SelectIsUniqueSkill = selectIsUniqueSkill;
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0001B5A8 File Offset: 0x000199A8
		public void InputTogetherAttack_Start()
		{
			this.Result = BattleInputData.eResult.TogetherAttack;
			this.SelectIsUniqueSkill = true;
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x0001B5B8 File Offset: 0x000199B8
		public void InputTogetherAttack_Decide()
		{
			this.Result = BattleInputData.eResult.TogetherAttack;
			this.SelectIsUniqueSkill = true;
		}

		// Token: 0x06000545 RID: 1349 RVA: 0x0001B5C8 File Offset: 0x000199C8
		public void InputTogetherAttack_Cancel()
		{
			this.Result = BattleInputData.eResult.TogetherAttack;
			this.IsCancel = true;
		}

		// Token: 0x06000546 RID: 1350 RVA: 0x0001B5D8 File Offset: 0x000199D8
		public void InputMaster_Start()
		{
			this.Result = BattleInputData.eResult.Master;
			this.SelectBenchIndex = -1;
			this.SelectMasterSkillIndex = -1;
			this.SelectSingleTargetIndex = BattleDefine.eJoinMember.None;
		}

		// Token: 0x06000547 RID: 1351 RVA: 0x0001B5F6 File Offset: 0x000199F6
		public void InputMemberChange_Decide(int benchIndex)
		{
			this.Result = BattleInputData.eResult.Master;
			this.SelectBenchIndex = benchIndex;
		}

		// Token: 0x06000548 RID: 1352 RVA: 0x0001B606 File Offset: 0x00019A06
		public void InputMasterSkill_Decide(int selectMasterSkillIndex, BattleDefine.eJoinMember selectSingleTargetIndex)
		{
			this.Result = BattleInputData.eResult.Master;
			this.SelectMasterSkillIndex = selectMasterSkillIndex;
			this.SelectSingleTargetIndex = selectSingleTargetIndex;
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0001B61D File Offset: 0x00019A1D
		public void InputMaster_Cancel()
		{
			this.Result = BattleInputData.eResult.Master;
			this.IsCancel = true;
		}

		// Token: 0x0600054A RID: 1354 RVA: 0x0001B62D File Offset: 0x00019A2D
		public void InputInterruptFriend_Start()
		{
			this.Result = BattleInputData.eResult.InterruptFriend;
			this.InterruptJoin = BattleDefine.eJoinMember.None;
			this.IsDecideInterruptJoin = false;
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x0001B644 File Offset: 0x00019A44
		public void InputInterruptFriend_Select(BattleDefine.eJoinMember interruptJoin)
		{
			this.Result = BattleInputData.eResult.InterruptFriend;
			this.InterruptJoin = interruptJoin;
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0001B654 File Offset: 0x00019A54
		public void InputInterruptFriend_Decide()
		{
			this.Result = BattleInputData.eResult.InterruptFriend;
			this.IsDecideInterruptJoin = true;
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x0001B664 File Offset: 0x00019A64
		public void InputInterruptFriend_Cancel()
		{
			this.Result = BattleInputData.eResult.InterruptFriend;
			this.IsCancel = true;
		}

		// Token: 0x020000C5 RID: 197
		public enum eResult
		{
			// Token: 0x04000415 RID: 1045
			None = -1,
			// Token: 0x04000416 RID: 1046
			Command,
			// Token: 0x04000417 RID: 1047
			Master,
			// Token: 0x04000418 RID: 1048
			TogetherAttack,
			// Token: 0x04000419 RID: 1049
			InterruptFriend
		}
	}
}
