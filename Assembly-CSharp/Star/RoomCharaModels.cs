﻿using System;
using Star.UI.Room;
using UnityEngine;

namespace Star
{
	// Token: 0x02000582 RID: 1410
	public class RoomCharaModels
	{
		// Token: 0x06001B79 RID: 7033 RVA: 0x00091491 File Offset: 0x0008F891
		public RoomCharaModels(int fcharaid, int froomid, IRoomCharaManager pmanager)
		{
			this.m_Manager = pmanager;
			this.m_Handle = null;
			this.m_CharaID = fcharaid;
			this.m_RoomID = froomid;
			this.m_RoomObj = null;
			this.m_State = RoomCharaModels.eState.Non;
			this.m_FloorID = 0;
		}

		// Token: 0x06001B7A RID: 7034 RVA: 0x000914CA File Offset: 0x0008F8CA
		public bool IsRoomIn()
		{
			return this.m_State != RoomCharaModels.eState.Non;
		}

		// Token: 0x06001B7B RID: 7035 RVA: 0x000914D8 File Offset: 0x0008F8D8
		public void AttachObjectHandle(CharacterHandler fhandle, RoomObjectCtrlChara proomobj)
		{
			this.m_Handle = fhandle;
			this.m_RoomObj = proomobj;
			this.m_State = RoomCharaModels.eState.SetUpWait;
		}

		// Token: 0x06001B7C RID: 7036 RVA: 0x000914F0 File Offset: 0x0008F8F0
		public void SetUpRoomCharaObject(Transform parent)
		{
			CharacterListDB_Param param;
			if (this.m_CharaID != 0)
			{
				param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaID);
			}
			else
			{
				param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(15000000);
			}
			CharacterParam characterParam = new CharacterParam();
			characterParam.CharaID = param.m_CharaID;
			characterParam.NamedType = (eCharaNamedType)param.m_NamedType;
			characterParam.RareType = (eRare)param.m_Rare;
			characterParam.ClassType = (eClassType)param.m_Class;
			CharacterHandler characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, null, null, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Room, true, CharacterDefine.eFriendType.None, "CHARA_" + this.m_CharaID, parent);
			if (characterHandler != null)
			{
				characterHandler.Prepare();
				characterHandler.CacheTransform.localPosition = new Vector3(-10000f, -10000f, -10000f);
				RoomObjectCtrlChara roomObjectCtrlChara = (RoomObjectCtrlChara)RoomObjectControllCreate.CreateCharaObject(characterHandler.gameObject);
				roomObjectCtrlChara.SetManageID(-1L);
				this.AttachObjectHandle(characterHandler, roomObjectCtrlChara);
			}
		}

		// Token: 0x06001B7D RID: 7037 RVA: 0x00091604 File Offset: 0x0008FA04
		public bool IsPreparing()
		{
			bool result = false;
			if (this.m_Handle != null)
			{
				result = this.m_Handle.IsPreparing();
			}
			return result;
		}

		// Token: 0x06001B7E RID: 7038 RVA: 0x00091634 File Offset: 0x0008FA34
		public void UpdatePakage(RoomBuilder pbuilder)
		{
			switch (this.m_State)
			{
			case RoomCharaModels.eState.Non:
			case RoomCharaModels.eState.SetUp:
				this.SetUpRoomCharaObject(pbuilder.CacheTransform);
				this.m_State = RoomCharaModels.eState.SetUpWait;
				break;
			case RoomCharaModels.eState.SetUpWait:
				if (!this.m_Handle.IsPreparing())
				{
					this.BuildLink(pbuilder);
				}
				break;
			case RoomCharaModels.eState.End:
				if (this.m_RoomObj != null)
				{
					this.m_RoomObj.SetObjectLinkCut();
					this.m_RoomObj.UpdateObj();
					this.m_RoomObj.ReleaseEntry();
				}
				this.m_RoomObj = null;
				this.m_State = RoomCharaModels.eState.Non;
				break;
			}
		}

		// Token: 0x06001B7F RID: 7039 RVA: 0x000916EC File Offset: 0x0008FAEC
		public void Clear(RoomBuilder pbuilder)
		{
			if (this.m_RoomObj != null)
			{
				this.m_RoomObj.AbortObjEventMode();
				this.m_RoomObj.ReleaseObj();
			}
			if (this.m_Handle != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.DestroyChara(this.m_Handle);
			}
			this.m_Handle = null;
			this.m_RoomObj = null;
			this.m_State = RoomCharaModels.eState.Non;
		}

		// Token: 0x06001B80 RID: 7040 RVA: 0x0009175C File Offset: 0x0008FB5C
		public void BuildLink(RoomBuilder pbuilder)
		{
			if (this.m_RoomObj != null)
			{
				this.m_RoomObj.LinkManager(pbuilder, this.m_Manager);
				this.m_RoomObj.Setup(this.m_Handle);
				pbuilder.AttachSortObject(this.m_RoomObj, this.m_CharaID);
				this.AttachCharaHud(pbuilder.GetHUDObject());
				this.SetFreePosition(pbuilder.GetGridStatus(this.m_FloorID), pbuilder);
				this.m_State = RoomCharaModels.eState.Update;
			}
		}

		// Token: 0x06001B81 RID: 7041 RVA: 0x000917D8 File Offset: 0x0008FBD8
		private void SetFreePosition(RoomGridState pgrid, RoomBuilder pbuilder)
		{
			if (this.m_RoomObj != null)
			{
				Vector2 vector = pgrid.CalcFreeMovePoint();
				this.m_RoomObj.Initialize((int)vector.x, (int)vector.y);
				Vector2 vector2 = RoomUtility.GridToTilePos(vector.x, vector.y);
				this.m_RoomObj.SetCurrentPos(new Vector3(vector2.x, vector2.y, 0f), (int)vector.x, (int)vector.y);
				this.m_RoomObj.RequestIdleMode(1f);
			}
		}

		// Token: 0x06001B82 RID: 7042 RVA: 0x00091870 File Offset: 0x0008FC70
		public void AttachCharaHud(GameObject phudobj)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(phudobj);
			gameObject.SetActive(true);
			this.m_RoomObj.AttachHud(gameObject.GetComponent<RoomCharaHud>());
		}

		// Token: 0x0400225C RID: 8796
		public int m_CharaID;

		// Token: 0x0400225D RID: 8797
		public int m_RoomID;

		// Token: 0x0400225E RID: 8798
		public int m_AccessKey;

		// Token: 0x0400225F RID: 8799
		private int m_FloorID;

		// Token: 0x04002260 RID: 8800
		public CharacterHandler m_Handle;

		// Token: 0x04002261 RID: 8801
		public RoomObjectCtrlChara m_RoomObj;

		// Token: 0x04002262 RID: 8802
		private IRoomCharaManager m_Manager;

		// Token: 0x04002263 RID: 8803
		private RoomCharaModels.eState m_State;

		// Token: 0x02000583 RID: 1411
		private enum eState
		{
			// Token: 0x04002265 RID: 8805
			Non,
			// Token: 0x04002266 RID: 8806
			SetUp,
			// Token: 0x04002267 RID: 8807
			SetUpWait,
			// Token: 0x04002268 RID: 8808
			Update,
			// Token: 0x04002269 RID: 8809
			End
		}
	}
}
