﻿using System;

namespace Star
{
	// Token: 0x020002E2 RID: 738
	public class EquipWeaponManagedBattlePartyController : EquipWeaponManagedPartyControllerExGen<EquipWeaponManagedBattlePartyController, EquipWeaponManagedBattlePartyMemberController>
	{
		// Token: 0x06000E5A RID: 3674 RVA: 0x0004DBB4 File Offset: 0x0004BFB4
		public EquipWeaponManagedBattlePartyController() : base(eCharacterDataControllerType.ManagedBattleParty)
		{
		}

		// Token: 0x06000E5B RID: 3675 RVA: 0x0004DBBD File Offset: 0x0004BFBD
		public static int SupportMemberToPartyMemberAllsNum(EquipWeaponCharacterDataController supportMember)
		{
			if (supportMember != null && (supportMember.GetCharacterDataControllerType() == eCharacterDataControllerType.Empty || supportMember.GetCharacterDataControllerType() == eCharacterDataControllerType.UnmanagedSupportParty))
			{
				return 6;
			}
			return 5;
		}

		// Token: 0x06000E5C RID: 3676 RVA: 0x0004DBE0 File Offset: 0x0004BFE0
		public static int SupportMemberToPartyMemberAllsNum(CharacterDataWrapperBase supportMember)
		{
			if (supportMember != null && (supportMember.GetCharacterDataType() == eCharacterDataType.Empty || supportMember.GetCharacterDataType() == eCharacterDataType.Lock || supportMember.GetCharacterDataType() == eCharacterDataType.UserSupportData))
			{
				return 6;
			}
			return 5;
		}

		// Token: 0x06000E5D RID: 3677 RVA: 0x0004DC0F File Offset: 0x0004C00F
		public override EquipWeaponManagedBattlePartyController Setup(int partyIndex)
		{
			return this.SetupEx(partyIndex);
		}

		// Token: 0x06000E5E RID: 3678 RVA: 0x0004DC18 File Offset: 0x0004C018
		public override EquipWeaponManagedBattlePartyController SetupEx(int partyIndex)
		{
			return this.SetupEx(partyIndex, null);
		}

		// Token: 0x06000E5F RID: 3679 RVA: 0x0004DC22 File Offset: 0x0004C022
		public EquipWeaponManagedBattlePartyController SetupEx(int partyIndex, CharacterDataWrapperBase supportData)
		{
			base.SetupEx(partyIndex);
			return this.SetupProcess(partyIndex, supportData);
		}

		// Token: 0x06000E60 RID: 3680 RVA: 0x0004DC34 File Offset: 0x0004C034
		protected EquipWeaponManagedBattlePartyController SetupProcess(int partyIndex, CharacterDataWrapperBase supportMember = null)
		{
			int num = EquipWeaponManagedBattlePartyController.SupportMemberToPartyMemberAllsNum(supportMember);
			this.partyMembers = new EquipWeaponPartyMemberController[num];
			this.enablePartyMembers = new EquipWeaponManagedBattlePartyMemberController[5];
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserBattlePartyData userBattlePartyData = userDataMng.UserBattlePartyDatas[partyIndex];
			this.m_MngID = userBattlePartyData.MngID;
			this.m_Name = userBattlePartyData.PartyName;
			for (int i = 0; i < 5; i++)
			{
				this.enablePartyMembers[i] = new EquipWeaponManagedBattlePartyMemberController().SetupEx(this.m_PartyIndex, i);
				this.partyMembers[i] = this.enablePartyMembers[i];
			}
			for (int j = 5; j < num; j++)
			{
				this.partyMembers[j] = new EquipWeaponUnmanagedSupportPartyMemberController().Setup(supportMember, this.m_PartyIndex, j);
			}
			return this;
		}

		// Token: 0x06000E61 RID: 3681 RVA: 0x0004DD00 File Offset: 0x0004C100
		public override int GetPartyCost()
		{
			int num = 0;
			for (int i = 0; i < this.partyMembers.Length; i++)
			{
				EquipWeaponPartyMemberController equipWeaponPartyMemberController = this.partyMembers[i];
				if (equipWeaponPartyMemberController != null && equipWeaponPartyMemberController.GetCharacterDataType() == eCharacterDataType.UserCharacterData)
				{
					num += equipWeaponPartyMemberController.GetCost();
				}
			}
			return num;
		}

		// Token: 0x040015A1 RID: 5537
		public const int PARTYPANEL_CHARA_ALL = 6;

		// Token: 0x040015A2 RID: 5538
		public const int PARTYPANEL_CHARA_ENABLES = 5;
	}
}
