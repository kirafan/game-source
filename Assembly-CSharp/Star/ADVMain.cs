﻿using System;
using Star.ADV;
using UnityEngine;

namespace Star
{
	// Token: 0x020003D9 RID: 985
	public class ADVMain : GameStateMain
	{
		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060012BA RID: 4794 RVA: 0x0006474E File Offset: 0x00062B4E
		public ADVPlayer Player
		{
			get
			{
				return this.m_Player;
			}
		}

		// Token: 0x060012BB RID: 4795 RVA: 0x00064756 File Offset: 0x00062B56
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x060012BC RID: 4796 RVA: 0x0006475F File Offset: 0x00062B5F
		public override void Destroy()
		{
			base.Destroy();
			this.Player.Destroy();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
		}

		// Token: 0x060012BD RID: 4797 RVA: 0x00064782 File Offset: 0x00062B82
		public override bool IsCompleteDestroy()
		{
			return this.Player.UpdateDestroy() && base.IsCompleteDestroy();
		}

		// Token: 0x060012BE RID: 4798 RVA: 0x0006479D File Offset: 0x00062B9D
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x060012BF RID: 4799 RVA: 0x000647A8 File Offset: 0x00062BA8
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new ADVState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new ADVState_Play(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x04001995 RID: 6549
		public const int STATE_INIT = 0;

		// Token: 0x04001996 RID: 6550
		public const int STATE_PLAY = 1;

		// Token: 0x04001997 RID: 6551
		[SerializeField]
		private ADVPlayer m_Player;
	}
}
