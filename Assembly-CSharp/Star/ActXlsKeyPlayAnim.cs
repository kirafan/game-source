﻿using System;

namespace Star
{
	// Token: 0x02000563 RID: 1379
	public class ActXlsKeyPlayAnim : ActXlsKeyBase
	{
		// Token: 0x06001B03 RID: 6915 RVA: 0x0008F500 File Offset: 0x0008D900
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_PlayAnimNo);
			pio.Int(ref this.m_SequenceNo);
			pio.Bool(ref this.m_Loop);
			pio.Int(ref this.m_AnmType);
		}

		// Token: 0x040021E9 RID: 8681
		public int m_PlayAnimNo;

		// Token: 0x040021EA RID: 8682
		public int m_SequenceNo;

		// Token: 0x040021EB RID: 8683
		public bool m_Loop;

		// Token: 0x040021EC RID: 8684
		public int m_AnmType;
	}
}
