﻿using System;

namespace Star
{
	// Token: 0x0200022C RID: 556
	public enum eRoomObjectCategory
	{
		// Token: 0x04000F37 RID: 3895
		Desk,
		// Token: 0x04000F38 RID: 3896
		Chair,
		// Token: 0x04000F39 RID: 3897
		Storage,
		// Token: 0x04000F3A RID: 3898
		Bedding,
		// Token: 0x04000F3B RID: 3899
		Appliances,
		// Token: 0x04000F3C RID: 3900
		Goods,
		// Token: 0x04000F3D RID: 3901
		Hobby,
		// Token: 0x04000F3E RID: 3902
		WallDecoration,
		// Token: 0x04000F3F RID: 3903
		Carpet,
		// Token: 0x04000F40 RID: 3904
		Screen,
		// Token: 0x04000F41 RID: 3905
		Floor,
		// Token: 0x04000F42 RID: 3906
		Wall,
		// Token: 0x04000F43 RID: 3907
		Background,
		// Token: 0x04000F44 RID: 3908
		Msgboard,
		// Token: 0x04000F45 RID: 3909
		Num
	}
}
