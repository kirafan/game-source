﻿using System;

namespace Star
{
	// Token: 0x020005FF RID: 1535
	public class RoomAnimAccessMap
	{
		// Token: 0x06001E37 RID: 7735 RVA: 0x000A2A2A File Offset: 0x000A0E2A
		public string GetAnimeKey(RoomDefine.eAnim fanim, CharacterDefine.eDir fdir)
		{
			if (fanim < RoomDefine.eAnim.Max)
			{
				return this.m_BodyMap[(int)fanim].m_KeyName[(int)fdir];
			}
			return this.m_BodyMap[0].m_KeyName[(int)fdir];
		}

		// Token: 0x06001E38 RID: 7736 RVA: 0x000A2A5A File Offset: 0x000A0E5A
		public string GetAnimeStep(RoomDefine.eAnim fanim, int fstep, CharacterDefine.eDir fdir)
		{
			if (fanim < RoomDefine.eAnim.Max)
			{
				return this.m_BodyMap[(int)fanim].m_KeyName[(int)(fstep * 2 + fdir)];
			}
			return this.m_BodyMap[0].m_KeyName[(int)(fstep * 2 + fdir)];
		}

		// Token: 0x06001E39 RID: 7737 RVA: 0x000A2A94 File Offset: 0x000A0E94
		public string GetAnimeGroup(int fstep, CharacterDefine.eDir fdir)
		{
			int num = (int)(fstep * 2 + fdir);
			if (this.m_BodyMap[this.m_MarkingID].m_KeyName.Length <= num)
			{
				num = 0;
			}
			return this.m_BodyMap[this.m_MarkingID].m_KeyName[num];
		}

		// Token: 0x06001E3A RID: 7738 RVA: 0x000A2AE0 File Offset: 0x000A0EE0
		public void SetMarkingGroup(RoomDefine.eAnim fanim)
		{
			this.m_MarkingID = 0;
			uint num = CRC32.Calc(fanim.ToString());
			for (int i = 0; i < this.m_BodyMap.Length; i++)
			{
				if (this.m_BodyMap[i].m_AccessKey == num)
				{
					this.m_MarkingID = i;
					break;
				}
			}
		}

		// Token: 0x06001E3B RID: 7739 RVA: 0x000A2B44 File Offset: 0x000A0F44
		public void SetMarkingGroup(uint fgroup)
		{
			this.m_MarkingID = 0;
			for (int i = 0; i < this.m_BodyMap.Length; i++)
			{
				if (this.m_BodyMap[i].m_AccessKey == fgroup)
				{
					this.m_MarkingID = i;
					break;
				}
			}
		}

		// Token: 0x040024CB RID: 9419
		public RoomAnimAccessKey[] m_BodyMap;

		// Token: 0x040024CC RID: 9420
		public int m_MarkingID;
	}
}
