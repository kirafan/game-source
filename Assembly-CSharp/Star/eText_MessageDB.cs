﻿using System;

namespace Star
{
	// Token: 0x0200023A RID: 570
	public enum eText_MessageDB
	{
		// Token: 0x04001239 RID: 4665
		None = -1,
		// Token: 0x0400123A RID: 4666
		Dummy_0,
		// Token: 0x0400123B RID: 4667
		DownloadEndTitle,
		// Token: 0x0400123C RID: 4668
		DownloadEnd,
		// Token: 0x0400123D RID: 4669
		DownloadErrTitle,
		// Token: 0x0400123E RID: 4670
		DownloadErr,
		// Token: 0x0400123F RID: 4671
		DownloadCautionTitle,
		// Token: 0x04001240 RID: 4672
		DownloadCaution,
		// Token: 0x04001241 RID: 4673
		PlayerResetTitle = 10,
		// Token: 0x04001242 RID: 4674
		PlayerReset,
		// Token: 0x04001243 RID: 4675
		PlayerMoveConfirmTitle,
		// Token: 0x04001244 RID: 4676
		PlayerMoveConfirm_Android,
		// Token: 0x04001245 RID: 4677
		PlayerMoveConfirm_iOS,
		// Token: 0x04001246 RID: 4678
		PlayerMoveTitle,
		// Token: 0x04001247 RID: 4679
		PlayerMove,
		// Token: 0x04001248 RID: 4680
		PlayerMoveErrTitle,
		// Token: 0x04001249 RID: 4681
		PlayerCacheClearTitle = 19,
		// Token: 0x0400124A RID: 4682
		PlayerCacheClear,
		// Token: 0x0400124B RID: 4683
		MovieSkipTitle = 50,
		// Token: 0x0400124C RID: 4684
		MovieSkip,
		// Token: 0x0400124D RID: 4685
		MovieSkipYes,
		// Token: 0x0400124E RID: 4686
		MovieSkipNo,
		// Token: 0x0400124F RID: 4687
		TutorialGuideTitle_0 = 100,
		// Token: 0x04001250 RID: 4688
		TutorialGuideMessage_0,
		// Token: 0x04001251 RID: 4689
		TutorialGuideTitle_1,
		// Token: 0x04001252 RID: 4690
		TutorialGuideMessage_1,
		// Token: 0x04001253 RID: 4691
		ADVUnlockTitle = 10000,
		// Token: 0x04001254 RID: 4692
		ADVUnlock,
		// Token: 0x04001255 RID: 4693
		CharaFriendshipPopUpTitle = 10010,
		// Token: 0x04001256 RID: 4694
		CharaFriendshipPopUpText,
		// Token: 0x04001257 RID: 4695
		CharaFriendshipPopUpADV,
		// Token: 0x04001258 RID: 4696
		CharaFriendshipPopUpADVCross,
		// Token: 0x04001259 RID: 4697
		CharaFriendshipPopUpVoice,
		// Token: 0x0400125A RID: 4698
		CharaFriendshipPopUpParam,
		// Token: 0x0400125B RID: 4699
		PresentGetTitle = 20002,
		// Token: 0x0400125C RID: 4700
		PresentGetSingle = 20000,
		// Token: 0x0400125D RID: 4701
		PresentGetMulti,
		// Token: 0x0400125E RID: 4702
		PresentHaveNumOver = 20100,
		// Token: 0x0400125F RID: 4703
		PresentAltConfirm = 20200,
		// Token: 0x04001260 RID: 4704
		PresentAltResult,
		// Token: 0x04001261 RID: 4705
		BattleRetireTitle = 50000,
		// Token: 0x04001262 RID: 4706
		BattleRetire,
		// Token: 0x04001263 RID: 4707
		BattleRetryTitle,
		// Token: 0x04001264 RID: 4708
		BattleRetry,
		// Token: 0x04001265 RID: 4709
		BattleRetryGemIsShortTitle,
		// Token: 0x04001266 RID: 4710
		BattleRetryGemIsShort,
		// Token: 0x04001267 RID: 4711
		RoomConfirmStoreOverTitle = 41000,
		// Token: 0x04001268 RID: 4712
		RoomConfirmStoreOver,
		// Token: 0x04001269 RID: 4713
		RoomConfirmSizeOverTitle,
		// Token: 0x0400126A RID: 4714
		RoomConfirmSizeOver,
		// Token: 0x0400126B RID: 4715
		RoomConfirmStoreAllTitle,
		// Token: 0x0400126C RID: 4716
		RoomConfirmStoreAll,
		// Token: 0x0400126D RID: 4717
		RoomConfirmStoreAllFailedAll,
		// Token: 0x0400126E RID: 4718
		RoomConfirmStoreAllFailed,
		// Token: 0x0400126F RID: 4719
		RoomConfirmRoomChangeTitle,
		// Token: 0x04001270 RID: 4720
		RoomConfirmRoomChangeMain,
		// Token: 0x04001271 RID: 4721
		RoomConfirmRoomChangeSub,
		// Token: 0x04001272 RID: 4722
		RoomShopBuyErrTitle = 41100,
		// Token: 0x04001273 RID: 4723
		RoomShopBuyOutOfPeriod,
		// Token: 0x04001274 RID: 4724
		RoomShopBargainOutOfPeriod,
		// Token: 0x04001275 RID: 4725
		RoomShopTutorialTitle = 41110,
		// Token: 0x04001276 RID: 4726
		RoomShopTutorialText,
		// Token: 0x04001277 RID: 4727
		ScheduleSwapTitle = 43000,
		// Token: 0x04001278 RID: 4728
		ScheduleSwap,
		// Token: 0x04001279 RID: 4729
		ScheduleSwapDetail,
		// Token: 0x0400127A RID: 4730
		ScheduleRemove,
		// Token: 0x0400127B RID: 4731
		WeaponEquipTitle = 91000,
		// Token: 0x0400127C RID: 4732
		WeaponEquip,
		// Token: 0x0400127D RID: 4733
		WeaponRemove,
		// Token: 0x0400127E RID: 4734
		WeaponExchange,
		// Token: 0x0400127F RID: 4735
		PartyCostOverTitle = 92000,
		// Token: 0x04001280 RID: 4736
		PartyCostOver,
		// Token: 0x04001281 RID: 4737
		CharacterUpgrade = 96000,
		// Token: 0x04001282 RID: 4738
		CharacterUpgradeCheckWindowTitle,
		// Token: 0x04001283 RID: 4739
		CharacterUpgradeCheckWindowMessage,
		// Token: 0x04001284 RID: 4740
		CharacterLimitBreak = 96100,
		// Token: 0x04001285 RID: 4741
		CharacterLimitBreakCheckWindowTitle,
		// Token: 0x04001286 RID: 4742
		CharacterLimitBreakCheckWindowMessage,
		// Token: 0x04001287 RID: 4743
		CharacterEvolve = 96200,
		// Token: 0x04001288 RID: 4744
		CharacterEvolveCheckWindowTitle,
		// Token: 0x04001289 RID: 4745
		CharacterEvolveCheckWindowMessage,
		// Token: 0x0400128A RID: 4746
		QuestFriendListReloadTitle = 100000,
		// Token: 0x0400128B RID: 4747
		QuestFriendListReloadMessage,
		// Token: 0x0400128C RID: 4748
		QuestFriendListReloadMessageErr,
		// Token: 0x0400128D RID: 4749
		QuestConditionTitle = 100100,
		// Token: 0x0400128E RID: 4750
		QuestCondition,
		// Token: 0x0400128F RID: 4751
		QuestConditionErrTitle,
		// Token: 0x04001290 RID: 4752
		QuestConditionErrItemIsShort,
		// Token: 0x04001291 RID: 4753
		QuestConditionErr,
		// Token: 0x04001292 RID: 4754
		GachaPlayReplayTitle = 120000,
		// Token: 0x04001293 RID: 4755
		GachaPlayReplayMessage,
		// Token: 0x04001294 RID: 4756
		GachaPlayReplayCheck,
		// Token: 0x04001295 RID: 4757
		GachaPlayReplayYes,
		// Token: 0x04001296 RID: 4758
		GachaPlayReplayNo,
		// Token: 0x04001297 RID: 4759
		GachaPlayConfirmTitle,
		// Token: 0x04001298 RID: 4760
		GachaPlayConfirm,
		// Token: 0x04001299 RID: 4761
		GachaPlayConfirmItem,
		// Token: 0x0400129A RID: 4762
		GachaPlayConfirmStep,
		// Token: 0x0400129B RID: 4763
		GachaPlayGemIsShortTitle,
		// Token: 0x0400129C RID: 4764
		GachaPlayGemIsShort,
		// Token: 0x0400129D RID: 4765
		GachaPlayItemIsShortTitle,
		// Token: 0x0400129E RID: 4766
		GachaPlayItemIsShort,
		// Token: 0x0400129F RID: 4767
		IAPCompleteTitle = 130000,
		// Token: 0x040012A0 RID: 4768
		IAPComplete,
		// Token: 0x040012A1 RID: 4769
		IAPFailureReason_PurchasingUnavailable_Title,
		// Token: 0x040012A2 RID: 4770
		IAPFailureReason_PurchasingUnavailable,
		// Token: 0x040012A3 RID: 4771
		IAPFailureReason_ExistingPurchasePending_Title,
		// Token: 0x040012A4 RID: 4772
		IAPFailureReason_ExistingPurchasePending,
		// Token: 0x040012A5 RID: 4773
		IAPFailureReason_ProductUnavailable_Title,
		// Token: 0x040012A6 RID: 4774
		IAPFailureReason_ProductUnavailable,
		// Token: 0x040012A7 RID: 4775
		IAPFailureReason_SignatureInvalid_Title,
		// Token: 0x040012A8 RID: 4776
		IAPFailureReason_SignatureInvalid,
		// Token: 0x040012A9 RID: 4777
		IAPFailureReason_UserCancelled_Title,
		// Token: 0x040012AA RID: 4778
		IAPFailureReason_UserCancelled,
		// Token: 0x040012AB RID: 4779
		IAPFailureReason_PaymentDeclined_Title,
		// Token: 0x040012AC RID: 4780
		IAPFailureReason_PaymentDeclined,
		// Token: 0x040012AD RID: 4781
		IAPFailureReason_DuplicateTransaction_Title,
		// Token: 0x040012AE RID: 4782
		IAPFailureReason_DuplicateTransaction,
		// Token: 0x040012AF RID: 4783
		IAPFailureReason_Unknown_Title,
		// Token: 0x040012B0 RID: 4784
		IAPFailureReason_Unknown,
		// Token: 0x040012B1 RID: 4785
		IAPGetPayloadFailed_Title,
		// Token: 0x040012B2 RID: 4786
		IAPGetPayloadFailed,
		// Token: 0x040012B3 RID: 4787
		IAPLimitTitle,
		// Token: 0x040012B4 RID: 4788
		IAPLimit,
		// Token: 0x040012B5 RID: 4789
		FriendConfirmTitle = 140000,
		// Token: 0x040012B6 RID: 4790
		FriendConfirmTerminate,
		// Token: 0x040012B7 RID: 4791
		FriendConfirmAccept,
		// Token: 0x040012B8 RID: 4792
		FriendConfirmRefuse,
		// Token: 0x040012B9 RID: 4793
		FriendConfirmCancel,
		// Token: 0x040012BA RID: 4794
		FriendConfirmPropose,
		// Token: 0x040012BB RID: 4795
		FriendConfirmSearchTitle = 140010,
		// Token: 0x040012BC RID: 4796
		FriendConfirmSearchNotFound,
		// Token: 0x040012BD RID: 4797
		FriendConfirmSearchOwnID,
		// Token: 0x040012BE RID: 4798
		MenuOptionWindowCheckCancelDialogTitle = 140400,
		// Token: 0x040012BF RID: 4799
		MenuOptionWindowCheckCancelDialogMessage,
		// Token: 0x040012C0 RID: 4800
		MenuOptionWindowCheckResetDialogTitle,
		// Token: 0x040012C1 RID: 4801
		MenuOptionWindowCheckResetDialogMessage,
		// Token: 0x040012C2 RID: 4802
		MenuOpenWebTitle = 141000,
		// Token: 0x040012C3 RID: 4803
		MenuOpenWeb,
		// Token: 0x040012C4 RID: 4804
		MenuGotoTitleTitle,
		// Token: 0x040012C5 RID: 4805
		MenuGotoTitle,
		// Token: 0x040012C6 RID: 4806
		ShopTradeTitle = 170000,
		// Token: 0x040012C7 RID: 4807
		ShopTrade,
		// Token: 0x040012C8 RID: 4808
		ShopTradeResult,
		// Token: 0x040012C9 RID: 4809
		ShopTradeConvert,
		// Token: 0x040012CA RID: 4810
		RoomLimitExtendTitle = 173000,
		// Token: 0x040012CB RID: 4811
		RoomLimitExtend,
		// Token: 0x040012CC RID: 4812
		RoomLimitExtendSub,
		// Token: 0x040012CD RID: 4813
		RoomLimitExtendErr,
		// Token: 0x040012CE RID: 4814
		RoomLimitExtendGemIsShort,
		// Token: 0x040012CF RID: 4815
		RoomLimitExtendResult,
		// Token: 0x040012D0 RID: 4816
		FacilityLimitExtendTitle = 173010,
		// Token: 0x040012D1 RID: 4817
		FacilityLimitExtend,
		// Token: 0x040012D2 RID: 4818
		FacilityLimitExtendSub,
		// Token: 0x040012D3 RID: 4819
		FacilityLimitExtendErr,
		// Token: 0x040012D4 RID: 4820
		FacilityLimitExtendGemIsShort,
		// Token: 0x040012D5 RID: 4821
		FacilityLimitExtendResult,
		// Token: 0x040012D6 RID: 4822
		WeaponCreateTitle = 180000,
		// Token: 0x040012D7 RID: 4823
		WeaponCreate,
		// Token: 0x040012D8 RID: 4824
		WeaponCreateResult,
		// Token: 0x040012D9 RID: 4825
		WeaponCreateDisable,
		// Token: 0x040012DA RID: 4826
		WeaponCreateShortOfGold,
		// Token: 0x040012DB RID: 4827
		WeaponCreateShortOfMaterial,
		// Token: 0x040012DC RID: 4828
		WeaponCreateLimitWeapon,
		// Token: 0x040012DD RID: 4829
		WeaponUpgradeTitle = 181000,
		// Token: 0x040012DE RID: 4830
		WeaponUpgrade,
		// Token: 0x040012DF RID: 4831
		WeaponUpgradeResult,
		// Token: 0x040012E0 RID: 4832
		WeaponLimitExtendTitle = 183000,
		// Token: 0x040012E1 RID: 4833
		WeaponLimitExtend,
		// Token: 0x040012E2 RID: 4834
		WeaponLimitExtendSub,
		// Token: 0x040012E3 RID: 4835
		WeaponLimitExtendErr,
		// Token: 0x040012E4 RID: 4836
		WeaponLimitExtendGemIsShort,
		// Token: 0x040012E5 RID: 4837
		WeaponLimitExtendResult,
		// Token: 0x040012E6 RID: 4838
		QuestRestartConfirmTitle = 190000,
		// Token: 0x040012E7 RID: 4839
		QuestRestartConfirmMessage,
		// Token: 0x040012E8 RID: 4840
		QuestRestartDeleteTitle,
		// Token: 0x040012E9 RID: 4841
		QuestRestartDeleteMessage,
		// Token: 0x040012EA RID: 4842
		QuestRestartFailedTitle,
		// Token: 0x040012EB RID: 4843
		QuestRestartFailedMessage,
		// Token: 0x040012EC RID: 4844
		AppQuitConfirmTitle = 200000,
		// Token: 0x040012ED RID: 4845
		AppQuitConfirmMessage,
		// Token: 0x040012EE RID: 4846
		PushNotifiStaminaFullTitle = 210000,
		// Token: 0x040012EF RID: 4847
		PushNotifiStaminaFull,
		// Token: 0x040012F0 RID: 4848
		PlayerMoveSettingSetting = 220000,
		// Token: 0x040012F1 RID: 4849
		PlayerMoveSettingConfirm,
		// Token: 0x040012F2 RID: 4850
		PlayerMoveSettingSetting_Android,
		// Token: 0x040012F3 RID: 4851
		PlayerMoveSettingSetting_iOS,
		// Token: 0x040012F4 RID: 4852
		PlayerMoveSettingConfirm_Android,
		// Token: 0x040012F5 RID: 4853
		PlayerMoveSettingConfirm_iOS,
		// Token: 0x040012F6 RID: 4854
		PlayerMoveSettingResultSuccess,
		// Token: 0x040012F7 RID: 4855
		PlayerMoveSettingResultError_Already,
		// Token: 0x040012F8 RID: 4856
		PlayerMoveSettingResultError_Android,
		// Token: 0x040012F9 RID: 4857
		PlayerMoveSettingResultError_iOS,
		// Token: 0x040012FA RID: 4858
		PlayerMoveSettingButtonTitle_Android_UseTitle,
		// Token: 0x040012FB RID: 4859
		PlayerMoveSettingButtonTitle_iOS_UseTitle,
		// Token: 0x040012FC RID: 4860
		PlayerMoveSettingButtonTitle_Android_UseMenu,
		// Token: 0x040012FD RID: 4861
		PlayerMoveSettingButtonTitle_iOS_UseMenu,
		// Token: 0x040012FE RID: 4862
		PlayerMoveSettingButtonTitle_UseIDPassword,
		// Token: 0x040012FF RID: 4863
		NgWordError = 230000,
		// Token: 0x04001300 RID: 4864
		Max
	}
}
