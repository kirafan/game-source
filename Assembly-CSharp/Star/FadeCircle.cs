﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000AD3 RID: 2771
	public class FadeCircle : MonoBehaviour
	{
		// Token: 0x060039FA RID: 14842 RVA: 0x00127A19 File Offset: 0x00125E19
		private void Start()
		{
			this.Init();
		}

		// Token: 0x060039FB RID: 14843 RVA: 0x00127A21 File Offset: 0x00125E21
		private void Init()
		{
			if (this.m_IsDoneInit)
			{
				return;
			}
			this.m_IsFading = false;
			this.m_IsDoneInit = true;
			this.m_FadeCircleRectTransform = this.m_FadeCircle.GetComponent<RectTransform>();
			this.m_FadeCircleGameObject = this.m_FadeCircle.gameObject;
		}

		// Token: 0x060039FC RID: 14844 RVA: 0x00127A60 File Offset: 0x00125E60
		private void SetAlphaToObj(float a)
		{
			this.SetColor(new Color(this.m_FadeCircle.color.r, this.m_FadeCircle.color.g, this.m_FadeCircle.color.b, a));
		}

		// Token: 0x060039FD RID: 14845 RVA: 0x00127AB2 File Offset: 0x00125EB2
		public bool IsEnableRender()
		{
			return this.m_FadeCircleGameObject.activeSelf;
		}

		// Token: 0x060039FE RID: 14846 RVA: 0x00127AC7 File Offset: 0x00125EC7
		public bool IsComplete()
		{
			return !this.m_IsFading;
		}

		// Token: 0x060039FF RID: 14847 RVA: 0x00127AD4 File Offset: 0x00125ED4
		public void SetFadeRatio(float ratio)
		{
			if (!this.m_IsDoneInit)
			{
				this.Init();
			}
			this.m_Ratio = ratio;
			this.m_FadeCircleRectTransform.localScale = Vector3.one * this.m_ScaleMax * this.m_Ratio;
			if (ratio <= 0f)
			{
				this.m_FadeCircleGameObject.SetActive(false);
			}
			else
			{
				this.m_FadeCircleGameObject.SetActive(true);
			}
		}

		// Token: 0x06003A00 RID: 14848 RVA: 0x00127B47 File Offset: 0x00125F47
		public void SetColor(Color color)
		{
			this.m_FadeCircle.color = color;
		}

		// Token: 0x06003A01 RID: 14849 RVA: 0x00127B55 File Offset: 0x00125F55
		public Coroutine FadeOut(float time, Action action)
		{
			base.StopAllCoroutines();
			return base.StartCoroutine(this.FadeoutCoroutine(time, action));
		}

		// Token: 0x06003A02 RID: 14850 RVA: 0x00127B6B File Offset: 0x00125F6B
		public Coroutine FadeOut(float time)
		{
			return this.FadeOut(time, null);
		}

		// Token: 0x06003A03 RID: 14851 RVA: 0x00127B75 File Offset: 0x00125F75
		public Coroutine FadeIn(float time, Action action)
		{
			base.StopAllCoroutines();
			return base.StartCoroutine(this.FadeinCoroutine(time, action));
		}

		// Token: 0x06003A04 RID: 14852 RVA: 0x00127B8B File Offset: 0x00125F8B
		public Coroutine FadeIn(float time)
		{
			return this.FadeIn(time, null);
		}

		// Token: 0x06003A05 RID: 14853 RVA: 0x00127B98 File Offset: 0x00125F98
		private IEnumerator FadeinCoroutine(float time, Action action)
		{
			this.m_IsFading = true;
			float endTime = Time.time + time * this.m_Ratio;
			WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
			while (Time.time <= endTime)
			{
				this.m_Ratio = (endTime - Time.time) / time;
				this.SetFadeRatio(this.m_Ratio);
				yield return endFrame;
			}
			this.m_IsFading = false;
			this.m_Ratio = 0f;
			this.SetFadeRatio(this.m_Ratio);
			if (action != null)
			{
				action();
			}
			yield break;
		}

		// Token: 0x06003A06 RID: 14854 RVA: 0x00127BC4 File Offset: 0x00125FC4
		private IEnumerator FadeoutCoroutine(float time, Action action)
		{
			this.m_IsFading = true;
			float endTime = Time.time + time * (1f - this.m_Ratio);
			WaitForEndOfFrame endFrame = new WaitForEndOfFrame();
			while (Time.time <= endTime)
			{
				this.m_Ratio = 1f - (endTime - Time.time) / time;
				this.SetFadeRatio(this.m_Ratio);
				yield return endFrame;
			}
			this.m_IsFading = false;
			this.m_Ratio = 1f;
			this.SetFadeRatio(this.m_Ratio);
			if (action != null)
			{
				action();
			}
			yield break;
		}

		// Token: 0x06003A07 RID: 14855 RVA: 0x00127BED File Offset: 0x00125FED
		private void OnValidate()
		{
			this.Init();
			this.SetFadeRatio(this.m_Ratio);
		}

		// Token: 0x0400413B RID: 16699
		[SerializeField]
		[Range(0f, 1f)]
		private float m_Ratio;

		// Token: 0x0400413C RID: 16700
		[SerializeField]
		private Image m_FadeCircle;

		// Token: 0x0400413D RID: 16701
		private RectTransform m_FadeCircleRectTransform;

		// Token: 0x0400413E RID: 16702
		private GameObject m_FadeCircleGameObject;

		// Token: 0x0400413F RID: 16703
		[SerializeField]
		private float m_ScaleMax = 8f;

		// Token: 0x04004140 RID: 16704
		private bool m_IsFading;

		// Token: 0x04004141 RID: 16705
		private bool m_IsDoneInit;
	}
}
