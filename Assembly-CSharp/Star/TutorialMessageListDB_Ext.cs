﻿using System;

namespace Star
{
	// Token: 0x020001B9 RID: 441
	public static class TutorialMessageListDB_Ext
	{
		// Token: 0x06000B34 RID: 2868 RVA: 0x00042ADC File Offset: 0x00040EDC
		public static TutorialMessageListDB_Param GetParam(this TutorialMessageListDB self, eTutorialMessageListDB id)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)id)
				{
					return self.m_Params[i];
				}
			}
			return default(TutorialMessageListDB_Param);
		}
	}
}
