﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004E7 RID: 1255
	public static class Sort
	{
		// Token: 0x06001909 RID: 6409 RVA: 0x000821C4 File Offset: 0x000805C4
		public static void StableSort<T>(List<T> list, Comparison<T> comparison)
		{
			List<KeyValuePair<int, T>> list2 = new List<KeyValuePair<int, T>>(list.Count);
			for (int i = 0; i < list.Count; i++)
			{
				list2.Add(new KeyValuePair<int, T>(i, list[i]));
			}
			list2.Sort(delegate(KeyValuePair<int, T> x, KeyValuePair<int, T> y)
			{
				int num = comparison(x.Value, y.Value);
				if (num == 0)
				{
					num = x.Key.CompareTo(y.Key);
				}
				return num;
			});
			for (int j = 0; j < list.Count; j++)
			{
				list[j] = list2[j].Value;
			}
		}
	}
}
