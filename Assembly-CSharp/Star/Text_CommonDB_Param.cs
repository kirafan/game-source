﻿using System;

namespace Star
{
	// Token: 0x02000238 RID: 568
	[Serializable]
	public struct Text_CommonDB_Param
	{
		// Token: 0x04001235 RID: 4661
		public int m_ID;

		// Token: 0x04001236 RID: 4662
		public string m_Text;
	}
}
