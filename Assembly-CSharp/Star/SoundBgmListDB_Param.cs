﻿using System;

namespace Star
{
	// Token: 0x02000210 RID: 528
	[Serializable]
	public struct SoundBgmListDB_Param
	{
		// Token: 0x04000D1B RID: 3355
		public int m_ID;

		// Token: 0x04000D1C RID: 3356
		public string m_CueName;

		// Token: 0x04000D1D RID: 3357
		public int m_CueSheetID;
	}
}
