﻿using System;
using System.Collections.Generic;
using Meige;

namespace Star
{
	// Token: 0x020004DA RID: 1242
	public class ABResourceLoader
	{
		// Token: 0x0600188E RID: 6286 RVA: 0x000802A8 File Offset: 0x0007E6A8
		public ABResourceLoader(int simultaneouslyNum = -1)
		{
			if (simultaneouslyNum > 0)
			{
				this.m_SimultaneouslyNum = simultaneouslyNum;
			}
			this.m_LoadingResources = new Dictionary<string, ABResourceObjectHandler>();
			this.m_LoadedResources = new Dictionary<string, ABResourceObjectHandler>();
			this.m_ReserveResources = new Dictionary<string, ABResourceObjectHandler>();
			this.m_RemoveKeys = new List<string>();
		}

		// Token: 0x0600188F RID: 6287 RVA: 0x00080300 File Offset: 0x0007E700
		public void Update()
		{
			if (this.m_ReserveResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text in this.m_ReserveResources.Keys)
				{
					if (this.m_LoadingResources.Count >= this.m_SimultaneouslyNum)
					{
						break;
					}
					ABResourceObjectHandler abresourceObjectHandler = this.m_ReserveResources[text];
					abresourceObjectHandler.Hndl = MeigeResourceManager.LoadHandler(text, abresourceObjectHandler.Options);
					this.m_LoadingResources.Add(abresourceObjectHandler.Path, abresourceObjectHandler);
					this.m_RemoveKeys.Add(text);
				}
				for (int i = 0; i < this.m_RemoveKeys.Count; i++)
				{
					this.m_ReserveResources.Remove(this.m_RemoveKeys[i]);
				}
			}
			if (this.m_LoadingResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text2 in this.m_LoadingResources.Keys)
				{
					ABResourceObjectHandler abresourceObjectHandler2 = this.m_LoadingResources[text2];
					if (abresourceObjectHandler2.Update())
					{
						this.m_LoadedResources.Add(text2, abresourceObjectHandler2);
						this.m_RemoveKeys.Add(text2);
					}
					else if (abresourceObjectHandler2.IsError())
					{
					}
				}
				for (int j = 0; j < this.m_RemoveKeys.Count; j++)
				{
					this.m_LoadingResources.Remove(this.m_RemoveKeys[j]);
				}
			}
			if (this.m_LoadedResources.Count > 0)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text3 in this.m_LoadedResources.Keys)
				{
					ABResourceObjectHandler abresourceObjectHandler3 = this.m_LoadedResources[text3];
					if (abresourceObjectHandler3.RefCount <= 0)
					{
						if (!this.IsContainsIgnorePathList(abresourceObjectHandler3.Path))
						{
							abresourceObjectHandler3.Destroy();
							this.m_RemoveKeys.Add(text3);
						}
					}
				}
				for (int k = 0; k < this.m_RemoveKeys.Count; k++)
				{
					this.m_LoadedResources.Remove(this.m_RemoveKeys[k]);
				}
			}
			if (this.m_UnloadAllFlg)
			{
				this.m_RemoveKeys.Clear();
				foreach (string text4 in this.m_LoadedResources.Keys)
				{
					if (this.m_IsUnloadIgnoreUnloadPathListOnUnloadAll || !this.IsContainsIgnorePathList(text4))
					{
						this.m_LoadedResources[text4].Destroy();
						this.m_RemoveKeys.Add(text4);
					}
				}
				for (int l = 0; l < this.m_RemoveKeys.Count; l++)
				{
					this.m_LoadedResources.Remove(this.m_RemoveKeys[l]);
				}
				this.m_LoadingResources.Clear();
				this.m_ReserveResources.Clear();
				this.m_UnloadAllFlg = false;
				this.m_IsUnloadIgnoreUnloadPathListOnUnloadAll = false;
			}
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x000806CC File Offset: 0x0007EACC
		public bool IsLoading()
		{
			return this.m_LoadingResources.Count > 0 || this.m_ReserveResources.Count > 0;
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x000806F0 File Offset: 0x0007EAF0
		public bool IsExistLoadedObj()
		{
			return this.m_LoadedResources.Count > 0;
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x00080700 File Offset: 0x0007EB00
		public bool IsError()
		{
			foreach (string key in this.m_LoadingResources.Keys)
			{
				if (this.m_LoadingResources[key].IsError())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001893 RID: 6291 RVA: 0x0008077C File Offset: 0x0007EB7C
		public ABResourceObjectHandler Load(string path, params MeigeResource.Option[] options)
		{
			if (this.m_UnloadAllFlg)
			{
				return null;
			}
			ABResourceObjectHandler abresourceObjectHandler = null;
			if (this.m_LoadedResources.TryGetValue(path, out abresourceObjectHandler))
			{
				abresourceObjectHandler.AddRef();
				return abresourceObjectHandler;
			}
			if (this.m_LoadingResources.TryGetValue(path, out abresourceObjectHandler))
			{
				abresourceObjectHandler.AddRef();
				return abresourceObjectHandler;
			}
			if (this.m_ReserveResources.TryGetValue(path, out abresourceObjectHandler))
			{
				abresourceObjectHandler.AddRef();
				return abresourceObjectHandler;
			}
			abresourceObjectHandler = new ABResourceObjectHandler(null, path, options);
			this.m_ReserveResources.Add(path, abresourceObjectHandler);
			return abresourceObjectHandler;
		}

		// Token: 0x06001894 RID: 6292 RVA: 0x00080804 File Offset: 0x0007EC04
		public void Unload(ABResourceObjectHandler unloadHndl)
		{
			if (unloadHndl == null)
			{
				return;
			}
			ABResourceObjectHandler abresourceObjectHandler = null;
			if (!this.m_LoadedResources.TryGetValue(unloadHndl.Path, out abresourceObjectHandler) && !this.m_LoadingResources.TryGetValue(unloadHndl.Path, out abresourceObjectHandler) && !this.m_ReserveResources.TryGetValue(unloadHndl.Path, out abresourceObjectHandler))
			{
				return;
			}
			if (abresourceObjectHandler == unloadHndl)
			{
				abresourceObjectHandler.RemoveRef();
			}
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x00080878 File Offset: 0x0007EC78
		public void Unload(string path)
		{
			ABResourceObjectHandler abresourceObjectHandler = null;
			if (!this.m_LoadedResources.TryGetValue(path, out abresourceObjectHandler) && !this.m_LoadingResources.TryGetValue(path, out abresourceObjectHandler) && !this.m_ReserveResources.TryGetValue(path, out abresourceObjectHandler))
			{
				return;
			}
			abresourceObjectHandler.RemoveRef();
		}

		// Token: 0x06001896 RID: 6294 RVA: 0x000808C8 File Offset: 0x0007ECC8
		public void UnloadAll(bool isUnloadIgnoreUnloadPathListOnUnloadAll = false)
		{
			this.m_UnloadAllFlg = true;
			this.m_IsUnloadIgnoreUnloadPathListOnUnloadAll = isUnloadIgnoreUnloadPathListOnUnloadAll;
		}

		// Token: 0x06001897 RID: 6295 RVA: 0x000808D8 File Offset: 0x0007ECD8
		public bool IsCompleteUnloadAll()
		{
			return !this.m_UnloadAllFlg;
		}

		// Token: 0x06001898 RID: 6296 RVA: 0x000808E3 File Offset: 0x0007ECE3
		public void AddIgnoreUnloadPathList(string path)
		{
			if (this.m_IgnoreUnloadPathList == null)
			{
				this.m_IgnoreUnloadPathList = new List<string>();
			}
			if (!this.IsContainsIgnorePathList(path))
			{
				this.m_IgnoreUnloadPathList.Add(path);
			}
		}

		// Token: 0x06001899 RID: 6297 RVA: 0x00080913 File Offset: 0x0007ED13
		public void RemoveIgnoreUnloadPathList(string path)
		{
			if (this.m_IgnoreUnloadPathList != null)
			{
				this.m_IgnoreUnloadPathList.Remove(path);
			}
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x00080930 File Offset: 0x0007ED30
		private bool IsContainsIgnorePathList(string path)
		{
			if (this.m_IgnoreUnloadPathList != null)
			{
				for (int i = 0; i < this.m_IgnoreUnloadPathList.Count; i++)
				{
					if (this.m_IgnoreUnloadPathList[i] == path)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x04001F49 RID: 8009
		private Dictionary<string, ABResourceObjectHandler> m_LoadingResources;

		// Token: 0x04001F4A RID: 8010
		private Dictionary<string, ABResourceObjectHandler> m_LoadedResources;

		// Token: 0x04001F4B RID: 8011
		private Dictionary<string, ABResourceObjectHandler> m_ReserveResources;

		// Token: 0x04001F4C RID: 8012
		private int m_SimultaneouslyNum = 32;

		// Token: 0x04001F4D RID: 8013
		private bool m_UnloadAllFlg;

		// Token: 0x04001F4E RID: 8014
		private bool m_IsUnloadIgnoreUnloadPathListOnUnloadAll;

		// Token: 0x04001F4F RID: 8015
		private List<string> m_IgnoreUnloadPathList;

		// Token: 0x04001F50 RID: 8016
		private List<string> m_RemoveKeys;
	}
}
