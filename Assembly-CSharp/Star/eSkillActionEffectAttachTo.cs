﻿using System;

namespace Star
{
	// Token: 0x0200010B RID: 267
	public enum eSkillActionEffectAttachTo
	{
		// Token: 0x040006E0 RID: 1760
		WeaponLeft,
		// Token: 0x040006E1 RID: 1761
		WeaponRight
	}
}
