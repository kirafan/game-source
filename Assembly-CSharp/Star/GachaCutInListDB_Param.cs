﻿using System;

namespace Star
{
	// Token: 0x020001C3 RID: 451
	[Serializable]
	public struct GachaCutInListDB_Param
	{
		// Token: 0x04000ADC RID: 2780
		public int m_TitleType;

		// Token: 0x04000ADD RID: 2781
		public string m_CharaID;

		// Token: 0x04000ADE RID: 2782
		public string m_CutInText;

		// Token: 0x04000ADF RID: 2783
		public string m_CutInVoiceCueName;
	}
}
