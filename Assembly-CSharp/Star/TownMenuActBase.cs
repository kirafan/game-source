﻿using System;

namespace Star
{
	// Token: 0x020006AD RID: 1709
	public class TownMenuActBase : ITownMenuAct
	{
		// Token: 0x0600221E RID: 8734 RVA: 0x000B55A0 File Offset: 0x000B39A0
		public void SetUp(TownBuilder pbuild)
		{
		}

		// Token: 0x0600221F RID: 8735 RVA: 0x000B55A2 File Offset: 0x000B39A2
		public void UpdateMenu(TownObjHandleMenu pmenu)
		{
		}
	}
}
