﻿using System;

namespace Star
{
	// Token: 0x02000163 RID: 355
	[Serializable]
	public struct AssetBundleDownloadListDB_Param
	{
		// Token: 0x0400096B RID: 2411
		public string[] m_Paths;
	}
}
