﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000F4 RID: 244
	public class BattleTreasureBoxHandler : BattleDropObjectHandler
	{
		// Token: 0x060006ED RID: 1773 RVA: 0x0002935C File Offset: 0x0002775C
		public override void Update()
		{
			this.m_Timer.Update(Time.deltaTime);
			this.UpdateVisiblity();
			switch (this.m_Step)
			{
			case BattleTreasureBoxHandler.eStep.Drop_Wait_0:
				if (!this.IsPlayingAnim())
				{
					this.m_Timer.SetTimeAndPlay(0.033f);
					this.m_Step = BattleTreasureBoxHandler.eStep.Drop_Wait_1;
				}
				break;
			case BattleTreasureBoxHandler.eStep.Drop_Wait_1:
				if (this.m_Timer.Sec <= 0f)
				{
					this.m_Step = BattleTreasureBoxHandler.eStep.GoToUI;
				}
				break;
			case BattleTreasureBoxHandler.eStep.GoToUI:
				this.m_Timer.SetTimeAndPlay(0.33f);
				this.m_Step = BattleTreasureBoxHandler.eStep.GoToUI_Wait;
				break;
			case BattleTreasureBoxHandler.eStep.GoToUI_Wait:
			{
				float num = 1f - this.m_Timer.Sec / 0.33f;
				this.m_EffectHndl.CacheTransform.position = Vector3.Lerp(this.m_StartPos, this.m_TargetPos, num * num);
				float num2 = 1f - num * num;
				this.m_EffectHndl.CacheTransform.localScale = new Vector3(num2, num2, 1f);
				if (num >= 1f)
				{
					this.OnComplete.Call(this.m_DropItem);
					this.m_DropItem = null;
					this.Stop();
				}
				break;
			}
			}
		}

		// Token: 0x060006EE RID: 1774 RVA: 0x000294A8 File Offset: 0x000278A8
		public void SetParameters(BattleDropItem dropItem, Vector3 startPos, Vector3 targetPos, Action<BattleDropItem> OnCompleteFunc)
		{
			this.m_DropItem = dropItem;
			this.m_StartPos = startPos;
			this.m_StartPos.y = this.m_StartPos.y + 0.35f;
			this.m_TargetPos = targetPos;
			this.OnComplete = OnCompleteFunc;
		}

		// Token: 0x060006EF RID: 1775 RVA: 0x000294E0 File Offset: 0x000278E0
		public override bool Play(Transform parent, int sortingOrder)
		{
			this.m_EffectHndl = SingletonMonoBehaviour<EffectManager>.Inst.PlayUnManaged("TreasureBox", this.m_StartPos, Quaternion.Euler(0f, 180f, 0f), parent, false, WrapMode.ClampForever, sortingOrder, 1f, true);
			if (this.m_EffectHndl != null)
			{
				for (int i = 0; i < BattleTreasureBoxHandler.TARGET_NAMES.Length; i++)
				{
					MsbObjectHandler msbObjectHandlerByName = this.m_EffectHndl.m_MsbHndl.GetMsbObjectHandlerByName(BattleTreasureBoxHandler.TARGET_NAMES[i]);
					if (msbObjectHandlerByName != null)
					{
						msbObjectHandlerByName.GetWork().m_bVisibility = false;
					}
				}
				this.m_Step = BattleTreasureBoxHandler.eStep.Drop_Wait_0;
				return true;
			}
			return false;
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x00029583 File Offset: 0x00027983
		public override void Stop()
		{
			if (this.m_EffectHndl != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.DestroyToCache(this.m_EffectHndl);
				this.m_EffectHndl = null;
			}
			this.m_Step = BattleTreasureBoxHandler.eStep.None;
		}

		// Token: 0x060006F1 RID: 1777 RVA: 0x000295B9 File Offset: 0x000279B9
		public override bool IsPlaying()
		{
			return this.m_Step >= BattleTreasureBoxHandler.eStep.Drop_Wait_0 && this.m_Step < BattleTreasureBoxHandler.eStep.End;
		}

		// Token: 0x060006F2 RID: 1778 RVA: 0x000295D6 File Offset: 0x000279D6
		private bool IsPlayingAnim()
		{
			return this.m_EffectHndl != null && this.m_EffectHndl.IsPlaying();
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x000295FC File Offset: 0x000279FC
		private void UpdateVisiblity()
		{
			if (this.m_EffectHndl != null && this.m_EffectHndl.m_MsbHndl != null)
			{
				int treasureBoxIndex = this.m_DropItem.GetTreasureBoxIndex();
				for (int i = 0; i < BattleTreasureBoxHandler.TARGET_NAMES.Length; i++)
				{
					MsbObjectHandler msbObjectHandlerByName = this.m_EffectHndl.m_MsbHndl.GetMsbObjectHandlerByName(BattleTreasureBoxHandler.TARGET_NAMES[i]);
					if (msbObjectHandlerByName != null && i != treasureBoxIndex)
					{
						msbObjectHandlerByName.GetWork().m_bVisibility = false;
					}
				}
			}
		}

		// Token: 0x04000603 RID: 1539
		private const string EFFECT_ID = "TreasureBox";

		// Token: 0x04000604 RID: 1540
		private static readonly string[] TARGET_NAMES = new string[]
		{
			"TreasureBox_00",
			"TreasureBox_01",
			"TreasureBox_02"
		};

		// Token: 0x04000605 RID: 1541
		private const float POS_OFFSET_Y = 0.35f;

		// Token: 0x04000606 RID: 1542
		private const float WAIT_SEC_AFTER_DROP = 0.033f;

		// Token: 0x04000607 RID: 1543
		private const float GO_TO_UI_SEC = 0.33f;

		// Token: 0x04000608 RID: 1544
		private BattleTreasureBoxHandler.eStep m_Step = BattleTreasureBoxHandler.eStep.None;

		// Token: 0x04000609 RID: 1545
		private SimpleTimer m_Timer = new SimpleTimer(0f);

		// Token: 0x0400060A RID: 1546
		private BattleDropItem m_DropItem;

		// Token: 0x0400060B RID: 1547
		private Vector3 m_StartPos = Vector3.zero;

		// Token: 0x0400060C RID: 1548
		private Vector3 m_TargetPos = Vector3.zero;

		// Token: 0x0400060D RID: 1549
		private EffectHandler m_EffectHndl;

		// Token: 0x0400060E RID: 1550
		public Action<BattleDropItem> OnComplete;

		// Token: 0x020000F5 RID: 245
		private enum eStep
		{
			// Token: 0x04000610 RID: 1552
			None = -1,
			// Token: 0x04000611 RID: 1553
			Drop_Wait_0,
			// Token: 0x04000612 RID: 1554
			Drop_Wait_1,
			// Token: 0x04000613 RID: 1555
			GoToUI,
			// Token: 0x04000614 RID: 1556
			GoToUI_Wait,
			// Token: 0x04000615 RID: 1557
			End
		}
	}
}
