﻿using System;

namespace Star
{
	// Token: 0x0200029E RID: 670
	public enum eCharacterDataType
	{
		// Token: 0x04001538 RID: 5432
		Error,
		// Token: 0x04001539 RID: 5433
		UserCharacterData,
		// Token: 0x0400153A RID: 5434
		UserSupportData,
		// Token: 0x0400153B RID: 5435
		OutputCharaParam,
		// Token: 0x0400153C RID: 5436
		AfterUpgradeParam,
		// Token: 0x0400153D RID: 5437
		AfterLimitBreakParam,
		// Token: 0x0400153E RID: 5438
		AfterEvolutionParam,
		// Token: 0x0400153F RID: 5439
		Empty,
		// Token: 0x04001540 RID: 5440
		Lock,
		// Token: 0x04001541 RID: 5441
		GachaDetailMessage
	}
}
