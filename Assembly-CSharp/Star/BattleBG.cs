﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200009C RID: 156
	public class BattleBG : MonoBehaviour
	{
		// Token: 0x0600045C RID: 1116 RVA: 0x00015D1C File Offset: 0x0001411C
		private void Awake()
		{
			if (this.m_MsbHndl != null && this.m_MeigeAnimCtrls != null && this.m_MeigeAnimClipHolders != null)
			{
				for (int i = 0; i < this.m_MeigeAnimCtrls.Count; i++)
				{
					if (this.m_MeigeAnimCtrls[i] != null && this.m_MeigeAnimClipHolders[i] != null)
					{
						this.m_MeigeAnimCtrls[i].Open();
						this.m_MeigeAnimCtrls[i].AddClip(this.m_MsbHndl, this.m_MeigeAnimClipHolders[i]);
						this.m_MeigeAnimCtrls[i].Close();
					}
				}
			}
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x00015DE0 File Offset: 0x000141E0
		private void LateUpdate()
		{
			this.UpdateColor();
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x00015DE8 File Offset: 0x000141E8
		public void Play()
		{
			int sortingOrder = -32768;
			if (this.m_MsbHndl != null)
			{
				Renderer[] rendererCache = this.m_MsbHndl.GetRendererCache();
				for (int i = 0; i < rendererCache.Length; i++)
				{
					rendererCache[i].sortingOrder = sortingOrder;
				}
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int j = 0; j < particleEmitterNum; j++)
				{
					MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(j);
					if (particleEmitter != null)
					{
						particleEmitter.SetSortingOrder(sortingOrder);
					}
				}
			}
			if (this.m_MeigeAnimCtrls != null)
			{
				for (int k = 0; k < this.m_MeigeAnimCtrls.Count; k++)
				{
					if (this.m_MeigeAnimCtrls[k] != null)
					{
						this.m_MeigeAnimCtrls[k].Play(string.Format("anim_{0}", k), 0f, WrapMode.Loop);
						this.m_Anim[string.Format("BattleBG_{0}@anim_{1}", this.m_ID, k)].layer = k + 1;
						this.m_Anim.SyncLayer(k + 1);
					}
				}
			}
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x00015F28 File Offset: 0x00014328
		public bool IsPlayingAnim()
		{
			if (this.m_MeigeAnimCtrls != null)
			{
				for (int i = 0; i < this.m_MeigeAnimCtrls.Count; i++)
				{
					if (this.m_MeigeAnimCtrls[i] != null && this.m_MeigeAnimCtrls[i].GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x00015F8C File Offset: 0x0001438C
		public void SetEnableRender(bool flg)
		{
			if (flg)
			{
				this.EnableRender();
			}
			else
			{
				this.DisableRender();
			}
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00015FA5 File Offset: 0x000143A5
		public void EnableRender()
		{
			base.gameObject.SetActive(true);
			this.Play();
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00015FBC File Offset: 0x000143BC
		public void DisableRender()
		{
			if (this.m_MsbHndl != null)
			{
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int i = 0; i < particleEmitterNum; i++)
				{
					MeigeParticleEmitter particleEmitter = this.m_MsbHndl.GetParticleEmitter(i);
					if (particleEmitter != null)
					{
						particleEmitter.Kill();
					}
				}
			}
			if (this.m_MeigeAnimCtrls != null)
			{
				for (int j = 0; j < this.m_MeigeAnimCtrls.Count; j++)
				{
					if (this.m_MeigeAnimCtrls[j] != null)
					{
						this.m_MeigeAnimCtrls[j].Pause();
					}
				}
			}
			base.gameObject.SetActive(false);
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x00016074 File Offset: 0x00014474
		public void ChangeColor(Color startColor, Color endColor, float sec = 0.3f)
		{
			this.m_StartColor = startColor;
			this.m_EndColor = endColor;
			this.m_ColorTime = sec;
			this.m_ColorTimer = 0f;
			Color color = this.m_StartColor;
			if (sec <= 0f)
			{
				color = this.m_EndColor;
			}
			this.SetColor(color);
			if (this.m_ColorTime <= 0f)
			{
				this.m_IsUpdateColor = false;
			}
			else
			{
				this.m_IsUpdateColor = true;
			}
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x000160E4 File Offset: 0x000144E4
		public void ChangeColorFromCurrent(Color endColor, float sec = 0.3f)
		{
			this.ChangeColor(this.m_CurrentColor, endColor, sec);
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x000160F4 File Offset: 0x000144F4
		public void ResetColor()
		{
			this.m_IsUpdateColor = false;
			this.SetColor(Color.white);
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00016108 File Offset: 0x00014508
		private void UpdateColor()
		{
			if (!this.m_IsUpdateColor)
			{
				return;
			}
			this.m_ColorTimer += Time.deltaTime;
			float num = this.m_ColorTimer / this.m_ColorTime;
			if (num > 1f)
			{
				num = 1f;
			}
			Color color = Color.Lerp(this.m_StartColor, this.m_EndColor, num);
			this.SetColor(color);
			if (num >= 1f)
			{
				this.m_IsUpdateColor = false;
			}
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x00016180 File Offset: 0x00014580
		public void SetColor(Color color)
		{
			this.m_CurrentColor = color;
			if (this.m_MsbHndl != null)
			{
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				for (int i = 0; i < msbObjectHandlerNum; i++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
					MsbObjectHandler.MsbObjectParam work = msbObjectHandler.GetWork();
					if (work != null)
					{
						work.m_MeshColor = new Color(this.m_CurrentColor.r, this.m_CurrentColor.g, this.m_CurrentColor.b, work.m_MeshColor.a);
					}
				}
				int num = (this.m_MsbHndl.m_MsbParticleEmitterArray == null) ? 0 : this.m_MsbHndl.m_MsbParticleEmitterArray.Length;
				for (int j = 0; j < num; j++)
				{
					MsbHandler.MsbParticleEmitter msbParticleEmitter = this.m_MsbHndl.m_MsbParticleEmitterArray[j];
					if (msbParticleEmitter != null && msbParticleEmitter.m_MsbMaterialHandler != null)
					{
						MsbMaterialHandler.MsbMaterialParam work2 = msbParticleEmitter.m_MsbMaterialHandler.GetWork();
						if (work2 != null)
						{
							work2.m_Diffuse = color;
						}
					}
				}
			}
		}

		// Token: 0x040002C2 RID: 706
		private const string PLAY_KEY = "anim_{0}";

		// Token: 0x040002C3 RID: 707
		private const string PLAY_KEY_FOR_ANIM = "BattleBG_{0}@anim_{1}";

		// Token: 0x040002C4 RID: 708
		private const float CHANGE_COLOR_DEFAULT_TIME = 0.3f;

		// Token: 0x040002C5 RID: 709
		public int m_ID;

		// Token: 0x040002C6 RID: 710
		public Transform m_Transform;

		// Token: 0x040002C7 RID: 711
		public Animation m_Anim;

		// Token: 0x040002C8 RID: 712
		public List<MeigeAnimCtrl> m_MeigeAnimCtrls;

		// Token: 0x040002C9 RID: 713
		public List<MeigeAnimClipHolder> m_MeigeAnimClipHolders;

		// Token: 0x040002CA RID: 714
		public MsbHandler m_MsbHndl;

		// Token: 0x040002CB RID: 715
		private Color m_StartColor;

		// Token: 0x040002CC RID: 716
		private Color m_EndColor;

		// Token: 0x040002CD RID: 717
		private Color m_CurrentColor = Color.white;

		// Token: 0x040002CE RID: 718
		private float m_ColorTime;

		// Token: 0x040002CF RID: 719
		private float m_ColorTimer;

		// Token: 0x040002D0 RID: 720
		private bool m_IsUpdateColor;
	}
}
