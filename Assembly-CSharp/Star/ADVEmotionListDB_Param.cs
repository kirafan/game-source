﻿using System;

namespace Star
{
	// Token: 0x02000019 RID: 25
	[Serializable]
	public struct ADVEmotionListDB_Param
	{
		// Token: 0x040000AA RID: 170
		public string m_EmotionID;

		// Token: 0x040000AB RID: 171
		public string m_EffectID;

		// Token: 0x040000AC RID: 172
		public int m_DefaultPosition;

		// Token: 0x040000AD RID: 173
		public sbyte m_Loop;
	}
}
