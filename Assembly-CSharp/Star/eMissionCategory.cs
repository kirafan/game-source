﻿using System;

namespace Star
{
	// Token: 0x020004FF RID: 1279
	public enum eMissionCategory
	{
		// Token: 0x04001FDF RID: 8159
		None = -1,
		// Token: 0x04001FE0 RID: 8160
		Day,
		// Token: 0x04001FE1 RID: 8161
		Week,
		// Token: 0x04001FE2 RID: 8162
		Unlock,
		// Token: 0x04001FE3 RID: 8163
		Event,
		// Token: 0x04001FE4 RID: 8164
		Max
	}
}
