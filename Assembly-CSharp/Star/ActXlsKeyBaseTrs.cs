﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000570 RID: 1392
	public class ActXlsKeyBaseTrs : ActXlsKeyBase
	{
		// Token: 0x06001B1D RID: 6941 RVA: 0x0008F848 File Offset: 0x0008DC48
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_TargetName);
			pio.Float(ref this.m_Pos.x);
			pio.Float(ref this.m_Pos.y);
			pio.Float(ref this.m_Pos.z);
			pio.Float(ref this.m_Rot);
		}

		// Token: 0x0400220B RID: 8715
		public string m_TargetName;

		// Token: 0x0400220C RID: 8716
		public Vector3 m_Pos;

		// Token: 0x0400220D RID: 8717
		public float m_Rot;
	}
}
