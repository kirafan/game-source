﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020003AC RID: 940
	public class UserTownData
	{
		// Token: 0x060011ED RID: 4589 RVA: 0x0005EDC6 File Offset: 0x0005D1C6
		public UserTownData()
		{
			this.MngID = -1L;
			this.DebugMngID = -1L;
		}

		// Token: 0x060011EE RID: 4590 RVA: 0x0005EDF4 File Offset: 0x0005D1F4
		public List<UserTownData.BuildObjectUp> CreateBuildTable()
		{
			List<UserTownData.BuildObjectUp> list = new List<UserTownData.BuildObjectUp>();
			int count = this.m_BuildObjectDatas.Count;
			for (int i = 0; i < count; i++)
			{
				UserTownData.BuildObjectUp buildObjectUp = new UserTownData.BuildObjectUp();
				buildObjectUp.CopyData(this.m_BuildObjectDatas[i]);
				list.Add(buildObjectUp);
			}
			return list;
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060011EF RID: 4591 RVA: 0x0005EE45 File Offset: 0x0005D245
		// (set) Token: 0x060011F0 RID: 4592 RVA: 0x0005EE4D File Offset: 0x0005D24D
		public long MngID { get; set; }

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060011F1 RID: 4593 RVA: 0x0005EE56 File Offset: 0x0005D256
		// (set) Token: 0x060011F2 RID: 4594 RVA: 0x0005EE5E File Offset: 0x0005D25E
		public long DataUp { get; set; }

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060011F3 RID: 4595 RVA: 0x0005EE67 File Offset: 0x0005D267
		// (set) Token: 0x060011F4 RID: 4596 RVA: 0x0005EE6F File Offset: 0x0005D26F
		public bool NewBuild { get; set; }

		// Token: 0x060011F5 RID: 4597 RVA: 0x0005EE78 File Offset: 0x0005D278
		public bool IsSetUpConnect()
		{
			return this.MngID != -1L;
		}

		// Token: 0x060011F6 RID: 4598 RVA: 0x0005EE87 File Offset: 0x0005D287
		public void UpManageID(long fmanageid)
		{
			this.MngID = fmanageid;
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x0005EE90 File Offset: 0x0005D290
		public UserTownData GetManageIDToData(long fmanageid)
		{
			if (this.MngID == fmanageid)
			{
				return this;
			}
			return null;
		}

		// Token: 0x060011F8 RID: 4600 RVA: 0x0005EEA4 File Offset: 0x0005D2A4
		public void ClearData(long fmanageid, long ftimes)
		{
			if (this.DataUp != ftimes || fmanageid != this.MngID || fmanageid == -1L)
			{
				int count = this.m_BuildObjectDatas.Count;
				for (int i = count - 1; i >= 0; i--)
				{
					this.m_BuildObjectDatas.RemoveAt(i);
				}
			}
			this.MngID = fmanageid;
			this.DataUp = ftimes;
		}

		// Token: 0x060011F9 RID: 4601 RVA: 0x0005EF0B File Offset: 0x0005D30B
		public void AddBuildObjectData(UserTownData.BuildObjectData buildObject)
		{
			this.m_BuildObjectDatas.Add(buildObject);
			this.NewBuild = true;
		}

		// Token: 0x060011FA RID: 4602 RVA: 0x0005EF20 File Offset: 0x0005D320
		public void RemoveBuildObjectDataAtBuildPointIndex(int buildPointIndex)
		{
			for (int i = 0; i < this.m_BuildObjectDatas.Count; i++)
			{
				if (this.m_BuildObjectDatas[i].m_BuildPointIndex == buildPointIndex)
				{
					this.m_BuildObjectDatas[i].ChangeBuildPoint(-1, false);
					this.m_BuildObjectDatas.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x060011FB RID: 4603 RVA: 0x0005EF84 File Offset: 0x0005D384
		public int GetBuildObjectDataNum()
		{
			return this.m_BuildObjectDatas.Count;
		}

		// Token: 0x060011FC RID: 4604 RVA: 0x0005EF91 File Offset: 0x0005D391
		public UserTownData.BuildObjectData GetBuildObjectDataAt(int index)
		{
			return this.m_BuildObjectDatas[index];
		}

		// Token: 0x060011FD RID: 4605 RVA: 0x0005EFA0 File Offset: 0x0005D3A0
		public UserTownData.BuildObjectData GetBuildObjectDataAtBuildPointIndex(int buildPointIndex)
		{
			for (int i = 0; i < this.m_BuildObjectDatas.Count; i++)
			{
				if (this.m_BuildObjectDatas[i].m_BuildPointIndex == buildPointIndex)
				{
					return this.m_BuildObjectDatas[i];
				}
			}
			return null;
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x0005EFF0 File Offset: 0x0005D3F0
		public UserTownData.BuildObjectData GetBuildObjectDataAtBuildMngID(long buildmngid)
		{
			for (int i = 0; i < this.m_BuildObjectDatas.Count; i++)
			{
				if (this.m_BuildObjectDatas[i].m_ManageID == buildmngid)
				{
					return this.m_BuildObjectDatas[i];
				}
			}
			return null;
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x0005F040 File Offset: 0x0005D440
		public int GetBuildNumByCategory(eTownObjectCategory category)
		{
			int num = 0;
			for (int i = 0; i < this.GetBuildObjectDataNum(); i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = this.GetBuildObjectDataAt(i);
				int num2 = buildObjectDataAt.m_ObjID;
				num2 = TownUtility.CalcAreaToContentID(num2);
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num2).m_Category == (int)category)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x0005F0A8 File Offset: 0x0005D4A8
		public void AddStoreDataNum(int objID, int addNum)
		{
			if (this.m_StoreDatas.ContainsKey(objID))
			{
				Dictionary<int, int> storeDatas;
				(storeDatas = this.m_StoreDatas)[objID] = storeDatas[objID] + addNum;
				if (this.m_StoreDatas[objID] <= 0)
				{
					this.m_StoreDatas.Remove(objID);
				}
			}
			else if (addNum > 0)
			{
				this.m_StoreDatas.Add(objID, addNum);
			}
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x0005F117 File Offset: 0x0005D517
		public int GetStoreDataNum(int objID)
		{
			if (this.m_StoreDatas.ContainsKey(objID))
			{
				return this.m_StoreDatas[objID];
			}
			return 0;
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x0005F138 File Offset: 0x0005D538
		public Dictionary<int, int> GetStoreDatas()
		{
			return this.m_StoreDatas;
		}

		// Token: 0x0400186D RID: 6253
		public int Level;

		// Token: 0x0400186E RID: 6254
		public long DebugMngID;

		// Token: 0x0400186F RID: 6255
		public const int Version = 1;

		// Token: 0x04001873 RID: 6259
		private List<UserTownData.BuildObjectData> m_BuildObjectDatas = new List<UserTownData.BuildObjectData>();

		// Token: 0x04001874 RID: 6260
		private Dictionary<int, int> m_StoreDatas = new Dictionary<int, int>();

		// Token: 0x020003AD RID: 941
		public class BuildObjectUp
		{
			// Token: 0x06001204 RID: 4612 RVA: 0x0005F148 File Offset: 0x0005D548
			public void CopyData(UserTownData.BuildObjectData pbase)
			{
				this.m_ManageID = pbase.m_ManageID;
				this.m_BuildPointIndex = pbase.m_BuildPointIndex;
				this.m_ObjID = pbase.m_ObjID;
				this.m_Lv = pbase.m_Lv;
				this.m_IsOpen = pbase.m_IsOpen;
				this.m_ActionTime = pbase.m_ActionTime;
				this.m_BuildingMs = pbase.m_BuildingMs;
			}

			// Token: 0x04001875 RID: 6261
			public long m_ManageID;

			// Token: 0x04001876 RID: 6262
			public int m_BuildPointIndex;

			// Token: 0x04001877 RID: 6263
			public int m_ObjID;

			// Token: 0x04001878 RID: 6264
			public int m_Lv;

			// Token: 0x04001879 RID: 6265
			public bool m_IsOpen;

			// Token: 0x0400187A RID: 6266
			public long m_BuildingMs;

			// Token: 0x0400187B RID: 6267
			public long m_ActionTime;
		}

		// Token: 0x020003AE RID: 942
		public class BuildObjectData
		{
			// Token: 0x06001205 RID: 4613 RVA: 0x0005F1AC File Offset: 0x0005D5AC
			public BuildObjectData(int buildPointIndex, int objID, bool isOpen, long buildms, long MngID, long actms)
			{
				this.m_BuildPointIndex = buildPointIndex;
				this.m_ObjID = objID;
				this.m_IsOpen = isOpen;
				this.m_BuildingMs = buildms;
				this.m_ManageID = MngID;
				this.m_ActionTime = 0L;
				this.m_Lv = 0;
				this.m_ActionTime = actms;
				if (this.m_BuildingMs > this.m_ActionTime)
				{
					this.m_ActionTime = this.m_BuildingMs;
				}
			}

			// Token: 0x06001206 RID: 4614 RVA: 0x0005F218 File Offset: 0x0005D618
			public void ChangeStoreData(UserTownObjectData pstoreobj, long fmarktime)
			{
				this.m_Lv = pstoreobj.Lv;
				this.m_IsOpen = true;
				this.m_ManageID = pstoreobj.m_ManageID;
				this.m_ObjID = pstoreobj.ObjID;
				this.m_ActionTime = fmarktime;
			}

			// Token: 0x06001207 RID: 4615 RVA: 0x0005F24C File Offset: 0x0005D64C
			public void SetStoreState()
			{
				if (this.m_Object != null)
				{
					this.m_Object.OpenState = UserTownObjectData.eOpenState.Non;
				}
			}

			// Token: 0x06001208 RID: 4616 RVA: 0x0005F265 File Offset: 0x0005D665
			public bool IsNextLevelUpObj()
			{
				return this.m_Object == null || this.m_Object.OpenState == UserTownObjectData.eOpenState.OnWait || this.m_Object.OpenState == UserTownObjectData.eOpenState.Wait;
			}

			// Token: 0x06001209 RID: 4617 RVA: 0x0005F296 File Offset: 0x0005D696
			public void ChangeBuildPoint(int fnextpoint, bool fchecks = false)
			{
				this.m_BuildPointIndex = fnextpoint;
				if (this.m_Object != null)
				{
					this.m_Object.BuildPoint = fnextpoint;
					this.m_Object.m_ChangeState = fchecks;
				}
			}

			// Token: 0x0600120A RID: 4618 RVA: 0x0005F2C2 File Offset: 0x0005D6C2
			public bool IsCheckStateChenge()
			{
				return this.m_Object != null && this.m_Object.m_ChangeState;
			}

			// Token: 0x0600120B RID: 4619 RVA: 0x0005F2DC File Offset: 0x0005D6DC
			public void ClearChangeState()
			{
				if (this.m_Object != null)
				{
					this.m_Object.m_ChangeState = false;
				}
			}

			// Token: 0x0600120C RID: 4620 RVA: 0x0005F2F5 File Offset: 0x0005D6F5
			public void SetActionTime(long fsettime)
			{
				this.m_ActionTime = fsettime;
				if (this.m_Object != null)
				{
					this.m_Object.ActionTime = fsettime;
				}
			}

			// Token: 0x0600120D RID: 4621 RVA: 0x0005F318 File Offset: 0x0005D718
			public bool isLimitLevelUp()
			{
				return this.m_Lv >= (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID).m_MaxLevel;
			}

			// Token: 0x0600120E RID: 4622 RVA: 0x0005F354 File Offset: 0x0005D754
			public bool AddLevel(int addLevel)
			{
				int num = this.m_Lv + addLevel;
				if (num > (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID).m_MaxLevel)
				{
					return false;
				}
				this.m_Lv = num;
				if (this.m_Object != null)
				{
					this.m_Object.Lv = num;
				}
				return true;
			}

			// Token: 0x0600120F RID: 4623 RVA: 0x0005F3B4 File Offset: 0x0005D7B4
			public int AddChkLevel(int addLevel)
			{
				if (this.m_Object != null)
				{
					return this.m_Object.AddChkLevel(addLevel);
				}
				int num = this.m_Lv + addLevel;
				if (num > (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID).m_MaxLevel)
				{
					return this.m_Lv;
				}
				return num;
			}

			// Token: 0x06001210 RID: 4624 RVA: 0x0005F412 File Offset: 0x0005D812
			public void ChangeLevelUp(long fmarktime, bool fopen)
			{
				this.m_IsOpen = fopen;
				this.m_BuildingMs = fmarktime;
				if (this.m_IsOpen && this.m_BuildingMs > this.m_ActionTime)
				{
					this.m_ActionTime = this.m_BuildingMs;
				}
			}

			// Token: 0x0400187C RID: 6268
			public long m_ManageID;

			// Token: 0x0400187D RID: 6269
			public int m_BuildPointIndex;

			// Token: 0x0400187E RID: 6270
			public int m_ObjID;

			// Token: 0x0400187F RID: 6271
			public int m_Lv;

			// Token: 0x04001880 RID: 6272
			public bool m_IsOpen;

			// Token: 0x04001881 RID: 6273
			public long m_BuildingMs;

			// Token: 0x04001882 RID: 6274
			public long m_ActionTime;

			// Token: 0x04001883 RID: 6275
			public UserTownObjectData m_Object;
		}
	}
}
