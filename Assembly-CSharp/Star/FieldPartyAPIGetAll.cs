﻿using System;
using FieldPartyMemberResponseTypes;

namespace Star
{
	// Token: 0x0200034B RID: 843
	public class FieldPartyAPIGetAll : INetComHandle
	{
		// Token: 0x0600101C RID: 4124 RVA: 0x00055C83 File Offset: 0x00054083
		public FieldPartyAPIGetAll()
		{
			this.ApiName = "player/field_party/member/get_all";
			this.Request = false;
			this.ResponseType = typeof(GetAll);
		}
	}
}
