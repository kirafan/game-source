﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200051B RID: 1307
	public class MovieObjectPrepareMethodArgument
	{
		// Token: 0x060019DE RID: 6622 RVA: 0x00086698 File Offset: 0x00084A98
		public MovieObjectPrepareMethodArgument(string moviePath, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic target = null, Material material = null, Canvas canvas = null, CanvasScaler scaler = null, bool imageNowCreated = false)
		{
			this.m_MoviePath = moviePath;
			this.m_Volume = volume;
			this.m_Loop = loop;
			this.m_AlphaMovie = alphaMovie;
			this.m_Target = target;
			this.m_Material = material;
			this.m_ImageNowCreated = imageNowCreated;
			this.m_Canvas = canvas;
			this.m_Scaler = scaler;
		}

		// Token: 0x0400207C RID: 8316
		public Graphic m_Target;

		// Token: 0x0400207D RID: 8317
		public string m_MoviePath;

		// Token: 0x0400207E RID: 8318
		public MovieVolumeInformation m_Volume;

		// Token: 0x0400207F RID: 8319
		public bool m_Loop;

		// Token: 0x04002080 RID: 8320
		public bool m_AlphaMovie;

		// Token: 0x04002081 RID: 8321
		public Material m_Material;

		// Token: 0x04002082 RID: 8322
		public bool m_ImageNowCreated;

		// Token: 0x04002083 RID: 8323
		public Canvas m_Canvas;

		// Token: 0x04002084 RID: 8324
		public CanvasScaler m_Scaler;
	}
}
