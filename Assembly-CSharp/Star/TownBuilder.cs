﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000705 RID: 1797
	public class TownBuilder : MonoBehaviour
	{
		// Token: 0x06002384 RID: 9092 RVA: 0x000BE889 File Offset: 0x000BCC89
		public void SetOwner(TownMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06002385 RID: 9093 RVA: 0x000BE894 File Offset: 0x000BCC94
		public int GetSceneLevel()
		{
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			int num = questMng.GetLastChapterID();
			if (questMng.IsClearChapterDifficulty(num, QuestDefine.eDifficulty.Normal))
			{
				num++;
			}
			num = Mathf.Max(0, num - 1);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level > num)
			{
				num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level;
			}
			return num;
		}

		// Token: 0x06002386 RID: 9094 RVA: 0x000BE8FE File Offset: 0x000BCCFE
		public void SetFieldMap(ITownObjectHandler phandle)
		{
			this.m_ActPakage.m_TownField = phandle;
		}

		// Token: 0x06002387 RID: 9095 RVA: 0x000BE90C File Offset: 0x000BCD0C
		public TownBuildTree GetBuildTree()
		{
			return this.m_ActPakage.m_BuildTrees;
		}

		// Token: 0x06002388 RID: 9096 RVA: 0x000BE91C File Offset: 0x000BCD1C
		public BindTownCharaMap GetCharaBind(long fmngid, bool fcreate = true)
		{
			BindTownCharaMap bindTownCharaMap = null;
			for (int i = 0; i < this.m_ActPakage.m_CharaBuildBind.Count; i++)
			{
				if (this.m_ActPakage.m_CharaBuildBind[i].m_ManageID == fmngid)
				{
					bindTownCharaMap = this.m_ActPakage.m_CharaBuildBind[i];
					break;
				}
			}
			if (bindTownCharaMap == null && fcreate)
			{
				bindTownCharaMap = new BindTownCharaMap(fmngid, this);
				this.m_ActPakage.m_CharaBuildBind.Add(bindTownCharaMap);
			}
			return bindTownCharaMap;
		}

		// Token: 0x06002389 RID: 9097 RVA: 0x000BE9A5 File Offset: 0x000BCDA5
		public GameObject GetMakerGauge()
		{
			return this.m_MarkerCache.GetNextObjectInCache();
		}

		// Token: 0x0600238A RID: 9098 RVA: 0x000BE9B2 File Offset: 0x000BCDB2
		public void ReleaseMakerGauge(GameObject pobj)
		{
			this.m_MarkerCache.FreeObjectInCache(pobj);
		}

		// Token: 0x0600238B RID: 9099 RVA: 0x000BE9C0 File Offset: 0x000BCDC0
		public GameObject GetGeneralObj(eTownObjectType fobjid)
		{
			return this.m_CharaCache.GetNextObjectInCache((int)fobjid);
		}

		// Token: 0x0600238C RID: 9100 RVA: 0x000BE9CE File Offset: 0x000BCDCE
		public void ReleaseCharaMarker(GameObject pobj)
		{
			this.m_CharaCache.FreeObjectInCache(pobj);
		}

		// Token: 0x0600238D RID: 9101 RVA: 0x000BE9DC File Offset: 0x000BCDDC
		public GameObject GetCharaHudObj()
		{
			return this.m_HudMsgCtrl.GetHudObject(this.m_CharaCache, 7);
		}

		// Token: 0x0600238E RID: 9102 RVA: 0x000BE9F0 File Offset: 0x000BCDF0
		public void ReleaseCharaHudObj(GameObject pobj)
		{
			this.m_HudMsgCtrl.ReleaseHudObject(this.m_CharaCache, pobj);
		}

		// Token: 0x0600238F RID: 9103 RVA: 0x000BEA04 File Offset: 0x000BCE04
		public GameObject GetMenuIcon(eTownMenuType ftype)
		{
			return this.m_TraningIcon;
		}

		// Token: 0x06002390 RID: 9104 RVA: 0x000BEA0C File Offset: 0x000BCE0C
		private void Awake()
		{
			this.m_Camera = Camera.main;
			this.m_SyncResult = new List<TownBuilder.SyncResult>();
			this.m_HudMsgCtrl = new TownHudMessageControll();
			ObjectResourceManager.SetUpManager(base.gameObject);
		}

		// Token: 0x06002391 RID: 9105 RVA: 0x000BEA3C File Offset: 0x000BCE3C
		private void Update()
		{
			this.m_DebugTimes += Time.deltaTime;
			this.m_Count++;
			switch (this.m_Mode)
			{
			case TownBuilder.eMode.Prepare_First:
				if (TownNetComManager.IsSetUp())
				{
					this.m_Log.Logs(string.Format("TownSetTime Start = {0} : {1}\n", this.m_DebugTimes, this.m_Count));
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Town);
					FieldMapManager.CheckWakeUpObject(base.gameObject);
					this.m_EffectMng = TownEffectManager.CreateEffectManager(base.gameObject);
					this.m_EffectMng.SetEffectList("Prefab/Town/Effect/EffectDatabase");
					TownBuildPointUtil.DatabaseSetUp();
					this.m_EventQue = new TownEventQue(60);
					this.m_Mode++;
					this.m_TimeCategory = 0;
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopChara, true);
					this.m_TimeCheckHour = ScheduleTimeUtil.GetManageUnixTime().Hour;
					if (this.m_TimeCheckHour >= 18 || this.m_TimeCheckHour < 6)
					{
						this.m_TimeCategory = 1;
					}
					this.m_ActPakage = new TownBuilder.ActiveBuildPakage();
				}
				break;
			case TownBuilder.eMode.Prepare_FieldObject:
				if (FieldMapManager.IsSetUp() && !SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets() && TownBuildPointUtil.IsSetUp())
				{
					this.m_Log.Logs(string.Format("TownSetTime FieldMapUp = {0} : {1}\n", this.m_DebugTimes, this.m_Count));
					FieldMapManager.EntryBuildCallback(new BuildUpCallback(this.CallbackBuildUp), new BuildUpCallback(this.CallbackChangePoint));
					this.m_ActPakage.m_BuildTrees = new TownBuildTree(this);
					TownBuildUpFieldParam prequest = new TownBuildUpFieldParam(0, this.m_TimeCategory, this);
					this.BuildRequest(prequest);
					UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
					int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
					for (int i = 0; i < buildObjectDataNum; i++)
					{
						UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
						if (buildObjectDataAt != null && this.IsDebugCodeCheck(buildObjectDataAt))
						{
							TownBuildUpBufParam prequest2 = new TownBuildUpBufParam(buildObjectDataAt.m_BuildPointIndex, buildObjectDataAt.m_ManageID, buildObjectDataAt.m_ObjID, true, null);
							this.BuildRequest(prequest2);
						}
					}
					this.m_Mode = TownBuilder.eMode.Prepare_BuildObjects_Wait;
				}
				break;
			case TownBuilder.eMode.Prepare_BuildObjects_Wait:
				if (this.m_EventQue.GetComCommandNum() <= 0)
				{
					this.m_Log.Logs(string.Format("TownSetTime TownObjUp = {0} : {1}\n", this.m_DebugTimes, this.m_Count));
					if (this.m_CharaManager == null)
					{
						this.m_CharaManager = new TownCharaManager(this);
						UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
						int roomInCharaNum = userDataMng.UserFieldChara.GetRoomInCharaNum();
						for (int j = 0; j < roomInCharaNum; j++)
						{
							this.m_CharaManager.AddCharacter(userDataMng.UserFieldChara.GetRoomInCharaAt(j).ManageID, true, false);
						}
					}
					this.m_Mode = TownBuilder.eMode.Prepare_CharaObject_Wait;
				}
				break;
			case TownBuilder.eMode.Prepare_CharaObject_Wait:
				this.m_Log.Logs(string.Format("TownSetTime TownCharaUp = {0} : {1}\n", this.m_DebugTimes, this.m_Count));
				Application.targetFrameRate = 30;
				this.m_Mode = TownBuilder.eMode.Main;
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopTown, true);
				this.m_Log.DebugOut();
				break;
			case TownBuilder.eMode.Main:
				this.UpdateMain();
				if (this.m_Owner != null)
				{
					bool flag = this.m_Owner.IsIdleStep() && this.m_Owner.TownUI != null;
					bool flag2 = this.m_Owner.IsHomeStep() && this.m_Owner.HomeUI != null;
					if (flag && this.m_Owner.TownUI.CheckRaycast())
					{
						this.CheckTimeBuilding();
					}
					else if (flag2 && this.m_Owner.HomeUI.CheckRaycast())
					{
						this.CheckTimeBuilding();
					}
				}
				break;
			case TownBuilder.eMode.RebuildMode:
				this.ModelDestory();
				this.m_Mode = TownBuilder.eMode.Prepare_FieldObject;
				Application.targetFrameRate = 60;
				break;
			case TownBuilder.eMode.FreeBufSelect:
			case TownBuilder.eMode.BufSelect:
				this.UpdateBufSelect();
				break;
			case TownBuilder.eMode.FreeAreaSelect:
			case TownBuilder.eMode.AreaSelect:
				this.UpdateAreaSelect();
				break;
			}
			this.FlushEventRequest();
		}

		// Token: 0x06002392 RID: 9106 RVA: 0x000BEE94 File Offset: 0x000BD294
		public void Initialize()
		{
			this.m_CharaCache = new TownResourceObjectData();
			this.m_CharaCache.Initialize("Prefab/Town/Object/TownObjectDatabase");
			this.m_MarkerCache.Initialize(base.transform);
		}

		// Token: 0x06002393 RID: 9107 RVA: 0x000BEEC2 File Offset: 0x000BD2C2
		public void OnDestroy()
		{
			ObjectResourceManager.ReleaseCheck();
		}

		// Token: 0x06002394 RID: 9108 RVA: 0x000BEECC File Offset: 0x000BD2CC
		private void ModelDestory()
		{
			if (this.m_ActPakage.m_BuildTrees != null)
			{
				this.m_ActPakage.m_BuildTrees.Destroy();
				this.m_ActPakage.m_BuildTrees = null;
			}
			if (this.m_ActPakage.m_TownField != null)
			{
				this.m_ActPakage.m_TownField.Destroy();
				ObjectResourceManager.ObjectDestroy(this.m_ActPakage.m_TownField.gameObject, true);
				this.m_ActPakage.m_TownField = null;
			}
		}

		// Token: 0x06002395 RID: 9109 RVA: 0x000BEF50 File Offset: 0x000BD350
		public void Destroy()
		{
			Application.targetFrameRate = 30;
			MissionManager.SetPopUpActive(eXlsPopupTiming.PopTown, false);
			this.ModelDestory();
			if (this.m_CharaManager != null)
			{
				this.m_CharaManager.Release();
				this.m_CharaManager = null;
			}
			FieldMapManager.StoreRelease();
			TownBuildPointUtil.DatabaseRelease();
			TownEffectManager.ReleaseEffectManager(this.m_EffectMng);
			this.m_Mode = TownBuilder.eMode.None;
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Town);
			if (this.m_CharaCache != null)
			{
				this.m_CharaCache.Destroy();
			}
			ObjectResourceManager.ReleaseCheck();
		}

		// Token: 0x06002396 RID: 9110 RVA: 0x000BEFD8 File Offset: 0x000BD3D8
		public bool Prepare()
		{
			if (this.m_Mode != TownBuilder.eMode.None)
			{
				return false;
			}
			this.m_Log.Logs(string.Format("TownSetTime Active = {0} : {1}\n", this.m_DebugTimes, this.m_Count));
			Application.targetFrameRate = 60;
			this.m_Mode = TownBuilder.eMode.Prepare_First;
			UserDataUtil.LinkUpUtil();
			TownNetComManager.SetUpManager(base.gameObject, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.IsSetUpConnect());
			return true;
		}

		// Token: 0x06002397 RID: 9111 RVA: 0x000BF051 File Offset: 0x000BD451
		public bool IsPreparing()
		{
			return this.m_Mode >= TownBuilder.eMode.Prepare_First && this.m_Mode <= TownBuilder.eMode.Prepare_Final;
		}

		// Token: 0x06002398 RID: 9112 RVA: 0x000BF06E File Offset: 0x000BD46E
		private bool IsDebugCodeCheck(UserTownData.BuildObjectData pobj)
		{
			return pobj.m_BuildPointIndex != 0 || pobj.m_ObjID == 0;
		}

		// Token: 0x06002399 RID: 9113 RVA: 0x000BF08C File Offset: 0x000BD48C
		public bool IsBuildMngIDToBuildUp(long fmanageid)
		{
			TownBuildLinkPoint buildMngIDTreeNode = this.m_ActPakage.m_BuildTrees.GetBuildMngIDTreeNode(fmanageid, -1);
			return buildMngIDTreeNode != null && buildMngIDTreeNode.m_Pakage.IsSetUpHandle(fmanageid);
		}

		// Token: 0x0600239A RID: 9114 RVA: 0x000BF0C8 File Offset: 0x000BD4C8
		public ITownObjectHandler GetBuildMngIDToHandle(long fmanageid)
		{
			TownBuildLinkPoint buildMngIDTreeNode = this.m_ActPakage.m_BuildTrees.GetBuildMngIDTreeNode(fmanageid, -1);
			if (buildMngIDTreeNode != null)
			{
				return buildMngIDTreeNode.m_Pakage.GetHandle();
			}
			return null;
		}

		// Token: 0x0600239B RID: 9115 RVA: 0x000BF0FC File Offset: 0x000BD4FC
		public bool BuildToPoint(TownBuildUpBufParam pbuildup, bool finitup = false)
		{
			bool result = false;
			if (pbuildup.m_FixModel)
			{
				TownBuildLinkPoint townBuildLinkPoint = this.m_ActPakage.m_BuildTrees.GetBuildMngIDTreeNode(pbuildup.m_ManageID, pbuildup.m_BuildPointID);
				if (townBuildLinkPoint != null)
				{
					TownBuildPakage pakage = townBuildLinkPoint.m_Pakage;
					ITownObjectHandler townObjectHandler = ITownObjectHandler.CreateHandler(townBuildLinkPoint.m_Object, TownUtility.eResCategory.Content, this);
					pakage.SetLinkHandle(townObjectHandler);
					townObjectHandler.SetupHandle(pbuildup.m_ObjID, pbuildup.m_BuildPointID, pbuildup.m_ManageID, pakage.m_Layer + 10, this.m_TimeCategory, finitup);
					townObjectHandler.Prepare();
					townBuildLinkPoint.m_Object.SetActive(true);
					townObjectHandler.SetCallback(pbuildup.m_EventCall);
					result = true;
				}
			}
			else
			{
				TownBuildLinkPoint townBuildLinkPoint;
				if (pbuildup.m_AccessLinkID)
				{
					townBuildLinkPoint = this.m_ActPakage.m_BuildTrees.GetBuildPointTreeNode(pbuildup.m_BuildPointID);
				}
				else
				{
					townBuildLinkPoint = this.m_ActPakage.m_BuildTrees.GetBuildMngIDTreeNode(pbuildup.m_ManageID, pbuildup.m_BuildPointID);
				}
				if (townBuildLinkPoint != null)
				{
					townBuildLinkPoint.m_AccessMngID = pbuildup.m_ManageID;
					TownBuildPakage pakage = townBuildLinkPoint.m_Pakage;
					TownUtility.eResCategory fcategory = TownUtility.eResCategory.Buf;
					switch (TownUtility.GetResourceCategory(pbuildup.m_ObjID))
					{
					case eTownObjectCategory.Room:
						fcategory = TownUtility.eResCategory.Room;
						break;
					case eTownObjectCategory.Item:
					case eTownObjectCategory.Money:
					case eTownObjectCategory.Num:
					case eTownObjectCategory.Buf:
						fcategory = TownUtility.eResCategory.Buf;
						break;
					case eTownObjectCategory.Area:
						fcategory = TownUtility.eResCategory.Area;
						break;
					case eTownObjectCategory.Contents:
						fcategory = TownUtility.eResCategory.Content;
						break;
					case eTownObjectCategory.Menu:
						fcategory = TownUtility.eResCategory.MenuBuf;
						break;
					}
					ITownObjectHandler townObjectHandler = ITownObjectHandler.CreateHandler(townBuildLinkPoint.m_Object, fcategory, this);
					pakage.SetLinkHandle(townObjectHandler);
					townObjectHandler.SetupHandle(pbuildup.m_ObjID, pbuildup.m_BuildPointID, pbuildup.m_ManageID, pakage.m_Layer + 10, this.m_TimeCategory, finitup);
					townObjectHandler.Prepare();
					townObjectHandler.SetCallback(pbuildup.m_EventCall);
					this.CancelSelectObject();
					result = true;
				}
			}
			return result;
		}

		// Token: 0x0600239C RID: 9116 RVA: 0x000BF2C0 File Offset: 0x000BD6C0
		public bool BuildToArea(TownBuildUpAreaParam pparam)
		{
			TownBuildLinkPoint buildPointTreeNode = this.m_ActPakage.m_BuildTrees.GetBuildPointTreeNode(pparam.m_BuildPointID);
			ITownObjectHandler townObjectHandler = ITownObjectHandler.CreateHandler(buildPointTreeNode.m_Object, TownUtility.eResCategory.Area, this);
			buildPointTreeNode.m_Object.SetActive(true);
			TownBuildPakage componentInChildren = buildPointTreeNode.m_Object.GetComponentInChildren<TownBuildPakage>();
			componentInChildren.SetLinkHandle(townObjectHandler);
			townObjectHandler.SetupHandle(pparam.m_ObjID, componentInChildren.m_Index, pparam.m_ManageID, componentInChildren.m_Layer + 10, this.m_TimeCategory, false);
			townObjectHandler.CacheTransform.SetParent(buildPointTreeNode.m_Object.transform, false);
			townObjectHandler.Prepare();
			this.CancelSelectObject();
			return true;
		}

		// Token: 0x0600239D RID: 9117 RVA: 0x000BF360 File Offset: 0x000BD760
		private void CallbackBuildUp(int fbuildpoint, int objID, long fmanageid)
		{
			int pointToBuildType = TownUtility.GetPointToBuildType(fbuildpoint);
			if (pointToBuildType != 0)
			{
				if (pointToBuildType != 268435456)
				{
					if (pointToBuildType != 536870912)
					{
						if (pointToBuildType == 1073741824)
						{
							TownBuildUpBufParam prequest = new TownBuildUpBufParam(fbuildpoint, fmanageid, objID, false, null);
							this.BuildRequest(prequest);
						}
					}
					else
					{
						TownBuildUpBufParam prequest2 = new TownBuildUpBufParam(fbuildpoint, fmanageid, objID, false, null);
						this.BuildRequest(prequest2);
						MissionManager.SetPopUpActive(eXlsPopupTiming.PopTown, false);
						MissionManager.UnlockAction(eXlsMissionSeg.Town, eXlsMissionTownFuncType.ContentBuild);
					}
				}
			}
			else
			{
				TownBuildUpAreaParam pparam = new TownBuildUpAreaParam(fbuildpoint, fmanageid, objID);
				this.BuildToArea(pparam);
			}
		}

		// Token: 0x0600239E RID: 9118 RVA: 0x000BF3FC File Offset: 0x000BD7FC
		private void CallbackChangePoint(int buildpoint, int objID, long fmanageid)
		{
			TownBuildChangeAreaParam prequest = new TownBuildChangeAreaParam(buildpoint, fmanageid, objID);
			this.BuildRequest(prequest);
		}

		// Token: 0x14000034 RID: 52
		// (add) Token: 0x0600239F RID: 9119 RVA: 0x000BF41C File Offset: 0x000BD81C
		// (remove) Token: 0x060023A0 RID: 9120 RVA: 0x000BF454 File Offset: 0x000BD854
		public event Action<IMainComCommand> OnEventSend;

		// Token: 0x060023A1 RID: 9121 RVA: 0x000BF48A File Offset: 0x000BD88A
		private void CancelSelectObject()
		{
			if (this.m_BackSelectingObj != null)
			{
				this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
			}
			this.m_BackSelectingObj = null;
			this.m_SelectingObj = null;
			this.m_MouseFirstTouch = false;
		}

		// Token: 0x060023A2 RID: 9122 RVA: 0x000BF4C4 File Offset: 0x000BD8C4
		public void SetUIMode(bool fset)
		{
			if (fset)
			{
				this.m_Mode = TownBuilder.eMode.MenuMode;
				if (this.m_BackSelectingObj != null)
				{
					this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
				}
				this.m_BackSelectingObj = null;
				this.m_SelectingObj = null;
			}
			else
			{
				this.m_Mode = TownBuilder.eMode.Main;
			}
		}

		// Token: 0x060023A3 RID: 9123 RVA: 0x000BF51B File Offset: 0x000BD91B
		public void SetMenuActive(bool fmenuactive)
		{
			this.m_MenuActive = fmenuactive;
			if (this.m_MenuActive)
			{
				this.m_MenuTouchLock = 2;
				this.m_MouseFirstTouch = false;
			}
		}

		// Token: 0x060023A4 RID: 9124 RVA: 0x000BF540 File Offset: 0x000BD940
		public void SetMainMode(TownMain.eMode fmode)
		{
			if (fmode != this.m_BackMode)
			{
				this.m_BackMode = fmode;
				TownHandleActionMenuMode pact = new TownHandleActionMenuMode(fmode == TownMain.eMode.Town);
				int treeNodeNum = this.m_ActPakage.m_BuildTrees.GetTreeNodeNum();
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null)
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						if (handle != null)
						{
							handle.RecvEvent(pact);
						}
					}
				}
			}
		}

		// Token: 0x060023A5 RID: 9125 RVA: 0x000BF5C8 File Offset: 0x000BD9C8
		public void UpdateMain()
		{
			this.m_MenuTouchLock--;
			if (this.m_MenuTouchLock <= 0)
			{
				this.m_MenuTouchLock = 0;
			}
			if (!this.m_MenuActive && this.m_MenuTouchLock == 0)
			{
				InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
				if (info.m_bAvailable)
				{
					if (info.m_TouchPhase == TouchPhase.Began || info.m_TouchPhase == TouchPhase.Stationary || info.m_TouchPhase == TouchPhase.Moved)
					{
						if (!this.m_MouseFirstTouch)
						{
							if (!UIUtility.CheckPointOverUI(info.m_StartPos))
							{
								Ray ray = this.m_Camera.ScreenPointToRay(info.m_StartPos);
								RaycastHit raycastHit;
								if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
								{
									"TownObject"
								})))
								{
									if (raycastHit.collider != null)
									{
										TownObjectConnect component = raycastHit.transform.gameObject.GetComponent<TownObjectConnect>();
										if (component != null)
										{
											this.m_SelectingObj = component.m_Link;
											this.m_SelectingObj.CalcTouchEvent(ITownObjectHandler.eTouchState.Begin, this);
										}
										else
										{
											this.CloseUIEventObject();
										}
									}
								}
								else
								{
									this.CloseUIEventObject();
								}
							}
							this.m_MouseFirstTouch = true;
						}
						else
						{
							Ray ray2 = this.m_Camera.ScreenPointToRay(info.m_RawPos);
							RaycastHit raycastHit2;
							if (Physics.Raycast(ray2.origin, ray2.direction, out raycastHit2, float.PositiveInfinity, LayerMask.GetMask(new string[]
							{
								"TownObject"
							})) && raycastHit2.collider != null)
							{
								TownObjectConnect component = raycastHit2.transform.gameObject.GetComponent<TownObjectConnect>();
								if (component != null && this.m_SelectingObj != component.m_Link)
								{
									this.m_SelectingObj = null;
									if (this.m_BackSelectingObj != null)
									{
										this.m_BackSelectingObj.CalcTouchEvent(ITownObjectHandler.eTouchState.Cancel, this);
										this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
										this.m_BackSelectingObj = null;
									}
								}
							}
							if (this.m_GameCamera.IsDragging)
							{
								if (this.m_SelectingObj != null)
								{
									this.m_SelectingObj.CalcTouchEvent(ITownObjectHandler.eTouchState.Cancel, this);
									this.CloseUIEventObject();
								}
								this.m_SelectingObj = null;
							}
						}
					}
					if (info.m_TouchPhase == TouchPhase.Ended)
					{
						if (this.m_MouseFirstTouch)
						{
							this.ObjectSelectEventCmd(this.m_SelectingObj);
							this.m_MouseFirstTouch = false;
						}
						this.m_SelectingObj = null;
					}
				}
			}
			else
			{
				this.m_SelectingObj = null;
			}
		}

		// Token: 0x060023A6 RID: 9126 RVA: 0x000BF868 File Offset: 0x000BDC68
		private void ObjectSelectEventCmd(ITownObjectHandler phandle)
		{
			if (phandle != null)
			{
				phandle.CalcTouchEvent(ITownObjectHandler.eTouchState.End, this);
				if (this.m_BackSelectingObj != null && this.m_BackSelectingObj != phandle)
				{
					this.m_BackSelectingObj.CalcTouchEvent(ITownObjectHandler.eTouchState.Cancel, this);
					this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
				}
				this.m_BackSelectingObj = phandle;
				this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(true));
			}
			else
			{
				if (this.m_BackSelectingObj != null)
				{
					this.m_BackSelectingObj.CalcTouchEvent(ITownObjectHandler.eTouchState.Cancel, this);
					this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
					this.m_BackSelectingObj = null;
				}
				TownComUI townComUI = new TownComUI(false);
				townComUI.m_SelectHandle = null;
				this.OnEventSend.Call(townComUI);
			}
		}

		// Token: 0x060023A7 RID: 9127 RVA: 0x000BF93C File Offset: 0x000BDD3C
		private void CloseUIEventObject()
		{
			TownComUI townComUI = new TownComUI(false);
			townComUI.m_SelectHandle = null;
			this.OnEventSend.Call(townComUI);
			if (this.m_BackSelectingObj != null)
			{
				this.m_BackSelectingObj.RecvEvent(new TownHandleActionTouchSelect(false));
				this.m_BackSelectingObj = null;
			}
		}

		// Token: 0x060023A8 RID: 9128 RVA: 0x000BF990 File Offset: 0x000BDD90
		public void SetBuildPointsEnable(TownBuilder.eSelectType fselect, int fbuildID, long fmanageid = 0L, int fmaskbuildpoint = 0)
		{
			int treeNodeNum = this.m_ActPakage.m_BuildTrees.GetTreeNodeNum();
			switch (fselect)
			{
			case TownBuilder.eSelectType.FreeArea:
			{
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.OpenSelectUI);
				this.m_Mode = TownBuilder.eMode.FreeAreaSelect;
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null && indexToNode.GetCategory() == eBuildPointCategory.Area)
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						if (handle.HandleCategory == TownUtility.eResCategory.AreaFree)
						{
							handle.RecvEvent(pact);
						}
					}
				}
				this.m_BuildBufID = fbuildID;
				this.m_MarkBuildPoint = fmaskbuildpoint;
				break;
			}
			case TownBuilder.eSelectType.MaskArea:
			{
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.OpenSelectUI);
				this.m_Mode = TownBuilder.eMode.AreaSelect;
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null && (indexToNode.GetCategory() == eBuildPointCategory.Area || indexToNode.GetCategory() == eBuildPointCategory.Contents) && TownUtility.GetBuildAreaID(indexToNode.GetAccessID()) != TownUtility.GetBuildAreaID(fmaskbuildpoint))
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						handle.RecvEvent(pact);
					}
				}
				this.m_BuildManageID = fmanageid;
				this.m_BuildBufID = fbuildID;
				this.m_MarkBuildPoint = fmaskbuildpoint;
				break;
			}
			case TownBuilder.eSelectType.FreeBufPoint:
			{
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.OpenSelectUI);
				this.m_Mode = TownBuilder.eMode.FreeBufSelect;
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null && indexToNode.GetCategory() == eBuildPointCategory.Buf)
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						if (handle.HandleCategory == TownUtility.eResCategory.BufFree)
						{
							handle.RecvEvent(pact);
						}
					}
				}
				break;
			}
			case TownBuilder.eSelectType.MaskBufPoint:
			{
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.OpenSelectUI);
				this.m_Mode = TownBuilder.eMode.BufSelect;
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null && indexToNode.GetCategory() == eBuildPointCategory.Buf)
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						handle.RecvEvent(pact);
					}
				}
				this.m_BuildBufID = fbuildID;
				this.m_BuildManageID = fmanageid;
				this.m_MarkBuildPoint = fmaskbuildpoint;
				break;
			}
			case TownBuilder.eSelectType.Close:
			{
				ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.CloseSelectUI);
				this.m_Mode = TownBuilder.eMode.Main;
				for (int i = 0; i < treeNodeNum; i++)
				{
					TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
					if (indexToNode != null && (indexToNode.GetCategory() == eBuildPointCategory.Area || indexToNode.GetCategory() == eBuildPointCategory.Contents || indexToNode.GetCategory() == eBuildPointCategory.Buf))
					{
						ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
						handle.RecvEvent(pact);
					}
				}
				break;
			}
			}
		}

		// Token: 0x060023A9 RID: 9129 RVA: 0x000BFC38 File Offset: 0x000BE038
		public void ClearSelectUI()
		{
			this.m_Mode = TownBuilder.eMode.Main;
			int treeNodeNum = this.m_ActPakage.m_BuildTrees.GetTreeNodeNum();
			for (int i = 0; i < treeNodeNum; i++)
			{
				TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
				if (indexToNode != null && (indexToNode.GetCategory() == eBuildPointCategory.Buf || indexToNode.GetCategory() == eBuildPointCategory.Area))
				{
					ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
					handle.RecvEvent(ITownHandleAction.CreateHandleAction(handle, eTownEventAct.CloseSelectUI));
				}
			}
		}

		// Token: 0x060023AA RID: 9130 RVA: 0x000BFCB8 File Offset: 0x000BE0B8
		private void UpdateAreaSelect()
		{
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			if (info.m_bAvailable)
			{
				Ray ray = this.m_Camera.ScreenPointToRay(info.m_CurrentPos);
				bool flag = false;
				RaycastHit raycastHit;
				bool flag2 = Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
				{
					"TownObject"
				}));
				if (this.m_DownBuildPoint == null)
				{
					this.m_IsPointOverUI = UIUtility.CheckPointOverUI(info.m_StartPos);
					if (info.m_TouchPhase == TouchPhase.Began && !this.m_IsPointOverUI && flag2)
					{
						TownObjectConnect component = raycastHit.transform.gameObject.GetComponent<TownObjectConnect>();
						if (component != null)
						{
							if (this.m_Mode == TownBuilder.eMode.FreeAreaSelect)
							{
								if (component.m_Link.HandleCategory == TownUtility.eResCategory.AreaFree)
								{
									this.m_DownBuildPoint = component.m_Link;
								}
							}
							else if (component.m_Link.HandleCategory == TownUtility.eResCategory.AreaFree || component.m_Link.HandleCategory == TownUtility.eResCategory.Area || component.m_Link.HandleCategory == TownUtility.eResCategory.Content)
							{
								this.m_DownBuildPoint = component.m_Link;
							}
						}
					}
				}
				else if (this.m_GameCamera.IsDragging)
				{
					flag = true;
				}
				else if (info.m_TouchPhase != TouchPhase.Moved && info.m_TouchPhase != TouchPhase.Stationary)
				{
					if (info.m_TouchPhase == TouchPhase.Ended)
					{
						if (this.m_Mode == TownBuilder.eMode.FreeAreaSelect)
						{
							this.DecideBuildAreaPoint(this.m_DownBuildPoint);
						}
						else
						{
							this.DecideSwapAreaPoint(this.m_DownBuildPoint);
						}
						flag = true;
					}
				}
				if (flag)
				{
					this.m_DownBuildPoint = null;
				}
			}
		}

		// Token: 0x060023AB RID: 9131 RVA: 0x000BFE78 File Offset: 0x000BE278
		private void DecideBuildAreaPoint(ITownObjectHandler buildPoint)
		{
			TownObjHandleFree townObjHandleFree = (TownObjHandleFree)buildPoint;
			TownComBuild townComBuild = new TownComBuild(eTownCommand.BuildArea);
			townComBuild.m_BuildPoint = townObjHandleFree.GetBuildAccessID();
			townComBuild.m_ObjID = this.m_BuildBufID;
			this.OnEventSend.Call(townComBuild);
		}

		// Token: 0x060023AC RID: 9132 RVA: 0x000BFEB8 File Offset: 0x000BE2B8
		private void DecideSwapAreaPoint(ITownObjectHandler buildPoint)
		{
			TownComMoveDecision townComMoveDecision = new TownComMoveDecision();
			townComMoveDecision.m_BaseBuildPoint = this.m_MarkBuildPoint;
			townComMoveDecision.m_BaseMngID = this.m_BuildManageID;
			townComMoveDecision.m_TargetBuildPoint = buildPoint.GetBuildAccessID();
			townComMoveDecision.m_ChangeMngID = buildPoint.GetManageID();
			this.OnEventSend.Call(townComMoveDecision);
			this.CloseSelectAreaMarker(townComMoveDecision.m_BaseBuildPoint, townComMoveDecision.m_TargetBuildPoint);
		}

		// Token: 0x060023AD RID: 9133 RVA: 0x000BFF1C File Offset: 0x000BE31C
		private void CloseSelectAreaMarker(int fselect, int ftarget)
		{
			ITownHandleAction pact = ITownHandleAction.CreateAction(eTownEventAct.CloseSelectUI);
			int treeNodeNum = this.m_ActPakage.m_BuildTrees.GetTreeNodeNum();
			for (int i = 0; i < treeNodeNum; i++)
			{
				TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
				if (indexToNode != null && (indexToNode.GetCategory() == eBuildPointCategory.Area || indexToNode.GetCategory() == eBuildPointCategory.Buf))
				{
					ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
					if (handle.GetBuildAccessID() != fselect && handle.GetBuildAccessID() != ftarget)
					{
						handle = indexToNode.m_Pakage.GetHandle();
						handle.RecvEvent(pact);
					}
				}
			}
		}

		// Token: 0x060023AE RID: 9134 RVA: 0x000BFFC0 File Offset: 0x000BE3C0
		private void UpdateBufSelect()
		{
			InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
			if (info.m_bAvailable)
			{
				Ray ray = this.m_Camera.ScreenPointToRay(info.m_CurrentPos);
				bool flag = false;
				RaycastHit raycastHit;
				bool flag2 = Physics.Raycast(ray.origin, ray.direction, out raycastHit, float.PositiveInfinity, LayerMask.GetMask(new string[]
				{
					"TownObject"
				}));
				if (this.m_DownBuildPoint == null)
				{
					this.m_IsPointOverUI = UIUtility.CheckPointOverUI(info.m_StartPos);
					if (info.m_TouchPhase == TouchPhase.Began && !this.m_IsPointOverUI && flag2)
					{
						TownObjectConnect component = raycastHit.transform.gameObject.GetComponent<TownObjectConnect>();
						if (component != null)
						{
							if (this.m_Mode == TownBuilder.eMode.FreeBufSelect)
							{
								if (component.m_Link.HandleCategory == TownUtility.eResCategory.BufFree)
								{
									this.m_DownBuildPoint = component.m_Link;
								}
							}
							else if (component.m_Link.HandleCategory == TownUtility.eResCategory.BufFree || component.m_Link.HandleCategory == TownUtility.eResCategory.Buf)
							{
								this.m_DownBuildPoint = component.m_Link;
							}
						}
					}
				}
				else if (this.m_GameCamera.IsDragging)
				{
					flag = true;
				}
				else if (info.m_TouchPhase != TouchPhase.Moved && info.m_TouchPhase != TouchPhase.Stationary)
				{
					if (info.m_TouchPhase == TouchPhase.Ended)
					{
						if (this.m_Mode == TownBuilder.eMode.FreeBufSelect)
						{
							this.DecideBuildBufPoint(this.m_DownBuildPoint);
						}
						else
						{
							this.DecideSwapBufPoint(this.m_DownBuildPoint);
						}
						flag = true;
					}
				}
				if (flag)
				{
					this.m_DownBuildPoint = null;
				}
			}
		}

		// Token: 0x060023AF RID: 9135 RVA: 0x000C0170 File Offset: 0x000BE570
		private void DecideBuildBufPoint(ITownObjectHandler buildPoint)
		{
			TownObjHandleBufFree townObjHandleBufFree = (TownObjHandleBufFree)buildPoint;
			TownComBuild townComBuild = new TownComBuild(eTownCommand.BuildBuf);
			townComBuild.m_BuildPoint = townObjHandleBufFree.GetBuildAccessID();
			this.OnEventSend.Call(townComBuild);
		}

		// Token: 0x060023B0 RID: 9136 RVA: 0x000C01A4 File Offset: 0x000BE5A4
		private void DecideSwapBufPoint(ITownObjectHandler buildPoint)
		{
			TownComMoveDecision townComMoveDecision = new TownComMoveDecision();
			townComMoveDecision.m_BaseBuildPoint = this.m_MarkBuildPoint;
			townComMoveDecision.m_BaseMngID = this.m_BuildManageID;
			townComMoveDecision.m_TargetBuildPoint = buildPoint.GetBuildAccessID();
			townComMoveDecision.m_ChangeMngID = buildPoint.GetManageID();
			this.OnEventSend.Call(townComMoveDecision);
			this.CloseSelectAreaMarker(townComMoveDecision.m_BaseBuildPoint, townComMoveDecision.m_TargetBuildPoint);
		}

		// Token: 0x060023B1 RID: 9137 RVA: 0x000C0208 File Offset: 0x000BE608
		private void FlushEventRequest()
		{
			if (this.m_EventQue != null)
			{
				int comCommandNum = this.m_EventQue.GetComCommandNum();
				for (int i = 0; i < comCommandNum; i++)
				{
					ITownEventCommand comCommand = this.m_EventQue.GetComCommand(i);
					comCommand.CalcRequest(this);
				}
				this.m_EventQue.Flush();
			}
		}

		// Token: 0x060023B2 RID: 9138 RVA: 0x000C025E File Offset: 0x000BE65E
		public void BuildRequest(ITownEventCommand prequest)
		{
			this.m_EventQue.PushComCommand(prequest);
		}

		// Token: 0x060023B3 RID: 9139 RVA: 0x000C026C File Offset: 0x000BE66C
		public void EventRequest(ITownEventCommand prequest)
		{
			this.m_EventQue.PushComCommand(prequest);
		}

		// Token: 0x060023B4 RID: 9140 RVA: 0x000C027C File Offset: 0x000BE67C
		public int CreateSyncKey(int fuid = -1)
		{
			TownBuilder.SyncResult syncResult = new TownBuilder.SyncResult();
			if (fuid <= 0)
			{
				this.m_SyncUID++;
				if (this.m_SyncUID >= 1073741823)
				{
					this.m_SyncUID = 0;
				}
				fuid = this.m_SyncUID;
			}
			syncResult.m_UID = fuid;
			this.m_SyncResult.Add(syncResult);
			return fuid;
		}

		// Token: 0x060023B5 RID: 9141 RVA: 0x000C02D8 File Offset: 0x000BE6D8
		public void DelSyncKey(int fuid)
		{
			for (int i = 0; i < this.m_SyncResult.Count; i++)
			{
				if (this.m_SyncResult[i].m_UID == fuid)
				{
					this.m_SyncResult.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x060023B6 RID: 9142 RVA: 0x000C032C File Offset: 0x000BE72C
		public bool SetActiveSyncKey(int fuid)
		{
			for (int i = 0; i < this.m_SyncResult.Count; i++)
			{
				if (this.m_SyncResult[i].m_UID == fuid)
				{
					this.m_SyncResult[i].m_Sync = true;
					break;
				}
			}
			return true;
		}

		// Token: 0x060023B7 RID: 9143 RVA: 0x000C0384 File Offset: 0x000BE784
		public bool IsActiveSyncKey(int fuid)
		{
			for (int i = 0; i < this.m_SyncResult.Count; i++)
			{
				if (this.m_SyncResult[i].m_UID == fuid)
				{
					return this.m_SyncResult[i].m_Sync;
				}
			}
			return true;
		}

		// Token: 0x060023B8 RID: 9144 RVA: 0x000C03D8 File Offset: 0x000BE7D8
		public void SendHandleAction(ITownHandleAction pact, long ftarget = 0L)
		{
			if (ftarget != -1L)
			{
				TownBuildLinkPoint buildMngIDTreeNode = this.m_ActPakage.m_BuildTrees.GetBuildMngIDTreeNode(ftarget, -1);
				if (buildMngIDTreeNode != null)
				{
					buildMngIDTreeNode.m_Pakage.GetHandle().RecvEvent(pact);
				}
			}
		}

		// Token: 0x060023B9 RID: 9145 RVA: 0x000C0418 File Offset: 0x000BE818
		public void RefreshIcon(UserFieldCharaData.UpFieldCharaData[] charaMngTable)
		{
			if (charaMngTable != null)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				FieldMapManager.MemberComCheck memberComCheck = new FieldMapManager.MemberComCheck();
				userDataMng.UserFieldChara.CheckManageCharaID(charaMngTable, new UserFieldCharaData.CharaInOutEvent(memberComCheck.CallbackEvent));
				memberComCheck.SendComCheck();
				FieldMapManager.RefreshManageChara(new CharaBuildState(this.m_CharaManager.CallbackCharaState));
			}
			this.m_CharaManager.RefreshBindPos();
		}

		// Token: 0x060023BA RID: 9146 RVA: 0x000C047B File Offset: 0x000BE87B
		public bool StackEventQue(IMainComCommand pevent)
		{
			this.OnEventSend.Call(pevent);
			return true;
		}

		// Token: 0x060023BB RID: 9147 RVA: 0x000C048C File Offset: 0x000BE88C
		public void ResetBuildPoint()
		{
			TownObjHandleField componentInChildren = base.gameObject.GetComponentInChildren<TownObjHandleField>();
			if (componentInChildren)
			{
				componentInChildren.OpenArea();
			}
		}

		// Token: 0x060023BC RID: 9148 RVA: 0x000C04B8 File Offset: 0x000BE8B8
		public bool IsEventActiveKey(int fkeyid)
		{
			for (int i = 0; i < this.m_ActionKey.Count; i++)
			{
				if (this.m_ActionKey[i] == fkeyid)
				{
					this.m_ActionKey.RemoveAt(i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023BD RID: 9149 RVA: 0x000C0502 File Offset: 0x000BE902
		public void SetEventActeiveKey(int fkeyid, bool factive)
		{
			this.m_ActionKey.Add(fkeyid);
		}

		// Token: 0x060023BE RID: 9150 RVA: 0x000C0510 File Offset: 0x000BE910
		private void CheckTimeBuilding()
		{
			int hour = ScheduleTimeUtil.GetManageUnixTime().Hour;
			if (this.m_TimeCheckHour != hour)
			{
				byte b = 0;
				if (hour >= 18 || hour < 6)
				{
					b = 1;
				}
				if (b != this.m_TimeCategory)
				{
					TownBuildCrossFade.CreateFade(this);
					this.m_TimeCategory = b;
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
				}
				this.m_TimeCheckHour = hour;
			}
		}

		// Token: 0x060023BF RID: 9151 RVA: 0x000C05A0 File Offset: 0x000BE9A0
		public void PreLoadResource()
		{
			Application.targetFrameRate = 60;
			GameObject gameObject = new GameObject("PreLoader");
			this.m_Preload = gameObject.AddComponent<TownResourceDownload>();
			this.m_Preload.AddResource(TownUtility.GetModelResourcePath(TownUtility.eResCategory.Field, 0, (int)this.m_TimeCategory));
			int treeNodeNum = this.m_ActPakage.m_BuildTrees.GetTreeNodeNum();
			for (int i = 0; i < treeNodeNum; i++)
			{
				TownBuildLinkPoint indexToNode = this.m_ActPakage.m_BuildTrees.GetIndexToNode(i);
				if (indexToNode != null && indexToNode.m_Pakage != null)
				{
					ITownObjectHandler handle = indexToNode.m_Pakage.GetHandle();
					if (handle != null && TownUtility.IsModelResourcePath(handle.HandleCategory, handle.GetResourceKey()))
					{
						this.m_Preload.AddResource(TownUtility.GetModelResourcePath(handle.HandleCategory, handle.GetResourceKey(), (int)this.m_TimeCategory));
					}
				}
			}
			this.m_DebugTimes = 0f;
			this.m_Count = 0;
		}

		// Token: 0x060023C0 RID: 9152 RVA: 0x000C0693 File Offset: 0x000BEA93
		public bool IsEndPreLoadResource()
		{
			return this.m_Preload.IsDownloadEnd();
		}

		// Token: 0x060023C1 RID: 9153 RVA: 0x000C06A0 File Offset: 0x000BEAA0
		public void RebuildObject()
		{
			if (this.m_Preload != null)
			{
				ObjectResourceManager.ObjectDestroy(this.m_Preload.gameObject, true);
				this.m_Preload = null;
			}
			this.m_Mode = TownBuilder.eMode.RebuildMode;
		}

		// Token: 0x060023C2 RID: 9154 RVA: 0x000C06D2 File Offset: 0x000BEAD2
		public bool IsSetUpRebuild()
		{
			return this.m_Mode == TownBuilder.eMode.Main;
		}

		// Token: 0x04002A5A RID: 10842
		private TownEventQue m_EventQue;

		// Token: 0x04002A5B RID: 10843
		private List<TownBuilder.SyncResult> m_SyncResult;

		// Token: 0x04002A5C RID: 10844
		private int m_SyncUID;

		// Token: 0x04002A5D RID: 10845
		private TownResourceObjectData m_CharaCache;

		// Token: 0x04002A5E RID: 10846
		private TownHudMessageControll m_HudMsgCtrl;

		// Token: 0x04002A5F RID: 10847
		[SerializeField]
		private TownCacheObjectData m_MarkerCache;

		// Token: 0x04002A60 RID: 10848
		private long m_UserTownMngID;

		// Token: 0x04002A61 RID: 10849
		public TownBuilder.ActiveBuildPakage m_ActPakage;

		// Token: 0x04002A62 RID: 10850
		public TownCamera m_GameCamera;

		// Token: 0x04002A63 RID: 10851
		private TownCharaManager m_CharaManager;

		// Token: 0x04002A64 RID: 10852
		private bool m_MenuActive;

		// Token: 0x04002A65 RID: 10853
		private bool m_MouseFirstTouch;

		// Token: 0x04002A66 RID: 10854
		private int m_MenuTouchLock;

		// Token: 0x04002A67 RID: 10855
		public GameObject m_TraningIcon;

		// Token: 0x04002A68 RID: 10856
		private TownEffectManager m_EffectMng;

		// Token: 0x04002A69 RID: 10857
		[NonSerialized]
		public byte m_TimeCategory;

		// Token: 0x04002A6A RID: 10858
		public int m_TimeCheckHour;

		// Token: 0x04002A6B RID: 10859
		public GameObject m_DebugTownData;

		// Token: 0x04002A6C RID: 10860
		private Camera m_Camera;

		// Token: 0x04002A6D RID: 10861
		private TownMain m_Owner;

		// Token: 0x04002A6E RID: 10862
		private TownBuilder.eMode m_Mode = TownBuilder.eMode.None;

		// Token: 0x04002A6F RID: 10863
		private TownBuilder.DebugLogUp m_Log = new TownBuilder.DebugLogUp();

		// Token: 0x04002A70 RID: 10864
		private float m_DebugTimes;

		// Token: 0x04002A71 RID: 10865
		private int m_Count;

		// Token: 0x04002A72 RID: 10866
		private ITownObjectHandler m_SelectingObj;

		// Token: 0x04002A73 RID: 10867
		private ITownObjectHandler m_BackSelectingObj;

		// Token: 0x04002A75 RID: 10869
		private TownMain.eMode m_BackMode;

		// Token: 0x04002A76 RID: 10870
		private int m_BuildBufID;

		// Token: 0x04002A77 RID: 10871
		private int m_MarkBuildPoint;

		// Token: 0x04002A78 RID: 10872
		private long m_BuildManageID;

		// Token: 0x04002A79 RID: 10873
		private ITownObjectHandler m_DownBuildPoint;

		// Token: 0x04002A7A RID: 10874
		private bool m_IsPointOverUI;

		// Token: 0x04002A7B RID: 10875
		private List<int> m_ActionKey = new List<int>();

		// Token: 0x04002A7C RID: 10876
		private TownResourceDownload m_Preload;

		// Token: 0x02000706 RID: 1798
		public enum eSelectType
		{
			// Token: 0x04002A7E RID: 10878
			FreeArea,
			// Token: 0x04002A7F RID: 10879
			MaskArea,
			// Token: 0x04002A80 RID: 10880
			FreeBufPoint,
			// Token: 0x04002A81 RID: 10881
			MaskBufPoint,
			// Token: 0x04002A82 RID: 10882
			Close
		}

		// Token: 0x02000707 RID: 1799
		public class SyncResult
		{
			// Token: 0x04002A83 RID: 10883
			public int m_UID;

			// Token: 0x04002A84 RID: 10884
			public bool m_Sync;
		}

		// Token: 0x02000708 RID: 1800
		public class ActiveBuildPakage
		{
			// Token: 0x04002A85 RID: 10885
			public TownBuildTree m_BuildTrees;

			// Token: 0x04002A86 RID: 10886
			public ITownObjectHandler m_TownField;

			// Token: 0x04002A87 RID: 10887
			public List<BindTownCharaMap> m_CharaBuildBind = new List<BindTownCharaMap>();
		}

		// Token: 0x02000709 RID: 1801
		public enum eMode
		{
			// Token: 0x04002A89 RID: 10889
			None = -1,
			// Token: 0x04002A8A RID: 10890
			Prepare_First,
			// Token: 0x04002A8B RID: 10891
			Prepare_FieldObject,
			// Token: 0x04002A8C RID: 10892
			Prepare_BuildObjects_Wait,
			// Token: 0x04002A8D RID: 10893
			Prepare_CharaObject_Wait,
			// Token: 0x04002A8E RID: 10894
			Prepare_Final,
			// Token: 0x04002A8F RID: 10895
			Main,
			// Token: 0x04002A90 RID: 10896
			RebuildMode,
			// Token: 0x04002A91 RID: 10897
			MenuMode,
			// Token: 0x04002A92 RID: 10898
			FreeBufSelect,
			// Token: 0x04002A93 RID: 10899
			BufSelect,
			// Token: 0x04002A94 RID: 10900
			FreeAreaSelect,
			// Token: 0x04002A95 RID: 10901
			AreaSelect,
			// Token: 0x04002A96 RID: 10902
			Num
		}

		// Token: 0x0200070A RID: 1802
		public class DebugLogUp
		{
			// Token: 0x060023C6 RID: 9158 RVA: 0x000C070B File Offset: 0x000BEB0B
			public void Logs(string fmsg)
			{
				this.m_List.Add(fmsg);
			}

			// Token: 0x060023C7 RID: 9159 RVA: 0x000C071C File Offset: 0x000BEB1C
			public void DebugOut()
			{
				for (int i = 0; i < this.m_List.Count; i++)
				{
					Debug.Log(this.m_List[i]);
				}
			}

			// Token: 0x04002A97 RID: 10903
			public List<string> m_List = new List<string>();
		}
	}
}
