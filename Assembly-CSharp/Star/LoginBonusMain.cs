﻿using System;

namespace Star
{
	// Token: 0x02000427 RID: 1063
	public class LoginBonusMain : GameStateMain
	{
		// Token: 0x0600146C RID: 5228 RVA: 0x0006C194 File Offset: 0x0006A594
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x0600146D RID: 5229 RVA: 0x0006C19D File Offset: 0x0006A59D
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600146E RID: 5230 RVA: 0x0006C1A8 File Offset: 0x0006A5A8
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID != 0)
			{
				return null;
			}
			return new LoginBonusState_Main(this);
		}

		// Token: 0x04001B48 RID: 6984
		public const int STATE_MAIN = 0;
	}
}
