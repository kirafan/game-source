﻿using System;

namespace Star
{
	// Token: 0x02000365 RID: 869
	public class FldComDefaultDB : IFldNetComModule
	{
		// Token: 0x06001077 RID: 4215 RVA: 0x00057239 File Offset: 0x00055639
		public FldComDefaultDB() : base(IFldNetComManager.Instance)
		{
			FldDefaultObjectUtil.DatabaseSetUp();
		}

		// Token: 0x06001078 RID: 4216 RVA: 0x0005724B File Offset: 0x0005564B
		public override bool UpFunc()
		{
			if (!FldComDefaultDB.m_SetUpChk && FldDefaultObjectUtil.IsSetUp())
			{
				FldComDefaultDB.m_SetUpChk = true;
				return false;
			}
			return true;
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x0005726A File Offset: 0x0005566A
		public static bool IsSetUp()
		{
			return FldComDefaultDB.m_SetUpChk;
		}

		// Token: 0x0400177C RID: 6012
		private static bool m_SetUpChk;
	}
}
