﻿using System;

namespace Star
{
	// Token: 0x020002A8 RID: 680
	public class AfterUpgradeParamWrapper : HaveBeforeAfterCharaParamCharacterEditSceneCharacterParamWrapper<EditUtility.SimulateUpgradeParam>
	{
		// Token: 0x06000CD2 RID: 3282 RVA: 0x00048ECC File Offset: 0x000472CC
		public AfterUpgradeParamWrapper(EditUtility.SimulateUpgradeParam simulateParam) : base(eCharacterDataType.AfterUpgradeParam, simulateParam)
		{
		}
	}
}
