﻿using System;

namespace Star
{
	// Token: 0x020005FB RID: 1531
	public class IRoomEventCommand
	{
		// Token: 0x06001E32 RID: 7730 RVA: 0x000A1E30 File Offset: 0x000A0230
		public virtual bool CalcRequest(RoomBuilder pbuild)
		{
			return true;
		}

		// Token: 0x040024C1 RID: 9409
		public eRoomRequest m_Type;

		// Token: 0x040024C2 RID: 9410
		public bool m_Enable;
	}
}
