﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x0200040F RID: 1039
	public class EditState_PartyEdit : EditState_PartyEditBase<PartyEditUI>
	{
		// Token: 0x060013C1 RID: 5057 RVA: 0x00069F18 File Offset: 0x00068318
		public EditState_PartyEdit(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013C2 RID: 5058 RVA: 0x00069F21 File Offset: 0x00068321
		public override SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.PartyEditUI;
		}

		// Token: 0x060013C3 RID: 5059 RVA: 0x00069F24 File Offset: 0x00068324
		public override int GetCharaListSceneId()
		{
			return 3;
		}

		// Token: 0x060013C4 RID: 5060 RVA: 0x00069F27 File Offset: 0x00068327
		public override int GetSelectedPartyIndex()
		{
			return LocalSaveData.Inst.SelectedPartyIndex;
		}

		// Token: 0x060013C5 RID: 5061 RVA: 0x00069F33 File Offset: 0x00068333
		public override void SetSelectedPartyIndex(int index)
		{
			LocalSaveData.Inst.SelectedPartyIndex = index;
			LocalSaveData.Inst.Save();
		}

		// Token: 0x060013C6 RID: 5062 RVA: 0x00069F4A File Offset: 0x0006834A
		public override void SetSelectedPartyMemberIndex(int index)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex = index;
		}

		// Token: 0x060013C7 RID: 5063 RVA: 0x00069F5C File Offset: 0x0006835C
		public override void SetSelectedCharaMngId(long charaMngId)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID = charaMngId;
		}

		// Token: 0x060013C8 RID: 5064 RVA: 0x00069F6E File Offset: 0x0006836E
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (LocalSaveData.Inst.SelectedPartyIndex != this.m_UI.SelectPartyIndex)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
			}
			base.OnClickBackButton(isCallFromShortCut);
			this.m_UI.SelectButton = PartyEditUIBase.eButton.Back;
		}

		// Token: 0x060013C9 RID: 5065 RVA: 0x00069FAD File Offset: 0x000683AD
		protected override void SetSceneInfo()
		{
			base.SetSceneInfo();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.PartyEdit);
		}

		// Token: 0x060013CA RID: 5066 RVA: 0x00069FC6 File Offset: 0x000683C6
		protected override void RequestSetPartyAll()
		{
			this.m_Owner.Request_BattlePartySetAll(new Action(base.GoToMenuEnd));
		}

		// Token: 0x060013CB RID: 5067 RVA: 0x00069FDF File Offset: 0x000683DF
		protected override void RequestSetPartyName(string partyName)
		{
			this.Request_BattlePartySetName(this.m_UI.SelectPartyIndex, partyName, new MeigewwwParam.Callback(this.OnResponse_BattlePartySetName));
		}

		// Token: 0x060013CC RID: 5068 RVA: 0x00069FFF File Offset: 0x000683FF
		protected override void OpenTutorialTipsWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_PARTY_EDIT, null, null);
		}

		// Token: 0x060013CD RID: 5069 RVA: 0x0006A018 File Offset: 0x00068418
		public void Request_BattlePartySetName(int partyIndex, string partyName, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Battlepartysetname battlepartysetname = new PlayerRequestTypes.Battlepartysetname();
			UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[partyIndex];
			this.m_RequestedPartyName = partyName;
			battlepartysetname.managedBattlePartyId = userBattlePartyData.MngID;
			battlepartysetname.name = partyName;
			MeigewwwParam wwwParam = PlayerRequest.Battlepartysetname(battlepartysetname, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013CE RID: 5070 RVA: 0x0006A078 File Offset: 0x00068478
		protected void OnResponse_BattlePartySetName(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Battlepartysetname battlepartysetname = PlayerResponse.Battlepartysetname(wwwParam, ResponseCommon.DialogType.None, null);
			if (battlepartysetname == null)
			{
				return;
			}
			ResultCode result = battlepartysetname.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.NG_WORD)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.NgWordError), result.ToMessageString(), null);
				}
			}
			else
			{
				UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[this.m_UI.SelectPartyIndex];
				userBattlePartyData.PartyName = this.m_RequestedPartyName;
				this.m_UI.CloseNameChangeWindow();
			}
		}
	}
}
