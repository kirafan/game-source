﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004FC RID: 1276
	public class MissionFuncRoom : IMissionFunction
	{
		// Token: 0x06001927 RID: 6439 RVA: 0x00082A84 File Offset: 0x00080E84
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			eXlsMissionRoomFuncType extensionScriptID = (eXlsMissionRoomFuncType)pparam.m_ExtensionScriptID;
			if (extensionScriptID != eXlsMissionRoomFuncType.ObjectSet)
			{
				if (extensionScriptID != eXlsMissionRoomFuncType.ObjectRemove)
				{
					if (extensionScriptID == eXlsMissionRoomFuncType.CharaTouch)
					{
						pparam.m_Rate++;
						result = (pparam.m_Rate >= pparam.m_RateBase);
					}
				}
				else
				{
					pparam.m_Rate++;
					result = (pparam.m_Rate >= pparam.m_RateBase);
				}
			}
			else
			{
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
			}
			return result;
		}
	}
}
