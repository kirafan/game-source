﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x020003FF RID: 1023
	public class EditState_CharaListView : EditState
	{
		// Token: 0x06001374 RID: 4980 RVA: 0x00067FB3 File Offset: 0x000663B3
		public EditState_CharaListView(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001375 RID: 4981 RVA: 0x00067FCB File Offset: 0x000663CB
		public override int GetStateID()
		{
			return 17;
		}

		// Token: 0x06001376 RID: 4982 RVA: 0x00067FCF File Offset: 0x000663CF
		public override void OnStateEnter()
		{
			this.m_Step = EditState_CharaListView.eStep.First;
		}

		// Token: 0x06001377 RID: 4983 RVA: 0x00067FD8 File Offset: 0x000663D8
		public override void OnStateExit()
		{
		}

		// Token: 0x06001378 RID: 4984 RVA: 0x00067FDA File Offset: 0x000663DA
		public override void OnDispose()
		{
		}

		// Token: 0x06001379 RID: 4985 RVA: 0x00067FDC File Offset: 0x000663DC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_CharaListView.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = EditState_CharaListView.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = EditState_CharaListView.eStep.LoadWait;
				}
				break;
			case EditState_CharaListView.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_CharaListView.eStep.PlayIn;
				}
				break;
			case EditState_CharaListView.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_CharaListView.eStep.Main;
				}
				break;
			case EditState_CharaListView.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					this.m_Step = EditState_CharaListView.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600137A RID: 4986 RVA: 0x000680F0 File Offset: 0x000664F0
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.m_Owner.CharaListUI.SetViewMode();
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x0600137B RID: 4987 RVA: 0x0006818A File Offset: 0x0006658A
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x0600137C RID: 4988 RVA: 0x00068199 File Offset: 0x00066599
		private void GoToMenuEnd()
		{
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x04001A64 RID: 6756
		private EditState_CharaListView.eStep m_Step = EditState_CharaListView.eStep.None;

		// Token: 0x04001A65 RID: 6757
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x02000400 RID: 1024
		private enum eStep
		{
			// Token: 0x04001A67 RID: 6759
			None = -1,
			// Token: 0x04001A68 RID: 6760
			First,
			// Token: 0x04001A69 RID: 6761
			LoadWait,
			// Token: 0x04001A6A RID: 6762
			PlayIn,
			// Token: 0x04001A6B RID: 6763
			Main
		}
	}
}
