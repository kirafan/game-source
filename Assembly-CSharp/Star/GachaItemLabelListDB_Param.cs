﻿using System;

namespace Star
{
	// Token: 0x020001C5 RID: 453
	[Serializable]
	public struct GachaItemLabelListDB_Param
	{
		// Token: 0x04000AE1 RID: 2785
		public int m_ItemID;

		// Token: 0x04000AE2 RID: 2786
		public string m_ResourcePath0;

		// Token: 0x04000AE3 RID: 2787
		public string m_ResourcePath1;
	}
}
