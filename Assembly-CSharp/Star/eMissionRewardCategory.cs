﻿using System;

namespace Star
{
	// Token: 0x02000501 RID: 1281
	public enum eMissionRewardCategory
	{
		// Token: 0x04001FEB RID: 8171
		Non,
		// Token: 0x04001FEC RID: 8172
		Money,
		// Token: 0x04001FED RID: 8173
		Item,
		// Token: 0x04001FEE RID: 8174
		KRRPoint,
		// Token: 0x04001FEF RID: 8175
		Stamina,
		// Token: 0x04001FF0 RID: 8176
		Friendship,
		// Token: 0x04001FF1 RID: 8177
		Gem
	}
}
