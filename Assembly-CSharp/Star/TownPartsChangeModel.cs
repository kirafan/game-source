﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006BD RID: 1725
	public class TownPartsChangeModel : ITownPartsAction
	{
		// Token: 0x06002253 RID: 8787 RVA: 0x000B689C File Offset: 0x000B4C9C
		public TownPartsChangeModel(GameObject pdelobj, ITownObjectHandler pbuild, int fnewobjid, long fmanageid)
		{
			this.m_Build = pbuild;
			this.m_ChangeResID = fnewobjid;
			this.m_ChangeManageID = fmanageid;
			this.m_DelObj = pdelobj;
			this.m_Active = true;
			this.m_MaxTime = 0f;
			this.m_Time = 0f;
			this.m_Step = TownPartsChangeModel.eStep.FadeOut;
		}

		// Token: 0x06002254 RID: 8788 RVA: 0x000B68F0 File Offset: 0x000B4CF0
		public override bool PartsUpdate()
		{
			TownPartsChangeModel.eStep step = this.m_Step;
			if (step != TownPartsChangeModel.eStep.FadeOut)
			{
				if (step == TownPartsChangeModel.eStep.ModelChange)
				{
					this.m_Active = false;
				}
			}
			else
			{
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Step = TownPartsChangeModel.eStep.ModelChange;
					this.m_Build.RecvEvent(new TownHandleActionModelObj(this.m_ChangeResID, this.m_ChangeManageID));
				}
			}
			return this.m_Active;
		}

		// Token: 0x040028F8 RID: 10488
		private float m_MaxTime;

		// Token: 0x040028F9 RID: 10489
		private float m_Time;

		// Token: 0x040028FA RID: 10490
		private TownPartsChangeModel.eStep m_Step;

		// Token: 0x040028FB RID: 10491
		private ITownObjectHandler m_Build;

		// Token: 0x040028FC RID: 10492
		public GameObject m_DelObj;

		// Token: 0x040028FD RID: 10493
		private long m_ChangeManageID;

		// Token: 0x040028FE RID: 10494
		private int m_ChangeResID;

		// Token: 0x020006BE RID: 1726
		public enum eStep
		{
			// Token: 0x04002900 RID: 10496
			FadeOut,
			// Token: 0x04002901 RID: 10497
			ModelChange
		}
	}
}
