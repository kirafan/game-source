﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000277 RID: 631
	public class CompareCharaViewerMain : MonoBehaviour
	{
		// Token: 0x06000BC7 RID: 3015 RVA: 0x00044EEC File Offset: 0x000432EC
		private void Start()
		{
			this.m_Step = CompareCharaViewerMain.eStep.First;
		}

		// Token: 0x06000BC8 RID: 3016 RVA: 0x00044EF8 File Offset: 0x000432F8
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			switch (this.m_Step)
			{
			case CompareCharaViewerMain.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					this.m_Step = CompareCharaViewerMain.eStep.Prepare;
				}
				break;
			case CompareCharaViewerMain.eStep.Prepare:
			{
				this.m_CharaHndls = new List<CharacterHandler>();
				DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
				for (int i = 0; i < this.m_CharaIDs.Length; i++)
				{
					int charaID = this.m_CharaIDs[i];
					CharacterListDB_Param param = dbMng.CharaListDB.GetParam(charaID);
					int defaultWeaponID = dbMng.ClassListDB.GetParam((eClassType)param.m_Class).m_DefaultWeaponID;
					CharacterParam characterParam = new CharacterParam();
					CharacterUtility.SetupCharacterParam(characterParam, 0L, param.m_CharaID, 1, 99, 0L, 0);
					WeaponParam weaponParam = new WeaponParam();
					CharacterUtility.SetupWeaponParam(weaponParam, 1L, defaultWeaponID, 1, 0L, 1, 0L);
					CharacterHandler characterHandler = SingletonMonoBehaviour<GameSystem>.Inst.CharaMng.InstantiateCharacter(characterParam, null, weaponParam, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Viewer, true, CharacterDefine.eFriendType.None, "CHARA_" + param.m_CharaID, null);
					if (characterHandler != null)
					{
						characterHandler.CacheTransform.position = new Vector3(0f, -10000f, 0f);
						characterHandler.SetEnableRenderWeapon(this.m_IsWeaponRender);
						characterHandler.Prepare();
						this.m_Step = CompareCharaViewerMain.eStep.Prepare_Wait;
						this.m_CharaHndls.Add(characterHandler);
					}
				}
				break;
			}
			case CompareCharaViewerMain.eStep.Prepare_Wait:
			{
				bool flag = false;
				for (int j = 0; j < this.m_CharaHndls.Count; j++)
				{
					if (this.m_CharaHndls[j].IsPreparing())
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					string[] array = null;
					for (int k = 0; k < this.m_CharaHndls.Count; k++)
					{
						this.m_CharaHndls[k].CharaAnim.ChangeDir(CharacterDefine.eDir.L, false);
						if (array == null)
						{
							array = this.m_CharaHndls[k].CharaResource.AnimKeyForViewer;
						}
						else
						{
							this.m_CharaHndls[k].CharaResource.AnimKeyForViewer = array;
						}
						int num = k % this.m_ColumnNum;
						int num2 = k / this.m_ColumnNum;
						int num3 = Mathf.Min(this.m_ColumnNum, this.m_CharaHndls.Count);
						float num4 = -((float)(num3 - 1) / 2f) * this.m_IntervalPos.x;
						float num5 = (float)(-(float)((this.m_CharaHndls.Count - 1) / this.m_ColumnNum)) * this.m_IntervalPos.y / 2f;
						this.m_CharaHndls[k].CacheTransform.position = new Vector3((float)num * this.m_IntervalPos.x + num4, (float)num2 * this.m_IntervalPos.y + num5, 0f);
					}
					this.m_AnimIndex = 0;
					this.ApplyAnim(true);
					this.m_Step = CompareCharaViewerMain.eStep.Main;
				}
				break;
			}
			}
		}

		// Token: 0x06000BC9 RID: 3017 RVA: 0x00045230 File Offset: 0x00043630
		private void ToggleAnimIndex(bool isRight)
		{
			CharacterHandler characterHandler = this.m_CharaHndls[0];
			if (characterHandler == null)
			{
				return;
			}
			if (characterHandler.CharaAnim == null)
			{
				return;
			}
			if (characterHandler.CharaResource == null)
			{
				return;
			}
			if (isRight)
			{
				this.m_AnimIndex++;
				if (this.m_AnimIndex >= characterHandler.CharaResource.AnimKeyForViewer.Length)
				{
					this.m_AnimIndex = 0;
				}
			}
			else
			{
				this.m_AnimIndex--;
				if (this.m_AnimIndex < 0)
				{
					this.m_AnimIndex = characterHandler.CharaResource.AnimKeyForViewer.Length - 1;
				}
			}
			this.ApplyAnim(true);
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x000452E0 File Offset: 0x000436E0
		private void ApplyAnim(bool isPlay = true)
		{
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_CharaHndls[i];
				if (!(characterHandler == null))
				{
					if (characterHandler.CharaAnim != null)
					{
						if (characterHandler.CharaResource != null)
						{
							if (characterHandler.CharaResource.AnimKeyForViewer.Length > this.m_AnimIndex)
							{
								this.m_AnimKeyText.text = characterHandler.CharaResource.AnimKeyForViewer[this.m_AnimIndex];
								this.m_AnimKey = this.m_AnimKeyText.text;
								if (isPlay)
								{
									characterHandler.CharaAnim.PlayAnim(this.m_AnimKey, WrapMode.Loop, 0f, 0f);
									string[] array = this.m_AnimKey.Split(new char[]
									{
										'_'
									});
									if (array[array.Length - 1] == "R")
									{
										characterHandler.CharaAnim.ChangeDir(CharacterDefine.eDir.R, false);
									}
									else
									{
										characterHandler.CharaAnim.ChangeDir(CharacterDefine.eDir.L, false);
									}
									bool flag = false;
									if (!this.m_IsForceFacial)
									{
										for (int j = 0; j < this.BLINK_ANIM_KEY_LIST.Length; j++)
										{
											if (this.BLINK_ANIM_KEY_LIST[j] == this.m_AnimKey)
											{
												flag = true;
												break;
											}
										}
										characterHandler.CharaAnim.SetBlink(flag);
										if (!flag && this.m_AnimKey == "abnormal")
										{
											characterHandler.CharaAnim.SetFacial(CharacterDefine.FACIAL_ID_ABNORMAL_BATTLE[(int)characterHandler.CharaAnim.Dir]);
										}
									}
									Debug.LogFormat("Play:{0}, isBlink:{1}", new object[]
									{
										this.m_AnimKey,
										flag
									});
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x000454AF File Offset: 0x000438AF
		public void OnDecideAnim_L()
		{
			if (this.m_Step != CompareCharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(false);
		}

		// Token: 0x06000BCC RID: 3020 RVA: 0x000454C5 File Offset: 0x000438C5
		public void OnDecideAnim_R()
		{
			if (this.m_Step != CompareCharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleAnimIndex(true);
		}

		// Token: 0x06000BCD RID: 3021 RVA: 0x000454DC File Offset: 0x000438DC
		private void ToggleFacialIndex(bool isRight)
		{
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_CharaHndls[i];
				if (characterHandler == null)
				{
					return;
				}
				if (characterHandler.CharaAnim == null)
				{
					return;
				}
				if (characterHandler.CharaResource == null)
				{
					return;
				}
				if (!this.m_IsForceFacial)
				{
					return;
				}
			}
			if (isRight)
			{
				this.m_FacialIndex++;
				if (this.m_FacialIndex >= 100)
				{
					this.m_FacialIndex = 0;
				}
			}
			else
			{
				this.m_FacialIndex--;
				if (this.m_FacialIndex < 0)
				{
					this.m_FacialIndex = 99;
				}
			}
			this.ApplyFacial();
		}

		// Token: 0x06000BCE RID: 3022 RVA: 0x00045598 File Offset: 0x00043998
		private void ApplyFacial()
		{
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_CharaHndls[i];
				if (characterHandler == null)
				{
					return;
				}
				if (characterHandler.CharaAnim == null)
				{
					return;
				}
				if (characterHandler.CharaResource == null)
				{
					return;
				}
				if (!this.m_IsForceFacial)
				{
					return;
				}
				this.m_FacialIDText.text = this.m_FacialIndex.ToString();
				characterHandler.CharaAnim.SetFacial(this.m_FacialIndex);
				characterHandler.CharaAnim.SetBlink(false);
			}
		}

		// Token: 0x06000BCF RID: 3023 RVA: 0x00045638 File Offset: 0x00043A38
		public void OnDecideFacial_L()
		{
			if (this.m_Step != CompareCharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleFacialIndex(false);
		}

		// Token: 0x06000BD0 RID: 3024 RVA: 0x0004564E File Offset: 0x00043A4E
		public void OnDecideFacial_R()
		{
			if (this.m_Step != CompareCharaViewerMain.eStep.Main)
			{
				return;
			}
			this.ToggleFacialIndex(true);
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x00045664 File Offset: 0x00043A64
		public void OnToggleForceFacial(bool value)
		{
			this.m_IsForceFacial = this.m_ToggleForceFacial.isOn;
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				CharacterHandler characterHandler = this.m_CharaHndls[i];
				if (!(characterHandler != null) || characterHandler.CharaAnim == null || characterHandler.CharaResource != null)
				{
				}
			}
			if (this.m_IsForceFacial)
			{
				this.ApplyFacial();
			}
			else
			{
				this.ApplyAnim(true);
			}
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x000456EC File Offset: 0x00043AEC
		public void OnToggleWeapon(bool value)
		{
			this.m_IsWeaponRender = this.m_ToggleWeapon.isOn;
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				this.m_CharaHndls[i].SetEnableRenderWeapon(this.m_IsWeaponRender);
			}
		}

		// Token: 0x06000BD3 RID: 3027 RVA: 0x0004573D File Offset: 0x00043B3D
		public void OnToggleBG(bool value)
		{
			this.m_BG.SetActive(this.m_ToggleBG.isOn);
		}

		// Token: 0x06000BD4 RID: 3028 RVA: 0x00045755 File Offset: 0x00043B55
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * value;
		}

		// Token: 0x04001436 RID: 5174
		[SerializeField]
		private Vector2 m_IntervalPos = new Vector2(0.25f, 1f);

		// Token: 0x04001437 RID: 5175
		[SerializeField]
		private int m_ColumnNum = 5;

		// Token: 0x04001438 RID: 5176
		[SerializeField]
		private int[] m_CharaIDs;

		// Token: 0x04001439 RID: 5177
		[SerializeField]
		private Text m_AnimKeyText;

		// Token: 0x0400143A RID: 5178
		[SerializeField]
		private Text m_FacialIDText;

		// Token: 0x0400143B RID: 5179
		[SerializeField]
		private Toggle m_ToggleForceFacial;

		// Token: 0x0400143C RID: 5180
		[SerializeField]
		private Toggle m_ToggleWeapon;

		// Token: 0x0400143D RID: 5181
		[SerializeField]
		private Toggle m_ToggleBG;

		// Token: 0x0400143E RID: 5182
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x0400143F RID: 5183
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x04001440 RID: 5184
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x04001441 RID: 5185
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x04001442 RID: 5186
		private readonly string[] BLINK_ANIM_KEY_LIST = new string[]
		{
			"idle",
			"room_idle_L",
			"room_idle_R",
			"room_idle_skirt_L",
			"room_idle_skirt_R",
			"room_walk_L",
			"room_walk_R",
			"room_sit_st_L",
			"room_sit_lp_L",
			"room_sit_ed_L",
			"room_sit_st_R",
			"room_sit_lp_R",
			"room_sit_ed_R"
		};

		// Token: 0x04001443 RID: 5187
		private List<CharacterHandler> m_CharaHndls;

		// Token: 0x04001444 RID: 5188
		private int m_AnimIndex;

		// Token: 0x04001445 RID: 5189
		private bool m_IsWeaponRender = true;

		// Token: 0x04001446 RID: 5190
		private string m_AnimKey;

		// Token: 0x04001447 RID: 5191
		private bool m_IsForceFacial;

		// Token: 0x04001448 RID: 5192
		private int m_FacialIndex;

		// Token: 0x04001449 RID: 5193
		private CompareCharaViewerMain.eStep m_Step = CompareCharaViewerMain.eStep.None;

		// Token: 0x02000278 RID: 632
		private enum eStep
		{
			// Token: 0x0400144B RID: 5195
			None = -1,
			// Token: 0x0400144C RID: 5196
			First,
			// Token: 0x0400144D RID: 5197
			Prepare,
			// Token: 0x0400144E RID: 5198
			Prepare_Wait,
			// Token: 0x0400144F RID: 5199
			Main,
			// Token: 0x04001450 RID: 5200
			Destroy_0,
			// Token: 0x04001451 RID: 5201
			Destroy_1
		}
	}
}
