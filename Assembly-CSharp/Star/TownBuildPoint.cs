﻿using System;
using Star.UI.Town;
using UnityEngine;

namespace Star
{
	// Token: 0x020006FA RID: 1786
	public class TownBuildPoint : MonoBehaviour
	{
		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06002362 RID: 9058 RVA: 0x000BE24D File Offset: 0x000BC64D
		public int Index
		{
			get
			{
				return this.m_Index;
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06002363 RID: 9059 RVA: 0x000BE255 File Offset: 0x000BC655
		public int OrderInLayer
		{
			get
			{
				return this.m_OrderInLayer;
			}
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x000BE25D File Offset: 0x000BC65D
		private void Awake()
		{
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x000BE25F File Offset: 0x000BC65F
		private void Start()
		{
			this.SetEnable(false);
		}

		// Token: 0x06002366 RID: 9062 RVA: 0x000BE268 File Offset: 0x000BC668
		public void SetEnable(bool flg)
		{
			if (flg)
			{
				this.m_Sprite.enabled = true;
				this.m_Arrow.SetActive(true);
				iTween.ValueTo(base.gameObject, iTween.Hash(new object[]
				{
					"from",
					0f,
					"to",
					1f,
					"time",
					0.75f,
					"loopType",
					iTween.LoopType.pingPong,
					"onupdate",
					"SetEnableColor"
				}));
			}
			else
			{
				this.m_Sprite.enabled = false;
				this.m_Arrow.SetActive(false);
				iTween.Stop(base.gameObject);
			}
		}

		// Token: 0x06002367 RID: 9063 RVA: 0x000BE333 File Offset: 0x000BC733
		private void SetEnableColor(float value)
		{
			this.m_Sprite.color = Color.Lerp(new Color(1f, 0f, 1f), new Color(0.7f, 0f, 0.7f), value);
		}

		// Token: 0x04002A2F RID: 10799
		[SerializeField]
		private SpriteRenderer m_Sprite;

		// Token: 0x04002A30 RID: 10800
		[SerializeField]
		private TownBuildArrow m_Arrow;

		// Token: 0x04002A31 RID: 10801
		[SerializeField]
		private Collider m_Collider;

		// Token: 0x04002A32 RID: 10802
		[SerializeField]
		private int m_Index;

		// Token: 0x04002A33 RID: 10803
		[SerializeField]
		private int m_OrderInLayer;
	}
}
