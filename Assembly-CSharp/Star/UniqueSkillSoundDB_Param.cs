﻿using System;

namespace Star
{
	// Token: 0x0200025E RID: 606
	[Serializable]
	public struct UniqueSkillSoundDB_Param
	{
		// Token: 0x040013BC RID: 5052
		public string m_CueSheet;

		// Token: 0x040013BD RID: 5053
		public UniqueSkillSoundDB_Data[] m_Datas;
	}
}
