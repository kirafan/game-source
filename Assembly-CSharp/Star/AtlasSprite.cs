﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Star
{
	// Token: 0x02000002 RID: 2
	public class AtlasSprite
	{
		// Token: 0x06000002 RID: 2 RVA: 0x00002058 File Offset: 0x00000458
		public void LoadAtlas(string path)
		{
			Sprite[] array = Resources.LoadAll<Sprite>(path);
			this.m_SpriteDict = new Dictionary<string, Sprite>();
			foreach (Sprite sprite in array)
			{
				this.m_SpriteDict.Add(Path.GetFileNameWithoutExtension(sprite.name), sprite);
			}
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000020A8 File Offset: 0x000004A8
		public void Dispose()
		{
			foreach (string key in this.m_SpriteDict.Keys)
			{
				this.m_SpriteDict[key] = null;
			}
			this.m_SpriteDict.Clear();
			this.m_SpriteDict = null;
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002124 File Offset: 0x00000524
		public Sprite GetSprite(string imgName)
		{
			if (this.m_SpriteDict == null)
			{
				return null;
			}
			if (!this.m_SpriteDict.ContainsKey(imgName))
			{
				return null;
			}
			return this.m_SpriteDict[imgName];
		}

		// Token: 0x04000001 RID: 1
		private Dictionary<string, Sprite> m_SpriteDict;
	}
}
