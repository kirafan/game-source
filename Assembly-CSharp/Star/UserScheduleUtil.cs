﻿using System;

namespace Star
{
	// Token: 0x020003AA RID: 938
	public class UserScheduleUtil
	{
		// Token: 0x060011C9 RID: 4553 RVA: 0x0005DCC4 File Offset: 0x0005C0C4
		public static int MoveToScheduleStateUp(UserScheduleData.ListPack pschedule, eTownMoveState fstate, int fpoint, long fmovemngid)
		{
			int num = -1;
			UserScheduleData.Seg table = pschedule.GetTable(fpoint);
			int result = table.m_TagNameID;
			switch (fstate)
			{
			case eTownMoveState.None:
			case eTownMoveState.NonPointMove:
			case eTownMoveState.SetUp:
				num = table.m_TagNameID;
				result = (table.m_TagNameID = 1);
				table.m_Type = UserScheduleData.eType.Room;
				break;
			case eTownMoveState.NearmissMove:
			{
				num = table.m_TagNameID;
				UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmovemngid);
				TownObjectListDB_Param townResouceData = UserTownUtil.GetTownResouceData(townBuildData.m_ObjID);
				result = (table.m_TagNameID = ScheduleDataBase.GetScheduleSetUpDB().ChangeFailTag(num, (eTitleType)townResouceData.m_TitleType));
				break;
			}
			}
			if (num >= 0 && fpoint < pschedule.GetListNum() - 1)
			{
				table = pschedule.GetTable(fpoint + 1);
				if (ScheduleNameUtil.GetScheduleNameTag(table.m_TagNameID).m_LiinkNo == num)
				{
					table.m_TagNameID = 1;
					table.m_Type = UserScheduleData.eType.Room;
				}
			}
			return result;
		}

		// Token: 0x060011CA RID: 4554 RVA: 0x0005DDAC File Offset: 0x0005C1AC
		public static void TimeToScheduleStateUp(UserScheduleData.ListPack pschedule, eTownMoveState fstate, long fsec)
		{
			int listNum = pschedule.GetListNum();
			fsec = (long)ScheduleTimeUtil.GetDayTimeSec(fsec);
			for (int i = 0; i < listNum; i++)
			{
				UserScheduleData.Seg table = pschedule.GetTable(i);
				if ((long)table.m_WakeTime <= fsec && (long)(table.m_WakeTime + table.m_UseTime) > fsec)
				{
					UserScheduleUtil.MoveToScheduleStateUp(pschedule, fstate, i, TownDefine.ROOM_ACCESS_MNG_ID);
					break;
				}
			}
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x0005DE18 File Offset: 0x0005C218
		public static bool CheckTagToBuilding(UserScheduleData.ListPack pschedule, int fpoint)
		{
			int num = -1;
			UserScheduleData.Seg table = pschedule.GetTable(fpoint);
			bool flag = false;
			switch (table.m_Type)
			{
			case UserScheduleData.eType.Office:
			{
				TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				flag = true;
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && buildObjectDataAt.m_IsOpen && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 536870912)
					{
						buildCheckDataBase.SetBuildParam(buildObjectDataAt.m_ManageID);
						if (TownAccessCheck.IsAccessSearchBuild(ref buildCheckDataBase, table.m_TagNameID))
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					num = table.m_TagNameID;
					table.m_TagNameID = 1;
					table.m_Type = UserScheduleData.eType.Room;
				}
				break;
			}
			case UserScheduleData.eType.Town:
			{
				TownAccessCheck.BuildCheckDataBase buildCheckDataBase = default(TownAccessCheck.BuildCheckDataBase);
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				flag = true;
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && buildObjectDataAt.m_IsOpen && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 1073741824)
					{
						buildCheckDataBase.SetBuildParam(buildObjectDataAt.m_ManageID);
						if (TownAccessCheck.IsAccessSearchBuild(ref buildCheckDataBase, table.m_TagNameID))
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					num = table.m_TagNameID;
					table.m_TagNameID = 1;
					table.m_Type = UserScheduleData.eType.Room;
				}
				break;
			}
			}
			if (num >= 0 && fpoint < pschedule.GetListNum() - 1)
			{
				table = pschedule.GetTable(fpoint + 1);
				if (ScheduleNameUtil.GetScheduleNameTag(table.m_TagNameID).m_LiinkNo == num)
				{
					table.m_TagNameID = 1;
					table.m_Type = UserScheduleData.eType.Room;
				}
			}
			return flag;
		}
	}
}
