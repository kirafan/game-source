﻿using System;

namespace Star
{
	// Token: 0x0200055A RID: 1370
	public class CActScriptKeyCharaCfg : IRoomScriptData
	{
		// Token: 0x06001AE3 RID: 6883 RVA: 0x0008F1E0 File Offset: 0x0008D5E0
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyCharaCfg actXlsKeyCharaCfg = (ActXlsKeyCharaCfg)pbase;
			this.m_OffsetLayer = actXlsKeyCharaCfg.m_OffsetLayer;
			this.m_OffsetZ = actXlsKeyCharaCfg.m_OffsetZ;
		}

		// Token: 0x040021BE RID: 8638
		public int m_OffsetLayer;

		// Token: 0x040021BF RID: 8639
		public float m_OffsetZ;
	}
}
