﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.Profiling;

namespace Star
{
	// Token: 0x020004E1 RID: 1249
	public class ProfileStatus
	{
		// Token: 0x060018E1 RID: 6369 RVA: 0x000815E4 File Offset: 0x0007F9E4
		public void Profile()
		{
			int num = GC.CollectionCount(0);
			if (this.m_LastCollectNum != (float)num)
			{
				this.m_LastCollectNum = (float)num;
				this.m_CollectDeltaTime = Time.realtimeSinceStartup - this.m_LastCollectTime;
				this.m_LastCollectTime = Time.realtimeSinceStartup;
				this.m_LastCollectDeltaTime = Time.deltaTime;
			}
			this.m_UsedMemSize = GC.GetTotalMemory(false);
			if (this.m_UsedMemSize > this.m_MaxUsedMemSize)
			{
				this.m_MaxUsedMemSize = this.m_UsedMemSize;
			}
			if (Time.realtimeSinceStartup - this.m_LastAllocSet > 0.3f)
			{
				long num2 = this.m_UsedMemSize - this.m_LastAllocMemSize;
				this.m_LastAllocMemSize = this.m_UsedMemSize;
				this.m_LastAllocSet = Time.realtimeSinceStartup;
				if (num2 >= 0L)
				{
					this.m_AllocRate = num2;
				}
			}
			this.UpdateText();
		}

		// Token: 0x060018E2 RID: 6370 RVA: 0x000816AE File Offset: 0x0007FAAE
		public string GetText()
		{
			return this.m_SB.ToString();
		}

		// Token: 0x060018E3 RID: 6371 RVA: 0x000816BC File Offset: 0x0007FABC
		private void UpdateText()
		{
			this.m_SB.Length = 0;
			this.m_SB.Append((1f / Time.deltaTime).ToString("0.000")).Append(" FPS").AppendLine();
			this.m_SB.Append("Used Mem             ").Append(((float)this.m_UsedMemSize / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Max Used Mem         ").Append(((float)this.m_MaxUsedMemSize / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Used Prog Heap       ").Append((Profiler.usedHeapSize / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Mono Used            ").Append((Profiler.GetMonoUsedSize() / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Mono Heap            ").Append((Profiler.GetMonoHeapSize() / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Total Alloc Mem      ").Append((Profiler.GetTotalAllocatedMemory() / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("System Memory        ").Append(SystemInfo.systemMemorySize.ToString()).Append(" MB").AppendLine();
			this.m_SB.Append("Total Reserved Mem   ").Append((Profiler.GetTotalReservedMemory() / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Total Unused Res-Mem ").Append((Profiler.GetTotalUnusedReservedMemory() / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Graphic Memory       ").Append(SystemInfo.graphicsMemorySize.ToString()).Append(" MB").AppendLine();
			this.m_SB.Append("Allocation rate      ").Append(((float)this.m_AllocRate / 1048576f).ToString("0.00")).Append(" MB").AppendLine();
			this.m_SB.Append("Collection frequency ").Append(this.m_CollectDeltaTime.ToString("0.00")).Append(" s").AppendLine();
			this.m_SB.Append("Last collect delta   ").Append(this.m_LastCollectDeltaTime.ToString("0.000")).Append(" s (").Append((1f / this.m_LastCollectDeltaTime).ToString("0.0")).Append(" fps)").AppendLine();
		}

		// Token: 0x04001F63 RID: 8035
		public const int SIZE_KB = 1024;

		// Token: 0x04001F64 RID: 8036
		public const int SIZE_MB = 1048576;

		// Token: 0x04001F65 RID: 8037
		private long m_UsedMemSize;

		// Token: 0x04001F66 RID: 8038
		private long m_MaxUsedMemSize;

		// Token: 0x04001F67 RID: 8039
		private float m_LastCollectTime;

		// Token: 0x04001F68 RID: 8040
		private float m_LastCollectNum;

		// Token: 0x04001F69 RID: 8041
		private float m_CollectDeltaTime;

		// Token: 0x04001F6A RID: 8042
		private float m_LastCollectDeltaTime;

		// Token: 0x04001F6B RID: 8043
		private long m_AllocRate;

		// Token: 0x04001F6C RID: 8044
		private long m_LastAllocMemSize;

		// Token: 0x04001F6D RID: 8045
		private float m_LastAllocSet = -9999f;

		// Token: 0x04001F6E RID: 8046
		private StringBuilder m_SB = new StringBuilder();
	}
}
