﻿using System;

namespace Star
{
	// Token: 0x02000206 RID: 518
	public enum eSkillContentType
	{
		// Token: 0x04000CB9 RID: 3257
		Attack,
		// Token: 0x04000CBA RID: 3258
		Recover,
		// Token: 0x04000CBB RID: 3259
		StatusChange,
		// Token: 0x04000CBC RID: 3260
		StatusChangeReset,
		// Token: 0x04000CBD RID: 3261
		Abnormal,
		// Token: 0x04000CBE RID: 3262
		AbnormalRecover,
		// Token: 0x04000CBF RID: 3263
		AbnormalDisable,
		// Token: 0x04000CC0 RID: 3264
		AbnormalAdditionalProbability,
		// Token: 0x04000CC1 RID: 3265
		ElementResist,
		// Token: 0x04000CC2 RID: 3266
		ElementChange,
		// Token: 0x04000CC3 RID: 3267
		WeakElementBonus,
		// Token: 0x04000CC4 RID: 3268
		NextAttackUp,
		// Token: 0x04000CC5 RID: 3269
		NextAttackCritical,
		// Token: 0x04000CC6 RID: 3270
		Barrier,
		// Token: 0x04000CC7 RID: 3271
		RecastChange,
		// Token: 0x04000CC8 RID: 3272
		KiraraJumpGaugeChange,
		// Token: 0x04000CC9 RID: 3273
		KiraraJumpGaugeCoef,
		// Token: 0x04000CCA RID: 3274
		OrderChange,
		// Token: 0x04000CCB RID: 3275
		HateChange,
		// Token: 0x04000CCC RID: 3276
		ChargeChange,
		// Token: 0x04000CCD RID: 3277
		ChainCoefChange,
		// Token: 0x04000CCE RID: 3278
		Card,
		// Token: 0x04000CCF RID: 3279
		StunRecover,
		// Token: 0x04000CD0 RID: 3280
		Regene,
		// Token: 0x04000CD1 RID: 3281
		Num
	}
}
