﻿using System;

namespace Star
{
	// Token: 0x02000657 RID: 1623
	public class LoginBonusPresent
	{
		// Token: 0x0400270A RID: 9994
		public ePresentType m_PresentType;

		// Token: 0x0400270B RID: 9995
		public int m_PresentID;

		// Token: 0x0400270C RID: 9996
		public int m_Amount;
	}
}
