﻿using System;

namespace Star
{
	// Token: 0x020001FB RID: 507
	[Serializable]
	public struct SceneInfoListDB_Param
	{
		// Token: 0x04000C4A RID: 3146
		public int m_SceneID;

		// Token: 0x04000C4B RID: 3147
		public string m_ResourceName;

		// Token: 0x04000C4C RID: 3148
		public string m_SceneName;
	}
}
