﻿using System;

namespace Star
{
	// Token: 0x020002D9 RID: 729
	public abstract class CharacterEditSceneAfterProcessCharacterController : CharacterDataWrapperControllerBase
	{
		// Token: 0x06000E40 RID: 3648 RVA: 0x0004DA91 File Offset: 0x0004BE91
		public CharacterEditSceneAfterProcessCharacterController() : base(eCharacterDataControllerType.UnmanagedCharacterDataWrapper)
		{
		}

		// Token: 0x06000E41 RID: 3649
		public abstract void Refresh();
	}
}
