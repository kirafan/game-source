﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000703 RID: 1795
	public class TownBuildUpFieldParam : ITownEventCommand
	{
		// Token: 0x06002381 RID: 9089 RVA: 0x000BE7BC File Offset: 0x000BCBBC
		public TownBuildUpFieldParam(int fieldno, byte ftimeid, TownBuilder pbuilder)
		{
			GameObject gameObject = new GameObject();
			this.m_FieldNo = fieldno;
			this.m_TimeID = ftimeid;
			this.m_TownField = ITownObjectHandler.CreateHandler(gameObject, TownUtility.eResCategory.Field, pbuilder);
			this.m_TownField.SetupHandle(fieldno, 0, -1L, -5000, ftimeid, true);
			this.m_TownField.Prepare();
			this.m_TownField.CacheTransform.SetParent(null, false);
			gameObject.SetActive(true);
			this.m_Enable = true;
		}

		// Token: 0x06002382 RID: 9090 RVA: 0x000BE832 File Offset: 0x000BCC32
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (this.m_TownField.IsAvailable())
			{
				pbuilder.SetFieldMap(this.m_TownField);
				this.m_TownField = null;
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002A54 RID: 10836
		public int m_FieldNo;

		// Token: 0x04002A55 RID: 10837
		public byte m_TimeID;

		// Token: 0x04002A56 RID: 10838
		private ITownObjectHandler m_TownField;

		// Token: 0x02000704 RID: 1796
		public enum eStep
		{
			// Token: 0x04002A58 RID: 10840
			BuildQue,
			// Token: 0x04002A59 RID: 10841
			SetUpCheck
		}
	}
}
