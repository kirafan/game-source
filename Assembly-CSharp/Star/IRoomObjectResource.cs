﻿using System;

namespace Star
{
	// Token: 0x020005DA RID: 1498
	public class IRoomObjectResource
	{
		// Token: 0x06001D74 RID: 7540 RVA: 0x0009DF0D File Offset: 0x0009C30D
		public bool IsPreparing()
		{
			return this.m_IsPreparing;
		}

		// Token: 0x06001D75 RID: 7541 RVA: 0x0009DF15 File Offset: 0x0009C315
		public virtual void Setup(IRoomObjectControll hndl)
		{
		}

		// Token: 0x06001D76 RID: 7542 RVA: 0x0009DF17 File Offset: 0x0009C317
		public virtual void Prepare()
		{
			this.m_IsPreparing = true;
		}

		// Token: 0x06001D77 RID: 7543 RVA: 0x0009DF20 File Offset: 0x0009C320
		public virtual void UpdateRes()
		{
		}

		// Token: 0x06001D78 RID: 7544 RVA: 0x0009DF22 File Offset: 0x0009C322
		public virtual void Destroy()
		{
		}

		// Token: 0x040023E3 RID: 9187
		protected bool m_IsPreparing;
	}
}
