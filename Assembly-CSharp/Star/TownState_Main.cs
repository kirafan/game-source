﻿using System;
using Star.UI.Global;

namespace Star
{
	// Token: 0x020004C1 RID: 1217
	public class TownState_Main : TownState
	{
		// Token: 0x060017EA RID: 6122 RVA: 0x0007CB08 File Offset: 0x0007AF08
		public TownState_Main(TownMain owner) : base(owner)
		{
		}

		// Token: 0x060017EB RID: 6123 RVA: 0x0007CB18 File Offset: 0x0007AF18
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x060017EC RID: 6124 RVA: 0x0007CB1B File Offset: 0x0007AF1B
		public override void OnStateEnter()
		{
			this.m_Step = TownState_Main.eStep.First;
		}

		// Token: 0x060017ED RID: 6125 RVA: 0x0007CB24 File Offset: 0x0007AF24
		public override void OnStateExit()
		{
			this.m_Owner.DestroyReq();
		}

		// Token: 0x060017EE RID: 6126 RVA: 0x0007CB31 File Offset: 0x0007AF31
		public override void OnDispose()
		{
		}

		// Token: 0x060017EF RID: 6127 RVA: 0x0007CB34 File Offset: 0x0007AF34
		public override int OnStateUpdate()
		{
			TownState_Main.eStep step = this.m_Step;
			if (step != TownState_Main.eStep.First)
			{
				if (step == TownState_Main.eStep.Main)
				{
					if (this.m_Owner.TownUI.IsMenuEnd())
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
						{
							this.m_NextState = 2147483646;
						}
						else
						{
							switch (this.m_Owner.Transit)
							{
							case TownMain.eTransit.Home:
								this.m_NextState = 2;
								break;
							case TownMain.eTransit.Quest:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Quest;
								break;
							case TownMain.eTransit.Edit:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Edit;
								break;
							case TownMain.eTransit.Room:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
								break;
							case TownMain.eTransit.Gacha:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Gacha;
								break;
							case TownMain.eTransit.Shop:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Shop;
								break;
							case TownMain.eTransit.Training:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Training;
								break;
							case TownMain.eTransit.ADV:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
								break;
							case TownMain.eTransit.Present:
								this.m_NextState = 2147483646;
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Present;
								break;
							}
						}
						return this.m_NextState;
					}
					this.m_Owner.UpdateEditing();
				}
			}
			else
			{
				this.m_Owner.StartMode(TownMain.eMode.Town);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
				this.m_Owner.SetTransitCallBack(new Action(this.OnTransitCallBack));
				this.m_Step = TownState_Main.eStep.Main;
			}
			return -1;
		}

		// Token: 0x060017F0 RID: 6128 RVA: 0x0007CD28 File Offset: 0x0007B128
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Home)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
				this.m_Owner.Transit = TownMain.eTransit.Home;
				this.GoToMenuEnd();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				if (!isCallFromShortCut)
				{
					this.m_Owner.Transit = TownMain.eTransit.Home;
				}
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060017F1 RID: 6129 RVA: 0x0007CD95 File Offset: 0x0007B195
		public void OnTransitCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060017F2 RID: 6130 RVA: 0x0007CD9D File Offset: 0x0007B19D
		private void GoToMenuEnd()
		{
			this.m_Owner.EndMode(this.m_Owner.Transit == TownMain.eTransit.Home);
		}

		// Token: 0x04001EBB RID: 7867
		private TownState_Main.eStep m_Step = TownState_Main.eStep.None;

		// Token: 0x020004C2 RID: 1218
		private enum eStep
		{
			// Token: 0x04001EBD RID: 7869
			None = -1,
			// Token: 0x04001EBE RID: 7870
			First,
			// Token: 0x04001EBF RID: 7871
			Main,
			// Token: 0x04001EC0 RID: 7872
			End
		}
	}
}
