﻿using System;

namespace Star
{
	// Token: 0x02000394 RID: 916
	public class ScheduleDataBase
	{
		// Token: 0x06001141 RID: 4417 RVA: 0x0005A74C File Offset: 0x00058B4C
		public static void DatabaseSetUp()
		{
			if (ScheduleDataBase.ms_ScheduleDataBase == null)
			{
				ScheduleDataBase.ms_ScheduleDataBase = new ScheduleSetUpDataBase();
				ScheduleDataBase.ms_ScheduleDataBase.LoadResInManager("Prefab/FldDB/ScheduleSetUpDatabase", ".asset");
				ScheduleDataBase.ms_ScheduleHolyday = new ScheduleHolydayCheck();
				ScheduleDataBase.ms_ScheduleHolyday.LoadResInManager("Prefab/FldDB/ScheduleHolyDay", ".asset");
			}
		}

		// Token: 0x06001142 RID: 4418 RVA: 0x0005A79F File Offset: 0x00058B9F
		public static bool IsSetUp()
		{
			return ScheduleDataBase.ms_ScheduleDataBase.m_Active && ScheduleDataBase.ms_ScheduleHolyday.m_Active;
		}

		// Token: 0x06001143 RID: 4419 RVA: 0x0005A7BD File Offset: 0x00058BBD
		public static void DatabaseRelease()
		{
			if (ScheduleDataBase.ms_ScheduleDataBase != null)
			{
				ScheduleDataBase.ms_ScheduleDataBase.Release();
			}
			ScheduleDataBase.ms_ScheduleDataBase = null;
			if (ScheduleDataBase.ms_ScheduleHolyday != null)
			{
				ScheduleDataBase.ms_ScheduleHolyday.Release();
			}
			ScheduleDataBase.ms_ScheduleHolyday = null;
		}

		// Token: 0x06001144 RID: 4420 RVA: 0x0005A7F3 File Offset: 0x00058BF3
		public static ScheduleSetUpDataBase GetScheduleSetUpDB()
		{
			return ScheduleDataBase.ms_ScheduleDataBase;
		}

		// Token: 0x06001145 RID: 4421 RVA: 0x0005A7FA File Offset: 0x00058BFA
		public static ScheduleHolydayCheck GetScheduleHolyday()
		{
			return ScheduleDataBase.ms_ScheduleHolyday;
		}

		// Token: 0x04001819 RID: 6169
		public static ScheduleSetUpDataBase ms_ScheduleDataBase;

		// Token: 0x0400181A RID: 6170
		public static ScheduleHolydayCheck ms_ScheduleHolyday;
	}
}
