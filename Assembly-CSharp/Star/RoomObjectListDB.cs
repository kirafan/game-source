﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001F6 RID: 502
	public class RoomObjectListDB : ScriptableObject
	{
		// Token: 0x04000C26 RID: 3110
		public RoomObjectListDB_Param[] m_Params;
	}
}
