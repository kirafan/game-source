﻿using System;
using Star.UI;
using UnityEngine;

namespace Star
{
	// Token: 0x02000084 RID: 132
	[RequireComponent(typeof(AnimUIPlayer))]
	[AddComponentMenu("ActionAggregation/ChildChildAnimUIPlayer")]
	[Serializable]
	public class ActionAggregationChildAnimUIPlayerActionAdapter : ActionAggregationChildActionAdapterExGen<AnimUIPlayer>
	{
		// Token: 0x06000411 RID: 1041 RVA: 0x00013FED File Offset: 0x000123ED
		public override void Update()
		{
			if (base.GetState() == eActionAggregationState.Play && this.m_Action.State == 2)
			{
				base.SetState(eActionAggregationState.Finish);
			}
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x00014013 File Offset: 0x00012413
		public override void PlayStartExtendProcess()
		{
			this.m_Action.PlayIn();
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x00014020 File Offset: 0x00012420
		public override void Skip()
		{
			this.m_Action.Show();
			base.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x00014034 File Offset: 0x00012434
		public override void RecoveryFinishToWaitExtendProcess()
		{
			this.m_Action.Hide();
		}
	}
}
