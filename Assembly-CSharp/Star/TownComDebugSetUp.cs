﻿using System;
using TownResponseTypes;

namespace Star
{
	// Token: 0x0200067F RID: 1663
	public class TownComDebugSetUp : IFldNetComModule
	{
		// Token: 0x060021A0 RID: 8608 RVA: 0x000B3038 File Offset: 0x000B1438
		public TownComDebugSetUp(int fmode = 0) : base(IFldNetComManager.Instance)
		{
		}

		// Token: 0x060021A1 RID: 8609 RVA: 0x000B3045 File Offset: 0x000B1445
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x060021A2 RID: 8610 RVA: 0x000B3054 File Offset: 0x000B1454
		public bool DefaultBuildUp()
		{
			UserTownData townData = UserDataUtil.TownData;
			if (townData.DebugMngID == -1L)
			{
				TownComAPINewSet townComAPINewSet = new TownComAPINewSet();
				townComAPINewSet.gridData = UserTownUtil.CreateTownDebugPlayString();
				this.m_NetMng.SendCom(townComAPINewSet, new INetComHandle.ResponseCallbak(this.CallbackDefaultBuildUp), false);
			}
			else
			{
				TownComAPISet townComAPISet = new TownComAPISet();
				townComAPISet.managedTownId = townData.DebugMngID;
				townComAPISet.gridData = UserTownUtil.CreateTownDebugPlayString();
				this.m_NetMng.SendCom(townComAPISet, new INetComHandle.ResponseCallbak(this.CallbackDefaultBuildUp), false);
			}
			return true;
		}

		// Token: 0x060021A3 RID: 8611 RVA: 0x000B30DC File Offset: 0x000B14DC
		public override bool UpFunc()
		{
			bool result = true;
			TownComDebugSetUp.eStep step = this.m_Step;
			if (step != TownComDebugSetUp.eStep.GetState)
			{
				if (step != TownComDebugSetUp.eStep.WaitCheck)
				{
					if (step == TownComDebugSetUp.eStep.End)
					{
						result = false;
					}
				}
			}
			else
			{
				this.m_Step = TownComDebugSetUp.eStep.WaitCheck;
				this.DefaultBuildUp();
			}
			return result;
		}

		// Token: 0x060021A4 RID: 8612 RVA: 0x000B312C File Offset: 0x000B152C
		private void CallbackDefaultBuildUp(INetComHandle phandle)
		{
			Set set = phandle.GetResponse() as Set;
			if (set != null)
			{
				UserDataUtil.TownData.DebugMngID = set.managedTownId;
				this.m_Step = TownComDebugSetUp.eStep.End;
			}
		}

		// Token: 0x04002805 RID: 10245
		private TownComDebugSetUp.eStep m_Step;

		// Token: 0x02000680 RID: 1664
		public enum eStep
		{
			// Token: 0x04002807 RID: 10247
			GetState,
			// Token: 0x04002808 RID: 10248
			WaitCheck,
			// Token: 0x04002809 RID: 10249
			End
		}
	}
}
