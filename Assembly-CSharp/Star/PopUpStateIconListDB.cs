﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001DD RID: 477
	public class PopUpStateIconListDB : ScriptableObject
	{
		// Token: 0x04000B38 RID: 2872
		public PopUpStateIconListDB_Param[] m_Params;
	}
}
