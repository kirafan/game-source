﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000A6 RID: 166
	public static class BattleCommandParser
	{
		// Token: 0x0600049B RID: 1179 RVA: 0x00017274 File Offset: 0x00015674
		public static void OptimizeTargetIndex(CharacterHandler charaHndl, eSkillTargetType targetType)
		{
			BattlePartyData battlePartyData;
			if (targetType != eSkillTargetType.TgtSingle)
			{
				if (targetType != eSkillTargetType.MySingle)
				{
					return;
				}
				battlePartyData = charaHndl.CharaBattle.MyParty;
			}
			else
			{
				battlePartyData = charaHndl.CharaBattle.TgtParty;
			}
			CharacterHandler member = battlePartyData.GetMember(charaHndl.CharaBattle.MyParty.SingleTargetIndex);
			if (member == null || member.CharaBattle.Param.IsDead())
			{
				for (int i = 0; i < 3; i++)
				{
					BattleDefine.eJoinMember eJoinMember = (BattleDefine.eJoinMember)i;
					if (eJoinMember != charaHndl.CharaBattle.MyParty.SingleTargetIndex)
					{
						CharacterHandler member2 = battlePartyData.GetMember(eJoinMember);
						if (member2 != null && !member2.CharaBattle.Param.IsDead())
						{
							charaHndl.CharaBattle.MyParty.SingleTargetIndex = eJoinMember;
							break;
						}
					}
				}
			}
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x00017360 File Offset: 0x00015760
		public static int UpdateOrder(BattleOrder battleOrder, int commandIndex, bool isUseUniqueSkill)
		{
			BattleOrder.FrameData frameDataAt = battleOrder.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			int result = -1;
			if (frameDataAt.IsCharaType)
			{
				float newOrderValue = BattleCommandParser.CalcOrderValue(owner, commandIndex, isUseUniqueSkill);
				result = battleOrder.MoveTopFrameWhileFixing(newOrderValue);
				for (int i = battleOrder.GetFrameNum() - 1; i >= 0; i--)
				{
					BattleOrder.FrameData frameDataAt2 = battleOrder.GetFrameDataAt(i);
					if (frameDataAt2.CompareOwnerCharaOnly(owner))
					{
						result = i;
						break;
					}
				}
			}
			else
			{
				frameDataAt.m_CardArgs.m_AliveNum--;
				if (frameDataAt.m_CardArgs.m_AliveNum > 0)
				{
					float newOrderValue2 = BattleCommandParser.CalcOrderValue(owner.CharaBattle.Param.FixedSpd(), frameDataAt.m_CardArgs.m_Command.LoadFactor, 1f);
					result = battleOrder.MoveTopFrameWhileFixing(newOrderValue2);
				}
			}
			return result;
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0001743C File Offset: 0x0001583C
		public static void UpdateOrderAfterTogetherAttack(BattleOrder battleOrder, List<CharacterHandler> togetherAttackedCharaHndls)
		{
			CharacterHandler frameOwnerAt = battleOrder.GetFrameOwnerAt(0);
			for (int i = 1; i < battleOrder.GetFrameNum(); i++)
			{
				BattleOrder.FrameData frameDataAt = battleOrder.GetFrameDataAt(i);
				if (frameDataAt.CompareOwnerCharaOnly(frameOwnerAt))
				{
					battleOrder.RemoveFrameDataAt(i);
					break;
				}
			}
			for (int j = 0; j < togetherAttackedCharaHndls.Count; j++)
			{
				battleOrder.RemoveFrameData(BattleOrder.eFrameType.Chara, togetherAttackedCharaHndls[j], false);
			}
			if (frameOwnerAt.IsFriendChara && frameOwnerAt.CharaBattle.System.BSD.FrdRemainTurn <= 0)
			{
				battleOrder.RemoveFrameData(BattleOrder.eFrameType.Chara, frameOwnerAt, false);
			}
			for (int k = 0; k < togetherAttackedCharaHndls.Count; k++)
			{
				if (togetherAttackedCharaHndls[k] != null)
				{
					float orderValue = BattleCommandParser.CalcOrderValue(togetherAttackedCharaHndls[k], -1, true);
					battleOrder.AddFrameData(BattleOrder.eFrameType.Chara, togetherAttackedCharaHndls[k], orderValue, null);
				}
			}
			battleOrder.SortOrder();
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x00017534 File Offset: 0x00015934
		public static float CalcOrderValue(int spd, float loadFactor, float orderCoef)
		{
			float num = BattleUtility.DefVal(eBattleDefineDB.OrderValueBaseMax);
			int num2 = (int)BattleUtility.DefVal(eBattleDefineDB.OrderValueDecreaseStart);
			if (spd >= num2)
			{
				int num3 = (spd - num2) / (int)BattleUtility.DefVal(eBattleDefineDB.OrderValueDecreaseInterval);
				num -= (float)(num3 + 1);
			}
			num = Mathf.Max(num, BattleUtility.DefVal(eBattleDefineDB.OrderValueBaseMin));
			float value = (float)((int)(num * loadFactor * orderCoef));
			return Mathf.Clamp(value, BattleUtility.DefVal(eBattleDefineDB.OrderValueMin), BattleUtility.DefVal(eBattleDefineDB.OrderValueMax));
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x00017594 File Offset: 0x00015994
		public static float CalcOrderValue(CharacterHandler charaHndl, int commandIndex, bool isUseUniqueSkill)
		{
			BattleCommandData command = null;
			if (isUseUniqueSkill)
			{
				command = charaHndl.CharaBattle.GetUniqueSkillCommandData();
			}
			else if (commandIndex > -1)
			{
				command = charaHndl.CharaBattle.GetCommandDataAt(commandIndex);
			}
			return BattleCommandParser.CalcOrderValue(charaHndl, command);
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x000175D8 File Offset: 0x000159D8
		public static float CalcOrderValue(CharacterHandler charaHndl, BattleCommandData command)
		{
			float loadFactor = 1f;
			float orderCoef = 1f;
			if (command != null)
			{
				loadFactor = command.LoadFactor;
			}
			else if (charaHndl.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Sleep))
			{
				loadFactor = BattleUtility.DefVal(eBattleDefineDB.StateAbnormalSleep_LoadFactor);
			}
			float statusBuffValue = charaHndl.CharaBattle.Param.GetStatusBuffValue(BattleDefine.eStatus.Spd);
			if (statusBuffValue != 0f)
			{
				orderCoef = statusBuffValue;
			}
			return BattleCommandParser.CalcOrderValue(charaHndl.CharaBattle.Param.FixedSpd(), loadFactor, orderCoef);
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x00017658 File Offset: 0x00015A58
		private static float CalcStunValue(int damage, int maxHp, float stunCoef, float elementCoef)
		{
			float num = (float)damage / (float)maxHp;
			float num2 = stunCoef * elementCoef;
			return num * num2;
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x00017674 File Offset: 0x00015A74
		private static float CalcStunerValue(float stuner, float stunMag, float ratio)
		{
			return stuner * 0.01f * stunMag * ratio;
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x00017690 File Offset: 0x00015A90
		public static float CalcStunDecreaseValueOnTurnStart()
		{
			return -Mathf.Abs(BattleUtility.DefVal(eBattleDefineDB.StunDecreaseValueOnTurnStart));
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x000176AC File Offset: 0x00015AAC
		private static float CalcAdditiveOrderValueOnStun(float orderValue)
		{
			return orderValue * (BattleUtility.DefVal(eBattleDefineDB.StunAdditiveOrderValueRatioWhenStun) * 0.01f);
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x000176CC File Offset: 0x00015ACC
		private static float CalcAdditiveOrderValueOnStunWeakAttacked(float orderValue, float ratio)
		{
			float num = orderValue * (BattleUtility.DefVal(eBattleDefineDB.StunAdditiveOrderValueRatioWhenAttacked) * 0.01f);
			return num * ratio;
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x000176F0 File Offset: 0x00015AF0
		public static List<CharacterHandler> UpdateOrderOnStun(BattleOrder battleOrder)
		{
			BattleOrder.FrameData frameDataAt = battleOrder.GetFrameDataAt(0);
			CharacterHandler owner = frameDataAt.m_Owner;
			float orderValue;
			if (frameDataAt.IsCharaType)
			{
				orderValue = BattleCommandParser.CalcOrderValue(owner, owner.CharaBattle.ExecCommand);
			}
			else
			{
				orderValue = BattleCommandParser.CalcOrderValue(owner.CharaBattle.Param.FixedSpd(), frameDataAt.m_CardArgs.m_Command.LoadFactor, 1f);
			}
			float num = BattleCommandParser.CalcAdditiveOrderValueOnStun(orderValue);
			List<CharacterHandler> list = new List<CharacterHandler>();
			List<CharacterHandler> availableMembers = owner.CharaBattle.TgtParty.GetAvailableMembers();
			for (int i = 0; i < availableMembers.Count; i++)
			{
				if (availableMembers[i].CharaBattle.JudgeAndApplyStun())
				{
					list.Add(availableMembers[i]);
					for (int j = battleOrder.GetFrameNum() - 1; j >= 0; j--)
					{
						BattleOrder.FrameData frameDataAt2 = battleOrder.GetFrameDataAt(j);
						if (frameDataAt2.CompareOwnerCharaOnly(availableMembers[i]))
						{
							frameDataAt2.m_OrderValue += num;
							break;
						}
					}
				}
			}
			if (list.Count > 0)
			{
				battleOrder.SortOrder();
			}
			return list;
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x00017828 File Offset: 0x00015C28
		public static void UpdateOrderOnStunWeakAttacked(BattleOrder battleOrder, CharacterHandler receiver, float orderValue, float ratio)
		{
			if (receiver == null)
			{
				return;
			}
			if (receiver.CharaBattle.Param.IsDead())
			{
				return;
			}
			float num = BattleCommandParser.CalcAdditiveOrderValueOnStunWeakAttacked(orderValue, ratio);
			for (int i = battleOrder.GetFrameNum() - 1; i >= 0; i--)
			{
				BattleOrder.FrameData frameDataAt = battleOrder.GetFrameDataAt(i);
				if (frameDataAt.CompareOwnerCharaOnly(receiver))
				{
					frameDataAt.m_OrderValue += num;
					break;
				}
			}
			battleOrder.SortOrder();
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x000178A8 File Offset: 0x00015CA8
		public static int CalcDamage(int power, int atkOrMgc, float togetherAttackCoef, int recv_def, float recv_barrierCutRatio, float elementCoef, bool isGuard, bool isCritical, float ratio, int opt_HpMaxRatio_damage)
		{
			float num = 1f;
			if (isGuard)
			{
				num = BattleUtility.DefVal(eBattleDefineDB.GuardCoef);
			}
			float num2 = UnityEngine.Random.Range(BattleUtility.DefVal(eBattleDefineDB.DamageRandMin), BattleUtility.DefVal(eBattleDefineDB.DamageRandMax));
			float num3 = 1f;
			if (isCritical)
			{
				num3 = BattleUtility.DefVal(eBattleDefineDB.CriticalCoef);
			}
			int num4 = (int)((float)(power * atkOrMgc) * togetherAttackCoef / ((float)recv_def * num));
			num4 = (int)((float)num4 * num2 * elementCoef * num3);
			if (opt_HpMaxRatio_damage > 0)
			{
				num4 = opt_HpMaxRatio_damage;
			}
			if (recv_barrierCutRatio > 0f)
			{
				num4 = (int)((float)num4 * (1f - recv_barrierCutRatio));
			}
			num4 = (int)((float)num4 * ratio);
			return (int)((float)num4 / BattleUtility.DefVal(eBattleDefineDB.DamageDiv));
		}

		// Token: 0x060004A9 RID: 1193 RVA: 0x00017944 File Offset: 0x00015D44
		public static int CalcRecover(int power, int mgc, float elementCoef, int recv_MaxHp, float togetherAttackCoef, bool recv_isUnhappy)
		{
			float num = BattleUtility.DefVal(eBattleDefineDB.RecoverBonusMin) + (float)mgc / BattleUtility.DefVal(eBattleDefineDB.RecoverBonusInterval) * BattleUtility.DefVal(eBattleDefineDB.RecoverBonusPlus);
			num = Mathf.Clamp(num, BattleUtility.DefVal(eBattleDefineDB.RecoverBonusMin), BattleUtility.DefVal(eBattleDefineDB.RecoverBonusMax));
			int num2 = (int)((float)power * num * elementCoef * 0.01f * (float)recv_MaxHp);
			num2 = (int)((float)num2 * togetherAttackCoef);
			if (recv_isUnhappy)
			{
				num2 = 0;
			}
			return num2;
		}

		// Token: 0x060004AA RID: 1194 RVA: 0x000179A4 File Offset: 0x00015DA4
		public static float CalcElementCoef(bool[] atkElementFlgs, CharacterHandler receiver, out int out_RegistHit_Or_DefaultHit_Or_WeakHit)
		{
			float num = -1f;
			for (int i = 0; i < atkElementFlgs.Length; i++)
			{
				if (atkElementFlgs[i])
				{
					float num2 = receiver.CharaBattle.Param.FixedElementCoef((eElementType)i);
					if (num2 > num)
					{
						num = num2;
					}
				}
			}
			out_RegistHit_Or_DefaultHit_Or_WeakHit = 0;
			eElementType weakElementType = BattleCommandParser.GetWeakElementType(receiver.CharaParam.ElementType);
			if (atkElementFlgs[(int)weakElementType])
			{
				out_RegistHit_Or_DefaultHit_Or_WeakHit = 1;
			}
			else
			{
				eElementType strongElementType = BattleCommandParser.GetStrongElementType(receiver.CharaParam.ElementType);
				if (atkElementFlgs[(int)strongElementType])
				{
					out_RegistHit_Or_DefaultHit_Or_WeakHit = -1;
				}
			}
			if (out_RegistHit_Or_DefaultHit_Or_WeakHit != 1)
			{
				if (out_RegistHit_Or_DefaultHit_Or_WeakHit != -1)
				{
					num = Mathf.Clamp(num, BattleUtility.DefVal(eBattleDefineDB.ElementCoefDefaultMin), BattleUtility.DefVal(eBattleDefineDB.ElementCoefDefaultMax));
				}
				else
				{
					num = Mathf.Clamp(num, BattleUtility.DefVal(eBattleDefineDB.ElementCoefRegistMin), BattleUtility.DefVal(eBattleDefineDB.ElementCoefRegistMax));
				}
			}
			else
			{
				num = Mathf.Clamp(num, BattleUtility.DefVal(eBattleDefineDB.ElementCoefWeakMin), BattleUtility.DefVal(eBattleDefineDB.ElementCoefWeakMax));
			}
			return num;
		}

		// Token: 0x060004AB RID: 1195 RVA: 0x00017A94 File Offset: 0x00015E94
		public static int JudgeElementCompatibility(List<eElementType> atkElementTypes, eElementType receiverElementType)
		{
			if (atkElementTypes != null && atkElementTypes.Count > 0)
			{
				eElementType weakElementType = BattleCommandParser.GetWeakElementType(receiverElementType);
				for (int i = 0; i < atkElementTypes.Count; i++)
				{
					if (atkElementTypes[i] == weakElementType)
					{
						return 1;
					}
				}
				eElementType strongElementType = BattleCommandParser.GetStrongElementType(receiverElementType);
				for (int j = 0; j < atkElementTypes.Count; j++)
				{
					if (atkElementTypes[j] == strongElementType)
					{
						return -1;
					}
				}
			}
			return 0;
		}

		// Token: 0x060004AC RID: 1196 RVA: 0x00017B10 File Offset: 0x00015F10
		public static void SetupDefaultElementCoef(eElementType elementType, ref float[] ref_results)
		{
			for (int i = 0; i < ref_results.Length; i++)
			{
				ref_results[i] = 1f;
			}
			eElementType strongElementType = BattleCommandParser.GetStrongElementType(elementType);
			ref_results[(int)strongElementType] = BattleUtility.DefVal(eBattleDefineDB.ElementCoefRegist);
			eElementType weakElementType = BattleCommandParser.GetWeakElementType(elementType);
			ref_results[(int)weakElementType] = BattleUtility.DefVal(eBattleDefineDB.ElementCoefWeak);
		}

		// Token: 0x060004AD RID: 1197 RVA: 0x00017B5F File Offset: 0x00015F5F
		public static eElementType GetStrongElementType(eElementType elementType)
		{
			switch (elementType)
			{
			case eElementType.Fire:
				return eElementType.Wind;
			case eElementType.Water:
				return eElementType.Fire;
			case eElementType.Earth:
				return eElementType.Water;
			case eElementType.Wind:
				return eElementType.Earth;
			case eElementType.Moon:
				return eElementType.Sun;
			case eElementType.Sun:
				return eElementType.Moon;
			default:
				return eElementType.Fire;
			}
		}

		// Token: 0x060004AE RID: 1198 RVA: 0x00017B91 File Offset: 0x00015F91
		public static eElementType GetWeakElementType(eElementType elementType)
		{
			switch (elementType)
			{
			case eElementType.Fire:
				return eElementType.Water;
			case eElementType.Water:
				return eElementType.Earth;
			case eElementType.Earth:
				return eElementType.Wind;
			case eElementType.Wind:
				return eElementType.Fire;
			case eElementType.Moon:
				return eElementType.Sun;
			case eElementType.Sun:
				return eElementType.Moon;
			default:
				return eElementType.Fire;
			}
		}

		// Token: 0x060004AF RID: 1199 RVA: 0x00017BC3 File Offset: 0x00015FC3
		public static float CalcTogetherAttackChainCoef(int chainCount)
		{
			switch (chainCount)
			{
			case 0:
				return BattleUtility.DefVal(eBattleDefineDB.TogetherAttackChainCoef_0);
			case 1:
				return BattleUtility.DefVal(eBattleDefineDB.TogetherAttackChainCoef_1);
			case 2:
				return BattleUtility.DefVal(eBattleDefineDB.TogetherAttackChainCoef_2);
			default:
				return 1f;
			}
		}

		// Token: 0x060004B0 RID: 1200 RVA: 0x00017BFC File Offset: 0x00015FFC
		public static bool CalcCritical(int luck, int recv_luck, int registHit_Or_DefaultHit_Or_WeakHit, bool isNextBuffCritical, bool isEnableStateAbnormalBearish, bool isStun)
		{
			if (isNextBuffCritical)
			{
				return true;
			}
			if (isStun)
			{
				return true;
			}
			if (isEnableStateAbnormalBearish)
			{
				return true;
			}
			if (registHit_Or_DefaultHit_Or_WeakHit == -1)
			{
				return false;
			}
			float num = (float)luck;
			if (registHit_Or_DefaultHit_Or_WeakHit == 1)
			{
				num *= BattleUtility.DefVal(eBattleDefineDB.CriticalStrongElementCoef);
			}
			num *= BattleUtility.DefVal(eBattleDefineDB.CriticalAdjustmentCoef);
			num -= (float)recv_luck;
			num = Mathf.Clamp(num, 0f, BattleUtility.DefVal(eBattleDefineDB.CriticalProbablityMax));
			return num > 0f && UnityEngine.Random.Range(0f, 100f) <= num;
		}

		// Token: 0x060004B1 RID: 1201 RVA: 0x00017C87 File Offset: 0x00016087
		public static bool SolveCommandAt(BattleCommandData command, int contentIndex, float[] optionParams, CharacterHandler targetCharaHndl, out List<SkillActionEffectSet> out_effectSets)
		{
			if (contentIndex < command.SkillContentSets.Count)
			{
				out_effectSets = BattleCommandParser.SolveSkillContent(command, contentIndex, optionParams, targetCharaHndl);
				return true;
			}
			out_effectSets = null;
			return false;
		}

		// Token: 0x060004B2 RID: 1202 RVA: 0x00017CB0 File Offset: 0x000160B0
		private static List<SkillActionEffectSet> SolveSkillContent(BattleCommandData command, int contentIndex, float[] optionParams, CharacterHandler targetCharaHndl)
		{
			BattleCommandData.SkillContentSet skillContentSet = command.SkillContentSets[contentIndex];
			bool isMasterSkill = command.CommandType == BattleCommandData.eCommandType.MasterSkill;
			bool isCard = command.CommandType == BattleCommandData.eCommandType.Card;
			List<CharacterHandler> skillTargets = BattleCommandParser.GetSkillTargets(skillContentSet.m_Target, command.Owner);
			if (targetCharaHndl != null && skillTargets.Contains(targetCharaHndl))
			{
				skillTargets.Clear();
				skillTargets.Add(targetCharaHndl);
			}
			List<SkillActionEffectSet> result = null;
			switch (skillContentSet.m_Data.m_Type)
			{
			case 0:
			{
				float orderValue = BattleCommandParser.CalcOrderValue(command.Owner, command);
				result = BattleCommandParser.SolveSkillContent_Attack(command.Owner, isMasterSkill, isCard, skillTargets, skillContentSet, command.SkillLv, optionParams, orderValue, command.SolveResult);
				break;
			}
			case 1:
				result = BattleCommandParser.SolveSkillContent_Recover(command.Owner, isMasterSkill, isCard, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 2:
				result = BattleCommandParser.SolveSkillContent_StatusChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 3:
				result = BattleCommandParser.SolveSkillContent_StatusChangeReset(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 4:
				result = BattleCommandParser.SolveSkillContent_Abnormal(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 5:
				result = BattleCommandParser.SolveSkillContent_AbnormalRecover(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 6:
				result = BattleCommandParser.SolveSkillContent_AbnormalDisable(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 7:
				result = BattleCommandParser.SolveSkillContent_AbnormalAdditionalProbability(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 8:
				result = BattleCommandParser.SolveSkillContent_ElementResist(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 9:
				result = BattleCommandParser.SolveSkillContent_ElementChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 10:
				result = BattleCommandParser.SolveSkillContent_WeakElementBonus(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 11:
				result = BattleCommandParser.SolveSkillContent_NextAttackUp(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 12:
				result = BattleCommandParser.SolveSkillContent_NextAttackCritical(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 13:
				result = BattleCommandParser.SolveSkillContent_Barrier(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 14:
				result = BattleCommandParser.SolveSkillContent_RecastChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 15:
				result = BattleCommandParser.SolveSkillContent_KiraraJumpGaugeChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 16:
				result = BattleCommandParser.SolveSkillContent_KiraraJumpGaugeCoef(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 17:
				result = BattleCommandParser.SolveSkillContent_OrderChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 18:
				result = BattleCommandParser.SolveSkillContent_HateChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 19:
				result = BattleCommandParser.SolveSkillContent_ChargeChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 20:
				result = BattleCommandParser.SolveSkillContent_ChainCoefChange(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 21:
				result = BattleCommandParser.SolveSkillContent_Card(command.Owner, command.CommandIndex, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 22:
				result = BattleCommandParser.SolveSkillContent_StunRecover(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			case 23:
				result = BattleCommandParser.SolveSkillContent_Regene(command.Owner, isMasterSkill, skillTargets, skillContentSet, command.SkillLv, command.SolveResult);
				break;
			}
			return result;
		}

		// Token: 0x060004B3 RID: 1203 RVA: 0x000180C4 File Offset: 0x000164C4
		public static string SkillContentTypeToString(eSkillContentType skillContentType)
		{
			switch (skillContentType)
			{
			case eSkillContentType.Attack:
				return "攻撃";
			case eSkillContentType.Recover:
				return "回復";
			case eSkillContentType.StatusChange:
				return "ステータス変化";
			case eSkillContentType.StatusChangeReset:
				return "ステータス変化リセット";
			case eSkillContentType.Abnormal:
				return "状態異常";
			case eSkillContentType.AbnormalRecover:
				return "状態異常回復";
			case eSkillContentType.AbnormalDisable:
				return "状態異常無効";
			case eSkillContentType.AbnormalAdditionalProbability:
				return "状態異常付与確率補正";
			case eSkillContentType.ElementResist:
				return "属性耐性";
			case eSkillContentType.ElementChange:
				return "属性変化";
			case eSkillContentType.WeakElementBonus:
				return "有利属性ボーナス";
			case eSkillContentType.NextAttackUp:
				return "次回の攻撃威力アップ";
			case eSkillContentType.NextAttackCritical:
				return "次回の攻撃絶対クリティカル";
			case eSkillContentType.Barrier:
				return "バリア";
			case eSkillContentType.RecastChange:
				return "リキャスト変化";
			case eSkillContentType.KiraraJumpGaugeChange:
				return "きららジャンプゲージ量変化";
			case eSkillContentType.KiraraJumpGaugeCoef:
				return "きららジャンプゲージ倍率変化";
			case eSkillContentType.OrderChange:
				return "行動順変化";
			case eSkillContentType.HateChange:
				return "ヘイト変化";
			case eSkillContentType.ChargeChange:
				return "チャージ変化";
			case eSkillContentType.ChainCoefChange:
				return "チェイン倍率変化";
			case eSkillContentType.Card:
				return "スキルカード";
			case eSkillContentType.StunRecover:
				return "スタン回復";
			case eSkillContentType.Regene:
				return "リジェネ";
			default:
				return "文字列未定義";
			}
		}

		// Token: 0x060004B4 RID: 1204 RVA: 0x000181D4 File Offset: 0x000165D4
		private static List<SkillActionEffectSet> SolveSkillContent_Attack(CharacterHandler executor, bool isMasterSkill, bool isCard, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, float[] optionParams, float orderValue, BattleCommandSolveResult out_result)
		{
			BattleCommandParser.eSkillContentTypeAttackOption eSkillContentTypeAttackOption = BattleCommandParser.eSkillContentTypeAttackOption.None;
			if (contentSet.m_Data.m_Args.Length > 9)
			{
				eSkillContentTypeAttackOption = (BattleCommandParser.eSkillContentTypeAttackOption)contentSet.m_Data.m_Args[9];
			}
			float num = (contentSet.m_Data.m_Args.Length <= 8) ? 0f : contentSet.m_Data.m_Args[8];
			int num2 = (int)contentSet.m_Data.m_Args[0];
			num2 = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, (float)num2);
			bool flag = (int)contentSet.m_Data.m_Args[1] == 0;
			int num3 = 1;
			if (!isMasterSkill)
			{
				if (flag)
				{
					num3 = executor.CharaBattle.Param.FixedAtk();
					if (!isCard)
					{
						num3 = (int)((float)num3 * (1f + executor.CharaBattle.Param.GetNextBuffCoef(BattleDefine.eNextBuff.Attack)));
					}
				}
				else
				{
					num3 = executor.CharaBattle.Param.FixedMgc();
					if (!isCard)
					{
						num3 = (int)((float)num3 * (1f + executor.CharaBattle.Param.GetNextBuffCoef(BattleDefine.eNextBuff.Magic)));
					}
				}
			}
			bool[] array = new bool[]
			{
				contentSet.m_Data.m_Args[2] != 0f,
				contentSet.m_Data.m_Args[3] != 0f,
				contentSet.m_Data.m_Args[4] != 0f,
				contentSet.m_Data.m_Args[5] != 0f,
				contentSet.m_Data.m_Args[6] != 0f,
				contentSet.m_Data.m_Args[7] != 0f
			};
			if (!isMasterSkill && contentSet.m_Data.m_Args[2] == 0f && contentSet.m_Data.m_Args[3] == 0f && contentSet.m_Data.m_Args[4] == 0f && contentSet.m_Data.m_Args[5] == 0f && contentSet.m_Data.m_Args[6] == 0f && contentSet.m_Data.m_Args[7] == 0f)
			{
				array[(int)executor.CharaParam.ElementType] = true;
			}
			float togetherAttackCoef = 1f;
			if (!isMasterSkill && !isCard)
			{
				togetherAttackCoef = BattleCommandParser.CalcTogetherAttackChainCoef(executor.CharaBattle.TogetherAttackChainCount);
			}
			int luck = 1;
			if (!isMasterSkill)
			{
				luck = executor.CharaBattle.Param.FixedLuck(false);
			}
			bool isNextBuffCritical = false;
			if (!isMasterSkill && !isCard && executor.CharaBattle.Param.GetNextBuffCoef(BattleDefine.eNextBuff.Critical) > 0f)
			{
				isNextBuffCritical = true;
			}
			float ratio = 1f;
			if (optionParams != null && optionParams.Length > 0)
			{
				ratio = optionParams[0] * 0.01f;
			}
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					int recv_def;
					if (flag)
					{
						recv_def = characterHandler.CharaBattle.Param.FixedDef();
					}
					else
					{
						recv_def = characterHandler.CharaBattle.Param.FixedMDef();
					}
					int num4 = 0;
					float num5 = BattleCommandParser.CalcElementCoef(array, characterHandler, out num4);
					if (num4 == 1)
					{
						num5 += executor.CharaBattle.Param.GetWeakElementBonusBuffValue();
					}
					bool isGuard = characterHandler.CharaBattle.Param.IsGuard();
					bool isCritical = BattleCommandParser.CalcCritical(luck, characterHandler.CharaBattle.Param.FixedLuck(true), num4, isNextBuffCritical, characterHandler.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Bearish), characterHandler.CharaBattle.IsStun());
					int opt_HpMaxRatio_damage = 0;
					int num6 = num2;
					if (eSkillContentTypeAttackOption == BattleCommandParser.eSkillContentTypeAttackOption.Abnormal)
					{
						if (characterHandler.CharaBattle.Param.IsEnableStateAbnormalAny())
						{
							num6 = (int)contentSet.m_Data.m_Args[10];
							num6 = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, (float)num6);
						}
					}
					else if (eSkillContentTypeAttackOption == BattleCommandParser.eSkillContentTypeAttackOption.MaxHpRatio)
					{
						float num7 = contentSet.m_Data.m_Args[10] * 0.01f;
						opt_HpMaxRatio_damage = (int)((float)characterHandler.CharaBattle.Param.FixedMaxHp() * num7);
					}
					int num8 = BattleCommandParser.CalcDamage(num6, num3, togetherAttackCoef, recv_def, characterHandler.CharaBattle.Param.GetBarrierCutRatio(), num5, isGuard, isCritical, ratio, opt_HpMaxRatio_damage);
					int num9 = characterHandler.CharaBattle.Param.CalcHp(-num8);
					if (num9 <= 0)
					{
						if (characterHandler.CharaBattle.MyParty.IsPlayerParty)
						{
							if (!characterHandler.CharaBattle.System.Tutorial.IsSeq(BattleTutorial.eSeq.None))
							{
								num9 = 1;
								characterHandler.CharaBattle.Param.SetHp(num9);
							}
						}
						else if (characterHandler.CharaBattle.MyParty.IsEnemyParty)
						{
							BattleTutorial.eSeq seq = characterHandler.CharaBattle.System.Tutorial.GetSeq();
							if (seq == BattleTutorial.eSeq._02_WAVE_IDX_0_TIPS_COMMAND || seq == BattleTutorial.eSeq._11_WAVE_IDX_1_TIPS_UNIQUE_SKILL)
							{
								num9 = 1;
								characterHandler.CharaBattle.Param.SetHp(num9);
							}
						}
					}
					if (num9 <= 0)
					{
						characterHandler.CharaBattle.Param.SetDeadDetail(BattleDefine.eDeadDetail.Dead_Ready);
					}
					characterHandler.CharaBattle.Param.SetWasAttacked(true);
					if (characterHandler.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Sleep))
					{
						characterHandler.CharaBattle.Param.SetWasAttackedOnSleeping(true);
					}
					BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
					BattleDefine.eFluctuation elementFluctuation = BattleDefine.eFluctuation.None;
					if (num5 > 1f)
					{
						elementFluctuation = BattleDefine.eFluctuation.Up;
					}
					else if (num5 < 1f)
					{
						elementFluctuation = BattleDefine.eFluctuation.Down;
					}
					charaResult.AddDamageHitData(new BattleCommandSolveResult.HitData(-num8, isCritical, num4, elementFluctuation));
					if (characterHandler.CharaBattle.Param.IsDead())
					{
						out_result.AddCharaDiedWithThisCommand_Safe(characterHandler);
					}
					if (!characterHandler.CharaBattle.IsStun())
					{
						bool flag2 = false;
						if (num > 0f)
						{
							if (!characterHandler.CharaBattle.IsStunerJudged())
							{
								characterHandler.CharaBattle.SetIsStunerJudged(true);
								float num10 = UnityEngine.Random.Range(0f, 100f);
								if (num10 <= characterHandler.CharaParam.StunerAvoid)
								{
									characterHandler.CharaBattle.SetIsStunerApply(false);
								}
								else
								{
									characterHandler.CharaBattle.SetIsStunerApply(true);
								}
							}
							flag2 = characterHandler.CharaBattle.IsStunerApply();
						}
						float value;
						if (flag2)
						{
							value = BattleCommandParser.CalcStunerValue(num, characterHandler.CharaParam.StunerMag, ratio);
						}
						else
						{
							value = BattleCommandParser.CalcStunValue(num8, characterHandler.CharaBattle.Param.FixedMaxHp(), characterHandler.CharaParam.StunCoef, num5);
						}
						if (characterHandler.CharaBattle.MyParty.IsEnemyParty)
						{
							BattleTutorial.eSeq seq2 = characterHandler.CharaBattle.System.Tutorial.GetSeq();
							if (seq2 == BattleTutorial.eSeq._11_WAVE_IDX_1_TIPS_UNIQUE_SKILL)
							{
								value = 1f;
							}
						}
						characterHandler.CharaBattle.CalcStunValue(value);
					}
					else if (num4 == 1)
					{
						BattleCommandParser.UpdateOrderOnStunWeakAttacked(characterHandler.CharaBattle.System.Order, characterHandler, orderValue, ratio);
					}
				}
			}
			if (!isMasterSkill && !isCard)
			{
				if (flag)
				{
					executor.CharaBattle.Param.SetUsedNextBuffIfAvailable(BattleDefine.eNextBuff.Attack);
				}
				else
				{
					executor.CharaBattle.Param.SetUsedNextBuffIfAvailable(BattleDefine.eNextBuff.Magic);
				}
				executor.CharaBattle.Param.SetUsedNextBuffIfAvailable(BattleDefine.eNextBuff.Critical);
			}
			return null;
		}

		// Token: 0x060004B5 RID: 1205 RVA: 0x000189BC File Offset: 0x00016DBC
		private static List<SkillActionEffectSet> SolveSkillContent_Recover(CharacterHandler executor, bool isMasterSkill, bool isCard, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			int num = (int)contentSet.m_Data.m_Args[0];
			num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, (float)num);
			float togetherAttackChainCoef = 1f;
			if (!isMasterSkill && !isCard)
			{
				togetherAttackChainCoef = BattleCommandParser.CalcTogetherAttackChainCoef(executor.CharaBattle.TogetherAttackChainCount);
			}
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (!characterHandler.CharaBattle.Param.IsDead())
				{
					float num2 = 1f;
					bool flg = false;
					int num3 = BattleCommandParser._SolveRecover(executor, characterHandler, isMasterSkill, num, togetherAttackChainCoef, out num2, out flg);
					BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
					BattleDefine.eFluctuation elementFluctuation = BattleDefine.eFluctuation.None;
					if (num2 > 1f)
					{
						elementFluctuation = BattleDefine.eFluctuation.Up;
					}
					else if (num2 < 1f)
					{
						elementFluctuation = BattleDefine.eFluctuation.Down;
					}
					charaResult.AddRecoverHitData(new BattleCommandSolveResult.HitData(num3, false, 0, elementFluctuation));
					charaResult.SetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.RecoveryValueIsZeroBecauseUnhappy, flg);
					if (num3 > 0)
					{
						charaResult.SetAutoFacialID(9);
					}
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_Recover((int)contentSet.m_Data.m_Args[0]));
				}
			}
			return list;
		}

		// Token: 0x060004B6 RID: 1206 RVA: 0x00018B04 File Offset: 0x00016F04
		private static int _SolveRecover(CharacterHandler executor, CharacterHandler receiver, bool isMasterSkill, int power, float togetherAttackChainCoef, out float out_elementCoef, out bool out_recoveryValueIsZeroBecauseUnhappy)
		{
			int mgc = 1;
			if (!isMasterSkill)
			{
				mgc = executor.CharaBattle.Param.FixedMgc();
			}
			out_elementCoef = 1f;
			if (!isMasterSkill && executor.CharaParam.ElementType == receiver.CharaParam.ElementType)
			{
				out_elementCoef = BattleUtility.DefVal(eBattleDefineDB.RecoverBonusSameElement);
			}
			out_recoveryValueIsZeroBecauseUnhappy = receiver.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Unhappy);
			int num = BattleCommandParser.CalcRecover(power, mgc, out_elementCoef, receiver.CharaBattle.Param.FixedMaxHp(), togetherAttackChainCoef, out_recoveryValueIsZeroBecauseUnhappy);
			receiver.CharaBattle.Param.CalcHp(num);
			return num;
		}

		// Token: 0x060004B7 RID: 1207 RVA: 0x00018BA4 File Offset: 0x00016FA4
		private static List<SkillActionEffectSet> SolveSkillContent_StatusChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			float[] array = null;
			float[] array2 = null;
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					for (int j = 2; j < contentSet.m_Data.m_Args.Length; j++)
					{
						BattleDefine.eStatus eStatus = (BattleDefine.eStatus)(j - 2);
						float num = contentSet.m_Data.m_Args[j];
						if (eStatus != BattleDefine.eStatus.Spd)
						{
							num *= 0.01f;
						}
						num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, num);
						if (num != 0f)
						{
							characterHandler.CharaBattle.Param.SetStatusBuff(eStatus, turnConsume, turn, num);
							if (eStatus != BattleDefine.eStatus.Spd)
							{
								if (num > 0f)
								{
									if (array == null)
									{
										array = new float[6];
									}
									array[(int)eStatus] = contentSet.m_Data.m_Args[j];
								}
								else
								{
									if (array2 == null)
									{
										array2 = new float[6];
									}
									array2[(int)eStatus] = contentSet.m_Data.m_Args[j];
								}
							}
							else if (num <= 1f)
							{
								if (array == null)
								{
									array = new float[6];
								}
								array[(int)eStatus] = contentSet.m_Data.m_Args[j];
							}
							else
							{
								if (array2 == null)
								{
									array2 = new float[6];
								}
								array2[(int)eStatus] = contentSet.m_Data.m_Args[j];
							}
						}
					}
					if (array != null)
					{
						SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
						list.Add(skillActionEffectSet.SetupTo_StatusChange_Up(array));
					}
					if (array2 != null)
					{
						SkillActionEffectSet skillActionEffectSet2 = new SkillActionEffectSet(characterHandler);
						list.Add(skillActionEffectSet2.SetupTo_StatusChange_Down(array2));
					}
				}
			}
			return list;
		}

		// Token: 0x060004B8 RID: 1208 RVA: 0x00018D98 File Offset: 0x00017198
		private static List<SkillActionEffectSet> SolveSkillContent_StatusChangeReset(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					for (int j = 0; j < contentSet.m_Data.m_Args.Length; j++)
					{
						if (contentSet.m_Data.m_Args[j] != 0f)
						{
							characterHandler.CharaBattle.Param.ResetStatusBuff((BattleDefine.eStatus)i);
						}
					}
				}
			}
			return null;
		}

		// Token: 0x060004B9 RID: 1209 RVA: 0x00018E28 File Offset: 0x00017228
		public static List<SkillActionEffectSet> SolveSkillContent_Abnormal(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					if (!characterHandler.CharaBattle.Param.GetStateAbnormalDisableBuffValue())
					{
						for (int j = 0; j < contentSet.m_Data.m_Args.Length; j++)
						{
							eStateAbnormal eStateAbnormal = (eStateAbnormal)j;
							BattleDefine.StateAbnormal stateAbnormal = characterHandler.CharaBattle.Param.GetStateAbnormal(eStateAbnormal);
							if (!stateAbnormal.IsRegist)
							{
								float num = contentSet.m_Data.m_Args[j];
								num += characterHandler.CharaBattle.Param.GetStateAbnormalAddProbabilityBuffValue();
								float num2 = UnityEngine.Random.Range(0f, 100f);
								bool flag = num2 <= num;
								if (flag)
								{
									int turn = 0;
									switch (eStateAbnormal)
									{
									case eStateAbnormal.Confusion:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalConfusion_Turn);
										break;
									case eStateAbnormal.Paralysis:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalParalysis_Turn);
										break;
									case eStateAbnormal.Poison:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalPoison_Turn);
										break;
									case eStateAbnormal.Bearish:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalBearish_Turn);
										break;
									case eStateAbnormal.Sleep:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalSleep_Turn);
										break;
									case eStateAbnormal.Unhappy:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalUnhappy_Turn);
										break;
									case eStateAbnormal.Silence:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalSilence_Turn);
										break;
									case eStateAbnormal.Isolation:
										turn = (int)BattleUtility.DefVal(eBattleDefineDB.StateAbnormalIsolation_Turn);
										break;
									}
									characterHandler.CharaBattle.Param.SetStateAbnormal(eStateAbnormal, turn);
									BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
									charaResult.SetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.BecameStateAbnormal, true);
									SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
									list.Add(skillActionEffectSet.SetupTo_Abnormal(eStateAbnormal));
								}
							}
						}
					}
					BattleCommandSolveResult.CharaResult charaResult2 = out_result.AddCharaResult_Safe(characterHandler);
					if (!charaResult2.GetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.BecameStateAbnormal))
					{
						SkillActionEffectSet skillActionEffectSet2 = new SkillActionEffectSet(characterHandler);
						list.Add(skillActionEffectSet2.SetupTo_AbnormalMiss());
					}
				}
			}
			return list;
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x00019024 File Offset: 0x00017424
		private static List<SkillActionEffectSet> SolveSkillContent_AbnormalRecover(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					for (int j = 0; j < contentSet.m_Data.m_Args.Length; j++)
					{
						if (contentSet.m_Data.m_Args[j] != 0f)
						{
							characterHandler.CharaBattle.Param.ResetStateAbnormal((eStateAbnormal)j);
						}
					}
					BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
					charaResult.SetIsSpecialReasonFlg(BattleCommandSolveResult.eSpecialReason.RecoverdStateAbnormal, true);
					charaResult.SetAutoFacialID(9);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_AbnormalRecover());
				}
			}
			return list;
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x000190EC File Offset: 0x000174EC
		private static List<SkillActionEffectSet> SolveSkillContent_AbnormalDisable(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetStateAbnormalDisableBuff(turnConsume, turn);
				}
			}
			return null;
		}

		// Token: 0x060004BC RID: 1212 RVA: 0x00019164 File Offset: 0x00017564
		private static List<SkillActionEffectSet> SolveSkillContent_AbnormalAdditionalProbability(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			float num = contentSet.m_Data.m_Args[2];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetStateAbnormalAddProbabilityBuff(turnConsume, turn, num);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_AbnormalAdditionalProbability(num > 0f));
				}
			}
			return list;
		}

		// Token: 0x060004BD RID: 1213 RVA: 0x00019218 File Offset: 0x00017618
		private static List<SkillActionEffectSet> SolveSkillContent_ElementResist(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					for (int j = 2; j < contentSet.m_Data.m_Args.Length; j++)
					{
						float num = contentSet.m_Data.m_Args[j] * 0.01f;
						num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, num);
						if (num != 0f)
						{
							eElementType eElementType = (eElementType)(j - 2);
							characterHandler.CharaBattle.Param.SetResistElement(eElementType, turnConsume, turn, num);
							if (num > 0f)
							{
								SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
								list.Add(skillActionEffectSet.SetupTo_ElementResist_Up(eElementType, contentSet.m_Data.m_Args[j]));
							}
							else
							{
								SkillActionEffectSet skillActionEffectSet2 = new SkillActionEffectSet(characterHandler);
								list.Add(skillActionEffectSet2.SetupTo_ElementResist_Down(eElementType, contentSet.m_Data.m_Args[j]));
							}
						}
					}
				}
			}
			return list;
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x0001936C File Offset: 0x0001776C
		private static List<SkillActionEffectSet> SolveSkillContent_ElementChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			eElementType eElementType = (eElementType)contentSet.m_Data.m_Args[0];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.ElementChange(eElementType);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_ElementChange(eElementType));
				}
			}
			return list;
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x000193F0 File Offset: 0x000177F0
		private static List<SkillActionEffectSet> SolveSkillContent_WeakElementBonus(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			float value = Mathf.Abs(contentSet.m_Data.m_Args[2] * 0.01f);
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetWeakElementBonusBuff(turnConsume, turn, value);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_WeakElementBonus(contentSet.m_Data.m_Args[2]));
				}
			}
			return list;
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x000194B4 File Offset: 0x000178B4
		private static List<SkillActionEffectSet> SolveSkillContent_NextAttackUp(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			bool flag = (int)contentSet.m_Data.m_Args[0] == 0;
			float num = contentSet.m_Data.m_Args[1] * 0.01f;
			num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, num);
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					if (flag)
					{
						characterHandler.CharaBattle.Param.SetNextBuff(BattleDefine.eNextBuff.Attack, num);
					}
					else
					{
						characterHandler.CharaBattle.Param.SetNextBuff(BattleDefine.eNextBuff.Magic, num);
					}
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_NextAttackUp(flag, contentSet.m_Data.m_Args[1]));
				}
			}
			return list;
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x000195AC File Offset: 0x000179AC
		private static List<SkillActionEffectSet> SolveSkillContent_NextAttackCritical(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetNextBuff(BattleDefine.eNextBuff.Critical, 1f);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_NextAttackCritical());
				}
			}
			return list;
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x00019624 File Offset: 0x00017A24
		private static List<SkillActionEffectSet> SolveSkillContent_Barrier(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			float num = contentSet.m_Data.m_Args[0] * 0.01f;
			num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, num);
			int count = (int)contentSet.m_Data.m_Args[1];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetBarrier(num, count);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_Barrier());
				}
			}
			return list;
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x000196E4 File Offset: 0x00017AE4
		private static List<SkillActionEffectSet> SolveSkillContent_RecastChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			float num = contentSet.m_Data.m_Args[0];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.RecastChange(num);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_RecastChange(num > 0f));
				}
			}
			return list;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x0001976C File Offset: 0x00017B6C
		private static List<SkillActionEffectSet> SolveSkillContent_KiraraJumpGaugeChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			float value = contentSet.m_Data.m_Args[0];
			if (executor.CharaBattle.System != null)
			{
				executor.CharaBattle.System.CalcTogetherAttackGauge(value);
			}
			SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(executor);
			list.Add(skillActionEffectSet.SetupTo_KiraraJumpGaugeChange());
			return list;
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x000197C8 File Offset: 0x00017BC8
		private static List<SkillActionEffectSet> SolveSkillContent_KiraraJumpGaugeCoef(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x060004C6 RID: 1222 RVA: 0x000197CB File Offset: 0x00017BCB
		private static List<SkillActionEffectSet> SolveSkillContent_OrderChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			return null;
		}

		// Token: 0x060004C7 RID: 1223 RVA: 0x000197D0 File Offset: 0x00017BD0
		private static List<SkillActionEffectSet> SolveSkillContent_HateChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			eSkillTurnConsume turnConsume = (eSkillTurnConsume)contentSet.m_Data.m_Args[0];
			int turn = (int)contentSet.m_Data.m_Args[1];
			float num = Mathf.Abs(contentSet.m_Data.m_Args[2] * 0.01f);
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetHateChange(turnConsume, turn, num);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_HateChange(num > 0f, contentSet.m_Data.m_Args[2]));
				}
			}
			return list;
		}

		// Token: 0x060004C8 RID: 1224 RVA: 0x0001989C File Offset: 0x00017C9C
		private static List<SkillActionEffectSet> SolveSkillContent_ChargeChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			int num = (int)contentSet.m_Data.m_Args[0];
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.CalcChargeCount(num);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_ChargeChange(num > 0));
				}
			}
			return list;
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x00019924 File Offset: 0x00017D24
		private static List<SkillActionEffectSet> SolveSkillContent_ChainCoefChange(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			int num = (int)contentSet.m_Data.m_Args[0];
			executor.CharaBattle.MyParty.SetTogetherAttackChainCoefChange((float)num);
			List<CharacterHandler> joinMembers = executor.CharaBattle.MyParty.GetJoinMembers(true);
			for (int i = 0; i < joinMembers.Count; i++)
			{
				CharacterHandler targetCharaHndl = joinMembers[i];
				SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(targetCharaHndl);
				list.Add(skillActionEffectSet.SetupTo_ChainCoefChange());
			}
			return list;
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x000199A4 File Offset: 0x00017DA4
		private static List<SkillActionEffectSet> SolveSkillContent_Card(CharacterHandler executor, int commandIndex, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			int aliveNum = (int)contentSet.m_Data.m_Args[0];
			int num = (int)contentSet.m_Data.m_Args[1];
			BattleOrder order = executor.CharaBattle.System.Order;
			BattleOrder.FrameData frameData = order.FindCardFrameData(executor, commandIndex);
			if (frameData == null)
			{
				BattleCommandData battleCommandData = new BattleCommandData();
				battleCommandData.Setup(BattleCommandData.eCommandType.Card, -1, executor, num, skillLv, skillLv, 0L, -1, true);
				BattleOrder.CardArguments cardArgs = new BattleOrder.CardArguments(aliveNum, num, commandIndex, battleCommandData);
				float orderValue = BattleCommandParser.CalcOrderValue(executor.CharaBattle.Param.FixedSpd(), battleCommandData.LoadFactor, 1f);
				order.AddFrameData(BattleOrder.eFrameType.Card, executor, orderValue, cardArgs);
				order.SortOrder();
			}
			else
			{
				frameData.m_CardArgs.m_AliveNum = aliveNum;
			}
			out_result.m_IsCreatedCard = true;
			return null;
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x00019A68 File Offset: 0x00017E68
		private static List<SkillActionEffectSet> SolveSkillContent_StunRecover(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.ResetStun();
					BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
					charaResult.SetAutoFacialID(9);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_StunRecover());
				}
			}
			return list;
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x00019AE8 File Offset: 0x00017EE8
		private static List<SkillActionEffectSet> SolveSkillContent_Regene(CharacterHandler executor, bool isMasterSkill, List<CharacterHandler> skillTargets, BattleCommandData.SkillContentSet contentSet, int skillLv, BattleCommandSolveResult out_result)
		{
			List<SkillActionEffectSet> list = new List<SkillActionEffectSet>();
			int turn = (int)contentSet.m_Data.m_Args[0];
			int num = (int)contentSet.m_Data.m_Args[1];
			num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillLvCoefDB.CalcValueFromSkillLv(skillLv, (int)contentSet.m_Data.m_SkillLvCoef, (float)num);
			for (int i = 0; i < skillTargets.Count; i++)
			{
				CharacterHandler characterHandler = skillTargets[i];
				if (characterHandler.CharaBattle.Param.GetDeadDetail() != BattleDefine.eDeadDetail.Dead_Complete)
				{
					characterHandler.CharaBattle.Param.SetRegene(executor.gameObject.name, turn, num);
					BattleCommandSolveResult.CharaResult charaResult = out_result.AddCharaResult_Safe(characterHandler);
					charaResult.SetAutoFacialID(9);
					SkillActionEffectSet skillActionEffectSet = new SkillActionEffectSet(characterHandler);
					list.Add(skillActionEffectSet.SetupTo_Regene());
				}
			}
			return list;
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x00019BC4 File Offset: 0x00017FC4
		private static List<CharacterHandler> GetSkillTargets(eSkillTargetType skillTargetType, CharacterHandler executor)
		{
			List<CharacterHandler> list = new List<CharacterHandler>();
			switch (skillTargetType)
			{
			case eSkillTargetType.Self:
				list.Add(executor);
				break;
			case eSkillTargetType.TgtSingle:
				list.Add(executor.CharaBattle.TgtParty.GetMember(executor.CharaBattle.MyParty.SingleTargetIndex));
				break;
			case eSkillTargetType.TgtAll:
				list = executor.CharaBattle.TgtParty.GetJoinMembers(false);
				break;
			case eSkillTargetType.MySingle:
				list.Add(executor.CharaBattle.MyParty.GetMember(executor.CharaBattle.MyParty.SingleTargetIndex));
				break;
			case eSkillTargetType.MyAll:
				list = executor.CharaBattle.MyParty.GetJoinMembers(false);
				break;
			}
			return list;
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x00019C88 File Offset: 0x00018088
		public static List<eElementType> GetElementsOnAttack(BattleCommandData.SkillContentSet contentSet)
		{
			List<eElementType> list = new List<eElementType>();
			if (contentSet.m_Data.m_Type != 0)
			{
				return list;
			}
			if (contentSet.m_Data.m_Args[2] != 0f)
			{
				list.Add(eElementType.Fire);
			}
			if (contentSet.m_Data.m_Args[3] != 0f)
			{
				list.Add(eElementType.Water);
			}
			if (contentSet.m_Data.m_Args[4] != 0f)
			{
				list.Add(eElementType.Earth);
			}
			if (contentSet.m_Data.m_Args[5] != 0f)
			{
				list.Add(eElementType.Wind);
			}
			if (contentSet.m_Data.m_Args[6] != 0f)
			{
				list.Add(eElementType.Moon);
			}
			if (contentSet.m_Data.m_Args[7] != 0f)
			{
				list.Add(eElementType.Sun);
			}
			return list;
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x00019D64 File Offset: 0x00018164
		public static int SolveAutoCommand(CharacterHandler targetCharaHndl)
		{
			return 0;
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x00019D74 File Offset: 0x00018174
		public static int SolveConfusionCommand(CharacterHandler targetCharaHndl)
		{
			int num = 0;
			if (targetCharaHndl.IsUserControll)
			{
				int num2 = UnityEngine.Random.Range(0, targetCharaHndl.CharaBattle.CommandNum);
				if (targetCharaHndl.CharaBattle.CanBeExecCommand(num2))
				{
					num = num2;
				}
			}
			else
			{
				List<int> list = new List<int>();
				int commandNum = targetCharaHndl.CharaBattle.CommandNum;
				for (int i = 0; i < commandNum; i++)
				{
					BattleCommandData commandDataAt = targetCharaHndl.CharaBattle.GetCommandDataAt(i);
					if (commandDataAt != null && commandDataAt.CanUseOnConfusion)
					{
						list.Add(i);
					}
				}
				if (list.Count > 0)
				{
					int index = UnityEngine.Random.Range(0, list.Count);
					num = list[index];
				}
			}
			BattleCommandData commandDataAt2 = targetCharaHndl.CharaBattle.GetCommandDataAt(num);
			eSkillTargetType mainSkillTargetType = commandDataAt2.MainSkillTargetType;
			if (mainSkillTargetType == eSkillTargetType.TgtSingle || mainSkillTargetType == eSkillTargetType.MySingle)
			{
				targetCharaHndl.CharaBattle.MyParty.SingleTargetIndex = (BattleDefine.eJoinMember)UnityEngine.Random.Range(0, 3);
			}
			BattleCommandParser.OptimizeTargetIndex(targetCharaHndl, commandDataAt2.MainSkillTargetType);
			return num;
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x00019E84 File Offset: 0x00018284
		public static bool SolveParalysisCancel(CharacterHandler targetCharaHndl)
		{
			if (targetCharaHndl.CharaBattle.Param.IsEnableStateAbnormal(eStateAbnormal.Paralysis))
			{
				float num = UnityEngine.Random.Range(0f, 100f);
				if (num <= BattleUtility.DefVal(eBattleDefineDB.StateAbnormalParalysis_CancelProbability))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x00019EC8 File Offset: 0x000182C8
		public static int SolvePoison(CharacterHandler targetCharaHndl)
		{
			int num = (int)((float)targetCharaHndl.CharaBattle.Param.FixedMaxHp() * BattleUtility.DefVal(eBattleDefineDB.StateAbnormalPoison_DamageRatio) * 0.01f);
			if (num >= targetCharaHndl.CharaBattle.Param.GetHp())
			{
				num = targetCharaHndl.CharaBattle.Param.GetHp() - 1;
			}
			targetCharaHndl.CharaBattle.Param.CalcHp(-num);
			return -num;
		}

		// Token: 0x060004D3 RID: 1235 RVA: 0x00019F34 File Offset: 0x00018334
		public static int SolveRegene(CharacterHandler targetCharaHndl, out bool out_recoveryValueIsZeroBecauseUnhappy)
		{
			out_recoveryValueIsZeroBecauseUnhappy = false;
			BattleDefine.RegeneData regene = targetCharaHndl.CharaBattle.Param.GetRegene();
			if (regene == null || !regene.IsEnable())
			{
				return 0;
			}
			int result = 0;
			CharacterHandler characterHandler = targetCharaHndl.CharaBattle.MyParty.FindMemberByName(regene.OwnerName);
			if (characterHandler == null)
			{
				characterHandler = targetCharaHndl.CharaBattle.TgtParty.FindMemberByName(regene.OwnerName);
			}
			if (characterHandler != null)
			{
				int pow = regene.Pow;
				float num = 0f;
				result = BattleCommandParser._SolveRecover(characterHandler, targetCharaHndl, false, pow, 1f, out num, out out_recoveryValueIsZeroBecauseUnhappy);
			}
			return result;
		}

		// Token: 0x060004D4 RID: 1236 RVA: 0x00019FD4 File Offset: 0x000183D4
		public static void GetTakeCharaList(BattleCommandData command, out List<CharacterHandler> out_tgtCharaList, out List<CharacterHandler> out_myCharaList)
		{
			out_tgtCharaList = null;
			out_myCharaList = null;
			if (command.SkillContentSets != null)
			{
				bool[] array = new bool[5];
				for (int i = 0; i < command.SkillContentSets.Count; i++)
				{
					if (command.SkillContentSets[i] != null)
					{
						array[(int)command.SkillContentSets[i].m_Target] = true;
					}
				}
				if (array[2])
				{
					out_tgtCharaList = command.Owner.CharaBattle.TgtParty.GetJoinMembers(true);
				}
				else if (array[1])
				{
					out_tgtCharaList = new List<CharacterHandler>();
					out_tgtCharaList.Add(command.Owner.CharaBattle.TgtParty.GetMember(command.Owner.CharaBattle.MyParty.SingleTargetIndex));
				}
				if (array[4])
				{
					out_myCharaList = command.Owner.CharaBattle.MyParty.GetJoinMembers(true);
				}
				else if (array[3])
				{
					out_myCharaList = new List<CharacterHandler>();
					out_myCharaList.Add(command.Owner.CharaBattle.MyParty.GetMember(command.Owner.CharaBattle.MyParty.SingleTargetIndex));
				}
			}
		}

		// Token: 0x060004D5 RID: 1237 RVA: 0x0001A104 File Offset: 0x00018504
		public static List<CharacterHandler> GetTgtAllCharaHndlsFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			for (int i = 0; i < command.SkillContentSets.Count; i++)
			{
				if (command.SkillContentSets[i].m_Target == eSkillTargetType.TgtAll)
				{
					return executor.CharaBattle.TgtParty.GetJoinMembers(true);
				}
			}
			return null;
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x0001A158 File Offset: 0x00018558
		public static CharacterHandler GetTgtSingleCharaHndlFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			for (int i = 0; i < command.SkillContentSets.Count; i++)
			{
				if (command.SkillContentSets[i].m_Target == eSkillTargetType.TgtSingle)
				{
					return executor.CharaBattle.TgtParty.GetMember(executor.CharaBattle.MyParty.SingleTargetIndex);
				}
			}
			return null;
		}

		// Token: 0x060004D7 RID: 1239 RVA: 0x0001A1BC File Offset: 0x000185BC
		public static List<CharacterHandler> GetMyAllCharaHndlsFromCommand(CharacterHandler executor, BattleCommandData command, bool isIncludeExecutor)
		{
			for (int i = 0; i < command.SkillContentSets.Count; i++)
			{
				if (command.SkillContentSets[i].m_Target == eSkillTargetType.MyAll)
				{
					List<CharacterHandler> joinMembers = executor.CharaBattle.MyParty.GetJoinMembers(true);
					if (!isIncludeExecutor)
					{
						joinMembers.Remove(executor);
					}
					return joinMembers;
				}
			}
			return null;
		}

		// Token: 0x060004D8 RID: 1240 RVA: 0x0001A220 File Offset: 0x00018620
		public static CharacterHandler GetMySingleCharaHndlFromCommand(CharacterHandler executor, BattleCommandData command)
		{
			for (int i = 0; i < command.SkillContentSets.Count; i++)
			{
				if (command.SkillContentSets[i].m_Target == eSkillTargetType.MySingle)
				{
					return executor.CharaBattle.MyParty.GetMember(executor.CharaBattle.MyParty.SingleTargetIndex);
				}
			}
			return null;
		}

		// Token: 0x020000A7 RID: 167
		private enum eSkillContentTypeAttackOption
		{
			// Token: 0x0400031F RID: 799
			None,
			// Token: 0x04000320 RID: 800
			Abnormal,
			// Token: 0x04000321 RID: 801
			MaxHpRatio
		}
	}
}
