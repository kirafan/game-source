﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200016E RID: 366
	public class CharacterExpDB : ScriptableObject
	{
		// Token: 0x040009C6 RID: 2502
		public CharacterExpDB_Param[] m_Params;
	}
}
