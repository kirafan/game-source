﻿using System;

namespace Star
{
	// Token: 0x020004F5 RID: 1269
	public enum eXlsMissionRoomFuncType
	{
		// Token: 0x04001FD0 RID: 8144
		ObjectSet,
		// Token: 0x04001FD1 RID: 8145
		ObjectRemove,
		// Token: 0x04001FD2 RID: 8146
		CharaTouch
	}
}
