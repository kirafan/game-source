﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200072C RID: 1836
	public class TownHitNodeState : MonoBehaviour
	{
		// Token: 0x06002439 RID: 9273 RVA: 0x000C257F File Offset: 0x000C097F
		public void CreateHitTable(int fmax)
		{
			this.m_Table = new TownHitNodeState.HitState[fmax];
			this.m_TableNum = 0;
			this.m_LocTable = new List<TownHitNodeState.LocState>();
		}

		// Token: 0x0600243A RID: 9274 RVA: 0x000C25A0 File Offset: 0x000C09A0
		public void AddHitNode(GameObject pmodel, GameObject ptrs, eTownOption ftype, int fkey)
		{
			this.m_Table[this.m_TableNum] = new TownHitNodeState.HitState();
			this.m_Table[this.m_TableNum].m_Model = pmodel;
			this.m_Table[this.m_TableNum].m_Node = ptrs;
			this.m_Table[this.m_TableNum].m_Type = ftype;
			this.m_Table[this.m_TableNum].m_HitKey = fkey;
			this.m_TableNum++;
		}

		// Token: 0x0600243B RID: 9275 RVA: 0x000C261C File Offset: 0x000C0A1C
		public void AddLocNode(Transform ptrs, eTownOption ftype, int fkey)
		{
			TownHitNodeState.LocState locState = new TownHitNodeState.LocState();
			locState.m_Node = ptrs;
			locState.m_Type = ftype;
			locState.m_Key = fkey;
			this.m_LocTable.Add(locState);
		}

		// Token: 0x0600243C RID: 9276 RVA: 0x000C2650 File Offset: 0x000C0A50
		public GameObject GetHitNode(int fkey)
		{
			for (int i = 0; i < this.m_TableNum; i++)
			{
				if (this.m_Table[i].m_HitKey == fkey)
				{
					return this.m_Table[i].m_Model;
				}
			}
			return null;
		}

		// Token: 0x0600243D RID: 9277 RVA: 0x000C2698 File Offset: 0x000C0A98
		public TownHitNodeState.HitState GetHitType(eTownOption ftype)
		{
			for (int i = 0; i < this.m_TableNum; i++)
			{
				if (this.m_Table[i].m_Type == ftype)
				{
					return this.m_Table[i];
				}
			}
			return null;
		}

		// Token: 0x0600243E RID: 9278 RVA: 0x000C26DC File Offset: 0x000C0ADC
		public void SetHitActive(eTownOption ftype, bool factive)
		{
			for (int i = 0; i < this.m_TableNum; i++)
			{
				if (this.m_Table[i].m_Type == ftype)
				{
					this.m_Table[i].m_Model.gameObject.SetActive(factive);
				}
			}
		}

		// Token: 0x0600243F RID: 9279 RVA: 0x000C272C File Offset: 0x000C0B2C
		public Transform GetLocNode(int fkey)
		{
			for (int i = 0; i < this.m_LocTable.Count; i++)
			{
				if (this.m_LocTable[i].m_Key == fkey)
				{
					return this.m_LocTable[i].m_Node;
				}
			}
			return null;
		}

		// Token: 0x04002B34 RID: 11060
		public TownHitNodeState.HitState[] m_Table;

		// Token: 0x04002B35 RID: 11061
		public int m_TableNum;

		// Token: 0x04002B36 RID: 11062
		public List<TownHitNodeState.LocState> m_LocTable;

		// Token: 0x0200072D RID: 1837
		[Serializable]
		public class HitState
		{
			// Token: 0x04002B37 RID: 11063
			public GameObject m_Model;

			// Token: 0x04002B38 RID: 11064
			public GameObject m_Node;

			// Token: 0x04002B39 RID: 11065
			public eTownOption m_Type;

			// Token: 0x04002B3A RID: 11066
			public int m_HitKey;
		}

		// Token: 0x0200072E RID: 1838
		public class LocState
		{
			// Token: 0x04002B3B RID: 11067
			public Transform m_Node;

			// Token: 0x04002B3C RID: 11068
			public eTownOption m_Type;

			// Token: 0x04002B3D RID: 11069
			public int m_Key;
		}
	}
}
