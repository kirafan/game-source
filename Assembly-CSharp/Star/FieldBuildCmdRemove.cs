﻿using System;

namespace Star
{
	// Token: 0x02000318 RID: 792
	public class FieldBuildCmdRemove : FieldBuilCmdBase
	{
		// Token: 0x06000F11 RID: 3857 RVA: 0x00050BA3 File Offset: 0x0004EFA3
		public FieldBuildCmdRemove(long fmanageid, int fpoint)
		{
			this.m_BuildPoint = fpoint;
			this.m_ManageID = fmanageid;
			this.m_Cmd = FieldBuilCmdBase.eCmd.Remove;
		}

		// Token: 0x04001698 RID: 5784
		public int m_BuildPoint;

		// Token: 0x04001699 RID: 5785
		public long m_ManageID;
	}
}
