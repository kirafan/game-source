﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000C6 RID: 198
	public class BattleKiraraJumpObjectHandler : MonoBehaviour
	{
		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600054F RID: 1359 RVA: 0x0001B67C File Offset: 0x00019A7C
		public MsbHandler MsbHndl
		{
			get
			{
				return this.m_MsbHndl;
			}
		}

		// Token: 0x06000550 RID: 1360 RVA: 0x0001B684 File Offset: 0x00019A84
		public void Setup(string resourceName, MeigeAnimClipHolder meigeAnimClipHolder, List<CharacterHandler> charaHndls)
		{
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(resourceName, false);
			this.m_BaseTransform = base.transform.Find(name);
			if (this.m_BaseTransform == null)
			{
				return;
			}
			this.m_Anim = this.m_BaseTransform.gameObject.GetComponent<Animation>();
			this.m_Anim.cullingType = AnimationCullingType.AlwaysAnimate;
			this.m_MeigeAnimCtrl = this.m_BaseTransform.gameObject.AddComponent<MeigeAnimCtrl>();
			this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
			this.m_MeigeAnimCtrl.Open();
			this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, meigeAnimClipHolder);
			this.m_MeigeAnimCtrl.Close();
			this.m_CharaHndls = charaHndls;
			for (int i = 0; i < this.m_CharaHndls.Count; i++)
			{
				Transform locator = this.GetLocator(this.m_CharaHndls.Count, i);
				this.SetCharaParent(this.m_CharaHndls[i], locator);
			}
		}

		// Token: 0x06000551 RID: 1361 RVA: 0x0001B77D File Offset: 0x00019B7D
		public void Destroy()
		{
			this.DetachAllCharas();
			this.DetachCamera();
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			this.m_MsbHndl = null;
		}

		// Token: 0x06000552 RID: 1362 RVA: 0x0001B7A0 File Offset: 0x00019BA0
		public void DetachAllCharas()
		{
			if (this.m_CharaHndls != null)
			{
				for (int i = 0; i < this.m_CharaHndls.Count; i++)
				{
					this.m_CharaHndls[i].CacheTransform.SetParent(null, false);
				}
				this.m_CharaHndls = null;
			}
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x0001B7F4 File Offset: 0x00019BF4
		private Transform GetLocator(int totalCharaNum, int index)
		{
			string name = string.Format("loc_{0}_{1}", totalCharaNum, index);
			return this.m_BaseTransform.Find(name);
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x0001B824 File Offset: 0x00019C24
		public void AttachCamera(Camera camera)
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.AttachCamera(0, camera);
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x0001B845 File Offset: 0x00019C45
		public void DetachCamera()
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.DetachCamera();
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x0001B864 File Offset: 0x00019C64
		public void Play()
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.ClampForever);
			for (int j = 0; j < this.m_CharaHndls.Count; j++)
			{
				this.m_CharaHndls[j].CharaBattle.RequestKiraraJump(j);
			}
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x0001B90E File Offset: 0x00019D0E
		public void Pause()
		{
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0001B930 File Offset: 0x00019D30
		public bool IsPlaying()
		{
			WrapMode wrapMode = this.m_MeigeAnimCtrl.m_WrapMode;
			if (wrapMode != WrapMode.ClampForever)
			{
				if (this.m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
				{
					return true;
				}
			}
			else if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0001B989 File Offset: 0x00019D89
		private void SetCharaParent(CharacterHandler charaHndl, Transform parent)
		{
			charaHndl.CacheTransform.SetParent(parent, false);
			charaHndl.CacheTransform.localScale = Vector3.one;
			charaHndl.CacheTransform.localPosition = Vector3.zero;
		}

		// Token: 0x0400041A RID: 1050
		private const string LOCATOR_KEY = "loc_{0}_{1}";

		// Token: 0x0400041B RID: 1051
		private const string PLAY_KEY = "Take 001";

		// Token: 0x0400041C RID: 1052
		private Transform m_BaseTransform;

		// Token: 0x0400041D RID: 1053
		private Animation m_Anim;

		// Token: 0x0400041E RID: 1054
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400041F RID: 1055
		private MsbHandler m_MsbHndl;

		// Token: 0x04000420 RID: 1056
		private List<CharacterHandler> m_CharaHndls;
	}
}
