﻿using System;

namespace Star
{
	// Token: 0x02000310 RID: 784
	public enum eXlsScheduleTagID
	{
		// Token: 0x04001642 RID: 5698
		def1,
		// Token: 0x04001643 RID: 5699
		def2,
		// Token: 0x04001644 RID: 5700
		def3,
		// Token: 0x04001645 RID: 5701
		def4,
		// Token: 0x04001646 RID: 5702
		def5,
		// Token: 0x04001647 RID: 5703
		def6,
		// Token: 0x04001648 RID: 5704
		def7,
		// Token: 0x04001649 RID: 5705
		def8,
		// Token: 0x0400164A RID: 5706
		def9,
		// Token: 0x0400164B RID: 5707
		def10,
		// Token: 0x0400164C RID: 5708
		def11,
		// Token: 0x0400164D RID: 5709
		def12,
		// Token: 0x0400164E RID: 5710
		def13,
		// Token: 0x0400164F RID: 5711
		def14,
		// Token: 0x04001650 RID: 5712
		def15,
		// Token: 0x04001651 RID: 5713
		def16,
		// Token: 0x04001652 RID: 5714
		def17,
		// Token: 0x04001653 RID: 5715
		def18,
		// Token: 0x04001654 RID: 5716
		def19,
		// Token: 0x04001655 RID: 5717
		def20,
		// Token: 0x04001656 RID: 5718
		def21,
		// Token: 0x04001657 RID: 5719
		def22,
		// Token: 0x04001658 RID: 5720
		def23,
		// Token: 0x04001659 RID: 5721
		def24,
		// Token: 0x0400165A RID: 5722
		def25,
		// Token: 0x0400165B RID: 5723
		def26,
		// Token: 0x0400165C RID: 5724
		def27,
		// Token: 0x0400165D RID: 5725
		def28,
		// Token: 0x0400165E RID: 5726
		def29,
		// Token: 0x0400165F RID: 5727
		def30,
		// Token: 0x04001660 RID: 5728
		def31,
		// Token: 0x04001661 RID: 5729
		def32,
		// Token: 0x04001662 RID: 5730
		def33,
		// Token: 0x04001663 RID: 5731
		def34,
		// Token: 0x04001664 RID: 5732
		def35,
		// Token: 0x04001665 RID: 5733
		def36,
		// Token: 0x04001666 RID: 5734
		def37,
		// Token: 0x04001667 RID: 5735
		def38,
		// Token: 0x04001668 RID: 5736
		def39,
		// Token: 0x04001669 RID: 5737
		def40,
		// Token: 0x0400166A RID: 5738
		def41,
		// Token: 0x0400166B RID: 5739
		def42,
		// Token: 0x0400166C RID: 5740
		def43,
		// Token: 0x0400166D RID: 5741
		def44,
		// Token: 0x0400166E RID: 5742
		def45,
		// Token: 0x0400166F RID: 5743
		def46,
		// Token: 0x04001670 RID: 5744
		def47,
		// Token: 0x04001671 RID: 5745
		def48,
		// Token: 0x04001672 RID: 5746
		def49,
		// Token: 0x04001673 RID: 5747
		def50,
		// Token: 0x04001674 RID: 5748
		def51,
		// Token: 0x04001675 RID: 5749
		def52,
		// Token: 0x04001676 RID: 5750
		def53,
		// Token: 0x04001677 RID: 5751
		def54,
		// Token: 0x04001678 RID: 5752
		def55
	}
}
