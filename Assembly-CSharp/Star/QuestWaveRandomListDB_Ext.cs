﻿using System;

namespace Star
{
	// Token: 0x020001A2 RID: 418
	public static class QuestWaveRandomListDB_Ext
	{
		// Token: 0x06000B0D RID: 2829 RVA: 0x00041D20 File Offset: 0x00040120
		public static QuestWaveRandomListDB_Param GetParam(this QuestWaveRandomListDB self, int randomID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == randomID)
				{
					return self.m_Params[i];
				}
			}
			return default(QuestWaveRandomListDB_Param);
		}
	}
}
