﻿using System;
using System.Collections.Generic;
using TownResponseTypes;

namespace Star
{
	// Token: 0x02000684 RID: 1668
	public class TownComSetUp : IFldNetComModule
	{
		// Token: 0x060021B0 RID: 8624 RVA: 0x000B35B5 File Offset: 0x000B19B5
		public TownComSetUp() : base(IFldNetComManager.Instance)
		{
			this.m_DefaultStep = new List<TownComSetUp.eStep>();
		}

		// Token: 0x060021B1 RID: 8625 RVA: 0x000B35D0 File Offset: 0x000B19D0
		public bool SetUp()
		{
			TownComAPIGetAll townComAPIGetAll = new TownComAPIGetAll();
			townComAPIGetAll.playerId = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID;
			this.m_NetMng.SendCom(townComAPIGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x060021B2 RID: 8626 RVA: 0x000B3618 File Offset: 0x000B1A18
		public bool DefaultBuildUp()
		{
			UserTownData townData = UserDataUtil.TownData;
			if (townData.MngID == -1L)
			{
				TownComAPINewSet townComAPINewSet = new TownComAPINewSet();
				townComAPINewSet.gridData = UserTownUtil.CreateTownBuildListString(null);
				this.m_NetMng.SendCom(townComAPINewSet, new INetComHandle.ResponseCallbak(this.CallbackDefaultBuildUp), false);
			}
			else
			{
				TownComAPISet townComAPISet = new TownComAPISet();
				townComAPISet.managedTownId = townData.MngID;
				townComAPISet.gridData = UserTownUtil.CreateTownBuildListString(null);
				this.m_NetMng.SendCom(townComAPISet, new INetComHandle.ResponseCallbak(this.CallbackDefaultBuildUp), false);
			}
			return true;
		}

		// Token: 0x060021B3 RID: 8627 RVA: 0x000B36A0 File Offset: 0x000B1AA0
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case TownComSetUp.eStep.GetState:
				this.SetUp();
				this.m_Step = TownComSetUp.eStep.WaitCheck;
				break;
			case TownComSetUp.eStep.DefaultBuildUp:
				this.DefaultBuildUp();
				this.m_Step = TownComSetUp.eStep.SetUpCheck;
				break;
			case TownComSetUp.eStep.DefStepChk:
				if (this.m_DefaultStep.Count == 0)
				{
					this.m_Step = TownComSetUp.eStep.End;
				}
				else
				{
					this.m_Step = this.m_DefaultStep[0];
					this.m_DefaultStep.RemoveAt(0);
				}
				break;
			case TownComSetUp.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x060021B4 RID: 8628 RVA: 0x000B3750 File Offset: 0x000B1B50
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				WWWComUtil.SetTownBuildListKey(getAll.managedTowns, true);
				this.CheckTownBuildListKey();
				if (this.m_DefaultStep.Count == 0)
				{
					this.m_Step = TownComSetUp.eStep.End;
				}
				else
				{
					this.m_Step = TownComSetUp.eStep.DefStepChk;
				}
			}
		}

		// Token: 0x060021B5 RID: 8629 RVA: 0x000B37A4 File Offset: 0x000B1BA4
		private void CallbackDefaultBuildUp(INetComHandle phandle)
		{
			Set set = phandle.GetResponse() as Set;
			if (set != null)
			{
				UserDataUtil.TownData.UpManageID(set.managedTownId);
				this.m_Step = TownComSetUp.eStep.DefStepChk;
			}
		}

		// Token: 0x060021B6 RID: 8630 RVA: 0x000B37DC File Offset: 0x000B1BDC
		private void CheckTownBuildListKey()
		{
			bool flag = true;
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			List<UserTownObjectData> townObjData = UserDataUtil.TownObjData;
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 0 && db.m_Params[i].m_WakeUp == 0)
				{
					bool flag2 = false;
					for (int j = 0; j < UserDataUtil.TownData.GetBuildObjectDataNum(); j++)
					{
						if (db.m_Params[i].m_ObjectNo == UserDataUtil.TownData.GetBuildObjectDataAt(j).m_ObjID)
						{
							flag2 = true;
							break;
						}
					}
					if (!flag2)
					{
						for (int j = 0; j < townObjData.Count; j++)
						{
							if (townObjData[j].ObjID == db.m_Params[i].m_ObjectNo)
							{
								UserTownData.BuildObjectData buildObject = UserTownUtil.CreateUserTownBuildData(townObjData[j].m_ManageID, townObjData[j].BuildPoint, townObjData[j].ObjID, true, townObjData[j].BuildTime, 0L);
								UserDataUtil.TownData.AddBuildObjectData(buildObject);
								flag = false;
								break;
							}
						}
					}
				}
			}
			if (!flag || UserDataUtil.TownData.MngID == -1L)
			{
				this.m_DefaultStep.Add(TownComSetUp.eStep.DefaultBuildUp);
			}
		}

		// Token: 0x04002817 RID: 10263
		private TownComSetUp.eStep m_Step;

		// Token: 0x04002818 RID: 10264
		private List<TownComSetUp.eStep> m_DefaultStep;

		// Token: 0x02000685 RID: 1669
		public enum eStep
		{
			// Token: 0x0400281A RID: 10266
			GetState,
			// Token: 0x0400281B RID: 10267
			WaitCheck,
			// Token: 0x0400281C RID: 10268
			DefaultBuildUp,
			// Token: 0x0400281D RID: 10269
			SetUpCheck,
			// Token: 0x0400281E RID: 10270
			DefStepChk,
			// Token: 0x0400281F RID: 10271
			End
		}
	}
}
