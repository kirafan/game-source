﻿using System;

namespace Star
{
	// Token: 0x02000576 RID: 1398
	public class ActXlsKeyObjPlayAnim : ActXlsKeyBase
	{
		// Token: 0x06001B29 RID: 6953 RVA: 0x0008F9C7 File Offset: 0x0008DDC7
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_PlayAnimNo);
			pio.Int(ref this.m_AnimeKey);
			pio.Bool(ref this.m_Loop);
			pio.String(ref this.m_AnmName);
		}

		// Token: 0x0400221A RID: 8730
		public int m_PlayAnimNo;

		// Token: 0x0400221B RID: 8731
		public int m_AnimeKey;

		// Token: 0x0400221C RID: 8732
		public bool m_Loop;

		// Token: 0x0400221D RID: 8733
		public string m_AnmName;
	}
}
