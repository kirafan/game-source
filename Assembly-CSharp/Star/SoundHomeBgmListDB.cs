﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000216 RID: 534
	public class SoundHomeBgmListDB : ScriptableObject
	{
		// Token: 0x04000D3F RID: 3391
		public SoundHomeBgmListDB_Param[] m_Params;
	}
}
