﻿using System;

namespace Star
{
	// Token: 0x0200060A RID: 1546
	public struct IVector4
	{
		// Token: 0x06001E57 RID: 7767 RVA: 0x000A39C3 File Offset: 0x000A1DC3
		public IVector4(int fx, int fy, int fz, int fw)
		{
			this.x = fx;
			this.y = fy;
			this.z = fz;
			this.w = fw;
		}

		// Token: 0x040024E5 RID: 9445
		public int x;

		// Token: 0x040024E6 RID: 9446
		public int y;

		// Token: 0x040024E7 RID: 9447
		public int z;

		// Token: 0x040024E8 RID: 9448
		public int w;
	}
}
