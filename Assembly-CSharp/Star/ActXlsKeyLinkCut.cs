﻿using System;

namespace Star
{
	// Token: 0x02000567 RID: 1383
	public class ActXlsKeyLinkCut : ActXlsKeyBase
	{
		// Token: 0x06001B0B RID: 6923 RVA: 0x0008F61E File Offset: 0x0008DA1E
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.String(ref this.m_BindHrcName);
			pio.Int(ref this.m_LinkTime);
		}

		// Token: 0x040021F5 RID: 8693
		public string m_BindHrcName;

		// Token: 0x040021F6 RID: 8694
		public int m_LinkTime;
	}
}
