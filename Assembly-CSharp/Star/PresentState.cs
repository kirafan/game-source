﻿using System;

namespace Star
{
	// Token: 0x02000452 RID: 1106
	public class PresentState : GameStateBase
	{
		// Token: 0x06001557 RID: 5463 RVA: 0x0006F560 File Offset: 0x0006D960
		public PresentState(PresentMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001558 RID: 5464 RVA: 0x0006F576 File Offset: 0x0006D976
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x06001559 RID: 5465 RVA: 0x0006F579 File Offset: 0x0006D979
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600155A RID: 5466 RVA: 0x0006F57B File Offset: 0x0006D97B
		public override void OnStateExit()
		{
		}

		// Token: 0x0600155B RID: 5467 RVA: 0x0006F57D File Offset: 0x0006D97D
		public override void OnDispose()
		{
		}

		// Token: 0x0600155C RID: 5468 RVA: 0x0006F57F File Offset: 0x0006D97F
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0600155D RID: 5469 RVA: 0x0006F582 File Offset: 0x0006D982
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001C2E RID: 7214
		protected PresentMain m_Owner;

		// Token: 0x04001C2F RID: 7215
		protected int m_NextState = -1;
	}
}
