﻿using System;

namespace Star
{
	// Token: 0x0200056D RID: 1389
	public class ActXlsKeyProgramEvent : ActXlsKeyBase
	{
		// Token: 0x06001B17 RID: 6935 RVA: 0x0008F774 File Offset: 0x0008DB74
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_EventID);
			pio.Int(ref this.m_OptionKey);
			pio.Int(ref this.m_EvtTime);
		}

		// Token: 0x04002204 RID: 8708
		public int m_EventID;

		// Token: 0x04002205 RID: 8709
		public int m_OptionKey;

		// Token: 0x04002206 RID: 8710
		public int m_EvtTime;
	}
}
