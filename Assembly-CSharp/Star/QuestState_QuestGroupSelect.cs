﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using Star.UI.Quest;

namespace Star
{
	// Token: 0x02000465 RID: 1125
	public class QuestState_QuestGroupSelect : QuestState
	{
		// Token: 0x060015D8 RID: 5592 RVA: 0x00071DBF File Offset: 0x000701BF
		public QuestState_QuestGroupSelect(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x060015D9 RID: 5593 RVA: 0x00071DD6 File Offset: 0x000701D6
		public override int GetStateID()
		{
			return 4;
		}

		// Token: 0x060015DA RID: 5594 RVA: 0x00071DD9 File Offset: 0x000701D9
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_QuestGroupSelect.eStep.First;
		}

		// Token: 0x060015DB RID: 5595 RVA: 0x00071DE2 File Offset: 0x000701E2
		public override void OnStateExit()
		{
		}

		// Token: 0x060015DC RID: 5596 RVA: 0x00071DE4 File Offset: 0x000701E4
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060015DD RID: 5597 RVA: 0x00071DF0 File Offset: 0x000701F0
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_QuestGroupSelect.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = QuestState_QuestGroupSelect.eStep.LoadWait;
				break;
			case QuestState_QuestGroupSelect.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestGroupSelect.eStep.PlayIn;
				}
				break;
			case QuestState_QuestGroupSelect.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackADVPop(new Action(this.OnConfirmADVUnlock));
					this.m_Step = QuestState_QuestGroupSelect.eStep.Main;
				}
				break;
			case QuestState_QuestGroupSelect.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (!this.m_IsTransitADV)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
						{
							this.m_NextState = 2147483646;
						}
						else
						{
							QuestGroupSelectUI.eButton selectButton = this.m_UI.SelectButton;
							if (selectButton != QuestGroupSelectUI.eButton.Group)
							{
								this.m_NextState = 1;
							}
							else
							{
								QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
								selectQuest.m_GroupID = this.m_UI.SelectGroupID;
								selectQuest.m_QuestID = -1;
								this.m_NextState = 4;
							}
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = QuestState_QuestGroupSelect.eStep.UnloadChildSceneWait;
				}
				break;
			case QuestState_QuestGroupSelect.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = QuestState_QuestGroupSelect.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060015DE RID: 5598 RVA: 0x00071F88 File Offset: 0x00070388
		private void OnConfirmADVUnlock()
		{
			this.GoToMenuEnd();
			this.m_NextState = 2147483646;
			this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			globalParam.SetAdvParam(globalParam.AdvID, SceneDefine.eSceneID.Quest, false);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.List;
			this.m_IsTransitADV = true;
		}

		// Token: 0x060015DF RID: 5599 RVA: 0x00071FE4 File Offset: 0x000703E4
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<QuestGroupSelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
			List<QuestManager.EventGroupData> eventGroupDatasFromGroupID = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEventGroupDatasFromGroupID(selectQuest.m_EventType);
			this.m_UI.Setup(eventGroupDatasFromGroupID);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Quest);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060015E0 RID: 5600 RVA: 0x00072092 File Offset: 0x00070492
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			if (this.m_UI.SelectButton != QuestGroupSelectUI.eButton.Group)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			}
			this.GoToMenuEnd();
		}

		// Token: 0x060015E1 RID: 5601 RVA: 0x000720C5 File Offset: 0x000704C5
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001C91 RID: 7313
		private QuestState_QuestGroupSelect.eStep m_Step = QuestState_QuestGroupSelect.eStep.None;

		// Token: 0x04001C92 RID: 7314
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.QuestGroupSelectUI;

		// Token: 0x04001C93 RID: 7315
		public QuestGroupSelectUI m_UI;

		// Token: 0x04001C94 RID: 7316
		private bool m_IsTransitADV;

		// Token: 0x02000466 RID: 1126
		private enum eStep
		{
			// Token: 0x04001C96 RID: 7318
			None = -1,
			// Token: 0x04001C97 RID: 7319
			First,
			// Token: 0x04001C98 RID: 7320
			LoadWait,
			// Token: 0x04001C99 RID: 7321
			PlayIn,
			// Token: 0x04001C9A RID: 7322
			Main,
			// Token: 0x04001C9B RID: 7323
			UnloadChildSceneWait
		}
	}
}
