﻿using System;

namespace Star
{
	// Token: 0x020004F1 RID: 1265
	public enum eXlsMissionSystemFuncType
	{
		// Token: 0x04001FB6 RID: 8118
		Login,
		// Token: 0x04001FB7 RID: 8119
		ContinutiyLogin,
		// Token: 0x04001FB8 RID: 8120
		MissionAllComplete,
		// Token: 0x04001FB9 RID: 8121
		UserRank
	}
}
