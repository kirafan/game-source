﻿using System;

namespace Star
{
	// Token: 0x02000339 RID: 825
	public interface BinaryIO
	{
		// Token: 0x06000FC6 RID: 4038
		int Enum(Enum fkey, Type ftype);

		// Token: 0x06000FC7 RID: 4039
		void Bool(ref bool fkey);

		// Token: 0x06000FC8 RID: 4040
		void Byte(ref byte fkey);

		// Token: 0x06000FC9 RID: 4041
		void Short(ref short fkey);

		// Token: 0x06000FCA RID: 4042
		void Int(ref int fkey);

		// Token: 0x06000FCB RID: 4043
		void Long(ref long fkey);

		// Token: 0x06000FCC RID: 4044
		void Float(ref float fkey);

		// Token: 0x06000FCD RID: 4045
		void String(ref string fkey);

		// Token: 0x06000FCE RID: 4046
		void ByteTable(ref byte[] fkey);

		// Token: 0x06000FCF RID: 4047
		void ShortTable(ref short[] fkey);

		// Token: 0x06000FD0 RID: 4048
		void IntTable(ref int[] fkey);
	}
}
