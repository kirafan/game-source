﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020000A3 RID: 163
	[Serializable]
	public class BattleCommandData
	{
		// Token: 0x17000038 RID: 56
		// (get) Token: 0x0600047D RID: 1149 RVA: 0x00016918 File Offset: 0x00014D18
		public BattleCommandData.eCommandType CommandType
		{
			get
			{
				return this.m_CommandType;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600047E RID: 1150 RVA: 0x00016920 File Offset: 0x00014D20
		public int CommandIndex
		{
			get
			{
				return this.m_CommandIndex;
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600047F RID: 1151 RVA: 0x00016928 File Offset: 0x00014D28
		public int SkillID
		{
			get
			{
				return this.m_SkillID;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000480 RID: 1152 RVA: 0x00016930 File Offset: 0x00014D30
		public int SkillLv
		{
			get
			{
				return this.m_SkillLv;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000481 RID: 1153 RVA: 0x00016938 File Offset: 0x00014D38
		public SkillListDB_Param SkillParam
		{
			get
			{
				return this.m_SkillParam;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000482 RID: 1154 RVA: 0x00016940 File Offset: 0x00014D40
		public List<BattleCommandData.SkillContentSet> SkillContentSets
		{
			get
			{
				return this.m_SkillContentSets;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000483 RID: 1155 RVA: 0x00016948 File Offset: 0x00014D48
		public string UniqueSkillScene
		{
			get
			{
				if (!string.IsNullOrEmpty(this.m_SkillParam.m_UniqueSkillScene))
				{
					return this.m_SkillParam.m_UniqueSkillScene;
				}
				return null;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000484 RID: 1156 RVA: 0x0001696C File Offset: 0x00014D6C
		public bool IsExistUniqueSkillScene
		{
			get
			{
				return !string.IsNullOrEmpty(this.UniqueSkillScene);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000485 RID: 1157 RVA: 0x00016981 File Offset: 0x00014D81
		public string SAP_ID
		{
			get
			{
				return this.m_SkillParam.m_SAP;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x0001698E File Offset: 0x00014D8E
		public string SAG_ID
		{
			get
			{
				return this.m_SkillParam.m_SAG;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000487 RID: 1159 RVA: 0x0001699B File Offset: 0x00014D9B
		public eSkillTargetType MainSkillTargetType
		{
			get
			{
				return this.m_MainSkillTargetType;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000488 RID: 1160 RVA: 0x000169A3 File Offset: 0x00014DA3
		public string ActionKey
		{
			get
			{
				return this.m_ActKey;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000489 RID: 1161 RVA: 0x000169AB File Offset: 0x00014DAB
		public float Ratio
		{
			get
			{
				if ((float)this.m_RecastMax <= 0f)
				{
					return 0f;
				}
				return 1f - (float)this.RecastVal / (float)this.m_RecastMax;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600048A RID: 1162 RVA: 0x000169D9 File Offset: 0x00014DD9
		public int RecastMax
		{
			get
			{
				return this.m_RecastMax;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x0600048B RID: 1163 RVA: 0x000169E1 File Offset: 0x00014DE1
		public bool RecastRecoveredFlg
		{
			get
			{
				return this.m_RecastRecoveredFlg;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600048C RID: 1164 RVA: 0x000169E9 File Offset: 0x00014DE9
		public float LoadFactor
		{
			get
			{
				return this.m_LoadFactor;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600048D RID: 1165 RVA: 0x000169F1 File Offset: 0x00014DF1
		public CharacterHandler Owner
		{
			get
			{
				return this.m_Owner;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600048E RID: 1166 RVA: 0x000169F9 File Offset: 0x00014DF9
		public BattleCommandSolveResult SolveResult
		{
			get
			{
				return this.m_SolveResult;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600048F RID: 1167 RVA: 0x00016A01 File Offset: 0x00014E01
		public bool CanUseOnConfusion
		{
			get
			{
				return this.m_CanUseOnConfusion;
			}
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x00016A09 File Offset: 0x00014E09
		public bool IsAvailable()
		{
			return this.m_CommandIndex != -1 || this.m_SkillID != -1;
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x00016A28 File Offset: 0x00014E28
		public void Setup(BattleCommandData.eCommandType type, int commandIndex, CharacterHandler owner, int skillID, int skillLv, int skillMaxLv, long totalUseNum, sbyte expTableID, bool canUseOnConfusion)
		{
			this.m_CommandType = type;
			this.m_CommandIndex = commandIndex;
			this.m_Owner = owner;
			this.m_SkillID = skillID;
			this.m_SkillLv = skillLv;
			this.m_SkillMaxLv = skillMaxLv;
			this.m_TotalUseNum = totalUseNum;
			this.m_ExpTableID = expTableID;
			this.m_CanUseOnConfusion = canUseOnConfusion;
			this.m_SkillContentSets = new List<BattleCommandData.SkillContentSet>();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			SkillListDB self;
			SkillContentListDB self2;
			if (this.m_CommandType == BattleCommandData.eCommandType.MasterSkill)
			{
				self = dbMng.SkillListDB_MST;
				self2 = dbMng.SkillContentListDB_MST;
			}
			else if (this.m_CommandType == BattleCommandData.eCommandType.WeaponSkill)
			{
				self = dbMng.SkillListDB_WPN;
				self2 = dbMng.SkillContentListDB_WPN;
			}
			else if (this.m_CommandType == BattleCommandData.eCommandType.Card)
			{
				self = dbMng.SkillListDB_CARD;
				self2 = dbMng.SkillContentListDB_CARD;
			}
			else if (this.m_Owner != null && this.m_Owner.IsUserControll)
			{
				self = dbMng.SkillListDB_PL;
				self2 = dbMng.SkillContentListDB_PL;
			}
			else
			{
				self = dbMng.SkillListDB_EN;
				self2 = dbMng.SkillContentListDB_EN;
			}
			switch (this.m_CommandType)
			{
			case BattleCommandData.eCommandType.NormalAttack:
				this.m_SkillID = dbMng.ClassListDB.GetParam(this.m_Owner.CharaParam.ClassType).m_NormalAttackSkillID;
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = 0;
				this.m_LoadFactor = SkillContentListDB_Ext.GetLoadFactorFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				this.m_ActKey = "attack";
				break;
			case BattleCommandData.eCommandType.Guard:
				this.m_SkillID = 0;
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = 0;
				this.m_LoadFactor = (float)SkillContentListDB_Ext.GetRecastFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				this.m_ActKey = string.Empty;
				break;
			case BattleCommandData.eCommandType.Skill:
			case BattleCommandData.eCommandType.WeaponSkill:
			{
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = SkillContentListDB_Ext.GetRecastFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				this.m_LoadFactor = SkillContentListDB_Ext.GetLoadFactorFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				string skillActionGraphicsID = self.GetSkillActionGraphicsID(this.m_SkillParam.m_ID);
				if (this.m_Owner.CharaBattle.SAGs.ContainsKey(skillActionGraphicsID))
				{
					SkillActionGraphics skillActionGraphics = this.m_Owner.CharaBattle.SAGs[skillActionGraphicsID];
					if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
					{
						this.m_ActKey = "class_skill_" + skillActionGraphics.m_SettingMotionID;
					}
					else
					{
						this.m_ActKey = "skill_" + skillActionGraphics.m_SettingMotionID;
					}
				}
				break;
			}
			case BattleCommandData.eCommandType.UniqueSkill:
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = SkillContentListDB_Ext.GetRecastFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				this.m_LoadFactor = SkillContentListDB_Ext.GetLoadFactorFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				if (this.m_Owner.CharaResource.ResourceType == eCharaResourceType.Player)
				{
					string skillActionGraphicsID2 = self.GetSkillActionGraphicsID(this.m_SkillParam.m_ID);
					if (this.m_Owner.CharaBattle.SAGs.ContainsKey(skillActionGraphicsID2))
					{
						SkillActionGraphics skillActionGraphics2 = this.m_Owner.CharaBattle.SAGs[skillActionGraphicsID2];
						this.m_ActKey = "class_skill_" + skillActionGraphics2.m_SettingMotionID;
					}
					else
					{
						this.m_ActKey = "attack";
					}
				}
				else
				{
					string skillActionGraphicsID3 = self.GetSkillActionGraphicsID(this.m_SkillParam.m_ID);
					if (this.m_Owner.CharaResource.IsBosssResource)
					{
						this.m_ActKey = "charge_skill";
					}
					else if (this.m_Owner.CharaBattle.SAGs.ContainsKey(skillActionGraphicsID3))
					{
						SkillActionGraphics skillActionGraphics3 = this.m_Owner.CharaBattle.SAGs[skillActionGraphicsID3];
						this.m_ActKey = "skill_" + skillActionGraphics3.m_SettingMotionID;
					}
					else
					{
						this.m_ActKey = "skill_0";
					}
				}
				break;
			case BattleCommandData.eCommandType.MasterSkill:
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = 0;
				this.m_LoadFactor = 1f;
				this.m_ActKey = string.Empty;
				break;
			case BattleCommandData.eCommandType.Card:
				this.m_SkillParam = self.GetParam(this.m_SkillID);
				this.m_RecastMax = 0;
				this.m_LoadFactor = SkillContentListDB_Ext.GetLoadFactorFromSkillLv(ref this.m_SkillParam, this.m_SkillLv, BattleUtility.GetBorderFromSkillLvs());
				this.m_ActKey = string.Empty;
				break;
			}
			if (this.m_CommandType != BattleCommandData.eCommandType.Guard)
			{
				SkillContentListDB_Param param = self2.GetParam(this.m_SkillID);
				bool flag = true;
				this.m_MainSkillTargetType = (eSkillTargetType)param.m_Datas[0].m_Target;
				for (int i = 0; i < param.m_Datas.Length; i++)
				{
					SkillContentListDB_Data data = param.m_Datas[i];
					eSkillTargetType target = (eSkillTargetType)data.m_Target;
					BattleCommandData.SkillContentSet item = new BattleCommandData.SkillContentSet(target, data);
					if (target != this.m_MainSkillTargetType)
					{
						flag = false;
					}
					if (!flag)
					{
					}
					this.m_SkillContentSets.Add(item);
				}
			}
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x00016F95 File Offset: 0x00015395
		public void Clear()
		{
			this.m_CommandIndex = -1;
			this.m_SkillID = -1;
			if (this.m_SkillContentSets != null)
			{
				this.m_SkillContentSets.Clear();
				this.m_SkillContentSets = null;
			}
			this.m_Owner = null;
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x00016FC9 File Offset: 0x000153C9
		public void SetOwner(CharacterHandler owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x00016FD4 File Offset: 0x000153D4
		public List<eElementType> GetAttackSkillElementTypes()
		{
			List<eElementType> list = new List<eElementType>();
			switch (this.m_SkillParam.m_SkillType)
			{
			case 0:
			case 1:
			case 3:
			case 4:
				for (int i = 0; i < this.m_SkillContentSets.Count; i++)
				{
					eSkillContentType type = (eSkillContentType)this.m_SkillContentSets[i].m_Data.m_Type;
					if (type == eSkillContentType.Attack || type == eSkillContentType.Card)
					{
						float[] args = this.m_SkillContentSets[i].m_Data.m_Args;
						if (type == eSkillContentType.Card)
						{
							int num = (args.Length <= 1) ? -1 : ((int)args[1]);
							if (num != -1)
							{
								SkillContentListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_CARD.GetParam(num);
								for (int j = 0; j < param.m_Datas.Length; j++)
								{
									if (param.m_Datas[j].m_Type == 0)
									{
										args = param.m_Datas[j].m_Args;
										break;
									}
								}
							}
						}
						for (eElementType eElementType = eElementType.Fire; eElementType < eElementType.Num; eElementType++)
						{
							if (args[(int)(eElementType + 2)] != 0f)
							{
								list.Add(eElementType);
							}
						}
						if (list.Count <= 0 && this.m_Owner != null)
						{
							list.Add(this.m_Owner.CharaParam.ElementType);
						}
						break;
					}
				}
				break;
			}
			return list;
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x00017168 File Offset: 0x00015568
		public bool IncrementSkillUseNum()
		{
			if ((int)this.m_ExpTableID == -1)
			{
				return false;
			}
			if (this.m_SkillLv >= this.m_SkillMaxLv)
			{
				return false;
			}
			this.m_TotalUseNum += 1L;
			this.UseNum++;
			int num = EditUtility.CalcSkillLvFromTotalUseNum(this.m_SkillMaxLv, this.m_TotalUseNum, this.m_ExpTableID);
			if (num > this.m_SkillLv)
			{
				this.m_SkillLv = num;
				return true;
			}
			return false;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x000171E4 File Offset: 0x000155E4
		public int CalcRecast(int val)
		{
			int recastVal = this.RecastVal;
			this.RecastVal = Mathf.Clamp(this.RecastVal + val, 0, this.m_RecastMax);
			if (recastVal != 0 && this.RecastVal == 0)
			{
				this.m_RecastRecoveredFlg = true;
			}
			return this.RecastVal;
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x00017230 File Offset: 0x00015630
		public void ResetRecastRecoverdFlg()
		{
			this.m_RecastRecoveredFlg = false;
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x00017239 File Offset: 0x00015639
		public void RecastZero(bool recastRecoveredFlg = false)
		{
			this.RecastVal = 0;
			this.m_RecastRecoveredFlg = recastRecoveredFlg;
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x00017249 File Offset: 0x00015649
		public void RecastFull()
		{
			this.RecastVal = this.m_RecastMax;
			this.m_RecastRecoveredFlg = false;
		}

		// Token: 0x04000301 RID: 769
		[NonSerialized]
		private BattleCommandData.eCommandType m_CommandType;

		// Token: 0x04000302 RID: 770
		[NonSerialized]
		private int m_CommandIndex = -1;

		// Token: 0x04000303 RID: 771
		[NonSerialized]
		private int m_SkillID = -1;

		// Token: 0x04000304 RID: 772
		[NonSerialized]
		private int m_SkillLv;

		// Token: 0x04000305 RID: 773
		[NonSerialized]
		private int m_SkillMaxLv;

		// Token: 0x04000306 RID: 774
		[NonSerialized]
		private long m_TotalUseNum;

		// Token: 0x04000307 RID: 775
		[SerializeField]
		public int UseNum;

		// Token: 0x04000308 RID: 776
		[NonSerialized]
		private sbyte m_ExpTableID = -1;

		// Token: 0x04000309 RID: 777
		[NonSerialized]
		private SkillListDB_Param m_SkillParam;

		// Token: 0x0400030A RID: 778
		[NonSerialized]
		private bool m_CanUseOnConfusion = true;

		// Token: 0x0400030B RID: 779
		[NonSerialized]
		private List<BattleCommandData.SkillContentSet> m_SkillContentSets;

		// Token: 0x0400030C RID: 780
		[NonSerialized]
		private eSkillTargetType m_MainSkillTargetType;

		// Token: 0x0400030D RID: 781
		[NonSerialized]
		private string m_ActKey;

		// Token: 0x0400030E RID: 782
		[SerializeField]
		public int RecastVal;

		// Token: 0x0400030F RID: 783
		[NonSerialized]
		private int m_RecastMax = 1;

		// Token: 0x04000310 RID: 784
		[NonSerialized]
		private bool m_RecastRecoveredFlg;

		// Token: 0x04000311 RID: 785
		[NonSerialized]
		private float m_LoadFactor = 1f;

		// Token: 0x04000312 RID: 786
		[NonSerialized]
		private CharacterHandler m_Owner;

		// Token: 0x04000313 RID: 787
		[NonSerialized]
		private BattleCommandSolveResult m_SolveResult = new BattleCommandSolveResult();

		// Token: 0x020000A4 RID: 164
		public enum eCommandType
		{
			// Token: 0x04000315 RID: 789
			NormalAttack,
			// Token: 0x04000316 RID: 790
			Guard,
			// Token: 0x04000317 RID: 791
			Skill,
			// Token: 0x04000318 RID: 792
			WeaponSkill,
			// Token: 0x04000319 RID: 793
			UniqueSkill,
			// Token: 0x0400031A RID: 794
			MasterSkill,
			// Token: 0x0400031B RID: 795
			Card
		}

		// Token: 0x020000A5 RID: 165
		public class SkillContentSet
		{
			// Token: 0x0600049A RID: 1178 RVA: 0x0001725E File Offset: 0x0001565E
			public SkillContentSet(eSkillTargetType target, SkillContentListDB_Data data)
			{
				this.m_Target = target;
				this.m_Data = data;
			}

			// Token: 0x0400031C RID: 796
			public eSkillTargetType m_Target;

			// Token: 0x0400031D RID: 797
			public SkillContentListDB_Data m_Data;
		}
	}
}
