﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002B7 RID: 695
	public abstract class CharacterDataWrapperNoExist : CharacterDataWrapperBase
	{
		// Token: 0x06000D00 RID: 3328 RVA: 0x00049311 File Offset: 0x00047711
		protected CharacterDataWrapperNoExist(eCharacterDataType characterDataType) : base(characterDataType)
		{
		}

		// Token: 0x06000D01 RID: 3329 RVA: 0x0004931A File Offset: 0x0004771A
		public override long GetMngId()
		{
			return -1L;
		}

		// Token: 0x06000D02 RID: 3330 RVA: 0x0004931E File Offset: 0x0004771E
		public override int GetCharaId()
		{
			return -1;
		}

		// Token: 0x06000D03 RID: 3331 RVA: 0x00049321 File Offset: 0x00047721
		public override eTitleType GetCharaTitleType()
		{
			base.Assert();
			return eTitleType.None;
		}

		// Token: 0x06000D04 RID: 3332 RVA: 0x0004932A File Offset: 0x0004772A
		public override eCharaNamedType GetCharaNamedType()
		{
			base.Assert();
			return eCharaNamedType.None;
		}

		// Token: 0x06000D05 RID: 3333 RVA: 0x00049333 File Offset: 0x00047733
		public override eClassType GetClassType()
		{
			base.Assert();
			return eClassType.None;
		}

		// Token: 0x06000D06 RID: 3334 RVA: 0x0004933C File Offset: 0x0004773C
		public override eElementType GetElementType()
		{
			base.Assert();
			return eElementType.None;
		}

		// Token: 0x06000D07 RID: 3335 RVA: 0x00049345 File Offset: 0x00047745
		public override eRare GetRarity()
		{
			return eRare.None;
		}

		// Token: 0x06000D08 RID: 3336 RVA: 0x00049348 File Offset: 0x00047748
		public override int GetCost()
		{
			return 0;
		}

		// Token: 0x06000D09 RID: 3337 RVA: 0x0004934B File Offset: 0x0004774B
		public override int GetLv()
		{
			return -1;
		}

		// Token: 0x06000D0A RID: 3338 RVA: 0x0004934E File Offset: 0x0004774E
		public override int GetMaxLv()
		{
			return -1;
		}

		// Token: 0x06000D0B RID: 3339 RVA: 0x00049351 File Offset: 0x00047751
		public override long GetExp()
		{
			return -1L;
		}

		// Token: 0x06000D0C RID: 3340 RVA: 0x00049355 File Offset: 0x00047755
		public override int GetLimitBreak()
		{
			return -1;
		}

		// Token: 0x06000D0D RID: 3341 RVA: 0x00049358 File Offset: 0x00047758
		public override int GetFriendship()
		{
			return -1;
		}

		// Token: 0x06000D0E RID: 3342 RVA: 0x0004935B File Offset: 0x0004775B
		public override int GetFriendshipMax()
		{
			return int.MaxValue;
		}

		// Token: 0x06000D0F RID: 3343 RVA: 0x00049362 File Offset: 0x00047762
		public override long GetFriendshipExp()
		{
			return -1L;
		}

		// Token: 0x06000D10 RID: 3344 RVA: 0x00049366 File Offset: 0x00047766
		public override SkillLearnData GetUniqueSkillLearnData()
		{
			return null;
		}

		// Token: 0x06000D11 RID: 3345 RVA: 0x00049369 File Offset: 0x00047769
		public override List<SkillLearnData> GetClassSkillLearnDatas()
		{
			return null;
		}
	}
}
