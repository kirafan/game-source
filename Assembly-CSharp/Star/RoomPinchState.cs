﻿using System;

namespace Star
{
	// Token: 0x0200057E RID: 1406
	public class RoomPinchState
	{
		// Token: 0x04002243 RID: 8771
		public bool m_Event;

		// Token: 0x04002244 RID: 8772
		public bool IsPinching;

		// Token: 0x04002245 RID: 8773
		public float PinchRatio;
	}
}
