﻿using System;

namespace Star
{
	// Token: 0x020001F5 RID: 501
	[Serializable]
	public struct RoomObjectListDB_Param
	{
		// Token: 0x04000C09 RID: 3081
		public int m_ID;

		// Token: 0x04000C0A RID: 3082
		public int m_Category;

		// Token: 0x04000C0B RID: 3083
		public int m_TitleType;

		// Token: 0x04000C0C RID: 3084
		public string m_Name;

		// Token: 0x04000C0D RID: 3085
		public int m_SizeX;

		// Token: 0x04000C0E RID: 3086
		public int m_SizeY;

		// Token: 0x04000C0F RID: 3087
		public int m_DBAccessID;

		// Token: 0x04000C10 RID: 3088
		public uint m_OptionID;

		// Token: 0x04000C11 RID: 3089
		public ushort m_Flag;

		// Token: 0x04000C12 RID: 3090
		public ushort m_SearchDown;

		// Token: 0x04000C13 RID: 3091
		public ushort m_SearchRight;

		// Token: 0x04000C14 RID: 3092
		public ushort m_SearchUp;

		// Token: 0x04000C15 RID: 3093
		public ushort m_SearchLeft;

		// Token: 0x04000C16 RID: 3094
		public int m_ObjEventType;

		// Token: 0x04000C17 RID: 3095
		public int[] m_ObjEventArgs;

		// Token: 0x04000C18 RID: 3096
		public int m_BuyAmount;

		// Token: 0x04000C19 RID: 3097
		public int m_SellAmount;

		// Token: 0x04000C1A RID: 3098
		public short m_MaxNum;

		// Token: 0x04000C1B RID: 3099
		public string m_Description;

		// Token: 0x04000C1C RID: 3100
		public sbyte m_ShopFlag;

		// Token: 0x04000C1D RID: 3101
		public string m_ShopDispStartAt;

		// Token: 0x04000C1E RID: 3102
		public string m_ShopDispEndAt;

		// Token: 0x04000C1F RID: 3103
		public sbyte m_BargainFlag;

		// Token: 0x04000C20 RID: 3104
		public string m_BargainStartAt;

		// Token: 0x04000C21 RID: 3105
		public string m_BargainEndAt;

		// Token: 0x04000C22 RID: 3106
		public int m_BargainBuyAmount;

		// Token: 0x04000C23 RID: 3107
		public sbyte m_PickUp;

		// Token: 0x04000C24 RID: 3108
		public sbyte m_Tutorial;

		// Token: 0x04000C25 RID: 3109
		public int m_Sort;
	}
}
