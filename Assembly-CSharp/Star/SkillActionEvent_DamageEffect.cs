﻿using System;

namespace Star
{
	// Token: 0x02000113 RID: 275
	[Serializable]
	public class SkillActionEvent_DamageEffect : SkillActionEvent_Base
	{
		// Token: 0x04000700 RID: 1792
		public SkillActionInfo_DamageEffect m_DamageEffect;
	}
}
