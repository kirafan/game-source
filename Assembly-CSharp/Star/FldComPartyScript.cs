﻿using System;
using System.Collections.Generic;
using FieldPartyMemberResponseTypes;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x02000366 RID: 870
	public class FldComPartyScript : IFldNetComModule
	{
		// Token: 0x0600107A RID: 4218 RVA: 0x00057271 File Offset: 0x00055671
		public FldComPartyScript() : base(IFldNetComManager.Instance)
		{
			this.m_Table = new List<FldComPartyScript.StackScriptCommand>();
			this.m_Step = FldComPartyScript.eStep.Check;
		}

		// Token: 0x0600107B RID: 4219 RVA: 0x00057290 File Offset: 0x00055690
		public ComItemUpState GetUpItemState()
		{
			return this.m_UpUserData;
		}

		// Token: 0x0600107C RID: 4220 RVA: 0x00057298 File Offset: 0x00055698
		public static bool IsExistWaitCmd(FldComPartyScript.eCmd cmd)
		{
			return FldComPartyScript.m_CmdRefCount[(int)cmd] > 0;
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x000572A4 File Offset: 0x000556A4
		private void CmdIncCnt(FldComPartyScript.eCmd cmd)
		{
			FldComPartyScript.m_CmdRefCount[(int)cmd]++;
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x000572B6 File Offset: 0x000556B6
		private void CallbackCmdDecCnt_ChangeState(IFldNetComModule pmodule)
		{
			FldComPartyScript.m_CmdRefCount[0]--;
			if (FldComPartyScript.m_CmdRefCount[0] < 0)
			{
				FldComPartyScript.m_CmdRefCount[0] = 0;
				Debug.LogWarning("CmdRefCount became to minus " + FldComPartyScript.eCmd.ChangeState);
			}
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x000572F2 File Offset: 0x000556F2
		private void CallbackCmdDecCnt_ChangeSchedule(IFldNetComModule pmodule)
		{
			FldComPartyScript.m_CmdRefCount[1]--;
			if (FldComPartyScript.m_CmdRefCount[1] < 0)
			{
				FldComPartyScript.m_CmdRefCount[1] = 0;
				Debug.LogWarning("CmdRefCount became to minus " + FldComPartyScript.eCmd.ChangeSchedule);
			}
		}

		// Token: 0x06001080 RID: 4224 RVA: 0x0005732E File Offset: 0x0005572E
		private void CallbackCmdDecCnt_AddMember(IFldNetComModule pmodule)
		{
			FldComPartyScript.m_CmdRefCount[2]--;
			if (FldComPartyScript.m_CmdRefCount[2] < 0)
			{
				FldComPartyScript.m_CmdRefCount[2] = 0;
				Debug.LogWarning("CmdRefCount became to minus " + FldComPartyScript.eCmd.AddMember);
			}
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x0005736A File Offset: 0x0005576A
		private void CallbackCmdDecCnt_RemoveMember(IFldNetComModule pmodule)
		{
			FldComPartyScript.m_CmdRefCount[3]--;
			if (FldComPartyScript.m_CmdRefCount[3] < 0)
			{
				FldComPartyScript.m_CmdRefCount[3] = 0;
				Debug.LogWarning("CmdRefCount became to minus " + FldComPartyScript.eCmd.RemoveMember);
			}
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x000573A6 File Offset: 0x000557A6
		private void CallbackCmdDecCnt_TouchItem(IFldNetComModule pmodule)
		{
			FldComPartyScript.m_CmdRefCount[4]--;
			if (FldComPartyScript.m_CmdRefCount[4] < 0)
			{
				FldComPartyScript.m_CmdRefCount[4] = 0;
				Debug.LogWarning("CmdRefCount became to minus " + FldComPartyScript.eCmd.TouchItem);
			}
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x000573E4 File Offset: 0x000557E4
		public void StackSendCmd(FldComPartyScript.eCmd fcalc, long fkeycode, IFldNetComModule.CallBack pcallback = null)
		{
			IFldNetComModule.CallBack b = null;
			FldComPartyScript.StackScriptCommand stackScriptCommand = this.FindComCode(fcalc, fkeycode);
			if (stackScriptCommand == null)
			{
				switch (fcalc)
				{
				case FldComPartyScript.eCmd.ChangeState:
					stackScriptCommand = new FldComPartyScript.ChangeStateCom();
					b = new IFldNetComModule.CallBack(this.CallbackCmdDecCnt_ChangeState);
					break;
				case FldComPartyScript.eCmd.ChangeSchedule:
					stackScriptCommand = new FldComPartyScript.ChangeScheduleCom();
					b = new IFldNetComModule.CallBack(this.CallbackCmdDecCnt_ChangeSchedule);
					break;
				case FldComPartyScript.eCmd.AddMember:
					stackScriptCommand = new FldComPartyScript.AddMemberCom();
					b = new IFldNetComModule.CallBack(this.CallbackCmdDecCnt_AddMember);
					break;
				case FldComPartyScript.eCmd.RemoveMember:
					stackScriptCommand = new FldComPartyScript.RemoveMemberCom();
					b = new IFldNetComModule.CallBack(this.CallbackCmdDecCnt_RemoveMember);
					break;
				case FldComPartyScript.eCmd.TouchItem:
					stackScriptCommand = new FldComPartyScript.TouchItemCom();
					b = new IFldNetComModule.CallBack(this.CallbackCmdDecCnt_TouchItem);
					break;
				case FldComPartyScript.eCmd.CallEvt:
					stackScriptCommand = new FldComPartyScript.DummyEventCom();
					break;
				}
				if (stackScriptCommand != null)
				{
					this.CmdIncCnt(fcalc);
					stackScriptCommand.m_NextStep = fcalc;
					stackScriptCommand.AddCode(fkeycode);
					stackScriptCommand.m_Callback = pcallback;
					FldComPartyScript.StackScriptCommand stackScriptCommand2 = stackScriptCommand;
					stackScriptCommand2.m_Callback = (IFldNetComModule.CallBack)Delegate.Combine(stackScriptCommand2.m_Callback, b);
					this.m_Table.Add(stackScriptCommand);
				}
			}
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x000574F0 File Offset: 0x000558F0
		private FldComPartyScript.StackScriptCommand FindComCode(FldComPartyScript.eCmd fcalc, long fkeycode)
		{
			int count = this.m_Table.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_Table[i].m_NextStep == fcalc && this.m_Table[i].IsAddCode())
				{
					this.m_Table[i].AddCode(fkeycode);
					return this.m_Table[i];
				}
			}
			return null;
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00057568 File Offset: 0x00055968
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x00057578 File Offset: 0x00055978
		public override bool UpFunc()
		{
			bool result = true;
			FldComPartyScript.eStep step = this.m_Step;
			if (step != FldComPartyScript.eStep.Check)
			{
				if (step != FldComPartyScript.eStep.ComCheck)
				{
					if (step == FldComPartyScript.eStep.End)
					{
						result = false;
					}
				}
				else if (!this.m_Table[0].m_Active)
				{
					if (this.m_Callback != null)
					{
						this.m_Callback(this);
					}
					this.m_Table.RemoveAt(0);
					this.m_Step = FldComPartyScript.eStep.Check;
				}
			}
			else if (this.m_Table.Count != 0)
			{
				this.m_Step = FldComPartyScript.eStep.ComCheck;
				base.EntryCallback(this.m_Table[0].m_Callback);
				this.m_Table[0].MakeSendCom(this);
			}
			else
			{
				this.m_Step = FldComPartyScript.eStep.End;
			}
			return result;
		}

		// Token: 0x0400177D RID: 6013
		public List<FldComPartyScript.StackScriptCommand> m_Table;

		// Token: 0x0400177E RID: 6014
		public FldComPartyScript.eStep m_Step;

		// Token: 0x0400177F RID: 6015
		public long m_SetUpManageID;

		// Token: 0x04001780 RID: 6016
		public ComItemUpState m_UpUserData;

		// Token: 0x04001781 RID: 6017
		private static int[] m_CmdRefCount = new int[6];

		// Token: 0x02000367 RID: 871
		public enum eCmd
		{
			// Token: 0x04001783 RID: 6019
			ChangeState,
			// Token: 0x04001784 RID: 6020
			ChangeSchedule,
			// Token: 0x04001785 RID: 6021
			AddMember,
			// Token: 0x04001786 RID: 6022
			RemoveMember,
			// Token: 0x04001787 RID: 6023
			TouchItem,
			// Token: 0x04001788 RID: 6024
			CallEvt,
			// Token: 0x04001789 RID: 6025
			Max
		}

		// Token: 0x02000368 RID: 872
		public enum eStep
		{
			// Token: 0x0400178B RID: 6027
			Check,
			// Token: 0x0400178C RID: 6028
			ComEnd,
			// Token: 0x0400178D RID: 6029
			ComCheck,
			// Token: 0x0400178E RID: 6030
			End
		}

		// Token: 0x02000369 RID: 873
		public class StackScriptCommand
		{
			// Token: 0x06001089 RID: 4233 RVA: 0x0005765A File Offset: 0x00055A5A
			public virtual void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = false;
			}

			// Token: 0x0600108A RID: 4234 RVA: 0x00057663 File Offset: 0x00055A63
			public virtual void AddCode(long fkeycode)
			{
			}

			// Token: 0x0600108B RID: 4235 RVA: 0x00057665 File Offset: 0x00055A65
			public virtual bool IsAddCode()
			{
				return false;
			}

			// Token: 0x0400178F RID: 6031
			public FldComPartyScript.eCmd m_NextStep;

			// Token: 0x04001790 RID: 6032
			public bool m_Active;

			// Token: 0x04001791 RID: 6033
			public IFldNetComModule.CallBack m_Callback;
		}

		// Token: 0x0200036A RID: 874
		public class ChangeStateCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x0600108D RID: 4237 RVA: 0x0005767B File Offset: 0x00055A7B
			public override void AddCode(long fkeycode)
			{
				this.m_KeyCode.Add(fkeycode);
			}

			// Token: 0x0600108E RID: 4238 RVA: 0x00057689 File Offset: 0x00055A89
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x0600108F RID: 4239 RVA: 0x0005768C File Offset: 0x00055A8C
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = true;
				FieldPartyAPISetAll fieldPartyAPISetAll = new FieldPartyAPISetAll();
				fieldPartyAPISetAll.fieldPartyMembers = new PlayerFieldPartyMember[this.m_KeyCode.Count];
				for (int i = 0; i < this.m_KeyCode.Count; i++)
				{
					fieldPartyAPISetAll.fieldPartyMembers[i] = UserDataUtil.FieldChara.CreateRoomInChara(this.m_KeyCode[i]);
				}
				pbase.m_NetMng.SendCom(fieldPartyAPISetAll, new INetComHandle.ResponseCallbak(this.CallbackStateChg), false);
			}

			// Token: 0x06001090 RID: 4240 RVA: 0x0005770F File Offset: 0x00055B0F
			private void CallbackStateChg(INetComHandle phandle)
			{
				this.m_Active = false;
			}

			// Token: 0x04001792 RID: 6034
			private List<long> m_KeyCode = new List<long>();
		}

		// Token: 0x0200036B RID: 875
		public class AddMemberCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x06001092 RID: 4242 RVA: 0x0005772B File Offset: 0x00055B2B
			public override void AddCode(long fkeycode)
			{
				this.m_KeyCode.Add(fkeycode);
			}

			// Token: 0x06001093 RID: 4243 RVA: 0x00057739 File Offset: 0x00055B39
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x06001094 RID: 4244 RVA: 0x0005773C File Offset: 0x00055B3C
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = true;
				FieldPartyAPIAddAll fieldPartyAPIAddAll = new FieldPartyAPIAddAll();
				fieldPartyAPIAddAll.fieldPartyMembers = new PlayerFieldPartyMember[this.m_KeyCode.Count];
				for (int i = 0; i < this.m_KeyCode.Count; i++)
				{
					fieldPartyAPIAddAll.fieldPartyMembers[i] = UserDataUtil.FieldChara.CreateRoomInChara(this.m_KeyCode[i]);
				}
				pbase.m_NetMng.SendCom(fieldPartyAPIAddAll, new INetComHandle.ResponseCallbak(this.CallbackPartyAdd), false);
			}

			// Token: 0x06001095 RID: 4245 RVA: 0x000577C0 File Offset: 0x00055BC0
			private void CallbackPartyAdd(INetComHandle phandle)
			{
				AddAll addAll = phandle.GetResponse() as AddAll;
				for (int i = 0; i < this.m_KeyCode.Count; i++)
				{
					UserFieldCharaData.RoomInCharaData roomInChara = UserDataUtil.FieldChara.GetRoomInChara(this.m_KeyCode[i]);
					roomInChara.ServerManageID = addAll.managedPartyMemberIds[i];
				}
				this.m_Active = false;
			}

			// Token: 0x04001793 RID: 6035
			private List<long> m_KeyCode = new List<long>();
		}

		// Token: 0x0200036C RID: 876
		public class RemoveMemberCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x06001097 RID: 4247 RVA: 0x00057834 File Offset: 0x00055C34
			public override void AddCode(long fkeycode)
			{
				this.m_KeyCode.Add(fkeycode);
			}

			// Token: 0x06001098 RID: 4248 RVA: 0x00057842 File Offset: 0x00055C42
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x06001099 RID: 4249 RVA: 0x00057848 File Offset: 0x00055C48
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = true;
				FieldPartyAPIRemoveAll fieldPartyAPIRemoveAll = new FieldPartyAPIRemoveAll();
				fieldPartyAPIRemoveAll.managedPartyMemberIds = new long[this.m_KeyCode.Count];
				for (int i = 0; i < this.m_KeyCode.Count; i++)
				{
					fieldPartyAPIRemoveAll.managedPartyMemberIds[i] = this.m_KeyCode[i];
				}
				pbase.m_NetMng.SendCom(fieldPartyAPIRemoveAll, new INetComHandle.ResponseCallbak(this.CallbackPartyRemove), false);
			}

			// Token: 0x0600109A RID: 4250 RVA: 0x000578C1 File Offset: 0x00055CC1
			private void CallbackPartyRemove(INetComHandle phandle)
			{
				this.m_Active = false;
			}

			// Token: 0x04001794 RID: 6036
			private List<long> m_KeyCode = new List<long>();
		}

		// Token: 0x0200036D RID: 877
		public class ChangeScheduleCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x0600109C RID: 4252 RVA: 0x000578DD File Offset: 0x00055CDD
			public override void AddCode(long fkeycode)
			{
				this.m_KeyCode.Add(fkeycode);
			}

			// Token: 0x0600109D RID: 4253 RVA: 0x000578EB File Offset: 0x00055CEB
			public override bool IsAddCode()
			{
				return true;
			}

			// Token: 0x0600109E RID: 4254 RVA: 0x000578F0 File Offset: 0x00055CF0
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = true;
				FieldPartyAPIChangeScheduleAll fieldPartyAPIChangeScheduleAll = new FieldPartyAPIChangeScheduleAll();
				fieldPartyAPIChangeScheduleAll.changeScheduleMembers = new ChangeScheduleMember[this.m_KeyCode.Count];
				for (int i = 0; i < this.m_KeyCode.Count; i++)
				{
					fieldPartyAPIChangeScheduleAll.changeScheduleMembers[i] = UserDataUtil.FieldChara.CreateRoomInCharaSchedule(this.m_KeyCode[i]);
				}
				pbase.m_NetMng.SendCom(fieldPartyAPIChangeScheduleAll, new INetComHandle.ResponseCallbak(this.CallbackScheduleChg), false);
			}

			// Token: 0x0600109F RID: 4255 RVA: 0x00057973 File Offset: 0x00055D73
			private void CallbackScheduleChg(INetComHandle phandle)
			{
				this.m_Active = false;
			}

			// Token: 0x04001795 RID: 6037
			private List<long> m_KeyCode = new List<long>();
		}

		// Token: 0x0200036E RID: 878
		public class TouchItemCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060010A1 RID: 4257 RVA: 0x00057984 File Offset: 0x00055D84
			public override void AddCode(long fkeycode)
			{
				this.m_KeyCode = fkeycode;
			}

			// Token: 0x060010A2 RID: 4258 RVA: 0x0005798D File Offset: 0x00055D8D
			public override bool IsAddCode()
			{
				return false;
			}

			// Token: 0x060010A3 RID: 4259 RVA: 0x00057990 File Offset: 0x00055D90
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = true;
				this.m_parent = pbase;
				UserFieldCharaData.RoomInCharaData roomInChara = UserDataUtil.FieldChara.GetRoomInChara(this.m_KeyCode);
				FieldPartyAPIItemUp fieldPartyAPIItemUp = new FieldPartyAPIItemUp();
				fieldPartyAPIItemUp.managedPartyMemberId = roomInChara.ServerManageID;
				fieldPartyAPIItemUp.touchItemResultNo = roomInChara.TouchItemResultNo;
				fieldPartyAPIItemUp.amount = 1;
				fieldPartyAPIItemUp.flag = roomInChara.Flags;
				pbase.m_NetMng.SendCom(fieldPartyAPIItemUp, new INetComHandle.ResponseCallbak(this.CallbackPartyItemUp), false);
			}

			// Token: 0x060010A4 RID: 4260 RVA: 0x00057A08 File Offset: 0x00055E08
			private void CallbackPartyItemUp(INetComHandle phandle)
			{
				Itemup itemup = phandle.GetResponse() as Itemup;
				this.m_parent.m_UpUserData = new ComItemUpState();
				this.m_parent.m_UpUserData.CalcPlayerDataNewParam(itemup.player);
				this.m_parent.m_UpUserData.CalcPlayerItemNewParam(itemup.itemSummary);
				this.m_parent.m_UpUserData.CalcNamedFriendShipNewParam(itemup.managedNamedTypes);
				APIUtility.wwwToUserData(itemup.player, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData);
				this.m_Active = false;
			}

			// Token: 0x04001796 RID: 6038
			private FldComPartyScript m_parent;

			// Token: 0x04001797 RID: 6039
			private long m_KeyCode;
		}

		// Token: 0x0200036F RID: 879
		public class DummyEventCom : FldComPartyScript.StackScriptCommand
		{
			// Token: 0x060010A6 RID: 4262 RVA: 0x00057A9C File Offset: 0x00055E9C
			public override void MakeSendCom(FldComPartyScript pbase)
			{
				this.m_Active = false;
			}
		}
	}
}
