﻿using System;

namespace Star
{
	// Token: 0x02000326 RID: 806
	// (Invoke) Token: 0x06000F48 RID: 3912
	public delegate void BuildUpCallback(int buildpoint, int objID, long fmanageid);
}
