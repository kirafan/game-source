﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000552 RID: 1362
	public class CActScriptKeyEffect : IRoomScriptData
	{
		// Token: 0x06001AD3 RID: 6867 RVA: 0x0008EF50 File Offset: 0x0008D350
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyEffect actXlsKeyEffect = (ActXlsKeyEffect)pbase;
			this.m_EffectNo = actXlsKeyEffect.m_EffectNo;
			this.m_OffsetPos = actXlsKeyEffect.m_OffsetPos;
			this.m_Size = actXlsKeyEffect.m_Size;
		}

		// Token: 0x040021A4 RID: 8612
		public int m_EffectNo;

		// Token: 0x040021A5 RID: 8613
		public Vector3 m_OffsetPos;

		// Token: 0x040021A6 RID: 8614
		public float m_Size;
	}
}
