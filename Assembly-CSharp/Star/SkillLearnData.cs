﻿using System;

namespace Star
{
	// Token: 0x02000154 RID: 340
	[Serializable]
	public class SkillLearnData
	{
		// Token: 0x060009A5 RID: 2469 RVA: 0x0003C21F File Offset: 0x0003A61F
		public SkillLearnData(int skillID, int skillLv, int skillMaxLv, long totalUseNum, sbyte expTableID, bool isConfusionSkillID = true)
		{
			this.SkillID = skillID;
			this.SkillLv = skillLv;
			this.SkillMaxLv = skillMaxLv;
			this.TotalUseNum = totalUseNum;
			this.ExpTableID = expTableID;
			this.IsConfusionSkillID = isConfusionSkillID;
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060009A6 RID: 2470 RVA: 0x0003C254 File Offset: 0x0003A654
		// (set) Token: 0x060009A7 RID: 2471 RVA: 0x0003C25C File Offset: 0x0003A65C
		public int SkillID { get; set; }

		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060009A8 RID: 2472 RVA: 0x0003C265 File Offset: 0x0003A665
		// (set) Token: 0x060009A9 RID: 2473 RVA: 0x0003C26D File Offset: 0x0003A66D
		public int SkillLv { get; set; }

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060009AA RID: 2474 RVA: 0x0003C276 File Offset: 0x0003A676
		// (set) Token: 0x060009AB RID: 2475 RVA: 0x0003C27E File Offset: 0x0003A67E
		public int SkillMaxLv { get; set; }

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060009AC RID: 2476 RVA: 0x0003C287 File Offset: 0x0003A687
		// (set) Token: 0x060009AD RID: 2477 RVA: 0x0003C28F File Offset: 0x0003A68F
		public long TotalUseNum { get; set; }

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060009AE RID: 2478 RVA: 0x0003C298 File Offset: 0x0003A698
		// (set) Token: 0x060009AF RID: 2479 RVA: 0x0003C2A0 File Offset: 0x0003A6A0
		public sbyte ExpTableID { get; set; }

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060009B0 RID: 2480 RVA: 0x0003C2A9 File Offset: 0x0003A6A9
		// (set) Token: 0x060009B1 RID: 2481 RVA: 0x0003C2B1 File Offset: 0x0003A6B1
		public bool IsConfusionSkillID { get; set; }
	}
}
