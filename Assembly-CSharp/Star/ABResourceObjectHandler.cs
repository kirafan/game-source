﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x020004DB RID: 1243
	public class ABResourceObjectHandler
	{
		// Token: 0x0600189B RID: 6299 RVA: 0x0008097E File Offset: 0x0007ED7E
		public ABResourceObjectHandler(MeigeResource.Handler hndl, string path, params MeigeResource.Option[] options)
		{
			this.Hndl = hndl;
			this.Path = path;
			this.Options = options;
			this.RefCount = 1;
			this.Objs = null;
			this.m_IsError = false;
			this.m_IsDone = false;
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x0600189C RID: 6300 RVA: 0x000809B7 File Offset: 0x0007EDB7
		// (set) Token: 0x0600189D RID: 6301 RVA: 0x000809BF File Offset: 0x0007EDBF
		public MeigeResource.Handler Hndl { get; set; }

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x0600189E RID: 6302 RVA: 0x000809C8 File Offset: 0x0007EDC8
		// (set) Token: 0x0600189F RID: 6303 RVA: 0x000809D0 File Offset: 0x0007EDD0
		public MeigeResource.Option[] Options { get; set; }

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x060018A0 RID: 6304 RVA: 0x000809D9 File Offset: 0x0007EDD9
		// (set) Token: 0x060018A1 RID: 6305 RVA: 0x000809E1 File Offset: 0x0007EDE1
		public string Path { get; private set; }

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x060018A2 RID: 6306 RVA: 0x000809EA File Offset: 0x0007EDEA
		// (set) Token: 0x060018A3 RID: 6307 RVA: 0x000809F2 File Offset: 0x0007EDF2
		public int RefCount { get; private set; }

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x060018A4 RID: 6308 RVA: 0x000809FB File Offset: 0x0007EDFB
		// (set) Token: 0x060018A5 RID: 6309 RVA: 0x00080A03 File Offset: 0x0007EE03
		public UnityEngine.Object[] Objs { get; private set; }

		// Token: 0x060018A6 RID: 6310 RVA: 0x00080A0C File Offset: 0x0007EE0C
		public int AddRef()
		{
			return ++this.RefCount;
		}

		// Token: 0x060018A7 RID: 6311 RVA: 0x00080A2C File Offset: 0x0007EE2C
		public int RemoveRef()
		{
			return --this.RefCount;
		}

		// Token: 0x060018A8 RID: 6312 RVA: 0x00080A4C File Offset: 0x0007EE4C
		public bool Update()
		{
			if (this.Hndl != null && this.Hndl.IsDone)
			{
				if (!this.Hndl.IsError)
				{
					this.Objs = new UnityEngine.Object[this.Hndl.assets.Length];
					for (int i = 0; i < this.Objs.Length; i++)
					{
						this.Objs[i] = this.Hndl.assets[i];
					}
					MeigeResourceManager.UnloadHandler(this.Hndl, false);
					this.Hndl = null;
					this.m_IsDone = true;
					return true;
				}
				MeigeResourceManager.RetryHandler(this.Hndl);
			}
			return false;
		}

		// Token: 0x060018A9 RID: 6313 RVA: 0x00080AF2 File Offset: 0x0007EEF2
		public bool IsDone()
		{
			return this.m_IsDone;
		}

		// Token: 0x060018AA RID: 6314 RVA: 0x00080AFA File Offset: 0x0007EEFA
		public bool IsError()
		{
			return this.m_IsError;
		}

		// Token: 0x060018AB RID: 6315 RVA: 0x00080B02 File Offset: 0x0007EF02
		public void Retry()
		{
			if (this.m_IsError)
			{
				MeigeResourceManager.RetryHandler(this.Hndl);
				this.m_IsError = false;
			}
		}

		// Token: 0x060018AC RID: 6316 RVA: 0x00080B24 File Offset: 0x0007EF24
		public void Destroy()
		{
			if (this.Hndl != null)
			{
				MeigeResourceManager.UnloadHandler(this.Hndl, false);
				this.Hndl = null;
			}
			if (this.Objs != null)
			{
				for (int i = 0; i < this.Objs.Length; i++)
				{
					if (this.Objs[i] != null)
					{
						this.Objs[i] = null;
					}
				}
				this.Objs = null;
			}
			this.RefCount = 0;
		}

		// Token: 0x060018AD RID: 6317 RVA: 0x00080BA0 File Offset: 0x0007EFA0
		public UnityEngine.Object FindObj(string findName, bool isIgnoreCase = true)
		{
			if (this.Objs != null)
			{
				for (int i = 0; i < this.Objs.Length; i++)
				{
					try
					{
						if (!isIgnoreCase)
						{
							if (this.Objs[i].name.ToLower() == findName.ToLower())
							{
								return this.Objs[i];
							}
						}
						else if (this.Objs[i].name == findName)
						{
							return this.Objs[i];
						}
					}
					catch (Exception ex)
					{
					}
				}
			}
			return null;
		}

		// Token: 0x060018AE RID: 6318 RVA: 0x00080C50 File Offset: 0x0007F050
		public UnityEngine.Object FindObjContains(string findName)
		{
			if (this.Objs != null)
			{
				for (int i = 0; i < this.Objs.Length; i++)
				{
					try
					{
						if (this.Objs[i].name.Contains(findName))
						{
							return this.Objs[i];
						}
					}
					catch (Exception ex)
					{
					}
				}
			}
			return null;
		}

		// Token: 0x04001F56 RID: 8022
		private bool m_IsError;

		// Token: 0x04001F57 RID: 8023
		private bool m_IsDone;
	}
}
