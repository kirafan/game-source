﻿using System;

namespace Star
{
	// Token: 0x0200009B RID: 155
	public class BattleAISolveResult
	{
		// Token: 0x040002BF RID: 703
		public int m_CommandIndex = -1;

		// Token: 0x040002C0 RID: 704
		public BattleDefine.eJoinMember m_TargetJoin = BattleDefine.eJoinMember.None;

		// Token: 0x040002C1 RID: 705
		public bool m_IsChargeSkill;
	}
}
