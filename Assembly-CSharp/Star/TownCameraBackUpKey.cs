﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000675 RID: 1653
	public class TownCameraBackUpKey : MonoBehaviour
	{
		// Token: 0x040027D9 RID: 10201
		public float m_OrthoSize;

		// Token: 0x040027DA RID: 10202
		public Vector3 m_BackPos;
	}
}
