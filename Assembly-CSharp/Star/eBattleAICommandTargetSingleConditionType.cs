﻿using System;

namespace Star
{
	// Token: 0x0200008E RID: 142
	public enum eBattleAICommandTargetSingleConditionType
	{
		// Token: 0x04000299 RID: 665
		Dying,
		// Token: 0x0400029A RID: 666
		Element,
		// Token: 0x0400029B RID: 667
		Class,
		// Token: 0x0400029C RID: 668
		StateAbnormal,
		// Token: 0x0400029D RID: 669
		WeakElement,
		// Token: 0x0400029E RID: 670
		Num
	}
}
