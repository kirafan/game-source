﻿using System;
using Meige;
using RoomObjectResponseTypes;
using Star.UI;
using Star.UI.Global;
using Star.UI.Shop;
using TownFacilityResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000481 RID: 1153
	public class ShopState_BuildTop : ShopState
	{
		// Token: 0x0600168B RID: 5771 RVA: 0x0007586F File Offset: 0x00073C6F
		public ShopState_BuildTop(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x0600168C RID: 5772 RVA: 0x00075887 File Offset: 0x00073C87
		public override int GetStateID()
		{
			return 20;
		}

		// Token: 0x0600168D RID: 5773 RVA: 0x0007588B File Offset: 0x00073C8B
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_BuildTop.eStep.First;
		}

		// Token: 0x0600168E RID: 5774 RVA: 0x00075894 File Offset: 0x00073C94
		public override void OnStateExit()
		{
		}

		// Token: 0x0600168F RID: 5775 RVA: 0x00075896 File Offset: 0x00073C96
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001690 RID: 5776 RVA: 0x000758A0 File Offset: 0x00073CA0
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_BuildTop.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_BuildTop.eStep.LoadWait;
				break;
			case ShopState_BuildTop.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildTop.eStep.PlayIn;
				}
				break;
			case ShopState_BuildTop.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_BuildTop.eStep.Main;
				}
				break;
			case ShopState_BuildTop.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						ShopBuildTopUI.eMainButton selectButton = this.m_UI.SelectButton;
						if (selectButton != ShopBuildTopUI.eMainButton.Buy)
						{
							if (selectButton != ShopBuildTopUI.eMainButton.Sale)
							{
								this.m_NextState = 1;
							}
							else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_003))
							{
								this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
								this.m_NextState = 2147483646;
							}
							else
							{
								this.m_NextState = 22;
							}
						}
						else if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_003))
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
							this.m_NextState = 2147483646;
						}
						else
						{
							this.m_NextState = 21;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildTop.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_BuildTop.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_BuildTop.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001691 RID: 5777 RVA: 0x00075A6C File Offset: 0x00073E6C
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopBuildTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickMainButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.ShopBuild);
			this.m_UI.PlayIn();
			if (this.m_Owner.IsRequestVocie)
			{
				this.m_Owner.m_NpcUI.PlayInVoice(eSoundVoiceControllListDB.Kanna_in);
				this.m_Owner.IsRequestVocie = false;
			}
			this.m_Owner.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Build);
			return true;
		}

		// Token: 0x06001692 RID: 5778 RVA: 0x00075B49 File Offset: 0x00073F49
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_Owner.m_NpcUI.PlayOut();
			this.GoToMenuEnd();
		}

		// Token: 0x06001693 RID: 5779 RVA: 0x00075B78 File Offset: 0x00073F78
		private void OnClickButtonCallBack()
		{
			if (this.m_UI.SelectButton == ShopBuildTopUI.eMainButton.LimitExtend)
			{
				this.LimitExtend();
				return;
			}
			if (this.m_UI.SelectButton == ShopBuildTopUI.eMainButton.LimitExtendTown)
			{
				this.LimitExtendTown();
				return;
			}
			if ((this.m_UI.SelectButton == ShopBuildTopUI.eMainButton.Buy || this.m_UI.SelectButton == ShopBuildTopUI.eMainButton.Sale) && !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsOccurredTips(eTutorialTipsListDB.Room_003))
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.RoomShopTutorialTitle, eText_MessageDB.RoomShopTutorialText, new Action<int>(this.OnConfirmTutorial));
				return;
			}
			this.GoToMenuEnd();
		}

		// Token: 0x06001694 RID: 5780 RVA: 0x00075C1C File Offset: 0x0007401C
		private void OnConfirmTutorial(int btn)
		{
			if (btn == 0)
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001695 RID: 5781 RVA: 0x00075C2C File Offset: 0x0007402C
		private void LimitExtend()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimitCount + 1 <= (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[18].m_Value)
			{
				int num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[17].m_Value;
				int num2 = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[16].m_Value;
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)num))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendGemIsShort, new object[]
					{
						num
					}), null, null);
				}
				else
				{
					int roomObjectLimit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit;
					int num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit + (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[16].m_Value;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtend, new object[]
					{
						UIUtility.GemValueToString(num, true),
						num2
					}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendSub, new object[]
					{
						roomObjectLimit,
						num3,
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true),
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem - (long)num, true)
					}), new Action<int>(this.OnConfirmLimitAdd));
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.RoomLimitExtendErr), null, null);
			}
		}

		// Token: 0x06001696 RID: 5782 RVA: 0x00075E74 File Offset: 0x00074274
		private void LimitExtendTown()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimitCount + 1 <= (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[27].m_Value)
			{
				int num = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[26].m_Value;
				int num2 = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[25].m_Value;
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)num))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendGemIsShort, new object[]
					{
						num
					}), null, null);
				}
				else
				{
					int facilityLimit = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimit;
					int num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimit + (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.m_Params[25].m_Value;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtend, new object[]
					{
						UIUtility.GemValueToString(num, true),
						num2
					}), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendSub, new object[]
					{
						facilityLimit,
						num3,
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true),
						UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem - (long)num, true)
					}), new Action<int>(this.OnConfirmLimitAddTown));
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.FacilityLimitExtendErr), null, null);
			}
		}

		// Token: 0x06001697 RID: 5783 RVA: 0x000760BB File Offset: 0x000744BB
		private void OnConfirmLimitAdd(int btn)
		{
			if (btn == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_RoomObjectLimitAdd(new Action<MeigewwwParam, RoomObjectResponseTypes.Limitadd>(this.OnResponseLimitAdd));
			}
		}

		// Token: 0x06001698 RID: 5784 RVA: 0x000760E0 File Offset: 0x000744E0
		private void OnResponseLimitAdd(MeigewwwParam wwwParam, RoomObjectResponseTypes.Limitadd param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
		}

		// Token: 0x06001699 RID: 5785 RVA: 0x0007611C File Offset: 0x0007451C
		private void OnConfirmLimitAddTown(int btn)
		{
			if (btn == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_TownFacilityLimitAdd(new Action<MeigewwwParam, TownFacilityResponseTypes.Limitadd>(this.OnResponseLimitAddTown));
			}
		}

		// Token: 0x0600169A RID: 5786 RVA: 0x00076140 File Offset: 0x00074540
		private void OnResponseLimitAddTown(MeigewwwParam wwwParam, TownFacilityResponseTypes.Limitadd param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
		}

		// Token: 0x0600169B RID: 5787 RVA: 0x0007617C File Offset: 0x0007457C
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001D4B RID: 7499
		private ShopState_BuildTop.eStep m_Step = ShopState_BuildTop.eStep.None;

		// Token: 0x04001D4C RID: 7500
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopBuildTopUI;

		// Token: 0x04001D4D RID: 7501
		private ShopBuildTopUI m_UI;

		// Token: 0x02000482 RID: 1154
		private enum eStep
		{
			// Token: 0x04001D4F RID: 7503
			None = -1,
			// Token: 0x04001D50 RID: 7504
			First,
			// Token: 0x04001D51 RID: 7505
			LoadStart,
			// Token: 0x04001D52 RID: 7506
			LoadWait,
			// Token: 0x04001D53 RID: 7507
			PlayIn,
			// Token: 0x04001D54 RID: 7508
			Main,
			// Token: 0x04001D55 RID: 7509
			UnloadChildSceneWait
		}
	}
}
