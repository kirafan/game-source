﻿using System;

namespace Star
{
	// Token: 0x02000549 RID: 1353
	public class CActScriptKeyViewMode : IRoomScriptData
	{
		// Token: 0x06001AC5 RID: 6853 RVA: 0x0008ED6C File Offset: 0x0008D16C
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyViewMode actXlsKeyViewMode = (ActXlsKeyViewMode)pbase;
			this.m_TargetName = actXlsKeyViewMode.m_TargetName;
			this.m_View = actXlsKeyViewMode.m_View;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x04002187 RID: 8583
		public string m_TargetName;

		// Token: 0x04002188 RID: 8584
		public bool m_View;

		// Token: 0x04002189 RID: 8585
		public uint m_CRCKey;
	}
}
