﻿using System;
using System.Collections.Generic;
using System.IO;
using CriMana;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000516 RID: 1302
	public class MovieManager
	{
		// Token: 0x060019A0 RID: 6560 RVA: 0x00085728 File Offset: 0x00083B28
		public void Initialize()
		{
			this.m_MovieIndices = new Dictionary<string, int>();
			string[] files = Directory.GetFiles(Application.streamingAssetsPath, "*.usm");
			for (int i = 0; i < files.Length; i++)
			{
				this.m_MovieIndices.Add(Path.GetFileName(files[i]), i);
			}
		}

		// Token: 0x060019A1 RID: 6561 RVA: 0x00085778 File Offset: 0x00083B78
		public void Destroy()
		{
			this.m_MovieIndices = null;
			this.m_LocalSaveData = null;
			for (int i = 0; i < this.m_InactiveObjs.Count; i++)
			{
				if (this.m_InactiveObjs[i] != null)
				{
					this.m_InactiveObjs[i].Destroy();
					this.m_InactiveObjs[i] = null;
				}
			}
			this.m_InactiveObjs.Clear();
			for (int j = 0; j < this.m_ActiveObjs.Count; j++)
			{
				if (this.m_ActiveObjs[j] != null)
				{
					this.m_ActiveObjs[j].Destroy();
					this.m_ActiveObjs[j] = null;
				}
			}
			this.m_ActiveObjs.Clear();
		}

		// Token: 0x060019A2 RID: 6562 RVA: 0x00085840 File Offset: 0x00083C40
		public void Update()
		{
			for (int i = this.m_ActiveObjs.Count - 1; i >= 0; i--)
			{
				MovieObject movieObject = this.m_ActiveObjs[i];
				movieObject.Update();
				if (movieObject.IsRemoved())
				{
					movieObject.RemoveRef();
					movieObject.Stop();
				}
				if (movieObject.GetStatus() == MovieObject.eStatus.Removed)
				{
					if (movieObject.GetStatusNative() == Player.Status.PlayEnd)
					{
						movieObject.Destroy();
					}
					if (movieObject.Ref <= 0)
					{
						this.SinkObject(movieObject);
					}
				}
			}
			for (int j = this.m_InactiveObjs.Count - 1; j >= 0; j--)
			{
				MovieObject movieObject2 = this.m_InactiveObjs[j];
				if (movieObject2.GetStatus() != MovieObject.eStatus.Removed)
				{
					movieObject2.Update();
				}
			}
		}

		// Token: 0x060019A3 RID: 6563 RVA: 0x00085904 File Offset: 0x00083D04
		public void Setup(LocalSaveData localSaveData)
		{
			for (int i = 0; i < 2; i++)
			{
				MovieObject item = this.CreateEmptyManagerObject();
				this.m_InactiveObjs.Add(item);
			}
			this.m_LocalSaveData = localSaveData;
			if (this.m_LocalSaveData != null)
			{
			}
		}

		// Token: 0x060019A4 RID: 6564 RVA: 0x00085948 File Offset: 0x00083D48
		private MovieObject CreateEmptyManagerObject()
		{
			return new MovieObject();
		}

		// Token: 0x060019A5 RID: 6565 RVA: 0x00085950 File Offset: 0x00083D50
		public void ForceResetOnReturnTitle()
		{
			for (int i = this.m_ActiveObjs.Count - 1; i >= 0; i--)
			{
				MovieObject movieObject = this.m_ActiveObjs[i];
				movieObject.Stop();
				this.SinkObject(movieObject);
			}
			this.m_ActiveObjs.Clear();
		}

		// Token: 0x060019A6 RID: 6566 RVA: 0x000859A4 File Offset: 0x00083DA4
		private MovieObject ScoopObject()
		{
			if (this.m_InactiveObjs.Count > 0)
			{
				MovieObject movieObject = this.m_InactiveObjs[0];
				this.m_ActiveObjs.Add(movieObject);
				this.m_InactiveObjs.Remove(movieObject);
				return movieObject;
			}
			return null;
		}

		// Token: 0x060019A7 RID: 6567 RVA: 0x000859EB File Offset: 0x00083DEB
		private void SinkObject(MovieObject o)
		{
			if (o != null && this.m_ActiveObjs.Remove(o))
			{
				this.m_InactiveObjs.Add(o);
			}
		}

		// Token: 0x060019A8 RID: 6568 RVA: 0x00085A10 File Offset: 0x00083E10
		public void RemoveHndl(MovieObjectHandler hndl, bool stopIfStatusIsNotRemoved = true)
		{
			if (hndl != null)
			{
				hndl.OnMngDestruct(stopIfStatusIsNotRemoved);
			}
		}

		// Token: 0x060019A9 RID: 6569 RVA: 0x00085A1F File Offset: 0x00083E1F
		public bool Prepare(MovieObjectHandler hndl, string moviePath, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic image = null, Material material = null)
		{
			return this.RequestPrepare(hndl, moviePath, volume, loop, alphaMovie, image, material);
		}

		// Token: 0x060019AA RID: 6570 RVA: 0x00085A32 File Offset: 0x00083E32
		public bool Play(MovieObjectHandler hndl = null, string moviePath = null, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic image = null, Material material = null)
		{
			return this.RequestPlay(hndl, moviePath, volume, loop, alphaMovie, image, material);
		}

		// Token: 0x060019AB RID: 6571 RVA: 0x00085A48 File Offset: 0x00083E48
		private bool RequestPrepare(MovieObjectHandler hndl, string moviePath, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic image = null, Material material = null)
		{
			if (hndl == null)
			{
				return false;
			}
			MovieObject movieObject = this.ScoopObject();
			if (movieObject == null)
			{
				return false;
			}
			bool flag;
			if (!string.IsNullOrEmpty(moviePath))
			{
				flag = movieObject.Prepare(moviePath, volume, loop, alphaMovie, image, material);
			}
			else
			{
				flag = movieObject.Prepare();
			}
			if (!flag)
			{
				this.SinkObject(movieObject);
				return false;
			}
			this.m_JustBeforePrepareMovieObject = movieObject;
			hndl.OnMngConstruct(movieObject);
			return true;
		}

		// Token: 0x060019AC RID: 6572 RVA: 0x00085AB4 File Offset: 0x00083EB4
		private bool RequestPlay(MovieObjectHandler hndl = null, string moviePath = null, MovieVolumeInformation volume = null, bool loop = false, bool alphaMovie = false, Graphic image = null, Material material = null)
		{
			MovieObject movieObject = this.m_JustBeforePrepareMovieObject;
			if (movieObject == null)
			{
				movieObject = this.ScoopObject();
			}
			this.m_JustBeforePrepareMovieObject = null;
			if (movieObject == null)
			{
				return false;
			}
			bool flag;
			if (!string.IsNullOrEmpty(moviePath))
			{
				flag = movieObject.Play(moviePath, volume, loop, alphaMovie, image, material);
			}
			else
			{
				flag = movieObject.Play(null, null, false, false, null, null);
			}
			if (!flag)
			{
				this.SinkObject(movieObject);
				return false;
			}
			if (hndl != null)
			{
				hndl.OnMngConstruct(movieObject);
			}
			else
			{
				movieObject.Constructed();
			}
			return true;
		}

		// Token: 0x060019AD RID: 6573 RVA: 0x00085B3C File Offset: 0x00083F3C
		public int GetDictionarySize()
		{
			return this.m_MovieIndices.Count;
		}

		// Token: 0x060019AE RID: 6574 RVA: 0x00085B4C File Offset: 0x00083F4C
		public string GetMoviePath(int i)
		{
			foreach (KeyValuePair<string, int> keyValuePair in this.m_MovieIndices)
			{
				if (keyValuePair.Value == i)
				{
					return keyValuePair.Key;
				}
			}
			return null;
		}

		// Token: 0x0400205C RID: 8284
		private LocalSaveData m_LocalSaveData;

		// Token: 0x0400205D RID: 8285
		private const int MOVIE_OBJ_POOL_NUM = 2;

		// Token: 0x0400205E RID: 8286
		private List<MovieObject> m_InactiveObjs = new List<MovieObject>(2);

		// Token: 0x0400205F RID: 8287
		private List<MovieObject> m_ActiveObjs = new List<MovieObject>(2);

		// Token: 0x04002060 RID: 8288
		private MovieObject m_JustBeforePrepareMovieObject;

		// Token: 0x04002061 RID: 8289
		private Dictionary<string, int> m_MovieIndices;
	}
}
