﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using StarApi.Models;

namespace Star
{
	// Token: 0x020003AB RID: 939
	public class UserTownUtil
	{
		// Token: 0x060011CD RID: 4557 RVA: 0x0005E030 File Offset: 0x0005C430
		public static UserTownObjectData GetTownObjData(long fmanageid)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].m_ManageID == fmanageid)
				{
					return userTownObjDatas[i];
				}
			}
			return null;
		}

		// Token: 0x060011CE RID: 4558 RVA: 0x0005E080 File Offset: 0x0005C480
		public static void LinkTownBuildToObjectData(UserTownData.BuildObjectData pbuild, bool fdebugup = false)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].m_ManageID == pbuild.m_ManageID)
				{
					pbuild.m_Object = userTownObjDatas[i];
					pbuild.m_Lv = userTownObjDatas[i].Lv;
					userTownObjDatas[i].BuildTime = pbuild.m_BuildingMs;
					userTownObjDatas[i].ActionTime = pbuild.m_ActionTime;
					userTownObjDatas[i].BuildTime = pbuild.m_BuildingMs;
					userTownObjDatas[i].ActionTime = pbuild.m_ActionTime;
					if (userTownObjDatas[i].OpenState == UserTownObjectData.eOpenState.Open)
					{
						pbuild.m_IsOpen = true;
					}
					else
					{
						pbuild.m_IsOpen = false;
					}
					break;
				}
			}
		}

		// Token: 0x060011CF RID: 4559 RVA: 0x0005E15C File Offset: 0x0005C55C
		public static UserTownData.BuildObjectData CreateUserTownBuildData(long fmanageid, int fbuildpoint, int fobjid, bool fopen, long ftime = 0L, long facttime = 0L)
		{
			if (ftime == 0L)
			{
				ftime = ScheduleTimeUtil.GetSystemTimeMs();
			}
			UserTownData.BuildObjectData buildObjectData = new UserTownData.BuildObjectData(fbuildpoint, fobjid, fopen, ftime, fmanageid, facttime);
			UserTownUtil.LinkTownBuildToObjectData(buildObjectData, false);
			return buildObjectData;
		}

		// Token: 0x060011D0 RID: 4560 RVA: 0x0005E18F File Offset: 0x0005C58F
		public static UserTownData.BuildObjectData GetTownBuildData(long fmanageid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(fmanageid);
		}

		// Token: 0x060011D1 RID: 4561 RVA: 0x0005E1A8 File Offset: 0x0005C5A8
		public static bool CalcTownBuildToDropItemState(ref FieldObjDropItem.DropItemInfo pstate, long fmanageid)
		{
			UserTownData.BuildObjectData buildObjectDataAtBuildMngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(fmanageid);
			if (buildObjectDataAtBuildMngID != null)
			{
				TownItemDropDatabase.DropItemTool dropItemTool = TownItemDropDatabase.CalcDropPeformsnce(ref pstate, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(buildObjectDataAtBuildMngID.m_ObjID).m_LevelUpListID, buildObjectDataAtBuildMngID.m_Lv);
				if (dropItemTool != null)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060011D2 RID: 4562 RVA: 0x0005E20C File Offset: 0x0005C60C
		public static bool FindTownBuildToDropItemState(ref FieldObjDropItem.DropItemInfo pstate, eTownObjectCategory fcategory)
		{
			int num = -1;
			int num2 = -1;
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				if (buildObjectDataAt.m_IsOpen && SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(buildObjectDataAt.m_ObjID).m_Category == (int)fcategory && buildObjectDataAt.m_Lv > num2)
				{
					num = i;
				}
			}
			if (num >= 0)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(num);
				TownItemDropDatabase.DropItemTool dropItemTool = TownItemDropDatabase.CalcDropPeformsnce(ref pstate, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(buildObjectDataAt.m_ObjID).m_LevelUpListID, buildObjectDataAt.m_Lv);
				if (dropItemTool != null)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060011D3 RID: 4563 RVA: 0x0005E2E0 File Offset: 0x0005C6E0
		public static UserTownData.BuildObjectData GetTownPointToBuildData(int fpoint)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(fpoint);
		}

		// Token: 0x060011D4 RID: 4564 RVA: 0x0005E2F8 File Offset: 0x0005C6F8
		public static string CreateTownBuildListString(UserTownData.BuildObjectUp padd = null)
		{
			TownServerForm.DataChunk dataChunk = new TownServerForm.DataChunk();
			TownServerForm.Build build = new TownServerForm.Build();
			build.m_Table = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.CreateBuildTable();
			if (padd != null)
			{
				build.m_Table.Add(padd);
			}
			string s = JsonConvert.SerializeObject(build);
			dataChunk.m_Ver = 1;
			dataChunk.m_DataType = 0;
			dataChunk.m_Data = Encoding.ASCII.GetBytes(s);
			string s2 = JsonConvert.SerializeObject(dataChunk);
			return Convert.ToBase64String(Encoding.ASCII.GetBytes(s2));
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x0005E37C File Offset: 0x0005C77C
		public static TownServerForm.DataChunk GetDataChunk(string fcode)
		{
			string @string = Encoding.ASCII.GetString(Convert.FromBase64String(fcode));
			return JsonConvert.DeserializeObject<TownServerForm.DataChunk>(@string);
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x0005E3A0 File Offset: 0x0005C7A0
		public static TownServerForm.Build BuildTownBuildList(TownServerForm.DataChunk pchunk)
		{
			if (pchunk.m_DataType == 0)
			{
				string @string = Encoding.ASCII.GetString(pchunk.m_Data);
				return JsonConvert.DeserializeObject<TownServerForm.Build>(@string);
			}
			return null;
		}

		// Token: 0x060011D7 RID: 4567 RVA: 0x0005E3D4 File Offset: 0x0005C7D4
		public static string CreateTownDebugPlayString()
		{
			TownServerForm.DataChunk dataChunk = new TownServerForm.DataChunk();
			string s = JsonConvert.SerializeObject(new TownServerForm.DebugPlay
			{
				m_DebugPlay = true,
				m_Level = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level
			});
			dataChunk.m_Ver = 1;
			dataChunk.m_DataType = 1;
			dataChunk.m_Data = Encoding.ASCII.GetBytes(s);
			string s2 = JsonConvert.SerializeObject(dataChunk);
			return Convert.ToBase64String(Encoding.ASCII.GetBytes(s2));
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x0005E44C File Offset: 0x0005C84C
		public static TownServerForm.DebugPlay BuildTownDebugCode(TownServerForm.DataChunk pchunk)
		{
			if (pchunk.m_DataType == 1)
			{
				string @string = Encoding.ASCII.GetString(pchunk.m_Data);
				return JsonConvert.DeserializeObject<TownServerForm.DebugPlay>(@string);
			}
			return null;
		}

		// Token: 0x060011D9 RID: 4569 RVA: 0x0005E480 File Offset: 0x0005C880
		public static long GetBuildUpTime(int fobjid, int flevel)
		{
			long num = (long)TownUtility.GetTownObjectLevelUpConfirm(fobjid, flevel + 1, TownDefine.eTownLevelUpConditionCategory.BuildTime);
			if (num == 0L)
			{
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_BuildTime * 1000L;
			}
			return num * 1000L;
		}

		// Token: 0x060011DA RID: 4570 RVA: 0x0005E4D0 File Offset: 0x0005C8D0
		public static bool IsBuildMarks(int fobjid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid).m_BuildTime > 0f;
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x0005E504 File Offset: 0x0005C904
		public static bool IsStoreTownObject(int fobjid)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid);
			int num = 0;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].ObjID == fobjid)
				{
					num++;
				}
			}
			return (int)param.m_MaxNum > num;
		}

		// Token: 0x060011DC RID: 4572 RVA: 0x0005E570 File Offset: 0x0005C970
		public static bool IsAddTownObjData(int fobjid)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid);
			int num = 0;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].ObjID == fobjid)
				{
					num++;
				}
			}
			return (int)param.m_MaxNum > num;
		}

		// Token: 0x060011DD RID: 4573 RVA: 0x0005E5DC File Offset: 0x0005C9DC
		public static long GetStoreTownObj(int fobjid)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].ObjID == fobjid)
				{
					return userTownObjDatas[i].m_ManageID;
				}
			}
			return -1L;
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x0005E634 File Offset: 0x0005CA34
		public static void ClearTownObjData()
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			int count = userTownObjDatas.Count;
			for (int i = count - 1; i >= 0; i--)
			{
				userTownObjDatas.RemoveAt(i);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetStoreDatas().Clear();
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x0005E68C File Offset: 0x0005CA8C
		public static void AddTownObjData(UserTownObjectData padd)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			userTownObjDatas.Add(padd);
			SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.AddStoreDataNum(padd.ObjID, 1);
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x0005E6CC File Offset: 0x0005CACC
		public static void RemoveTownObjData(long fmanageid)
		{
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				if (userTownObjDatas[i].m_ManageID == fmanageid)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.AddStoreDataNum(userTownObjDatas[i].ObjID, -1);
					userTownObjDatas.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x060011E1 RID: 4577 RVA: 0x0005E740 File Offset: 0x0005CB40
		public static int GetTownObjBuildNum(int objID)
		{
			int buildObjectDataNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataNum();
			int num = 0;
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAt(i).m_ObjID == objID)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x0005E79C File Offset: 0x0005CB9C
		public static int GetTownObjStockNum()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			int num = 0;
			for (int i = 0; i < userTownObjDatas.Count; i++)
			{
				UserTownObjectData userTownObjectData = userTownObjDatas[i];
				int objID = userTownObjDatas[i].ObjID;
				TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
				if (param.m_Category != 4 && param.m_Category != 6)
				{
					bool flag = false;
					for (int j = 0; j < userTownData.GetBuildObjectDataNum(); j++)
					{
						if (userTownData.GetBuildObjectDataAt(j).m_ManageID == userTownObjectData.m_ManageID)
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						num++;
					}
				}
			}
			return num;
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x0005E880 File Offset: 0x0005CC80
		public static long MakeBuildObjectToFreeManageID()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			long num = 0L;
			int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				if (num < userTownData.GetBuildObjectDataAt(i).m_ManageID)
				{
					num = userTownData.GetBuildObjectDataAt(i).m_ManageID;
				}
			}
			return num + 1L;
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x0005E8DC File Offset: 0x0005CCDC
		public static bool CheckPointToBuilding(int fbuildpoint, int fobjid)
		{
			TownObjectListDB_Param townResouceData = UserTownUtil.GetTownResouceData(fobjid);
			bool result = false;
			int pointToBuildType = TownUtility.GetPointToBuildType(fbuildpoint);
			if (pointToBuildType != 0)
			{
				if (pointToBuildType != 268435456)
				{
					if (pointToBuildType != 536870912)
					{
						if (pointToBuildType == 1073741824)
						{
							if (townResouceData.m_Category == 1 || townResouceData.m_Category == 2 || townResouceData.m_Category == 5)
							{
								result = true;
							}
						}
					}
					else if (townResouceData.m_Category == 6)
					{
						result = true;
					}
				}
				else if (townResouceData.m_Category == 7)
				{
					result = true;
				}
			}
			else if (townResouceData.m_Category == 4 || townResouceData.m_Category == 6)
			{
				result = true;
			}
			return result;
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x0005E9A0 File Offset: 0x0005CDA0
		public static int GetSettingOptionKey(int fobjid)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid);
			TownObjectLevelUpDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, 1);
			if (param2.m_TownUseFunc != 0 && param2.m_TownUseFunc == 1)
			{
				int num = 0;
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
				for (int i = 0; i < buildObjectDataNum; i++)
				{
					UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
					if (buildObjectDataAt.m_BuildPointIndex != 0 && TownUtility.GetPointToBuildType(buildObjectDataAt.m_BuildPointIndex) == 0)
					{
						num++;
					}
				}
				return num + param2.m_TownActionStepKey;
			}
			return 0;
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x0005EA60 File Offset: 0x0005CE60
		public static bool IsManageIDToPreComplete(long fmanageid)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			if (!townBuildData.m_IsOpen)
			{
				long systemTimeMs = ScheduleTimeUtil.GetSystemTimeMs();
				if (systemTimeMs - townBuildData.m_BuildingMs >= UserTownUtil.GetBuildUpTime(townBuildData.m_ObjID, townBuildData.m_Lv))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x0005EAA8 File Offset: 0x0005CEA8
		public static long GetBuildTimeMs(long fmanageid)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			long systemTimeMs = ScheduleTimeUtil.GetSystemTimeMs();
			return systemTimeMs - townBuildData.m_BuildingMs;
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x0005EACA File Offset: 0x0005CECA
		public static TownObjectListDB_Param GetTownResouceData(int fresid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fresid);
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x0005EAE4 File Offset: 0x0005CEE4
		public static long GetRemainingTime(long fmanageid, int fprebuild = -1)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			long num = 0L;
			if (!townBuildData.m_IsOpen || fprebuild != -1)
			{
				int flevel = townBuildData.m_Lv;
				if (fprebuild >= 0)
				{
					flevel = fprebuild;
				}
				num = ScheduleTimeUtil.GetSystemTimeMs() - townBuildData.m_BuildingMs;
				long buildUpTime = UserTownUtil.GetBuildUpTime(townBuildData.m_ObjID, flevel);
				if (num > buildUpTime)
				{
					num = buildUpTime;
				}
				num = buildUpTime - num;
			}
			return num;
		}

		// Token: 0x060011EA RID: 4586 RVA: 0x0005EB48 File Offset: 0x0005CF48
		public static long GetRemainingUseGem(long fmanageid)
		{
			long remainingTime = UserTownUtil.GetRemainingTime(fmanageid, -1);
			return StarDefine.CalcRemainGemNum(remainingTime);
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x0005EB64 File Offset: 0x0005CF64
		public static float CalcScheduleDropPer(float fbase)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			int num = 0;
			int buildObjectDataNum = userTownData.GetBuildObjectDataNum();
			for (int i = 0; i < buildObjectDataNum; i++)
			{
				if (TownUtility.GetPointToBuildType(userTownData.GetBuildObjectDataAt(i).m_BuildPointIndex) == 0)
				{
					num++;
				}
			}
			switch (num)
			{
			case 0:
				return fbase * 0.7f;
			case 1:
				return fbase * 0.8f;
			case 2:
				return fbase * 0.9f;
			case 3:
				return fbase * 1f;
			case 4:
				return fbase * 1.1f;
			case 5:
				return fbase * 1.2f;
			}
			fbase *= 1.3f;
			return fbase;
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x0005EC48 File Offset: 0x0005D048
		public static UserScheduleData.DropState GetFieldDropItems(int fkeyid)
		{
			FieldItemDropListDB fieldItemDropListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.FieldItemDropListDB;
			int num = fieldItemDropListDB.m_Params.Length;
			UserScheduleData.DropState dropState = null;
			for (int i = 0; i < num; i++)
			{
				if (fieldItemDropListDB.m_Params[i].m_ID == fkeyid)
				{
					switch (fieldItemDropListDB.m_Params[i].m_Category[0])
					{
					case 1:
						dropState = new UserScheduleData.DropState();
						dropState.m_Category = UserScheduleData.eDropCategory.Money;
						dropState.m_Num = fieldItemDropListDB.m_Params[i].m_Num[0];
						break;
					case 2:
						dropState = new UserScheduleData.DropState();
						dropState.m_Category = UserScheduleData.eDropCategory.Item;
						dropState.m_CategoryNo = fieldItemDropListDB.m_Params[i].m_ObjectID[0];
						dropState.m_Num = fieldItemDropListDB.m_Params[i].m_Num[0];
						break;
					case 3:
						dropState = new UserScheduleData.DropState();
						dropState.m_Category = UserScheduleData.eDropCategory.KRRPoint;
						dropState.m_Num = fieldItemDropListDB.m_Params[i].m_Num[0];
						break;
					case 4:
						dropState = new UserScheduleData.DropState();
						dropState.m_Category = UserScheduleData.eDropCategory.Stamina;
						dropState.m_Num = fieldItemDropListDB.m_Params[i].m_Num[0];
						break;
					case 5:
						dropState = new UserScheduleData.DropState();
						dropState.m_Category = UserScheduleData.eDropCategory.FriendShip;
						dropState.m_Num = fieldItemDropListDB.m_Params[i].m_Num[0];
						break;
					}
					break;
				}
			}
			return dropState;
		}
	}
}
