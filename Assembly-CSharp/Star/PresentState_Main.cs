﻿using System;
using System.Collections.Generic;
using Star.UI;
using Star.UI.Global;
using Star.UI.Present;

namespace Star
{
	// Token: 0x02000455 RID: 1109
	public class PresentState_Main : PresentState
	{
		// Token: 0x06001566 RID: 5478 RVA: 0x0006F6D9 File Offset: 0x0006DAD9
		public PresentState_Main(PresentMain owner) : base(owner)
		{
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x0006F6FC File Offset: 0x0006DAFC
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001568 RID: 5480 RVA: 0x0006F6FF File Offset: 0x0006DAFF
		public override void OnStateEnter()
		{
			this.m_Step = PresentState_Main.eStep.First;
		}

		// Token: 0x06001569 RID: 5481 RVA: 0x0006F708 File Offset: 0x0006DB08
		public override void OnStateExit()
		{
		}

		// Token: 0x0600156A RID: 5482 RVA: 0x0006F70A File Offset: 0x0006DB0A
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600156B RID: 5483 RVA: 0x0006F714 File Offset: 0x0006DB14
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case PresentState_Main.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = PresentState_Main.eStep.LoadWait;
				break;
			case PresentState_Main.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = PresentState_Main.eStep.PlayIn;
				}
				break;
			case PresentState_Main.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = PresentState_Main.eStep.Main;
				}
				break;
			case PresentState_Main.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq() == eTutorialSeq.PresentGot_NextIsADVGachaPrevPlay)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.Clear();
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.ADV;
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000000, SceneDefine.eSceneID.ADV, false);
						}
						else
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						}
						this.m_NextState = 2147483646;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = PresentState_Main.eStep.UnloadChildSceneWait;
				}
				break;
			case PresentState_Main.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = PresentState_Main.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600156C RID: 5484 RVA: 0x0006F890 File Offset: 0x0006DC90
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<PresentUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.GetPresentWindow().OnClickExecuteGet += this.ExecuteGet;
			this.m_UI.GetPresentWindow().OnClickGetMultiButton += this.ExecuteGetMulti;
			this.m_UI.GetPresentWindow().OnEndResultWindow += this.OnEndResultWindow;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq != eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				inst.GlobalUI.Open();
				inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Present);
				inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			}
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x0006F98C File Offset: 0x0006DD8C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Present)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x0006F9E4 File Offset: 0x0006DDE4
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x0006F9F1 File Offset: 0x0006DDF1
		public void ExecuteGet(long presentMngID)
		{
			this.m_RequestMngIDList.Clear();
			this.m_RequestMngIDList.Add(presentMngID);
			SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.Request_Get(this.m_RequestMngIDList, new Action<bool>(this.OnResponse_Get));
		}

		// Token: 0x06001570 RID: 5488 RVA: 0x0006FA2C File Offset: 0x0006DE2C
		public void ExecuteGetMulti(List<long> presentMngIDs)
		{
			this.m_RequestMngIDList = presentMngIDs;
			SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.Request_Get(presentMngIDs, new Action<bool>(this.OnResponse_Get));
		}

		// Token: 0x06001571 RID: 5489 RVA: 0x0006FA54 File Offset: 0x0006DE54
		public void OnResponse_Get(bool isError)
		{
			if (!isError)
			{
				this.m_UI.GetPresentWindow().Refresh();
				List<PresentManager.PresentData> gotList = SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetGotList();
				if (gotList.Count > 0 && this.m_UI.GetPresentWindow().IsGetMulti)
				{
					this.m_UI.GetPresentWindow().OpenResult(gotList);
				}
				if (this.m_RequestMngIDList.Count > gotList.Count)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PresentGetTitle, eText_MessageDB.PresentHaveNumOver, null);
				}
				for (int i = 0; i < gotList.Count; i++)
				{
					if (gotList[i].m_AltItemID != -1)
					{
						string name = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(gotList[i].m_ID).m_Name;
						string name2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(gotList[i].m_AltItemID).m_Name;
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PresentGetTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PresentAltResult, new object[]
						{
							name,
							name2
						}), null, null);
					}
				}
			}
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x0006FBB0 File Offset: 0x0006DFB0
		public void OnEndResultWindow()
		{
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.PresentGot_NextIsADVGachaPrevPlay)
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x04001C37 RID: 7223
		private PresentState_Main.eStep m_Step = PresentState_Main.eStep.None;

		// Token: 0x04001C38 RID: 7224
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.PresentUI;

		// Token: 0x04001C39 RID: 7225
		private PresentUI m_UI;

		// Token: 0x04001C3A RID: 7226
		private List<long> m_RequestMngIDList = new List<long>();

		// Token: 0x02000456 RID: 1110
		private enum eStep
		{
			// Token: 0x04001C3C RID: 7228
			None = -1,
			// Token: 0x04001C3D RID: 7229
			First,
			// Token: 0x04001C3E RID: 7230
			LoadWait,
			// Token: 0x04001C3F RID: 7231
			PlayIn,
			// Token: 0x04001C40 RID: 7232
			Main,
			// Token: 0x04001C41 RID: 7233
			UnloadChildSceneWait
		}
	}
}
