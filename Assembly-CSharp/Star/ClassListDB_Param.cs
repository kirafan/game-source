﻿using System;

namespace Star
{
	// Token: 0x0200017C RID: 380
	[Serializable]
	public struct ClassListDB_Param
	{
		// Token: 0x04000A0D RID: 2573
		public string m_ResouceBaseName;

		// Token: 0x04000A0E RID: 2574
		public int m_NormalAttackSkillID;

		// Token: 0x04000A0F RID: 2575
		public int m_DefaultWeaponID;
	}
}
