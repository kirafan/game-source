﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x02000646 RID: 1606
	public class GameGlobalParameter
	{
		// Token: 0x06001FCD RID: 8141 RVA: 0x000AC080 File Offset: 0x000AA480
		public GameGlobalParameter()
		{
			this.Reset();
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x06001FCE RID: 8142 RVA: 0x000AC08E File Offset: 0x000AA48E
		// (set) Token: 0x06001FCF RID: 8143 RVA: 0x000AC096 File Offset: 0x000AA496
		public bool IsRequestedReturnTitle { get; set; }

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x06001FD0 RID: 8144 RVA: 0x000AC09F File Offset: 0x000AA49F
		// (set) Token: 0x06001FD1 RID: 8145 RVA: 0x000AC0A7 File Offset: 0x000AA4A7
		public List<UserBattlePartyData> SavedPartyData { get; set; }

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x06001FD2 RID: 8146 RVA: 0x000AC0B0 File Offset: 0x000AA4B0
		// (set) Token: 0x06001FD3 RID: 8147 RVA: 0x000AC0B8 File Offset: 0x000AA4B8
		public int SelectedPartyMemberIndex { get; set; }

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06001FD4 RID: 8148 RVA: 0x000AC0C1 File Offset: 0x000AA4C1
		// (set) Token: 0x06001FD5 RID: 8149 RVA: 0x000AC0C9 File Offset: 0x000AA4C9
		public long SelectedCharaMngID { get; set; }

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06001FD6 RID: 8150 RVA: 0x000AC0D2 File Offset: 0x000AA4D2
		// (set) Token: 0x06001FD7 RID: 8151 RVA: 0x000AC0DA File Offset: 0x000AA4DA
		public int SelectedSupportPartyIndex { get; set; }

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06001FD8 RID: 8152 RVA: 0x000AC0E3 File Offset: 0x000AA4E3
		// (set) Token: 0x06001FD9 RID: 8153 RVA: 0x000AC0EB File Offset: 0x000AA4EB
		public int SelectedSupportPartyMemberIndex { get; set; }

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06001FDA RID: 8154 RVA: 0x000AC0F4 File Offset: 0x000AA4F4
		// (set) Token: 0x06001FDB RID: 8155 RVA: 0x000AC0FC File Offset: 0x000AA4FC
		public long SelectedSupportCharaMngID { get; set; }

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x06001FDC RID: 8156 RVA: 0x000AC105 File Offset: 0x000AA505
		// (set) Token: 0x06001FDD RID: 8157 RVA: 0x000AC10D File Offset: 0x000AA50D
		public FriendManager.FriendData QuestFriendData { get; set; }

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x06001FDE RID: 8158 RVA: 0x000AC116 File Offset: 0x000AA516
		// (set) Token: 0x06001FDF RID: 8159 RVA: 0x000AC11E File Offset: 0x000AA51E
		public UserSupportData QuestUserSupportData { get; set; }

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x06001FE0 RID: 8160 RVA: 0x000AC127 File Offset: 0x000AA527
		// (set) Token: 0x06001FE1 RID: 8161 RVA: 0x000AC12F File Offset: 0x000AA52F
		public bool IsRequestAPIWhenEnterQuestScene { get; set; }

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06001FE2 RID: 8162 RVA: 0x000AC138 File Offset: 0x000AA538
		// (set) Token: 0x06001FE3 RID: 8163 RVA: 0x000AC140 File Offset: 0x000AA540
		public bool IsDirtyPartyEdit { get; set; }

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06001FE4 RID: 8164 RVA: 0x000AC149 File Offset: 0x000AA549
		// (set) Token: 0x06001FE5 RID: 8165 RVA: 0x000AC151 File Offset: 0x000AA551
		public bool IsHomeModeStart { get; set; }

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06001FE6 RID: 8166 RVA: 0x000AC15A File Offset: 0x000AA55A
		// (set) Token: 0x06001FE7 RID: 8167 RVA: 0x000AC162 File Offset: 0x000AA562
		public long RoomPlayerID { get; set; }

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06001FE8 RID: 8168 RVA: 0x000AC16B File Offset: 0x000AA56B
		// (set) Token: 0x06001FE9 RID: 8169 RVA: 0x000AC173 File Offset: 0x000AA573
		public int AdvID { get; set; }

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06001FEA RID: 8170 RVA: 0x000AC17C File Offset: 0x000AA57C
		// (set) Token: 0x06001FEB RID: 8171 RVA: 0x000AC184 File Offset: 0x000AA584
		public SceneDefine.eSceneID AdvToNextSceneID { get; set; }

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06001FEC RID: 8172 RVA: 0x000AC18D File Offset: 0x000AA58D
		// (set) Token: 0x06001FED RID: 8173 RVA: 0x000AC195 File Offset: 0x000AA595
		public bool AdvOnlyQuest { get; set; }

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06001FEE RID: 8174 RVA: 0x000AC19E File Offset: 0x000AA59E
		// (set) Token: 0x06001FEF RID: 8175 RVA: 0x000AC1A6 File Offset: 0x000AA5A6
		public string MoviePlayFileNameWithoutExt { get; set; }

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06001FF0 RID: 8176 RVA: 0x000AC1AF File Offset: 0x000AA5AF
		// (set) Token: 0x06001FF1 RID: 8177 RVA: 0x000AC1B7 File Offset: 0x000AA5B7
		public SceneDefine.eSceneID MoviePlayToNextSceneID { get; set; }

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06001FF2 RID: 8178 RVA: 0x000AC1C0 File Offset: 0x000AA5C0
		// (set) Token: 0x06001FF3 RID: 8179 RVA: 0x000AC1C8 File Offset: 0x000AA5C8
		public bool IsMenuFriendStart { get; set; }

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06001FF4 RID: 8180 RVA: 0x000AC1D1 File Offset: 0x000AA5D1
		// (set) Token: 0x06001FF5 RID: 8181 RVA: 0x000AC1D9 File Offset: 0x000AA5D9
		public bool IsMenuLibraryStart { get; set; }

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x06001FF6 RID: 8182 RVA: 0x000AC1E2 File Offset: 0x000AA5E2
		// (set) Token: 0x06001FF7 RID: 8183 RVA: 0x000AC1EA File Offset: 0x000AA5EA
		public bool IsMenuUpgradeStart { get; set; }

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x06001FF8 RID: 8184 RVA: 0x000AC1F3 File Offset: 0x000AA5F3
		// (set) Token: 0x06001FF9 RID: 8185 RVA: 0x000AC1FB File Offset: 0x000AA5FB
		public bool IsMenuBackToHome { get; set; }

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x06001FFA RID: 8186 RVA: 0x000AC204 File Offset: 0x000AA604
		// (set) Token: 0x06001FFB RID: 8187 RVA: 0x000AC20C File Offset: 0x000AA60C
		public bool IsOccuredOpenInfo { get; set; }

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x06001FFC RID: 8188 RVA: 0x000AC215 File Offset: 0x000AA615
		// (set) Token: 0x06001FFD RID: 8189 RVA: 0x000AC21D File Offset: 0x000AA61D
		public float RefreshBadgeTimeSec { get; set; }

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x06001FFE RID: 8190 RVA: 0x000AC226 File Offset: 0x000AA626
		// (set) Token: 0x06001FFF RID: 8191 RVA: 0x000AC22E File Offset: 0x000AA62E
		public GameGlobalParameter.eQuestStartMode QuestStartMode { get; set; }

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06002000 RID: 8192 RVA: 0x000AC237 File Offset: 0x000AA637
		// (set) Token: 0x06002001 RID: 8193 RVA: 0x000AC23F File Offset: 0x000AA63F
		public bool QuestFirstCleared { get; set; }

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06002002 RID: 8194 RVA: 0x000AC248 File Offset: 0x000AA648
		// (set) Token: 0x06002003 RID: 8195 RVA: 0x000AC250 File Offset: 0x000AA650
		public GameGlobalParameter.MenuLibraryADVParam MenuLibADVParam { get; set; }

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06002004 RID: 8196 RVA: 0x000AC259 File Offset: 0x000AA659
		// (set) Token: 0x06002005 RID: 8197 RVA: 0x000AC261 File Offset: 0x000AA661
		public GameGlobalParameter.RoomShopRequestParam RoomShopReqParam { get; set; }

		// Token: 0x06002006 RID: 8198 RVA: 0x000AC26C File Offset: 0x000AA66C
		public void Reset()
		{
			this.ResetSelectionQuestParam();
			this.SavedPartyData = null;
			this.SelectedPartyMemberIndex = 0;
			this.SelectedCharaMngID = -1L;
			this.SelectedSupportPartyIndex = 0;
			this.SelectedSupportPartyMemberIndex = 0;
			this.SelectedSupportCharaMngID = -1L;
			this.QuestStartMode = GameGlobalParameter.eQuestStartMode.None;
			this.QuestFirstCleared = false;
			this.AdvID = -1;
			this.AdvToNextSceneID = SceneDefine.eSceneID.Town;
			this.AdvOnlyQuest = false;
			this.MenuLibADVParam = null;
			this.MoviePlayFileNameWithoutExt = null;
			this.MoviePlayToNextSceneID = SceneDefine.eSceneID.Town;
			this.RoomPlayerID = -1L;
			this.IsDirtyPartyEdit = false;
			this.IsHomeModeStart = true;
			this.RoomShopReqParam = null;
			this.IsMenuFriendStart = false;
			this.IsMenuLibraryStart = false;
			this.IsMenuUpgradeStart = false;
			this.IsMenuBackToHome = false;
			this.IsOccuredOpenInfo = false;
			this.RefreshBadgeTimeSec = 0f;
		}

		// Token: 0x06002007 RID: 8199 RVA: 0x000AC32E File Offset: 0x000AA72E
		public void ResetSelectionQuestParam()
		{
			this.QuestFriendData = null;
			this.QuestUserSupportData = null;
			this.IsRequestAPIWhenEnterQuestScene = true;
		}

		// Token: 0x06002008 RID: 8200 RVA: 0x000AC345 File Offset: 0x000AA745
		public void SetAdvParam(int advID, SceneDefine.eSceneID advToNextSceneID, bool advOnlyQuest = false)
		{
			this.AdvID = advID;
			this.AdvToNextSceneID = advToNextSceneID;
			this.AdvOnlyQuest = advOnlyQuest;
		}

		// Token: 0x06002009 RID: 8201 RVA: 0x000AC35C File Offset: 0x000AA75C
		public void SetMoviePlayParam(string moviePlayFileNameWithoutExt, SceneDefine.eSceneID moviePlayToNextSceneID)
		{
			this.MoviePlayFileNameWithoutExt = moviePlayFileNameWithoutExt;
			this.MoviePlayToNextSceneID = moviePlayToNextSceneID;
		}

		// Token: 0x02000647 RID: 1607
		public enum eQuestStartMode
		{
			// Token: 0x04002667 RID: 9831
			None = -1,
			// Token: 0x04002668 RID: 9832
			List
		}

		// Token: 0x02000648 RID: 1608
		public class MenuLibraryADVParam
		{
			// Token: 0x04002669 RID: 9833
			public int m_AdvId;

			// Token: 0x0400266A RID: 9834
			public int m_AdvLibID;

			// Token: 0x0400266B RID: 9835
			public eCharaNamedType m_Named;

			// Token: 0x0400266C RID: 9836
			public int m_SingleorCross;
		}

		// Token: 0x02000649 RID: 1609
		public class RoomShopRequestParam
		{
			// Token: 0x0600200B RID: 8203 RVA: 0x000AC374 File Offset: 0x000AA774
			public RoomShopRequestParam(bool isBuy, eRoomObjectCategory category, int id)
			{
				this.m_IsBuy = isBuy;
				this.m_Category = category;
				this.m_RoomObjID = id;
			}

			// Token: 0x0400266D RID: 9837
			public bool m_IsBuy;

			// Token: 0x0400266E RID: 9838
			public eRoomObjectCategory m_Category;

			// Token: 0x0400266F RID: 9839
			public int m_RoomObjID;
		}
	}
}
