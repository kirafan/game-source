﻿using System;

namespace Star
{
	// Token: 0x020001AF RID: 431
	public static class SoundVoiceControllListDB_Ext
	{
		// Token: 0x06000B26 RID: 2854 RVA: 0x00042658 File Offset: 0x00040A58
		public static SoundVoiceControllListDB_Param GetParam(this SoundVoiceControllListDB self, eSoundVoiceControllListDB id)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)id)
				{
					return self.m_Params[i];
				}
			}
			return default(SoundVoiceControllListDB_Param);
		}
	}
}
