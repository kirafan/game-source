﻿using System;

namespace Star
{
	// Token: 0x020006A5 RID: 1701
	public class TownHandleActionModelObj : ITownHandleAction
	{
		// Token: 0x06002214 RID: 8724 RVA: 0x000B54CF File Offset: 0x000B38CF
		public TownHandleActionModelObj(int fobjid, long fmanageid)
		{
			this.m_Type = eTownEventAct.ChangeModel;
			this.m_ObjectID = fobjid;
			this.m_ManageID = fmanageid;
		}

		// Token: 0x04002897 RID: 10391
		public int m_ObjectID;

		// Token: 0x04002898 RID: 10392
		public long m_ManageID;
	}
}
