﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020003DF RID: 991
	public class BattleMain : GameStateMain
	{
		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060012DA RID: 4826 RVA: 0x00064EE9 File Offset: 0x000632E9
		public BattleSystem System
		{
			get
			{
				return this.m_System;
			}
		}

		// Token: 0x060012DB RID: 4827 RVA: 0x00064EF1 File Offset: 0x000632F1
		private void Start()
		{
			base.SetNextState(1);
		}

		// Token: 0x060012DC RID: 4828 RVA: 0x00064EFA File Offset: 0x000632FA
		protected override void Update()
		{
			base.Update();
			this.m_System.SystemUpdate();
		}

		// Token: 0x060012DD RID: 4829 RVA: 0x00064F0D File Offset: 0x0006330D
		private void LateUpdate()
		{
			this.m_System.SystemLateUpdate();
		}

		// Token: 0x060012DE RID: 4830 RVA: 0x00064F1C File Offset: 0x0006331C
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 1)
			{
				return new BattleState_Main(this);
			}
			if (nextStateID == 2)
			{
				return new BattleState_Result(this);
			}
			if (nextStateID != 3)
			{
				return null;
			}
			return new BattleState_Final(this);
		}

		// Token: 0x040019B3 RID: 6579
		public const int STATE_MAIN = 1;

		// Token: 0x040019B4 RID: 6580
		public const int STATE_RESULT = 2;

		// Token: 0x040019B5 RID: 6581
		public const int STATE_FINAL = 3;

		// Token: 0x040019B6 RID: 6582
		[SerializeField]
		private BattleSystem m_System;
	}
}
