﻿using System;

namespace Star
{
	// Token: 0x020004AE RID: 1198
	public class TownComBuild : IMainComCommand
	{
		// Token: 0x0600176A RID: 5994 RVA: 0x000794E2 File Offset: 0x000778E2
		public TownComBuild(eTownCommand fcommand)
		{
			this.m_BuildType = fcommand;
		}

		// Token: 0x0600176B RID: 5995 RVA: 0x000794F1 File Offset: 0x000778F1
		public override int GetHashCode()
		{
			return 3;
		}

		// Token: 0x04001E24 RID: 7716
		public int m_BuildPoint;

		// Token: 0x04001E25 RID: 7717
		public int m_ObjID;

		// Token: 0x04001E26 RID: 7718
		public eTownCommand m_BuildType;
	}
}
