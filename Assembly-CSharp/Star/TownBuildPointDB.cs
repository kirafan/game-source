﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006F2 RID: 1778
	public class TownBuildPointDB : ScriptableObject
	{
		// Token: 0x04002A16 RID: 10774
		public TownBuildPointDB_Param[] m_Params;
	}
}
