﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008CE RID: 2254
	public class iTweenScaleToWrapper : iTweenWrapper
	{
		// Token: 0x06002EB6 RID: 11958 RVA: 0x000F4F2D File Offset: 0x000F332D
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
			iTween.ScaleTo(target, args);
		}

		// Token: 0x06002EB7 RID: 11959 RVA: 0x000F4F36 File Offset: 0x000F3336
		public override void Stop()
		{
			if (base.IsPlaying())
			{
				iTween.Stop(this.m_Target, "scale");
				base.Stop();
			}
		}
	}
}
