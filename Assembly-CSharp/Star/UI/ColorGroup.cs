﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000806 RID: 2054
	public class ColorGroup : MonoBehaviour
	{
		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x06002A8B RID: 10891 RVA: 0x000E0B76 File Offset: 0x000DEF76
		public bool IgnoreParentColorGroup
		{
			get
			{
				return this.m_IgnoreParentColorGroup;
			}
		}

		// Token: 0x06002A8C RID: 10892 RVA: 0x000E0B7E File Offset: 0x000DEF7E
		public void AddIgnoreGraphic(Graphic graphic)
		{
			this.m_IgnoreGraphicList.Add(graphic);
		}

		// Token: 0x06002A8D RID: 10893 RVA: 0x000E0B8C File Offset: 0x000DEF8C
		private void Prepare()
		{
			GraphForButton[] componentsInChildren = base.GetComponentsInChildren<GraphForButton>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].Prepare();
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x06002A8E RID: 10894 RVA: 0x000E0BC4 File Offset: 0x000DEFC4
		private void LateUpdate()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			if (this.m_IsDirty)
			{
				this.UpdateColor();
				this.m_IsDirty = false;
			}
		}

		// Token: 0x06002A8F RID: 10895 RVA: 0x000E0BF0 File Offset: 0x000DEFF0
		public void UpdateColor()
		{
			if (!this.m_IsChange)
			{
				return;
			}
			ColorGroup[] componentsInChildren = base.GetComponentsInChildren<ColorGroup>(true);
			for (int i = 1; i < componentsInChildren.Length; i++)
			{
				if (!(componentsInChildren[i].gameObject == base.gameObject))
				{
					componentsInChildren[i].UpdateColor();
					if (componentsInChildren[i].IgnoreParentColorGroup)
					{
						Graphic[] componentsInChildren2 = componentsInChildren[i].GetComponentsInChildren<Graphic>(true);
						for (int j = 0; j < componentsInChildren2.Length; j++)
						{
							if (!this.m_IgnoreGraphicList.Contains(componentsInChildren2[j]))
							{
								this.m_IgnoreGraphicList.Add(componentsInChildren2[j]);
							}
						}
					}
				}
			}
			Graphic[] componentsInChildren3 = base.GetComponentsInChildren<Graphic>(true);
			for (int k = 0; k < componentsInChildren3.Length; k++)
			{
				bool flag = false;
				for (int l = 0; l < this.m_IgnoreGraphicList.Count; l++)
				{
					if (this.m_IgnoreGraphicList[l] == componentsInChildren3[k])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					ColorGroup.ColorStatus colorStatus = null;
					for (int m = 0; m < this.m_ColorStatusList.Count; m++)
					{
						if (this.m_ColorStatusList[m].graph == componentsInChildren3[k])
						{
							colorStatus = this.m_ColorStatusList[m];
						}
					}
					if (colorStatus == null)
					{
						colorStatus = new ColorGroup.ColorStatus();
						colorStatus.graph = componentsInChildren3[k];
						colorStatus.m_DefaultColor = componentsInChildren3[k].color;
						colorStatus.material = componentsInChildren3[k].material;
						colorStatus.m_CurrentColor = componentsInChildren3[k].color;
						this.m_ColorStatusList.Add(colorStatus);
					}
					if ((Mathf.Abs(colorStatus.m_CurrentColor.r - componentsInChildren3[k].color.r) > 0.01f || Mathf.Abs(colorStatus.m_CurrentColor.g - componentsInChildren3[k].color.g) > 0.01f || Mathf.Abs(colorStatus.m_CurrentColor.b - componentsInChildren3[k].color.b) > 0.01f || Mathf.Abs(colorStatus.m_CurrentColor.a - componentsInChildren3[k].color.a) > 0.01f) && (Mathf.Abs(colorStatus.m_DefaultColor.r - componentsInChildren3[k].color.r) > 0.01f || Mathf.Abs(colorStatus.m_DefaultColor.g - componentsInChildren3[k].color.g) > 0.01f || Mathf.Abs(colorStatus.m_DefaultColor.b - componentsInChildren3[k].color.b) > 0.01f || Mathf.Abs(colorStatus.m_DefaultColor.a - componentsInChildren3[k].color.a) > 0.01f))
					{
						colorStatus.m_DefaultColor = componentsInChildren3[k].color;
					}
					if (this.m_ColorBlendMode == ColorGroup.eColorBlendMode.Mul || !(componentsInChildren3[k].GetComponent<Text>() != null))
					{
						Color color = colorStatus.m_DefaultColor;
						ColorGroup.eColorBlendMode colorBlendMode = this.m_ColorBlendMode;
						if (colorBlendMode != ColorGroup.eColorBlendMode.Normal)
						{
							if (colorBlendMode != ColorGroup.eColorBlendMode.Mul)
							{
								if (colorBlendMode == ColorGroup.eColorBlendMode.Set)
								{
									if (componentsInChildren3[k].GetComponent<Text>() != null)
									{
										goto IL_454;
									}
									Color black = Color.black;
									color = black + (this.m_Color - black) * this.m_Rate;
								}
							}
							else
							{
								Color color2 = color;
								color = color2 + (color2 * this.m_Color - color2) * this.m_Rate;
							}
						}
						else
						{
							color = color * (1f - this.m_Rate) + this.m_Color * this.m_Rate;
						}
						componentsInChildren3[k].color = color;
						if (this.m_MaterialChange)
						{
							componentsInChildren3[k].material = this.m_Material;
						}
					}
				}
				IL_454:;
			}
			GraphForButton[] componentsInChildren4 = base.GetComponentsInChildren<GraphForButton>(true);
			for (int n = 0; n < componentsInChildren4.Length; n++)
			{
				componentsInChildren4[n].ResetState();
			}
			this.SaveCurrentColor();
			this.m_IsDirty = false;
		}

		// Token: 0x06002A90 RID: 10896 RVA: 0x000E109C File Offset: 0x000DF49C
		public void SaveCurrentColor()
		{
			ColorGroup[] componentsInChildren = base.GetComponentsInChildren<ColorGroup>(true);
			for (int i = 1; i < componentsInChildren.Length; i++)
			{
				if (!(componentsInChildren[i].gameObject == base.gameObject))
				{
					componentsInChildren[i].SaveCurrentColor();
				}
			}
			for (int j = 0; j < this.m_ColorStatusList.Count; j++)
			{
				this.m_ColorStatusList[j].m_CurrentColor = this.m_ColorStatusList[j].graph.color;
			}
		}

		// Token: 0x06002A91 RID: 10897 RVA: 0x000E1130 File Offset: 0x000DF530
		public void ChangeColor(Color col, ColorGroup.eColorBlendMode blendMode = ColorGroup.eColorBlendMode.Mul, float rate = 1f)
		{
			this.m_Color = col;
			this.m_MaterialChange = false;
			this.m_Material = null;
			this.m_ColorBlendMode = blendMode;
			this.m_Rate = rate;
			this.m_IsChange = true;
			this.m_IsDirty = true;
			ColorGroup[] componentsInParent = base.GetComponentsInParent<ColorGroup>();
			for (int i = 0; i < componentsInParent.Length; i++)
			{
				componentsInParent[i].m_IsDirty = true;
			}
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
		}

		// Token: 0x06002A92 RID: 10898 RVA: 0x000E11A4 File Offset: 0x000DF5A4
		public void ChangeColor(Color col, Material mat, ColorGroup.eColorBlendMode blendMode = ColorGroup.eColorBlendMode.Mul, float rate = 1f)
		{
			this.m_Color = col;
			this.m_MaterialChange = true;
			this.m_Material = mat;
			this.m_ColorBlendMode = blendMode;
			this.m_Rate = rate;
			this.m_IsChange = true;
			this.m_IsDirty = true;
			ColorGroup[] componentsInParent = base.GetComponentsInParent<ColorGroup>();
			for (int i = 0; i < componentsInParent.Length; i++)
			{
				componentsInParent[i].m_IsDirty = true;
			}
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
		}

		// Token: 0x06002A93 RID: 10899 RVA: 0x000E1218 File Offset: 0x000DF618
		public void RevertColor()
		{
			this.m_IsChange = false;
			this.m_IsDirty = true;
			this.m_Color = Color.white;
			this.m_MaterialChange = false;
			this.m_Material = null;
			this.m_ColorBlendMode = ColorGroup.eColorBlendMode.Normal;
			this.m_Rate = 0f;
			if (this.m_ColorStatusList == null)
			{
				return;
			}
			for (int i = 0; i < this.m_ColorStatusList.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < this.m_IgnoreGraphicList.Count; j++)
				{
					if (this.m_IgnoreGraphicList[j] == this.m_ColorStatusList[i].graph)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					this.m_ColorStatusList[i].graph.color = this.m_ColorStatusList[i].m_DefaultColor;
					this.m_ColorStatusList[i].m_CurrentColor = this.m_ColorStatusList[i].m_DefaultColor;
					this.m_ColorStatusList[i].graph.material = this.m_ColorStatusList[i].material;
				}
			}
		}

		// Token: 0x040030EC RID: 12524
		[SerializeField]
		private bool m_IgnoreParentColorGroup;

		// Token: 0x040030ED RID: 12525
		private Color m_Color = Color.black;

		// Token: 0x040030EE RID: 12526
		private Material m_Material;

		// Token: 0x040030EF RID: 12527
		private bool m_MaterialChange;

		// Token: 0x040030F0 RID: 12528
		private float m_Rate;

		// Token: 0x040030F1 RID: 12529
		private ColorGroup.eColorBlendMode m_ColorBlendMode;

		// Token: 0x040030F2 RID: 12530
		private List<ColorGroup.ColorStatus> m_ColorStatusList = new List<ColorGroup.ColorStatus>();

		// Token: 0x040030F3 RID: 12531
		private List<Graphic> m_IgnoreGraphicList = new List<Graphic>();

		// Token: 0x040030F4 RID: 12532
		private bool m_IsChange;

		// Token: 0x040030F5 RID: 12533
		private bool m_IsDirty = true;

		// Token: 0x040030F6 RID: 12534
		private bool m_IsDonePrepare;

		// Token: 0x02000807 RID: 2055
		public enum eColorBlendMode
		{
			// Token: 0x040030F8 RID: 12536
			Normal,
			// Token: 0x040030F9 RID: 12537
			Mul,
			// Token: 0x040030FA RID: 12538
			Set
		}

		// Token: 0x02000808 RID: 2056
		private class ColorStatus
		{
			// Token: 0x040030FB RID: 12539
			public Graphic graph;

			// Token: 0x040030FC RID: 12540
			public Color m_CurrentColor;

			// Token: 0x040030FD RID: 12541
			public Color m_DefaultColor;

			// Token: 0x040030FE RID: 12542
			public Material material;
		}
	}
}
