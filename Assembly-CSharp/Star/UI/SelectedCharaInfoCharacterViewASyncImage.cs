﻿using System;

namespace Star.UI
{
	// Token: 0x020007BA RID: 1978
	public abstract class SelectedCharaInfoCharacterViewASyncImage : SelectedCharaInfoCharacterViewBase
	{
		// Token: 0x060028E1 RID: 10465 RVA: 0x000D96E8 File Offset: 0x000D7AE8
		public override void Setup()
		{
			base.Setup();
			this.m_ASyncImage = this.CreateASyncImageAdapter();
		}

		// Token: 0x060028E2 RID: 10466
		protected abstract ASyncImageAdapter CreateASyncImageAdapter();

		// Token: 0x060028E3 RID: 10467 RVA: 0x000D96FC File Offset: 0x000D7AFC
		public override void Destroy()
		{
			base.Destroy();
			ASyncImageAdapter asyncImageAdapter = this.GetASyncImageAdapter();
			if (asyncImageAdapter != null)
			{
				asyncImageAdapter.Destroy();
			}
		}

		// Token: 0x060028E4 RID: 10468 RVA: 0x000D9724 File Offset: 0x000D7B24
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			SelectedCharaInfoCharacterViewBase.eState state = base.GetState();
			if (state != SelectedCharaInfoCharacterViewBase.eState.Wait && state != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			this.m_AnimPlayer = null;
			ASyncImageAdapter asyncImageAdapter = this.GetASyncImageAdapter();
			if (asyncImageAdapter != null)
			{
				asyncImageAdapter.Apply(charaID, weaponID);
			}
			base.RequestLoad(charaID, weaponID, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028E5 RID: 10469 RVA: 0x000D976C File Offset: 0x000D7B6C
		public override void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepare && base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			if (this.m_AnimPlayer == null)
			{
				ASyncImageAdapter asyncImageAdapter = this.GetASyncImageAdapter();
				if (asyncImageAdapter != null)
				{
					this.m_AnimPlayer = asyncImageAdapter.GetAnimUIPlayer();
				}
			}
			if (this.m_AnimPlayer != null)
			{
				this.m_AnimPlayer.PlayIn();
			}
			base.PlayIn(toStateAutomaticallyAdvance);
		}

		// Token: 0x060028E6 RID: 10470 RVA: 0x000D97DF File Offset: 0x000D7BDF
		public override void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Idle)
			{
				return;
			}
			if (this.m_AnimPlayer != null)
			{
				this.m_AnimPlayer.PlayOut();
			}
			base.PlayOut(toStateAutomaticallyAdvance);
		}

		// Token: 0x060028E7 RID: 10471 RVA: 0x000D9811 File Offset: 0x000D7C11
		public override void FinishWait()
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Out)
			{
				return;
			}
			base.FinishWait();
		}

		// Token: 0x060028E8 RID: 10472 RVA: 0x000D9828 File Offset: 0x000D7C28
		public override bool IsPreparing()
		{
			ASyncImageAdapter asyncImageAdapter = this.GetASyncImageAdapter();
			return asyncImageAdapter != null && !asyncImageAdapter.IsDoneLoad();
		}

		// Token: 0x060028E9 RID: 10473 RVA: 0x000D984D File Offset: 0x000D7C4D
		public override bool IsAnimStateIn()
		{
			return base.GetState() == SelectedCharaInfoCharacterViewBase.eState.In && this.GetAnimState() == 1;
		}

		// Token: 0x060028EA RID: 10474 RVA: 0x000D9867 File Offset: 0x000D7C67
		public override bool IsAnimStateOut()
		{
			return base.GetState() == SelectedCharaInfoCharacterViewBase.eState.Out && this.GetAnimState() == 3;
		}

		// Token: 0x060028EB RID: 10475 RVA: 0x000D9881 File Offset: 0x000D7C81
		public int GetAnimState()
		{
			if (this.m_AnimPlayer != null)
			{
				return this.m_AnimPlayer.State;
			}
			return -1;
		}

		// Token: 0x060028EC RID: 10476 RVA: 0x000D98A1 File Offset: 0x000D7CA1
		protected ASyncImageAdapter GetASyncImageAdapter()
		{
			return this.m_ASyncImage;
		}

		// Token: 0x04002FAB RID: 12203
		protected ASyncImageAdapter m_ASyncImage;

		// Token: 0x04002FAC RID: 12204
		protected AnimUIPlayer m_AnimPlayer;
	}
}
