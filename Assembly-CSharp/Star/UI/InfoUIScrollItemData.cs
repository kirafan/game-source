﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009C2 RID: 2498
	public class InfoUIScrollItemData : ScrollItemData
	{
		// Token: 0x04003A1D RID: 14877
		public int m_ID;

		// Token: 0x04003A1E RID: 14878
		public string m_URL;

		// Token: 0x04003A1F RID: 14879
		public Sprite m_Sprite;

		// Token: 0x04003A20 RID: 14880
		public DateTime m_startTime;

		// Token: 0x04003A21 RID: 14881
		public DateTime m_endTime;

		// Token: 0x04003A22 RID: 14882
		public InfoUI m_InfoUI;
	}
}
