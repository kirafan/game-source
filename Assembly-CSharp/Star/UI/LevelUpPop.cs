﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000895 RID: 2197
	public class LevelUpPop : MonoBehaviour
	{
		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06002D9B RID: 11675 RVA: 0x000F0A5D File Offset: 0x000EEE5D
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002D9C RID: 11676 RVA: 0x000F0A82 File Offset: 0x000EEE82
		public void Play()
		{
			base.gameObject.SetActive(true);
			this.m_Arrow.PlayIn();
			this.m_Label.PlayIn();
			this.m_Step = LevelUpPop.eStep.PlayIn;
		}

		// Token: 0x06002D9D RID: 11677 RVA: 0x000F0AB0 File Offset: 0x000EEEB0
		private void Update()
		{
			if (this.m_Step == LevelUpPop.eStep.PlayIn && this.m_Label.State == 2)
			{
				this.m_Arrow.PlayOut();
				this.m_Label.PlayOut();
				this.m_Step = LevelUpPop.eStep.PlayOut;
			}
			if (this.m_Step == LevelUpPop.eStep.PlayOut && this.m_Label.State == 0)
			{
				this.m_Step = LevelUpPop.eStep.None;
			}
		}

		// Token: 0x040034A7 RID: 13479
		[SerializeField]
		private AnimUIPlayer m_Arrow;

		// Token: 0x040034A8 RID: 13480
		[SerializeField]
		private AnimUIPlayer m_Label;

		// Token: 0x040034A9 RID: 13481
		private RectTransform m_RectTransform;

		// Token: 0x040034AA RID: 13482
		private LevelUpPop.eStep m_Step;

		// Token: 0x02000896 RID: 2198
		private enum eStep
		{
			// Token: 0x040034AC RID: 13484
			None,
			// Token: 0x040034AD RID: 13485
			PlayIn,
			// Token: 0x040034AE RID: 13486
			PlayOut
		}
	}
}
