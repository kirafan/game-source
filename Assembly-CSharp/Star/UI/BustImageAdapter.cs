﻿using System;

namespace Star.UI
{
	// Token: 0x020007B8 RID: 1976
	public class BustImageAdapter : ASyncImageAdapterSyncImage
	{
		// Token: 0x060028D8 RID: 10456 RVA: 0x000D9BAF File Offset: 0x000D7FAF
		public BustImageAdapter(BustImage bustImage)
		{
			this.m_BustImage = bustImage;
		}

		// Token: 0x060028D9 RID: 10457 RVA: 0x000D9BBE File Offset: 0x000D7FBE
		public override void Apply(int charaID, int weaponID)
		{
			if (this.m_BustImage == null)
			{
				return;
			}
			this.m_BustImage.Apply(charaID, BustImage.eBustImageType.Chara);
		}

		// Token: 0x060028DA RID: 10458 RVA: 0x000D9BDF File Offset: 0x000D7FDF
		protected override ASyncImage GetASyncImage()
		{
			return this.m_BustImage;
		}

		// Token: 0x04002FA9 RID: 12201
		private BustImage m_BustImage;
	}
}
