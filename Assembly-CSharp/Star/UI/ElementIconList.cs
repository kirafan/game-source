﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000831 RID: 2097
	public class ElementIconList : MonoBehaviour
	{
		// Token: 0x06002BC4 RID: 11204 RVA: 0x000E6EC2 File Offset: 0x000E52C2
		public void ApplyNowSelectedQuest()
		{
			this.Apply(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest().m_QuestID);
		}

		// Token: 0x06002BC5 RID: 11205 RVA: 0x000E6EDE File Offset: 0x000E52DE
		public void Apply(int QuestId)
		{
			this.Apply(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEnemyTypes(QuestId));
		}

		// Token: 0x06002BC6 RID: 11206 RVA: 0x000E6EF8 File Offset: 0x000E52F8
		public void Apply(bool[] elementsFlags)
		{
			if (this.m_Icons != null)
			{
				for (int i = 0; i < this.m_Icons.Count; i++)
				{
					UnityEngine.Object.Destroy(this.m_Icons[i].gameObject);
				}
			}
			this.m_Icons = new List<ElementIcon>();
			List<eElementType> list = new List<eElementType>();
			if (elementsFlags != null)
			{
				for (int j = 0; j < 6; j++)
				{
					if (elementsFlags[j])
					{
						list.Add((eElementType)j);
					}
				}
			}
			if (list.Count <= 0)
			{
				ElementIcon elementIcon = UnityEngine.Object.Instantiate<ElementIcon>(this.m_Prefab, base.transform);
				elementIcon.Apply(eElementType.None);
				elementIcon.GetComponent<RectTransform>().sizeDelta = this.m_IconSize;
				this.m_Icons.Add(elementIcon);
			}
			else
			{
				while (list.Count > 0)
				{
					int num = -1;
					eElementType eElementType = eElementType.None;
					for (int k = 0; k < list.Count; k++)
					{
						int num2 = UIUtility.ElementEnumToSortIdx(list[k]);
						if (num == -1 || num2 < num)
						{
							num = num2;
							eElementType = list[k];
						}
					}
					list.Remove(eElementType);
					ElementIcon elementIcon2 = UnityEngine.Object.Instantiate<ElementIcon>(this.m_Prefab, base.transform);
					elementIcon2.Apply(eElementType);
					elementIcon2.GetComponent<RectTransform>().sizeDelta = this.m_IconSize;
					this.m_Icons.Add(elementIcon2);
				}
			}
		}

		// Token: 0x04003256 RID: 12886
		[SerializeField]
		[Tooltip("元になるPrefab")]
		private ElementIcon m_Prefab;

		// Token: 0x04003257 RID: 12887
		[SerializeField]
		private Vector2 m_IconSize;

		// Token: 0x04003258 RID: 12888
		private List<ElementIcon> m_Icons;
	}
}
