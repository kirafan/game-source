﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000966 RID: 2406
	public abstract class EnumerationPanelCoreBase
	{
		// Token: 0x060031C9 RID: 12745 RVA: 0x000FD5AC File Offset: 0x000FB9AC
		public static bool ModeToTouchActive(EnumerationPanelCoreBase.eMode mode)
		{
			return true;
		}

		// Token: 0x060031CA RID: 12746 RVA: 0x000FD5AF File Offset: 0x000FB9AF
		public static EnumerationCharaPanelBase.eMode ConvertEnumerationPanelCoreBaseeModeToEnumerationCharaPanelBaseeMode(EnumerationPanelCoreBase.eMode mode)
		{
			switch (mode)
			{
			case EnumerationPanelCoreBase.eMode.Edit:
				return EnumerationCharaPanelBase.eMode.Edit;
			case EnumerationPanelCoreBase.eMode.Quest:
				return EnumerationCharaPanelBase.eMode.Quest;
			case EnumerationPanelCoreBase.eMode.QuestStart:
				return EnumerationCharaPanelBase.eMode.QuestStart;
			case EnumerationPanelCoreBase.eMode.View:
				return EnumerationCharaPanelBase.eMode.View;
			case EnumerationPanelCoreBase.eMode.PartyDetail:
				return EnumerationCharaPanelBase.eMode.PartyDetail;
			default:
				return EnumerationCharaPanelBase.eMode.Edit;
			}
		}

		// Token: 0x02000967 RID: 2407
		public enum eMode
		{
			// Token: 0x04003819 RID: 14361
			Edit,
			// Token: 0x0400381A RID: 14362
			Quest,
			// Token: 0x0400381B RID: 14363
			QuestStart,
			// Token: 0x0400381C RID: 14364
			View,
			// Token: 0x0400381D RID: 14365
			PartyDetail
		}

		// Token: 0x02000968 RID: 2408
		[Serializable]
		public class SharedInstanceBase
		{
			// Token: 0x060031CB RID: 12747 RVA: 0x000FD5DB File Offset: 0x000FB9DB
			public SharedInstanceBase()
			{
			}

			// Token: 0x060031CC RID: 12748 RVA: 0x000FD5E3 File Offset: 0x000FB9E3
			public SharedInstanceBase(EnumerationPanelCoreBase.SharedInstanceBase argument)
			{
				this.CloneNewMemberOnlyBase(argument);
			}

			// Token: 0x060031CD RID: 12749 RVA: 0x000FD5F3 File Offset: 0x000FB9F3
			protected EnumerationPanelCoreBase.SharedInstanceBase CloneNewMemberOnlyBase(EnumerationPanelCoreBase.SharedInstanceBase argument)
			{
				this.m_CharaPanelParent = argument.m_CharaPanelParent;
				return this;
			}

			// Token: 0x0400381E RID: 14366
			[SerializeField]
			[Tooltip("生成したCloneの親.")]
			public RectTransform m_CharaPanelParent;
		}

		// Token: 0x02000969 RID: 2409
		[Serializable]
		public abstract class SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> : EnumerationPanelCoreBase.SharedInstanceBase where ThisType : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ParentType : MonoBehaviour where CharaPanelType : MonoBehaviour
		{
			// Token: 0x060031CE RID: 12750 RVA: 0x000FD602 File Offset: 0x000FBA02
			public SharedInstanceBaseExGen()
			{
			}

			// Token: 0x060031CF RID: 12751 RVA: 0x000FD60A File Offset: 0x000FBA0A
			public SharedInstanceBaseExGen(ThisType argument) : base(argument)
			{
				this.CloneNewMemberOnly(argument);
			}

			// Token: 0x060031D0 RID: 12752 RVA: 0x000FD620 File Offset: 0x000FBA20
			public virtual ThisType Clone(ThisType argument)
			{
				base.CloneNewMemberOnlyBase(argument);
				return this.CloneNewMemberOnly(argument);
			}

			// Token: 0x060031D1 RID: 12753 RVA: 0x000FD636 File Offset: 0x000FBA36
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				this.m_CharaPanelPrefab = argument.m_CharaPanelPrefab;
				return (ThisType)((object)this);
			}

			// Token: 0x0400381F RID: 14367
			public ParentType parent;

			// Token: 0x04003820 RID: 14368
			[SerializeField]
			[Tooltip("パネル内キャラクタ情報部の元Prefabリソース.")]
			public CharaPanelType m_CharaPanelPrefab;
		}
	}
}
