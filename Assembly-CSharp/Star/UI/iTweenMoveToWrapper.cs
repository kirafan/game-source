﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008CD RID: 2253
	public class iTweenMoveToWrapper : iTweenWrapper
	{
		// Token: 0x06002EB3 RID: 11955 RVA: 0x000F4CDB File Offset: 0x000F30DB
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
			iTween.MoveTo(target, args);
		}

		// Token: 0x06002EB4 RID: 11956 RVA: 0x000F4CE4 File Offset: 0x000F30E4
		public override void Stop()
		{
			if (base.IsPlaying())
			{
				iTween.Stop(this.m_Target, "move");
				base.Stop();
			}
		}
	}
}
