﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007EE RID: 2030
	[RequireComponent(typeof(CanvasGroup))]
	public class AlphaAnimUI : AnimUIBase
	{
		// Token: 0x060029E9 RID: 10729 RVA: 0x000DD2BC File Offset: 0x000DB6BC
		protected override void Prepare()
		{
			base.Prepare();
			this.m_GameObject = base.gameObject;
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			this.m_CanvasGroup.alpha = this.m_HideAlpha;
			if (this.m_ReturnMode)
			{
				this.m_EndAlpha = this.m_HideAlpha;
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x060029EA RID: 10730 RVA: 0x000DD318 File Offset: 0x000DB718
		protected override void ExecutePlayIn()
		{
			if (this.m_Restart)
			{
				this.m_CanvasGroup.alpha = this.m_HideAlpha;
			}
			iTween.Stop(this.m_GameObject, "value");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", this.m_CanvasGroup.alpha);
			hashtable.Add("to", this.m_ShowAlpha);
			hashtable.Add("time", num);
			hashtable.Add("delay", 0.001f + this.m_Delay);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "OnCompleteInAlpha");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x060029EB RID: 10731 RVA: 0x000DD438 File Offset: 0x000DB838
		protected override void ExecutePlayOut()
		{
			if (this.m_Restart)
			{
				this.m_CanvasGroup.alpha = this.m_ShowAlpha;
			}
			iTween.Stop(this.m_GameObject, "value");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", this.m_CanvasGroup.alpha);
			hashtable.Add("to", this.m_EndAlpha);
			hashtable.Add("time", num);
			hashtable.Add("delay", 0.001f + this.m_Delay);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "OnCompleteOutAlpha");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x060029EC RID: 10732 RVA: 0x000DD556 File Offset: 0x000DB956
		public override void Hide()
		{
			base.Hide();
			iTween.Stop(this.m_GameObject, "value");
			this.m_CanvasGroup.alpha = this.m_HideAlpha;
		}

		// Token: 0x060029ED RID: 10733 RVA: 0x000DD57F File Offset: 0x000DB97F
		public override void Show()
		{
			base.Show();
			iTween.Stop(this.m_GameObject, "value");
			this.m_CanvasGroup.alpha = this.m_ShowAlpha;
		}

		// Token: 0x060029EE RID: 10734 RVA: 0x000DD5A8 File Offset: 0x000DB9A8
		public void OnUpdateAlpha(float value)
		{
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x060029EF RID: 10735 RVA: 0x000DD5B6 File Offset: 0x000DB9B6
		public void OnCompleteInAlpha()
		{
			this.m_CanvasGroup.alpha = this.m_ShowAlpha;
			this.m_IsPlaying = false;
			this.OnAnimCompleteIn.Call();
		}

		// Token: 0x060029F0 RID: 10736 RVA: 0x000DD5DB File Offset: 0x000DB9DB
		public void OnCompleteOutAlpha()
		{
			this.m_CanvasGroup.alpha = this.m_EndAlpha;
			this.m_IsPlaying = false;
			this.OnAnimCompleteOut.Call();
		}

		// Token: 0x04003048 RID: 12360
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x04003049 RID: 12361
		[SerializeField]
		private bool m_ReturnMode = true;

		// Token: 0x0400304A RID: 12362
		[SerializeField]
		private bool m_Restart;

		// Token: 0x0400304B RID: 12363
		[SerializeField]
		private float m_HideAlpha;

		// Token: 0x0400304C RID: 12364
		[SerializeField]
		private float m_ShowAlpha = 1f;

		// Token: 0x0400304D RID: 12365
		[SerializeField]
		private float m_EndAlpha;

		// Token: 0x0400304E RID: 12366
		[SerializeField]
		private float m_AnimDuration = -1f;

		// Token: 0x0400304F RID: 12367
		[SerializeField]
		private float m_Delay;

		// Token: 0x04003050 RID: 12368
		[SerializeField]
		private iTween.EaseType m_EaseType = iTween.EaseType.easeOutQuad;

		// Token: 0x04003051 RID: 12369
		private GameObject m_GameObject;

		// Token: 0x04003052 RID: 12370
		private CanvasGroup m_CanvasGroup;
	}
}
