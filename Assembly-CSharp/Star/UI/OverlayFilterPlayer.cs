﻿using System;

namespace Star.UI
{
	// Token: 0x02000A31 RID: 2609
	public class OverlayFilterPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06003650 RID: 13904 RVA: 0x00112668 File Offset: 0x00110A68
		public override void Open(OverlayUIArgBase arg, Action OnClose, Action OnEnd)
		{
			base.Open(arg, OnClose, OnEnd);
			FilterUIArg filterUIArg = (FilterUIArg)arg;
			UISettings.eUsingType_Filter usingType = filterUIArg.m_UsingType;
			if (usingType == UISettings.eUsingType_Filter.CharaListView || usingType == UISettings.eUsingType_Filter.SupportTitle || usingType == UISettings.eUsingType_Filter.Recommend)
			{
				this.m_SceneID = SceneDefine.eChildSceneID.ScrollFilterUI;
			}
		}

		// Token: 0x06003651 RID: 13905 RVA: 0x001126B2 File Offset: 0x00110AB2
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return this.m_SceneID;
		}

		// Token: 0x06003652 RID: 13906 RVA: 0x001126BC File Offset: 0x00110ABC
		protected override void Setup(OverlayUIArgBase arg)
		{
			this.m_FilterUI = this.GetUI<FilterUI>();
			FilterUIArg filterUIArg = (FilterUIArg)arg;
			this.m_FilterUI.Setup(filterUIArg.m_UsingType, filterUIArg.m_OnFilterExecute);
			FilterUI filterUI = this.m_FilterUI;
			filterUI.OnStartOut = (Action)Delegate.Combine(filterUI.OnStartOut, new Action(this.GoToMenuEnd));
		}

		// Token: 0x06003653 RID: 13907 RVA: 0x0011271B File Offset: 0x00110B1B
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
			base.OnClickBackButtonCallBack(isShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x04003CA7 RID: 15527
		private SceneDefine.eChildSceneID m_SceneID = SceneDefine.eChildSceneID.FilterUI;

		// Token: 0x04003CA8 RID: 15528
		protected FilterUI m_FilterUI;
	}
}
