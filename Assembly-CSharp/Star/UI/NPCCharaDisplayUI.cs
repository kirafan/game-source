﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000A28 RID: 2600
	public class NPCCharaDisplayUI : MenuUIBase
	{
		// Token: 0x060035CD RID: 13773 RVA: 0x00110AA6 File Offset: 0x0010EEA6
		public void SetCompletePlayInCallBack(Action action)
		{
			this.m_OnCompletePlayIn = action;
		}

		// Token: 0x060035CE RID: 13774 RVA: 0x00110AAF File Offset: 0x0010EEAF
		public void PlayInVoice(eSoundVoiceControllListDB voiceCtrl)
		{
			this.m_IsReserveVocieCtrl = true;
			this.m_VoiceCtrl = voiceCtrl;
		}

		// Token: 0x060035CF RID: 13775 RVA: 0x00110ABF File Offset: 0x0010EEBF
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
		}

		// Token: 0x060035D0 RID: 13776 RVA: 0x00110AEB File Offset: 0x0010EEEB
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrder, int offset)
		{
			base.GetComponent<FixSortOrder>().SetSortOrder(sortOrder, offset);
		}

		// Token: 0x060035D1 RID: 13777 RVA: 0x00110AFC File Offset: 0x0010EEFC
		private void Update()
		{
			switch (this.m_Step)
			{
			case NPCCharaDisplayUI.eStep.In:
				if (this.m_NPCIllust.IsDoneLoad())
				{
					AnimUIPlayer component = this.m_NPCIllust.GetComponent<AnimUIPlayer>();
					if (component.State == 0 || component.State == 3)
					{
						component.PlayIn();
						if (this.m_IsReserveVocieCtrl)
						{
							if (this.m_VoiceReq != -1)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceReq);
							}
							this.m_VoiceReq = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(this.m_VoiceCtrl, true);
							this.m_IsReserveVocieCtrl = false;
						}
					}
					if (component.State == 2)
					{
						this.m_OnCompletePlayIn.Call();
						this.ChangeStep(NPCCharaDisplayUI.eStep.Idle);
					}
				}
				break;
			case NPCCharaDisplayUI.eStep.Out:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 3)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					if (this.m_VoiceReq != -1)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceReq);
					}
					this.m_IsReserveVocieCtrl = false;
					this.ChangeStep(NPCCharaDisplayUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060035D2 RID: 13778 RVA: 0x00110C58 File Offset: 0x0010F058
		private void ChangeStep(NPCCharaDisplayUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case NPCCharaDisplayUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case NPCCharaDisplayUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case NPCCharaDisplayUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case NPCCharaDisplayUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case NPCCharaDisplayUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060035D3 RID: 13779 RVA: 0x00110D00 File Offset: 0x0010F100
		public override void PlayIn()
		{
			this.ChangeStep(NPCCharaDisplayUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x060035D4 RID: 13780 RVA: 0x00110D3C File Offset: 0x0010F13C
		public void PlayInNpc(NpcIllust.eNpc npc)
		{
			if (this.m_Current == npc)
			{
				return;
			}
			this.m_Current = npc;
			this.m_NPCIllust.Destroy();
			this.m_NPCIllust.GetComponent<AnimUIPlayer>().Hide();
			this.m_NPCIllust.Apply(npc);
			this.m_NPCIllust.GetComponent<RectTransform>().localScale = Vector3.one;
			if (this.POSITIONS[(int)npc].x != 0f)
			{
				this.m_OffsetObj.anchoredPosition = this.GetFixedPosition(npc);
			}
			this.PlayIn();
		}

		// Token: 0x060035D5 RID: 13781 RVA: 0x00110DCC File Offset: 0x0010F1CC
		public Vector2 GetFixedPosition(NpcIllust.eNpc npc)
		{
			return this.POSITIONS[(int)npc];
		}

		// Token: 0x060035D6 RID: 13782 RVA: 0x00110DE0 File Offset: 0x0010F1E0
		public override void PlayOut()
		{
			this.m_Current = NpcIllust.eNpc.None;
			this.ChangeStep(NPCCharaDisplayUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_NPCIllust.GetComponent<AnimUIPlayer>().PlayOut();
		}

		// Token: 0x060035D7 RID: 13783 RVA: 0x00110E31 File Offset: 0x0010F231
		public override bool IsMenuEnd()
		{
			return this.m_Step == NPCCharaDisplayUI.eStep.End;
		}

		// Token: 0x060035D8 RID: 13784 RVA: 0x00110E3C File Offset: 0x0010F23C
		public bool IsIdle()
		{
			return this.m_Step == NPCCharaDisplayUI.eStep.Idle;
		}

		// Token: 0x04003C67 RID: 15463
		private readonly Vector2[] POSITIONS = new Vector2[]
		{
			new Vector2(0f, 0f),
			new Vector2(0f, 0f),
			new Vector2(262f, -583f),
			new Vector2(259f, -897f),
			new Vector2(236f, -600f),
			new Vector2(201f, -752f),
			new Vector2(306f, -651f),
			new Vector2(0f, 0f)
		};

		// Token: 0x04003C68 RID: 15464
		private NPCCharaDisplayUI.eStep m_Step;

		// Token: 0x04003C69 RID: 15465
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003C6A RID: 15466
		[SerializeField]
		private RectTransform m_OffsetObj;

		// Token: 0x04003C6B RID: 15467
		[SerializeField]
		private NpcIllust m_NPCIllust;

		// Token: 0x04003C6C RID: 15468
		private NpcIllust.eNpc m_Current;

		// Token: 0x04003C6D RID: 15469
		private Action m_OnCompletePlayIn;

		// Token: 0x04003C6E RID: 15470
		private bool m_IsReserveVocieCtrl;

		// Token: 0x04003C6F RID: 15471
		private eSoundVoiceControllListDB m_VoiceCtrl;

		// Token: 0x04003C70 RID: 15472
		private int m_VoiceReq = -1;

		// Token: 0x02000A29 RID: 2601
		private enum eStep
		{
			// Token: 0x04003C72 RID: 15474
			Hide,
			// Token: 0x04003C73 RID: 15475
			In,
			// Token: 0x04003C74 RID: 15476
			Idle,
			// Token: 0x04003C75 RID: 15477
			Out,
			// Token: 0x04003C76 RID: 15478
			End
		}
	}
}
