﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000A35 RID: 2613
	public abstract class OverlayUIPlayerBase
	{
		// Token: 0x0600365B RID: 13915 RVA: 0x001123D0 File Offset: 0x001107D0
		public virtual bool IsOpen()
		{
			return this.m_OverlayStep != OverlayUIPlayerBase.eLoadStep.None;
		}

		// Token: 0x0600365C RID: 13916 RVA: 0x001123DE File Offset: 0x001107DE
		public bool IsDoneLoad()
		{
			return this.m_OverlayStep == OverlayUIPlayerBase.eLoadStep.Loaded;
		}

		// Token: 0x0600365D RID: 13917
		public abstract SceneDefine.eChildSceneID GetUISceneID();

		// Token: 0x0600365E RID: 13918 RVA: 0x001123E9 File Offset: 0x001107E9
		public virtual T GetUI<T>() where T : MenuUIBase
		{
			return (T)((object)this.m_UI);
		}

		// Token: 0x0600365F RID: 13919 RVA: 0x001123F8 File Offset: 0x001107F8
		public virtual void Open(OverlayUIArgBase arg, Action OnClose, Action OnEnd)
		{
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButtonCallBack));
			this.m_Arg = arg;
			this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.Load;
			this.m_OnClose = OnClose;
			this.m_OnEnd = OnEnd;
		}

		// Token: 0x06003660 RID: 13920 RVA: 0x00112452 File Offset: 0x00110852
		public void Unload()
		{
			SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.GetUISceneID());
			this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.Unload;
		}

		// Token: 0x06003661 RID: 13921 RVA: 0x0011246B File Offset: 0x0011086B
		protected virtual void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
		}

		// Token: 0x06003662 RID: 13922 RVA: 0x00112484 File Offset: 0x00110884
		public virtual void UpdateStep()
		{
			switch (this.m_OverlayStep)
			{
			case OverlayUIPlayerBase.eLoadStep.Load:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.GetUISceneID(), true);
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.NowLoadingOnly, eTipsCategory.None);
				this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.LoadWait;
				break;
			case OverlayUIPlayerBase.eLoadStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.GetUISceneID()))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.GetUISceneID());
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.Setup;
				}
				break;
			case OverlayUIPlayerBase.eLoadStep.Setup:
			{
				bool flag = this.SetupAndPlayIn();
				if (flag)
				{
					this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.Loaded;
				}
				break;
			}
			case OverlayUIPlayerBase.eLoadStep.Loaded:
				if (this.m_UI.IsMenuEnd())
				{
					this.Unload();
				}
				break;
			case OverlayUIPlayerBase.eLoadStep.Unload:
				SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.GetUISceneID());
				this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.UnloadWait;
				break;
			case OverlayUIPlayerBase.eLoadStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.GetUISceneID()))
				{
					this.Destroy();
					this.m_OnEnd.Call();
					this.m_OverlayStep = OverlayUIPlayerBase.eLoadStep.None;
				}
				break;
			}
		}

		// Token: 0x06003663 RID: 13923 RVA: 0x001125B4 File Offset: 0x001109B4
		protected virtual bool SetupAndPlayIn()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuUIBase>(SceneDefine.CHILD_SCENE_INFOS[this.GetUISceneID()].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.GetComponent<Canvas>().planeDistance = 95f;
			this.Setup(this.m_Arg);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06003664 RID: 13924
		protected abstract void Setup(OverlayUIArgBase arg);

		// Token: 0x06003665 RID: 13925 RVA: 0x00112624 File Offset: 0x00110A24
		protected virtual void OnClickBackButtonCallBack(bool isShortCut)
		{
			this.GoToMenuEnd();
		}

		// Token: 0x06003666 RID: 13926 RVA: 0x0011262C File Offset: 0x00110A2C
		protected virtual void GoToMenuEnd()
		{
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
				this.m_OnClose.Call();
			}
		}

		// Token: 0x04003CAC RID: 15532
		protected OverlayUIPlayerBase.eLoadStep m_OverlayStep;

		// Token: 0x04003CAD RID: 15533
		protected MenuUIBase m_UI;

		// Token: 0x04003CAE RID: 15534
		protected OverlayUIArgBase m_Arg;

		// Token: 0x04003CAF RID: 15535
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04003CB0 RID: 15536
		private Action m_OnClose;

		// Token: 0x04003CB1 RID: 15537
		private Action m_OnEnd;

		// Token: 0x02000A36 RID: 2614
		public enum eLoadStep
		{
			// Token: 0x04003CB3 RID: 15539
			None,
			// Token: 0x04003CB4 RID: 15540
			Load,
			// Token: 0x04003CB5 RID: 15541
			LoadWait,
			// Token: 0x04003CB6 RID: 15542
			Setup,
			// Token: 0x04003CB7 RID: 15543
			Loaded,
			// Token: 0x04003CB8 RID: 15544
			Unload,
			// Token: 0x04003CB9 RID: 15545
			UnloadWait
		}
	}
}
