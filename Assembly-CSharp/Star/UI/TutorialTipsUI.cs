﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B15 RID: 2837
	public class TutorialTipsUI : MonoBehaviour
	{
		// Token: 0x06003B9D RID: 15261 RVA: 0x0013062D File Offset: 0x0012EA2D
		public bool IsOpen()
		{
			return this.m_Step != TutorialTipsUI.eStep.None;
		}

		// Token: 0x06003B9E RID: 15262 RVA: 0x0013063C File Offset: 0x0012EA3C
		private void Start()
		{
			this.m_Scroll.Setup();
			this.m_TipsID = eTutorialTipsListDB.Max;
			this.m_TipsImageSpriteHndls = new List<SpriteHandler>();
			this.m_ScrollItemData = new List<TutorialTipsUIItemData>();
			this.m_CloseButton.AddListerner(new UnityAction(this.OnClickClose));
			this.m_Destroyed = false;
			this.m_Preparing = false;
			this.m_Step = TutorialTipsUI.eStep.None;
		}

		// Token: 0x06003B9F RID: 15263 RVA: 0x001306A4 File Offset: 0x0012EAA4
		private void Initialize()
		{
			this.m_Scroll.RemoveAll();
			this.m_Scroll.InitializeMainImage();
			if (this.m_ScrollItemData != null)
			{
				for (int i = 0; i < this.m_ScrollItemData.Count; i++)
				{
					this.m_ScrollItemData[i].Destroy();
				}
				this.m_ScrollItemData.Clear();
			}
			this.DestroySpriteHndls();
		}

		// Token: 0x06003BA0 RID: 15264 RVA: 0x00130710 File Offset: 0x0012EB10
		public void OnDestroy()
		{
			if (!this.m_Destroyed)
			{
				this.Destroy();
			}
		}

		// Token: 0x06003BA1 RID: 15265 RVA: 0x00130723 File Offset: 0x0012EB23
		public void Destroy()
		{
			this.Initialize();
			this.m_Scroll.Destroy();
			this.m_Destroyed = true;
		}

		// Token: 0x06003BA2 RID: 15266 RVA: 0x0013073D File Offset: 0x0012EB3D
		public void Abort()
		{
			this.m_Group.Close();
			this.m_Step = TutorialTipsUI.eStep.Out;
		}

		// Token: 0x06003BA3 RID: 15267 RVA: 0x00130751 File Offset: 0x0012EB51
		public void Open(eTutorialTipsListDB tutorialTipsID)
		{
			this.m_Mode = TutorialTipsUI.eMode.Tutorial;
			this.m_TipsID = tutorialTipsID;
			this.Prepare(tutorialTipsID);
			this.Open();
		}

		// Token: 0x06003BA4 RID: 15268 RVA: 0x0013076E File Offset: 0x0012EB6E
		public void Open(eRetireTipsListDB retireTipsID)
		{
			this.m_Mode = TutorialTipsUI.eMode.Retire;
			this.m_RetireTipsID = retireTipsID;
			this.Prepare(retireTipsID);
			this.Open();
		}

		// Token: 0x06003BA5 RID: 15269 RVA: 0x0013078C File Offset: 0x0012EB8C
		private void Open()
		{
			if (this.m_CloseButton != null)
			{
				this.m_CloseButton.gameObject.SetActive(false);
			}
			this.m_DestTitleText.text = string.Empty;
			this.m_Scroll.SetSelectIdx(0, false);
			this.m_Group.Open();
			this.m_Step = TutorialTipsUI.eStep.In;
		}

		// Token: 0x06003BA6 RID: 15270 RVA: 0x001307EA File Offset: 0x0012EBEA
		public void Close()
		{
			this.m_Group.Close();
			this.m_OnClickCloseCallback.Call();
			this.m_Step = TutorialTipsUI.eStep.Out;
		}

		// Token: 0x06003BA7 RID: 15271 RVA: 0x0013080C File Offset: 0x0012EC0C
		private void Update()
		{
			this.UpdatePrepare();
			TutorialTipsUI.eStep step = this.m_Step;
			switch (step + 1)
			{
			case TutorialTipsUI.eStep.Idle:
				this.UpdateIn();
				break;
			case TutorialTipsUI.eStep.Out:
				this.UpdateIdle();
				break;
			case (TutorialTipsUI.eStep)3:
				this.UpdateOut();
				break;
			}
		}

		// Token: 0x06003BA8 RID: 15272 RVA: 0x00130869 File Offset: 0x0012EC69
		private void UpdateIn()
		{
			if (!this.m_Group.IsDonePlayIn)
			{
				return;
			}
			this.m_OnOpendCallback.Call();
			this.m_OnOpendCallback = null;
			this.m_Step = TutorialTipsUI.eStep.Idle;
		}

		// Token: 0x06003BA9 RID: 15273 RVA: 0x00130898 File Offset: 0x0012EC98
		private void UpdateIdle()
		{
			if (!this.m_Preparing)
			{
				int nowSelectDataIdx = this.m_Scroll.GetNowSelectDataIdx();
				if (nowSelectDataIdx >= 0 && nowSelectDataIdx < this.m_ScrollItemData.Count && this.m_CurrentIdx != this.m_Scroll.GetNowSelectDataIdx())
				{
					for (int i = nowSelectDataIdx; i >= 0; i--)
					{
						if (!string.IsNullOrEmpty(this.m_ScrollItemData[i].m_TitleText))
						{
							this.m_DestTitleText.text = this.m_ScrollItemData[i].m_TitleText;
							break;
						}
					}
					this.m_CurrentIdx = this.m_Scroll.GetNowSelectDataIdx();
				}
			}
			this.UpdateCloseButtonActive();
		}

		// Token: 0x06003BAA RID: 15274 RVA: 0x00130950 File Offset: 0x0012ED50
		private void UpdateCloseButtonActive()
		{
			if (this.m_CloseButton != null)
			{
				bool active = true;
				if (this.m_Mode == TutorialTipsUI.eMode.Tutorial && this.m_TTLDBP.m_IsCloseAfterViewAllPages == 1 && !this.m_Scroll.IsEndPage())
				{
					active = false;
				}
				this.m_CloseButton.gameObject.SetActive(active);
			}
		}

		// Token: 0x06003BAB RID: 15275 RVA: 0x001309AF File Offset: 0x0012EDAF
		private void UpdateOut()
		{
			if (!this.m_Group.IsDonePlayOut)
			{
				return;
			}
			this.DestroySpriteHndls();
			this.m_Step = TutorialTipsUI.eStep.None;
		}

		// Token: 0x06003BAC RID: 15276 RVA: 0x001309D0 File Offset: 0x0012EDD0
		private void Prepare(eTutorialTipsListDB tutorialTipsID)
		{
			this.Initialize();
			this.m_TipsID = tutorialTipsID;
			this.m_TTLDBP = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TutorialTipsListDB.GetParam(tutorialTipsID);
			SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
			for (int i = 0; i < this.m_TTLDBP.m_Datas.Length; i++)
			{
				SpriteHandler spriteHandler = spriteMng.LoadAsyncTutorialTips(this.m_TipsID, this.m_TTLDBP.m_Datas[i].m_ImageID);
				if (spriteHandler != null)
				{
					this.m_TipsImageSpriteHndls.Add(spriteHandler);
				}
			}
			for (int j = 0; j < this.m_TTLDBP.m_Datas.Length; j++)
			{
				TutorialTipsUIItemData tutorialTipsUIItemData = new TutorialTipsUIItemData(this.m_DestTitleText, this.m_TTLDBP.m_Datas[j].m_Title, null, this.m_TTLDBP.m_Datas[j].m_Text);
				this.m_Scroll.AddItem(tutorialTipsUIItemData);
				this.m_ScrollItemData.Add(tutorialTipsUIItemData);
			}
			this.m_Preparing = true;
		}

		// Token: 0x06003BAD RID: 15277 RVA: 0x00130ADC File Offset: 0x0012EEDC
		private void Prepare(eRetireTipsListDB retireTipsID)
		{
			this.Initialize();
			this.m_RetireTipsID = retireTipsID;
			this.m_RetireDBParam = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RetireTipsListDB.GetParam(retireTipsID);
			SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
			for (int i = 0; i < this.m_RetireDBParam.m_Datas.Length; i++)
			{
				SpriteHandler spriteHandler = spriteMng.LoadAsyncRetireTips(this.m_RetireTipsID, this.m_RetireDBParam.m_Datas[i].m_ImageID);
				if (spriteHandler != null)
				{
					this.m_TipsImageSpriteHndls.Add(spriteHandler);
				}
			}
			for (int j = 0; j < this.m_RetireDBParam.m_Datas.Length; j++)
			{
				TutorialTipsUIItemData tutorialTipsUIItemData = new TutorialTipsUIItemData(this.m_DestTitleText, this.m_RetireDBParam.m_Datas[j].m_Title, null, this.m_RetireDBParam.m_Datas[j].m_Text);
				this.m_Scroll.AddItem(tutorialTipsUIItemData);
				this.m_ScrollItemData.Add(tutorialTipsUIItemData);
			}
			this.m_Preparing = true;
		}

		// Token: 0x06003BAE RID: 15278 RVA: 0x00130BE8 File Offset: 0x0012EFE8
		private void UpdatePrepare()
		{
			if (!this.m_Preparing)
			{
				return;
			}
			if (!this.IsSpriteHandlerAvailable())
			{
				return;
			}
			this.m_Preparing = false;
			for (int i = 0; i < this.m_ScrollItemData.Count; i++)
			{
				this.m_ScrollItemData[i].m_ImageSprite = this.m_TipsImageSpriteHndls[i].Obj;
			}
			this.m_Scroll.ForceApply();
			this.m_CurrentIdx = -1;
			this.m_Scroll.SetSelectIdx(0, false);
			this.UpdateCloseButtonActive();
		}

		// Token: 0x06003BAF RID: 15279 RVA: 0x00130C78 File Offset: 0x0012F078
		private void DestroySpriteHndls()
		{
			if (this.m_TipsImageSpriteHndls != null)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng != null)
				{
					for (int i = 0; i < this.m_TipsImageSpriteHndls.Count; i++)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_TipsImageSpriteHndls[i]);
						this.m_TipsImageSpriteHndls[i] = null;
					}
				}
				this.m_TipsImageSpriteHndls.Clear();
			}
		}

		// Token: 0x06003BB0 RID: 15280 RVA: 0x00130D00 File Offset: 0x0012F100
		private bool IsSpriteHandlerAvailable()
		{
			for (int i = 0; i < this.m_TipsImageSpriteHndls.Count; i++)
			{
				if (this.m_TipsImageSpriteHndls[i] == null)
				{
					return false;
				}
				if (!this.m_TipsImageSpriteHndls[i].IsAvailable())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003BB1 RID: 15281 RVA: 0x00130D55 File Offset: 0x0012F155
		private bool IsOpenTips()
		{
			return this.m_Group.State != UIGroup.eState.Hide && this.m_Group.State != UIGroup.eState.Prepare && this.m_Group.State != UIGroup.eState.WaitFinalize;
		}

		// Token: 0x06003BB2 RID: 15282 RVA: 0x00130D8C File Offset: 0x0012F18C
		public void SetOnOpendCallback(Action callback)
		{
			this.m_OnOpendCallback = callback;
		}

		// Token: 0x06003BB3 RID: 15283 RVA: 0x00130D95 File Offset: 0x0012F195
		public void SetOnClickCloseCallback(Action callback)
		{
			this.m_OnClickCloseCallback = callback;
		}

		// Token: 0x06003BB4 RID: 15284 RVA: 0x00130D9E File Offset: 0x0012F19E
		public void OnClickClose()
		{
			this.Close();
		}

		// Token: 0x04004334 RID: 17204
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x04004335 RID: 17205
		[SerializeField]
		private TutorialTipsScroll m_Scroll;

		// Token: 0x04004336 RID: 17206
		[SerializeField]
		private Text m_DestTitleText;

		// Token: 0x04004337 RID: 17207
		private TutorialTipsUI.eMode m_Mode;

		// Token: 0x04004338 RID: 17208
		private eTutorialTipsListDB m_TipsID;

		// Token: 0x04004339 RID: 17209
		private TutorialTipsListDB_Param m_TTLDBP;

		// Token: 0x0400433A RID: 17210
		private RetireTipsListDB_Param m_RetireDBParam;

		// Token: 0x0400433B RID: 17211
		private eRetireTipsListDB m_RetireTipsID;

		// Token: 0x0400433C RID: 17212
		private List<SpriteHandler> m_TipsImageSpriteHndls;

		// Token: 0x0400433D RID: 17213
		private List<TutorialTipsUIItemData> m_ScrollItemData;

		// Token: 0x0400433E RID: 17214
		[SerializeField]
		[Tooltip("閉じるボタン.Callbackへのリンクはソースから行う.")]
		private CustomButton m_CloseButton;

		// Token: 0x0400433F RID: 17215
		private Action m_OnOpendCallback;

		// Token: 0x04004340 RID: 17216
		private Action m_OnClickCloseCallback;

		// Token: 0x04004341 RID: 17217
		private bool m_Destroyed;

		// Token: 0x04004342 RID: 17218
		private bool m_Preparing;

		// Token: 0x04004343 RID: 17219
		private TutorialTipsUI.eStep m_Step = TutorialTipsUI.eStep.None;

		// Token: 0x04004344 RID: 17220
		private int m_CurrentIdx;

		// Token: 0x02000B16 RID: 2838
		public enum eMode
		{
			// Token: 0x04004346 RID: 17222
			Tutorial,
			// Token: 0x04004347 RID: 17223
			Retire
		}

		// Token: 0x02000B17 RID: 2839
		public enum eStep
		{
			// Token: 0x04004349 RID: 17225
			None = -1,
			// Token: 0x0400434A RID: 17226
			In,
			// Token: 0x0400434B RID: 17227
			Idle,
			// Token: 0x0400434C RID: 17228
			Out
		}
	}
}
