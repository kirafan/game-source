﻿using System;

namespace Star.UI
{
	// Token: 0x02000856 RID: 2134
	public class BustImage : ASyncImage
	{
		// Token: 0x06002C6D RID: 11373 RVA: 0x000EA790 File Offset: 0x000E8B90
		public void Apply(int charaID, BustImage.eBustImageType bustImageType = BustImage.eBustImageType.Chara)
		{
			if (this.m_CharaID != charaID)
			{
				this.Apply();
				if (charaID != -1)
				{
					if (bustImageType != BustImage.eBustImageType.Chara)
					{
						if (bustImageType == BustImage.eBustImageType.Full)
						{
							this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncBustFull(charaID);
						}
					}
					else
					{
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncBust(charaID);
					}
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_CharaID = charaID;
			}
		}

		// Token: 0x06002C6E RID: 11374 RVA: 0x000EA828 File Offset: 0x000E8C28
		public override void Destroy()
		{
			base.Destroy();
			this.m_CharaID = -1;
		}

		// Token: 0x04003350 RID: 13136
		protected int m_CharaID = -1;

		// Token: 0x02000857 RID: 2135
		public enum eBustImageType
		{
			// Token: 0x04003352 RID: 13138
			Chara,
			// Token: 0x04003353 RID: 13139
			Full
		}
	}
}
