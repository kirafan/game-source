﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200084C RID: 2124
	[ExecuteInEditMode]
	[RequireComponent(typeof(Canvas), typeof(GraphicRaycaster))]
	public class FixSortOrder : MonoBehaviour
	{
		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06002C39 RID: 11321 RVA: 0x000E9878 File Offset: 0x000E7C78
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002C3A RID: 11322 RVA: 0x000E989D File Offset: 0x000E7C9D
		public UIDefine.eSortOrderTypeID GetRawSortOrder()
		{
			return this.m_SortOrder;
		}

		// Token: 0x06002C3B RID: 11323 RVA: 0x000E98A8 File Offset: 0x000E7CA8
		public UIDefine.eSortOrderTypeID GetSortOrderID()
		{
			UIDefine.eSortOrderTypeID result = this.m_SortOrder;
			if (this.m_SortOrder == UIDefine.eSortOrderTypeID.Inherited)
			{
				FixSortOrder componentInParent = this.RectTransform.parent.GetComponentInParent<FixSortOrder>();
				if (componentInParent == null)
				{
					result = UIDefine.eSortOrderTypeID.UI;
				}
				else
				{
					result = componentInParent.GetSortOrderID();
				}
			}
			return result;
		}

		// Token: 0x06002C3C RID: 11324 RVA: 0x000E98F4 File Offset: 0x000E7CF4
		public int GetSortOrder()
		{
			int result;
			if (this.m_SortOrder == UIDefine.eSortOrderTypeID.Inherited)
			{
				Transform parent = this.RectTransform.parent;
				if (parent != null)
				{
					FixSortOrder componentInParent = parent.GetComponentInParent<FixSortOrder>();
					if (componentInParent == null)
					{
						result = UIDefine.SORT_ORDER[1];
					}
					else
					{
						result = componentInParent.GetSortOrder() + this.m_Offset;
					}
				}
				else
				{
					result = UIDefine.SORT_ORDER[1];
				}
			}
			else
			{
				result = UIDefine.SORT_ORDER[(int)this.m_SortOrder] + this.m_Offset;
			}
			return result;
		}

		// Token: 0x06002C3D RID: 11325 RVA: 0x000E997A File Offset: 0x000E7D7A
		public int GetOffset()
		{
			return this.m_Offset;
		}

		// Token: 0x06002C3E RID: 11326 RVA: 0x000E9982 File Offset: 0x000E7D82
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrderType, int offset = 0)
		{
			this.m_SortOrder = sortOrderType;
			this.m_Offset = offset;
			this.m_IsDirty = true;
			this.Apply();
		}

		// Token: 0x06002C3F RID: 11327 RVA: 0x000E99A0 File Offset: 0x000E7DA0
		private void Apply()
		{
			this.m_Canvas = base.GetComponent<Canvas>();
			if (this.m_Canvas == null)
			{
				return;
			}
			this.m_Canvas.overrideSorting = true;
			this.m_Canvas.sortingOrder = this.GetSortOrder();
			this.m_IsDirty = false;
		}

		// Token: 0x06002C40 RID: 11328 RVA: 0x000E99EF File Offset: 0x000E7DEF
		private void Start()
		{
			this.Apply();
		}

		// Token: 0x06002C41 RID: 11329 RVA: 0x000E99F8 File Offset: 0x000E7DF8
		private void Update()
		{
			if (this.m_IsDirty)
			{
				this.Apply();
			}
			if (this.m_Canvas != null && this.m_Canvas.worldCamera == null && SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.ApplyCamera();
			}
		}

		// Token: 0x06002C42 RID: 11330 RVA: 0x000E9A64 File Offset: 0x000E7E64
		public void ApplyCamera()
		{
			this.m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
			if (!this.m_isUseSaveAreaCamera)
			{
				this.m_Canvas.worldCamera = SingletonMonoBehaviour<GameSystem>.Inst.UICamera;
			}
			else
			{
				this.m_Canvas.worldCamera = SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera;
			}
		}

		// Token: 0x0400330B RID: 13067
		[SerializeField]
		private UIDefine.eSortOrderTypeID m_SortOrder = UIDefine.eSortOrderTypeID.Inherited;

		// Token: 0x0400330C RID: 13068
		[SerializeField]
		private bool m_isUseSaveAreaCamera;

		// Token: 0x0400330D RID: 13069
		[SerializeField]
		private int m_Offset;

		// Token: 0x0400330E RID: 13070
		private bool m_IsDirty = true;

		// Token: 0x0400330F RID: 13071
		private Canvas m_Canvas;

		// Token: 0x04003310 RID: 13072
		private RectTransform m_RectTransform;
	}
}
