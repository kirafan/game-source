﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200084A RID: 2122
	public class FixFontText : MonoBehaviour
	{
		// Token: 0x06002C35 RID: 11317 RVA: 0x000E9712 File Offset: 0x000E7B12
		private void OnValidate()
		{
			this.Apply();
		}

		// Token: 0x06002C36 RID: 11318 RVA: 0x000E971A File Offset: 0x000E7B1A
		private void Awake()
		{
			this.Apply();
		}

		// Token: 0x06002C37 RID: 11319 RVA: 0x000E9724 File Offset: 0x000E7B24
		private void Apply()
		{
			Text component = base.GetComponent<Text>();
			Outline component2 = base.GetComponent<Outline>();
			switch (this.m_TextColorType)
			{
			case FixFontText.eColorType.Black:
				if (component.color != this.m_BlackColor)
				{
					component.color = this.m_BlackColor;
				}
				if (component2 != null && component2.effectColor != this.m_WhiteColor)
				{
					component2.effectColor = this.m_WhiteColor;
				}
				break;
			case FixFontText.eColorType.White:
				if (component.color != this.m_WhiteColor)
				{
					component.color = this.m_WhiteColor;
				}
				if (component2 != null && component2.effectColor != this.m_BlackColor)
				{
					component2.effectColor = this.m_BlackColor;
				}
				break;
			case FixFontText.eColorType.Warning:
				if (component.color != this.m_WarningColor)
				{
					component.color = this.m_WarningColor;
				}
				break;
			case FixFontText.eColorType.Blue:
				if (component.color != this.m_BlueColor)
				{
					component.color = this.m_BlueColor;
				}
				break;
			}
		}

		// Token: 0x04003300 RID: 13056
		private readonly Color m_BlackColor = new Color(0.43137255f, 0.25490198f, 0.20784314f);

		// Token: 0x04003301 RID: 13057
		private readonly Color m_WhiteColor = new Color(1f, 1f, 1f);

		// Token: 0x04003302 RID: 13058
		private readonly Color m_WarningColor = new Color(1f, 0.047058824f, 0.047058824f);

		// Token: 0x04003303 RID: 13059
		private readonly Color m_BlueColor = new Color(0.21568628f, 0.58431375f, 0.98039216f);

		// Token: 0x04003304 RID: 13060
		[SerializeField]
		private FixFontText.eColorType m_TextColorType = FixFontText.eColorType.Black;

		// Token: 0x0200084B RID: 2123
		private enum eColorType
		{
			// Token: 0x04003306 RID: 13062
			None,
			// Token: 0x04003307 RID: 13063
			Black,
			// Token: 0x04003308 RID: 13064
			White,
			// Token: 0x04003309 RID: 13065
			Warning,
			// Token: 0x0400330A RID: 13066
			Blue
		}
	}
}
