﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F3 RID: 2035
	public class MoveAnimUI : AnimUIBase
	{
		// Token: 0x06002A16 RID: 10774 RVA: 0x000DDE0C File Offset: 0x000DC20C
		protected override void Prepare()
		{
			base.Prepare();
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_ShowPos = this.m_RectTransform.anchoredPosition;
			this.m_FixedHidePos = this.m_ShowPos + this.m_HidePos;
			if (this.m_ReturnMode)
			{
				this.m_EndPos = this.m_HidePos;
			}
			this.m_FixedEndPos = this.m_ShowPos + this.m_EndPos;
			this.m_RectTransform.anchoredPosition = this.m_FixedHidePos;
		}

		// Token: 0x06002A17 RID: 10775 RVA: 0x000DDEA8 File Offset: 0x000DC2A8
		protected override void ExecutePlayIn()
		{
			if (this.m_Restart)
			{
				this.m_RectTransform.anchoredPosition = this.m_FixedHidePos;
			}
			RectTransform component = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector3 vector = this.m_ShowPos + new Vector3(((this.m_RectTransform.anchorMin.x + this.m_RectTransform.anchorMax.x) * 0.5f - component.pivot.x) * component.rect.width, ((this.m_RectTransform.anchorMin.y + this.m_RectTransform.anchorMax.y) * 0.5f - component.pivot.y) * component.rect.height);
			iTween.Stop(this.m_GameObject, "move");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", num);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("oncomplete", "OnCompleteInMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002A18 RID: 10776 RVA: 0x000DE07C File Offset: 0x000DC47C
		protected override void ExecutePlayOut()
		{
			if (this.m_Restart)
			{
				this.m_RectTransform.anchoredPosition = this.m_ShowPos;
			}
			RectTransform component = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector3 vector = this.m_FixedEndPos + new Vector3(((this.m_RectTransform.anchorMin.x + this.m_RectTransform.anchorMax.x) * 0.5f - 0.5f) * component.rect.width, ((this.m_RectTransform.anchorMin.y + this.m_RectTransform.anchorMax.y) * 0.5f - 0.5f) * component.rect.height);
			iTween.Stop(this.m_GameObject, "move");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", num);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("oncomplete", "OnCompleteOutMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002A19 RID: 10777 RVA: 0x000DE23C File Offset: 0x000DC63C
		public override void Hide()
		{
			base.Hide();
			iTween.Stop(this.m_GameObject, "move");
			this.m_RectTransform.anchoredPosition = this.m_FixedHidePos;
		}

		// Token: 0x06002A1A RID: 10778 RVA: 0x000DE26A File Offset: 0x000DC66A
		public override void Show()
		{
			base.Show();
			iTween.Stop(this.m_GameObject, "move");
			this.m_RectTransform.anchoredPosition = this.m_ShowPos;
		}

		// Token: 0x06002A1B RID: 10779 RVA: 0x000DE298 File Offset: 0x000DC698
		public void OnCompleteInMove()
		{
			this.m_RectTransform.anchoredPosition = this.m_ShowPos;
			this.m_IsPlaying = false;
			this.OnAnimCompleteIn.Call();
		}

		// Token: 0x06002A1C RID: 10780 RVA: 0x000DE2C2 File Offset: 0x000DC6C2
		public void OnCompleteOutMove()
		{
			this.m_RectTransform.anchoredPosition = this.m_FixedEndPos;
			this.m_IsPlaying = false;
			this.OnAnimCompleteOut.Call();
		}

		// Token: 0x0400306C RID: 12396
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x0400306D RID: 12397
		[SerializeField]
		private bool m_ReturnMode = true;

		// Token: 0x0400306E RID: 12398
		[SerializeField]
		private bool m_Restart;

		// Token: 0x0400306F RID: 12399
		[SerializeField]
		private Vector3 m_HidePos = Vector3.zero;

		// Token: 0x04003070 RID: 12400
		[SerializeField]
		private Vector3 m_EndPos = Vector3.zero;

		// Token: 0x04003071 RID: 12401
		private Vector3 m_ShowPos;

		// Token: 0x04003072 RID: 12402
		private Vector3 m_FixedHidePos;

		// Token: 0x04003073 RID: 12403
		private Vector3 m_FixedEndPos;

		// Token: 0x04003074 RID: 12404
		[SerializeField]
		private float m_AnimDuration = -1f;

		// Token: 0x04003075 RID: 12405
		[SerializeField]
		private iTween.EaseType m_EaseType = iTween.EaseType.easeOutQuad;

		// Token: 0x04003076 RID: 12406
		private GameObject m_GameObject;

		// Token: 0x04003077 RID: 12407
		private RectTransform m_RectTransform;
	}
}
