﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000859 RID: 2137
	public class CharaIconFrame : MonoBehaviour
	{
		// Token: 0x06002C73 RID: 11379 RVA: 0x000EA8D0 File Offset: 0x000E8CD0
		private void Update()
		{
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			float width = this.m_OwnerRect.rect.width;
			if (width > 0f)
			{
				this.m_RectTransform.localScale = Vector3.one * (width / 128f);
			}
		}

		// Token: 0x06002C74 RID: 11380 RVA: 0x000EA935 File Offset: 0x000E8D35
		public void SetOwnerIcon(CharaIconWithFrame owner)
		{
			this.m_OwnerRect = owner.GetComponent<RectTransform>();
		}

		// Token: 0x06002C75 RID: 11381 RVA: 0x000EA944 File Offset: 0x000E8D44
		private void ApplyCharaParam(int charaID)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			this.m_FrameImage.sprite = this.m_FrameSprites[param.m_Rare];
			this.m_ElementIcon.Apply((eElementType)param.m_Element);
			this.m_ClassIcon.Apply((eClassType)param.m_Class);
		}

		// Token: 0x06002C76 RID: 11382 RVA: 0x000EA9A4 File Offset: 0x000E8DA4
		public void ApplyCharaID(int charaID, bool showUserCharaParam = false)
		{
			this.m_SupportData = null;
			this.ApplyCharaParam(charaID);
			if (!showUserCharaParam)
			{
				if (this.m_LBIcon.GameObject.activeSelf && !this.forceActive)
				{
					this.m_LBIcon.GameObject.SetActive(false);
				}
				this.ChangeDetailText(CharaIconFrame.eDetailType.None);
				this.SetNew(false);
			}
			else
			{
				if (!this.m_LBIcon.GameObject.activeSelf)
				{
					this.m_LBIcon.GameObject.SetActive(true);
				}
				this.m_LBIcon.SetValue(0);
				if (!this.m_StatusObj.activeSelf)
				{
					this.m_StatusObj.SetActive(true);
				}
				this.m_StatusTitle.Apply(IconStatusTitle.eType.Lv);
				this.m_DetailText.text = 1.ToString();
				this.SetNew(false);
			}
		}

		// Token: 0x06002C77 RID: 11383 RVA: 0x000EAA84 File Offset: 0x000E8E84
		public void ApplyCharaMngID(long charaMngID)
		{
			this.m_SupportData = null;
			this.m_CharaMngID = charaMngID;
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			CharacterParam param = userCharaData.Param;
			int charaID = param.CharaID;
			this.ApplyCharaParam(charaID);
			if (!this.m_LBIcon.GameObject.activeSelf)
			{
				this.m_LBIcon.GameObject.SetActive(true);
			}
			if (!this.m_StatusObj.activeSelf)
			{
				this.m_StatusObj.SetActive(true);
			}
			this.SetLimitBreak(param.LimitBreak);
			this.ChangeDetailText(this.m_DetailType);
			this.SetNew(userCharaData.IsNew);
		}

		// Token: 0x06002C78 RID: 11384 RVA: 0x000EAB2C File Offset: 0x000E8F2C
		public void ApplySupport(UserSupportData supportData)
		{
			this.m_CharaMngID = -1L;
			int charaID = supportData.CharaID;
			this.ApplyCharaID(charaID, false);
			this.m_SupportData = supportData;
			if (!this.m_LBIcon.GameObject.activeSelf)
			{
				this.m_LBIcon.GameObject.SetActive(true);
			}
			if (!this.m_StatusObj.activeSelf)
			{
				this.m_StatusObj.SetActive(true);
			}
			this.SetLimitBreak(this.m_SupportData.LimitBreak);
			this.ChangeDetailText(this.m_DetailType);
			this.SetNew(false);
		}

		// Token: 0x06002C79 RID: 11385 RVA: 0x000EABC0 File Offset: 0x000E8FC0
		public void SetLimitBreak(int lb)
		{
			if (lb <= 0)
			{
				if (this.m_LBIcon.GameObject.activeSelf)
				{
					this.m_LBIcon.GameObject.SetActive(false);
				}
				this.forceActive = false;
				return;
			}
			if (!this.m_LBIcon.GameObject.activeSelf)
			{
				this.m_LBIcon.GameObject.SetActive(true);
			}
			this.m_LBIcon.SetValue(lb);
			this.forceActive = true;
		}

		// Token: 0x06002C7A RID: 11386 RVA: 0x000EAC3B File Offset: 0x000E903B
		public void SetNew(bool flg)
		{
			this.m_NewObj.SetActive(flg);
		}

		// Token: 0x06002C7B RID: 11387 RVA: 0x000EAC4C File Offset: 0x000E904C
		public void ChangeDetailText(CharaIconFrame.eDetailType detailType)
		{
			if (this.m_SupportData == null)
			{
				CharacterParam characterParam = null;
				if (this.m_CharaMngID != -1L)
				{
					UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_CharaMngID);
					characterParam = userCharaData.Param;
				}
				if (detailType == CharaIconFrame.eDetailType.None)
				{
					if (this.m_StatusObj.activeSelf)
					{
						this.m_StatusObj.SetActive(false);
					}
				}
				else if (!this.m_StatusObj.activeSelf)
				{
					this.m_StatusObj.SetActive(true);
				}
				this.m_DetailType = detailType;
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Length = 0;
				switch (detailType)
				{
				case CharaIconFrame.eDetailType.Lv:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Lv);
					this.m_DetailText.text = characterParam.Lv.ToString();
					break;
				case CharaIconFrame.eDetailType.HP:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Hp);
					this.m_DetailText.text = characterParam.Hp.ToString();
					break;
				case CharaIconFrame.eDetailType.Atk:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Atk);
					this.m_DetailText.text = characterParam.Atk.ToString();
					break;
				case CharaIconFrame.eDetailType.Mgc:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Mgc);
					this.m_DetailText.text = characterParam.Mgc.ToString();
					break;
				case CharaIconFrame.eDetailType.Def:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Def);
					this.m_DetailText.text = characterParam.Def.ToString();
					break;
				case CharaIconFrame.eDetailType.MDef:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.MDef);
					this.m_DetailText.text = characterParam.MDef.ToString();
					break;
				case CharaIconFrame.eDetailType.Spd:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Spd);
					this.m_DetailText.text = characterParam.Spd.ToString();
					break;
				case CharaIconFrame.eDetailType.Cost:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Cost);
					this.m_DetailText.text = characterParam.Cost.ToString();
					break;
				}
			}
			else
			{
				EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(this.m_SupportData);
				if (detailType == CharaIconFrame.eDetailType.None)
				{
					this.m_StatusObj.SetActive(false);
				}
				else
				{
					this.m_StatusObj.SetActive(true);
				}
				this.m_DetailType = detailType;
				StringBuilder stringBuilder2 = new StringBuilder();
				stringBuilder2.Length = 0;
				switch (detailType)
				{
				case CharaIconFrame.eDetailType.Lv:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Lv);
					this.m_DetailText.text = this.m_SupportData.Lv.ToString();
					break;
				case CharaIconFrame.eDetailType.HP:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Hp);
					this.m_DetailText.text = outputCharaParam.Hp.ToString();
					break;
				case CharaIconFrame.eDetailType.Atk:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Atk);
					this.m_DetailText.text = outputCharaParam.Atk.ToString();
					break;
				case CharaIconFrame.eDetailType.Mgc:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Mgc);
					this.m_DetailText.text = outputCharaParam.Mgc.ToString();
					break;
				case CharaIconFrame.eDetailType.Def:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Def);
					this.m_DetailText.text = outputCharaParam.Def.ToString();
					break;
				case CharaIconFrame.eDetailType.MDef:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.MDef);
					this.m_DetailText.text = outputCharaParam.MDef.ToString();
					break;
				case CharaIconFrame.eDetailType.Spd:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Spd);
					this.m_DetailText.text = outputCharaParam.Spd.ToString();
					break;
				case CharaIconFrame.eDetailType.Cost:
					this.m_StatusTitle.Apply(IconStatusTitle.eType.Cost);
					this.m_DetailText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_SupportData.CharaID).m_Cost.ToString();
					break;
				}
			}
		}

		// Token: 0x06002C7C RID: 11388 RVA: 0x000EB0D2 File Offset: 0x000E94D2
		public void SetDetailText(string text)
		{
			this.m_DetailText.text = text;
		}

		// Token: 0x06002C7D RID: 11389 RVA: 0x000EB0E0 File Offset: 0x000E94E0
		public void Destroy()
		{
		}

		// Token: 0x04003355 RID: 13141
		[SerializeField]
		private Image m_FrameImage;

		// Token: 0x04003356 RID: 13142
		[SerializeField]
		private Sprite[] m_FrameSprites;

		// Token: 0x04003357 RID: 13143
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04003358 RID: 13144
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04003359 RID: 13145
		[SerializeField]
		private LimitBreakIcon m_LBIcon;

		// Token: 0x0400335A RID: 13146
		[SerializeField]
		private GameObject m_StatusObj;

		// Token: 0x0400335B RID: 13147
		[SerializeField]
		private IconStatusTitle m_StatusTitle;

		// Token: 0x0400335C RID: 13148
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400335D RID: 13149
		private bool forceActive;

		// Token: 0x0400335E RID: 13150
		private CharaIconFrame.eDetailType m_DetailType;

		// Token: 0x0400335F RID: 13151
		[SerializeField]
		private GameObject m_NewObj;

		// Token: 0x04003360 RID: 13152
		private long m_CharaMngID = -1L;

		// Token: 0x04003361 RID: 13153
		private UserSupportData m_SupportData;

		// Token: 0x04003362 RID: 13154
		private RectTransform m_RectTransform;

		// Token: 0x04003363 RID: 13155
		private RectTransform m_OwnerRect;

		// Token: 0x0200085A RID: 2138
		public enum eDetailType
		{
			// Token: 0x04003365 RID: 13157
			None,
			// Token: 0x04003366 RID: 13158
			Lv,
			// Token: 0x04003367 RID: 13159
			HP,
			// Token: 0x04003368 RID: 13160
			Atk,
			// Token: 0x04003369 RID: 13161
			Mgc,
			// Token: 0x0400336A RID: 13162
			Def,
			// Token: 0x0400336B RID: 13163
			MDef,
			// Token: 0x0400336C RID: 13164
			Spd,
			// Token: 0x0400336D RID: 13165
			Cost
		}
	}
}
