﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008B7 RID: 2231
	public class UIEmittersGroup : PlayInterfaceMonoBehaviour
	{
		// Token: 0x06002E43 RID: 11843 RVA: 0x000F3AAC File Offset: 0x000F1EAC
		public override void Play()
		{
			if (this.m_Emitters != null)
			{
				for (int i = 0; i < this.m_Emitters.Length; i++)
				{
					this.m_Emitters[i].Play();
				}
			}
		}

		// Token: 0x06002E44 RID: 11844 RVA: 0x000F3AEC File Offset: 0x000F1EEC
		public override void Stop()
		{
			if (this.m_Emitters != null)
			{
				for (int i = 0; i < this.m_Emitters.Length; i++)
				{
					this.m_Emitters[i].Stop();
				}
			}
		}

		// Token: 0x0400356E RID: 13678
		[SerializeField]
		private UIEmitter[] m_Emitters;
	}
}
