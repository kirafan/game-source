﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000897 RID: 2199
	public class LvupPopPlayer : MonoBehaviour
	{
		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06002D9F RID: 11679 RVA: 0x000F0B22 File Offset: 0x000EEF22
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002DA0 RID: 11680 RVA: 0x000F0B48 File Offset: 0x000EEF48
		public void Play()
		{
			this.m_Pops[this.m_Idx].Play();
			this.m_Idx++;
			if (this.m_Idx >= this.m_Pops.Length)
			{
				this.m_Idx = 0;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_LV_UP, 1f, 0, -1, -1);
		}

		// Token: 0x06002DA1 RID: 11681 RVA: 0x000F0BAC File Offset: 0x000EEFAC
		public void HidePopUp()
		{
			for (int i = 0; i < this.m_Pops.Length; i++)
			{
				this.m_Pops[i].gameObject.SetActive(false);
			}
		}

		// Token: 0x040034AF RID: 13487
		private const int INST_NUM = 2;

		// Token: 0x040034B0 RID: 13488
		[SerializeField]
		private LevelUpPop[] m_Pops;

		// Token: 0x040034B1 RID: 13489
		private int m_Idx;

		// Token: 0x040034B2 RID: 13490
		private RectTransform m_RectTransform;
	}
}
