﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008A1 RID: 2209
	public class ScrollRectEx : ScrollRect
	{
		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06002DD4 RID: 11732 RVA: 0x000F12F8 File Offset: 0x000EF6F8
		public ScrollRect ScrollRect
		{
			get
			{
				return this;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06002DD5 RID: 11733 RVA: 0x000F12FB File Offset: 0x000EF6FB
		public bool IsDrag
		{
			get
			{
				return this.m_IsDrag;
			}
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06002DD6 RID: 11734 RVA: 0x000F1303 File Offset: 0x000EF703
		public float DragTime
		{
			get
			{
				return this.m_DragTime;
			}
		}

		// Token: 0x06002DD7 RID: 11735 RVA: 0x000F130B File Offset: 0x000EF70B
		private void Update()
		{
			if (this.m_IsDrag)
			{
				this.m_DragTime += Time.deltaTime;
			}
			else
			{
				this.m_DragTime = 0f;
			}
		}

		// Token: 0x06002DD8 RID: 11736 RVA: 0x000F133A File Offset: 0x000EF73A
		public override void OnBeginDrag(PointerEventData eventData)
		{
			if (this.ScrollRect.horizontal || this.ScrollRect.vertical)
			{
				this.m_IsDrag = true;
			}
			base.OnBeginDrag(eventData);
		}

		// Token: 0x06002DD9 RID: 11737 RVA: 0x000F136A File Offset: 0x000EF76A
		public override void OnEndDrag(PointerEventData eventData)
		{
			this.m_IsDrag = false;
			this.m_DragTime = 0f;
			base.OnEndDrag(eventData);
		}

		// Token: 0x06002DDA RID: 11738 RVA: 0x000F1385 File Offset: 0x000EF785
		public virtual void ReleaseDrag()
		{
			this.m_IsDrag = false;
			this.m_DragTime = 0f;
		}

		// Token: 0x040034D8 RID: 13528
		private bool m_IsDrag;

		// Token: 0x040034D9 RID: 13529
		private float m_DragTime;
	}
}
