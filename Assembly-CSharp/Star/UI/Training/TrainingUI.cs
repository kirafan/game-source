﻿using System;
using UnityEngine;

namespace Star.UI.Training
{
	// Token: 0x02000B0E RID: 2830
	public class TrainingUI : MenuUIBase
	{
		// Token: 0x06003B7A RID: 15226 RVA: 0x0012FAF4 File Offset: 0x0012DEF4
		public void Setup(NPCCharaDisplayUI npcUI)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_NpcUI = npcUI;
		}

		// Token: 0x06003B7B RID: 15227 RVA: 0x0012FB28 File Offset: 0x0012DF28
		private void Update()
		{
			switch (this.m_Step)
			{
			case TrainingUI.eStep.InNpc:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag && this.m_NpcUI.IsIdle())
				{
					this.ChangeStep(TrainingUI.eStep.In);
				}
				break;
			}
			case TrainingUI.eStep.In:
			{
				bool flag2 = true;
				if (!this.m_MainGroup.IsDonePlayIn || !this.m_NpcUI.IsIdle())
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(TrainingUI.eStep.Idle);
				}
				break;
			}
			case TrainingUI.eStep.Idle:
				this.m_TimeCount += Time.deltaTime;
				if (this.m_TimeCount > 6f)
				{
					this.PlayOut();
				}
				break;
			case TrainingUI.eStep.Out:
			{
				bool flag3 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag3 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag3 = false;
				}
				if (flag3)
				{
					this.ChangeStep(TrainingUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003B7C RID: 15228 RVA: 0x0012FC88 File Offset: 0x0012E088
		private void ChangeStep(TrainingUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case TrainingUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case TrainingUI.eStep.InNpc:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case TrainingUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case TrainingUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case TrainingUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case TrainingUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003B7D RID: 15229 RVA: 0x0012FD60 File Offset: 0x0012E160
		public override void PlayIn()
		{
			this.ChangeStep(TrainingUI.eStep.InNpc);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Training);
		}

		// Token: 0x06003B7E RID: 15230 RVA: 0x0012FDA8 File Offset: 0x0012E1A8
		public override void PlayOut()
		{
			this.ChangeStep(TrainingUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_NpcUI.PlayOut();
		}

		// Token: 0x06003B7F RID: 15231 RVA: 0x0012FDED File Offset: 0x0012E1ED
		public override bool IsMenuEnd()
		{
			return this.m_Step == TrainingUI.eStep.End;
		}

		// Token: 0x04004311 RID: 17169
		private const float DISPLAY_TIME = 6f;

		// Token: 0x04004312 RID: 17170
		private TrainingUI.eStep m_Step;

		// Token: 0x04004313 RID: 17171
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04004314 RID: 17172
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004315 RID: 17173
		private float m_TimeCount;

		// Token: 0x04004316 RID: 17174
		private NPCCharaDisplayUI m_NpcUI;

		// Token: 0x02000B0F RID: 2831
		private enum eStep
		{
			// Token: 0x04004318 RID: 17176
			Hide,
			// Token: 0x04004319 RID: 17177
			InNpc,
			// Token: 0x0400431A RID: 17178
			In,
			// Token: 0x0400431B RID: 17179
			Idle,
			// Token: 0x0400431C RID: 17180
			Out,
			// Token: 0x0400431D RID: 17181
			End
		}
	}
}
