﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200087B RID: 2171
	public class WeaponIconWithFrame : MonoBehaviour
	{
		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06002CF3 RID: 11507 RVA: 0x000ED138 File Offset: 0x000EB538
		private GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x06002CF4 RID: 11508 RVA: 0x000ED15D File Offset: 0x000EB55D
		public void Apply(int weaponID)
		{
			if (this.m_DetailObj.activeSelf)
			{
				this.m_DetailObj.SetActive(false);
			}
			this.ApplyWeaponID(weaponID);
		}

		// Token: 0x06002CF5 RID: 11509 RVA: 0x000ED184 File Offset: 0x000EB584
		public void Apply(UserWeaponData wpnData)
		{
			int weaponID = -1;
			if (wpnData != null)
			{
				weaponID = wpnData.Param.WeaponID;
				int lv = wpnData.Param.Lv;
			}
			this.m_DetailStatus.m_Lv = wpnData.Param.Lv;
			this.m_DetailStatus.m_Cost = wpnData.Param.Cost;
			this.m_DetailStatus.m_Atk = wpnData.Param.Atk;
			this.m_DetailStatus.m_Mgc = wpnData.Param.Mgc;
			this.m_DetailStatus.m_Def = wpnData.Param.Def;
			this.m_DetailStatus.m_MDef = wpnData.Param.MDef;
			this.ApplyWeaponID(weaponID);
			this.ApplyDetailText(this.m_CurrentDetail);
		}

		// Token: 0x06002CF6 RID: 11510 RVA: 0x000ED24C File Offset: 0x000EB64C
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
			int weaponID = -1;
			if (cdc == null || cdc.GetWeaponIdNaked() == -1 || cdc.GetWeaponLv() == -1)
			{
				this.SetEnableRenderDetailText(false);
				if (cdc != null)
				{
					weaponID = cdc.GetWeaponId();
				}
			}
			else
			{
				weaponID = cdc.GetWeaponId();
				this.SetEnableRenderDetailText(true);
				this.m_DetailStatus.m_Lv = cdc.GetWeaponLv();
				this.m_DetailStatus.m_Cost = cdc.GetWeaponCost();
				this.m_DetailStatus.m_Atk = cdc.GetWeaponParam().Atk;
				this.m_DetailStatus.m_Mgc = cdc.GetWeaponParam().Mgc;
				this.m_DetailStatus.m_Def = cdc.GetWeaponParam().Def;
				this.m_DetailStatus.m_MDef = cdc.GetWeaponParam().MDef;
			}
			this.ApplyWeaponID(weaponID);
			this.ApplyDetailText(this.m_CurrentDetail);
		}

		// Token: 0x06002CF7 RID: 11511 RVA: 0x000ED33C File Offset: 0x000EB73C
		private void ApplyWeaponID(int weaponID)
		{
			if ((this.m_Mode == WeaponIconWithFrame.eMode.Add || this.m_Mode == WeaponIconWithFrame.eMode.Disable || this.m_Mode == WeaponIconWithFrame.eMode.NakedBGOnly) && EditUtility.CheckDefaultWeaponNaked(weaponID))
			{
				weaponID = -1;
			}
			if (weaponID == -1)
			{
				this.m_WeaponIcon.Destroy();
				this.m_RareStar.Apply(eRare.None);
				this.m_Background.sprite = this.GetNoExistBackgroundSprite();
				this.m_SwapBackground.sprite = this.GetNoExistSwapBackgroundSprite();
				if (this.m_Mode == WeaponIconWithFrame.eMode.Normal || this.m_Mode == WeaponIconWithFrame.eMode.IgnoreNaked)
				{
					if (this.GameObject.activeSelf)
					{
						this.GameObject.SetActive(false);
					}
				}
				else
				{
					this.SetNoExistDisableModeHideImagesActive(false);
					if (this.m_Mode == WeaponIconWithFrame.eMode.Disable)
					{
						this.m_Background.enabled = false;
						this.m_SwapBackground.enabled = false;
					}
					else
					{
						this.m_Background.enabled = true;
						this.m_SwapBackground.enabled = true;
					}
				}
				return;
			}
			if (!this.GameObject.activeSelf)
			{
				this.GameObject.SetActive(true);
			}
			this.m_Background.enabled = true;
			this.m_SwapBackground.enabled = true;
			int weaponID2 = weaponID;
			if (EditUtility.CheckDefaultWeaponNaked(weaponID2) && this.m_Mode == WeaponIconWithFrame.eMode.IgnoreNaked)
			{
				weaponID2 = -1;
			}
			this.m_WeaponIcon.Apply(weaponID2);
			this.m_RareStar.Apply((eRare)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_Rare);
			if (EditUtility.CheckDefaultWeaponNaked(weaponID) && this.m_Mode == WeaponIconWithFrame.eMode.IgnoreNaked)
			{
				this.m_Background.sprite = this.GetNoExistBackgroundSprite();
				this.m_SwapBackground.sprite = this.GetNoExistSwapBackgroundSprite();
			}
			else
			{
				this.m_Background.sprite = this.GetExistBackgroundSprite();
				this.m_SwapBackground.sprite = this.GetExistSwapBackgroundSprite();
			}
			if (this.m_Mode == WeaponIconWithFrame.eMode.Add || this.m_Mode == WeaponIconWithFrame.eMode.Disable || this.m_Mode == WeaponIconWithFrame.eMode.NakedBGOnly)
			{
				this.SetNoExistDisableModeHideImagesActive(true);
			}
		}

		// Token: 0x06002CF8 RID: 11512 RVA: 0x000ED54A File Offset: 0x000EB94A
		public void SetEnableRenderDetailText(bool flg)
		{
			if (this.m_DetailObj.activeSelf != flg)
			{
				this.m_DetailObj.SetActive(flg);
			}
		}

		// Token: 0x06002CF9 RID: 11513 RVA: 0x000ED56C File Offset: 0x000EB96C
		public void ApplyDetailText(UISettings.eSortValue_Weapon sortValue)
		{
			switch (sortValue)
			{
			case UISettings.eSortValue_Weapon.Cost:
				this.m_DetailText.text = this.m_DetailStatus.m_Cost.ToString();
				this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.Cost);
				return;
			case UISettings.eSortValue_Weapon.Atk:
				this.m_DetailText.text = this.m_DetailStatus.m_Atk.ToString();
				this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.Atk);
				return;
			case UISettings.eSortValue_Weapon.Mgc:
				this.m_DetailText.text = this.m_DetailStatus.m_Mgc.ToString();
				this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.Mgc);
				return;
			case UISettings.eSortValue_Weapon.Def:
				this.m_DetailText.text = this.m_DetailStatus.m_Def.ToString();
				this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.Def);
				return;
			case UISettings.eSortValue_Weapon.MDef:
				this.m_DetailText.text = this.m_DetailStatus.m_MDef.ToString();
				this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.MDef);
				return;
			}
			this.m_DetailText.text = this.m_DetailStatus.m_Lv.ToString();
			this.m_DetailStatusTitle.Apply(IconStatusTitle.eType.Lv);
		}

		// Token: 0x06002CFA RID: 11514 RVA: 0x000ED6CE File Offset: 0x000EBACE
		private void ChangeEmptyState()
		{
		}

		// Token: 0x06002CFB RID: 11515 RVA: 0x000ED6D0 File Offset: 0x000EBAD0
		public void Destroy()
		{
			if (this.m_WeaponIcon != null)
			{
				this.m_WeaponIcon.Destroy();
			}
		}

		// Token: 0x06002CFC RID: 11516 RVA: 0x000ED6EE File Offset: 0x000EBAEE
		public void SetMode(WeaponIconWithFrame.eMode mode)
		{
			this.m_Mode = mode;
		}

		// Token: 0x06002CFD RID: 11517 RVA: 0x000ED6F8 File Offset: 0x000EBAF8
		private Sprite GetExistBackgroundSprite()
		{
			WeaponIconWithFrame.eMode mode = this.m_Mode;
			if (mode == WeaponIconWithFrame.eMode.Normal || mode != WeaponIconWithFrame.eMode.IgnoreNaked)
			{
			}
			return this.m_ExistBackground;
		}

		// Token: 0x06002CFE RID: 11518 RVA: 0x000ED724 File Offset: 0x000EBB24
		private Sprite GetExistSwapBackgroundSprite()
		{
			Sprite existSwapBackgroundSpriteNaked = this.GetExistSwapBackgroundSpriteNaked();
			if (existSwapBackgroundSpriteNaked == null)
			{
				return this.GetExistBackgroundSprite();
			}
			return existSwapBackgroundSpriteNaked;
		}

		// Token: 0x06002CFF RID: 11519 RVA: 0x000ED74C File Offset: 0x000EBB4C
		private Sprite GetExistSwapBackgroundSpriteNaked()
		{
			WeaponIconWithFrame.eMode mode = this.m_Mode;
			if (mode != WeaponIconWithFrame.eMode.Normal && mode != WeaponIconWithFrame.eMode.IgnoreNaked)
			{
				return null;
			}
			return this.m_ExistSwapBackground;
		}

		// Token: 0x06002D00 RID: 11520 RVA: 0x000ED77C File Offset: 0x000EBB7C
		private Sprite GetNoExistBackgroundSprite()
		{
			switch (this.m_Mode)
			{
			case WeaponIconWithFrame.eMode.Normal:
			case WeaponIconWithFrame.eMode.IgnoreNaked:
				return this.m_NoExistBackground;
			case WeaponIconWithFrame.eMode.Add:
				return this.m_NoExistAddModeBackground;
			case WeaponIconWithFrame.eMode.Disable:
				return null;
			case WeaponIconWithFrame.eMode.NakedBGOnly:
				return this.m_NoExistNakedSkipLoadModeBackground;
			default:
				return null;
			}
		}

		// Token: 0x06002D01 RID: 11521 RVA: 0x000ED7C8 File Offset: 0x000EBBC8
		private Sprite GetNoExistSwapBackgroundSprite()
		{
			Sprite noExistSwapBackgroundSpriteNaked = this.GetNoExistSwapBackgroundSpriteNaked();
			if (noExistSwapBackgroundSpriteNaked == null)
			{
				return this.GetNoExistBackgroundSprite();
			}
			return noExistSwapBackgroundSpriteNaked;
		}

		// Token: 0x06002D02 RID: 11522 RVA: 0x000ED7F0 File Offset: 0x000EBBF0
		private Sprite GetNoExistSwapBackgroundSpriteNaked()
		{
			switch (this.m_Mode)
			{
			case WeaponIconWithFrame.eMode.Normal:
			case WeaponIconWithFrame.eMode.IgnoreNaked:
				return this.m_NoExistSwapBackground;
			case WeaponIconWithFrame.eMode.Add:
				return this.m_NoExistAddModeSwapBackground;
			case WeaponIconWithFrame.eMode.Disable:
				return null;
			case WeaponIconWithFrame.eMode.NakedBGOnly:
				return this.m_NoExistNakedSkipLoadModeBackground;
			default:
				return null;
			}
		}

		// Token: 0x06002D03 RID: 11523 RVA: 0x000ED83C File Offset: 0x000EBC3C
		private void SetNoExistDisableModeHideImagesActive(bool active)
		{
			for (int i = 0; i < this.m_NoExistDisableModeHideImages.Length; i++)
			{
				if (this.m_NoExistDisableModeHideImages[i] != null)
				{
					this.m_NoExistDisableModeHideImages[i].SetActive(active);
				}
			}
		}

		// Token: 0x040033FE RID: 13310
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x040033FF RID: 13311
		[SerializeField]
		[Tooltip("Frame内BG.")]
		private Image m_Background;

		// Token: 0x04003400 RID: 13312
		[SerializeField]
		[Tooltip("WeaponIconの親のボタンが押されているときに表示するFrame内BG.指定がなければSwap用処理を行わない.")]
		private Image m_SwapBackground;

		// Token: 0x04003401 RID: 13313
		[SerializeField]
		[Tooltip("通常状態で武器を表示中に表示するBG.指定がない場合は何も表示しない.")]
		private Sprite m_ExistBackground;

		// Token: 0x04003402 RID: 13314
		[SerializeField]
		[Tooltip("通常状態で武器を表示中に表示するBG.指定がなければSwap時も通常時と同じ画像になる.")]
		private Sprite m_ExistSwapBackground;

		// Token: 0x04003403 RID: 13315
		[SerializeField]
		[Tooltip("通常状態で武器を表示していない時に表示するBG.指定がない場合は何も表示しない.現状旧実装モード時は親オブジェクトのステートが切り替わるので表示されない.")]
		private Sprite m_NoExistBackground;

		// Token: 0x04003404 RID: 13316
		[SerializeField]
		[Tooltip("通常状態で武器を表示していない時に表示するBG.指定がなければSwap時も通常時と同じ画像になる.現状旧実装モード時は親オブジェクトのステートが切り替わるので表示されない.")]
		private Sprite m_NoExistSwapBackground;

		// Token: 0x04003405 RID: 13317
		[SerializeField]
		[Tooltip("AddModeで武器を表示していない時に表示するBG.指定がない場合は何も表示しない.元の実装に対して関数コールで追加機能を制御できるようにするための実装なので,基本的にはこちらは変更せずNormal用画像を差し替えて対応する.")]
		private Sprite m_NoExistAddModeBackground;

		// Token: 0x04003406 RID: 13318
		[SerializeField]
		[Tooltip("AddModeで武器を表示していない時に表示するBG.指定がなければSwap時も通常時と同じ画像になる.元の実装に対して関数コールで追加機能を制御できるようにするための実装なので,基本的にはこちらは変更せずNormal用画像を差し替えて対応する.")]
		private Sprite m_NoExistAddModeSwapBackground;

		// Token: 0x04003407 RID: 13319
		[SerializeField]
		[Tooltip("AddModeで武器を表示していない時に表示するBG.指定がない場合は何も表示しない.元の実装に対して関数コールで追加機能を制御できるようにするための実装なので,基本的にはこちらは変更せずNormal用画像を差し替えて対応する.")]
		private Sprite m_NoExistNakedSkipLoadModeBackground;

		// Token: 0x04003408 RID: 13320
		[SerializeField]
		[Tooltip("AddModeで武器を表示していない時に表示するBG.指定がなければSwap時も通常時と同じ画像になる.元の実装に対して関数コールで追加機能を制御できるようにするための実装なので,基本的にはこちらは変更せずNormal用画像を差し替えて対応する.")]
		private Sprite m_NoExistNakedSkipLoadModeSwapBackground;

		// Token: 0x04003409 RID: 13321
		[SerializeField]
		[Tooltip("AddModeとDisableModeで武器を表示していない時に非表示にする画像.現状共通なので定義を統合中.現状表示している場合に全てActiveにするので,場合によっては処理順や例外対応等を行う事.")]
		private GameObject[] m_NoExistDisableModeHideImages;

		// Token: 0x0400340A RID: 13322
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x0400340B RID: 13323
		[SerializeField]
		private GameObject m_DetailObj;

		// Token: 0x0400340C RID: 13324
		[SerializeField]
		private IconStatusTitle m_DetailStatusTitle;

		// Token: 0x0400340D RID: 13325
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400340E RID: 13326
		private WeaponIconWithFrame.eMode m_Mode;

		// Token: 0x0400340F RID: 13327
		private WeaponIconWithFrame.WeaponDetailStatus m_DetailStatus = new WeaponIconWithFrame.WeaponDetailStatus();

		// Token: 0x04003410 RID: 13328
		private UISettings.eSortValue_Weapon m_CurrentDetail;

		// Token: 0x04003411 RID: 13329
		private GameObject m_GameObject;

		// Token: 0x0200087C RID: 2172
		public enum eMode
		{
			// Token: 0x04003413 RID: 13331
			Normal,
			// Token: 0x04003414 RID: 13332
			Add,
			// Token: 0x04003415 RID: 13333
			Disable,
			// Token: 0x04003416 RID: 13334
			IgnoreNaked,
			// Token: 0x04003417 RID: 13335
			NakedBGOnly
		}

		// Token: 0x0200087D RID: 2173
		public class WeaponDetailStatus
		{
			// Token: 0x04003418 RID: 13336
			public int m_Lv;

			// Token: 0x04003419 RID: 13337
			public int m_Cost;

			// Token: 0x0400341A RID: 13338
			public int m_Atk;

			// Token: 0x0400341B RID: 13339
			public int m_Mgc;

			// Token: 0x0400341C RID: 13340
			public int m_Def;

			// Token: 0x0400341D RID: 13341
			public int m_MDef;
		}
	}
}
