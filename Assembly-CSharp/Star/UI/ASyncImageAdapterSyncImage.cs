﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B6 RID: 1974
	public abstract class ASyncImageAdapterSyncImage : ASyncImageAdapter
	{
		// Token: 0x060028D1 RID: 10449 RVA: 0x000D9B0C File Offset: 0x000D7F0C
		public override void Destroy()
		{
			ASyncImage asyncImage = this.GetASyncImage();
			if (asyncImage == null)
			{
				return;
			}
			asyncImage.Destroy();
		}

		// Token: 0x060028D2 RID: 10450 RVA: 0x000D9B34 File Offset: 0x000D7F34
		public override bool IsDoneLoad()
		{
			ASyncImage asyncImage = this.GetASyncImage();
			return asyncImage == null || asyncImage.IsDoneLoad();
		}

		// Token: 0x060028D3 RID: 10451
		protected abstract ASyncImage GetASyncImage();

		// Token: 0x060028D4 RID: 10452 RVA: 0x000D9B5C File Offset: 0x000D7F5C
		protected override MonoBehaviour GetMonoBehaviour()
		{
			return this.GetASyncImage();
		}
	}
}
