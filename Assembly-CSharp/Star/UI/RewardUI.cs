﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000A6C RID: 2668
	public class RewardUI : MenuUIBase
	{
		// Token: 0x0600377A RID: 14202 RVA: 0x001190BC File Offset: 0x001174BC
		public void ApplyDisplayData(RewardPlayer.DisplayData displayData)
		{
			switch (displayData.m_RewardType)
			{
			case RewardPlayer.eRewardType.FirstClear:
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardFirstClear);
				this.m_DetailText.text = displayData.m_Text;
				break;
			case RewardPlayer.eRewardType.GroupClear:
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardGroupClear);
				this.m_DetailText.text = displayData.m_Text;
				break;
			case RewardPlayer.eRewardType.Complete:
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardCompleteClear);
				this.m_DetailText.text = displayData.m_Text;
				break;
			case RewardPlayer.eRewardType.ADV:
			{
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardADV);
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < displayData.m_ADVReward.m_NamedList.Count; i++)
				{
					if (i > 0)
					{
						stringBuilder.Append("、");
					}
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(displayData.m_ADVReward.m_NamedList[i]).m_NickName);
				}
				this.m_DetailText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardADVMessage, new object[]
				{
					stringBuilder.ToString()
				});
				break;
			}
			}
			for (int j = 0; j < this.m_RewardPanels.Length; j++)
			{
				this.m_RewardPanels[j].gameObject.SetActive(false);
			}
			int num = 0;
			if (displayData.m_Reward != null)
			{
				if (displayData.m_Reward.gold > 0L)
				{
					this.m_RewardPanels[num].gameObject.SetActive(true);
					this.m_RewardPanels[num].ApplyGold(displayData.m_Reward.gold);
					num++;
				}
				if (displayData.m_Reward.gem > 0L)
				{
					this.m_RewardPanels[num].gameObject.SetActive(true);
					this.m_RewardPanels[num].ApplyGem(displayData.m_Reward.gem);
					num++;
				}
				if (displayData.m_Reward.itemId1 != -1)
				{
					this.m_RewardPanels[num].gameObject.SetActive(true);
					this.m_RewardPanels[num].ApplyItem(displayData.m_Reward.itemId1, displayData.m_Reward.amount1);
					num++;
				}
				if (displayData.m_Reward.itemId2 != -1)
				{
					this.m_RewardPanels[num].gameObject.SetActive(true);
					this.m_RewardPanels[num].ApplyItem(displayData.m_Reward.itemId2, displayData.m_Reward.amount2);
					num++;
				}
				if (displayData.m_Reward.itemId3 != -1)
				{
					this.m_RewardPanels[num].gameObject.SetActive(true);
					this.m_RewardPanels[num].ApplyItem(displayData.m_Reward.itemId3, displayData.m_Reward.amount3);
					num++;
				}
			}
			if (displayData.m_ADVReward != null && displayData.m_ADVReward.m_AdvGem > 0)
			{
				this.m_RewardPanels[num].gameObject.SetActive(true);
				this.m_RewardPanels[num].ApplyGem((long)displayData.m_ADVReward.m_AdvGem);
				num++;
			}
			this.m_WindowRect.sizeDelta = new Vector2(this.WINDOW_WIDTH[num - 1], this.m_WindowRect.sizeDelta.y);
		}

		// Token: 0x0600377B RID: 14203 RVA: 0x00119484 File Offset: 0x00117884
		private void Update()
		{
			switch (this.m_Step)
			{
			case RewardUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(RewardUI.eStep.Idle);
				}
				break;
			}
			case RewardUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(RewardUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600377C RID: 14204 RVA: 0x00119580 File Offset: 0x00117980
		private void ChangeStep(RewardUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case RewardUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case RewardUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case RewardUI.eStep.Idle:
				this.m_MainGroup.SetEnableInput(true);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case RewardUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case RewardUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600377D RID: 14205 RVA: 0x00119634 File Offset: 0x00117A34
		public override void PlayIn()
		{
			this.ChangeStep(RewardUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x0600377E RID: 14206 RVA: 0x0011967C File Offset: 0x00117A7C
		public override void PlayOut()
		{
			this.ChangeStep(RewardUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x0600377F RID: 14207 RVA: 0x001196C1 File Offset: 0x00117AC1
		public override bool IsMenuEnd()
		{
			return this.m_Step == RewardUI.eStep.End;
		}

		// Token: 0x06003780 RID: 14208 RVA: 0x001196CC File Offset: 0x00117ACC
		public void OnCloseButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x04003E3E RID: 15934
		public readonly float[] WINDOW_WIDTH = new float[]
		{
			720f,
			720f,
			920f
		};

		// Token: 0x04003E3F RID: 15935
		private RewardUI.eStep m_Step;

		// Token: 0x04003E40 RID: 15936
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003E41 RID: 15937
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003E42 RID: 15938
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04003E43 RID: 15939
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04003E44 RID: 15940
		[SerializeField]
		private RewardPanel[] m_RewardPanels;

		// Token: 0x04003E45 RID: 15941
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x02000A6D RID: 2669
		private enum eStep
		{
			// Token: 0x04003E47 RID: 15943
			Hide,
			// Token: 0x04003E48 RID: 15944
			In,
			// Token: 0x04003E49 RID: 15945
			Idle,
			// Token: 0x04003E4A RID: 15946
			Out,
			// Token: 0x04003E4B RID: 15947
			End
		}
	}
}
