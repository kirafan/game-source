﻿using System;

namespace Star.UI
{
	// Token: 0x02000B38 RID: 2872
	[Serializable]
	public class WebViewWindowTabInformation
	{
		// Token: 0x06003C6B RID: 15467 RVA: 0x00135224 File Offset: 0x00133624
		public WebViewWindowTabInformation(string title, string URL)
		{
			this.m_Title = title;
			this.m_URL = URL;
		}

		// Token: 0x06003C6C RID: 15468 RVA: 0x0013523A File Offset: 0x0013363A
		public WebViewWindowTabInformation()
		{
		}

		// Token: 0x0400443D RID: 17469
		public string m_Title;

		// Token: 0x0400443E RID: 17470
		public string m_URL;
	}
}
