﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200089E RID: 2206
	public class RectCloner : MonoBehaviour
	{
		// Token: 0x06002DCA RID: 11722 RVA: 0x000F11A8 File Offset: 0x000EF5A8
		private void Update()
		{
			if (0 < this.m_Frames)
			{
				this.m_Frames--;
				if (this.m_IsClonePosition)
				{
					this.m_Destination.localPosition = this.m_Source.localPosition;
				}
				if (this.m_IsCloneSize)
				{
					this.m_Destination.sizeDelta = this.m_Source.sizeDelta;
				}
			}
		}

		// Token: 0x06002DCB RID: 11723 RVA: 0x000F1211 File Offset: 0x000EF611
		public void Setup(int frames = 10)
		{
			this.m_Frames = frames;
		}

		// Token: 0x040034CA RID: 13514
		[SerializeField]
		private RectTransform m_Source;

		// Token: 0x040034CB RID: 13515
		[SerializeField]
		private RectTransform m_Destination;

		// Token: 0x040034CC RID: 13516
		[SerializeField]
		private bool m_IsClonePosition;

		// Token: 0x040034CD RID: 13517
		[SerializeField]
		private bool m_IsCloneSize;

		// Token: 0x040034CE RID: 13518
		private int m_Frames;
	}
}
