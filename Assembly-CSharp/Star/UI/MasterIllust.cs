﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000867 RID: 2151
	public class MasterIllust : ASyncImage
	{
		// Token: 0x06002CA5 RID: 11429 RVA: 0x000EBABA File Offset: 0x000E9EBA
		public override bool IsDoneLoad()
		{
			return this.m_SpriteHandler != null && this.m_OrbHandler != null && this.m_SpriteHandler.IsAvailable() && this.m_OrbHandler.IsAvailable();
		}

		// Token: 0x06002CA6 RID: 11430 RVA: 0x000EBAF2 File Offset: 0x000E9EF2
		public override void Start()
		{
			base.Start();
			if (!this.m_IsDoneApply && this.m_OrbImage != null)
			{
				this.m_OrbImage.enabled = false;
			}
		}

		// Token: 0x06002CA7 RID: 11431 RVA: 0x000EBB24 File Offset: 0x000E9F24
		protected override void Apply()
		{
			if (this.m_IsDoneApply && this.m_OrbHandler != null && SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.m_OrbImage.enabled = false;
				this.m_OrbImage.sprite = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_OrbHandler);
			}
			this.m_IsDoneApply = true;
		}

		// Token: 0x06002CA8 RID: 11432 RVA: 0x000EBB9C File Offset: 0x000E9F9C
		public void Apply(int orbID)
		{
			if (this.m_OrbID != orbID)
			{
				this.Apply();
				if (this.m_SpriteHandler == null)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncMasterIllust();
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				if (orbID != -1)
				{
					this.m_OrbHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncMasterOrb(orbID);
					this.m_OrbImage.enabled = false;
					this.m_OrbImage.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_OrbID = orbID;
			}
		}

		// Token: 0x06002CA9 RID: 11433 RVA: 0x000EBC3C File Offset: 0x000EA03C
		public void ApplyCurrentEquip()
		{
			UserMasterOrbData equipUserMasterOrbData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetEquipUserMasterOrbData();
			int orbID = 1;
			if (equipUserMasterOrbData != null)
			{
				orbID = equipUserMasterOrbData.OrbID;
			}
			this.Apply(orbID);
		}

		// Token: 0x06002CAA RID: 11434 RVA: 0x000EBC70 File Offset: 0x000EA070
		public override void LateUpdate()
		{
			if (this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable() && this.m_OrbHandler != null && this.m_OrbHandler.IsAvailable())
			{
				if (this.m_Image.sprite == null)
				{
					this.ApplyLoadedSprite();
				}
				if (this.m_OrbImage.sprite == null)
				{
					this.ApplyLoadedOrbSprite();
				}
			}
		}

		// Token: 0x06002CAB RID: 11435 RVA: 0x000EBCEC File Offset: 0x000EA0EC
		protected void ApplyLoadedOrbSprite()
		{
			if (this.m_OrbImage.sprite == null && this.m_OrbHandler != null && this.m_OrbHandler.IsAvailable())
			{
				this.m_OrbImage.sprite = this.m_OrbHandler.Obj;
				this.m_OrbImage.enabled = true;
			}
		}

		// Token: 0x06002CAC RID: 11436 RVA: 0x000EBD4C File Offset: 0x000EA14C
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x06002CAD RID: 11437 RVA: 0x000EBD54 File Offset: 0x000EA154
		public override void Destroy()
		{
			base.Destroy();
			if (this.m_OrbImage != null)
			{
				this.m_OrbImage.enabled = false;
				this.m_OrbImage.sprite = null;
			}
			if (this.m_OrbHandler != null && SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_OrbHandler);
			}
			this.m_OrbID = -1;
		}

		// Token: 0x040033A1 RID: 13217
		[SerializeField]
		private Image m_OrbImage;

		// Token: 0x040033A2 RID: 13218
		private SpriteHandler m_OrbHandler;

		// Token: 0x040033A3 RID: 13219
		private int m_OrbID = -1;
	}
}
