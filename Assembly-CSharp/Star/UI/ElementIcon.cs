﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000860 RID: 2144
	public class ElementIcon : MonoBehaviour
	{
		// Token: 0x06002C93 RID: 11411 RVA: 0x000EB6DC File Offset: 0x000E9ADC
		public void Apply(eElementType element)
		{
			this.m_IconObj = base.GetComponent<Image>();
			switch (element + 1)
			{
			case eElementType.Fire:
				this.m_IconObj.sprite = this.m_Random;
				break;
			case eElementType.Water:
				this.m_IconObj.sprite = this.m_Fire;
				break;
			case eElementType.Earth:
				this.m_IconObj.sprite = this.m_Water;
				break;
			case eElementType.Wind:
				this.m_IconObj.sprite = this.m_Earth;
				break;
			case eElementType.Moon:
				this.m_IconObj.sprite = this.m_Wind;
				break;
			case eElementType.Sun:
				this.m_IconObj.sprite = this.m_Moon;
				break;
			case eElementType.Num:
				this.m_IconObj.sprite = this.m_Sun;
				break;
			default:
				this.m_IconObj.sprite = null;
				break;
			}
		}

		// Token: 0x06002C94 RID: 11412 RVA: 0x000EB7C9 File Offset: 0x000E9BC9
		public void SetActive(bool flag)
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x0400337F RID: 13183
		[SerializeField]
		private Sprite m_Random;

		// Token: 0x04003380 RID: 13184
		[SerializeField]
		private Sprite m_Fire;

		// Token: 0x04003381 RID: 13185
		[SerializeField]
		private Sprite m_Water;

		// Token: 0x04003382 RID: 13186
		[SerializeField]
		private Sprite m_Earth;

		// Token: 0x04003383 RID: 13187
		[SerializeField]
		private Sprite m_Wind;

		// Token: 0x04003384 RID: 13188
		[SerializeField]
		private Sprite m_Moon;

		// Token: 0x04003385 RID: 13189
		[SerializeField]
		private Sprite m_Sun;

		// Token: 0x04003386 RID: 13190
		private Image m_IconObj;

		// Token: 0x04003387 RID: 13191
		private GameObject m_GameObject;
	}
}
