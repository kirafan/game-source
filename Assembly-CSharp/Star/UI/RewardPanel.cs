﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000A66 RID: 2662
	public class RewardPanel : MonoBehaviour
	{
		// Token: 0x06003764 RID: 14180 RVA: 0x001188C8 File Offset: 0x00116CC8
		public void ApplyItem(int itemID, int num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyItem(itemID);
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID).m_Name;
			this.m_NumText.text = "x" + num.ToString();
		}

		// Token: 0x06003765 RID: 14181 RVA: 0x00118930 File Offset: 0x00116D30
		public void ApplyGem(long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyGem();
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem);
			this.m_NumText.text = "x" + UIUtility.GemValueToString(num, false);
		}

		// Token: 0x06003766 RID: 14182 RVA: 0x00118988 File Offset: 0x00116D88
		public void ApplyGold(long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyGold();
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gold);
			this.m_NumText.text = "x" + UIUtility.GoldValueToString(num, false);
		}

		// Token: 0x06003767 RID: 14183 RVA: 0x001189E0 File Offset: 0x00116DE0
		public void ApplyKRR(long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyKirara();
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.KiraraPoint);
			this.m_NumText.text = "x" + UIUtility.KPValueToString(num, false);
		}

		// Token: 0x06003768 RID: 14184 RVA: 0x00118A38 File Offset: 0x00116E38
		public void ApplyRoomObj(int accesskey, long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyRoomObject(accesskey);
			this.m_NameText.text = RoomObjectListUtil.GetAccessKeyToObjectParam(accesskey).m_Name;
			this.m_NumText.text = "x" + num.ToString();
		}

		// Token: 0x06003769 RID: 14185 RVA: 0x00118A94 File Offset: 0x00116E94
		public void ApplyTownObj(int objID, long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyTownObject(objID, 1);
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_ObjName;
			this.m_NumText.text = "x" + num.ToString();
		}

		// Token: 0x0600376A RID: 14186 RVA: 0x00118B00 File Offset: 0x00116F00
		public void ApplyChara(int charaID)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyChara(charaID);
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_Name;
			this.m_NumText.text = string.Empty;
		}

		// Token: 0x0600376B RID: 14187 RVA: 0x00118B58 File Offset: 0x00116F58
		public void ApplyWeapon(int weaponID, long num)
		{
			this.m_IconCloner.GetInst<VariableIconWithFrame>().ApplyWeapon(weaponID);
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_WeaponName;
			this.m_NumText.text = "x" + num.ToString();
		}

		// Token: 0x04003E1E RID: 15902
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04003E1F RID: 15903
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003E20 RID: 15904
		[SerializeField]
		private Text m_NumText;
	}
}
