﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000821 RID: 2081
	[RequireComponent(typeof(ScrollRect))]
	public class ScrollViewBase : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x06002B54 RID: 11092 RVA: 0x000C42CB File Offset: 0x000C26CB
		public List<ScrollItemData> GetDataList()
		{
			return this.m_ItemDataList;
		}

		// Token: 0x06002B55 RID: 11093 RVA: 0x000C42D4 File Offset: 0x000C26D4
		public virtual void Setup()
		{
			this.m_ItemWidth = this.m_ItemIconPrefab.GetComponent<RectTransform>().rect.width;
			this.m_ItemHeight = this.m_ItemIconPrefab.GetComponent<RectTransform>().rect.height;
			this.m_ItemDataList = new List<ScrollItemData>();
			this.m_DispItemList = new List<ScrollViewBase.DispItem>();
			this.m_ActiveDispItemList = new List<ScrollViewBase.DispItem>();
			this.m_EmptyDispItemList = new List<ScrollViewBase.DispItem>();
			this.m_ScrollRect = base.GetComponent<ScrollRect>();
			this.m_ScrollRectTransform = this.m_ScrollRect.GetComponent<RectTransform>();
			this.m_Content = this.m_ScrollRect.content;
			this.m_ScrollRect.onValueChanged.AddListener(new UnityAction<Vector2>(this.OnScrollRectValueChanged));
			this.m_ItemDataList.Clear();
			if (this.m_VerticalBar != null)
			{
				this.m_VerticalBar.gameObject.SetActive(false);
				this.m_VerticalBar.value = 1f;
				this.m_VerticalBar.onValueChanged.AddListener(new UnityAction<float>(this.OnVerticalScrollBarValueChanged));
			}
			this.SetScrollNormalizedPosition(1f);
			this.m_CurrentScrollX = -1;
			this.m_CurrentScrollY = -1;
			this.CalcViewItemMax();
			this.m_IsDirty = true;
		}

		// Token: 0x06002B56 RID: 11094 RVA: 0x000C4414 File Offset: 0x000C2814
		protected virtual void RebuildScroll()
		{
			this.InstantiateItem();
			int num = (this.m_ItemDataList.Count + this.m_ViewItemStartIdx) / this.m_DispItemMaxX;
			if ((this.m_ItemDataList.Count + this.m_ViewItemStartIdx) % this.m_DispItemMaxX > 0)
			{
				num++;
			}
			this.m_Content.sizeDelta = new Vector2(this.m_Content.sizeDelta.x, (float)num * (this.m_ItemHeight + this.m_SpaceY) + this.m_MarginY * 2f - this.m_SpaceY);
			this.m_ScrollMaxVertical = this.m_Content.rect.height - this.m_ScrollRect.viewport.rect.height;
			if (this.m_VerticalBar != null)
			{
				if (this.m_Content.rect.height < this.m_ScrollRect.viewport.rect.height)
				{
					this.m_VerticalBar.gameObject.SetActive(false);
					this.m_ScrollRect.viewport.anchorMin = new Vector2(0f, 0f);
					this.m_ScrollRect.viewport.anchorMax = new Vector2(1f, 1f);
					this.m_ScrollRect.viewport.sizeDelta = new Vector2(0f, 0f);
					this.m_ScrollRect.viewport.anchoredPosition = Vector2.zero;
				}
				else
				{
					this.m_VerticalBar.gameObject.SetActive(true);
					this.m_ScrollRect.viewport.anchorMin = new Vector2(0f, 0f);
					this.m_ScrollRect.viewport.anchorMax = new Vector2(1f, 1f);
					this.m_ScrollRect.viewport.sizeDelta = new Vector2(-this.m_VerticalBar.GetComponent<RectTransform>().sizeDelta.x, 0f);
					this.m_ScrollRect.viewport.anchoredPosition = Vector2.zero;
				}
			}
			if (this.m_EmptyObj != null)
			{
				if (this.m_ItemDataList.Count + this.m_ViewItemStartIdx <= 0 && !this.m_EmptyObj.activeSelf)
				{
					this.m_EmptyObj.SetActive(true);
				}
				else if (this.m_ItemDataList.Count + this.m_ViewItemStartIdx > 0 && this.m_EmptyObj.activeSelf)
				{
					this.m_EmptyObj.SetActive(false);
				}
			}
			this.m_IsDirty = false;
		}

		// Token: 0x06002B57 RID: 11095 RVA: 0x000C46C8 File Offset: 0x000C2AC8
		public virtual void LateUpdate()
		{
			if (this.m_IsDirty)
			{
				this.RebuildScroll();
				this.UpdateItemPosition();
			}
			if (this.m_ReserveSetIdx != -1)
			{
				this.SetScrollValueFromIdx(this.m_ReserveSetIdx);
			}
			if (this.m_Content != null && this.m_VerticalBar != null && this.m_Content.rect.height > 0f)
			{
				float num = this.m_ScrollRect.viewport.rect.height;
				if (this.m_Content.anchoredPosition.y < 0f)
				{
					num += this.m_ScrollRect.content.anchoredPosition.y;
				}
				else if (this.m_ScrollRect.content.anchoredPosition.y > this.m_ScrollRect.content.rect.height - this.m_ScrollRect.viewport.rect.height)
				{
					num -= this.m_ScrollRect.content.anchoredPosition.y - (this.m_ScrollRect.content.rect.height - this.m_ScrollRect.viewport.rect.height);
				}
				this.m_VerticalBar.size = num / this.m_Content.rect.height;
				RectTransform component = this.m_VerticalBar.GetComponent<RectTransform>();
				if (component.rect.height * this.m_VerticalBar.size < 32f)
				{
					this.m_VerticalBar.size = 32f / component.rect.height;
				}
			}
			if (this.m_ScrollRect == null)
			{
				this.m_ScrollRect = base.GetComponent<ScrollRect>();
			}
		}

		// Token: 0x06002B58 RID: 11096 RVA: 0x000C48CD File Offset: 0x000C2CCD
		public virtual void AddItem(ScrollItemData item)
		{
			this.m_ItemDataList.Add(item);
			this.m_IsDirty = true;
		}

		// Token: 0x06002B59 RID: 11097 RVA: 0x000C48E2 File Offset: 0x000C2CE2
		public ScrollItemIcon GetItemInst(int idx)
		{
			if (this.m_DispItemList.Count <= idx)
			{
				return null;
			}
			return this.m_DispItemList[idx].m_ItemIconObject;
		}

		// Token: 0x06002B5A RID: 11098 RVA: 0x000C4908 File Offset: 0x000C2D08
		public virtual void RemoveAll()
		{
			this.m_ScrollMaxVertical = 0f;
			if (this.m_DispItemList != null)
			{
				for (int i = 0; i < this.m_DispItemList.Count; i++)
				{
					this.m_DispItemList[i].m_ItemIconObject.Destroy();
					UnityEngine.Object.Destroy(this.m_DispItemList[i].m_ItemIconObject.gameObject);
				}
			}
			this.m_ItemDataList = new List<ScrollItemData>();
			this.m_DispItemList = new List<ScrollViewBase.DispItem>();
			this.m_EmptyDispItemList = new List<ScrollViewBase.DispItem>();
			this.m_ActiveDispItemList = new List<ScrollViewBase.DispItem>();
			this.m_ItemDataList.Clear();
			this.SetScrollNormalizedPosition(1f);
			this.RebuildScroll();
			this.m_CurrentScrollX = -1;
			this.m_CurrentScrollY = -1;
		}

		// Token: 0x06002B5B RID: 11099 RVA: 0x000C49CE File Offset: 0x000C2DCE
		public void RemoveAllData()
		{
			this.m_ScrollMaxVertical = 0f;
			this.m_ScrollRect.velocity = Vector2.zero;
			this.SetScrollNormalizedPosition(0f);
			this.m_ItemDataList.Clear();
			this.m_IsDirty = true;
			this.Refresh();
		}

		// Token: 0x06002B5C RID: 11100 RVA: 0x000C4A10 File Offset: 0x000C2E10
		public void Refresh()
		{
			for (int i = 0; i < this.m_EmptyDispItemList.Count; i++)
			{
				this.m_EmptyDispItemList[i].m_ItemIconObject.GameObject.SetActive(false);
			}
			for (int j = 0; j < this.m_ActiveDispItemList.Count; j++)
			{
				this.m_ActiveDispItemList[j].m_Visible = false;
				this.m_EmptyDispItemList.Add(this.m_ActiveDispItemList[j]);
				this.m_ActiveDispItemList[j].m_ItemIconObject.gameObject.SetActive(false);
			}
			this.m_ActiveDispItemList.Clear();
			this.m_CurrentScrollX = -1;
			this.m_CurrentScrollY = -1;
			this.m_IsDirty = true;
		}

		// Token: 0x06002B5D RID: 11101 RVA: 0x000C4AD8 File Offset: 0x000C2ED8
		public void ForceApply()
		{
			for (int i = 0; i < this.m_DispItemList.Count; i++)
			{
				if (this.m_DispItemList[i].m_Visible)
				{
					this.m_DispItemList[i].m_ItemIconObject.Refresh();
				}
			}
		}

		// Token: 0x06002B5E RID: 11102 RVA: 0x000C4B34 File Offset: 0x000C2F34
		protected void OnVerticalScrollBarValueChanged(float value)
		{
			if (this.m_ItemDataList.Count <= 0)
			{
				return;
			}
			if (this.m_ScrollMaxVertical <= 0f)
			{
				return;
			}
			if (this.m_VerticalBar != null)
			{
				this.m_Content.localPosY((1f - value) * this.m_ScrollMaxVertical);
			}
			this.UpdateItemPosition();
		}

		// Token: 0x06002B5F RID: 11103 RVA: 0x000C4B94 File Offset: 0x000C2F94
		public void OnScrollRectValueChanged(Vector2 scrollPosition)
		{
			if (this.m_ItemDataList.Count <= 0)
			{
				return;
			}
			if (this.m_ScrollMaxVertical <= 0f)
			{
				return;
			}
			if (this.m_VerticalBar != null)
			{
				this.m_VerticalBar.value = 1f - this.m_Content.localPosition.y / this.m_ScrollMaxVertical;
			}
			this.UpdateItemPosition();
		}

		// Token: 0x06002B60 RID: 11104 RVA: 0x000C4C08 File Offset: 0x000C3008
		public virtual void SetScrollNormalizedPosition(float position)
		{
			if (this.m_IsDirty)
			{
				this.RebuildScroll();
			}
			if (this.m_ScrollMaxVertical <= 0f)
			{
				position = 1f;
			}
			this.m_ScrollRect.verticalNormalizedPosition = position;
			if (this.m_VerticalBar != null)
			{
				this.m_VerticalBar.value = position;
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06002B61 RID: 11105 RVA: 0x000C4C70 File Offset: 0x000C3070
		public void SetScrollValue(float value)
		{
			if (this.m_IsDirty)
			{
				this.RebuildScroll();
			}
			if (this.m_ScrollMaxVertical > 0f)
			{
				value = Mathf.Clamp(value, 0f, this.m_ScrollMaxVertical);
			}
			this.m_Content.anchoredPosition = new Vector2(0f, value);
			this.m_VerticalBar.value = 1f - this.m_Content.localPosition.y / this.m_ScrollMaxVertical;
			this.m_IsDirty = true;
		}

		// Token: 0x06002B62 RID: 11106 RVA: 0x000C4CFC File Offset: 0x000C30FC
		public void SetScrollValueFromIdx(int idx)
		{
			if (this.m_ScrollMaxVertical <= 0f)
			{
				this.m_ReserveSetIdx = idx;
				return;
			}
			if (this.m_IsDirty)
			{
				this.RebuildScroll();
			}
			float scrollValue = Mathf.Clamp((float)idx * (this.m_ItemHeight + this.m_SpaceY) + this.m_MarginY, 0f, this.m_ScrollMaxVertical);
			this.SetScrollValue(scrollValue);
			this.m_ReserveSetIdx = -1;
		}

		// Token: 0x06002B63 RID: 11107 RVA: 0x000C4D68 File Offset: 0x000C3168
		public float GetScrollValue()
		{
			return this.m_Content.localPosition.y;
		}

		// Token: 0x06002B64 RID: 11108 RVA: 0x000C4D88 File Offset: 0x000C3188
		protected void CalcViewItemMax()
		{
			float width = this.m_ScrollRect.viewport.rect.width;
			float height = this.m_ScrollRect.viewport.rect.height;
			if (height <= 0f)
			{
				return;
			}
			this.m_DispItemMaxY = (int)(height / (this.m_ItemHeight + this.m_SpaceY)) + 2;
			this.m_DispItemMax = this.m_DispItemMaxX * this.m_DispItemMaxY;
			this.m_MarginX = (this.m_Content.rect.width - (this.m_ItemWidth + this.m_SpaceX) * (float)this.m_DispItemMaxX) * 0.5f;
		}

		// Token: 0x06002B65 RID: 11109 RVA: 0x000C4E34 File Offset: 0x000C3234
		protected virtual void InstantiateItem()
		{
			while (this.m_DispItemList.Count < this.m_DispItemMax && this.m_DispItemList.Count < this.m_ItemDataList.Count)
			{
				ScrollViewBase.DispItem dispItem = new ScrollViewBase.DispItem();
				dispItem.m_Idx = this.m_ItemDataList.Count - 1;
				dispItem.m_Visible = false;
				dispItem.m_PositionX = (this.m_ItemDataList.Count - 1) % this.m_DispItemMaxX;
				dispItem.m_PositionY = (this.m_ItemDataList.Count - 1) / this.m_DispItemMaxY;
				ScrollItemIcon scrollItemIcon = UnityEngine.Object.Instantiate<ScrollItemIcon>(this.m_ItemIconPrefab, this.m_Content);
				scrollItemIcon.Scroll = this;
				scrollItemIcon.RectTransform.pivot = new Vector2(0f, 1f);
				scrollItemIcon.RectTransform.anchorMin = new Vector2(0f, 1f);
				scrollItemIcon.RectTransform.anchorMax = new Vector2(0f, 1f);
				scrollItemIcon.RectTransform.localScale = Vector3.one;
				ScrollItemIcon scrollItemIcon2 = scrollItemIcon;
				scrollItemIcon2.OnClickIcon = (Action<ScrollItemIcon>)Delegate.Combine(scrollItemIcon2.OnClickIcon, new Action<ScrollItemIcon>(this.OnClickIconCallBack));
				scrollItemIcon.OnHoldIcon += this.OnHoldIconCallBack;
				scrollItemIcon.GameObject.SetActive(false);
				dispItem.m_ItemIconObject = scrollItemIcon;
				this.m_DispItemList.Add(dispItem);
				this.m_EmptyDispItemList.Add(dispItem);
			}
		}

		// Token: 0x06002B66 RID: 11110 RVA: 0x000C4FA3 File Offset: 0x000C33A3
		protected virtual void OnClickIconCallBack(ScrollItemIcon icon)
		{
		}

		// Token: 0x06002B67 RID: 11111 RVA: 0x000C4FA5 File Offset: 0x000C33A5
		protected virtual void OnHoldIconCallBack(ScrollItemIcon icon)
		{
		}

		// Token: 0x06002B68 RID: 11112 RVA: 0x000C4FA8 File Offset: 0x000C33A8
		protected virtual void UpdateItemPosition()
		{
			Vector2 anchoredPosition = this.m_Content.anchoredPosition;
			this.m_MarginX = (this.m_Content.rect.width - (this.m_ItemWidth + this.m_SpaceX) * (float)this.m_DispItemMaxX + this.m_SpaceX) * 0.5f;
			int num = (int)((anchoredPosition.x - this.m_MarginX) / (this.m_ItemWidth + this.m_SpaceX));
			int num2 = (int)((anchoredPosition.y - this.m_MarginY * 0.5f) / (this.m_ItemHeight + this.m_SpaceY));
			if (num < 0)
			{
				num = 0;
			}
			if (num2 < 0)
			{
				num2 = 0;
			}
			if (this.m_CurrentScrollX == num && this.m_CurrentScrollY == num2)
			{
				return;
			}
			this.m_CurrentScrollX = num;
			this.m_CurrentScrollY = num2;
			while (this.m_ActiveDispItemList.Count > 0)
			{
				int index = 0;
				if (!this.CheckOutOfView(this.m_ActiveDispItemList[index]))
				{
					break;
				}
				this.HideItem(this.m_ActiveDispItemList[index]);
				this.m_EmptyDispItemList.Add(this.m_ActiveDispItemList[index]);
				this.m_ActiveDispItemList.RemoveAt(index);
			}
			while (this.m_ActiveDispItemList.Count > 0)
			{
				int index2 = this.m_ActiveDispItemList.Count - 1;
				if (!this.CheckOutOfView(this.m_ActiveDispItemList[index2]))
				{
					break;
				}
				this.HideItem(this.m_ActiveDispItemList[index2]);
				this.m_EmptyDispItemList.Add(this.m_ActiveDispItemList[index2]);
				this.m_ActiveDispItemList.RemoveAt(index2);
			}
			for (int i = 0; i < this.m_DispItemMaxY; i++)
			{
				for (int j = 0; j < this.m_DispItemMaxX; j++)
				{
					if (this.m_DispItemMaxX * (this.m_CurrentScrollY + i) + (this.m_CurrentScrollX + j) >= this.m_ViewItemStartIdx)
					{
						if (this.m_CurrentScrollX + j >= 0 && this.m_CurrentScrollX + j < this.m_DispItemMaxX && num2 + i >= 0 && this.m_ItemDataList.Count + this.m_ViewItemStartIdx > (this.m_CurrentScrollY + i) * this.m_DispItemMaxX + (this.m_CurrentScrollX + j))
						{
							int num3 = this.m_ActiveDispItemList.Count;
							for (int k = 0; k < this.m_ActiveDispItemList.Count; k++)
							{
								if (this.m_ActiveDispItemList[k].m_Visible)
								{
									if (this.m_ActiveDispItemList[k].m_PositionX == this.m_CurrentScrollX + j && this.m_ActiveDispItemList[k].m_PositionY == this.m_CurrentScrollY + i)
									{
										num3 = -1;
										break;
									}
									if (this.m_ActiveDispItemList[k].m_PositionY > this.m_CurrentScrollY + i)
									{
										num3 = k;
										break;
									}
									if (this.m_ActiveDispItemList[k].m_PositionX >= this.m_CurrentScrollX + j && this.m_ActiveDispItemList[k].m_PositionY == this.m_CurrentScrollY + i)
									{
										num3 = k;
										break;
									}
								}
							}
							if (num3 > -1)
							{
								int num4 = this.m_CurrentScrollX + j;
								int num5 = this.m_CurrentScrollY + i;
								int num6 = this.m_DispItemMaxX * num5 + num4 - this.m_ViewItemStartIdx;
								if (this.m_EmptyDispItemList.Count > 0)
								{
									this.m_EmptyDispItemList[0].m_PositionX = num4;
									this.m_EmptyDispItemList[0].m_PositionY = num5;
									this.m_EmptyDispItemList[0].m_Idx = num6;
									this.ShowItem(this.m_EmptyDispItemList[0]);
									this.m_EmptyDispItemList[0].m_ItemIconObject.RectTransform.anchoredPosition = new Vector2((float)num4 * (this.m_ItemWidth + this.m_SpaceX) + this.m_MarginX, -(float)num5 * (this.m_ItemHeight + this.m_SpaceY) - this.m_MarginY);
									this.m_EmptyDispItemList[0].m_ItemIconObject.RectTransform.localScale = Vector3.one;
									this.m_EmptyDispItemList[0].m_ItemIconObject.SetItemData(this.m_ItemDataList[num6]);
									if (num3 < this.m_ActiveDispItemList.Count)
									{
										this.m_ActiveDispItemList.Insert(num3, this.m_EmptyDispItemList[0]);
									}
									else
									{
										this.m_ActiveDispItemList.Add(this.m_EmptyDispItemList[0]);
									}
									this.m_EmptyDispItemList.RemoveAt(0);
								}
								else
								{
									Debug.LogWarning(string.Concat(new object[]
									{
										"Item Object Pool is empty m_EmptyDispItemList.Count = ",
										this.m_EmptyDispItemList.Count,
										"m_ActiveDispItemList.Count = ",
										this.m_ActiveDispItemList.Count
									}));
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06002B69 RID: 11113 RVA: 0x000C54F4 File Offset: 0x000C38F4
		protected bool CheckOutOfView(ScrollViewBase.DispItem item)
		{
			return !item.m_Visible || (item.m_PositionX < this.m_CurrentScrollX || item.m_PositionX >= this.m_CurrentScrollX + this.m_DispItemMaxX || item.m_PositionY < this.m_CurrentScrollY || item.m_PositionY >= this.m_CurrentScrollY + this.m_DispItemMaxY) || (item.m_PositionX < 0 || item.m_PositionX >= this.m_DispItemMaxX || item.m_PositionY < 0 || this.m_ItemDataList.Count + this.m_ViewItemStartIdx <= item.m_PositionY * this.m_DispItemMaxX + item.m_PositionX);
		}

		// Token: 0x06002B6A RID: 11114 RVA: 0x000C55B9 File Offset: 0x000C39B9
		protected virtual void HideItem(ScrollViewBase.DispItem item)
		{
			item.m_Visible = false;
			item.m_ItemIconObject.gameObject.SetActive(false);
		}

		// Token: 0x06002B6B RID: 11115 RVA: 0x000C55D3 File Offset: 0x000C39D3
		protected virtual void ShowItem(ScrollViewBase.DispItem item)
		{
			item.m_Visible = true;
			if (!item.m_ItemIconObject.GameObject.activeSelf)
			{
				item.m_ItemIconObject.GameObject.SetActive(true);
			}
			item.m_Visible = true;
		}

		// Token: 0x06002B6C RID: 11116 RVA: 0x000C5609 File Offset: 0x000C3A09
		public virtual void Destroy()
		{
			this.RemoveAll();
		}

		// Token: 0x06002B6D RID: 11117 RVA: 0x000C5611 File Offset: 0x000C3A11
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06002B6E RID: 11118 RVA: 0x000C5614 File Offset: 0x000C3A14
		public void OnDrag(PointerEventData eventData)
		{
			if (this.m_ScrollRect.vertical)
			{
				RectTransform component = base.GetComponent<RectTransform>();
				Vector2 a;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.position, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out a);
				Vector2 b;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.pressPosition, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out b);
				if ((a - b).sqrMagnitude > 1024f)
				{
					CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].ForceEnableHold(false);
					}
				}
			}
		}

		// Token: 0x06002B6F RID: 11119 RVA: 0x000C56B0 File Offset: 0x000C3AB0
		public void OnEndDrag(PointerEventData eventData)
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].ForceRelease();
			}
		}

		// Token: 0x040031AF RID: 12719
		private const float VERTICAL_SCROLLBAR_MINHEIGHT = 32f;

		// Token: 0x040031B0 RID: 12720
		protected List<ScrollItemData> m_ItemDataList;

		// Token: 0x040031B1 RID: 12721
		protected List<ScrollViewBase.DispItem> m_DispItemList;

		// Token: 0x040031B2 RID: 12722
		protected List<ScrollViewBase.DispItem> m_ActiveDispItemList;

		// Token: 0x040031B3 RID: 12723
		protected List<ScrollViewBase.DispItem> m_EmptyDispItemList;

		// Token: 0x040031B4 RID: 12724
		protected int m_ViewItemStartIdx;

		// Token: 0x040031B5 RID: 12725
		protected bool m_IsDirty;

		// Token: 0x040031B6 RID: 12726
		protected float m_ScrollMaxVertical;

		// Token: 0x040031B7 RID: 12727
		[SerializeField]
		[Tooltip("対応するスクロールバー")]
		protected Scrollbar m_VerticalBar;

		// Token: 0x040031B8 RID: 12728
		[SerializeField]
		[Tooltip("一行に並ぶアイテム数")]
		protected int m_DispItemMaxX = 1;

		// Token: 0x040031B9 RID: 12729
		protected int m_DispItemMaxY;

		// Token: 0x040031BA RID: 12730
		protected int m_DispItemMax;

		// Token: 0x040031BB RID: 12731
		[SerializeField]
		[Tooltip("余白")]
		protected float m_MarginY;

		// Token: 0x040031BC RID: 12732
		protected float m_MarginX;

		// Token: 0x040031BD RID: 12733
		[SerializeField]
		protected float m_SpaceX;

		// Token: 0x040031BE RID: 12734
		[SerializeField]
		[Tooltip("間隔")]
		protected float m_SpaceY;

		// Token: 0x040031BF RID: 12735
		[SerializeField]
		protected ScrollItemIcon m_ItemIconPrefab;

		// Token: 0x040031C0 RID: 12736
		[SerializeField]
		[Tooltip("項目がない場合にアクティブになる")]
		protected GameObject m_EmptyObj;

		// Token: 0x040031C1 RID: 12737
		protected ScrollRect m_ScrollRect;

		// Token: 0x040031C2 RID: 12738
		protected RectTransform m_ScrollRectTransform;

		// Token: 0x040031C3 RID: 12739
		protected RectTransform m_Content;

		// Token: 0x040031C4 RID: 12740
		protected float m_ItemWidth;

		// Token: 0x040031C5 RID: 12741
		protected float m_ItemHeight;

		// Token: 0x040031C6 RID: 12742
		protected int m_CurrentScrollX = -1;

		// Token: 0x040031C7 RID: 12743
		protected int m_CurrentScrollY = -1;

		// Token: 0x040031C8 RID: 12744
		private int m_ReserveSetIdx = -1;

		// Token: 0x02000822 RID: 2082
		protected class DispItem
		{
			// Token: 0x040031C9 RID: 12745
			public int m_Idx;

			// Token: 0x040031CA RID: 12746
			public int m_PositionX;

			// Token: 0x040031CB RID: 12747
			public int m_PositionY;

			// Token: 0x040031CC RID: 12748
			public ScrollItemIcon m_ItemIconObject;

			// Token: 0x040031CD RID: 12749
			public bool m_Visible;
		}
	}
}
