﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008CC RID: 2252
	public class iTweenMoveAddWrapper : iTweenWrapper
	{
		// Token: 0x06002EB0 RID: 11952 RVA: 0x000F4EF9 File Offset: 0x000F32F9
		protected override void PlayProcess(GameObject target, Hashtable args)
		{
			iTween.MoveAdd(target, args);
		}

		// Token: 0x06002EB1 RID: 11953 RVA: 0x000F4F02 File Offset: 0x000F3302
		public override void Stop()
		{
			if (base.IsPlaying())
			{
				iTween.Stop(this.m_Target, "move");
				base.Stop();
			}
		}
	}
}
