﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200095A RID: 2394
	public abstract class EnumerationCharaPanelBase
	{
		// Token: 0x060031A6 RID: 12710 RVA: 0x000FE6C4 File Offset: 0x000FCAC4
		public static WeaponIconWithFrame.eMode ConvertEnumerationCharaPanelBaseeModeToWeaponIconWithFrameeMode(EnumerationCharaPanelBase.eMode mode)
		{
			switch (mode)
			{
			case EnumerationCharaPanelBase.eMode.Edit:
				return WeaponIconWithFrame.eMode.Add;
			case EnumerationCharaPanelBase.eMode.Quest:
				return WeaponIconWithFrame.eMode.Add;
			case EnumerationCharaPanelBase.eMode.QuestStart:
				return WeaponIconWithFrame.eMode.NakedBGOnly;
			case EnumerationCharaPanelBase.eMode.View:
				return WeaponIconWithFrame.eMode.Disable;
			case EnumerationCharaPanelBase.eMode.PartyDetail:
				return WeaponIconWithFrame.eMode.Normal;
			default:
				return WeaponIconWithFrame.eMode.Normal;
			}
		}

		// Token: 0x060031A7 RID: 12711 RVA: 0x000FE6F0 File Offset: 0x000FCAF0
		public virtual void Setup(EnumerationCharaPanelBase.SharedInstance sharedInstance)
		{
		}

		// Token: 0x060031A8 RID: 12712
		public abstract void Destroy();

		// Token: 0x060031A9 RID: 12713 RVA: 0x000FE6F2 File Offset: 0x000FCAF2
		protected EnumerationCharaPanelBase.eMode GetMode()
		{
			return this.m_Mode;
		}

		// Token: 0x060031AA RID: 12714 RVA: 0x000FE6FA File Offset: 0x000FCAFA
		public void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
			this.m_Mode = mode;
		}

		// Token: 0x04003806 RID: 14342
		private EnumerationCharaPanelBase.eMode m_Mode;

		// Token: 0x0200095B RID: 2395
		public enum eMode
		{
			// Token: 0x04003808 RID: 14344
			Edit,
			// Token: 0x04003809 RID: 14345
			Quest,
			// Token: 0x0400380A RID: 14346
			QuestStart,
			// Token: 0x0400380B RID: 14347
			View,
			// Token: 0x0400380C RID: 14348
			PartyDetail
		}

		// Token: 0x0200095C RID: 2396
		public enum eButton
		{
			// Token: 0x0400380E RID: 14350
			Chara,
			// Token: 0x0400380F RID: 14351
			Weapon
		}

		// Token: 0x0200095D RID: 2397
		[Serializable]
		public class SharedInstance
		{
			// Token: 0x060031AB RID: 12715 RVA: 0x000FE703 File Offset: 0x000FCB03
			public SharedInstance()
			{
			}

			// Token: 0x060031AC RID: 12716 RVA: 0x000FE70C File Offset: 0x000FCB0C
			public SharedInstance(EnumerationCharaPanelBase.SharedInstance argument)
			{
				this.m_DataContent = argument.m_DataContent;
				this.m_CharaInfo = argument.m_CharaInfo;
				this.m_Blank = argument.m_Blank;
				this.m_EmptyObj = argument.m_EmptyObj;
				this.m_FriendObj = argument.m_FriendObj;
			}

			// Token: 0x04003810 RID: 14352
			[SerializeField]
			public GameObject m_DataContent;

			// Token: 0x04003811 RID: 14353
			[SerializeField]
			[Tooltip("キャラ情報部.")]
			public EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

			// Token: 0x04003812 RID: 14354
			[SerializeField]
			public GameObject m_Blank;

			// Token: 0x04003813 RID: 14355
			[SerializeField]
			public GameObject m_EmptyObj;

			// Token: 0x04003814 RID: 14356
			[SerializeField]
			public GameObject m_FriendObj;
		}

		// Token: 0x0200095E RID: 2398
		[Serializable]
		public abstract class SharedInstanceExGen<ParentType> : EnumerationCharaPanelBase.SharedInstance where ParentType : MonoBehaviour
		{
			// Token: 0x060031AD RID: 12717 RVA: 0x000FE75B File Offset: 0x000FCB5B
			public SharedInstanceExGen()
			{
			}

			// Token: 0x060031AE RID: 12718 RVA: 0x000FE763 File Offset: 0x000FCB63
			public SharedInstanceExGen(EnumerationCharaPanelBase.SharedInstance argument) : base(argument)
			{
			}

			// Token: 0x04003815 RID: 14357
			public ParentType parent;
		}
	}
}
