﻿using System;

namespace Star.UI
{
	// Token: 0x020009CA RID: 2506
	public class Range
	{
		// Token: 0x06003412 RID: 13330 RVA: 0x00108295 File Offset: 0x00106695
		public Range(float in_now, float in_max)
		{
			this.now = in_now;
			this.max = in_max;
		}

		// Token: 0x04003A57 RID: 14935
		public float now;

		// Token: 0x04003A58 RID: 14936
		public float max;
	}
}
