﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000899 RID: 2201
	public class PrefabCloner : MonoBehaviour
	{
		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06002DAE RID: 11694 RVA: 0x000F0BF4 File Offset: 0x000EEFF4
		public GameObject Inst
		{
			get
			{
				if (this.m_CloneInstance == null)
				{
					this.Setup(true);
				}
				return this.m_CloneInstance;
			}
		}

		// Token: 0x06002DAF RID: 11695 RVA: 0x000F0C14 File Offset: 0x000EF014
		public T GetInst<T>() where T : MonoBehaviour
		{
			return this.Inst.GetComponent<T>();
		}

		// Token: 0x06002DB0 RID: 11696 RVA: 0x000F0C21 File Offset: 0x000EF021
		public bool IsExistPrefab()
		{
			return this.m_Prefab != null;
		}

		// Token: 0x06002DB1 RID: 11697 RVA: 0x000F0C30 File Offset: 0x000EF030
		public void Setup(bool force = true)
		{
			if (this.m_CloneInstance != null)
			{
				if (!force)
				{
					return;
				}
				UnityEngine.Object.Destroy(this.m_CloneInstance);
			}
			RectTransform component = base.GetComponent<RectTransform>();
			this.m_CloneInstance = UnityEngine.Object.Instantiate<GameObject>(this.m_Prefab, component);
			RectTransform component2 = this.m_CloneInstance.GetComponent<RectTransform>();
			if (this.m_FitInstanceRect)
			{
				component2.anchorMin = new Vector2(0f, 0f);
				component2.anchorMax = new Vector2(1f, 1f);
				component2.sizeDelta = new Vector2(0f, 0f);
			}
			component2.anchoredPosition = Vector3.zero;
			component2.localScale = Vector3.one;
			PrefabCloner[] componentsInChildren = this.m_CloneInstance.GetComponentsInChildren<PrefabCloner>();
			for (int i = 1; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].Setup(true);
			}
		}

		// Token: 0x040034B8 RID: 13496
		[SerializeField]
		private GameObject m_Prefab;

		// Token: 0x040034B9 RID: 13497
		[SerializeField]
		[Tooltip("生成したInstのサイズを自身にあわせる")]
		private bool m_FitInstanceRect = true;

		// Token: 0x040034BA RID: 13498
		private GameObject m_CloneInstance;
	}
}
