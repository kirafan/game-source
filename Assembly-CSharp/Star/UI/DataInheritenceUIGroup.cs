﻿using System;
using Star.UI.Window;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200088F RID: 2191
	public class DataInheritenceUIGroup : UIGroup
	{
		// Token: 0x06002D74 RID: 11636 RVA: 0x000F0038 File Offset: 0x000EE438
		public override void Open()
		{
			this.m_DataInheritenceSelectWindow.Setup(false);
			this.m_DataInheritenceSelectWindow.SetCallback(new Action(this.OnClickALButtonPlatform), new Action(this.OnClickALButtonUseIDAndPassword), new Action(this.OnClickCloseButton));
			this.SetMode(DataInheritenceUIGroup.eMode.Open);
		}

		// Token: 0x06002D75 RID: 11637 RVA: 0x000F0087 File Offset: 0x000EE487
		public override void Close()
		{
			if (this.m_CustomWindow == this.m_DataInheritenceWindow)
			{
				this.m_DataInheritenceWindow.Close();
				this.SetMode(DataInheritenceUIGroup.eMode.Open);
			}
			else
			{
				base.Close();
				this.m_Mode = DataInheritenceUIGroup.eMode.Close;
			}
		}

		// Token: 0x06002D76 RID: 11638 RVA: 0x000F00C3 File Offset: 0x000EE4C3
		public void SetOnClickButtonCallback(Action<int> callback)
		{
			if (this.m_Content != null)
			{
				this.m_Content.SetOnClickButtonCallback(callback);
			}
		}

		// Token: 0x06002D77 RID: 11639 RVA: 0x000F00E2 File Offset: 0x000EE4E2
		public void SetOnPlayerMoveSuccessCallback(Action callback)
		{
			this.m_RestartCallback = callback;
		}

		// Token: 0x06002D78 RID: 11640 RVA: 0x000F00EC File Offset: 0x000EE4EC
		public override void Update()
		{
			base.Update();
			switch (this.m_Mode)
			{
			case DataInheritenceUIGroup.eMode.Open:
				if (!this.m_DataInheritenceSelectWindow.IsPlayingAnim())
				{
					this.SetMode(DataInheritenceUIGroup.eMode.MainSelect);
				}
				break;
			case DataInheritenceUIGroup.eMode.AccountLinkWait:
				if (AccountLinker.GetState() != AccountLinker.eResult.Access)
				{
					this.SetMode(DataInheritenceUIGroup.eMode.AccountLinkEndDialog);
				}
				break;
			}
		}

		// Token: 0x06002D79 RID: 11641 RVA: 0x000F0178 File Offset: 0x000EE578
		private void SetMode(DataInheritenceUIGroup.eMode mode)
		{
			Debug.Log("IMODE : " + mode);
			this.m_Mode = mode;
			switch (this.m_Mode)
			{
			case DataInheritenceUIGroup.eMode.Open:
				this.m_CustomWindow = this.m_DataInheritenceSelectWindow;
				base.Open();
				if (this.m_Content != null)
				{
					this.m_Content.Setup();
					this.m_Content.SetWindowType(this.m_InheritenceWindowType);
				}
				break;
			case DataInheritenceUIGroup.eMode.AccountLinkNotifyDialog:
			{
				DataInheritenceWindowContent.eType inheritenceWindowType = this.m_InheritenceWindowType;
				if (inheritenceWindowType != DataInheritenceWindowContent.eType.ConfigurationWindow)
				{
					if (inheritenceWindowType == DataInheritenceWindowContent.eType.ExecuteWindow)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.PlayerMoveConfirmTitle, eText_MessageDB.PlayerMoveConfirm_Android, new Action<int>(this.OnClickNotifyConfirm));
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.PlayerMoveSettingConfirm, eText_MessageDB.PlayerMoveSettingConfirm_Android, new Action<int>(this.OnClickNotifyConfirm));
				}
				break;
			}
			case DataInheritenceUIGroup.eMode.AccountLinkWait:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				AccountLinker.CheckCreate();
				DataInheritenceWindowContent.eType inheritenceWindowType2 = this.m_InheritenceWindowType;
				if (inheritenceWindowType2 != DataInheritenceWindowContent.eType.ConfigurationWindow)
				{
					if (inheritenceWindowType2 == DataInheritenceWindowContent.eType.ExecuteWindow)
					{
						AccountLinker.BuildUpAcountLinkPlayer();
					}
				}
				else
				{
					AccountLinker.SetAcountLinkData();
				}
				break;
			}
			case DataInheritenceUIGroup.eMode.AccountLinkEndDialog:
			{
				AccountLinker.eResult state = AccountLinker.GetState();
				if (state != AccountLinker.eResult.Success)
				{
					if (state != AccountLinker.eResult.LogInError)
					{
						if (state == AccountLinker.eResult.ComError)
						{
							this.SetMode(DataInheritenceUIGroup.eMode.MainSelect);
						}
					}
					else
					{
						APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingResultError_Android), new Action(this.OnClickResultErrorConfirm));
						this.SetMode(DataInheritenceUIGroup.eMode.MainSelect);
					}
				}
				else
				{
					this.m_RestartCallback();
					this.m_DataInheritenceSelectWindow.Close();
				}
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				break;
			}
			}
		}

		// Token: 0x06002D7A RID: 11642 RVA: 0x000F0362 File Offset: 0x000EE762
		public void OnClickALButtonPlatform()
		{
			if (this.m_Mode != DataInheritenceUIGroup.eMode.MainSelect)
			{
				return;
			}
			this.SetMode(DataInheritenceUIGroup.eMode.AccountLinkNotifyDialog);
		}

		// Token: 0x06002D7B RID: 11643 RVA: 0x000F0378 File Offset: 0x000EE778
		public void OnClickALButtonUseIDAndPassword()
		{
			if (this.m_Mode != DataInheritenceUIGroup.eMode.MainSelect)
			{
				return;
			}
			this.m_DataInheritenceSelectWindow.Close();
			this.m_Content.Setup();
			this.m_Content.SetWindowType(this.m_InheritenceWindowType);
			this.m_DataInheritenceWindow.Open();
			this.m_CustomWindow = this.m_DataInheritenceWindow;
		}

		// Token: 0x06002D7C RID: 11644 RVA: 0x000F03D0 File Offset: 0x000EE7D0
		public void OnClickCloseButton()
		{
			if (this.m_Mode != DataInheritenceUIGroup.eMode.MainSelect && this.m_Mode != DataInheritenceUIGroup.eMode.MainIDAndPass)
			{
				return;
			}
			this.Close();
		}

		// Token: 0x06002D7D RID: 11645 RVA: 0x000F03F1 File Offset: 0x000EE7F1
		public void OnClickNotifyConfirm(int idx)
		{
			if (idx == 0)
			{
				this.SetMode(DataInheritenceUIGroup.eMode.AccountLinkWait);
			}
			else
			{
				this.SetMode(DataInheritenceUIGroup.eMode.Open);
			}
		}

		// Token: 0x06002D7E RID: 11646 RVA: 0x000F040C File Offset: 0x000EE80C
		public void OnClickResultSuccessConfirm(int idx)
		{
			this.SetMode(DataInheritenceUIGroup.eMode.Close);
		}

		// Token: 0x06002D7F RID: 11647 RVA: 0x000F0415 File Offset: 0x000EE815
		public void OnClickResultErrorConfirm()
		{
			this.SetMode(DataInheritenceUIGroup.eMode.MainSelect);
		}

		// Token: 0x06002D80 RID: 11648 RVA: 0x000F041E File Offset: 0x000EE81E
		public string GetIdString()
		{
			if (this.m_Content != null)
			{
				return this.m_Content.GetIdString();
			}
			return null;
		}

		// Token: 0x06002D81 RID: 11649 RVA: 0x000F043E File Offset: 0x000EE83E
		public string GetPasswordString()
		{
			if (this.m_Content != null)
			{
				return this.m_Content.GetPasswordString();
			}
			return null;
		}

		// Token: 0x06002D82 RID: 11650 RVA: 0x000F045E File Offset: 0x000EE85E
		public void SetIdString(string id)
		{
			if (this.m_Content != null)
			{
				this.m_Content.SetIdString(id);
			}
		}

		// Token: 0x06002D83 RID: 11651 RVA: 0x000F047D File Offset: 0x000EE87D
		public void SetPasswordString(string password)
		{
			if (this.m_Content != null)
			{
				this.m_Content.SetPasswordString(password);
			}
		}

		// Token: 0x06002D84 RID: 11652 RVA: 0x000F049C File Offset: 0x000EE89C
		public void SetDeadlineString(DateTime deadline)
		{
			if (this.m_Content != null)
			{
				string textCommon = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDeadlineValue, new object[]
				{
					deadline.Year,
					deadline.Month,
					deadline.Day,
					deadline.Hour,
					deadline.Minute
				});
				this.m_Content.SetDeadlineString(textCommon);
			}
		}

		// Token: 0x04003484 RID: 13444
		[SerializeField]
		private DataInheritenceWindowContent.eType m_InheritenceWindowType;

		// Token: 0x04003485 RID: 13445
		[SerializeField]
		private DataInheritenceWindowContent m_Content;

		// Token: 0x04003486 RID: 13446
		[SerializeField]
		private DataInheritenceSelectContnt m_DataInheritenceSelectWindow;

		// Token: 0x04003487 RID: 13447
		[SerializeField]
		private CustomWindow m_DataInheritenceWindow;

		// Token: 0x04003488 RID: 13448
		private Action m_RestartCallback;

		// Token: 0x04003489 RID: 13449
		private DataInheritenceUIGroup.eMode m_Mode = DataInheritenceUIGroup.eMode.Invalid;

		// Token: 0x02000890 RID: 2192
		private enum eMode
		{
			// Token: 0x0400348B RID: 13451
			Invalid = -1,
			// Token: 0x0400348C RID: 13452
			Open,
			// Token: 0x0400348D RID: 13453
			MainSelect,
			// Token: 0x0400348E RID: 13454
			MainIDAndPass,
			// Token: 0x0400348F RID: 13455
			AccountLinkNotifyDialog,
			// Token: 0x04003490 RID: 13456
			AccountLinkWait,
			// Token: 0x04003491 RID: 13457
			AccountLinkEndDialog,
			// Token: 0x04003492 RID: 13458
			Close
		}
	}
}
