﻿using System;

namespace Star.UI
{
	// Token: 0x02000874 RID: 2164
	public class TownObjectIcon : ASyncImage
	{
		// Token: 0x06002CCD RID: 11469 RVA: 0x000EC624 File Offset: 0x000EAA24
		public void Apply(int townObjectId, int townObjectLv = 1)
		{
			if (this.m_TownObjectId != townObjectId || this.m_TownObjectLv != townObjectLv)
			{
				this.Apply();
				if (townObjectId != -1)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(townObjectId).m_Category == 0)
					{
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncTownObjectIcon(townObjectId, townObjectLv);
					}
					else
					{
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncTownObjectIcon(townObjectId, 1);
					}
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_TownObjectId = townObjectId;
				this.m_TownObjectLv = townObjectLv;
			}
		}

		// Token: 0x06002CCE RID: 11470 RVA: 0x000EC6DC File Offset: 0x000EAADC
		public override void Destroy()
		{
			base.Destroy();
			this.m_TownObjectId = -1;
			this.m_TownObjectLv = 0;
		}

		// Token: 0x040033C9 RID: 13257
		protected int m_TownObjectId = -1;

		// Token: 0x040033CA RID: 13258
		protected int m_TownObjectLv;
	}
}
