﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B1 RID: 1969
	public class SelectedCharaInfoCharacterIcon : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x060028A1 RID: 10401 RVA: 0x000D9A6D File Offset: 0x000D7E6D
		public override void Setup()
		{
			if (this.m_CharacterIconCloner != null)
			{
				this.m_CharacterIconCloner.Setup(true);
				this.m_CharacterIcon = this.m_CharacterIconCloner.GetInst<CharaIconWithFrame>();
			}
			base.Setup();
		}

		// Token: 0x060028A2 RID: 10402 RVA: 0x000D9AA3 File Offset: 0x000D7EA3
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return new CharaIconWithFrameAdapter(this.m_CharacterIcon);
		}

		// Token: 0x060028A3 RID: 10403 RVA: 0x000D9AB0 File Offset: 0x000D7EB0
		public override void SetLimitBreak(int lb)
		{
			this.m_CharacterIcon.SetLimitBreak(lb);
		}

		// Token: 0x060028A4 RID: 10404 RVA: 0x000D9ABE File Offset: 0x000D7EBE
		public override bool IsPreparing()
		{
			return false;
		}

		// Token: 0x060028A5 RID: 10405 RVA: 0x000D9AC1 File Offset: 0x000D7EC1
		public override bool IsAnimStateIn()
		{
			return base.IsAnimStateIn();
		}

		// Token: 0x060028A6 RID: 10406 RVA: 0x000D9AC9 File Offset: 0x000D7EC9
		public override bool IsAnimStateOut()
		{
			return base.IsAnimStateOut();
		}

		// Token: 0x04002F96 RID: 12182
		[SerializeField]
		[Tooltip("Modelの親.")]
		private PrefabCloner m_CharacterIconCloner;

		// Token: 0x04002F97 RID: 12183
		private CharaIconWithFrame m_CharacterIcon;
	}
}
