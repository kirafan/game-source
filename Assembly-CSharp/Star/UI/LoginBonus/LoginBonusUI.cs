﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009D2 RID: 2514
	public class LoginBonusUI : MenuUIBase
	{
		// Token: 0x0600342C RID: 13356 RVA: 0x001087A7 File Offset: 0x00106BA7
		private void Update()
		{
			this.UpdateStep();
		}

		// Token: 0x0600342D RID: 13357 RVA: 0x001087B0 File Offset: 0x00106BB0
		private void UpdateStep()
		{
			switch (this.m_Step)
			{
			case LoginBonusUI.eStep.WaitPrepare:
				if (this.m_Loader != null)
				{
					this.m_Loader.Update();
				}
				if (this.m_BackGroundResourceHandler != null && this.m_BackGroundResourceHandler.IsDone())
				{
					if (this.m_BackGroundResourceHandler.IsError())
					{
						APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
						return;
					}
					for (int i = 0; i < this.m_BackGroundResourceHandler.Objs.Length; i++)
					{
						if (this.m_BackGroundResourceHandler.Objs[i] is Sprite)
						{
							this.m_BGSpriteList.Add(this.m_BackGroundResourceHandler.Objs[i] as Sprite);
						}
					}
					this.m_BackGroundResourceHandler = null;
				}
				if (this.m_IconResourceHandler != null && this.m_IconResourceHandler.IsDone())
				{
					if (this.m_IconResourceHandler.IsError())
					{
						APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
						return;
					}
					for (int j = 0; j < this.m_IconResourceHandler.Objs.Length; j++)
					{
						if (this.m_IconResourceHandler.Objs[j] is Sprite)
						{
							this.m_IconSpriteList.Add(this.m_IconResourceHandler.Objs[j] as Sprite);
						}
					}
					this.m_IconResourceHandler = null;
				}
				if (this.m_BackGroundResourceHandler == null && this.m_IconResourceHandler == null && (this.m_IsEvent || this.m_NpcIllust.IsDoneLoad()))
				{
					this.ApplyBG(this.m_ID);
					for (int k = 0; k < this.m_BonusPanelInstList.Count; k++)
					{
						this.m_BonusPanelInstList[k].ApplyIcon(this.GetIcon(this.m_BonusPanelInstList[k].GetIconID()));
					}
					this.InStart();
					if (!this.m_IsEvent)
					{
						this.m_NpcIllust.GetComponent<AnimUIPlayer>().PlayIn();
					}
				}
				break;
			case LoginBonusUI.eStep.In:
			{
				bool flag = true;
				for (int l = 0; l < this.m_AnimUIPlayerArray.Length; l++)
				{
					if (this.m_AnimUIPlayerArray[l].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag = false;
				}
				if (!SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(LoginBonusUI.eStep.Stamp);
				}
				break;
			}
			case LoginBonusUI.eStep.Stamp:
				if (!this.m_IsEvent)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_001, "voice_002", true);
				}
				this.m_BonusPanelInstList[this.m_StampIdx].SetIconState(BonusPanel.eIconState.Anim);
				this.m_Step = LoginBonusUI.eStep.WaitStamp;
				break;
			case LoginBonusUI.eStep.WaitStamp:
				if (!this.m_BonusPanelInstList[this.m_StampIdx].IsPlayingAnim())
				{
					this.m_Step = LoginBonusUI.eStep.Idle;
				}
				break;
			case LoginBonusUI.eStep.Idle:
				this.UpdateAndroidBackKey();
				break;
			case LoginBonusUI.eStep.Out:
			{
				bool flag2 = true;
				for (int m = 0; m < this.m_AnimUIPlayerArray.Length; m++)
				{
					if (this.m_AnimUIPlayerArray[m].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (!SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					flag2 = false;
				}
				if (flag2)
				{
					if (this.m_PageUIDataList.Count > 0)
					{
						this.PrepareBonus();
					}
					else
					{
						for (int n = 0; n < this.m_BonusPanelInstList.Count; n++)
						{
							this.m_BonusPanelInstList[n].Destroy();
						}
						this.ChangeStep(LoginBonusUI.eStep.End);
					}
				}
				break;
			}
			}
		}

		// Token: 0x0600342E RID: 13358 RVA: 0x00108B98 File Offset: 0x00106F98
		private void ChangeStep(LoginBonusUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case LoginBonusUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case LoginBonusUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case LoginBonusUI.eStep.Stamp:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case LoginBonusUI.eStep.WaitStamp:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case LoginBonusUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case LoginBonusUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case LoginBonusUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600342F RID: 13359 RVA: 0x00108C88 File Offset: 0x00107088
		public void Setup(List<PageUIData> uiDataList)
		{
			this.m_PageUIDataList = uiDataList;
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
			this.m_NpcIllust.Apply(NpcIllust.eNpc.Lamp);
			this.m_Loader = new ABResourceLoader(-1);
			this.m_BackGroundResourceHandler = this.m_Loader.Load("texture/loginbonusbackground.muast", new MeigeResource.Option[0]);
			this.m_IconResourceHandler = this.m_Loader.Load("texture/loginbonusicon.muast", new MeigeResource.Option[0]);
		}

		// Token: 0x06003430 RID: 13360 RVA: 0x00108D00 File Offset: 0x00107100
		public override void PlayIn()
		{
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.PrepareBonus();
		}

		// Token: 0x06003431 RID: 13361 RVA: 0x00108D40 File Offset: 0x00107140
		public override void PlayOut()
		{
			this.ChangeStep(LoginBonusUI.eStep.Out);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_NpcIllust.GetComponent<AnimUIPlayer>().PlayOut();
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(-1f, null);
		}

		// Token: 0x06003432 RID: 13362 RVA: 0x00108DA6 File Offset: 0x001071A6
		public override bool IsMenuEnd()
		{
			return this.m_Step == LoginBonusUI.eStep.End;
		}

		// Token: 0x06003433 RID: 13363 RVA: 0x00108DB1 File Offset: 0x001071B1
		public override void Destroy()
		{
			this.m_Loader.UnloadAll(false);
			base.Destroy();
		}

		// Token: 0x06003434 RID: 13364 RVA: 0x00108DC8 File Offset: 0x001071C8
		private void PrepareBonus()
		{
			PageUIData pageUIData = this.m_PageUIDataList[0];
			this.m_PageUIDataList.RemoveAt(0);
			while (pageUIData.m_RewardList.Count > this.m_BonusPanelInstList.Count)
			{
				BonusPanel item = UnityEngine.Object.Instantiate<BonusPanel>(this.m_BonusPanelPrefab);
				this.m_BonusPanelInstList.Add(item);
			}
			this.m_IconParentEvents[1].gameObject.SetActive(pageUIData.m_RewardList.Count > 4);
			int num = (pageUIData.m_RewardList.Count + 1) / this.m_IconParentEvents.Length;
			for (int i = 0; i < this.m_BonusPanelInstList.Count; i++)
			{
				if (i < pageUIData.m_RewardList.Count)
				{
					this.m_BonusPanelInstList[i].gameObject.SetActive(true);
					this.m_BonusPanelInstList[i].Apply(pageUIData.m_RewardList[i]);
					if (i < pageUIData.m_CurrentIdx)
					{
						this.m_BonusPanelInstList[i].SetIconState(BonusPanel.eIconState.Received);
					}
					else
					{
						this.m_BonusPanelInstList[i].SetIconState(BonusPanel.eIconState.None);
					}
					RectTransform parent = this.m_IconParentNormals[0];
					if (pageUIData.m_Type == PageUIData.eType.Normal)
					{
						if (i < 4)
						{
							parent = this.m_IconParentNormals[0];
						}
						else
						{
							parent = this.m_IconParentNormals[1];
						}
					}
					else if (pageUIData.m_Type == PageUIData.eType.Event)
					{
						if (pageUIData.m_RewardList.Count > 4)
						{
							int num2 = i / num;
							parent = this.m_IconParentEvents[num2];
						}
						else
						{
							parent = this.m_IconParentEvents[0];
						}
					}
					this.m_BonusPanelInstList[i].GetComponent<RectTransform>().SetParent(parent, false);
				}
				else
				{
					this.m_BonusPanelInstList[i].gameObject.SetActive(false);
				}
			}
			this.m_StampIdx = pageUIData.m_CurrentIdx;
			this.m_BGImage.sprite = null;
			this.m_IsEvent = (pageUIData.m_Type == PageUIData.eType.Event);
			this.m_ID = pageUIData.m_ImgID;
			this.m_NormalObj.SetActive(!this.m_IsEvent);
			this.ChangeStep(LoginBonusUI.eStep.WaitPrepare);
		}

		// Token: 0x06003435 RID: 13365 RVA: 0x00108FEC File Offset: 0x001073EC
		private void ApplyBG(int id)
		{
			for (int i = 0; i < this.m_BGSpriteList.Count; i++)
			{
				if (this.m_BGSpriteList[i].name == "LoginBonusBackGround_" + id.ToString())
				{
					this.m_BGImage.sprite = this.m_BGSpriteList[i];
					break;
				}
			}
		}

		// Token: 0x06003436 RID: 13366 RVA: 0x00109064 File Offset: 0x00107464
		private Sprite GetIcon(int iconID)
		{
			for (int i = 0; i < this.m_IconSpriteList.Count; i++)
			{
				if (this.m_IconSpriteList[i].name == "LoginBonusIcon_" + iconID.ToString())
				{
					return this.m_IconSpriteList[i];
				}
			}
			return null;
		}

		// Token: 0x06003437 RID: 13367 RVA: 0x001090CD File Offset: 0x001074CD
		private void InStart()
		{
			this.ChangeStep(LoginBonusUI.eStep.In);
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(-1f, null);
		}

		// Token: 0x06003438 RID: 13368 RVA: 0x001090EC File Offset: 0x001074EC
		public void OnClickSkipButtonCallBack()
		{
			LoginBonusUI.eStep step = this.m_Step;
			if (step != LoginBonusUI.eStep.Stamp && step != LoginBonusUI.eStep.WaitStamp)
			{
				if (step == LoginBonusUI.eStep.Idle)
				{
					this.PlayOut();
				}
			}
			else
			{
				this.SkipStamp();
			}
		}

		// Token: 0x06003439 RID: 13369 RVA: 0x00109130 File Offset: 0x00107530
		public void OnClickSkipAllButtonCallBack()
		{
			LoginBonusUI.eStep step = this.m_Step;
			if (step != LoginBonusUI.eStep.Stamp && step != LoginBonusUI.eStep.WaitStamp)
			{
				if (step == LoginBonusUI.eStep.Idle)
				{
					this.SkipStamp();
					this.PlayOut();
				}
			}
			else
			{
				this.SkipStamp();
				this.PlayOut();
			}
		}

		// Token: 0x0600343A RID: 13370 RVA: 0x00109180 File Offset: 0x00107580
		private void SkipStamp()
		{
			this.m_BonusPanelInstList[this.m_StampIdx].SetIconState(BonusPanel.eIconState.Received);
			this.m_Step = LoginBonusUI.eStep.Idle;
		}

		// Token: 0x0600343B RID: 13371 RVA: 0x001091A0 File Offset: 0x001075A0
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_SkipButton, null);
		}

		// Token: 0x04003A7A RID: 14970
		private const int NORMAL_ROW_MAX = 4;

		// Token: 0x04003A7B RID: 14971
		private const int ROW_MAX = 4;

		// Token: 0x04003A7C RID: 14972
		private LoginBonusUI.eStep m_Step;

		// Token: 0x04003A7D RID: 14973
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003A7E RID: 14974
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003A7F RID: 14975
		[SerializeField]
		private BonusPanel m_BonusPanelPrefab;

		// Token: 0x04003A80 RID: 14976
		[SerializeField]
		[Tooltip("アイコンの親(通常版時)")]
		private RectTransform[] m_IconParentNormals;

		// Token: 0x04003A81 RID: 14977
		[SerializeField]
		[Tooltip("アイコンの親(イベント時) 配列で一列目、二列目を指定")]
		private RectTransform[] m_IconParentEvents;

		// Token: 0x04003A82 RID: 14978
		[SerializeField]
		private Image m_BGImage;

		// Token: 0x04003A83 RID: 14979
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x04003A84 RID: 14980
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x04003A85 RID: 14981
		[SerializeField]
		private CustomButton m_SkipButton;

		// Token: 0x04003A86 RID: 14982
		private List<PageUIData> m_PageUIDataList;

		// Token: 0x04003A87 RID: 14983
		private List<BonusPanel> m_BonusPanelInstList = new List<BonusPanel>();

		// Token: 0x04003A88 RID: 14984
		private int m_StampIdx;

		// Token: 0x04003A89 RID: 14985
		private bool m_IsEvent;

		// Token: 0x04003A8A RID: 14986
		private int m_ID = -1;

		// Token: 0x04003A8B RID: 14987
		private ABResourceLoader m_Loader;

		// Token: 0x04003A8C RID: 14988
		private ABResourceObjectHandler m_BackGroundResourceHandler;

		// Token: 0x04003A8D RID: 14989
		private ABResourceObjectHandler m_IconResourceHandler;

		// Token: 0x04003A8E RID: 14990
		private const string BG_RESOURCE_PATH = "texture/loginbonusbackground.muast";

		// Token: 0x04003A8F RID: 14991
		private const string ICON_RESOURCE_PATH = "texture/loginbonusicon.muast";

		// Token: 0x04003A90 RID: 14992
		private const string BG_OBJ_BASENAME = "LoginBonusBackGround_";

		// Token: 0x04003A91 RID: 14993
		private const string ICON_OBJ_BASENAME = "LoginBonusIcon_";

		// Token: 0x04003A92 RID: 14994
		private List<Sprite> m_BGSpriteList = new List<Sprite>();

		// Token: 0x04003A93 RID: 14995
		private List<Sprite> m_IconSpriteList = new List<Sprite>();

		// Token: 0x020009D3 RID: 2515
		private enum eStep
		{
			// Token: 0x04003A95 RID: 14997
			Hide,
			// Token: 0x04003A96 RID: 14998
			WaitPrepare,
			// Token: 0x04003A97 RID: 14999
			In,
			// Token: 0x04003A98 RID: 15000
			Stamp,
			// Token: 0x04003A99 RID: 15001
			WaitStamp,
			// Token: 0x04003A9A RID: 15002
			Idle,
			// Token: 0x04003A9B RID: 15003
			Out,
			// Token: 0x04003A9C RID: 15004
			End,
			// Token: 0x04003A9D RID: 15005
			Num
		}
	}
}
