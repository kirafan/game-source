﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009CD RID: 2509
	public class BonusPanel : MonoBehaviour
	{
		// Token: 0x06003422 RID: 13346 RVA: 0x001085BE File Offset: 0x001069BE
		public int GetDay()
		{
			return this.m_Day;
		}

		// Token: 0x06003423 RID: 13347 RVA: 0x001085C6 File Offset: 0x001069C6
		public int GetIconID()
		{
			return this.m_IconID;
		}

		// Token: 0x06003424 RID: 13348 RVA: 0x001085D0 File Offset: 0x001069D0
		public void Apply(RewardUIData uiData)
		{
			this.m_Day = uiData.m_Day;
			this.m_IconID = uiData.m_IconID;
			if (uiData.m_IsEvent)
			{
				this.m_DayImage.enabled = true;
				this.m_DayImage.sprite = this.m_EventDaySprites[uiData.m_Day];
				this.m_Icon.GetComponent<RectTransform>().localScale = Vector3.one * 0.75f;
				this.m_GetIconScale.localScale = Vector3.one * 0.75f;
			}
			else
			{
				this.m_DayImage.enabled = false;
				this.m_Icon.GetComponent<RectTransform>().localScale = Vector3.one;
				this.m_GetIconScale.localScale = Vector3.one;
			}
		}

		// Token: 0x06003425 RID: 13349 RVA: 0x00108693 File Offset: 0x00106A93
		public void ApplyIcon(Sprite sprite)
		{
			this.m_Icon.sprite = sprite;
		}

		// Token: 0x06003426 RID: 13350 RVA: 0x001086A1 File Offset: 0x00106AA1
		public void Destroy()
		{
			this.m_Icon.sprite = null;
		}

		// Token: 0x06003427 RID: 13351 RVA: 0x001086B0 File Offset: 0x00106AB0
		public void SetIconState(BonusPanel.eIconState state)
		{
			if (state != BonusPanel.eIconState.None)
			{
				if (state != BonusPanel.eIconState.Anim)
				{
					if (state == BonusPanel.eIconState.Received)
					{
						this.m_GetIcon.Show();
					}
				}
				else
				{
					this.m_GetIcon.Hide();
					this.m_GetIcon.PlayIn();
				}
			}
			else
			{
				this.m_GetIcon.Hide();
			}
		}

		// Token: 0x06003428 RID: 13352 RVA: 0x00108711 File Offset: 0x00106B11
		public bool IsPlayingAnim()
		{
			return this.m_GetIcon.State == 1 || this.m_GetIcon.State == 3;
		}

		// Token: 0x04003A65 RID: 14949
		[SerializeField]
		private Image m_Icon;

		// Token: 0x04003A66 RID: 14950
		[SerializeField]
		private AnimUIPlayer m_GetIcon;

		// Token: 0x04003A67 RID: 14951
		[SerializeField]
		[Tooltip("日数表示\u3000Eventのみ使用")]
		private Image m_DayImage;

		// Token: 0x04003A68 RID: 14952
		[SerializeField]
		[Tooltip("日数表示スプライト\u3000Eventのみ使用")]
		private Sprite[] m_EventDaySprites;

		// Token: 0x04003A69 RID: 14953
		[SerializeField]
		[Tooltip("Getアイコンにスケールをかけるためのオブジェクト")]
		private RectTransform m_GetIconScale;

		// Token: 0x04003A6A RID: 14954
		private int m_Day;

		// Token: 0x04003A6B RID: 14955
		private int m_IconID;

		// Token: 0x020009CE RID: 2510
		public enum eIconState
		{
			// Token: 0x04003A6D RID: 14957
			None,
			// Token: 0x04003A6E RID: 14958
			Anim,
			// Token: 0x04003A6F RID: 14959
			Received
		}
	}
}
