﻿using System;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009CF RID: 2511
	public class RewardUIData
	{
		// Token: 0x06003429 RID: 13353 RVA: 0x00108735 File Offset: 0x00106B35
		public RewardUIData(bool isEvent, int iconID, int day)
		{
			this.m_IsEvent = isEvent;
			this.m_IconID = iconID;
			this.m_Day = day;
		}

		// Token: 0x04003A70 RID: 14960
		public bool m_IsEvent;

		// Token: 0x04003A71 RID: 14961
		public int m_IconID;

		// Token: 0x04003A72 RID: 14962
		public int m_Day;
	}
}
