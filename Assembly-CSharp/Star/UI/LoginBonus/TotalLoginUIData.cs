﻿using System;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009D4 RID: 2516
	public class TotalLoginUIData
	{
		// Token: 0x0600343C RID: 13372 RVA: 0x001091AE File Offset: 0x001075AE
		public TotalLoginUIData(int day, ePresentType[] type, int[] id, int[] amount)
		{
			this.m_Day = day;
			this.m_Type = type;
			this.m_ID = id;
			this.m_Amount = amount;
		}

		// Token: 0x04003A9E RID: 15006
		public int m_Day;

		// Token: 0x04003A9F RID: 15007
		public ePresentType[] m_Type;

		// Token: 0x04003AA0 RID: 15008
		public int[] m_ID;

		// Token: 0x04003AA1 RID: 15009
		public int[] m_Amount;
	}
}
