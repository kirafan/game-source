﻿using System;
using System.Collections.Generic;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009D0 RID: 2512
	public class PageUIData
	{
		// Token: 0x0600342A RID: 13354 RVA: 0x00108752 File Offset: 0x00106B52
		public PageUIData(PageUIData.eType type, int imgID, List<RewardUIData> rewardList, int currentIdx)
		{
			this.m_Type = type;
			this.m_ImgID = imgID;
			this.m_RewardList = rewardList;
			this.m_CurrentIdx = currentIdx;
		}

		// Token: 0x04003A73 RID: 14963
		public PageUIData.eType m_Type;

		// Token: 0x04003A74 RID: 14964
		public int m_ImgID;

		// Token: 0x04003A75 RID: 14965
		public List<RewardUIData> m_RewardList;

		// Token: 0x04003A76 RID: 14966
		public int m_CurrentIdx;

		// Token: 0x020009D1 RID: 2513
		public enum eType
		{
			// Token: 0x04003A78 RID: 14968
			Normal,
			// Token: 0x04003A79 RID: 14969
			Event
		}
	}
}
