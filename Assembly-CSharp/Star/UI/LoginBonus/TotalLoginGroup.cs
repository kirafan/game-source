﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.LoginBonus
{
	// Token: 0x020009D5 RID: 2517
	public class TotalLoginGroup : UIGroup
	{
		// Token: 0x0600343E RID: 13374 RVA: 0x001091F4 File Offset: 0x001075F4
		public void Open(TotalLoginUIData uiData)
		{
			this.m_DayText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TotalLoginText, new object[]
			{
				uiData.m_Day.ToString()
			});
			int num = 0;
			for (int i = 0; i < this.m_Panels.Length; i++)
			{
				if (uiData.m_Amount[i] <= 0)
				{
					this.m_Panels[i].gameObject.SetActive(false);
				}
				else
				{
					switch (uiData.m_Type[i])
					{
					case ePresentType.Chara:
						this.m_Panels[i].ApplyChara(uiData.m_ID[i]);
						break;
					case ePresentType.Item:
						this.m_Panels[i].ApplyItem(uiData.m_ID[i], uiData.m_Amount[i]);
						break;
					case ePresentType.Weapon:
						this.m_Panels[i].ApplyWeapon(uiData.m_ID[i], (long)uiData.m_Amount[i]);
						break;
					case ePresentType.TownFacility:
						this.m_Panels[i].ApplyTownObj(uiData.m_ID[i], (long)uiData.m_Amount[i]);
						break;
					case ePresentType.RoomObject:
						this.m_Panels[i].ApplyRoomObj(uiData.m_ID[i], (long)uiData.m_Amount[i]);
						break;
					case ePresentType.KRRPoint:
						this.m_Panels[i].ApplyKRR((long)uiData.m_Amount[i]);
						break;
					case ePresentType.Gold:
						this.m_Panels[i].ApplyGold((long)uiData.m_Amount[i]);
						break;
					case ePresentType.LimitedGem:
						this.m_Panels[i].ApplyGem((long)uiData.m_Amount[i]);
						break;
					}
					this.m_Panels[i].gameObject.SetActive(true);
					num++;
				}
			}
			this.m_WindowRect.sizeDelta = new Vector2(this.WINDOW_WIDTH[num - 1], this.m_WindowRect.sizeDelta.y);
			base.Open();
		}

		// Token: 0x04003AA2 RID: 15010
		public readonly float[] WINDOW_WIDTH = new float[]
		{
			580f,
			667f,
			920f
		};

		// Token: 0x04003AA3 RID: 15011
		[SerializeField]
		private Text m_DayText;

		// Token: 0x04003AA4 RID: 15012
		[SerializeField]
		private RewardPanel[] m_Panels;

		// Token: 0x04003AA5 RID: 15013
		[SerializeField]
		private RectTransform m_WindowRect;
	}
}
