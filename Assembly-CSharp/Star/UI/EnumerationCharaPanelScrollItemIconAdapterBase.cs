﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000958 RID: 2392
	public abstract class EnumerationCharaPanelScrollItemIconAdapterBase : ScrollItemIcon
	{
		// Token: 0x0600319E RID: 12702 RVA: 0x000FF78A File Offset: 0x000FDB8A
		public virtual EnumerationCharaPanelScrollItemIcon CreateCore()
		{
			return new EnumerationCharaPanelScrollItemIcon();
		}

		// Token: 0x0600319F RID: 12703 RVA: 0x000FF794 File Offset: 0x000FDB94
		public virtual EnumerationCharaPanelScrollItemIcon.SharedInstanceEx CreateArgument()
		{
			if (this.m_DataContent != null)
			{
				this.m_SerializedConstructInstances.m_DataContent = this.m_DataContent;
			}
			if (this.m_CharaInfo != null)
			{
				this.m_SerializedConstructInstances.m_CharaInfo = this.m_CharaInfo;
			}
			if (this.m_Blank != null)
			{
				this.m_SerializedConstructInstances.m_Blank = this.m_Blank;
			}
			if (this.m_EmptyObj != null)
			{
				this.m_SerializedConstructInstances.m_EmptyObj = this.m_EmptyObj;
			}
			if (this.m_FriendObj != null)
			{
				this.m_SerializedConstructInstances.m_FriendObj = this.m_FriendObj;
			}
			return new EnumerationCharaPanelScrollItemIcon.SharedInstanceEx(this.m_SerializedConstructInstances)
			{
				parent = this
			};
		}

		// Token: 0x060031A0 RID: 12704 RVA: 0x000FF85F File Offset: 0x000FDC5F
		public virtual void Setup(EnumerationPanelScrollItemIconBase parent)
		{
			this.m_Core = this.CreateCore();
			this.m_Parent = parent;
			this.m_SharedInstance = this.CreateArgument();
			this.m_Core.Setup(this.m_SharedInstance);
		}

		// Token: 0x060031A1 RID: 12705 RVA: 0x000FF891 File Offset: 0x000FDC91
		public override void Destroy()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Destroy();
			}
		}

		// Token: 0x060031A2 RID: 12706 RVA: 0x000FF8A9 File Offset: 0x000FDCA9
		public virtual void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
			if (this.m_Core != null)
			{
				this.m_Core.Apply(partyMemberController);
			}
		}

		// Token: 0x060031A3 RID: 12707 RVA: 0x000FF8C2 File Offset: 0x000FDCC2
		public void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
			if (this.m_Core != null)
			{
				this.m_Core.SetMode(mode);
			}
		}

		// Token: 0x040037FD RID: 14333
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		private GameObject m_DataContent;

		// Token: 0x040037FE RID: 14334
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.キャラ情報部.")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

		// Token: 0x040037FF RID: 14335
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		private GameObject m_Blank;

		// Token: 0x04003800 RID: 14336
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		private GameObject m_EmptyObj;

		// Token: 0x04003801 RID: 14337
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		private GameObject m_FriendObj;

		// Token: 0x04003802 RID: 14338
		[SerializeField]
		private EnumerationCharaPanelBase.SharedInstance m_SerializedConstructInstances;

		// Token: 0x04003803 RID: 14339
		protected EnumerationPanelScrollItemIconBase m_Parent;

		// Token: 0x04003804 RID: 14340
		private EnumerationCharaPanelScrollItemIcon m_Core;

		// Token: 0x04003805 RID: 14341
		protected EnumerationCharaPanelScrollItemIcon.SharedInstanceEx m_SharedInstance;
	}
}
