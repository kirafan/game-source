﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000B13 RID: 2835
	public class TutorialTarget : MonoBehaviour
	{
		// Token: 0x06003B93 RID: 15251 RVA: 0x0013044B File Offset: 0x0012E84B
		public void SetEnable(bool flg)
		{
			this.m_Enable = flg;
		}

		// Token: 0x06003B94 RID: 15252 RVA: 0x00130454 File Offset: 0x0012E854
		public bool GetEnable()
		{
			return this.m_Enable;
		}

		// Token: 0x06003B95 RID: 15253 RVA: 0x0013045C File Offset: 0x0012E85C
		private void Start()
		{
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x06003B96 RID: 15254 RVA: 0x0013046C File Offset: 0x0012E86C
		private void Update()
		{
			if (this.m_Enable && !this.m_IsAttached)
			{
				Debug.Log("TutorialTarget.Attach:" + this.m_GameObject);
				this.m_IsAttached = true;
				UIUtility.AttachCanvasForChangeRenderOrder(this.m_GameObject, SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
			}
			else if (!this.m_Enable && this.m_IsAttached)
			{
				Debug.Log("TutorialTarget.Detach:" + this.m_GameObject);
				this.m_IsAttached = false;
				UIUtility.DetachCanvasForChangeRenderOrder(this.m_GameObject, true);
			}
		}

		// Token: 0x04004330 RID: 17200
		private bool m_Enable;

		// Token: 0x04004331 RID: 17201
		private bool m_IsAttached;

		// Token: 0x04004332 RID: 17202
		private GameObject m_GameObject;
	}
}
