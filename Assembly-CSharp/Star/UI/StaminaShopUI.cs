﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000ACC RID: 2764
	public class StaminaShopUI : MenuUIBase
	{
		// Token: 0x17000360 RID: 864
		// (get) Token: 0x060039BC RID: 14780 RVA: 0x00126694 File Offset: 0x00124A94
		public StaminaShopUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000EE RID: 238
		// (add) Token: 0x060039BD RID: 14781 RVA: 0x0012669C File Offset: 0x00124A9C
		// (remove) Token: 0x060039BE RID: 14782 RVA: 0x001266D4 File Offset: 0x00124AD4
		public event Action<int, int> OnRequest;

		// Token: 0x060039BF RID: 14783 RVA: 0x0012670C File Offset: 0x00124B0C
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_Scroll.Setup();
			this.m_ConfirmGroup.OnConfirm += this.OnConfirmCallBack;
			this.Refresh();
		}

		// Token: 0x060039C0 RID: 14784 RVA: 0x0012676C File Offset: 0x00124B6C
		public void Refresh()
		{
			this.m_Scroll.RemoveAllData();
			this.m_Scroll.AddItem(new StaminaRecipeItemData(-1, new Action<int, int>(this.OnClickRecipeButtonCallBack)));
			for (int i = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.m_Params.Length - 1; i >= 0; i--)
			{
				ItemListDB_Param itemListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.m_Params[i];
				if (itemListDB_Param.m_Type == 7)
				{
					this.m_Scroll.AddItem(new StaminaRecipeItemData(itemListDB_Param.m_ID, new Action<int, int>(this.OnClickRecipeButtonCallBack)));
				}
			}
		}

		// Token: 0x060039C1 RID: 14785 RVA: 0x0012681C File Offset: 0x00124C1C
		private void Update()
		{
			switch (this.m_Step)
			{
			case StaminaShopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(StaminaShopUI.eStep.Idle);
				}
				break;
			}
			case StaminaShopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(StaminaShopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060039C2 RID: 14786 RVA: 0x00126918 File Offset: 0x00124D18
		private void ChangeStep(StaminaShopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case StaminaShopUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case StaminaShopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case StaminaShopUI.eStep.Idle:
				this.m_MainGroup.SetEnableInput(true);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case StaminaShopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case StaminaShopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060039C3 RID: 14787 RVA: 0x001269CC File Offset: 0x00124DCC
		public override void PlayIn()
		{
			this.ChangeStep(StaminaShopUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x060039C4 RID: 14788 RVA: 0x00126A14 File Offset: 0x00124E14
		public override void PlayOut()
		{
			this.ChangeStep(StaminaShopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x060039C5 RID: 14789 RVA: 0x00126A59 File Offset: 0x00124E59
		public override bool IsMenuEnd()
		{
			return this.m_Step == StaminaShopUI.eStep.End;
		}

		// Token: 0x060039C6 RID: 14790 RVA: 0x00126A64 File Offset: 0x00124E64
		public void OnCloseButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x060039C7 RID: 14791 RVA: 0x00126A6C File Offset: 0x00124E6C
		public void OnClickRecipeButtonCallBack(int itemID, int useNum)
		{
			this.m_ConfirmGroup.Apply(itemID, useNum);
			this.m_ConfirmGroup.Open();
		}

		// Token: 0x060039C8 RID: 14792 RVA: 0x00126A86 File Offset: 0x00124E86
		public void OnConfirmCallBack(int itemID, int useNum)
		{
			this.m_ConfirmGroup.Close();
			this.OnRequest.Call(itemID, useNum);
		}

		// Token: 0x060039C9 RID: 14793 RVA: 0x00126AA0 File Offset: 0x00124EA0
		public void CloseConfirm()
		{
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x040040FE RID: 16638
		private StaminaShopUI.eStep m_Step;

		// Token: 0x040040FF RID: 16639
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04004100 RID: 16640
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004101 RID: 16641
		[SerializeField]
		private StaminaConfirmGroup m_ConfirmGroup;

		// Token: 0x04004102 RID: 16642
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04004103 RID: 16643
		private StaminaShopUI.eButton m_SelectButton = StaminaShopUI.eButton.None;

		// Token: 0x02000ACD RID: 2765
		private enum eStep
		{
			// Token: 0x04004106 RID: 16646
			Hide,
			// Token: 0x04004107 RID: 16647
			In,
			// Token: 0x04004108 RID: 16648
			Idle,
			// Token: 0x04004109 RID: 16649
			Out,
			// Token: 0x0400410A RID: 16650
			End
		}

		// Token: 0x02000ACE RID: 2766
		public enum eButton
		{
			// Token: 0x0400410C RID: 16652
			None = -1,
			// Token: 0x0400410D RID: 16653
			Back
		}
	}
}
