﻿using System;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200086C RID: 2156
	public class OrderIcon : ASyncImage
	{
		// Token: 0x06002CB8 RID: 11448 RVA: 0x000EBF1C File Offset: 0x000EA31C
		public void ApplyPL(int charaID)
		{
			if (this.m_Type != OrderIcon.eType.PL || charaID != this.m_ID)
			{
				this.Apply();
				if (charaID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncPLOrderIcon(charaID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_Type = OrderIcon.eType.PL;
				this.m_ID = charaID;
			}
		}

		// Token: 0x06002CB9 RID: 11449 RVA: 0x000EBF94 File Offset: 0x000EA394
		public void ApplyEN(int resourceID)
		{
			if (this.m_Type != OrderIcon.eType.EN || resourceID != this.m_ID)
			{
				this.Apply();
				this.m_Image = base.GetComponent<Image>();
				if (resourceID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncENOrderIcon(resourceID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_Type = OrderIcon.eType.EN;
				this.m_ID = resourceID;
			}
		}

		// Token: 0x06002CBA RID: 11450 RVA: 0x000EC019 File Offset: 0x000EA419
		public override void Destroy()
		{
			base.Destroy();
			this.m_ID = -1;
		}

		// Token: 0x040033B0 RID: 13232
		protected OrderIcon.eType m_Type;

		// Token: 0x040033B1 RID: 13233
		protected int m_ID = -1;

		// Token: 0x0200086D RID: 2157
		protected enum eType
		{
			// Token: 0x040033B3 RID: 13235
			PL,
			// Token: 0x040033B4 RID: 13236
			EN
		}
	}
}
