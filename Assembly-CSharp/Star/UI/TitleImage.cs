﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008AC RID: 2220
	public class TitleImage : MonoBehaviour
	{
		// Token: 0x06002E10 RID: 11792 RVA: 0x000F2D04 File Offset: 0x000F1104
		public void Setup()
		{
		}

		// Token: 0x06002E11 RID: 11793 RVA: 0x000F2D06 File Offset: 0x000F1106
		public void Destroy()
		{
			this.m_TitleImage.sprite = null;
			if (this.m_SpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
			}
			this.m_SpriteHandler = null;
		}

		// Token: 0x06002E12 RID: 11794 RVA: 0x000F2D3B File Offset: 0x000F113B
		private void Update()
		{
			if (this.m_autoDisplay && this.IsTitleImageAvailable())
			{
				this.m_autoDisplay = false;
				this.Display();
			}
		}

		// Token: 0x06002E13 RID: 11795 RVA: 0x000F2D60 File Offset: 0x000F1160
		public void Prepare(eTitleType title)
		{
			if (this.m_TitleType == title)
			{
				return;
			}
			this.m_TitleType = title;
			this.m_autoDisplay = false;
			this.SetTitleImageEnabled(false);
			this.m_TitleImage.sprite = null;
			if (this.m_SpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
			}
			this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncContentTitleLogo(title);
		}

		// Token: 0x06002E14 RID: 11796 RVA: 0x000F2DD1 File Offset: 0x000F11D1
		public void Display()
		{
			this.m_TitleImage.sprite = this.m_SpriteHandler.Obj;
			base.gameObject.SetActive(true);
			this.SetTitleImageEnabled(true);
		}

		// Token: 0x06002E15 RID: 11797 RVA: 0x000F2DFC File Offset: 0x000F11FC
		public bool IsTitleImageAvailable()
		{
			return this.m_TitleImage.sprite == null && this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable();
		}

		// Token: 0x06002E16 RID: 11798 RVA: 0x000F2E2D File Offset: 0x000F122D
		public void SetTitleImageEnabled(bool enabled)
		{
			this.m_TitleImage.enabled = enabled;
		}

		// Token: 0x06002E17 RID: 11799 RVA: 0x000F2E3B File Offset: 0x000F123B
		public void AutoDisplay()
		{
			this.m_autoDisplay = true;
			base.gameObject.SetActive(true);
		}

		// Token: 0x0400351D RID: 13597
		[SerializeField]
		private Image m_TitleImage;

		// Token: 0x0400351E RID: 13598
		private SpriteHandler m_SpriteHandler;

		// Token: 0x0400351F RID: 13599
		private bool m_autoDisplay;

		// Token: 0x04003520 RID: 13600
		private eTitleType m_TitleType = eTitleType.None;
	}
}
