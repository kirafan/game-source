﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000893 RID: 2195
	public class InputFieldController : MonoBehaviour
	{
		// Token: 0x06002D93 RID: 11667 RVA: 0x000F0985 File Offset: 0x000EED85
		public string GetString()
		{
			if (this.m_InputField != null)
			{
				return this.m_InputField.text;
			}
			return null;
		}

		// Token: 0x06002D94 RID: 11668 RVA: 0x000F09A5 File Offset: 0x000EEDA5
		public void SetTitleString(string title)
		{
			if (this.m_Title != null)
			{
				this.m_Title.text = title;
			}
		}

		// Token: 0x06002D95 RID: 11669 RVA: 0x000F09C4 File Offset: 0x000EEDC4
		public void SetTitleSprite(Sprite sprite)
		{
			if (this.m_Image != null)
			{
				this.m_Image.sprite = sprite;
			}
		}

		// Token: 0x06002D96 RID: 11670 RVA: 0x000F09E3 File Offset: 0x000EEDE3
		public void SetInputFieldActive(bool active)
		{
			if (this.m_InputField != null)
			{
				this.m_InputField.enabled = active;
			}
			if (this.m_ActiveEditIcon != null)
			{
				this.m_ActiveEditIcon.SetActive(active);
			}
		}

		// Token: 0x06002D97 RID: 11671 RVA: 0x000F0A1F File Offset: 0x000EEE1F
		public void SetInputFieldString(string text)
		{
			if (this.m_InputField != null)
			{
				this.m_InputField.text = text;
			}
		}

		// Token: 0x040034A3 RID: 13475
		[SerializeField]
		[Tooltip("null許容.")]
		private Text m_Title;

		// Token: 0x040034A4 RID: 13476
		[SerializeField]
		[Tooltip("null許容.")]
		private Image m_Image;

		// Token: 0x040034A5 RID: 13477
		[SerializeField]
		[Tooltip("null許容.")]
		private InputField m_InputField;

		// Token: 0x040034A6 RID: 13478
		[SerializeField]
		[Tooltip("null許容.InputFieldが有効な場合に出すアイコン.")]
		private GameObject m_ActiveEditIcon;
	}
}
