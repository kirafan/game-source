﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200082A RID: 2090
	public class WeaponStatus : MonoBehaviour
	{
		// Token: 0x06002B99 RID: 11161 RVA: 0x000E5AB4 File Offset: 0x000E3EB4
		public void Apply(int[] values, int[] addValues = null)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < 4; i++)
			{
				if (this.m_ValueText[i] != null)
				{
					this.m_ValueText[i].text = values[i].ToString();
				}
			}
			if (addValues != null)
			{
				for (int j = 0; j < 4; j++)
				{
					if (this.m_AddValueText[j] != null)
					{
						stringBuilder.Length = 0;
						stringBuilder.Append(values[j] + addValues[j]);
						if (addValues[j] > 0)
						{
							stringBuilder = UIUtility.GetIncreaseString(stringBuilder);
						}
						this.m_AddValueText[j].text = stringBuilder.ToString();
					}
				}
			}
			else if (this.m_AddValueText != null && this.m_AddValueText.Length > 0)
			{
				for (int k = 0; k < 4; k++)
				{
					if (this.m_AddValueText[k] != null)
					{
						this.m_AddValueText[k].gameObject.SetActive(false);
					}
				}
			}
		}

		// Token: 0x04003204 RID: 12804
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x04003205 RID: 12805
		[SerializeField]
		private Text[] m_AddValueText;

		// Token: 0x0200082B RID: 2091
		public enum eStatus
		{
			// Token: 0x04003207 RID: 12807
			Atk,
			// Token: 0x04003208 RID: 12808
			Mgc,
			// Token: 0x04003209 RID: 12809
			Def,
			// Token: 0x0400320A RID: 12810
			MDef,
			// Token: 0x0400320B RID: 12811
			Num
		}
	}
}
