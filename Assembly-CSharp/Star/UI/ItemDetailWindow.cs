﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200088A RID: 2186
	public class ItemDetailWindow : UIGroup
	{
		// Token: 0x06002D62 RID: 11618 RVA: 0x000EFCB6 File Offset: 0x000EE0B6
		public void OpenItem(int itemID)
		{
			base.Open();
			this.m_Detail.ApplyItem(itemID, -1);
		}

		// Token: 0x06002D63 RID: 11619 RVA: 0x000EFCCB File Offset: 0x000EE0CB
		public void OpenGem()
		{
			base.Open();
			this.m_Detail.ApplyGem(-1);
		}

		// Token: 0x06002D64 RID: 11620 RVA: 0x000EFCDF File Offset: 0x000EE0DF
		public void OpenGold()
		{
			base.Open();
			this.m_Detail.ApplyGold(-1);
		}

		// Token: 0x06002D65 RID: 11621 RVA: 0x000EFCF3 File Offset: 0x000EE0F3
		public void OpenKP()
		{
			base.Open();
			this.m_Detail.ApplyKirara(-1);
		}

		// Token: 0x06002D66 RID: 11622 RVA: 0x000EFD08 File Offset: 0x000EE108
		public void OpenPresent(ePresentType type, int id)
		{
			base.Open();
			switch (type)
			{
			case ePresentType.KRRPoint:
				this.OpenKP();
				break;
			case ePresentType.Gold:
				this.OpenGold();
				break;
			case ePresentType.UnlimitedGem:
			case ePresentType.LimitedGem:
				this.OpenGem();
				break;
			default:
				if (type == ePresentType.Item)
				{
					this.OpenItem(id);
				}
				break;
			}
		}

		// Token: 0x0400346F RID: 13423
		[SerializeField]
		private ItemDetailDisplay m_Detail;
	}
}
