﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000864 RID: 2148
	public class ItemIconWithDetail : MonoBehaviour
	{
		// Token: 0x06002C9B RID: 11419 RVA: 0x000EB978 File Offset: 0x000E9D78
		public void Apply(int itemID)
		{
			this.m_ItemID = itemID;
			this.m_IconCloner.GetInst<ItemIconWithFrame>().Apply(itemID);
		}

		// Token: 0x06002C9C RID: 11420 RVA: 0x000EB992 File Offset: 0x000E9D92
		public void OnClickCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ItemDetailWindow.OpenItem(this.m_ItemID);
		}

		// Token: 0x0400339B RID: 13211
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x0400339C RID: 13212
		private int m_ItemID = -1;
	}
}
