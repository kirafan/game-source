﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009C9 RID: 2505
	public class LoadingUIAccessories : MonoBehaviour
	{
		// Token: 0x0600340D RID: 13325 RVA: 0x001080BD File Offset: 0x001064BD
		protected void Start()
		{
		}

		// Token: 0x0600340E RID: 13326 RVA: 0x001080C0 File Offset: 0x001064C0
		public void Setup()
		{
			this.m_LabelImage.gameObject.SetActive(false);
			for (int i = 0; i < this.m_LabelDots.Length; i++)
			{
				this.m_LabelDots[i].gameObject.SetActive(false);
			}
			this.m_MoveLoadIconGameObject.SetActive(false);
			this.m_MatchAct = false;
		}

		// Token: 0x0600340F RID: 13327 RVA: 0x00108120 File Offset: 0x00106520
		public void Open()
		{
			this.m_LabelImage.gameObject.SetActive(true);
			for (int i = 0; i < this.m_LabelDots.Length; i++)
			{
				this.m_LabelDots[i].gameObject.SetActive(false);
			}
			this.m_MoveLoadIconGameObject.SetActive(true);
			this.m_MatchAct = true;
			this.m_Count = 0f;
		}

		// Token: 0x06003410 RID: 13328 RVA: 0x00108188 File Offset: 0x00106588
		public void Close()
		{
			this.m_LabelImage.gameObject.SetActive(false);
			for (int i = 0; i < this.m_LabelDots.Length; i++)
			{
				this.m_LabelDots[i].gameObject.SetActive(false);
			}
			this.m_MoveLoadIconGameObject.SetActive(false);
			this.m_MatchAct = false;
		}

		// Token: 0x06003411 RID: 13329 RVA: 0x001081E8 File Offset: 0x001065E8
		private void Update()
		{
			if (this.m_LabelImage.gameObject.activeSelf)
			{
				for (int i = 0; i < this.m_LabelDots.Length; i++)
				{
					this.m_LabelDots[i].gameObject.SetActive((float)(i + 1) < this.m_Count);
				}
				if (this.m_Count >= 4f)
				{
					this.m_Count = 0f;
				}
				this.m_Count += 4f * Time.deltaTime;
			}
			if (this.m_MoveLoadIconGameObject.activeSelf)
			{
				this.m_MoveLoadIconGameObject.SetActive(this.m_MatchAct);
			}
		}

		// Token: 0x04003A51 RID: 14929
		private const float LOOPTIME = 1f;

		// Token: 0x04003A52 RID: 14930
		[SerializeField]
		[Tooltip("読み込み中表示本体.")]
		private Image m_LabelImage;

		// Token: 0x04003A53 RID: 14931
		[SerializeField]
		[Tooltip("読み込み中表示本体.")]
		private Image[] m_LabelDots;

		// Token: 0x04003A54 RID: 14932
		private float m_Count;

		// Token: 0x04003A55 RID: 14933
		[SerializeField]
		[Tooltip("アニメーションを行うキャラクタ.")]
		private GameObject m_MoveLoadIconGameObject;

		// Token: 0x04003A56 RID: 14934
		private bool m_MatchAct = true;
	}
}
