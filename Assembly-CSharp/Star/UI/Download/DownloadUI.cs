﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Download
{
	// Token: 0x020008DA RID: 2266
	public class DownloadUI : MenuUIBase
	{
		// Token: 0x06002EF1 RID: 12017 RVA: 0x000F57CC File Offset: 0x000F3BCC
		public void Setup(bool isPlayMovie)
		{
			this.m_GameObject = base.gameObject;
			this.m_Gauge.fillAmount = 0f;
			this.m_Text.text = string.Empty;
			this.m_Scroll.Setup();
			this.m_IsPlayMovie = isPlayMovie;
			if (!this.m_IsPlayMovie)
			{
				this.m_LoadIdx = 0;
				for (int i = 0; i < 3; i++)
				{
					DownloadImageItemData item = new DownloadImageItemData();
					this.m_ItemList.Add(item);
					this.m_Scroll.AddItem(this.m_ItemList[i]);
				}
				this.m_MovieObject.SetActive(false);
			}
			else
			{
				this.m_LoadIdx = -1;
				this.m_VisibleOnPlayMovie.SetActive(false);
				this.m_InvisibleOnPlayMoive.SetActive(false);
				this.m_BG.SetActive(false);
			}
		}

		// Token: 0x06002EF2 RID: 12018 RVA: 0x000F58A0 File Offset: 0x000F3CA0
		private void Update()
		{
			if (this.m_LoadIdx >= 0 && this.m_LoadIdx < 3)
			{
				if (this.m_Gauge.fillAmount < 0.9f && this.m_ResReq == null && this.m_LoadIdx < 3)
				{
					this.m_ResReq = Resources.LoadAsync<Sprite>("Texture/Download/Download" + this.m_LoadIdx.ToString());
				}
				if (this.m_ResReq != null && this.m_ResReq.isDone)
				{
					Sprite sprite = this.m_ResReq.asset as Sprite;
					this.m_ItemList[this.m_LoadIdx].m_Sprite = sprite;
					this.m_Scroll.ForceApply();
					this.m_ResReq = null;
					this.m_LoadIdx++;
				}
			}
			switch (this.m_Step)
			{
			case DownloadUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(DownloadUI.eStep.Idle);
				}
				break;
			}
			case DownloadUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(DownloadUI.eStep.Unload);
				}
				break;
			}
			case DownloadUI.eStep.Unload:
				if (this.m_ResReq == null)
				{
					this.m_Scroll.Destroy();
					for (int k = 0; k < this.m_ItemList.Count; k++)
					{
						Resources.UnloadAsset(this.m_ItemList[k].m_Sprite);
					}
					this.ChangeStep(DownloadUI.eStep.End);
				}
				break;
			}
		}

		// Token: 0x06002EF3 RID: 12019 RVA: 0x000F5AC8 File Offset: 0x000F3EC8
		private void ChangeStep(DownloadUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case DownloadUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case DownloadUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case DownloadUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case DownloadUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case DownloadUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06002EF4 RID: 12020 RVA: 0x000F5B88 File Offset: 0x000F3F88
		public override void PlayIn()
		{
			this.ChangeStep(DownloadUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06002EF5 RID: 12021 RVA: 0x000F5BD0 File Offset: 0x000F3FD0
		public override void PlayOut()
		{
			this.ChangeStep(DownloadUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06002EF6 RID: 12022 RVA: 0x000F5C15 File Offset: 0x000F4015
		public override bool IsMenuEnd()
		{
			return this.m_Step == DownloadUI.eStep.End;
		}

		// Token: 0x06002EF7 RID: 12023 RVA: 0x000F5C20 File Offset: 0x000F4020
		public int GetLoadedSprNum()
		{
			return this.m_LoadIdx;
		}

		// Token: 0x06002EF8 RID: 12024 RVA: 0x000F5C28 File Offset: 0x000F4028
		public void UpdateDownloadStatus(float progress)
		{
			progress = Mathf.Clamp(progress, 0f, 1f);
			if (this.m_Progress != progress)
			{
				this.m_Progress = progress;
				this.m_Gauge.fillAmount = this.m_Progress;
				this.m_sb.Length = 0;
				this.m_sb.Append((this.m_Progress * 100f).ToString("F1"));
				this.m_sb.Append("％");
				this.m_Text.text = this.m_sb.ToString();
			}
		}

		// Token: 0x06002EF9 RID: 12025 RVA: 0x000F5CC4 File Offset: 0x000F40C4
		public void TouchDisplay()
		{
			if (this.m_IsPlayMovie)
			{
				this.m_VisibleOnPlayMovie.SetActive(true);
				if (this.m_Coroutine_DiactivateVisibleOnPlayMovieObject != null)
				{
					base.StopCoroutine(this.m_Coroutine_DiactivateVisibleOnPlayMovieObject);
					this.m_Coroutine_DiactivateVisibleOnPlayMovieObject = null;
				}
				this.m_Coroutine_DiactivateVisibleOnPlayMovieObject = base.StartCoroutine(this.DiactivateVisibleOnPlayMovieObject(5f));
			}
		}

		// Token: 0x06002EFA RID: 12026 RVA: 0x000F5D20 File Offset: 0x000F4120
		private IEnumerator DiactivateVisibleOnPlayMovieObject(float waitTime)
		{
			yield return new WaitForSeconds(waitTime);
			this.m_VisibleOnPlayMovie.SetActive(false);
			yield break;
		}

		// Token: 0x040035FC RID: 13820
		private const int SPRITE_MAX = 3;

		// Token: 0x040035FD RID: 13821
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040035FE RID: 13822
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040035FF RID: 13823
		[SerializeField]
		private GameObject m_BG;

		// Token: 0x04003600 RID: 13824
		[SerializeField]
		private GameObject m_MovieObject;

		// Token: 0x04003601 RID: 13825
		[SerializeField]
		private GameObject m_InvisibleOnPlayMoive;

		// Token: 0x04003602 RID: 13826
		[SerializeField]
		private GameObject m_VisibleOnPlayMovie;

		// Token: 0x04003603 RID: 13827
		[SerializeField]
		private InfiniteScroll m_Scroll;

		// Token: 0x04003604 RID: 13828
		[SerializeField]
		private GameObject m_ScrObj;

		// Token: 0x04003605 RID: 13829
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04003606 RID: 13830
		[SerializeField]
		private Text m_Text;

		// Token: 0x04003607 RID: 13831
		private DownloadUI.eStep m_Step;

		// Token: 0x04003608 RID: 13832
		private bool m_IsPlayMovie;

		// Token: 0x04003609 RID: 13833
		private float m_Progress = -1f;

		// Token: 0x0400360A RID: 13834
		private List<DownloadImageItemData> m_ItemList = new List<DownloadImageItemData>();

		// Token: 0x0400360B RID: 13835
		private ResourceRequest m_ResReq;

		// Token: 0x0400360C RID: 13836
		private int m_LoadIdx = -1;

		// Token: 0x0400360D RID: 13837
		private Coroutine m_Coroutine_DiactivateVisibleOnPlayMovieObject;

		// Token: 0x0400360E RID: 13838
		private StringBuilder m_sb = new StringBuilder();

		// Token: 0x020008DB RID: 2267
		private enum eStep
		{
			// Token: 0x04003610 RID: 13840
			Hide,
			// Token: 0x04003611 RID: 13841
			In,
			// Token: 0x04003612 RID: 13842
			Idle,
			// Token: 0x04003613 RID: 13843
			Out,
			// Token: 0x04003614 RID: 13844
			Unload,
			// Token: 0x04003615 RID: 13845
			End
		}
	}
}
