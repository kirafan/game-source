﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000887 RID: 2183
	public class PageSignalButton : MonoBehaviour
	{
		// Token: 0x1400006C RID: 108
		// (add) Token: 0x06002D54 RID: 11604 RVA: 0x000EF920 File Offset: 0x000EDD20
		// (remove) Token: 0x06002D55 RID: 11605 RVA: 0x000EF958 File Offset: 0x000EDD58
		public event Action<int> OnClick;

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06002D56 RID: 11606 RVA: 0x000EF98E File Offset: 0x000EDD8E
		// (set) Token: 0x06002D57 RID: 11607 RVA: 0x000EF996 File Offset: 0x000EDD96
		public int Idx { get; set; }

		// Token: 0x06002D58 RID: 11608 RVA: 0x000EF99F File Offset: 0x000EDD9F
		public void SetSprite(Sprite sprite)
		{
			this.m_Image.sprite = sprite;
		}

		// Token: 0x06002D59 RID: 11609 RVA: 0x000EF9AD File Offset: 0x000EDDAD
		public void OnClickButtonCallBack()
		{
			this.OnClick.Call(this.Idx);
		}

		// Token: 0x04003464 RID: 13412
		[SerializeField]
		private Image m_Image;
	}
}
