﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007DC RID: 2012
	public class SelectedCharaInfoLevelData : MonoBehaviour
	{
		// Token: 0x06002975 RID: 10613 RVA: 0x000DBB31 File Offset: 0x000D9F31
		public void Setup()
		{
		}

		// Token: 0x06002976 RID: 10614 RVA: 0x000DBB34 File Offset: 0x000D9F34
		public void SetBeforeLevel(int nowLevel, int maxLevel)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			stringBuilder.Append(nowLevel.ToString().PadLeft(3));
			stringBuilder.Append(" / ");
			stringBuilder.Append(maxLevel.ToString().PadLeft(3));
			this.m_BeforeLevelText.text = stringBuilder.ToString();
		}

		// Token: 0x06002977 RID: 10615 RVA: 0x000DBBA0 File Offset: 0x000D9FA0
		public void SetAfterLevel(int nowLevel, int maxLevel)
		{
			if (this.m_AfterLevelText != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Length = 0;
				stringBuilder.Append(nowLevel.ToString().PadLeft(3));
				stringBuilder.Append("/");
				stringBuilder.Append(maxLevel.ToString().PadLeft(3));
				this.m_AfterLevelText.text = stringBuilder.ToString();
			}
		}

		// Token: 0x06002978 RID: 10616 RVA: 0x000DBC1C File Offset: 0x000DA01C
		public void PlayAfterLevelAnimation()
		{
			if (this.m_AfterLevelTextAnim != null)
			{
				this.m_AfterLevelTextAnim.PlayIn();
			}
		}

		// Token: 0x06002979 RID: 10617 RVA: 0x000DBC3A File Offset: 0x000DA03A
		public int GetAfterLevelAnimationState()
		{
			if (this.m_AfterLevelTextAnim != null)
			{
				return this.m_AfterLevelTextAnim.State;
			}
			return -1;
		}

		// Token: 0x0600297A RID: 10618 RVA: 0x000DBC5A File Offset: 0x000DA05A
		public void SetBeforeLimitBreakIconValue(int value)
		{
			if (this.m_BeforeLimitBreakIcon != null)
			{
				this.m_BeforeLimitBreakIcon.SetValue(value);
			}
		}

		// Token: 0x0600297B RID: 10619 RVA: 0x000DBC79 File Offset: 0x000DA079
		public void SetAfterLimitBreakIconValue(int value)
		{
			if (this.m_AfterLimitBreakIcon != null)
			{
				this.m_AfterLimitBreakIcon.SetValue(value);
			}
		}

		// Token: 0x0600297C RID: 10620 RVA: 0x000DBC98 File Offset: 0x000DA098
		public Text GetAfterLevelText()
		{
			return this.m_AfterLevelText;
		}

		// Token: 0x04003004 RID: 12292
		[SerializeField]
		private LimitBreakIcon m_BeforeLimitBreakIcon;

		// Token: 0x04003005 RID: 12293
		[SerializeField]
		private Text m_BeforeLevelText;

		// Token: 0x04003006 RID: 12294
		[SerializeField]
		private LimitBreakIcon m_AfterLimitBreakIcon;

		// Token: 0x04003007 RID: 12295
		[SerializeField]
		private Text m_AfterLevelText;

		// Token: 0x04003008 RID: 12296
		[SerializeField]
		private AnimUIPlayer m_AfterLevelTextAnim;
	}
}
