﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000830 RID: 2096
	[ExecuteInEditMode]
	public class CustomButtonGroup : MonoBehaviour
	{
		// Token: 0x06002BBB RID: 11195 RVA: 0x000E6D9D File Offset: 0x000E519D
		private void OnValidate()
		{
			this.IsEnableInput = this.m_IsEnableInput;
			this.Interactable = this.m_IsInteractable;
		}

		// Token: 0x06002BBC RID: 11196 RVA: 0x000E6DB7 File Offset: 0x000E51B7
		private void Start()
		{
			this.IsEnableInput = this.m_IsEnableInput;
			this.Interactable = this.m_IsInteractable;
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06002BBE RID: 11198 RVA: 0x000E6DE0 File Offset: 0x000E51E0
		// (set) Token: 0x06002BBD RID: 11197 RVA: 0x000E6DD1 File Offset: 0x000E51D1
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInput && this.m_IsEnableInputParent;
			}
			set
			{
				this.m_IsEnableInput = value;
				this.UpdateInput();
			}
		}

		// Token: 0x170002CF RID: 719
		// (set) Token: 0x06002BBF RID: 11199 RVA: 0x000E6DF6 File Offset: 0x000E51F6
		public bool IsEnableInputParent
		{
			set
			{
				this.m_IsEnableInputParent = value;
			}
		}

		// Token: 0x06002BC0 RID: 11200 RVA: 0x000E6E00 File Offset: 0x000E5200
		private void UpdateInput()
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].IsEnableInputParent = this.IsEnableInput;
			}
			CustomButtonGroup[] componentsInChildren2 = base.GetComponentsInChildren<CustomButtonGroup>(true);
			for (int j = 1; j < componentsInChildren2.Length; j++)
			{
				componentsInChildren2[j].m_IsEnableInputParent = this.IsEnableInput;
				componentsInChildren2[j].UpdateInput();
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06002BC2 RID: 11202 RVA: 0x000E6EB2 File Offset: 0x000E52B2
		// (set) Token: 0x06002BC1 RID: 11201 RVA: 0x000E6E6C File Offset: 0x000E526C
		public bool Interactable
		{
			get
			{
				return this.m_IsInteractable;
			}
			set
			{
				this.m_IsInteractable = value;
				CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>(true);
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					if (!componentsInChildren[i].IgnoreGroup)
					{
						componentsInChildren[i].InteractableParent = value;
					}
				}
			}
		}

		// Token: 0x04003253 RID: 12883
		[SerializeField]
		private bool m_IsEnableInput = true;

		// Token: 0x04003254 RID: 12884
		[SerializeField]
		private bool m_IsInteractable = true;

		// Token: 0x04003255 RID: 12885
		private bool m_IsEnableInputParent = true;
	}
}
