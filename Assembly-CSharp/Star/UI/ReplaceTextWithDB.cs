﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200089F RID: 2207
	public class ReplaceTextWithDB : MonoBehaviour
	{
		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06002DCD RID: 11725 RVA: 0x000F1229 File Offset: 0x000EF629
		public ReplaceTextWithDB.eDBType DBType
		{
			get
			{
				return this.m_DBType;
			}
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06002DCE RID: 11726 RVA: 0x000F1231 File Offset: 0x000EF631
		public eText_CommonDB CommonIndex
		{
			get
			{
				return this.m_CommonIndex;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06002DCF RID: 11727 RVA: 0x000F1239 File Offset: 0x000EF639
		public eText_MessageDB MessageIndex
		{
			get
			{
				return this.m_MessageIndex;
			}
		}

		// Token: 0x06002DD0 RID: 11728 RVA: 0x000F1241 File Offset: 0x000EF641
		private void Start()
		{
			this.m_IsReplaced = false;
			this.m_Text = base.GetComponent<Text>();
		}

		// Token: 0x06002DD1 RID: 11729 RVA: 0x000F1256 File Offset: 0x000EF656
		private void Update()
		{
			if (!this.m_IsReplaced)
			{
				this.ReplaceInRuntime();
			}
		}

		// Token: 0x06002DD2 RID: 11730 RVA: 0x000F126C File Offset: 0x000EF66C
		private void ReplaceInRuntime()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				if (this.m_DBType == ReplaceTextWithDB.eDBType.Common)
				{
					this.m_Text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(this.m_CommonIndex);
				}
				else
				{
					this.m_Text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(this.m_MessageIndex);
				}
				this.m_IsReplaced = true;
			}
		}

		// Token: 0x040034CF RID: 13519
		[SerializeField]
		private ReplaceTextWithDB.eDBType m_DBType = ReplaceTextWithDB.eDBType.Common;

		// Token: 0x040034D0 RID: 13520
		[SerializeField]
		private eText_CommonDB m_CommonIndex;

		// Token: 0x040034D1 RID: 13521
		[SerializeField]
		private eText_MessageDB m_MessageIndex;

		// Token: 0x040034D2 RID: 13522
		private Text m_Text;

		// Token: 0x040034D3 RID: 13523
		private bool m_IsReplaced;

		// Token: 0x020008A0 RID: 2208
		public enum eDBType
		{
			// Token: 0x040034D5 RID: 13525
			None,
			// Token: 0x040034D6 RID: 13526
			Common,
			// Token: 0x040034D7 RID: 13527
			Message
		}
	}
}
