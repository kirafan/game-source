﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007BE RID: 1982
	public class SelectedCharaInfoModelArea : SelectedCharaInfoCharacterViewBase
	{
		// Token: 0x06002920 RID: 10528 RVA: 0x000DA8DE File Offset: 0x000D8CDE
		[Obsolete]
		public void Setup(long charaMngID)
		{
			this.Setup();
			this.RequestLoad(charaMngID, true);
		}

		// Token: 0x06002921 RID: 10529 RVA: 0x000DA8EE File Offset: 0x000D8CEE
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x06002922 RID: 10530 RVA: 0x000DA8F8 File Offset: 0x000D8CF8
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			SelectedCharaInfoCharacterViewBase.eState state = base.GetState();
			if (state != SelectedCharaInfoCharacterViewBase.eState.Wait && state != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			this.m_CharaHandl = SingletonMonoBehaviour<GameSystem>.Inst.PrepareCharaHndlForMenu(charaID, weaponID, null);
			this.SetHideActive(false);
			base.RequestLoad(charaID, weaponID, toStateAutomaticallyAdvance);
		}

		// Token: 0x06002923 RID: 10531 RVA: 0x000DA940 File Offset: 0x000D8D40
		public void SetStencilParamForResult(int idx)
		{
			List<Material> materials = this.m_CharaHandl.CharaAnim.GetMaterials();
			if (materials != null)
			{
				idx = Mathf.Clamp(idx, 0, 255);
				for (int i = 0; i < materials.Count; i++)
				{
					MeigeShaderUtility.SetOutlineStencilID(materials[i], (byte)(255 - idx));
					MeigeShaderUtility.SetOutlineStencilCompare(materials[i], eCompareFunc.Equal);
					MeigeShaderUtility.SetOutlineStencilOp(materials[i], eStencilOp.Keep);
					MeigeShaderUtility.SetStencilID(materials[i], (byte)(255 - idx));
					MeigeShaderUtility.SetStencilCompare(materials[i], eCompareFunc.Equal);
					MeigeShaderUtility.SetStencilOp(materials[i], eStencilOp.Keep);
				}
			}
		}

		// Token: 0x06002924 RID: 10532 RVA: 0x000DA9E8 File Offset: 0x000D8DE8
		public override void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepare && base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			if (this.m_CharaFrameImage != null && this.m_CharaFrameImage.sprite != null && this.m_RarityFrameTexture[(int)this.m_CharaHandl.CharaParam.RareType] != null)
			{
				this.m_CharaFrameImage.enabled = true;
				this.m_CharaFrameImage.sprite = Sprite.Create(this.m_RarityFrameTexture[(int)this.m_CharaHandl.CharaParam.RareType], this.m_CharaFrameImage.sprite.rect, Vector2.zero);
				if (this.m_BGImage != null)
				{
					for (int i = 0; i < this.m_BGImage.Length; i++)
					{
						this.m_BGImage[i].enabled = true;
					}
				}
				this.SetStencilParamForResult(0);
			}
			this.m_CharaHandl.CharaAnim.ChangeDir(this.m_NowDir, false);
			this.m_CharaHandl.CacheTransform.SetParent(this.m_CharaParent);
			this.m_CharaHandl.CacheTransform.localPosition = new Vector3(0f, 0f, -200f);
			this.m_CharaHandl.CacheTransform.localScale = Vector3.one * 500f;
			int num = UIDefine.SORT_ORDER[1];
			if (this.m_UICanvas != null)
			{
				num = this.m_UICanvas.sortingOrder;
			}
			this.m_CharaHandl.CharaAnim.SetSortingOrder(num + 1);
			if (this.m_HideCanvas != null)
			{
				this.m_HideCanvas.overrideSorting = true;
				this.m_HideCanvas.sortingOrder = num + 2;
			}
			this.m_CharaHide.Show();
			this.m_CharaHide.PlayOut();
			base.PlayIn(toStateAutomaticallyAdvance);
		}

		// Token: 0x06002925 RID: 10533 RVA: 0x000DABC8 File Offset: 0x000D8FC8
		public override void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Idle)
			{
				return;
			}
			if (this.m_CharaFrameImage != null && this.m_CharaFrameImage.sprite != null)
			{
				this.m_CharaFrameImage.enabled = false;
				if (this.m_BGImage != null)
				{
					for (int i = 0; i < this.m_BGImage.Length; i++)
					{
						this.m_BGImage[i].enabled = false;
					}
				}
			}
			this.HidePlayIn();
			base.PlayOut(toStateAutomaticallyAdvance);
		}

		// Token: 0x06002926 RID: 10534 RVA: 0x000DAC54 File Offset: 0x000D9054
		public override void FinishWait()
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Out)
			{
				return;
			}
			this.SetHideActive(false);
			SingletonMonoBehaviour<GameSystem>.Inst.SuspendCharaHndlForMenu();
			base.FinishWait();
		}

		// Token: 0x06002927 RID: 10535 RVA: 0x000DAC7A File Offset: 0x000D907A
		public void HidePlayIn()
		{
			this.m_CharaHide.PlayIn();
		}

		// Token: 0x06002928 RID: 10536 RVA: 0x000DAC87 File Offset: 0x000D9087
		public override bool IsPreparing()
		{
			return this.m_CharaHandl.IsPreparing();
		}

		// Token: 0x06002929 RID: 10537 RVA: 0x000DAC94 File Offset: 0x000D9094
		public override bool IsAnimStateIn()
		{
			return base.GetState() == SelectedCharaInfoCharacterViewBase.eState.In && this.GetHideState() == 3;
		}

		// Token: 0x0600292A RID: 10538 RVA: 0x000DACAE File Offset: 0x000D90AE
		public override bool IsAnimStateOut()
		{
			return base.GetState() == SelectedCharaInfoCharacterViewBase.eState.Out && this.GetHideState() == 1;
		}

		// Token: 0x0600292B RID: 10539 RVA: 0x000DACC8 File Offset: 0x000D90C8
		public void SetHideActive(bool active)
		{
			this.m_CharaHide.gameObject.SetActive(active);
		}

		// Token: 0x0600292C RID: 10540 RVA: 0x000DACDB File Offset: 0x000D90DB
		public int GetHideState()
		{
			return this.m_CharaHide.State;
		}

		// Token: 0x0600292D RID: 10541 RVA: 0x000DACE8 File Offset: 0x000D90E8
		public void OnPushButton()
		{
			if (this.m_CharaHandl != null && this.m_CharaHandl.CharaAnim != null && this.m_CharaHandl.IsDonePreparePerfect())
			{
				this.m_NowDir = ((this.m_NowDir != CharacterDefine.eDir.L) ? CharacterDefine.eDir.L : CharacterDefine.eDir.R);
				this.m_CharaHandl.CharaAnim.ChangeDir(this.m_NowDir, false);
				this.m_CharaHandl.CharaMenu.RequestIdleMode(0f, 0f);
			}
		}

		// Token: 0x04002FC1 RID: 12225
		[SerializeField]
		[Tooltip("Modelの親.")]
		private RectTransform m_CharaParent;

		// Token: 0x04002FC2 RID: 12226
		[SerializeField]
		[Tooltip("一旦不使用.該当Imageコンポーネント無効中.")]
		private AnimUIPlayer m_CharaHide;

		// Token: 0x04002FC3 RID: 12227
		[SerializeField]
		[Tooltip("指定したCanvasのSortingOrderより上にModelを置く.")]
		private Canvas m_UICanvas;

		// Token: 0x04002FC4 RID: 12228
		[SerializeField]
		[Tooltip("一旦不使用.該当Imageコンポーネント無効中.")]
		private Canvas m_HideCanvas;

		// Token: 0x04002FC5 RID: 12229
		private CharacterHandler m_CharaHandl;

		// Token: 0x04002FC6 RID: 12230
		[SerializeField]
		private Image m_CharaFrameImage;

		// Token: 0x04002FC7 RID: 12231
		[SerializeField]
		private Image[] m_BGImage;

		// Token: 0x04002FC8 RID: 12232
		[SerializeField]
		private Texture2D[] m_RarityFrameTexture = new Texture2D[5];

		// Token: 0x04002FC9 RID: 12233
		private CharacterDefine.eDir m_NowDir;
	}
}
