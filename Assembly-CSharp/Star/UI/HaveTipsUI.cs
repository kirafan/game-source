﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009B3 RID: 2483
	public class HaveTipsUI : MonoBehaviour
	{
		// Token: 0x0600338C RID: 13196 RVA: 0x00105E2E File Offset: 0x0010422E
		protected virtual void Start()
		{
			this.m_Body.SetActive(false);
			if (this.m_TipsWindowButton != null)
			{
				this.m_TipsWindowButton.enabled = false;
			}
			this.m_TipsWindow.Setup();
		}

		// Token: 0x0600338D RID: 13197 RVA: 0x00105E64 File Offset: 0x00104264
		public virtual void Abort()
		{
			this.m_Body.SetActive(false);
			this.CloseTips();
		}

		// Token: 0x0600338E RID: 13198 RVA: 0x00105E78 File Offset: 0x00104278
		protected virtual void Open(eTipsCategory tipsCategory)
		{
			this.m_TipsCategory = tipsCategory;
			this.m_Body.SetActive(true);
			this.m_Step = HaveTipsUI.eStep.In;
		}

		// Token: 0x0600338F RID: 13199 RVA: 0x00105E94 File Offset: 0x00104294
		public virtual void Close()
		{
			this.m_Body.SetActive(true);
			this.CloseTips();
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(0f);
			this.m_Step = HaveTipsUI.eStep.Out;
		}

		// Token: 0x06003390 RID: 13200 RVA: 0x00105EC3 File Offset: 0x001042C3
		public virtual bool IsComplete()
		{
			return this.m_Step != HaveTipsUI.eStep.In && this.m_Step != HaveTipsUI.eStep.Out;
		}

		// Token: 0x06003391 RID: 13201 RVA: 0x00105EE0 File Offset: 0x001042E0
		private void Update()
		{
			switch (this.m_Step)
			{
			case HaveTipsUI.eStep.In:
				this.UpdateIn();
				break;
			case HaveTipsUI.eStep.WaitTipsOpen:
				this.UpdateWaitTipsOpen();
				break;
			case HaveTipsUI.eStep.Overray:
				this.UpdateOverray();
				break;
			case HaveTipsUI.eStep.Out:
				this.UpdateOut();
				break;
			}
		}

		// Token: 0x06003392 RID: 13202 RVA: 0x00105F3B File Offset: 0x0010433B
		protected virtual void UpdateIn()
		{
			if (this.IsOpenTips())
			{
				this.PlayTipsAnimIn();
			}
			if (this.m_TipsWindowAnim.State == 1)
			{
				this.m_Step = HaveTipsUI.eStep.WaitTipsOpen;
			}
			else
			{
				this.m_Step = HaveTipsUI.eStep.Overray;
			}
		}

		// Token: 0x06003393 RID: 13203 RVA: 0x00105F72 File Offset: 0x00104372
		protected virtual void UpdateWaitTipsOpen()
		{
			if (this.m_TipsWindowAnim.State == 1)
			{
				return;
			}
			this.PlayTips();
			this.m_Step = HaveTipsUI.eStep.Overray;
		}

		// Token: 0x06003394 RID: 13204 RVA: 0x00105F93 File Offset: 0x00104393
		protected virtual void UpdateOverray()
		{
		}

		// Token: 0x06003395 RID: 13205 RVA: 0x00105F95 File Offset: 0x00104395
		protected virtual void UpdateOut()
		{
			this.m_Body.SetActive(false);
			this.m_Step = HaveTipsUI.eStep.None;
		}

		// Token: 0x06003396 RID: 13206 RVA: 0x00105FAA File Offset: 0x001043AA
		protected virtual bool IsOpenTips()
		{
			return this.m_TipsCategory != eTipsCategory.None;
		}

		// Token: 0x06003397 RID: 13207 RVA: 0x00105FB8 File Offset: 0x001043B8
		protected virtual void PlayTipsAnimIn()
		{
			if (this.m_TipsCategory != eTipsCategory.None)
			{
				this.m_TipsWindowAnim.PlayIn();
			}
		}

		// Token: 0x06003398 RID: 13208 RVA: 0x00105FD1 File Offset: 0x001043D1
		protected virtual void PlayTips()
		{
			this.m_TipsWindow.Play(this.m_TipsCategory);
			if (this.m_TipsWindowButton != null)
			{
				this.m_TipsWindowButton.enabled = true;
			}
		}

		// Token: 0x06003399 RID: 13209 RVA: 0x00106004 File Offset: 0x00104404
		protected virtual void CloseTips()
		{
			this.m_TipsWindow.Stop();
			if (this.m_TipsWindowAnim.State == 1)
			{
				this.m_TipsWindowAnim.Show();
			}
			this.m_TipsWindowAnim.PlayOut();
			if (this.m_TipsWindowButton != null)
			{
				this.m_TipsWindowButton.enabled = false;
			}
		}

		// Token: 0x040039CA RID: 14794
		[SerializeField]
		protected GameObject m_Body;

		// Token: 0x040039CB RID: 14795
		[SerializeField]
		protected LoadingUITipsWindow m_TipsWindow;

		// Token: 0x040039CC RID: 14796
		[SerializeField]
		private AnimUIPlayer m_TipsWindowAnim;

		// Token: 0x040039CD RID: 14797
		[SerializeField]
		private CustomButton m_TipsWindowButton;

		// Token: 0x040039CE RID: 14798
		private HaveTipsUI.eStep m_Step = HaveTipsUI.eStep.None;

		// Token: 0x040039CF RID: 14799
		private eTipsCategory m_TipsCategory = eTipsCategory.None;

		// Token: 0x020009B4 RID: 2484
		public enum eStep
		{
			// Token: 0x040039D1 RID: 14801
			None = -1,
			// Token: 0x040039D2 RID: 14802
			In,
			// Token: 0x040039D3 RID: 14803
			WaitTipsOpen,
			// Token: 0x040039D4 RID: 14804
			Overray,
			// Token: 0x040039D5 RID: 14805
			Out
		}
	}
}
