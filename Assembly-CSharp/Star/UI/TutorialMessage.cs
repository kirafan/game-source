﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B10 RID: 2832
	public class TutorialMessage : MonoBehaviour
	{
		// Token: 0x06003B81 RID: 15233 RVA: 0x0012FE18 File Offset: 0x0012E218
		public void Open(eTutorialMessageListDB tutorialMessageID)
		{
			TutorialMessageListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TutorialMessageListDB.GetParam(tutorialMessageID);
			this.SetEnableDark(true, 1f, true);
			this.SetEnableMessage(true);
			this.SetMessage(param.m_Text);
			this.SetMessagePosition((TutorialMessage.eMessagePosition)param.m_Position);
			this.SetEnableArrow(false);
		}

		// Token: 0x06003B82 RID: 15234 RVA: 0x0012FE70 File Offset: 0x0012E270
		public void Clear()
		{
			this.SetEnableDark(false, 1f, true);
			this.SetEnableMessage(false);
			this.SetEnableArrow(false);
			this.SetEnableTrace(false);
			this.m_Body.SetActive(false);
		}

		// Token: 0x06003B83 RID: 15235 RVA: 0x0012FEA0 File Offset: 0x0012E2A0
		public void SetEnableDark(bool flg, float alpha = 1f, bool raycastTarget = true)
		{
			if (!this.m_Body.activeSelf)
			{
				this.m_Body.SetActive(true);
			}
			if (this.m_DarkObj.activeSelf != flg)
			{
				this.m_DarkObj.SetActive(flg);
			}
			Image component = this.m_DarkObj.GetComponent<Image>();
			if (component != null)
			{
				if (this.m_DardDefaultAlpha < 0f)
				{
					this.m_DardDefaultAlpha = component.color.a;
				}
				component.color = new Color(component.color.r, component.color.g, component.color.b, this.m_DardDefaultAlpha * alpha);
				component.raycastTarget = raycastTarget;
			}
		}

		// Token: 0x06003B84 RID: 15236 RVA: 0x0012FF68 File Offset: 0x0012E368
		public int GetDarkCanvasSortingOrder()
		{
			Canvas component = this.m_DarkObj.transform.parent.GetComponent<Canvas>();
			if (component != null)
			{
				return component.sortingOrder;
			}
			return 0;
		}

		// Token: 0x06003B85 RID: 15237 RVA: 0x0012FF9F File Offset: 0x0012E39F
		public void SetEnableMessage(bool flg)
		{
			if (!this.m_Body.activeSelf)
			{
				this.m_Body.SetActive(true);
			}
			this.m_MessageObj.SetActive(flg);
		}

		// Token: 0x06003B86 RID: 15238 RVA: 0x0012FFCC File Offset: 0x0012E3CC
		public void SetMessage(eTutorialMessageListDB tutorialMessageID)
		{
			TutorialMessageListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TutorialMessageListDB.GetParam(tutorialMessageID);
			this.SetMessage(param.m_Text);
			this.SetMessagePosition((TutorialMessage.eMessagePosition)param.m_Position);
		}

		// Token: 0x06003B87 RID: 15239 RVA: 0x00130009 File Offset: 0x0012E409
		public void SetMessage(string message)
		{
			this.m_MessageText.text = message;
		}

		// Token: 0x06003B88 RID: 15240 RVA: 0x00130018 File Offset: 0x0012E418
		public void SetMessagePosition(TutorialMessage.eMessagePosition pos)
		{
			RectTransform component = this.m_MessageObj.GetComponent<RectTransform>();
			switch (pos)
			{
			case TutorialMessage.eMessagePosition.RightTop:
				component.anchorMin = new Vector2(1f, 1f);
				component.anchorMax = new Vector2(1f, 1f);
				component.pivot = new Vector2(1f, 1f);
				component.anchoredPosition = new Vector2(-this.MESSAGE_OFFSET, -this.MESSAGE_OFFSET);
				break;
			case TutorialMessage.eMessagePosition.RightBottom:
				component.anchorMin = new Vector2(1f, 0f);
				component.anchorMax = new Vector2(1f, 0f);
				component.pivot = new Vector2(1f, 0f);
				component.anchoredPosition = new Vector2(-this.MESSAGE_OFFSET, this.MESSAGE_OFFSET);
				break;
			case TutorialMessage.eMessagePosition.LeftTop:
				component.anchorMin = new Vector2(0f, 1f);
				component.anchorMax = new Vector2(0f, 1f);
				component.pivot = new Vector2(0f, 1f);
				component.anchoredPosition = new Vector2(this.MESSAGE_OFFSET, -this.MESSAGE_OFFSET);
				break;
			case TutorialMessage.eMessagePosition.LeftBottom:
				component.anchorMin = new Vector2(0f, 0f);
				component.anchorMax = new Vector2(0f, 0f);
				component.pivot = new Vector2(0f, 0f);
				component.anchoredPosition = new Vector2(this.MESSAGE_OFFSET, this.MESSAGE_OFFSET);
				break;
			}
		}

		// Token: 0x06003B89 RID: 15241 RVA: 0x001301BC File Offset: 0x0012E5BC
		public void SetEnableArrow(bool flg)
		{
			if (!this.m_Body.activeSelf)
			{
				this.m_Body.SetActive(true);
			}
			this.m_ArrowObj.SetActive(flg);
		}

		// Token: 0x06003B8A RID: 15242 RVA: 0x001301E6 File Offset: 0x0012E5E6
		public bool GetEnableArrow()
		{
			return this.m_ArrowObj.activeSelf;
		}

		// Token: 0x06003B8B RID: 15243 RVA: 0x001301F3 File Offset: 0x0012E5F3
		public void SetArrowPosition(Vector3 position)
		{
			this.m_ArrowObj.GetComponent<RectTransform>().position = position;
		}

		// Token: 0x06003B8C RID: 15244 RVA: 0x00130208 File Offset: 0x0012E608
		public void SetArrowWorldPosition(Vector3 position)
		{
			Vector3 v = RectTransformUtility.WorldToScreenPoint(Camera.main, position);
			RectTransform component = this.m_ArrowObj.GetComponent<RectTransform>();
			Vector2 v2;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(component.parent.GetComponent<RectTransform>(), v, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out v2);
			component.localPosition = v2;
		}

		// Token: 0x06003B8D RID: 15245 RVA: 0x00130264 File Offset: 0x0012E664
		public void SetArrowOffset(GameObject obj, Vector3 offset)
		{
			RectTransform component = obj.GetComponent<RectTransform>();
			RectTransform component2 = this.m_ArrowObj.GetComponent<RectTransform>();
			component2.position = component.position;
			component2.localPosition += offset;
		}

		// Token: 0x06003B8E RID: 15246 RVA: 0x001302A4 File Offset: 0x0012E6A4
		public void SetArrowDirection(TutorialMessage.eArrowDirection direction)
		{
			float z = 0f;
			switch (direction)
			{
			case TutorialMessage.eArrowDirection.RightTop:
				z = 45f;
				break;
			case TutorialMessage.eArrowDirection.RightBottom:
				z = -45f;
				break;
			case TutorialMessage.eArrowDirection.LeftTop:
				z = 135f;
				break;
			case TutorialMessage.eArrowDirection.LeftBottom:
				z = -135f;
				break;
			}
			this.m_ArrowObj.GetComponent<RectTransform>().localEulerAngles = new Vector3(0f, 0f, z);
		}

		// Token: 0x06003B8F RID: 15247 RVA: 0x0013031E File Offset: 0x0012E71E
		public void SetEnableTrace(bool flg)
		{
			if (!this.m_Body.activeSelf)
			{
				this.m_Body.SetActive(true);
			}
			this.m_TraceObj.SetActive(flg);
		}

		// Token: 0x06003B90 RID: 15248 RVA: 0x00130348 File Offset: 0x0012E748
		public void SetTracePosition(Vector2 startPos, Vector2 endPos)
		{
			RectTransform component = this.m_TraceObj.GetComponent<RectTransform>();
			component.position = endPos;
			Vector2 anchoredPosition = component.anchoredPosition;
			component.position = startPos;
			Vector2 anchoredPosition2 = component.anchoredPosition;
			component.sizeDelta = new Vector2(anchoredPosition.x - anchoredPosition2.x, component.sizeDelta.y);
			component.localPosZ(0f);
		}

		// Token: 0x06003B91 RID: 15249 RVA: 0x001303BC File Offset: 0x0012E7BC
		public void SetTracePositionOffset(GameObject obj, Vector2 offset, Vector2 move)
		{
			RectTransform component = obj.GetComponent<RectTransform>();
			RectTransform component2 = this.m_TraceObj.GetComponent<RectTransform>();
			component2.position = component.position;
			component2.anchoredPosition += offset;
			Vector2 a = component2.anchoredPosition + offset;
			component2.sizeDelta = new Vector2((a + move).x - a.x, component2.sizeDelta.y);
			component2.localPosZ(0f);
		}

		// Token: 0x0400431E RID: 17182
		private float MESSAGE_OFFSET = 64f;

		// Token: 0x0400431F RID: 17183
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x04004320 RID: 17184
		[SerializeField]
		private GameObject m_DarkObj;

		// Token: 0x04004321 RID: 17185
		[SerializeField]
		private GameObject m_MessageObj;

		// Token: 0x04004322 RID: 17186
		[SerializeField]
		private GameObject m_ArrowObj;

		// Token: 0x04004323 RID: 17187
		[SerializeField]
		private Text m_MessageText;

		// Token: 0x04004324 RID: 17188
		[SerializeField]
		private GameObject m_TraceObj;

		// Token: 0x04004325 RID: 17189
		private float m_DardDefaultAlpha = -1f;

		// Token: 0x02000B11 RID: 2833
		public enum eArrowDirection
		{
			// Token: 0x04004327 RID: 17191
			RightTop,
			// Token: 0x04004328 RID: 17192
			RightBottom,
			// Token: 0x04004329 RID: 17193
			LeftTop,
			// Token: 0x0400432A RID: 17194
			LeftBottom
		}

		// Token: 0x02000B12 RID: 2834
		public enum eMessagePosition
		{
			// Token: 0x0400432C RID: 17196
			RightTop,
			// Token: 0x0400432D RID: 17197
			RightBottom,
			// Token: 0x0400432E RID: 17198
			LeftTop,
			// Token: 0x0400432F RID: 17199
			LeftBottom
		}
	}
}
