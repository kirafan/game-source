﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000B37 RID: 2871
	public class WebViewGroup : UIGroup
	{
		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06003C64 RID: 15460 RVA: 0x001350C6 File Offset: 0x001334C6
		// (set) Token: 0x06003C63 RID: 15459 RVA: 0x001350BD File Offset: 0x001334BD
		public RectTransform targetRect
		{
			get
			{
				return this.m_TargetRect;
			}
			set
			{
				this.m_TargetRect = value;
			}
		}

		// Token: 0x06003C65 RID: 15461 RVA: 0x001350CE File Offset: 0x001334CE
		public void Open(string url, Action<string> callback = null)
		{
			this.m_Url = Utility.ConvertKrrURL(url);
			this.m_Callback = callback;
			base.Open();
		}

		// Token: 0x06003C66 RID: 15462 RVA: 0x001350E9 File Offset: 0x001334E9
		public override void Hide()
		{
			base.Hide();
			this.m_WVCtrl.DestroyWebViewObject();
		}

		// Token: 0x06003C67 RID: 15463 RVA: 0x001350FD File Offset: 0x001334FD
		public override void Close()
		{
			base.Close();
			this.m_WVCtrl.DestroyWebViewObject();
		}

		// Token: 0x06003C68 RID: 15464 RVA: 0x00135114 File Offset: 0x00133514
		public override void Update()
		{
			base.Update();
			if (base.IsIdle() && !this.m_WVCtrl.IsActiveWebViewObj() && !SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.IsActive && this.m_Url != null)
			{
				this.m_WVCtrl.LoadURL(this.m_Url, this.m_TargetRect, new Action<string>(this.WVCallBack));
			}
		}

		// Token: 0x06003C69 RID: 15465 RVA: 0x00135184 File Offset: 0x00133584
		private void OnDestroy()
		{
			this.m_WVCtrl.DestroyWebViewObject();
		}

		// Token: 0x06003C6A RID: 15466 RVA: 0x00135194 File Offset: 0x00133594
		private void WVCallBack(string msg)
		{
			int num = msg.IndexOf("<");
			int num2 = msg.IndexOf(">");
			if (num != -1 && num2 != -1)
			{
				string text = msg.Substring(num + 1, num2 - num - 1);
				string url = msg.Substring(num2 + 1);
				if (text != null)
				{
					if (text == "browser")
					{
						Application.OpenURL(url);
						return;
					}
				}
				this.m_Callback.Call(msg);
			}
			else
			{
				this.m_Callback.Call(msg);
			}
		}

		// Token: 0x04004439 RID: 17465
		[SerializeField]
		private WebViewController m_WVCtrl;

		// Token: 0x0400443A RID: 17466
		[SerializeField]
		private RectTransform m_TargetRect;

		// Token: 0x0400443B RID: 17467
		private Action<string> m_Callback;

		// Token: 0x0400443C RID: 17468
		private string m_Url;
	}
}
