﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200081B RID: 2075
	public class FixScrollRect : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06002B1B RID: 11035 RVA: 0x000E3B63 File Offset: 0x000E1F63
		public ScrollRect ScrollRect
		{
			get
			{
				if (this.m_ScrollRect == null)
				{
					this.m_ScrollRect = base.GetComponent<ScrollRect>();
				}
				return this.m_ScrollRect;
			}
		}

		// Token: 0x06002B1C RID: 11036 RVA: 0x000E3B88 File Offset: 0x000E1F88
		private void Start()
		{
			this.m_VerticalScrollBarObj = this.m_VerticalScrollBar.gameObject;
			this.ScrollRect.onValueChanged.AddListener(new UnityAction<Vector2>(this.OnScrollRectValueChanged));
			this.m_VerticalScrollBar.onValueChanged.AddListener(new UnityAction<float>(this.OnVerticalScrollBarValueChanged));
			this.SetVerticalNormalizedPosition(1f);
			this.m_DefaultSizeDelta = this.ScrollRect.viewport.GetComponent<RectTransform>().sizeDelta;
		}

		// Token: 0x06002B1D RID: 11037 RVA: 0x000E3C04 File Offset: 0x000E2004
		public void SetVerticalNormalizedPosition(float value)
		{
			this.ScrollRect.verticalNormalizedPosition = value;
			this.m_ScrollMaxVertical = this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height;
			if (this.m_ScrollMaxVertical > 0f)
			{
				this.m_VerticalScrollBar.value = 1f - this.ScrollRect.content.localPosition.y / this.m_ScrollMaxVertical;
			}
		}

		// Token: 0x06002B1E RID: 11038 RVA: 0x000E3C94 File Offset: 0x000E2094
		private void LateUpdate()
		{
			this.m_ScrollMaxVertical = this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height;
			if (this.m_ScrollMaxVertical > 0f)
			{
				if (!this.m_VerticalScrollBarObj.activeSelf)
				{
					this.m_VerticalScrollBarObj.SetActive(true);
					if (this.m_ScrollMaxVertical > 0f)
					{
						this.m_VerticalScrollBar.value = 1f - this.ScrollRect.content.localPosition.y / this.m_ScrollMaxVertical;
					}
					RectTransform component = this.ScrollRect.viewport.GetComponent<RectTransform>();
					component.sizeDelta = new Vector2(this.m_DefaultSizeDelta.x, this.m_DefaultSizeDelta.y);
				}
			}
			else if (this.m_VerticalScrollBarObj.activeSelf)
			{
				this.m_VerticalScrollBarObj.SetActive(false);
				RectTransform component2 = this.ScrollRect.viewport.GetComponent<RectTransform>();
				component2.sizeDelta = new Vector2(0f, this.m_DefaultSizeDelta.y);
			}
			if (this.m_VerticalScrollBarObj.activeInHierarchy)
			{
				float num = this.ScrollRect.viewport.rect.height;
				if (this.ScrollRect.content.anchoredPosition.y < 0f)
				{
					num += this.ScrollRect.content.anchoredPosition.y;
				}
				else if (this.ScrollRect.content.anchoredPosition.y > this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height)
				{
					num -= this.ScrollRect.content.anchoredPosition.y - (this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height);
				}
				this.m_VerticalScrollBar.size = num / this.ScrollRect.content.rect.height;
				if (this.m_VerticalScrollBar.size < 0.2f)
				{
					this.m_VerticalScrollBar.size = 0.2f;
				}
			}
		}

		// Token: 0x06002B1F RID: 11039 RVA: 0x000E3F28 File Offset: 0x000E2328
		public void OnVerticalScrollBarValueChanged(float value)
		{
			this.m_ScrollMaxVertical = this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height;
			this.ScrollRect.content.localPosY((1f - this.m_VerticalScrollBar.value) * this.m_ScrollMaxVertical);
		}

		// Token: 0x06002B20 RID: 11040 RVA: 0x000E3F94 File Offset: 0x000E2394
		public void OnScrollRectValueChanged(Vector2 pos)
		{
			this.m_ScrollMaxVertical = this.ScrollRect.content.rect.height - this.ScrollRect.viewport.rect.height;
			if (this.m_ScrollMaxVertical > 0f)
			{
				this.m_VerticalScrollBar.value = 1f - this.ScrollRect.content.localPosition.y / this.m_ScrollMaxVertical;
			}
		}

		// Token: 0x06002B21 RID: 11041 RVA: 0x000E4018 File Offset: 0x000E2418
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06002B22 RID: 11042 RVA: 0x000E401C File Offset: 0x000E241C
		public void OnDrag(PointerEventData eventData)
		{
			if (this.m_ScrollRect.vertical)
			{
				RectTransform component = base.GetComponent<RectTransform>();
				Vector2 a;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.position, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out a);
				Vector2 b;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.pressPosition, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out b);
				if ((a - b).sqrMagnitude > 1024f)
				{
					CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].ForceEnableHold(false);
					}
				}
			}
		}

		// Token: 0x06002B23 RID: 11043 RVA: 0x000E40B8 File Offset: 0x000E24B8
		public void OnEndDrag(PointerEventData eventData)
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].ForceRelease();
			}
		}

		// Token: 0x04003182 RID: 12674
		private const float VERTICAL_SCROLLBAR_MINSIZE = 0.2f;

		// Token: 0x04003183 RID: 12675
		private const float SCROLLBAR_WIDTH = 24f;

		// Token: 0x04003184 RID: 12676
		[SerializeField]
		private Scrollbar m_VerticalScrollBar;

		// Token: 0x04003185 RID: 12677
		private GameObject m_VerticalScrollBarObj;

		// Token: 0x04003186 RID: 12678
		private float m_ScrollMaxVertical;

		// Token: 0x04003187 RID: 12679
		private ScrollRect m_ScrollRect;

		// Token: 0x04003188 RID: 12680
		private Vector2 m_DefaultSizeDelta;
	}
}
