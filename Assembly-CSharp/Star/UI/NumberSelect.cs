﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200081D RID: 2077
	public class NumberSelect : MonoBehaviour
	{
		// Token: 0x14000062 RID: 98
		// (add) Token: 0x06002B2A RID: 11050 RVA: 0x000E423C File Offset: 0x000E263C
		// (remove) Token: 0x06002B2B RID: 11051 RVA: 0x000E4274 File Offset: 0x000E2674
		public event Action OnChangeCurrentNum;

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06002B2C RID: 11052 RVA: 0x000E42AA File Offset: 0x000E26AA
		// (set) Token: 0x06002B2D RID: 11053 RVA: 0x000E42B2 File Offset: 0x000E26B2
		public bool AddInteractable
		{
			get
			{
				return this.m_AddInteractable;
			}
			set
			{
				this.m_AddInteractable = value;
				this.SetRange(this.m_Min, this.m_Max);
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06002B2E RID: 11054 RVA: 0x000E42CD File Offset: 0x000E26CD
		// (set) Token: 0x06002B2F RID: 11055 RVA: 0x000E42D5 File Offset: 0x000E26D5
		public bool SubInteractable
		{
			get
			{
				return this.m_SubInteractable;
			}
			set
			{
				this.m_SubInteractable = value;
				this.SetRange(this.m_Min, this.m_Max);
			}
		}

		// Token: 0x06002B30 RID: 11056 RVA: 0x000E42F0 File Offset: 0x000E26F0
		public int GetCurrentNum()
		{
			return this.m_CurrentNum;
		}

		// Token: 0x06002B31 RID: 11057 RVA: 0x000E42F8 File Offset: 0x000E26F8
		public void SetCurrentNum(int value)
		{
			int currentNum = this.m_CurrentNum;
			this.m_CurrentNum = value;
			if (this.m_Min >= 0 && this.m_Min >= this.m_CurrentNum)
			{
				this.m_CurrentNum = this.m_Min;
			}
			if (this.m_Max >= 0 && this.m_Max <= this.m_CurrentNum)
			{
				this.m_CurrentNum = this.m_Max;
			}
			if (this.m_Min >= this.m_CurrentNum)
			{
				this.m_SubButton.Interactable = false;
				this.EndHoldSub();
			}
			else
			{
				this.m_SubButton.Interactable = this.m_SubInteractable;
			}
			if (this.m_Max <= this.m_CurrentNum)
			{
				this.m_AddButton.Interactable = false;
				this.EndHoldAdd();
			}
			else
			{
				this.m_AddButton.Interactable = this.m_AddInteractable;
			}
			this.m_ImageNumbers.SetValue(this.m_CurrentNum);
			if (this.m_CurrentNum != currentNum)
			{
				this.OnChangeCurrentNum.Call();
			}
		}

		// Token: 0x06002B32 RID: 11058 RVA: 0x000E43FF File Offset: 0x000E27FF
		public void SetRange(int min, int max)
		{
			this.m_Min = min;
			this.m_Max = max;
			this.SetCurrentNum(this.m_CurrentNum);
		}

		// Token: 0x06002B33 RID: 11059 RVA: 0x000E441B File Offset: 0x000E281B
		public void OnClickSubButton()
		{
			this.SetCurrentNum(this.m_CurrentNum - 1);
		}

		// Token: 0x06002B34 RID: 11060 RVA: 0x000E442B File Offset: 0x000E282B
		public void OnClickAddButton()
		{
			this.SetCurrentNum(this.m_CurrentNum + 1);
		}

		// Token: 0x06002B35 RID: 11061 RVA: 0x000E443B File Offset: 0x000E283B
		public void StartHoldSub()
		{
			this.m_HoldSub = true;
			this.m_SubWait = 0.2f;
		}

		// Token: 0x06002B36 RID: 11062 RVA: 0x000E444F File Offset: 0x000E284F
		public void StartHoldAdd()
		{
			this.m_HoldAdd = true;
			this.m_AddWait = 0.2f;
		}

		// Token: 0x06002B37 RID: 11063 RVA: 0x000E4463 File Offset: 0x000E2863
		public void EndHoldSub()
		{
			this.m_HoldSub = false;
		}

		// Token: 0x06002B38 RID: 11064 RVA: 0x000E446C File Offset: 0x000E286C
		public void EndHoldAdd()
		{
			this.m_HoldAdd = false;
		}

		// Token: 0x06002B39 RID: 11065 RVA: 0x000E4478 File Offset: 0x000E2878
		private void Update()
		{
			if (this.m_AddInteractable && this.m_HoldAdd)
			{
				this.m_AddWait -= Time.deltaTime;
				if (this.m_AddWait <= 0f)
				{
					this.SetCurrentNum(this.m_CurrentNum + 1);
					this.m_AddWait = 0.05f;
				}
			}
			if (this.m_SubInteractable && this.m_HoldSub)
			{
				this.m_SubWait -= Time.deltaTime;
				if (this.m_SubWait <= 0f)
				{
					this.SetCurrentNum(this.m_CurrentNum - 1);
					this.m_SubWait = 0.05f;
				}
			}
		}

		// Token: 0x06002B3A RID: 11066 RVA: 0x000E4527 File Offset: 0x000E2927
		public void SetChangeCurrentNumCallBack(Action callBack)
		{
			this.OnChangeCurrentNum = callBack;
		}

		// Token: 0x0400318F RID: 12687
		private const float HOLD_START_WAIT = 0.2f;

		// Token: 0x04003190 RID: 12688
		private const float HOLD_REPEAT_WAIT = 0.05f;

		// Token: 0x04003191 RID: 12689
		[SerializeField]
		private CustomButton m_AddButton;

		// Token: 0x04003192 RID: 12690
		[SerializeField]
		private CustomButton m_SubButton;

		// Token: 0x04003193 RID: 12691
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x04003194 RID: 12692
		private int m_CurrentNum;

		// Token: 0x04003195 RID: 12693
		private int m_Min = -1;

		// Token: 0x04003196 RID: 12694
		private int m_Max = -1;

		// Token: 0x04003197 RID: 12695
		private bool m_HoldSub;

		// Token: 0x04003198 RID: 12696
		private bool m_HoldAdd;

		// Token: 0x04003199 RID: 12697
		private float m_SubWait;

		// Token: 0x0400319A RID: 12698
		private float m_AddWait;

		// Token: 0x0400319B RID: 12699
		private bool m_AddInteractable = true;

		// Token: 0x0400319C RID: 12700
		private bool m_SubInteractable = true;
	}
}
