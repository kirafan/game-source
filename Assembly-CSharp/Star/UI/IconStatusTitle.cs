﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000861 RID: 2145
	public class IconStatusTitle : MonoBehaviour
	{
		// Token: 0x06002C96 RID: 11414 RVA: 0x000EB7FC File Offset: 0x000E9BFC
		public void Apply(IconStatusTitle.eType type)
		{
			switch (type)
			{
			case IconStatusTitle.eType.Lv:
				this.m_Image.sprite = this.m_SpriteLv;
				break;
			case IconStatusTitle.eType.Hp:
				this.m_Image.sprite = this.m_SpriteHp;
				break;
			case IconStatusTitle.eType.Atk:
				this.m_Image.sprite = this.m_SpriteAtk;
				break;
			case IconStatusTitle.eType.Mgc:
				this.m_Image.sprite = this.m_SpriteMgc;
				break;
			case IconStatusTitle.eType.Def:
				this.m_Image.sprite = this.m_SpriteDef;
				break;
			case IconStatusTitle.eType.MDef:
				this.m_Image.sprite = this.m_SpriteMDef;
				break;
			case IconStatusTitle.eType.Spd:
				this.m_Image.sprite = this.m_SpriteSpd;
				break;
			case IconStatusTitle.eType.Cost:
				this.m_Image.sprite = this.m_SpriteCost;
				break;
			}
		}

		// Token: 0x04003388 RID: 13192
		[SerializeField]
		private Sprite m_SpriteLv;

		// Token: 0x04003389 RID: 13193
		[SerializeField]
		private Sprite m_SpriteHp;

		// Token: 0x0400338A RID: 13194
		[SerializeField]
		private Sprite m_SpriteAtk;

		// Token: 0x0400338B RID: 13195
		[SerializeField]
		private Sprite m_SpriteMgc;

		// Token: 0x0400338C RID: 13196
		[SerializeField]
		private Sprite m_SpriteDef;

		// Token: 0x0400338D RID: 13197
		[SerializeField]
		private Sprite m_SpriteMDef;

		// Token: 0x0400338E RID: 13198
		[SerializeField]
		private Sprite m_SpriteSpd;

		// Token: 0x0400338F RID: 13199
		[SerializeField]
		private Sprite m_SpriteCost;

		// Token: 0x04003390 RID: 13200
		[SerializeField]
		private Image m_Image;

		// Token: 0x02000862 RID: 2146
		public enum eType
		{
			// Token: 0x04003392 RID: 13202
			Lv,
			// Token: 0x04003393 RID: 13203
			Hp,
			// Token: 0x04003394 RID: 13204
			Atk,
			// Token: 0x04003395 RID: 13205
			Mgc,
			// Token: 0x04003396 RID: 13206
			Def,
			// Token: 0x04003397 RID: 13207
			MDef,
			// Token: 0x04003398 RID: 13208
			Spd,
			// Token: 0x04003399 RID: 13209
			Cost
		}
	}
}
