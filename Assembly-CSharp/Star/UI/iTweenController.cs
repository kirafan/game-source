﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008C8 RID: 2248
	public class iTweenController : MonoBehaviour
	{
		// Token: 0x06002EA8 RID: 11944 RVA: 0x000F4B33 File Offset: 0x000F2F33
		public void Play(iTweenWrapperPlayArgument argument)
		{
		}

		// Token: 0x020008C9 RID: 2249
		public enum eiTweenActionType
		{
			// Token: 0x040035C8 RID: 13768
			None,
			// Token: 0x040035C9 RID: 13769
			MoveTo,
			// Token: 0x040035CA RID: 13770
			RotateTo,
			// Token: 0x040035CB RID: 13771
			ScaleTo,
			// Token: 0x040035CC RID: 13772
			ColorTo,
			// Token: 0x040035CD RID: 13773
			ValueTo
		}

		// Token: 0x020008CA RID: 2250
		// (Invoke) Token: 0x06002EAA RID: 11946
		public delegate void OnCompleteDelegate(object obj);
	}
}
