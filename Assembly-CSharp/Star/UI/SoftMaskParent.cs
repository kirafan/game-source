﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000AC6 RID: 2758
	[ExecuteInEditMode]
	[RequireComponent(typeof(Image))]
	public class SoftMaskParent : MonoBehaviour
	{
		// Token: 0x060039A2 RID: 14754 RVA: 0x00125D42 File Offset: 0x00124142
		private void Start()
		{
			this.ApplyAllChildren();
		}

		// Token: 0x060039A3 RID: 14755 RVA: 0x00125D4C File Offset: 0x0012414C
		public void ApplyAllChildren()
		{
			if (this.m_Image == null)
			{
				this.m_Image = base.GetComponent<Image>();
			}
			if (this.m_Image == null)
			{
				return;
			}
			Image[] componentsInChildren = base.GetComponentsInChildren<Image>(true);
			for (int i = 1; i < componentsInChildren.Length; i++)
			{
				this.AddChild(componentsInChildren[i]);
			}
			Text[] componentsInChildren2 = base.GetComponentsInChildren<Text>();
			for (int j = 0; j < componentsInChildren2.Length; j++)
			{
				this.AddChild(componentsInChildren2[j]);
			}
		}

		// Token: 0x060039A4 RID: 14756 RVA: 0x00125DD4 File Offset: 0x001241D4
		public void AddChild(Image child)
		{
			SoftMask softMask = child.GetComponent<SoftMask>();
			if (softMask == null)
			{
				softMask = child.gameObject.AddComponent<SoftMask>();
			}
			softMask.m_maskImage = this.m_Image;
			softMask.ApplyMaterial(this.m_SoftMaskMaterial);
		}

		// Token: 0x060039A5 RID: 14757 RVA: 0x00125E18 File Offset: 0x00124218
		public void AddChild(Text child)
		{
			SoftMask softMask = child.GetComponent<SoftMask>();
			if (softMask == null)
			{
				softMask = child.gameObject.AddComponent<SoftMask>();
			}
			softMask.m_maskImage = this.m_Image;
			softMask.ApplyMaterial(this.m_SoftMaskMaterialText);
		}

		// Token: 0x060039A6 RID: 14758 RVA: 0x00125E5C File Offset: 0x0012425C
		public void RemoveChild(SoftMask removeChild)
		{
			Image component = removeChild.GetComponent<Image>();
			removeChild.RemoveComponent<SoftMask>();
			component.material = null;
		}

		// Token: 0x040040DD RID: 16605
		[SerializeField]
		private Material m_SoftMaskMaterial;

		// Token: 0x040040DE RID: 16606
		[SerializeField]
		private Material m_SoftMaskMaterialText;

		// Token: 0x040040DF RID: 16607
		private Image m_Image;
	}
}
