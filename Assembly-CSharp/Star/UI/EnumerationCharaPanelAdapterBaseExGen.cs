﻿using System;

namespace Star.UI
{
	// Token: 0x02000953 RID: 2387
	public abstract class EnumerationCharaPanelAdapterBaseExGen<CoreType> : EnumerationCharaPanelAdapterBase where CoreType : EnumerationCharaPanelBase, new()
	{
		// Token: 0x0600318C RID: 12684 RVA: 0x000FBE3B File Offset: 0x000FA23B
		public virtual CoreType CreateCore()
		{
			return Activator.CreateInstance<CoreType>();
		}

		// Token: 0x0600318D RID: 12685 RVA: 0x000FBE42 File Offset: 0x000FA242
		public override void Destroy()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Destroy();
			}
		}

		// Token: 0x0600318E RID: 12686 RVA: 0x000FBE65 File Offset: 0x000FA265
		public override void SetMode(EnumerationCharaPanelBase.eMode mode)
		{
			if (this.m_Core != null)
			{
				this.m_Core.SetMode(mode);
			}
		}

		// Token: 0x040037FA RID: 14330
		protected CoreType m_Core;
	}
}
