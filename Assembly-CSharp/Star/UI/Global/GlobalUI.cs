﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using UnityEngine;
using UnityEngine.UI;
using WWWTypes;

namespace Star.UI.Global
{
	// Token: 0x020009AD RID: 2477
	public class GlobalUI : MonoBehaviour
	{
		// Token: 0x17000329 RID: 809
		// (get) Token: 0x0600334A RID: 13130 RVA: 0x00104CAF File Offset: 0x001030AF
		public bool IsSelectedShortCut
		{
			get
			{
				return this.m_SelectedShortCutButton != GlobalUI.eShortCutButton.None;
			}
		}

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x0600334B RID: 13131 RVA: 0x00104CBD File Offset: 0x001030BD
		public GlobalUI.eShortCutButton SelectedShortCutButton
		{
			get
			{
				return this.m_SelectedShortCutButton;
			}
		}

		// Token: 0x14000090 RID: 144
		// (add) Token: 0x0600334C RID: 13132 RVA: 0x00104CC8 File Offset: 0x001030C8
		// (remove) Token: 0x0600334D RID: 13133 RVA: 0x00104D00 File Offset: 0x00103100
		private event Action<bool> OnClickBackButton;

		// Token: 0x0600334E RID: 13134 RVA: 0x00104D38 File Offset: 0x00103138
		public void Setup()
		{
			this.m_StateController.Setup(5, 0, false);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 2, true);
			this.m_StateController.AddTransit(1, 3, true);
			this.m_StateController.AddTransit(2, 3, true);
			this.m_StateController.AddTransit(2, 4, true);
			this.m_StateController.AddTransit(3, 0, true);
			this.m_StateController.AddTransit(3, 1, true);
			this.m_StateController.AddTransit(4, 2, true);
			this.m_StateController.AddTransit(4, 3, true);
			this.m_StateController.AddOnEnter(2, new Action(this.OnEnterWait));
			this.m_StateController.AddOnExit(2, new Action(this.OnExitWait));
			this.m_CancelButton.gameObject.SetActive(false);
			this.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.None);
			this.SetBackButtonCallBack(null);
			for (int i = 0; i < 12; i++)
			{
				ShortCutButton shortCutButton = UnityEngine.Object.Instantiate<ShortCutButton>(this.m_ShortCutButtonPrefab, this.m_ShortCutButtonParent);
				shortCutButton.SetShortCutTransit((GlobalUI.eShortCutButton)i);
				shortCutButton.OnClick += this.OnShortCutButtonCallBack;
				this.m_ShortCutButtonInstList.Add(shortCutButton);
			}
			base.gameObject.SetActive(false);
		}

		// Token: 0x0600334F RID: 13135 RVA: 0x00104E78 File Offset: 0x00103278
		private void Update()
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			if (inst != null && inst.IsAvailable())
			{
				UserData userData = inst.UserDataMng.UserData;
				this.SetUserStamina(userData.Stamina.GetValue(), userData.Stamina.GetValueMax());
				this.SetUserRemainSec(userData.Stamina.GetRemainSec());
				if (this.m_CurrentUserLevel != userData.Lv)
				{
					this.m_CurrentUserLevel = userData.Lv;
					this.SetUserLevel(userData.Lv);
					this.m_MaxExp = inst.DbMng.MasterRankDB.GetNextExp(userData.Lv);
				}
				if (this.m_CurrentExp != userData.LvExp)
				{
					this.m_CurrentExp = userData.LvExp;
					this.SetUserExp(userData.LvExp, (long)this.m_MaxExp);
				}
				this.SetUserGold(userData.Gold);
				this.SetUserGem(userData.Gem);
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RefreshBadgeTimeSec <= 0f)
				{
					this.Request_GetBadge();
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RefreshBadgeTimeSec = 1800f;
				}
				this.SetMissionBadge(MissionManager.GetNewListUpNum());
				this.SetPresentBadge(inst.UserDataMng.NoticePresentCount);
				this.SetFriendBadge(inst.UserDataMng.NoticeFriendCount);
				this.ApplyTotalBadge();
			}
			this.UpdateAndroidBackKey();
			GlobalUI.eUIState nowState = (GlobalUI.eUIState)this.m_StateController.NowState;
			if (nowState != GlobalUI.eUIState.In)
			{
				if (nowState != GlobalUI.eUIState.Out)
				{
					if (nowState == GlobalUI.eUIState.ShortCutWait)
					{
						this.m_StateController.ChangeState(2);
					}
				}
				else
				{
					bool flag = true;
					if (this.m_StateController.IsPlayingInOut())
					{
						flag = false;
					}
					else
					{
						for (int i = 0; i < this.m_Anims.Length; i++)
						{
							if (this.m_Anims[i].State == 3)
							{
								flag = false;
								break;
							}
						}
					}
					if (flag)
					{
						this.m_CancelButton.gameObject.SetActive(false);
						this.m_StateController.ChangeState(0);
						base.gameObject.SetActive(false);
					}
				}
			}
			else
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_Anims.Length; j++)
				{
					if (this.m_Anims[j].State == 1)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.m_CancelButton.gameObject.SetActive(false);
					this.m_StateController.ChangeState(2);
				}
			}
		}

		// Token: 0x06003350 RID: 13136 RVA: 0x001050FC File Offset: 0x001034FC
		public bool Request_GetBadge()
		{
			PlayerRequestTypes.Getbadgecount param = new PlayerRequestTypes.Getbadgecount();
			MeigewwwParam wwwParam = PlayerRequest.Getbadgecount(param, new MeigewwwParam.Callback(this.OnResponse_GetBadge));
			NetworkQueueManager.Request(wwwParam);
			return true;
		}

		// Token: 0x06003351 RID: 13137 RVA: 0x0010512C File Offset: 0x0010352C
		private void OnResponse_GetBadge(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Getbadgecount getbadgecount = PlayerResponse.Getbadgecount(wwwParam, ResponseCommon.DialogType.None, null);
			if (getbadgecount == null)
			{
				return;
			}
			ResultCode result = getbadgecount.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticeMissionCount = getbadgecount.missionClearCount;
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticeFriendCount = getbadgecount.friendProposedCount;
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticePresentCount = getbadgecount.presentCount;
			}
		}

		// Token: 0x06003352 RID: 13138 RVA: 0x001051AF File Offset: 0x001035AF
		private void OnEnterWait()
		{
			this.m_BaseWindow.SetEnableInput(true);
			this.m_SceneInfoWindow.SetEnableInput(true);
			this.UpdateInput();
		}

		// Token: 0x06003353 RID: 13139 RVA: 0x001051CF File Offset: 0x001035CF
		private void OnExitWait()
		{
			this.m_BaseWindow.SetEnableInput(false);
			this.m_SceneInfoWindow.SetEnableInput(false);
			this.UpdateInput();
		}

		// Token: 0x06003354 RID: 13140 RVA: 0x001051F0 File Offset: 0x001035F0
		public void Open()
		{
			this.SetBackButtonCallBack(null);
			base.gameObject.SetActive(true);
			if (this.m_StateController.ChangeState(1))
			{
				for (int i = 0; i < this.m_Anims.Length; i++)
				{
					this.m_Anims[i].PlayIn();
				}
				if (!this.m_BackButtonOnly)
				{
					this.m_BaseWindow.Open();
				}
				else
				{
					this.m_BaseWindow.Hide();
				}
				this.m_SceneInfoWindow.Open();
				this.UpdateInput();
			}
		}

		// Token: 0x06003355 RID: 13141 RVA: 0x00105280 File Offset: 0x00103680
		public void Close()
		{
			this.SetBackButtonCallBack(null);
			if (this.m_StateController.ChangeState(3))
			{
				for (int i = 0; i < this.m_Anims.Length; i++)
				{
					this.m_Anims[i].PlayOut();
				}
				this.CloseShortCutWindow();
				this.m_BaseWindow.Close();
				this.m_SceneInfoWindow.Close();
				this.UpdateInput();
			}
		}

		// Token: 0x06003356 RID: 13142 RVA: 0x001052ED File Offset: 0x001036ED
		public bool IsOpen()
		{
			return this.m_StateController.NowState != 0 && this.m_StateController.NowState != 3;
		}

		// Token: 0x06003357 RID: 13143 RVA: 0x00105313 File Offset: 0x00103713
		public void SetEnableInput(bool flg)
		{
			this.m_EnableInput = flg;
			this.UpdateInput();
		}

		// Token: 0x06003358 RID: 13144 RVA: 0x00105324 File Offset: 0x00103724
		private void UpdateInput()
		{
			bool flag = this.m_EnableInput && this.m_StateController.NowState == 2 && this.m_EnableInputUIGroupList.Count > 0;
			if (this.m_CustomButtonGroup.IsEnableInput != flag)
			{
				this.m_CustomButtonGroup.IsEnableInput = flag;
			}
		}

		// Token: 0x06003359 RID: 13145 RVA: 0x0010537C File Offset: 0x0010377C
		public void SetInteractable(bool flg)
		{
			this.SetEnableInput(flg);
		}

		// Token: 0x0600335A RID: 13146 RVA: 0x00105385 File Offset: 0x00103785
		public bool GetInteractable()
		{
			return this.m_CustomButtonGroup.Interactable;
		}

		// Token: 0x0600335B RID: 13147 RVA: 0x00105392 File Offset: 0x00103792
		public void SetBackButtonInteractable(bool flg)
		{
			this.m_SceneInfoWindow.SetInteractable(flg);
		}

		// Token: 0x0600335C RID: 13148 RVA: 0x001053A0 File Offset: 0x001037A0
		public void SetBackButtonOnly(bool only)
		{
			this.m_BackButtonOnly = only;
			if (this.m_BackButtonOnly)
			{
				this.m_BaseWindow.GameObject.SetActive(false);
			}
			else
			{
				this.m_BaseWindow.GameObject.SetActive(true);
			}
		}

		// Token: 0x0600335D RID: 13149 RVA: 0x001053DB File Offset: 0x001037DB
		public void SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType iconType)
		{
		}

		// Token: 0x0600335E RID: 13150 RVA: 0x001053DD File Offset: 0x001037DD
		public void SetBackButtonCallBack(Action<bool> callback)
		{
			this.OnClickBackButton = null;
			this.OnClickBackButton += callback;
		}

		// Token: 0x0600335F RID: 13151 RVA: 0x001053ED File Offset: 0x001037ED
		public Action<bool> GetBackButtonCallBack()
		{
			return this.OnClickBackButton;
		}

		// Token: 0x06003360 RID: 13152 RVA: 0x001053F5 File Offset: 0x001037F5
		public void SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo)
		{
			this.ClearSceneInfoStack();
			this.AddSceneInfoStack(backButtonType, sceneInfo);
		}

		// Token: 0x06003361 RID: 13153 RVA: 0x00105406 File Offset: 0x00103806
		public void ClearSceneInfoStack()
		{
			this.m_InfoStack.Clear();
		}

		// Token: 0x06003362 RID: 13154 RVA: 0x00105414 File Offset: 0x00103814
		public GlobalUI.SceneInfoStack AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo)
		{
			GlobalUI.SceneInfoStack sceneInfoStack = new GlobalUI.SceneInfoStack(backButtonType, sceneInfo);
			this.m_InfoStack.Add(sceneInfoStack);
			if (this.m_InfoStack.Count > 0)
			{
				this.m_SceneInfoWindow.SetBackButtonIconType(this.m_InfoStack[this.m_InfoStack.Count - 1].m_BackButtonType);
				this.m_SceneInfoWindow.SetInfo(this.m_InfoStack[this.m_InfoStack.Count - 1].m_SceneInfo);
			}
			return sceneInfoStack;
		}

		// Token: 0x06003363 RID: 13155 RVA: 0x00105498 File Offset: 0x00103898
		public void RemoveSceneInfoStack(GlobalUI.SceneInfoStack stack)
		{
			this.m_InfoStack.Remove(stack);
			if (this.m_InfoStack.Count > 0)
			{
				this.m_SceneInfoWindow.SetBackButtonIconType(this.m_InfoStack[this.m_InfoStack.Count - 1].m_BackButtonType);
				this.m_SceneInfoWindow.SetInfo(this.m_InfoStack[this.m_InfoStack.Count - 1].m_SceneInfo);
			}
		}

		// Token: 0x06003364 RID: 13156 RVA: 0x00105513 File Offset: 0x00103913
		public void CloseSceneInfo()
		{
			this.m_SceneInfoWindow.Close();
		}

		// Token: 0x06003365 RID: 13157 RVA: 0x00105520 File Offset: 0x00103920
		public void HideSceneInfo()
		{
			this.m_SceneInfoWindow.Hide();
		}

		// Token: 0x06003366 RID: 13158 RVA: 0x0010552D File Offset: 0x0010392D
		public void SetUserLevel(int lv)
		{
			this.m_LvImageNumbers.SetValue(lv);
		}

		// Token: 0x06003367 RID: 13159 RVA: 0x0010553C File Offset: 0x0010393C
		public void SetUserExp(long exp, long max)
		{
			if (max > 0L)
			{
				this.m_ExpGauge.fillAmount = (float)exp / (float)max;
			}
			else
			{
				this.m_ExpGauge.fillAmount = 0f;
			}
			this.m_ExpRemainText.text = (max - exp).ToString();
		}

		// Token: 0x06003368 RID: 13160 RVA: 0x00105594 File Offset: 0x00103994
		public void SetUserStamina(long stamina, long staminaMax)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append(UIUtility.StaminaValueToString(stamina, true));
			this.m_sb.Append("/");
			this.m_sb.Append(UIUtility.StaminaValueToString(staminaMax, false)).ToString();
			this.m_StaminaText.text = this.m_sb.ToString();
			if (staminaMax > 0L)
			{
				this.m_StaminaGauge.fillAmount = (float)stamina / (float)staminaMax;
			}
			else
			{
				this.m_StaminaGauge.fillAmount = 0f;
			}
		}

		// Token: 0x06003369 RID: 13161 RVA: 0x0010562C File Offset: 0x00103A2C
		public void SetUserRemainSec(float sec)
		{
			int num = (int)sec / 60;
			int value = (int)sec - num * 60;
			if (sec <= 0f)
			{
				if (this.m_StaminaRemainObj.activeSelf)
				{
					this.m_StaminaRemainObj.SetActive(false);
					this.m_StaminaRemainBaseObj.SetActive(false);
				}
			}
			else
			{
				if (!this.m_StaminaRemainObj.activeSelf)
				{
					this.m_StaminaRemainObj.SetActive(true);
					this.m_StaminaRemainBaseObj.SetActive(true);
				}
				this.m_StaminaRemainTimeMin.SetValue(num);
				this.m_StaminaRemainTimeSec.SetValue(value);
			}
		}

		// Token: 0x0600336A RID: 13162 RVA: 0x001056BF File Offset: 0x00103ABF
		public void SetUserGold(long gold)
		{
			if (this.m_GoldOld != gold)
			{
				this.m_GoldTextObj.text = UIUtility.GoldValueToString(gold, true);
				this.m_GoldOld = gold;
			}
		}

		// Token: 0x0600336B RID: 13163 RVA: 0x001056E6 File Offset: 0x00103AE6
		public void SetUserGem(long gem)
		{
			if (this.m_GemOld != gem)
			{
				this.m_GemTextObj.text = UIUtility.GemValueToString(gem, true);
				this.m_GemOld = gem;
			}
		}

		// Token: 0x0600336C RID: 13164 RVA: 0x0010570D File Offset: 0x00103B0D
		public void ToggleShortCutWindow()
		{
			if (this.m_ShortCutWindow.IsOpenOrIn())
			{
				this.CloseShortCutWindow();
			}
			else
			{
				this.OpenShortCutWindow();
			}
		}

		// Token: 0x0600336D RID: 13165 RVA: 0x00105730 File Offset: 0x00103B30
		private void OpenShortCutWindow()
		{
			this.m_ShortCutWindow.Open();
			this.m_CancelButton.gameObject.SetActive(true);
			this.m_CancelButton.IsEnableInput = true;
		}

		// Token: 0x0600336E RID: 13166 RVA: 0x0010575A File Offset: 0x00103B5A
		private void CloseShortCutWindow()
		{
			if (this.m_ShortCutWindow.IsOpenOrIn())
			{
				this.m_ShortCutWindow.Close();
			}
			this.m_CancelButton.gameObject.SetActive(false);
			this.m_CancelButton.IsEnableInput = false;
		}

		// Token: 0x0600336F RID: 13167 RVA: 0x00105794 File Offset: 0x00103B94
		public void OnShortCutButtonCallBack(GlobalUI.eShortCutButton shortCut)
		{
			if (shortCut == GlobalUI.eShortCutButton.Mission)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.MissionMng.OpenUI();
			}
			else if (this.m_StateController.ChangeState(4))
			{
				this.m_SelectedShortCutButton = shortCut;
				this.CloseShortCutWindow();
				if (this.OnClickBackButton != null)
				{
					this.OnClickBackButton(true);
				}
				else
				{
					base.StartCoroutine(this.ShortCutCoroutine());
				}
			}
		}

		// Token: 0x06003370 RID: 13168 RVA: 0x00105804 File Offset: 0x00103C04
		private IEnumerator ShortCutCoroutine()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.OpenQuickIfNotDisplay(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
			while (!SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
			{
				yield return new WaitForEndOfFrame();
			}
			this.ExecShortCut();
			yield break;
		}

		// Token: 0x06003371 RID: 13169 RVA: 0x00105820 File Offset: 0x00103C20
		public bool ExecShortCut()
		{
			if (this.m_SelectedShortCutButton == GlobalUI.eShortCutButton.None)
			{
				return false;
			}
			switch (this.m_SelectedShortCutButton)
			{
			case GlobalUI.eShortCutButton.Quest:
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Quest);
				break;
			case GlobalUI.eShortCutButton.Edit:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuUpgradeStart = false;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Edit);
				break;
			case GlobalUI.eShortCutButton.Upgrade:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuUpgradeStart = true;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Edit);
				break;
			case GlobalUI.eShortCutButton.Library:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart = false;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart = true;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Menu);
				break;
			case GlobalUI.eShortCutButton.Present:
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Present);
				break;
			case GlobalUI.eShortCutButton.Home:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsHomeModeStart = true;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Town);
				break;
			case GlobalUI.eShortCutButton.Gacha:
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Gacha);
				break;
			case GlobalUI.eShortCutButton.Shop:
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Shop);
				break;
			case GlobalUI.eShortCutButton.Room:
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Room);
				break;
			case GlobalUI.eShortCutButton.Friend:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart = true;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart = false;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Menu);
				break;
			case GlobalUI.eShortCutButton.Menu:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart = false;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuLibraryStart = false;
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Menu);
				break;
			}
			this.m_SelectedShortCutButton = GlobalUI.eShortCutButton.None;
			return true;
		}

		// Token: 0x06003372 RID: 13170 RVA: 0x001059D5 File Offset: 0x00103DD5
		public void ResetSelectedShortCut()
		{
			this.m_SelectedShortCutButton = GlobalUI.eShortCutButton.None;
		}

		// Token: 0x06003373 RID: 13171 RVA: 0x001059DE File Offset: 0x00103DDE
		private void SetMissionBadge(int value)
		{
			this.SetBadgeValue(GlobalUI.eShortCutButton.Mission, value);
		}

		// Token: 0x06003374 RID: 13172 RVA: 0x001059E8 File Offset: 0x00103DE8
		private void SetFriendBadge(int value)
		{
			this.SetBadgeValue(GlobalUI.eShortCutButton.Friend, value);
		}

		// Token: 0x06003375 RID: 13173 RVA: 0x001059F3 File Offset: 0x00103DF3
		private void SetPresentBadge(int value)
		{
			this.SetBadgeValue(GlobalUI.eShortCutButton.Present, value);
		}

		// Token: 0x06003376 RID: 13174 RVA: 0x001059FD File Offset: 0x00103DFD
		private void SetBadgeValue(GlobalUI.eShortCutButton shortCutButton, int value)
		{
			if (value != this.m_ShortCutButtonInstList[(int)shortCutButton].GetBadgeValue())
			{
				this.m_ShortCutButtonInstList[(int)shortCutButton].SetBadgeValue(value);
			}
		}

		// Token: 0x06003377 RID: 13175 RVA: 0x00105A28 File Offset: 0x00103E28
		private void ApplyTotalBadge()
		{
			int num = 0;
			for (int i = 0; i < this.m_ShortCutButtonInstList.Count; i++)
			{
				num += this.m_ShortCutButtonInstList[i].GetBadgeValue();
			}
			this.m_ShortCutToggleBadge.GetInst<Badge>().Apply(num);
		}

		// Token: 0x06003378 RID: 13176 RVA: 0x00105A78 File Offset: 0x00103E78
		public void OnClickBackButtonCallBack()
		{
			this.CloseShortCutWindow();
			this.ResetSelectedShortCut();
			this.OnClickBackButton.Call(false);
		}

		// Token: 0x06003379 RID: 13177 RVA: 0x00105A92 File Offset: 0x00103E92
		public void OnClickAddStaminaButtonCallBack()
		{
			this.CloseShortCutWindow();
			SingletonMonoBehaviour<StaminaShopPlayer>.Inst.Open(0, null);
		}

		// Token: 0x0600337A RID: 13178 RVA: 0x00105AA6 File Offset: 0x00103EA6
		public void OnClickAddGemButtonCallBack()
		{
			this.CloseShortCutWindow();
			SingletonMonoBehaviour<GemShopPlayer>.Inst.Open(0, null);
		}

		// Token: 0x0600337B RID: 13179 RVA: 0x00105ABA File Offset: 0x00103EBA
		public void OnClickCancel()
		{
			this.CloseShortCutWindow();
		}

		// Token: 0x0600337C RID: 13180 RVA: 0x00105AC4 File Offset: 0x00103EC4
		public void SetEnableInputFromUIGroup(UIGroup uiGroup, bool flg)
		{
			int num = this.m_EnableInputUIGroupList.IndexOf(uiGroup);
			if (num == -1 && flg)
			{
				this.m_EnableInputUIGroupList.Add(uiGroup);
			}
			else if (num != -1 && !flg)
			{
				this.m_EnableInputUIGroupList.RemoveAt(num);
			}
			this.UpdateInput();
		}

		// Token: 0x0600337D RID: 13181 RVA: 0x00105B1B File Offset: 0x00103F1B
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BackButton, new Func<bool>(this.CheckUpdateAndroidBackKey));
		}

		// Token: 0x0600337E RID: 13182 RVA: 0x00105B34 File Offset: 0x00103F34
		private bool CheckUpdateAndroidBackKey()
		{
			return Utility.CheckTapUI(this.m_BackButtonRaycastTargetObj);
		}

		// Token: 0x0400397D RID: 14717
		private const float BADGE_REFRESH_SEC = 1800f;

		// Token: 0x0400397E RID: 14718
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x0400397F RID: 14719
		[SerializeField]
		private AnimUIPlayer[] m_Anims;

		// Token: 0x04003980 RID: 14720
		private bool m_EnableInput = true;

		// Token: 0x04003981 RID: 14721
		[SerializeField]
		private CustomButtonGroup m_CustomButtonGroup;

		// Token: 0x04003982 RID: 14722
		[SerializeField]
		private UIGroup m_BaseWindow;

		// Token: 0x04003983 RID: 14723
		[SerializeField]
		private GlobalSceneInfoWindow m_SceneInfoWindow;

		// Token: 0x04003984 RID: 14724
		[SerializeField]
		private UIGroup m_ShortCutWindow;

		// Token: 0x04003985 RID: 14725
		[SerializeField]
		private ImageNumbers m_LvImageNumbers;

		// Token: 0x04003986 RID: 14726
		[SerializeField]
		private Image m_ExpGauge;

		// Token: 0x04003987 RID: 14727
		[SerializeField]
		private Text m_ExpRemainText;

		// Token: 0x04003988 RID: 14728
		[SerializeField]
		private Image m_StaminaGauge;

		// Token: 0x04003989 RID: 14729
		[SerializeField]
		private Text m_StaminaText;

		// Token: 0x0400398A RID: 14730
		[SerializeField]
		private GameObject m_StaminaRemainObj;

		// Token: 0x0400398B RID: 14731
		[SerializeField]
		private GameObject m_StaminaRemainBaseObj;

		// Token: 0x0400398C RID: 14732
		[SerializeField]
		private ImageNumbers m_StaminaRemainTimeMin;

		// Token: 0x0400398D RID: 14733
		[SerializeField]
		private ImageNumbers m_StaminaRemainTimeSec;

		// Token: 0x0400398E RID: 14734
		[SerializeField]
		private CustomButton m_AddStaminaButton;

		// Token: 0x0400398F RID: 14735
		[SerializeField]
		private Text m_GemTextObj;

		// Token: 0x04003990 RID: 14736
		[SerializeField]
		private CustomButton m_GemAddButton;

		// Token: 0x04003991 RID: 14737
		[SerializeField]
		private Text m_GoldTextObj;

		// Token: 0x04003992 RID: 14738
		[SerializeField]
		private CustomButton m_ShortCutButton;

		// Token: 0x04003993 RID: 14739
		[SerializeField]
		private PrefabCloner m_ShortCutToggleBadge;

		// Token: 0x04003994 RID: 14740
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x04003995 RID: 14741
		[SerializeField]
		private RectTransform m_ShortCutButtonParent;

		// Token: 0x04003996 RID: 14742
		[SerializeField]
		private ShortCutButton m_ShortCutButtonPrefab;

		// Token: 0x04003997 RID: 14743
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04003998 RID: 14744
		[SerializeField]
		private GameObject m_BackButtonRaycastTargetObj;

		// Token: 0x04003999 RID: 14745
		private List<ShortCutButton> m_ShortCutButtonInstList = new List<ShortCutButton>();

		// Token: 0x0400399A RID: 14746
		private List<UIGroup> m_EnableInputUIGroupList = new List<UIGroup>();

		// Token: 0x0400399B RID: 14747
		private List<GlobalUI.SceneInfoStack> m_InfoStack = new List<GlobalUI.SceneInfoStack>();

		// Token: 0x0400399C RID: 14748
		private GlobalUI.eShortCutButton m_SelectedShortCutButton = GlobalUI.eShortCutButton.None;

		// Token: 0x0400399D RID: 14749
		private int m_CurrentUserLevel = -1;

		// Token: 0x0400399E RID: 14750
		private int m_MaxExp = -1;

		// Token: 0x0400399F RID: 14751
		private long m_CurrentExp = -1L;

		// Token: 0x040039A0 RID: 14752
		private bool m_BackButtonOnly;

		// Token: 0x040039A1 RID: 14753
		private long m_GoldOld = -1L;

		// Token: 0x040039A2 RID: 14754
		private long m_GemOld = -1L;

		// Token: 0x040039A3 RID: 14755
		private StringBuilder m_sb = new StringBuilder();

		// Token: 0x020009AE RID: 2478
		private enum eUIState
		{
			// Token: 0x040039A6 RID: 14758
			Hide,
			// Token: 0x040039A7 RID: 14759
			In,
			// Token: 0x040039A8 RID: 14760
			Wait,
			// Token: 0x040039A9 RID: 14761
			Out,
			// Token: 0x040039AA RID: 14762
			ShortCutWait,
			// Token: 0x040039AB RID: 14763
			Num
		}

		// Token: 0x020009AF RID: 2479
		public class SceneInfoStack
		{
			// Token: 0x0600337F RID: 13183 RVA: 0x00105B49 File Offset: 0x00103F49
			public SceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType backButtonType, eSceneInfo sceneInfo)
			{
				this.m_BackButtonType = backButtonType;
				this.m_SceneInfo = sceneInfo;
			}

			// Token: 0x040039AC RID: 14764
			public GlobalSceneInfoWindow.eBackButtonIconType m_BackButtonType;

			// Token: 0x040039AD RID: 14765
			public eSceneInfo m_SceneInfo;
		}

		// Token: 0x020009B0 RID: 2480
		public enum eShortCutButton
		{
			// Token: 0x040039AF RID: 14767
			None = -1,
			// Token: 0x040039B0 RID: 14768
			Quest,
			// Token: 0x040039B1 RID: 14769
			Edit,
			// Token: 0x040039B2 RID: 14770
			Upgrade,
			// Token: 0x040039B3 RID: 14771
			Mission,
			// Token: 0x040039B4 RID: 14772
			Library,
			// Token: 0x040039B5 RID: 14773
			Present,
			// Token: 0x040039B6 RID: 14774
			Home,
			// Token: 0x040039B7 RID: 14775
			Gacha,
			// Token: 0x040039B8 RID: 14776
			Shop,
			// Token: 0x040039B9 RID: 14777
			Room,
			// Token: 0x040039BA RID: 14778
			Friend,
			// Token: 0x040039BB RID: 14779
			Menu,
			// Token: 0x040039BC RID: 14780
			Num
		}
	}
}
