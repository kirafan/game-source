﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global
{
	// Token: 0x020009B2 RID: 2482
	public class ShortCutButton : MonoBehaviour
	{
		// Token: 0x14000091 RID: 145
		// (add) Token: 0x06003385 RID: 13189 RVA: 0x00105D14 File Offset: 0x00104114
		// (remove) Token: 0x06003386 RID: 13190 RVA: 0x00105D4C File Offset: 0x0010414C
		public event Action<GlobalUI.eShortCutButton> OnClick;

		// Token: 0x06003387 RID: 13191 RVA: 0x00105D82 File Offset: 0x00104182
		public void SetShortCutTransit(GlobalUI.eShortCutButton shortCut)
		{
			this.m_ShortCut = shortCut;
			this.m_IconImage.sprite = this.m_Sprite[(int)shortCut];
		}

		// Token: 0x06003388 RID: 13192 RVA: 0x00105DA0 File Offset: 0x001041A0
		public void SetBadgeValue(int badgeValue = -1)
		{
			this.m_BadgeValue = badgeValue;
			if (this.m_BadgeValue > 0)
			{
				this.m_BadgeCloner.gameObject.SetActive(true);
				this.m_BadgeCloner.GetInst<Badge>().Apply(this.m_BadgeValue);
			}
			else
			{
				this.m_BadgeCloner.gameObject.SetActive(false);
			}
		}

		// Token: 0x06003389 RID: 13193 RVA: 0x00105DFD File Offset: 0x001041FD
		public int GetBadgeValue()
		{
			return this.m_BadgeValue;
		}

		// Token: 0x0600338A RID: 13194 RVA: 0x00105E05 File Offset: 0x00104205
		public void OnClickButtonCallBack()
		{
			this.OnClick.Call(this.m_ShortCut);
		}

		// Token: 0x040039C3 RID: 14787
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x040039C4 RID: 14788
		private GlobalUI.eShortCutButton m_ShortCut;

		// Token: 0x040039C5 RID: 14789
		[SerializeField]
		private Sprite[] m_Sprite;

		// Token: 0x040039C6 RID: 14790
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040039C7 RID: 14791
		[SerializeField]
		private PrefabCloner m_BadgeCloner;

		// Token: 0x040039C8 RID: 14792
		private int m_BadgeValue;
	}
}
