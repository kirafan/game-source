﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Global
{
	// Token: 0x020009AB RID: 2475
	public class GlobalSceneInfoWindow : UIGroup
	{
		// Token: 0x17000328 RID: 808
		// (get) Token: 0x0600333E RID: 13118 RVA: 0x00104A27 File Offset: 0x00102E27
		public bool IsChange
		{
			get
			{
				return this.m_IsChange;
			}
		}

		// Token: 0x0600333F RID: 13119 RVA: 0x00104A2F File Offset: 0x00102E2F
		public void SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType iconType)
		{
			if (this.m_BackButtonIconType == iconType)
			{
				return;
			}
			this.m_BackButtonIconType = iconType;
			if (this.m_IsChange)
			{
				return;
			}
			this.Hide();
			this.Open();
		}

		// Token: 0x06003340 RID: 13120 RVA: 0x00104A60 File Offset: 0x00102E60
		public void SetInfo(eSceneInfo sceneInfo)
		{
			if (this.m_CurrentSceneInfo == sceneInfo)
			{
				this.Open();
				return;
			}
			this.m_CurrentSceneInfo = sceneInfo;
			this.m_SceneTitle.sprite = null;
			this.m_SceneTitle.enabled = false;
			if (this.m_Handler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_Handler);
				this.m_Handler = null;
			}
			if (sceneInfo == eSceneInfo.None)
			{
				return;
			}
			this.m_Handler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncSceneTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SceneInfoListDB.GetParam(sceneInfo).m_ResourceName);
			if (this.m_IsChange)
			{
				return;
			}
			this.Hide();
			this.Open();
		}

		// Token: 0x06003341 RID: 13121 RVA: 0x00104B18 File Offset: 0x00102F18
		public void SetInteractable(bool flg)
		{
			base.CustomButtonGroup.Interactable = flg;
		}

		// Token: 0x06003342 RID: 13122 RVA: 0x00104B28 File Offset: 0x00102F28
		private void ChangeInfo()
		{
			for (int i = 0; i < this.m_BackButtonObjs.Length; i++)
			{
				if (i == (int)this.m_BackButtonIconType)
				{
					this.m_BackButtonObjs[(int)this.m_BackButtonIconType].SetActive(true);
				}
				else
				{
					this.m_BackButtonObjs[i].SetActive(false);
				}
			}
		}

		// Token: 0x06003343 RID: 13123 RVA: 0x00104B80 File Offset: 0x00102F80
		public override void Open()
		{
			this.ChangeInfo();
			base.Open();
		}

		// Token: 0x06003344 RID: 13124 RVA: 0x00104B8E File Offset: 0x00102F8E
		public override void Close()
		{
			base.Close();
			this.m_IsChange = false;
		}

		// Token: 0x06003345 RID: 13125 RVA: 0x00104B9D File Offset: 0x00102F9D
		public override void Hide()
		{
			base.Hide();
			this.m_IsChange = false;
		}

		// Token: 0x06003346 RID: 13126 RVA: 0x00104BAC File Offset: 0x00102FAC
		private void CloseChange()
		{
			base.Close();
			this.m_IsChange = true;
		}

		// Token: 0x06003347 RID: 13127 RVA: 0x00104BBB File Offset: 0x00102FBB
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_IsChange)
			{
				this.m_IsChange = false;
				this.Open();
			}
		}

		// Token: 0x06003348 RID: 13128 RVA: 0x00104BDC File Offset: 0x00102FDC
		public override void Update()
		{
			base.Update();
			if (!this.m_SceneTitle.enabled && this.m_Handler != null && this.m_Handler.IsAvailable())
			{
				this.m_SceneTitle.sprite = this.m_Handler.Obj;
				this.m_SceneTitle.enabled = true;
			}
		}

		// Token: 0x04003973 RID: 14707
		[SerializeField]
		private GameObject[] m_BackButtonObjs;

		// Token: 0x04003974 RID: 14708
		[SerializeField]
		private Image m_SceneTitle;

		// Token: 0x04003975 RID: 14709
		private GlobalSceneInfoWindow.eBackButtonIconType m_BackButtonIconType;

		// Token: 0x04003976 RID: 14710
		private eSceneInfo m_CurrentSceneInfo = eSceneInfo.None;

		// Token: 0x04003977 RID: 14711
		private bool m_IsChange;

		// Token: 0x04003978 RID: 14712
		private SpriteHandler m_Handler;

		// Token: 0x020009AC RID: 2476
		public enum eBackButtonIconType
		{
			// Token: 0x0400397A RID: 14714
			Back,
			// Token: 0x0400397B RID: 14715
			Home,
			// Token: 0x0400397C RID: 14716
			Town
		}
	}
}
