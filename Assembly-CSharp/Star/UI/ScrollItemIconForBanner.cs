﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000820 RID: 2080
	public class ScrollItemIconForBanner : ScrollItemIcon
	{
		// Token: 0x06002B4E RID: 11086 RVA: 0x000E4538 File Offset: 0x000E2938
		protected override void ApplyData()
		{
			base.ApplyData();
			this.m_ItemData = (InfoUIScrollItemData)this.m_Data;
			this.SetBannerSprite(this.m_ItemData.m_Sprite);
			this.SetTimeRemaining(this.m_ItemData.m_startTime, this.m_ItemData.m_endTime);
			if (!this.m_IsDoneSetup)
			{
				this.EnableNewIcon(false);
				this.m_CustomButton.m_OnClick.AddListener(new UnityAction(this.OnClickButton));
				this.m_IsDoneSetup = true;
			}
		}

		// Token: 0x06002B4F RID: 11087 RVA: 0x000E45BE File Offset: 0x000E29BE
		private void SetBannerSprite(Sprite sprite)
		{
			if (this.m_BannerImage != null)
			{
				this.m_BannerImage.sprite = sprite;
			}
		}

		// Token: 0x06002B50 RID: 11088 RVA: 0x000E45E0 File Offset: 0x000E29E0
		private void SetTimeRemaining(DateTime startTime, DateTime endTime)
		{
			if (this.m_BannerText != null)
			{
				this.m_BannerText.text = "▼開催期間 " + string.Format("{0}/{1} {2:D2}:{3:D2}", new object[]
				{
					startTime.Month,
					startTime.Day,
					startTime.Hour,
					startTime.Minute
				}) + "～" + string.Format("{0}/{1} {2:D2}:{3:D2}", new object[]
				{
					endTime.Month,
					endTime.Day,
					endTime.Hour,
					endTime.Minute
				});
			}
		}

		// Token: 0x06002B51 RID: 11089 RVA: 0x000E46B0 File Offset: 0x000E2AB0
		public void EnableNewIcon(bool flg)
		{
			if (this.m_NewIcon != null)
			{
				this.m_NewIcon.enabled = flg;
			}
		}

		// Token: 0x06002B52 RID: 11090 RVA: 0x000E46CF File Offset: 0x000E2ACF
		public void OnClickButton()
		{
			if (this.m_ItemData != null)
			{
				this.m_ItemData.m_InfoUI.OnClickBannerButton(this.m_ItemData.m_URL);
			}
		}

		// Token: 0x040031A9 RID: 12713
		public Image m_BannerImage;

		// Token: 0x040031AA RID: 12714
		public Text m_BannerText;

		// Token: 0x040031AB RID: 12715
		public Image m_NewIcon;

		// Token: 0x040031AC RID: 12716
		public CustomButton m_CustomButton;

		// Token: 0x040031AD RID: 12717
		public InfoUIScrollItemData m_ItemData;

		// Token: 0x040031AE RID: 12718
		private bool m_IsDoneSetup;
	}
}
