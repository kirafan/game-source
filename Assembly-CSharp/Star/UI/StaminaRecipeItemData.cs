﻿using System;

namespace Star.UI
{
	// Token: 0x02000AC9 RID: 2761
	public class StaminaRecipeItemData : ScrollItemData
	{
		// Token: 0x060039AF RID: 14767 RVA: 0x0012617C File Offset: 0x0012457C
		public StaminaRecipeItemData(int itemID, Action<int, int> onClick)
		{
			this.OnClickCallBack = onClick;
			long num;
			if (itemID == -1)
			{
				this.m_Title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem);
				this.m_ItemID = itemID;
				num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem;
				this.m_UseNum = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.StaminaAdditiveAmount);
				this.m_HaveNumText = UIUtility.GemValueToString(num, true);
				this.m_DetailText = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StaminaShopRecipeDetail, new object[]
				{
					this.m_UseNum,
					100,
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax()
				});
			}
			else
			{
				this.m_Title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID).m_Name;
				this.m_ItemID = itemID;
				num = (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(itemID);
				this.m_UseNum = this.ITEM_USE_NUM;
				int num2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID).m_TypeArgs[0];
				long num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax() * (long)num2 / 100L;
				this.m_HaveNumText = UIUtility.ItemHaveNumToString((int)num, true);
				this.m_DetailText = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StaminaShopRecipeDetail, new object[]
				{
					this.m_UseNum,
					num2,
					num3
				});
			}
			if (num < (long)this.m_UseNum)
			{
				this.m_Interactable = false;
			}
		}

		// Token: 0x060039B0 RID: 14768 RVA: 0x00126356 File Offset: 0x00124756
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_ItemID, this.m_UseNum);
		}

		// Token: 0x040040EB RID: 16619
		public int ITEM_USE_NUM = 1;

		// Token: 0x040040EC RID: 16620
		public bool m_Interactable = true;

		// Token: 0x040040ED RID: 16621
		public string m_Title;

		// Token: 0x040040EE RID: 16622
		public int m_ItemID;

		// Token: 0x040040EF RID: 16623
		public int m_UseNum;

		// Token: 0x040040F0 RID: 16624
		public string m_HaveNumText;

		// Token: 0x040040F1 RID: 16625
		public string m_DetailText;

		// Token: 0x040040F2 RID: 16626
		private Action<int, int> OnClickCallBack;
	}
}
