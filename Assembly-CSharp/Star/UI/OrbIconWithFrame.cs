﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200086B RID: 2155
	public class OrbIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002CB5 RID: 11445 RVA: 0x000EBEE1 File Offset: 0x000EA2E1
		public void Apply(int orbID)
		{
			this.m_Icon.Apply(orbID);
		}

		// Token: 0x06002CB6 RID: 11446 RVA: 0x000EBEEF File Offset: 0x000EA2EF
		public void Destroy()
		{
			if (this.m_Icon != null)
			{
				this.m_Icon.Destroy();
			}
		}

		// Token: 0x040033AF RID: 13231
		[SerializeField]
		private OrbIcon m_Icon;
	}
}
