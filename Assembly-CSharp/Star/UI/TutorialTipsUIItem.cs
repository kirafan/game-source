﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B19 RID: 2841
	public class TutorialTipsUIItem : InfiniteScrollItem
	{
		// Token: 0x06003BB9 RID: 15289 RVA: 0x00130DE4 File Offset: 0x0012F1E4
		public override void Apply(InfiniteScrollItemData data)
		{
			base.Apply(data);
			TutorialTipsUIItemData tutorialTipsUIItemData = (TutorialTipsUIItemData)data;
			this.m_Text.text = tutorialTipsUIItemData.m_Message;
		}

		// Token: 0x06003BBA RID: 15290 RVA: 0x00130E10 File Offset: 0x0012F210
		public override void Destroy()
		{
			if (this.m_ImageImage != null)
			{
				this.m_ImageImage.sprite = null;
			}
			base.Destroy();
		}

		// Token: 0x06003BBB RID: 15291 RVA: 0x00130E35 File Offset: 0x0012F235
		private void Update()
		{
		}

		// Token: 0x04004351 RID: 17233
		[SerializeField]
		private Image m_ImageImage;

		// Token: 0x04004352 RID: 17234
		[SerializeField]
		private Text m_Text;
	}
}
