﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000B1D RID: 2845
	public class UISettings
	{
		// Token: 0x06003BC1 RID: 15297 RVA: 0x00131170 File Offset: 0x0012F570
		public void Setup(LocalSaveData localSaveData)
		{
			this.m_LocalSaveData = localSaveData;
			eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
			this.FilterCategoryMaxs = new int[8];
			this.FilterCategoryMaxs[0] = 5;
			this.FilterCategoryMaxs[1] = 5;
			this.FilterCategoryMaxs[2] = 6;
			this.FilterCategoryMaxs[3] = eTitleTypeConverter.HowManyTitles();
			this.FilterCategoryMaxs[4] = 2;
			this.FilterCategoryMaxs[5] = 5;
			this.FilterCategoryMaxs[6] = 6;
			this.FilterCategoryMaxs[7] = eTitleTypeConverter.HowManyTitles() + 1;
			if (this.m_LocalSaveData != null)
			{
				bool flag = false;
				if (this.m_LocalSaveData.UISortParams == null)
				{
					this.m_LocalSaveData.UISortParams = new List<LocalSaveData.UISortParam>();
				}
				int num = 3 - this.m_LocalSaveData.UISortParams.Count;
				for (int i = 0; i < num; i++)
				{
					this.m_LocalSaveData.UISortParams.Add(new LocalSaveData.UISortParam());
				}
				if (num > 0)
				{
					flag = true;
				}
				if (this.m_LocalSaveData.UIFilterParams == null)
				{
					this.m_LocalSaveData.UIFilterParams = new List<LocalSaveData.UIFilterParam>();
				}
				int num2 = 7 - this.m_LocalSaveData.UIFilterParams.Count;
				for (int j = 0; j < num2; j++)
				{
					this.m_LocalSaveData.UIFilterParams.Add(new LocalSaveData.UIFilterParam(1L));
				}
				if (num2 > 0)
				{
					flag = true;
				}
				if (flag)
				{
					this.Save();
				}
			}
		}

		// Token: 0x06003BC2 RID: 15298 RVA: 0x001312D8 File Offset: 0x0012F6D8
		public void SetFilterDefault(UISettings.eUsingType_Filter usingType)
		{
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				uifilterParam.Clear();
				for (int i = 0; i < uifilterParam.GetTypeNum(); i++)
				{
					uifilterParam.SetFlag(i, 0, true);
				}
			}
		}

		// Token: 0x06003BC3 RID: 15299 RVA: 0x00131328 File Offset: 0x0012F728
		public void SetSortDefault(UISettings.eUsingType_Sort usingType)
		{
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UISortParam uisortParam = this.m_LocalSaveData.UISortParams[(int)usingType];
				uisortParam.Clear();
			}
		}

		// Token: 0x06003BC4 RID: 15300 RVA: 0x00131358 File Offset: 0x0012F758
		public void Save()
		{
			if (this.m_LocalSaveData != null)
			{
				this.m_LocalSaveData.Save();
			}
		}

		// Token: 0x06003BC5 RID: 15301 RVA: 0x00131370 File Offset: 0x0012F770
		public ushort GetSortParamValue(UISettings.eUsingType_Sort type)
		{
			LocalSaveData.UISortParam uisortParam;
			if (this.m_LocalSaveData != null)
			{
				uisortParam = this.m_LocalSaveData.UISortParams[(int)type];
			}
			else
			{
				uisortParam = new LocalSaveData.UISortParam();
			}
			return uisortParam.m_Value;
		}

		// Token: 0x06003BC6 RID: 15302 RVA: 0x001313B0 File Offset: 0x0012F7B0
		public ushort GetSortParamOrderType(UISettings.eUsingType_Sort type)
		{
			LocalSaveData.UISortParam uisortParam;
			if (this.m_LocalSaveData != null)
			{
				uisortParam = this.m_LocalSaveData.UISortParams[(int)type];
			}
			else
			{
				uisortParam = new LocalSaveData.UISortParam();
			}
			return uisortParam.m_OrderType;
		}

		// Token: 0x06003BC7 RID: 15303 RVA: 0x001313F0 File Offset: 0x0012F7F0
		public void SetSortParamValue(UISettings.eUsingType_Sort type, ushort value)
		{
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UISortParam uisortParam = this.m_LocalSaveData.UISortParams[(int)type];
				uisortParam.m_Value = value;
			}
		}

		// Token: 0x06003BC8 RID: 15304 RVA: 0x00131424 File Offset: 0x0012F824
		public void SetSortParamOrderType(UISettings.eUsingType_Sort type, ushort orderType)
		{
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UISortParam uisortParam = this.m_LocalSaveData.UISortParams[(int)type];
				uisortParam.m_OrderType = orderType;
			}
		}

		// Token: 0x06003BC9 RID: 15305 RVA: 0x00131455 File Offset: 0x0012F855
		public void SetSortParamOrderType(UISettings.eUsingType_Sort usingType, UISettings.eSortOrderType orderType)
		{
			this.SetSortParamOrderType(usingType, (ushort)orderType);
		}

		// Token: 0x06003BCA RID: 15306 RVA: 0x00131460 File Offset: 0x0012F860
		public UISettings.UsingSortMap GetUsingSortMap(UISettings.eUsingType_Sort type)
		{
			return this.UsingSortMaps[(int)type];
		}

		// Token: 0x06003BCB RID: 15307 RVA: 0x00131474 File Offset: 0x0012F874
		public bool GetFilterFlagIsAll(UISettings.eUsingType_Filter usingType, int typeIndex)
		{
			int bitIndex = 0;
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				return uifilterParam.HasFlag(typeIndex, bitIndex);
			}
			return true;
		}

		// Token: 0x06003BCC RID: 15308 RVA: 0x001314AC File Offset: 0x0012F8AC
		public void SetFilterFlagIsAll(UISettings.eUsingType_Filter usingType, int typeIndex, bool isOn)
		{
			int bitIndex = 0;
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				uifilterParam.SetFlag(typeIndex, bitIndex, isOn);
			}
		}

		// Token: 0x06003BCD RID: 15309 RVA: 0x001314E4 File Offset: 0x0012F8E4
		public bool GetFilterFlag(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex)
		{
			bitIndex++;
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				return uifilterParam.HasFlag(typeIndex, bitIndex);
			}
			return true;
		}

		// Token: 0x06003BCE RID: 15310 RVA: 0x00131520 File Offset: 0x0012F920
		public bool GetFilterFlag(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category, int bitIndex)
		{
			UISettings.eFilterCategory[] categories = this.GetUsingFilterMap(usingType).m_Categories;
			for (int i = 0; i < categories.Length; i++)
			{
				if (categories[i] == category)
				{
					return this.GetFilterFlag(usingType, i, bitIndex);
				}
			}
			Debug.LogError(string.Concat(new object[]
			{
				"not Exist ",
				category,
				"in",
				usingType
			}));
			return false;
		}

		// Token: 0x06003BCF RID: 15311 RVA: 0x00131597 File Offset: 0x0012F997
		public bool GetFilterFlagWithAll(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex)
		{
			return this.GetFilterFlagIsAll(usingType, typeIndex) || this.GetFilterFlag(usingType, typeIndex, bitIndex);
		}

		// Token: 0x06003BD0 RID: 15312 RVA: 0x001315B4 File Offset: 0x0012F9B4
		public bool GetFilterFlagWithAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category, int bitIndex)
		{
			UISettings.eFilterCategory[] categories = this.GetUsingFilterMap(usingType).m_Categories;
			for (int i = 0; i < categories.Length; i++)
			{
				if (categories[i] == category)
				{
					return this.GetFilterFlagWithAll(usingType, i, bitIndex);
				}
			}
			Debug.LogError(string.Concat(new object[]
			{
				"not Exist ",
				category,
				"in",
				usingType
			}));
			return false;
		}

		// Token: 0x06003BD1 RID: 15313 RVA: 0x0013162C File Offset: 0x0012FA2C
		public bool[] GetFilterFlagsWithAll(UISettings.eUsingType_Filter usingType, int typeIndex, UISettings.eFilterCategory category)
		{
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				bool[] array = new bool[this.GetFilterCategoryMax(category)];
				for (int i = 0; i < array.Length; i++)
				{
					if (uifilterParam.HasFlag(typeIndex, 0))
					{
						array[i] = true;
					}
					else
					{
						array[i] = uifilterParam.HasFlag(typeIndex, i + 1);
					}
				}
				return array;
			}
			return null;
		}

		// Token: 0x06003BD2 RID: 15314 RVA: 0x001316A0 File Offset: 0x0012FAA0
		public bool[] GetFilterFlagsWithAll(UISettings.eUsingType_Filter usingType, UISettings.eFilterCategory category)
		{
			UISettings.eFilterCategory[] categories = this.GetUsingFilterMap(usingType).m_Categories;
			for (int i = 0; i < categories.Length; i++)
			{
				if (categories[i] == category)
				{
					return this.GetFilterFlagsWithAll(usingType, i, category);
				}
			}
			Debug.LogError(string.Concat(new object[]
			{
				"not Exist ",
				category,
				"in",
				usingType
			}));
			return null;
		}

		// Token: 0x06003BD3 RID: 15315 RVA: 0x00131718 File Offset: 0x0012FB18
		public void SetFilterFlag(UISettings.eUsingType_Filter usingType, int typeIndex, int bitIndex, bool isOn)
		{
			bitIndex++;
			if (this.m_LocalSaveData != null)
			{
				LocalSaveData.UIFilterParam uifilterParam = this.m_LocalSaveData.UIFilterParams[(int)usingType];
				uifilterParam.SetFlag(typeIndex, bitIndex, isOn);
			}
		}

		// Token: 0x06003BD4 RID: 15316 RVA: 0x00131751 File Offset: 0x0012FB51
		public UISettings.UsingFilterMap GetUsingFilterMap(UISettings.eUsingType_Filter usingType)
		{
			return this.UsingFilterMaps[(int)usingType];
		}

		// Token: 0x06003BD5 RID: 15317 RVA: 0x00131764 File Offset: 0x0012FB64
		public int GetFilterCategoryMax(UISettings.eFilterCategory category)
		{
			return this.FilterCategoryMaxs[(int)category];
		}

		// Token: 0x04004380 RID: 17280
		private LocalSaveData m_LocalSaveData;

		// Token: 0x04004381 RID: 17281
		public const int USING_TYPE_SORT_NUM = 3;

		// Token: 0x04004382 RID: 17282
		private readonly UISettings.UsingSortMap[] UsingSortMaps = new UISettings.UsingSortMap[]
		{
			new UISettings.UsingSortMap(11),
			new UISettings.UsingSortMap(9),
			new UISettings.UsingSortMap(9)
		};

		// Token: 0x04004383 RID: 17283
		public const int FILTER_CATEGORY_NUM = 8;

		// Token: 0x04004384 RID: 17284
		private int[] FilterCategoryMaxs;

		// Token: 0x04004385 RID: 17285
		public const int USING_TYPE_FILTER_NUM = 7;

		// Token: 0x04004386 RID: 17286
		private readonly UISettings.UsingFilterMap[] UsingFilterMaps = new UISettings.UsingFilterMap[]
		{
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.Class,
				UISettings.eFilterCategory.Rare,
				UISettings.eFilterCategory.Element,
				UISettings.eFilterCategory.Title
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.Class,
				UISettings.eFilterCategory.Rare
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.Rare
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.Class,
				UISettings.eFilterCategory.Rare,
				UISettings.eFilterCategory.Element
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.TitleSelect
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.DeadLine,
				UISettings.eFilterCategory.PresentType
			}),
			new UISettings.UsingFilterMap(new UISettings.eFilterCategory[]
			{
				UISettings.eFilterCategory.Class,
				UISettings.eFilterCategory.Rare,
				UISettings.eFilterCategory.Element,
				UISettings.eFilterCategory.StatusPriority,
				UISettings.eFilterCategory.Title
			})
		};

		// Token: 0x02000B1E RID: 2846
		public enum eSortOrderType
		{
			// Token: 0x04004388 RID: 17288
			Ascending,
			// Token: 0x04004389 RID: 17289
			Descending,
			// Token: 0x0400438A RID: 17290
			Num
		}

		// Token: 0x02000B1F RID: 2847
		public enum eSortValue_Chara
		{
			// Token: 0x0400438C RID: 17292
			Level,
			// Token: 0x0400438D RID: 17293
			Rare,
			// Token: 0x0400438E RID: 17294
			Cost,
			// Token: 0x0400438F RID: 17295
			Title,
			// Token: 0x04004390 RID: 17296
			GetTime,
			// Token: 0x04004391 RID: 17297
			Hp,
			// Token: 0x04004392 RID: 17298
			Atk,
			// Token: 0x04004393 RID: 17299
			Mgc,
			// Token: 0x04004394 RID: 17300
			Def,
			// Token: 0x04004395 RID: 17301
			MDef,
			// Token: 0x04004396 RID: 17302
			Spd,
			// Token: 0x04004397 RID: 17303
			Num
		}

		// Token: 0x02000B20 RID: 2848
		public enum eSortValue_Weapon
		{
			// Token: 0x04004399 RID: 17305
			Level,
			// Token: 0x0400439A RID: 17306
			Rare,
			// Token: 0x0400439B RID: 17307
			Cost,
			// Token: 0x0400439C RID: 17308
			HaveNum,
			// Token: 0x0400439D RID: 17309
			Get,
			// Token: 0x0400439E RID: 17310
			Atk,
			// Token: 0x0400439F RID: 17311
			Mgc,
			// Token: 0x040043A0 RID: 17312
			Def,
			// Token: 0x040043A1 RID: 17313
			MDef,
			// Token: 0x040043A2 RID: 17314
			Num
		}

		// Token: 0x02000B21 RID: 2849
		public enum eSortValue_Support
		{
			// Token: 0x040043A4 RID: 17316
			Level,
			// Token: 0x040043A5 RID: 17317
			Rare,
			// Token: 0x040043A6 RID: 17318
			Cost,
			// Token: 0x040043A7 RID: 17319
			HP,
			// Token: 0x040043A8 RID: 17320
			Atk,
			// Token: 0x040043A9 RID: 17321
			Mgc,
			// Token: 0x040043AA RID: 17322
			Def,
			// Token: 0x040043AB RID: 17323
			MDef,
			// Token: 0x040043AC RID: 17324
			Spd,
			// Token: 0x040043AD RID: 17325
			Num
		}

		// Token: 0x02000B22 RID: 2850
		public enum eUsingType_Sort
		{
			// Token: 0x040043AF RID: 17327
			None = -1,
			// Token: 0x040043B0 RID: 17328
			CharaListView,
			// Token: 0x040043B1 RID: 17329
			WeaponList,
			// Token: 0x040043B2 RID: 17330
			SupportSelect,
			// Token: 0x040043B3 RID: 17331
			Num
		}

		// Token: 0x02000B23 RID: 2851
		public struct UsingSortMap
		{
			// Token: 0x06003BD6 RID: 15318 RVA: 0x0013176E File Offset: 0x0012FB6E
			public UsingSortMap(ushort sortValueMax)
			{
				this.m_SortValueMax = sortValueMax;
			}

			// Token: 0x040043B4 RID: 17332
			public ushort m_SortValueMax;
		}

		// Token: 0x02000B24 RID: 2852
		public enum eFilterFlag_DeadLine
		{
			// Token: 0x040043B6 RID: 17334
			Exist,
			// Token: 0x040043B7 RID: 17335
			NoExist,
			// Token: 0x040043B8 RID: 17336
			Num
		}

		// Token: 0x02000B25 RID: 2853
		public enum eFilterFlag_PresentType
		{
			// Token: 0x040043BA RID: 17338
			Chara,
			// Token: 0x040043BB RID: 17339
			Weapon,
			// Token: 0x040043BC RID: 17340
			Item,
			// Token: 0x040043BD RID: 17341
			Gold,
			// Token: 0x040043BE RID: 17342
			Etc,
			// Token: 0x040043BF RID: 17343
			Num
		}

		// Token: 0x02000B26 RID: 2854
		public enum eFilterCategory
		{
			// Token: 0x040043C1 RID: 17345
			Class,
			// Token: 0x040043C2 RID: 17346
			Rare,
			// Token: 0x040043C3 RID: 17347
			Element,
			// Token: 0x040043C4 RID: 17348
			Title,
			// Token: 0x040043C5 RID: 17349
			DeadLine,
			// Token: 0x040043C6 RID: 17350
			PresentType,
			// Token: 0x040043C7 RID: 17351
			StatusPriority,
			// Token: 0x040043C8 RID: 17352
			TitleSelect,
			// Token: 0x040043C9 RID: 17353
			Num
		}

		// Token: 0x02000B27 RID: 2855
		public enum eUsingType_Filter
		{
			// Token: 0x040043CB RID: 17355
			None = -1,
			// Token: 0x040043CC RID: 17356
			CharaListView,
			// Token: 0x040043CD RID: 17357
			WeaponList,
			// Token: 0x040043CE RID: 17358
			WeaponEquip,
			// Token: 0x040043CF RID: 17359
			SupportSelect,
			// Token: 0x040043D0 RID: 17360
			SupportTitle,
			// Token: 0x040043D1 RID: 17361
			Present,
			// Token: 0x040043D2 RID: 17362
			Recommend,
			// Token: 0x040043D3 RID: 17363
			Num
		}

		// Token: 0x02000B28 RID: 2856
		public struct UsingFilterMap
		{
			// Token: 0x06003BD7 RID: 15319 RVA: 0x00131777 File Offset: 0x0012FB77
			public UsingFilterMap(UISettings.eFilterCategory[] categories)
			{
				this.m_Categories = categories;
			}

			// Token: 0x040043D4 RID: 17364
			public UISettings.eFilterCategory[] m_Categories;
		}
	}
}
