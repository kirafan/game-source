﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Present
{
	// Token: 0x02000A3D RID: 2621
	public class PresentScrollItem : ScrollItemIcon
	{
		// Token: 0x06003677 RID: 13943 RVA: 0x00112B29 File Offset: 0x00110F29
		public void SetEnableGet(bool flg)
		{
			this.m_GetIcon.SetActive(flg);
			this.m_IsEnableClick = flg;
		}

		// Token: 0x06003678 RID: 13944 RVA: 0x00112B40 File Offset: 0x00110F40
		protected override void ApplyData()
		{
			PresentScrollItemData presentScrollItemData = (PresentScrollItemData)this.m_Data;
			this.m_NameText.text = presentScrollItemData.m_Name;
			if (presentScrollItemData.m_IsExistDeadline)
			{
				this.m_DeadLineText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentDate, new object[]
				{
					presentScrollItemData.m_DeadLine.Year,
					presentScrollItemData.m_DeadLine.Month,
					presentScrollItemData.m_DeadLine.Day,
					presentScrollItemData.m_DeadLine.Hour,
					presentScrollItemData.m_DeadLine.Minute
				});
			}
			else
			{
				this.m_DeadLineText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentNoExistDeadLine);
			}
			this.m_CreatedText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentDate, new object[]
			{
				presentScrollItemData.m_CreatedAt.Year,
				presentScrollItemData.m_CreatedAt.Month,
				presentScrollItemData.m_CreatedAt.Day,
				presentScrollItemData.m_CreatedAt.Hour,
				presentScrollItemData.m_CreatedAt.Minute
			});
			this.m_ReceivedText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentDate, new object[]
			{
				presentScrollItemData.m_ReceivedAt.Year,
				presentScrollItemData.m_ReceivedAt.Month,
				presentScrollItemData.m_ReceivedAt.Day,
				presentScrollItemData.m_ReceivedAt.Hour,
				presentScrollItemData.m_ReceivedAt.Minute
			});
			this.m_DescriptText.text = presentScrollItemData.m_Descript;
			if (presentScrollItemData.m_IsHistroy)
			{
				if (!this.m_HistoryObj.activeSelf)
				{
					this.m_HistoryObj.SetActive(true);
				}
				if (this.m_NormalObj.activeSelf)
				{
					this.m_NormalObj.SetActive(false);
				}
			}
			else
			{
				if (!this.m_NormalObj.activeSelf)
				{
					this.m_NormalObj.SetActive(true);
				}
				if (this.m_HistoryObj.activeSelf)
				{
					this.m_HistoryObj.SetActive(false);
				}
			}
			switch (presentScrollItemData.m_Type)
			{
			case ePresentType.Chara:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyChara(presentScrollItemData.m_ID);
				break;
			case ePresentType.Item:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyItem(presentScrollItemData.m_ID);
				break;
			case ePresentType.Weapon:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyWeapon(presentScrollItemData.m_ID);
				break;
			case ePresentType.TownFacility:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyTownObject(presentScrollItemData.m_ID, 1);
				break;
			case ePresentType.RoomObject:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyRoomObject(presentScrollItemData.m_ID);
				break;
			case ePresentType.MasterOrb:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyOrb(presentScrollItemData.m_ID);
				break;
			case ePresentType.KRRPoint:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyKirara();
				break;
			case ePresentType.Gold:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyGold();
				break;
			case ePresentType.UnlimitedGem:
			case ePresentType.LimitedGem:
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyGem();
				break;
			}
		}

		// Token: 0x06003679 RID: 13945 RVA: 0x00112ED4 File Offset: 0x001112D4
		public void OnClickGetButtonCallBack()
		{
			PresentScrollItemData presentScrollItemData = (PresentScrollItemData)this.m_Data;
			if (!presentScrollItemData.m_IsHistroy && this.m_IsEnableClick)
			{
				presentScrollItemData.ExecuteGetPresent();
			}
		}

		// Token: 0x04003CC8 RID: 15560
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x04003CC9 RID: 15561
		[SerializeField]
		private GameObject m_HistoryObj;

		// Token: 0x04003CCA RID: 15562
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x04003CCB RID: 15563
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003CCC RID: 15564
		[SerializeField]
		private Text m_DeadLineText;

		// Token: 0x04003CCD RID: 15565
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04003CCE RID: 15566
		[SerializeField]
		private Text m_CreatedText;

		// Token: 0x04003CCF RID: 15567
		[SerializeField]
		private Text m_ReceivedText;

		// Token: 0x04003CD0 RID: 15568
		[SerializeField]
		private GameObject m_GetIcon;
	}
}
