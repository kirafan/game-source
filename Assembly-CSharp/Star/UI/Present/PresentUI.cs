﻿using System;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Present
{
	// Token: 0x02000A3F RID: 2623
	public class PresentUI : MenuUIBase
	{
		// Token: 0x17000331 RID: 817
		// (get) Token: 0x0600367D RID: 13949 RVA: 0x00112F8E File Offset: 0x0011138E
		public PresentUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x0600367E RID: 13950 RVA: 0x00112F98 File Offset: 0x00111398
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_PresentWindow.Setup();
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				this.m_WindowRect.localPosition = Vector3.zero;
			}
		}

		// Token: 0x0600367F RID: 13951 RVA: 0x00113013 File Offset: 0x00111413
		public PresentWindow GetPresentWindow()
		{
			return this.m_PresentWindow;
		}

		// Token: 0x06003680 RID: 13952 RVA: 0x0011301C File Offset: 0x0011141C
		private void Update()
		{
			switch (this.m_Step)
			{
			case PresentUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_PresentWindow.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
					eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
					if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
					{
						inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.TutorialGuideTitle_0, eText_MessageDB.TutorialGuideMessage_0, null);
					}
					this.ChangeStep(PresentUI.eStep.Idle);
				}
				break;
			}
			case PresentUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_PresentWindow.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(PresentUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003681 RID: 13953 RVA: 0x00113148 File Offset: 0x00111548
		private void ChangeStep(PresentUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case PresentUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case PresentUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_PresentWindow.Open();
				break;
			case PresentUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case PresentUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_PresentWindow.Close();
				break;
			case PresentUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003682 RID: 13954 RVA: 0x00113204 File Offset: 0x00111604
		public override void PlayIn()
		{
			this.ChangeStep(PresentUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				CustomButton[] componentsInChildren = this.GetPresentWindow().GetComponentsInChildren<CustomButton>(true);
				for (int j = 0; j < componentsInChildren.Length; j++)
				{
					if (this.GetPresentWindow().GetMultiButton != componentsInChildren[j])
					{
						componentsInChildren[j].gameObject.SetActive(false);
					}
				}
				this.m_EraseGetIcon = true;
			}
		}

		// Token: 0x06003683 RID: 13955 RVA: 0x001132AC File Offset: 0x001116AC
		private void LateUpdate()
		{
			if (this.m_EraseGetIcon)
			{
				PresentScrollItem[] componentsInChildren = base.GetComponentsInChildren<PresentScrollItem>(true);
				if (componentsInChildren != null && componentsInChildren.Length > 0)
				{
					for (int i = 0; i < componentsInChildren.Length; i++)
					{
						componentsInChildren[i].SetEnableGet(false);
					}
					this.m_EraseGetIcon = false;
				}
			}
		}

		// Token: 0x06003684 RID: 13956 RVA: 0x00113300 File Offset: 0x00111700
		public override void PlayOut()
		{
			this.ChangeStep(PresentUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003685 RID: 13957 RVA: 0x0011333A File Offset: 0x0011173A
		public override bool IsMenuEnd()
		{
			return this.m_Step == PresentUI.eStep.End;
		}

		// Token: 0x04003CDC RID: 15580
		private PresentUI.eStep m_Step;

		// Token: 0x04003CDD RID: 15581
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003CDE RID: 15582
		[SerializeField]
		private PresentWindow m_PresentWindow;

		// Token: 0x04003CDF RID: 15583
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04003CE0 RID: 15584
		private PresentUI.eButton m_SelectButton = PresentUI.eButton.None;

		// Token: 0x04003CE1 RID: 15585
		private bool m_EraseGetIcon;

		// Token: 0x02000A40 RID: 2624
		private enum eStep
		{
			// Token: 0x04003CE3 RID: 15587
			Hide,
			// Token: 0x04003CE4 RID: 15588
			In,
			// Token: 0x04003CE5 RID: 15589
			Idle,
			// Token: 0x04003CE6 RID: 15590
			Out,
			// Token: 0x04003CE7 RID: 15591
			End
		}

		// Token: 0x02000A41 RID: 2625
		public enum eButton
		{
			// Token: 0x04003CE9 RID: 15593
			None = -1,
			// Token: 0x04003CEA RID: 15594
			Get,
			// Token: 0x04003CEB RID: 15595
			History
		}
	}
}
