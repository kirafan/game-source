﻿using System;

namespace Star.UI.Present
{
	// Token: 0x02000A3E RID: 2622
	public class PresentScrollItemData : ScrollItemData
	{
		// Token: 0x0600367A RID: 13946 RVA: 0x00112F0C File Offset: 0x0011130C
		public PresentScrollItemData(long presentMngID, ePresentType type, int id, string name, string descript, bool isHistory, bool isExistDeadline, DateTime deadLine, DateTime receivedAt, DateTime createdAt)
		{
			this.m_PresentMngID = presentMngID;
			this.m_Type = type;
			this.m_ID = id;
			this.m_Name = name;
			this.m_Descript = descript;
			this.m_IsExistDeadline = isExistDeadline;
			this.m_DeadLine = deadLine;
			this.m_ReceivedAt = receivedAt;
			this.m_CreatedAt = createdAt;
			this.m_IsHistroy = isHistory;
		}

		// Token: 0x0600367B RID: 13947 RVA: 0x00112F6C File Offset: 0x0011136C
		public void ExecuteGetPresent()
		{
			this.m_OnClick(this.m_PresentMngID);
		}

		// Token: 0x04003CD1 RID: 15569
		public long m_PresentMngID;

		// Token: 0x04003CD2 RID: 15570
		public ePresentType m_Type;

		// Token: 0x04003CD3 RID: 15571
		public int m_ID;

		// Token: 0x04003CD4 RID: 15572
		public string m_Name;

		// Token: 0x04003CD5 RID: 15573
		public string m_Descript;

		// Token: 0x04003CD6 RID: 15574
		public bool m_IsExistDeadline;

		// Token: 0x04003CD7 RID: 15575
		public DateTime m_DeadLine;

		// Token: 0x04003CD8 RID: 15576
		public DateTime m_ReceivedAt;

		// Token: 0x04003CD9 RID: 15577
		public DateTime m_CreatedAt;

		// Token: 0x04003CDA RID: 15578
		public bool m_IsHistroy;

		// Token: 0x04003CDB RID: 15579
		public Action<long> m_OnClick;
	}
}
