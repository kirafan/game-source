﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Present
{
	// Token: 0x02000A42 RID: 2626
	public class PresentWindow : UIGroup
	{
		// Token: 0x140000AF RID: 175
		// (add) Token: 0x06003687 RID: 13959 RVA: 0x00113384 File Offset: 0x00111784
		// (remove) Token: 0x06003688 RID: 13960 RVA: 0x001133BC File Offset: 0x001117BC
		public event Action<long> OnClickExecuteGet;

		// Token: 0x140000B0 RID: 176
		// (add) Token: 0x06003689 RID: 13961 RVA: 0x001133F4 File Offset: 0x001117F4
		// (remove) Token: 0x0600368A RID: 13962 RVA: 0x0011342C File Offset: 0x0011182C
		public event Action<List<long>> OnClickGetMultiButton;

		// Token: 0x140000B1 RID: 177
		// (add) Token: 0x0600368B RID: 13963 RVA: 0x00113464 File Offset: 0x00111864
		// (remove) Token: 0x0600368C RID: 13964 RVA: 0x0011349C File Offset: 0x0011189C
		public event Action OnEndResultWindow;

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x0600368D RID: 13965 RVA: 0x001134D2 File Offset: 0x001118D2
		public CustomButton GetMultiButton
		{
			get
			{
				return this.m_GetMultiButton;
			}
		}

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x0600368E RID: 13966 RVA: 0x001134DA File Offset: 0x001118DA
		public bool IsGetMulti
		{
			get
			{
				return this.m_ReadyGetMngID == -1L;
			}
		}

		// Token: 0x0600368F RID: 13967 RVA: 0x001134EC File Offset: 0x001118EC
		public void Setup()
		{
			this.m_Scroll.Setup();
			this.Refresh();
			this.SetMode(PresentWindow.eMode.Normal);
			this.m_ResultGroup.OnEnd += this.OnEndResultCallBack;
			this.m_DescriptText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentDescript, new object[]
			{
				99
			});
		}

		// Token: 0x06003690 RID: 13968 RVA: 0x00113557 File Offset: 0x00111957
		public override void Update()
		{
			base.Update();
			if (base.IsDonePlayOut && this.m_ReserveOpen)
			{
				this.m_ReserveOpen = false;
				this.SetMode(this.m_Mode);
				this.Open();
			}
		}

		// Token: 0x06003691 RID: 13969 RVA: 0x0011358E File Offset: 0x0011198E
		private void OnEndResultCallBack()
		{
			this.OnEndResultWindow.Call();
		}

		// Token: 0x06003692 RID: 13970 RVA: 0x0011359C File Offset: 0x0011199C
		private PresentScrollItemData DataToUIData(PresentManager.PresentData data, bool isHistory)
		{
			return new PresentScrollItemData(data.m_MngID, data.m_Type, data.m_ID, data.m_Title, data.m_Message, isHistory, !data.m_IsIndefinitePeriod, data.m_Date, data.m_ReceivedDate, data.m_CreatedDate);
		}

		// Token: 0x06003693 RID: 13971 RVA: 0x001135EC File Offset: 0x001119EC
		public void Refresh()
		{
			List<PresentManager.PresentData> list = new List<PresentManager.PresentData>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetPresentList(ePresentType.None).Count; i++)
			{
				list.Add(SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetPresentList(ePresentType.None)[i]);
			}
			list.Sort();
			this.m_DataList.Clear();
			for (int j = 0; j < list.Count; j++)
			{
				PresentScrollItemData item = this.DataToUIData(list[j], false);
				this.m_DataList.Add(item);
			}
			this.m_GetMultiButton.Interactable = (this.m_DataList.Count > 0);
			list = SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetHistoryList();
			this.m_HistoryList.Clear();
			for (int k = 0; k < list.Count; k++)
			{
				PresentScrollItemData item2 = this.DataToUIData(list[k], true);
				this.m_HistoryList.Add(item2);
			}
			this.ExecuteFilter();
			this.SetMode(this.m_Mode);
		}

		// Token: 0x06003694 RID: 13972 RVA: 0x00113704 File Offset: 0x00111B04
		private void SetMode(PresentWindow.eMode mode)
		{
			this.m_Mode = mode;
			if (mode != PresentWindow.eMode.Normal)
			{
				if (mode == PresentWindow.eMode.History)
				{
					this.m_NormalObj.SetActive(false);
					this.m_HistoryObj.SetActive(true);
					this.m_Scroll.RemoveAll();
					if (this.m_HistoryList != null)
					{
						for (int i = 0; i < this.m_HistoryList.Count; i++)
						{
							this.m_HistoryList[i].m_OnClick = new Action<long>(this.OnClickExecuteGetCallBack);
							this.m_Scroll.AddItem(this.m_HistoryList[i]);
						}
					}
				}
			}
			else
			{
				this.m_NormalObj.SetActive(true);
				this.m_HistoryObj.SetActive(false);
				this.m_Scroll.RemoveAll();
				if (this.m_FilteredDataList != null)
				{
					for (int j = 0; j < this.m_FilteredDataList.Count; j++)
					{
						this.m_FilteredDataList[j].m_OnClick = new Action<long>(this.OnClickExecuteGetCallBack);
						this.m_Scroll.AddItem(this.m_FilteredDataList[j]);
					}
				}
			}
		}

		// Token: 0x06003695 RID: 13973 RVA: 0x00113830 File Offset: 0x00111C30
		private void OnClickExecuteGetCallBack(long mngID)
		{
			this.m_ReadyGetMngIDList.Clear();
			this.m_ReadyGetMngID = mngID;
			if (this.CheckHaveChara(mngID) != -1)
			{
				this.ConfirmCharaConv();
			}
			else
			{
				this.OnClickExecuteGet.Call(mngID);
			}
		}

		// Token: 0x06003696 RID: 13974 RVA: 0x00113868 File Offset: 0x00111C68
		public void OnClickGetMultiButtonCallBack()
		{
			this.m_ReadyGetMngIDList.Clear();
			this.m_ReadyGetMngID = -1L;
			bool flag = false;
			for (int i = 0; i < this.m_FilteredDataList.Count; i++)
			{
				this.m_ReadyGetMngIDList.Add(this.m_FilteredDataList[i].m_PresentMngID);
				if (this.CheckHaveChara(this.m_FilteredDataList[i].m_PresentMngID) != -1)
				{
					flag = true;
				}
				if (this.m_ReadyGetMngIDList.Count >= 99)
				{
					break;
				}
			}
			if (flag)
			{
				this.ConfirmCharaConv();
			}
			else
			{
				this.CallGetMultiButton(this.m_ReadyGetMngIDList);
			}
		}

		// Token: 0x06003697 RID: 13975 RVA: 0x00113918 File Offset: 0x00111D18
		public int CheckHaveChara(long presentMngID)
		{
			PresentManager.PresentData presentData = SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetPresentData(presentMngID);
			if (presentData != null && presentData.m_Type == ePresentType.Chara)
			{
				for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas.Count; i++)
				{
					int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[i].Param.CharaID;
					if (charaID == presentData.m_ID)
					{
						return charaID;
					}
				}
			}
			return -1;
		}

		// Token: 0x06003698 RID: 13976 RVA: 0x0011399C File Offset: 0x00111D9C
		public void ConfirmCharaConv()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "プレゼントの受け取り", SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PresentAltConfirm), null, new Action<int>(this.OnConfirmCharaConv));
		}

		// Token: 0x06003699 RID: 13977 RVA: 0x001139D4 File Offset: 0x00111DD4
		private void OnConfirmCharaConv(int btn)
		{
			if (btn == 0)
			{
				if (this.m_ReadyGetMngID != -1L)
				{
					this.OnClickExecuteGet.Call(this.m_ReadyGetMngID);
				}
				else if (this.m_ReadyGetMngIDList.Count > 0)
				{
					this.CallGetMultiButton(this.m_ReadyGetMngIDList);
				}
			}
			else if (this.m_ReadyGetMngIDList.Count > 0)
			{
				List<long> list = new List<long>();
				for (int i = 0; i < this.m_ReadyGetMngIDList.Count; i++)
				{
					if (this.CheckHaveChara(this.m_ReadyGetMngIDList[i]) == -1)
					{
						list.Add(this.m_ReadyGetMngIDList[i]);
					}
				}
				this.m_ReadyGetMngIDList = list;
				this.CallGetMultiButton(this.m_ReadyGetMngIDList);
			}
		}

		// Token: 0x0600369A RID: 13978 RVA: 0x00113A9C File Offset: 0x00111E9C
		private void CallGetMultiButton(List<long> presentMngIDs)
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				inst.TutorialMessage.SetEnableDark(true, 0.1f, false);
			}
			this.OnClickGetMultiButton.Call(presentMngIDs);
		}

		// Token: 0x0600369B RID: 13979 RVA: 0x00113AE0 File Offset: 0x00111EE0
		public void OpenResult(List<PresentManager.PresentData> list)
		{
			this.m_ResultGroup.Open(list);
		}

		// Token: 0x0600369C RID: 13980 RVA: 0x00113AEE File Offset: 0x00111EEE
		public void OnClickHistoryButtonCallBack()
		{
			this.m_ReserveOpen = true;
			this.m_Mode = PresentWindow.eMode.History;
			this.Close();
		}

		// Token: 0x0600369D RID: 13981 RVA: 0x00113B04 File Offset: 0x00111F04
		public void OnClickReturnButtonCallBack()
		{
			this.m_ReserveOpen = true;
			this.m_Mode = PresentWindow.eMode.Normal;
			this.Close();
		}

		// Token: 0x0600369E RID: 13982 RVA: 0x00113B1A File Offset: 0x00111F1A
		public void OpenFilterWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.Present, new Action(this.ExecuteFilter)), null, null);
		}

		// Token: 0x0600369F RID: 13983 RVA: 0x00113B40 File Offset: 0x00111F40
		public void ExecuteFilter()
		{
			this.m_FilteredDataList = new List<PresentScrollItemData>();
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.SignupComplete_NextIsPresentGet)
			{
				for (int i = 0; i < this.m_DataList.Count; i++)
				{
					this.m_FilteredDataList.Add(this.m_DataList[i]);
				}
				return;
			}
			for (int j = 0; j < this.m_DataList.Count; j++)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.Present, UISettings.eFilterCategory.DeadLine, 0) || !this.m_DataList[j].m_IsExistDeadline)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.Present, UISettings.eFilterCategory.DeadLine, 1) || this.m_DataList[j].m_IsExistDeadline)
					{
						UISettings.eFilterFlag_PresentType bitIndex;
						switch (this.m_DataList[j].m_Type)
						{
						case ePresentType.Chara:
							bitIndex = UISettings.eFilterFlag_PresentType.Chara;
							break;
						case ePresentType.Item:
							bitIndex = UISettings.eFilterFlag_PresentType.Item;
							break;
						case ePresentType.Weapon:
							bitIndex = UISettings.eFilterFlag_PresentType.Weapon;
							break;
						case ePresentType.TownFacility:
						case ePresentType.RoomObject:
						case ePresentType.MasterOrb:
						case ePresentType.KRRPoint:
							goto IL_127;
						case ePresentType.Gold:
							bitIndex = UISettings.eFilterFlag_PresentType.Gold;
							break;
						default:
							goto IL_127;
						}
						IL_12F:
						if (!SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.Present, UISettings.eFilterCategory.PresentType, (int)bitIndex))
						{
							goto IL_163;
						}
						this.m_FilteredDataList.Add(this.m_DataList[j]);
						goto IL_163;
						IL_127:
						bitIndex = UISettings.eFilterFlag_PresentType.Etc;
						goto IL_12F;
					}
				}
				IL_163:;
			}
			this.m_GetMultiButton.Interactable = (this.m_FilteredDataList.Count > 0);
			this.SetMode(this.m_Mode);
		}

		// Token: 0x04003CEC RID: 15596
		private const int MULTIMAX = 99;

		// Token: 0x04003CED RID: 15597
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003CEE RID: 15598
		[SerializeField]
		private GameObject m_NormalObj;

		// Token: 0x04003CEF RID: 15599
		[SerializeField]
		private GameObject m_HistoryObj;

		// Token: 0x04003CF0 RID: 15600
		[SerializeField]
		private PresentResultGroup m_ResultGroup;

		// Token: 0x04003CF1 RID: 15601
		[SerializeField]
		private CustomButton m_GetMultiButton;

		// Token: 0x04003CF2 RID: 15602
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04003CF3 RID: 15603
		private PresentWindow.eMode m_Mode;

		// Token: 0x04003CF4 RID: 15604
		private bool m_ReserveOpen;

		// Token: 0x04003CF5 RID: 15605
		private List<PresentScrollItemData> m_DataList = new List<PresentScrollItemData>();

		// Token: 0x04003CF6 RID: 15606
		private List<PresentScrollItemData> m_FilteredDataList = new List<PresentScrollItemData>();

		// Token: 0x04003CF7 RID: 15607
		private List<PresentScrollItemData> m_HistoryList = new List<PresentScrollItemData>();

		// Token: 0x04003CF8 RID: 15608
		private long m_ReadyGetMngID = -1L;

		// Token: 0x04003CF9 RID: 15609
		private List<long> m_ReadyGetMngIDList = new List<long>();

		// Token: 0x02000A43 RID: 2627
		public enum eMode
		{
			// Token: 0x04003CFE RID: 15614
			Normal,
			// Token: 0x04003CFF RID: 15615
			History
		}
	}
}
