﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Present
{
	// Token: 0x02000A3C RID: 2620
	public class PresentResultGroup : UIGroup
	{
		// Token: 0x06003674 RID: 13940 RVA: 0x00112AAE File Offset: 0x00110EAE
		public void Open(List<PresentManager.PresentData> list)
		{
			base.Open();
			this.m_ResultText.text = this.GetResultString(list);
		}

		// Token: 0x06003675 RID: 13941 RVA: 0x00112AC8 File Offset: 0x00110EC8
		private string GetResultString(List<PresentManager.PresentData> list)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < list.Count; i++)
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.GetPresentText(list[i]));
				stringBuilder.Append("\n");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04003CC7 RID: 15559
		[SerializeField]
		private Text m_ResultText;
	}
}
