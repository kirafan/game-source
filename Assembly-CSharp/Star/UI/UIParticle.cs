﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008B9 RID: 2233
	public class UIParticle : MonoBehaviour
	{
		// Token: 0x06002E46 RID: 11846 RVA: 0x000F3B32 File Offset: 0x000F1F32
		private void OnDestroy()
		{
			this.m_ImageDefaultSizeInformation.m_Value = null;
			this.m_ImageDefaultSizeInformation.m_Sprite = null;
			this.m_ImageDefaultSizeInformation = null;
			this.m_MoveX = null;
			this.m_MoveY = null;
			this.m_Image.sprite = null;
		}

		// Token: 0x06002E47 RID: 11847 RVA: 0x000F3B6D File Offset: 0x000F1F6D
		public void SetImageDefaultSizeInformation(UIParticle.ImageAxisDefaultSizeInformation imageDefaultSizeInformation)
		{
			this.m_ImageDefaultSizeInformation = imageDefaultSizeInformation;
		}

		// Token: 0x06002E48 RID: 11848 RVA: 0x000F3B78 File Offset: 0x000F1F78
		public void SetImageDefaultSizeInformationValue(UIParticle.ImageAxisDefaultSizeInformationValue imageDefaultSizeInformationValue)
		{
			UIParticle.ImageAxisDefaultSizeInformation imageAxisDefaultSizeInformation = this.m_ImageDefaultSizeInformation;
			if (imageAxisDefaultSizeInformation == null)
			{
				imageAxisDefaultSizeInformation = new UIParticle.ImageAxisDefaultSizeInformation();
			}
			imageAxisDefaultSizeInformation.m_Value = imageDefaultSizeInformationValue;
			this.SetImageDefaultSizeInformation(imageAxisDefaultSizeInformation);
		}

		// Token: 0x06002E49 RID: 11849 RVA: 0x000F3BA8 File Offset: 0x000F1FA8
		public void SetSprite(Sprite sprite)
		{
			UIParticle.ImageAxisDefaultSizeInformation imageAxisDefaultSizeInformation = this.m_ImageDefaultSizeInformation;
			if (imageAxisDefaultSizeInformation == null)
			{
				imageAxisDefaultSizeInformation = new UIParticle.ImageAxisDefaultSizeInformation();
			}
			imageAxisDefaultSizeInformation.m_Sprite = sprite;
			this.SetImageDefaultSizeInformation(imageAxisDefaultSizeInformation);
		}

		// Token: 0x06002E4A RID: 11850 RVA: 0x000F3BD8 File Offset: 0x000F1FD8
		public void SetDefaultImageSizeType(UIParticle.eImageAxisDefaultSizeType defaultSizeType)
		{
			UIParticle.ImageAxisDefaultSizeInformationValue imageAxisDefaultSizeInformationValue = null;
			if (this.m_ImageDefaultSizeInformation != null && this.m_ImageDefaultSizeInformation.m_Value != null)
			{
				imageAxisDefaultSizeInformationValue = this.m_ImageDefaultSizeInformation.m_Value;
			}
			if (imageAxisDefaultSizeInformationValue == null)
			{
				imageAxisDefaultSizeInformationValue = new UIParticle.ImageAxisDefaultSizeInformationValue();
			}
			imageAxisDefaultSizeInformationValue.m_ImageDefaultSizeType = defaultSizeType;
			this.SetImageDefaultSizeInformationValue(imageAxisDefaultSizeInformationValue);
		}

		// Token: 0x06002E4B RID: 11851 RVA: 0x000F3C28 File Offset: 0x000F2028
		public void SetDefaultImageSize(float x, float y)
		{
			UIParticle.ImageAxisDefaultSizeInformationValue imageAxisDefaultSizeInformationValue = null;
			if (this.m_ImageDefaultSizeInformation != null && this.m_ImageDefaultSizeInformation.m_Value != null)
			{
				imageAxisDefaultSizeInformationValue = this.m_ImageDefaultSizeInformation.m_Value;
			}
			if (imageAxisDefaultSizeInformationValue == null)
			{
				imageAxisDefaultSizeInformationValue = new UIParticle.ImageAxisDefaultSizeInformationValue();
			}
			imageAxisDefaultSizeInformationValue.m_X = x;
			imageAxisDefaultSizeInformationValue.m_Y = y;
			this.SetImageDefaultSizeInformationValue(imageAxisDefaultSizeInformationValue);
		}

		// Token: 0x06002E4C RID: 11852 RVA: 0x000F3C7F File Offset: 0x000F207F
		public void SetActionTime(float actionTime)
		{
			this.m_ActionTime = actionTime;
		}

		// Token: 0x06002E4D RID: 11853 RVA: 0x000F3C88 File Offset: 0x000F2088
		public void SetMoveX(float move)
		{
			this.m_MoveX.m_Move = move;
		}

		// Token: 0x06002E4E RID: 11854 RVA: 0x000F3C96 File Offset: 0x000F2096
		public void SetMoveY(float move)
		{
			this.m_MoveY.m_Move = move;
		}

		// Token: 0x06002E4F RID: 11855 RVA: 0x000F3CA4 File Offset: 0x000F20A4
		public void SetMoveEaseTypeX(iTween.EaseType easeType)
		{
			this.m_MoveX.m_EaseType = easeType;
		}

		// Token: 0x06002E50 RID: 11856 RVA: 0x000F3CB2 File Offset: 0x000F20B2
		public void SetMoveEaseTypeY(iTween.EaseType easeType)
		{
			this.m_MoveY.m_EaseType = easeType;
		}

		// Token: 0x06002E51 RID: 11857 RVA: 0x000F3CC0 File Offset: 0x000F20C0
		public void SetMoveLoopTypeX(iTween.LoopType loopType)
		{
			this.m_MoveX.m_LoopType = loopType;
		}

		// Token: 0x06002E52 RID: 11858 RVA: 0x000F3CCE File Offset: 0x000F20CE
		public void SetMoveLoopTypeY(iTween.LoopType loopType)
		{
			this.m_MoveY.m_LoopType = loopType;
		}

		// Token: 0x06002E53 RID: 11859 RVA: 0x000F3CDC File Offset: 0x000F20DC
		public void Play()
		{
			this.ApplyBeforePlay();
			this.m_MoveX.m_Tween.Play(new iTweenWrapperPlayArgument(this.m_MoveX.m_Tween.gameObject)
			{
				isLocal = true,
				time = this.m_ActionTime,
				easeType = this.m_MoveX.m_EaseType,
				loopType = this.m_MoveX.m_LoopType,
				oncompletecallback = delegate(object obj)
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}.AddXParameter(this.m_MoveX.m_Move));
			iTweenWrapper tween = this.m_MoveY.m_Tween;
			iTweenWrapperPlayArgument iTweenWrapperPlayArgument = new iTweenWrapperPlayArgument(this.m_MoveY.m_Tween.gameObject);
			iTweenWrapperPlayArgument.isLocal = true;
			iTweenWrapperPlayArgument.time = this.m_ActionTime;
			iTweenWrapperPlayArgument.easeType = this.m_MoveY.m_EaseType;
			iTweenWrapperPlayArgument.loopType = this.m_MoveY.m_LoopType;
			iTweenWrapperPlayArgument.oncompletecallback = delegate(object obj)
			{
			};
			tween.Play(iTweenWrapperPlayArgument.AddYParameter(this.m_MoveY.m_Move));
		}

		// Token: 0x06002E54 RID: 11860 RVA: 0x000F3DFC File Offset: 0x000F21FC
		private void ApplyBeforePlay()
		{
			if (this.m_ImageDefaultSizeInformation == null)
			{
				return;
			}
			if (this.m_Image == null)
			{
				return;
			}
			this.m_Image.sprite = this.m_ImageDefaultSizeInformation.m_Sprite;
			UIParticle.ImageAxisDefaultSizeInformationValue value = this.m_ImageDefaultSizeInformation.m_Value;
			if (value == null)
			{
				return;
			}
			switch (value.m_ImageDefaultSizeType)
			{
			case UIParticle.eImageAxisDefaultSizeType.SpriteSize:
				this.SetSizeFitterFitMode(eAxis.Vertical, ContentSizeFitter.FitMode.PreferredSize);
				this.SetSizeFitterFitMode(eAxis.Horizontal, ContentSizeFitter.FitMode.PreferredSize);
				break;
			case UIParticle.eImageAxisDefaultSizeType.Constant:
				this.SetImageAxisDefaultSizeConstant(eAxis.Vertical, value.m_Y);
				this.SetImageAxisDefaultSizeConstant(eAxis.Horizontal, value.m_X);
				break;
			case UIParticle.eImageAxisDefaultSizeType.ParentSize:
				if (!(this.m_ImageDefaultSizeInformation.m_ParentRectTransform == null))
				{
					this.SetImageAxisDefaultSizeConstant(eAxis.Vertical, this.m_ImageDefaultSizeInformation.m_ParentRectTransform.rect.height);
					this.SetImageAxisDefaultSizeConstant(eAxis.Horizontal, this.m_ImageDefaultSizeInformation.m_ParentRectTransform.rect.width);
				}
				break;
			}
		}

		// Token: 0x06002E55 RID: 11861 RVA: 0x000F3F09 File Offset: 0x000F2309
		private void SetImageAxisDefaultSizeConstant(eAxis axis, float value)
		{
			this.SetSizeFitterFitMode(axis, ContentSizeFitter.FitMode.Unconstrained);
			this.SetImageSize(axis, value);
		}

		// Token: 0x06002E56 RID: 11862 RVA: 0x000F3F1B File Offset: 0x000F231B
		private void SetSizeFitterActive(bool active)
		{
			if (this.m_SizeFitter == null)
			{
				return;
			}
			this.m_SizeFitter.enabled = active;
		}

		// Token: 0x06002E57 RID: 11863 RVA: 0x000F3F3C File Offset: 0x000F233C
		private void SetSizeFitterFitMode(eAxis axis, ContentSizeFitter.FitMode fitMode)
		{
			if (this.m_SizeFitter == null)
			{
				return;
			}
			if (axis != eAxis.Error)
			{
				if (axis != eAxis.Vertical)
				{
					if (axis == eAxis.Horizontal)
					{
						this.m_SizeFitter.horizontalFit = fitMode;
					}
				}
				else
				{
					this.m_SizeFitter.verticalFit = fitMode;
				}
			}
		}

		// Token: 0x06002E58 RID: 11864 RVA: 0x000F3F9C File Offset: 0x000F239C
		private void SetImageSize(eAxis axis, float value)
		{
			if (this.m_Image == null)
			{
				return;
			}
			if (this.m_Image.rectTransform == null)
			{
				return;
			}
			if (axis != eAxis.Error)
			{
				if (axis != eAxis.Vertical)
				{
					if (axis == eAxis.Horizontal)
					{
						this.m_Image.rectTransform.sizeDelta = new Vector2(value, this.m_Image.rectTransform.sizeDelta.y);
					}
				}
				else
				{
					this.m_Image.rectTransform.sizeDelta = new Vector2(this.m_Image.rectTransform.sizeDelta.x, value);
				}
			}
		}

		// Token: 0x04003573 RID: 13683
		[SerializeField]
		[Tooltip("表示画像.")]
		private Image m_Image;

		// Token: 0x04003574 RID: 13684
		[SerializeField]
		[Tooltip("自動レイアウトコンポーネント")]
		private ContentSizeFitter m_SizeFitter;

		// Token: 0x04003575 RID: 13685
		[SerializeField]
		private float m_ActionTime;

		// Token: 0x04003576 RID: 13686
		[SerializeField]
		private UIParticle.UIParticleActionInformation m_MoveX;

		// Token: 0x04003577 RID: 13687
		[SerializeField]
		private UIParticle.UIParticleActionInformation m_MoveY;

		// Token: 0x04003578 RID: 13688
		[SerializeField]
		private UIParticle.ImageAxisDefaultSizeInformation m_ImageDefaultSizeInformation;

		// Token: 0x020008BA RID: 2234
		[Serializable]
		public enum eImageAxisDefaultSizeType
		{
			// Token: 0x0400357B RID: 13691
			Error,
			// Token: 0x0400357C RID: 13692
			SpriteSize,
			// Token: 0x0400357D RID: 13693
			Constant,
			// Token: 0x0400357E RID: 13694
			ParentSize
		}

		// Token: 0x020008BB RID: 2235
		[Serializable]
		public class ImageAxisDefaultSizeInformation
		{
			// Token: 0x0400357F RID: 13695
			[SerializeField]
			public RectTransform m_ParentRectTransform;

			// Token: 0x04003580 RID: 13696
			[SerializeField]
			public Sprite m_Sprite;

			// Token: 0x04003581 RID: 13697
			[SerializeField]
			public UIParticle.ImageAxisDefaultSizeInformationValue m_Value;
		}

		// Token: 0x020008BC RID: 2236
		[Serializable]
		public class ImageAxisDefaultSizeInformationValue
		{
			// Token: 0x04003582 RID: 13698
			[SerializeField]
			public UIParticle.eImageAxisDefaultSizeType m_ImageDefaultSizeType = UIParticle.eImageAxisDefaultSizeType.SpriteSize;

			// Token: 0x04003583 RID: 13699
			[SerializeField]
			public float m_X;

			// Token: 0x04003584 RID: 13700
			[SerializeField]
			public float m_Y;
		}

		// Token: 0x020008BD RID: 2237
		[Serializable]
		public class UIParticleActionInformation
		{
			// Token: 0x04003585 RID: 13701
			[SerializeField]
			public iTweenMoveToWrapper m_Tween;

			// Token: 0x04003586 RID: 13702
			[SerializeField]
			public float m_Move;

			// Token: 0x04003587 RID: 13703
			[SerializeField]
			public iTween.EaseType m_EaseType = iTween.EaseType.linear;

			// Token: 0x04003588 RID: 13704
			[SerializeField]
			public iTween.LoopType m_LoopType;
		}
	}
}
