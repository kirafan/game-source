﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008CB RID: 2251
	public class iTweenLocalMoveAddWrapper : iTweenMoveToWrapper
	{
		// Token: 0x06002EAE RID: 11950 RVA: 0x000F4D10 File Offset: 0x000F3110
		public override void Play(iTweenWrapperPlayArgument argument)
		{
			if (argument != null)
			{
				if (argument.GetParameter(0) != null)
				{
					iTweenParameter parameter = argument.GetParameter("x");
					iTweenParameter parameter2 = argument.GetParameter("y");
					iTweenParameter parameter3 = argument.GetParameter("z");
					if (parameter == null)
					{
						parameter = argument.AddXParameter(0f).GetParameter("x");
					}
					if (parameter2 == null)
					{
						parameter2 = argument.AddYParameter(0f).GetParameter("y");
					}
					if (parameter3 == null)
					{
						parameter3 = argument.AddZParameter(0f).GetParameter("z");
					}
					Vector3 a = new Vector3((parameter == null) ? 0f : parameter.value, (parameter2 == null) ? 0f : parameter2.value, (parameter3 == null) ? 0f : parameter3.value);
					RectTransform component = argument.target.GetComponent<RectTransform>();
					Vector3 b = component.anchoredPosition;
					Vector3 a2 = a + b;
					RectTransform component2 = component.parent.GetComponent<RectTransform>();
					Vector3 vector = a2 + new Vector3(((component.anchorMin.x + component.anchorMax.x) * 0.5f - component2.pivot.x) * component2.rect.width, ((component.anchorMin.y + component.anchorMax.y) * 0.5f - component2.pivot.y) * component2.rect.height);
					parameter.value = vector.x;
					parameter2.value = vector.y;
					parameter3.value = vector.z;
				}
				base.Play(argument);
			}
		}
	}
}
