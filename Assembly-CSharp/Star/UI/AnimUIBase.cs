﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007EF RID: 2031
	public abstract class AnimUIBase : MonoBehaviour
	{
		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x060029F2 RID: 10738 RVA: 0x000DD1BD File Offset: 0x000DB5BD
		public bool IsPlaying
		{
			get
			{
				return this.m_IsPlaying;
			}
		}

		// Token: 0x060029F3 RID: 10739 RVA: 0x000DD1C5 File Offset: 0x000DB5C5
		public void PlayIn()
		{
			this.m_ReservePlay = AnimUIBase.ePlayMode.In;
			this.m_IsPlaying = true;
		}

		// Token: 0x060029F4 RID: 10740 RVA: 0x000DD1D5 File Offset: 0x000DB5D5
		public void PlayOut()
		{
			this.m_ReservePlay = AnimUIBase.ePlayMode.Out;
			this.m_IsPlaying = true;
		}

		// Token: 0x060029F5 RID: 10741 RVA: 0x000DD1E5 File Offset: 0x000DB5E5
		public virtual void Show()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			this.m_ReservePlay = AnimUIBase.ePlayMode.None;
			this.m_IsPlaying = false;
		}

		// Token: 0x060029F6 RID: 10742 RVA: 0x000DD206 File Offset: 0x000DB606
		public virtual void Hide()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			this.m_ReservePlay = AnimUIBase.ePlayMode.None;
			this.m_IsPlaying = false;
		}

		// Token: 0x060029F7 RID: 10743 RVA: 0x000DD227 File Offset: 0x000DB627
		protected virtual void Prepare()
		{
			this.m_IsDonePrepare = true;
		}

		// Token: 0x060029F8 RID: 10744 RVA: 0x000DD230 File Offset: 0x000DB630
		public virtual void Start()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
		}

		// Token: 0x060029F9 RID: 10745 RVA: 0x000DD244 File Offset: 0x000DB644
		protected virtual void Update()
		{
			AnimUIBase.ePlayMode reservePlay = this.m_ReservePlay;
			if (reservePlay != AnimUIBase.ePlayMode.In)
			{
				if (reservePlay == AnimUIBase.ePlayMode.Out)
				{
					this.m_ReservePlay = AnimUIBase.ePlayMode.None;
					this.ExecutePlayOut();
				}
			}
			else
			{
				this.m_ReservePlay = AnimUIBase.ePlayMode.None;
				this.ExecutePlayIn();
			}
		}

		// Token: 0x060029FA RID: 10746
		protected abstract void ExecutePlayIn();

		// Token: 0x060029FB RID: 10747
		protected abstract void ExecutePlayOut();

		// Token: 0x04003053 RID: 12371
		protected bool m_IsPlaying;

		// Token: 0x04003054 RID: 12372
		protected bool m_IsDonePrepare;

		// Token: 0x04003055 RID: 12373
		protected AnimUIBase.ePlayMode m_ReservePlay;

		// Token: 0x04003056 RID: 12374
		public Action OnAnimCompleteIn;

		// Token: 0x04003057 RID: 12375
		public Action OnAnimCompleteOut;

		// Token: 0x020007F0 RID: 2032
		protected enum ePlayMode
		{
			// Token: 0x04003059 RID: 12377
			None,
			// Token: 0x0400305A RID: 12378
			In,
			// Token: 0x0400305B RID: 12379
			Out
		}
	}
}
