﻿using System;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000984 RID: 2436
	public class GachaMessageData : CharacterDataWrapperNoExist
	{
		// Token: 0x06003259 RID: 12889 RVA: 0x00100A94 File Offset: 0x000FEE94
		public GachaMessageData(string in_message) : base(eCharacterDataType.GachaDetailMessage)
		{
			this.message = in_message;
		}

		// Token: 0x0600325A RID: 12890 RVA: 0x00100AA5 File Offset: 0x000FEEA5
		public string GetMessage()
		{
			return this.message;
		}

		// Token: 0x04003861 RID: 14433
		private string message;
	}
}
