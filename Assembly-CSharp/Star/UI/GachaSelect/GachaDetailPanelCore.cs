﻿using System;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000986 RID: 2438
	public class GachaDetailPanelCore : EnumerationPanelScrollItemIconCoreGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x0600325D RID: 12893 RVA: 0x00100ABD File Offset: 0x000FEEBD
		public override void Setup(GachaDetailPanelCore.SharedInstanceExOverride argument)
		{
			base.Setup(argument);
			this.m_SharedIntance.m_ScrollViewBase.Setup();
		}

		// Token: 0x0600325E RID: 12894 RVA: 0x00100AD6 File Offset: 0x000FEED6
		public override void Destroy()
		{
			base.Destroy();
			this.m_SharedIntance.m_ScrollViewBase.Destroy();
		}

		// Token: 0x0600325F RID: 12895 RVA: 0x00100AEE File Offset: 0x000FEEEE
		protected override void SetPartyData(GachaDetailPanelData data)
		{
			this.m_SharedIntance.m_ScrollViewBase.RemoveAll();
			base.SetPartyData(data);
		}

		// Token: 0x06003260 RID: 12896 RVA: 0x00100B08 File Offset: 0x000FEF08
		protected override void ApplyCharaData(int idx)
		{
			this.m_SharedIntance.m_ScrollViewBase.AddItem(new GachaDetailScrollItemData
			{
				parent = this.m_SharedIntance.parent,
				partyMemberController = this.m_PartyData.m_Party.GetMember(idx)
			});
		}

		// Token: 0x02000987 RID: 2439
		[Serializable]
		public class SharedInstanceExOverride : EnumerationPanelScrollItemIconCoreGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore.SharedInstanceExOverride>.SharedInstanceEx<GachaDetailPanelCore.SharedInstanceExOverride>
		{
			// Token: 0x06003261 RID: 12897 RVA: 0x00100B54 File Offset: 0x000FEF54
			public SharedInstanceExOverride()
			{
			}

			// Token: 0x06003262 RID: 12898 RVA: 0x00100B5C File Offset: 0x000FEF5C
			public SharedInstanceExOverride(GachaDetailPanelCore.SharedInstanceExOverride argument) : base(argument)
			{
			}

			// Token: 0x06003263 RID: 12899 RVA: 0x00100B65 File Offset: 0x000FEF65
			public override GachaDetailPanelCore.SharedInstanceExOverride Clone(GachaDetailPanelCore.SharedInstanceExOverride argument)
			{
				return base.Clone(argument).CloneNewMemberOnly(argument);
			}

			// Token: 0x06003264 RID: 12900 RVA: 0x00100B74 File Offset: 0x000FEF74
			private GachaDetailPanelCore.SharedInstanceExOverride CloneNewMemberOnly(GachaDetailPanelCore.SharedInstanceExOverride argument)
			{
				this.m_ScrollViewBase = argument.m_ScrollViewBase;
				return this;
			}

			// Token: 0x04003862 RID: 14434
			[SerializeField]
			public ScrollViewBase m_ScrollViewBase;
		}
	}
}
