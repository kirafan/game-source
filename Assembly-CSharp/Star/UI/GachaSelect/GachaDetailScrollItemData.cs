﻿using System;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200097D RID: 2429
	public class GachaDetailScrollItemData : ScrollItemData
	{
		// Token: 0x04003852 RID: 14418
		public GachaDetailPanel parent;

		// Token: 0x04003853 RID: 14419
		public EquipWeaponPartyMemberController partyMemberController;
	}
}
