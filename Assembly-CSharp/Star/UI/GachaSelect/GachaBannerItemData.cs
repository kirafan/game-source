﻿using System;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200099A RID: 2458
	public class GachaBannerItemData : InfiniteScrollItemData
	{
		// Token: 0x060032F0 RID: 13040 RVA: 0x00102A75 File Offset: 0x00100E75
		public GachaBannerItemData(string gachaBannerID, int currentStep = -1, int maxStep = -1, bool isFirstBanner = false)
		{
			this.m_GachaBannerID = gachaBannerID;
			this.m_Sprite = null;
			this.m_CurrentStep = currentStep;
			this.m_MaxStep = maxStep;
			this.m_IsFirstBanner = isFirstBanner;
		}

		// Token: 0x060032F1 RID: 13041 RVA: 0x00102AA1 File Offset: 0x00100EA1
		public void SetSprite(Sprite sprite)
		{
			this.m_Sprite = sprite;
		}

		// Token: 0x060032F2 RID: 13042 RVA: 0x00102AAA File Offset: 0x00100EAA
		public void Destroy()
		{
			this.m_Sprite = null;
		}

		// Token: 0x040038F5 RID: 14581
		public Sprite m_Sprite;

		// Token: 0x040038F6 RID: 14582
		public string m_GachaBannerID;

		// Token: 0x040038F7 RID: 14583
		public int m_CurrentStep;

		// Token: 0x040038F8 RID: 14584
		public int m_MaxStep;

		// Token: 0x040038F9 RID: 14585
		public bool m_IsFirstBanner;
	}
}
