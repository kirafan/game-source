﻿using System;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000988 RID: 2440
	public class GachaDetailPanel : EnumerationPanelScrollItemIconGen<GachaDetailPanel, GachaDetailCharaPanel, GachaDetailPanelData, GachaDetailPanelCore, GachaDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x06003266 RID: 12902 RVA: 0x00100B8B File Offset: 0x000FEF8B
		public override GachaDetailPanelCore CreateCore()
		{
			return new GachaDetailPanelCore();
		}

		// Token: 0x06003267 RID: 12903 RVA: 0x00100B94 File Offset: 0x000FEF94
		public override GachaDetailPanelCore.SharedInstanceExOverride CreateArgument()
		{
			if (this.m_CharaPanelPrefab != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelPrefab = this.m_CharaPanelPrefab;
			}
			if (this.m_CharaPanelParent != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelParent = this.m_CharaPanelParent;
			}
			if (this.m_ScrollViewBase != null)
			{
				this.m_SerializedConstructInstances.m_ScrollViewBase = this.m_ScrollViewBase;
			}
			return new GachaDetailPanelCore.SharedInstanceExOverride(this.m_SerializedConstructInstances)
			{
				parent = this
			};
		}

		// Token: 0x06003268 RID: 12904 RVA: 0x00100C1B File Offset: 0x000FF01B
		public override void Setup()
		{
			base.Setup();
		}

		// Token: 0x06003269 RID: 12905 RVA: 0x00100C23 File Offset: 0x000FF023
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x0600326A RID: 12906 RVA: 0x00100C2B File Offset: 0x000FF02B
		private void Update()
		{
		}

		// Token: 0x04003863 RID: 14435
		[SerializeField]
		private GachaDetailCharaPanel m_CharaPanelPrefabEx;

		// Token: 0x04003864 RID: 14436
		[SerializeField]
		private ScrollViewBase m_ScrollViewBase;
	}
}
