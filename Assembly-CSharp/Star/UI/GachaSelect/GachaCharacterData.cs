﻿using System;
using System.Collections.Generic;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000982 RID: 2434
	public class GachaCharacterData : CharacterDataWrapperBase
	{
		// Token: 0x06003240 RID: 12864 RVA: 0x001008E4 File Offset: 0x000FECE4
		public GachaCharacterData(int charaId) : base(eCharacterDataType.UserSupportData)
		{
			this.m_CharaId = charaId;
			this.cldbp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaId);
			this.usld = new SkillLearnData(this.cldbp.m_CharaSkillID, 1, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetMaxLv(this.cldbp.m_CharaSkillExpTableID), -1L, this.cldbp.m_CharaSkillExpTableID, true);
		}

		// Token: 0x06003241 RID: 12865 RVA: 0x00100963 File Offset: 0x000FED63
		public override long GetMngId()
		{
			return -1L;
		}

		// Token: 0x06003242 RID: 12866 RVA: 0x00100967 File Offset: 0x000FED67
		public override int GetCharaId()
		{
			return this.m_CharaId;
		}

		// Token: 0x06003243 RID: 12867 RVA: 0x0010096F File Offset: 0x000FED6F
		public override eCharaNamedType GetCharaNamedType()
		{
			return (eCharaNamedType)this.cldbp.m_NamedType;
		}

		// Token: 0x06003244 RID: 12868 RVA: 0x0010097C File Offset: 0x000FED7C
		public override eClassType GetClassType()
		{
			return (eClassType)this.cldbp.m_Class;
		}

		// Token: 0x06003245 RID: 12869 RVA: 0x00100989 File Offset: 0x000FED89
		public override eElementType GetElementType()
		{
			return (eElementType)this.cldbp.m_Element;
		}

		// Token: 0x06003246 RID: 12870 RVA: 0x00100996 File Offset: 0x000FED96
		public override eRare GetRarity()
		{
			return (eRare)this.cldbp.m_Rare;
		}

		// Token: 0x06003247 RID: 12871 RVA: 0x001009A3 File Offset: 0x000FEDA3
		public override int GetCost()
		{
			return 1;
		}

		// Token: 0x06003248 RID: 12872 RVA: 0x001009A6 File Offset: 0x000FEDA6
		public override int GetLv()
		{
			return 1;
		}

		// Token: 0x06003249 RID: 12873 RVA: 0x001009A9 File Offset: 0x000FEDA9
		public override int GetMaxLv()
		{
			return 1;
		}

		// Token: 0x0600324A RID: 12874 RVA: 0x001009AC File Offset: 0x000FEDAC
		public override long GetExp()
		{
			return 1L;
		}

		// Token: 0x0600324B RID: 12875 RVA: 0x001009B0 File Offset: 0x000FEDB0
		public override int GetLimitBreak()
		{
			return 1;
		}

		// Token: 0x0600324C RID: 12876 RVA: 0x001009B3 File Offset: 0x000FEDB3
		public override int GetFriendship()
		{
			return 1;
		}

		// Token: 0x0600324D RID: 12877 RVA: 0x001009B8 File Offset: 0x000FEDB8
		public override int GetFriendshipMax()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			return dbMng.NamedFriendshipExpDB.GetFriendShipMax(dbMng.NamedListDB.GetParam(this.GetCharaNamedType()).m_FriendshipTableID);
		}

		// Token: 0x0600324E RID: 12878 RVA: 0x001009F4 File Offset: 0x000FEDF4
		public override long GetFriendshipExp()
		{
			return 1L;
		}

		// Token: 0x0600324F RID: 12879 RVA: 0x001009F8 File Offset: 0x000FEDF8
		public override SkillLearnData GetUniqueSkillLearnData()
		{
			return this.usld;
		}

		// Token: 0x06003250 RID: 12880 RVA: 0x00100A00 File Offset: 0x000FEE00
		public override int GetUniqueSkillLv()
		{
			return 1;
		}

		// Token: 0x06003251 RID: 12881 RVA: 0x00100A03 File Offset: 0x000FEE03
		public override int GetUniqueSkillMaxLv()
		{
			base.Assert();
			return 1;
		}

		// Token: 0x06003252 RID: 12882 RVA: 0x00100A0C File Offset: 0x000FEE0C
		public override List<SkillLearnData> GetClassSkillLearnDatas()
		{
			return null;
		}

		// Token: 0x06003253 RID: 12883 RVA: 0x00100A0F File Offset: 0x000FEE0F
		public override int GetClassSkillsCount()
		{
			return 0;
		}

		// Token: 0x06003254 RID: 12884 RVA: 0x00100A14 File Offset: 0x000FEE14
		public override int[] GetClassSkillLevels()
		{
			List<SkillLearnData> classSkillLearnDatas = this.GetClassSkillLearnDatas();
			if (classSkillLearnDatas == null)
			{
				return null;
			}
			int[] array = new int[classSkillLearnDatas.Count];
			for (int i = 0; i < classSkillLearnDatas.Count; i++)
			{
				array[i] = base.GetSkillLevel(classSkillLearnDatas[i]);
			}
			return array;
		}

		// Token: 0x06003255 RID: 12885 RVA: 0x00100A64 File Offset: 0x000FEE64
		public override int GetClassSkillLevel(int index)
		{
			base.Assert();
			return 1;
		}

		// Token: 0x0400385B RID: 14427
		private int m_CharaId;

		// Token: 0x0400385C RID: 14428
		protected CharacterListDB_Param cldbp;

		// Token: 0x0400385D RID: 14429
		protected SkillLearnData usld;

		// Token: 0x0400385E RID: 14430
		protected List<SkillLearnData> cslds;
	}
}
