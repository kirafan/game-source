﻿using System;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000983 RID: 2435
	public class GachaTitleData : CharacterDataWrapperNoExist
	{
		// Token: 0x06003256 RID: 12886 RVA: 0x00100A6D File Offset: 0x000FEE6D
		public GachaTitleData(eRare in_rare, bool in_isPickUp) : base(eCharacterDataType.Lock)
		{
			this.rare = in_rare;
			this.isPickUp = in_isPickUp;
		}

		// Token: 0x06003257 RID: 12887 RVA: 0x00100A84 File Offset: 0x000FEE84
		public override eRare GetRarity()
		{
			return this.rare;
		}

		// Token: 0x06003258 RID: 12888 RVA: 0x00100A8C File Offset: 0x000FEE8C
		public bool IsPickUp()
		{
			return this.isPickUp;
		}

		// Token: 0x0400385F RID: 14431
		private eRare rare;

		// Token: 0x04003860 RID: 14432
		private bool isPickUp;
	}
}
