﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000981 RID: 2433
	public class GachaDetailGroup : UIGroup
	{
		// Token: 0x06003236 RID: 12854 RVA: 0x00100512 File Offset: 0x000FE912
		public void Setup()
		{
			if (this.m_GachaDetailPanel != null)
			{
				this.m_GachaDetailPanel.Setup();
			}
		}

		// Token: 0x06003237 RID: 12855 RVA: 0x00100530 File Offset: 0x000FE930
		public void Destroy()
		{
			if (this.m_GachaDetailPanel != null)
			{
				this.m_GachaDetailPanel.Destroy();
			}
		}

		// Token: 0x06003238 RID: 12856 RVA: 0x0010054E File Offset: 0x000FE94E
		public void SetGachaId(int gachaId)
		{
			this.SetGachaId(this.GetCharaIds(gachaId));
		}

		// Token: 0x06003239 RID: 12857 RVA: 0x0010055D File Offset: 0x000FE95D
		public void SetGachaId(List<int> charaIds)
		{
			this.SetEnumerationDatas(this.ConvertCharaIdsToGachaListDatas(charaIds, false));
		}

		// Token: 0x0600323A RID: 12858 RVA: 0x00100570 File Offset: 0x000FE970
		public void SetLineUpCharacterIds(List<int> pickupCharaIDList, List<int> normalCharaIDList)
		{
			List<CharacterDataWrapperBase> pickUpItems = this.ConvertCharaIdsToGachaListDatas(pickupCharaIDList, true);
			List<CharacterDataWrapperBase> normalItems = this.ConvertCharaIdsToGachaListDatas(normalCharaIDList, false);
			this.SetLineUpItems(pickUpItems, normalItems);
		}

		// Token: 0x0600323B RID: 12859 RVA: 0x0010059C File Offset: 0x000FE99C
		public void SetLineUpItems(List<CharacterDataWrapperBase> pickUpItems, List<CharacterDataWrapperBase> normalItems)
		{
			bool flag = pickUpItems != null && 0 < pickUpItems.Count;
			bool flag2 = normalItems != null && 0 < normalItems.Count;
			List<List<CharacterDataWrapperBase>> list = new List<List<CharacterDataWrapperBase>>();
			if (flag)
			{
				list.Add(pickUpItems);
			}
			if (flag2)
			{
				list.Add(normalItems);
			}
			this.SetLineUpItems(list);
		}

		// Token: 0x0600323C RID: 12860 RVA: 0x001005F8 File Offset: 0x000FE9F8
		public void SetLineUpItems(List<List<CharacterDataWrapperBase>> itemLists)
		{
			if (itemLists == null)
			{
				itemLists = new List<List<CharacterDataWrapperBase>>();
			}
			List<CharacterDataWrapperBase> list = new List<CharacterDataWrapperBase>();
			int num = 0;
			for (int i = 0; i < itemLists.Count; i++)
			{
				List<CharacterDataWrapperBase> list2 = itemLists[i];
				if (list2 != null)
				{
					if (0 < num)
					{
						list.Add(new GachaMessageData("・ピックアップ対象のキャラクターは同じレアリティの他のキャラクターに比べ、\n提供割合が高く設定されています。"));
						list.Add(new GachaMessageData("・提供割合に関しましては、ゲーム内の召喚画面\u3000提供割合ボタンからご確認ください。"));
					}
					list.AddRange(list2);
					num = list2.Count;
				}
			}
			this.SetEnumerationDatas(list);
		}

		// Token: 0x0600323D RID: 12861 RVA: 0x0010067C File Offset: 0x000FEA7C
		public void SetEnumerationDatas(List<CharacterDataWrapperBase> enumerationDatas)
		{
			if (enumerationDatas == null)
			{
				return;
			}
			EquipWeaponUnmanagedSupportPartyController party = new EquipWeaponUnmanagedSupportPartyController().Setup(0, enumerationDatas, "ガチャ排出リストさんチーム", enumerationDatas.Count, enumerationDatas.Count);
			if (this.m_GachaDetailPanel != null)
			{
				this.m_GachaDetailPanel.Apply(new GachaDetailPanelData
				{
					m_Party = party
				});
			}
		}

		// Token: 0x0600323E RID: 12862 RVA: 0x001006D8 File Offset: 0x000FEAD8
		public List<CharacterDataWrapperBase> ConvertCharaIdsToGachaListDatas(List<int> charaIds, bool isPickUp = false)
		{
			if (charaIds == null)
			{
				return null;
			}
			List<CharacterDataWrapperBase> list = new List<CharacterDataWrapperBase>();
			eRare eRare = eRare.None;
			for (int i = 0; i < charaIds.Count; i++)
			{
				if (-1 < charaIds[i])
				{
					GachaCharacterData gachaCharacterData = new GachaCharacterData(charaIds[i]);
					CharacterDataWrapperBase item = gachaCharacterData;
					if (eRare != gachaCharacterData.GetRarity())
					{
						eRare = gachaCharacterData.GetRarity();
						list.Add(new GachaTitleData(eRare, isPickUp));
					}
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x0600323F RID: 12863 RVA: 0x00100758 File Offset: 0x000FEB58
		private List<int> GetCharaIds(int gachaId)
		{
			return new List<int>
			{
				-1,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				-1,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				-1,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001,
				14001001,
				15001001
			};
		}

		// Token: 0x0400385A RID: 14426
		[SerializeField]
		[Tooltip("編成の名前空間なので問題があれば直す.")]
		protected GachaDetailPanel m_GachaDetailPanel;
	}
}
