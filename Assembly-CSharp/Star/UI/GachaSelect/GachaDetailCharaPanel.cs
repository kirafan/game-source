﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200097F RID: 2431
	public class GachaDetailCharaPanel : EnumerationCharaPanelScrollItemIconAdapter
	{
		// Token: 0x0600322C RID: 12844 RVA: 0x00100215 File Offset: 0x000FE615
		public override EnumerationCharaPanelScrollItemIcon CreateCore()
		{
			return new GachaDetailCharaPanelCore();
		}

		// Token: 0x0600322D RID: 12845 RVA: 0x0010021C File Offset: 0x000FE61C
		protected override void ApplyData()
		{
			base.ApplyData();
			GachaDetailScrollItemData dataEx = this.m_DataEx;
			this.m_DataEx = (GachaDetailScrollItemData)this.m_Data;
			if (dataEx == null)
			{
				this.Setup(this.m_DataEx.parent);
			}
			this.Apply(this.m_DataEx.partyMemberController);
			if (this.m_DataEx.partyMemberController.GetCharacterDataType() == eCharacterDataType.Lock)
			{
				this.SetActiveSafety(this.m_TitleRare, true);
				this.SetActiveSafety(this.m_TitleRareIcon, true);
				this.SetActiveSafety(this.m_TitleLine, true);
				this.SetActiveSafety(this.m_TitlePickUp, ((GachaTitleData)this.m_DataEx.partyMemberController.GetData()).IsPickUp());
				this.SetActiveSafety(this.m_Message, false);
				this.SetRare(this.m_DataEx.partyMemberController.GetRarity());
			}
			else
			{
				if (this.m_DataEx.partyMemberController.GetCharacterDataType() != eCharacterDataType.Empty)
				{
				}
				this.SetActiveSafety(this.m_TitleRare, false);
				this.SetActiveSafety(this.m_TitleRareIcon, false);
				this.SetActiveSafety(this.m_TitleLine, false);
				this.SetActiveSafety(this.m_TitlePickUp, false);
				if (this.m_DataEx.partyMemberController.GetCharacterDataType() == eCharacterDataType.GachaDetailMessage)
				{
					this.SetActiveSafety(this.m_Message, true);
					if (this.m_Message != null)
					{
						this.m_Message.text = ((GachaMessageData)this.m_DataEx.partyMemberController.GetData()).GetMessage();
					}
				}
				else
				{
					this.SetActiveSafety(this.m_Message, false);
				}
			}
		}

		// Token: 0x0600322E RID: 12846 RVA: 0x001003B0 File Offset: 0x000FE7B0
		private void SetRare(eRare rare)
		{
			if (this.m_TitleRare != null)
			{
				this.m_TitleRare.Apply(rare);
			}
			if (this.m_TitleRareIcon != null)
			{
				this.m_TitleRareIcon.Apply(rare);
			}
		}

		// Token: 0x0600322F RID: 12847 RVA: 0x001003EC File Offset: 0x000FE7EC
		private void SetActiveSafety(MonoBehaviour mb, bool active)
		{
			if (mb != null)
			{
				GameObject gameObject = mb.gameObject;
				if (gameObject != null)
				{
					this.SetActiveSafety(gameObject, active);
				}
			}
		}

		// Token: 0x06003230 RID: 12848 RVA: 0x00100420 File Offset: 0x000FE820
		private void SetActiveSafety(GameObject go, bool active)
		{
			if (go != null && go.activeSelf != active)
			{
				go.SetActive(active);
			}
		}

		// Token: 0x04003854 RID: 14420
		[SerializeField]
		private RareStar m_TitleRare;

		// Token: 0x04003855 RID: 14421
		[SerializeField]
		private RareIcon m_TitleRareIcon;

		// Token: 0x04003856 RID: 14422
		[SerializeField]
		private GameObject m_TitlePickUp;

		// Token: 0x04003857 RID: 14423
		[SerializeField]
		private GameObject m_TitleLine;

		// Token: 0x04003858 RID: 14424
		[SerializeField]
		private Text m_Message;

		// Token: 0x04003859 RID: 14425
		private GachaDetailScrollItemData m_DataEx;
	}
}
