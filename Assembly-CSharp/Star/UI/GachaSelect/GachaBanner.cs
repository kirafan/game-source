﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000999 RID: 2457
	public class GachaBanner : InfiniteScrollItem
	{
		// Token: 0x060032EC RID: 13036 RVA: 0x0010294A File Offset: 0x00100D4A
		public override void Setup()
		{
		}

		// Token: 0x060032ED RID: 13037 RVA: 0x0010294C File Offset: 0x00100D4C
		private void Update()
		{
			if (this.m_Data != null && this.m_Image.sprite == null)
			{
				GachaBannerItemData gachaBannerItemData = (GachaBannerItemData)this.m_Data;
				if (gachaBannerItemData.m_Sprite != null)
				{
					this.m_Image.sprite = gachaBannerItemData.m_Sprite;
				}
			}
		}

		// Token: 0x060032EE RID: 13038 RVA: 0x001029A8 File Offset: 0x00100DA8
		public override void Apply(InfiniteScrollItemData data)
		{
			base.Apply(data);
			this.m_Image.sprite = null;
			GachaBannerItemData gachaBannerItemData = (GachaBannerItemData)this.m_Data;
			if (gachaBannerItemData.m_Sprite != null)
			{
				this.m_Image.sprite = gachaBannerItemData.m_Sprite;
			}
			if (gachaBannerItemData.m_MaxStep > 0)
			{
				if (!this.m_StepObj.activeSelf)
				{
					this.m_StepObj.SetActive(true);
				}
				this.m_CurrentNum.SetValue(gachaBannerItemData.m_CurrentStep);
				this.m_MaxNum.SetValue(gachaBannerItemData.m_MaxStep);
			}
			else if (this.m_StepObj.activeSelf)
			{
				this.m_StepObj.SetActive(false);
			}
		}

		// Token: 0x060032EF RID: 13039 RVA: 0x00102A61 File Offset: 0x00100E61
		public override void Destroy()
		{
			base.Destroy();
			this.m_Image.sprite = null;
		}

		// Token: 0x040038F1 RID: 14577
		[SerializeField]
		private Image m_Image;

		// Token: 0x040038F2 RID: 14578
		[SerializeField]
		private GameObject m_StepObj;

		// Token: 0x040038F3 RID: 14579
		[SerializeField]
		private ImageNumbers m_CurrentNum;

		// Token: 0x040038F4 RID: 14580
		[SerializeField]
		private ImageNumbers m_MaxNum;
	}
}
