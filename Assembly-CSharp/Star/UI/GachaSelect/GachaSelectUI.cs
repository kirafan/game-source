﻿using System;
using System.Collections.Generic;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200099E RID: 2462
	public class GachaSelectUI : MenuUIBase
	{
		// Token: 0x1400008C RID: 140
		// (add) Token: 0x060032FC RID: 13052 RVA: 0x00102E1C File Offset: 0x0010121C
		// (remove) Token: 0x060032FD RID: 13053 RVA: 0x00102E54 File Offset: 0x00101254
		public event Action<int, GachaDefine.ePlayType> OnClickDecideButton;

		// Token: 0x1400008D RID: 141
		// (add) Token: 0x060032FE RID: 13054 RVA: 0x00102E8C File Offset: 0x0010128C
		// (remove) Token: 0x060032FF RID: 13055 RVA: 0x00102EC4 File Offset: 0x001012C4
		public event Action<int, GachaDefine.ePlayType> OnClickDecideStepButton;

		// Token: 0x06003300 RID: 13056 RVA: 0x00102EFA File Offset: 0x001012FA
		private void Start()
		{
		}

		// Token: 0x06003301 RID: 13057 RVA: 0x00102EFC File Offset: 0x001012FC
		public void Setup(Gacha gacha)
		{
			this.m_Gacha = gacha;
			this.m_GameObject = base.gameObject;
			this.m_GachaScroll.Setup();
			this.m_GachaDetailGroup.Setup();
			this.m_GachaDataList = new List<GachaSelectUI.UIGachaData>();
			this.m_GachaDataList.Clear();
			List<Gacha.GachaData> gachaList = gacha.GetGachaList();
			for (int i = 0; i < gachaList.Count; i++)
			{
				GachaSelectUI.UIGachaData uigachaData = new GachaSelectUI.UIGachaData(gachaList[i]);
				GachaBannerItemData gachaBannerItemData = new GachaBannerItemData(uigachaData.m_GachaBannerID, uigachaData.m_CurrentStep, uigachaData.m_MaxStep, uigachaData.m_IsFirstBanner);
				this.m_GachaBannerDataList.Add(gachaBannerItemData);
				this.m_GachaScroll.AddItem(gachaBannerItemData);
				this.m_GachaDataList.Add(uigachaData);
			}
			List<Gacha.StepGachaData> stepGachaList = gacha.GetStepGachaList();
			for (int j = 0; j < stepGachaList.Count; j++)
			{
				GachaSelectUI.UIGachaData uigachaData2 = new GachaSelectUI.UIGachaData(stepGachaList[j]);
				GachaBannerItemData gachaBannerItemData2 = new GachaBannerItemData(uigachaData2.m_GachaBannerID, uigachaData2.m_CurrentStep, uigachaData2.m_MaxStep, uigachaData2.m_IsFirstBanner);
				this.m_GachaBannerDataList.Add(gachaBannerItemData2);
				this.m_GachaScroll.AddItem(gachaBannerItemData2);
				this.m_GachaDataList.Add(uigachaData2);
			}
			this.m_DayButton.OnClickGachaButton += this.OnClickButtonCallback;
			this.m_SingleButton.OnClickGachaButton += this.OnClickButtonCallback;
			this.m_MultiButton.OnClickGachaButton += this.OnClickButtonCallback;
			if (this.m_GachaDataList.Count > 1)
			{
				this.m_GachaScroll.OnDragAction += this.OnScrollCallBack;
			}
			this.m_GachaDataAnim.Hide();
			this.m_TicketNumObj.SetActive(false);
			this.m_UnlimitedGemNumText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.UnlimitedGem, true);
			this.m_LimitedGemNumText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.LimitedGem, true);
			this.m_Loader = new ABResourceLoader(-1);
			this.LoadBanner();
		}

		// Token: 0x06003302 RID: 13058 RVA: 0x00103114 File Offset: 0x00101514
		public void Refresh()
		{
			this.m_UnlimitedGemNumText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.UnlimitedGem, true);
			this.m_LimitedGemNumText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.LimitedGem, true);
		}

		// Token: 0x06003303 RID: 13059 RVA: 0x0010316C File Offset: 0x0010156C
		private void LoadBanner()
		{
			if (this.m_Hndl != null)
			{
				this.m_Loader.Unload(this.m_Hndl);
			}
			this.m_Hndl = this.m_Loader.Load("texture/gachabanner.muast", new MeigeResource.Option[0]);
			this.m_IsDonePrepare = false;
		}

		// Token: 0x06003304 RID: 13060 RVA: 0x001031B8 File Offset: 0x001015B8
		public override void Destroy()
		{
			this.m_GachaBannerDataList.Clear();
			this.m_GachaScroll.Destroy();
			if (this.m_Hndl != null)
			{
				this.m_Loader.Unload(this.m_Hndl);
				this.m_Hndl = null;
			}
			Debug.Log("Loader Unload isExistLoaded " + this.m_Loader.IsExistLoadedObj());
		}

		// Token: 0x06003305 RID: 13061 RVA: 0x0010321D File Offset: 0x0010161D
		public void OnScrollCallBack()
		{
			if (this.m_GachaDataAnim.State == 1 || this.m_GachaDataAnim.State == 2)
			{
				this.m_GachaDataAnim.PlayOut();
			}
			this.m_ButtonAppearWait = 0f;
		}

		// Token: 0x06003306 RID: 13062 RVA: 0x00103258 File Offset: 0x00101658
		private void Update()
		{
			switch (this.m_Step)
			{
			case GachaSelectUI.eStep.Prepare:
				if (!this.m_IsDonePrepare)
				{
					this.m_Loader.Update();
					if (this.m_Hndl != null && this.m_Hndl.IsDone())
					{
						for (int i = 0; i < this.m_GachaBannerDataList.Count; i++)
						{
							Sprite sprite = null;
							if (this.m_GachaBannerDataList[i].m_IsFirstBanner)
							{
								string b = "GachaBannerFirst_" + this.m_GachaBannerDataList[i].m_GachaBannerID;
								sprite = null;
								for (int j = 0; j < this.m_Hndl.Objs.Length; j++)
								{
									if (this.m_Hndl.Objs[j] is Sprite && this.m_Hndl.Objs[j].name == b)
									{
										sprite = (this.m_Hndl.Objs[j] as Sprite);
										break;
									}
								}
							}
							if (sprite == null)
							{
								string b = "GachaBanner_" + this.m_GachaBannerDataList[i].m_GachaBannerID;
								for (int k = 0; k < this.m_Hndl.Objs.Length; k++)
								{
									if (this.m_Hndl.Objs[k] is Sprite && this.m_Hndl.Objs[k].name == b)
									{
										sprite = (this.m_Hndl.Objs[k] as Sprite);
										break;
									}
								}
							}
							this.m_GachaBannerDataList[i].SetSprite(sprite);
						}
						this.m_GachaScroll.Refresh();
						this.ChangeStep(GachaSelectUI.eStep.In);
						for (int l = 0; l < this.m_AnimList.Length; l++)
						{
							this.m_AnimList[l].PlayIn();
						}
						this.m_MainGroup.Open();
						this.m_IsDonePrepare = true;
					}
				}
				break;
			case GachaSelectUI.eStep.In:
			{
				bool flag = true;
				for (int m = 0; m < this.m_AnimList.Length; m++)
				{
					if (!this.m_AnimList[m].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag = false;
				}
				if (flag)
				{
					GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
					eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
					if (tutoarialSeq == eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay)
					{
						inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.TutorialGuideTitle_1, eText_MessageDB.TutorialGuideMessage_1, null);
					}
					this.ChangeStep(GachaSelectUI.eStep.Idle);
				}
				break;
			}
			case GachaSelectUI.eStep.Idle:
				if (this.m_ButtonAppearWait < 0f)
				{
					this.m_ButtonAppearWait = 0f;
					this.m_GachaDataAnim.PlayIn();
					this.UpdateButtonState();
				}
				if (this.m_GachaDataAnim.State == 0)
				{
					this.m_ButtonAppearWait -= Time.deltaTime;
				}
				break;
			case GachaSelectUI.eStep.Out:
			{
				bool flag2 = true;
				for (int n = 0; n < this.m_AnimList.Length; n++)
				{
					if (!this.m_AnimList[n].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(GachaSelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003307 RID: 13063 RVA: 0x001035D4 File Offset: 0x001019D4
		private void ChangeStep(GachaSelectUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case GachaSelectUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case GachaSelectUI.eStep.Prepare:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case GachaSelectUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case GachaSelectUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case GachaSelectUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case GachaSelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003308 RID: 13064 RVA: 0x00103696 File Offset: 0x00101A96
		public override void PlayIn()
		{
			this.ChangeStep(GachaSelectUI.eStep.Prepare);
		}

		// Token: 0x06003309 RID: 13065 RVA: 0x001036A0 File Offset: 0x00101AA0
		public override void PlayOut()
		{
			this.ChangeStep(GachaSelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x0600330A RID: 13066 RVA: 0x001036E5 File Offset: 0x00101AE5
		public override bool IsMenuEnd()
		{
			return this.m_Step == GachaSelectUI.eStep.End;
		}

		// Token: 0x0600330B RID: 13067 RVA: 0x001036F0 File Offset: 0x00101AF0
		public void OnClickButtonCallback(GachaSelectUI.eCostPattern costPattern)
		{
			if (costPattern != GachaSelectUI.eCostPattern.Day)
			{
				if (costPattern != GachaSelectUI.eCostPattern.Single)
				{
					if (costPattern == GachaSelectUI.eCostPattern.Multi)
					{
						this.OnClickMultiButtonCallBack();
					}
				}
				else
				{
					this.OnClickSingleButtonCallBack();
				}
			}
			else
			{
				this.OnClickUnlimitedGemButtonCallBack();
			}
		}

		// Token: 0x0600330C RID: 13068 RVA: 0x0010372C File Offset: 0x00101B2C
		public void OnClickSingleButtonCallBack()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			if (uigachaData.m_IsStep)
			{
				this.OnClickDecideStepButton.Call(uigachaData.m_GachaID, uigachaData.m_ButtonDatas[1].m_PlayType);
			}
			else
			{
				this.OnClickDecideButton.Call(uigachaData.m_GachaID, uigachaData.m_ButtonDatas[1].m_PlayType);
			}
		}

		// Token: 0x0600330D RID: 13069 RVA: 0x0010379C File Offset: 0x00101B9C
		public void OnClickMultiButtonCallBack()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			this.OnClickDecideButton.Call(uigachaData.m_GachaID, uigachaData.m_ButtonDatas[2].m_PlayType);
		}

		// Token: 0x0600330E RID: 13070 RVA: 0x001037E0 File Offset: 0x00101BE0
		public void OnClickUnlimitedGemButtonCallBack()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			this.OnClickDecideButton.Call(uigachaData.m_GachaID, uigachaData.m_ButtonDatas[0].m_PlayType);
		}

		// Token: 0x0600330F RID: 13071 RVA: 0x00103824 File Offset: 0x00101C24
		private void UpdateButtonState()
		{
			int nowSelectDataIdx = this.m_GachaScroll.GetNowSelectDataIdx();
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[nowSelectDataIdx];
			if (uigachaData == null)
			{
				return;
			}
			if ((uigachaData.m_EndAt - uigachaData.m_StartAt).TotalDays >= 365.0)
			{
				this.m_PeriodText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.GachaSelectPeriodNoExist);
			}
			else
			{
				this.m_PeriodText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.GachaSelectPeriod, new object[]
				{
					uigachaData.m_StartAt.Month,
					uigachaData.m_StartAt.Day,
					UIUtility.GetDayOfWeek(uigachaData.m_StartAt.DayOfWeek),
					uigachaData.m_StartAt.Hour,
					uigachaData.m_StartAt.Minute,
					uigachaData.m_EndAt.Month,
					uigachaData.m_EndAt.Day,
					UIUtility.GetDayOfWeek(uigachaData.m_EndAt.DayOfWeek),
					uigachaData.m_EndAt.Hour,
					uigachaData.m_EndAt.Minute
				});
			}
			List<GachaButton> list = new List<GachaButton>();
			list.Add(this.m_DayButton);
			list.Add(this.m_SingleButton);
			list.Add(this.m_MultiButton);
			int i = 0;
			this.m_TicketNumObj.SetActive(false);
			for (int j = 0; j < uigachaData.m_ButtonDatas.Length; j++)
			{
				if (i < list.Count)
				{
					GachaSelectUI.UIGachaData.UIGachaButtonData uigachaButtonData = uigachaData.m_ButtonDatas[j];
					list[i++].Apply((GachaSelectUI.eCostPattern)j, uigachaButtonData);
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(uigachaButtonData.m_ItemID) > 0)
					{
						this.m_TicketNumObj.SetActive(true);
						this.m_TicketHaveNumIcon.ApplyItem(uigachaButtonData.m_ItemID);
						this.m_TicketHaveNumText.text = UIUtility.ItemHaveNumToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(uigachaButtonData.m_ItemID), true) + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTicketNum);
					}
				}
			}
			while (i < list.Count)
			{
				GachaButton gachaButton = list[i];
				gachaButton.gameObject.SetActive(false);
				i++;
			}
		}

		// Token: 0x06003310 RID: 13072 RVA: 0x00103AB0 File Offset: 0x00101EB0
		public void OnClickURLCallBack()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			this.m_WebviewGroup.Open(uigachaData.m_Url, null);
		}

		// Token: 0x06003311 RID: 13073 RVA: 0x00103AE8 File Offset: 0x00101EE8
		public void OnClickOpenGachaDetailButton()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			this.m_Gacha.Request_GachaBox(uigachaData.m_GachaID, new Action(this.OnResponseRequestGachaBox), true);
		}

		// Token: 0x06003312 RID: 13074 RVA: 0x00103B2C File Offset: 0x00101F2C
		private void OnResponseRequestGachaBox()
		{
			GachaSelectUI.UIGachaData uigachaData = this.m_GachaDataList[this.m_GachaScroll.GetNowSelectDataIdx()];
			this.m_GachaDetailGroup.SetLineUpCharacterIds(this.m_Gacha.GetBoxPickupCharaIDs(), this.m_Gacha.GetBoxCharaIDs());
			this.m_GachaDetailGroup.Open();
		}

		// Token: 0x0400390F RID: 14607
		private const float BUTTON_APPEAR_WAIT = 0f;

		// Token: 0x04003910 RID: 14608
		public const string BANNER_PATH = "texture/gachabanner.muast";

		// Token: 0x04003911 RID: 14609
		public const string BANNER_NAME_BASE = "GachaBanner_";

		// Token: 0x04003912 RID: 14610
		public const string BANNER_NAME_BASE_FIRST = "GachaBannerFirst_";

		// Token: 0x04003913 RID: 14611
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003914 RID: 14612
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003915 RID: 14613
		[SerializeField]
		private GachaDetailGroup m_GachaDetailGroup;

		// Token: 0x04003916 RID: 14614
		[SerializeField]
		private WebViewGroup m_WebviewGroup;

		// Token: 0x04003917 RID: 14615
		[SerializeField]
		private InfiniteScrollEx m_GachaScroll;

		// Token: 0x04003918 RID: 14616
		[SerializeField]
		private AnimUIPlayer m_GachaDataAnim;

		// Token: 0x04003919 RID: 14617
		[SerializeField]
		private GachaButton m_DayButton;

		// Token: 0x0400391A RID: 14618
		[SerializeField]
		private GachaButton m_SingleButton;

		// Token: 0x0400391B RID: 14619
		[SerializeField]
		private GachaButton m_MultiButton;

		// Token: 0x0400391C RID: 14620
		[SerializeField]
		private Text m_UnlimitedGemNumText;

		// Token: 0x0400391D RID: 14621
		[SerializeField]
		private Text m_LimitedGemNumText;

		// Token: 0x0400391E RID: 14622
		[SerializeField]
		private GameObject m_TicketNumObj;

		// Token: 0x0400391F RID: 14623
		[SerializeField]
		private Text m_TicketHaveNumText;

		// Token: 0x04003920 RID: 14624
		[SerializeField]
		private VariableIcon m_TicketHaveNumIcon;

		// Token: 0x04003921 RID: 14625
		[SerializeField]
		private Text m_PeriodText;

		// Token: 0x04003922 RID: 14626
		private List<GachaBannerItemData> m_GachaBannerDataList = new List<GachaBannerItemData>();

		// Token: 0x04003923 RID: 14627
		private float m_ButtonAppearWait;

		// Token: 0x04003924 RID: 14628
		private GachaSelectUI.eStep m_Step;

		// Token: 0x04003925 RID: 14629
		private ABResourceLoader m_Loader;

		// Token: 0x04003926 RID: 14630
		private ABResourceObjectHandler m_Hndl;

		// Token: 0x04003927 RID: 14631
		private bool m_IsDonePrepare;

		// Token: 0x04003928 RID: 14632
		private Gacha m_Gacha;

		// Token: 0x0400392B RID: 14635
		private List<GachaSelectUI.UIGachaData> m_GachaDataList;

		// Token: 0x0200099F RID: 2463
		private enum eStep
		{
			// Token: 0x0400392D RID: 14637
			Hide,
			// Token: 0x0400392E RID: 14638
			Prepare,
			// Token: 0x0400392F RID: 14639
			In,
			// Token: 0x04003930 RID: 14640
			Idle,
			// Token: 0x04003931 RID: 14641
			Out,
			// Token: 0x04003932 RID: 14642
			End
		}

		// Token: 0x020009A0 RID: 2464
		public enum eCostPattern
		{
			// Token: 0x04003934 RID: 14644
			None = -1,
			// Token: 0x04003935 RID: 14645
			Day,
			// Token: 0x04003936 RID: 14646
			Single,
			// Token: 0x04003937 RID: 14647
			Multi,
			// Token: 0x04003938 RID: 14648
			Num
		}

		// Token: 0x020009A1 RID: 2465
		public class UIGachaData
		{
			// Token: 0x06003313 RID: 13075 RVA: 0x00103B7C File Offset: 0x00101F7C
			public UIGachaData(Gacha.GachaData gachaData)
			{
				this.m_GachaID = gachaData.m_ID;
				this.m_GachaBannerID = gachaData.m_BannerID;
				this.m_Url = gachaData.m_WebViewUrl;
				this.m_CurrentStep = GachaUtility.GetPlayNum(gachaData) + 1;
				this.m_MaxStep = gachaData.m_WonLimit;
				this.m_StartAt = gachaData.m_StartAt;
				this.m_EndAt = gachaData.m_EndAt;
				this.m_IsFirstBanner = GachaUtility.IsEnablePlayFirst10(gachaData);
				for (int i = 0; i < this.m_ButtonDatas.Length; i++)
				{
					this.m_ButtonDatas[i] = new GachaSelectUI.UIGachaData.UIGachaButtonData();
				}
				if (!GachaUtility.IsExistPlayUnlimitedGem(gachaData))
				{
					this.m_ButtonDatas[0].m_Exist = false;
				}
				else
				{
					this.m_ButtonDatas[0].m_PlayType = GachaDefine.ePlayType.Unlimited;
					this.m_ButtonDatas[0].m_Exist = true;
					this.m_ButtonDatas[0].m_CostNum = gachaData.m_Cost_UnlimitedGem1;
					if (GachaUtility.IsEnablePlayUnlimitedGem(gachaData))
					{
						this.m_ButtonDatas[0].m_Interactable = true;
					}
					else
					{
						this.m_ButtonDatas[0].m_Interactable = false;
					}
				}
				if (!GachaUtility.IsEnablePlayGem1(gachaData))
				{
					this.m_ButtonDatas[1].m_Exist = false;
				}
				else
				{
					this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Gem1;
					this.m_ButtonDatas[1].m_Exist = true;
					this.m_ButtonDatas[1].m_CostNum = gachaData.m_Cost_Gem1;
					this.m_ButtonDatas[1].m_Interactable = true;
				}
				if (!GachaUtility.IsEnablePlayGem10(gachaData))
				{
					this.m_ButtonDatas[2].m_Exist = false;
				}
				else if (GachaUtility.IsEnablePlayFirst10(gachaData))
				{
					this.m_ButtonDatas[2].m_PlayType = GachaDefine.ePlayType.Gem10;
					this.m_ButtonDatas[2].m_Exist = true;
					this.m_ButtonDatas[2].m_CostNum = gachaData.m_Cost_First10;
					this.m_ButtonDatas[2].m_Interactable = true;
					this.m_ButtonDatas[2].m_ExCondition = GachaSelectUI.UIGachaData.eExCondition.First10;
				}
				else
				{
					this.m_ButtonDatas[2].m_PlayType = GachaDefine.ePlayType.Gem10;
					this.m_ButtonDatas[2].m_Exist = true;
					this.m_ButtonDatas[2].m_CostNum = gachaData.m_Cost_Gem10;
					this.m_ButtonDatas[2].m_Interactable = true;
				}
				if (GachaUtility.IsExistPlayItem(gachaData))
				{
					if (GachaUtility.IsEnablePlayItem(gachaData))
					{
						this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Item;
						this.m_ButtonDatas[1].m_Exist = true;
						this.m_ButtonDatas[1].m_ItemID = gachaData.m_Cost_ItemID;
						this.m_ButtonDatas[1].m_CostNum = gachaData.m_Cost_ItemAmount;
						this.m_ButtonDatas[1].m_Interactable = true;
					}
					else if (!this.m_ButtonDatas[1].m_Exist)
					{
						this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Item;
						this.m_ButtonDatas[1].m_Exist = true;
						this.m_ButtonDatas[1].m_ItemID = gachaData.m_Cost_ItemID;
						this.m_ButtonDatas[1].m_CostNum = gachaData.m_Cost_ItemAmount;
						this.m_ButtonDatas[1].m_Interactable = false;
					}
				}
			}

			// Token: 0x06003314 RID: 13076 RVA: 0x00103E84 File Offset: 0x00102284
			public UIGachaData(Gacha.StepGachaData gachaData)
			{
				this.m_IsStep = true;
				this.m_GachaID = gachaData.m_ID;
				this.m_GachaBannerID = gachaData.m_BannerID;
				this.m_Url = gachaData.m_WebViewUrl;
				this.m_CurrentStep = GachaUtility.GetCurrentStep(gachaData) + 1;
				this.m_MaxStep = GachaUtility.GetMaxStep(gachaData);
				this.m_StartAt = gachaData.m_StartAt;
				this.m_EndAt = gachaData.m_EndAt;
				for (int i = 0; i < this.m_ButtonDatas.Length; i++)
				{
					this.m_ButtonDatas[i] = new GachaSelectUI.UIGachaData.UIGachaButtonData();
				}
				this.m_ButtonDatas[0].m_Exist = false;
				this.m_ButtonDatas[2].m_Exist = false;
				this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Gem1;
				if (GachaUtility.IsEnablePlayUnlimitedGemStep(gachaData))
				{
					this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Unlimited;
					this.m_ButtonDatas[1].m_Exist = true;
					this.m_ButtonDatas[1].m_CostNum = gachaData.m_Steps[GachaUtility.GetCurrentStep(gachaData)].m_Cost_UnlimitedGem;
				}
				else
				{
					this.m_ButtonDatas[1].m_PlayType = GachaDefine.ePlayType.Gem1;
					this.m_ButtonDatas[1].m_Exist = true;
					this.m_ButtonDatas[1].m_CostNum = gachaData.m_Steps[GachaUtility.GetCurrentStep(gachaData)].m_Cost_LimitedGem;
				}
				this.m_ButtonDatas[1].m_Interactable = true;
			}

			// Token: 0x04003939 RID: 14649
			public bool m_IsStep;

			// Token: 0x0400393A RID: 14650
			public int m_GachaID = -1;

			// Token: 0x0400393B RID: 14651
			public string m_GachaBannerID;

			// Token: 0x0400393C RID: 14652
			public string m_Url;

			// Token: 0x0400393D RID: 14653
			public int m_CurrentStep;

			// Token: 0x0400393E RID: 14654
			public int m_MaxStep;

			// Token: 0x0400393F RID: 14655
			public DateTime m_StartAt;

			// Token: 0x04003940 RID: 14656
			public DateTime m_EndAt;

			// Token: 0x04003941 RID: 14657
			public bool m_IsFirstBanner;

			// Token: 0x04003942 RID: 14658
			public GachaSelectUI.UIGachaData.UIGachaButtonData[] m_ButtonDatas = new GachaSelectUI.UIGachaData.UIGachaButtonData[3];

			// Token: 0x020009A2 RID: 2466
			public enum eExCondition
			{
				// Token: 0x04003944 RID: 14660
				None,
				// Token: 0x04003945 RID: 14661
				First10
			}

			// Token: 0x020009A3 RID: 2467
			public class UIGachaButtonData
			{
				// Token: 0x04003946 RID: 14662
				public GachaDefine.ePlayType m_PlayType = GachaDefine.ePlayType.Gem1;

				// Token: 0x04003947 RID: 14663
				public bool m_Exist;

				// Token: 0x04003948 RID: 14664
				public bool m_Interactable;

				// Token: 0x04003949 RID: 14665
				public int m_ItemID = -1;

				// Token: 0x0400394A RID: 14666
				public int m_CostNum;

				// Token: 0x0400394B RID: 14667
				public GachaSelectUI.UIGachaData.eExCondition m_ExCondition;
			}
		}
	}
}
