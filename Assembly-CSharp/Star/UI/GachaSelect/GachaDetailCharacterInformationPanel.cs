﻿using System;
using Star.UI.Edit;

namespace Star.UI.GachaSelect
{
	// Token: 0x02000980 RID: 2432
	public class GachaDetailCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x06003232 RID: 12850 RVA: 0x00100449 File Offset: 0x000FE849
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetTitleImage(cdc.GetCharaId());
			base.SetStateWeaponIconMode(WeaponIconWithFrame.eMode.NakedBGOnly);
			base.SetStateWeaponIcon(cdc);
			base.SetEnableRenderCharacterView(true);
			base.ForceRequestLoadCharacterView(SelectedCharaInfoCharacterViewController.eViewType.Icon, cdc);
			base.SetCharacterIconLimitBreak(cdc.GetCharaLimitBreak());
		}

		// Token: 0x06003233 RID: 12851 RVA: 0x00100480 File Offset: 0x000FE880
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetCharaInfoTitle(cdc.GetCharaId());
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetExpData(cdc, true, true);
			base.ApplyState(cdc);
			base.SetCost(cdc.GetCost());
			base.SetCharaInfoTitleCost(cdc);
		}

		// Token: 0x06003234 RID: 12852 RVA: 0x001004FA File Offset: 0x000FE8FA
		protected override void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = true;
			this.m_IsDirtyParameter = true;
		}
	}
}
