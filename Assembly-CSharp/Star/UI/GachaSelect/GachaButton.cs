﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaSelect
{
	// Token: 0x0200099B RID: 2459
	public class GachaButton : MonoBehaviour
	{
		// Token: 0x1400008B RID: 139
		// (add) Token: 0x060032F4 RID: 13044 RVA: 0x00102ABC File Offset: 0x00100EBC
		// (remove) Token: 0x060032F5 RID: 13045 RVA: 0x00102AF4 File Offset: 0x00100EF4
		public event Action<GachaSelectUI.eCostPattern> OnClickGachaButton;

		// Token: 0x060032F6 RID: 13046 RVA: 0x00102B2C File Offset: 0x00100F2C
		public void Apply(GachaSelectUI.eCostPattern costPattern, GachaSelectUI.UIGachaData.UIGachaButtonData buttonData)
		{
			this.m_CostPattern = costPattern;
			GachaButton.eGachaButtonType eGachaButtonType = GachaButton.eGachaButtonType.Error;
			if (buttonData != null && buttonData.m_Exist)
			{
				switch (buttonData.m_PlayType)
				{
				case GachaDefine.ePlayType.Unlimited:
					eGachaButtonType = GachaButton.eGachaButtonType.Only;
					break;
				case GachaDefine.ePlayType.Gem1:
					eGachaButtonType = GachaButton.eGachaButtonType.Stone;
					break;
				case GachaDefine.ePlayType.Gem10:
					if (buttonData.m_ExCondition == GachaSelectUI.UIGachaData.eExCondition.First10)
					{
						eGachaButtonType = GachaButton.eGachaButtonType.First10;
					}
					else
					{
						eGachaButtonType = GachaButton.eGachaButtonType.Stone10;
					}
					break;
				case GachaDefine.ePlayType.Item:
					if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(buttonData.m_ItemID).m_TypeArgs[0] == 0)
					{
						eGachaButtonType = GachaButton.eGachaButtonType.Ticket;
					}
					else
					{
						eGachaButtonType = GachaButton.eGachaButtonType.LimitTicket;
					}
					break;
				}
			}
			if (eGachaButtonType != GachaButton.eGachaButtonType.Error)
			{
				base.gameObject.SetActive(true);
				this.SetSprite(eGachaButtonType);
				this.SetState(buttonData.m_Interactable, buttonData.m_PlayType, buttonData.m_CostNum, buttonData.m_ItemID);
			}
			else
			{
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x060032F7 RID: 13047 RVA: 0x00102C20 File Offset: 0x00101020
		private void SetSprite(GachaButton.eGachaButtonType type)
		{
			this.m_ButtonImageNormal.sprite = this.m_ButtonSpriteInfos[(int)type].m_SpriteNormal;
			this.m_ButtonImagePush.sprite = this.m_ButtonSpriteInfos[(int)type].m_SpritePush;
			this.m_ButtonImageLabel.sprite = this.m_ButtonSpriteInfos[(int)type].m_SpriteLabel;
		}

		// Token: 0x060032F8 RID: 13048 RVA: 0x00102C78 File Offset: 0x00101078
		public void SetState(bool interactable, GachaDefine.ePlayType playType, int useNum, int itemID = -1)
		{
			this.m_CustomButton.Interactable = interactable;
			if (useNum == 0)
			{
				this.m_VariableIconCloner.GetInst<VariableIcon>().ApplyGem();
				this.m_CostNumText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.GachaSelectFree);
				this.m_LimitObj.SetActive(false);
			}
			else
			{
				switch (playType)
				{
				case GachaDefine.ePlayType.Unlimited:
					this.m_VariableIconCloner.GetInst<VariableIcon>().ApplyGem();
					this.m_CostNumText.text = useNum.ToString() + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonNum);
					this.m_LimitObj.SetActive(true);
					break;
				case GachaDefine.ePlayType.Gem1:
				case GachaDefine.ePlayType.Gem10:
					this.m_VariableIconCloner.GetInst<VariableIcon>().ApplyGem();
					this.m_CostNumText.text = useNum.ToString() + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonNum);
					this.m_LimitObj.SetActive(false);
					break;
				case GachaDefine.ePlayType.Item:
					this.m_VariableIconCloner.GetInst<VariableIcon>().ApplyItem(itemID);
					this.m_CostNumText.text = useNum.ToString() + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonTicketNum);
					this.m_LimitObj.SetActive(false);
					break;
				}
			}
		}

		// Token: 0x060032F9 RID: 13049 RVA: 0x00102DEC File Offset: 0x001011EC
		public void OnClickButtonCallBack()
		{
			this.OnClickGachaButton.Call(this.m_CostPattern);
		}

		// Token: 0x040038FA RID: 14586
		[SerializeField]
		[Tooltip("ボタンの元リソース.列挙順.")]
		private GachaButton.ButtonSpriteInformation[] m_ButtonSpriteInfos;

		// Token: 0x040038FB RID: 14587
		[SerializeField]
		private Image m_ButtonImageNormal;

		// Token: 0x040038FC RID: 14588
		[SerializeField]
		private Image m_ButtonImagePush;

		// Token: 0x040038FD RID: 14589
		[SerializeField]
		private Image m_ButtonImageLabel;

		// Token: 0x040038FE RID: 14590
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040038FF RID: 14591
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x04003900 RID: 14592
		[SerializeField]
		private GameObject m_LimitObj;

		// Token: 0x04003901 RID: 14593
		[SerializeField]
		private Text m_CostNumText;

		// Token: 0x04003902 RID: 14594
		private GachaSelectUI.eCostPattern m_CostPattern;

		// Token: 0x0200099C RID: 2460
		public enum eGachaButtonType
		{
			// Token: 0x04003905 RID: 14597
			Error,
			// Token: 0x04003906 RID: 14598
			Stone,
			// Token: 0x04003907 RID: 14599
			Stone10,
			// Token: 0x04003908 RID: 14600
			Only,
			// Token: 0x04003909 RID: 14601
			Ticket,
			// Token: 0x0400390A RID: 14602
			LimitTicket,
			// Token: 0x0400390B RID: 14603
			First10
		}

		// Token: 0x0200099D RID: 2461
		[Serializable]
		public class ButtonSpriteInformation
		{
			// Token: 0x0400390C RID: 14604
			[SerializeField]
			public Sprite m_SpriteNormal;

			// Token: 0x0400390D RID: 14605
			[SerializeField]
			public Sprite m_SpritePush;

			// Token: 0x0400390E RID: 14606
			[SerializeField]
			public Sprite m_SpriteLabel;
		}
	}
}
