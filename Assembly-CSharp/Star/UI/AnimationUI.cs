﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F2 RID: 2034
	public class AnimationUI : AnimUIBase
	{
		// Token: 0x06002A10 RID: 10768 RVA: 0x000DDB42 File Offset: 0x000DBF42
		protected override void Update()
		{
			base.Update();
			if (this.m_IsPlaying && !this.m_Animation.isPlaying)
			{
				this.m_IsPlaying = false;
			}
		}

		// Token: 0x06002A11 RID: 10769 RVA: 0x000DDB6C File Offset: 0x000DBF6C
		protected override void ExecutePlayIn()
		{
			if (this.m_Animation == null)
			{
				this.m_Animation = base.GetComponent<Animation>();
			}
			this.m_Animation.Stop();
			if (this.m_InClip != null)
			{
				this.m_Animation.clip = this.m_InClip;
				this.m_Animation[this.m_InClip.name].speed = 1f;
			}
			this.m_Animation.Play();
		}

		// Token: 0x06002A12 RID: 10770 RVA: 0x000DDBF0 File Offset: 0x000DBFF0
		protected override void ExecutePlayOut()
		{
			if (this.m_Animation == null)
			{
				this.m_Animation = base.GetComponent<Animation>();
			}
			this.m_Animation.Stop();
			if (this.m_ReturnMode)
			{
				if (this.m_InClip != null)
				{
					this.m_Animation.clip = this.m_InClip;
					this.m_Animation[this.m_InClip.name].normalizedTime = 1f;
					this.m_Animation[this.m_InClip.name].speed = -1f;
				}
			}
			else if (this.m_OutClip != null)
			{
				this.m_Animation.clip = this.m_OutClip;
				this.m_Animation[this.m_OutClip.name].speed = 1f;
			}
			this.m_Animation.Play();
		}

		// Token: 0x06002A13 RID: 10771 RVA: 0x000DDCE8 File Offset: 0x000DC0E8
		public override void Hide()
		{
			base.Hide();
			if (this.m_InClip != null)
			{
				this.m_Animation.clip = this.m_InClip;
				this.m_Animation[this.m_InClip.name].normalizedTime = 0f;
				this.m_Animation[this.m_InClip.name].speed = 0f;
			}
		}

		// Token: 0x06002A14 RID: 10772 RVA: 0x000DDD60 File Offset: 0x000DC160
		public override void Show()
		{
			base.Show();
			if (this.m_InClip != null)
			{
				this.m_Animation.clip = this.m_InClip;
				this.m_Animation[this.m_InClip.name].normalizedTime = 1f;
				this.m_Animation[this.m_InClip.name].speed = 0f;
			}
		}

		// Token: 0x04003068 RID: 12392
		[SerializeField]
		private bool m_ReturnMode = true;

		// Token: 0x04003069 RID: 12393
		[SerializeField]
		private AnimationClip m_InClip;

		// Token: 0x0400306A RID: 12394
		[SerializeField]
		private AnimationClip m_OutClip;

		// Token: 0x0400306B RID: 12395
		private Animation m_Animation;
	}
}
