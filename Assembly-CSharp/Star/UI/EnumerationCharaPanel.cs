﻿using System;

namespace Star.UI
{
	// Token: 0x02000962 RID: 2402
	public class EnumerationCharaPanel : EnumerationCharaPanelStandardBase
	{
		// Token: 0x060031BF RID: 12735 RVA: 0x000FE9E1 File Offset: 0x000FCDE1
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
			base.Setup(argument);
		}

		// Token: 0x060031C0 RID: 12736 RVA: 0x000FE9EA File Offset: 0x000FCDEA
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
			this.m_SharedIntance.partyMemberController = partyMemberController;
			this.ApplyProcess(this.m_SharedIntance.partyMemberController);
		}
	}
}
