﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200089C RID: 2204
	public class RecipeMaterial : MonoBehaviour
	{
		// Token: 0x06002DC2 RID: 11714 RVA: 0x000F0F94 File Offset: 0x000EF394
		public void Apply(int itemID, int useNum, int haveNum)
		{
			if (itemID != -1 && useNum > 0)
			{
				this.m_ItemID = itemID;
				this.m_ItemIconCloner.GetInst<ItemIconWithFrame>().Apply(itemID);
				base.gameObject.SetActive(true);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(useNum);
				if (useNum > haveNum)
				{
					stringBuilder = UIUtility.GetNotEnoughString(stringBuilder);
				}
				this.m_UseText.text = stringBuilder.ToString();
				this.m_HaveText.text = UIUtility.ItemHaveNumToString(haveNum, true);
			}
			else
			{
				base.gameObject.SetActive(false);
				this.m_UseText.text = "--";
				this.m_HaveText.text = "--";
			}
		}

		// Token: 0x06002DC3 RID: 11715 RVA: 0x000F1043 File Offset: 0x000EF443
		public void SetEnableRenderItemIcon(bool flg)
		{
			this.m_ItemIconCloner.GetInst<ItemIconWithFrame>().SetEnableRenderItemIcon(flg);
		}

		// Token: 0x06002DC4 RID: 11716 RVA: 0x000F1056 File Offset: 0x000EF456
		public void OnClickIconCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ItemDetailWindow.OpenItem(this.m_ItemID);
		}

		// Token: 0x040034C1 RID: 13505
		[SerializeField]
		private PrefabCloner m_ItemIconCloner;

		// Token: 0x040034C2 RID: 13506
		[SerializeField]
		private Text m_UseText;

		// Token: 0x040034C3 RID: 13507
		[SerializeField]
		private Text m_HaveText;

		// Token: 0x040034C4 RID: 13508
		private int m_ItemID = -1;
	}
}
