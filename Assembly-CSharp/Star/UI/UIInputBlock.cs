﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008C3 RID: 2243
	public class UIInputBlock : MonoBehaviour
	{
		// Token: 0x06002E8D RID: 11917 RVA: 0x000F423A File Offset: 0x000F263A
		private void Start()
		{
			this.m_BlockObj.raycastTarget = false;
		}

		// Token: 0x06002E8E RID: 11918 RVA: 0x000F4248 File Offset: 0x000F2648
		private void Update()
		{
			this.m_BlockObj.raycastTarget = this.IsBlockInput();
		}

		// Token: 0x06002E8F RID: 11919 RVA: 0x000F425B File Offset: 0x000F265B
		public void Clear()
		{
			this.m_BlockObj.raycastTarget = false;
			this.m_BlockObjStackList.Clear();
		}

		// Token: 0x06002E90 RID: 11920 RVA: 0x000F4274 File Offset: 0x000F2674
		public void SetBlockObj(GameObject blockObj, bool flg)
		{
			if (flg)
			{
				if (this.m_BlockObjStackList.IndexOf(blockObj) == -1)
				{
					this.m_BlockObjStackList.Add(blockObj);
				}
			}
			else
			{
				this.m_BlockObjStackList.Remove(blockObj);
			}
			this.m_BlockObj.raycastTarget = this.IsBlockInput();
		}

		// Token: 0x06002E91 RID: 11921 RVA: 0x000F42C8 File Offset: 0x000F26C8
		public bool IsBlockInput()
		{
			for (int i = 0; i < this.m_BlockObjStackList.Count; i++)
			{
				if (this.m_BlockObjStackList[i].activeInHierarchy)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x040035B6 RID: 13750
		[SerializeField]
		private InvisibleGraphic m_BlockObj;

		// Token: 0x040035B7 RID: 13751
		private List<GameObject> m_BlockObjStackList = new List<GameObject>();
	}
}
