﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F6 RID: 2038
	[RequireComponent(typeof(CanvasGroup))]
	public class BlinkCanvasGroup : MonoBehaviour
	{
		// Token: 0x06002A2C RID: 10796 RVA: 0x000DE795 File Offset: 0x000DCB95
		public void Start()
		{
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
		}

		// Token: 0x06002A2D RID: 10797 RVA: 0x000DE7A4 File Offset: 0x000DCBA4
		private void Update()
		{
			float num;
			if (this.m_Sync)
			{
				num = 0.5f + Mathf.Sin(Time.timeSinceLevelLoad * this.m_Speed) * 0.5f;
			}
			else
			{
				num = 0.5f + Mathf.Sin(this.m_TimeCount * this.m_Speed) * 0.5f;
				this.m_TimeCount += Time.deltaTime;
			}
			this.m_CanvasGroup.alpha = this.m_MinValue * (1f - num) + this.m_MaxValue * num;
		}

		// Token: 0x0400308A RID: 12426
		private CanvasGroup m_CanvasGroup;

		// Token: 0x0400308B RID: 12427
		[SerializeField]
		private float m_MaxValue = 1f;

		// Token: 0x0400308C RID: 12428
		[SerializeField]
		private float m_MinValue;

		// Token: 0x0400308D RID: 12429
		[SerializeField]
		private float m_Speed = 1f;

		// Token: 0x0400308E RID: 12430
		[SerializeField]
		private bool m_Sync = true;

		// Token: 0x0400308F RID: 12431
		private float m_TimeCount;
	}
}
