﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B5 RID: 1973
	public abstract class ASyncImageAdapter
	{
		// Token: 0x060028CB RID: 10443
		public abstract void Apply(int charaID, int weaponID);

		// Token: 0x060028CC RID: 10444
		public abstract void Destroy();

		// Token: 0x060028CD RID: 10445 RVA: 0x000D9ADC File Offset: 0x000D7EDC
		public AnimUIPlayer GetAnimUIPlayer()
		{
			MonoBehaviour monoBehaviour = this.GetMonoBehaviour();
			if (monoBehaviour == null)
			{
				return null;
			}
			return monoBehaviour.GetComponent<AnimUIPlayer>();
		}

		// Token: 0x060028CE RID: 10446
		public abstract bool IsDoneLoad();

		// Token: 0x060028CF RID: 10447
		protected abstract MonoBehaviour GetMonoBehaviour();
	}
}
