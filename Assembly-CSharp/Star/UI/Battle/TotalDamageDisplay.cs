﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200079C RID: 1948
	public class TotalDamageDisplay : MonoBehaviour
	{
		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x060027C9 RID: 10185 RVA: 0x000D50F0 File Offset: 0x000D34F0
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x060027CA RID: 10186 RVA: 0x000D5118 File Offset: 0x000D3518
		public void Play(float time, int totalDamage, int totalRecover, int bonus)
		{
			this.GameObject.SetActive(true);
			if (totalDamage <= 0)
			{
				this.m_NumberObj.SetActive(true);
				this.m_Number.PlayIn(-totalDamage, bonus);
			}
			else
			{
				this.m_NumberObj.SetActive(false);
			}
			if (totalRecover > 0)
			{
				this.m_RecoverObj.SetActive(true);
				this.m_RecoverNumber.PlayIn(totalRecover, bonus);
			}
			else
			{
				this.m_RecoverObj.SetActive(false);
			}
			this.m_IsPlay = true;
			this.m_PlayTime = 0f;
			this.m_DisplayTime = time;
		}

		// Token: 0x060027CB RID: 10187 RVA: 0x000D51AF File Offset: 0x000D35AF
		public void PlayTotalDamageOnly(float time, int totalDamage, int bonus)
		{
			this.Play(time, totalDamage, 0, bonus);
		}

		// Token: 0x060027CC RID: 10188 RVA: 0x000D51BB File Offset: 0x000D35BB
		public void PlayTotalRecoverOnly(float time, int totalRecover, int bonus)
		{
			this.Play(time, 1, totalRecover, bonus);
		}

		// Token: 0x060027CD RID: 10189 RVA: 0x000D51C8 File Offset: 0x000D35C8
		private void Update()
		{
			if (this.m_IsPlay)
			{
				if (this.m_PlayTime > this.m_DisplayTime && !this.m_Number.IsPlaying && !this.m_RecoverNumber.IsPlaying)
				{
					if (this.m_NumberObj.activeSelf)
					{
						this.m_Number.PlayOut();
					}
					if (this.m_RecoverObj.activeSelf)
					{
						this.m_RecoverNumber.PlayOut();
					}
					this.m_IsPlay = false;
				}
				this.m_PlayTime += Time.deltaTime;
			}
		}

		// Token: 0x04002EE5 RID: 12005
		[SerializeField]
		private TotalDamage m_Number;

		// Token: 0x04002EE6 RID: 12006
		[SerializeField]
		private TotalDamage m_RecoverNumber;

		// Token: 0x04002EE7 RID: 12007
		[SerializeField]
		private GameObject m_NumberObj;

		// Token: 0x04002EE8 RID: 12008
		[SerializeField]
		private GameObject m_RecoverObj;

		// Token: 0x04002EE9 RID: 12009
		private float m_PlayTime;

		// Token: 0x04002EEA RID: 12010
		private float m_DisplayTime;

		// Token: 0x04002EEB RID: 12011
		private bool m_IsPlay;

		// Token: 0x04002EEC RID: 12012
		private GameObject m_GameObject;
	}
}
