﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000766 RID: 1894
	public class BenchStatus : MonoBehaviour
	{
		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06002673 RID: 9843 RVA: 0x000CC5DC File Offset: 0x000CA9DC
		public RectTransform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x000CC5E4 File Offset: 0x000CA9E4
		public void Setup(int idx)
		{
			this.m_Idx = idx;
			this.m_Transform = base.gameObject.GetComponent<RectTransform>();
			this.SetEnableInput(true);
		}

		// Token: 0x06002675 RID: 9845 RVA: 0x000CC605 File Offset: 0x000CAA05
		public void Destroy()
		{
			this.m_BustImage.Destroy();
		}

		// Token: 0x06002676 RID: 9846 RVA: 0x000CC614 File Offset: 0x000CAA14
		public void SetupForOpenMemberChange(int charaID, int lv, int maxLv, int currentHp, int maxHp, eElementType elementType, int recast, int recastMax, string nickName, string turnOwnerNickName, bool isStateAbnormalIsolation, bool executedMemberChange, bool isTurnOwnerFriend)
		{
			this.m_TurnOwnerNickName = turnOwnerNickName;
			this.m_IsStateAbnormalIsolation = isStateAbnormalIsolation;
			this.m_ExecutedMemberChange = executedMemberChange;
			this.m_IsTurnOwnerFriend = isTurnOwnerFriend;
			this.m_BustImage.Apply(charaID, BustImage.eBustImageType.Chara);
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			this.m_ElementIcon.Apply((eElementType)param.m_Element);
			this.m_ClassIcon.Apply((eClassType)param.m_Class);
			this.m_RareStar.Apply((eRare)param.m_Rare);
			this.m_LvText.text = lv + " / " + maxLv;
			this.m_Frame.sprite = this.m_FrameSprites[param.m_Rare - 2];
			this.m_HPGauge.fillAmount = (float)currentHp / (float)maxHp;
			this.m_HPText.text = currentHp.ToString();
			this.m_RecastValue = recast;
			if (this.m_RecastValue > 0)
			{
				this.m_ChangeDisable.SetActive(true);
				this.m_ImageNumbersBase.gameObject.SetActive(true);
				this.m_ImageNumbers.gameObject.SetActive(true);
				this.m_ImageNumbersBase.SetValue(this.m_RecastValue);
				this.m_ImageNumbers.SetValue(this.m_RecastValue);
			}
			else
			{
				this.m_ChangeDisable.SetActive(false);
				this.m_ImageNumbersBase.gameObject.SetActive(false);
				this.m_ImageNumbers.gameObject.SetActive(false);
			}
			if (this.m_RecastValue > 0 || this.m_IsStateAbnormalIsolation || this.m_ExecutedMemberChange || this.m_IsTurnOwnerFriend)
			{
				this.m_Button.Interactable = false;
			}
			else
			{
				this.m_Button.Interactable = true;
			}
		}

		// Token: 0x06002677 RID: 9847 RVA: 0x000CC7E1 File Offset: 0x000CABE1
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06002678 RID: 9848 RVA: 0x000CC7EF File Offset: 0x000CABEF
		public void SetEnableInput(bool flag)
		{
			this.m_IsEnableInput = flag;
		}

		// Token: 0x06002679 RID: 9849 RVA: 0x000CC7F8 File Offset: 0x000CABF8
		public void OnClickCallBack()
		{
			if (!this.m_IsEnableInput)
			{
				return;
			}
			if (this.m_IsTurnOwnerFriend)
			{
				string textCommon = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeFriendCantUseMemberChange);
				this.m_UI.GetBattleMessage().SetMessage(textCommon, false);
				return;
			}
			if (this.m_ExecutedMemberChange)
			{
				string textCommon2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeExecutedMemberChange);
				this.m_UI.GetBattleMessage().SetMessage(textCommon2, false);
				return;
			}
			if (this.m_IsStateAbnormalIsolation)
			{
				string textCommon3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleNoticeIsolation, new object[]
				{
					this.m_TurnOwnerNickName
				});
				this.m_UI.GetBattleMessage().SetMessage(textCommon3, false);
				return;
			}
			if (this.m_RecastValue > 0)
			{
				return;
			}
			this.m_MemberChange.DecideMember(this.m_Idx);
		}

		// Token: 0x04002CD6 RID: 11478
		[SerializeField]
		protected BustImage m_BustImage;

		// Token: 0x04002CD7 RID: 11479
		[SerializeField]
		private Image m_Frame;

		// Token: 0x04002CD8 RID: 11480
		[SerializeField]
		private Sprite[] m_FrameSprites;

		// Token: 0x04002CD9 RID: 11481
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04002CDA RID: 11482
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04002CDB RID: 11483
		[SerializeField]
		private RareStar m_RareStar;

		// Token: 0x04002CDC RID: 11484
		[SerializeField]
		private Text m_LvText;

		// Token: 0x04002CDD RID: 11485
		[SerializeField]
		private Image m_HPGauge;

		// Token: 0x04002CDE RID: 11486
		[SerializeField]
		private Text m_HPText;

		// Token: 0x04002CDF RID: 11487
		[SerializeField]
		private MemberChange m_MemberChange;

		// Token: 0x04002CE0 RID: 11488
		[SerializeField]
		private GameObject m_ChangeDisable;

		// Token: 0x04002CE1 RID: 11489
		[SerializeField]
		private ImageNumbers m_ImageNumbersBase;

		// Token: 0x04002CE2 RID: 11490
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x04002CE3 RID: 11491
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x04002CE4 RID: 11492
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04002CE5 RID: 11493
		private int m_Idx;

		// Token: 0x04002CE6 RID: 11494
		private int m_RecastValue;

		// Token: 0x04002CE7 RID: 11495
		private int m_RecastMax;

		// Token: 0x04002CE8 RID: 11496
		private bool m_IsStateAbnormalIsolation;

		// Token: 0x04002CE9 RID: 11497
		private bool m_ExecutedMemberChange;

		// Token: 0x04002CEA RID: 11498
		private bool m_IsTurnOwnerFriend;

		// Token: 0x04002CEB RID: 11499
		private string m_TurnOwnerNickName;

		// Token: 0x04002CEC RID: 11500
		private RectTransform m_Transform;

		// Token: 0x04002CED RID: 11501
		private bool m_IsEnableInput;
	}
}
