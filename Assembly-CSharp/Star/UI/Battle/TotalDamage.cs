﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200079B RID: 1947
	public class TotalDamage : MonoBehaviour
	{
		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x060027C3 RID: 10179 RVA: 0x000D4C4F File Offset: 0x000D304F
		public bool IsPlaying
		{
			get
			{
				return this.m_IsPlaying;
			}
		}

		// Token: 0x060027C4 RID: 10180 RVA: 0x000D4C58 File Offset: 0x000D3058
		private void Setup()
		{
			RectTransform[] componentsInChildren = this.m_Nums.GetComponentsInChildren<RectTransform>(true);
			this.m_NumRects = new RectTransform[componentsInChildren.Length - 1];
			for (int i = 1; i < componentsInChildren.Length; i++)
			{
				this.m_NumRects[i - 1] = componentsInChildren[i];
			}
		}

		// Token: 0x060027C5 RID: 10181 RVA: 0x000D4CA4 File Offset: 0x000D30A4
		public void PlayIn(int value, int bonus)
		{
			if (!this.m_IsDoneSetup)
			{
				this.Setup();
				this.m_IsDoneSetup = true;
			}
			this.m_Nums.SetValue(value);
			if (value <= 0)
			{
				this.m_Digit = 1;
			}
			else
			{
				this.m_Digit = (int)Mathf.Log10((float)value) + 1;
			}
			float num = (float)(this.m_Digit / 2);
			if (this.m_Digit % 2 == 0)
			{
				num -= 0.5f;
			}
			float num2 = this.m_NumRects[0].rect.width * 1.25f;
			for (int i = 0; i < this.m_NumRects.Length; i++)
			{
				this.m_NumRects[i].localScale = new Vector3(1.25f, 1.25f, 1f);
				this.m_NumRects[i].localPosition = new Vector3(num * num2 - num2 * (float)i, 0f, 1f);
			}
			this.m_Anim.PlayIn();
			this.m_Title.localScale = new Vector3(1.25f, 1.25f, 1f);
			if (bonus < 3 && bonus > 0)
			{
				this.m_BonusImage.gameObject.SetActive(true);
				this.m_BonusImage.sprite = this.m_BonusSprites[bonus - 1];
				this.m_BonusImage.GetComponent<AnimUIPlayer>().Hide();
				this.m_BonusImage.GetComponent<AnimUIPlayer>().PlayIn();
			}
			else
			{
				this.m_BonusImage.gameObject.SetActive(false);
				this.m_BonusImage.GetComponent<AnimUIPlayer>().Hide();
			}
			this.m_IsPlaying = true;
			this.m_StartScale = false;
			this.m_PlayTime = 0f;
		}

		// Token: 0x060027C6 RID: 10182 RVA: 0x000D4E4E File Offset: 0x000D324E
		public void PlayOut()
		{
			this.m_Anim.PlayOut();
		}

		// Token: 0x060027C7 RID: 10183 RVA: 0x000D4E5C File Offset: 0x000D325C
		private void Update()
		{
			if (this.m_IsPlaying && this.m_Anim.IsEnd)
			{
				if (!this.m_StartScale)
				{
					this.m_StartScale = true;
					float num = (float)(this.m_Digit / 2);
					if (this.m_Digit % 2 == 0)
					{
						num -= 0.5f;
					}
					for (int i = 0; i < this.m_Digit; i++)
					{
						GameObject gameObject = this.m_NumRects[i].gameObject;
						iTween.Stop(gameObject, "scale");
						iTween.ScaleTo(gameObject, new Hashtable
						{
							{
								"x",
								1f
							},
							{
								"y",
								1f
							},
							{
								"time",
								0.3f
							},
							{
								"delay",
								0.95f - (float)i * (0.2f / (float)this.m_Digit)
							},
							{
								"easeType",
								iTween.EaseType.easeOutQuad
							}
						});
						float width = this.m_NumRects[0].rect.width;
						iTween.Stop(gameObject, "move");
						iTween.MoveTo(gameObject, new Hashtable
						{
							{
								"isLocal",
								true
							},
							{
								"x",
								num * width - width * (float)i
							},
							{
								"y",
								0f
							},
							{
								"time",
								0.3f
							},
							{
								"delay",
								0.95f - (float)i * (0.2f / (float)this.m_Digit)
							},
							{
								"easeType",
								iTween.EaseType.easeOutQuad
							}
						});
					}
					GameObject gameObject2 = this.m_Title.gameObject;
					iTween.Stop(gameObject2, "scale");
					iTween.ScaleTo(gameObject2, new Hashtable
					{
						{
							"x",
							1f
						},
						{
							"y",
							1f
						},
						{
							"time",
							0.3f
						},
						{
							"delay",
							0.75f
						},
						{
							"easeType",
							iTween.EaseType.easeOutQuad
						}
					});
				}
				this.m_PlayTime += Time.deltaTime;
				if (this.m_PlayTime > 1.25f)
				{
					this.m_IsPlaying = false;
				}
			}
		}

		// Token: 0x04002ED5 RID: 11989
		private const float START_SCALE = 1.25f;

		// Token: 0x04002ED6 RID: 11990
		private const float SCALE_START_TIME = 0.75f;

		// Token: 0x04002ED7 RID: 11991
		private const float SCALE_DURATION = 0.3f;

		// Token: 0x04002ED8 RID: 11992
		private const float SCALE_TIME = 0.2f;

		// Token: 0x04002ED9 RID: 11993
		private const float END_TIME = 0f;

		// Token: 0x04002EDA RID: 11994
		[SerializeField]
		private ImageNumbers m_Nums;

		// Token: 0x04002EDB RID: 11995
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04002EDC RID: 11996
		[SerializeField]
		private RectTransform m_Title;

		// Token: 0x04002EDD RID: 11997
		[SerializeField]
		private Image m_BonusImage;

		// Token: 0x04002EDE RID: 11998
		[SerializeField]
		private Sprite[] m_BonusSprites;

		// Token: 0x04002EDF RID: 11999
		private RectTransform[] m_NumRects;

		// Token: 0x04002EE0 RID: 12000
		private int m_Digit;

		// Token: 0x04002EE1 RID: 12001
		private bool m_IsDoneSetup;

		// Token: 0x04002EE2 RID: 12002
		private bool m_IsPlaying;

		// Token: 0x04002EE3 RID: 12003
		private bool m_StartScale;

		// Token: 0x04002EE4 RID: 12004
		private float m_PlayTime;
	}
}
