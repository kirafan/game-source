﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000768 RID: 1896
	public class CharacterStatus : MonoBehaviour
	{
		// Token: 0x06002686 RID: 9862 RVA: 0x000CCD8C File Offset: 0x000CB18C
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			if (this.m_ChargeParent != null)
			{
				this.m_ChargeCounts = this.m_ChargeParent.GetComponentsInChildren<ChargeCountSignal>();
			}
			this.m_AbnormalIcon.Setup();
			this.m_BuffIcon.Setup();
			this.SetEnableRender(false, false);
		}

		// Token: 0x06002687 RID: 9863 RVA: 0x000CCDF4 File Offset: 0x000CB1F4
		private void Update()
		{
			if (this.m_IsDirtyHP)
			{
				if (this.m_HPGauge.fillAmount < this.m_HP)
				{
					this.m_HPGauge.fillAmount += 0.05f;
					if (this.m_HPGauge.fillAmount > this.m_HP)
					{
						this.m_HPGauge.fillAmount = this.m_HP;
					}
				}
				else
				{
					this.m_HPGauge.fillAmount -= 0.05f;
					if (this.m_HPGauge.fillAmount < this.m_HP)
					{
						this.m_HPGauge.fillAmount = this.m_HP;
					}
				}
				if (this.m_BGHPGauge.fillAmount < this.m_HPGauge.fillAmount)
				{
					this.m_BGHPGauge.fillAmount += 0.02f;
					if (this.m_BGHPGauge.fillAmount > this.m_HPGauge.fillAmount)
					{
						this.m_BGHPGauge.fillAmount = this.m_HPGauge.fillAmount;
					}
				}
				else
				{
					this.m_BGHPGauge.fillAmount -= 0.02f;
					if (this.m_BGHPGauge.fillAmount < this.m_HPGauge.fillAmount)
					{
						this.m_BGHPGauge.fillAmount = this.m_HPGauge.fillAmount;
					}
				}
				if (this.m_HPGauge.fillAmount == this.m_HP && this.m_BGHPGauge.fillAmount == this.m_HPGauge.fillAmount)
				{
					this.m_IsDirtyHP = false;
				}
			}
			if (this.m_IsDirtyStun)
			{
				if (this.m_StunGauge.fillAmount < this.m_Stun)
				{
					this.m_StunGauge.fillAmount += 0.05f;
					if (this.m_StunGauge.fillAmount > this.m_Stun)
					{
						this.m_StunGauge.fillAmount = this.m_Stun;
					}
				}
				else
				{
					this.m_StunGauge.fillAmount -= 0.05f;
					if (this.m_StunGauge.fillAmount < this.m_Stun)
					{
						this.m_StunGauge.fillAmount = this.m_Stun;
					}
				}
				if (this.m_StunGauge.fillAmount == this.m_Stun)
				{
					this.m_IsDirtyStun = false;
				}
			}
		}

		// Token: 0x06002688 RID: 9864 RVA: 0x000CD04C File Offset: 0x000CB44C
		public void SetHp(int hp, int maxhp, bool isMoveBar = false)
		{
			this.m_HP = (float)hp / (float)maxhp;
			if (this.m_HPText != null)
			{
				this.m_HPText.text = hp.ToString();
			}
			if (!isMoveBar)
			{
				this.m_HPGauge.fillAmount = this.m_HP;
				this.m_BGHPGauge.fillAmount = this.m_HPGauge.fillAmount;
			}
			this.m_IsDirtyHP = true;
		}

		// Token: 0x06002689 RID: 9865 RVA: 0x000CD0C4 File Offset: 0x000CB4C4
		public void SetStun(float ratio, bool isMoveBar = false)
		{
			if (ratio >= 1f && this.m_Stun < ratio)
			{
				this.m_StunEffectAnim.PlayIn();
			}
			this.m_Stun = ratio;
			if (this.m_Stun >= 1f)
			{
				this.m_StunBlinkColor.enabled = true;
			}
			else
			{
				this.m_StunBlinkColor.enabled = false;
				this.m_StunBlinkColor.Reset();
			}
			if (!isMoveBar)
			{
				this.m_StunGauge.fillAmount = ratio;
			}
			this.m_IsDirtyStun = true;
		}

		// Token: 0x0600268A RID: 9866 RVA: 0x000CD14C File Offset: 0x000CB54C
		public void SetCharge(int charge, int max)
		{
			if (this.m_ChargeCounts == null)
			{
				return;
			}
			for (int i = 0; i < this.m_ChargeCounts.Length; i++)
			{
				if (charge == max && i < charge)
				{
					this.m_ChargeCounts[i].Apply(ChargeCountSignal.eState.Effect);
				}
				else if (i < charge)
				{
					this.m_ChargeCounts[i].Apply(ChargeCountSignal.eState.Active);
				}
				else if (i < max)
				{
					this.m_ChargeCounts[i].Apply(ChargeCountSignal.eState.Empty);
				}
				else
				{
					this.m_ChargeCounts[i].Apply(ChargeCountSignal.eState.None);
				}
			}
		}

		// Token: 0x0600268B RID: 9867 RVA: 0x000CD1E1 File Offset: 0x000CB5E1
		public void SetElementType(eElementType element)
		{
			this.m_ElementIcon.Apply(element);
		}

		// Token: 0x0600268C RID: 9868 RVA: 0x000CD1F0 File Offset: 0x000CB5F0
		public void SetIcons(sbyte[] flags)
		{
			for (int i = 0; i < flags.Length; i++)
			{
				bool flg = (int)flags[i] != 0;
				if (i != 0)
				{
					eStateIconType eStateIconType = StateIcon.ConvertUIStatusIconToType((BattleDefine.eFlagForUIStatusIcon)i, flags[i], true);
					if (eStateIconType >= eStateIconType.Confusion && eStateIconType <= eStateIconType.Isolation)
					{
						this.m_AbnormalIcon.SetIcon(eStateIconType, flg);
					}
					else
					{
						this.m_BuffIcon.SetIcon(eStateIconType, flg);
						eStateIconType type = StateIcon.ConvertUIStatusIconToType((BattleDefine.eFlagForUIStatusIcon)i, flags[i], false);
						this.m_BuffIcon.SetIcon(type, flg);
					}
				}
			}
		}

		// Token: 0x0600268D RID: 9869 RVA: 0x000CD278 File Offset: 0x000CB678
		public void SetEnableRender(bool flag, bool immediate = false)
		{
			if (immediate)
			{
				if (this.m_GameObject != null)
				{
					this.m_GameObject.SetActive(flag);
				}
			}
			else if (flag)
			{
				this.m_Anim.PlayIn();
			}
			else if (!flag)
			{
				this.m_Anim.PlayOut();
			}
		}

		// Token: 0x0600268E RID: 9870 RVA: 0x000CD2D4 File Offset: 0x000CB6D4
		public void SetAnchoredPosition(Vector2 position)
		{
			this.m_RectTransform.anchoredPosition = position;
		}

		// Token: 0x04002CF9 RID: 11513
		private const float HPSPEED = 0.05f;

		// Token: 0x04002CFA RID: 11514
		private const float BGHPSPEED = 0.02f;

		// Token: 0x04002CFB RID: 11515
		private const float STUNSPEED = 0.05f;

		// Token: 0x04002CFC RID: 11516
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04002CFD RID: 11517
		[SerializeField]
		private Image m_BGHPGauge;

		// Token: 0x04002CFE RID: 11518
		[SerializeField]
		private Image m_HPGauge;

		// Token: 0x04002CFF RID: 11519
		[SerializeField]
		private Text m_HPText;

		// Token: 0x04002D00 RID: 11520
		[SerializeField]
		private GameObject m_ChargeParent;

		// Token: 0x04002D01 RID: 11521
		private ChargeCountSignal[] m_ChargeCounts;

		// Token: 0x04002D02 RID: 11522
		[SerializeField]
		private Image m_StunGauge;

		// Token: 0x04002D03 RID: 11523
		[SerializeField]
		private AnimUIPlayer m_StunEffectAnim;

		// Token: 0x04002D04 RID: 11524
		[SerializeField]
		private BlinkColor m_StunBlinkColor;

		// Token: 0x04002D05 RID: 11525
		[SerializeField]
		private BuffIcon m_BuffIcon;

		// Token: 0x04002D06 RID: 11526
		[SerializeField]
		private BuffIcon m_AbnormalIcon;

		// Token: 0x04002D07 RID: 11527
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x04002D08 RID: 11528
		private float m_HP = 1f;

		// Token: 0x04002D09 RID: 11529
		private bool m_IsDirtyHP = true;

		// Token: 0x04002D0A RID: 11530
		private float m_Stun;

		// Token: 0x04002D0B RID: 11531
		private bool m_IsDirtyStun = true;

		// Token: 0x04002D0C RID: 11532
		private GameObject m_GameObject;

		// Token: 0x04002D0D RID: 11533
		private RectTransform m_RectTransform;
	}
}
