﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000788 RID: 1928
	public class RandomStar : MonoBehaviour
	{
		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06002752 RID: 10066 RVA: 0x000D1998 File Offset: 0x000CFD98
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06002753 RID: 10067 RVA: 0x000D19A0 File Offset: 0x000CFDA0
		// (set) Token: 0x06002754 RID: 10068 RVA: 0x000D19A8 File Offset: 0x000CFDA8
		public bool Loop
		{
			get
			{
				return this.m_Loop;
			}
			set
			{
				this.m_Loop = value;
			}
		}

		// Token: 0x06002755 RID: 10069 RVA: 0x000D19B4 File Offset: 0x000CFDB4
		public void Setup(RandomStar.Status status)
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_CanvasGroup = this.m_GameObject.GetComponent<CanvasGroup>();
			this.m_Image = this.m_GameObject.GetComponent<Image>();
			this.m_Status = status;
		}

		// Token: 0x06002756 RID: 10070 RVA: 0x000D1A08 File Offset: 0x000CFE08
		public void Play()
		{
			float f = UnityEngine.Random.Range(0f, 6.2831855f);
			float num = UnityEngine.Random.Range(this.m_Status.m_StartDistanceMin, this.m_Status.m_StartDistanceMax);
			float num2 = UnityEngine.Random.Range(this.m_Status.m_MoveMin, this.m_Status.m_MoveMax);
			Vector2 v = new Vector2(Mathf.Cos(f) * num, Mathf.Sin(f) * num);
			Vector2 vector = new Vector2(Mathf.Cos(f) * (num + num2), Mathf.Sin(f) * (num + num2));
			this.m_RectTransform.localEulerAngles = new Vector3(0f, 0f, UnityEngine.Random.Range(0f, 360f));
			float num3 = UnityEngine.Random.Range(1f, 2f);
			this.m_RectTransform.localScale = new Vector3(num3, num3, 1f);
			int num4 = UnityEngine.Random.Range(0, 3);
			if (num4 != 0)
			{
				if (num4 != 1)
				{
					if (num4 == 2)
					{
						this.m_Image.color = new Color(0f, 0f, 1f, 1f);
					}
				}
				else
				{
					this.m_Image.color = new Color(0f, 1f, 0f, 1f);
				}
			}
			else
			{
				this.m_Image.color = new Color(1f, 0f, 0f, 1f);
			}
			this.m_RectTransform.localPosition = v;
			iTween.Stop(this.m_GameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", this.m_Status.m_MoveSec);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", this.m_Status.m_Delay + 0.001f);
			iTween.MoveTo(this.m_GameObject, hashtable);
			this.m_CanvasGroup.alpha = 0f;
			iTween.Stop(this.m_GameObject, "value");
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("from", this.m_CanvasGroup.alpha);
			hashtable2.Add("to", 1f);
			hashtable2.Add("time", this.m_Status.m_MoveSec * 0.5f);
			hashtable2.Add("delay", this.m_Status.m_Delay + 0.001f);
			hashtable2.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable2.Add("onupdate", "OnUpdateAlpha");
			hashtable2.Add("onupdatetarget", this.m_GameObject);
			hashtable2.Add("oncomplete", "OnCompleteInAlpha");
			hashtable2.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable2);
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x000D1D4B File Offset: 0x000D014B
		public void OnUpdateAlpha(float value)
		{
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x06002758 RID: 10072 RVA: 0x000D1D5C File Offset: 0x000D015C
		public void OnCompleteInAlpha()
		{
			this.m_CanvasGroup.alpha = 1f;
			iTween.Stop(this.m_GameObject, "value");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", this.m_CanvasGroup.alpha);
			hashtable.Add("to", 0f);
			hashtable.Add("time", this.m_Status.m_MoveSec * 0.5f);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "OnCompleteOutAlpha");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002759 RID: 10073 RVA: 0x000D1E54 File Offset: 0x000D0254
		public void OnCompleteOutAlpha()
		{
			if (this.m_Loop)
			{
				this.Play();
			}
		}

		// Token: 0x04002E21 RID: 11809
		private RandomStar.Status m_Status;

		// Token: 0x04002E22 RID: 11810
		private bool m_Loop;

		// Token: 0x04002E23 RID: 11811
		private GameObject m_GameObject;

		// Token: 0x04002E24 RID: 11812
		private RectTransform m_RectTransform;

		// Token: 0x04002E25 RID: 11813
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04002E26 RID: 11814
		private Image m_Image;

		// Token: 0x02000789 RID: 1929
		public class Status
		{
			// Token: 0x04002E27 RID: 11815
			public float m_StartDistanceMin = 2f;

			// Token: 0x04002E28 RID: 11816
			public float m_StartDistanceMax = 64f;

			// Token: 0x04002E29 RID: 11817
			public float m_MoveMin = 32f;

			// Token: 0x04002E2A RID: 11818
			public float m_MoveMax = 128f;

			// Token: 0x04002E2B RID: 11819
			public float m_MoveSec = 1f;

			// Token: 0x04002E2C RID: 11820
			public float m_Delay;
		}
	}
}
