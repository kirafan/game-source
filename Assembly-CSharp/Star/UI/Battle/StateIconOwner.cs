﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200078F RID: 1935
	public class StateIconOwner : MonoBehaviour
	{
		// Token: 0x0600276F RID: 10095 RVA: 0x000D2C28 File Offset: 0x000D1028
		public void Setup()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			for (int i = 0; i < this.m_DefaultCacheSize; i++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate<StateIcon>(this.m_IconPrefab, this.m_RectTransform, false).gameObject;
				gameObject.SetActive(false);
				gameObject.GetComponent<StateIcon>().Setup(this);
				this.m_EmptyList.Add(gameObject);
			}
			this.m_DictionaryTypeToIdx.Clear();
			for (int j = 0; j < this.m_StateIconDatas.Length; j++)
			{
				this.m_DictionaryTypeToIdx.Add(this.m_StateIconDatas[j].m_Type, j);
				for (int k = 0; k < this.m_IconSprites.Length; k++)
				{
					if (this.m_IconSprites[k].name == this.m_StateIconDatas[j].m_SpriteName)
					{
						this.m_StateIconDatas[j].m_Sprite = this.m_IconSprites[k];
					}
				}
			}
		}

		// Token: 0x06002770 RID: 10096 RVA: 0x000D2D20 File Offset: 0x000D1120
		public GameObject ScoopEmptyIcon()
		{
			GameObject gameObject;
			if (this.m_EmptyList.Count > 0)
			{
				gameObject = this.m_EmptyList[0];
				this.m_EmptyList.RemoveAt(0);
			}
			else
			{
				gameObject = UnityEngine.Object.Instantiate<StateIcon>(this.m_IconPrefab, this.m_RectTransform, false).gameObject;
				gameObject.SetActive(false);
				gameObject.GetComponent<StateIcon>().Setup(this);
			}
			this.m_ActiveList.Add(gameObject);
			gameObject.SetActive(false);
			return gameObject;
		}

		// Token: 0x06002771 RID: 10097 RVA: 0x000D2D9D File Offset: 0x000D119D
		public void SinkEmptyIcon(StateIcon obj)
		{
			this.SinkEmptyIcon(obj.gameObject);
		}

		// Token: 0x06002772 RID: 10098 RVA: 0x000D2DAC File Offset: 0x000D11AC
		public void SinkEmptyIcon(GameObject obj)
		{
			obj.gameObject.SetActive(false);
			RectTransform component = obj.GetComponent<RectTransform>();
			component.SetParent(this.m_RectTransform);
			component.localPosition = Vector2.zero;
			component.localScale = Vector2.one;
			this.m_ActiveList.Remove(obj);
			this.m_EmptyList.Add(obj);
		}

		// Token: 0x06002773 RID: 10099 RVA: 0x000D2E14 File Offset: 0x000D1214
		public void Destroy()
		{
			for (int i = 0; i < this.m_EmptyList.Count; i++)
			{
				UnityEngine.Object.Destroy(this.m_EmptyList[i]);
			}
			for (int j = 0; j < this.m_ActiveList.Count; j++)
			{
				UnityEngine.Object.Destroy(this.m_ActiveList[j]);
			}
		}

		// Token: 0x06002774 RID: 10100 RVA: 0x000D2E7C File Offset: 0x000D127C
		public StateIconOwner.StateIconData GetIconData(eStateIconType type)
		{
			int num;
			if (this.m_DictionaryTypeToIdx.TryGetValue(type, out num))
			{
				return this.m_StateIconDatas[num];
			}
			return new StateIconOwner.StateIconData(eStateIconType.None, null, StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalConfusion);
		}

		// Token: 0x06002775 RID: 10101 RVA: 0x000D2EB4 File Offset: 0x000D12B4
		public Sprite GetArrowSprite()
		{
			return this.m_ArrowSprite;
		}

		// Token: 0x04002E57 RID: 11863
		[SerializeField]
		private StateIcon m_IconPrefab;

		// Token: 0x04002E58 RID: 11864
		[SerializeField]
		private int m_DefaultCacheSize;

		// Token: 0x04002E59 RID: 11865
		[SerializeField]
		private Sprite m_ArrowSprite;

		// Token: 0x04002E5A RID: 11866
		private StateIconOwner.StateIconData[] m_StateIconDatas = new StateIconOwner.StateIconData[]
		{
			new StateIconOwner.StateIconData(eStateIconType.Confusion, "Abnormal_Confusion", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalConfusion),
			new StateIconOwner.StateIconData(eStateIconType.Paralysis, "Abnormal_Paralysis", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalParalysis),
			new StateIconOwner.StateIconData(eStateIconType.Poison, "Abnormal_Poison", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalPoison),
			new StateIconOwner.StateIconData(eStateIconType.Bearish, "Abnormal_Bearish", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalBearish),
			new StateIconOwner.StateIconData(eStateIconType.Sleep, "Abnormal_Sleep", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalSleep),
			new StateIconOwner.StateIconData(eStateIconType.Unhappy, "Abnormal_Unhappy", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalUnhappy),
			new StateIconOwner.StateIconData(eStateIconType.Silence, "Abnormal_Silence", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalSilence),
			new StateIconOwner.StateIconData(eStateIconType.Isolation, "Abnormal_Isolation", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.StateAbnormalIsolation),
			new StateIconOwner.StateIconData(eStateIconType.StatusAtkUp, "Buff_Atk", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffAtkUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusMgcUp, "Buff_Mgc", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffMgcUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusDefUp, "Buff_Def", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffDefUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusMDefUp, "Buff_MDef", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffMDefUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusSpdUp, "Buff_Spd", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffSpdUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusLuckUp, "Buff_Luck", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.Up, eText_CommonDB.BuffLuckUp),
			new StateIconOwner.StateIconData(eStateIconType.StatusAtkDown, "Buff_Atk", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffAtkDown),
			new StateIconOwner.StateIconData(eStateIconType.StatusMgcDown, "Buff_Mgc", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffMgcDown),
			new StateIconOwner.StateIconData(eStateIconType.StatusDefDown, "Buff_Def", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffDefDown),
			new StateIconOwner.StateIconData(eStateIconType.StatusMDefDown, "Buff_MDef", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffMDefDown),
			new StateIconOwner.StateIconData(eStateIconType.StatusSpdDown, "Buff_Spd", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffSpdDown),
			new StateIconOwner.StateIconData(eStateIconType.StatusLuckDown, "Buff_Luck", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.Down, eText_CommonDB.BuffLuckDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementFireUp, "Buff_Fire", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementFireUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementWindUp, "Buff_Wind", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementWindUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementEarthUp, "Buff_Earth", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementEarthUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementWaterUp, "Buff_Water", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementWaterUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementMoonUp, "Buff_Moon", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementMoonUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementSunUp, "Buff_Sun", StateIcon.eSubIconType.UpArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementSunUp),
			new StateIconOwner.StateIconData(eStateIconType.ElementFireDown, "Buff_Fire", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementFireDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementWindDown, "Buff_Wind", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementWindDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementEarthDown, "Buff_Earth", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementEarthDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementWaterDown, "Buff_Water", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementWaterDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementMoonDown, "Buff_Moon", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementMoonDown),
			new StateIconOwner.StateIconData(eStateIconType.ElementSunDown, "Buff_Sun", StateIcon.eSubIconType.DownArrow, false, StateIcon.eColorChange.None, eText_CommonDB.BuffElementSunDown),
			new StateIconOwner.StateIconData(eStateIconType.Regene, "Abnormal_Recovery", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffRegene),
			new StateIconOwner.StateIconData(eStateIconType.StateAbnormalDisable, "Buff_AbnormalResist", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffStateAbnormalDisable),
			new StateIconOwner.StateIconData(eStateIconType.StateAbnormalAdditionalProbabilityUp, "Buff_AbnormalProbabilityUp", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffStateAbnormalAdditionalProbabilityUp),
			new StateIconOwner.StateIconData(eStateIconType.StateAbnormalAdditionalProbabilityDown, "Buff_AbnormalProbabilityDown", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffStateAbnormalAdditionalProbabilityDown),
			new StateIconOwner.StateIconData(eStateIconType.WeakElementBonus, "Buff_WeakElementBonus", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffWeakElementBonus),
			new StateIconOwner.StateIconData(eStateIconType.NextAtkUp, "Buff_Atk", StateIcon.eSubIconType.UpArrow, true, StateIcon.eColorChange.Up, eText_CommonDB.BuffNextAtkUp),
			new StateIconOwner.StateIconData(eStateIconType.NextMgcUp, "Buff_Mgc", StateIcon.eSubIconType.UpArrow, true, StateIcon.eColorChange.Up, eText_CommonDB.BuffNextMgcUp),
			new StateIconOwner.StateIconData(eStateIconType.NextCritical, "Buff_Luck", StateIcon.eSubIconType.UpArrow, true, StateIcon.eColorChange.Up, eText_CommonDB.BuffNextCritical),
			new StateIconOwner.StateIconData(eStateIconType.Barrier, "Buff_Barrier", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffBarrier),
			new StateIconOwner.StateIconData(eStateIconType.ChainBonusUp, "Buff_ChainBonusUp", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffChainBonusUp),
			new StateIconOwner.StateIconData(eStateIconType.HateUp, "Buff_HateUp", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffHateUp),
			new StateIconOwner.StateIconData(eStateIconType.HateDown, "Buff_HateDown", StateIcon.eSubIconType.None, false, StateIcon.eColorChange.None, eText_CommonDB.BuffHateDown)
		};

		// Token: 0x04002E5B RID: 11867
		[SerializeField]
		private Sprite[] m_IconSprites;

		// Token: 0x04002E5C RID: 11868
		public Dictionary<eStateIconType, int> m_DictionaryTypeToIdx = new Dictionary<eStateIconType, int>();

		// Token: 0x04002E5D RID: 11869
		private List<GameObject> m_EmptyList = new List<GameObject>();

		// Token: 0x04002E5E RID: 11870
		private List<GameObject> m_ActiveList = new List<GameObject>();

		// Token: 0x04002E5F RID: 11871
		private RectTransform m_RectTransform;

		// Token: 0x02000790 RID: 1936
		public class StateIconData
		{
			// Token: 0x06002776 RID: 10102 RVA: 0x000D2EBC File Offset: 0x000D12BC
			public StateIconData(eStateIconType type, string spriteName, StateIcon.eSubIconType subIconType, bool existNextImage, StateIcon.eColorChange colorChangeType, eText_CommonDB textID)
			{
				this.m_Type = type;
				this.m_SpriteName = spriteName;
				this.m_SubIconType = subIconType;
				this.m_IsNextImage = existNextImage;
				this.m_ColorChangeType = colorChangeType;
				this.m_DescriptTextID = textID;
			}

			// Token: 0x04002E60 RID: 11872
			public eStateIconType m_Type;

			// Token: 0x04002E61 RID: 11873
			public Sprite m_Sprite;

			// Token: 0x04002E62 RID: 11874
			public string m_SpriteName;

			// Token: 0x04002E63 RID: 11875
			public StateIcon.eSubIconType m_SubIconType;

			// Token: 0x04002E64 RID: 11876
			public bool m_IsNextImage;

			// Token: 0x04002E65 RID: 11877
			public StateIcon.eColorChange m_ColorChangeType;

			// Token: 0x04002E66 RID: 11878
			public eText_CommonDB m_DescriptTextID = eText_CommonDB.StateAbnormalConfusion;
		}
	}
}
