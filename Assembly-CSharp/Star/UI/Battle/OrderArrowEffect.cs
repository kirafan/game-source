﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000781 RID: 1921
	public class OrderArrowEffect : MonoBehaviour
	{
		// Token: 0x06002721 RID: 10017 RVA: 0x000D0748 File Offset: 0x000CEB48
		private void Start()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_TargetRects[0].position = this.m_RectTransform.position;
			this.m_TargetRects[0].localPosition = this.m_TargetRects[0].localPosition + new Vector3(this.m_TargetRects[0].rect.width * 0.5f + this.m_TargetRects[0].rect.width, 0f);
			for (int i = 1; i < this.m_TargetRects.Length; i++)
			{
				this.m_TargetRects[i].position = this.m_TargetRects[0].position;
			}
		}

		// Token: 0x06002722 RID: 10018 RVA: 0x000D0808 File Offset: 0x000CEC08
		private void LateUpdate()
		{
			this.m_TimeCount += Time.deltaTime;
			while (this.m_TimeCount > 1f)
			{
				this.m_TimeCount -= 1f;
			}
			this.m_TargetRects[0].position = this.m_RectTransform.position;
			this.m_TargetRects[0].localPosition = this.m_TargetRects[0].localPosition + new Vector3(this.m_TargetRects[0].rect.width * 0.5f + -this.m_TimeCount / 1f * (this.m_RectTransform.rect.width + this.m_TargetRects[0].rect.width), 0f);
			for (int i = 1; i < this.m_TargetRects.Length; i++)
			{
				this.m_TargetRects[i].position = this.m_TargetRects[0].position;
			}
		}

		// Token: 0x04002DDF RID: 11743
		protected const float DURATION = 1f;

		// Token: 0x04002DE0 RID: 11744
		[SerializeField]
		protected RectTransform[] m_TargetRects;

		// Token: 0x04002DE1 RID: 11745
		protected float m_TimeCount;

		// Token: 0x04002DE2 RID: 11746
		protected RectTransform m_RectTransform;
	}
}
