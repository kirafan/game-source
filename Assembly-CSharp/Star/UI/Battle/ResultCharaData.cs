﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020007A3 RID: 1955
	public class ResultCharaData : MonoBehaviour
	{
		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x060027EF RID: 10223 RVA: 0x000D5FBE File Offset: 0x000D43BE
		public bool IsExist
		{
			get
			{
				return this.m_IsExist;
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x060027F0 RID: 10224 RVA: 0x000D5FC6 File Offset: 0x000D43C6
		public ResultCharaData.eStep Step
		{
			get
			{
				return this.m_Step;
			}
		}

		// Token: 0x060027F1 RID: 10225 RVA: 0x000D5FCE File Offset: 0x000D43CE
		public void Setup()
		{
			this.m_EXPGauge.Setup();
			this.m_Exist.SetActive(false);
			this.m_NoExist.SetActive(true);
		}

		// Token: 0x060027F2 RID: 10226 RVA: 0x000D5FF4 File Offset: 0x000D43F4
		private void Update()
		{
			switch (this.m_Step)
			{
			case ResultCharaData.eStep.ExpIn:
				if (this.m_FriendshipWindowAnim.State == 2)
				{
					this.m_FriendshipWindowAnim.PlayOut();
				}
				if (this.m_ExpWindowAnim.State == 0)
				{
					this.m_ExpWindowAnim.PlayIn();
				}
				else if (this.m_ExpWindowAnim.State == 2)
				{
					this.m_EXPGauge.Play(false);
					this.m_Step = ResultCharaData.eStep.Exp;
				}
				break;
			case ResultCharaData.eStep.Exp:
				if (!this.m_EXPGauge.IsPlaying)
				{
					this.m_Step = ResultCharaData.eStep.ExpStop;
				}
				break;
			case ResultCharaData.eStep.FriendshipIn:
				this.m_EXPGauge.HidePopUp();
				if (this.m_ExpWindowAnim.State == 2)
				{
					this.m_ExpWindowAnim.PlayOut();
				}
				if (this.m_FriendshipWindowAnim.State == 0)
				{
					this.m_FriendshipWindowAnim.PlayIn();
				}
				else if (this.m_FriendshipWindowAnim.State == 2)
				{
					this.m_FriendshipGauge.Play(this.m_Friendship, this.m_FriendshipExp, this.m_AfterFrendship, this.m_AfterFriendshipExp, this.m_FriendshipExpTableID);
					this.m_Step = ResultCharaData.eStep.Friendship;
				}
				break;
			case ResultCharaData.eStep.Friendship:
				if (!this.m_FriendshipGauge.IsPlaying())
				{
					this.m_Step = ResultCharaData.eStep.FriendshipStop;
				}
				break;
			}
			this.m_TimeCount += Time.deltaTime;
		}

		// Token: 0x060027F3 RID: 10227 RVA: 0x000D616C File Offset: 0x000D456C
		public void SetData(int limitBreak)
		{
			this.m_LBIcon.SetValue(limitBreak);
			this.m_Exist.SetActive(true);
			this.m_NoExist.SetActive(false);
			this.m_IsExist = true;
		}

		// Token: 0x060027F4 RID: 10228 RVA: 0x000D6199 File Offset: 0x000D4599
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExps)
		{
			this.m_EXPGauge.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExps);
		}

		// Token: 0x060027F5 RID: 10229 RVA: 0x000D61B1 File Offset: 0x000D45B1
		public void PlayExp()
		{
			this.m_Step = ResultCharaData.eStep.ExpIn;
		}

		// Token: 0x060027F6 RID: 10230 RVA: 0x000D61BA File Offset: 0x000D45BA
		public void SetFriendshipData(int beforeLevel, long beforeExp, int afterLevel, long afterExp, sbyte expTableID)
		{
			this.m_Friendship = beforeLevel;
			this.m_FriendshipExp = beforeExp;
			this.m_AfterFrendship = afterLevel;
			this.m_AfterFriendshipExp = afterExp;
			this.m_FriendshipExpTableID = expTableID;
			this.m_FriendshipGauge.Apply(beforeLevel, beforeExp, expTableID);
		}

		// Token: 0x060027F7 RID: 10231 RVA: 0x000D61F0 File Offset: 0x000D45F0
		public void PlayFriendship()
		{
			this.m_Step = ResultCharaData.eStep.FriendshipIn;
		}

		// Token: 0x060027F8 RID: 10232 RVA: 0x000D61F9 File Offset: 0x000D45F9
		public void SkipExp()
		{
			if (!this.m_IsExist)
			{
				return;
			}
			this.m_EXPGauge.SkipUpdateExp();
		}

		// Token: 0x060027F9 RID: 10233 RVA: 0x000D6212 File Offset: 0x000D4612
		public void SkipFriendship()
		{
			if (!this.m_IsExist)
			{
				return;
			}
			this.m_FriendshipGauge.Skip();
		}

		// Token: 0x060027FA RID: 10234 RVA: 0x000D622C File Offset: 0x000D462C
		public void SetEnableRenderLevelUpPop()
		{
			for (int i = 0; i < this.m_EXPGauge.GetLevelUpPops().Length; i++)
			{
				this.m_EXPGauge.GetLevelUpPops()[i].gameObject.SetActive(false);
			}
			this.m_FriendshipGauge.HidePop();
		}

		// Token: 0x04002F1A RID: 12058
		[SerializeField]
		private AnimUIPlayer m_ExpWindowAnim;

		// Token: 0x04002F1B RID: 12059
		[SerializeField]
		private AnimUIPlayer m_FriendshipWindowAnim;

		// Token: 0x04002F1C RID: 12060
		[SerializeField]
		private LimitBreakIcon m_LBIcon;

		// Token: 0x04002F1D RID: 12061
		private bool m_IsExist;

		// Token: 0x04002F1E RID: 12062
		[SerializeField]
		private ExpGauge m_EXPGauge;

		// Token: 0x04002F1F RID: 12063
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x04002F20 RID: 12064
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x04002F21 RID: 12065
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x04002F22 RID: 12066
		private ResultCharaData.eStep m_Step;

		// Token: 0x04002F23 RID: 12067
		private float m_TimeCount;

		// Token: 0x04002F24 RID: 12068
		private int m_Friendship;

		// Token: 0x04002F25 RID: 12069
		private long m_FriendshipExp;

		// Token: 0x04002F26 RID: 12070
		private int m_AfterFrendship;

		// Token: 0x04002F27 RID: 12071
		private long m_AfterFriendshipExp;

		// Token: 0x04002F28 RID: 12072
		private sbyte m_FriendshipExpTableID;

		// Token: 0x020007A4 RID: 1956
		public enum eStep
		{
			// Token: 0x04002F2A RID: 12074
			None,
			// Token: 0x04002F2B RID: 12075
			ExpIn,
			// Token: 0x04002F2C RID: 12076
			Exp,
			// Token: 0x04002F2D RID: 12077
			ExpStop,
			// Token: 0x04002F2E RID: 12078
			FriendshipIn,
			// Token: 0x04002F2F RID: 12079
			Friendship,
			// Token: 0x04002F30 RID: 12080
			FriendshipStop
		}
	}
}
