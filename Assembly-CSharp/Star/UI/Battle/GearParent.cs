﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000776 RID: 1910
	public class GearParent : MonoBehaviour
	{
		// Token: 0x060026E0 RID: 9952 RVA: 0x000CF158 File Offset: 0x000CD558
		private void Start()
		{
			this.m_Gears = base.GetComponentsInChildren<RectTransform>();
		}

		// Token: 0x060026E1 RID: 9953 RVA: 0x000CF168 File Offset: 0x000CD568
		private void Update()
		{
			if (this.m_Speed != 0f)
			{
				for (int i = 0; i < this.m_Gears.Length; i++)
				{
					if (i != 0)
					{
						this.m_Gears[i].localEulerAngles = new Vector3(0f, 0f, this.m_Gears[i].localEulerAngles.z + this.m_Speed * (float)(i % 2 * 2 - 1) * Time.deltaTime);
					}
				}
			}
		}

		// Token: 0x060026E2 RID: 9954 RVA: 0x000CF1F0 File Offset: 0x000CD5F0
		public void SetSpeed(float speed = -1f, bool reverse = false)
		{
			float num;
			if (speed < 0f)
			{
				num = 200f;
			}
			else
			{
				num = speed;
			}
			if (reverse)
			{
				num = -num;
			}
			this.m_Speed = num;
		}

		// Token: 0x060026E3 RID: 9955 RVA: 0x000CF227 File Offset: 0x000CD627
		public void Stop()
		{
			this.SetSpeed(0f, false);
		}

		// Token: 0x04002D8D RID: 11661
		public const float DEFAULT_SPEED = 200f;

		// Token: 0x04002D8E RID: 11662
		private RectTransform[] m_Gears;

		// Token: 0x04002D8F RID: 11663
		private float m_Speed;
	}
}
