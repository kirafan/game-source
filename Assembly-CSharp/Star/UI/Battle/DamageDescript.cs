﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000771 RID: 1905
	public class DamageDescript : MonoBehaviour
	{
		// Token: 0x060026CE RID: 9934 RVA: 0x000CE85C File Offset: 0x000CCC5C
		public void Apply(DamageDescript.eType type)
		{
			if (type == DamageDescript.eType.None)
			{
				this.m_Image.enabled = false;
				return;
			}
			this.m_Image.enabled = true;
			switch (type)
			{
			case DamageDescript.eType.Critical:
				this.m_Image.sprite = this.m_SpriteCritical;
				break;
			case DamageDescript.eType.Resist:
				this.m_Image.sprite = this.m_SpriteResist;
				break;
			case DamageDescript.eType.Weak:
				this.m_Image.sprite = this.m_SpriteWeak;
				break;
			case DamageDescript.eType.Recovery:
				this.m_Image.sprite = this.m_SpriteRecovery;
				break;
			case DamageDescript.eType.Unhappy:
				this.m_Image.sprite = this.m_SpriteUnhappy;
				break;
			case DamageDescript.eType.Poison:
				this.m_Image.sprite = this.m_SpritePoison;
				break;
			}
		}

		// Token: 0x04002D5C RID: 11612
		[SerializeField]
		private Sprite m_SpriteCritical;

		// Token: 0x04002D5D RID: 11613
		[SerializeField]
		private Sprite m_SpriteResist;

		// Token: 0x04002D5E RID: 11614
		[SerializeField]
		private Sprite m_SpriteWeak;

		// Token: 0x04002D5F RID: 11615
		[SerializeField]
		private Sprite m_SpriteRecovery;

		// Token: 0x04002D60 RID: 11616
		[SerializeField]
		private Sprite m_SpriteUnhappy;

		// Token: 0x04002D61 RID: 11617
		[SerializeField]
		private Sprite m_SpritePoison;

		// Token: 0x04002D62 RID: 11618
		[SerializeField]
		private Image m_Image;

		// Token: 0x02000772 RID: 1906
		public enum eType
		{
			// Token: 0x04002D64 RID: 11620
			None,
			// Token: 0x04002D65 RID: 11621
			Critical,
			// Token: 0x04002D66 RID: 11622
			Resist,
			// Token: 0x04002D67 RID: 11623
			Weak,
			// Token: 0x04002D68 RID: 11624
			Recovery,
			// Token: 0x04002D69 RID: 11625
			Unhappy,
			// Token: 0x04002D6A RID: 11626
			Poison
		}
	}
}
