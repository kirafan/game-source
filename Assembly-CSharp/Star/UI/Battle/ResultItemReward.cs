﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007A9 RID: 1961
	public class ResultItemReward : UIGroup
	{
		// Token: 0x0600280E RID: 10254 RVA: 0x000D6C09 File Offset: 0x000D5009
		public void Prepare()
		{
			this.m_ItemNum = 0;
			this.m_IsDirty = true;
		}

		// Token: 0x0600280F RID: 10255 RVA: 0x000D6C19 File Offset: 0x000D5019
		public void SetGoldData(long get, long have)
		{
			this.m_GetMoneyTextObj.text = "+" + UIUtility.GoldValueToString(get, true);
			this.m_HaveMoneyTextObj.text = UIUtility.GoldValueToString(have, true);
		}

		// Token: 0x06002810 RID: 10256 RVA: 0x000D6C4C File Offset: 0x000D504C
		public void AddItemData(int id, int num)
		{
			ResultItemReward.ItemData item = default(ResultItemReward.ItemData);
			item.m_Id = id;
			item.m_Num = num;
			this.m_DataList.Add(item);
			this.m_ItemNum++;
			this.m_IsDirty = true;
		}

		// Token: 0x06002811 RID: 10257 RVA: 0x000D6C94 File Offset: 0x000D5094
		public override void Update()
		{
			base.Update();
			if (this.m_IsDirty)
			{
				for (int i = 0; i < this.m_DataList.Count; i++)
				{
					if (this.m_DataList[i].m_Num > 0)
					{
						ResultItemData resultItemData = UnityEngine.Object.Instantiate<ResultItemData>(this.m_ItemIconPrefab, this.m_ItemsParent);
						resultItemData.OnClick = new Action<int>(this.m_OwnerUI.OpenItemDetail);
						resultItemData.GetComponent<RectTransform>().localScale = Vector3.one;
						resultItemData.Apply(this.m_DataList[i].m_Id, this.m_DataList[i].m_Num);
					}
				}
				this.m_IsDirty = false;
			}
		}

		// Token: 0x04002F57 RID: 12119
		[SerializeField]
		private BattleResultUI m_OwnerUI;

		// Token: 0x04002F58 RID: 12120
		[SerializeField]
		private ResultItemData m_ItemIconPrefab;

		// Token: 0x04002F59 RID: 12121
		[SerializeField]
		private RectTransform m_ItemsParent;

		// Token: 0x04002F5A RID: 12122
		[SerializeField]
		private Text m_GetMoneyTextObj;

		// Token: 0x04002F5B RID: 12123
		[SerializeField]
		private Text m_HaveMoneyTextObj;

		// Token: 0x04002F5C RID: 12124
		private List<ResultItemReward.ItemData> m_DataList = new List<ResultItemReward.ItemData>();

		// Token: 0x04002F5D RID: 12125
		private int m_ItemNum;

		// Token: 0x04002F5E RID: 12126
		private bool m_IsDirty;

		// Token: 0x020007AA RID: 1962
		private struct ItemData
		{
			// Token: 0x04002F5F RID: 12127
			public int m_Id;

			// Token: 0x04002F60 RID: 12128
			public int m_Num;
		}
	}
}
