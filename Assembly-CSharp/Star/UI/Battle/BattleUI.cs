﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000763 RID: 1891
	public class BattleUI : MenuUIBase
	{
		// Token: 0x17000286 RID: 646
		// (get) Token: 0x060025D7 RID: 9687 RVA: 0x000CA887 File Offset: 0x000C8C87
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x060025D8 RID: 9688 RVA: 0x000CA88F File Offset: 0x000C8C8F
		public BattleSystem BattleSystem
		{
			get
			{
				return this.m_System;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x060025D9 RID: 9689 RVA: 0x000CA897 File Offset: 0x000C8C97
		public BattleTutorial BattleTutorial
		{
			get
			{
				return this.m_Tutorial;
			}
		}

		// Token: 0x1400003C RID: 60
		// (add) Token: 0x060025DA RID: 9690 RVA: 0x000CA8A0 File Offset: 0x000C8CA0
		// (remove) Token: 0x060025DB RID: 9691 RVA: 0x000CA8D8 File Offset: 0x000C8CD8
		public event Action OnClickMenuButton;

		// Token: 0x1400003D RID: 61
		// (add) Token: 0x060025DC RID: 9692 RVA: 0x000CA910 File Offset: 0x000C8D10
		// (remove) Token: 0x060025DD RID: 9693 RVA: 0x000CA948 File Offset: 0x000C8D48
		public event Action OnClickAutoButton;

		// Token: 0x1400003E RID: 62
		// (add) Token: 0x060025DE RID: 9694 RVA: 0x000CA980 File Offset: 0x000C8D80
		// (remove) Token: 0x060025DF RID: 9695 RVA: 0x000CA9B8 File Offset: 0x000C8DB8
		public event Action OnClickFriendButton;

		// Token: 0x1400003F RID: 63
		// (add) Token: 0x060025E0 RID: 9696 RVA: 0x000CA9F0 File Offset: 0x000C8DF0
		// (remove) Token: 0x060025E1 RID: 9697 RVA: 0x000CAA28 File Offset: 0x000C8E28
		public event Action OnCancelBattleMenuWindow;

		// Token: 0x14000040 RID: 64
		// (add) Token: 0x060025E2 RID: 9698 RVA: 0x000CAA60 File Offset: 0x000C8E60
		// (remove) Token: 0x060025E3 RID: 9699 RVA: 0x000CAA98 File Offset: 0x000C8E98
		public event Action OnClickRetireButton;

		// Token: 0x14000041 RID: 65
		// (add) Token: 0x060025E4 RID: 9700 RVA: 0x000CAAD0 File Offset: 0x000C8ED0
		// (remove) Token: 0x060025E5 RID: 9701 RVA: 0x000CAB08 File Offset: 0x000C8F08
		public event Action OnMemberChange;

		// Token: 0x14000042 RID: 66
		// (add) Token: 0x060025E6 RID: 9702 RVA: 0x000CAB40 File Offset: 0x000C8F40
		// (remove) Token: 0x060025E7 RID: 9703 RVA: 0x000CAB78 File Offset: 0x000C8F78
		public event Action<int> OnSelectMemberChange;

		// Token: 0x14000043 RID: 67
		// (add) Token: 0x060025E8 RID: 9704 RVA: 0x000CABB0 File Offset: 0x000C8FB0
		// (remove) Token: 0x060025E9 RID: 9705 RVA: 0x000CABE8 File Offset: 0x000C8FE8
		public event Action<int, BattleDefine.eJoinMember> OnSelectMasterSkill;

		// Token: 0x14000044 RID: 68
		// (add) Token: 0x060025EA RID: 9706 RVA: 0x000CAC20 File Offset: 0x000C9020
		// (remove) Token: 0x060025EB RID: 9707 RVA: 0x000CAC58 File Offset: 0x000C9058
		public event Action OnCancelMemberChange;

		// Token: 0x14000045 RID: 69
		// (add) Token: 0x060025EC RID: 9708 RVA: 0x000CAC90 File Offset: 0x000C9090
		// (remove) Token: 0x060025ED RID: 9709 RVA: 0x000CACC8 File Offset: 0x000C90C8
		public event Action OnClickTogetherButton;

		// Token: 0x14000046 RID: 70
		// (add) Token: 0x060025EE RID: 9710 RVA: 0x000CAD00 File Offset: 0x000C9100
		// (remove) Token: 0x060025EF RID: 9711 RVA: 0x000CAD38 File Offset: 0x000C9138
		public event Action<BattleDefine.eJoinMember> OnDecideTogetherMember;

		// Token: 0x14000047 RID: 71
		// (add) Token: 0x060025F0 RID: 9712 RVA: 0x000CAD70 File Offset: 0x000C9170
		// (remove) Token: 0x060025F1 RID: 9713 RVA: 0x000CADA8 File Offset: 0x000C91A8
		public event Action<List<BattleDefine.eJoinMember>> OnDecideTogether;

		// Token: 0x14000048 RID: 72
		// (add) Token: 0x060025F2 RID: 9714 RVA: 0x000CADE0 File Offset: 0x000C91E0
		// (remove) Token: 0x060025F3 RID: 9715 RVA: 0x000CAE18 File Offset: 0x000C9218
		public event Action OnCancelTogether;

		// Token: 0x14000049 RID: 73
		// (add) Token: 0x060025F4 RID: 9716 RVA: 0x000CAE50 File Offset: 0x000C9250
		// (remove) Token: 0x060025F5 RID: 9717 RVA: 0x000CAE88 File Offset: 0x000C9288
		public event Func<bool> OnJudgeInterruptFriendJoinSelect;

		// Token: 0x1400004A RID: 74
		// (add) Token: 0x060025F6 RID: 9718 RVA: 0x000CAEC0 File Offset: 0x000C92C0
		// (remove) Token: 0x060025F7 RID: 9719 RVA: 0x000CAEF8 File Offset: 0x000C92F8
		public event Action OnDecideInterruptFriendJoinSelect;

		// Token: 0x1400004B RID: 75
		// (add) Token: 0x060025F8 RID: 9720 RVA: 0x000CAF30 File Offset: 0x000C9330
		// (remove) Token: 0x060025F9 RID: 9721 RVA: 0x000CAF68 File Offset: 0x000C9368
		public event Action OnCancelInterruptFriendJoinSelect;

		// Token: 0x1400004C RID: 76
		// (add) Token: 0x060025FA RID: 9722 RVA: 0x000CAFA0 File Offset: 0x000C93A0
		// (remove) Token: 0x060025FB RID: 9723 RVA: 0x000CAFD8 File Offset: 0x000C93D8
		public event Action<int, bool> OnChangeCommandButton;

		// Token: 0x1400004D RID: 77
		// (add) Token: 0x060025FC RID: 9724 RVA: 0x000CB010 File Offset: 0x000C9410
		// (remove) Token: 0x060025FD RID: 9725 RVA: 0x000CB048 File Offset: 0x000C9448
		public event Action<int> OnClickCommandButton;

		// Token: 0x1400004E RID: 78
		// (add) Token: 0x060025FE RID: 9726 RVA: 0x000CB080 File Offset: 0x000C9480
		// (remove) Token: 0x060025FF RID: 9727 RVA: 0x000CB0B8 File Offset: 0x000C94B8
		public event Action OnEndCommandButtonLevelUp;

		// Token: 0x06002600 RID: 9728 RVA: 0x000CB0EE File Offset: 0x000C94EE
		public void Awake()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x06002601 RID: 9729 RVA: 0x000CB0FC File Offset: 0x000C94FC
		public void Setup(BattleSystem system, int friendcharaID, int masterOrbID)
		{
			Debug.Log("BattleUI.Setup() : friendCharaID = " + friendcharaID);
			this.m_System = system;
			this.m_MainCamera = this.m_System.MainCamera;
			this.m_Tutorial = this.m_System.Tutorial;
			for (int i = 0; i < this.m_PlayerStatusObjs.Length; i++)
			{
				this.m_PlayerStatusObjs[i].Setup();
			}
			for (int j = 0; j < this.m_EnemyStatusObjs.Length; j++)
			{
				this.m_EnemyStatusObjs[j].Setup();
			}
			this.m_BattleReward.SetRewardNum(0, 0, false);
			this.m_MainUIGroup.SetEnableInput(false);
			this.m_BattleFriendButton.Setup(friendcharaID);
			this.m_BattleFriendButton.OnClick += this.OnClickBattleFriendButtonCallBack;
			this.m_CommandSet.Setup(this.m_Tutorial);
			for (int k = 0; k < this.m_CommandSet.GetButtonNum(); k++)
			{
				this.m_CommandSet.GetButton(k).OnClickDecideButton += this.OnClickCommandButtonCallBack;
			}
			this.m_OrderIndicator.Setup(this.m_StateIconOwner);
			this.m_StateIconOwner.Setup();
			this.m_PopUpIconDisplay.Setup();
			this.m_DamageValueDisplay.Setup();
			this.m_OwnerImage.Setup();
			for (int l = 0; l < this.m_TargetMarkers.Length; l++)
			{
				this.m_TargetMarkers[l].Setup(this);
			}
			this.m_MemberChange.Setup(masterOrbID);
			this.m_TogetherSelect.Setup();
			this.m_TogetherSelect.OnDecideMember += this.OnDecideTogetherMemberCallBack;
			this.m_TogetherSelect.OnDecideTogether += this.OnDecideTogetherCallBack;
			this.m_TogetherSelect.OnCancelTogether += this.OnCancelTogetherCallBack;
			this.m_TogetherCutIn.Setup(this);
			this.m_InterruptFriendJoinSelect.Setup();
			this.m_BattleMenuWindow.Setup(this.m_System.Option);
			this.m_BattleMenuWindow.OnCancel += this.OnCancelBattleMenuWindowCallBack;
			this.m_BattleMenuWindow.OnRetire += this.OnClickRetireButtonCallBack;
			this.m_BattleMessage.Setup();
			this.m_UniqueSkillNameWindow.Setup();
			this.m_CommandButtonLevelUpAnimation.OnEndAnimation += this.OnEndCommandButtonLevelUpCallBack;
			this.m_StateController.Setup(5, 0, false);
			this.m_StateController.AddTransitEach(0, 1, true);
			this.m_StateController.AddTransitEach(0, 3, true);
			this.m_StateController.AddTransit(1, 2, true);
			this.m_StateController.AddTransit(2, 0, true);
			this.m_StateController.AddTransitEach(0, 4, true);
			if (!this.m_Tutorial.IsSeq(BattleTutorial.eSeq.None))
			{
				this.m_MenuButton.Interactable = false;
				this.m_AutoButton.Interactable = false;
			}
		}

		// Token: 0x06002602 RID: 9730 RVA: 0x000CB3E0 File Offset: 0x000C97E0
		public void PrepareCharaResource(List<int> charaIDs)
		{
			SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
			for (int i = 0; i < charaIDs.Count; i++)
			{
				if (charaIDs[i] != -1)
				{
					BattleUI.CharaUIResource charaUIResource = new BattleUI.CharaUIResource();
					charaUIResource.m_CharaID = charaIDs[i];
					charaUIResource.m_BustSpriteHandler = spriteMng.LoadAsyncBust(charaIDs[i]);
					this.m_CharaResourceList.Add(charaUIResource);
				}
			}
		}

		// Token: 0x06002603 RID: 9731 RVA: 0x000CB450 File Offset: 0x000C9850
		public bool IsDonePrepareCharaResource()
		{
			for (int i = 0; i < this.m_CharaResourceList.Count; i++)
			{
				if (this.m_CharaResourceList[i] != null && this.m_CharaResourceList[i].m_BustSpriteHandler != null && !this.m_CharaResourceList[i].m_BustSpriteHandler.IsAvailable())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002604 RID: 9732 RVA: 0x000CB4C0 File Offset: 0x000C98C0
		private void Update()
		{
			switch (this.m_State)
			{
			case 1:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.SetState(2);
				}
				break;
			}
			case 3:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.SetState(4);
				}
				break;
			}
			}
		}

		// Token: 0x06002605 RID: 9733 RVA: 0x000CB598 File Offset: 0x000C9998
		private void SetState(int state)
		{
			this.m_State = state;
			switch (state)
			{
			case 0:
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					this.m_AnimUIPlayerArray[i].gameObject.SetActive(false);
				}
				this.SetEnableInput(false);
				break;
			case 1:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case 2:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case 3:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case 4:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06002606 RID: 9734 RVA: 0x000CB658 File Offset: 0x000C9A58
		public override void PlayIn()
		{
			this.SetState(1);
			this.PlayInTopUI();
			this.PlayInStatus();
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainUIGroup.Open();
			this.m_MenuButton.Interactable = false;
			this.SetCommandSetEnableInput(false);
		}

		// Token: 0x06002607 RID: 9735 RVA: 0x000CB6BC File Offset: 0x000C9ABC
		public override void PlayOut()
		{
			this.SetState(3);
			this.PlayOutTopUI();
			this.PlayOutStatus();
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06002608 RID: 9736 RVA: 0x000CB702 File Offset: 0x000C9B02
		public void Hide()
		{
			this.SetState(0);
		}

		// Token: 0x06002609 RID: 9737 RVA: 0x000CB70B File Offset: 0x000C9B0B
		public override bool IsMenuEnd()
		{
			return this.m_State == 4;
		}

		// Token: 0x0600260A RID: 9738 RVA: 0x000CB718 File Offset: 0x000C9B18
		public override void Destroy()
		{
			base.Destroy();
			this.m_OrderIndicator.Destroy();
			this.m_OwnerImage.Destroy();
			this.m_BattleFriendButton.Destroy();
			this.m_MemberChange.Destroy();
			this.m_TogetherSelect.Destroy();
			SoftMask[] componentsInChildren = base.GetComponentsInChildren<SoftMask>(true);
			if (componentsInChildren != null)
			{
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].Destroy();
				}
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
				for (int j = 0; j < this.m_CharaResourceList.Count; j++)
				{
					if (this.m_CharaResourceList[j] != null && this.m_CharaResourceList[j].m_BustSpriteHandler != null)
					{
						spriteMng.Unload(this.m_CharaResourceList[j].m_BustSpriteHandler);
					}
				}
			}
		}

		// Token: 0x0600260B RID: 9739 RVA: 0x000CB804 File Offset: 0x000C9C04
		public void StartCommandInput()
		{
			this.m_StateController.ForceChangeState(0);
			this.PlayInTopUI();
			this.PlayInStatus();
			this.m_MenuButton.Interactable = true;
			this.SetCommandSetEnableInput(true);
			this.SetEnableInputOwnerImage(true);
			if (!this.m_Tutorial.IsSeq(BattleTutorial.eSeq.None))
			{
				this.m_MenuButton.Interactable = false;
			}
		}

		// Token: 0x0600260C RID: 9740 RVA: 0x000CB860 File Offset: 0x000C9C60
		private void EndCommandInput()
		{
			this.m_BattleFriendButton.SetEnableInput(false);
			this.m_MenuButton.Interactable = false;
			this.SetCommandSetEnableInput(false);
			this.SetEnableInputOwnerImage(false);
		}

		// Token: 0x0600260D RID: 9741 RVA: 0x000CB888 File Offset: 0x000C9C88
		public void PlayInTopUI()
		{
			this.SetEnableInput(true);
			this.m_MainUIGroup.Open();
			this.m_CommandSet.PlayIn();
			this.m_WaveGroup.Open();
			this.PlayInStatus();
		}

		// Token: 0x0600260E RID: 9742 RVA: 0x000CB8B8 File Offset: 0x000C9CB8
		public void PlayOutTopUI()
		{
			this.m_MainUIGroup.Close();
			this.m_CommandSet.PlayOut();
			this.m_WaveGroup.Close();
		}

		// Token: 0x0600260F RID: 9743 RVA: 0x000CB8DB File Offset: 0x000C9CDB
		public void PlayInReward()
		{
			this.m_WaveGroup.Open();
		}

		// Token: 0x06002610 RID: 9744 RVA: 0x000CB8E8 File Offset: 0x000C9CE8
		public void PlayOutReward()
		{
			this.m_WaveGroup.Close();
		}

		// Token: 0x06002611 RID: 9745 RVA: 0x000CB8F5 File Offset: 0x000C9CF5
		public void HideTopUI()
		{
			this.m_MainUIGroup.Hide();
			this.m_OrderIndicator.GetComponent<AnimUIPlayer>().Hide();
			this.SetCommandSetEnableRender(false);
			this.HideStatus();
		}

		// Token: 0x06002612 RID: 9746 RVA: 0x000CB91F File Offset: 0x000C9D1F
		public BattleTransitFade GetTransitFade()
		{
			return this.m_TransitFade;
		}

		// Token: 0x06002613 RID: 9747 RVA: 0x000CB927 File Offset: 0x000C9D27
		public void PlayInStatus()
		{
			this.PlayInStatusPlayer();
			this.PlayInStatusEnemy();
		}

		// Token: 0x06002614 RID: 9748 RVA: 0x000CB935 File Offset: 0x000C9D35
		public void PlayOutStatus()
		{
			this.PlayOutStatusPlayer();
			this.PlayOutStatusEnemy();
		}

		// Token: 0x06002615 RID: 9749 RVA: 0x000CB943 File Offset: 0x000C9D43
		public void PlayInStatusPlayer()
		{
			this.m_PlayerStatusAnim.PlayIn();
		}

		// Token: 0x06002616 RID: 9750 RVA: 0x000CB950 File Offset: 0x000C9D50
		public void PlayInStatusEnemy()
		{
			this.m_EnemyStatusAnim.PlayIn();
		}

		// Token: 0x06002617 RID: 9751 RVA: 0x000CB95D File Offset: 0x000C9D5D
		public void PlayOutStatusPlayer()
		{
			this.m_PlayerStatusAnim.PlayOut();
		}

		// Token: 0x06002618 RID: 9752 RVA: 0x000CB96A File Offset: 0x000C9D6A
		public void PlayOutStatusEnemy()
		{
			this.m_EnemyStatusAnim.PlayOut();
		}

		// Token: 0x06002619 RID: 9753 RVA: 0x000CB977 File Offset: 0x000C9D77
		public void HideStatus()
		{
			this.m_PlayerStatusAnim.Hide();
			this.m_EnemyStatusAnim.Hide();
		}

		// Token: 0x0600261A RID: 9754 RVA: 0x000CB98F File Offset: 0x000C9D8F
		public CharacterStatus GetPlayerStatus(BattleDefine.eJoinMember join)
		{
			return this.GetPlayerStatus((int)join);
		}

		// Token: 0x0600261B RID: 9755 RVA: 0x000CB998 File Offset: 0x000C9D98
		public CharacterStatus GetPlayerStatus(int index)
		{
			if (index < 0 || index >= this.m_PlayerStatusObjs.Length)
			{
				return null;
			}
			return this.m_PlayerStatusObjs[index];
		}

		// Token: 0x0600261C RID: 9756 RVA: 0x000CB9BC File Offset: 0x000C9DBC
		public void SetPlayerStatusAllEnableRender(bool flg)
		{
			for (int i = 0; i < this.m_PlayerStatusObjs.Length; i++)
			{
				this.m_PlayerStatusObjs[i].SetEnableRender(flg, false);
			}
		}

		// Token: 0x0600261D RID: 9757 RVA: 0x000CB9F1 File Offset: 0x000C9DF1
		public void SetPlayerStatusPosition(int index, Vector3 worldPoint)
		{
		}

		// Token: 0x0600261E RID: 9758 RVA: 0x000CB9F3 File Offset: 0x000C9DF3
		public CharacterStatus GetEnemyStatus(BattleDefine.eJoinMember join)
		{
			return this.GetEnemyStatus((int)join);
		}

		// Token: 0x0600261F RID: 9759 RVA: 0x000CB9FC File Offset: 0x000C9DFC
		public CharacterStatus GetEnemyStatus(int index)
		{
			if (index < 0 || index >= this.m_EnemyStatusObjs.Length)
			{
				return null;
			}
			return this.m_EnemyStatusObjs[index];
		}

		// Token: 0x06002620 RID: 9760 RVA: 0x000CBA20 File Offset: 0x000C9E20
		public void SetEnemyStatusAllEnableRender(bool flg)
		{
			for (int i = 0; i < this.m_EnemyStatusObjs.Length; i++)
			{
				this.m_EnemyStatusObjs[i].SetEnableRender(flg, false);
			}
		}

		// Token: 0x06002621 RID: 9761 RVA: 0x000CBA55 File Offset: 0x000C9E55
		public void SetEnemyStatusPosition(int index, Vector3 worldPoint)
		{
		}

		// Token: 0x06002622 RID: 9762 RVA: 0x000CBA58 File Offset: 0x000C9E58
		public void SetWave(int waveIndex, int waveNum)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append((waveIndex + 1).ToString());
			stringBuilder.Append(" / ");
			stringBuilder.Append(waveNum.ToString());
			this.m_WaveTextObj.text = stringBuilder.ToString();
		}

		// Token: 0x06002623 RID: 9763 RVA: 0x000CBAB4 File Offset: 0x000C9EB4
		public CustomButton GetMenuButton()
		{
			return this.m_MenuButton;
		}

		// Token: 0x06002624 RID: 9764 RVA: 0x000CBABC File Offset: 0x000C9EBC
		public CustomButton GetAutoButton()
		{
			return this.m_AutoButton;
		}

		// Token: 0x06002625 RID: 9765 RVA: 0x000CBAC4 File Offset: 0x000C9EC4
		public void OnClickMenuButtonCallBack()
		{
			this.m_StateController.ForceChangeState(0);
			this.EndCommandInput();
			this.PlayOutTopUI();
			this.PlayOutStatus();
			this.OnClickMenuButton.Call();
		}

		// Token: 0x06002626 RID: 9766 RVA: 0x000CBAEF File Offset: 0x000C9EEF
		public void OnClickAutoButtonCallBack()
		{
			this.OnClickAutoButton.Call();
		}

		// Token: 0x06002627 RID: 9767 RVA: 0x000CBAFC File Offset: 0x000C9EFC
		public void SetAutoFlag(bool flag)
		{
			this.m_AutoButton.IsDecided = flag;
			if (flag)
			{
				this.m_StateController.ForceChangeState(0);
				this.EndCommandInput();
			}
		}

		// Token: 0x06002628 RID: 9768 RVA: 0x000CBB22 File Offset: 0x000C9F22
		public BattleFriendButton GetFriendButton()
		{
			return this.m_BattleFriendButton;
		}

		// Token: 0x06002629 RID: 9769 RVA: 0x000CBB2A File Offset: 0x000C9F2A
		public void OnClickBattleFriendButtonCallBack()
		{
			this.OnClickFriendButton.Call();
		}

		// Token: 0x0600262A RID: 9770 RVA: 0x000CBB37 File Offset: 0x000C9F37
		public BattleOrderIndicator GetOrder()
		{
			return this.m_OrderIndicator;
		}

		// Token: 0x0600262B RID: 9771 RVA: 0x000CBB3F File Offset: 0x000C9F3F
		public BattleReward GetReward()
		{
			return this.m_BattleReward;
		}

		// Token: 0x0600262C RID: 9772 RVA: 0x000CBB48 File Offset: 0x000C9F48
		public Vector2 GetWorldRewardPosition()
		{
			Vector2 result = this.m_BattleReward.GetTreasureBoxRectTransform().position;
			result.x = result.x / SingletonMonoBehaviour<GameSystem>.Inst.UICamera.orthographicSize * this.m_MainCamera.orthographicSize + this.m_MainCamera.GetComponent<Transform>().position.x;
			result.y = result.y / SingletonMonoBehaviour<GameSystem>.Inst.UICamera.orthographicSize * this.m_MainCamera.orthographicSize + this.m_MainCamera.GetComponent<Transform>().position.y;
			return result;
		}

		// Token: 0x0600262D RID: 9773 RVA: 0x000CBBF2 File Offset: 0x000C9FF2
		public void ShiftDownCommandSet(bool flg, bool immediate = false)
		{
			this.m_CommandSet.ShiftDown(flg, immediate);
		}

		// Token: 0x0600262E RID: 9774 RVA: 0x000CBC04 File Offset: 0x000CA004
		public void SetCommandSetBlank()
		{
			this.SetAllCommandButtonNotSelect();
			this.SetCommandSetEnableInput(false);
			this.SetEnableInputOwnerImage(false);
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				this.m_CommandSet.GetButton(i).SetupBlank();
			}
		}

		// Token: 0x0600262F RID: 9775 RVA: 0x000CBC52 File Offset: 0x000CA052
		private void SetCommandSetEnableRender(bool flg)
		{
			this.m_CommandSet.gameObject.SetActive(flg);
		}

		// Token: 0x06002630 RID: 9776 RVA: 0x000CBC65 File Offset: 0x000CA065
		private void SetCommandSetEnableInput(bool flg)
		{
			this.m_CommandSet.SetEnableInput(flg);
			this.m_TogetherButton.SetEnableInput(flg);
		}

		// Token: 0x06002631 RID: 9777 RVA: 0x000CBC80 File Offset: 0x000CA080
		public void SetOwnerImage(int charaID, bool isChangeOnOutOfScreen)
		{
			for (int i = 0; i < this.m_CharaResourceList.Count; i++)
			{
				if (this.m_CharaResourceList[i].m_CharaID == charaID)
				{
					this.m_OwnerImage.SetSprite(this.m_CharaResourceList[i].m_BustSpriteHandler.Obj, isChangeOnOutOfScreen);
					return;
				}
			}
			this.m_OwnerImage.SetSprite(null, isChangeOnOutOfScreen);
		}

		// Token: 0x06002632 RID: 9778 RVA: 0x000CBCF0 File Offset: 0x000CA0F0
		public GameObject GetOwnerImageObject()
		{
			return this.m_OwnerImage.gameObject;
		}

		// Token: 0x06002633 RID: 9779 RVA: 0x000CBD00 File Offset: 0x000CA100
		public void SetEnableInputOwnerImage(bool flg)
		{
			BattleTutorial.eSeq seq = this.m_Tutorial.GetSeq();
			if (seq != BattleTutorial.eSeq.None && flg && seq < BattleTutorial.eSeq._08_WAVE_IDX_1_OWNER_IMAGE_SLIDE)
			{
				flg = false;
			}
			this.m_OwnerImage.SetEnableInput(flg);
		}

		// Token: 0x06002634 RID: 9780 RVA: 0x000CBD3C File Offset: 0x000CA13C
		public void AppearOwnerImage()
		{
			if (!this.m_OwnerImage.IsDisplay)
			{
				this.m_OwnerImage.Appear();
			}
		}

		// Token: 0x06002635 RID: 9781 RVA: 0x000CBD59 File Offset: 0x000CA159
		public BattleDescription GetDescription()
		{
			return this.m_Description;
		}

		// Token: 0x06002636 RID: 9782 RVA: 0x000CBD61 File Offset: 0x000CA161
		public CommandButton GetCommandButton(int index)
		{
			return this.m_CommandSet.GetButton(index);
		}

		// Token: 0x06002637 RID: 9783 RVA: 0x000CBD6F File Offset: 0x000CA16F
		public int GetCommandButtonNum()
		{
			return this.m_CommandSet.GetButtonNum();
		}

		// Token: 0x06002638 RID: 9784 RVA: 0x000CBD7C File Offset: 0x000CA17C
		private void OnClickCommandButtonCallBack(int index, bool isFirstSelect, bool isSilence)
		{
			if (this.m_StateController.NowState != 0)
			{
				return;
			}
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				CommandButton button = this.m_CommandSet.GetButton(i);
				if (button.CommandIndex != index)
				{
					button.SetMode(CommandButton.eMode.None);
				}
				else
				{
					button.SetMode(CommandButton.eMode.Select);
				}
			}
			if (isFirstSelect)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_SELECT, 1f, 0, -1, -1);
				this.OnChangeCommandButton.Call(index, isSilence);
			}
			else if (this.m_CommandSet.GetButton(index).IsEnableInput && !isSilence)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_DECISION, 1f, 0, -1, -1);
				this.m_CommandSet.GetButton(index).SetMode(CommandButton.eMode.Decide);
				this.EndCommandInput();
				this.OnClickCommandButton.Call(index);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG, 1f, 0, -1, -1);
			}
		}

		// Token: 0x06002639 RID: 9785 RVA: 0x000CBE98 File Offset: 0x000CA298
		public void SetAllCommandButtonNotSelect()
		{
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				this.m_CommandSet.GetButton(i).SetMode(CommandButton.eMode.None);
			}
			this.m_Description.SetText(string.Empty);
		}

		// Token: 0x0600263A RID: 9786 RVA: 0x000CBEE4 File Offset: 0x000CA2E4
		public void SetCommandButtonSelecting(int idx)
		{
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				CommandButton button = this.m_CommandSet.GetButton(i);
				if (i == idx)
				{
					button.SetMode(CommandButton.eMode.Select);
				}
				else
				{
					button.SetMode(CommandButton.eMode.None);
				}
			}
		}

		// Token: 0x0600263B RID: 9787 RVA: 0x000CBF34 File Offset: 0x000CA334
		public void SetCommandButtonDecided(int idx, bool flg)
		{
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				CommandButton button = this.m_CommandSet.GetButton(i);
				if (flg)
				{
					if (i == idx)
					{
						button.SetMode(CommandButton.eMode.Decide);
					}
					else
					{
						button.SetMode(CommandButton.eMode.None);
					}
				}
				else
				{
					button.SetMode(CommandButton.eMode.None);
				}
			}
		}

		// Token: 0x0600263C RID: 9788 RVA: 0x000CBF98 File Offset: 0x000CA398
		public void SetAllCommandButtonEnableInput(bool flg)
		{
			for (int i = 0; i < this.m_CommandSet.GetButtonNum(); i++)
			{
				this.m_CommandSet.GetButton(i).SetEnableInput(flg);
			}
		}

		// Token: 0x0600263D RID: 9789 RVA: 0x000CBFD4 File Offset: 0x000CA3D4
		public void PlayCommandButtonLevelup(int idx)
		{
			RectTransform component = this.m_CommandButtonLevelUpAnimation.GetComponent<RectTransform>();
			component.SetParent(this.m_CommandSet.GetButton(idx).CacheTransform);
			component.localPosition = Vector3.zero;
			this.m_CommandButtonLevelUpAnimation.Play();
		}

		// Token: 0x0600263E RID: 9790 RVA: 0x000CC01A File Offset: 0x000CA41A
		public void OnEndCommandButtonLevelUpCallBack()
		{
			this.OnEndCommandButtonLevelUp.Call();
		}

		// Token: 0x0600263F RID: 9791 RVA: 0x000CC027 File Offset: 0x000CA427
		public TogetherButton GetTogetherButton()
		{
			return this.m_TogetherButton;
		}

		// Token: 0x06002640 RID: 9792 RVA: 0x000CC02F File Offset: 0x000CA42F
		public void OnClickTogetherButtonCallBack()
		{
			if (this.m_StateController.ChangeState(3))
			{
				this.OnClickTogetherButton.Call();
			}
		}

		// Token: 0x06002641 RID: 9793 RVA: 0x000CC04D File Offset: 0x000CA44D
		public TogetherSelect GetTogetherSelect()
		{
			return this.m_TogetherSelect;
		}

		// Token: 0x06002642 RID: 9794 RVA: 0x000CC055 File Offset: 0x000CA455
		public TogetherCutIn GetTogetherCutIn()
		{
			return this.m_TogetherCutIn;
		}

		// Token: 0x06002643 RID: 9795 RVA: 0x000CC05D File Offset: 0x000CA45D
		public void OpenTogetherCutInSkipSelect(int[] charaIDs, eElementType[] element, BattleDefine.eJoinMember[] join, int[] skillIDs, List<BattleDefine.eJoinMember> selectOrder)
		{
			this.PlayTogetherCutIn(charaIDs);
		}

		// Token: 0x06002644 RID: 9796 RVA: 0x000CC068 File Offset: 0x000CA468
		public void OpenTogetherSelect(int[] charaIDs, eElementType[] elements, BattleDefine.eJoinMember[] join, int[] skillIDs, int selectableMax)
		{
			this.EndCommandInput();
			this.PlayOutTopUI();
			this.PlayOutStatusPlayer();
			this.m_TogetherSelect.gameObject.SetActive(true);
			this.m_TogetherSelect.SetCharacter(charaIDs, elements, skillIDs, join);
			this.m_TogetherSelect.SetSelectableMax(selectableMax);
			this.m_TogetherSelect.ResetSelectOrder();
			this.m_TogetherSelect.Open();
		}

		// Token: 0x06002645 RID: 9797 RVA: 0x000CC0CB File Offset: 0x000CA4CB
		public void OnDecideTogetherMemberCallBack(BattleDefine.eJoinMember join)
		{
			this.OnDecideTogetherMember.Call(join);
		}

		// Token: 0x06002646 RID: 9798 RVA: 0x000CC0D9 File Offset: 0x000CA4D9
		public void OnDecideTogetherCallBack()
		{
			this.PlayOutStatus();
			this.OnDecideTogether.Call(this.m_TogetherSelect.GetSelectOrderJoin());
			this.PlayTogetherCutIn(this.m_TogetherSelect.GetSelectOrderCharaIDs().ToArray());
		}

		// Token: 0x06002647 RID: 9799 RVA: 0x000CC10D File Offset: 0x000CA50D
		public void OnCancelTogetherCallBack()
		{
			if (this.m_StateController.ChangeState(0))
			{
				this.OnCancelTogether.Call();
				this.StartCommandInput();
			}
		}

		// Token: 0x06002648 RID: 9800 RVA: 0x000CC131 File Offset: 0x000CA531
		public void PlayTogetherCutIn(int[] charaIDs)
		{
			this.EndCommandInput();
			this.PlayOutTopUI();
			this.PlayOutStatus();
			this.m_TogetherCutIn.Play(charaIDs);
			this.StartTogetherCutInChara();
		}

		// Token: 0x06002649 RID: 9801 RVA: 0x000CC157 File Offset: 0x000CA557
		public void StartTogetherCutInChara()
		{
			this.m_TogetherCutIn.ShowChara();
		}

		// Token: 0x0600264A RID: 9802 RVA: 0x000CC164 File Offset: 0x000CA564
		public void FinishTogetherCutIn()
		{
			this.m_TogetherCutIn.End();
		}

		// Token: 0x0600264B RID: 9803 RVA: 0x000CC171 File Offset: 0x000CA571
		public TurnOwnerMarker GetTurnOwnerMarker()
		{
			return this.m_TurnOwnerMarker;
		}

		// Token: 0x0600264C RID: 9804 RVA: 0x000CC17C File Offset: 0x000CA57C
		public void SetTurnOwnerMarker(bool flg, Vector3 worldPoint)
		{
			if (flg)
			{
				Vector2 vector = base.WorldToUIPosition(worldPoint);
				this.m_TurnOwnerMarker.SetPosition(vector.x, vector.y);
			}
			else
			{
				this.ClearTargetMarkerOrderIcon();
			}
			this.m_TurnOwnerMarker.SetEnableRender(flg);
		}

		// Token: 0x0600264D RID: 9805 RVA: 0x000CC1C8 File Offset: 0x000CA5C8
		public void SetTargetMarkerAllEnableRender(bool flg)
		{
			for (int i = 0; i < this.m_TargetMarkers.Length; i++)
			{
				this.m_TargetMarkers[i].SetEnableRender(flg);
			}
		}

		// Token: 0x0600264E RID: 9806 RVA: 0x000CC1FC File Offset: 0x000CA5FC
		public void SetTargetMarker(int idx, bool flg, Transform refTransform, Vector3 offsetFromRefTransform, TargetMarker.eMode mode = TargetMarker.eMode.Normal)
		{
			TargetMarker targetMarker = this.m_TargetMarkers[idx];
			if (flg)
			{
				targetMarker.SetTransform(refTransform, offsetFromRefTransform);
			}
			targetMarker.SetMode(mode);
			targetMarker.SetEnableRender(flg);
		}

		// Token: 0x0600264F RID: 9807 RVA: 0x000CC230 File Offset: 0x000CA630
		public void ClearTargetMarkerOrderIcon()
		{
			for (int i = 0; i < 3; i++)
			{
				this.m_OrderIndicator.SetTargetCursor(BattleDefine.ePartyType.Player, (BattleDefine.eJoinMember)i, false);
				this.m_OrderIndicator.SetTargetCursor(BattleDefine.ePartyType.Enemy, (BattleDefine.eJoinMember)i, false);
			}
		}

		// Token: 0x06002650 RID: 9808 RVA: 0x000CC26B File Offset: 0x000CA66B
		public void SetTargetMarkerToOrderIcon(BattleDefine.ePartyType party, BattleDefine.eJoinMember join, bool flg)
		{
			this.m_OrderIndicator.SetTargetCursor(party, join, flg);
		}

		// Token: 0x06002651 RID: 9809 RVA: 0x000CC27C File Offset: 0x000CA67C
		public void SetTargetMarkerToOrderIconPlayer(bool[] flg)
		{
			for (int i = 0; i < flg.Length; i++)
			{
				this.m_OrderIndicator.SetTargetCursor(BattleDefine.ePartyType.Player, (BattleDefine.eJoinMember)i, flg[i]);
			}
		}

		// Token: 0x06002652 RID: 9810 RVA: 0x000CC2B0 File Offset: 0x000CA6B0
		public void SetTargetMarkerToOrderIconEnemy(bool[] flg)
		{
			for (int i = 0; i < flg.Length; i++)
			{
				this.m_OrderIndicator.SetTargetCursor(BattleDefine.ePartyType.Enemy, (BattleDefine.eJoinMember)i, flg[i]);
			}
		}

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06002653 RID: 9811 RVA: 0x000CC2E1 File Offset: 0x000CA6E1
		// (set) Token: 0x06002654 RID: 9812 RVA: 0x000CC2E9 File Offset: 0x000CA6E9
		public bool IsExectuteRetireFromDirect
		{
			get
			{
				return this.m_IsExectuteRetireFromDirect;
			}
			set
			{
				this.m_IsExectuteRetireFromDirect = value;
			}
		}

		// Token: 0x06002655 RID: 9813 RVA: 0x000CC2F2 File Offset: 0x000CA6F2
		public void OpenBattleMenuWindow()
		{
			this.m_BattleMenuWindow.PlayIn();
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x000CC2FF File Offset: 0x000CA6FF
		public void OnCancelBattleMenuWindowCallBack()
		{
			this.OnCancelBattleMenuWindow.Call();
			this.StartCommandInput();
		}

		// Token: 0x06002657 RID: 9815 RVA: 0x000CC312 File Offset: 0x000CA712
		public void OnClickRetireButtonCallBack()
		{
			this.ExecuteRetire(false);
		}

		// Token: 0x06002658 RID: 9816 RVA: 0x000CC31B File Offset: 0x000CA71B
		private void ExecuteRetire(bool isExecuteRetireFromMenu)
		{
			this.m_IsExectuteRetireFromDirect = isExecuteRetireFromMenu;
			this.EndCommandInput();
			this.PlayOutTopUI();
			this.OnClickRetireButton.Call();
		}

		// Token: 0x06002659 RID: 9817 RVA: 0x000CC33B File Offset: 0x000CA73B
		public void OpenMemberChange()
		{
			this.m_MemberChange.PlayIn();
		}

		// Token: 0x0600265A RID: 9818 RVA: 0x000CC348 File Offset: 0x000CA748
		public void CloseMemberChange()
		{
			this.m_MemberChange.PlayOut();
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x000CC355 File Offset: 0x000CA755
		public MemberChange GetMemberChange()
		{
			return this.m_MemberChange;
		}

		// Token: 0x0600265C RID: 9820 RVA: 0x000CC35D File Offset: 0x000CA75D
		public BenchStatus GetBenchStatus(BattleDefine.eJoinMember bench)
		{
			return this.m_MemberChange.GetBenchStatus(bench);
		}

		// Token: 0x0600265D RID: 9821 RVA: 0x000CC36B File Offset: 0x000CA76B
		public void SetMasterSkillTarget(BattleDefine.ePartyType partyType, BattleDefine.eJoinMember join, int charaIDorResourceID, bool isInteractable)
		{
			if (partyType == BattleDefine.ePartyType.Player)
			{
				this.m_MemberChange.SetPlayerTargetState((int)join, charaIDorResourceID, isInteractable);
			}
			else
			{
				this.m_MemberChange.SetEnemyTargetState((int)join, charaIDorResourceID, isInteractable);
			}
		}

		// Token: 0x0600265E RID: 9822 RVA: 0x000CC396 File Offset: 0x000CA796
		public void StartMemberChange()
		{
			if (this.m_StateController.ChangeState(1))
			{
				this.m_OwnerImage.Disappear();
				this.EndCommandInput();
				this.PlayOutTopUI();
				this.PlayOutStatus();
				this.OnMemberChange.Call();
			}
		}

		// Token: 0x0600265F RID: 9823 RVA: 0x000CC3D1 File Offset: 0x000CA7D1
		public void DecideMemberChange(int idx)
		{
			if (this.m_StateController.ChangeState(0))
			{
				this.OnSelectMemberChange.Call(idx);
			}
		}

		// Token: 0x06002660 RID: 9824 RVA: 0x000CC3F0 File Offset: 0x000CA7F0
		public void DecideMasterSkill(int commandIndex, BattleDefine.eJoinMember target)
		{
			if (this.m_StateController.ChangeState(2))
			{
				this.m_SelectMasterSkillCommandIndex = commandIndex;
				this.m_SelectMasterSkillTarget = target;
				this.OnSelectMasterSkill.Call(this.m_SelectMasterSkillCommandIndex, this.m_SelectMasterSkillTarget);
				this.m_SelectMasterSkillCommandIndex = -1;
				this.m_StateController.ChangeState(0);
			}
		}

		// Token: 0x06002661 RID: 9825 RVA: 0x000CC447 File Offset: 0x000CA847
		public void CancelMemberChange()
		{
			if (this.m_StateController.ChangeState(0))
			{
				this.OnCancelMemberChange.Call();
			}
		}

		// Token: 0x06002662 RID: 9826 RVA: 0x000CC465 File Offset: 0x000CA865
		public void OpenInterruptFriendJoinSelect()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.EndCommandInput();
				this.PlayOutTopUI();
				this.m_InterruptFriendJoinSelect.PlayIn();
			}
		}

		// Token: 0x06002663 RID: 9827 RVA: 0x000CC48F File Offset: 0x000CA88F
		public bool JudgeInterruptFriendJoinSelect()
		{
			return this.OnJudgeInterruptFriendJoinSelect != null && this.OnJudgeInterruptFriendJoinSelect();
		}

		// Token: 0x06002664 RID: 9828 RVA: 0x000CC4A9 File Offset: 0x000CA8A9
		public void DecideInterruptFriendJoinSelect()
		{
			this.PlayOutStatus();
			this.OnDecideInterruptFriendJoinSelect.Call();
			this.m_StateController.ForceChangeState(0);
		}

		// Token: 0x06002665 RID: 9829 RVA: 0x000CC4C8 File Offset: 0x000CA8C8
		public void CancelInterruptFriendJoinSelect()
		{
			this.OnCancelInterruptFriendJoinSelect.Call();
			this.m_StateController.ForceChangeState(0);
			this.StartCommandInput();
		}

		// Token: 0x06002666 RID: 9830 RVA: 0x000CC4E7 File Offset: 0x000CA8E7
		public DamageValueDisplay GetDamageDisplay()
		{
			return this.m_DamageValueDisplay;
		}

		// Token: 0x06002667 RID: 9831 RVA: 0x000CC4EF File Offset: 0x000CA8EF
		public void DisplayDamage(int value, bool isCritical, int registHit_Or_DefaultHit_Or_WeakHit, bool isUnhappy, bool isPoison, Transform targetTransform, float offsetYfromTargetTransform)
		{
			this.m_DamageValueDisplay.Display(value, isCritical, isUnhappy, isPoison, registHit_Or_DefaultHit_Or_WeakHit, targetTransform, offsetYfromTargetTransform);
		}

		// Token: 0x06002668 RID: 9832 RVA: 0x000CC507 File Offset: 0x000CA907
		public PopUpIconDisplay GetPopUpIconDisplay()
		{
			return this.m_PopUpIconDisplay;
		}

		// Token: 0x06002669 RID: 9833 RVA: 0x000CC50F File Offset: 0x000CA90F
		public void PopUpIcon(List<SkillActionEffectSet.IconData> iconDatas, Transform targetTransform, float offsetYfromTargetTransform)
		{
			this.m_PopUpIconDisplay.Display(iconDatas, targetTransform, offsetYfromTargetTransform);
		}

		// Token: 0x0600266A RID: 9834 RVA: 0x000CC51F File Offset: 0x000CA91F
		public bool IsPlayingPopUpIcon()
		{
			return this.m_PopUpIconDisplay.IsPlaying();
		}

		// Token: 0x0600266B RID: 9835 RVA: 0x000CC52C File Offset: 0x000CA92C
		public BattleMessage GetBattleMessage()
		{
			return this.m_BattleMessage;
		}

		// Token: 0x0600266C RID: 9836 RVA: 0x000CC534 File Offset: 0x000CA934
		public UniqueSkillNameWindow GetUniqueSkillNameWindow()
		{
			return this.m_UniqueSkillNameWindow;
		}

		// Token: 0x0600266D RID: 9837 RVA: 0x000CC53C File Offset: 0x000CA93C
		public TotalDamageDisplay GetTotalDamageDisplay()
		{
			return this.m_TotalDamageDisplay;
		}

		// Token: 0x0600266E RID: 9838 RVA: 0x000CC544 File Offset: 0x000CA944
		public TitlePlayer GetWarningStart()
		{
			return this.m_Warning;
		}

		// Token: 0x0600266F RID: 9839 RVA: 0x000CC54C File Offset: 0x000CA94C
		public Vector2 UIPositionToWorld(Vector2 UIPosition)
		{
			float num = this.m_UICanvasScaler.referenceResolution.y * 0.5f;
			UIPosition.x = UIPosition.x / num * this.m_MainCamera.orthographicSize;
			UIPosition.y = UIPosition.y / num * this.m_MainCamera.orthographicSize;
			return UIPosition;
		}

		// Token: 0x06002670 RID: 9840 RVA: 0x000CC5AC File Offset: 0x000CA9AC
		public void UpdateAndroidBackKey()
		{
			if (UIUtility.GetInputAndroidBackKey(this.m_MenuButton))
			{
				this.ExecuteRetire(true);
			}
		}

		// Token: 0x04002C8A RID: 11402
		public const int STATE_HIDE = 0;

		// Token: 0x04002C8B RID: 11403
		public const int STATE_IN = 1;

		// Token: 0x04002C8C RID: 11404
		public const int STATE_WAIT = 2;

		// Token: 0x04002C8D RID: 11405
		public const int STATE_OUT = 3;

		// Token: 0x04002C8E RID: 11406
		public const int STATE_END = 4;

		// Token: 0x04002C8F RID: 11407
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04002C90 RID: 11408
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04002C91 RID: 11409
		[SerializeField]
		private CharacterStatus[] m_PlayerStatusObjs;

		// Token: 0x04002C92 RID: 11410
		[SerializeField]
		private CharacterStatus[] m_EnemyStatusObjs;

		// Token: 0x04002C93 RID: 11411
		[SerializeField]
		[Tooltip("プレイヤー全体")]
		private AnimUIPlayer m_PlayerStatusAnim;

		// Token: 0x04002C94 RID: 11412
		[SerializeField]
		[Tooltip("エネミー全体")]
		private AnimUIPlayer m_EnemyStatusAnim;

		// Token: 0x04002C95 RID: 11413
		[SerializeField]
		private BattleTransitFade m_TransitFade;

		// Token: 0x04002C96 RID: 11414
		[SerializeField]
		private UIGroup m_MainUIGroup;

		// Token: 0x04002C97 RID: 11415
		[SerializeField]
		private BattleReward m_BattleReward;

		// Token: 0x04002C98 RID: 11416
		[SerializeField]
		private UIGroup m_WaveGroup;

		// Token: 0x04002C99 RID: 11417
		[SerializeField]
		private Text m_WaveTextObj;

		// Token: 0x04002C9A RID: 11418
		[SerializeField]
		private CustomButton m_AutoButton;

		// Token: 0x04002C9B RID: 11419
		[SerializeField]
		private CustomButton m_MenuButton;

		// Token: 0x04002C9C RID: 11420
		[SerializeField]
		private BattleFriendButton m_BattleFriendButton;

		// Token: 0x04002C9D RID: 11421
		[SerializeField]
		private BattleOrderIndicator m_OrderIndicator;

		// Token: 0x04002C9E RID: 11422
		[SerializeField]
		private TogetherButton m_TogetherButton;

		// Token: 0x04002C9F RID: 11423
		[SerializeField]
		private CommandSet m_CommandSet;

		// Token: 0x04002CA0 RID: 11424
		[SerializeField]
		private BattleDescription m_Description;

		// Token: 0x04002CA1 RID: 11425
		[SerializeField]
		private OwnerImage m_OwnerImage;

		// Token: 0x04002CA2 RID: 11426
		[SerializeField]
		private ElementIcon m_OwnerElement;

		// Token: 0x04002CA3 RID: 11427
		[SerializeField]
		private CommandButtonLevelUp m_CommandButtonLevelUpAnimation;

		// Token: 0x04002CA4 RID: 11428
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04002CA5 RID: 11429
		[SerializeField]
		private DamageValueDisplay m_DamageValueDisplay;

		// Token: 0x04002CA6 RID: 11430
		[SerializeField]
		private PopUpIconDisplay m_PopUpIconDisplay;

		// Token: 0x04002CA7 RID: 11431
		[SerializeField]
		private TargetMarker[] m_TargetMarkers;

		// Token: 0x04002CA8 RID: 11432
		[SerializeField]
		private TurnOwnerMarker m_TurnOwnerMarker;

		// Token: 0x04002CA9 RID: 11433
		[SerializeField]
		private MemberChange m_MemberChange;

		// Token: 0x04002CAA RID: 11434
		[SerializeField]
		private TogetherSelect m_TogetherSelect;

		// Token: 0x04002CAB RID: 11435
		[SerializeField]
		private TogetherCutIn m_TogetherCutIn;

		// Token: 0x04002CAC RID: 11436
		[SerializeField]
		private InterruptFriendJoinSelect m_InterruptFriendJoinSelect;

		// Token: 0x04002CAD RID: 11437
		[SerializeField]
		private BattleMenuWindow m_BattleMenuWindow;

		// Token: 0x04002CAE RID: 11438
		[SerializeField]
		private BattleMessage m_BattleMessage;

		// Token: 0x04002CAF RID: 11439
		[SerializeField]
		private UniqueSkillNameWindow m_UniqueSkillNameWindow;

		// Token: 0x04002CB0 RID: 11440
		[SerializeField]
		private TotalDamageDisplay m_TotalDamageDisplay;

		// Token: 0x04002CB1 RID: 11441
		[SerializeField]
		private TitlePlayer m_Warning;

		// Token: 0x04002CB2 RID: 11442
		private RectTransform m_RectTransform;

		// Token: 0x04002CB3 RID: 11443
		private int m_SelectMasterSkillCommandIndex = -1;

		// Token: 0x04002CB4 RID: 11444
		private BattleDefine.eJoinMember m_SelectMasterSkillTarget = BattleDefine.eJoinMember.None;

		// Token: 0x04002CB5 RID: 11445
		private List<BattleUI.CharaUIResource> m_CharaResourceList = new List<BattleUI.CharaUIResource>();

		// Token: 0x04002CB6 RID: 11446
		private BattleSystem m_System;

		// Token: 0x04002CB7 RID: 11447
		private Camera m_MainCamera;

		// Token: 0x04002CB8 RID: 11448
		private BattleTutorial m_Tutorial;

		// Token: 0x04002CCC RID: 11468
		private bool m_IsExectuteRetireFromDirect;

		// Token: 0x02000764 RID: 1892
		private enum eUIState
		{
			// Token: 0x04002CCE RID: 11470
			Wait,
			// Token: 0x04002CCF RID: 11471
			MemberChange,
			// Token: 0x04002CD0 RID: 11472
			MasterSkill,
			// Token: 0x04002CD1 RID: 11473
			Together,
			// Token: 0x04002CD2 RID: 11474
			InterruptFriendJoinSelect,
			// Token: 0x04002CD3 RID: 11475
			Num
		}

		// Token: 0x02000765 RID: 1893
		private class CharaUIResource
		{
			// Token: 0x04002CD4 RID: 11476
			public int m_CharaID = -1;

			// Token: 0x04002CD5 RID: 11477
			public SpriteHandler m_BustSpriteHandler;
		}
	}
}
