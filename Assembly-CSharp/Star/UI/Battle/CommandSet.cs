﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200076F RID: 1903
	public class CommandSet : MonoBehaviour
	{
		// Token: 0x060026BE RID: 9918 RVA: 0x000CE228 File Offset: 0x000CC628
		public void Setup(BattleTutorial tutorial)
		{
			this.m_CommandButtons = new CommandButton[4];
			for (int i = 0; i < 4; i++)
			{
				this.m_CommandButtons[i] = UnityEngine.Object.Instantiate<CommandButton>(this.m_CommandButtonPrefab, this.m_Parent, true);
				this.m_CommandButtons[i].Setup(this, tutorial);
				RectTransform component = this.m_CommandButtons[i].GetComponent<RectTransform>();
				component.localPosition = new Vector3(this.m_Parent.rect.width * 0.5f - (component.sizeDelta.x + this.m_CommandSpace) * ((float)i + 0.5f), 0f);
			}
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			for (int j = 0; j < this.m_ShiftAnims.Length; j++)
			{
				this.m_ShiftAnims[j].Show();
			}
		}

		// Token: 0x060026BF RID: 9919 RVA: 0x000CE30C File Offset: 0x000CC70C
		private void Update()
		{
			bool flag = true;
			for (int i = 0; i < 4; i++)
			{
				this.m_CommandButtons[i].UpdateStatus();
				if (!this.m_CommandButtons[i].IsReadyApplyReserve())
				{
					flag = false;
				}
			}
			if (flag)
			{
				for (int j = 0; j < 4; j++)
				{
					this.m_CommandButtons[j].ApplyReserve();
				}
				for (int k = 0; k < 4; k++)
				{
					if (k > 0)
					{
						if (!this.m_CommandButtons[k].gameObject.activeSelf)
						{
							this.m_CommandButtons[k - 1].SetEnableRenderFlickArrowLeft(false);
						}
						else
						{
							this.m_CommandButtons[k - 1].SetEnableRenderFlickArrowLeft(true);
						}
					}
					if (k == 0)
					{
						this.m_CommandButtons[k].SetEnableRenderFlickArrowRight(false);
					}
					else if (k == 3)
					{
						this.m_CommandButtons[k].SetEnableRenderFlickArrowLeft(false);
					}
				}
			}
			if (this.m_RequestRecastSE)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_RECAST_COMPLETE, 1f, 0, -1, -1);
				this.m_RequestRecastSE = false;
			}
		}

		// Token: 0x060026C0 RID: 9920 RVA: 0x000CE425 File Offset: 0x000CC825
		public CommandButton GetButton(int index)
		{
			return this.m_CommandButtons[index];
		}

		// Token: 0x060026C1 RID: 9921 RVA: 0x000CE42F File Offset: 0x000CC82F
		public int GetButtonNum()
		{
			return this.m_CommandButtons.Length;
		}

		// Token: 0x060026C2 RID: 9922 RVA: 0x000CE439 File Offset: 0x000CC839
		public Sprite GetAttackSprite()
		{
			return this.m_SpriteNormalAttack;
		}

		// Token: 0x060026C3 RID: 9923 RVA: 0x000CE441 File Offset: 0x000CC841
		public Sprite GetMagicAttackSprite()
		{
			return this.m_SpriteNormalMagicAttack;
		}

		// Token: 0x060026C4 RID: 9924 RVA: 0x000CE44C File Offset: 0x000CC84C
		public Sprite GetSkillSprite(eSkillType skillType)
		{
			switch (skillType)
			{
			case eSkillType.Attack_atk:
				return this.m_SpriteSkillAttack;
			case eSkillType.Attack_mgc:
				return this.m_SpriteSkillMagic;
			case eSkillType.Recovery:
				return this.m_SpriteSkillRecovery;
			case eSkillType.AssistBuff:
				return this.m_SpriteSkillBuff;
			case eSkillType.AssistDeBuff:
				return this.m_SpriteSkillDebuff;
			case eSkillType.Other:
				return this.m_SpriteSkillOther;
			default:
				return null;
			}
		}

		// Token: 0x060026C5 RID: 9925 RVA: 0x000CE4AC File Offset: 0x000CC8AC
		public void SetEnableInput(bool flag)
		{
			this.m_CanvasGroup.interactable = flag;
			for (int i = 0; i < this.m_CommandButtons.Length; i++)
			{
				this.m_CommandButtons[i].SetEnableInput(flag);
			}
		}

		// Token: 0x060026C6 RID: 9926 RVA: 0x000CE4EC File Offset: 0x000CC8EC
		public void PlayIn()
		{
			for (int i = 0; i < this.m_Anim.Length; i++)
			{
				this.m_Anim[i].PlayIn();
			}
		}

		// Token: 0x060026C7 RID: 9927 RVA: 0x000CE520 File Offset: 0x000CC920
		public void PlayOut()
		{
			for (int i = 0; i < this.m_Anim.Length; i++)
			{
				this.m_Anim[i].PlayOut();
			}
		}

		// Token: 0x060026C8 RID: 9928 RVA: 0x000CE553 File Offset: 0x000CC953
		public void RequestRecastRecoverSound()
		{
			this.m_RequestRecastSE = true;
		}

		// Token: 0x060026C9 RID: 9929 RVA: 0x000CE55C File Offset: 0x000CC95C
		public void SetRecastSaveData(CharacterBattle charaBattle, int idx, int recast)
		{
			for (int i = 0; i < this.m_RecastDataList.Count; i++)
			{
				if (this.m_RecastDataList[i].m_CharaBattle == charaBattle)
				{
					this.m_RecastDataList[i].m_Recasts[idx] = recast;
					return;
				}
			}
			CommandSet.RecastData recastData = new CommandSet.RecastData();
			recastData.m_CharaBattle = charaBattle;
			for (int j = 0; j < recastData.m_Recasts.Length; j++)
			{
				recastData.m_Recasts[j] = 0;
			}
			recastData.m_Recasts[idx] = recast;
			this.m_RecastDataList.Add(recastData);
		}

		// Token: 0x060026CA RID: 9930 RVA: 0x000CE5F8 File Offset: 0x000CC9F8
		public int GetRecastSaveData(CharacterBattle charaBattle, int idx)
		{
			for (int i = 0; i < this.m_RecastDataList.Count; i++)
			{
				if (this.m_RecastDataList[i].m_CharaBattle == charaBattle)
				{
					return this.m_RecastDataList[i].m_Recasts[idx];
				}
			}
			return 0;
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x000CE650 File Offset: 0x000CCA50
		public void ShiftDown(bool flg, bool immediate = false)
		{
			if (!immediate)
			{
				if (flg)
				{
					Hashtable hashtable = new Hashtable();
					hashtable.Add("isLocal", true);
					hashtable.Add("x", 0f);
					hashtable.Add("y", -180f);
					hashtable.Add("time", 0.1f);
					hashtable.Add("easeType", iTween.EaseType.easeOutCubic);
					iTween.MoveTo(this.m_ShiftMoveObj, hashtable);
					for (int i = 0; i < this.m_ShiftAnims.Length; i++)
					{
						this.m_ShiftAnims[i].PlayOut();
					}
				}
				else
				{
					Hashtable hashtable2 = new Hashtable();
					hashtable2.Add("isLocal", true);
					hashtable2.Add("x", 0f);
					hashtable2.Add("y", 0f);
					hashtable2.Add("time", 0.1f);
					hashtable2.Add("easeType", iTween.EaseType.easeOutCubic);
					iTween.MoveTo(this.m_ShiftMoveObj, hashtable2);
					for (int j = 0; j < this.m_ShiftAnims.Length; j++)
					{
						this.m_ShiftAnims[j].PlayIn();
					}
				}
			}
			else if (flg)
			{
				this.m_ShiftMoveObj.GetComponent<RectTransform>().localPosition = new Vector3(0f, -180f, 0f);
				for (int k = 0; k < this.m_ShiftAnims.Length; k++)
				{
					this.m_ShiftAnims[k].Hide();
				}
			}
			else
			{
				this.m_ShiftMoveObj.GetComponent<RectTransform>().localPosition = Vector3.zero;
				for (int l = 0; l < this.m_ShiftAnims.Length; l++)
				{
					this.m_ShiftAnims[l].Show();
				}
			}
		}

		// Token: 0x04002D46 RID: 11590
		public const int COMMAND_MAX = 4;

		// Token: 0x04002D47 RID: 11591
		private const float SHIFT_MOVE = 180f;

		// Token: 0x04002D48 RID: 11592
		[SerializeField]
		private float m_CommandSpace;

		// Token: 0x04002D49 RID: 11593
		[SerializeField]
		private Sprite m_SpriteNormalAttack;

		// Token: 0x04002D4A RID: 11594
		[SerializeField]
		private Sprite m_SpriteNormalMagicAttack;

		// Token: 0x04002D4B RID: 11595
		[SerializeField]
		private Sprite m_SpriteSkillAttack;

		// Token: 0x04002D4C RID: 11596
		[SerializeField]
		private Sprite m_SpriteSkillMagic;

		// Token: 0x04002D4D RID: 11597
		[SerializeField]
		private Sprite m_SpriteSkillRecovery;

		// Token: 0x04002D4E RID: 11598
		[SerializeField]
		private Sprite m_SpriteSkillBuff;

		// Token: 0x04002D4F RID: 11599
		[SerializeField]
		private Sprite m_SpriteSkillDebuff;

		// Token: 0x04002D50 RID: 11600
		[SerializeField]
		private Sprite m_SpriteSkillOther;

		// Token: 0x04002D51 RID: 11601
		[SerializeField]
		private CommandButton m_CommandButtonPrefab;

		// Token: 0x04002D52 RID: 11602
		private CommandButton[] m_CommandButtons;

		// Token: 0x04002D53 RID: 11603
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x04002D54 RID: 11604
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04002D55 RID: 11605
		[SerializeField]
		private AnimUIPlayer[] m_Anim;

		// Token: 0x04002D56 RID: 11606
		[SerializeField]
		[Tooltip("スタン演出時等のMoveTween対象")]
		private GameObject m_ShiftMoveObj;

		// Token: 0x04002D57 RID: 11607
		[SerializeField]
		[Tooltip("スタン演出時等のOut対象")]
		private AnimUIPlayer[] m_ShiftAnims;

		// Token: 0x04002D58 RID: 11608
		private bool m_RequestRecastSE;

		// Token: 0x04002D59 RID: 11609
		private List<CommandSet.RecastData> m_RecastDataList = new List<CommandSet.RecastData>();

		// Token: 0x02000770 RID: 1904
		private class RecastData
		{
			// Token: 0x04002D5A RID: 11610
			public CharacterBattle m_CharaBattle;

			// Token: 0x04002D5B RID: 11611
			public int[] m_Recasts = new int[4];
		}
	}
}
