﻿using System;
using Star.UI.Window;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200077E RID: 1918
	public class MemberChange : MonoBehaviour
	{
		// Token: 0x06002704 RID: 9988 RVA: 0x000CF950 File Offset: 0x000CDD50
		public void Setup(int masterOrbID)
		{
			this.m_GameObject = base.gameObject;
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			for (int i = 0; i < this.m_BenchStatus.Length; i++)
			{
				this.m_BenchStatus[i].Setup(i);
			}
			this.m_GameObject.SetActive(false);
			this.m_StateController.Setup(8, 0, true);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 2, true);
			this.m_StateController.AddTransit(1, 3, true);
			this.m_StateController.AddTransit(2, 3, true);
			this.m_StateController.AddTransit(3, 0, true);
			this.m_StateController.AddTransit(3, 1, true);
			this.m_StateController.AddTransitEach(2, 5, true);
			this.m_StateController.AddTransit(2, 4, true);
			this.m_StateController.AddTransitEach(4, 3, true);
			this.m_StateController.AddTransit(5, 7, true);
			this.m_StateController.AddTransit(5, 6, true);
			this.m_StateController.AddTransit(6, 7, true);
			this.m_StateController.AddTransit(6, 2, true);
			this.m_StateController.AddTransit(6, 3, true);
			this.m_StateController.AddTransit(7, 0, true);
			this.m_TargetWindow.OnDecide += this.OnDecideTarget;
			for (int j = 0; j < this.m_PlayerTargetState.Length; j++)
			{
				this.m_PlayerTargetState[j] = new MemberChange.TargetState();
			}
			for (int k = 0; k < this.m_EnemyTargetState.Length; k++)
			{
				this.m_EnemyTargetState[k] = new MemberChange.TargetState();
			}
			this.m_CutIn.Prepare(masterOrbID, 0);
			this.m_ConfirmGroup.OnEnd += this.OnEndConfirmWindow;
		}

		// Token: 0x06002705 RID: 9989 RVA: 0x000CFB10 File Offset: 0x000CDF10
		public void SetPlayerTargetState(int join, int charaID, bool isInteractable)
		{
			if (charaID != -1)
			{
				this.m_PlayerTargetState[join].isActive = true;
				this.m_PlayerTargetState[join].id = charaID;
				this.m_PlayerTargetState[join].isInteractable = isInteractable;
			}
			else
			{
				this.m_PlayerTargetState[join].isActive = false;
				this.m_PlayerTargetState[join].id = charaID;
				this.m_PlayerTargetState[join].isInteractable = isInteractable;
			}
		}

		// Token: 0x06002706 RID: 9990 RVA: 0x000CFB80 File Offset: 0x000CDF80
		public void SetEnemyTargetState(int join, int resourceID, bool isInteractable)
		{
			if (resourceID != -1)
			{
				this.m_EnemyTargetState[join].isActive = true;
				this.m_EnemyTargetState[join].id = resourceID;
				this.m_EnemyTargetState[join].isInteractable = isInteractable;
			}
			else
			{
				this.m_EnemyTargetState[join].isActive = false;
				this.m_EnemyTargetState[join].id = resourceID;
				this.m_EnemyTargetState[join].isInteractable = isInteractable;
			}
		}

		// Token: 0x06002707 RID: 9991 RVA: 0x000CFBF0 File Offset: 0x000CDFF0
		public void SetMasterOrb(BattleMasterOrbData masterOrbData, bool executed)
		{
			this.m_MasterOrbData = masterOrbData;
			for (int i = 0; i < 3; i++)
			{
				this.m_MasterSkillPanel[i].gameObject.SetActive(false);
			}
			if (this.m_MasterOrbData.OrbID == -1)
			{
				this.m_MasterOrbIcon.Apply(this.m_MasterOrbData.OrbID);
				this.m_MasterOrbLevelTextObj.enabled = false;
				this.m_MasterOrbNameTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleMasterNoOrb);
			}
			else
			{
				MasterOrbListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(this.m_MasterOrbData.OrbID);
				this.m_MasterOrbIcon.Apply(this.m_MasterOrbData.OrbID);
				this.m_MasterOrbNameTextObj.enabled = true;
				this.m_MasterOrbNameTextObj.text = param.m_Name;
				this.m_MasterOrbLevelTextObj.enabled = true;
				this.m_MasterOrbLevelTextObj.text = this.m_MasterOrbData.OrbLv + " / " + param.m_LimitLv;
				for (int j = 0; j < 3; j++)
				{
					this.m_MasterSkillPanel[j].gameObject.SetActive(true);
					if (j < this.m_MasterOrbData.CommandNum)
					{
						this.m_MasterSkillPanel[j].SetInteractable(!executed);
						BattleCommandData commandDataAt = this.m_MasterOrbData.GetCommandDataAt(j);
						this.m_MasterSkillPanel[j].SetData(commandDataAt.SkillParam.m_SkillName, commandDataAt.SkillParam.m_SkillDetail);
					}
					else
					{
						this.m_MasterSkillPanel[j].SetInteractable(false);
						this.m_MasterSkillPanel[j].SetData(string.Empty, string.Empty);
					}
				}
			}
		}

		// Token: 0x06002708 RID: 9992 RVA: 0x000CFDBC File Offset: 0x000CE1BC
		public void Destroy()
		{
			for (int i = 0; i < this.m_BenchStatus.Length; i++)
			{
				this.m_BenchStatus[i].Destroy();
			}
			this.m_CutIn.Destroy();
		}

		// Token: 0x06002709 RID: 9993 RVA: 0x000CFDFC File Offset: 0x000CE1FC
		public void PlayIn()
		{
			this.m_MainGroup.Open();
			this.SetState(MemberChange.eState.In);
			this.m_BG.gameObject.SetActive(true);
			for (int i = 0; i < this.m_Anim.Length; i++)
			{
				this.m_Anim[i].PlayIn();
			}
			this.m_MasterSkillWindow.Open();
			Vector3 localScale = this.m_UI.RectTransform.localScale;
			this.m_CutIn.GetComponent<RectTransform>().localScale = new Vector3(1f / localScale.x * 5f / 3.75f, 1f / localScale.y * 5f / 3.75f, 0.01f);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_WINDOW_OPEN, 1f, 0, -1, -1);
			this.m_MainGroup.SetEnableInput(false);
		}

		// Token: 0x0600270A RID: 9994 RVA: 0x000CFEE4 File Offset: 0x000CE2E4
		public void PlayOut()
		{
			this.PlayOutOnCutIn();
			this.m_CutIn.HideChara();
		}

		// Token: 0x0600270B RID: 9995 RVA: 0x000CFEF8 File Offset: 0x000CE2F8
		public void PlayOutOnCutIn()
		{
			this.SetState(MemberChange.eState.Out);
			this.m_BG.gameObject.SetActive(false);
			for (int i = 0; i < this.m_Anim.Length; i++)
			{
				this.m_Anim[i].PlayOut();
			}
			this.m_MasterSkillWindow.Close();
		}

		// Token: 0x0600270C RID: 9996 RVA: 0x000CFF50 File Offset: 0x000CE350
		public bool IsAnimPlaying()
		{
			for (int i = 0; i < this.m_Anim.Length; i++)
			{
				if (this.m_Anim[i].State != 0 && this.m_Anim[i].State != 2)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600270D RID: 9997 RVA: 0x000CFFA0 File Offset: 0x000CE3A0
		private void Update()
		{
			switch (this.m_StateController.NowState)
			{
			case 1:
			{
				bool flag = true;
				for (int i = 0; i < this.m_Anim.Length; i++)
				{
					if (this.m_Anim[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_CutIn.IsCompletePrepare())
				{
					flag = false;
				}
				if (flag)
				{
					BattleTutorial.eSeq seq = this.m_UI.BattleTutorial.GetSeq();
					if (seq == BattleTutorial.eSeq._08_WAVE_IDX_1_OWNER_IMAGE_SLIDE)
					{
						UIUtility.DetachCanvasForChangeRenderOrder(this.m_UI.GetOwnerImageObject(), true);
						MasterSkillPanel masterSkillPanel = this.m_UI.GetMemberChange().GetMasterSkillPanel(0);
						TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
						tutorialMessage.Open(eTutorialMessageListDB.BTL_SKILL_ICON);
						tutorialMessage.SetEnableArrow(true);
						tutorialMessage.SetArrowPosition(masterSkillPanel.transform.position);
						tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.RightBottom);
						tutorialMessage.SetEnableTrace(false);
						UIUtility.AttachCanvasForChangeRenderOrder(masterSkillPanel.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
					}
					this.m_CutIn.ShowChara();
					this.SetState(MemberChange.eState.Wait);
					this.m_MainGroup.SetEnableInput(true);
				}
				break;
			}
			case 2:
				this.UpdateAndroidBackKey();
				break;
			case 3:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_Anim.Length; j++)
				{
					if (this.m_Anim[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.SetState(MemberChange.eState.Hide);
				}
				break;
			}
			case 5:
				if (this.m_ConfirmGroup.IsDonePlayOut)
				{
				}
				break;
			case 6:
				if (this.m_TargetWindow.IsDonePlayOut)
				{
					if (this.m_SelectMasterSkillTarget != BattleDefine.eJoinMember.None)
					{
						this.m_CutIn.Play(this.m_SelectMasterSkillCommandIndex);
						this.PlayOutOnCutIn();
						this.SetState(MemberChange.eState.MasterCutIn);
					}
					else
					{
						this.SetState(MemberChange.eState.Wait);
						this.m_MainGroup.SetEnableInput(true);
					}
				}
				break;
			case 7:
				if (this.m_CutIn.IsCompletePlay() && !this.IsAnimPlaying())
				{
					this.m_UI.DecideMasterSkill(this.m_SelectMasterSkillCommandIndex, this.m_SelectMasterSkillTarget);
					this.SetState(MemberChange.eState.Hide);
				}
				break;
			}
		}

		// Token: 0x0600270E RID: 9998 RVA: 0x000D01F8 File Offset: 0x000CE5F8
		private bool SetState(MemberChange.eState state)
		{
			if (this.m_StateController.ChangeState((int)state))
			{
				switch (state)
				{
				case MemberChange.eState.Hide:
					this.m_GameObject.SetActive(false);
					this.SetEnableInput(false);
					break;
				case MemberChange.eState.In:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(false);
					break;
				case MemberChange.eState.Wait:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(true);
					this.m_MainGroup.SetEnableInput(true);
					break;
				case MemberChange.eState.Out:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(false);
					break;
				}
				return true;
			}
			return false;
		}

		// Token: 0x0600270F RID: 9999 RVA: 0x000D02A0 File Offset: 0x000CE6A0
		public void OnClickBackButtonCallBack()
		{
			if (this.m_StateController.NowState != 2)
			{
				return;
			}
			this.m_CutIn.HideChara();
			this.m_UI.CancelMemberChange();
		}

		// Token: 0x06002710 RID: 10000 RVA: 0x000D02CA File Offset: 0x000CE6CA
		public MasterSkillPanel GetMasterSkillPanel(int index)
		{
			if (index >= 0 && index < this.m_MasterSkillPanel.Length)
			{
				return this.m_MasterSkillPanel[index];
			}
			return null;
		}

		// Token: 0x06002711 RID: 10001 RVA: 0x000D02EB File Offset: 0x000CE6EB
		public BenchStatus GetBenchStatus(BattleDefine.eJoinMember member)
		{
			return this.m_BenchStatus[member - BattleDefine.eJoinMember.Bench_1];
		}

		// Token: 0x06002712 RID: 10002 RVA: 0x000D02F7 File Offset: 0x000CE6F7
		public void SetEnableInput(bool flg)
		{
			this.m_CanvasGroup.blocksRaycasts = flg;
		}

		// Token: 0x06002713 RID: 10003 RVA: 0x000D0305 File Offset: 0x000CE705
		private void CloseSkillConfirm()
		{
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x06002714 RID: 10004 RVA: 0x000D0312 File Offset: 0x000CE712
		public void DecideMember(int idx)
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_UI.DecideMemberChange(idx);
			}
		}

		// Token: 0x06002715 RID: 10005 RVA: 0x000D0334 File Offset: 0x000CE734
		public void OnClickMasterSkill(int idx)
		{
			if (this.m_StateController.ChangeState(5))
			{
				this.m_SelectMasterSkillCommandIndex = idx;
				BattleCommandData commandDataAt = this.m_MasterOrbData.GetCommandDataAt(this.m_SelectMasterSkillCommandIndex);
				this.m_ConfirmSkillNameText.text = commandDataAt.SkillParam.m_SkillName;
				this.m_ConfirmGroup.Open();
				BattleTutorial.eSeq seq = this.m_UI.BattleTutorial.GetSeq();
				if (seq == BattleTutorial.eSeq._08_WAVE_IDX_1_OWNER_IMAGE_SLIDE)
				{
					MasterSkillPanel masterSkillPanel = this.GetMasterSkillPanel(0);
					UIUtility.DetachCanvasForChangeRenderOrder(masterSkillPanel.gameObject, true);
					this.m_ConfirmGroup.OnIdle += this.OnIdleConfirmWindow;
				}
			}
		}

		// Token: 0x06002716 RID: 10006 RVA: 0x000D03D4 File Offset: 0x000CE7D4
		public void OnIdleConfirmWindow()
		{
			this.m_ConfirmGroup.OnIdle -= this.OnIdleConfirmWindow;
			BattleTutorial.eSeq seq = this.m_UI.BattleTutorial.GetSeq();
			if (seq == BattleTutorial.eSeq._08_WAVE_IDX_1_OWNER_IMAGE_SLIDE)
			{
				CustomButton executeSkillButton = this.m_ExecuteSkillButton;
				if (executeSkillButton != null)
				{
					TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
					tutorialMessage.SetEnableArrow(true);
					tutorialMessage.SetArrowPosition(executeSkillButton.transform.position);
					tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.RightBottom);
					UIUtility.AttachCanvasForChangeRenderOrder(executeSkillButton.gameObject, tutorialMessage.GetDarkCanvasSortingOrder() + 1, true);
				}
			}
		}

		// Token: 0x06002717 RID: 10007 RVA: 0x000D0464 File Offset: 0x000CE864
		public void OnClickConfirmSkillButtonCallBack()
		{
			this.CloseSkillConfirm();
			BattleTutorial.eSeq seq = this.m_UI.BattleTutorial.GetSeq();
			if (seq == BattleTutorial.eSeq._08_WAVE_IDX_1_OWNER_IMAGE_SLIDE)
			{
				FooterButton footerButton = this.m_ConfirmGroup.Window.GetFooterButton(0);
				if (footerButton != null)
				{
					UIUtility.DetachCanvasForChangeRenderOrder(footerButton.gameObject, true);
				}
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.Clear();
				this.m_UI.BattleTutorial.ApplyNextSeq();
			}
		}

		// Token: 0x06002718 RID: 10008 RVA: 0x000D04DB File Offset: 0x000CE8DB
		public void OnClickCancelSkillButtonCallBack()
		{
			this.m_SelectMasterSkillCommandIndex = -1;
			this.CloseSkillConfirm();
		}

		// Token: 0x06002719 RID: 10009 RVA: 0x000D04EC File Offset: 0x000CE8EC
		public void OnEndConfirmWindow()
		{
			if (this.m_SelectMasterSkillCommandIndex != -1)
			{
				BattleCommandData commandDataAt = this.m_MasterOrbData.GetCommandDataAt(this.m_SelectMasterSkillCommandIndex);
				eSkillTargetType mainSkillTargetType = commandDataAt.MainSkillTargetType;
				if (mainSkillTargetType != eSkillTargetType.MySingle && mainSkillTargetType != eSkillTargetType.TgtSingle)
				{
					this.m_CutIn.Play(this.m_SelectMasterSkillCommandIndex);
					this.PlayOutOnCutIn();
					this.SetState(MemberChange.eState.MasterCutIn);
				}
				else if (this.m_StateController.ChangeState(6))
				{
					this.OpenTargetWindow();
				}
			}
			else
			{
				this.SetState(MemberChange.eState.Wait);
				this.m_MainGroup.SetEnableInput(true);
			}
		}

		// Token: 0x0600271A RID: 10010 RVA: 0x000D058C File Offset: 0x000CE98C
		private void SetTargetState(int idx, bool isDisplay, eCharaResourceType resType, int id, bool interactable)
		{
			if (isDisplay)
			{
				if (resType == eCharaResourceType.Player)
				{
					this.m_TargetWindow.SetActive(idx, isDisplay);
					this.m_TargetWindow.SetPlayerIcon(idx, id);
				}
				else if (resType == eCharaResourceType.Enemy)
				{
					this.m_TargetWindow.SetActive(idx, isDisplay);
					this.m_TargetWindow.SetEnemyIcon(idx, id);
				}
			}
			else
			{
				this.m_TargetWindow.SetActive(idx, false);
			}
			this.m_TargetWindow.SetInteractable(idx, interactable);
		}

		// Token: 0x0600271B RID: 10011 RVA: 0x000D0608 File Offset: 0x000CEA08
		public void OpenTargetWindow()
		{
			BattleCommandData commandDataAt = this.m_MasterOrbData.GetCommandDataAt(this.m_SelectMasterSkillCommandIndex);
			if (commandDataAt.MainSkillTargetType == eSkillTargetType.MySingle)
			{
				this.m_TargetWindow.SetMode(MasterSkillTargetWindow.eMode.Player);
				for (int i = 0; i < this.m_PlayerTargetState.Length; i++)
				{
					this.SetTargetState(i, this.m_PlayerTargetState[i].isActive, eCharaResourceType.Player, this.m_PlayerTargetState[i].id, this.m_PlayerTargetState[i].isInteractable);
				}
			}
			else
			{
				this.m_TargetWindow.SetMode(MasterSkillTargetWindow.eMode.Enemy);
				for (int j = 0; j < this.m_EnemyTargetState.Length; j++)
				{
					this.SetTargetState(j, this.m_EnemyTargetState[j].isActive, eCharaResourceType.Enemy, this.m_EnemyTargetState[j].id, this.m_EnemyTargetState[j].isInteractable);
				}
			}
			this.m_TargetWindow.Open();
		}

		// Token: 0x0600271C RID: 10012 RVA: 0x000D06EB File Offset: 0x000CEAEB
		public void OnClickTargetSelectFootButtonCallBack()
		{
			if (this.m_TargetWindow.LastPressedFooterButtonID == 0)
			{
				this.CloseSkillConfirm();
			}
			else
			{
				this.m_SelectMasterSkillCommandIndex = -1;
				this.CloseSkillConfirm();
			}
		}

		// Token: 0x0600271D RID: 10013 RVA: 0x000D0715 File Offset: 0x000CEB15
		public void OnDecideTarget(int targetIdx)
		{
			this.m_SelectMasterSkillTarget = (BattleDefine.eJoinMember)targetIdx;
			this.m_TargetWindow.Close();
		}

		// Token: 0x0600271E RID: 10014 RVA: 0x000D0729 File Offset: 0x000CEB29
		public void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BackButton, null);
		}

		// Token: 0x04002DB3 RID: 11699
		private const int MASTER_SKILL_MAX = 3;

		// Token: 0x04002DB4 RID: 11700
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04002DB5 RID: 11701
		[SerializeField]
		private BenchStatus[] m_BenchStatus;

		// Token: 0x04002DB6 RID: 11702
		[SerializeField]
		private Image m_BG;

		// Token: 0x04002DB7 RID: 11703
		[SerializeField]
		private AnimUIPlayer[] m_Anim;

		// Token: 0x04002DB8 RID: 11704
		[SerializeField]
		private UIGroup m_MasterSkillWindow;

		// Token: 0x04002DB9 RID: 11705
		[SerializeField]
		private OrbIcon m_MasterOrbIcon;

		// Token: 0x04002DBA RID: 11706
		[SerializeField]
		private Text m_MasterOrbLevelTextObj;

		// Token: 0x04002DBB RID: 11707
		[SerializeField]
		private Text m_MasterOrbNameTextObj;

		// Token: 0x04002DBC RID: 11708
		[SerializeField]
		private MasterSkillPanel[] m_MasterSkillPanel;

		// Token: 0x04002DBD RID: 11709
		[SerializeField]
		private Sprite m_SpriteSkillAttack;

		// Token: 0x04002DBE RID: 11710
		[SerializeField]
		private Sprite m_SpriteSkillMagic;

		// Token: 0x04002DBF RID: 11711
		[SerializeField]
		private Sprite m_SpriteSkillRecovery;

		// Token: 0x04002DC0 RID: 11712
		[SerializeField]
		private Sprite m_SpriteSkillBuff;

		// Token: 0x04002DC1 RID: 11713
		[SerializeField]
		private Sprite m_SpriteSkillDebuff;

		// Token: 0x04002DC2 RID: 11714
		[SerializeField]
		private Sprite m_SpriteSkillOther;

		// Token: 0x04002DC3 RID: 11715
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04002DC4 RID: 11716
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04002DC5 RID: 11717
		[SerializeField]
		private UIGroup m_ConfirmGroup;

		// Token: 0x04002DC6 RID: 11718
		[SerializeField]
		private MasterSkillTargetWindow m_TargetWindow;

		// Token: 0x04002DC7 RID: 11719
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x04002DC8 RID: 11720
		[SerializeField]
		private BattleMasterSkillCutIn m_CutIn;

		// Token: 0x04002DC9 RID: 11721
		[SerializeField]
		private Text m_ConfirmSkillNameText;

		// Token: 0x04002DCA RID: 11722
		[SerializeField]
		private CustomButton m_ExecuteSkillButton;

		// Token: 0x04002DCB RID: 11723
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04002DCC RID: 11724
		private GameObject m_GameObject;

		// Token: 0x04002DCD RID: 11725
		private BattleMasterOrbData m_MasterOrbData;

		// Token: 0x04002DCE RID: 11726
		private int m_SelectMasterSkillCommandIndex = -1;

		// Token: 0x04002DCF RID: 11727
		private BattleDefine.eJoinMember m_SelectMasterSkillTarget = BattleDefine.eJoinMember.None;

		// Token: 0x04002DD0 RID: 11728
		private MemberChange.TargetState[] m_PlayerTargetState = new MemberChange.TargetState[3];

		// Token: 0x04002DD1 RID: 11729
		private MemberChange.TargetState[] m_EnemyTargetState = new MemberChange.TargetState[3];

		// Token: 0x0200077F RID: 1919
		private enum eState
		{
			// Token: 0x04002DD3 RID: 11731
			Hide,
			// Token: 0x04002DD4 RID: 11732
			In,
			// Token: 0x04002DD5 RID: 11733
			Wait,
			// Token: 0x04002DD6 RID: 11734
			Out,
			// Token: 0x04002DD7 RID: 11735
			MemberChange,
			// Token: 0x04002DD8 RID: 11736
			SkillConfirm,
			// Token: 0x04002DD9 RID: 11737
			TargetSelect,
			// Token: 0x04002DDA RID: 11738
			MasterCutIn,
			// Token: 0x04002DDB RID: 11739
			Num
		}

		// Token: 0x02000780 RID: 1920
		private class TargetState
		{
			// Token: 0x04002DDC RID: 11740
			public bool isActive;

			// Token: 0x04002DDD RID: 11741
			public int id;

			// Token: 0x04002DDE RID: 11742
			public bool isInteractable;
		}
	}
}
