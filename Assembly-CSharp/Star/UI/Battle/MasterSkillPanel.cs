﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200077B RID: 1915
	public class MasterSkillPanel : MonoBehaviour
	{
		// Token: 0x060026F4 RID: 9972 RVA: 0x000CF5DA File Offset: 0x000CD9DA
		public void SetData(string text, string detail)
		{
			this.m_NameTextObj.text = text;
			this.m_DetailTextObj.text = detail;
		}

		// Token: 0x060026F5 RID: 9973 RVA: 0x000CF5F4 File Offset: 0x000CD9F4
		public void SetInteractable(bool flg)
		{
			this.m_CustomButton.Interactable = flg;
		}

		// Token: 0x04002DA8 RID: 11688
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x04002DA9 RID: 11689
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04002DAA RID: 11690
		[SerializeField]
		private Text m_DetailTextObj;
	}
}
