﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200079E RID: 1950
	public class UniqueSkillNameWindow : MonoBehaviour
	{
		// Token: 0x060027D5 RID: 10197 RVA: 0x000D5311 File Offset: 0x000D3711
		public void Setup()
		{
			this.m_NameTextGameObj = this.m_NameTextObj.gameObject;
			this.m_RubyTextGameObj = this.m_RubyTextObj.gameObject;
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x060027D6 RID: 10198 RVA: 0x000D5341 File Offset: 0x000D3741
		private void Update()
		{
			this.m_DisplayTimeCount += Time.deltaTime;
			if (this.m_DisplayTimeCount > 2f)
			{
				this.m_DisplayTimeCount = 0f;
				this.m_GameObject.SetActive(false);
			}
		}

		// Token: 0x060027D7 RID: 10199 RVA: 0x000D537C File Offset: 0x000D377C
		public void SetText(string name, string ruby, int Lv)
		{
			this.m_GameObject.SetActive(true);
			if (!string.IsNullOrEmpty(name))
			{
				this.m_NameTextGameObj.SetActive(true);
				this.m_NameTextObj.text = name;
			}
			else
			{
				this.m_NameTextGameObj.SetActive(false);
			}
			if (!string.IsNullOrEmpty(ruby))
			{
				this.m_RubyTextGameObj.SetActive(true);
				this.m_RubyTextObj.text = ruby;
			}
			else
			{
				this.m_RubyTextGameObj.SetActive(false);
			}
			this.m_LvTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Lv) + Lv.ToString();
		}

		// Token: 0x04002EF3 RID: 12019
		private const float DISPLAY_TIME_MAX = 2f;

		// Token: 0x04002EF4 RID: 12020
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04002EF5 RID: 12021
		[SerializeField]
		private Text m_RubyTextObj;

		// Token: 0x04002EF6 RID: 12022
		[SerializeField]
		private Text m_LvTextObj;

		// Token: 0x04002EF7 RID: 12023
		private GameObject m_NameTextGameObj;

		// Token: 0x04002EF8 RID: 12024
		private GameObject m_RubyTextGameObj;

		// Token: 0x04002EF9 RID: 12025
		private GameObject m_GameObject;

		// Token: 0x04002EFA RID: 12026
		private float m_DisplayTimeCount;
	}
}
