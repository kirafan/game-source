﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000759 RID: 1881
	public class BattleOrderFrameBase : MonoBehaviour
	{
		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06002574 RID: 9588 RVA: 0x000C7EFB File Offset: 0x000C62FB
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.GetComponent<GameObject>();
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06002575 RID: 9589 RVA: 0x000C7F20 File Offset: 0x000C6320
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06002576 RID: 9590 RVA: 0x000C7F45 File Offset: 0x000C6345
		public float Alpha
		{
			get
			{
				return this.m_CanvasGroup.alpha;
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06002578 RID: 9592 RVA: 0x000C7F5B File Offset: 0x000C635B
		// (set) Token: 0x06002577 RID: 9591 RVA: 0x000C7F52 File Offset: 0x000C6352
		public float OrderValue
		{
			get
			{
				return this.m_OrderValue;
			}
			set
			{
				this.m_OrderValue = value;
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06002579 RID: 9593 RVA: 0x000C7F63 File Offset: 0x000C6363
		public bool IsChaseMove
		{
			get
			{
				return this.m_ChaseTarget != null;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x0600257A RID: 9594 RVA: 0x000C7F71 File Offset: 0x000C6371
		// (set) Token: 0x0600257B RID: 9595 RVA: 0x000C7F79 File Offset: 0x000C6379
		public bool IsEnableJump { get; set; }

		// Token: 0x0600257C RID: 9596 RVA: 0x000C7F82 File Offset: 0x000C6382
		public virtual bool IsChara()
		{
			return false;
		}

		// Token: 0x0600257D RID: 9597 RVA: 0x000C7F85 File Offset: 0x000C6385
		public virtual bool IsCard()
		{
			return false;
		}

		// Token: 0x0600257E RID: 9598 RVA: 0x000C7F88 File Offset: 0x000C6388
		public virtual void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			this.m_Blink.enabled = false;
		}

		// Token: 0x0600257F RID: 9599 RVA: 0x000C7FC0 File Offset: 0x000C63C0
		public float GetGoalPos()
		{
			if (this.m_MoveParam != null)
			{
				return this.m_MoveParam.m_Goal;
			}
			return this.RectTransform.localPosition.x;
		}

		// Token: 0x06002580 RID: 9600 RVA: 0x000C7FF8 File Offset: 0x000C63F8
		public Vector2 GetNextPos(float sec)
		{
			float num = this.GetGoalPos() - this.RectTransform.localPosition.x;
			float num2 = 1f;
			if (num != 0f)
			{
				num2 = num / Mathf.Abs(num);
			}
			float num3 = Mathf.Abs(num);
			if (this.m_MoveParam != null && num3 > this.m_MoveParam.m_Speed * sec)
			{
				num3 = this.m_MoveParam.m_Speed * sec;
			}
			return new Vector2(this.RectTransform.localPosition.x + num3 * num2, this.RectTransform.localPosition.y);
		}

		// Token: 0x06002581 RID: 9601 RVA: 0x000C80A0 File Offset: 0x000C64A0
		public void MoverCollision(float sec, out float left, out float right)
		{
			float x = this.GetNextPos(sec).x;
			float x2 = this.m_RectTransform.localPosition.x;
			if (x < x2)
			{
				left = x;
				right = x2;
			}
			else
			{
				left = x2;
				right = x;
			}
			left -= this.m_RectTransform.rect.width * 0.495f;
			right += this.m_RectTransform.rect.width * 0.495f;
		}

		// Token: 0x06002582 RID: 9602 RVA: 0x000C8127 File Offset: 0x000C6527
		public virtual void Destroy()
		{
		}

		// Token: 0x06002583 RID: 9603 RVA: 0x000C8129 File Offset: 0x000C6529
		public bool IsPlayer()
		{
			return this.m_IsPlayer;
		}

		// Token: 0x06002584 RID: 9604 RVA: 0x000C8134 File Offset: 0x000C6534
		private bool UpdateMoveX()
		{
			if (this.m_MoveParam == null)
			{
				return true;
			}
			if (this.IsMoveEnd())
			{
				this.m_RectTransform.localPosX(this.m_MoveParam.m_Goal);
				return true;
			}
			if (this.m_RectTransform.localPosition.x < this.m_MoveParam.m_Goal)
			{
				this.m_RectTransform.localPosX(this.m_RectTransform.localPosition.x + this.m_MoveParam.m_Speed * Time.deltaTime);
			}
			else if (this.m_RectTransform.localPosition.x > this.m_MoveParam.m_Goal)
			{
				this.m_RectTransform.localPosX(this.m_RectTransform.localPosition.x - this.m_MoveParam.m_Speed * Time.deltaTime);
			}
			return false;
		}

		// Token: 0x06002585 RID: 9605 RVA: 0x000C8220 File Offset: 0x000C6620
		private bool IsMoveEnd()
		{
			float num = Mathf.Abs(this.m_RectTransform.localPosition.x - this.m_MoveParam.m_Goal);
			return num <= this.m_MoveParam.m_Speed * Time.deltaTime;
		}

		// Token: 0x06002586 RID: 9606 RVA: 0x000C826C File Offset: 0x000C666C
		private void UpdateWithMoveParam()
		{
			switch (this.m_DetourState)
			{
			case BattleOrderFrameBase.eDetourState.None:
				if (this.m_MoveParam != null)
				{
					if (this.m_MoveType == BattleOrderFrameBase.eMoveType.Lane || this.m_MoveType == BattleOrderFrameBase.eMoveType.Free)
					{
						bool flag = this.UpdateMoveX();
						if (flag)
						{
							this.m_MoveParam = null;
						}
					}
					else if (this.IsMoveEnd())
					{
						this.m_MoveParam = null;
					}
				}
				break;
			case BattleOrderFrameBase.eDetourState.Down:
			{
				float num = 256f;
				if (this.m_IsFreeDetour)
				{
					num = 256f;
				}
				if (this.m_RectTransform.localPosition.y <= num * Time.deltaTime)
				{
					this.m_RectTransform.localPosY(0f);
					this.m_DetourState = BattleOrderFrameBase.eDetourState.None;
					this.m_MoveType = BattleOrderFrameBase.eMoveType.Free;
				}
				else
				{
					this.m_RectTransform.localPosY(this.m_RectTransform.localPosition.y - num * Time.deltaTime);
				}
				break;
			}
			case BattleOrderFrameBase.eDetourState.Up:
			{
				float num2 = 256f;
				if (this.m_IsFreeDetour)
				{
					num2 = 2048f;
				}
				if (56f - this.m_RectTransform.localPosition.y <= num2 * Time.deltaTime)
				{
					this.m_RectTransform.localPosY(56f);
					this.m_DetourState = BattleOrderFrameBase.eDetourState.Detour;
				}
				else
				{
					this.m_RectTransform.localPosY(this.m_RectTransform.localPosition.y + num2 * Time.deltaTime);
				}
				break;
			}
			case BattleOrderFrameBase.eDetourState.Detour:
				if (this.m_MoveParam != null)
				{
					bool flag2 = this.UpdateMoveX();
					if (flag2)
					{
						this.m_DetourState = BattleOrderFrameBase.eDetourState.Down;
					}
				}
				else if (this.m_ChaseTarget == null)
				{
					this.m_DetourState = BattleOrderFrameBase.eDetourState.Down;
				}
				break;
			}
		}

		// Token: 0x06002587 RID: 9607 RVA: 0x000C843E File Offset: 0x000C683E
		public virtual void Update()
		{
			this.UpdateWithMoveParam();
		}

		// Token: 0x06002588 RID: 9608 RVA: 0x000C8448 File Offset: 0x000C6848
		public virtual void LateUpdate()
		{
			if (this.m_DetourState != BattleOrderFrameBase.eDetourState.Up && this.m_ChaseTarget != null)
			{
				if (this.m_DetourState == BattleOrderFrameBase.eDetourState.None)
				{
					this.m_RectTransform.localPosition = this.m_ChaseTarget.localPosition;
				}
				else
				{
					float num = Mathf.Abs(this.m_RectTransform.localPosition.x - this.m_ChaseTarget.localPosition.x);
					if (num > this.m_ChaseSpeed * Time.deltaTime)
					{
						float num2 = (this.m_ChaseTarget.localPosition.x - this.m_RectTransform.localPosition.x) / num * this.m_ChaseSpeed * Time.deltaTime;
						this.m_RectTransform.localPosX(this.m_RectTransform.localPosition.x + num2);
					}
					else
					{
						this.m_RectTransform.localPosX(this.m_ChaseTarget.localPosition.x);
						this.m_ChaseSpeed = 100000f;
						if (this.m_DetourState != BattleOrderFrameBase.eDetourState.None)
						{
							this.MoveWithLaneChange(false, false);
						}
					}
				}
			}
		}

		// Token: 0x06002589 RID: 9609 RVA: 0x000C8573 File Offset: 0x000C6973
		public void SetCurrent(bool flg)
		{
			if (this.m_IsPlayer)
			{
				this.m_BaseColorGroup.ChangeColor(this.PLAYER_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
			}
			else
			{
				this.m_BaseColorGroup.ChangeColor(this.ENEMY_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
			}
		}

		// Token: 0x0600258A RID: 9610 RVA: 0x000C85B3 File Offset: 0x000C69B3
		public void SetActive(bool flag)
		{
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x0600258B RID: 9611 RVA: 0x000C85C1 File Offset: 0x000C69C1
		public void SetPos(Vector3 position)
		{
			this.StopMove();
			this.m_RectTransform.localPosition = position;
		}

		// Token: 0x0600258C RID: 9612 RVA: 0x000C85D5 File Offset: 0x000C69D5
		public void SetMoveType(BattleOrderFrameBase.eMoveType moveType)
		{
			this.m_MoveType = moveType;
		}

		// Token: 0x0600258D RID: 9613 RVA: 0x000C85DE File Offset: 0x000C69DE
		public void StopMove()
		{
			this.m_MoveType = BattleOrderFrameBase.eMoveType.FreeStop;
			this.m_RectTransform.localPosY(0f);
			this.m_DetourState = BattleOrderFrameBase.eDetourState.None;
			this.m_ChaseTarget = null;
		}

		// Token: 0x0600258E RID: 9614 RVA: 0x000C8605 File Offset: 0x000C6A05
		public void LaneMove(float goalPosX, float speed)
		{
			if (this.m_DetourState == BattleOrderFrameBase.eDetourState.None)
			{
				this.StopMove();
			}
			this.m_MoveType = BattleOrderFrameBase.eMoveType.Lane;
			this.MoveToConstSpeed(goalPosX, speed, false);
		}

		// Token: 0x0600258F RID: 9615 RVA: 0x000C8628 File Offset: 0x000C6A28
		public void FreeWaitMove(float goalPosX, float speed)
		{
			if (this.m_DetourState == BattleOrderFrameBase.eDetourState.None)
			{
				this.StopMove();
			}
			this.m_MoveType = BattleOrderFrameBase.eMoveType.FreeStop;
			this.MoveToConstSpeed(goalPosX, speed, false);
		}

		// Token: 0x06002590 RID: 9616 RVA: 0x000C864B File Offset: 0x000C6A4B
		public void FreeMove(float goalPosX, float speed)
		{
			if (this.m_DetourState == BattleOrderFrameBase.eDetourState.None)
			{
				this.StopMove();
			}
			this.m_MoveType = BattleOrderFrameBase.eMoveType.Free;
			this.MoveToConstSpeed(goalPosX, speed, false);
		}

		// Token: 0x06002591 RID: 9617 RVA: 0x000C866E File Offset: 0x000C6A6E
		public void MoveToConstSpeed(float goalPosX, float speed, bool detour)
		{
			this.m_ChaseTarget = null;
			this.m_MoveParam = new BattleOrderFrameBase.MoveParam(goalPosX, speed);
			this.MoveWithLaneChange(detour, false);
		}

		// Token: 0x06002592 RID: 9618 RVA: 0x000C868C File Offset: 0x000C6A8C
		public void MoveToKeepSpeed(float goalPosX, float speed = 0f)
		{
			this.m_ChaseTarget = null;
			if (this.m_MoveParam != null && this.m_MoveParam.m_Speed > 0f)
			{
				this.m_MoveParam = new BattleOrderFrameBase.MoveParam(goalPosX, this.m_MoveParam.m_Speed);
				this.MoveWithLaneChange(false, false);
			}
			else
			{
				this.MoveToConstSpeed(goalPosX, speed, false);
				this.m_MoveType = BattleOrderFrameBase.eMoveType.Free;
			}
		}

		// Token: 0x06002593 RID: 9619 RVA: 0x000C86F4 File Offset: 0x000C6AF4
		public void ChaseTo(RectTransform chaseTarget, float speed)
		{
			this.StopMove();
			this.m_MoveParam = null;
			this.m_ChaseTarget = chaseTarget;
			this.m_ChaseSpeed = speed;
			if (chaseTarget != null)
			{
				this.MoveWithLaneChange(true, false);
			}
			else
			{
				this.m_MoveType = BattleOrderFrameBase.eMoveType.FreeStop;
			}
		}

		// Token: 0x06002594 RID: 9620 RVA: 0x000C8731 File Offset: 0x000C6B31
		public void Detour()
		{
			this.MoveWithLaneChange(true, true);
		}

		// Token: 0x06002595 RID: 9621 RVA: 0x000C873C File Offset: 0x000C6B3C
		private void MoveWithLaneChange(bool detourMode, bool isFree = false)
		{
			if (detourMode)
			{
				if (this.m_DetourState != BattleOrderFrameBase.eDetourState.Detour && this.m_DetourState != BattleOrderFrameBase.eDetourState.Up)
				{
					this.m_DetourState = BattleOrderFrameBase.eDetourState.Up;
				}
				this.m_IsFreeDetour = isFree;
			}
			else
			{
				if (this.m_DetourState == BattleOrderFrameBase.eDetourState.Detour || this.m_DetourState == BattleOrderFrameBase.eDetourState.Up)
				{
					this.m_DetourState = BattleOrderFrameBase.eDetourState.Down;
				}
				this.m_IsFreeDetour = isFree;
			}
		}

		// Token: 0x06002596 RID: 9622 RVA: 0x000C87A0 File Offset: 0x000C6BA0
		public void SetBlink(bool flg)
		{
			this.m_Blink.enabled = flg;
		}

		// Token: 0x06002597 RID: 9623 RVA: 0x000C87B0 File Offset: 0x000C6BB0
		public void Fade(float value, float duration)
		{
			this.m_Blink.enabled = false;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", this.m_CanvasGroup.alpha);
			hashtable.Add("to", value);
			hashtable.Add("time", duration);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "OnCompleteAlpha");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
			this.m_IsFade = true;
		}

		// Token: 0x06002598 RID: 9624 RVA: 0x000C8872 File Offset: 0x000C6C72
		public void OnUpdateAlpha(float value)
		{
			if (!this.m_IsFade)
			{
				return;
			}
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x06002599 RID: 9625 RVA: 0x000C888C File Offset: 0x000C6C8C
		public void OnCompleteAlpha()
		{
			this.m_IsFade = false;
		}

		// Token: 0x0600259A RID: 9626 RVA: 0x000C8895 File Offset: 0x000C6C95
		public void SetAlpha(float value)
		{
			if (this.m_GameObject.activeInHierarchy)
			{
				iTween.Stop(this.m_GameObject, "value");
			}
			this.m_IsFade = false;
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x0600259B RID: 9627 RVA: 0x000C88CA File Offset: 0x000C6CCA
		public bool isMove()
		{
			return (this.m_MoveParam != null || this.m_DetourState != BattleOrderFrameBase.eDetourState.None) && this.Alpha > 0f;
		}

		// Token: 0x0600259C RID: 9628 RVA: 0x000C88F2 File Offset: 0x000C6CF2
		public BattleOrderFrameBase.eMoveType GetMoveType()
		{
			return this.m_MoveType;
		}

		// Token: 0x0600259D RID: 9629 RVA: 0x000C88FC File Offset: 0x000C6CFC
		public float GetMoveDistance()
		{
			if (this.m_MoveParam == null)
			{
				return 0f;
			}
			return this.m_MoveParam.m_Goal - this.m_RectTransform.localPosition.x;
		}

		// Token: 0x04002C21 RID: 11297
		protected readonly Color PLAYER_COLOR = new Color(0.4392157f, 0.9098039f, 0.7647059f);

		// Token: 0x04002C22 RID: 11298
		protected readonly Color ENEMY_COLOR = new Color(1f, 0.5254902f, 0.011764706f);

		// Token: 0x04002C23 RID: 11299
		protected readonly Color CURRENT_COLOR = new Color(1f, 0.41568628f, 0.59607846f);

		// Token: 0x04002C24 RID: 11300
		protected const float DETOUR_UP_Y = 56f;

		// Token: 0x04002C25 RID: 11301
		protected const float DETOUR_DOWN_Y = 0f;

		// Token: 0x04002C26 RID: 11302
		protected const float DETOUR_LANEMOVE_SPEED = 256f;

		// Token: 0x04002C27 RID: 11303
		protected const float DETOUR_FREEUP_SPEED = 2048f;

		// Token: 0x04002C28 RID: 11304
		protected const float DETOUR_FREEDOWN_SPEED = 256f;

		// Token: 0x04002C29 RID: 11305
		protected const float JUMPHEIGHT = 38f;

		// Token: 0x04002C2A RID: 11306
		[SerializeField]
		protected ColorGroup m_BaseColorGroup;

		// Token: 0x04002C2B RID: 11307
		[SerializeField]
		protected BlinkCanvasGroup m_Blink;

		// Token: 0x04002C2C RID: 11308
		[SerializeField]
		protected GameObject m_JumpUpObj;

		// Token: 0x04002C2D RID: 11309
		protected bool m_IsPlayer;

		// Token: 0x04002C2E RID: 11310
		protected BattleOrderFrameBase.eMoveType m_MoveType = BattleOrderFrameBase.eMoveType.FreeStop;

		// Token: 0x04002C2F RID: 11311
		protected BattleOrderFrameBase.eDetourState m_DetourState;

		// Token: 0x04002C30 RID: 11312
		protected bool m_IsFreeDetour;

		// Token: 0x04002C31 RID: 11313
		protected RectTransform m_ChaseTarget;

		// Token: 0x04002C32 RID: 11314
		protected float m_ChaseSpeed;

		// Token: 0x04002C33 RID: 11315
		protected BattleOrderFrameBase.MoveParam m_MoveParam;

		// Token: 0x04002C34 RID: 11316
		protected BattleOrderFrameBase.eJumpState m_JumpState;

		// Token: 0x04002C35 RID: 11317
		protected float m_JumpDuration;

		// Token: 0x04002C36 RID: 11318
		protected bool m_IsFade;

		// Token: 0x04002C37 RID: 11319
		protected CanvasGroup m_CanvasGroup;

		// Token: 0x04002C38 RID: 11320
		protected float m_OrderValue = -1f;

		// Token: 0x04002C39 RID: 11321
		protected RectTransform m_RectTransform;

		// Token: 0x04002C3A RID: 11322
		protected GameObject m_GameObject;

		// Token: 0x0200075A RID: 1882
		public enum eMoveType
		{
			// Token: 0x04002C3D RID: 11325
			Lane,
			// Token: 0x04002C3E RID: 11326
			Free,
			// Token: 0x04002C3F RID: 11327
			FreeStop
		}

		// Token: 0x0200075B RID: 1883
		public enum eDetourState
		{
			// Token: 0x04002C41 RID: 11329
			None,
			// Token: 0x04002C42 RID: 11330
			Down,
			// Token: 0x04002C43 RID: 11331
			Up,
			// Token: 0x04002C44 RID: 11332
			Detour
		}

		// Token: 0x0200075C RID: 1884
		protected class MoveParam
		{
			// Token: 0x0600259E RID: 9630 RVA: 0x000C8939 File Offset: 0x000C6D39
			public MoveParam(float goal, float speed)
			{
				this.m_Goal = goal;
				this.m_Speed = speed;
			}

			// Token: 0x04002C45 RID: 11333
			public float m_Goal;

			// Token: 0x04002C46 RID: 11334
			public float m_Speed;
		}

		// Token: 0x0200075D RID: 1885
		public enum eJumpState
		{
			// Token: 0x04002C48 RID: 11336
			None,
			// Token: 0x04002C49 RID: 11337
			Up,
			// Token: 0x04002C4A RID: 11338
			Down
		}
	}
}
