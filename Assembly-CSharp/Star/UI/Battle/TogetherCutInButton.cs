﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000798 RID: 1944
	public class TogetherCutInButton : MonoBehaviour
	{
		// Token: 0x14000052 RID: 82
		// (add) Token: 0x06002796 RID: 10134 RVA: 0x000D3AC4 File Offset: 0x000D1EC4
		// (remove) Token: 0x06002797 RID: 10135 RVA: 0x000D3AFC File Offset: 0x000D1EFC
		public event Action<int> OnClickButton;

		// Token: 0x14000053 RID: 83
		// (add) Token: 0x06002798 RID: 10136 RVA: 0x000D3B34 File Offset: 0x000D1F34
		// (remove) Token: 0x06002799 RID: 10137 RVA: 0x000D3B6C File Offset: 0x000D1F6C
		public event Action<int> OnHoldButton;

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x0600279A RID: 10138 RVA: 0x000D3BA2 File Offset: 0x000D1FA2
		public GameObject GameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x000D3BAA File Offset: 0x000D1FAA
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x0600279C RID: 10140 RVA: 0x000D3BB4 File Offset: 0x000D1FB4
		public void Setup(int idx)
		{
			this.m_Idx = idx;
			this.m_Anim = base.GetComponent<Animation>();
			this.SetNumber(-1);
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_GameObject = base.gameObject;
			this.m_DarkImage.gameObject.SetActive(true);
			this.m_DarkImage.gameObject.SetActive(false);
			this.m_Button.IsEnableInput = true;
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x000D3C24 File Offset: 0x000D2024
		private void Update()
		{
			if (this.m_FlashImage.color.a > 0f)
			{
				Color color = this.m_FlashImage.color;
				color.a -= 5f * (Time.realtimeSinceStartup - this.m_BeforeTime);
				this.m_FlashImage.color = color;
			}
			this.m_BeforeTime = Time.realtimeSinceStartup;
		}

		// Token: 0x0600279E RID: 10142 RVA: 0x000D3C91 File Offset: 0x000D2091
		public void Play()
		{
			this.m_Anim.Play();
			this.m_BeforeTime = Time.realtimeSinceStartup;
		}

		// Token: 0x0600279F RID: 10143 RVA: 0x000D3CAA File Offset: 0x000D20AA
		public bool IsPlay()
		{
			return this.m_Anim.isPlaying;
		}

		// Token: 0x060027A0 RID: 10144 RVA: 0x000D3CB8 File Offset: 0x000D20B8
		public void SetChara(int charaID)
		{
			this.m_BustImage.Apply(charaID, BustImage.eBustImageType.Chara);
			if (charaID == -1)
			{
				return;
			}
			RectTransform component = this.m_BustImage.GetComponent<RectTransform>();
			float bustEyeOffsetY = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaIllustOffsetDB.GetParam(charaID).m_BustEyeOffsetY;
			component.localPosY((250f - bustEyeOffsetY) * component.localScale.y);
		}

		// Token: 0x060027A1 RID: 10145 RVA: 0x000D3D20 File Offset: 0x000D2120
		public void SetCharaElement(eElementType element)
		{
			Color color = default(Color);
			Color col = default(Color);
			color.a = 1f;
			col.a = 1f;
			switch (element)
			{
			case eElementType.Fire:
				color.r = 0.8901961f;
				color.g = 0.078431375f;
				color.b = 0.007843138f;
				col.r = 0.8901961f;
				col.g = 0.8980392f;
				col.b = 0f;
				break;
			case eElementType.Water:
				color.r = 0.28627452f;
				color.g = 0.38431373f;
				color.b = 0.69411767f;
				col.r = 0.7019608f;
				col.g = 0.7764706f;
				col.b = 0.98039216f;
				break;
			case eElementType.Earth:
				color.r = 0.827451f;
				color.g = 0.5647059f;
				color.b = 0.36862746f;
				col.r = 0.95686275f;
				col.g = 0.9019608f;
				col.b = 0.4627451f;
				break;
			case eElementType.Wind:
				color.r = 0.22352941f;
				color.g = 0.5411765f;
				color.b = 0.2901961f;
				col.r = 0.83137256f;
				col.g = 0.9843137f;
				col.b = 1f;
				break;
			case eElementType.Moon:
				color.r = 0.5176471f;
				color.g = 0.29411766f;
				color.b = 0.7294118f;
				col.r = 0.7764706f;
				col.g = 0.8235294f;
				col.b = 1f;
				break;
			case eElementType.Sun:
				color.r = 0.92156863f;
				color.g = 0.7254902f;
				color.b = 0.05882353f;
				col.r = 1f;
				col.g = 0.96862745f;
				col.b = 0.85490197f;
				break;
			}
			this.m_BackGroundImage.color = color;
			this.m_BackGroundEffect.ChangeColor(col, ColorGroup.eColorBlendMode.Mul, 1f);
		}

		// Token: 0x060027A2 RID: 10146 RVA: 0x000D3F64 File Offset: 0x000D2364
		public void SetNumber(int num)
		{
			this.m_Number = num;
			if (num < 0 || num > 2)
			{
				this.m_NumberObj.SetActive(false);
				this.m_NumberImage.sprite = null;
				this.m_NumberExtendImage.sprite = null;
				this.m_BackGroundEffect.GetComponent<CanvasGroup>().alpha = 0f;
			}
			else
			{
				this.m_NumberObj.SetActive(true);
				this.m_NumberImage.sprite = this.m_NumberSprite[num];
				this.m_NumberExtendImage.sprite = this.m_NumberExtendSprite[num];
				if (num != 0)
				{
					if (num != 1)
					{
						if (num == 2)
						{
							this.m_NumberColorGroup.ChangeColor(this.NUMCOLOR_3, ColorGroup.eColorBlendMode.Mul, 1f);
						}
					}
					else
					{
						this.m_NumberColorGroup.ChangeColor(this.NUMCOLOR_2, ColorGroup.eColorBlendMode.Mul, 1f);
					}
				}
				else
				{
					this.m_NumberColorGroup.ChangeColor(this.NUMCOLOR_1, ColorGroup.eColorBlendMode.Mul, 1f);
				}
				this.m_BackGroundEffect.GetComponent<CanvasGroup>().alpha = 1f;
			}
		}

		// Token: 0x060027A3 RID: 10147 RVA: 0x000D4078 File Offset: 0x000D2478
		public int GetNumber()
		{
			return this.m_Number;
		}

		// Token: 0x060027A4 RID: 10148 RVA: 0x000D4080 File Offset: 0x000D2480
		public void OnClickCallBack()
		{
			if (!this.m_Anim.isPlaying)
			{
				this.OnClickButton.Call(this.m_Idx);
			}
		}

		// Token: 0x060027A5 RID: 10149 RVA: 0x000D40A3 File Offset: 0x000D24A3
		public void OnHoldCallBack()
		{
			if (!this.m_Anim.isPlaying)
			{
				this.OnHoldButton.Call(this.m_Idx);
			}
		}

		// Token: 0x060027A6 RID: 10150 RVA: 0x000D40C8 File Offset: 0x000D24C8
		public void Flash()
		{
			Color color = this.m_FlashImage.color;
			color.a = 1f;
			this.m_FlashImage.color = color;
		}

		// Token: 0x060027A7 RID: 10151 RVA: 0x000D40F9 File Offset: 0x000D24F9
		public void SetEnableDark(bool flg)
		{
			this.m_DarkImage.gameObject.SetActive(flg);
		}

		// Token: 0x04002EA4 RID: 11940
		private readonly Color NUMCOLOR_1 = new Color(0.02745098f, 0.8901961f, 0.81960785f);

		// Token: 0x04002EA5 RID: 11941
		private readonly Color NUMCOLOR_2 = new Color(1f, 0.8f, 0f);

		// Token: 0x04002EA6 RID: 11942
		private readonly Color NUMCOLOR_3 = new Color(1f, 0.45882353f, 0.6117647f);

		// Token: 0x04002EA7 RID: 11943
		private const float FLASH_SEC = 0.2f;

		// Token: 0x04002EA8 RID: 11944
		private const float FRAME_HEIGHT = 262f;

		// Token: 0x04002EA9 RID: 11945
		private const float DEFAULT_FRAME_POSITION_Y = 250f;

		// Token: 0x04002EAA RID: 11946
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04002EAB RID: 11947
		[SerializeField]
		private Image m_BackGroundImage;

		// Token: 0x04002EAC RID: 11948
		[SerializeField]
		private ColorGroup m_BackGroundEffect;

		// Token: 0x04002EAD RID: 11949
		[SerializeField]
		private Sprite[] m_NumberSprite;

		// Token: 0x04002EAE RID: 11950
		[SerializeField]
		private Sprite[] m_NumberExtendSprite;

		// Token: 0x04002EAF RID: 11951
		[SerializeField]
		private GameObject m_NumberObj;

		// Token: 0x04002EB0 RID: 11952
		[SerializeField]
		private ColorGroup m_NumberColorGroup;

		// Token: 0x04002EB1 RID: 11953
		[SerializeField]
		private Image m_NumberImage;

		// Token: 0x04002EB2 RID: 11954
		[SerializeField]
		private Image m_NumberExtendImage;

		// Token: 0x04002EB3 RID: 11955
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x04002EB4 RID: 11956
		[SerializeField]
		private Image m_FlashImage;

		// Token: 0x04002EB5 RID: 11957
		[SerializeField]
		private Image m_DarkImage;

		// Token: 0x04002EB6 RID: 11958
		private int m_Idx;

		// Token: 0x04002EB7 RID: 11959
		private int m_Number = -1;

		// Token: 0x04002EB8 RID: 11960
		private Animation m_Anim;

		// Token: 0x04002EBB RID: 11963
		private float m_BeforeTime = -1f;

		// Token: 0x04002EBC RID: 11964
		private RectTransform m_RectTransform;

		// Token: 0x04002EBD RID: 11965
		private GameObject m_GameObject;
	}
}
