﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000761 RID: 1889
	public class BattleReward : MonoBehaviour
	{
		// Token: 0x060025C5 RID: 9669 RVA: 0x000CA50C File Offset: 0x000C890C
		public void SetRewardNum(int rank, int value, bool isAnim = false)
		{
			if (rank > this.m_Rank)
			{
				this.m_Rank = rank;
				this.m_TreasureBoxImage.sprite = this.m_TreasureSprites[rank];
			}
			this.m_NumTextObj.text = value.ToString();
			if (isAnim)
			{
				this.m_Animation.Play();
			}
		}

		// Token: 0x060025C6 RID: 9670 RVA: 0x000CA569 File Offset: 0x000C8969
		public RectTransform GetTreasureBoxRectTransform()
		{
			return this.m_TreasureBox;
		}

		// Token: 0x04002C81 RID: 11393
		[SerializeField]
		private Image m_TreasureBoxImage;

		// Token: 0x04002C82 RID: 11394
		[SerializeField]
		private RectTransform m_TreasureBox;

		// Token: 0x04002C83 RID: 11395
		[SerializeField]
		private Text m_NumTextObj;

		// Token: 0x04002C84 RID: 11396
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x04002C85 RID: 11397
		[SerializeField]
		[Tooltip("ランク順に0～２")]
		private Sprite[] m_TreasureSprites;

		// Token: 0x04002C86 RID: 11398
		private int m_Rank;
	}
}
