﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000785 RID: 1925
	public class PopUpIcon : MonoBehaviour
	{
		// Token: 0x17000297 RID: 663
		// (get) Token: 0x0600273B RID: 10043 RVA: 0x000D0F83 File Offset: 0x000CF383
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x0600273C RID: 10044 RVA: 0x000D0F8B File Offset: 0x000CF38B
		public Vector3 TargetPosition
		{
			get
			{
				return this.m_TargetPosition;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x0600273D RID: 10045 RVA: 0x000D0F93 File Offset: 0x000CF393
		public float TimeCount
		{
			get
			{
				return this.m_TimeCount;
			}
		}

		// Token: 0x0600273E RID: 10046 RVA: 0x000D0F9B File Offset: 0x000CF39B
		public void Setup(StateIconOwner stateIconOwner)
		{
			this.m_StateIconOwner = stateIconOwner;
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_GameObject.SetActive(false);
		}

		// Token: 0x0600273F RID: 10047 RVA: 0x000D0FCD File Offset: 0x000CF3CD
		public void SetParent(RectTransform parent)
		{
			this.m_RectTransform.SetParent(parent, false);
		}

		// Token: 0x06002740 RID: 10048 RVA: 0x000D0FDC File Offset: 0x000CF3DC
		public void ApplyMiss(bool iconSpace = false)
		{
			this.m_IconSpaceObj.SetActive(iconSpace);
			this.m_Text.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleMiss);
			this.m_MissImage.enabled = true;
		}

		// Token: 0x06002741 RID: 10049 RVA: 0x000D1018 File Offset: 0x000CF418
		public void Apply(SkillActionEffectSet.IconData iconData, bool iconSpace = false)
		{
			if (iconData.m_IsMiss)
			{
				this.ApplyMiss(false);
				return;
			}
			this.m_MissImage.enabled = false;
			PopUpStateIconListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.PopUpStateIconListDB.GetParam(iconData.m_IconType);
			if (param.m_StateIconType != 0)
			{
				this.m_IconSpaceObj.SetActive(true);
				StateIcon component = this.m_StateIconOwner.ScoopEmptyIcon().GetComponent<StateIcon>();
				component.GameObject.SetActive(true);
				component.RectTransform.SetParent(this.m_IconParent);
				component.RectTransform.SetAsFirstSibling();
				component.RectTransform.localPosition = Vector2.zero;
				component.RectTransform.localScale = Vector3.one * 1.5f;
				component.Apply((eStateIconType)param.m_StateIconType);
				this.m_StateIconList.Add(component);
			}
			else
			{
				this.m_IconSpaceObj.SetActive(iconSpace);
			}
			if (iconData.m_Grade == -1 || param.m_Text.Length <= iconData.m_Grade)
			{
				this.m_Text.text = param.m_Text[param.m_Text.Length - 1];
			}
			else
			{
				this.m_Text.text = param.m_Text[iconData.m_Grade];
			}
		}

		// Token: 0x06002742 RID: 10050 RVA: 0x000D1168 File Offset: 0x000CF568
		public float CalcWidth()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(this.m_IconParent);
			return this.m_IconParent.rect.width;
		}

		// Token: 0x06002743 RID: 10051 RVA: 0x000D1194 File Offset: 0x000CF594
		public void Display(SkillActionEffectSet.IconData data, Transform targetTransform, float offsetFromTargetTransform, float pivotX, Vector2 offsetUISpace, bool iconSpace)
		{
			this.m_GameObject.SetActive(true);
			this.m_TargetPosition = targetTransform.position + new Vector3(0f, offsetFromTargetTransform, 0f);
			this.Clear();
			this.Apply(data, iconSpace);
			this.m_Animation.Play();
			this.m_Pivot = pivotX;
			this.m_OffsetUISpace = offsetUISpace;
			this.m_CanvasGroup.alpha = 0f;
			this.UpdatePosition();
			this.m_IsAdjust = false;
			this.m_TimeCount = 0f;
		}

		// Token: 0x06002744 RID: 10052 RVA: 0x000D1224 File Offset: 0x000CF624
		private void UpdatePosition()
		{
			this.m_RectTransform.localScale = Vector3.one;
			this.m_ParentRectTransform = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, this.m_TargetPosition);
			Vector2 v;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_ParentRectTransform, screenPoint, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out v);
			this.m_RectTransform.localPosition = v;
			this.m_RectTransform.localPosition = this.m_RectTransform.localPosition + new Vector3(-this.m_IconParent.rect.width * 0.5f + this.m_Pivot * this.m_IconParent.rect.width, 96f);
			this.m_RectTransform.localPosition += new Vector3(this.m_OffsetUISpace.x, this.m_OffsetUISpace.y, 0f);
		}

		// Token: 0x06002745 RID: 10053 RVA: 0x000D1324 File Offset: 0x000CF724
		public void Clear()
		{
			for (int i = 0; i < this.m_StateIconList.Count; i++)
			{
				this.m_StateIconOwner.SinkEmptyIcon(this.m_StateIconList[i].GameObject);
			}
			this.m_StateIconList.Clear();
		}

		// Token: 0x06002746 RID: 10054 RVA: 0x000D1374 File Offset: 0x000CF774
		private void Update()
		{
			this.UpdatePosition();
			if (!this.m_IsAdjust)
			{
				this.m_IsAdjust = true;
			}
			else
			{
				this.m_CanvasGroup.alpha = 1f;
			}
			if (!this.m_Animation.isPlaying)
			{
				this.Clear();
				this.SetActive(false);
			}
			this.m_TimeCount += Time.deltaTime;
		}

		// Token: 0x06002747 RID: 10055 RVA: 0x000D13DD File Offset: 0x000CF7DD
		public void SetActive(bool flag)
		{
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x06002748 RID: 10056 RVA: 0x000D13EB File Offset: 0x000CF7EB
		public bool IsComplete()
		{
			return !this.m_GameObject.activeSelf;
		}

		// Token: 0x04002DFA RID: 11770
		private const float SPACE = 0f;

		// Token: 0x04002DFB RID: 11771
		private const float OFFSETY = 96f;

		// Token: 0x04002DFC RID: 11772
		private const float ICON_SCALE = 1.5f;

		// Token: 0x04002DFD RID: 11773
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04002DFE RID: 11774
		private List<StateIcon> m_StateIconList = new List<StateIcon>();

		// Token: 0x04002DFF RID: 11775
		[SerializeField]
		private GameObject m_IconSpaceObj;

		// Token: 0x04002E00 RID: 11776
		[SerializeField]
		private RectTransform m_IconParent;

		// Token: 0x04002E01 RID: 11777
		[SerializeField]
		private Text m_Text;

		// Token: 0x04002E02 RID: 11778
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x04002E03 RID: 11779
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04002E04 RID: 11780
		[SerializeField]
		private Image m_MissImage;

		// Token: 0x04002E05 RID: 11781
		private float m_TimeCount;

		// Token: 0x04002E06 RID: 11782
		private float m_Pivot;

		// Token: 0x04002E07 RID: 11783
		private Vector2 m_OffsetUISpace;

		// Token: 0x04002E08 RID: 11784
		private bool m_IsAdjust;

		// Token: 0x04002E09 RID: 11785
		private Vector3 m_TargetPosition;

		// Token: 0x04002E0A RID: 11786
		private GameObject m_GameObject;

		// Token: 0x04002E0B RID: 11787
		private RectTransform m_RectTransform;

		// Token: 0x04002E0C RID: 11788
		private RectTransform m_ParentRectTransform;
	}
}
