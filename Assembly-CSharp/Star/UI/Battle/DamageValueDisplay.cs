﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000774 RID: 1908
	public class DamageValueDisplay : MonoBehaviour
	{
		// Token: 0x060026DA RID: 9946 RVA: 0x000CED8C File Offset: 0x000CD18C
		public void Setup()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			for (int i = 0; i < 16; i++)
			{
				DamageValue damageValue = UnityEngine.Object.Instantiate<DamageValue>(this.m_DamageValuePrefab, this.m_RectTransform, false);
				damageValue.Setup();
				damageValue.SetActive(false);
				this.m_EmptyDamageValueList.Add(damageValue);
			}
		}

		// Token: 0x060026DB RID: 9947 RVA: 0x000CEDE4 File Offset: 0x000CD1E4
		private void Update()
		{
			List<DamageValueDisplay.DamageData> damageDataListWork = this.m_DamageDataListWork;
			damageDataListWork.Clear();
			for (int i = 0; i < this.m_DamageDataList.Count; i++)
			{
				this.DisplayFromReserve(this.m_DamageDataList[i]);
				damageDataListWork.Add(this.m_DamageDataList[i]);
			}
			while (damageDataListWork.Count > 0)
			{
				this.m_DamageDataList.Remove(damageDataListWork[0]);
				damageDataListWork.RemoveAt(0);
			}
			this.RemoveCompleteDisplay();
		}

		// Token: 0x060026DC RID: 9948 RVA: 0x000CEE70 File Offset: 0x000CD270
		public void Display(int value, bool isCritical, bool isUnhappy, bool isPoison, int registHit_Or_DefaultHit_Or_WeakHit, Transform targetTransform, float offsetY)
		{
			DamageValueDisplay.DamageData item = default(DamageValueDisplay.DamageData);
			item.m_Value = value;
			item.m_IsCritical = isCritical;
			item.m_IsUnhappy = isUnhappy;
			item.m_IsPoison = isPoison;
			item.m_registHit_Or_DefaultHit_Or_WeakHit = registHit_Or_DefaultHit_Or_WeakHit;
			item.m_TargetTransform = targetTransform;
			item.m_OffsetY = offsetY;
			this.m_DamageDataList.Add(item);
		}

		// Token: 0x060026DD RID: 9949 RVA: 0x000CEED0 File Offset: 0x000CD2D0
		private void DisplayFromReserve(DamageValueDisplay.DamageData data)
		{
			if (data.m_IsCritical)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_ACT_CRITICAL, 1f, 0, -1, -1);
			}
			if (this.m_EmptyDamageValueList.Count <= 0)
			{
				DamageValue damageValue = UnityEngine.Object.Instantiate<DamageValue>(this.m_DamageValuePrefab, this.m_RectTransform, false);
				damageValue.Setup();
				damageValue.SetActive(false);
				this.m_EmptyDamageValueList.Add(damageValue);
			}
			this.m_EmptyDamageValueList[0].Display(data.m_Value, data.m_IsCritical, data.m_registHit_Or_DefaultHit_Or_WeakHit, data.m_IsUnhappy, data.m_IsPoison, data.m_TargetTransform, data.m_OffsetY);
			this.m_EmptyDamageValueList[0].RectTransform.SetAsLastSibling();
			bool flag = false;
			for (int i = 0; i < this.m_ActiveDamageValueList.Count; i++)
			{
				if (!flag)
				{
					float y = this.m_ActiveDamageValueList[i].TargetPosition.y;
					float y2 = this.m_EmptyDamageValueList[0].TargetPosition.y;
					if (this.m_ActiveDamageValueList[i].TimeCount < 0.1f && y < y2)
					{
						this.m_ActiveDamageValueList.Insert(i, this.m_EmptyDamageValueList[0]);
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				this.m_ActiveDamageValueList.Add(this.m_EmptyDamageValueList[0]);
			}
			for (int j = 0; j < this.m_ActiveDamageValueList.Count; j++)
			{
				this.m_ActiveDamageValueList[j].RectTransform.SetSiblingIndex(j);
			}
			this.m_EmptyDamageValueList.RemoveAt(0);
		}

		// Token: 0x060026DE RID: 9950 RVA: 0x000CF0A8 File Offset: 0x000CD4A8
		private void RemoveCompleteDisplay()
		{
			List<DamageValue> damageValueListWork = this.m_DamageValueListWork;
			damageValueListWork.Clear();
			for (int i = 0; i < this.m_ActiveDamageValueList.Count; i++)
			{
				if (this.m_ActiveDamageValueList[i].IsComplete())
				{
					damageValueListWork.Add(this.m_ActiveDamageValueList[i]);
				}
			}
			while (damageValueListWork.Count > 0)
			{
				damageValueListWork[0].SetActive(false);
				this.m_EmptyDamageValueList.Add(damageValueListWork[0]);
				this.m_ActiveDamageValueList.Remove(damageValueListWork[0]);
				damageValueListWork.RemoveAt(0);
			}
		}

		// Token: 0x04002D7D RID: 11645
		private const int DISPLAY_CACHE_NUM = 16;

		// Token: 0x04002D7E RID: 11646
		[SerializeField]
		private DamageValue m_DamageValuePrefab;

		// Token: 0x04002D7F RID: 11647
		private List<DamageValue> m_EmptyDamageValueList = new List<DamageValue>();

		// Token: 0x04002D80 RID: 11648
		private List<DamageValue> m_ActiveDamageValueList = new List<DamageValue>();

		// Token: 0x04002D81 RID: 11649
		private List<DamageValue> m_DamageValueListWork = new List<DamageValue>();

		// Token: 0x04002D82 RID: 11650
		private List<DamageValueDisplay.DamageData> m_DamageDataList = new List<DamageValueDisplay.DamageData>();

		// Token: 0x04002D83 RID: 11651
		private List<DamageValueDisplay.DamageData> m_DamageDataListWork = new List<DamageValueDisplay.DamageData>();

		// Token: 0x04002D84 RID: 11652
		private float m_LastDisplayY;

		// Token: 0x04002D85 RID: 11653
		private RectTransform m_RectTransform;

		// Token: 0x02000775 RID: 1909
		private struct DamageData
		{
			// Token: 0x04002D86 RID: 11654
			public int m_Value;

			// Token: 0x04002D87 RID: 11655
			public bool m_IsCritical;

			// Token: 0x04002D88 RID: 11656
			public bool m_IsUnhappy;

			// Token: 0x04002D89 RID: 11657
			public bool m_IsPoison;

			// Token: 0x04002D8A RID: 11658
			public int m_registHit_Or_DefaultHit_Or_WeakHit;

			// Token: 0x04002D8B RID: 11659
			public Transform m_TargetTransform;

			// Token: 0x04002D8C RID: 11660
			public float m_OffsetY;
		}
	}
}
