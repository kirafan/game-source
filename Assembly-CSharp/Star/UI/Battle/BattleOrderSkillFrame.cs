﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000760 RID: 1888
	public class BattleOrderSkillFrame : BattleOrderFrameBase
	{
		// Token: 0x17000284 RID: 644
		// (get) Token: 0x060025BA RID: 9658 RVA: 0x000CA3B1 File Offset: 0x000C87B1
		// (set) Token: 0x060025BB RID: 9659 RVA: 0x000CA3B9 File Offset: 0x000C87B9
		public BattleOrder.CardArguments CardArg { get; set; }

		// Token: 0x060025BC RID: 9660 RVA: 0x000CA3C2 File Offset: 0x000C87C2
		public override bool IsCard()
		{
			return true;
		}

		// Token: 0x060025BD RID: 9661 RVA: 0x000CA3C5 File Offset: 0x000C87C5
		public override void Setup()
		{
			base.Setup();
			this.m_SkillIcon.DefaultBlack = true;
		}

		// Token: 0x060025BE RID: 9662 RVA: 0x000CA3D9 File Offset: 0x000C87D9
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x060025BF RID: 9663 RVA: 0x000CA3E4 File Offset: 0x000C87E4
		public void ApplySkill(int skillID, BattleOrder.FrameData frameData)
		{
			this.m_SkillIcon.Apply(SkillIcon.eSkillDBType.Card, skillID, frameData.m_Owner.CharaParam.ElementType);
			if (frameData.m_Owner.CharaBattle.MyParty.PartyType == BattleDefine.ePartyType.Player)
			{
				this.m_BaseColorGroup.ChangeColor(this.PLAYER_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
				this.m_PopEffectAnim.GetComponent<Image>().color = this.PLAYER_COLOR;
				this.m_IsPlayer = true;
			}
			else
			{
				this.m_BaseColorGroup.ChangeColor(this.ENEMY_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
				this.m_PopEffectAnim.GetComponent<Image>().color = this.ENEMY_COLOR;
				this.m_IsPlayer = false;
			}
		}

		// Token: 0x060025C0 RID: 9664 RVA: 0x000CA495 File Offset: 0x000C8895
		public void SetAliveNumCount(int count)
		{
			if (count < 0)
			{
				count = 0;
			}
			this.m_AliveNum.text = count.ToString();
		}

		// Token: 0x060025C1 RID: 9665 RVA: 0x000CA4B9 File Offset: 0x000C88B9
		public void Popup()
		{
			this.m_PopAnim.PlayIn();
			this.m_PopEffectAnim.PlayIn();
		}

		// Token: 0x060025C2 RID: 9666 RVA: 0x000CA4D1 File Offset: 0x000C88D1
		public bool IsPopup()
		{
			return this.m_PopAnim.State == 1 || this.m_PopEffectAnim.State == 1;
		}

		// Token: 0x060025C3 RID: 9667 RVA: 0x000CA4F5 File Offset: 0x000C88F5
		public void SkipPopup()
		{
			this.m_PopAnim.Show();
		}

		// Token: 0x04002C7C RID: 11388
		[SerializeField]
		private SkillIcon m_SkillIcon;

		// Token: 0x04002C7D RID: 11389
		[SerializeField]
		private Text m_AliveNum;

		// Token: 0x04002C7E RID: 11390
		[SerializeField]
		private AnimUIPlayer m_PopAnim;

		// Token: 0x04002C7F RID: 11391
		[SerializeField]
		private AnimUIPlayer m_PopEffectAnim;
	}
}
