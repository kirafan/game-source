﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200079A RID: 1946
	public class TogetherSelect : UIGroup
	{
		// Token: 0x14000054 RID: 84
		// (add) Token: 0x060027AB RID: 10155 RVA: 0x000D4228 File Offset: 0x000D2628
		// (remove) Token: 0x060027AC RID: 10156 RVA: 0x000D4260 File Offset: 0x000D2660
		public event Action<BattleDefine.eJoinMember> OnDecideMember;

		// Token: 0x14000055 RID: 85
		// (add) Token: 0x060027AD RID: 10157 RVA: 0x000D4298 File Offset: 0x000D2698
		// (remove) Token: 0x060027AE RID: 10158 RVA: 0x000D42D0 File Offset: 0x000D26D0
		public event Action OnDecideTogether;

		// Token: 0x14000056 RID: 86
		// (add) Token: 0x060027AF RID: 10159 RVA: 0x000D4308 File Offset: 0x000D2708
		// (remove) Token: 0x060027B0 RID: 10160 RVA: 0x000D4340 File Offset: 0x000D2740
		public event Action OnCancelTogether;

		// Token: 0x060027B1 RID: 10161 RVA: 0x000D4376 File Offset: 0x000D2776
		public bool IsSelectEnd()
		{
			return base.IsDonePlayOut;
		}

		// Token: 0x060027B2 RID: 10162 RVA: 0x000D4380 File Offset: 0x000D2780
		public void Setup()
		{
			this.m_SelectOrder = new int[3];
			for (int i = 0; i < 3; i++)
			{
				this.m_SelectOrder[i] = -1;
			}
			for (int j = 0; j < this.m_CutInButtons.Length; j++)
			{
				this.m_CutInButtons[j].Setup(j);
				this.m_CutInButtons[j].OnClickButton += this.OnPressCallBack;
				this.m_CutInButtons[j].OnHoldButton += this.OnHoldCallBack;
			}
			this.m_DescriptCancelImage.gameObject.SetActive(false);
			base.gameObject.SetActive(false);
		}

		// Token: 0x060027B3 RID: 10163 RVA: 0x000D442C File Offset: 0x000D282C
		public void SetCharacter(int[] charaIDs, eElementType[] elements, int[] skillIDs, BattleDefine.eJoinMember[] join)
		{
			this.m_CharaIDs = charaIDs;
			this.m_SkillIDs = new int[this.m_CutInButtons.Length];
			this.m_JoinMembers = join;
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				if (i >= join.Length)
				{
					this.m_CutInButtons[i].GameObject.SetActive(false);
				}
				else
				{
					this.m_CutInButtons[i].GameObject.SetActive(true);
					this.m_CutInButtons[i].SetChara(charaIDs[i]);
					this.m_CutInButtons[i].SetCharaElement(elements[i]);
					this.m_SkillIDs[i] = skillIDs[i];
				}
			}
		}

		// Token: 0x060027B4 RID: 10164 RVA: 0x000D44D4 File Offset: 0x000D28D4
		public void SetSelectableMax(int max)
		{
			this.m_SelectableMax = max;
			for (int i = 0; i < this.m_GaugeStars.Length; i++)
			{
				if (i < max)
				{
					this.m_GaugeStars[i].RevertColor();
				}
				else
				{
					this.m_GaugeStars[i].ChangeColor(Color.gray, ColorGroup.eColorBlendMode.Mul, 1f);
				}
			}
		}

		// Token: 0x060027B5 RID: 10165 RVA: 0x000D4534 File Offset: 0x000D2934
		public void ResetSelectOrder()
		{
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				this.m_CutInButtons[i].SetNumber(-1);
			}
			this.m_SelectingNumber = 0;
			for (int j = 0; j < 3; j++)
			{
				this.m_SelectOrder[j] = -1;
			}
		}

		// Token: 0x060027B6 RID: 10166 RVA: 0x000D458C File Offset: 0x000D298C
		public override void Open()
		{
			base.Open();
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				if (this.m_JoinMembers == null)
				{
					break;
				}
				if (i < this.m_JoinMembers.Length)
				{
					this.m_CutInButtons[i].Play();
				}
			}
			bool interactable = false;
			for (int j = 0; j < this.m_SelectOrder.Length; j++)
			{
				if (this.m_SelectOrder[j] != -1)
				{
					this.m_GaugeStarEffects[j].SetActive(true);
					Animation component = this.m_GaugeStarEffects[j].GetComponent<Animation>();
					component.Stop();
					component.Play();
					interactable = true;
				}
				else
				{
					this.m_GaugeStarEffects[j].SetActive(false);
				}
			}
			this.m_LabelAnim[this.m_LabelAnim.clip.name].normalizedTime = 0f;
			this.m_LabelAnim[this.m_LabelAnim.clip.name].speed = 0f;
			this.m_RainbowScrAnim.SetSpeed(0f);
			this.m_DecideButton.Interactable = interactable;
		}

		// Token: 0x060027B7 RID: 10167 RVA: 0x000D46B0 File Offset: 0x000D2AB0
		public override void Close()
		{
			base.Close();
		}

		// Token: 0x060027B8 RID: 10168 RVA: 0x000D46B8 File Offset: 0x000D2AB8
		public void Destroy()
		{
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				this.m_CutInButtons[i].SetChara(-1);
			}
		}

		// Token: 0x060027B9 RID: 10169 RVA: 0x000D46EC File Offset: 0x000D2AEC
		public List<BattleDefine.eJoinMember> GetSelectOrderJoin()
		{
			List<BattleDefine.eJoinMember> list = new List<BattleDefine.eJoinMember>();
			for (int i = 0; i < this.m_SelectOrder.Length; i++)
			{
				if (this.m_SelectOrder[i] == -1)
				{
					break;
				}
				list.Add(this.m_JoinMembers[this.m_SelectOrder[i]]);
			}
			return list;
		}

		// Token: 0x060027BA RID: 10170 RVA: 0x000D4744 File Offset: 0x000D2B44
		public List<int> GetSelectOrderCharaIDs()
		{
			List<int> list = new List<int>();
			for (int i = 0; i < this.m_SelectOrder.Length; i++)
			{
				if (this.m_SelectOrder[i] == -1)
				{
					break;
				}
				list.Add(this.m_CharaIDs[this.m_SelectOrder[i]]);
			}
			return list;
		}

		// Token: 0x060027BB RID: 10171 RVA: 0x000D4799 File Offset: 0x000D2B99
		public void OnPressCallBack(int idx)
		{
			if (!base.IsIdle())
			{
				return;
			}
			if (this.m_DescriptWindow.isActiveAndEnabled)
			{
				this.CancelDescript();
				return;
			}
			this.SelectChara(idx);
			this.OnDecideMember.Call(this.m_JoinMembers[idx]);
		}

		// Token: 0x060027BC RID: 10172 RVA: 0x000D47D8 File Offset: 0x000D2BD8
		public void SelectChara(int idx)
		{
			if (this.m_CutInButtons[idx].GetNumber() == -1)
			{
				if (this.m_SelectableMax <= this.m_SelectingNumber)
				{
					return;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_DECISION, 1f, 0, -1, -1);
				this.m_CutInButtons[idx].SetNumber(this.m_SelectingNumber);
				this.m_SelectOrder[this.m_SelectingNumber] = idx;
				for (int i = 0; i < 3; i++)
				{
					if (this.m_SelectOrder[i] == -1)
					{
						this.m_SelectingNumber = i;
						break;
					}
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_CANCEL, 1f, 0, -1, -1);
				this.m_SelectOrder[this.m_CutInButtons[idx].GetNumber()] = -1;
				this.m_CutInButtons[idx].SetNumber(-1);
				for (int j = 0; j < 3; j++)
				{
					if (this.m_SelectOrder[j] == -1)
					{
						this.m_SelectingNumber = j;
						break;
					}
				}
			}
			this.m_CutInButtons[idx].Flash();
			for (int k = 0; k < this.m_SelectOrder.Length; k++)
			{
				if (this.m_SelectOrder[k] == -1)
				{
					for (int l = k; l < this.m_SelectOrder.Length - 1; l++)
					{
						if (this.m_SelectOrder[l + 1] != -1)
						{
							this.m_CutInButtons[this.m_SelectOrder[l + 1]].SetNumber(l);
						}
						this.m_SelectOrder[l] = this.m_SelectOrder[l + 1];
					}
					this.m_SelectOrder[this.m_SelectOrder.Length - 1] = -1;
				}
				if (this.m_SelectOrder[k] == -1)
				{
					this.m_SelectingNumber = k;
					break;
				}
			}
			bool flag = false;
			for (int m = 0; m < this.m_SelectOrder.Length; m++)
			{
				if (this.m_SelectOrder[m] != -1)
				{
					this.m_GaugeStarEffects[m].SetActive(true);
					Animation component = this.m_GaugeStarEffects[m].GetComponent<Animation>();
					component.Stop();
					component.Play();
					flag = true;
				}
				else
				{
					this.m_GaugeStarEffects[m].SetActive(false);
				}
			}
			if (flag)
			{
				this.m_LabelAnim[this.m_LabelAnim.clip.name].speed = 1f;
				this.m_LabelAnim.Play();
				this.m_RainbowScrAnim.SetSpeed(1f);
			}
			else
			{
				this.m_LabelAnim[this.m_LabelAnim.clip.name].normalizedTime = 0f;
				this.m_LabelAnim[this.m_LabelAnim.clip.name].speed = 0f;
				this.m_RainbowScrAnim.SetSpeed(0f);
			}
			this.m_DecideButton.Interactable = flag;
		}

		// Token: 0x060027BD RID: 10173 RVA: 0x000D4AC1 File Offset: 0x000D2EC1
		public void OnHoldCallBack(int idx)
		{
			this.DispDescript(idx);
		}

		// Token: 0x060027BE RID: 10174 RVA: 0x000D4ACC File Offset: 0x000D2ECC
		public void DispDescript(int idx)
		{
			if (!this.m_DescriptWindow.IsOpenOrIn())
			{
				this.m_DescriptWindow.Open();
			}
			this.m_DescriptCancelImage.gameObject.SetActive(true);
			this.m_DescriptNameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(this.m_SkillIDs[idx]).m_SkillName;
			this.m_DescriptText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(this.m_SkillIDs[idx]).m_SkillDetail;
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				this.m_CutInButtons[i].SetEnableDark(i != idx);
			}
		}

		// Token: 0x060027BF RID: 10175 RVA: 0x000D4B90 File Offset: 0x000D2F90
		public void CancelDescript()
		{
			this.m_DescriptWindow.Close();
			this.m_DescriptCancelImage.gameObject.SetActive(false);
			for (int i = 0; i < this.m_CutInButtons.Length; i++)
			{
				this.m_CutInButtons[i].SetEnableDark(false);
			}
		}

		// Token: 0x060027C0 RID: 10176 RVA: 0x000D4BE0 File Offset: 0x000D2FE0
		public void OnClickDecideCallBack()
		{
			if (!base.IsIdle())
			{
				return;
			}
			this.OnDecideTogether.Call();
			base.SetEnableInput(false);
			this.CancelDescript();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_KIRARAJUMP_DECISION, 1f, 0, -1, -1);
		}

		// Token: 0x060027C1 RID: 10177 RVA: 0x000D4C2E File Offset: 0x000D302E
		public void OnClickCancelCallBack()
		{
			this.OnCancelTogether.Call();
			this.CancelDescript();
			this.Close();
		}

		// Token: 0x04002EC2 RID: 11970
		[SerializeField]
		private TogetherCutInButton[] m_CutInButtons;

		// Token: 0x04002EC3 RID: 11971
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04002EC4 RID: 11972
		[SerializeField]
		private UIGroup m_DescriptWindow;

		// Token: 0x04002EC5 RID: 11973
		[SerializeField]
		private Text m_DescriptNameText;

		// Token: 0x04002EC6 RID: 11974
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04002EC7 RID: 11975
		[SerializeField]
		private CustomButton m_DescriptCancelImage;

		// Token: 0x04002EC8 RID: 11976
		[SerializeField]
		private ColorGroup[] m_GaugeStars;

		// Token: 0x04002EC9 RID: 11977
		[SerializeField]
		private GameObject[] m_GaugeStarEffects;

		// Token: 0x04002ECA RID: 11978
		[SerializeField]
		private Animation m_LabelAnim;

		// Token: 0x04002ECB RID: 11979
		[SerializeField]
		private OrderTogetherFrame m_RainbowScrAnim;

		// Token: 0x04002ECC RID: 11980
		private int m_SelectingNumber;

		// Token: 0x04002ECD RID: 11981
		private int[] m_SelectOrder;

		// Token: 0x04002ECE RID: 11982
		private int m_SelectableMax = 3;

		// Token: 0x04002ECF RID: 11983
		private BattleDefine.eJoinMember[] m_JoinMembers;

		// Token: 0x04002ED0 RID: 11984
		private int[] m_CharaIDs;

		// Token: 0x04002ED1 RID: 11985
		private int[] m_SkillIDs;
	}
}
