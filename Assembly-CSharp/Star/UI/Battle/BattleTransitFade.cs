﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000762 RID: 1890
	public class BattleTransitFade : MonoBehaviour
	{
		// Token: 0x17000285 RID: 645
		// (get) Token: 0x060025C8 RID: 9672 RVA: 0x000CA579 File Offset: 0x000C8979
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x060025C9 RID: 9673 RVA: 0x000CA59E File Offset: 0x000C899E
		private void SetColor(Color color)
		{
			this.m_Fade.SetColor(color);
		}

		// Token: 0x060025CA RID: 9674 RVA: 0x000CA5AC File Offset: 0x000C89AC
		public void PlayInQuick(Color color, int waveIndex, int waveMax)
		{
			this.GameObject.SetActive(true);
			this.SetColor(color);
			this.m_Fade.ShowQuick();
			this.m_Title.Apply(waveIndex + 1, waveMax);
			this.m_Title.Anim.PlayIn();
		}

		// Token: 0x060025CB RID: 9675 RVA: 0x000CA5EB File Offset: 0x000C89EB
		public void PlayInQuick(int waveIndex, int waveMax)
		{
			this.PlayInQuick(Color.black, waveIndex, waveMax);
		}

		// Token: 0x060025CC RID: 9676 RVA: 0x000CA5FA File Offset: 0x000C89FA
		public void PlayIn(Color color, int waveIndex, int waveMax)
		{
			this.GameObject.SetActive(true);
			this.SetColor(color);
			this.m_Fade.Show();
			this.m_Title.Apply(waveIndex + 1, waveMax);
			this.m_Title.Anim.PlayIn();
		}

		// Token: 0x060025CD RID: 9677 RVA: 0x000CA639 File Offset: 0x000C8A39
		public void PlayIn(int waveIndex, int waveMax)
		{
			this.PlayIn(Color.black, waveIndex, waveMax);
		}

		// Token: 0x060025CE RID: 9678 RVA: 0x000CA648 File Offset: 0x000C8A48
		public void PlayIn(Color color)
		{
			this.GameObject.SetActive(true);
			this.SetColor(color);
			this.m_Fade.Show();
		}

		// Token: 0x060025CF RID: 9679 RVA: 0x000CA668 File Offset: 0x000C8A68
		public void PlayIn()
		{
			this.PlayIn(Color.black);
		}

		// Token: 0x060025D0 RID: 9680 RVA: 0x000CA675 File Offset: 0x000C8A75
		public void PlayInQuick(Color color)
		{
			this.GameObject.SetActive(true);
			this.SetColor(color);
			this.m_Fade.ShowQuick();
		}

		// Token: 0x060025D1 RID: 9681 RVA: 0x000CA695 File Offset: 0x000C8A95
		public void PlayInQuick()
		{
			this.PlayInQuick(Color.black);
		}

		// Token: 0x060025D2 RID: 9682 RVA: 0x000CA6A2 File Offset: 0x000C8AA2
		public void PlayOut()
		{
			this.GameObject.SetActive(true);
			this.m_Fade.Hide();
			this.m_Title.Anim.PlayOut();
		}

		// Token: 0x060025D3 RID: 9683 RVA: 0x000CA6CB File Offset: 0x000C8ACB
		public bool IsShowComplete()
		{
			return !this.m_Fade.IsPlaying;
		}

		// Token: 0x060025D4 RID: 9684 RVA: 0x000CA6DB File Offset: 0x000C8ADB
		public bool IsHideComplete()
		{
			return !this.m_Fade.IsPlaying;
		}

		// Token: 0x060025D5 RID: 9685 RVA: 0x000CA6EB File Offset: 0x000C8AEB
		public bool IsEnableRender()
		{
			return this.m_Fade.state != BattleFade.eState.Hide;
		}

		// Token: 0x04002C87 RID: 11399
		[SerializeField]
		private BattleFade m_Fade;

		// Token: 0x04002C88 RID: 11400
		[SerializeField]
		private WaveTitle m_Title;

		// Token: 0x04002C89 RID: 11401
		private GameObject m_GameObject;
	}
}
