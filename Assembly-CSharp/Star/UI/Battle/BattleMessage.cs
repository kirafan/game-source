﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000757 RID: 1879
	public class BattleMessage : MonoBehaviour
	{
		// Token: 0x17000278 RID: 632
		// (get) Token: 0x0600255D RID: 9565 RVA: 0x000C7D0B File Offset: 0x000C610B
		public GameObject GameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x0600255E RID: 9566 RVA: 0x000C7D13 File Offset: 0x000C6113
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x0600255F RID: 9567 RVA: 0x000C7D1B File Offset: 0x000C611B
		public int MessageNum
		{
			get
			{
				return this.m_MessageList.Count;
			}
		}

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06002560 RID: 9568 RVA: 0x000C7D28 File Offset: 0x000C6128
		public bool IsDispMessage
		{
			get
			{
				return this.m_MessageList.Count > 0;
			}
		}

		// Token: 0x06002561 RID: 9569 RVA: 0x000C7D38 File Offset: 0x000C6138
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_MessageList = new List<string>();
			this.m_GameObject.SetActive(false);
		}

		// Token: 0x06002562 RID: 9570 RVA: 0x000C7D70 File Offset: 0x000C6170
		private void Update()
		{
			if (this.m_MessageList != null && this.m_MessageList.Count > 0)
			{
				this.m_DispTime += Time.deltaTime;
				if (this.m_DispTime > 2f)
				{
					this.m_MessageList.RemoveAt(0);
					this.m_DispTime = 0f;
					if (this.m_MessageList.Count > 0)
					{
						this.m_TextObj.text = this.m_MessageList[0];
					}
					else
					{
						this.m_GameObject.SetActive(false);
					}
				}
			}
		}

		// Token: 0x06002563 RID: 9571 RVA: 0x000C7E0B File Offset: 0x000C620B
		public void SetActive(bool flag)
		{
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x06002564 RID: 9572 RVA: 0x000C7E1C File Offset: 0x000C621C
		public void SetMessage(string text, bool stack = false)
		{
			if (string.IsNullOrEmpty(text))
			{
				return;
			}
			if (!stack)
			{
				this.m_MessageList.Clear();
			}
			if (this.m_MessageList.Count == 0)
			{
				this.m_TextObj.text = text;
				this.m_DispTime = 0f;
			}
			this.m_MessageList.Add(text);
			this.m_GameObject.SetActive(true);
		}

		// Token: 0x04002C10 RID: 11280
		private const float DISPLAY_TIME_MAX = 2f;

		// Token: 0x04002C11 RID: 11281
		private List<string> m_MessageList;

		// Token: 0x04002C12 RID: 11282
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04002C13 RID: 11283
		private float m_DispTime;

		// Token: 0x04002C14 RID: 11284
		private RectTransform m_RectTransform;

		// Token: 0x04002C15 RID: 11285
		private GameObject m_GameObject;
	}
}
