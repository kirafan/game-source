﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000795 RID: 1941
	public class TogetherButton : MonoBehaviour
	{
		// Token: 0x06002785 RID: 10117 RVA: 0x000D321C File Offset: 0x000D161C
		private void Awake()
		{
			this.Apply(0f, true);
			for (int i = 0; i < this.m_StarObjs.Length; i++)
			{
				this.m_StarObjs[i].SetActive(false);
			}
			this.SetEnableFlash(false);
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x000D3264 File Offset: 0x000D1664
		private void Update()
		{
			int num = (int)this.m_DispGauge;
			if (this.m_DispGauge < this.m_GoalGauge)
			{
				this.m_DispGauge += this.m_GaugeSpeed * Time.deltaTime;
				if (this.m_DispGauge > this.m_GoalGauge)
				{
					this.m_DispGauge = this.m_GoalGauge;
				}
				if (num < (int)this.m_DispGauge)
				{
					if ((float)((int)this.m_DispGauge) < 3f)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_KIRARAJUMP_CHARGE, 1f, 0, -1, -1);
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_KIRARAJUMP_CHARGE_MAX, 1f, 0, -1, -1);
					}
				}
			}
			this.m_GaugeBody.fillAmount = this.m_DispGauge - (float)((int)this.m_DispGauge);
			if (this.m_DispGauge >= 3f)
			{
				this.m_GaugeBody.fillAmount = 1f;
			}
			if (this.m_DispGauge >= 3f)
			{
				this.m_GaugeMaxObj.SetActive(true);
			}
			else
			{
				this.m_GaugeMaxObj.SetActive(false);
			}
			if (num != (int)this.m_DispGauge)
			{
				for (int i = 0; i < this.m_StarObjs.Length; i++)
				{
					this.m_StarObjs[i].SetActive(i < (int)this.m_DispGauge);
				}
			}
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x000D33C4 File Offset: 0x000D17C4
		public void Apply(float gauge, bool isImmediate)
		{
			if (gauge < 0f)
			{
				gauge = 0f;
			}
			else if (gauge > 3f)
			{
				gauge = 3f;
			}
			this.m_GoalGauge = gauge;
			if (this.m_DispGauge > this.m_GoalGauge || isImmediate)
			{
				for (int i = 0; i < this.m_StarObjs.Length; i++)
				{
					bool flag = i < (int)gauge;
					if (this.m_StarObjs[i].activeSelf != flag)
					{
						this.m_StarObjs[i].SetActive(flag);
					}
				}
				this.m_DispGauge = this.m_GoalGauge;
			}
			if (gauge >= 1f)
			{
				this.SetExecutableTogetherAttack(true);
			}
			else
			{
				this.SetExecutableTogetherAttack(false);
			}
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x000D3484 File Offset: 0x000D1884
		private void SetEnableFlash(bool flg)
		{
			if (flg)
			{
				this.m_InteractableAnimation[this.m_InteractableAnimation.clip.name].speed = 1f;
				this.m_InteractableAnimation.Play();
				this.m_SelectedAnimObj.SetActive(true);
			}
			else
			{
				this.m_InteractableAnimation[this.m_InteractableAnimation.clip.name].normalizedTime = 0f;
				this.m_InteractableAnimation[this.m_InteractableAnimation.clip.name].speed = 0f;
				this.m_SelectedAnimObj.SetActive(false);
			}
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x000D352F File Offset: 0x000D192F
		public void SetEnableRender(bool flag)
		{
			base.gameObject.SetActive(flag);
		}

		// Token: 0x0600278A RID: 10122 RVA: 0x000D3540 File Offset: 0x000D1940
		public void SetEnableInput(bool flag)
		{
			this.m_IsEnableInput = flag;
			bool flag2 = this.m_Executable && this.m_IsEnableInput;
			this.m_Button.IsEnableInput = flag2;
			this.m_Button.Interactable = this.m_Executable;
			if (flag2)
			{
				this.SetEnableFlash(true);
			}
			else
			{
				this.SetEnableFlash(false);
			}
		}

		// Token: 0x0600278B RID: 10123 RVA: 0x000D35A0 File Offset: 0x000D19A0
		public void SetExecutableTogetherAttack(bool flag)
		{
			this.m_Executable = flag;
			bool flag2 = this.m_Executable && this.m_IsEnableInput;
			this.m_Button.IsEnableInput = flag2;
			this.m_Button.Interactable = this.m_Executable;
			if (flag2)
			{
				this.SetEnableFlash(true);
			}
			else
			{
				this.SetEnableFlash(false);
			}
		}

		// Token: 0x04002E7F RID: 11903
		private const float GAUGE_INCREASE_SPEED_PER_SEC = 0.5f;

		// Token: 0x04002E80 RID: 11904
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04002E81 RID: 11905
		[SerializeField]
		private Animation m_InteractableAnimation;

		// Token: 0x04002E82 RID: 11906
		[SerializeField]
		private GameObject m_SelectedAnimObj;

		// Token: 0x04002E83 RID: 11907
		[SerializeField]
		private Image m_GaugeBody;

		// Token: 0x04002E84 RID: 11908
		[SerializeField]
		private GameObject m_GaugeMaxObj;

		// Token: 0x04002E85 RID: 11909
		[SerializeField]
		private GameObject[] m_StarObjs;

		// Token: 0x04002E86 RID: 11910
		private bool m_IsEnableInput;

		// Token: 0x04002E87 RID: 11911
		private bool m_Executable;

		// Token: 0x04002E88 RID: 11912
		private float m_GoalGauge;

		// Token: 0x04002E89 RID: 11913
		private float m_DispGauge;

		// Token: 0x04002E8A RID: 11914
		private float m_GaugeSpeed = 0.5f;
	}
}
