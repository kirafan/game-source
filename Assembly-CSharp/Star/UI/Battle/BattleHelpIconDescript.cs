﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000752 RID: 1874
	public class BattleHelpIconDescript : MonoBehaviour
	{
		// Token: 0x06002546 RID: 9542 RVA: 0x000C772C File Offset: 0x000C5B2C
		public void Setup(StateIconOwner owner)
		{
			this.m_Owner = owner;
			this.m_StateIcon = this.m_Owner.ScoopEmptyIcon().GetComponent<StateIcon>();
			this.m_StateIcon.RectTransform.SetParent(this.m_IconLocator);
			this.m_StateIcon.RectTransform.localPosition = Vector2.zero;
			this.m_StateIcon.RectTransform.localScale = Vector2.one;
		}

		// Token: 0x06002547 RID: 9543 RVA: 0x000C77A0 File Offset: 0x000C5BA0
		public void Apply(eStateIconType type, bool line)
		{
			this.m_Line.SetActive(line);
			this.m_StateIcon.GameObject.SetActive(true);
			this.m_StateIcon.Apply(type);
			this.m_DescriptText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(this.m_Owner.GetIconData(type).m_DescriptTextID);
		}

		// Token: 0x04002BF1 RID: 11249
		[SerializeField]
		private GameObject m_Line;

		// Token: 0x04002BF2 RID: 11250
		[SerializeField]
		private RectTransform m_IconLocator;

		// Token: 0x04002BF3 RID: 11251
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04002BF4 RID: 11252
		private StateIconOwner m_Owner;

		// Token: 0x04002BF5 RID: 11253
		private StateIcon m_StateIcon;
	}
}
