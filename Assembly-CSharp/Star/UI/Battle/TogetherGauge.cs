﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000799 RID: 1945
	public class TogetherGauge : MonoBehaviour
	{
		// Token: 0x060027A9 RID: 10153 RVA: 0x000D4164 File Offset: 0x000D2564
		public void Apply(float value)
		{
			for (int i = 0; i < this.m_GaugeMask.Length; i++)
			{
				this.m_GaugeMask[i].fillAmount = 0f;
			}
			for (int j = 0; j < 5; j++)
			{
				if (value <= this.OCCUPATION[j])
				{
					this.m_GaugeMask[j].fillAmount = this.OCCUPATION_START[j] + value / this.OCCUPATION[j] * (this.OCCUPATION_END[j] - this.OCCUPATION_START[j]);
					break;
				}
				value -= this.OCCUPATION[j];
				this.m_GaugeMask[j].fillAmount = this.OCCUPATION_END[j];
			}
		}

		// Token: 0x04002EBE RID: 11966
		private float[] OCCUPATION = new float[]
		{
			0.1f,
			0.1f,
			0.6f,
			0.1f,
			0.1f
		};

		// Token: 0x04002EBF RID: 11967
		private float[] OCCUPATION_START = new float[]
		{
			0f,
			0.14f,
			0.25f,
			0.75f,
			0.95f
		};

		// Token: 0x04002EC0 RID: 11968
		private float[] OCCUPATION_END = new float[]
		{
			0.05f,
			0.25f,
			0.75f,
			0.86f,
			1f
		};

		// Token: 0x04002EC1 RID: 11969
		[SerializeField]
		private Image[] m_GaugeMask;
	}
}
