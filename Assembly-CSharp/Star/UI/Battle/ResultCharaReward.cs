﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007A5 RID: 1957
	public class ResultCharaReward : UIGroup
	{
		// Token: 0x060027FC RID: 10236 RVA: 0x000D6284 File Offset: 0x000D4684
		private void OnDestroy()
		{
			if (this.m_CharaMaskMaterial_PL0 != null)
			{
				UnityEngine.Object.Destroy(this.m_CharaMaskMaterial_PL0);
				this.m_CharaMaskMaterial_PL0 = null;
			}
			if (this.m_CharaMaskImage_PL0 != null)
			{
				this.m_CharaMaskImage_PL0.material = null;
				this.m_CharaMaskImage_PL0 = null;
			}
			if (this.m_CharaMaskMaterial_PL1 != null)
			{
				UnityEngine.Object.Destroy(this.m_CharaMaskMaterial_PL1);
				this.m_CharaMaskMaterial_PL1 = null;
			}
			if (this.m_CharaMaskImage_PL1 != null)
			{
				this.m_CharaMaskImage_PL1.material = null;
				this.m_CharaMaskImage_PL1 = null;
			}
			if (this.m_CharaMaskMaterial_PL2 != null)
			{
				UnityEngine.Object.Destroy(this.m_CharaMaskMaterial_PL2);
				this.m_CharaMaskMaterial_PL2 = null;
			}
			if (this.m_CharaMaskImage_PL2 != null)
			{
				this.m_CharaMaskImage_PL2.material = null;
				this.m_CharaMaskImage_PL2 = null;
			}
			if (this.m_CharaMaskMaterial_PL3 != null)
			{
				UnityEngine.Object.Destroy(this.m_CharaMaskMaterial_PL3);
				this.m_CharaMaskMaterial_PL3 = null;
			}
			if (this.m_CharaMaskImage_PL3 != null)
			{
				this.m_CharaMaskImage_PL3.material = null;
				this.m_CharaMaskImage_PL3 = null;
			}
			if (this.m_CharaMaskMaterial_PL4 != null)
			{
				UnityEngine.Object.Destroy(this.m_CharaMaskMaterial_PL4);
				this.m_CharaMaskMaterial_PL4 = null;
			}
			if (this.m_CharaMaskImage_PL4 != null)
			{
				this.m_CharaMaskImage_PL4.material = null;
				this.m_CharaMaskImage_PL4 = null;
			}
		}

		// Token: 0x060027FD RID: 10237 RVA: 0x000D63F4 File Offset: 0x000D47F4
		public void Setup()
		{
			for (int i = 0; i < this.m_CharaData.Length; i++)
			{
				this.m_CharaData[i].Setup();
			}
			if (this.m_CharaMaskMaterialBase != null)
			{
				if (this.m_CharaMaskImage_PL0 != null && this.m_CharaMaskMaterial_PL0 == null)
				{
					this.m_CharaMaskMaterial_PL0 = UnityEngine.Object.Instantiate<Material>(this.m_CharaMaskMaterialBase);
					MeigeShaderUtility.SetStencilID(this.m_CharaMaskMaterial_PL0, byte.MaxValue);
					this.m_CharaMaskImage_PL0.material = this.m_CharaMaskMaterial_PL0;
				}
				if (this.m_CharaMaskImage_PL1 != null && this.m_CharaMaskMaterial_PL1 == null)
				{
					this.m_CharaMaskMaterial_PL1 = UnityEngine.Object.Instantiate<Material>(this.m_CharaMaskMaterialBase);
					MeigeShaderUtility.SetStencilID(this.m_CharaMaskMaterial_PL1, 254);
					this.m_CharaMaskImage_PL1.material = this.m_CharaMaskMaterial_PL1;
				}
				if (this.m_CharaMaskImage_PL2 != null && this.m_CharaMaskMaterial_PL2 == null)
				{
					this.m_CharaMaskMaterial_PL2 = UnityEngine.Object.Instantiate<Material>(this.m_CharaMaskMaterialBase);
					MeigeShaderUtility.SetStencilID(this.m_CharaMaskMaterial_PL2, 253);
					this.m_CharaMaskImage_PL2.material = this.m_CharaMaskMaterial_PL2;
				}
				if (this.m_CharaMaskImage_PL3 != null && this.m_CharaMaskMaterial_PL3 == null)
				{
					this.m_CharaMaskMaterial_PL3 = UnityEngine.Object.Instantiate<Material>(this.m_CharaMaskMaterialBase);
					MeigeShaderUtility.SetStencilID(this.m_CharaMaskMaterial_PL3, 252);
					this.m_CharaMaskImage_PL3.material = this.m_CharaMaskMaterial_PL3;
				}
				if (this.m_CharaMaskImage_PL4 != null && this.m_CharaMaskMaterial_PL4 == null)
				{
					this.m_CharaMaskMaterial_PL4 = UnityEngine.Object.Instantiate<Material>(this.m_CharaMaskMaterialBase);
					MeigeShaderUtility.SetStencilID(this.m_CharaMaskMaterial_PL4, 251);
					this.m_CharaMaskImage_PL4.material = this.m_CharaMaskMaterial_PL4;
				}
			}
		}

		// Token: 0x060027FE RID: 10238 RVA: 0x000D65DC File Offset: 0x000D49DC
		public void SetRewardExp(long value)
		{
			this.m_GetExpText.text = "+" + value.ToString();
		}

		// Token: 0x060027FF RID: 10239 RVA: 0x000D6600 File Offset: 0x000D4A00
		public void SetRewardFriendshipExp(long value)
		{
			this.m_GetFriendshipExpText.text = "+" + value.ToString();
		}

		// Token: 0x06002800 RID: 10240 RVA: 0x000D6624 File Offset: 0x000D4A24
		public void SetCharaData(int idx, int limitbreak)
		{
			this.m_CharaData[idx].SetData(limitbreak);
		}

		// Token: 0x06002801 RID: 10241 RVA: 0x000D6634 File Offset: 0x000D4A34
		public void SetCharaExpData(int idx, long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExps)
		{
			this.m_CharaData[idx].SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExps);
		}

		// Token: 0x06002802 RID: 10242 RVA: 0x000D664F File Offset: 0x000D4A4F
		public void SetCharaFriendshipData(int idx, int beforeLevel, long beforeExp, int afterLevel, long afterExp, sbyte expTableID)
		{
			this.m_CharaData[idx].SetFriendshipData(beforeLevel, beforeExp, afterLevel, afterExp, expTableID);
		}

		// Token: 0x06002803 RID: 10243 RVA: 0x000D6668 File Offset: 0x000D4A68
		public override void Update()
		{
			base.Update();
			if (this.m_Step == ResultCharaReward.eStep.Exp && this.m_EXPObjAnim.State == 0)
			{
				this.m_EXPObjAnim.PlayIn();
			}
			else if (this.m_Step == ResultCharaReward.eStep.Friendship && this.m_FriendshipObjAnim.State == 0)
			{
				this.m_EXPObjAnim.PlayOut();
				this.m_FriendshipObjAnim.PlayIn();
			}
			ResultCharaReward.eStep step = this.m_Step;
			if (step != ResultCharaReward.eStep.Idle)
			{
				if (step != ResultCharaReward.eStep.Exp)
				{
					if (step == ResultCharaReward.eStep.Friendship)
					{
						bool flag = true;
						for (int i = 0; i < this.m_CharaData.Length; i++)
						{
							if (this.m_CharaData[i].IsExist && this.m_CharaData[i].Step != ResultCharaData.eStep.FriendshipStop)
							{
								flag = false;
								if (this.m_CharaData[i].Step == ResultCharaData.eStep.Friendship && this.m_SoundHandler == null)
								{
									this.m_SoundHandler = new SoundHandler();
									SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_EXP_GAUGE, this.m_SoundHandler, 1f, 0, -1, -1);
								}
							}
						}
						if (flag)
						{
							this.m_Step = ResultCharaReward.eStep.FriendshipStop;
							this.StopSE();
						}
					}
				}
				else
				{
					bool flag2 = true;
					for (int j = 0; j < this.m_CharaData.Length; j++)
					{
						if (this.m_CharaData[j].IsExist && this.m_CharaData[j].Step != ResultCharaData.eStep.ExpStop)
						{
							flag2 = false;
							if (this.m_CharaData[j].Step == ResultCharaData.eStep.Exp && this.m_SoundHandler == null)
							{
								this.m_SoundHandler = new SoundHandler();
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_EXP_GAUGE, this.m_SoundHandler, 1f, 0, -1, -1);
							}
						}
					}
					if (flag2)
					{
						this.m_Step = ResultCharaReward.eStep.ExpStop;
						this.StopSE();
					}
				}
			}
			else if (!this.m_IsDoneStart && this.m_TimeCount > 0.2f)
			{
				for (int k = 0; k < this.m_CharaData.Length; k++)
				{
					if (this.m_CharaData[k].IsExist)
					{
						this.m_CharaData[k].PlayExp();
					}
				}
				this.m_IsDoneStart = true;
				this.m_Step = ResultCharaReward.eStep.Exp;
			}
			this.m_TimeCount += Time.deltaTime;
		}

		// Token: 0x06002804 RID: 10244 RVA: 0x000D68C7 File Offset: 0x000D4CC7
		private void StopSE()
		{
			if (this.m_SoundHandler != null)
			{
				this.m_SoundHandler.Stop();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHandler, true);
				this.m_SoundHandler = null;
			}
		}

		// Token: 0x06002805 RID: 10245 RVA: 0x000D6900 File Offset: 0x000D4D00
		public void Tap()
		{
			switch (this.m_Step)
			{
			case ResultCharaReward.eStep.Exp:
				for (int i = 0; i < this.m_CharaData.Length; i++)
				{
					if (this.m_CharaData[i].IsExist)
					{
						this.m_CharaData[i].SkipExp();
					}
				}
				this.StopSE();
				this.m_Step = ResultCharaReward.eStep.ExpStop;
				break;
			case ResultCharaReward.eStep.ExpStop:
				for (int j = 0; j < this.m_CharaData.Length; j++)
				{
					if (this.m_CharaData[j].IsExist)
					{
						this.m_CharaData[j].PlayFriendship();
					}
				}
				this.StopSE();
				this.m_Step = ResultCharaReward.eStep.Friendship;
				break;
			case ResultCharaReward.eStep.Friendship:
				for (int k = 0; k < this.m_CharaData.Length; k++)
				{
					if (this.m_CharaData[k].IsExist)
					{
						this.m_CharaData[k].SkipFriendship();
					}
				}
				this.StopSE();
				this.m_Step = ResultCharaReward.eStep.FriendshipStop;
				break;
			case ResultCharaReward.eStep.FriendshipStop:
				for (int l = 0; l < this.m_CharaData.Length; l++)
				{
					if (this.m_CharaData[l].IsExist)
					{
						this.m_CharaData[l].SetEnableRenderLevelUpPop();
					}
				}
				this.StopSE();
				SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.OpenStackPop();
				if (SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.IsOpen())
				{
					this.m_Step = ResultCharaReward.eStep.UnlockWait;
				}
				else
				{
					this.Close();
					this.m_Step = ResultCharaReward.eStep.End;
				}
				break;
			case ResultCharaReward.eStep.UnlockWait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.IsOpen())
				{
					this.Close();
					this.m_Step = ResultCharaReward.eStep.End;
				}
				break;
			}
		}

		// Token: 0x06002806 RID: 10246 RVA: 0x000D6ABE File Offset: 0x000D4EBE
		public Vector2 GetCharaPosition()
		{
			return base.GetComponent<RectTransform>().localPosition + this.m_CharaPosition.localPosition;
		}

		// Token: 0x04002F31 RID: 12081
		private const float STARTTIME = 0.2f;

		// Token: 0x04002F32 RID: 12082
		[SerializeField]
		private AnimUIPlayer m_EXPObjAnim;

		// Token: 0x04002F33 RID: 12083
		[SerializeField]
		private AnimUIPlayer m_FriendshipObjAnim;

		// Token: 0x04002F34 RID: 12084
		[SerializeField]
		private ResultCharaData[] m_CharaData;

		// Token: 0x04002F35 RID: 12085
		[SerializeField]
		private Text m_GetExpText;

		// Token: 0x04002F36 RID: 12086
		[SerializeField]
		private Text m_GetFriendshipExpText;

		// Token: 0x04002F37 RID: 12087
		[SerializeField]
		private RectTransform m_CharaPosition;

		// Token: 0x04002F38 RID: 12088
		private float m_TimeCount;

		// Token: 0x04002F39 RID: 12089
		private bool m_IsDoneStart;

		// Token: 0x04002F3A RID: 12090
		private SoundHandler m_SoundHandler;

		// Token: 0x04002F3B RID: 12091
		[SerializeField]
		private Material m_CharaMaskMaterialBase;

		// Token: 0x04002F3C RID: 12092
		[SerializeField]
		private Image m_CharaMaskImage_PL0;

		// Token: 0x04002F3D RID: 12093
		[SerializeField]
		private Image m_CharaMaskImage_PL1;

		// Token: 0x04002F3E RID: 12094
		[SerializeField]
		private Image m_CharaMaskImage_PL2;

		// Token: 0x04002F3F RID: 12095
		[SerializeField]
		private Image m_CharaMaskImage_PL3;

		// Token: 0x04002F40 RID: 12096
		[SerializeField]
		private Image m_CharaMaskImage_PL4;

		// Token: 0x04002F41 RID: 12097
		private Material m_CharaMaskMaterial_PL0;

		// Token: 0x04002F42 RID: 12098
		private Material m_CharaMaskMaterial_PL1;

		// Token: 0x04002F43 RID: 12099
		private Material m_CharaMaskMaterial_PL2;

		// Token: 0x04002F44 RID: 12100
		private Material m_CharaMaskMaterial_PL3;

		// Token: 0x04002F45 RID: 12101
		private Material m_CharaMaskMaterial_PL4;

		// Token: 0x04002F46 RID: 12102
		private ResultCharaReward.eStep m_Step;

		// Token: 0x020007A6 RID: 1958
		private enum eStep
		{
			// Token: 0x04002F48 RID: 12104
			Idle,
			// Token: 0x04002F49 RID: 12105
			Exp,
			// Token: 0x04002F4A RID: 12106
			ExpStop,
			// Token: 0x04002F4B RID: 12107
			Friendship,
			// Token: 0x04002F4C RID: 12108
			FriendshipStop,
			// Token: 0x04002F4D RID: 12109
			UnlockWait,
			// Token: 0x04002F4E RID: 12110
			End
		}
	}
}
