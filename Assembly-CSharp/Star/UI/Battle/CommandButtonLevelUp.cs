﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200076E RID: 1902
	public class CommandButtonLevelUp : MonoBehaviour
	{
		// Token: 0x14000050 RID: 80
		// (add) Token: 0x060026B8 RID: 9912 RVA: 0x000CE120 File Offset: 0x000CC520
		// (remove) Token: 0x060026B9 RID: 9913 RVA: 0x000CE158 File Offset: 0x000CC558
		public event Action OnEndAnimation;

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x060026BA RID: 9914 RVA: 0x000CE18E File Offset: 0x000CC58E
		public Animation Animation
		{
			get
			{
				if (this.m_Animation == null)
				{
					this.m_Animation = base.GetComponent<Animation>();
				}
				return this.m_Animation;
			}
		}

		// Token: 0x060026BB RID: 9915 RVA: 0x000CE1B3 File Offset: 0x000CC5B3
		private void Update()
		{
			if (!this.Animation.isPlaying)
			{
				this.OnEndAnimation.Call();
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x060026BC RID: 9916 RVA: 0x000CE1DC File Offset: 0x000CC5DC
		public void Play()
		{
			base.gameObject.SetActive(true);
			this.Animation.Play();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_SKILLLV_UP, 1f, 0, -1, -1);
		}

		// Token: 0x04002D45 RID: 11589
		private Animation m_Animation;
	}
}
