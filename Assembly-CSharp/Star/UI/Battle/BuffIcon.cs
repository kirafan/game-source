﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000767 RID: 1895
	[RequireComponent(typeof(CanvasGroup))]
	public class BuffIcon : MonoBehaviour
	{
		// Token: 0x0600267B RID: 9851 RVA: 0x000CC8EC File Offset: 0x000CACEC
		public void SetStateIconOwner(StateIconOwner owner)
		{
			this.m_IconOwner = owner;
		}

		// Token: 0x0600267C RID: 9852 RVA: 0x000CC8F5 File Offset: 0x000CACF5
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			this.Clear();
		}

		// Token: 0x0600267D RID: 9853 RVA: 0x000CC924 File Offset: 0x000CAD24
		public void Clear()
		{
			if (base.GetComponent<iTween>() != null)
			{
				iTween.Stop(this.m_GameObject, "value");
			}
			if (this.m_CurrentStateIcon != null)
			{
				this.m_IconOwner.SinkEmptyIcon(this.m_CurrentStateIcon);
			}
			this.m_CurrentStateIcon = null;
			this.m_CurrentBuff = eStateIconType.None;
			for (int i = 0; i < this.m_Buffs.Length; i++)
			{
				this.m_Buffs[i] = false;
			}
			this.m_BuffNum = 0;
		}

		// Token: 0x0600267E RID: 9854 RVA: 0x000CC9AB File Offset: 0x000CADAB
		public void SetIcon(eStateIconType type, bool flg)
		{
			this.m_Buffs[(int)type] = flg;
			this.m_IsDirty = true;
		}

		// Token: 0x0600267F RID: 9855 RVA: 0x000CC9BD File Offset: 0x000CADBD
		private void LateUpdate()
		{
			if (this.m_IsDirty)
			{
				this.Apply();
				this.m_IsDirty = false;
			}
		}

		// Token: 0x06002680 RID: 9856 RVA: 0x000CC9D8 File Offset: 0x000CADD8
		public void Apply()
		{
			int num = 0;
			for (int i = 0; i < this.m_Buffs.Length; i++)
			{
				if (this.m_Buffs[i])
				{
					num++;
				}
			}
			this.m_BuffNum = num;
			if (num > 0)
			{
				if (this.m_CurrentBuff == eStateIconType.None || !this.m_Buffs[(int)this.m_CurrentBuff])
				{
					iTween.Stop(this.m_GameObject, "value");
					this.ChangeStateIcon();
				}
				else if (num > 1 && this.m_CanvasGroup.alpha >= 1f)
				{
					this.PlayOut();
				}
			}
			else
			{
				this.Clear();
			}
		}

		// Token: 0x06002681 RID: 9857 RVA: 0x000CCA84 File Offset: 0x000CAE84
		private void PlayIn()
		{
			iTween.Stop(this.m_GameObject, "value");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", 0f);
			hashtable.Add("to", 1f);
			hashtable.Add("time", 0.2f);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", iTween.EaseType.linear);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "PlayOut");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002682 RID: 9858 RVA: 0x000CCB5C File Offset: 0x000CAF5C
		private void PlayOut()
		{
			iTween.Stop(this.m_GameObject, "value");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("from", 1f);
			hashtable.Add("to", 0f);
			hashtable.Add("time", 0.2f);
			hashtable.Add("delay", 1f);
			hashtable.Add("easeType", iTween.EaseType.linear);
			hashtable.Add("onupdate", "OnUpdateAlpha");
			hashtable.Add("onupdatetarget", this.m_GameObject);
			hashtable.Add("oncomplete", "ChangeStateIcon");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ValueTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002683 RID: 9859 RVA: 0x000CCC33 File Offset: 0x000CB033
		private void OnUpdateAlpha(float value)
		{
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x06002684 RID: 9860 RVA: 0x000CCC44 File Offset: 0x000CB044
		private void ChangeStateIcon()
		{
			int num = (int)this.m_CurrentBuff;
			for (int i = 0; i < this.m_Buffs.Length; i++)
			{
				num++;
				if (num >= this.m_Buffs.Length)
				{
					num = 0;
				}
				if (this.m_Buffs[num])
				{
					this.m_CurrentBuff = (eStateIconType)num;
					break;
				}
			}
			if (this.m_CurrentStateIcon == null)
			{
				this.m_CurrentStateIcon = this.m_IconOwner.ScoopEmptyIcon().GetComponent<StateIcon>();
				this.m_CurrentStateIcon.GameObject.SetActive(true);
				this.m_CurrentStateIcon.RectTransform.SetParent(this.m_RectTransform);
				this.m_CurrentStateIcon.RectTransform.localPosition = Vector2.zero;
				this.m_CurrentStateIcon.RectTransform.localScale = Vector2.one;
			}
			this.m_CurrentStateIcon.Apply(this.m_CurrentBuff);
			if (this.m_BuffNum > 1)
			{
				this.m_CanvasGroup.alpha = 0f;
				this.PlayIn();
			}
			else
			{
				this.m_CanvasGroup.alpha = 1f;
			}
		}

		// Token: 0x04002CEE RID: 11502
		private const float TRANSIT_SEC = 0.2f;

		// Token: 0x04002CEF RID: 11503
		private const float DISP_SEC = 1f;

		// Token: 0x04002CF0 RID: 11504
		[SerializeField]
		private StateIconOwner m_IconOwner;

		// Token: 0x04002CF1 RID: 11505
		private StateIcon m_CurrentStateIcon;

		// Token: 0x04002CF2 RID: 11506
		private bool[] m_Buffs = new bool[45];

		// Token: 0x04002CF3 RID: 11507
		private eStateIconType m_CurrentBuff;

		// Token: 0x04002CF4 RID: 11508
		private int m_BuffNum;

		// Token: 0x04002CF5 RID: 11509
		private bool m_IsDirty;

		// Token: 0x04002CF6 RID: 11510
		private GameObject m_GameObject;

		// Token: 0x04002CF7 RID: 11511
		private RectTransform m_RectTransform;

		// Token: 0x04002CF8 RID: 11512
		private CanvasGroup m_CanvasGroup;
	}
}
