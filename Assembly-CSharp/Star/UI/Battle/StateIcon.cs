﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200078C RID: 1932
	public class StateIcon : MonoBehaviour
	{
		// Token: 0x06002769 RID: 10089 RVA: 0x000D2354 File Offset: 0x000D0754
		public static eStateIconType ConvertUIStatusIconToType(BattleDefine.eFlagForUIStatusIcon icon, sbyte value, bool up)
		{
			switch (icon)
			{
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_Atk:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusAtkUp;
				}
				return eStateIconType.StatusAtkDown;
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_Mgc:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusMgcUp;
				}
				return eStateIconType.StatusMgcDown;
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_Def:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusDefUp;
				}
				return eStateIconType.StatusDefDown;
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_MDef:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusMDefUp;
				}
				return eStateIconType.StatusMDefDown;
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_Spd:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusSpdUp;
				}
				return eStateIconType.StatusSpdDown;
			case BattleDefine.eFlagForUIStatusIcon.StatusChange_Luck:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StatusLuckUp;
				}
				return eStateIconType.StatusLuckDown;
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Confusion:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Paralysis:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Poison:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Bearish:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Sleep:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Unhappy:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Silence:
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormal_Isolation:
				return eStateIconType.Confusion + (int)icon - 7;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Fire:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementFireUp;
				}
				return eStateIconType.ElementFireDown;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Water:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementWaterUp;
				}
				return eStateIconType.ElementWaterDown;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Earth:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementEarthUp;
				}
				return eStateIconType.ElementEarthDown;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Wind:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementWindUp;
				}
				return eStateIconType.ElementWindDown;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Moon:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementMoonUp;
				}
				return eStateIconType.ElementMoonDown;
			case BattleDefine.eFlagForUIStatusIcon.ResistElement_Sun:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.ElementSunUp;
				}
				return eStateIconType.ElementSunDown;
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormalAdditionalProbability:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.StateAbnormalAdditionalProbabilityUp;
				}
				return eStateIconType.StateAbnormalAdditionalProbabilityDown;
			case BattleDefine.eFlagForUIStatusIcon.StateAbnormalDisable:
				return eStateIconType.StateAbnormalDisable;
			case BattleDefine.eFlagForUIStatusIcon.WeakElementBonus:
				return eStateIconType.WeakElementBonus;
			case BattleDefine.eFlagForUIStatusIcon.NextAtkUp:
				return eStateIconType.NextAtkUp;
			case BattleDefine.eFlagForUIStatusIcon.NextMgcUp:
				return eStateIconType.NextMgcUp;
			case BattleDefine.eFlagForUIStatusIcon.NextCritical:
				return eStateIconType.NextCritical;
			case BattleDefine.eFlagForUIStatusIcon.Barrier:
				return eStateIconType.Barrier;
			case BattleDefine.eFlagForUIStatusIcon.HateChange:
				if ((int)value > 0 || ((int)value == 0 && up))
				{
					return eStateIconType.HateUp;
				}
				return eStateIconType.HateDown;
			case BattleDefine.eFlagForUIStatusIcon.Regene:
				return eStateIconType.Regene;
			default:
				Debug.LogWarning("Unknown Icon" + icon.ToString());
				return eStateIconType.None;
			}
		}

		// Token: 0x1700029C RID: 668
		// (get) Token: 0x0600276A RID: 10090 RVA: 0x000D2594 File Offset: 0x000D0994
		public GameObject GameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x0600276B RID: 10091 RVA: 0x000D259C File Offset: 0x000D099C
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x0600276C RID: 10092 RVA: 0x000D25A4 File Offset: 0x000D09A4
		public void Setup(StateIconOwner owner)
		{
			this.m_Owner = owner;
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x0600276D RID: 10093 RVA: 0x000D25C8 File Offset: 0x000D09C8
		public void Apply(eStateIconType type)
		{
			StateIconOwner.StateIconData iconData = this.m_Owner.GetIconData(type);
			if (iconData.m_Sprite != null)
			{
				this.m_MainImage.enabled = true;
				this.m_MainImage.sprite = iconData.m_Sprite;
			}
			else
			{
				this.m_MainImage.enabled = false;
			}
			StateIcon.eSubIconType subIconType = iconData.m_SubIconType;
			if (subIconType != StateIcon.eSubIconType.None)
			{
				if (subIconType != StateIcon.eSubIconType.UpArrow)
				{
					if (subIconType == StateIcon.eSubIconType.DownArrow)
					{
						this.m_SubImage.enabled = true;
						this.m_SubImage.material = this.m_ColorScreenMaterial;
						this.m_SubImage.color = this.m_ColorDown;
						this.m_SubImage.sprite = this.m_Owner.GetArrowSprite();
						this.m_SubImage.GetComponent<RectTransform>().localScaleY(-1f);
					}
				}
				else
				{
					this.m_SubImage.enabled = true;
					this.m_SubImage.material = this.m_ColorScreenMaterial;
					this.m_SubImage.color = this.m_ColorUp;
					this.m_SubImage.sprite = this.m_Owner.GetArrowSprite();
					this.m_SubImage.GetComponent<RectTransform>().localScaleY(1f);
				}
			}
			else
			{
				this.m_SubImage.enabled = false;
				this.m_SubImage.material = null;
				this.m_SubImage.color = Color.white;
				this.m_SubImage.sprite = null;
			}
			this.m_NextImage.enabled = iconData.m_IsNextImage;
			StateIcon.eColorChange colorChangeType = iconData.m_ColorChangeType;
			if (colorChangeType != StateIcon.eColorChange.None)
			{
				if (colorChangeType != StateIcon.eColorChange.Up)
				{
					if (colorChangeType == StateIcon.eColorChange.Down)
					{
						this.m_MainImage.material = this.m_ColorScreenMaterial;
						this.m_MainImage.color = this.m_ColorDown;
					}
				}
				else
				{
					this.m_MainImage.material = this.m_ColorScreenMaterial;
					this.m_MainImage.color = this.m_ColorUp;
				}
			}
			else
			{
				this.m_MainImage.material = null;
				this.m_MainImage.color = Color.white;
			}
		}

		// Token: 0x04002E42 RID: 11842
		[SerializeField]
		private Color m_ColorUp = new Color(1f, 0f, 0f, 1f);

		// Token: 0x04002E43 RID: 11843
		[SerializeField]
		private Color m_ColorDown = new Color(0f, 0f, 1f, 1f);

		// Token: 0x04002E44 RID: 11844
		public const int ABNORMAL_START_IDX = 1;

		// Token: 0x04002E45 RID: 11845
		public const int ABNORMAL_END_IDX = 8;

		// Token: 0x04002E46 RID: 11846
		public const int ABNORMAL_NUM = 8;

		// Token: 0x04002E47 RID: 11847
		[SerializeField]
		private Image m_MainImage;

		// Token: 0x04002E48 RID: 11848
		[SerializeField]
		private Image m_SubImage;

		// Token: 0x04002E49 RID: 11849
		[SerializeField]
		private Image m_NextImage;

		// Token: 0x04002E4A RID: 11850
		[SerializeField]
		private Material m_ColorScreenMaterial;

		// Token: 0x04002E4B RID: 11851
		private StateIconOwner m_Owner;

		// Token: 0x04002E4C RID: 11852
		private GameObject m_GameObject;

		// Token: 0x04002E4D RID: 11853
		private RectTransform m_RectTransform;

		// Token: 0x0200078D RID: 1933
		public enum eSubIconType
		{
			// Token: 0x04002E4F RID: 11855
			None,
			// Token: 0x04002E50 RID: 11856
			UpArrow,
			// Token: 0x04002E51 RID: 11857
			DownArrow,
			// Token: 0x04002E52 RID: 11858
			Next
		}

		// Token: 0x0200078E RID: 1934
		public enum eColorChange
		{
			// Token: 0x04002E54 RID: 11860
			None,
			// Token: 0x04002E55 RID: 11861
			Up,
			// Token: 0x04002E56 RID: 11862
			Down
		}
	}
}
