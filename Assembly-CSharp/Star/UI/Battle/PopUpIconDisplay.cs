﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000786 RID: 1926
	public class PopUpIconDisplay : MonoBehaviour
	{
		// Token: 0x0600274A RID: 10058 RVA: 0x000D143C File Offset: 0x000CF83C
		public void Setup()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			for (int i = 0; i < 16; i++)
			{
				PopUpIcon popUpIcon = UnityEngine.Object.Instantiate<PopUpIcon>(this.m_PopUpIconPrefab, this.m_RectTransform, false);
				popUpIcon.Setup(this.m_StateIconOwner);
				popUpIcon.SetActive(false);
				this.m_EmptyList.Add(popUpIcon);
			}
			this.m_CalcObj = UnityEngine.Object.Instantiate<PopUpIcon>(this.m_PopUpIconPrefab, this.m_RectTransform, false);
			this.m_CalcObj.Setup(this.m_StateIconOwner);
			this.m_CalcObj.SetActive(true);
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x000D14D0 File Offset: 0x000CF8D0
		private void Update()
		{
			List<PopUpIconDisplay.PopUpIconReserveData> reserveWorkList = this.m_ReserveWorkList;
			reserveWorkList.Clear();
			for (int i = 0; i < this.m_ReserveList.Count; i++)
			{
				this.m_ReserveList[i].m_Delay = this.m_ReserveList[i].m_Delay - Time.deltaTime;
				if (this.m_ReserveList[i].m_Delay <= 0f)
				{
					this.DisplayFromReserve(this.m_ReserveList[i]);
					reserveWorkList.Add(this.m_ReserveList[i]);
				}
			}
			while (reserveWorkList.Count > 0)
			{
				this.m_ReserveList.Remove(reserveWorkList[0]);
				reserveWorkList.RemoveAt(0);
			}
			this.RemoveCompleteDisplay();
		}

		// Token: 0x0600274C RID: 10060 RVA: 0x000D15A0 File Offset: 0x000CF9A0
		public void Display(List<SkillActionEffectSet.IconData> datas, Transform targetTransform, float offsetY)
		{
			float num = 0f;
			this.m_CalcObj.SetActive(true);
			bool iconSpace = false;
			for (int i = 0; i < datas.Count; i++)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.PopUpStateIconListDB.GetParam(datas[i].m_IconType).m_StateIconType != 0 && !datas[i].m_IsMiss)
				{
					iconSpace = true;
				}
				PopUpIconDisplay.PopUpIconReserveData popUpIconReserveData = new PopUpIconDisplay.PopUpIconReserveData();
				popUpIconReserveData.m_Data = datas[i];
				popUpIconReserveData.m_TargetTransform = targetTransform;
				popUpIconReserveData.m_OffsetYFromTargetTransform = offsetY;
				popUpIconReserveData.m_PivotX = 0.5f;
				popUpIconReserveData.m_OffsetUISpace = new Vector2(0f, (float)datas.Count * 32f - (float)i * 32f);
				popUpIconReserveData.m_Delay = 0.1f * (float)(i / 2);
				this.m_ReserveList.Add(popUpIconReserveData);
				this.m_CalcObj.Apply(datas[i], false);
				float num2 = this.m_CalcObj.CalcWidth();
				if (num2 > num)
				{
					num = num2;
				}
				this.m_CalcObj.Clear();
			}
			this.m_CalcObj.SetActive(false);
			for (int j = 0; j < this.m_ReserveList.Count; j++)
			{
				this.m_ReserveList[j].m_OffsetUISpace = new Vector2(-num * 0.5f, this.m_ReserveList[j].m_OffsetUISpace.y);
				this.m_ReserveList[j].m_IconSpace = iconSpace;
			}
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x000D173C File Offset: 0x000CFB3C
		private void DisplayFromReserve(PopUpIconDisplay.PopUpIconReserveData data)
		{
			if (this.m_EmptyList.Count <= 0)
			{
				PopUpIcon popUpIcon = UnityEngine.Object.Instantiate<PopUpIcon>(this.m_PopUpIconPrefab, this.m_RectTransform, false);
				popUpIcon.Setup(this.m_StateIconOwner);
				popUpIcon.SetActive(false);
				this.m_EmptyList.Add(popUpIcon);
			}
			this.m_EmptyList[0].Display(data.m_Data, data.m_TargetTransform, data.m_OffsetYFromTargetTransform, data.m_PivotX, data.m_OffsetUISpace, data.m_IconSpace);
			this.m_EmptyList[0].RectTransform.SetAsLastSibling();
			bool flag = false;
			for (int i = 0; i < this.m_ActiveList.Count; i++)
			{
				if (!flag && this.m_ActiveList[i].TimeCount < 0.1f && this.m_ActiveList[i].TargetPosition.y < this.m_EmptyList[0].TargetPosition.y)
				{
					this.m_ActiveList.Insert(i, this.m_EmptyList[0]);
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				this.m_ActiveList.Add(this.m_EmptyList[0]);
			}
			for (int j = 0; j < this.m_ActiveList.Count; j++)
			{
				this.m_ActiveList[j].RectTransform.SetSiblingIndex(j);
			}
			this.m_EmptyList.RemoveAt(0);
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x000D18D0 File Offset: 0x000CFCD0
		private void RemoveCompleteDisplay()
		{
			List<PopUpIcon> workList = this.m_WorkList;
			workList.Clear();
			for (int i = 0; i < this.m_ActiveList.Count; i++)
			{
				if (this.m_ActiveList[i].IsComplete())
				{
					workList.Add(this.m_ActiveList[i]);
				}
			}
			while (workList.Count > 0)
			{
				workList[0].SetActive(false);
				this.m_EmptyList.Add(workList[0]);
				this.m_ActiveList.Remove(workList[0]);
				workList.RemoveAt(0);
			}
		}

		// Token: 0x0600274F RID: 10063 RVA: 0x000D1978 File Offset: 0x000CFD78
		public bool IsPlaying()
		{
			return this.m_ActiveList.Count > 0;
		}

		// Token: 0x04002E0D RID: 11789
		private const int DISPLAY_CACHE_NUM = 16;

		// Token: 0x04002E0E RID: 11790
		private const float DELAY_TIME_PER_IDX = 0.1f;

		// Token: 0x04002E0F RID: 11791
		private const float SPACE_WIDTH = 256f;

		// Token: 0x04002E10 RID: 11792
		private const float SPACE_HEIGHT = 32f;

		// Token: 0x04002E11 RID: 11793
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04002E12 RID: 11794
		[SerializeField]
		private PopUpIcon m_PopUpIconPrefab;

		// Token: 0x04002E13 RID: 11795
		private List<PopUpIcon> m_EmptyList = new List<PopUpIcon>();

		// Token: 0x04002E14 RID: 11796
		private List<PopUpIcon> m_ActiveList = new List<PopUpIcon>();

		// Token: 0x04002E15 RID: 11797
		private List<PopUpIcon> m_WorkList = new List<PopUpIcon>();

		// Token: 0x04002E16 RID: 11798
		private PopUpIcon m_CalcObj;

		// Token: 0x04002E17 RID: 11799
		private List<PopUpIconDisplay.PopUpIconReserveData> m_ReserveList = new List<PopUpIconDisplay.PopUpIconReserveData>();

		// Token: 0x04002E18 RID: 11800
		private List<PopUpIconDisplay.PopUpIconReserveData> m_ReserveWorkList = new List<PopUpIconDisplay.PopUpIconReserveData>();

		// Token: 0x04002E19 RID: 11801
		private RectTransform m_RectTransform;

		// Token: 0x02000787 RID: 1927
		private class PopUpIconReserveData
		{
			// Token: 0x04002E1A RID: 11802
			public SkillActionEffectSet.IconData m_Data;

			// Token: 0x04002E1B RID: 11803
			public Transform m_TargetTransform;

			// Token: 0x04002E1C RID: 11804
			public float m_OffsetYFromTargetTransform;

			// Token: 0x04002E1D RID: 11805
			public float m_PivotX;

			// Token: 0x04002E1E RID: 11806
			public Vector2 m_OffsetUISpace;

			// Token: 0x04002E1F RID: 11807
			public float m_Delay;

			// Token: 0x04002E20 RID: 11808
			public bool m_IconSpace;
		}
	}
}
