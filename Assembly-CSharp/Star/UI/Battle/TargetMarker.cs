﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000791 RID: 1937
	public class TargetMarker : MonoBehaviour
	{
		// Token: 0x06002778 RID: 10104 RVA: 0x000D2F04 File Offset: 0x000D1304
		public void Setup(BattleUI owner)
		{
			this.m_Owner = owner;
			this.m_GameObject = base.gameObject;
			this.m_Transform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_Images = this.m_TargetParent.GetComponentsInChildren<Image>();
		}

		// Token: 0x06002779 RID: 10105 RVA: 0x000D2F3B File Offset: 0x000D133B
		private void Update()
		{
			if (this.IsEnableRender())
			{
				this.UpdatePosition();
			}
		}

		// Token: 0x0600277A RID: 10106 RVA: 0x000D2F50 File Offset: 0x000D1350
		public void SetMode(TargetMarker.eMode mode)
		{
			for (int i = 0; i < this.m_Animations.Length; i++)
			{
				this.m_Animations[i].Play();
			}
			if (mode != TargetMarker.eMode.Normal)
			{
				if (mode != TargetMarker.eMode.Weak)
				{
					if (mode == TargetMarker.eMode.Resist)
					{
						this.m_WeakLabelImage.enabled = false;
						this.m_ResistLabelImage.enabled = true;
						for (int j = 0; j < this.m_Images.Length; j++)
						{
							this.m_Images[j].sprite = this.m_ResistSprite;
						}
					}
				}
				else
				{
					this.m_WeakLabelImage.enabled = true;
					this.m_ResistLabelImage.enabled = false;
					for (int k = 0; k < this.m_Images.Length; k++)
					{
						this.m_Images[k].sprite = this.m_WeakSprite;
					}
				}
			}
			else
			{
				this.m_WeakLabelImage.enabled = false;
				this.m_ResistLabelImage.enabled = false;
				for (int l = 0; l < this.m_Images.Length; l++)
				{
					this.m_Images[l].sprite = this.m_NormalSprite;
				}
			}
		}

		// Token: 0x0600277B RID: 10107 RVA: 0x000D3078 File Offset: 0x000D1478
		public void SetTransform(Transform refTransform, Vector3 offsetFromRefTransform)
		{
			this.m_RefTransform = refTransform;
			this.m_OffsetFromRefTransform = offsetFromRefTransform;
			this.UpdatePosition();
		}

		// Token: 0x0600277C RID: 10108 RVA: 0x000D3090 File Offset: 0x000D1490
		private void UpdatePosition()
		{
			if (this.m_Owner != null && this.m_RefTransform != null)
			{
				this.m_Transform.localPosition = this.m_Owner.WorldToUIPosition(this.m_RefTransform.position + this.m_OffsetFromRefTransform);
			}
		}

		// Token: 0x0600277D RID: 10109 RVA: 0x000D30F0 File Offset: 0x000D14F0
		public void SetEnableRender(bool flg)
		{
			if (this.m_GameObject != null)
			{
				this.m_GameObject.SetActive(flg);
			}
			else
			{
				base.gameObject.SetActive(flg);
			}
		}

		// Token: 0x0600277E RID: 10110 RVA: 0x000D3120 File Offset: 0x000D1520
		public bool IsEnableRender()
		{
			if (this.m_GameObject != null)
			{
				return this.m_GameObject.activeSelf;
			}
			return base.gameObject.activeSelf;
		}

		// Token: 0x04002E67 RID: 11879
		[SerializeField]
		private Animation[] m_Animations;

		// Token: 0x04002E68 RID: 11880
		private Image[] m_Images;

		// Token: 0x04002E69 RID: 11881
		[SerializeField]
		private GameObject m_TargetParent;

		// Token: 0x04002E6A RID: 11882
		[SerializeField]
		private Sprite m_NormalSprite;

		// Token: 0x04002E6B RID: 11883
		[SerializeField]
		private Sprite m_WeakSprite;

		// Token: 0x04002E6C RID: 11884
		[SerializeField]
		private Sprite m_ResistSprite;

		// Token: 0x04002E6D RID: 11885
		[SerializeField]
		private Image m_WeakLabelImage;

		// Token: 0x04002E6E RID: 11886
		[SerializeField]
		private Image m_ResistLabelImage;

		// Token: 0x04002E6F RID: 11887
		private BattleUI m_Owner;

		// Token: 0x04002E70 RID: 11888
		private Transform m_RefTransform;

		// Token: 0x04002E71 RID: 11889
		private Vector3 m_OffsetFromRefTransform;

		// Token: 0x04002E72 RID: 11890
		private GameObject m_GameObject;

		// Token: 0x04002E73 RID: 11891
		private RectTransform m_Transform;

		// Token: 0x02000792 RID: 1938
		public enum eMode
		{
			// Token: 0x04002E75 RID: 11893
			Normal,
			// Token: 0x04002E76 RID: 11894
			Weak,
			// Token: 0x04002E77 RID: 11895
			Resist
		}
	}
}
