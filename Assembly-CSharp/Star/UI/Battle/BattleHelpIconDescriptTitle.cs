﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000753 RID: 1875
	public class BattleHelpIconDescriptTitle : MonoBehaviour
	{
		// Token: 0x06002549 RID: 9545 RVA: 0x000C7809 File Offset: 0x000C5C09
		public void Apply(string text)
		{
			this.m_Text.text = text;
		}

		// Token: 0x04002BF6 RID: 11254
		[SerializeField]
		private Text m_Text;
	}
}
