﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000773 RID: 1907
	public class DamageValue : MonoBehaviour
	{
		// Token: 0x17000291 RID: 657
		// (get) Token: 0x060026D0 RID: 9936 RVA: 0x000CE939 File Offset: 0x000CCD39
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x060026D1 RID: 9937 RVA: 0x000CE941 File Offset: 0x000CCD41
		public Vector3 TargetPosition
		{
			get
			{
				return this.m_TargetPosition;
			}
		}

		// Token: 0x17000293 RID: 659
		// (get) Token: 0x060026D2 RID: 9938 RVA: 0x000CE949 File Offset: 0x000CCD49
		public float TimeCount
		{
			get
			{
				return this.m_TimeCount;
			}
		}

		// Token: 0x060026D3 RID: 9939 RVA: 0x000CE951 File Offset: 0x000CCD51
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_Numbers = this.m_NumbersParent.GetComponentsInChildren<ImageNumber>();
			this.m_GameObject.SetActive(false);
		}

		// Token: 0x060026D4 RID: 9940 RVA: 0x000CE990 File Offset: 0x000CCD90
		public void Display(int value, bool isCritical, int registHit_Or_DefaultHit_Or_WeakHit, bool isUnhappy, bool isPoison, Transform targetTransform, float offsetY)
		{
			this.m_GameObject.SetActive(true);
			this.m_NumbersParent.SetValue(Mathf.Abs(value));
			Color color = DamageValue.m_NormalColor;
			float scale = 1f;
			if (isUnhappy)
			{
				color = DamageValue.m_UnhappyColor;
				scale = 1f;
				this.m_Descript.Apply(DamageDescript.eType.Unhappy);
				this.m_FluctuationDescript.Apply(DamageDescript.eType.None);
			}
			else if (value > 0)
			{
				color = DamageValue.m_RecoveryColor;
				scale = 1.1f;
				this.m_Descript.Apply(DamageDescript.eType.Recovery);
				this.m_FluctuationDescript.Apply(DamageDescript.eType.None);
			}
			else
			{
				if (registHit_Or_DefaultHit_Or_WeakHit == 1)
				{
					color = DamageValue.m_WeakColor;
					scale = 1.1f;
					this.m_FluctuationDescript.Apply(DamageDescript.eType.Weak);
				}
				else if (registHit_Or_DefaultHit_Or_WeakHit == -1)
				{
					color = DamageValue.m_ResistColor;
					scale = 1f;
					this.m_FluctuationDescript.Apply(DamageDescript.eType.Resist);
				}
				else
				{
					this.m_FluctuationDescript.Apply(DamageDescript.eType.None);
				}
				if (isCritical)
				{
					color = DamageValue.m_CriticalColor;
					scale = 1.1f;
					this.m_Descript.Apply(DamageDescript.eType.Critical);
				}
				else
				{
					this.m_Descript.Apply(DamageDescript.eType.None);
				}
				if (isPoison)
				{
					this.m_Descript.Apply(DamageDescript.eType.Poison);
				}
			}
			if (isUnhappy)
			{
				for (int i = 0; i < this.m_Numbers.Length; i++)
				{
					this.m_Numbers[i].GetComponent<Image>().material = null;
				}
			}
			else
			{
				for (int j = 0; j < this.m_Numbers.Length; j++)
				{
					this.m_Numbers[j].GetComponent<Image>().material = this.m_Material;
				}
			}
			if (isCritical)
			{
				this.m_Anim.Play("DamageValueCritical");
			}
			else
			{
				this.m_Anim.Play("DamageValue");
			}
			this.m_NumbersParent.SetColor(color);
			this.m_NumbersParent.SetScale(scale);
			this.m_TargetPosition = targetTransform.position;
			this.m_Offset = new Vector3(0f, offsetY, 0f);
			this.m_ParentRectTransform = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, this.m_TargetPosition + this.m_Offset);
			Vector2 v;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_ParentRectTransform, screenPoint, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out v);
			this.m_RectTransform.localPosition = v;
			this.m_RectTransform.localScale = Vector3.one;
			this.m_TimeCount = 0f;
		}

		// Token: 0x060026D5 RID: 9941 RVA: 0x000CEC08 File Offset: 0x000CD008
		private void Update()
		{
			Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, this.m_TargetPosition + this.m_Offset);
			Vector2 v;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(this.m_ParentRectTransform, screenPoint, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out v);
			this.m_RectTransform.localPosition = v;
			this.m_TimeCount += Time.deltaTime;
		}

		// Token: 0x060026D6 RID: 9942 RVA: 0x000CEC6D File Offset: 0x000CD06D
		public void SetActive(bool flag)
		{
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x060026D7 RID: 9943 RVA: 0x000CEC7B File Offset: 0x000CD07B
		public bool IsComplete()
		{
			return !this.m_Anim.isPlaying;
		}

		// Token: 0x04002D6B RID: 11627
		[SerializeField]
		public static Color m_NormalColor = new Color(0f, 0f, 0f, 1f);

		// Token: 0x04002D6C RID: 11628
		[SerializeField]
		public static Color m_CriticalColor = new Color(0.8117647f, 0.6509804f, 0.27450982f, 1f);

		// Token: 0x04002D6D RID: 11629
		[SerializeField]
		public static Color m_WeakColor = new Color(0.72156864f, 0f, 0f, 1f);

		// Token: 0x04002D6E RID: 11630
		[SerializeField]
		public static Color m_ResistColor = new Color(0f, 0.16862746f, 0.7137255f, 1f);

		// Token: 0x04002D6F RID: 11631
		[SerializeField]
		public static Color m_RecoveryColor = new Color(0.13725491f, 0.6627451f, 0.06666667f, 1f);

		// Token: 0x04002D70 RID: 11632
		[SerializeField]
		public static Color m_UnhappyColor = new Color(0.5529412f, 0.25882354f, 1f, 1f);

		// Token: 0x04002D71 RID: 11633
		[SerializeField]
		private ImageNumbers m_NumbersParent;

		// Token: 0x04002D72 RID: 11634
		private ImageNumber[] m_Numbers;

		// Token: 0x04002D73 RID: 11635
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04002D74 RID: 11636
		[SerializeField]
		private Material m_Material;

		// Token: 0x04002D75 RID: 11637
		[SerializeField]
		private DamageDescript m_Descript;

		// Token: 0x04002D76 RID: 11638
		[SerializeField]
		private DamageDescript m_FluctuationDescript;

		// Token: 0x04002D77 RID: 11639
		private float m_TimeCount;

		// Token: 0x04002D78 RID: 11640
		private Vector3 m_Offset;

		// Token: 0x04002D79 RID: 11641
		private GameObject m_GameObject;

		// Token: 0x04002D7A RID: 11642
		private RectTransform m_RectTransform;

		// Token: 0x04002D7B RID: 11643
		private RectTransform m_ParentRectTransform;

		// Token: 0x04002D7C RID: 11644
		private Vector3 m_TargetPosition;
	}
}
