﻿using System;
using Star.UI.Menu;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x020007A0 RID: 1952
	public class BattleFriendPropose : UIGroup
	{
		// Token: 0x060027DC RID: 10204 RVA: 0x000D5480 File Offset: 0x000D3880
		public void Open(FriendManager.FriendData friendData)
		{
			base.Open();
			this.m_FriendData = friendData;
			IFriendUIData itemData = new IFriendUIData(this.m_FriendData, null);
			this.m_Item.SetItemData(itemData);
		}

		// Token: 0x060027DD RID: 10205 RVA: 0x000D54B3 File Offset: 0x000D38B3
		public void OnClickYesCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Propose(this.m_FriendData.m_ID, new Action<bool>(this.OnResponse));
		}

		// Token: 0x060027DE RID: 10206 RVA: 0x000D54DC File Offset: 0x000D38DC
		public void OnResponse(bool isErr)
		{
			this.Close();
		}

		// Token: 0x04002EFE RID: 12030
		[SerializeField]
		private MenuFriendListItem m_Item;

		// Token: 0x04002EFF RID: 12031
		private FriendManager.FriendData m_FriendData;
	}
}
