﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000778 RID: 1912
	public class InterruptFriendJoinSelect : MonoBehaviour
	{
		// Token: 0x060026E8 RID: 9960 RVA: 0x000CF338 File Offset: 0x000CD738
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
			this.m_GameObject.SetActive(false);
			this.m_StateController.Setup(4, 0, false);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 2, true);
			this.m_StateController.AddTransit(2, 3, true);
			this.m_StateController.AddTransit(3, 0, true);
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x000CF3AF File Offset: 0x000CD7AF
		public void Destroy()
		{
		}

		// Token: 0x060026EA RID: 9962 RVA: 0x000CF3B1 File Offset: 0x000CD7B1
		public void PlayIn()
		{
			this.SetState(InterruptFriendJoinSelect.eState.In);
			this.m_MainGroup.Open();
		}

		// Token: 0x060026EB RID: 9963 RVA: 0x000CF3C6 File Offset: 0x000CD7C6
		public void PlayOut()
		{
			if (this.SetState(InterruptFriendJoinSelect.eState.Out))
			{
				this.m_MainGroup.Close();
			}
		}

		// Token: 0x060026EC RID: 9964 RVA: 0x000CF3DF File Offset: 0x000CD7DF
		public bool IsAnimPlaying()
		{
			return this.m_StateController.IsPlayingInOut();
		}

		// Token: 0x060026ED RID: 9965 RVA: 0x000CF3F4 File Offset: 0x000CD7F4
		private void Update()
		{
			switch (this.m_StateController.NowState)
			{
			case 1:
			{
				bool flag = true;
				if (this.m_StateController.IsPlayingInOut())
				{
					flag = false;
				}
				if (flag)
				{
					this.SetState(InterruptFriendJoinSelect.eState.Wait);
					this.m_MainGroup.SetEnableInput(true);
				}
				break;
			}
			case 2:
				this.UpdateAndroidBackKey();
				break;
			case 3:
			{
				bool flag2 = true;
				if (this.m_StateController.IsPlayingInOut())
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.SetState(InterruptFriendJoinSelect.eState.Hide);
					if (this.m_SelectButton == InterruptFriendJoinSelect.eButton.Decide)
					{
						this.m_UI.DecideInterruptFriendJoinSelect();
					}
					else
					{
						this.m_UI.CancelInterruptFriendJoinSelect();
					}
				}
				break;
			}
			}
		}

		// Token: 0x060026EE RID: 9966 RVA: 0x000CF4BC File Offset: 0x000CD8BC
		private bool SetState(InterruptFriendJoinSelect.eState state)
		{
			if (this.m_StateController.ChangeState((int)state))
			{
				switch (state)
				{
				case InterruptFriendJoinSelect.eState.Hide:
					this.m_GameObject.SetActive(false);
					this.SetEnableInput(false);
					break;
				case InterruptFriendJoinSelect.eState.In:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(false);
					break;
				case InterruptFriendJoinSelect.eState.Wait:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(true);
					this.m_MainGroup.SetEnableInput(true);
					break;
				case InterruptFriendJoinSelect.eState.Out:
					this.m_GameObject.SetActive(true);
					this.SetEnableInput(false);
					break;
				}
				return true;
			}
			return false;
		}

		// Token: 0x060026EF RID: 9967 RVA: 0x000CF564 File Offset: 0x000CD964
		public void OnClickDecideButtonCallBack()
		{
			if (this.m_StateController.NowState != 2)
			{
				return;
			}
			if (this.m_UI.JudgeInterruptFriendJoinSelect())
			{
				this.m_SelectButton = InterruptFriendJoinSelect.eButton.Decide;
				this.PlayOut();
			}
		}

		// Token: 0x060026F0 RID: 9968 RVA: 0x000CF595 File Offset: 0x000CD995
		public void OnClickBackButtonCallBack()
		{
			if (this.m_StateController.NowState != 2)
			{
				return;
			}
			this.m_SelectButton = InterruptFriendJoinSelect.eButton.Cancel;
			this.PlayOut();
		}

		// Token: 0x060026F1 RID: 9969 RVA: 0x000CF5B6 File Offset: 0x000CD9B6
		public void SetEnableInput(bool flg)
		{
			this.m_CanvasGroup.blocksRaycasts = flg;
		}

		// Token: 0x060026F2 RID: 9970 RVA: 0x000CF5C4 File Offset: 0x000CD9C4
		public void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BackButton, null);
		}

		// Token: 0x04002D97 RID: 11671
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04002D98 RID: 11672
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04002D99 RID: 11673
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04002D9A RID: 11674
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04002D9B RID: 11675
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x04002D9C RID: 11676
		private GameObject m_GameObject;

		// Token: 0x04002D9D RID: 11677
		private InterruptFriendJoinSelect.eButton m_SelectButton;

		// Token: 0x02000779 RID: 1913
		private enum eState
		{
			// Token: 0x04002D9F RID: 11679
			Hide,
			// Token: 0x04002DA0 RID: 11680
			In,
			// Token: 0x04002DA1 RID: 11681
			Wait,
			// Token: 0x04002DA2 RID: 11682
			Out,
			// Token: 0x04002DA3 RID: 11683
			Num
		}

		// Token: 0x0200077A RID: 1914
		private enum eButton
		{
			// Token: 0x04002DA5 RID: 11685
			None,
			// Token: 0x04002DA6 RID: 11686
			Decide,
			// Token: 0x04002DA7 RID: 11687
			Cancel
		}
	}
}
