﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007A8 RID: 1960
	public class ResultItemDetail : UIGroup
	{
		// Token: 0x0600280B RID: 10251 RVA: 0x000D6B78 File Offset: 0x000D4F78
		public void Setup()
		{
			this.m_Icon = UnityEngine.Object.Instantiate<ResultItemData>(this.m_IconPrefab, this.m_IconParent, false);
			RectTransform component = this.m_Icon.GetComponent<RectTransform>();
			component.localPosition = Vector3.zero;
		}

		// Token: 0x0600280C RID: 10252 RVA: 0x000D6BB4 File Offset: 0x000D4FB4
		public void Apply(int itemID)
		{
			ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID);
			this.m_Icon.Apply(itemID, -1);
			this.m_NameTextObj.text = param.m_Name;
		}

		// Token: 0x04002F53 RID: 12115
		[SerializeField]
		private RectTransform m_IconParent;

		// Token: 0x04002F54 RID: 12116
		[SerializeField]
		private ResultItemData m_IconPrefab;

		// Token: 0x04002F55 RID: 12117
		private ResultItemData m_Icon;

		// Token: 0x04002F56 RID: 12118
		[SerializeField]
		private Text m_NameTextObj;
	}
}
