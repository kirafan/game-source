﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007A1 RID: 1953
	public class BattleResultUI : MenuUIBase
	{
		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x060027E0 RID: 10208 RVA: 0x000D54EC File Offset: 0x000D38EC
		public StoreReviewController StoreReviewController
		{
			get
			{
				return this.m_StoreReviewController;
			}
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x060027E1 RID: 10209 RVA: 0x000D54F4 File Offset: 0x000D38F4
		public BattleFriendPropose ProposeGroup
		{
			get
			{
				return this.m_ProposeGroup;
			}
		}

		// Token: 0x060027E2 RID: 10210 RVA: 0x000D54FC File Offset: 0x000D38FC
		public void Setup(BattleResult result, Camera renderCamera, float planeDistance, BattleState_Result stateResult)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_MasterIllust.ApplyCurrentEquip();
			this.m_Camera = renderCamera;
			this.m_CameraTransform = renderCamera.GetComponent<Transform>();
			this.m_Canvas = base.GetComponent<Canvas>();
			this.m_Canvas.worldCamera = this.m_Camera;
			this.m_Canvas.planeDistance = planeDistance;
			this.m_BattleResult = result;
			this.m_StateResult = stateResult;
			this.m_PlayerRewardUI.Prepare();
			this.m_PlayerRewardUI.SetRank(this.m_BattleResult.m_ClearRank, this.m_BattleResult.m_ContinueCount, this.m_BattleResult.m_DeadCount);
			long currentExp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterRankDB.GetCurrentExp(this.m_BattleResult.m_BeforeUserLv, this.m_BattleResult.m_BeforeUserExp);
			long currentExp2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterRankDB.GetCurrentExp(this.m_BattleResult.m_AfterUserLv, this.m_BattleResult.m_AfterUserExp);
			this.m_PlayerRewardUI.SetExpData(this.m_BattleResult.m_RewardUserExp, this.m_BattleResult.m_BeforeUserLv, currentExp, this.m_BattleResult.m_AfterUserLv, currentExp2, UIUtility.GetMasterMaxLv(), this.m_BattleResult.m_UserNextExps);
			UserMasterOrbData equipUserMasterOrbData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetEquipUserMasterOrbData();
			if (equipUserMasterOrbData != null)
			{
				long currentExp3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbExpDB.GetCurrentExp(this.m_BattleResult.m_BeforeMasterOrbLv, this.m_BattleResult.m_BeforeMasterOrbExp, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(equipUserMasterOrbData.OrbID).m_ExpTableID);
				long currentExp4 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbExpDB.GetCurrentExp(this.m_BattleResult.m_AfterMasterOrbLv, this.m_BattleResult.m_AfterMasterOrbExp, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(equipUserMasterOrbData.OrbID).m_ExpTableID);
				this.m_PlayerRewardUI.SetWeaponExpData(this.m_BattleResult.m_RewardMasterOrbExp, this.m_BattleResult.m_BeforeMasterOrbLv, currentExp3, this.m_BattleResult.m_AfterMasterOrbLv, currentExp4, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbListDB.GetParam(equipUserMasterOrbData.OrbID).m_LimitLv, this.m_BattleResult.m_MasterOrbNextExps);
			}
			else
			{
				this.m_PlayerRewardUI.SetWeaponExpDataEmpty(this.m_BattleResult.m_RewardMasterOrbExp);
			}
			this.m_CharaRewardUI.Setup();
			this.m_CharaRewardUI.SetRewardExp(this.m_BattleResult.m_RewardCharaExp);
			this.m_CharaRewardUI.SetRewardFriendshipExp(this.m_BattleResult.m_RewardFriendshipExp);
			for (int i = 0; i < this.m_BattleResult.m_CharaDatas.Length; i++)
			{
				BattleResult.CharaData charaData = this.m_BattleResult.m_CharaDatas[i];
				if (charaData.m_IsAvailable)
				{
					if (charaData.m_MngID != -1L)
					{
						UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
						UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaData.m_MngID);
						int charaID = userCharaData.Param.CharaID;
						this.m_CharaRewardUI.SetCharaData(i, userCharaData.Param.LimitBreak);
						this.m_CharaRewardUI.SetCharaExpData(i, charaData.m_AddExp, charaData.m_BeforeLv, EditUtility.GetCharaNowExp(charaData.m_BeforeLv, charaData.m_BeforeExp), charaData.m_AfterLv, EditUtility.GetCharaNowExp(charaData.m_AfterLv, charaData.m_AfterExp), userCharaData.Param.MaxLv, charaData.m_NextExps);
						sbyte friendshipTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(userCharaData.Param.NamedType).m_FriendshipTableID;
						this.m_CharaRewardUI.SetCharaFriendshipData(i, charaData.m_BeforeFriendship, charaData.m_BeforeFriendshipExp, charaData.m_AfterFriendship, charaData.m_AfterFriendshipExp, friendshipTableID);
						if (charaData.m_AfterFriendship > charaData.m_BeforeFriendship)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.CharaUnlockWindow.AddPopParam(userCharaData.Param.CharaID, charaData.m_BeforeFriendship, charaData.m_AfterFriendship);
						}
					}
				}
			}
			this.m_ItemRewardUI.Prepare();
			for (int j = 0; j < this.m_BattleResult.m_DropItems.Count; j++)
			{
				for (int k = 0; k < this.m_BattleResult.m_DropItems[j].GetDropDataNum(); k++)
				{
					BattleDropItem.DropData dropDataAt = this.m_BattleResult.m_DropItems[j].GetDropDataAt(k);
					this.m_ItemRewardUI.AddItemData(dropDataAt.ID, dropDataAt.Num);
				}
			}
			this.m_ItemRewardUI.SetGoldData(this.m_BattleResult.m_RewardGold, this.m_BattleResult.m_AfterGold);
			this.m_TouchNext.enabled = false;
		}

		// Token: 0x060027E3 RID: 10211 RVA: 0x000D5A04 File Offset: 0x000D3E04
		private void Update()
		{
			switch (this.m_State)
			{
			case 1:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopBattle, true);
					this.m_TouchNext.enabled = true;
					if (this.m_StateResult != null)
					{
						this.m_StateResult.HideCharactersForResult();
					}
					this.SetState(2);
				}
				break;
			}
			case 2:
			{
				BattleResultUI.eSequence sequence = this.m_Sequence;
				if (sequence != BattleResultUI.eSequence.PlayerReward)
				{
					if (sequence != BattleResultUI.eSequence.CharaReward)
					{
						if (sequence != BattleResultUI.eSequence.ItemReward)
						{
						}
					}
					else
					{
						if (!this.m_IsShowChara && this.m_CharaRewardUI.IsIdle() && this.m_StateResult != null)
						{
							this.m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
							this.m_StateResult.ShowCharactersForResult();
							this.SetCameraPosition(this.m_CharaRewardUI.GetCharaPosition());
							this.m_IsShowChara = true;
						}
						if (!this.m_CharaRewardUI.IsOpenOrIn())
						{
							this.m_StateResult.HideCharactersForResult();
							this.m_Sequence = BattleResultUI.eSequence.ItemReward;
							this.m_ItemRewardUI.Open();
							this.m_Button.gameObject.SetActive(false);
							MissionManager.SetPopUpActive(eXlsPopupTiming.PopChara, true);
						}
					}
				}
				else if (!this.m_PlayerRewardUI.IsOpenOrIn())
				{
					this.m_Sequence = BattleResultUI.eSequence.CharaReward;
					this.m_CharaRewardUI.Open();
					this.m_TouchNext.enabled = true;
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopPlayer, true);
				}
				break;
			}
			case 3:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopBattle, false);
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopChara, false);
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopPlayer, false);
					this.SetState(4);
				}
				break;
			}
			}
		}

		// Token: 0x060027E4 RID: 10212 RVA: 0x000D5C18 File Offset: 0x000D4018
		private void LateUpdate()
		{
			BattleResultUI.eSequence sequence = this.m_Sequence;
			if (sequence == BattleResultUI.eSequence.CharaReward)
			{
				if (this.m_CharaRewardUI.IsOpenOrIn())
				{
					this.SetCameraPosition(this.m_CharaRewardUI.GetCharaPosition());
				}
			}
		}

		// Token: 0x060027E5 RID: 10213 RVA: 0x000D5C60 File Offset: 0x000D4060
		private void SetState(int state)
		{
			this.m_State = state;
			switch (state)
			{
			case 0:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case 1:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case 2:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case 3:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case 4:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060027E6 RID: 10214 RVA: 0x000D5D00 File Offset: 0x000D4100
		public override void PlayIn()
		{
			this.SetState(1);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_PlayerRewardUI.Open();
			this.m_MasterIllust.GetComponent<AnimUIPlayer>().PlayIn();
		}

		// Token: 0x060027E7 RID: 10215 RVA: 0x000D5D58 File Offset: 0x000D4158
		public override void PlayOut()
		{
			this.m_TouchNext.enabled = false;
			this.SetState(3);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MasterIllust.Destroy();
		}

		// Token: 0x060027E8 RID: 10216 RVA: 0x000D5DA9 File Offset: 0x000D41A9
		public override bool IsMenuEnd()
		{
			return this.m_State == 4;
		}

		// Token: 0x060027E9 RID: 10217 RVA: 0x000D5DB4 File Offset: 0x000D41B4
		public void OnTapNext()
		{
			BattleResultUI.eSequence sequence = this.m_Sequence;
			if (sequence != BattleResultUI.eSequence.PlayerReward)
			{
				if (sequence != BattleResultUI.eSequence.CharaReward)
				{
					if (sequence != BattleResultUI.eSequence.ItemReward)
					{
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_DECISION, 1f, 0, -1, -1);
					if (this.m_CharaRewardUI.IsIdle())
					{
						this.m_CharaRewardUI.Tap();
					}
				}
			}
			else if (this.m_PlayerRewardUI.IsOpen())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_DECISION, 1f, 0, -1, -1);
				this.m_PlayerRewardUI.Tap();
				if (this.m_PlayerRewardUI.IsHideOrOut())
				{
					this.m_MasterIllust.GetComponent<AnimUIPlayer>().PlayOut();
				}
			}
		}

		// Token: 0x060027EA RID: 10218 RVA: 0x000D5E80 File Offset: 0x000D4280
		public void OnClickNextButtonCallBack()
		{
			BattleResultUI.eSequence sequence = this.m_Sequence;
			if (sequence == BattleResultUI.eSequence.ItemReward)
			{
				this.m_Sequence = BattleResultUI.eSequence.End;
				this.m_ItemRewardUI.Close();
				this.PlayOut();
			}
		}

		// Token: 0x060027EB RID: 10219 RVA: 0x000D5EBD File Offset: 0x000D42BD
		public void OpenItemDetail(int itemID)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ItemDetailWindow.OpenItem(itemID);
		}

		// Token: 0x060027EC RID: 10220 RVA: 0x000D5ED0 File Offset: 0x000D42D0
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			RectTransform component = this.m_Canvas.GetComponent<RectTransform>();
			pos.x = pos.x / component.sizeDelta.y * this.m_Camera.orthographicSize * 2f;
			pos.y = pos.y / component.sizeDelta.y * this.m_Camera.orthographicSize * 2f;
			return pos;
		}

		// Token: 0x060027ED RID: 10221 RVA: 0x000D5F50 File Offset: 0x000D4350
		public void SetCameraPosition(Vector2 pos)
		{
			Vector2 pos2 = pos;
			pos2.x = -pos2.x;
			pos2.y = -pos2.y;
			Vector3 vector = this.ConvertPositionTo3D(pos2);
			this.m_CameraTransform.localPosition = new Vector3(vector.x, vector.y, this.m_CameraTransform.localPosition.z);
		}

		// Token: 0x04002F00 RID: 12032
		public const int STATE_HIDE = 0;

		// Token: 0x04002F01 RID: 12033
		public const int STATE_IN = 1;

		// Token: 0x04002F02 RID: 12034
		public const int STATE_WAIT = 2;

		// Token: 0x04002F03 RID: 12035
		public const int STATE_OUT = 3;

		// Token: 0x04002F04 RID: 12036
		public const int STATE_END = 4;

		// Token: 0x04002F05 RID: 12037
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04002F06 RID: 12038
		[SerializeField]
		private ResultPlayerReward m_PlayerRewardUI;

		// Token: 0x04002F07 RID: 12039
		[SerializeField]
		private ResultCharaReward m_CharaRewardUI;

		// Token: 0x04002F08 RID: 12040
		[SerializeField]
		private ResultItemReward m_ItemRewardUI;

		// Token: 0x04002F09 RID: 12041
		[SerializeField]
		private Image m_TouchNext;

		// Token: 0x04002F0A RID: 12042
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04002F0B RID: 12043
		[SerializeField]
		private StoreReviewController m_StoreReviewController;

		// Token: 0x04002F0C RID: 12044
		[SerializeField]
		private BattleFriendPropose m_ProposeGroup;

		// Token: 0x04002F0D RID: 12045
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x04002F0E RID: 12046
		private BattleResultUI.eSequence m_Sequence;

		// Token: 0x04002F0F RID: 12047
		private BattleResult m_BattleResult;

		// Token: 0x04002F10 RID: 12048
		private BattleState_Result m_StateResult;

		// Token: 0x04002F11 RID: 12049
		private Camera m_Camera;

		// Token: 0x04002F12 RID: 12050
		private Transform m_CameraTransform;

		// Token: 0x04002F13 RID: 12051
		private Canvas m_Canvas;

		// Token: 0x04002F14 RID: 12052
		private bool m_IsShowChara;

		// Token: 0x020007A2 RID: 1954
		private enum eSequence
		{
			// Token: 0x04002F16 RID: 12054
			PlayerReward,
			// Token: 0x04002F17 RID: 12055
			CharaReward,
			// Token: 0x04002F18 RID: 12056
			ItemReward,
			// Token: 0x04002F19 RID: 12057
			End
		}
	}
}
