﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007AB RID: 1963
	public class ResultPlayerReward : UIGroup
	{
		// Token: 0x06002813 RID: 10259 RVA: 0x000D6D64 File Offset: 0x000D5164
		public void Prepare()
		{
			this.m_ExpGauge.Setup();
			this.m_WeaponExpGauge.Setup();
			for (int i = 0; i < this.m_RankStarObjArray.Length; i++)
			{
				this.m_RankStarObjArray[i].SetActive(false);
				this.m_RankStarObjAnimArray[i].gameObject.SetActive(false);
			}
		}

		// Token: 0x06002814 RID: 10260 RVA: 0x000D6DC4 File Offset: 0x000D51C4
		public override void Update()
		{
			base.Update();
			if (!base.IsIdle())
			{
				return;
			}
			if (this.m_Time > 1f)
			{
				if (!this.m_StartExp)
				{
					this.m_ExpGauge.Play(false);
					this.m_WeaponExpGauge.Play(false);
				}
				this.m_StartExp = true;
			}
			this.m_Time += Time.deltaTime;
			if (!this.m_StartSE && (this.m_ExpGauge.IsPlaying || this.m_WeaponExpGauge.IsPlaying))
			{
				if (this.m_SoundHandler == null)
				{
					this.m_SoundHandler = new SoundHandler();
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_EXP_GAUGE, this.m_SoundHandler, 1f, 0, -1, -1);
				}
				this.m_StartSE = true;
			}
			else if (!this.m_ExpGauge.IsPlaying && !this.m_WeaponExpGauge.IsPlaying && this.m_SoundHandler != null)
			{
				this.m_SoundHandler.Stop();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHandler, true);
				this.m_SoundHandler = null;
			}
			if (this.m_Time > 0f)
			{
				int num = (int)(this.m_Time / 0.1f);
				for (int i = 0; i < this.m_RankStarObjArray.Length; i++)
				{
					if (i < this.m_Rank && i < num && !this.m_RankStarObjAnimArray[i].gameObject.activeSelf)
					{
						this.m_RankStarObjAnimArray[i].gameObject.SetActive(true);
						this.m_RankStarObjAnimArray[i].Play();
					}
				}
			}
			int num2 = (int)(this.m_Time / 0.1f + 0.2f);
			for (int j = this.m_SoundRankIdx; j < this.m_RankStarObjArray.Length; j++)
			{
				if (this.m_RankStarObjArray[j].activeSelf && j < num2)
				{
					this.m_SoundRankIdx = j + 1;
					if (j != 0)
					{
						if (j != 1)
						{
							if (j == 2)
							{
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_RESULT_ACHIEVE_3, 1f, 0, -1, -1);
								this.m_CompleteAnim.PlayIn();
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_RECAST_COMPLETE, 1f, 0, -1, -1);
							}
						}
						else
						{
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_RESULT_ACHIEVE_2, 1f, 0, -1, -1);
						}
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_RESULT_ACHIEVE_1, 1f, 0, -1, -1);
					}
				}
			}
		}

		// Token: 0x06002815 RID: 10261 RVA: 0x000D7070 File Offset: 0x000D5470
		public void SetRank(QuestDefine.eClearRank clearRank, int continueCount, int deadCount)
		{
			this.m_Rank = (int)clearRank;
			for (int i = 0; i < this.m_RankStarObjArray.Length; i++)
			{
				this.m_RankStarObjArray[i].SetActive(this.m_Rank > i);
				this.m_RankStarObjAnimArray[i].GetComponent<Image>().sprite = this.m_Crowns[this.m_Rank - 1];
			}
			bool flag = this.m_Rank == 3;
			if (continueCount > 0)
			{
				this.m_CompleteObj.SetActive(false);
				this.m_RankTextObj.gameObject.SetActive(true);
				this.m_RankTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleResultRetry, new object[]
				{
					continueCount
				});
			}
			else if (flag)
			{
				this.m_CompleteObj.SetActive(true);
				this.m_RankTextObj.gameObject.SetActive(true);
				this.m_RankTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleResultDead0);
			}
			else
			{
				this.m_CompleteObj.SetActive(false);
				this.m_RankTextObj.gameObject.SetActive(true);
				this.m_RankTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.BattleResultDead0 + Mathf.Min(deadCount, 3));
			}
		}

		// Token: 0x06002816 RID: 10262 RVA: 0x000D71C0 File Offset: 0x000D55C0
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			this.m_AddExpValue.text = "+" + rewardExp.ToString();
			this.m_ExpGauge.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
		}

		// Token: 0x06002817 RID: 10263 RVA: 0x000D71FA File Offset: 0x000D55FA
		public void SetWeaponExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			this.m_AddWeaponExpValue.text = "+" + rewardExp.ToString();
			this.m_WeaponExpGauge.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
		}

		// Token: 0x06002818 RID: 10264 RVA: 0x000D7234 File Offset: 0x000D5634
		public void SetWeaponExpDataEmpty(long rewardExp)
		{
			this.m_WeaponExpGauge.SetExpData(0, 0L, 0, 0L);
		}

		// Token: 0x06002819 RID: 10265 RVA: 0x000D7248 File Offset: 0x000D5648
		private void ForceUpdateExp()
		{
			this.m_ExpGauge.SkipUpdateExp();
			this.m_WeaponExpGauge.SkipUpdateExp();
			if (this.m_SoundHandler != null)
			{
				this.m_SoundHandler.Stop();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHandler, true);
				this.m_SoundHandler = null;
			}
		}

		// Token: 0x0600281A RID: 10266 RVA: 0x000D72A0 File Offset: 0x000D56A0
		public void Tap()
		{
			if (!this.m_ExpGauge.IsPlaying && !this.m_WeaponExpGauge.IsPlaying)
			{
				if (this.m_SoundHandler != null)
				{
					this.m_SoundHandler.Stop();
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHandler, true);
					this.m_SoundHandler = null;
				}
				this.Close();
			}
			else
			{
				this.ForceUpdateExp();
			}
		}

		// Token: 0x04002F61 RID: 12129
		private const float RESULTSTAR_STARTTIME = 0f;

		// Token: 0x04002F62 RID: 12130
		private const float RESULTSTAR_DURATION = 0.1f;

		// Token: 0x04002F63 RID: 12131
		private const float RESULTSTAR_DELAY = 0.2f;

		// Token: 0x04002F64 RID: 12132
		private const float EXPINCREASE_STARTTIME = 1f;

		// Token: 0x04002F65 RID: 12133
		private const int LEVELUP_POPUP_MAX = 2;

		// Token: 0x04002F66 RID: 12134
		[SerializeField]
		private Sprite[] m_Crowns;

		// Token: 0x04002F67 RID: 12135
		[SerializeField]
		private GameObject[] m_RankStarObjArray;

		// Token: 0x04002F68 RID: 12136
		[SerializeField]
		private Animation[] m_RankStarObjAnimArray;

		// Token: 0x04002F69 RID: 12137
		[SerializeField]
		private Text m_RankTextObj;

		// Token: 0x04002F6A RID: 12138
		[SerializeField]
		private GameObject m_CompleteObj;

		// Token: 0x04002F6B RID: 12139
		[SerializeField]
		private AnimUIPlayer m_CompleteAnim;

		// Token: 0x04002F6C RID: 12140
		[SerializeField]
		private Text m_AddExpValue;

		// Token: 0x04002F6D RID: 12141
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04002F6E RID: 12142
		[SerializeField]
		private Text m_AddWeaponExpValue;

		// Token: 0x04002F6F RID: 12143
		[SerializeField]
		private ExpGauge m_WeaponExpGauge;

		// Token: 0x04002F70 RID: 12144
		private int m_Rank;

		// Token: 0x04002F71 RID: 12145
		private bool m_IsUpdateGold;

		// Token: 0x04002F72 RID: 12146
		private int m_SoundRankIdx;

		// Token: 0x04002F73 RID: 12147
		private float m_Time;

		// Token: 0x04002F74 RID: 12148
		private bool m_StartExp;

		// Token: 0x04002F75 RID: 12149
		private bool m_StartSE;

		// Token: 0x04002F76 RID: 12150
		private SoundHandler m_SoundHandler;
	}
}
