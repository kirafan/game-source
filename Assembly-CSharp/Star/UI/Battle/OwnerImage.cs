﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000783 RID: 1923
	public class OwnerImage : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IEventSystemHandler
	{
		// Token: 0x17000296 RID: 662
		// (get) Token: 0x0600272B RID: 10027 RVA: 0x000D0A73 File Offset: 0x000CEE73
		public bool IsDisplay
		{
			get
			{
				return this.m_IsDisplay;
			}
		}

		// Token: 0x0600272C RID: 10028 RVA: 0x000D0A7C File Offset: 0x000CEE7C
		public void Setup()
		{
			this.m_IsDisplay = false;
			this.m_IsDrag = false;
			this.m_StartPosition = Vector3.zero;
			this.m_HidePosition = this.m_StartPosition + new Vector3(380f, 0f);
			this.m_Handlers = new OwnerImage.ImageHandler[this.m_Images.Length];
			for (int i = 0; i < this.m_Images.Length; i++)
			{
				this.m_Handlers[i] = new OwnerImage.ImageHandler();
				this.m_Handlers[i].m_Image = this.m_Images[i];
				this.m_Handlers[i].m_RectTransform = this.m_Images[i].GetComponent<RectTransform>();
				this.m_Handlers[i].m_IsMoving = false;
			}
		}

		// Token: 0x0600272D RID: 10029 RVA: 0x000D0B38 File Offset: 0x000CEF38
		public void Destroy()
		{
			for (int i = 0; i < this.m_Images.Length; i++)
			{
				this.m_Images[i].sprite = null;
			}
		}

		// Token: 0x0600272E RID: 10030 RVA: 0x000D0B6C File Offset: 0x000CEF6C
		private void Update()
		{
			if (this.m_IsEnableInput && this.m_IsDrag && InputTouch.Inst.GetAvailableTouchCnt() != 1)
			{
				this.m_IsDrag = false;
			}
			int i = 0;
			while (i < this.m_Images.Length)
			{
				Vector3 vector = this.m_HidePosition;
				if (i != this.m_CurrentIdx)
				{
					goto IL_17C;
				}
				if (this.m_IsEnableInput && this.m_IsDrag && InputTouch.Inst.GetAvailableTouchCnt() > 0)
				{
					InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
					if (info.m_DiffFromPrev.x > 0f)
					{
						this.m_Handlers[i].m_RectTransform.localPosition = this.m_Handlers[i].m_RectTransform.localPosition + new Vector3(info.m_DiffFromPrev.x * info.m_DiffFromPrev.z / (float)Screen.width * this.m_CanvasScaler.referenceResolution.x, 0f);
						this.m_Handlers[i].m_IsMoving = false;
						if (this.m_Handlers[i].m_RectTransform.localPosition.x > this.m_StartPosition.x + 32f)
						{
							this.m_UI.StartMemberChange();
							this.MoveTo(this.m_Handlers[i], this.m_HidePosition.x, 0.1f);
							this.m_IsDisplay = false;
						}
					}
				}
				else
				{
					if (this.m_IsDisplay)
					{
						vector = this.m_StartPosition;
						goto IL_17C;
					}
					goto IL_17C;
				}
				IL_1DA:
				i++;
				continue;
				IL_17C:
				if (!this.m_Handlers[i].m_IsMoving && Mathf.Abs(this.m_Handlers[i].m_RectTransform.localPosition.x - vector.x) > 1f)
				{
					this.MoveTo(this.m_Handlers[i], vector.x, 0.2f);
					goto IL_1DA;
				}
				goto IL_1DA;
			}
		}

		// Token: 0x0600272F RID: 10031 RVA: 0x000D0D68 File Offset: 0x000CF168
		private void MoveTo(OwnerImage.ImageHandler handler, float x, float time)
		{
			iTween.Stop(handler.m_RectTransform.gameObject);
			handler.m_IsMoving = true;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("islocal", true);
			hashtable.Add("x", x);
			hashtable.Add("y", handler.m_RectTransform.localPosition.y);
			hashtable.Add("time", time);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", 0f);
			hashtable.Add("oncomplete", "OnCompleteMove");
			hashtable.Add("oncompleteparams", handler);
			hashtable.Add("oncompletetarget", base.gameObject);
			iTween.MoveTo(handler.m_RectTransform.gameObject, hashtable);
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x000D0E4C File Offset: 0x000CF24C
		private void OnCompleteMove(OwnerImage.ImageHandler handler)
		{
			handler.m_IsMoving = false;
		}

		// Token: 0x06002731 RID: 10033 RVA: 0x000D0E55 File Offset: 0x000CF255
		public void Appear()
		{
			this.m_IsDisplay = true;
			this.m_IsDrag = false;
		}

		// Token: 0x06002732 RID: 10034 RVA: 0x000D0E65 File Offset: 0x000CF265
		public void Disappear()
		{
			this.m_IsDisplay = false;
			this.m_IsDrag = false;
			this.SetEnableInput(false);
		}

		// Token: 0x06002733 RID: 10035 RVA: 0x000D0E7C File Offset: 0x000CF27C
		public void SetSprite(Sprite sprite, bool isChangeOnOutOfScreen)
		{
			if (this.m_Images[this.m_CurrentIdx].sprite == sprite)
			{
				return;
			}
			if (isChangeOnOutOfScreen)
			{
				this.m_CurrentIdx++;
				if (this.m_CurrentIdx >= this.m_Images.Length)
				{
					this.m_CurrentIdx = 0;
				}
				this.m_Handlers[this.m_CurrentIdx].m_RectTransform.localPosition = this.m_HidePosition;
				this.m_Images[this.m_CurrentIdx].sprite = sprite;
			}
			else
			{
				this.m_Images[this.m_CurrentIdx].sprite = sprite;
			}
		}

		// Token: 0x06002734 RID: 10036 RVA: 0x000D0F1D File Offset: 0x000CF31D
		public void SetEnableInput(bool flag)
		{
			this.m_IsEnableInput = flag;
			if (!flag)
			{
				this.m_IsDrag = false;
			}
		}

		// Token: 0x06002735 RID: 10037 RVA: 0x000D0F33 File Offset: 0x000CF333
		public void SetEnableRender(bool flag)
		{
			base.gameObject.SetActive(flag);
		}

		// Token: 0x06002736 RID: 10038 RVA: 0x000D0F41 File Offset: 0x000CF341
		public void OnPointerDown(PointerEventData eventData)
		{
			if (!this.m_IsDisplay)
			{
				return;
			}
			this.m_IsDrag = true;
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x000D0F56 File Offset: 0x000CF356
		public void OnPointerUp(PointerEventData eventData)
		{
			this.m_IsDrag = false;
		}

		// Token: 0x06002738 RID: 10040 RVA: 0x000D0F5F File Offset: 0x000CF35F
		public void OnPointerExit(PointerEventData eventData)
		{
			this.m_IsDrag = false;
		}

		// Token: 0x04002DEA RID: 11754
		private const float EASE_SPEED = 1f;

		// Token: 0x04002DEB RID: 11755
		private const float FLICK_THRESHOLD = 32f;

		// Token: 0x04002DEC RID: 11756
		private const float HIDE_OFFSETX = 380f;

		// Token: 0x04002DED RID: 11757
		private bool m_IsDrag;

		// Token: 0x04002DEE RID: 11758
		private bool m_IsDisplay = true;

		// Token: 0x04002DEF RID: 11759
		private Vector3 m_StartPosition;

		// Token: 0x04002DF0 RID: 11760
		private Vector3 m_HidePosition;

		// Token: 0x04002DF1 RID: 11761
		[SerializeField]
		private CanvasScaler m_CanvasScaler;

		// Token: 0x04002DF2 RID: 11762
		[SerializeField]
		private BattleUI m_UI;

		// Token: 0x04002DF3 RID: 11763
		[SerializeField]
		private Image[] m_Images;

		// Token: 0x04002DF4 RID: 11764
		private int m_CurrentIdx;

		// Token: 0x04002DF5 RID: 11765
		private bool m_IsEnableInput = true;

		// Token: 0x04002DF6 RID: 11766
		private OwnerImage.ImageHandler[] m_Handlers;

		// Token: 0x02000784 RID: 1924
		public class ImageHandler
		{
			// Token: 0x04002DF7 RID: 11767
			public Image m_Image;

			// Token: 0x04002DF8 RID: 11768
			public RectTransform m_RectTransform;

			// Token: 0x04002DF9 RID: 11769
			public bool m_IsMoving;
		}
	}
}
