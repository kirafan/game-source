﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000777 RID: 1911
	public class HPBarGleam : MonoBehaviour
	{
		// Token: 0x060026E5 RID: 9957 RVA: 0x000CF23D File Offset: 0x000CD63D
		private void Start()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_ParentRectTransform = this.m_RectTransform.parent.GetComponent<RectTransform>();
		}

		// Token: 0x060026E6 RID: 9958 RVA: 0x000CF264 File Offset: 0x000CD664
		private void Update()
		{
			this.m_RectTransform.sizeDelta = new Vector2(this.m_ParentRectTransform.rect.width * this.m_Bar.fillAmount, this.m_RectTransform.sizeDelta.y);
			this.m_Rate += Time.deltaTime * 1f;
			this.m_Gleam.anchoredPosition = new Vector3((this.m_ParentRectTransform.rect.width + this.m_Gleam.rect.width) * this.m_Rate, 0f);
			if (this.m_Rate > 3f)
			{
				this.m_Rate = 0f;
			}
		}

		// Token: 0x04002D90 RID: 11664
		private const float MAXRATE = 3f;

		// Token: 0x04002D91 RID: 11665
		[SerializeField]
		private Image m_Bar;

		// Token: 0x04002D92 RID: 11666
		[SerializeField]
		private RectTransform m_Gleam;

		// Token: 0x04002D93 RID: 11667
		private const float SPEED = 1f;

		// Token: 0x04002D94 RID: 11668
		private RectTransform m_RectTransform;

		// Token: 0x04002D95 RID: 11669
		private RectTransform m_ParentRectTransform;

		// Token: 0x04002D96 RID: 11670
		private float m_Rate;
	}
}
