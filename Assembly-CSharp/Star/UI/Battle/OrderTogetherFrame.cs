﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000782 RID: 1922
	public class OrderTogetherFrame : MonoBehaviour
	{
		// Token: 0x06002724 RID: 10020 RVA: 0x000D093D File Offset: 0x000CED3D
		public void SetSpeed(float speed)
		{
			this.m_ScrollSpeed = speed;
		}

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06002725 RID: 10021 RVA: 0x000D0946 File Offset: 0x000CED46
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06002726 RID: 10022 RVA: 0x000D096B File Offset: 0x000CED6B
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002727 RID: 10023 RVA: 0x000D0973 File Offset: 0x000CED73
		public void Start()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x06002728 RID: 10024 RVA: 0x000D0990 File Offset: 0x000CED90
		public void Update()
		{
			this.m_ScrollRate += this.m_ScrollSpeed * Time.deltaTime;
			if (this.m_ScrollRate >= 1f)
			{
				this.m_ScrollRate -= (float)((int)this.m_ScrollRate);
			}
			for (int i = 0; i < this.m_RainbowImage.Length; i++)
			{
				this.m_WorkVec.x = -this.m_ScrollRate + (float)i - 0.01f * (float)i;
				this.m_WorkVec.y = 0f;
				this.m_RainbowImage[i].anchoredPosition = this.m_RainbowImage[i].rect.width * this.m_WorkVec;
			}
		}

		// Token: 0x06002729 RID: 10025 RVA: 0x000D0A4F File Offset: 0x000CEE4F
		public void SetActive(bool flg)
		{
			this.GameObject.SetActive(flg);
		}

		// Token: 0x04002DE3 RID: 11747
		private const int RAINBOW_IMAGE_NUM = 2;

		// Token: 0x04002DE4 RID: 11748
		[SerializeField]
		private RectTransform[] m_RainbowImage;

		// Token: 0x04002DE5 RID: 11749
		[SerializeField]
		private float m_ScrollSpeed = 1f;

		// Token: 0x04002DE6 RID: 11750
		private GameObject m_GameObject;

		// Token: 0x04002DE7 RID: 11751
		private RectTransform m_RectTransform;

		// Token: 0x04002DE8 RID: 11752
		private float m_ScrollRate;

		// Token: 0x04002DE9 RID: 11753
		private Vector2 m_WorkVec = new Vector2(0f, 0f);
	}
}
