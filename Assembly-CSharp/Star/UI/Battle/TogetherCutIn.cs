﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000796 RID: 1942
	public class TogetherCutIn : MonoBehaviour
	{
		// Token: 0x0600278D RID: 10125 RVA: 0x000D3607 File Offset: 0x000D1A07
		private void Start()
		{
			if (this.m_Step == TogetherCutIn.eStep.Hide)
			{
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x0600278E RID: 10126 RVA: 0x000D3620 File Offset: 0x000D1A20
		public void Setup(BattleUI owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600278F RID: 10127 RVA: 0x000D362C File Offset: 0x000D1A2C
		private void Destroy()
		{
			for (int i = 0; i < this.m_BustImages.Length; i++)
			{
				this.m_BustImages[i].Destroy();
			}
		}

		// Token: 0x06002790 RID: 10128 RVA: 0x000D365F File Offset: 0x000D1A5F
		public void ShowChara()
		{
			this.m_IsReserveShowChara = true;
		}

		// Token: 0x06002791 RID: 10129 RVA: 0x000D3668 File Offset: 0x000D1A68
		public void End()
		{
			this.m_Owner.BattleSystem.UniqueSkillCutIn.Stop();
			this.m_CharaObj.SetActive(false);
			base.gameObject.SetActive(false);
			this.m_Step = TogetherCutIn.eStep.Hide;
			this.Destroy();
		}

		// Token: 0x06002792 RID: 10130 RVA: 0x000D36A4 File Offset: 0x000D1AA4
		public bool IsFilledScreen()
		{
			return this.m_Step == TogetherCutIn.eStep.Filled;
		}

		// Token: 0x06002793 RID: 10131 RVA: 0x000D36B0 File Offset: 0x000D1AB0
		private void Update()
		{
			switch (this.m_Step)
			{
			case TogetherCutIn.eStep.WaitStartFade:
				if (!this.m_Fade.IsPlaying)
				{
					this.m_Owner.BattleSystem.UniqueSkillCutIn.Play();
					this.m_SelectUI.Hide();
					this.m_Step = TogetherCutIn.eStep.WaitPrepare;
				}
				break;
			case TogetherCutIn.eStep.WaitPrepare:
			{
				bool flag = true;
				for (int i = 0; i < this.m_BustImages.Length; i++)
				{
					if (this.m_CharaObjs[i].activeSelf && !this.m_BustImages[i].IsDoneLoad())
					{
						flag = false;
						break;
					}
				}
				if (flag && this.m_IsReserveShowChara)
				{
					this.m_IsReserveShowChara = false;
					this.m_Fade.Hide();
					this.m_CharaObj.SetActive(true);
					for (int j = 0; j < this.m_CharaAnims.Length; j++)
					{
						this.m_CharaAnims[j].gameObject.SetActive(false);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_KIRARAJUMP_CUTIN_1 + (this.m_InCharaNum - 1), 1f, 0, -1, -1);
					this.m_InCharaIdx = 0;
					this.m_Timer = 0f;
					this.m_Step = TogetherCutIn.eStep.CharaIn;
				}
				break;
			}
			case TogetherCutIn.eStep.CharaIn:
				this.m_Timer += Time.deltaTime;
				if (this.m_Timer >= 0.2f * (float)this.m_InCharaIdx + 0.1f)
				{
					this.m_CharaAnims[this.m_InCharaIdx].gameObject.SetActive(true);
					this.m_CharaAnims[this.m_InCharaIdx].Play();
					if (this.m_InCharaIdx == 0)
					{
						this.m_Owner.BattleSystem.RequestKiraraJumpVoice();
					}
					this.m_InCharaIdx++;
					if (this.m_InCharaIdx >= this.m_CharaAnims.Length)
					{
						this.m_Step = TogetherCutIn.eStep.WaitCharaIn;
					}
				}
				break;
			case TogetherCutIn.eStep.WaitCharaIn:
			{
				bool flag2 = true;
				for (int k = 0; k < this.m_CharaAnims.Length; k++)
				{
					if (this.m_CharaAnims[k].isPlaying)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.m_Timer = 0f;
					this.m_Step = TogetherCutIn.eStep.ShowChara;
				}
				break;
			}
			case TogetherCutIn.eStep.ShowChara:
				this.m_Timer += Time.deltaTime;
				if (this.m_Timer >= 1.2f)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.2f, null);
					this.m_Step = TogetherCutIn.eStep.WaitEndFadeOut;
				}
				break;
			case TogetherCutIn.eStep.WaitEndFadeOut:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = TogetherCutIn.eStep.Filled;
				}
				break;
			}
		}

		// Token: 0x06002794 RID: 10132 RVA: 0x000D3980 File Offset: 0x000D1D80
		public void Play(int[] charaIDs)
		{
			base.gameObject.SetActive(true);
			this.m_CharaObj.SetActive(false);
			this.m_Fade.SetColor(Color.white);
			this.m_Fade.Show();
			this.m_Step = TogetherCutIn.eStep.WaitStartFade;
			this.m_IsReserveShowChara = false;
			this.m_InCharaNum = 0;
			for (int i = 0; i < this.m_CharaObjs.Length; i++)
			{
				if (i < charaIDs.Length && charaIDs[i] != -1)
				{
					this.m_InCharaNum++;
					this.m_CharaObjs[i].SetActive(true);
					this.m_BustImages[i].Apply(charaIDs[i], BustImage.eBustImageType.Full);
				}
				else
				{
					this.m_CharaObjs[i].SetActive(false);
					this.m_BustImages[i].Destroy();
				}
			}
		}

		// Token: 0x04002E8B RID: 11915
		private const float CHARAIN_START_TIME = 0.1f;

		// Token: 0x04002E8C RID: 11916
		private const float CHARAIN_INTERVAL = 0.2f;

		// Token: 0x04002E8D RID: 11917
		private const float SHOWCHARA_TIME = 1.2f;

		// Token: 0x04002E8E RID: 11918
		private const float ENDFADE_TIME = 0.2f;

		// Token: 0x04002E8F RID: 11919
		[SerializeField]
		private GameObject m_CharaObj;

		// Token: 0x04002E90 RID: 11920
		[SerializeField]
		private GameObject[] m_CharaObjs;

		// Token: 0x04002E91 RID: 11921
		[SerializeField]
		private BustImage[] m_BustImages;

		// Token: 0x04002E92 RID: 11922
		[SerializeField]
		private Animation[] m_CharaAnims;

		// Token: 0x04002E93 RID: 11923
		[SerializeField]
		private BattleFade m_Fade;

		// Token: 0x04002E94 RID: 11924
		[SerializeField]
		private TogetherSelect m_SelectUI;

		// Token: 0x04002E95 RID: 11925
		private TogetherCutIn.eStep m_Step;

		// Token: 0x04002E96 RID: 11926
		private bool m_IsReserveShowChara;

		// Token: 0x04002E97 RID: 11927
		private float m_Timer;

		// Token: 0x04002E98 RID: 11928
		private int m_InCharaNum;

		// Token: 0x04002E99 RID: 11929
		private int m_InCharaIdx;

		// Token: 0x04002E9A RID: 11930
		private BattleUI m_Owner;

		// Token: 0x02000797 RID: 1943
		private enum eStep
		{
			// Token: 0x04002E9C RID: 11932
			Hide,
			// Token: 0x04002E9D RID: 11933
			WaitStartFade,
			// Token: 0x04002E9E RID: 11934
			WaitPrepare,
			// Token: 0x04002E9F RID: 11935
			CharaIn,
			// Token: 0x04002EA0 RID: 11936
			WaitCharaIn,
			// Token: 0x04002EA1 RID: 11937
			ShowChara,
			// Token: 0x04002EA2 RID: 11938
			WaitEndFadeOut,
			// Token: 0x04002EA3 RID: 11939
			Filled
		}
	}
}
