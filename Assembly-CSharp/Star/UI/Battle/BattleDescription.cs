﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200074E RID: 1870
	public class BattleDescription : MonoBehaviour
	{
		// Token: 0x06002531 RID: 9521 RVA: 0x000C7063 File Offset: 0x000C5463
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06002532 RID: 9522 RVA: 0x000C7071 File Offset: 0x000C5471
		public void SetText(string text)
		{
			this.m_ScrollText.ResetScroll();
			this.m_Text.text = text.Replace("\n", string.Empty);
		}

		// Token: 0x04002BDC RID: 11228
		[SerializeField]
		private Text m_Text;

		// Token: 0x04002BDD RID: 11229
		[SerializeField]
		private ScrollText m_ScrollText;
	}
}
