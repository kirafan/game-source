﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200075E RID: 1886
	public class BattleOrderIndicator : MonoBehaviour
	{
		// Token: 0x17000283 RID: 643
		// (get) Token: 0x060025A0 RID: 9632 RVA: 0x000C8C2D File Offset: 0x000C702D
		public int SelectingIndex
		{
			get
			{
				return this.m_SelectingIndex;
			}
		}

		// Token: 0x060025A1 RID: 9633 RVA: 0x000C8C38 File Offset: 0x000C7038
		private BattleOrderFrame CreateCharaFrame()
		{
			BattleOrderFrame battleOrderFrame = UnityEngine.Object.Instantiate<BattleOrderFrame>(this.m_FramePrefab, this.m_FrameRoot);
			battleOrderFrame.Setup(this.m_StateIconOwner);
			battleOrderFrame.RectTransform.localScale = Vector3.one;
			battleOrderFrame.SetActive(false);
			return battleOrderFrame;
		}

		// Token: 0x060025A2 RID: 9634 RVA: 0x000C8C7C File Offset: 0x000C707C
		private BattleOrderSkillFrame CreateSkillFrame()
		{
			BattleOrderSkillFrame battleOrderSkillFrame = UnityEngine.Object.Instantiate<BattleOrderSkillFrame>(this.m_FrameSkillPrefab, this.m_FrameRoot);
			battleOrderSkillFrame.Setup();
			battleOrderSkillFrame.RectTransform.localScale = Vector3.one;
			battleOrderSkillFrame.SetActive(false);
			return battleOrderSkillFrame;
		}

		// Token: 0x060025A3 RID: 9635 RVA: 0x000C8CBC File Offset: 0x000C70BC
		public void Setup(StateIconOwner stateIconOwner)
		{
			this.m_StateIconOwner = stateIconOwner;
			int num = 6;
			this.m_FrameWidth = this.m_FramePrefab.GetComponent<RectTransform>().rect.width;
			this.m_SkillFrameWidth = this.m_FrameSkillPrefab.GetComponent<RectTransform>().rect.width;
			this.m_FrameSpacePerOrderValue = (this.m_FrameRoot.rect.width - this.m_FrameWidth * (float)num) / (BattleUtility.DefVal(eBattleDefineDB.OrderValueMax) - BattleUtility.DefVal(eBattleDefineDB.OrderValueMin));
			this.m_TogetherLineObj = this.m_TogetherLine.gameObject;
			for (int i = 0; i < num + 1; i++)
			{
				BattleOrderFrame battleOrderFrame = this.CreateCharaFrame();
				if (i == 0)
				{
					this.m_SelectFrameChara = battleOrderFrame;
				}
				else
				{
					this.m_EmptyCharaFrameInsts.Add(battleOrderFrame);
				}
				this.m_Frames.Add(battleOrderFrame);
			}
			this.SetCursorEnableRender(true);
			for (int j = 0; j < this.m_TogetherFrames.Length; j++)
			{
				this.m_TogetherFrames[j] = UnityEngine.Object.Instantiate<RectTransform>(this.m_TogetherFramePrefab, this.m_FrameRoot, false);
				this.m_TogetherFrames[j].localScale = Vector3.one;
				this.m_TogetherFrameObjs[j] = this.m_TogetherFrames[j].gameObject;
				this.m_TogetherFrameObjs[j].SetActive(false);
			}
			for (int k = 0; k < 5; k++)
			{
				BattleOrderSkillFrame battleOrderSkillFrame = this.CreateSkillFrame();
				if (k == 0)
				{
					this.m_SelectFrameCard = battleOrderSkillFrame;
				}
				else
				{
					this.m_EmptySkillFrameInsts.Add(battleOrderSkillFrame);
				}
				this.m_Frames.Add(battleOrderSkillFrame);
			}
			this.m_GoObj.SetActive(false);
			this.m_SelectFrame = this.m_SelectFrameChara;
		}

		// Token: 0x060025A4 RID: 9636 RVA: 0x000C8E7C File Offset: 0x000C727C
		public void Destroy()
		{
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				this.m_Frames[i].Destroy();
			}
		}

		// Token: 0x060025A5 RID: 9637 RVA: 0x000C8EB6 File Offset: 0x000C72B6
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x060025A6 RID: 9638 RVA: 0x000C8EC4 File Offset: 0x000C72C4
		public void SetTargetCursor(BattleDefine.ePartyType party, BattleDefine.eJoinMember join, bool flg)
		{
			for (int i = 0; i < this.m_ActiveCharaFrameInsts.Count; i++)
			{
				CharacterHandler charaHandler = this.m_ActiveCharaFrameInsts[i].CharaHandler;
				if (charaHandler.CharaBattle.MyParty.PartyType == party && charaHandler.CharaBattle.Join == join)
				{
					this.m_ActiveCharaFrameInsts[i].SetTargetCursor(flg);
				}
			}
		}

		// Token: 0x060025A7 RID: 9639 RVA: 0x000C8F38 File Offset: 0x000C7338
		private void Update()
		{
			if (this.m_IsReserveMoveFirstFrame)
			{
				this.MoveOwnerFrameToSelectFrame();
			}
			if (!this.IsSlide())
			{
				this.m_GearParent.Stop();
			}
			float deltaTime = Time.deltaTime;
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				if (this.m_Frames[i].isActiveAndEnabled)
				{
					if (this.m_Frames[i].GetMoveType() == BattleOrderFrameBase.eMoveType.Lane)
					{
						if (this.m_Frames[i].GetGoalPos() != this.m_Frames[i].RectTransform.localPosition.x)
						{
							float num;
							float num2;
							this.m_Frames[i].MoverCollision(deltaTime, out num, out num2);
							for (int j = 0; j < this.m_Frames.Count; j++)
							{
								if (this.m_Frames[j].isActiveAndEnabled)
								{
									if (this.m_Frames[j].GetMoveType() != BattleOrderFrameBase.eMoveType.Lane)
									{
										if (i != j)
										{
											if (this.m_Frames[j].GetMoveType() != BattleOrderFrameBase.eMoveType.FreeStop || this.m_Frames[j].GetGoalPos() != this.m_Frames[j].RectTransform.localPosition.x)
											{
												float num3;
												float num4;
												this.m_Frames[j].MoverCollision(deltaTime, out num3, out num4);
												if (num < num4 && num2 > num3)
												{
													this.m_Frames[j].Detour();
												}
												this.m_Frames[j].IsEnableJump = false;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x060025A8 RID: 9640 RVA: 0x000C911C File Offset: 0x000C751C
		private void LateUpdate()
		{
			if (this.m_IsSelecting)
			{
				this.SetCursorPos(-this.m_SelectFrame.RectTransform.anchoredPosition.x - this.m_FrameWidth * 0.5f);
				this.m_ArrowHeadAnimCount += Time.deltaTime * 2f;
				this.m_ArrowHead.sizeDelta = new Vector2(this.m_ArrowHead.sizeDelta.x, 29f + Mathf.Sin(this.m_ArrowHeadAnimCount) * 2f);
			}
			if (this.m_TogetherFrameList.Count > 0)
			{
				float num = -2000f;
				float num2 = 0f;
				for (int i = 0; i < this.m_TogetherFrameList.Count; i++)
				{
					if (this.m_TogetherFrameList[i].RectTransform.localPosition.y <= 0f)
					{
						float x = this.m_TogetherFrameList[i].RectTransform.localPosition.x;
						if (x > num)
						{
							num = x;
						}
					}
				}
				for (int j = 0; j < this.m_TogetherFrameList.Count; j++)
				{
					if (this.m_TogetherFrameList[j].RectTransform.localPosition.y <= 0f)
					{
						float x2 = this.m_TogetherFrameList[j].RectTransform.localPosition.x;
						if (x2 < num2)
						{
							num2 = x2;
						}
					}
				}
				if (!this.m_TogetherLineObj.activeSelf)
				{
					this.m_TogetherLineObj.SetActive(true);
				}
				this.m_TogetherLine.anchoredPosition = new Vector2((num + num2) * 0.5f, this.m_TogetherLine.anchoredPosition.y);
				this.m_TogetherLine.sizeDelta = new Vector2(num - num2 - this.m_FrameWidth + 4f, this.m_TogetherLine.sizeDelta.y);
				for (int k = 0; k < this.m_TogetherFrames.Length; k++)
				{
					if (this.m_TogetherFrameList.Count > k)
					{
						this.ApplyTogetherFrameState(k, this.m_TogetherFrameList[k].RectTransform);
					}
					else if (this.m_TogetherFrameObjs[k].activeSelf)
					{
						this.m_TogetherFrameObjs[k].SetActive(false);
					}
				}
			}
			else
			{
				if (this.m_TogetherLineObj.activeSelf)
				{
					this.m_TogetherLineObj.SetActive(false);
				}
				for (int l = 0; l < this.m_TogetherFrames.Length; l++)
				{
					if (this.m_TogetherFrameObjs[l].activeSelf)
					{
						this.m_TogetherFrameObjs[l].SetActive(false);
					}
				}
			}
		}

		// Token: 0x060025A9 RID: 9641 RVA: 0x000C9414 File Offset: 0x000C7814
		private void ApplyTogetherFrameState(int idx, RectTransform parent)
		{
			if (!this.m_TogetherFrameObjs[idx].activeSelf)
			{
				this.m_TogetherFrameObjs[idx].SetActive(true);
			}
			this.m_TogetherFrames[idx].SetParent(parent, false);
			this.m_TogetherFrames[idx].localPosition = Vector2.zero;
		}

		// Token: 0x060025AA RID: 9642 RVA: 0x000C9468 File Offset: 0x000C7868
		public void ClearTogetherFrame()
		{
			for (int i = 0; i < this.m_TogetherFrames.Length; i++)
			{
				if (this.m_TogetherFrameObjs[i].activeSelf)
				{
					this.m_TogetherFrameObjs[i].SetActive(false);
				}
			}
		}

		// Token: 0x060025AB RID: 9643 RVA: 0x000C94B0 File Offset: 0x000C78B0
		public BattleOrderFrame GetCharaFrame(CharacterHandler charaHandler)
		{
			for (int i = 0; i < this.m_ActiveCharaFrameInsts.Count; i++)
			{
				if (this.m_ActiveCharaFrameInsts[i].CharaHandler == charaHandler)
				{
					return this.m_ActiveCharaFrameInsts[i];
				}
			}
			if (this.m_EmptyCharaFrameInsts.Count > 0)
			{
				BattleOrderFrame battleOrderFrame = this.m_EmptyCharaFrameInsts[0];
				battleOrderFrame.SetTargetCursor(false);
				battleOrderFrame.CharaHandler = charaHandler;
				if (charaHandler.CharaBattle.MyParty.IsPlayerParty)
				{
					battleOrderFrame.ApplyPL(charaHandler.CharaID);
					battleOrderFrame.SetType(BattleDefine.eJoinMember.None);
				}
				else
				{
					battleOrderFrame.ApplyEN(charaHandler.CharaResource.ResourceID);
					battleOrderFrame.SetType(charaHandler.CharaBattle.Join);
				}
				this.m_ActiveCharaFrameInsts.Add(battleOrderFrame);
				this.m_EmptyCharaFrameInsts.Remove(battleOrderFrame);
				return battleOrderFrame;
			}
			return null;
		}

		// Token: 0x060025AC RID: 9644 RVA: 0x000C959C File Offset: 0x000C799C
		public BattleOrderSkillFrame GetSkillFrame(BattleOrder.CardArguments cardArg)
		{
			for (int i = 0; i < this.m_ActiveSkillFrameInsts.Count; i++)
			{
				if (this.m_ActiveSkillFrameInsts[i].CardArg == cardArg)
				{
					return this.m_ActiveSkillFrameInsts[i];
				}
			}
			if (this.m_EmptySkillFrameInsts.Count > 0)
			{
				BattleOrderSkillFrame battleOrderSkillFrame = this.m_EmptySkillFrameInsts[0];
				battleOrderSkillFrame.CardArg = cardArg;
				this.m_ActiveSkillFrameInsts.Add(battleOrderSkillFrame);
				this.m_EmptySkillFrameInsts.Remove(battleOrderSkillFrame);
				battleOrderSkillFrame.Popup();
				return battleOrderSkillFrame;
			}
			BattleOrderSkillFrame battleOrderSkillFrame2 = this.CreateSkillFrame();
			battleOrderSkillFrame2.CardArg = cardArg;
			this.m_ActiveSkillFrameInsts.Add(battleOrderSkillFrame2);
			battleOrderSkillFrame2.Popup();
			return battleOrderSkillFrame2;
		}

		// Token: 0x060025AD RID: 9645 RVA: 0x000C9650 File Offset: 0x000C7A50
		private void RemoveInactiveChara(BattleOrder order)
		{
			List<BattleOrderFrame> list = new List<BattleOrderFrame>();
			for (int i = 0; i < this.m_ActiveCharaFrameInsts.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < order.GetFrameNum(); j++)
				{
					BattleOrder.FrameData frameDataAt = order.GetFrameDataAt(j);
					if (!frameDataAt.IsCardType)
					{
						if (frameDataAt.m_Owner == this.m_ActiveCharaFrameInsts[i].CharaHandler)
						{
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					list.Add(this.m_ActiveCharaFrameInsts[i]);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				this.m_ActiveCharaFrameInsts.Remove(list[k]);
				this.m_EmptyCharaFrameInsts.Add(list[k]);
				list[k].SetMoveType(BattleOrderFrameBase.eMoveType.Lane);
				list[k].Fade(0f, 0.2f);
			}
		}

		// Token: 0x060025AE RID: 9646 RVA: 0x000C975C File Offset: 0x000C7B5C
		private void RemoveInactiveSkill(BattleOrder order)
		{
			List<BattleOrderSkillFrame> list = new List<BattleOrderSkillFrame>();
			for (int i = 0; i < this.m_ActiveSkillFrameInsts.Count; i++)
			{
				bool flag = false;
				for (int j = 0; j < order.GetFrameNum(); j++)
				{
					BattleOrder.FrameData frameDataAt = order.GetFrameDataAt(j);
					if (!frameDataAt.IsCharaType)
					{
						if (frameDataAt.m_CardArgs == this.m_ActiveSkillFrameInsts[i].CardArg)
						{
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					list.Add(this.m_ActiveSkillFrameInsts[i]);
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				this.m_ActiveSkillFrameInsts.Remove(list[k]);
				this.m_EmptySkillFrameInsts.Add(list[k]);
				list[k].SetMoveType(BattleOrderFrameBase.eMoveType.Lane);
				list[k].Fade(0f, 0.2f);
			}
		}

		// Token: 0x060025AF RID: 9647 RVA: 0x000C9864 File Offset: 0x000C7C64
		public void SetFrames(BattleOrder battleOrder, int selectingIndex, BattleOrderIndicator.eMoveType moveType)
		{
			if (selectingIndex < 0)
			{
				selectingIndex = 0;
			}
			int num = 6;
			int num2 = 0;
			for (int i = 0; i < battleOrder.GetFrameNum(); i++)
			{
				if (battleOrder.GetFrameDataAt(i).IsCardType)
				{
					num2++;
				}
			}
			this.m_FrameSpacePerOrderValue = (this.m_FrameRoot.rect.width - this.m_FrameWidth * (float)num - this.m_SkillFrameWidth * (float)num2) / (BattleUtility.DefVal(eBattleDefineDB.OrderValueMax) - BattleUtility.DefVal(eBattleDefineDB.OrderValueMin));
			bool flag = false;
			bool flag2 = false;
			if (this.m_SelectingIndex == 0 && selectingIndex > 0)
			{
				flag2 = true;
			}
			this.m_SelectingIndex = selectingIndex;
			this.SetCursorEnableRender(this.m_SelectingIndex > 0);
			if (this.m_SelectingIndex > 0 && !this.m_IsSelecting)
			{
				if (!battleOrder.GetFrameDataAt(0).IsCardType)
				{
					this.m_SelectFrame.SetAlpha(0f);
					this.m_SelectFrame.SetBlink(false);
					this.m_SelectFrame = this.m_SelectFrameChara;
				}
				else
				{
					this.m_SelectFrame.SetAlpha(0f);
					this.m_SelectFrame.SetBlink(false);
					this.m_SelectFrame = this.m_SelectFrameCard;
				}
				this.m_SelectFrame.SetPos(new Vector3(-this.m_SelectFrame.RectTransform.rect.width * 0.5f, 0f, 0f));
				this.m_SelectFrame.SetBlink(true);
				this.m_IsSelecting = true;
			}
			if (moveType == BattleOrderIndicator.eMoveType.Immediate)
			{
				if (this.m_ActionFrame != null)
				{
					this.m_ActionFrame.SetAlpha(0f);
					this.m_ActionFrame.SetBlink(false);
					this.m_ActionFrame.ChaseTo(null, 0f);
					this.m_ActionFrame = null;
				}
				if (this.m_SelectFrame != null)
				{
					this.m_SelectFrame.SetAlpha(0f);
					this.m_SelectFrame.SetBlink(false);
					this.m_IsSelecting = false;
				}
			}
			this.RemoveInactiveChara(battleOrder);
			this.RemoveInactiveSkill(battleOrder);
			List<BattleDefine.eJoinMember> list = null;
			if (this.m_SelectingIndex > 0)
			{
				int num3;
				int num4;
				list = battleOrder.CalcTogetherAttackDefaultOrders(out num3, out num4, true);
			}
			this.m_TogetherFrameList.Clear();
			int frameNum = battleOrder.GetFrameNum();
			float num5 = 0f;
			float num6 = 0f;
			for (int j = 0; j < frameNum; j++)
			{
				BattleOrderFrameBase battleOrderFrameBase = null;
				BattleOrder.FrameData frameDataAt = battleOrder.GetFrameDataAt(j);
				BattleOrder.eFrameType frameType = frameDataAt.m_FrameType;
				if (frameType != BattleOrder.eFrameType.Chara)
				{
					if (frameType == BattleOrder.eFrameType.Card)
					{
						BattleOrder.CardArguments cardArgs = frameDataAt.m_CardArgs;
						BattleOrderSkillFrame battleOrderSkillFrame;
						if (this.m_SelectingIndex > 0 && this.m_SelectingIndex == j)
						{
							this.m_SelectFrame.SetActive(false);
							battleOrderSkillFrame = (BattleOrderSkillFrame)this.m_SelectFrameCard;
							this.m_SelectFrame = battleOrderSkillFrame;
							battleOrderSkillFrame.SkipPopup();
						}
						else
						{
							battleOrderSkillFrame = this.GetSkillFrame(cardArgs);
						}
						battleOrderSkillFrame.ApplySkill(cardArgs.m_RefID, frameDataAt);
						battleOrderSkillFrame.SetAliveNumCount(cardArgs.m_AliveNum);
						battleOrderFrameBase = battleOrderSkillFrame;
					}
				}
				else
				{
					CharacterHandler owner = frameDataAt.m_Owner;
					BattleOrderFrame battleOrderFrame;
					if (this.m_SelectingIndex > 0 && this.m_SelectingIndex == j)
					{
						this.m_SelectFrame.SetActive(false);
						battleOrderFrame = (BattleOrderFrame)this.m_SelectFrameChara;
						this.m_SelectFrame = battleOrderFrame;
					}
					else
					{
						battleOrderFrame = this.GetCharaFrame(owner);
					}
					if (owner.CharaBattle.MyParty.IsPlayerParty)
					{
						battleOrderFrame.ApplyPL(frameDataAt.m_Owner.CharaID);
						battleOrderFrame.SetType(BattleDefine.eJoinMember.None);
					}
					else
					{
						battleOrderFrame.ApplyEN(frameDataAt.m_Owner.CharaResource.ResourceID);
						battleOrderFrame.SetType(frameDataAt.m_Owner.CharaBattle.Join);
					}
					battleOrderFrameBase = battleOrderFrame;
					if (list != null && this.m_SelectFrame != battleOrderFrame)
					{
						for (int k = 0; k < list.Count; k++)
						{
							if (frameDataAt.m_Owner.CharaBattle.MyParty.IsPlayerParty && frameDataAt.m_Owner.CharaBattle.Join == list[k])
							{
								this.m_TogetherFrameList.Add(battleOrderFrameBase);
							}
						}
					}
				}
				float orderValue = frameDataAt.m_OrderValue;
				float num7 = this.m_FrameWidth;
				if (frameDataAt.IsCardType)
				{
					num7 = this.m_SkillFrameWidth;
				}
				float num8;
				if (j == 0)
				{
					num8 = num7 * 0.5f;
				}
				else
				{
					float num9 = orderValue - num5;
					float num10 = num9 * this.m_FrameSpacePerOrderValue;
					num8 = num6 + num7 * 0.5f + num10;
				}
				num6 = num8 + num7 * 0.5f;
				num5 = orderValue;
				if (battleOrderFrameBase != this.m_SelectFrame && (!battleOrderFrameBase.isActiveAndEnabled || battleOrderFrameBase.Alpha < 1f))
				{
					battleOrderFrameBase.SetPos(new Vector3(-num8, 0f));
				}
				battleOrderFrameBase.SetActive(true);
				battleOrderFrameBase.SetAlpha(1f);
				if (j == 0)
				{
					this.m_FirstFrame = battleOrderFrameBase;
				}
				if (frameDataAt.IsCharaType)
				{
					((BattleOrderFrame)battleOrderFrameBase).SetStunIcon(frameDataAt.m_Owner.CharaBattle.IsStun());
					((BattleOrderFrame)battleOrderFrameBase).SetAbnormal(frameDataAt.m_Owner.CharaBattle.Param.GetStateAbnormalFlags());
				}
				if (moveType != BattleOrderIndicator.eMoveType.Immediate)
				{
					if (!flag)
					{
						float num11 = Mathf.Abs(-num8 - battleOrderFrameBase.RectTransform.localPosition.x);
						if (num11 > 10f)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.BTL_TIMELINE_MOVE, 1f, 0, -1, -1);
							flag = true;
						}
					}
					if (moveType == BattleOrderIndicator.eMoveType.Selecting)
					{
						if (battleOrderFrameBase == this.m_SelectFrame)
						{
							if (flag2)
							{
								battleOrderFrameBase.LaneMove(-num8, 1600f);
							}
							else
							{
								battleOrderFrameBase.LaneMove(-num8, 800f);
							}
						}
						else if (flag2 && j == 0)
						{
							battleOrderFrameBase.LaneMove(-num8, 800f);
						}
						else
						{
							battleOrderFrameBase.FreeMove(-num8, 800f);
						}
					}
					else if (this.m_ActionFrame != battleOrderFrameBase)
					{
						if (moveType == BattleOrderIndicator.eMoveType.Stun && battleOrderFrameBase.OrderValue != orderValue)
						{
							battleOrderFrameBase.LaneMove(-num8, 2000f);
						}
						else if (moveType == BattleOrderIndicator.eMoveType.Slide)
						{
							battleOrderFrameBase.LaneMove(-num8, 800f);
						}
						else
						{
							battleOrderFrameBase.MoveToKeepSpeed(-num8, 800f);
						}
					}
					else
					{
						this.m_SelectFrame.MoveToKeepSpeed(-num8, 800f);
					}
				}
				else
				{
					battleOrderFrameBase.SetPos(new Vector3(-num8, 0f, 0f));
					battleOrderFrameBase.SetMoveType(BattleOrderFrameBase.eMoveType.Lane);
				}
				battleOrderFrameBase.RectTransform.SetAsLastSibling();
				battleOrderFrameBase.OrderValue = orderValue;
			}
			if (this.m_IsSelecting)
			{
				this.m_SelectFrame.RectTransform.SetAsLastSibling();
			}
			this.m_FirstFrame.SetCurrent(true);
			if (this.m_FirstFrame.IsPlayer())
			{
				this.m_GoObj.SetActive(true);
			}
			else
			{
				this.m_GoObj.SetActive(false);
			}
			if (flag2)
			{
				this.PlayTurnEffect();
			}
			if (this.m_SelectFrame.GetMoveDistance() < 0f)
			{
				this.m_GearParent.SetSpeed(-1f, true);
			}
			else if (this.m_SelectFrame.GetMoveDistance() > 0f)
			{
				this.m_GearParent.SetSpeed(-1f, false);
			}
		}

		// Token: 0x060025B0 RID: 9648 RVA: 0x000CA018 File Offset: 0x000C8418
		public void SlideFrames(BattleOrder battleOrder)
		{
			if (this.m_ActionFrame != null)
			{
				this.m_ActionFrame.ChaseTo(null, 0f);
				this.m_ActionFrame = null;
			}
			this.SetFrames(battleOrder, 0, BattleOrderIndicator.eMoveType.Slide);
			this.m_IsSelecting = false;
			this.m_SelectFrame.SetAlpha(0f);
			this.m_SelectFrame.SetBlink(false);
			this.m_GearParent.SetSpeed(-1f, false);
		}

		// Token: 0x060025B1 RID: 9649 RVA: 0x000CA08B File Offset: 0x000C848B
		public void SlideStunFrames(BattleOrder battleOrder)
		{
			this.SetFrames(battleOrder, 0, BattleOrderIndicator.eMoveType.Stun);
			this.m_IsSelecting = false;
		}

		// Token: 0x060025B2 RID: 9650 RVA: 0x000CA0A0 File Offset: 0x000C84A0
		public void MoveOwnerFrameToSelectFrame()
		{
			this.ClearTogetherFrame();
			this.m_TogetherFrameList.Clear();
			if (this.m_FirstFrame.IsCard() && ((BattleOrderSkillFrame)this.m_FirstFrame).CardArg.m_AliveNum <= 0)
			{
				return;
			}
			this.m_ActionFrame = this.m_FirstFrame;
			if (this.IsSlideWithOutFirstFrameMove())
			{
				this.m_IsReserveMoveFirstFrame = true;
			}
			else
			{
				this.m_SelectFrame.SetAlpha(1f);
				this.m_ActionFrame.ChaseTo(this.m_SelectFrame.RectTransform, 2000f);
				this.m_ActionFrame.RectTransform.SetAsLastSibling();
				this.SetCursorEnableRender(false);
				this.m_IsReserveMoveFirstFrame = false;
			}
		}

		// Token: 0x060025B3 RID: 9651 RVA: 0x000CA156 File Offset: 0x000C8556
		public bool IsSlide()
		{
			return this.IsSlideWithOutFirstFrameMove() || this.m_IsReserveMoveFirstFrame;
		}

		// Token: 0x060025B4 RID: 9652 RVA: 0x000CA16C File Offset: 0x000C856C
		private bool IsSlideWithOutFirstFrameMove()
		{
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				if (this.m_Frames[i].isActiveAndEnabled)
				{
					if (this.m_Frames[i].isMove())
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x060025B5 RID: 9653 RVA: 0x000CA1CC File Offset: 0x000C85CC
		public bool IsPopup()
		{
			for (int i = 0; i < this.m_Frames.Count; i++)
			{
				if (this.m_Frames[i].IsCard())
				{
					if (((BattleOrderSkillFrame)this.m_Frames[i]).IsPopup())
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x060025B6 RID: 9654 RVA: 0x000CA230 File Offset: 0x000C8630
		private void SetCursorPos(float x)
		{
			this.m_Arrow.sizeDelta = new Vector2(x, this.m_Arrow.sizeDelta.y);
			GameObject gameObject = this.m_Arrow.gameObject;
			if (x < 16f)
			{
				this.m_EnoughArrowLength = false;
				if (gameObject.activeSelf)
				{
					gameObject.SetActive(false);
				}
			}
			else
			{
				this.m_EnoughArrowLength = true;
				if (!gameObject.activeSelf)
				{
					gameObject.SetActive(this.m_EnableArrow && this.m_EnoughArrowLength);
				}
			}
		}

		// Token: 0x060025B7 RID: 9655 RVA: 0x000CA2C2 File Offset: 0x000C86C2
		private void SetCursorEnableRender(bool flg)
		{
			this.m_EnableArrow = flg;
			this.m_Arrow.gameObject.SetActive(this.m_EnableArrow && this.m_EnoughArrowLength);
		}

		// Token: 0x060025B8 RID: 9656 RVA: 0x000CA2F0 File Offset: 0x000C86F0
		private void PlayTurnEffect()
		{
			this.m_TurnEffect.position = this.m_FirstFrame.RectTransform.position;
			this.m_TurnEffect.sizeDelta = new Vector2(this.m_FirstFrame.RectTransform.rect.width, this.m_FirstFrame.RectTransform.rect.height);
			this.m_TurnEffect.GetComponent<AnimUIPlayer>().PlayIn();
			if (this.m_FirstFrame.IsPlayer())
			{
				this.m_TurnEffect.GetComponent<Image>().color = this.PLAYERCOLOR;
			}
			else
			{
				this.m_TurnEffect.GetComponent<Image>().color = this.ENEMYCOLOR;
			}
		}

		// Token: 0x04002C4B RID: 11339
		private readonly Color ENEMYCOLOR = new Color(0.93333334f, 0.5372549f, 0.22352941f);

		// Token: 0x04002C4C RID: 11340
		private readonly Color PLAYERCOLOR = new Color(0.14117648f, 0.73333335f, 0.40392157f);

		// Token: 0x04002C4D RID: 11341
		private const int SKILL_FRAME_INST_DEFAULT = 4;

		// Token: 0x04002C4E RID: 11342
		private const float FRAME_FIRSTSELECT_SPEED = 1600f;

		// Token: 0x04002C4F RID: 11343
		private const float FRAME_SELECT_SPEED = 800f;

		// Token: 0x04002C50 RID: 11344
		private const float FRAME_SLIDE_SPEED = 800f;

		// Token: 0x04002C51 RID: 11345
		private const float FRAME_MOVE_SPEED = 2000f;

		// Token: 0x04002C52 RID: 11346
		private const float FRAME_FADE_TIME = 0.2f;

		// Token: 0x04002C53 RID: 11347
		private const float ARROW_HEAD_ANIM_SPEED = 2f;

		// Token: 0x04002C54 RID: 11348
		private const float ARROW_HEAD_ANIM_RADIUS = 2f;

		// Token: 0x04002C55 RID: 11349
		[SerializeField]
		private BattleOrderFrame m_FramePrefab;

		// Token: 0x04002C56 RID: 11350
		[SerializeField]
		private BattleOrderSkillFrame m_FrameSkillPrefab;

		// Token: 0x04002C57 RID: 11351
		[SerializeField]
		private RectTransform m_FrameRoot;

		// Token: 0x04002C58 RID: 11352
		[SerializeField]
		private RectTransform m_Arrow;

		// Token: 0x04002C59 RID: 11353
		[SerializeField]
		private RectTransform m_ArrowHead;

		// Token: 0x04002C5A RID: 11354
		[SerializeField]
		private RectTransform m_TogetherLine;

		// Token: 0x04002C5B RID: 11355
		private GameObject m_TogetherLineObj;

		// Token: 0x04002C5C RID: 11356
		[SerializeField]
		private RectTransform m_TogetherFramePrefab;

		// Token: 0x04002C5D RID: 11357
		private RectTransform[] m_TogetherFrames = new RectTransform[4];

		// Token: 0x04002C5E RID: 11358
		private GameObject[] m_TogetherFrameObjs = new GameObject[4];

		// Token: 0x04002C5F RID: 11359
		[SerializeField]
		private RectTransform m_TurnEffect;

		// Token: 0x04002C60 RID: 11360
		[SerializeField]
		private GearParent m_GearParent;

		// Token: 0x04002C61 RID: 11361
		[SerializeField]
		private GameObject m_GoObj;

		// Token: 0x04002C62 RID: 11362
		private List<BattleOrderFrame> m_EmptyCharaFrameInsts = new List<BattleOrderFrame>();

		// Token: 0x04002C63 RID: 11363
		private List<BattleOrderFrame> m_ActiveCharaFrameInsts = new List<BattleOrderFrame>();

		// Token: 0x04002C64 RID: 11364
		private List<BattleOrderSkillFrame> m_EmptySkillFrameInsts = new List<BattleOrderSkillFrame>();

		// Token: 0x04002C65 RID: 11365
		private List<BattleOrderSkillFrame> m_ActiveSkillFrameInsts = new List<BattleOrderSkillFrame>();

		// Token: 0x04002C66 RID: 11366
		private List<BattleOrderFrameBase> m_Frames = new List<BattleOrderFrameBase>();

		// Token: 0x04002C67 RID: 11367
		private BattleOrderFrameBase m_FirstFrame;

		// Token: 0x04002C68 RID: 11368
		private BattleOrderFrameBase m_SelectFrame;

		// Token: 0x04002C69 RID: 11369
		private BattleOrderFrameBase m_SelectFrameChara;

		// Token: 0x04002C6A RID: 11370
		private BattleOrderFrameBase m_SelectFrameCard;

		// Token: 0x04002C6B RID: 11371
		private BattleOrderFrameBase m_ActionFrame;

		// Token: 0x04002C6C RID: 11372
		private float m_FrameWidth;

		// Token: 0x04002C6D RID: 11373
		private float m_SkillFrameWidth;

		// Token: 0x04002C6E RID: 11374
		private float m_FrameSpacePerOrderValue;

		// Token: 0x04002C6F RID: 11375
		private int m_SelectingIndex;

		// Token: 0x04002C70 RID: 11376
		private bool m_IsSelecting;

		// Token: 0x04002C71 RID: 11377
		private List<BattleOrderFrameBase> m_TogetherFrameList = new List<BattleOrderFrameBase>();

		// Token: 0x04002C72 RID: 11378
		private bool m_IsReserveMoveFirstFrame;

		// Token: 0x04002C73 RID: 11379
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04002C74 RID: 11380
		private bool m_EnableArrow;

		// Token: 0x04002C75 RID: 11381
		private bool m_EnoughArrowLength;

		// Token: 0x04002C76 RID: 11382
		private float m_ArrowHeadAnimCount;

		// Token: 0x0200075F RID: 1887
		public enum eMoveType
		{
			// Token: 0x04002C78 RID: 11384
			Immediate,
			// Token: 0x04002C79 RID: 11385
			Selecting,
			// Token: 0x04002C7A RID: 11386
			Slide,
			// Token: 0x04002C7B RID: 11387
			Stun
		}
	}
}
