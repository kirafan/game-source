﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000751 RID: 1873
	public class BattleFriendButton : MonoBehaviour
	{
		// Token: 0x14000039 RID: 57
		// (add) Token: 0x0600253F RID: 9535 RVA: 0x000C7618 File Offset: 0x000C5A18
		// (remove) Token: 0x06002540 RID: 9536 RVA: 0x000C7650 File Offset: 0x000C5A50
		public event Action OnClick;

		// Token: 0x06002541 RID: 9537 RVA: 0x000C7688 File Offset: 0x000C5A88
		public void Setup(int charaID)
		{
			this.m_CharaID = charaID;
			if (this.m_CharaID != -1)
			{
				this.m_BustImage.Apply(this.m_CharaID, BustImage.eBustImageType.Chara);
			}
			else
			{
				this.m_BustImage.Destroy();
				this.m_Button.Interactable = false;
			}
			this.SetEnableInput(false);
		}

		// Token: 0x06002542 RID: 9538 RVA: 0x000C76DD File Offset: 0x000C5ADD
		public void Destroy()
		{
			this.m_BustImage.Destroy();
		}

		// Token: 0x06002543 RID: 9539 RVA: 0x000C76EA File Offset: 0x000C5AEA
		public void SetEnableInput(bool flg)
		{
			if (this.m_CharaID == -1)
			{
				return;
			}
			this.m_Button.Interactable = flg;
		}

		// Token: 0x06002544 RID: 9540 RVA: 0x000C7705 File Offset: 0x000C5B05
		public void OnClickCallBack()
		{
			if (this.m_Button.IsEnableInput)
			{
				this.OnClick.Call();
			}
		}

		// Token: 0x04002BEB RID: 11243
		private const float FLASH_TIME_SEC = 0.5f;

		// Token: 0x04002BEC RID: 11244
		private const float BLINK_TIME_SEC = 0.5f;

		// Token: 0x04002BED RID: 11245
		private int m_CharaID = -1;

		// Token: 0x04002BEE RID: 11246
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x04002BEF RID: 11247
		[SerializeField]
		private CustomButton m_Button;
	}
}
