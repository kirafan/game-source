﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x020007A7 RID: 1959
	public class ResultItemData : MonoBehaviour
	{
		// Token: 0x06002808 RID: 10248 RVA: 0x000D6AE8 File Offset: 0x000D4EE8
		public void Apply(int itemID, int num)
		{
			this.m_itemID = itemID;
			this.m_ItemIconWithFrameCloner.GetInst<ItemIconWithFrame>().Apply(itemID);
			if (num == -1)
			{
				this.m_Text.enabled = false;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("x");
				stringBuilder.Append(num.ToString("D2"));
				this.m_Text.text = stringBuilder.ToString();
			}
		}

		// Token: 0x06002809 RID: 10249 RVA: 0x000D6B5B File Offset: 0x000D4F5B
		public void OnClickCallBack()
		{
			this.OnClick(this.m_itemID);
		}

		// Token: 0x04002F4F RID: 12111
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04002F50 RID: 12112
		[SerializeField]
		private Text m_Text;

		// Token: 0x04002F51 RID: 12113
		private int m_itemID;

		// Token: 0x04002F52 RID: 12114
		public Action<int> OnClick;
	}
}
