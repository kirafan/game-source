﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200078A RID: 1930
	public class RandomStarEmitter : MonoBehaviour
	{
		// Token: 0x0600275C RID: 10076 RVA: 0x000D1F04 File Offset: 0x000D0304
		private void Prepare()
		{
			this.m_RandomStars = new RandomStar[this.m_StarMax];
			this.m_RectTransform = base.GetComponent<RectTransform>();
			RectTransform component = this.m_RectTransform.GetComponent<RectTransform>();
			for (int i = 0; i < this.m_RandomStars.Length; i++)
			{
				RandomStar.Status status = new RandomStar.Status();
				status.m_StartDistanceMin = this.m_StartDistanceMin;
				status.m_StartDistanceMax = this.m_StartDistanceMax;
				status.m_MoveMin = this.m_MoveMin;
				status.m_MoveMax = this.m_MoveMax;
				status.m_MoveSec = this.m_MoveSec;
				status.m_Delay = (float)i * this.m_Delay;
				this.m_RandomStars[i] = UnityEngine.Object.Instantiate<RandomStar>(this.m_RandomStarPrefab, component, false);
				this.m_RandomStars[i].Setup(status);
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x0600275D RID: 10077 RVA: 0x000D1FCE File Offset: 0x000D03CE
		private void Start()
		{
			if (this.m_AutoStart)
			{
				this.Play();
			}
		}

		// Token: 0x0600275E RID: 10078 RVA: 0x000D1FE4 File Offset: 0x000D03E4
		private void Update()
		{
			if (this.m_IsReservePlay)
			{
				for (int i = 0; i < this.m_RandomStars.Length; i++)
				{
					this.m_RandomStars[i].Loop = this.m_Loop;
					this.m_RandomStars[i].Play();
				}
				this.m_IsReservePlay = false;
			}
		}

		// Token: 0x0600275F RID: 10079 RVA: 0x000D203C File Offset: 0x000D043C
		public void Play()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			if (base.gameObject.activeInHierarchy)
			{
				for (int i = 0; i < this.m_RandomStars.Length; i++)
				{
					this.m_RandomStars[i].Loop = this.m_Loop;
					this.m_RandomStars[i].Play();
				}
				this.m_IsReservePlay = false;
			}
			else
			{
				this.m_IsReservePlay = true;
			}
		}

		// Token: 0x06002760 RID: 10080 RVA: 0x000D20B8 File Offset: 0x000D04B8
		private void OnDestroy()
		{
			if (this.m_IsDonePrepare)
			{
				for (int i = 0; i < this.m_RandomStars.Length; i++)
				{
					UnityEngine.Object.Destroy(this.m_RandomStars[i].gameObject);
				}
			}
		}

		// Token: 0x06002761 RID: 10081 RVA: 0x000D20FB File Offset: 0x000D04FB
		private void OnEnable()
		{
			this.Play();
		}

		// Token: 0x04002E2D RID: 11821
		[SerializeField]
		private RandomStar m_RandomStarPrefab;

		// Token: 0x04002E2E RID: 11822
		[SerializeField]
		private int m_StarMax = 8;

		// Token: 0x04002E2F RID: 11823
		[SerializeField]
		private float m_StartDistanceMin = 2f;

		// Token: 0x04002E30 RID: 11824
		[SerializeField]
		private float m_StartDistanceMax = 64f;

		// Token: 0x04002E31 RID: 11825
		[SerializeField]
		private float m_MoveMin = 32f;

		// Token: 0x04002E32 RID: 11826
		[SerializeField]
		private float m_MoveMax = 128f;

		// Token: 0x04002E33 RID: 11827
		[SerializeField]
		private float m_MoveSec = 1f;

		// Token: 0x04002E34 RID: 11828
		[SerializeField]
		private float m_Delay = 0.1f;

		// Token: 0x04002E35 RID: 11829
		[SerializeField]
		private bool m_AutoStart;

		// Token: 0x04002E36 RID: 11830
		[SerializeField]
		private bool m_Loop;

		// Token: 0x04002E37 RID: 11831
		private bool m_IsDonePrepare;

		// Token: 0x04002E38 RID: 11832
		private RandomStar[] m_RandomStars;

		// Token: 0x04002E39 RID: 11833
		private RectTransform m_RectTransform;

		// Token: 0x04002E3A RID: 11834
		private bool m_IsReservePlay;
	}
}
