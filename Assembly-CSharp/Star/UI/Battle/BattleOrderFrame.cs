﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000758 RID: 1880
	public class BattleOrderFrame : BattleOrderFrameBase
	{
		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06002566 RID: 9574 RVA: 0x000C8957 File Offset: 0x000C6D57
		// (set) Token: 0x06002567 RID: 9575 RVA: 0x000C895F File Offset: 0x000C6D5F
		public CharacterHandler CharaHandler { get; set; }

		// Token: 0x06002568 RID: 9576 RVA: 0x000C8968 File Offset: 0x000C6D68
		public override bool IsChara()
		{
			return true;
		}

		// Token: 0x06002569 RID: 9577 RVA: 0x000C896B File Offset: 0x000C6D6B
		public void Setup(StateIconOwner stateIconOwner)
		{
			base.Setup();
			this.m_AbnormalIcon.SetStateIconOwner(stateIconOwner);
			this.m_AbnormalIcon.Setup();
			this.SetTargetCursor(false);
		}

		// Token: 0x0600256A RID: 9578 RVA: 0x000C8991 File Offset: 0x000C6D91
		public override void Destroy()
		{
			base.Destroy();
			this.m_OrderIcon.Destroy();
		}

		// Token: 0x0600256B RID: 9579 RVA: 0x000C89A4 File Offset: 0x000C6DA4
		public void ApplyPL(int charaID)
		{
			this.m_OrderIcon.ApplyPL(charaID);
			this.m_BaseColorGroup.ChangeColor(this.PLAYER_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
			this.m_IsPlayer = true;
		}

		// Token: 0x0600256C RID: 9580 RVA: 0x000C89D0 File Offset: 0x000C6DD0
		public void ApplyEN(int resourceID)
		{
			this.m_OrderIcon.ApplyEN(resourceID);
			this.m_BaseColorGroup.ChangeColor(this.ENEMY_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
			this.m_IsPlayer = false;
		}

		// Token: 0x0600256D RID: 9581 RVA: 0x000C89FC File Offset: 0x000C6DFC
		public void SetTargetCursor(bool flg)
		{
			this.m_TargetObj.SetActive(flg);
		}

		// Token: 0x0600256E RID: 9582 RVA: 0x000C8A0A File Offset: 0x000C6E0A
		public void SetType(BattleDefine.eJoinMember member)
		{
			if (member == BattleDefine.eJoinMember.None)
			{
				this.m_Type.enabled = false;
				return;
			}
			this.m_Type.enabled = true;
			this.m_Type.sprite = this.m_TypeSprite[(int)member];
		}

		// Token: 0x0600256F RID: 9583 RVA: 0x000C8A40 File Offset: 0x000C6E40
		public void SetStunIcon(bool flg)
		{
			this.m_StunIconObj.SetActive(flg);
			this.m_IsStun = flg;
			if (flg)
			{
				this.m_AbnormalIcon.gameObject.SetActive(false);
			}
			else
			{
				this.m_AbnormalIcon.gameObject.SetActive(this.m_IsAbnormal);
			}
			this.m_DarkObj.SetActive(this.m_IsStun || this.m_IsAbnormal);
		}

		// Token: 0x06002570 RID: 9584 RVA: 0x000C8AB1 File Offset: 0x000C6EB1
		public void ClearAbnormalIcon()
		{
			this.m_AbnormalIcon.Clear();
			this.m_DarkObj.SetActive(this.m_IsStun || this.m_IsAbnormal);
		}

		// Token: 0x06002571 RID: 9585 RVA: 0x000C8AE0 File Offset: 0x000C6EE0
		public void SetAbnormal(bool[] abnormal)
		{
			this.ClearAbnormalIcon();
			this.m_IsAbnormal = false;
			for (int i = 0; i < abnormal.Length; i++)
			{
				if (abnormal[i])
				{
					this.m_IsAbnormal = true;
				}
				this.m_AbnormalIcon.gameObject.SetActive(true);
				this.m_AbnormalIcon.SetIcon(eStateIconType.Confusion + i, abnormal[i]);
			}
			this.m_DarkObj.SetActive(this.m_IsStun || this.m_IsAbnormal);
		}

		// Token: 0x06002572 RID: 9586 RVA: 0x000C8B5E File Offset: 0x000C6F5E
		public void SetInteractableFrame(bool flg)
		{
			if (flg)
			{
				this.m_ColorGroup.ChangeColor(Color.gray, ColorGroup.eColorBlendMode.Mul, 1f);
			}
			else
			{
				this.m_ColorGroup.RevertColor();
			}
		}

		// Token: 0x04002C16 RID: 11286
		[SerializeField]
		private Sprite[] m_TypeSprite;

		// Token: 0x04002C17 RID: 11287
		[SerializeField]
		private OrderIcon m_OrderIcon;

		// Token: 0x04002C18 RID: 11288
		[SerializeField]
		private Image m_Type;

		// Token: 0x04002C19 RID: 11289
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x04002C1A RID: 11290
		[SerializeField]
		private GameObject m_TargetObj;

		// Token: 0x04002C1B RID: 11291
		[SerializeField]
		private GameObject m_DarkObj;

		// Token: 0x04002C1C RID: 11292
		[SerializeField]
		private GameObject m_StunIconObj;

		// Token: 0x04002C1D RID: 11293
		[SerializeField]
		private BuffIcon m_AbnormalIcon;

		// Token: 0x04002C1E RID: 11294
		private bool m_IsAbnormal;

		// Token: 0x04002C1F RID: 11295
		private bool m_IsStun;
	}
}
