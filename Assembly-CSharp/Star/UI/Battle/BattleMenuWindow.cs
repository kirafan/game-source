﻿using System;
using Star.UI.Menu;
using Star.UI.Town;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x02000754 RID: 1876
	public class BattleMenuWindow : MonoBehaviour
	{
		// Token: 0x1400003A RID: 58
		// (add) Token: 0x0600254B RID: 9547 RVA: 0x000C7820 File Offset: 0x000C5C20
		// (remove) Token: 0x0600254C RID: 9548 RVA: 0x000C7858 File Offset: 0x000C5C58
		public event Action OnCancel;

		// Token: 0x1400003B RID: 59
		// (add) Token: 0x0600254D RID: 9549 RVA: 0x000C7890 File Offset: 0x000C5C90
		// (remove) Token: 0x0600254E RID: 9550 RVA: 0x000C78C8 File Offset: 0x000C5CC8
		public event Action OnRetire;

		// Token: 0x0600254F RID: 9551 RVA: 0x000C7900 File Offset: 0x000C5D00
		public void Setup(BattleOption battleOption)
		{
			this.m_BattleOption = battleOption;
			this.m_StateController.Setup(5, 0, false);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 2, true);
			this.m_StateController.AddTransit(2, 3, true);
			this.m_StateController.AddTransit(3, 0, true);
			this.m_StateController.AddTransitEach(2, 4, true);
			for (int i = 1; i < 45; i++)
			{
				BattleHelpIconDescript battleHelpIconDescript = UnityEngine.Object.Instantiate<BattleHelpIconDescript>(this.m_BuffIconDescriptPrefab, this.m_IconDescriptRoot, false);
				battleHelpIconDescript.Setup(this.m_StateIconOwner);
				battleHelpIconDescript.Apply((eStateIconType)i, i != 1);
			}
			this.m_OptionGroup.OnClose += this.OnCloseOptionGroupCallBack;
			this.m_BuffWindow.OnClose += this.OnCloseTownBuffCallBack;
		}

		// Token: 0x06002550 RID: 9552 RVA: 0x000C79D8 File Offset: 0x000C5DD8
		private void Update()
		{
			BattleMenuWindow.eState nowState = (BattleMenuWindow.eState)this.m_StateController.NowState;
			if (nowState != BattleMenuWindow.eState.Hide)
			{
				if (nowState != BattleMenuWindow.eState.In)
				{
					if (nowState == BattleMenuWindow.eState.Out)
					{
						bool flag = true;
						for (int i = 0; i < this.m_Anims.Length; i++)
						{
							if (!this.m_Anims[i].IsEnd)
							{
								flag = false;
								break;
							}
						}
						if (flag && this.m_MainGroup.IsPlayingInOut)
						{
							flag = false;
						}
						if (flag)
						{
							this.SetState(BattleMenuWindow.eState.Hide);
							base.gameObject.SetActive(false);
							BattleMenuWindow.eButton button = this.m_Button;
							if (button != BattleMenuWindow.eButton.Close)
							{
								if (button == BattleMenuWindow.eButton.Retire)
								{
									this.OnRetire();
								}
							}
							else
							{
								this.OnCancel();
							}
						}
					}
				}
				else
				{
					bool flag2 = true;
					for (int j = 0; j < this.m_Anims.Length; j++)
					{
						if (!this.m_Anims[j].IsEnd)
						{
							flag2 = false;
							break;
						}
					}
					if (flag2 && this.m_MainGroup.IsPlayingInOut)
					{
						flag2 = false;
					}
					if (flag2)
					{
						this.SetState(BattleMenuWindow.eState.Wait);
					}
				}
			}
		}

		// Token: 0x06002551 RID: 9553 RVA: 0x000C7B1C File Offset: 0x000C5F1C
		private void SetState(BattleMenuWindow.eState state)
		{
			this.m_StateController.ForceChangeState((int)state);
			if (state == BattleMenuWindow.eState.Wait)
			{
				this.m_MainGroup.SetEnableInput(true);
			}
		}

		// Token: 0x06002552 RID: 9554 RVA: 0x000C7B54 File Offset: 0x000C5F54
		public void PlayIn()
		{
			if (this.m_StateController.ChangeState(1))
			{
				for (int i = 0; i < this.m_Anims.Length; i++)
				{
					this.m_Anims[i].PlayIn();
				}
				base.gameObject.SetActive(true);
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x06002553 RID: 9555 RVA: 0x000C7BB0 File Offset: 0x000C5FB0
		public void PlayOut()
		{
			if (this.m_StateController.ChangeState(3))
			{
				for (int i = 0; i < this.m_Anims.Length; i++)
				{
					this.m_Anims[i].PlayOut();
				}
				this.m_MainGroup.Close();
			}
		}

		// Token: 0x06002554 RID: 9556 RVA: 0x000C7BFF File Offset: 0x000C5FFF
		public void OnClickCloseButtonCallBack()
		{
			this.m_Button = BattleMenuWindow.eButton.Close;
			this.PlayOut();
		}

		// Token: 0x06002555 RID: 9557 RVA: 0x000C7C0E File Offset: 0x000C600E
		public void OnClickRetireButtonCallBack()
		{
			this.m_Button = BattleMenuWindow.eButton.Retire;
			this.PlayOut();
		}

		// Token: 0x06002556 RID: 9558 RVA: 0x000C7C1D File Offset: 0x000C601D
		public void OnClickOpenHelpButtonCallBack()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainGroup.Close();
				this.m_HelpGroup.Open();
			}
		}

		// Token: 0x06002557 RID: 9559 RVA: 0x000C7C46 File Offset: 0x000C6046
		public void OnClickCloseHelpButtonCallBack()
		{
			if (this.m_StateController.ChangeState(2))
			{
				this.m_HelpGroup.Close();
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x06002558 RID: 9560 RVA: 0x000C7C6F File Offset: 0x000C606F
		public void OnClickOpenOptionButtonCallBack()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainGroup.Close();
				this.m_OptionGroup.Open(this.m_BattleOption);
			}
		}

		// Token: 0x06002559 RID: 9561 RVA: 0x000C7C9E File Offset: 0x000C609E
		public void OnCloseOptionGroupCallBack()
		{
			if (this.m_StateController.ChangeState(2))
			{
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x0600255A RID: 9562 RVA: 0x000C7CBC File Offset: 0x000C60BC
		public void OnClickOpenTownBuffButtonCallBack()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainGroup.Close();
				this.m_BuffWindow.Open();
			}
		}

		// Token: 0x0600255B RID: 9563 RVA: 0x000C7CE5 File Offset: 0x000C60E5
		public void OnCloseTownBuffCallBack()
		{
			if (this.m_StateController.ChangeState(2))
			{
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x04002BF7 RID: 11255
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04002BF8 RID: 11256
		[SerializeField]
		private AnimUIPlayer[] m_Anims;

		// Token: 0x04002BF9 RID: 11257
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04002BFA RID: 11258
		[SerializeField]
		private UIGroup m_HelpGroup;

		// Token: 0x04002BFB RID: 11259
		[SerializeField]
		private OptionWindow m_OptionGroup;

		// Token: 0x04002BFC RID: 11260
		[SerializeField]
		private TownBufWindow m_BuffWindow;

		// Token: 0x04002BFD RID: 11261
		[SerializeField]
		private StateIconOwner m_StateIconOwner;

		// Token: 0x04002BFE RID: 11262
		[SerializeField]
		private RectTransform m_IconDescriptRoot;

		// Token: 0x04002BFF RID: 11263
		[SerializeField]
		private BattleHelpIconDescriptTitle m_IconDescriptTitlePrefab;

		// Token: 0x04002C00 RID: 11264
		[SerializeField]
		private BattleHelpIconDescript m_BuffIconDescriptPrefab;

		// Token: 0x04002C01 RID: 11265
		private BattleMenuWindow.eButton m_Button;

		// Token: 0x04002C04 RID: 11268
		private BattleOption m_BattleOption;

		// Token: 0x02000755 RID: 1877
		private enum eState
		{
			// Token: 0x04002C06 RID: 11270
			Hide,
			// Token: 0x04002C07 RID: 11271
			In,
			// Token: 0x04002C08 RID: 11272
			Wait,
			// Token: 0x04002C09 RID: 11273
			Out,
			// Token: 0x04002C0A RID: 11274
			Window,
			// Token: 0x04002C0B RID: 11275
			Num
		}

		// Token: 0x02000756 RID: 1878
		private enum eButton
		{
			// Token: 0x04002C0D RID: 11277
			None,
			// Token: 0x04002C0E RID: 11278
			Close,
			// Token: 0x04002C0F RID: 11279
			Retire
		}
	}
}
