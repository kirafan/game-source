﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200074F RID: 1871
	public class BattleFade : MonoBehaviour
	{
		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06002534 RID: 9524 RVA: 0x000C70A8 File Offset: 0x000C54A8
		public BattleFade.eState state
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06002535 RID: 9525 RVA: 0x000C70B0 File Offset: 0x000C54B0
		public bool IsPlaying
		{
			get
			{
				return this.m_IsPlaying;
			}
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x000C70B8 File Offset: 0x000C54B8
		public void SetColor(Color color)
		{
			for (int i = 0; i < this.m_ColorChangeTargets.Length; i++)
			{
				this.m_ColorChangeTargets[i].color = color;
			}
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x000C70EC File Offset: 0x000C54EC
		public void Show()
		{
			this.m_IsEnable = true;
			if (this.m_State == BattleFade.eState.Hide)
			{
				this.m_IsPlaying = true;
				this.PlayIn();
			}
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x000C710D File Offset: 0x000C550D
		public void Hide()
		{
			this.m_IsEnable = false;
			if (this.m_State == BattleFade.eState.Wait)
			{
				this.m_IsPlaying = true;
				this.PlayOut();
			}
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x000C7130 File Offset: 0x000C5530
		public void ShowQuick()
		{
			this.m_GameObject = base.gameObject;
			this.m_GameObject.SetActive(true);
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_RectTransform.localPosition = new Vector3(0f, 0f, 0f);
			this.m_IsPlaying = false;
			this.m_State = BattleFade.eState.Wait;
		}

		// Token: 0x0600253A RID: 9530 RVA: 0x000C7190 File Offset: 0x000C5590
		private void PlayIn()
		{
			this.m_GameObject = base.gameObject;
			this.m_GameObject.SetActive(true);
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_State = BattleFade.eState.In;
			if (!this.m_Vertical)
			{
				this.m_RectTransform.localPosition = new Vector3(this.m_RectTransform.rect.width, 0f, 0f);
				iTween.Stop(this.m_GameObject, "move");
				Hashtable hashtable = new Hashtable();
				hashtable.Add("isLocal", true);
				hashtable.Add("x", 0f);
				hashtable.Add("y", 0f);
				hashtable.Add("time", 0.2f);
				hashtable.Add("easeType", iTween.EaseType.linear);
				hashtable.Add("delay", 0.001f);
				hashtable.Add("oncomplete", "OnCompleteInMove");
				hashtable.Add("oncompletetarget", this.m_GameObject);
				iTween.MoveTo(this.m_GameObject, hashtable);
			}
			else
			{
				this.m_RectTransform.localPosition = new Vector3(0f, -this.m_RectTransform.rect.height, 0f);
				iTween.Stop(this.m_GameObject, "move");
				Hashtable hashtable2 = new Hashtable();
				hashtable2.Add("isLocal", true);
				hashtable2.Add("x", 0f);
				hashtable2.Add("y", 0f);
				hashtable2.Add("time", 0.2f);
				hashtable2.Add("easeType", iTween.EaseType.linear);
				hashtable2.Add("delay", 0.001f);
				hashtable2.Add("oncomplete", "OnCompleteInMove");
				hashtable2.Add("oncompletetarget", this.m_GameObject);
				iTween.MoveTo(this.m_GameObject, hashtable2);
			}
		}

		// Token: 0x0600253B RID: 9531 RVA: 0x000C73A7 File Offset: 0x000C57A7
		public void OnCompleteInMove()
		{
			this.m_State = BattleFade.eState.Wait;
			this.m_IsPlaying = false;
			if (!this.m_IsEnable && this.m_State == BattleFade.eState.Wait)
			{
				this.PlayOut();
			}
		}

		// Token: 0x0600253C RID: 9532 RVA: 0x000C73D4 File Offset: 0x000C57D4
		private void PlayOut()
		{
			this.m_GameObject = base.gameObject;
			this.m_GameObject.SetActive(true);
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_State = BattleFade.eState.In;
			this.m_RectTransform.localPosition = new Vector3(0f, 0f, 0f);
			if (!this.m_Vertical)
			{
				iTween.Stop(this.m_GameObject, "move");
				Hashtable hashtable = new Hashtable();
				hashtable.Add("isLocal", true);
				hashtable.Add("x", -this.m_RectTransform.rect.width);
				hashtable.Add("y", 0f);
				hashtable.Add("time", 0.2f);
				hashtable.Add("easeType", iTween.EaseType.linear);
				hashtable.Add("delay", 0.001f);
				hashtable.Add("oncomplete", "OnCompleteOutMove");
				hashtable.Add("oncompletetarget", this.m_GameObject);
				iTween.MoveTo(this.m_GameObject, hashtable);
			}
			else
			{
				iTween.Stop(this.m_GameObject, "move");
				Hashtable hashtable2 = new Hashtable();
				hashtable2.Add("isLocal", true);
				hashtable2.Add("x", 0f);
				hashtable2.Add("y", this.m_RectTransform.rect.height);
				hashtable2.Add("time", 0.2f);
				hashtable2.Add("easeType", iTween.EaseType.linear);
				hashtable2.Add("delay", 0.001f);
				hashtable2.Add("oncomplete", "OnCompleteOutMove");
				hashtable2.Add("oncompletetarget", this.m_GameObject);
				iTween.MoveTo(this.m_GameObject, hashtable2);
			}
		}

		// Token: 0x0600253D RID: 9533 RVA: 0x000C75CC File Offset: 0x000C59CC
		public void OnCompleteOutMove()
		{
			this.m_State = BattleFade.eState.Hide;
			this.m_IsPlaying = false;
			if (this.m_IsEnable)
			{
				if (this.m_State == BattleFade.eState.Hide)
				{
					this.PlayIn();
				}
			}
			else
			{
				this.m_GameObject.SetActive(false);
			}
		}

		// Token: 0x04002BDE RID: 11230
		private const float FADE_SEC = 0.2f;

		// Token: 0x04002BDF RID: 11231
		private BattleFade.eState m_State;

		// Token: 0x04002BE0 RID: 11232
		private bool m_IsEnable = true;

		// Token: 0x04002BE1 RID: 11233
		private bool m_IsPlaying;

		// Token: 0x04002BE2 RID: 11234
		private RectTransform m_RectTransform;

		// Token: 0x04002BE3 RID: 11235
		private GameObject m_GameObject;

		// Token: 0x04002BE4 RID: 11236
		[SerializeField]
		private Image[] m_ColorChangeTargets;

		// Token: 0x04002BE5 RID: 11237
		[SerializeField]
		private bool m_Vertical;

		// Token: 0x02000750 RID: 1872
		public enum eState
		{
			// Token: 0x04002BE7 RID: 11239
			Hide,
			// Token: 0x04002BE8 RID: 11240
			In,
			// Token: 0x04002BE9 RID: 11241
			Wait,
			// Token: 0x04002BEA RID: 11242
			Out
		}
	}
}
