﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200078B RID: 1931
	public class RecastGauge : MonoBehaviour
	{
		// Token: 0x06002763 RID: 10083 RVA: 0x000D210B File Offset: 0x000D050B
		public void SetOwner(CommandButton owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06002764 RID: 10084 RVA: 0x000D2114 File Offset: 0x000D0514
		public void Apply(int value, int max, int valueTo)
		{
			if (value <= 0 && valueTo > 0)
			{
				value = max;
			}
			this.m_CurrentValue = (float)value;
			this.m_ValueTo = (float)valueTo;
			this.m_Max = (float)max;
			if (value > 0 || valueTo > 0)
			{
				base.gameObject.SetActive(true);
				this.m_IsPlaying = true;
			}
			else
			{
				base.gameObject.SetActive(false);
				this.m_IsPlaying = false;
			}
			this.Apply(this.m_CurrentValue, this.m_Max);
		}

		// Token: 0x06002765 RID: 10085 RVA: 0x000D2195 File Offset: 0x000D0595
		private void Apply(float value, float max)
		{
			if (value > 0f)
			{
				this.m_GaugeImage.fillAmount = 1f - value / max;
			}
		}

		// Token: 0x06002766 RID: 10086 RVA: 0x000D21B8 File Offset: 0x000D05B8
		private void Update()
		{
			if (this.m_ValueTo != this.m_CurrentValue)
			{
				if (this.m_CurrentValue > this.m_ValueTo)
				{
					this.m_CurrentValue -= 0.2f * this.m_Max * Time.deltaTime;
					if (this.m_CurrentValue <= this.m_ValueTo)
					{
						this.m_CurrentValue = this.m_ValueTo;
						if (this.m_CurrentValue <= 0f)
						{
							base.gameObject.SetActive(false);
							this.m_Owner.PlayRecoverAnim();
						}
						this.m_IsPlaying = false;
					}
					this.Apply(this.m_CurrentValue, this.m_Max);
				}
				else if (this.m_CurrentValue < this.m_ValueTo)
				{
					this.m_CurrentValue += 0.2f * this.m_Max * Time.deltaTime;
					if (this.m_CurrentValue >= this.m_ValueTo)
					{
						this.m_CurrentValue = this.m_ValueTo;
						if (this.m_CurrentValue <= 0f)
						{
							base.gameObject.SetActive(false);
							this.m_Owner.PlayRecoverAnim();
						}
						this.m_IsPlaying = false;
					}
					this.Apply(this.m_CurrentValue, this.m_Max);
				}
			}
		}

		// Token: 0x06002767 RID: 10087 RVA: 0x000D22F5 File Offset: 0x000D06F5
		public bool IsPlaying()
		{
			return this.m_IsPlaying;
		}

		// Token: 0x04002E3B RID: 11835
		private const float GAUGE_SPEED = 0.2f;

		// Token: 0x04002E3C RID: 11836
		[SerializeField]
		private Image m_GaugeImage;

		// Token: 0x04002E3D RID: 11837
		private float m_CurrentValue;

		// Token: 0x04002E3E RID: 11838
		private float m_ValueTo;

		// Token: 0x04002E3F RID: 11839
		private float m_Max;

		// Token: 0x04002E40 RID: 11840
		private bool m_IsPlaying;

		// Token: 0x04002E41 RID: 11841
		private CommandButton m_Owner;
	}
}
