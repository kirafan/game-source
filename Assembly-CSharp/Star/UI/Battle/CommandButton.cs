﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200076B RID: 1899
	public class CommandButton : MonoBehaviour
	{
		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06002692 RID: 9874 RVA: 0x000CD44E File Offset: 0x000CB84E
		public int CommandIndex
		{
			get
			{
				return this.m_CommandIndex;
			}
		}

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06002693 RID: 9875 RVA: 0x000CD456 File Offset: 0x000CB856
		public RectTransform CacheTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06002694 RID: 9876 RVA: 0x000CD45E File Offset: 0x000CB85E
		public AnimUIPlayer Anim
		{
			get
			{
				return this.m_Anim;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06002695 RID: 9877 RVA: 0x000CD466 File Offset: 0x000CB866
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInput;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06002696 RID: 9878 RVA: 0x000CD46E File Offset: 0x000CB86E
		// (set) Token: 0x06002697 RID: 9879 RVA: 0x000CD476 File Offset: 0x000CB876
		private bool IsEnableBlink
		{
			get
			{
				return this.m_IsEnableBlink;
			}
			set
			{
				this.m_IsEnableBlink = value;
				this.UpdateAnimState();
			}
		}

		// Token: 0x1400004F RID: 79
		// (add) Token: 0x06002698 RID: 9880 RVA: 0x000CD488 File Offset: 0x000CB888
		// (remove) Token: 0x06002699 RID: 9881 RVA: 0x000CD4C0 File Offset: 0x000CB8C0
		public event Action<int, bool, bool> OnClickDecideButton;

		// Token: 0x0600269A RID: 9882 RVA: 0x000CD4F8 File Offset: 0x000CB8F8
		public void Setup(CommandSet owner, BattleTutorial tutorial)
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_Anim = this.m_GameObject.GetComponent<AnimUIPlayer>();
			this.m_ButtonImageColorGroup = this.m_ButtonImage.GetComponent<ColorGroup>();
			this.m_Owner = owner;
			this.m_Mode = CommandButton.eMode.None;
			this.m_Tutorial = tutorial;
			this.m_RecastGauge.SetOwner(this);
			this.m_RecastGauge.Apply(0, 0, 0);
			this.m_SelectAnimObj.SetActive(false);
			this.m_DecidedAnimObj.SetActive(false);
			this.m_SilenceImageObj.SetActive(false);
			this.m_IsEnableDecide = false;
			this.m_PressObj.SetActive(false);
		}

		// Token: 0x0600269B RID: 9883 RVA: 0x000CD5AA File Offset: 0x000CB9AA
		public void SetEnableRenderFlickArrowLeft(bool flg)
		{
			if (this.m_FlickArrowLeft.activeSelf != flg)
			{
				this.m_FlickArrowLeft.SetActive(flg);
			}
		}

		// Token: 0x0600269C RID: 9884 RVA: 0x000CD5C9 File Offset: 0x000CB9C9
		public void SetEnableRenderFlickArrowRight(bool flg)
		{
			if (this.m_FlickArrowRight.activeSelf != flg)
			{
				this.m_FlickArrowRight.SetActive(flg);
			}
		}

		// Token: 0x0600269D RID: 9885 RVA: 0x000CD5E8 File Offset: 0x000CB9E8
		private void Update()
		{
			if (!this.m_Button.IsEnableInput && !this.m_IsRecast && this.m_IsEnableInput && this.m_Anim.State == 2)
			{
				this.m_Button.IsEnableInput = true;
			}
			this.SetInteractable(this.m_IsInteractable && !this.m_RecastGauge.IsPlaying());
			if (this.m_IsScaleUp && InputTouch.Inst.GetAvailableTouchCnt() == 0)
			{
				this.ScaleRevert();
			}
		}

		// Token: 0x0600269E RID: 9886 RVA: 0x000CD67C File Offset: 0x000CBA7C
		public void UpdateStatus()
		{
			if (this.m_ReserveStatus != null)
			{
				if (base.gameObject.activeInHierarchy && (this.m_Anim.State == 2 || this.m_Anim.State == 1))
				{
					this.m_Anim.PlayOut();
				}
				this.IsEnableBlink = false;
			}
		}

		// Token: 0x0600269F RID: 9887 RVA: 0x000CD6D8 File Offset: 0x000CBAD8
		public bool IsReadyApplyReserve()
		{
			return this.m_ReserveStatus == null || (!base.gameObject.activeInHierarchy || this.m_Anim.State == 0);
		}

		// Token: 0x060026A0 RID: 9888 RVA: 0x000CD70C File Offset: 0x000CBB0C
		public void ApplyReserve()
		{
			if (this.m_ReserveStatus != null && (!base.gameObject.activeInHierarchy || this.m_Anim.State == 0))
			{
				this.m_IsInteractable = (!this.m_ReserveStatus.m_IsSilence && this.m_ReserveStatus.m_RecastValue <= 0);
				this.m_ButtonImage.sprite = this.m_ReserveStatus.m_Sprite;
				if (this.m_ReserveStatus.m_Sprite == null)
				{
					this.m_ButtonImage.enabled = false;
					this.m_BlankObj.SetActive(true);
				}
				else
				{
					this.m_ButtonImage.enabled = true;
					this.m_BlankObj.SetActive(false);
				}
				this.m_Anim.PlayIn();
				if (this.m_ReserveStatus.m_Elements != null)
				{
					if (this.m_ReserveStatus.m_Elements.Count > 0)
					{
						this.m_ButtonImageColorGroup.ChangeColor(UIUtility.GetIconColor(this.m_ReserveStatus.m_Elements[0]), ColorGroup.eColorBlendMode.Mul, 1f);
					}
					else
					{
						this.m_ButtonImageColorGroup.ChangeColor(Color.white, ColorGroup.eColorBlendMode.Mul, 1f);
					}
					for (int i = 0; i < this.m_ElementIconObjs.Length; i++)
					{
						if (this.m_ReserveStatus.m_Elements.Count <= i)
						{
							this.m_ElementIconObjs[i].SetActive(false);
							this.m_ElementIconBaseObjs[i].SetActive(false);
						}
						else
						{
							this.m_ElementIconObjs[i].SetActive(true);
							this.m_ElementIconBaseObjs[i].SetActive(true);
							this.m_ElementIcons[i].Apply(this.m_ReserveStatus.m_Elements[i]);
						}
					}
				}
				else
				{
					this.m_ButtonImageColorGroup.ChangeColor(Color.white, ColorGroup.eColorBlendMode.Mul, 1f);
					for (int j = 0; j < this.m_ElementIconObjs.Length; j++)
					{
						this.m_ElementIconObjs[j].SetActive(false);
						this.m_ElementIconBaseObjs[j].SetActive(false);
					}
				}
				this.SetSilence(this.m_ReserveStatus.m_IsSilence);
				this.m_RecastGauge.Apply(this.m_Owner.GetRecastSaveData(this.m_ReserveStatus.m_CharaBattle, this.m_CommandIndex), this.m_ReserveStatus.m_RecastMax, this.m_ReserveStatus.m_RecastValue);
				this.m_Owner.SetRecastSaveData(this.m_ReserveStatus.m_CharaBattle, this.m_CommandIndex, this.m_ReserveStatus.m_RecastValue);
				this.m_RecoverAnimObj.SetActive(false);
				this.IsEnableBlink = true;
				this.m_ReserveStatus = null;
			}
		}

		// Token: 0x060026A1 RID: 9889 RVA: 0x000CD9AB File Offset: 0x000CBDAB
		public void PlayRecoverAnim()
		{
			this.m_Owner.RequestRecastRecoverSound();
			this.m_RecoverAnimObj.SetActive(true);
		}

		// Token: 0x060026A2 RID: 9890 RVA: 0x000CD9C4 File Offset: 0x000CBDC4
		public void SetCommandIndex(int commandIndex)
		{
			this.m_CommandIndex = commandIndex;
		}

		// Token: 0x060026A3 RID: 9891 RVA: 0x000CD9CD File Offset: 0x000CBDCD
		public void Hide()
		{
			this.m_ReserveStatus = null;
			this.SetEnableRender(false);
		}

		// Token: 0x060026A4 RID: 9892 RVA: 0x000CD9E0 File Offset: 0x000CBDE0
		public void SetupBlank()
		{
			if (this.m_GameObject.activeInHierarchy && !this.m_ButtonImage.enabled)
			{
				return;
			}
			this.SetEnableInput(false);
			if (this.m_IsNone)
			{
				this.m_ReserveStatus = new CommandButton.Status();
				this.m_ReserveStatus.m_Sprite = null;
			}
			else
			{
				this.m_IsNone = false;
				for (int i = 0; i < this.m_ElementIconObjs.Length; i++)
				{
					this.m_ElementIconObjs[i].SetActive(false);
					this.m_ElementIconBaseObjs[i].SetActive(false);
				}
				this.m_ReserveStatus = new CommandButton.Status();
				this.m_ReserveStatus.m_Display = true;
				this.m_ReserveStatus.m_Sprite = null;
				this.m_ReserveStatus.m_Elements = null;
				this.m_ReserveStatus.m_IsSilence = false;
				this.m_ReserveStatus.m_RecastValue = 0;
				this.m_ReserveStatus.m_RecastMax = 0;
				this.m_ReserveStatus.m_CharaBattle = null;
				this.SetRecastGauge(0, 0, false, null);
			}
			this.SetMode(CommandButton.eMode.None);
		}

		// Token: 0x060026A5 RID: 9893 RVA: 0x000CDAE8 File Offset: 0x000CBEE8
		public void SetupNormalAttack(eSkillType type, List<eElementType> elements)
		{
			this.SetupSkill(type, elements, eClassType.None, false);
		}

		// Token: 0x060026A6 RID: 9894 RVA: 0x000CDAF4 File Offset: 0x000CBEF4
		public void SetupSkill(eSkillType type, List<eElementType> elements, eClassType classType, bool isSilence)
		{
			this.m_IsNone = false;
			this.m_ReserveStatus = new CommandButton.Status();
			for (int i = 0; i < this.m_ElementIconObjs.Length; i++)
			{
				this.m_ElementIconObjs[i].SetActive(false);
				this.m_ElementIconBaseObjs[i].SetActive(false);
			}
			this.m_ReserveStatus.m_Display = true;
			if (type == eSkillType.NormalAttack_atk)
			{
				this.m_ReserveStatus.m_Sprite = this.m_Owner.GetAttackSprite();
			}
			else if (type == eSkillType.NormalAttack_mgc)
			{
				this.m_ReserveStatus.m_Sprite = this.m_Owner.GetMagicAttackSprite();
			}
			else
			{
				this.m_ReserveStatus.m_Sprite = this.m_Owner.GetSkillSprite(type);
			}
			this.m_ReserveStatus.m_Elements = elements;
			this.m_ReserveStatus.m_IsSilence = isSilence;
			this.m_ReserveStatus.m_RecastValue = 0;
			this.m_ReserveStatus.m_RecastMax = 0;
			this.m_ReserveStatus.m_CharaBattle = null;
			this.SetRecastGauge(0, 0, true, null);
			this.SetMode(CommandButton.eMode.None);
		}

		// Token: 0x060026A7 RID: 9895 RVA: 0x000CDBFC File Offset: 0x000CBFFC
		public void PlayOut()
		{
			if (this.m_Anim.State == 0 || this.m_Anim.State == 3)
			{
				return;
			}
			this.m_IsNone = true;
			this.SetEnableInput(false);
			this.m_ReserveStatus = null;
			this.SetRecastGauge(0, 0, true, null);
			this.m_Anim.PlayOut();
			this.SetMode(CommandButton.eMode.None);
		}

		// Token: 0x060026A8 RID: 9896 RVA: 0x000CDC5C File Offset: 0x000CC05C
		public void OnPointerDownCallBack(BaseEventData eventData)
		{
			if (((PointerEventData)eventData).pointerId > 0)
			{
				return;
			}
			if (this.m_Anim.State != 2)
			{
				return;
			}
			if (!this.m_IsEnableInput)
			{
				return;
			}
			if (this.m_Mode == CommandButton.eMode.None)
			{
				this.m_Tutorial.ClearCommandBtnIndexHistory();
				this.m_Tutorial.AddCommandBtnIndexHistory(this.m_CommandIndex);
				this.OnClickDecideButton.Call(this.m_CommandIndex, true, this.m_IsSilence);
				this.m_IsEnableDecide = false;
			}
			else
			{
				this.m_IsEnableDecide = true;
			}
			this.ScaleUp();
		}

		// Token: 0x060026A9 RID: 9897 RVA: 0x000CDCF4 File Offset: 0x000CC0F4
		public void OnPointerClickCallBack()
		{
			if (this.m_Anim.State != 2)
			{
				return;
			}
			if (!this.m_IsEnableInput)
			{
				return;
			}
			if (this.m_Tutorial.IsSeq(BattleTutorial.eSeq._03_WAVE_IDX_0_SWIPE))
			{
				return;
			}
			if (this.m_IsEnableDecide)
			{
				this.OnClickDecideButton.Call(this.m_CommandIndex, false, this.m_IsSilence);
			}
		}

		// Token: 0x060026AA RID: 9898 RVA: 0x000CDD54 File Offset: 0x000CC154
		public void OnPointerEnterCallBack(BaseEventData eventData)
		{
			if (((PointerEventData)eventData).pointerId > 0)
			{
				return;
			}
			if (this.m_Anim.State != 2)
			{
				return;
			}
			if (!this.m_IsEnableInput)
			{
				return;
			}
			if (InputTouch.Inst.GetAvailableTouchCnt() == 1)
			{
				if (this.m_Mode == CommandButton.eMode.None)
				{
					this.m_Tutorial.AddCommandBtnIndexHistory(this.m_CommandIndex);
					this.OnClickDecideButton.Call(this.m_CommandIndex, true, this.m_IsSilence);
					this.m_IsEnableDecide = false;
				}
				this.ScaleUp();
				this.m_Button.ForceEnableHold(true);
			}
		}

		// Token: 0x060026AB RID: 9899 RVA: 0x000CDDEE File Offset: 0x000CC1EE
		public void OnPointerUpCallBack(BaseEventData eventData)
		{
			this.ScaleRevert();
		}

		// Token: 0x060026AC RID: 9900 RVA: 0x000CDDF6 File Offset: 0x000CC1F6
		public void OnPointerExitCallBack(BaseEventData eventData)
		{
			this.ScaleRevert();
		}

		// Token: 0x060026AD RID: 9901 RVA: 0x000CDE00 File Offset: 0x000CC200
		public void ScaleUp()
		{
			this.m_PressObj.SetActive(true);
			this.m_IsScaleUp = true;
			iTween.Stop(this.m_SelectScaleObj, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", 1.075f);
			hashtable.Add("y", 1.075f);
			hashtable.Add("time", 0.2f);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			iTween.ScaleTo(this.m_SelectScaleObj, hashtable);
		}

		// Token: 0x060026AE RID: 9902 RVA: 0x000CDEA8 File Offset: 0x000CC2A8
		public void ScaleRevert()
		{
			this.m_PressObj.SetActive(false);
			this.m_IsScaleUp = false;
			iTween.Stop(this.m_SelectScaleObj, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", 1f);
			hashtable.Add("y", 1f);
			hashtable.Add("time", 0.2f);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			iTween.ScaleTo(this.m_SelectScaleObj, hashtable);
		}

		// Token: 0x060026AF RID: 9903 RVA: 0x000CDF4F File Offset: 0x000CC34F
		public void SetEnableRender(bool flg)
		{
			if (!flg && this.m_GameObject.activeInHierarchy)
			{
				this.m_Anim.Hide();
			}
			this.m_GameObject.SetActive(flg);
		}

		// Token: 0x060026B0 RID: 9904 RVA: 0x000CDF7E File Offset: 0x000CC37E
		public void SetEnableInput(bool flg)
		{
			this.m_IsEnableInput = flg;
			if (!flg)
			{
				this.m_Button.IsEnableInput = flg;
			}
		}

		// Token: 0x060026B1 RID: 9905 RVA: 0x000CDF99 File Offset: 0x000CC399
		public void SetInteractable(bool flg)
		{
			this.m_Button.Interactable = flg;
		}

		// Token: 0x060026B2 RID: 9906 RVA: 0x000CDFA7 File Offset: 0x000CC3A7
		public void SetSilence(bool flg)
		{
			this.m_IsSilence = flg;
			this.m_SilenceImageObj.SetActive(flg);
		}

		// Token: 0x060026B3 RID: 9907 RVA: 0x000CDFBC File Offset: 0x000CC3BC
		public void SetMode(CommandButton.eMode mode)
		{
			this.m_Mode = mode;
			if (mode == CommandButton.eMode.Select || mode == CommandButton.eMode.Decide)
			{
				this.m_RectTransform.SetAsLastSibling();
			}
			else
			{
				this.m_IsEnableDecide = false;
			}
			this.UpdateAnimState();
		}

		// Token: 0x060026B4 RID: 9908 RVA: 0x000CDFF0 File Offset: 0x000CC3F0
		private void UpdateAnimState()
		{
			if (this.m_IsEnableBlink)
			{
				CommandButton.eMode mode = this.m_Mode;
				if (mode != CommandButton.eMode.None)
				{
					if (mode != CommandButton.eMode.Select)
					{
						if (mode == CommandButton.eMode.Decide)
						{
							this.m_SelectAnimObj.SetActive(false);
							this.m_DecidedAnimObj.SetActive(true);
						}
					}
					else
					{
						this.m_SelectAnimObj.SetActive(true);
						this.m_DecidedAnimObj.SetActive(false);
					}
				}
				else
				{
					this.m_SelectAnimObj.SetActive(false);
					this.m_DecidedAnimObj.SetActive(false);
				}
			}
			else
			{
				this.m_SelectAnimObj.SetActive(false);
				this.m_DecidedAnimObj.SetActive(false);
			}
		}

		// Token: 0x060026B5 RID: 9909 RVA: 0x000CE09C File Offset: 0x000CC49C
		public void SetRecastGauge(int value, int max, bool recastRecoverdFlag, CharacterBattle charaBattle = null)
		{
			if (this.m_ReserveStatus != null)
			{
				this.m_ReserveStatus.m_CharaBattle = charaBattle;
				this.m_ReserveStatus.m_RecastValue = value;
				this.m_ReserveStatus.m_RecastMax = max;
				this.m_ReserveStatus.m_RecastRecover = recastRecoverdFlag;
				if (recastRecoverdFlag)
				{
					this.m_ReserveStatus.m_RecastValue = 0;
				}
			}
			if (value == 0)
			{
				this.m_IsRecast = false;
			}
			else
			{
				this.m_IsRecast = true;
			}
		}

		// Token: 0x04002D16 RID: 11542
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04002D17 RID: 11543
		[SerializeField]
		private Image m_ButtonImage;

		// Token: 0x04002D18 RID: 11544
		[SerializeField]
		private GameObject m_BlankObj;

		// Token: 0x04002D19 RID: 11545
		[SerializeField]
		private GameObject m_Recast;

		// Token: 0x04002D1A RID: 11546
		[SerializeField]
		private RecastGauge m_RecastGauge;

		// Token: 0x04002D1B RID: 11547
		[SerializeField]
		private GameObject m_SilenceImageObj;

		// Token: 0x04002D1C RID: 11548
		[SerializeField]
		private GameObject[] m_ElementIconObjs;

		// Token: 0x04002D1D RID: 11549
		[SerializeField]
		private GameObject[] m_ElementIconBaseObjs;

		// Token: 0x04002D1E RID: 11550
		[SerializeField]
		private ElementIcon[] m_ElementIcons;

		// Token: 0x04002D1F RID: 11551
		[SerializeField]
		private GameObject m_SelectAnimObj;

		// Token: 0x04002D20 RID: 11552
		[SerializeField]
		private GameObject m_DecidedAnimObj;

		// Token: 0x04002D21 RID: 11553
		[SerializeField]
		private GameObject m_RecoverAnimObj;

		// Token: 0x04002D22 RID: 11554
		[SerializeField]
		private GameObject m_SelectScaleObj;

		// Token: 0x04002D23 RID: 11555
		[SerializeField]
		private GameObject m_PressObj;

		// Token: 0x04002D24 RID: 11556
		[SerializeField]
		private GameObject m_FlickArrowLeft;

		// Token: 0x04002D25 RID: 11557
		[SerializeField]
		private GameObject m_FlickArrowRight;

		// Token: 0x04002D26 RID: 11558
		private int m_CommandIndex;

		// Token: 0x04002D27 RID: 11559
		private bool m_IsEnableInput;

		// Token: 0x04002D28 RID: 11560
		private bool m_IsInteractable;

		// Token: 0x04002D29 RID: 11561
		private CommandButton.eMode m_Mode;

		// Token: 0x04002D2A RID: 11562
		private bool m_IsNone = true;

		// Token: 0x04002D2B RID: 11563
		private bool m_IsRecast = true;

		// Token: 0x04002D2C RID: 11564
		private bool m_IsEnableBlink;

		// Token: 0x04002D2D RID: 11565
		private bool m_IsSilence;

		// Token: 0x04002D2E RID: 11566
		private bool m_IsEnableDecide;

		// Token: 0x04002D2F RID: 11567
		private bool m_IsScaleUp;

		// Token: 0x04002D30 RID: 11568
		private CommandSet m_Owner;

		// Token: 0x04002D31 RID: 11569
		private GameObject m_GameObject;

		// Token: 0x04002D32 RID: 11570
		private RectTransform m_RectTransform;

		// Token: 0x04002D33 RID: 11571
		private AnimUIPlayer m_Anim;

		// Token: 0x04002D34 RID: 11572
		private ColorGroup m_ButtonImageColorGroup;

		// Token: 0x04002D35 RID: 11573
		private CommandButton.Status m_ReserveStatus;

		// Token: 0x04002D36 RID: 11574
		private BattleTutorial m_Tutorial;

		// Token: 0x0200076C RID: 1900
		public enum eMode
		{
			// Token: 0x04002D39 RID: 11577
			None,
			// Token: 0x04002D3A RID: 11578
			Select,
			// Token: 0x04002D3B RID: 11579
			Decide
		}

		// Token: 0x0200076D RID: 1901
		private class Status
		{
			// Token: 0x04002D3C RID: 11580
			public bool m_Display;

			// Token: 0x04002D3D RID: 11581
			public Sprite m_Sprite;

			// Token: 0x04002D3E RID: 11582
			public List<eElementType> m_Elements;

			// Token: 0x04002D3F RID: 11583
			public bool m_IsSilence;

			// Token: 0x04002D40 RID: 11584
			public CharacterBattle m_CharaBattle;

			// Token: 0x04002D41 RID: 11585
			public int m_RecastValue;

			// Token: 0x04002D42 RID: 11586
			public int m_RecastMax;

			// Token: 0x04002D43 RID: 11587
			public bool m_RecastRecover;
		}
	}
}
