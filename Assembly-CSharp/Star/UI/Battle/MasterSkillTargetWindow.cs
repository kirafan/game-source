﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x0200077C RID: 1916
	public class MasterSkillTargetWindow : UIGroup
	{
		// Token: 0x14000051 RID: 81
		// (add) Token: 0x060026F7 RID: 9975 RVA: 0x000CF618 File Offset: 0x000CDA18
		// (remove) Token: 0x060026F8 RID: 9976 RVA: 0x000CF650 File Offset: 0x000CDA50
		public event Action<int> OnDecide;

		// Token: 0x060026F9 RID: 9977 RVA: 0x000CF686 File Offset: 0x000CDA86
		public override void Open()
		{
			base.Open();
		}

		// Token: 0x060026FA RID: 9978 RVA: 0x000CF690 File Offset: 0x000CDA90
		public override void Update()
		{
			base.Update();
			for (int i = 0; i < this.m_SpriteHandler.Length; i++)
			{
				if (this.m_CharaIconImage[i].sprite == null && this.m_SpriteHandler[i] != null && this.m_SpriteHandler[i].Obj)
				{
					this.m_CharaIconImage[i].sprite = this.m_SpriteHandler[i].Obj;
				}
			}
		}

		// Token: 0x060026FB RID: 9979 RVA: 0x000CF714 File Offset: 0x000CDB14
		public override void OnFinishPlayOut()
		{
			base.OnFinishPlayOut();
			for (int i = 0; i < this.m_SpriteHandler.Length; i++)
			{
				if (this.m_SpriteHandler[i] != null)
				{
					this.m_CharaIconImage[i].sprite = null;
					SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler[i]);
					this.m_SpriteHandler[i] = null;
				}
			}
		}

		// Token: 0x060026FC RID: 9980 RVA: 0x000CF77C File Offset: 0x000CDB7C
		public void SetMode(MasterSkillTargetWindow.eMode mode)
		{
			if (mode == MasterSkillTargetWindow.eMode.Player)
			{
				for (int i = 0; i < this.m_CustomButton.Length; i++)
				{
					this.m_CustomButton[i].GetComponent<RectTransform>().SetAsLastSibling();
				}
			}
			else
			{
				for (int j = 0; j < this.m_CustomButton.Length; j++)
				{
					this.m_CustomButton[this.m_CustomButton.Length - 1 - j].GetComponent<RectTransform>().SetAsLastSibling();
				}
			}
		}

		// Token: 0x060026FD RID: 9981 RVA: 0x000CF7F5 File Offset: 0x000CDBF5
		public void SetActive(int idx, bool flg)
		{
			this.m_CharaIconImage[idx].gameObject.SetActive(flg);
			this.m_TypeIconObjs[idx].SetActive(false);
		}

		// Token: 0x060026FE RID: 9982 RVA: 0x000CF818 File Offset: 0x000CDC18
		public void SetPlayerIcon(int idx, int charaID)
		{
			if (this.m_SpriteHandler[idx] != null)
			{
				this.m_CharaIconImage[idx].sprite = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler[idx]);
				this.m_SpriteHandler[idx] = null;
			}
			this.m_SpriteHandler[idx] = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncPLOrderIcon(charaID);
			this.m_TypeIconObjs[idx].SetActive(false);
		}

		// Token: 0x060026FF RID: 9983 RVA: 0x000CF888 File Offset: 0x000CDC88
		public void SetEnemyIcon(int idx, int resourceID)
		{
			if (this.m_SpriteHandler[idx] != null)
			{
				this.m_CharaIconImage[idx].sprite = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler[idx]);
				this.m_SpriteHandler[idx] = null;
			}
			this.m_SpriteHandler[idx] = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncENOrderIcon(resourceID);
			this.m_TypeIconObjs[idx].SetActive(true);
		}

		// Token: 0x06002700 RID: 9984 RVA: 0x000CF8F6 File Offset: 0x000CDCF6
		public void SetInteractable(int idx, bool flg)
		{
			this.m_CustomButton[idx].Interactable = flg;
		}

		// Token: 0x06002701 RID: 9985 RVA: 0x000CF906 File Offset: 0x000CDD06
		public void OnClickButtonCallBack(int idx)
		{
			this.OnDecide(idx);
		}

		// Token: 0x06002702 RID: 9986 RVA: 0x000CF914 File Offset: 0x000CDD14
		public void OnClickCancelButtonCallBack()
		{
			this.OnDecide(-1);
		}

		// Token: 0x04002DAB RID: 11691
		[SerializeField]
		private Image[] m_CharaIconImage;

		// Token: 0x04002DAC RID: 11692
		[SerializeField]
		private GameObject[] m_TypeIconObjs;

		// Token: 0x04002DAD RID: 11693
		[SerializeField]
		private CustomButton[] m_CustomButton;

		// Token: 0x04002DAE RID: 11694
		private SpriteHandler[] m_SpriteHandler = new SpriteHandler[3];

		// Token: 0x0200077D RID: 1917
		public enum eMode
		{
			// Token: 0x04002DB1 RID: 11697
			Player,
			// Token: 0x04002DB2 RID: 11698
			Enemy
		}
	}
}
