﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200079D RID: 1949
	public class TurnOwnerMarker : MonoBehaviour
	{
		// Token: 0x060027CF RID: 10191 RVA: 0x000D527E File Offset: 0x000D367E
		private void Awake()
		{
			this.m_GameObject = base.gameObject;
			this.m_Transform = this.m_GameObject.GetComponent<RectTransform>();
		}

		// Token: 0x060027D0 RID: 10192 RVA: 0x000D529D File Offset: 0x000D369D
		private void Update()
		{
			this.m_ImageTransform.localPosY(Mathf.Sin(this.m_Time * this.m_AnimSpeed) * this.m_Radius);
			this.m_Time += Time.deltaTime;
		}

		// Token: 0x060027D1 RID: 10193 RVA: 0x000D52D5 File Offset: 0x000D36D5
		public void SetPosition(float x, float y)
		{
			this.m_Transform.localPosition = new Vector3(x, y, 0f);
		}

		// Token: 0x060027D2 RID: 10194 RVA: 0x000D52EE File Offset: 0x000D36EE
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x060027D3 RID: 10195 RVA: 0x000D52FC File Offset: 0x000D36FC
		public bool IsEnableRender()
		{
			return base.gameObject.activeSelf;
		}

		// Token: 0x04002EED RID: 12013
		[SerializeField]
		private float m_Radius = 4f;

		// Token: 0x04002EEE RID: 12014
		[SerializeField]
		private float m_AnimSpeed = 4f;

		// Token: 0x04002EEF RID: 12015
		[SerializeField]
		private RectTransform m_ImageTransform;

		// Token: 0x04002EF0 RID: 12016
		private float m_Time;

		// Token: 0x04002EF1 RID: 12017
		private GameObject m_GameObject;

		// Token: 0x04002EF2 RID: 12018
		private RectTransform m_Transform;
	}
}
