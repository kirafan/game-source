﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Battle
{
	// Token: 0x02000769 RID: 1897
	public class ChargeCountSignal : MonoBehaviour
	{
		// Token: 0x06002690 RID: 9872 RVA: 0x000CD2EC File Offset: 0x000CB6EC
		public void Apply(ChargeCountSignal.eState state)
		{
			switch (state)
			{
			case ChargeCountSignal.eState.None:
				if (this.m_Empty.activeSelf)
				{
					this.m_Empty.SetActive(false);
				}
				if (this.m_Active.activeSelf)
				{
					this.m_Active.SetActive(false);
				}
				this.m_EffectImage.enabled = false;
				break;
			case ChargeCountSignal.eState.Empty:
				if (!this.m_Empty.activeSelf)
				{
					this.m_Empty.SetActive(true);
				}
				if (this.m_Active.activeSelf)
				{
					this.m_Active.SetActive(false);
				}
				this.m_EffectImage.enabled = false;
				break;
			case ChargeCountSignal.eState.Active:
				if (!this.m_Empty.activeSelf)
				{
					this.m_Empty.SetActive(true);
				}
				if (!this.m_Active.activeSelf)
				{
					this.m_Active.SetActive(true);
				}
				this.m_EffectImage.enabled = false;
				break;
			case ChargeCountSignal.eState.Effect:
				if (!this.m_Empty.activeSelf)
				{
					this.m_Empty.SetActive(true);
				}
				if (!this.m_Active.activeSelf)
				{
					this.m_Active.SetActive(true);
				}
				this.m_EffectImage.enabled = true;
				break;
			}
		}

		// Token: 0x04002D0E RID: 11534
		[SerializeField]
		private GameObject m_Empty;

		// Token: 0x04002D0F RID: 11535
		[SerializeField]
		private GameObject m_Active;

		// Token: 0x04002D10 RID: 11536
		[SerializeField]
		private Image m_EffectImage;

		// Token: 0x0200076A RID: 1898
		public enum eState
		{
			// Token: 0x04002D12 RID: 11538
			None,
			// Token: 0x04002D13 RID: 11539
			Empty,
			// Token: 0x04002D14 RID: 11540
			Active,
			// Token: 0x04002D15 RID: 11541
			Effect
		}
	}
}
