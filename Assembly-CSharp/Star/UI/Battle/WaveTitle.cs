﻿using System;
using UnityEngine;

namespace Star.UI.Battle
{
	// Token: 0x0200079F RID: 1951
	public class WaveTitle : MonoBehaviour
	{
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x060027D9 RID: 10201 RVA: 0x000D5436 File Offset: 0x000D3836
		public AnimUIPlayer Anim
		{
			get
			{
				if (this.m_Anim == null)
				{
					this.m_Anim = base.GetComponent<AnimUIPlayer>();
				}
				return this.m_Anim;
			}
		}

		// Token: 0x060027DA RID: 10202 RVA: 0x000D545B File Offset: 0x000D385B
		public void Apply(int wave, int maxWave)
		{
			this.m_NowWave.SetValue(wave);
			this.m_MaxWave.SetValue(maxWave);
		}

		// Token: 0x04002EFB RID: 12027
		[SerializeField]
		private ImageNumbers m_NowWave;

		// Token: 0x04002EFC RID: 12028
		[SerializeField]
		private ImageNumbers m_MaxWave;

		// Token: 0x04002EFD RID: 12029
		private AnimUIPlayer m_Anim;
	}
}
