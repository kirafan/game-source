﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008D9 RID: 2265
	public class DownloadImageItem : InfiniteScrollItem
	{
		// Token: 0x06002EEE RID: 12014 RVA: 0x000F572C File Offset: 0x000F3B2C
		public override void Apply(InfiniteScrollItemData data)
		{
			base.Apply(data);
			DownloadImageItemData downloadImageItemData = (DownloadImageItemData)data;
			if (downloadImageItemData.m_Sprite == null)
			{
				this.m_Image.enabled = false;
			}
			else
			{
				this.m_Image.sprite = downloadImageItemData.m_Sprite;
				this.m_Image.enabled = true;
			}
		}

		// Token: 0x06002EEF RID: 12015 RVA: 0x000F5786 File Offset: 0x000F3B86
		public override void Destroy()
		{
			base.Destroy();
			this.m_Image.sprite = null;
		}

		// Token: 0x040035FB RID: 13819
		[SerializeField]
		private Image m_Image;
	}
}
