﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008C5 RID: 2245
	public class UIStateController : MonoBehaviour
	{
		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06002E97 RID: 11927 RVA: 0x000F4429 File Offset: 0x000F2829
		public int NowState
		{
			get
			{
				return this.m_NowState;
			}
		}

		// Token: 0x06002E98 RID: 11928 RVA: 0x000F4434 File Offset: 0x000F2834
		public void Setup(int stateNum, int startState = -1, bool transit = false)
		{
			this.m_StateDatas = new UIStateController.StateData[stateNum];
			for (int i = 0; i < stateNum; i++)
			{
				this.m_StateDatas[i].m_TransitDest = new bool[stateNum];
				for (int j = 0; j < stateNum; j++)
				{
					this.m_StateDatas[i].m_TransitDest[j] = transit;
				}
			}
			this.m_NowState = startState;
		}

		// Token: 0x06002E99 RID: 11929 RVA: 0x000F44A3 File Offset: 0x000F28A3
		public void AddTransit(int sourceStateID, int destStateID, bool flg = true)
		{
			if (sourceStateID >= this.m_StateDatas.Length || destStateID >= this.m_StateDatas.Length)
			{
				return;
			}
			this.m_StateDatas[sourceStateID].m_TransitDest[destStateID] = flg;
		}

		// Token: 0x06002E9A RID: 11930 RVA: 0x000F44D8 File Offset: 0x000F28D8
		public void AddTransitEach(int sourceStateID, int destStateID, bool flg = true)
		{
			if (sourceStateID >= this.m_StateDatas.Length || destStateID >= this.m_StateDatas.Length)
			{
				return;
			}
			this.m_StateDatas[sourceStateID].m_TransitDest[destStateID] = flg;
			this.m_StateDatas[destStateID].m_TransitDest[sourceStateID] = flg;
		}

		// Token: 0x06002E9B RID: 11931 RVA: 0x000F452A File Offset: 0x000F292A
		public void AddOnEnter(int stateID, Action OnEnter)
		{
			UIStateController.StateData[] stateDatas = this.m_StateDatas;
			stateDatas[stateID].OnEnter = (Action)Delegate.Combine(stateDatas[stateID].OnEnter, OnEnter);
		}

		// Token: 0x06002E9C RID: 11932 RVA: 0x000F454E File Offset: 0x000F294E
		public void AddOnExit(int stateID, Action OnExit)
		{
			UIStateController.StateData[] stateDatas = this.m_StateDatas;
			stateDatas[stateID].OnExit = (Action)Delegate.Combine(stateDatas[stateID].OnExit, OnExit);
		}

		// Token: 0x06002E9D RID: 11933 RVA: 0x000F4572 File Offset: 0x000F2972
		public bool ChangeState(int state)
		{
			if (state < 0 || state >= this.m_StateDatas.Length)
			{
				return false;
			}
			if (!this.m_StateDatas[this.m_NowState].m_TransitDest[state])
			{
				return false;
			}
			this.DoTransit(state);
			return true;
		}

		// Token: 0x06002E9E RID: 11934 RVA: 0x000F45B2 File Offset: 0x000F29B2
		public void ForceChangeState(int state)
		{
			if (state < 0 || state >= this.m_StateDatas.Length)
			{
				return;
			}
			if (this.m_NowState == state)
			{
				return;
			}
			this.DoTransit(state);
		}

		// Token: 0x06002E9F RID: 11935 RVA: 0x000F45E0 File Offset: 0x000F29E0
		private void DoTransit(int state)
		{
			this.m_StateDatas[this.m_NowState].OnExit.Call();
			if (this.m_UIGroups != null && this.m_UIGroups.Length > 0)
			{
				for (int i = 0; i < this.m_UIGroups.Length; i++)
				{
					this.m_UIGroups[i].SetEnableInput(false);
				}
			}
			this.m_NowState = state;
			this.m_StateDatas[this.m_NowState].OnEnter.Call();
		}

		// Token: 0x06002EA0 RID: 11936 RVA: 0x000F466C File Offset: 0x000F2A6C
		public void SetEnableUIGroups(bool flg)
		{
			if (this.m_UIGroups != null && this.m_UIGroups.Length > 0)
			{
				for (int i = 0; i < this.m_UIGroups.Length; i++)
				{
					this.m_UIGroups[i].SetEnableInput(flg);
				}
			}
		}

		// Token: 0x06002EA1 RID: 11937 RVA: 0x000F46BC File Offset: 0x000F2ABC
		public bool IsPlayingInOut()
		{
			for (int i = 0; i < this.m_UIGroups.Length; i++)
			{
				if (this.m_UIGroups[i].IsPlayingInOut)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x040035BD RID: 13757
		[SerializeField]
		private UIGroup[] m_UIGroups;

		// Token: 0x040035BE RID: 13758
		private UIStateController.StateData[] m_StateDatas;

		// Token: 0x040035BF RID: 13759
		private int m_NowState = -1;

		// Token: 0x020008C6 RID: 2246
		private struct StateData
		{
			// Token: 0x040035C0 RID: 13760
			public bool[] m_TransitDest;

			// Token: 0x040035C1 RID: 13761
			public Action OnEnter;

			// Token: 0x040035C2 RID: 13762
			public Action OnExit;
		}
	}
}
