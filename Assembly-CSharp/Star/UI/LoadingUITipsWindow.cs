﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009CB RID: 2507
	public class LoadingUITipsWindow : MonoBehaviour
	{
		// Token: 0x06003414 RID: 13332 RVA: 0x001082C5 File Offset: 0x001066C5
		public void Setup()
		{
			this.SetState(LoadingUITipsWindow.eState.Hide);
			this.m_NowIndex = 0;
		}

		// Token: 0x06003415 RID: 13333 RVA: 0x001082D8 File Offset: 0x001066D8
		private void Update()
		{
			LoadingUITipsWindow.eState state = this.GetState();
			switch (this.GetState())
			{
			case LoadingUITipsWindow.eState.In:
				if (this.m_Player.State == 2)
				{
					this.SetState(LoadingUITipsWindow.eState.Idle);
				}
				break;
			case LoadingUITipsWindow.eState.Idle:
			case LoadingUITipsWindow.eState.Out:
			case LoadingUITipsWindow.eState.OutAndIn:
				if (this.m_Player.State == 0)
				{
					this.SetState(LoadingUITipsWindow.eState.Hide);
				}
				break;
			}
			if (state == LoadingUITipsWindow.eState.OutAndIn && this.GetState() == LoadingUITipsWindow.eState.Hide)
			{
				this.PlayInText();
			}
		}

		// Token: 0x06003416 RID: 13334 RVA: 0x00108361 File Offset: 0x00106761
		public void Play(eTipsCategory tipsCategory)
		{
			this.m_ParamList.Clear();
			this.AcquireTipsDBParams(tipsCategory);
			this.PlayInText();
		}

		// Token: 0x06003417 RID: 13335 RVA: 0x0010837B File Offset: 0x0010677B
		public void Stop()
		{
			if (this.GetState() != LoadingUITipsWindow.eState.Hide)
			{
				this.PlayOut();
			}
		}

		// Token: 0x06003418 RID: 13336 RVA: 0x0010838E File Offset: 0x0010678E
		private void ToNext()
		{
			if (this.GetState() == LoadingUITipsWindow.eState.Hide)
			{
				this.PlayInText();
			}
			else
			{
				this.PlayOut();
				if (this.GetState() == LoadingUITipsWindow.eState.Out)
				{
					this.SetState(LoadingUITipsWindow.eState.OutAndIn);
				}
			}
		}

		// Token: 0x06003419 RID: 13337 RVA: 0x001083C0 File Offset: 0x001067C0
		private void PlayInText()
		{
			this.m_Player.PlayIn();
			TipsDB_Param nextDisplayTipsMessageParameter = this.GetNextDisplayTipsMessageParameter();
			this.m_Title.text = nextDisplayTipsMessageParameter.m_Title;
			this.m_Text.text = nextDisplayTipsMessageParameter.m_Text;
			this.SetState(LoadingUITipsWindow.eState.In);
		}

		// Token: 0x0600341A RID: 13338 RVA: 0x0010840A File Offset: 0x0010680A
		private void PlayOut()
		{
			if (this.GetState() == LoadingUITipsWindow.eState.In)
			{
				this.m_Player.Show();
			}
			if (this.GetState() == LoadingUITipsWindow.eState.Idle)
			{
				this.m_Player.PlayOut();
				this.SetState(LoadingUITipsWindow.eState.Out);
			}
		}

		// Token: 0x0600341B RID: 13339 RVA: 0x00108441 File Offset: 0x00106841
		private void SkipPlayIn()
		{
			if (this.GetState() == LoadingUITipsWindow.eState.In)
			{
				this.m_Player.Show();
				this.SetState(LoadingUITipsWindow.eState.Idle);
			}
		}

		// Token: 0x0600341C RID: 13340 RVA: 0x00108461 File Offset: 0x00106861
		protected void SetState(LoadingUITipsWindow.eState state)
		{
			this.m_State = state;
		}

		// Token: 0x0600341D RID: 13341 RVA: 0x0010846A File Offset: 0x0010686A
		public LoadingUITipsWindow.eState GetState()
		{
			return this.m_State;
		}

		// Token: 0x0600341E RID: 13342 RVA: 0x00108474 File Offset: 0x00106874
		protected TipsDB_Param GetNextDisplayTipsMessageParameter()
		{
			if (this.m_ParamList.Count > 0)
			{
				if (this.m_NowIndex == -1)
				{
					this.m_NowIndex = UnityEngine.Random.Range(0, this.m_ParamList.Count);
				}
				else
				{
					this.m_NowIndex = (this.m_NowIndex + UnityEngine.Random.Range(1, this.m_ParamList.Count - 1)) % this.m_ParamList.Count;
				}
				return this.m_ParamList[this.m_NowIndex];
			}
			return new TipsDB_Param
			{
				m_Title = string.Empty,
				m_Title = string.Empty
			};
		}

		// Token: 0x0600341F RID: 13343 RVA: 0x0010851C File Offset: 0x0010691C
		protected void AcquireTipsDBParams(eTipsCategory category)
		{
			this.m_ParamList.Clear();
			TipsDB_Param[] @params = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TipsLoadingDB.m_Params;
			for (int i = 0; i < @params.Length; i++)
			{
				if (@params[i].m_Category == (int)category || @params[i].m_Category == 0)
				{
					this.m_ParamList.Add(@params[i]);
				}
			}
			this.m_NowIndex = -1;
		}

		// Token: 0x06003420 RID: 13344 RVA: 0x0010859E File Offset: 0x0010699E
		public void OnClickTipsWindow()
		{
			if (base.gameObject.activeSelf)
			{
				this.ToNext();
			}
		}

		// Token: 0x04003A59 RID: 14937
		[SerializeField]
		[Tooltip("出現退場演出.")]
		private AnimUIPlayer m_Player;

		// Token: 0x04003A5A RID: 14938
		[SerializeField]
		[Tooltip("Tipsタイトル.")]
		private Text m_Title;

		// Token: 0x04003A5B RID: 14939
		[SerializeField]
		[Tooltip("Tips文章.")]
		private Text m_Text;

		// Token: 0x04003A5C RID: 14940
		private LoadingUITipsWindow.eState m_State;

		// Token: 0x04003A5D RID: 14941
		private List<TipsDB_Param> m_ParamList = new List<TipsDB_Param>();

		// Token: 0x04003A5E RID: 14942
		private int m_NowIndex = -1;

		// Token: 0x020009CC RID: 2508
		public enum eState
		{
			// Token: 0x04003A60 RID: 14944
			Hide,
			// Token: 0x04003A61 RID: 14945
			In,
			// Token: 0x04003A62 RID: 14946
			Idle,
			// Token: 0x04003A63 RID: 14947
			Out,
			// Token: 0x04003A64 RID: 14948
			OutAndIn
		}
	}
}
