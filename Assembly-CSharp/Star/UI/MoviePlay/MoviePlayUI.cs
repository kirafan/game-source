﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Star.UI.MoviePlay
{
	// Token: 0x02000A24 RID: 2596
	public class MoviePlayUI : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x140000AC RID: 172
		// (add) Token: 0x060035AD RID: 13741 RVA: 0x00110358 File Offset: 0x0010E758
		// (remove) Token: 0x060035AE RID: 13742 RVA: 0x00110390 File Offset: 0x0010E790
		private event Action OnConfirmSkip;

		// Token: 0x140000AD RID: 173
		// (add) Token: 0x060035AF RID: 13743 RVA: 0x001103C8 File Offset: 0x0010E7C8
		// (remove) Token: 0x060035B0 RID: 13744 RVA: 0x00110400 File Offset: 0x0010E800
		private event Action OnExecSkip;

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x060035B1 RID: 13745 RVA: 0x00110436 File Offset: 0x0010E836
		// (set) Token: 0x060035B2 RID: 13746 RVA: 0x0011043E File Offset: 0x0010E83E
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInput;
			}
			set
			{
				this.EnableInput(value);
			}
		}

		// Token: 0x060035B3 RID: 13747 RVA: 0x00110447 File Offset: 0x0010E847
		public void Start()
		{
			this.SetEnableSkipButton(false);
		}

		// Token: 0x060035B4 RID: 13748 RVA: 0x00110450 File Offset: 0x0010E850
		public void Setup(MoviePlayUI.eSkipMode mode)
		{
			this.SetSkipMode(mode);
		}

		// Token: 0x060035B5 RID: 13749 RVA: 0x00110459 File Offset: 0x0010E859
		public void Destroy()
		{
			this.m_MovieCanvas = null;
			this.m_SkipButton = null;
			this.OnConfirmSkip = null;
			this.OnExecSkip = null;
		}

		// Token: 0x060035B6 RID: 13750 RVA: 0x00110478 File Offset: 0x0010E878
		private void Update()
		{
			if (this.m_SkipMode == MoviePlayUI.eSkipMode.Tap && this.m_SkipButton.gameObject.activeSelf && this.m_SkipBtnDisplayTimer > 0f && this.m_IsSkipBtnDisplayTimerUpdate)
			{
				this.m_SkipBtnDisplayTimer -= Time.deltaTime;
				if (this.m_SkipBtnDisplayTimer <= 0f)
				{
					this.m_SkipBtnDisplayTimer = 0f;
					this.SetEnableSkipButton(false);
				}
			}
			this.UpdateAndroidBackKey();
		}

		// Token: 0x060035B7 RID: 13751 RVA: 0x001104FB File Offset: 0x0010E8FB
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x060035B8 RID: 13752 RVA: 0x00110503 File Offset: 0x0010E903
		public void OnPointerClick(PointerEventData eventData)
		{
			this.CheckEnableSkipButtonOnTap();
		}

		// Token: 0x060035B9 RID: 13753 RVA: 0x0011050C File Offset: 0x0010E90C
		private bool CheckEnableSkipButtonOnTap()
		{
			if (this.m_IsEnableInput && this.m_SkipMode == MoviePlayUI.eSkipMode.Tap && !this.m_SkipButton.gameObject.activeSelf)
			{
				this.m_SkipBtnDisplayTimer = 5f;
				this.SetEnableSkipButton(true);
				return true;
			}
			return false;
		}

		// Token: 0x060035BA RID: 13754 RVA: 0x0011055C File Offset: 0x0010E95C
		public void SetSkipMode(MoviePlayUI.eSkipMode mode)
		{
			this.m_SkipMode = mode;
			MoviePlayUI.eSkipMode skipMode = this.m_SkipMode;
			if (skipMode != MoviePlayUI.eSkipMode.Disable)
			{
				if (skipMode != MoviePlayUI.eSkipMode.Always)
				{
					if (skipMode == MoviePlayUI.eSkipMode.Tap)
					{
						this.m_IsSkipBtnDisplayTimerUpdate = true;
						this.SetEnableSkipButton(false);
					}
				}
				else
				{
					this.SetEnableSkipButton(true);
				}
			}
			else
			{
				this.SetEnableSkipButton(false);
			}
		}

		// Token: 0x060035BB RID: 13755 RVA: 0x001105BB File Offset: 0x0010E9BB
		public void SetConfirmSkip(Action onConfirmSkip)
		{
			this.OnConfirmSkip = onConfirmSkip;
		}

		// Token: 0x060035BC RID: 13756 RVA: 0x001105C4 File Offset: 0x0010E9C4
		public void SetExecSkip(Action onExecSkip)
		{
			this.OnExecSkip = onExecSkip;
		}

		// Token: 0x060035BD RID: 13757 RVA: 0x001105CD File Offset: 0x0010E9CD
		public void EnableInput(bool flag)
		{
			this.m_IsEnableInput = flag;
		}

		// Token: 0x060035BE RID: 13758 RVA: 0x001105D6 File Offset: 0x0010E9D6
		public void SetEnableSkipButton(bool flg)
		{
			this.m_SkipButton.gameObject.SetActive(flg);
		}

		// Token: 0x060035BF RID: 13759 RVA: 0x001105EC File Offset: 0x0010E9EC
		public void OpenSkipWindow()
		{
			this.OnConfirmSkip.Call();
			if (this.m_MovieCanvas != null)
			{
				this.m_MovieCanvas.Pause();
			}
			this.EnableInput(false);
			this.m_IsSkipBtnDisplayTimerUpdate = false;
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.ChangeYesButtonText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.MovieSkipYes));
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.ChangeNoButtonText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.MovieSkipNo));
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MovieSkipTitle, eText_MessageDB.MovieSkip, new Action<int>(this.OnConfirmSkipWindow));
		}

		// Token: 0x060035C0 RID: 13760 RVA: 0x00110690 File Offset: 0x0010EA90
		private void OnConfirmSkipWindow(int answer)
		{
			if (answer == 0)
			{
				this.OnExecSkip.Call();
			}
			else
			{
				if (this.m_MovieCanvas != null)
				{
					this.m_MovieCanvas.Resume();
				}
				this.EnableInput(true);
				this.m_IsSkipBtnDisplayTimerUpdate = true;
			}
		}

		// Token: 0x060035C1 RID: 13761 RVA: 0x001106DD File Offset: 0x0010EADD
		private void UpdateAndroidBackKey()
		{
			if (UIUtility.GetInputAndroidBackKey(null) && this.CheckEnableSkipButtonOnTap())
			{
				return;
			}
			if (this.m_SkipMode != MoviePlayUI.eSkipMode.Disable)
			{
				UIUtility.UpdateAndroidBackKey(this.m_SkipButton, null);
			}
		}

		// Token: 0x04003C4E RID: 15438
		private const float SKIP_BTN_DISPLAY_SEC_ON_TAP = 5f;

		// Token: 0x04003C4F RID: 15439
		[SerializeField]
		private MovieCanvas m_MovieCanvas;

		// Token: 0x04003C50 RID: 15440
		[SerializeField]
		private CustomButton m_SkipButton;

		// Token: 0x04003C51 RID: 15441
		private MoviePlayUI.eSkipMode m_SkipMode;

		// Token: 0x04003C52 RID: 15442
		private float m_SkipBtnDisplayTimer;

		// Token: 0x04003C53 RID: 15443
		private bool m_IsSkipBtnDisplayTimerUpdate = true;

		// Token: 0x04003C54 RID: 15444
		private bool m_IsEnableInput = true;

		// Token: 0x02000A25 RID: 2597
		public enum eSkipMode
		{
			// Token: 0x04003C58 RID: 15448
			Disable,
			// Token: 0x04003C59 RID: 15449
			Always,
			// Token: 0x04003C5A RID: 15450
			Tap
		}
	}
}
