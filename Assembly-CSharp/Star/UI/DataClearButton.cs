﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000ADC RID: 2780
	[RequireComponent(typeof(InvisibleGraphic), typeof(ColorGroup), typeof(Animation))]
	public class DataClearButton : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IEventSystemHandler
	{
		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06003A22 RID: 14882 RVA: 0x001285F8 File Offset: 0x001269F8
		public ColorGroup ColorGroup
		{
			get
			{
				if (this.m_ColorGroup != null)
				{
					return this.m_ColorGroup;
				}
				this.m_ColorGroup = base.GetComponent<ColorGroup>();
				if (this.m_ColorGroup != null)
				{
					return this.m_ColorGroup;
				}
				return this.m_ColorGroup;
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06003A23 RID: 14883 RVA: 0x00128647 File Offset: 0x00126A47
		// (set) Token: 0x06003A24 RID: 14884 RVA: 0x0012865D File Offset: 0x00126A5D
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInputParent && this.m_IsEnableInputSelf;
			}
			set
			{
				this.m_IsEnableInputSelf = value;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06003A25 RID: 14885 RVA: 0x00128666 File Offset: 0x00126A66
		// (set) Token: 0x06003A26 RID: 14886 RVA: 0x0012866E File Offset: 0x00126A6E
		public bool IsEnableInputParent
		{
			get
			{
				return this.m_IsEnableInputParent;
			}
			set
			{
				this.m_IsEnableInputParent = value;
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06003A27 RID: 14887 RVA: 0x00128677 File Offset: 0x00126A77
		// (set) Token: 0x06003A28 RID: 14888 RVA: 0x0012868D File Offset: 0x00126A8D
		public bool Interactable
		{
			get
			{
				return this.m_Interactable && this.m_InteractableParent;
			}
			set
			{
				this.m_Interactable = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06003A29 RID: 14889 RVA: 0x0012869C File Offset: 0x00126A9C
		// (set) Token: 0x06003A2A RID: 14890 RVA: 0x001286A4 File Offset: 0x00126AA4
		public bool InteractableParent
		{
			get
			{
				return this.m_InteractableParent;
			}
			set
			{
				this.m_InteractableParent = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06003A2C RID: 14892 RVA: 0x001286C2 File Offset: 0x00126AC2
		// (set) Token: 0x06003A2B RID: 14891 RVA: 0x001286B3 File Offset: 0x00126AB3
		public bool IsDecided
		{
			get
			{
				return this.m_IsDecided;
			}
			set
			{
				this.m_IsDecided = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06003A2D RID: 14893 RVA: 0x001286CA File Offset: 0x00126ACA
		// (set) Token: 0x06003A2E RID: 14894 RVA: 0x001286D2 File Offset: 0x00126AD2
		public bool IgnoreGroup
		{
			get
			{
				return this.m_IsIgnoreGroup;
			}
			set
			{
				this.m_IsIgnoreGroup = value;
			}
		}

		// Token: 0x06003A2F RID: 14895 RVA: 0x001286DB File Offset: 0x00126ADB
		public void AddListerner(UnityAction onClickCallBack)
		{
			this.m_OnClick.AddListener(onClickCallBack);
		}

		// Token: 0x06003A30 RID: 14896 RVA: 0x001286E9 File Offset: 0x00126AE9
		public void RemoveListener(UnityAction onClickCallBack)
		{
			this.m_OnClick.RemoveListener(onClickCallBack);
		}

		// Token: 0x06003A31 RID: 14897 RVA: 0x001286F8 File Offset: 0x00126AF8
		private void ApplyPreset()
		{
			if (this.m_IsDoneApplyPreset)
			{
				return;
			}
			this.m_IsDoneApplyPreset = true;
			switch (this.m_Preset)
			{
			case DataClearButton.ePreset.CommonButton:
				this.m_TransitSec = 0f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_ExtendWidth = 0f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = false;
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				this.m_DecideColorChange = false;
				break;
			case DataClearButton.ePreset.Button:
				this.m_TransitSec = 0f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = true;
				this.m_ExtendWidth = 5f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_PressColor = new Color(1f, 1f, 1f);
				this.m_DecideColor = new Color(0.7f, 1f, 0.4f);
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				break;
			case DataClearButton.ePreset.Icon:
				this.m_TransitSec = 0.1f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_ColorTintMaterial = null;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_PlaySwapObj = false;
				break;
			case DataClearButton.ePreset.Panel:
				this.m_TransitSec = 0.1f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_ColorTintMaterial = null;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_PlaySwapObj = false;
				break;
			case DataClearButton.ePreset.Tab:
				this.m_TransitSec = 0.1f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_ColorTintMaterial = null;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(0.7f, 1f, 0.4f);
				this.m_PlaySwapObj = false;
				break;
			}
			this.Interactable = this.m_Interactable;
		}

		// Token: 0x06003A32 RID: 14898 RVA: 0x00128988 File Offset: 0x00126D88
		private void Start()
		{
			this.ApplyPreset();
			this.m_SwapObjs = base.GetComponentsInChildren<SwapObj>(true);
			this.m_GraphForButtons = base.GetComponentsInChildren<GraphForButton>(true);
			if (this.m_CurrentButtonState == DataClearButton.eButtonState.None)
			{
				this.ChangeButtonState(DataClearButton.eButtonState.Wait);
			}
			if (this.m_BodyRectTransform == null)
			{
				this.m_BodyRectTransform = (RectTransform)base.GetComponent<RectTransform>().Find("body");
			}
			if (this.m_BodyRectTransform != null)
			{
				this.m_DefaultLocalPosition = this.m_BodyRectTransform.localPosition;
				this.m_DefaultLocalScale = this.m_BodyRectTransform.localScale;
			}
			this.ApplyStandardColor();
		}

		// Token: 0x06003A33 RID: 14899 RVA: 0x00128A2C File Offset: 0x00126E2C
		private void Update()
		{
			if (!this.IsEnableInput || (!this.Interactable && !this.m_EnableInputHoldOnNotInteractable) || SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput())
			{
				this.m_IsHold = false;
			}
			if (this.m_BodyRectTransform != null && this.m_DefaultRectWidth == 0f)
			{
				this.m_DefaultRectWidth = this.m_BodyRectTransform.rect.width;
			}
			this.m_HoldCount += Time.realtimeSinceStartup - this.m_BeforeTime;
			this.m_BeforeTime = Time.realtimeSinceStartup;
			if (this.m_IsHold)
			{
				if (this.m_OutputHoldTime != null)
				{
					this.m_OutputHoldTime.text = this.m_HoldCount.ToString();
				}
				if (this.m_OutFrameImage != null)
				{
					this.m_OutFrameImage.fillAmount = this.m_HoldCount / this.m_HoldThreshold;
				}
				if (this.m_HoldCount > this.m_HoldThreshold)
				{
					this.ExecuteHold();
					this.m_IsHold = false;
				}
			}
			else
			{
				if (this.m_OutFrameImage != null)
				{
					float num = this.m_OutFrameImage.fillAmount - Time.deltaTime;
					if (num < 0f)
					{
					}
					this.m_OutFrameImage.fillAmount = 0f;
				}
				if (this.m_PlayAnim && this.m_CurrentButtonState == DataClearButton.eButtonState.Release && (this.m_Animation == null || this.m_ReleaseAnim == null || !this.m_Animation.IsPlaying(this.m_ReleaseAnim.name)))
				{
					if (this.m_Animation != null)
					{
						this.m_Animation.Stop();
					}
					this.ChangeButtonState(DataClearButton.eButtonState.Wait);
				}
			}
			if (this.m_PlayMove || this.m_PlayScale || this.m_PlayExtend || this.m_PlayColorTint)
			{
				if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press || this.m_CurrentButtonState == DataClearButton.eButtonState.Release)
				{
					float num2;
					if (this.m_TransitSec > 0f)
					{
						num2 = Time.deltaTime * 1f / this.m_TransitSec;
					}
					else
					{
						num2 = 1f;
					}
					if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press)
					{
						this.m_TransitRate += num2;
					}
					else if (this.m_CurrentButtonState == DataClearButton.eButtonState.Release)
					{
						this.m_TransitRate -= num2;
					}
					if (this.m_TransitRate > 1f)
					{
						this.m_TransitRate = 1f;
					}
					else if (this.m_TransitRate < 0f)
					{
						this.m_TransitRate = 0f;
						if (this.m_CurrentButtonState == DataClearButton.eButtonState.Release)
						{
							this.ChangeButtonState(DataClearButton.eButtonState.Wait);
						}
					}
					if (this.m_PlayMove)
					{
						Vector3 localPosition = this.m_DefaultLocalPosition + this.m_MoveVec * this.m_TransitRate;
						if (this.m_BodyRectTransform)
						{
							this.m_BodyRectTransform.localPosition = localPosition;
						}
					}
					if (this.m_PlayScale || this.m_PlayExtend)
					{
						Vector3 vector = this.m_DefaultLocalScale;
						if (this.m_PlayScale)
						{
							vector = this.m_DefaultLocalScale + (this.m_ScaleVec - this.m_DefaultLocalScale) * this.m_TransitRate;
						}
						if (this.m_PlayExtend && this.m_DefaultRectWidth > 0f)
						{
							float num3 = this.m_ExtendWidth / this.m_DefaultRectWidth;
							vector *= 1f + num3 * this.m_TransitRate;
						}
						if (this.m_BodyRectTransform)
						{
							this.m_BodyRectTransform.localScale = vector;
						}
					}
					if (this.m_PlayColorTint)
					{
						this.ApplyStandardColor();
					}
				}
				else if (this.m_BodyRectTransform)
				{
					this.m_BodyRectTransform.localPosition = this.m_DefaultLocalPosition;
					this.m_BodyRectTransform.localScale = this.m_DefaultLocalScale;
				}
			}
			if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press && InputTouch.Inst != null && InputTouch.Inst.GetAvailableTouchCnt() == 0)
			{
				this.ChangeButtonState(DataClearButton.eButtonState.Release);
			}
		}

		// Token: 0x06003A34 RID: 14900 RVA: 0x00128E7C File Offset: 0x0012727C
		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!this.IsEnableInput)
			{
				return;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput())
			{
				return;
			}
			if (!this.Interactable && !this.m_EnableInputOnNotInteractable)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG, 1f, 0, -1, -1);
				return;
			}
			if (!this.m_IsHold)
			{
				return;
			}
			if (this.m_HoldCount > this.m_HoldThreshold)
			{
				this.ExecuteHold();
				this.m_IsHold = false;
			}
			else
			{
				this.ExecuteClick();
				this.m_IsHold = false;
			}
			this.ApplyStandardColor();
			if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press)
			{
				this.ChangeButtonState(DataClearButton.eButtonState.Release);
				this.ResetHold();
			}
		}

		// Token: 0x06003A35 RID: 14901 RVA: 0x00128F48 File Offset: 0x00127348
		public void OnPointerDown(PointerEventData eventData)
		{
			if (eventData == null)
			{
				return;
			}
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!this.IsEnableInput)
			{
				return;
			}
			if (eventData == null)
			{
				return;
			}
			if (eventData == null)
			{
				return;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput())
			{
				return;
			}
			if (!this.Interactable && !this.m_EnableInputOnNotInteractable && !this.m_EnableInputHoldOnNotInteractable)
			{
				return;
			}
			this.ApplyStandardColor();
			this.ChangeButtonState(DataClearButton.eButtonState.Press);
			this.ResetHold();
			this.m_IsHold = true;
		}

		// Token: 0x06003A36 RID: 14902 RVA: 0x00128FD4 File Offset: 0x001273D4
		public void OnPointerUp(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!eventData.IsPointerMoving() && this.m_CurrentButtonState == DataClearButton.eButtonState.Press)
			{
				this.ForceRelease();
				this.m_IsHold = false;
			}
		}

		// Token: 0x06003A37 RID: 14903 RVA: 0x00129007 File Offset: 0x00127407
		public void ForceRelease()
		{
			this.ApplyStandardColor();
			if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press && this.m_HoldCount < this.m_HoldThreshold)
			{
				this.ExecuteClick();
				this.m_IsHold = false;
			}
			this.m_IsHold = false;
			this.ChangeButtonState(DataClearButton.eButtonState.Release);
		}

		// Token: 0x06003A38 RID: 14904 RVA: 0x00129047 File Offset: 0x00127447
		public void OnPointerExit(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press)
			{
				this.ApplyStandardColor();
				this.ChangeButtonState(DataClearButton.eButtonState.Release);
				this.m_IsHold = false;
			}
		}

		// Token: 0x06003A39 RID: 14905 RVA: 0x00129078 File Offset: 0x00127478
		public void ForceEnableHold(bool flg)
		{
			if (!this.IsEnableInput || (!this.Interactable && !this.m_EnableInputOnNotInteractable))
			{
				return;
			}
			if (flg)
			{
				this.ChangeButtonState(DataClearButton.eButtonState.Press);
				this.ResetHold();
				this.m_TransitRate = 0f;
			}
			else
			{
				this.ChangeButtonState(DataClearButton.eButtonState.Wait);
				this.ResetHold();
			}
		}

		// Token: 0x06003A3A RID: 14906 RVA: 0x001290D7 File Offset: 0x001274D7
		private void OnEnable()
		{
			this.ResetHold();
		}

		// Token: 0x06003A3B RID: 14907 RVA: 0x001290E0 File Offset: 0x001274E0
		private void ResetHold()
		{
			if (this.m_OutputHoldTime != null)
			{
				if (!this.m_IsHold)
				{
					this.m_PreHoldTimeText = this.m_OutputHoldTime.text;
				}
				else
				{
					this.m_OutputHoldTime.text = this.m_PreHoldTimeText;
					this.m_PreHoldTimeText = null;
				}
			}
			if (this.m_OutFrameImage != null)
			{
				this.m_OutFrameImage.fillAmount = 0f;
			}
			this.m_IsHold = false;
			this.m_HoldCount = 0f;
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.DbMng != null)
			{
				string textCommon = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageCount);
				if (!float.TryParse(textCommon, out this.m_HoldThreshold))
				{
					this.m_HoldThreshold = 5f;
				}
			}
			else
			{
				this.m_HoldThreshold = 5f;
			}
			this.m_OutFrameImage.fillAmount = 0f;
			this.m_BeforeTime = Time.realtimeSinceStartup;
		}

		// Token: 0x06003A3C RID: 14908 RVA: 0x001291EC File Offset: 0x001275EC
		private void ExecuteClick()
		{
			if (this.m_OnClick != null)
			{
				this.m_OnClick.Invoke();
			}
			if (this.m_PlaySEClick)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_ClickSound, 1f, 0, -1, -1);
			}
			this.ForceEnableHold(false);
		}

		// Token: 0x06003A3D RID: 14909 RVA: 0x00129240 File Offset: 0x00127640
		private void ExecuteHold()
		{
			if (this.m_OnHold != null)
			{
				this.m_OnHold.Invoke();
			}
			if (this.m_PlaySEHold)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_HoldSound, 1f, 0, -1, -1);
			}
		}

		// Token: 0x06003A3E RID: 14910 RVA: 0x0012928C File Offset: 0x0012768C
		private void ChangeButtonState(DataClearButton.eButtonState state)
		{
			if (this.m_CurrentButtonState != state && state == DataClearButton.eButtonState.Press && this.m_PlaySEPress)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_PressSound, 1f, 0, -1, -1);
			}
			this.m_CurrentButtonState = state;
			if (this.m_PlayAnim)
			{
				AnimationClip clip = null;
				switch (state)
				{
				case DataClearButton.eButtonState.Wait:
					clip = this.m_WaitAnim;
					break;
				case DataClearButton.eButtonState.Press:
					clip = this.m_PressAnim;
					if (this.m_PlaySEPress)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_PressSound, 1f, 0, -1, -1);
					}
					break;
				case DataClearButton.eButtonState.Release:
					clip = this.m_ReleaseAnim;
					break;
				case DataClearButton.eButtonState.Decide:
					clip = this.m_DecideAnim;
					break;
				}
				this.PlayAnim(clip);
			}
			if (this.m_PlaySwapObj)
			{
				DataClearButton.eSwapObjState eSwapObjState = DataClearButton.eSwapObjState.Default;
				if (this.m_CurrentButtonState == DataClearButton.eButtonState.Press)
				{
					eSwapObjState = DataClearButton.eSwapObjState.Press;
				}
				if (this.m_CurrentButtonState == DataClearButton.eButtonState.Decide)
				{
					eSwapObjState = DataClearButton.eSwapObjState.Decide;
				}
				for (int i = 0; i < this.m_SwapObjs.Length; i++)
				{
					if (!this.m_SwapObjs[i].IgnoreCustomButton)
					{
						if (eSwapObjState < (DataClearButton.eSwapObjState)this.m_SwapObjs[i].GetTargetObjNum())
						{
							this.m_SwapObjs[i].Swap((int)eSwapObjState);
						}
						else if (eSwapObjState == DataClearButton.eSwapObjState.Decide)
						{
							this.m_SwapObjs[i].Swap(2);
						}
					}
				}
			}
			GraphForButton.eState state2 = GraphForButton.eState.Default;
			if (!this.m_Interactable)
			{
				state2 = GraphForButton.eState.Disable;
			}
			else if (state == DataClearButton.eButtonState.Press)
			{
				state2 = GraphForButton.eState.Press;
			}
			for (int j = 0; j < this.m_GraphForButtons.Length; j++)
			{
				this.m_GraphForButtons[j].ChangeState(state2);
			}
		}

		// Token: 0x06003A3F RID: 14911 RVA: 0x00129444 File Offset: 0x00127844
		private void PlayAnim(AnimationClip clip)
		{
			if (this.m_Animation == null)
			{
				this.m_Animation = base.GetComponent<Animation>();
				if (this.m_Animation == null)
				{
					this.m_Animation = base.gameObject.AddComponent<Animation>();
					if (this.m_Animation == null)
					{
						return;
					}
				}
				if (this.m_WaitAnim != null)
				{
					this.m_Animation.AddClip(this.m_WaitAnim, this.m_WaitAnim.name);
				}
				if (this.m_PressAnim != null)
				{
					this.m_Animation.AddClip(this.m_PressAnim, this.m_PressAnim.name);
				}
				if (this.m_ReleaseAnim != null)
				{
					this.m_Animation.AddClip(this.m_ReleaseAnim, this.m_ReleaseAnim.name);
				}
				if (this.m_DecideAnim != null)
				{
					this.m_Animation.AddClip(this.m_DecideAnim, this.m_DecideAnim.name);
				}
			}
			this.m_Animation.playAutomatically = false;
			if (clip != null)
			{
				this.m_Animation.CrossFade(clip.name, 0.2f);
			}
			else
			{
				this.m_BodyRectTransform.localPosition = this.m_DefaultLocalPosition;
			}
		}

		// Token: 0x06003A40 RID: 14912 RVA: 0x0012959C File Offset: 0x0012799C
		private void ApplyStandardColor()
		{
			this.ApplyPreset();
			if (this.Interactable && this.ColorGroup != null && this.m_InteractableColorChange)
			{
				if (this.m_IsDecided && this.m_DecideColorChange)
				{
					if (this.ColorGroup != null)
					{
						if (this.m_ColorTintMaterial == null)
						{
							this.ColorGroup.ChangeColor(this.m_DecideColor, ColorGroup.eColorBlendMode.Mul, 1f);
						}
						else
						{
							this.ColorGroup.ChangeColor(this.m_DecideColor, this.m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set, 1f);
						}
					}
				}
				else if (this.ColorGroup != null && this.m_PlayColorTint)
				{
					if (this.m_ColorTintMaterial == null)
					{
						this.ColorGroup.ChangeColor(this.m_PressColor, ColorGroup.eColorBlendMode.Mul, this.m_TransitRate);
					}
					else
					{
						this.ColorGroup.ChangeColor(this.m_PressColor, this.m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set, this.m_TransitRate);
					}
				}
			}
			if (this.m_GraphForButtons != null)
			{
				for (int i = 0; i < this.m_GraphForButtons.Length; i++)
				{
					this.m_GraphForButtons[i].SetDecide(this.m_IsDecided);
					if (!this.Interactable)
					{
						this.m_GraphForButtons[i].ChangeState(GraphForButton.eState.Disable);
					}
					else
					{
						this.m_GraphForButtons[i].ChangeState(GraphForButton.eState.Default);
					}
				}
				this.ChangeButtonState(this.m_CurrentButtonState);
			}
			if (!this.Interactable && this.ColorGroup != null && this.m_InteractableColorChange)
			{
				this.ColorGroup.ChangeColor(Color.gray, null, ColorGroup.eColorBlendMode.Mul, 1f);
			}
		}

		// Token: 0x06003A41 RID: 14913 RVA: 0x00129761 File Offset: 0x00127B61
		public string GetMethodName(MethodBase method)
		{
			return string.Concat(new string[]
			{
				"[",
				method.DeclaringType.Name,
				"::",
				method.Name,
				"]"
			});
		}

		// Token: 0x04004165 RID: 16741
		public const float SCROLL_THRESHOLD = 24f;

		// Token: 0x04004166 RID: 16742
		[SerializeField]
		private float m_HoldThreshold = 0.5f;

		// Token: 0x04004167 RID: 16743
		[SerializeField]
		private Text m_OutputHoldTime;

		// Token: 0x04004168 RID: 16744
		private string m_PreHoldTimeText;

		// Token: 0x04004169 RID: 16745
		[SerializeField]
		private Image m_OutFrameImage;

		// Token: 0x0400416A RID: 16746
		[SerializeField]
		private bool m_Interactable = true;

		// Token: 0x0400416B RID: 16747
		private bool m_InteractableParent = true;

		// Token: 0x0400416C RID: 16748
		[SerializeField]
		private bool m_InteractableColorChange = true;

		// Token: 0x0400416D RID: 16749
		[SerializeField]
		private bool m_EnableInputOnNotInteractable;

		// Token: 0x0400416E RID: 16750
		[SerializeField]
		private bool m_EnableInputHoldOnNotInteractable;

		// Token: 0x0400416F RID: 16751
		[SerializeField]
		private bool m_IsIgnoreGroup;

		// Token: 0x04004170 RID: 16752
		[SerializeField]
		private RectTransform m_BodyRectTransform;

		// Token: 0x04004171 RID: 16753
		private DataClearButton.eButtonState m_CurrentButtonState = DataClearButton.eButtonState.Wait;

		// Token: 0x04004172 RID: 16754
		private float m_TransitRate;

		// Token: 0x04004173 RID: 16755
		[SerializeField]
		private DataClearButton.ePreset m_Preset;

		// Token: 0x04004174 RID: 16756
		private bool m_IsDoneApplyPreset;

		// Token: 0x04004175 RID: 16757
		[SerializeField]
		private float m_TransitSec;

		// Token: 0x04004176 RID: 16758
		[SerializeField]
		private bool m_PlayMove;

		// Token: 0x04004177 RID: 16759
		[SerializeField]
		private Vector3 m_MoveVec = new Vector3(0f, -10f, 0f);

		// Token: 0x04004178 RID: 16760
		[SerializeField]
		private bool m_PlayScale;

		// Token: 0x04004179 RID: 16761
		[SerializeField]
		private Vector3 m_ScaleVec = Vector3.one;

		// Token: 0x0400417A RID: 16762
		[SerializeField]
		private bool m_PlayExtend = true;

		// Token: 0x0400417B RID: 16763
		[SerializeField]
		private float m_ExtendWidth = 5f;

		// Token: 0x0400417C RID: 16764
		[SerializeField]
		private bool m_PlayAnim;

		// Token: 0x0400417D RID: 16765
		[SerializeField]
		private AnimationClip m_WaitAnim;

		// Token: 0x0400417E RID: 16766
		[SerializeField]
		private AnimationClip m_PressAnim;

		// Token: 0x0400417F RID: 16767
		[SerializeField]
		private AnimationClip m_ReleaseAnim;

		// Token: 0x04004180 RID: 16768
		[SerializeField]
		private AnimationClip m_DecideAnim;

		// Token: 0x04004181 RID: 16769
		[SerializeField]
		private bool m_PlayColorTint;

		// Token: 0x04004182 RID: 16770
		[SerializeField]
		private Material m_ColorTintMaterial;

		// Token: 0x04004183 RID: 16771
		[SerializeField]
		private Color m_PressColor = Color.gray;

		// Token: 0x04004184 RID: 16772
		[SerializeField]
		private Color m_DecideColor = new Color(0.7f, 0.6f, 0.5f, 1f);

		// Token: 0x04004185 RID: 16773
		[SerializeField]
		private bool m_PlaySwapObj;

		// Token: 0x04004186 RID: 16774
		private SwapObj[] m_SwapObjs;

		// Token: 0x04004187 RID: 16775
		[SerializeField]
		private bool m_PlaySEPress;

		// Token: 0x04004188 RID: 16776
		[SerializeField]
		private eSoundSeListDB m_PressSound;

		// Token: 0x04004189 RID: 16777
		[SerializeField]
		private bool m_PlaySEClick;

		// Token: 0x0400418A RID: 16778
		[SerializeField]
		private eSoundSeListDB m_ClickSound;

		// Token: 0x0400418B RID: 16779
		[SerializeField]
		private bool m_PlaySEHold;

		// Token: 0x0400418C RID: 16780
		[SerializeField]
		private eSoundSeListDB m_HoldSound;

		// Token: 0x0400418D RID: 16781
		[SerializeField]
		public UnityEvent m_OnClick;

		// Token: 0x0400418E RID: 16782
		[SerializeField]
		public UnityEvent m_OnHold;

		// Token: 0x0400418F RID: 16783
		private GraphForButton[] m_GraphForButtons;

		// Token: 0x04004190 RID: 16784
		private ColorGroup m_ColorGroup;

		// Token: 0x04004191 RID: 16785
		private Animation m_Animation;

		// Token: 0x04004192 RID: 16786
		private bool m_IsHold;

		// Token: 0x04004193 RID: 16787
		private float m_HoldCount;

		// Token: 0x04004194 RID: 16788
		private float m_BeforeTime;

		// Token: 0x04004195 RID: 16789
		private bool m_IsEnableInputParent = true;

		// Token: 0x04004196 RID: 16790
		private bool m_IsEnableInputSelf = true;

		// Token: 0x04004197 RID: 16791
		private bool m_IsDecided;

		// Token: 0x04004198 RID: 16792
		private bool m_DecideColorChange = true;

		// Token: 0x04004199 RID: 16793
		private Vector3 m_DefaultLocalPosition;

		// Token: 0x0400419A RID: 16794
		private Vector3 m_DefaultLocalScale;

		// Token: 0x0400419B RID: 16795
		private float m_DefaultRectWidth;

		// Token: 0x02000ADD RID: 2781
		private enum eButtonState
		{
			// Token: 0x0400419D RID: 16797
			None,
			// Token: 0x0400419E RID: 16798
			Wait,
			// Token: 0x0400419F RID: 16799
			Press,
			// Token: 0x040041A0 RID: 16800
			Release,
			// Token: 0x040041A1 RID: 16801
			Decide
		}

		// Token: 0x02000ADE RID: 2782
		public enum ePreset
		{
			// Token: 0x040041A3 RID: 16803
			None,
			// Token: 0x040041A4 RID: 16804
			CommonButton,
			// Token: 0x040041A5 RID: 16805
			Button,
			// Token: 0x040041A6 RID: 16806
			Icon,
			// Token: 0x040041A7 RID: 16807
			Panel,
			// Token: 0x040041A8 RID: 16808
			Tab
		}

		// Token: 0x02000ADF RID: 2783
		private enum eSwapObjState
		{
			// Token: 0x040041AA RID: 16810
			Default,
			// Token: 0x040041AB RID: 16811
			Press,
			// Token: 0x040041AC RID: 16812
			Decide
		}
	}
}
