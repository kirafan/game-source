﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B0 RID: 1968
	public class SelectedCharaInfoCharacterBustImage : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x0600289E RID: 10398 RVA: 0x000D98EA File Offset: 0x000D7CEA
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return new BustImageAdapter(this.m_BustImage);
		}

		// Token: 0x0600289F RID: 10399 RVA: 0x000D98F8 File Offset: 0x000D7CF8
		public override void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (base.GetState() != SelectedCharaInfoCharacterViewBase.eState.Wait)
			{
				return;
			}
			base.RequestLoad(charaID, weaponID, toStateAutomaticallyAdvance);
			Vector3? basePosition = this.m_BasePosition;
			if (basePosition == null)
			{
				this.m_BasePosition = new Vector3?(this.m_BustImage.RectTransform.localPosition);
				this.m_BaseScale = this.m_BustImage.RectTransform.localScale.x;
			}
			this.m_BustImage.RectTransform.localScale = Vector3.one * this.m_BaseScale * SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaIllustOffsetDB.GetParam(charaID).m_BustSupportScl;
			this.m_BustImage.RectTransform.localPosX(this.m_BasePosition.Value.x - SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaIllustOffsetDB.GetParam(charaID).m_BustSupportX * this.m_BustImage.RectTransform.localScale.y);
			this.m_BustImage.RectTransform.localPosY(this.m_BasePosition.Value.y - SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaIllustOffsetDB.GetParam(charaID).m_BustSupportY * this.m_BustImage.RectTransform.localScale.y);
		}

		// Token: 0x04002F93 RID: 12179
		[SerializeField]
		[Tooltip("Modelの親.")]
		private BustImage m_BustImage;

		// Token: 0x04002F94 RID: 12180
		private Vector3? m_BasePosition;

		// Token: 0x04002F95 RID: 12181
		private float m_BaseScale;
	}
}
