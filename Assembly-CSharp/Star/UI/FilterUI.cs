﻿using System;
using Star.UI.Filter;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000979 RID: 2425
	public class FilterUI : MenuUIBase
	{
		// Token: 0x0600321C RID: 12828 RVA: 0x000FFC54 File Offset: 0x000FE054
		public void Setup(UISettings.eUsingType_Filter usingType, Action OnExecute)
		{
			this.m_FilterWindow.Setup(usingType);
			this.m_FilterWindow.OnExecute += OnExecute;
			if (usingType != UISettings.eUsingType_Filter.Recommend)
			{
				if (usingType != UISettings.eUsingType_Filter.SupportTitle)
				{
					this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonFilter);
				}
				else
				{
					this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterTitleSelectTitle);
				}
			}
			else
			{
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RecommendParty);
			}
			if (usingType == UISettings.eUsingType_Filter.WeaponEquip)
			{
				this.m_WindowRect.sizeDelta = new Vector2(this.m_WindowRect.sizeDelta.x, 490f);
			}
			else if (usingType == UISettings.eUsingType_Filter.WeaponList)
			{
				this.m_WindowRect.sizeDelta = new Vector2(this.m_WindowRect.sizeDelta.x, 640f);
			}
			else if (usingType == UISettings.eUsingType_Filter.Present)
			{
				this.m_WindowRect.sizeDelta = new Vector2(this.m_WindowRect.sizeDelta.x, 640f);
			}
		}

		// Token: 0x0600321D RID: 12829 RVA: 0x000FFD90 File Offset: 0x000FE190
		private void Update()
		{
			switch (this.m_Step)
			{
			case FilterUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (this.m_AnimList[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(FilterUI.eStep.Idle);
				}
				break;
			}
			case FilterUI.eStep.Idle:
				if (!this.m_FilterWindow.IsOpen())
				{
					this.OnStartOut.Call();
				}
				break;
			case FilterUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (this.m_AnimList[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(FilterUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600321E RID: 12830 RVA: 0x000FFE80 File Offset: 0x000FE280
		private void ChangeStep(FilterUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case FilterUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case FilterUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case FilterUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case FilterUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case FilterUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600321F RID: 12831 RVA: 0x000FFF28 File Offset: 0x000FE328
		public override void PlayIn()
		{
			this.ChangeStep(FilterUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_FilterWindow.Open();
		}

		// Token: 0x06003220 RID: 12832 RVA: 0x000FFF70 File Offset: 0x000FE370
		public override void PlayOut()
		{
			this.ChangeStep(FilterUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
		}

		// Token: 0x06003221 RID: 12833 RVA: 0x000FFFAA File Offset: 0x000FE3AA
		public override bool IsMenuEnd()
		{
			return this.m_Step == FilterUI.eStep.End;
		}

		// Token: 0x0400383C RID: 14396
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x0400383D RID: 14397
		[SerializeField]
		private FilterWindow m_FilterWindow;

		// Token: 0x0400383E RID: 14398
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x0400383F RID: 14399
		[SerializeField]
		private RectTransform m_WindowRect;

		// Token: 0x04003840 RID: 14400
		private FilterUI.eStep m_Step;

		// Token: 0x04003841 RID: 14401
		public Action OnStartOut;

		// Token: 0x0200097A RID: 2426
		private enum eStep
		{
			// Token: 0x04003843 RID: 14403
			Hide,
			// Token: 0x04003844 RID: 14404
			In,
			// Token: 0x04003845 RID: 14405
			Idle,
			// Token: 0x04003846 RID: 14406
			Out,
			// Token: 0x04003847 RID: 14407
			End
		}
	}
}
