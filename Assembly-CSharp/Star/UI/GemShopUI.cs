﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009A9 RID: 2473
	public class GemShopUI : MenuUIBase
	{
		// Token: 0x1400008E RID: 142
		// (add) Token: 0x0600332B RID: 13099 RVA: 0x001044BC File Offset: 0x001028BC
		// (remove) Token: 0x0600332C RID: 13100 RVA: 0x001044F4 File Offset: 0x001028F4
		public event Action<int> OnRequestPurchase;

		// Token: 0x1400008F RID: 143
		// (add) Token: 0x0600332D RID: 13101 RVA: 0x0010452C File Offset: 0x0010292C
		// (remove) Token: 0x0600332E RID: 13102 RVA: 0x00104564 File Offset: 0x00102964
		public event Action<eAgeType> OnConfirmAge;

		// Token: 0x0600332F RID: 13103 RVA: 0x0010459C File Offset: 0x0010299C
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_Scroll.Setup();
			this.m_AgeConfirmGroup.OnEnd += this.OnCloseAgeConfirmGroup;
		}

		// Token: 0x06003330 RID: 13104 RVA: 0x001045F8 File Offset: 0x001029F8
		public void Refresh()
		{
			this.m_Scroll.RemoveAll();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.GetProducts().Count; i++)
			{
				StoreManager.Product product = SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.GetProducts()[i];
				this.m_Scroll.AddItem(new GemShopRecipeItemData(product.m_ID, product.m_Name, product.m_Desc, product.m_Price, product.m_uiType, new Action<int>(this.OnClickRecipeButtonCallBack)));
			}
			this.m_HaveNumText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true);
		}

		// Token: 0x06003331 RID: 13105 RVA: 0x001046AA File Offset: 0x00102AAA
		public void CloseAgeConfirm(Action onClose)
		{
			this.m_OnCloseAgeConfirm = onClose;
			this.m_AgeConfirmGroup.Close();
		}

		// Token: 0x06003332 RID: 13106 RVA: 0x001046BE File Offset: 0x00102ABE
		private void OnCloseAgeConfirmGroup()
		{
			this.m_OnCloseAgeConfirm.Call();
			this.m_OnCloseAgeConfirm = null;
		}

		// Token: 0x06003333 RID: 13107 RVA: 0x001046D4 File Offset: 0x00102AD4
		private void Update()
		{
			switch (this.m_Step)
			{
			case GemShopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(GemShopUI.eStep.Idle);
				}
				break;
			}
			case GemShopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(GemShopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003334 RID: 13108 RVA: 0x001047D0 File Offset: 0x00102BD0
		private void ChangeStep(GemShopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case GemShopUI.eStep.Hide:
				this.SetEnableInput(true);
				base.gameObject.SetActive(true);
				break;
			case GemShopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case GemShopUI.eStep.Idle:
				this.m_MainGroup.SetEnableInput(true);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case GemShopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case GemShopUI.eStep.End:
				this.SetEnableInput(true);
				base.gameObject.SetActive(true);
				break;
			}
		}

		// Token: 0x06003335 RID: 13109 RVA: 0x00104890 File Offset: 0x00102C90
		public override void PlayIn()
		{
			this.ChangeStep(GemShopUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003336 RID: 13110 RVA: 0x001048D8 File Offset: 0x00102CD8
		public void PlayInAgeConfirm()
		{
			this.ChangeStep(GemShopUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_AgeConfirmGroup.Open();
		}

		// Token: 0x06003337 RID: 13111 RVA: 0x00104920 File Offset: 0x00102D20
		public override void PlayOut()
		{
			this.ChangeStep(GemShopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
			this.m_AgeConfirmGroup.Close();
		}

		// Token: 0x06003338 RID: 13112 RVA: 0x00104970 File Offset: 0x00102D70
		public override bool IsMenuEnd()
		{
			return this.m_Step == GemShopUI.eStep.End;
		}

		// Token: 0x06003339 RID: 13113 RVA: 0x0010497B File Offset: 0x00102D7B
		public void OnCloseButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x0600333A RID: 13114 RVA: 0x00104983 File Offset: 0x00102D83
		public void OnClickRecipeButtonCallBack(int id)
		{
			this.OnRequestPurchase.Call(id);
		}

		// Token: 0x0600333B RID: 13115 RVA: 0x00104994 File Offset: 0x00102D94
		public void OnClickURLButton(int idx)
		{
			if (idx == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.WebView.Open(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WVGem0Title), WebDataListUtil.GetIDToAdress(1005));
			}
			if (idx == 1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.WebView.Open(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WVGem1Title), WebDataListUtil.GetIDToAdress(1003));
			}
		}

		// Token: 0x0600333C RID: 13116 RVA: 0x00104A08 File Offset: 0x00102E08
		public void OnClickAgeButton(int idx)
		{
			this.OnConfirmAge.Call(idx + eAgeType.Age15);
		}

		// Token: 0x04003964 RID: 14692
		private GemShopUI.eStep m_Step;

		// Token: 0x04003965 RID: 14693
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003966 RID: 14694
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003967 RID: 14695
		[SerializeField]
		private UIGroup m_AgeConfirmGroup;

		// Token: 0x04003968 RID: 14696
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003969 RID: 14697
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x0400396A RID: 14698
		private Action m_OnCloseAgeConfirm;

		// Token: 0x020009AA RID: 2474
		private enum eStep
		{
			// Token: 0x0400396E RID: 14702
			Hide,
			// Token: 0x0400396F RID: 14703
			In,
			// Token: 0x04003970 RID: 14704
			Idle,
			// Token: 0x04003971 RID: 14705
			Out,
			// Token: 0x04003972 RID: 14706
			End
		}
	}
}
