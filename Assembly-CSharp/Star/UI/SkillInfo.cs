﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000823 RID: 2083
	public class SkillInfo : MonoBehaviour
	{
		// Token: 0x06002B72 RID: 11122 RVA: 0x000E4700 File Offset: 0x000E2B00
		public void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, int lv = -1, int maxlv = 1, long remainExp = -1L)
		{
			if (skillID == -1)
			{
				this.ApplyEmpty();
				return;
			}
			if (this.m_Exist)
			{
				this.m_Exist.SetActive(true);
			}
			if (this.m_NoExist)
			{
				this.m_NoExist.SetActive(false);
			}
			this.m_SkillIconCloner.gameObject.SetActive(true);
			this.m_DetailText.gameObject.SetActive(true);
			SkillListDB_Param skillListDB_Param;
			switch (skillDBType)
			{
			case SkillIcon.eSkillDBType.Player:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(skillID);
				goto IL_E1;
			case SkillIcon.eSkillDBType.Master:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
				goto IL_E1;
			case SkillIcon.eSkillDBType.Weapon:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_WPN.GetParam(skillID);
				goto IL_E1;
			}
			skillListDB_Param = default(SkillListDB_Param);
			IL_E1:
			this.m_NameText.text = skillListDB_Param.m_SkillName;
			this.m_DetailText.text = skillListDB_Param.m_SkillDetail;
			this.m_SkillIconCloner.GetInst<SkillIcon>().Apply(skillDBType, skillID, ownerElement);
			if (lv > 0)
			{
				this.m_LvText.text = UIUtility.SkillLvValueToString(lv, maxlv, 0, 0);
			}
			else
			{
				this.m_LvText.text = null;
			}
			if (remainExp > 0L)
			{
				this.m_ExpRemainText.gameObject.SetActive(true);
				this.m_ExpRemainText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.SkillLvUpRemain, new object[]
				{
					remainExp
				});
			}
			else
			{
				this.m_ExpRemainText.gameObject.SetActive(false);
			}
		}

		// Token: 0x06002B73 RID: 11123 RVA: 0x000E48B3 File Offset: 0x000E2CB3
		public void ApplyEmpty()
		{
			if (this.m_Exist)
			{
				this.m_Exist.SetActive(false);
			}
			if (this.m_NoExist)
			{
				this.m_NoExist.SetActive(true);
			}
		}

		// Token: 0x040031CE RID: 12750
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040031CF RID: 12751
		[SerializeField]
		private PrefabCloner m_SkillIconCloner;

		// Token: 0x040031D0 RID: 12752
		[SerializeField]
		private Sprite[] m_IconSprites;

		// Token: 0x040031D1 RID: 12753
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040031D2 RID: 12754
		[SerializeField]
		private Text m_LvText;

		// Token: 0x040031D3 RID: 12755
		[SerializeField]
		private Text m_ExpRemainText;

		// Token: 0x040031D4 RID: 12756
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x040031D5 RID: 12757
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x02000824 RID: 2084
		public enum eSkillIconSprite
		{
			// Token: 0x040031D7 RID: 12759
			Unique,
			// Token: 0x040031D8 RID: 12760
			Atk,
			// Token: 0x040031D9 RID: 12761
			Mgc,
			// Token: 0x040031DA RID: 12762
			Recover,
			// Token: 0x040031DB RID: 12763
			Buff,
			// Token: 0x040031DC RID: 12764
			Debuff
		}
	}
}
