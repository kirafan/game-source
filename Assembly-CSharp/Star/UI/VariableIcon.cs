﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000876 RID: 2166
	public class VariableIcon : MonoBehaviour
	{
		// Token: 0x06002CD3 RID: 11475 RVA: 0x000EC740 File Offset: 0x000EAB40
		private void Prepare()
		{
			if (!this.m_IsDonePrepare)
			{
				this.m_CharaIcon = this.m_IconImage.GetComponent<CharaIcon>();
				this.m_ItemIcon = this.m_IconImage.GetComponent<ItemIcon>();
				this.m_WeaponIcon = this.m_IconImage.GetComponent<WeaponIcon>();
				this.m_TownObjectIcon = this.m_IconImage.GetComponent<TownObjectIcon>();
				this.m_RoomObjectIcon = this.m_IconImage.GetComponent<RoomObjectIcon>();
				this.m_OrbIcon = this.m_IconImage.GetComponent<OrbIcon>();
				this.DestroyAllIcon();
				this.m_IsDonePrepare = true;
			}
		}

		// Token: 0x06002CD4 RID: 11476 RVA: 0x000EC7CC File Offset: 0x000EABCC
		private void DestroyAllIcon()
		{
			if (this.m_IsDonePrepare)
			{
				this.m_CharaIcon.Destroy();
				this.m_ItemIcon.Destroy();
				this.m_WeaponIcon.Destroy();
				this.m_TownObjectIcon.Destroy();
				this.m_RoomObjectIcon.Destroy();
				this.m_OrbIcon.Destroy();
			}
			this.m_CharaIcon.enabled = false;
			this.m_ItemIcon.enabled = false;
			this.m_WeaponIcon.enabled = false;
			this.m_TownObjectIcon.enabled = false;
			this.m_RoomObjectIcon.enabled = false;
			this.m_OrbIcon.enabled = false;
		}

		// Token: 0x06002CD5 RID: 11477 RVA: 0x000EC870 File Offset: 0x000EAC70
		public void ApplyChara(int charaID)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Chara || this.m_ID != charaID)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Chara;
				this.m_ID = charaID;
				this.m_CharaIcon.enabled = true;
				this.m_CharaIcon.Apply(charaID);
			}
		}

		// Token: 0x06002CD6 RID: 11478 RVA: 0x000EC8C8 File Offset: 0x000EACC8
		public void ApplyItem(int itemID)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Item || this.m_ID != itemID)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Item;
				this.m_ID = itemID;
				this.m_ItemIcon.enabled = true;
				this.m_ItemIcon.Apply(itemID);
			}
		}

		// Token: 0x06002CD7 RID: 11479 RVA: 0x000EC920 File Offset: 0x000EAD20
		public void ApplyWeapon(int weaponID)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Weapon || this.m_ID != weaponID)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Weapon;
				this.m_ID = weaponID;
				this.m_WeaponIcon.enabled = true;
				this.m_WeaponIcon.Apply(weaponID);
			}
		}

		// Token: 0x06002CD8 RID: 11480 RVA: 0x000EC978 File Offset: 0x000EAD78
		public void ApplyTownObject(int townObjectID, int lv = 1)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.TownObject || this.m_ID != townObjectID || this.m_SubID != lv)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.TownObject;
				this.m_ID = townObjectID;
				this.m_SubID = lv;
				this.m_TownObjectIcon.enabled = true;
				this.m_TownObjectIcon.Apply(townObjectID, lv);
			}
		}

		// Token: 0x06002CD9 RID: 11481 RVA: 0x000EC9E4 File Offset: 0x000EADE4
		public void ApplyRoomObject(eRoomObjectCategory category, int roomObjectID)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.RoomObject || this.m_ID != (int)category || this.m_SubID != roomObjectID)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.RoomObject;
				this.m_ID = (int)category;
				this.m_SubID = roomObjectID;
				this.m_RoomObjectIcon.enabled = true;
				this.m_RoomObjectIcon.Apply(category, roomObjectID);
			}
		}

		// Token: 0x06002CDA RID: 11482 RVA: 0x000ECA50 File Offset: 0x000EAE50
		public void ApplyOrb(int orbID)
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Orb || this.m_ID != orbID)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Orb;
				this.m_ID = orbID;
				this.m_OrbIcon.enabled = true;
				this.m_OrbIcon.Apply(orbID);
			}
		}

		// Token: 0x06002CDB RID: 11483 RVA: 0x000ECAA7 File Offset: 0x000EAEA7
		public void ApplyGold()
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Gold)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Gold;
				this.m_IconImage.sprite = this.m_GoldSprite;
				this.m_IconImage.enabled = true;
			}
		}

		// Token: 0x06002CDC RID: 11484 RVA: 0x000ECAE5 File Offset: 0x000EAEE5
		public void ApplyGem()
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Gem)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Gem;
				this.m_IconImage.sprite = this.m_GemSprite;
				this.m_IconImage.enabled = true;
			}
		}

		// Token: 0x06002CDD RID: 11485 RVA: 0x000ECB23 File Offset: 0x000EAF23
		public void ApplyKirara()
		{
			this.Prepare();
			if (this.m_Type != VariableIcon.eType.Kirara)
			{
				this.DestroyAllIcon();
				this.m_Type = VariableIcon.eType.Kirara;
				this.m_IconImage.sprite = this.m_KPSprite;
				this.m_IconImage.enabled = true;
			}
		}

		// Token: 0x06002CDE RID: 11486 RVA: 0x000ECB63 File Offset: 0x000EAF63
		public void Destroy()
		{
			if (this.m_IsDonePrepare)
			{
				this.DestroyAllIcon();
			}
		}

		// Token: 0x040033CC RID: 13260
		[SerializeField]
		private Sprite m_GoldSprite;

		// Token: 0x040033CD RID: 13261
		[SerializeField]
		private Sprite m_GemSprite;

		// Token: 0x040033CE RID: 13262
		[SerializeField]
		private Sprite m_KPSprite;

		// Token: 0x040033CF RID: 13263
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040033D0 RID: 13264
		private VariableIcon.eType m_Type;

		// Token: 0x040033D1 RID: 13265
		private int m_ID = -1;

		// Token: 0x040033D2 RID: 13266
		private int m_SubID = -1;

		// Token: 0x040033D3 RID: 13267
		private CharaIcon m_CharaIcon;

		// Token: 0x040033D4 RID: 13268
		private ItemIcon m_ItemIcon;

		// Token: 0x040033D5 RID: 13269
		private WeaponIcon m_WeaponIcon;

		// Token: 0x040033D6 RID: 13270
		private TownObjectIcon m_TownObjectIcon;

		// Token: 0x040033D7 RID: 13271
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x040033D8 RID: 13272
		private OrbIcon m_OrbIcon;

		// Token: 0x040033D9 RID: 13273
		private bool m_IsDonePrepare;

		// Token: 0x02000877 RID: 2167
		public enum eType
		{
			// Token: 0x040033DB RID: 13275
			None,
			// Token: 0x040033DC RID: 13276
			Chara,
			// Token: 0x040033DD RID: 13277
			Item,
			// Token: 0x040033DE RID: 13278
			Weapon,
			// Token: 0x040033DF RID: 13279
			TownObject,
			// Token: 0x040033E0 RID: 13280
			RoomObject,
			// Token: 0x040033E1 RID: 13281
			Orb,
			// Token: 0x040033E2 RID: 13282
			Gem,
			// Token: 0x040033E3 RID: 13283
			Gold,
			// Token: 0x040033E4 RID: 13284
			Kirara
		}
	}
}
