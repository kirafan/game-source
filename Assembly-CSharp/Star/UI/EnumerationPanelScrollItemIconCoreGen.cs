﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200096F RID: 2415
	public class EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelScrollItemIconBase where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapterBase where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x060031F9 RID: 12793 RVA: 0x000FF934 File Offset: 0x000FDD34
		public override void Destroy()
		{
			if (this.m_CharaPanels != null)
			{
				for (int i = 0; i < this.m_CharaPanels.Length; i++)
				{
					this.m_CharaPanels[i].Destroy();
				}
			}
		}

		// Token: 0x060031FA RID: 12794 RVA: 0x000FF980 File Offset: 0x000FDD80
		protected override CharaPanelType InstantiateProcess()
		{
			if (this.m_SharedIntance.m_CharaPanelPrefab == null)
			{
				return (CharaPanelType)((object)null);
			}
			RectTransform charaPanelParent = base.GetCharaPanelParent();
			if (charaPanelParent == null && charaPanelParent == null)
			{
				this.m_SharedIntance.m_CharaPanelParent = (this.m_SharedIntance.parent.transform as RectTransform);
				charaPanelParent = base.GetCharaPanelParent();
			}
			return UnityEngine.Object.Instantiate<CharaPanelType>(this.m_SharedIntance.m_CharaPanelPrefab, base.GetCharaPanelParent(), false);
		}

		// Token: 0x060031FB RID: 12795 RVA: 0x000FFA28 File Offset: 0x000FDE28
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
			if (charaPanel == null)
			{
				return;
			}
			charaPanel.Setup(this.m_SharedIntance.parent);
			charaPanel.SetMode(EnumerationPanelCoreBase.ConvertEnumerationPanelCoreBaseeModeToEnumerationCharaPanelBaseeMode(this.m_Mode));
			charaPanel.Apply(partyMemberController);
		}

		// Token: 0x060031FC RID: 12796 RVA: 0x000FFA8F File Offset: 0x000FDE8F
		public override SetupArgumentType GetSharedIntance()
		{
			return this.m_SharedIntance;
		}

		// Token: 0x060031FD RID: 12797 RVA: 0x000FFA97 File Offset: 0x000FDE97
		public override void SetSharedIntance(SetupArgumentType arg)
		{
			this.m_SharedIntance = arg;
		}

		// Token: 0x04003827 RID: 14375
		protected SetupArgumentType m_SharedIntance;

		// Token: 0x02000970 RID: 2416
		[Serializable]
		public class SharedInstanceEx<ThisType> : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ThisType : EnumerationPanelScrollItemIconCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<ThisType>
		{
			// Token: 0x060031FE RID: 12798 RVA: 0x000FFAA0 File Offset: 0x000FDEA0
			public SharedInstanceEx()
			{
			}

			// Token: 0x060031FF RID: 12799 RVA: 0x000FFAA8 File Offset: 0x000FDEA8
			public SharedInstanceEx(ThisType argument) : base(argument)
			{
				this.Clone(argument);
			}

			// Token: 0x06003200 RID: 12800 RVA: 0x000FFABC File Offset: 0x000FDEBC
			public override ThisType Clone(ThisType argument)
			{
				ThisType thisType = base.Clone(argument);
				return thisType.CloneNewMemberOnly(argument);
			}

			// Token: 0x06003201 RID: 12801 RVA: 0x000FFADF File Offset: 0x000FDEDF
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				return (ThisType)((object)this);
			}
		}
	}
}
