﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008B4 RID: 2228
	public class UIEmitter : MonoBehaviour
	{
		// Token: 0x06002E3D RID: 11837 RVA: 0x000F38F0 File Offset: 0x000F1CF0
		private void Start()
		{
			if (this.m_AutoStart)
			{
				this.Play();
			}
		}

		// Token: 0x06002E3E RID: 11838 RVA: 0x000F3903 File Offset: 0x000F1D03
		public void Play()
		{
			this.m_State = UIEmitter.eState.Active;
		}

		// Token: 0x06002E3F RID: 11839 RVA: 0x000F390C File Offset: 0x000F1D0C
		public void Stop()
		{
			this.m_State = UIEmitter.eState.NotActive;
		}

		// Token: 0x06002E40 RID: 11840 RVA: 0x000F3918 File Offset: 0x000F1D18
		private void Update()
		{
			UIEmitter.eState state = this.m_State;
			if (state != UIEmitter.eState.Error && state != UIEmitter.eState.NotActive)
			{
				if (state == UIEmitter.eState.Active)
				{
					this.m_WaitTime += Time.deltaTime;
					if (this.m_InstantiateIntervalTime < this.m_WaitTime)
					{
						this.m_WaitTime -= this.m_WaitTime;
						UIParticle uiparticle = UnityEngine.Object.Instantiate<UIParticle>(this.m_ParticleBasePrefab);
						RectTransform component = uiparticle.GetComponent<RectTransform>();
						component.SetParent(base.transform);
						component.localScale = Vector3.one;
						component.localPosition = Vector3.zero;
						uiparticle.SetImageDefaultSizeInformation(new UIParticle.ImageAxisDefaultSizeInformation
						{
							m_ParentRectTransform = base.GetComponent<RectTransform>(),
							m_Sprite = this.m_ParticleBaseSprites[this.m_CreateSpriteIndex++],
							m_Value = this.m_ImageAxisDefaultSizeInformationValue
						});
						uiparticle.SetActionTime(this.m_ChildActionTime);
						uiparticle.SetMoveX(this.m_ChildActionX.m_Move);
						uiparticle.SetMoveY(this.m_ChildActionY.m_Move);
						uiparticle.SetMoveEaseTypeX(this.m_ChildActionX.m_MoveEaseType);
						uiparticle.SetMoveEaseTypeY(this.m_ChildActionY.m_MoveEaseType);
						uiparticle.SetMoveLoopTypeX(this.m_ChildActionX.m_MoveLoopType);
						uiparticle.SetMoveLoopTypeY(this.m_ChildActionY.m_MoveLoopType);
						uiparticle.Play();
						if (this.m_ParticleBaseSprites.Length <= this.m_CreateSpriteIndex)
						{
							this.m_CreateSpriteIndex = 0;
						}
					}
				}
			}
		}

		// Token: 0x0400355C RID: 13660
		[SerializeField]
		private bool m_AutoStart;

		// Token: 0x0400355D RID: 13661
		[SerializeField]
		[Tooltip("パーティクルの元リソース.現状順番に割り当てる.")]
		private Sprite[] m_ParticleBaseSprites;

		// Token: 0x0400355E RID: 13662
		[SerializeField]
		[Tooltip("パーティクルの元Prefab.")]
		private UIParticle m_ParticleBasePrefab;

		// Token: 0x0400355F RID: 13663
		[SerializeField]
		private float m_InstantiateIntervalTime = 1f;

		// Token: 0x04003560 RID: 13664
		private float m_WaitTime;

		// Token: 0x04003561 RID: 13665
		private int m_CreateSpriteIndex;

		// Token: 0x04003562 RID: 13666
		[SerializeField]
		private UIParticle.ImageAxisDefaultSizeInformationValue m_ImageAxisDefaultSizeInformationValue;

		// Token: 0x04003563 RID: 13667
		[SerializeField]
		private float m_ChildActionTime;

		// Token: 0x04003564 RID: 13668
		[SerializeField]
		private UIEmitter.UIEmitterChildActionInformation m_ChildActionX;

		// Token: 0x04003565 RID: 13669
		[SerializeField]
		private UIEmitter.UIEmitterChildActionInformation m_ChildActionY;

		// Token: 0x04003566 RID: 13670
		private UIEmitter.eState m_State = UIEmitter.eState.NotActive;

		// Token: 0x020008B5 RID: 2229
		[Serializable]
		public class UIEmitterChildActionInformation
		{
			// Token: 0x04003567 RID: 13671
			[SerializeField]
			public float m_Move;

			// Token: 0x04003568 RID: 13672
			[SerializeField]
			public iTween.EaseType m_MoveEaseType = iTween.EaseType.linear;

			// Token: 0x04003569 RID: 13673
			[SerializeField]
			public iTween.LoopType m_MoveLoopType;
		}

		// Token: 0x020008B6 RID: 2230
		public enum eState
		{
			// Token: 0x0400356B RID: 13675
			Error,
			// Token: 0x0400356C RID: 13676
			NotActive,
			// Token: 0x0400356D RID: 13677
			Active
		}
	}
}
