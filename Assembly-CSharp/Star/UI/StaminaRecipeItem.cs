﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000AC8 RID: 2760
	public class StaminaRecipeItem : ScrollItemIcon
	{
		// Token: 0x060039AE RID: 14766 RVA: 0x001260CC File Offset: 0x001244CC
		protected override void ApplyData()
		{
			StaminaRecipeItemData staminaRecipeItemData = (StaminaRecipeItemData)this.m_Data;
			if (staminaRecipeItemData.m_ItemID == -1)
			{
				this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyGem();
				this.m_GemNumObj.SetActive(true);
			}
			else
			{
				this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyItem(staminaRecipeItemData.m_ItemID);
				this.m_GemNumObj.SetActive(false);
			}
			this.m_TitleNameText.text = staminaRecipeItemData.m_Title;
			this.m_HavenumText.text = staminaRecipeItemData.m_HaveNumText;
			this.m_DetailText.text = staminaRecipeItemData.m_DetailText;
			this.m_IsDark = !staminaRecipeItemData.m_Interactable;
			base.ApplyData();
		}

		// Token: 0x040040E6 RID: 16614
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x040040E7 RID: 16615
		[SerializeField]
		private Text m_TitleNameText;

		// Token: 0x040040E8 RID: 16616
		[SerializeField]
		private Text m_HavenumText;

		// Token: 0x040040E9 RID: 16617
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040040EA RID: 16618
		[SerializeField]
		private GameObject m_GemNumObj;
	}
}
