﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI
{
	// Token: 0x02000828 RID: 2088
	public class ToggleSwitch : MonoBehaviour
	{
		// Token: 0x06002B8A RID: 11146 RVA: 0x000E4E95 File Offset: 0x000E3295
		private void Start()
		{
			this.m_OnButton.AddListerner(new UnityAction(this.OnClickOnButtonCallBack));
			this.m_OffButton.AddListerner(new UnityAction(this.OnClickOffButtonCallBack));
		}

		// Token: 0x06002B8B RID: 11147 RVA: 0x000E4EC5 File Offset: 0x000E32C5
		public void SetDecided(bool isOn)
		{
			this.m_OnButton.IsDecided = isOn;
			this.m_OffButton.IsDecided = !isOn;
		}

		// Token: 0x06002B8C RID: 11148 RVA: 0x000E4EE2 File Offset: 0x000E32E2
		public void SetInteractable(bool interactable)
		{
			this.m_OnButton.Interactable = interactable;
			this.m_OffButton.Interactable = interactable;
		}

		// Token: 0x06002B8D RID: 11149 RVA: 0x000E4EFC File Offset: 0x000E32FC
		public bool IsOn()
		{
			return this.m_OnButton.IsDecided;
		}

		// Token: 0x06002B8E RID: 11150 RVA: 0x000E4F09 File Offset: 0x000E3309
		public void OnClickOnButtonCallBack()
		{
			this.SetDecided(true);
			this.m_OnToggle.Invoke();
		}

		// Token: 0x06002B8F RID: 11151 RVA: 0x000E4F1D File Offset: 0x000E331D
		public void OnClickOffButtonCallBack()
		{
			this.SetDecided(false);
			this.m_OnToggle.Invoke();
		}

		// Token: 0x040031EC RID: 12780
		[SerializeField]
		private CustomButton m_OnButton;

		// Token: 0x040031ED RID: 12781
		[SerializeField]
		private CustomButton m_OffButton;

		// Token: 0x040031EE RID: 12782
		[SerializeField]
		private UnityEvent m_OnToggle;
	}
}
