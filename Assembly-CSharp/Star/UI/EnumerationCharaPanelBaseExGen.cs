﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200095F RID: 2399
	public abstract class EnumerationCharaPanelBaseExGen<ParentType, SetupArgumentType> : EnumerationCharaPanelBase where ParentType : MonoBehaviour where SetupArgumentType : EnumerationCharaPanelBase.SharedInstanceExGen<ParentType>
	{
		// Token: 0x060031B0 RID: 12720 RVA: 0x000FE774 File Offset: 0x000FCB74
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
			base.Setup(argument);
			this.m_SharedIntance = (SetupArgumentType)((object)argument);
			this.m_SharedIntance.m_CharaInfo.Setup();
		}

		// Token: 0x060031B1 RID: 12721 RVA: 0x000FE79E File Offset: 0x000FCB9E
		public override void Destroy()
		{
			if (this.m_SharedIntance.m_CharaInfo != null)
			{
				this.m_SharedIntance.m_CharaInfo.Destroy();
			}
		}

		// Token: 0x060031B2 RID: 12722 RVA: 0x000FE7D0 File Offset: 0x000FCBD0
		private void ApplyPanelState(eCharacterDataType characterDataType)
		{
			EnumerationCharaPanelBase.eMode mode = base.GetMode();
			bool flag = CharacterDataController.IsExistCharacterStatic(characterDataType);
			this.SetActive(flag);
			WeaponIconWithFrame.eMode weaponIconMode;
			if (flag)
			{
				weaponIconMode = EnumerationCharaPanelBase.ConvertEnumerationCharaPanelBaseeModeToWeaponIconWithFrameeMode(mode);
			}
			else
			{
				weaponIconMode = WeaponIconWithFrame.eMode.Disable;
				this.FriendMode(characterDataType == eCharacterDataType.Lock);
			}
			this.SetWeaponIconMode(weaponIconMode);
		}

		// Token: 0x060031B3 RID: 12723 RVA: 0x000FE81C File Offset: 0x000FCC1C
		private void SetActive(bool flag)
		{
			this.m_SharedIntance.m_DataContent.SetActive(flag);
			if (this.m_SharedIntance.m_Blank)
			{
				this.m_SharedIntance.m_Blank.SetActive(!flag);
			}
		}

		// Token: 0x060031B4 RID: 12724 RVA: 0x000FE874 File Offset: 0x000FCC74
		private void FriendMode(bool flg)
		{
			if (this.m_SharedIntance.m_FriendObj != null)
			{
				this.m_SharedIntance.m_FriendObj.SetActive(flg);
			}
			if (this.m_SharedIntance.m_EmptyObj != null)
			{
				this.m_SharedIntance.m_EmptyObj.SetActive(!flg);
			}
			this.m_SharedIntance.parent.GetComponentInChildren<CustomButton>().IsEnableInput = !flg;
		}

		// Token: 0x060031B5 RID: 12725 RVA: 0x000FE90C File Offset: 0x000FCD0C
		protected virtual void ApplyProcess(EquipWeaponPartyMemberController partyMemberController)
		{
			eCharacterDataType eCharacterDataType = (partyMemberController == null) ? eCharacterDataType.Lock : partyMemberController.GetCharacterDataType();
			this.ApplyPanelState(eCharacterDataType);
			if (eCharacterDataType != eCharacterDataType.Empty)
			{
				if (eCharacterDataType != eCharacterDataType.Lock)
				{
					this.ApplyExistCharacter(partyMemberController);
				}
				else
				{
					this.ApplyLock();
				}
			}
			else
			{
				this.ApplyEmpty();
			}
			this.m_SharedIntance.m_CharaInfo.Apply(partyMemberController);
		}

		// Token: 0x060031B6 RID: 12726 RVA: 0x000FE97E File Offset: 0x000FCD7E
		protected virtual void ApplyExistCharacter(EquipWeaponPartyMemberController partyMemberController)
		{
		}

		// Token: 0x060031B7 RID: 12727 RVA: 0x000FE980 File Offset: 0x000FCD80
		protected virtual void ApplyEmpty()
		{
		}

		// Token: 0x060031B8 RID: 12728 RVA: 0x000FE982 File Offset: 0x000FCD82
		protected virtual void ApplyLock()
		{
		}

		// Token: 0x060031B9 RID: 12729 RVA: 0x000FE984 File Offset: 0x000FCD84
		private void SetWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
			this.m_SharedIntance.m_CharaInfo.SetStateWeaponIconMode(mode);
		}

		// Token: 0x04003816 RID: 14358
		protected SetupArgumentType m_SharedIntance;
	}
}
