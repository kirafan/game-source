﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000AEE RID: 2798
	public class SpecialTweet : MonoBehaviour
	{
		// Token: 0x06003AA2 RID: 15010 RVA: 0x0012B05C File Offset: 0x0012945C
		public bool IsPlaying()
		{
			return this.m_Step != SpecialTweet.eStep.None;
		}

		// Token: 0x06003AA3 RID: 15011 RVA: 0x0012B06C File Offset: 0x0012946C
		public void Play(long mngID, int tweetID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(mngID);
			int charaID = userCharaData.Param.CharaID;
			TweetListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(userCharaData.Param.NamedType).GetParam(tweetID);
			string text = param.m_Text;
			this.m_ReqVocieID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(userCharaData.Param.NamedType, param.m_VoiceCueName, true);
			base.gameObject.SetActive(true);
			this.m_TalkText.text = text;
			this.m_CharaIllust.Apply(charaID, CharaIllust.eCharaIllustType.Bust);
			this.m_DarkBG.PlayIn();
			this.m_TimeCount = 0f;
			this.m_Step = SpecialTweet.eStep.Loading;
		}

		// Token: 0x06003AA4 RID: 15012 RVA: 0x0012B12C File Offset: 0x0012952C
		public void PlayVoiceOnly(long mngID, eSoundVoiceListDB voiceID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(mngID);
			int charaID = userCharaData.Param.CharaID;
			this.m_ReqVocieID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(userCharaData.Param.NamedType, voiceID, true);
			base.gameObject.SetActive(true);
			this.m_CharaIllust.Apply(charaID, CharaIllust.eCharaIllustType.Bust);
			this.m_TalkText.text = string.Empty;
			this.m_DarkBG.PlayIn();
			this.m_TimeCount = 0f;
			this.m_Step = SpecialTweet.eStep.Loading;
		}

		// Token: 0x06003AA5 RID: 15013 RVA: 0x0012B1C0 File Offset: 0x001295C0
		private void Update()
		{
			SpecialTweet.eStep step = this.m_Step;
			if (step != SpecialTweet.eStep.Loading)
			{
				if (step != SpecialTweet.eStep.Play)
				{
					if (step == SpecialTweet.eStep.PlayOut)
					{
						if (this.m_CharaAnim.IsEnd && this.m_TextAnim.IsEnd)
						{
							this.m_CharaAnim.Hide();
							this.m_TextAnim.Hide();
							this.m_DarkBG.Hide();
							base.gameObject.SetActive(false);
							this.m_Step = SpecialTweet.eStep.None;
							this.OnComplete.Call();
						}
					}
				}
				else
				{
					this.m_TimeCount += Time.deltaTime;
					if (this.m_ReqVocieID != -1 && !SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_ReqVocieID) && this.m_TimeCount > 2f)
					{
						this.PlayOut();
					}
				}
			}
			else if (this.m_CharaIllust.IsDoneLoad())
			{
				this.m_CharaAnim.PlayIn();
				if (!string.IsNullOrEmpty(this.m_TalkText.text))
				{
					this.m_TextAnim.PlayIn();
				}
				this.m_Step = SpecialTweet.eStep.Play;
			}
		}

		// Token: 0x06003AA6 RID: 15014 RVA: 0x0012B2EC File Offset: 0x001296EC
		private void PlayOut()
		{
			this.m_CharaAnim.PlayOut();
			if (!string.IsNullOrEmpty(this.m_TalkText.text))
			{
				this.m_TextAnim.PlayOut();
			}
			this.m_DarkBG.PlayOut();
			this.m_Step = SpecialTweet.eStep.PlayOut;
		}

		// Token: 0x06003AA7 RID: 15015 RVA: 0x0012B32B File Offset: 0x0012972B
		public void OnClickSkipButtonCallBack()
		{
			if (this.m_Step == SpecialTweet.eStep.Play)
			{
				this.PlayOut();
			}
		}

		// Token: 0x04004215 RID: 16917
		private const float SKIP_TIME = 2f;

		// Token: 0x04004216 RID: 16918
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04004217 RID: 16919
		[SerializeField]
		private AnimUIPlayer m_DarkBG;

		// Token: 0x04004218 RID: 16920
		[SerializeField]
		private AnimUIPlayer m_CharaAnim;

		// Token: 0x04004219 RID: 16921
		[SerializeField]
		private AnimUIPlayer m_TextAnim;

		// Token: 0x0400421A RID: 16922
		[SerializeField]
		private Text m_TalkText;

		// Token: 0x0400421B RID: 16923
		private float m_TimeCount;

		// Token: 0x0400421C RID: 16924
		private int m_ReqVocieID = -1;

		// Token: 0x0400421D RID: 16925
		private SpecialTweet.eStep m_Step;

		// Token: 0x0400421E RID: 16926
		public Action OnComplete;

		// Token: 0x02000AEF RID: 2799
		public enum eStep
		{
			// Token: 0x04004220 RID: 16928
			None,
			// Token: 0x04004221 RID: 16929
			Loading,
			// Token: 0x04004222 RID: 16930
			Play,
			// Token: 0x04004223 RID: 16931
			PlayOut
		}
	}
}
