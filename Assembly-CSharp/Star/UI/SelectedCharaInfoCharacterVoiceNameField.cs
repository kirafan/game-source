﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007DA RID: 2010
	public class SelectedCharaInfoCharacterVoiceNameField : SelectedCharaInfoHaveTitleTextField
	{
		// Token: 0x06002971 RID: 10609 RVA: 0x000DBA9B File Offset: 0x000D9E9B
		public void SetCharacterVoiceName(string text)
		{
			base.SetText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ProfileProfileCharacterVoiceTextHead) + text);
			this.m_Frames = 30;
		}

		// Token: 0x06002972 RID: 10610 RVA: 0x000DBAC8 File Offset: 0x000D9EC8
		private void Update()
		{
			if (0 < this.m_Frames)
			{
				RectTransform rectTransform = base.transform as RectTransform;
				for (int i = 0; i < 4; i++)
				{
					rectTransform = (rectTransform.parent as RectTransform);
				}
				LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
				this.m_Frames--;
			}
		}

		// Token: 0x04003002 RID: 12290
		public const int REBUILD_FRAMES = 30;

		// Token: 0x04003003 RID: 12291
		private int m_Frames;
	}
}
