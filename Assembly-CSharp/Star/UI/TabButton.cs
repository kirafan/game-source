﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008AA RID: 2218
	public class TabButton : MonoBehaviour
	{
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06002DF7 RID: 11767 RVA: 0x000F1F1C File Offset: 0x000F031C
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06002DF8 RID: 11768 RVA: 0x000F1F41 File Offset: 0x000F0341
		public CustomButton Button
		{
			get
			{
				return this.m_Button;
			}
		}

		// Token: 0x1400006D RID: 109
		// (add) Token: 0x06002DF9 RID: 11769 RVA: 0x000F1F4C File Offset: 0x000F034C
		// (remove) Token: 0x06002DFA RID: 11770 RVA: 0x000F1F84 File Offset: 0x000F0384
		public event Action<int> OnClick;

		// Token: 0x06002DFB RID: 11771 RVA: 0x000F1FBC File Offset: 0x000F03BC
		public void Setup(int idx, string text)
		{
			this.m_Idx = idx;
			this.m_Text.text = text;
			if (this.m_Image != null)
			{
				this.m_Image.gameObject.SetActive(false);
			}
			if (this.m_Text != null)
			{
				this.m_Text.gameObject.SetActive(true);
			}
		}

		// Token: 0x06002DFC RID: 11772 RVA: 0x000F2020 File Offset: 0x000F0420
		public void Setup(int idx, Sprite sprite)
		{
			this.m_Idx = idx;
			this.m_Image.sprite = sprite;
			RectTransform component = this.m_Image.GetComponent<RectTransform>();
			Vector2 lowestSize = this.GetLowestSize();
			lowestSize = new Vector2(lowestSize.x - 24f, lowestSize.y);
			component.sizeDelta = lowestSize;
			if (this.m_Image != null)
			{
				this.m_Image.gameObject.SetActive(true);
			}
			if (this.m_Text != null)
			{
				this.m_Text.text = string.Empty;
				this.m_Text.gameObject.SetActive(false);
			}
		}

		// Token: 0x06002DFD RID: 11773 RVA: 0x000F20CC File Offset: 0x000F04CC
		public void SetWidth(float width)
		{
			this.RectTransform.sizeDelta = new Vector2(width, this.RectTransform.sizeDelta.y);
		}

		// Token: 0x06002DFE RID: 11774 RVA: 0x000F2100 File Offset: 0x000F0500
		public Vector2 GetLowestSize()
		{
			if (this.m_Image != null && this.m_Image.sprite != null)
			{
				float num = 1f;
				if (32f < this.m_Image.sprite.rect.height)
				{
					num = 32f / this.m_Image.sprite.rect.height;
				}
				return new Vector2(this.m_Image.sprite.rect.width * num + 24f, this.m_Image.sprite.rect.height * num);
			}
			return new Vector2(this.m_Text.preferredWidth + 24f, this.m_Text.preferredHeight);
		}

		// Token: 0x06002DFF RID: 11775 RVA: 0x000F21DD File Offset: 0x000F05DD
		public void OnClickButtonCallBack()
		{
			this.OnClick.Call(this.m_Idx);
		}

		// Token: 0x04003506 RID: 13574
		private const float MARGIN = 24f;

		// Token: 0x04003507 RID: 13575
		private const float IMAGE_HEIGHT = 32f;

		// Token: 0x04003508 RID: 13576
		private const float INACTIVE_POSITION_Y = -4f;

		// Token: 0x04003509 RID: 13577
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x0400350A RID: 13578
		[SerializeField]
		private Image m_Image;

		// Token: 0x0400350B RID: 13579
		[SerializeField]
		private Text m_Text;

		// Token: 0x0400350C RID: 13580
		private int m_Idx = -1;

		// Token: 0x0400350D RID: 13581
		private RectTransform m_RectTransform;
	}
}
