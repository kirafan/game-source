﻿using System;

namespace Star.UI
{
	// Token: 0x02000863 RID: 2147
	public class ItemIcon : ASyncImage
	{
		// Token: 0x06002C98 RID: 11416 RVA: 0x000EB8F4 File Offset: 0x000E9CF4
		public void Apply(int itemID)
		{
			if (this.m_ItemID != itemID)
			{
				this.Apply();
				if (itemID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncItemIcon(itemID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_ItemID = itemID;
			}
		}

		// Token: 0x06002C99 RID: 11417 RVA: 0x000EB95A File Offset: 0x000E9D5A
		public override void Destroy()
		{
			base.Destroy();
			this.m_ItemID = -1;
		}

		// Token: 0x0400339A RID: 13210
		protected int m_ItemID = -1;
	}
}
