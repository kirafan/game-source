﻿using System;

namespace Star.UI
{
	// Token: 0x02000977 RID: 2423
	public abstract class EnumerationPanelConstGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where ThisType : EnumerationPanelConstGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelAdapterConstExGen<ThisType> where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelConstCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
	}
}
