﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000803 RID: 2051
	public class CharaUnlockWindow : MonoBehaviour
	{
		// Token: 0x06002A7E RID: 10878 RVA: 0x000E019B File Offset: 0x000DE59B
		private void Start()
		{
			this.m_MainGroup.OnEnd += this.OnEnd;
		}

		// Token: 0x06002A7F RID: 10879 RVA: 0x000E01B4 File Offset: 0x000DE5B4
		public void AddPopParam(int charaID, int beforeFriendship, int afterFriendship)
		{
			eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
			for (int i = beforeFriendship + 1; i <= afterFriendship; i++)
			{
				bool flag = false;
				for (int j = 0; j < this.m_PopParamList.Count; j++)
				{
					if (this.m_PopParamList[j].m_NamedType == namedType && this.m_PopParamList[j].m_Friendship == i)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					CharaUnlockWindow.PopParam item = new CharaUnlockWindow.PopParam(charaID, namedType, i);
					this.m_PopParamList.Add(item);
				}
			}
			this.AddUnlockADV(charaID, beforeFriendship, afterFriendship);
		}

		// Token: 0x06002A80 RID: 10880 RVA: 0x000E0274 File Offset: 0x000DE674
		private List<int> GetLimitADV(eCharaNamedType named, int beforeFriendship, int afterFriendship)
		{
			List<int> list = new List<int>();
			ADVListDB_Param[] @params = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.m_Params;
			for (int i = 0; i < @params.Length; i++)
			{
				bool flag = false;
				for (int j = 0; j < @params[i].m_LimitNamedType.Length; j++)
				{
					if (@params[i].m_LimitNamedType[j] == (int)named && @params[i].m_LimitFriendShipLv > beforeFriendship && @params[i].m_LimitFriendShipLv <= afterFriendship)
					{
						flag = true;
					}
				}
				if (flag)
				{
					bool flag2 = true;
					for (int k = 0; k < @params[i].m_LimitNamedType.Length; k++)
					{
						if (@params[i].m_LimitNamedType[k] != -1)
						{
							UserNamedData userNamedData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData((eCharaNamedType)@params[i].m_LimitNamedType[k]);
							if (userNamedData == null || userNamedData.FriendShip < @params[i].m_LimitFriendShipLv)
							{
								flag2 = false;
							}
						}
					}
					if (flag2)
					{
						list.Add(@params[i].m_AdvID);
					}
				}
			}
			return list;
		}

		// Token: 0x06002A81 RID: 10881 RVA: 0x000E03B8 File Offset: 0x000DE7B8
		private void AddUnlockADV(int charaID, int beforeFriendship, int afterFriendship)
		{
			eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
			List<int> limitADV = this.GetLimitADV(namedType, beforeFriendship, afterFriendship);
			if (limitADV != null)
			{
				if (this.m_UnlockADVList == null)
				{
					this.m_UnlockADVList = new List<CharaUnlockWindow.UnlockADVParam>();
				}
				for (int i = 0; i < limitADV.Count; i++)
				{
					bool flag = false;
					for (int j = 0; j < this.m_UnlockADVList.Count; j++)
					{
						if (this.m_UnlockADVList[j].m_ADVID == limitADV[i])
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						this.m_UnlockADVList.Add(new CharaUnlockWindow.UnlockADVParam(charaID, limitADV[i]));
					}
				}
			}
		}

		// Token: 0x06002A82 RID: 10882 RVA: 0x000E0488 File Offset: 0x000DE888
		public void OpenStackPop()
		{
			this.m_IsADVPop = false;
			while (this.m_PopParamList != null && this.m_PopParamList.Count > 0)
			{
				int charaID = this.m_PopParamList[0].m_CharaID;
				eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
				NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(namedType);
				string nickName = param.m_NickName;
				List<int> limitADV = this.GetLimitADV(namedType, this.m_PopParamList[0].m_Friendship - 1, this.m_PopParamList[0].m_Friendship);
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < limitADV.Count; i++)
				{
					if (!ADVUtility.IsCrossScenario(limitADV[i]))
					{
						stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpADV, new object[]
						{
							SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(limitADV[i]).m_Title
						}));
						stringBuilder.Append("\n");
					}
				}
				for (int j = 0; j < limitADV.Count; j++)
				{
					if (ADVUtility.IsCrossScenario(limitADV[j]))
					{
						stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpADVCross, new object[]
						{
							SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(limitADV[j]).m_Title
						}));
						stringBuilder.Append("\n");
					}
				}
				for (int k = 0; k < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ProfileVoiceListDB.m_Params.Length; k++)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ProfileVoiceListDB.m_Params[k].m_Friendship == this.m_PopParamList[0].m_Friendship)
					{
						stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpVoice, new object[]
						{
							SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ProfileVoiceListDB.m_Params[k].m_Title
						}));
						stringBuilder.Append("\n");
					}
				}
				NamedFriendshipExpDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB.GetParam(this.m_PopParamList[0].m_Friendship - 1, param.m_FriendshipTableID);
				NamedFriendshipExpDB_Param param3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB.GetParam(this.m_PopParamList[0].m_Friendship, param.m_FriendshipTableID);
				if (param2.m_CorrectHp < param3.m_CorrectHp || param2.m_CorrectAtk < param3.m_CorrectMgc || param2.m_CorrectDef < param3.m_CorrectDef || param2.m_CorrectMDef < param3.m_CorrectMDef || param2.m_CorrectSpd < param3.m_CorrectSpd || param2.m_CorrectLuck < param3.m_CorrectLuck)
				{
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpParam, new object[]
					{
						nickName
					}));
					stringBuilder.Append("\n");
				}
				if (stringBuilder.Length > 0)
				{
					this.m_CharaIconCloner.GetInst<CharaIconWithFrame>().ApplyCharaID(charaID);
					this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpTitle);
					this.m_MainText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.CharaFriendshipPopUpText, new object[]
					{
						nickName,
						this.m_PopParamList[0].m_Friendship
					});
					this.m_MainText.gameObject.SetActive(true);
					this.m_DetailText.text = stringBuilder.ToString();
					this.m_UnlockButtonObj.SetActive(true);
					this.m_ADVButtonObj.SetActive(false);
					this.m_MainGroup.Open();
					return;
				}
				this.OnEnd();
			}
		}

		// Token: 0x06002A83 RID: 10883 RVA: 0x000E08D4 File Offset: 0x000DECD4
		public void OpenStackADVPop(Action OnADVConfirm)
		{
			this.m_IsADVPop = true;
			this.m_OnConfirmADV = OnADVConfirm;
			if (this.m_UnlockADVList == null || this.m_UnlockADVList.Count <= 0)
			{
				return;
			}
			this.m_CharaIconCloner.GetInst<CharaIconWithFrame>().ApplyCharaID(this.m_UnlockADVList[0].m_CharaID);
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ADVUnlockTitle);
			ADVListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(this.m_UnlockADVList[0].m_ADVID);
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < param.m_LimitNamedType.Length; i++)
			{
				if (param.m_LimitNamedType[i] != -1)
				{
					if (i != 0)
					{
						stringBuilder.Append("、");
					}
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam((eCharaNamedType)param.m_LimitNamedType[i]).m_NickName);
				}
			}
			this.m_MainText.gameObject.SetActive(false);
			this.m_DetailText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ADVUnlock, new object[]
			{
				stringBuilder.ToString(),
				param.m_Title
			});
			this.m_UnlockButtonObj.SetActive(false);
			this.m_ADVButtonObj.SetActive(true);
			this.m_MainGroup.Open();
		}

		// Token: 0x06002A84 RID: 10884 RVA: 0x000E0A50 File Offset: 0x000DEE50
		private void OnEnd()
		{
			if (!this.m_IsADVPop)
			{
				this.m_PopParamList.RemoveAt(0);
				this.OpenStackPop();
			}
			else if (this.m_TransitADV)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvID = this.m_UnlockADVList[0].m_ADVID;
				this.m_UnlockADVList.RemoveAt(0);
				this.m_OnConfirmADV.Call();
			}
			else
			{
				this.m_UnlockADVList.RemoveAt(0);
				this.OpenStackADVPop(this.m_OnConfirmADV);
			}
		}

		// Token: 0x06002A85 RID: 10885 RVA: 0x000E0ADE File Offset: 0x000DEEDE
		public bool IsOpen()
		{
			return this.m_MainGroup.IsOpen();
		}

		// Token: 0x06002A86 RID: 10886 RVA: 0x000E0AEB File Offset: 0x000DEEEB
		public void OnClickCloseCallBack()
		{
			this.m_TransitADV = false;
			this.m_MainGroup.Close();
		}

		// Token: 0x06002A87 RID: 10887 RVA: 0x000E0AFF File Offset: 0x000DEEFF
		public void OnClickTransitADVCallBack()
		{
			this.m_TransitADV = true;
			this.m_MainGroup.Close();
		}

		// Token: 0x040030DB RID: 12507
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040030DC RID: 12508
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x040030DD RID: 12509
		[SerializeField]
		private Text m_Title;

		// Token: 0x040030DE RID: 12510
		[SerializeField]
		private Text m_MainText;

		// Token: 0x040030DF RID: 12511
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x040030E0 RID: 12512
		[SerializeField]
		private GameObject m_UnlockButtonObj;

		// Token: 0x040030E1 RID: 12513
		[SerializeField]
		private GameObject m_ADVButtonObj;

		// Token: 0x040030E2 RID: 12514
		private List<CharaUnlockWindow.PopParam> m_PopParamList = new List<CharaUnlockWindow.PopParam>();

		// Token: 0x040030E3 RID: 12515
		private List<CharaUnlockWindow.UnlockADVParam> m_UnlockADVList = new List<CharaUnlockWindow.UnlockADVParam>();

		// Token: 0x040030E4 RID: 12516
		private Action m_OnConfirmADV;

		// Token: 0x040030E5 RID: 12517
		private bool m_IsADVPop;

		// Token: 0x040030E6 RID: 12518
		private bool m_TransitADV;

		// Token: 0x02000804 RID: 2052
		private class PopParam
		{
			// Token: 0x06002A88 RID: 10888 RVA: 0x000E0B13 File Offset: 0x000DEF13
			public PopParam(int charaID, eCharaNamedType named, int friendship)
			{
				this.m_CharaID = charaID;
				this.m_NamedType = named;
				this.m_Friendship = friendship;
			}

			// Token: 0x040030E7 RID: 12519
			public int m_CharaID;

			// Token: 0x040030E8 RID: 12520
			public eCharaNamedType m_NamedType;

			// Token: 0x040030E9 RID: 12521
			public int m_Friendship;
		}

		// Token: 0x02000805 RID: 2053
		private class UnlockADVParam
		{
			// Token: 0x06002A89 RID: 10889 RVA: 0x000E0B30 File Offset: 0x000DEF30
			public UnlockADVParam(int charaID, int advID)
			{
				this.m_CharaID = charaID;
				this.m_ADVID = advID;
			}

			// Token: 0x040030EA RID: 12522
			public int m_CharaID;

			// Token: 0x040030EB RID: 12523
			public int m_ADVID;
		}
	}
}
