﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008A6 RID: 2214
	public class SkillDetail : MonoBehaviour
	{
		// Token: 0x06002DE6 RID: 11750 RVA: 0x000F1870 File Offset: 0x000EFC70
		public void Apply(SkillDetail.eSkillCategory category, int skillID, int skillLv = 0)
		{
			if (skillID != -1)
			{
				SkillListDB_Param param;
				switch (category)
				{
				case SkillDetail.eSkillCategory.PL:
					param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(skillID);
					break;
				case SkillDetail.eSkillCategory.EN:
					param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_EN.GetParam(skillID);
					break;
				case SkillDetail.eSkillCategory.MST:
					param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
					break;
				case SkillDetail.eSkillCategory.WPN:
					param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_WPN.GetParam(skillID);
					break;
				default:
					return;
				}
				this.m_SkillName.text = param.m_SkillName;
				this.m_SkillDetail.text = param.m_SkillDetail;
				if (this.m_SkillLevel != null)
				{
					if (skillLv > 0)
					{
						this.m_SkillLevel.enabled = true;
						this.m_SkillLevel.text = "Lv." + skillLv.ToString(",3");
					}
					else
					{
						this.m_SkillLevel.enabled = false;
					}
				}
			}
			else
			{
				if (this.m_SkillIcon != null)
				{
					this.m_SkillIcon.gameObject.SetActive(false);
				}
				this.m_SkillName.text = "スキルなし";
				this.m_SkillDetail.text = string.Empty;
				this.m_SkillLevel.text = string.Empty;
			}
		}

		// Token: 0x040034F2 RID: 13554
		[SerializeField]
		private SkillIcon m_SkillIcon;

		// Token: 0x040034F3 RID: 13555
		[SerializeField]
		private Text m_SkillName;

		// Token: 0x040034F4 RID: 13556
		[SerializeField]
		private Text m_SkillDetail;

		// Token: 0x040034F5 RID: 13557
		[SerializeField]
		private Text m_SkillLevel;

		// Token: 0x020008A7 RID: 2215
		public enum eSkillCategory
		{
			// Token: 0x040034F7 RID: 13559
			PL,
			// Token: 0x040034F8 RID: 13560
			EN,
			// Token: 0x040034F9 RID: 13561
			MST,
			// Token: 0x040034FA RID: 13562
			WPN
		}
	}
}
