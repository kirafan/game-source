﻿using System;
using UnityEngine;

namespace Star.UI.Signup
{
	// Token: 0x02000AC3 RID: 2755
	public class SignupUI : MenuUIBase
	{
		// Token: 0x140000EC RID: 236
		// (add) Token: 0x06003989 RID: 14729 RVA: 0x00125280 File Offset: 0x00123680
		// (remove) Token: 0x0600398A RID: 14730 RVA: 0x001252B8 File Offset: 0x001236B8
		public event Action OnClickDecide;

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x0600398B RID: 14731 RVA: 0x001252EE File Offset: 0x001236EE
		// (set) Token: 0x0600398C RID: 14732 RVA: 0x001252FB File Offset: 0x001236FB
		public string UserName
		{
			get
			{
				return this.m_TextInput.GetText();
			}
			set
			{
				this.m_TextInput.SetText(value);
			}
		}

		// Token: 0x0600398D RID: 14733 RVA: 0x00125309 File Offset: 0x00123709
		private void Update()
		{
			this.UpdateStep();
		}

		// Token: 0x0600398E RID: 14734 RVA: 0x00125314 File Offset: 0x00123714
		private void UpdateStep()
		{
			switch (this.m_Step)
			{
			case SignupUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(SignupUI.eStep.Idle);
				}
				break;
			}
			case SignupUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(SignupUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600398F RID: 14735 RVA: 0x00125410 File Offset: 0x00123810
		private void ChangeStep(SignupUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case SignupUI.eStep.Hide:
				this.SetEnableInput(false);
				break;
			case SignupUI.eStep.In:
				if (this.m_TextInput.GetText().Length == 0)
				{
					this.m_DecideButton.Interactable = false;
				}
				else
				{
					this.m_DecideButton.Interactable = true;
				}
				this.SetEnableInput(false);
				break;
			case SignupUI.eStep.Out:
				this.SetEnableInput(false);
				break;
			case SignupUI.eStep.Idle:
				this.SetEnableInput(true);
				this.m_MainGroup.SetEnableInput(true);
				break;
			}
		}

		// Token: 0x06003990 RID: 14736 RVA: 0x001254B4 File Offset: 0x001238B4
		public void Setup()
		{
		}

		// Token: 0x06003991 RID: 14737 RVA: 0x001254B8 File Offset: 0x001238B8
		public override void PlayIn()
		{
			this.ChangeStep(SignupUI.eStep.In);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003992 RID: 14738 RVA: 0x00125504 File Offset: 0x00123904
		public override void PlayOut()
		{
			this.ChangeStep(SignupUI.eStep.Out);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003993 RID: 14739 RVA: 0x00125550 File Offset: 0x00123950
		public override bool IsMenuEnd()
		{
			return this.m_Step == SignupUI.eStep.End;
		}

		// Token: 0x06003994 RID: 14740 RVA: 0x0012555B File Offset: 0x0012395B
		public void OnClickDecideButtonCallBack()
		{
			this.OnClickDecide.Call();
		}

		// Token: 0x040040BC RID: 16572
		private SignupUI.eStep m_Step;

		// Token: 0x040040BD RID: 16573
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040040BE RID: 16574
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040040BF RID: 16575
		[SerializeField]
		private TextInput m_TextInput;

		// Token: 0x040040C0 RID: 16576
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x02000AC4 RID: 2756
		private enum eStep
		{
			// Token: 0x040040C3 RID: 16579
			Hide,
			// Token: 0x040040C4 RID: 16580
			In,
			// Token: 0x040040C5 RID: 16581
			Out,
			// Token: 0x040040C6 RID: 16582
			Idle,
			// Token: 0x040040C7 RID: 16583
			End,
			// Token: 0x040040C8 RID: 16584
			Num
		}
	}
}
