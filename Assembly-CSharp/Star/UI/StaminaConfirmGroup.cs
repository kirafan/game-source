﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000AC7 RID: 2759
	public class StaminaConfirmGroup : UIGroup
	{
		// Token: 0x140000ED RID: 237
		// (add) Token: 0x060039A8 RID: 14760 RVA: 0x00125E8C File Offset: 0x0012428C
		// (remove) Token: 0x060039A9 RID: 14761 RVA: 0x00125EC4 File Offset: 0x001242C4
		public event Action<int, int> OnConfirm;

		// Token: 0x060039AA RID: 14762 RVA: 0x00125EFC File Offset: 0x001242FC
		public void Apply(int itemID, int useNum)
		{
			this.m_ItemID = itemID;
			this.m_UseNum = useNum;
			long num;
			if (itemID == -1)
			{
				num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax();
				this.m_DescriptText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StaminaShopConfirm, new object[]
				{
					SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem),
					useNum,
					100,
					num
				});
			}
			else
			{
				ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID);
				num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax() * (long)param.m_TypeArgs[0] / 100L;
				this.m_DescriptText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StaminaShopConfirm, new object[]
				{
					param.m_Name,
					useNum,
					param.m_TypeArgs[0],
					num
				});
			}
			long value = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValue();
			long valueMax = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValueMax();
			this.m_BeforeText.text = UIUtility.StaminaValueToString(value, true) + "/" + UIUtility.StaminaValueToString(valueMax, false);
			this.m_AfterText.text = UIUtility.StaminaValueToString(value + num, true) + "/" + UIUtility.StaminaValueToString(valueMax, false);
		}

		// Token: 0x060039AB RID: 14763 RVA: 0x001260A2 File Offset: 0x001244A2
		public void OnClickYesButtonCallBack()
		{
			this.OnConfirm.Call(this.m_ItemID, this.m_UseNum);
		}

		// Token: 0x060039AC RID: 14764 RVA: 0x001260BB File Offset: 0x001244BB
		public void OnClickNoButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x040040E0 RID: 16608
		private int m_ItemID = -1;

		// Token: 0x040040E1 RID: 16609
		private int m_UseNum;

		// Token: 0x040040E2 RID: 16610
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x040040E3 RID: 16611
		[SerializeField]
		private Text m_BeforeText;

		// Token: 0x040040E4 RID: 16612
		[SerializeField]
		private Text m_AfterText;
	}
}
