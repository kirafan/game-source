﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000884 RID: 2180
	public class InfiniteScrollItem : MonoBehaviour
	{
		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06002D3F RID: 11583 RVA: 0x000EF650 File Offset: 0x000EDA50
		// (set) Token: 0x06002D40 RID: 11584 RVA: 0x000EF658 File Offset: 0x000EDA58
		public int PosIdx
		{
			get
			{
				return this.m_PosIdx;
			}
			set
			{
				this.m_PosIdx = value;
			}
		}

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06002D41 RID: 11585 RVA: 0x000EF661 File Offset: 0x000EDA61
		// (set) Token: 0x06002D42 RID: 11586 RVA: 0x000EF669 File Offset: 0x000EDA69
		public int DataIdx
		{
			get
			{
				return this.m_DataIdx;
			}
			set
			{
				this.m_DataIdx = value;
			}
		}

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06002D43 RID: 11587 RVA: 0x000EF672 File Offset: 0x000EDA72
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06002D44 RID: 11588 RVA: 0x000EF697 File Offset: 0x000EDA97
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06002D45 RID: 11589 RVA: 0x000EF6BC File Offset: 0x000EDABC
		// (set) Token: 0x06002D46 RID: 11590 RVA: 0x000EF6C4 File Offset: 0x000EDAC4
		public InfiniteScroll Scroll
		{
			get
			{
				return this.m_Scroll;
			}
			set
			{
				this.m_Scroll = value;
			}
		}

		// Token: 0x06002D47 RID: 11591 RVA: 0x000EF6CD File Offset: 0x000EDACD
		public virtual void Setup()
		{
		}

		// Token: 0x06002D48 RID: 11592 RVA: 0x000EF6CF File Offset: 0x000EDACF
		public virtual void Apply(InfiniteScrollItemData data)
		{
			this.m_Data = data;
		}

		// Token: 0x06002D49 RID: 11593 RVA: 0x000EF6D8 File Offset: 0x000EDAD8
		public virtual void Destroy()
		{
		}

		// Token: 0x04003457 RID: 13399
		protected InfiniteScroll m_Scroll;

		// Token: 0x04003458 RID: 13400
		protected InfiniteScrollItemData m_Data;

		// Token: 0x04003459 RID: 13401
		protected int m_PosIdx;

		// Token: 0x0400345A RID: 13402
		protected int m_DataIdx;

		// Token: 0x0400345B RID: 13403
		private RectTransform m_RectTransform;

		// Token: 0x0400345C RID: 13404
		private GameObject m_GameObject;
	}
}
