﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200081C RID: 2076
	public class MixPopUp : MonoBehaviour
	{
		// Token: 0x06002B25 RID: 11045 RVA: 0x000E40F0 File Offset: 0x000E24F0
		public void PlayIn(int successLv = -1)
		{
			if (successLv >= 0)
			{
				if (this.m_Great != null)
				{
					if (successLv < 2)
					{
						this.m_Great.SetActive(false);
					}
					else
					{
						this.m_Great.SetActive(true);
					}
				}
				if (this.m_Big != null)
				{
					if (successLv < 1)
					{
						this.m_Big.SetActive(false);
					}
					else
					{
						this.m_Big.SetActive(true);
					}
				}
			}
			this.m_Anim.PlayIn();
		}

		// Token: 0x06002B26 RID: 11046 RVA: 0x000E4179 File Offset: 0x000E2579
		public void PlayOut()
		{
			this.m_Anim.PlayOut();
		}

		// Token: 0x06002B27 RID: 11047 RVA: 0x000E4186 File Offset: 0x000E2586
		public void PlayInOut(int successLv)
		{
			this.PlayIn(successLv);
			this.m_AutoOut = true;
			this.m_Wait = 1f;
		}

		// Token: 0x06002B28 RID: 11048 RVA: 0x000E41A4 File Offset: 0x000E25A4
		private void Update()
		{
			if (this.m_AutoOut && this.m_Anim.IsEnd)
			{
				this.m_Wait = 1f;
				this.m_AutoOut = false;
			}
			if (this.m_Wait > 0f)
			{
				this.m_Wait -= Time.deltaTime;
				if (this.m_Wait <= 0f)
				{
					this.PlayOut();
				}
			}
		}

		// Token: 0x04003189 RID: 12681
		private const float INTERVAL_INOUT = 1f;

		// Token: 0x0400318A RID: 12682
		[SerializeField]
		private GameObject m_Great;

		// Token: 0x0400318B RID: 12683
		[SerializeField]
		private GameObject m_Big;

		// Token: 0x0400318C RID: 12684
		[SerializeField]
		private AnimUIPlayer m_Anim;

		// Token: 0x0400318D RID: 12685
		private bool m_AutoOut;

		// Token: 0x0400318E RID: 12686
		private float m_Wait;
	}
}
