﻿using System;
using Star.UI.Filter;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200097B RID: 2427
	public class SortUI : MenuUIBase
	{
		// Token: 0x06003223 RID: 12835 RVA: 0x000FFFBD File Offset: 0x000FE3BD
		public void Setup(UISettings.eUsingType_Sort usingType, Action OnExecute)
		{
			this.m_SortWindow.Setup(usingType);
			this.m_SortWindow.OnExecute += OnExecute;
		}

		// Token: 0x06003224 RID: 12836 RVA: 0x000FFFD8 File Offset: 0x000FE3D8
		private void Update()
		{
			switch (this.m_Step)
			{
			case SortUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (this.m_AnimList[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(SortUI.eStep.Idle);
				}
				break;
			}
			case SortUI.eStep.Idle:
				if (!this.m_SortWindow.IsOpen())
				{
					this.OnStartOut.Call();
				}
				break;
			case SortUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (this.m_AnimList[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(SortUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003225 RID: 12837 RVA: 0x001000C8 File Offset: 0x000FE4C8
		private void ChangeStep(SortUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case SortUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case SortUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case SortUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case SortUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case SortUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003226 RID: 12838 RVA: 0x00100170 File Offset: 0x000FE570
		public override void PlayIn()
		{
			this.ChangeStep(SortUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_SortWindow.Open();
		}

		// Token: 0x06003227 RID: 12839 RVA: 0x001001B8 File Offset: 0x000FE5B8
		public override void PlayOut()
		{
			this.ChangeStep(SortUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
		}

		// Token: 0x06003228 RID: 12840 RVA: 0x001001F2 File Offset: 0x000FE5F2
		public override bool IsMenuEnd()
		{
			return this.m_Step == SortUI.eStep.End;
		}

		// Token: 0x04003848 RID: 14408
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003849 RID: 14409
		[SerializeField]
		private SortWindow m_SortWindow;

		// Token: 0x0400384A RID: 14410
		private SortUI.eStep m_Step;

		// Token: 0x0400384B RID: 14411
		public Action OnStartOut;

		// Token: 0x0200097C RID: 2428
		private enum eStep
		{
			// Token: 0x0400384D RID: 14413
			Hide,
			// Token: 0x0400384E RID: 14414
			In,
			// Token: 0x0400384F RID: 14415
			Idle,
			// Token: 0x04003850 RID: 14416
			Out,
			// Token: 0x04003851 RID: 14417
			End
		}
	}
}
