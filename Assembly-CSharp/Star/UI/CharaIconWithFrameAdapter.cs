﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B9 RID: 1977
	public class CharaIconWithFrameAdapter : ASyncImageAdapter
	{
		// Token: 0x060028DB RID: 10459 RVA: 0x000D9BE7 File Offset: 0x000D7FE7
		public CharaIconWithFrameAdapter(CharaIconWithFrame characterIcon)
		{
			this.m_CharacterIcon = characterIcon;
		}

		// Token: 0x060028DC RID: 10460 RVA: 0x000D9BF6 File Offset: 0x000D7FF6
		public override void Destroy()
		{
			if (this.m_CharacterIcon == null)
			{
				return;
			}
			this.m_CharacterIcon.Destroy();
		}

		// Token: 0x060028DD RID: 10461 RVA: 0x000D9C15 File Offset: 0x000D8015
		public override void Apply(int charaID, int weaponID)
		{
			if (this.m_CharacterIcon == null)
			{
				return;
			}
			this.m_CharacterIcon.ApplyCharaID(charaID);
		}

		// Token: 0x060028DE RID: 10462 RVA: 0x000D9C35 File Offset: 0x000D8035
		public override bool IsDoneLoad()
		{
			return false;
		}

		// Token: 0x060028DF RID: 10463 RVA: 0x000D9C38 File Offset: 0x000D8038
		protected override MonoBehaviour GetMonoBehaviour()
		{
			return this.m_CharacterIcon;
		}

		// Token: 0x04002FAA RID: 12202
		private CharaIconWithFrame m_CharacterIcon;
	}
}
