﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009A5 RID: 2469
	public class GemShopPlayer : SingletonMonoBehaviour<GemShopPlayer>
	{
		// Token: 0x0600331A RID: 13082 RVA: 0x00104015 File Offset: 0x00102415
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x0600331B RID: 13083 RVA: 0x0010401D File Offset: 0x0010241D
		public bool IsOpen()
		{
			return this.m_Step != GemShopPlayer.eStep.Hide;
		}

		// Token: 0x0600331C RID: 13084 RVA: 0x0010402C File Offset: 0x0010242C
		public void Open(int type, Action OnEnd)
		{
			if (this.m_UI != null && this.m_Step != GemShopPlayer.eStep.Hide)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.GemShopUI, true);
			this.m_OnEndCallBack = OnEnd;
			this.m_Step = GemShopPlayer.eStep.LoadWait;
		}

		// Token: 0x0600331D RID: 13085 RVA: 0x00104087 File Offset: 0x00102487
		public void Close()
		{
			if (this.m_Step != GemShopPlayer.eStep.Main)
			{
				return;
			}
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
			}
			this.m_OnEndCallBack.Call();
			this.m_Step = GemShopPlayer.eStep.Hide;
		}

		// Token: 0x0600331E RID: 13086 RVA: 0x001040C4 File Offset: 0x001024C4
		public void ForceHide()
		{
			this.m_Step = GemShopPlayer.eStep.Hide;
		}

		// Token: 0x0600331F RID: 13087 RVA: 0x001040D0 File Offset: 0x001024D0
		private void Update()
		{
			switch (this.m_Step)
			{
			case GemShopPlayer.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.GemShopUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.GemShopUI);
					this.m_Step = GemShopPlayer.eStep.SetupAndPlayIn;
				}
				break;
			case GemShopPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<GemShopUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.GemShopUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					this.m_UI.GetComponent<Canvas>().planeDistance = 95f;
					this.m_UI.Setup();
					this.m_UI.OnConfirmAge += this.OnRequestSetAgeCallBack;
					this.m_UI.OnRequestPurchase += this.OnRequestPurchaseCallBack;
					if (SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.IsNeedCheckAge())
					{
						this.m_Step = GemShopPlayer.eStep.SetAge;
					}
					else
					{
						this.m_Step = GemShopPlayer.eStep.InitProducts;
					}
				}
				break;
			case GemShopPlayer.eStep.SetAge:
				this.m_Step = GemShopPlayer.eStep.SetAge_Wait;
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				this.m_UI.PlayInAgeConfirm();
				break;
			case GemShopPlayer.eStep.SetAge_Wait:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.GemShopUI);
					this.m_Step = GemShopPlayer.eStep.UnloadWait;
				}
				break;
			case GemShopPlayer.eStep.InitProducts:
				this.m_Step = GemShopPlayer.eStep.InitProducts_Wait;
				SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.Request_InitializeProducts(new Action(this.OnCompleteInitProducts));
				break;
			case GemShopPlayer.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.GemShopUI);
					this.m_Step = GemShopPlayer.eStep.UnloadWait;
				}
				break;
			case GemShopPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.GemShopUI))
				{
					this.m_OnEndCallBack.Call();
					this.m_Step = GemShopPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x06003320 RID: 13088 RVA: 0x001042CF File Offset: 0x001026CF
		private void OnRequestSetAgeCallBack(eAgeType ageType)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.Request_SetAge(ageType, new Action(this.OnCompleteSetAge));
		}

		// Token: 0x06003321 RID: 13089 RVA: 0x001042ED File Offset: 0x001026ED
		private void OnCompleteSetAge()
		{
			this.m_UI.CloseAgeConfirm(new Action(this.OnCloseAgeConfirm));
		}

		// Token: 0x06003322 RID: 13090 RVA: 0x00104306 File Offset: 0x00102706
		private void OnCloseAgeConfirm()
		{
			this.m_Step = GemShopPlayer.eStep.InitProducts;
		}

		// Token: 0x06003323 RID: 13091 RVA: 0x0010430F File Offset: 0x0010270F
		private void OnCompleteInitProducts()
		{
			this.m_UI.Refresh();
			this.m_UI.PlayIn();
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
			this.m_Step = GemShopPlayer.eStep.Main;
		}

		// Token: 0x06003324 RID: 13092 RVA: 0x00104344 File Offset: 0x00102744
		private void OnRequestPurchaseCallBack(int id)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.Purchase(id, new Action<StoreManager.Product>(this.OnCompletePurchase));
		}

		// Token: 0x06003325 RID: 13093 RVA: 0x00104364 File Offset: 0x00102764
		private void OnCompletePurchase(StoreManager.Product product)
		{
			Debug.Log("GemShopPlayer.OnCompletePurchase()");
			this.m_UI.Refresh();
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			string textMessage = inst.DbMng.GetTextMessage(eText_MessageDB.IAPCompleteTitle);
			string textMessage2 = inst.DbMng.GetTextMessage(eText_MessageDB.IAPComplete, new object[]
			{
				product.m_Name
			});
			inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, textMessage, textMessage2, null, null);
		}

		// Token: 0x0400394C RID: 14668
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.GemShopUI;

		// Token: 0x0400394D RID: 14669
		private GemShopPlayer.eStep m_Step;

		// Token: 0x0400394E RID: 14670
		private GemShopUI m_UI;

		// Token: 0x0400394F RID: 14671
		private Action m_OnEndCallBack;

		// Token: 0x020009A6 RID: 2470
		private enum eStep
		{
			// Token: 0x04003951 RID: 14673
			Hide,
			// Token: 0x04003952 RID: 14674
			LoadWait,
			// Token: 0x04003953 RID: 14675
			SetupAndPlayIn,
			// Token: 0x04003954 RID: 14676
			SetAge,
			// Token: 0x04003955 RID: 14677
			SetAge_Wait,
			// Token: 0x04003956 RID: 14678
			InitProducts,
			// Token: 0x04003957 RID: 14679
			InitProducts_Wait,
			// Token: 0x04003958 RID: 14680
			Main,
			// Token: 0x04003959 RID: 14681
			UnloadWait
		}
	}
}
