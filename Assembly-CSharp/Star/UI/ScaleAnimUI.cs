﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F4 RID: 2036
	public class ScaleAnimUI : AnimUIBase
	{
		// Token: 0x06002A1E RID: 10782 RVA: 0x000DE33C File Offset: 0x000DC73C
		protected override void Prepare()
		{
			base.Prepare();
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_RectTransform.localScale = this.m_HideScale;
			if (this.m_ReturnMode)
			{
				this.m_EndScale = this.m_HideScale;
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x06002A1F RID: 10783 RVA: 0x000DE398 File Offset: 0x000DC798
		protected override void ExecutePlayIn()
		{
			if (this.m_Restart)
			{
				this.m_RectTransform.localScale = this.m_HideScale;
			}
			iTween.Stop(this.m_GameObject, "scale");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", this.m_ShowScale.x);
			hashtable.Add("y", this.m_ShowScale.y);
			hashtable.Add("time", num);
			hashtable.Add("delay", 0.001f + this.m_Delay);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("oncomplete", "OnCompleteIn");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ScaleTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002A20 RID: 10784 RVA: 0x000DE49C File Offset: 0x000DC89C
		protected override void ExecutePlayOut()
		{
			if (this.m_Restart)
			{
				this.m_RectTransform.localScale = this.m_ShowScale;
			}
			iTween.Stop(this.m_GameObject, "scale");
			float num = this.m_AnimDuration;
			if (this.m_AnimDuration < 0f)
			{
				num = 0.1f;
			}
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", this.m_EndScale.x);
			hashtable.Add("y", this.m_EndScale.y);
			hashtable.Add("time", num);
			hashtable.Add("delay", 0.001f + this.m_Delay);
			hashtable.Add("easeType", this.m_EaseType);
			hashtable.Add("oncomplete", "OnCompleteOut");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ScaleTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002A21 RID: 10785 RVA: 0x000DE59E File Offset: 0x000DC99E
		public override void Hide()
		{
			base.Hide();
			iTween.Stop(this.m_GameObject, "scale");
			this.m_RectTransform.localScale = this.m_HideScale;
		}

		// Token: 0x06002A22 RID: 10786 RVA: 0x000DE5C7 File Offset: 0x000DC9C7
		public override void Show()
		{
			base.Show();
			base.gameObject.SetActive(true);
			iTween.Stop(this.m_GameObject, "scale");
			this.m_RectTransform.localScale = this.m_ShowScale;
		}

		// Token: 0x06002A23 RID: 10787 RVA: 0x000DE5FC File Offset: 0x000DC9FC
		public void OnCompleteIn()
		{
			this.m_RectTransform.localScale = this.m_ShowScale;
			this.m_IsPlaying = false;
			this.OnAnimCompleteIn.Call();
		}

		// Token: 0x06002A24 RID: 10788 RVA: 0x000DE621 File Offset: 0x000DCA21
		public void OnCompleteOut()
		{
			this.m_RectTransform.localScale = this.m_EndScale;
			this.m_IsPlaying = false;
			this.OnAnimCompleteOut.Call();
		}

		// Token: 0x04003078 RID: 12408
		private const float DEFAULT_DURATION = 0.1f;

		// Token: 0x04003079 RID: 12409
		[SerializeField]
		private bool m_ReturnMode = true;

		// Token: 0x0400307A RID: 12410
		[SerializeField]
		private bool m_Restart;

		// Token: 0x0400307B RID: 12411
		[SerializeField]
		private Vector3 m_HideScale = Vector3.zero;

		// Token: 0x0400307C RID: 12412
		[SerializeField]
		private Vector3 m_ShowScale = Vector3.one;

		// Token: 0x0400307D RID: 12413
		[SerializeField]
		private Vector3 m_EndScale = Vector3.zero;

		// Token: 0x0400307E RID: 12414
		[SerializeField]
		private float m_AnimDuration = -1f;

		// Token: 0x0400307F RID: 12415
		[SerializeField]
		private float m_Delay;

		// Token: 0x04003080 RID: 12416
		[SerializeField]
		private iTween.EaseType m_EaseType = iTween.EaseType.easeOutQuad;

		// Token: 0x04003081 RID: 12417
		private GameObject m_GameObject;

		// Token: 0x04003082 RID: 12418
		private RectTransform m_RectTransform;
	}
}
