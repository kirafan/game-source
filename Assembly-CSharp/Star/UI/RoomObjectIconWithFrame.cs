﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000871 RID: 2161
	public class RoomObjectIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002CC5 RID: 11461 RVA: 0x000EC1E0 File Offset: 0x000EA5E0
		public void Apply(eRoomObjectCategory category, int roomObjectID)
		{
			this.m_Icon.Apply(category, roomObjectID);
		}

		// Token: 0x06002CC6 RID: 11462 RVA: 0x000EC1EF File Offset: 0x000EA5EF
		public void Destroy()
		{
			if (this.m_Icon != null)
			{
				this.m_Icon.Destroy();
			}
		}

		// Token: 0x040033B9 RID: 13241
		[SerializeField]
		private RoomObjectIcon m_Icon;
	}
}
