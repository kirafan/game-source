﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000825 RID: 2085
	public class TextField : MonoBehaviour
	{
		// Token: 0x06002B75 RID: 11125 RVA: 0x000E48F5 File Offset: 0x000E2CF5
		public void SetTitle(string text)
		{
			this.m_Title.text = text;
		}

		// Token: 0x06002B76 RID: 11126 RVA: 0x000E4903 File Offset: 0x000E2D03
		public void SetValue(string text)
		{
			this.m_Value.text = text;
		}

		// Token: 0x06002B77 RID: 11127 RVA: 0x000E4914 File Offset: 0x000E2D14
		public void SetTitleBGWidth(float width)
		{
			this.m_TitleBG.sizeDelta = new Vector2(width, this.m_TitleBG.sizeDelta.y);
		}

		// Token: 0x040031DD RID: 12765
		private const float TITLEBG_MARGIN = 24f;

		// Token: 0x040031DE RID: 12766
		[SerializeField]
		private RectTransform m_TitleBG;

		// Token: 0x040031DF RID: 12767
		[SerializeField]
		[Tooltip("左側")]
		private Text m_Title;

		// Token: 0x040031E0 RID: 12768
		[SerializeField]
		[Tooltip("右側")]
		private Text m_Value;
	}
}
