﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000827 RID: 2087
	public class TextInput : MonoBehaviour
	{
		// Token: 0x06002B7F RID: 11135 RVA: 0x000E4ACC File Offset: 0x000E2ECC
		private void Start()
		{
			if (this.m_RowLimit > 0)
			{
				this.m_InputLimit++;
			}
			this.m_InputField.onValueChanged.AddListener(new UnityAction<string>(this.OnValueChanged));
			InputField inputField = this.m_InputField;
			inputField.onValidateInput = (InputField.OnValidateInput)Delegate.Combine(inputField.onValidateInput, new InputField.OnValidateInput(this.OnValidateInput));
			this.m_InputField.onEndEdit.AddListener(new UnityAction<string>(this.OnEndEdit));
			if (this.m_TmpLimit <= 0)
			{
				this.m_TmpLimit = this.m_InputLimit;
			}
			this.m_InputField.characterLimit = this.m_TmpLimit;
			this.m_InputField.lineType = InputField.LineType.SingleLine;
			this.m_DisplayFixedText.supportRichText = false;
			this.m_InputField.textComponent.supportRichText = false;
		}

		// Token: 0x06002B80 RID: 11136 RVA: 0x000E4BA4 File Offset: 0x000E2FA4
		private void LateUpdate()
		{
			if (this.m_ChangedText != null)
			{
				this.FixText(this.m_ChangedText);
				this.m_ChangedText = null;
			}
			this.m_BeforeCaret = this.m_InputField.caretPosition;
		}

		// Token: 0x06002B81 RID: 11137 RVA: 0x000E4BD5 File Offset: 0x000E2FD5
		public void SetDecideInteractable(bool flg)
		{
			this.m_DecideButton.Interactable = flg;
		}

		// Token: 0x06002B82 RID: 11138 RVA: 0x000E4BE4 File Offset: 0x000E2FE4
		public void SetText(string text)
		{
			this.m_DisplayFixedText.text = text;
			text = text.Replace("\n", string.Empty);
			this.m_InputField.text = text;
			this.m_DisplayFixedText.enabled = true;
			this.m_InputField.textComponent.enabled = false;
		}

		// Token: 0x06002B83 RID: 11139 RVA: 0x000E4C38 File Offset: 0x000E3038
		public string GetText()
		{
			return this.m_DisplayFixedText.text;
		}

		// Token: 0x06002B84 RID: 11140 RVA: 0x000E4C45 File Offset: 0x000E3045
		private char OnValidateInput(string text, int charIndex, char addedChar)
		{
			if (char.IsSurrogate(addedChar))
			{
				Debug.Log("IsSurrogate:" + addedChar);
				addedChar = '\0';
			}
			return addedChar;
		}

		// Token: 0x06002B85 RID: 11141 RVA: 0x000E4C6B File Offset: 0x000E306B
		private void OnValueChanged(string text)
		{
			this.m_DisplayFixedText.enabled = false;
			this.m_InputField.textComponent.enabled = true;
			this.m_ChangedText = text;
		}

		// Token: 0x06002B86 RID: 11142 RVA: 0x000E4C91 File Offset: 0x000E3091
		private void OnEndEdit(string text)
		{
			this.m_ChangedText = null;
			this.FixText(text);
		}

		// Token: 0x06002B87 RID: 11143 RVA: 0x000E4CA4 File Offset: 0x000E30A4
		private void FixText(string text)
		{
			int num = 0;
			if (this.m_RowLimit > 0 && text.Length >= this.m_RowLimit && text.IndexOf('\n') != -1)
			{
				text = text.Replace("\n", string.Empty);
			}
			if (this.m_RowLimit > 0 && text.Length > this.m_RowLimit && text.IndexOf('\n') == -1)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(text.Substring(0, this.m_RowLimit));
				stringBuilder.Append('\n');
				stringBuilder.Append(text.Substring(this.m_RowLimit, text.Length - this.m_RowLimit));
				text = stringBuilder.ToString();
				if (this.m_BeforeCaret < this.m_RowLimit && this.m_InputField.caretPosition > this.m_RowLimit)
				{
					num++;
				}
			}
			if (text.Length > this.m_InputLimit)
			{
				text = text.Substring(0, this.m_InputLimit);
				this.m_InputField.text = text;
			}
			if (!this.m_IsEnableEmpty)
			{
				if (this.CheckEmptyString(text))
				{
					this.m_DecideButton.Interactable = false;
				}
				else
				{
					this.m_DecideButton.Interactable = true;
				}
			}
			else
			{
				this.m_DecideButton.Interactable = true;
			}
			this.m_DisplayFixedText.text = text;
			this.m_DisplayFixedText.enabled = true;
			this.m_InputField.textComponent.enabled = false;
		}

		// Token: 0x06002B88 RID: 11144 RVA: 0x000E4E2C File Offset: 0x000E322C
		private bool CheckEmptyString(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return true;
			}
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] != ' ' && text[i] != '\u3000' && text[i] != '\n')
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x040031E3 RID: 12771
		[SerializeField]
		[Tooltip("入力可能な文字数 0で無制限")]
		private int m_InputLimit;

		// Token: 0x040031E4 RID: 12772
		[SerializeField]
		private int m_RowLimit;

		// Token: 0x040031E5 RID: 12773
		[SerializeField]
		[Tooltip("入力確定までに入力可能な文字数 0でInputLimitと同等")]
		private int m_TmpLimit;

		// Token: 0x040031E6 RID: 12774
		[SerializeField]
		[Tooltip("空、あるいはスペースのみを許可するか")]
		private bool m_IsEnableEmpty;

		// Token: 0x040031E7 RID: 12775
		[SerializeField]
		private InputField m_InputField;

		// Token: 0x040031E8 RID: 12776
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040031E9 RID: 12777
		[SerializeField]
		private Text m_DisplayFixedText;

		// Token: 0x040031EA RID: 12778
		private string m_ChangedText;

		// Token: 0x040031EB RID: 12779
		private int m_BeforeCaret;
	}
}
