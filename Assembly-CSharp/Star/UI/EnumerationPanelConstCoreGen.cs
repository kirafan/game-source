﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200096E RID: 2414
	public class EnumerationPanelConstCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterConstExGen<ParentType> where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x060031F4 RID: 12788 RVA: 0x000FEC34 File Offset: 0x000FD034
		protected override CharaPanelType InstantiateProcess()
		{
			if (this.m_SharedIntance.m_CharaPanelPrefab == null)
			{
				return (CharaPanelType)((object)null);
			}
			RectTransform charaPanelParent = base.GetCharaPanelParent();
			if (charaPanelParent == null && charaPanelParent == null)
			{
				this.m_SharedIntance.m_CharaPanelParent = (this.m_SharedIntance.parent.transform as RectTransform);
				charaPanelParent = base.GetCharaPanelParent();
			}
			return UnityEngine.Object.Instantiate<CharaPanelType>(this.m_SharedIntance.m_CharaPanelPrefab, base.GetCharaPanelParent(), false);
		}

		// Token: 0x060031F5 RID: 12789 RVA: 0x000FECDC File Offset: 0x000FD0DC
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
			if (charaPanel == null)
			{
				return;
			}
			charaPanel.Setup(this.m_SharedIntance.parent, partyMemberController);
			charaPanel.SetMode(EnumerationPanelCoreBase.ConvertEnumerationPanelCoreBaseeModeToEnumerationCharaPanelBaseeMode(this.m_Mode));
			charaPanel.Apply();
		}

		// Token: 0x060031F6 RID: 12790 RVA: 0x000FED3E File Offset: 0x000FD13E
		public override SetupArgumentType GetSharedIntance()
		{
			return this.m_SharedIntance;
		}

		// Token: 0x060031F7 RID: 12791 RVA: 0x000FED46 File Offset: 0x000FD146
		public override void SetSharedIntance(SetupArgumentType arg)
		{
			this.m_SharedIntance = arg;
		}

		// Token: 0x04003826 RID: 14374
		protected SetupArgumentType m_SharedIntance;
	}
}
