﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200084D RID: 2125
	public class FriendshipGauge : MonoBehaviour
	{
		// Token: 0x06002C44 RID: 11332 RVA: 0x000E9ABF File Offset: 0x000E7EBF
		public bool IsPlaying()
		{
			return this.m_Step != FriendshipGauge.eStep.None;
		}

		// Token: 0x14000067 RID: 103
		// (add) Token: 0x06002C45 RID: 11333 RVA: 0x000E9AD0 File Offset: 0x000E7ED0
		// (remove) Token: 0x06002C46 RID: 11334 RVA: 0x000E9B08 File Offset: 0x000E7F08
		public event Action m_OnLevelUp;

		// Token: 0x14000068 RID: 104
		// (add) Token: 0x06002C47 RID: 11335 RVA: 0x000E9B40 File Offset: 0x000E7F40
		// (remove) Token: 0x06002C48 RID: 11336 RVA: 0x000E9B78 File Offset: 0x000E7F78
		public event Action m_OnFinishPlay;

		// Token: 0x06002C49 RID: 11337 RVA: 0x000E9BB0 File Offset: 0x000E7FB0
		public void Apply(int friendship, long exp, sbyte expTableID)
		{
			this.m_ImageNumber.SetValue(friendship);
			NamedFriendshipExpDB namedFriendshipExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB;
			long currentExp = namedFriendshipExpDB.GetCurrentExp(friendship, exp, expTableID);
			int nextExp = namedFriendshipExpDB.GetNextExp(friendship, expTableID);
			if (nextExp <= 0)
			{
				this.m_TextValue.text = "--";
				this.m_Gauge.fillAmount = 1f;
			}
			else
			{
				this.m_TextValue.text = ((long)nextExp - currentExp).ToString();
				this.m_Gauge.fillAmount = (float)currentExp / (float)nextExp;
			}
		}

		// Token: 0x06002C4A RID: 11338 RVA: 0x000E9C48 File Offset: 0x000E8048
		public void Play(int friendship, long exp, int afterFrendship, long afterExp, sbyte expTableID)
		{
			this.Apply(friendship, exp, expTableID);
			this.m_Step = FriendshipGauge.eStep.Play;
			this.m_Lv = friendship;
			this.m_Exp = exp;
			this.m_AfterLv = afterFrendship;
			this.m_AfterExp = afterExp;
			this.m_TableID = expTableID;
			NamedFriendshipExpDB namedFriendshipExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB;
			this.m_NextExp = (long)namedFriendshipExpDB.GetNextExp(this.m_Lv, this.m_TableID);
		}

		// Token: 0x06002C4B RID: 11339 RVA: 0x000E9CB4 File Offset: 0x000E80B4
		public void Skip()
		{
			this.Apply(this.m_AfterLv, this.m_AfterExp, this.m_TableID);
			this.m_OnFinishPlay.Call();
			this.m_Step = FriendshipGauge.eStep.None;
		}

		// Token: 0x06002C4C RID: 11340 RVA: 0x000E9CE0 File Offset: 0x000E80E0
		private void Update()
		{
			FriendshipGauge.eStep step = this.m_Step;
			if (step != FriendshipGauge.eStep.None)
			{
				if (step == FriendshipGauge.eStep.Play)
				{
					if (this.m_NextExp > 0L)
					{
						this.m_Exp += (long)((float)this.m_NextExp * Time.deltaTime) + 1L;
						NamedFriendshipExpDB namedFriendshipExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB;
						if (namedFriendshipExpDB.GetCurrentExp(this.m_Lv, this.m_Exp, this.m_TableID) >= this.m_NextExp && this.m_NextExp != 0L)
						{
							this.m_Lv++;
							this.m_ImageNumber.SetValue(this.m_Lv);
							this.m_NextExp = (long)namedFriendshipExpDB.GetNextExp(this.m_Lv, this.m_TableID);
							this.m_OnLevelUp.Call();
							this.m_LvPopPlayer.Play();
						}
						this.Apply(this.m_Lv, this.m_Exp, this.m_TableID);
					}
					if ((this.m_Lv >= this.m_AfterLv && this.m_Exp >= this.m_AfterExp) || this.m_NextExp == 0L)
					{
						this.Apply(this.m_AfterLv, this.m_AfterExp, this.m_TableID);
						this.m_OnFinishPlay.Call();
						this.m_Step = FriendshipGauge.eStep.None;
					}
				}
			}
		}

		// Token: 0x06002C4D RID: 11341 RVA: 0x000E9E3A File Offset: 0x000E823A
		public void HidePop()
		{
			this.m_LvPopPlayer.HidePopUp();
		}

		// Token: 0x04003311 RID: 13073
		private const float GAUGE_SPEED = 100f;

		// Token: 0x04003312 RID: 13074
		[SerializeField]
		private ImageNumbers m_ImageNumber;

		// Token: 0x04003313 RID: 13075
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04003314 RID: 13076
		[SerializeField]
		private Text m_TextValue;

		// Token: 0x04003315 RID: 13077
		[SerializeField]
		private LvupPopPlayer m_LvPopPlayer;

		// Token: 0x04003316 RID: 13078
		private FriendshipGauge.eStep m_Step;

		// Token: 0x04003317 RID: 13079
		private int m_Lv;

		// Token: 0x04003318 RID: 13080
		private long m_Exp;

		// Token: 0x04003319 RID: 13081
		private int m_AfterLv;

		// Token: 0x0400331A RID: 13082
		private long m_AfterExp;

		// Token: 0x0400331B RID: 13083
		private sbyte m_TableID;

		// Token: 0x0400331C RID: 13084
		private long m_NextExp;

		// Token: 0x0200084E RID: 2126
		private enum eStep
		{
			// Token: 0x04003320 RID: 13088
			None,
			// Token: 0x04003321 RID: 13089
			Play
		}
	}
}
