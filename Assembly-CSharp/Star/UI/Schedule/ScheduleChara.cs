﻿using System;
using UnityEngine;

namespace Star.UI.Schedule
{
	// Token: 0x02000A94 RID: 2708
	public class ScheduleChara : MonoBehaviour
	{
		// Token: 0x140000D6 RID: 214
		// (add) Token: 0x06003886 RID: 14470 RVA: 0x0011E66C File Offset: 0x0011CA6C
		// (remove) Token: 0x06003887 RID: 14471 RVA: 0x0011E6A4 File Offset: 0x0011CAA4
		public event Action<int> OnClickScheduleChara;

		// Token: 0x06003888 RID: 14472 RVA: 0x0011E6DA File Offset: 0x0011CADA
		public long GetCharaMngID()
		{
			return this.m_CharaMngID;
		}

		// Token: 0x06003889 RID: 14473 RVA: 0x0011E6E4 File Offset: 0x0011CAE4
		public void Apply(int idx, long charaMngID)
		{
			this.m_MemberIdx = idx;
			this.m_CharaMngID = charaMngID;
			if (charaMngID != -1L)
			{
				this.m_EmptyBody.SetActive(false);
				this.m_Body.SetActive(true);
				int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID).Param.CharaID;
				this.m_IconCloner.GetInst<CharaIconWithFrame>().ApplyCharaMngID(charaMngID);
			}
			else
			{
				this.m_Body.SetActive(false);
				this.m_EmptyBody.SetActive(true);
				this.m_IconCloner.GetInst<CharaIconWithFrame>().Destroy();
			}
		}

		// Token: 0x0600388A RID: 14474 RVA: 0x0011E778 File Offset: 0x0011CB78
		public void Refresh()
		{
			this.m_IconCloner.GetInst<CharaIconWithFrame>().Destroy();
			if (this.m_CharaMngID != -1L)
			{
				this.m_IconCloner.GetInst<CharaIconWithFrame>().ApplyCharaMngID(this.m_CharaMngID);
			}
		}

		// Token: 0x0600388B RID: 14475 RVA: 0x0011E7AD File Offset: 0x0011CBAD
		public void Destroy()
		{
			this.m_IconCloner.GetInst<CharaIconWithFrame>().Destroy();
		}

		// Token: 0x0600388C RID: 14476 RVA: 0x0011E7BF File Offset: 0x0011CBBF
		public void OnClickCallBack()
		{
			this.OnClickScheduleChara.Call(this.m_MemberIdx);
		}

		// Token: 0x0600388D RID: 14477 RVA: 0x0011E7D2 File Offset: 0x0011CBD2
		public void OnHoldCallBack()
		{
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(this.m_CharaMngID, false);
		}

		// Token: 0x04003F5B RID: 16219
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x04003F5C RID: 16220
		[SerializeField]
		private GameObject m_EmptyBody;

		// Token: 0x04003F5D RID: 16221
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04003F5E RID: 16222
		private int m_MemberIdx;

		// Token: 0x04003F5F RID: 16223
		private long m_CharaMngID = -1L;
	}
}
