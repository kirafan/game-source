﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000A97 RID: 2711
	public class ScheduleTimeItem : ScrollItemIcon
	{
		// Token: 0x06003896 RID: 14486 RVA: 0x0011E9B4 File Offset: 0x0011CDB4
		protected override void ApplyData()
		{
			this.m_Text.text = ((ScheduleTimeItemData)this.m_Data).m_Text;
		}

		// Token: 0x04003F6A RID: 16234
		[SerializeField]
		private Text m_Text;
	}
}
