﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000A95 RID: 2709
	public class ScheduleCharaSwapGroup : UIGroup
	{
		// Token: 0x0600388F RID: 14479 RVA: 0x0011E7F0 File Offset: 0x0011CBF0
		public void Open(long outMngID, long inMngID, Action OnDecide)
		{
			this.m_OnDecide = OnDecide;
			this.m_OutCharaCloner.GetInst<CharaIconWithFrame>().ApplyCharaMngID(outMngID);
			this.m_InCharaCloner.GetInst<CharaIconWithFrame>().ApplyCharaMngID(inMngID);
			this.m_OutCharaNameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(outMngID).Param.NamedType).m_NickName;
			this.m_InCharaNameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(inMngID).Param.NamedType).m_NickName;
			this.Open();
		}

		// Token: 0x06003890 RID: 14480 RVA: 0x0011E8AE File Offset: 0x0011CCAE
		public void OnClickDecideCallBack()
		{
			this.m_OnDecide.Call();
			this.Close();
		}

		// Token: 0x04003F61 RID: 16225
		[SerializeField]
		private PrefabCloner m_OutCharaCloner;

		// Token: 0x04003F62 RID: 16226
		[SerializeField]
		private Text m_OutCharaNameText;

		// Token: 0x04003F63 RID: 16227
		[SerializeField]
		private PrefabCloner m_InCharaCloner;

		// Token: 0x04003F64 RID: 16228
		[SerializeField]
		private Text m_InCharaNameText;

		// Token: 0x04003F65 RID: 16229
		private Action m_OnDecide;
	}
}
