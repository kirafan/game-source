﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000A96 RID: 2710
	public class ScheduleItem : MonoBehaviour
	{
		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06003892 RID: 14482 RVA: 0x0011E8C9 File Offset: 0x0011CCC9
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06003893 RID: 14483 RVA: 0x0011E8EE File Offset: 0x0011CCEE
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x06003894 RID: 14484 RVA: 0x0011E914 File Offset: 0x0011CD14
		public void SetSchedule(string scheduleName, Sprite baseSprite, float length)
		{
			this.RectTransform.sizeDelta = new Vector2(length, this.RectTransform.sizeDelta.y);
			this.m_BaseImage.sprite = baseSprite;
			this.m_NameText.text = scheduleName;
			if (scheduleName.Length <= 4)
			{
				if (this.m_NameText.fontSize != 30)
				{
					this.m_NameText.fontSize = 30;
				}
			}
			else if (this.m_NameText.fontSize != 25)
			{
				this.m_NameText.fontSize = 25;
			}
		}

		// Token: 0x04003F66 RID: 16230
		[SerializeField]
		private Image m_BaseImage;

		// Token: 0x04003F67 RID: 16231
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003F68 RID: 16232
		private RectTransform m_Rect;

		// Token: 0x04003F69 RID: 16233
		private GameObject m_GameObject;
	}
}
