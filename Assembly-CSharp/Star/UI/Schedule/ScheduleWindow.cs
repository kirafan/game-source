﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.CharaList;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Schedule
{
	// Token: 0x02000A99 RID: 2713
	public class ScheduleWindow : UIGroup
	{
		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06003899 RID: 14489 RVA: 0x0011E9FE File Offset: 0x0011CDFE
		public long[] LiveCharaMngIDs
		{
			get
			{
				return this.m_CharaMngIDs;
			}
		}

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x0600389A RID: 14490 RVA: 0x0011EA06 File Offset: 0x0011CE06
		public int MemberIdx
		{
			get
			{
				return this.m_MemberIdx;
			}
		}

		// Token: 0x140000D7 RID: 215
		// (add) Token: 0x0600389B RID: 14491 RVA: 0x0011EA10 File Offset: 0x0011CE10
		// (remove) Token: 0x0600389C RID: 14492 RVA: 0x0011EA48 File Offset: 0x0011CE48
		public event Action OnChangeChara;

		// Token: 0x0600389D RID: 14493 RVA: 0x0011EA80 File Offset: 0x0011CE80
		public void Setup()
		{
			this.m_Content = this.m_HndleScroll.content;
			this.m_ViewPort = this.m_HndleScroll.viewport;
			this.m_Content.sizeDelta = new Vector2(this.m_LengthHour * 24f, this.m_Content.sizeDelta.y);
			for (int i = 0; i < 24; i++)
			{
				Text text = UnityEngine.Object.Instantiate<Text>(this.m_TimeTextPrefab, this.m_TimeScroll.content);
				text.text = i.ToString() + ":00";
			}
			this.m_CharaListWindow.OnEnd += this.OnEndCharaListCallBack;
			this.m_CharaListWindow.OnSelect += this.OnSelectCallBack;
			this.m_ScheduleCharas = new ScheduleChara[this.m_CharaCloners.Length];
			for (int j = 0; j < this.m_CharaCloners.Length; j++)
			{
				this.m_ScheduleCharas[j] = this.m_CharaCloners[j].GetInst<ScheduleChara>();
				this.m_ScheduleCharas[j].OnClickScheduleChara += this.OpenCharaList;
			}
			this.ApplyRoomChangeButton();
		}

		// Token: 0x0600389E RID: 14494 RVA: 0x0011EBB4 File Offset: 0x0011CFB4
		public override void Update()
		{
			base.Update();
			if (base.State == UIGroup.eState.In)
			{
				DateTime manageUnixTime = ScheduleTimeUtil.GetManageUnixTime();
				float num = this.m_ViewPort.rect.width / this.m_LengthHour;
				float value = (float)manageUnixTime.Hour / (24f - num);
				this.m_HndleScroll.horizontalNormalizedPosition = Mathf.Clamp(value, 0f, 1f);
				this.OnScrollValueChanged();
			}
			ScheduleWindow.eStep step = this.m_Step;
			if (step != ScheduleWindow.eStep.Schedule)
			{
				if (step == ScheduleWindow.eStep.CharaList)
				{
					if (this.m_CharaListWindow.IsDonePlayOut)
					{
						this.m_Step = ScheduleWindow.eStep.Schedule;
						base.SetEnableInput(true);
					}
				}
			}
		}

		// Token: 0x0600389F RID: 14495 RVA: 0x0011EC6C File Offset: 0x0011D06C
		public override void LateUpdate()
		{
			base.LateUpdate();
			if (this.m_IsDirty)
			{
				for (int i = 0; i < this.m_Scrolls.Length; i++)
				{
					this.m_Scrolls[i].gameObject.SetActive(true);
				}
				this.OnScrollValueChanged();
				this.m_IsDirty = false;
			}
		}

		// Token: 0x060038A0 RID: 14496 RVA: 0x0011ECC4 File Offset: 0x0011D0C4
		public void OnScrollValueChanged()
		{
			if (this.m_Content != null)
			{
				this.m_TimeScroll.horizontalNormalizedPosition = 0f - this.m_Content.anchoredPosition.x / (this.m_Content.sizeDelta.x - this.m_ViewPort.rect.width - this.m_LengthHour * 0.5f);
				for (int i = 0; i < this.m_Scrolls.Length; i++)
				{
					this.m_Scrolls[i].horizontalNormalizedPosition = 0f - this.m_Content.anchoredPosition.x / (this.m_Content.sizeDelta.x - this.m_ViewPort.rect.width);
				}
			}
		}

		// Token: 0x060038A1 RID: 14497 RVA: 0x0011EDA8 File Offset: 0x0011D1A8
		private void Apply(long[] charaMngIDs)
		{
			this.SinkAll();
			for (int i = 0; i < this.m_ScheduleCharas.Length; i++)
			{
				int num = i + this.m_RoomIdx * this.m_ScheduleCharas.Length;
				if (num >= charaMngIDs.Length)
				{
					this.m_ScheduleCharas[i].Apply(i, -1L);
				}
				else
				{
					this.m_ScheduleCharas[i].Apply(i, charaMngIDs[num]);
				}
			}
			for (int j = 0; j < this.m_ScheduleCharas.Length; j++)
			{
				int num2 = j + this.m_RoomIdx * this.m_ScheduleCharas.Length;
				if (num2 < charaMngIDs.Length)
				{
					ScheduleUIDataPack charaManageIDToScheduleUIData = FieldMapManager.GetCharaManageIDToScheduleUIData(charaMngIDs[num2]);
					Debug.LogWarning("CharaMngID = " + charaMngIDs[num2]);
					if (charaManageIDToScheduleUIData != null)
					{
						int num3 = 0;
						for (int k = 1; k < charaManageIDToScheduleUIData.m_Table.Length; k++)
						{
							Debug.LogWarning(string.Concat(new object[]
							{
								"Schedule nameTagID = ",
								charaManageIDToScheduleUIData.m_Table[k].m_NameTagID,
								" Time = ",
								charaManageIDToScheduleUIData.m_Table[k].m_Time
							}));
							if (charaManageIDToScheduleUIData.m_Table[num3].m_NameTagID == charaManageIDToScheduleUIData.m_Table[k].m_NameTagID)
							{
								ScheduleUIData[] table = charaManageIDToScheduleUIData.m_Table;
								int num4 = num3;
								table[num4].m_Time = table[num4].m_Time + charaManageIDToScheduleUIData.m_Table[k].m_Time;
								charaManageIDToScheduleUIData.m_Table[k].m_Time = 0;
								Debug.LogWarning("↑Schedule UI Link! NameTagID = " + charaManageIDToScheduleUIData.m_Table[num3].m_NameTagID);
							}
							else
							{
								num3 = k;
							}
						}
						for (int k = 0; k < charaManageIDToScheduleUIData.m_Table.Length; k++)
						{
							if (charaManageIDToScheduleUIData.m_Table[k].m_Time > 0)
							{
								ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(charaManageIDToScheduleUIData.m_Table[k].m_NameTagID);
								ScheduleItem scheduleItem = this.ScoopInst(this.m_Scrolls[j].content);
								scheduleItem.SetSchedule(scheduleNameTag.m_DisplayName, this.m_BaseSprites[scheduleNameTag.m_DisplayColor], (float)charaManageIDToScheduleUIData.m_Table[k].m_Time * this.m_LengthHour / 60f);
								scheduleItem.RectTransform.SetParent(this.m_Scrolls[j].content, false);
								scheduleItem.RectTransform.SetAsLastSibling();
							}
						}
					}
				}
				this.m_Scrolls[j].gameObject.SetActive(false);
			}
			this.m_IsDirty = true;
		}

		// Token: 0x060038A2 RID: 14498 RVA: 0x0011F058 File Offset: 0x0011D458
		private ScheduleItem ScoopInst(RectTransform parent)
		{
			ScheduleItem scheduleItem;
			if (this.m_EmptyInstList.Count <= 0)
			{
				scheduleItem = UnityEngine.Object.Instantiate<ScheduleItem>(this.m_ScheduleItemPrefab, parent, false);
				scheduleItem.RectTransform.SetAsLastSibling();
			}
			else
			{
				scheduleItem = this.m_EmptyInstList[0];
				this.m_EmptyInstList.RemoveAt(0);
				scheduleItem.RectTransform.SetParent(parent);
			}
			if (!scheduleItem.GameObject.activeSelf)
			{
				scheduleItem.GameObject.SetActive(true);
			}
			this.m_ActiveInstList.Add(scheduleItem);
			return scheduleItem;
		}

		// Token: 0x060038A3 RID: 14499 RVA: 0x0011F0E4 File Offset: 0x0011D4E4
		private void SinkAll()
		{
			while (this.m_ActiveInstList.Count > 0)
			{
				if (this.m_ActiveInstList[0].GameObject.activeSelf)
				{
					this.m_ActiveInstList[0].GameObject.SetActive(false);
				}
				this.m_EmptyInstList.Add(this.m_ActiveInstList[0]);
				this.m_ActiveInstList.RemoveAt(0);
			}
		}

		// Token: 0x060038A4 RID: 14500 RVA: 0x0011F15C File Offset: 0x0011D55C
		public void Open(int roomIdx, long[] charaMngIDs)
		{
			this.m_CharaMngIDs = new long[10];
			for (int i = 0; i < this.m_CharaMngIDs.Length; i++)
			{
				if (i < charaMngIDs.Length)
				{
					this.m_CharaMngIDs[i] = charaMngIDs[i];
				}
				else
				{
					this.m_CharaMngIDs[i] = -1L;
				}
			}
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnCancelButtonCallBack));
			this.m_InfoStack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Schedule);
			this.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Common, true);
		}

		// Token: 0x060038A5 RID: 14501 RVA: 0x0011F212 File Offset: 0x0011D612
		public override void Open()
		{
			this.m_Step = ScheduleWindow.eStep.Schedule;
			this.Apply(this.m_CharaMngIDs);
			base.Open();
		}

		// Token: 0x060038A6 RID: 14502 RVA: 0x0011F230 File Offset: 0x0011D630
		public override void Close()
		{
			base.Close();
			if (this.m_MemberIdx == -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_InfoStack);
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
				{
					this.m_SavedBackButtonCallBack(true);
				}
			}
		}

		// Token: 0x060038A7 RID: 14503 RVA: 0x0011F2A9 File Offset: 0x0011D6A9
		public void OnCancelButtonCallBack(bool isCallFromShortCut)
		{
			this.m_MemberIdx = -1;
			this.Close();
		}

		// Token: 0x060038A8 RID: 14504 RVA: 0x0011F2B8 File Offset: 0x0011D6B8
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_MemberIdx != -1)
			{
				this.m_CharaListWindow.Open(this.m_CharaMngIDs, this.m_MemberIdx);
			}
		}

		// Token: 0x060038A9 RID: 14505 RVA: 0x0011F2E4 File Offset: 0x0011D6E4
		public void Destroy()
		{
			for (int i = 0; i < this.m_ScheduleCharas.Length; i++)
			{
				this.m_ScheduleCharas[i].Destroy();
			}
		}

		// Token: 0x060038AA RID: 14506 RVA: 0x0011F317 File Offset: 0x0011D717
		public void OpenCharaList(int memberIdx)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_MemberIdx = this.m_RoomIdx * this.m_ScheduleCharas.Length + memberIdx;
			this.m_Step = ScheduleWindow.eStep.CharaList;
			base.SetEnableInput(false);
			this.Close();
		}

		// Token: 0x060038AB RID: 14507 RVA: 0x0011F354 File Offset: 0x0011D754
		public void OnSelectCallBack(long mngID)
		{
			if (mngID == -1L)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.ScheduleSwapTitle, eText_MessageDB.ScheduleRemove, new Action<int>(this.OnConfirmRemove));
			}
			else if (this.m_CharaMngIDs[this.m_MemberIdx] == -1L || this.m_CharaMngIDs[this.m_MemberIdx] == mngID)
			{
				this.m_CharaListWindow.DecideChara();
			}
			else
			{
				this.m_SwapGroup.Open(this.m_CharaMngIDs[this.m_MemberIdx], mngID, new Action(this.OnDecideSwap));
			}
		}

		// Token: 0x060038AC RID: 14508 RVA: 0x0011F3F0 File Offset: 0x0011D7F0
		private void OnConfirmRemove(int btn)
		{
			if (btn == 0)
			{
				this.OnDecideSwap();
			}
		}

		// Token: 0x060038AD RID: 14509 RVA: 0x0011F3FE File Offset: 0x0011D7FE
		private void OnDecideSwap()
		{
			this.m_CharaListWindow.DecideChara();
		}

		// Token: 0x060038AE RID: 14510 RVA: 0x0011F40C File Offset: 0x0011D80C
		public void OnEndCharaListCallBack()
		{
			if (!this.m_CharaListWindow.IsCanceled)
			{
				this.m_CharaMngIDs[this.m_MemberIdx] = this.m_CharaListWindow.SelectCharaMngID;
				this.OnChangeChara.Call();
			}
			this.Open();
			for (int i = 0; i < this.m_ScheduleCharas.Length; i++)
			{
				int num = i + this.m_RoomIdx * this.m_ScheduleCharas.Length;
				if (num >= this.m_CharaMngIDs.Length)
				{
					this.m_ScheduleCharas[i].Refresh();
				}
				else
				{
					this.m_ScheduleCharas[i].Refresh();
				}
			}
		}

		// Token: 0x060038AF RID: 14511 RVA: 0x0011F4AA File Offset: 0x0011D8AA
		public void OnClickRoomToggleButtonCallBack()
		{
			this.m_RoomIdx = 1 - this.m_RoomIdx;
			this.Apply(this.m_CharaMngIDs);
			this.ApplyRoomChangeButton();
		}

		// Token: 0x060038B0 RID: 14512 RVA: 0x0011F4CC File Offset: 0x0011D8CC
		private void ApplyRoomChangeButton()
		{
			if (this.m_RoomIdx == 0)
			{
				this.m_SubRoomButton.Interactable = UserRoomUtil.IsHaveSubRoom(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
				this.m_SubRoomButton.GetComponentInChildren<Text>().text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomSubRoom);
			}
			else
			{
				this.m_SubRoomButton.Interactable = true;
				this.m_SubRoomButton.GetComponentInChildren<Text>().text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomMainRoom);
			}
		}

		// Token: 0x04003F6C RID: 16236
		private ScheduleWindow.eStep m_Step;

		// Token: 0x04003F6D RID: 16237
		[SerializeField]
		private CustomButton m_SubRoomButton;

		// Token: 0x04003F6E RID: 16238
		[SerializeField]
		private PrefabCloner[] m_CharaCloners;

		// Token: 0x04003F6F RID: 16239
		private ScheduleChara[] m_ScheduleCharas;

		// Token: 0x04003F70 RID: 16240
		[SerializeField]
		private float m_LengthHour;

		// Token: 0x04003F71 RID: 16241
		[SerializeField]
		private ScrollRect[] m_Scrolls;

		// Token: 0x04003F72 RID: 16242
		[SerializeField]
		private ScrollRect m_HndleScroll;

		// Token: 0x04003F73 RID: 16243
		private RectTransform m_ViewPort;

		// Token: 0x04003F74 RID: 16244
		private RectTransform m_Content;

		// Token: 0x04003F75 RID: 16245
		[SerializeField]
		private ScrollRect m_TimeScroll;

		// Token: 0x04003F76 RID: 16246
		[SerializeField]
		private Text m_TimeTextPrefab;

		// Token: 0x04003F77 RID: 16247
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x04003F78 RID: 16248
		[SerializeField]
		private ScheduleCharaSwapGroup m_SwapGroup;

		// Token: 0x04003F79 RID: 16249
		[SerializeField]
		private ScheduleItem m_ScheduleItemPrefab;

		// Token: 0x04003F7A RID: 16250
		[SerializeField]
		private Sprite[] m_BaseSprites;

		// Token: 0x04003F7B RID: 16251
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04003F7C RID: 16252
		private GlobalUI.SceneInfoStack m_InfoStack;

		// Token: 0x04003F7D RID: 16253
		private List<ScheduleItem> m_EmptyInstList = new List<ScheduleItem>();

		// Token: 0x04003F7E RID: 16254
		private List<ScheduleItem> m_ActiveInstList = new List<ScheduleItem>();

		// Token: 0x04003F7F RID: 16255
		private long[] m_CharaMngIDs;

		// Token: 0x04003F80 RID: 16256
		private int m_MemberIdx = -1;

		// Token: 0x04003F81 RID: 16257
		public int m_RoomIdx;

		// Token: 0x04003F82 RID: 16258
		private bool m_IsDirty;

		// Token: 0x02000A9A RID: 2714
		private enum eStep
		{
			// Token: 0x04003F85 RID: 16261
			Schedule,
			// Token: 0x04003F86 RID: 16262
			CharaList
		}
	}
}
