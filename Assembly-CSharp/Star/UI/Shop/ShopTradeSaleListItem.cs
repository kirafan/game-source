﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AA9 RID: 2729
	public class ShopTradeSaleListItem : ScrollItemIcon
	{
		// Token: 0x060038F7 RID: 14583 RVA: 0x00121428 File Offset: 0x0011F828
		protected override void ApplyData()
		{
			base.ApplyData();
			ShopTradeSaleListItemData shopTradeSaleListItemData = (ShopTradeSaleListItemData)this.m_Data;
			this.m_ItemIconWithFrameCloner.GetInst<ItemIconWithFrame>().Apply(shopTradeSaleListItemData.m_ItemID);
			this.m_NameText.text = shopTradeSaleListItemData.m_NameText;
			this.m_HaveNumText.text = UIUtility.ItemHaveNumToString(shopTradeSaleListItemData.m_HaveNum, true);
			this.m_SaleAmountText.text = UIUtility.GoldValueToString(shopTradeSaleListItemData.m_SaleAmount, false);
		}

		// Token: 0x060038F8 RID: 14584 RVA: 0x0012149C File Offset: 0x0011F89C
		public void ChangeHaveNum(int useNum)
		{
			ShopTradeSaleListItemData shopTradeSaleListItemData = (ShopTradeSaleListItemData)this.m_Data;
			if (useNum > 0)
			{
				this.m_HaveNumText.text = "<color=red>" + UIUtility.ItemHaveNumToString(shopTradeSaleListItemData.m_HaveNum - useNum, true) + "</color>";
			}
			else
			{
				this.m_HaveNumText.text = UIUtility.ItemHaveNumToString(shopTradeSaleListItemData.m_HaveNum - useNum, true);
			}
		}

		// Token: 0x04003FEC RID: 16364
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x04003FED RID: 16365
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003FEE RID: 16366
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04003FEF RID: 16367
		[SerializeField]
		private Text m_SaleAmountText;
	}
}
