﻿using System;

namespace Star.UI.Shop
{
	// Token: 0x02000AA4 RID: 2724
	public class ShopTradeListItemData : ScrollItemData
	{
		// Token: 0x060038D7 RID: 14551 RVA: 0x00120608 File Offset: 0x0011EA08
		public ShopTradeListItemData(ShopManager.TradeData tradeData)
		{
			this.m_TradeData = tradeData;
			ePresentType dstType = tradeData.m_DstType;
			if (dstType != ePresentType.Item)
			{
				if (dstType != ePresentType.Chara)
				{
					if (dstType == ePresentType.Weapon)
					{
						this.m_TradeName = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(tradeData.m_DstID).m_WeaponName;
					}
				}
				else
				{
					this.m_TradeName = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(tradeData.m_DstID).m_Name;
				}
			}
			else if (tradeData.m_DstAmount > 1)
			{
				this.m_TradeName = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(tradeData.m_DstID).m_Name + "x" + tradeData.m_DstAmount;
			}
			else
			{
				this.m_TradeName = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(tradeData.m_DstID).m_Name;
			}
		}

		// Token: 0x140000DB RID: 219
		// (add) Token: 0x060038D8 RID: 14552 RVA: 0x0012071C File Offset: 0x0011EB1C
		// (remove) Token: 0x060038D9 RID: 14553 RVA: 0x00120754 File Offset: 0x0011EB54
		public event Action<ShopManager.TradeData> OnClick;

		// Token: 0x060038DA RID: 14554 RVA: 0x0012078A File Offset: 0x0011EB8A
		public override void OnClickItemCallBack()
		{
			this.OnClick.Call(this.m_TradeData);
		}

		// Token: 0x04003FC3 RID: 16323
		public ShopManager.TradeData m_TradeData;

		// Token: 0x04003FC4 RID: 16324
		public string m_TradeName;
	}
}
