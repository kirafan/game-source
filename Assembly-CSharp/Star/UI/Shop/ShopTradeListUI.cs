﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AA5 RID: 2725
	public class ShopTradeListUI : MenuUIBase
	{
		// Token: 0x140000DC RID: 220
		// (add) Token: 0x060038DC RID: 14556 RVA: 0x001207C4 File Offset: 0x0011EBC4
		// (remove) Token: 0x060038DD RID: 14557 RVA: 0x001207FC File Offset: 0x0011EBFC
		public event Action<int, int> OnClickExecuteTradeButton;

		// Token: 0x060038DE RID: 14558 RVA: 0x00120834 File Offset: 0x0011EC34
		public void Setup(bool isLimitList, List<ShopManager.TradeData> tradeList)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_TradeList = tradeList;
			this.SetupTab();
			this.m_TabGroup.OnChangeIdx += this.OnChangeTabCallBack;
			this.m_Scroll.Setup();
			this.ApplyTab(this.m_TabList[0]);
			this.m_ConfirmGroup.OnClickExecute += this.OnClickExecuteButton;
			this.m_HaveIcons = new ItemIcon[this.m_HaveItems.Length];
			this.m_HaveItemNumText = new Text[this.m_HaveItems.Length];
			for (int i = 0; i < this.m_HaveItems.Length; i++)
			{
				this.m_HaveIcons[i] = this.m_HaveItems[i].GetComponentInChildren<ItemIcon>(true);
				this.m_HaveItemNumText[i] = this.m_HaveItems[i].GetComponentInChildren<Text>(true);
			}
			this.CalcHaveItemID(tradeList);
		}

		// Token: 0x060038DF RID: 14559 RVA: 0x00120948 File Offset: 0x0011ED48
		private void SetupTab()
		{
			this.m_TabList.Clear();
			for (int i = 0; i < this.m_TradeList.Count; i++)
			{
				if (this.m_TradeList[i].m_DstType == ePresentType.Item)
				{
					int dstID = this.m_TradeList[i].m_DstID;
					switch (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(dstID).m_Type)
					{
					case 0:
						if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Upgrade))
						{
							this.m_TabList.Add(ShopTradeListUI.eTab.Upgrade);
						}
						break;
					case 1:
						if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Evolution))
						{
							this.m_TabList.Add(ShopTradeListUI.eTab.Evolution);
						}
						break;
					case 2:
						goto IL_DA;
					case 3:
						if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Weapon))
						{
							this.m_TabList.Add(ShopTradeListUI.eTab.Weapon);
						}
						break;
					default:
						goto IL_DA;
					}
					goto IL_157;
					IL_DA:
					if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Etc))
					{
						this.m_TabList.Add(ShopTradeListUI.eTab.Etc);
					}
				}
				else if (this.m_TradeList[i].m_DstType == ePresentType.Weapon)
				{
					if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Weapon))
					{
						this.m_TabList.Add(ShopTradeListUI.eTab.Weapon);
					}
				}
				else if (!this.m_TabList.Contains(ShopTradeListUI.eTab.Etc))
				{
					this.m_TabList.Add(ShopTradeListUI.eTab.Etc);
				}
				IL_157:;
			}
			this.m_TabList.Sort();
			string[] array = new string[4];
			for (int j = 0; j < 4; j++)
			{
				array[j] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ShopTradeItemTypeUpgrade + j);
			}
			this.m_TabGroup.Setup(array, -1f);
			for (int k = 0; k < 4; k++)
			{
				this.m_TabGroup.SetIntaractable(this.m_TabList.Contains((ShopTradeListUI.eTab)k), k);
			}
			this.m_TabGroup.ChangeIdx((int)this.m_TabList[0]);
		}

		// Token: 0x060038E0 RID: 14560 RVA: 0x00120B60 File Offset: 0x0011EF60
		private void CalcHaveItemID(List<ShopManager.TradeData> tradeList)
		{
			this.m_HaveItemIDList.Clear();
			for (int i = 0; i < tradeList.Count; i++)
			{
				for (int j = 0; j < tradeList[i].m_SrcIDs.Length; j++)
				{
					int num = tradeList[i].m_SrcIDs[j];
					if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(num).m_Type == 6 && !this.m_HaveItemIDList.Contains(num))
					{
						this.m_HaveItemIDList.Add(num);
					}
				}
			}
			this.m_HaveItemIDList.Sort();
			for (int k = 0; k < this.m_HaveItems.Length; k++)
			{
				this.m_HaveItems[k].SetActive(k < this.m_HaveItemIDList.Count);
				if (k < this.m_HaveItemIDList.Count)
				{
					this.m_HaveIcons[k].Apply(this.m_HaveItemIDList[k]);
					this.m_HaveItemNumText[k].text = UIUtility.ItemHaveNumToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(this.m_HaveItemIDList[k]), true);
				}
			}
			this.m_KPText.text = UIUtility.KPValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint(), true);
		}

		// Token: 0x060038E1 RID: 14561 RVA: 0x00120CCC File Offset: 0x0011F0CC
		public void Refresh(List<ShopManager.TradeData> tradeList)
		{
			float scrollValue = this.m_Scroll.GetScrollValue();
			this.m_TradeList = tradeList;
			this.ApplyTab(this.m_Tab);
			this.m_Scroll.SetScrollValue(scrollValue);
			this.m_ConfirmGroup.Refresh();
			this.CalcHaveItemID(tradeList);
			if (this.m_ConfirmGroup.GetCurrentTradeData().RemainNum <= 0)
			{
				this.CloseConfirm();
			}
		}

		// Token: 0x060038E2 RID: 14562 RVA: 0x00120D34 File Offset: 0x0011F134
		private void ApplyTab(ShopTradeListUI.eTab tab)
		{
			this.m_Tab = tab;
			this.m_Scroll.RemoveAllData();
			for (int i = 0; i < this.m_TradeList.Count; i++)
			{
				ShopManager.TradeData tradeData = this.m_TradeList[i];
				ShopTradeListUI.eTab eTab = ShopTradeListUI.eTab.Etc;
				ePresentType dstType = tradeData.m_DstType;
				if (dstType != ePresentType.Item)
				{
					if (dstType == ePresentType.Weapon)
					{
						eTab = ShopTradeListUI.eTab.Weapon;
					}
				}
				else
				{
					eItemType type = (eItemType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(tradeData.m_DstID).m_Type;
					if (type != eItemType.Upgrade)
					{
						if (type != eItemType.Evolution)
						{
							if (type == eItemType.Weapon)
							{
								eTab = ShopTradeListUI.eTab.Weapon;
							}
						}
						else
						{
							eTab = ShopTradeListUI.eTab.Evolution;
						}
					}
					else
					{
						eTab = ShopTradeListUI.eTab.Upgrade;
					}
				}
				if (eTab == tab)
				{
					ShopTradeListItemData shopTradeListItemData = new ShopTradeListItemData(tradeData);
					shopTradeListItemData.OnClick += this.OnClickRecipeCallBack;
					this.m_Scroll.AddItem(shopTradeListItemData);
				}
			}
		}

		// Token: 0x060038E3 RID: 14563 RVA: 0x00120E2E File Offset: 0x0011F22E
		private void OnChangeTabCallBack(int idx)
		{
			this.ApplyTab((ShopTradeListUI.eTab)idx);
		}

		// Token: 0x060038E4 RID: 14564 RVA: 0x00120E38 File Offset: 0x0011F238
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopTradeListUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopTradeListUI.eStep.Idle);
				}
				break;
			}
			case ShopTradeListUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopTradeListUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060038E5 RID: 14565 RVA: 0x00120F48 File Offset: 0x0011F348
		private void ChangeStep(ShopTradeListUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopTradeListUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopTradeListUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Trade, true);
				this.m_MainGroup.Open();
				break;
			case ShopTradeListUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopTradeListUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				if (this.m_MainGroup.IsOpenOrIn())
				{
					this.m_MainGroup.Close();
				}
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				break;
			case ShopTradeListUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060038E6 RID: 14566 RVA: 0x00121044 File Offset: 0x0011F444
		public override void PlayIn()
		{
			this.ChangeStep(ShopTradeListUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x060038E7 RID: 14567 RVA: 0x00121080 File Offset: 0x0011F480
		public override void PlayOut()
		{
			this.ChangeStep(ShopTradeListUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x060038E8 RID: 14568 RVA: 0x001210BA File Offset: 0x0011F4BA
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopTradeListUI.eStep.End;
		}

		// Token: 0x060038E9 RID: 14569 RVA: 0x001210C8 File Offset: 0x0011F4C8
		public void OnClickRecipeCallBack(ShopManager.TradeData tradeData)
		{
			this.m_MainGroup.Close();
			this.m_SceneInfo = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.TradeConfirm);
			this.m_ConfirmGroup.Open(tradeData);
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnBackConfirmCallBack));
		}

		// Token: 0x060038EA RID: 14570 RVA: 0x00121134 File Offset: 0x0011F534
		public void OnClickExecuteButton(int recipeId, int idx)
		{
			this.OnClickExecuteTradeButton.Call(recipeId, idx);
		}

		// Token: 0x060038EB RID: 14571 RVA: 0x00121143 File Offset: 0x0011F543
		public void OnBackConfirmCallBack(bool shortcut)
		{
			if (shortcut)
			{
				this.m_ConfirmGroup.Close();
				this.m_SavedBackButtonCallBack.Call(shortcut);
			}
			else
			{
				this.CloseConfirm();
			}
		}

		// Token: 0x060038EC RID: 14572 RVA: 0x00121170 File Offset: 0x0011F570
		public void CloseConfirm()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfo);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
			this.m_MainGroup.Open();
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x04003FC6 RID: 16326
		private ShopTradeListUI.eStep m_Step;

		// Token: 0x04003FC7 RID: 16327
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003FC8 RID: 16328
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003FC9 RID: 16329
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04003FCA RID: 16330
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003FCB RID: 16331
		[SerializeField]
		private ShopTradeConfirmGroup m_ConfirmGroup;

		// Token: 0x04003FCC RID: 16332
		[SerializeField]
		private GameObject[] m_HaveItems;

		// Token: 0x04003FCD RID: 16333
		private ItemIcon[] m_HaveIcons;

		// Token: 0x04003FCE RID: 16334
		private Text[] m_HaveItemNumText;

		// Token: 0x04003FCF RID: 16335
		[SerializeField]
		private Text m_KPText;

		// Token: 0x04003FD0 RID: 16336
		private List<int> m_HaveItemIDList = new List<int>();

		// Token: 0x04003FD1 RID: 16337
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04003FD2 RID: 16338
		private List<ShopTradeListUI.eTab> m_TabList = new List<ShopTradeListUI.eTab>();

		// Token: 0x04003FD3 RID: 16339
		private ShopTradeListUI.eTab m_Tab = ShopTradeListUI.eTab.Etc;

		// Token: 0x04003FD4 RID: 16340
		private List<ShopManager.TradeData> m_TradeList;

		// Token: 0x04003FD5 RID: 16341
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x02000AA6 RID: 2726
		private enum eStep
		{
			// Token: 0x04003FD8 RID: 16344
			Hide,
			// Token: 0x04003FD9 RID: 16345
			In,
			// Token: 0x04003FDA RID: 16346
			Idle,
			// Token: 0x04003FDB RID: 16347
			Out,
			// Token: 0x04003FDC RID: 16348
			End
		}

		// Token: 0x02000AA7 RID: 2727
		public enum eTab
		{
			// Token: 0x04003FDE RID: 16350
			Upgrade,
			// Token: 0x04003FDF RID: 16351
			Evolution,
			// Token: 0x04003FE0 RID: 16352
			Weapon,
			// Token: 0x04003FE1 RID: 16353
			Etc,
			// Token: 0x04003FE2 RID: 16354
			Num
		}
	}
}
