﻿using System;

namespace Star.UI.Shop
{
	// Token: 0x02000AAA RID: 2730
	public class ShopTradeSaleListItemData : ScrollItemData
	{
		// Token: 0x060038F9 RID: 14585 RVA: 0x00121504 File Offset: 0x0011F904
		public ShopTradeSaleListItemData(int itemID)
		{
			this.m_ItemID = itemID;
			ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID);
			this.m_NameText = param.m_Name;
			this.m_HaveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(itemID);
			this.m_SaleAmount = param.m_SaleAmount;
		}

		// Token: 0x140000DF RID: 223
		// (add) Token: 0x060038FA RID: 14586 RVA: 0x00121564 File Offset: 0x0011F964
		// (remove) Token: 0x060038FB RID: 14587 RVA: 0x0012159C File Offset: 0x0011F99C
		public event Action<int> OnClick;

		// Token: 0x060038FC RID: 14588 RVA: 0x001215D2 File Offset: 0x0011F9D2
		public override void OnClickItemCallBack()
		{
			this.OnClick.Call(this.m_ItemID);
		}

		// Token: 0x04003FF0 RID: 16368
		public int m_ItemID;

		// Token: 0x04003FF1 RID: 16369
		public string m_NameText;

		// Token: 0x04003FF2 RID: 16370
		public int m_HaveNum;

		// Token: 0x04003FF3 RID: 16371
		public int m_SaleAmount;
	}
}
