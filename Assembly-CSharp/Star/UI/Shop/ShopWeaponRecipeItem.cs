﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AB6 RID: 2742
	public class ShopWeaponRecipeItem : ScrollItemIcon
	{
		// Token: 0x0600393F RID: 14655 RVA: 0x001232A4 File Offset: 0x001216A4
		protected override void ApplyData()
		{
			base.ApplyData();
			ShopWeaponRecipeItemData shopWeaponRecipeItemData = (ShopWeaponRecipeItemData)this.m_Data;
			WeaponRecipeListDB_Param paramWithRecipeID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.GetParamWithRecipeID(shopWeaponRecipeItemData.m_RecipeID);
			bool flag = true;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit <= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas.Count)
			{
				flag = false;
			}
			else if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)paramWithRecipeID.m_Amount))
			{
				flag = false;
			}
			else
			{
				for (int i = 0; i < paramWithRecipeID.m_ItemIDs.Length; i++)
				{
					if (paramWithRecipeID.m_ItemIDs[i] != -1)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(paramWithRecipeID.m_ItemIDs[i]) < paramWithRecipeID.m_ItemNums[i])
						{
							flag = false;
							break;
						}
					}
				}
			}
			this.m_WeaponIconPrefabCloner.GetInst<WeaponIconWithFrame>().Apply(paramWithRecipeID.m_WeaponID);
			if (!this.m_CantExec && !flag)
			{
				this.m_CantExecCG.ChangeColor(UIDefine.CANT_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
				this.m_CantExec = true;
			}
			else if (this.m_CantExec && flag)
			{
				this.m_CantExecCG.RevertColor();
				this.m_CantExec = false;
			}
			this.m_NameText.text = shopWeaponRecipeItemData.m_WpnName;
			this.m_HaveNumText.text = EditUtility.CalcWeaponNum(shopWeaponRecipeItemData.m_WpnID).ToString();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(UIUtility.GoldValueToString(shopWeaponRecipeItemData.m_Amount, true));
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)shopWeaponRecipeItemData.m_Amount))
			{
				stringBuilder = UIUtility.GetNotEnoughString(stringBuilder);
			}
			this.m_AmountText.text = stringBuilder.ToString();
		}

		// Token: 0x04004055 RID: 16469
		[SerializeField]
		private PrefabCloner m_WeaponIconPrefabCloner;

		// Token: 0x04004056 RID: 16470
		[SerializeField]
		private ColorGroup m_CantExecCG;

		// Token: 0x04004057 RID: 16471
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004058 RID: 16472
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04004059 RID: 16473
		[SerializeField]
		private Text m_AmountText;

		// Token: 0x0400405A RID: 16474
		private bool m_CantExec;
	}
}
