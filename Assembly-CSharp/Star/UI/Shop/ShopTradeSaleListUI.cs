﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000AAB RID: 2731
	public class ShopTradeSaleListUI : MenuUIBase
	{
		// Token: 0x140000E0 RID: 224
		// (add) Token: 0x060038FE RID: 14590 RVA: 0x001215F0 File Offset: 0x0011F9F0
		// (remove) Token: 0x060038FF RID: 14591 RVA: 0x00121628 File Offset: 0x0011FA28
		public event Action<int, int> OnClickExecuteSaleButton;

		// Token: 0x06003900 RID: 14592 RVA: 0x00121660 File Offset: 0x0011FA60
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			string[] buttonTexts = new string[]
			{
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ShopTradeItemTypeUpgrade),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ShopTradeItemTypeLimitBreak),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ShopTradeItemTypeEvolution),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ShopTradeItemTypeWeaponItem)
			};
			this.m_TabGroup.Setup(buttonTexts, -1f);
			this.m_TabGroup.OnChangeIdx += this.OnChangeTabCallBack;
			this.m_Scroll.Setup();
			this.ApplyTab(this.m_Tab);
			this.m_ConfirmGroup.OnExecuteSaleButton += this.OnClickExecuteButton;
			this.m_ConfirmGroup.OnCancelSaleButton += this.OnCancelSaleButtonCallBack;
		}

		// Token: 0x06003901 RID: 14593 RVA: 0x00121768 File Offset: 0x0011FB68
		public void Refresh()
		{
			float scrollValue = this.m_Scroll.GetScrollValue();
			this.m_Scroll.RemoveAll();
			this.ApplyTab(this.m_Tab);
			this.m_Scroll.SetScrollValue(scrollValue);
		}

		// Token: 0x06003902 RID: 14594 RVA: 0x001217A4 File Offset: 0x0011FBA4
		private void ApplyTab(ShopTradeSaleListUI.eTab tab)
		{
			this.m_Tab = tab;
			this.m_Scroll.RemoveAllData();
			List<UserItemData> userItemDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserItemDatas;
			for (int i = 0; i < userItemDatas.Count; i++)
			{
				ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(userItemDatas[i].ItemID);
				if (userItemDatas[i].ItemNum > 0)
				{
					if (!string.IsNullOrEmpty(param.m_Name))
					{
						ShopTradeSaleListUI.eTab eTab;
						switch (param.m_Type)
						{
						case 0:
							eTab = ShopTradeSaleListUI.eTab.Upgrade;
							break;
						case 1:
							eTab = ShopTradeSaleListUI.eTab.Evolution;
							break;
						case 2:
							eTab = ShopTradeSaleListUI.eTab.LimitBreak;
							break;
						case 3:
							eTab = ShopTradeSaleListUI.eTab.Weapon;
							break;
						default:
							eTab = ShopTradeSaleListUI.eTab.None;
							break;
						}
						if (eTab == tab)
						{
							ShopTradeSaleListItemData shopTradeSaleListItemData = new ShopTradeSaleListItemData(param.m_ID);
							shopTradeSaleListItemData.OnClick += this.OnClickSaleListButtonCallBack;
							this.m_Scroll.AddItem(shopTradeSaleListItemData);
						}
					}
				}
			}
		}

		// Token: 0x06003903 RID: 14595 RVA: 0x001218BC File Offset: 0x0011FCBC
		private void OnChangeTabCallBack(int idx)
		{
			this.ApplyTab((ShopTradeSaleListUI.eTab)idx);
		}

		// Token: 0x06003904 RID: 14596 RVA: 0x001218C8 File Offset: 0x0011FCC8
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopTradeSaleListUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopTradeSaleListUI.eStep.Idle);
				}
				break;
			}
			case ShopTradeSaleListUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopTradeSaleListUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003905 RID: 14597 RVA: 0x001219D8 File Offset: 0x0011FDD8
		private void ChangeStep(ShopTradeSaleListUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopTradeSaleListUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopTradeSaleListUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Trade, true);
				this.m_MainGroup.Open();
				break;
			case ShopTradeSaleListUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopTradeSaleListUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				if (this.m_MainGroup.IsOpenOrIn())
				{
					this.m_MainGroup.Close();
				}
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				break;
			case ShopTradeSaleListUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003906 RID: 14598 RVA: 0x00121AD4 File Offset: 0x0011FED4
		public override void PlayIn()
		{
			this.ChangeStep(ShopTradeSaleListUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003907 RID: 14599 RVA: 0x00121B10 File Offset: 0x0011FF10
		public override void PlayOut()
		{
			this.ChangeStep(ShopTradeSaleListUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003908 RID: 14600 RVA: 0x00121B4A File Offset: 0x0011FF4A
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopTradeSaleListUI.eStep.End;
		}

		// Token: 0x06003909 RID: 14601 RVA: 0x00121B58 File Offset: 0x0011FF58
		public void OnClickSaleListButtonCallBack(int itemID)
		{
			this.m_MainGroup.Close();
			this.m_ConfirmGroup.Open(itemID);
			this.m_SceneInfo = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.SaleConfirm);
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnBackConfirmCallBack));
		}

		// Token: 0x0600390A RID: 14602 RVA: 0x00121BC4 File Offset: 0x0011FFC4
		public void OnClickExecuteButton(int itemID, int num)
		{
			this.OnClickExecuteSaleButton.Call(itemID, num);
		}

		// Token: 0x0600390B RID: 14603 RVA: 0x00121BD3 File Offset: 0x0011FFD3
		public void OnCancelSaleButtonCallBack()
		{
			this.CloseConfirm();
		}

		// Token: 0x0600390C RID: 14604 RVA: 0x00121BDB File Offset: 0x0011FFDB
		public void OnBackConfirmCallBack(bool shortcut)
		{
			if (shortcut)
			{
				this.m_ConfirmGroup.Close();
				this.m_SavedBackButtonCallBack.Call(shortcut);
			}
			else
			{
				this.CloseConfirm();
			}
		}

		// Token: 0x0600390D RID: 14605 RVA: 0x00121C08 File Offset: 0x00120008
		public void CloseConfirm()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfo);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
			this.m_MainGroup.Open();
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x04003FF5 RID: 16373
		private ShopTradeSaleListUI.eStep m_Step;

		// Token: 0x04003FF6 RID: 16374
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003FF7 RID: 16375
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003FF8 RID: 16376
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04003FF9 RID: 16377
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003FFA RID: 16378
		[SerializeField]
		private ShopTradeSaleConfirmGroup m_ConfirmGroup;

		// Token: 0x04003FFB RID: 16379
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x04003FFC RID: 16380
		private ShopTradeSaleListUI.eTab m_Tab;

		// Token: 0x04003FFD RID: 16381
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x02000AAC RID: 2732
		private enum eStep
		{
			// Token: 0x04004000 RID: 16384
			Hide,
			// Token: 0x04004001 RID: 16385
			In,
			// Token: 0x04004002 RID: 16386
			Idle,
			// Token: 0x04004003 RID: 16387
			Out,
			// Token: 0x04004004 RID: 16388
			End
		}

		// Token: 0x02000AAD RID: 2733
		public enum eTab
		{
			// Token: 0x04004006 RID: 16390
			None = -1,
			// Token: 0x04004007 RID: 16391
			Upgrade,
			// Token: 0x04004008 RID: 16392
			LimitBreak,
			// Token: 0x04004009 RID: 16393
			Evolution,
			// Token: 0x0400400A RID: 16394
			Weapon
		}
	}
}
