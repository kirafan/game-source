﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AB8 RID: 2744
	public class ShopWeaponSaleConfirmGroup : UIGroup
	{
		// Token: 0x140000E6 RID: 230
		// (add) Token: 0x06003945 RID: 14661 RVA: 0x001235AC File Offset: 0x001219AC
		// (remove) Token: 0x06003946 RID: 14662 RVA: 0x001235E4 File Offset: 0x001219E4
		public event Action<List<long>> OnExecute;

		// Token: 0x140000E7 RID: 231
		// (add) Token: 0x06003947 RID: 14663 RVA: 0x0012361C File Offset: 0x00121A1C
		// (remove) Token: 0x06003948 RID: 14664 RVA: 0x00123654 File Offset: 0x00121A54
		public event Action OnCancel;

		// Token: 0x06003949 RID: 14665 RVA: 0x0012368C File Offset: 0x00121A8C
		public void Open(List<long> weaponMngIDs)
		{
			this.m_WeaponMngIDs = weaponMngIDs;
			while (this.m_IconList.Count < weaponMngIDs.Count)
			{
				WeaponIconWithFrame item = UnityEngine.Object.Instantiate<WeaponIconWithFrame>(this.m_WeaponIconPrefab, this.m_IconParent, false);
				this.m_IconList.Add(item);
			}
			int num = 0;
			for (int i = 0; i < this.m_IconList.Count; i++)
			{
				if (i < weaponMngIDs.Count)
				{
					this.m_IconList[i].gameObject.SetActive(true);
					UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(weaponMngIDs[i]);
					WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID);
					this.m_IconList[i].Apply(userWpnData);
					num += param.m_SaleAmount;
				}
				else
				{
					this.m_IconList[i].gameObject.SetActive(false);
				}
			}
			this.m_GetAmountText.text = UIUtility.GoldValueToString(num, true);
			this.Open();
		}

		// Token: 0x0600394A RID: 14666 RVA: 0x001237A4 File Offset: 0x00121BA4
		public void OnClickExecuteButtonCallBack()
		{
			bool flag = false;
			bool flag2 = false;
			for (int i = 0; i < this.m_WeaponMngIDs.Count; i++)
			{
				UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponMngIDs[i]);
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID).m_Rare >= 4)
				{
					flag = true;
				}
				if (userWpnData.Param.Lv > 1)
				{
					flag2 = true;
				}
			}
			if (flag2 && flag)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "ぶき売却の確認", "高レアリティのぶきやレベルが上がっているぶきが選択されています。\n売却してもよろしいでしょうか", null, new Action<int>(this.OnConfirmRareWeapon));
			}
			else if (flag2)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "ぶき売却の確認", "レベルが上がっているぶきが選択されています。\n売却してもよろしいでしょうか", null, new Action<int>(this.OnConfirmRareWeapon));
			}
			else if (flag)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "ぶき売却の確認", "高レアリティのぶきが選択されています。\n売却してもよろしいでしょうか", null, new Action<int>(this.OnConfirmRareWeapon));
			}
			else
			{
				this.OnExecute.Call(this.m_WeaponMngIDs);
			}
		}

		// Token: 0x0600394B RID: 14667 RVA: 0x001238DD File Offset: 0x00121CDD
		private void OnConfirmRareWeapon(int btn)
		{
			if (btn == 0)
			{
				this.OnExecute.Call(this.m_WeaponMngIDs);
			}
		}

		// Token: 0x0600394C RID: 14668 RVA: 0x001238F6 File Offset: 0x00121CF6
		public void OnCancelButtonCallBack()
		{
			this.OnCancel.Call();
		}

		// Token: 0x04004061 RID: 16481
		[SerializeField]
		private RectTransform m_IconParent;

		// Token: 0x04004062 RID: 16482
		[SerializeField]
		private WeaponIconWithFrame m_WeaponIconPrefab;

		// Token: 0x04004063 RID: 16483
		private List<WeaponIconWithFrame> m_IconList = new List<WeaponIconWithFrame>();

		// Token: 0x04004066 RID: 16486
		public Text m_GetAmountText;

		// Token: 0x04004067 RID: 16487
		private List<long> m_WeaponMngIDs;
	}
}
