﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AA8 RID: 2728
	public class ShopTradeSaleConfirmGroup : UIGroup
	{
		// Token: 0x140000DD RID: 221
		// (add) Token: 0x060038EE RID: 14574 RVA: 0x001211CC File Offset: 0x0011F5CC
		// (remove) Token: 0x060038EF RID: 14575 RVA: 0x00121204 File Offset: 0x0011F604
		public event Action<int, int> OnExecuteSaleButton;

		// Token: 0x140000DE RID: 222
		// (add) Token: 0x060038F0 RID: 14576 RVA: 0x0012123C File Offset: 0x0011F63C
		// (remove) Token: 0x060038F1 RID: 14577 RVA: 0x00121274 File Offset: 0x0011F674
		public event Action OnCancelSaleButton;

		// Token: 0x060038F2 RID: 14578 RVA: 0x001212AC File Offset: 0x0011F6AC
		public void Open(int itemID)
		{
			this.m_ItemID = itemID;
			this.m_Data = new ShopTradeSaleListItemData(this.m_ItemID);
			this.m_Item.SetItemData(this.m_Data);
			if (!this.m_IsDonePrepare)
			{
				this.m_NumSelect.OnChangeCurrentNum += this.OnChangeValueCallBack;
				this.m_IsDonePrepare = true;
			}
			this.m_NumSelect.SetCurrentNum(1);
			int num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(this.m_ItemID);
			if (num > 999)
			{
				num = 999;
			}
			this.m_NumSelect.SetRange(1, num);
			this.m_BeforeGold.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			this.OnChangeValueCallBack();
			this.Open();
		}

		// Token: 0x060038F3 RID: 14579 RVA: 0x0012137C File Offset: 0x0011F77C
		public void OnClickYesButtonCallBack()
		{
			this.OnExecuteSaleButton.Call(this.m_ItemID, this.m_NumSelect.GetCurrentNum());
		}

		// Token: 0x060038F4 RID: 14580 RVA: 0x0012139A File Offset: 0x0011F79A
		public void OnClickNoButtonCallBack()
		{
			this.OnCancelSaleButton.Call();
		}

		// Token: 0x060038F5 RID: 14581 RVA: 0x001213A8 File Offset: 0x0011F7A8
		private void OnChangeValueCallBack()
		{
			this.m_AfterGold.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold + (long)(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(this.m_ItemID).m_SaleAmount * this.m_NumSelect.GetCurrentNum()), true);
			this.m_Item.ChangeHaveNum(this.m_NumSelect.GetCurrentNum());
		}

		// Token: 0x04003FE3 RID: 16355
		[SerializeField]
		private NumberSelect m_NumSelect;

		// Token: 0x04003FE4 RID: 16356
		[SerializeField]
		private ShopTradeSaleListItem m_Item;

		// Token: 0x04003FE5 RID: 16357
		[SerializeField]
		private Text m_BeforeGold;

		// Token: 0x04003FE6 RID: 16358
		[SerializeField]
		private Text m_AfterGold;

		// Token: 0x04003FE9 RID: 16361
		private int m_ItemID = -1;

		// Token: 0x04003FEA RID: 16362
		private ShopTradeSaleListItemData m_Data;

		// Token: 0x04003FEB RID: 16363
		private bool m_IsDonePrepare;
	}
}
