﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000ABB RID: 2747
	public class ShopWeaponTopUI : MenuUIBase
	{
		// Token: 0x1700035A RID: 858
		// (get) Token: 0x0600395A RID: 14682 RVA: 0x00123CE8 File Offset: 0x001220E8
		public ShopWeaponTopUI.eMainButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x0600395B RID: 14683 RVA: 0x00123CF0 File Offset: 0x001220F0
		public eClassType SelectClassType
		{
			get
			{
				return this.m_SelectClassType;
			}
		}

		// Token: 0x140000E9 RID: 233
		// (add) Token: 0x0600395C RID: 14684 RVA: 0x00123CF8 File Offset: 0x001220F8
		// (remove) Token: 0x0600395D RID: 14685 RVA: 0x00123D30 File Offset: 0x00122130
		public event Action OnClickMainButton;

		// Token: 0x0600395E RID: 14686 RVA: 0x00123D66 File Offset: 0x00122166
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
		}

		// Token: 0x0600395F RID: 14687 RVA: 0x00123DA4 File Offset: 0x001221A4
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopWeaponTopUI.eStep.BGLoad:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.ChangeStep(ShopWeaponTopUI.eStep.In);
				}
				break;
			case ShopWeaponTopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopWeaponTopUI.eStep.Idle);
				}
				break;
			}
			case ShopWeaponTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopWeaponTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003960 RID: 14688 RVA: 0x00123EC4 File Offset: 0x001222C4
		private void ChangeStep(ShopWeaponTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopWeaponTopUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopWeaponTopUI.eStep.BGLoad:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Weapon, true);
				break;
			case ShopWeaponTopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					this.m_AnimUIPlayerArray[i].PlayIn();
				}
				this.m_MainGroup.Open();
				break;
			case ShopWeaponTopUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopWeaponTopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case ShopWeaponTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003961 RID: 14689 RVA: 0x00123FD4 File Offset: 0x001223D4
		public override void PlayIn()
		{
			this.ChangeStep(ShopWeaponTopUI.eStep.BGLoad);
		}

		// Token: 0x06003962 RID: 14690 RVA: 0x00123FE0 File Offset: 0x001223E0
		public override void PlayOut()
		{
			this.ChangeStep(ShopWeaponTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003963 RID: 14691 RVA: 0x0012401A File Offset: 0x0012241A
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopWeaponTopUI.eStep.End;
		}

		// Token: 0x06003964 RID: 14692 RVA: 0x00124025 File Offset: 0x00122425
		public void OnClickMainButtonCallBack(int btn)
		{
			this.m_SelectButton = (ShopWeaponTopUI.eMainButton)btn;
			this.OnClickMainButton.Call();
		}

		// Token: 0x04004075 RID: 16501
		private ShopWeaponTopUI.eStep m_Step;

		// Token: 0x04004076 RID: 16502
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04004077 RID: 16503
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004078 RID: 16504
		private ShopWeaponTopUI.eMainButton m_SelectButton = ShopWeaponTopUI.eMainButton.None;

		// Token: 0x04004079 RID: 16505
		private eClassType m_SelectClassType = eClassType.None;

		// Token: 0x0400407A RID: 16506
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x02000ABC RID: 2748
		private enum eStep
		{
			// Token: 0x0400407D RID: 16509
			Hide,
			// Token: 0x0400407E RID: 16510
			BGLoad,
			// Token: 0x0400407F RID: 16511
			In,
			// Token: 0x04004080 RID: 16512
			Idle,
			// Token: 0x04004081 RID: 16513
			Out,
			// Token: 0x04004082 RID: 16514
			End
		}

		// Token: 0x02000ABD RID: 2749
		public enum eMainButton
		{
			// Token: 0x04004084 RID: 16516
			None = -1,
			// Token: 0x04004085 RID: 16517
			Create,
			// Token: 0x04004086 RID: 16518
			Upgrade,
			// Token: 0x04004087 RID: 16519
			Sale,
			// Token: 0x04004088 RID: 16520
			LimitExtend
		}
	}
}
