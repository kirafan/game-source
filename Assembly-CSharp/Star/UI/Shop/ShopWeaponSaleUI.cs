﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AB9 RID: 2745
	public class ShopWeaponSaleUI : MenuUIBase
	{
		// Token: 0x140000E8 RID: 232
		// (add) Token: 0x0600394E RID: 14670 RVA: 0x00123914 File Offset: 0x00121D14
		// (remove) Token: 0x0600394F RID: 14671 RVA: 0x0012394C File Offset: 0x00121D4C
		public event Action<List<long>> OnClickExecuteButton;

		// Token: 0x06003950 RID: 14672 RVA: 0x00123984 File Offset: 0x00121D84
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_ConfirmGroup.OnExecute += this.OnClickExecuteButtonCallBack;
			this.m_ConfirmGroup.OnCancel += this.CloseConfirm;
		}

		// Token: 0x06003951 RID: 14673 RVA: 0x001239F9 File Offset: 0x00121DF9
		public void CloseConfirm()
		{
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x06003952 RID: 14674 RVA: 0x00123A08 File Offset: 0x00121E08
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopWeaponSaleUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(ShopWeaponSaleUI.eStep.Idle);
				}
				break;
			}
			case ShopWeaponSaleUI.eStep.Idle:
				if (this.m_ConfirmGroup.IsHideOrOut())
				{
					this.PlayOut();
				}
				break;
			case ShopWeaponSaleUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(ShopWeaponSaleUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003953 RID: 14675 RVA: 0x00123B0C File Offset: 0x00121F0C
		private void ChangeStep(ShopWeaponSaleUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopWeaponSaleUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopWeaponSaleUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Weapon, true);
				break;
			case ShopWeaponSaleUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopWeaponSaleUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				break;
			case ShopWeaponSaleUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003954 RID: 14676 RVA: 0x00123BE0 File Offset: 0x00121FE0
		public void PlayIn(List<long> list)
		{
			this.ChangeStep(ShopWeaponSaleUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_ConfirmGroup.Open(list);
		}

		// Token: 0x06003955 RID: 14677 RVA: 0x00123C28 File Offset: 0x00122028
		public override void PlayIn()
		{
			this.ChangeStep(ShopWeaponSaleUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003956 RID: 14678 RVA: 0x00123C64 File Offset: 0x00122064
		public override void PlayOut()
		{
			this.ChangeStep(ShopWeaponSaleUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			if (this.m_ConfirmGroup.IsOpen())
			{
				this.m_ConfirmGroup.Close();
			}
		}

		// Token: 0x06003957 RID: 14679 RVA: 0x00123CB9 File Offset: 0x001220B9
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopWeaponSaleUI.eStep.End;
		}

		// Token: 0x06003958 RID: 14680 RVA: 0x00123CC4 File Offset: 0x001220C4
		public void OnClickExecuteButtonCallBack(List<long> mngIDs)
		{
			this.OnClickExecuteButton.Call(mngIDs);
		}

		// Token: 0x04004068 RID: 16488
		public int MAX_SELECT = 20;

		// Token: 0x04004069 RID: 16489
		private ShopWeaponSaleUI.eStep m_Step;

		// Token: 0x0400406A RID: 16490
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400406B RID: 16491
		[SerializeField]
		private ShopWeaponSaleConfirmGroup m_ConfirmGroup;

		// Token: 0x0400406C RID: 16492
		[SerializeField]
		private Text m_SaleAmountText;

		// Token: 0x0400406D RID: 16493
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x02000ABA RID: 2746
		private enum eStep
		{
			// Token: 0x04004070 RID: 16496
			Hide,
			// Token: 0x04004071 RID: 16497
			In,
			// Token: 0x04004072 RID: 16498
			Idle,
			// Token: 0x04004073 RID: 16499
			Out,
			// Token: 0x04004074 RID: 16500
			End
		}
	}
}
