﻿using System;
using Star.UI.BackGround;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000A9E RID: 2718
	public class ShopTopUI : MenuUIBase
	{
		// Token: 0x17000357 RID: 855
		// (get) Token: 0x060038BD RID: 14525 RVA: 0x0011F8C8 File Offset: 0x0011DCC8
		public ShopTopUI.eMainButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000D9 RID: 217
		// (add) Token: 0x060038BE RID: 14526 RVA: 0x0011F8D0 File Offset: 0x0011DCD0
		// (remove) Token: 0x060038BF RID: 14527 RVA: 0x0011F908 File Offset: 0x0011DD08
		public event Action OnClickMainButton;

		// Token: 0x060038C0 RID: 14528 RVA: 0x0011F93E File Offset: 0x0011DD3E
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
		}

		// Token: 0x060038C1 RID: 14529 RVA: 0x0011F96C File Offset: 0x0011DD6C
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopTopUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopTopUI.eStep.Idle);
				}
				break;
			}
			case ShopTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060038C2 RID: 14530 RVA: 0x0011FA7C File Offset: 0x0011DE7C
		private void ChangeStep(ShopTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopTopUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopTopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Shop, true);
				this.m_MainGroup.Open();
				break;
			case ShopTopUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopTopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case ShopTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060038C3 RID: 14531 RVA: 0x0011FB4C File Offset: 0x0011DF4C
		public override void PlayIn()
		{
			this.ChangeStep(ShopTopUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x060038C4 RID: 14532 RVA: 0x0011FB88 File Offset: 0x0011DF88
		public override void PlayOut()
		{
			this.ChangeStep(ShopTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x060038C5 RID: 14533 RVA: 0x0011FBC2 File Offset: 0x0011DFC2
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopTopUI.eStep.End;
		}

		// Token: 0x060038C6 RID: 14534 RVA: 0x0011FBCD File Offset: 0x0011DFCD
		public void OnClickMainButtonCallBack(int btn)
		{
			this.m_SelectButton = (ShopTopUI.eMainButton)btn;
			this.OnClickMainButton.Call();
		}

		// Token: 0x04003F9A RID: 16282
		private ShopTopUI.eStep m_Step;

		// Token: 0x04003F9B RID: 16283
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003F9C RID: 16284
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003F9D RID: 16285
		private ShopTopUI.eMainButton m_SelectButton = ShopTopUI.eMainButton.None;

		// Token: 0x02000A9F RID: 2719
		private enum eStep
		{
			// Token: 0x04003FA0 RID: 16288
			Hide,
			// Token: 0x04003FA1 RID: 16289
			In,
			// Token: 0x04003FA2 RID: 16290
			Idle,
			// Token: 0x04003FA3 RID: 16291
			Out,
			// Token: 0x04003FA4 RID: 16292
			End
		}

		// Token: 0x02000AA0 RID: 2720
		public enum eMainButton
		{
			// Token: 0x04003FA6 RID: 16294
			None = -1,
			// Token: 0x04003FA7 RID: 16295
			Trade,
			// Token: 0x04003FA8 RID: 16296
			Weapon,
			// Token: 0x04003FA9 RID: 16297
			Build
		}
	}
}
