﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AA1 RID: 2721
	public class ShopTradeConfirmGroup : UIGroup
	{
		// Token: 0x140000DA RID: 218
		// (add) Token: 0x060038C8 RID: 14536 RVA: 0x0011FBEC File Offset: 0x0011DFEC
		// (remove) Token: 0x060038C9 RID: 14537 RVA: 0x0011FC24 File Offset: 0x0011E024
		public event Action<int, int> OnClickExecute;

		// Token: 0x060038CA RID: 14538 RVA: 0x0011FC5A File Offset: 0x0011E05A
		public ShopManager.TradeData GetCurrentTradeData()
		{
			return this.m_TradeData;
		}

		// Token: 0x060038CB RID: 14539 RVA: 0x0011FC62 File Offset: 0x0011E062
		public void Open(ShopManager.TradeData tradeData)
		{
			this.Apply(tradeData);
			this.Open();
		}

		// Token: 0x060038CC RID: 14540 RVA: 0x0011FC71 File Offset: 0x0011E071
		public void Refresh()
		{
			this.Apply(this.m_TradeData);
		}

		// Token: 0x060038CD RID: 14541 RVA: 0x0011FC80 File Offset: 0x0011E080
		private void Apply(ShopManager.TradeData tradeData)
		{
			this.m_TradeData = tradeData;
			this.m_LimitText.text = this.m_TradeData.RemainNum.ToString();
			this.m_DeadLineText.text = UIUtility.GetTradeLimitSpanString(this.m_TradeData.DeadLineSpan);
			this.m_ReasonTrade = ShopTradeConfirmGroup.eReasonDisable.None;
			ePresentType dstType = this.m_TradeData.m_DstType;
			if (dstType != ePresentType.Item)
			{
				if (dstType != ePresentType.Chara)
				{
					if (dstType == ePresentType.Weapon)
					{
						this.ApplyWeapon(this.m_TradeData.m_DstID);
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit <= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas.Count)
						{
							this.m_ReasonTrade = ShopTradeConfirmGroup.eReasonDisable.WeaponLimit;
						}
						int num = 0;
						for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas.Count; i++)
						{
							if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas[i].Param.WeaponID == this.m_TradeData.m_DstID)
							{
								num++;
							}
						}
						this.m_WeaponHaveNumText.text = num.ToString();
					}
				}
				else
				{
					this.ApplyChara(this.m_TradeData.m_DstID);
				}
			}
			else
			{
				this.ApplyItem(this.m_TradeData.m_DstID, this.m_TradeData.m_DstAmount);
				this.m_ItemHaveNumText.text = UIUtility.ItemHaveNumToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(this.m_TradeData.m_DstID), true);
			}
			for (int j = 0; j < this.m_TradeData.m_SrcTypes.Length; j++)
			{
				this.m_Srcs[j].Apply(this.m_TradeData.m_SrcTypes[j], this.m_TradeData.m_SrcIDs[j], this.m_TradeData.m_SrcAmounts[j], this.m_ReasonTrade == ShopTradeConfirmGroup.eReasonDisable.None);
			}
		}

		// Token: 0x060038CE RID: 14542 RVA: 0x0011FE84 File Offset: 0x0011E284
		private void ApplyItem(int itemID, int num)
		{
			this.m_ItemDetailDisp.gameObject.SetActive(true);
			this.m_CharaDetailDisp.gameObject.SetActive(false);
			this.m_WeaponInfo.gameObject.SetActive(false);
			this.m_ItemDetailDisp.ApplyItem(itemID, num);
		}

		// Token: 0x060038CF RID: 14543 RVA: 0x0011FED4 File Offset: 0x0011E2D4
		private void ApplyChara(int charaID)
		{
			this.m_ItemDetailDisp.gameObject.SetActive(false);
			this.m_CharaDetailDisp.gameObject.SetActive(true);
			this.m_WeaponInfo.gameObject.SetActive(false);
			this.m_CharaDetailDisp.Apply(charaID);
		}

		// Token: 0x060038D0 RID: 14544 RVA: 0x0011FF20 File Offset: 0x0011E320
		private void ApplyWeapon(int weaponID)
		{
			this.m_ItemDetailDisp.gameObject.SetActive(false);
			this.m_CharaDetailDisp.gameObject.SetActive(false);
			this.m_WeaponInfo.gameObject.SetActive(true);
			this.m_WeaponInfo.Apply(weaponID);
		}

		// Token: 0x060038D1 RID: 14545 RVA: 0x0011FF6C File Offset: 0x0011E36C
		public void OnClickExecuteButtonCallBack(int idx)
		{
			this.m_SelectIdx = idx;
			if (this.m_Srcs[this.m_SelectIdx].IsEnoughSrc)
			{
				ShopTradeConfirmGroup.eReasonDisable reasonTrade = this.m_ReasonTrade;
				if (reasonTrade != ShopTradeConfirmGroup.eReasonDisable.None)
				{
					if (reasonTrade == ShopTradeConfirmGroup.eReasonDisable.WeaponLimit)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.ShopTradeTitle, eText_MessageDB.WeaponCreateLimitWeapon, null);
					}
				}
				else
				{
					this.m_SelectIdx = idx;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTradeTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTrade, new object[]
					{
						this.m_Srcs[this.m_SelectIdx].GetName()
					}), null, new Action<int>(this.OnConfirmExec));
				}
			}
		}

		// Token: 0x060038D2 RID: 14546 RVA: 0x0012003C File Offset: 0x0011E43C
		private void OnConfirmExec(int btn)
		{
			if (btn == 0)
			{
				if (this.m_TradeData.m_DstType == ePresentType.Chara)
				{
					for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas.Count; i++)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[i].Param.CharaID == this.m_TradeData.m_DstID)
						{
							this.ConfirmConvert();
							return;
						}
					}
				}
				this.OnClickExecute.Call(this.m_TradeData.m_ID, this.m_SelectIdx);
			}
		}

		// Token: 0x060038D3 RID: 14547 RVA: 0x001200D8 File Offset: 0x0011E4D8
		private void ConfirmConvert()
		{
			string name = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_TradeData.m_DstID).m_Name;
			string text = null;
			ePresentType altType = this.m_TradeData.m_AltType;
			if (altType != ePresentType.Item)
			{
				if (altType == ePresentType.Weapon)
				{
					text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.m_TradeData.m_AltID).m_WeaponName;
				}
			}
			else
			{
				text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(this.m_TradeData.m_AltID).m_Name;
			}
			if (this.m_TradeData.m_AltAmount > 1)
			{
				text = text + "x" + this.m_TradeData.m_AltAmount;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTradeTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.ShopTradeConvert, new object[]
			{
				name,
				text
			}), null, new Action<int>(this.OnConfirmConvert));
		}

		// Token: 0x060038D4 RID: 14548 RVA: 0x0012020A File Offset: 0x0011E60A
		private void OnConfirmConvert(int btn)
		{
			if (btn == 0)
			{
				this.OnClickExecute.Call(this.m_TradeData.m_ID, this.m_SelectIdx);
			}
		}

		// Token: 0x04003FAA RID: 16298
		[SerializeField]
		private ItemDetailDisplay m_ItemDetailDisp;

		// Token: 0x04003FAB RID: 16299
		[SerializeField]
		private CharaDetailDisplay m_CharaDetailDisp;

		// Token: 0x04003FAC RID: 16300
		[SerializeField]
		private WeaponInfo m_WeaponInfo;

		// Token: 0x04003FAD RID: 16301
		[SerializeField]
		private ShopTradeSrc[] m_Srcs;

		// Token: 0x04003FAE RID: 16302
		[SerializeField]
		private Text m_LimitText;

		// Token: 0x04003FAF RID: 16303
		[SerializeField]
		private Text m_DeadLineText;

		// Token: 0x04003FB0 RID: 16304
		[SerializeField]
		private Text m_WeaponHaveNumText;

		// Token: 0x04003FB1 RID: 16305
		[SerializeField]
		private Text m_ItemHaveNumText;

		// Token: 0x04003FB2 RID: 16306
		private ShopManager.TradeData m_TradeData;

		// Token: 0x04003FB3 RID: 16307
		private int m_SelectIdx;

		// Token: 0x04003FB4 RID: 16308
		private ShopTradeConfirmGroup.eReasonDisable m_ReasonTrade;

		// Token: 0x02000AA2 RID: 2722
		private enum eReasonDisable
		{
			// Token: 0x04003FB7 RID: 16311
			None,
			// Token: 0x04003FB8 RID: 16312
			WeaponLimit
		}
	}
}
