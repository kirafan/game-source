﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AB2 RID: 2738
	public class ShopWeaponCreateConfirmGroup : UIGroup
	{
		// Token: 0x140000E2 RID: 226
		// (add) Token: 0x0600391F RID: 14623 RVA: 0x001223C0 File Offset: 0x001207C0
		// (remove) Token: 0x06003920 RID: 14624 RVA: 0x001223F8 File Offset: 0x001207F8
		public event Action OnCancel;

		// Token: 0x140000E3 RID: 227
		// (add) Token: 0x06003921 RID: 14625 RVA: 0x00122430 File Offset: 0x00120830
		// (remove) Token: 0x06003922 RID: 14626 RVA: 0x00122468 File Offset: 0x00120868
		public event Action OnClickExecute;

		// Token: 0x06003923 RID: 14627 RVA: 0x0012249E File Offset: 0x0012089E
		public void Setup()
		{
			this.m_RecipeMaterials.Setup();
			this.m_MixEffectScene.Prepare(1);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Mix);
		}

		// Token: 0x06003924 RID: 14628 RVA: 0x001224C9 File Offset: 0x001208C9
		public void Destroy()
		{
			this.m_MixEffectScene.Destroy();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Mix);
		}

		// Token: 0x06003925 RID: 14629 RVA: 0x001224E8 File Offset: 0x001208E8
		public void Open(int recipeID)
		{
			this.m_RecipeID = recipeID;
			this.Refresh();
			this.Open();
		}

		// Token: 0x06003926 RID: 14630 RVA: 0x00122500 File Offset: 0x00120900
		private void Refresh()
		{
			this.m_RecipeMaterials.SetEnableRenderItemIcon(true);
			WeaponRecipeListDB_Param paramWithRecipeID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.GetParamWithRecipeID(this.m_RecipeID);
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(paramWithRecipeID.m_WeaponID);
			int maxLv = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetMaxLv(param.m_SkillExpTableID);
			int nextExp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetNextExp(1, param.m_SkillExpTableID);
			this.m_NameText.text = param.m_WeaponName;
			this.m_ClassIcon.Apply((eClassType)param.m_ClassType);
			this.m_ClassText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WeaponEquipClass, new object[]
			{
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + param.m_ClassType)
			});
			this.m_Rare.Apply((eRare)param.m_Rare);
			this.m_WeaponIcon.Apply(param.m_ID);
			this.m_SkillInfo.Apply(SkillIcon.eSkillDBType.Weapon, param.m_SkillID, eElementType.None, 1, maxLv, (long)nextExp);
			this.m_CostText.text = param.m_Cost.ToString();
			this.m_HaveNumText.text = EditUtility.CalcWeaponNum(param.m_ID).ToString();
			this.m_RecipeMaterials.Apply(paramWithRecipeID.m_ItemIDs, paramWithRecipeID.m_ItemNums);
			this.m_AtkText.text = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, 1).ToString().PadLeft(6);
			this.m_MgcText.text = EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, 1).ToString().PadLeft(6);
			this.m_DefText.text = EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, 1).ToString().PadLeft(6);
			this.m_MDefText.text = EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, 1).ToString().PadLeft(6);
			this.m_IsShortOfGold = false;
			this.m_IsShortOfMaterial = false;
			this.m_IsLimitWeapon = false;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas.Count >= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit)
			{
				this.m_IsLimitWeapon = true;
			}
			for (int i = 0; i < paramWithRecipeID.m_ItemIDs.Length; i++)
			{
				if (paramWithRecipeID.m_ItemIDs[i] != -1)
				{
					this.m_UseMaterialIcons[i].gameObject.SetActive(false);
					this.m_UseMaterialIcons[i].SetStartPosition();
					this.m_UseMaterialIcons[i].Apply(paramWithRecipeID.m_ItemIDs[i]);
					this.m_UseMaterialIcons[i].SetEnableRender(false);
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(paramWithRecipeID.m_ItemIDs[i]) < paramWithRecipeID.m_ItemNums[i])
					{
						this.m_IsShortOfMaterial = true;
					}
				}
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)paramWithRecipeID.m_Amount))
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(paramWithRecipeID.m_Amount);
				this.m_GoldAmount.text = UIUtility.GetNotEnoughString(stringBuilder).ToString();
				this.m_IsShortOfGold = true;
			}
			else
			{
				this.m_GoldAmount.text = UIUtility.GoldValueToString(paramWithRecipeID.m_Amount, true);
			}
			this.m_ExecButton.Interactable = this.IsEnableCreate();
		}

		// Token: 0x06003927 RID: 14631 RVA: 0x001228F0 File Offset: 0x00120CF0
		private bool IsEnableCreate()
		{
			return !this.m_IsShortOfGold && !this.m_IsShortOfMaterial && !this.m_IsLimitWeapon;
		}

		// Token: 0x06003928 RID: 14632 RVA: 0x00122914 File Offset: 0x00120D14
		public void OnClickExecuteButtonCallBack()
		{
			if (this.IsEnableCreate())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreate), null, new Action<int>(this.OnConfirmExec));
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (this.m_IsShortOfGold)
				{
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateShortOfGold));
				}
				if (this.m_IsShortOfMaterial)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append("\n");
					}
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateShortOfMaterial));
				}
				if (this.m_IsLimitWeapon)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append("\n");
					}
					stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateLimitWeapon));
				}
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateDisable), stringBuilder.ToString(), null);
			}
		}

		// Token: 0x06003929 RID: 14633 RVA: 0x00122A5D File Offset: 0x00120E5D
		public void OnConfirmExec(int btn)
		{
			if (btn == 0)
			{
				this.OnClickExecute.Call();
			}
		}

		// Token: 0x0600392A RID: 14634 RVA: 0x00122A70 File Offset: 0x00120E70
		public void StartEffect()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_212", true);
			base.SetEnableInput(false);
			this.m_RecipeMaterials.SetEnableRenderItemIcon(false);
			int[] itemIDs = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.GetParamWithRecipeID(this.m_RecipeID).m_ItemIDs;
			int num = 0;
			for (int i = 0; i < itemIDs.Length; i++)
			{
				if (itemIDs[i] != -1)
				{
					num++;
				}
			}
			GameObject gameObject = base.gameObject;
			for (int j = 0; j < itemIDs.Length; j++)
			{
				if (itemIDs[j] != -1)
				{
					this.m_UseMaterialIcons[j].SetStartPosition();
					this.m_UseMaterialIcons[j].Apply(itemIDs[j]);
					this.m_UseMaterialIcons[j].SetEnableRender(true);
					this.m_UseMaterialIcons[j].MoveTo((float)j * 0.1f);
					num--;
					if (num <= 0)
					{
						break;
					}
				}
			}
			this.m_Step = ShopWeaponCreateConfirmGroup.eEffectStep.Move;
		}

		// Token: 0x0600392B RID: 14635 RVA: 0x00122B7C File Offset: 0x00120F7C
		public override void LateUpdate()
		{
			base.LateUpdate();
			switch (this.m_Step)
			{
			case ShopWeaponCreateConfirmGroup.eEffectStep.WaitPrepare:
				if (this.m_MixEffectScene.IsCompletePrepare())
				{
					this.m_Step = ShopWeaponCreateConfirmGroup.eEffectStep.Move;
				}
				break;
			case ShopWeaponCreateConfirmGroup.eEffectStep.Move:
			{
				bool flag = true;
				for (int i = 0; i < this.m_UseMaterialIcons.Length; i++)
				{
					if (this.m_UseMaterialIcons[i].IsPlaying)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.m_Step = ShopWeaponCreateConfirmGroup.eEffectStep.FinalEffect;
				}
				break;
			}
			case ShopWeaponCreateConfirmGroup.eEffectStep.FinalEffect:
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.MIX_WEAPON, 1f, 0, -1, -1);
				this.m_MixEffectScene.Play();
				this.m_Step = ShopWeaponCreateConfirmGroup.eEffectStep.WaitFinalEffect;
				break;
			case ShopWeaponCreateConfirmGroup.eEffectStep.WaitFinalEffect:
				if (this.m_MixEffectScene.IsCompletePlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponCreateResult), null, new Action<int>(this.OnFinish));
					this.m_Step = ShopWeaponCreateConfirmGroup.eEffectStep.None;
				}
				break;
			}
		}

		// Token: 0x0600392C RID: 14636 RVA: 0x00122CAA File Offset: 0x001210AA
		private void OnFinish(int btn)
		{
			this.Refresh();
			base.SetEnableInput(true);
		}

		// Token: 0x04004025 RID: 16421
		private const float MOVESPAN = 0.1f;

		// Token: 0x04004026 RID: 16422
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004027 RID: 16423
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04004028 RID: 16424
		[SerializeField]
		private Text m_ClassText;

		// Token: 0x04004029 RID: 16425
		[SerializeField]
		private RareStar m_Rare;

		// Token: 0x0400402A RID: 16426
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x0400402B RID: 16427
		[SerializeField]
		private Text m_CostText;

		// Token: 0x0400402C RID: 16428
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x0400402D RID: 16429
		[SerializeField]
		private RecipeMaterials m_RecipeMaterials;

		// Token: 0x0400402E RID: 16430
		[SerializeField]
		private WeaponMaterialIcon[] m_UseMaterialIcons;

		// Token: 0x0400402F RID: 16431
		[SerializeField]
		private Text m_AtkText;

		// Token: 0x04004030 RID: 16432
		[SerializeField]
		private Text m_MgcText;

		// Token: 0x04004031 RID: 16433
		[SerializeField]
		private Text m_DefText;

		// Token: 0x04004032 RID: 16434
		[SerializeField]
		private Text m_MDefText;

		// Token: 0x04004033 RID: 16435
		[SerializeField]
		private SkillInfo m_SkillInfo;

		// Token: 0x04004034 RID: 16436
		[SerializeField]
		private Text m_GoldAmount;

		// Token: 0x04004035 RID: 16437
		[SerializeField]
		private CustomButton m_ExecButton;

		// Token: 0x04004036 RID: 16438
		[SerializeField]
		private MixWpnCreateEffectScene m_MixEffectScene;

		// Token: 0x04004037 RID: 16439
		private int m_RecipeID = -1;

		// Token: 0x04004038 RID: 16440
		private bool m_IsShortOfGold;

		// Token: 0x04004039 RID: 16441
		private bool m_IsShortOfMaterial;

		// Token: 0x0400403A RID: 16442
		private bool m_IsLimitWeapon;

		// Token: 0x0400403B RID: 16443
		private ShopWeaponCreateConfirmGroup.eEffectStep m_Step;

		// Token: 0x02000AB3 RID: 2739
		private enum eEffectStep
		{
			// Token: 0x0400403F RID: 16447
			None,
			// Token: 0x04004040 RID: 16448
			WaitPrepare,
			// Token: 0x04004041 RID: 16449
			Move,
			// Token: 0x04004042 RID: 16450
			FinalEffect,
			// Token: 0x04004043 RID: 16451
			WaitFinalEffect
		}
	}
}
