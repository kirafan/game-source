﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000AC0 RID: 2752
	public class ShopWeaponUpgradeUI : MenuUIBase
	{
		// Token: 0x140000EB RID: 235
		// (add) Token: 0x06003975 RID: 14709 RVA: 0x00124BC0 File Offset: 0x00122FC0
		// (remove) Token: 0x06003976 RID: 14710 RVA: 0x00124BF8 File Offset: 0x00122FF8
		public event Action<long, List<int>, List<int>> OnClickExecuteButton;

		// Token: 0x06003977 RID: 14711 RVA: 0x00124C30 File Offset: 0x00123030
		public void Setup(long mngID)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_MngID = mngID;
			this.m_ConfirmGroup.OnExecute += this.OnClickExecuteButtonCallBack;
			this.m_ConfirmGroup.Setup();
		}

		// Token: 0x06003978 RID: 14712 RVA: 0x00124CA0 File Offset: 0x001230A0
		public override void Destroy()
		{
			this.m_ConfirmGroup.Destroy();
		}

		// Token: 0x06003979 RID: 14713 RVA: 0x00124CB0 File Offset: 0x001230B0
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopWeaponUpgradeUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_ConfirmGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopWeaponUpgradeUI.eStep.Idle);
				}
				break;
			}
			case ShopWeaponUpgradeUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_ConfirmGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopWeaponUpgradeUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600397A RID: 14714 RVA: 0x00124DC0 File Offset: 0x001231C0
		private void ChangeStep(ShopWeaponUpgradeUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopWeaponUpgradeUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopWeaponUpgradeUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Weapon, true);
				this.m_ConfirmGroup.Open(this.m_MngID);
				break;
			case ShopWeaponUpgradeUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopWeaponUpgradeUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				break;
			case ShopWeaponUpgradeUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600397B RID: 14715 RVA: 0x00124EC0 File Offset: 0x001232C0
		public override void PlayIn()
		{
			this.ChangeStep(ShopWeaponUpgradeUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x0600397C RID: 14716 RVA: 0x00124EFC File Offset: 0x001232FC
		public override void PlayOut()
		{
			this.ChangeStep(ShopWeaponUpgradeUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x0600397D RID: 14717 RVA: 0x00124F41 File Offset: 0x00123341
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopWeaponUpgradeUI.eStep.End;
		}

		// Token: 0x0600397E RID: 14718 RVA: 0x00124F4C File Offset: 0x0012334C
		private void OnClickExecuteButtonCallBack(long weaponMngID, List<int> itemIDs, List<int> amounts)
		{
			this.OnClickExecuteButton.Call(weaponMngID, itemIDs, amounts);
		}

		// Token: 0x0600397F RID: 14719 RVA: 0x00124F5C File Offset: 0x0012335C
		public void StartUpgradeEffect(int successLv)
		{
			this.m_ConfirmGroup.StartEffect(successLv);
		}

		// Token: 0x040040A7 RID: 16551
		private ShopWeaponUpgradeUI.eStep m_Step;

		// Token: 0x040040A8 RID: 16552
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040040A9 RID: 16553
		[SerializeField]
		private ShopWeaponUpgradeConfirmGroup m_ConfirmGroup;

		// Token: 0x040040AA RID: 16554
		[SerializeField]
		private long m_MngID;

		// Token: 0x02000AC1 RID: 2753
		private enum eStep
		{
			// Token: 0x040040AD RID: 16557
			Hide,
			// Token: 0x040040AE RID: 16558
			In,
			// Token: 0x040040AF RID: 16559
			Idle,
			// Token: 0x040040B0 RID: 16560
			Out,
			// Token: 0x040040B1 RID: 16561
			End
		}
	}
}
