﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000AB4 RID: 2740
	public class ShopWeaponCreateUI : MenuUIBase
	{
		// Token: 0x140000E4 RID: 228
		// (add) Token: 0x0600392E RID: 14638 RVA: 0x00122CC8 File Offset: 0x001210C8
		// (remove) Token: 0x0600392F RID: 14639 RVA: 0x00122D00 File Offset: 0x00121100
		public event Action<int> OnClickExecuteButton;

		// Token: 0x06003930 RID: 14640 RVA: 0x00122D38 File Offset: 0x00121138
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_Scroll.Setup();
			this.m_ConfirmGroup.Setup();
			this.m_ConfirmGroup.OnClickExecute += this.OnClickExecuteButtonCallBack;
			this.m_ConfirmGroup.OnCancel += this.CloseConfirm;
			string[] array = new string[5];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + i);
			}
			this.m_TabGroup.Setup(array, -1f);
			this.ChangeTab(0);
			this.m_TabGroup.OnChangeIdx += this.ChangeTab;
		}

		// Token: 0x06003931 RID: 14641 RVA: 0x00122E26 File Offset: 0x00121226
		public override void Destroy()
		{
			this.m_ConfirmGroup.Destroy();
		}

		// Token: 0x06003932 RID: 14642 RVA: 0x00122E33 File Offset: 0x00121233
		public void ChangeTab(int idx)
		{
			this.m_CurrentTab = idx;
			this.Listup((eClassType)idx);
		}

		// Token: 0x06003933 RID: 14643 RVA: 0x00122E44 File Offset: 0x00121244
		private void Listup(eClassType classType)
		{
			this.m_Scroll.RemoveAll();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.m_Params.Length; i++)
			{
				WeaponRecipeListDB_Param weaponRecipeListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.m_Params[i];
				if (classType == (eClassType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponRecipeListDB_Param.m_WeaponID).m_ClassType)
				{
					ShopWeaponRecipeItemData shopWeaponRecipeItemData = new ShopWeaponRecipeItemData(weaponRecipeListDB_Param.m_ID);
					shopWeaponRecipeItemData.OnClickRecipe += this.OnClickRecipeCallBack;
					this.m_Scroll.AddItem(shopWeaponRecipeItemData);
				}
			}
		}

		// Token: 0x06003934 RID: 14644 RVA: 0x00122EFC File Offset: 0x001212FC
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopWeaponCreateUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopWeaponCreateUI.eStep.Idle);
				}
				break;
			}
			case ShopWeaponCreateUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopWeaponCreateUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003935 RID: 14645 RVA: 0x0012300C File Offset: 0x0012140C
		private void ChangeStep(ShopWeaponCreateUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopWeaponCreateUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopWeaponCreateUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Weapon, true);
				this.m_MainGroup.Open();
				break;
			case ShopWeaponCreateUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopWeaponCreateUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				if (this.m_MainGroup.IsOpenOrIn())
				{
					this.m_MainGroup.Close();
				}
				if (this.m_ConfirmGroup.IsOpenOrIn())
				{
					this.m_ConfirmGroup.Close();
				}
				break;
			case ShopWeaponCreateUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003936 RID: 14646 RVA: 0x00123108 File Offset: 0x00121508
		public override void PlayIn()
		{
			this.ChangeStep(ShopWeaponCreateUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003937 RID: 14647 RVA: 0x00123144 File Offset: 0x00121544
		public override void PlayOut()
		{
			this.ChangeStep(ShopWeaponCreateUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003938 RID: 14648 RVA: 0x0012317E File Offset: 0x0012157E
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopWeaponCreateUI.eStep.End;
		}

		// Token: 0x06003939 RID: 14649 RVA: 0x0012318C File Offset: 0x0012158C
		public void OnClickRecipeCallBack(int recipeID)
		{
			this.m_RecipeID = recipeID;
			this.m_MainGroup.Close();
			this.m_ConfirmGroup.Open(recipeID);
			this.m_SceneInfo = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponCreateConfirm);
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnBackConfirmCallBack));
		}

		// Token: 0x0600393A RID: 14650 RVA: 0x001231FF File Offset: 0x001215FF
		public void OnClickExecuteButtonCallBack()
		{
			this.OnClickExecuteButton.Call(this.m_RecipeID);
		}

		// Token: 0x0600393B RID: 14651 RVA: 0x00123212 File Offset: 0x00121612
		public void StartCreateEffect()
		{
			this.m_ConfirmGroup.StartEffect();
		}

		// Token: 0x0600393C RID: 14652 RVA: 0x0012321F File Offset: 0x0012161F
		public void OnBackConfirmCallBack(bool shortcut)
		{
			if (shortcut)
			{
				this.m_SavedBackButtonCallBack.Call(shortcut);
			}
			else
			{
				this.CloseConfirm();
			}
		}

		// Token: 0x0600393D RID: 14653 RVA: 0x00123240 File Offset: 0x00121640
		public void CloseConfirm()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfo);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
			this.ChangeTab(this.m_CurrentTab);
			this.m_MainGroup.Open();
			this.m_ConfirmGroup.Close();
		}

		// Token: 0x04004044 RID: 16452
		private ShopWeaponCreateUI.eStep m_Step;

		// Token: 0x04004045 RID: 16453
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04004046 RID: 16454
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004047 RID: 16455
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04004048 RID: 16456
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04004049 RID: 16457
		[SerializeField]
		private ShopWeaponCreateConfirmGroup m_ConfirmGroup;

		// Token: 0x0400404A RID: 16458
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x0400404B RID: 16459
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x0400404C RID: 16460
		private int m_RecipeID = -1;

		// Token: 0x0400404D RID: 16461
		private int m_CurrentTab;

		// Token: 0x02000AB5 RID: 2741
		private enum eStep
		{
			// Token: 0x04004050 RID: 16464
			Hide,
			// Token: 0x04004051 RID: 16465
			In,
			// Token: 0x04004052 RID: 16466
			Idle,
			// Token: 0x04004053 RID: 16467
			Out,
			// Token: 0x04004054 RID: 16468
			End
		}
	}
}
