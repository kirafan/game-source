﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000A9B RID: 2715
	public class ShopBuildTopUI : MenuUIBase
	{
		// Token: 0x17000356 RID: 854
		// (get) Token: 0x060038B2 RID: 14514 RVA: 0x0011F570 File Offset: 0x0011D970
		public ShopBuildTopUI.eMainButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000D8 RID: 216
		// (add) Token: 0x060038B3 RID: 14515 RVA: 0x0011F578 File Offset: 0x0011D978
		// (remove) Token: 0x060038B4 RID: 14516 RVA: 0x0011F5B0 File Offset: 0x0011D9B0
		public event Action OnClickMainButton;

		// Token: 0x060038B5 RID: 14517 RVA: 0x0011F5E6 File Offset: 0x0011D9E6
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
		}

		// Token: 0x060038B6 RID: 14518 RVA: 0x0011F624 File Offset: 0x0011DA24
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopBuildTopUI.eStep.BGLoad:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.ChangeStep(ShopBuildTopUI.eStep.In);
				}
				break;
			case ShopBuildTopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopBuildTopUI.eStep.Idle);
				}
				break;
			}
			case ShopBuildTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopBuildTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060038B7 RID: 14519 RVA: 0x0011F744 File Offset: 0x0011DB44
		private void ChangeStep(ShopBuildTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopBuildTopUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopBuildTopUI.eStep.BGLoad:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Build, true);
				break;
			case ShopBuildTopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					this.m_AnimUIPlayerArray[i].PlayIn();
				}
				this.m_MainGroup.Open();
				break;
			case ShopBuildTopUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopBuildTopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case ShopBuildTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060038B8 RID: 14520 RVA: 0x0011F854 File Offset: 0x0011DC54
		public override void PlayIn()
		{
			this.ChangeStep(ShopBuildTopUI.eStep.BGLoad);
		}

		// Token: 0x060038B9 RID: 14521 RVA: 0x0011F860 File Offset: 0x0011DC60
		public override void PlayOut()
		{
			this.ChangeStep(ShopBuildTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x060038BA RID: 14522 RVA: 0x0011F89A File Offset: 0x0011DC9A
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopBuildTopUI.eStep.End;
		}

		// Token: 0x060038BB RID: 14523 RVA: 0x0011F8A5 File Offset: 0x0011DCA5
		public void OnClickMainButtonCallBack(int btn)
		{
			this.m_SelectButton = (ShopBuildTopUI.eMainButton)btn;
			this.OnClickMainButton.Call();
		}

		// Token: 0x04003F87 RID: 16263
		private ShopBuildTopUI.eStep m_Step;

		// Token: 0x04003F88 RID: 16264
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003F89 RID: 16265
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003F8A RID: 16266
		private ShopBuildTopUI.eMainButton m_SelectButton = ShopBuildTopUI.eMainButton.None;

		// Token: 0x04003F8B RID: 16267
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x02000A9C RID: 2716
		private enum eStep
		{
			// Token: 0x04003F8E RID: 16270
			Hide,
			// Token: 0x04003F8F RID: 16271
			BGLoad,
			// Token: 0x04003F90 RID: 16272
			In,
			// Token: 0x04003F91 RID: 16273
			Idle,
			// Token: 0x04003F92 RID: 16274
			Out,
			// Token: 0x04003F93 RID: 16275
			End
		}

		// Token: 0x02000A9D RID: 2717
		public enum eMainButton
		{
			// Token: 0x04003F95 RID: 16277
			None = -1,
			// Token: 0x04003F96 RID: 16278
			Buy,
			// Token: 0x04003F97 RID: 16279
			Sale,
			// Token: 0x04003F98 RID: 16280
			LimitExtend,
			// Token: 0x04003F99 RID: 16281
			LimitExtendTown
		}
	}
}
