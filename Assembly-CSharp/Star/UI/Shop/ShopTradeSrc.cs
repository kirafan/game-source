﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AAE RID: 2734
	public class ShopTradeSrc : MonoBehaviour
	{
		// Token: 0x17000358 RID: 856
		// (get) Token: 0x0600390F RID: 14607 RVA: 0x00121C5D File Offset: 0x0012005D
		// (set) Token: 0x06003910 RID: 14608 RVA: 0x00121C65 File Offset: 0x00120065
		public bool IsEnoughSrc { get; set; }

		// Token: 0x06003911 RID: 14609 RVA: 0x00121C70 File Offset: 0x00120070
		public void Apply(ePresentType src, int id, int useNum, bool interactable)
		{
			if (src == ePresentType.None)
			{
				base.gameObject.SetActive(false);
			}
			else
			{
				base.gameObject.SetActive(true);
			}
			bool isEnoughSrc = true;
			this.m_Use = useNum;
			this.m_SrcPresentType = src;
			switch (src)
			{
			case ePresentType.KRRPoint:
			{
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyKirara();
				this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.KiraraPoint);
				this.m_HaveNum.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint().ToString();
				long point = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint();
				this.m_HaveNum.text = UIUtility.KPValueToString(point, true);
				this.m_UseNum.text = UIUtility.KPValueToString(useNum, false);
				if (point < (long)useNum)
				{
					isEnoughSrc = false;
					this.m_UseNum.text = UIUtility.GetNotEnoughString(new StringBuilder(this.m_UseNum.text)).ToString();
				}
				break;
			}
			case ePresentType.Gold:
			{
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyGold();
				this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gold);
				long gold = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold;
				this.m_HaveNum.text = UIUtility.GemValueToString(gold, true);
				this.m_UseNum.text = UIUtility.GoldValueToString(useNum, false);
				if (gold < (long)useNum)
				{
					isEnoughSrc = false;
					this.m_UseNum.text = UIUtility.GetNotEnoughString(new StringBuilder(this.m_UseNum.text)).ToString();
				}
				break;
			}
			case ePresentType.UnlimitedGem:
			case ePresentType.LimitedGem:
			{
				this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyGem();
				this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Gem);
				long gem = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem;
				this.m_HaveNum.text = UIUtility.GemValueToString(gem, true);
				this.m_UseNum.text = UIUtility.GemValueToString(useNum, false);
				if (gem < (long)useNum)
				{
					isEnoughSrc = false;
					this.m_UseNum.text = UIUtility.GetNotEnoughString(new StringBuilder(this.m_UseNum.text)).ToString();
				}
				break;
			}
			default:
				if (src == ePresentType.Item)
				{
					this.m_VariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyItem(id);
					this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(id).m_Name;
					int itemNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(id);
					this.m_HaveNum.text = UIUtility.ItemHaveNumToString(itemNum, true);
					this.m_UseNum.text = useNum.ToString();
					if (itemNum < useNum)
					{
						isEnoughSrc = false;
						this.m_UseNum.text = UIUtility.GetNotEnoughString(new StringBuilder(this.m_UseNum.text)).ToString();
					}
				}
				break;
			}
			this.IsEnoughSrc = isEnoughSrc;
			this.m_ExecButton.Interactable = (this.IsEnoughSrc && interactable);
		}

		// Token: 0x06003912 RID: 14610 RVA: 0x00121FA8 File Offset: 0x001203A8
		public string GetName()
		{
			if (this.m_Use > 1)
			{
				string str = string.Empty;
				switch (this.m_SrcPresentType)
				{
				case ePresentType.KRRPoint:
					str = UIUtility.KPValueToString(this.m_Use, false);
					break;
				case ePresentType.Gold:
					str = UIUtility.GoldValueToString(this.m_Use, false);
					break;
				case ePresentType.UnlimitedGem:
				case ePresentType.LimitedGem:
					str = UIUtility.GemValueToString(this.m_Use, false);
					break;
				default:
					str = this.m_Use.ToString();
					break;
				}
				return this.m_NameText.text + "x" + str;
			}
			return this.m_NameText.text;
		}

		// Token: 0x0400400B RID: 16395
		[SerializeField]
		private PrefabCloner m_VariableIconWithFrameCloner;

		// Token: 0x0400400C RID: 16396
		[SerializeField]
		private Text m_NameText;

		// Token: 0x0400400D RID: 16397
		[SerializeField]
		private Text m_HaveNum;

		// Token: 0x0400400E RID: 16398
		[SerializeField]
		private Text m_UseNum;

		// Token: 0x0400400F RID: 16399
		[SerializeField]
		private CustomButton m_ExecButton;

		// Token: 0x04004010 RID: 16400
		private int m_Use;

		// Token: 0x04004011 RID: 16401
		private ePresentType m_SrcPresentType;
	}
}
