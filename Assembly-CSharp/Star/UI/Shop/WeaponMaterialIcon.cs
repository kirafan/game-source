﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AC2 RID: 2754
	public class WeaponMaterialIcon : MonoBehaviour
	{
		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06003981 RID: 14721 RVA: 0x00124F72 File Offset: 0x00123372
		public bool IsPlaying
		{
			get
			{
				return this.m_IsPlaying;
			}
		}

		// Token: 0x06003982 RID: 14722 RVA: 0x00124F7A File Offset: 0x0012337A
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06003983 RID: 14723 RVA: 0x00124F88 File Offset: 0x00123388
		public void SetStartPosition()
		{
			if (!this.m_IsDonePrepare)
			{
				return;
			}
			this.m_Anim.gameObject.SetActive(false);
			this.m_RectTransform.position = this.m_StartPosition;
			this.m_ItemIcon.GetComponent<RectTransform>().localScale = Vector3.one;
			this.m_IsPlaying = false;
			if (this.m_GameObject.GetComponent<iTween>() != null)
			{
				iTween.Stop(this.m_GameObject);
			}
			if (this.m_GameObject.GetComponent<iTween>() != null)
			{
				iTween.Stop(this.m_ItemIcon.gameObject);
			}
		}

		// Token: 0x06003984 RID: 14724 RVA: 0x00125026 File Offset: 0x00123426
		public void Apply(int itemID)
		{
			this.m_ItemIcon.Apply(itemID);
		}

		// Token: 0x06003985 RID: 14725 RVA: 0x00125034 File Offset: 0x00123434
		public void MoveTo(float delay)
		{
			if (!this.m_IsDonePrepare)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
				this.m_GameObject = base.gameObject;
				this.m_StartPosition = this.m_RectTransform.position;
				this.m_IsDonePrepare = true;
			}
			this.m_ItemIcon.GetComponent<Image>().color = Color.white;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", this.m_EndLocator.position.x);
			hashtable.Add("y", this.m_EndLocator.position.y);
			hashtable.Add("z", this.m_EndLocator.position.z);
			hashtable.Add("time", 0.4f);
			hashtable.Add("easeType", iTween.EaseType.easeOutCubic);
			hashtable.Add("delay", delay);
			hashtable.Add("oncomplete", "OnCompleteMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_GameObject, hashtable);
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("isLocal", true);
			hashtable2.Add("from", 1f);
			hashtable2.Add("to", 0f);
			hashtable2.Add("time", 0.2f);
			hashtable2.Add("easeType", iTween.EaseType.easeOutCubic);
			hashtable2.Add("delay", delay + 0.4f - 0.2f);
			hashtable2.Add("onupdate", "OnUpdateAlpha");
			hashtable2.Add("onupdatetarget", this.m_GameObject);
			iTween.ValueTo(this.m_ItemIcon.gameObject, hashtable2);
			this.m_IsPlaying = true;
		}

		// Token: 0x06003986 RID: 14726 RVA: 0x0012522B File Offset: 0x0012362B
		public void OnCompleteMove()
		{
			this.m_Anim.gameObject.SetActive(true);
			this.m_Anim.Play();
			this.m_IsPlaying = false;
		}

		// Token: 0x06003987 RID: 14727 RVA: 0x00125251 File Offset: 0x00123651
		public void OnUpdateAlpha(float value)
		{
			this.m_ItemIcon.GetComponent<Image>().color = new Color(1f, 1f, 1f, value);
		}

		// Token: 0x040040B2 RID: 16562
		private const float MOVEDURATION = 0.4f;

		// Token: 0x040040B3 RID: 16563
		private const float FADEDURATION = 0.2f;

		// Token: 0x040040B4 RID: 16564
		[SerializeField]
		private RectTransform m_EndLocator;

		// Token: 0x040040B5 RID: 16565
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x040040B6 RID: 16566
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x040040B7 RID: 16567
		private Vector3 m_StartPosition;

		// Token: 0x040040B8 RID: 16568
		private RectTransform m_RectTransform;

		// Token: 0x040040B9 RID: 16569
		private GameObject m_GameObject;

		// Token: 0x040040BA RID: 16570
		private bool m_IsDonePrepare;

		// Token: 0x040040BB RID: 16571
		private bool m_IsPlaying;
	}
}
