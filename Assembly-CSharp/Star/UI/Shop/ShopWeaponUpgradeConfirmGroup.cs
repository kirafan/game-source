﻿using System;
using System.Collections.Generic;
using Star.UI.Edit;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000ABE RID: 2750
	public class ShopWeaponUpgradeConfirmGroup : UIGroup
	{
		// Token: 0x140000EA RID: 234
		// (add) Token: 0x06003966 RID: 14694 RVA: 0x0012404C File Offset: 0x0012244C
		// (remove) Token: 0x06003967 RID: 14695 RVA: 0x00124084 File Offset: 0x00122484
		public event Action<long, List<int>, List<int>> OnExecute;

		// Token: 0x06003968 RID: 14696 RVA: 0x001240BA File Offset: 0x001224BA
		public void Setup()
		{
			this.m_MixEffectScene.Prepare(1);
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.Mix);
		}

		// Token: 0x06003969 RID: 14697 RVA: 0x001240DA File Offset: 0x001224DA
		public void Destroy()
		{
			this.m_MixEffectScene.Destroy();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadCueSheet(eSoundCueSheetDB.Mix);
		}

		// Token: 0x0600396A RID: 14698 RVA: 0x001240F9 File Offset: 0x001224F9
		public void Open(long weaponMngID)
		{
			this.m_WeaponMngID = weaponMngID;
			this.m_MaterialPanel.Setup(UpgradeMaterialPanel.eMode.Weapon, -1L);
			this.m_MaterialPanel.OnChangeItem += this.OnChangeItem;
			this.Refresh();
			this.Open();
		}

		// Token: 0x0600396B RID: 14699 RVA: 0x00124134 File Offset: 0x00122534
		private void Refresh()
		{
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponMngID);
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID);
			this.m_ClassIcon.Apply((eClassType)param.m_ClassType);
			this.m_RareIcon.Apply((eRare)param.m_Rare);
			this.m_NameText.text = param.m_WeaponName;
			this.m_WeaponIcon.Apply(param.m_ID);
			this.m_ClassText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WeaponEquipClass, new object[]
			{
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + param.m_ClassType)
			});
			this.m_BeforeLv.text = UIUtility.WeaponLvValueToString(userWpnData.Param.Lv, userWpnData.Param.MaxLv, 0, 0);
			this.m_MaterialPanel.ResetUseItem();
			this.m_StartLv = userWpnData.Param.Lv;
			this.m_StartExp = userWpnData.Param.Exp;
			this.OnChangeItem();
		}

		// Token: 0x0600396C RID: 14700 RVA: 0x00124260 File Offset: 0x00122660
		private void OnChangeItem()
		{
			this.m_Amount = 0;
			this.m_Exp = 0L;
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponMngID);
			EditUtility.OutputCharaParam outputCharaParam;
			EditUtility.OutputCharaParam outputCharaParam2;
			EditUtility.SimulateWeaponUpgrade(userWpnData.Param.MngID, this.m_MaterialPanel.GetSelectItemPanel(), out this.m_Amount, out this.m_Exp, out outputCharaParam, out outputCharaParam2);
			this.m_AfterLv.text = UIUtility.WeaponLvValueToString(outputCharaParam2.Lv, userWpnData.Param.MaxLv, outputCharaParam2.Lv - outputCharaParam.Lv, 0);
			this.m_ExpGauge.SetExpData(outputCharaParam.Lv, outputCharaParam.NowExp, userWpnData.Param.MaxLv, outputCharaParam.NextMaxExp);
			this.m_ExpGauge.SetSimulateGauge(outputCharaParam2.NowExp, outputCharaParam2.NextMaxExp, outputCharaParam2.Lv - outputCharaParam.Lv);
			int[] values = new int[]
			{
				outputCharaParam.Atk,
				outputCharaParam.Mgc,
				outputCharaParam.Def,
				outputCharaParam.MDef
			};
			int[] addValues = new int[]
			{
				outputCharaParam2.Atk - outputCharaParam.Atk,
				outputCharaParam2.Mgc - outputCharaParam.Mgc,
				outputCharaParam2.Def - outputCharaParam.Def,
				outputCharaParam2.MDef - outputCharaParam.MDef
			};
			this.m_WeaponStatus.Apply(values, addValues);
			this.m_MaterialPanel.GetUpgradeInfoPanel().Apply(this.m_Exp, (long)this.m_Amount);
			if (userWpnData.Param.MaxLv <= outputCharaParam2.Lv)
			{
				this.m_MaterialPanel.IsEnableIncrease = false;
			}
			else
			{
				this.m_MaterialPanel.IsEnableIncrease = true;
			}
			this.m_MaterialPanel.GetUpgradeInfoPanel().GetDecideButton().Interactable = (this.m_Exp > 0L);
		}

		// Token: 0x0600396D RID: 14701 RVA: 0x00124444 File Offset: 0x00122844
		private void UpdateInfo(int lv, long exp, long getExp)
		{
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponMngID);
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID);
			this.m_AfterLv.text = UIUtility.WeaponLvValueToString(lv, userWpnData.Param.MaxLv, 0, 0);
			long needExp = (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB.GetNextExp(lv, param.m_ExpTableID);
			this.m_ExpGauge.SetExpData(lv, EditUtility.GetWeaponNowExp(userWpnData.Param.WeaponID, lv, exp), userWpnData.Param.MaxLv, needExp);
			int[] array = new int[]
			{
				EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, userWpnData.Param.Lv),
				EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, userWpnData.Param.Lv),
				EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, userWpnData.Param.Lv),
				EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, userWpnData.Param.Lv)
			};
			int[] addValues = new int[]
			{
				EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, lv) - array[0],
				EditUtility.CalcParamCorrectLv(param.m_InitMgc, param.m_MaxMgc, 1, param.m_LimitLv, lv) - array[1],
				EditUtility.CalcParamCorrectLv(param.m_InitDef, param.m_MaxDef, 1, param.m_LimitLv, lv) - array[2],
				EditUtility.CalcParamCorrectLv(param.m_InitMDef, param.m_MaxMDef, 1, param.m_LimitLv, lv) - array[3]
			};
			this.m_WeaponStatus.Apply(array, addValues);
			this.m_MaterialPanel.GetUpgradeInfoPanel().Apply(getExp, (long)this.m_Amount);
			if (userWpnData.Param.MaxLv <= lv)
			{
				this.m_MaterialPanel.IsEnableIncrease = false;
			}
			else
			{
				this.m_MaterialPanel.IsEnableIncrease = true;
			}
			this.m_MaterialPanel.GetUpgradeInfoPanel().GetDecideButton().Interactable = (getExp > 0L);
		}

		// Token: 0x0600396E RID: 14702 RVA: 0x0012469E File Offset: 0x00122A9E
		public void Reset()
		{
			this.m_MaterialPanel.ResetUseItem();
		}

		// Token: 0x0600396F RID: 14703 RVA: 0x001246AC File Offset: 0x00122AAC
		public void OnClickExecuteButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponUpgradeTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponUpgrade), null, new Action<int>(this.OnConfirmExec));
		}

		// Token: 0x06003970 RID: 14704 RVA: 0x00124700 File Offset: 0x00122B00
		private void OnConfirmExec(int btn)
		{
			if (btn == 0)
			{
				List<int> list = new List<int>();
				List<int> list2 = new List<int>();
				for (int i = 0; i < 5; i++)
				{
					int useItemId = this.m_MaterialPanel.GetSelectItemPanel().GetUseItemId(i);
					int useItemNumIndex = this.m_MaterialPanel.GetSelectItemPanel().GetUseItemNumIndex(i);
					if (useItemId != -1)
					{
						list.Add(useItemId);
						list2.Add(useItemNumIndex);
					}
				}
				this.OnExecute.Call(this.m_WeaponMngID, list, list2);
			}
		}

		// Token: 0x06003971 RID: 14705 RVA: 0x00124780 File Offset: 0x00122B80
		public void StartEffect(int lv)
		{
			base.SetEnableInput(false);
			this.m_SuccessLv = lv;
			int[] array = new int[5];
			for (int i = 0; i < 5; i++)
			{
				array[i] = this.m_MaterialPanel.GetSelectItemPanel().GetUseItemId(i);
			}
			this.m_ErasedItemIcon = this.m_MaterialPanel.GetSelectItemPanel().GetComponentsInChildren<ItemIcon>();
			for (int j = 0; j < this.m_ErasedItemIcon.Length; j++)
			{
				this.m_ErasedItemIcon[j].gameObject.SetActive(false);
			}
			int num = 0;
			for (int k = 0; k < array.Length; k++)
			{
				if (array[k] != -1)
				{
					num++;
				}
			}
			for (int l = 0; l < array.Length; l++)
			{
				if (array[l] != -1)
				{
					this.m_UseMaterialIcons[l].Apply(array[l]);
					this.m_UseMaterialIcons[l].SetStartPosition();
					this.m_UseMaterialIcons[l].MoveTo((float)l * 0.1f);
					num--;
				}
			}
			this.m_ExpGauge.SetSimulateOff();
			this.m_Step = ShopWeaponUpgradeConfirmGroup.eEffectStep.Move;
		}

		// Token: 0x06003972 RID: 14706 RVA: 0x001248A8 File Offset: 0x00122CA8
		public override void LateUpdate()
		{
			base.LateUpdate();
			switch (this.m_Step)
			{
			case ShopWeaponUpgradeConfirmGroup.eEffectStep.WaitPrepare:
				if (this.m_MixEffectScene.IsCompletePrepare())
				{
					this.m_Step = ShopWeaponUpgradeConfirmGroup.eEffectStep.Move;
				}
				break;
			case ShopWeaponUpgradeConfirmGroup.eEffectStep.Move:
			{
				bool flag = true;
				for (int i = 0; i < this.m_UseMaterialIcons.Length; i++)
				{
					if (this.m_UseMaterialIcons[i].IsPlaying)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.m_Step = ShopWeaponUpgradeConfirmGroup.eEffectStep.FinalEffect;
				}
				break;
			}
			case ShopWeaponUpgradeConfirmGroup.eEffectStep.FinalEffect:
			{
				int successLv = this.m_SuccessLv;
				if (successLv != 0)
				{
					if (successLv != 1)
					{
						if (successLv == 2)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_218", true);
						}
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_216", true);
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_214", true);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.MIX_WEAPON, 1f, 0, -1, -1);
				this.m_MixEffectScene.Play();
				this.m_UpgradePopUpCloner.GetInst<MixPopUp>().PlayIn(this.m_SuccessLv);
				UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponMngID);
				sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userWpnData.Param.WeaponID).m_ExpTableID;
				WeaponExpDB weaponExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB;
				long[] nextMaxExps = weaponExpDB.GetNextMaxExps(this.m_StartLv, userWpnData.Param.Lv, expTableID);
				this.m_ExpGauge.SetExpData(0L, this.m_StartLv, EditUtility.GetWeaponNowExp(userWpnData.Param.WeaponID, this.m_StartLv, this.m_StartExp), userWpnData.Param.Lv, EditUtility.GetWeaponNowExp(userWpnData.Param.WeaponID, userWpnData.Param.Lv, userWpnData.Param.Exp), userWpnData.Param.MaxLv, nextMaxExps);
				this.m_ExpGauge.Play(true);
				this.m_Step = ShopWeaponUpgradeConfirmGroup.eEffectStep.WaitFinalEffect;
				break;
			}
			case ShopWeaponUpgradeConfirmGroup.eEffectStep.WaitFinalEffect:
				if (this.m_MixEffectScene.IsCompletePlay())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponUpgradeTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponUpgradeResult), null, new Action<int>(this.OnFinish));
					this.m_Step = ShopWeaponUpgradeConfirmGroup.eEffectStep.None;
				}
				break;
			}
		}

		// Token: 0x06003973 RID: 14707 RVA: 0x00124B54 File Offset: 0x00122F54
		private void OnFinish(int btn)
		{
			this.Refresh();
			base.SetEnableInput(true);
			this.m_UpgradePopUpCloner.GetInst<MixPopUp>().PlayOut();
			if (this.m_ErasedItemIcon != null)
			{
				for (int i = 0; i < this.m_ErasedItemIcon.Length; i++)
				{
					this.m_ErasedItemIcon[i].gameObject.SetActive(true);
				}
			}
		}

		// Token: 0x04004089 RID: 16521
		private const float MOVESPAN = 0.1f;

		// Token: 0x0400408A RID: 16522
		private const int MATERIAL_NUM = 5;

		// Token: 0x0400408B RID: 16523
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x0400408C RID: 16524
		[SerializeField]
		private Text m_ClassText;

		// Token: 0x0400408D RID: 16525
		[SerializeField]
		private RareStar m_RareIcon;

		// Token: 0x0400408E RID: 16526
		[SerializeField]
		private Text m_NameText;

		// Token: 0x0400408F RID: 16527
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x04004090 RID: 16528
		[SerializeField]
		private WeaponMaterialIcon[] m_UseMaterialIcons;

		// Token: 0x04004091 RID: 16529
		[SerializeField]
		private UpgradeMaterialPanel m_MaterialPanel;

		// Token: 0x04004092 RID: 16530
		[SerializeField]
		private Text m_BeforeLv;

		// Token: 0x04004093 RID: 16531
		[SerializeField]
		private Text m_AfterLv;

		// Token: 0x04004094 RID: 16532
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04004095 RID: 16533
		[SerializeField]
		private WeaponStatus m_WeaponStatus;

		// Token: 0x04004096 RID: 16534
		[SerializeField]
		private MixWpnUpgradeEffectScene m_MixEffectScene;

		// Token: 0x04004097 RID: 16535
		[SerializeField]
		private PrefabCloner m_UpgradePopUpCloner;

		// Token: 0x04004098 RID: 16536
		private ShopWeaponUpgradeConfirmGroup.eEffectStep m_Step;

		// Token: 0x04004099 RID: 16537
		private int m_Amount;

		// Token: 0x0400409A RID: 16538
		private long m_Exp;

		// Token: 0x0400409B RID: 16539
		private int m_SuccessLv;

		// Token: 0x0400409C RID: 16540
		private long m_WeaponMngID = -1L;

		// Token: 0x0400409E RID: 16542
		private ItemIcon[] m_ErasedItemIcon;

		// Token: 0x0400409F RID: 16543
		private int m_StartLv;

		// Token: 0x040040A0 RID: 16544
		private long m_StartExp;

		// Token: 0x02000ABF RID: 2751
		private enum eEffectStep
		{
			// Token: 0x040040A2 RID: 16546
			None,
			// Token: 0x040040A3 RID: 16547
			WaitPrepare,
			// Token: 0x040040A4 RID: 16548
			Move,
			// Token: 0x040040A5 RID: 16549
			FinalEffect,
			// Token: 0x040040A6 RID: 16550
			WaitFinalEffect
		}
	}
}
