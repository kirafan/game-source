﻿using System;
using Star.UI.BackGround;
using UnityEngine;

namespace Star.UI.Shop
{
	// Token: 0x02000AAF RID: 2735
	public class ShopTradeTopUI : MenuUIBase
	{
		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06003914 RID: 14612 RVA: 0x00122069 File Offset: 0x00120469
		public ShopTradeTopUI.eMainButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000E1 RID: 225
		// (add) Token: 0x06003915 RID: 14613 RVA: 0x00122074 File Offset: 0x00120474
		// (remove) Token: 0x06003916 RID: 14614 RVA: 0x001220AC File Offset: 0x001204AC
		public event Action OnClickMainButton;

		// Token: 0x06003917 RID: 14615 RVA: 0x001220E2 File Offset: 0x001204E2
		public void Setup(bool existLimitTrade)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_LimitButton.Interactable = existLimitTrade;
		}

		// Token: 0x06003918 RID: 14616 RVA: 0x0012211C File Offset: 0x0012051C
		private void Update()
		{
			switch (this.m_Step)
			{
			case ShopTradeTopUI.eStep.BGLoad:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.ChangeStep(ShopTradeTopUI.eStep.In);
				}
				break;
			case ShopTradeTopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(ShopTradeTopUI.eStep.Idle);
				}
				break;
			}
			case ShopTradeTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(ShopTradeTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003919 RID: 14617 RVA: 0x0012223C File Offset: 0x0012063C
		private void ChangeStep(ShopTradeTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case ShopTradeTopUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case ShopTradeTopUI.eStep.BGLoad:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Trade, true);
				break;
			case ShopTradeTopUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					this.m_AnimUIPlayerArray[i].PlayIn();
				}
				this.m_MainGroup.Open();
				break;
			case ShopTradeTopUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case ShopTradeTopUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case ShopTradeTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600391A RID: 14618 RVA: 0x0012234C File Offset: 0x0012074C
		public override void PlayIn()
		{
			this.ChangeStep(ShopTradeTopUI.eStep.BGLoad);
		}

		// Token: 0x0600391B RID: 14619 RVA: 0x00122358 File Offset: 0x00120758
		public override void PlayOut()
		{
			this.ChangeStep(ShopTradeTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x0600391C RID: 14620 RVA: 0x00122392 File Offset: 0x00120792
		public override bool IsMenuEnd()
		{
			return this.m_Step == ShopTradeTopUI.eStep.End;
		}

		// Token: 0x0600391D RID: 14621 RVA: 0x0012239D File Offset: 0x0012079D
		public void OnClickMainButtonCallBack(int btn)
		{
			this.m_SelectButton = (ShopTradeTopUI.eMainButton)btn;
			this.OnClickMainButton.Call();
		}

		// Token: 0x04004013 RID: 16403
		private ShopTradeTopUI.eStep m_Step;

		// Token: 0x04004014 RID: 16404
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04004015 RID: 16405
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04004016 RID: 16406
		[SerializeField]
		private CustomButton m_LimitButton;

		// Token: 0x04004017 RID: 16407
		private ShopTradeTopUI.eMainButton m_SelectButton = ShopTradeTopUI.eMainButton.None;

		// Token: 0x02000AB0 RID: 2736
		private enum eStep
		{
			// Token: 0x0400401A RID: 16410
			Hide,
			// Token: 0x0400401B RID: 16411
			BGLoad,
			// Token: 0x0400401C RID: 16412
			In,
			// Token: 0x0400401D RID: 16413
			Idle,
			// Token: 0x0400401E RID: 16414
			Out,
			// Token: 0x0400401F RID: 16415
			End
		}

		// Token: 0x02000AB1 RID: 2737
		public enum eMainButton
		{
			// Token: 0x04004021 RID: 16417
			None = -1,
			// Token: 0x04004022 RID: 16418
			Trade,
			// Token: 0x04004023 RID: 16419
			Limit,
			// Token: 0x04004024 RID: 16420
			Sale
		}
	}
}
