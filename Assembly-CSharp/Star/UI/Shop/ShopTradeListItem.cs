﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Shop
{
	// Token: 0x02000AA3 RID: 2723
	public class ShopTradeListItem : ScrollItemIcon
	{
		// Token: 0x060038D6 RID: 14550 RVA: 0x00120238 File Offset: 0x0011E638
		protected override void ApplyData()
		{
			base.ApplyData();
			ShopTradeListItemData shopTradeListItemData = (ShopTradeListItemData)this.m_Data;
			ShopManager.TradeData tradeData = shopTradeListItemData.m_TradeData;
			int dstID = tradeData.m_DstID;
			ePresentType dstType = tradeData.m_DstType;
			if (dstType != ePresentType.Item)
			{
				if (dstType != ePresentType.Chara)
				{
					if (dstType == ePresentType.Weapon)
					{
						this.m_DstVariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyWeapon(dstID);
					}
				}
				else
				{
					this.m_DstVariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyChara(dstID);
				}
			}
			else
			{
				this.m_DstVariableIconWithFrameCloner.GetInst<VariableIconWithFrame>().ApplyItem(dstID);
			}
			this.m_NameText.text = shopTradeListItemData.m_TradeName;
			bool flag = true;
			this.m_LimitText.text = tradeData.RemainNum.ToString();
			if (tradeData.RemainNum <= 0)
			{
				flag = false;
			}
			this.m_DeadlineText.text = UIUtility.GetTradeLimitSpanString(tradeData.DeadLineSpan);
			if (tradeData.DeadLineSpan.TotalSeconds <= 0.0)
			{
				flag = false;
			}
			bool flag2 = false;
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < tradeData.m_SrcTypes.Length; i++)
			{
				bool flag3 = true;
				if (tradeData.m_SrcTypes[i] == ePresentType.None)
				{
					this.m_SrcObj[i].SetActive(false);
				}
				else
				{
					this.m_SrcObj[i].SetActive(true);
					ePresentType ePresentType = tradeData.m_SrcTypes[i];
					switch (ePresentType)
					{
					case ePresentType.KRRPoint:
						this.m_SrcVariableIconCloners[i].GetInst<VariableIcon>().ApplyKirara();
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfKP((long)tradeData.m_SrcAmounts[i]))
						{
							flag3 = false;
						}
						stringBuilder.Length = 0;
						stringBuilder.Append(UIUtility.KPValueToString(tradeData.m_SrcAmounts[i], false));
						break;
					case ePresentType.Gold:
						this.m_SrcVariableIconCloners[i].GetInst<VariableIcon>().ApplyGold();
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)tradeData.m_SrcAmounts[i]))
						{
							flag3 = false;
						}
						stringBuilder.Length = 0;
						stringBuilder.Append(UIUtility.GoldValueToString(tradeData.m_SrcAmounts[i], true));
						break;
					case ePresentType.UnlimitedGem:
					case ePresentType.LimitedGem:
						this.m_SrcVariableIconCloners[i].GetInst<VariableIcon>().ApplyGem();
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)tradeData.m_SrcAmounts[i]))
						{
							flag3 = false;
						}
						stringBuilder.Length = 0;
						stringBuilder.Append(UIUtility.GemValueToString(tradeData.m_SrcAmounts[i], true));
						break;
					default:
						if (ePresentType == ePresentType.Item)
						{
							this.m_SrcVariableIconCloners[i].GetInst<VariableIcon>().ApplyItem(tradeData.m_SrcIDs[i]);
							if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(tradeData.m_SrcIDs[i]) < tradeData.m_SrcAmounts[i])
							{
								flag3 = false;
							}
							stringBuilder.Length = 0;
							stringBuilder.Append(tradeData.m_SrcAmounts[i]);
						}
						break;
					}
					if (flag3)
					{
						flag2 = true;
					}
					if (!flag3)
					{
						UIUtility.GetNotEnoughString(stringBuilder);
					}
					this.m_SrcAmountTexts[i].text = stringBuilder.ToString();
				}
			}
			if (!this.m_CantExec && tradeData.RemainNum > 0 && (!flag || !flag2))
			{
				this.m_CantExecCG.ChangeColor(UIDefine.CANT_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
				this.m_CantExec = true;
			}
			else if (this.m_CantExec && (tradeData.RemainNum == 0 || (flag && flag2)))
			{
				this.m_CantExecCG.RevertColor();
				this.m_CantExec = false;
			}
			this.m_Button.Interactable = (tradeData.RemainNum > 0);
		}

		// Token: 0x04003FB9 RID: 16313
		[SerializeField]
		private PrefabCloner m_DstVariableIconWithFrameCloner;

		// Token: 0x04003FBA RID: 16314
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003FBB RID: 16315
		[SerializeField]
		private Text m_LimitText;

		// Token: 0x04003FBC RID: 16316
		[SerializeField]
		private Text m_DeadlineText;

		// Token: 0x04003FBD RID: 16317
		[SerializeField]
		private GameObject[] m_SrcObj;

		// Token: 0x04003FBE RID: 16318
		[SerializeField]
		private PrefabCloner[] m_SrcVariableIconCloners;

		// Token: 0x04003FBF RID: 16319
		[SerializeField]
		private Text[] m_SrcAmountTexts;

		// Token: 0x04003FC0 RID: 16320
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04003FC1 RID: 16321
		[SerializeField]
		private ColorGroup m_CantExecCG;

		// Token: 0x04003FC2 RID: 16322
		private bool m_CantExec;
	}
}
