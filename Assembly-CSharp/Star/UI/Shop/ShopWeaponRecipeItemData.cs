﻿using System;

namespace Star.UI.Shop
{
	// Token: 0x02000AB7 RID: 2743
	public class ShopWeaponRecipeItemData : ScrollItemData
	{
		// Token: 0x06003940 RID: 14656 RVA: 0x00123490 File Offset: 0x00121890
		public ShopWeaponRecipeItemData(int recipeID)
		{
			this.m_RecipeID = recipeID;
			WeaponRecipeListDB_Param paramWithRecipeID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponRecipeListDB.GetParamWithRecipeID(recipeID);
			this.m_WpnID = paramWithRecipeID.m_WeaponID;
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.m_WpnID);
			this.m_WpnName = param.m_WeaponName;
			this.m_Rare = (eRare)param.m_Rare;
			this.m_Amount = paramWithRecipeID.m_Amount;
		}

		// Token: 0x140000E5 RID: 229
		// (add) Token: 0x06003941 RID: 14657 RVA: 0x00123518 File Offset: 0x00121918
		// (remove) Token: 0x06003942 RID: 14658 RVA: 0x00123550 File Offset: 0x00121950
		public event Action<int> OnClickRecipe;

		// Token: 0x06003943 RID: 14659 RVA: 0x00123586 File Offset: 0x00121986
		public override void OnClickItemCallBack()
		{
			this.OnClickRecipe.Call(this.m_RecipeID);
		}

		// Token: 0x0400405B RID: 16475
		public int m_RecipeID = -1;

		// Token: 0x0400405C RID: 16476
		public int m_Amount;

		// Token: 0x0400405D RID: 16477
		public int m_WpnID;

		// Token: 0x0400405E RID: 16478
		public string m_WpnName;

		// Token: 0x0400405F RID: 16479
		public eRare m_Rare;
	}
}
