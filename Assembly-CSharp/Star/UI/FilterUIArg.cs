﻿using System;

namespace Star.UI
{
	// Token: 0x02000A30 RID: 2608
	public class FilterUIArg : OverlayUIArgBase
	{
		// Token: 0x0600364E RID: 13902 RVA: 0x001123B2 File Offset: 0x001107B2
		public FilterUIArg(UISettings.eUsingType_Filter usingType, Action OnExecute)
		{
			this.m_UsingType = usingType;
			this.m_OnFilterExecute = OnExecute;
		}

		// Token: 0x04003CA4 RID: 15524
		public UISettings.eUsingType_Filter m_UsingType;

		// Token: 0x04003CA5 RID: 15525
		public bool m_IsSupportTitleSelect;

		// Token: 0x04003CA6 RID: 15526
		public Action m_OnFilterExecute;
	}
}
