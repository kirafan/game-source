﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007E4 RID: 2020
	public class SelectedCharaInfoHaveTitleTextField : SelectedCharaInfoHaveTitleField
	{
		// Token: 0x060029B9 RID: 10681 RVA: 0x000DBA74 File Offset: 0x000D9E74
		public void SetText(string text)
		{
			if (this.m_text != null)
			{
				this.m_text.text = text;
			}
		}

		// Token: 0x0400302C RID: 12332
		[SerializeField]
		private Text m_text;
	}
}
