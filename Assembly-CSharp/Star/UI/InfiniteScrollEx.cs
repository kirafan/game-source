﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000882 RID: 2178
	public class InfiniteScrollEx : InfiniteScroll
	{
		// Token: 0x06002D37 RID: 11575 RVA: 0x000EF51D File Offset: 0x000ED91D
		protected override void Update()
		{
			this.InfiniteScrollUpdate();
			this.UpdateScrollActive();
			this.UpdateScrollButtonActive();
		}

		// Token: 0x06002D38 RID: 11576 RVA: 0x000EF531 File Offset: 0x000ED931
		protected void InfiniteScrollUpdate()
		{
			base.Update();
		}

		// Token: 0x06002D39 RID: 11577 RVA: 0x000EF53C File Offset: 0x000ED93C
		public void UpdateScrollActive()
		{
			if (this.m_ScrollActiveControlType == InfiniteScrollEx.eActiveControlType.None)
			{
				return;
			}
			bool horizontal = true;
			InfiniteScrollEx.eActiveControlType scrollActiveControlType = this.m_ScrollActiveControlType;
			if (scrollActiveControlType != InfiniteScrollEx.eActiveControlType.AlwaysDeactive)
			{
				if (scrollActiveControlType != InfiniteScrollEx.eActiveControlType.AlwaysActive)
				{
					if (scrollActiveControlType == InfiniteScrollEx.eActiveControlType.AutoActive)
					{
						horizontal = (1 < this.m_ItemList.Count);
					}
				}
				else
				{
					horizontal = true;
				}
			}
			else
			{
				horizontal = false;
			}
			this.m_ScrollRect.horizontal = horizontal;
		}

		// Token: 0x06002D3A RID: 11578 RVA: 0x000EF5A8 File Offset: 0x000ED9A8
		public void UpdateScrollButtonActive()
		{
			if (this.m_ScrollActiveControlType == InfiniteScrollEx.eActiveControlType.None)
			{
				return;
			}
			bool active = true;
			InfiniteScrollEx.eActiveControlType buttonActiveControlType = this.m_ButtonActiveControlType;
			if (buttonActiveControlType != InfiniteScrollEx.eActiveControlType.AlwaysDeactive)
			{
				if (buttonActiveControlType != InfiniteScrollEx.eActiveControlType.AlwaysActive)
				{
					if (buttonActiveControlType == InfiniteScrollEx.eActiveControlType.AutoActive)
					{
						active = (1 < this.m_ItemList.Count);
					}
				}
				else
				{
					active = true;
				}
			}
			else
			{
				active = false;
			}
			this.m_LeftButtonObj.SetActive(active);
			this.m_RightButtonObj.SetActive(active);
		}

		// Token: 0x06002D3B RID: 11579 RVA: 0x000EF61E File Offset: 0x000EDA1E
		public virtual void OnLeftButton()
		{
			this.OnClickScrollButton(this.m_NowSelect - 1);
		}

		// Token: 0x06002D3C RID: 11580 RVA: 0x000EF62E File Offset: 0x000EDA2E
		public virtual void OnRightButton()
		{
			this.OnClickScrollButton(this.m_NowSelect + 1);
		}

		// Token: 0x06002D3D RID: 11581 RVA: 0x000EF63E File Offset: 0x000EDA3E
		protected virtual void OnClickScrollButton(int addIdx)
		{
			base.SetSelectIdx(addIdx, true);
		}

		// Token: 0x0400344E RID: 13390
		[SerializeField]
		protected GameObject m_RightButtonObj;

		// Token: 0x0400344F RID: 13391
		[SerializeField]
		protected GameObject m_LeftButtonObj;

		// Token: 0x04003450 RID: 13392
		protected InfiniteScrollEx.eActiveControlType m_ScrollActiveControlType = InfiniteScrollEx.eActiveControlType.AutoActive;

		// Token: 0x04003451 RID: 13393
		protected InfiniteScrollEx.eActiveControlType m_ButtonActiveControlType = InfiniteScrollEx.eActiveControlType.AutoActive;

		// Token: 0x02000883 RID: 2179
		public enum eActiveControlType
		{
			// Token: 0x04003453 RID: 13395
			None,
			// Token: 0x04003454 RID: 13396
			AlwaysDeactive,
			// Token: 0x04003455 RID: 13397
			AlwaysActive,
			// Token: 0x04003456 RID: 13398
			AutoActive
		}
	}
}
