﻿using System;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B14 RID: 2836
	public class TutorialTipsScroll : InfiniteScrollEx
	{
		// Token: 0x06003B98 RID: 15256 RVA: 0x00130513 File Offset: 0x0012E913
		public override void Setup()
		{
			base.Setup();
			this.m_ScrollActiveControlType = InfiniteScrollEx.eActiveControlType.AutoActive;
			this.m_ButtonActiveControlType = InfiniteScrollEx.eActiveControlType.AutoActive;
			base.SetNotInfiniteFlag(true);
		}

		// Token: 0x06003B99 RID: 15257 RVA: 0x00130530 File Offset: 0x0012E930
		protected override void Update()
		{
			base.Update();
			this.m_LeftButtonObj.SetActive(0 < this.m_NowSelect);
			this.m_RightButtonObj.SetActive(this.m_NowSelect < this.m_ItemList.Count - 1);
			if (0 < this.m_ItemList.Count)
			{
				this.m_Image.sprite = ((TutorialTipsUIItemData)this.m_ItemList[base.GetNowSelectDataIdx()]).m_ImageSprite;
			}
		}

		// Token: 0x06003B9A RID: 15258 RVA: 0x001305AE File Offset: 0x0012E9AE
		public void InitializeMainImage()
		{
			this.m_Image.sprite = null;
		}

		// Token: 0x06003B9B RID: 15259 RVA: 0x001305BC File Offset: 0x0012E9BC
		public bool IsEndPage()
		{
			int num = (this.m_ItemList.Count <= 0) ? 0 : (this.m_NowSelect % this.m_ItemList.Count);
			num = ((num >= 0) ? num : (this.m_ItemList.Count + num));
			return num == this.m_ItemList.Count - 1;
		}

		// Token: 0x04004333 RID: 17203
		public Image m_Image;
	}
}
