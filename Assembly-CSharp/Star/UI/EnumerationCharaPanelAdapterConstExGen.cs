﻿using System;

namespace Star.UI
{
	// Token: 0x02000957 RID: 2391
	public abstract class EnumerationCharaPanelAdapterConstExGen<ParentType> : EnumerationCharaPanelAdapterConst where ParentType : EnumerationPanelBase
	{
		// Token: 0x0600319B RID: 12699 RVA: 0x000FEB28 File Offset: 0x000FCF28
		public virtual void Setup(ParentType parent, EquipWeaponPartyMemberController partyMemberController)
		{
			this.m_Core = this.CreateCore();
			this.m_Parent = parent;
			this.m_SharedInstance = this.CreateArgument(partyMemberController);
			this.m_Core.Setup(this.m_SharedInstance);
		}

		// Token: 0x0600319C RID: 12700 RVA: 0x000FEB5B File Offset: 0x000FCF5B
		protected override EnumerationPanelBase GetParent()
		{
			return this.m_Parent;
		}

		// Token: 0x040037FC RID: 14332
		protected ParentType m_Parent;
	}
}
