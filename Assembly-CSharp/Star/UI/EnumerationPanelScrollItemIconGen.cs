﻿using System;

namespace Star.UI
{
	// Token: 0x02000973 RID: 2419
	public abstract class EnumerationPanelScrollItemIconGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where ThisType : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapter where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
	}
}
