﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008A9 RID: 2217
	public class SwapObj : MonoBehaviour
	{
		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06002DF1 RID: 11761 RVA: 0x000F1DFB File Offset: 0x000F01FB
		public bool IgnoreCustomButton
		{
			get
			{
				return this.m_IgnoreCustomButton;
			}
		}

		// Token: 0x06002DF2 RID: 11762 RVA: 0x000F1E04 File Offset: 0x000F0204
		private void Start()
		{
			for (int i = 0; i < this.m_Objs.Length; i++)
			{
				if (this.m_Objs[i].activeSelf)
				{
					this.m_Objs[i].SetActive(false);
				}
			}
			this.m_Objs[this.m_CurrentIdx].SetActive(true);
		}

		// Token: 0x06002DF3 RID: 11763 RVA: 0x000F1E5D File Offset: 0x000F025D
		public int GetTargetObjNum()
		{
			return this.m_Objs.Length;
		}

		// Token: 0x06002DF4 RID: 11764 RVA: 0x000F1E67 File Offset: 0x000F0267
		public void Swap()
		{
			if (this.m_CurrentIdx == 0)
			{
				this.Swap(1);
			}
			else
			{
				this.Swap(0);
			}
		}

		// Token: 0x06002DF5 RID: 11765 RVA: 0x000F1E88 File Offset: 0x000F0288
		public void Swap(int idx)
		{
			if (idx >= this.m_Objs.Length)
			{
				idx = 0;
			}
			if (this.m_CurrentIdx == idx)
			{
				return;
			}
			this.m_Objs[this.m_CurrentIdx].SetActive(false);
			if (idx >= this.m_Objs.Length || this.m_Objs[idx] == null)
			{
				this.m_CurrentIdx = 0;
			}
			else
			{
				this.m_CurrentIdx = idx;
			}
			this.m_Objs[this.m_CurrentIdx].SetActive(true);
		}

		// Token: 0x04003503 RID: 13571
		private int m_CurrentIdx;

		// Token: 0x04003504 RID: 13572
		[SerializeField]
		[Tooltip("CustomButtonから操作する場合、0=base, 1=press, 2=notInteractable, 3=decide")]
		private GameObject[] m_Objs;

		// Token: 0x04003505 RID: 13573
		[SerializeField]
		private bool m_IgnoreCustomButton;
	}
}
