﻿using System;

namespace Star.UI
{
	// Token: 0x02000954 RID: 2388
	public abstract class EnumerationCharaPanelAdapter : EnumerationCharaPanelAdapterBaseExGen<EnumerationCharaPanel>
	{
		// Token: 0x06003190 RID: 12688 RVA: 0x000FBE94 File Offset: 0x000FA294
		public virtual EnumerationCharaPanelStandardBase.SharedInstanceEx CreateArgument()
		{
			if (this.m_DataContent != null)
			{
				this.m_SerializedConstructInstances.m_DataContent = this.m_DataContent;
			}
			if (this.m_CharaInfo != null)
			{
				this.m_SerializedConstructInstances.m_CharaInfo = this.m_CharaInfo;
			}
			if (this.m_Blank != null)
			{
				this.m_SerializedConstructInstances.m_Blank = this.m_Blank;
			}
			if (this.m_EmptyObj != null)
			{
				this.m_SerializedConstructInstances.m_EmptyObj = this.m_EmptyObj;
			}
			if (this.m_FriendObj != null)
			{
				this.m_SerializedConstructInstances.m_FriendObj = this.m_FriendObj;
			}
			return new EnumerationCharaPanelStandardBase.SharedInstanceEx(this.m_SerializedConstructInstances)
			{
				parent = this
			};
		}

		// Token: 0x06003191 RID: 12689 RVA: 0x000FBF5F File Offset: 0x000FA35F
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
			if (this.m_Core != null)
			{
				this.m_Core.Apply(partyMemberController);
			}
		}

		// Token: 0x06003192 RID: 12690 RVA: 0x000FBF78 File Offset: 0x000FA378
		public int GetSlotIndex()
		{
			if (this.m_Core == null)
			{
				return -1;
			}
			return this.m_Core.GetSlotIndex();
		}
	}
}
