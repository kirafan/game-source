﻿using System;

namespace Star.UI
{
	// Token: 0x02000960 RID: 2400
	public abstract class EnumerationCharaPanelStandardBase : EnumerationCharaPanelBaseExGen<EnumerationCharaPanelAdapterBase, EnumerationCharaPanelStandardBase.SharedInstanceEx>
	{
		// Token: 0x060031BB RID: 12731 RVA: 0x000FE9A4 File Offset: 0x000FCDA4
		public int GetSlotIndex()
		{
			if (this.m_SharedIntance.partyMemberController == null)
			{
				return -1;
			}
			return this.m_SharedIntance.partyMemberController.GetPartySlotIndex();
		}

		// Token: 0x02000961 RID: 2401
		[Serializable]
		public class SharedInstanceEx : EnumerationCharaPanelBase.SharedInstanceExGen<EnumerationCharaPanelAdapterBase>
		{
			// Token: 0x060031BC RID: 12732 RVA: 0x000FE9C8 File Offset: 0x000FCDC8
			public SharedInstanceEx()
			{
			}

			// Token: 0x060031BD RID: 12733 RVA: 0x000FE9D0 File Offset: 0x000FCDD0
			public SharedInstanceEx(EnumerationCharaPanelBase.SharedInstance argument) : base(argument)
			{
			}

			// Token: 0x04003817 RID: 14359
			public EquipWeaponPartyMemberController partyMemberController;
		}
	}
}
