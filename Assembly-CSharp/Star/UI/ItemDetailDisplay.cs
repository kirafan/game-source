﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000889 RID: 2185
	public class ItemDetailDisplay : MonoBehaviour
	{
		// Token: 0x06002D5D RID: 11613 RVA: 0x000EFAF4 File Offset: 0x000EDEF4
		public void ApplyGem(int num = -1)
		{
			this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyGem();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("星彩石");
			if (num > 0)
			{
				stringBuilder.Append("x");
				stringBuilder.Append(num);
			}
			this.m_NameText.text = stringBuilder.ToString();
			this.m_DetailText.text = "いし";
		}

		// Token: 0x06002D5E RID: 11614 RVA: 0x000EFB60 File Offset: 0x000EDF60
		public void ApplyGold(int num = -1)
		{
			this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyGold();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("おかね");
			if (num > 0)
			{
				stringBuilder.Append("x");
				stringBuilder.Append(num);
			}
			this.m_NameText.text = stringBuilder.ToString();
			this.m_DetailText.text = "おかね";
		}

		// Token: 0x06002D5F RID: 11615 RVA: 0x000EFBCC File Offset: 0x000EDFCC
		public void ApplyKirara(int num = -1)
		{
			this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyKirara();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("きららポイント");
			if (num > 0)
			{
				stringBuilder.Append("x");
				stringBuilder.Append(num);
			}
			this.m_DetailText.text = "きららぽいんと";
		}

		// Token: 0x06002D60 RID: 11616 RVA: 0x000EFC28 File Offset: 0x000EE028
		public void ApplyItem(int itemID, int num = -1)
		{
			ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID);
			this.m_VariableIconCloner.GetInst<VariableIconWithFrame>().ApplyItem(itemID);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(param.m_Name);
			if (num > 1)
			{
				stringBuilder.Append("x");
				stringBuilder.Append(num);
			}
			this.m_NameText.text = stringBuilder.ToString();
			this.m_DetailText.text = param.m_DetailText;
		}

		// Token: 0x0400346C RID: 13420
		[SerializeField]
		private PrefabCloner m_VariableIconCloner;

		// Token: 0x0400346D RID: 13421
		[SerializeField]
		private Text m_NameText;

		// Token: 0x0400346E RID: 13422
		[SerializeField]
		private Text m_DetailText;
	}
}
