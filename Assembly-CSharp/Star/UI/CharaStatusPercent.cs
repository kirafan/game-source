﻿using System;

namespace Star.UI
{
	// Token: 0x0200080F RID: 2063
	public class CharaStatusPercent : CharaStatusBase
	{
		// Token: 0x06002AA4 RID: 10916 RVA: 0x000E1750 File Offset: 0x000DFB50
		public void Apply(float[] values)
		{
			if (values == null || values.Length != 6)
			{
				return;
			}
			string[] array = new string[6];
			for (int i = 0; i < 6; i++)
			{
				array[i] = values[i].ToString() + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ProfileStateDetailPercent);
			}
			base.Apply(array);
		}
	}
}
