﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F1 RID: 2033
	public class AnimUIPlayer : MonoBehaviour
	{
		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x060029FD RID: 10749 RVA: 0x000DD608 File Offset: 0x000DBA08
		public int State
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x060029FE RID: 10750 RVA: 0x000DD610 File Offset: 0x000DBA10
		public bool IsEnd
		{
			get
			{
				return this.m_State != 1 && this.m_State != 3;
			}
		}

		// Token: 0x14000057 RID: 87
		// (add) Token: 0x060029FF RID: 10751 RVA: 0x000DD630 File Offset: 0x000DBA30
		// (remove) Token: 0x06002A00 RID: 10752 RVA: 0x000DD668 File Offset: 0x000DBA68
		public event Action OnAnimCompleteIn;

		// Token: 0x14000058 RID: 88
		// (add) Token: 0x06002A01 RID: 10753 RVA: 0x000DD6A0 File Offset: 0x000DBAA0
		// (remove) Token: 0x06002A02 RID: 10754 RVA: 0x000DD6D8 File Offset: 0x000DBAD8
		public event Action OnAnimCompleteOut;

		// Token: 0x06002A03 RID: 10755 RVA: 0x000DD70E File Offset: 0x000DBB0E
		private void Awake()
		{
			this.m_GameObject = base.gameObject;
			this.m_AnimUIList = base.GetComponents<AnimUIBase>();
		}

		// Token: 0x06002A04 RID: 10756 RVA: 0x000DD728 File Offset: 0x000DBB28
		private void Start()
		{
			if (this.m_State == 0)
			{
				this.SetState(0);
			}
		}

		// Token: 0x06002A05 RID: 10757 RVA: 0x000DD73C File Offset: 0x000DBB3C
		private void LateUpdate()
		{
			int state = this.m_State;
			if (state != 1)
			{
				if (state == 3)
				{
					if (this.m_IsWaitingSyncImage)
					{
						if (this.CheckDoneLoad())
						{
							this.m_IsWaitingSyncImage = false;
							this.ExecPlayOut();
						}
						else
						{
							this.m_IsWaitingSyncImage = true;
						}
					}
					else if (this.CheckEnd())
					{
						if (this.OnAnimCompleteOut != null)
						{
							this.OnAnimCompleteOut();
						}
						iTween.Stop(this.m_GameObject);
						this.SetState(0);
					}
				}
			}
			else if (this.m_IsWaitingSyncImage)
			{
				if (this.CheckDoneLoad())
				{
					this.m_IsWaitingSyncImage = false;
					this.ExecPlayIn();
				}
				else
				{
					this.m_IsWaitingSyncImage = true;
				}
			}
			else if (this.CheckEnd())
			{
				if (this.OnAnimCompleteIn != null)
				{
					this.OnAnimCompleteIn();
				}
				iTween.Stop(this.m_GameObject);
				this.SetState(2);
			}
		}

		// Token: 0x06002A06 RID: 10758 RVA: 0x000DD83C File Offset: 0x000DBC3C
		private void SetState(int state)
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
				this.m_AnimUIList = base.GetComponents<AnimUIBase>();
			}
			this.m_State = state;
			if (!this.m_ActiveOnHide && state == 0)
			{
				this.m_GameObject.SetActive(false);
			}
			else
			{
				this.m_GameObject.SetActive(true);
			}
		}

		// Token: 0x06002A07 RID: 10759 RVA: 0x000DD8A8 File Offset: 0x000DBCA8
		private bool CheckDoneLoad()
		{
			if (this.m_WaitASyncImages != null)
			{
				for (int i = 0; i < this.m_WaitASyncImages.Length; i++)
				{
					if (this.m_WaitASyncImages[i].GetSpriteHandler() != null && !this.m_WaitASyncImages[i].IsDoneLoad())
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06002A08 RID: 10760 RVA: 0x000DD900 File Offset: 0x000DBD00
		public void PlayIn()
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			if (!this.m_GameObject.activeSelf)
			{
				this.m_GameObject.SetActive(true);
			}
			this.m_AnimUIList = base.GetComponents<AnimUIBase>();
			this.SetState(1);
			if (this.CheckDoneLoad())
			{
				this.m_IsWaitingSyncImage = false;
				this.ExecPlayIn();
			}
			else
			{
				this.m_IsWaitingSyncImage = true;
			}
		}

		// Token: 0x06002A09 RID: 10761 RVA: 0x000DD980 File Offset: 0x000DBD80
		private void ExecPlayIn()
		{
			for (int i = 0; i < this.m_AnimUIList.Length; i++)
			{
				this.m_AnimUIList[i].PlayIn();
			}
		}

		// Token: 0x06002A0A RID: 10762 RVA: 0x000DD9B4 File Offset: 0x000DBDB4
		public void PlayOut()
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			if (!this.m_GameObject.activeSelf)
			{
				this.m_GameObject.SetActive(true);
			}
			this.m_AnimUIList = base.GetComponents<AnimUIBase>();
			this.SetState(3);
			if (this.CheckDoneLoad())
			{
				this.m_IsWaitingSyncImage = false;
				this.ExecPlayOut();
			}
			else
			{
				this.m_IsWaitingSyncImage = true;
			}
		}

		// Token: 0x06002A0B RID: 10763 RVA: 0x000DDA34 File Offset: 0x000DBE34
		private void ExecPlayOut()
		{
			for (int i = 0; i < this.m_AnimUIList.Length; i++)
			{
				this.m_AnimUIList[i].PlayOut();
			}
		}

		// Token: 0x06002A0C RID: 10764 RVA: 0x000DDA68 File Offset: 0x000DBE68
		public void Hide()
		{
			this.m_AnimUIList = base.GetComponents<AnimUIBase>();
			this.SetState(0);
			for (int i = 0; i < this.m_AnimUIList.Length; i++)
			{
				this.m_AnimUIList[i].Hide();
			}
		}

		// Token: 0x06002A0D RID: 10765 RVA: 0x000DDAB0 File Offset: 0x000DBEB0
		public void Show()
		{
			this.m_AnimUIList = base.GetComponents<AnimUIBase>();
			this.SetState(2);
			for (int i = 0; i < this.m_AnimUIList.Length; i++)
			{
				this.m_AnimUIList[i].Show();
			}
		}

		// Token: 0x06002A0E RID: 10766 RVA: 0x000DDAF8 File Offset: 0x000DBEF8
		private bool CheckEnd()
		{
			for (int i = 0; i < this.m_AnimUIList.Length; i++)
			{
				if (this.m_AnimUIList[i].IsPlaying)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0400305C RID: 12380
		public const int STATE_HIDE = 0;

		// Token: 0x0400305D RID: 12381
		public const int STATE_IN = 1;

		// Token: 0x0400305E RID: 12382
		public const int STATE_WAIT = 2;

		// Token: 0x0400305F RID: 12383
		public const int STATE_OUT = 3;

		// Token: 0x04003060 RID: 12384
		private int m_State;

		// Token: 0x04003061 RID: 12385
		private AnimUIBase[] m_AnimUIList;

		// Token: 0x04003062 RID: 12386
		private GameObject m_GameObject;

		// Token: 0x04003063 RID: 12387
		[SerializeField]
		[Tooltip("Hide時にアクティブのままにする")]
		private bool m_ActiveOnHide;

		// Token: 0x04003064 RID: 12388
		[SerializeField]
		[Tooltip("指定ASyncImageのロードが完了していない場合、Playは実行されない")]
		private ASyncImage[] m_WaitASyncImages;

		// Token: 0x04003065 RID: 12389
		private bool m_IsWaitingSyncImage;
	}
}
