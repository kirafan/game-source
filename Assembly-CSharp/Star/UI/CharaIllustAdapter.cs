﻿using System;

namespace Star.UI
{
	// Token: 0x020007B7 RID: 1975
	public class CharaIllustAdapter : ASyncImageAdapterSyncImage
	{
		// Token: 0x060028D5 RID: 10453 RVA: 0x000D9B64 File Offset: 0x000D7F64
		public CharaIllustAdapter(CharaIllust charaIllust, CharaIllust.eCharaIllustType characterIllustType)
		{
			this.m_CharaIllust = charaIllust;
			this.m_CharacterIllustType = characterIllustType;
		}

		// Token: 0x060028D6 RID: 10454 RVA: 0x000D9B81 File Offset: 0x000D7F81
		public override void Apply(int charaID, int weaponID)
		{
			if (this.m_CharaIllust == null)
			{
				return;
			}
			this.m_CharaIllust.Apply(charaID, this.m_CharacterIllustType);
		}

		// Token: 0x060028D7 RID: 10455 RVA: 0x000D9BA7 File Offset: 0x000D7FA7
		protected override ASyncImage GetASyncImage()
		{
			return this.m_CharaIllust;
		}

		// Token: 0x04002FA7 RID: 12199
		private CharaIllust m_CharaIllust;

		// Token: 0x04002FA8 RID: 12200
		private CharaIllust.eCharaIllustType m_CharacterIllustType = CharaIllust.eCharaIllustType.Card;
	}
}
