﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000832 RID: 2098
	public class ExpGauge : MonoBehaviour
	{
		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06002BC8 RID: 11208 RVA: 0x000E708A File Offset: 0x000E548A
		public bool IsPlaying
		{
			get
			{
				return this.m_IsPlaying;
			}
		}

		// Token: 0x06002BC9 RID: 11209 RVA: 0x000E7092 File Offset: 0x000E5492
		private void OnDestroy()
		{
			this.StopSE();
		}

		// Token: 0x06002BCA RID: 11210 RVA: 0x000E709A File Offset: 0x000E549A
		public LevelUpPop[] GetLevelUpPops()
		{
			return this.m_LevelUpImageObj;
		}

		// Token: 0x06002BCB RID: 11211 RVA: 0x000E70A4 File Offset: 0x000E54A4
		public void Setup()
		{
			if (this.m_LevelUpImageObj != null && this.m_LevelUpImageObj.Length > 0)
			{
				this.m_LevelUpImageObjRectTransform = new RectTransform[this.m_LevelUpImageObj.Length];
				for (int i = 0; i < this.m_LevelUpImageObj.Length; i++)
				{
					this.m_LevelUpImageObjRectTransform[i] = this.m_LevelUpImageObj[i].GetComponent<RectTransform>();
					this.m_LevelUpImageObjRectTransform[i].localScale = Vector3.one;
					this.m_LevelUpImageObj[i].gameObject.SetActive(false);
				}
			}
			if (this.m_LevelText != null)
			{
				this.m_Outline = this.m_LevelText.GetComponent<Outline>();
			}
		}

		// Token: 0x06002BCC RID: 11212 RVA: 0x000E7154 File Offset: 0x000E5554
		private void Update()
		{
			if (this.m_IsPlaying)
			{
				float num = (float)this.m_NeedExps[this.m_NowLv - this.m_BeforeLv];
				this.m_NowExp += (long)((ulong)((uint)(num * Time.deltaTime) + 1U));
				if (this.m_NowLv == this.m_AfterLv && this.m_NowExp > this.m_AfterExp)
				{
					this.m_NowExp = this.m_AfterExp;
				}
				if (this.m_AfterLv > this.m_NowLv && this.m_NowExp >= this.m_NeedExps[this.m_NowLv - this.m_BeforeLv])
				{
					this.m_NowLv++;
					this.m_NowExp = 0L;
					this.PopUpLevelUp();
				}
				if (this.m_NowLv >= this.m_AfterLv && this.m_NowExp >= this.m_AfterExp)
				{
					this.m_NowLv = this.m_AfterLv;
					this.m_NowExp = this.m_AfterExp;
					this.m_IsPlaying = false;
					this.StopSE();
				}
				this.UpdateExpObj();
			}
			if (this.m_Simulate)
			{
				int totalWidth = 3;
				if (this.m_IsFriendshipMode)
				{
					totalWidth = 2;
				}
				if (this.m_LevelText != null)
				{
					StringBuilder stringBuilder = new StringBuilder();
					int num2 = this.m_NowLv + this.m_SimulateAddLv;
					stringBuilder.Append(num2.ToString().PadLeft(totalWidth));
					if (num2 >= this.m_MaxLevel)
					{
						if (this.m_Outline != null)
						{
							this.m_Outline.enabled = true;
						}
						stringBuilder.Insert(0, "<color=#FFBE41>");
						stringBuilder.Append("</color>");
					}
					else if (num2 > this.m_BeforeLv)
					{
						UIUtility.GetIncreaseString(stringBuilder);
					}
					if (this.m_DisplayMaxLevel)
					{
						stringBuilder.Append(" / ");
						stringBuilder.Append(this.m_MaxLevel.ToString().PadLeft(totalWidth));
					}
					this.m_LevelText.text = stringBuilder.ToString();
				}
				if (this.m_NextText != null)
				{
					StringBuilder stringBuilder2 = new StringBuilder();
					if (this.m_MaxLevel > this.m_BeforeLv + this.m_SimulateAddLv)
					{
						stringBuilder2.Length = 0;
						stringBuilder2.Append((this.m_SimulateNeedExp - this.m_SimulateExp).ToString().PadLeft(9));
						this.m_NextText.text = stringBuilder2.ToString();
					}
					else if (this.m_NextText != null)
					{
						this.m_NextText.text = "--".PadLeft(9);
					}
				}
				float num3 = this.SIMURATE_GAUGE_SPEED * (1f + (float)((int)Mathf.Abs(this.m_SimulateRate - this.m_SimulateRateDisp)) * this.SIMURATE_GAUGE_SPEED_RATE);
				if (this.m_SimulateRateDisp < this.m_SimulateRate)
				{
					this.m_SimulateRateDisp += num3 * Time.deltaTime;
					if (this.m_SimulateRateDisp > this.m_SimulateRate)
					{
						this.m_SimulateRateDisp = this.m_SimulateRate;
					}
				}
				if (this.m_SimulateRateDisp > this.m_SimulateRate)
				{
					this.m_SimulateRateDisp -= num3 * Time.deltaTime;
					if (this.m_SimulateRateDisp < this.m_SimulateRate)
					{
						this.m_SimulateRateDisp = this.m_SimulateRate;
					}
				}
				if (!this.m_ExtendGauge.gameObject.activeSelf)
				{
					this.m_ExtendGauge.gameObject.SetActive(true);
				}
				this.m_ExtendGauge.fillAmount = Mathf.Clamp(this.m_SimulateRateDisp, 0f, 1f);
				if (this.m_SimulateGaugeObj.activeSelf != this.m_SimulateRateDisp >= 1f)
				{
					this.m_SimulateGaugeObj.SetActive(this.m_SimulateRateDisp >= 1f);
				}
				this.m_SimulateGauge.fillAmount = Mathf.Clamp(this.m_SimulateRateDisp - (float)((int)this.m_SimulateRateDisp), 0f, 1f);
			}
			else
			{
				if (this.m_ExtendGauge != null && this.m_ExtendGauge.gameObject.activeSelf)
				{
					this.m_ExtendGauge.gameObject.SetActive(false);
				}
				if (this.m_SimulateGaugeObj != null && this.m_SimulateGaugeObj.activeSelf)
				{
					this.m_SimulateGaugeObj.SetActive(false);
				}
			}
		}

		// Token: 0x06002BCD RID: 11213 RVA: 0x000E75C0 File Offset: 0x000E59C0
		public void Play(bool playSE = true)
		{
			this.m_IsPlaying = true;
			if (this.m_NeedExps == null || (this.m_BeforeLv == this.m_AfterLv && this.m_BeforeExp == this.m_AfterExp))
			{
				this.m_IsPlaying = false;
			}
			if (this.m_IsPlaying && playSE)
			{
				this.PlaySE();
			}
			else
			{
				this.StopSE();
			}
		}

		// Token: 0x06002BCE RID: 11214 RVA: 0x000E762C File Offset: 0x000E5A2C
		private void PlaySE()
		{
			if (this.m_SoundHandler != null)
			{
				this.StopSE();
			}
			this.m_SoundHandler = new SoundHandler();
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_EXP_GAUGE, this.m_SoundHandler, 1f, 0, -1, -1);
		}

		// Token: 0x06002BCF RID: 11215 RVA: 0x000E7678 File Offset: 0x000E5A78
		private void StopSE()
		{
			if (this.m_SoundHandler != null)
			{
				this.m_SoundHandler.Stop();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.RemoveHndl(this.m_SoundHandler, true);
				this.m_SoundHandler = null;
			}
		}

		// Token: 0x06002BD0 RID: 11216 RVA: 0x000E76B0 File Offset: 0x000E5AB0
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
			this.m_RewardExp = 0L;
			this.m_BeforeLv = level;
			this.m_BeforeExp = nowexp;
			this.m_AfterLv = level;
			this.m_AfterExp = nowexp;
			this.m_MaxLevel = maxLevel;
			this.m_NeedExps = new long[1];
			this.m_NeedExps[0] = needExp;
			this.m_NowLv = this.m_BeforeLv;
			this.m_NowExp = this.m_BeforeExp;
			this.UpdateExpObj();
		}

		// Token: 0x06002BD1 RID: 11217 RVA: 0x000E771C File Offset: 0x000E5B1C
		public void SetRewardExp(long rewardExp)
		{
			this.m_RewardExp = rewardExp;
			this.UpdateExpObj();
		}

		// Token: 0x06002BD2 RID: 11218 RVA: 0x000E772C File Offset: 0x000E5B2C
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			this.m_RewardExp = rewardExp;
			this.m_BeforeLv = beforeLevel;
			this.m_BeforeExp = beforeExp;
			this.m_AfterLv = afterLevel;
			this.m_AfterExp = afterExp;
			this.m_MaxLevel = maxLevel;
			this.m_NeedExps = needExp;
			if (this.m_GetText != null)
			{
				this.m_GetText.text = "+" + this.m_RewardExp.ToString();
			}
			this.m_NowLv = this.m_BeforeLv;
			this.m_NowExp = this.m_BeforeExp;
			this.UpdateExpObj();
		}

		// Token: 0x06002BD3 RID: 11219 RVA: 0x000E77C4 File Offset: 0x000E5BC4
		public void SetSimulateGauge(long exp, long needExp, int addLv)
		{
			this.m_SimulateAddLv = addLv;
			this.m_SimulateExp = exp;
			this.m_SimulateNeedExp = needExp;
			this.m_SimulateRate = (float)exp / (float)needExp;
			this.m_Simulate = true;
			this.m_SimulateRate += (float)addLv;
			this.m_SimulateGaugeObj.GetComponent<RectTransform>().sizeDelta = this.m_ExpGaugeBodyObj.GetComponent<RectTransform>().sizeDelta;
			this.m_SimulateGaugeObj.GetComponent<RectTransform>().position = this.m_ExpGaugeBodyObj.GetComponent<RectTransform>().position;
			this.m_ExtendGauge.GetComponent<RectTransform>().sizeDelta = this.m_ExpGaugeBodyObj.GetComponent<RectTransform>().sizeDelta;
			this.m_ExtendGauge.GetComponent<RectTransform>().position = this.m_ExpGaugeBodyObj.GetComponent<RectTransform>().position;
		}

		// Token: 0x06002BD4 RID: 11220 RVA: 0x000E7887 File Offset: 0x000E5C87
		public void SetSimulateOff()
		{
			this.m_SimulateRate = 0f;
			this.m_SimulateRateDisp = 0f;
			this.m_Simulate = false;
		}

		// Token: 0x06002BD5 RID: 11221 RVA: 0x000E78A8 File Offset: 0x000E5CA8
		private void PopUpLevelUp()
		{
			if (this.m_LevelUpImageObj == null || this.m_LevelUpImageObj.Length <= 0)
			{
				return;
			}
			this.m_LevelUpImageObj[this.m_LevelUpPopUpUseIdx].Play();
			this.m_LevelUpImageObjRectTransform[this.m_LevelUpPopUpUseIdx].SetAsLastSibling();
			this.m_LevelUpPopUpUseIdx++;
			if (this.m_LevelUpPopUpUseIdx >= this.m_LevelUpImageObj.Length)
			{
				this.m_LevelUpPopUpUseIdx = 0;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_LV_UP, 1f, 0, -1, -1);
		}

		// Token: 0x06002BD6 RID: 11222 RVA: 0x000E7938 File Offset: 0x000E5D38
		public void HidePopUp()
		{
			for (int i = 0; i < this.m_LevelUpImageObj.Length; i++)
			{
				this.m_LevelUpImageObj[i].gameObject.SetActive(false);
			}
		}

		// Token: 0x06002BD7 RID: 11223 RVA: 0x000E7974 File Offset: 0x000E5D74
		public void SkipUpdateExp()
		{
			if (this.m_NowLv < this.m_AfterLv)
			{
				this.PopUpLevelUp();
			}
			this.m_NowLv = this.m_AfterLv;
			this.m_NowExp = this.m_AfterExp;
			this.m_IsPlaying = false;
			this.StopSE();
			this.UpdateExpObj();
		}

		// Token: 0x06002BD8 RID: 11224 RVA: 0x000E79C4 File Offset: 0x000E5DC4
		private void UpdateExpObj()
		{
			int totalWidth = 3;
			if (this.m_IsFriendshipMode)
			{
				totalWidth = 2;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			if (this.m_LevelText != null)
			{
				if (this.m_NowLv <= 0)
				{
					this.m_LevelText.text = string.Empty;
				}
				else
				{
					stringBuilder.Append(this.m_NowLv.ToString().PadLeft(totalWidth));
					if (this.m_NowLv >= this.m_MaxLevel)
					{
						if (this.m_Outline != null)
						{
							this.m_Outline.enabled = true;
						}
						stringBuilder.Insert(0, "<color=#FFBE41>");
						stringBuilder.Append("</color>");
					}
					else if (this.m_NowLv > this.m_BeforeLv)
					{
						UIUtility.GetIncreaseString(stringBuilder);
					}
					if (this.m_DisplayMaxLevel)
					{
						stringBuilder.Append(" / ");
						stringBuilder.Append(this.m_MaxLevel.ToString().PadLeft(totalWidth));
					}
					this.m_LevelText.text = stringBuilder.ToString();
				}
			}
			if (this.m_MaxLevel > this.m_NowLv)
			{
				int num = this.m_NowLv - this.m_BeforeLv;
				long num2 = this.m_NowExp;
				if (this.m_NeedExps != null)
				{
					if (num >= 0 && num < this.m_NeedExps.Length)
					{
						num2 = this.m_NeedExps[this.m_NowLv - this.m_BeforeLv];
					}
					if (this.m_NextText != null)
					{
						stringBuilder.Length = 0;
						stringBuilder.Append((num2 - this.m_NowExp).ToString().PadLeft(9));
						this.m_NextText.text = stringBuilder.ToString();
					}
				}
				this.m_ExpGaugeBodyObj.fillAmount = (float)this.m_NowExp / (float)num2;
			}
			else
			{
				if (this.m_NextText != null)
				{
					this.m_NextText.text = "--".PadLeft(9);
				}
				this.m_ExpGaugeBodyObj.fillAmount = 1f;
			}
		}

		// Token: 0x06002BD9 RID: 11225 RVA: 0x000E7BE2 File Offset: 0x000E5FE2
		public void SetLevelText(Text text)
		{
			this.m_LevelText = text;
		}

		// Token: 0x04003259 RID: 12889
		private float SIMURATE_GAUGE_SPEED = 3f;

		// Token: 0x0400325A RID: 12890
		private float SIMURATE_GAUGE_SPEED_RATE = 4f;

		// Token: 0x0400325B RID: 12891
		private int m_LevelUpPopUpUseIdx;

		// Token: 0x0400325C RID: 12892
		private int m_MaxLevel;

		// Token: 0x0400325D RID: 12893
		private long m_RewardExp;

		// Token: 0x0400325E RID: 12894
		private long m_BeforeExp;

		// Token: 0x0400325F RID: 12895
		private long m_AfterExp;

		// Token: 0x04003260 RID: 12896
		private long m_NowExp;

		// Token: 0x04003261 RID: 12897
		private int m_BeforeLv;

		// Token: 0x04003262 RID: 12898
		private int m_AfterLv;

		// Token: 0x04003263 RID: 12899
		private int m_NowLv;

		// Token: 0x04003264 RID: 12900
		private long[] m_NeedExps;

		// Token: 0x04003265 RID: 12901
		private bool m_IsPlaying;

		// Token: 0x04003266 RID: 12902
		private SoundHandler m_SoundHandler;

		// Token: 0x04003267 RID: 12903
		[SerializeField]
		private Image m_ExpGaugeBodyObj;

		// Token: 0x04003268 RID: 12904
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x04003269 RID: 12905
		private Outline m_Outline;

		// Token: 0x0400326A RID: 12906
		[SerializeField]
		private Text m_GetText;

		// Token: 0x0400326B RID: 12907
		[SerializeField]
		private Text m_NextText;

		// Token: 0x0400326C RID: 12908
		[SerializeField]
		private bool m_DisplayMaxLevel = true;

		// Token: 0x0400326D RID: 12909
		[SerializeField]
		private bool m_IsFriendshipMode;

		// Token: 0x0400326E RID: 12910
		[SerializeField]
		private LevelUpPop[] m_LevelUpImageObj;

		// Token: 0x0400326F RID: 12911
		private RectTransform[] m_LevelUpImageObjRectTransform;

		// Token: 0x04003270 RID: 12912
		[SerializeField]
		private Image m_ExtendGauge;

		// Token: 0x04003271 RID: 12913
		[SerializeField]
		private GameObject m_SimulateGaugeObj;

		// Token: 0x04003272 RID: 12914
		[SerializeField]
		private Image m_SimulateGauge;

		// Token: 0x04003273 RID: 12915
		private bool m_Simulate;

		// Token: 0x04003274 RID: 12916
		private float m_SimulateRate;

		// Token: 0x04003275 RID: 12917
		private float m_SimulateRateDisp;

		// Token: 0x04003276 RID: 12918
		private int m_SimulateAddLv;

		// Token: 0x04003277 RID: 12919
		private long m_SimulateExp;

		// Token: 0x04003278 RID: 12920
		private long m_SimulateNeedExp;
	}
}
