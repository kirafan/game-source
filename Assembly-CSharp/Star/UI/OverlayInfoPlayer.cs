﻿using System;

namespace Star.UI
{
	// Token: 0x02000A33 RID: 2611
	public class OverlayInfoPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06003656 RID: 13910 RVA: 0x00112749 File Offset: 0x00110B49
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return this.m_SceneID;
		}

		// Token: 0x06003657 RID: 13911 RVA: 0x00112754 File Offset: 0x00110B54
		protected override void Setup(OverlayUIArgBase arg)
		{
			this.m_InfoUI = this.GetUI<InfoUI>();
			InfoUIArg infoUIArg = (InfoUIArg)arg;
			this.m_InfoUI.Setup(infoUIArg.m_StartUrl);
		}

		// Token: 0x06003658 RID: 13912 RVA: 0x00112785 File Offset: 0x00110B85
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
			base.OnClickBackButtonCallBack(isShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x04003CAA RID: 15530
		private SceneDefine.eChildSceneID m_SceneID = SceneDefine.eChildSceneID.InfoUI;

		// Token: 0x04003CAB RID: 15531
		protected InfoUI m_InfoUI;
	}
}
