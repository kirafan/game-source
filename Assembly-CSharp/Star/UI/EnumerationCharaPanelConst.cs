﻿using System;

namespace Star.UI
{
	// Token: 0x02000963 RID: 2403
	public class EnumerationCharaPanelConst : EnumerationCharaPanelStandardBase
	{
		// Token: 0x060031C2 RID: 12738 RVA: 0x000FF8EB File Offset: 0x000FDCEB
		public override void Setup(EnumerationCharaPanelBase.SharedInstance argument)
		{
			base.Setup(argument);
		}

		// Token: 0x060031C3 RID: 12739 RVA: 0x000FF8F4 File Offset: 0x000FDCF4
		public void Apply()
		{
			this.ApplyProcess(this.m_SharedIntance.partyMemberController);
		}
	}
}
