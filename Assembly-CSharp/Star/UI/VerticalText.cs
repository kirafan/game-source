﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008C7 RID: 2247
	public class VerticalText : UIBehaviour, IMeshModifier
	{
		// Token: 0x06002EA3 RID: 11939 RVA: 0x000F4700 File Offset: 0x000F2B00
		protected override void Start()
		{
			base.Start();
			this.m_CacheText = base.GetComponent<Text>();
			this.m_NoRotateCharacter = new List<char>();
			this.m_NoRotateCharacter.Add('ー');
			this.m_NoRotateCharacter.Add('－');
			this.m_NoRotateCharacter.Add('-');
			this.m_NoRotateCharacter.Add('―');
			this.m_NoRotateCharacter.Add('‐');
			this.m_NoRotateCharacter.Add('…');
			this.m_NoRotateCharacter.Add('「');
			this.m_NoRotateCharacter.Add('」');
			this.m_NoRotateCharacter.Add('『');
			this.m_NoRotateCharacter.Add('』');
			this.m_ShiftCharacter = new List<char>();
			this.m_ShiftCharacter.Add('ぁ');
			this.m_ShiftCharacter.Add('ァ');
			this.m_ShiftCharacter.Add('ぃ');
			this.m_ShiftCharacter.Add('ィ');
			this.m_ShiftCharacter.Add('ぅ');
			this.m_ShiftCharacter.Add('ゥ');
			this.m_ShiftCharacter.Add('ぇ');
			this.m_ShiftCharacter.Add('ェ');
			this.m_ShiftCharacter.Add('ぉ');
			this.m_ShiftCharacter.Add('ォ');
			this.m_ShiftCharacter.Add('ゃ');
			this.m_ShiftCharacter.Add('ャ');
			this.m_ShiftCharacter.Add('ゅ');
			this.m_ShiftCharacter.Add('ュ');
			this.m_ShiftCharacter.Add('ょ');
			this.m_ShiftCharacter.Add('ョ');
			this.m_VertexList = new List<UIVertex>();
			Graphic component = base.GetComponent<Graphic>();
			if (component != null)
			{
				component.SetVerticesDirty();
			}
		}

		// Token: 0x06002EA4 RID: 11940 RVA: 0x000F48F6 File Offset: 0x000F2CF6
		public void ModifyMesh(Mesh mesh)
		{
		}

		// Token: 0x06002EA5 RID: 11941 RVA: 0x000F48F8 File Offset: 0x000F2CF8
		public void ModifyMesh(VertexHelper verts)
		{
			if (!this.IsActive())
			{
				return;
			}
			verts.GetUIVertexStream(this.m_VertexList);
			if (this.m_VertexList != null)
			{
				this.Modify(this.m_VertexList);
				verts.Clear();
				verts.AddUIVertexTriangleStream(this.m_VertexList);
			}
		}

		// Token: 0x06002EA6 RID: 11942 RVA: 0x000F4948 File Offset: 0x000F2D48
		private void Modify(List<UIVertex> vertexList)
		{
			for (int i = 0; i < vertexList.Count; i += 6)
			{
				int num = i / 6;
				bool flag = false;
				for (int j = 0; j < this.m_NoRotateCharacter.Count; j++)
				{
					if (this.m_NoRotateCharacter[j] == this.m_CacheText.text.ToCharArray()[num])
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					for (int k = 0; k < this.m_ShiftCharacter.Count; k++)
					{
						if (this.m_ShiftCharacter[k] == this.m_CacheText.text.ToCharArray()[num])
						{
							flag = true;
							break;
						}
					}
					Vector2 vector = Vector2.Lerp(vertexList[i].position, vertexList[i + 3].position, 0.5f);
					for (int l = 0; l < 6; l++)
					{
						UIVertex value = vertexList[i + l];
						Vector3 vector2 = value.position - vector;
						Vector2 a = new Vector2(vector2.x * Mathf.Cos(1.5707964f) - vector2.y * Mathf.Sin(1.5707964f), vector2.x * Mathf.Sin(1.5707964f) + vector2.y * Mathf.Cos(1.5707964f));
						value.position = a + vector;
						if (flag)
						{
							value.position.y = value.position.y - (vertexList[0].position.y - vector.y) * 0.5f;
						}
						vertexList[i + l] = value;
					}
				}
			}
		}

		// Token: 0x040035C3 RID: 13763
		private Text m_CacheText;

		// Token: 0x040035C4 RID: 13764
		private List<char> m_NoRotateCharacter;

		// Token: 0x040035C5 RID: 13765
		private List<char> m_ShiftCharacter;

		// Token: 0x040035C6 RID: 13766
		private List<UIVertex> m_VertexList;
	}
}
