﻿using System;

namespace Star.UI
{
	// Token: 0x020008D0 RID: 2256
	public class iTweenParameter
	{
		// Token: 0x06002EBC RID: 11964 RVA: 0x000F4F59 File Offset: 0x000F3359
		public iTweenParameter(string in_paramName, float in_value)
		{
			this.paramName = in_paramName;
			this.value = in_value;
		}

		// Token: 0x040035CE RID: 13774
		public string paramName;

		// Token: 0x040035CF RID: 13775
		public float value;
	}
}
