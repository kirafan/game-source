﻿using System;

namespace Star.UI
{
	// Token: 0x0200096B RID: 2411
	public abstract class EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterBase where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x060031E8 RID: 12776 RVA: 0x000FD850 File Offset: 0x000FBC50
		public override void Destroy()
		{
			if (this.m_CharaPanels != null)
			{
				for (int i = 0; i < this.m_CharaPanels.Length; i++)
				{
					this.m_CharaPanels[i].Destroy();
				}
			}
		}

		// Token: 0x060031E9 RID: 12777 RVA: 0x000FD89C File Offset: 0x000FBC9C
		public override void Update()
		{
			if (!base.AcquirePanelsArray())
			{
				return;
			}
			for (int i = 0; i < base.GetInstantiatePerFrame(); i++)
			{
				if (this.m_CharaPanelNum < base.HowManyAllChildren() && this.m_CharaPanels[this.m_CharaPanelNum] == null)
				{
					this.m_CharaPanels[this.m_CharaPanelNum] = this.InstantiateProcess();
					this.ApplyCharaData(this.m_CharaPanelNum);
					this.m_CharaPanelNum++;
				}
			}
		}

		// Token: 0x060031EA RID: 12778 RVA: 0x000FD92F File Offset: 0x000FBD2F
		protected override CharaPanelType InstantiateProcess()
		{
			return (CharaPanelType)((object)null);
		}

		// Token: 0x060031EB RID: 12779 RVA: 0x000FD937 File Offset: 0x000FBD37
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
		}

		// Token: 0x0200096C RID: 2412
		[Serializable]
		public class SharedInstanceEx<ThisType> : EnumerationPanelCoreBase.SharedInstanceBaseExGen<ThisType, ParentType, CharaPanelType> where ThisType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<ThisType>
		{
			// Token: 0x060031EC RID: 12780 RVA: 0x000FD939 File Offset: 0x000FBD39
			public SharedInstanceEx()
			{
			}

			// Token: 0x060031ED RID: 12781 RVA: 0x000FD941 File Offset: 0x000FBD41
			public SharedInstanceEx(ThisType argument) : base(argument)
			{
				this.CloneNewMemberOnly(argument);
			}

			// Token: 0x060031EE RID: 12782 RVA: 0x000FD954 File Offset: 0x000FBD54
			public override ThisType Clone(ThisType argument)
			{
				ThisType thisType = base.Clone(argument);
				return thisType.CloneNewMemberOnly(argument);
			}

			// Token: 0x060031EF RID: 12783 RVA: 0x000FD977 File Offset: 0x000FBD77
			private ThisType CloneNewMemberOnly(ThisType argument)
			{
				return (ThisType)((object)this);
			}
		}
	}
}
