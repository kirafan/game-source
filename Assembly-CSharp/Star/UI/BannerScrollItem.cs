﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009B5 RID: 2485
	public class BannerScrollItem : InfiniteScrollItem
	{
		// Token: 0x0600339B RID: 13211 RVA: 0x00106068 File Offset: 0x00104468
		public override void Setup()
		{
		}

		// Token: 0x0600339C RID: 13212 RVA: 0x0010606C File Offset: 0x0010446C
		private void Update()
		{
			if (this.m_Data != null && this.m_Image.sprite == null)
			{
				BannerScrollItemData bannerScrollItemData = (BannerScrollItemData)this.m_Data;
				if (bannerScrollItemData.m_Sprite != null)
				{
					this.m_Image.sprite = bannerScrollItemData.m_Sprite;
				}
			}
		}

		// Token: 0x0600339D RID: 13213 RVA: 0x001060C8 File Offset: 0x001044C8
		public override void Apply(InfiniteScrollItemData data)
		{
			base.Apply(data);
			this.m_Image.sprite = null;
			BannerScrollItemData bannerScrollItemData = (BannerScrollItemData)this.m_Data;
			if (bannerScrollItemData.m_InfoID == -1)
			{
				this.m_Image.sprite = this.m_TopSprite;
			}
			else if (bannerScrollItemData.m_Sprite != null)
			{
				this.m_Image.sprite = bannerScrollItemData.m_Sprite;
			}
		}

		// Token: 0x0600339E RID: 13214 RVA: 0x00106138 File Offset: 0x00104538
		public override void Destroy()
		{
			base.Destroy();
			this.m_Image.sprite = null;
		}

		// Token: 0x0600339F RID: 13215 RVA: 0x0010614C File Offset: 0x0010454C
		public void OnClickCallBack()
		{
			BannerScrollItemData bannerScrollItemData = (BannerScrollItemData)this.m_Data;
			if (bannerScrollItemData.m_InfoID == -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Info, new InfoUIArg(null), null, null);
			}
			else if (bannerScrollItemData.m_Url != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Info, new InfoUIArg(bannerScrollItemData.m_Url), null, null);
			}
		}

		// Token: 0x040039D6 RID: 14806
		[SerializeField]
		private Image m_Image;

		// Token: 0x040039D7 RID: 14807
		[SerializeField]
		private Sprite m_TopSprite;
	}
}
