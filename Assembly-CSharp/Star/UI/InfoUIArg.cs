﻿using System;

namespace Star.UI
{
	// Token: 0x02000A32 RID: 2610
	public class InfoUIArg : OverlayUIArgBase
	{
		// Token: 0x06003654 RID: 13908 RVA: 0x0011272A File Offset: 0x00110B2A
		public InfoUIArg(string url)
		{
			this.m_StartUrl = url;
		}

		// Token: 0x04003CA9 RID: 15529
		public string m_StartUrl;
	}
}
