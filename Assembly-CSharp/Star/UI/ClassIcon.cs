﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200085E RID: 2142
	public class ClassIcon : MonoBehaviour
	{
		// Token: 0x06002C8D RID: 11405 RVA: 0x000EB568 File Offset: 0x000E9968
		public void Apply(eClassType classType)
		{
			this.m_IconObj = base.GetComponent<Image>();
			switch (classType)
			{
			case eClassType.Fighter:
				this.m_IconObj.sprite = this.m_Fighter;
				break;
			case eClassType.Magician:
				this.m_IconObj.sprite = this.m_Magician;
				break;
			case eClassType.Priest:
				this.m_IconObj.sprite = this.m_Priest;
				break;
			case eClassType.Knight:
				this.m_IconObj.sprite = this.m_Knight;
				break;
			case eClassType.Alchemist:
				this.m_IconObj.sprite = this.m_Alchemist;
				break;
			default:
				this.m_IconObj.sprite = null;
				break;
			}
		}

		// Token: 0x06002C8E RID: 11406 RVA: 0x000EB61F File Offset: 0x000E9A1F
		public void SetActive(bool flag)
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			this.m_GameObject.SetActive(flag);
		}

		// Token: 0x04003377 RID: 13175
		[SerializeField]
		private Sprite m_Fighter;

		// Token: 0x04003378 RID: 13176
		[SerializeField]
		private Sprite m_Magician;

		// Token: 0x04003379 RID: 13177
		[SerializeField]
		private Sprite m_Priest;

		// Token: 0x0400337A RID: 13178
		[SerializeField]
		private Sprite m_Knight;

		// Token: 0x0400337B RID: 13179
		[SerializeField]
		private Sprite m_Alchemist;

		// Token: 0x0400337C RID: 13180
		private Image m_IconObj;

		// Token: 0x0400337D RID: 13181
		private GameObject m_GameObject;
	}
}
