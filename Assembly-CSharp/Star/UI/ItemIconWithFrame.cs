﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000865 RID: 2149
	public class ItemIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002C9E RID: 11422 RVA: 0x000EB9B4 File Offset: 0x000E9DB4
		public void Apply(int itemID)
		{
			if (itemID == -1)
			{
				this.m_Icon.Destroy();
				this.m_RareStar.Apply(eRare.None);
				return;
			}
			this.m_Icon.Apply(itemID);
			this.m_RareStar.Apply((eRare)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(itemID).m_Rare);
		}

		// Token: 0x06002C9F RID: 11423 RVA: 0x000EBA14 File Offset: 0x000E9E14
		public void Destroy()
		{
			if (this.m_Icon != null)
			{
				this.m_Icon.Destroy();
			}
		}

		// Token: 0x06002CA0 RID: 11424 RVA: 0x000EBA32 File Offset: 0x000E9E32
		public void SetEnableRenderItemIcon(bool flg)
		{
			this.m_Icon.gameObject.SetActive(flg);
		}

		// Token: 0x0400339D RID: 13213
		[SerializeField]
		private ItemIcon m_Icon;

		// Token: 0x0400339E RID: 13214
		[SerializeField]
		private RareStar m_RareStar;
	}
}
