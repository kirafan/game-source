﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000A26 RID: 2598
	public class MasterDisplayUI : MenuUIBase
	{
		// Token: 0x060035C3 RID: 13763 RVA: 0x00110715 File Offset: 0x0010EB15
		public void SetCompletePlayInCallBack(Action action)
		{
			this.m_OnCompletePlayIn = action;
		}

		// Token: 0x060035C4 RID: 13764 RVA: 0x0011071E File Offset: 0x0010EB1E
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
		}

		// Token: 0x060035C5 RID: 13765 RVA: 0x0011074A File Offset: 0x0010EB4A
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrder, int offset)
		{
			base.GetComponent<FixSortOrder>().SetSortOrder(sortOrder, offset);
		}

		// Token: 0x060035C6 RID: 13766 RVA: 0x0011075C File Offset: 0x0010EB5C
		private void Update()
		{
			switch (this.m_Step)
			{
			case MasterDisplayUI.eStep.In:
				if (this.m_MasterIllust.IsDoneLoad())
				{
					if (this.m_MasterAnim.State == 0 || this.m_MasterAnim.State == 3)
					{
						this.m_MasterAnim.PlayIn();
					}
					if (this.m_MasterAnim.State == 2)
					{
						this.m_OnCompletePlayIn.Call();
						this.ChangeStep(MasterDisplayUI.eStep.Idle);
					}
				}
				break;
			case MasterDisplayUI.eStep.Out:
				if (this.m_MasterIllust.IsDoneLoad())
				{
					if (this.m_MasterAnim.State == 2 || this.m_MasterAnim.State == 1)
					{
						this.m_MasterAnim.PlayOut();
					}
					if (this.m_MasterAnim.State == 0)
					{
						this.m_MasterIllust.Destroy();
						this.ChangeStep(MasterDisplayUI.eStep.End);
					}
				}
				break;
			}
		}

		// Token: 0x060035C7 RID: 13767 RVA: 0x00110868 File Offset: 0x0010EC68
		private void ChangeStep(MasterDisplayUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MasterDisplayUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case MasterDisplayUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MasterDisplayUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MasterDisplayUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MasterDisplayUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060035C8 RID: 13768 RVA: 0x00110910 File Offset: 0x0010ED10
		public override void PlayIn()
		{
			this.ChangeStep(MasterDisplayUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MasterIllust.ApplyCurrentEquip();
		}

		// Token: 0x060035C9 RID: 13769 RVA: 0x00110958 File Offset: 0x0010ED58
		public override void PlayOut()
		{
			this.ChangeStep(MasterDisplayUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x060035CA RID: 13770 RVA: 0x00110992 File Offset: 0x0010ED92
		public override bool IsMenuEnd()
		{
			return this.m_Step == MasterDisplayUI.eStep.End;
		}

		// Token: 0x060035CB RID: 13771 RVA: 0x0011099D File Offset: 0x0010ED9D
		public bool IsIdle()
		{
			return this.m_Step == MasterDisplayUI.eStep.Idle;
		}

		// Token: 0x04003C5B RID: 15451
		private MasterDisplayUI.eStep m_Step;

		// Token: 0x04003C5C RID: 15452
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003C5D RID: 15453
		[SerializeField]
		private RectTransform m_OffsetObj;

		// Token: 0x04003C5E RID: 15454
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x04003C5F RID: 15455
		[SerializeField]
		private AnimUIPlayer m_MasterAnim;

		// Token: 0x04003C60 RID: 15456
		private Action m_OnCompletePlayIn;

		// Token: 0x02000A27 RID: 2599
		private enum eStep
		{
			// Token: 0x04003C62 RID: 15458
			Hide,
			// Token: 0x04003C63 RID: 15459
			In,
			// Token: 0x04003C64 RID: 15460
			Idle,
			// Token: 0x04003C65 RID: 15461
			Out,
			// Token: 0x04003C66 RID: 15462
			End
		}
	}
}
