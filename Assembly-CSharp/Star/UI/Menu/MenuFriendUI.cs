﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009DE RID: 2526
	public class MenuFriendUI : MenuFriendUIBase
	{
		// Token: 0x14000098 RID: 152
		// (add) Token: 0x0600345A RID: 13402 RVA: 0x00109BFC File Offset: 0x00107FFC
		// (remove) Token: 0x0600345B RID: 13403 RVA: 0x00109C34 File Offset: 0x00108034
		public event Action OnFriendRoom;

		// Token: 0x0600345C RID: 13404 RVA: 0x00109C6C File Offset: 0x0010806C
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_ListView.Setup();
			this.m_PendingListView.Setup();
			this.m_TabGroup.Setup(-1f);
			this.m_FindInput.text = string.Empty;
			this.m_UserCode.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode;
			SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_GetAll(FriendDefine.eGetAllType.Friend, new Action<bool>(this.OnResponse_GetAll));
			this.m_FindButton.Interactable = false;
			this.m_Sort = false;
		}

		// Token: 0x0600345D RID: 13405 RVA: 0x00109D0A File Offset: 0x0010810A
		public void OnClickSortButton()
		{
			this.SetSort((!this.m_Sort) ? MenuFriendUIBase.eListSortOrderType.Asc : MenuFriendUIBase.eListSortOrderType.Desc);
		}

		// Token: 0x0600345E RID: 13406 RVA: 0x00109D24 File Offset: 0x00108124
		public void SetSort(MenuFriendUIBase.eListSortOrderType order)
		{
			if (order == MenuFriendUIBase.eListSortOrderType.Invalid)
			{
				return;
			}
			this.m_Sort = (order == MenuFriendUIBase.eListSortOrderType.Asc);
			this.OnClickCategoryButtonCallBack();
		}

		// Token: 0x0600345F RID: 13407 RVA: 0x00109D47 File Offset: 0x00108147
		private void OnResponse_GetAll(bool isError)
		{
			this.RefreshFriendList();
			if (!isError)
			{
				this.PlayIn();
			}
			else
			{
				this.PlayOut();
			}
		}

		// Token: 0x06003460 RID: 13408 RVA: 0x00109D68 File Offset: 0x00108168
		private void RefreshFriendList()
		{
			FriendManager friendMng = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng;
			int num = 0;
			int num2 = 0;
			base.ClearList(MenuFriendUIBase.eFriendListType.Friend);
			base.ClearList(MenuFriendUIBase.eFriendListType.Pending);
			for (int i = 0; i < friendMng.GetFriendListNum(); i++)
			{
				FriendManager.FriendData friendDataAt = friendMng.GetFriendDataAt(i);
				IFriendUIData from = new IFriendUIData(friendDataAt, new Action<MenuFriendListItem.eAction>(this.OnAction));
				FriendDefine.eType type = friendDataAt.m_Type;
				if (type != FriendDefine.eType.Friend)
				{
					if (type == FriendDefine.eType.OpponentPropose || type == FriendDefine.eType.SelfPropose)
					{
						num2++;
						base.AddList(MenuFriendUIBase.eFriendListType.Pending, from);
					}
				}
				else
				{
					base.AddList(MenuFriendUIBase.eFriendListType.Friend, from);
					num++;
				}
			}
			if (this.m_FriendNum != null)
			{
				this.m_FriendNum.text = num.ToString() + "/" + UIUtility.GetFriendListMax();
			}
			this.OnClickCategoryButtonCallBack();
		}

		// Token: 0x06003461 RID: 13409 RVA: 0x00109E54 File Offset: 0x00108254
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuFriendUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuFriendUI.eStep.Idle);
				}
				break;
			}
			case MenuFriendUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuFriendUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003462 RID: 13410 RVA: 0x00109F40 File Offset: 0x00108340
		private void ChangeStep(MenuFriendUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuFriendUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuFriendUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuFriendUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuFriendUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuFriendUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003463 RID: 13411 RVA: 0x00109FEC File Offset: 0x001083EC
		public override void PlayIn()
		{
			this.ChangeStep(MenuFriendUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003464 RID: 13412 RVA: 0x0010A034 File Offset: 0x00108434
		public override void PlayOut()
		{
			this.ChangeStep(MenuFriendUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003465 RID: 13413 RVA: 0x0010A079 File Offset: 0x00108479
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuFriendUI.eStep.End;
		}

		// Token: 0x06003466 RID: 13414 RVA: 0x0010A084 File Offset: 0x00108484
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06003467 RID: 13415 RVA: 0x0010A086 File Offset: 0x00108486
		public void OnCopyButtonCallBack()
		{
			Clipboard.value = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode;
		}

		// Token: 0x06003468 RID: 13416 RVA: 0x0010A0A1 File Offset: 0x001084A1
		public void OnEndEditInputField()
		{
			this.m_FindButton.Interactable = (this.m_FindInput.text.Length == 10);
		}

		// Token: 0x06003469 RID: 13417 RVA: 0x0010A0C4 File Offset: 0x001084C4
		public void OnFindButtonCallBack()
		{
			string text = this.m_FindInput.text.ToUpper();
			if (text == SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode.ToUpper())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.FriendConfirmSearchTitle, eText_MessageDB.FriendConfirmSearchOwnID, null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Search(text, new Action<bool>(this.OnResponse_Search));
			}
		}

		// Token: 0x0600346A RID: 13418 RVA: 0x0010A144 File Offset: 0x00108544
		private void OnResponse_Search(bool isError)
		{
			if (!isError)
			{
				if (this.m_FindResultCloner != null)
				{
					this.m_FindPlayerData = this.m_FindResultCloner.GetInst<MenuFriendListItem>();
				}
				this.m_FindPlayer = new IFriendUIData(SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.SearchedFriendData, new Action<MenuFriendListItem.eAction>(this.OnAction));
				this.m_FindPlayerData.gameObject.SetActive(true);
				this.m_FindPlayerData.SetItemData(this.m_FindPlayer);
			}
			else if (this.m_FindPlayerData != null)
			{
				this.m_FindPlayerData.gameObject.SetActive(false);
			}
		}

		// Token: 0x0600346B RID: 13419 RVA: 0x0010A1E8 File Offset: 0x001085E8
		public void OnClickCategoryButtonCallBack()
		{
			for (int i = 0; i < this.m_TabSelWindow.Length; i++)
			{
				if (i == this.m_TabGroup.SelectIdx)
				{
					this.m_TabSelWindow[i].SetActive(true);
				}
				else
				{
					this.m_TabSelWindow[i].SetActive(false);
				}
			}
			this.SetUpBoardItem(this.m_TabGroup.SelectIdx);
		}

		// Token: 0x0600346C RID: 13420 RVA: 0x0010A254 File Offset: 0x00108654
		private void SetUpBoardItem(int ftabno)
		{
			ScrollViewBase scrollViewBase = null;
			MenuFriendUIBase.eFriendListType type;
			switch (ftabno)
			{
			case 0:
				type = MenuFriendUIBase.eFriendListType.Friend;
				scrollViewBase = this.m_ListView;
				this.m_SortButton.gameObject.SetActive(true);
				goto IL_6F;
			case 1:
				type = MenuFriendUIBase.eFriendListType.Pending;
				scrollViewBase = this.m_PendingListView;
				this.m_SortButton.gameObject.SetActive(true);
				goto IL_6F;
			}
			this.m_SortButton.gameObject.SetActive(false);
			type = MenuFriendUIBase.eFriendListType.Invalid;
			IL_6F:
			if (scrollViewBase != null)
			{
				this.m_SortASC.SetActive(!this.m_Sort);
				this.m_SortDSC.SetActive(this.m_Sort);
				int num = base.HowManyListItems(type);
				scrollViewBase.RemoveAll();
				if (this.m_Sort)
				{
					for (int i = 0; i < num; i++)
					{
						scrollViewBase.AddItem(base.GetListItem(type, i));
					}
				}
				else
				{
					for (int i = num - 1; i >= 0; i--)
					{
						scrollViewBase.AddItem(base.GetListItem(type, i));
					}
				}
			}
		}

		// Token: 0x0600346D RID: 13421 RVA: 0x0010A361 File Offset: 0x00108761
		private void OnAction(MenuFriendListItem.eAction action)
		{
			if (action == MenuFriendListItem.eAction.RoomIn)
			{
				this.OnFriendRoom.Call();
			}
			else
			{
				this.RefreshFriendList();
				if (this.m_FindPlayerData != null)
				{
					this.m_FindPlayerData.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x04003AD5 RID: 15061
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003AD6 RID: 15062
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003AD7 RID: 15063
		[SerializeField]
		private CustomButton m_FindButton;

		// Token: 0x04003AD8 RID: 15064
		[SerializeField]
		private CustomButton m_SortButton;

		// Token: 0x04003AD9 RID: 15065
		[SerializeField]
		private GameObject m_SortASC;

		// Token: 0x04003ADA RID: 15066
		[SerializeField]
		private GameObject m_SortDSC;

		// Token: 0x04003ADB RID: 15067
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04003ADC RID: 15068
		[SerializeField]
		private ScrollViewBase m_ListView;

		// Token: 0x04003ADD RID: 15069
		[SerializeField]
		private ScrollViewBase m_PendingListView;

		// Token: 0x04003ADE RID: 15070
		[SerializeField]
		private InputField m_FindInput;

		// Token: 0x04003ADF RID: 15071
		[SerializeField]
		private Text m_FriendNum;

		// Token: 0x04003AE0 RID: 15072
		[SerializeField]
		private GameObject[] m_TabSelWindow;

		// Token: 0x04003AE1 RID: 15073
		private long m_FindPlayerID;

		// Token: 0x04003AE2 RID: 15074
		[SerializeField]
		private PrefabCloner m_FindResultCloner;

		// Token: 0x04003AE3 RID: 15075
		[SerializeField]
		private Text m_UserCode;

		// Token: 0x04003AE4 RID: 15076
		private bool m_Sort;

		// Token: 0x04003AE5 RID: 15077
		private MenuFriendListItem m_FindPlayerData;

		// Token: 0x04003AE6 RID: 15078
		private MenuFriendUI.eStep m_Step;

		// Token: 0x04003AE7 RID: 15079
		private IFriendUIData m_FindPlayer;

		// Token: 0x020009DF RID: 2527
		private enum eStep
		{
			// Token: 0x04003AEA RID: 15082
			Hide,
			// Token: 0x04003AEB RID: 15083
			Wait,
			// Token: 0x04003AEC RID: 15084
			In,
			// Token: 0x04003AED RID: 15085
			Idle,
			// Token: 0x04003AEE RID: 15086
			Out,
			// Token: 0x04003AEF RID: 15087
			End
		}
	}
}
