﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x02000A12 RID: 2578
	public class MenuUI : MenuUIBase
	{
		// Token: 0x06003550 RID: 13648 RVA: 0x0010E86F File Offset: 0x0010CC6F
		public void Setup()
		{
		}

		// Token: 0x06003551 RID: 13649 RVA: 0x0010E871 File Offset: 0x0010CC71
		private void Update()
		{
		}

		// Token: 0x06003552 RID: 13650 RVA: 0x0010E873 File Offset: 0x0010CC73
		public override void PlayIn()
		{
		}

		// Token: 0x06003553 RID: 13651 RVA: 0x0010E875 File Offset: 0x0010CC75
		public override void PlayOut()
		{
		}

		// Token: 0x06003554 RID: 13652 RVA: 0x0010E877 File Offset: 0x0010CC77
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x06003555 RID: 13653 RVA: 0x0010E87F File Offset: 0x0010CC7F
		public override bool IsMenuEnd()
		{
			return false;
		}

		// Token: 0x06003556 RID: 13654 RVA: 0x0010E882 File Offset: 0x0010CC82
		public void SetEnableObjInput(bool flg)
		{
		}
	}
}
