﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x02000A00 RID: 2560
	public class MenuNoticeItemData : ScrollItemData
	{
		// Token: 0x060034FF RID: 13567 RVA: 0x0010D0B0 File Offset: 0x0010B4B0
		public MenuNoticeItemData(NotificationController.ePush push, string title, bool flg, bool interactable)
		{
			this.m_Push = push;
			this.m_Title = title;
			this.m_Flag = flg;
			this.m_Interactable = interactable;
		}

		// Token: 0x140000A7 RID: 167
		// (add) Token: 0x06003500 RID: 13568 RVA: 0x0010D0D8 File Offset: 0x0010B4D8
		// (remove) Token: 0x06003501 RID: 13569 RVA: 0x0010D110 File Offset: 0x0010B510
		public event Action<NotificationController.ePush, bool> OnSwitch;

		// Token: 0x06003502 RID: 13570 RVA: 0x0010D146 File Offset: 0x0010B546
		public void OnClickCallBack()
		{
			this.OnSwitch.Call(this.m_Push, this.m_Flag);
		}

		// Token: 0x04003B97 RID: 15255
		public NotificationController.ePush m_Push;

		// Token: 0x04003B98 RID: 15256
		public string m_Title;

		// Token: 0x04003B99 RID: 15257
		public bool m_Flag;

		// Token: 0x04003B9A RID: 15258
		public bool m_Interactable;
	}
}
