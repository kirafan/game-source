﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000A2A RID: 2602
	public class OptionMenuItem : MonoBehaviour
	{
		// Token: 0x17000330 RID: 816
		// (get) Token: 0x060035DA RID: 13786 RVA: 0x00110E4F File Offset: 0x0010F24F
		public OptionMenuItem.eType Type
		{
			get
			{
				return this.m_Type;
			}
		}

		// Token: 0x140000AE RID: 174
		// (add) Token: 0x060035DB RID: 13787 RVA: 0x00110E58 File Offset: 0x0010F258
		// (remove) Token: 0x060035DC RID: 13788 RVA: 0x00110E90 File Offset: 0x0010F290
		public event Action OnValueChanged;

		// Token: 0x060035DD RID: 13789 RVA: 0x00110EC6 File Offset: 0x0010F2C6
		private void Start()
		{
			if (this.m_Slider != null)
			{
				this.m_Slider.OnValueChanged += this.OnValueChangedCallBack;
			}
		}

		// Token: 0x060035DE RID: 13790 RVA: 0x00110EF0 File Offset: 0x0010F2F0
		public void RegisterSliderStartingNormalizedValue(float value)
		{
			this.m_SliderStartingValue = new float?(value);
			this.SetSliderNormalizedValue(value);
		}

		// Token: 0x060035DF RID: 13791 RVA: 0x00110F05 File Offset: 0x0010F305
		public void RegisterSliderDefaultNormalizedValue(float value)
		{
			this.m_SliderDefaultValue = new float?(value);
		}

		// Token: 0x060035E0 RID: 13792 RVA: 0x00110F13 File Offset: 0x0010F313
		public void RegisterToggleStartingValue(bool value)
		{
			this.m_ToggleStartingValue = new bool?(value);
			this.m_ToggleSwitch.SetDecided(value);
		}

		// Token: 0x060035E1 RID: 13793 RVA: 0x00110F2D File Offset: 0x0010F32D
		public void RegisterToggleDefaultValue(bool value)
		{
			this.m_ToggleDefaultValue = new bool?(value);
		}

		// Token: 0x060035E2 RID: 13794 RVA: 0x00110F3C File Offset: 0x0010F33C
		public void Revert()
		{
			float? sliderStartingValue = this.m_SliderStartingValue;
			if (sliderStartingValue != null && this.m_Slider.Value != this.m_SliderStartingValue.Value)
			{
				this.SetSliderNormalizedValue(this.m_SliderStartingValue.Value);
			}
			bool? toggleStartingValue = this.m_ToggleStartingValue;
			if (toggleStartingValue != null && this.m_ToggleSwitch.IsOn() != this.m_ToggleStartingValue.Value)
			{
				this.SetToggleValue(this.m_ToggleStartingValue.Value);
			}
		}

		// Token: 0x060035E3 RID: 13795 RVA: 0x00110FC8 File Offset: 0x0010F3C8
		public void Restore()
		{
			float? sliderDefaultValue = this.m_SliderDefaultValue;
			if (sliderDefaultValue != null && this.m_Slider.Value != this.m_SliderDefaultValue.Value)
			{
				this.SetSliderNormalizedValue(this.m_SliderDefaultValue.Value);
			}
			bool? toggleDefaultValue = this.m_ToggleDefaultValue;
			if (toggleDefaultValue != null && this.m_ToggleSwitch.IsOn() != this.m_ToggleDefaultValue.Value)
			{
				this.SetToggleValue(this.m_ToggleDefaultValue.Value);
			}
		}

		// Token: 0x060035E4 RID: 13796 RVA: 0x00111054 File Offset: 0x0010F454
		public bool IsMatchValueAndStartingValue()
		{
			float? sliderStartingValue = this.m_SliderStartingValue;
			if (sliderStartingValue != null && this.m_Slider.NormalizedValue != this.m_SliderStartingValue.Value)
			{
				return false;
			}
			bool? toggleStartingValue = this.m_ToggleStartingValue;
			return toggleStartingValue == null || this.m_ToggleSwitch.IsOn() == this.m_ToggleStartingValue.Value;
		}

		// Token: 0x060035E5 RID: 13797 RVA: 0x001110C4 File Offset: 0x0010F4C4
		public bool IsMatchValueAndDefaultValue()
		{
			float? sliderDefaultValue = this.m_SliderDefaultValue;
			if (sliderDefaultValue != null && this.m_Slider.NormalizedValue != this.m_SliderDefaultValue.Value)
			{
				return false;
			}
			bool? toggleDefaultValue = this.m_ToggleDefaultValue;
			return toggleDefaultValue == null || this.m_ToggleSwitch.IsOn() == this.m_ToggleDefaultValue.Value;
		}

		// Token: 0x060035E6 RID: 13798 RVA: 0x00111132 File Offset: 0x0010F532
		public void SetTitleText(string text)
		{
			this.m_TitleText.text = text;
		}

		// Token: 0x060035E7 RID: 13799 RVA: 0x00111140 File Offset: 0x0010F540
		public float GetSliderNormalizedValue()
		{
			if (this.m_Type != OptionMenuItem.eType.Slider || this.m_Slider == null)
			{
				return 0f;
			}
			return this.m_Slider.NormalizedValue;
		}

		// Token: 0x060035E8 RID: 13800 RVA: 0x00111170 File Offset: 0x0010F570
		public float GetSliderValue()
		{
			if (this.m_Type != OptionMenuItem.eType.Slider || this.m_Slider == null)
			{
				return 0f;
			}
			return this.m_Slider.Value;
		}

		// Token: 0x060035E9 RID: 13801 RVA: 0x001111A0 File Offset: 0x0010F5A0
		public void SetSliderNormalizedValue(float value)
		{
			if (this.m_Type != OptionMenuItem.eType.Slider || this.m_Slider == null)
			{
			}
			this.m_Slider.NormalizedValue = value;
		}

		// Token: 0x060035EA RID: 13802 RVA: 0x001111CB File Offset: 0x0010F5CB
		[Obsolete("GetとSetで関数名のフォーマットが異なるので.")]
		public void SetNormalizedSliderValue(float value)
		{
			this.SetSliderNormalizedValue(value);
		}

		// Token: 0x060035EB RID: 13803 RVA: 0x001111D4 File Offset: 0x0010F5D4
		public void SetSliderValue(float value)
		{
			if (this.m_Type != OptionMenuItem.eType.Slider || this.m_Slider == null)
			{
			}
			this.m_Slider.Value = value;
		}

		// Token: 0x060035EC RID: 13804 RVA: 0x001111FF File Offset: 0x0010F5FF
		public bool GetToggleValue()
		{
			return this.m_Type == OptionMenuItem.eType.Toggle && !(this.m_ToggleSwitch == null) && this.m_ToggleSwitch.IsOn();
		}

		// Token: 0x060035ED RID: 13805 RVA: 0x0011122B File Offset: 0x0010F62B
		public void SetToggleValue(bool value)
		{
			if (this.m_Type != OptionMenuItem.eType.Toggle || this.m_ToggleSwitch == null)
			{
				return;
			}
			this.m_ToggleSwitch.SetDecided(value);
		}

		// Token: 0x060035EE RID: 13806 RVA: 0x00111257 File Offset: 0x0010F657
		public void OnClickToggleButtonCallBack()
		{
			this.OnValueChanged.Call();
		}

		// Token: 0x060035EF RID: 13807 RVA: 0x00111264 File Offset: 0x0010F664
		public void OnValueChangedCallBack()
		{
			this.OnValueChanged.Call();
		}

		// Token: 0x04003C77 RID: 15479
		[SerializeField]
		private OptionMenuItem.eType m_Type;

		// Token: 0x04003C78 RID: 15480
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04003C79 RID: 15481
		[SerializeField]
		private CustomSlider m_Slider;

		// Token: 0x04003C7A RID: 15482
		[SerializeField]
		private ToggleSwitch m_ToggleSwitch;

		// Token: 0x04003C7B RID: 15483
		private float? m_SliderStartingValue;

		// Token: 0x04003C7C RID: 15484
		private float? m_SliderDefaultValue;

		// Token: 0x04003C7D RID: 15485
		private bool? m_ToggleStartingValue;

		// Token: 0x04003C7E RID: 15486
		private bool? m_ToggleDefaultValue;

		// Token: 0x02000A2B RID: 2603
		public enum eType
		{
			// Token: 0x04003C81 RID: 15489
			Title,
			// Token: 0x04003C82 RID: 15490
			Slider,
			// Token: 0x04003C83 RID: 15491
			Toggle
		}
	}
}
