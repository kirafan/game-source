﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009E8 RID: 2536
	public class MenuLibraryADVLibItem : ScrollItemIcon
	{
		// Token: 0x06003492 RID: 13458 RVA: 0x0010ABD0 File Offset: 0x00108FD0
		protected override void ApplyData()
		{
			MenuLibraryADVLibItemData menuLibraryADVLibItemData = (MenuLibraryADVLibItemData)this.m_Data;
			this.m_Icon.Apply(menuLibraryADVLibItemData.m_ADVLibID);
		}

		// Token: 0x04003B0E RID: 15118
		[SerializeField]
		private ADVLibraryIcon m_Icon;
	}
}
