﻿using System;
using System.Text;

namespace Star.UI.Menu
{
	// Token: 0x020009F6 RID: 2550
	public class MenuLibrarySummonItemData : ScrollItemData
	{
		// Token: 0x060034D4 RID: 13524 RVA: 0x0010C560 File Offset: 0x0010A960
		public MenuLibrarySummonItemData(int charaID)
		{
			this.m_CharaID = charaID;
			StringBuilder stringBuilder = new StringBuilder();
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			for (int i = 0; i < param.m_Rare + 1; i++)
			{
				stringBuilder.Append("☆");
			}
			stringBuilder.Append(" ");
			stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + param.m_Class));
			stringBuilder.Append("\n");
			stringBuilder.Append(param.m_Name);
			this.m_Title = stringBuilder.ToString();
		}

		// Token: 0x140000A3 RID: 163
		// (add) Token: 0x060034D5 RID: 13525 RVA: 0x0010C614 File Offset: 0x0010AA14
		// (remove) Token: 0x060034D6 RID: 13526 RVA: 0x0010C64C File Offset: 0x0010AA4C
		public event Action<int> OnClickCallBack;

		// Token: 0x060034D7 RID: 13527 RVA: 0x0010C682 File Offset: 0x0010AA82
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_CharaID);
		}

		// Token: 0x04003B62 RID: 15202
		public string m_Title;

		// Token: 0x04003B63 RID: 15203
		public int m_CharaID;
	}
}
