﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009F7 RID: 2551
	public class MenuLibraryTitleItem : ScrollItemIcon
	{
		// Token: 0x060034D9 RID: 13529 RVA: 0x0010C69D File Offset: 0x0010AA9D
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x060034DA RID: 13530 RVA: 0x0010C6A5 File Offset: 0x0010AAA5
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x060034DB RID: 13531 RVA: 0x0010C6B0 File Offset: 0x0010AAB0
		protected override void ApplyData()
		{
			ITitleUIData titleUIData = (ITitleUIData)this.m_Data;
			this.m_TitleName.text = titleUIData.m_TitleName;
			this.m_Button.IsDecided = titleUIData.m_IsSelected;
		}

		// Token: 0x060034DC RID: 13532 RVA: 0x0010C6EB File Offset: 0x0010AAEB
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x060034DD RID: 13533 RVA: 0x0010C6F3 File Offset: 0x0010AAF3
		public void OnCallbackButton()
		{
		}

		// Token: 0x04003B65 RID: 15205
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04003B66 RID: 15206
		[SerializeField]
		private CustomButton m_Button;
	}
}
