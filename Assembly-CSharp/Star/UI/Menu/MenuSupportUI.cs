﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A0D RID: 2573
	public class MenuSupportUI : MenuUIBase
	{
		// Token: 0x0600353C RID: 13628 RVA: 0x0010E194 File Offset: 0x0010C594
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x0600353D RID: 13629 RVA: 0x0010E1A4 File Offset: 0x0010C5A4
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuSupportUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuSupportUI.eStep.Idle);
				}
				break;
			}
			case MenuSupportUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuSupportUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600353E RID: 13630 RVA: 0x0010E28C File Offset: 0x0010C68C
		private void ChangeStep(MenuSupportUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuSupportUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuSupportUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuSupportUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuSupportUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuSupportUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600353F RID: 13631 RVA: 0x0010E334 File Offset: 0x0010C734
		public override void PlayIn()
		{
			this.ChangeStep(MenuSupportUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003540 RID: 13632 RVA: 0x0010E37C File Offset: 0x0010C77C
		public override void PlayOut()
		{
			this.ChangeStep(MenuSupportUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003541 RID: 13633 RVA: 0x0010E3C1 File Offset: 0x0010C7C1
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuSupportUI.eStep.End;
		}

		// Token: 0x06003542 RID: 13634 RVA: 0x0010E3CC File Offset: 0x0010C7CC
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06003543 RID: 13635 RVA: 0x0010E3D0 File Offset: 0x0010C7D0
		public void OnSelectButtonCallBack(int fbutton)
		{
			if (fbutton == 1004)
			{
				this.OnClickPurchaseHistory();
			}
			else
			{
				string text = WebDataListUtil.GetIDToAdress(fbutton);
				if (fbutton == 1001)
				{
					text = string.Format(text, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode);
				}
				Application.OpenURL(text);
			}
		}

		// Token: 0x06003544 RID: 13636 RVA: 0x0010E426 File Offset: 0x0010C826
		public void OnClickPurchaseHistory()
		{
			this.m_OnClickPurchaseHistory.Call();
		}

		// Token: 0x04003BE2 RID: 15330
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003BE3 RID: 15331
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003BE4 RID: 15332
		private MenuSupportUI.eStep m_Step;

		// Token: 0x04003BE5 RID: 15333
		public Action m_OnClickPurchaseHistory;

		// Token: 0x02000A0E RID: 2574
		private enum eStep
		{
			// Token: 0x04003BE7 RID: 15335
			Hide,
			// Token: 0x04003BE8 RID: 15336
			In,
			// Token: 0x04003BE9 RID: 15337
			Idle,
			// Token: 0x04003BEA RID: 15338
			Out,
			// Token: 0x04003BEB RID: 15339
			End
		}
	}
}
