﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009E2 RID: 2530
	public class MenuLibraryADVCharaGroup : UIGroup
	{
		// Token: 0x14000099 RID: 153
		// (add) Token: 0x06003479 RID: 13433 RVA: 0x0010A614 File Offset: 0x00108A14
		// (remove) Token: 0x0600347A RID: 13434 RVA: 0x0010A64C File Offset: 0x00108A4C
		public event Action<eCharaNamedType> OnClickNamed;

		// Token: 0x0600347B RID: 13435 RVA: 0x0010A682 File Offset: 0x00108A82
		public void Setup()
		{
			this.m_Scroll.Setup();
		}

		// Token: 0x0600347C RID: 13436 RVA: 0x0010A690 File Offset: 0x00108A90
		public void ListupTitleChara(eTitleType title)
		{
			this.m_Scroll.RemoveAll();
			NamedListDB namedListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB;
			for (int i = 0; i < namedListDB.m_Params.Length; i++)
			{
				eCharaNamedType namedType = (eCharaNamedType)namedListDB.m_Params[i].m_NamedType;
				if (namedListDB.GetTitleType(namedType) == title)
				{
					if (EditUtility.GetHaveNamedToCharaID(namedType) != -1)
					{
						if (MenuUtility.GetAvailableSingleADVList(namedType).Count != 0 || MenuUtility.GetAvailableCrossADVList(namedType).Count != 0)
						{
							MenuLibraryADVCharaItemData menuLibraryADVCharaItemData = new MenuLibraryADVCharaItemData(namedType);
							menuLibraryADVCharaItemData.OnClickCallBack += this.OnClickNamedCallBack;
							this.m_Scroll.AddItem(menuLibraryADVCharaItemData);
						}
					}
				}
			}
		}

		// Token: 0x0600347D RID: 13437 RVA: 0x0010A750 File Offset: 0x00108B50
		private void OnClickNamedCallBack(eCharaNamedType named)
		{
			this.OnClickNamed.Call(named);
		}

		// Token: 0x04003AFA RID: 15098
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
