﻿using System;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009F8 RID: 2552
	public class MenuLibraryUI : MenuUIBase
	{
		// Token: 0x1700032C RID: 812
		// (get) Token: 0x060034DF RID: 13535 RVA: 0x0010C704 File Offset: 0x0010AB04
		public MenuLibraryUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000A4 RID: 164
		// (add) Token: 0x060034E0 RID: 13536 RVA: 0x0010C70C File Offset: 0x0010AB0C
		// (remove) Token: 0x060034E1 RID: 13537 RVA: 0x0010C744 File Offset: 0x0010AB44
		public event Action OnClickButton;

		// Token: 0x060034E2 RID: 13538 RVA: 0x0010C77A File Offset: 0x0010AB7A
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
		}

		// Token: 0x060034E3 RID: 13539 RVA: 0x0010C798 File Offset: 0x0010AB98
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuLibraryUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuLibraryUI.eStep.Idle);
				}
				break;
			}
			case MenuLibraryUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuLibraryUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060034E4 RID: 13540 RVA: 0x0010C880 File Offset: 0x0010AC80
		private void ChangeStep(MenuLibraryUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuLibraryUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuLibraryUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuLibraryUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060034E5 RID: 13541 RVA: 0x0010C928 File Offset: 0x0010AD28
		public override void PlayIn()
		{
			this.ChangeStep(MenuLibraryUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x060034E6 RID: 13542 RVA: 0x0010C970 File Offset: 0x0010AD70
		public override void PlayOut()
		{
			this.ChangeStep(MenuLibraryUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x060034E7 RID: 13543 RVA: 0x0010C9B5 File Offset: 0x0010ADB5
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuLibraryUI.eStep.End;
		}

		// Token: 0x060034E8 RID: 13544 RVA: 0x0010C9C0 File Offset: 0x0010ADC0
		public void OnBackButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x060034E9 RID: 13545 RVA: 0x0010C9C8 File Offset: 0x0010ADC8
		public void OnSelectButtonCallBack(int fbutton)
		{
			this.m_SelectButton = (MenuLibraryUI.eButton)fbutton;
			this.OnClickButton.Call();
		}

		// Token: 0x04003B67 RID: 15207
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003B68 RID: 15208
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003B69 RID: 15209
		private MenuLibraryUI.eButton m_SelectButton = MenuLibraryUI.eButton.None;

		// Token: 0x04003B6A RID: 15210
		private MenuLibraryUI.eStep m_Step;

		// Token: 0x020009F9 RID: 2553
		public enum eButton
		{
			// Token: 0x04003B6D RID: 15213
			None = -1,
			// Token: 0x04003B6E RID: 15214
			TitleExp,
			// Token: 0x04003B6F RID: 15215
			Word,
			// Token: 0x04003B70 RID: 15216
			OriginalChara,
			// Token: 0x04003B71 RID: 15217
			ADV,
			// Token: 0x04003B72 RID: 15218
			OPMovie
		}

		// Token: 0x020009FA RID: 2554
		private enum eStep
		{
			// Token: 0x04003B74 RID: 15220
			Hide,
			// Token: 0x04003B75 RID: 15221
			In,
			// Token: 0x04003B76 RID: 15222
			Idle,
			// Token: 0x04003B77 RID: 15223
			Out,
			// Token: 0x04003B78 RID: 15224
			End
		}
	}
}
