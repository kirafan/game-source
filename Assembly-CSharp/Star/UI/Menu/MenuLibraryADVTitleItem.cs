﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009EC RID: 2540
	public class MenuLibraryADVTitleItem : ScrollItemIcon
	{
		// Token: 0x060034A4 RID: 13476 RVA: 0x0010AFE0 File Offset: 0x001093E0
		protected override void ApplyData()
		{
			MenuLibraryADVTitleItemData menuLibraryADVTitleItemData = (MenuLibraryADVTitleItemData)this.m_Data;
			this.m_Logo.Apply(menuLibraryADVTitleItemData.m_Title);
			this.m_NewIcon.SetActive(menuLibraryADVTitleItemData.m_IsNew);
		}

		// Token: 0x04003B17 RID: 15127
		[SerializeField]
		private ContentTitleLogo m_Logo;

		// Token: 0x04003B18 RID: 15128
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
