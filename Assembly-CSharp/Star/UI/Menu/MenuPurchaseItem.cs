﻿using System;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000A0B RID: 2571
	public class MenuPurchaseItem : ScrollItemIcon
	{
		// Token: 0x06003538 RID: 13624 RVA: 0x0010E0FB File Offset: 0x0010C4FB
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x06003539 RID: 13625 RVA: 0x0010E104 File Offset: 0x0010C504
		protected override void ApplyData()
		{
			MenuPurchaseItemData menuPurchaseItemData = (MenuPurchaseItemData)this.m_Data;
			this.m_Date.text = menuPurchaseItemData.m_Date.ToString("yyyy/MM/dd");
			this.m_ProductName.text = menuPurchaseItemData.m_ProductName;
			this.m_Price.text = UIUtility.GemValueToString(menuPurchaseItemData.m_Price, true);
		}

		// Token: 0x04003BDC RID: 15324
		public Text m_Date;

		// Token: 0x04003BDD RID: 15325
		public Text m_ProductName;

		// Token: 0x04003BDE RID: 15326
		public Text m_Price;
	}
}
