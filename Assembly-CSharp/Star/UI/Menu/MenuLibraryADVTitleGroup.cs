﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009EB RID: 2539
	public class MenuLibraryADVTitleGroup : UIGroup
	{
		// Token: 0x1400009F RID: 159
		// (add) Token: 0x0600349E RID: 13470 RVA: 0x0010ADEC File Offset: 0x001091EC
		// (remove) Token: 0x0600349F RID: 13471 RVA: 0x0010AE24 File Offset: 0x00109224
		public event Action<eTitleType> OnClickTitle;

		// Token: 0x060034A0 RID: 13472 RVA: 0x0010AE5C File Offset: 0x0010925C
		public void Setup()
		{
			this.m_Scroll.Setup();
			this.m_TitleTypes.Clear();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			for (int i = 0; i < dbMng.NamedListDB.m_Params.Length; i++)
			{
				NamedListDB_Param namedListDB_Param = dbMng.NamedListDB.m_Params[i];
				eCharaNamedType namedType = (eCharaNamedType)namedListDB_Param.m_NamedType;
				eTitleType titleType = (eTitleType)namedListDB_Param.m_TitleType;
				if (!this.m_TitleTypes.Contains(titleType))
				{
					if (EditUtility.GetHaveNamedToCharaID(namedType) != -1)
					{
						this.m_TitleTypes.Add(titleType);
					}
				}
			}
			eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
			for (int j = 0; j < eTitleTypeConverter.HowManyTitles(); j++)
			{
				eTitleType eTitleType = eTitleTypeConverter.ConvertFromOrderToTitleType(j);
				if (this.m_TitleTypes.Contains(eTitleType))
				{
					MenuLibraryADVTitleItemData menuLibraryADVTitleItemData = new MenuLibraryADVTitleItemData(eTitleType);
					menuLibraryADVTitleItemData.OnClickCallBack += this.OnClickADVTitleCallBack;
					this.m_Scroll.AddItem(menuLibraryADVTitleItemData);
				}
			}
		}

		// Token: 0x060034A1 RID: 13473 RVA: 0x0010AF74 File Offset: 0x00109374
		public void Open(eTitleType titleType)
		{
			base.Open();
			int scrollValueFromIdx = 0;
			for (int i = 0; i < this.m_TitleTypes.Count; i++)
			{
				if (this.m_TitleTypes[i] == titleType)
				{
					scrollValueFromIdx = i;
					break;
				}
			}
			this.m_Scroll.SetScrollValueFromIdx(scrollValueFromIdx);
		}

		// Token: 0x060034A2 RID: 13474 RVA: 0x0010AFCA File Offset: 0x001093CA
		private void OnClickADVTitleCallBack(eTitleType titleType)
		{
			this.OnClickTitle.Call(titleType);
		}

		// Token: 0x04003B14 RID: 15124
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003B15 RID: 15125
		private List<eTitleType> m_TitleTypes = new List<eTitleType>();
	}
}
