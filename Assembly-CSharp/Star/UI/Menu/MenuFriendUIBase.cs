﻿using System;
using System.Collections.Generic;

namespace Star.UI.Menu
{
	// Token: 0x020009DB RID: 2523
	public abstract class MenuFriendUIBase : MenuUIBase
	{
		// Token: 0x06003452 RID: 13394 RVA: 0x00109A34 File Offset: 0x00107E34
		protected void ClearList(MenuFriendUIBase.eFriendListType type)
		{
			Wrapper<List<IFriendUIData>> listPointer = this.GetListPointer(type);
			if (listPointer == null)
			{
				return;
			}
			listPointer.value = null;
			this.Acquire(type);
		}

		// Token: 0x06003453 RID: 13395 RVA: 0x00109A60 File Offset: 0x00107E60
		protected void AddList(MenuFriendUIBase.eFriendListType to, IFriendUIData from)
		{
			List<IFriendUIData> list = this.GetList(to);
			if (list == null)
			{
				return;
			}
			for (int i = 0; i < list.Count; i++)
			{
				if (from.m_LoginTime < list[i].m_LoginTime)
				{
					list.Insert(i, from);
					return;
				}
			}
			list.Add(from);
		}

		// Token: 0x06003454 RID: 13396 RVA: 0x00109AC0 File Offset: 0x00107EC0
		protected int HowManyListItems(MenuFriendUIBase.eFriendListType type)
		{
			List<IFriendUIData> list = this.GetList(type);
			if (list == null)
			{
				return -1;
			}
			return list.Count;
		}

		// Token: 0x06003455 RID: 13397 RVA: 0x00109AE4 File Offset: 0x00107EE4
		protected IFriendUIData GetListItem(MenuFriendUIBase.eFriendListType type, int index)
		{
			List<IFriendUIData> list = this.GetList(type);
			if (list == null)
			{
				return null;
			}
			if (index < 0 || list.Count <= index)
			{
				return null;
			}
			return list[index];
		}

		// Token: 0x06003456 RID: 13398 RVA: 0x00109B20 File Offset: 0x00107F20
		private List<IFriendUIData> GetList(MenuFriendUIBase.eFriendListType type)
		{
			if (!this.Acquire(type))
			{
				return null;
			}
			Wrapper<List<IFriendUIData>> listPointer = this.GetListPointer(type);
			if (listPointer == null)
			{
				return null;
			}
			return listPointer.value;
		}

		// Token: 0x06003457 RID: 13399 RVA: 0x00109B54 File Offset: 0x00107F54
		private Wrapper<List<IFriendUIData>> GetListPointer(MenuFriendUIBase.eFriendListType type)
		{
			if (type == MenuFriendUIBase.eFriendListType.Friend)
			{
				if (this.m_FriendList == null)
				{
					this.m_FriendList = new Wrapper<List<IFriendUIData>>();
				}
				return this.m_FriendList;
			}
			if (type != MenuFriendUIBase.eFriendListType.Pending)
			{
				return null;
			}
			if (this.m_PendingFriendList == null)
			{
				this.m_PendingFriendList = new Wrapper<List<IFriendUIData>>();
			}
			return this.m_PendingFriendList;
		}

		// Token: 0x06003458 RID: 13400 RVA: 0x00109BB0 File Offset: 0x00107FB0
		private bool Acquire(MenuFriendUIBase.eFriendListType type)
		{
			Wrapper<List<IFriendUIData>> listPointer = this.GetListPointer(type);
			if (listPointer == null)
			{
				return false;
			}
			if (listPointer.value != null)
			{
				return true;
			}
			listPointer.value = new List<IFriendUIData>();
			return listPointer.value != null;
		}

		// Token: 0x04003ACB RID: 15051
		private Wrapper<List<IFriendUIData>> m_FriendList;

		// Token: 0x04003ACC RID: 15052
		private Wrapper<List<IFriendUIData>> m_PendingFriendList;

		// Token: 0x020009DC RID: 2524
		protected enum eFriendListType
		{
			// Token: 0x04003ACE RID: 15054
			Invalid,
			// Token: 0x04003ACF RID: 15055
			Friend,
			// Token: 0x04003AD0 RID: 15056
			Pending
		}

		// Token: 0x020009DD RID: 2525
		public enum eListSortOrderType
		{
			// Token: 0x04003AD2 RID: 15058
			Invalid,
			// Token: 0x04003AD3 RID: 15059
			Asc,
			// Token: 0x04003AD4 RID: 15060
			Desc
		}
	}
}
