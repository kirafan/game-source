﻿using System;
using Star.UI.GachaPlay;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009EE RID: 2542
	public class MenuLibraryEventUI : MenuUIBase
	{
		// Token: 0x1700032B RID: 811
		// (get) Token: 0x060034AA RID: 13482 RVA: 0x0010B227 File Offset: 0x00109627
		// (set) Token: 0x060034AB RID: 13483 RVA: 0x0010B22F File Offset: 0x0010962F
		public int SelectADVID { get; set; }

		// Token: 0x140000A1 RID: 161
		// (add) Token: 0x060034AC RID: 13484 RVA: 0x0010B238 File Offset: 0x00109638
		// (remove) Token: 0x060034AD RID: 13485 RVA: 0x0010B270 File Offset: 0x00109670
		public event Action OnTransit;

		// Token: 0x060034AE RID: 13486 RVA: 0x0010B2A8 File Offset: 0x001096A8
		public void Setup(NPCCharaDisplayUI npcUI)
		{
			this.m_NpcUI = npcUI;
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_ADVLibGroup.Setup();
			this.m_ADVLibGroup.OnClickADVLib += this.OnClickADVLibCallBack;
			this.m_ADVListGroup.Setup();
			this.m_ADVListGroup.OnClickADV += this.OnClickADVCallBack;
			this.m_TitleGroup.Setup();
			this.m_TitleGroup.OnClickTitle += this.OnClickADVTitleCallBack;
			this.m_CharaGroup.Setup();
			this.m_CharaGroup.OnClickNamed += this.OnClickNamedCallBack;
			this.m_SummonGroup.Setup();
			this.m_SummonGroup.OnClickSummon += this.OnClickSummonCallBack;
			this.m_Flavor.gameObject.SetActive(false);
			this.m_FlavorSkipButtonObj.SetActive(false);
			this.m_Flavor.OnEnd += this.OnCloseFlavor;
			this.SelectADVID = -1;
			this.m_UIGroups = base.GetComponentsInChildren<UIGroup>(true);
			this.m_MainScenarioButton.Interactable = (MenuUtility.GetAvailableADVLibrary(eADVLibraryCategory.Story).Count > 0);
			this.m_EventScenarioButton.Interactable = (MenuUtility.GetAvailableADVLibrary(eADVLibraryCategory.Event).Count > 0);
		}

		// Token: 0x060034AF RID: 13487 RVA: 0x0010B3FC File Offset: 0x001097FC
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuLibraryEventUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				for (int j = 0; j < this.m_UIGroups.Length; j++)
				{
					if (this.m_UIGroups[j].IsPlayingInOut)
					{
						flag = false;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuLibraryEventUI.eStep.Idle);
				}
				break;
			}
			case MenuLibraryEventUI.eStep.Out:
			{
				bool flag2 = true;
				for (int k = 0; k < this.m_AnimList.Length; k++)
				{
					if (!this.m_AnimList[k].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				for (int l = 0; l < this.m_UIGroups.Length; l++)
				{
					if (this.m_UIGroups[l].IsPlayingInOut)
					{
						flag2 = false;
					}
				}
				if (flag2)
				{
					this.ChangeStep(MenuLibraryEventUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060034B0 RID: 13488 RVA: 0x0010B534 File Offset: 0x00109934
		private void ChangeStep(MenuLibraryEventUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuLibraryEventUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuLibraryEventUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryEventUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuLibraryEventUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryEventUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060034B1 RID: 13489 RVA: 0x0010B5DC File Offset: 0x001099DC
		public override void PlayIn()
		{
			this.ChangeStep(MenuLibraryEventUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			GameGlobalParameter.MenuLibraryADVParam menuLibADVParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam;
			if (menuLibADVParam != null)
			{
				if (menuLibADVParam.m_AdvLibID != -1)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LibraryADV);
					this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADV;
					this.m_ADVListGroup.Open(MenuUtility.GetAvailableADVList(menuLibADVParam.m_AdvLibID), menuLibADVParam.m_AdvId);
					this.m_ADVLibID = menuLibADVParam.m_AdvLibID;
				}
				else if (menuLibADVParam.m_Named != eCharaNamedType.None)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LibraryADV);
					this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADV;
					this.m_SingleOrCross = menuLibADVParam.m_SingleorCross;
					if (menuLibADVParam.m_SingleorCross == 0)
					{
						this.m_ADVListGroup.Open(MenuUtility.GetAvailableSingleADVList(menuLibADVParam.m_Named), menuLibADVParam.m_AdvId);
					}
					else
					{
						this.m_ADVListGroup.Open(MenuUtility.GetAvailableCrossADVList(menuLibADVParam.m_Named), menuLibADVParam.m_AdvId);
					}
					this.m_NamedType = menuLibADVParam.m_Named;
				}
			}
			else
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Main;
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x060034B2 RID: 13490 RVA: 0x0010B71C File Offset: 0x00109B1C
		public override void PlayOut()
		{
			this.ChangeStep(MenuLibraryEventUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			for (int j = 0; j < this.m_UIGroups.Length; j++)
			{
				if (this.m_UIGroups[j].IsOpen())
				{
					this.m_UIGroups[j].Close();
				}
			}
		}

		// Token: 0x060034B3 RID: 13491 RVA: 0x0010B78E File Offset: 0x00109B8E
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuLibraryEventUI.eStep.End;
		}

		// Token: 0x060034B4 RID: 13492 RVA: 0x0010B799 File Offset: 0x00109B99
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x060034B5 RID: 13493 RVA: 0x0010B79C File Offset: 0x00109B9C
		public void OnClickCategoryCallBack(int btn)
		{
			this.m_MainGroup.Close();
			if (btn == 0)
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADVLib;
				this.m_ADVLibGroup.Open(eADVLibraryCategory.Story);
			}
			else if (btn == 1)
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADVLib;
				this.m_ADVLibGroup.Open(eADVLibraryCategory.Event);
			}
			else
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Title;
				this.m_TitleGroup.Open();
			}
		}

		// Token: 0x060034B6 RID: 13494 RVA: 0x0010B803 File Offset: 0x00109C03
		public void OnClickADVTitleCallBack(eTitleType title)
		{
			this.m_TitleGroup.Close();
			this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Chara;
			this.m_CharaGroup.ListupTitleChara(title);
			this.m_CharaGroup.Open();
		}

		// Token: 0x060034B7 RID: 13495 RVA: 0x0010B82E File Offset: 0x00109C2E
		public void OnClickADVLibCallBack(int advLibID)
		{
			this.m_ADVLibID = advLibID;
			this.m_NamedType = eCharaNamedType.None;
			this.m_ADVLibGroup.Close();
			this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADV;
			this.m_ADVListGroup.Open(MenuUtility.GetAvailableADVList(advLibID), -1);
		}

		// Token: 0x060034B8 RID: 13496 RVA: 0x0010B862 File Offset: 0x00109C62
		public void OnClickNamedCallBack(eCharaNamedType named)
		{
			this.m_NamedType = named;
			this.m_ADVLibID = -1;
			this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.SingleOrCross;
			this.m_CharaGroup.Close();
			this.m_SingleOrCrossGroup.Open(named);
		}

		// Token: 0x060034B9 RID: 13497 RVA: 0x0010B890 File Offset: 0x00109C90
		public void OnClickADVCallBack(int advID)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam = new GameGlobalParameter.MenuLibraryADVParam();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam.m_AdvId = advID;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam.m_AdvLibID = this.m_ADVLibID;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam.m_Named = this.m_NamedType;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.MenuLibADVParam.m_SingleorCross = this.m_SingleOrCross;
			this.SelectADVID = advID;
			this.OnTransit.Call();
		}

		// Token: 0x060034BA RID: 13498 RVA: 0x0010B928 File Offset: 0x00109D28
		public void OnClickSingleOrCross(int btn)
		{
			this.m_SingleOrCrossGroup.Close();
			if (btn == 0)
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Summon;
				this.m_SummonGroup.Open(this.m_NamedType);
			}
			else if (btn == 1)
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADV;
				this.m_ADVListGroup.Open(MenuUtility.GetAvailableSingleADVList(this.m_NamedType), -1);
				this.m_SingleOrCross = 0;
			}
			else if (btn == 2)
			{
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADV;
				this.m_ADVListGroup.Open(MenuUtility.GetAvailableCrossADVList(this.m_NamedType), -1);
				this.m_SingleOrCross = 1;
			}
		}

		// Token: 0x060034BB RID: 13499 RVA: 0x0010B9C0 File Offset: 0x00109DC0
		public void OnClickSummonCallBack(int charaID)
		{
			this.m_SummonGroup.Close();
			this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Flavor;
			this.m_Flavor.gameObject.SetActive(true);
			this.m_FlavorSkipButtonObj.SetActive(true);
			this.m_Flavor.SetCharaIDs(new int[]
			{
				charaID
			});
			this.m_Flavor.Open();
			this.m_NpcUI.PlayOut();
		}

		// Token: 0x060034BC RID: 13500 RVA: 0x0010BA27 File Offset: 0x00109E27
		public void OnClickSkipFlavor()
		{
			this.m_Flavor.GoNext();
		}

		// Token: 0x060034BD RID: 13501 RVA: 0x0010BA34 File Offset: 0x00109E34
		private void OnCloseFlavor()
		{
			this.m_SummonGroup.Open();
			this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Summon;
			this.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Training);
			this.m_FlavorSkipButtonObj.SetActive(false);
		}

		// Token: 0x060034BE RID: 13502 RVA: 0x0010BA60 File Offset: 0x00109E60
		public bool ExecuteBack()
		{
			switch (this.m_CurrentGroup)
			{
			case MenuLibraryEventUI.eUIGroup.Main:
				return true;
			case MenuLibraryEventUI.eUIGroup.ADVLib:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Main;
				this.m_ADVLibGroup.Close();
				this.m_MainGroup.Open();
				return false;
			case MenuLibraryEventUI.eUIGroup.ADV:
				this.m_ADVListGroup.Close();
				if (this.m_NamedType != eCharaNamedType.None)
				{
					this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.SingleOrCross;
					this.m_SingleOrCrossGroup.Open(this.m_NamedType);
				}
				else
				{
					this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.ADVLib;
					this.m_ADVLibGroup.Open((eADVLibraryCategory)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVLibraryListDB.GetParam(this.m_ADVLibID).m_Category);
				}
				return false;
			case MenuLibraryEventUI.eUIGroup.Title:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Main;
				this.m_TitleGroup.Close();
				this.m_MainGroup.Open();
				return false;
			case MenuLibraryEventUI.eUIGroup.Chara:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Title;
				this.m_CharaGroup.Close();
				this.m_TitleGroup.Open();
				return false;
			case MenuLibraryEventUI.eUIGroup.SingleOrCross:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Chara;
				this.m_SingleOrCrossGroup.Close();
				this.m_CharaGroup.ListupTitleChara(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(this.m_NamedType));
				this.m_CharaGroup.Open();
				return false;
			case MenuLibraryEventUI.eUIGroup.Summon:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.SingleOrCross;
				this.m_SummonGroup.Close();
				this.m_SingleOrCrossGroup.Open();
				return false;
			case MenuLibraryEventUI.eUIGroup.Flavor:
				this.m_CurrentGroup = MenuLibraryEventUI.eUIGroup.Summon;
				this.m_Flavor.Close();
				this.m_SummonGroup.Open();
				return false;
			default:
				return false;
			}
		}

		// Token: 0x04003B1C RID: 15132
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003B1D RID: 15133
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003B1E RID: 15134
		[SerializeField]
		private CustomButton m_MainScenarioButton;

		// Token: 0x04003B1F RID: 15135
		[SerializeField]
		private CustomButton m_EventScenarioButton;

		// Token: 0x04003B20 RID: 15136
		[SerializeField]
		private CustomButton m_CharaScenarioButton;

		// Token: 0x04003B21 RID: 15137
		[SerializeField]
		private MenuLibraryADVLibGroup m_ADVLibGroup;

		// Token: 0x04003B22 RID: 15138
		[SerializeField]
		private MenuLibraryADVListGroup m_ADVListGroup;

		// Token: 0x04003B23 RID: 15139
		[SerializeField]
		private MenuLibraryADVTitleGroup m_TitleGroup;

		// Token: 0x04003B24 RID: 15140
		[SerializeField]
		private MenuLibraryADVCharaGroup m_CharaGroup;

		// Token: 0x04003B25 RID: 15141
		[SerializeField]
		private MenuLibrarySingleOrCrossGroup m_SingleOrCrossGroup;

		// Token: 0x04003B26 RID: 15142
		[SerializeField]
		private MenuLibrarySummonGroup m_SummonGroup;

		// Token: 0x04003B27 RID: 15143
		[SerializeField]
		private GachaCharaFlavor m_Flavor;

		// Token: 0x04003B28 RID: 15144
		[SerializeField]
		private GameObject m_FlavorSkipButtonObj;

		// Token: 0x04003B29 RID: 15145
		private UIGroup[] m_UIGroups;

		// Token: 0x04003B2A RID: 15146
		private MenuLibraryEventUI.eUIGroup m_CurrentGroup;

		// Token: 0x04003B2B RID: 15147
		private int m_ADVLibID = -1;

		// Token: 0x04003B2C RID: 15148
		private eCharaNamedType m_NamedType = eCharaNamedType.None;

		// Token: 0x04003B2D RID: 15149
		private int m_SingleOrCross;

		// Token: 0x04003B2E RID: 15150
		private MenuLibraryEventUI.eStep m_Step;

		// Token: 0x04003B31 RID: 15153
		private NPCCharaDisplayUI m_NpcUI;

		// Token: 0x020009EF RID: 2543
		public enum eUIGroup
		{
			// Token: 0x04003B33 RID: 15155
			Main,
			// Token: 0x04003B34 RID: 15156
			ADVLib,
			// Token: 0x04003B35 RID: 15157
			ADV,
			// Token: 0x04003B36 RID: 15158
			Title,
			// Token: 0x04003B37 RID: 15159
			Chara,
			// Token: 0x04003B38 RID: 15160
			SingleOrCross,
			// Token: 0x04003B39 RID: 15161
			Summon,
			// Token: 0x04003B3A RID: 15162
			Flavor
		}

		// Token: 0x020009F0 RID: 2544
		private enum eStep
		{
			// Token: 0x04003B3C RID: 15164
			Hide,
			// Token: 0x04003B3D RID: 15165
			In,
			// Token: 0x04003B3E RID: 15166
			Idle,
			// Token: 0x04003B3F RID: 15167
			Out,
			// Token: 0x04003B40 RID: 15168
			End
		}
	}
}
