﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009FF RID: 2559
	public class MenuNoticeItem : ScrollItemIcon
	{
		// Token: 0x060034FC RID: 13564 RVA: 0x0010D029 File Offset: 0x0010B429
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x060034FD RID: 13565 RVA: 0x0010D034 File Offset: 0x0010B434
		protected override void ApplyData()
		{
			MenuNoticeItemData menuNoticeItemData = (MenuNoticeItemData)this.m_Data;
			this.m_TitleName.text = menuNoticeItemData.m_Title;
			this.m_ToggleSwitch.SetDecided(menuNoticeItemData.m_Flag);
			this.m_ToggleSwitch.SetInteractable(menuNoticeItemData.m_Interactable);
		}

		// Token: 0x060034FE RID: 13566 RVA: 0x0010D080 File Offset: 0x0010B480
		public void OnClickToggleSwitchCallBack()
		{
			MenuNoticeItemData menuNoticeItemData = (MenuNoticeItemData)this.m_Data;
			menuNoticeItemData.m_Flag = this.m_ToggleSwitch.IsOn();
			menuNoticeItemData.OnClickCallBack();
		}

		// Token: 0x04003B95 RID: 15253
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04003B96 RID: 15254
		[SerializeField]
		private ToggleSwitch m_ToggleSwitch;
	}
}
