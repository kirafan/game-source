﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x02000A0C RID: 2572
	public class MenuPurchaseItemData : ScrollItemData
	{
		// Token: 0x0600353A RID: 13626 RVA: 0x0010E160 File Offset: 0x0010C560
		public MenuPurchaseItemData(StoreManager.PurchaseLog data)
		{
			this.m_Date = data.m_Date;
			this.m_Price = data.m_Price;
			this.m_ProductName = data.m_Name;
		}

		// Token: 0x04003BDF RID: 15327
		public DateTime m_Date;

		// Token: 0x04003BE0 RID: 15328
		public long m_Price;

		// Token: 0x04003BE1 RID: 15329
		public string m_ProductName;
	}
}
