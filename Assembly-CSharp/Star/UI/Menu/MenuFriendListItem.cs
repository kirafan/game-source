﻿using System;
using Star.UI.Edit;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009D6 RID: 2518
	public class MenuFriendListItem : ScrollItemIcon
	{
		// Token: 0x06003440 RID: 13376 RVA: 0x00109408 File Offset: 0x00107808
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06003441 RID: 13377 RVA: 0x00109410 File Offset: 0x00107810
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x06003442 RID: 13378 RVA: 0x00109418 File Offset: 0x00107818
		protected override void ApplyData()
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			if (this.m_UserName != null)
			{
				this.m_UserName.text = friendUIData.m_Name;
			}
			if (this.m_Comment != null)
			{
				this.m_Comment.text = friendUIData.m_Comment;
			}
			if (this.m_Level != null)
			{
				this.m_Level.text = friendUIData.m_Level.ToString();
			}
			if (this.m_Login != null)
			{
				this.m_Login.text = UIUtility.GetLastLoginString(friendUIData.m_LoginTime);
			}
			if (this.m_IconCloner != null && friendUIData.m_UserSupportData != null)
			{
				this.m_IconCloner.gameObject.SetActive(true);
				this.m_IconCloner.GetInst<CharaIconWithFrame>().ApplySupport(friendUIData.m_UserSupportData);
			}
			else
			{
				this.m_IconCloner.gameObject.SetActive(false);
			}
			int select = (int)friendUIData.m_Select;
			for (int i = 0; i < this.m_ButtonGroup.Length; i++)
			{
				this.m_ButtonGroup[i].SetActive(i == select);
			}
		}

		// Token: 0x06003443 RID: 13379 RVA: 0x00109553 File Offset: 0x00107953
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x06003444 RID: 13380 RVA: 0x0010955C File Offset: 0x0010795C
		public void OnCallbackButton(int fbuttonno)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			switch (fbuttonno)
			{
			case 0:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomPlayerID = friendUIData.m_PlayerID;
				friendUIData.OnAction(MenuFriendListItem.eAction.RoomIn);
				break;
			case 1:
			{
				FriendManager.FriendData friendData = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.GetFriendData(friendUIData.m_PlayerID);
				SingletonMonoBehaviour<SupportEditPlayer>.Inst.Open(friendData.m_UserSupportData, friendData.m_SupportPartyName, friendData.m_SupportLimit, PartyEditUIBase.eMode.View);
				break;
			}
			case 2:
				this.m_Select = (MenuFriendListItem.eAction)fbuttonno;
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.FriendConfirmTitle, eText_MessageDB.FriendConfirmTerminate, new Action<int>(this.OnConfirm));
				break;
			default:
				if (fbuttonno != 10)
				{
					if (fbuttonno != 11)
					{
						if (fbuttonno != 20)
						{
							if (fbuttonno == 30)
							{
								this.m_Select = (MenuFriendListItem.eAction)fbuttonno;
								SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.FriendConfirmTitle, eText_MessageDB.FriendConfirmPropose, new Action<int>(this.OnConfirm));
							}
						}
						else
						{
							this.m_Select = (MenuFriendListItem.eAction)fbuttonno;
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.FriendConfirmTitle, eText_MessageDB.FriendConfirmCancel, new Action<int>(this.OnConfirm));
						}
					}
					else
					{
						this.m_Select = (MenuFriendListItem.eAction)fbuttonno;
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.FriendConfirmTitle, eText_MessageDB.FriendConfirmRefuse, new Action<int>(this.OnConfirm));
					}
				}
				else
				{
					this.m_Select = (MenuFriendListItem.eAction)fbuttonno;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.FriendConfirmTitle, eText_MessageDB.FriendConfirmAccept, new Action<int>(this.OnConfirm));
				}
				break;
			}
		}

		// Token: 0x06003445 RID: 13381 RVA: 0x00109704 File Offset: 0x00107B04
		private void OnConfirm(int btn)
		{
			if (btn == 0)
			{
				IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
				MenuFriendListItem.eAction select = this.m_Select;
				if (select != MenuFriendListItem.eAction.Accept)
				{
					if (select != MenuFriendListItem.eAction.Refuse)
					{
						if (select != MenuFriendListItem.eAction.Terminate)
						{
							if (select != MenuFriendListItem.eAction.Cancel)
							{
								if (select == MenuFriendListItem.eAction.Propose)
								{
									SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Propose(friendUIData.m_PlayerID, new Action<bool>(this.OnResponse_Propose));
								}
							}
							else
							{
								SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Cancel(friendUIData.m_FriendMngID, new Action<bool>(this.OnResponse_Cancel));
							}
						}
						else
						{
							SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Terminate(friendUIData.m_FriendMngID, new Action<bool>(this.OnResponse_Terminate));
						}
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Refuse(friendUIData.m_FriendMngID, new Action<bool>(this.OnResponse_Refuse));
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_Accept(friendUIData.m_FriendMngID, new Action<bool>(this.OnResponse_Accept));
				}
				this.m_Select = MenuFriendListItem.eAction.None;
			}
		}

		// Token: 0x06003446 RID: 13382 RVA: 0x00109820 File Offset: 0x00107C20
		private void OnResponse_Terminate(bool isError)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			friendUIData.OnAction(MenuFriendListItem.eAction.Terminate);
		}

		// Token: 0x06003447 RID: 13383 RVA: 0x00109840 File Offset: 0x00107C40
		private void OnResponse_Accept(bool isError)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			friendUIData.OnAction(MenuFriendListItem.eAction.Accept);
		}

		// Token: 0x06003448 RID: 13384 RVA: 0x00109864 File Offset: 0x00107C64
		private void OnResponse_Refuse(bool isError)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			friendUIData.OnAction(MenuFriendListItem.eAction.Refuse);
		}

		// Token: 0x06003449 RID: 13385 RVA: 0x00109888 File Offset: 0x00107C88
		private void OnResponse_Cancel(bool isError)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			friendUIData.OnAction(MenuFriendListItem.eAction.Cancel);
		}

		// Token: 0x0600344A RID: 13386 RVA: 0x001098AC File Offset: 0x00107CAC
		private void OnResponse_Propose(bool isError)
		{
			IFriendUIData friendUIData = (IFriendUIData)this.m_Data;
			friendUIData.OnAction(MenuFriendListItem.eAction.Propose);
		}

		// Token: 0x04003AA6 RID: 15014
		[SerializeField]
		private Text m_UserName;

		// Token: 0x04003AA7 RID: 15015
		[SerializeField]
		private Text m_Comment;

		// Token: 0x04003AA8 RID: 15016
		[SerializeField]
		private Text m_Level;

		// Token: 0x04003AA9 RID: 15017
		[SerializeField]
		private Text m_Login;

		// Token: 0x04003AAA RID: 15018
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04003AAB RID: 15019
		[SerializeField]
		private GameObject[] m_ButtonGroup;

		// Token: 0x04003AAC RID: 15020
		private MenuFriendListItem.eAction m_Select = MenuFriendListItem.eAction.None;

		// Token: 0x020009D7 RID: 2519
		public enum eAction
		{
			// Token: 0x04003AAE RID: 15022
			None = -1,
			// Token: 0x04003AAF RID: 15023
			RoomIn,
			// Token: 0x04003AB0 RID: 15024
			Support,
			// Token: 0x04003AB1 RID: 15025
			Terminate,
			// Token: 0x04003AB2 RID: 15026
			Accept = 10,
			// Token: 0x04003AB3 RID: 15027
			Refuse,
			// Token: 0x04003AB4 RID: 15028
			Cancel = 20,
			// Token: 0x04003AB5 RID: 15029
			Propose = 30
		}
	}
}
