﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009E0 RID: 2528
	public class MenuHelpUI : MenuUIBase
	{
		// Token: 0x0600346F RID: 13423 RVA: 0x0010A3A9 File Offset: 0x001087A9
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x06003470 RID: 13424 RVA: 0x0010A3B7 File Offset: 0x001087B7
		private void CallbackBackButton(bool flg)
		{
			this.OnBackButtonCallBack();
		}

		// Token: 0x06003471 RID: 13425 RVA: 0x0010A3C0 File Offset: 0x001087C0
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuHelpUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuHelpUI.eStep.Idle);
				}
				break;
			}
			case MenuHelpUI.eStep.Idle:
				this.UpdateAndroidBackKey();
				break;
			case MenuHelpUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_WebViewGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuHelpUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003472 RID: 13426 RVA: 0x0010A4AC File Offset: 0x001088AC
		private void ChangeStep(MenuHelpUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuHelpUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuHelpUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuHelpUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuHelpUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuHelpUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003473 RID: 13427 RVA: 0x0010A554 File Offset: 0x00108954
		public override void PlayIn()
		{
			this.ChangeStep(MenuHelpUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_WebViewGroup.Open(WebDataListUtil.GetIDToAdress(1009), null);
		}

		// Token: 0x06003474 RID: 13428 RVA: 0x0010A5A4 File Offset: 0x001089A4
		public override void PlayOut()
		{
			this.ChangeStep(MenuHelpUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_WebViewGroup.Close();
		}

		// Token: 0x06003475 RID: 13429 RVA: 0x0010A5E9 File Offset: 0x001089E9
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuHelpUI.eStep.End;
		}

		// Token: 0x06003476 RID: 13430 RVA: 0x0010A5F4 File Offset: 0x001089F4
		public void OnBackButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x06003477 RID: 13431 RVA: 0x0010A5FC File Offset: 0x001089FC
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BackButton, null);
		}

		// Token: 0x04003AF0 RID: 15088
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003AF1 RID: 15089
		[SerializeField]
		private WebViewGroup m_WebViewGroup;

		// Token: 0x04003AF2 RID: 15090
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04003AF3 RID: 15091
		private MenuHelpUI.eStep m_Step;

		// Token: 0x020009E1 RID: 2529
		private enum eStep
		{
			// Token: 0x04003AF5 RID: 15093
			Hide,
			// Token: 0x04003AF6 RID: 15094
			In,
			// Token: 0x04003AF7 RID: 15095
			Idle,
			// Token: 0x04003AF8 RID: 15096
			Out,
			// Token: 0x04003AF9 RID: 15097
			End
		}
	}
}
