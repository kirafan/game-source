﻿using System;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A0F RID: 2575
	public class MenuTopUI : MenuUIBase
	{
		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06003546 RID: 13638 RVA: 0x0010E442 File Offset: 0x0010C842
		public MenuTopUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x06003547 RID: 13639 RVA: 0x0010E44A File Offset: 0x0010C84A
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
		}

		// Token: 0x06003548 RID: 13640 RVA: 0x0010E468 File Offset: 0x0010C868
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuTopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuTopUI.eStep.Idle);
				}
				break;
			}
			case MenuTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003549 RID: 13641 RVA: 0x0010E550 File Offset: 0x0010C950
		private void ChangeStep(MenuTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuTopUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuTopUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuTopUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuTopUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600354A RID: 13642 RVA: 0x0010E5F8 File Offset: 0x0010C9F8
		public override void PlayIn()
		{
			this.ChangeStep(MenuTopUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x0600354B RID: 13643 RVA: 0x0010E640 File Offset: 0x0010CA40
		public override void PlayOut()
		{
			this.ChangeStep(MenuTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x0600354C RID: 13644 RVA: 0x0010E685 File Offset: 0x0010CA85
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuTopUI.eStep.End;
		}

		// Token: 0x0600354D RID: 13645 RVA: 0x0010E690 File Offset: 0x0010CA90
		public void OnClickButtonCallBack(int buttonId)
		{
			this.m_SelectButton = (MenuTopUI.eButton)buttonId;
			switch (this.m_SelectButton)
			{
			case MenuTopUI.eButton.Ask:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuOpenWebTitle, eText_MessageDB.MenuOpenWeb, new Action<int>(this.OnConfirmTransit));
				break;
			case MenuTopUI.eButton.Site:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuOpenWebTitle, eText_MessageDB.MenuOpenWeb, new Action<int>(this.OnConfirmTransit));
				break;
			case MenuTopUI.eButton.Twitter:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuOpenWebTitle, eText_MessageDB.MenuOpenWeb, new Action<int>(this.OnConfirmTransit));
				break;
			case MenuTopUI.eButton.Title:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuGotoTitleTitle, eText_MessageDB.MenuGotoTitle, new Action<int>(this.OnConfirmTransit));
				break;
			default:
				if (this.m_SelectButton != MenuTopUI.eButton.Option && this.m_SelectButton != MenuTopUI.eButton.Help && this.m_SelectButton != MenuTopUI.eButton.Notification && this.m_SelectButton != MenuTopUI.eButton.PlayerMoveConfiguration)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				}
				this.PlayOut();
				break;
			}
		}

		// Token: 0x0600354E RID: 13646 RVA: 0x0010E7C0 File Offset: 0x0010CBC0
		public void OnConfirmTransit(int btn)
		{
			if (btn == 0)
			{
				switch (this.m_SelectButton)
				{
				case MenuTopUI.eButton.Ask:
				{
					string text = WebDataListUtil.GetIDToAdress(1001);
					text = string.Format(text, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode);
					Application.OpenURL(text);
					break;
				}
				case MenuTopUI.eButton.Site:
					Application.OpenURL(WebDataListUtil.GetIDToAdress(1007));
					break;
				case MenuTopUI.eButton.Twitter:
					Application.OpenURL(WebDataListUtil.GetIDToAdress(1008));
					break;
				case MenuTopUI.eButton.Title:
					this.PlayOut();
					break;
				}
			}
			else
			{
				this.m_SelectButton = MenuTopUI.eButton.None;
			}
		}

		// Token: 0x04003BEC RID: 15340
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003BED RID: 15341
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003BEE RID: 15342
		private MenuTopUI.eStep m_Step;

		// Token: 0x04003BEF RID: 15343
		private MenuTopUI.eButton m_SelectButton = MenuTopUI.eButton.None;

		// Token: 0x02000A10 RID: 2576
		private enum eStep
		{
			// Token: 0x04003BF1 RID: 15345
			Hide,
			// Token: 0x04003BF2 RID: 15346
			In,
			// Token: 0x04003BF3 RID: 15347
			Idle,
			// Token: 0x04003BF4 RID: 15348
			Out,
			// Token: 0x04003BF5 RID: 15349
			End
		}

		// Token: 0x02000A11 RID: 2577
		public enum eButton
		{
			// Token: 0x04003BF7 RID: 15351
			None = -1,
			// Token: 0x04003BF8 RID: 15352
			UserInfomation,
			// Token: 0x04003BF9 RID: 15353
			Purchase,
			// Token: 0x04003BFA RID: 15354
			Friend,
			// Token: 0x04003BFB RID: 15355
			Help,
			// Token: 0x04003BFC RID: 15356
			Option,
			// Token: 0x04003BFD RID: 15357
			Notification,
			// Token: 0x04003BFE RID: 15358
			Ask,
			// Token: 0x04003BFF RID: 15359
			Site,
			// Token: 0x04003C00 RID: 15360
			Twitter,
			// Token: 0x04003C01 RID: 15361
			Title,
			// Token: 0x04003C02 RID: 15362
			PlayerMoveConfiguration,
			// Token: 0x04003C03 RID: 15363
			Library
		}
	}
}
