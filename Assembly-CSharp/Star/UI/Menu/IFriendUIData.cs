﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x020009D9 RID: 2521
	public class IFriendUIData : ScrollItemData
	{
		// Token: 0x0600344C RID: 13388 RVA: 0x001098D5 File Offset: 0x00107CD5
		public IFriendUIData(FriendManager.FriendData pdata, Action<MenuFriendListItem.eAction> OnResponse)
		{
			this.m_OnAction = OnResponse;
			this.ApplyFriendData(pdata);
		}

		// Token: 0x14000097 RID: 151
		// (add) Token: 0x0600344D RID: 13389 RVA: 0x001098EC File Offset: 0x00107CEC
		// (remove) Token: 0x0600344E RID: 13390 RVA: 0x00109924 File Offset: 0x00107D24
		public event Action<MenuFriendListItem.eAction> m_OnAction;

		// Token: 0x0600344F RID: 13391 RVA: 0x0010995C File Offset: 0x00107D5C
		public void ApplyFriendData(FriendManager.FriendData pdata)
		{
			this.m_FriendMngID = pdata.m_MngFriendID;
			this.m_PlayerID = pdata.m_ID;
			this.m_Name = pdata.m_Name;
			this.m_Comment = pdata.m_Comment;
			this.m_MyCode = pdata.m_MyCode;
			this.m_Level = pdata.m_Lv;
			this.m_LoginTime = pdata.m_LastLoginAt;
			this.m_UserSupportData = pdata.m_LeaderCharaData;
			switch (pdata.m_Type)
			{
			case FriendDefine.eType.Friend:
				this.m_Select = IFriendUIData.eSelect.Base;
				break;
			case FriendDefine.eType.OpponentPropose:
				this.m_Select = IFriendUIData.eSelect.OpponentPropose;
				break;
			case FriendDefine.eType.SelfPropose:
				this.m_Select = IFriendUIData.eSelect.SelfPropose;
				break;
			case FriendDefine.eType.Guest:
				this.m_Select = IFriendUIData.eSelect.Guest;
				break;
			}
		}

		// Token: 0x06003450 RID: 13392 RVA: 0x00109A1B File Offset: 0x00107E1B
		public void OnAction(MenuFriendListItem.eAction action)
		{
			this.m_OnAction.Call(action);
		}

		// Token: 0x04003AB7 RID: 15031
		public long m_FriendMngID;

		// Token: 0x04003AB8 RID: 15032
		public long m_PlayerID;

		// Token: 0x04003AB9 RID: 15033
		public string m_Name;

		// Token: 0x04003ABA RID: 15034
		public string m_Comment;

		// Token: 0x04003ABB RID: 15035
		public string m_MyCode;

		// Token: 0x04003ABC RID: 15036
		public int m_Level;

		// Token: 0x04003ABD RID: 15037
		public UserSupportData m_UserSupportData;

		// Token: 0x04003ABE RID: 15038
		public DateTime m_LoginTime;

		// Token: 0x04003ABF RID: 15039
		public IFriendUIData.eSelect m_Select;

		// Token: 0x020009DA RID: 2522
		public enum eSelect
		{
			// Token: 0x04003AC2 RID: 15042
			Non = -1,
			// Token: 0x04003AC3 RID: 15043
			Base,
			// Token: 0x04003AC4 RID: 15044
			OpponentPropose,
			// Token: 0x04003AC5 RID: 15045
			SelfPropose,
			// Token: 0x04003AC6 RID: 15046
			Guest,
			// Token: 0x04003AC7 RID: 15047
			RoomBase,
			// Token: 0x04003AC8 RID: 15048
			RoomOpponentPropose,
			// Token: 0x04003AC9 RID: 15049
			RoomSelfPropose,
			// Token: 0x04003ACA RID: 15050
			RoomGuest
		}
	}
}
