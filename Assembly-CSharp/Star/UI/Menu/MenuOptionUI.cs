﻿using System;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A04 RID: 2564
	public class MenuOptionUI : MenuUIBase
	{
		// Token: 0x06003510 RID: 13584 RVA: 0x0010D653 File Offset: 0x0010BA53
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_OptionGroup.OnEnd += this.PlayOut;
		}

		// Token: 0x06003511 RID: 13585 RVA: 0x0010D68C File Offset: 0x0010BA8C
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuOptionUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuOptionUI.eStep.Idle);
				}
				break;
			}
			case MenuOptionUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_OptionGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuOptionUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003512 RID: 13586 RVA: 0x0010D774 File Offset: 0x0010BB74
		private void ChangeStep(MenuOptionUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuOptionUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuOptionUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuOptionUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuOptionUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuOptionUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003513 RID: 13587 RVA: 0x0010D81C File Offset: 0x0010BC1C
		public override void PlayIn()
		{
			this.ChangeStep(MenuOptionUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_OptionGroup.Open(null);
		}

		// Token: 0x06003514 RID: 13588 RVA: 0x0010D864 File Offset: 0x0010BC64
		public override void PlayOut()
		{
			this.ChangeStep(MenuOptionUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
		}

		// Token: 0x06003515 RID: 13589 RVA: 0x0010D89E File Offset: 0x0010BC9E
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuOptionUI.eStep.End;
		}

		// Token: 0x06003516 RID: 13590 RVA: 0x0010D8A9 File Offset: 0x0010BCA9
		public void OnBackButtonCallBack()
		{
		}

		// Token: 0x06003517 RID: 13591 RVA: 0x0010D8AB File Offset: 0x0010BCAB
		public void OnCopyButtonCallBack()
		{
		}

		// Token: 0x04003BAE RID: 15278
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003BAF RID: 15279
		[SerializeField]
		private OptionWindow m_OptionGroup;

		// Token: 0x04003BB0 RID: 15280
		private MenuOptionUI.eStep m_Step;

		// Token: 0x02000A05 RID: 2565
		private enum eStep
		{
			// Token: 0x04003BB2 RID: 15282
			Hide,
			// Token: 0x04003BB3 RID: 15283
			In,
			// Token: 0x04003BB4 RID: 15284
			Idle,
			// Token: 0x04003BB5 RID: 15285
			Out,
			// Token: 0x04003BB6 RID: 15286
			End
		}
	}
}
