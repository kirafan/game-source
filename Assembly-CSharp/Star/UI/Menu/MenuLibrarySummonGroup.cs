﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009F4 RID: 2548
	public class MenuLibrarySummonGroup : UIGroup
	{
		// Token: 0x140000A2 RID: 162
		// (add) Token: 0x060034CD RID: 13517 RVA: 0x0010C410 File Offset: 0x0010A810
		// (remove) Token: 0x060034CE RID: 13518 RVA: 0x0010C448 File Offset: 0x0010A848
		public event Action<int> OnClickSummon;

		// Token: 0x060034CF RID: 13519 RVA: 0x0010C47E File Offset: 0x0010A87E
		public void Setup()
		{
			this.m_Scroll.Setup();
		}

		// Token: 0x060034D0 RID: 13520 RVA: 0x0010C48C File Offset: 0x0010A88C
		public void Open(eCharaNamedType named)
		{
			this.m_Scroll.RemoveAllData();
			List<int> haveNamedToCharaIDList = EditUtility.GetHaveNamedToCharaIDList(named);
			for (int i = 0; i < haveNamedToCharaIDList.Count; i++)
			{
				if (!string.IsNullOrEmpty(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaFlavorTextDB.GetFlavorText(haveNamedToCharaIDList[i])))
				{
					MenuLibrarySummonItemData menuLibrarySummonItemData = new MenuLibrarySummonItemData(haveNamedToCharaIDList[i]);
					menuLibrarySummonItemData.OnClickCallBack += this.OnClickSummonCallBack;
					this.m_Scroll.AddItem(menuLibrarySummonItemData);
				}
			}
			base.Open();
		}

		// Token: 0x060034D1 RID: 13521 RVA: 0x0010C51D File Offset: 0x0010A91D
		private void OnClickSummonCallBack(int charaID)
		{
			this.OnClickSummon.Call(charaID);
		}

		// Token: 0x04003B5F RID: 15199
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
