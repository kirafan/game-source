﻿using System;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A2C RID: 2604
	public class OptionWindow : UIGroup
	{
		// Token: 0x060035F1 RID: 13809 RVA: 0x0011136F File Offset: 0x0010F76F
		public static OptionWindow.eCategory ConverteOptionItemToeCategory(OptionWindow.eOptionItem itemType)
		{
			switch (itemType)
			{
			case OptionWindow.eOptionItem.BGM:
				return OptionWindow.eCategory.Sound;
			case OptionWindow.eOptionItem.SE:
				return OptionWindow.eCategory.Sound;
			case OptionWindow.eOptionItem.Voice:
				return OptionWindow.eCategory.Sound;
			case OptionWindow.eOptionItem.BattlePlayerSpeed:
				return OptionWindow.eCategory.Battle;
			case OptionWindow.eOptionItem.BattleEnemySpeed:
				return OptionWindow.eCategory.Battle;
			case OptionWindow.eOptionItem.BattleTogetherSpeed:
				return OptionWindow.eCategory.Battle;
			case OptionWindow.eOptionItem.SkipTogetherAttackInput:
				return OptionWindow.eCategory.Battle;
			case OptionWindow.eOptionItem.SkipGreet:
				return OptionWindow.eCategory.Town;
			default:
				return OptionWindow.eCategory.Sound;
			}
		}

		// Token: 0x060035F2 RID: 13810 RVA: 0x001113AD File Offset: 0x0010F7AD
		public static OptionMenuItem.eType ConverteOptionItemToOptionMenuItemeType(OptionWindow.eOptionItem itemType)
		{
			switch (itemType)
			{
			case OptionWindow.eOptionItem.BGM:
				return OptionMenuItem.eType.Slider;
			case OptionWindow.eOptionItem.SE:
				return OptionMenuItem.eType.Slider;
			case OptionWindow.eOptionItem.Voice:
				return OptionMenuItem.eType.Slider;
			case OptionWindow.eOptionItem.BattlePlayerSpeed:
				return OptionMenuItem.eType.Toggle;
			case OptionWindow.eOptionItem.BattleEnemySpeed:
				return OptionMenuItem.eType.Toggle;
			case OptionWindow.eOptionItem.BattleTogetherSpeed:
				return OptionMenuItem.eType.Toggle;
			case OptionWindow.eOptionItem.SkipTogetherAttackInput:
				return OptionMenuItem.eType.Toggle;
			case OptionWindow.eOptionItem.SkipGreet:
				return OptionMenuItem.eType.Toggle;
			default:
				return OptionMenuItem.eType.Title;
			}
		}

		// Token: 0x060035F3 RID: 13811 RVA: 0x001113EC File Offset: 0x0010F7EC
		public static eText_CommonDB ConverteOptionItemToeText_CommonDB(OptionWindow.eOptionItem itemType)
		{
			switch (itemType)
			{
			case OptionWindow.eOptionItem.BGM:
				return eText_CommonDB.MenuOptionWindowSoundBGM;
			case OptionWindow.eOptionItem.SE:
				return eText_CommonDB.MenuOptionWindowSoundSE;
			case OptionWindow.eOptionItem.Voice:
				return eText_CommonDB.MenuOptionWindowSoundVoice;
			case OptionWindow.eOptionItem.BattlePlayerSpeed:
				return eText_CommonDB.MenuOptionWindowBattlePlayerSpeed;
			case OptionWindow.eOptionItem.BattleEnemySpeed:
				return eText_CommonDB.MenuOptionWindowBattleEnemySpeed;
			case OptionWindow.eOptionItem.BattleTogetherSpeed:
				return eText_CommonDB.MenuOptionWindowBattleTogetherSpeed;
			case OptionWindow.eOptionItem.SkipTogetherAttackInput:
				return eText_CommonDB.MenuOptionWindowBattleSkipTogetherAttackInput;
			case OptionWindow.eOptionItem.SkipGreet:
				return eText_CommonDB.MenuOptionWindowSkipGreet;
			default:
				return eText_CommonDB.TitleLaunchNotice;
			}
		}

		// Token: 0x060035F4 RID: 13812 RVA: 0x00111459 File Offset: 0x0010F859
		public static bool IsBattleOption(OptionWindow.eOptionItem itemType)
		{
			switch (itemType)
			{
			case OptionWindow.eOptionItem.BGM:
				return true;
			case OptionWindow.eOptionItem.SE:
				return true;
			case OptionWindow.eOptionItem.Voice:
				return true;
			case OptionWindow.eOptionItem.BattlePlayerSpeed:
				return true;
			case OptionWindow.eOptionItem.BattleEnemySpeed:
				return true;
			case OptionWindow.eOptionItem.BattleTogetherSpeed:
				return true;
			case OptionWindow.eOptionItem.SkipTogetherAttackInput:
				return true;
			case OptionWindow.eOptionItem.SkipGreet:
				return false;
			default:
				return false;
			}
		}

		// Token: 0x060035F5 RID: 13813 RVA: 0x00111497 File Offset: 0x0010F897
		public static SoundDefine.eCategory ConverteOptionItemToSoundDefineeCategory(OptionWindow.eOptionItem itemType)
		{
			if (itemType == OptionWindow.eOptionItem.BGM)
			{
				return SoundDefine.eCategory.BGM;
			}
			if (itemType == OptionWindow.eOptionItem.SE)
			{
				return SoundDefine.eCategory.SE;
			}
			if (itemType != OptionWindow.eOptionItem.Voice)
			{
				return SoundDefine.eCategory.Num;
			}
			return SoundDefine.eCategory.VOICE;
		}

		// Token: 0x060035F6 RID: 13814 RVA: 0x001114BC File Offset: 0x0010F8BC
		public void Open(BattleOption battleOption)
		{
			base.Open();
			this.m_IsDirty = false;
			this.m_Objs = new OptionMenuItem[8];
			if (battleOption != null)
			{
				this.m_BattleOption = battleOption;
				this.m_TownOnlyObject.SetActive(false);
			}
			else
			{
				this.m_TownOnlyObject.SetActive(true);
			}
			for (int i = 0; i < this.m_ItemDatas.Length; i++)
			{
				if (this.m_BattleOption != null && !this.m_ItemDatas[i].m_IsBattleOption)
				{
					this.m_Objs[(int)this.m_ItemDatas[i].m_Option] = null;
				}
				else
				{
					RectTransform parent = this.m_ItemParentSound;
					OptionWindow.eCategory category = this.m_ItemDatas[i].m_Category;
					if (category != OptionWindow.eCategory.Sound)
					{
						if (category != OptionWindow.eCategory.Battle)
						{
							if (category == OptionWindow.eCategory.Town)
							{
								parent = this.m_ItemParentTown;
							}
						}
						else
						{
							parent = this.m_ItemParentBattle;
						}
					}
					else
					{
						parent = this.m_ItemParentSound;
					}
					OptionMenuItem optionMenuItem = null;
					OptionMenuItem.eType type = this.m_ItemDatas[i].m_Type;
					if (type != OptionMenuItem.eType.Slider)
					{
						if (type == OptionMenuItem.eType.Toggle)
						{
							optionMenuItem = UnityEngine.Object.Instantiate<OptionMenuItem>(this.m_ItemTogglePrefab, parent, false);
							this.RegisterToggleStartingValue(optionMenuItem, this.GetToggleStartingValue(this.m_ItemDatas[i].m_Option));
							this.RegisterToggleDefaultValue(optionMenuItem, this.GetToggleDefaultValue(this.m_ItemDatas[i].m_Option));
						}
					}
					else
					{
						optionMenuItem = UnityEngine.Object.Instantiate<OptionMenuItem>(this.m_ItemSliderPrefab, parent, false);
						this.RegisterSliderStartingNormalizedValue(optionMenuItem, this.GetSliderStartingNormalizedValue(this.m_ItemDatas[i].m_Option));
						this.RegisterSliderDefaultNormalizedValue(optionMenuItem, this.GetSliderDefaultNormalizedValue(this.m_ItemDatas[i].m_Option));
						OptionWindow.eOptionItem option = this.m_ItemDatas[i].m_Option;
						if (option != OptionWindow.eOptionItem.BGM)
						{
							if (option != OptionWindow.eOptionItem.SE)
							{
								if (option == OptionWindow.eOptionItem.Voice)
								{
									optionMenuItem.OnValueChanged += this.OnChangeVoiceVolume;
								}
							}
							else
							{
								optionMenuItem.OnValueChanged += this.OnChangeSEVolume;
							}
						}
						else
						{
							optionMenuItem.OnValueChanged += this.OnChangeBGMVolume;
						}
					}
					optionMenuItem.SetTitleText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(this.m_ItemDatas[i].m_Title));
					optionMenuItem.OnValueChanged += this.OnValueChanged;
					this.m_Objs[(int)this.m_ItemDatas[i].m_Option] = optionMenuItem;
				}
			}
			this.RefreshRestoreButtonInteractable();
		}

		// Token: 0x060035F7 RID: 13815 RVA: 0x00111750 File Offset: 0x0010FB50
		public override void Close()
		{
			base.Close();
			if (this.m_Objs != null)
			{
				for (int i = 0; i < this.m_Objs.Length; i++)
				{
					OptionMenuItem optionMenuItem = this.m_Objs[i];
					if (optionMenuItem != null)
					{
						UnityEngine.Object.Destroy(optionMenuItem.gameObject);
					}
					optionMenuItem = null;
					this.m_Objs[i] = optionMenuItem;
				}
				this.m_Objs = null;
			}
		}

		// Token: 0x060035F8 RID: 13816 RVA: 0x001117B9 File Offset: 0x0010FBB9
		private void OnChangeBGMVolume()
		{
			this.OnChangeVolume(OptionWindow.eOptionItem.BGM);
		}

		// Token: 0x060035F9 RID: 13817 RVA: 0x001117C2 File Offset: 0x0010FBC2
		private void OnChangeSEVolume()
		{
			this.OnChangeVolume(OptionWindow.eOptionItem.SE);
		}

		// Token: 0x060035FA RID: 13818 RVA: 0x001117CB File Offset: 0x0010FBCB
		private void OnChangeVoiceVolume()
		{
			this.OnChangeVolume(OptionWindow.eOptionItem.Voice);
		}

		// Token: 0x060035FB RID: 13819 RVA: 0x001117D4 File Offset: 0x0010FBD4
		private void OnChangeVolume(OptionWindow.eOptionItem itemType)
		{
			this.SetSoundManagerSliderValue(itemType, this.GetSliderNormalizedValue(itemType));
		}

		// Token: 0x060035FC RID: 13820 RVA: 0x001117E4 File Offset: 0x0010FBE4
		private void OnValueChanged()
		{
			this.SetDirty();
			this.RefreshRestoreButtonInteractable();
		}

		// Token: 0x060035FD RID: 13821 RVA: 0x001117F2 File Offset: 0x0010FBF2
		private void SetDirty()
		{
			this.m_IsDirty = true;
		}

		// Token: 0x060035FE RID: 13822 RVA: 0x001117FC File Offset: 0x0010FBFC
		private void RefreshRestoreButtonInteractable()
		{
			if (this.m_RestoreButton != null)
			{
				for (int i = 0; i < this.m_Objs.Length; i++)
				{
					OptionMenuItem optionMenuItem = this.m_Objs[i];
					if (optionMenuItem != null && !optionMenuItem.IsMatchValueAndDefaultValue())
					{
						this.m_RestoreButton.Interactable = true;
						return;
					}
				}
				this.m_RestoreButton.Interactable = false;
			}
		}

		// Token: 0x060035FF RID: 13823 RVA: 0x0011186C File Offset: 0x0010FC6C
		public void Decide()
		{
			if (this.m_IsDirty)
			{
				for (int i = 0; i < this.m_Objs.Length; i++)
				{
					OptionMenuItem.eType eType = OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)i);
					if (eType != OptionMenuItem.eType.Slider)
					{
						if (eType == OptionMenuItem.eType.Toggle)
						{
							this.CommitOptionDataToggleValue(i);
						}
					}
					else
					{
						this.CommitOptionDataSliderValue(i);
					}
				}
				LocalSaveData.Inst.Save();
			}
			this.Close();
		}

		// Token: 0x06003600 RID: 13824 RVA: 0x001118DF File Offset: 0x0010FCDF
		public void Cancel()
		{
			this.RevertSoundManagerSliderValue(OptionWindow.eOptionItem.BGM);
			this.RevertSoundManagerSliderValue(OptionWindow.eOptionItem.SE);
			this.RevertSoundManagerSliderValue(OptionWindow.eOptionItem.Voice);
			this.Close();
		}

		// Token: 0x06003601 RID: 13825 RVA: 0x001118FC File Offset: 0x0010FCFC
		private void ApplyDefault()
		{
			for (int i = 0; i < this.m_Objs.Length; i++)
			{
				OptionMenuItem.eType eType = OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)i);
				if (eType != OptionMenuItem.eType.Slider)
				{
					if (eType == OptionMenuItem.eType.Toggle)
					{
						this.RestoreToggleValue(i);
					}
				}
				else
				{
					this.RestoreSliderValue(i);
				}
			}
		}

		// Token: 0x06003602 RID: 13826 RVA: 0x00111954 File Offset: 0x0010FD54
		private float GetSliderStartingNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetSliderStartingNormalizedValue((int)itemType);
		}

		// Token: 0x06003603 RID: 13827 RVA: 0x00111960 File Offset: 0x0010FD60
		private float GetSliderStartingNormalizedValue(int nItemType)
		{
			if (nItemType == 0)
			{
				return LocalSaveData.Inst.BgmVolume;
			}
			if (nItemType == 1)
			{
				return LocalSaveData.Inst.SeVolume;
			}
			if (nItemType != 2)
			{
				return -1f;
			}
			return LocalSaveData.Inst.VoiceVolume;
		}

		// Token: 0x06003604 RID: 13828 RVA: 0x001119AE File Offset: 0x0010FDAE
		private bool GetToggleStartingValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetToggleStartingValue((int)itemType);
		}

		// Token: 0x06003605 RID: 13829 RVA: 0x001119B8 File Offset: 0x0010FDB8
		private bool GetToggleStartingValue(int nItemType)
		{
			if (this.m_BattleOption != null)
			{
				switch (nItemType)
				{
				case 3:
					return this.m_BattleOption.GetSpeedLv(LocalSaveData.eBattleSpeedLv.PL) != 0;
				case 4:
					return this.m_BattleOption.GetSpeedLv(LocalSaveData.eBattleSpeedLv.EN) != 0;
				case 5:
					return this.m_BattleOption.GetSpeedLv(LocalSaveData.eBattleSpeedLv.UniqueSkill) != 0;
				case 6:
					return this.m_BattleOption.GetIsSkipTogetherAttackInput();
				}
			}
			switch (nItemType)
			{
			case 3:
				return LocalSaveData.Inst.GetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.PL) != 0;
			case 4:
				return LocalSaveData.Inst.GetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.EN) != 0;
			case 5:
				return LocalSaveData.Inst.GetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.UniqueSkill) != 0;
			case 6:
				return LocalSaveData.Inst.BattleSkipTogetherAttackInput;
			case 7:
				return LocalSaveData.Inst.TownSkipGreet;
			default:
				return true;
			}
		}

		// Token: 0x06003606 RID: 13830 RVA: 0x00111AA4 File Offset: 0x0010FEA4
		private float GetSliderDefaultNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetSliderDefaultNormalizedValue((int)itemType);
		}

		// Token: 0x06003607 RID: 13831 RVA: 0x00111AB0 File Offset: 0x0010FEB0
		private float GetSliderDefaultNormalizedValue(int nItemType)
		{
			if (nItemType == 0)
			{
				return new LocalSaveData.SaveData().m_BgmVolume;
			}
			if (nItemType == 1)
			{
				return new LocalSaveData.SaveData().m_SeVolume;
			}
			if (nItemType != 2)
			{
				return -1f;
			}
			return new LocalSaveData.SaveData().m_VoiceVolume;
		}

		// Token: 0x06003608 RID: 13832 RVA: 0x00111AFE File Offset: 0x0010FEFE
		private bool GetToggleDefaultValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetToggleDefaultValue((int)itemType);
		}

		// Token: 0x06003609 RID: 13833 RVA: 0x00111B08 File Offset: 0x0010FF08
		private bool GetToggleDefaultValue(int nItemType)
		{
			switch (nItemType)
			{
			case 3:
				return new LocalSaveData.SaveData().m_BattleSpeedLvs[0] != 0;
			case 4:
				return new LocalSaveData.SaveData().m_BattleSpeedLvs[1] != 0;
			case 5:
				return new LocalSaveData.SaveData().m_BattleSpeedLvs[2] != 0;
			case 6:
				return new LocalSaveData.SaveData().m_BattleSkipTogetherAttackInput;
			case 7:
				return new LocalSaveData.SaveData().m_RoomSkipGreet;
			default:
				return true;
			}
		}

		// Token: 0x0600360A RID: 13834 RVA: 0x00111B88 File Offset: 0x0010FF88
		private void RegisterSliderStartingNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			this.RegisterSliderStartingNormalizedValue((int)itemType);
		}

		// Token: 0x0600360B RID: 13835 RVA: 0x00111B91 File Offset: 0x0010FF91
		private void RegisterSliderStartingNormalizedValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.RegisterSliderStartingNormalizedValue(this.m_Objs[nItemType], nItemType);
		}

		// Token: 0x0600360C RID: 13836 RVA: 0x00111BBB File Offset: 0x0010FFBB
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
			this.RegisterSliderStartingNormalizedValue(item, (int)itemType);
		}

		// Token: 0x0600360D RID: 13837 RVA: 0x00111BC5 File Offset: 0x0010FFC5
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, int nItemType)
		{
			this.RegisterSliderStartingNormalizedValue(item, this.GetSliderStartingNormalizedValue(nItemType));
		}

		// Token: 0x0600360E RID: 13838 RVA: 0x00111BD5 File Offset: 0x0010FFD5
		private void RegisterSliderStartingNormalizedValue(OptionMenuItem item, float value)
		{
			if (item == null)
			{
				return;
			}
			item.RegisterSliderStartingNormalizedValue(value);
		}

		// Token: 0x0600360F RID: 13839 RVA: 0x00111BEB File Offset: 0x0010FFEB
		private void RegisterToggleStartingValue(OptionWindow.eOptionItem itemType)
		{
			this.RegisterToggleStartingValue((int)itemType);
		}

		// Token: 0x06003610 RID: 13840 RVA: 0x00111BF4 File Offset: 0x0010FFF4
		private void RegisterToggleStartingValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.RegisterToggleStartingValue(this.m_Objs[nItemType], nItemType);
		}

		// Token: 0x06003611 RID: 13841 RVA: 0x00111C1E File Offset: 0x0011001E
		private void RegisterToggleStartingValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
			this.RegisterToggleStartingValue(item, (int)itemType);
		}

		// Token: 0x06003612 RID: 13842 RVA: 0x00111C28 File Offset: 0x00110028
		private void RegisterToggleStartingValue(OptionMenuItem item, int nItemType)
		{
			this.RegisterToggleStartingValue(item, this.GetToggleStartingValue(nItemType));
		}

		// Token: 0x06003613 RID: 13843 RVA: 0x00111C38 File Offset: 0x00110038
		private void RegisterToggleStartingValue(OptionMenuItem item, bool value)
		{
			if (item == null)
			{
				return;
			}
			item.RegisterToggleStartingValue(value);
		}

		// Token: 0x06003614 RID: 13844 RVA: 0x00111C4E File Offset: 0x0011004E
		private void RegisterSliderDefaultNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			this.RegisterSliderDefaultNormalizedValue((int)itemType);
		}

		// Token: 0x06003615 RID: 13845 RVA: 0x00111C57 File Offset: 0x00110057
		private void RegisterSliderDefaultNormalizedValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.RegisterSliderDefaultNormalizedValue(this.m_Objs[nItemType], nItemType);
		}

		// Token: 0x06003616 RID: 13846 RVA: 0x00111C81 File Offset: 0x00110081
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
			this.RegisterSliderDefaultNormalizedValue(item, (int)itemType);
		}

		// Token: 0x06003617 RID: 13847 RVA: 0x00111C8B File Offset: 0x0011008B
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, int nItemType)
		{
			this.RegisterSliderDefaultNormalizedValue(item, this.GetSliderDefaultNormalizedValue(nItemType));
		}

		// Token: 0x06003618 RID: 13848 RVA: 0x00111C9B File Offset: 0x0011009B
		private void RegisterSliderDefaultNormalizedValue(OptionMenuItem item, float value)
		{
			if (item == null)
			{
				return;
			}
			item.RegisterSliderDefaultNormalizedValue(value);
		}

		// Token: 0x06003619 RID: 13849 RVA: 0x00111CB1 File Offset: 0x001100B1
		private void RegisterToggleDefaultValue(OptionWindow.eOptionItem itemType)
		{
			this.RegisterToggleDefaultValue((int)itemType);
		}

		// Token: 0x0600361A RID: 13850 RVA: 0x00111CBA File Offset: 0x001100BA
		private void RegisterToggleDefaultValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.RegisterToggleDefaultValue(this.m_Objs[nItemType], nItemType);
		}

		// Token: 0x0600361B RID: 13851 RVA: 0x00111CE4 File Offset: 0x001100E4
		private void RegisterToggleDefaultValue(OptionMenuItem item, OptionWindow.eOptionItem itemType)
		{
			this.RegisterToggleDefaultValue(item, (int)itemType);
		}

		// Token: 0x0600361C RID: 13852 RVA: 0x00111CEE File Offset: 0x001100EE
		private void RegisterToggleDefaultValue(OptionMenuItem item, int nItemType)
		{
			this.RegisterToggleDefaultValue(item, this.GetToggleDefaultValue(nItemType));
		}

		// Token: 0x0600361D RID: 13853 RVA: 0x00111CFE File Offset: 0x001100FE
		private void RegisterToggleDefaultValue(OptionMenuItem item, bool value)
		{
			if (item == null)
			{
				return;
			}
			item.RegisterToggleDefaultValue(value);
		}

		// Token: 0x0600361E RID: 13854 RVA: 0x00111D14 File Offset: 0x00110114
		private void CommitOptionDataSliderValue(OptionWindow.eOptionItem itemType)
		{
			this.CommitOptionDataSliderValue((int)itemType);
		}

		// Token: 0x0600361F RID: 13855 RVA: 0x00111D20 File Offset: 0x00110120
		private void CommitOptionDataSliderValue(int nItemType)
		{
			if (nItemType == 0)
			{
				LocalSaveData.Inst.BgmVolume = this.GetSliderNormalizedValue(nItemType);
				return;
			}
			if (nItemType == 1)
			{
				LocalSaveData.Inst.SeVolume = this.GetSliderNormalizedValue(nItemType);
				return;
			}
			if (nItemType != 2)
			{
				return;
			}
			LocalSaveData.Inst.VoiceVolume = this.GetSliderNormalizedValue(nItemType);
		}

		// Token: 0x06003620 RID: 13856 RVA: 0x00111D7E File Offset: 0x0011017E
		private void CommitOptionDataToggleValue(OptionWindow.eOptionItem itemType)
		{
			this.CommitOptionDataToggleValue((int)itemType);
		}

		// Token: 0x06003621 RID: 13857 RVA: 0x00111D88 File Offset: 0x00110188
		private void CommitOptionDataToggleValue(int nItemType)
		{
			if (this.m_BattleOption != null)
			{
				switch (nItemType)
				{
				case 3:
					this.m_BattleOption.SetSpeedLv(LocalSaveData.eBattleSpeedLv.PL, (!this.GetToggleValue(nItemType)) ? 0 : 1, true);
					return;
				case 4:
					this.m_BattleOption.SetSpeedLv(LocalSaveData.eBattleSpeedLv.EN, (!this.GetToggleValue(nItemType)) ? 0 : 1, true);
					return;
				case 5:
					this.m_BattleOption.SetSpeedLv(LocalSaveData.eBattleSpeedLv.UniqueSkill, (!this.GetToggleValue(nItemType)) ? 0 : 1, true);
					return;
				case 6:
					this.m_BattleOption.SetIsSkipTogetherAttackInput(this.GetToggleValue(nItemType));
					return;
				}
			}
			switch (nItemType)
			{
			case 3:
				LocalSaveData.Inst.SetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.PL, (!this.GetToggleValue(nItemType)) ? 0 : 1);
				return;
			case 4:
				LocalSaveData.Inst.SetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.EN, (!this.GetToggleValue(nItemType)) ? 0 : 1);
				return;
			case 5:
				LocalSaveData.Inst.SetBattleSpeedLv(LocalSaveData.eBattleSpeedLv.UniqueSkill, (!this.GetToggleValue(nItemType)) ? 0 : 1);
				return;
			case 6:
				LocalSaveData.Inst.BattleSkipTogetherAttackInput = this.GetToggleValue(nItemType);
				return;
			case 7:
				LocalSaveData.Inst.TownSkipGreet = this.GetToggleValue(nItemType);
				return;
			default:
				return;
			}
		}

		// Token: 0x06003622 RID: 13858 RVA: 0x00111ED9 File Offset: 0x001102D9
		private void RevertSoundManagerSliderValue(OptionWindow.eOptionItem itemType)
		{
			this.RevertSoundManagerSliderValue((int)itemType);
		}

		// Token: 0x06003623 RID: 13859 RVA: 0x00111EE2 File Offset: 0x001102E2
		private void RevertSoundManagerSliderValue(int nItemType)
		{
			this.SetSoundManagerSliderValue(nItemType, this.GetSliderStartingNormalizedValue(nItemType));
		}

		// Token: 0x06003624 RID: 13860 RVA: 0x00111EF2 File Offset: 0x001102F2
		private void RevertSliderValue(OptionWindow.eOptionItem itemType)
		{
			this.RevertSliderValue((int)itemType);
		}

		// Token: 0x06003625 RID: 13861 RVA: 0x00111EFB File Offset: 0x001102FB
		private void RevertSliderValue(int nItemType)
		{
			this.RevertValue(nItemType);
		}

		// Token: 0x06003626 RID: 13862 RVA: 0x00111F04 File Offset: 0x00110304
		private void RevertToggleValue(OptionWindow.eOptionItem itemType)
		{
			this.RevertToggleValue((int)itemType);
		}

		// Token: 0x06003627 RID: 13863 RVA: 0x00111F0D File Offset: 0x0011030D
		private void RevertToggleValue(int nItemType)
		{
			this.RevertValue(nItemType);
		}

		// Token: 0x06003628 RID: 13864 RVA: 0x00111F16 File Offset: 0x00110316
		private void RevertValue(OptionWindow.eOptionItem itemType)
		{
			this.RevertValue((int)itemType);
		}

		// Token: 0x06003629 RID: 13865 RVA: 0x00111F1F File Offset: 0x0011031F
		private void RevertValue(int nItemType)
		{
			if (this.m_Objs == null)
			{
				return;
			}
			if (nItemType < 0 || this.m_Objs.Length <= nItemType)
			{
				return;
			}
			this.RevertValue(this.m_Objs[nItemType]);
		}

		// Token: 0x0600362A RID: 13866 RVA: 0x00111F51 File Offset: 0x00110351
		private void RevertValue(OptionMenuItem item)
		{
			if (item == null)
			{
				return;
			}
			item.Revert();
		}

		// Token: 0x0600362B RID: 13867 RVA: 0x00111F66 File Offset: 0x00110366
		private void RestoreSliderValue(OptionWindow.eOptionItem itemType)
		{
			this.RestoreSliderValue((int)itemType);
		}

		// Token: 0x0600362C RID: 13868 RVA: 0x00111F6F File Offset: 0x0011036F
		private void RestoreSliderValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			this.RestoreValue(nItemType);
		}

		// Token: 0x0600362D RID: 13869 RVA: 0x00111F85 File Offset: 0x00110385
		private void RestoreToggleValue(OptionWindow.eOptionItem itemType)
		{
			this.RestoreToggleValue((int)itemType);
		}

		// Token: 0x0600362E RID: 13870 RVA: 0x00111F8E File Offset: 0x0011038E
		private void RestoreToggleValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle)
			{
				return;
			}
			this.RestoreValue(nItemType);
		}

		// Token: 0x0600362F RID: 13871 RVA: 0x00111FA4 File Offset: 0x001103A4
		private void RestoreValue(OptionWindow.eOptionItem itemType)
		{
			this.RestoreValue((int)itemType);
		}

		// Token: 0x06003630 RID: 13872 RVA: 0x00111FAD File Offset: 0x001103AD
		private void RestoreValue(int nItemType)
		{
			if (this.m_Objs == null)
			{
				return;
			}
			if (nItemType < 0 || this.m_Objs.Length <= nItemType)
			{
				return;
			}
			this.RestoreValue(this.m_Objs[nItemType]);
		}

		// Token: 0x06003631 RID: 13873 RVA: 0x00111FDF File Offset: 0x001103DF
		private void RestoreValue(OptionMenuItem item)
		{
			if (item == null)
			{
				return;
			}
			item.Restore();
		}

		// Token: 0x06003632 RID: 13874 RVA: 0x00111FF4 File Offset: 0x001103F4
		private void SetSoundManagerSliderValue(OptionWindow.eOptionItem itemType, float value)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType(itemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.SetCategoryVolume(OptionWindow.ConverteOptionItemToSoundDefineeCategory(itemType), value, true);
		}

		// Token: 0x06003633 RID: 13875 RVA: 0x0011201A File Offset: 0x0011041A
		private void SetSoundManagerSliderValue(int nItemType, float value)
		{
			this.SetSoundManagerSliderValue((OptionWindow.eOptionItem)nItemType, value);
		}

		// Token: 0x06003634 RID: 13876 RVA: 0x00112024 File Offset: 0x00110424
		private void IsMatchValueAndStartingValue(OptionWindow.eOptionItem itemType)
		{
			this.IsMatchValueAndStartingValue((int)itemType);
		}

		// Token: 0x06003635 RID: 13877 RVA: 0x0011202D File Offset: 0x0011042D
		private void IsMatchValueAndStartingValue(int nItemType)
		{
			if (this.m_Objs == null)
			{
				return;
			}
			if (nItemType < 0 || this.m_Objs.Length <= nItemType)
			{
				return;
			}
			this.IsMatchValueAndStartingValue(this.m_Objs[nItemType]);
		}

		// Token: 0x06003636 RID: 13878 RVA: 0x0011205F File Offset: 0x0011045F
		private void IsMatchValueAndStartingValue(OptionMenuItem item)
		{
			if (item == null)
			{
				return;
			}
			item.IsMatchValueAndStartingValue();
		}

		// Token: 0x06003637 RID: 13879 RVA: 0x00112075 File Offset: 0x00110475
		private void IsMatchValueAndDefaultValue(OptionWindow.eOptionItem itemType)
		{
			this.IsMatchValueAndDefaultValue((int)itemType);
		}

		// Token: 0x06003638 RID: 13880 RVA: 0x0011207E File Offset: 0x0011047E
		private void IsMatchValueAndDefaultValue(int nItemType)
		{
			if (this.m_Objs == null)
			{
				return;
			}
			if (nItemType < 0 || this.m_Objs.Length <= nItemType)
			{
				return;
			}
			this.IsMatchValueAndDefaultValue(this.m_Objs[nItemType]);
		}

		// Token: 0x06003639 RID: 13881 RVA: 0x001120B0 File Offset: 0x001104B0
		private void IsMatchValueAndDefaultValue(OptionMenuItem item)
		{
			if (item == null)
			{
				return;
			}
			item.IsMatchValueAndDefaultValue();
		}

		// Token: 0x0600363A RID: 13882 RVA: 0x001120C6 File Offset: 0x001104C6
		private float GetSliderNormalizedValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetSliderNormalizedValue((int)itemType);
		}

		// Token: 0x0600363B RID: 13883 RVA: 0x001120CF File Offset: 0x001104CF
		private float GetSliderNormalizedValue(int nItemType)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return -1f;
			}
			if (this.m_Objs == null)
			{
				return -1f;
			}
			return this.GetSliderNormalizedValue(this.m_Objs[nItemType]);
		}

		// Token: 0x0600363C RID: 13884 RVA: 0x00112102 File Offset: 0x00110502
		private float GetSliderNormalizedValue(OptionMenuItem item)
		{
			if (item == null)
			{
				return -1f;
			}
			return item.GetSliderNormalizedValue();
		}

		// Token: 0x0600363D RID: 13885 RVA: 0x0011211C File Offset: 0x0011051C
		private bool GetToggleValue(OptionWindow.eOptionItem itemType)
		{
			return this.GetToggleValue((int)itemType);
		}

		// Token: 0x0600363E RID: 13886 RVA: 0x00112125 File Offset: 0x00110525
		private bool GetToggleValue(int nItemType)
		{
			return OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle || this.m_Objs == null || this.GetToggleValue(this.m_Objs[nItemType]);
		}

		// Token: 0x0600363F RID: 13887 RVA: 0x00112150 File Offset: 0x00110550
		private bool GetToggleValue(OptionMenuItem item)
		{
			return item == null || item.GetToggleValue();
		}

		// Token: 0x06003640 RID: 13888 RVA: 0x00112166 File Offset: 0x00110566
		private void SetSliderNormalizedValue(OptionWindow.eOptionItem itemType, float value)
		{
			this.SetSliderNormalizedValue((int)itemType, value);
		}

		// Token: 0x06003641 RID: 13889 RVA: 0x00112170 File Offset: 0x00110570
		private void SetSliderNormalizedValue(int nItemType, float value)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.SetSliderNormalizedValue(this.m_Objs[nItemType], nItemType, value);
		}

		// Token: 0x06003642 RID: 13890 RVA: 0x0011219B File Offset: 0x0011059B
		private void SetSliderNormalizedValue(OptionMenuItem item, OptionWindow.eOptionItem itemType, float value)
		{
			this.SetSliderNormalizedValue(item, (int)itemType, value);
		}

		// Token: 0x06003643 RID: 13891 RVA: 0x001121A6 File Offset: 0x001105A6
		private void SetSliderNormalizedValue(OptionMenuItem item, int nItemType, float value)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Slider)
			{
				return;
			}
			if (item == null)
			{
				return;
			}
			item.SetSliderNormalizedValue(value);
		}

		// Token: 0x06003644 RID: 13892 RVA: 0x001121C9 File Offset: 0x001105C9
		private void SetToggleValue(OptionWindow.eOptionItem itemType, bool value)
		{
			this.SetToggleValue((int)itemType, value);
		}

		// Token: 0x06003645 RID: 13893 RVA: 0x001121D3 File Offset: 0x001105D3
		private void SetToggleValue(int nItemType, bool value)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle)
			{
				return;
			}
			if (this.m_Objs == null)
			{
				return;
			}
			this.SetToggleValue(this.m_Objs[nItemType], nItemType, value);
		}

		// Token: 0x06003646 RID: 13894 RVA: 0x001121FE File Offset: 0x001105FE
		private void SetToggleValue(OptionMenuItem item, OptionWindow.eOptionItem itemType, bool value)
		{
			this.SetToggleValue(item, (int)itemType, value);
		}

		// Token: 0x06003647 RID: 13895 RVA: 0x00112209 File Offset: 0x00110609
		private void SetToggleValue(OptionMenuItem item, int nItemType, bool value)
		{
			if (OptionWindow.ConverteOptionItemToOptionMenuItemeType((OptionWindow.eOptionItem)nItemType) != OptionMenuItem.eType.Toggle)
			{
				return;
			}
			if (item == null)
			{
				return;
			}
			item.SetToggleValue(value);
		}

		// Token: 0x06003648 RID: 13896 RVA: 0x0011222C File Offset: 0x0011062C
		public void OnClickCancelButtonCallback()
		{
			if (this.m_Objs == null)
			{
				return;
			}
			for (int i = 0; i < this.m_Objs.Length; i++)
			{
				OptionMenuItem optionMenuItem = this.m_Objs[i];
				if (optionMenuItem != null && !optionMenuItem.IsMatchValueAndStartingValue())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuOptionWindowCheckCancelDialogTitle, eText_MessageDB.MenuOptionWindowCheckCancelDialogMessage, new Action<int>(this.OnClickCheckCancelDialogButtonCallback));
					return;
				}
			}
			this.OnClickCheckCancelDialogButtonCallback(0);
		}

		// Token: 0x06003649 RID: 13897 RVA: 0x001122AC File Offset: 0x001106AC
		public void OnClickCheckCancelDialogButtonCallback(int index)
		{
			if (index == 0)
			{
				this.Cancel();
			}
		}

		// Token: 0x0600364A RID: 13898 RVA: 0x001122BC File Offset: 0x001106BC
		public void OnClickDefalutButtonCallBack()
		{
			if (this.m_Objs == null)
			{
				return;
			}
			for (int i = 0; i < this.m_Objs.Length; i++)
			{
				OptionMenuItem optionMenuItem = this.m_Objs[i];
				if (optionMenuItem != null && !optionMenuItem.IsMatchValueAndDefaultValue())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.MenuOptionWindowCheckResetDialogTitle, eText_MessageDB.MenuOptionWindowCheckResetDialogMessage, new Action<int>(this.OnClickCheckDefalutDialogButtonCallBack));
					return;
				}
			}
			this.OnClickCheckDefalutDialogButtonCallBack(0);
		}

		// Token: 0x0600364B RID: 13899 RVA: 0x0011233C File Offset: 0x0011073C
		public void OnClickCheckDefalutDialogButtonCallBack(int index)
		{
			if (index == 0)
			{
				this.ApplyDefault();
			}
		}

		// Token: 0x04003C84 RID: 15492
		public OptionWindow.OptionItemData[] m_ItemDatas = new OptionWindow.OptionItemData[]
		{
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Sound, OptionWindow.eOptionItem.BGM, OptionMenuItem.eType.Slider, eText_CommonDB.MenuOptionWindowSoundBGM, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Sound, OptionWindow.eOptionItem.SE, OptionMenuItem.eType.Slider, eText_CommonDB.MenuOptionWindowSoundSE, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Sound, OptionWindow.eOptionItem.Voice, OptionMenuItem.eType.Slider, eText_CommonDB.MenuOptionWindowSoundVoice, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Battle, OptionWindow.eOptionItem.BattlePlayerSpeed, OptionMenuItem.eType.Toggle, eText_CommonDB.MenuOptionWindowBattlePlayerSpeed, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Battle, OptionWindow.eOptionItem.BattleEnemySpeed, OptionMenuItem.eType.Toggle, eText_CommonDB.MenuOptionWindowBattleEnemySpeed, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Battle, OptionWindow.eOptionItem.BattleTogetherSpeed, OptionMenuItem.eType.Toggle, eText_CommonDB.MenuOptionWindowBattleTogetherSpeed, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Battle, OptionWindow.eOptionItem.SkipTogetherAttackInput, OptionMenuItem.eType.Toggle, eText_CommonDB.MenuOptionWindowBattleSkipTogetherAttackInput, true),
			new OptionWindow.OptionItemData(OptionWindow.eCategory.Town, OptionWindow.eOptionItem.SkipGreet, OptionMenuItem.eType.Toggle, eText_CommonDB.MenuOptionWindowSkipGreet, false)
		};

		// Token: 0x04003C85 RID: 15493
		[SerializeField]
		private FixScrollRect m_Scroll;

		// Token: 0x04003C86 RID: 15494
		[SerializeField]
		private RectTransform m_ItemParentSound;

		// Token: 0x04003C87 RID: 15495
		[SerializeField]
		private RectTransform m_ItemParentBattle;

		// Token: 0x04003C88 RID: 15496
		[SerializeField]
		private RectTransform m_ItemParentTown;

		// Token: 0x04003C89 RID: 15497
		[SerializeField]
		private OptionMenuItem m_ItemSliderPrefab;

		// Token: 0x04003C8A RID: 15498
		[SerializeField]
		private OptionMenuItem m_ItemTogglePrefab;

		// Token: 0x04003C8B RID: 15499
		[SerializeField]
		private GameObject m_TownOnlyObject;

		// Token: 0x04003C8C RID: 15500
		[SerializeField]
		private CustomButton m_RestoreButton;

		// Token: 0x04003C8D RID: 15501
		private OptionMenuItem[] m_Objs = new OptionMenuItem[8];

		// Token: 0x04003C8E RID: 15502
		private bool m_IsDirty;

		// Token: 0x04003C8F RID: 15503
		private BattleOption m_BattleOption;

		// Token: 0x02000A2D RID: 2605
		public enum eCategory
		{
			// Token: 0x04003C91 RID: 15505
			Sound,
			// Token: 0x04003C92 RID: 15506
			Battle,
			// Token: 0x04003C93 RID: 15507
			Town
		}

		// Token: 0x02000A2E RID: 2606
		public enum eOptionItem
		{
			// Token: 0x04003C95 RID: 15509
			None = -1,
			// Token: 0x04003C96 RID: 15510
			BGM,
			// Token: 0x04003C97 RID: 15511
			SE,
			// Token: 0x04003C98 RID: 15512
			Voice,
			// Token: 0x04003C99 RID: 15513
			BattlePlayerSpeed,
			// Token: 0x04003C9A RID: 15514
			BattleEnemySpeed,
			// Token: 0x04003C9B RID: 15515
			BattleTogetherSpeed,
			// Token: 0x04003C9C RID: 15516
			SkipTogetherAttackInput,
			// Token: 0x04003C9D RID: 15517
			SkipGreet,
			// Token: 0x04003C9E RID: 15518
			Num
		}

		// Token: 0x02000A2F RID: 2607
		public struct OptionItemData
		{
			// Token: 0x0600364C RID: 13900 RVA: 0x0011234A File Offset: 0x0011074A
			public OptionItemData(OptionWindow.eCategory category, OptionWindow.eOptionItem option, OptionMenuItem.eType type, eText_CommonDB title, bool isBattleOption = false)
			{
				this.m_Category = category;
				this.m_Option = option;
				this.m_Type = type;
				this.m_Title = title;
				this.m_IsBattleOption = isBattleOption;
			}

			// Token: 0x0600364D RID: 13901 RVA: 0x00112371 File Offset: 0x00110771
			public OptionItemData(OptionWindow.eOptionItem option)
			{
				this.m_Option = option;
				this.m_Category = OptionWindow.ConverteOptionItemToeCategory(option);
				this.m_Type = OptionWindow.ConverteOptionItemToOptionMenuItemeType(option);
				this.m_Title = OptionWindow.ConverteOptionItemToeText_CommonDB(option);
				this.m_IsBattleOption = OptionWindow.IsBattleOption(option);
			}

			// Token: 0x04003C9F RID: 15519
			public OptionWindow.eCategory m_Category;

			// Token: 0x04003CA0 RID: 15520
			public OptionWindow.eOptionItem m_Option;

			// Token: 0x04003CA1 RID: 15521
			public OptionMenuItem.eType m_Type;

			// Token: 0x04003CA2 RID: 15522
			public eText_CommonDB m_Title;

			// Token: 0x04003CA3 RID: 15523
			public bool m_IsBattleOption;
		}
	}
}
