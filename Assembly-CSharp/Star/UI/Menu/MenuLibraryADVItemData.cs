﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x020009E6 RID: 2534
	public class MenuLibraryADVItemData : ScrollItemData
	{
		// Token: 0x06003487 RID: 13447 RVA: 0x0010A9E0 File Offset: 0x00108DE0
		public MenuLibraryADVItemData(int advID)
		{
			this.m_ADVID = advID;
			this.m_Title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(advID).m_Title;
			this.m_LockText = MenuUtility.GetLockTextCharaADV(advID);
			this.m_IsNew = !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(advID);
		}

		// Token: 0x1400009B RID: 155
		// (add) Token: 0x06003488 RID: 13448 RVA: 0x0010AA48 File Offset: 0x00108E48
		// (remove) Token: 0x06003489 RID: 13449 RVA: 0x0010AA80 File Offset: 0x00108E80
		public event Action<int> OnClickCallBack;

		// Token: 0x0600348A RID: 13450 RVA: 0x0010AAB6 File Offset: 0x00108EB6
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_ADVID);
		}

		// Token: 0x04003B07 RID: 15111
		public string m_Title;

		// Token: 0x04003B08 RID: 15112
		public int m_ADVID;

		// Token: 0x04003B09 RID: 15113
		public string m_LockText;

		// Token: 0x04003B0A RID: 15114
		public bool m_IsNew;
	}
}
