﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x020009E9 RID: 2537
	public class MenuLibraryADVLibItemData : ScrollItemData
	{
		// Token: 0x06003493 RID: 13459 RVA: 0x0010ABFC File Offset: 0x00108FFC
		public MenuLibraryADVLibItemData(int advLibID)
		{
			this.m_ADVLibID = advLibID;
			this.m_Title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVLibraryListDB.GetParam(advLibID).m_ListName;
		}

		// Token: 0x1400009D RID: 157
		// (add) Token: 0x06003494 RID: 13460 RVA: 0x0010AC3C File Offset: 0x0010903C
		// (remove) Token: 0x06003495 RID: 13461 RVA: 0x0010AC74 File Offset: 0x00109074
		public event Action<int> OnClickCallBack;

		// Token: 0x06003496 RID: 13462 RVA: 0x0010ACAA File Offset: 0x001090AA
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_ADVLibID);
		}

		// Token: 0x04003B0F RID: 15119
		public string m_Title;

		// Token: 0x04003B10 RID: 15120
		public int m_ADVLibID;
	}
}
