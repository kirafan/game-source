﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009E5 RID: 2533
	public class MenuLibraryADVItem : ScrollItemIcon
	{
		// Token: 0x06003486 RID: 13446 RVA: 0x0010A96C File Offset: 0x00108D6C
		protected override void ApplyData()
		{
			MenuLibraryADVItemData menuLibraryADVItemData = (MenuLibraryADVItemData)this.m_Data;
			if (menuLibraryADVItemData.m_LockText == null)
			{
				this.m_TitleName.text = menuLibraryADVItemData.m_Title;
				this.m_Button.Interactable = true;
			}
			else
			{
				this.m_TitleName.text = menuLibraryADVItemData.m_LockText;
				this.m_Button.Interactable = false;
			}
			this.m_NewIcon.SetActive(menuLibraryADVItemData.m_IsNew);
		}

		// Token: 0x04003B04 RID: 15108
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04003B05 RID: 15109
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04003B06 RID: 15110
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
