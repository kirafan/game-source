﻿using System;
using System.Collections.Generic;

namespace Star.UI.Menu
{
	// Token: 0x020009E4 RID: 2532
	public class MenuLibraryADVCharaItemData : ScrollItemData
	{
		// Token: 0x06003481 RID: 13441 RVA: 0x0010A7C8 File Offset: 0x00108BC8
		public MenuLibraryADVCharaItemData(eCharaNamedType named)
		{
			this.m_Named = named;
			this.m_Title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(named).m_NickName;
			this.m_CharaID = EditUtility.GetHaveNamedToCharaID(named);
			List<int> availableSingleADVList = MenuUtility.GetAvailableSingleADVList(named);
			List<int> availableCrossADVList = MenuUtility.GetAvailableCrossADVList(named);
			this.m_IsNew = false;
			for (int i = 0; i < availableSingleADVList.Count; i++)
			{
				if (MenuUtility.GetLockTextCharaADV(availableSingleADVList[i]) == null && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableSingleADVList[i]))
				{
					this.m_IsNew = true;
					break;
				}
			}
			if (!this.m_IsNew)
			{
				for (int j = 0; j < availableCrossADVList.Count; j++)
				{
					if (MenuUtility.GetLockTextCharaADV(availableCrossADVList[j]) == null && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableCrossADVList[j]))
					{
						this.m_IsNew = true;
						break;
					}
				}
			}
		}

		// Token: 0x1400009A RID: 154
		// (add) Token: 0x06003482 RID: 13442 RVA: 0x0010A8E0 File Offset: 0x00108CE0
		// (remove) Token: 0x06003483 RID: 13443 RVA: 0x0010A918 File Offset: 0x00108D18
		public event Action<eCharaNamedType> OnClickCallBack;

		// Token: 0x06003484 RID: 13444 RVA: 0x0010A94E File Offset: 0x00108D4E
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_Named);
		}

		// Token: 0x04003AFF RID: 15103
		public string m_Title;

		// Token: 0x04003B00 RID: 15104
		public eCharaNamedType m_Named;

		// Token: 0x04003B01 RID: 15105
		public int m_CharaID;

		// Token: 0x04003B02 RID: 15106
		public bool m_IsNew;
	}
}
