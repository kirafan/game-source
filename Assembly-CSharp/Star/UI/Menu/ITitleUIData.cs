﻿using System;

namespace Star.UI.Menu
{
	// Token: 0x020009FB RID: 2555
	public class ITitleUIData : ScrollItemData
	{
		// Token: 0x140000A5 RID: 165
		// (add) Token: 0x060034EB RID: 13547 RVA: 0x0010C9E4 File Offset: 0x0010ADE4
		// (remove) Token: 0x060034EC RID: 13548 RVA: 0x0010CA1C File Offset: 0x0010AE1C
		public event Action<eTitleType> OnClickTitle;

		// Token: 0x140000A6 RID: 166
		// (add) Token: 0x060034ED RID: 13549 RVA: 0x0010CA54 File Offset: 0x0010AE54
		// (remove) Token: 0x060034EE RID: 13550 RVA: 0x0010CA8C File Offset: 0x0010AE8C
		public event Action<int> OnClickWord;

		// Token: 0x060034EF RID: 13551 RVA: 0x0010CAC2 File Offset: 0x0010AEC2
		public override void OnClickItemCallBack()
		{
			this.OnClickTitle.Call(this.m_Type);
			this.OnClickWord.Call(this.m_WordIdx);
		}

		// Token: 0x04003B79 RID: 15225
		public string m_TitleName;

		// Token: 0x04003B7A RID: 15226
		public eTitleType m_Type;

		// Token: 0x04003B7B RID: 15227
		public int m_WordIdx;

		// Token: 0x04003B7C RID: 15228
		public bool m_IsSelected;
	}
}
