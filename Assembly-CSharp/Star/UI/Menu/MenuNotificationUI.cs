﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A01 RID: 2561
	public class MenuNotificationUI : MenuUIBase
	{
		// Token: 0x140000A8 RID: 168
		// (add) Token: 0x06003504 RID: 13572 RVA: 0x0010D174 File Offset: 0x0010B574
		// (remove) Token: 0x06003505 RID: 13573 RVA: 0x0010D1AC File Offset: 0x0010B5AC
		public event Action OnClickDecideButton;

		// Token: 0x06003506 RID: 13574 RVA: 0x0010D1E4 File Offset: 0x0010B5E4
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_Scroll.Setup();
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			inst.NotificationCtrl.Save();
			bool pushFlag = inst.NotificationCtrl.GetPushFlag(NotificationController.ePush.ForUIFlag);
			MenuNoticeItemData menuNoticeItemData = new MenuNoticeItemData(NotificationController.ePush.ForUIFlag, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuNotificationAll), pushFlag, true);
			menuNoticeItemData.OnSwitch += this.OnClickToggleButtonCallBack;
			this.m_AllItem.SetItemData(menuNoticeItemData);
			this.m_AllItem.Apply();
			this.m_DataList.Add(menuNoticeItemData);
			for (NotificationController.ePush ePush = NotificationController.ePush.InfoFromMng; ePush < NotificationController.ePush.Num; ePush++)
			{
				bool pushFlag2 = inst.NotificationCtrl.GetPushFlag(ePush);
				MenuNoticeItemData menuNoticeItemData2 = new MenuNoticeItemData(ePush, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuNotificationManagement + (ePush - NotificationController.ePush.InfoFromMng)), pushFlag2, pushFlag);
				this.m_DataList.Add(menuNoticeItemData2);
				menuNoticeItemData2.OnSwitch += this.OnClickToggleButtonCallBack;
				this.m_Scroll.AddItem(menuNoticeItemData2);
			}
			this.m_Scroll.Refresh();
		}

		// Token: 0x06003507 RID: 13575 RVA: 0x0010D2F4 File Offset: 0x0010B6F4
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuNotificationUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuNotificationUI.eStep.Idle);
				}
				break;
			}
			case MenuNotificationUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuNotificationUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003508 RID: 13576 RVA: 0x0010D3DC File Offset: 0x0010B7DC
		private void ChangeStep(MenuNotificationUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuNotificationUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuNotificationUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuNotificationUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuNotificationUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuNotificationUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003509 RID: 13577 RVA: 0x0010D484 File Offset: 0x0010B884
		public override void PlayIn()
		{
			this.ChangeStep(MenuNotificationUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x0600350A RID: 13578 RVA: 0x0010D4CC File Offset: 0x0010B8CC
		public override void PlayOut()
		{
			this.ChangeStep(MenuNotificationUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x0600350B RID: 13579 RVA: 0x0010D511 File Offset: 0x0010B911
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuNotificationUI.eStep.End;
		}

		// Token: 0x0600350C RID: 13580 RVA: 0x0010D51C File Offset: 0x0010B91C
		public void OnDecideButtonCallBack()
		{
			this.OnClickDecideButton.Call();
		}

		// Token: 0x0600350D RID: 13581 RVA: 0x0010D529 File Offset: 0x0010B929
		public void OnBackButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.NotificationCtrl.RestoreFromSave();
			this.PlayOut();
		}

		// Token: 0x0600350E RID: 13582 RVA: 0x0010D540 File Offset: 0x0010B940
		public void OnClickToggleButtonCallBack(NotificationController.ePush push, bool flg)
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			if (push == NotificationController.ePush.ForUIFlag)
			{
				if (!flg)
				{
					inst.NotificationCtrl.SetPushFlag(NotificationController.ePush.ForUIFlag, false);
					for (int i = 1; i < this.m_DataList.Count; i++)
					{
						this.m_DataList[i].m_Flag = false;
						this.m_DataList[i].m_Interactable = false;
						inst.NotificationCtrl.SetPushFlag((NotificationController.ePush)i, false);
					}
				}
				else
				{
					inst.NotificationCtrl.SetPushFlag(NotificationController.ePush.ForUIFlag, true);
					for (int j = 1; j < this.m_DataList.Count; j++)
					{
						this.m_DataList[j].m_Flag = true;
						this.m_DataList[j].m_Interactable = true;
						inst.NotificationCtrl.SetPushFlag((NotificationController.ePush)j, true);
					}
				}
				this.m_AllItem.Apply();
				this.m_Scroll.ForceApply();
			}
			else
			{
				inst.NotificationCtrl.SetPushFlag(push, flg);
				this.m_Scroll.ForceApply();
			}
		}

		// Token: 0x04003B9C RID: 15260
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003B9D RID: 15261
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003B9E RID: 15262
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003B9F RID: 15263
		[SerializeField]
		private MenuNoticeItem m_AllItem;

		// Token: 0x04003BA0 RID: 15264
		private MenuNotificationUI.eStep m_Step;

		// Token: 0x04003BA1 RID: 15265
		private List<MenuNoticeItemData> m_DataList = new List<MenuNoticeItemData>();

		// Token: 0x02000A02 RID: 2562
		private enum eStep
		{
			// Token: 0x04003BA4 RID: 15268
			Hide,
			// Token: 0x04003BA5 RID: 15269
			In,
			// Token: 0x04003BA6 RID: 15270
			Idle,
			// Token: 0x04003BA7 RID: 15271
			Out,
			// Token: 0x04003BA8 RID: 15272
			End
		}

		// Token: 0x02000A03 RID: 2563
		public enum eNotice
		{
			// Token: 0x04003BAA RID: 15274
			All,
			// Token: 0x04003BAB RID: 15275
			Info,
			// Token: 0x04003BAC RID: 15276
			Stamina,
			// Token: 0x04003BAD RID: 15277
			Num
		}
	}
}
