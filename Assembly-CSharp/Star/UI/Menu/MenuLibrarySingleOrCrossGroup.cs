﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009F3 RID: 2547
	public class MenuLibrarySingleOrCrossGroup : UIGroup
	{
		// Token: 0x060034CB RID: 13515 RVA: 0x0010C180 File Offset: 0x0010A580
		public void Open(eCharaNamedType named)
		{
			base.Open();
			bool interactable = false;
			List<int> haveNamedToCharaIDList = EditUtility.GetHaveNamedToCharaIDList(named);
			for (int i = 0; i < haveNamedToCharaIDList.Count; i++)
			{
				if (!string.IsNullOrEmpty(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaFlavorTextDB.GetFlavorText(haveNamedToCharaIDList[i])))
				{
					interactable = true;
					break;
				}
			}
			this.m_SummonButton.Interactable = interactable;
			List<int> availableSingleADVList = MenuUtility.GetAvailableSingleADVList(named);
			List<int> availableCrossADVList = MenuUtility.GetAvailableCrossADVList(named);
			int num = 0;
			int num2 = 0;
			this.m_SingleNew.SetActive(false);
			for (int j = 0; j < availableSingleADVList.Count; j++)
			{
				if (MenuUtility.GetLockTextCharaADV(availableSingleADVList[j]) == null)
				{
					num++;
					if (!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableSingleADVList[j]))
					{
						this.m_SingleNew.SetActive(true);
					}
				}
			}
			this.m_CrossNew.SetActive(false);
			for (int k = 0; k < availableCrossADVList.Count; k++)
			{
				if (MenuUtility.GetLockTextCharaADV(availableCrossADVList[k]) == null)
				{
					num2++;
					if (!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableCrossADVList[k]))
					{
						this.m_CrossNew.SetActive(true);
					}
				}
			}
			this.m_BustImage.Apply(EditUtility.GetHaveNamedToCharaID(named), BustImage.eBustImageType.Chara);
			this.m_NameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(named).m_NickName;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(num.ToString().PadLeft(2));
			stringBuilder.Append("/");
			stringBuilder.Append(availableSingleADVList.Count.ToString().PadLeft(2));
			this.m_SingleText.text = stringBuilder.ToString();
			stringBuilder.Length = 0;
			stringBuilder.Append(num2.ToString().PadLeft(2));
			stringBuilder.Append("/");
			stringBuilder.Append(availableCrossADVList.Count.ToString().PadLeft(2));
			this.m_CrossText.text = stringBuilder.ToString();
			stringBuilder.Length = 0;
			this.m_SingleButton.Interactable = (num > 0);
			this.m_CrossButton.Interactable = (num2 > 0);
		}

		// Token: 0x04003B56 RID: 15190
		[SerializeField]
		private CustomButton m_SummonButton;

		// Token: 0x04003B57 RID: 15191
		[SerializeField]
		private CustomButton m_SingleButton;

		// Token: 0x04003B58 RID: 15192
		[SerializeField]
		private CustomButton m_CrossButton;

		// Token: 0x04003B59 RID: 15193
		[SerializeField]
		private Text m_SingleText;

		// Token: 0x04003B5A RID: 15194
		[SerializeField]
		private Text m_CrossText;

		// Token: 0x04003B5B RID: 15195
		[SerializeField]
		private GameObject m_SingleNew;

		// Token: 0x04003B5C RID: 15196
		[SerializeField]
		private GameObject m_CrossNew;

		// Token: 0x04003B5D RID: 15197
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003B5E RID: 15198
		[SerializeField]
		private BustImage m_BustImage;
	}
}
