﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009FC RID: 2556
	public class MenuLibraryWordUI : MenuUIBase
	{
		// Token: 0x060034F1 RID: 13553 RVA: 0x0010CAFC File Offset: 0x0010AEFC
		public void Setup(MenuLibraryWordUI.eMode mode)
		{
			this.m_Mode = mode;
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_DescriptText.text = string.Empty;
			this.m_TitleList.Setup();
			if (this.m_Mode == MenuLibraryWordUI.eMode.ContentTitle)
			{
				this.m_TitleObj.SetActive(true);
				this.m_WordObj.SetActive(false);
				eTitleType eTitleType = eTitleType.None;
				eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
				for (int i = 0; i < eTitleTypeConverter.HowManyTitles(); i++)
				{
					ITitleUIData titleUIData = new ITitleUIData();
					titleUIData.m_TitleName = eTitleTypeConverter.ConvertFromOrderToTitleName(i);
					titleUIData.m_Type = eTitleTypeConverter.ConvertFromOrderToTitleType(i);
					titleUIData.OnClickTitle += this.OnClickTitleButtonCallBack;
					this.m_TitleList.AddItem(titleUIData);
					this.m_DataList.Add(titleUIData);
					if (i == 0)
					{
						eTitleType = titleUIData.m_Type;
					}
				}
				if (eTitleType != eTitleType.None)
				{
					this.OnClickTitleButtonCallBack(eTitleType);
				}
			}
			else if (this.m_Mode == MenuLibraryWordUI.eMode.Word)
			{
				this.m_WordText.text = string.Empty;
				this.m_TitleObj.SetActive(false);
				this.m_WordObj.SetActive(true);
				int num = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WordLibraryListDB.m_Params.Length;
				for (int i = 0; i < num; i++)
				{
					ITitleUIData titleUIData = new ITitleUIData();
					titleUIData.m_TitleName = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WordLibraryListDB.m_Params[i].m_TitleName;
					titleUIData.m_WordIdx = i;
					titleUIData.OnClickWord += this.OnClickWordButtonCallBack;
					this.m_TitleList.AddItem(titleUIData);
					this.m_DataList.Add(titleUIData);
				}
				this.OnClickWordButtonCallBack(0);
				this.m_ScrollField.FitTarget(null);
			}
		}

		// Token: 0x060034F2 RID: 13554 RVA: 0x0010CCC8 File Offset: 0x0010B0C8
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuLibraryWordUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuLibraryWordUI.eStep.Idle);
				}
				break;
			}
			case MenuLibraryWordUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuLibraryWordUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060034F3 RID: 13555 RVA: 0x0010CDB0 File Offset: 0x0010B1B0
		private void ChangeStep(MenuLibraryWordUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuLibraryWordUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuLibraryWordUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryWordUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuLibraryWordUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryWordUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060034F4 RID: 13556 RVA: 0x0010CE58 File Offset: 0x0010B258
		public override void PlayIn()
		{
			this.ChangeStep(MenuLibraryWordUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x060034F5 RID: 13557 RVA: 0x0010CEA0 File Offset: 0x0010B2A0
		public override void PlayOut()
		{
			this.ChangeStep(MenuLibraryWordUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x060034F6 RID: 13558 RVA: 0x0010CEE5 File Offset: 0x0010B2E5
		public TitleListDB GetTitleListDB()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB;
		}

		// Token: 0x060034F7 RID: 13559 RVA: 0x0010CEF6 File Offset: 0x0010B2F6
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuLibraryWordUI.eStep.End;
		}

		// Token: 0x060034F8 RID: 13560 RVA: 0x0010CF01 File Offset: 0x0010B301
		public void OnBackButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x060034F9 RID: 13561 RVA: 0x0010CF0C File Offset: 0x0010B30C
		private void OnClickTitleButtonCallBack(eTitleType ftitle)
		{
			TitleListDB titleListDB = this.GetTitleListDB();
			for (int i = 0; i < this.m_DataList.Count; i++)
			{
				this.m_DataList[i].m_IsSelected = (this.m_DataList[i].m_Type == ftitle);
			}
			this.m_TitleList.ForceApply();
			this.m_TitleLogo.Apply(ftitle);
			this.m_DescriptText.text = titleListDB.GetParam(ftitle).m_Descript;
		}

		// Token: 0x060034FA RID: 13562 RVA: 0x0010CF94 File Offset: 0x0010B394
		private void OnClickWordButtonCallBack(int idx)
		{
			for (int i = 0; i < this.m_DataList.Count; i++)
			{
				this.m_DataList[i].m_IsSelected = (i == idx);
			}
			this.m_TitleList.ForceApply();
			WordLibraryListDB_Param wordLibraryListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WordLibraryListDB.m_Params[idx];
			this.m_WordText.text = wordLibraryListDB_Param.m_TitleName;
			this.m_WordDescriptText.text = wordLibraryListDB_Param.m_Descript;
		}

		// Token: 0x04003B7F RID: 15231
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003B80 RID: 15232
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003B81 RID: 15233
		[SerializeField]
		private GameObject m_TitleObj;

		// Token: 0x04003B82 RID: 15234
		[SerializeField]
		private GameObject m_WordObj;

		// Token: 0x04003B83 RID: 15235
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x04003B84 RID: 15236
		[SerializeField]
		private Text m_WordText;

		// Token: 0x04003B85 RID: 15237
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04003B86 RID: 15238
		[SerializeField]
		private Text m_WordDescriptText;

		// Token: 0x04003B87 RID: 15239
		[SerializeField]
		private ScrollField m_ScrollField;

		// Token: 0x04003B88 RID: 15240
		[SerializeField]
		private ScrollViewBase m_TitleList;

		// Token: 0x04003B89 RID: 15241
		private List<ITitleUIData> m_DataList = new List<ITitleUIData>();

		// Token: 0x04003B8A RID: 15242
		private MenuLibraryWordUI.eStep m_Step;

		// Token: 0x04003B8B RID: 15243
		private MenuLibraryWordUI.eMode m_Mode;

		// Token: 0x020009FD RID: 2557
		private enum eStep
		{
			// Token: 0x04003B8D RID: 15245
			Hide,
			// Token: 0x04003B8E RID: 15246
			In,
			// Token: 0x04003B8F RID: 15247
			Idle,
			// Token: 0x04003B90 RID: 15248
			Out,
			// Token: 0x04003B91 RID: 15249
			End
		}

		// Token: 0x020009FE RID: 2558
		public enum eMode
		{
			// Token: 0x04003B93 RID: 15251
			ContentTitle,
			// Token: 0x04003B94 RID: 15252
			Word
		}
	}
}
