﻿using System;
using System.Collections.Generic;

namespace Star.UI.Menu
{
	// Token: 0x020009ED RID: 2541
	public class MenuLibraryADVTitleItemData : ScrollItemData
	{
		// Token: 0x060034A5 RID: 13477 RVA: 0x0010B01C File Offset: 0x0010941C
		public MenuLibraryADVTitleItemData(eTitleType titleType)
		{
			this.m_Title = titleType;
			this.m_IsNew = false;
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas.Count; i++)
			{
				eCharaNamedType namedType = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[i].Param.NamedType;
				if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(namedType).m_TitleType == (int)this.m_Title)
				{
					List<int> availableSingleADVList = MenuUtility.GetAvailableSingleADVList(namedType);
					List<int> availableCrossADVList = MenuUtility.GetAvailableCrossADVList(namedType);
					for (int j = 0; j < availableSingleADVList.Count; j++)
					{
						if (MenuUtility.GetLockTextCharaADV(availableSingleADVList[j]) == null && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableSingleADVList[j]))
						{
							this.m_IsNew = true;
							break;
						}
					}
					if (!this.m_IsNew)
					{
						for (int k = 0; k < availableCrossADVList.Count; k++)
						{
							if (MenuUtility.GetLockTextCharaADV(availableCrossADVList[k]) == null && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(availableCrossADVList[k]))
							{
								this.m_IsNew = true;
								break;
							}
						}
					}
					if (this.m_IsNew)
					{
						break;
					}
				}
			}
		}

		// Token: 0x140000A0 RID: 160
		// (add) Token: 0x060034A6 RID: 13478 RVA: 0x0010B190 File Offset: 0x00109590
		// (remove) Token: 0x060034A7 RID: 13479 RVA: 0x0010B1C8 File Offset: 0x001095C8
		public event Action<eTitleType> OnClickCallBack;

		// Token: 0x060034A8 RID: 13480 RVA: 0x0010B1FE File Offset: 0x001095FE
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_Title);
		}

		// Token: 0x04003B19 RID: 15129
		public eTitleType m_Title;

		// Token: 0x04003B1A RID: 15130
		public bool m_IsNew;
	}
}
