﻿using System;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000A13 RID: 2579
	public class MenuUserInfomationUI : MenuUIBase
	{
		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06003558 RID: 13656 RVA: 0x0010E88C File Offset: 0x0010CC8C
		// (set) Token: 0x06003559 RID: 13657 RVA: 0x0010E894 File Offset: 0x0010CC94
		public bool m_IsDirtyUserData { get; set; }

		// Token: 0x0600355A RID: 13658 RVA: 0x0010E89D File Offset: 0x0010CC9D
		public string GetInputUserName()
		{
			return this.m_UserName.text;
		}

		// Token: 0x0600355B RID: 13659 RVA: 0x0010E8AA File Offset: 0x0010CCAA
		public string GetInputCommentName()
		{
			return this.m_UserComment.text;
		}

		// Token: 0x0600355C RID: 13660 RVA: 0x0010E8B8 File Offset: 0x0010CCB8
		public void Setup()
		{
			this.m_IsDirtyUserData = false;
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_UserName.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Name;
			this.m_UserComment.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Comment;
			this.m_MyCode.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode;
			long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
			for (int i = 0; i < roomInCharaManageID.Length; i++)
			{
				if (roomInCharaManageID[i] != -1L && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInChara(roomInCharaManageID[i]).LiveIdx == 0)
				{
					CharacterParam param = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(roomInCharaManageID[i]).Param;
					this.m_CharaIllust.Apply(param.CharaID, CharaIllust.eCharaIllustType.Card);
					break;
				}
			}
		}

		// Token: 0x0600355D RID: 13661 RVA: 0x0010E9C8 File Offset: 0x0010CDC8
		public void Refresh()
		{
			this.m_IsDirtyUserData = false;
			this.m_UserName.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Name;
			this.m_UserComment.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Comment;
		}

		// Token: 0x0600355E RID: 13662 RVA: 0x0010EA1C File Offset: 0x0010CE1C
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuUserInfomationUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuUserInfomationUI.eStep.Idle);
				}
				break;
			}
			case MenuUserInfomationUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuUserInfomationUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600355F RID: 13663 RVA: 0x0010EB04 File Offset: 0x0010CF04
		private void ChangeStep(MenuUserInfomationUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuUserInfomationUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuUserInfomationUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuUserInfomationUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuUserInfomationUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuUserInfomationUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003560 RID: 13664 RVA: 0x0010EBAC File Offset: 0x0010CFAC
		public override void PlayIn()
		{
			this.ChangeStep(MenuUserInfomationUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003561 RID: 13665 RVA: 0x0010EBF4 File Offset: 0x0010CFF4
		public override void PlayOut()
		{
			this.ChangeStep(MenuUserInfomationUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003562 RID: 13666 RVA: 0x0010EC39 File Offset: 0x0010D039
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuUserInfomationUI.eStep.End;
		}

		// Token: 0x06003563 RID: 13667 RVA: 0x0010EC44 File Offset: 0x0010D044
		public void OnCopyButtonCallBack()
		{
			Clipboard.value = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.MyCode;
		}

		// Token: 0x06003564 RID: 13668 RVA: 0x0010EC5F File Offset: 0x0010D05F
		public void OnClickUserNameCallBack()
		{
			this.m_UserNameInput.SetDecideInteractable(false);
			this.m_UserNameInput.SetText(this.m_UserName.text);
			this.m_UserNameInputGroup.Open();
		}

		// Token: 0x06003565 RID: 13669 RVA: 0x0010EC8E File Offset: 0x0010D08E
		public void OnDecideUserNameCallBack()
		{
			this.m_UserName.text = this.m_UserNameInput.GetText();
			this.m_IsDirtyUserData = true;
			this.m_UserNameInputGroup.Close();
		}

		// Token: 0x06003566 RID: 13670 RVA: 0x0010ECB8 File Offset: 0x0010D0B8
		public void OnClickUserCommentCallBack()
		{
			this.m_UserCommentInput.SetDecideInteractable(false);
			this.m_UserCommentInput.SetText(this.m_UserComment.text);
			this.m_UserCommentInputGroup.Open();
		}

		// Token: 0x06003567 RID: 13671 RVA: 0x0010ECE7 File Offset: 0x0010D0E7
		public void OnDecideUserCommentCallBack()
		{
			this.m_UserComment.text = this.m_UserCommentInput.GetText();
			this.m_IsDirtyUserData = true;
			this.m_UserCommentInputGroup.Close();
		}

		// Token: 0x04003C04 RID: 15364
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003C05 RID: 15365
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003C06 RID: 15366
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04003C07 RID: 15367
		private MenuUserInfomationUI.eStep m_Step;

		// Token: 0x04003C08 RID: 15368
		[SerializeField]
		private UIGroup m_UserNameInputGroup;

		// Token: 0x04003C09 RID: 15369
		[SerializeField]
		private TextInput m_UserNameInput;

		// Token: 0x04003C0A RID: 15370
		[SerializeField]
		private Text m_UserName;

		// Token: 0x04003C0B RID: 15371
		[SerializeField]
		private UIGroup m_UserCommentInputGroup;

		// Token: 0x04003C0C RID: 15372
		[SerializeField]
		private TextInput m_UserCommentInput;

		// Token: 0x04003C0D RID: 15373
		[SerializeField]
		private Text m_UserComment;

		// Token: 0x04003C0E RID: 15374
		[SerializeField]
		private Text m_MyCode;

		// Token: 0x02000A14 RID: 2580
		private enum eStep
		{
			// Token: 0x04003C11 RID: 15377
			Hide,
			// Token: 0x04003C12 RID: 15378
			In,
			// Token: 0x04003C13 RID: 15379
			Idle,
			// Token: 0x04003C14 RID: 15380
			Out,
			// Token: 0x04003C15 RID: 15381
			End
		}
	}
}
