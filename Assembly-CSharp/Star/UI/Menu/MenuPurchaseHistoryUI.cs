﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x02000A09 RID: 2569
	public class MenuPurchaseHistoryUI : MenuUIBase
	{
		// Token: 0x0600352C RID: 13612 RVA: 0x0010DD13 File Offset: 0x0010C113
		private void Start()
		{
		}

		// Token: 0x0600352D RID: 13613 RVA: 0x0010DD18 File Offset: 0x0010C118
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_Scroll.Setup();
			long gem = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem;
			List<StoreManager.PurchaseLog> purchaseLogs = SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.GetPurchaseLogs();
			if (false || purchaseLogs == null || purchaseLogs.Count <= 0 || false)
			{
				List<StoreManager.PurchaseLog> purchaseLogs2 = SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.GetPurchaseLogs();
				for (int i = 0; i < purchaseLogs2.Count; i++)
				{
					purchaseLogs.Add(new StoreManager.PurchaseLog
					{
						m_Name = purchaseLogs2[i].m_Name,
						m_Date = purchaseLogs2[i].m_Date,
						m_Price = purchaseLogs2[i].m_Price
					});
				}
			}
			this.m_PaidGemField.SetValue(UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.UnlimitedGem, true));
			this.m_FreeGemField.SetValue(UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.LimitedGem, true));
			this.m_AllGemField.SetValue(UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true));
			for (int j = 0; j < purchaseLogs.Count; j++)
			{
				MenuPurchaseItemData item = new MenuPurchaseItemData(purchaseLogs[j]);
				this.m_DataList.Add(item);
				this.m_Scroll.AddItem(item);
			}
			this.m_Scroll.Refresh();
		}

		// Token: 0x0600352E RID: 13614 RVA: 0x0010DEAC File Offset: 0x0010C2AC
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuPurchaseHistoryUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuPurchaseHistoryUI.eStep.Idle);
				}
				break;
			}
			case MenuPurchaseHistoryUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuPurchaseHistoryUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600352F RID: 13615 RVA: 0x0010DF94 File Offset: 0x0010C394
		private void ChangeStep(MenuPurchaseHistoryUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuPurchaseHistoryUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuPurchaseHistoryUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuPurchaseHistoryUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuPurchaseHistoryUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuPurchaseHistoryUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003530 RID: 13616 RVA: 0x0010E03C File Offset: 0x0010C43C
		public override void PlayIn()
		{
			this.ChangeStep(MenuPurchaseHistoryUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003531 RID: 13617 RVA: 0x0010E084 File Offset: 0x0010C484
		public override void PlayOut()
		{
			this.ChangeStep(MenuPurchaseHistoryUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003532 RID: 13618 RVA: 0x0010E0C9 File Offset: 0x0010C4C9
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuPurchaseHistoryUI.eStep.End;
		}

		// Token: 0x06003533 RID: 13619 RVA: 0x0010E0D4 File Offset: 0x0010C4D4
		public void OnDecideButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x06003534 RID: 13620 RVA: 0x0010E0DC File Offset: 0x0010C4DC
		public void OnBackButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x06003535 RID: 13621 RVA: 0x0010E0E4 File Offset: 0x0010C4E4
		public void OnClickAllButtonCallBack()
		{
		}

		// Token: 0x06003536 RID: 13622 RVA: 0x0010E0E6 File Offset: 0x0010C4E6
		public void OnClickToggleButtonCallBack()
		{
			this.m_Scroll.ForceApply();
		}

		// Token: 0x04003BCE RID: 15310
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003BCF RID: 15311
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003BD0 RID: 15312
		[SerializeField]
		private TextField m_PaidGemField;

		// Token: 0x04003BD1 RID: 15313
		[SerializeField]
		private TextField m_FreeGemField;

		// Token: 0x04003BD2 RID: 15314
		[SerializeField]
		private TextField m_AllGemField;

		// Token: 0x04003BD3 RID: 15315
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003BD4 RID: 15316
		private MenuPurchaseHistoryUI.eStep m_Step;

		// Token: 0x04003BD5 RID: 15317
		private List<MenuPurchaseItemData> m_DataList = new List<MenuPurchaseItemData>();

		// Token: 0x02000A0A RID: 2570
		private enum eStep
		{
			// Token: 0x04003BD7 RID: 15319
			Hide,
			// Token: 0x04003BD8 RID: 15320
			In,
			// Token: 0x04003BD9 RID: 15321
			Idle,
			// Token: 0x04003BDA RID: 15322
			Out,
			// Token: 0x04003BDB RID: 15323
			End
		}
	}
}
