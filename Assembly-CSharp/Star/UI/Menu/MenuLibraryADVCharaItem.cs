﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009E3 RID: 2531
	public class MenuLibraryADVCharaItem : ScrollItemIcon
	{
		// Token: 0x0600347F RID: 13439 RVA: 0x0010A768 File Offset: 0x00108B68
		protected override void ApplyData()
		{
			MenuLibraryADVCharaItemData menuLibraryADVCharaItemData = (MenuLibraryADVCharaItemData)this.m_Data;
			this.m_TitleName.text = menuLibraryADVCharaItemData.m_Title;
			this.m_BustImage.Apply(menuLibraryADVCharaItemData.m_CharaID, BustImage.eBustImageType.Chara);
			this.m_NewIcon.SetActive(menuLibraryADVCharaItemData.m_IsNew);
		}

		// Token: 0x06003480 RID: 13440 RVA: 0x0010A7B5 File Offset: 0x00108BB5
		public override void Destroy()
		{
			base.Destroy();
			this.m_BustImage.Destroy();
		}

		// Token: 0x04003AFC RID: 15100
		[SerializeField]
		private Text m_TitleName;

		// Token: 0x04003AFD RID: 15101
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x04003AFE RID: 15102
		[SerializeField]
		private GameObject m_NewIcon;
	}
}
