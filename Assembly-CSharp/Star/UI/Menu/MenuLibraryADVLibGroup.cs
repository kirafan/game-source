﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009E7 RID: 2535
	public class MenuLibraryADVLibGroup : UIGroup
	{
		// Token: 0x1400009C RID: 156
		// (add) Token: 0x0600348C RID: 13452 RVA: 0x0010AAD4 File Offset: 0x00108ED4
		// (remove) Token: 0x0600348D RID: 13453 RVA: 0x0010AB0C File Offset: 0x00108F0C
		public event Action<int> OnClickADVLib;

		// Token: 0x0600348E RID: 13454 RVA: 0x0010AB42 File Offset: 0x00108F42
		public void Setup()
		{
			this.m_Scroll.Setup();
		}

		// Token: 0x0600348F RID: 13455 RVA: 0x0010AB50 File Offset: 0x00108F50
		public void Open(eADVLibraryCategory category)
		{
			this.m_Scroll.RemoveAllData();
			List<int> availableADVLibrary = MenuUtility.GetAvailableADVLibrary(category);
			for (int i = 0; i < availableADVLibrary.Count; i++)
			{
				MenuLibraryADVLibItemData menuLibraryADVLibItemData = new MenuLibraryADVLibItemData(availableADVLibrary[i]);
				menuLibraryADVLibItemData.OnClickCallBack += this.OnClickADVLibCallBack;
				this.m_Scroll.AddItem(menuLibraryADVLibItemData);
			}
			base.Open();
		}

		// Token: 0x06003490 RID: 13456 RVA: 0x0010ABB7 File Offset: 0x00108FB7
		private void OnClickADVLibCallBack(int advLibID)
		{
			this.OnClickADVLib.Call(advLibID);
		}

		// Token: 0x04003B0C RID: 15116
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
