﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x02000A06 RID: 2566
	public class MenuPlayerMoveConfigurationUI : MenuUIBase
	{
		// Token: 0x140000A9 RID: 169
		// (add) Token: 0x06003519 RID: 13593 RVA: 0x0010D8B8 File Offset: 0x0010BCB8
		// (remove) Token: 0x0600351A RID: 13594 RVA: 0x0010D8F0 File Offset: 0x0010BCF0
		public event Action<string> m_OnClickInputPasswordDecideButton;

		// Token: 0x140000AA RID: 170
		// (add) Token: 0x0600351B RID: 13595 RVA: 0x0010D928 File Offset: 0x0010BD28
		// (remove) Token: 0x0600351C RID: 13596 RVA: 0x0010D960 File Offset: 0x0010BD60
		public event Action m_OnClickPlayerMoveDataCloseButton;

		// Token: 0x0600351D RID: 13597 RVA: 0x0010D996 File Offset: 0x0010BD96
		private void Start()
		{
		}

		// Token: 0x0600351E RID: 13598 RVA: 0x0010D998 File Offset: 0x0010BD98
		public void Setup()
		{
			this.m_IsDecided = false;
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x0600351F RID: 13599 RVA: 0x0010D9B0 File Offset: 0x0010BDB0
		private void Update()
		{
			switch (this.m_Step)
			{
			case MenuPlayerMoveConfigurationUI.eStep.In:
				if (this.m_ActiveUIGroup.IsDonePlayIn)
				{
					this.ChangeStep(MenuPlayerMoveConfigurationUI.eStep.Idle);
				}
				break;
			case MenuPlayerMoveConfigurationUI.eStep.Idle:
				if (this.m_ActiveWindowType == MenuPlayerMoveConfigurationUI.eWindowType.InputPassword)
				{
					bool interactable = true;
					if (this.m_PassWordInputField != null)
					{
						if (this.m_PassWordInputField.text.Length < 4)
						{
							interactable = false;
						}
					}
					else
					{
						interactable = false;
					}
					if (this.m_InputPasswordDecideButton != null)
					{
						this.m_InputPasswordDecideButton.Interactable = interactable;
					}
				}
				break;
			case MenuPlayerMoveConfigurationUI.eStep.Out:
				if (this.m_ActiveUIGroup.IsDonePlayOut)
				{
					if (this.IsDecided() && this.m_ActiveWindowType == MenuPlayerMoveConfigurationUI.eWindowType.InputPassword)
					{
						this.m_OnClickInputPasswordDecideButton(this.m_PassWordInputField.text);
					}
					this.ChangeStep(MenuPlayerMoveConfigurationUI.eStep.End);
				}
				break;
			}
		}

		// Token: 0x06003520 RID: 13600 RVA: 0x0010DAB4 File Offset: 0x0010BEB4
		private void ChangeStep(MenuPlayerMoveConfigurationUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuPlayerMoveConfigurationUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuPlayerMoveConfigurationUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuPlayerMoveConfigurationUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuPlayerMoveConfigurationUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuPlayerMoveConfigurationUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003521 RID: 13601 RVA: 0x0010DB5A File Offset: 0x0010BF5A
		public override void PlayIn()
		{
			if (this.m_ActiveWindowType != MenuPlayerMoveConfigurationUI.eWindowType.None)
			{
				if (this.m_InputPasswordDecideButton != null)
				{
					this.m_InputPasswordDecideButton.Interactable = false;
				}
				this.ChangeStep(MenuPlayerMoveConfigurationUI.eStep.In);
				this.m_ActiveUIGroup.Open();
			}
		}

		// Token: 0x06003522 RID: 13602 RVA: 0x0010DB96 File Offset: 0x0010BF96
		public override void PlayOut()
		{
			this.ChangeStep(MenuPlayerMoveConfigurationUI.eStep.Out);
			this.m_ActiveUIGroup.Close();
		}

		// Token: 0x06003523 RID: 13603 RVA: 0x0010DBAA File Offset: 0x0010BFAA
		public MenuPlayerMoveConfigurationUI.eWindowType GetActiveWindowType()
		{
			return this.m_ActiveWindowType;
		}

		// Token: 0x06003524 RID: 13604 RVA: 0x0010DBB4 File Offset: 0x0010BFB4
		public void SetOpenWindowType(MenuPlayerMoveConfigurationUI.eWindowType openWindowType)
		{
			if (this.m_Step == MenuPlayerMoveConfigurationUI.eStep.Hide || this.m_Step == MenuPlayerMoveConfigurationUI.eStep.End)
			{
				this.m_ActiveWindowType = openWindowType;
				if (this.m_ActiveWindowType == MenuPlayerMoveConfigurationUI.eWindowType.InputPassword)
				{
					this.m_ActiveUIGroup = this.m_InputPasswordGroup;
				}
				else if (this.m_ActiveWindowType == MenuPlayerMoveConfigurationUI.eWindowType.PlayerMoveData)
				{
					this.m_ActiveUIGroup = this.m_ResultGroup;
				}
				this.m_Step = MenuPlayerMoveConfigurationUI.eStep.Hide;
			}
		}

		// Token: 0x06003525 RID: 13605 RVA: 0x0010DC1C File Offset: 0x0010C01C
		public void SetInheritanceWindowIdPassword(string moveCode, string movePassward, DateTime moveDeadLine)
		{
			if (this.m_ResultGroup != null)
			{
				this.m_IDText.text = moveCode;
				this.m_PassWordText.text = movePassward;
				this.m_DeadlineText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDeadlineValue, new object[]
				{
					moveDeadLine.Year,
					moveDeadLine.Month,
					moveDeadLine.Day,
					moveDeadLine.Hour,
					moveDeadLine.Minute
				});
			}
		}

		// Token: 0x06003526 RID: 13606 RVA: 0x0010DCC2 File Offset: 0x0010C0C2
		public bool IsDecided()
		{
			return this.m_IsDecided;
		}

		// Token: 0x06003527 RID: 13607 RVA: 0x0010DCCA File Offset: 0x0010C0CA
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuPlayerMoveConfigurationUI.eStep.End;
		}

		// Token: 0x06003528 RID: 13608 RVA: 0x0010DCD5 File Offset: 0x0010C0D5
		public void OnClickCancelButtonCallBack()
		{
			this.m_IsDecided = false;
			this.PlayOut();
		}

		// Token: 0x06003529 RID: 13609 RVA: 0x0010DCE4 File Offset: 0x0010C0E4
		public void OnClickInputPasswordDecideButton()
		{
			this.PlayOut();
			this.m_IsDecided = true;
		}

		// Token: 0x0600352A RID: 13610 RVA: 0x0010DCF3 File Offset: 0x0010C0F3
		public void OnClickPlayerMoveDataCloseButton(int index)
		{
			this.m_OnClickPlayerMoveDataCloseButton();
		}

		// Token: 0x04003BB7 RID: 15287
		[SerializeField]
		private UIGroup m_InputPasswordGroup;

		// Token: 0x04003BB8 RID: 15288
		[SerializeField]
		private UIGroup m_ResultGroup;

		// Token: 0x04003BB9 RID: 15289
		[SerializeField]
		private InputField m_PassWordInputField;

		// Token: 0x04003BBA RID: 15290
		[SerializeField]
		private CustomButton m_InputPasswordDecideButton;

		// Token: 0x04003BBB RID: 15291
		[SerializeField]
		private Text m_IDText;

		// Token: 0x04003BBC RID: 15292
		[SerializeField]
		private Text m_PassWordText;

		// Token: 0x04003BBD RID: 15293
		[SerializeField]
		private Text m_DeadlineText;

		// Token: 0x04003BBE RID: 15294
		private MenuPlayerMoveConfigurationUI.eWindowType m_ActiveWindowType;

		// Token: 0x04003BBF RID: 15295
		private UIGroup m_ActiveUIGroup;

		// Token: 0x04003BC0 RID: 15296
		private bool m_IsDecided;

		// Token: 0x04003BC1 RID: 15297
		private MenuPlayerMoveConfigurationUI.eStep m_Step;

		// Token: 0x02000A07 RID: 2567
		public enum eWindowType
		{
			// Token: 0x04003BC5 RID: 15301
			None,
			// Token: 0x04003BC6 RID: 15302
			InputPassword,
			// Token: 0x04003BC7 RID: 15303
			PlayerMoveData
		}

		// Token: 0x02000A08 RID: 2568
		private enum eStep
		{
			// Token: 0x04003BC9 RID: 15305
			Hide,
			// Token: 0x04003BCA RID: 15306
			In,
			// Token: 0x04003BCB RID: 15307
			Idle,
			// Token: 0x04003BCC RID: 15308
			Out,
			// Token: 0x04003BCD RID: 15309
			End
		}
	}
}
