﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009F5 RID: 2549
	public class MenuLibrarySummonItem : ScrollItemIcon
	{
		// Token: 0x060034D3 RID: 13523 RVA: 0x0010C534 File Offset: 0x0010A934
		protected override void ApplyData()
		{
			MenuLibrarySummonItemData menuLibrarySummonItemData = (MenuLibrarySummonItemData)this.m_Data;
			this.m_TitleName.text = menuLibrarySummonItemData.m_Title;
		}

		// Token: 0x04003B61 RID: 15201
		[SerializeField]
		private Text m_TitleName;
	}
}
