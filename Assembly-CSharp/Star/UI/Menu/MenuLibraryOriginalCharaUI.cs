﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Menu
{
	// Token: 0x020009F1 RID: 2545
	public class MenuLibraryOriginalCharaUI : MenuUIBase
	{
		// Token: 0x060034C0 RID: 13504 RVA: 0x0010BC08 File Offset: 0x0010A008
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_SpriteHandler = null;
			this.m_SpriteHandlerIllust = null;
			this.m_Scroll.Setup();
			OriginalCharaLibraryListDB originalCharaLibraryListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.OriginalCharaLibraryListDB;
			int num = -1;
			for (int i = 0; i < originalCharaLibraryListDB.m_Params.Length; i++)
			{
				int condQuestID = originalCharaLibraryListDB.m_Params[i].m_CondQuestID;
				if (condQuestID == -1 || SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.IsClearQuest(condQuestID))
				{
					ITitleUIData titleUIData = new ITitleUIData();
					titleUIData.m_WordIdx = i;
					titleUIData.m_TitleName = originalCharaLibraryListDB.m_Params[i].m_Title;
					titleUIData.OnClickWord += this.OnClickWordButtonCallBack;
					this.m_Scroll.AddItem(titleUIData);
					this.m_DataList.Add(titleUIData);
					if (num == -1)
					{
						num = i;
					}
				}
			}
			if (num != -1)
			{
				this.OnClickWordButtonCallBack(num);
			}
		}

		// Token: 0x060034C1 RID: 13505 RVA: 0x0010BD14 File Offset: 0x0010A114
		public override void Destroy()
		{
			this.m_Icon.sprite = null;
			this.m_Illust.sprite = null;
			if (this.m_SpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
				this.m_SpriteHandler = null;
			}
			if (this.m_SpriteHandlerIllust != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandlerIllust);
				this.m_SpriteHandlerIllust = null;
			}
		}

		// Token: 0x060034C2 RID: 13506 RVA: 0x0010BD88 File Offset: 0x0010A188
		private void Update()
		{
			if (this.m_SpriteHandler != null && this.m_Icon.sprite == null && this.m_SpriteHandler.IsAvailable())
			{
				this.m_Icon.sprite = this.m_SpriteHandler.Obj;
			}
			if (this.m_SpriteHandlerIllust != null && this.m_Illust.sprite == null && this.m_SpriteHandlerIllust.IsAvailable())
			{
				this.m_Illust.sprite = this.m_SpriteHandlerIllust.Obj;
			}
			switch (this.m_Step)
			{
			case MenuLibraryOriginalCharaUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(MenuLibraryOriginalCharaUI.eStep.Idle);
				}
				break;
			}
			case MenuLibraryOriginalCharaUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MenuLibraryOriginalCharaUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060034C3 RID: 13507 RVA: 0x0010BEFC File Offset: 0x0010A2FC
		private void ChangeStep(MenuLibraryOriginalCharaUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuLibraryOriginalCharaUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case MenuLibraryOriginalCharaUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryOriginalCharaUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case MenuLibraryOriginalCharaUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case MenuLibraryOriginalCharaUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060034C4 RID: 13508 RVA: 0x0010BFA4 File Offset: 0x0010A3A4
		public override void PlayIn()
		{
			this.ChangeStep(MenuLibraryOriginalCharaUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x060034C5 RID: 13509 RVA: 0x0010BFEC File Offset: 0x0010A3EC
		public override void PlayOut()
		{
			this.ChangeStep(MenuLibraryOriginalCharaUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x060034C6 RID: 13510 RVA: 0x0010C031 File Offset: 0x0010A431
		public override bool IsMenuEnd()
		{
			return this.m_Step == MenuLibraryOriginalCharaUI.eStep.End;
		}

		// Token: 0x060034C7 RID: 13511 RVA: 0x0010C03C File Offset: 0x0010A43C
		public void OnBackButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x060034C8 RID: 13512 RVA: 0x0010C044 File Offset: 0x0010A444
		private void OnClickWordButtonCallBack(int idx)
		{
			if (idx == this.m_CurrentIdx)
			{
				return;
			}
			this.m_CurrentIdx = idx;
			for (int i = 0; i < this.m_DataList.Count; i++)
			{
				this.m_DataList[i].m_IsSelected = (idx == this.m_DataList[i].m_WordIdx);
			}
			this.m_Scroll.ForceApply();
			OriginalCharaLibraryListDB_Param originalCharaLibraryListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.OriginalCharaLibraryListDB.m_Params[idx];
			this.m_Icon.sprite = null;
			this.m_Illust.sprite = null;
			this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncLibraryOriginalCharaIcon(originalCharaLibraryListDB_Param.m_ID);
			this.m_SpriteHandlerIllust = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncLibraryOriginalCharaIllust(originalCharaLibraryListDB_Param.m_ID);
			this.m_DescriptText.text = originalCharaLibraryListDB_Param.m_Descript;
			this.m_NameText.text = originalCharaLibraryListDB_Param.m_Title;
			this.m_IllustratorText.text = originalCharaLibraryListDB_Param.m_Illustrator;
			this.m_CVText.text = originalCharaLibraryListDB_Param.m_CV;
		}

		// Token: 0x060034C9 RID: 13513 RVA: 0x0010C169 File Offset: 0x0010A569
		public void OnClickIllustButtonCallBack()
		{
			this.m_IllustGroup.Open();
		}

		// Token: 0x04003B41 RID: 15169
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003B42 RID: 15170
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003B43 RID: 15171
		[SerializeField]
		private UIGroup m_IllustGroup;

		// Token: 0x04003B44 RID: 15172
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003B45 RID: 15173
		[SerializeField]
		private Image m_Icon;

		// Token: 0x04003B46 RID: 15174
		[SerializeField]
		private Image m_Illust;

		// Token: 0x04003B47 RID: 15175
		[SerializeField]
		private Text m_DescriptText;

		// Token: 0x04003B48 RID: 15176
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003B49 RID: 15177
		[SerializeField]
		private Text m_IllustratorText;

		// Token: 0x04003B4A RID: 15178
		[SerializeField]
		private Text m_CVText;

		// Token: 0x04003B4B RID: 15179
		private List<ITitleUIData> m_DataList = new List<ITitleUIData>();

		// Token: 0x04003B4C RID: 15180
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04003B4D RID: 15181
		private SpriteHandler m_SpriteHandlerIllust;

		// Token: 0x04003B4E RID: 15182
		private int m_CurrentIdx = -1;

		// Token: 0x04003B4F RID: 15183
		private MenuLibraryOriginalCharaUI.eStep m_Step;

		// Token: 0x020009F2 RID: 2546
		private enum eStep
		{
			// Token: 0x04003B51 RID: 15185
			Hide,
			// Token: 0x04003B52 RID: 15186
			In,
			// Token: 0x04003B53 RID: 15187
			Idle,
			// Token: 0x04003B54 RID: 15188
			Out,
			// Token: 0x04003B55 RID: 15189
			End
		}
	}
}
