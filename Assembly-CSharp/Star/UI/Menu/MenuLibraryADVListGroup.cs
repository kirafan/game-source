﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Menu
{
	// Token: 0x020009EA RID: 2538
	public class MenuLibraryADVListGroup : UIGroup
	{
		// Token: 0x1400009E RID: 158
		// (add) Token: 0x06003498 RID: 13464 RVA: 0x0010ACC8 File Offset: 0x001090C8
		// (remove) Token: 0x06003499 RID: 13465 RVA: 0x0010AD00 File Offset: 0x00109100
		public event Action<int> OnClickADV;

		// Token: 0x0600349A RID: 13466 RVA: 0x0010AD36 File Offset: 0x00109136
		public void Setup()
		{
			this.m_Scroll.Setup();
		}

		// Token: 0x0600349B RID: 13467 RVA: 0x0010AD44 File Offset: 0x00109144
		public void Open(List<int> advList, int advid = -1)
		{
			this.m_Scroll.RemoveAllData();
			int scrollValueFromIdx = 0;
			for (int i = 0; i < advList.Count; i++)
			{
				MenuLibraryADVItemData menuLibraryADVItemData = new MenuLibraryADVItemData(advList[i]);
				menuLibraryADVItemData.OnClickCallBack += this.OnClickADVCallBack;
				if (advList[i] == advid)
				{
					scrollValueFromIdx = i;
				}
				this.m_Scroll.AddItem(menuLibraryADVItemData);
			}
			if (advid != -1)
			{
				this.m_Scroll.SetScrollValueFromIdx(scrollValueFromIdx);
			}
			base.Open();
		}

		// Token: 0x0600349C RID: 13468 RVA: 0x0010ADC8 File Offset: 0x001091C8
		private void OnClickADVCallBack(int advID)
		{
			this.OnClickADV.Call(advID);
		}

		// Token: 0x04003B12 RID: 15122
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
