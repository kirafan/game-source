﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using Star.UI.Edit;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000948 RID: 2376
	public class SupportEditPlayer : SingletonMonoBehaviour<SupportEditPlayer>
	{
		// Token: 0x14000081 RID: 129
		// (add) Token: 0x06003163 RID: 12643 RVA: 0x000FF04C File Offset: 0x000FD44C
		// (remove) Token: 0x06003164 RID: 12644 RVA: 0x000FF084 File Offset: 0x000FD484
		public event Action OnClose;

		// Token: 0x06003165 RID: 12645 RVA: 0x000FF0BA File Offset: 0x000FD4BA
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06003166 RID: 12646 RVA: 0x000FF0C2 File Offset: 0x000FD4C2
		public bool IsOpen()
		{
			return this.m_Step != SupportEditPlayer.eStep.Hide;
		}

		// Token: 0x06003167 RID: 12647 RVA: 0x000FF0D0 File Offset: 0x000FD4D0
		public void Open(List<UserSupportData> supportDatas, string partyName, int supportLimit, PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.View)
		{
			if (this.m_Step != SupportEditPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.m_SupportDatas = supportDatas;
			this.m_PartyName = partyName;
			this.m_SupportLimit = supportLimit;
			this.m_Mode = mode;
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.OverlayUI, UIBackGroundManager.eBackGroundID.Common, true);
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_SceneInfoStack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Friend);
			this.OnClose = null;
			this.m_Step = SupportEditPlayer.eStep.UILoad;
		}

		// Token: 0x06003168 RID: 12648 RVA: 0x000FF18C File Offset: 0x000FD58C
		public void Close()
		{
			if (this.m_Step != SupportEditPlayer.eStep.Main)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Close() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.OverlayUI);
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfoStack);
			this.OnClose.Call();
			this.OnClose = null;
		}

		// Token: 0x06003169 RID: 12649 RVA: 0x000FF217 File Offset: 0x000FD617
		public void ForceHide()
		{
			this.m_Step = SupportEditPlayer.eStep.Hide;
		}

		// Token: 0x0600316A RID: 12650 RVA: 0x000FF220 File Offset: 0x000FD620
		private void Update()
		{
			switch (this.m_Step)
			{
			case SupportEditPlayer.eStep.UILoad:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.SupportEditUI, true);
				this.m_Step = SupportEditPlayer.eStep.LoadWait;
				break;
			case SupportEditPlayer.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.SupportEditUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.SupportEditUI);
					this.m_Step = SupportEditPlayer.eStep.SetupAndPlayIn;
				}
				break;
			case SupportEditPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<SupportEditUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.SupportEditUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					this.m_UI.Setup(this.m_SupportDatas, this.m_PartyName, this.m_SupportLimit, this.m_Mode);
					this.m_UI.PlayIn();
					this.m_Step = SupportEditPlayer.eStep.Main;
				}
				break;
			case SupportEditPlayer.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.SupportEditUI);
					this.m_Step = SupportEditPlayer.eStep.UnloadWait;
				}
				break;
			case SupportEditPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.SupportEditUI))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_SavedBackButtonCallBack(true);
					}
					this.m_Step = SupportEditPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x0600316B RID: 12651 RVA: 0x000FF388 File Offset: 0x000FD788
		private void OnClickBackButton(bool isCallFromShortCut)
		{
			this.Close();
		}

		// Token: 0x0600316C RID: 12652 RVA: 0x000FF390 File Offset: 0x000FD790
		private void OnStartIllustView()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
		}

		// Token: 0x0600316D RID: 12653 RVA: 0x000FF3A1 File Offset: 0x000FD7A1
		private void OnEndIllustView()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
		}

		// Token: 0x040037C9 RID: 14281
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.SupportEditUI;

		// Token: 0x040037CA RID: 14282
		private SupportEditPlayer.eStep m_Step;

		// Token: 0x040037CB RID: 14283
		private SupportEditUI m_UI;

		// Token: 0x040037CC RID: 14284
		private List<UserSupportData> m_SupportDatas;

		// Token: 0x040037CD RID: 14285
		private string m_PartyName;

		// Token: 0x040037CE RID: 14286
		private int m_SupportLimit = -1;

		// Token: 0x040037CF RID: 14287
		private PartyEditUIBase.eMode m_Mode = PartyEditUIBase.eMode.View;

		// Token: 0x040037D0 RID: 14288
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x040037D1 RID: 14289
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x02000949 RID: 2377
		private enum eStep
		{
			// Token: 0x040037D4 RID: 14292
			Hide,
			// Token: 0x040037D5 RID: 14293
			UILoad,
			// Token: 0x040037D6 RID: 14294
			LoadWait,
			// Token: 0x040037D7 RID: 14295
			SetupAndPlayIn,
			// Token: 0x040037D8 RID: 14296
			Main,
			// Token: 0x040037D9 RID: 14297
			UnloadWait
		}
	}
}
