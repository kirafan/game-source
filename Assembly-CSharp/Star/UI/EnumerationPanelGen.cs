﻿using System;

namespace Star.UI
{
	// Token: 0x02000976 RID: 2422
	public abstract class EnumerationPanelGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where ThisType : EnumerationPanelGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelAdapterExGen<ThisType> where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
	}
}
