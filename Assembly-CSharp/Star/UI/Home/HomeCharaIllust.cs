﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Home
{
	// Token: 0x020009B7 RID: 2487
	public class HomeCharaIllust : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x060033A3 RID: 13219 RVA: 0x00106212 File Offset: 0x00104612
		public void SetEnablePlayVoice(bool flg)
		{
			this.m_IsEnablePlayVoice = flg;
		}

		// Token: 0x060033A4 RID: 13220 RVA: 0x0010621B File Offset: 0x0010461B
		public void StopVoice()
		{
			if (this.m_VoiceReqID != -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceReqID);
				this.m_VoiceReqID = -1;
			}
		}

		// Token: 0x060033A5 RID: 13221 RVA: 0x00106248 File Offset: 0x00104648
		public void Setup()
		{
			this.m_IllustRectTransforms = new RectTransform[this.m_CharaIllusts.Length];
			for (int i = 0; i < this.m_IllustAnims.Length; i++)
			{
				this.m_IllustRectTransforms[i] = this.m_CharaIllusts[i].GetComponent<RectTransform>();
				this.m_IllustAnims[i].Hide();
			}
			this.NextChara();
		}

		// Token: 0x060033A6 RID: 13222 RVA: 0x001062A9 File Offset: 0x001046A9
		private bool UpdatePrepare()
		{
			return this.m_TweetListDB != null;
		}

		// Token: 0x060033A7 RID: 13223 RVA: 0x001062C0 File Offset: 0x001046C0
		private void Update()
		{
			if (this.UpdatePrepare() && this.m_IsEnablePlayVoice && this.m_TweetListDB != null && this.m_IllustAnims[this.m_CurrentIdx].State == 2 && this.m_TalkWindow.State == 0)
			{
				TweetListDB_Param param = this.m_TweetListDB.GetParam(eTweetCategory.HomeComment);
				this.m_TalkTextObj.text = param.m_Text;
				GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
				inst.VoiceCtrl.StopRequest(this.m_VoiceReqID);
				this.m_VoiceReqID = inst.VoiceCtrl.Request((eCharaNamedType)inst.DbMng.CharaListDB.GetParam(this.m_CharaID).m_NamedType, param.m_VoiceCueName, true);
				this.m_TalkWindow.PlayIn();
			}
			for (int i = 0; i < this.m_CharaIllusts.Length; i++)
			{
				if (i == this.m_CurrentIdx)
				{
					if (this.m_IllustAnims[i].State == 0)
					{
						this.m_IllustAnims[i].PlayIn();
						this.m_TweetListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaID).m_NamedType);
					}
				}
				else if (this.m_IllustAnims[i].State == 0)
				{
					this.m_CharaIllusts[i].Destroy();
				}
			}
		}

		// Token: 0x060033A8 RID: 13224 RVA: 0x00106434 File Offset: 0x00104834
		public void Destroy()
		{
			for (int i = 0; i < this.m_CharaIllusts.Length; i++)
			{
				this.m_CharaIllusts[i].Destroy();
			}
			this.m_TalkTextObj.text = string.Empty;
			this.m_TweetListDB = null;
		}

		// Token: 0x060033A9 RID: 13225 RVA: 0x0010647E File Offset: 0x0010487E
		public bool IsDisplay()
		{
			return this.m_CurrentIdx > -1 && this.m_IllustAnims[this.m_CurrentIdx].State == 2;
		}

		// Token: 0x060033AA RID: 13226 RVA: 0x001064A3 File Offset: 0x001048A3
		public void OnBeginDrag(PointerEventData eventData)
		{
			this.m_IsHold = true;
		}

		// Token: 0x060033AB RID: 13227 RVA: 0x001064AC File Offset: 0x001048AC
		public void OnDrag(PointerEventData eventData)
		{
			if (this.m_IsHold && eventData.position.x - eventData.pressPosition.x > 0f)
			{
				if (eventData.position.x - eventData.pressPosition.x > this.m_Threshold)
				{
					this.NextChara();
				}
				this.m_IsHold = false;
			}
		}

		// Token: 0x060033AC RID: 13228 RVA: 0x00106520 File Offset: 0x00104920
		public void OnEndDrag(PointerEventData eventData)
		{
			if (!this.m_IsHold)
			{
				this.m_IsHold = false;
			}
		}

		// Token: 0x060033AD RID: 13229 RVA: 0x00106534 File Offset: 0x00104934
		public void DisplayFirstChara()
		{
			if (!this.m_TalkWindow.IsEnd)
			{
				this.m_TalkWindow.Hide();
			}
			this.m_CurrentIdx = -1;
			for (int i = 0; i < this.m_IllustAnims.Length; i++)
			{
				if (this.m_IllustAnims[i].isActiveAndEnabled)
				{
					this.m_IllustAnims[i].Hide();
				}
			}
			this.m_RoomInCharaIdx = -1;
			this.NextChara();
		}

		// Token: 0x060033AE RID: 13230 RVA: 0x001065A8 File Offset: 0x001049A8
		public void HideTalkWindow()
		{
			this.m_TalkWindow.Hide();
		}

		// Token: 0x060033AF RID: 13231 RVA: 0x001065B8 File Offset: 0x001049B8
		public void NextChara()
		{
			for (int i = 0; i < this.m_IllustAnims.Length; i++)
			{
				if (!this.m_IllustAnims[i].IsEnd)
				{
					return;
				}
			}
			if (this.m_TalkWindow.State != 0)
			{
				this.m_TalkWindow.PlayOut();
			}
			long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
			for (int j = 0; j < roomInCharaManageID.Length; j++)
			{
				this.m_RoomInCharaIdx++;
				if (roomInCharaManageID.Length <= this.m_RoomInCharaIdx)
				{
					this.m_RoomInCharaIdx = 0;
				}
				if (roomInCharaManageID[this.m_RoomInCharaIdx] != -1L)
				{
					break;
				}
			}
			if (roomInCharaManageID != null && roomInCharaManageID.Length > 0 && roomInCharaManageID[this.m_RoomInCharaIdx] != -1L)
			{
				this.m_CharaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(roomInCharaManageID[this.m_RoomInCharaIdx]).Param.CharaID;
				this.SetChara(this.m_CharaID);
			}
		}

		// Token: 0x060033B0 RID: 13232 RVA: 0x001066BD File Offset: 0x00104ABD
		private void SetChara(int charaID)
		{
			this.m_CharaID = charaID;
			this.DisappearChara();
			this.AppearChara();
		}

		// Token: 0x060033B1 RID: 13233 RVA: 0x001066D2 File Offset: 0x00104AD2
		private void DisappearChara()
		{
			if (this.m_CurrentIdx > -1)
			{
				this.m_IllustAnims[this.m_CurrentIdx].PlayOut();
			}
		}

		// Token: 0x060033B2 RID: 13234 RVA: 0x001066F4 File Offset: 0x00104AF4
		private void AppearChara()
		{
			this.m_CurrentIdx++;
			if (this.m_CharaIllusts.Length <= this.m_CurrentIdx)
			{
				this.m_CurrentIdx = 0;
			}
			this.m_CharaIllusts[this.m_CurrentIdx].Apply(this.m_CharaID, CharaIllust.eCharaIllustType.Bust);
		}

		// Token: 0x040039DB RID: 14811
		[SerializeField]
		private CharaIllust[] m_CharaIllusts;

		// Token: 0x040039DC RID: 14812
		[SerializeField]
		private AnimUIPlayer[] m_IllustAnims;

		// Token: 0x040039DD RID: 14813
		private RectTransform[] m_IllustRectTransforms;

		// Token: 0x040039DE RID: 14814
		[SerializeField]
		private AnimUIPlayer m_TalkWindow;

		// Token: 0x040039DF RID: 14815
		[SerializeField]
		private Text m_TalkTextObj;

		// Token: 0x040039E0 RID: 14816
		private int m_CurrentIdx = -1;

		// Token: 0x040039E1 RID: 14817
		private float m_Threshold = 0.2f;

		// Token: 0x040039E2 RID: 14818
		private bool m_IsHold;

		// Token: 0x040039E3 RID: 14819
		private int m_RoomInCharaIdx = -1;

		// Token: 0x040039E4 RID: 14820
		private int m_CharaID = -1;

		// Token: 0x040039E5 RID: 14821
		private TweetListDB m_TweetListDB;

		// Token: 0x040039E6 RID: 14822
		private int m_VoiceReqID = -1;

		// Token: 0x040039E7 RID: 14823
		private bool m_IsEnablePlayVoice;
	}
}
