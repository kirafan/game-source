﻿using System;
using Star.UI.LoginBonus;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x020009BC RID: 2492
	public class HomeUI : MenuUIBase
	{
		// Token: 0x14000092 RID: 146
		// (add) Token: 0x060033C0 RID: 13248 RVA: 0x001069B0 File Offset: 0x00104DB0
		// (remove) Token: 0x060033C1 RID: 13249 RVA: 0x001069E8 File Offset: 0x00104DE8
		private event Action OnEndTotalLoginBonusData;

		// Token: 0x14000093 RID: 147
		// (add) Token: 0x060033C2 RID: 13250 RVA: 0x00106A20 File Offset: 0x00104E20
		// (remove) Token: 0x060033C3 RID: 13251 RVA: 0x00106A58 File Offset: 0x00104E58
		public event Action OnClickPresentButton;

		// Token: 0x14000094 RID: 148
		// (add) Token: 0x060033C4 RID: 13252 RVA: 0x00106A90 File Offset: 0x00104E90
		// (remove) Token: 0x060033C5 RID: 13253 RVA: 0x00106AC8 File Offset: 0x00104EC8
		public event Action OnClickMissionButton;

		// Token: 0x14000095 RID: 149
		// (add) Token: 0x060033C6 RID: 13254 RVA: 0x00106B00 File Offset: 0x00104F00
		// (remove) Token: 0x060033C7 RID: 13255 RVA: 0x00106B38 File Offset: 0x00104F38
		public event Action OnClickPartyButton;

		// Token: 0x14000096 RID: 150
		// (add) Token: 0x060033C8 RID: 13256 RVA: 0x00106B70 File Offset: 0x00104F70
		// (remove) Token: 0x060033C9 RID: 13257 RVA: 0x00106BA8 File Offset: 0x00104FA8
		public event Action OnEnd;

		// Token: 0x060033CA RID: 13258 RVA: 0x00106BDE File Offset: 0x00104FDE
		public bool CheckRaycast()
		{
			return this.m_RaycastCheck.Check();
		}

		// Token: 0x060033CB RID: 13259 RVA: 0x00106BEC File Offset: 0x00104FEC
		public void Setup()
		{
			this.m_StateController.Setup(6, 0, false);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 4, true);
			this.m_StateController.AddTransit(4, 2, true);
			this.m_StateController.AddTransit(2, 3, true);
			this.m_StateController.AddTransit(3, 1, true);
			this.m_StateController.AddTransitEach(4, 5, true);
			this.m_StateController.AddOnExit(4, new Action(this.CloseMainGroup));
			this.m_HomeCharaIllust.Setup();
			this.m_InfoBanner.Setup();
			this.m_LoginGroup.OnEnd += this.OnEndLoginGroup;
		}

		// Token: 0x060033CC RID: 13260 RVA: 0x00106CA0 File Offset: 0x001050A0
		public InfoBanner GetInfoBanner()
		{
			return this.m_InfoBanner;
		}

		// Token: 0x060033CD RID: 13261 RVA: 0x00106CA8 File Offset: 0x001050A8
		public void OpenTotalLogin(TotalLoginUIData totalLoginBonusData, Action onEndTotalLoginBonusData)
		{
			this.m_LoginGroup.Open(totalLoginBonusData);
			this.OnEndTotalLoginBonusData = onEndTotalLoginBonusData;
		}

		// Token: 0x060033CE RID: 13262 RVA: 0x00106CC0 File Offset: 0x001050C0
		private void Update()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.SetMissionBadgeValue(MissionManager.GetNewListUpNum());
				this.SetPresentBadgeValue(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticePresentCount);
			}
			HomeUI.eUIState nowState = (HomeUI.eUIState)this.m_StateController.NowState;
			if (nowState != HomeUI.eUIState.In)
			{
				if (nowState == HomeUI.eUIState.Out)
				{
					bool flag = true;
					if (this.m_StateController.IsPlayingInOut())
					{
						flag = false;
					}
					else
					{
						for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
						{
							if (this.m_AnimUIPlayerArray[i].State == 3)
							{
								flag = false;
								break;
							}
						}
					}
					if (flag)
					{
						this.m_StateController.ChangeState(3);
						this.OnEnd.Call();
					}
				}
			}
		}

		// Token: 0x060033CF RID: 13263 RVA: 0x00106DA0 File Offset: 0x001051A0
		public override void PlayIn()
		{
			this.m_StateController.ForceChangeState(1);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_HomeCharaIllust.HideTalkWindow();
			this.m_HomeCharaIllust.DisplayFirstChara();
			this.OpenMainGroup();
		}

		// Token: 0x060033D0 RID: 13264 RVA: 0x00106DFB File Offset: 0x001051FB
		private void OnEndLoginGroup()
		{
			this.OnEndTotalLoginBonusData.Call();
		}

		// Token: 0x060033D1 RID: 13265 RVA: 0x00106E08 File Offset: 0x00105208
		public void SetEnablePlayVoice(bool flg)
		{
			this.m_HomeCharaIllust.SetEnablePlayVoice(flg);
		}

		// Token: 0x060033D2 RID: 13266 RVA: 0x00106E18 File Offset: 0x00105218
		public override void PlayOut()
		{
			this.m_StateController.ForceChangeState(2);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_HomeCharaIllust.StopVoice();
		}

		// Token: 0x060033D3 RID: 13267 RVA: 0x00106E62 File Offset: 0x00105262
		public override void Destroy()
		{
			base.Destroy();
			this.m_HomeCharaIllust.Destroy();
			this.m_InfoBanner.Destroy();
		}

		// Token: 0x060033D4 RID: 13268 RVA: 0x00106E80 File Offset: 0x00105280
		public void SetPresentBadgeValue(int value)
		{
			this.m_PresentButton.SetBadgeValue(value);
		}

		// Token: 0x060033D5 RID: 13269 RVA: 0x00106E8E File Offset: 0x0010528E
		public void SetMissionBadgeValue(int value)
		{
			this.m_MissionButton.SetBadgeValue(value);
		}

		// Token: 0x060033D6 RID: 13270 RVA: 0x00106E9C File Offset: 0x0010529C
		public override bool IsMenuEnd()
		{
			return this.m_StateController.NowState == 3;
		}

		// Token: 0x060033D7 RID: 13271 RVA: 0x00106EAC File Offset: 0x001052AC
		public void OnClickBackButtonCallBack()
		{
		}

		// Token: 0x060033D8 RID: 13272 RVA: 0x00106EAE File Offset: 0x001052AE
		private void OpenMainGroup()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainGroup.Open();
			}
		}

		// Token: 0x060033D9 RID: 13273 RVA: 0x00106ECC File Offset: 0x001052CC
		public void CloseMainGroup()
		{
			this.m_MainGroup.Close();
		}

		// Token: 0x060033DA RID: 13274 RVA: 0x00106ED9 File Offset: 0x001052D9
		public void OnClickPresentButtonCallBack()
		{
			this.OnClickPresentButton.Call();
		}

		// Token: 0x060033DB RID: 13275 RVA: 0x00106EE6 File Offset: 0x001052E6
		public void OnClickMissionButtonCallBack()
		{
			this.OnClickMissionButton.Call();
		}

		// Token: 0x060033DC RID: 13276 RVA: 0x00106EF3 File Offset: 0x001052F3
		public void OnClickPartyButtonCallBack()
		{
			this.OnClickPartyButton.Call();
		}

		// Token: 0x040039EF RID: 14831
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x040039F0 RID: 14832
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040039F1 RID: 14833
		[SerializeField]
		private TotalLoginGroup m_LoginGroup;

		// Token: 0x040039F2 RID: 14834
		[SerializeField]
		private HomeCharaIllust m_HomeCharaIllust;

		// Token: 0x040039F3 RID: 14835
		[SerializeField]
		private InfoBanner m_InfoBanner;

		// Token: 0x040039F4 RID: 14836
		[SerializeField]
		private HomeTransitButton m_PresentButton;

		// Token: 0x040039F5 RID: 14837
		[SerializeField]
		private HomeTransitButton m_MissionButton;

		// Token: 0x040039F6 RID: 14838
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040039F7 RID: 14839
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x020009BD RID: 2493
		public enum eUIState
		{
			// Token: 0x040039FE RID: 14846
			Hide,
			// Token: 0x040039FF RID: 14847
			In,
			// Token: 0x04003A00 RID: 14848
			Out,
			// Token: 0x04003A01 RID: 14849
			End,
			// Token: 0x04003A02 RID: 14850
			Main,
			// Token: 0x04003A03 RID: 14851
			Window,
			// Token: 0x04003A04 RID: 14852
			Num
		}
	}
}
