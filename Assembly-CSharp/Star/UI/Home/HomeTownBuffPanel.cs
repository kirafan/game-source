﻿using System;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x020009B9 RID: 2489
	public class HomeTownBuffPanel : MonoBehaviour
	{
		// Token: 0x060033B6 RID: 13238 RVA: 0x001067D2 File Offset: 0x00104BD2
		public void Destroy()
		{
			this.m_CharaIcon.Destroy();
		}

		// Token: 0x060033B7 RID: 13239 RVA: 0x001067DF File Offset: 0x00104BDF
		public void Apply(int charaID, eElementType elementType)
		{
			this.m_CharaIcon.Apply(charaID);
			this.m_ElementIcon.Apply(elementType);
			this.m_ClassIcon.gameObject.SetActive(false);
		}

		// Token: 0x060033B8 RID: 13240 RVA: 0x0010680A File Offset: 0x00104C0A
		public void Apply(int charaID, eClassType classType)
		{
			this.m_CharaIcon.Apply(charaID);
			this.m_ClassIcon.Apply(classType);
			this.m_ElementIcon.gameObject.SetActive(false);
		}

		// Token: 0x040039EA RID: 14826
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x040039EB RID: 14827
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x040039EC RID: 14828
		[SerializeField]
		private ClassIcon m_ClassIcon;
	}
}
