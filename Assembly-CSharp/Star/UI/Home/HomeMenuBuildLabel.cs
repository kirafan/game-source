﻿using System;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x020009B8 RID: 2488
	[ExecuteInEditMode]
	public class HomeMenuBuildLabel : MonoBehaviour
	{
		// Token: 0x060033B4 RID: 13236 RVA: 0x0010674C File Offset: 0x00104B4C
		private void Update()
		{
			if (Camera.main != null)
			{
				Vector3 position = this.m_Position;
				position.x /= Camera.main.orthographicSize;
				position.y /= Camera.main.orthographicSize;
				position.z = 0f;
				this.m_TargetRectTransform.position = position;
				this.m_TargetRectTransform.localPosZ(0f);
			}
		}

		// Token: 0x040039E8 RID: 14824
		[SerializeField]
		private Vector3 m_Position;

		// Token: 0x040039E9 RID: 14825
		[SerializeField]
		private RectTransform m_TargetRectTransform;
	}
}
