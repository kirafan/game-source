﻿using System;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x020009BA RID: 2490
	public class HomeTownBuffWindow : MonoBehaviour
	{
		// Token: 0x060033BA RID: 13242 RVA: 0x00106840 File Offset: 0x00104C40
		public void Setup()
		{
			for (int i = 0; i < this.m_Panels.Length; i++)
			{
				this.m_Panels[i].gameObject.SetActive(false);
			}
		}

		// Token: 0x060033BB RID: 13243 RVA: 0x0010687C File Offset: 0x00104C7C
		public void Destroy()
		{
			long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
			int num = roomInCharaManageID.Length;
			if (num > this.m_Panels.Length)
			{
				num = this.m_Panels.Length;
			}
			for (int i = 0; i < num; i++)
			{
				this.m_Panels[i].Destroy();
			}
		}

		// Token: 0x060033BC RID: 13244 RVA: 0x001068D8 File Offset: 0x00104CD8
		public void Apply()
		{
			long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
			int num = roomInCharaManageID.Length;
			if (num > this.m_Panels.Length)
			{
				num = this.m_Panels.Length;
			}
			for (int i = 0; i < num; i++)
			{
				if (UserTownDataCheck.CharaManageIDToBuildPoint(roomInCharaManageID[i]) != -1)
				{
					this.m_Panels[i].gameObject.SetActive(true);
					this.m_Panels[i].Apply(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(roomInCharaManageID[i]).Param.CharaID, eClassType.Fighter);
				}
				else
				{
					this.m_Panels[i].gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x040039ED RID: 14829
		[SerializeField]
		private HomeTownBuffPanel[] m_Panels;
	}
}
