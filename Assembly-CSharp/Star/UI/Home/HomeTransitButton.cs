﻿using System;
using UnityEngine;

namespace Star.UI.Home
{
	// Token: 0x020009BB RID: 2491
	public class HomeTransitButton : MonoBehaviour
	{
		// Token: 0x060033BE RID: 13246 RVA: 0x00106992 File Offset: 0x00104D92
		public void SetBadgeValue(int value)
		{
			this.m_BadgeCloner.GetInst<Badge>().Apply(value);
		}

		// Token: 0x040039EE RID: 14830
		[SerializeField]
		private PrefabCloner m_BadgeCloner;
	}
}
