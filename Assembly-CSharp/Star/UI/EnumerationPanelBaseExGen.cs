﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000975 RID: 2421
	public abstract class EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelBase where ThisType : EnumerationPanelBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelAdapterBase where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelCoreGenBase<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
		// Token: 0x0600320E RID: 12814
		public abstract CoreType CreateCore();

		// Token: 0x0600320F RID: 12815 RVA: 0x000FDBD4 File Offset: 0x000FBFD4
		public virtual SetupArgumentType CreateArgument()
		{
			if (this.m_CharaPanelPrefab != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelPrefab = this.m_CharaPanelPrefab;
			}
			if (this.m_CharaPanelParent != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelParent = this.m_CharaPanelParent;
			}
			SetupArgumentType setupArgumentType = Activator.CreateInstance<SetupArgumentType>();
			setupArgumentType.parent = (ThisType)((object)this);
			return setupArgumentType.Clone(this.m_SerializedConstructInstances);
		}

		// Token: 0x06003210 RID: 12816 RVA: 0x000FDC5E File Offset: 0x000FC05E
		public override void Setup()
		{
			base.Setup();
			this.m_Core = this.CreateCore();
			this.m_SharedInstance = this.CreateArgument();
			this.m_Core.Setup(this.m_SharedInstance);
		}

		// Token: 0x06003211 RID: 12817 RVA: 0x000FDC95 File Offset: 0x000FC095
		public override void Destroy()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Destroy();
			}
			base.Destroy();
		}

		// Token: 0x06003212 RID: 12818 RVA: 0x000FDCBE File Offset: 0x000FC0BE
		private void Update()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Update();
			}
		}

		// Token: 0x06003213 RID: 12819 RVA: 0x000FDCE1 File Offset: 0x000FC0E1
		public int GetPartyIndex()
		{
			return this.m_DataIdx;
		}

		// Token: 0x06003214 RID: 12820 RVA: 0x000FDCE9 File Offset: 0x000FC0E9
		public int GetNowSelectedCharaSlotIndex()
		{
			if (this.m_Core == null)
			{
				return -1;
			}
			return this.m_Core.GetNowSelectedCharaSlotIndex();
		}

		// Token: 0x06003215 RID: 12821 RVA: 0x000FDD0E File Offset: 0x000FC10E
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
			if (this.m_Core != null)
			{
				this.m_Core.SetEditMode(mode);
			}
		}

		// Token: 0x06003216 RID: 12822 RVA: 0x000FDD32 File Offset: 0x000FC132
		public override void Apply(InfiniteScrollItemData data)
		{
			base.Apply(data);
			if (this.m_Core != null)
			{
				this.m_Core.Apply((EnumerationPanelData)data);
			}
		}

		// Token: 0x04003831 RID: 14385
		private const int INSTANTIATE_PER_FRAME = 6;

		// Token: 0x04003832 RID: 14386
		[SerializeField]
		[Tooltip("パネル内キャラクタ情報部の元Prefabリソース.")]
		protected CharaPanelType m_CharaPanelPrefab;

		// Token: 0x04003833 RID: 14387
		[SerializeField]
		[Tooltip("生成したCloneの親.")]
		protected RectTransform m_CharaPanelParent;

		// Token: 0x04003834 RID: 14388
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x04003835 RID: 14389
		protected BasedDataType m_PartyData;

		// Token: 0x04003836 RID: 14390
		protected int m_CharaPanelNum;

		// Token: 0x04003837 RID: 14391
		[SerializeField]
		protected SetupArgumentType m_SerializedConstructInstances;

		// Token: 0x04003838 RID: 14392
		protected CoreType m_Core;

		// Token: 0x04003839 RID: 14393
		protected SetupArgumentType m_SharedInstance;
	}
}
