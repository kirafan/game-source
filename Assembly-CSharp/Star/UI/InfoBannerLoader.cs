﻿using System;
using System.Collections.Generic;
using InformationRequestTypes;
using InformationResponseTypes;
using Meige;
using UnityEngine;
using WWWTypes;

namespace Star.UI
{
	// Token: 0x020009BF RID: 2495
	public class InfoBannerLoader
	{
		// Token: 0x060033E4 RID: 13284 RVA: 0x001070A4 File Offset: 0x001054A4
		public void Request_InfoBanner(Action onResponse)
		{
			this.m_OnResponse = onResponse;
			MeigewwwParam wwwParam = InformationRequest.Getall(new InformationRequestTypes.Getall
			{
				platform = Platform.Get()
			}, new MeigewwwParam.Callback(this.OnResponse_InfoBanner));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x060033E5 RID: 13285 RVA: 0x001070E4 File Offset: 0x001054E4
		private void OnResponse_InfoBanner(MeigewwwParam wwwParam)
		{
			InformationResponseTypes.Getall getall = InformationResponse.Getall(wwwParam, ResponseCommon.DialogType.None, null);
			if (getall == null)
			{
				return;
			}
			ResultCode result = getall.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				if (getall.informations == null || getall.informations.Length <= 0)
				{
					this.m_InfoList.Clear();
				}
				else
				{
					for (int i = 0; i < getall.informations.Length; i++)
					{
						this.m_InfoList.Add(new InfoBannerLoader.InfoData(getall.informations[i].id, getall.informations[i].imgId.ToString(), getall.informations[i].url, getall.informations[i].startAt, getall.informations[i].endAt));
					}
				}
			}
			if (this.m_OnResponse != null)
			{
				this.m_OnResponse.Call();
			}
		}

		// Token: 0x060033E6 RID: 13286 RVA: 0x001071CB File Offset: 0x001055CB
		public List<InfoBannerLoader.InfoData> GetList()
		{
			return this.m_InfoList;
		}

		// Token: 0x060033E7 RID: 13287 RVA: 0x001071D3 File Offset: 0x001055D3
		public bool IsDonePrepare()
		{
			return this.m_IsDonePrepare;
		}

		// Token: 0x060033E8 RID: 13288 RVA: 0x001071DC File Offset: 0x001055DC
		public void Prepare(InfoBannerLoader.eBannerType bannerType)
		{
			this.m_BannerType = bannerType;
			this.m_Loader = new ABResourceLoader(-1);
			if (this.m_BannerType == InfoBannerLoader.eBannerType.Normal)
			{
				this.m_Hndl = this.m_Loader.Load("texture/infobanner.muast", new MeigeResource.Option[0]);
			}
			else
			{
				this.m_Hndl = this.m_Loader.Load("texture/infobannerlarge.muast", new MeigeResource.Option[0]);
			}
			this.m_IsDonePrepare = false;
		}

		// Token: 0x060033E9 RID: 13289 RVA: 0x0010724C File Offset: 0x0010564C
		public void UpdatePrepare()
		{
			if (!this.m_IsDonePrepare && this.m_Loader != null)
			{
				this.m_Loader.Update();
				if (this.m_Hndl != null)
				{
					if (this.m_Hndl.IsError())
					{
						APIUtility.ShowErrorWindowTitle(null, "通信エラーが発生しました。");
						this.m_IsDonePrepare = true;
					}
					else if (this.m_Hndl.IsDone())
					{
						this.m_IsDonePrepare = true;
					}
				}
			}
		}

		// Token: 0x060033EA RID: 13290 RVA: 0x001072C4 File Offset: 0x001056C4
		public Sprite GetSprite(string imgID)
		{
			string b = null;
			if (this.m_BannerType == InfoBannerLoader.eBannerType.Normal)
			{
				b = "InfoBanner_" + imgID;
			}
			else if (this.m_BannerType == InfoBannerLoader.eBannerType.Large)
			{
				b = "InfoBannerLarge_" + imgID;
			}
			Sprite result = null;
			for (int i = 0; i < this.m_Hndl.Objs.Length; i++)
			{
				if (this.m_Hndl.Objs[i] is Sprite && this.m_Hndl.Objs[i].name == b)
				{
					result = (this.m_Hndl.Objs[i] as Sprite);
					break;
				}
			}
			return result;
		}

		// Token: 0x060033EB RID: 13291 RVA: 0x00107374 File Offset: 0x00105774
		public void Destroy()
		{
			this.m_Loader.UnloadAll(false);
		}

		// Token: 0x04003A0B RID: 14859
		public const string BANNER_PATH = "texture/infobanner.muast";

		// Token: 0x04003A0C RID: 14860
		public const string BANNER_NAME_BASE = "InfoBanner_";

		// Token: 0x04003A0D RID: 14861
		public const string BANNER_PATH_LARGE = "texture/infobannerlarge.muast";

		// Token: 0x04003A0E RID: 14862
		public const string BANNER_NAME_BASE_LARGE = "InfoBannerLarge_";

		// Token: 0x04003A0F RID: 14863
		private InfoBannerLoader.eBannerType m_BannerType;

		// Token: 0x04003A10 RID: 14864
		private List<InfoBannerLoader.InfoData> m_InfoList = new List<InfoBannerLoader.InfoData>();

		// Token: 0x04003A11 RID: 14865
		private ABResourceLoader m_Loader;

		// Token: 0x04003A12 RID: 14866
		private ABResourceObjectHandler m_Hndl;

		// Token: 0x04003A13 RID: 14867
		private bool m_IsDonePrepare;

		// Token: 0x04003A14 RID: 14868
		private Action m_OnResponse;

		// Token: 0x020009C0 RID: 2496
		public enum eBannerType
		{
			// Token: 0x04003A16 RID: 14870
			Normal,
			// Token: 0x04003A17 RID: 14871
			Large
		}

		// Token: 0x020009C1 RID: 2497
		public class InfoData
		{
			// Token: 0x060033EC RID: 13292 RVA: 0x00107382 File Offset: 0x00105782
			public InfoData(int id, string imgID, string url, DateTime startTime, DateTime endTime)
			{
				this.m_ID = id;
				this.m_ImgID = imgID;
				this.m_Url = url;
				this.m_StartTime = startTime;
				this.m_EndTime = endTime;
			}

			// Token: 0x04003A18 RID: 14872
			public int m_ID;

			// Token: 0x04003A19 RID: 14873
			public string m_ImgID;

			// Token: 0x04003A1A RID: 14874
			public string m_Url;

			// Token: 0x04003A1B RID: 14875
			public DateTime m_StartTime;

			// Token: 0x04003A1C RID: 14876
			public DateTime m_EndTime;
		}
	}
}
