﻿using System;

namespace Star.UI
{
	// Token: 0x0200087A RID: 2170
	public class WeaponIcon : ASyncImage
	{
		// Token: 0x06002CF0 RID: 11504 RVA: 0x000ED0B0 File Offset: 0x000EB4B0
		public void Apply(int weaponID)
		{
			if (this.m_WeaponID != weaponID)
			{
				this.Apply();
				if (weaponID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncWeaponIcon(weaponID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_WeaponID = weaponID;
			}
		}

		// Token: 0x06002CF1 RID: 11505 RVA: 0x000ED116 File Offset: 0x000EB516
		public override void Destroy()
		{
			base.Destroy();
			this.m_WeaponID = -1;
		}

		// Token: 0x040033FD RID: 13309
		protected int m_WeaponID = -1;
	}
}
