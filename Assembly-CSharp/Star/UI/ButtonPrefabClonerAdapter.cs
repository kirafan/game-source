﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007F8 RID: 2040
	public class ButtonPrefabClonerAdapter<ArgType> : MonoBehaviour
	{
		// Token: 0x170002AB RID: 683
		// (get) Token: 0x06002A33 RID: 10803 RVA: 0x000DE9AB File Offset: 0x000DCDAB
		public GameObject Inst
		{
			get
			{
				if (this.m_Cloner != null)
				{
					return this.m_Cloner.Inst;
				}
				return null;
			}
		}

		// Token: 0x06002A34 RID: 10804 RVA: 0x000DE9CB File Offset: 0x000DCDCB
		public T GetInst<T>() where T : MonoBehaviour
		{
			if (this.m_Cloner != null)
			{
				return this.m_Cloner.GetInst<T>();
			}
			return (T)((object)null);
		}

		// Token: 0x06002A35 RID: 10805 RVA: 0x000DE9F0 File Offset: 0x000DCDF0
		public bool IsExistPrefab()
		{
			return this.m_Cloner != null && this.m_Cloner.IsExistPrefab();
		}

		// Token: 0x06002A36 RID: 10806 RVA: 0x000DEA10 File Offset: 0x000DCE10
		public void Setup(bool force = true)
		{
			if (this.m_Cloner != null)
			{
				this.m_Cloner.Setup(force);
				this.m_Button = this.GetInst<CustomButton>();
				if (this.m_Button != null)
				{
					this.m_Button.gameObject.GetComponentInChildren<Text>().text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon((eText_CommonDB)this.m_ButtonTextCommonId);
					this.m_Button.gameObject.GetComponentInChildren<Text>().fontSize = this.m_ButtonTextFontSize;
					this.m_Button.m_OnClick.AddListener(new UnityAction(this.OnClickButton));
				}
			}
		}

		// Token: 0x06002A37 RID: 10807 RVA: 0x000DEAB8 File Offset: 0x000DCEB8
		public void SetOnClickButtonCallback(Action<ArgType> callback)
		{
			this.m_OnClickCallback = callback;
		}

		// Token: 0x06002A38 RID: 10808 RVA: 0x000DEAC1 File Offset: 0x000DCEC1
		public void OnClickButton()
		{
			if (this.m_OnClickCallback != null)
			{
				this.m_OnClickCallback(this.m_Arg);
			}
		}

		// Token: 0x04003097 RID: 12439
		[SerializeField]
		private PrefabCloner m_Cloner;

		// Token: 0x04003098 RID: 12440
		private CustomButton m_Button;

		// Token: 0x04003099 RID: 12441
		[SerializeField]
		private int m_ButtonTextCommonId;

		// Token: 0x0400309A RID: 12442
		[SerializeField]
		private int m_ButtonTextFontSize;

		// Token: 0x0400309B RID: 12443
		private Action<ArgType> m_OnClickCallback;

		// Token: 0x0400309C RID: 12444
		[SerializeField]
		private ArgType m_Arg;
	}
}
