﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000952 RID: 2386
	public abstract class EnumerationCharaPanelAdapterBase : MonoBehaviour
	{
		// Token: 0x06003188 RID: 12680
		public abstract void Destroy();

		// Token: 0x06003189 RID: 12681
		protected abstract EnumerationPanelBase GetParent();

		// Token: 0x0600318A RID: 12682
		public abstract void SetMode(EnumerationCharaPanelBase.eMode mode);

		// Token: 0x040037F3 RID: 14323
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		protected GameObject m_DataContent;

		// Token: 0x040037F4 RID: 14324
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.キャラ情報部.")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfo;

		// Token: 0x040037F5 RID: 14325
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		protected GameObject m_Blank;

		// Token: 0x040037F6 RID: 14326
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		protected GameObject m_EmptyObj;

		// Token: 0x040037F7 RID: 14327
		[SerializeField]
		[Tooltip("互換用旧仕様.m_SerializeArgumentを使うこと.")]
		protected GameObject m_FriendObj;

		// Token: 0x040037F8 RID: 14328
		[SerializeField]
		protected EnumerationCharaPanelBase.SharedInstance m_SerializedConstructInstances;

		// Token: 0x040037F9 RID: 14329
		protected EnumerationCharaPanelBase.SharedInstance m_SharedInstance;
	}
}
