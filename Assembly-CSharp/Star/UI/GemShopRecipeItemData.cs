﻿using System;

namespace Star.UI
{
	// Token: 0x020009A8 RID: 2472
	public class GemShopRecipeItemData : ScrollItemData
	{
		// Token: 0x06003328 RID: 13096 RVA: 0x0010443C File Offset: 0x0010283C
		public GemShopRecipeItemData(int id, string title, string detail, long amount, StoreDefine.eUIType uiType, Action<int> onClick)
		{
			this.OnClickCallBack = onClick;
			this.m_Id = id;
			this.m_Title = title;
			this.m_Detail = detail;
			this.m_UIType = uiType;
			this.m_Amount = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.GemShopAmount, new object[]
			{
				UIUtility.MoneyValueToString(amount)
			});
		}

		// Token: 0x06003329 RID: 13097 RVA: 0x0010449E File Offset: 0x0010289E
		public override void OnClickItemCallBack()
		{
			this.OnClickCallBack.Call(this.m_Id);
		}

		// Token: 0x0400395E RID: 14686
		public int m_Id;

		// Token: 0x0400395F RID: 14687
		public string m_Title;

		// Token: 0x04003960 RID: 14688
		public string m_Detail;

		// Token: 0x04003961 RID: 14689
		public string m_Amount;

		// Token: 0x04003962 RID: 14690
		public StoreDefine.eUIType m_UIType;

		// Token: 0x04003963 RID: 14691
		private Action<int> OnClickCallBack;
	}
}
