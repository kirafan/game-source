﻿using System;

namespace Star.UI
{
	// Token: 0x02000868 RID: 2152
	public class NpcIllust : ASyncImage
	{
		// Token: 0x06002CAF RID: 11439 RVA: 0x000EBDE0 File Offset: 0x000EA1E0
		public void Apply(NpcIllust.eNpc npc)
		{
			if (this.m_Current != npc)
			{
				this.Apply();
				if (npc != NpcIllust.eNpc.None)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncNpcIllust(npc);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_Current = npc;
			}
		}

		// Token: 0x06002CB0 RID: 11440 RVA: 0x000EBE45 File Offset: 0x000EA245
		public override void Destroy()
		{
			base.Destroy();
			this.m_Current = NpcIllust.eNpc.None;
		}

		// Token: 0x040033A4 RID: 13220
		private NpcIllust.eNpc m_Current;

		// Token: 0x02000869 RID: 2153
		public enum eNpc
		{
			// Token: 0x040033A6 RID: 13222
			None,
			// Token: 0x040033A7 RID: 13223
			Master,
			// Token: 0x040033A8 RID: 13224
			Summon,
			// Token: 0x040033A9 RID: 13225
			Trade,
			// Token: 0x040033AA RID: 13226
			Weapon,
			// Token: 0x040033AB RID: 13227
			Build,
			// Token: 0x040033AC RID: 13228
			Training,
			// Token: 0x040033AD RID: 13229
			Lamp
		}
	}
}
