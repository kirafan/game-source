﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B39 RID: 2873
	public class WebViewWindowUI : MonoBehaviour
	{
		// Token: 0x06003C6E RID: 15470 RVA: 0x00135251 File Offset: 0x00133651
		protected virtual void Start()
		{
			this.Setup();
		}

		// Token: 0x06003C6F RID: 15471 RVA: 0x00135259 File Offset: 0x00133659
		public void Setup()
		{
			if (this.m_Setuped)
			{
				return;
			}
			this.m_TabInformations = new List<WebViewWindowTabInformation>();
			this.m_Setuped = true;
		}

		// Token: 0x06003C70 RID: 15472 RVA: 0x00135279 File Offset: 0x00133679
		public void Destroy()
		{
			this.DestroyWebView();
			UnityEngine.Object.Destroy(this.m_Controller);
			this.m_Controller = null;
		}

		// Token: 0x06003C71 RID: 15473 RVA: 0x00135294 File Offset: 0x00133694
		public void OpenInfoWindow(string url)
		{
			this.Open();
			this.AddTab(new WebViewWindowTabInformation(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.InfoUpdate), WebDataListUtil.GetIDToAdress(1010)));
			this.AddTab(new WebViewWindowTabInformation(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.InfoEvent), WebDataListUtil.GetIDToAdress(1011)));
			this.AddTab(new WebViewWindowTabInformation(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.InfoBug), WebDataListUtil.GetIDToAdress(1012)));
			this.SetWindowTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Info));
			if (url.Contains("update"))
			{
				this.SetSelectedTab(0);
			}
			else if (url.Contains("event"))
			{
				this.SetSelectedTab(1);
			}
			else if (url.Contains("bug"))
			{
				this.SetSelectedTab(2);
			}
			this.m_OpenURL = url;
		}

		// Token: 0x06003C72 RID: 15474 RVA: 0x00135394 File Offset: 0x00133794
		public virtual void Open()
		{
			if (0 >= this.m_TabGroup.GetButtonNum())
			{
				if (1 < this.m_TabInformations.Count)
				{
					if (this.m_TabGroup != null)
					{
						this.m_TabGroup.gameObject.SetActive(true);
						this.m_TabGroup.Setup(this.GetTabTitleArray(this.m_TabInformations), -1f);
					}
				}
				else
				{
					if (0 >= this.m_TabInformations.Count)
					{
						return;
					}
					if (this.m_TabGroup != null)
					{
						this.m_TabGroup.gameObject.SetActive(false);
					}
				}
			}
			this.m_Group.Open();
			this.m_Step = WebViewWindowUI.eStep.In;
		}

		// Token: 0x06003C73 RID: 15475 RVA: 0x00135456 File Offset: 0x00133856
		public virtual void Close()
		{
			this.m_Group.Close();
			this.m_Controller.DestroyWebViewObject();
			this.m_Step = WebViewWindowUI.eStep.Out;
		}

		// Token: 0x06003C74 RID: 15476 RVA: 0x00135478 File Offset: 0x00133878
		private void Update()
		{
			WebViewWindowUI.eStep step = this.m_Step;
			switch (step + 1)
			{
			case WebViewWindowUI.eStep.Idle:
				this.UpdateIn();
				break;
			case WebViewWindowUI.eStep.Out:
				this.UpdateIdle();
				break;
			case (WebViewWindowUI.eStep)3:
				this.UpdateOut();
				break;
			}
		}

		// Token: 0x06003C75 RID: 15477 RVA: 0x001354D0 File Offset: 0x001338D0
		protected virtual void UpdateIn()
		{
			if (!this.m_Group.IsDonePlayIn)
			{
				return;
			}
			if (this.m_OpenURL != null)
			{
				this.m_Controller.LoadURL(this.m_OpenURL, this.m_RectSamples, null);
			}
			else
			{
				this.m_Controller.LoadURL(this.m_TabInformations[this.m_TabGroup.SelectIdx].m_URL, this.m_RectSamples, null);
			}
			this.m_Step = WebViewWindowUI.eStep.Idle;
		}

		// Token: 0x06003C76 RID: 15478 RVA: 0x0013554A File Offset: 0x0013394A
		protected virtual void UpdateIdle()
		{
		}

		// Token: 0x06003C77 RID: 15479 RVA: 0x0013554C File Offset: 0x0013394C
		protected virtual void UpdateOut()
		{
			if (!this.m_Group.IsDonePlayOut)
			{
				return;
			}
			this.m_Step = WebViewWindowUI.eStep.None;
		}

		// Token: 0x06003C78 RID: 15480 RVA: 0x00135566 File Offset: 0x00133966
		private void OnGUI()
		{
		}

		// Token: 0x06003C79 RID: 15481 RVA: 0x00135568 File Offset: 0x00133968
		private void DestroyWebView()
		{
			if (this.m_Controller == null)
			{
				return;
			}
			this.m_Controller.DestroyWebViewObject();
		}

		// Token: 0x06003C7A RID: 15482 RVA: 0x00135588 File Offset: 0x00133988
		public void AddTab(WebViewWindowTabInformation tabInformation)
		{
			this.m_TabInformations.Add(tabInformation);
		}

		// Token: 0x06003C7B RID: 15483 RVA: 0x00135598 File Offset: 0x00133998
		public string[] GetTabTitleArray(List<WebViewWindowTabInformation> tabInformations)
		{
			if (tabInformations == null)
			{
				return null;
			}
			string[] array = new string[tabInformations.Count];
			for (int i = 0; i < tabInformations.Count; i++)
			{
				array[i] = tabInformations[i].m_Title;
			}
			return array;
		}

		// Token: 0x06003C7C RID: 15484 RVA: 0x001355E0 File Offset: 0x001339E0
		public void SetSelectedTab(int index)
		{
			if (this.IsOpenWindow())
			{
				this.m_TabGroup.SelectIdx = index;
				this.m_Controller.LoadURL(this.m_TabInformations[this.m_TabGroup.SelectIdx].m_URL, this.m_RectSamples, null);
			}
		}

		// Token: 0x06003C7D RID: 15485 RVA: 0x00135634 File Offset: 0x00133A34
		public void SetViewMargin(int marginLeft, int marginTop, int marginRight, int marginBottom)
		{
			if (this.m_RectSamples != null && this.m_Controller != null)
			{
				Rect rect = this.m_Controller.MarginToRect(new WebViewMargins(marginLeft, marginTop, marginRight, marginBottom));
				this.m_RectSamples.anchoredPosition = new Vector2(rect.x, rect.yMax);
				this.m_RectSamples.sizeDelta = new Vector2(rect.width, rect.height);
			}
		}

		// Token: 0x06003C7E RID: 15486 RVA: 0x001356B5 File Offset: 0x00133AB5
		public void SetWindowTitle(string title)
		{
			if (this.m_WindowTitle != null)
			{
				this.m_WindowTitle.text = title;
			}
		}

		// Token: 0x06003C7F RID: 15487 RVA: 0x001356D4 File Offset: 0x00133AD4
		public bool IsOpenWindow()
		{
			return this.m_Group.State != UIGroup.eState.Hide && this.m_Group.State != UIGroup.eState.Prepare && this.m_Group.State != UIGroup.eState.WaitFinalize;
		}

		// Token: 0x06003C80 RID: 15488 RVA: 0x0013570B File Offset: 0x00133B0B
		public void OnClickTabButton(int index)
		{
			this.SetSelectedTab(this.m_TabGroup.SelectIdx);
		}

		// Token: 0x0400443F RID: 17471
		private const string URL_UPDATE = "update";

		// Token: 0x04004440 RID: 17472
		private const string URL_EVENT = "event";

		// Token: 0x04004441 RID: 17473
		private const string URL_BUG = "bug";

		// Token: 0x04004442 RID: 17474
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x04004443 RID: 17475
		[SerializeField]
		private Text m_WindowTitle;

		// Token: 0x04004444 RID: 17476
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04004445 RID: 17477
		[SerializeField]
		private WebViewController m_Controller;

		// Token: 0x04004446 RID: 17478
		[SerializeField]
		private RectTransform m_RectSamples;

		// Token: 0x04004447 RID: 17479
		private List<WebViewWindowTabInformation> m_TabInformations;

		// Token: 0x04004448 RID: 17480
		private bool m_Setuped;

		// Token: 0x04004449 RID: 17481
		private string m_OpenURL;

		// Token: 0x0400444A RID: 17482
		private WebViewWindowUI.eStep m_Step = WebViewWindowUI.eStep.None;

		// Token: 0x02000B3A RID: 2874
		public enum eStep
		{
			// Token: 0x0400444C RID: 17484
			None = -1,
			// Token: 0x0400444D RID: 17485
			In,
			// Token: 0x0400444E RID: 17486
			Idle,
			// Token: 0x0400444F RID: 17487
			Out
		}
	}
}
