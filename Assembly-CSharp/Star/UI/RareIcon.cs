﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200089A RID: 2202
	public class RareIcon : UIBehaviour
	{
		// Token: 0x06002DB3 RID: 11699 RVA: 0x000F0D1C File Offset: 0x000EF11C
		public void Apply(eRare rare)
		{
			int num = (int)rare;
			if (this.m_ImgIcon == null)
			{
				return;
			}
			if (this.m_SprIcon == null)
			{
				num = -1;
			}
			if (num < 0 || this.m_SprIcon.Length <= num)
			{
				num = -1;
			}
			if (0 <= num)
			{
				this.SetIconActive(true);
				this.m_ImgIcon.sprite = this.m_SprIcon[num];
			}
			else
			{
				this.SetIconActive(false);
			}
		}

		// Token: 0x06002DB4 RID: 11700 RVA: 0x000F0D90 File Offset: 0x000EF190
		private void SetIconActive(bool active)
		{
			if (this.m_ImgIcon == null)
			{
				return;
			}
			GameObject gameObject = this.m_ImgIcon.gameObject;
			if (gameObject == null)
			{
				return;
			}
			if (gameObject.activeSelf != active)
			{
				gameObject.SetActive(active);
			}
		}

		// Token: 0x040034BB RID: 13499
		[SerializeField]
		[Tooltip("シーン内の切り替え対象表示物.")]
		private Image m_ImgIcon;

		// Token: 0x040034BC RID: 13500
		[SerializeField]
		[Tooltip("レアリティによって表示される元リソース.")]
		private Sprite[] m_SprIcon;
	}
}
