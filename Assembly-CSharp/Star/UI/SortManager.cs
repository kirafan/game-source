﻿using System;

namespace Star.UI
{
	// Token: 0x0200083F RID: 2111
	public class SortManager
	{
		// Token: 0x06002C15 RID: 11285 RVA: 0x000E90F0 File Offset: 0x000E74F0
		public void SetupDefault()
		{
			for (int i = 0; i < 3; i++)
			{
				this.m_SortParams[i] = new SortManager.SortParam();
				this.SetDefault(this.m_SortParams[i], (SortManager.eListType)i);
			}
		}

		// Token: 0x06002C16 RID: 11286 RVA: 0x000E912B File Offset: 0x000E752B
		public void SetDefault(SortManager.eListType type)
		{
			this.SetDefault(this.GetParam(type), type);
		}

		// Token: 0x06002C17 RID: 11287 RVA: 0x000E913B File Offset: 0x000E753B
		public SortManager.SortParam GetParam(SortManager.eListType type)
		{
			return this.m_SortParams[(int)type];
		}

		// Token: 0x06002C18 RID: 11288 RVA: 0x000E9145 File Offset: 0x000E7545
		private void SetDefault(SortManager.SortParam sortParam, SortManager.eListType type)
		{
			sortParam.m_Type = type;
			sortParam.m_Idx = 0;
			sortParam.m_OrderType = SortManager.eSortOrderType.Ascending;
		}

		// Token: 0x040032C0 RID: 12992
		public SortManager.SortParam[] m_SortParams = new SortManager.SortParam[3];

		// Token: 0x02000840 RID: 2112
		public enum eListType
		{
			// Token: 0x040032C2 RID: 12994
			None = -1,
			// Token: 0x040032C3 RID: 12995
			Chara,
			// Token: 0x040032C4 RID: 12996
			Weapon,
			// Token: 0x040032C5 RID: 12997
			Support,
			// Token: 0x040032C6 RID: 12998
			Num
		}

		// Token: 0x02000841 RID: 2113
		public enum eSortOrderType
		{
			// Token: 0x040032C8 RID: 13000
			Ascending,
			// Token: 0x040032C9 RID: 13001
			Descending,
			// Token: 0x040032CA RID: 13002
			Num
		}

		// Token: 0x02000842 RID: 2114
		public enum eSortCategoryChara
		{
			// Token: 0x040032CC RID: 13004
			Level,
			// Token: 0x040032CD RID: 13005
			Rare,
			// Token: 0x040032CE RID: 13006
			Cost,
			// Token: 0x040032CF RID: 13007
			Hp,
			// Token: 0x040032D0 RID: 13008
			Atk,
			// Token: 0x040032D1 RID: 13009
			Mgc,
			// Token: 0x040032D2 RID: 13010
			Def,
			// Token: 0x040032D3 RID: 13011
			MDef,
			// Token: 0x040032D4 RID: 13012
			Spd,
			// Token: 0x040032D5 RID: 13013
			Title,
			// Token: 0x040032D6 RID: 13014
			GetTime
		}

		// Token: 0x02000843 RID: 2115
		public enum eSortCategoryWeapon
		{
			// Token: 0x040032D8 RID: 13016
			Level,
			// Token: 0x040032D9 RID: 13017
			Rare,
			// Token: 0x040032DA RID: 13018
			Cost,
			// Token: 0x040032DB RID: 13019
			Atk,
			// Token: 0x040032DC RID: 13020
			Mgc,
			// Token: 0x040032DD RID: 13021
			Def,
			// Token: 0x040032DE RID: 13022
			MDef,
			// Token: 0x040032DF RID: 13023
			Get
		}

		// Token: 0x02000844 RID: 2116
		public enum eSortCategorySupport
		{
			// Token: 0x040032E1 RID: 13025
			Level,
			// Token: 0x040032E2 RID: 13026
			Rare,
			// Token: 0x040032E3 RID: 13027
			Cost,
			// Token: 0x040032E4 RID: 13028
			HP,
			// Token: 0x040032E5 RID: 13029
			Atk,
			// Token: 0x040032E6 RID: 13030
			Mgc,
			// Token: 0x040032E7 RID: 13031
			Def,
			// Token: 0x040032E8 RID: 13032
			MDef,
			// Token: 0x040032E9 RID: 13033
			Spd
		}

		// Token: 0x02000845 RID: 2117
		public class SortParam
		{
			// Token: 0x040032EA RID: 13034
			public SortManager.eListType m_Type;

			// Token: 0x040032EB RID: 13035
			public int m_Idx;

			// Token: 0x040032EC RID: 13036
			public SortManager.eSortOrderType m_OrderType;
		}
	}
}
