﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008C4 RID: 2244
	public class UIScrollAnim : MonoBehaviour
	{
		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06002E93 RID: 11923 RVA: 0x000F431D File Offset: 0x000F271D
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002E94 RID: 11924 RVA: 0x000F4342 File Offset: 0x000F2742
		private void Start()
		{
			this.m_StartPos = this.RectTransform.anchoredPosition;
		}

		// Token: 0x06002E95 RID: 11925 RVA: 0x000F4358 File Offset: 0x000F2758
		private void Update()
		{
			this.m_TimeCount += Time.deltaTime;
			if (this.m_TimeCount > this.m_Duration)
			{
				this.m_TimeCount -= (float)((int)this.m_TimeCount) / this.m_Duration * this.m_Duration;
			}
			int num = 1;
			if (this.m_Reverse)
			{
				num = -1;
			}
			Vector2 a = this.m_StartPos + new Vector2(this.RectTransform.rect.width * (float)num, 0f);
			this.RectTransform.anchoredPosition = this.m_StartPos + (a - this.m_StartPos) * (this.m_TimeCount / this.m_Duration);
		}

		// Token: 0x040035B8 RID: 13752
		[SerializeField]
		private float m_Duration = 1f;

		// Token: 0x040035B9 RID: 13753
		[SerializeField]
		private bool m_Reverse;

		// Token: 0x040035BA RID: 13754
		private float m_TimeCount;

		// Token: 0x040035BB RID: 13755
		private Vector2 m_StartPos;

		// Token: 0x040035BC RID: 13756
		private RectTransform m_RectTransform;
	}
}
