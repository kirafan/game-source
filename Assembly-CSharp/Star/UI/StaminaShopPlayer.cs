﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using WWWTypes;

namespace Star.UI
{
	// Token: 0x02000ACA RID: 2762
	public class StaminaShopPlayer : SingletonMonoBehaviour<StaminaShopPlayer>
	{
		// Token: 0x060039B2 RID: 14770 RVA: 0x00126377 File Offset: 0x00124777
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x060039B3 RID: 14771 RVA: 0x0012637F File Offset: 0x0012477F
		public bool IsOpen()
		{
			return this.m_Step != StaminaShopPlayer.eStep.Hide;
		}

		// Token: 0x060039B4 RID: 14772 RVA: 0x00126390 File Offset: 0x00124790
		public void Open(int type, Action OnEnd)
		{
			if (this.m_UI != null && this.m_Step != StaminaShopPlayer.eStep.Hide)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			this.m_OnEndCallBack = OnEnd;
			this.m_Step = StaminaShopPlayer.eStep.LoadUI;
		}

		// Token: 0x060039B5 RID: 14773 RVA: 0x001263DE File Offset: 0x001247DE
		public void Close()
		{
			if (this.m_Step != StaminaShopPlayer.eStep.Main)
			{
				return;
			}
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
			}
			this.m_OnEndCallBack.Call();
		}

		// Token: 0x060039B6 RID: 14774 RVA: 0x00126414 File Offset: 0x00124814
		public void ForceHide()
		{
			this.m_Step = StaminaShopPlayer.eStep.Hide;
		}

		// Token: 0x060039B7 RID: 14775 RVA: 0x00126420 File Offset: 0x00124820
		private void Update()
		{
			switch (this.m_Step)
			{
			case StaminaShopPlayer.eStep.LoadUI:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.StaminaShopUI, true);
				this.m_Step = StaminaShopPlayer.eStep.LoadWait;
				break;
			case StaminaShopPlayer.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.StaminaShopUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.StaminaShopUI);
					this.m_Step = StaminaShopPlayer.eStep.SetupAndPlayIn;
				}
				break;
			case StaminaShopPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<StaminaShopUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.StaminaShopUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					this.m_UI.Setup();
					this.m_UI.OnRequest += this.OnRequestCallBack;
					this.m_UI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_Step = StaminaShopPlayer.eStep.Main;
				}
				break;
			case StaminaShopPlayer.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.StaminaShopUI);
					this.m_Step = StaminaShopPlayer.eStep.UnloadWait;
				}
				break;
			case StaminaShopPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.StaminaShopUI))
				{
					this.m_OnEndCallBack.Call();
					this.m_Step = StaminaShopPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x060039B8 RID: 14776 RVA: 0x00126579 File Offset: 0x00124979
		private void OnRequestCallBack(int itemID, int useNum)
		{
			this.Request_StaminaAdd(itemID, useNum);
		}

		// Token: 0x060039B9 RID: 14777 RVA: 0x00126584 File Offset: 0x00124984
		private bool Request_StaminaAdd(int itemID, int useNum)
		{
			PlayerRequestTypes.Staminaadd staminaadd = new PlayerRequestTypes.Staminaadd();
			if (itemID == -1)
			{
				staminaadd.type = 1;
				staminaadd.num = 1;
				staminaadd.itemId = -1;
			}
			else
			{
				staminaadd.type = 2;
				staminaadd.num = 1;
				staminaadd.itemId = itemID;
			}
			MeigewwwParam wwwParam = PlayerRequest.Staminaadd(staminaadd, new MeigewwwParam.Callback(this.OnResponse_StaminaAdd));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x060039BA RID: 14778 RVA: 0x00126608 File Offset: 0x00124A08
		private void OnResponse_StaminaAdd(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Staminaadd staminaadd = PlayerResponse.Staminaadd(wwwParam, ResponseCommon.DialogType.None, null);
			if (staminaadd == null)
			{
				return;
			}
			ResultCode result = staminaadd.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(staminaadd.player, userDataMng.UserData);
				APIUtility.wwwToUserData(staminaadd.itemSummary, userDataMng.UserItemDatas);
				this.m_UI.Refresh();
			}
		}

		// Token: 0x040040F3 RID: 16627
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.StaminaShopUI;

		// Token: 0x040040F4 RID: 16628
		private StaminaShopPlayer.eStep m_Step;

		// Token: 0x040040F5 RID: 16629
		private StaminaShopUI m_UI;

		// Token: 0x040040F6 RID: 16630
		private Action m_OnEndCallBack;

		// Token: 0x02000ACB RID: 2763
		private enum eStep
		{
			// Token: 0x040040F8 RID: 16632
			Hide,
			// Token: 0x040040F9 RID: 16633
			LoadUI,
			// Token: 0x040040FA RID: 16634
			LoadWait,
			// Token: 0x040040FB RID: 16635
			SetupAndPlayIn,
			// Token: 0x040040FC RID: 16636
			Main,
			// Token: 0x040040FD RID: 16637
			UnloadWait
		}
	}
}
