﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.BackGround
{
	// Token: 0x020008AE RID: 2222
	public class UIBackGroundHandler : MonoBehaviour
	{
		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06002E1D RID: 11805 RVA: 0x000F2ED7 File Offset: 0x000F12D7
		// (set) Token: 0x06002E1E RID: 11806 RVA: 0x000F2EDF File Offset: 0x000F12DF
		public bool Fade
		{
			get
			{
				return this.m_Fade;
			}
			set
			{
				this.m_Fade = true;
			}
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06002E1F RID: 11807 RVA: 0x000F2EE8 File Offset: 0x000F12E8
		public string FilePath
		{
			get
			{
				return this.m_FileName;
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06002E20 RID: 11808 RVA: 0x000F2EF0 File Offset: 0x000F12F0
		// (set) Token: 0x06002E21 RID: 11809 RVA: 0x000F2EF8 File Offset: 0x000F12F8
		public UIBackGroundManager.eBackGroundOrderID BGOrderID
		{
			get
			{
				return this.m_BGOrderID;
			}
			set
			{
				this.m_BGOrderID = value;
			}
		}

		// Token: 0x06002E22 RID: 11810 RVA: 0x000F2F01 File Offset: 0x000F1301
		public bool IsActive()
		{
			return this.m_IsActive;
		}

		// Token: 0x06002E23 RID: 11811 RVA: 0x000F2F09 File Offset: 0x000F1309
		public bool IsAvailable()
		{
			return this.m_SpriteHandler == null || this.m_SpriteHandler.IsAvailable();
		}

		// Token: 0x06002E24 RID: 11812 RVA: 0x000F2F2C File Offset: 0x000F132C
		public void Setup(UIBackGroundManager owner, Camera camera)
		{
			this.m_GameObject = base.gameObject;
			this.m_Image.enabled = false;
			this.m_GameObject.SetActive(false);
			this.m_Step = UIBackGroundHandler.eStep.Hide;
			this.m_IsDonePrepare = false;
			this.m_Owner = owner;
			this.m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
			this.m_Canvas.worldCamera = camera;
			this.m_DefaultCamera = camera;
		}

		// Token: 0x06002E25 RID: 11813 RVA: 0x000F2F91 File Offset: 0x000F1391
		public void SetCamera(Camera camera)
		{
			this.m_Canvas.worldCamera = camera;
		}

		// Token: 0x06002E26 RID: 11814 RVA: 0x000F2F9F File Offset: 0x000F139F
		public void SetRenderMode(RenderMode mode)
		{
			this.m_Canvas.renderMode = mode;
		}

		// Token: 0x06002E27 RID: 11815 RVA: 0x000F2FB0 File Offset: 0x000F13B0
		public void Open(string filename, bool fade, UIDefine.eSortOrderTypeID sortorderID)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.Log("GameSystem is failed");
				return;
			}
			this.Destroy();
			this.m_Canvas.worldCamera = this.m_DefaultCamera;
			this.m_FileName = filename;
			this.m_Fade = fade;
			this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncBackGround(filename);
			this.m_Image.enabled = false;
			this.m_GameObject.SetActive(true);
			this.m_Step = UIBackGroundHandler.eStep.Load;
			this.m_IsDonePrepare = false;
			this.m_Canvas.renderMode = RenderMode.ScreenSpaceCamera;
			this.m_Canvas.sortingOrder = UIDefine.SORT_ORDER[(int)sortorderID];
			this.m_IsActive = true;
		}

		// Token: 0x06002E28 RID: 11816 RVA: 0x000F306D File Offset: 0x000F146D
		public void Close()
		{
			if (this.m_IsDonePrepare && this.m_Fade)
			{
				this.m_Step = UIBackGroundHandler.eStep.FadeOut;
			}
			else
			{
				this.Destroy();
			}
		}

		// Token: 0x06002E29 RID: 11817 RVA: 0x000F3098 File Offset: 0x000F1498
		public void SetAnchor(UIBackGroundHandler.eAnchor anchor)
		{
			RectTransform component = this.m_Image.GetComponent<RectTransform>();
			if (anchor != UIBackGroundHandler.eAnchor.Bottom)
			{
				if (anchor != UIBackGroundHandler.eAnchor.Middle)
				{
					if (anchor == UIBackGroundHandler.eAnchor.Top)
					{
						component.pivot = new Vector2(0.5f, 0.87f);
						component.anchorMin = new Vector2(0f, 1f);
						component.anchorMax = new Vector2(1f, 1f);
						component.anchoredPosition = Vector2.zero;
					}
				}
				else
				{
					component.pivot = new Vector2(0.5f, 0.5f);
					component.anchorMin = new Vector2(0f, 0.5f);
					component.anchorMax = new Vector2(1f, 0.5f);
					component.anchoredPosition = Vector2.zero;
				}
			}
			else
			{
				component.pivot = new Vector2(0.5f, 0.13f);
				component.anchorMin = new Vector2(0f, 0f);
				component.anchorMax = new Vector2(1f, 0f);
				component.anchoredPosition = Vector2.zero;
			}
		}

		// Token: 0x06002E2A RID: 11818 RVA: 0x000F31B8 File Offset: 0x000F15B8
		private void Update()
		{
			UIBackGroundHandler.eStep step = this.m_Step;
			if (step != UIBackGroundHandler.eStep.Load)
			{
				if (step != UIBackGroundHandler.eStep.FadeIn)
				{
					if (step == UIBackGroundHandler.eStep.FadeOut)
					{
						float num = this.m_Image.color.a - Time.deltaTime / 0.2f;
						if (num < 0f)
						{
							num = 0f;
						}
						this.m_Image.color = new Color(this.m_Image.color.r, this.m_Image.color.g, this.m_Image.color.b, num);
						if (num <= 0f)
						{
							this.Destroy();
						}
					}
				}
				else
				{
					float num2 = this.m_Image.color.a + Time.deltaTime / 0.2f;
					if (num2 > 1f)
					{
						num2 = 1f;
					}
					this.m_Image.color = new Color(this.m_Image.color.r, this.m_Image.color.g, this.m_Image.color.b, num2);
					if (num2 >= 1f)
					{
						this.m_Owner.Flush();
						this.m_Step = UIBackGroundHandler.eStep.Idle;
					}
				}
			}
			else if (this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable())
			{
				this.m_Image.sprite = this.m_SpriteHandler.Obj;
				this.m_Image.enabled = true;
				this.m_IsDonePrepare = true;
				if (this.m_Fade)
				{
					this.m_Image.color = new Color(this.m_Image.color.r, this.m_Image.color.g, this.m_Image.color.b, 0f);
					this.m_Step = UIBackGroundHandler.eStep.FadeIn;
				}
				else
				{
					this.m_Owner.Flush();
					this.m_Image.color = new Color(this.m_Image.color.r, this.m_Image.color.g, this.m_Image.color.b, 1f);
					this.m_Step = UIBackGroundHandler.eStep.Idle;
				}
			}
		}

		// Token: 0x06002E2B RID: 11819 RVA: 0x000F3438 File Offset: 0x000F1838
		public void Destroy()
		{
			this.m_Step = UIBackGroundHandler.eStep.Hide;
			if (this.m_SpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
				this.m_SpriteHandler = null;
			}
			this.m_FileName = string.Empty;
			this.m_Image.sprite = null;
			this.m_Image.enabled = false;
			this.m_GameObject.SetActive(false);
			if (this.m_IsActive)
			{
				this.m_Owner.Sink(this);
			}
			this.m_IsActive = false;
		}

		// Token: 0x04003525 RID: 13605
		private const float FADEIN_TIME = 0.2f;

		// Token: 0x04003526 RID: 13606
		[SerializeField]
		private Canvas m_Canvas;

		// Token: 0x04003527 RID: 13607
		[SerializeField]
		private Image m_Image;

		// Token: 0x04003528 RID: 13608
		private SpriteHandler m_SpriteHandler;

		// Token: 0x04003529 RID: 13609
		private string m_FileName;

		// Token: 0x0400352A RID: 13610
		private UIBackGroundManager.eBackGroundOrderID m_BGOrderID;

		// Token: 0x0400352B RID: 13611
		private bool m_Fade;

		// Token: 0x0400352C RID: 13612
		private GameObject m_GameObject;

		// Token: 0x0400352D RID: 13613
		private bool m_IsActive;

		// Token: 0x0400352E RID: 13614
		private UIBackGroundManager m_Owner;

		// Token: 0x0400352F RID: 13615
		private UIBackGroundHandler.eStep m_Step;

		// Token: 0x04003530 RID: 13616
		private Camera m_DefaultCamera;

		// Token: 0x04003531 RID: 13617
		private bool m_IsDonePrepare;

		// Token: 0x020008AF RID: 2223
		public enum eAnchor
		{
			// Token: 0x04003533 RID: 13619
			Bottom,
			// Token: 0x04003534 RID: 13620
			Middle,
			// Token: 0x04003535 RID: 13621
			Top
		}

		// Token: 0x020008B0 RID: 2224
		private enum eStep
		{
			// Token: 0x04003537 RID: 13623
			Hide,
			// Token: 0x04003538 RID: 13624
			Load,
			// Token: 0x04003539 RID: 13625
			FadeIn,
			// Token: 0x0400353A RID: 13626
			Idle,
			// Token: 0x0400353B RID: 13627
			FadeOut
		}
	}
}
