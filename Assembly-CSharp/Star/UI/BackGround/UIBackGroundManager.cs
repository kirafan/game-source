﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.BackGround
{
	// Token: 0x020008B1 RID: 2225
	public class UIBackGroundManager : MonoBehaviour
	{
		// Token: 0x06002E2D RID: 11821 RVA: 0x000F34EA File Offset: 0x000F18EA
		public string GetStoryFileName(int chapterID)
		{
			return UIBackGroundManager.FileNames[9 + chapterID];
		}

		// Token: 0x06002E2E RID: 11822 RVA: 0x000F34F8 File Offset: 0x000F18F8
		public bool IsLoadingAny()
		{
			for (int i = 0; i < this.m_ActiveHandlerList.Count; i++)
			{
				if (!this.m_ActiveHandlerList[i].IsAvailable())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002E2F RID: 11823 RVA: 0x000F353C File Offset: 0x000F193C
		public void Setup()
		{
			Transform component = base.GetComponent<Transform>();
			for (int i = 0; i < 4; i++)
			{
				UIBackGroundHandler uibackGroundHandler = UnityEngine.Object.Instantiate<UIBackGroundHandler>(this.m_HandlerPrefab, component, false);
				uibackGroundHandler.Setup(this, this.m_UICamera);
				this.m_EmptyHandlerList.Add(uibackGroundHandler);
			}
		}

		// Token: 0x06002E30 RID: 11824 RVA: 0x000F3589 File Offset: 0x000F1989
		public UIBackGroundHandler GetHandler(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
			return this.m_ActiveHandlers[(int)bgorderID];
		}

		// Token: 0x06002E31 RID: 11825 RVA: 0x000F3593 File Offset: 0x000F1993
		public void Open(UIBackGroundManager.eBackGroundOrderID bgorderID, string filePath, bool fade)
		{
			this.Change(bgorderID, filePath, fade);
		}

		// Token: 0x06002E32 RID: 11826 RVA: 0x000F359E File Offset: 0x000F199E
		public void Open(UIBackGroundManager.eBackGroundOrderID bgOrderID, UIBackGroundManager.eBackGroundID bgID, bool fade)
		{
			this.Open(bgOrderID, UIBackGroundManager.FileNames[(int)bgID], fade);
		}

		// Token: 0x06002E33 RID: 11827 RVA: 0x000F35B0 File Offset: 0x000F19B0
		public void Change(UIBackGroundManager.eBackGroundOrderID bgorderID, string filePath, bool fade)
		{
			if (this.m_ActiveHandlers[(int)bgorderID] != null && this.m_ActiveHandlers[(int)bgorderID].FilePath == filePath)
			{
				return;
			}
			UIBackGroundHandler uibackGroundHandler;
			if (this.m_EmptyHandlerList.Count != 0)
			{
				uibackGroundHandler = this.m_EmptyHandlerList[0];
			}
			else
			{
				uibackGroundHandler = this.m_ActiveHandlerList[0];
			}
			this.m_EmptyHandlerList.RemoveAt(0);
			this.m_ActiveHandlerList.Add(uibackGroundHandler);
			UIDefine.eSortOrderTypeID sortorderID = UIDefine.eSortOrderTypeID.BackGround_Low;
			switch (bgorderID)
			{
			case UIBackGroundManager.eBackGroundOrderID.Low:
				sortorderID = UIDefine.eSortOrderTypeID.BackGround_Low;
				break;
			case UIBackGroundManager.eBackGroundOrderID.OverlayUI:
				sortorderID = UIDefine.eSortOrderTypeID.BackGround_OverlayUI0;
				break;
			case UIBackGroundManager.eBackGroundOrderID.OverlayUI1:
				sortorderID = UIDefine.eSortOrderTypeID.BackGround_OverlayUI1;
				break;
			case UIBackGroundManager.eBackGroundOrderID.OverlayUI2:
				sortorderID = UIDefine.eSortOrderTypeID.BackGround_OverlayUI2;
				break;
			}
			uibackGroundHandler.BGOrderID = bgorderID;
			uibackGroundHandler.Open(filePath, fade, sortorderID);
			uibackGroundHandler.SetAnchor(UIBackGroundHandler.eAnchor.Middle);
			this.m_ActiveHandlers[(int)bgorderID] = uibackGroundHandler;
		}

		// Token: 0x06002E34 RID: 11828 RVA: 0x000F368B File Offset: 0x000F1A8B
		public void Change(UIBackGroundManager.eBackGroundOrderID bgOrderID, UIBackGroundManager.eBackGroundID bgID, bool fade)
		{
			this.Change(bgOrderID, UIBackGroundManager.FileNames[(int)bgID], fade);
		}

		// Token: 0x06002E35 RID: 11829 RVA: 0x000F369C File Offset: 0x000F1A9C
		public void Close(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
			if (this.m_ActiveHandlers[(int)bgorderID] != null)
			{
				this.m_ActiveHandlers[(int)bgorderID].Close();
				this.m_ActiveHandlers[(int)bgorderID] = null;
			}
		}

		// Token: 0x06002E36 RID: 11830 RVA: 0x000F36C7 File Offset: 0x000F1AC7
		public void Hide(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
			if (this.m_ActiveHandlers[(int)bgorderID] != null)
			{
				this.m_ActiveHandlers[(int)bgorderID].gameObject.SetActive(false);
			}
		}

		// Token: 0x06002E37 RID: 11831 RVA: 0x000F36EF File Offset: 0x000F1AEF
		public void Show(UIBackGroundManager.eBackGroundOrderID bgorderID)
		{
			if (this.m_ActiveHandlers[(int)bgorderID] != null)
			{
				this.m_ActiveHandlers[(int)bgorderID].gameObject.SetActive(true);
			}
		}

		// Token: 0x06002E38 RID: 11832 RVA: 0x000F3718 File Offset: 0x000F1B18
		public void Flush()
		{
			for (int i = 0; i < this.m_ActiveHandlerList.Count; i++)
			{
				if (this.m_ActiveHandlerList[i] != this.m_ActiveHandlers[(int)this.m_ActiveHandlerList[i].BGOrderID])
				{
					this.m_ActiveHandlerList[i].Close();
				}
			}
		}

		// Token: 0x06002E39 RID: 11833 RVA: 0x000F3780 File Offset: 0x000F1B80
		public void Clear()
		{
			while (this.m_ActiveHandlerList.Count > 0)
			{
				UIBackGroundHandler uibackGroundHandler = this.m_ActiveHandlerList[0];
				uibackGroundHandler.Destroy();
				if (this.m_ActiveHandlerList.Contains(uibackGroundHandler))
				{
					this.m_ActiveHandlerList.Remove(uibackGroundHandler);
				}
			}
		}

		// Token: 0x06002E3A RID: 11834 RVA: 0x000F37D4 File Offset: 0x000F1BD4
		public void Sink(UIBackGroundHandler handler)
		{
			if (handler == this.m_ActiveHandlers[(int)handler.BGOrderID])
			{
				this.m_ActiveHandlers[(int)handler.BGOrderID] = null;
			}
			if (this.m_ActiveHandlerList.Remove(handler))
			{
				this.m_EmptyHandlerList.Add(handler);
			}
		}

		// Token: 0x0400353C RID: 13628
		private const int BG_INST_NUM = 4;

		// Token: 0x0400353D RID: 13629
		public static readonly string[] FileNames = new string[]
		{
			"BG_Menu_Com_02_11",
			"BG_Menu_Com_02_01",
			"BG_Menu_Com_02_03",
			"BG_Menu_Com_02_02",
			"BG_Menu_Com_02_04",
			"BG_Menu_Com_02_05",
			"BG_Menu_Com_02_06",
			"BG_Menu_Com_01_00",
			"BG_Menu_Com_02_13",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_09",
			"BG_Menu_Com_01_12",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_01",
			"BG_Menu_Com_01_01"
		};

		// Token: 0x0400353E RID: 13630
		[SerializeField]
		private UIBackGroundHandler m_HandlerPrefab;

		// Token: 0x0400353F RID: 13631
		[SerializeField]
		private Camera m_UICamera;

		// Token: 0x04003540 RID: 13632
		private List<UIBackGroundHandler> m_EmptyHandlerList = new List<UIBackGroundHandler>();

		// Token: 0x04003541 RID: 13633
		private List<UIBackGroundHandler> m_ActiveHandlerList = new List<UIBackGroundHandler>();

		// Token: 0x04003542 RID: 13634
		private UIBackGroundHandler[] m_ActiveHandlers = new UIBackGroundHandler[4];

		// Token: 0x020008B2 RID: 2226
		public enum eBackGroundID
		{
			// Token: 0x04003544 RID: 13636
			Common,
			// Token: 0x04003545 RID: 13637
			Shop,
			// Token: 0x04003546 RID: 13638
			Trade,
			// Token: 0x04003547 RID: 13639
			Weapon,
			// Token: 0x04003548 RID: 13640
			Build,
			// Token: 0x04003549 RID: 13641
			Summon,
			// Token: 0x0400354A RID: 13642
			Training,
			// Token: 0x0400354B RID: 13643
			Quest,
			// Token: 0x0400354C RID: 13644
			Menu,
			// Token: 0x0400354D RID: 13645
			QuestStory_0,
			// Token: 0x0400354E RID: 13646
			QuestStory_1,
			// Token: 0x0400354F RID: 13647
			QuestStory_2,
			// Token: 0x04003550 RID: 13648
			QuestStory_3,
			// Token: 0x04003551 RID: 13649
			QuestStory_4,
			// Token: 0x04003552 RID: 13650
			QuestStory_5,
			// Token: 0x04003553 RID: 13651
			QuestStory_6,
			// Token: 0x04003554 RID: 13652
			QuestStory_7,
			// Token: 0x04003555 RID: 13653
			QuestStory_8
		}

		// Token: 0x020008B3 RID: 2227
		public enum eBackGroundOrderID
		{
			// Token: 0x04003557 RID: 13655
			Low,
			// Token: 0x04003558 RID: 13656
			OverlayUI,
			// Token: 0x04003559 RID: 13657
			OverlayUI1,
			// Token: 0x0400355A RID: 13658
			OverlayUI2,
			// Token: 0x0400355B RID: 13659
			Num
		}
	}
}
