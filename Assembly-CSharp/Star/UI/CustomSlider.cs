﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000810 RID: 2064
	public class CustomSlider : MonoBehaviour
	{
		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x06002AA6 RID: 10918 RVA: 0x000E17C3 File Offset: 0x000DFBC3
		public Slider Slider
		{
			get
			{
				return this.m_Slider;
			}
		}

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x06002AA7 RID: 10919 RVA: 0x000E17CB File Offset: 0x000DFBCB
		// (set) Token: 0x06002AA8 RID: 10920 RVA: 0x000E17D8 File Offset: 0x000DFBD8
		public float NormalizedValue
		{
			get
			{
				return this.m_Slider.normalizedValue;
			}
			set
			{
				this.m_Slider.normalizedValue = value;
				this.OnValueChangedCallBack(this.m_Slider.value);
			}
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x06002AA9 RID: 10921 RVA: 0x000E17F7 File Offset: 0x000DFBF7
		// (set) Token: 0x06002AAA RID: 10922 RVA: 0x000E1804 File Offset: 0x000DFC04
		public float Value
		{
			get
			{
				return this.m_Slider.value;
			}
			set
			{
				this.m_Slider.value = value;
				this.OnValueChangedCallBack(value);
			}
		}

		// Token: 0x14000060 RID: 96
		// (add) Token: 0x06002AAB RID: 10923 RVA: 0x000E181C File Offset: 0x000DFC1C
		// (remove) Token: 0x06002AAC RID: 10924 RVA: 0x000E1854 File Offset: 0x000DFC54
		public event Action OnValueChanged;

		// Token: 0x06002AAD RID: 10925 RVA: 0x000E188A File Offset: 0x000DFC8A
		private void Start()
		{
			this.m_Slider.onValueChanged.AddListener(new UnityAction<float>(this.OnValueChangedCallBack));
		}

		// Token: 0x06002AAE RID: 10926 RVA: 0x000E18A8 File Offset: 0x000DFCA8
		private void OnValueChangedCallBack(float value)
		{
			if (this.m_ValueText != null)
			{
				this.m_ValueText.text = this.m_Slider.value.ToString();
			}
			this.OnValueChanged.Call();
		}

		// Token: 0x04003116 RID: 12566
		[SerializeField]
		private Slider m_Slider;

		// Token: 0x04003117 RID: 12567
		[SerializeField]
		private Text m_ValueText;
	}
}
