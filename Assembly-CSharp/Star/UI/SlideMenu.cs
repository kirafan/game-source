﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008A8 RID: 2216
	public class SlideMenu : MonoBehaviour
	{
		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06002DE8 RID: 11752 RVA: 0x000F19E5 File Offset: 0x000EFDE5
		public bool IsOpen
		{
			get
			{
				return this.m_IsOpenSlideMenu;
			}
		}

		// Token: 0x06002DE9 RID: 11753 RVA: 0x000F19ED File Offset: 0x000EFDED
		private void Start()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
		}

		// Token: 0x06002DEA RID: 11754 RVA: 0x000F1A00 File Offset: 0x000EFE00
		public void Prepare()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = this.m_GameObject.GetComponent<RectTransform>();
			this.m_ShowPos = this.m_RectTransform.anchoredPosition;
			this.m_HidePos = this.m_ShowPos + new Vector3(this.m_RectTransform.sizeDelta.x, 0f);
			this.m_RectTransform.anchoredPosition = this.m_HidePos;
			if (this.m_ToggleImageRectTransform != null)
			{
				this.m_ToggleImageRectTransform.localScaleX(-1f);
			}
			this.m_IsDonePrepare = true;
		}

		// Token: 0x06002DEB RID: 11755 RVA: 0x000F1AAC File Offset: 0x000EFEAC
		public void ToggleSlideMenu()
		{
			this.m_IsOpenSlideMenu = !this.m_IsOpenSlideMenu;
			if (this.m_IsOpenSlideMenu)
			{
				this.Open();
			}
			else
			{
				this.Close();
			}
		}

		// Token: 0x06002DEC RID: 11756 RVA: 0x000F1AD9 File Offset: 0x000EFED9
		public void Open()
		{
			this.m_IsOpenSlideMenu = true;
			if (this.m_ToggleImageRectTransform != null)
			{
				this.m_ToggleImageRectTransform.localScaleX(1f);
			}
			this.PlayInSlideMenu();
		}

		// Token: 0x06002DED RID: 11757 RVA: 0x000F1B09 File Offset: 0x000EFF09
		public void Close()
		{
			this.m_IsOpenSlideMenu = false;
			if (this.m_ToggleImageRectTransform != null)
			{
				this.m_ToggleImageRectTransform.localScaleX(-1f);
			}
			this.PlayOutSlideMenu();
		}

		// Token: 0x06002DEE RID: 11758 RVA: 0x000F1B3C File Offset: 0x000EFF3C
		private void PlayInSlideMenu()
		{
			RectTransform component = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector3 vector = this.m_ShowPos + new Vector3(((this.m_RectTransform.anchorMin.x + this.m_RectTransform.anchorMax.x) * 0.5f - 0.5f) * component.rect.width, ((this.m_RectTransform.anchorMin.y + this.m_RectTransform.anchorMax.y) * 0.5f - 0.5f) * component.rect.height);
			iTween.Stop(this.m_GameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", 0.2f);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", 0.001f);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x06002DEF RID: 11759 RVA: 0x000F1C98 File Offset: 0x000F0098
		private void PlayOutSlideMenu()
		{
			RectTransform component = this.m_RectTransform.parent.GetComponent<RectTransform>();
			Vector3 vector = this.m_HidePos + new Vector3(((this.m_RectTransform.anchorMin.x + this.m_RectTransform.anchorMax.x) * 0.5f - 0.5f) * component.rect.width, ((this.m_RectTransform.anchorMin.y + this.m_RectTransform.anchorMax.y) * 0.5f - 0.5f) * component.rect.height);
			iTween.Stop(this.m_GameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", vector.x);
			hashtable.Add("y", vector.y);
			hashtable.Add("time", 0.2f);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", 0.001f);
			iTween.MoveTo(this.m_GameObject, hashtable);
		}

		// Token: 0x040034FB RID: 13563
		private const float SLIDE_DURATION = 0.2f;

		// Token: 0x040034FC RID: 13564
		private GameObject m_GameObject;

		// Token: 0x040034FD RID: 13565
		private RectTransform m_RectTransform;

		// Token: 0x040034FE RID: 13566
		private Vector3 m_ShowPos;

		// Token: 0x040034FF RID: 13567
		private Vector3 m_HidePos;

		// Token: 0x04003500 RID: 13568
		[SerializeField]
		private RectTransform m_ToggleImageRectTransform;

		// Token: 0x04003501 RID: 13569
		private bool m_IsDonePrepare;

		// Token: 0x04003502 RID: 13570
		private bool m_IsOpenSlideMenu;
	}
}
