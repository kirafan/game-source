﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007EB RID: 2027
	[Serializable]
	public class BCloneInfo
	{
		// Token: 0x060029DA RID: 10714 RVA: 0x000DCF89 File Offset: 0x000DB389
		public virtual void Setup(Transform parent)
		{
		}

		// Token: 0x060029DB RID: 10715 RVA: 0x000DCF8B File Offset: 0x000DB38B
		public virtual bool IsExistPrefab()
		{
			return false;
		}

		// Token: 0x060029DC RID: 10716 RVA: 0x000DCF8E File Offset: 0x000DB38E
		public virtual void SetLocalPositionX(float x)
		{
		}

		// Token: 0x060029DD RID: 10717 RVA: 0x000DCF90 File Offset: 0x000DB390
		public virtual void SetLocalPositionY(float y)
		{
		}

		// Token: 0x060029DE RID: 10718 RVA: 0x000DCF92 File Offset: 0x000DB392
		public virtual float GetAxisRange()
		{
			return 0f;
		}

		// Token: 0x060029DF RID: 10719 RVA: 0x000DCF99 File Offset: 0x000DB399
		public virtual int GetDepth()
		{
			return -1;
		}
	}
}
