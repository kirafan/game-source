﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000ACF RID: 2767
	public class CommonMessageWindow : MonoBehaviour
	{
		// Token: 0x17000361 RID: 865
		// (get) Token: 0x060039CB RID: 14795 RVA: 0x00126ACB File Offset: 0x00124ECB
		public bool IsActive
		{
			get
			{
				return this.m_IsActive;
			}
		}

		// Token: 0x060039CC RID: 14796 RVA: 0x00126AD3 File Offset: 0x00124ED3
		private void Update()
		{
			this.UpdateAndroidBackKey();
		}

		// Token: 0x060039CD RID: 14797 RVA: 0x00126ADB File Offset: 0x00124EDB
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_TargetBackButton, new Func<bool>(this.CheckUpdateAndroidBackKey));
		}

		// Token: 0x060039CE RID: 14798 RVA: 0x00126AF4 File Offset: 0x00124EF4
		private bool CheckUpdateAndroidBackKey()
		{
			return this.m_IsActive && !this.IsPlayingAnim();
		}

		// Token: 0x060039CF RID: 14799 RVA: 0x00126B14 File Offset: 0x00124F14
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_Body.SetActive(false);
			this.m_BodyTransform = this.m_Body.GetComponent<RectTransform>();
			this.m_InvisibleGraphic.raycastTarget = false;
			this.m_MainTextObj = this.m_MainText.gameObject;
			this.m_SubTextObj = this.m_SubText.gameObject;
		}

		// Token: 0x060039D0 RID: 14800 RVA: 0x00126B78 File Offset: 0x00124F78
		public void ChangeYesButtonText(string text)
		{
			this.m_ButtonYes.GetComponentInChildren<Text>().text = text;
		}

		// Token: 0x060039D1 RID: 14801 RVA: 0x00126B8B File Offset: 0x00124F8B
		public void ChangeNoButtonText(string text)
		{
			this.m_ButtonNo.GetComponentInChildren<Text>().text = text;
		}

		// Token: 0x060039D2 RID: 14802 RVA: 0x00126B9E File Offset: 0x00124F9E
		public void ChangeOKButtonText(string text)
		{
			this.m_ButtonOK.GetComponentInChildren<Text>().text = text;
		}

		// Token: 0x060039D3 RID: 14803 RVA: 0x00126BB1 File Offset: 0x00124FB1
		public void Open(CommonMessageWindow.eType type, string titleText, string text, string subText, float delay, Action<int> onClosedWindowFunc = null)
		{
			this.OpenCommonConstruct(type, titleText, text, subText, delay, onClosedWindowFunc);
		}

		// Token: 0x060039D4 RID: 14804 RVA: 0x00126BC2 File Offset: 0x00124FC2
		public void Open(CommonMessageWindow.eType type, string titleText, string text, string subText, Action<int> onClosedWindowFunc = null)
		{
			this.OpenCommonConstruct(type, titleText, text, subText, 0f, onClosedWindowFunc);
		}

		// Token: 0x060039D5 RID: 14805 RVA: 0x00126BD6 File Offset: 0x00124FD6
		public void Open(CommonMessageWindow.eType type, string text, float delay, Action<int> onClosedWindowFunc = null)
		{
			this.OpenCommonConstruct(type, string.Empty, text, string.Empty, delay, onClosedWindowFunc);
		}

		// Token: 0x060039D6 RID: 14806 RVA: 0x00126BED File Offset: 0x00124FED
		public void Open(CommonMessageWindow.eType type, string text, Action<int> onClosedWindowFunc = null)
		{
			this.OpenCommonConstruct(type, string.Empty, text, string.Empty, 0f, onClosedWindowFunc);
		}

		// Token: 0x060039D7 RID: 14807 RVA: 0x00126C08 File Offset: 0x00125008
		public void Open(CommonMessageWindow.eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, float delay, Action<int> onClosedWindowFunc = null)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Open(type, string.Empty, string.Empty, string.Empty, onClosedWindowFunc);
			}
			else
			{
				string textMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(titleTextDBIndex);
				string textMessage2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(textDBIndex);
				this.Open(type, textMessage, textMessage2, null, onClosedWindowFunc);
			}
		}

		// Token: 0x060039D8 RID: 14808 RVA: 0x00126C7F File Offset: 0x0012507F
		public void Open(CommonMessageWindow.eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, Action<int> onClosedWindowFunc = null)
		{
			this.Open(type, titleTextDBIndex, textDBIndex, 0f, onClosedWindowFunc);
		}

		// Token: 0x060039D9 RID: 14809 RVA: 0x00126C94 File Offset: 0x00125094
		public void Open(CommonMessageWindow.eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, eText_MessageDB subTextDBIndex, float delay, Action<int> onClosedWindowFunc = null)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Open(type, string.Empty, string.Empty, string.Empty, onClosedWindowFunc);
			}
			else
			{
				string textMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(titleTextDBIndex);
				string textMessage2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(textDBIndex);
				string textMessage3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(subTextDBIndex);
				this.Open(type, textMessage, textMessage2, textMessage3, onClosedWindowFunc);
			}
		}

		// Token: 0x060039DA RID: 14810 RVA: 0x00126D1D File Offset: 0x0012511D
		public void Open(CommonMessageWindow.eType type, eText_MessageDB titleTextDBIndex, eText_MessageDB textDBIndex, eText_MessageDB subTextDBIndex, Action<int> onClosedWindowFunc = null)
		{
			this.Open(type, titleTextDBIndex, textDBIndex, subTextDBIndex, 0f, onClosedWindowFunc);
		}

		// Token: 0x060039DB RID: 14811 RVA: 0x00126D34 File Offset: 0x00125134
		public void Open(CommonMessageWindow.eType type, eText_MessageDB textDBIndex, float delay, Action<int> onClosedWindowFunc = null)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Open(type, string.Empty, onClosedWindowFunc);
			}
			else
			{
				this.Open(type, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(textDBIndex), onClosedWindowFunc);
			}
		}

		// Token: 0x060039DC RID: 14812 RVA: 0x00126D8C File Offset: 0x0012518C
		public void Open(CommonMessageWindow.eType type, eText_MessageDB textDBIndex, Action<int> onClosedWindowFunc = null)
		{
			this.Open(type, textDBIndex, 0f, onClosedWindowFunc);
		}

		// Token: 0x060039DD RID: 14813 RVA: 0x00126D9C File Offset: 0x0012519C
		private void OpenCommonConstruct(CommonMessageWindow.eType type, string titleText, string text, string subText, float delay, Action<int> onClosedWindowFunc)
		{
			CommonMessageWindow.Param param = new CommonMessageWindow.Param();
			param.m_Type = type;
			param.m_TitleText = titleText;
			param.m_Text = text;
			param.m_SubText = subText;
			param.m_Delay = delay;
			param.OnClosedWindowFunc = onClosedWindowFunc;
			this.m_List.Add(param);
			if (this.m_List.Count == 1)
			{
				this.OpenFromStack();
			}
			this.m_InvisibleGraphic.raycastTarget = true;
		}

		// Token: 0x060039DE RID: 14814 RVA: 0x00126E0C File Offset: 0x0012520C
		private void OpenFromStack()
		{
			if (this.m_List.Count <= 0)
			{
				this.m_InvisibleGraphic.raycastTarget = false;
				return;
			}
			CommonMessageWindow.Param param = this.m_List[0];
			this.m_Type = param.m_Type;
			if (string.IsNullOrEmpty(param.m_TitleText))
			{
				this.m_TitleBarObj.SetActive(false);
			}
			else
			{
				this.m_TitleBarObj.SetActive(true);
				this.m_TitleText.text = param.m_TitleText;
			}
			if (string.IsNullOrEmpty(param.m_Text))
			{
				this.m_MainTextObj.SetActive(false);
			}
			else
			{
				this.m_MainTextObj.SetActive(true);
				this.m_MainText.text = param.m_Text;
			}
			if (string.IsNullOrEmpty(param.m_SubText))
			{
				this.m_SubTextObj.SetActive(false);
			}
			else
			{
				this.m_SubTextObj.SetActive(true);
				this.m_SubText.text = param.m_SubText;
			}
			this.OnClosedWindow = null;
			if (param.OnClosedWindowFunc != null)
			{
				this.OnClosedWindow = param.OnClosedWindowFunc;
			}
			this.m_TargetBackButton = null;
			this.m_Body.SetActive(true);
			switch (this.m_Type)
			{
			case CommonMessageWindow.eType.None:
				this.m_ButtonOK.SetActive(false);
				this.m_ButtonYes.SetActive(false);
				this.m_ButtonNo.SetActive(false);
				break;
			case CommonMessageWindow.eType.OK:
			case CommonMessageWindow.eType.OK_NoClose:
				this.m_ButtonOK.SetActive(true);
				this.m_ButtonYes.SetActive(false);
				this.m_ButtonNo.SetActive(false);
				this.m_TargetBackButton = this.m_ButtonOK.GetComponent<CustomButton>();
				break;
			case CommonMessageWindow.eType.YES_NO:
				this.m_ButtonOK.SetActive(false);
				this.m_ButtonYes.SetActive(true);
				this.m_ButtonNo.SetActive(true);
				this.m_TargetBackButton = this.m_ButtonNo.GetComponent<CustomButton>();
				break;
			case CommonMessageWindow.eType.Retry:
				this.m_ButtonOK.SetActive(true);
				this.m_ButtonYes.SetActive(false);
				this.m_ButtonNo.SetActive(false);
				break;
			}
			this.m_BodyTransform.localScale = new Vector3(0.5f, 0.5f, 1f);
			this.m_CanvasGroup.alpha = 0f;
			iTween.Stop(this.m_Body);
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", 1f);
			hashtable.Add("y", 1f);
			hashtable.Add("time", this.m_AnimDuration);
			hashtable.Add("easeType", iTween.EaseType.linear);
			hashtable.Add("delay", param.m_Delay);
			hashtable.Add("oncomplete", "OnCompleteOpen");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ScaleTo(this.m_Body, hashtable);
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("from", 0f);
			hashtable2.Add("to", 1f);
			hashtable2.Add("time", this.m_AnimDuration);
			hashtable2.Add("easeType", iTween.EaseType.linear);
			hashtable2.Add("delay", param.m_Delay);
			hashtable2.Add("onupdate", "OnUpdateAlpha");
			hashtable2.Add("onupdatetarget", this.m_GameObject);
			iTween.ValueTo(this.m_Body, hashtable2);
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				this.m_Anims[i].PlayIn();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_WINDOW_OPEN, 1f, 0, -1, -1);
			if (this.m_Type == CommonMessageWindow.eType.None)
			{
				this.m_ButtonParent.SetActive(false);
			}
			else
			{
				this.m_ButtonParent.SetActive(true);
			}
			this.m_InvisibleGraphic.raycastTarget = true;
			this.SetEnableInput(false);
			this.m_IsActive = true;
		}

		// Token: 0x060039DF RID: 14815 RVA: 0x00127224 File Offset: 0x00125624
		public void Close()
		{
			this.m_BodyTransform.localScale = new Vector3(1f, 1f, 1f);
			this.m_CanvasGroup.alpha = 1f;
			iTween.Stop(this.m_Body);
			Hashtable hashtable = new Hashtable();
			hashtable.Add("x", 0f);
			hashtable.Add("y", 0f);
			hashtable.Add("time", this.m_AnimDuration);
			hashtable.Add("easeType", iTween.EaseType.linear);
			hashtable.Add("oncomplete", "OnCompleteClose");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.ScaleTo(this.m_Body, hashtable);
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("from", 1f);
			hashtable2.Add("to", 0f);
			hashtable2.Add("time", this.m_AnimDuration);
			hashtable2.Add("easeType", iTween.EaseType.linear);
			hashtable2.Add("onupdate", "OnUpdateAlpha");
			hashtable2.Add("onupdatetarget", this.m_GameObject);
			iTween.ValueTo(this.m_Body, hashtable2);
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				this.m_Anims[i].PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_WINDOW_CLOSE, 1f, 0, -1, -1);
			this.SetEnableInput(false);
		}

		// Token: 0x060039E0 RID: 14816 RVA: 0x001273C0 File Offset: 0x001257C0
		private bool IsPlayingAnim()
		{
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				if (!this.m_Anims[i].IsEnd)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060039E1 RID: 14817 RVA: 0x001273FB File Offset: 0x001257FB
		public void SetEnableInput(bool flg)
		{
			this.m_CanvasGroup.blocksRaycasts = flg;
		}

		// Token: 0x060039E2 RID: 14818 RVA: 0x00127409 File Offset: 0x00125809
		private void OnCompleteOpen()
		{
			this.SetEnableInput(true);
		}

		// Token: 0x060039E3 RID: 14819 RVA: 0x00127414 File Offset: 0x00125814
		private void OnCompleteClose()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (dbMng.TextCmnDB != null)
			{
				this.ChangeYesButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonYES));
				this.ChangeNoButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonNo));
				this.ChangeOKButtonText(dbMng.GetTextCommon(eText_CommonDB.CommonOK));
			}
			this.OnClosedWindow.Call(this.m_SelectedAnswerIndex);
			this.m_InvisibleGraphic.raycastTarget = false;
			iTween.Stop(this.m_GameObject);
			this.m_Body.SetActive(false);
			this.m_IsActive = false;
			this.m_List.RemoveAt(0);
			this.OpenFromStack();
		}

		// Token: 0x060039E4 RID: 14820 RVA: 0x001274BD File Offset: 0x001258BD
		public void OnClickButtonOK()
		{
			this.m_SelectedAnswerIndex = 0;
			if (this.m_Type == CommonMessageWindow.eType.OK_NoClose)
			{
				this.OnClosedWindow.Call(this.m_SelectedAnswerIndex);
			}
			else
			{
				this.Close();
			}
		}

		// Token: 0x060039E5 RID: 14821 RVA: 0x001274EE File Offset: 0x001258EE
		public void OnClickButtonYes()
		{
			this.m_SelectedAnswerIndex = 0;
			this.Close();
		}

		// Token: 0x060039E6 RID: 14822 RVA: 0x001274FD File Offset: 0x001258FD
		public void OnClickButtonNo()
		{
			this.m_SelectedAnswerIndex = 1;
			this.Close();
		}

		// Token: 0x060039E7 RID: 14823 RVA: 0x0012750C File Offset: 0x0012590C
		public void OnUpdateAlpha(float value)
		{
			this.m_CanvasGroup.alpha = value;
		}

		// Token: 0x0400410E RID: 16654
		[SerializeField]
		private float m_AnimDuration = 1f;

		// Token: 0x0400410F RID: 16655
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x04004110 RID: 16656
		private RectTransform m_BodyTransform;

		// Token: 0x04004111 RID: 16657
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04004112 RID: 16658
		[SerializeField]
		private GameObject m_TitleBarObj;

		// Token: 0x04004113 RID: 16659
		[SerializeField]
		private Text m_MainText;

		// Token: 0x04004114 RID: 16660
		private GameObject m_MainTextObj;

		// Token: 0x04004115 RID: 16661
		[SerializeField]
		private Text m_SubText;

		// Token: 0x04004116 RID: 16662
		private GameObject m_SubTextObj;

		// Token: 0x04004117 RID: 16663
		[SerializeField]
		private GameObject m_ButtonParent;

		// Token: 0x04004118 RID: 16664
		[SerializeField]
		private GameObject m_ButtonOK;

		// Token: 0x04004119 RID: 16665
		[SerializeField]
		private GameObject m_ButtonYes;

		// Token: 0x0400411A RID: 16666
		[SerializeField]
		private GameObject m_ButtonNo;

		// Token: 0x0400411B RID: 16667
		[SerializeField]
		private InvisibleGraphic m_InvisibleGraphic;

		// Token: 0x0400411C RID: 16668
		[SerializeField]
		private CanvasGroup m_CanvasGroup;

		// Token: 0x0400411D RID: 16669
		[SerializeField]
		private AnimUIPlayer[] m_Anims;

		// Token: 0x0400411E RID: 16670
		private const float WINDOW_HEIGHT_MARGIN = 200f;

		// Token: 0x0400411F RID: 16671
		private const float WINDOW_HEIGHT_BUTTON_SPACE = 100f;

		// Token: 0x04004120 RID: 16672
		private const float WINDOW_HEIGHT_MAX = 720f;

		// Token: 0x04004121 RID: 16673
		private const float TEXTBOX_HEIGHT_MAX = 450f;

		// Token: 0x04004122 RID: 16674
		private bool m_IsActive;

		// Token: 0x04004123 RID: 16675
		private CommonMessageWindow.eType m_Type;

		// Token: 0x04004124 RID: 16676
		public const int ANSWER_YES = 0;

		// Token: 0x04004125 RID: 16677
		public const int ANSWER_NO = 1;

		// Token: 0x04004126 RID: 16678
		private int m_SelectedAnswerIndex;

		// Token: 0x04004127 RID: 16679
		private List<CommonMessageWindow.Param> m_List = new List<CommonMessageWindow.Param>();

		// Token: 0x04004128 RID: 16680
		private CustomButton m_TargetBackButton;

		// Token: 0x04004129 RID: 16681
		private GameObject m_GameObject;

		// Token: 0x0400412A RID: 16682
		private Action<int> OnClosedWindow;

		// Token: 0x02000AD0 RID: 2768
		public enum eType
		{
			// Token: 0x0400412C RID: 16684
			None,
			// Token: 0x0400412D RID: 16685
			OK,
			// Token: 0x0400412E RID: 16686
			YES_NO,
			// Token: 0x0400412F RID: 16687
			Retry,
			// Token: 0x04004130 RID: 16688
			OK_NoClose
		}

		// Token: 0x02000AD1 RID: 2769
		private class Param
		{
			// Token: 0x04004131 RID: 16689
			public CommonMessageWindow.eType m_Type;

			// Token: 0x04004132 RID: 16690
			public string m_TitleText;

			// Token: 0x04004133 RID: 16691
			public string m_Text;

			// Token: 0x04004134 RID: 16692
			public string m_SubText;

			// Token: 0x04004135 RID: 16693
			public float m_Delay;

			// Token: 0x04004136 RID: 16694
			public Action<int> OnClosedWindowFunc;
		}
	}
}
