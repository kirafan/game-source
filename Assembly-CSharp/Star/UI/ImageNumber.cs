﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200087E RID: 2174
	public class ImageNumber : MonoBehaviour
	{
		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06002D06 RID: 11526 RVA: 0x000ED893 File Offset: 0x000EBC93
		public RectTransform CacheRectTransform
		{
			get
			{
				if (this.m_Transform == null)
				{
					this.m_Transform = base.gameObject.GetComponent<RectTransform>();
				}
				return this.m_Transform;
			}
		}

		// Token: 0x06002D07 RID: 11527 RVA: 0x000ED8BD File Offset: 0x000EBCBD
		private void Awake()
		{
			if (this.m_Transform == null)
			{
				this.m_Transform = base.gameObject.GetComponent<RectTransform>();
			}
		}

		// Token: 0x06002D08 RID: 11528 RVA: 0x000ED8E4 File Offset: 0x000EBCE4
		public void SetValue(int value)
		{
			switch (value)
			{
			case 0:
				this.m_Image.sprite = this.m_0;
				break;
			case 1:
				this.m_Image.sprite = this.m_1;
				break;
			case 2:
				this.m_Image.sprite = this.m_2;
				break;
			case 3:
				this.m_Image.sprite = this.m_3;
				break;
			case 4:
				this.m_Image.sprite = this.m_4;
				break;
			case 5:
				this.m_Image.sprite = this.m_5;
				break;
			case 6:
				this.m_Image.sprite = this.m_6;
				break;
			case 7:
				this.m_Image.sprite = this.m_7;
				break;
			case 8:
				this.m_Image.sprite = this.m_8;
				break;
			case 9:
				this.m_Image.sprite = this.m_9;
				break;
			}
		}

		// Token: 0x06002D09 RID: 11529 RVA: 0x000EDA00 File Offset: 0x000EBE00
		public void SetColor(Color color)
		{
			this.m_Image.color = color;
		}

		// Token: 0x06002D0A RID: 11530 RVA: 0x000EDA0E File Offset: 0x000EBE0E
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x0400341E RID: 13342
		[SerializeField]
		private Image m_Image;

		// Token: 0x0400341F RID: 13343
		[SerializeField]
		private Sprite m_0;

		// Token: 0x04003420 RID: 13344
		[SerializeField]
		private Sprite m_1;

		// Token: 0x04003421 RID: 13345
		[SerializeField]
		private Sprite m_2;

		// Token: 0x04003422 RID: 13346
		[SerializeField]
		private Sprite m_3;

		// Token: 0x04003423 RID: 13347
		[SerializeField]
		private Sprite m_4;

		// Token: 0x04003424 RID: 13348
		[SerializeField]
		private Sprite m_5;

		// Token: 0x04003425 RID: 13349
		[SerializeField]
		private Sprite m_6;

		// Token: 0x04003426 RID: 13350
		[SerializeField]
		private Sprite m_7;

		// Token: 0x04003427 RID: 13351
		[SerializeField]
		private Sprite m_8;

		// Token: 0x04003428 RID: 13352
		[SerializeField]
		private Sprite m_9;

		// Token: 0x04003429 RID: 13353
		private RectTransform m_Transform;
	}
}
