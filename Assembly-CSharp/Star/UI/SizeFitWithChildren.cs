﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008A4 RID: 2212
	public class SizeFitWithChildren : MonoBehaviour
	{
		// Token: 0x06002DE1 RID: 11745 RVA: 0x000F16F3 File Offset: 0x000EFAF3
		private void Start()
		{
			this.Rebuild();
		}

		// Token: 0x06002DE2 RID: 11746 RVA: 0x000F16FB File Offset: 0x000EFAFB
		private void OnValidate()
		{
			this.Rebuild();
		}

		// Token: 0x06002DE3 RID: 11747 RVA: 0x000F1704 File Offset: 0x000EFB04
		public void Rebuild()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(this.m_FitTarget);
			RectTransform component = base.GetComponent<RectTransform>();
			float num = component.sizeDelta.x;
			float num2 = component.sizeDelta.y;
			if (this.m_Width)
			{
				num = this.m_Padding.m_Left + this.m_Padding.m_Right + this.m_FitTarget.sizeDelta.x;
			}
			if (this.m_Height)
			{
				num2 = this.m_Padding.m_Top + this.m_Padding.m_Bottom + this.m_FitTarget.sizeDelta.y;
			}
			component.sizeDelta = new Vector2(num, num2);
			this.m_FitTarget.anchoredPosition = new Vector2(-num * component.pivot.x + this.m_Padding.m_Left + this.m_FitTarget.rect.width * this.m_FitTarget.pivot.x, num2 * component.pivot.y - this.m_Padding.m_Top - this.m_FitTarget.rect.height * this.m_FitTarget.pivot.y);
		}

		// Token: 0x040034EA RID: 13546
		[SerializeField]
		private RectTransform m_FitTarget;

		// Token: 0x040034EB RID: 13547
		[SerializeField]
		private bool m_Width = true;

		// Token: 0x040034EC RID: 13548
		[SerializeField]
		private bool m_Height = true;

		// Token: 0x040034ED RID: 13549
		[SerializeField]
		private SizeFitWithChildren.Padding m_Padding;

		// Token: 0x020008A5 RID: 2213
		[Serializable]
		public class Padding
		{
			// Token: 0x040034EE RID: 13550
			public float m_Left;

			// Token: 0x040034EF RID: 13551
			public float m_Right;

			// Token: 0x040034F0 RID: 13552
			public float m_Top;

			// Token: 0x040034F1 RID: 13553
			public float m_Bottom;
		}
	}
}
