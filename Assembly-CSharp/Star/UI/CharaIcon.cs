﻿using System;

namespace Star.UI
{
	// Token: 0x02000858 RID: 2136
	public class CharaIcon : ASyncImage
	{
		// Token: 0x06002C70 RID: 11376 RVA: 0x000EA848 File Offset: 0x000E8C48
		public void Apply(int charaID)
		{
			if (this.m_CharaID != charaID)
			{
				this.Apply();
				if (charaID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIcon(charaID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_CharaID = charaID;
			}
		}

		// Token: 0x06002C71 RID: 11377 RVA: 0x000EA8AE File Offset: 0x000E8CAE
		public override void Destroy()
		{
			base.Destroy();
			this.m_CharaID = -1;
		}

		// Token: 0x04003354 RID: 13140
		protected int m_CharaID = -1;
	}
}
