﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009C7 RID: 2503
	public class LoadingUI : HaveTipsUI
	{
		// Token: 0x06003401 RID: 13313 RVA: 0x00107CED File Offset: 0x001060ED
		protected override void Start()
		{
			base.Start();
			this.m_InvisibleGraphic.SetActive(false);
			this.m_Accessories.Setup();
		}

		// Token: 0x06003402 RID: 13314 RVA: 0x00107D0C File Offset: 0x0010610C
		public bool IsOverlay()
		{
			return this.m_InvisibleGraphic.activeSelf;
		}

		// Token: 0x06003403 RID: 13315 RVA: 0x00107D24 File Offset: 0x00106124
		public override void Abort()
		{
			base.Abort();
			this.m_Accessories.Close();
			this.m_InvisibleGraphic.SetActive(false);
			if (this.m_PlayChangeAnim && this.m_SceneChange != null)
			{
				this.m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
			}
		}

		// Token: 0x06003404 RID: 13316 RVA: 0x00107D78 File Offset: 0x00106178
		public void Open(LoadingUI.eDisplayMode displayMode = LoadingUI.eDisplayMode.Default, eTipsCategory tipCategory = eTipsCategory.None)
		{
			this.m_DisplayMode = displayMode;
			this.m_InvisibleGraphic.SetActive(true);
			this.m_ProgressBar.SetActive(false);
			this.SetProgress(0f);
			base.Open(tipCategory);
			LoadingUI.eDisplayMode displayMode2 = this.m_DisplayMode;
			if (displayMode2 != LoadingUI.eDisplayMode.NowLoadingOnly)
			{
				if (this.m_PlayChangeAnim && this.m_SceneChange != null)
				{
					this.m_SceneChange.Play(SceneChange.eAnimType.In);
				}
			}
			else if (this.m_PlayChangeAnim && this.m_SceneChange != null)
			{
				this.m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
			}
		}

		// Token: 0x06003405 RID: 13317 RVA: 0x00107E24 File Offset: 0x00106224
		public virtual void OpenQuickIfNotDisplay(LoadingUI.eDisplayMode displayMode = LoadingUI.eDisplayMode.Default, eTipsCategory tipCategory = eTipsCategory.None)
		{
			if (this.m_Body.activeSelf)
			{
				return;
			}
			this.m_DisplayMode = displayMode;
			this.m_InvisibleGraphic.SetActive(true);
			this.m_ProgressBar.SetActive(false);
			this.SetProgress(0f);
			LoadingUI.eDisplayMode displayMode2 = this.m_DisplayMode;
			if (displayMode2 != LoadingUI.eDisplayMode.NowLoadingOnly)
			{
				if (this.m_PlayChangeAnim && this.m_SceneChange != null)
				{
					this.m_SceneChange.PlayQuick(SceneChange.eAnimType.In);
				}
			}
			else if (this.m_PlayChangeAnim && this.m_SceneChange != null)
			{
				this.m_SceneChange.PlayQuick(SceneChange.eAnimType.In);
			}
		}

		// Token: 0x06003406 RID: 13318 RVA: 0x00107EDC File Offset: 0x001062DC
		public override void Close()
		{
			this.m_InvisibleGraphic.SetActive(true);
			this.m_Accessories.Close();
			this.m_ProgressBar.SetActive(false);
			base.Close();
			LoadingUI.eDisplayMode displayMode = this.m_DisplayMode;
			if (displayMode != LoadingUI.eDisplayMode.NowLoadingOnly)
			{
				if (this.m_PlayChangeAnim && this.m_SceneChange != null)
				{
					this.m_SceneChange.Play(SceneChange.eAnimType.Out);
				}
			}
			else if (this.m_PlayChangeAnim && this.m_SceneChange != null)
			{
				this.m_SceneChange.PlayQuick(SceneChange.eAnimType.Out);
			}
		}

		// Token: 0x06003407 RID: 13319 RVA: 0x00107F7F File Offset: 0x0010637F
		protected override void UpdateIn()
		{
			if (this.m_PlayChangeAnim && this.m_SceneChange != null && !this.m_SceneChange.IsComplete())
			{
				return;
			}
			base.UpdateIn();
			this.m_Accessories.Open();
		}

		// Token: 0x06003408 RID: 13320 RVA: 0x00107FC0 File Offset: 0x001063C0
		protected override void UpdateOut()
		{
			if (this.m_PlayChangeAnim && this.m_SceneChange != null && !this.m_SceneChange.IsComplete())
			{
				return;
			}
			this.m_InvisibleGraphic.SetActive(false);
			base.UpdateOut();
		}

		// Token: 0x06003409 RID: 13321 RVA: 0x0010800C File Offset: 0x0010640C
		protected override bool IsOpenTips()
		{
			return this.m_DisplayMode != LoadingUI.eDisplayMode.NowLoadingOnly && base.IsOpenTips();
		}

		// Token: 0x0600340A RID: 13322 RVA: 0x00108024 File Offset: 0x00106424
		public void UpdateProgress(float progress)
		{
			if (this.m_SceneChange != null && !this.m_SceneChange.IsComplete())
			{
				return;
			}
			if (!this.m_ProgressBar.activeSelf)
			{
				this.m_ProgressBar.SetActive(true);
			}
			progress = Mathf.Clamp(progress, 0f, 1f);
			if (this.m_Progress != progress)
			{
				this.SetProgress(progress);
			}
		}

		// Token: 0x0600340B RID: 13323 RVA: 0x00108094 File Offset: 0x00106494
		private void SetProgress(float progress)
		{
			this.m_Progress = progress;
			this.m_Gauge.fillAmount = this.m_Progress;
		}

		// Token: 0x04003A46 RID: 14918
		[SerializeField]
		[Tooltip("切り替え演出.")]
		private SceneChange m_SceneChange;

		// Token: 0x04003A47 RID: 14919
		[SerializeField]
		[Tooltip("タッチ判定を無効化するためのシート.")]
		private GameObject m_InvisibleGraphic;

		// Token: 0x04003A48 RID: 14920
		[SerializeField]
		[Tooltip("シーン固有のアニメーション演出など")]
		private LoadingUIAccessories m_Accessories;

		// Token: 0x04003A49 RID: 14921
		[SerializeField]
		[Tooltip("進捗バー")]
		private GameObject m_ProgressBar;

		// Token: 0x04003A4A RID: 14922
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x04003A4B RID: 14923
		[SerializeField]
		private bool m_PlayChangeAnim = true;

		// Token: 0x04003A4C RID: 14924
		private LoadingUI.eDisplayMode m_DisplayMode;

		// Token: 0x04003A4D RID: 14925
		private float m_Progress = -1f;

		// Token: 0x020009C8 RID: 2504
		public enum eDisplayMode
		{
			// Token: 0x04003A4F RID: 14927
			Default,
			// Token: 0x04003A50 RID: 14928
			NowLoadingOnly
		}
	}
}
