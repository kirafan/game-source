﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000ADB RID: 2779
	public class WebViewWindow : MonoBehaviour
	{
		// Token: 0x06003A1E RID: 14878 RVA: 0x00128505 File Offset: 0x00126905
		public void Open(string title, string url)
		{
			this.m_TitleText.text = title;
			base.gameObject.SetActive(true);
			this.m_WebviewGroup.Open(url, null);
		}

		// Token: 0x06003A1F RID: 14879 RVA: 0x0012852C File Offset: 0x0012692C
		public void Close()
		{
			this.m_WebviewGroup.Close();
		}

		// Token: 0x06003A20 RID: 14880 RVA: 0x00128539 File Offset: 0x00126939
		public void Hide()
		{
			this.m_WebviewGroup.Hide();
		}

		// Token: 0x04004163 RID: 16739
		[SerializeField]
		private WebViewGroup m_WebviewGroup;

		// Token: 0x04004164 RID: 16740
		[SerializeField]
		private Text m_TitleText;
	}
}
