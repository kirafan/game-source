﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B08 RID: 2824
	public class TownObjectLivingIcon : MonoBehaviour
	{
		// Token: 0x06003B08 RID: 15112 RVA: 0x0012DF37 File Offset: 0x0012C337
		public void SetEnableRender(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06003B09 RID: 15113 RVA: 0x0012DF45 File Offset: 0x0012C345
		public void SetIcon(Sprite sprite)
		{
			this.m_IconImage.sprite = sprite;
		}

		// Token: 0x040042C3 RID: 17091
		[SerializeField]
		private Image m_IconImage;
	}
}
