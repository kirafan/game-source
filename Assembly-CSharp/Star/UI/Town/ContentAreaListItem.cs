﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AEA RID: 2794
	public class ContentAreaListItem : ScrollItemIcon
	{
		// Token: 0x06003A8A RID: 14986 RVA: 0x0012AB5D File Offset: 0x00128F5D
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06003A8B RID: 14987 RVA: 0x0012AB65 File Offset: 0x00128F65
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x06003A8C RID: 14988 RVA: 0x0012AB70 File Offset: 0x00128F70
		private void Update()
		{
			if (this.m_IconImage.sprite == null && this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable())
			{
				this.m_IconImage.enabled = true;
				this.m_IconImage.sprite = this.m_SpriteHandler.Obj;
			}
		}

		// Token: 0x06003A8D RID: 14989 RVA: 0x0012ABD0 File Offset: 0x00128FD0
		protected override void ApplyData()
		{
			ContentAreaListItemData contentAreaListItemData = (ContentAreaListItemData)this.m_Data;
			int townObjectID = TownUtility.CalcAreaToContentID(contentAreaListItemData.m_ObjID);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(townObjectID);
			this.m_IconImage.enabled = false;
			if (this.m_SpriteHandler != null)
			{
				this.m_IconImage.sprite = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
			}
			this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncContentTitleLogo((eTitleType)param.m_TitleType);
			this.m_TextObj.text = param.m_ObjName;
			int townObjBuildNum = UserTownUtil.GetTownObjBuildNum(contentAreaListItemData.m_ObjID);
			if (townObjBuildNum > 0)
			{
				this.m_CustomButton.Interactable = false;
				this.m_Builded.SetActive(true);
			}
			else
			{
				this.m_CustomButton.Interactable = true;
				this.m_Builded.SetActive(false);
			}
		}

		// Token: 0x06003A8E RID: 14990 RVA: 0x0012ACB9 File Offset: 0x001290B9
		public override void Destroy()
		{
			base.Destroy();
			this.m_IconImage.sprite = null;
			SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
			this.m_SpriteHandler = null;
		}

		// Token: 0x040041FE RID: 16894
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040041FF RID: 16895
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x04004200 RID: 16896
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04004201 RID: 16897
		[SerializeField]
		private GameObject m_Builded;

		// Token: 0x04004202 RID: 16898
		private SpriteHandler m_SpriteHandler;
	}
}
