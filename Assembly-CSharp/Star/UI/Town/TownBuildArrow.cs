﻿using System;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000AF4 RID: 2804
	public class TownBuildArrow : MonoBehaviour
	{
		// Token: 0x06003AB6 RID: 15030 RVA: 0x0012B9B2 File Offset: 0x00129DB2
		private void Start()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_ArrowPos = this.m_RectTransform.localPosition;
		}

		// Token: 0x06003AB7 RID: 15031 RVA: 0x0012B9D1 File Offset: 0x00129DD1
		private void Update()
		{
			this.m_WorkVec.y = Mathf.Sin(Time.time) * this.m_Radius;
			this.m_RectTransform.localPosition = this.m_ArrowPos + this.m_WorkVec;
		}

		// Token: 0x06003AB8 RID: 15032 RVA: 0x0012BA0B File Offset: 0x00129E0B
		public void SetActive(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x0400422E RID: 16942
		[SerializeField]
		private float m_Radius = 20f;

		// Token: 0x0400422F RID: 16943
		private Vector3 m_ArrowPos = new Vector3(0f, 0f, 0f);

		// Token: 0x04004230 RID: 16944
		private Vector3 m_WorkVec = new Vector3(0f, 0f, 0f);

		// Token: 0x04004231 RID: 16945
		private RectTransform m_RectTransform;
	}
}
