﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AFD RID: 2813
	public class TownObjChangeWindow : UIGroup
	{
		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06003AD1 RID: 15057 RVA: 0x0012C3A3 File Offset: 0x0012A7A3
		public TownObjChangeWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x06003AD2 RID: 15058 RVA: 0x0012C3AB File Offset: 0x0012A7AB
		public void Open(long basemngID, int targetobjID)
		{
			this.m_BaseObjMngID = basemngID;
			this.m_TargetObjID = targetobjID;
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Apply();
			}
			base.Open();
		}

		// Token: 0x06003AD3 RID: 15059 RVA: 0x0012C3E6 File Offset: 0x0012A7E6
		public void OnClickYesButtonCallBack()
		{
			this.m_SelectButton = TownObjChangeWindow.eButton.Yes;
			this.Close();
		}

		// Token: 0x06003AD4 RID: 15060 RVA: 0x0012C3F5 File Offset: 0x0012A7F5
		public void OnClickNoButtonCallBack()
		{
			this.m_SelectButton = TownObjChangeWindow.eButton.No;
			this.Close();
		}

		// Token: 0x06003AD5 RID: 15061 RVA: 0x0012C404 File Offset: 0x0012A804
		private void Apply()
		{
			UserTownData.BuildObjectData buildObjectDataAtBuildMngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(this.m_BaseObjMngID);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(TownUtility.CalcAreaToContentID(buildObjectDataAtBuildMngID.m_ObjID));
			this.m_BaseIcon.Apply(buildObjectDataAtBuildMngID.m_ObjID, 1);
			this.m_BaseName.text = param.m_ObjName;
			param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(TownUtility.CalcAreaToContentID(this.m_TargetObjID));
			this.m_TargetIcon.Apply(this.m_TargetObjID, 1);
			this.m_TargetName.text = param.m_ObjName;
		}

		// Token: 0x04004262 RID: 16994
		private long m_BaseObjMngID;

		// Token: 0x04004263 RID: 16995
		private int m_TargetObjID;

		// Token: 0x04004264 RID: 16996
		[SerializeField]
		private TownObjectIcon m_BaseIcon;

		// Token: 0x04004265 RID: 16997
		[SerializeField]
		private Text m_BaseName;

		// Token: 0x04004266 RID: 16998
		[SerializeField]
		private TownObjectIcon m_TargetIcon;

		// Token: 0x04004267 RID: 16999
		[SerializeField]
		private Text m_TargetName;

		// Token: 0x04004268 RID: 17000
		private TownObjChangeWindow.eButton m_SelectButton;

		// Token: 0x02000AFE RID: 2814
		public enum eButton
		{
			// Token: 0x0400426A RID: 17002
			Yes,
			// Token: 0x0400426B RID: 17003
			No
		}
	}
}
