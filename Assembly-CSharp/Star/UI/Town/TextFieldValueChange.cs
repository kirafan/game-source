﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000826 RID: 2086
	public class TextFieldValueChange : MonoBehaviour
	{
		// Token: 0x06002B79 RID: 11129 RVA: 0x000E494D File Offset: 0x000E2D4D
		public void Apply(int before, int after)
		{
			this.Apply(before.ToString(), after.ToString(), (float)(after - before));
		}

		// Token: 0x06002B7A RID: 11130 RVA: 0x000E4974 File Offset: 0x000E2D74
		public void ApplyBuff(int before, int after)
		{
			string before2 = "+" + before.ToString();
			string after2 = "+" + after.ToString();
			this.Apply(before2, after2, (float)(after - before));
		}

		// Token: 0x06002B7B RID: 11131 RVA: 0x000E49C0 File Offset: 0x000E2DC0
		public void ApplyPercent(float before, float after)
		{
			string before2 = before.ToString() + "%";
			string after2 = after.ToString() + "%";
			this.Apply(before2, after2, after - before);
		}

		// Token: 0x06002B7C RID: 11132 RVA: 0x000E4A08 File Offset: 0x000E2E08
		public void ApplyBuffPercent(float before, float after)
		{
			string before2 = "+" + before.ToString() + "%";
			string after2 = "+" + after.ToString() + "%";
			this.Apply(before2, after2, after - before);
		}

		// Token: 0x06002B7D RID: 11133 RVA: 0x000E4A5C File Offset: 0x000E2E5C
		public void Apply(string before, string after, float sub)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(after);
			if (sub > 0f)
			{
				UIUtility.GetIncreaseString(stringBuilder);
			}
			else if (sub < 0f)
			{
				UIUtility.GetDecreaseString(stringBuilder);
			}
			this.m_BeforeValue.text = before.ToString();
			this.m_AfterValue.text = stringBuilder.ToString();
		}

		// Token: 0x040031E1 RID: 12769
		[SerializeField]
		private Text m_BeforeValue;

		// Token: 0x040031E2 RID: 12770
		[SerializeField]
		private Text m_AfterValue;
	}
}
