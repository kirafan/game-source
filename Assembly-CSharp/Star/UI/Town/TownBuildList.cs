﻿using System;
using System.Collections.Generic;

namespace Star.UI.Town
{
	// Token: 0x02000AF5 RID: 2805
	public class TownBuildList
	{
		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06003ABA RID: 15034 RVA: 0x0012BA21 File Offset: 0x00129E21
		public bool IsAvailable
		{
			get
			{
				return this.m_IsAvailable;
			}
		}

		// Token: 0x06003ABB RID: 15035 RVA: 0x0012BA29 File Offset: 0x00129E29
		public void Open(TownBuildList.eCategory category)
		{
			this.m_Category = category;
			this.m_IsOpen = true;
			this.m_IsAvailable = false;
		}

		// Token: 0x06003ABC RID: 15036 RVA: 0x0012BA40 File Offset: 0x00129E40
		public bool UpdateOpen()
		{
			if (!this.m_IsOpen)
			{
				return false;
			}
			if (this.m_IsAvailable)
			{
				return true;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				return false;
			}
			this.m_ObjList = new List<TownBuildList.TownObj>();
			if (this.m_Category != TownBuildList.eCategory.Have)
			{
				this.m_ShopListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownShopListDB;
				for (int i = 0; i < this.m_ShopListDB.m_Params.Length; i++)
				{
					TownBuildList.TownObj townObj = new TownBuildList.TownObj();
					int id = this.m_ShopListDB.m_Params[i].m_ID;
					TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(id);
					bool flag = false;
					TownBuildList.eCategory category = this.m_Category;
					if (category != TownBuildList.eCategory.Area)
					{
						if (category != TownBuildList.eCategory.Product)
						{
							if (category == TownBuildList.eCategory.Buff)
							{
								if (param.m_Category != 5)
								{
									flag = true;
								}
							}
						}
						else if (param.m_Category != 1 && param.m_Category != 2)
						{
							flag = true;
						}
					}
					else if (param.m_Category != 6)
					{
						flag = true;
					}
					if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildMoney && (param.m_Category != 2 || (int)param.m_Tutorial == 0))
					{
						flag = true;
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTownTutorialStep() == TutorialManager.eTownTutorialStep.BuildWeapon && (param.m_Category != 1 || (int)param.m_Tutorial == 0))
					{
						flag = true;
					}
					if (!flag)
					{
						townObj.objID = this.m_ShopListDB.m_Params[i].m_ID;
						if (this.m_Category != TownBuildList.eCategory.Area)
						{
							townObj.mngID = -1L;
						}
						else
						{
							townObj.mngID = UserTownUtil.GetStoreTownObj(townObj.objID);
						}
						townObj.cost = TownUtility.GetTownObjectLevelUpConfirm(townObj.objID, 1, TownDefine.eTownLevelUpConditionCategory.Gold);
						townObj.costKP = TownUtility.GetTownObjectLevelUpConfirm(townObj.objID, 1, TownDefine.eTownLevelUpConditionCategory.Kirara);
						if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
						{
							townObj.cost = 0;
							townObj.costKP = 0;
						}
						townObj.lv = 1;
						townObj.buildNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetStoreDataNum(townObj.objID);
						this.m_ObjList.Add(townObj);
					}
				}
			}
			else
			{
				UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
				List<UserTownObjectData> userTownObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
				for (int j = 0; j < userTownObjDatas.Count; j++)
				{
					UserTownObjectData userTownObjectData = userTownObjDatas[j];
					int objID = userTownObjDatas[j].ObjID;
					TownObjectListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
					if (param2.m_Category != 4 && param2.m_Category != 6)
					{
						bool flag2 = false;
						for (int k = 0; k < userTownData.GetBuildObjectDataNum(); k++)
						{
							if (userTownData.GetBuildObjectDataAt(k).m_ManageID == userTownObjectData.m_ManageID)
							{
								flag2 = true;
								break;
							}
						}
						if (!flag2)
						{
							TownBuildList.TownObj townObj2 = new TownBuildList.TownObj();
							townObj2.objID = objID;
							townObj2.mngID = userTownObjectData.m_ManageID;
							townObj2.cost = 0;
							townObj2.costKP = 0;
							townObj2.lv = userTownObjectData.Lv;
							townObj2.buildNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetStoreDataNum(townObj2.objID);
							this.m_ObjList.Add(townObj2);
						}
					}
				}
			}
			this.m_ShopListDB = null;
			this.m_IsAvailable = true;
			return true;
		}

		// Token: 0x06003ABD RID: 15037 RVA: 0x0012BE09 File Offset: 0x0012A209
		public TownBuildList.TownObj GetItem(int idx)
		{
			if (this.m_IsAvailable && this.m_ObjList.Count > idx)
			{
				return this.m_ObjList[idx];
			}
			return null;
		}

		// Token: 0x06003ABE RID: 15038 RVA: 0x0012BE35 File Offset: 0x0012A235
		public int GetItemNum()
		{
			if (this.m_IsAvailable)
			{
				return this.m_ObjList.Count;
			}
			return 0;
		}

		// Token: 0x04004232 RID: 16946
		private bool m_IsAvailable;

		// Token: 0x04004233 RID: 16947
		private bool m_IsOpen;

		// Token: 0x04004234 RID: 16948
		private List<TownBuildList.TownObj> m_ObjList;

		// Token: 0x04004235 RID: 16949
		private TownShopListDB m_ShopListDB;

		// Token: 0x04004236 RID: 16950
		private TownBuildList.eCategory m_Category;

		// Token: 0x02000AF6 RID: 2806
		public class TownObj
		{
			// Token: 0x04004237 RID: 16951
			public int objID;

			// Token: 0x04004238 RID: 16952
			public long mngID;

			// Token: 0x04004239 RID: 16953
			public int cost;

			// Token: 0x0400423A RID: 16954
			public int costKP;

			// Token: 0x0400423B RID: 16955
			public int lv;

			// Token: 0x0400423C RID: 16956
			public int buildNum;
		}

		// Token: 0x02000AF7 RID: 2807
		public enum eCategory
		{
			// Token: 0x0400423E RID: 16958
			Area,
			// Token: 0x0400423F RID: 16959
			Product,
			// Token: 0x04004240 RID: 16960
			Buff,
			// Token: 0x04004241 RID: 16961
			Have
		}
	}
}
