﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AFB RID: 2811
	public class TownCharaTweet : MonoBehaviour
	{
		// Token: 0x06003ACC RID: 15052 RVA: 0x0012C1C9 File Offset: 0x0012A5C9
		public void Open(string text)
		{
			this.m_Text.text = text;
			base.gameObject.SetActive(true);
		}

		// Token: 0x06003ACD RID: 15053 RVA: 0x0012C1E3 File Offset: 0x0012A5E3
		public void Close()
		{
			base.gameObject.SetActive(false);
		}

		// Token: 0x0400425A RID: 16986
		[SerializeField]
		private Text m_Text;

		// Token: 0x0400425B RID: 16987
		private RectTransform m_Transform;
	}
}
