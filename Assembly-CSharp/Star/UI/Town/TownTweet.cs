﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B0A RID: 2826
	public class TownTweet : UIGroup
	{
		// Token: 0x06003B0E RID: 15118 RVA: 0x0012DFC5 File Offset: 0x0012C3C5
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_IconRectTransform = this.m_IconGameObject.GetComponent<RectTransform>();
			this.m_IconGameObject.SetActive(false);
		}

		// Token: 0x06003B0F RID: 15119 RVA: 0x0012DFF0 File Offset: 0x0012C3F0
		public void SetStartPosition(Vector2 pos, float size)
		{
			this.m_StartPosition = pos;
			this.m_StartScale = Vector2.one * (size / this.m_IconRectTransform.sizeDelta.y);
			this.m_IconRectTransform.localPosition = pos;
			this.m_IconRectTransform.localScale = this.m_StartScale;
		}

		// Token: 0x06003B10 RID: 15120 RVA: 0x0012E050 File Offset: 0x0012C450
		public override void Update()
		{
			base.Update();
			this.UpdatePrepare();
		}

		// Token: 0x06003B11 RID: 15121 RVA: 0x0012E05E File Offset: 0x0012C45E
		private void UpdatePrepare()
		{
			if (!this.m_IsDonePrepare)
			{
				this.m_IsDonePrepare = true;
				this.OpenLoaded();
			}
		}

		// Token: 0x06003B12 RID: 15122 RVA: 0x0012E078 File Offset: 0x0012C478
		public void Open(int charaID, int tweetID)
		{
			this.m_IsDonePrepare = false;
			eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
			this.m_TweetListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(namedType);
			this.m_CharaID = charaID;
			this.m_TweetID = tweetID;
			this.m_CharaIcon.Apply(this.m_CharaID);
			base.Open();
		}

		// Token: 0x06003B13 RID: 15123 RVA: 0x0012E0E8 File Offset: 0x0012C4E8
		private string CalcTweetText(int charaID, int tweetID)
		{
			if (this.m_TweetListDB != null)
			{
				int num = UnityEngine.Random.Range(0, 2);
				return this.m_TweetListDB.GetText(tweetID + num);
			}
			return string.Empty;
		}

		// Token: 0x06003B14 RID: 15124 RVA: 0x0012E124 File Offset: 0x0012C524
		private void OpenLoaded()
		{
			this.m_IconGameObject.SetActive(true);
			this.m_TweetTextObj.text = this.CalcTweetText(this.m_CharaID, this.m_TweetID);
			base.Open();
			iTween.Stop(this.m_IconGameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", this.m_IconPlace.localPosition.x);
			hashtable.Add("y", this.m_IconPlace.localPosition.y);
			hashtable.Add("time", this.m_MoveTimeSec);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("oncomplete", "OnCompleteInMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_IconGameObject, hashtable);
			iTween.Stop(this.m_IconGameObject, "scale");
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("isLocal", true);
			hashtable2.Add("x", this.m_IconPlace.localScale.x);
			hashtable2.Add("y", this.m_IconPlace.localScale.y);
			hashtable2.Add("time", this.m_MoveTimeSec);
			hashtable2.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable2.Add("delay", 0.001f);
			iTween.ScaleTo(this.m_IconGameObject, hashtable2);
			this.m_IsMoving = true;
		}

		// Token: 0x06003B15 RID: 15125 RVA: 0x0012E2F4 File Offset: 0x0012C6F4
		public void OnCompleteInMove()
		{
			this.m_IsMoving = false;
			this.m_Button.SetActive(true);
		}

		// Token: 0x06003B16 RID: 15126 RVA: 0x0012E30C File Offset: 0x0012C70C
		public override void Close()
		{
			this.m_IconGameObject.SetActive(true);
			base.Close();
			iTween.Stop(this.m_IconGameObject, "move");
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", this.m_StartPosition.x);
			hashtable.Add("y", this.m_StartPosition.y);
			hashtable.Add("time", this.m_MoveTimeSec);
			hashtable.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable.Add("delay", 0.001f);
			hashtable.Add("oncomplete", "OnCompleteOutMove");
			hashtable.Add("oncompletetarget", this.m_GameObject);
			iTween.MoveTo(this.m_IconGameObject, hashtable);
			iTween.Stop(this.m_IconGameObject, "scale");
			Hashtable hashtable2 = new Hashtable();
			hashtable2.Add("isLocal", true);
			hashtable2.Add("x", this.m_StartScale.x);
			hashtable2.Add("y", this.m_StartScale.y);
			hashtable2.Add("time", this.m_MoveTimeSec);
			hashtable2.Add("easeType", iTween.EaseType.easeOutQuad);
			hashtable2.Add("delay", 0.001f);
			iTween.ScaleTo(this.m_IconGameObject, hashtable2);
			this.m_IsMoving = true;
		}

		// Token: 0x06003B17 RID: 15127 RVA: 0x0012E49D File Offset: 0x0012C89D
		public override void OnFinishPlayOut()
		{
			base.OnFinishPlayOut();
			this.Destroy();
		}

		// Token: 0x06003B18 RID: 15128 RVA: 0x0012E4AB File Offset: 0x0012C8AB
		public void Destroy()
		{
			this.m_TweetListDB = null;
			this.m_CharaIcon.Destroy();
		}

		// Token: 0x06003B19 RID: 15129 RVA: 0x0012E4BF File Offset: 0x0012C8BF
		public void OnCompleteOutMove()
		{
			this.m_IconGameObject.SetActive(false);
			this.m_IsMoving = false;
			this.m_Button.SetActive(false);
		}

		// Token: 0x06003B1A RID: 15130 RVA: 0x0012E4E0 File Offset: 0x0012C8E0
		public void OnCancelButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003B1B RID: 15131 RVA: 0x0012E4E8 File Offset: 0x0012C8E8
		protected override bool CheckOutEnd()
		{
			return base.CheckOutEnd() && !this.m_IsMoving;
		}

		// Token: 0x040042C7 RID: 17095
		[SerializeField]
		private GameObject m_IconGameObject;

		// Token: 0x040042C8 RID: 17096
		[SerializeField]
		private CharaIcon m_CharaIcon;

		// Token: 0x040042C9 RID: 17097
		[SerializeField]
		private Text m_TweetTextObj;

		// Token: 0x040042CA RID: 17098
		[SerializeField]
		private RectTransform m_IconPlace;

		// Token: 0x040042CB RID: 17099
		[SerializeField]
		private float m_MoveTimeSec;

		// Token: 0x040042CC RID: 17100
		[SerializeField]
		private GameObject m_Button;

		// Token: 0x040042CD RID: 17101
		private bool m_IsDonePrepare;

		// Token: 0x040042CE RID: 17102
		private TweetListDB m_TweetListDB;

		// Token: 0x040042CF RID: 17103
		private bool m_IsMoving;

		// Token: 0x040042D0 RID: 17104
		private Vector2 m_StartPosition;

		// Token: 0x040042D1 RID: 17105
		private Vector2 m_StartScale;

		// Token: 0x040042D2 RID: 17106
		private int m_CharaID;

		// Token: 0x040042D3 RID: 17107
		private int m_TweetID;

		// Token: 0x040042D4 RID: 17108
		private RectTransform m_IconRectTransform;

		// Token: 0x040042D5 RID: 17109
		private GameObject m_GameObject;
	}
}
