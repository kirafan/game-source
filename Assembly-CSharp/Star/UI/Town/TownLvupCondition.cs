﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AFC RID: 2812
	public class TownLvupCondition : MonoBehaviour
	{
		// Token: 0x06003ACF RID: 15055 RVA: 0x0012C1FC File Offset: 0x0012A5FC
		public bool Apply(int objID, int afterLv)
		{
			int townObjectLevelUpConfirm = TownUtility.GetTownObjectLevelUpConfirm(objID, afterLv, TownDefine.eTownLevelUpConditionCategory.Gold);
			int townObjectLevelUpConfirm2 = TownUtility.GetTownObjectLevelUpConfirm(objID, afterLv, TownDefine.eTownLevelUpConditionCategory.Kirara);
			int sec = (int)UserTownUtil.GetBuildUpTime(objID, afterLv - 1) / 1000;
			int townObjectLevelUpConfirm3 = TownUtility.GetTownObjectLevelUpConfirm(objID, afterLv, TownDefine.eTownLevelUpConditionCategory.UserLv);
			if (townObjectLevelUpConfirm3 > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildLevelUpConditionUserLv, new object[]
				{
					townObjectLevelUpConfirm3
				}));
				if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Lv < townObjectLevelUpConfirm3)
				{
					UIUtility.GetNotEnoughString(stringBuilder);
				}
				this.m_ConditionText.text = stringBuilder.ToString();
			}
			else
			{
				this.m_ConditionText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildLevelUpConditionNone);
			}
			this.m_CostTimeText.text = UIUtility.GetBuildTimeString(sec);
			this.m_CostGoldText.text = UIUtility.GetNeedGoldString(townObjectLevelUpConfirm);
			this.m_HaveGoldText.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			this.m_CostKPText.text = UIUtility.GetNeedKPString(townObjectLevelUpConfirm2);
			this.m_HaveKPText.text = UIUtility.KPValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint(), true);
			return !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)townObjectLevelUpConfirm) && !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfKP((long)townObjectLevelUpConfirm2) && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Lv >= townObjectLevelUpConfirm3;
		}

		// Token: 0x0400425C RID: 16988
		[SerializeField]
		private Text m_ConditionText;

		// Token: 0x0400425D RID: 16989
		[SerializeField]
		private Text m_CostTimeText;

		// Token: 0x0400425E RID: 16990
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x0400425F RID: 16991
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x04004260 RID: 16992
		[SerializeField]
		private Text m_CostKPText;

		// Token: 0x04004261 RID: 16993
		[SerializeField]
		private Text m_HaveKPText;
	}
}
