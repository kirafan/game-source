﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AE4 RID: 2788
	public class BuildBufPanel : MonoBehaviour
	{
		// Token: 0x06003A6E RID: 14958 RVA: 0x0012A502 File Offset: 0x00128902
		public void SetActive(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x06003A6F RID: 14959 RVA: 0x0012A510 File Offset: 0x00128910
		public void Apply(string type, float value)
		{
			StringBuilder stringBuilder = new StringBuilder();
			this.m_TypeTextObj.text = type;
			stringBuilder.Append("+");
			stringBuilder.Append((int)value);
			stringBuilder.Append("%");
			this.m_ValueTextObj.text = stringBuilder.ToString();
		}

		// Token: 0x040041E0 RID: 16864
		[SerializeField]
		private Text m_TypeTextObj;

		// Token: 0x040041E1 RID: 16865
		[SerializeField]
		private Text m_ValueTextObj;
	}
}
