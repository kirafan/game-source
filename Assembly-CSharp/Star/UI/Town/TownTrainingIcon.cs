﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B09 RID: 2825
	public class TownTrainingIcon : MonoBehaviour
	{
		// Token: 0x06003B0B RID: 15115 RVA: 0x0012DF5C File Offset: 0x0012C35C
		private void Start()
		{
			this.m_TownMain = UnityEngine.Object.FindObjectOfType<TownMain>();
			this.m_CharaMngID = -1L;
			if (this.m_CharaMngID != -1L)
			{
				this.m_IconImage.gameObject.SetActive(true);
			}
			else
			{
				this.m_IconImage.gameObject.SetActive(false);
			}
		}

		// Token: 0x06003B0C RID: 15116 RVA: 0x0012DFB0 File Offset: 0x0012C3B0
		public void OnClickIconCallBack()
		{
			this.m_TownMain.OnClickTrainingIcon();
		}

		// Token: 0x040042C4 RID: 17092
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040042C5 RID: 17093
		private TownMain m_TownMain;

		// Token: 0x040042C6 RID: 17094
		private long m_CharaMngID;
	}
}
