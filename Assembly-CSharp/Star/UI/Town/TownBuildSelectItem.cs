﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AF8 RID: 2808
	public class TownBuildSelectItem : ScrollItemIcon
	{
		// Token: 0x06003AC1 RID: 15041 RVA: 0x0012BE5F File Offset: 0x0012A25F
		public void Apply()
		{
			this.ApplyData();
		}

		// Token: 0x06003AC2 RID: 15042 RVA: 0x0012BE68 File Offset: 0x0012A268
		protected override void ApplyData()
		{
			TownBuildSelectItemData townBuildSelectItemData = (TownBuildSelectItemData)this.m_Data;
			int objID = townBuildSelectItemData.m_ObjID;
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_NameTextObj.text = param.m_ObjName;
			int placeNum = townBuildSelectItemData.m_PlaceNum;
			int maxNum = (int)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_MaxNum;
			this.m_HaveNumTextObj.text = placeNum.ToString() + "/" + maxNum.ToString();
			if (townBuildSelectItemData.m_IsStore)
			{
				this.m_BuildObj.SetActive(false);
				this.m_StoreObj.SetActive(true);
				this.m_Icon.Apply(objID, UserTownUtil.GetTownObjData(townBuildSelectItemData.m_ObjMngID).Lv);
			}
			else
			{
				this.m_BuildObj.SetActive(true);
				this.m_StoreObj.SetActive(false);
				this.m_Icon.Apply(objID, 1);
				if (!this.m_CostGoldObj.activeSelf)
				{
					this.m_CostGoldObj.SetActive(true);
				}
				this.m_CostKPObj.SetActive(townBuildSelectItemData.m_CostKP > 0);
				this.m_CostGoldTextObj.text = UIUtility.GetNeedGoldString(townBuildSelectItemData.m_Cost);
				this.m_CostKPTextObj.text = UIUtility.GetNeedKPString(townBuildSelectItemData.m_CostKP);
				if (placeNum >= maxNum)
				{
					this.m_BuildButton.Interactable = false;
					this.m_HaveNumTextObj.text = "<color=red>" + placeNum.ToString() + "</color>/" + maxNum.ToString();
				}
				else if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)townBuildSelectItemData.m_Cost))
				{
					this.m_BuildButton.Interactable = false;
				}
				else
				{
					this.m_BuildButton.Interactable = true;
				}
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.SetEnableDark(true, 1f, true);
				this.m_BuildButton.Interactable = true;
				this.m_TutorialTarget.SetEnable(true);
				tutorialMessage.SetEnableArrow(true);
				tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				tutorialMessage.SetArrowPosition(this.m_TutorialTarget.GetComponent<RectTransform>().position);
			}
		}

		// Token: 0x06003AC3 RID: 15043 RVA: 0x0012C0C4 File Offset: 0x0012A4C4
		public void OnClickBuildCallBack()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				this.m_TutorialTarget.SetEnable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
			}
			TownBuildSelectItemData townBuildSelectItemData = (TownBuildSelectItemData)this.m_Data;
			townBuildSelectItemData.OnClickBuild.Call(townBuildSelectItemData.m_ObjID);
		}

		// Token: 0x06003AC4 RID: 15044 RVA: 0x0012C120 File Offset: 0x0012A520
		public void OnClickPlacementCallBack()
		{
			TownBuildSelectItemData townBuildSelectItemData = (TownBuildSelectItemData)this.m_Data;
			townBuildSelectItemData.OnClickPlacement.Call(townBuildSelectItemData.m_ObjMngID);
		}

		// Token: 0x06003AC5 RID: 15045 RVA: 0x0012C14C File Offset: 0x0012A54C
		public void OnClickSellCallBack()
		{
			TownBuildSelectItemData townBuildSelectItemData = (TownBuildSelectItemData)this.m_Data;
			townBuildSelectItemData.OnClickSell.Call(townBuildSelectItemData.m_ObjMngID);
		}

		// Token: 0x04004242 RID: 16962
		[SerializeField]
		private TownObjectIcon m_Icon;

		// Token: 0x04004243 RID: 16963
		[SerializeField]
		private Text m_HaveNumTextObj;

		// Token: 0x04004244 RID: 16964
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04004245 RID: 16965
		[SerializeField]
		private GameObject m_BuildObj;

		// Token: 0x04004246 RID: 16966
		[SerializeField]
		private CustomButton m_BuildButton;

		// Token: 0x04004247 RID: 16967
		[SerializeField]
		private GameObject m_CostGoldObj;

		// Token: 0x04004248 RID: 16968
		[SerializeField]
		private Text m_CostGoldTextObj;

		// Token: 0x04004249 RID: 16969
		[SerializeField]
		private GameObject m_CostKPObj;

		// Token: 0x0400424A RID: 16970
		[SerializeField]
		private Text m_CostKPTextObj;

		// Token: 0x0400424B RID: 16971
		[SerializeField]
		private GameObject m_StoreObj;

		// Token: 0x0400424C RID: 16972
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x0400424D RID: 16973
		[SerializeField]
		private CustomButton m_SellButton;

		// Token: 0x0400424E RID: 16974
		[SerializeField]
		private TutorialTarget m_TutorialTarget;
	}
}
