﻿using System;

namespace Star.UI.Town
{
	// Token: 0x02000AF9 RID: 2809
	public class TownBuildSelectItemData : ScrollItemData
	{
		// Token: 0x06003AC6 RID: 15046 RVA: 0x0012C176 File Offset: 0x0012A576
		public TownBuildSelectItemData()
		{
			this.m_ObjID = -1;
		}

		// Token: 0x0400424F RID: 16975
		public int m_ObjID;

		// Token: 0x04004250 RID: 16976
		public long m_ObjMngID;

		// Token: 0x04004251 RID: 16977
		public bool m_IsStore;

		// Token: 0x04004252 RID: 16978
		public int m_PlaceNum;

		// Token: 0x04004253 RID: 16979
		public int m_Cost;

		// Token: 0x04004254 RID: 16980
		public int m_CostKP;

		// Token: 0x04004255 RID: 16981
		public Action<int> OnClickBuild;

		// Token: 0x04004256 RID: 16982
		public Action<long> OnClickPlacement;

		// Token: 0x04004257 RID: 16983
		public Action<long> OnClickSell;
	}
}
