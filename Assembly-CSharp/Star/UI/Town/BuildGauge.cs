﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AE5 RID: 2789
	public class BuildGauge : MonoBehaviour
	{
		// Token: 0x06003A71 RID: 14961 RVA: 0x0012A569 File Offset: 0x00128969
		private void Start()
		{
			if (this.m_CompleteImage != null)
			{
				this.m_CompleteImage.gameObject.SetActive(false);
			}
		}

		// Token: 0x06003A72 RID: 14962 RVA: 0x0012A590 File Offset: 0x00128990
		private void Update()
		{
			if (this.m_CompleteImage != null && this.m_IsDirty)
			{
				if (this.m_Value >= 1f)
				{
					this.m_CompleteImage.gameObject.SetActive(true);
				}
				else
				{
					this.m_CompleteImage.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x06003A73 RID: 14963 RVA: 0x0012A5F0 File Offset: 0x001289F0
		public void Apply(float value)
		{
			this.m_Value = value;
			if (this.m_Gauge)
			{
				this.m_Gauge.fillAmount = this.m_Value;
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06003A74 RID: 14964 RVA: 0x0012A621 File Offset: 0x00128A21
		public void SetActive(bool flg)
		{
			base.gameObject.SetActive(flg);
		}

		// Token: 0x040041E2 RID: 16866
		[SerializeField]
		private Image m_Gauge;

		// Token: 0x040041E3 RID: 16867
		[SerializeField]
		private Image m_CompleteImage;

		// Token: 0x040041E4 RID: 16868
		private float m_Value;

		// Token: 0x040041E5 RID: 16869
		private bool m_IsDirty;
	}
}
