﻿using System;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000B07 RID: 2823
	public class TownObjWindowUtil
	{
		// Token: 0x06003B06 RID: 15110 RVA: 0x0012DEF8 File Offset: 0x0012C2F8
		public static void SetAfterTextActive(GameObject ptarget, bool factive)
		{
			Transform transform = TownUtility.FindTransform(ptarget.transform, "TargetText", 4);
			if (transform != null)
			{
				transform.gameObject.SetActive(factive);
			}
		}
	}
}
