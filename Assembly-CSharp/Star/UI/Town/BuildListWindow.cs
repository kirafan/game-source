﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AE6 RID: 2790
	public class BuildListWindow : UIGroup
	{
		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06003A76 RID: 14966 RVA: 0x0012A658 File Offset: 0x00128A58
		public bool IsHaveList
		{
			get
			{
				return this.m_Category == TownBuildList.eCategory.Have;
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06003A77 RID: 14967 RVA: 0x0012A663 File Offset: 0x00128A63
		public BuildListWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06003A78 RID: 14968 RVA: 0x0012A66B File Offset: 0x00128A6B
		public int SelectObjID
		{
			get
			{
				return this.m_SelectObjID;
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06003A79 RID: 14969 RVA: 0x0012A673 File Offset: 0x00128A73
		public long SelectObjMngID
		{
			get
			{
				return this.m_SelectObjMngID;
			}
		}

		// Token: 0x06003A7A RID: 14970 RVA: 0x0012A67B File Offset: 0x00128A7B
		public void Setup()
		{
			if (this.m_Scroll != null)
			{
				this.m_Scroll.Setup();
			}
		}

		// Token: 0x06003A7B RID: 14971 RVA: 0x0012A699 File Offset: 0x00128A99
		public override void Open()
		{
			this.Open(this.m_Category);
		}

		// Token: 0x06003A7C RID: 14972 RVA: 0x0012A6A8 File Offset: 0x00128AA8
		public void Open(TownBuildList.eCategory category)
		{
			this.m_Category = category;
			this.m_TownBuildList.Open(category);
			base.gameObject.SetActive(true);
			if (this.m_TitleTextObj != null)
			{
				switch (this.m_Category)
				{
				case TownBuildList.eCategory.Product:
					this.m_TitleTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuProductTitle);
					this.m_HeaderText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuProductMessage);
					break;
				case TownBuildList.eCategory.Buff:
					this.m_TitleTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuBuffTitle);
					this.m_HeaderText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuBuffMessage);
					break;
				case TownBuildList.eCategory.Have:
					this.m_TitleTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuWarehouseTitle);
					this.m_HeaderText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownMenuWarehouseMessage);
					break;
				}
			}
			if (this.m_Category == TownBuildList.eCategory.Have)
			{
				this.m_StockNum.gameObject.SetActive(true);
				this.m_StockNum.SetValue(UserTownUtil.GetTownObjStockNum() + "/" + SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimit);
			}
			else
			{
				this.m_StockNum.gameObject.SetActive(false);
			}
			this.PrepareStart();
			this.m_IsDonePrepare = false;
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				this.m_Scroll.GetComponent<ScrollRect>().vertical = false;
			}
			else
			{
				this.m_Scroll.GetComponent<ScrollRect>().vertical = true;
			}
		}

		// Token: 0x06003A7D RID: 14973 RVA: 0x0012A888 File Offset: 0x00128C88
		public override void Update()
		{
			base.Update();
			if (!this.m_IsDonePrepare)
			{
				if (this.m_TownBuildList.IsAvailable)
				{
					this.Apply();
					this.m_IsDonePrepare = true;
					base.Open();
				}
				else
				{
					this.m_TownBuildList.UpdateOpen();
				}
			}
		}

		// Token: 0x06003A7E RID: 14974 RVA: 0x0012A8DA File Offset: 0x00128CDA
		public void OnClickBuildCallBack(int objID)
		{
			this.m_SelectButton = BuildListWindow.eButton.Build;
			this.m_SelectObjID = objID;
			this.m_SelectObjMngID = -1L;
			this.Close();
		}

		// Token: 0x06003A7F RID: 14975 RVA: 0x0012A8F8 File Offset: 0x00128CF8
		public void OnClickPlacementCallBack(long objMngID)
		{
			this.m_SelectButton = BuildListWindow.eButton.Placement;
			this.m_SelectObjID = UserTownUtil.GetTownObjData(objMngID).ObjID;
			this.m_SelectObjMngID = objMngID;
			this.Close();
		}

		// Token: 0x06003A80 RID: 14976 RVA: 0x0012A91F File Offset: 0x00128D1F
		public void OnClickSellCallBack(long objMngID)
		{
			this.m_SelectButton = BuildListWindow.eButton.Sell;
			this.m_SelectObjID = -1;
			this.m_SelectObjMngID = objMngID;
			this.Close();
		}

		// Token: 0x06003A81 RID: 14977 RVA: 0x0012A93C File Offset: 0x00128D3C
		public void OnBackButtonCallBack()
		{
			this.m_SelectButton = BuildListWindow.eButton.Back;
			this.m_SelectObjID = -1;
			this.m_SelectObjMngID = -1L;
			this.Close();
		}

		// Token: 0x06003A82 RID: 14978 RVA: 0x0012A95A File Offset: 0x00128D5A
		public void OnCancelButtonCallBack()
		{
			this.m_SelectButton = BuildListWindow.eButton.Cancel;
			this.m_SelectObjID = -1;
			this.m_SelectObjMngID = -1L;
			this.Close();
		}

		// Token: 0x06003A83 RID: 14979 RVA: 0x0012A978 File Offset: 0x00128D78
		private void Apply()
		{
			if (this.m_Scroll != null)
			{
				this.m_Scroll.RemoveAll();
				for (int i = 0; i < this.m_TownBuildList.GetItemNum(); i++)
				{
					TownBuildSelectItemData townBuildSelectItemData = new TownBuildSelectItemData();
					townBuildSelectItemData.m_ObjID = this.m_TownBuildList.GetItem(i).objID;
					townBuildSelectItemData.m_ObjMngID = this.m_TownBuildList.GetItem(i).mngID;
					townBuildSelectItemData.m_PlaceNum = this.m_TownBuildList.GetItem(i).buildNum;
					townBuildSelectItemData.m_Cost = this.m_TownBuildList.GetItem(i).cost;
					townBuildSelectItemData.m_CostKP = this.m_TownBuildList.GetItem(i).costKP;
					townBuildSelectItemData.m_IsStore = (this.m_Category == TownBuildList.eCategory.Have);
					TownBuildSelectItemData townBuildSelectItemData2 = townBuildSelectItemData;
					townBuildSelectItemData2.OnClickBuild = (Action<int>)Delegate.Combine(townBuildSelectItemData2.OnClickBuild, new Action<int>(this.OnClickBuildCallBack));
					TownBuildSelectItemData townBuildSelectItemData3 = townBuildSelectItemData;
					townBuildSelectItemData3.OnClickPlacement = (Action<long>)Delegate.Combine(townBuildSelectItemData3.OnClickPlacement, new Action<long>(this.OnClickPlacementCallBack));
					TownBuildSelectItemData townBuildSelectItemData4 = townBuildSelectItemData;
					townBuildSelectItemData4.OnClickSell = (Action<long>)Delegate.Combine(townBuildSelectItemData4.OnClickSell, new Action<long>(this.OnClickSellCallBack));
					this.m_Scroll.AddItem(townBuildSelectItemData);
				}
			}
		}

		// Token: 0x040041E6 RID: 16870
		private BuildListWindow.eButton m_SelectButton = BuildListWindow.eButton.Cancel;

		// Token: 0x040041E7 RID: 16871
		private int m_SelectObjID = -1;

		// Token: 0x040041E8 RID: 16872
		private long m_SelectObjMngID = -1L;

		// Token: 0x040041E9 RID: 16873
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x040041EA RID: 16874
		[SerializeField]
		private Text m_TitleTextObj;

		// Token: 0x040041EB RID: 16875
		[SerializeField]
		private Text m_HeaderText;

		// Token: 0x040041EC RID: 16876
		[SerializeField]
		private TextField m_StockNum;

		// Token: 0x040041ED RID: 16877
		private TownBuildList.eCategory m_Category;

		// Token: 0x040041EE RID: 16878
		private TownBuildList m_TownBuildList = new TownBuildList();

		// Token: 0x040041EF RID: 16879
		private bool m_IsDonePrepare;

		// Token: 0x02000AE7 RID: 2791
		public enum eButton
		{
			// Token: 0x040041F1 RID: 16881
			Build,
			// Token: 0x040041F2 RID: 16882
			Placement,
			// Token: 0x040041F3 RID: 16883
			Sell,
			// Token: 0x040041F4 RID: 16884
			Cancel,
			// Token: 0x040041F5 RID: 16885
			Back
		}
	}
}
