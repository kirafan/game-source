﻿using System;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000B06 RID: 2822
	public class TownObjStateWindow : UIGroup
	{
		// Token: 0x06003B00 RID: 15104 RVA: 0x0012DE8F File Offset: 0x0012C28F
		public void Open(long mngID)
		{
			this.m_ObjMngID = mngID;
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Apply();
			}
			base.Open();
		}

		// Token: 0x06003B01 RID: 15105 RVA: 0x0012DEC3 File Offset: 0x0012C2C3
		public void OnClickCloseButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003B02 RID: 15106 RVA: 0x0012DECB File Offset: 0x0012C2CB
		public void OnBackButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003B03 RID: 15107 RVA: 0x0012DED3 File Offset: 0x0012C2D3
		public void OnCancelButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003B04 RID: 15108 RVA: 0x0012DEDB File Offset: 0x0012C2DB
		private void Apply()
		{
			this.m_ObjInfo.Apply(this.m_ObjMngID);
		}

		// Token: 0x040042C1 RID: 17089
		private long m_ObjMngID;

		// Token: 0x040042C2 RID: 17090
		[SerializeField]
		private TownObjInfo m_ObjInfo;
	}
}
