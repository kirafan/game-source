﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B0B RID: 2827
	public class TownTweetSpecial : UIGroup
	{
		// Token: 0x06003B1D RID: 15133 RVA: 0x0012E509 File Offset: 0x0012C909
		public void Setup()
		{
			this.m_BustAnim = this.m_BustImage.GetComponent<AnimUIPlayer>();
		}

		// Token: 0x06003B1E RID: 15134 RVA: 0x0012E51C File Offset: 0x0012C91C
		public override void Update()
		{
			base.Update();
			if (!this.m_IsDonePrepare)
			{
				this.UpdatePrepare();
			}
			else if (base.IsDonePlayIn)
			{
				if (this.m_BustAnim.State == 0 && this.m_BustImage.IsDoneLoad())
				{
					this.m_BustAnim.PlayIn();
					this.m_TweetWindowAnim.PlayIn();
				}
				if (this.m_BGAnim.State == 0)
				{
					this.m_BGAnim.PlayIn();
				}
			}
		}

		// Token: 0x06003B1F RID: 15135 RVA: 0x0012E5A1 File Offset: 0x0012C9A1
		private void UpdatePrepare()
		{
			this.m_IsDonePrepare = true;
			this.OpenLoaded();
		}

		// Token: 0x06003B20 RID: 15136 RVA: 0x0012E5B0 File Offset: 0x0012C9B0
		public void Open(int charaID, int tweetID)
		{
			this.m_IsDonePrepare = false;
			eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
			this.m_TweetListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(namedType);
			this.m_CharaID = charaID;
			this.m_TweetID = tweetID;
			this.m_BustImage.Apply(charaID, BustImage.eBustImageType.Chara);
			base.Open();
		}

		// Token: 0x06003B21 RID: 15137 RVA: 0x0012E619 File Offset: 0x0012CA19
		private string CalcTweetText(int charaID, int tweetID)
		{
			if (this.m_TweetListDB != null)
			{
				return this.m_TweetListDB.GetText(tweetID);
			}
			return string.Empty;
		}

		// Token: 0x06003B22 RID: 15138 RVA: 0x0012E63E File Offset: 0x0012CA3E
		private void OpenLoaded()
		{
			this.m_TweetTextObj.text = this.CalcTweetText(this.m_CharaID, this.m_TweetID);
			base.Open();
			this.Destroy();
		}

		// Token: 0x06003B23 RID: 15139 RVA: 0x0012E669 File Offset: 0x0012CA69
		public override void Close()
		{
			this.m_BustAnim.PlayOut();
			this.m_TweetWindowAnim.PlayOut();
			this.m_BGAnim.PlayOut();
			base.Close();
		}

		// Token: 0x06003B24 RID: 15140 RVA: 0x0012E692 File Offset: 0x0012CA92
		public void Destroy()
		{
			this.m_TweetListDB = null;
		}

		// Token: 0x06003B25 RID: 15141 RVA: 0x0012E69B File Offset: 0x0012CA9B
		protected override bool CheckOutEnd()
		{
			return base.CheckOutEnd() && this.m_BustAnim.State == 0 && this.m_TweetWindowAnim.State == 0 && this.m_BGAnim.State == 0;
		}

		// Token: 0x06003B26 RID: 15142 RVA: 0x0012E6D9 File Offset: 0x0012CAD9
		public void OnCancelButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003B27 RID: 15143 RVA: 0x0012E6E1 File Offset: 0x0012CAE1
		public override void OnFinishPlayOut()
		{
			this.m_BustImage.Destroy();
			base.OnFinishPlayOut();
		}

		// Token: 0x040042D6 RID: 17110
		[SerializeField]
		private Image m_BackGround;

		// Token: 0x040042D7 RID: 17111
		[SerializeField]
		private AnimUIPlayer m_BGAnim;

		// Token: 0x040042D8 RID: 17112
		[SerializeField]
		private BustImage m_BustImage;

		// Token: 0x040042D9 RID: 17113
		[SerializeField]
		private AnimUIPlayer m_TweetWindowAnim;

		// Token: 0x040042DA RID: 17114
		private AnimUIPlayer m_BustAnim;

		// Token: 0x040042DB RID: 17115
		[SerializeField]
		private Text m_TweetTextObj;

		// Token: 0x040042DC RID: 17116
		private bool m_IsDonePrepare;

		// Token: 0x040042DD RID: 17117
		private TweetListDB m_TweetListDB;

		// Token: 0x040042DE RID: 17118
		private int m_CharaID;

		// Token: 0x040042DF RID: 17119
		private int m_TweetID;

		// Token: 0x040042E0 RID: 17120
		private RectTransform m_IconRectTransform;
	}
}
