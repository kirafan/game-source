﻿using System;
using System.Collections.Generic;
using Star.UI.CharaList;
using Star.UI.Schedule;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B0C RID: 2828
	public class TownUI : MenuUIBase
	{
		// Token: 0x17000379 RID: 889
		// (get) Token: 0x06003B29 RID: 15145 RVA: 0x0012E707 File Offset: 0x0012CB07
		public bool IsMain
		{
			get
			{
				return this.m_StateController.NowState == 4;
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06003B2A RID: 15146 RVA: 0x0012E717 File Offset: 0x0012CB17
		public bool IsSelect
		{
			get
			{
				return this.m_StateController.NowState == 5;
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06003B2B RID: 15147 RVA: 0x0012E727 File Offset: 0x0012CB27
		public bool IsActive
		{
			get
			{
				return this.m_IsActive;
			}
		}

		// Token: 0x140000F7 RID: 247
		// (add) Token: 0x06003B2C RID: 15148 RVA: 0x0012E730 File Offset: 0x0012CB30
		// (remove) Token: 0x06003B2D RID: 15149 RVA: 0x0012E768 File Offset: 0x0012CB68
		public event Action<long[]> OnDecideLiveCharas;

		// Token: 0x140000F8 RID: 248
		// (add) Token: 0x06003B2E RID: 15150 RVA: 0x0012E7A0 File Offset: 0x0012CBA0
		// (remove) Token: 0x06003B2F RID: 15151 RVA: 0x0012E7D8 File Offset: 0x0012CBD8
		public event Action OnRoomIn;

		// Token: 0x140000F9 RID: 249
		// (add) Token: 0x06003B30 RID: 15152 RVA: 0x0012E810 File Offset: 0x0012CC10
		// (remove) Token: 0x06003B31 RID: 15153 RVA: 0x0012E848 File Offset: 0x0012CC48
		public event Action OnExecuteBuild;

		// Token: 0x140000FA RID: 250
		// (add) Token: 0x06003B32 RID: 15154 RVA: 0x0012E880 File Offset: 0x0012CC80
		// (remove) Token: 0x06003B33 RID: 15155 RVA: 0x0012E8B8 File Offset: 0x0012CCB8
		public event Action OnExecuteLevelUp;

		// Token: 0x140000FB RID: 251
		// (add) Token: 0x06003B34 RID: 15156 RVA: 0x0012E8F0 File Offset: 0x0012CCF0
		// (remove) Token: 0x06003B35 RID: 15157 RVA: 0x0012E928 File Offset: 0x0012CD28
		public event Action OnExecuteQuick;

		// Token: 0x140000FC RID: 252
		// (add) Token: 0x06003B36 RID: 15158 RVA: 0x0012E960 File Offset: 0x0012CD60
		// (remove) Token: 0x06003B37 RID: 15159 RVA: 0x0012E998 File Offset: 0x0012CD98
		public event Action OnExecuteSell;

		// Token: 0x140000FD RID: 253
		// (add) Token: 0x06003B38 RID: 15160 RVA: 0x0012E9D0 File Offset: 0x0012CDD0
		// (remove) Token: 0x06003B39 RID: 15161 RVA: 0x0012EA08 File Offset: 0x0012CE08
		public event Action OnExecuteStore;

		// Token: 0x140000FE RID: 254
		// (add) Token: 0x06003B3A RID: 15162 RVA: 0x0012EA40 File Offset: 0x0012CE40
		// (remove) Token: 0x06003B3B RID: 15163 RVA: 0x0012EA78 File Offset: 0x0012CE78
		public event Action OnClickBuildPointCancelButton;

		// Token: 0x140000FF RID: 255
		// (add) Token: 0x06003B3C RID: 15164 RVA: 0x0012EAB0 File Offset: 0x0012CEB0
		// (remove) Token: 0x06003B3D RID: 15165 RVA: 0x0012EAE8 File Offset: 0x0012CEE8
		public event Action OnClickMovePointCancelButton;

		// Token: 0x14000100 RID: 256
		// (add) Token: 0x06003B3E RID: 15166 RVA: 0x0012EB20 File Offset: 0x0012CF20
		// (remove) Token: 0x06003B3F RID: 15167 RVA: 0x0012EB58 File Offset: 0x0012CF58
		public event Action OnClosedScheduleWindow;

		// Token: 0x14000101 RID: 257
		// (add) Token: 0x06003B40 RID: 15168 RVA: 0x0012EB90 File Offset: 0x0012CF90
		// (remove) Token: 0x06003B41 RID: 15169 RVA: 0x0012EBC8 File Offset: 0x0012CFC8
		public event Action OnClosedTweetWindow;

		// Token: 0x14000102 RID: 258
		// (add) Token: 0x06003B42 RID: 15170 RVA: 0x0012EC00 File Offset: 0x0012D000
		// (remove) Token: 0x06003B43 RID: 15171 RVA: 0x0012EC38 File Offset: 0x0012D038
		public event Action OnEnd;

		// Token: 0x06003B44 RID: 15172 RVA: 0x0012EC70 File Offset: 0x0012D070
		public void Setup(TownMain owner)
		{
			this.m_Owner = owner;
			this.m_BuildTopWindow.OnClose += this.OnCloseBuildTopWindowCallBack;
			this.m_BuildListWindow.Setup();
			this.m_BuildListWindow.OnClose += this.OnCloseBuildSelectWindowCallBack;
			this.m_ContentAreaWindow.Setup();
			this.m_ContentAreaWindow.OnClose += this.OnCloseContentAreaWindowCallBack;
			this.m_TownBufWindow.OnClose += this.OnCloseTownBufWindowCallBack;
			this.m_ScheduleWindow.Setup();
			this.m_ScheduleWindow.OnChangeChara += this.OnDecideLiveMember;
			this.m_ScheduleWindow.OnClose += this.OnCloseScheduleCallBack;
			this.m_ConfirmWindow.OnClose += this.OnCloseConfirmWindowCallBack;
			this.m_StateController.Setup(9, 0, true);
			this.m_StateController.AddOnExit(4, new Action(this.CloseMainGroup));
			this.m_StateController.AddOnExit(5, new Action(this.CloseSelectWindow));
			this.m_StateController.AddOnExit(6, new Action(this.CloseBuildPointGroup));
			this.m_StateController.AddOnExit(7, new Action(this.CloseBuildPointGroup));
			this.SetEnableObjInput(false);
			SpecialTweet specialTweet = this.m_SpecialTweet;
			specialTweet.OnComplete = (Action)Delegate.Combine(specialTweet.OnComplete, new Action(this.OnCompleteTweet));
		}

		// Token: 0x06003B45 RID: 15173 RVA: 0x0012EDE4 File Offset: 0x0012D1E4
		private void Update()
		{
			this.UpdateKRRPoint();
			if (this.m_IsShowNpc && this.m_NpcAnim.State == 0 && this.m_Npc.IsDoneLoad())
			{
				this.m_NpcAnim.PlayIn();
			}
			else if (!this.m_IsShowNpc && this.m_NpcAnim.State == 2)
			{
				this.m_NpcAnim.PlayOut();
			}
			switch (this.m_StateController.NowState)
			{
			case 2:
			{
				bool flag = true;
				if (this.m_StateController.IsPlayingInOut())
				{
					flag = false;
				}
				else
				{
					for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
					{
						if (this.m_AnimUIPlayerArray[i].State == 3)
						{
							flag = false;
							break;
						}
					}
				}
				if (flag)
				{
					this.Destroy();
					this.m_StateController.ChangeState(3);
					this.OnEnd.Call();
				}
				break;
			}
			case 5:
				switch (this.m_SelectWindow.SelectButton)
				{
				case TownObjSelectWindow.eButton.None:
					if (this.m_SelectWindow.IsDonePlayOut)
					{
						this.OpenMainGroup();
					}
					break;
				case TownObjSelectWindow.eButton.RoomIn:
					this.OnRoomIn.Call();
					break;
				case TownObjSelectWindow.eButton.Schedule:
					this.OpenMainGroup();
					this.OpenScheduleWindow(0, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID());
					break;
				case TownObjSelectWindow.eButton.Info:
					this.OpenMainGroup();
					this.OpenInfoWindow(this.m_Owner.BuildReqData.m_MngID);
					break;
				case TownObjSelectWindow.eButton.LevelUp:
					this.OpenMainGroup();
					this.OpenLevelUpWindow(this.m_Owner.BuildReqData.m_MngID);
					break;
				case TownObjSelectWindow.eButton.Quick:
					this.OpenMainGroup();
					this.OpenQuick(this.m_Owner.BuildReqData.m_MngID);
					break;
				case TownObjSelectWindow.eButton.Store:
					this.OpenMainGroup();
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildStockTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildStockText), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildStockSubText), new Action<int>(this.OnConfirmStore));
					break;
				case TownObjSelectWindow.eButton.Move:
					this.OpenMainGroup();
					this.m_Owner.BuildReqData.m_ObjID = UserTownUtil.GetTownObjData(this.m_Owner.BuildReqData.m_MngID).ObjID;
					this.OpenBuildPointGroup();
					break;
				case TownObjSelectWindow.eButton.Swap:
					this.OpenMainGroup();
					this.OpenBuildAreaWindow(true);
					break;
				}
				break;
			case 6:
				this.UpdateAndroidBackKey();
				break;
			case 7:
				this.UpdateAndroidBackKey();
				break;
			}
		}

		// Token: 0x06003B46 RID: 15174 RVA: 0x0012F0C2 File Offset: 0x0012D4C2
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BuildPointCancelButton, null);
		}

		// Token: 0x06003B47 RID: 15175 RVA: 0x0012F0D0 File Offset: 0x0012D4D0
		public override void PlayIn()
		{
			this.m_StateController.ForceChangeState(1);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.OpenMainGroup();
			this.m_Owner.RefreshTutorialStep();
		}

		// Token: 0x06003B48 RID: 15176 RVA: 0x0012F120 File Offset: 0x0012D520
		public override void PlayOut()
		{
			this.m_StateController.ForceChangeState(2);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003B49 RID: 15177 RVA: 0x0012F15F File Offset: 0x0012D55F
		public override void Destroy()
		{
			base.Destroy();
			this.m_ScheduleWindow.Destroy();
		}

		// Token: 0x06003B4A RID: 15178 RVA: 0x0012F172 File Offset: 0x0012D572
		public override bool IsMenuEnd()
		{
			return this.m_StateController.NowState == 3;
		}

		// Token: 0x06003B4B RID: 15179 RVA: 0x0012F184 File Offset: 0x0012D584
		public void SetMainGroupButtonInteractable(bool flg)
		{
			CustomButton[] componentsInChildren = this.m_MainGroup.GetComponentsInChildren<CustomButton>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (componentsInChildren[i] != null)
				{
					componentsInChildren[i].Interactable = flg;
				}
			}
		}

		// Token: 0x06003B4C RID: 15180 RVA: 0x0012F1C9 File Offset: 0x0012D5C9
		public void SetEnableObjInput(bool flg)
		{
			this.m_Invisible.SetActive(!flg);
			this.m_IsActive = !flg;
		}

		// Token: 0x06003B4D RID: 15181 RVA: 0x0012F1E4 File Offset: 0x0012D5E4
		public bool CheckRaycast()
		{
			return this.m_RaycastCheck.Check();
		}

		// Token: 0x06003B4E RID: 15182 RVA: 0x0012F1F1 File Offset: 0x0012D5F1
		public void SetDarkBG(bool flg)
		{
		}

		// Token: 0x06003B4F RID: 15183 RVA: 0x0012F1F3 File Offset: 0x0012D5F3
		public void UpdateKRRPoint()
		{
			this.m_KRRPointValueText.text = UIUtility.KPValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint(), true);
		}

		// Token: 0x06003B50 RID: 15184 RVA: 0x0012F220 File Offset: 0x0012D620
		public void OpenMainGroup()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainGroup.Open();
				this.SetEnableObjInput(true);
				this.HideNpc();
			}
			else if (this.m_StateController.NowState == 4)
			{
				this.m_MainGroup.Open();
				this.SetEnableObjInput(true);
				this.HideNpc();
			}
		}

		// Token: 0x06003B51 RID: 15185 RVA: 0x0012F284 File Offset: 0x0012D684
		public void CloseMainGroup()
		{
			this.m_MainGroup.Close();
			this.SetEnableObjInput(false);
		}

		// Token: 0x06003B52 RID: 15186 RVA: 0x0012F298 File Offset: 0x0012D698
		public void OnClickBuildButtonCallBack()
		{
			this.m_Owner.BuildReqData.Clear();
			this.OpenBuildTopWindow();
		}

		// Token: 0x06003B53 RID: 15187 RVA: 0x0012F2B0 File Offset: 0x0012D6B0
		public void OpenBuildTopWindow()
		{
			this.ShowNpc();
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_003, "voice_208", true);
			this.m_BuildTopWindow.Open();
		}

		// Token: 0x06003B54 RID: 15188 RVA: 0x0012F2DC File Offset: 0x0012D6DC
		public void OnCloseBuildTopWindowCallBack()
		{
			switch (this.m_BuildTopWindow.SelectButton)
			{
			case BuildTopWindow.eButton.Area:
				this.OpenBuildAreaWindow(false);
				break;
			case BuildTopWindow.eButton.Product:
				this.m_BuildListWindow.Open(TownBuildList.eCategory.Product);
				break;
			case BuildTopWindow.eButton.Buff:
				this.m_BuildListWindow.Open(TownBuildList.eCategory.Buff);
				break;
			case BuildTopWindow.eButton.Have:
				this.m_BuildListWindow.Open(TownBuildList.eCategory.Have);
				break;
			case BuildTopWindow.eButton.Cancel:
				this.OpenMainGroup();
				break;
			}
		}

		// Token: 0x06003B55 RID: 15189 RVA: 0x0012F360 File Offset: 0x0012D760
		public void OpenSelectWindow()
		{
			this.HideNpc();
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildMngID(this.m_Owner.BuildReqData.m_MngID) == null)
			{
				Debug.Log("Not Found ObjData  buildPointIdx=" + this.m_Owner.BuildReqData.m_BuildPoint);
				return;
			}
			if (this.m_StateController.NowState == 5 || this.m_StateController.ChangeState(5))
			{
				this.m_SelectWindow.Open(this.m_Owner.BuildReqData.m_BuildPoint);
				this.SetEnableObjInput(true);
			}
		}

		// Token: 0x06003B56 RID: 15190 RVA: 0x0012F407 File Offset: 0x0012D807
		public TownObjSelectWindow GetSelectWindow()
		{
			return this.m_SelectWindow;
		}

		// Token: 0x06003B57 RID: 15191 RVA: 0x0012F40F File Offset: 0x0012D80F
		public void CloseSelectWindow()
		{
			this.m_SelectWindow.Close();
			this.SetEnableObjInput(false);
		}

		// Token: 0x06003B58 RID: 15192 RVA: 0x0012F423 File Offset: 0x0012D823
		public void OnCompleteObjSelectWindow()
		{
		}

		// Token: 0x06003B59 RID: 15193 RVA: 0x0012F425 File Offset: 0x0012D825
		public void OpenInfoWindow(long mngID)
		{
			this.m_StateWindow.Open(mngID);
		}

		// Token: 0x06003B5A RID: 15194 RVA: 0x0012F433 File Offset: 0x0012D833
		public void OpenBuildWindow(int objID)
		{
			this.m_ConfirmWindow.SetupBuild(objID, SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial());
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B5B RID: 15195 RVA: 0x0012F45B File Offset: 0x0012D85B
		public void OpenPlacementWindow(long mngID)
		{
			this.m_ConfirmWindow.SetupPlacement(mngID);
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B5C RID: 15196 RVA: 0x0012F474 File Offset: 0x0012D874
		public void OpenSwapAreaWindow(int objID)
		{
			this.m_ConfirmWindow.SetupSwapArea(objID);
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B5D RID: 15197 RVA: 0x0012F48D File Offset: 0x0012D88D
		public void OpenLevelUpWindow(long mngID)
		{
			this.m_ConfirmWindow.SetupLevelUp(mngID);
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B5E RID: 15198 RVA: 0x0012F4A6 File Offset: 0x0012D8A6
		public void OpenQuick(long mngID)
		{
			this.m_ConfirmWindow.SetupQuick(mngID);
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B5F RID: 15199 RVA: 0x0012F4BF File Offset: 0x0012D8BF
		public void OpenSell(long mngID)
		{
			this.m_ConfirmWindow.SetupSell(mngID);
			this.m_ConfirmWindow.Open();
		}

		// Token: 0x06003B60 RID: 15200 RVA: 0x0012F4D8 File Offset: 0x0012D8D8
		public void OnCloseConfirmWindowCallBack()
		{
			if (this.m_ConfirmWindow.SelectButton == TownObjConfirmWindow.eButton.Yes)
			{
				switch (this.m_ConfirmWindow.Mode)
				{
				case TownObjConfirmWindow.eMode.Build:
					this.OnExecuteBuild.Call();
					break;
				case TownObjConfirmWindow.eMode.Placement:
					this.OnExecuteBuild.Call();
					break;
				case TownObjConfirmWindow.eMode.Quick:
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownQuickFinalConfirmTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownQuickFinalConfirm, new object[]
					{
						SingletonMonoBehaviour<GameSystem>.Inst.DbMng.StarDefineDB.GetValue(eStarDefineDB.ReduceBuildTimeTownFacilityAmount)
					}), null, new Action<int>(this.OnConfirmQuick));
					break;
				case TownObjConfirmWindow.eMode.Levelup:
					this.OnExecuteLevelUp.Call();
					break;
				case TownObjConfirmWindow.eMode.Sell:
					this.OnExecuteSell.Call();
					break;
				}
				this.OpenSelectWindow();
			}
			else
			{
				this.OpenSelectWindow();
			}
		}

		// Token: 0x06003B61 RID: 15201 RVA: 0x0012F5E0 File Offset: 0x0012D9E0
		private void OnConfirmQuick(int btn)
		{
			if (btn == 0)
			{
				this.OnExecuteQuick.Call();
				this.OpenSelectWindow();
			}
		}

		// Token: 0x06003B62 RID: 15202 RVA: 0x0012F5F9 File Offset: 0x0012D9F9
		public void OpenChangeWindow(long mngID, int targetID)
		{
			this.m_TownObjChangeWindow.Open(mngID, targetID);
		}

		// Token: 0x06003B63 RID: 15203 RVA: 0x0012F608 File Offset: 0x0012DA08
		public void OpenBuildPointGroup()
		{
			this.m_StateController.ForceChangeState(6);
			this.m_BuildPointGroup.Open();
			this.SetEnableObjInput(true);
			this.m_Owner.StartPointSelect();
		}

		// Token: 0x06003B64 RID: 15204 RVA: 0x0012F633 File Offset: 0x0012DA33
		public void CloseBuildPointGroup()
		{
			this.m_BuildPointGroup.Close();
		}

		// Token: 0x06003B65 RID: 15205 RVA: 0x0012F640 File Offset: 0x0012DA40
		public void OnClickCancelBuildButtonCallBack()
		{
			if (this.m_StateController.NowState == 6)
			{
				this.OnClickBuildPointCancelButton.Call();
			}
			else if (this.m_StateController.NowState == 7)
			{
				this.OnClickMovePointCancelButton.Call();
			}
		}

		// Token: 0x06003B66 RID: 15206 RVA: 0x0012F67F File Offset: 0x0012DA7F
		public void OpenMovePointGroup()
		{
			if (this.m_StateController.ChangeState(7))
			{
				this.m_BuildPointGroup.Open();
				this.SetEnableObjInput(true);
			}
		}

		// Token: 0x06003B67 RID: 15207 RVA: 0x0012F6A4 File Offset: 0x0012DAA4
		public void OpenBuildAreaWindow(bool swapMode)
		{
			this.ShowNpc();
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_003, "voice_208", true);
			this.m_ContentAreaWindow.Open(swapMode);
		}

		// Token: 0x06003B68 RID: 15208 RVA: 0x0012F6D0 File Offset: 0x0012DAD0
		public void OnCloseContentAreaWindowCallBack()
		{
			if (this.m_ContentAreaWindow.SelectButton == ContentAreaListWindow.eButton.Build)
			{
				this.m_Owner.BuildReqData.m_ObjID = this.m_ContentAreaWindow.SelectObjID;
				this.m_Owner.BuildReqData.m_MngID = this.m_ContentAreaWindow.SelectObjMngID;
				if (this.m_ContentAreaWindow.IsSwapMode)
				{
					this.OpenSwapAreaWindow(this.m_ContentAreaWindow.SelectObjID);
				}
				else
				{
					this.OpenBuildWindow(this.m_ContentAreaWindow.SelectObjID);
				}
			}
			else if (this.m_ContentAreaWindow.SelectButton == ContentAreaListWindow.eButton.Cancel)
			{
				this.OpenMainGroup();
			}
			else if (this.m_ContentAreaWindow.SelectButton == ContentAreaListWindow.eButton.Back)
			{
				this.OpenMainGroup();
			}
		}

		// Token: 0x06003B69 RID: 15209 RVA: 0x0012F794 File Offset: 0x0012DB94
		public void OnCloseBuildSelectWindowCallBack()
		{
			if (this.m_BuildListWindow.SelectButton == BuildListWindow.eButton.Build || this.m_BuildListWindow.SelectButton == BuildListWindow.eButton.Placement)
			{
				this.m_Owner.BuildReqData.m_ObjID = this.m_BuildListWindow.SelectObjID;
				this.m_Owner.BuildReqData.m_MngID = this.m_BuildListWindow.SelectObjMngID;
				if (this.m_Owner.BuildReqData.m_MngID != -1L)
				{
					this.OpenPlacementWindow(this.m_Owner.BuildReqData.m_MngID);
				}
				else if (this.m_Owner.BuildReqData.m_ObjID != -1)
				{
					this.OpenBuildWindow(this.m_Owner.BuildReqData.m_ObjID);
				}
				else
				{
					this.OpenMainGroup();
				}
			}
			else if (this.m_BuildListWindow.SelectButton == BuildListWindow.eButton.Sell)
			{
				this.m_Owner.BuildReqData.m_ObjID = this.m_BuildListWindow.SelectObjID;
				this.m_Owner.BuildReqData.m_MngID = this.m_BuildListWindow.SelectObjMngID;
				if (this.m_Owner.BuildReqData.m_MngID != -1L)
				{
					this.OpenSell(this.m_Owner.BuildReqData.m_MngID);
				}
			}
			else if (this.m_BuildListWindow.SelectButton == BuildListWindow.eButton.Cancel)
			{
				this.m_BuildTopWindow.Open();
			}
			else if (this.m_BuildListWindow.SelectButton == BuildListWindow.eButton.Back)
			{
				this.m_BuildTopWindow.Open();
			}
		}

		// Token: 0x06003B6A RID: 15210 RVA: 0x0012F91C File Offset: 0x0012DD1C
		private void OnConfirmStore(int btn)
		{
			if (btn == 0)
			{
				this.OnExecuteStore.Call();
			}
		}

		// Token: 0x06003B6B RID: 15211 RVA: 0x0012F92F File Offset: 0x0012DD2F
		public void OnClickTownBufButtonCallBack()
		{
			this.SetEnableObjInput(false);
			this.m_MainGroup.SetEnableInput(false);
			this.m_TownBufWindow.Open();
		}

		// Token: 0x06003B6C RID: 15212 RVA: 0x0012F94F File Offset: 0x0012DD4F
		public void OnCloseTownBufWindowCallBack()
		{
			this.OpenMainGroup();
		}

		// Token: 0x06003B6D RID: 15213 RVA: 0x0012F957 File Offset: 0x0012DD57
		public void OnClickScheduleButtonCallBack()
		{
			this.SetEnableObjInput(false);
			this.m_MainGroup.SetEnableInput(false);
			this.OpenScheduleWindow(0, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID());
		}

		// Token: 0x06003B6E RID: 15214 RVA: 0x0012F987 File Offset: 0x0012DD87
		public void OpenScheduleWindow(int roomIdx, long[] liveCharaMngID)
		{
			this.m_MainGroup.Close();
			this.m_ScheduleWindow.Open(roomIdx, liveCharaMngID);
			this.SetEnableObjInput(false);
		}

		// Token: 0x06003B6F RID: 15215 RVA: 0x0012F9A8 File Offset: 0x0012DDA8
		public void OnDecideLiveMember()
		{
			this.OnDecideLiveCharas.Call(this.m_ScheduleWindow.LiveCharaMngIDs);
		}

		// Token: 0x06003B70 RID: 15216 RVA: 0x0012F9C0 File Offset: 0x0012DDC0
		public void OnCloseScheduleCallBack()
		{
			if (this.m_ScheduleWindow.MemberIdx == -1)
			{
				this.OnClosedScheduleWindow.Call();
				this.OpenMainGroup();
			}
		}

		// Token: 0x06003B71 RID: 15217 RVA: 0x0012F9E4 File Offset: 0x0012DDE4
		public void OnCloseTweetWindowCallBack()
		{
			this.OnClosedTweetWindow();
			this.OpenMainGroup();
		}

		// Token: 0x06003B72 RID: 15218 RVA: 0x0012F9F8 File Offset: 0x0012DDF8
		public void OpenCharaTweetSpecial(long charaMngID, int tweetID, List<TownPartsGetItem> popupList)
		{
			if (popupList != null)
			{
				for (int i = 0; i < popupList.Count; i++)
				{
					popupList[i].IsEnableUpdate = false;
					this.m_HoldPopupList.Add(popupList[i]);
				}
			}
			this.m_SpecialTweet.Play(charaMngID, tweetID);
		}

		// Token: 0x06003B73 RID: 15219 RVA: 0x0012FA50 File Offset: 0x0012DE50
		public void OnCompleteTweet()
		{
			for (int i = 0; i < this.m_HoldPopupList.Count; i++)
			{
				this.m_HoldPopupList[i].IsEnableUpdate = true;
			}
		}

		// Token: 0x06003B74 RID: 15220 RVA: 0x0012FA8B File Offset: 0x0012DE8B
		public void OnCloseTweetWindowSpecialCallBack()
		{
			this.OnClosedTweetWindow();
			this.OpenMainGroup();
		}

		// Token: 0x06003B75 RID: 15221 RVA: 0x0012FA9E File Offset: 0x0012DE9E
		public void OnClickBackButtonCallBack()
		{
			this.m_BuildTopWindow.OnCancelButtonCallBack();
			this.m_BuildListWindow.OnBackButtonCallBack();
			this.m_ContentAreaWindow.OnBackButtonCallBack();
			this.m_TownBufWindow.OnCancelButtonCallBack();
		}

		// Token: 0x06003B76 RID: 15222 RVA: 0x0012FACC File Offset: 0x0012DECC
		public void ShowNpc()
		{
			this.m_Npc.Apply(NpcIllust.eNpc.Build);
			this.m_IsShowNpc = true;
		}

		// Token: 0x06003B77 RID: 15223 RVA: 0x0012FAE1 File Offset: 0x0012DEE1
		public void HideNpc()
		{
			this.m_IsShowNpc = false;
		}

		// Token: 0x06003B78 RID: 15224 RVA: 0x0012FAEA File Offset: 0x0012DEEA
		public void OnAddBuildComEnd(long manageID)
		{
		}

		// Token: 0x040042E1 RID: 17121
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x040042E2 RID: 17122
		[SerializeField]
		private Text m_KRRPointValueText;

		// Token: 0x040042E3 RID: 17123
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040042E4 RID: 17124
		[SerializeField]
		private TownObjSelectWindow m_SelectWindow;

		// Token: 0x040042E5 RID: 17125
		[SerializeField]
		private UIGroup m_BuildPointGroup;

		// Token: 0x040042E6 RID: 17126
		[SerializeField]
		private CustomButton m_BuildPointCancelButton;

		// Token: 0x040042E7 RID: 17127
		[SerializeField]
		private TownBufWindow m_TownBufWindow;

		// Token: 0x040042E8 RID: 17128
		[SerializeField]
		[Tooltip("建築トップ")]
		private BuildTopWindow m_BuildTopWindow;

		// Token: 0x040042E9 RID: 17129
		[SerializeField]
		[Tooltip("建築リスト")]
		private BuildListWindow m_BuildListWindow;

		// Token: 0x040042EA RID: 17130
		[SerializeField]
		[Tooltip("コンテンツエリア選択リスト")]
		private ContentAreaListWindow m_ContentAreaWindow;

		// Token: 0x040042EB RID: 17131
		[SerializeField]
		[Tooltip("スケジュール")]
		private ScheduleWindow m_ScheduleWindow;

		// Token: 0x040042EC RID: 17132
		[SerializeField]
		private CharaListWindow m_CharaListWindow;

		// Token: 0x040042ED RID: 17133
		[SerializeField]
		[Tooltip("入れ替え")]
		private TownObjChangeWindow m_TownObjChangeWindow;

		// Token: 0x040042EE RID: 17134
		[SerializeField]
		[Tooltip("汎用確認ウィンドウ")]
		private TownObjConfirmWindow m_ConfirmWindow;

		// Token: 0x040042EF RID: 17135
		[SerializeField]
		private TownObjStateWindow m_StateWindow;

		// Token: 0x040042F0 RID: 17136
		[SerializeField]
		private SpecialTweet m_SpecialTweet;

		// Token: 0x040042F1 RID: 17137
		[SerializeField]
		private NpcIllust m_Npc;

		// Token: 0x040042F2 RID: 17138
		[SerializeField]
		private AnimUIPlayer m_NpcAnim;

		// Token: 0x040042F3 RID: 17139
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040042F4 RID: 17140
		[SerializeField]
		private GameObject m_Invisible;

		// Token: 0x040042F5 RID: 17141
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x040042F6 RID: 17142
		private TownMain m_Owner;

		// Token: 0x040042F7 RID: 17143
		private bool m_IsActive;

		// Token: 0x040042F8 RID: 17144
		private bool m_IsShowNpc;

		// Token: 0x04004305 RID: 17157
		private List<TownPartsGetItem> m_HoldPopupList = new List<TownPartsGetItem>();

		// Token: 0x02000B0D RID: 2829
		public enum eUIState
		{
			// Token: 0x04004307 RID: 17159
			Hide,
			// Token: 0x04004308 RID: 17160
			In,
			// Token: 0x04004309 RID: 17161
			Out,
			// Token: 0x0400430A RID: 17162
			End,
			// Token: 0x0400430B RID: 17163
			Main,
			// Token: 0x0400430C RID: 17164
			Select,
			// Token: 0x0400430D RID: 17165
			BuildPoint,
			// Token: 0x0400430E RID: 17166
			MovePoint,
			// Token: 0x0400430F RID: 17167
			Tweet,
			// Token: 0x04004310 RID: 17168
			Num
		}
	}
}
