﻿using System;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000AFA RID: 2810
	public class TownCharaHud : MonoBehaviour
	{
		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06003AC8 RID: 15048 RVA: 0x0012C18D File Offset: 0x0012A58D
		public RectTransform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x06003AC9 RID: 15049 RVA: 0x0012C195 File Offset: 0x0012A595
		private void Awake()
		{
			this.m_Transform = base.gameObject.GetComponent<RectTransform>();
			this.m_CharaTweet.gameObject.SetActive(false);
		}

		// Token: 0x06003ACA RID: 15050 RVA: 0x0012C1B9 File Offset: 0x0012A5B9
		public TownCharaTweet GetCharaTweet()
		{
			return this.m_CharaTweet;
		}

		// Token: 0x04004258 RID: 16984
		[SerializeField]
		private TownCharaTweet m_CharaTweet;

		// Token: 0x04004259 RID: 16985
		private RectTransform m_Transform;
	}
}
