﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AFF RID: 2815
	public class TownObjConfirmWindow : UIGroup
	{
		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06003AD7 RID: 15063 RVA: 0x0012C4CB File Offset: 0x0012A8CB
		public TownObjConfirmWindow.eButton SelectButton
		{
			get
			{
				return this.m_Button;
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06003AD8 RID: 15064 RVA: 0x0012C4D3 File Offset: 0x0012A8D3
		public TownObjConfirmWindow.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x06003AD9 RID: 15065 RVA: 0x0012C4DC File Offset: 0x0012A8DC
		private void HideAllObj()
		{
			this.m_BuildInfoTitle.SetActive(false);
			this.m_SellInfoTitle.SetActive(false);
			this.m_CostTimeObj.SetActive(false);
			this.m_CostGold.SetActive(false);
			this.m_CostKP.SetActive(false);
			this.m_CostRemainTime.SetActive(false);
			this.m_CostGem.SetActive(false);
			this.m_LvUp.SetActive(false);
			this.m_SellGold.SetActive(false);
			this.m_SellKP.SetActive(false);
		}

		// Token: 0x06003ADA RID: 15066 RVA: 0x0012C564 File Offset: 0x0012A964
		public void SetupBuild(int objID, bool tutorial = false)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Build;
			this.m_ObjID = objID;
			long num = -1L;
			if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_Category == 6)
			{
				this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmContentPlacementTitle);
				this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmContentPlacementText);
				num = UserTownUtil.GetStoreTownObj(objID);
			}
			else
			{
				this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmBuildTitle);
				this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmBuildText);
			}
			if (num != -1L)
			{
				this.m_ObjInfo.Apply(num);
			}
			else
			{
				this.m_ObjInfo.Apply(objID, 1);
			}
			int num2 = TownUtility.GetTownObjectLevelUpConfirm(this.m_ObjID, 1, TownDefine.eTownLevelUpConditionCategory.Gold);
			int num3 = TownUtility.GetTownObjectLevelUpConfirm(this.m_ObjID, 1, TownDefine.eTownLevelUpConditionCategory.Kirara);
			int sec = (int)UserTownUtil.GetBuildUpTime(objID, 0) / 1000;
			if (tutorial)
			{
				num2 = 0;
				num3 = 0;
			}
			this.HideAllObj();
			this.m_BuildInfoTitle.SetActive(true);
			this.m_CostTimeObj.SetActive(true);
			this.m_CostTimeText.text = UIUtility.GetBuildTimeString(sec);
			if (num2 > 0)
			{
				this.m_CostGold.SetActive(true);
				this.m_CostGoldText.text = UIUtility.GetNeedGoldString(num2);
				this.m_CostHaveGoldText.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			}
			if (num3 > 0)
			{
				this.m_CostKP.SetActive(true);
				this.m_CostKPText.text = UIUtility.GetNeedKPString(num3);
				this.m_CostHaveKPText.text = UIUtility.KPValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint(), true);
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)num2) || SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfKP((long)num3))
			{
				this.m_YesButton.Interactable = false;
			}
			else
			{
				this.m_YesButton.Interactable = true;
			}
			if (tutorial)
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.SetEnableDark(true, 1f, true);
				this.m_YesButton.Interactable = true;
				this.m_TutorialTarget.SetEnable(true);
			}
		}

		// Token: 0x06003ADB RID: 15067 RVA: 0x0012C7DC File Offset: 0x0012ABDC
		public override void Update()
		{
			base.Update();
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				if (!tutorialMessage.GetEnableArrow() && base.IsIdle())
				{
					tutorialMessage.SetEnableArrow(true);
					tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
					tutorialMessage.SetArrowPosition(this.m_YesButton.GetComponent<RectTransform>().position);
				}
			}
		}

		// Token: 0x06003ADC RID: 15068 RVA: 0x0012C848 File Offset: 0x0012AC48
		public void SetupPlacement(long mngID)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Placement;
			this.m_MngID = mngID;
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_MngID);
			this.m_ObjID = townObjData.ObjID;
			this.m_ObjInfo.Apply(this.m_MngID);
			this.HideAllObj();
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmPlacementTitle);
			this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmPlacementText);
			this.m_YesButton.Interactable = true;
		}

		// Token: 0x06003ADD RID: 15069 RVA: 0x0012C8DC File Offset: 0x0012ACDC
		public void SetupSwapArea(int objID)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Placement;
			this.m_ObjID = objID;
			long storeTownObj = UserTownUtil.GetStoreTownObj(objID);
			if (storeTownObj == -1L)
			{
				this.m_ObjInfo.Apply(this.m_ObjID, 1);
			}
			else
			{
				this.m_ObjInfo.Apply(storeTownObj);
			}
			this.HideAllObj();
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmContentSwapTitle);
			this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmContentSwapText);
			this.m_YesButton.Interactable = true;
		}

		// Token: 0x06003ADE RID: 15070 RVA: 0x0012C97C File Offset: 0x0012AD7C
		public void SetupQuick(long mngID)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Quick;
			this.m_MngID = mngID;
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_MngID);
			this.m_ObjID = townObjData.ObjID;
			this.m_ObjInfo.Apply(this.m_MngID);
			this.HideAllObj();
			this.m_BuildInfoTitle.SetActive(true);
			this.m_CostRemainTime.SetActive(true);
			this.m_CostGem.SetActive(true);
			long remainingTime = UserTownUtil.GetRemainingTime(this.m_MngID, -1);
			this.m_RemainTimeText.text = UIUtility.GetBuildTimeString((int)(remainingTime / 1000L));
			int num = (int)UserTownUtil.GetRemainingUseGem(this.m_MngID);
			this.m_CostGemText.text = UIUtility.GetNeedGemString(num);
			this.m_HaveGemText.text = UIUtility.GemValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gem, true);
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmQuickTitle);
			this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmQuickText);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGem((long)num))
			{
				this.m_YesButton.Interactable = false;
			}
			else
			{
				this.m_YesButton.Interactable = true;
			}
		}

		// Token: 0x06003ADF RID: 15071 RVA: 0x0012CACC File Offset: 0x0012AECC
		public void SetupLevelUp(long mngID)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Levelup;
			this.m_MngID = mngID;
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_MngID);
			this.m_ObjID = townObjData.ObjID;
			this.m_ObjInfo.ApplyLvUp(this.m_MngID, townObjData.Lv, townObjData.Lv + 1);
			this.HideAllObj();
			this.m_LvUp.SetActive(true);
			bool flag = this.m_LvCondition.Apply(this.m_ObjID, townObjData.Lv + 1);
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmLvupTitle);
			this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmLvupText);
			if (!flag)
			{
				this.m_YesButton.Interactable = false;
			}
			else
			{
				this.m_YesButton.Interactable = true;
			}
		}

		// Token: 0x06003AE0 RID: 15072 RVA: 0x0012CBAC File Offset: 0x0012AFAC
		public void SetupSell(long mngID)
		{
			this.m_Mode = TownObjConfirmWindow.eMode.Sell;
			this.m_MngID = mngID;
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(this.m_MngID);
			this.m_ObjID = townObjData.ObjID;
			this.m_ObjInfo.Apply(this.m_MngID);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(this.m_ObjID);
			TownObjectLevelUpDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, townObjData.Lv);
			this.HideAllObj();
			this.m_SellInfoTitle.SetActive(true);
			this.m_SellGold.SetActive(true);
			this.m_SellKP.SetActive(true);
			this.m_SellGoldText.text = UIUtility.GoldValueToString(param2.m_SaleGoldAmount, false);
			this.m_SellHaveGoldText.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			this.m_SellKPText.text = UIUtility.KPValueToString(param2.m_SaleKiraraAmount, false);
			this.m_SellHaveKPText.text = UIUtility.KPValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.KRRPoint.GetPoint(), true);
			this.m_Title.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmSellTitle);
			this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownConfirmSellText);
			this.m_YesButton.Interactable = true;
		}

		// Token: 0x06003AE1 RID: 15073 RVA: 0x0012CD20 File Offset: 0x0012B120
		public void OnClickYesCallBack()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
				this.m_TutorialTarget.SetEnable(false);
			}
			this.m_Button = TownObjConfirmWindow.eButton.Yes;
			this.Close();
		}

		// Token: 0x06003AE2 RID: 15074 RVA: 0x0012CD80 File Offset: 0x0012B180
		public void OnClickNoCallBack()
		{
			this.m_Button = TownObjConfirmWindow.eButton.No;
			this.Close();
		}

		// Token: 0x0400426C RID: 17004
		[SerializeField]
		private Text m_Title;

		// Token: 0x0400426D RID: 17005
		[SerializeField]
		private TownObjInfo m_ObjInfo;

		// Token: 0x0400426E RID: 17006
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x0400426F RID: 17007
		[SerializeField]
		private Text m_CostTimeText;

		// Token: 0x04004270 RID: 17008
		[SerializeField]
		private Text m_CostGoldText;

		// Token: 0x04004271 RID: 17009
		[SerializeField]
		private Text m_CostHaveGoldText;

		// Token: 0x04004272 RID: 17010
		[SerializeField]
		private Text m_CostKPText;

		// Token: 0x04004273 RID: 17011
		[SerializeField]
		private Text m_CostHaveKPText;

		// Token: 0x04004274 RID: 17012
		[SerializeField]
		private Text m_SellGoldText;

		// Token: 0x04004275 RID: 17013
		[SerializeField]
		private Text m_SellHaveGoldText;

		// Token: 0x04004276 RID: 17014
		[SerializeField]
		private Text m_SellKPText;

		// Token: 0x04004277 RID: 17015
		[SerializeField]
		private Text m_SellHaveKPText;

		// Token: 0x04004278 RID: 17016
		[SerializeField]
		private Text m_RemainTimeText;

		// Token: 0x04004279 RID: 17017
		[SerializeField]
		private Text m_CostGemText;

		// Token: 0x0400427A RID: 17018
		[SerializeField]
		private Text m_HaveGemText;

		// Token: 0x0400427B RID: 17019
		[SerializeField]
		private TownLvupCondition m_LvCondition;

		// Token: 0x0400427C RID: 17020
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x0400427D RID: 17021
		[SerializeField]
		private GameObject m_BuildInfoTitle;

		// Token: 0x0400427E RID: 17022
		[SerializeField]
		private GameObject m_SellInfoTitle;

		// Token: 0x0400427F RID: 17023
		[SerializeField]
		private GameObject m_CostTimeObj;

		// Token: 0x04004280 RID: 17024
		[SerializeField]
		private GameObject m_CostGold;

		// Token: 0x04004281 RID: 17025
		[SerializeField]
		private GameObject m_CostKP;

		// Token: 0x04004282 RID: 17026
		[SerializeField]
		private GameObject m_SellGold;

		// Token: 0x04004283 RID: 17027
		[SerializeField]
		private GameObject m_SellKP;

		// Token: 0x04004284 RID: 17028
		[SerializeField]
		private GameObject m_CostRemainTime;

		// Token: 0x04004285 RID: 17029
		[SerializeField]
		private GameObject m_CostGem;

		// Token: 0x04004286 RID: 17030
		[SerializeField]
		private GameObject m_LvUp;

		// Token: 0x04004287 RID: 17031
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x04004288 RID: 17032
		private TownObjConfirmWindow.eMode m_Mode;

		// Token: 0x04004289 RID: 17033
		private int m_ObjID = -1;

		// Token: 0x0400428A RID: 17034
		private long m_MngID = -1L;

		// Token: 0x0400428B RID: 17035
		private TownObjConfirmWindow.eButton m_Button;

		// Token: 0x02000B00 RID: 2816
		public enum eMode
		{
			// Token: 0x0400428D RID: 17037
			Build,
			// Token: 0x0400428E RID: 17038
			Placement,
			// Token: 0x0400428F RID: 17039
			Quick,
			// Token: 0x04004290 RID: 17040
			Levelup,
			// Token: 0x04004291 RID: 17041
			Sell
		}

		// Token: 0x02000B01 RID: 2817
		public enum eButton
		{
			// Token: 0x04004293 RID: 17043
			None,
			// Token: 0x04004294 RID: 17044
			Yes,
			// Token: 0x04004295 RID: 17045
			No
		}
	}
}
