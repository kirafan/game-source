﻿using System;
using System.Collections.Generic;
using System.Text;
using Star.Town;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000AF1 RID: 2801
	public class TownBufWindow : UIGroup
	{
		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06003AAC RID: 15020 RVA: 0x0012B4B5 File Offset: 0x001298B5
		public TownBufWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x06003AAD RID: 15021 RVA: 0x0012B4BD File Offset: 0x001298BD
		public override void Open()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.Apply();
			}
			base.Open();
		}

		// Token: 0x06003AAE RID: 15022 RVA: 0x0012B4EA File Offset: 0x001298EA
		public void OnCancelButtonCallBack()
		{
			this.m_SelectButton = TownBufWindow.eButton.Cancel;
			this.Close();
		}

		// Token: 0x06003AAF RID: 15023 RVA: 0x0012B4FC File Offset: 0x001298FC
		private void Apply()
		{
			TownBuff townBuff = TownBuff.CalcTownBuff();
			for (int i = 0; i < this.m_ItemList.Count; i++)
			{
				UnityEngine.Object.Destroy(this.m_ItemList[i].gameObject);
			}
			this.m_ItemList.Clear();
			bool flag = false;
			long num = TownBuff.CalcTownGold();
			if (num > 0L)
			{
				this.AddTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowCategoryProduction));
				this.AddObj(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowGold), UIUtility.GoldValueToString(num, true) + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowMinute));
			}
			bool flag2 = false;
			for (int j = 0; j < 5; j++)
			{
				List<TownBuff.BuffParam> buffData = townBuff.GetBuffData(eTownObjectBuffTargetConditionType.ClassType, j);
				string title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowClassFighter + j);
				if (!flag2 && buffData.Count > 0)
				{
					this.AddTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowCategoryClass));
					flag2 = true;
				}
				for (int k = 0; k < buffData.Count; k++)
				{
					this.AddObj(title, buffData[k]);
					flag = true;
				}
			}
			flag2 = false;
			for (int l = 0; l < UIDefine.ELEMENT_SORT.Length; l++)
			{
				eElementType eElementType = UIUtility.ElementSortIdxToEnum(l);
				List<TownBuff.BuffParam> typeData = townBuff.GetTypeData(eTownObjectBuffTargetConditionType.None, eTownObjectBuffType.ResistFire + (int)eElementType);
				string title = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowElementFire + (int)eElementType);
				if (!flag2 && typeData.Count > 0)
				{
					this.AddTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowCategoryElement));
					flag2 = true;
				}
				for (int m = 0; m < typeData.Count; m++)
				{
					this.AddObj(title, typeData[m]);
					flag = true;
				}
			}
			flag2 = false;
			eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
			for (int n = 0; n < eTitleTypeConverter.HowManyTitles(); n++)
			{
				List<TownBuff.BuffParam> buffData2 = townBuff.GetBuffData(eTownObjectBuffTargetConditionType.TitleType, (int)eTitleTypeConverter.ConvertFromOrderToTitleType(n));
				string title = eTitleTypeConverter.ConvertFromOrderToTitleName(n) + SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowContentUpgrade);
				if (!flag2 && buffData2.Count > 0)
				{
					this.AddTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuffWindowCategoryContent));
					flag2 = true;
				}
				for (int num2 = 0; num2 < buffData2.Count; num2++)
				{
					this.AddObj(title, buffData2[num2]);
					flag = true;
				}
			}
			if (!flag)
			{
				RectTransform rectTransform = UnityEngine.Object.Instantiate<RectTransform>(this.m_NoEffectPrefab, this.m_Parent, false);
				this.m_ItemList.Add(rectTransform.GetComponent<RectTransform>());
			}
		}

		// Token: 0x06003AB0 RID: 15024 RVA: 0x0012B7EC File Offset: 0x00129BEC
		private void AddTitle(string title)
		{
			TownBuffItem townBuffItem = UnityEngine.Object.Instantiate<TownBuffItem>(this.m_TownBuffItemTitlePrefab, this.m_Parent, false);
			townBuffItem.Apply(title, null);
			this.m_ItemList.Add(townBuffItem.GetComponent<RectTransform>());
		}

		// Token: 0x06003AB1 RID: 15025 RVA: 0x0012B828 File Offset: 0x00129C28
		private void AddObj(string title, TownBuff.BuffParam param)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (param.m_Type <= eTownObjectBuffType.Luck)
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StatusHp + (int)param.m_Type));
			}
			else
			{
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ElementFire + (int)param.m_Type - 7));
				stringBuilder.Append("耐性");
			}
			stringBuilder.Append("+");
			stringBuilder.Append(param.m_Val);
			stringBuilder.Append("%");
			TownBuffItem townBuffItem = UnityEngine.Object.Instantiate<TownBuffItem>(this.m_TownBuffItemPrefab, this.m_Parent, false);
			townBuffItem.Apply(title, stringBuilder.ToString());
			this.m_ItemList.Add(townBuffItem.GetComponent<RectTransform>());
		}

		// Token: 0x06003AB2 RID: 15026 RVA: 0x0012B8F4 File Offset: 0x00129CF4
		private void AddObj(string title, string value)
		{
			TownBuffItem townBuffItem = UnityEngine.Object.Instantiate<TownBuffItem>(this.m_TownBuffItemPrefab, this.m_Parent, false);
			townBuffItem.Apply(title, value);
			this.m_ItemList.Add(townBuffItem.GetComponent<RectTransform>());
		}

		// Token: 0x04004224 RID: 16932
		private TownBufWindow.eButton m_SelectButton;

		// Token: 0x04004225 RID: 16933
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x04004226 RID: 16934
		[SerializeField]
		private RectTransform m_NoEffectPrefab;

		// Token: 0x04004227 RID: 16935
		[SerializeField]
		private TownBuffItem m_TownBuffItemTitlePrefab;

		// Token: 0x04004228 RID: 16936
		[SerializeField]
		private TownBuffItem m_TownBuffItemPrefab;

		// Token: 0x04004229 RID: 16937
		private List<RectTransform> m_ItemList = new List<RectTransform>();

		// Token: 0x02000AF2 RID: 2802
		public enum eButton
		{
			// Token: 0x0400422B RID: 16939
			Cancel
		}
	}
}
