﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B02 RID: 2818
	public class TownObjInfo : MonoBehaviour
	{
		// Token: 0x06003AE4 RID: 15076 RVA: 0x0012CD98 File Offset: 0x0012B198
		public void Apply(long objMngID)
		{
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(objMngID);
			this.Apply(townObjData.ObjID, townObjData.Lv);
		}

		// Token: 0x06003AE5 RID: 15077 RVA: 0x0012CDC0 File Offset: 0x0012B1C0
		public void Apply(int objID, int lv)
		{
			objID = TownUtility.CalcAreaToContentID(objID);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.ApplyBaseInfo(objID, lv);
			if (this.m_LvValueChange != null)
			{
				this.m_LvValueChange.gameObject.SetActive(false);
			}
			this.m_LvText.gameObject.SetActive(true);
			this.m_LvText.SetValue(lv.ToString());
			TownObjectBuffDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjBuffDB.GetParam(param.m_BuffParamID);
			int num = lv - 1;
			if (num < 0)
			{
				num = 0;
			}
			switch (param.m_Category)
			{
			case 4:
			case 5:
			case 6:
				this.m_EffectObj.SetActive(false);
				this.m_BuffObj.SetActive(true);
				for (int i = 0; i < this.m_StatusTexts.Length; i++)
				{
					this.m_StatusTexts[i].gameObject.SetActive(false);
				}
				for (int j = 0; j < this.m_ValueChanges.Length; j++)
				{
					this.m_ValueChanges[j].gameObject.SetActive(false);
				}
				for (int k = 0; k < param2.m_Datas.Length; k++)
				{
					int num2 = this.BuffTypeToIdx((eTownObjectBuffType)param2.m_Datas[k].m_BuffType);
					this.m_StatusTexts[num2].gameObject.SetActive(true);
					this.m_StatusTexts[num2].SetValue("+" + param2.m_Datas[k].m_Value[num].ToString() + "%");
				}
				break;
			default:
			{
				this.m_EffectObj.SetActive(true);
				this.m_BuffObj.SetActive(false);
				TownObjectLevelUpDB_Param param3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, lv);
				string text = string.Empty;
				eTownObjectCategory category = (eTownObjectCategory)param.m_Category;
				if (category != eTownObjectCategory.Money)
				{
					if (category != eTownObjectCategory.Item)
					{
						if (category == eTownObjectCategory.Room)
						{
							text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoRoom, new object[]
							{
								param3.m_ExtensionKey
							});
						}
					}
					else
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoItem, new object[]
						{
							param3.m_WakeTime / 60,
							param3.m_LimitNum
						});
					}
				}
				else
				{
					text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoGold, new object[]
					{
						this.GetMoneyMaxStock(param3),
						this.GetMoneySpeed(param3),
						param3.m_LimitNum * param3.m_WakeTime / 60
					});
				}
				this.m_EffectText.text = text;
				break;
			}
			}
		}

		// Token: 0x06003AE6 RID: 15078 RVA: 0x0012D0D6 File Offset: 0x0012B4D6
		public int GetMoneyMaxStock(TownObjectLevelUpDB_Param param)
		{
			return UserTownUtil.GetFieldDropItems((int)param.m_ResultNo[0]).m_Num * param.m_LimitNum;
		}

		// Token: 0x06003AE7 RID: 15079 RVA: 0x0012D0F3 File Offset: 0x0012B4F3
		public int GetMoneySpeed(TownObjectLevelUpDB_Param param)
		{
			return UserTownUtil.GetFieldDropItems((int)param.m_ResultNo[0]).m_Num * 60 / param.m_WakeTime;
		}

		// Token: 0x06003AE8 RID: 15080 RVA: 0x0012D114 File Offset: 0x0012B514
		public void ApplyLvUp(long objMngID, int beforeLv, int afterLv)
		{
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(objMngID);
			int num = townObjData.ObjID;
			num = TownUtility.CalcAreaToContentID(num);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num);
			this.ApplyBaseInfo(num, beforeLv);
			if (this.m_LvValueChange != null)
			{
				this.m_LvValueChange.gameObject.SetActive(true);
			}
			this.m_LvText.gameObject.SetActive(false);
			this.m_LvValueChange.Apply(beforeLv, afterLv);
			TownObjectBuffDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjBuffDB.GetParam(param.m_BuffParamID);
			switch (param.m_Category)
			{
			case 4:
			case 5:
			case 6:
				for (int i = 0; i < this.m_StatusTexts.Length; i++)
				{
					this.m_StatusTexts[i].gameObject.SetActive(false);
				}
				for (int j = 0; j < this.m_ValueChanges.Length; j++)
				{
					this.m_ValueChanges[j].gameObject.SetActive(false);
				}
				for (int k = 0; k < param2.m_Datas.Length; k++)
				{
					eTownObjectBuffType buffType = (eTownObjectBuffType)param2.m_Datas[k].m_BuffType;
					this.m_ValueChanges[this.BuffTypeToIdx(buffType)].gameObject.SetActive(true);
					this.m_ValueChanges[this.BuffTypeToIdx(buffType)].ApplyBuffPercent(param2.m_Datas[k].m_Value[beforeLv - 1], param2.m_Datas[k].m_Value[afterLv - 1]);
				}
				break;
			default:
			{
				this.m_EffectObj.SetActive(true);
				this.m_BuffObj.SetActive(false);
				TownObjectLevelUpDB_Param param3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, beforeLv);
				TownObjectLevelUpDB_Param param4 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, afterLv);
				string text = string.Empty;
				eTownObjectCategory category = (eTownObjectCategory)param.m_Category;
				if (category != eTownObjectCategory.Money)
				{
					if (category != eTownObjectCategory.Item)
					{
						if (category == eTownObjectCategory.Room)
						{
							int num2 = param4.m_ExtensionKey - param3.m_ExtensionKey;
							StringBuilder stringBuilder = new StringBuilder();
							stringBuilder.Append(UIUtility.KPValueToString(param3.m_ExtensionKey, false));
							if (num2 > 0)
							{
								stringBuilder.Append("→");
								StringBuilder stringBuilder2 = new StringBuilder();
								stringBuilder2.Append(UIUtility.KPValueToString(param4.m_ExtensionKey, false));
								stringBuilder2 = UIUtility.GetIncreaseString(stringBuilder2);
								stringBuilder.Append(stringBuilder2);
							}
							text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoRoom, new object[]
							{
								stringBuilder.ToString()
							});
						}
					}
					else
					{
						int num3 = param4.m_LimitNum - param3.m_LimitNum;
						StringBuilder stringBuilder3 = new StringBuilder();
						stringBuilder3.Append(param3.m_LimitNum);
						if (num3 > 0)
						{
							stringBuilder3.Append("→");
							StringBuilder stringBuilder4 = new StringBuilder();
							stringBuilder4.Append(param4.m_LimitNum);
							stringBuilder4 = UIUtility.GetIncreaseString(stringBuilder4);
							stringBuilder3.Append(stringBuilder4);
						}
						int num4 = param4.m_WakeTime / 60 - param3.m_WakeTime / 60;
						StringBuilder stringBuilder5 = new StringBuilder();
						stringBuilder5.Append(param3.m_WakeTime / 60);
						if (num4 < 0)
						{
							stringBuilder5.Append("→");
							StringBuilder stringBuilder6 = new StringBuilder();
							stringBuilder6.Append(param4.m_WakeTime / 60);
							stringBuilder6 = UIUtility.GetIncreaseString(stringBuilder6);
							stringBuilder5.Append(stringBuilder6);
						}
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoItem, new object[]
						{
							stringBuilder5.ToString(),
							stringBuilder3.ToString()
						});
					}
				}
				else
				{
					StringBuilder stringBuilder7 = new StringBuilder();
					int moneyMaxStock = this.GetMoneyMaxStock(param3);
					int moneyMaxStock2 = this.GetMoneyMaxStock(param4);
					stringBuilder7.Append(moneyMaxStock);
					if (moneyMaxStock2 - moneyMaxStock > 0)
					{
						stringBuilder7.Append("→");
						StringBuilder stringBuilder8 = new StringBuilder();
						stringBuilder8.Append(moneyMaxStock2);
						stringBuilder8 = UIUtility.GetIncreaseString(stringBuilder8);
						stringBuilder7.Append(stringBuilder8);
					}
					string text2 = stringBuilder7.ToString();
					int moneySpeed = this.GetMoneySpeed(param3);
					int moneySpeed2 = this.GetMoneySpeed(param4);
					stringBuilder7.Length = 0;
					stringBuilder7.Append(moneySpeed);
					if (moneySpeed2 - moneySpeed > 0)
					{
						stringBuilder7.Append("→");
						StringBuilder stringBuilder9 = new StringBuilder();
						stringBuilder9.Append(moneySpeed2);
						stringBuilder9 = UIUtility.GetIncreaseString(stringBuilder9);
						stringBuilder7.Append(stringBuilder9);
					}
					string text3 = stringBuilder7.ToString();
					int num5 = param3.m_WakeTime * param3.m_LimitNum / 60;
					int num6 = param4.m_WakeTime * param4.m_LimitNum / 60;
					stringBuilder7.Length = 0;
					stringBuilder7.Append(num5);
					if (num6 - num5 > 0)
					{
						stringBuilder7.Append("→");
						StringBuilder stringBuilder10 = new StringBuilder();
						stringBuilder10.Append(num6);
						stringBuilder10 = UIUtility.GetIncreaseString(stringBuilder10);
						stringBuilder7.Append(stringBuilder10);
					}
					string text4 = stringBuilder7.ToString();
					text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildInfoGold, new object[]
					{
						text2,
						text3,
						text4
					});
				}
				this.m_EffectText.text = text;
				break;
			}
			}
		}

		// Token: 0x06003AE9 RID: 15081 RVA: 0x0012D688 File Offset: 0x0012BA88
		private void ApplyBaseInfo(int objID, int lv)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_NameText.text = param.m_ObjName;
			this.m_Icon.Apply(objID, lv);
			this.m_DetailText.text = param.m_DetailText;
			eTownObjectCategory category = (eTownObjectCategory)param.m_Category;
			if (category != eTownObjectCategory.Buf && category != eTownObjectCategory.Contents)
			{
				this.m_EffectObj.SetActive(true);
				this.m_BuffObj.SetActive(false);
				this.m_EffectText.text = null;
				this.m_BuffConditionText.text = null;
			}
			else
			{
				this.m_EffectObj.SetActive(false);
				this.m_BuffObj.SetActive(true);
				TownObjectBuffDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjBuffDB.GetParam(param.m_BuffParamID);
				string text = string.Empty;
				eTownObjectBuffTargetConditionType targetConditionType = (eTownObjectBuffTargetConditionType)param2.m_Datas[0].m_TargetConditionType;
				if (targetConditionType != eTownObjectBuffTargetConditionType.ClassType)
				{
					if (targetConditionType != eTownObjectBuffTargetConditionType.ElementType)
					{
						if (targetConditionType == eTownObjectBuffTargetConditionType.TitleType)
						{
							text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB.GetParam((eTitleType)param2.m_Datas[0].m_TargetCondition).m_DisplayName;
						}
					}
					else
					{
						text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ElementFire + param2.m_Datas[0].m_TargetCondition);
					}
				}
				else
				{
					text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + param2.m_Datas[0].m_TargetCondition);
				}
				if (string.IsNullOrEmpty(text))
				{
					this.m_BuffConditionText.text = string.Empty;
				}
				else
				{
					this.m_BuffConditionText.text = "※「" + text + "」のキャラクターにのみ効果が適用されます。";
				}
			}
		}

		// Token: 0x06003AEA RID: 15082 RVA: 0x0012D868 File Offset: 0x0012BC68
		private int BuffTypeToIdx(eTownObjectBuffType buffType)
		{
			switch (buffType)
			{
			case eTownObjectBuffType.Hp:
			case eTownObjectBuffType.Atk:
			case eTownObjectBuffType.Mgc:
			case eTownObjectBuffType.Def:
			case eTownObjectBuffType.MDef:
			case eTownObjectBuffType.Spd:
				return (int)buffType;
			case eTownObjectBuffType.ResistFire:
			case eTownObjectBuffType.ResistWater:
			case eTownObjectBuffType.ResistEarth:
			case eTownObjectBuffType.ResistWind:
			case eTownObjectBuffType.ResistMoon:
			case eTownObjectBuffType.ResistSun:
				return buffType - eTownObjectBuffType.Atk;
			}
			return buffType - eTownObjectBuffType.Atk;
		}

		// Token: 0x04004296 RID: 17046
		[SerializeField]
		private TownObjectIcon m_Icon;

		// Token: 0x04004297 RID: 17047
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04004298 RID: 17048
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x04004299 RID: 17049
		[SerializeField]
		[Tooltip("LV")]
		private TextField m_LvText;

		// Token: 0x0400429A RID: 17050
		[SerializeField]
		[Tooltip("LV上昇表示")]
		private TextFieldValueChange m_LvValueChange;

		// Token: 0x0400429B RID: 17051
		[SerializeField]
		[Tooltip("バフでない場合の表示物")]
		private GameObject m_EffectObj;

		// Token: 0x0400429C RID: 17052
		[SerializeField]
		[Tooltip("バフでない場合のテキスト")]
		private Text m_EffectText;

		// Token: 0x0400429D RID: 17053
		[SerializeField]
		[Tooltip("バフの場合の表示物")]
		private GameObject m_BuffObj;

		// Token: 0x0400429E RID: 17054
		[SerializeField]
		[Tooltip("バフのステータス表示\u3000Hp,Atk,Mgc,Def,MDef,Spd,Fire,Water,Earth,Wind,Moon,Sun")]
		private TextField[] m_StatusTexts;

		// Token: 0x0400429F RID: 17055
		[SerializeField]
		[Tooltip("LVUP時バフの上昇表示\u3000Hp,Atk,Mgc,Def,MDef,Spd,Fire,Water,Earth,Wind,Moon,Sun")]
		private TextFieldValueChange[] m_ValueChanges;

		// Token: 0x040042A0 RID: 17056
		[SerializeField]
		[Tooltip("～のキャラクターにのみ効果が適用されます。")]
		private Text m_BuffConditionText;
	}
}
