﻿using System;

namespace Star.UI.Town
{
	// Token: 0x02000AEB RID: 2795
	public class ContentAreaListItemData : ScrollItemData
	{
		// Token: 0x06003A8F RID: 14991 RVA: 0x0012ACE9 File Offset: 0x001290E9
		public ContentAreaListItemData()
		{
			this.m_ObjID = -1;
			this.m_ObjMngID = -1L;
		}

		// Token: 0x140000F5 RID: 245
		// (add) Token: 0x06003A90 RID: 14992 RVA: 0x0012AD00 File Offset: 0x00129100
		// (remove) Token: 0x06003A91 RID: 14993 RVA: 0x0012AD38 File Offset: 0x00129138
		public event Action<int, long> OnClick;

		// Token: 0x06003A92 RID: 14994 RVA: 0x0012AD6E File Offset: 0x0012916E
		public override void OnClickItemCallBack()
		{
			this.OnClick.Call(this.m_ObjID, this.m_ObjMngID);
		}

		// Token: 0x04004203 RID: 16899
		public int m_ObjID;

		// Token: 0x04004204 RID: 16900
		public long m_ObjMngID;

		// Token: 0x04004205 RID: 16901
		public int m_PlaceNum;
	}
}
