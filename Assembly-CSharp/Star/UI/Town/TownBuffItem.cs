﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AF3 RID: 2803
	public class TownBuffItem : MonoBehaviour
	{
		// Token: 0x06003AB4 RID: 15028 RVA: 0x0012B935 File Offset: 0x00129D35
		public void Apply(string title, string value = null)
		{
			this.m_TitleTextObj.text = title;
			if (this.m_ValueTextObj != null)
			{
				this.m_ValueTextObj.text = value;
			}
		}

		// Token: 0x0400422C RID: 16940
		[SerializeField]
		private Text m_TitleTextObj;

		// Token: 0x0400422D RID: 16941
		[SerializeField]
		private Text m_ValueTextObj;
	}
}
