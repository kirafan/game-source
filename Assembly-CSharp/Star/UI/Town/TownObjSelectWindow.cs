﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000B03 RID: 2819
	public class TownObjSelectWindow : UIGroup
	{
		// Token: 0x17000378 RID: 888
		// (get) Token: 0x06003AEC RID: 15084 RVA: 0x0012D8D3 File Offset: 0x0012BCD3
		public TownObjSelectWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x140000F6 RID: 246
		// (add) Token: 0x06003AED RID: 15085 RVA: 0x0012D8DC File Offset: 0x0012BCDC
		// (remove) Token: 0x06003AEE RID: 15086 RVA: 0x0012D914 File Offset: 0x0012BD14
		public event Action OnComplete;

		// Token: 0x06003AEF RID: 15087 RVA: 0x0012D94C File Offset: 0x0012BD4C
		public void Open(int buildPointIdx)
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.None;
			if (buildPointIdx == this.m_SelectBuildPointIdx)
			{
				return;
			}
			if (base.State != UIGroup.eState.Hide)
			{
				if (base.State == UIGroup.eState.In || base.State == UIGroup.eState.Idle)
				{
					this.Close();
				}
				this.m_ReservePointIdx = buildPointIdx;
			}
			else
			{
				this.m_ReservePointIdx = -1;
				this.m_SelectBuildPointIdx = buildPointIdx;
				UserTownData.BuildObjectData buildObjectDataAtBuildPointIndex = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(this.m_SelectBuildPointIdx);
				int num = buildObjectDataAtBuildPointIndex.m_ObjID;
				num = TownUtility.CalcAreaToContentID(num);
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num).m_ObjName);
				stringBuilder.Append("(Lv.");
				stringBuilder.Append(buildObjectDataAtBuildPointIndex.m_Lv);
				stringBuilder.Append(")");
				this.m_NameTextObj.text = stringBuilder.ToString();
				switch (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num).m_Category)
				{
				case 0:
					this.OpenRoom();
					break;
				case 1:
				case 2:
				case 5:
					this.OpenBuff();
					break;
				case 4:
				case 6:
					this.OpenContents();
					break;
				}
			}
		}

		// Token: 0x06003AF0 RID: 15088 RVA: 0x0012DAA4 File Offset: 0x0012BEA4
		private void OpenRoom()
		{
			this.m_Mode = TownObjSelectWindow.eMode.Room;
			base.gameObject.SetActive(true);
			this.m_Room.SetActive(true);
			this.m_Schedule.SetActive(true);
			this.m_Info.SetActive(false);
			this.m_LvUp.SetActive(true);
			this.m_Quick.SetActive(false);
			this.m_Store.SetActive(false);
			this.m_Move.SetActive(false);
			this.m_Swap.SetActive(false);
			this.SetUpBuildLevelButton();
			base.Open();
		}

		// Token: 0x06003AF1 RID: 15089 RVA: 0x0012DB30 File Offset: 0x0012BF30
		private void OpenBuff()
		{
			base.gameObject.SetActive(true);
			this.m_Room.SetActive(false);
			this.m_Schedule.SetActive(false);
			this.m_Info.SetActive(true);
			this.m_LvUp.SetActive(false);
			this.m_Quick.SetActive(false);
			this.m_Store.SetActive(true);
			this.m_Move.SetActive(true);
			this.m_Swap.SetActive(false);
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(this.m_SelectBuildPointIdx).m_IsOpen)
			{
				this.m_LvUp.SetActive(true);
				this.m_Quick.SetActive(false);
				this.SetUpBuildLevelButton();
				this.m_BuffStoreButton.Interactable = true;
				this.m_BuffMoveButton.Interactable = true;
				this.m_Mode = TownObjSelectWindow.eMode.BuffOpen;
			}
			else
			{
				this.m_LvUp.SetActive(false);
				this.m_Quick.SetActive(true);
				this.m_BuffStoreButton.Interactable = false;
				this.m_BuffMoveButton.Interactable = false;
				this.m_Mode = TownObjSelectWindow.eMode.BuffClose;
			}
			if (UserTownUtil.GetTownObjStockNum() >= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.FacilityLimit)
			{
				this.m_BuffStoreButton.Interactable = false;
			}
			base.Open();
		}

		// Token: 0x06003AF2 RID: 15090 RVA: 0x0012DC78 File Offset: 0x0012C078
		private void OpenContents()
		{
			this.m_Mode = TownObjSelectWindow.eMode.Contents;
			base.gameObject.SetActive(true);
			this.m_Room.SetActive(false);
			this.m_Schedule.SetActive(false);
			this.m_Info.SetActive(true);
			this.m_LvUp.SetActive(true);
			this.m_Quick.SetActive(false);
			this.m_Store.SetActive(false);
			this.m_Move.SetActive(true);
			this.m_Swap.SetActive(true);
			this.SetUpBuildLevelButton();
			base.Open();
		}

		// Token: 0x06003AF3 RID: 15091 RVA: 0x0012DD04 File Offset: 0x0012C104
		private void SetUpBuildLevelButton()
		{
			Transform transform = UIUtility.FindHrcTransform(this.m_LvUp.transform, "MaxIcon");
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(this.m_SelectBuildPointIdx).isLimitLevelUp())
			{
				this.m_LevelUpButton.Interactable = false;
				if (transform != null)
				{
					transform.gameObject.SetActive(true);
				}
			}
			else
			{
				this.m_LevelUpButton.Interactable = true;
				if (transform != null)
				{
					transform.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x06003AF4 RID: 15092 RVA: 0x0012DD98 File Offset: 0x0012C198
		public override void Close()
		{
			this.m_SelectBuildPointIdx = -1;
			this.m_ReservePointIdx = -1;
			this.m_SelectButton = TownObjSelectWindow.eButton.None;
			base.Close();
		}

		// Token: 0x06003AF5 RID: 15093 RVA: 0x0012DDB5 File Offset: 0x0012C1B5
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_ReservePointIdx != -1)
			{
				this.Open(this.m_ReservePointIdx);
			}
		}

		// Token: 0x06003AF6 RID: 15094 RVA: 0x0012DDD8 File Offset: 0x0012C1D8
		public override void Update()
		{
			base.Update();
			if (base.IsDonePlayIn && this.m_Mode == TownObjSelectWindow.eMode.BuffClose)
			{
				float num = (float)UserTownUtil.GetRemainingTime(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.GetBuildObjectDataAtBuildPointIndex(this.m_SelectBuildPointIdx).m_ManageID, -1);
				if (num <= 0f)
				{
					this.OnComplete.Call();
				}
			}
		}

		// Token: 0x06003AF7 RID: 15095 RVA: 0x0012DE3F File Offset: 0x0012C23F
		public void OnClickRoomInButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.RoomIn;
		}

		// Token: 0x06003AF8 RID: 15096 RVA: 0x0012DE48 File Offset: 0x0012C248
		public void OnClickScheduleButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Schedule;
		}

		// Token: 0x06003AF9 RID: 15097 RVA: 0x0012DE51 File Offset: 0x0012C251
		public void OnClickInfoButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Info;
		}

		// Token: 0x06003AFA RID: 15098 RVA: 0x0012DE5A File Offset: 0x0012C25A
		public void OnClickLevelUpButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.LevelUp;
		}

		// Token: 0x06003AFB RID: 15099 RVA: 0x0012DE63 File Offset: 0x0012C263
		public void OnClickQuickButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Quick;
		}

		// Token: 0x06003AFC RID: 15100 RVA: 0x0012DE6C File Offset: 0x0012C26C
		public void OnClickStoreButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Store;
		}

		// Token: 0x06003AFD RID: 15101 RVA: 0x0012DE75 File Offset: 0x0012C275
		public void OnClickMoveButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Move;
		}

		// Token: 0x06003AFE RID: 15102 RVA: 0x0012DE7E File Offset: 0x0012C27E
		public void OnClickSwapButtonCallBack()
		{
			this.m_SelectButton = TownObjSelectWindow.eButton.Swap;
		}

		// Token: 0x040042A1 RID: 17057
		private TownObjSelectWindow.eMode m_Mode;

		// Token: 0x040042A2 RID: 17058
		private TownObjSelectWindow.eButton m_SelectButton;

		// Token: 0x040042A3 RID: 17059
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x040042A4 RID: 17060
		[SerializeField]
		private GameObject m_Room;

		// Token: 0x040042A5 RID: 17061
		[SerializeField]
		private GameObject m_Schedule;

		// Token: 0x040042A6 RID: 17062
		[SerializeField]
		private GameObject m_Info;

		// Token: 0x040042A7 RID: 17063
		[SerializeField]
		private GameObject m_LvUp;

		// Token: 0x040042A8 RID: 17064
		[SerializeField]
		private GameObject m_Quick;

		// Token: 0x040042A9 RID: 17065
		[SerializeField]
		private GameObject m_Store;

		// Token: 0x040042AA RID: 17066
		[SerializeField]
		private GameObject m_Move;

		// Token: 0x040042AB RID: 17067
		[SerializeField]
		private GameObject m_Swap;

		// Token: 0x040042AC RID: 17068
		[SerializeField]
		private CustomButton m_LevelUpButton;

		// Token: 0x040042AD RID: 17069
		[SerializeField]
		private CustomButton m_BuffStoreButton;

		// Token: 0x040042AE RID: 17070
		[SerializeField]
		private CustomButton m_BuffMoveButton;

		// Token: 0x040042AF RID: 17071
		private int m_SelectBuildPointIdx = -1;

		// Token: 0x040042B0 RID: 17072
		private int m_ReservePointIdx = -1;

		// Token: 0x02000B04 RID: 2820
		private enum eMode
		{
			// Token: 0x040042B3 RID: 17075
			Room,
			// Token: 0x040042B4 RID: 17076
			BuffClose,
			// Token: 0x040042B5 RID: 17077
			BuffOpen,
			// Token: 0x040042B6 RID: 17078
			Contents
		}

		// Token: 0x02000B05 RID: 2821
		public enum eButton
		{
			// Token: 0x040042B8 RID: 17080
			None,
			// Token: 0x040042B9 RID: 17081
			RoomIn,
			// Token: 0x040042BA RID: 17082
			Schedule,
			// Token: 0x040042BB RID: 17083
			Info,
			// Token: 0x040042BC RID: 17084
			LevelUp,
			// Token: 0x040042BD RID: 17085
			Quick,
			// Token: 0x040042BE RID: 17086
			Store,
			// Token: 0x040042BF RID: 17087
			Move,
			// Token: 0x040042C0 RID: 17088
			Swap
		}
	}
}
