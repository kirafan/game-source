﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AF0 RID: 2800
	public class TownObjectCommon : MonoBehaviour
	{
		// Token: 0x06003AA9 RID: 15017 RVA: 0x0012B348 File Offset: 0x00129748
		public static TownObjectCommon CreateCommon(GameObject pobj)
		{
			TownObjectCommon townObjectCommon = pobj.GetComponent<TownObjectCommon>();
			if (townObjectCommon == null)
			{
				townObjectCommon = pobj.AddComponent<TownObjectCommon>();
			}
			return townObjectCommon;
		}

		// Token: 0x06003AAA RID: 15018 RVA: 0x0012B370 File Offset: 0x00129770
		public void SetUpView(int fobjid)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(TownUtility.CalcAreaToContentID(fobjid));
			Transform transform = TownUtility.FindTransform(base.transform, "BuildName", 4);
			if (transform != null)
			{
				Text component = transform.gameObject.GetComponent<Text>();
				if (component != null)
				{
					component.text = param.m_ObjName;
				}
			}
			transform = TownUtility.FindTransform(base.transform, "ContentName", 4);
			if (transform != null)
			{
				Text component = transform.gameObject.GetComponent<Text>();
				if (component != null)
				{
					if (param.m_TitleType != -1)
					{
						component.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB.GetParam((eTitleType)param.m_TitleType).m_DisplayName;
					}
					else
					{
						component.text = string.Empty;
					}
				}
			}
			transform = TownUtility.FindTransform(base.transform, "IconPoint", 4);
			if (transform != null)
			{
				PrefabCloner component2 = transform.gameObject.GetComponent<PrefabCloner>();
				if (component2 != null)
				{
					component2.GetInst<TownObjectIconWithFrame>().Apply(TownUtility.CalcAreaToContentID(fobjid), 1);
				}
			}
		}
	}
}
