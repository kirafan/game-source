﻿using System;
using UnityEngine;

namespace Star.UI.Town
{
	// Token: 0x02000AE8 RID: 2792
	public class BuildTopWindow : UIGroup
	{
		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06003A85 RID: 14981 RVA: 0x0012AABF File Offset: 0x00128EBF
		public BuildTopWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x06003A86 RID: 14982 RVA: 0x0012AAC8 File Offset: 0x00128EC8
		public override void Open()
		{
			base.Open();
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.SetEnableDark(true, 1f, true);
				tutorialMessage.SetEnableArrow(true);
				tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				tutorialMessage.SetArrowPosition(this.m_TutorialTarget.GetComponent<RectTransform>().position);
				this.m_TutorialTarget.SetEnable(true);
			}
		}

		// Token: 0x06003A87 RID: 14983 RVA: 0x0012AB37 File Offset: 0x00128F37
		public void OnButtonCallBack(int buttonIdx)
		{
			this.m_SelectButton = (BuildTopWindow.eButton)buttonIdx;
			this.Close();
		}

		// Token: 0x06003A88 RID: 14984 RVA: 0x0012AB46 File Offset: 0x00128F46
		public void OnCancelButtonCallBack()
		{
			this.m_SelectButton = BuildTopWindow.eButton.Cancel;
			this.Close();
		}

		// Token: 0x040041F6 RID: 16886
		private BuildTopWindow.eButton m_SelectButton;

		// Token: 0x040041F7 RID: 16887
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x02000AE9 RID: 2793
		public enum eButton
		{
			// Token: 0x040041F9 RID: 16889
			Area,
			// Token: 0x040041FA RID: 16890
			Product,
			// Token: 0x040041FB RID: 16891
			Buff,
			// Token: 0x040041FC RID: 16892
			Have,
			// Token: 0x040041FD RID: 16893
			Cancel
		}
	}
}
