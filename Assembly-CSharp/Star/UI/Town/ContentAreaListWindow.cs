﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Town
{
	// Token: 0x02000AEC RID: 2796
	public class ContentAreaListWindow : UIGroup
	{
		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06003A94 RID: 14996 RVA: 0x0012ADB0 File Offset: 0x001291B0
		public ContentAreaListWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06003A95 RID: 14997 RVA: 0x0012ADB8 File Offset: 0x001291B8
		public int SelectObjID
		{
			get
			{
				return this.m_SelectObjID;
			}
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06003A96 RID: 14998 RVA: 0x0012ADC0 File Offset: 0x001291C0
		public long SelectObjMngID
		{
			get
			{
				return this.m_SelectObjMngID;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06003A97 RID: 14999 RVA: 0x0012ADC8 File Offset: 0x001291C8
		public bool IsSwapMode
		{
			get
			{
				return this.m_IsSwapMode;
			}
		}

		// Token: 0x06003A98 RID: 15000 RVA: 0x0012ADD0 File Offset: 0x001291D0
		public void Setup()
		{
			if (this.m_Scroll != null)
			{
				this.m_Scroll.Setup();
			}
		}

		// Token: 0x06003A99 RID: 15001 RVA: 0x0012ADF0 File Offset: 0x001291F0
		public void Open(bool swapMode)
		{
			base.Open();
			this.m_IsSwapMode = swapMode;
			if (swapMode)
			{
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentSwapTitle);
				this.m_MessageText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentSwapQuestion);
			}
			else
			{
				this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentTitle);
				this.m_MessageText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TownBuildContentQuestion);
			}
			this.m_TownBuildList.Open(TownBuildList.eCategory.Area);
			base.gameObject.SetActive(true);
			this.PrepareStart();
			this.m_IsDonePrepare = false;
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsTownTutorial())
			{
				this.m_CancelButton.Interactable = false;
			}
			else
			{
				this.m_CancelButton.Interactable = true;
			}
		}

		// Token: 0x06003A9A RID: 15002 RVA: 0x0012AEE7 File Offset: 0x001292E7
		public override void Open()
		{
			this.Open(false);
		}

		// Token: 0x06003A9B RID: 15003 RVA: 0x0012AEF0 File Offset: 0x001292F0
		public override void Update()
		{
			base.Update();
			if (!this.m_IsDonePrepare)
			{
				if (this.m_TownBuildList.IsAvailable)
				{
					this.Apply();
					base.Open();
					this.m_IsDonePrepare = true;
				}
				else
				{
					this.m_TownBuildList.UpdateOpen();
				}
			}
		}

		// Token: 0x06003A9C RID: 15004 RVA: 0x0012AF42 File Offset: 0x00129342
		public override void OnFinishPlayOut()
		{
			base.OnFinishPlayOut();
			this.m_Scroll.Destroy();
		}

		// Token: 0x06003A9D RID: 15005 RVA: 0x0012AF55 File Offset: 0x00129355
		public void OnClickItemCallBack(int objID, long objMngID)
		{
			this.m_SelectButton = ContentAreaListWindow.eButton.Build;
			this.m_SelectObjID = objID;
			this.m_SelectObjMngID = objMngID;
			this.Close();
		}

		// Token: 0x06003A9E RID: 15006 RVA: 0x0012AF72 File Offset: 0x00129372
		public void OnBackButtonCallBack()
		{
			this.m_SelectButton = ContentAreaListWindow.eButton.Back;
			this.m_SelectObjID = -1;
			this.m_SelectObjMngID = -1L;
			this.Close();
		}

		// Token: 0x06003A9F RID: 15007 RVA: 0x0012AF90 File Offset: 0x00129390
		public void OnCancelButtonCallBack()
		{
			this.m_SelectButton = ContentAreaListWindow.eButton.Cancel;
			this.m_SelectObjID = -1;
			this.m_SelectObjMngID = -1L;
			this.Close();
		}

		// Token: 0x06003AA0 RID: 15008 RVA: 0x0012AFB0 File Offset: 0x001293B0
		private void Apply()
		{
			this.m_Scroll.RemoveAll();
			for (int i = 0; i < this.m_TownBuildList.GetItemNum(); i++)
			{
				ContentAreaListItemData contentAreaListItemData = new ContentAreaListItemData();
				contentAreaListItemData.m_ObjID = this.m_TownBuildList.GetItem(i).objID;
				contentAreaListItemData.m_ObjMngID = this.m_TownBuildList.GetItem(i).mngID;
				contentAreaListItemData.m_PlaceNum = this.m_TownBuildList.GetItem(i).buildNum;
				contentAreaListItemData.OnClick += this.OnClickItemCallBack;
				this.m_Scroll.AddItem(contentAreaListItemData);
			}
		}

		// Token: 0x04004207 RID: 16903
		private ContentAreaListWindow.eButton m_SelectButton = ContentAreaListWindow.eButton.Cancel;

		// Token: 0x04004208 RID: 16904
		private int m_SelectObjID = -1;

		// Token: 0x04004209 RID: 16905
		private long m_SelectObjMngID = -1L;

		// Token: 0x0400420A RID: 16906
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x0400420B RID: 16907
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x0400420C RID: 16908
		[SerializeField]
		private Text m_MessageText;

		// Token: 0x0400420D RID: 16909
		[SerializeField]
		private CustomButton m_CancelButton;

		// Token: 0x0400420E RID: 16910
		private TownBuildList m_TownBuildList = new TownBuildList();

		// Token: 0x0400420F RID: 16911
		private bool m_IsDonePrepare;

		// Token: 0x04004210 RID: 16912
		private bool m_IsSwapMode;

		// Token: 0x02000AED RID: 2797
		public enum eButton
		{
			// Token: 0x04004212 RID: 16914
			Build,
			// Token: 0x04004213 RID: 16915
			Cancel,
			// Token: 0x04004214 RID: 16916
			Back
		}
	}
}
