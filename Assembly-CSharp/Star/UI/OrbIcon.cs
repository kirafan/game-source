﻿using System;

namespace Star.UI
{
	// Token: 0x0200086A RID: 2154
	public class OrbIcon : ASyncImage
	{
		// Token: 0x06002CB2 RID: 11442 RVA: 0x000EBE64 File Offset: 0x000EA264
		public void Apply(int orbID)
		{
			if (this.m_OrbID != orbID)
			{
				this.Apply();
				if (orbID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncOrbIcon(orbID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_OrbID = orbID;
			}
		}

		// Token: 0x06002CB3 RID: 11443 RVA: 0x000EBECA File Offset: 0x000EA2CA
		public override void Destroy()
		{
			base.Destroy();
			this.m_OrbID = -1;
		}

		// Token: 0x040033AE RID: 13230
		protected int m_OrbID = -1;
	}
}
