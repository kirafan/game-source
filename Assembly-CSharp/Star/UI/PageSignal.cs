﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000886 RID: 2182
	public class PageSignal : MonoBehaviour
	{
		// Token: 0x1400006B RID: 107
		// (add) Token: 0x06002D4C RID: 11596 RVA: 0x000EF6F8 File Offset: 0x000EDAF8
		// (remove) Token: 0x06002D4D RID: 11597 RVA: 0x000EF730 File Offset: 0x000EDB30
		public event Action<int> OnClickButton;

		// Token: 0x06002D4E RID: 11598 RVA: 0x000EF766 File Offset: 0x000EDB66
		private void Start()
		{
			this.m_Signal.gameObject.SetActive(false);
		}

		// Token: 0x06002D4F RID: 11599 RVA: 0x000EF77C File Offset: 0x000EDB7C
		public void SetPageMax(int max)
		{
			this.m_Max = max;
			if (this.m_SignalList.Count >= max)
			{
				for (int i = 0; i < this.m_SignalList.Count; i++)
				{
					this.m_SignalList[i].gameObject.SetActive(i < max);
				}
			}
		}

		// Token: 0x06002D50 RID: 11600 RVA: 0x000EF7D8 File Offset: 0x000EDBD8
		public void SetPage(int currentPage)
		{
			this.m_Current = currentPage;
			for (int i = 0; i < this.m_SignalList.Count; i++)
			{
				if (currentPage != i)
				{
					this.m_SignalList[i].SetSprite(this.m_SpriteOff);
				}
				else
				{
					this.m_SignalList[i].SetSprite(this.m_SpriteOn);
				}
			}
		}

		// Token: 0x06002D51 RID: 11601 RVA: 0x000EF844 File Offset: 0x000EDC44
		private void Update()
		{
			if (this.m_SignalList.Count < this.m_Max)
			{
				RectTransform component = base.GetComponent<RectTransform>();
				while (this.m_SignalList.Count < this.m_Max)
				{
					PageSignalButton pageSignalButton = UnityEngine.Object.Instantiate<PageSignalButton>(this.m_Signal, component);
					this.m_SignalList.Add(pageSignalButton);
					pageSignalButton.gameObject.SetActive(true);
					pageSignalButton.Idx = this.m_SignalList.Count - 1;
					pageSignalButton.OnClick += this.OnClickSignalButton;
					if (this.m_Current == this.m_SignalList.Count - 1)
					{
						pageSignalButton.SetSprite(this.m_SpriteOn);
					}
					else
					{
						pageSignalButton.SetSprite(this.m_SpriteOff);
					}
				}
			}
		}

		// Token: 0x06002D52 RID: 11602 RVA: 0x000EF908 File Offset: 0x000EDD08
		public void OnClickSignalButton(int idx)
		{
			this.OnClickButton.Call(idx);
		}

		// Token: 0x0400345D RID: 13405
		[SerializeField]
		private Sprite m_SpriteOff;

		// Token: 0x0400345E RID: 13406
		[SerializeField]
		private Sprite m_SpriteOn;

		// Token: 0x0400345F RID: 13407
		[SerializeField]
		private PageSignalButton m_Signal;

		// Token: 0x04003460 RID: 13408
		private List<PageSignalButton> m_SignalList = new List<PageSignalButton>();

		// Token: 0x04003461 RID: 13409
		private int m_Max;

		// Token: 0x04003462 RID: 13410
		private int m_Current;
	}
}
