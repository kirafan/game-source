﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000AC5 RID: 2757
	[ExecuteInEditMode]
	public class SoftMask : MonoBehaviour
	{
		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06003996 RID: 14742 RVA: 0x0012557B File Offset: 0x0012397B
		private Material material
		{
			get
			{
				return this.m_mat;
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06003997 RID: 14743 RVA: 0x00125583 File Offset: 0x00123983
		private Sprite maskSprite
		{
			get
			{
				return this.m_maskImage.sprite;
			}
		}

		// Token: 0x06003998 RID: 14744 RVA: 0x00125590 File Offset: 0x00123990
		private void Start()
		{
			if (this.m_SoftMaskMaterial != null)
			{
				this.ApplyMaterial(this.m_SoftMaskMaterial);
			}
		}

		// Token: 0x06003999 RID: 14745 RVA: 0x001255AF File Offset: 0x001239AF
		public void Destroy()
		{
			if (this.m_mat != null)
			{
				UnityEngine.Object.Destroy(this.m_mat);
				this.m_mat = null;
			}
			this.m_init = false;
		}

		// Token: 0x0600399A RID: 14746 RVA: 0x001255DB File Offset: 0x001239DB
		private void OnDestory()
		{
			if (this.m_mat != null)
			{
				UnityEngine.Object.Destroy(this.m_mat);
				this.m_mat = null;
				this.m_image.material = null;
			}
			this.m_init = false;
		}

		// Token: 0x0600399B RID: 14747 RVA: 0x00125614 File Offset: 0x00123A14
		public void ApplyMaterial(Material softMaskMaterial)
		{
			this.m_myRect = base.GetComponent<RectTransform>();
			this.m_image = base.GetComponent<Image>();
			if (this.m_image)
			{
				this.m_image.material = (this.m_mat = new Material(softMaskMaterial));
			}
			if (base.GetComponent<Text>())
			{
				this.m_isText = true;
				this.m_image.material = (this.m_mat = new Material(softMaskMaterial));
				this.GetCanvas();
				if (base.transform.parent.GetComponent<Mask>() == null)
				{
					base.transform.parent.gameObject.AddComponent<Mask>();
				}
				base.transform.parent.GetComponent<Mask>().enabled = false;
			}
			if (this.m_maskImage != null)
			{
				this.Init(this.m_maskImage);
			}
		}

		// Token: 0x0600399C RID: 14748 RVA: 0x001256FE File Offset: 0x00123AFE
		public void Init(Image img)
		{
			this.m_maskImage = img;
			this.m_maskArea = img.GetComponent<RectTransform>();
			this.m_init = true;
		}

		// Token: 0x0600399D RID: 14749 RVA: 0x0012571C File Offset: 0x00123B1C
		private void GetCanvas()
		{
			Transform transform = base.transform;
			int num = 100;
			int num2 = 0;
			while (this.m_canvas == null && num2 < num)
			{
				this.m_canvas = transform.gameObject.GetComponent<Canvas>();
				if (this.m_canvas == null)
				{
					transform = this.GetParentTransform(transform);
				}
				num2++;
			}
		}

		// Token: 0x0600399E RID: 14750 RVA: 0x00125780 File Offset: 0x00123B80
		private Transform GetParentTransform(Transform t)
		{
			return t.parent;
		}

		// Token: 0x0600399F RID: 14751 RVA: 0x00125788 File Offset: 0x00123B88
		private void LateUpdate()
		{
			this.SetMask();
		}

		// Token: 0x060039A0 RID: 14752 RVA: 0x00125790 File Offset: 0x00123B90
		private void SetMask()
		{
			if (!this.m_init)
			{
				return;
			}
			this.m_maskRect = this.m_maskArea.rect;
			this.m_contentRect = this.m_myRect.rect;
			if (this.m_isText)
			{
				if (this.m_canvas.renderMode == RenderMode.ScreenSpaceOverlay)
				{
					this.m_p = this.m_canvas.transform.InverseTransformPoint(this.m_maskArea.transform.position);
					this.m_siz = new Vector2(this.m_maskRect.width, this.m_maskRect.height);
				}
				else
				{
					Vector3[] array = new Vector3[4];
					this.m_maskArea.GetWorldCorners(array);
					this.m_siz = array[2] - array[0];
					this.m_p = this.m_maskArea.transform.position;
				}
				this.m_min = this.m_p - new Vector2(this.m_siz.x, this.m_siz.y) * 0.5f + this.m_myRect.pivot;
				this.m_max = this.m_p + new Vector2(this.m_siz.x, this.m_siz.y) * 0.5f + this.m_myRect.pivot;
			}
			else
			{
				this.m_center = this.m_myRect.transform.InverseTransformPoint(this.m_maskArea.transform.position);
				this.m_alphaUV = new Vector2(this.m_maskRect.width / this.m_contentRect.width, this.m_maskRect.height / this.m_contentRect.height);
				this.m_min = this.m_center;
				this.m_max = this.m_min;
				this.m_siz = new Vector2(this.m_maskRect.width, this.m_maskRect.height) * 0.5f;
				this.m_min -= this.m_siz;
				this.m_max += this.m_siz;
				this.m_min = new Vector2(this.m_min.x / this.m_contentRect.width, this.m_min.y / this.m_contentRect.height) + new Vector2(0.5f, 0.5f);
				this.m_max = new Vector2(this.m_max.x / this.m_contentRect.width, this.m_max.y / this.m_contentRect.height) + new Vector2(0.5f, 0.5f);
				if (this.m_image.sprite != null)
				{
					this.m_atlasUV.x = this.m_image.sprite.rect.x / (float)this.m_image.mainTexture.width;
					this.m_atlasUV.y = this.m_image.sprite.rect.y / (float)this.m_image.mainTexture.height;
					this.m_atlasUV.z = this.m_image.sprite.rect.width / (float)this.m_image.mainTexture.width;
					this.m_atlasUV.w = this.m_image.sprite.rect.height / (float)this.m_image.mainTexture.height;
				}
				else
				{
					this.m_atlasUV.x = 0f;
					this.m_atlasUV.y = 0f;
					this.m_atlasUV.z = 1f;
					this.m_atlasUV.w = 1f;
				}
			}
			this.m_maskAtlasUV.x = this.maskSprite.rect.x / (float)this.maskSprite.texture.width;
			this.m_maskAtlasUV.y = this.maskSprite.rect.y / (float)this.maskSprite.texture.height;
			this.m_maskAtlasUV.z = this.maskSprite.rect.width / (float)this.maskSprite.texture.width;
			this.m_maskAtlasUV.w = this.maskSprite.rect.height / (float)this.maskSprite.texture.height;
			this.material.SetVector("_Min", this.m_min);
			this.material.SetVector("_Max", this.m_max);
			this.material.SetTexture("_AlphaMask", this.maskSprite.texture);
			if (!this.m_isText)
			{
				this.material.SetVector("_AlphaUV", this.m_alphaUV);
				this.material.SetVector("_AtlasUV", this.m_atlasUV);
			}
			this.material.SetVector("_MaskAtlasUV", this.m_maskAtlasUV);
			this.material.SetFloat("_CutOff", this.m_cutOff);
		}

		// Token: 0x040040C9 RID: 16585
		[SerializeField]
		private Material m_SoftMaskMaterial;

		// Token: 0x040040CA RID: 16586
		public Image m_maskImage;

		// Token: 0x040040CB RID: 16587
		[Range(0f, 1f)]
		public float m_cutOff;

		// Token: 0x040040CC RID: 16588
		private Material m_mat;

		// Token: 0x040040CD RID: 16589
		private Canvas m_canvas;

		// Token: 0x040040CE RID: 16590
		private RectTransform m_myRect;

		// Token: 0x040040CF RID: 16591
		private RectTransform m_maskArea;

		// Token: 0x040040D0 RID: 16592
		private Vector2 m_alphaUV;

		// Token: 0x040040D1 RID: 16593
		private Vector2 m_min;

		// Token: 0x040040D2 RID: 16594
		private Vector2 m_max = Vector2.zero;

		// Token: 0x040040D3 RID: 16595
		private Vector2 m_p;

		// Token: 0x040040D4 RID: 16596
		private Vector2 m_siz;

		// Token: 0x040040D5 RID: 16597
		private Rect m_maskRect;

		// Token: 0x040040D6 RID: 16598
		private Rect m_contentRect;

		// Token: 0x040040D7 RID: 16599
		private Vector4 m_atlasUV;

		// Token: 0x040040D8 RID: 16600
		private Vector4 m_maskAtlasUV;

		// Token: 0x040040D9 RID: 16601
		private Vector2 m_center;

		// Token: 0x040040DA RID: 16602
		private bool m_isText;

		// Token: 0x040040DB RID: 16603
		private Image m_image;

		// Token: 0x040040DC RID: 16604
		private bool m_init;
	}
}
