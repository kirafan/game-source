﻿using System;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000894 RID: 2196
	public class InvisibleGraphic : Graphic
	{
		// Token: 0x06002D99 RID: 11673 RVA: 0x000F0A46 File Offset: 0x000EEE46
		protected override void OnPopulateMesh(VertexHelper vh)
		{
			base.OnPopulateMesh(vh);
			vh.Clear();
		}
	}
}
