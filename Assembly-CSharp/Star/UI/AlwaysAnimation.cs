﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007ED RID: 2029
	public class AlwaysAnimation : MonoBehaviour
	{
		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x060029E6 RID: 10726 RVA: 0x000DD0FB File Offset: 0x000DB4FB
		private Animation Animation
		{
			get
			{
				if (this.m_Animation == null)
				{
					this.m_Animation = base.GetComponent<Animation>();
				}
				return this.m_Animation;
			}
		}

		// Token: 0x060029E7 RID: 10727 RVA: 0x000DD120 File Offset: 0x000DB520
		private void Update()
		{
			if (this.Animation.isPlaying)
			{
				if (Time.timeScale == 0f)
				{
					if (this.m_BeforeTime == -1f)
					{
						this.m_BeforeTime = Time.realtimeSinceStartup;
					}
					this.Animation[this.Animation.clip.name].time += Time.realtimeSinceStartup - this.m_BeforeTime;
					this.m_BeforeTime = Time.realtimeSinceStartup;
				}
			}
			else
			{
				this.m_BeforeTime = -1f;
			}
		}

		// Token: 0x04003046 RID: 12358
		private Animation m_Animation;

		// Token: 0x04003047 RID: 12359
		private float m_BeforeTime = -1f;
	}
}
