﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000878 RID: 2168
	public class VariableIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002CE0 RID: 11488 RVA: 0x000ECB94 File Offset: 0x000EAF94
		private void DestroyAllIcon()
		{
			this.m_CharaIconWithFrameCloner.gameObject.SetActive(false);
			this.m_ItemIconWithFrameCloner.gameObject.SetActive(false);
			this.m_WeaponIconWithFrameCloner.gameObject.SetActive(false);
			this.m_TownObjectIconWithFrameCloner.gameObject.SetActive(false);
			this.m_RoomObjectIconWithFrameCloner.gameObject.SetActive(false);
			this.m_MasterOrbIconWithFrameCloner.gameObject.SetActive(false);
			this.m_GoldCloner.gameObject.SetActive(false);
			this.m_GemCloner.gameObject.SetActive(false);
			this.m_KPCloner.gameObject.SetActive(false);
		}

		// Token: 0x06002CE1 RID: 11489 RVA: 0x000ECC3A File Offset: 0x000EB03A
		private void ResetParam()
		{
			this.m_Type = VariableIconWithFrame.eType.None;
			this.m_ID = -1;
			this.m_SubID = -1;
			this.m_MngID = -1L;
		}

		// Token: 0x06002CE2 RID: 11490 RVA: 0x000ECC59 File Offset: 0x000EB059
		public void Destroy()
		{
			this.DestroyAllIcon();
		}

		// Token: 0x06002CE3 RID: 11491 RVA: 0x000ECC64 File Offset: 0x000EB064
		public void ApplyChara(int charaID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.Chara || this.m_ID != charaID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.Chara;
				this.m_ID = charaID;
				this.m_CharaIconWithFrameCloner.gameObject.SetActive(true);
				this.m_CharaIconWithFrameCloner.GetInst<CharaIconWithFrame>().ApplyCharaID(charaID);
			}
		}

		// Token: 0x06002CE4 RID: 11492 RVA: 0x000ECCC8 File Offset: 0x000EB0C8
		public void ApplyCharaMngID(long charaMngID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.Chara || this.m_MngID != charaMngID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.Chara;
				this.m_MngID = charaMngID;
				this.m_CharaIconWithFrameCloner.gameObject.SetActive(true);
				this.m_CharaIconWithFrameCloner.GetInst<CharaIconWithFrame>().ApplyCharaMngID(charaMngID);
			}
		}

		// Token: 0x06002CE5 RID: 11493 RVA: 0x000ECD2C File Offset: 0x000EB12C
		public void ApplyItem(int itemID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.Item || this.m_ID != itemID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.Item;
				this.m_ID = itemID;
				this.m_ItemIconWithFrameCloner.gameObject.SetActive(true);
				this.m_ItemIconWithFrameCloner.GetInst<ItemIconWithFrame>().Apply(itemID);
			}
		}

		// Token: 0x06002CE6 RID: 11494 RVA: 0x000ECD90 File Offset: 0x000EB190
		public void ApplyWeapon(int weaponID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.Weapon || this.m_ID != weaponID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.Weapon;
				this.m_ID = weaponID;
				this.m_WeaponIconWithFrameCloner.gameObject.SetActive(true);
				this.m_WeaponIconWithFrameCloner.GetInst<WeaponIconWithFrame>().Apply(weaponID);
			}
		}

		// Token: 0x06002CE7 RID: 11495 RVA: 0x000ECDF4 File Offset: 0x000EB1F4
		public void ApplyTownObject(int townObjectID, int lv = 1)
		{
			if (this.m_Type != VariableIconWithFrame.eType.TownObject || this.m_ID != townObjectID || this.m_SubID != lv)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.TownObject;
				this.m_ID = townObjectID;
				this.m_SubID = lv;
				this.m_TownObjectIconWithFrameCloner.gameObject.SetActive(true);
				this.m_TownObjectIconWithFrameCloner.GetInst<TownObjectIconWithFrame>().Apply(townObjectID, lv);
			}
		}

		// Token: 0x06002CE8 RID: 11496 RVA: 0x000ECE6C File Offset: 0x000EB26C
		public void ApplyRoomObject(eRoomObjectCategory category, int roomObjectID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.RoomObject || this.m_ID != (int)category || this.m_SubID != roomObjectID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.RoomObject;
				this.m_ID = (int)category;
				this.m_SubID = roomObjectID;
				this.m_RoomObjectIconWithFrameCloner.gameObject.SetActive(true);
				this.m_RoomObjectIconWithFrameCloner.GetInst<RoomObjectIconWithFrame>().Apply(category, roomObjectID);
			}
		}

		// Token: 0x06002CE9 RID: 11497 RVA: 0x000ECEE4 File Offset: 0x000EB2E4
		public void ApplyRoomObject(int accessKey)
		{
			RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(accessKey);
			this.ApplyRoomObject((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
		}

		// Token: 0x06002CEA RID: 11498 RVA: 0x000ECF0C File Offset: 0x000EB30C
		public void ApplyOrb(int orbID)
		{
			if (this.m_Type != VariableIconWithFrame.eType.Orb || this.m_ID != orbID)
			{
				this.DestroyAllIcon();
				this.ResetParam();
				this.m_Type = VariableIconWithFrame.eType.Orb;
				this.m_ID = orbID;
				this.m_MasterOrbIconWithFrameCloner.gameObject.SetActive(true);
				this.m_MasterOrbIconWithFrameCloner.GetInst<OrbIconWithFrame>().Apply(orbID);
			}
		}

		// Token: 0x06002CEB RID: 11499 RVA: 0x000ECF6D File Offset: 0x000EB36D
		public void ApplyGold()
		{
			this.DestroyAllIcon();
			this.m_GoldCloner.Setup(false);
			this.m_GoldCloner.gameObject.SetActive(true);
			this.m_Type = VariableIconWithFrame.eType.Gold;
		}

		// Token: 0x06002CEC RID: 11500 RVA: 0x000ECF99 File Offset: 0x000EB399
		public void ApplyGem()
		{
			this.DestroyAllIcon();
			this.m_GemCloner.Setup(false);
			this.m_GemCloner.gameObject.SetActive(true);
			this.m_Type = VariableIconWithFrame.eType.Gem;
		}

		// Token: 0x06002CED RID: 11501 RVA: 0x000ECFC5 File Offset: 0x000EB3C5
		public void ApplyKirara()
		{
			this.DestroyAllIcon();
			this.m_KPCloner.Setup(false);
			this.m_KPCloner.gameObject.SetActive(true);
			this.m_Type = VariableIconWithFrame.eType.Kirara;
		}

		// Token: 0x06002CEE RID: 11502 RVA: 0x000ECFF4 File Offset: 0x000EB3F4
		public void ApplyPresent(ePresentType presentType, int id)
		{
			switch (presentType)
			{
			case ePresentType.Chara:
				this.ApplyChara(id);
				break;
			case ePresentType.Item:
				this.ApplyItem(id);
				break;
			case ePresentType.Weapon:
				this.ApplyWeapon(id);
				break;
			case ePresentType.TownFacility:
				this.ApplyTownObject(id, 1);
				break;
			case ePresentType.RoomObject:
				this.ApplyRoomObject(id);
				break;
			case ePresentType.MasterOrb:
				this.ApplyOrb(id);
				break;
			case ePresentType.KRRPoint:
				this.ApplyKirara();
				break;
			case ePresentType.Gold:
				this.ApplyGold();
				break;
			case ePresentType.UnlimitedGem:
			case ePresentType.LimitedGem:
				this.ApplyGem();
				break;
			}
		}

		// Token: 0x040033E5 RID: 13285
		[SerializeField]
		private PrefabCloner m_CharaIconWithFrameCloner;

		// Token: 0x040033E6 RID: 13286
		[SerializeField]
		private PrefabCloner m_ItemIconWithFrameCloner;

		// Token: 0x040033E7 RID: 13287
		[SerializeField]
		private PrefabCloner m_WeaponIconWithFrameCloner;

		// Token: 0x040033E8 RID: 13288
		[SerializeField]
		private PrefabCloner m_TownObjectIconWithFrameCloner;

		// Token: 0x040033E9 RID: 13289
		[SerializeField]
		private PrefabCloner m_RoomObjectIconWithFrameCloner;

		// Token: 0x040033EA RID: 13290
		[SerializeField]
		private PrefabCloner m_MasterOrbIconWithFrameCloner;

		// Token: 0x040033EB RID: 13291
		[SerializeField]
		private PrefabCloner m_GoldCloner;

		// Token: 0x040033EC RID: 13292
		[SerializeField]
		private PrefabCloner m_GemCloner;

		// Token: 0x040033ED RID: 13293
		[SerializeField]
		private PrefabCloner m_KPCloner;

		// Token: 0x040033EE RID: 13294
		private VariableIconWithFrame.eType m_Type;

		// Token: 0x040033EF RID: 13295
		private int m_ID = -1;

		// Token: 0x040033F0 RID: 13296
		private int m_SubID = -1;

		// Token: 0x040033F1 RID: 13297
		private long m_MngID = -1L;

		// Token: 0x02000879 RID: 2169
		public enum eType
		{
			// Token: 0x040033F3 RID: 13299
			None,
			// Token: 0x040033F4 RID: 13300
			Chara,
			// Token: 0x040033F5 RID: 13301
			Item,
			// Token: 0x040033F6 RID: 13302
			Weapon,
			// Token: 0x040033F7 RID: 13303
			TownObject,
			// Token: 0x040033F8 RID: 13304
			RoomObject,
			// Token: 0x040033F9 RID: 13305
			Orb,
			// Token: 0x040033FA RID: 13306
			Gem,
			// Token: 0x040033FB RID: 13307
			Gold,
			// Token: 0x040033FC RID: 13308
			Kirara
		}
	}
}
