﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A4D RID: 2637
	public class QuestChapterSelectUI : MenuUIBase
	{
		// Token: 0x1700033A RID: 826
		// (get) Token: 0x060036DC RID: 14044 RVA: 0x001155AB File Offset: 0x001139AB
		public QuestChapterSelectUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x060036DD RID: 14045 RVA: 0x001155B3 File Offset: 0x001139B3
		public int SelectChapterIdx
		{
			get
			{
				return this.m_SelectChapterIdx;
			}
		}

		// Token: 0x060036DE RID: 14046 RVA: 0x001155BB File Offset: 0x001139BB
		private void Awake()
		{
			this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
		}

		// Token: 0x060036DF RID: 14047 RVA: 0x001155C9 File Offset: 0x001139C9
		private void Start()
		{
		}

		// Token: 0x060036E0 RID: 14048 RVA: 0x001155CB File Offset: 0x001139CB
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x060036E1 RID: 14049 RVA: 0x001155D4 File Offset: 0x001139D4
		public void Setup(int chapterID, bool playOpenAnim = false)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_Scroll.Setup();
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			int num = questMng.GetLastChapterIndex();
			if (questMng.QuestGetAllApiTypeIsDebugFullOpen())
			{
				num = questMng.GetChapterNum() - 1;
			}
			int num2 = 0;
			for (int i = 0; i < num + 1; i++)
			{
				QuestManager.ChapterData chapterDataAt = questMng.GetChapterDataAt(i, QuestDefine.eDifficulty.Normal);
				if (chapterDataAt == null)
				{
					break;
				}
				ChapterPanel.ChapterData chapterData = default(ChapterPanel.ChapterData);
				chapterData.m_ChapterID = chapterDataAt.m_ChapterID;
				chapterData.m_IsClear = questMng.IsClearChapterAt(i);
				chapterData.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncQuestChapterIcon(i);
				if (chapterData.m_ChapterID == chapterID)
				{
					num2 = i;
				}
				chapterData.m_ClearPercents = new int[3];
				for (int j = 0; j < 3; j++)
				{
					chapterData.m_ClearPercents[j] = questMng.CalcChapterClearRatio(chapterData.m_ChapterID, (QuestDefine.eDifficulty)j);
				}
				this.m_List.Add(chapterData);
				this.m_Scroll.AddItem(chapterData);
			}
			this.m_Scroll.OnClickChapterButton += this.OnClickChapterButtonCallBack;
			this.m_ChapterOpenAnim.Initialize();
			if (playOpenAnim)
			{
				this.m_ChapterOpenAnim.SetUpFirst();
				this.m_OpenChapterIdx = num;
				if (num2 == this.m_OpenChapterIdx)
				{
					num2--;
				}
			}
			this.m_Scroll.SetSelectChapterIdx(num2);
		}

		// Token: 0x060036E2 RID: 14050 RVA: 0x00115770 File Offset: 0x00113B70
		public override void Destroy()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
				for (int i = 0; i < this.m_List.Count; i++)
				{
					spriteMng.Unload(this.m_List[i].m_SpriteHandler);
				}
			}
			if (this.m_ChapterPanelClone != null)
			{
				UnityEngine.Object.Destroy(this.m_ChapterPanelClone);
			}
		}

		// Token: 0x060036E3 RID: 14051 RVA: 0x001157EC File Offset: 0x00113BEC
		private void Update()
		{
			switch (this.m_Step)
			{
			case QuestChapterSelectUI.eStep.LoadWait:
			{
				bool flag = true;
				for (int i = 0; i < this.m_List.Count; i++)
				{
					if (!this.m_List[i].m_SpriteHandler.IsAvailable())
					{
						flag = false;
					}
				}
				if (flag)
				{
					this.ChangeStep(QuestChapterSelectUI.eStep.In);
					for (int j = 0; j < this.m_AnimArray.Length; j++)
					{
						this.m_AnimArray[j].PlayIn();
					}
					for (int k = 0; k < this.m_UIGroups.Length; k++)
					{
						this.m_UIGroups[k].Open();
					}
				}
				break;
			}
			case QuestChapterSelectUI.eStep.In:
			{
				if (this.m_Scroll.GetExistChapterPanelNum() == 0)
				{
					return;
				}
				if (this.m_OpenChapterIdx != -1 && !this.m_ChapterOpenAnim.IsDoneSetTarget())
				{
					ChapterPanel chapterPanel = this.m_Scroll.GetChapterPanel(this.m_OpenChapterIdx);
					if (chapterPanel != null)
					{
						this.m_ChapterOpenAnim.SetChapterPanelObj(chapterPanel);
						SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
					}
				}
				bool flag2 = true;
				for (int l = 0; l < this.m_AnimArray.Length; l++)
				{
					if (!this.m_AnimArray[l].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				for (int m = 0; m < this.m_UIGroups.Length; m++)
				{
					if (!this.m_UIGroups[m].IsDonePlayIn)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.m_CanvasGroup.interactable = true;
					if (this.m_ChapterOpenAnim.GetChapterPanelObj() != null)
					{
						this.ChangeStep(QuestChapterSelectUI.eStep.OpenAnimStart);
					}
					else
					{
						this.ChangeStep(QuestChapterSelectUI.eStep.Idle);
					}
				}
				break;
			}
			case QuestChapterSelectUI.eStep.OpenAnimStart:
				this.m_ChapterOpenAnim.GetComponent<RectTransform>().SetAsLastSibling();
				this.m_Scroll.SetCurrentIdx(this.m_OpenChapterIdx, false);
				this.m_ChapterOpenAnim.Play();
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				this.ChangeStep(QuestChapterSelectUI.eStep.OpenAnimWait);
				break;
			case QuestChapterSelectUI.eStep.OpenAnimWait:
				if (this.m_Delay > 0f)
				{
					this.m_Delay -= Time.deltaTime;
					if (this.m_Delay <= 0f)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_CHAPTER_OPEN, 1f, 0, -1, -1);
					}
				}
				if (!this.m_ChapterOpenAnim.IsPlaying())
				{
					this.ChangeStep(QuestChapterSelectUI.eStep.Idle);
				}
				break;
			case QuestChapterSelectUI.eStep.Out:
			{
				bool flag3 = true;
				for (int n = 0; n < this.m_AnimArray.Length; n++)
				{
					if (!this.m_AnimArray[n].IsEnd)
					{
						flag3 = false;
						break;
					}
				}
				for (int num = 0; num < this.m_UIGroups.Length; num++)
				{
					if (!this.m_UIGroups[num].IsDonePlayOut)
					{
						flag3 = false;
						break;
					}
				}
				if (flag3)
				{
					this.ChangeStep(QuestChapterSelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060036E4 RID: 14052 RVA: 0x00115B44 File Offset: 0x00113F44
		private void ChangeStep(QuestChapterSelectUI.eStep step)
		{
			this.m_Step = step;
			switch (step)
			{
			case QuestChapterSelectUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestChapterSelectUI.eStep.LoadWait:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestChapterSelectUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestChapterSelectUI.eStep.OpenAnimStart:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestChapterSelectUI.eStep.OpenAnimWait:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestChapterSelectUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestChapterSelectUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestChapterSelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060036E5 RID: 14053 RVA: 0x00115C37 File Offset: 0x00114037
		public override void PlayIn()
		{
			this.ChangeStep(QuestChapterSelectUI.eStep.LoadWait);
		}

		// Token: 0x060036E6 RID: 14054 RVA: 0x00115C40 File Offset: 0x00114040
		public override void PlayOut()
		{
			this.ChangeStep(QuestChapterSelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimArray.Length; i++)
			{
				this.m_AnimArray[i].PlayOut();
			}
			for (int j = 0; j < this.m_UIGroups.Length; j++)
			{
				this.m_UIGroups[j].Close();
			}
		}

		// Token: 0x060036E7 RID: 14055 RVA: 0x00115CA0 File Offset: 0x001140A0
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestChapterSelectUI.eStep.End;
		}

		// Token: 0x060036E8 RID: 14056 RVA: 0x00115CAB File Offset: 0x001140AB
		public bool IsIdle()
		{
			return this.m_Step == QuestChapterSelectUI.eStep.Idle;
		}

		// Token: 0x060036E9 RID: 14057 RVA: 0x00115CB6 File Offset: 0x001140B6
		public void OnClickBackButtonCallBack()
		{
			this.m_SelectButton = QuestChapterSelectUI.eButton.Back;
			this.m_SelectChapterIdx = -1;
			this.PlayOut();
		}

		// Token: 0x060036EA RID: 14058 RVA: 0x00115CCC File Offset: 0x001140CC
		public void OnClickChapterButtonCallBack(int idx)
		{
			this.m_SelectButton = QuestChapterSelectUI.eButton.Chapter;
			this.m_SelectChapterIdx = idx;
			this.PlayOut();
		}

		// Token: 0x04003D4A RID: 15690
		private QuestChapterSelectUI.eStep m_Step;

		// Token: 0x04003D4B RID: 15691
		[SerializeField]
		private ChapterSelectScroll m_Scroll;

		// Token: 0x04003D4C RID: 15692
		[SerializeField]
		private AnimUIPlayer[] m_AnimArray;

		// Token: 0x04003D4D RID: 15693
		[SerializeField]
		private UIGroup[] m_UIGroups;

		// Token: 0x04003D4E RID: 15694
		[SerializeField]
		private ChapterOpenAnim m_ChapterOpenAnim;

		// Token: 0x04003D4F RID: 15695
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04003D50 RID: 15696
		private QuestChapterSelectUI.eButton m_SelectButton = QuestChapterSelectUI.eButton.None;

		// Token: 0x04003D51 RID: 15697
		private int m_SelectChapterIdx = -1;

		// Token: 0x04003D52 RID: 15698
		private List<ChapterPanel.ChapterData> m_List = new List<ChapterPanel.ChapterData>();

		// Token: 0x04003D53 RID: 15699
		private ChapterPanel m_ChapterPanelClone;

		// Token: 0x04003D54 RID: 15700
		private int m_OpenChapterIdx = -1;

		// Token: 0x04003D55 RID: 15701
		private float m_Delay = 0.3f;

		// Token: 0x02000A4E RID: 2638
		private enum eStep
		{
			// Token: 0x04003D57 RID: 15703
			Hide,
			// Token: 0x04003D58 RID: 15704
			LoadWait,
			// Token: 0x04003D59 RID: 15705
			In,
			// Token: 0x04003D5A RID: 15706
			OpenAnimStart,
			// Token: 0x04003D5B RID: 15707
			OpenAnimWait,
			// Token: 0x04003D5C RID: 15708
			Idle,
			// Token: 0x04003D5D RID: 15709
			Out,
			// Token: 0x04003D5E RID: 15710
			End
		}

		// Token: 0x02000A4F RID: 2639
		public enum eButton
		{
			// Token: 0x04003D60 RID: 15712
			None = -1,
			// Token: 0x04003D61 RID: 15713
			Back,
			// Token: 0x04003D62 RID: 15714
			Chapter
		}
	}
}
