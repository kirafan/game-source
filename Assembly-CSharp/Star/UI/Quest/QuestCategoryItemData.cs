﻿using System;

namespace Star.UI.Quest
{
	// Token: 0x02000A45 RID: 2629
	public class QuestCategoryItemData : ScrollItemData
	{
		// Token: 0x060036A2 RID: 13986 RVA: 0x00113D77 File Offset: 0x00112177
		public QuestCategoryItemData(int eventType, Action<int> callback)
		{
			this.m_EventType = eventType;
			this.OnClick += callback;
		}

		// Token: 0x140000B2 RID: 178
		// (add) Token: 0x060036A3 RID: 13987 RVA: 0x00113D90 File Offset: 0x00112190
		// (remove) Token: 0x060036A4 RID: 13988 RVA: 0x00113DC8 File Offset: 0x001121C8
		public event Action<int> OnClick;

		// Token: 0x060036A5 RID: 13989 RVA: 0x00113DFE File Offset: 0x001121FE
		public override void OnClickItemCallBack()
		{
			this.OnClick.Call(this.m_EventType);
		}

		// Token: 0x04003D02 RID: 15618
		public int m_EventType;
	}
}
