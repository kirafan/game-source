﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A63 RID: 2659
	public class StageSelectScroll : MonoBehaviour, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x140000BC RID: 188
		// (add) Token: 0x06003752 RID: 14162 RVA: 0x0011803C File Offset: 0x0011643C
		// (remove) Token: 0x06003753 RID: 14163 RVA: 0x00118074 File Offset: 0x00116474
		public event Action<int> OnDecideStage;

		// Token: 0x06003754 RID: 14164 RVA: 0x001180AC File Offset: 0x001164AC
		public void Setup()
		{
			this.m_DataList = new List<StageSelectPanel.StageData>();
			this.m_InstList = new List<StageSelectPanel>();
			this.m_EmptyInstList = new List<StageSelectPanel>();
			this.m_ScrollRect.GetComponent<ScrollRect>();
			this.m_ViewPort = this.m_ScrollRect.viewport;
			this.m_Content = this.m_ScrollRect.content;
		}

		// Token: 0x06003755 RID: 14165 RVA: 0x00118108 File Offset: 0x00116508
		private void Update()
		{
			if (this.m_IsDirty)
			{
				this.m_Content.sizeDelta = new Vector2(this.m_Content.sizeDelta.x, this.m_ViewPort.rect.height + (float)(this.m_DataList.Count + 2) * (this.m_PanelHeight + this.m_Space));
				float num = (float)this.m_SelectStage * (this.m_PanelHeight + this.m_Space);
				float verticalNormalizedPosition = 1f - num / (this.m_Content.rect.height - this.m_ViewPort.rect.height);
				this.m_ScrollRect.verticalNormalizedPosition = verticalNormalizedPosition;
				this.m_IsDirty = false;
			}
			if (!this.m_Drag)
			{
				float num2 = (float)this.m_SelectStage * (this.m_PanelHeight + this.m_Space);
				float num3 = 1f - num2 / (this.m_Content.rect.height - this.m_ViewPort.rect.height);
				int num4 = 0;
				while ((float)num4 < 1f + Time.deltaTime * 60f)
				{
					this.m_ScrollRect.verticalNormalizedPosition += (num3 - this.m_ScrollRect.verticalNormalizedPosition) * this.m_ScrollRate;
					num4++;
				}
			}
			else
			{
				this.m_SelectStage = (int)((this.m_Content.localPosition.y + (this.m_PanelHeight + this.m_Space) * 0.5f) / (this.m_PanelHeight + this.m_Space));
				if (this.m_SelectStage < 0)
				{
					this.m_SelectStage = 0;
				}
				if (this.m_SelectStage > this.m_DataList.Count - 1)
				{
					this.m_SelectStage = this.m_DataList.Count - 1;
				}
			}
			this.UpdateItemPosition();
		}

		// Token: 0x06003756 RID: 14166 RVA: 0x001182F8 File Offset: 0x001166F8
		private void UpdateItemPosition()
		{
			if (this.m_DataList.Count <= 0)
			{
				return;
			}
			this.m_EmptyInstList.Clear();
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_EmptyInstList.Add(this.m_InstList[i]);
			}
			for (int j = 0; j < 5; j++)
			{
				int num = this.m_SelectStage - 2 + j;
				if (num >= 0)
				{
					if (this.m_DataList.Count <= num)
					{
						break;
					}
					if (this.m_EmptyInstList.Count <= 0)
					{
						break;
					}
					StageSelectPanel stageSelectPanel = this.m_EmptyInstList[0];
					this.m_EmptyInstList.Remove(stageSelectPanel);
					stageSelectPanel.GameObject.SetActive(true);
					stageSelectPanel.SetStageData(num, this.m_DataList[num]);
					RectTransform rectTransform = stageSelectPanel.RectTransform;
					rectTransform.localPosition = new Vector3(this.m_Content.rect.width * 0.5f, -this.m_ViewPort.rect.height * 0.5f - (this.m_PanelHeight + this.m_Space) * (float)num, 0f);
					float num2 = Mathf.Abs((rectTransform.localPosition.y + this.m_Content.localPosition.y + this.m_ViewPort.rect.height * 0.5f) / this.m_ViewPort.rect.height);
					float num3 = 0.5f + 0.5f * (1f - num2);
					rectTransform.localPosition = new Vector3(rectTransform.localPosition.x + rectTransform.rect.width * 0.5f * (1f - num3), rectTransform.localPosition.y, 0f);
					rectTransform.localScale = Vector3.one * num3;
				}
			}
			for (int k = 0; k < this.m_EmptyInstList.Count; k++)
			{
				this.m_EmptyInstList[k].GameObject.SetActive(false);
			}
		}

		// Token: 0x06003757 RID: 14167 RVA: 0x00118560 File Offset: 0x00116960
		public int AddItem(StageSelectPanel.StageData data)
		{
			int itemNum = this.m_ItemNum;
			this.m_DataList.Add(data);
			if (this.m_InstList.Count < 5)
			{
				StageSelectPanel stageSelectPanel = UnityEngine.Object.Instantiate<StageSelectPanel>(this.m_StageSelectPanelPrefab, this.m_Content);
				RectTransform rectTransform = stageSelectPanel.RectTransform;
				rectTransform.localScale = Vector3.one;
				stageSelectPanel.OnClickButton += this.OnTapButton;
				stageSelectPanel.OnClickPanel += this.OnTapPanel;
				this.m_InstList.Add(stageSelectPanel);
				if (this.m_ItemNum == 0)
				{
					this.m_PanelHeight = stageSelectPanel.RectTransform.rect.height;
				}
			}
			this.m_ItemNum++;
			this.m_IsDirty = true;
			return itemNum;
		}

		// Token: 0x06003758 RID: 14168 RVA: 0x00118620 File Offset: 0x00116A20
		public void OnTapButton(int idx)
		{
			if (this.m_SelectStage != idx)
			{
				this.OnTapPanel(idx);
				return;
			}
			if (this.OnDecideStage != null)
			{
				this.OnDecideStage(this.m_DataList[idx].m_StageID);
			}
		}

		// Token: 0x06003759 RID: 14169 RVA: 0x0011866B File Offset: 0x00116A6B
		public void OnTapPanel(int idx)
		{
			this.m_SelectStage = idx;
		}

		// Token: 0x0600375A RID: 14170 RVA: 0x00118674 File Offset: 0x00116A74
		public void OnDrag(PointerEventData eventData)
		{
			this.m_Drag = true;
		}

		// Token: 0x0600375B RID: 14171 RVA: 0x0011867D File Offset: 0x00116A7D
		public void OnEndDrag(PointerEventData eventData)
		{
			this.m_Drag = false;
		}

		// Token: 0x04003E04 RID: 15876
		private const float SELECT_PANEL_SCALE = 1f;

		// Token: 0x04003E05 RID: 15877
		private const float NONSELECT_PANEL_SCALE = 0.5f;

		// Token: 0x04003E06 RID: 15878
		private const int MAX_SHOW_PANEL = 5;

		// Token: 0x04003E07 RID: 15879
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x04003E08 RID: 15880
		[SerializeField]
		private StageSelectPanel m_StageSelectPanelPrefab;

		// Token: 0x04003E09 RID: 15881
		[SerializeField]
		private float m_Space;

		// Token: 0x04003E0A RID: 15882
		private int m_ItemNum;

		// Token: 0x04003E0B RID: 15883
		private List<StageSelectPanel.StageData> m_DataList;

		// Token: 0x04003E0C RID: 15884
		private List<StageSelectPanel> m_InstList;

		// Token: 0x04003E0D RID: 15885
		private List<StageSelectPanel> m_EmptyInstList;

		// Token: 0x04003E0E RID: 15886
		private bool m_IsDirty;

		// Token: 0x04003E0F RID: 15887
		private int m_SelectStage;

		// Token: 0x04003E10 RID: 15888
		private float m_PanelHeight;

		// Token: 0x04003E11 RID: 15889
		private bool m_Drag;

		// Token: 0x04003E12 RID: 15890
		private float m_ScrollRate = 0.1f;

		// Token: 0x04003E14 RID: 15892
		private RectTransform m_ViewPort;

		// Token: 0x04003E15 RID: 15893
		private RectTransform m_Content;
	}
}
