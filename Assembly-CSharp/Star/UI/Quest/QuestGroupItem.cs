﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A50 RID: 2640
	public class QuestGroupItem : ScrollItemIcon
	{
		// Token: 0x060036EC RID: 14060 RVA: 0x00115CEC File Offset: 0x001140EC
		protected override void ApplyData()
		{
			base.ApplyData();
			QuestGroupItemData questGroupItemData = (QuestGroupItemData)this.m_Data;
			QuestManager.EventGroupData eventGroupData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEventGroupData(questGroupItemData.m_GroupID);
			this.m_QuestGroupIcon.Apply(eventGroupData.m_GroupID);
			QuestUtility.eGroupClearRank eGroupClearRank = QuestUtility.CheckClearGroup(eventGroupData);
			if (eGroupClearRank != QuestUtility.eGroupClearRank.None)
			{
				if (eGroupClearRank != QuestUtility.eGroupClearRank.Clear)
				{
					if (eGroupClearRank == QuestUtility.eGroupClearRank.Complete)
					{
						this.m_ClearIcon.gameObject.SetActive(true);
						this.m_ClearIcon.sprite = this.m_ClearIconCompleteSprite;
					}
				}
				else
				{
					this.m_ClearIcon.gameObject.SetActive(true);
					this.m_ClearIcon.sprite = this.m_ClearIconSprite;
				}
			}
			else
			{
				this.m_ClearIcon.gameObject.SetActive(false);
			}
			TimeSpan span = QuestUtility.CalcTimeSpan(eventGroupData);
			this.m_SpanText.text = UIUtility.GetSpanString(span);
		}

		// Token: 0x04003D63 RID: 15715
		[SerializeField]
		private QuestGroupIcon m_QuestGroupIcon;

		// Token: 0x04003D64 RID: 15716
		[SerializeField]
		private Image m_ClearIcon;

		// Token: 0x04003D65 RID: 15717
		[SerializeField]
		private Sprite m_ClearIconSprite;

		// Token: 0x04003D66 RID: 15718
		[SerializeField]
		private Sprite m_ClearIconCompleteSprite;

		// Token: 0x04003D67 RID: 15719
		[SerializeField]
		private Text m_SpanText;
	}
}
