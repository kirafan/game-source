﻿using System;

namespace Star.UI.Quest
{
	// Token: 0x02000A51 RID: 2641
	public class QuestGroupItemData : ScrollItemData
	{
		// Token: 0x060036ED RID: 14061 RVA: 0x00115DCE File Offset: 0x001141CE
		public QuestGroupItemData(int idx, int groupID)
		{
			this.m_Idx = idx;
			this.m_GroupID = groupID;
			this.m_TimeSpan = QuestUtility.CalcTimeSpan(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEventGroupData(groupID));
		}

		// Token: 0x060036EE RID: 14062 RVA: 0x00115DFF File Offset: 0x001141FF
		public override void OnClickItemCallBack()
		{
			base.OnClickItemCallBack();
			this.OnClick.Call(this.m_GroupID);
		}

		// Token: 0x04003D68 RID: 15720
		public int m_Idx;

		// Token: 0x04003D69 RID: 15721
		public int m_GroupID;

		// Token: 0x04003D6A RID: 15722
		public TimeSpan m_TimeSpan;

		// Token: 0x04003D6B RID: 15723
		public Action<int> OnClick;

		// Token: 0x04003D6C RID: 15724
		public Action<int> OnClickDropItem;
	}
}
