﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A61 RID: 2657
	public class StageSelectPanel : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06003747 RID: 14151 RVA: 0x00117DFD File Offset: 0x001161FD
		public GameObject GameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06003748 RID: 14152 RVA: 0x00117E05 File Offset: 0x00116205
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x140000BA RID: 186
		// (add) Token: 0x06003749 RID: 14153 RVA: 0x00117E10 File Offset: 0x00116210
		// (remove) Token: 0x0600374A RID: 14154 RVA: 0x00117E48 File Offset: 0x00116248
		public event Action<int> OnClickButton;

		// Token: 0x140000BB RID: 187
		// (add) Token: 0x0600374B RID: 14155 RVA: 0x00117E80 File Offset: 0x00116280
		// (remove) Token: 0x0600374C RID: 14156 RVA: 0x00117EB8 File Offset: 0x001162B8
		public event Action<int> OnClickPanel;

		// Token: 0x0600374D RID: 14157 RVA: 0x00117EEE File Offset: 0x001162EE
		private void Awake()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x0600374E RID: 14158 RVA: 0x00117F08 File Offset: 0x00116308
		public void SetStageData(int idx, StageSelectPanel.StageData data)
		{
			this.m_Idx = idx;
			this.m_NumberObj.text = data.m_Number.ToString("D3");
			this.m_TitleObj.text = data.m_Title;
			this.m_StaminaText.text = "x" + data.m_Stamina.ToString("D2");
			Image[] componentsInChildren = this.m_MissionObj.GetComponentsInChildren<Image>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				if (i < data.m_ClearRank)
				{
					componentsInChildren[i].gameObject.SetActive(true);
				}
				else
				{
					componentsInChildren[i].gameObject.SetActive(false);
				}
			}
			this.m_ClearMark.SetActive(data.m_IsClear);
		}

		// Token: 0x0600374F RID: 14159 RVA: 0x00117FD0 File Offset: 0x001163D0
		public void OnTapBattleButton()
		{
			if (this.OnClickButton != null)
			{
				this.OnClickButton(this.m_Idx);
			}
		}

		// Token: 0x06003750 RID: 14160 RVA: 0x00117FEE File Offset: 0x001163EE
		public void OnPointerClick(PointerEventData eventdata)
		{
			if (this.OnClickPanel != null)
			{
				this.OnClickPanel(this.m_Idx);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
		}

		// Token: 0x04003DF2 RID: 15858
		[SerializeField]
		private Text m_NumberObj;

		// Token: 0x04003DF3 RID: 15859
		[SerializeField]
		private Text m_TitleObj;

		// Token: 0x04003DF4 RID: 15860
		[SerializeField]
		private GameObject m_EnemyTypeObj;

		// Token: 0x04003DF5 RID: 15861
		[SerializeField]
		private GameObject m_MissionObj;

		// Token: 0x04003DF6 RID: 15862
		[SerializeField]
		private Text m_StaminaText;

		// Token: 0x04003DF7 RID: 15863
		[SerializeField]
		private Button m_BattleButton;

		// Token: 0x04003DF8 RID: 15864
		[SerializeField]
		private GameObject m_ClearMark;

		// Token: 0x04003DF9 RID: 15865
		private int m_Idx;

		// Token: 0x04003DFA RID: 15866
		private GameObject m_GameObject;

		// Token: 0x04003DFB RID: 15867
		private RectTransform m_RectTransform;

		// Token: 0x02000A62 RID: 2658
		public struct StageData
		{
			// Token: 0x04003DFE RID: 15870
			public int m_StageID;

			// Token: 0x04003DFF RID: 15871
			public int m_Number;

			// Token: 0x04003E00 RID: 15872
			public string m_Title;

			// Token: 0x04003E01 RID: 15873
			public int m_Stamina;

			// Token: 0x04003E02 RID: 15874
			public int m_ClearRank;

			// Token: 0x04003E03 RID: 15875
			public bool m_IsClear;
		}
	}
}
