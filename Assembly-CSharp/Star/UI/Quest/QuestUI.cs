﻿using System;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A64 RID: 2660
	public class QuestUI : MenuUIBase
	{
		// Token: 0x0600375D RID: 14173 RVA: 0x0011868E File Offset: 0x00116A8E
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
		}

		// Token: 0x0600375E RID: 14174 RVA: 0x001186BC File Offset: 0x00116ABC
		private void Update()
		{
			switch (this.m_Step)
			{
			case QuestUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(QuestUI.eStep.Idle);
				}
				break;
			}
			case QuestUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(QuestUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600375F RID: 14175 RVA: 0x00118794 File Offset: 0x00116B94
		private void ChangeStep(QuestUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case QuestUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case QuestUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003760 RID: 14176 RVA: 0x0011883C File Offset: 0x00116C3C
		public override void PlayIn()
		{
			this.ChangeStep(QuestUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003761 RID: 14177 RVA: 0x00118878 File Offset: 0x00116C78
		public override void PlayOut()
		{
			this.ChangeStep(QuestUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003762 RID: 14178 RVA: 0x001188B2 File Offset: 0x00116CB2
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestUI.eStep.End;
		}

		// Token: 0x04003E16 RID: 15894
		private QuestUI.eStep m_Step;

		// Token: 0x04003E17 RID: 15895
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x02000A65 RID: 2661
		private enum eStep
		{
			// Token: 0x04003E19 RID: 15897
			Hide,
			// Token: 0x04003E1A RID: 15898
			In,
			// Token: 0x04003E1B RID: 15899
			Idle,
			// Token: 0x04003E1C RID: 15900
			Out,
			// Token: 0x04003E1D RID: 15901
			End
		}
	}
}
