﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A46 RID: 2630
	public class QuestCategorySelectUI : MenuUIBase
	{
		// Token: 0x17000334 RID: 820
		// (get) Token: 0x060036A7 RID: 13991 RVA: 0x00113E20 File Offset: 0x00112220
		public bool IsSelectQuestCategory
		{
			get
			{
				return this.m_IsSelectQuestCategory;
			}
		}

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x060036A8 RID: 13992 RVA: 0x00113E28 File Offset: 0x00112228
		public int SelectEventType
		{
			get
			{
				return this.m_SelectEventType;
			}
		}

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x060036A9 RID: 13993 RVA: 0x00113E30 File Offset: 0x00112230
		public bool IsSelectChapterQuest
		{
			get
			{
				return this.m_IsSelectQuestCategory && this.m_SelectEventType == -1;
			}
		}

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x060036AA RID: 13994 RVA: 0x00113E4C File Offset: 0x0011224C
		public bool IsSelectEventQuest
		{
			get
			{
				return this.m_IsSelectQuestCategory && this.m_SelectEventType > -1;
			}
		}

		// Token: 0x060036AB RID: 13995 RVA: 0x00113E68 File Offset: 0x00112268
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			List<int> availableEventQuestTypes = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetAvailableEventQuestTypes();
			this.m_Scroll.Setup();
			this.m_Scroll.AddItem(new QuestCategoryItemData(-1, new Action<int>(this.OnClickQuestCategoryButtonCallBack)));
			for (int i = 0; i < availableEventQuestTypes.Count; i++)
			{
				this.m_Scroll.AddItem(new QuestCategoryItemData(availableEventQuestTypes[i], new Action<int>(this.OnClickQuestCategoryButtonCallBack)));
			}
		}

		// Token: 0x060036AC RID: 13996 RVA: 0x00113F14 File Offset: 0x00112314
		private void Update()
		{
			switch (this.m_Step)
			{
			case QuestCategorySelectUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(QuestCategorySelectUI.eStep.Idle);
				}
				break;
			}
			case QuestCategorySelectUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(QuestCategorySelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060036AD RID: 13997 RVA: 0x00114024 File Offset: 0x00112424
		private void ChangeStep(QuestCategorySelectUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case QuestCategorySelectUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestCategorySelectUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Change(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Quest, true);
				this.m_MainGroup.Open();
				break;
			case QuestCategorySelectUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestCategorySelectUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case QuestCategorySelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060036AE RID: 13998 RVA: 0x001140F4 File Offset: 0x001124F4
		public override void PlayIn()
		{
			this.ChangeStep(QuestCategorySelectUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_SelectEventType = -1;
		}

		// Token: 0x060036AF RID: 13999 RVA: 0x00114138 File Offset: 0x00112538
		public override void PlayOut()
		{
			this.ChangeStep(QuestCategorySelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x060036B0 RID: 14000 RVA: 0x00114172 File Offset: 0x00112572
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestCategorySelectUI.eStep.End;
		}

		// Token: 0x060036B1 RID: 14001 RVA: 0x0011417D File Offset: 0x0011257D
		public void OnClickQuestCategoryButtonCallBack(int questEventType)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_IsSelectQuestCategory = true;
			this.m_SelectEventType = questEventType;
			this.PlayOut();
		}

		// Token: 0x04003D04 RID: 15620
		private QuestCategorySelectUI.eStep m_Step;

		// Token: 0x04003D05 RID: 15621
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003D06 RID: 15622
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003D07 RID: 15623
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003D08 RID: 15624
		private CustomButton[] m_CategoryButtons;

		// Token: 0x04003D09 RID: 15625
		private bool m_IsSelectQuestCategory;

		// Token: 0x04003D0A RID: 15626
		private int m_SelectEventType = -1;

		// Token: 0x02000A47 RID: 2631
		private enum eStep
		{
			// Token: 0x04003D0C RID: 15628
			Hide,
			// Token: 0x04003D0D RID: 15629
			In,
			// Token: 0x04003D0E RID: 15630
			Idle,
			// Token: 0x04003D0F RID: 15631
			Out,
			// Token: 0x04003D10 RID: 15632
			End
		}
	}
}
