﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A5C RID: 2652
	public class QuestListItem : ScrollItemIcon
	{
		// Token: 0x06003729 RID: 14121 RVA: 0x00116C68 File Offset: 0x00115068
		protected override void ApplyData()
		{
			base.ApplyData();
			QuestListItemData questListItemData = (QuestListItemData)this.m_Data;
			QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(questListItemData.m_QuestID);
			if (questData == null)
			{
				return;
			}
			this.m_BaseImage.sprite = this.m_BaseSprite[(int)questListItemData.m_Difficulty];
			if (questData.m_Param.m_IsAdvOnly != 0)
			{
				this.m_ADVOnly.SetActive(true);
				this.m_QuestObj.SetActive(false);
			}
			else
			{
				this.m_QuestObj.SetActive(true);
				this.m_ADVOnly.SetActive(false);
			}
			this.m_Number.SetValue(questListItemData.m_Idx + 1);
			this.m_TitleText.text = questData.m_Param.m_QuestName;
			if (questListItemData.m_EventData != null && questListItemData.m_EventData.m_ExKeyItemID != -1)
			{
				this.m_CostStamina.SetActive(false);
				this.m_CostItem.SetActive(true);
				this.m_ItemIcon.Apply(questListItemData.m_EventData.m_ExKeyItemID);
				this.m_CostValue.text = "x" + questListItemData.m_EventData.m_ExKeyItemNum.ToString().PadLeft(3);
			}
			else
			{
				this.m_CostStamina.SetActive(true);
				this.m_CostItem.SetActive(false);
				this.m_CostValue.text = "x" + questData.m_Param.m_Stamina.ToString().PadLeft(3);
			}
			bool[] enemyTypes = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEnemyTypes(questListItemData.m_QuestID);
			for (int i = 0; i < this.m_ElementIcons.Length; i++)
			{
				if (enemyTypes == null || !enemyTypes[i])
				{
					this.m_ElementIcons[i].SetActive(false);
				}
				else
				{
					this.m_ElementIcons[i].SetActive(true);
				}
			}
			this.m_ElementObj.SetActive(enemyTypes != null);
			this.m_ElementRandomObj.SetActive(enemyTypes == null);
			QuestDefine.eClearRank clearRank = questData.m_ClearRank;
			this.m_ClearIcon.sprite = this.m_ClearIconSprites[(int)clearRank];
			if (questListItemData.m_EventData != null && questListItemData.m_EventData.m_FreqType != eEventQuestFreqType.EveryDay)
			{
				TimeSpan timeSpan = questListItemData.m_TimeSpan;
				if (timeSpan.Days <= 999)
				{
					this.m_SpanObj.SetActive(true);
					this.m_SpanText.text = UIUtility.GetSpanString(timeSpan);
				}
				else
				{
					this.m_SpanObj.SetActive(false);
				}
			}
			else
			{
				this.m_SpanObj.SetActive(false);
			}
			this.ApplyLimit();
		}

		// Token: 0x0600372A RID: 14122 RVA: 0x00116F0C File Offset: 0x0011530C
		private void ApplyLimit()
		{
			QuestListItemData questListItemData = (QuestListItemData)this.m_Data;
			if (questListItemData.m_EventData == null)
			{
				this.m_LimitParentObj.SetActive(false);
				return;
			}
			this.m_LimitParentObj.SetActive(true);
			for (int i = 0; i < this.m_ActiveSeparateList.Count; i++)
			{
				this.m_ActiveSeparateList[i].SetActive(false);
			}
			RectTransform component = this.m_LimitParentObj.GetComponent<RectTransform>();
			bool flag = false;
			int num = 0;
			for (int j = 0; j < this.m_ClassLimitList.Count; j++)
			{
				this.m_ClassLimitList[j].gameObject.SetActive(false);
			}
			if (questListItemData.m_EventData.m_ExClasses != null)
			{
				for (int k = 0; k < questListItemData.m_EventData.m_ExClasses.Length; k++)
				{
					if (questListItemData.m_EventData.m_ExClasses[k])
					{
						QuestLimit questLimit;
						if (this.m_ClassLimitList.Count <= num)
						{
							questLimit = UnityEngine.Object.Instantiate<QuestLimit>(this.m_ClassLimitPrefab, component);
							this.m_ClassLimitList.Add(questLimit);
						}
						else
						{
							questLimit = this.m_ClassLimitList[num];
						}
						questLimit.gameObject.SetActive(true);
						questLimit.GetComponent<RectTransform>().SetAsFirstSibling();
						questLimit.SetupClassLimit((eClassType)k);
						num++;
						flag = true;
					}
				}
			}
			num = 0;
			for (int l = 0; l < this.m_ElementLimitList.Count; l++)
			{
				this.m_ElementLimitList[l].gameObject.SetActive(false);
			}
			if (questListItemData.m_EventData.m_ExElements != null)
			{
				for (int m = 0; m < questListItemData.m_EventData.m_ExElements.Length; m++)
				{
					if (questListItemData.m_EventData.m_ExElements[m])
					{
						if (num == 0 && flag)
						{
							this.ScoopSeparate(component);
						}
						QuestLimit questLimit2;
						if (this.m_ElementLimitList.Count <= num)
						{
							questLimit2 = UnityEngine.Object.Instantiate<QuestLimit>(this.m_ElementLimitPrefab, component);
							this.m_ElementLimitList.Add(questLimit2);
						}
						else
						{
							questLimit2 = this.m_ElementLimitList[num];
						}
						questLimit2.gameObject.SetActive(true);
						questLimit2.GetComponent<RectTransform>().SetAsFirstSibling();
						questLimit2.SetupElementLimit((eElementType)m);
						num++;
						flag = true;
					}
				}
			}
			if (questListItemData.m_EventData.m_ExTitleType != eTitleType.None)
			{
				if (flag)
				{
					this.ScoopSeparate(component);
				}
				if (this.m_ContentLimit == null)
				{
					this.m_ContentLimit = UnityEngine.Object.Instantiate<QuestLimit>(this.m_ContentLimitPrefab, component);
				}
				this.m_ContentLimit.gameObject.SetActive(true);
				this.m_ContentLimit.GetComponent<RectTransform>().SetAsFirstSibling();
				this.m_ContentLimit.SetupTitleLimit(questListItemData.m_EventData.m_ExTitleType);
				flag = true;
			}
			else if (this.m_ContentLimit != null)
			{
				this.m_ContentLimit.gameObject.SetActive(false);
			}
			if (questListItemData.m_EventData.m_ExNamedType != eCharaNamedType.None)
			{
				if (flag)
				{
					this.ScoopSeparate(component);
				}
				if (this.m_NamedLimit == null)
				{
					this.m_NamedLimit = UnityEngine.Object.Instantiate<QuestLimit>(this.m_NamedLimitPrefab, component);
				}
				this.m_NamedLimit.gameObject.SetActive(true);
				this.m_NamedLimit.GetComponent<RectTransform>().SetAsFirstSibling();
				this.m_NamedLimit.SetupNamedLimit(questListItemData.m_EventData.m_ExNamedType);
				flag = true;
			}
			else if (this.m_NamedLimit != null)
			{
				this.m_NamedLimit.gameObject.SetActive(false);
			}
			if (questListItemData.m_EventData.m_ExRare != eRare.None)
			{
				if (flag)
				{
					this.ScoopSeparate(component);
				}
				if (this.m_RareLimit == null)
				{
					this.m_RareLimit = UnityEngine.Object.Instantiate<QuestLimit>(this.m_RareLimitPrefab, component);
				}
				this.m_RareLimit.gameObject.SetActive(true);
				this.m_RareLimit.GetComponent<RectTransform>().SetAsFirstSibling();
				this.m_RareLimit.SetupRareLimit(questListItemData.m_EventData.m_ExRare);
				flag = true;
			}
			else if (this.m_RareLimit != null)
			{
				this.m_RareLimit.gameObject.SetActive(false);
			}
			if (questListItemData.m_EventData.m_ExCost != 0)
			{
				if (flag)
				{
					this.ScoopSeparate(component);
				}
				if (this.m_CostLimit == null)
				{
					this.m_CostLimit = UnityEngine.Object.Instantiate<QuestLimit>(this.m_CostLimitPrefab, component);
				}
				this.m_CostLimit.gameObject.SetActive(true);
				this.m_CostLimit.GetComponent<RectTransform>().SetAsFirstSibling();
				this.m_CostLimit.SetupCostLimit(questListItemData.m_EventData.m_ExCost);
				flag = true;
			}
			else if (this.m_CostLimit != null)
			{
				this.m_CostLimit.gameObject.SetActive(false);
			}
			this.m_LimitParentObj.SetActive(flag);
		}

		// Token: 0x0600372B RID: 14123 RVA: 0x00117404 File Offset: 0x00115804
		public GameObject ScoopSeparate(RectTransform parent)
		{
			if (this.m_EmptySeparateList.Count > 0)
			{
				this.m_ActiveSeparateList.Add(this.m_EmptySeparateList[0]);
				this.m_EmptySeparateList.RemoveAt(0);
				this.m_ActiveSeparateList[0].SetActive(true);
				this.m_ActiveSeparateList[0].GetComponent<RectTransform>().SetParent(parent);
				return this.m_ActiveSeparateList[0];
			}
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_SeparateLimitPrefab, parent);
			this.m_ActiveSeparateList.Add(gameObject);
			return gameObject;
		}

		// Token: 0x0600372C RID: 14124 RVA: 0x00117498 File Offset: 0x00115898
		public void OnClickDropItemButtonCallBack()
		{
			QuestListItemData questListItemData = (QuestListItemData)this.m_Data;
			questListItemData.OnClickDropItem.Call(questListItemData.m_QuestID);
		}

		// Token: 0x04003DAE RID: 15790
		[SerializeField]
		private Image m_BaseImage;

		// Token: 0x04003DAF RID: 15791
		[SerializeField]
		private Sprite[] m_BaseSprite;

		// Token: 0x04003DB0 RID: 15792
		[SerializeField]
		private ImageNumbers m_Number;

		// Token: 0x04003DB1 RID: 15793
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x04003DB2 RID: 15794
		[SerializeField]
		private GameObject m_QuestObj;

		// Token: 0x04003DB3 RID: 15795
		[SerializeField]
		private GameObject m_ADVOnly;

		// Token: 0x04003DB4 RID: 15796
		[SerializeField]
		private GameObject m_CostStamina;

		// Token: 0x04003DB5 RID: 15797
		[SerializeField]
		private GameObject m_CostItem;

		// Token: 0x04003DB6 RID: 15798
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04003DB7 RID: 15799
		[SerializeField]
		private Text m_CostValue;

		// Token: 0x04003DB8 RID: 15800
		[SerializeField]
		private GameObject m_ElementObj;

		// Token: 0x04003DB9 RID: 15801
		[SerializeField]
		private GameObject[] m_ElementIcons;

		// Token: 0x04003DBA RID: 15802
		[SerializeField]
		private GameObject m_ElementRandomObj;

		// Token: 0x04003DBB RID: 15803
		[SerializeField]
		private Image m_ClearIcon;

		// Token: 0x04003DBC RID: 15804
		[SerializeField]
		private Sprite[] m_ClearIconSprites;

		// Token: 0x04003DBD RID: 15805
		[SerializeField]
		private GameObject m_SpanObj;

		// Token: 0x04003DBE RID: 15806
		[SerializeField]
		private Text m_SpanText;

		// Token: 0x04003DBF RID: 15807
		[SerializeField]
		private GameObject m_LimitParentObj;

		// Token: 0x04003DC0 RID: 15808
		[SerializeField]
		private GameObject m_SeparateLimitPrefab;

		// Token: 0x04003DC1 RID: 15809
		private List<GameObject> m_EmptySeparateList = new List<GameObject>();

		// Token: 0x04003DC2 RID: 15810
		private List<GameObject> m_ActiveSeparateList = new List<GameObject>();

		// Token: 0x04003DC3 RID: 15811
		[SerializeField]
		private QuestLimit m_ClassLimitPrefab;

		// Token: 0x04003DC4 RID: 15812
		private List<QuestLimit> m_ClassLimitList = new List<QuestLimit>();

		// Token: 0x04003DC5 RID: 15813
		[SerializeField]
		private QuestLimit m_ElementLimitPrefab;

		// Token: 0x04003DC6 RID: 15814
		private List<QuestLimit> m_ElementLimitList = new List<QuestLimit>();

		// Token: 0x04003DC7 RID: 15815
		[SerializeField]
		private QuestLimit m_NamedLimitPrefab;

		// Token: 0x04003DC8 RID: 15816
		private QuestLimit m_NamedLimit;

		// Token: 0x04003DC9 RID: 15817
		[SerializeField]
		private QuestLimit m_RareLimitPrefab;

		// Token: 0x04003DCA RID: 15818
		private QuestLimit m_RareLimit;

		// Token: 0x04003DCB RID: 15819
		[SerializeField]
		private QuestLimit m_CostLimitPrefab;

		// Token: 0x04003DCC RID: 15820
		private QuestLimit m_CostLimit;

		// Token: 0x04003DCD RID: 15821
		[SerializeField]
		private QuestLimit m_ContentLimitPrefab;

		// Token: 0x04003DCE RID: 15822
		private QuestLimit m_ContentLimit;
	}
}
