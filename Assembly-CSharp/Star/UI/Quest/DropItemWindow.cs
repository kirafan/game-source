﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A5A RID: 2650
	public class DropItemWindow : UIGroup
	{
		// Token: 0x0600371D RID: 14109 RVA: 0x00116A51 File Offset: 0x00114E51
		public void Open(List<int> dropItemIDs)
		{
			this.m_ItemIDList = dropItemIDs;
			this.Open();
		}

		// Token: 0x0600371E RID: 14110 RVA: 0x00116A60 File Offset: 0x00114E60
		public override void Update()
		{
			base.Update();
			if (this.m_ItemIDList != null && this.m_InstList.Count < this.m_ItemIDList.Count)
			{
				ItemIconWithDetail itemIconWithDetail = UnityEngine.Object.Instantiate<ItemIconWithDetail>(this.m_ItemIconPrefab, this.m_ItemIconParent, false);
				itemIconWithDetail.Apply(this.m_ItemIDList[this.m_InstList.Count]);
				this.m_InstList.Add(itemIconWithDetail);
			}
		}

		// Token: 0x0600371F RID: 14111 RVA: 0x00116AD4 File Offset: 0x00114ED4
		public override void OnFinishPlayOut()
		{
			base.OnFinishPlayOut();
			while (this.m_InstList.Count > 0)
			{
				UnityEngine.Object.Destroy(this.m_InstList[0].gameObject);
				this.m_InstList.RemoveAt(0);
			}
			if (this.m_ItemIDList != null)
			{
				this.m_ItemIDList.Clear();
			}
		}

		// Token: 0x06003720 RID: 14112 RVA: 0x00116B35 File Offset: 0x00114F35
		public override bool IsCompleteFinalize()
		{
			return base.IsCompleteFinalize();
		}

		// Token: 0x04003DA4 RID: 15780
		[SerializeField]
		private ItemIconWithDetail m_ItemIconPrefab;

		// Token: 0x04003DA5 RID: 15781
		[SerializeField]
		private RectTransform m_ItemIconParent;

		// Token: 0x04003DA6 RID: 15782
		private List<int> m_ItemIDList;

		// Token: 0x04003DA7 RID: 15783
		private List<ItemIconWithDetail> m_InstList = new List<ItemIconWithDetail>();
	}
}
