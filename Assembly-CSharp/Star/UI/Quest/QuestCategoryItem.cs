﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A44 RID: 2628
	public class QuestCategoryItem : ScrollItemIcon
	{
		// Token: 0x060036A1 RID: 13985 RVA: 0x00113CF4 File Offset: 0x001120F4
		protected override void ApplyData()
		{
			base.ApplyData();
			QuestCategoryItemData questCategoryItemData = (QuestCategoryItemData)this.m_Data;
			if (questCategoryItemData.m_EventType == -1)
			{
				this.m_Icon.Destroy();
				this.m_Icon.enabled = false;
				Image component = this.m_Icon.GetComponent<Image>();
				component.sprite = this.m_StorySprite;
				component.enabled = true;
			}
			else
			{
				this.m_Icon.enabled = true;
				this.m_Icon.Apply(questCategoryItemData.m_EventType);
			}
		}

		// Token: 0x04003D00 RID: 15616
		[SerializeField]
		private QuestEventTypeIcon m_Icon;

		// Token: 0x04003D01 RID: 15617
		[SerializeField]
		private Sprite m_StorySprite;
	}
}
