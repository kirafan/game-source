﻿using System;

namespace Star.UI.Quest
{
	// Token: 0x02000A56 RID: 2646
	public class QuestFriendSelectItemData : ScrollItemData
	{
		// Token: 0x060036FD RID: 14077 RVA: 0x001162D4 File Offset: 0x001146D4
		public QuestFriendSelectItemData(FriendManager.FriendData friendData, UserSupportData supportData, Action<FriendManager.FriendData, UserSupportData> onClick)
		{
			this.m_FriendData = friendData;
			this.m_SupportData = supportData;
			this.m_UserName = friendData.m_Name;
			this.m_UserLv = friendData.m_Lv;
			this.m_CharaID = supportData.CharaID;
			this.m_WeaponID = supportData.WeaponID;
			this.m_LogInTime = friendData.m_LastLoginAt;
			this.m_IsFriend = (friendData.m_Type == FriendDefine.eType.Friend);
			this.OnClick = onClick;
		}

		// Token: 0x140000B5 RID: 181
		// (add) Token: 0x060036FE RID: 14078 RVA: 0x00116348 File Offset: 0x00114748
		// (remove) Token: 0x060036FF RID: 14079 RVA: 0x00116380 File Offset: 0x00114780
		public event Action<FriendManager.FriendData, UserSupportData> OnClick;

		// Token: 0x06003700 RID: 14080 RVA: 0x001163B6 File Offset: 0x001147B6
		public override void OnClickItemCallBack()
		{
			base.OnClickItemCallBack();
			this.OnClick.Call(this.m_FriendData, this.m_SupportData);
		}

		// Token: 0x04003D83 RID: 15747
		public string m_UserName;

		// Token: 0x04003D84 RID: 15748
		public int m_UserLv;

		// Token: 0x04003D85 RID: 15749
		public int m_CharaID;

		// Token: 0x04003D86 RID: 15750
		public int m_WeaponID;

		// Token: 0x04003D87 RID: 15751
		public DateTime m_LogInTime;

		// Token: 0x04003D88 RID: 15752
		public bool m_IsFriend;

		// Token: 0x04003D89 RID: 15753
		public FriendManager.FriendData m_FriendData;

		// Token: 0x04003D8A RID: 15754
		public UserSupportData m_SupportData;
	}
}
