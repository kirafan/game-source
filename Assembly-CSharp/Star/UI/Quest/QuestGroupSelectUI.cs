﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A52 RID: 2642
	public class QuestGroupSelectUI : MenuUIBase
	{
		// Token: 0x1700033C RID: 828
		// (get) Token: 0x060036F0 RID: 14064 RVA: 0x00115E2E File Offset: 0x0011422E
		public QuestGroupSelectUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x060036F1 RID: 14065 RVA: 0x00115E36 File Offset: 0x00114236
		public int SelectGroupID
		{
			get
			{
				return this.m_SelectGroupID;
			}
		}

		// Token: 0x060036F2 RID: 14066 RVA: 0x00115E40 File Offset: 0x00114240
		public void Setup(List<QuestManager.EventGroupData> groupDataList)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_Scroll.Setup();
			for (int i = 0; i < groupDataList.Count; i++)
			{
				QuestGroupItemData questGroupItemData = new QuestGroupItemData(i, groupDataList[i].m_GroupID);
				QuestGroupItemData questGroupItemData2 = questGroupItemData;
				questGroupItemData2.OnClick = (Action<int>)Delegate.Combine(questGroupItemData2.OnClick, new Action<int>(this.OnClickGroupButtonCallBack));
				this.m_Scroll.AddItem(questGroupItemData);
			}
		}

		// Token: 0x060036F3 RID: 14067 RVA: 0x00115EEC File Offset: 0x001142EC
		private void Update()
		{
			switch (this.m_Step)
			{
			case QuestGroupSelectUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(QuestGroupSelectUI.eStep.Idle);
				}
				break;
			}
			case QuestGroupSelectUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(QuestGroupSelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x060036F4 RID: 14068 RVA: 0x00115FE8 File Offset: 0x001143E8
		private void ChangeStep(QuestGroupSelectUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case QuestGroupSelectUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestGroupSelectUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case QuestGroupSelectUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestGroupSelectUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case QuestGroupSelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060036F5 RID: 14069 RVA: 0x001160A4 File Offset: 0x001144A4
		public override void PlayIn()
		{
			this.ChangeStep(QuestGroupSelectUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x060036F6 RID: 14070 RVA: 0x001160EC File Offset: 0x001144EC
		public override void PlayOut()
		{
			this.ChangeStep(QuestGroupSelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x060036F7 RID: 14071 RVA: 0x00116131 File Offset: 0x00114531
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestGroupSelectUI.eStep.End;
		}

		// Token: 0x060036F8 RID: 14072 RVA: 0x0011613C File Offset: 0x0011453C
		public void OnClickGroupButtonCallBack(int groupID)
		{
			this.m_SelectButton = QuestGroupSelectUI.eButton.Group;
			this.m_SelectGroupID = groupID;
			this.PlayOut();
		}

		// Token: 0x04003D6D RID: 15725
		private QuestGroupSelectUI.eStep m_Step;

		// Token: 0x04003D6E RID: 15726
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003D6F RID: 15727
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003D70 RID: 15728
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003D71 RID: 15729
		private QuestGroupSelectUI.eButton m_SelectButton = QuestGroupSelectUI.eButton.None;

		// Token: 0x04003D72 RID: 15730
		private int m_SelectGroupID = -1;

		// Token: 0x02000A53 RID: 2643
		private enum eStep
		{
			// Token: 0x04003D74 RID: 15732
			Hide,
			// Token: 0x04003D75 RID: 15733
			In,
			// Token: 0x04003D76 RID: 15734
			Idle,
			// Token: 0x04003D77 RID: 15735
			Out,
			// Token: 0x04003D78 RID: 15736
			End
		}

		// Token: 0x02000A54 RID: 2644
		public enum eButton
		{
			// Token: 0x04003D7A RID: 15738
			None = -1,
			// Token: 0x04003D7B RID: 15739
			Group
		}
	}
}
