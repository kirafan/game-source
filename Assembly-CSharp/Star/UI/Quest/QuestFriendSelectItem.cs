﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A55 RID: 2645
	public class QuestFriendSelectItem : ScrollItemIcon
	{
		// Token: 0x060036FA RID: 14074 RVA: 0x0011615C File Offset: 0x0011455C
		protected override void ApplyData()
		{
			base.ApplyData();
			QuestFriendSelectItemData questFriendSelectItemData = (QuestFriendSelectItemData)this.m_Data;
			this.m_CharaIconCloner.GetInst<CharaIconWithFrame>().ApplySupport(questFriendSelectItemData.m_SupportData);
			this.m_CharaIconCloner.GetInst<CharaIconWithFrame>().ChangeDetailTextToSupportSelectSort();
			this.m_UserNameText.text = questFriendSelectItemData.m_UserName;
			this.m_CharaNameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(questFriendSelectItemData.m_CharaID).m_Name;
			if (questFriendSelectItemData.m_WeaponID == -1)
			{
				this.m_WeaponNameText.text = "装備無し";
			}
			else
			{
				this.m_WeaponNameText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(questFriendSelectItemData.m_WeaponID).m_WeaponName;
			}
			this.m_UserLvText.text = questFriendSelectItemData.m_UserLv.ToString().PadLeft(3);
			this.m_LogInTimeText.text = UIUtility.GetLastLoginString(questFriendSelectItemData.m_LogInTime);
			if (questFriendSelectItemData.m_IsFriend)
			{
				this.m_FriendSwapObj.Swap(0);
			}
			else
			{
				this.m_FriendSwapObj.Swap(1);
			}
		}

		// Token: 0x060036FB RID: 14075 RVA: 0x0011628D File Offset: 0x0011468D
		public override void Destroy()
		{
			base.Destroy();
			this.m_CharaIconCloner.GetInst<CharaIconWithFrame>().Destroy();
		}

		// Token: 0x060036FC RID: 14076 RVA: 0x001162A8 File Offset: 0x001146A8
		public void OpenSupportCharaDetail()
		{
			QuestFriendSelectItemData questFriendSelectItemData = (QuestFriendSelectItemData)this.m_Data;
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(questFriendSelectItemData.m_SupportData, false);
		}

		// Token: 0x04003D7C RID: 15740
		[SerializeField]
		private PrefabCloner m_CharaIconCloner;

		// Token: 0x04003D7D RID: 15741
		[SerializeField]
		private Text m_UserNameText;

		// Token: 0x04003D7E RID: 15742
		[SerializeField]
		private Text m_CharaNameText;

		// Token: 0x04003D7F RID: 15743
		[SerializeField]
		private Text m_WeaponNameText;

		// Token: 0x04003D80 RID: 15744
		[SerializeField]
		private Text m_UserLvText;

		// Token: 0x04003D81 RID: 15745
		[SerializeField]
		private Text m_LogInTimeText;

		// Token: 0x04003D82 RID: 15746
		[SerializeField]
		private SwapObj m_FriendSwapObj;
	}
}
