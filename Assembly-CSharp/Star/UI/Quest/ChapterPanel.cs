﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A4A RID: 2634
	public class ChapterPanel : MonoBehaviour
	{
		// Token: 0x17000338 RID: 824
		// (get) Token: 0x060036C0 RID: 14016 RVA: 0x00114BF1 File Offset: 0x00112FF1
		public GameObject GameObject
		{
			get
			{
				return this.m_GameObject;
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x060036C1 RID: 14017 RVA: 0x00114BF9 File Offset: 0x00112FF9
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x140000B3 RID: 179
		// (add) Token: 0x060036C2 RID: 14018 RVA: 0x00114C04 File Offset: 0x00113004
		// (remove) Token: 0x060036C3 RID: 14019 RVA: 0x00114C3C File Offset: 0x0011303C
		public event Action<int> OnClickPanel;

		// Token: 0x060036C4 RID: 14020 RVA: 0x00114C72 File Offset: 0x00113072
		public void Setup()
		{
			this.m_GameObject = base.gameObject;
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x060036C5 RID: 14021 RVA: 0x00114C8C File Offset: 0x0011308C
		public void SetData(int idx, ChapterPanel.ChapterData data)
		{
			this.m_Idx = idx;
			this.m_ChapterTitleImage.sprite = data.m_SpriteHandler.Obj;
			this.m_NewIcon.SetActive(!data.m_IsClear);
			for (int i = 0; i < this.m_DifficultyObj.Length; i++)
			{
				this.m_DifficultyObj[i].SetActive(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetChapterData(data.m_ChapterID, (QuestDefine.eDifficulty)i) != null);
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int j = 0; j < this.m_ClearPerText.Length; j++)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(data.m_ClearPercents[j].ToString().PadLeft(3));
				stringBuilder.Append("%");
				this.m_ClearPerText[j].text = stringBuilder.ToString();
			}
		}

		// Token: 0x060036C6 RID: 14022 RVA: 0x00114D79 File Offset: 0x00113179
		public int GetIdx()
		{
			return this.m_Idx;
		}

		// Token: 0x060036C7 RID: 14023 RVA: 0x00114D81 File Offset: 0x00113181
		public void OnClickButtonCallBack()
		{
			this.OnClickPanel.Call(this.m_Idx);
		}

		// Token: 0x060036C8 RID: 14024 RVA: 0x00114D94 File Offset: 0x00113194
		public Image GetImage()
		{
			return this.m_ChapterTitleImage;
		}

		// Token: 0x04003D2C RID: 15660
		[SerializeField]
		private Image m_ChapterTitleImage;

		// Token: 0x04003D2D RID: 15661
		[SerializeField]
		private GameObject m_NewIcon;

		// Token: 0x04003D2E RID: 15662
		[SerializeField]
		private GameObject[] m_DifficultyObj;

		// Token: 0x04003D2F RID: 15663
		[SerializeField]
		private Text[] m_ClearPerText;

		// Token: 0x04003D30 RID: 15664
		private int m_Idx;

		// Token: 0x04003D31 RID: 15665
		private GameObject m_GameObject;

		// Token: 0x04003D32 RID: 15666
		private RectTransform m_RectTransform;

		// Token: 0x02000A4B RID: 2635
		public struct ChapterData
		{
			// Token: 0x04003D34 RID: 15668
			public int m_ChapterID;

			// Token: 0x04003D35 RID: 15669
			public bool m_IsClear;

			// Token: 0x04003D36 RID: 15670
			public int[] m_ClearPercents;

			// Token: 0x04003D37 RID: 15671
			public SpriteHandler m_SpriteHandler;
		}
	}
}
