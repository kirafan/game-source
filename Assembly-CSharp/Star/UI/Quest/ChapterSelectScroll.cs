﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A4C RID: 2636
	public class ChapterSelectScroll : MonoBehaviour, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x140000B4 RID: 180
		// (add) Token: 0x060036CA RID: 14026 RVA: 0x00114DC0 File Offset: 0x001131C0
		// (remove) Token: 0x060036CB RID: 14027 RVA: 0x00114DF8 File Offset: 0x001131F8
		public event Action<int> OnClickChapterButton;

		// Token: 0x060036CC RID: 14028 RVA: 0x00114E30 File Offset: 0x00113230
		public void Setup()
		{
			this.m_DataList = new List<ChapterPanel.ChapterData>();
			this.m_InstList = new List<ChapterPanel>();
			this.m_EmptyInstList = new List<ChapterPanel>();
			this.m_ScrollRect.GetComponent<ScrollRect>();
			this.m_ViewPort = this.m_ScrollRect.viewport;
			this.m_Content = this.m_ScrollRect.content;
			this.SetSelectChapterIdx(0);
		}

		// Token: 0x060036CD RID: 14029 RVA: 0x00114E94 File Offset: 0x00113294
		public ChapterPanel GetChapterPanel(int chapterIdx)
		{
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				if (this.m_InstList[i].GetIdx() == chapterIdx)
				{
					return this.m_InstList[i];
				}
			}
			return null;
		}

		// Token: 0x060036CE RID: 14030 RVA: 0x00114EE2 File Offset: 0x001132E2
		public int GetExistChapterPanelNum()
		{
			if (this.m_InstList == null)
			{
				return 0;
			}
			return this.m_InstList.Count;
		}

		// Token: 0x060036CF RID: 14031 RVA: 0x00114EFC File Offset: 0x001132FC
		private void UpdateContentWidth()
		{
			this.m_Content.sizeDelta = new Vector2((float)this.m_DataList.Count * (this.m_PanelWidth + this.m_Space) + this.m_ViewPort.rect.width, this.m_Content.sizeDelta.y);
			if (this.m_DataList.Count <= 1)
			{
				this.m_ScrollRect.horizontal = false;
			}
			else
			{
				this.m_ScrollRect.horizontal = true;
			}
		}

		// Token: 0x060036D0 RID: 14032 RVA: 0x00114F88 File Offset: 0x00113388
		private void Update()
		{
			if (this.m_IsDirty)
			{
				this.UpdateContentWidth();
			}
			if (!this.m_Drag)
			{
				float idxPosition = this.GetIdxPosition(this.m_CurrentIdx);
				float num = 0f;
				if (this.m_Content.rect.width > this.m_ViewPort.rect.width)
				{
					num = idxPosition / (this.m_Content.rect.width - this.m_ViewPort.rect.width);
				}
				int num2 = 0;
				while ((float)num2 < 1f + Time.deltaTime * 60f)
				{
					this.m_ScrollRect.horizontalNormalizedPosition += (num - this.m_ScrollRect.horizontalNormalizedPosition) * this.m_ScrollRate;
					num2++;
				}
			}
			else
			{
				this.SetCurrentIdx((int)((-this.m_Content.localPosition.x + (this.m_PanelWidth + this.m_Space) * 0.5f) / (this.m_PanelWidth + this.m_Space)), false);
			}
			this.UpdateItemPosition();
			if (this.m_CurrentIdx <= 0)
			{
				if (this.m_LButton.gameObject.activeSelf)
				{
					this.m_LButton.gameObject.SetActive(false);
				}
			}
			else if (!this.m_LButton.gameObject.activeSelf)
			{
				this.m_LButton.gameObject.SetActive(true);
			}
			if (this.m_CurrentIdx >= this.m_DataList.Count - 1)
			{
				if (this.m_RButton.gameObject.activeSelf)
				{
					this.m_RButton.gameObject.SetActive(false);
				}
			}
			else if (!this.m_RButton.gameObject.activeSelf)
			{
				this.m_RButton.gameObject.SetActive(true);
			}
		}

		// Token: 0x060036D1 RID: 14033 RVA: 0x00115178 File Offset: 0x00113578
		private void UpdateItemPosition()
		{
			if (this.m_DataList.Count <= 0)
			{
				return;
			}
			int num = (int)(-this.m_Content.localPosition.x / (this.m_PanelWidth + this.m_Space));
			this.m_EmptyInstList.Clear();
			for (int i = 0; i < this.m_InstList.Count; i++)
			{
				this.m_EmptyInstList.Add(this.m_InstList[i]);
			}
			for (int j = 0; j < this.MAX_SHOW_PANEL; j++)
			{
				int num2 = num - this.MAX_SHOW_PANEL / 2 + j;
				if (num2 >= 0)
				{
					if (this.m_DataList.Count <= num2)
					{
						break;
					}
					if (this.m_EmptyInstList.Count <= 0)
					{
						break;
					}
					ChapterPanel chapterPanel = this.m_EmptyInstList[0];
					this.m_EmptyInstList.Remove(chapterPanel);
					chapterPanel.GameObject.SetActive(true);
					chapterPanel.SetData(num2, this.m_DataList[num2]);
					RectTransform rectTransform = chapterPanel.RectTransform;
					rectTransform.localPosition = new Vector3(this.m_ViewPort.rect.width * 0.5f + (this.m_PanelWidth + this.m_Space) * (float)num2, -this.m_Content.rect.height * 0.5f, 0f);
				}
			}
			for (int k = 0; k < this.m_EmptyInstList.Count; k++)
			{
				this.m_EmptyInstList[k].GameObject.SetActive(false);
			}
		}

		// Token: 0x060036D2 RID: 14034 RVA: 0x00115330 File Offset: 0x00113730
		public void AddItem(ChapterPanel.ChapterData data)
		{
			if (this.m_InstList.Count < this.MAX_SHOW_PANEL)
			{
				ChapterPanel chapterPanel = UnityEngine.Object.Instantiate<ChapterPanel>(this.m_ChapterPanelPrefab, this.m_Content);
				chapterPanel.Setup();
				RectTransform rectTransform = chapterPanel.RectTransform;
				rectTransform.localScale = Vector3.one;
				chapterPanel.OnClickPanel += this.OnClickChapterButtonCallBack;
				this.m_InstList.Add(chapterPanel);
				if (this.m_DataList.Count == 0)
				{
					this.m_PanelWidth = chapterPanel.GetComponent<RectTransform>().rect.width;
					this.m_Space = (this.m_ViewPort.rect.width - this.m_PanelWidth * (float)this.PANEL_MAX_PER_PAGE) / (float)(this.PANEL_MAX_PER_PAGE + 1);
				}
			}
			this.m_DataList.Add(data);
			this.m_IsDirty = true;
		}

		// Token: 0x060036D3 RID: 14035 RVA: 0x00115409 File Offset: 0x00113809
		public void SetSelectChapterIdx(int chapterIdx)
		{
			this.SetCurrentIdx(chapterIdx, true);
		}

		// Token: 0x060036D4 RID: 14036 RVA: 0x00115414 File Offset: 0x00113814
		public void SetCurrentIdx(int currentIdx, bool immediate = false)
		{
			this.m_CurrentIdx = currentIdx;
			if (this.m_CurrentIdx <= 0)
			{
				this.m_CurrentIdx = 0;
			}
			else if (this.m_CurrentIdx >= this.m_DataList.Count)
			{
				this.m_CurrentIdx = this.m_DataList.Count - 1;
			}
			this.UpdateContentWidth();
			if (immediate)
			{
				float idxPosition = this.GetIdxPosition(this.m_CurrentIdx);
				float horizontalNormalizedPosition = idxPosition / (this.m_Content.rect.width - this.m_ViewPort.rect.width);
				this.m_ScrollRect.horizontalNormalizedPosition = horizontalNormalizedPosition;
			}
		}

		// Token: 0x060036D5 RID: 14037 RVA: 0x001154B8 File Offset: 0x001138B8
		private float GetIdxPosition(int idx)
		{
			return (float)idx * (this.m_PanelWidth + this.m_Space);
		}

		// Token: 0x060036D6 RID: 14038 RVA: 0x001154CA File Offset: 0x001138CA
		public void OnDrag(PointerEventData eventData)
		{
			if (!this.m_Drag)
			{
			}
			this.m_Drag = true;
		}

		// Token: 0x060036D7 RID: 14039 RVA: 0x001154DE File Offset: 0x001138DE
		public void OnEndDrag(PointerEventData eventData)
		{
			this.m_Drag = false;
		}

		// Token: 0x060036D8 RID: 14040 RVA: 0x001154E7 File Offset: 0x001138E7
		public void OnLeftButtonCallBack()
		{
			this.SetCurrentIdx(this.m_CurrentIdx - 1, false);
		}

		// Token: 0x060036D9 RID: 14041 RVA: 0x001154F8 File Offset: 0x001138F8
		public void OnRightButtonCallBack()
		{
			this.SetCurrentIdx(this.m_CurrentIdx + 1, false);
		}

		// Token: 0x060036DA RID: 14042 RVA: 0x0011550C File Offset: 0x0011390C
		public void OnClickChapterButtonCallBack(int idx)
		{
			if (this.m_CurrentIdx == idx)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_2, 1f, 0, -1, -1);
				this.OnClickChapterButton.Call(idx);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
				this.SetCurrentIdx(idx, false);
			}
		}

		// Token: 0x04003D38 RID: 15672
		private int MAX_SHOW_PANEL = 5;

		// Token: 0x04003D39 RID: 15673
		private int PANEL_MAX_PER_PAGE = 3;

		// Token: 0x04003D3A RID: 15674
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x04003D3B RID: 15675
		[SerializeField]
		private ChapterPanel m_ChapterPanelPrefab;

		// Token: 0x04003D3C RID: 15676
		[SerializeField]
		private CustomButton m_LButton;

		// Token: 0x04003D3D RID: 15677
		[SerializeField]
		private CustomButton m_RButton;

		// Token: 0x04003D3E RID: 15678
		private float m_Space;

		// Token: 0x04003D3F RID: 15679
		private List<ChapterPanel.ChapterData> m_DataList;

		// Token: 0x04003D40 RID: 15680
		private List<ChapterPanel> m_InstList;

		// Token: 0x04003D41 RID: 15681
		private List<ChapterPanel> m_EmptyInstList;

		// Token: 0x04003D42 RID: 15682
		private bool m_IsDirty;

		// Token: 0x04003D43 RID: 15683
		private float m_PanelWidth;

		// Token: 0x04003D44 RID: 15684
		private bool m_Drag;

		// Token: 0x04003D45 RID: 15685
		private float m_ScrollRate = 0.1f;

		// Token: 0x04003D46 RID: 15686
		private int m_CurrentIdx;

		// Token: 0x04003D48 RID: 15688
		private RectTransform m_ViewPort;

		// Token: 0x04003D49 RID: 15689
		private RectTransform m_Content;
	}
}
