﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A5E RID: 2654
	public class QuestSelectUI : MenuUIBase
	{
		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06003730 RID: 14128 RVA: 0x00117579 File Offset: 0x00115979
		public QuestSelectUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06003731 RID: 14129 RVA: 0x00117581 File Offset: 0x00115981
		public QuestDefine.eDifficulty SelectDifficulty
		{
			get
			{
				return this.m_SelectDifficulty;
			}
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06003732 RID: 14130 RVA: 0x00117589 File Offset: 0x00115989
		public int SelectQuestID
		{
			get
			{
				return this.m_SelectQuestID;
			}
		}

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06003733 RID: 14131 RVA: 0x00117591 File Offset: 0x00115991
		public int SelectEventID
		{
			get
			{
				return this.m_SelectEventID;
			}
		}

		// Token: 0x140000B8 RID: 184
		// (add) Token: 0x06003734 RID: 14132 RVA: 0x0011759C File Offset: 0x0011599C
		// (remove) Token: 0x06003735 RID: 14133 RVA: 0x001175D4 File Offset: 0x001159D4
		public event Action OnClickQuestButton;

		// Token: 0x140000B9 RID: 185
		// (add) Token: 0x06003736 RID: 14134 RVA: 0x0011760C File Offset: 0x00115A0C
		// (remove) Token: 0x06003737 RID: 14135 RVA: 0x00117644 File Offset: 0x00115A44
		public event Action OnClickChapterButton;

		// Token: 0x06003738 RID: 14136 RVA: 0x0011767C File Offset: 0x00115A7C
		public void Setup()
		{
			QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
			if (selectQuest.m_EventType == -1)
			{
				this.SetupChapter(selectQuest.m_ChapterID, selectQuest.m_Difficulty);
			}
			else
			{
				this.SetupEvent(selectQuest.m_GroupID);
			}
		}

		// Token: 0x06003739 RID: 14137 RVA: 0x001176C8 File Offset: 0x00115AC8
		private void SetupChapter(int chapterId, QuestDefine.eDifficulty difficulty)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_QuestListScroll.Setup();
			this.m_SelectQuestID = -1;
			this.m_SelectEventID = -1;
			this.m_ChapterID = chapterId;
			this.ApplyChapterQuestList(difficulty);
			this.SetScrollPositionByQuestID(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest().m_QuestID);
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			if (!questMng.QuestGetAllApiTypeIsDebugFullOpen())
			{
				if (!questMng.IsClearChapterDifficulty(this.m_ChapterID, QuestDefine.eDifficulty.Normal) || questMng.GetChapterData(chapterId, QuestDefine.eDifficulty.Hard) == null)
				{
					this.m_DifficultyButtonHard.gameObject.SetActive(false);
					this.m_DifficultyButtonStar.gameObject.SetActive(false);
				}
				else if (!questMng.IsClearChapterDifficulty(this.m_ChapterID, QuestDefine.eDifficulty.Hard) || questMng.GetChapterData(chapterId, QuestDefine.eDifficulty.Star) == null)
				{
					this.m_DifficultyButtonStar.gameObject.SetActive(false);
				}
			}
			this.m_GroupID = -1;
			this.m_StoryObj.SetActive(true);
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.QuestStory_0 + chapterId - 1, true);
		}

		// Token: 0x0600373A RID: 14138 RVA: 0x00117808 File Offset: 0x00115C08
		private void ApplyChapterQuestList(QuestDefine.eDifficulty difficulty)
		{
			this.m_SelectDifficulty = difficulty;
			this.m_QuestListScroll.RemoveAllData();
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			QuestManager.ChapterData chapterData = questMng.GetChapterData(this.m_ChapterID, difficulty);
			int num = chapterData.m_QuestIDs.Length;
			int i = num - 1;
			if (num > 0)
			{
				if (!questMng.QuestGetAllApiTypeIsDebugFullOpen())
				{
					for (i = num - 1; i >= 0; i--)
					{
						if (i <= 0)
						{
							break;
						}
						int questID = chapterData.m_QuestIDs[i - 1];
						QuestManager.QuestData questData = questMng.GetQuestData(questID);
						if (questData == null)
						{
							break;
						}
						if (questData.IsCleared())
						{
							break;
						}
					}
				}
				for (int j = i; j >= 0; j--)
				{
					int questID2 = chapterData.m_QuestIDs[j];
					QuestListItemData questListItemData = new QuestListItemData(j, questID2, this.m_SelectDifficulty, null);
					QuestListItemData questListItemData2 = questListItemData;
					questListItemData2.OnClick = (Action<int, int>)Delegate.Combine(questListItemData2.OnClick, new Action<int, int>(this.OnClickQuestButtonCallBack));
					QuestListItemData questListItemData3 = questListItemData;
					questListItemData3.OnClickDropItem = (Action<int>)Delegate.Combine(questListItemData3.OnClickDropItem, new Action<int>(this.OnClickDropItemButtonCallBack));
					this.m_QuestListScroll.AddItem(questListItemData);
				}
			}
			this.m_ChapterTitleTextObj.text = chapterData.m_ChapterName;
		}

		// Token: 0x0600373B RID: 14139 RVA: 0x00117950 File Offset: 0x00115D50
		private void SetupEvent(int groupID)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_QuestListScroll.Setup();
			this.m_SelectQuestID = -1;
			this.m_SelectEventID = -1;
			QuestManager questMng = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng;
			QuestManager.EventGroupData eventGroupData = questMng.GetEventGroupData(groupID);
			this.m_ChapterTitleTextObj.text = eventGroupData.m_GroupName;
			int count = eventGroupData.m_EventDatas.Count;
			if (count > 0)
			{
				for (int i = count - 1; i >= 0; i--)
				{
					int questID = eventGroupData.m_EventDatas[i].m_QuestID;
					QuestListItemData questListItemData = new QuestListItemData(i, questID, QuestDefine.eDifficulty.Normal, eventGroupData.m_EventDatas[i]);
					QuestListItemData questListItemData2 = questListItemData;
					questListItemData2.OnClick = (Action<int, int>)Delegate.Combine(questListItemData2.OnClick, new Action<int, int>(this.OnClickQuestButtonCallBack));
					QuestListItemData questListItemData3 = questListItemData;
					questListItemData3.OnClickDropItem = (Action<int>)Delegate.Combine(questListItemData3.OnClickDropItem, new Action<int>(this.OnClickDropItemButtonCallBack));
					this.m_QuestListScroll.AddItem(questListItemData);
				}
			}
			this.SetScrollPositionByQuestID(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest().m_QuestID);
			this.m_ChapterID = -1;
			this.m_GroupID = groupID;
			this.m_StoryObj.SetActive(false);
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Quest, true);
		}

		// Token: 0x0600373C RID: 14140 RVA: 0x00117AC0 File Offset: 0x00115EC0
		private void SetScrollPositionByQuestID(int questID)
		{
			for (int i = 0; i < this.m_QuestListScroll.GetDataList().Count; i++)
			{
				if (((QuestListItemData)this.m_QuestListScroll.GetDataList()[i]).m_QuestID == questID)
				{
					this.m_QuestListScroll.SetScrollValueFromIdx(i);
					return;
				}
			}
		}

		// Token: 0x0600373D RID: 14141 RVA: 0x00117B1C File Offset: 0x00115F1C
		private void Update()
		{
			switch (this.m_Step)
			{
			case QuestSelectUI.eStep.In:
			{
				bool flag = true;
				if (SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					flag = false;
				}
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(QuestSelectUI.eStep.Idle);
				}
				break;
			}
			case QuestSelectUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(QuestSelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600373E RID: 14142 RVA: 0x00117C2C File Offset: 0x0011602C
		private void ChangeStep(QuestSelectUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case QuestSelectUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestSelectUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case QuestSelectUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestSelectUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case QuestSelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600373F RID: 14143 RVA: 0x00117CE8 File Offset: 0x001160E8
		public override void PlayIn()
		{
			this.ChangeStep(QuestSelectUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003740 RID: 14144 RVA: 0x00117D24 File Offset: 0x00116124
		public override void PlayOut()
		{
			this.ChangeStep(QuestSelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003741 RID: 14145 RVA: 0x00117D5E File Offset: 0x0011615E
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestSelectUI.eStep.End;
		}

		// Token: 0x06003742 RID: 14146 RVA: 0x00117D69 File Offset: 0x00116169
		public void OnClickChapterSelectButtonCallBack()
		{
			this.m_SelectButton = QuestSelectUI.eButton.Chapter;
			this.OnClickChapterButton.Call();
		}

		// Token: 0x06003743 RID: 14147 RVA: 0x00117D7D File Offset: 0x0011617D
		public void OnClickQuestButtonCallBack(int questID, int eventID)
		{
			this.m_SelectQuestID = questID;
			this.m_SelectEventID = eventID;
			this.m_SelectButton = QuestSelectUI.eButton.Quest;
			this.OnClickQuestButton.Call();
		}

		// Token: 0x06003744 RID: 14148 RVA: 0x00117DA0 File Offset: 0x001161A0
		public void OnClickDifficultyButtonCallBack(int difficulty)
		{
			if (this.m_SelectDifficulty != (QuestDefine.eDifficulty)difficulty)
			{
				float scrollValue = this.m_QuestListScroll.GetScrollValue();
				this.ApplyChapterQuestList((QuestDefine.eDifficulty)difficulty);
				this.m_QuestListScroll.SetScrollValue(scrollValue);
			}
		}

		// Token: 0x06003745 RID: 14149 RVA: 0x00117DD8 File Offset: 0x001161D8
		public void OnClickDropItemButtonCallBack(int questID)
		{
			this.m_DropWindow.Open(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetDropItemIDs(questID));
		}

		// Token: 0x04003DD6 RID: 15830
		private QuestSelectUI.eStep m_Step;

		// Token: 0x04003DD7 RID: 15831
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003DD8 RID: 15832
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003DD9 RID: 15833
		[SerializeField]
		private Text m_ChapterTitleTextObj;

		// Token: 0x04003DDA RID: 15834
		[SerializeField]
		private ScrollViewBase m_QuestListScroll;

		// Token: 0x04003DDB RID: 15835
		[SerializeField]
		private GameObject m_StoryObj;

		// Token: 0x04003DDC RID: 15836
		[SerializeField]
		private DropItemWindow m_DropWindow;

		// Token: 0x04003DDD RID: 15837
		[SerializeField]
		private CustomButton m_DifficultyButtonNormal;

		// Token: 0x04003DDE RID: 15838
		[SerializeField]
		private CustomButton m_DifficultyButtonHard;

		// Token: 0x04003DDF RID: 15839
		[SerializeField]
		private CustomButton m_DifficultyButtonStar;

		// Token: 0x04003DE0 RID: 15840
		private QuestSelectUI.eButton m_SelectButton = QuestSelectUI.eButton.None;

		// Token: 0x04003DE1 RID: 15841
		private int m_SelectQuestID = -1;

		// Token: 0x04003DE2 RID: 15842
		private int m_SelectEventID = -1;

		// Token: 0x04003DE3 RID: 15843
		private QuestDefine.eDifficulty m_SelectDifficulty;

		// Token: 0x04003DE4 RID: 15844
		private int m_ChapterID = -1;

		// Token: 0x04003DE5 RID: 15845
		private int m_GroupID = -1;

		// Token: 0x02000A5F RID: 2655
		private enum eStep
		{
			// Token: 0x04003DE9 RID: 15849
			Hide,
			// Token: 0x04003DEA RID: 15850
			In,
			// Token: 0x04003DEB RID: 15851
			Idle,
			// Token: 0x04003DEC RID: 15852
			Out,
			// Token: 0x04003DED RID: 15853
			End
		}

		// Token: 0x02000A60 RID: 2656
		public enum eButton
		{
			// Token: 0x04003DEF RID: 15855
			None = -1,
			// Token: 0x04003DF0 RID: 15856
			Chapter,
			// Token: 0x04003DF1 RID: 15857
			Quest
		}
	}
}
