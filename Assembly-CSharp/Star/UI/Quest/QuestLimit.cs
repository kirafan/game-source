﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A5B RID: 2651
	public class QuestLimit : MonoBehaviour
	{
		// Token: 0x06003722 RID: 14114 RVA: 0x00116B45 File Offset: 0x00114F45
		public void SetupClassLimit(eClassType limitClass)
		{
			this.m_ClassIcon.Apply(limitClass);
		}

		// Token: 0x06003723 RID: 14115 RVA: 0x00116B53 File Offset: 0x00114F53
		public void SetupElementLimit(eElementType elementType)
		{
			this.m_ElementIcon.Apply(elementType);
		}

		// Token: 0x06003724 RID: 14116 RVA: 0x00116B64 File Offset: 0x00114F64
		public void SetupNamedLimit(eCharaNamedType named)
		{
			this.m_NamedText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(named).m_NickName;
		}

		// Token: 0x06003725 RID: 14117 RVA: 0x00116B99 File Offset: 0x00114F99
		public void SetupRareLimit(eRare rare)
		{
			this.m_Rare.Apply(rare);
		}

		// Token: 0x06003726 RID: 14118 RVA: 0x00116BA8 File Offset: 0x00114FA8
		public void SetupCostLimit(int cost)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharaCost));
			stringBuilder.Append(cost);
			stringBuilder.Append("以下");
			this.m_CostText.text = stringBuilder.ToString();
		}

		// Token: 0x06003727 RID: 14119 RVA: 0x00116BFC File Offset: 0x00114FFC
		public void SetupTitleLimit(eTitleType title)
		{
			this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB.GetParam(title).m_DisplayName;
		}

		// Token: 0x04003DA8 RID: 15784
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x04003DA9 RID: 15785
		[SerializeField]
		private ElementIcon m_ElementIcon;

		// Token: 0x04003DAA RID: 15786
		[SerializeField]
		private Text m_NamedText;

		// Token: 0x04003DAB RID: 15787
		[SerializeField]
		private RareStar m_Rare;

		// Token: 0x04003DAC RID: 15788
		[SerializeField]
		private Text m_CostText;

		// Token: 0x04003DAD RID: 15789
		[SerializeField]
		private Text m_TitleText;
	}
}
