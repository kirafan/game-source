﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Quest
{
	// Token: 0x02000A57 RID: 2647
	public class QuestFriendSelectUI : MenuUIBase
	{
		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06003702 RID: 14082 RVA: 0x001163EB File Offset: 0x001147EB
		public QuestFriendSelectUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06003703 RID: 14083 RVA: 0x001163F3 File Offset: 0x001147F3
		public FriendManager.FriendData SelectFriendData
		{
			get
			{
				return this.m_SelectFriendData;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06003704 RID: 14084 RVA: 0x001163FB File Offset: 0x001147FB
		public UserSupportData SelectSupport
		{
			get
			{
				return this.m_SelectSupport;
			}
		}

		// Token: 0x140000B6 RID: 182
		// (add) Token: 0x06003705 RID: 14085 RVA: 0x00116404 File Offset: 0x00114804
		// (remove) Token: 0x06003706 RID: 14086 RVA: 0x0011643C File Offset: 0x0011483C
		public event Action OnClickButton;

		// Token: 0x140000B7 RID: 183
		// (add) Token: 0x06003707 RID: 14087 RVA: 0x00116474 File Offset: 0x00114874
		// (remove) Token: 0x06003708 RID: 14088 RVA: 0x001164AC File Offset: 0x001148AC
		public event Action OnClickReloadButton;

		// Token: 0x06003709 RID: 14089 RVA: 0x001164E4 File Offset: 0x001148E4
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.RefreshFriendSupportList();
			this.m_Scroll.Setup();
			eTitleType title = this.GetTitle();
			this.Apply(title);
		}

		// Token: 0x0600370A RID: 14090 RVA: 0x00116543 File Offset: 0x00114943
		public void Refresh()
		{
			this.Apply(this.m_SelectTitle);
		}

		// Token: 0x0600370B RID: 14091 RVA: 0x00116554 File Offset: 0x00114954
		private eTitleType GetTitle()
		{
			bool[] filterFlagsWithAll = SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagsWithAll(UISettings.eUsingType_Filter.SupportTitle, UISettings.eFilterCategory.TitleSelect);
			int i = 0;
			while (i < filterFlagsWithAll.Length)
			{
				if (filterFlagsWithAll[i])
				{
					eTitleType eTitleType = (eTitleType)(i - 1);
					if (eTitleType == eTitleType.None || SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TitleListDB.IsExist(eTitleType))
					{
						return eTitleType;
					}
					return eTitleType.None;
				}
				else
				{
					i++;
				}
			}
			return eTitleType.None;
		}

		// Token: 0x0600370C RID: 14092 RVA: 0x001165BC File Offset: 0x001149BC
		private void Apply(eTitleType title)
		{
			this.m_SelectTitle = title;
			this.m_RandomTitle.SetActive(this.m_SelectTitle == eTitleType.None);
			this.m_Scroll.RemoveAll();
			List<FriendManager.FriendData> list = new List<FriendManager.FriendData>();
			List<UserSupportData> list2 = new List<UserSupportData>();
			long mngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex].MngID;
			bool flag = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.FilterFriendList(this.m_SelectTitle, mngID, ref list, ref list2);
			if (flag)
			{
				for (int i = 0; i < list.Count; i++)
				{
					QuestFriendSelectItemData item = new QuestFriendSelectItemData(list[i], list2[i], new Action<FriendManager.FriendData, UserSupportData>(this.OnClickFriendButtonCallBack));
					this.m_Scroll.AddItem(item);
				}
			}
			this.m_TitleLogo.Apply(title);
		}

		// Token: 0x0600370D RID: 14093 RVA: 0x00116698 File Offset: 0x00114A98
		private void Update()
		{
			if (this.m_ReloadWaitTime > 0f)
			{
				this.m_ReloadWaitTime -= Time.deltaTime;
			}
			switch (this.m_Step)
			{
			case QuestFriendSelectUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(QuestFriendSelectUI.eStep.Idle);
				}
				break;
			}
			case QuestFriendSelectUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(QuestFriendSelectUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600370E RID: 14094 RVA: 0x001167B4 File Offset: 0x00114BB4
		private void ChangeStep(QuestFriendSelectUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case QuestFriendSelectUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case QuestFriendSelectUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Open();
				break;
			case QuestFriendSelectUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case QuestFriendSelectUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_MainGroup.Close();
				break;
			case QuestFriendSelectUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600370F RID: 14095 RVA: 0x00116870 File Offset: 0x00114C70
		public override void PlayIn()
		{
			this.ChangeStep(QuestFriendSelectUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
		}

		// Token: 0x06003710 RID: 14096 RVA: 0x001168AC File Offset: 0x00114CAC
		public override void PlayOut()
		{
			this.ChangeStep(QuestFriendSelectUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x06003711 RID: 14097 RVA: 0x001168E6 File Offset: 0x00114CE6
		public override bool IsMenuEnd()
		{
			return this.m_Step == QuestFriendSelectUI.eStep.End;
		}

		// Token: 0x06003712 RID: 14098 RVA: 0x001168F1 File Offset: 0x00114CF1
		public void OnClickBackButtonCallBack()
		{
			this.m_SelectButton = QuestFriendSelectUI.eButton.Back;
			this.OnClickButton.Call();
		}

		// Token: 0x06003713 RID: 14099 RVA: 0x00116905 File Offset: 0x00114D05
		public void OnClickFriendButtonCallBack(FriendManager.FriendData friendData, UserSupportData support)
		{
			this.m_SelectButton = QuestFriendSelectUI.eButton.Friend;
			this.m_SelectFriendData = friendData;
			this.m_SelectSupport = support;
			this.OnClickButton.Call();
		}

		// Token: 0x06003714 RID: 14100 RVA: 0x00116927 File Offset: 0x00114D27
		public void OnClickNoFriendButtonCallBack()
		{
			this.m_SelectButton = QuestFriendSelectUI.eButton.Friend;
			this.m_SelectFriendData = null;
			this.m_SelectSupport = null;
			this.OnClickButton.Call();
		}

		// Token: 0x06003715 RID: 14101 RVA: 0x00116949 File Offset: 0x00114D49
		public void OpenTitleFilterWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.SupportTitle, new Action(this.ChangeTitle)), null, null);
		}

		// Token: 0x06003716 RID: 14102 RVA: 0x0011696F File Offset: 0x00114D6F
		public void ChangeTitle()
		{
			this.Apply(this.GetTitle());
		}

		// Token: 0x06003717 RID: 14103 RVA: 0x00116980 File Offset: 0x00114D80
		public void OnClickReloadButtonCallBack()
		{
			if (this.m_ReloadWaitTime <= 0f)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.RefreshFriendSupportList();
				this.m_ReloadWaitTime = 15f;
				this.OnClickReloadButton.Call();
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.QuestFriendListReloadTitle, eText_MessageDB.QuestFriendListReloadMessageErr, null);
			}
		}

		// Token: 0x06003718 RID: 14104 RVA: 0x001169E2 File Offset: 0x00114DE2
		public void OpenFilterWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.SupportSelect, new Action(this.OnExecuteFilter)), null, null);
		}

		// Token: 0x06003719 RID: 14105 RVA: 0x00116A08 File Offset: 0x00114E08
		public void OnExecuteFilter()
		{
			this.Refresh();
		}

		// Token: 0x0600371A RID: 14106 RVA: 0x00116A10 File Offset: 0x00114E10
		public void OpenSortWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Sort, new SortUIArg(UISettings.eUsingType_Sort.SupportSelect, new Action(this.OnExecuteSort)), null, null);
		}

		// Token: 0x0600371B RID: 14107 RVA: 0x00116A36 File Offset: 0x00114E36
		public void OnExecuteSort()
		{
			this.Refresh();
		}

		// Token: 0x04003D8C RID: 15756
		private const float RELOAD_WAIT = 15f;

		// Token: 0x04003D8D RID: 15757
		private QuestFriendSelectUI.eStep m_Step;

		// Token: 0x04003D8E RID: 15758
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003D8F RID: 15759
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003D90 RID: 15760
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003D91 RID: 15761
		[SerializeField]
		private ContentTitleLogo m_TitleLogo;

		// Token: 0x04003D92 RID: 15762
		[SerializeField]
		private GameObject m_RandomTitle;

		// Token: 0x04003D93 RID: 15763
		private QuestFriendSelectUI.eButton m_SelectButton = QuestFriendSelectUI.eButton.None;

		// Token: 0x04003D94 RID: 15764
		private eTitleType m_SelectTitle = eTitleType.None;

		// Token: 0x04003D95 RID: 15765
		private FriendManager.FriendData m_SelectFriendData;

		// Token: 0x04003D96 RID: 15766
		private UserSupportData m_SelectSupport;

		// Token: 0x04003D97 RID: 15767
		private float m_ReloadWaitTime;

		// Token: 0x02000A58 RID: 2648
		private enum eStep
		{
			// Token: 0x04003D9B RID: 15771
			Hide,
			// Token: 0x04003D9C RID: 15772
			In,
			// Token: 0x04003D9D RID: 15773
			Idle,
			// Token: 0x04003D9E RID: 15774
			Out,
			// Token: 0x04003D9F RID: 15775
			End
		}

		// Token: 0x02000A59 RID: 2649
		public enum eButton
		{
			// Token: 0x04003DA1 RID: 15777
			None = -1,
			// Token: 0x04003DA2 RID: 15778
			Back,
			// Token: 0x04003DA3 RID: 15779
			Friend
		}
	}
}
