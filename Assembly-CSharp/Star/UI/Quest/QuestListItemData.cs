﻿using System;

namespace Star.UI.Quest
{
	// Token: 0x02000A5D RID: 2653
	public class QuestListItemData : ScrollItemData
	{
		// Token: 0x0600372D RID: 14125 RVA: 0x001174C4 File Offset: 0x001158C4
		public QuestListItemData(int idx, int questID, QuestDefine.eDifficulty difficulty, QuestManager.EventData eventData)
		{
			this.m_Idx = idx;
			this.m_QuestID = questID;
			this.m_Difficulty = difficulty;
			this.m_EventData = eventData;
			if (this.m_EventData != null)
			{
				this.m_TimeSpan = QuestUtility.CalcTimeSpan(this.m_EventData);
			}
		}

		// Token: 0x0600372E RID: 14126 RVA: 0x00117510 File Offset: 0x00115910
		public override void OnClickItemCallBack()
		{
			base.OnClickItemCallBack();
			int arg = -1;
			if (this.m_EventData != null)
			{
				arg = this.m_EventData.m_EventID;
			}
			this.OnClick.Call(this.m_QuestID, arg);
		}

		// Token: 0x04003DCF RID: 15823
		public int m_Idx;

		// Token: 0x04003DD0 RID: 15824
		public int m_QuestID;

		// Token: 0x04003DD1 RID: 15825
		public QuestDefine.eDifficulty m_Difficulty;

		// Token: 0x04003DD2 RID: 15826
		public QuestManager.EventData m_EventData;

		// Token: 0x04003DD3 RID: 15827
		public TimeSpan m_TimeSpan;

		// Token: 0x04003DD4 RID: 15828
		public Action<int, int> OnClick;

		// Token: 0x04003DD5 RID: 15829
		public Action<int> OnClickDropItem;
	}
}
