﻿using System;
using Meige;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Quest
{
	// Token: 0x02000A48 RID: 2632
	public class ChapterOpenAnim : MonoBehaviour
	{
		// Token: 0x060036B3 RID: 14003 RVA: 0x001141C9 File Offset: 0x001125C9
		public bool IsDoneSetTarget()
		{
			return this.m_TargetChapterPanel != null;
		}

		// Token: 0x060036B4 RID: 14004 RVA: 0x001141D7 File Offset: 0x001125D7
		private void Start()
		{
			this.m_LabelObj.SetActive(false);
		}

		// Token: 0x060036B5 RID: 14005 RVA: 0x001141E8 File Offset: 0x001125E8
		private void OnDestroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.enabled = false;
			SingletonMonoBehaviour<GameSystem>.Inst.UICamera.clearFlags = CameraClearFlags.Depth;
			this.m_PixelCrashWrapperPrefab = null;
			if (this.m_PixelCrashWrapper != null)
			{
				Helper.DestroyAll(this.m_PixelCrashWrapper.gameObject);
				this.m_PixelCrashWrapper = null;
			}
			if (this.m_ChapterPanelClone != null)
			{
				UnityEngine.Object.Destroy(this.m_ChapterPanelClone);
				this.m_ChapterPanelClone = null;
			}
			this.m_SaveAreasCanvas = null;
		}

		// Token: 0x060036B6 RID: 14006 RVA: 0x00114270 File Offset: 0x00112670
		public void SetChapterPanelObj(ChapterPanel obj)
		{
			this.m_TargetChapterPanel = obj;
			this.m_PanelEffectRect.position = this.m_TargetChapterPanel.GetComponent<RectTransform>().position;
			this.m_ImageDark.sprite = this.m_TargetChapterPanel.GetImage().sprite;
			this.m_ImageWhite.sprite = this.m_TargetChapterPanel.GetImage().sprite;
			this.m_ImageAdd.sprite = this.m_TargetChapterPanel.GetImage().sprite;
			SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.orthographicSize = SingletonMonoBehaviour<GameSystem>.Inst.UICamera.orthographicSize;
			SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.enabled = true;
			SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.clearFlags = CameraClearFlags.Color;
			SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.backgroundColor = new Color(0f, 0f, 0f, 0f);
			if (this.m_ChapterPanelClone == null)
			{
				this.m_ChapterPanelClone = UnityEngine.Object.Instantiate<ChapterPanel>(obj);
			}
			this.m_ChapterPanelClone.transform.parent = this.m_SaveAreasCanvas.gameObject.transform;
			this.m_ChapterPanelClone.gameObject.SetLayer(LayerMask.NameToLayer("SaveArea"), true);
			if (this.m_PixelCrashWrapper == null)
			{
				this.m_PixelCrashWrapper = UnityEngine.Object.Instantiate<PixelCrashWrapper>(this.m_PixelCrashWrapperPrefab);
			}
			this.m_PixelCrashWrapper.enabled = true;
			this.m_PixelCrashWrapper.m_RenderCamera = SingletonMonoBehaviour<GameSystem>.Inst.UICamera;
			this.m_PixelCrashWrapper.m_CrashTargetCamera = SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera;
			this.m_PixelCrashWrapper.Setup();
			this.m_PixelCrashWrapper.m_Param.m_HDRFactor = 1f;
			this.m_PixelCrashWrapper.m_Param.m_ChangeHDRFactor = 0.75f;
			this.m_PixelCrashWrapper.m_Param.m_BaseAddColor = this.m_PixelCrashBaseColor;
			this.m_PixelCrashWrapper.m_Param.m_ShapeArgment = new Vector4(0.5f, 0.1f, 0f, 1f);
			this.m_PostProcessRenderer = SingletonMonoBehaviour<GameSystem>.Inst.UICamera.gameObject.GetComponent<PostProcessRenderer>();
			if (this.m_PostProcessRenderer == null)
			{
				this.m_PostProcessRenderer = SingletonMonoBehaviour<GameSystem>.Inst.UICamera.gameObject.AddComponent<PostProcessRenderer>();
			}
			if (this.m_PostProcessRenderer != null)
			{
				this.m_PostProcessRenderer.enableCorrectToneCurve = false;
				this.m_PostProcessRenderer.enableCorrectContrast = false;
				this.m_PostProcessRenderer.enableCorrectBrightness = false;
				this.m_PostProcessRenderer.enableCorrectChroma = false;
				this.m_PostProcessRenderer.enableCorrectColorBlend = false;
				this.m_PostProcessRenderer.enableBloom = true;
				this.m_PostProcessRenderer.bloomBrightThresholdOffset = 1f;
				this.m_PostProcessRenderer.bloomOffset = 2f;
				this.m_PostProcessRenderer.bloomIntensity = 2f;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.UICamera.clearFlags = CameraClearFlags.Color;
			foreach (Image image in this.m_TargetChapterPanel.GetComponentsInChildren<Image>(true))
			{
				image.enabled = false;
			}
			foreach (Text text in this.m_TargetChapterPanel.GetComponentsInChildren<Text>(true))
			{
				text.enabled = false;
			}
		}

		// Token: 0x060036B7 RID: 14007 RVA: 0x001145B9 File Offset: 0x001129B9
		public ChapterPanel GetChapterPanelObj()
		{
			return this.m_TargetChapterPanel;
		}

		// Token: 0x060036B8 RID: 14008 RVA: 0x001145C4 File Offset: 0x001129C4
		public void Play()
		{
			this.m_ScreenObj.SetActive(false);
			this.m_LabelObj.SetActive(false);
			this.m_ChapterPanelClone.gameObject.SetActive(true);
			if (this.m_PixelCrashWrapper.m_Param.m_Type == PixelCrashWrapper.eType.Line)
			{
				if (this.m_PixelCrashWrapper.m_Param.m_RateMove == PixelCrashWrapper.eMove.Acceleration)
				{
					this.m_PixelCrashWrapper.Play(0.9f);
				}
				else if (this.m_PixelCrashWrapper.m_Param.m_RateMove == PixelCrashWrapper.eMove.Acceleration_x2)
				{
					this.m_PixelCrashWrapper.Play(1f);
				}
				else if (this.m_PixelCrashWrapper.m_Param.m_RateMove == PixelCrashWrapper.eMove.Slowdown)
				{
					this.m_PixelCrashWrapper.Play(0.5f);
				}
				else if (this.m_PixelCrashWrapper.m_Param.m_RateMove == PixelCrashWrapper.eMove.Slowdown_x2)
				{
					this.m_PixelCrashWrapper.Play(0.6f);
				}
				else
				{
					this.m_PixelCrashWrapper.Play(0.6f);
				}
			}
			else
			{
				this.m_PixelCrashWrapper.Play(0f);
			}
			this.m_IsFirstFadeOut = false;
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			this.m_Step = ChapterOpenAnim.eStep.Play;
		}

		// Token: 0x060036B9 RID: 14009 RVA: 0x0011470A File Offset: 0x00112B0A
		public bool IsPlaying()
		{
			return this.m_Step != ChapterOpenAnim.eStep.None;
		}

		// Token: 0x060036BA RID: 14010 RVA: 0x00114718 File Offset: 0x00112B18
		public void Initialize()
		{
			this.m_ScreenObj.SetActive(false);
			this.m_PanelEffectRect.gameObject.SetActive(false);
			this.m_LabelObj.SetActive(false);
			this.m_IsFirstFadeOut = false;
		}

		// Token: 0x060036BB RID: 14011 RVA: 0x0011474C File Offset: 0x00112B4C
		public void SetUpFirst()
		{
			this.m_PanelEffectRect.gameObject.SetActive(true);
			this.m_ImageDark.color = new Color(0f, 0f, 0f, 1f);
			this.m_Fade.color = new Color(0f, 0f, 0f, 0f);
			this.m_ImageWhite.color = new Color(1f, 1f, 1f, 0f);
			this.m_ImageAdd.color = new Color(1f, 1f, 1f, 0f);
			this.m_IsFirstFadeOut = true;
		}

		// Token: 0x060036BC RID: 14012 RVA: 0x00114804 File Offset: 0x00112C04
		private void Update()
		{
			if (this.m_IsFirstFadeOut)
			{
				float a = Mathf.Clamp(this.m_Fade.color.a + Time.deltaTime * 2f, 0f, 0.5f);
				this.m_Fade.color = new Color(0f, 0f, 0f, a);
			}
			switch (this.m_Step)
			{
			case ChapterOpenAnim.eStep.None:
				return;
			case ChapterOpenAnim.eStep.Play:
				if (!this.m_PixelCrashWrapper.isAlive || this.m_PixelCrashWrapper.rate <= 0.15f)
				{
					this.m_TargetChapterPanel.gameObject.SetActive(true);
					foreach (Image image in this.m_TargetChapterPanel.GetComponentsInChildren<Image>(true))
					{
						image.enabled = true;
					}
					foreach (Text text in this.m_TargetChapterPanel.GetComponentsInChildren<Text>(true))
					{
						text.enabled = true;
					}
					this.m_FadeoutRate = 0f;
					this.m_Step = ChapterOpenAnim.eStep.PlayInterval;
				}
				break;
			case ChapterOpenAnim.eStep.PlayInterval:
			{
				this.m_FadeoutRate = Mathf.Min(new float[]
				{
					this.m_FadeoutRate + Time.deltaTime * 1.5f
				});
				if (this.m_FadeoutRate >= 1f)
				{
					this.m_ChapterPanelClone.gameObject.SetActive(false);
					this.m_ScreenObj.SetActive(true);
					this.m_LabelObj.SetActive(true);
					this.m_Anim[this.m_Anim.clip.name].speed = 1f;
					this.m_Anim.Play();
					this.m_Step = ChapterOpenAnim.eStep.PlaySecond;
				}
				float num = 1f - this.m_FadeoutRate;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.r = num * this.m_PixelCrashBaseColor.r;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.g = num * this.m_PixelCrashBaseColor.g;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.b = num * this.m_PixelCrashBaseColor.b;
				this.m_PixelCrashWrapper.m_Param.m_BaseAddColor.a = num;
				this.m_PixelCrashWrapper.SetBaseAddColor();
				this.m_PixelCrashWrapper.SetMeshScale(1f + this.m_FadeoutRate * 0.175f);
				break;
			}
			case ChapterOpenAnim.eStep.PlaySecond:
				if (!this.m_Anim.isPlaying)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_Step = ChapterOpenAnim.eStep.WaitTap;
				}
				break;
			case ChapterOpenAnim.eStep.Out:
				if (!this.m_OutAnim.IsPlaying)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SaveAreaCamera.enabled = false;
					SingletonMonoBehaviour<GameSystem>.Inst.UICamera.clearFlags = CameraClearFlags.Depth;
					if (this.m_PostProcessRenderer != null)
					{
						UnityEngine.Object.Destroy(this.m_PostProcessRenderer);
					}
					this.m_ScreenObj.SetActive(false);
					this.m_Step = ChapterOpenAnim.eStep.None;
				}
				break;
			}
			if (this.m_ChapterPanelClone != null)
			{
				ChapterPanel chapterPanelObj = this.GetChapterPanelObj();
				if (chapterPanelObj != null)
				{
					Transform transform = this.m_ChapterPanelClone.transform;
					Transform transform2 = chapterPanelObj.transform;
					transform.position = transform2.transform.position;
					transform.rotation = transform2.transform.rotation;
					transform.localScale = transform2.transform.localScale;
				}
			}
		}

		// Token: 0x060036BD RID: 14013 RVA: 0x00114BBB File Offset: 0x00112FBB
		public void PlayIdle()
		{
			this.m_IdleAnim.Play();
		}

		// Token: 0x060036BE RID: 14014 RVA: 0x00114BC9 File Offset: 0x00112FC9
		public void OnClickWaitTapCallBack()
		{
			if (this.m_Step == ChapterOpenAnim.eStep.WaitTap)
			{
				this.m_Step = ChapterOpenAnim.eStep.Out;
				this.m_OutAnim.PlayOut();
			}
		}

		// Token: 0x04003D11 RID: 15633
		private ChapterPanel m_TargetChapterPanel;

		// Token: 0x04003D12 RID: 15634
		[SerializeField]
		private Image m_ImageDark;

		// Token: 0x04003D13 RID: 15635
		[SerializeField]
		private Image m_ImageWhite;

		// Token: 0x04003D14 RID: 15636
		[SerializeField]
		private Image m_ImageAdd;

		// Token: 0x04003D15 RID: 15637
		[SerializeField]
		[Tooltip("画面全体オブジェクト")]
		private GameObject m_ScreenObj;

		// Token: 0x04003D16 RID: 15638
		[SerializeField]
		private Image m_Fade;

		// Token: 0x04003D17 RID: 15639
		[SerializeField]
		private GameObject m_LabelObj;

		// Token: 0x04003D18 RID: 15640
		[SerializeField]
		private RectTransform m_PanelEffectRect;

		// Token: 0x04003D19 RID: 15641
		[SerializeField]
		private AlphaAnimUI m_OutAnim;

		// Token: 0x04003D1A RID: 15642
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04003D1B RID: 15643
		[SerializeField]
		private Animation m_IdleAnim;

		// Token: 0x04003D1C RID: 15644
		private bool m_IsFirstFadeOut;

		// Token: 0x04003D1D RID: 15645
		[SerializeField]
		private PixelCrashWrapper m_PixelCrashWrapperPrefab;

		// Token: 0x04003D1E RID: 15646
		private PixelCrashWrapper m_PixelCrashWrapper;

		// Token: 0x04003D1F RID: 15647
		private ChapterPanel m_ChapterPanelClone;

		// Token: 0x04003D20 RID: 15648
		[SerializeField]
		private Canvas m_SaveAreasCanvas;

		// Token: 0x04003D21 RID: 15649
		private PostProcessRenderer m_PostProcessRenderer;

		// Token: 0x04003D22 RID: 15650
		private float m_FadeoutRate;

		// Token: 0x04003D23 RID: 15651
		private readonly Color m_PixelCrashBaseColor = new Color(0.2f, 0.2f, 0.2f, 1f);

		// Token: 0x04003D24 RID: 15652
		private ChapterOpenAnim.eStep m_Step;

		// Token: 0x02000A49 RID: 2633
		public enum eStep
		{
			// Token: 0x04003D26 RID: 15654
			None,
			// Token: 0x04003D27 RID: 15655
			Play,
			// Token: 0x04003D28 RID: 15656
			PlayInterval,
			// Token: 0x04003D29 RID: 15657
			PlaySecond,
			// Token: 0x04003D2A RID: 15658
			WaitTap,
			// Token: 0x04003D2B RID: 15659
			Out
		}
	}
}
