﻿using System;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI
{
	// Token: 0x020009B1 RID: 2481
	public class SceneInfoSetter : MonoBehaviour
	{
		// Token: 0x06003381 RID: 13185 RVA: 0x00105C24 File Offset: 0x00104024
		public void Open()
		{
			this.m_SceneInfoStack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, (eSceneInfo)Enum.Parse(typeof(eSceneInfo), this.m_SceneDefineName));
			if (this.OnClickBackButton != null)
			{
				this.m_SavedCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButtonCallBack));
			}
		}

		// Token: 0x06003382 RID: 13186 RVA: 0x00105C9C File Offset: 0x0010409C
		public void Close()
		{
			if (this.m_SceneInfoStack != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfoStack);
			}
			if (this.OnClickBackButton != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedCallBack);
			}
		}

		// Token: 0x06003383 RID: 13187 RVA: 0x00105CE9 File Offset: 0x001040E9
		public void OnClickBackButtonCallBack(bool isShortCut)
		{
			if (!isShortCut)
			{
				this.OnClickBackButton();
			}
			else
			{
				this.OnClickShortCutButton();
			}
		}

		// Token: 0x040039BD RID: 14781
		[SerializeField]
		private UnityAction OnClickBackButton;

		// Token: 0x040039BE RID: 14782
		[SerializeField]
		private UnityAction OnClickShortCutButton;

		// Token: 0x040039BF RID: 14783
		[SerializeField]
		private GlobalSceneInfoWindow.eBackButtonIconType m_BackButtonType;

		// Token: 0x040039C0 RID: 14784
		[SerializeField]
		private string m_SceneDefineName;

		// Token: 0x040039C1 RID: 14785
		private Action<bool> m_SavedCallBack;

		// Token: 0x040039C2 RID: 14786
		private GlobalUI.SceneInfoStack m_SceneInfoStack;
	}
}
