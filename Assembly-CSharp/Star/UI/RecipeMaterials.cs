﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200089D RID: 2205
	public class RecipeMaterials : MonoBehaviour
	{
		// Token: 0x06002DC6 RID: 11718 RVA: 0x000F1084 File Offset: 0x000EF484
		public void Setup()
		{
			if (this.m_IsDoneSetup)
			{
				return;
			}
			this.m_IsDoneSetup = true;
			for (int i = 0; i < 5; i++)
			{
				this.m_Materials[i] = UnityEngine.Object.Instantiate<RecipeMaterial>(this.m_MaterialPrefab, this.m_Parent, false);
				this.m_Materials[i].Apply(-1, 0, 0);
			}
		}

		// Token: 0x06002DC7 RID: 11719 RVA: 0x000F10E0 File Offset: 0x000EF4E0
		public void Apply(int[] itemIDs, int[] useNums)
		{
			for (int i = 0; i < 5; i++)
			{
				int num = -1;
				int useNum = 0;
				int haveNum = 0;
				if (itemIDs.Length > i)
				{
					num = itemIDs[i];
				}
				if (num != -1)
				{
					this.m_Materials[i].gameObject.SetActive(true);
					useNum = useNums[i];
					haveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(num);
				}
				else
				{
					this.m_Materials[i].gameObject.SetActive(false);
				}
				this.m_Materials[i].Apply(num, useNum, haveNum);
			}
		}

		// Token: 0x06002DC8 RID: 11720 RVA: 0x000F116C File Offset: 0x000EF56C
		public void SetEnableRenderItemIcon(bool flg)
		{
			for (int i = 0; i < this.m_Materials.Length; i++)
			{
				this.m_Materials[i].SetEnableRenderItemIcon(flg);
			}
		}

		// Token: 0x040034C5 RID: 13509
		private const int MATERIAL_MAX = 5;

		// Token: 0x040034C6 RID: 13510
		[SerializeField]
		private RecipeMaterial m_MaterialPrefab;

		// Token: 0x040034C7 RID: 13511
		[SerializeField]
		private RectTransform m_Parent;

		// Token: 0x040034C8 RID: 13512
		private RecipeMaterial[] m_Materials = new RecipeMaterial[5];

		// Token: 0x040034C9 RID: 13513
		private bool m_IsDoneSetup;
	}
}
