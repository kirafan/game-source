﻿using System;

namespace Star.UI
{
	// Token: 0x02000A3A RID: 2618
	public class SortUIArg : OverlayUIArgBase
	{
		// Token: 0x0600366E RID: 13934 RVA: 0x00112A08 File Offset: 0x00110E08
		public SortUIArg(UISettings.eUsingType_Sort usingType, Action OnExecute)
		{
			this.m_UsingType = usingType;
			this.m_OnFilterExecute = OnExecute;
		}

		// Token: 0x04003CC3 RID: 15555
		public UISettings.eUsingType_Sort m_UsingType;

		// Token: 0x04003CC4 RID: 15556
		public Action m_OnFilterExecute;
	}
}
