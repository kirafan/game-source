﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200081A RID: 2074
	[ExecuteInEditMode]
	public class ScrollField : UIBehaviour, ILayoutGroup, ILayoutController
	{
		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x06002B0D RID: 11021 RVA: 0x000E30C6 File Offset: 0x000E14C6
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x06002B0E RID: 11022 RVA: 0x000E30EB File Offset: 0x000E14EB
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x06002B0F RID: 11023 RVA: 0x000E30F9 File Offset: 0x000E14F9
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06002B10 RID: 11024 RVA: 0x000E3117 File Offset: 0x000E1517
		protected override void OnRectTransformDimensionsChange()
		{
			this.SetDirty();
		}

		// Token: 0x06002B11 RID: 11025 RVA: 0x000E311F File Offset: 0x000E151F
		protected virtual void OnTransformChildrenChanged()
		{
			this.SetDirty();
		}

		// Token: 0x06002B12 RID: 11026 RVA: 0x000E3127 File Offset: 0x000E1527
		public virtual void SetLayoutHorizontal()
		{
		}

		// Token: 0x06002B13 RID: 11027 RVA: 0x000E3129 File Offset: 0x000E1529
		public virtual void SetLayoutVertical()
		{
			this.m_Tracker.Clear();
			this.FitTarget(this.m_FitTarget);
			this.RebuildField();
		}

		// Token: 0x06002B14 RID: 11028 RVA: 0x000E3148 File Offset: 0x000E1548
		protected void SetDirty()
		{
			if (!this.IsActive())
			{
				return;
			}
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
		}

		// Token: 0x06002B15 RID: 11029 RVA: 0x000E3161 File Offset: 0x000E1561
		private void LateUpdate()
		{
			this.FitTarget(this.m_FitTarget);
			this.RebuildField();
		}

		// Token: 0x06002B16 RID: 11030 RVA: 0x000E3178 File Offset: 0x000E1578
		public void RebuildField()
		{
			if (this.m_Top)
			{
				this.m_Tracker.Add(this, this.m_Top, DrivenTransformProperties.AnchoredPosition);
				this.m_Tracker.Add(this, this.m_Top, DrivenTransformProperties.Anchors);
				this.m_Tracker.Add(this, this.m_Top, DrivenTransformProperties.Pivot);
				this.m_Tracker.Add(this, this.m_Top, DrivenTransformProperties.SizeDelta);
			}
			if (this.m_Bottom)
			{
				this.m_Tracker.Add(this, this.m_Bottom, DrivenTransformProperties.AnchoredPosition);
				this.m_Tracker.Add(this, this.m_Bottom, DrivenTransformProperties.Anchors);
				this.m_Tracker.Add(this, this.m_Bottom, DrivenTransformProperties.Pivot);
				this.m_Tracker.Add(this, this.m_Bottom, DrivenTransformProperties.SizeDelta);
			}
			if (this.m_Right)
			{
				this.m_Tracker.Add(this, this.m_Right, DrivenTransformProperties.AnchoredPosition);
				this.m_Tracker.Add(this, this.m_Right, DrivenTransformProperties.Anchors);
				this.m_Tracker.Add(this, this.m_Right, DrivenTransformProperties.Pivot);
				this.m_Tracker.Add(this, this.m_Right, DrivenTransformProperties.SizeDelta);
			}
			if (this.m_Left)
			{
				this.m_Tracker.Add(this, this.m_Left, DrivenTransformProperties.AnchoredPosition);
				this.m_Tracker.Add(this, this.m_Left, DrivenTransformProperties.Anchors);
				this.m_Tracker.Add(this, this.m_Left, DrivenTransformProperties.Pivot);
				this.m_Tracker.Add(this, this.m_Left, DrivenTransformProperties.SizeDelta);
			}
			this.m_Tracker.Add(this, this.m_Body, DrivenTransformProperties.AnchoredPosition);
			this.m_Tracker.Add(this, this.m_Body, DrivenTransformProperties.Anchors);
			this.m_Tracker.Add(this, this.m_Body, DrivenTransformProperties.Pivot);
			this.m_Tracker.Add(this, this.m_Body, DrivenTransformProperties.SizeDelta);
			this.m_Body.anchorMin = new Vector2(0.5f, 0.5f);
			this.m_Body.anchorMax = new Vector2(0.5f, 0.5f);
			this.m_Body.pivot = new Vector2(0.5f, 0.5f);
			if (this.m_Top)
			{
				this.m_Top.anchorMin = new Vector2(0f, 1f);
				this.m_Top.anchorMax = new Vector2(1f, 1f);
				this.m_Top.pivot = new Vector2(0.5f, 1f);
				this.m_Top.anchoredPosition = new Vector2(0f, 0f);
				this.m_Top.sizeDelta = new Vector2(0f, this.rectTransform.rect.height * 0.5f - this.m_Body.localPosition.y - this.m_Body.rect.height * 0.5f);
			}
			if (this.m_Bottom)
			{
				this.m_Bottom.anchorMin = new Vector2(0f, 0f);
				this.m_Bottom.anchorMax = new Vector2(1f, 0f);
				this.m_Bottom.pivot = new Vector2(0.5f, 1f);
				this.m_Bottom.anchoredPosition = new Vector2(0f, 0f);
				this.m_Bottom.sizeDelta = new Vector2(0f, this.rectTransform.rect.height * 0.5f + this.m_Body.localPosition.y - this.m_Body.rect.height * 0.5f);
			}
			if (this.m_TopFrame != null)
			{
				if (this.m_Left)
				{
					this.m_Left.anchorMin = new Vector2(0f, 0f);
					this.m_Left.anchorMax = new Vector2(0f, 1f);
					this.m_Left.pivot = new Vector2(0f, 0.5f);
					this.m_Left.anchoredPosition = new Vector2(0f, (this.m_BottomFrame.rect.height - this.m_TopFrame.rect.height) * 0.5f);
					this.m_Left.sizeDelta = new Vector2(this.rectTransform.rect.width * 0.5f + this.m_Body.localPosition.x - this.m_Body.rect.width * 0.5f, -this.m_TopFrame.rect.height - this.m_BottomFrame.rect.height);
				}
				if (this.m_Right)
				{
					this.m_Right.anchorMin = new Vector2(1f, 0f);
					this.m_Right.anchorMax = new Vector2(1f, 1f);
					this.m_Right.pivot = new Vector2(0f, 0.5f);
					this.m_Right.anchoredPosition = new Vector2(0f, (this.m_BottomFrame.rect.height - this.m_TopFrame.rect.height) * 0.5f);
					this.m_Right.sizeDelta = new Vector2(this.rectTransform.rect.width * 0.5f - this.m_Body.localPosition.x - this.m_Body.rect.width * 0.5f, -this.m_TopFrame.rect.height - this.m_BottomFrame.rect.height);
				}
			}
			else
			{
				if (this.m_Left)
				{
					this.m_Left.anchorMin = new Vector2(0f, 0.5f);
					this.m_Left.anchorMax = new Vector2(0f, 0.5f);
					this.m_Left.pivot = new Vector2(0f, 0.5f);
					this.m_Left.anchoredPosition = new Vector2(0f, this.m_Body.localPosition.y);
					this.m_Left.sizeDelta = new Vector2(this.rectTransform.rect.width * 0.5f + this.m_Body.localPosition.x - this.m_Body.rect.width * 0.5f, this.m_Body.rect.height);
				}
				if (this.m_Right)
				{
					this.m_Right.anchorMin = new Vector2(1f, 0.5f);
					this.m_Right.anchorMax = new Vector2(1f, 0.5f);
					this.m_Right.pivot = new Vector2(0f, 0.5f);
					this.m_Right.anchoredPosition = new Vector2(0f, this.m_Body.localPosition.y);
					this.m_Right.sizeDelta = new Vector2(this.rectTransform.rect.width * 0.5f - this.m_Body.localPosition.x - this.m_Body.rect.width * 0.5f, this.m_Body.rect.height);
				}
			}
		}

		// Token: 0x06002B17 RID: 11031 RVA: 0x000E39C1 File Offset: 0x000E1DC1
		public void SetFieldPosition(Vector2 pos)
		{
			this.m_Body.localPosition = pos;
		}

		// Token: 0x06002B18 RID: 11032 RVA: 0x000E39D4 File Offset: 0x000E1DD4
		public void SetFieldSize(Vector2 sizeDelta)
		{
			this.m_Body.sizeDelta = sizeDelta;
		}

		// Token: 0x06002B19 RID: 11033 RVA: 0x000E39E4 File Offset: 0x000E1DE4
		public void FitTarget(RectTransform target)
		{
			this.m_FitTarget = target;
			if (this.m_FitTarget == null)
			{
				this.SetFieldSize(new Vector2(0f, 0f));
			}
			else
			{
				Vector2 fieldPosition = default(Vector2);
				fieldPosition = this.m_FitTarget.position - this.rectTransform.position;
				fieldPosition.y -= target.rect.height * (target.pivot.y - 0.5f) * target.lossyScale.y;
				fieldPosition.y /= base.GetComponentInParent<CanvasScaler>().GetComponent<RectTransform>().localScale.y;
				fieldPosition.x -= target.rect.width * (target.pivot.x - 0.5f) * target.lossyScale.x;
				fieldPosition.x /= base.GetComponentInParent<CanvasScaler>().GetComponent<RectTransform>().localScale.x;
				this.SetFieldPosition(fieldPosition);
				this.SetFieldSize(new Vector2(target.rect.width + 10f, target.rect.height + 4f));
			}
		}

		// Token: 0x04003175 RID: 12661
		private const float MARGIN_X = 5f;

		// Token: 0x04003176 RID: 12662
		private const float MARGIN_Y = 2f;

		// Token: 0x04003177 RID: 12663
		private const float OFFSET_Y = 0f;

		// Token: 0x04003178 RID: 12664
		[SerializeField]
		private RectTransform m_Top;

		// Token: 0x04003179 RID: 12665
		[SerializeField]
		private RectTransform m_Body;

		// Token: 0x0400317A RID: 12666
		[SerializeField]
		private RectTransform m_Bottom;

		// Token: 0x0400317B RID: 12667
		[SerializeField]
		private RectTransform m_Left;

		// Token: 0x0400317C RID: 12668
		[SerializeField]
		private RectTransform m_Right;

		// Token: 0x0400317D RID: 12669
		[SerializeField]
		[Tooltip("指定されている場合、それに合わせてスクロールを調整する")]
		private RectTransform m_FitTarget;

		// Token: 0x0400317E RID: 12670
		[SerializeField]
		[Tooltip("上下部枠 指定されている場合、LeftとRightはこれに干渉しないように調整される")]
		private RectTransform m_TopFrame;

		// Token: 0x0400317F RID: 12671
		[SerializeField]
		[Tooltip("上下部枠 指定されている場合、LeftとRightはこれに干渉しないように調整される")]
		private RectTransform m_BottomFrame;

		// Token: 0x04003180 RID: 12672
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x04003181 RID: 12673
		private DrivenRectTransformTracker m_Tracker;
	}
}
