﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200087F RID: 2175
	public class ImageNumbers : MonoBehaviour
	{
		// Token: 0x06002D0C RID: 11532 RVA: 0x000EDA38 File Offset: 0x000EBE38
		public void SetValue(int value)
		{
			int num = value;
			if (num < 0)
			{
				num = -num;
			}
			int num2 = 0;
			bool flag = true;
			for (int i = this.m_Numbers.Length - 1; i >= 0; i--)
			{
				int num3 = (int)Mathf.Pow(10f, (float)i);
				int num4 = num / num3;
				this.m_Numbers[i].SetValue(num4);
				if (i != 0)
				{
					if (this.m_RenderOffOnZero && flag && num4 == 0)
					{
						this.m_Numbers[i].SetEnableRender(false);
					}
					else
					{
						this.m_Numbers[i].SetEnableRender(true);
						num2++;
					}
					flag = (flag && num4 == 0);
					num %= num3;
				}
				else
				{
					num2++;
				}
			}
			if (this.m_EnableAutoCenterling)
			{
				float num5 = this.m_CenterlingOneDigitInterval * (float)(num2 - 1) / 2f;
				for (int j = 0; j < this.m_Numbers.Length; j++)
				{
					float x = -((float)j * this.m_CenterlingOneDigitInterval) + num5;
					this.m_Numbers[j].CacheRectTransform.localPosition = new Vector3(x, this.m_Numbers[j].CacheRectTransform.localPosition.y, this.m_Numbers[j].CacheRectTransform.localPosition.z);
				}
			}
		}

		// Token: 0x06002D0D RID: 11533 RVA: 0x000EDB94 File Offset: 0x000EBF94
		public void SetColor(Color color)
		{
			for (int i = this.m_Numbers.Length - 1; i >= 0; i--)
			{
				this.m_Numbers[i].SetColor(color);
			}
		}

		// Token: 0x06002D0E RID: 11534 RVA: 0x000EDBCA File Offset: 0x000EBFCA
		public void SetScale(float scale)
		{
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			this.m_RectTransform.localScale = Vector3.one * scale;
		}

		// Token: 0x06002D0F RID: 11535 RVA: 0x000EDBFF File Offset: 0x000EBFFF
		public ImageNumber GetNumber(int idx)
		{
			return this.m_Numbers[idx];
		}

		// Token: 0x0400342A RID: 13354
		[SerializeField]
		[Tooltip("１の位～")]
		private ImageNumber[] m_Numbers;

		// Token: 0x0400342B RID: 13355
		[SerializeField]
		private bool m_RenderOffOnZero = true;

		// Token: 0x0400342C RID: 13356
		[SerializeField]
		private bool m_EnableAutoCenterling;

		// Token: 0x0400342D RID: 13357
		[SerializeField]
		private float m_CenterlingOneDigitInterval = 64f;

		// Token: 0x0400342E RID: 13358
		private RectTransform m_RectTransform;
	}
}
