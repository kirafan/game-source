﻿using System;
using System.Collections;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008D2 RID: 2258
	public class iTweenWrapper : MonoBehaviour
	{
		// Token: 0x06002ECA RID: 11978 RVA: 0x000F4B3D File Offset: 0x000F2F3D
		private void Start()
		{
			this.m_Target = null;
		}

		// Token: 0x06002ECB RID: 11979 RVA: 0x000F4B48 File Offset: 0x000F2F48
		public virtual void Play(iTweenWrapperPlayArgument argument)
		{
			if (argument != null)
			{
				Hashtable hashtable = new Hashtable();
				hashtable.Add("isLocal", argument.isLocal);
				if (argument.GetParameter(0) != null)
				{
					for (int i = 0; i < argument.HowManyParameters(); i++)
					{
						iTweenParameter parameter = argument.GetParameter(i);
						hashtable.Add(parameter.paramName, parameter.value);
					}
				}
				hashtable.Add("time", argument.time);
				hashtable.Add("easeType", argument.easeType);
				hashtable.Add("delay", argument.delay);
				hashtable.Add("loopType", argument.loopType);
				if (argument.onupdatetarget != null)
				{
					hashtable.Add("onupdate", argument.onupdate);
					hashtable.Add("onupdatetarget", argument.onupdatetarget);
				}
				this.m_OnCompleteCallback = argument.oncompletecallback;
				hashtable.Add("oncomplete", "OnCompleteCallback");
				hashtable.Add("oncompletetarget", base.gameObject);
				hashtable.Add("ignoretimescale", argument.ignoretimescale);
				this.m_Target = argument.target;
				this.PlayProcess(argument.target, hashtable);
			}
		}

		// Token: 0x06002ECC RID: 11980 RVA: 0x000F4C9A File Offset: 0x000F309A
		protected virtual void PlayProcess(GameObject target, Hashtable args)
		{
		}

		// Token: 0x06002ECD RID: 11981 RVA: 0x000F4C9C File Offset: 0x000F309C
		public virtual void Stop()
		{
			this.m_Target = null;
		}

		// Token: 0x06002ECE RID: 11982 RVA: 0x000F4CA5 File Offset: 0x000F30A5
		public bool IsPlaying()
		{
			return this.m_Target != null;
		}

		// Token: 0x06002ECF RID: 11983 RVA: 0x000F4CB3 File Offset: 0x000F30B3
		protected virtual void OnCompleteCallback()
		{
			this.m_Target = null;
			if (this.m_OnCompleteCallback != null)
			{
				this.m_OnCompleteCallback(null);
			}
		}

		// Token: 0x040035DB RID: 13787
		protected GameObject m_Target;

		// Token: 0x040035DC RID: 13788
		protected OniTweenCompleteDelegate m_OnCompleteCallback;
	}
}
