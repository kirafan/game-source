﻿using System;

namespace Star.UI
{
	// Token: 0x0200085C RID: 2140
	public class CharaIllust : ASyncImage
	{
		// Token: 0x06002C8A RID: 11402 RVA: 0x000EB41C File Offset: 0x000E981C
		public void Apply(int charaID, CharaIllust.eCharaIllustType type = CharaIllust.eCharaIllustType.Chara)
		{
			if (this.m_CharaID != charaID)
			{
				this.Apply();
				if (charaID != -1)
				{
					switch (type)
					{
					case CharaIllust.eCharaIllustType.Chara:
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustChara(charaID);
						break;
					case CharaIllust.eCharaIllustType.Full:
						if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_Rare == 4)
						{
							this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustFull(charaID);
						}
						else
						{
							this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustChara(charaID);
						}
						break;
					case CharaIllust.eCharaIllustType.Bust:
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustBust(charaID);
						break;
					case CharaIllust.eCharaIllustType.Edit:
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIllustEdit(charaID);
						break;
					case CharaIllust.eCharaIllustType.Card:
						this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaCard(charaID);
						break;
					}
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_CharaID = charaID;
			}
		}

		// Token: 0x06002C8B RID: 11403 RVA: 0x000EB550 File Offset: 0x000E9950
		public override void Destroy()
		{
			base.Destroy();
			this.m_CharaID = -1;
		}

		// Token: 0x04003370 RID: 13168
		protected int m_CharaID = -1;

		// Token: 0x0200085D RID: 2141
		public enum eCharaIllustType
		{
			// Token: 0x04003372 RID: 13170
			Chara,
			// Token: 0x04003373 RID: 13171
			Full,
			// Token: 0x04003374 RID: 13172
			Bust,
			// Token: 0x04003375 RID: 13173
			Edit,
			// Token: 0x04003376 RID: 13174
			Card
		}
	}
}
