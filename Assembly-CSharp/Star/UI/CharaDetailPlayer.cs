﻿using System;
using Meige;
using PlayerRequestTypes;
using Star.UI.Edit;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008DC RID: 2268
	public class CharaDetailPlayer : SingletonMonoBehaviour<CharaDetailPlayer>
	{
		// Token: 0x14000072 RID: 114
		// (add) Token: 0x06002EFC RID: 12028 RVA: 0x000F5DEC File Offset: 0x000F41EC
		// (remove) Token: 0x06002EFD RID: 12029 RVA: 0x000F5E24 File Offset: 0x000F4224
		public event Action OnClose;

		// Token: 0x06002EFE RID: 12030 RVA: 0x000F5E5A File Offset: 0x000F425A
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06002EFF RID: 12031 RVA: 0x000F5E62 File Offset: 0x000F4262
		public bool IsOpen()
		{
			return this.m_Step != CharaDetailPlayer.eStep.Hide;
		}

		// Token: 0x06002F00 RID: 12032 RVA: 0x000F5E70 File Offset: 0x000F4270
		public void OpenProfileOnly(int charaId)
		{
			if (this.m_Step != CharaDetailPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.Open(new EquipWeaponUnmanagedSupportPartyMemberController().Setup(new UserSupportData
			{
				CharaID = charaId,
				Lv = 1,
				WeaponID = -1
			}), CharaDetailUI.eMode.ProfileOnly, 1);
		}

		// Token: 0x06002F01 RID: 12033 RVA: 0x000F5ED8 File Offset: 0x000F42D8
		public void Open(long charaMngID, bool profileStart = false)
		{
			if (this.m_Step != CharaDetailPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.Open(new EquipWeaponUnmanagedUserCharacterDataController().Setup(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID), null), profileStart);
		}

		// Token: 0x06002F02 RID: 12034 RVA: 0x000F5F31 File Offset: 0x000F4331
		public void OpenManagedBattlePartyMember(int partyIndex, int partySlotIndex, bool profileStart = false)
		{
			this.Open((EquipWeaponManagedBattlePartyMemberController)new EquipWeaponManagedBattlePartyMemberController().Setup(partyIndex, partySlotIndex), profileStart);
		}

		// Token: 0x06002F03 RID: 12035 RVA: 0x000F5F4B File Offset: 0x000F434B
		public void OpenManagedSupportPartyMember(int partyIndex, int partySlotIndex, bool profileStart = false)
		{
			this.Open((EquipWeaponManagedSupportPartyMemberController)new EquipWeaponManagedSupportPartyMemberController().Setup(partyIndex, partySlotIndex), profileStart);
		}

		// Token: 0x06002F04 RID: 12036 RVA: 0x000F5F65 File Offset: 0x000F4365
		public void Open(EquipWeaponCharacterDataControllerExist partyMember, bool profileStart)
		{
			this.Open(partyMember, (!profileStart) ? 0 : 1);
		}

		// Token: 0x06002F05 RID: 12037 RVA: 0x000F5F7B File Offset: 0x000F437B
		public void Open(EquipWeaponCharacterDataController partyMember, int startTabIndex = 0)
		{
			this.Open(partyMember, CharaDetailUI.eMode.Auto, startTabIndex);
		}

		// Token: 0x06002F06 RID: 12038 RVA: 0x000F5F88 File Offset: 0x000F4388
		public void Open(EquipWeaponCharacterDataController partyMember, CharaDetailUI.eMode mode, int startTabIndex)
		{
			if (this.m_Step != CharaDetailPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.m_Mode = mode;
			this.m_StartTabIndex = startTabIndex;
			this.m_PartyMember = partyMember;
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_SceneInfoStack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharaDetail);
			this.OnClose = null;
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			this.m_IsInputBlock = true;
			this.m_Step = CharaDetailPlayer.eStep.UILoad;
		}

		// Token: 0x06002F07 RID: 12039 RVA: 0x000F6047 File Offset: 0x000F4447
		public void Open(UserSupportData supportData, bool profileStart = false)
		{
			if (this.m_Step != CharaDetailPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.Open(new EquipWeaponUnmanagedSupportPartyMemberController().Setup(supportData), profileStart);
		}

		// Token: 0x06002F08 RID: 12040 RVA: 0x000F6088 File Offset: 0x000F4488
		public void Close()
		{
			if (this.m_Step != CharaDetailPlayer.eStep.Main)
			{
				Debug.LogWarningFormat("CharaDetailPlayer.Close() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfoStack);
			this.Request_CharacterSetShown();
			this.OnClose.Call();
			this.OnClose = null;
		}

		// Token: 0x06002F09 RID: 12041 RVA: 0x000F610A File Offset: 0x000F450A
		public void ForceHide()
		{
			this.m_Step = CharaDetailPlayer.eStep.Hide;
		}

		// Token: 0x06002F0A RID: 12042 RVA: 0x000F6114 File Offset: 0x000F4514
		private void Update()
		{
			switch (this.m_Step)
			{
			case CharaDetailPlayer.eStep.UILoad:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.CharaDetailUI, true);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadVoiceCueSheet(this.m_PartyMember.GetCharaNamedType());
				this.m_Step = CharaDetailPlayer.eStep.LoadWait;
				break;
			case CharaDetailPlayer.eStep.LoadWait:
				if (!SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.CharaDetailUI))
				{
					return;
				}
				if (SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets())
				{
					Debug.LogError("GameSystem.Inst.SoundMng.IsLoadingCueSheets()");
					return;
				}
				SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.CharaDetailUI);
				this.m_Step = CharaDetailPlayer.eStep.SetupAndPlayIn;
				break;
			case CharaDetailPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<CharaDetailUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.CharaDetailUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					if (this.m_PartyMember != null)
					{
						this.m_UI.Setup(this.m_PartyMember, this.m_Mode);
					}
					this.m_Mode = CharaDetailUI.eMode.Auto;
					CharaDetailUI ui = this.m_UI;
					ui.OnStartViewIllust = (Action)Delegate.Combine(ui.OnStartViewIllust, new Action(this.OnStartIllustView));
					CharaDetailUI ui2 = this.m_UI;
					ui2.OnEndViewIllust = (Action)Delegate.Combine(ui2.OnEndViewIllust, new Action(this.OnEndIllustView));
					this.m_UI.PlayIn();
					if (0 < this.m_StartTabIndex)
					{
						this.m_UI.SetSelectTab(this.m_StartTabIndex);
					}
					this.m_StartTabIndex = 0;
					this.m_Step = CharaDetailPlayer.eStep.Main;
				}
				break;
			case CharaDetailPlayer.eStep.Main:
				if (this.m_IsInputBlock && this.m_UI.GetStep() == CharaDetailUI.eStep.Idle)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_IsInputBlock = false;
				}
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.CharaDetailUI);
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.UnloadVoiceCueSheet(this.m_PartyMember.GetCharaNamedType());
					this.m_Step = CharaDetailPlayer.eStep.UnloadWait;
				}
				break;
			case CharaDetailPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.CharaDetailUI))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_SavedBackButtonCallBack(true);
					}
					this.m_Step = CharaDetailPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x06002F0B RID: 12043 RVA: 0x000F6389 File Offset: 0x000F4789
		private void OnClickBackButton(bool isCallFromShortCut)
		{
			this.Close();
		}

		// Token: 0x06002F0C RID: 12044 RVA: 0x000F6391 File Offset: 0x000F4791
		private void OnStartIllustView()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
		}

		// Token: 0x06002F0D RID: 12045 RVA: 0x000F63A2 File Offset: 0x000F47A2
		private void OnEndIllustView()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
		}

		// Token: 0x06002F0E RID: 12046 RVA: 0x000F63D0 File Offset: 0x000F47D0
		private bool Request_CharacterSetShown()
		{
			long charaMngId = this.m_PartyMember.GetCharaMngId();
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngId);
			if (userCharaData == null)
			{
				return false;
			}
			if (!userCharaData.IsNew)
			{
				return false;
			}
			MeigewwwParam wwwParam = PlayerRequest.Charactersetshown(new Charactersetshown
			{
				managedCharacterId = charaMngId,
				shown = 1
			}, null);
			NetworkQueueManager.Request(wwwParam);
			userCharaData.IsNew = false;
			return true;
		}

		// Token: 0x04003616 RID: 13846
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.CharaDetailUI;

		// Token: 0x04003617 RID: 13847
		private CharaDetailPlayer.eStep m_Step;

		// Token: 0x04003618 RID: 13848
		private CharaDetailUI.eMode m_Mode;

		// Token: 0x04003619 RID: 13849
		private CharaDetailUI m_UI;

		// Token: 0x0400361A RID: 13850
		private EquipWeaponCharacterDataController m_PartyMember;

		// Token: 0x0400361B RID: 13851
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x0400361C RID: 13852
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x0400361D RID: 13853
		private int m_StartTabIndex;

		// Token: 0x0400361E RID: 13854
		private bool m_IsInputBlock;

		// Token: 0x020008DD RID: 2269
		private enum eStep
		{
			// Token: 0x04003621 RID: 13857
			Hide,
			// Token: 0x04003622 RID: 13858
			UILoad,
			// Token: 0x04003623 RID: 13859
			LoadWait,
			// Token: 0x04003624 RID: 13860
			SetupAndPlayIn,
			// Token: 0x04003625 RID: 13861
			Main,
			// Token: 0x04003626 RID: 13862
			UnloadWait
		}
	}
}
