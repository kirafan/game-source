﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200080A RID: 2058
	public class CategorySelectMenu : MonoBehaviour
	{
		// Token: 0x06002A98 RID: 10904 RVA: 0x000E13F8 File Offset: 0x000DF7F8
		private void Reset()
		{
			RectTransform component = base.GetComponent<RectTransform>();
			component.anchorMin = new Vector2(0.5f, 0f);
			component.anchorMax = new Vector2(0.5f, 1f);
			component.pivot = new Vector2(0.5f, 0.5f);
			component.sizeDelta = this.SIZEDELTA;
			component.localPosition = new Vector3(342f, -36f, 0f);
			this.SetVerticalNormalizedPosition(1f);
		}

		// Token: 0x06002A99 RID: 10905 RVA: 0x000E147C File Offset: 0x000DF87C
		private void OnValidate()
		{
			this.Reset();
		}

		// Token: 0x06002A9A RID: 10906 RVA: 0x000E1484 File Offset: 0x000DF884
		private void Start()
		{
			this.Reset();
		}

		// Token: 0x06002A9B RID: 10907 RVA: 0x000E148C File Offset: 0x000DF88C
		private void LateUpdate()
		{
			if (this.m_Scroll.content.rect.height > this.m_Scroll.viewport.rect.height)
			{
				this.m_Scroll.vertical = true;
			}
			else
			{
				this.m_Scroll.vertical = false;
			}
		}

		// Token: 0x06002A9C RID: 10908 RVA: 0x000E14EC File Offset: 0x000DF8EC
		public void SetVerticalNormalizedPosition(float value)
		{
			FixScrollRect component = this.m_Scroll.GetComponent<FixScrollRect>();
			if (component != null)
			{
				component.SetVerticalNormalizedPosition(value);
			}
		}

		// Token: 0x06002A9D RID: 10909 RVA: 0x000E1518 File Offset: 0x000DF918
		public float GetVerticalNormalizedPosition()
		{
			return this.m_Scroll.verticalNormalizedPosition;
		}

		// Token: 0x04003101 RID: 12545
		private readonly Vector2 SIZEDELTA = new Vector2(596f, -115f);

		// Token: 0x04003102 RID: 12546
		[SerializeField]
		private ScrollRect m_Scroll;
	}
}
