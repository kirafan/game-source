﻿using System;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B35 RID: 2869
	public class WeaponScrollItemData : ScrollItemData
	{
		// Token: 0x06003C61 RID: 15457 RVA: 0x0013507B File Offset: 0x0013347B
		public WeaponScrollItemData(long mngID, WeaponScrollItemData.eStatus status)
		{
			this.m_WeaponMngID = mngID;
			this.m_Status = status;
			this.m_WeaponData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID);
		}

		// Token: 0x04004429 RID: 17449
		public long m_WeaponMngID;

		// Token: 0x0400442A RID: 17450
		public UserWeaponData m_WeaponData;

		// Token: 0x0400442B RID: 17451
		public int m_SortedIdx;

		// Token: 0x0400442C RID: 17452
		public int m_SaleSelectIdx = -1;

		// Token: 0x0400442D RID: 17453
		public bool m_IsEquieped;

		// Token: 0x0400442E RID: 17454
		public bool m_Enable = true;

		// Token: 0x0400442F RID: 17455
		public WeaponScrollItemData.eStatus m_Status;

		// Token: 0x02000B36 RID: 2870
		public enum eStatus
		{
			// Token: 0x04004431 RID: 17457
			None,
			// Token: 0x04004432 RID: 17458
			EnableEquip,
			// Token: 0x04004433 RID: 17459
			DisableEquip,
			// Token: 0x04004434 RID: 17460
			Upgrade,
			// Token: 0x04004435 RID: 17461
			LevelMax,
			// Token: 0x04004436 RID: 17462
			Sale,
			// Token: 0x04004437 RID: 17463
			SaleSelected,
			// Token: 0x04004438 RID: 17464
			Equiped
		}
	}
}
