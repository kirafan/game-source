﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B2E RID: 2862
	public class WeaponListUI : MenuUIBase
	{
		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06003C26 RID: 15398 RVA: 0x001335A7 File Offset: 0x001319A7
		public WeaponListUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06003C27 RID: 15399 RVA: 0x001335AF File Offset: 0x001319AF
		public long SelectedWeaponMngID
		{
			get
			{
				return this.m_SelectedWeaponMngID;
			}
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06003C28 RID: 15400 RVA: 0x001335B8 File Offset: 0x001319B8
		public List<long> SelectedWeaponList
		{
			get
			{
				List<long> list = new List<long>();
				for (int i = 0; i < this.m_SelectedList.Count; i++)
				{
					list.Add(this.m_SelectedList[i].m_WeaponMngID);
				}
				return list;
			}
		}

		// Token: 0x14000105 RID: 261
		// (add) Token: 0x06003C29 RID: 15401 RVA: 0x00133600 File Offset: 0x00131A00
		// (remove) Token: 0x06003C2A RID: 15402 RVA: 0x00133638 File Offset: 0x00131A38
		public event Action OnClickButton;

		// Token: 0x06003C2B RID: 15403 RVA: 0x00133670 File Offset: 0x00131A70
		public void SetupEquip(UserBattlePartyData partyData, int slotIdx)
		{
			this.m_PartyData = partyData;
			this.m_SupportPartyData = null;
			this.m_SlotIdx = slotIdx;
			this.m_CharaMngID = this.m_PartyData.GetMemberAt(this.m_SlotIdx);
			List<long> list = new List<long>();
			for (int i = 0; i < this.m_PartyData.WeaponMngIDs.Length; i++)
			{
				if (this.m_PartyData.WeaponMngIDs[i] != -1L)
				{
					list.Add(this.m_PartyData.WeaponMngIDs[i]);
				}
			}
			this.SetupEquip(list);
		}

		// Token: 0x06003C2C RID: 15404 RVA: 0x001336FC File Offset: 0x00131AFC
		public void SetupEquip(UserSupportPartyData supportPartyData, int slotIdx)
		{
			this.m_PartyData = null;
			this.m_SupportPartyData = supportPartyData;
			this.m_SlotIdx = slotIdx;
			this.m_CharaMngID = this.m_SupportPartyData.GetMemberAt(this.m_SlotIdx);
			List<long> list = new List<long>();
			for (int i = 0; i < this.m_SupportPartyData.WeaponMngIDs.Count; i++)
			{
				if (this.m_SupportPartyData.WeaponMngIDs[i] != -1L)
				{
					list.Add(this.m_SupportPartyData.WeaponMngIDs[i]);
				}
			}
			this.SetupEquip(list);
		}

		// Token: 0x06003C2D RID: 15405 RVA: 0x00133794 File Offset: 0x00131B94
		private void SetupEquip(List<long> equipped)
		{
			RectTransform component = this.m_Scroll.GetComponent<RectTransform>();
			component.sizeDelta = new Vector2(component.sizeDelta.x, 480f);
			this.m_Scroll.SetMode(WeaponScroll.eMode.Equip);
			this.m_Scroll.SetEquipData(this.m_CharaMngID, equipped);
			this.m_Scroll.Setup();
			this.m_Scroll.OnClickWeapon += this.OnClickWeaponEquipCallBack;
			this.m_Scroll.OnClickRemove += this.OnClickRemoveCallBack;
			this.m_DetailGroup.OnEquip += this.OnClickEquip;
			this.m_SaleObj.SetActive(false);
		}

		// Token: 0x06003C2E RID: 15406 RVA: 0x00133848 File Offset: 0x00131C48
		public void SetupUpgrade()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			RectTransform component = this.m_Scroll.GetComponent<RectTransform>();
			component.sizeDelta = new Vector2(component.sizeDelta.x, 480f);
			this.m_Scroll.SetMode(WeaponScroll.eMode.Upgrade);
			this.m_Scroll.Setup();
			this.m_Scroll.OnClickWeapon += this.OnClickWeaponCallBack;
			this.m_Scroll.OnHoldWeapon += this.OnHoldWeaponCallBack;
			this.m_Scroll.OnClickRemove += this.OnClickRemoveCallBack;
			this.m_SaleObj.SetActive(false);
		}

		// Token: 0x06003C2F RID: 15407 RVA: 0x00133914 File Offset: 0x00131D14
		public void SetupSale()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			List<long> list = new List<long>();
			for (int i = 0; i < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas.Count; i++)
			{
				for (int j = 0; j < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[i].WeaponMngIDs.Length; j++)
				{
					long num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[i].WeaponMngIDs[j];
					if (num != -1L)
					{
						list.Add(num);
					}
				}
			}
			for (int k = 0; k < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas.Count; k++)
			{
				for (int l = 0; l < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[k].WeaponMngIDs.Count; l++)
				{
					long num2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserSupportPartyDatas[k].WeaponMngIDs[l];
					if (num2 != -1L)
					{
						list.Add(num2);
					}
				}
			}
			RectTransform component = this.m_Scroll.GetComponent<RectTransform>();
			component.sizeDelta = new Vector2(component.sizeDelta.x, 415f);
			this.m_Scroll.SetEquipData(-1L, list);
			this.m_Scroll.SetMode(WeaponScroll.eMode.Sale);
			this.m_Scroll.Setup();
			this.m_Scroll.OnClickWeapon += this.OnClickWeaponCallBack;
			this.m_Scroll.OnHoldWeapon += this.OnHoldWeaponCallBack;
			this.m_Scroll.OnClickRemove += this.OnClickRemoveCallBack;
			this.m_SaleObj.SetActive(true);
			this.UpdateDisp();
		}

		// Token: 0x06003C30 RID: 15408 RVA: 0x00133B0C File Offset: 0x00131F0C
		public void Refresh()
		{
			for (int i = 0; i < this.m_SelectedList.Count; i++)
			{
				this.m_SelectedList[i].m_SaleSelectIdx = -1;
			}
			this.m_SelectedList.Clear();
			this.m_Scroll.OpenUserWeaponList();
			this.UpdateDisp();
		}

		// Token: 0x06003C31 RID: 15409 RVA: 0x00133B64 File Offset: 0x00131F64
		private void Update()
		{
			switch (this.m_Step)
			{
			case WeaponListUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(WeaponListUI.eStep.Idle);
				}
				break;
			}
			case WeaponListUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(WeaponListUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003C32 RID: 15410 RVA: 0x00133C60 File Offset: 0x00132060
		private void ChangeStep(WeaponListUI.eStep step)
		{
			this.m_Step = step;
			switch (step)
			{
			case WeaponListUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case WeaponListUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case WeaponListUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case WeaponListUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case WeaponListUI.eStep.End:
				this.SetEnableInput(false);
				this.m_Scroll.Destroy();
				break;
			}
		}

		// Token: 0x06003C33 RID: 15411 RVA: 0x00133D0C File Offset: 0x0013210C
		public override void PlayIn()
		{
			this.ChangeStep(WeaponListUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003C34 RID: 15412 RVA: 0x00133D54 File Offset: 0x00132154
		public override void PlayOut()
		{
			this.ChangeStep(WeaponListUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
			this.m_DetailGroup.Close();
		}

		// Token: 0x06003C35 RID: 15413 RVA: 0x00133DA4 File Offset: 0x001321A4
		public override bool IsMenuEnd()
		{
			return this.m_Step == WeaponListUI.eStep.End;
		}

		// Token: 0x06003C36 RID: 15414 RVA: 0x00133DB0 File Offset: 0x001321B0
		public void OnClickWeaponEquipCallBack(WeaponScrollItemData data)
		{
			int charaCost = -1;
			if (this.m_CharaMngID != -1L)
			{
				charaCost = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_CharaMngID).Param.Cost;
			}
			this.m_DetailGroup.Open(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(data.m_WeaponMngID), true, charaCost);
		}

		// Token: 0x06003C37 RID: 15415 RVA: 0x00133E0E File Offset: 0x0013220E
		private void OnClickEquip(long mngID)
		{
			this.m_SelectedWeaponMngID = mngID;
			this.m_SelectButton = WeaponListUI.eButton.Weapon;
			this.OnClickButton.Call();
		}

		// Token: 0x06003C38 RID: 15416 RVA: 0x00133E29 File Offset: 0x00132229
		public void OnClickRemoveCallBack()
		{
			this.m_SelectedWeaponMngID = -1L;
			this.m_SelectButton = WeaponListUI.eButton.Weapon;
			this.OnClickButton.Call();
		}

		// Token: 0x06003C39 RID: 15417 RVA: 0x00133E48 File Offset: 0x00132248
		public void OnClickWeaponCallBack(WeaponScrollItemData data)
		{
			if (this.m_Scroll.Mode == WeaponScroll.eMode.Sale)
			{
				if (this.m_SelectedList.Contains(data))
				{
					data.m_SaleSelectIdx = -1;
					this.m_SelectedList.Remove(data);
					for (int i = 0; i < this.m_SelectedList.Count; i++)
					{
						this.m_SelectedList[i].m_SaleSelectIdx = i;
					}
				}
				else if (this.m_SelectedList.Count < 20)
				{
					data.m_SaleSelectIdx = this.m_SelectedList.Count;
					this.m_SelectedList.Add(data);
				}
				this.UpdateDisp();
			}
			this.m_Scroll.ForceApply();
			this.m_SelectedWeaponMngID = data.m_WeaponMngID;
			this.m_SelectButton = WeaponListUI.eButton.Weapon;
			this.OnClickButton.Call();
		}

		// Token: 0x06003C3A RID: 15418 RVA: 0x00133F1C File Offset: 0x0013231C
		public void OnClickSaleButtonCallBack()
		{
			this.m_SelectButton = WeaponListUI.eButton.Sale;
			this.OnClickButton.Call();
		}

		// Token: 0x06003C3B RID: 15419 RVA: 0x00133F30 File Offset: 0x00132330
		public void OnClickReleaseSelectButtonCallBack()
		{
			for (int i = 0; i < this.m_SelectedList.Count; i++)
			{
				this.m_SelectedList[i].m_SaleSelectIdx = -1;
			}
			this.m_SelectedList.Clear();
			this.m_Scroll.ForceApply();
			this.UpdateDisp();
		}

		// Token: 0x06003C3C RID: 15420 RVA: 0x00133F88 File Offset: 0x00132388
		private void UpdateDisp()
		{
			if (this.m_SelectedList.Count >= 20)
			{
				this.m_Scroll.SetEnableSaleSelect(false);
			}
			else
			{
				this.m_Scroll.SetEnableSaleSelect(true);
			}
			this.m_SelectNumText.text = this.m_SelectedList.Count + " / " + 20;
			int num = 0;
			for (int i = 0; i < this.m_SelectedList.Count; i++)
			{
				int weaponID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_SelectedList[i].m_WeaponMngID).Param.WeaponID;
				num += SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_SaleAmount;
			}
			this.m_SalePrice.text = UIUtility.GoldValueToString(num, false);
			this.m_ResetButton.Interactable = (this.m_SelectedList.Count > 0);
			this.m_ExecSaleButton.Interactable = (this.m_SelectedList.Count > 0);
		}

		// Token: 0x06003C3D RID: 15421 RVA: 0x0013409D File Offset: 0x0013249D
		public void OnHoldWeaponCallBack(WeaponScrollItemData data)
		{
			this.m_DetailGroup.Open(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(data.m_WeaponMngID), false, -1);
		}

		// Token: 0x040043F0 RID: 17392
		private const float NORMALHEIGHT = 480f;

		// Token: 0x040043F1 RID: 17393
		private const float FOOTERHEIGHT = 415f;

		// Token: 0x040043F2 RID: 17394
		private const int SELECT_MAX = 20;

		// Token: 0x040043F3 RID: 17395
		private WeaponListUI.eStep m_Step;

		// Token: 0x040043F4 RID: 17396
		[SerializeField]
		private WeaponScroll m_Scroll;

		// Token: 0x040043F5 RID: 17397
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040043F6 RID: 17398
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040043F7 RID: 17399
		[SerializeField]
		private WeaponDetailGroup m_DetailGroup;

		// Token: 0x040043F8 RID: 17400
		[SerializeField]
		private GameObject m_SaleObj;

		// Token: 0x040043F9 RID: 17401
		[SerializeField]
		private Text m_SelectNumText;

		// Token: 0x040043FA RID: 17402
		[SerializeField]
		private Text m_SalePrice;

		// Token: 0x040043FB RID: 17403
		[SerializeField]
		private CustomButton m_ResetButton;

		// Token: 0x040043FC RID: 17404
		[SerializeField]
		private CustomButton m_ExecSaleButton;

		// Token: 0x040043FD RID: 17405
		private UserBattlePartyData m_PartyData;

		// Token: 0x040043FE RID: 17406
		private UserSupportPartyData m_SupportPartyData;

		// Token: 0x040043FF RID: 17407
		private int m_SlotIdx;

		// Token: 0x04004400 RID: 17408
		private long m_CharaMngID;

		// Token: 0x04004401 RID: 17409
		private long m_SelectedWeaponMngID = -1L;

		// Token: 0x04004402 RID: 17410
		private List<WeaponScrollItemData> m_SelectedList = new List<WeaponScrollItemData>();

		// Token: 0x04004403 RID: 17411
		private WeaponListUI.eButton m_SelectButton = WeaponListUI.eButton.None;

		// Token: 0x02000B2F RID: 2863
		private enum eStep
		{
			// Token: 0x04004406 RID: 17414
			Hide,
			// Token: 0x04004407 RID: 17415
			In,
			// Token: 0x04004408 RID: 17416
			Idle,
			// Token: 0x04004409 RID: 17417
			Out,
			// Token: 0x0400440A RID: 17418
			End,
			// Token: 0x0400440B RID: 17419
			Window,
			// Token: 0x0400440C RID: 17420
			Num
		}

		// Token: 0x02000B30 RID: 2864
		public enum eButton
		{
			// Token: 0x0400440E RID: 17422
			None = -1,
			// Token: 0x0400440F RID: 17423
			Weapon,
			// Token: 0x04004410 RID: 17424
			Sale
		}
	}
}
