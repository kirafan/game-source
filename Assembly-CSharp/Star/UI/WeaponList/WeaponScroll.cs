﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B32 RID: 2866
	public class WeaponScroll : ScrollViewBase
	{
		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06003C40 RID: 15424 RVA: 0x001340E0 File Offset: 0x001324E0
		public int WeaponNum
		{
			get
			{
				return this.m_RawItemDataList.Count;
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06003C41 RID: 15425 RVA: 0x001340ED File Offset: 0x001324ED
		public int FilteredNum
		{
			get
			{
				return this.m_FilteredItemDataList.Count;
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06003C42 RID: 15426 RVA: 0x001340FA File Offset: 0x001324FA
		public WeaponScroll.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x14000106 RID: 262
		// (add) Token: 0x06003C43 RID: 15427 RVA: 0x00134104 File Offset: 0x00132504
		// (remove) Token: 0x06003C44 RID: 15428 RVA: 0x0013413C File Offset: 0x0013253C
		public event Action<WeaponScrollItemData> OnClickWeapon;

		// Token: 0x14000107 RID: 263
		// (add) Token: 0x06003C45 RID: 15429 RVA: 0x00134174 File Offset: 0x00132574
		// (remove) Token: 0x06003C46 RID: 15430 RVA: 0x001341AC File Offset: 0x001325AC
		public event Action<WeaponScrollItemData> OnHoldWeapon;

		// Token: 0x14000108 RID: 264
		// (add) Token: 0x06003C47 RID: 15431 RVA: 0x001341E4 File Offset: 0x001325E4
		// (remove) Token: 0x06003C48 RID: 15432 RVA: 0x0013421C File Offset: 0x0013261C
		public event Action OnClickRemove;

		// Token: 0x06003C49 RID: 15433 RVA: 0x00134252 File Offset: 0x00132652
		public bool IsEquipMode()
		{
			return this.m_Mode == WeaponScroll.eMode.Equip;
		}

		// Token: 0x06003C4A RID: 15434 RVA: 0x0013425D File Offset: 0x0013265D
		public void SetEquipData(long charaMngID, List<long> equippedWeapon)
		{
			this.m_CharaMngID = charaMngID;
			if (charaMngID != -1L)
			{
				this.m_ClassType = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_CharaMngID).Param.ClassType;
			}
			this.m_EquippedWeaponMngIDList = equippedWeapon;
		}

		// Token: 0x06003C4B RID: 15435 RVA: 0x0013429A File Offset: 0x0013269A
		public void SetMode(WeaponScroll.eMode mode)
		{
			this.m_Mode = mode;
			this.ApplyMode();
		}

		// Token: 0x06003C4C RID: 15436 RVA: 0x001342AC File Offset: 0x001326AC
		public void SetEnableSaleSelect(bool flg)
		{
			if (!flg)
			{
				for (int i = 0; i < this.m_RawItemDataList.Count; i++)
				{
					this.m_RawItemDataList[i].m_Enable = (this.m_RawItemDataList[i].m_SaleSelectIdx != -1);
				}
			}
			else
			{
				for (int j = 0; j < this.m_RawItemDataList.Count; j++)
				{
					this.m_RawItemDataList[j].m_Enable = !this.m_RawItemDataList[j].m_IsEquieped;
				}
			}
			base.ForceApply();
		}

		// Token: 0x06003C4D RID: 15437 RVA: 0x0013434F File Offset: 0x0013274F
		public override void Setup()
		{
			base.Setup();
			this.OpenUserWeaponList();
		}

		// Token: 0x06003C4E RID: 15438 RVA: 0x00134360 File Offset: 0x00132760
		public void OpenUserWeaponList()
		{
			WeaponScroll.eMode mode = this.m_Mode;
			if (mode != WeaponScroll.eMode.View)
			{
				if (mode == WeaponScroll.eMode.Equip)
				{
					this.SetEnbaleRemoveIcon(true);
				}
			}
			this.RemoveAll();
			this.m_ViewItemStartIdx = 0;
			if (this.m_EnableRemoveIcon)
			{
				this.AddRemoveIcon();
			}
			else if (this.m_RemoveIconViewItem != null)
			{
				this.m_RemoveIconViewItem.m_ItemIconObject.gameObject.SetActive(false);
			}
			this.m_RawItemDataList = new List<WeaponScrollItemData>();
			this.m_FilteredItemDataList = new List<WeaponScrollItemData>();
			this.SetupRawItemDataList();
			this.SortRawItemList();
			this.SetupFilteredItemList();
			this.SetScrollNormalizedPosition(1f);
			if (this.m_VerticalBar != null)
			{
				this.m_VerticalBar.gameObject.SetActive(false);
				this.m_VerticalBar.onValueChanged.AddListener(new UnityAction<float>(base.OnVerticalScrollBarValueChanged));
			}
		}

		// Token: 0x06003C4F RID: 15439 RVA: 0x0013444C File Offset: 0x0013284C
		private void ApplyMode()
		{
			if (this.m_RawItemDataList != null)
			{
				for (int i = 0; i < this.m_RawItemDataList.Count; i++)
				{
					switch (this.m_Mode)
					{
					case WeaponScroll.eMode.Equip:
						this.m_RawItemDataList[i].m_IsEquieped = false;
						for (int j = 0; j < this.m_EquippedWeaponMngIDList.Count; j++)
						{
							if (this.m_EquippedWeaponMngIDList[j] == this.m_RawItemDataList[i].m_WeaponMngID)
							{
								this.m_RawItemDataList[i].m_IsEquieped = true;
							}
						}
						break;
					case WeaponScroll.eMode.Upgrade:
					{
						WeaponParam param = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_RawItemDataList[i].m_WeaponMngID).Param;
						if (param.Lv >= param.MaxLv)
						{
							this.m_RawItemDataList[i].m_Enable = false;
						}
						break;
					}
					case WeaponScroll.eMode.Sale:
						this.m_RawItemDataList[i].m_SaleSelectIdx = -1;
						this.m_RawItemDataList[i].m_IsEquieped = false;
						this.m_RawItemDataList[i].m_Enable = true;
						for (int k = 0; k < this.m_EquippedWeaponMngIDList.Count; k++)
						{
							if (this.m_EquippedWeaponMngIDList[k] == this.m_RawItemDataList[i].m_WeaponMngID)
							{
								this.m_RawItemDataList[i].m_IsEquieped = true;
								this.m_RawItemDataList[i].m_Enable = false;
							}
						}
						break;
					}
				}
				this.SetupFilteredItemList();
				this.RebuildScroll();
				base.Refresh();
				this.m_ScrollRect.verticalNormalizedPosition = 1f;
				this.UpdateItemPosition();
			}
		}

		// Token: 0x06003C50 RID: 15440 RVA: 0x00134624 File Offset: 0x00132A24
		public void SetEnbaleRemoveIcon(bool flg)
		{
			this.m_EnableRemoveIcon = flg;
			this.ApplyMode();
		}

		// Token: 0x06003C51 RID: 15441 RVA: 0x00134634 File Offset: 0x00132A34
		public void AddRemoveIcon()
		{
			this.m_ViewItemStartIdx++;
			if (this.m_RemoveIconViewItem == null)
			{
				ScrollViewBase.DispItem dispItem = new ScrollViewBase.DispItem();
				dispItem.m_Idx = 0;
				dispItem.m_Visible = false;
				dispItem.m_PositionX = 0;
				dispItem.m_PositionY = 0;
				WeaponRemoveItem weaponRemoveItem = UnityEngine.Object.Instantiate<WeaponRemoveItem>(this.m_RemoveIconPrefab, this.m_Content);
				weaponRemoveItem.Scroll = this;
				weaponRemoveItem.RectTransform.pivot = new Vector2(0f, 1f);
				weaponRemoveItem.RectTransform.anchorMin = new Vector2(0f, 1f);
				weaponRemoveItem.RectTransform.anchorMax = new Vector2(0f, 1f);
				weaponRemoveItem.RectTransform.anchoredPosition = new Vector3(0f * (this.m_ItemWidth + this.m_SpaceX) + this.m_MarginX, --0f * (this.m_ItemHeight + this.m_SpaceY) - this.m_MarginY, 0f);
				weaponRemoveItem.RectTransform.localScale = Vector3.one;
				WeaponRemoveItem weaponRemoveItem2 = weaponRemoveItem;
				weaponRemoveItem2.OnClickIcon = (Action<ScrollItemIcon>)Delegate.Combine(weaponRemoveItem2.OnClickIcon, new Action<ScrollItemIcon>(this.OnClickRemoveIconCallBack));
				weaponRemoveItem.OnHoldIcon += this.OnHoldIconCallBack;
				weaponRemoveItem.gameObject.SetActive(true);
				dispItem.m_ItemIconObject = weaponRemoveItem;
				this.m_RemoveIconViewItem = dispItem;
			}
			else if (this.m_RemoveIconViewItem != null)
			{
				this.m_RemoveIconViewItem.m_ItemIconObject.gameObject.SetActive(true);
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06003C52 RID: 15442 RVA: 0x001347C0 File Offset: 0x00132BC0
		private void SetupRawItemDataList()
		{
			List<UserWeaponData> userWeaponDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserWeaponDatas;
			this.m_RawItemDataList.Clear();
			for (int i = 0; i < userWeaponDatas.Count; i++)
			{
				UserWeaponData userWeaponData = userWeaponDatas[i];
				if (!EditUtility.CheckDefaultWeapon(userWeaponData.MngID))
				{
					WeaponScrollItemData weaponScrollItemData = new WeaponScrollItemData(userWeaponData.MngID, WeaponScrollItemData.eStatus.None);
					if (this.m_Mode == WeaponScroll.eMode.Equip)
					{
						if (EditUtility.CanEquipWeapon(userWeaponData.MngID, this.m_CharaMngID))
						{
							weaponScrollItemData.m_Status = WeaponScrollItemData.eStatus.EnableEquip;
						}
						else
						{
							weaponScrollItemData.m_Status = WeaponScrollItemData.eStatus.DisableEquip;
						}
					}
					else if (this.m_Mode == WeaponScroll.eMode.Upgrade)
					{
						if (!EditUtility.IsMaxLevelWeapon(weaponScrollItemData.m_WeaponMngID))
						{
							weaponScrollItemData.m_Status = WeaponScrollItemData.eStatus.Upgrade;
						}
						else
						{
							weaponScrollItemData.m_Status = WeaponScrollItemData.eStatus.LevelMax;
						}
					}
					else if (this.m_Mode == WeaponScroll.eMode.Sale)
					{
						weaponScrollItemData.m_Status = WeaponScrollItemData.eStatus.Equiped;
					}
					this.m_RawItemDataList.Add(weaponScrollItemData);
				}
			}
			this.ApplyMode();
		}

		// Token: 0x06003C53 RID: 15443 RVA: 0x001348BC File Offset: 0x00132CBC
		public List<WeaponScrollItemData> GetRawDataList()
		{
			return this.m_RawItemDataList;
		}

		// Token: 0x06003C54 RID: 15444 RVA: 0x001348C4 File Offset: 0x00132CC4
		public List<WeaponScrollItemData> GetFilteredDataList()
		{
			return this.m_FilteredItemDataList;
		}

		// Token: 0x06003C55 RID: 15445 RVA: 0x001348CC File Offset: 0x00132CCC
		private void SetupFilteredItemList()
		{
			this.m_FilteredItemDataList.Clear();
			for (int i = 0; i < this.m_RawItemDataList.Count; i++)
			{
				int weaponID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_RawItemDataList[i].m_WeaponMngID).Param.WeaponID;
				WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID);
				if (this.m_ClassType == eClassType.None || this.m_ClassType == (eClassType)param.m_ClassType)
				{
					if (this.m_Mode == WeaponScroll.eMode.Equip)
					{
						if (!SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.WeaponEquip, UISettings.eFilterCategory.Rare, param.m_Rare))
						{
							goto IL_104;
						}
					}
					else
					{
						if (!SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.WeaponList, UISettings.eFilterCategory.Class, param.m_ClassType))
						{
							goto IL_104;
						}
						if (!SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.WeaponList, UISettings.eFilterCategory.Rare, param.m_Rare))
						{
							goto IL_104;
						}
					}
					this.m_FilteredItemDataList.Add(this.m_RawItemDataList[i]);
				}
				IL_104:;
			}
			this.RemoveAll();
			for (int j = 0; j < this.m_FilteredItemDataList.Count; j++)
			{
				this.AddItem(this.m_FilteredItemDataList[j]);
			}
			if (this.m_NumText != null)
			{
				this.m_NumText.text = this.m_FilteredItemDataList.Count.ToString() + "/" + SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.WeaponLimit;
			}
		}

		// Token: 0x06003C56 RID: 15446 RVA: 0x00134A80 File Offset: 0x00132E80
		protected override void OnClickIconCallBack(ScrollItemIcon itemIcon)
		{
			WeaponScrollItemData arg = (WeaponScrollItemData)itemIcon.GetItemData();
			this.OnClickWeapon.Call(arg);
		}

		// Token: 0x06003C57 RID: 15447 RVA: 0x00134AA8 File Offset: 0x00132EA8
		protected override void OnHoldIconCallBack(ScrollItemIcon itemIcon)
		{
			WeaponScrollItemData arg = (WeaponScrollItemData)itemIcon.GetItemData();
			this.OnHoldWeapon.Call(arg);
		}

		// Token: 0x06003C58 RID: 15448 RVA: 0x00134ACD File Offset: 0x00132ECD
		protected void OnClickRemoveIconCallBack(ScrollItemIcon itemIcon)
		{
			this.OnClickRemove.Call();
		}

		// Token: 0x06003C59 RID: 15449 RVA: 0x00134ADC File Offset: 0x00132EDC
		public void DoSort()
		{
			this.SortRawItemList();
			this.SetupFilteredItemList();
			this.m_EmptyDispItemList.Clear();
			for (int i = 0; i < this.m_DispItemList.Count; i++)
			{
				this.m_DispItemList[i].m_Visible = false;
				this.m_DispItemList[i].m_ItemIconObject.gameObject.SetActive(false);
				this.m_EmptyDispItemList.Add(this.m_DispItemList[i]);
			}
			this.UpdateItemPosition();
		}

		// Token: 0x06003C5A RID: 15450 RVA: 0x00134B68 File Offset: 0x00132F68
		private void SortRawItemList()
		{
			this.m_RawItemDataList.Sort(new Comparison<WeaponScrollItemData>(this.SortCompare));
			for (int i = 0; i < this.m_RawItemDataList.Count; i++)
			{
				this.m_RawItemDataList[i].m_SortedIdx = i;
			}
		}

		// Token: 0x06003C5B RID: 15451 RVA: 0x00134BBC File Offset: 0x00132FBC
		private int SortCompare(WeaponScrollItemData r, WeaponScrollItemData l)
		{
			int num = -1;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamOrderType(UISettings.eUsingType_Sort.WeaponList) == 1)
			{
				num = 1;
			}
			if (this.GetSortKey(r.m_WeaponMngID) < this.GetSortKey(l.m_WeaponMngID))
			{
				return num;
			}
			if (this.GetSortKey(r.m_WeaponMngID) > this.GetSortKey(l.m_WeaponMngID))
			{
				return -num;
			}
			if (r.m_WeaponData.Param.WeaponID < l.m_WeaponData.Param.WeaponID)
			{
				return num;
			}
			if (r.m_WeaponData.Param.WeaponID > l.m_WeaponData.Param.WeaponID)
			{
				return -num;
			}
			if (r.m_SortedIdx < l.m_SortedIdx)
			{
				return -1;
			}
			if (r.m_SortedIdx > l.m_SortedIdx)
			{
				return 1;
			}
			if (r.m_WeaponMngID < l.m_WeaponMngID)
			{
				return -1;
			}
			if (r.m_WeaponMngID > l.m_WeaponMngID)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06003C5C RID: 15452 RVA: 0x00134CC0 File Offset: 0x001330C0
		private long GetSortKey(long mngID)
		{
			switch (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.WeaponList))
			{
			case 0:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.Lv;
			case 1:
			{
				int weaponID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.WeaponID;
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID).m_Rare;
			}
			case 2:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.Cost;
			case 3:
			{
				int weaponID2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.WeaponID;
				return (long)EditUtility.CalcWeaponNum(weaponID2);
			}
			case 4:
				return mngID;
			case 5:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.Atk;
			case 6:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.Mgc;
			case 7:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.Def;
			case 8:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(mngID).Param.MDef;
			default:
				return mngID;
			}
		}

		// Token: 0x06003C5D RID: 15453 RVA: 0x00134E18 File Offset: 0x00133218
		public void DoFilter()
		{
			this.SetupFilteredItemList();
			this.m_EmptyDispItemList.Clear();
			for (int i = 0; i < this.m_DispItemList.Count; i++)
			{
				this.m_DispItemList[i].m_Visible = false;
				this.m_DispItemList[i].m_ItemIconObject.gameObject.SetActive(false);
				this.m_EmptyDispItemList.Add(this.m_DispItemList[i]);
			}
			this.m_ScrollRect.verticalNormalizedPosition = 1f;
			this.UpdateItemPosition();
		}

		// Token: 0x06003C5E RID: 15454 RVA: 0x00134EB0 File Offset: 0x001332B0
		protected override void UpdateItemPosition()
		{
			base.UpdateItemPosition();
			if (this.m_RemoveIconViewItem != null)
			{
				this.m_RemoveIconViewItem.m_ItemIconObject.RectTransform.anchoredPosition = new Vector3(0f * (this.m_ItemWidth + this.m_SpaceX) + this.m_MarginX, --0f * (this.m_ItemHeight + this.m_SpaceY) - this.m_MarginY, 0f);
			}
		}

		// Token: 0x04004412 RID: 17426
		[SerializeField]
		private Text m_NumText;

		// Token: 0x04004413 RID: 17427
		[SerializeField]
		private WeaponRemoveItem m_RemoveIconPrefab;

		// Token: 0x04004414 RID: 17428
		private eClassType m_ClassType = eClassType.None;

		// Token: 0x04004415 RID: 17429
		private long m_CharaMngID = -1L;

		// Token: 0x04004416 RID: 17430
		private List<long> m_EquippedWeaponMngIDList;

		// Token: 0x04004417 RID: 17431
		private WeaponScroll.eMode m_Mode;

		// Token: 0x04004418 RID: 17432
		private ScrollViewBase.DispItem m_RemoveIconViewItem;

		// Token: 0x04004419 RID: 17433
		private bool m_EnableRemoveIcon;

		// Token: 0x0400441A RID: 17434
		private List<WeaponScrollItemData> m_RawItemDataList;

		// Token: 0x0400441B RID: 17435
		private List<WeaponScrollItemData> m_FilteredItemDataList;

		// Token: 0x02000B33 RID: 2867
		public enum eMode
		{
			// Token: 0x04004420 RID: 17440
			View,
			// Token: 0x04004421 RID: 17441
			Equip,
			// Token: 0x04004422 RID: 17442
			Upgrade,
			// Token: 0x04004423 RID: 17443
			Sale
		}
	}
}
