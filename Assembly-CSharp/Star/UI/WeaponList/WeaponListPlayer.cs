﻿using System;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B2C RID: 2860
	public class WeaponListPlayer : SingletonMonoBehaviour<WeaponListPlayer>
	{
		// Token: 0x14000104 RID: 260
		// (add) Token: 0x06003C15 RID: 15381 RVA: 0x00132C90 File Offset: 0x00131090
		// (remove) Token: 0x06003C16 RID: 15382 RVA: 0x00132CC8 File Offset: 0x001310C8
		public event Action OnClose;

		// Token: 0x06003C17 RID: 15383 RVA: 0x00132CFE File Offset: 0x001310FE
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06003C18 RID: 15384 RVA: 0x00132D06 File Offset: 0x00131106
		public bool IsOpen()
		{
			return this.m_Step != WeaponListPlayer.eStep.Hide;
		}

		// Token: 0x06003C19 RID: 15385 RVA: 0x00132D14 File Offset: 0x00131114
		public void Open(UserBattlePartyData partyData, int slotIdx)
		{
			if (this.m_Step != WeaponListPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("WeaponListPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.m_PartyData = partyData;
			this.m_SupportPartyData = null;
			this.m_SlotIdx = slotIdx;
			this.Open();
		}

		// Token: 0x06003C1A RID: 15386 RVA: 0x00132D68 File Offset: 0x00131168
		public void Open(UserSupportPartyData supportPartyData, int slotIdx)
		{
			if (this.m_Step != WeaponListPlayer.eStep.Hide)
			{
				Debug.LogWarningFormat("WeaponListPlayer.Open() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			this.m_PartyData = null;
			this.m_SupportPartyData = supportPartyData;
			this.m_SlotIdx = slotIdx;
			this.Open();
		}

		// Token: 0x06003C1B RID: 15387 RVA: 0x00132DBC File Offset: 0x001311BC
		private void Open()
		{
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_SceneInfoStack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponEquip);
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.OverlayUI2, UIBackGroundManager.eBackGroundID.Common, true);
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			this.OnClose = null;
			this.m_Step = WeaponListPlayer.eStep.LoadUI;
		}

		// Token: 0x06003C1C RID: 15388 RVA: 0x00132E48 File Offset: 0x00131248
		public void Close()
		{
			if (this.m_Step != WeaponListPlayer.eStep.Main)
			{
				Debug.LogWarningFormat("WeaponListPlayer.Close() is failed. step:{0}", new object[]
				{
					this.m_Step
				});
				return;
			}
			if (this.m_UI != null)
			{
				this.m_UI.PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfoStack);
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.OverlayUI2);
			this.OnClose.Call();
		}

		// Token: 0x06003C1D RID: 15389 RVA: 0x00132ECC File Offset: 0x001312CC
		public void ForceHide()
		{
			this.m_Step = WeaponListPlayer.eStep.Hide;
		}

		// Token: 0x06003C1E RID: 15390 RVA: 0x00132ED8 File Offset: 0x001312D8
		private void Update()
		{
			switch (this.m_Step)
			{
			case WeaponListPlayer.eStep.LoadUI:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.WeaponListUI, true);
				this.m_Step = WeaponListPlayer.eStep.LoadWait;
				break;
			case WeaponListPlayer.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.WeaponListUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.WeaponListUI);
					this.m_Step = WeaponListPlayer.eStep.WaitBG;
				}
				break;
			case WeaponListPlayer.eStep.WaitBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = WeaponListPlayer.eStep.SetupAndPlayIn;
				}
				break;
			case WeaponListPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<WeaponListUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.WeaponListUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					if (this.m_PartyData != null)
					{
						this.m_UI.SetupEquip(this.m_PartyData, this.m_SlotIdx);
					}
					else if (this.m_SupportPartyData != null)
					{
						this.m_UI.SetupEquip(this.m_SupportPartyData, this.m_SlotIdx);
					}
					this.m_UI.OnClickButton += this.OnClickButton;
					this.m_UI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_Step = WeaponListPlayer.eStep.Main;
				}
				break;
			case WeaponListPlayer.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.WeaponListUI);
					this.m_Step = WeaponListPlayer.eStep.UnloadWait;
				}
				break;
			case WeaponListPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.WeaponListUI))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_SavedBackButtonCallBack(true);
					}
					this.m_Step = WeaponListPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x06003C1F RID: 15391 RVA: 0x001330BD File Offset: 0x001314BD
		private void OnClickBackButton(bool isCallFromShortCut)
		{
			this.OnExit();
		}

		// Token: 0x06003C20 RID: 15392 RVA: 0x001330C8 File Offset: 0x001314C8
		private void OnClickButton()
		{
			WeaponListUI.eButton selectButton = this.m_UI.SelectButton;
			if (selectButton == WeaponListUI.eButton.Weapon)
			{
				if (this.m_PartyData != null)
				{
					bool flag = false;
					for (int i = 0; i < this.m_PartyData.WeaponMngIDs.Length; i++)
					{
						if (this.m_UI.SelectedWeaponMngID != -1L && this.m_PartyData.WeaponMngIDs[i] == this.m_UI.SelectedWeaponMngID && i != this.m_SlotIdx)
						{
							int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_PartyData.CharaMngIDs[i]).Param.CharaID;
							string name = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_Name;
							int charaID2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_PartyData.CharaMngIDs[this.m_SlotIdx]).Param.CharaID;
							string name2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID2).m_Name;
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "装備の変更", SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponExchange, new object[]
							{
								null,
								name2,
								name
							}), string.Empty, new Action<int>(this.OnConfirmExchange));
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						this.ChangeWeapon();
						this.GoToMenuEnd();
					}
				}
				else if (this.m_SupportPartyData != null)
				{
					bool flag2 = false;
					for (int j = 0; j < this.m_SupportPartyData.WeaponMngIDs.Count; j++)
					{
						if (this.m_UI.SelectedWeaponMngID != -1L && this.m_SupportPartyData.WeaponMngIDs[j] == this.m_UI.SelectedWeaponMngID && j != this.m_SlotIdx)
						{
							int charaID3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_SupportPartyData.CharaMngIDs[j]).Param.CharaID;
							string name3 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID3).m_Name;
							int charaID4 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_SupportPartyData.CharaMngIDs[this.m_SlotIdx]).Param.CharaID;
							string name4 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID4).m_Name;
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, "ぶき装備", SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.WeaponExchange, new object[]
							{
								null,
								name4,
								name3
							}), null, new Action<int>(this.OnConfirmExchange));
							flag2 = true;
							break;
						}
					}
					if (!flag2)
					{
						this.ChangeWeapon();
						this.GoToMenuEnd();
					}
				}
			}
		}

		// Token: 0x06003C21 RID: 15393 RVA: 0x001333C9 File Offset: 0x001317C9
		private void OnExit()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x06003C22 RID: 15394 RVA: 0x001333D1 File Offset: 0x001317D1
		private void OnConfirmExchange(int btn)
		{
			if (btn == 0)
			{
				this.ChangeWeapon();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06003C23 RID: 15395 RVA: 0x001333E8 File Offset: 0x001317E8
		private void ChangeWeapon()
		{
			if (this.m_UI.SelectButton == WeaponListUI.eButton.Weapon)
			{
				if (this.m_PartyData != null)
				{
					if (this.m_UI.SelectedWeaponMngID != -1L)
					{
						for (int i = 0; i < this.m_PartyData.WeaponMngIDs.Length; i++)
						{
							if (this.m_PartyData.WeaponMngIDs[i] == this.m_UI.SelectedWeaponMngID)
							{
								EditUtility.RemovePartyWeapon(this.m_PartyData, i);
							}
						}
						EditUtility.AssignPartyWeapon(this.m_PartyData, this.m_SlotIdx, this.m_UI.SelectedWeaponMngID);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
					}
					else
					{
						EditUtility.RemovePartyWeapon(this.m_PartyData, this.m_SlotIdx);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
					}
				}
				else if (this.m_SupportPartyData != null)
				{
					if (this.m_UI.SelectedWeaponMngID != -1L)
					{
						for (int j = 0; j < this.m_SupportPartyData.WeaponMngIDs.Count; j++)
						{
							if (this.m_SupportPartyData.WeaponMngIDs[j] == this.m_UI.SelectedWeaponMngID)
							{
								EditUtility.RemoveSupportPartyWeapon(this.m_SupportPartyData, j);
							}
						}
						EditUtility.AssignSupportPartyWeapon(this.m_SupportPartyData, this.m_SlotIdx, this.m_UI.SelectedWeaponMngID);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
					}
					else
					{
						EditUtility.RemoveSupportPartyWeapon(this.m_SupportPartyData, this.m_SlotIdx);
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
					}
				}
			}
		}

		// Token: 0x06003C24 RID: 15396 RVA: 0x0013357D File Offset: 0x0013197D
		private void GoToMenuEnd()
		{
			this.Close();
		}

		// Token: 0x040043DF RID: 17375
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.WeaponListUI;

		// Token: 0x040043E0 RID: 17376
		private WeaponListPlayer.eStep m_Step;

		// Token: 0x040043E1 RID: 17377
		private WeaponListUI m_UI;

		// Token: 0x040043E2 RID: 17378
		private UserBattlePartyData m_PartyData;

		// Token: 0x040043E3 RID: 17379
		private UserSupportPartyData m_SupportPartyData;

		// Token: 0x040043E4 RID: 17380
		private int m_SlotIdx;

		// Token: 0x040043E5 RID: 17381
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x040043E6 RID: 17382
		private GlobalUI.SceneInfoStack m_SceneInfoStack;

		// Token: 0x02000B2D RID: 2861
		private enum eStep
		{
			// Token: 0x040043E9 RID: 17385
			Hide,
			// Token: 0x040043EA RID: 17386
			LoadUI,
			// Token: 0x040043EB RID: 17387
			LoadWait,
			// Token: 0x040043EC RID: 17388
			WaitBG,
			// Token: 0x040043ED RID: 17389
			SetupAndPlayIn,
			// Token: 0x040043EE RID: 17390
			Main,
			// Token: 0x040043EF RID: 17391
			UnloadWait
		}
	}
}
