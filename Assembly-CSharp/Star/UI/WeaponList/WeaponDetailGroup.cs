﻿using System;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B2B RID: 2859
	public class WeaponDetailGroup : UIGroup
	{
		// Token: 0x14000103 RID: 259
		// (add) Token: 0x06003C10 RID: 15376 RVA: 0x00132B78 File Offset: 0x00130F78
		// (remove) Token: 0x06003C11 RID: 15377 RVA: 0x00132BB0 File Offset: 0x00130FB0
		public event Action<long> OnEquip;

		// Token: 0x06003C12 RID: 15378 RVA: 0x00132BE8 File Offset: 0x00130FE8
		public void Open(UserWeaponData wpnData, bool equipMode, int charaCost = -1)
		{
			if (equipMode)
			{
				this.m_CloseButton.SetActive(false);
				this.m_EquipButton.SetActive(true);
				this.m_CancelButton.SetActive(true);
			}
			else
			{
				this.m_CloseButton.SetActive(true);
				this.m_EquipButton.SetActive(false);
				this.m_CancelButton.SetActive(false);
			}
			this.m_Info.SetCharaCost(charaCost);
			this.m_Info.Apply(wpnData);
			this.m_WpnMngID = wpnData.MngID;
			base.Open();
		}

		// Token: 0x06003C13 RID: 15379 RVA: 0x00132C72 File Offset: 0x00131072
		public void OnClickEquip()
		{
			this.OnEquip.Call(this.m_WpnMngID);
		}

		// Token: 0x040043D9 RID: 17369
		[SerializeField]
		private WeaponInfo m_Info;

		// Token: 0x040043DA RID: 17370
		private long m_WpnMngID;

		// Token: 0x040043DC RID: 17372
		[SerializeField]
		private GameObject m_CloseButton;

		// Token: 0x040043DD RID: 17373
		[SerializeField]
		private GameObject m_EquipButton;

		// Token: 0x040043DE RID: 17374
		[SerializeField]
		private GameObject m_CancelButton;
	}
}
