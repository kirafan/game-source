﻿using System;
using UnityEngine;

namespace Star.UI.WeaponList
{
	// Token: 0x02000B34 RID: 2868
	public class WeaponScrollItem : ScrollItemIcon
	{
		// Token: 0x06003C60 RID: 15456 RVA: 0x00134F30 File Offset: 0x00133330
		protected override void ApplyData()
		{
			this.m_WeaponScroll = (WeaponScroll)this.m_Scroll;
			this.m_WeaponData = (WeaponScrollItemData)this.m_Data;
			UserWeaponData userWpnData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserWpnData(this.m_WeaponData.m_WeaponMngID);
			int weaponID = userWpnData.Param.WeaponID;
			if (this.m_IconCloner)
			{
				this.m_IconCloner.GetInst<WeaponIconWithFrame>().Apply(userWpnData);
				this.m_IconCloner.GetInst<WeaponIconWithFrame>().ApplyDetailText((UISettings.eSortValue_Weapon)SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.WeaponList));
				if (this.m_Equip.activeSelf != this.m_WeaponData.m_IsEquieped)
				{
					this.m_Equip.SetActive(this.m_WeaponData.m_IsEquieped);
				}
				if (this.m_Sale.activeSelf != (this.m_WeaponData.m_SaleSelectIdx != -1))
				{
					this.m_Sale.SetActive(this.m_WeaponData.m_SaleSelectIdx != -1);
				}
			}
			this.m_IsDark = false;
			if (!this.m_WeaponData.m_Enable)
			{
				this.m_IsDark = true;
			}
			if (this.m_IsDark)
			{
				base.GetComponent<CustomButton>().Interactable = false;
			}
			else
			{
				base.GetComponent<CustomButton>().Interactable = true;
			}
		}

		// Token: 0x04004424 RID: 17444
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04004425 RID: 17445
		[SerializeField]
		private GameObject m_Equip;

		// Token: 0x04004426 RID: 17446
		[SerializeField]
		private GameObject m_Sale;

		// Token: 0x04004427 RID: 17447
		protected WeaponScroll m_WeaponScroll;

		// Token: 0x04004428 RID: 17448
		protected WeaponScrollItemData m_WeaponData;
	}
}
