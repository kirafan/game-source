﻿using System;

namespace Star.UI
{
	// Token: 0x02000978 RID: 2424
	public class EnumerationPanelData : InfiniteScrollItemData
	{
		// Token: 0x0600321A RID: 12826 RVA: 0x000FDE9E File Offset: 0x000FC29E
		public EquipWeaponPartyMemberController GetSelectedMember()
		{
			return this.m_Party.GetMember(this.m_NowSelectedCharaSlotIndex);
		}

		// Token: 0x0400383A RID: 14394
		public EquipWeaponPartyController m_Party;

		// Token: 0x0400383B RID: 14395
		public int m_NowSelectedCharaSlotIndex;
	}
}
