﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009B6 RID: 2486
	public class BannerScrollItemData : InfiniteScrollItemData
	{
		// Token: 0x060033A0 RID: 13216 RVA: 0x001061B6 File Offset: 0x001045B6
		public BannerScrollItemData(int infoID, Sprite sprite, string url)
		{
			this.m_InfoID = infoID;
			this.m_Url = url;
			this.m_Sprite = sprite;
		}

		// Token: 0x060033A1 RID: 13217 RVA: 0x001061DA File Offset: 0x001045DA
		public void Destroy()
		{
			this.m_Sprite = null;
		}

		// Token: 0x040039D8 RID: 14808
		public int m_InfoID = -1;

		// Token: 0x040039D9 RID: 14809
		public Sprite m_Sprite;

		// Token: 0x040039DA RID: 14810
		public string m_Url;
	}
}
