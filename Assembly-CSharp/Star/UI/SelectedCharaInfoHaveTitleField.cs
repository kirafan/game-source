﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007E3 RID: 2019
	public class SelectedCharaInfoHaveTitleField : MonoBehaviour
	{
		// Token: 0x060029B4 RID: 10676 RVA: 0x000DB7AC File Offset: 0x000D9BAC
		public virtual void Setup()
		{
		}

		// Token: 0x060029B5 RID: 10677 RVA: 0x000DB7AE File Offset: 0x000D9BAE
		public virtual void Destroy()
		{
		}

		// Token: 0x060029B6 RID: 10678 RVA: 0x000DB7B0 File Offset: 0x000D9BB0
		public void SetTitle(string title)
		{
			if (this.m_title != null)
			{
				this.m_title.SetTitle(title);
			}
		}

		// Token: 0x060029B7 RID: 10679 RVA: 0x000DB7CF File Offset: 0x000D9BCF
		public void SetTitleActive(bool active)
		{
			if (this.m_title != null)
			{
				this.m_title.SetActive(active);
			}
		}

		// Token: 0x0400302B RID: 12331
		[SerializeField]
		private HaveImageTitle m_title;
	}
}
