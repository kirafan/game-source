﻿using System;

namespace Star.UI
{
	// Token: 0x02000956 RID: 2390
	public abstract class EnumerationCharaPanelAdapterConst : EnumerationCharaPanelAdapterBaseExGen<EnumerationCharaPanelConst>
	{
		// Token: 0x06003197 RID: 12695 RVA: 0x000FEA1C File Offset: 0x000FCE1C
		public virtual EnumerationCharaPanelStandardBase.SharedInstanceEx CreateArgument(EquipWeaponPartyMemberController partyMemberController)
		{
			if (this.m_DataContent != null)
			{
				this.m_SerializedConstructInstances.m_DataContent = this.m_DataContent;
			}
			if (this.m_CharaInfo != null)
			{
				this.m_SerializedConstructInstances.m_CharaInfo = this.m_CharaInfo;
			}
			if (this.m_Blank != null)
			{
				this.m_SerializedConstructInstances.m_Blank = this.m_Blank;
			}
			if (this.m_EmptyObj != null)
			{
				this.m_SerializedConstructInstances.m_EmptyObj = this.m_EmptyObj;
			}
			if (this.m_FriendObj != null)
			{
				this.m_SerializedConstructInstances.m_FriendObj = this.m_FriendObj;
			}
			return new EnumerationCharaPanelStandardBase.SharedInstanceEx(this.m_SerializedConstructInstances)
			{
				parent = this,
				partyMemberController = partyMemberController
			};
		}

		// Token: 0x06003198 RID: 12696 RVA: 0x000FEAEE File Offset: 0x000FCEEE
		public void Apply()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Apply();
			}
		}

		// Token: 0x06003199 RID: 12697 RVA: 0x000FEB06 File Offset: 0x000FCF06
		public int GetSlotIndex()
		{
			if (this.m_Core == null)
			{
				return -1;
			}
			return this.m_Core.GetSlotIndex();
		}
	}
}
