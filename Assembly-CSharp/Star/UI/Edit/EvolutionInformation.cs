﻿using System;
using System.Text;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F1 RID: 2289
	public class EvolutionInformation : MonoBehaviour
	{
		// Token: 0x06002F78 RID: 12152 RVA: 0x000F8264 File Offset: 0x000F6664
		public void Setup(long charaMngID, CharacterEvolutionListDB_Param evolutionListParam)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.m_ItemInfomations.Length; i++)
			{
				if (evolutionListParam.m_ItemIDs[i] == -1)
				{
					this.m_ItemInfomations[i].SetActive(false);
				}
				else
				{
					int itemNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(evolutionListParam.m_ItemIDs[i]);
					int num = evolutionListParam.m_ItemNums[i];
					stringBuilder.Length = 0;
					stringBuilder.Append("所持数 ");
					if (num > itemNum)
					{
						stringBuilder.Append("<color=red>");
					}
					stringBuilder.Append(UIUtility.ItemHaveNumToString(itemNum, true));
					if (num > itemNum)
					{
						stringBuilder.Append("</color>");
					}
					this.m_ItemInfomations[i].SetActive(true);
					this.m_ItemInfomations[i].Apply(evolutionListParam.m_ItemIDs[i], itemNum, num);
				}
			}
			bool flag = (long)evolutionListParam.m_Amount > SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold;
			stringBuilder.Length = 0;
			if (flag)
			{
				stringBuilder.Append("<color=red>");
			}
			stringBuilder.Append(evolutionListParam.m_Amount);
			if (flag)
			{
				stringBuilder.Append("</color>");
			}
			this.m_NeedMoneyText.SetTitle("必要資金");
			this.m_NeedMoneyText.SetValue(UIUtility.GoldValueToString(evolutionListParam.m_Amount, true));
			bool recipeType = evolutionListParam.m_RecipeType != 0;
			bool flag2 = EditUtility.CanEvolution(charaMngID, recipeType);
			this.m_DecideButton.gameObject.SetActive(flag2);
			this.m_DetailButton.gameObject.SetActive(!flag2);
			this.m_CannotEvolveReasonsString = EditUtility.GetEvolutionFailString(charaMngID, recipeType);
		}

		// Token: 0x06002F79 RID: 12153 RVA: 0x000F8419 File Offset: 0x000F6819
		public CustomButton GetDecideButton()
		{
			return this.m_DecideButton;
		}

		// Token: 0x06002F7A RID: 12154 RVA: 0x000F8421 File Offset: 0x000F6821
		public void SetActive(bool active)
		{
			base.gameObject.SetActive(active);
		}

		// Token: 0x06002F7B RID: 12155 RVA: 0x000F842F File Offset: 0x000F682F
		public string GetCannotEvolveReasonsString()
		{
			return this.m_CannotEvolveReasonsString;
		}

		// Token: 0x0400369F RID: 13983
		[SerializeField]
		private EvolutionItemInformation[] m_ItemInfomations;

		// Token: 0x040036A0 RID: 13984
		[SerializeField]
		private TextField m_NeedMoneyText;

		// Token: 0x040036A1 RID: 13985
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040036A2 RID: 13986
		[SerializeField]
		private CustomButton m_DetailButton;

		// Token: 0x040036A3 RID: 13987
		private string m_CannotEvolveReasonsString;
	}
}
