﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000901 RID: 2305
	public class StatusDisplay : MonoBehaviour
	{
		// Token: 0x06002FD7 RID: 12247 RVA: 0x000F9584 File Offset: 0x000F7984
		public void Apply(int[] values, int[] addValues = null)
		{
			string format = "D" + 4;
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < 6; i++)
			{
				this.m_ValueText[i].text = values[i].ToString(format);
			}
			if (addValues != null)
			{
				for (int j = 0; j < 6; j++)
				{
					if (addValues[j] > 0)
					{
						this.m_AddValueText[j].gameObject.SetActive(true);
						stringBuilder.Length = 0;
						stringBuilder.Append("+");
						stringBuilder.Append(addValues[j]);
						this.m_AddValueText[j].text = stringBuilder.ToString();
					}
					else
					{
						this.m_AddValueText[j].gameObject.SetActive(false);
					}
				}
			}
			else
			{
				for (int k = 0; k < 6; k++)
				{
					this.m_AddValueText[k].gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x040036CD RID: 14029
		private const int DIGIT_MAX = 4;

		// Token: 0x040036CE RID: 14030
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x040036CF RID: 14031
		[SerializeField]
		private Text[] m_AddValueText;

		// Token: 0x02000902 RID: 2306
		public enum eStatus
		{
			// Token: 0x040036D1 RID: 14033
			Hp,
			// Token: 0x040036D2 RID: 14034
			Atk,
			// Token: 0x040036D3 RID: 14035
			Mgc,
			// Token: 0x040036D4 RID: 14036
			Def,
			// Token: 0x040036D5 RID: 14037
			MDef,
			// Token: 0x040036D6 RID: 14038
			Spd,
			// Token: 0x040036D7 RID: 14039
			Num
		}
	}
}
