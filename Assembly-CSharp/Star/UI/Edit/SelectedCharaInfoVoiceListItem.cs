﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007E6 RID: 2022
	public class SelectedCharaInfoVoiceListItem : ScrollItemIcon
	{
		// Token: 0x060029BF RID: 10687 RVA: 0x000DCB24 File Offset: 0x000DAF24
		protected override void ApplyData()
		{
			this.m_VoiceListItemData = (SelectedCharaInfoVoiceListItemData)this.m_Data;
			this.SetCategory(this.m_VoiceListItemData.category);
			this.SetVoiceTitle(this.m_VoiceListItemData.voiceTitle);
			this.SetLock(this.m_VoiceListItemData);
			this.SetEqualizerNoPlaying();
			int num = SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.GetTrackNum(this.m_VoiceListItemData.charaNamed, this.m_VoiceListItemData.cueName);
			if (num == 0)
			{
				num = 1;
			}
			this.m_ImageNumbers.SetValue(num);
		}

		// Token: 0x060029C0 RID: 10688 RVA: 0x000DCBB0 File Offset: 0x000DAFB0
		private void Update()
		{
			if (0 <= this.m_UId)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_UId))
				{
					this.m_WaitTime += Time.deltaTime;
					while (this.m_EqualizerInformations[this.m_Index].GetWaitTime() < this.m_WaitTime)
					{
						this.m_WaitTime -= this.m_EqualizerInformations[this.m_Index].GetWaitTime();
						this.m_Index++;
						if (this.m_EqualizerInformations.Length <= this.m_Index)
						{
							this.m_Index = 0;
						}
						this.SetEqualizerFirstIndex();
					}
				}
				else
				{
					this.SetEqualizerNoPlaying();
				}
			}
		}

		// Token: 0x060029C1 RID: 10689 RVA: 0x000DCC6F File Offset: 0x000DB06F
		public void SetCategory(string category)
		{
			this.m_Category.text = category;
		}

		// Token: 0x060029C2 RID: 10690 RVA: 0x000DCC7D File Offset: 0x000DB07D
		public void SetVoiceTitle(string title)
		{
			this.m_VoiceTitle.text = title;
		}

		// Token: 0x060029C3 RID: 10691 RVA: 0x000DCC8C File Offset: 0x000DB08C
		protected void SetLock(SelectedCharaInfoVoiceListItemData data)
		{
			this.SetLock(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(data.charaNamed).m_NickName, data.needFriendship);
		}

		// Token: 0x060029C4 RID: 10692 RVA: 0x000DCCC8 File Offset: 0x000DB0C8
		protected void SetLock(string charaName, int needFriendship)
		{
			string @lock = null;
			if (0 <= needFriendship)
			{
				@lock = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ProfileVoiceReleaseCondition), charaName, needFriendship);
			}
			this.SetLock(@lock);
		}

		// Token: 0x060029C5 RID: 10693 RVA: 0x000DCD06 File Offset: 0x000DB106
		protected void SetLock(string lockMessage)
		{
			if (this.m_Lock != null)
			{
				this.m_Lock.SetLock(lockMessage);
			}
		}

		// Token: 0x060029C6 RID: 10694 RVA: 0x000DCD28 File Offset: 0x000DB128
		private void SetEqualizerNoPlaying()
		{
			this.m_UId = -1;
			this.m_WaitTime = 0f;
			this.m_Index = this.m_EqualizerInformations.Length - 1;
			for (int i = 0; i < this.m_EqualizerInformations.Length; i++)
			{
				this.m_EqualizerInformations[i].SetActive(true);
			}
		}

		// Token: 0x060029C7 RID: 10695 RVA: 0x000DCD80 File Offset: 0x000DB180
		private void SetEqualizerFirstIndex()
		{
			if (this.m_Index == 0)
			{
				for (int i = 1; i < this.m_EqualizerInformations.Length; i++)
				{
					this.m_EqualizerInformations[i].SetActive(false);
				}
			}
			else
			{
				this.m_EqualizerInformations[this.m_Index].SetActive(true);
			}
		}

		// Token: 0x060029C8 RID: 10696 RVA: 0x000DCDD7 File Offset: 0x000DB1D7
		public void OnClickItem()
		{
			if (this.m_VoiceListItemData != null)
			{
				this.m_UId = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(this.m_VoiceListItemData.charaNamed, this.m_VoiceListItemData.cueName, true);
				this.m_Index = 0;
			}
		}

		// Token: 0x0400302E RID: 12334
		[SerializeField]
		private Text m_Category;

		// Token: 0x0400302F RID: 12335
		[SerializeField]
		private Text m_VoiceTitle;

		// Token: 0x04003030 RID: 12336
		[SerializeField]
		private SelectedCharaInfoVoiceListItemLock m_Lock;

		// Token: 0x04003031 RID: 12337
		[SerializeField]
		private SelectedCharaInfoVoiceListItem.EqualizerInformation[] m_EqualizerInformations;

		// Token: 0x04003032 RID: 12338
		[SerializeField]
		private ImageNumbers m_ImageNumbers;

		// Token: 0x04003033 RID: 12339
		private SelectedCharaInfoVoiceListItemData m_VoiceListItemData;

		// Token: 0x04003034 RID: 12340
		private int m_UId = 1;

		// Token: 0x04003035 RID: 12341
		private int m_Index;

		// Token: 0x04003036 RID: 12342
		private float m_WaitTime;

		// Token: 0x020007E7 RID: 2023
		[Serializable]
		public class EqualizerInformation
		{
			// Token: 0x060029CA RID: 10698 RVA: 0x000DCE1F File Offset: 0x000DB21F
			public float GetWaitTime()
			{
				return this.m_WaitTime;
			}

			// Token: 0x060029CB RID: 10699 RVA: 0x000DCE27 File Offset: 0x000DB227
			public void SetActive(bool active)
			{
				if (this.m_Object != null)
				{
					this.m_Object.SetActive(active);
				}
			}

			// Token: 0x04003037 RID: 12343
			[SerializeField]
			[Tooltip("その状態で待つ時間.")]
			private float m_WaitTime;

			// Token: 0x04003038 RID: 12344
			[SerializeField]
			[Tooltip("前の要素の待ち時間の後に表示するオブジェクト.")]
			private GameObject m_Object;
		}
	}
}
