﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000937 RID: 2359
	public class PartyDetailPanelCore : EnumerationPanelConstCoreGen<PartyDetailPanel, PartyDetailCharaPanel, PartyPanelData, PartyDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x02000938 RID: 2360
		[Serializable]
		public class SharedInstanceExOverride : EnumerationPanelCoreGenBase<PartyDetailPanel, PartyDetailCharaPanel, PartyPanelData, PartyDetailPanelCore.SharedInstanceExOverride>.SharedInstanceEx<PartyDetailPanelCore.SharedInstanceExOverride>
		{
			// Token: 0x060030DD RID: 12509 RVA: 0x000FED57 File Offset: 0x000FD157
			public SharedInstanceExOverride()
			{
			}

			// Token: 0x060030DE RID: 12510 RVA: 0x000FED5F File Offset: 0x000FD15F
			public SharedInstanceExOverride(PartyDetailPanelCore.SharedInstanceExOverride argument) : base(argument)
			{
			}

			// Token: 0x060030DF RID: 12511 RVA: 0x000FED68 File Offset: 0x000FD168
			public override PartyDetailPanelCore.SharedInstanceExOverride Clone(PartyDetailPanelCore.SharedInstanceExOverride argument)
			{
				return base.Clone(argument).CloneNewMemberOnly(argument);
			}

			// Token: 0x060030E0 RID: 12512 RVA: 0x000FED77 File Offset: 0x000FD177
			private PartyDetailPanelCore.SharedInstanceExOverride CloneNewMemberOnly(PartyDetailPanelCore.SharedInstanceExOverride argument)
			{
				return this;
			}
		}
	}
}
