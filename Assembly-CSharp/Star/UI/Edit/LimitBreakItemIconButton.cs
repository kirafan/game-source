﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008F6 RID: 2294
	public class LimitBreakItemIconButton : MonoBehaviour
	{
		// Token: 0x06002F95 RID: 12181 RVA: 0x000F89BD File Offset: 0x000F6DBD
		public void Setup()
		{
		}

		// Token: 0x06002F96 RID: 12182 RVA: 0x000F89BF File Offset: 0x000F6DBF
		private void Update()
		{
		}

		// Token: 0x06002F97 RID: 12183 RVA: 0x000F89C4 File Offset: 0x000F6DC4
		public void Apply(int itemID, string itemName, int needItems)
		{
			if (this.m_ItemIcon != null)
			{
				this.m_ItemIcon.Apply(itemID);
			}
			if (this.m_ItemName != null)
			{
				this.m_ItemName.text = itemName;
			}
			if (this.m_NeedItems != null)
			{
				this.m_NeedItems.text = needItems.ToString();
			}
		}

		// Token: 0x06002F98 RID: 12184 RVA: 0x000F8A34 File Offset: 0x000F6E34
		public void SetActive(bool active)
		{
			base.gameObject.SetActive(active);
		}

		// Token: 0x06002F99 RID: 12185 RVA: 0x000F8A42 File Offset: 0x000F6E42
		public void SetCustomButtonInteractable(bool interactable)
		{
			this.m_CustomButton.Interactable = interactable;
		}

		// Token: 0x06002F9A RID: 12186 RVA: 0x000F8A50 File Offset: 0x000F6E50
		public void SetIsSelecting(bool selecting)
		{
			this.m_Selecting.SetActive(selecting);
		}

		// Token: 0x06002F9B RID: 12187 RVA: 0x000F8A5E File Offset: 0x000F6E5E
		public void SetIsSpecialChoices(bool specialChoices)
		{
			this.m_SpecialChoices.SetActive(specialChoices);
			this.m_NotSpecialChoices.SetActive(!specialChoices);
		}

		// Token: 0x040036AC RID: 13996
		[SerializeField]
		private Text m_ItemName;

		// Token: 0x040036AD RID: 13997
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x040036AE RID: 13998
		[SerializeField]
		private Text m_NeedItems;

		// Token: 0x040036AF RID: 13999
		[SerializeField]
		private CustomButton m_CustomButton;

		// Token: 0x040036B0 RID: 14000
		[SerializeField]
		[Tooltip("専用素材の時に表示するオブジェクト")]
		private GameObject m_SpecialChoices;

		// Token: 0x040036B1 RID: 14001
		[SerializeField]
		[Tooltip("専用素材ではない時に表示するオブジェクト")]
		private GameObject m_NotSpecialChoices;

		// Token: 0x040036B2 RID: 14002
		[SerializeField]
		[Tooltip("選択されている時に表示するオブジェクト.")]
		private GameObject m_Selecting;
	}
}
