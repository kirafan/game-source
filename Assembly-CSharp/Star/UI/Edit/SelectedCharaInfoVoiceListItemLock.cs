﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007E9 RID: 2025
	public class SelectedCharaInfoVoiceListItemLock : ScrollItemIcon
	{
		// Token: 0x060029CF RID: 10703 RVA: 0x000DCE84 File Offset: 0x000DB284
		public void SetLock(string conditionMeesage)
		{
			if (conditionMeesage == null)
			{
				this.m_BlockSheet.SetActive(false);
				this.m_Message.gameObject.SetActive(false);
			}
			else
			{
				this.m_BlockSheet.SetActive(true);
				this.m_Message.gameObject.SetActive(true);
				this.m_Message.text = conditionMeesage;
			}
		}

		// Token: 0x0400303E RID: 12350
		[SerializeField]
		private GameObject m_BlockSheet;

		// Token: 0x0400303F RID: 12351
		[SerializeField]
		private Text m_Message;
	}
}
