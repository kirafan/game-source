﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F0 RID: 2288
	public class EvolutionCharacterInformationPanel : CharacterEditSceneCharacterInformationPanel
	{
		// Token: 0x06002F74 RID: 12148 RVA: 0x000F8196 File Offset: 0x000F6596
		protected override void ApplyResource(CharacterDataController cdc, CharacterDataController after)
		{
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
		}

		// Token: 0x06002F75 RID: 12149 RVA: 0x000F81A0 File Offset: 0x000F65A0
		protected override void ApplyParameter(CharacterDataController cdc, CharacterDataController after)
		{
			if (cdc == null)
			{
				return;
			}
			if (after == null)
			{
				after = cdc;
			}
			SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = new SelectedCharaInfoLevelInfo.EmphasisExpressionInfo
			{
				max = ((cdc.GetCharaMaxLv() >= after.GetCharaMaxLv()) ? ((after.GetCharaMaxLv() >= cdc.GetCharaMaxLv()) ? SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down) : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
			};
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLimitBreakIconValue(after.GetCharaLimitBreak());
			base.SetAfterLevel(after.GetCharaLv(), after.GetCharaMaxLv(), info);
			base.SetExpData(after, true, true);
			base.ApplyState(cdc, after);
		}

		// Token: 0x06002F76 RID: 12150 RVA: 0x000F8249 File Offset: 0x000F6649
		protected override void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = false;
			this.m_IsDirtyParameter = false;
		}

		// Token: 0x0400369E RID: 13982
		[SerializeField]
		[Tooltip("アイテム使用数情報.")]
		private SelectItemPanel m_InfoSelectItem;
	}
}
