﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200090D RID: 2317
	public abstract class CharacterEditResultSceneUIBase : MenuUIBase
	{
		// Token: 0x04003708 RID: 14088
		protected CharacterEditResultSceneUIBase.eStep m_Step;

		// Token: 0x0200090E RID: 2318
		public enum eStep
		{
			// Token: 0x0400370A RID: 14090
			Hide,
			// Token: 0x0400370B RID: 14091
			In,
			// Token: 0x0400370C RID: 14092
			Idle,
			// Token: 0x0400370D RID: 14093
			Out,
			// Token: 0x0400370E RID: 14094
			End
		}
	}
}
