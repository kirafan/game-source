﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000936 RID: 2358
	public class PartyDetailCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x060030DA RID: 12506 RVA: 0x000FEB78 File Offset: 0x000FCF78
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetTitleImage(cdc.GetCharaId());
			base.SetStateWeaponIconMode(WeaponIconWithFrame.eMode.NakedBGOnly);
			base.SetStateWeaponIcon(cdc);
			base.SetEnableRenderCharacterView(true);
			base.RequestLoadCharacterViewCharacterIconOnly(cdc, true);
			base.SetCharacterIconLimitBreak(cdc.GetCharaLimitBreak());
		}

		// Token: 0x060030DB RID: 12507 RVA: 0x000FEBB0 File Offset: 0x000FCFB0
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetCharaInfoTitle(cdc.GetCharaId());
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetExpData(cdc, true, true);
			base.ApplyState(cdc);
			base.SetCost(cdc.GetCost());
			base.SetCharaInfoTitleCost(cdc);
		}
	}
}
