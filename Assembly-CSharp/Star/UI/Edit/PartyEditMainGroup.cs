﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000926 RID: 2342
	public class PartyEditMainGroup : PartyEditMainGroupBase
	{
		// Token: 0x06003080 RID: 12416 RVA: 0x000FC554 File Offset: 0x000FA954
		public override void Setup(PartyEditUIBase ui, PartyEditMainGroupBase.eMode mode = PartyEditMainGroupBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				return;
			}
			if (startPartyIndex < 0)
			{
				startPartyIndex = LocalSaveData.Inst.SelectedPartyIndex;
			}
			if (startPartyIndex < 0)
			{
				startPartyIndex = 0;
			}
			if (parties == null)
			{
				parties = new EquipWeaponManagedBattlePartiesController().SetupEx(null);
			}
			base.Setup(ui, mode, startPartyIndex, parties);
			base.GetScroll().SetPageSignalActive(mode != PartyEditMainGroupBase.eMode.QuestStart);
			if (mode != PartyEditMainGroupBase.eMode.Edit)
			{
				if (mode != PartyEditMainGroupBase.eMode.Quest)
				{
					if (mode == PartyEditMainGroupBase.eMode.QuestStart)
					{
						this.m_ControlPanel.SetMode(-2);
						this.m_QuestStartControlPanel.SetMode(-1);
						base.SetOpenNameChangeButtonInteractive(false);
					}
				}
				else
				{
					this.m_ControlPanel.SetMode(1);
					this.m_QuestStartControlPanel.SetMode(-2);
					base.SetOpenNameChangeButtonInteractive(true);
				}
			}
			else
			{
				this.m_ControlPanel.SetMode(0);
				this.m_QuestStartControlPanel.SetMode(-2);
				base.SetOpenNameChangeButtonInteractive(true);
			}
			this.SetPartyScrollEditMode(mode);
		}

		// Token: 0x06003081 RID: 12417 RVA: 0x000FC65E File Offset: 0x000FAA5E
		public void OnClickADVSkipButtonCallBack()
		{
			this.m_QuestStartControlPanel.OnClickADVSkipButtonCallBack();
		}

		// Token: 0x06003082 RID: 12418 RVA: 0x000FC66B File Offset: 0x000FAA6B
		public override void OnChangePage()
		{
			base.OnChangePage();
			((BattlePartyEditStandardControlPanel)this.m_ControlPanel).OnChangePage();
		}

		// Token: 0x06003083 RID: 12419 RVA: 0x000FC683 File Offset: 0x000FAA83
		public void OpenRecommendedOrganizationButton()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.Recommend, new Action(this.ExecuteRecommended)), null, null);
		}

		// Token: 0x06003084 RID: 12420 RVA: 0x000FC6AC File Offset: 0x000FAAAC
		public void ExecuteRecommended()
		{
			List<EditUtility.EquipWeaponCharacterReferenceData> recommendedOrganizationEquipWeaponCharacterReferenceDatas = EditUtility.GetRecommendedOrganizationEquipWeaponCharacterReferenceDatas(SingletonMonoBehaviour<GameSystem>.Inst.UISettings);
			LocalSaveData.Inst.SelectedPartyIndex = base.GetScroll().SelectPartyIdx;
			for (int i = 0; i < 5; i++)
			{
				if (i < recommendedOrganizationEquipWeaponCharacterReferenceDatas.Count)
				{
					EditUtility.AssignPartyMember(LocalSaveData.Inst.SelectedPartyIndex, i, recommendedOrganizationEquipWeaponCharacterReferenceDatas[i].m_CharaMngId);
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserBattlePartyDatas[LocalSaveData.Inst.SelectedPartyIndex].SetWeaponAt(i, recommendedOrganizationEquipWeaponCharacterReferenceDatas[i].m_WeaponMngId);
				}
				else
				{
					EditUtility.RemovePartyMember(LocalSaveData.Inst.SelectedPartyIndex, i);
				}
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
			this.Resetup();
		}

		// Token: 0x04003764 RID: 14180
		[SerializeField]
		protected BattlePartyEditQuestStartControlPanel m_QuestStartControlPanel;

		// Token: 0x04003765 RID: 14181
		[SerializeField]
		private RectTransform m_TotalCostParent;

		// Token: 0x04003766 RID: 14182
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x04003767 RID: 14183
		[SerializeField]
		private RectTransform m_TotalCostParentEditRect;

		// Token: 0x04003768 RID: 14184
		[SerializeField]
		private RectTransform m_ControlPanelEditRect;

		// Token: 0x04003769 RID: 14185
		[SerializeField]
		private RectTransform m_TotalCostParentQuestRect;

		// Token: 0x0400376A RID: 14186
		[SerializeField]
		private RectTransform m_ControlPanelQuestRect;
	}
}
