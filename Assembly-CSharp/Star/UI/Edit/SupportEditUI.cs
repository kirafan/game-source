﻿using System;
using System.Collections.Generic;

namespace Star.UI.Edit
{
	// Token: 0x0200094A RID: 2378
	public class SupportEditUI : PartyEditUIBaseExGen<SupportEditMainGroup>
	{
		// Token: 0x0600316F RID: 12655 RVA: 0x000FF3D8 File Offset: 0x000FD7D8
		public void Setup(List<UserSupportData> supportDatas, string partyName, int supportLimit, PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.View)
		{
			if (supportDatas == null)
			{
				return;
			}
			List<EquipWeaponUnmanagedSupportPartiesController.UnmanagedSupportPartyInformation> list = new List<EquipWeaponUnmanagedSupportPartiesController.UnmanagedSupportPartyInformation>();
			list.Add(new EquipWeaponUnmanagedSupportPartiesController.UnmanagedSupportPartyInformation
			{
				supportDatas = supportDatas,
				partyName = partyName
			});
			EquipWeaponUnmanagedSupportPartiesController parties = new EquipWeaponUnmanagedSupportPartiesController().SetupEx(list, supportLimit);
			this.Setup(mode, 0, parties);
		}

		// Token: 0x06003170 RID: 12656 RVA: 0x000FF424 File Offset: 0x000FD824
		public override void Setup(PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null)
		{
			SupportEditMainGroup mainGroupEx = this.m_MainGroupEx;
			mainGroupEx.m_OnClickView = (Action)Delegate.Combine(mainGroupEx.m_OnClickView, new Action(this.OnClickView));
			base.Setup(mode, startPartyIndex, parties);
		}

		// Token: 0x06003171 RID: 12657 RVA: 0x000FF456 File Offset: 0x000FD856
		public void OnClickView()
		{
			base.ChangeStep(PartyEditUIBase.eStep.CharaDetail);
		}

		// Token: 0x06003172 RID: 12658 RVA: 0x000FF45F File Offset: 0x000FD85F
		protected override void UpdateStepEnd()
		{
		}

		// Token: 0x06003173 RID: 12659 RVA: 0x000FF461 File Offset: 0x000FD861
		protected override void RemoveMember(int partyIndex, int memberIndex)
		{
			EditUtility.RemoveSupportPartyMember(partyIndex, memberIndex);
		}

		// Token: 0x06003174 RID: 12660 RVA: 0x000FF46A File Offset: 0x000FD86A
		public override void OpenCharaDetailPlayer()
		{
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(this.m_MainGroupEx.SelectPartyChara, 0);
		}

		// Token: 0x06003175 RID: 12661 RVA: 0x000FF482 File Offset: 0x000FD882
		public override string GetGlobalParamPartyName(long partyMngId)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserSupportPartyData(this.m_MainGroupEx.SelectPartyMngID).PartyName;
		}

		// Token: 0x06003176 RID: 12662 RVA: 0x000FF4A3 File Offset: 0x000FD8A3
		public override void OnChangePage()
		{
			base.OnChangePage();
			this.m_MainGroupEx.OnChangePage();
		}

		// Token: 0x040037DA RID: 14298
		private const int PARTYPANEL_CHARA_ALL = 8;

		// Token: 0x040037DB RID: 14299
		private const int PARTYPANEL_CHARA_ENABLES = 8;
	}
}
