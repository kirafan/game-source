﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000927 RID: 2343
	public class PartyEditPartyDetailGroup : UIGroup
	{
		// Token: 0x06003086 RID: 12422 RVA: 0x000FC77B File Offset: 0x000FAB7B
		public PartyDetailPanel GetPanel()
		{
			return this.m_PartyDetailPanel;
		}

		// Token: 0x0400376B RID: 14187
		[SerializeField]
		[Tooltip("パーティ詳細パネル内表示部.サポートはターゲットがない箇所はserializeをnullにすればいいので,メインパネルから削除する項目に対してメンバの削除等を行わない事.")]
		protected PartyDetailPanel m_PartyDetailPanel;
	}
}
