﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200092E RID: 2350
	public class PartyPanel : PartyEditPartyPanelBase
	{
		// Token: 0x060030A9 RID: 12457 RVA: 0x000FDDF6 File Offset: 0x000FC1F6
		public override PartyEditPartyPanelBaseCore CreateCore()
		{
			return new PartyEditPartyPanelCore();
		}

		// Token: 0x060030AA RID: 12458 RVA: 0x000FDE00 File Offset: 0x000FC200
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CreateArgument()
		{
			PartyEditPartyPanelCore.SharedInstanceExOverride sharedInstanceExOverride = new PartyEditPartyPanelCore.SharedInstanceExOverride();
			if (this.m_CharaPanelPrefab != null)
			{
				sharedInstanceExOverride.m_CharaPanelPrefab = this.m_CharaPanelPrefab;
			}
			if (this.m_CharaPanelParent != null)
			{
				sharedInstanceExOverride.m_CharaPanelParent = this.m_CharaPanelParent;
			}
			if (this.m_PartyScroll != null)
			{
				sharedInstanceExOverride.m_PartyScroll = this.m_PartyScroll;
			}
			if (this.m_FriendMemberTitle != null)
			{
				sharedInstanceExOverride.m_FriendMemberTitle = this.m_FriendMemberTitle;
			}
			return new PartyEditPartyPanelCore.SharedInstanceExOverride(sharedInstanceExOverride)
			{
				parent = this
			};
		}

		// Token: 0x04003779 RID: 14201
		[SerializeField]
		private GameObject m_FriendMemberTitle;
	}
}
