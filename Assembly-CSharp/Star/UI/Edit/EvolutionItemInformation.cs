﻿using System;
using System.Text;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F2 RID: 2290
	public class EvolutionItemInformation : MonoBehaviour
	{
		// Token: 0x06002F7D RID: 12157 RVA: 0x000F8440 File Offset: 0x000F6840
		public void Apply(int itemID, int havenum, int usenum)
		{
			this.m_ItemIcon.Apply(itemID);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			if (usenum > havenum)
			{
				stringBuilder.Append("<color=red>");
			}
			stringBuilder.Append(UIUtility.ItemHaveNumToString(havenum, true));
			if (usenum > havenum)
			{
				stringBuilder.Append("</color>");
			}
			this.m_NeedNumText.SetTitle("必要");
			this.m_NeedNumText.SetValue(usenum.ToString());
			this.m_HaveNumText.SetTitle("所持");
			this.m_HaveNumText.SetValue(stringBuilder.ToString());
		}

		// Token: 0x06002F7E RID: 12158 RVA: 0x000F84E3 File Offset: 0x000F68E3
		public void SetActive(bool active)
		{
			base.gameObject.SetActive(active);
		}

		// Token: 0x040036A4 RID: 13988
		[SerializeField]
		private ItemIconWithFrame m_ItemIcon;

		// Token: 0x040036A5 RID: 13989
		[SerializeField]
		private TextField m_NeedNumText;

		// Token: 0x040036A6 RID: 13990
		[SerializeField]
		private TextField m_HaveNumText;
	}
}
