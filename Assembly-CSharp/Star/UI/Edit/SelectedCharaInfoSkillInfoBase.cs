﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007CD RID: 1997
	public class SelectedCharaInfoSkillInfoBase : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x06002957 RID: 10583 RVA: 0x000DB3B9 File Offset: 0x000D97B9
		protected void ApplySkillInfo(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, LevelInformation beforeLevel = null, long remainExp = -1L)
		{
			if (beforeLevel == null)
			{
				beforeLevel = new LevelInformation();
			}
			this.m_SkillInfo.Apply(skillDBType, skillID, ownerElement, beforeLevel.m_Now, beforeLevel.m_Max, remainExp);
		}

		// Token: 0x06002958 RID: 10584 RVA: 0x000DB3E7 File Offset: 0x000D97E7
		public override void ApplyEmpty()
		{
			this.m_SkillInfo.ApplyEmpty();
		}

		// Token: 0x04002FE5 RID: 12261
		[SerializeField]
		[Tooltip("コア.")]
		private SkillInfo m_SkillInfo;

		// Token: 0x020007CE RID: 1998
		public abstract class SkillInfoApplyArgumentBase : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x06002959 RID: 10585 RVA: 0x000DB3F4 File Offset: 0x000D97F4
			public SkillInfoApplyArgumentBase(SkillIcon.eSkillDBType in_skillDBType, int in_skillID, eElementType in_ownerElement, LevelInformation in_beforeLevel)
			{
				this.skillDBType = in_skillDBType;
				this.skillID = in_skillID;
				this.ownerElement = in_ownerElement;
				this.beforeLevel = in_beforeLevel;
			}

			// Token: 0x04002FE6 RID: 12262
			public SkillIcon.eSkillDBType skillDBType;

			// Token: 0x04002FE7 RID: 12263
			public int skillID;

			// Token: 0x04002FE8 RID: 12264
			public eElementType ownerElement;

			// Token: 0x04002FE9 RID: 12265
			public LevelInformation beforeLevel;
		}
	}
}
