﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007D1 RID: 2001
	public class SelectedCharaInfoWeaponInfo : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x0600295E RID: 10590 RVA: 0x000DB580 File Offset: 0x000D9980
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase weaponApplyArgumentBase = (SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase)argument;
			if (weaponApplyArgumentBase.GetArgumentType() == SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType.userWeaponData)
			{
				UserWeaponData argument2 = ((SelectedCharaInfoWeaponInfo.ApplyArgumentUserWeaponData)weaponApplyArgumentBase).argument;
				if (argument2 != null)
				{
					this.m_WeaponInfo.gameObject.SetActive(true);
					this.m_WeaponInfo.Apply(argument2);
					this.m_Cost.text = argument2.Param.Cost.ToString();
				}
				else
				{
					this.m_WeaponInfo.ApplyEmpty();
					this.m_WeaponInfo.gameObject.SetActive(true);
				}
			}
			else if (weaponApplyArgumentBase.GetArgumentType() == SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType.userSupportData)
			{
				EquipWeaponCharacterDataController argument3 = ((SelectedCharaInfoWeaponInfo.ApplyArgumentEquipWeaponPartyMemberController)weaponApplyArgumentBase).argument;
				int num = -1;
				if (argument3 != null && -1 < argument3.GetWeaponIdNaked())
				{
					num = argument3.GetWeaponIdNaked();
				}
				if (-1 < num)
				{
					this.m_WeaponInfo.gameObject.SetActive(true);
					this.m_WeaponInfo.Apply(argument3);
					this.m_Cost.text = argument3.GetWeaponCost().ToString();
					if (this.m_Exist != null)
					{
						this.m_Exist.SetActive(true);
					}
					if (this.m_NoExist != null)
					{
						this.m_NoExist.SetActive(false);
					}
				}
				else
				{
					if (argument3 != null && -1 < argument3.GetWeaponId())
					{
						num = argument3.GetWeaponId();
					}
					this.m_WeaponInfo.ApplyNoEquip();
					this.m_Cost.text = 0.ToString();
					if (this.m_Exist != null)
					{
						this.m_Exist.SetActive(false);
					}
					if (this.m_NoExist != null)
					{
						this.m_NoExist.SetActive(true);
					}
					this.m_WeaponInfo.gameObject.SetActive(true);
				}
			}
		}

		// Token: 0x0600295F RID: 10591 RVA: 0x000DB75C File Offset: 0x000D9B5C
		public override void ApplyEmpty()
		{
			this.m_WeaponInfo.ApplyEmpty();
		}

		// Token: 0x04002FEC RID: 12268
		[SerializeField]
		[Tooltip("コア.")]
		private WeaponInfo m_WeaponInfo;

		// Token: 0x04002FED RID: 12269
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x04002FEE RID: 12270
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x04002FEF RID: 12271
		[SerializeField]
		private Text m_Cost;

		// Token: 0x020007D2 RID: 2002
		public abstract class WeaponApplyArgumentBase : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x06002960 RID: 10592 RVA: 0x000DB769 File Offset: 0x000D9B69
			public WeaponApplyArgumentBase(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType in_argumentType)
			{
				this.argumentType = in_argumentType;
			}

			// Token: 0x06002961 RID: 10593 RVA: 0x000DB778 File Offset: 0x000D9B78
			public SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType GetArgumentType()
			{
				return this.argumentType;
			}

			// Token: 0x04002FF0 RID: 12272
			private SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType argumentType;

			// Token: 0x020007D3 RID: 2003
			public enum eArgumentType
			{
				// Token: 0x04002FF2 RID: 12274
				userWeaponData,
				// Token: 0x04002FF3 RID: 12275
				userSupportData
			}
		}

		// Token: 0x020007D4 RID: 2004
		public abstract class ApplyArgument<ArgumentType> : SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase
		{
			// Token: 0x06002962 RID: 10594 RVA: 0x000DB780 File Offset: 0x000D9B80
			public ApplyArgument(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType in_argumentType, ArgumentType in_argument) : base(in_argumentType)
			{
				this.argument = in_argument;
			}

			// Token: 0x04002FF4 RID: 12276
			public ArgumentType argument;
		}

		// Token: 0x020007D5 RID: 2005
		public class ApplyArgumentUserWeaponData : SelectedCharaInfoWeaponInfo.ApplyArgument<UserWeaponData>
		{
			// Token: 0x06002963 RID: 10595 RVA: 0x000DB790 File Offset: 0x000D9B90
			public ApplyArgumentUserWeaponData(UserWeaponData in_argument) : base(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType.userWeaponData, in_argument)
			{
			}
		}

		// Token: 0x020007D6 RID: 2006
		public class ApplyArgumentEquipWeaponPartyMemberController : SelectedCharaInfoWeaponInfo.ApplyArgument<EquipWeaponCharacterDataController>
		{
			// Token: 0x06002964 RID: 10596 RVA: 0x000DB79A File Offset: 0x000D9B9A
			public ApplyArgumentEquipWeaponPartyMemberController(EquipWeaponCharacterDataController in_argument) : base(SelectedCharaInfoWeaponInfo.WeaponApplyArgumentBase.eArgumentType.userSupportData, in_argument)
			{
			}
		}
	}
}
