﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200092B RID: 2347
	public class PartyEditUI : PartyEditUIBaseExGen<PartyEditMainGroup>
	{
		// Token: 0x06003097 RID: 12439 RVA: 0x000FD4A0 File Offset: 0x000FB8A0
		public void Setup(int startPartyIndex, PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, UserSupportData supportData = null)
		{
			CharacterDataWrapperBase supportMember = null;
			if (mode == PartyEditUIBase.eMode.QuestStart)
			{
				if (supportData != null)
				{
					supportMember = new UserSupportDataWrapper(supportData);
				}
				else
				{
					supportMember = new CharacterDataEmpty();
				}
			}
			this.Setup(mode, startPartyIndex, new EquipWeaponManagedBattlePartiesController().SetupEx(supportMember));
			if (this.m_Mode == PartyEditUIBase.eMode.QuestStart)
			{
				this.m_QuestStartGroup.Open();
			}
		}

		// Token: 0x06003098 RID: 12440 RVA: 0x000FD4F8 File Offset: 0x000FB8F8
		protected override void RemoveMember(int partyIndex, int memberIndex)
		{
			EditUtility.RemovePartyMember(partyIndex, memberIndex);
		}

		// Token: 0x06003099 RID: 12441 RVA: 0x000FD501 File Offset: 0x000FB901
		public override void OpenCharaDetailPlayer()
		{
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.OpenManagedBattlePartyMember(this.m_MainGroupEx.SelectPartyIndex, this.m_MainGroupEx.SelectCharaIndex, false);
		}

		// Token: 0x0600309A RID: 12442 RVA: 0x000FD524 File Offset: 0x000FB924
		public override string GetGlobalParamPartyName(long partyMngId)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserBattlePartyData(this.m_MainGroupEx.GetScroll().SelectPartyMngID).PartyName;
		}

		// Token: 0x0600309B RID: 12443 RVA: 0x000FD54A File Offset: 0x000FB94A
		public void OnClickDecideButtonCallBack()
		{
			this.m_SelectButton = PartyEditUIBase.eButton.Decide;
			base.ExecuteOnClickButton();
		}

		// Token: 0x0600309C RID: 12444 RVA: 0x000FD559 File Offset: 0x000FB959
		public void OnClickADVSkipButtonCallBack()
		{
			this.m_MainGroupEx.OnClickADVSkipButtonCallBack();
		}

		// Token: 0x0600309D RID: 12445 RVA: 0x000FD566 File Offset: 0x000FB966
		public void OnClickRecommendedOrganizationButton()
		{
			this.m_MainGroupEx.OpenRecommendedOrganizationButton();
		}

		// Token: 0x0600309E RID: 12446 RVA: 0x000FD573 File Offset: 0x000FB973
		public override void OnClickCharaCallBack()
		{
			if (this.m_Mode != PartyEditUIBase.eMode.QuestStart)
			{
				base.OnClickCharaCallBack();
			}
			else
			{
				this.m_QuestStartGroup.ShowWarning();
			}
		}

		// Token: 0x0600309F RID: 12447 RVA: 0x000FD597 File Offset: 0x000FB997
		public override void OnChangePage()
		{
			this.m_MainGroupEx.OnChangePage();
		}

		// Token: 0x04003776 RID: 14198
		[SerializeField]
		private PartyEditQuestStartGroup m_QuestStartGroup;
	}
}
