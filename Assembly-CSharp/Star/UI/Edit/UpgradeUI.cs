﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200090C RID: 2316
	public class UpgradeUI : CharaEditSceneUI<UpgradeMaterialPanel, EditUtility.SimulateUpgradeParam>
	{
		// Token: 0x06003019 RID: 12313 RVA: 0x000FA48C File Offset: 0x000F888C
		protected override EditUtility.SimulateUpgradeParam Simulate(EquipWeaponCharacterDataController cdc)
		{
			SelectItemPanel.UseItemInfos selectItemInfos = this.m_MaterialPanel.GetSelectItemInfos();
			return EditUtility.SimulateUpgrade(cdc.GetCharaMngId(), selectItemInfos);
		}

		// Token: 0x0600301A RID: 12314 RVA: 0x000FA4B4 File Offset: 0x000F88B4
		protected override CharacterDataController ConvertSimulateParamToAfterCharacterData(EditUtility.SimulateUpgradeParam param)
		{
			if (param == null)
			{
				return null;
			}
			SelectItemPanel.UseItemInfos selectItemInfos = this.m_MaterialPanel.GetSelectItemInfos();
			int num = 0;
			if (selectItemInfos != null)
			{
				num = selectItemInfos.GetNum(0);
			}
			if (num <= 0)
			{
				return null;
			}
			return new AfterUpgradeCharacterController().Setup(new AfterUpgradeParamWrapper(param));
		}

		// Token: 0x0600301B RID: 12315 RVA: 0x000FA500 File Offset: 0x000F8900
		protected override void ApplyMaterialPanel(EditUtility.SimulateUpgradeParam param)
		{
			SelectItemPanel.UseItemInfos selectItemInfos = this.m_MaterialPanel.GetSelectItemInfos();
			bool flag = false;
			if (selectItemInfos != null)
			{
				flag = selectItemInfos.IsSomethingSelected();
			}
			this.m_MaterialPanel.SetDecideButtonInteractable(!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)param.amount) && flag);
			this.m_MaterialPanel.IsEnableIncrease = (param.after.Lv < param.nowUserCharaData.Param.MaxLv);
			this.m_MaterialPanel.GetUpgradeInfoPanel().Apply(param.exp, (long)param.amount);
		}

		// Token: 0x0600301C RID: 12316 RVA: 0x000FA5A8 File Offset: 0x000F89A8
		protected override eText_MessageDB GetCheckWindowTitle()
		{
			return eText_MessageDB.CharacterUpgradeCheckWindowTitle;
		}

		// Token: 0x0600301D RID: 12317 RVA: 0x000FA5AF File Offset: 0x000F89AF
		protected override eText_MessageDB GetCheckWindowMessage()
		{
			return eText_MessageDB.CharacterUpgradeCheckWindowMessage;
		}

		// Token: 0x0600301E RID: 12318 RVA: 0x000FA5B6 File Offset: 0x000F89B6
		public void OnClickResetButtonCallBack()
		{
			this.m_MaterialPanel.ResetUseItem();
			base.Refresh();
		}

		// Token: 0x0600301F RID: 12319 RVA: 0x000FA5C9 File Offset: 0x000F89C9
		[Obsolete]
		public SelectItemPanel GetSelectItemPanel()
		{
			if (this.m_MaterialPanel == null)
			{
				return null;
			}
			return this.m_MaterialPanel.GetSelectItemPanel();
		}

		// Token: 0x06003020 RID: 12320 RVA: 0x000FA5E9 File Offset: 0x000F89E9
		public SelectItemPanel.UseItemInfos GetSelectItemInfos()
		{
			if (this.m_MaterialPanel == null)
			{
				return null;
			}
			return this.m_MaterialPanel.GetSelectItemInfos();
		}
	}
}
