﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007C3 RID: 1987
	public class SelectedCharaInfoDetailInfoGroupBase : MonoBehaviour
	{
		// Token: 0x0600293D RID: 10557 RVA: 0x000DADBB File Offset: 0x000D91BB
		public virtual void Setup()
		{
		}

		// Token: 0x0600293E RID: 10558 RVA: 0x000DADBD File Offset: 0x000D91BD
		public virtual void Destroy()
		{
		}

		// Token: 0x0600293F RID: 10559 RVA: 0x000DADBF File Offset: 0x000D91BF
		public virtual void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06002940 RID: 10560 RVA: 0x000DADC1 File Offset: 0x000D91C1
		public virtual SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.None;
		}

		// Token: 0x04002FCE RID: 12238
		[SerializeField]
		[Tooltip("スキルの種類の頭に付く種類グループのタイトル.")]
		protected HaveImageTitle m_GroupTypeLabel;

		// Token: 0x020007C4 RID: 1988
		public enum eSkillInfoGroupType
		{
			// Token: 0x04002FD0 RID: 12240
			None,
			// Token: 0x04002FD1 RID: 12241
			UniquieSkill,
			// Token: 0x04002FD2 RID: 12242
			GeneralSkill,
			// Token: 0x04002FD3 RID: 12243
			Weapon,
			// Token: 0x04002FD4 RID: 12244
			WeaponSkill,
			// Token: 0x04002FD5 RID: 12245
			MasterSkill,
			// Token: 0x04002FD6 RID: 12246
			ScheduleDropState
		}
	}
}
