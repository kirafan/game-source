﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008FA RID: 2298
	public class SelectItemPanel : MonoBehaviour
	{
		// Token: 0x06002FB0 RID: 12208 RVA: 0x000F8F5C File Offset: 0x000F735C
		public void Setup()
		{
			this.m_UseItemIconInfo = new SelectItemPanel.UseItemIconInfo[this.m_UseItemObjs.Length];
			for (int i = 0; i < this.m_UseItemObjs.Length; i++)
			{
				this.m_UseItemIconInfo[i] = new SelectItemPanel.UseItemIconInfo(this.m_UseItemObjs[i]);
			}
			this.m_UseItemInfoGetterAdapters = new SelectItemPanel.UseItemInfos(this.m_UseItemIconInfo);
			this.ResetUseItem();
		}

		// Token: 0x06002FB1 RID: 12209 RVA: 0x000F8FC4 File Offset: 0x000F73C4
		public bool CanAddUseItem(int itemID)
		{
			bool result = false;
			for (int i = 0; i < this.m_UseItemIconInfo.Length; i++)
			{
				if (this.m_UseItemInfoGetterAdapters.GetId(i) == itemID)
				{
					return this.m_UseItemInfoGetterAdapters.GetNum(i) < SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(this.m_UseItemInfoGetterAdapters.GetId(i)) && this.m_UseItemInfoGetterAdapters.GetNum(i) <= 99;
				}
				if (this.m_UseItemInfoGetterAdapters.GetId(i) == -1)
				{
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06002FB2 RID: 12210 RVA: 0x000F9058 File Offset: 0x000F7458
		public int GetUseItemNumID(int itemID)
		{
			for (int i = 0; i < this.m_UseItemIconInfo.Length; i++)
			{
				if (this.m_UseItemInfoGetterAdapters.GetId(i) == itemID)
				{
					return this.m_UseItemInfoGetterAdapters.GetNum(i);
				}
			}
			return 0;
		}

		// Token: 0x06002FB3 RID: 12211 RVA: 0x000F90A0 File Offset: 0x000F74A0
		public bool SetUseItemNum(int itemID, int num)
		{
			for (int i = 0; i < this.m_UseItemIconInfo.Length; i++)
			{
				SelectItemPanel.UseItemInfo info = this.m_UseItemInfoGetterAdapters.GetInfo(i);
				if (info.GetId() == itemID)
				{
					return this.SetUseItemNum(i, itemID, num);
				}
			}
			return this.SetUseItemNum(-1, itemID, num);
		}

		// Token: 0x06002FB4 RID: 12212 RVA: 0x000F90F4 File Offset: 0x000F74F4
		private bool SetUseItemNum(int index, int itemID, int num)
		{
			if (0 <= index)
			{
				SelectItemPanel.UseItemIconInfo useItemIconInfo = this.m_UseItemIconInfo[index];
				SelectItemPanel.UseItemInfo info = this.m_UseItemInfoGetterAdapters.GetInfo(index);
				if (info.GetId() == itemID)
				{
					if (num > 0)
					{
						useItemIconInfo.SetNum(num);
					}
					else
					{
						useItemIconInfo.SetNum(0);
						bool flag = true;
						int num2 = index + 1;
						if (num2 < this.m_UseItemIconInfo.Length && -1 < this.m_UseItemInfoGetterAdapters.GetId(num2))
						{
							int id = this.m_UseItemInfoGetterAdapters.GetId(num2);
							int num3 = this.m_UseItemInfoGetterAdapters.GetNum(num2);
							this.SetUseItemNum(num2, id, 0);
							this.SetUseItemNum(index, id, num3);
							flag = false;
						}
						if (flag)
						{
							this.m_UseItemIconInfo[index].SetActive(false);
						}
					}
					return true;
				}
			}
			if (index < 0)
			{
				for (int i = 0; i < this.m_UseItemIconInfo.Length; i++)
				{
					if (this.m_UseItemInfoGetterAdapters.GetId(i) == -1)
					{
						index = i;
						break;
					}
				}
			}
			if (0 <= index && this.m_UseItemInfoGetterAdapters.GetId(index) == -1)
			{
				this.m_UseItemIconInfo[index].SetActive(true);
				this.m_UseItemIconInfo[index].SetId(itemID);
				this.m_UseItemIconInfo[index].SetNum(num);
				return true;
			}
			return false;
		}

		// Token: 0x06002FB5 RID: 12213 RVA: 0x000F923C File Offset: 0x000F763C
		public void ResetUseItem()
		{
			for (int i = 0; i < this.m_UseItemIconInfo.Length; i++)
			{
				this.m_UseItemIconInfo[i].Reset();
			}
		}

		// Token: 0x06002FB6 RID: 12214 RVA: 0x000F926F File Offset: 0x000F766F
		public SelectItemPanel.UseItemInfos GetUseItemInfos()
		{
			return this.m_UseItemInfoGetterAdapters;
		}

		// Token: 0x06002FB7 RID: 12215 RVA: 0x000F9277 File Offset: 0x000F7677
		public int GetUseItemId(int index)
		{
			if (this.m_UseItemIconInfo == null)
			{
				return -1;
			}
			return this.m_UseItemInfoGetterAdapters.GetId(index);
		}

		// Token: 0x06002FB8 RID: 12216 RVA: 0x000F9292 File Offset: 0x000F7692
		public int GetUseItemNumIndex(int index)
		{
			if (this.m_UseItemIconInfo == null)
			{
				return -1;
			}
			return this.m_UseItemInfoGetterAdapters.GetNum(index);
		}

		// Token: 0x06002FB9 RID: 12217 RVA: 0x000F92AD File Offset: 0x000F76AD
		public int GetItemsLimit()
		{
			if (this.m_UseItemIconInfo == null)
			{
				return -1;
			}
			return this.m_UseItemIconInfo.Length;
		}

		// Token: 0x06002FBA RID: 12218 RVA: 0x000F92C4 File Offset: 0x000F76C4
		public void OnClickIconCallBack(int idx)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ItemDetailWindow.OpenItem(this.m_UseItemInfoGetterAdapters.GetId(idx));
		}

		// Token: 0x040036BC RID: 14012
		public const int USE_MATERIAL_MAX = 99;

		// Token: 0x040036BD RID: 14013
		public const int REBUILD_FRAMES = 30;

		// Token: 0x040036BE RID: 14014
		[SerializeField]
		private GameObject[] m_UseItemObjs;

		// Token: 0x040036BF RID: 14015
		private SelectItemPanel.UseItemIconInfo[] m_UseItemIconInfo;

		// Token: 0x040036C0 RID: 14016
		private SelectItemPanel.UseItemInfos m_UseItemInfoGetterAdapters;

		// Token: 0x020008FB RID: 2299
		public enum eMode
		{
			// Token: 0x040036C2 RID: 14018
			Chara,
			// Token: 0x040036C3 RID: 14019
			Weapon
		}

		// Token: 0x020008FC RID: 2300
		public abstract class UseItemInfo
		{
			// Token: 0x06002FBC RID: 12220
			public abstract int GetId();

			// Token: 0x06002FBD RID: 12221
			public abstract int GetNum();
		}

		// Token: 0x020008FD RID: 2301
		public class UseItemInfoEditable : SelectItemPanel.UseItemInfo
		{
			// Token: 0x06002FBE RID: 12222 RVA: 0x000F92E9 File Offset: 0x000F76E9
			public UseItemInfoEditable()
			{
				this.m_Adapter = new SelectItemPanel.UseItemInfoGetterAdapter(this);
				this.SetId(-1);
				this.SetNum(-1);
			}

			// Token: 0x06002FBF RID: 12223 RVA: 0x000F9319 File Offset: 0x000F7719
			public SelectItemPanel.UseItemInfo GetAdapter()
			{
				return this.m_Adapter;
			}

			// Token: 0x06002FC0 RID: 12224 RVA: 0x000F9321 File Offset: 0x000F7721
			public override int GetId()
			{
				return this.m_ID;
			}

			// Token: 0x06002FC1 RID: 12225 RVA: 0x000F9329 File Offset: 0x000F7729
			public override int GetNum()
			{
				return this.m_Num;
			}

			// Token: 0x06002FC2 RID: 12226 RVA: 0x000F9331 File Offset: 0x000F7731
			public void SetId(int id)
			{
				this.m_ID = id;
			}

			// Token: 0x06002FC3 RID: 12227 RVA: 0x000F933A File Offset: 0x000F773A
			public void SetNum(int num)
			{
				if (num > 0)
				{
					this.m_Num = num;
				}
				else
				{
					this.m_ID = -1;
					this.m_Num = 0;
				}
			}

			// Token: 0x040036C4 RID: 14020
			private SelectItemPanel.UseItemInfoGetterAdapter m_Adapter;

			// Token: 0x040036C5 RID: 14021
			private int m_ID = -1;

			// Token: 0x040036C6 RID: 14022
			private int m_Num = -1;
		}

		// Token: 0x020008FE RID: 2302
		public class UseItemInfoGetterAdapter : SelectItemPanel.UseItemInfo
		{
			// Token: 0x06002FC4 RID: 12228 RVA: 0x000F935D File Offset: 0x000F775D
			public UseItemInfoGetterAdapter(SelectItemPanel.UseItemInfo info)
			{
				this.m_Info = info;
			}

			// Token: 0x06002FC5 RID: 12229 RVA: 0x000F936C File Offset: 0x000F776C
			public override int GetId()
			{
				return this.m_Info.GetId();
			}

			// Token: 0x06002FC6 RID: 12230 RVA: 0x000F9379 File Offset: 0x000F7779
			public override int GetNum()
			{
				return this.m_Info.GetNum();
			}

			// Token: 0x040036C7 RID: 14023
			private SelectItemPanel.UseItemInfo m_Info;
		}

		// Token: 0x020008FF RID: 2303
		public class UseItemInfos
		{
			// Token: 0x06002FC7 RID: 12231 RVA: 0x000F9388 File Offset: 0x000F7788
			public UseItemInfos(SelectItemPanel.UseItemIconInfo[] useItemIconInfo)
			{
				this.m_UseItemInfos = new SelectItemPanel.UseItemInfoGetterAdapter[useItemIconInfo.Length];
				for (int i = 0; i < useItemIconInfo.Length; i++)
				{
					this.m_UseItemInfos[i] = useItemIconInfo[i].GetAdapter();
				}
			}

			// Token: 0x06002FC8 RID: 12232 RVA: 0x000F93CD File Offset: 0x000F77CD
			public int HowManyAdapters()
			{
				return this.m_UseItemInfos.Length;
			}

			// Token: 0x06002FC9 RID: 12233 RVA: 0x000F93D7 File Offset: 0x000F77D7
			public SelectItemPanel.UseItemInfo GetInfo(int index)
			{
				if (index < 0 || this.m_UseItemInfos.Length <= index)
				{
					return null;
				}
				return this.m_UseItemInfos[index];
			}

			// Token: 0x06002FCA RID: 12234 RVA: 0x000F93F8 File Offset: 0x000F77F8
			public int GetId(int index)
			{
				SelectItemPanel.UseItemInfo info = this.GetInfo(index);
				if (info == null)
				{
					return -1;
				}
				return info.GetId();
			}

			// Token: 0x06002FCB RID: 12235 RVA: 0x000F941C File Offset: 0x000F781C
			public int GetNum(int index)
			{
				SelectItemPanel.UseItemInfo info = this.GetInfo(index);
				if (info == null)
				{
					return -1;
				}
				return info.GetNum();
			}

			// Token: 0x06002FCC RID: 12236 RVA: 0x000F943F File Offset: 0x000F783F
			public bool IsSomethingSelected()
			{
				return 0 < this.GetNum(0);
			}

			// Token: 0x040036C8 RID: 14024
			private SelectItemPanel.UseItemInfo[] m_UseItemInfos;
		}

		// Token: 0x02000900 RID: 2304
		public class UseItemIconInfo
		{
			// Token: 0x06002FCD RID: 12237 RVA: 0x000F944B File Offset: 0x000F784B
			public UseItemIconInfo(GameObject gO)
			{
				this.m_GameObject = gO;
				this.m_Number = gO.GetComponentInChildren<ImageNumbers>();
				this.m_IconComponent = gO.GetComponentInChildren<PrefabCloner>().GetInst<ItemIconWithFrame>();
				this.m_UseItemInfo = new SelectItemPanel.UseItemInfoEditable();
				this.ApplyIcon(-1);
			}

			// Token: 0x06002FCE RID: 12238 RVA: 0x000F948C File Offset: 0x000F788C
			public void SetId(int id)
			{
				if (this.GetId() != id)
				{
					if (this.m_UseItemInfo == null)
					{
						this.m_UseItemInfo = new SelectItemPanel.UseItemInfoEditable();
					}
					this.m_UseItemInfo.SetId(id);
					this.ApplyIcon(this.m_UseItemInfo.GetId());
				}
			}

			// Token: 0x06002FCF RID: 12239 RVA: 0x000F94D8 File Offset: 0x000F78D8
			public void SetNum(int num)
			{
				if (this.m_UseItemInfo == null)
				{
					this.m_UseItemInfo = new SelectItemPanel.UseItemInfoEditable();
				}
				this.m_UseItemInfo.SetNum(num);
				this.m_Number.SetValue(num);
			}

			// Token: 0x06002FD0 RID: 12240 RVA: 0x000F9508 File Offset: 0x000F7908
			public void SetActive(bool active)
			{
				this.m_GameObject.SetActive(active);
			}

			// Token: 0x06002FD1 RID: 12241 RVA: 0x000F9516 File Offset: 0x000F7916
			private void ApplyIcon(int id)
			{
				this.m_IconComponent.Apply(id);
			}

			// Token: 0x06002FD2 RID: 12242 RVA: 0x000F9524 File Offset: 0x000F7924
			public void Reset()
			{
				this.SetActive(false);
				this.SetId(-1);
				this.SetNum(0);
			}

			// Token: 0x06002FD3 RID: 12243 RVA: 0x000F953B File Offset: 0x000F793B
			public SelectItemPanel.UseItemInfo GetAdapter()
			{
				return this.m_UseItemInfo.GetAdapter();
			}

			// Token: 0x06002FD4 RID: 12244 RVA: 0x000F9548 File Offset: 0x000F7948
			private int GetId()
			{
				if (this.m_UseItemInfo == null)
				{
					return -1;
				}
				return this.m_UseItemInfo.GetId();
			}

			// Token: 0x06002FD5 RID: 12245 RVA: 0x000F9562 File Offset: 0x000F7962
			private int GetNum()
			{
				if (this.m_UseItemInfo == null)
				{
					return 0;
				}
				return this.m_UseItemInfo.GetNum();
			}

			// Token: 0x040036C9 RID: 14025
			private GameObject m_GameObject;

			// Token: 0x040036CA RID: 14026
			private ItemIconWithFrame m_IconComponent;

			// Token: 0x040036CB RID: 14027
			private ImageNumbers m_Number;

			// Token: 0x040036CC RID: 14028
			private SelectItemPanel.UseItemInfoEditable m_UseItemInfo;
		}
	}
}
