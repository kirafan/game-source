﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007C5 RID: 1989
	public class SelectedCharaInfoDetailInfoGroupBaseExGen<DetailInfoType, ArgumentType> : SelectedCharaInfoDetailInfoGroupBase where DetailInfoType : SelectedCharaInfoDetailInfoBase where ArgumentType : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
	{
		// Token: 0x06002942 RID: 10562 RVA: 0x000DAF1C File Offset: 0x000D931C
		public override void Setup()
		{
			base.Setup();
			for (int i = 0; i < this.m_SkillInfos.Length; i++)
			{
				this.m_SkillInfos[i].Setup();
			}
		}

		// Token: 0x06002943 RID: 10563 RVA: 0x000DAF61 File Offset: 0x000D9361
		protected void SetupProcess(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType type, string title)
		{
			this.m_GroupType = type;
			this.m_GroupTypeLabel.SetTitle(title);
		}

		// Token: 0x06002944 RID: 10564 RVA: 0x000DAF78 File Offset: 0x000D9378
		public override void Destroy()
		{
			if (this.m_SkillInfos != null)
			{
				for (int i = 0; i < this.m_SkillInfos.Length; i++)
				{
					this.m_SkillInfos[i].Destroy();
					this.m_SkillInfos[i] = (DetailInfoType)((object)null);
				}
				this.m_SkillInfos = null;
			}
		}

		// Token: 0x06002945 RID: 10565 RVA: 0x000DAFDB File Offset: 0x000D93DB
		public override void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			if (this.m_SkillInfos != null)
			{
				this.m_SkillInfos[index].Apply(argument);
			}
		}

		// Token: 0x06002946 RID: 10566 RVA: 0x000DB002 File Offset: 0x000D9402
		public override SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return this.m_GroupType;
		}

		// Token: 0x04002FD7 RID: 12247
		protected SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType m_GroupType;

		// Token: 0x04002FD8 RID: 12248
		[SerializeField]
		[Tooltip("種類グループに該当するスキル一覧.")]
		private DetailInfoType[] m_SkillInfos;
	}
}
