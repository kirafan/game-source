﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F7 RID: 2295
	public class LimitBreakItemInformation : MonoBehaviour
	{
		// Token: 0x06002F9D RID: 12189 RVA: 0x000F8A83 File Offset: 0x000F6E83
		public void Setup()
		{
			this.m_ItemIconButton.Setup();
		}

		// Token: 0x06002F9E RID: 12190 RVA: 0x000F8A90 File Offset: 0x000F6E90
		public void Apply(int itemID, string itemName, int needItems, int haveItems)
		{
			if (this.m_ItemIconButton != null)
			{
				this.m_ItemIconButton.Apply(itemID, itemName, needItems);
			}
			if (this.m_HaveItemsLabel != null)
			{
				this.m_HaveItemsLabel.SetTitle("所持数");
			}
			if (this.m_HaveItemsLabel != null)
			{
				this.m_HaveItemsLabel.SetValue(UIUtility.ItemHaveNumToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(itemID), true));
			}
		}

		// Token: 0x06002F9F RID: 12191 RVA: 0x000F8B0F File Offset: 0x000F6F0F
		public void SetActive(bool active)
		{
			base.gameObject.SetActive(active);
		}

		// Token: 0x06002FA0 RID: 12192 RVA: 0x000F8B1D File Offset: 0x000F6F1D
		public void SetCustomButtonInteractable(bool interactable)
		{
			this.m_ItemIconButton.SetCustomButtonInteractable(interactable);
		}

		// Token: 0x06002FA1 RID: 12193 RVA: 0x000F8B2B File Offset: 0x000F6F2B
		public void SetIsSelecting(bool selecting)
		{
			this.m_ItemIconButton.SetIsSelecting(selecting);
		}

		// Token: 0x040036B3 RID: 14003
		[SerializeField]
		private LimitBreakItemIconButton m_ItemIconButton;

		// Token: 0x040036B4 RID: 14004
		[SerializeField]
		[Tooltip("所持数.")]
		private TextField m_HaveItemsLabel;
	}
}
