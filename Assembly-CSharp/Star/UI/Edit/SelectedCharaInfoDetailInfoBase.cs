﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007C0 RID: 1984
	public class SelectedCharaInfoDetailInfoBase : MonoBehaviour
	{
		// Token: 0x06002931 RID: 10545 RVA: 0x000DADA3 File Offset: 0x000D91A3
		public virtual void Setup()
		{
		}

		// Token: 0x06002932 RID: 10546 RVA: 0x000DADA5 File Offset: 0x000D91A5
		public virtual void Destroy()
		{
		}

		// Token: 0x06002933 RID: 10547 RVA: 0x000DADA7 File Offset: 0x000D91A7
		public virtual void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
		}

		// Token: 0x06002934 RID: 10548 RVA: 0x000DADA9 File Offset: 0x000D91A9
		public virtual void ApplyEmpty()
		{
		}

		// Token: 0x020007C1 RID: 1985
		public abstract class ApplyArgumentBase
		{
		}
	}
}
