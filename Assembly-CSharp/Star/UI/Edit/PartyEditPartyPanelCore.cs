﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200092C RID: 2348
	public class PartyEditPartyPanelCore : PartyEditPartyPanelBaseCore
	{
		// Token: 0x060030A1 RID: 12449 RVA: 0x000FDB1C File Offset: 0x000FBF1C
		public override void Apply(EnumerationPanelData data)
		{
			base.Apply(data);
			this.SetPartyData((PartyPanelData)data);
			this.m_SharedIntance.m_FriendMemberTitle.SetActive(5 < base.HowManyAllChildren());
			LayoutRebuilder.MarkLayoutForRebuild(this.m_SharedIntance.parent.transform as RectTransform);
		}

		// Token: 0x060030A2 RID: 12450 RVA: 0x000FDB6F File Offset: 0x000FBF6F
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase GetSharedIntance()
		{
			return this.m_SharedIntance;
		}

		// Token: 0x060030A3 RID: 12451 RVA: 0x000FDB77 File Offset: 0x000FBF77
		public override void SetSharedIntance(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase arg)
		{
			this.m_SharedIntance = (PartyEditPartyPanelCore.SharedInstanceExOverride)arg;
		}

		// Token: 0x04003777 RID: 14199
		protected PartyEditPartyPanelCore.SharedInstanceExOverride m_SharedIntance;

		// Token: 0x0200092D RID: 2349
		[Serializable]
		public class SharedInstanceExOverride : PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase
		{
			// Token: 0x060030A4 RID: 12452 RVA: 0x000FDB85 File Offset: 0x000FBF85
			public SharedInstanceExOverride()
			{
			}

			// Token: 0x060030A5 RID: 12453 RVA: 0x000FDB8D File Offset: 0x000FBF8D
			public SharedInstanceExOverride(PartyEditPartyPanelCore.SharedInstanceExOverride argument) : base(argument)
			{
				this.CloneNewMemberOnly(argument);
			}

			// Token: 0x060030A6 RID: 12454 RVA: 0x000FDB9E File Offset: 0x000FBF9E
			public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase Clone(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				base.Clone(argument);
				return this.CloneNewMemberOnly((PartyEditPartyPanelCore.SharedInstanceExOverride)argument);
			}

			// Token: 0x060030A7 RID: 12455 RVA: 0x000FDBB4 File Offset: 0x000FBFB4
			private PartyEditPartyPanelCore.SharedInstanceExOverride CloneNewMemberOnly(PartyEditPartyPanelCore.SharedInstanceExOverride argument)
			{
				this.m_FriendMemberTitle = argument.m_FriendMemberTitle;
				return this;
			}

			// Token: 0x04003778 RID: 14200
			[SerializeField]
			public GameObject m_FriendMemberTitle;
		}
	}
}
