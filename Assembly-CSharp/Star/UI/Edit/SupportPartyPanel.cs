﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200094E RID: 2382
	public class SupportPartyPanel : PartyEditPartyPanelBase
	{
		// Token: 0x0600317F RID: 12671 RVA: 0x000FF593 File Offset: 0x000FD993
		public override PartyEditPartyPanelBaseCore CreateCore()
		{
			return new PartyEditSupportPartyPanelCore();
		}

		// Token: 0x06003180 RID: 12672 RVA: 0x000FF59C File Offset: 0x000FD99C
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CreateArgument()
		{
			PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase serializedConstructInstances = this.m_SerializedConstructInstances;
			if (this.m_CharaPanelPrefab != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelPrefab = this.m_CharaPanelPrefab;
			}
			if (this.m_CharaPanelParent != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelParent = this.m_CharaPanelParent;
			}
			if (this.m_PartyScroll != null)
			{
				serializedConstructInstances.m_PartyScroll = this.m_PartyScroll;
			}
			return new PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase(serializedConstructInstances)
			{
				parent = this
			};
		}
	}
}
