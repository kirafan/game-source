﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000903 RID: 2307
	public class UpgradeCharacterInformationPanel : CharacterEditSceneCharacterInformationPanel
	{
		// Token: 0x06002FD9 RID: 12249 RVA: 0x000F9687 File Offset: 0x000F7A87
		public override void Setup()
		{
			base.Setup();
			this.m_IsSetExp = false;
		}

		// Token: 0x06002FDA RID: 12250 RVA: 0x000F9696 File Offset: 0x000F7A96
		protected override void ApplyResource(CharacterDataController cdc, CharacterDataController after)
		{
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
		}

		// Token: 0x06002FDB RID: 12251 RVA: 0x000F96A0 File Offset: 0x000F7AA0
		protected override void ApplyParameter(CharacterDataController cdc, CharacterDataController after)
		{
			CharacterDataController characterDataController = after;
			if (characterDataController == null)
			{
				characterDataController = cdc;
			}
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLimitBreakIconValue(characterDataController.GetCharaLimitBreak());
			if (!this.m_IsSetExp)
			{
				base.SetExpData(cdc, true, true);
				this.m_IsSetExp = true;
			}
			base.SetSimulateExpGauge(characterDataController.GetCharaNowExp(), characterDataController.GetCharaNowMaxExp(), characterDataController.GetCharaLv() - cdc.GetCharaLv());
			base.ApplyState(cdc, characterDataController);
		}

		// Token: 0x06002FDC RID: 12252 RVA: 0x000F9724 File Offset: 0x000F7B24
		protected override void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = false;
			this.m_IsDirtyParameter = false;
		}

		// Token: 0x040036D8 RID: 14040
		[SerializeField]
		[Tooltip("アイテム使用数情報.")]
		private SelectItemPanel m_InfoSelectItem;

		// Token: 0x040036D9 RID: 14041
		private bool m_IsSetExp;
	}
}
