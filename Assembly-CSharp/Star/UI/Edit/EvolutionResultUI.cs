﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000910 RID: 2320
	public class EvolutionResultUI : CharacterEditResultSceneUIBase<EditMain.EvolutionResultData>
	{
		// Token: 0x06003033 RID: 12339 RVA: 0x000FAA9A File Offset: 0x000F8E9A
		public override void Setup(EditMain.EvolutionResultData result)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			base.Setup(result);
		}

		// Token: 0x06003034 RID: 12340 RVA: 0x000FAACD File Offset: 0x000F8ECD
		protected override CharacterDataController ConvertResultDataToBeforeCharacterData(EditMain.EvolutionResultData result)
		{
			return new CharacterDataWrapperController().Setup(new EvolutionResultBeforeAfterPairBeforePartCharacterWrapper(result));
		}

		// Token: 0x06003035 RID: 12341 RVA: 0x000FAADF File Offset: 0x000F8EDF
		protected override CharacterDataController ConvertResultDataToAfterCharacterData(EditMain.EvolutionResultData result)
		{
			return new CharacterDataWrapperController().Setup(new EvolutionResultBeforeAfterPairAfterPartCharacterWrapper(result));
		}

		// Token: 0x06003036 RID: 12342 RVA: 0x000FAAF1 File Offset: 0x000F8EF1
		protected override eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_006;
		}
	}
}
