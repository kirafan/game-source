﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200094C RID: 2380
	public class SupportPartyEditCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x06003179 RID: 12665 RVA: 0x000FF4C6 File Offset: 0x000FD8C6
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetTitleImage(cdc.GetCharaId());
			base.SetStateWeaponIcon(cdc);
			base.SetEnableRenderCharacterView(true);
			base.RequestLoadCharacterViewCharacterBustImageOnly(cdc, true);
			base.SetCharacterIconLimitBreak(cdc.GetCharaLimitBreak());
		}

		// Token: 0x0600317A RID: 12666 RVA: 0x000FF4F8 File Offset: 0x000FD8F8
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetCharaInfoTitle(cdc.GetCharaId());
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetExpData(cdc, true, true);
			base.ApplyState(cdc);
			base.SetCost(cdc.GetCost());
			base.SetCharaInfoTitleCost(cdc);
		}
	}
}
