﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000913 RID: 2323
	public class UpgradeResultUI : CharacterEditResultSceneUIBase<EditMain.UpgradeResultData>
	{
		// Token: 0x06003040 RID: 12352 RVA: 0x000FABE2 File Offset: 0x000F8FE2
		public override void Setup(EditMain.UpgradeResultData result)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_SuccessLevel = result.m_SuccessType;
			base.Setup(result);
		}

		// Token: 0x06003041 RID: 12353 RVA: 0x000FAC21 File Offset: 0x000F9021
		protected override CharacterDataController ConvertResultDataToBeforeCharacterData(EditMain.UpgradeResultData result)
		{
			return new CharacterDataWrapperController().Setup(new UpgradeResultBeforeAfterPairBeforePartCharacterWrapper(result));
		}

		// Token: 0x06003042 RID: 12354 RVA: 0x000FAC33 File Offset: 0x000F9033
		protected override CharacterDataController ConvertResultDataToAfterCharacterData(EditMain.UpgradeResultData result)
		{
			return new CharacterDataWrapperController().Setup(new UpgradeResultBeforeAfterPairAfterPartCharacterWrapper(result));
		}

		// Token: 0x06003043 RID: 12355 RVA: 0x000FAC45 File Offset: 0x000F9045
		protected override eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_006;
		}
	}
}
