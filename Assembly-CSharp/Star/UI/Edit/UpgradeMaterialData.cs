﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000905 RID: 2309
	public class UpgradeMaterialData : ScrollItemData
	{
		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06002FE2 RID: 12258 RVA: 0x000F97BB File Offset: 0x000F7BBB
		// (set) Token: 0x06002FE3 RID: 12259 RVA: 0x000F97C3 File Offset: 0x000F7BC3
		public int ItemID
		{
			get
			{
				return this.m_ItemID;
			}
			set
			{
				this.m_ItemID = value;
			}
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06002FE4 RID: 12260 RVA: 0x000F97CC File Offset: 0x000F7BCC
		// (set) Token: 0x06002FE5 RID: 12261 RVA: 0x000F97D4 File Offset: 0x000F7BD4
		public string ItemName
		{
			get
			{
				return this.m_ItemName;
			}
			set
			{
				this.m_ItemName = value;
			}
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06002FE6 RID: 12262 RVA: 0x000F97DD File Offset: 0x000F7BDD
		// (set) Token: 0x06002FE7 RID: 12263 RVA: 0x000F97E5 File Offset: 0x000F7BE5
		public int HaveNum
		{
			get
			{
				return this.m_HaveNum;
			}
			set
			{
				this.m_HaveNum = value;
			}
		}

		// Token: 0x1700030B RID: 779
		// (get) Token: 0x06002FE8 RID: 12264 RVA: 0x000F97EE File Offset: 0x000F7BEE
		// (set) Token: 0x06002FE9 RID: 12265 RVA: 0x000F97F6 File Offset: 0x000F7BF6
		public int UseNum
		{
			get
			{
				return this.m_UseNum;
			}
			set
			{
				this.m_UseNum = value;
			}
		}

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x06002FEA RID: 12266 RVA: 0x000F97FF File Offset: 0x000F7BFF
		// (set) Token: 0x06002FEB RID: 12267 RVA: 0x000F9807 File Offset: 0x000F7C07
		public bool IsEnableAdd
		{
			get
			{
				return this.m_IsEnableAdd;
			}
			set
			{
				this.m_IsEnableAdd = value;
			}
		}

		// Token: 0x040036DD RID: 14045
		private int m_ItemID;

		// Token: 0x040036DE RID: 14046
		private string m_ItemName;

		// Token: 0x040036DF RID: 14047
		private int m_HaveNum;

		// Token: 0x040036E0 RID: 14048
		private int m_UseNum;

		// Token: 0x040036E1 RID: 14049
		private bool m_IsEnableAdd;
	}
}
