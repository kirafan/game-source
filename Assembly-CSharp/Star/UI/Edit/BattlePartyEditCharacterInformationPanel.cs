﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200091E RID: 2334
	public class BattlePartyEditCharacterInformationPanel : PartyEditPartyCharaPanelCharacterInformation
	{
		// Token: 0x0600306A RID: 12394 RVA: 0x000FB940 File Offset: 0x000F9D40
		protected override void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetTitleImage(cdc.GetCharaId());
			base.SetStateWeaponIcon(cdc);
			base.SetEnableRenderCharacterView(true);
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
			base.SetCharacterIconLimitBreak(cdc.GetCharaLimitBreak());
		}

		// Token: 0x0600306B RID: 12395 RVA: 0x000FB970 File Offset: 0x000F9D70
		protected override void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetCharaInfoTitle(cdc.GetCharaId());
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetExpData(cdc, true, true);
			base.ApplyState(cdc);
			base.SetCost(cdc.GetCost());
			base.SetCharaInfoTitleCost(cdc);
		}
	}
}
