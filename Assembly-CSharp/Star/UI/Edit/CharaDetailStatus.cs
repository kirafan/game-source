﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008E1 RID: 2273
	public class CharaDetailStatus : MonoBehaviour
	{
		// Token: 0x06002F16 RID: 12054 RVA: 0x000F6580 File Offset: 0x000F4980
		public void Apply(long charaMngID)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			if (userCharaData == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			UserNamedData userNamedData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(userCharaData.Param.NamedType);
			if (userNamedData == null)
			{
				Debug.LogError("Invalid NamedType");
				return;
			}
			UserWeaponData userWeaponData = null;
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(userCharaData, userNamedData, userWeaponData);
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.CharaCost));
			stringBuilder.Append(":");
			stringBuilder.Append(userCharaData.Param.Cost.ToString());
			this.m_Cost.text = stringBuilder.ToString();
			this.m_ExpGauge.Setup();
			this.m_ExpGauge.SetExpData(userCharaData.Param.Lv, EditUtility.GetCharaNowExp(userCharaData.Param.Lv, userCharaData.Param.Exp), userCharaData.Param.MaxLv, EditUtility.GetCharaNextMaxExp(userCharaData.Param.Lv));
			this.m_LBIcon.SetValue(userCharaData.Param.LimitBreak);
			int[] values = new int[]
			{
				outputCharaParam.Hp,
				outputCharaParam.Atk,
				outputCharaParam.Mgc,
				outputCharaParam.Def,
				outputCharaParam.MDef,
				outputCharaParam.Spd
			};
			this.m_Status.Apply(values, null);
			this.m_FriendShipLv.text = userNamedData.FriendShip.ToString();
			this.m_UniqueSkill.Apply(SkillDetail.eSkillCategory.PL, userCharaData.Param.UniqueSkillLearnData.SkillID, userCharaData.Param.UniqueSkillLearnData.SkillLv);
			for (int i = 0; i < this.m_SkillDetails.Length; i++)
			{
				this.m_SkillDetails[i].Apply(SkillDetail.eSkillCategory.PL, userCharaData.Param.ClassSkillLearnDatas[i].SkillID, userCharaData.Param.ClassSkillLearnDatas[i].SkillLv);
			}
			if (userWeaponData != null && !EditUtility.CheckDefaultWeapon(userWeaponData.MngID))
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(dbMng.WeaponListDB.GetParam(userWeaponData.Param.WeaponID).m_WeaponName);
				stringBuilder.Append(" ");
				stringBuilder.Append(dbMng.GetTextCommon(eText_CommonDB.Lv));
				stringBuilder.Append(userWeaponData.Param.Lv);
				this.m_WeaponName.text = stringBuilder.ToString();
				this.m_WeaponIcon.Apply(userWeaponData.Param.WeaponID);
				this.m_WeaponSkill.Apply(SkillDetail.eSkillCategory.WPN, userWeaponData.Param.SkillLearnData.SkillID, 0);
			}
			else
			{
				this.m_WeaponName.text = "装備なし";
				this.m_WeaponIcon.Apply(-1);
				this.m_WeaponSkill.Apply(SkillDetail.eSkillCategory.WPN, -1, 0);
			}
		}

		// Token: 0x04003632 RID: 13874
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x04003633 RID: 13875
		[SerializeField]
		private LimitBreakIcon m_LBIcon;

		// Token: 0x04003634 RID: 13876
		[SerializeField]
		private Text m_Cost;

		// Token: 0x04003635 RID: 13877
		[SerializeField]
		private StatusDisplay m_Status;

		// Token: 0x04003636 RID: 13878
		[SerializeField]
		private Text m_FriendShipLv;

		// Token: 0x04003637 RID: 13879
		[SerializeField]
		private SkillDetail m_UniqueSkill;

		// Token: 0x04003638 RID: 13880
		[SerializeField]
		private SkillDetail[] m_SkillDetails;

		// Token: 0x04003639 RID: 13881
		[SerializeField]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x0400363A RID: 13882
		[SerializeField]
		private Text m_WeaponName;

		// Token: 0x0400363B RID: 13883
		[SerializeField]
		private SkillDetail m_WeaponSkill;
	}
}
