﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020008EB RID: 2283
	public abstract class CharaEditSceneUIBase : MenuUIBase
	{
		// Token: 0x020008EC RID: 2284
		public enum eStep
		{
			// Token: 0x0400368A RID: 13962
			Hide,
			// Token: 0x0400368B RID: 13963
			InPrepare,
			// Token: 0x0400368C RID: 13964
			In,
			// Token: 0x0400368D RID: 13965
			Idle,
			// Token: 0x0400368E RID: 13966
			CharaOut,
			// Token: 0x0400368F RID: 13967
			Out,
			// Token: 0x04003690 RID: 13968
			End
		}

		// Token: 0x020008ED RID: 2285
		public enum eButton
		{
			// Token: 0x04003692 RID: 13970
			None = -1,
			// Token: 0x04003693 RID: 13971
			Decide
		}
	}
}
