﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007C6 RID: 1990
	public class SelectedCharaInfoDetailInfoList : MonoBehaviour
	{
		// Token: 0x06002948 RID: 10568 RVA: 0x000DB014 File Offset: 0x000D9414
		public void Setup()
		{
			List<SelectedCharaInfoDetailInfoGroupBase> list = new List<SelectedCharaInfoDetailInfoGroupBase>(this.m_SkillInfoGroup);
			for (int i = 0; i < this.m_SkillInfoGroups.Length; i++)
			{
				list.Add(this.m_SkillInfoGroups[i]);
			}
			this.m_SkillInfoGroups = list.ToArray();
			for (int j = 0; j < this.m_SkillInfoGroups.Length; j++)
			{
				this.m_SkillInfoGroups[j].Setup();
			}
		}

		// Token: 0x06002949 RID: 10569 RVA: 0x000DB088 File Offset: 0x000D9488
		public void Destroy()
		{
			if (this.m_SkillInfoGroups != null)
			{
				for (int i = 0; i < this.m_SkillInfoGroups.Length; i++)
				{
					this.m_SkillInfoGroups[i].Destroy();
					this.m_SkillInfoGroups[i] = null;
				}
				this.m_SkillInfoGroups = null;
			}
		}

		// Token: 0x0600294A RID: 10570 RVA: 0x000DB0D8 File Offset: 0x000D94D8
		public void ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			if (this.m_SkillInfoGroups != null)
			{
				for (int i = 0; i < this.m_SkillInfoGroups.Length; i++)
				{
					if (this.m_SkillInfoGroups[i].GetGroupType() == groupType)
					{
						this.m_SkillInfoGroups[i].ApplySkill(index, argument);
					}
				}
			}
			if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.UniquieSkill && this.m_UniqueSkillLevel != null)
			{
				SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument applyArgument = (SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument)argument;
				SelectedCharaInfoLevelInfo.EmphasisExpressionInfo emphasisExpressionInfo = new SelectedCharaInfoLevelInfo.EmphasisExpressionInfo
				{
					max = ((applyArgument.beforeLevel.m_Max >= applyArgument.afterLevel.m_Max) ? ((applyArgument.afterLevel.m_Max >= applyArgument.beforeLevel.m_Max) ? SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down) : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
				};
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(emphasisExpressionInfo.now) + applyArgument.beforeLevel.m_Max + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(emphasisExpressionInfo.now));
				stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(emphasisExpressionInfo.slash) + " → " + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(emphasisExpressionInfo.slash));
				stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(emphasisExpressionInfo.max) + applyArgument.afterLevel.m_Max + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(emphasisExpressionInfo.max));
				this.m_UniqueSkillLevel.text = stringBuilder.ToString();
			}
		}

		// Token: 0x0600294B RID: 10571 RVA: 0x000DB244 File Offset: 0x000D9644
		public void SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, bool active)
		{
			if (this.m_SkillInfoGroups != null)
			{
				for (int i = 0; i < this.m_SkillInfoGroups.Length; i++)
				{
					if (this.m_SkillInfoGroups[i].GetGroupType() == groupType)
					{
						this.m_SkillInfoGroups[i].gameObject.SetActive(active);
					}
				}
			}
		}

		// Token: 0x04002FD9 RID: 12249
		[SerializeField]
		[Tooltip("非推奨.旧型.m_SkillInfoGroupsに追加する事.")]
		private SelectedCharaInfoDetailInfoGroup[] m_SkillInfoGroup;

		// Token: 0x04002FDA RID: 12250
		[SerializeField]
		[Tooltip("タイトルと任意数の詳細を持つ子.")]
		private SelectedCharaInfoDetailInfoGroupBase[] m_SkillInfoGroups;

		// Token: 0x04002FDB RID: 12251
		[SerializeField]
		[Tooltip("とっておきのレベル表示.")]
		private Text m_UniqueSkillLevel;
	}
}
