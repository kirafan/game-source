﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000918 RID: 2328
	public class HaveOrbListItem : ScrollItemIcon
	{
		// Token: 0x06003053 RID: 12371 RVA: 0x000FAF2C File Offset: 0x000F932C
		protected override void ApplyData()
		{
			base.ApplyData();
			long orbMngID = ((HaveOrbListItemData)this.m_Data).m_OrbMngID;
			if (((HaveOrbListItemData)this.m_Data).m_IsEquip)
			{
				this.m_EquipIcon.SetActive(true);
			}
			else
			{
				this.m_EquipIcon.SetActive(false);
			}
			UserMasterOrbData userMasterOrbData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserMasterOrbData(orbMngID);
			this.m_OrbIcon.Apply(userMasterOrbData.OrbID);
			this.m_LvText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.Lv) + userMasterOrbData.Lv;
			if (this.m_OrbInformation == null)
			{
				this.m_OrbInformation = base.gameObject.GetComponent<MasterOrbInformation>();
			}
			if (this.m_OrbInformation != null)
			{
				this.m_OrbInformation.ApplyCurrentEquip(orbMngID);
			}
		}

		// Token: 0x06003054 RID: 12372 RVA: 0x000FB012 File Offset: 0x000F9412
		public override void Refresh()
		{
			base.Refresh();
			if (((HaveOrbListItemData)this.m_Data).m_IsEquip)
			{
				this.m_EquipIcon.SetActive(true);
			}
			else
			{
				this.m_EquipIcon.SetActive(false);
			}
		}

		// Token: 0x0400372C RID: 14124
		[SerializeField]
		private Text m_LvText;

		// Token: 0x0400372D RID: 14125
		[SerializeField]
		private OrbIcon m_OrbIcon;

		// Token: 0x0400372E RID: 14126
		[SerializeField]
		private GameObject m_EquipIcon;

		// Token: 0x0400372F RID: 14127
		[SerializeField]
		private MasterOrbInformation m_OrbInformation;
	}
}
