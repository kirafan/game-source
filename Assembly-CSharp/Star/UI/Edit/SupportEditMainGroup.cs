﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000947 RID: 2375
	public class SupportEditMainGroup : PartyEditMainGroupBase
	{
		// Token: 0x0600315F RID: 12639 RVA: 0x000FEE98 File Offset: 0x000FD298
		public override void Setup(PartyEditUIBase ui, PartyEditMainGroupBase.eMode mode = PartyEditMainGroupBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				return;
			}
			if (startPartyIndex < 0)
			{
				startPartyIndex = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedSupportPartyIndex;
			}
			if (parties == null)
			{
				parties = new EquipWeaponManagedSupportPartiesController().SetupEx();
			}
			base.Setup(ui, mode, startPartyIndex, parties);
			base.GetScroll().SetPageSignalActive(mode != PartyEditMainGroupBase.eMode.View);
			switch (mode)
			{
			case PartyEditMainGroupBase.eMode.Edit:
				base.SetOpenNameChangeButtonInteractive(true);
				break;
			case PartyEditMainGroupBase.eMode.Quest:
				base.SetOpenNameChangeButtonInteractive(true);
				break;
			case PartyEditMainGroupBase.eMode.QuestStart:
				base.SetOpenNameChangeButtonInteractive(false);
				break;
			case PartyEditMainGroupBase.eMode.View:
				base.SetOpenNameChangeButtonInteractive(false);
				break;
			}
			this.SetPartyScrollEditMode(mode);
			this.m_ResetButton.Interactable = !EditUtility.IsEmptySupportParty(base.GetScroll().SelectPartyIdx);
			if (mode == PartyEditMainGroupBase.eMode.View)
			{
				ui.OnClickButton += this.OnClickView;
				this.m_Message.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.SupportEditFriendSupportMessage);
			}
			else
			{
				this.m_Message.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.SupportEditMessage);
			}
			if (this.m_ResetButton != null)
			{
				this.m_ResetButton.gameObject.SetActive(mode != PartyEditMainGroupBase.eMode.View);
			}
		}

		// Token: 0x06003160 RID: 12640 RVA: 0x000FF001 File Offset: 0x000FD401
		public override void OnChangePage()
		{
			base.OnChangePage();
			this.m_ResetButton.Interactable = !EditUtility.IsEmptySupportParty(base.GetScroll().SelectPartyIdx);
		}

		// Token: 0x06003161 RID: 12641 RVA: 0x000FF027 File Offset: 0x000FD427
		private void OnClickView()
		{
			this.m_OnClickView.Call();
		}

		// Token: 0x040037C6 RID: 14278
		[SerializeField]
		private Text m_Message;

		// Token: 0x040037C7 RID: 14279
		[SerializeField]
		protected CustomButton m_ResetButton;

		// Token: 0x040037C8 RID: 14280
		public Action m_OnClickView;
	}
}
