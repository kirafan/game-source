﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200093B RID: 2363
	public class PartyEditCharaPanelBase : PartyCharaPanelBase
	{
		// Token: 0x060030E8 RID: 12520 RVA: 0x000FC05C File Offset: 0x000FA45C
		public override EnumerationCharaPanel CreateCore()
		{
			return new PartyEditCharaPanelBaseCore();
		}

		// Token: 0x060030E9 RID: 12521 RVA: 0x000FC063 File Offset: 0x000FA463
		public Image GetFrame()
		{
			return this.m_Frame;
		}

		// Token: 0x060030EA RID: 12522 RVA: 0x000FC06B File Offset: 0x000FA46B
		public Sprite[] GetRareFrameSprites()
		{
			return this.m_RareFrameSprites;
		}

		// Token: 0x04003791 RID: 14225
		[SerializeField]
		private Image m_Frame;

		// Token: 0x04003792 RID: 14226
		[SerializeField]
		private Sprite[] m_RareFrameSprites;
	}
}
