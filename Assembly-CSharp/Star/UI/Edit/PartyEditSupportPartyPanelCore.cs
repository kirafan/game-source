﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200094D RID: 2381
	public class PartyEditSupportPartyPanelCore : PartyEditPartyPanelBaseCore
	{
		// Token: 0x0600317C RID: 12668 RVA: 0x000FF57A File Offset: 0x000FD97A
		public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase GetSharedIntance()
		{
			return this.m_SharedIntance;
		}

		// Token: 0x0600317D RID: 12669 RVA: 0x000FF582 File Offset: 0x000FD982
		public override void SetSharedIntance(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase arg)
		{
			this.m_SharedIntance = arg;
		}

		// Token: 0x040037DC RID: 14300
		protected PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase m_SharedIntance;
	}
}
