﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000908 RID: 2312
	public class UpgradeMaterialList : ScrollViewBase
	{
		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06002FFE RID: 12286 RVA: 0x000F9D1C File Offset: 0x000F811C
		// (set) Token: 0x06002FFD RID: 12285 RVA: 0x000F9D13 File Offset: 0x000F8113
		public UpgradeMaterialPanel Owner
		{
			get
			{
				return this.m_Owner;
			}
			set
			{
				this.m_Owner = value;
			}
		}

		// Token: 0x06002FFF RID: 12287 RVA: 0x000F9D24 File Offset: 0x000F8124
		public void Setup(UpgradeMaterialList.eMode mode, UpgradeMaterialPanel owner, long charaMngID)
		{
			this.m_Mode = mode;
			this.m_Owner = owner;
			this.m_CharaMngID = charaMngID;
			this.Setup();
		}

		// Token: 0x06003000 RID: 12288 RVA: 0x000F9D44 File Offset: 0x000F8144
		public override void Setup()
		{
			base.Setup();
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_AllItemDataList = new List<ScrollItemData>();
			if (this.m_Mode == UpgradeMaterialList.eMode.Chara)
			{
				this.m_TitleType = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_CharaMngID).Param.CharaID).m_NamedType).m_TitleType;
				this.m_ItemDataLists = new List<ScrollItemData>[this.includeSpecifyLength];
				for (int i = 0; i < this.m_ItemDataLists.Length; i++)
				{
					this.m_ItemDataLists[i] = new List<ScrollItemData>();
					if (i < 6)
					{
						List<ItemListDB_Param> upgradeItemParams = EditUtility.GetUpgradeItemParams(UIUtility.ElementSortIdxToEnum(i));
						for (int j = 0; j < upgradeItemParams.Count; j++)
						{
							if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(upgradeItemParams[j].m_ID) > 0)
							{
								UpgradeMaterialData upgradeMaterialData = new UpgradeMaterialData();
								upgradeMaterialData.ItemID = upgradeItemParams[j].m_ID;
								upgradeMaterialData.ItemName = upgradeItemParams[j].m_Name;
								upgradeMaterialData.HaveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(upgradeMaterialData.ItemID);
								upgradeMaterialData.UseNum = 0;
								this.AddItem(upgradeMaterialData, i);
								this.m_AllItemDataList.Add(upgradeMaterialData);
							}
						}
					}
				}
				ItemListDB itemListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB;
				for (int k = 0; k < itemListDB.m_Params.Length; k++)
				{
					ItemListDB_Param itemListDB_Param = itemListDB.m_Params[k];
					if (itemListDB_Param.m_Type == 0 && itemListDB_Param.m_TypeArgs[3] == this.m_TitleType)
					{
						if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(itemListDB_Param.m_ID) > 0)
						{
							UpgradeMaterialData upgradeMaterialData2 = new UpgradeMaterialData();
							upgradeMaterialData2.ItemID = itemListDB_Param.m_ID;
							upgradeMaterialData2.ItemName = itemListDB_Param.m_Name;
							upgradeMaterialData2.HaveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(upgradeMaterialData2.ItemID);
							upgradeMaterialData2.UseNum = 0;
							this.AddItem(upgradeMaterialData2, this.includeSpecifyLength - 1);
							this.m_AllItemDataList.Add(upgradeMaterialData2);
						}
					}
				}
				this.ChangePage(0, true);
			}
			else
			{
				this.m_ItemDataLists = new List<ScrollItemData>[1];
				this.m_ItemDataLists[0] = new List<ScrollItemData>();
				List<ItemListDB_Param> weaponUpgradeItemParams = EditUtility.GetWeaponUpgradeItemParams();
				for (int l = 0; l < weaponUpgradeItemParams.Count; l++)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(weaponUpgradeItemParams[l].m_ID) > 0)
					{
						UpgradeMaterialData upgradeMaterialData3 = new UpgradeMaterialData();
						upgradeMaterialData3.ItemID = weaponUpgradeItemParams[l].m_ID;
						upgradeMaterialData3.ItemName = weaponUpgradeItemParams[l].m_Name;
						upgradeMaterialData3.HaveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(upgradeMaterialData3.ItemID);
						upgradeMaterialData3.UseNum = 0;
						this.AddItem(upgradeMaterialData3, 0);
						this.m_AllItemDataList.Add(upgradeMaterialData3);
					}
				}
				this.ChangePage(0, true);
			}
		}

		// Token: 0x06003001 RID: 12289 RVA: 0x000FA0D1 File Offset: 0x000F84D1
		public void AddItem(ScrollItemData item, int page)
		{
			this.m_ItemDataLists[page].Add(item);
			this.m_IsDirty = true;
		}

		// Token: 0x06003002 RID: 12290 RVA: 0x000FA0E8 File Offset: 0x000F84E8
		public void ChangePage(int page, bool force = false)
		{
			if (!force && this.m_SelectPage == page)
			{
				return;
			}
			this.m_SelectPage = page;
			if (this.m_Mode == UpgradeMaterialList.eMode.Chara)
			{
				if (page > -1 && page < 6)
				{
					this.m_ItemDataList = this.m_ItemDataLists[page];
				}
				else if (this.m_Mode == UpgradeMaterialList.eMode.Chara)
				{
					if (page == this.includeSpecifyLength - 1)
					{
						this.m_ItemDataList = this.m_ItemDataLists[page];
					}
				}
				else
				{
					this.m_ItemDataList = this.m_AllItemDataList;
				}
			}
			else
			{
				this.m_ItemDataList = this.m_AllItemDataList;
			}
			base.SetScrollValue(0f);
			base.Refresh();
			this.m_IsDirty = true;
		}

		// Token: 0x06003003 RID: 12291 RVA: 0x000FA19C File Offset: 0x000F859C
		public void ResetUseNum()
		{
			for (int i = 0; i < this.m_AllItemDataList.Count; i++)
			{
				((UpgradeMaterialData)this.m_AllItemDataList[i]).HaveNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(((UpgradeMaterialData)this.m_AllItemDataList[i]).ItemID);
				((UpgradeMaterialData)this.m_AllItemDataList[i]).UseNum = 0;
			}
			base.ForceApply();
		}

		// Token: 0x040036F3 RID: 14067
		[SerializeField]
		private UpgradeMaterialPanel m_Owner;

		// Token: 0x040036F4 RID: 14068
		private List<ScrollItemData>[] m_ItemDataLists;

		// Token: 0x040036F5 RID: 14069
		private List<ScrollItemData> m_AllItemDataList;

		// Token: 0x040036F6 RID: 14070
		private int m_SelectPage;

		// Token: 0x040036F7 RID: 14071
		private UpgradeMaterialList.eMode m_Mode;

		// Token: 0x040036F8 RID: 14072
		private int m_TitleType = -1;

		// Token: 0x040036F9 RID: 14073
		private long m_CharaMngID = -1L;

		// Token: 0x040036FA RID: 14074
		private int includeSpecifyLength = 7;

		// Token: 0x02000909 RID: 2313
		public enum eMode
		{
			// Token: 0x040036FC RID: 14076
			Chara,
			// Token: 0x040036FD RID: 14077
			Weapon
		}
	}
}
