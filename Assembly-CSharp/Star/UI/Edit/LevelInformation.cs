﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020007BF RID: 1983
	public class LevelInformation
	{
		// Token: 0x0600292E RID: 10542 RVA: 0x000DAD6F File Offset: 0x000D916F
		public LevelInformation(int now, int max)
		{
			this.m_Now = now;
			this.m_Max = max;
		}

		// Token: 0x0600292F RID: 10543 RVA: 0x000DAD85 File Offset: 0x000D9185
		public LevelInformation()
		{
			this.m_Now = -1;
			this.m_Max = -1;
		}

		// Token: 0x04002FCA RID: 12234
		public int m_Now;

		// Token: 0x04002FCB RID: 12235
		public int m_Max;
	}
}
