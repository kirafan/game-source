﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000924 RID: 2340
	public class PartyEditControlPanelElementDataField : PartyEditControlPanelElement
	{
		// Token: 0x0600307A RID: 12410 RVA: 0x000FC08C File Offset: 0x000FA48C
		public override void SetMode(int mode)
		{
			base.SetMode(mode);
			if (mode == 0)
			{
				this.m_EnemyTypeField.gameObject.SetActive(false);
			}
			else if (mode == 1)
			{
				this.m_EnemyTypeField.gameObject.SetActive(true);
				this.m_EnemyType.ApplyNowSelectedQuest();
			}
			this.m_RepaintFrame = 150;
		}

		// Token: 0x0400375D RID: 14173
		[SerializeField]
		private LayoutGroup m_LayoutGroup;

		// Token: 0x0400375E RID: 14174
		[SerializeField]
		private GameObject m_EnemyTypeField;

		// Token: 0x0400375F RID: 14175
		[SerializeField]
		private ElementIconList m_EnemyType;
	}
}
