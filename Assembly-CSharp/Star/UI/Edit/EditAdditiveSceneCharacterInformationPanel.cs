﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000914 RID: 2324
	public class EditAdditiveSceneCharacterInformationPanel : CharacterInformationPanel
	{
		// Token: 0x06003045 RID: 12357 RVA: 0x000FAC50 File Offset: 0x000F9050
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
			if (this.m_IsDirtyResource)
			{
				this.ApplyResource(cdc);
			}
			if (this.m_IsDirtyParameter)
			{
				this.ApplyParameter(cdc);
			}
			this.ResetDirtyFlag();
		}

		// Token: 0x06003046 RID: 12358 RVA: 0x000FAC7C File Offset: 0x000F907C
		protected virtual void ApplyResource(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06003047 RID: 12359 RVA: 0x000FAC7E File Offset: 0x000F907E
		protected virtual void ApplyParameter(EquipWeaponCharacterDataController cdc)
		{
		}
	}
}
