﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200091F RID: 2335
	public class BattlePartyEditQuestStartControlPanel : PartyEditControlPanel
	{
		// Token: 0x0600306D RID: 12397 RVA: 0x000FBA64 File Offset: 0x000F9E64
		public override void SetMode(int mode)
		{
			base.SetMode(mode);
			if (mode == -1)
			{
				this.m_QuestStartButton.Interactable = EditUtility.IsAvailableParty(this.m_MainGroup.SelectPartyIndex);
				QuestManager.SelectQuest selectQuest = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetSelectQuest();
				QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(selectQuest.m_QuestID);
				if (questData.m_ClearRank != QuestDefine.eClearRank.None)
				{
					this.m_ADVSkipButton.Interactable = true;
					this.m_ADVSkipButton.IsDecided = LocalSaveData.Inst.ADVSkipOnBattle;
					this.m_ADVSkipToggleButton.SetInteractable(true);
					this.m_ADVSkipToggleButton.SetDecided(LocalSaveData.Inst.ADVSkipOnBattle);
				}
				else
				{
					this.m_ADVSkipButton.Interactable = false;
					this.m_ADVSkipToggleButton.SetInteractable(false);
				}
				int num = -1;
				int use = 0;
				if (questData.IsEventQuest())
				{
					QuestManager.EventData eventData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEventData(questData.m_EventID);
					if (eventData != null)
					{
						num = eventData.m_ExKeyItemID;
						use = eventData.m_ExKeyItemNum;
					}
				}
				this.m_UseItemID = num;
				if (num != -1)
				{
					this.m_CostItemObj.SetActive(true);
					this.m_CostStaminaObj.SetActive(false);
					this.m_ItemIcon.Apply(num);
					this.m_Use = use;
					this.m_Current = (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(num);
				}
				else
				{
					this.m_CostItemObj.SetActive(false);
					this.m_CostStaminaObj.SetActive(true);
					this.m_Use = questData.m_Param.m_Stamina;
					this.m_Current = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValue();
				}
				this.UpdateCostText();
			}
		}

		// Token: 0x0600306E RID: 12398 RVA: 0x000FBC0C File Offset: 0x000FA00C
		private void UpdateCostText()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if ((long)this.m_Use > this.m_Current)
			{
				stringBuilder.Append("<color=red>");
			}
			stringBuilder.Append("x");
			stringBuilder.Append(this.m_Use.ToString());
			if ((long)this.m_Use > this.m_Current)
			{
				stringBuilder.Append("</color>");
			}
			this.m_StaminaValue.text = stringBuilder.ToString();
		}

		// Token: 0x0600306F RID: 12399 RVA: 0x000FBC90 File Offset: 0x000FA090
		protected override void Update()
		{
			base.Update();
			if (this.m_UseItemID == -1 && this.m_Use > 0)
			{
				this.m_Current = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Stamina.GetValue();
				this.UpdateCostText();
			}
		}

		// Token: 0x06003070 RID: 12400 RVA: 0x000FBCE0 File Offset: 0x000FA0E0
		public void OnClickADVSkipButtonCallBack()
		{
			this.OnClickADVSkipToggleButtonCallbackProcess(!this.m_ADVSkipButton.IsDecided);
		}

		// Token: 0x06003071 RID: 12401 RVA: 0x000FBCF6 File Offset: 0x000FA0F6
		public void OnClickADVSkipToggleButtonCallback()
		{
			this.OnClickADVSkipToggleButtonCallbackProcess(this.m_ADVSkipToggleButton.IsOn());
		}

		// Token: 0x06003072 RID: 12402 RVA: 0x000FBD09 File Offset: 0x000FA109
		public void OnClickADVSkipToggleButtonCallbackProcess(bool flag)
		{
			this.m_ADVSkipButton.IsDecided = flag;
			this.m_ADVSkipToggleButton.SetDecided(flag);
			LocalSaveData.Inst.ADVSkipOnBattle = this.m_ADVSkipButton.IsDecided;
			LocalSaveData.Inst.Save();
		}

		// Token: 0x0400374C RID: 14156
		[SerializeField]
		private PartyEditMainGroup m_MainGroup;

		// Token: 0x0400374D RID: 14157
		[SerializeField]
		private ToggleSwitch m_ADVSkipToggleButton;

		// Token: 0x0400374E RID: 14158
		[SerializeField]
		private CustomButton m_ADVSkipButton;

		// Token: 0x0400374F RID: 14159
		[SerializeField]
		private GameObject m_CostStaminaObj;

		// Token: 0x04003750 RID: 14160
		[SerializeField]
		private Text m_StaminaValue;

		// Token: 0x04003751 RID: 14161
		[SerializeField]
		private GameObject m_CostItemObj;

		// Token: 0x04003752 RID: 14162
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x04003753 RID: 14163
		[SerializeField]
		private CustomButton m_QuestStartButton;

		// Token: 0x04003754 RID: 14164
		private int m_Use;

		// Token: 0x04003755 RID: 14165
		private long m_Current;

		// Token: 0x04003756 RID: 14166
		private int m_UseItemID = -1;
	}
}
