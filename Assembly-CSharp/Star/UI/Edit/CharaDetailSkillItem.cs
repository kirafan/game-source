﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008DE RID: 2270
	public class CharaDetailSkillItem : ScrollItemIcon
	{
		// Token: 0x06002F10 RID: 12048 RVA: 0x000F6444 File Offset: 0x000F4844
		protected override void ApplyData()
		{
			StringBuilder stringBuilder = new StringBuilder();
			CharaDetailSkillItemData charaDetailSkillItemData = (CharaDetailSkillItemData)this.m_Data;
			int skillID = ((CharaDetailSkillItemData)this.m_Data).m_SkillID;
			SkillListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(skillID);
			this.m_SkillNameTextObj.text = param.m_SkillName;
			stringBuilder.Length = 0;
			stringBuilder.Append("Lv.");
			stringBuilder.Append(charaDetailSkillItemData.m_Level);
			stringBuilder.Append("/");
			stringBuilder.Append(charaDetailSkillItemData.m_MaxLevel);
			this.m_SkillLevelTextObj.text = stringBuilder.ToString();
			this.m_DetailTextObj.text = param.m_SkillDetail;
		}

		// Token: 0x04003627 RID: 13863
		[SerializeField]
		private Image m_Image;

		// Token: 0x04003628 RID: 13864
		[SerializeField]
		private Text m_SkillNameTextObj;

		// Token: 0x04003629 RID: 13865
		[SerializeField]
		private Text m_SkillLevelTextObj;

		// Token: 0x0400362A RID: 13866
		[SerializeField]
		private Text m_DetailTextObj;
	}
}
