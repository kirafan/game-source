﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200093F RID: 2367
	public abstract class PartyEditPartyPanelBaseCore : EnumerationPanelCoreGen<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
	{
		// Token: 0x06003114 RID: 12564 RVA: 0x000FDA8F File Offset: 0x000FBE8F
		public void OnClickCharaPanelChara(int slotIndex)
		{
			base.SetNowSelectedCharaSlotIndex(slotIndex);
			this.m_PartyData.m_OnClickCharaPanel.Call(PartyCharaPanelBase.eButton.Chara);
		}

		// Token: 0x06003115 RID: 12565 RVA: 0x000FDAA9 File Offset: 0x000FBEA9
		public void OnClickCharaPanelWeapon(int slotIndex)
		{
			base.SetNowSelectedCharaSlotIndex(slotIndex);
			this.m_PartyData.m_OnClickCharaPanel.Call(PartyCharaPanelBase.eButton.Weapon);
		}

		// Token: 0x06003116 RID: 12566 RVA: 0x000FDAC3 File Offset: 0x000FBEC3
		public void OnHoldCharaPanelChara(int slotIndex)
		{
			base.SetNowSelectedCharaSlotIndex(slotIndex);
			this.m_PartyData.m_OnHoldCharaPanel.Call(PartyCharaPanelBase.eButton.Chara);
		}

		// Token: 0x02000940 RID: 2368
		[Serializable]
		public class SharedInstanceExOverrideBase : EnumerationPanelCoreGenBase<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>.SharedInstanceEx<PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
		{
			// Token: 0x06003117 RID: 12567 RVA: 0x000FDADD File Offset: 0x000FBEDD
			public SharedInstanceExOverrideBase()
			{
			}

			// Token: 0x06003118 RID: 12568 RVA: 0x000FDAE5 File Offset: 0x000FBEE5
			public SharedInstanceExOverrideBase(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument) : base(argument)
			{
				this.CloneNewMemberOnly(argument);
			}

			// Token: 0x06003119 RID: 12569 RVA: 0x000FDAF6 File Offset: 0x000FBEF6
			public override PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase Clone(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				return base.Clone(argument).CloneNewMemberOnly(argument);
			}

			// Token: 0x0600311A RID: 12570 RVA: 0x000FDB05 File Offset: 0x000FBF05
			private PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase CloneNewMemberOnly(PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase argument)
			{
				this.m_PartyScroll = argument.m_PartyScroll;
				return this;
			}

			// Token: 0x040037A2 RID: 14242
			public PartyScroll m_PartyScroll;
		}
	}
}
