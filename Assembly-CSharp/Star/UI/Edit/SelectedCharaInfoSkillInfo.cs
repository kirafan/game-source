﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020007CB RID: 1995
	public class SelectedCharaInfoSkillInfo : SelectedCharaInfoSkillInfoBase
	{
		// Token: 0x06002954 RID: 10580 RVA: 0x000DB424 File Offset: 0x000D9824
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			SelectedCharaInfoSkillInfo.ApplyArgument applyArgument = (SelectedCharaInfoSkillInfo.ApplyArgument)argument;
			base.ApplySkillInfo(applyArgument.skillDBType, applyArgument.skillID, applyArgument.ownerElement, applyArgument.beforeLevel, applyArgument.remainExp);
		}

		// Token: 0x020007CC RID: 1996
		public class ApplyArgument : SelectedCharaInfoSkillInfoBase.SkillInfoApplyArgumentBase
		{
			// Token: 0x06002955 RID: 10581 RVA: 0x000DB45C File Offset: 0x000D985C
			public ApplyArgument(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, LevelInformation in_beforeLevel, long in_remainExp) : base(skillDBType, skillID, ownerElement, in_beforeLevel)
			{
				this.remainExp = in_remainExp;
			}

			// Token: 0x04002FE4 RID: 12260
			public long remainExp = -1L;
		}
	}
}
