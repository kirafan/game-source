﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200093E RID: 2366
	public class PartyEditPartyCharaPanelCharacterInformation : EditAdditiveSceneCharacterInformationPanel
	{
		// Token: 0x0600310D RID: 12557 RVA: 0x000FB804 File Offset: 0x000F9C04
		protected sealed override void ApplyResource(EquipWeaponCharacterDataController cdc)
		{
			bool flag = cdc != null && cdc.IsExistCharacter();
			if (flag)
			{
				this.ApplyResourceExist(cdc);
			}
			else
			{
				this.ApplyResourceNoExist(cdc);
			}
		}

		// Token: 0x0600310E RID: 12558 RVA: 0x000FB83D File Offset: 0x000F9C3D
		protected virtual void ApplyResourceExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetTitleImage(cdc.GetCharaId());
			base.SetStateWeaponIcon(cdc);
			base.SetEnableRenderCharacterView(true);
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
			base.SetCharacterIconLimitBreak(cdc.GetCharaLimitBreak());
		}

		// Token: 0x0600310F RID: 12559 RVA: 0x000FB86D File Offset: 0x000F9C6D
		protected virtual void ApplyResourceNoExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetEnableRenderCharacterView(false);
			base.SetStateWeaponIcon(-1);
		}

		// Token: 0x06003110 RID: 12560 RVA: 0x000FB880 File Offset: 0x000F9C80
		protected sealed override void ApplyParameter(EquipWeaponCharacterDataController cdc)
		{
			bool flag = cdc != null && cdc.IsExistCharacter();
			if (flag)
			{
				this.ApplyParameterExist(cdc);
			}
			else
			{
				this.ApplyParameterNoExist(cdc);
			}
		}

		// Token: 0x06003111 RID: 12561 RVA: 0x000FB8BC File Offset: 0x000F9CBC
		protected virtual void ApplyParameterExist(EquipWeaponCharacterDataController cdc)
		{
			base.SetCharaInfoTitle(cdc.GetCharaId());
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetExpData(cdc, true, true);
			base.ApplyState(cdc);
			base.SetCost(cdc.GetCost());
			base.SetCharaInfoTitleCost(cdc);
		}

		// Token: 0x06003112 RID: 12562 RVA: 0x000FB936 File Offset: 0x000F9D36
		protected virtual void ApplyParameterNoExist(EquipWeaponCharacterDataController cdc)
		{
		}
	}
}
