﻿using System;
using Star.UI.WeaponList;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000946 RID: 2374
	public abstract class PartyEditUIBaseExGen<MainGroupType> : PartyEditUIBase where MainGroupType : PartyEditMainGroupBase
	{
		// Token: 0x06003137 RID: 12599 RVA: 0x000FCB51 File Offset: 0x000FAF51
		private void Start()
		{
		}

		// Token: 0x06003138 RID: 12600 RVA: 0x000FCB54 File Offset: 0x000FAF54
		public override void Setup(PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null)
		{
			this.m_MainGroupEx.m_OnClickChara += this.OnClickCharaCallBack;
			this.m_MainGroupEx.m_OnHoldChara += this.OnHoldCharaCallBack;
			this.m_MainGroupEx.m_OnClickWeapon += this.OnClickWeaponCallBack;
			this.m_MainGroupEx.Setup(this, base.ConvertPartyEditUIBaseeModeToPartyEditMainGroupeMode(mode), startPartyIndex, parties);
			this.m_Mode = mode;
			this.SetupPartyDetailPanel();
		}

		// Token: 0x06003139 RID: 12601 RVA: 0x000FCBE4 File Offset: 0x000FAFE4
		protected void SetupPartyDetailPanel()
		{
			if (this.GetPartyDetailPanel() != null)
			{
				this.GetPartyDetailPanel().DataIdx = -1;
				this.GetPartyDetailPanel().Setup();
				this.GetPartyDetailPanel().GameObject.SetActive(true);
				this.GetPartyDetailPanel().Scroll = null;
				this.GetPartyDetailPanel().SetEditMode(PartyEditUIBase.ConvertPartyEditUIBaseeModeToEnumerationPanelCoreBaseeMode(PartyEditUIBase.eMode.PartyDetail));
			}
		}

		// Token: 0x0600313A RID: 12602 RVA: 0x000FCC47 File Offset: 0x000FB047
		public PartyPanelData GetPartyData(int idx)
		{
			return this.m_MainGroupEx.GetPartyData(idx);
		}

		// Token: 0x0600313B RID: 12603 RVA: 0x000FCC5B File Offset: 0x000FB05B
		private void Update()
		{
			this.UpdateStep();
		}

		// Token: 0x0600313C RID: 12604 RVA: 0x000FCC64 File Offset: 0x000FB064
		private void UpdateStep()
		{
			switch (this.m_Step)
			{
			case PartyEditUIBase.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (this.GetMainGroup().IsPlayingInOut)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(PartyEditUIBase.eStep.Idle);
				}
				break;
			}
			case PartyEditUIBase.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.GetMainGroup().IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.m_MainGroupEx.Destroy();
					this.m_PartyDetailGroupEx.GetPanel().Destroy();
					this.ChangeStep(PartyEditUIBase.eStep.End);
				}
				break;
			}
			case PartyEditUIBase.eStep.CharaDetail:
				if (!SingletonMonoBehaviour<CharaDetailPlayer>.Inst.IsOpen())
				{
					this.m_MainGroupEx.RefreshScroll();
					this.ChangeStep(PartyEditUIBase.eStep.Idle);
				}
				break;
			case PartyEditUIBase.eStep.Weapon:
				if (!SingletonMonoBehaviour<WeaponListPlayer>.Inst.IsOpen())
				{
					this.m_MainGroupEx.Resetup();
					this.ChangeStep(PartyEditUIBase.eStep.Idle);
				}
				break;
			case PartyEditUIBase.eStep.End:
				this.UpdateStepEnd();
				break;
			}
		}

		// Token: 0x0600313D RID: 12605 RVA: 0x000FCDF1 File Offset: 0x000FB1F1
		protected virtual void UpdateStepEnd()
		{
		}

		// Token: 0x0600313E RID: 12606 RVA: 0x000FCDF4 File Offset: 0x000FB1F4
		protected void ChangeStep(PartyEditUIBase.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case PartyEditUIBase.eStep.Hide:
				this.SetEnableInput(false);
				break;
			case PartyEditUIBase.eStep.In:
				this.SetEnableInput(false);
				break;
			case PartyEditUIBase.eStep.Out:
				this.SetEnableInput(false);
				break;
			case PartyEditUIBase.eStep.Idle:
				this.SetEnableInput(true);
				this.GetMainGroup().SetEnableInput(true);
				break;
			case PartyEditUIBase.eStep.NameChange:
				this.GetPartyNameChangeGroup().Open();
				this.SetPartyNameChangeInputFieldText(this.m_MainGroupEx.GetSelectedPartyName());
				break;
			case PartyEditUIBase.eStep.NameChangeClose:
				this.GetPartyNameChangeGroup().Close();
				this.ChangeStep(PartyEditUIBase.eStep.Idle);
				break;
			case PartyEditUIBase.eStep.CharaDetail:
			{
				this.GetMainGroup().SetEnableInput(false);
				EquipWeaponPartyMemberController selectPartyChara;
				if (0L <= this.m_MainGroupEx.SelectCharaMngID)
				{
					this.OpenCharaDetailPlayer();
				}
				else if ((selectPartyChara = this.m_MainGroupEx.SelectPartyChara) != null)
				{
					eCharacterDataType characterDataType = selectPartyChara.GetCharacterDataType();
					if (characterDataType != eCharacterDataType.Empty && characterDataType != eCharacterDataType.Lock)
					{
						SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(selectPartyChara, 0);
					}
				}
				break;
			}
			case PartyEditUIBase.eStep.Weapon:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600313F RID: 12607 RVA: 0x000FCF38 File Offset: 0x000FB338
		public override void PlayIn()
		{
			this.ChangeStep(PartyEditUIBase.eStep.In);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.GetMainGroup().Open();
		}

		// Token: 0x06003140 RID: 12608 RVA: 0x000FCF84 File Offset: 0x000FB384
		public override void PlayOut()
		{
			this.ChangeStep(PartyEditUIBase.eStep.Out);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.GetMainGroup().Close();
		}

		// Token: 0x06003141 RID: 12609 RVA: 0x000FCFD0 File Offset: 0x000FB3D0
		public void Dissolution()
		{
			int selectPartyIndex = this.m_MainGroupEx.SelectPartyIndex;
			for (int i = 0; i < this.m_MainGroupEx.HowManyChildPanelAllChildren(); i++)
			{
				this.RemoveMember(selectPartyIndex, i);
			}
			this.m_MainGroupEx.ScrollRemoveAll();
			this.Setup(this.m_Mode, selectPartyIndex, null);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
		}

		// Token: 0x06003142 RID: 12610 RVA: 0x000FD048 File Offset: 0x000FB448
		protected virtual void RemoveMember(int partyIndex, int memberIndex)
		{
		}

		// Token: 0x06003143 RID: 12611 RVA: 0x000FD04A File Offset: 0x000FB44A
		public virtual void OpenCharaDetailPlayer()
		{
		}

		// Token: 0x06003144 RID: 12612 RVA: 0x000FD04C File Offset: 0x000FB44C
		public override bool IsMenuEnd()
		{
			return this.m_Step == PartyEditUIBase.eStep.End;
		}

		// Token: 0x06003145 RID: 12613 RVA: 0x000FD058 File Offset: 0x000FB458
		protected override PartyEditMainGroupBase GetMainGroupBase()
		{
			return this.m_MainGroupEx;
		}

		// Token: 0x06003146 RID: 12614 RVA: 0x000FD065 File Offset: 0x000FB465
		public UIGroup GetMainGroup()
		{
			if (this.m_MainGroupEx == null)
			{
				return null;
			}
			return this.m_MainGroupEx;
		}

		// Token: 0x06003147 RID: 12615 RVA: 0x000FD08A File Offset: 0x000FB48A
		public UIGroup GetPartyNameChangeGroup()
		{
			if (this.m_PartyNameChangeGroupEx == null)
			{
				return null;
			}
			return this.m_PartyNameChangeGroupEx;
		}

		// Token: 0x06003148 RID: 12616 RVA: 0x000FD0A5 File Offset: 0x000FB4A5
		public InputField GetPartyNameChangeInputField()
		{
			if (this.m_PartyNameChangeGroupEx == null)
			{
				return null;
			}
			return this.m_PartyNameChangeGroupEx.GetInputField();
		}

		// Token: 0x06003149 RID: 12617 RVA: 0x000FD0C5 File Offset: 0x000FB4C5
		public string GetPartyNameChangeInputFieldText()
		{
			if (this.m_PartyNameChangeGroupEx)
			{
				return this.m_PartyNameChangeGroupEx.GetInputFieldText();
			}
			return this.GetPartyNameChangeInputField().text;
		}

		// Token: 0x0600314A RID: 12618 RVA: 0x000FD0EE File Offset: 0x000FB4EE
		public void SetPartyNameChangeInputFieldText(string partyName)
		{
			if (this.m_PartyNameChangeGroupEx)
			{
				this.m_PartyNameChangeGroupEx.SetInputFieldText(partyName);
				return;
			}
			this.GetPartyNameChangeInputField().text = partyName;
		}

		// Token: 0x0600314B RID: 12619 RVA: 0x000FD119 File Offset: 0x000FB519
		public CustomButton GetPartyNameChangeDecideButton()
		{
			if (this.m_PartyNameChangeGroupEx == null)
			{
				return null;
			}
			return this.m_PartyNameChangeGroupEx.GetDecideButton();
		}

		// Token: 0x0600314C RID: 12620 RVA: 0x000FD139 File Offset: 0x000FB539
		public UIGroup GetPartyDetailGroup()
		{
			if (this.m_PartyDetailGroupEx == null)
			{
				return null;
			}
			return this.m_PartyDetailGroupEx;
		}

		// Token: 0x0600314D RID: 12621 RVA: 0x000FD154 File Offset: 0x000FB554
		public PartyDetailPanel GetPartyDetailPanel()
		{
			if (this.m_PartyDetailGroupEx == null)
			{
				return null;
			}
			return this.m_PartyDetailGroupEx.GetPanel();
		}

		// Token: 0x0600314E RID: 12622 RVA: 0x000FD174 File Offset: 0x000FB574
		public void CloseNameChangeWindow()
		{
			this.m_MainGroupEx.SetPartyName(this.GetGlobalParamPartyName(this.m_MainGroupEx.SelectPartyMngID));
			this.ChangeStep(PartyEditUIBase.eStep.NameChangeClose);
		}

		// Token: 0x0600314F RID: 12623 RVA: 0x000FD1A5 File Offset: 0x000FB5A5
		public string GetPartyName(int partyID)
		{
			return this.m_MainGroupEx.GetPartyName(partyID);
		}

		// Token: 0x06003150 RID: 12624 RVA: 0x000FD1B9 File Offset: 0x000FB5B9
		public virtual string GetGlobalParamPartyName(long partyMngId)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserBattlePartyData(this.m_MainGroupEx.SelectPartyMngID).PartyName;
		}

		// Token: 0x06003151 RID: 12625 RVA: 0x000FD1E0 File Offset: 0x000FB5E0
		public override void OnClickCharaCallBack()
		{
			this.m_SelectButton = PartyEditUIBase.eButton.Chara;
			base.ExecuteOnClickButton();
		}

		// Token: 0x06003152 RID: 12626 RVA: 0x000FD1EF File Offset: 0x000FB5EF
		public override void OnHoldCharaCallBack()
		{
			if (this.m_Mode != PartyEditUIBase.eMode.QuestStart)
			{
				this.ChangeStep(PartyEditUIBase.eStep.CharaDetail);
			}
		}

		// Token: 0x06003153 RID: 12627 RVA: 0x000FD204 File Offset: 0x000FB604
		public override void OnClickWeaponCallBack()
		{
			if (PartyEditUIBase.ModeToWeaponActive(this.m_Mode) && this.m_MainGroupEx.SelectCharaMngID != -1L)
			{
				EquipWeaponCharacterDataController selectPartyChara = this.m_MainGroupEx.SelectPartyChara;
				EquipWeaponManagedPartyMemberController equipWeaponManagedPartyMemberController = null;
				if (selectPartyChara.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedBattleParty || selectPartyChara.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedSupportParty)
				{
					equipWeaponManagedPartyMemberController = (EquipWeaponManagedPartyMemberController)selectPartyChara;
				}
				if (equipWeaponManagedPartyMemberController != null)
				{
					if (0 < EditUtility.GetUserWeaponDataRefineClassFromUserData(equipWeaponManagedPartyMemberController.GetClassType()).Count)
					{
						if (equipWeaponManagedPartyMemberController.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedBattleParty)
						{
							SingletonMonoBehaviour<WeaponListPlayer>.Inst.Open(EditUtility.GetBattlePartyUserBattlePartyData(equipWeaponManagedPartyMemberController.GetPartyIndex()), equipWeaponManagedPartyMemberController.GetPartySlotIndex());
						}
						else if (equipWeaponManagedPartyMemberController.GetCharacterDataControllerType() == eCharacterDataControllerType.ManagedSupportParty)
						{
							SingletonMonoBehaviour<WeaponListPlayer>.Inst.Open(EditUtility.GetSupportPartyUserSupportPartyData(equipWeaponManagedPartyMemberController.GetPartyIndex()), equipWeaponManagedPartyMemberController.GetPartySlotIndex());
						}
						SingletonMonoBehaviour<WeaponListPlayer>.Inst.OnClose += this.OnCloseWeaponList;
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PartyEditNotHaveClassWeaponTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PartyEditNotHaveClassWeaponMessage), null, null);
					}
				}
				else
				{
					Debug.LogError("不正なステートのため武器リストを開けません.");
				}
				this.ChangeStep(PartyEditUIBase.eStep.Weapon);
			}
		}

		// Token: 0x06003154 RID: 12628 RVA: 0x000FD346 File Offset: 0x000FB746
		private void OnCloseWeaponList()
		{
			this.m_MainGroupEx.RefreshScroll();
		}

		// Token: 0x06003155 RID: 12629 RVA: 0x000FD359 File Offset: 0x000FB759
		public void OnNameChangeButton()
		{
			this.ChangeStep(PartyEditUIBase.eStep.NameChange);
		}

		// Token: 0x06003156 RID: 12630 RVA: 0x000FD364 File Offset: 0x000FB764
		public void OnClickDecideNameChangeButton()
		{
			this.OnPartyNameInputFieldValueChanged();
			if (this.GetPartyNameChangeInputFieldText() != this.GetGlobalParamPartyName(this.m_MainGroupEx.SelectPartyMngID))
			{
				base.ExecuteOnDecideNameChange(this.GetPartyNameChangeInputFieldText());
			}
			else
			{
				this.ChangeStep(PartyEditUIBase.eStep.NameChangeClose);
			}
		}

		// Token: 0x06003157 RID: 12631 RVA: 0x000FD3B6 File Offset: 0x000FB7B6
		public void OnCancelNameChangeButton()
		{
			this.CloseNameChangeWindow();
		}

		// Token: 0x06003158 RID: 12632 RVA: 0x000FD3C0 File Offset: 0x000FB7C0
		public void OnClickResetButton()
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			DatabaseManager dbMng = inst.DbMng;
			inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, dbMng.GetTextCommon(eText_CommonDB.PartyEditCheckResetWindowTitle), dbMng.GetTextCommon(eText_CommonDB.PartyEditCheckResetWindowMessage), null, new Action<int>(this.OnClickCheckResetWindowButton));
		}

		// Token: 0x06003159 RID: 12633 RVA: 0x000FD409 File Offset: 0x000FB809
		public void OnClickCheckResetWindowButton(int index)
		{
			if (!Convert.ToBoolean(index))
			{
				this.Dissolution();
			}
		}

		// Token: 0x0600315A RID: 12634 RVA: 0x000FD41C File Offset: 0x000FB81C
		public void OnClickOpenPartyDetailButton()
		{
			if (this.GetPartyDetailGroup() != null)
			{
				PartyDetailPanel partyDetailPanel = this.GetPartyDetailPanel();
				if (partyDetailPanel != null)
				{
					partyDetailPanel.Apply(this.GetPartyData(this.m_MainGroupEx.SelectPartyIndex));
				}
				this.GetPartyDetailGroup().Open();
			}
		}

		// Token: 0x0600315B RID: 12635 RVA: 0x000FD475 File Offset: 0x000FB875
		public void OnClickClosePartyDetailButton()
		{
			if (this.GetPartyDetailGroup() != null)
			{
				this.GetPartyDetailGroup().Close();
			}
		}

		// Token: 0x0600315C RID: 12636 RVA: 0x000FD493 File Offset: 0x000FB893
		public void OnPartyNameInputFieldValueChanged()
		{
		}

		// Token: 0x0600315D RID: 12637 RVA: 0x000FD495 File Offset: 0x000FB895
		public override void OnChangePage()
		{
		}

		// Token: 0x040037C0 RID: 14272
		[SerializeField]
		[Tooltip("非推奨.")]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x040037C1 RID: 14273
		[SerializeField]
		[Tooltip("拡張メインUIGroup.現状バトル編成のみ.こちらがnullなら従来のフローを使う.")]
		protected MainGroupType m_MainGroupEx;

		// Token: 0x040037C2 RID: 14274
		[SerializeField]
		[Tooltip("拡張名前変更UIGroup.現状バトル編成のみ.こちらがnullなら従来のフローを使う.")]
		private PartyEditPartyNameChangeGroup m_PartyNameChangeGroupEx;

		// Token: 0x040037C3 RID: 14275
		[SerializeField]
		[Tooltip("拡張パーティ詳細パネル.現状バトル編成のみ.こちらがnullなら従来のフローを使う.")]
		private PartyEditPartyDetailGroup m_PartyDetailGroupEx;

		// Token: 0x040037C4 RID: 14276
		protected bool m_AddScrollCallback;

		// Token: 0x040037C5 RID: 14277
		private PartyEditUIBase.eStep m_Step;
	}
}
