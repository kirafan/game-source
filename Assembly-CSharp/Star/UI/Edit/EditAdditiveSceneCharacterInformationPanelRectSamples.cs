﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200094F RID: 2383
	public class EditAdditiveSceneCharacterInformationPanelRectSamples : MonoBehaviour
	{
		// Token: 0x06003182 RID: 12674 RVA: 0x000FF628 File Offset: 0x000FDA28
		public RectTransform GetSampleRect(EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType rectType)
		{
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.InfoParent)
			{
				return this.m_SampleRectInfoParent;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.Title)
			{
				return this.m_SampleRectTitle;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.Model)
			{
				return this.m_SampleRectModel;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.State)
			{
				return this.m_SampleRectState;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.State_Level)
			{
				return this.m_SampleRectState_Level;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.State_ExpGauge)
			{
				return this.m_SampleRectState_ExpGauge;
			}
			if (rectType == EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.State_Parameter)
			{
				return this.m_SampleRectState_Parameter;
			}
			if (rectType != EditAdditiveSceneCharacterInformationPanelRectSamples.eRectType.Upgrade_SelectItem)
			{
				return null;
			}
			return this.m_SampleRectUpgrade_SelectItem;
		}

		// Token: 0x040037DD RID: 14301
		[SerializeField]
		private RectTransform m_SampleRectInfoParent;

		// Token: 0x040037DE RID: 14302
		[SerializeField]
		private RectTransform m_SampleRectTitle;

		// Token: 0x040037DF RID: 14303
		[SerializeField]
		private RectTransform m_SampleRectModel;

		// Token: 0x040037E0 RID: 14304
		[SerializeField]
		private RectTransform m_SampleRectState;

		// Token: 0x040037E1 RID: 14305
		[SerializeField]
		private RectTransform m_SampleRectState_Level;

		// Token: 0x040037E2 RID: 14306
		[SerializeField]
		private RectTransform m_SampleRectState_ExpGauge;

		// Token: 0x040037E3 RID: 14307
		[SerializeField]
		private RectTransform m_SampleRectState_Parameter;

		// Token: 0x040037E4 RID: 14308
		[SerializeField]
		private RectTransform m_SampleRectUpgrade_SelectItem;

		// Token: 0x02000950 RID: 2384
		public enum eRectType
		{
			// Token: 0x040037E6 RID: 14310
			InfoParent,
			// Token: 0x040037E7 RID: 14311
			Title = 1000,
			// Token: 0x040037E8 RID: 14312
			Model = 2000,
			// Token: 0x040037E9 RID: 14313
			State = 3000,
			// Token: 0x040037EA RID: 14314
			State_Level = 3100,
			// Token: 0x040037EB RID: 14315
			State_ExpGauge = 3200,
			// Token: 0x040037EC RID: 14316
			State_Parameter = 3300,
			// Token: 0x040037ED RID: 14317
			Upgrade_SelectItem = 10000
		}
	}
}
