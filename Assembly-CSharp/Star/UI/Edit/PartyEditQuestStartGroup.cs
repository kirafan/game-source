﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200092A RID: 2346
	public class PartyEditQuestStartGroup : UIGroup
	{
		// Token: 0x06003093 RID: 12435 RVA: 0x000FC873 File Offset: 0x000FAC73
		public override void Open()
		{
			this.m_SortiePartyComfirmed.SetActive(true);
			this.m_UnableEditWarningAnim.Hide();
			base.Open();
		}

		// Token: 0x06003094 RID: 12436 RVA: 0x000FC892 File Offset: 0x000FAC92
		public void ShowWarning()
		{
			this.m_SortiePartyComfirmed.SetActive(false);
			this.m_UnableEditWarningAnim.Show();
			this.m_Timer = 3f;
		}

		// Token: 0x06003095 RID: 12437 RVA: 0x000FC8B8 File Offset: 0x000FACB8
		public override void Update()
		{
			base.Update();
			if (base.State == UIGroup.eState.Idle && 0f < this.m_Timer)
			{
				this.m_Timer -= Time.deltaTime;
				if (this.m_Timer < 0f)
				{
					this.m_SortiePartyComfirmed.SetActive(true);
					this.m_UnableEditWarningAnim.PlayOut();
				}
			}
		}

		// Token: 0x04003773 RID: 14195
		[SerializeField]
		[Tooltip("出撃パーティ確定！画像.")]
		private GameObject m_SortiePartyComfirmed;

		// Token: 0x04003774 RID: 14196
		[SerializeField]
		[Tooltip("確定しているため変更は出来ません。画像.")]
		private AnimUIPlayer m_UnableEditWarningAnim;

		// Token: 0x04003775 RID: 14197
		private float m_Timer;
	}
}
