﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007CF RID: 1999
	public class SelectedCharaInfoSkillLevelBeforeAfterInfo : SelectedCharaInfoSkillInfoBase
	{
		// Token: 0x0600295B RID: 10587 RVA: 0x000DB484 File Offset: 0x000D9884
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument applyArgument = (SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument)argument;
			if (this.m_afterLevelInfo != null)
			{
				if (applyArgument == null)
				{
					applyArgument.afterLevel = new LevelInformation();
				}
				if (applyArgument.afterLevel.m_Now > 0)
				{
					this.m_afterLevelInfo.SetLevel(applyArgument.afterLevel.m_Now, applyArgument.afterLevel.m_Max, new SelectedCharaInfoLevelInfo.EmphasisExpressionInfo
					{
						max = ((applyArgument.beforeLevel.m_Max >= applyArgument.afterLevel.m_Max) ? ((applyArgument.afterLevel.m_Max >= applyArgument.beforeLevel.m_Max) ? SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down) : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
					});
				}
			}
			base.ApplySkillInfo(applyArgument.skillDBType, applyArgument.skillID, applyArgument.ownerElement, applyArgument.beforeLevel, -1L);
		}

		// Token: 0x04002FEA RID: 12266
		[SerializeField]
		private SelectedCharaInfoLevelInfo m_afterLevelInfo;

		// Token: 0x020007D0 RID: 2000
		public class ApplyArgument : SelectedCharaInfoSkillInfoBase.SkillInfoApplyArgumentBase
		{
			// Token: 0x0600295C RID: 10588 RVA: 0x000DB561 File Offset: 0x000D9961
			public ApplyArgument(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement, LevelInformation in_beforeLevel, LevelInformation in_afterLevel) : base(skillDBType, skillID, ownerElement, in_beforeLevel)
			{
				this.afterLevel = in_afterLevel;
			}

			// Token: 0x04002FEB RID: 12267
			public LevelInformation afterLevel;
		}
	}
}
