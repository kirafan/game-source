﻿using System;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000915 RID: 2325
	public class EditTopUI : MenuUIBase
	{
		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06003049 RID: 12361 RVA: 0x000FAC8F File Offset: 0x000F908F
		public EditTopUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x0600304A RID: 12362 RVA: 0x000FAC97 File Offset: 0x000F9097
		public void Setup(float scrollValue)
		{
			this.m_MasterIllust.ApplyCurrentEquip();
			this.m_GameObject = base.gameObject;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Home);
			this.m_MenuScroll.SetVerticalNormalizedPosition(scrollValue);
		}

		// Token: 0x0600304B RID: 12363 RVA: 0x000FACCC File Offset: 0x000F90CC
		public float GetScrollValue()
		{
			return this.m_MenuScroll.GetVerticalNormalizedPosition();
		}

		// Token: 0x0600304C RID: 12364 RVA: 0x000FACDC File Offset: 0x000F90DC
		private void Update()
		{
			switch (this.m_Step)
			{
			case EditTopUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(EditTopUI.eStep.Idle);
				}
				break;
			}
			case EditTopUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(EditTopUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600304D RID: 12365 RVA: 0x000FADC4 File Offset: 0x000F91C4
		private void ChangeStep(EditTopUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case EditTopUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case EditTopUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case EditTopUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case EditTopUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case EditTopUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600304E RID: 12366 RVA: 0x000FAE6C File Offset: 0x000F926C
		public override void PlayIn()
		{
			this.ChangeStep(EditTopUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x0600304F RID: 12367 RVA: 0x000FAEB4 File Offset: 0x000F92B4
		public override void PlayOut()
		{
			this.ChangeStep(EditTopUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003050 RID: 12368 RVA: 0x000FAEF9 File Offset: 0x000F92F9
		public override bool IsMenuEnd()
		{
			return this.m_Step == EditTopUI.eStep.End;
		}

		// Token: 0x06003051 RID: 12369 RVA: 0x000FAF04 File Offset: 0x000F9304
		public void OnClickButtonCallBack(int buttonId)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_SelectButton = (EditTopUI.eButton)buttonId;
			this.PlayOut();
		}

		// Token: 0x04003717 RID: 14103
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003718 RID: 14104
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003719 RID: 14105
		[SerializeField]
		private CategorySelectMenu m_MenuScroll;

		// Token: 0x0400371A RID: 14106
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x0400371B RID: 14107
		private EditTopUI.eStep m_Step;

		// Token: 0x0400371C RID: 14108
		private EditTopUI.eButton m_SelectButton = EditTopUI.eButton.None;

		// Token: 0x02000916 RID: 2326
		private enum eStep
		{
			// Token: 0x0400371E RID: 14110
			Hide,
			// Token: 0x0400371F RID: 14111
			In,
			// Token: 0x04003720 RID: 14112
			Idle,
			// Token: 0x04003721 RID: 14113
			Out,
			// Token: 0x04003722 RID: 14114
			End
		}

		// Token: 0x02000917 RID: 2327
		public enum eButton
		{
			// Token: 0x04003724 RID: 14116
			None = -1,
			// Token: 0x04003725 RID: 14117
			PartyEdit,
			// Token: 0x04003726 RID: 14118
			SupportPartyEdit,
			// Token: 0x04003727 RID: 14119
			MasterEquip,
			// Token: 0x04003728 RID: 14120
			Upgrade,
			// Token: 0x04003729 RID: 14121
			LimitBreak,
			// Token: 0x0400372A RID: 14122
			Evolution,
			// Token: 0x0400372B RID: 14123
			CharaView
		}
	}
}
