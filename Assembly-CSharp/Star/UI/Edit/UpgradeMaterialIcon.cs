﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000906 RID: 2310
	public class UpgradeMaterialIcon : ScrollItemIcon
	{
		// Token: 0x06002FED RID: 12269 RVA: 0x000F9818 File Offset: 0x000F7C18
		protected virtual void Start()
		{
			this.AcquireNumberSelect();
			this.m_IsNumberSelectInitialized = false;
		}

		// Token: 0x06002FEE RID: 12270 RVA: 0x000F9828 File Offset: 0x000F7C28
		protected override void ApplyData()
		{
			this.AcquireUpgradeMaterialDataIgnoreCache();
			this.AcquireNumberSelect();
			this.RegisterNumberSelectLatestNumViaSetter();
			this.m_NameTextObj.text = this.m_UpgradeMaterialData.ItemName;
			if (this.m_NumberSelect.GetCurrentNum() > 0)
			{
				this.m_HaveNumTextObj.text = "<color=red>" + UIUtility.ItemHaveNumToString(this.m_UpgradeMaterialData.HaveNum - this.m_NumberSelect.GetCurrentNum(), true) + "</color>";
			}
			else
			{
				this.m_HaveNumTextObj.text = UIUtility.ItemHaveNumToString(this.m_UpgradeMaterialData.HaveNum, true);
			}
			if (this.m_ItemIcon == null)
			{
				this.m_ItemIconCloner.Setup(true);
				this.m_ItemIcon = this.m_ItemIconCloner.GetInst<ItemIconWithFrame>();
			}
			if (this.m_ItemIcon != null)
			{
				this.m_ItemIcon.Apply(this.m_UpgradeMaterialData.ItemID);
			}
			int num = this.m_UpgradeMaterialData.HaveNum;
			if (99 < num)
			{
				num = 99;
			}
			this.m_NumberSelect.SetRange(0, num);
			if (!((UpgradeMaterialList)this.m_Scroll).Owner.CanAddUseItem(this.m_UpgradeMaterialData.ItemID))
			{
				this.m_NumberSelect.AddInteractable = false;
			}
			else
			{
				this.m_NumberSelect.AddInteractable = true;
			}
		}

		// Token: 0x06002FEF RID: 12271 RVA: 0x000F9984 File Offset: 0x000F7D84
		protected void Update()
		{
			this.AcquireNumberSelect();
			if (this.m_ButtonHold != UpgradeMaterialIcon.eButton.None)
			{
				this.m_ButtonHoldCount += Time.deltaTime;
				if (this.m_ButtonHoldCount < 0.2f)
				{
					this.m_ButtonRepeatCount = 0f;
				}
				else
				{
					this.m_ButtonRepeatCount += Time.deltaTime;
					if (this.m_ButtonRepeatCount > 0.05f)
					{
						if (this.m_ButtonHold == UpgradeMaterialIcon.eButton.Left)
						{
							this.Decrease();
						}
						else
						{
							this.Increase();
						}
						this.m_ButtonRepeatCount = 0f;
					}
				}
			}
		}

		// Token: 0x06002FF0 RID: 12272 RVA: 0x000F9A20 File Offset: 0x000F7E20
		public void Increase()
		{
			if (this.m_UpgradeMaterialData == null)
			{
				return;
			}
			if (!((UpgradeMaterialList)this.m_Scroll).Owner.CanAddUseItem(this.m_UpgradeMaterialData.ItemID))
			{
				return;
			}
			this.m_UpgradeMaterialData.UseNum++;
			if (this.m_UpgradeMaterialData.UseNum > this.m_UpgradeMaterialData.HaveNum)
			{
				this.m_UpgradeMaterialData.UseNum = this.m_UpgradeMaterialData.HaveNum;
				return;
			}
			if (this.m_UpgradeMaterialData.UseNum > 99)
			{
				this.m_UpgradeMaterialData.UseNum = 99;
				return;
			}
			this.ApplyData();
		}

		// Token: 0x06002FF1 RID: 12273 RVA: 0x000F9ACC File Offset: 0x000F7ECC
		public void Decrease()
		{
			if (this.m_UpgradeMaterialData == null)
			{
				return;
			}
			this.m_UpgradeMaterialData.UseNum--;
			if (this.m_UpgradeMaterialData.UseNum < 0)
			{
				this.m_UpgradeMaterialData.UseNum = 0;
				return;
			}
			this.ApplyData();
		}

		// Token: 0x06002FF2 RID: 12274 RVA: 0x000F9B1C File Offset: 0x000F7F1C
		protected void InitializeNumberSelect()
		{
			if (this.m_IsNumberSelectInitialized)
			{
				return;
			}
			this.m_NumberSelect.SetRange(0, 99);
			this.RegisterNumberSelectOnChangeCallback();
			this.m_IsNumberSelectInitialized = true;
		}

		// Token: 0x06002FF3 RID: 12275 RVA: 0x000F9B45 File Offset: 0x000F7F45
		protected void RegisterNumberSelectLatestNumViaSetter()
		{
			this.m_NumberSelect.SetChangeCurrentNumCallBack(null);
			this.m_NumberSelect.SetCurrentNum(this.m_UpgradeMaterialData.UseNum);
			this.RegisterNumberSelectOnChangeCallback();
		}

		// Token: 0x06002FF4 RID: 12276 RVA: 0x000F9B6F File Offset: 0x000F7F6F
		protected void RegisterNumberSelectOnChangeCallback()
		{
			this.m_NumberSelect.SetChangeCurrentNumCallBack(new Action(this.OnChangeCurrentNum));
		}

		// Token: 0x06002FF5 RID: 12277 RVA: 0x000F9B88 File Offset: 0x000F7F88
		protected bool AcquireUpgradeMaterialData()
		{
			return this.m_UpgradeMaterialData != null || this.AcquireUpgradeMaterialDataIgnoreCache();
		}

		// Token: 0x06002FF6 RID: 12278 RVA: 0x000F9B9D File Offset: 0x000F7F9D
		protected bool AcquireUpgradeMaterialDataIgnoreCache()
		{
			this.m_UpgradeMaterialData = (UpgradeMaterialData)this.m_Data;
			if (this.m_NumberSelect != null)
			{
				this.InitializeNumberSelect();
			}
			return this.m_UpgradeMaterialData != null;
		}

		// Token: 0x06002FF7 RID: 12279 RVA: 0x000F9BD4 File Offset: 0x000F7FD4
		protected bool AcquireNumberSelect()
		{
			if (this.m_NumberSelect != null)
			{
				return true;
			}
			if (this.m_NumberSelectCloner != null)
			{
				this.m_NumberSelectCloner.Setup(true);
				this.m_NumberSelect = this.m_NumberSelectCloner.GetInst<NumberSelect>();
			}
			if (this.m_UpgradeMaterialData != null)
			{
				this.InitializeNumberSelect();
			}
			return this.m_NumberSelect != null;
		}

		// Token: 0x06002FF8 RID: 12280 RVA: 0x000F9C3F File Offset: 0x000F803F
		public void OnButtonLeft()
		{
			this.m_ButtonHold = UpgradeMaterialIcon.eButton.Left;
			this.m_ButtonHoldCount = 0f;
			this.Decrease();
		}

		// Token: 0x06002FF9 RID: 12281 RVA: 0x000F9C59 File Offset: 0x000F8059
		public void OnButtonRight()
		{
			this.m_ButtonHold = UpgradeMaterialIcon.eButton.Right;
			this.m_ButtonHoldCount = 0f;
			this.Increase();
		}

		// Token: 0x06002FFA RID: 12282 RVA: 0x000F9C73 File Offset: 0x000F8073
		public void OnReleaseButton()
		{
			this.m_ButtonHold = UpgradeMaterialIcon.eButton.None;
			this.m_ButtonHoldCount = 0f;
			this.m_ButtonRepeatCount = 0f;
		}

		// Token: 0x06002FFB RID: 12283 RVA: 0x000F9C94 File Offset: 0x000F8094
		public void OnChangeCurrentNum()
		{
			if (this.m_UpgradeMaterialData == null)
			{
				return;
			}
			this.m_UpgradeMaterialData.UseNum = this.m_NumberSelect.GetCurrentNum();
			((UpgradeMaterialList)this.m_Scroll).Owner.SetSelectedItemNum(this.m_UpgradeMaterialData.ItemID, this.m_UpgradeMaterialData.UseNum);
			this.ApplyData();
		}

		// Token: 0x040036E2 RID: 14050
		private const float BUTTON_REPEAT_THRESHOLD = 0.2f;

		// Token: 0x040036E3 RID: 14051
		private const float BUTTON_REPEAT_WAIT = 0.05f;

		// Token: 0x040036E4 RID: 14052
		private UpgradeMaterialData m_UpgradeMaterialData;

		// Token: 0x040036E5 RID: 14053
		[SerializeField]
		private PrefabCloner m_ItemIconCloner;

		// Token: 0x040036E6 RID: 14054
		private ItemIconWithFrame m_ItemIcon;

		// Token: 0x040036E7 RID: 14055
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x040036E8 RID: 14056
		[SerializeField]
		private Text m_HaveNumTextObj;

		// Token: 0x040036E9 RID: 14057
		[SerializeField]
		private PrefabCloner m_NumberSelectCloner;

		// Token: 0x040036EA RID: 14058
		private NumberSelect m_NumberSelect;

		// Token: 0x040036EB RID: 14059
		private bool m_IsNumberSelectInitialized;

		// Token: 0x040036EC RID: 14060
		private UpgradeMaterialIcon.eButton m_ButtonHold;

		// Token: 0x040036ED RID: 14061
		private float m_ButtonHoldCount;

		// Token: 0x040036EE RID: 14062
		private float m_ButtonRepeatCount;

		// Token: 0x02000907 RID: 2311
		private enum eButton
		{
			// Token: 0x040036F0 RID: 14064
			None,
			// Token: 0x040036F1 RID: 14065
			Left,
			// Token: 0x040036F2 RID: 14066
			Right
		}
	}
}
