﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007C7 RID: 1991
	public class SelectedCharaInfoScheduleDropStateInfo : SelectedCharaInfoDetailInfoBase
	{
		// Token: 0x0600294D RID: 10573 RVA: 0x000DB2A4 File Offset: 0x000D96A4
		public override void Apply(SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			SelectedCharaInfoScheduleDropStateInfo.ApplyArgument applyArgument = (SelectedCharaInfoScheduleDropStateInfo.ApplyArgument)argument;
			bool flag = false;
			int dropItemKey = UserCharaUtil.GetCharaIDToNameParam(applyArgument.charaId).m_DropItemKey;
			for (int i = 0; i < this.m_ScheduleDropStateInfoParams.Length; i++)
			{
				if (this.m_ScheduleDropStateInfoParams[i].m_DropItemKey == dropItemKey)
				{
					this.m_ScheduleDropStateImage.sprite = this.m_ScheduleDropStateInfoParams[i].m_SpriteAssets;
					this.m_ScheduleDropStateMessage.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ProfileStateScheduleDrop1000 + i);
					flag = true;
					base.gameObject.SetActive(true);
				}
			}
			if (!flag)
			{
				this.m_ScheduleDropStateImage.gameObject.SetActive(false);
				this.m_ScheduleDropStateMessage.text = ">>お土産はありません<<";
				base.gameObject.SetActive(true);
			}
		}

		// Token: 0x0600294E RID: 10574 RVA: 0x000DB37B File Offset: 0x000D977B
		public override void ApplyEmpty()
		{
		}

		// Token: 0x04002FDC RID: 12252
		[SerializeField]
		[Tooltip("表示するアイコンを紐付ける擬似DB.")]
		private SelectedCharaInfoScheduleDropStateInfo.ScheduleDropStateInfoParam[] m_ScheduleDropStateInfoParams;

		// Token: 0x04002FDD RID: 12253
		[SerializeField]
		[Tooltip("アイコン表示先.")]
		private Image m_ScheduleDropStateImage;

		// Token: 0x04002FDE RID: 12254
		[SerializeField]
		[Tooltip("メッセージ.")]
		private Text m_ScheduleDropStateMessage;

		// Token: 0x020007C8 RID: 1992
		[Serializable]
		public class ScheduleDropStateInfoParam
		{
			// Token: 0x04002FDF RID: 12255
			public int m_DropItemKey;

			// Token: 0x04002FE0 RID: 12256
			public Sprite m_SpriteAssets;

			// Token: 0x04002FE1 RID: 12257
			public int m_TextCommonId;
		}

		// Token: 0x020007C9 RID: 1993
		public class ApplyArgument : SelectedCharaInfoDetailInfoBase.ApplyArgumentBase
		{
			// Token: 0x06002950 RID: 10576 RVA: 0x000DB385 File Offset: 0x000D9785
			public ApplyArgument(int in_charaId, UserScheduleData.DropState in_dropState)
			{
				this.charaId = in_charaId;
				this.dropState = in_dropState;
			}

			// Token: 0x04002FE2 RID: 12258
			public int charaId;

			// Token: 0x04002FE3 RID: 12259
			public UserScheduleData.DropState dropState;
		}
	}
}
