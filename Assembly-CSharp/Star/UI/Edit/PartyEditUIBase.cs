﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000942 RID: 2370
	public abstract class PartyEditUIBase : MenuUIBase
	{
		// Token: 0x06003121 RID: 12577 RVA: 0x000FC92F File Offset: 0x000FAD2F
		public static bool ModeToWeaponActive(PartyEditUIBase.eMode mode)
		{
			return mode == PartyEditUIBase.eMode.Edit || mode == PartyEditUIBase.eMode.Quest;
		}

		// Token: 0x06003122 RID: 12578 RVA: 0x000FC93E File Offset: 0x000FAD3E
		public PartyEditMainGroupBase.eMode ConvertPartyEditUIBaseeModeToPartyEditMainGroupeMode(PartyEditUIBase.eMode mode)
		{
			switch (mode)
			{
			case PartyEditUIBase.eMode.Edit:
				return PartyEditMainGroupBase.eMode.Edit;
			case PartyEditUIBase.eMode.Quest:
				return PartyEditMainGroupBase.eMode.Quest;
			case PartyEditUIBase.eMode.QuestStart:
				return PartyEditMainGroupBase.eMode.QuestStart;
			case PartyEditUIBase.eMode.View:
				return PartyEditMainGroupBase.eMode.View;
			case PartyEditUIBase.eMode.PartyDetail:
				return PartyEditMainGroupBase.eMode.PartyDetail;
			default:
				return PartyEditMainGroupBase.eMode.Edit;
			}
		}

		// Token: 0x06003123 RID: 12579 RVA: 0x000FC96A File Offset: 0x000FAD6A
		public static EnumerationPanelCoreBase.eMode ConvertPartyEditUIBaseeModeToEnumerationPanelCoreBaseeMode(PartyEditUIBase.eMode mode)
		{
			switch (mode)
			{
			case PartyEditUIBase.eMode.Edit:
				return EnumerationPanelCoreBase.eMode.Edit;
			case PartyEditUIBase.eMode.Quest:
				return EnumerationPanelCoreBase.eMode.Quest;
			case PartyEditUIBase.eMode.QuestStart:
				return EnumerationPanelCoreBase.eMode.QuestStart;
			case PartyEditUIBase.eMode.View:
				return EnumerationPanelCoreBase.eMode.View;
			case PartyEditUIBase.eMode.PartyDetail:
				return EnumerationPanelCoreBase.eMode.PartyDetail;
			default:
				return EnumerationPanelCoreBase.eMode.Edit;
			}
		}

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06003124 RID: 12580 RVA: 0x000FC996 File Offset: 0x000FAD96
		// (set) Token: 0x06003125 RID: 12581 RVA: 0x000FC99E File Offset: 0x000FAD9E
		public PartyEditUIBase.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
			set
			{
				this.m_SelectButton = value;
			}
		}

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06003126 RID: 12582 RVA: 0x000FC9A8 File Offset: 0x000FADA8
		public int SelectPartyIndex
		{
			get
			{
				PartyEditMainGroupBase mainGroupBase = this.GetMainGroupBase();
				if (mainGroupBase == null)
				{
					return -1;
				}
				return mainGroupBase.SelectPartyIndex;
			}
		}

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06003127 RID: 12583 RVA: 0x000FC9D0 File Offset: 0x000FADD0
		public long SelectPartyMngID
		{
			get
			{
				PartyEditMainGroupBase mainGroupBase = this.GetMainGroupBase();
				if (mainGroupBase == null)
				{
					return -1L;
				}
				return mainGroupBase.SelectPartyMngID;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06003128 RID: 12584 RVA: 0x000FC9FC File Offset: 0x000FADFC
		public int SelectCharaIndex
		{
			get
			{
				PartyEditMainGroupBase mainGroupBase = this.GetMainGroupBase();
				if (mainGroupBase == null)
				{
					return -1;
				}
				return mainGroupBase.SelectCharaIndex;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06003129 RID: 12585 RVA: 0x000FCA24 File Offset: 0x000FAE24
		public long SelectCharaMngID
		{
			get
			{
				PartyEditMainGroupBase mainGroupBase = this.GetMainGroupBase();
				if (mainGroupBase == null)
				{
					return -1L;
				}
				return mainGroupBase.SelectCharaMngID;
			}
		}

		// Token: 0x1400007F RID: 127
		// (add) Token: 0x0600312A RID: 12586 RVA: 0x000FCA50 File Offset: 0x000FAE50
		// (remove) Token: 0x0600312B RID: 12587 RVA: 0x000FCA88 File Offset: 0x000FAE88
		public event Action OnClickButton;

		// Token: 0x14000080 RID: 128
		// (add) Token: 0x0600312C RID: 12588 RVA: 0x000FCAC0 File Offset: 0x000FAEC0
		// (remove) Token: 0x0600312D RID: 12589 RVA: 0x000FCAF8 File Offset: 0x000FAEF8
		public event Action<string> OnDecideNameChange;

		// Token: 0x0600312E RID: 12590
		public abstract void Setup(PartyEditUIBase.eMode mode = PartyEditUIBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null);

		// Token: 0x0600312F RID: 12591 RVA: 0x000FCB2E File Offset: 0x000FAF2E
		protected void ExecuteOnClickButton()
		{
			this.OnClickButton.Call();
		}

		// Token: 0x06003130 RID: 12592 RVA: 0x000FCB3B File Offset: 0x000FAF3B
		protected void ExecuteOnDecideNameChange(string arg)
		{
			this.OnDecideNameChange.Call(arg);
		}

		// Token: 0x06003131 RID: 12593
		protected abstract PartyEditMainGroupBase GetMainGroupBase();

		// Token: 0x06003132 RID: 12594
		public abstract void OnClickCharaCallBack();

		// Token: 0x06003133 RID: 12595
		public abstract void OnHoldCharaCallBack();

		// Token: 0x06003134 RID: 12596
		public abstract void OnClickWeaponCallBack();

		// Token: 0x06003135 RID: 12597
		public abstract void OnChangePage();

		// Token: 0x040037A4 RID: 14244
		private const int PARTY_NAME_NUMMAX = 10;

		// Token: 0x040037A5 RID: 14245
		protected PartyEditUIBase.eButton m_SelectButton = PartyEditUIBase.eButton.None;

		// Token: 0x040037A6 RID: 14246
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x02000943 RID: 2371
		public enum eMode
		{
			// Token: 0x040037AA RID: 14250
			Edit,
			// Token: 0x040037AB RID: 14251
			Quest,
			// Token: 0x040037AC RID: 14252
			QuestStart,
			// Token: 0x040037AD RID: 14253
			View,
			// Token: 0x040037AE RID: 14254
			PartyDetail
		}

		// Token: 0x02000944 RID: 2372
		protected enum eStep
		{
			// Token: 0x040037B0 RID: 14256
			Hide,
			// Token: 0x040037B1 RID: 14257
			In,
			// Token: 0x040037B2 RID: 14258
			Out,
			// Token: 0x040037B3 RID: 14259
			Idle,
			// Token: 0x040037B4 RID: 14260
			NameChange,
			// Token: 0x040037B5 RID: 14261
			NameChangeClose,
			// Token: 0x040037B6 RID: 14262
			ConfirmBack,
			// Token: 0x040037B7 RID: 14263
			CharaDetail,
			// Token: 0x040037B8 RID: 14264
			Weapon,
			// Token: 0x040037B9 RID: 14265
			End,
			// Token: 0x040037BA RID: 14266
			Num
		}

		// Token: 0x02000945 RID: 2373
		public enum eButton
		{
			// Token: 0x040037BC RID: 14268
			None = -1,
			// Token: 0x040037BD RID: 14269
			Back,
			// Token: 0x040037BE RID: 14270
			Chara,
			// Token: 0x040037BF RID: 14271
			Decide
		}
	}
}
