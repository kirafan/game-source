﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000941 RID: 2369
	public abstract class PartyEditPartyPanelBase : EnumerationPanelGen<PartyEditPartyPanelBase, PartyCharaPanelBase, PartyPanelData, PartyEditPartyPanelBaseCore, PartyEditPartyPanelBaseCore.SharedInstanceExOverrideBase>
	{
		// Token: 0x0600311C RID: 12572 RVA: 0x000FDD74 File Offset: 0x000FC174
		protected bool AcquirePartyScroll()
		{
			if (this.m_PartyScroll != null)
			{
				return true;
			}
			this.m_PartyScroll = (PartyScroll)base.Scroll;
			return !(this.m_PartyScroll == null) && this.m_PartyScroll != null;
		}

		// Token: 0x0600311D RID: 12573 RVA: 0x000FDDC4 File Offset: 0x000FC1C4
		public void OnClickCharaPanelChara(int slotIndex)
		{
			this.m_Core.OnClickCharaPanelChara(slotIndex);
		}

		// Token: 0x0600311E RID: 12574 RVA: 0x000FDDD2 File Offset: 0x000FC1D2
		public void OnClickCharaPanelWeapon(int slotIndex)
		{
			this.m_Core.OnClickCharaPanelWeapon(slotIndex);
		}

		// Token: 0x0600311F RID: 12575 RVA: 0x000FDDE0 File Offset: 0x000FC1E0
		public void OnHoldCharaPanelChara(int slotIndex)
		{
			this.m_Core.OnHoldCharaPanelChara(slotIndex);
		}

		// Token: 0x040037A3 RID: 14243
		protected PartyScroll m_PartyScroll;
	}
}
