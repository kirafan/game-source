﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020007E8 RID: 2024
	public class SelectedCharaInfoVoiceListItemData : ScrollItemData
	{
		// Token: 0x060029CC RID: 10700 RVA: 0x000DCE46 File Offset: 0x000DB246
		public SelectedCharaInfoVoiceListItemData()
		{
		}

		// Token: 0x060029CD RID: 10701 RVA: 0x000DCE4E File Offset: 0x000DB24E
		public SelectedCharaInfoVoiceListItemData(eCharaNamedType in_charaNamed, string in_category, string in_voiceTitle, string in_cueName, int in_needFriendship)
		{
			this.charaNamed = in_charaNamed;
			this.category = in_category;
			this.voiceTitle = in_voiceTitle;
			this.cueName = in_cueName;
			this.needFriendship = in_needFriendship;
		}

		// Token: 0x04003039 RID: 12345
		public eCharaNamedType charaNamed;

		// Token: 0x0400303A RID: 12346
		public string category;

		// Token: 0x0400303B RID: 12347
		public string voiceTitle;

		// Token: 0x0400303C RID: 12348
		public string cueName;

		// Token: 0x0400303D RID: 12349
		public int needFriendship;
	}
}
