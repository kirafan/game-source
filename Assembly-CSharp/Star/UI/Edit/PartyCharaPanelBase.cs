﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000933 RID: 2355
	public class PartyCharaPanelBase : EnumerationCharaPanelAdapterExGen<PartyEditPartyPanelBase>
	{
		// Token: 0x060030D4 RID: 12500 RVA: 0x000FBFE1 File Offset: 0x000FA3E1
		public override EnumerationCharaPanel CreateCore()
		{
			return new PartyCharaPanelBaseCore();
		}

		// Token: 0x060030D5 RID: 12501 RVA: 0x000FBFE8 File Offset: 0x000FA3E8
		public void OnClickCallBack()
		{
			if (this.m_Parent != null)
			{
				this.m_Parent.OnClickCharaPanelChara(base.GetSlotIndex());
			}
		}

		// Token: 0x060030D6 RID: 12502 RVA: 0x000FC00C File Offset: 0x000FA40C
		public void OnClickWeaponCallBack()
		{
			if (this.m_Parent != null)
			{
				this.m_Parent.OnClickCharaPanelWeapon(base.GetSlotIndex());
			}
		}

		// Token: 0x060030D7 RID: 12503 RVA: 0x000FC030 File Offset: 0x000FA430
		public void OnHoldCharaCallBack()
		{
			if (this.m_Parent != null)
			{
				this.m_Parent.OnHoldCharaPanelChara(base.GetSlotIndex());
			}
		}

		// Token: 0x02000934 RID: 2356
		public enum eButton
		{
			// Token: 0x0400378F RID: 14223
			Chara,
			// Token: 0x04003790 RID: 14224
			Weapon
		}
	}
}
