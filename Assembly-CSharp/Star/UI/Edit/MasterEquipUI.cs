﻿using System;
using System.Collections.Generic;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200091A RID: 2330
	public class MasterEquipUI : MenuUIBase
	{
		// Token: 0x17000310 RID: 784
		// (get) Token: 0x0600305A RID: 12378 RVA: 0x000FB10E File Offset: 0x000F950E
		public long CurrentEquipMasterOrbMngID
		{
			get
			{
				return this.m_CurrentEquipMasterOrbMngID;
			}
		}

		// Token: 0x0600305B RID: 12379 RVA: 0x000FB116 File Offset: 0x000F9516
		private void Start()
		{
		}

		// Token: 0x0600305C RID: 12380 RVA: 0x000FB118 File Offset: 0x000F9518
		public void Setup()
		{
			this.m_MasterIllust.ApplyCurrentEquip();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.m_HaveScroll.Setup();
			this.m_CurrentEquipMasterOrbMngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetEquipUserMasterOrbMngID();
			List<UserMasterOrbData> userMasterOrbDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserMasterOrbDatas;
			for (int i = 0; i < userMasterOrbDatas.Count; i++)
			{
				HaveOrbListItemData item = new HaveOrbListItemData(userMasterOrbDatas[i].MngID, userMasterOrbDatas[i].IsEquip, new Action<long>(this.OnClickHaveCallBack));
				this.m_HaveOrbDataList.Add(item);
				this.m_HaveScroll.AddItem(item);
			}
			this.ApplyCurrentEquip();
		}

		// Token: 0x0600305D RID: 12381 RVA: 0x000FB1D0 File Offset: 0x000F95D0
		private void Update()
		{
			switch (this.m_Step)
			{
			case MasterEquipUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(MasterEquipUI.eStep.Idle);
				}
				break;
			}
			case MasterEquipUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(MasterEquipUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600305E RID: 12382 RVA: 0x000FB2CC File Offset: 0x000F96CC
		private void ChangeStep(MasterEquipUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MasterEquipUI.eStep.Hide:
				this.SetEnableInput(false);
				break;
			case MasterEquipUI.eStep.In:
				this.SetEnableInput(false);
				break;
			case MasterEquipUI.eStep.Out:
				this.SetEnableInput(false);
				break;
			case MasterEquipUI.eStep.Idle:
				this.SetEnableInput(true);
				this.m_MainGroup.SetEnableInput(true);
				break;
			}
		}

		// Token: 0x0600305F RID: 12383 RVA: 0x000FB340 File Offset: 0x000F9740
		public override void PlayIn()
		{
			this.ChangeStep(MasterEquipUI.eStep.In);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_MainGroup.Open();
		}

		// Token: 0x06003060 RID: 12384 RVA: 0x000FB38C File Offset: 0x000F978C
		public override void PlayOut()
		{
			this.ChangeStep(MasterEquipUI.eStep.Out);
			this.SetEnableInput(false);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003061 RID: 12385 RVA: 0x000FB3D8 File Offset: 0x000F97D8
		public override bool IsMenuEnd()
		{
			return this.m_Step == MasterEquipUI.eStep.End;
		}

		// Token: 0x06003062 RID: 12386 RVA: 0x000FB3E3 File Offset: 0x000F97E3
		private void OnClickHaveCallBack(long equipMasterOrbMngID)
		{
			if (this.m_CurrentEquipMasterOrbMngID != equipMasterOrbMngID)
			{
				this.m_CurrentEquipMasterOrbMngID = equipMasterOrbMngID;
				this.ApplyCurrentEquip();
			}
		}

		// Token: 0x06003063 RID: 12387 RVA: 0x000FB400 File Offset: 0x000F9800
		private void ApplyCurrentEquip()
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (this.m_CurrentEquipMasterOrbMngID != -1L)
			{
				UserMasterOrbData userMasterOrbData = userDataMng.GetUserMasterOrbData(this.m_CurrentEquipMasterOrbMngID);
				MasterOrbListDB_Param param = dbMng.MasterOrbListDB.GetParam(userMasterOrbData.OrbID);
				this.m_MasterIllust.Apply(userMasterOrbData.OrbID);
				this.m_OrbInformation.ApplyCurrentEquip(this.m_CurrentEquipMasterOrbMngID);
				int[] skillIDs = param.m_SkillIDs;
				if (skillIDs != null)
				{
					for (int i = 0; i < this.m_OrbSkillIcons.Length; i++)
					{
						if (i < skillIDs.Length && skillIDs[i] != -1)
						{
							this.m_OrbSkillIcons[i].Display(true);
							this.m_OrbSkillIcons[i].Apply(skillIDs[i], userMasterOrbData.Lv >= param.m_SkillAavailableLvs[i], param.m_SkillAavailableLvs[i]);
						}
						else
						{
							this.m_OrbSkillIcons[i].Display(false);
						}
					}
				}
				else
				{
					for (int j = 0; j < skillIDs.Length; j++)
					{
						this.m_OrbSkillIcons[j].Display(false);
					}
				}
			}
			for (int k = 0; k < this.m_HaveOrbDataList.Count; k++)
			{
				if (this.m_HaveOrbDataList[k].m_OrbMngID == this.m_CurrentEquipMasterOrbMngID)
				{
					this.m_HaveOrbDataList[k].m_IsEquip = true;
					this.m_HaveOrbDataList[k].RefreshItem();
				}
				else if (this.m_HaveOrbDataList[k].m_IsEquip)
				{
					this.m_HaveOrbDataList[k].m_IsEquip = false;
					this.m_HaveOrbDataList[k].RefreshItem();
				}
			}
		}

		// Token: 0x04003733 RID: 14131
		private MasterEquipUI.eStep m_Step;

		// Token: 0x04003734 RID: 14132
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003735 RID: 14133
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x04003736 RID: 14134
		[SerializeField]
		private MasterOrbInformation m_OrbInformation;

		// Token: 0x04003737 RID: 14135
		[SerializeField]
		private OrbSkillIcon[] m_OrbSkillIcons;

		// Token: 0x04003738 RID: 14136
		[SerializeField]
		private ScrollViewBase m_HaveScroll;

		// Token: 0x04003739 RID: 14137
		private List<HaveOrbListItemData> m_HaveOrbDataList = new List<HaveOrbListItemData>();

		// Token: 0x0400373A RID: 14138
		[SerializeField]
		private MasterIllust m_MasterIllust;

		// Token: 0x0400373B RID: 14139
		private long m_CurrentEquipMasterOrbMngID = -1L;

		// Token: 0x0200091B RID: 2331
		private enum eStep
		{
			// Token: 0x0400373D RID: 14141
			Hide,
			// Token: 0x0400373E RID: 14142
			In,
			// Token: 0x0400373F RID: 14143
			Out,
			// Token: 0x04003740 RID: 14144
			Idle,
			// Token: 0x04003741 RID: 14145
			End,
			// Token: 0x04003742 RID: 14146
			Num
		}
	}
}
