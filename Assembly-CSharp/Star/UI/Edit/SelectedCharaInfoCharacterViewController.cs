﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007BB RID: 1979
	public class SelectedCharaInfoCharacterViewController : MonoBehaviour
	{
		// Token: 0x060028EE RID: 10478 RVA: 0x000D9C58 File Offset: 0x000D8058
		public void Setup()
		{
			this.m_ViewRequestFlagPairs = new SelectedCharaInfoCharacterViewController.ViewRequestFlagPair[4];
			this.m_ViewRequestFlagPairs[0] = new SelectedCharaInfoCharacterViewController.ViewRequestFlagPair(this.m_ModelArea);
			this.m_ViewRequestFlagPairs[1] = new SelectedCharaInfoCharacterViewController.ViewRequestFlagPair(this.m_CharacterIllust);
			this.m_ViewRequestFlagPairs[2] = new SelectedCharaInfoCharacterViewController.ViewRequestFlagPair(this.m_BustImage);
			this.m_ViewRequestFlagPairs[3] = new SelectedCharaInfoCharacterViewController.ViewRequestFlagPair(this.m_CharacterIcon);
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				SelectedCharaInfoCharacterViewBase view = this.GetView(i);
				if (view != null)
				{
					view.Setup();
					view.gameObject.SetActive(false);
				}
			}
			this.ResetNextRequestFlags();
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Wait);
		}

		// Token: 0x060028EF RID: 10479 RVA: 0x000D9D0C File Offset: 0x000D810C
		public void Destroy()
		{
			if (this.m_ViewRequestFlagPairs != null)
			{
				for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
				{
					if (this.m_ViewRequestFlagPairs[i] != null && this.m_ViewRequestFlagPairs[i].m_CharaView != null)
					{
						this.m_ViewRequestFlagPairs[i].m_CharaView.Destroy();
					}
				}
			}
		}

		// Token: 0x060028F0 RID: 10480 RVA: 0x000D9D74 File Offset: 0x000D8174
		private void Update()
		{
			if (this.m_ViewRequestFlagPairs != null)
			{
				SelectedCharaInfoCharacterViewBase.eState state = this.GetState();
				if (state == SelectedCharaInfoCharacterViewBase.eState.Wait || state == SelectedCharaInfoCharacterViewBase.eState.Prepared)
				{
					if (this.m_Ewcdc != null || this.m_Cdc != null || this.m_CharaID != -1)
					{
						this.RequestLoadAll();
					}
				}
			}
		}

		// Token: 0x060028F1 RID: 10481 RVA: 0x000D9DD3 File Offset: 0x000D81D3
		public virtual void PlayIn(bool autoIdle, EquipWeaponCharacterDataController ewcdc, SelectedCharaInfoCharacterViewController.eViewType[] viewTypes)
		{
			if (this.GetState() == SelectedCharaInfoCharacterViewBase.eState.Wait)
			{
				this.RequestLoadView(viewTypes, ewcdc, true);
				return;
			}
			this.PlayIn(autoIdle);
		}

		// Token: 0x060028F2 RID: 10482 RVA: 0x000D9DF2 File Offset: 0x000D81F2
		public virtual void PlayIn(bool autoIdle, CharacterDataController cdc, SelectedCharaInfoCharacterViewController.eViewType[] viewTypes)
		{
			if (this.GetState() == SelectedCharaInfoCharacterViewBase.eState.Wait)
			{
				this.RequestLoadView(viewTypes, cdc, true);
				return;
			}
			this.PlayIn(autoIdle);
		}

		// Token: 0x060028F3 RID: 10483 RVA: 0x000D9E11 File Offset: 0x000D8211
		public virtual void PlayIn(bool autoIdle = true)
		{
			this.PlayIn((!autoIdle) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle);
		}

		// Token: 0x060028F4 RID: 10484 RVA: 0x000D9E28 File Offset: 0x000D8228
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance)
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepare)
			{
				return;
			}
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				if (this.m_ViewRequestFlagPairs[i] != null && this.m_ViewRequestFlagPairs[i].m_CharaView != null && this.m_ViewRequestFlagPairs[i].m_CharaView.IsPreparing())
				{
					return;
				}
			}
			for (int j = 0; j < this.m_ViewRequestFlagPairs.Length; j++)
			{
				if (this.m_ViewRequestFlagPairs[j] != null && this.m_ViewRequestFlagPairs[j].m_CharaView != null)
				{
					this.m_ViewRequestFlagPairs[j].m_CharaView.PlayIn(toStateAutomaticallyAdvance);
				}
			}
		}

		// Token: 0x060028F5 RID: 10485 RVA: 0x000D9EFC File Offset: 0x000D82FC
		public virtual void FinishIn()
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.In)
			{
				return;
			}
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				if (this.m_ViewRequestFlagPairs[i] != null && this.m_ViewRequestFlagPairs[i].m_CharaView != null && this.m_ViewRequestFlagPairs[i].m_CharaView.IsAnimStateIn())
				{
					return;
				}
			}
			for (int j = 0; j < this.m_ViewRequestFlagPairs.Length; j++)
			{
				if (this.m_ViewRequestFlagPairs[j] != null && this.m_ViewRequestFlagPairs[j].m_CharaView != null)
				{
					this.m_ViewRequestFlagPairs[j].m_CharaView.FinishIn();
				}
			}
		}

		// Token: 0x060028F6 RID: 10486 RVA: 0x000D9FD0 File Offset: 0x000D83D0
		public void PlayOut()
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Idle)
			{
				return;
			}
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				if (this.m_ViewRequestFlagPairs[i] != null && this.m_ViewRequestFlagPairs[i].m_CharaView != null)
				{
					this.m_ViewRequestFlagPairs[i].m_CharaView.PlayOut(SelectedCharaInfoCharacterViewBase.eState.Num);
				}
			}
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Out);
		}

		// Token: 0x060028F7 RID: 10487 RVA: 0x000DA058 File Offset: 0x000D8458
		public SelectedCharaInfoCharacterViewBase.eState GetState()
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				return SelectedCharaInfoCharacterViewBase.eState.Invalid;
			}
			int[] array = new int[7];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = 0;
			}
			int num = 0;
			for (int j = 0; j < this.m_ViewRequestFlagPairs.Length; j++)
			{
				SelectedCharaInfoCharacterViewBase view = this.GetView(j);
				if (view != null)
				{
					num++;
					array[(int)view.GetState()]++;
				}
			}
			SelectedCharaInfoCharacterViewBase.eState eState = SelectedCharaInfoCharacterViewBase.eState.Invalid;
			eState++;
			int num2 = array[(int)eState];
			if (num <= num2)
			{
				return eState;
			}
			num -= num2;
			eState = SelectedCharaInfoCharacterViewBase.eState.Prepared;
			num2 = array[(int)eState];
			if (num <= num2)
			{
				return eState;
			}
			num -= num2;
			int num3 = 6;
			while (0 <= num3)
			{
				SelectedCharaInfoCharacterViewBase.eState eState2 = (SelectedCharaInfoCharacterViewBase.eState)num3;
				if (eState2 != SelectedCharaInfoCharacterViewBase.eState.Wait && eState2 != SelectedCharaInfoCharacterViewBase.eState.Prepared)
				{
					num2 = array[num3];
					num -= num2;
					if (num <= 0)
					{
						return eState2;
					}
				}
				num3--;
			}
			return SelectedCharaInfoCharacterViewBase.eState.Invalid;
		}

		// Token: 0x060028F8 RID: 10488 RVA: 0x000DA158 File Offset: 0x000D8558
		protected void SetState(SelectedCharaInfoCharacterViewBase.eState state)
		{
		}

		// Token: 0x060028F9 RID: 10489 RVA: 0x000DA15C File Offset: 0x000D855C
		public SelectedCharaInfoCharacterViewBase.eState GetViewState(SelectedCharaInfoCharacterViewController.eViewType viewType)
		{
			SelectedCharaInfoCharacterViewBase view = this.GetView(viewType);
			if (view == null)
			{
				return SelectedCharaInfoCharacterViewBase.eState.Invalid;
			}
			return view.GetState();
		}

		// Token: 0x060028FA RID: 10490 RVA: 0x000DA185 File Offset: 0x000D8585
		public SelectedCharaInfoCharacterViewBase.eState GetModelAreaState()
		{
			return this.GetViewState(SelectedCharaInfoCharacterViewController.eViewType.Begin);
		}

		// Token: 0x060028FB RID: 10491 RVA: 0x000DA18E File Offset: 0x000D858E
		public SelectedCharaInfoCharacterViewBase.eState GetCharacterIllustState()
		{
			return this.GetViewState(SelectedCharaInfoCharacterViewController.eViewType.Illust);
		}

		// Token: 0x060028FC RID: 10492 RVA: 0x000DA197 File Offset: 0x000D8597
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair GetViewRequestFlagPair(SelectedCharaInfoCharacterViewController.eViewType viewType)
		{
			return this.GetViewRequestFlagPair((int)viewType);
		}

		// Token: 0x060028FD RID: 10493 RVA: 0x000DA1A0 File Offset: 0x000D85A0
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair GetViewRequestFlagPair(int nViewType)
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				return null;
			}
			if (nViewType < 0 || this.m_ViewRequestFlagPairs.Length <= nViewType)
			{
				return null;
			}
			return this.m_ViewRequestFlagPairs[nViewType];
		}

		// Token: 0x060028FE RID: 10494 RVA: 0x000DA1DB File Offset: 0x000D85DB
		private SelectedCharaInfoCharacterViewBase GetView(SelectedCharaInfoCharacterViewController.eViewType viewType)
		{
			return this.GetView((int)viewType);
		}

		// Token: 0x060028FF RID: 10495 RVA: 0x000DA1E4 File Offset: 0x000D85E4
		private SelectedCharaInfoCharacterViewBase GetView(int nViewType)
		{
			SelectedCharaInfoCharacterViewController.ViewRequestFlagPair viewRequestFlagPair = this.GetViewRequestFlagPair(nViewType);
			if (viewRequestFlagPair == null)
			{
				return null;
			}
			return viewRequestFlagPair.m_CharaView;
		}

		// Token: 0x06002900 RID: 10496 RVA: 0x000DA207 File Offset: 0x000D8607
		[Obsolete]
		public void RequestLoadModelOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			this.RequestLoadModelOnly(partyMember.GetCharaId(), partyMember.GetWeaponId(), automaticDisplay);
		}

		// Token: 0x06002901 RID: 10497 RVA: 0x000DA21C File Offset: 0x000D861C
		[Obsolete]
		public void RequestLoadModelOnly(long charaMngID, bool automaticDisplay = true)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			UserWeaponData userWeaponData = null;
			int weaponID;
			if (userWeaponData != null)
			{
				weaponID = userWeaponData.Param.WeaponID;
			}
			else
			{
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, userCharaData.Param.ClassType);
				weaponID = weaponParam.WeaponID;
			}
			this.RequestLoadModelOnly(userCharaData.Param.CharaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002902 RID: 10498 RVA: 0x000DA286 File Offset: 0x000D8686
		[Obsolete]
		public void RequestLoadModelOnly(int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Begin, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002903 RID: 10499 RVA: 0x000DA292 File Offset: 0x000D8692
		[Obsolete]
		public void RequestLoadCharacterIllustOnly(CharacterDataController partyMember, bool automaticDisplay = true)
		{
			this.RequestLoadCharacterIllustOnly(partyMember.GetCharaId(), -1, automaticDisplay);
		}

		// Token: 0x06002904 RID: 10500 RVA: 0x000DA2A2 File Offset: 0x000D86A2
		[Obsolete]
		public void RequestLoadCharacterIllustOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			this.RequestLoadCharacterIllustOnly(partyMember.GetCharaId(), partyMember.GetWeaponId(), automaticDisplay);
		}

		// Token: 0x06002905 RID: 10501 RVA: 0x000DA2B8 File Offset: 0x000D86B8
		[Obsolete]
		public void RequestLoadCharacterIllustOnly(long charaMngID, bool automaticDisplay = true)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			UserWeaponData userWeaponData = null;
			int weaponID;
			if (userWeaponData != null)
			{
				weaponID = userWeaponData.Param.WeaponID;
			}
			else
			{
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, userCharaData.Param.ClassType);
				weaponID = weaponParam.WeaponID;
			}
			this.RequestLoadCharacterIllustOnly(userCharaData.Param.CharaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002906 RID: 10502 RVA: 0x000DA322 File Offset: 0x000D8722
		[Obsolete]
		public void RequestLoadCharacterIllustOnly(int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Illust, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002907 RID: 10503 RVA: 0x000DA32E File Offset: 0x000D872E
		[Obsolete]
		public void RequestLoadCharacterBustImageOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			this.RequestLoadCharacterBustImageOnly(partyMember.GetCharaId(), partyMember.GetWeaponId(), automaticDisplay);
		}

		// Token: 0x06002908 RID: 10504 RVA: 0x000DA344 File Offset: 0x000D8744
		[Obsolete]
		public void RequestLoadCharacterBustImageOnly(long charaMngID, bool automaticDisplay = true)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			UserWeaponData userWeaponData = null;
			int weaponID;
			if (userWeaponData != null)
			{
				weaponID = userWeaponData.Param.WeaponID;
			}
			else
			{
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, userCharaData.Param.ClassType);
				weaponID = weaponParam.WeaponID;
			}
			this.RequestLoadCharacterBustImageOnly(userCharaData.Param.CharaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002909 RID: 10505 RVA: 0x000DA3AE File Offset: 0x000D87AE
		[Obsolete]
		public void RequestLoadCharacterBustImageOnly(int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Bust, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x0600290A RID: 10506 RVA: 0x000DA3BA File Offset: 0x000D87BA
		[Obsolete]
		public void RequestLoadCharacterIconOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			this.RequestLoadCharacterIconOnly(partyMember.GetCharaId(), partyMember.GetWeaponId(), automaticDisplay);
		}

		// Token: 0x0600290B RID: 10507 RVA: 0x000DA3D0 File Offset: 0x000D87D0
		[Obsolete]
		public void RequestLoadCharacterIconOnly(long charaMngID, bool automaticDisplay = true)
		{
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			UserWeaponData userWeaponData = null;
			int weaponID;
			if (userWeaponData != null)
			{
				weaponID = userWeaponData.Param.WeaponID;
			}
			else
			{
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, userCharaData.Param.ClassType);
				weaponID = weaponParam.WeaponID;
			}
			this.RequestLoadCharacterIconOnly(userCharaData.Param.CharaID, weaponID, automaticDisplay);
		}

		// Token: 0x0600290C RID: 10508 RVA: 0x000DA43A File Offset: 0x000D883A
		[Obsolete]
		public void RequestLoadCharacterIconOnly(int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Icon, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x0600290D RID: 10509 RVA: 0x000DA446 File Offset: 0x000D8846
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(new SelectedCharaInfoCharacterViewController.eViewType[]
			{
				viewType
			}, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x0600290E RID: 10510 RVA: 0x000DA45C File Offset: 0x000D885C
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
			this.RequestLoadView(new SelectedCharaInfoCharacterViewController.eViewType[]
			{
				viewType
			}, ewcdc, automaticDisplay);
		}

		// Token: 0x0600290F RID: 10511 RVA: 0x000DA470 File Offset: 0x000D8870
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, CharacterDataController cdc, bool automaticDisplay = true)
		{
			this.RequestLoadView(new SelectedCharaInfoCharacterViewController.eViewType[]
			{
				viewType
			}, cdc, automaticDisplay);
		}

		// Token: 0x06002910 RID: 10512 RVA: 0x000DA484 File Offset: 0x000D8884
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, int charaID, int weaponID, bool automaticDisplay = true)
		{
			this.RequestLoadView(viewTypes, null, null, charaID, weaponID, automaticDisplay);
		}

		// Token: 0x06002911 RID: 10513 RVA: 0x000DA493 File Offset: 0x000D8893
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
			this.RequestLoadView(viewTypes, ewcdc, null, -1, -1, automaticDisplay);
		}

		// Token: 0x06002912 RID: 10514 RVA: 0x000DA4A1 File Offset: 0x000D88A1
		public void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, CharacterDataController cdc, bool automaticDisplay = true)
		{
			this.RequestLoadView(viewTypes, null, cdc, -1, -1, automaticDisplay);
		}

		// Token: 0x06002913 RID: 10515 RVA: 0x000DA4B0 File Offset: 0x000D88B0
		protected void RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, EquipWeaponCharacterDataController ewcdc, CharacterDataController cdc, int charaID, int weaponID, bool automaticDisplay = true)
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			this.ResetNextRequestFlags();
			this.m_Ewcdc = ewcdc;
			this.m_Cdc = cdc;
			this.m_CharaID = charaID;
			this.m_WeaponID = weaponID;
			this.m_AutomaticStateAdvance = automaticDisplay;
			for (int i = 0; i < viewTypes.Length; i++)
			{
				SelectedCharaInfoCharacterViewController.ViewRequestFlagPair viewRequestFlagPair = this.GetViewRequestFlagPair(viewTypes[i]);
				if (viewRequestFlagPair != null)
				{
					viewRequestFlagPair.m_NextRequestFlag = true;
				}
			}
			this.PlayOut();
		}

		// Token: 0x06002914 RID: 10516 RVA: 0x000DA530 File Offset: 0x000D8930
		[Obsolete]
		public void RequestLoadAll(bool[] requestFlags, long charaMngID, bool automaticDisplay = true)
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			this.ResetNextRequestFlags();
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				bool nextRequestFlag = false;
				if (requestFlags != null && i < requestFlags.Length)
				{
					nextRequestFlag = requestFlags[i];
				}
				this.m_ViewRequestFlagPairs[i].m_NextRequestFlag = nextRequestFlag;
			}
			this.m_AutomaticStateAdvance = automaticDisplay;
			this.RequestLoadAll();
		}

		// Token: 0x06002915 RID: 10517 RVA: 0x000DA59F File Offset: 0x000D899F
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
			this.ForceRequestLoadView(new SelectedCharaInfoCharacterViewController.eViewType[]
			{
				viewType
			}, ewcdc, automaticDisplay);
		}

		// Token: 0x06002916 RID: 10518 RVA: 0x000DA5B3 File Offset: 0x000D89B3
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType viewType, CharacterDataController cdc, bool automaticDisplay = true)
		{
			this.ForceRequestLoadView(new SelectedCharaInfoCharacterViewController.eViewType[]
			{
				viewType
			}, cdc, automaticDisplay);
		}

		// Token: 0x06002917 RID: 10519 RVA: 0x000DA5C7 File Offset: 0x000D89C7
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, EquipWeaponCharacterDataController ewcdc, bool automaticDisplay = true)
		{
			this.ForceRequestLoadView(viewTypes, ewcdc, null, automaticDisplay);
		}

		// Token: 0x06002918 RID: 10520 RVA: 0x000DA5D3 File Offset: 0x000D89D3
		public void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, CharacterDataController cdc, bool automaticDisplay = true)
		{
			this.ForceRequestLoadView(viewTypes, null, cdc, automaticDisplay);
		}

		// Token: 0x06002919 RID: 10521 RVA: 0x000DA5E0 File Offset: 0x000D89E0
		protected void ForceRequestLoadView(SelectedCharaInfoCharacterViewController.eViewType[] viewTypes, EquipWeaponCharacterDataController ewcdc, CharacterDataController cdc, bool automaticDisplay = true)
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			this.m_Ewcdc = ewcdc;
			this.m_Cdc = cdc;
			for (int i = 0; i < viewTypes.Length; i++)
			{
				SelectedCharaInfoCharacterViewController.ViewRequestFlagPair viewRequestFlagPair = this.GetViewRequestFlagPair(viewTypes[i]);
				if (viewRequestFlagPair != null)
				{
					viewRequestFlagPair.m_NextRequestFlag = true;
					SelectedCharaInfoCharacterViewBase charaView = viewRequestFlagPair.m_CharaView;
					if (charaView != null)
					{
						bool active = false;
						if (ewcdc != null)
						{
							active = true;
							viewRequestFlagPair.m_CharaView.ForceRequestLoad(ewcdc, (!automaticDisplay) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle);
						}
						else if (cdc != null)
						{
							active = true;
							viewRequestFlagPair.m_CharaView.ForceRequestLoad(cdc, (!automaticDisplay) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle);
						}
						charaView.gameObject.SetActive(active);
					}
				}
			}
		}

		// Token: 0x0600291A RID: 10522 RVA: 0x000DA6A4 File Offset: 0x000D8AA4
		protected void RequestLoadAll()
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			if (this.m_Ewcdc != null && this.m_Cdc != null && this.m_CharaID == -1)
			{
				return;
			}
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Wait && this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				SelectedCharaInfoCharacterViewController.ViewRequestFlagPair viewRequestFlagPair = this.GetViewRequestFlagPair(i);
				if (viewRequestFlagPair != null)
				{
					SelectedCharaInfoCharacterViewBase charaView = viewRequestFlagPair.m_CharaView;
					if (charaView != null)
					{
						if (viewRequestFlagPair.m_NextRequestFlag)
						{
							SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = (!this.m_AutomaticStateAdvance) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle;
							if (this.m_Ewcdc != null)
							{
								charaView.RequestLoad(this.m_Ewcdc, toStateAutomaticallyAdvance);
							}
							else if (this.m_Cdc != null)
							{
								charaView.RequestLoad(this.m_Cdc, toStateAutomaticallyAdvance);
							}
							else
							{
								charaView.RequestLoad(this.m_CharaID, this.m_WeaponID, toStateAutomaticallyAdvance);
							}
							charaView.gameObject.SetActive(true);
						}
						else
						{
							charaView.gameObject.SetActive(false);
						}
					}
				}
			}
			this.ResetNextRequestFlags();
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Prepare);
		}

		// Token: 0x0600291B RID: 10523 RVA: 0x000DA7D0 File Offset: 0x000D8BD0
		public void ResetNextRequestFlags()
		{
			if (this.m_ViewRequestFlagPairs == null)
			{
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Invalid);
				return;
			}
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				if (this.m_ViewRequestFlagPairs[i] != null)
				{
					this.m_ViewRequestFlagPairs[i].m_NextRequestFlag = false;
				}
			}
			this.m_Ewcdc = null;
			this.m_Cdc = null;
			this.m_CharaID = -1;
			this.m_WeaponID = -1;
		}

		// Token: 0x0600291C RID: 10524 RVA: 0x000DA840 File Offset: 0x000D8C40
		public void SetLimitBreak(int lb)
		{
			for (int i = 0; i < this.m_ViewRequestFlagPairs.Length; i++)
			{
				if (this.m_ViewRequestFlagPairs[i] != null && this.m_ViewRequestFlagPairs[i].m_CharaView != null)
				{
					this.m_ViewRequestFlagPairs[i].m_CharaView.SetLimitBreak(lb);
				}
			}
		}

		// Token: 0x04002FAD RID: 12205
		[SerializeField]
		private Image m_ViewFrame;

		// Token: 0x04002FAE RID: 12206
		[SerializeField]
		private SelectedCharaInfoModelArea m_ModelArea;

		// Token: 0x04002FAF RID: 12207
		[SerializeField]
		private SelectedCharaInfoCharacterIllust m_CharacterIllust;

		// Token: 0x04002FB0 RID: 12208
		[SerializeField]
		private SelectedCharaInfoCharacterBustImage m_BustImage;

		// Token: 0x04002FB1 RID: 12209
		[SerializeField]
		private SelectedCharaInfoCharacterIcon m_CharacterIcon;

		// Token: 0x04002FB2 RID: 12210
		private int m_CharaID = -1;

		// Token: 0x04002FB3 RID: 12211
		private int m_WeaponID = -1;

		// Token: 0x04002FB4 RID: 12212
		private EquipWeaponCharacterDataController m_Ewcdc;

		// Token: 0x04002FB5 RID: 12213
		private CharacterDataController m_Cdc;

		// Token: 0x04002FB6 RID: 12214
		private SelectedCharaInfoCharacterViewController.ViewRequestFlagPair[] m_ViewRequestFlagPairs;

		// Token: 0x04002FB7 RID: 12215
		private bool m_AutomaticStateAdvance;

		// Token: 0x020007BC RID: 1980
		public enum eViewType
		{
			// Token: 0x04002FB9 RID: 12217
			Begin,
			// Token: 0x04002FBA RID: 12218
			Model = 0,
			// Token: 0x04002FBB RID: 12219
			Illust,
			// Token: 0x04002FBC RID: 12220
			Bust,
			// Token: 0x04002FBD RID: 12221
			Icon,
			// Token: 0x04002FBE RID: 12222
			End
		}

		// Token: 0x020007BD RID: 1981
		private class ViewRequestFlagPair
		{
			// Token: 0x0600291D RID: 10525 RVA: 0x000DA89E File Offset: 0x000D8C9E
			public ViewRequestFlagPair()
			{
				this.m_CharaView = null;
				this.m_NextRequestFlag = false;
			}

			// Token: 0x0600291E RID: 10526 RVA: 0x000DA8B4 File Offset: 0x000D8CB4
			public ViewRequestFlagPair(SelectedCharaInfoCharacterViewBase charaView)
			{
				this.m_CharaView = charaView;
				this.m_NextRequestFlag = false;
			}

			// Token: 0x04002FBF RID: 12223
			public SelectedCharaInfoCharacterViewBase m_CharaView;

			// Token: 0x04002FC0 RID: 12224
			public bool m_NextRequestFlag;
		}
	}
}
