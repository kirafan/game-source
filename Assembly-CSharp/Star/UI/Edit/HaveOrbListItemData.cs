﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000919 RID: 2329
	public class HaveOrbListItemData : ScrollItemData
	{
		// Token: 0x06003055 RID: 12373 RVA: 0x000FB04C File Offset: 0x000F944C
		public HaveOrbListItemData(long mngID, bool isEquip, Action<long> onClick)
		{
			this.m_OrbMngID = mngID;
			this.m_IsEquip = isEquip;
			this.OnClick += onClick;
		}

		// Token: 0x14000077 RID: 119
		// (add) Token: 0x06003056 RID: 12374 RVA: 0x000FB06C File Offset: 0x000F946C
		// (remove) Token: 0x06003057 RID: 12375 RVA: 0x000FB0A4 File Offset: 0x000F94A4
		public event Action<long> OnClick;

		// Token: 0x06003058 RID: 12376 RVA: 0x000FB0DA File Offset: 0x000F94DA
		public override void OnClickItemCallBack()
		{
			base.OnClickItemCallBack();
			this.OnClick.Call(this.m_OrbMngID);
		}

		// Token: 0x04003730 RID: 14128
		public long m_OrbMngID;

		// Token: 0x04003731 RID: 14129
		public bool m_IsEquip;
	}
}
