﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007C2 RID: 1986
	public class SelectedCharaInfoDetailInfoGroup : SelectedCharaInfoDetailInfoGroupBase
	{
		// Token: 0x06002937 RID: 10551 RVA: 0x000DADCC File Offset: 0x000D91CC
		public override void Setup()
		{
			this.Setup(this.m_GroupType);
		}

		// Token: 0x06002938 RID: 10552 RVA: 0x000DADDC File Offset: 0x000D91DC
		protected void Setup(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType)
		{
			if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.None)
			{
				this.m_GroupTypeLabel.SetActive(false);
			}
			else
			{
				this.m_GroupTypeLabel.SetActive(true);
				if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.UniquieSkill)
				{
					this.m_GroupTypeLabel.SetTitle(">>固有スキル.<<");
				}
				else if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.GeneralSkill)
				{
					this.m_GroupTypeLabel.SetTitle(">>汎用スキル.<<");
				}
				else if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.Weapon)
				{
					this.m_GroupTypeLabel.SetTitle(">>装備中のぶき.<<");
				}
				else if (groupType == SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.MasterSkill)
				{
					this.m_GroupTypeLabel.SetTitle(">>マスタースキル.<<");
				}
			}
			for (int i = 0; i < this.m_SkillInfos.Length; i++)
			{
				this.m_SkillInfos[i].Setup();
			}
		}

		// Token: 0x06002939 RID: 10553 RVA: 0x000DAEA0 File Offset: 0x000D92A0
		public override void Destroy()
		{
			if (this.m_SkillInfos != null)
			{
				for (int i = 0; i < this.m_SkillInfos.Length; i++)
				{
					this.m_SkillInfos[i].Destroy();
					this.m_SkillInfos[i] = null;
				}
				this.m_SkillInfos = null;
			}
		}

		// Token: 0x0600293A RID: 10554 RVA: 0x000DAEEE File Offset: 0x000D92EE
		public override void ApplySkill(int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			if (this.m_SkillInfos != null)
			{
				this.m_SkillInfos[index].Apply(argument);
			}
		}

		// Token: 0x0600293B RID: 10555 RVA: 0x000DAF09 File Offset: 0x000D9309
		public override SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType GetGroupType()
		{
			return this.m_GroupType;
		}

		// Token: 0x04002FCC RID: 12236
		[SerializeField]
		[Tooltip("固有か汎用か.又は種類表示をしないか.")]
		protected SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType m_GroupType;

		// Token: 0x04002FCD RID: 12237
		[SerializeField]
		[Tooltip("種類グループに該当するスキル一覧.")]
		private SelectedCharaInfoDetailInfoBase[] m_SkillInfos;
	}
}
