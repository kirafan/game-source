﻿using System;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008EE RID: 2286
	public abstract class CharaEditSceneUI<MaterialPanelType, SimulateParamType> : CharaEditSceneUIBase where MaterialPanelType : CharaEditSceneMaterialPanel where SimulateParamType : EditUtility.CharacterEditSimulateParam
	{
		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06002F52 RID: 12114 RVA: 0x000F7A36 File Offset: 0x000F5E36
		public CharaEditSceneUIBase.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06002F53 RID: 12115 RVA: 0x000F7A3E File Offset: 0x000F5E3E
		public long CharaMngID
		{
			get
			{
				if (this.m_CharacterDataController == null)
				{
					return -1L;
				}
				return this.m_CharacterDataController.GetCharaMngId();
			}
		}

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06002F54 RID: 12116 RVA: 0x000F7A59 File Offset: 0x000F5E59
		public int SelectButtonIdx
		{
			get
			{
				if (this.m_MaterialPanel == null)
				{
					return -1;
				}
				return this.m_MaterialPanel.GetSelectButtonIdx();
			}
		}

		// Token: 0x14000075 RID: 117
		// (add) Token: 0x06002F55 RID: 12117 RVA: 0x000F7A84 File Offset: 0x000F5E84
		// (remove) Token: 0x06002F56 RID: 12118 RVA: 0x000F7ABC File Offset: 0x000F5EBC
		public event Action OnClickButton;

		// Token: 0x06002F57 RID: 12119 RVA: 0x000F7AF2 File Offset: 0x000F5EF2
		private void Start()
		{
		}

		// Token: 0x06002F58 RID: 12120 RVA: 0x000F7AF4 File Offset: 0x000F5EF4
		public void Setup(long charaMngID)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				return;
			}
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngID);
			if (userCharaData == null)
			{
				return;
			}
			this.Setup(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharaData, null));
		}

		// Token: 0x06002F59 RID: 12121 RVA: 0x000F7B50 File Offset: 0x000F5F50
		public virtual void Setup(EquipWeaponCharacterDataController cdc)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				return;
			}
			if (cdc == null)
			{
				return;
			}
			this.m_CharacterDataController = cdc;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			this.SetupCharacterInformationPanel();
			this.SetupMaterialPanel();
			this.Refresh();
			this.m_LayoutRebuildFrames = 240;
		}

		// Token: 0x06002F5A RID: 12122 RVA: 0x000F7BB8 File Offset: 0x000F5FB8
		private void SetupCharacterInformationPanel()
		{
			this.m_CharaInfoPanel.Setup();
			this.m_CharaInfoPanel.OnDirtyResource();
		}

		// Token: 0x06002F5B RID: 12123 RVA: 0x000F7BD0 File Offset: 0x000F5FD0
		private void SetupMaterialPanel()
		{
			this.m_MaterialPanel.Setup(this.m_CharacterDataController);
			this.m_MaterialPanel.OnMaterialsChanged += this.OnMaterialsChanged;
			this.m_MaterialPanel.onClickButton += this.OnClickDecideButtonCallback;
		}

		// Token: 0x06002F5C RID: 12124 RVA: 0x000F7C2E File Offset: 0x000F602E
		protected void Refresh()
		{
			this.Apply(this.m_CharacterDataController);
		}

		// Token: 0x06002F5D RID: 12125 RVA: 0x000F7C3C File Offset: 0x000F603C
		protected virtual void Apply(EquipWeaponCharacterDataController cdc)
		{
			SimulateParamType param = this.Simulate(cdc);
			CharacterDataController after = this.ConvertSimulateParamToAfterCharacterData(param);
			this.ApplyCharacterInformation(cdc, after);
			this.ApplyMaterialPanel(param);
		}

		// Token: 0x06002F5E RID: 12126 RVA: 0x000F7C68 File Offset: 0x000F6068
		protected virtual SimulateParamType Simulate(EquipWeaponCharacterDataController cdc)
		{
			return (SimulateParamType)((object)null);
		}

		// Token: 0x06002F5F RID: 12127 RVA: 0x000F7C70 File Offset: 0x000F6070
		protected virtual CharacterDataController ConvertSimulateParamToAfterCharacterData(SimulateParamType param)
		{
			return null;
		}

		// Token: 0x06002F60 RID: 12128 RVA: 0x000F7C73 File Offset: 0x000F6073
		protected void ApplyCharacterInformation(EquipWeaponCharacterDataController cdc, CharacterDataController after)
		{
			this.m_CharaInfoPanel.OnDirtyParameter();
			this.m_CharaInfoPanel.Apply(cdc, after);
		}

		// Token: 0x06002F61 RID: 12129 RVA: 0x000F7C8D File Offset: 0x000F608D
		protected virtual void ApplyMaterialPanel(SimulateParamType param)
		{
		}

		// Token: 0x06002F62 RID: 12130 RVA: 0x000F7C90 File Offset: 0x000F6090
		protected virtual void Update()
		{
			if (0 < this.m_LayoutRebuildFrames)
			{
				this.m_LayoutRebuildFrames--;
				LayoutRebuilder.MarkLayoutForRebuild(this.m_MaterialPanel.transform as RectTransform);
			}
			switch (this.m_Step)
			{
			case CharaEditSceneUIBase.eStep.InPrepare:
				this.PlayInExec();
				break;
			case CharaEditSceneUIBase.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				for (int j = 0; j < this.m_UIGroups.Length; j++)
				{
					if (!this.m_UIGroups[j].IsDonePlayIn)
					{
						flag = false;
						break;
					}
				}
				if (this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Prepare || this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.In)
				{
					flag = false;
				}
				if (flag)
				{
					this.ChangeStep(CharaEditSceneUIBase.eStep.Idle);
				}
				break;
			}
			case CharaEditSceneUIBase.eStep.Idle:
				if (this.m_IsInputBlock)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_IsInputBlock = false;
				}
				break;
			case CharaEditSceneUIBase.eStep.CharaOut:
				if (this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Idle)
				{
					this.m_CharaInfoPanel.PlayOutCharacterView();
				}
				if (this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Invalid || this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Wait || this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Prepared)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SuspendCharaHndlForMenu();
					for (int k = 0; k < this.m_AnimUIPlayerArray.Length; k++)
					{
						this.m_AnimUIPlayerArray[k].PlayOut();
					}
					for (int l = 0; l < this.m_UIGroups.Length; l++)
					{
						this.m_UIGroups[l].Close();
					}
					this.ChangeStep(CharaEditSceneUIBase.eStep.Out);
				}
				break;
			case CharaEditSceneUIBase.eStep.Out:
			{
				bool flag2 = true;
				for (int m = 0; m < this.m_AnimUIPlayerArray.Length; m++)
				{
					if (this.m_AnimUIPlayerArray[m].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				for (int n = 0; n < this.m_UIGroups.Length; n++)
				{
					if (!this.m_UIGroups[n].IsDonePlayOut)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(CharaEditSceneUIBase.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06002F63 RID: 12131 RVA: 0x000F7F20 File Offset: 0x000F6320
		private void ChangeStep(CharaEditSceneUIBase.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case CharaEditSceneUIBase.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case CharaEditSceneUIBase.eStep.InPrepare:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaEditSceneUIBase.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaEditSceneUIBase.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case CharaEditSceneUIBase.eStep.CharaOut:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaEditSceneUIBase.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaEditSceneUIBase.eStep.End:
				this.SetEnableInput(false);
				this.m_CharaInfoPanel.Destroy();
				SingletonMonoBehaviour<GameSystem>.Inst.DestroyCharaHndlForMenu();
				break;
			}
		}

		// Token: 0x06002F64 RID: 12132 RVA: 0x000F8013 File Offset: 0x000F6413
		public override void PlayIn()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			this.m_IsInputBlock = true;
			this.ChangeStep(CharaEditSceneUIBase.eStep.InPrepare);
		}

		// Token: 0x06002F65 RID: 12133 RVA: 0x000F803C File Offset: 0x000F643C
		private void PlayInExec()
		{
			this.ChangeStep(CharaEditSceneUIBase.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			for (int j = 0; j < this.m_UIGroups.Length; j++)
			{
				this.m_UIGroups[j].Open();
			}
		}

		// Token: 0x06002F66 RID: 12134 RVA: 0x000F809C File Offset: 0x000F649C
		public override void PlayOut()
		{
			this.ChangeStep(CharaEditSceneUIBase.eStep.CharaOut);
			this.m_CharaInfoPanel.PlayOutCharacterView();
		}

		// Token: 0x06002F67 RID: 12135 RVA: 0x000F80B0 File Offset: 0x000F64B0
		protected virtual eText_MessageDB GetCheckWindowTitle()
		{
			return eText_MessageDB.CharacterUpgradeCheckWindowTitle;
		}

		// Token: 0x06002F68 RID: 12136 RVA: 0x000F80B7 File Offset: 0x000F64B7
		protected virtual eText_MessageDB GetCheckWindowMessage()
		{
			return eText_MessageDB.CharacterUpgradeCheckWindowMessage;
		}

		// Token: 0x06002F69 RID: 12137 RVA: 0x000F80BE File Offset: 0x000F64BE
		public override bool IsMenuEnd()
		{
			return this.m_Step == CharaEditSceneUIBase.eStep.End;
		}

		// Token: 0x06002F6A RID: 12138 RVA: 0x000F80C9 File Offset: 0x000F64C9
		protected void OnClickButtonProcess(CharaEditSceneUIBase.eButton button)
		{
			this.m_SelectButton = button;
			this.OnClickButtonEventCall();
		}

		// Token: 0x06002F6B RID: 12139 RVA: 0x000F80D8 File Offset: 0x000F64D8
		private void OnClickDecideButtonCallback(CharaEditSceneUIBase.eButton button)
		{
			if (button == CharaEditSceneUIBase.eButton.Decide)
			{
				this.OnClickDecideButtonCallback();
			}
		}

		// Token: 0x06002F6C RID: 12140 RVA: 0x000F80E6 File Offset: 0x000F64E6
		private void OnClickDecideButtonCallback()
		{
			this.OnClickDecideButtonCallback(this.SelectButtonIdx);
		}

		// Token: 0x06002F6D RID: 12141 RVA: 0x000F80F4 File Offset: 0x000F64F4
		protected virtual void OnClickDecideButtonCallback(int idx)
		{
			if (this.m_Step != CharaEditSceneUIBase.eStep.Idle)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(this.GetCheckWindowTitle()), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(this.GetCheckWindowMessage()), null, new Action<int>(this.OnClickDecideButtonCallbackProcess));
		}

		// Token: 0x06002F6E RID: 12142 RVA: 0x000F8155 File Offset: 0x000F6555
		private void OnClickDecideButtonCallbackProcess(int idx)
		{
			if (idx == 0)
			{
				this.OnClickButtonProcess(CharaEditSceneUIBase.eButton.Decide);
			}
			else
			{
				this.OnClickDecideButtonCallbackProcessNoProcess();
			}
		}

		// Token: 0x06002F6F RID: 12143 RVA: 0x000F816F File Offset: 0x000F656F
		private void OnClickDecideButtonCallbackProcessNoProcess()
		{
		}

		// Token: 0x06002F70 RID: 12144 RVA: 0x000F8171 File Offset: 0x000F6571
		private void OnMaterialsChanged()
		{
			this.Refresh();
		}

		// Token: 0x06002F71 RID: 12145 RVA: 0x000F8179 File Offset: 0x000F6579
		private void OnClickButtonEventCall()
		{
			this.OnClickButton.Call();
		}

		// Token: 0x04003694 RID: 13972
		[SerializeField]
		[Tooltip("廃止予定.")]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003695 RID: 13973
		[SerializeField]
		private UIGroup[] m_UIGroups;

		// Token: 0x04003696 RID: 13974
		[SerializeField]
		[Tooltip("パネル左側.骨組み共通化部.")]
		protected CharacterEditSceneCharacterInformationPanel m_CharaInfoPanel;

		// Token: 0x04003697 RID: 13975
		[SerializeField]
		[Tooltip("パネル右側.シーン依存部.")]
		protected MaterialPanelType m_MaterialPanel;

		// Token: 0x04003698 RID: 13976
		protected CharaEditSceneUIBase.eStep m_Step;

		// Token: 0x04003699 RID: 13977
		protected EquipWeaponCharacterDataController m_CharacterDataController;

		// Token: 0x0400369A RID: 13978
		protected CharaEditSceneUIBase.eButton m_SelectButton = CharaEditSceneUIBase.eButton.None;

		// Token: 0x0400369B RID: 13979
		private bool m_IsInputBlock;

		// Token: 0x0400369C RID: 13980
		private int m_LayoutRebuildFrames;
	}
}
