﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007E2 RID: 2018
	public class SelectedCharaInfoState : MonoBehaviour
	{
		// Token: 0x0600298C RID: 10636 RVA: 0x000DBF98 File Offset: 0x000DA398
		public void Setup()
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.Setup();
			}
			if (this.m_LevelInfo != null)
			{
				this.m_LevelInfo.Setup();
			}
			if (this.m_AfterLevelInfo != null)
			{
				this.m_AfterLevelInfo.Setup();
			}
			if (this.m_ExpGaugeWrapper != null)
			{
				this.m_ExpGaugeWrapper.Setup();
				if (this.m_LevelState != null)
				{
					this.m_ExpGaugeWrapper.SetDestination(this.m_LevelState.GetAfterLevelText());
				}
				if (this.m_AfterLevelInfo != null)
				{
					this.m_ExpGaugeWrapper.SetDestination(this.m_AfterLevelInfo.GetLevelText());
				}
			}
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.Setup();
				if (this.m_LevelState != null)
				{
					this.m_ExpGauge.SetLevelText(this.m_LevelState.GetAfterLevelText());
				}
				if (this.m_AfterLevelInfo != null)
				{
					this.m_ExpGauge.SetLevelText(this.m_AfterLevelInfo.GetLevelText());
				}
			}
			if (this.m_WeaponIconCloner != null && this.m_WeaponIconCloner.IsExistPrefab())
			{
				this.m_WeaponIconCloner.Setup(false);
				this.m_WeaponIconComponent = this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>();
			}
		}

		// Token: 0x0600298D RID: 10637 RVA: 0x000DC10B File Offset: 0x000DA50B
		public void Destroy()
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Destroy();
			}
		}

		// Token: 0x0600298E RID: 10638 RVA: 0x000DC129 File Offset: 0x000DA529
		public void SetBeforeLevel(int nowLevel, int maxLevel, SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = null)
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.SetBeforeLevel(nowLevel, maxLevel);
			}
			if (this.m_LevelInfo != null)
			{
				this.m_LevelInfo.SetLevel(nowLevel, maxLevel, info);
			}
		}

		// Token: 0x0600298F RID: 10639 RVA: 0x000DC168 File Offset: 0x000DA568
		public void SetAfterLevel(int nowLevel, int maxLevel, SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = null)
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.SetAfterLevel(nowLevel, maxLevel);
			}
			if (this.m_AfterLevelInfo != null)
			{
				this.m_AfterLevelInfo.SetLevel(nowLevel, maxLevel, info);
			}
		}

		// Token: 0x06002990 RID: 10640 RVA: 0x000DC1A7 File Offset: 0x000DA5A7
		public void PlayAfterLevelAnimation()
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.PlayAfterLevelAnimation();
			}
			if (this.m_AfterLevelInfo != null)
			{
				this.m_AfterLevelInfo.PlayAnimation();
			}
		}

		// Token: 0x06002991 RID: 10641 RVA: 0x000DC1E1 File Offset: 0x000DA5E1
		public int GetAfterLevelAnimationState()
		{
			if (this.m_LevelState != null)
			{
				return this.m_LevelState.GetAfterLevelAnimationState();
			}
			if (this.m_AfterLevelInfo != null)
			{
				return this.m_AfterLevelInfo.GetAnimationState();
			}
			return -1;
		}

		// Token: 0x06002992 RID: 10642 RVA: 0x000DC21E File Offset: 0x000DA61E
		public void SetBeforeLimitBreakIconValue(int value)
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.SetBeforeLimitBreakIconValue(value);
			}
			if (this.m_LevelInfo != null)
			{
				this.m_LevelInfo.SetLimitBreakIconValue(value);
			}
		}

		// Token: 0x06002993 RID: 10643 RVA: 0x000DC25A File Offset: 0x000DA65A
		public void SetAfterLimitBreakIconValue(int value)
		{
			if (this.m_LevelState != null)
			{
				this.m_LevelState.SetAfterLimitBreakIconValue(value);
			}
			if (this.m_AfterLevelInfo != null)
			{
				this.m_AfterLevelInfo.SetLimitBreakIconValue(value);
			}
		}

		// Token: 0x06002994 RID: 10644 RVA: 0x000DC298 File Offset: 0x000DA698
		public void SetExpData(CharacterDataController cdc, bool appliedFriendship = true, bool appliedTownBuff = true)
		{
			if (cdc == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			EditUtility.OutputCharaParam param = EditUtility.CalcCharaParamNoWeapon(cdc, appliedFriendship, appliedTownBuff);
			this.SetExpData(cdc, param);
		}

		// Token: 0x06002995 RID: 10645 RVA: 0x000DC2C7 File Offset: 0x000DA6C7
		public void SetExpData(CharacterDataController cdc, CharacterDataController after)
		{
			this.SetExpData(after.GetCharaLv(), after.GetCharaNowExp(), after.GetCharaMaxLv(), after.GetCharaNowMaxExp());
		}

		// Token: 0x06002996 RID: 10646 RVA: 0x000DC2E7 File Offset: 0x000DA6E7
		public void SetExpData(CharacterDataController partyMember, EditUtility.OutputCharaParam param)
		{
			this.SetExpData(partyMember.GetCharaMaxLv(), new EditUtility.OutputCharaParam?(param));
		}

		// Token: 0x06002997 RID: 10647 RVA: 0x000DC2FC File Offset: 0x000DA6FC
		public void SetExpData(int maxLevel, EditUtility.OutputCharaParam? param)
		{
			this.SetExpData(param.Value.Lv, param.Value.NowExp, maxLevel, param.Value.NextMaxExp);
		}

		// Token: 0x06002998 RID: 10648 RVA: 0x000DC340 File Offset: 0x000DA740
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
			if (this.m_ExpGaugeWrapper != null)
			{
				this.m_ExpGaugeWrapper.SetExpData(level, nowexp, maxLevel, needExp);
			}
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.SetExpData(level, nowexp, maxLevel, needExp);
			}
		}

		// Token: 0x06002999 RID: 10649 RVA: 0x000DC390 File Offset: 0x000DA790
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			if (this.m_ExpGaugeWrapper != null)
			{
				this.m_ExpGaugeWrapper.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
			}
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
			}
		}

		// Token: 0x0600299A RID: 10650 RVA: 0x000DC3EB File Offset: 0x000DA7EB
		public void PlayExpGauge()
		{
			if (this.m_ExpGaugeWrapper != null)
			{
				this.m_ExpGaugeWrapper.PlayExpGauge();
			}
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.Play(true);
			}
		}

		// Token: 0x0600299B RID: 10651 RVA: 0x000DC426 File Offset: 0x000DA826
		public bool IsPlayingGauge()
		{
			if (this.m_ExpGaugeWrapper != null)
			{
				return this.m_ExpGaugeWrapper.IsPlayingExpGauge();
			}
			return this.m_ExpGauge != null && this.m_ExpGauge.IsPlaying;
		}

		// Token: 0x0600299C RID: 10652 RVA: 0x000DC463 File Offset: 0x000DA863
		public void SkipExpGaugePlaying()
		{
			if (this.m_ExpGaugeWrapper != null)
			{
				this.m_ExpGaugeWrapper.SkipExpGaugePlaying();
			}
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.SkipUpdateExp();
			}
		}

		// Token: 0x0600299D RID: 10653 RVA: 0x000DC49D File Offset: 0x000DA89D
		public void SetSimulateExpGauge(long exp, long needExp, int addLv)
		{
			if (this.m_ExpGauge != null)
			{
				this.m_ExpGauge.SetSimulateGauge(exp, needExp, addLv);
			}
		}

		// Token: 0x0600299E RID: 10654 RVA: 0x000DC4BE File Offset: 0x000DA8BE
		public void SetWeaponIcon(int weaponId)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Apply(weaponId);
			}
		}

		// Token: 0x0600299F RID: 10655 RVA: 0x000DC4DD File Offset: 0x000DA8DD
		public void SetWeaponIcon(UserWeaponData weaponData)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Apply(weaponData);
			}
		}

		// Token: 0x060029A0 RID: 10656 RVA: 0x000DC4FC File Offset: 0x000DA8FC
		public void SetWeaponIcon(EquipWeaponCharacterDataController partyMember)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Apply(partyMember);
			}
		}

		// Token: 0x060029A1 RID: 10657 RVA: 0x000DC51B File Offset: 0x000DA91B
		public void SetWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.SetMode(mode);
			}
		}

		// Token: 0x060029A2 RID: 10658 RVA: 0x000DC53A File Offset: 0x000DA93A
		public void SetWeaponIconRareStarActive(bool active)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.GetComponentInChildren<RareStar>().gameObject.SetActive(active);
			}
		}

		// Token: 0x060029A3 RID: 10659 RVA: 0x000DC563 File Offset: 0x000DA963
		public void SetCost(int cost)
		{
			if (this.m_CostText != null)
			{
				this.m_CostText.text = cost.ToString();
			}
		}

		// Token: 0x060029A4 RID: 10660 RVA: 0x000DC590 File Offset: 0x000DA990
		public void ApplyState(EquipWeaponCharacterDataController managedBattlePartyMember, bool appliedFriendship = true, bool appliedWeapon = true)
		{
			if (managedBattlePartyMember == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			EditUtility.OutputCharaParam beforeParam = EditUtility.CalcCharaParam(managedBattlePartyMember, appliedFriendship, appliedWeapon, true);
			this.ApplyState(beforeParam);
		}

		// Token: 0x060029A5 RID: 10661 RVA: 0x000DC5BF File Offset: 0x000DA9BF
		public void ApplyState(long charaMngID)
		{
			this.ApplyState(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID), null);
		}

		// Token: 0x060029A6 RID: 10662 RVA: 0x000DC5D8 File Offset: 0x000DA9D8
		public void ApplyState(UserCharacterData userCharaData, UserWeaponData userWeaponData = null)
		{
			if (userCharaData == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			EditUtility.OutputCharaParam beforeParam = EditUtility.CalcCharaParam(userCharaData, userWeaponData);
			this.ApplyState(beforeParam);
		}

		// Token: 0x060029A7 RID: 10663 RVA: 0x000DC605 File Offset: 0x000DAA05
		public void ApplyState(UserSupportDataWrapper userSupportDataWrapper)
		{
			this.ApplyState(EditUtility.CalcCharaParam(userSupportDataWrapper.GetData()));
		}

		// Token: 0x060029A8 RID: 10664 RVA: 0x000DC618 File Offset: 0x000DAA18
		public void ApplyState(EditUtility.OutputCharaParam beforeParam)
		{
			this.ApplyState(new EditUtility.OutputCharaParam?(beforeParam), null);
		}

		// Token: 0x060029A9 RID: 10665 RVA: 0x000DC63A File Offset: 0x000DAA3A
		public void ApplyState(EquipWeaponCharacterDataController cdc, EquipWeaponCharacterDataController after)
		{
			this.ApplyState(cdc, new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParam(after, true, true, true)));
		}

		// Token: 0x060029AA RID: 10666 RVA: 0x000DC651 File Offset: 0x000DAA51
		public void ApplyState(EquipWeaponCharacterDataController cdc, EditUtility.OutputCharaParam? afterParam)
		{
			this.ApplyState(new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParam(cdc, true, true, true)), afterParam);
		}

		// Token: 0x060029AB RID: 10667 RVA: 0x000DC668 File Offset: 0x000DAA68
		public void ApplyState(CharacterDataController cdc, CharacterDataController after)
		{
			this.ApplyState(new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParamLevelOnly(cdc)), new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParamLevelOnly(after)));
		}

		// Token: 0x060029AC RID: 10668 RVA: 0x000DC688 File Offset: 0x000DAA88
		public void ApplyState(EditUtility.OutputCharaParam? beforeParam, EditUtility.OutputCharaParam? afterParam = null)
		{
			int[] adds = null;
			int[] states = new int[]
			{
				beforeParam.Value.Hp,
				beforeParam.Value.Atk,
				beforeParam.Value.Mgc,
				beforeParam.Value.Def,
				beforeParam.Value.MDef,
				beforeParam.Value.Spd
			};
			if (afterParam != null)
			{
				adds = new int[]
				{
					afterParam.Value.Hp - beforeParam.Value.Hp,
					afterParam.Value.Atk - beforeParam.Value.Atk,
					afterParam.Value.Mgc - beforeParam.Value.Mgc,
					afterParam.Value.Def - beforeParam.Value.Def,
					afterParam.Value.MDef - beforeParam.Value.MDef,
					afterParam.Value.Spd - beforeParam.Value.Spd
				};
			}
			this.ApplyState(states, adds);
		}

		// Token: 0x060029AD RID: 10669 RVA: 0x000DC803 File Offset: 0x000DAC03
		public void ApplyState(int[] states, int[] adds = null)
		{
			if (this.m_StatusDisplay != null)
			{
				this.m_StatusDisplay.Apply(states, adds);
			}
		}

		// Token: 0x060029AE RID: 10670 RVA: 0x000DC823 File Offset: 0x000DAC23
		public void ApplyStateBeforeAfterFromPartyMember(EquipWeaponCharacterDataController partyMember)
		{
			this.ApplyState(new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParam(partyMember, false, false, false)), new EditUtility.OutputCharaParam?(EditUtility.CalcCharaParam(partyMember, true, true, true)));
		}

		// Token: 0x060029AF RID: 10671 RVA: 0x000DC848 File Offset: 0x000DAC48
		public void ApplyState(EquipWeaponCharacterDataController partyMember)
		{
			CharaStatus[] array = new CharaStatus[2];
			if (this.m_StatusDisplayAfter != null)
			{
				array[1] = this.m_StatusDisplayAfter;
				if (this.m_StatusDisplay != null)
				{
					array[0] = this.m_StatusDisplay;
				}
			}
			else if (this.m_StatusDisplay != null)
			{
				array[1] = this.m_StatusDisplay;
			}
			if (array[0] != null)
			{
				array[0].Apply(EditUtility.CalcCharaParam(partyMember, false, false, false).ToIntsForCharaStatus(), null);
			}
			if (array[1] != null)
			{
				array[1].Apply(EditUtility.CalcCharaParam(partyMember, true, true, true).ToIntsForCharaStatus(), null);
			}
			if (this.m_StatusFriendship != null)
			{
				this.m_StatusFriendship.Apply(EditUtility.GetFriendshipIncreaseRate(partyMember).ToFloatsForCharaStatusPercent());
			}
			if (this.m_StatusDisplayTown != null)
			{
				this.m_StatusDisplayTown.Apply(EditUtility.GetTownBuffIncreaseRate(partyMember).ToFloatsForCharaStatusPercent());
			}
			if (this.m_StatusDisplayWeapon != null)
			{
				this.m_StatusDisplayWeapon.Apply(EditUtility.CalcWeaponParam(partyMember).ToIntsForCharaStatus(), null);
			}
		}

		// Token: 0x060029B0 RID: 10672 RVA: 0x000DC980 File Offset: 0x000DAD80
		public void SetHaveTitleFieldTitle(int index, string title)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetTitle(title);
			}
		}

		// Token: 0x060029B1 RID: 10673 RVA: 0x000DC9D0 File Offset: 0x000DADD0
		public void SetHaveTitleFieldText(int index, string text)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetText(text);
			}
		}

		// Token: 0x060029B2 RID: 10674 RVA: 0x000DCA20 File Offset: 0x000DAE20
		public void SetHaveTitleFieldTitleActive(int index, bool active)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetTitleActive(active);
			}
		}

		// Token: 0x0400301D RID: 12317
		[SerializeField]
		[Tooltip("非推奨.LevelInfoを使用して下さい.")]
		private SelectedCharaInfoLevelData m_LevelState;

		// Token: 0x0400301E RID: 12318
		[SerializeField]
		[Tooltip("現在のレベル情報.")]
		private SelectedCharaInfoLevelInfo m_LevelInfo;

		// Token: 0x0400301F RID: 12319
		[SerializeField]
		[Tooltip("強化等を行った後のレベル情報.")]
		private SelectedCharaInfoLevelInfo m_AfterLevelInfo;

		// Token: 0x04003020 RID: 12320
		[SerializeField]
		[Tooltip("非推奨.ExpGaugeを使用して下さい.")]
		private ExpGaugeWrapper m_ExpGaugeWrapper;

		// Token: 0x04003021 RID: 12321
		[SerializeField]
		[Tooltip("経験値ゲージ.")]
		private ExpGauge m_ExpGauge;

		// Token: 0x04003022 RID: 12322
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x04003023 RID: 12323
		private WeaponIconWithFrame m_WeaponIconComponent;

		// Token: 0x04003024 RID: 12324
		[SerializeField]
		private Text m_CostText;

		// Token: 0x04003025 RID: 12325
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x04003026 RID: 12326
		[SerializeField]
		private CharaStatus m_StatusDisplayAfter;

		// Token: 0x04003027 RID: 12327
		[SerializeField]
		private CharaStatusPercent m_StatusFriendship;

		// Token: 0x04003028 RID: 12328
		[SerializeField]
		private CharaStatusPercent m_StatusDisplayTown;

		// Token: 0x04003029 RID: 12329
		[SerializeField]
		private CharaStatus m_StatusDisplayWeapon;

		// Token: 0x0400302A RID: 12330
		[SerializeField]
		[Tooltip("汎用テキスト出力領域.")]
		private SelectedCharaInfoHaveTitleTextField[] m_HaveTitleTextFields;
	}
}
