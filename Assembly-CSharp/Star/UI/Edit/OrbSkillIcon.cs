﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200091D RID: 2333
	public class OrbSkillIcon : MonoBehaviour
	{
		// Token: 0x06003067 RID: 12391 RVA: 0x000FB73C File Offset: 0x000F9B3C
		public void Apply(int skillID, bool learned, int learnLv = 0)
		{
			SkillListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
			this.m_SkillName.text = param.m_SkillName;
			this.m_SkillNotLearned.SetActive(!learned);
			if (!learned)
			{
				this.m_LearnLvText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MasterEquipSkillLimit, new object[]
				{
					learnLv
				});
			}
			param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
			if (this.m_DetailText != null)
			{
				this.m_DetailText.text = param.m_SkillDetail;
			}
		}

		// Token: 0x06003068 RID: 12392 RVA: 0x000FB7ED File Offset: 0x000F9BED
		public void Display(bool active)
		{
			this.m_DisplayParent.SetActive(active);
		}

		// Token: 0x04003747 RID: 14151
		[SerializeField]
		private Text m_SkillName;

		// Token: 0x04003748 RID: 14152
		[SerializeField]
		private GameObject m_SkillNotLearned;

		// Token: 0x04003749 RID: 14153
		[SerializeField]
		private Text m_LearnLvText;

		// Token: 0x0400374A RID: 14154
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400374B RID: 14155
		[SerializeField]
		[Tooltip("描画全体を切るための親.")]
		private GameObject m_DisplayParent;
	}
}
