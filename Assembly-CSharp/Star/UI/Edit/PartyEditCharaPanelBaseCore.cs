﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200093A RID: 2362
	public class PartyEditCharaPanelBaseCore : PartyCharaPanelBaseCore
	{
		// Token: 0x060030E5 RID: 12517 RVA: 0x000FEDA4 File Offset: 0x000FD1A4
		protected override void ApplyProcess(EquipWeaponPartyMemberController partyMemberController)
		{
			if (this.m_SharedIntance == null)
			{
				return;
			}
			base.ApplyProcess(partyMemberController);
			int rare = (int)((this.m_SharedIntance.partyMemberController == null) ? eRare.None : ((!this.m_SharedIntance.partyMemberController.IsExistCharacter()) ? eRare.None : (this.m_SharedIntance.partyMemberController.GetRarity() + 1)));
			this.ApplyFrameImage(rare);
		}

		// Token: 0x060030E6 RID: 12518 RVA: 0x000FEE10 File Offset: 0x000FD210
		protected void ApplyFrameImage(int rare)
		{
			PartyEditCharaPanelBase partyEditCharaPanelBase = (PartyEditCharaPanelBase)this.m_SharedIntance.parent;
			if (partyEditCharaPanelBase == null)
			{
				return;
			}
			Sprite[] rareFrameSprites = partyEditCharaPanelBase.GetRareFrameSprites();
			if (rareFrameSprites == null || rareFrameSprites.Length <= 0)
			{
				return;
			}
			if (rareFrameSprites.Length - 1 < rare)
			{
				rare = ((PartyEditCharaPanelBase)this.m_SharedIntance.parent).GetRareFrameSprites().Length - 1;
			}
			if (rare < 0)
			{
				rare = 0;
			}
			partyEditCharaPanelBase.GetFrame().sprite = rareFrameSprites[rare];
		}
	}
}
