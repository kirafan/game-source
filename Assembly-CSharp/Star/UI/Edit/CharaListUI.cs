﻿using System;
using Star.UI.CharaList;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008E6 RID: 2278
	public class CharaListUI : MenuUIBase
	{
		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06002F2F RID: 12079 RVA: 0x000F74E6 File Offset: 0x000F58E6
		public CharaListUI.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06002F30 RID: 12080 RVA: 0x000F74EE File Offset: 0x000F58EE
		public long SelectCharaMngID
		{
			get
			{
				return this.m_SelectCharaMngID;
			}
		}

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06002F31 RID: 12081 RVA: 0x000F74F6 File Offset: 0x000F58F6
		public int PartyIndex
		{
			get
			{
				return this.m_PartyIndex;
			}
		}

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06002F32 RID: 12082 RVA: 0x000F74FE File Offset: 0x000F58FE
		public int MemberIndex
		{
			get
			{
				return this.m_MemberIndex;
			}
		}

		// Token: 0x06002F33 RID: 12083 RVA: 0x000F7506 File Offset: 0x000F5906
		private void Start()
		{
		}

		// Token: 0x06002F34 RID: 12084 RVA: 0x000F7508 File Offset: 0x000F5908
		public void SetViewMode()
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = -1;
			this.m_MemberIndex = -1;
			this.m_Scroll.SetViewMode();
		}

		// Token: 0x06002F35 RID: 12085 RVA: 0x000F7532 File Offset: 0x000F5932
		public void SetPartyEditMode(int partyIndex, int memberIndex)
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = partyIndex;
			this.m_MemberIndex = memberIndex;
			this.m_Scroll.SetPartyEditMode(partyIndex, memberIndex);
		}

		// Token: 0x06002F36 RID: 12086 RVA: 0x000F755E File Offset: 0x000F595E
		public void SetSupportEditMode(int supportPartyIndex, int supportMemberIndex)
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = supportPartyIndex;
			this.m_MemberIndex = supportMemberIndex;
			this.m_Scroll.SetSupportEditMode(supportPartyIndex, supportMemberIndex);
		}

		// Token: 0x06002F37 RID: 12087 RVA: 0x000F758A File Offset: 0x000F598A
		public void SetUpgradeMode()
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = -1;
			this.m_MemberIndex = -1;
			this.m_Scroll.SetUpgradeMode();
		}

		// Token: 0x06002F38 RID: 12088 RVA: 0x000F75B4 File Offset: 0x000F59B4
		public void SetEvolutionMode()
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = -1;
			this.m_MemberIndex = -1;
			this.m_Scroll.SetEvolutionMode();
		}

		// Token: 0x06002F39 RID: 12089 RVA: 0x000F75DE File Offset: 0x000F59DE
		public void SetLimitBreakMode()
		{
			this.m_SelectButton = CharaListUI.eButton.None;
			this.m_SelectCharaMngID = -1L;
			this.m_PartyIndex = -1;
			this.m_MemberIndex = -1;
			this.m_Scroll.SetLimitBreakMode();
		}

		// Token: 0x06002F3A RID: 12090 RVA: 0x000F7608 File Offset: 0x000F5A08
		public void Setup()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharaList);
			this.m_CharaListGroup.OnClickChara += this.OnClickCharaIconCallBack;
			this.m_CharaListGroup.OnClickRemove += this.OnClickRemoveIconCallBack;
		}

		// Token: 0x06002F3B RID: 12091 RVA: 0x000F7680 File Offset: 0x000F5A80
		private void Update()
		{
			switch (this.m_Step)
			{
			case CharaListUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(CharaListUI.eStep.Idle);
				}
				break;
			}
			case CharaListUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(CharaListUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06002F3C RID: 12092 RVA: 0x000F775C File Offset: 0x000F5B5C
		private void ChangeStep(CharaListUI.eStep step)
		{
			this.m_Step = step;
			switch (step)
			{
			case CharaListUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case CharaListUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaListUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case CharaListUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaListUI.eStep.Window:
				this.SetEnableInput(true);
				break;
			case CharaListUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06002F3D RID: 12093 RVA: 0x000F780C File Offset: 0x000F5C0C
		public override void PlayIn()
		{
			this.ChangeStep(CharaListUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			this.m_CharaListGroup.Open();
		}

		// Token: 0x06002F3E RID: 12094 RVA: 0x000F7854 File Offset: 0x000F5C54
		public override void PlayOut()
		{
			this.ChangeStep(CharaListUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_CharaListGroup.Close();
		}

		// Token: 0x06002F3F RID: 12095 RVA: 0x000F78A8 File Offset: 0x000F5CA8
		public override bool IsMenuEnd()
		{
			return this.m_Step == CharaListUI.eStep.End;
		}

		// Token: 0x06002F40 RID: 12096 RVA: 0x000F78B3 File Offset: 0x000F5CB3
		public override void Destroy()
		{
			base.Destroy();
			this.m_Scroll.Destroy();
		}

		// Token: 0x06002F41 RID: 12097 RVA: 0x000F78C6 File Offset: 0x000F5CC6
		public void OnClickCharaIconCallBack(long charaMngID)
		{
			this.m_SelectButton = CharaListUI.eButton.Chara;
			this.m_SelectCharaMngID = charaMngID;
			this.PlayOut();
		}

		// Token: 0x06002F42 RID: 12098 RVA: 0x000F78DC File Offset: 0x000F5CDC
		public void OnClickRemoveIconCallBack()
		{
			this.m_SelectButton = CharaListUI.eButton.Remove;
			this.PlayOut();
		}

		// Token: 0x0400366C RID: 13932
		private CharaListUI.eStep m_Step;

		// Token: 0x0400366D RID: 13933
		[SerializeField]
		private CharaListScroll m_Scroll;

		// Token: 0x0400366E RID: 13934
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400366F RID: 13935
		[SerializeField]
		private CharaListWindow m_CharaListGroup;

		// Token: 0x04003670 RID: 13936
		private CharaListUI.eButton m_SelectButton = CharaListUI.eButton.None;

		// Token: 0x04003671 RID: 13937
		private long m_SelectCharaMngID = -1L;

		// Token: 0x04003672 RID: 13938
		private int m_PartyIndex = -1;

		// Token: 0x04003673 RID: 13939
		private int m_MemberIndex = -1;

		// Token: 0x020008E7 RID: 2279
		private enum eStep
		{
			// Token: 0x04003675 RID: 13941
			Hide,
			// Token: 0x04003676 RID: 13942
			In,
			// Token: 0x04003677 RID: 13943
			Idle,
			// Token: 0x04003678 RID: 13944
			Out,
			// Token: 0x04003679 RID: 13945
			Window,
			// Token: 0x0400367A RID: 13946
			End,
			// Token: 0x0400367B RID: 13947
			Num
		}

		// Token: 0x020008E8 RID: 2280
		public enum eButton
		{
			// Token: 0x0400367D RID: 13949
			None = -1,
			// Token: 0x0400367E RID: 13950
			Chara,
			// Token: 0x0400367F RID: 13951
			CharaHold,
			// Token: 0x04003680 RID: 13952
			Remove
		}

		// Token: 0x020008E9 RID: 2281
		public enum eMode
		{
			// Token: 0x04003682 RID: 13954
			View,
			// Token: 0x04003683 RID: 13955
			PartyEdit,
			// Token: 0x04003684 RID: 13956
			Upgrade,
			// Token: 0x04003685 RID: 13957
			LimitBreak,
			// Token: 0x04003686 RID: 13958
			Evolution
		}
	}
}
