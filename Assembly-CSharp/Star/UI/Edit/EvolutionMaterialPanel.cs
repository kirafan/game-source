﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F3 RID: 2291
	public class EvolutionMaterialPanel : CharaEditSceneMaterialPanel
	{
		// Token: 0x06002F80 RID: 12160 RVA: 0x000F84F9 File Offset: 0x000F68F9
		public override void Setup(EquipWeaponCharacterDataController cdc)
		{
			this.m_CharacterDataController = cdc;
			this.m_TabGroup.Setup(-1f);
			this.ApplyInfomationData(0);
		}

		// Token: 0x06002F81 RID: 12161 RVA: 0x000F851C File Offset: 0x000F691C
		private void ApplyInfomationData(int index)
		{
			long charaMngId = this.m_CharacterDataController.GetCharaMngId();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			bool[] array = new bool[2];
			CharacterEvolutionListDB_Param[] array2 = new CharacterEvolutionListDB_Param[2];
			for (int i = 0; i < array.Length; i++)
			{
				if (i == 0)
				{
					array[i] = dbMng.CharaEvolutionListDB.GetNormalMaterialParamBySrcCharaID(this.m_CharacterDataController.GetCharaId(), out array2[i]);
				}
				if (i == 1)
				{
					array[i] = dbMng.CharaEvolutionListDB.GetSpecifyMaterialParamBySrcCharaID(this.m_CharacterDataController.GetCharaId(), out array2[i]);
				}
			}
			for (int j = 0; j < array.Length; j++)
			{
				this.m_TabGroup.SetIntaractable(array[j], j);
			}
			if (!array[index])
			{
				this.m_EvolutionInfomations[0].SetActive(false);
				this.m_Caption.SetActive(false);
				this.m_Message.SetActive(false);
				return;
			}
			this.m_EvolutionInfomations[0].SetActive(true);
			this.m_EvolutionInfomations[0].Setup(charaMngId, array2[index]);
			bool active = false;
			bool active2 = false;
			if (index == 1 && array[index])
			{
				active = true;
				active2 = EditUtility.CanEvolution(charaMngId, true);
			}
			if (this.m_Caption != null)
			{
				this.m_Caption.SetActive(active);
			}
			if (this.m_Message != null)
			{
				this.m_Message.SetActive(active2);
			}
		}

		// Token: 0x06002F82 RID: 12162 RVA: 0x000F8696 File Offset: 0x000F6A96
		public override int GetSelectButtonIdx()
		{
			return this.GetTabGroupSelectIndex();
		}

		// Token: 0x06002F83 RID: 12163 RVA: 0x000F869E File Offset: 0x000F6A9E
		public int GetTabGroupSelectIndex()
		{
			if (this.m_TabGroup == null)
			{
				return -1;
			}
			return this.m_TabGroup.SelectIdx;
		}

		// Token: 0x06002F84 RID: 12164 RVA: 0x000F86BE File Offset: 0x000F6ABE
		protected override CustomButton GetDecideButton()
		{
			if (this.m_EvolutionInfomations == null)
			{
				return null;
			}
			if (this.m_EvolutionInfomations[0] == null)
			{
				return null;
			}
			return this.m_EvolutionInfomations[0].GetDecideButton();
		}

		// Token: 0x06002F85 RID: 12165 RVA: 0x000F86EF File Offset: 0x000F6AEF
		public string GetCannotEvolveReasonsString()
		{
			if (this.m_EvolutionInfomations == null)
			{
				return null;
			}
			if (this.m_EvolutionInfomations[0] == null)
			{
				return null;
			}
			return this.m_EvolutionInfomations[0].GetCannotEvolveReasonsString();
		}

		// Token: 0x06002F86 RID: 12166 RVA: 0x000F8720 File Offset: 0x000F6B20
		public void OnClickTabButton()
		{
			this.OnClickSelectButton();
		}

		// Token: 0x06002F87 RID: 12167 RVA: 0x000F8728 File Offset: 0x000F6B28
		public override void OnClickSelectButton()
		{
			this.OnClickSelectButton(this.m_TabGroup.SelectIdx);
		}

		// Token: 0x06002F88 RID: 12168 RVA: 0x000F873B File Offset: 0x000F6B3B
		public override void OnClickSelectButton(int idx)
		{
			this.ApplyInfomationData(idx);
			base.ExecuteOnMaterialsChanged();
		}

		// Token: 0x040036A7 RID: 13991
		[SerializeField]
		[Tooltip("表示する内容を選択するタブグループ.")]
		private TabGroup m_TabGroup;

		// Token: 0x040036A8 RID: 13992
		[SerializeField]
		private EvolutionInformation[] m_EvolutionInfomations;

		// Token: 0x040036A9 RID: 13993
		[SerializeField]
		private GameObject m_Caption;

		// Token: 0x040036AA RID: 13994
		[SerializeField]
		private GameObject m_Message;

		// Token: 0x040036AB RID: 13995
		private EquipWeaponCharacterDataController m_CharacterDataController;
	}
}
