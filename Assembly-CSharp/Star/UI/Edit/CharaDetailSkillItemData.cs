﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020008DF RID: 2271
	public class CharaDetailSkillItemData : ScrollItemData
	{
		// Token: 0x06002F11 RID: 12049 RVA: 0x000F64F8 File Offset: 0x000F48F8
		public CharaDetailSkillItemData()
		{
			this.m_SkillID = -1;
		}

		// Token: 0x0400362B RID: 13867
		public int m_SkillID;

		// Token: 0x0400362C RID: 13868
		public int m_Level;

		// Token: 0x0400362D RID: 13869
		public int m_MaxLevel;
	}
}
