﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000930 RID: 2352
	public class PartyScroll : InfiniteScrollEx
	{
		// Token: 0x060030AD RID: 12461 RVA: 0x000FDEC8 File Offset: 0x000FC2C8
		public static bool ModeToEditActive(PartyScroll.eMode mode)
		{
			return true;
		}

		// Token: 0x060030AE RID: 12462 RVA: 0x000FDECB File Offset: 0x000FC2CB
		public static bool ModeToScrollActive(PartyScroll.eMode mode)
		{
			return mode == PartyScroll.eMode.Edit || mode == PartyScroll.eMode.Quest;
		}

		// Token: 0x060030AF RID: 12463 RVA: 0x000FDEDA File Offset: 0x000FC2DA
		public static bool ModeToTouchActive(PartyScroll.eMode mode)
		{
			return true;
		}

		// Token: 0x060030B0 RID: 12464 RVA: 0x000FDEDD File Offset: 0x000FC2DD
		public static EnumerationPanelCoreBase.eMode ConvertPartyScrolleModeToEnumerationPanelCoreBaseeMode(PartyScroll.eMode mode)
		{
			switch (mode)
			{
			case PartyScroll.eMode.Edit:
				return EnumerationPanelCoreBase.eMode.Edit;
			case PartyScroll.eMode.Quest:
				return EnumerationPanelCoreBase.eMode.Quest;
			case PartyScroll.eMode.QuestStart:
				return EnumerationPanelCoreBase.eMode.QuestStart;
			case PartyScroll.eMode.View:
				return EnumerationPanelCoreBase.eMode.View;
			case PartyScroll.eMode.PartyDetail:
				return EnumerationPanelCoreBase.eMode.PartyDetail;
			default:
				return EnumerationPanelCoreBase.eMode.Edit;
			}
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x060030B1 RID: 12465 RVA: 0x000FDF09 File Offset: 0x000FC309
		public PartyPanelData SelectParty
		{
			get
			{
				return (PartyPanelData)this.m_ItemList[base.GetNowSelectDataIdx()];
			}
		}

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x060030B2 RID: 12466 RVA: 0x000FDF21 File Offset: 0x000FC321
		public int SelectPartyIdx
		{
			get
			{
				return this.SelectParty.m_Party.GetPartyIndex();
			}
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x060030B3 RID: 12467 RVA: 0x000FDF33 File Offset: 0x000FC333
		public long SelectPartyMngID
		{
			get
			{
				return this.SelectParty.m_Party.GetMngId();
			}
		}

		// Token: 0x17000314 RID: 788
		// (get) Token: 0x060030B4 RID: 12468 RVA: 0x000FDF45 File Offset: 0x000FC345
		public EquipWeaponPartyMemberController SelectPartyChara
		{
			get
			{
				return this.GetSelectedPartyChara();
			}
		}

		// Token: 0x17000315 RID: 789
		// (get) Token: 0x060030B5 RID: 12469 RVA: 0x000FDF4D File Offset: 0x000FC34D
		public int SelectCharaIdx
		{
			get
			{
				return this.SelectPartyChara.GetPartySlotIndex();
			}
		}

		// Token: 0x17000316 RID: 790
		// (get) Token: 0x060030B6 RID: 12470 RVA: 0x000FDF5A File Offset: 0x000FC35A
		public long SelectCharaMngID
		{
			get
			{
				return this.SelectPartyChara.GetCharaMngId();
			}
		}

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x060030B7 RID: 12471 RVA: 0x000FDF67 File Offset: 0x000FC367
		public string PartyName
		{
			get
			{
				return this.SelectParty.m_Party.GetPartyName();
			}
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x060030B8 RID: 12472 RVA: 0x000FDF79 File Offset: 0x000FC379
		public int PartyCost
		{
			get
			{
				return this.SelectParty.m_Party.GetPartyCost();
			}
		}

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x060030B9 RID: 12473 RVA: 0x000FDF8B File Offset: 0x000FC38B
		// (set) Token: 0x060030BA RID: 12474 RVA: 0x000FDF93 File Offset: 0x000FC393
		public int PartyMaxCost
		{
			get
			{
				return this.m_PartyMaxCost;
			}
			set
			{
				this.m_PartyMaxCost = value;
			}
		}

		// Token: 0x14000078 RID: 120
		// (add) Token: 0x060030BB RID: 12475 RVA: 0x000FDF9C File Offset: 0x000FC39C
		// (remove) Token: 0x060030BC RID: 12476 RVA: 0x000FDFD4 File Offset: 0x000FC3D4
		public event Action OnChangePage;

		// Token: 0x14000079 RID: 121
		// (add) Token: 0x060030BD RID: 12477 RVA: 0x000FE00C File Offset: 0x000FC40C
		// (remove) Token: 0x060030BE RID: 12478 RVA: 0x000FE044 File Offset: 0x000FC444
		public event Action OnClickChara;

		// Token: 0x1400007A RID: 122
		// (add) Token: 0x060030BF RID: 12479 RVA: 0x000FE07C File Offset: 0x000FC47C
		// (remove) Token: 0x060030C0 RID: 12480 RVA: 0x000FE0B4 File Offset: 0x000FC4B4
		public event Action OnHoldChara;

		// Token: 0x1400007B RID: 123
		// (add) Token: 0x060030C1 RID: 12481 RVA: 0x000FE0EC File Offset: 0x000FC4EC
		// (remove) Token: 0x060030C2 RID: 12482 RVA: 0x000FE124 File Offset: 0x000FC524
		public event Action OnClickWeapon;

		// Token: 0x060030C3 RID: 12483 RVA: 0x000FE15A File Offset: 0x000FC55A
		public void Initialize(EquipWeaponPartiesController parties)
		{
			this.m_parties = parties;
		}

		// Token: 0x060030C4 RID: 12484 RVA: 0x000FE164 File Offset: 0x000FC564
		public override void Setup()
		{
			if (this.m_Window != null)
			{
				Vector2 sizeDelta = new Vector2((float)(160 * this.m_parties.HowManyPartyMemberAll()), 520f);
				base.GetComponent<RectTransform>().sizeDelta = sizeDelta;
				this.m_Window.sizeDelta = new Vector2(sizeDelta.x + 56f, sizeDelta.y + 56f);
				if (this.m_parties.HowManyPartyMemberAll() <= 5)
				{
				}
				base.Setup(sizeDelta.x);
			}
			else
			{
				base.Setup();
			}
			this.m_PartyMaxCost = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.PartyCost;
			this.m_ScrollActiveControlType = InfiniteScrollEx.eActiveControlType.None;
			this.m_ButtonActiveControlType = InfiniteScrollEx.eActiveControlType.None;
			for (int i = 0; i < this.m_parties.HowManyParties(); i++)
			{
				base.AddItem(new PartyPanelData
				{
					m_Party = this.m_parties.GetParty(i),
					m_NowSelectedCharaSlotIndex = 0,
					m_OnClickCharaPanel = new Action<PartyCharaPanelBase.eButton>(this.OnClickPartyChara),
					m_OnHoldCharaPanel = new Action<PartyCharaPanelBase.eButton>(this.OnHoldPartyChara)
				});
			}
		}

		// Token: 0x060030C5 RID: 12485 RVA: 0x000FE290 File Offset: 0x000FC690
		protected override void Update()
		{
			base.Update();
			for (int i = 0; i < this.m_ActiveInstList.Count; i++)
			{
				((PartyEditPartyPanelBase)this.m_ActiveInstList[i]).SetEditMode(PartyScroll.ConvertPartyScrolleModeToEnumerationPanelCoreBaseeMode(this.m_Mode));
			}
			int num = this.m_NowSelect % this.m_ItemList.Count;
			if (this.m_NowSelect < 0)
			{
				num = (this.m_ItemList.Count + num) % this.m_ItemList.Count;
			}
			if (num != this.SelectPartyIdx)
			{
				this.SetSelectPartyIdx(num, false, false);
			}
			if (this.SelectPartyIdx != this.m_PreSelectPartyIdx)
			{
				this.SetSelectPartyIdx(num, false, false);
			}
			this.m_PreSelectPartyIdx = this.SelectPartyIdx;
		}

		// Token: 0x060030C6 RID: 12486 RVA: 0x000FE354 File Offset: 0x000FC754
		public int HowManyChildPanelAllChildren()
		{
			return this.m_parties.HowManyPartyMemberAll();
		}

		// Token: 0x060030C7 RID: 12487 RVA: 0x000FE361 File Offset: 0x000FC761
		public int HowManyChildPanelEnableChildren()
		{
			return this.m_parties.HowManyPartyMemberEnable();
		}

		// Token: 0x060030C8 RID: 12488 RVA: 0x000FE370 File Offset: 0x000FC770
		public void SetEditMode(PartyScroll.eMode mode)
		{
			this.m_Mode = mode;
			bool flag = PartyScroll.ModeToScrollActive(this.m_Mode);
			for (int i = 0; i < this.m_ActiveInstList.Count; i++)
			{
				((PartyEditPartyPanelBase)this.m_ActiveInstList[i]).SetEditMode(PartyScroll.ConvertPartyScrolleModeToEnumerationPanelCoreBaseeMode(this.m_Mode));
			}
			this.m_ScrollRect.horizontal = flag;
			this.m_LeftButtonObj.SetActive(flag);
			this.m_RightButtonObj.SetActive(flag);
		}

		// Token: 0x060030C9 RID: 12489 RVA: 0x000FE3F4 File Offset: 0x000FC7F4
		public void SetSelectPartyIdx(int idx, bool scroll, bool immediate)
		{
			int i;
			for (i = idx % this.m_ItemList.Count; i < 0; i += this.m_ItemList.Count)
			{
			}
			if (scroll)
			{
				base.SetSelectIdx(idx, !immediate);
			}
			this.SetPartyName(this.GetPartyData(i).m_Party.GetPartyName());
			int partyCost = this.PartyCost;
			if (this.m_PartyCostObj != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				if (partyCost > this.m_PartyMaxCost)
				{
					stringBuilder.Append("<color=red>");
				}
				stringBuilder.Append(partyCost.ToString().PadLeft(3));
				if (partyCost > this.m_PartyMaxCost)
				{
					stringBuilder.Append("</color>");
				}
				stringBuilder.Append("/");
				stringBuilder.Append(this.m_PartyMaxCost.ToString().PadLeft(3));
				this.m_PartyCostObj.text = stringBuilder.ToString();
			}
			this.OnChangePage.Call();
		}

		// Token: 0x060030CA RID: 12490 RVA: 0x000FE4FF File Offset: 0x000FC8FF
		public PartyPanelData GetPartyData(int idx)
		{
			if (idx < 0 || idx >= this.m_ItemList.Count)
			{
				return null;
			}
			return (PartyPanelData)this.m_ItemList[idx];
		}

		// Token: 0x060030CB RID: 12491 RVA: 0x000FE52C File Offset: 0x000FC92C
		public void SetPartyName(string name)
		{
			this.SelectParty.m_Party.SetPartyName(name);
			this.m_PartyNameObj.text = name;
		}

		// Token: 0x060030CC RID: 12492 RVA: 0x000FE54B File Offset: 0x000FC94B
		public string GetPartyName(int PartyIndex)
		{
			return ((PartyPanelData)this.m_ItemList[PartyIndex]).m_Party.GetPartyName();
		}

		// Token: 0x060030CD RID: 12493 RVA: 0x000FE568 File Offset: 0x000FC968
		public EquipWeaponPartyMemberController GetSelectedPartyChara()
		{
			return this.SelectParty.GetSelectedMember();
		}

		// Token: 0x060030CE RID: 12494 RVA: 0x000FE575 File Offset: 0x000FC975
		public void SetPageSignalActive(bool flag)
		{
			this.m_Signal.gameObject.SetActive(flag);
		}

		// Token: 0x060030CF RID: 12495 RVA: 0x000FE588 File Offset: 0x000FC988
		protected override void OnClickScrollButton(int addIdx)
		{
			base.OnClickScrollButton(addIdx);
			this.SetSelectPartyIdx(this.m_NowSelect, true, false);
		}

		// Token: 0x060030D0 RID: 12496 RVA: 0x000FE5A0 File Offset: 0x000FC9A0
		public void OnClickPartyChara(PartyCharaPanelBase.eButton place)
		{
			if (!PartyScroll.ModeToEditActive(this.m_Mode))
			{
				return;
			}
			int nowSelectedCharaSlotIndex = ((PartyPanelData)this.m_ItemList[base.GetNowSelectDataIdx()]).m_NowSelectedCharaSlotIndex;
			if (nowSelectedCharaSlotIndex >= this.SelectParty.m_Party.HowManyPartyMemberAll() || nowSelectedCharaSlotIndex >= this.m_parties.HowManyPartyMemberEnable())
			{
				return;
			}
			if (place != PartyCharaPanelBase.eButton.Chara)
			{
				if (place == PartyCharaPanelBase.eButton.Weapon)
				{
					if (this.OnClickWeapon != null)
					{
						this.OnClickWeapon();
					}
				}
			}
			else if (this.OnClickChara != null)
			{
				this.OnClickChara();
			}
		}

		// Token: 0x060030D1 RID: 12497 RVA: 0x000FE64C File Offset: 0x000FCA4C
		public void OnHoldPartyChara(PartyCharaPanelBase.eButton place)
		{
			if (!PartyScroll.ModeToTouchActive(this.m_Mode))
			{
				return;
			}
			this.ForceReleaseDrag();
			if (place == PartyCharaPanelBase.eButton.Chara)
			{
				if (this.SelectPartyChara != null)
				{
					if (this.OnHoldChara != null)
					{
						this.OnHoldChara();
					}
				}
				else if (this.OnHoldChara != null)
				{
					this.OnHoldChara();
				}
			}
		}

		// Token: 0x0400377C RID: 14204
		[SerializeField]
		[Obsolete]
		private PartyEditUIBase m_UI;

		// Token: 0x0400377D RID: 14205
		[SerializeField]
		private RectTransform m_Window;

		// Token: 0x0400377E RID: 14206
		[SerializeField]
		private Text m_PartyNameObj;

		// Token: 0x0400377F RID: 14207
		[SerializeField]
		private Text m_PartyCostObj;

		// Token: 0x04003780 RID: 14208
		private PartyScroll.eMode m_Mode;

		// Token: 0x04003781 RID: 14209
		private EquipWeaponPartiesController m_parties;

		// Token: 0x04003782 RID: 14210
		private int m_PreSelectPartyIdx = -1;

		// Token: 0x04003783 RID: 14211
		private int m_PartyMaxCost;

		// Token: 0x02000931 RID: 2353
		public enum eMode
		{
			// Token: 0x04003789 RID: 14217
			Edit,
			// Token: 0x0400378A RID: 14218
			Quest,
			// Token: 0x0400378B RID: 14219
			QuestStart,
			// Token: 0x0400378C RID: 14220
			View,
			// Token: 0x0400378D RID: 14221
			PartyDetail
		}
	}
}
