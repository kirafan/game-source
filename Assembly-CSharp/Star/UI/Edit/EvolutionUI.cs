﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020008F4 RID: 2292
	public class EvolutionUI : CharaEditSceneUI<EvolutionMaterialPanel, EditUtility.SimulateEvolutionParam>
	{
		// Token: 0x06002F8A RID: 12170 RVA: 0x000F8754 File Offset: 0x000F6B54
		protected override EditUtility.SimulateEvolutionParam Simulate(EquipWeaponCharacterDataController cdc)
		{
			return EditUtility.SimulateEvolution(cdc.GetCharaMngId());
		}

		// Token: 0x06002F8B RID: 12171 RVA: 0x000F8770 File Offset: 0x000F6B70
		protected override CharacterDataController ConvertSimulateParamToAfterCharacterData(EditUtility.SimulateEvolutionParam param)
		{
			if (param == null)
			{
				return null;
			}
			CharacterDataController result = null;
			if (EditUtility.CanEvolution(param.nowUserCharaData.MngID, false) || EditUtility.CanEvolution(param.nowUserCharaData.MngID, true))
			{
				result = new AfterEvolutionCharacterController().Setup(new AfterEvolutionParamWrapper(param));
			}
			return result;
		}

		// Token: 0x06002F8C RID: 12172 RVA: 0x000F87C5 File Offset: 0x000F6BC5
		protected override eText_MessageDB GetCheckWindowTitle()
		{
			return eText_MessageDB.CharacterEvolveCheckWindowTitle;
		}

		// Token: 0x06002F8D RID: 12173 RVA: 0x000F87CC File Offset: 0x000F6BCC
		protected override eText_MessageDB GetCheckWindowMessage()
		{
			return eText_MessageDB.CharacterEvolveCheckWindowMessage;
		}

		// Token: 0x06002F8E RID: 12174 RVA: 0x000F87D3 File Offset: 0x000F6BD3
		protected override void OnClickDecideButtonCallback(int idx)
		{
			if (this.m_Step != CharaEditSceneUIBase.eStep.Idle)
			{
				return;
			}
			if (!EditUtility.CanEvolution(this.m_CharacterDataController.GetCharaMngId(), idx == 1))
			{
				this.OnClickDetailButton();
			}
			else
			{
				base.OnClickDecideButtonCallback(idx);
			}
		}

		// Token: 0x06002F8F RID: 12175 RVA: 0x000F880D File Offset: 0x000F6C0D
		public void OnClickDetailButton()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, "進化条件", SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CharacterEvolveFailed), this.m_MaterialPanel.GetCannotEvolveReasonsString(), null);
		}
	}
}
