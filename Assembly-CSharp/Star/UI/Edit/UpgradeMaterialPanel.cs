﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200090A RID: 2314
	public class UpgradeMaterialPanel : CharaEditSceneMaterialPanel
	{
		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06003005 RID: 12293 RVA: 0x000FA22C File Offset: 0x000F862C
		// (set) Token: 0x06003006 RID: 12294 RVA: 0x000FA234 File Offset: 0x000F8634
		public bool IsEnableIncrease
		{
			get
			{
				return this.m_IsEnableIncrease;
			}
			set
			{
				this.m_IsEnableIncrease = value;
			}
		}

		// Token: 0x14000076 RID: 118
		// (add) Token: 0x06003007 RID: 12295 RVA: 0x000FA240 File Offset: 0x000F8640
		// (remove) Token: 0x06003008 RID: 12296 RVA: 0x000FA278 File Offset: 0x000F8678
		public event Action OnChangeItem;

		// Token: 0x06003009 RID: 12297 RVA: 0x000FA2AE File Offset: 0x000F86AE
		public override void Setup(EquipWeaponCharacterDataController cdc)
		{
			this.Setup(UpgradeMaterialPanel.eMode.Chara, cdc.GetCharaMngId());
		}

		// Token: 0x0600300A RID: 12298 RVA: 0x000FA2C0 File Offset: 0x000F86C0
		public void Setup(UpgradeMaterialPanel.eMode mode, long charaMngID)
		{
			this.m_InfoPanel.Setup();
			this.m_InfoSelectItem.Setup();
			if (mode == UpgradeMaterialPanel.eMode.Chara)
			{
				this.m_TabGroup.Setup(-1f);
				this.m_Scroll.Setup(UpgradeMaterialList.eMode.Chara, this, charaMngID);
				this.m_TabGroup.SelectIdx = UIUtility.ElementEnumToSortIdx(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID).Param.ElementType);
			}
			else
			{
				this.m_Scroll.Setup(UpgradeMaterialList.eMode.Weapon, this, charaMngID);
			}
			this.ResetUseItem();
		}

		// Token: 0x0600300B RID: 12299 RVA: 0x000FA34A File Offset: 0x000F874A
		public void ChangeTab()
		{
			this.OnClickSelectButton();
		}

		// Token: 0x0600300C RID: 12300 RVA: 0x000FA352 File Offset: 0x000F8752
		public bool CanAddUseItem(int itemID)
		{
			return this.m_IsEnableIncrease && this.m_InfoSelectItem.CanAddUseItem(itemID);
		}

		// Token: 0x0600300D RID: 12301 RVA: 0x000FA36D File Offset: 0x000F876D
		public int GetUseItemNum(int itemID)
		{
			return this.m_InfoSelectItem.GetUseItemNumID(itemID);
		}

		// Token: 0x0600300E RID: 12302 RVA: 0x000FA37B File Offset: 0x000F877B
		public override int GetSelectButtonIdx()
		{
			return this.m_TabGroup.SelectIdx;
		}

		// Token: 0x0600300F RID: 12303 RVA: 0x000FA388 File Offset: 0x000F8788
		protected override CustomButton GetDecideButton()
		{
			if (this.m_InfoPanel == null)
			{
				return null;
			}
			return this.m_InfoPanel.GetDecideButton();
		}

		// Token: 0x06003010 RID: 12304 RVA: 0x000FA3A8 File Offset: 0x000F87A8
		public void SetUseItemNum(int itemID, int num)
		{
			if (this.SetSelectedItemNum(itemID, num))
			{
				this.m_Scroll.ForceApply();
			}
		}

		// Token: 0x06003011 RID: 12305 RVA: 0x000FA3C2 File Offset: 0x000F87C2
		public bool SetSelectedItemNum(int itemID, int num)
		{
			if (this.m_InfoSelectItem.SetUseItemNum(itemID, num))
			{
				base.ExecuteOnMaterialsChanged();
				this.OnChangeItem.Call();
				this.m_Scroll.ForceApply();
				return true;
			}
			return false;
		}

		// Token: 0x06003012 RID: 12306 RVA: 0x000FA3F5 File Offset: 0x000F87F5
		public void ResetUseItem()
		{
			this.m_Scroll.ResetUseNum();
			this.m_InfoSelectItem.ResetUseItem();
			base.ExecuteOnMaterialsChanged();
			this.OnChangeItem.Call();
			this.m_Scroll.ForceApply();
		}

		// Token: 0x06003013 RID: 12307 RVA: 0x000FA429 File Offset: 0x000F8829
		public SelectItemPanel GetSelectItemPanel()
		{
			return this.m_InfoSelectItem;
		}

		// Token: 0x06003014 RID: 12308 RVA: 0x000FA431 File Offset: 0x000F8831
		public SelectItemPanel.UseItemInfos GetSelectItemInfos()
		{
			if (this.m_InfoSelectItem == null)
			{
				return null;
			}
			return this.m_InfoSelectItem.GetUseItemInfos();
		}

		// Token: 0x06003015 RID: 12309 RVA: 0x000FA451 File Offset: 0x000F8851
		public UpgradeInfoPanel GetUpgradeInfoPanel()
		{
			return this.m_InfoPanel;
		}

		// Token: 0x06003016 RID: 12310 RVA: 0x000FA459 File Offset: 0x000F8859
		public override void OnClickSelectButton()
		{
			this.OnClickSelectButton(this.m_TabGroup.SelectIdx);
		}

		// Token: 0x06003017 RID: 12311 RVA: 0x000FA46C File Offset: 0x000F886C
		public override void OnClickSelectButton(int idx)
		{
			this.m_Scroll.ChangePage(idx, false);
			base.ExecuteOnMaterialsChanged();
		}

		// Token: 0x040036FE RID: 14078
		public const int USE_MATERIAL_MAX = 99;

		// Token: 0x040036FF RID: 14079
		[SerializeField]
		private SelectItemPanel m_InfoSelectItem;

		// Token: 0x04003700 RID: 14080
		[SerializeField]
		[Tooltip("ScrollViewに表示する内容を選択するタブグループ.")]
		private TabGroup m_TabGroup;

		// Token: 0x04003701 RID: 14081
		[SerializeField]
		[Tooltip("素材を選択するScrollView.")]
		private UpgradeMaterialList m_Scroll;

		// Token: 0x04003702 RID: 14082
		[SerializeField]
		[Tooltip("現状費用と選択した状態に対する操作を持つパネル.")]
		private UpgradeInfoPanel m_InfoPanel;

		// Token: 0x04003703 RID: 14083
		private bool m_IsEnableIncrease = true;

		// Token: 0x0200090B RID: 2315
		public enum eMode
		{
			// Token: 0x04003706 RID: 14086
			Chara,
			// Token: 0x04003707 RID: 14087
			Weapon
		}
	}
}
