﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020008F9 RID: 2297
	public class LimitBreakUI : CharaEditSceneUI<LimitBreakMaterialPanel, EditUtility.SimulateLimitBreakParam>
	{
		// Token: 0x06002FAA RID: 12202 RVA: 0x000F8EE0 File Offset: 0x000F72E0
		protected override EditUtility.SimulateLimitBreakParam Simulate(EquipWeaponCharacterDataController cdc)
		{
			return EditUtility.SimulateLimitBreak(cdc.GetCharaMngId());
		}

		// Token: 0x06002FAB RID: 12203 RVA: 0x000F8EFC File Offset: 0x000F72FC
		protected override CharacterDataController ConvertSimulateParamToAfterCharacterData(EditUtility.SimulateLimitBreakParam param)
		{
			if (param == null)
			{
				return null;
			}
			CharacterDataController result = null;
			if (0 <= this.m_MaterialPanel.GetSelectButtonIdx())
			{
				result = new AfterLimitBreakCharacterController().Setup(new AfterLimitBreakParamWrapper(param));
			}
			return result;
		}

		// Token: 0x06002FAC RID: 12204 RVA: 0x000F8F36 File Offset: 0x000F7336
		protected override eText_MessageDB GetCheckWindowTitle()
		{
			return eText_MessageDB.CharacterLimitBreakCheckWindowTitle;
		}

		// Token: 0x06002FAD RID: 12205 RVA: 0x000F8F3D File Offset: 0x000F733D
		protected override eText_MessageDB GetCheckWindowMessage()
		{
			return eText_MessageDB.CharacterLimitBreakCheckWindowMessage;
		}

		// Token: 0x06002FAE RID: 12206 RVA: 0x000F8F44 File Offset: 0x000F7344
		public void OnClickChoicesButton(int idx)
		{
			this.m_MaterialPanel.OnClickSelectButton(idx);
		}
	}
}
