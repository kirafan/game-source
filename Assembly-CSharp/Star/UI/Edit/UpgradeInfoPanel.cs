﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000904 RID: 2308
	public class UpgradeInfoPanel : MonoBehaviour
	{
		// Token: 0x06002FDE RID: 12254 RVA: 0x000F973C File Offset: 0x000F7B3C
		public void Setup()
		{
		}

		// Token: 0x06002FDF RID: 12255 RVA: 0x000F9740 File Offset: 0x000F7B40
		public void Apply(long exp, long gold)
		{
			this.m_ExpText.text = exp.ToString();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(UIUtility.GoldValueToString(gold, true));
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold(gold))
			{
				UIUtility.GetNotEnoughString(stringBuilder);
			}
			this.m_MoneyText.text = stringBuilder.ToString();
		}

		// Token: 0x06002FE0 RID: 12256 RVA: 0x000F97AB File Offset: 0x000F7BAB
		public CustomButton GetDecideButton()
		{
			return this.m_DecideButton;
		}

		// Token: 0x040036DA RID: 14042
		[SerializeField]
		private Text m_ExpText;

		// Token: 0x040036DB RID: 14043
		[SerializeField]
		private Text m_MoneyText;

		// Token: 0x040036DC RID: 14044
		[SerializeField]
		private CustomButton m_DecideButton;
	}
}
