﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000911 RID: 2321
	public class LimitBreakResultUI : CharacterEditResultSceneUIBase<EditMain.LimitBreakResultData>
	{
		// Token: 0x06003038 RID: 12344 RVA: 0x000FAAFC File Offset: 0x000F8EFC
		protected override CharacterDataController ConvertResultDataToBeforeCharacterData(EditMain.LimitBreakResultData result)
		{
			return new CharacterDataWrapperController().Setup(new LimitBreakResultBeforeAfterPairBeforePartCharacterWrapper(result));
		}

		// Token: 0x06003039 RID: 12345 RVA: 0x000FAB0E File Offset: 0x000F8F0E
		protected override CharacterDataController ConvertResultDataToAfterCharacterData(EditMain.LimitBreakResultData result)
		{
			return new CharacterDataWrapperController().Setup(new LimitBreakResultBeforeAfterPairAfterPartCharacterWrapper(result));
		}

		// Token: 0x0600303A RID: 12346 RVA: 0x000FAB20 File Offset: 0x000F8F20
		protected override eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_008;
		}
	}
}
