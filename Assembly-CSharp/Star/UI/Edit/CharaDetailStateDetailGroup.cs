﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008E0 RID: 2272
	public class CharaDetailStateDetailGroup : UIGroup
	{
		// Token: 0x06002F13 RID: 12051 RVA: 0x000F6516 File Offset: 0x000F4916
		public void SetCharacter(EquipWeaponCharacterDataController partyMember)
		{
			this.m_CharaInfoPanel.Setup();
			this.m_CharaInfoPanel.ApplyCharaDetailStateDetail(partyMember);
			this.m_RebuildFrames = 10;
		}

		// Token: 0x06002F14 RID: 12052 RVA: 0x000F6537 File Offset: 0x000F4937
		public override void Update()
		{
			base.Update();
			if (0 < this.m_RebuildFrames)
			{
				this.m_RebuildFrames--;
				if (this.m_RebuildLayout != null)
				{
					LayoutRebuilder.MarkLayoutForRebuild(this.m_RebuildLayout);
				}
			}
		}

		// Token: 0x0400362E RID: 13870
		[SerializeField]
		[Tooltip("キャラクタ情報表示パネル.")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfoPanel;

		// Token: 0x0400362F RID: 13871
		[SerializeField]
		protected RectTransform m_RebuildLayout;

		// Token: 0x04003630 RID: 13872
		private const int REBUILD_FRAMES = 10;

		// Token: 0x04003631 RID: 13873
		private int m_RebuildFrames = -1;
	}
}
