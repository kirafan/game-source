﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200093C RID: 2364
	public class PartyEditMainGroupBase : UIGroup
	{
		// Token: 0x060030EC RID: 12524 RVA: 0x000FC0F2 File Offset: 0x000FA4F2
		public static bool ModeToEditActive(PartyEditMainGroupBase.eMode mode)
		{
			return true;
		}

		// Token: 0x060030ED RID: 12525 RVA: 0x000FC0F5 File Offset: 0x000FA4F5
		public static PartyScroll.eMode ConvertPartyScrolleModeToPartyEditUIBaseeMode(PartyEditMainGroupBase.eMode mode)
		{
			switch (mode)
			{
			case PartyEditMainGroupBase.eMode.Edit:
				return PartyScroll.eMode.Edit;
			case PartyEditMainGroupBase.eMode.Quest:
				return PartyScroll.eMode.Quest;
			case PartyEditMainGroupBase.eMode.QuestStart:
				return PartyScroll.eMode.QuestStart;
			case PartyEditMainGroupBase.eMode.View:
				return PartyScroll.eMode.View;
			case PartyEditMainGroupBase.eMode.PartyDetail:
				return PartyScroll.eMode.PartyDetail;
			default:
				return PartyScroll.eMode.Edit;
			}
		}

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x060030EE RID: 12526 RVA: 0x000FC121 File Offset: 0x000FA521
		public EquipWeaponPartyMemberController SelectPartyChara
		{
			get
			{
				if (this.m_Scroll == null)
				{
					return null;
				}
				return this.m_Scroll.GetSelectedPartyChara();
			}
		}

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x060030EF RID: 12527 RVA: 0x000FC141 File Offset: 0x000FA541
		public int SelectPartyIndex
		{
			get
			{
				return this.m_Scroll.SelectPartyIdx;
			}
		}

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x060030F0 RID: 12528 RVA: 0x000FC14E File Offset: 0x000FA54E
		public long SelectPartyMngID
		{
			get
			{
				return this.m_Scroll.SelectPartyMngID;
			}
		}

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x060030F1 RID: 12529 RVA: 0x000FC15C File Offset: 0x000FA55C
		public int SelectCharaIndex
		{
			get
			{
				EquipWeaponPartyMemberController selectPartyChara = this.SelectPartyChara;
				if (selectPartyChara == null)
				{
					return -1;
				}
				return selectPartyChara.GetPartySlotIndex();
			}
		}

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x060030F2 RID: 12530 RVA: 0x000FC180 File Offset: 0x000FA580
		public long SelectCharaMngID
		{
			get
			{
				EquipWeaponPartyMemberController selectPartyChara = this.SelectPartyChara;
				if (selectPartyChara == null)
				{
					return -1L;
				}
				return selectPartyChara.GetCharaMngId();
			}
		}

		// Token: 0x1400007C RID: 124
		// (add) Token: 0x060030F3 RID: 12531 RVA: 0x000FC1A4 File Offset: 0x000FA5A4
		// (remove) Token: 0x060030F4 RID: 12532 RVA: 0x000FC1DC File Offset: 0x000FA5DC
		public event Action m_OnClickChara;

		// Token: 0x1400007D RID: 125
		// (add) Token: 0x060030F5 RID: 12533 RVA: 0x000FC214 File Offset: 0x000FA614
		// (remove) Token: 0x060030F6 RID: 12534 RVA: 0x000FC24C File Offset: 0x000FA64C
		public event Action m_OnHoldChara;

		// Token: 0x1400007E RID: 126
		// (add) Token: 0x060030F7 RID: 12535 RVA: 0x000FC284 File Offset: 0x000FA684
		// (remove) Token: 0x060030F8 RID: 12536 RVA: 0x000FC2BC File Offset: 0x000FA6BC
		public event Action m_OnClickWeapon;

		// Token: 0x060030F9 RID: 12537 RVA: 0x000FC2F4 File Offset: 0x000FA6F4
		public virtual void Setup(PartyEditUIBase ui, PartyEditMainGroupBase.eMode mode = PartyEditMainGroupBase.eMode.Edit, int startPartyIndex = -1, EquipWeaponPartiesController parties = null)
		{
			this.m_Mode = mode;
			this.m_Parties = parties;
			this.GetScroll().Initialize(parties);
			this.GetScroll().Setup();
			this.GetScroll().SetSelectPartyIdx(startPartyIndex, true, true);
			if (!this.m_AddScrollCallback)
			{
				this.GetScroll().OnChangePage += ui.OnChangePage;
				this.GetScroll().OnClickChara += ui.OnClickCharaCallBack;
				this.GetScroll().OnHoldChara += ui.OnHoldCharaCallBack;
				this.GetScroll().OnClickWeapon += ui.OnClickWeaponCallBack;
				this.m_AddScrollCallback = true;
			}
			if (this.m_PartyNamePanel != null)
			{
				this.m_PartyNamePanel.Setup();
			}
		}

		// Token: 0x060030FA RID: 12538 RVA: 0x000FC3C4 File Offset: 0x000FA7C4
		public void Destroy()
		{
			this.m_Scroll.Destroy();
		}

		// Token: 0x060030FB RID: 12539 RVA: 0x000FC3D4 File Offset: 0x000FA7D4
		public virtual void Resetup()
		{
			int selectPartyIndex = this.SelectPartyIndex;
			this.m_Scroll.Refresh();
			this.GetScroll().RemoveAll();
			this.GetScroll().Refresh();
			this.Setup(null, this.m_Mode, selectPartyIndex, (!PartyEditMainGroupBase.ModeToEditActive(this.m_Mode)) ? this.m_Parties : null);
		}

		// Token: 0x060030FC RID: 12540 RVA: 0x000FC434 File Offset: 0x000FA834
		public void RefreshScroll()
		{
			PartyScroll scroll = this.GetScroll();
			if (scroll == null)
			{
				return;
			}
			scroll.Refresh();
		}

		// Token: 0x060030FD RID: 12541 RVA: 0x000FC45C File Offset: 0x000FA85C
		public void ScrollRemoveAll()
		{
			PartyScroll scroll = this.GetScroll();
			if (scroll == null)
			{
				return;
			}
			scroll.RemoveAll();
		}

		// Token: 0x060030FE RID: 12542 RVA: 0x000FC483 File Offset: 0x000FA883
		public PartyScroll GetScroll()
		{
			return this.m_Scroll;
		}

		// Token: 0x060030FF RID: 12543 RVA: 0x000FC48B File Offset: 0x000FA88B
		public void SetOpenNameChangeButtonInteractive(bool Interactive)
		{
			if (this.m_PartyNamePanel == null)
			{
				return;
			}
			this.m_PartyNamePanel.SetTouchInteractive(Interactive);
		}

		// Token: 0x06003100 RID: 12544 RVA: 0x000FC4AB File Offset: 0x000FA8AB
		public int HowManyChildPanelAllChildren()
		{
			return this.GetScroll().HowManyChildPanelAllChildren();
		}

		// Token: 0x06003101 RID: 12545 RVA: 0x000FC4B8 File Offset: 0x000FA8B8
		public int HowManyChildPanelEnableChildren()
		{
			return this.GetScroll().HowManyChildPanelEnableChildren();
		}

		// Token: 0x06003102 RID: 12546 RVA: 0x000FC4C5 File Offset: 0x000FA8C5
		public PartyPanelData GetSelectedPartyData()
		{
			return this.GetPartyData(this.GetScroll().SelectPartyIdx);
		}

		// Token: 0x06003103 RID: 12547 RVA: 0x000FC4D8 File Offset: 0x000FA8D8
		public PartyPanelData GetPartyData(int idx)
		{
			return this.GetScroll().GetPartyData(idx);
		}

		// Token: 0x06003104 RID: 12548 RVA: 0x000FC4E6 File Offset: 0x000FA8E6
		public string GetSelectedPartyName()
		{
			return this.GetScroll().PartyName;
		}

		// Token: 0x06003105 RID: 12549 RVA: 0x000FC4F3 File Offset: 0x000FA8F3
		public string GetPartyName(int partyIndex)
		{
			return this.GetScroll().GetPartyName(partyIndex);
		}

		// Token: 0x06003106 RID: 12550 RVA: 0x000FC501 File Offset: 0x000FA901
		protected virtual void SetPartyScrollEditMode(PartyEditMainGroupBase.eMode mode)
		{
			this.GetScroll().SetEditMode(PartyEditMainGroupBase.ConvertPartyScrolleModeToPartyEditUIBaseeMode(mode));
		}

		// Token: 0x06003107 RID: 12551 RVA: 0x000FC514 File Offset: 0x000FA914
		public void SetPartyName(string name)
		{
			this.GetScroll().SetPartyName(name);
		}

		// Token: 0x06003108 RID: 12552 RVA: 0x000FC522 File Offset: 0x000FA922
		public void OnClickCharaCallBack()
		{
			this.m_OnClickChara.Call();
		}

		// Token: 0x06003109 RID: 12553 RVA: 0x000FC52F File Offset: 0x000FA92F
		public void OnHoldCharaCallBack()
		{
			this.m_OnHoldChara.Call();
		}

		// Token: 0x0600310A RID: 12554 RVA: 0x000FC53C File Offset: 0x000FA93C
		public void OnClickWeaponCallBack()
		{
			this.m_OnClickWeapon.Call();
		}

		// Token: 0x0600310B RID: 12555 RVA: 0x000FC549 File Offset: 0x000FA949
		public virtual void OnChangePage()
		{
		}

		// Token: 0x04003793 RID: 14227
		[SerializeField]
		protected PartyScroll m_Scroll;

		// Token: 0x04003794 RID: 14228
		[SerializeField]
		protected PartyEditControlPanel m_ControlPanel;

		// Token: 0x04003795 RID: 14229
		[SerializeField]
		protected PartyEditPartyNamePanel m_PartyNamePanel;

		// Token: 0x04003796 RID: 14230
		protected PartyEditMainGroupBase.eMode m_Mode;

		// Token: 0x04003797 RID: 14231
		protected EquipWeaponPartiesController m_Parties;

		// Token: 0x04003798 RID: 14232
		protected bool m_AddScrollCallback;

		// Token: 0x0200093D RID: 2365
		public enum eMode
		{
			// Token: 0x0400379D RID: 14237
			Edit,
			// Token: 0x0400379E RID: 14238
			Quest,
			// Token: 0x0400379F RID: 14239
			QuestStart,
			// Token: 0x040037A0 RID: 14240
			View,
			// Token: 0x040037A1 RID: 14241
			PartyDetail
		}
	}
}
