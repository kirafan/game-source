﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000951 RID: 2385
	public class UseMaterialPanel : MonoBehaviour
	{
		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06003184 RID: 12676 RVA: 0x000FF6CE File Offset: 0x000FDACE
		public bool IsNotEnough
		{
			get
			{
				return this.m_IsNotEnough;
			}
		}

		// Token: 0x06003185 RID: 12677 RVA: 0x000FF6D6 File Offset: 0x000FDAD6
		public void Setup()
		{
			this.m_TextColor = this.m_UseNumTextObj.color;
		}

		// Token: 0x06003186 RID: 12678 RVA: 0x000FF6EC File Offset: 0x000FDAEC
		public void Apply(int itemID, int useNum)
		{
			int itemNum = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetItemNum(itemID);
			this.m_HaveNumTextObj.text = "所持" + UIUtility.ItemHaveNumToString(itemNum, true);
			this.m_UseNumTextObj.text = "必要" + useNum.ToString("D2");
			if (itemNum < useNum)
			{
				this.m_UseNumTextObj.color = Color.red;
				this.m_IsNotEnough = true;
			}
			else
			{
				this.m_UseNumTextObj.color = this.m_TextColor;
				this.m_IsNotEnough = false;
			}
		}

		// Token: 0x040037EE RID: 14318
		[SerializeField]
		private Image m_IconImage;

		// Token: 0x040037EF RID: 14319
		[SerializeField]
		private Text m_HaveNumTextObj;

		// Token: 0x040037F0 RID: 14320
		[SerializeField]
		private Text m_UseNumTextObj;

		// Token: 0x040037F1 RID: 14321
		private bool m_IsNotEnough;

		// Token: 0x040037F2 RID: 14322
		private Color m_TextColor;
	}
}
