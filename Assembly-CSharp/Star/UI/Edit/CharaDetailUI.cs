﻿using System;
using Star.UI.BackGround;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020008E2 RID: 2274
	public class CharaDetailUI : MenuUIBase
	{
		// Token: 0x06002F18 RID: 12056 RVA: 0x000F68C0 File Offset: 0x000F4CC0
		public void SetupProfileOnly(int charaId)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.SetupCommonProcessing();
			this.m_Mode = CharaDetailUI.eMode.ProfileOnly;
			this.Apply(new EquipWeaponUnmanagedSupportPartyMemberController().Setup(new UserSupportData
			{
				CharaID = charaId
			}));
		}

		// Token: 0x06002F19 RID: 12057 RVA: 0x000F6924 File Offset: 0x000F4D24
		public void Setup(long charaMngID)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.SetupCommonProcessing();
			this.m_Mode = CharaDetailUI.eMode.Normal;
			this.Apply(new EquipWeaponUnmanagedUserCharacterDataController().Setup(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID), null));
		}

		// Token: 0x06002F1A RID: 12058 RVA: 0x000F698C File Offset: 0x000F4D8C
		public void Setup(EquipWeaponCharacterDataController partyMember, CharaDetailUI.eMode mode = CharaDetailUI.eMode.Auto)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.SetupCommonProcessing();
			if (mode == CharaDetailUI.eMode.Auto)
			{
				if (partyMember.GetCharacterDataControllerType() == eCharacterDataControllerType.UnmanagedSupportParty)
				{
					this.m_Mode = CharaDetailUI.eMode.StateOnly;
				}
				else
				{
					this.m_Mode = CharaDetailUI.eMode.Normal;
				}
			}
			else
			{
				this.m_Mode = mode;
			}
			this.Apply(partyMember);
		}

		// Token: 0x06002F1B RID: 12059 RVA: 0x000F6A04 File Offset: 0x000F4E04
		protected void SetupCommonProcessing()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.m_TabGroup.Setup(-1f);
			if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsFinishedTutorialSeq())
			{
				this.m_TabGroup.SetButtonActive(false, 2);
			}
			this.m_ViewTab.Setup(new string[]
			{
				dbMng.GetTextCommon(eText_CommonDB.ProfileViewRadioButtonIllustButton),
				dbMng.GetTextCommon(eText_CommonDB.ProfileViewRadioButtonSDButton)
			}, 210f);
			this.m_CharaInfoPanel.Setup();
		}

		// Token: 0x06002F1C RID: 12060 RVA: 0x000F6A8C File Offset: 0x000F4E8C
		private void Update()
		{
			if (this.m_IsDirty)
			{
				this.UpdateCategoryInfo();
				this.m_IsDirty = false;
			}
			if (0 < this.m_Frames)
			{
				this.m_Frames--;
				RectTransform rectTransform = this.m_StateContentLayoutGroup;
				for (int i = 0; i < 0; i++)
				{
					rectTransform = (rectTransform.parent as RectTransform);
				}
				LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
			}
			if (this.m_ViewTab != null && this.m_ViewTab != null && this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Idle)
			{
				for (int j = 0; j < this.m_ViewTab.GetButtonNum(); j++)
				{
					this.m_ViewTab.SetIntaractable(true, j);
				}
			}
			switch (this.m_Step)
			{
			case CharaDetailUI.eStep.BGIn:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.PlayInUI();
				}
				break;
			case CharaDetailUI.eStep.In:
			{
				bool flag = true;
				for (int k = 0; k < this.m_AnimUIPlayerArray.Length; k++)
				{
					if (this.m_AnimUIPlayerArray[k].State == 1)
					{
						flag = false;
						break;
					}
				}
				for (int l = 0; l < this.m_MainGroups.Length; l++)
				{
					if (!this.m_MainGroups[l].IsDonePlayIn)
					{
						flag = false;
					}
				}
				if (flag)
				{
					this.ChangeStep(CharaDetailUI.eStep.Idle);
				}
				break;
			}
			case CharaDetailUI.eStep.Out:
			{
				bool flag2 = true;
				for (int m = 0; m < this.m_AnimUIPlayerArray.Length; m++)
				{
					if (this.m_AnimUIPlayerArray[m].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				for (int n = 0; n < this.m_MainGroups.Length; n++)
				{
					if (!this.m_MainGroups[n].IsDonePlayOut)
					{
						flag2 = false;
					}
				}
				if (flag2)
				{
					this.ChangeStep(CharaDetailUI.eStep.End);
				}
				break;
			}
			case CharaDetailUI.eStep.StateDetail:
				if (!this.m_StateDetailGroup.IsOpen())
				{
					this.ChangeStep(CharaDetailUI.eStep.Idle);
				}
				break;
			case CharaDetailUI.eStep.IllustViewIn:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_MenuObject.SetActive(false);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, -1f, null);
					this.ChangeStep(CharaDetailUI.eStep.IllustView);
				}
				break;
			case CharaDetailUI.eStep.IllustView:
			{
				Vector2 vector;
				if (InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0))
				{
					this.ChangeStep(CharaDetailUI.eStep.IllustViewRotateFadeOut);
				}
				break;
			}
			case CharaDetailUI.eStep.IllustViewRotateFadeOut:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.ChangeStep(CharaDetailUI.eStep.IllustViewRotateFadeIn);
				}
				break;
			case CharaDetailUI.eStep.IllustViewRotateFadeIn:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.ChangeStep(CharaDetailUI.eStep.IllustViewVertical);
				}
				break;
			case CharaDetailUI.eStep.IllustViewVertical:
			{
				Vector2 vector2;
				if (InputTouch.Inst.DetectTriggerOfSingleTouch(out vector2, 0))
				{
					this.ChangeStep(CharaDetailUI.eStep.IllustViewOut);
				}
				break;
			}
			case CharaDetailUI.eStep.IllustViewOut:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.OnEndViewIllust.Call();
					this.m_ViewIllust.Destroy();
					this.m_MenuObject.SetActive(true);
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.1f, null);
					this.ChangeStep(CharaDetailUI.eStep.Idle);
				}
				break;
			}
		}

		// Token: 0x06002F1D RID: 12061 RVA: 0x000F6E0C File Offset: 0x000F520C
		private void ChangeStep(CharaDetailUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case CharaDetailUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case CharaDetailUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaDetailUI.eStep.Idle:
				for (int i = 0; i < this.m_MainGroups.Length; i++)
				{
					this.m_MainGroups[i].SetEnableInput(true);
				}
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case CharaDetailUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharaDetailUI.eStep.End:
				this.SetEnableInput(false);
				this.m_CharaInfoPanel.Destroy();
				break;
			case CharaDetailUI.eStep.StateDetail:
				this.m_StateDetailGroup.SetCharacter(this.m_CharacterDataController);
				this.m_StateDetailGroup.Open();
				break;
			case CharaDetailUI.eStep.IllustViewIn:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.1f, null);
				break;
			case CharaDetailUI.eStep.IllustView:
			{
				if (this.m_CharacterDataController != null)
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharacterDataController.GetCharaId()).m_Rare == 4)
					{
						this.m_ViewIllust.Apply(this.m_CharacterDataController.GetCharaId(), CharaIllust.eCharaIllustType.Full);
					}
					else
					{
						this.m_ViewIllust.Apply(this.m_CharacterDataController.GetCharaId(), CharaIllust.eCharaIllustType.Chara);
					}
				}
				this.m_IllustViewGroup.Open();
				RectTransform component = this.m_ViewIllust.GetComponent<RectTransform>();
				component.eulerAngles = new Vector3(0f, 0f, 0f);
				component.sizeDelta = base.GetComponent<RectTransform>().sizeDelta;
				break;
			}
			case CharaDetailUI.eStep.IllustViewRotateFadeOut:
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.1f, null);
				break;
			case CharaDetailUI.eStep.IllustViewRotateFadeIn:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, 0.1f, null);
				RectTransform component2 = this.m_ViewIllust.GetComponent<RectTransform>();
				component2.eulerAngles = new Vector3(0f, 0f, 90f);
				RectTransform component3 = base.GetComponent<RectTransform>();
				component2.sizeDelta = new Vector2(component3.sizeDelta.y, component3.sizeDelta.y * component2.sizeDelta.y / component2.sizeDelta.x);
				break;
			}
			case CharaDetailUI.eStep.IllustViewOut:
				this.m_IllustViewGroup.Close();
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, 0.1f, null);
				break;
			}
		}

		// Token: 0x06002F1E RID: 12062 RVA: 0x000F70DC File Offset: 0x000F54DC
		public override void PlayIn()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.OverlayUI1, UIBackGroundManager.eBackGroundID.Common, true);
			this.ChangeStep(CharaDetailUI.eStep.BGIn);
		}

		// Token: 0x06002F1F RID: 12063 RVA: 0x000F70F8 File Offset: 0x000F54F8
		public override void PlayOut()
		{
			this.ChangeStep(CharaDetailUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			this.m_CharaInfoPanel.SetEnableRenderCharacterView(false);
			for (int j = 0; j < this.m_MainGroups.Length; j++)
			{
				this.m_MainGroups[j].Close();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.OverlayUI1);
		}

		// Token: 0x06002F20 RID: 12064 RVA: 0x000F7174 File Offset: 0x000F5574
		private void PlayInUI()
		{
			this.ChangeStep(CharaDetailUI.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			for (int j = 0; j < this.m_MainGroups.Length; j++)
			{
				this.m_MainGroups[j].Open();
			}
		}

		// Token: 0x06002F21 RID: 12065 RVA: 0x000F71D4 File Offset: 0x000F55D4
		public void Refresh()
		{
			if (this.m_CharacterDataController != null)
			{
				this.Apply(this.m_CharacterDataController);
			}
		}

		// Token: 0x06002F22 RID: 12066 RVA: 0x000F71F0 File Offset: 0x000F55F0
		private void UpdateCategoryInfo()
		{
			for (int i = 0; i < this.m_InfoObject.Length; i++)
			{
				if (i == (int)this.m_Category)
				{
					this.m_InfoObject[i].SetActive(true);
					RectTransform target = null;
					if (i != 1)
					{
						target = this.m_InfoObject[i].GetComponent<RectTransform>();
					}
					this.m_MainGroupWindowBody.FitTarget(target);
					this.m_Frames = 30;
				}
				else
				{
					this.m_InfoObject[i].SetActive(false);
				}
			}
		}

		// Token: 0x06002F23 RID: 12067 RVA: 0x000F7270 File Offset: 0x000F5670
		private void Apply(long charaMngID)
		{
			EquipWeaponUnmanagedUserCharacterDataController equipWeaponUnmanagedUserCharacterDataController = new EquipWeaponUnmanagedUserCharacterDataController().Setup(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID), null);
			this.m_CharacterDataController = equipWeaponUnmanagedUserCharacterDataController;
			this.m_CharaInfoPanel.ApplyCharaDetail(equipWeaponUnmanagedUserCharacterDataController);
			this.m_PreViewTabIndex = 0;
			this.m_ViewIllust.gameObject.SetActive(true);
			this.m_IsDirty = true;
		}

		// Token: 0x06002F24 RID: 12068 RVA: 0x000F72CC File Offset: 0x000F56CC
		private void Apply(EquipWeaponCharacterDataController partyMember)
		{
			if (this.m_Mode == CharaDetailUI.eMode.StateOnly)
			{
				this.SetTabButtonIntaractable(1, false);
				this.SetTabButtonIntaractable(2, false);
				this.SetSelectTab(0);
			}
			else if (this.m_Mode == CharaDetailUI.eMode.ProfileOnly)
			{
				this.SetTabButtonIntaractable(0, false);
				this.SetTabButtonIntaractable(2, false);
				this.SetSelectTab(1);
			}
			else if (this.m_Mode == CharaDetailUI.eMode.GachaResult)
			{
				this.SetSelectTab(0);
			}
			this.m_CharacterDataController = partyMember;
			this.m_CharaInfoPanel.ApplyCharaDetail(partyMember);
			this.m_PreViewTabIndex = 0;
			this.m_ViewIllust.gameObject.SetActive(true);
			this.m_IsDirty = true;
		}

		// Token: 0x06002F25 RID: 12069 RVA: 0x000F736E File Offset: 0x000F576E
		public void SetSelectTab(int index)
		{
			this.m_TabGroup.SelectIdx = index;
		}

		// Token: 0x06002F26 RID: 12070 RVA: 0x000F737C File Offset: 0x000F577C
		public void SetTabButtonIntaractable(int index, bool flag)
		{
			this.m_TabGroup.SetIntaractable(flag, index);
		}

		// Token: 0x06002F27 RID: 12071 RVA: 0x000F738B File Offset: 0x000F578B
		public override bool IsMenuEnd()
		{
			return this.m_Step == CharaDetailUI.eStep.End;
		}

		// Token: 0x06002F28 RID: 12072 RVA: 0x000F7396 File Offset: 0x000F5796
		public CharaDetailUI.eStep GetStep()
		{
			return this.m_Step;
		}

		// Token: 0x06002F29 RID: 12073 RVA: 0x000F739E File Offset: 0x000F579E
		public void OnClickCategoryButton()
		{
			this.m_Category = (CharaDetailUI.eCategory)this.m_TabGroup.SelectIdx;
			this.UpdateCategoryInfo();
		}

		// Token: 0x06002F2A RID: 12074 RVA: 0x000F73B8 File Offset: 0x000F57B8
		public void OnClickViewTabButton()
		{
			if (this.m_ViewTab != null)
			{
				if (this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Wait || this.m_CharaInfoPanel.GetCharacterViewState() == SelectedCharaInfoCharacterViewBase.eState.Idle)
				{
					if (this.m_ViewTab.SelectIdx == 0)
					{
						if (this.m_CharacterDataController != null)
						{
							this.m_CharaInfoPanel.RequestLoadCharacterViewCharacterIllustOnly(this.m_CharacterDataController, true);
						}
					}
					else if (this.m_CharacterDataController != null)
					{
						this.m_CharaInfoPanel.RequestLoadCharacterViewModelOnly(this.m_CharacterDataController, true);
					}
					this.m_PreViewTabIndex = this.m_ViewTab.SelectIdx;
					for (int i = 0; i < this.m_ViewTab.GetButtonNum(); i++)
					{
						this.m_ViewTab.SetIntaractable(false, i);
					}
				}
				else
				{
					this.m_ViewTab.SelectIdx = this.m_PreViewTabIndex;
				}
			}
		}

		// Token: 0x06002F2B RID: 12075 RVA: 0x000F7497 File Offset: 0x000F5897
		public void OnClickOpenStateDetailCallback()
		{
			this.ChangeStep(CharaDetailUI.eStep.StateDetail);
		}

		// Token: 0x06002F2C RID: 12076 RVA: 0x000F74A0 File Offset: 0x000F58A0
		public void OnClickCloseStateDetailCallback()
		{
			this.m_StateDetailGroup.Close();
		}

		// Token: 0x06002F2D RID: 12077 RVA: 0x000F74AD File Offset: 0x000F58AD
		public void OnClickIllustCallBack()
		{
			this.ChangeStep(CharaDetailUI.eStep.IllustViewIn);
			this.OnStartViewIllust.Call();
		}

		// Token: 0x0400363C RID: 13884
		private const float ILLUST_VIEW_FADETIME = 0.1f;

		// Token: 0x0400363D RID: 13885
		[SerializeField]
		[Tooltip("非推奨.")]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x0400363E RID: 13886
		[SerializeField]
		[Tooltip("メインウィンドウグループ.")]
		private UIGroup[] m_MainGroups;

		// Token: 0x0400363F RID: 13887
		[SerializeField]
		[Tooltip("メインウィンドウウィンドウボディ.")]
		private ScrollField m_MainGroupWindowBody;

		// Token: 0x04003640 RID: 13888
		[SerializeField]
		[Tooltip("ステータス詳細ウィンドウグループ.")]
		private CharaDetailStateDetailGroup m_StateDetailGroup;

		// Token: 0x04003641 RID: 13889
		[SerializeField]
		[Tooltip("イラスト表示時グループ.")]
		private UIGroup m_IllustViewGroup;

		// Token: 0x04003642 RID: 13890
		[SerializeField]
		[Tooltip("キャラ絵全画面表示中は消すオブジェクト")]
		private GameObject m_MenuObject;

		// Token: 0x04003643 RID: 13891
		[SerializeField]
		[Tooltip("イラスト表示時グループ内イラスト表示部.")]
		private CharaIllust m_ViewIllust;

		// Token: 0x04003644 RID: 13892
		[SerializeField]
		[Tooltip("キャラクタ情報表示パネル.")]
		protected EditAdditiveSceneCharacterInformationPanel m_CharaInfoPanel;

		// Token: 0x04003645 RID: 13893
		[SerializeField]
		[Tooltip("表示カテゴリを選択するタブグループ.")]
		private TabGroup m_TabGroup;

		// Token: 0x04003646 RID: 13894
		[SerializeField]
		private TabGroup m_ViewTab;

		// Token: 0x04003647 RID: 13895
		[SerializeField]
		[Tooltip("タブウィンドウの子コンテンツ")]
		private GameObject[] m_InfoObject;

		// Token: 0x04003648 RID: 13896
		[SerializeField]
		[Tooltip("ステータスコンテントのレイアウトグループ.リサイズが正常に行われない場合がある.")]
		private RectTransform m_StateContentLayoutGroup;

		// Token: 0x04003649 RID: 13897
		[SerializeField]
		[Tooltip("最初に開くタブ.主にここで指定するのはデバッグ用")]
		private int m_RequestTabIndex;

		// Token: 0x0400364A RID: 13898
		private CharaDetailUI.eStep m_Step;

		// Token: 0x0400364B RID: 13899
		private CharaDetailUI.eMode m_Mode = CharaDetailUI.eMode.Normal;

		// Token: 0x0400364C RID: 13900
		private EquipWeaponCharacterDataController m_CharacterDataController;

		// Token: 0x0400364D RID: 13901
		private bool m_IsDirty;

		// Token: 0x0400364E RID: 13902
		private int m_PreViewTabIndex;

		// Token: 0x0400364F RID: 13903
		private CharaDetailUI.eCategory m_Category;

		// Token: 0x04003650 RID: 13904
		private int m_Frames;

		// Token: 0x04003651 RID: 13905
		public Action OnStartViewIllust;

		// Token: 0x04003652 RID: 13906
		public Action OnEndViewIllust;

		// Token: 0x020008E3 RID: 2275
		public enum eStep
		{
			// Token: 0x04003654 RID: 13908
			Hide,
			// Token: 0x04003655 RID: 13909
			BGIn,
			// Token: 0x04003656 RID: 13910
			In,
			// Token: 0x04003657 RID: 13911
			Idle,
			// Token: 0x04003658 RID: 13912
			Out,
			// Token: 0x04003659 RID: 13913
			End,
			// Token: 0x0400365A RID: 13914
			StateDetail,
			// Token: 0x0400365B RID: 13915
			IllustViewIn,
			// Token: 0x0400365C RID: 13916
			IllustView,
			// Token: 0x0400365D RID: 13917
			IllustViewRotateFadeOut,
			// Token: 0x0400365E RID: 13918
			IllustViewRotateFadeIn,
			// Token: 0x0400365F RID: 13919
			IllustViewVertical,
			// Token: 0x04003660 RID: 13920
			IllustViewOut
		}

		// Token: 0x020008E4 RID: 2276
		public enum eMode
		{
			// Token: 0x04003662 RID: 13922
			Auto,
			// Token: 0x04003663 RID: 13923
			Normal,
			// Token: 0x04003664 RID: 13924
			StateOnly,
			// Token: 0x04003665 RID: 13925
			ProfileOnly,
			// Token: 0x04003666 RID: 13926
			GachaResult
		}

		// Token: 0x020008E5 RID: 2277
		private enum eCategory
		{
			// Token: 0x04003668 RID: 13928
			Status,
			// Token: 0x04003669 RID: 13929
			Profile,
			// Token: 0x0400366A RID: 13930
			Voice,
			// Token: 0x0400366B RID: 13931
			Max
		}
	}
}
