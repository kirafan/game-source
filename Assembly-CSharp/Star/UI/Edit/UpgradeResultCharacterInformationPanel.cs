﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000912 RID: 2322
	public class UpgradeResultCharacterInformationPanel : CharacterEditSceneCharacterInformationPanel
	{
		// Token: 0x0600303C RID: 12348 RVA: 0x000FAB2B File Offset: 0x000F8F2B
		protected override void ApplyResource(CharacterDataController cdc, CharacterDataController after)
		{
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
		}

		// Token: 0x0600303D RID: 12349 RVA: 0x000FAB38 File Offset: 0x000F8F38
		protected override void ApplyParameter(CharacterDataController cdc, CharacterDataController after)
		{
			CharacterDataController characterDataController = after;
			if (characterDataController == null)
			{
				characterDataController = cdc;
			}
			base.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			base.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			base.SetAfterLimitBreakIconValue(characterDataController.GetCharaLimitBreak());
			long[] nextMaxExps = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaExpDB.GetNextMaxExps(cdc.GetCharaLv(), characterDataController.GetCharaLv());
			base.SetExpData(0L, cdc.GetCharaLv(), cdc.GetCharaNowExp(), characterDataController.GetCharaLv(), characterDataController.GetCharaNowExp(), after.GetCharaMaxLv(), nextMaxExps);
			base.ApplyState(cdc, characterDataController);
		}

		// Token: 0x0600303E RID: 12350 RVA: 0x000FABCA File Offset: 0x000F8FCA
		protected override void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = false;
			this.m_IsDirtyParameter = false;
		}

		// Token: 0x04003716 RID: 14102
		[SerializeField]
		[Tooltip("アイテム使用数情報.")]
		private SelectItemPanel m_InfoSelectItem;
	}
}
