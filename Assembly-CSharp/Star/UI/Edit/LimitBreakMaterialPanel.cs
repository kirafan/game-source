﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008F8 RID: 2296
	public class LimitBreakMaterialPanel : CharaEditSceneMaterialPanel
	{
		// Token: 0x06002FA3 RID: 12195 RVA: 0x000F8B48 File Offset: 0x000F6F48
		public override void Setup(EquipWeaponCharacterDataController cdc)
		{
			long charaMngId = cdc.GetCharaMngId();
			int charaLimitBreak = cdc.GetCharaLimitBreak();
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(cdc.GetCharaId());
			CharacterLimitBreakListDB_Param param2 = dbMng.CharaLimitBreakListDB.GetParam(param.m_LimitBreakRecipeID);
			this.m_NecessaryFunds.SetValue(param2.m_Amounts[charaLimitBreak].ToString("#,0"));
			bool customButtonInteractable = true;
			this.m_ShortAmount.SetActive(false);
			if (userDataMng.UserData.IsShortOfGold((long)param2.m_Amounts[charaLimitBreak]))
			{
				customButtonInteractable = false;
			}
			for (int i = 0; i < this.m_ItemIconButtons.Length; i++)
			{
				int num = -1;
				int num2 = 0;
				string itemName = "-----";
				switch (i)
				{
				case 0:
					num = param2.m_ItemIDs_1[charaLimitBreak];
					num2 = param2.m_ItemNums_1[charaLimitBreak];
					break;
				case 1:
					num = param2.m_ItemIDs_2[charaLimitBreak];
					num2 = param2.m_ItemNums_2[charaLimitBreak];
					break;
				case 2:
					num = param2.m_ItemIDs_3[charaLimitBreak];
					num2 = param2.m_ItemNums_3[charaLimitBreak];
					break;
				case 3:
					num = dbMng.TitleListDB.GetParam(dbMng.NamedListDB.GetTitleType((eCharaNamedType)dbMng.CharaListDB.GetParam(userDataMng.GetUserCharaData(charaMngId).Param.CharaID).m_NamedType)).m_LimitBreakItemID;
					num2 = param2.m_TitleItemNums[charaLimitBreak];
					break;
				}
				if (num2 > 0 && num != -1)
				{
					int itemNum = userDataMng.GetItemNum(num);
					ItemListDB itemListDB = dbMng.ItemListDB;
					for (int j = 0; j < itemListDB.m_Params.Length; j++)
					{
						ItemListDB_Param itemListDB_Param = itemListDB.m_Params[j];
						if (itemListDB_Param.m_ID == num)
						{
							itemName = itemListDB_Param.m_Name;
						}
					}
					this.m_ItemIconButtons[i].Setup();
					this.m_ItemIconButtons[i].Apply(num, itemName, num2, itemNum);
					this.m_ItemIconButtons[i].SetCustomButtonInteractable(customButtonInteractable);
					if (num2 > itemNum)
					{
						this.m_ItemIconButtons[i].SetCustomButtonInteractable(false);
					}
				}
				else
				{
					this.m_ItemIconButtons[i].SetActive(false);
				}
			}
			bool flag = EditUtility.CanLimitBreak(charaMngId);
			this.m_CantLB.SetActive(false);
			if (!flag)
			{
				for (int k = 0; k < this.m_ItemIconButtons.Length; k++)
				{
					this.m_ItemIconButtons[k].SetCustomButtonInteractable(false);
				}
			}
			this.m_TitleImage.Setup();
			this.m_TitleImage.Prepare(cdc.GetCharaTitleType());
			this.m_TitleImage.AutoDisplay();
			this.SetIsSelecting(-1);
		}

		// Token: 0x06002FA4 RID: 12196 RVA: 0x000F8E1C File Offset: 0x000F721C
		public override int GetSelectButtonIdx()
		{
			return this.m_SelectButtonIdx;
		}

		// Token: 0x06002FA5 RID: 12197 RVA: 0x000F8E24 File Offset: 0x000F7224
		protected override CustomButton GetDecideButton()
		{
			return this.m_DecideButton;
		}

		// Token: 0x06002FA6 RID: 12198 RVA: 0x000F8E2C File Offset: 0x000F722C
		public void SetIsSelecting(int idx)
		{
			for (int i = 0; i < this.m_ItemIconButtons.Length; i++)
			{
				this.m_ItemIconButtons[i].SetIsSelecting(i == idx);
			}
			if (this.m_DecideButton != null)
			{
				this.m_DecideButton.Interactable = (0 <= idx && idx < this.m_ItemIconButtons.Length);
			}
		}

		// Token: 0x06002FA7 RID: 12199 RVA: 0x000F8E94 File Offset: 0x000F7294
		public override void OnClickSelectButton()
		{
			this.OnClickSelectButton(this.m_SelectButtonIdx);
		}

		// Token: 0x06002FA8 RID: 12200 RVA: 0x000F8EA2 File Offset: 0x000F72A2
		public override void OnClickSelectButton(int idx)
		{
			if (this.m_SelectButtonIdx != idx)
			{
				this.m_SelectButtonIdx = idx;
			}
			else
			{
				this.m_SelectButtonIdx = -1;
			}
			this.SetIsSelecting(this.m_SelectButtonIdx);
			base.ExecuteOnMaterialsChanged();
		}

		// Token: 0x040036B5 RID: 14005
		[SerializeField]
		private TextField m_NecessaryFunds;

		// Token: 0x040036B6 RID: 14006
		[SerializeField]
		private LimitBreakItemInformation[] m_ItemIconButtons;

		// Token: 0x040036B7 RID: 14007
		[SerializeField]
		private GameObject m_CantLB;

		// Token: 0x040036B8 RID: 14008
		[SerializeField]
		private GameObject m_ShortAmount;

		// Token: 0x040036B9 RID: 14009
		[SerializeField]
		private TitleImage m_TitleImage;

		// Token: 0x040036BA RID: 14010
		[SerializeField]
		private CustomButton m_DecideButton;

		// Token: 0x040036BB RID: 14011
		protected int m_SelectButtonIdx = -1;
	}
}
