﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x02000920 RID: 2336
	public class BattlePartyEditStandardControlPanel : PartyEditControlPanel
	{
		// Token: 0x06003074 RID: 12404 RVA: 0x000FBD4C File Offset: 0x000FA14C
		public override void SetMode(int mode)
		{
			base.SetMode(mode);
			this.m_ElementDataPanel.SetMode(mode);
			if (mode == 0)
			{
				this.m_DecideButton.gameObject.SetActive(false);
			}
			else if (mode == 1)
			{
				this.m_DecideButton.gameObject.SetActive(true);
				this.m_DecideButton.Interactable = EditUtility.IsAvailableParty(this.m_MainGroup.SelectPartyIndex);
			}
			this.m_ResetButton.Interactable = !EditUtility.IsEmptyBattleParty(this.m_MainGroup.SelectPartyIndex);
			this.m_RepaintFrame = this.m_ElementDataPanel.GetRepaintFrame() + 150;
		}

		// Token: 0x06003075 RID: 12405 RVA: 0x000FBDF0 File Offset: 0x000FA1F0
		public void OnChangePage()
		{
			this.m_DecideButton.Interactable = EditUtility.IsAvailableParty(this.m_MainGroup.SelectPartyIndex);
			this.m_ResetButton.Interactable = !EditUtility.IsEmptyBattleParty(this.m_MainGroup.SelectPartyIndex);
		}

		// Token: 0x04003757 RID: 14167
		public const int BATTLEPARTYEDIT_STANDARDCONTROLLPANEL_MODE_EDITSCENE = 0;

		// Token: 0x04003758 RID: 14168
		public const int BATTLEPARTYEDIT_STANDARDCONTROLLPANEL_MODE_QUESTSCENE = 1;

		// Token: 0x04003759 RID: 14169
		[SerializeField]
		private PartyEditMainGroup m_MainGroup;

		// Token: 0x0400375A RID: 14170
		[SerializeField]
		private PartyEditControlPanelElementDataField m_ElementDataPanel;

		// Token: 0x0400375B RID: 14171
		[SerializeField]
		protected CustomButton m_ResetButton;

		// Token: 0x0400375C RID: 14172
		[SerializeField]
		private CustomButton m_DecideButton;
	}
}
