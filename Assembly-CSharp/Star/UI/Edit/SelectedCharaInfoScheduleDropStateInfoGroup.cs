﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020007CA RID: 1994
	public class SelectedCharaInfoScheduleDropStateInfoGroup : SelectedCharaInfoDetailInfoGroupBaseExGen<SelectedCharaInfoScheduleDropStateInfo, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase>
	{
		// Token: 0x06002952 RID: 10578 RVA: 0x000DB3A3 File Offset: 0x000D97A3
		public override void Setup()
		{
			base.SetupProcess(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.ScheduleDropState, ">>お土産タイプ.<<");
		}
	}
}
