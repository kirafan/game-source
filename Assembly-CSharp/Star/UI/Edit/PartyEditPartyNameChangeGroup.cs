﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000928 RID: 2344
	public class PartyEditPartyNameChangeGroup : UIGroup
	{
		// Token: 0x06003088 RID: 12424 RVA: 0x000FC78B File Offset: 0x000FAB8B
		public InputField GetInputField()
		{
			return this.m_NameChangeInputField;
		}

		// Token: 0x06003089 RID: 12425 RVA: 0x000FC793 File Offset: 0x000FAB93
		public CustomButton GetDecideButton()
		{
			return this.m_NameChangeDecideButton;
		}

		// Token: 0x0600308A RID: 12426 RVA: 0x000FC79B File Offset: 0x000FAB9B
		public string GetInputFieldText()
		{
			if (this.m_NameChangeInputFieldEx == null)
			{
				return this.m_NameChangeInputField.text;
			}
			return this.m_NameChangeInputFieldEx.GetText();
		}

		// Token: 0x0600308B RID: 12427 RVA: 0x000FC7C5 File Offset: 0x000FABC5
		public void SetInputFieldText(string text)
		{
			if (this.m_NameChangeInputFieldEx == null)
			{
				this.m_NameChangeInputField.text = text;
				return;
			}
			this.m_NameChangeInputFieldEx.SetText(text);
		}

		// Token: 0x0400376C RID: 14188
		[SerializeField]
		private InputField m_NameChangeInputField;

		// Token: 0x0400376D RID: 14189
		[SerializeField]
		private TextInput m_NameChangeInputFieldEx;

		// Token: 0x0400376E RID: 14190
		[SerializeField]
		private CustomButton m_NameChangeDecideButton;
	}
}
