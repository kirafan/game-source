﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x0200091C RID: 2332
	public class MasterOrbInformation : MonoBehaviour
	{
		// Token: 0x06003065 RID: 12389 RVA: 0x000FB5E8 File Offset: 0x000F99E8
		public void ApplyCurrentEquip(long m_CurrentEquipMasterOrbMngID = -1L)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			if (m_CurrentEquipMasterOrbMngID != -1L)
			{
				UserMasterOrbData userMasterOrbData = userDataMng.GetUserMasterOrbData(m_CurrentEquipMasterOrbMngID);
				MasterOrbListDB_Param param = dbMng.MasterOrbListDB.GetParam(userMasterOrbData.OrbID);
				this.m_Icon.Apply(userMasterOrbData.OrbID);
				this.m_NameText.text = param.m_Name;
				this.m_LvText.text = userMasterOrbData.Lv.ToString() + " / " + param.m_LimitLv.ToString();
				long currentExp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbExpDB.GetCurrentExp(userMasterOrbData.Lv, userMasterOrbData.Exp, param.m_ExpTableID);
				this.m_ExpGauge.SetExpData(userMasterOrbData.Lv, currentExp, param.m_LimitLv, (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.MasterOrbExpDB.GetNextExp(userMasterOrbData.Lv, param.m_ExpTableID));
			}
			else
			{
				this.m_Icon.Apply(-1);
				this.m_NameText.text = "装備無し";
				this.m_LvText.text = string.Empty;
				this.m_ExpGauge.SetExpData(0, 0L, 0, 0L);
			}
		}

		// Token: 0x04003743 RID: 14147
		[SerializeField]
		private OrbIcon m_Icon;

		// Token: 0x04003744 RID: 14148
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003745 RID: 14149
		[SerializeField]
		private Text m_LvText;

		// Token: 0x04003746 RID: 14150
		[SerializeField]
		private ExpGauge m_ExpGauge;
	}
}
