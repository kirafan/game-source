﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000929 RID: 2345
	public class PartyEditPartyNamePanel : MonoBehaviour
	{
		// Token: 0x0600308D RID: 12429 RVA: 0x000FC800 File Offset: 0x000FAC00
		public void Setup()
		{
		}

		// Token: 0x0600308E RID: 12430 RVA: 0x000FC802 File Offset: 0x000FAC02
		private void Update()
		{
			if (0 < this.m_RebuildFrames)
			{
				this.m_RebuildFrames--;
				LayoutRebuilder.MarkLayoutForRebuild(base.transform as RectTransform);
			}
		}

		// Token: 0x0600308F RID: 12431 RVA: 0x000FC82E File Offset: 0x000FAC2E
		public string GetPartyName()
		{
			return this.m_PartyName.text;
		}

		// Token: 0x06003090 RID: 12432 RVA: 0x000FC83B File Offset: 0x000FAC3B
		public void SetPartyName(string partyName)
		{
			this.m_PartyName.text = partyName;
		}

		// Token: 0x06003091 RID: 12433 RVA: 0x000FC849 File Offset: 0x000FAC49
		public void SetTouchInteractive(bool interactive)
		{
			this.m_Button.enabled = interactive;
			this.m_GoIcon.SetActive(interactive);
			this.m_RebuildFrames = 30;
		}

		// Token: 0x0400376F RID: 14191
		[SerializeField]
		[Tooltip("現状直接制御している箇所があるが将来的には全てこちらに統一したい.")]
		private Text m_PartyName;

		// Token: 0x04003770 RID: 14192
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04003771 RID: 14193
		[SerializeField]
		private GameObject m_GoIcon;

		// Token: 0x04003772 RID: 14194
		private int m_RebuildFrames = -1;
	}
}
