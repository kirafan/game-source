﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007AE RID: 1966
	public class SelectedCharaInfoTitle : MonoBehaviour
	{
		// Token: 0x0600288A RID: 10378 RVA: 0x000D8BEC File Offset: 0x000D6FEC
		public void Constructor(bool activeClassIcon = true, bool activeElementIcon = true, bool activeRareStar = true, bool activeName = true, bool activeUnderLine = true, float rareStarClonePositionY = 12f)
		{
			this.m_ClassIcon = ((!activeClassIcon) ? null : this.m_ClassIcon);
			this.m_ElementIcon = ((!activeElementIcon) ? null : this.m_ElementIcon);
			this.m_NameText = ((!activeName) ? null : this.m_NameText);
			this.m_UnderLine = ((!activeUnderLine) ? null : this.m_UnderLine);
		}

		// Token: 0x0600288B RID: 10379 RVA: 0x000D8C5B File Offset: 0x000D705B
		public void Setup(int charaMngId)
		{
			this.Setup(null);
		}

		// Token: 0x0600288C RID: 10380 RVA: 0x000D8C64 File Offset: 0x000D7064
		public void Setup(UserCharacterData data)
		{
			this.Setup();
		}

		// Token: 0x0600288D RID: 10381 RVA: 0x000D8C6C File Offset: 0x000D706C
		public void Setup()
		{
			if (this.m_RareStarCloner != null)
			{
				if (this.m_RareStarCloner.IsExistPrefab())
				{
					this.m_RareStarCloner.Setup(false);
					this.m_RareStarComponent = this.m_RareStarCloner.GetInst<RareStar>();
					this.m_RareStarComponent.GetComponent<HorizontalOrVerticalLayoutGroup>().spacing = 4f;
				}
			}
			else if (this.m_RareStar != null)
			{
				this.m_RareStarComponent = this.m_RareStar;
			}
			if (this.m_WeaponIconCloner != null && this.m_WeaponIconCloner.IsExistPrefab())
			{
				this.m_WeaponIconCloner.Setup(false);
				this.m_WeaponIconComponent = this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>();
			}
			this.m_ObjectsArray = new List<GameObject>();
			this.m_ObjectsArray.Add((!(this.m_TitleImage != null)) ? null : this.m_TitleImage.gameObject);
			this.m_ObjectsArray.Add((!(this.m_ClassIcon != null)) ? null : this.m_ClassIcon.gameObject);
			this.m_ObjectsArray.Add((!(this.m_ElementIcon != null)) ? null : this.m_ElementIcon.gameObject);
			this.m_ObjectsArray.Add((!(this.m_RareStarComponent != null)) ? null : this.m_RareStarComponent.gameObject);
			this.m_ObjectsArray.Add((!(this.m_RareIcon != null)) ? null : this.m_RareIcon.gameObject);
			this.m_ObjectsArray.Add((!(this.m_NameText != null)) ? null : this.m_NameText.gameObject);
			this.m_ObjectsArray.Add((!(this.m_CostText != null)) ? null : this.m_CostText.gameObject);
			this.m_ObjectsArray.Add((!(this.m_WeaponIconComponent != null)) ? null : this.m_WeaponIconComponent.gameObject);
			this.m_ObjectsArray.Add((!(this.m_BackGround != null)) ? null : this.m_BackGround.gameObject);
			if (this.m_BoxBackGround != null)
			{
				for (int i = 0; i < this.m_BoxBackGround.Length; i++)
				{
					this.m_ObjectsArray.Add((!(this.m_BoxBackGround[i] != null)) ? null : this.m_BoxBackGround[i].gameObject);
				}
			}
			this.m_ObjectsArray.Add((!(this.m_UnderLine != null)) ? null : this.m_UnderLine.gameObject);
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Setup();
			}
			for (int j = 0; j < this.m_ObjectsArray.Count; j++)
			{
				if (this.m_ObjectsArray[j] == null)
				{
				}
			}
		}

		// Token: 0x0600288E RID: 10382 RVA: 0x000D8FA2 File Offset: 0x000D73A2
		public void Destroy()
		{
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Destroy();
			}
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Destroy();
			}
		}

		// Token: 0x0600288F RID: 10383 RVA: 0x000D8FDC File Offset: 0x000D73DC
		public void SetTitleImage(eTitleType title)
		{
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Prepare(title);
			}
		}

		// Token: 0x06002890 RID: 10384 RVA: 0x000D8FFB File Offset: 0x000D73FB
		public void SetClassIcon(eClassType classType)
		{
			if (this.m_ClassIcon != null)
			{
				this.m_ClassIcon.Apply(classType);
			}
		}

		// Token: 0x06002891 RID: 10385 RVA: 0x000D901A File Offset: 0x000D741A
		public void SetElementIcon(eElementType elementType)
		{
			if (this.m_ElementIcon != null)
			{
				this.m_ElementIcon.Apply(elementType);
			}
		}

		// Token: 0x06002892 RID: 10386 RVA: 0x000D9039 File Offset: 0x000D7439
		public void SetCharacterRareStars(eRare rare)
		{
			if (this.m_RareStarComponent != null)
			{
				this.m_RareStarComponent.Apply(rare);
			}
			if (this.m_RareIcon != null)
			{
				this.m_RareIcon.Apply(rare);
			}
		}

		// Token: 0x06002893 RID: 10387 RVA: 0x000D9075 File Offset: 0x000D7475
		public void SetName(string name)
		{
			if (this.m_NameText != null)
			{
				this.m_NameText.text = name;
			}
		}

		// Token: 0x06002894 RID: 10388 RVA: 0x000D9094 File Offset: 0x000D7494
		public void SetCostText(int cost)
		{
			if (this.m_CostText != null)
			{
				this.m_CostText.text = cost.ToString();
			}
		}

		// Token: 0x06002895 RID: 10389 RVA: 0x000D90C0 File Offset: 0x000D74C0
		public void SetCostText(int charaCost, int weaponCost)
		{
			if (this.m_CostText != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Length = 0;
				stringBuilder.Append(charaCost.ToString().PadLeft(3));
				if (0 < weaponCost)
				{
					stringBuilder.Append("+");
					stringBuilder.Append(weaponCost.ToString().PadLeft(2));
				}
				this.m_CostText.text = stringBuilder.ToString();
			}
		}

		// Token: 0x06002896 RID: 10390 RVA: 0x000D9143 File Offset: 0x000D7543
		public void SetWeaponIcon(int weaponId)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.Apply(weaponId);
			}
		}

		// Token: 0x06002897 RID: 10391 RVA: 0x000D9162 File Offset: 0x000D7562
		public void SetWeaponIconRareStarActive(bool active)
		{
			if (this.m_WeaponIconComponent != null)
			{
				this.m_WeaponIconComponent.GetComponentInChildren<RareStar>().gameObject.SetActive(active);
			}
		}

		// Token: 0x06002898 RID: 10392 RVA: 0x000D918B File Offset: 0x000D758B
		public void SetBackGround(Sprite background)
		{
			if (this.m_BackGround != null)
			{
				this.m_BackGround.sprite = background;
			}
		}

		// Token: 0x06002899 RID: 10393 RVA: 0x000D91AC File Offset: 0x000D75AC
		public void SetBoxBackGround(Sprite boxBackgroundSprite)
		{
			if (this.m_BoxBackGround != null)
			{
				for (int i = 0; i < this.m_BoxBackGround.Length; i++)
				{
					this.m_BoxBackGround[i].sprite = boxBackgroundSprite;
				}
			}
		}

		// Token: 0x0600289A RID: 10394 RVA: 0x000D91EB File Offset: 0x000D75EB
		public void SetUnderLine(Sprite underLine)
		{
			if (this.m_UnderLine != null)
			{
				this.m_UnderLine.sprite = underLine;
			}
		}

		// Token: 0x04002F84 RID: 12164
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private TitleImage m_TitleImage;

		// Token: 0x04002F85 RID: 12165
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private ClassIcon m_ClassIcon;

		// Token: 0x04002F86 RID: 12166
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private ElementIcon m_ElementIcon;

		// Token: 0x04002F87 RID: 12167
		[SerializeField]
		[Tooltip("nullの場合は生成されない.")]
		private PrefabCloner m_RareStarCloner;

		// Token: 0x04002F88 RID: 12168
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private RareStar m_RareStar;

		// Token: 0x04002F89 RID: 12169
		[SerializeField]
		private RareIcon m_RareIcon;

		// Token: 0x04002F8A RID: 12170
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private Text m_NameText;

		// Token: 0x04002F8B RID: 12171
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private Text m_CostText;

		// Token: 0x04002F8C RID: 12172
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x04002F8D RID: 12173
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private Image m_BackGround;

		// Token: 0x04002F8E RID: 12174
		[SerializeField]
		[Tooltip("nullの場合は制御しない.4分割されている背景.")]
		private Image[] m_BoxBackGround;

		// Token: 0x04002F8F RID: 12175
		[SerializeField]
		[Tooltip("nullの場合は制御しない.")]
		private Image m_UnderLine;

		// Token: 0x04002F90 RID: 12176
		private List<GameObject> m_ObjectsArray;

		// Token: 0x04002F91 RID: 12177
		private RareStar m_RareStarComponent;

		// Token: 0x04002F92 RID: 12178
		private WeaponIconWithFrame m_WeaponIconComponent;
	}
}
