﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x020008F5 RID: 2293
	public class LimitBreakCharacterInformationPanel : CharacterEditSceneCharacterInformationPanel
	{
		// Token: 0x06002F91 RID: 12177 RVA: 0x000F884C File Offset: 0x000F6C4C
		protected override void ApplyResource(CharacterDataController cdc, CharacterDataController after)
		{
			base.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
		}

		// Token: 0x06002F92 RID: 12178 RVA: 0x000F8858 File Offset: 0x000F6C58
		protected override void ApplyParameter(CharacterDataController cdc, CharacterDataController after)
		{
			CharacterDataController characterDataController = after;
			if (characterDataController == null)
			{
				characterDataController = cdc;
			}
			int charaLimitBreak = cdc.GetCharaLimitBreak();
			int charaMaxLv = cdc.GetCharaMaxLv();
			int uniqueSkillMaxLv = cdc.GetUniqueSkillMaxLv();
			int[] classSkillLevels = cdc.GetClassSkillLevels();
			int charaLimitBreak2 = characterDataController.GetCharaLimitBreak();
			int charaMaxLv2 = characterDataController.GetCharaMaxLv();
			int uniqueSkillMaxLv2 = characterDataController.GetUniqueSkillMaxLv();
			int[] classSkillLevels2 = characterDataController.GetClassSkillLevels();
			base.SetBeforeLevel(cdc.GetCharaLv(), charaMaxLv, null);
			base.SetAfterLevel(cdc.GetCharaLv(), charaMaxLv2, new SelectedCharaInfoLevelInfo.EmphasisExpressionInfo
			{
				max = ((charaMaxLv >= charaMaxLv2) ? ((charaMaxLv2 >= charaMaxLv) ? SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down) : SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
			});
			base.SetBeforeLimitBreakIconValue(charaLimitBreak);
			base.SetAfterLimitBreakIconValue(charaLimitBreak2);
			SkillLearnData skillLearnData = cdc.GetUniqueSkillLearnData();
			base.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.UniquieSkill, 0, new SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument(SkillIcon.eSkillDBType.Player, skillLearnData.SkillID, cdc.GetCharaElementType(), new LevelInformation(skillLearnData.SkillLv, uniqueSkillMaxLv), new LevelInformation(skillLearnData.SkillLv, uniqueSkillMaxLv2)));
			for (int i = 0; i < cdc.GetClassSkillsCount(); i++)
			{
				skillLearnData = cdc.GetClassSkillLearnData(i);
				base.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.GeneralSkill, i, new SelectedCharaInfoSkillLevelBeforeAfterInfo.ApplyArgument(SkillIcon.eSkillDBType.Player, skillLearnData.SkillID, cdc.GetCharaElementType(), new LevelInformation(skillLearnData.SkillLv, classSkillLevels[i]), new LevelInformation(skillLearnData.SkillLv, classSkillLevels2[i])));
			}
		}

		// Token: 0x06002F93 RID: 12179 RVA: 0x000F89A5 File Offset: 0x000F6DA5
		protected override void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = false;
			this.m_IsDirtyParameter = false;
		}
	}
}
