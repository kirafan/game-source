﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007D8 RID: 2008
	public class SelectedCharaInfoFriendshipIcon : MonoBehaviour
	{
		// Token: 0x0600296D RID: 10605 RVA: 0x000DB9F9 File Offset: 0x000D9DF9
		public void Setup()
		{
		}

		// Token: 0x0600296E RID: 10606 RVA: 0x000DB9FB File Offset: 0x000D9DFB
		public void Destroy()
		{
		}

		// Token: 0x0600296F RID: 10607 RVA: 0x000DBA00 File Offset: 0x000D9E00
		public void SetState(SelectedCharaInfoFriendshipIcon.eState state)
		{
			if (state == SelectedCharaInfoFriendshipIcon.eState.Complete)
			{
				this.m_Complete.SetActive(true);
				this.m_NotComplete.SetActive(false);
			}
			else if (state == SelectedCharaInfoFriendshipIcon.eState.NotComplete)
			{
				this.m_Complete.SetActive(false);
				this.m_NotComplete.SetActive(true);
			}
			else
			{
				this.m_Complete.SetActive(false);
				this.m_NotComplete.SetActive(false);
			}
		}

		// Token: 0x04002FFC RID: 12284
		[SerializeField]
		private GameObject m_Complete;

		// Token: 0x04002FFD RID: 12285
		[SerializeField]
		private GameObject m_NotComplete;

		// Token: 0x020007D9 RID: 2009
		public enum eState
		{
			// Token: 0x04002FFF RID: 12287
			Complete,
			// Token: 0x04003000 RID: 12288
			NotComplete,
			// Token: 0x04003001 RID: 12289
			NotExist
		}
	}
}
