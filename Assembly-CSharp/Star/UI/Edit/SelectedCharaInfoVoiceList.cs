﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020007E5 RID: 2021
	public class SelectedCharaInfoVoiceList : MonoBehaviour
	{
		// Token: 0x060029BB RID: 10683 RVA: 0x000DCA76 File Offset: 0x000DAE76
		public void Setup()
		{
			this.m_ScrollView.Setup();
		}

		// Token: 0x060029BC RID: 10684 RVA: 0x000DCA83 File Offset: 0x000DAE83
		public void Destroy()
		{
			this.m_ScrollView.Destroy();
		}

		// Token: 0x060029BD RID: 10685 RVA: 0x000DCA90 File Offset: 0x000DAE90
		public void SetVoiceListItems(eCharaNamedType charaNamed, int nowFriendship)
		{
			foreach (ProfileVoiceListDB_Param profileVoiceListDB_Param in SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ProfileVoiceListDB.m_Params)
			{
				int in_needFriendship = -1;
				if (nowFriendship < profileVoiceListDB_Param.m_Friendship)
				{
					in_needFriendship = profileVoiceListDB_Param.m_Friendship;
				}
				SelectedCharaInfoVoiceListItemData item = new SelectedCharaInfoVoiceListItemData(charaNamed, profileVoiceListDB_Param.m_Category, profileVoiceListDB_Param.m_Title, profileVoiceListDB_Param.m_CueName, in_needFriendship);
				this.m_ScrollView.AddItem(item);
			}
		}

		// Token: 0x0400302D RID: 12333
		[SerializeField]
		private ScrollViewBase m_ScrollView;
	}
}
