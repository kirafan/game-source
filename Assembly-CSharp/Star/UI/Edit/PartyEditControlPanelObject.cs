﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x02000925 RID: 2341
	public class PartyEditControlPanelObject : MonoBehaviour
	{
		// Token: 0x0600307C RID: 12412 RVA: 0x000FB9F2 File Offset: 0x000F9DF2
		protected virtual void Update()
		{
			if (0 < this.m_RepaintFrame)
			{
				LayoutRebuilder.MarkLayoutForRebuild(base.transform as RectTransform);
				this.m_RepaintFrame--;
			}
		}

		// Token: 0x0600307D RID: 12413 RVA: 0x000FBA1E File Offset: 0x000F9E1E
		public virtual int GetRepaintFrame()
		{
			return this.m_RepaintFrame;
		}

		// Token: 0x0600307E RID: 12414 RVA: 0x000FBA26 File Offset: 0x000F9E26
		public virtual void SetMode(int mode)
		{
			if (mode != -2)
			{
				base.gameObject.SetActive(true);
			}
			else
			{
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x04003760 RID: 14176
		public const int PARTYEDIT_CONTROLLPANEL_MODE_DISACTIVE = -2;

		// Token: 0x04003761 RID: 14177
		public const int PARTYEDIT_CONTROLLPANEL_MODE_ACTIVE = -1;

		// Token: 0x04003762 RID: 14178
		public const int DEFAULT_REPAINT_FRAMES = 150;

		// Token: 0x04003763 RID: 14179
		protected int m_RepaintFrame;
	}
}
