﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Edit
{
	// Token: 0x020007D7 RID: 2007
	public class SelectedCharaInfoFriendship : SelectedCharaInfoHaveTitleField
	{
		// Token: 0x06002966 RID: 10598 RVA: 0x000DB7F8 File Offset: 0x000D9BF8
		public void SetFriendship(EquipWeaponCharacterDataController cdc)
		{
			if (cdc == null)
			{
				return;
			}
			this.SetFriendship(cdc.GetFriendship(), cdc.GetFriendshipExp(), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(cdc.GetCharaNamedType()).m_FriendshipTableID);
		}

		// Token: 0x06002967 RID: 10599 RVA: 0x000DB840 File Offset: 0x000D9C40
		public void SetFriendship(eCharaNamedType charaNamed)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null)
			{
				return;
			}
			this.SetFriendship(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(charaNamed));
		}

		// Token: 0x06002968 RID: 10600 RVA: 0x000DB86C File Offset: 0x000D9C6C
		public void SetFriendship(UserNamedData und)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null)
			{
				return;
			}
			if (und == null)
			{
				return;
			}
			NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(und.NamedType);
			this.SetFriendship(und.FriendShip, und.FriendShipExp, param.m_FriendshipTableID);
		}

		// Token: 0x06002969 RID: 10601 RVA: 0x000DB8C5 File Offset: 0x000D9CC5
		public void SetFriendship(int friendship, long exp, sbyte expTableID)
		{
			if (this.m_FriendshipGauge == null)
			{
				return;
			}
			this.m_FriendshipGauge.Apply(friendship, exp, expTableID);
			this.m_frame = 30;
		}

		// Token: 0x0600296A RID: 10602 RVA: 0x000DB8F0 File Offset: 0x000D9CF0
		[Obsolete("旧仕様.")]
		public void SetFriendship(int friendship, int maxFriendship, long nextExp)
		{
			this.m_FriendshipText.text = friendship.ToString();
			for (int i = 0; i < this.m_FriendshipIcons.Length; i++)
			{
				if (maxFriendship <= i)
				{
					this.m_FriendshipIcons[i].SetState(SelectedCharaInfoFriendshipIcon.eState.NotExist);
				}
				else if (i < friendship)
				{
					this.m_FriendshipIcons[i].SetState(SelectedCharaInfoFriendshipIcon.eState.Complete);
				}
				else
				{
					this.m_FriendshipIcons[i].SetState(SelectedCharaInfoFriendshipIcon.eState.NotComplete);
				}
			}
			bool active = false;
			if (this.m_NextExpValue != null && 0L < nextExp)
			{
				active = true;
				this.m_NextExpValue.text = nextExp.ToString();
			}
			this.m_NextTextGameObject.SetActive(active);
			this.m_NextExpValue.gameObject.SetActive(active);
			this.m_frame = 30;
		}

		// Token: 0x0600296B RID: 10603 RVA: 0x000DB9CA File Offset: 0x000D9DCA
		private void Update()
		{
			if (0 < this.m_frame)
			{
				this.m_frame--;
				LayoutRebuilder.MarkLayoutForRebuild(this.m_LayoutGroupTransform);
			}
		}

		// Token: 0x04002FF5 RID: 12277
		[SerializeField]
		private FriendshipGauge m_FriendshipGauge;

		// Token: 0x04002FF6 RID: 12278
		[SerializeField]
		private Text m_FriendshipText;

		// Token: 0x04002FF7 RID: 12279
		[SerializeField]
		private SelectedCharaInfoFriendshipIcon[] m_FriendshipIcons;

		// Token: 0x04002FF8 RID: 12280
		[SerializeField]
		private GameObject m_NextTextGameObject;

		// Token: 0x04002FF9 RID: 12281
		[SerializeField]
		private Text m_NextExpValue;

		// Token: 0x04002FFA RID: 12282
		[SerializeField]
		[Tooltip("レイアウト崩れが起こるため,子のレイアウト再計算を呼び出す際に必要.")]
		private RectTransform m_LayoutGroupTransform;

		// Token: 0x04002FFB RID: 12283
		private int m_frame;
	}
}
