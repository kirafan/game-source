﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x0200092F RID: 2351
	public class PartyPanelData : EnumerationPanelData
	{
		// Token: 0x0400377A RID: 14202
		public Action<PartyCharaPanelBase.eButton> m_OnClickCharaPanel;

		// Token: 0x0400377B RID: 14203
		public Action<PartyCharaPanelBase.eButton> m_OnHoldCharaPanel;
	}
}
