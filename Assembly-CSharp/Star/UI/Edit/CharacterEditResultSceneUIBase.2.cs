﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x0200090F RID: 2319
	public class CharacterEditResultSceneUIBase<ResultType> : CharacterEditResultSceneUIBase where ResultType : EditMain.EditAdditiveSceneResultData
	{
		// Token: 0x06003023 RID: 12323 RVA: 0x000FA620 File Offset: 0x000F8A20
		private void Start()
		{
		}

		// Token: 0x06003024 RID: 12324 RVA: 0x000FA622 File Offset: 0x000F8A22
		public virtual void Setup(ResultType result)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_Result = result;
			this.SetupCharacterInformationPanel();
			this.Apply(result);
		}

		// Token: 0x06003025 RID: 12325 RVA: 0x000FA662 File Offset: 0x000F8A62
		private void SetupCharacterInformationPanel()
		{
			this.m_CharaInfoPanel.Setup();
		}

		// Token: 0x06003026 RID: 12326 RVA: 0x000FA670 File Offset: 0x000F8A70
		protected virtual void Update()
		{
			switch (this.m_Step)
			{
			case CharacterEditResultSceneUIBase.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				for (int j = 0; j < this.m_UIGroups.Length; j++)
				{
					if (!this.m_UIGroups[j].IsDonePlayIn)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.m_MixPopUp.PlayIn(this.m_SuccessLevel);
					this.PlayInVoice();
					this.m_CharaInfoPanel.PlayExpGauge();
					this.m_CharaInfoPanel.PlayAfterLevelAnimation();
					this.ChangeStep(CharacterEditResultSceneUIBase.eStep.Idle);
				}
				break;
			}
			case CharacterEditResultSceneUIBase.eStep.Idle:
			{
				Vector2 vector;
				if ((this.m_CharaInfoPanel.GetAfterLevelAnimationState() == 2 || this.m_CharaInfoPanel.GetAfterLevelAnimationState() == -1) && InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0))
				{
					if (this.m_CharaInfoPanel.IsPlayingGauge())
					{
						this.m_CharaInfoPanel.SkipExpGaugePlaying();
					}
					else
					{
						this.PlayOut();
					}
				}
				break;
			}
			case CharacterEditResultSceneUIBase.eStep.Out:
			{
				bool flag2 = true;
				for (int k = 0; k < this.m_AnimUIPlayerArray.Length; k++)
				{
					if (this.m_AnimUIPlayerArray[k].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				for (int l = 0; l < this.m_UIGroups.Length; l++)
				{
					if (!this.m_UIGroups[l].IsDonePlayOut)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(CharacterEditResultSceneUIBase.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x06003027 RID: 12327 RVA: 0x000FA83C File Offset: 0x000F8C3C
		private void ChangeStep(CharacterEditResultSceneUIBase.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case CharacterEditResultSceneUIBase.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case CharacterEditResultSceneUIBase.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharacterEditResultSceneUIBase.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case CharacterEditResultSceneUIBase.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case CharacterEditResultSceneUIBase.eStep.End:
				this.m_CharaImage.Destroy();
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003028 RID: 12328 RVA: 0x000FA8ED File Offset: 0x000F8CED
		public override void PlayIn()
		{
			this.PlayInExec();
		}

		// Token: 0x06003029 RID: 12329 RVA: 0x000FA8F8 File Offset: 0x000F8CF8
		private void PlayInExec()
		{
			this.ChangeStep(CharacterEditResultSceneUIBase.eStep.In);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayIn();
			}
			for (int j = 0; j < this.m_UIGroups.Length; j++)
			{
				this.m_UIGroups[j].Open();
			}
			this.m_CharaInfoPanel.PlayInCharacterView(true);
		}

		// Token: 0x0600302A RID: 12330 RVA: 0x000FA964 File Offset: 0x000F8D64
		public override void PlayOut()
		{
			this.ChangeStep(CharacterEditResultSceneUIBase.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
			for (int j = 0; j < this.m_UIGroups.Length; j++)
			{
				this.m_UIGroups[j].Close();
			}
			this.m_CharaInfoPanel.SetEnableRenderCharacterView(false);
			this.m_MixPopUp.PlayOut();
		}

		// Token: 0x0600302B RID: 12331 RVA: 0x000FA9DB File Offset: 0x000F8DDB
		public override bool IsMenuEnd()
		{
			return this.m_Step == CharacterEditResultSceneUIBase.eStep.End;
		}

		// Token: 0x0600302C RID: 12332 RVA: 0x000FA9E8 File Offset: 0x000F8DE8
		protected void Apply(ResultType result)
		{
			CharacterDataController cdc = this.ConvertResultDataToBeforeCharacterData(result);
			CharacterDataController characterDataController = this.ConvertResultDataToAfterCharacterData(result);
			this.ApplyCharacterInformation(cdc, characterDataController);
			this.m_CharaInfoPanel.RequestLoadCharacterViewCharacterIllustOnly(characterDataController, false);
		}

		// Token: 0x0600302D RID: 12333 RVA: 0x000FAA1A File Offset: 0x000F8E1A
		protected virtual CharacterDataController ConvertResultDataToBeforeCharacterData(ResultType result)
		{
			return null;
		}

		// Token: 0x0600302E RID: 12334 RVA: 0x000FAA1D File Offset: 0x000F8E1D
		protected virtual CharacterDataController ConvertResultDataToAfterCharacterData(ResultType result)
		{
			return null;
		}

		// Token: 0x0600302F RID: 12335 RVA: 0x000FAA20 File Offset: 0x000F8E20
		protected void ApplyCharacterInformation(CharacterDataController cdc, CharacterDataController after)
		{
			this.m_CharaInfoPanel.OnDirtyParameter();
			this.m_CharaInfoPanel.Apply(cdc, after);
		}

		// Token: 0x06003030 RID: 12336 RVA: 0x000FAA3C File Offset: 0x000F8E3C
		protected void PlayInVoice()
		{
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			UserCharacterData userCharaData = inst.UserDataMng.GetUserCharaData(this.m_Result.m_CharaMngID);
			if (userCharaData != null)
			{
				inst.VoiceCtrl.Request(userCharaData.Param.NamedType, this.GetPlayInVoiceCueID(), true);
			}
		}

		// Token: 0x06003031 RID: 12337 RVA: 0x000FAA8F File Offset: 0x000F8E8F
		protected virtual eSoundVoiceListDB GetPlayInVoiceCueID()
		{
			return eSoundVoiceListDB.voice_006;
		}

		// Token: 0x0400370F RID: 14095
		[SerializeField]
		[Tooltip("廃止予定.")]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003710 RID: 14096
		[SerializeField]
		private UIGroup[] m_UIGroups;

		// Token: 0x04003711 RID: 14097
		[SerializeField]
		private BustImage m_CharaImage;

		// Token: 0x04003712 RID: 14098
		[SerializeField]
		[Tooltip("パネル左側.骨組み共通化部.")]
		protected BeforeAfterCharacterInformationPanel m_CharaInfoPanel;

		// Token: 0x04003713 RID: 14099
		[SerializeField]
		protected MixPopUp m_MixPopUp;

		// Token: 0x04003714 RID: 14100
		protected ResultType m_Result;

		// Token: 0x04003715 RID: 14101
		protected int m_SuccessLevel = -1;
	}
}
