﻿using System;

namespace Star.UI.Edit
{
	// Token: 0x02000939 RID: 2361
	public class PartyDetailPanel : EnumerationPanelConstGen<PartyDetailPanel, PartyDetailCharaPanel, PartyPanelData, PartyDetailPanelCore, PartyDetailPanelCore.SharedInstanceExOverride>
	{
		// Token: 0x060030E2 RID: 12514 RVA: 0x000FED8A File Offset: 0x000FD18A
		public override PartyDetailPanelCore CreateCore()
		{
			return new PartyDetailPanelCore();
		}

		// Token: 0x060030E3 RID: 12515 RVA: 0x000FED91 File Offset: 0x000FD191
		public override PartyDetailPanelCore.SharedInstanceExOverride CreateArgument()
		{
			return base.CreateArgument();
		}
	}
}
