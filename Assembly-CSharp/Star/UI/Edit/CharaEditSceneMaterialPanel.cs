﻿using System;
using UnityEngine;

namespace Star.UI.Edit
{
	// Token: 0x020008EA RID: 2282
	public abstract class CharaEditSceneMaterialPanel : MonoBehaviour
	{
		// Token: 0x14000073 RID: 115
		// (add) Token: 0x06002F44 RID: 12100 RVA: 0x000F78F4 File Offset: 0x000F5CF4
		// (remove) Token: 0x06002F45 RID: 12101 RVA: 0x000F792C File Offset: 0x000F5D2C
		public event Action OnMaterialsChanged;

		// Token: 0x14000074 RID: 116
		// (add) Token: 0x06002F46 RID: 12102 RVA: 0x000F7964 File Offset: 0x000F5D64
		// (remove) Token: 0x06002F47 RID: 12103 RVA: 0x000F799C File Offset: 0x000F5D9C
		public event Action<CharaEditSceneUIBase.eButton> onClickButton;

		// Token: 0x06002F48 RID: 12104 RVA: 0x000F79D2 File Offset: 0x000F5DD2
		public virtual void Setup(EquipWeaponCharacterDataController cdc)
		{
		}

		// Token: 0x06002F49 RID: 12105
		public abstract int GetSelectButtonIdx();

		// Token: 0x06002F4A RID: 12106
		protected abstract CustomButton GetDecideButton();

		// Token: 0x06002F4B RID: 12107 RVA: 0x000F79D4 File Offset: 0x000F5DD4
		public void SetDecideButtonInteractable(bool interactable)
		{
			CustomButton decideButton = this.GetDecideButton();
			if (decideButton == null)
			{
				return;
			}
			decideButton.Interactable = interactable;
		}

		// Token: 0x06002F4C RID: 12108
		public abstract void OnClickSelectButton();

		// Token: 0x06002F4D RID: 12109 RVA: 0x000F79FC File Offset: 0x000F5DFC
		public virtual void OnClickSelectButton(int idx)
		{
			this.ExecuteOnMaterialsChanged();
		}

		// Token: 0x06002F4E RID: 12110 RVA: 0x000F7A04 File Offset: 0x000F5E04
		public void OnClickDecideButton()
		{
			this.onClickButton.Call(CharaEditSceneUIBase.eButton.Decide);
		}

		// Token: 0x06002F4F RID: 12111 RVA: 0x000F7A12 File Offset: 0x000F5E12
		protected void ExecuteOnMaterialsChanged()
		{
			this.OnMaterialsChanged.Call();
		}
	}
}
