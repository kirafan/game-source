﻿using System;

namespace Star.UI
{
	// Token: 0x0200086E RID: 2158
	public class QuestEventTypeIcon : ASyncImage
	{
		// Token: 0x06002CBC RID: 11452 RVA: 0x000EC038 File Offset: 0x000EA438
		public void Apply(int eventType)
		{
			if (this.m_EventType != eventType)
			{
				this.Apply();
				if (eventType != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncQuestEventTypeIcon(eventType);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_EventType = eventType;
			}
		}

		// Token: 0x06002CBD RID: 11453 RVA: 0x000EC09E File Offset: 0x000EA49E
		public override void Destroy()
		{
			base.Destroy();
			this.m_EventType = -1;
		}

		// Token: 0x040033B5 RID: 13237
		protected int m_EventType = -1;
	}
}
