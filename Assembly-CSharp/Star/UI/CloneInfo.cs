﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007EC RID: 2028
	[Serializable]
	public class CloneInfo<CloneType> : BCloneInfo where CloneType : MonoBehaviour
	{
		// Token: 0x060029E1 RID: 10721 RVA: 0x000DCFA4 File Offset: 0x000DB3A4
		public override void Setup(Transform parent)
		{
			if (this.m_Prefab == null)
			{
				return;
			}
			if (this.m_FitCloneRect == null)
			{
				return;
			}
			Rect rect = this.m_FitCloneRect.rect;
			this.m_FitCloneRect.anchorMin = this.m_Prefab.GetComponent<RectTransform>().anchorMin;
			this.m_FitCloneRect.anchorMax = this.m_Prefab.GetComponent<RectTransform>().anchorMax;
			this.m_FitCloneRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, rect.width);
			this.m_FitCloneRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, rect.height);
			this.m_Clone = UnityEngine.Object.Instantiate<CloneType>(this.m_Prefab, parent, false);
			this.m_Clone.transform.localPosition = this.m_FitCloneRect.localPosition;
			this.m_Clone.GetComponent<RectTransform>().sizeDelta = this.m_FitCloneRect.sizeDelta;
		}

		// Token: 0x060029E2 RID: 10722 RVA: 0x000DD0A3 File Offset: 0x000DB4A3
		public override bool IsExistPrefab()
		{
			return this.m_Prefab != null;
		}

		// Token: 0x060029E3 RID: 10723 RVA: 0x000DD0B6 File Offset: 0x000DB4B6
		public override void SetLocalPositionX(float x)
		{
			this.m_Clone.transform.localPosX(x);
		}

		// Token: 0x060029E4 RID: 10724 RVA: 0x000DD0CF File Offset: 0x000DB4CF
		public override void SetLocalPositionY(float y)
		{
			this.m_Clone.transform.localPosY(y);
		}

		// Token: 0x04003043 RID: 12355
		public CloneType m_Prefab;

		// Token: 0x04003044 RID: 12356
		public RectTransform m_FitCloneRect;

		// Token: 0x04003045 RID: 12357
		[HideInInspector]
		public CloneType m_Clone;
	}
}
