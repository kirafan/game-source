﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009BE RID: 2494
	public class InfoBanner : MonoBehaviour
	{
		// Token: 0x060033DE RID: 13278 RVA: 0x00106F08 File Offset: 0x00105308
		public void Setup()
		{
			this.m_BannerScroll.Setup();
		}

		// Token: 0x060033DF RID: 13279 RVA: 0x00106F18 File Offset: 0x00105318
		public void RequestGetAndPrepare(Action OnResponse)
		{
			this.m_OnResponse = OnResponse;
			this.m_IsDoneRequest = false;
			this.m_BannerLoader = new InfoBannerLoader();
			this.m_BannerLoader.Prepare(InfoBannerLoader.eBannerType.Normal);
			this.m_BannerLoader.Request_InfoBanner(new Action(this.OnResponseCallBack));
			this.m_IsDoneSetupScroll = false;
		}

		// Token: 0x060033E0 RID: 13280 RVA: 0x00106F68 File Offset: 0x00105368
		private void OnResponseCallBack()
		{
			this.m_IsDoneRequest = true;
			this.m_OnResponse.Call();
		}

		// Token: 0x060033E1 RID: 13281 RVA: 0x00106F7C File Offset: 0x0010537C
		private void Update()
		{
			if (!this.m_BannerLoader.IsDonePrepare())
			{
				this.m_BannerLoader.UpdatePrepare();
			}
			if (!this.m_IsDoneSetupScroll && this.m_BannerLoader.IsDonePrepare() && this.m_IsDoneRequest)
			{
				this.m_BannerScroll.RemoveAll();
				this.m_BannerScroll.AddItem(new BannerScrollItemData(-1, null, null));
				for (int i = 0; i < this.m_BannerLoader.GetList().Count; i++)
				{
					this.m_BannerScroll.AddItem(new BannerScrollItemData(this.m_BannerLoader.GetList()[i].m_ID, this.m_BannerLoader.GetSprite(this.m_BannerLoader.GetList()[i].m_ImgID), this.m_BannerLoader.GetList()[i].m_Url));
				}
				this.m_IsDoneSetupScroll = true;
			}
		}

		// Token: 0x060033E2 RID: 13282 RVA: 0x0010706D File Offset: 0x0010546D
		public void Destroy()
		{
			this.m_BannerScroll.Destroy();
			if (this.m_BannerLoader != null)
			{
				this.m_BannerLoader.Destroy();
			}
		}

		// Token: 0x04003A05 RID: 14853
		[SerializeField]
		private GameObject m_Obj;

		// Token: 0x04003A06 RID: 14854
		[SerializeField]
		private InfiniteScroll m_BannerScroll;

		// Token: 0x04003A07 RID: 14855
		private InfoBannerLoader m_BannerLoader;

		// Token: 0x04003A08 RID: 14856
		private bool m_IsDoneRequest;

		// Token: 0x04003A09 RID: 14857
		public Action m_OnResponse;

		// Token: 0x04003A0A RID: 14858
		private bool m_IsDoneSetupScroll;
	}
}
