﻿using System;

namespace Star.UI
{
	// Token: 0x0200081E RID: 2078
	public abstract class ScrollItemData
	{
		// Token: 0x06002B3C RID: 11068 RVA: 0x000C41FF File Offset: 0x000C25FF
		public void SetIcon(ScrollItemIcon icon)
		{
			this.m_Icon = icon;
		}

		// Token: 0x06002B3D RID: 11069 RVA: 0x000C4208 File Offset: 0x000C2608
		public void RefreshItem()
		{
			if (this.m_Icon != null)
			{
				this.m_Icon.Refresh();
			}
		}

		// Token: 0x06002B3E RID: 11070 RVA: 0x000C4226 File Offset: 0x000C2626
		public virtual void OnClickItemCallBack()
		{
		}

		// Token: 0x0400319E RID: 12702
		private ScrollItemIcon m_Icon;
	}
}
