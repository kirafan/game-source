﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008D1 RID: 2257
	public class iTweenWrapperPlayArgument
	{
		// Token: 0x06002EBD RID: 11965 RVA: 0x000F4F6F File Offset: 0x000F336F
		public iTweenWrapperPlayArgument(GameObject in_target)
		{
			this.target = in_target;
		}

		// Token: 0x06002EBE RID: 11966 RVA: 0x000F4FA3 File Offset: 0x000F33A3
		public iTweenWrapperPlayArgument AddXParameter(float value)
		{
			return this.AddParameter("x", value);
		}

		// Token: 0x06002EBF RID: 11967 RVA: 0x000F4FB1 File Offset: 0x000F33B1
		public iTweenWrapperPlayArgument AddYParameter(float value)
		{
			return this.AddParameter("y", value);
		}

		// Token: 0x06002EC0 RID: 11968 RVA: 0x000F4FBF File Offset: 0x000F33BF
		public iTweenWrapperPlayArgument AddZParameter(float value)
		{
			return this.AddParameter("z", value);
		}

		// Token: 0x06002EC1 RID: 11969 RVA: 0x000F4FCD File Offset: 0x000F33CD
		public iTweenWrapperPlayArgument AddRParameter(float value)
		{
			return this.AddParameter("r", value);
		}

		// Token: 0x06002EC2 RID: 11970 RVA: 0x000F4FDB File Offset: 0x000F33DB
		public iTweenWrapperPlayArgument AddGParameter(float value)
		{
			return this.AddParameter("g", value);
		}

		// Token: 0x06002EC3 RID: 11971 RVA: 0x000F4FE9 File Offset: 0x000F33E9
		public iTweenWrapperPlayArgument AddBParameter(float value)
		{
			return this.AddParameter("b", value);
		}

		// Token: 0x06002EC4 RID: 11972 RVA: 0x000F4FF7 File Offset: 0x000F33F7
		public iTweenWrapperPlayArgument AddAParameter(float value)
		{
			return this.AddParameter("a", value);
		}

		// Token: 0x06002EC5 RID: 11973 RVA: 0x000F5008 File Offset: 0x000F3408
		public iTweenWrapperPlayArgument AddParameter(string paramName, float value)
		{
			if (this.parameters == null)
			{
				this.parameters = new List<iTweenParameter>();
			}
			for (int i = 0; i < this.parameters.Count; i++)
			{
				if (this.parameters[i].paramName == paramName)
				{
					this.parameters[i].value = value;
					break;
				}
			}
			this.parameters.Add(new iTweenParameter(paramName, value));
			return this;
		}

		// Token: 0x06002EC6 RID: 11974 RVA: 0x000F508D File Offset: 0x000F348D
		public int HowManyParameters()
		{
			if (this.parameters == null)
			{
				return -1;
			}
			return this.parameters.Count;
		}

		// Token: 0x06002EC7 RID: 11975 RVA: 0x000F50A7 File Offset: 0x000F34A7
		public iTweenParameter GetParameter(int index)
		{
			if (this.parameters == null)
			{
				return null;
			}
			if (index < 0 || this.parameters.Count < index)
			{
				return null;
			}
			return this.parameters[index];
		}

		// Token: 0x06002EC8 RID: 11976 RVA: 0x000F50DC File Offset: 0x000F34DC
		public iTweenParameter GetParameter(string paramName)
		{
			if (this.parameters == null)
			{
				return null;
			}
			for (int i = 0; i < this.parameters.Count; i++)
			{
				if (this.parameters[i].paramName == paramName)
				{
					return this.parameters[i];
				}
			}
			return null;
		}

		// Token: 0x040035D0 RID: 13776
		public GameObject target;

		// Token: 0x040035D1 RID: 13777
		public bool isLocal = true;

		// Token: 0x040035D2 RID: 13778
		private List<iTweenParameter> parameters = new List<iTweenParameter>();

		// Token: 0x040035D3 RID: 13779
		public float time;

		// Token: 0x040035D4 RID: 13780
		public iTween.EaseType easeType = iTween.EaseType.linear;

		// Token: 0x040035D5 RID: 13781
		public float delay = 0.001f;

		// Token: 0x040035D6 RID: 13782
		public iTween.LoopType loopType;

		// Token: 0x040035D7 RID: 13783
		public string onupdate;

		// Token: 0x040035D8 RID: 13784
		public object onupdatetarget;

		// Token: 0x040035D9 RID: 13785
		public OniTweenCompleteDelegate oncompletecallback;

		// Token: 0x040035DA RID: 13786
		public bool ignoretimescale;
	}
}
