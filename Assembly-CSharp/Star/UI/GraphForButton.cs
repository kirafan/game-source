﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200084F RID: 2127
	public class GraphForButton : MonoBehaviour
	{
		// Token: 0x06002C4F RID: 11343 RVA: 0x000E9E9C File Offset: 0x000E829C
		public void Prepare()
		{
			this.m_Graphics = base.GetComponentsInChildren<Graphic>(true);
			this.m_DefaultColors = new Color[this.m_Graphics.Length];
			for (int i = 0; i < this.m_Graphics.Length; i++)
			{
				this.m_DefaultColors[i] = this.m_Graphics[i].color;
			}
			switch (this.m_Preset)
			{
			case GraphForButton.ePreset.CommonButtonBase:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = true;
				this.m_FixedDefaultColor = GraphForButton.BASE_COLOR;
				this.m_RenderPress = true;
				this.m_PressColor = GraphForButton.TEXT_COLOR;
				this.m_RenderDisable = true;
				this.m_DisableColor = GraphForButton.DISABLE_COLOR;
				this.m_DecidedColor = GraphForButton.TEXT_COLOR;
				break;
			case GraphForButton.ePreset.CommonButtonText:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = true;
				this.m_FixedDefaultColor = GraphForButton.TEXT_COLOR;
				this.m_RenderPress = true;
				this.m_PressColor = GraphForButton.TEXT_COLOR;
				this.m_RenderDisable = true;
				this.m_DisableColor = GraphForButton.DISABLE_TEXT_COLOR;
				this.m_DecidedColor = GraphForButton.BASE_COLOR;
				break;
			case GraphForButton.ePreset.CommonButtonShadow:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = false;
				this.m_RenderPress = false;
				this.m_RenderDisable = false;
				break;
			case GraphForButton.ePreset.Icon:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = false;
				this.m_RenderPress = true;
				this.m_PressColor = Color.gray;
				this.m_RenderDisable = true;
				this.m_DisableColor = Color.gray;
				this.m_DecidedColor = GraphForButton.BASE_COLOR;
				break;
			case GraphForButton.ePreset.ToggleButtonBase:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = true;
				this.m_FixedDefaultColor = GraphForButton.BASE_COLOR;
				this.m_RenderPress = true;
				this.m_PressColor = GraphForButton.TOGGLE_DECIDED_COLOR;
				this.m_RenderDisable = true;
				this.m_DisableColor = GraphForButton.DISABLE_TEXT_COLOR;
				this.m_DecidedColor = GraphForButton.TOGGLE_DECIDED_COLOR;
				break;
			case GraphForButton.ePreset.ToggleButtonText:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = true;
				this.m_FixedDefaultColor = GraphForButton.TEXT_COLOR;
				this.m_RenderPress = true;
				this.m_PressColor = GraphForButton.BASE_COLOR;
				this.m_RenderDisable = true;
				this.m_DisableColor = GraphForButton.DISABLE_TEXT_COLOR;
				this.m_DecidedColor = GraphForButton.BASE_COLOR;
				break;
			case GraphForButton.ePreset.TabButtonText:
				this.m_RenderDefault = true;
				this.m_FixDefaultColor = true;
				this.m_FixedDefaultColor = GraphForButton.TEXT_COLOR;
				this.m_RenderPress = true;
				this.m_PressColor = GraphForButton.TEXT_COLOR;
				this.m_RenderDisable = true;
				this.m_DisableColor = GraphForButton.DISABLE_TEXT_COLOR;
				this.m_DecidedColor = GraphForButton.TEXT_COLOR;
				break;
			}
			if (this.m_PressedAnim != null)
			{
				this.m_PressedAnim.gameObject.SetActive(false);
			}
			this.m_IsDonePrepare = true;
			this.m_GameObject = base.gameObject;
		}

		// Token: 0x06002C50 RID: 11344 RVA: 0x000EA149 File Offset: 0x000E8549
		private void Start()
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
		}

		// Token: 0x06002C51 RID: 11345 RVA: 0x000EA15C File Offset: 0x000E855C
		private void OnDisable()
		{
			if (this.m_PressedAnim != null)
			{
				this.m_PressedAnim.Stop();
				this.m_PressedAnim.gameObject.SetActive(false);
			}
		}

		// Token: 0x06002C52 RID: 11346 RVA: 0x000EA18C File Offset: 0x000E858C
		public bool CheckRenderDecide()
		{
			GraphForButton.eDecideObjState renderDecideState = this.m_RenderDecideState;
			if (renderDecideState == GraphForButton.eDecideObjState.Each)
			{
				return true;
			}
			if (renderDecideState != GraphForButton.eDecideObjState.DecideOnly)
			{
				return renderDecideState == GraphForButton.eDecideObjState.NotDecideOnly && !this.m_IsDecide;
			}
			return this.m_IsDecide;
		}

		// Token: 0x06002C53 RID: 11347 RVA: 0x000EA1CD File Offset: 0x000E85CD
		public void SetDecide(bool flg)
		{
			this.m_IsDecide = flg;
			this.ChangeState(this.m_CurrentState);
		}

		// Token: 0x06002C54 RID: 11348 RVA: 0x000EA1E2 File Offset: 0x000E85E2
		public void ResetState()
		{
			this.ChangeState(this.m_CurrentState);
		}

		// Token: 0x06002C55 RID: 11349 RVA: 0x000EA1F0 File Offset: 0x000E85F0
		public void ChangeState(GraphForButton.eState state)
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			this.m_CurrentState = state;
			if (state != GraphForButton.eState.Default)
			{
				if (state != GraphForButton.eState.Press)
				{
					if (state == GraphForButton.eState.Disable)
					{
						for (int i = 0; i < this.m_Graphics.Length; i++)
						{
							this.m_GameObject.SetActive(this.m_RenderDisable && this.CheckRenderDecide());
							if (this.m_RenderDisable)
							{
								this.m_Graphics[i].color = this.m_DisableColor;
							}
						}
					}
				}
				else
				{
					for (int j = 0; j < this.m_Graphics.Length; j++)
					{
						this.m_GameObject.SetActive(this.m_RenderPress && this.CheckRenderDecide());
						if (this.m_RenderPress)
						{
							this.m_Graphics[j].color = this.m_PressColor;
						}
					}
					if (this.m_PressedAnim != null)
					{
						this.m_PressedAnim.gameObject.SetActive(true);
						this.m_PressedAnim.Play();
					}
				}
			}
			else
			{
				for (int k = 0; k < this.m_Graphics.Length; k++)
				{
					if (this.m_IsDecide)
					{
						this.m_GameObject.SetActive(this.m_RenderPress && this.CheckRenderDecide());
						if (this.m_RenderPress)
						{
							this.m_Graphics[k].color = this.m_DecidedColor;
						}
					}
					else
					{
						this.m_GameObject.SetActive(this.m_RenderDefault && this.CheckRenderDecide());
						if (this.m_FixDefaultColor)
						{
							this.m_Graphics[k].color = this.m_FixedDefaultColor;
						}
						else
						{
							this.m_Graphics[k].color = this.m_DefaultColors[k];
						}
					}
				}
			}
		}

		// Token: 0x04003322 RID: 13090
		private static readonly Color BASE_COLOR = new Color(1f, 1f, 1f);

		// Token: 0x04003323 RID: 13091
		private static readonly Color TEXT_COLOR = new Color(0.43137255f, 0.25490198f, 0.20784314f);

		// Token: 0x04003324 RID: 13092
		private static readonly Color DISABLE_COLOR = new Color(0.627451f, 0.627451f, 0.627451f);

		// Token: 0x04003325 RID: 13093
		private static readonly Color DISABLE_TEXT_COLOR = new Color(0.19607843f, 0.19607843f, 0.19607843f);

		// Token: 0x04003326 RID: 13094
		private static readonly Color TOGGLE_DECIDED_COLOR = new Color(1f, 0.60784316f, 0.7254902f);

		// Token: 0x04003327 RID: 13095
		private Graphic[] m_Graphics;

		// Token: 0x04003328 RID: 13096
		private Color[] m_DefaultColors;

		// Token: 0x04003329 RID: 13097
		[SerializeField]
		private GraphForButton.ePreset m_Preset;

		// Token: 0x0400332A RID: 13098
		[SerializeField]
		private bool m_RenderDefault = true;

		// Token: 0x0400332B RID: 13099
		private bool m_FixDefaultColor;

		// Token: 0x0400332C RID: 13100
		private Color m_FixedDefaultColor = Color.white;

		// Token: 0x0400332D RID: 13101
		[SerializeField]
		private bool m_RenderPress = true;

		// Token: 0x0400332E RID: 13102
		[SerializeField]
		private Color m_PressColor = GraphForButton.BASE_COLOR;

		// Token: 0x0400332F RID: 13103
		[SerializeField]
		private bool m_RenderDisable = true;

		// Token: 0x04003330 RID: 13104
		[SerializeField]
		private Color m_DisableColor = Color.gray;

		// Token: 0x04003331 RID: 13105
		[SerializeField]
		private Color m_DecidedColor = GraphForButton.BASE_COLOR;

		// Token: 0x04003332 RID: 13106
		[SerializeField]
		private GraphForButton.eDecideObjState m_RenderDecideState;

		// Token: 0x04003333 RID: 13107
		[SerializeField]
		private Animation m_PressedAnim;

		// Token: 0x04003334 RID: 13108
		private bool m_IsDecide;

		// Token: 0x04003335 RID: 13109
		private GraphForButton.eState m_CurrentState;

		// Token: 0x04003336 RID: 13110
		private bool m_IsDonePrepare;

		// Token: 0x04003337 RID: 13111
		private GameObject m_GameObject;

		// Token: 0x02000850 RID: 2128
		public enum ePreset
		{
			// Token: 0x04003339 RID: 13113
			None,
			// Token: 0x0400333A RID: 13114
			CommonButtonBase,
			// Token: 0x0400333B RID: 13115
			CommonButtonText,
			// Token: 0x0400333C RID: 13116
			CommonButtonShadow,
			// Token: 0x0400333D RID: 13117
			Icon,
			// Token: 0x0400333E RID: 13118
			ToggleButtonBase,
			// Token: 0x0400333F RID: 13119
			ToggleButtonText,
			// Token: 0x04003340 RID: 13120
			TabButtonText
		}

		// Token: 0x02000851 RID: 2129
		public enum eDecideObjState
		{
			// Token: 0x04003342 RID: 13122
			Each,
			// Token: 0x04003343 RID: 13123
			DecideOnly,
			// Token: 0x04003344 RID: 13124
			NotDecideOnly
		}

		// Token: 0x02000852 RID: 2130
		public enum eState
		{
			// Token: 0x04003346 RID: 13126
			Default,
			// Token: 0x04003347 RID: 13127
			Press,
			// Token: 0x04003348 RID: 13128
			Disable
		}
	}
}
