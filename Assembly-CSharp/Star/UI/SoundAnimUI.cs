﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007F5 RID: 2037
	public class SoundAnimUI : AnimUIBase
	{
		// Token: 0x06002A26 RID: 10790 RVA: 0x000DE64E File Offset: 0x000DCA4E
		protected override void ExecutePlayIn()
		{
			this.m_ReservePlayIn = true;
			this.m_ReservePlayOut = false;
			this.m_TimeCount = 0f;
		}

		// Token: 0x06002A27 RID: 10791 RVA: 0x000DE669 File Offset: 0x000DCA69
		protected override void ExecutePlayOut()
		{
			this.m_ReservePlayOut = true;
			this.m_ReservePlayIn = false;
			this.m_TimeCount = 0f;
		}

		// Token: 0x06002A28 RID: 10792 RVA: 0x000DE684 File Offset: 0x000DCA84
		public override void Hide()
		{
			this.m_ReservePlayIn = false;
			this.m_ReservePlayOut = false;
		}

		// Token: 0x06002A29 RID: 10793 RVA: 0x000DE694 File Offset: 0x000DCA94
		public override void Show()
		{
			this.m_ReservePlayIn = false;
			this.m_ReservePlayOut = false;
		}

		// Token: 0x06002A2A RID: 10794 RVA: 0x000DE6A4 File Offset: 0x000DCAA4
		protected override void Update()
		{
			base.Update();
			if (this.m_ReservePlayIn)
			{
				this.m_TimeCount += Time.deltaTime;
				if (this.m_TimeCount > this.m_DelayInSe)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_InSound, 1f, 0, -1, -1);
					this.m_IsPlaying = false;
				}
				this.m_ReservePlayIn = false;
			}
			else if (this.m_ReservePlayOut)
			{
				this.m_TimeCount += Time.deltaTime;
				if (this.m_TimeCount > this.m_DelayOutSe)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_OutSound, 1f, 0, -1, -1);
					this.m_IsPlaying = false;
				}
				this.m_ReservePlayOut = false;
			}
		}

		// Token: 0x04003083 RID: 12419
		[SerializeField]
		private eSoundSeListDB m_InSound;

		// Token: 0x04003084 RID: 12420
		[SerializeField]
		private float m_DelayInSe;

		// Token: 0x04003085 RID: 12421
		[SerializeField]
		private eSoundSeListDB m_OutSound;

		// Token: 0x04003086 RID: 12422
		[SerializeField]
		private float m_DelayOutSe;

		// Token: 0x04003087 RID: 12423
		private bool m_ReservePlayIn;

		// Token: 0x04003088 RID: 12424
		private bool m_ReservePlayOut;

		// Token: 0x04003089 RID: 12425
		private float m_TimeCount;
	}
}
