﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200085B RID: 2139
	public class CharaIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002C7F RID: 11391 RVA: 0x000EB0EC File Offset: 0x000E94EC
		public void ApplyCharaMngID(long charaMngID)
		{
			if (charaMngID == -1L)
			{
				this.m_CharaIcon.Destroy();
				return;
			}
			if (this.m_CharaIcon == null)
			{
				this.m_CharaIcon = base.GetComponent<CharaIcon>();
			}
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			this.m_CharaIcon.Apply(userCharaData.Param.CharaID);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().SetOwnerIcon(this);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().ApplyCharaMngID(charaMngID);
		}

		// Token: 0x06002C80 RID: 11392 RVA: 0x000EB174 File Offset: 0x000E9574
		public void ApplyCharaID(int charaID)
		{
			if (charaID == -1)
			{
				this.m_CharaIcon.Destroy();
				return;
			}
			if (this.m_CharaIcon == null)
			{
				this.m_CharaIcon = base.GetComponent<CharaIcon>();
			}
			this.m_CharaIcon.Apply(charaID);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().SetOwnerIcon(this);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().ApplyCharaID(charaID, false);
		}

		// Token: 0x06002C81 RID: 11393 RVA: 0x000EB1E0 File Offset: 0x000E95E0
		public void ApplySupport(UserSupportData supportData)
		{
			if (supportData == null)
			{
				this.m_CharaIcon.Destroy();
				return;
			}
			if (this.m_CharaIcon == null)
			{
				this.m_CharaIcon = base.GetComponent<CharaIcon>();
			}
			this.m_CharaIcon.Apply(supportData.CharaID);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().SetOwnerIcon(this);
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().ApplySupport(supportData);
		}

		// Token: 0x06002C82 RID: 11394 RVA: 0x000EB24F File Offset: 0x000E964F
		public void SetLimitBreak(int lb)
		{
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().SetLimitBreak(lb);
		}

		// Token: 0x06002C83 RID: 11395 RVA: 0x000EB262 File Offset: 0x000E9662
		public void SetNew(bool flg)
		{
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().SetNew(flg);
		}

		// Token: 0x06002C84 RID: 11396 RVA: 0x000EB275 File Offset: 0x000E9675
		public void ChangeDetailText(CharaIconFrame.eDetailType detailType)
		{
			this.m_CharaIconFrame.GetInst<CharaIconFrame>().ChangeDetailText(detailType);
		}

		// Token: 0x06002C85 RID: 11397 RVA: 0x000EB288 File Offset: 0x000E9688
		public void ChangeDetailTextToCharaListSort()
		{
			switch (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.CharaListView))
			{
			case 2:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Cost);
				return;
			case 5:
				this.ChangeDetailText(CharaIconFrame.eDetailType.HP);
				return;
			case 6:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Atk);
				return;
			case 7:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Mgc);
				return;
			case 8:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Def);
				return;
			case 9:
				this.ChangeDetailText(CharaIconFrame.eDetailType.MDef);
				return;
			case 10:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Spd);
				return;
			}
			this.ChangeDetailText(CharaIconFrame.eDetailType.Lv);
		}

		// Token: 0x06002C86 RID: 11398 RVA: 0x000EB338 File Offset: 0x000E9738
		public void ChangeDetailTextToSupportSelectSort()
		{
			switch (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.SupportSelect))
			{
			case 2:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Cost);
				break;
			case 3:
				this.ChangeDetailText(CharaIconFrame.eDetailType.HP);
				break;
			case 4:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Atk);
				break;
			case 5:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Mgc);
				break;
			case 6:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Def);
				break;
			case 7:
				this.ChangeDetailText(CharaIconFrame.eDetailType.MDef);
				break;
			case 8:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Spd);
				break;
			default:
				this.ChangeDetailText(CharaIconFrame.eDetailType.Lv);
				break;
			}
		}

		// Token: 0x06002C87 RID: 11399 RVA: 0x000EB3DF File Offset: 0x000E97DF
		public CharaIconFrame GetCharaIconFrame()
		{
			return this.m_CharaIconFrame.GetInst<CharaIconFrame>();
		}

		// Token: 0x06002C88 RID: 11400 RVA: 0x000EB3EC File Offset: 0x000E97EC
		public void Destroy()
		{
			if (this.m_CharaIcon != null)
			{
				this.m_CharaIcon.Destroy();
			}
		}

		// Token: 0x0400336E RID: 13166
		[SerializeField]
		private PrefabCloner m_CharaIconFrame;

		// Token: 0x0400336F RID: 13167
		[SerializeField]
		private CharaIcon m_CharaIcon;
	}
}
