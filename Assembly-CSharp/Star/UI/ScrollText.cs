﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008A2 RID: 2210
	public class ScrollText : MonoBehaviour
	{
		// Token: 0x06002DDC RID: 11740 RVA: 0x000F13D4 File Offset: 0x000EF7D4
		private void Awake()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x06002DDD RID: 11741 RVA: 0x000F13E2 File Offset: 0x000EF7E2
		private void Start()
		{
			this.m_TextRectTransform = this.m_Text.GetComponent<RectTransform>();
			this.m_StartPosition = this.m_TextRectTransform.localPosition;
			this.ResetScroll();
		}

		// Token: 0x06002DDE RID: 11742 RVA: 0x000F140C File Offset: 0x000EF80C
		private void Update()
		{
			if (this.m_TextRectTransform.rect.width < this.m_RectTransform.rect.width)
			{
				return;
			}
			switch (this.m_State)
			{
			case ScrollText.eState.In:
			{
				Color color = this.m_Text.color;
				color.a += Time.deltaTime * this.m_InOutAlphaSpeedPerSec;
				if (color.a >= 1f)
				{
					color.a = 1f;
					this.m_State = ScrollText.eState.Start;
					this.m_StateTime = 0f;
				}
				this.m_Text.color = color;
				break;
			}
			case ScrollText.eState.Start:
				if (this.m_StateTime > this.m_StartSec)
				{
					this.m_State = ScrollText.eState.Scroll;
					this.m_StateTime = 0f;
				}
				break;
			case ScrollText.eState.Scroll:
			{
				float num = Time.deltaTime * this.m_SpeedPerSec;
				this.m_TextRectTransform.localPosX(this.m_TextRectTransform.localPosition.x - num);
				if (this.m_RectTransform.rect.width * 0.5f > this.m_TextRectTransform.localPosition.x + this.m_TextRectTransform.rect.width)
				{
					this.m_TextRectTransform.localPosX(this.m_RectTransform.rect.width * 0.5f - this.m_TextRectTransform.rect.width);
					this.m_State = ScrollText.eState.End;
					this.m_StateTime = 0f;
				}
				break;
			}
			case ScrollText.eState.End:
				if (this.m_StateTime > this.m_ReStartWaitSec)
				{
					this.m_State = ScrollText.eState.Out;
					this.m_StateTime = 0f;
				}
				break;
			case ScrollText.eState.Out:
			{
				Color color2 = this.m_Text.color;
				color2.a -= Time.deltaTime * this.m_InOutAlphaSpeedPerSec;
				if (color2.a <= 0f)
				{
					color2.a = 0f;
					this.m_State = ScrollText.eState.In;
					this.m_StateTime = 0f;
					this.m_TextRectTransform.localPosition = this.m_StartPosition;
				}
				this.m_Text.color = color2;
				break;
			}
			}
			this.m_StateTime += Time.deltaTime;
		}

		// Token: 0x06002DDF RID: 11743 RVA: 0x000F1678 File Offset: 0x000EFA78
		public void ResetScroll()
		{
			if (this.m_TextRectTransform != null)
			{
				this.m_TextRectTransform.localPosition = this.m_StartPosition;
				Color color = this.m_Text.color;
				color.a = 1f;
				this.m_State = ScrollText.eState.Start;
				this.m_StateTime = 0f;
				this.m_Text.color = color;
			}
		}

		// Token: 0x040034DA RID: 13530
		[SerializeField]
		private Text m_Text;

		// Token: 0x040034DB RID: 13531
		private float m_SpeedPerSec = 128f;

		// Token: 0x040034DC RID: 13532
		private float m_StartSec = 1f;

		// Token: 0x040034DD RID: 13533
		private float m_ReStartWaitSec = 1f;

		// Token: 0x040034DE RID: 13534
		private float m_InOutAlphaSpeedPerSec = 4f;

		// Token: 0x040034DF RID: 13535
		private ScrollText.eState m_State = ScrollText.eState.Start;

		// Token: 0x040034E0 RID: 13536
		private float m_StateTime;

		// Token: 0x040034E1 RID: 13537
		private Vector3 m_StartPosition;

		// Token: 0x040034E2 RID: 13538
		private RectTransform m_RectTransform;

		// Token: 0x040034E3 RID: 13539
		private RectTransform m_TextRectTransform;

		// Token: 0x020008A3 RID: 2211
		private enum eState
		{
			// Token: 0x040034E5 RID: 13541
			In,
			// Token: 0x040034E6 RID: 13542
			Start,
			// Token: 0x040034E7 RID: 13543
			Scroll,
			// Token: 0x040034E8 RID: 13544
			End,
			// Token: 0x040034E9 RID: 13545
			Out
		}
	}
}
