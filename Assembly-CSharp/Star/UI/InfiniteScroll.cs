﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000880 RID: 2176
	public class InfiniteScroll : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		// Token: 0x14000069 RID: 105
		// (add) Token: 0x06002D11 RID: 11537 RVA: 0x000EDC40 File Offset: 0x000EC040
		// (remove) Token: 0x06002D12 RID: 11538 RVA: 0x000EDC78 File Offset: 0x000EC078
		public event Action OnChangedCurrent;

		// Token: 0x1400006A RID: 106
		// (add) Token: 0x06002D13 RID: 11539 RVA: 0x000EDCB0 File Offset: 0x000EC0B0
		// (remove) Token: 0x06002D14 RID: 11540 RVA: 0x000EDCE8 File Offset: 0x000EC0E8
		public event Action OnDragAction;

		// Token: 0x06002D15 RID: 11541 RVA: 0x000EDD1E File Offset: 0x000EC11E
		public int GetNowSelect()
		{
			return this.m_NowSelect;
		}

		// Token: 0x06002D16 RID: 11542 RVA: 0x000EDD28 File Offset: 0x000EC128
		public int GetNowSelectDataIdx()
		{
			if (!this.m_NotInfinite)
			{
				int i = this.m_NowSelect % this.m_ItemList.Count;
				if (this.m_ItemList.Count > 0)
				{
					while (i < 0)
					{
						i += this.m_ItemList.Count;
					}
				}
				return i;
			}
			int nowSelect = this.m_NowSelect;
			if (nowSelect < 0)
			{
				return 0;
			}
			if (nowSelect >= this.m_ItemList.Count)
			{
				return this.m_ItemList.Count - 1;
			}
			return nowSelect;
		}

		// Token: 0x06002D17 RID: 11543 RVA: 0x000EDDB2 File Offset: 0x000EC1B2
		public float GetNormalizedScrollValue()
		{
			if (this.m_Direction == InfiniteScroll.eScrollDirection.Horizontal)
			{
				return this.m_ScrollRect.horizontalNormalizedPosition;
			}
			return this.m_ScrollRect.verticalNormalizedPosition;
		}

		// Token: 0x06002D18 RID: 11544 RVA: 0x000EDDD8 File Offset: 0x000EC1D8
		public void SetNotInfiniteFlag(bool flg)
		{
			this.m_NotInfinite = flg;
			if (!this.m_NotInfinite)
			{
				this.m_ScrollRect.inertia = false;
				this.m_ScrollRect.movementType = ScrollRect.MovementType.Unrestricted;
			}
			else
			{
				this.m_ScrollRect.inertia = true;
				this.m_ScrollRect.movementType = ScrollRect.MovementType.Elastic;
			}
		}

		// Token: 0x06002D19 RID: 11545 RVA: 0x000EDE2C File Offset: 0x000EC22C
		public virtual void Setup(float width)
		{
			this.m_ScrollRect = base.GetComponent<ScrollRectEx>();
			this.m_IsDonePrepare = true;
			RectTransform component = this.m_ItemPrefab.GetComponent<RectTransform>();
			this.m_ItemWidth = width;
			this.m_ItemHeight = component.rect.height;
			if (this.m_Direction == InfiniteScroll.eScrollDirection.Horizontal)
			{
				this.m_MaxDispItem = (int)(this.m_ScrollRect.viewport.sizeDelta.x / this.m_ItemWidth) + 1;
			}
			else
			{
				this.m_MaxDispItem = (int)(this.m_ScrollRect.viewport.sizeDelta.y / this.m_ItemHeight) + 1;
			}
			if (this.m_MaxDispItem < 3)
			{
				this.m_MaxDispItem = 3;
			}
			if (this.m_Signal != null)
			{
				this.m_Signal.OnClickButton += this.OnClickSignalCallBack;
			}
			this.SetNotInfiniteFlag(this.m_NotInfinite);
		}

		// Token: 0x06002D1A RID: 11546 RVA: 0x000EDF1C File Offset: 0x000EC31C
		public virtual void Setup()
		{
			this.m_ScrollRect = base.GetComponent<ScrollRectEx>();
			this.m_IsDonePrepare = true;
			RectTransform component = this.m_ItemPrefab.GetComponent<RectTransform>();
			this.m_ItemWidth = component.rect.width;
			this.m_ItemHeight = component.rect.height;
			if (this.m_Direction == InfiniteScroll.eScrollDirection.Horizontal)
			{
				this.m_MaxDispItem = (int)(this.m_ScrollRect.viewport.sizeDelta.x / this.m_ItemWidth) + 1;
			}
			else
			{
				this.m_MaxDispItem = (int)(this.m_ScrollRect.viewport.sizeDelta.y / this.m_ItemHeight) + 1;
			}
			if (this.m_MaxDispItem < 3)
			{
				this.m_MaxDispItem = 3;
			}
			if (this.m_Signal != null)
			{
				this.m_Signal.OnClickButton += this.OnClickSignalCallBack;
			}
			this.SetNotInfiniteFlag(this.m_NotInfinite);
		}

		// Token: 0x06002D1B RID: 11547 RVA: 0x000EE018 File Offset: 0x000EC418
		protected virtual void Update()
		{
			if (this.m_ItemList.Count == 0)
			{
				return;
			}
			if (this.m_DispItemNum < this.m_MaxDispItem)
			{
				InfiniteScrollItem infiniteScrollItem = UnityEngine.Object.Instantiate<InfiniteScrollItem>(this.m_ItemPrefab, this.m_ScrollRect.content, false);
				infiniteScrollItem.RectTransform.anchorMin = new Vector2(0f, 1f);
				infiniteScrollItem.RectTransform.anchorMax = new Vector2(0f, 1f);
				infiniteScrollItem.DataIdx = -1;
				this.m_EmptyInstList.Add(infiniteScrollItem);
				infiniteScrollItem.Setup();
				infiniteScrollItem.GameObject.SetActive(false);
				infiniteScrollItem.Scroll = this;
				this.m_DispItemNum++;
			}
			if (this.m_ScrollRect.IsDrag)
			{
				this.OnDragAction.Call();
				if (this.m_ScrollRect.DragTime < 0.2f)
				{
					this.m_IsReserveFlick = true;
				}
				else
				{
					this.m_IsReserveFlick = false;
				}
				this.StopEase();
				float scrollValue = this.m_ScrollValue;
				float num = this.m_ItemWidth;
				this.m_ScrollValue = -this.m_ScrollRect.content.localPosition.x;
				if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
				{
					num = this.m_ItemHeight;
					this.m_ScrollValue = this.m_ScrollRect.content.localPosition.y;
				}
				int nowSelect = (int)(this.m_ScrollValue / num);
				if (this.m_ScrollValue < 0f)
				{
					nowSelect = (int)(this.m_ScrollValue / num) - 1;
				}
				this.SetNowSelect(nowSelect);
				this.m_ScrollMove += scrollValue - this.m_ScrollValue;
			}
			else if (this.m_Adjust && !this.m_ScrollRect.IsDrag)
			{
				float num2 = this.m_ItemWidth;
				this.m_ScrollValue = -this.m_ScrollRect.content.localPosition.x;
				if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
				{
					num2 = this.m_ItemHeight;
					this.m_ScrollValue = this.m_ScrollRect.content.localPosition.y;
				}
				if (this.m_IsReserveFlick)
				{
					float num3 = Mathf.Abs(this.m_ScrollMove);
					if (num3 > 0.001f && num3 < num2 * 0.5f)
					{
						this.SetSelectIdx(this.m_NowSelect - (int)(this.m_ScrollMove / Mathf.Abs(num3)), true);
					}
					this.ForceEnableHoldAllButton();
					this.m_IsReserveFlick = false;
				}
				this.m_ScrollMove = 0f;
				this.AdjustPosition(false);
				this.m_IsDirty = true;
			}
		}

		// Token: 0x06002D1C RID: 11548 RVA: 0x000EE2AB File Offset: 0x000EC6AB
		protected virtual void LateUpdate()
		{
			this.OnUpdateEase();
			if (this.m_IsDirty)
			{
				this.m_IsDirty = false;
				this.UpdateItemPosition();
			}
		}

		// Token: 0x06002D1D RID: 11549 RVA: 0x000EE2CC File Offset: 0x000EC6CC
		private void AdjustPosition(bool immediate = false)
		{
			if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
			{
				float verticalNormalizedPosition = 0.5f;
				if (this.m_ScrollRect.content.sizeDelta.y > this.m_ScrollRect.viewport.sizeDelta.y)
				{
					verticalNormalizedPosition = ((float)this.m_NowSelect * this.m_ItemHeight - this.m_ScrollRect.viewport.rect.height * 0.5f + this.m_ItemHeight * 0.5f) / (this.m_ScrollRect.content.sizeDelta.y - this.m_ScrollRect.viewport.sizeDelta.y);
				}
				if (immediate)
				{
					this.m_ScrollRect.verticalNormalizedPosition = verticalNormalizedPosition;
				}
				else
				{
					this.OnDragAction.Call();
				}
			}
			else
			{
				this.RefreshOverScroll();
				float num = 0f;
				if (this.m_ScrollRect.content.rect.width > this.m_ScrollRect.viewport.rect.width)
				{
					num = ((float)this.GetNowSelectDataIdx() * this.m_ItemWidth - this.m_ScrollRect.viewport.rect.width * 0.5f + this.m_ItemWidth * 0.5f) / (this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width);
				}
				if (immediate)
				{
					this.m_ScrollRect.horizontalNormalizedPosition = num;
					this.StopEase();
				}
				else if (!this.m_IsEasing)
				{
					if (!this.m_NotInfinite)
					{
						if (num - this.GetNormalizedScrollValue() - this.m_ItemWidth * 0.5f / this.m_ScrollRect.content.rect.width > 0.5f)
						{
							num = ((float)(this.GetNowSelectDataIdx() - this.m_ItemList.Count) * this.m_ItemWidth - this.m_ScrollRect.viewport.rect.width * 0.5f + this.m_ItemWidth * 0.5f) / (this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width);
						}
						else if (num - this.GetNormalizedScrollValue() + this.m_ItemWidth * 0.5f / this.m_ScrollRect.content.rect.width < -0.5f)
						{
							num = ((float)(this.GetNowSelectDataIdx() + this.m_ItemList.Count) * this.m_ItemWidth - this.m_ScrollRect.viewport.rect.width * 0.5f + this.m_ItemWidth * 0.5f) / (this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width);
						}
					}
					if (Mathf.Abs(num - this.m_ScrollRect.horizontalNormalizedPosition) * this.m_ScrollRect.viewport.rect.width > 10f)
					{
						this.OnDragAction.Call();
						this.ScrollEase(num);
					}
					else
					{
						this.m_ScrollRect.horizontalNormalizedPosition = num;
						this.StopEase();
					}
				}
			}
		}

		// Token: 0x06002D1E RID: 11550 RVA: 0x000EE674 File Offset: 0x000ECA74
		protected void RefreshOverScroll()
		{
			if (!this.m_NotInfinite)
			{
				float num = this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width;
				if (num > 0f)
				{
					float num2 = (float)this.m_ItemList.Count * this.m_ItemWidth / num;
					if (num2 > 0f)
					{
						while (this.m_ScrollRect.horizontalNormalizedPosition > 1f)
						{
							this.m_ScrollRect.horizontalNormalizedPosition -= num2;
							this.m_EaseGoal -= num2;
						}
						while (this.m_ScrollRect.horizontalNormalizedPosition < 0f)
						{
							this.m_ScrollRect.horizontalNormalizedPosition += num2;
							this.m_EaseGoal += num2;
						}
					}
				}
			}
		}

		// Token: 0x06002D1F RID: 11551 RVA: 0x000EE760 File Offset: 0x000ECB60
		protected void ScrollEase(float goal)
		{
			this.m_IsEasing = true;
			this.m_EaseGoal = goal;
		}

		// Token: 0x06002D20 RID: 11552 RVA: 0x000EE770 File Offset: 0x000ECB70
		protected void OnUpdateEase()
		{
			if (this.m_IsEasing)
			{
				float num = (this.m_EaseGoal - this.GetNormalizedScrollValue()) * this.m_AdjustSpeed * Time.deltaTime;
				float num2 = Mathf.Abs(num);
				if (num2 < 0.001f)
				{
					this.m_IsEasing = false;
					this.OnCompleteEase();
					this.m_ScrollRect.horizontalNormalizedPosition = this.m_EaseGoal;
					this.OnDragAction.Call();
					return;
				}
				if (num2 < 0.01f)
				{
					num = 0.01f * num / num2;
					num2 = 0.01f;
				}
				if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
				{
					if (Mathf.Abs(this.m_EaseGoal - this.m_ScrollRect.verticalNormalizedPosition) <= num2)
					{
						this.m_IsEasing = false;
						this.OnCompleteEase();
						this.m_ScrollRect.verticalNormalizedPosition = this.m_EaseGoal;
					}
					else
					{
						this.m_ScrollRect.verticalNormalizedPosition += num;
					}
				}
				else if (Mathf.Abs(this.m_EaseGoal - this.m_ScrollRect.horizontalNormalizedPosition) <= num2)
				{
					this.m_IsEasing = false;
					this.OnCompleteEase();
					this.m_ScrollRect.horizontalNormalizedPosition = this.m_EaseGoal;
				}
				else
				{
					this.m_ScrollRect.horizontalNormalizedPosition += num;
				}
				this.OnDragAction.Call();
			}
		}

		// Token: 0x06002D21 RID: 11553 RVA: 0x000EE8BC File Offset: 0x000ECCBC
		protected void OnCompleteEase()
		{
			this.StopEase();
		}

		// Token: 0x06002D22 RID: 11554 RVA: 0x000EE8C4 File Offset: 0x000ECCC4
		protected void StopEase()
		{
			this.m_IsEasing = false;
		}

		// Token: 0x06002D23 RID: 11555 RVA: 0x000EE8CD File Offset: 0x000ECCCD
		public void OnScrollValueChanged()
		{
			if (!this.m_ScrollRect.IsDrag)
			{
				this.OnDragAction.Call();
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06002D24 RID: 11556 RVA: 0x000EE8F4 File Offset: 0x000ECCF4
		public void UpdateItemPosition()
		{
			if (this.m_IsDonePrepare && this.m_ItemList.Count > 0)
			{
				float num = this.m_ItemWidth;
				this.m_ScrollValue = -this.m_ScrollRect.content.localPosition.x;
				if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
				{
					num = this.m_ItemHeight;
					this.m_ScrollValue = this.m_ScrollRect.content.localPosition.y;
				}
				int num2 = (int)(this.m_ScrollValue / num);
				if (this.m_ScrollValue < 0f)
				{
					num2 = (int)(this.m_ScrollValue / num) - 1;
				}
				num2 -= this.m_MaxDispItem / 2;
				if (this.m_NotInfinite && num2 < 0)
				{
					num2 = 0;
				}
				int[] array = new int[this.m_MaxDispItem];
				for (int i = 0; i < this.m_MaxDispItem; i++)
				{
					array[i] = num2;
					num2++;
				}
				if (this.m_NotInfinite)
				{
					int num3 = array[array.Length - 1] - (this.m_ItemList.Count - 1);
					if (0 < num3)
					{
						for (int j = 0; j < array.Length; j++)
						{
							array[j] -= num3;
						}
					}
				}
				List<InfiniteScrollItem> list = new List<InfiniteScrollItem>();
				for (int k = 0; k < this.m_ActiveInstList.Count; k++)
				{
					if (this.m_ActiveInstList[k].PosIdx < array[0] || this.m_ActiveInstList[k].PosIdx >= array[0] + this.m_MaxDispItem || (this.m_NotInfinite && this.m_ActiveInstList[k].PosIdx < 0))
					{
						list.Add(this.m_ActiveInstList[k]);
						this.m_ActiveInstList[k].GameObject.SetActive(false);
					}
				}
				for (int l = 0; l < list.Count; l++)
				{
					this.m_ActiveInstList.Remove(list[l]);
					this.m_EmptyInstList.Add(list[l]);
				}
				for (int m = 0; m < this.m_MaxDispItem; m++)
				{
					int num4 = m + this.m_MaxDispItem / 2;
					if (num4 >= this.m_MaxDispItem)
					{
						num4 -= this.m_MaxDispItem;
					}
					bool flag = false;
					for (int n = 0; n < this.m_ActiveInstList.Count; n++)
					{
						if (this.m_ActiveInstList[n].PosIdx == array[num4])
						{
							flag = true;
							break;
						}
					}
					if (!flag)
					{
						if (!this.m_NotInfinite || array[num4] >= 0)
						{
							if (this.m_EmptyInstList.Count <= 0)
							{
								this.m_IsDirty = true;
								break;
							}
							int num5;
							for (num5 = array[num4] % this.m_ItemList.Count; num5 < 0; num5 += this.m_ItemList.Count)
							{
							}
							InfiniteScrollItem infiniteScrollItem = null;
							for (int num6 = 0; num6 < this.m_EmptyInstList.Count; num6++)
							{
								if (this.m_EmptyInstList[num6].DataIdx == num5)
								{
									infiniteScrollItem = this.m_EmptyInstList[num6];
									this.m_EmptyInstList.RemoveAt(num6);
									break;
								}
							}
							if (infiniteScrollItem == null)
							{
								infiniteScrollItem = this.m_EmptyInstList[0];
								this.m_EmptyInstList.RemoveAt(0);
							}
							this.m_ActiveInstList.Add(infiniteScrollItem);
							if (infiniteScrollItem.DataIdx != num5)
							{
								infiniteScrollItem.Apply(this.m_ItemList[num5]);
							}
							infiniteScrollItem.GameObject.SetActive(true);
							infiniteScrollItem.DataIdx = num5;
							infiniteScrollItem.PosIdx = array[num4];
							if (this.m_Direction == InfiniteScroll.eScrollDirection.Horizontal)
							{
								infiniteScrollItem.RectTransform.anchoredPosition = new Vector3((float)array[num4] * this.m_ItemWidth + this.m_ItemWidth * 0.5f, -this.m_ItemHeight * 0.5f);
							}
							else
							{
								infiniteScrollItem.RectTransform.anchoredPosition = new Vector3(this.m_ItemWidth * 0.5f, (float)(-(float)array[num4]) * this.m_ItemHeight - this.m_ItemHeight * 0.5f);
							}
						}
					}
				}
			}
		}

		// Token: 0x06002D25 RID: 11557 RVA: 0x000EED94 File Offset: 0x000ED194
		public void AddItem(InfiniteScrollItemData data)
		{
			this.m_ItemList.Add(data);
			if (this.m_Direction == InfiniteScroll.eScrollDirection.Vertical)
			{
				this.m_ScrollMax = this.m_ItemHeight * (float)this.m_ItemList.Count;
				this.m_ScrollRect.content.anchorMin = new Vector2(this.m_ScrollRect.content.anchorMin.x, 0f);
				this.m_ScrollRect.content.anchorMax = new Vector2(this.m_ScrollRect.content.anchorMax.x, 0f);
				this.m_ScrollRect.content.sizeDelta = new Vector2(this.m_ScrollRect.content.sizeDelta.x, this.m_ScrollMax);
				if (this.m_ScrollRect.content.sizeDelta.y >= this.m_ScrollRect.viewport.sizeDelta.y)
				{
					this.m_ScrollRect.vertical = true;
				}
				else
				{
					this.m_ScrollRect.vertical = false;
				}
			}
			else
			{
				this.m_ScrollMax = this.m_ItemWidth * (float)this.m_ItemList.Count;
				this.m_ScrollRect.content.anchorMin = new Vector2(0f, this.m_ScrollRect.content.anchorMin.y);
				this.m_ScrollRect.content.anchorMax = new Vector2(0f, this.m_ScrollRect.content.anchorMax.y);
				this.m_ScrollRect.content.sizeDelta = new Vector2(this.m_ScrollMax, this.m_ScrollRect.content.sizeDelta.y);
				if (this.m_ScrollRect.content.sizeDelta.x >= this.m_ScrollRect.viewport.sizeDelta.x)
				{
					this.m_ScrollRect.horizontal = true;
				}
				else
				{
					this.m_ScrollRect.horizontal = false;
				}
			}
			if (this.m_ItemList.Count <= 1)
			{
				this.m_ScrollRect.horizontal = false;
			}
			else
			{
				this.m_ScrollRect.horizontal = true;
			}
			if (this.m_Signal != null)
			{
				int i;
				for (i = this.m_NowSelect % this.m_ItemList.Count; i < 0; i += this.m_ItemList.Count)
				{
				}
				this.m_Signal.SetPageMax(this.m_ItemList.Count);
				this.m_Signal.SetPage(i);
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06002D26 RID: 11558 RVA: 0x000EF062 File Offset: 0x000ED462
		public void ChangeItemData(int idx, InfiniteScrollItemData data)
		{
			this.m_ItemList[idx] = data;
			this.Refresh();
		}

		// Token: 0x06002D27 RID: 11559 RVA: 0x000EF077 File Offset: 0x000ED477
		public void Remove(InfiniteScrollItemData data)
		{
			this.m_ItemList.Remove(data);
			this.Refresh();
		}

		// Token: 0x06002D28 RID: 11560 RVA: 0x000EF08C File Offset: 0x000ED48C
		public void RemoveAll()
		{
			this.m_ItemList.Clear();
			this.Refresh();
		}

		// Token: 0x06002D29 RID: 11561 RVA: 0x000EF0A0 File Offset: 0x000ED4A0
		public void Refresh()
		{
			while (this.m_ActiveInstList.Count > 0)
			{
				this.m_ActiveInstList[0].GameObject.SetActive(false);
				this.m_EmptyInstList.Add(this.m_ActiveInstList[0]);
				this.m_ActiveInstList[0].PosIdx = -1;
				this.m_ActiveInstList[0].DataIdx = -1;
				this.m_ActiveInstList.RemoveAt(0);
			}
		}

		// Token: 0x06002D2A RID: 11562 RVA: 0x000EF124 File Offset: 0x000ED524
		public void ForceApply()
		{
			for (int i = 0; i < this.m_ActiveInstList.Count; i++)
			{
				this.m_ActiveInstList[i].Apply(this.m_ItemList[this.m_ActiveInstList[i].DataIdx]);
			}
		}

		// Token: 0x06002D2B RID: 11563 RVA: 0x000EF17C File Offset: 0x000ED57C
		public virtual void Destroy()
		{
			this.Refresh();
			for (int i = 0; i < this.m_EmptyInstList.Count; i++)
			{
				this.m_EmptyInstList[i].Destroy();
			}
		}

		// Token: 0x06002D2C RID: 11564 RVA: 0x000EF1BC File Offset: 0x000ED5BC
		public void SetSelectIdx(int idx, bool move)
		{
			this.SetNowSelect(idx);
			this.m_IsDirty = true;
			this.StopEase();
			this.AdjustPosition(!move);
		}

		// Token: 0x06002D2D RID: 11565 RVA: 0x000EF1DC File Offset: 0x000ED5DC
		private void SetNowSelect(int idx)
		{
			if (this.m_NowSelect != idx)
			{
				this.m_IsDirty = true;
				this.OnChangedCurrent.Call();
				this.m_NowSelect = idx;
				if (this.m_NotInfinite)
				{
					this.m_NowSelect = this.GetNowSelectDataIdx();
				}
				if (this.m_Signal != null)
				{
					this.m_Signal.SetPage(this.GetNowSelectDataIdx());
				}
			}
		}

		// Token: 0x06002D2E RID: 11566 RVA: 0x000EF248 File Offset: 0x000ED648
		public void OnClickSignalCallBack(int idx)
		{
			int num = this.m_NowSelect / this.m_ItemList.Count * this.m_ItemList.Count + idx;
			if (this.m_NowSelect - num > this.m_ItemList.Count / 2)
			{
				num += this.m_ItemList.Count;
			}
			else if (this.m_NowSelect - num < -this.m_ItemList.Count / 2)
			{
				num -= this.m_ItemList.Count;
			}
			this.SetSelectIdx(num, true);
		}

		// Token: 0x06002D2F RID: 11567 RVA: 0x000EF2D4 File Offset: 0x000ED6D4
		public void OnClickRightButtonCallBack()
		{
			this.RefreshOverScroll();
			this.SetSelectIdx(this.m_NowSelect + 1, true);
			if (this.m_EaseGoal < this.m_ScrollRect.horizontalNormalizedPosition)
			{
				this.m_EaseGoal += this.m_ItemWidth * (float)this.m_ItemList.Count / (this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width);
			}
		}

		// Token: 0x06002D30 RID: 11568 RVA: 0x000EF360 File Offset: 0x000ED760
		public void OnClickLeftButtonCallBack()
		{
			this.RefreshOverScroll();
			this.SetSelectIdx(this.m_NowSelect - 1, true);
			if (this.m_EaseGoal > this.m_ScrollRect.horizontalNormalizedPosition)
			{
				this.m_EaseGoal -= this.m_ItemWidth * (float)this.m_ItemList.Count / (this.m_ScrollRect.content.rect.width - this.m_ScrollRect.viewport.rect.width);
			}
		}

		// Token: 0x06002D31 RID: 11569 RVA: 0x000EF3EA File Offset: 0x000ED7EA
		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		// Token: 0x06002D32 RID: 11570 RVA: 0x000EF3EC File Offset: 0x000ED7EC
		public void OnDrag(PointerEventData eventData)
		{
			if (this.m_ScrollRect.horizontal)
			{
				RectTransform component = base.GetComponent<RectTransform>();
				Vector2 a;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.position, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out a);
				Vector2 b;
				RectTransformUtility.ScreenPointToLocalPointInRectangle(component, eventData.pressPosition, SingletonMonoBehaviour<GameSystem>.Inst.UICamera, out b);
				if ((a - b).sqrMagnitude > 1024f)
				{
					this.ForceEnableHoldAllButton();
				}
			}
		}

		// Token: 0x06002D33 RID: 11571 RVA: 0x000EF464 File Offset: 0x000ED864
		private void ForceEnableHoldAllButton()
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].ForceEnableHold(false);
			}
		}

		// Token: 0x06002D34 RID: 11572 RVA: 0x000EF498 File Offset: 0x000ED898
		public void OnEndDrag(PointerEventData eventData)
		{
			if (!this.m_IsReserveFlick)
			{
				CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].ForceRelease();
				}
			}
		}

		// Token: 0x06002D35 RID: 11573 RVA: 0x000EF4D4 File Offset: 0x000ED8D4
		public virtual void ForceReleaseDrag()
		{
			this.m_ScrollRect.ReleaseDrag();
			this.m_IsReserveFlick = false;
			EventSystem current = EventSystem.current;
			current.enabled = false;
			current.enabled = true;
		}

		// Token: 0x0400342F RID: 13359
		private const int MIN_DISP_ITEM = 3;

		// Token: 0x04003430 RID: 13360
		private const float MINSPEED = 0.01f;

		// Token: 0x04003431 RID: 13361
		protected const float FLICK_THRESHOLD = 0.2f;

		// Token: 0x04003432 RID: 13362
		protected ScrollRectEx m_ScrollRect;

		// Token: 0x04003433 RID: 13363
		[SerializeField]
		private InfiniteScrollItem m_ItemPrefab;

		// Token: 0x04003434 RID: 13364
		[SerializeField]
		protected InfiniteScroll.eScrollDirection m_Direction;

		// Token: 0x04003435 RID: 13365
		[SerializeField]
		protected bool m_Adjust;

		// Token: 0x04003436 RID: 13366
		[SerializeField]
		protected float m_AdjustSpeed = 10f;

		// Token: 0x04003437 RID: 13367
		[SerializeField]
		protected PageSignal m_Signal;

		// Token: 0x04003438 RID: 13368
		protected List<InfiniteScrollItemData> m_ItemList = new List<InfiniteScrollItemData>();

		// Token: 0x04003439 RID: 13369
		protected List<InfiniteScrollItem> m_EmptyInstList = new List<InfiniteScrollItem>();

		// Token: 0x0400343A RID: 13370
		protected List<InfiniteScrollItem> m_ActiveInstList = new List<InfiniteScrollItem>();

		// Token: 0x0400343B RID: 13371
		protected float m_ScrollValue;

		// Token: 0x0400343C RID: 13372
		protected float m_ScrollMax;

		// Token: 0x0400343D RID: 13373
		protected float m_ItemWidth;

		// Token: 0x0400343E RID: 13374
		protected float m_ItemHeight;

		// Token: 0x0400343F RID: 13375
		protected int m_MaxDispItem;

		// Token: 0x04003440 RID: 13376
		protected int m_DispItemNum;

		// Token: 0x04003441 RID: 13377
		protected int m_NowSelect;

		// Token: 0x04003442 RID: 13378
		protected float m_ScrollMove;

		// Token: 0x04003443 RID: 13379
		protected bool m_IsDonePrepare;

		// Token: 0x04003444 RID: 13380
		protected bool m_IsDirty;

		// Token: 0x04003445 RID: 13381
		protected bool m_IsEasing;

		// Token: 0x04003446 RID: 13382
		protected float m_EaseGoal;

		// Token: 0x04003447 RID: 13383
		protected bool m_IsReserveFlick;

		// Token: 0x0400344A RID: 13386
		protected bool m_NotInfinite;

		// Token: 0x02000881 RID: 2177
		protected enum eScrollDirection
		{
			// Token: 0x0400344C RID: 13388
			Horizontal,
			// Token: 0x0400344D RID: 13389
			Vertical
		}
	}
}
