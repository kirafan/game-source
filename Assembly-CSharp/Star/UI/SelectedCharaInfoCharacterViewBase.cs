﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B3 RID: 1971
	public class SelectedCharaInfoCharacterViewBase : MonoBehaviour
	{
		// Token: 0x060028AA RID: 10410 RVA: 0x000D9212 File Offset: 0x000D7612
		public virtual void Setup()
		{
			this.m_AllowStateAdvances = new bool[7];
			this.m_Ewcdc = null;
			this.m_Cdc = null;
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Wait);
		}

		// Token: 0x060028AB RID: 10411 RVA: 0x000D9235 File Offset: 0x000D7635
		public virtual void Destroy()
		{
		}

		// Token: 0x060028AC RID: 10412 RVA: 0x000D9237 File Offset: 0x000D7637
		public virtual void RequestLoad(EquipWeaponCharacterDataController ewcdc, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			this.m_Ewcdc = ewcdc;
			this.m_Cdc = this.m_Ewcdc;
			this.RequestLoad(ewcdc.GetCharaId(), ewcdc.GetWeaponId(), toStateAutomaticallyAdvance);
		}

		// Token: 0x060028AD RID: 10413 RVA: 0x000D925F File Offset: 0x000D765F
		public virtual void RequestLoad(CharacterDataController cdc, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			this.m_Ewcdc = null;
			this.m_Cdc = cdc;
			this.RequestLoad(cdc.GetCharaId(), -1, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028AE RID: 10414 RVA: 0x000D927D File Offset: 0x000D767D
		[Obsolete]
		public virtual void RequestLoad(long charaMngID, bool autoDisplay = false)
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Wait)
			{
				return;
			}
			this.RegisterStateAutomaticAdvance(SelectedCharaInfoCharacterViewBase.eState.Prepare);
			this.RegisterStateAutomaticAdvance(SelectedCharaInfoCharacterViewBase.eState.In);
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Prepare);
		}

		// Token: 0x060028AF RID: 10415 RVA: 0x000D92A1 File Offset: 0x000D76A1
		[Obsolete]
		public virtual void RequestLoad(int charaID, int weaponID, bool autoDisplay)
		{
			this.RequestLoad(charaID, weaponID, (!autoDisplay) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle);
		}

		// Token: 0x060028B0 RID: 10416 RVA: 0x000D92B8 File Offset: 0x000D76B8
		public virtual void RequestLoad(int charaID, int weaponID, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			SelectedCharaInfoCharacterViewBase.eState state = this.GetState();
			if (state != SelectedCharaInfoCharacterViewBase.eState.Wait && state != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			this.SetStateAndRegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState.Prepare, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028B1 RID: 10417 RVA: 0x000D92E4 File Offset: 0x000D76E4
		public virtual void ForceRequestLoad(EquipWeaponCharacterDataController ewcdc, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			SelectedCharaInfoCharacterViewBase.eState state = this.GetState();
			if (state != SelectedCharaInfoCharacterViewBase.eState.Wait && state != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				this.m_Ewcdc = ewcdc;
				this.m_Cdc = this.m_Ewcdc;
				this.RegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState.Wait, toStateAutomaticallyAdvance);
				this.PlayOut(SelectedCharaInfoCharacterViewBase.eState.Num);
				return;
			}
			this.RequestLoad(ewcdc, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028B2 RID: 10418 RVA: 0x000D9334 File Offset: 0x000D7734
		public virtual void ForceRequestLoad(CharacterDataController cdc, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			SelectedCharaInfoCharacterViewBase.eState state = this.GetState();
			if (state != SelectedCharaInfoCharacterViewBase.eState.Wait && state != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				this.m_Ewcdc = null;
				this.m_Cdc = cdc;
				this.RegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState.Wait, toStateAutomaticallyAdvance);
				this.PlayOut(SelectedCharaInfoCharacterViewBase.eState.Num);
				return;
			}
			this.RequestLoad(cdc, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028B3 RID: 10419 RVA: 0x000D937C File Offset: 0x000D777C
		protected virtual void Update()
		{
			SelectedCharaInfoCharacterViewBase.eState state = this.GetState();
			if (this.IsAllowStateAdvance(state))
			{
				switch (state)
				{
				case SelectedCharaInfoCharacterViewBase.eState.Prepare:
					if (!this.IsPreparing())
					{
						this.SetState(SelectedCharaInfoCharacterViewBase.eState.Prepared);
					}
					break;
				case SelectedCharaInfoCharacterViewBase.eState.Prepared:
					this.PlayIn(SelectedCharaInfoCharacterViewBase.eState.Idle);
					break;
				case SelectedCharaInfoCharacterViewBase.eState.In:
					if (!this.IsAnimStateIn())
					{
						this.FinishIn();
					}
					break;
				case SelectedCharaInfoCharacterViewBase.eState.Out:
					if (!this.IsAnimStateOut())
					{
						this.FinishWait();
					}
					break;
				}
			}
		}

		// Token: 0x060028B4 RID: 10420 RVA: 0x000D941A File Offset: 0x000D781A
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, EquipWeaponCharacterDataController ewcdc)
		{
			if (this.m_Cdc != ewcdc)
			{
				if (toStateAutomaticallyAdvance < SelectedCharaInfoCharacterViewBase.eState.In)
				{
					toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.In;
				}
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Wait);
				this.RequestLoad(ewcdc, toStateAutomaticallyAdvance);
				return;
			}
			this.PlayIn(toStateAutomaticallyAdvance);
		}

		// Token: 0x060028B5 RID: 10421 RVA: 0x000D9449 File Offset: 0x000D7849
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance, CharacterDataController cdc)
		{
			if (this.m_Cdc != cdc)
			{
				if (toStateAutomaticallyAdvance < SelectedCharaInfoCharacterViewBase.eState.In)
				{
					toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.In;
				}
				this.SetState(SelectedCharaInfoCharacterViewBase.eState.Wait);
				this.RequestLoad(cdc, toStateAutomaticallyAdvance);
				return;
			}
			this.PlayIn(toStateAutomaticallyAdvance);
		}

		// Token: 0x060028B6 RID: 10422 RVA: 0x000D9478 File Offset: 0x000D7878
		[Obsolete]
		public virtual void PlayIn(bool autoIdle)
		{
			this.PlayIn((!autoIdle) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Idle);
		}

		// Token: 0x060028B7 RID: 10423 RVA: 0x000D948D File Offset: 0x000D788D
		public virtual void PlayIn(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Idle)
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepare && this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Prepared)
			{
				return;
			}
			if (toStateAutomaticallyAdvance != SelectedCharaInfoCharacterViewBase.eState.Invalid)
			{
				this.RegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState.In, SelectedCharaInfoCharacterViewBase.eState.Idle);
			}
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.In);
		}

		// Token: 0x060028B8 RID: 10424 RVA: 0x000D94BD File Offset: 0x000D78BD
		public virtual void FinishIn()
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.In)
			{
				return;
			}
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Idle);
		}

		// Token: 0x060028B9 RID: 10425 RVA: 0x000D94D3 File Offset: 0x000D78D3
		[Obsolete]
		public virtual void PlayOut(bool autoFinish)
		{
			this.PlayOut((!autoFinish) ? SelectedCharaInfoCharacterViewBase.eState.Invalid : SelectedCharaInfoCharacterViewBase.eState.Num);
		}

		// Token: 0x060028BA RID: 10426 RVA: 0x000D94E8 File Offset: 0x000D78E8
		public virtual void PlayOut(SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Num)
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Idle)
			{
				return;
			}
			this.SetStateAndRegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState.Out, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028BB RID: 10427 RVA: 0x000D94FF File Offset: 0x000D78FF
		public virtual void FinishWait()
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Out)
			{
				return;
			}
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Prepared);
		}

		// Token: 0x060028BC RID: 10428 RVA: 0x000D9515 File Offset: 0x000D7915
		public virtual void Recovery()
		{
			if (this.GetState() != SelectedCharaInfoCharacterViewBase.eState.Invalid)
			{
				return;
			}
			this.SetState(SelectedCharaInfoCharacterViewBase.eState.Wait);
		}

		// Token: 0x060028BD RID: 10429 RVA: 0x000D952A File Offset: 0x000D792A
		public SelectedCharaInfoCharacterViewBase.eState GetState()
		{
			return this.m_State;
		}

		// Token: 0x060028BE RID: 10430 RVA: 0x000D9534 File Offset: 0x000D7934
		protected void SetState(SelectedCharaInfoCharacterViewBase.eState state)
		{
			this.ResetAllowStateAdvance(this.m_State);
			this.m_State = state;
			if (this.m_State == SelectedCharaInfoCharacterViewBase.eState.Wait && this.IsAllowStateAdvance(this.m_State))
			{
				if (this.m_Ewcdc != null)
				{
					this.RequestLoad(this.m_Ewcdc, SelectedCharaInfoCharacterViewBase.eState.Idle);
				}
				else if (this.m_Cdc != null)
				{
					this.RequestLoad(this.m_Cdc, SelectedCharaInfoCharacterViewBase.eState.Idle);
				}
			}
		}

		// Token: 0x060028BF RID: 10431 RVA: 0x000D95A6 File Offset: 0x000D79A6
		protected void SetStateAndRegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState state, SelectedCharaInfoCharacterViewBase.eState toStateAutomaticallyAdvance = SelectedCharaInfoCharacterViewBase.eState.Invalid)
		{
			this.SetState(state);
			this.RegisterStateAutomaticAdvanceRange(state, toStateAutomaticallyAdvance);
		}

		// Token: 0x060028C0 RID: 10432 RVA: 0x000D95B8 File Offset: 0x000D79B8
		protected void RegisterStateAutomaticAdvanceRange(SelectedCharaInfoCharacterViewBase.eState begin, SelectedCharaInfoCharacterViewBase.eState end)
		{
			if (begin == SelectedCharaInfoCharacterViewBase.eState.Invalid || end == SelectedCharaInfoCharacterViewBase.eState.Invalid)
			{
				return;
			}
			for (SelectedCharaInfoCharacterViewBase.eState eState = begin; eState < end; eState++)
			{
				this.RegisterStateAutomaticAdvance(eState);
			}
		}

		// Token: 0x060028C1 RID: 10433 RVA: 0x000D95EC File Offset: 0x000D79EC
		protected void ResetAllowStateAdvanceAll()
		{
			for (int i = 0; i < this.m_AllowStateAdvances.Length; i++)
			{
				this.ResetAllowStateAdvance(i);
			}
		}

		// Token: 0x060028C2 RID: 10434 RVA: 0x000D961C File Offset: 0x000D7A1C
		protected void RegisterStateAutomaticAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
			if (this.m_AllowStateAdvances == null)
			{
				return;
			}
			if (automaticAdvanceState < SelectedCharaInfoCharacterViewBase.eState.Invalid || (SelectedCharaInfoCharacterViewBase.eState)this.m_AllowStateAdvances.Length <= automaticAdvanceState)
			{
				return;
			}
			this.m_AllowStateAdvances[(int)automaticAdvanceState] = true;
		}

		// Token: 0x060028C3 RID: 10435 RVA: 0x000D9656 File Offset: 0x000D7A56
		protected void ResetAllowStateAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
			this.ResetAllowStateAdvance((int)automaticAdvanceState);
		}

		// Token: 0x060028C4 RID: 10436 RVA: 0x000D9660 File Offset: 0x000D7A60
		protected void ResetAllowStateAdvance(int nAutomaticAdvanceState)
		{
			if (this.m_AllowStateAdvances == null)
			{
				return;
			}
			if (nAutomaticAdvanceState < 0 || this.m_AllowStateAdvances.Length <= nAutomaticAdvanceState)
			{
				return;
			}
			this.m_AllowStateAdvances[nAutomaticAdvanceState] = false;
		}

		// Token: 0x060028C5 RID: 10437 RVA: 0x000D969A File Offset: 0x000D7A9A
		public virtual void SetLimitBreak(int lb)
		{
		}

		// Token: 0x060028C6 RID: 10438 RVA: 0x000D969C File Offset: 0x000D7A9C
		protected bool IsAllowStateAdvance(SelectedCharaInfoCharacterViewBase.eState automaticAdvanceState)
		{
			return this.m_AllowStateAdvances != null && automaticAdvanceState >= SelectedCharaInfoCharacterViewBase.eState.Invalid && (SelectedCharaInfoCharacterViewBase.eState)this.m_AllowStateAdvances.Length > automaticAdvanceState && this.m_AllowStateAdvances[(int)automaticAdvanceState];
		}

		// Token: 0x060028C7 RID: 10439 RVA: 0x000D96D7 File Offset: 0x000D7AD7
		public virtual bool IsPreparing()
		{
			return false;
		}

		// Token: 0x060028C8 RID: 10440 RVA: 0x000D96DA File Offset: 0x000D7ADA
		public virtual bool IsAnimStateIn()
		{
			return false;
		}

		// Token: 0x060028C9 RID: 10441 RVA: 0x000D96DD File Offset: 0x000D7ADD
		public virtual bool IsAnimStateOut()
		{
			return false;
		}

		// Token: 0x04002F9A RID: 12186
		private SelectedCharaInfoCharacterViewBase.eState m_State;

		// Token: 0x04002F9B RID: 12187
		private EquipWeaponCharacterDataController m_Ewcdc;

		// Token: 0x04002F9C RID: 12188
		private CharacterDataController m_Cdc;

		// Token: 0x04002F9D RID: 12189
		private bool[] m_AllowStateAdvances;

		// Token: 0x020007B4 RID: 1972
		public enum eState
		{
			// Token: 0x04002F9F RID: 12191
			Invalid,
			// Token: 0x04002FA0 RID: 12192
			Wait,
			// Token: 0x04002FA1 RID: 12193
			Prepare,
			// Token: 0x04002FA2 RID: 12194
			Prepared,
			// Token: 0x04002FA3 RID: 12195
			In,
			// Token: 0x04002FA4 RID: 12196
			Idle,
			// Token: 0x04002FA5 RID: 12197
			Out,
			// Token: 0x04002FA6 RID: 12198
			Num
		}
	}
}
