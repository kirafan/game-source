﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000888 RID: 2184
	public class CharaDetailDisplay : MonoBehaviour
	{
		// Token: 0x06002D5B RID: 11611 RVA: 0x000EF9C8 File Offset: 0x000EDDC8
		public void Apply(int charaID)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID);
			this.m_CharaIconWithFrame.ApplyCharaID(charaID);
			this.m_NameText.text = param.m_Name;
			this.m_CostText.text = param.m_Cost.ToString();
			int[] values = new int[]
			{
				param.m_InitHp,
				param.m_InitAtk,
				param.m_InitMgc,
				param.m_InitDef,
				param.m_InitMDef,
				param.m_InitSpd
			};
			this.m_StatusDisplay.Apply(values, null);
			for (int i = 0; i < this.m_SkillInfos.Length; i++)
			{
				if (i == 0)
				{
					this.m_SkillInfos[i].Apply(SkillIcon.eSkillDBType.Player, param.m_CharaSkillID, (eElementType)param.m_Element, 1, param.m_SkillLimitLv, -1L);
				}
				else
				{
					this.m_SkillInfos[i].Apply(SkillIcon.eSkillDBType.Player, param.m_ClassSkillIDs[i - 1], (eElementType)param.m_Element, 1, param.m_SkillLimitLv, -1L);
				}
			}
		}

		// Token: 0x04003467 RID: 13415
		[SerializeField]
		private CharaIconWithFrame m_CharaIconWithFrame;

		// Token: 0x04003468 RID: 13416
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003469 RID: 13417
		[SerializeField]
		private Text m_CostText;

		// Token: 0x0400346A RID: 13418
		[SerializeField]
		private CharaStatus m_StatusDisplay;

		// Token: 0x0400346B RID: 13419
		[SerializeField]
		private SkillInfo[] m_SkillInfos;
	}
}
