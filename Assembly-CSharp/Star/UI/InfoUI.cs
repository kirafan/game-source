﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020009C3 RID: 2499
	public class InfoUI : MenuUIBase
	{
		// Token: 0x060033EF RID: 13295 RVA: 0x001073D4 File Offset: 0x001057D4
		private void SetSelectTab(InfoUI.eTabButton tab)
		{
			for (int i = 0; i < this.m_TabButtons.Length; i++)
			{
				this.m_SwapObjects[i].Swap((tab != (InfoUI.eTabButton)i) ? 0 : 1);
			}
			this.m_SelectTab = tab;
			if (this.m_WebViewGroup != null)
			{
				switch (this.m_SelectTab)
				{
				case InfoUI.eTabButton.Notice:
					this.m_ActiveURL = WebDataListUtil.GetIDToAdress(1010);
					break;
				case InfoUI.eTabButton.Mente:
					this.m_ActiveURL = WebDataListUtil.GetIDToAdress(1011);
					break;
				case InfoUI.eTabButton.Update:
					this.m_ActiveURL = WebDataListUtil.GetIDToAdress(1012);
					break;
				}
			}
		}

		// Token: 0x060033F0 RID: 13296 RVA: 0x00107491 File Offset: 0x00105891
		private void CallbackFromWebView(string msg)
		{
			Debug.Log("CallbackFromWebView : msg = " + msg);
			if (this.m_Type != InfoUI.eType.Top)
			{
				return;
			}
			this.OnClickBannerButton(msg);
		}

		// Token: 0x060033F1 RID: 13297 RVA: 0x001074B8 File Offset: 0x001058B8
		private void OnDestroy()
		{
			if (this.m_ScrollView != null)
			{
				this.m_ScrollView.Destroy();
				this.m_ScrollView = null;
			}
			if (this.m_BannerLoader != null)
			{
				this.m_BannerLoader.Destroy();
				this.m_BannerLoader = null;
			}
		}

		// Token: 0x060033F2 RID: 13298 RVA: 0x00107508 File Offset: 0x00105908
		public void OnClickTabButton(int tabButtonIdx)
		{
			switch (tabButtonIdx)
			{
			}
			this.SetSelectTab((InfoUI.eTabButton)tabButtonIdx);
			this.OpenWindow(null);
		}

		// Token: 0x060033F3 RID: 13299 RVA: 0x00107550 File Offset: 0x00105950
		public void Setup(string startUrl)
		{
			if (this.m_SwapObjects == null)
			{
				this.m_SwapObjects = new SwapObj[this.m_TabButtons.Length];
				for (int i = 0; i < this.m_TabButtons.Length; i++)
				{
					this.m_SwapObjects[i] = this.m_TabButtons[i].GetComponent<SwapObj>();
				}
			}
			this.OpenWindow(startUrl);
			if (this.m_BannerLoader == null)
			{
				this.m_BannerLoader = new InfoBannerLoader();
				this.m_BannerLoader.Prepare(InfoBannerLoader.eBannerType.Large);
				this.m_BannerLoader.Request_InfoBanner(new Action(this.OnDoneRequest));
			}
		}

		// Token: 0x060033F4 RID: 13300 RVA: 0x001075EC File Offset: 0x001059EC
		private void OpenWindow(string url)
		{
			InfoUI.eType type = this.m_Type;
			if (url == null)
			{
				this.m_Type = InfoUI.eType.Top;
				this.m_WebViewGroup.targetRect = this.m_WebViewTargetForTop;
				this.SetSelectTab((this.m_SelectTab != InfoUI.eTabButton.Max) ? this.m_SelectTab : InfoUI.eTabButton.Notice);
			}
			else
			{
				this.m_Type = InfoUI.eType.Details;
				this.m_WebViewGroup.targetRect = this.m_WebViewTargetForDetails;
				this.m_ActiveURL = url;
			}
			if (this.m_Step == InfoUI.eStep.Idle)
			{
				if (type == this.m_Type)
				{
					this.ChangeStep(InfoUI.eStep.WebWindowClose);
				}
				else
				{
					this.ChangeStep(InfoUI.eStep.ChildWindowOpen);
				}
			}
			else
			{
				this.ChangeStep(InfoUI.eStep.In);
			}
		}

		// Token: 0x060033F5 RID: 13301 RVA: 0x00107697 File Offset: 0x00105A97
		private void OnDoneRequest()
		{
			this.m_isDoneRequest = true;
		}

		// Token: 0x060033F6 RID: 13302 RVA: 0x001076A0 File Offset: 0x00105AA0
		private void Update()
		{
			switch (this.m_Step)
			{
			case InfoUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (this.m_AnimList[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag && this.m_isDoneRequest)
				{
					this.m_BannerLoader.UpdatePrepare();
					if (this.m_BannerLoader.IsDonePrepare())
					{
						if (this.m_ScrollView != null)
						{
							this.m_ScrollView.Setup();
							for (int j = 0; j < this.m_BannerLoader.GetList().Count; j++)
							{
								if (this.m_BannerLoader.GetList()[j].m_StartTime <= SingletonMonoBehaviour<GameSystem>.Inst.ServerTime && this.m_BannerLoader.GetList()[j].m_EndTime >= SingletonMonoBehaviour<GameSystem>.Inst.ServerTime)
								{
									InfoUIScrollItemData infoUIScrollItemData = new InfoUIScrollItemData();
									infoUIScrollItemData.m_ID = this.m_BannerLoader.GetList()[j].m_ID;
									infoUIScrollItemData.m_URL = this.m_BannerLoader.GetList()[j].m_Url;
									infoUIScrollItemData.m_Sprite = this.m_BannerLoader.GetSprite(this.m_BannerLoader.GetList()[j].m_ImgID);
									infoUIScrollItemData.m_startTime = this.m_BannerLoader.GetList()[j].m_StartTime;
									infoUIScrollItemData.m_endTime = this.m_BannerLoader.GetList()[j].m_EndTime;
									infoUIScrollItemData.m_InfoUI = this;
									this.m_ScrollView.AddItem(infoUIScrollItemData);
								}
							}
						}
						this.ChangeStep(InfoUI.eStep.ChildWindowOpen);
					}
				}
				break;
			}
			case InfoUI.eStep.ChildWindowOpen:
			{
				InfoUI.eType type = this.m_Type;
				if (type != InfoUI.eType.Top)
				{
					if (type == InfoUI.eType.Details)
					{
						if (!this.m_DetailUIGroup.IsOpen())
						{
							return;
						}
					}
				}
				else if (!this.m_TopUIGroup.IsOpen())
				{
					return;
				}
				this.ChangeStep(InfoUI.eStep.WebWindowClose);
				break;
			}
			case InfoUI.eStep.WebWindowClose:
				if (this.m_WebViewGroup.IsOpen())
				{
					return;
				}
				this.ChangeStep(InfoUI.eStep.WebWindowOpen);
				break;
			case InfoUI.eStep.WebWindowOpen:
				if (!this.m_WebViewGroup.IsOpen())
				{
					return;
				}
				this.ChangeStep(InfoUI.eStep.Idle);
				break;
			case InfoUI.eStep.Idle:
				this.UpdateAndroidBackKey();
				break;
			case InfoUI.eStep.Out:
			{
				bool flag2 = true;
				for (int k = 0; k < this.m_AnimList.Length; k++)
				{
					if (this.m_AnimList[k].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					this.ChangeStep(InfoUI.eStep.End);
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				}
				break;
			}
			}
		}

		// Token: 0x060033F7 RID: 13303 RVA: 0x001079A4 File Offset: 0x00105DA4
		private void ChangeStep(InfoUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case InfoUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case InfoUI.eStep.In:
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					this.m_AnimList[i].PlayIn();
				}
				this.m_BGGroup.Open();
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case InfoUI.eStep.ChildWindowOpen:
			{
				if (this.m_WebViewGroup.IsOpen())
				{
					this.m_WebViewGroup.Close();
				}
				InfoUI.eType type = this.m_Type;
				if (type != InfoUI.eType.Top)
				{
					if (type == InfoUI.eType.Details)
					{
						this.m_DetailUIGroup.Open();
						if (this.m_TopUIGroup.IsOpen())
						{
							this.m_TopUIGroup.Close();
						}
					}
				}
				else
				{
					this.m_TopUIGroup.Open();
					if (this.m_DetailUIGroup.IsOpen())
					{
						this.m_DetailUIGroup.Close();
					}
				}
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			}
			case InfoUI.eStep.WebWindowClose:
				if (this.m_WebViewGroup.IsOpen())
				{
					this.m_WebViewGroup.Close();
				}
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case InfoUI.eStep.WebWindowOpen:
			{
				if (this.m_WebViewGroup.IsOpen())
				{
					this.m_WebViewGroup.Close();
				}
				InfoUI.eType type2 = this.m_Type;
				if (type2 != InfoUI.eType.Top)
				{
					if (type2 == InfoUI.eType.Details)
					{
						this.m_WebViewGroup.Open(this.m_ActiveURL, null);
					}
				}
				else
				{
					this.m_WebViewGroup.Open(this.m_ActiveURL, new Action<string>(this.CallbackFromWebView));
				}
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			}
			case InfoUI.eStep.Idle:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case InfoUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case InfoUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x060033F8 RID: 13304 RVA: 0x00107BE1 File Offset: 0x00105FE1
		public override void PlayIn()
		{
			this.ChangeStep(InfoUI.eStep.In);
		}

		// Token: 0x060033F9 RID: 13305 RVA: 0x00107BEC File Offset: 0x00105FEC
		public override void PlayOut()
		{
			this.m_BGGroup.Close();
			if (this.m_WebViewGroup.IsOpen())
			{
				this.m_WebViewGroup.Close();
			}
			this.ChangeStep(InfoUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
		}

		// Token: 0x060033FA RID: 13306 RVA: 0x00107C62 File Offset: 0x00106062
		public override bool IsMenuEnd()
		{
			return this.m_Step == InfoUI.eStep.End;
		}

		// Token: 0x060033FB RID: 13307 RVA: 0x00107C70 File Offset: 0x00106070
		public void OnBackButtonCallBack()
		{
			InfoUI.eType type = this.m_Type;
			if (type != InfoUI.eType.Top)
			{
				if (type == InfoUI.eType.Details)
				{
					this.OpenWindow(null);
				}
			}
			else
			{
				this.PlayOut();
			}
		}

		// Token: 0x060033FC RID: 13308 RVA: 0x00107CAD File Offset: 0x001060AD
		public void OnCloseButtonCallBack()
		{
			this.PlayOut();
		}

		// Token: 0x060033FD RID: 13309 RVA: 0x00107CB5 File Offset: 0x001060B5
		private void OnClickBannerButton()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060033FE RID: 13310 RVA: 0x00107CBC File Offset: 0x001060BC
		public void OnClickBannerButton(string url)
		{
			this.OpenWindow(url);
		}

		// Token: 0x060033FF RID: 13311 RVA: 0x00107CC5 File Offset: 0x001060C5
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_BackButton, null);
		}

		// Token: 0x04003A23 RID: 14883
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003A24 RID: 14884
		[SerializeField]
		private UIGroup m_BGGroup;

		// Token: 0x04003A25 RID: 14885
		[SerializeField]
		private UIGroup m_TopUIGroup;

		// Token: 0x04003A26 RID: 14886
		[SerializeField]
		private UIGroup m_DetailUIGroup;

		// Token: 0x04003A27 RID: 14887
		[SerializeField]
		private ScrollViewBase m_ScrollView;

		// Token: 0x04003A28 RID: 14888
		[SerializeField]
		private CustomButton m_BackButton;

		// Token: 0x04003A29 RID: 14889
		[SerializeField]
		private CustomButton[] m_TabButtons = new CustomButton[3];

		// Token: 0x04003A2A RID: 14890
		private SwapObj[] m_SwapObjects;

		// Token: 0x04003A2B RID: 14891
		private InfoUI.eStep m_Step;

		// Token: 0x04003A2C RID: 14892
		private string m_ActiveURL;

		// Token: 0x04003A2D RID: 14893
		private InfoUI.eTabButton m_SelectTab = InfoUI.eTabButton.Max;

		// Token: 0x04003A2E RID: 14894
		private InfoUI.eType m_Type;

		// Token: 0x04003A2F RID: 14895
		private InfoBannerLoader m_BannerLoader;

		// Token: 0x04003A30 RID: 14896
		private bool m_isDoneRequest;

		// Token: 0x04003A31 RID: 14897
		[SerializeField]
		private RectTransform m_WebViewTargetForTop;

		// Token: 0x04003A32 RID: 14898
		[SerializeField]
		private RectTransform m_WebViewTargetForDetails;

		// Token: 0x04003A33 RID: 14899
		[SerializeField]
		private WebViewGroup m_WebViewGroup;

		// Token: 0x020009C4 RID: 2500
		private enum eStep
		{
			// Token: 0x04003A35 RID: 14901
			Hide,
			// Token: 0x04003A36 RID: 14902
			In,
			// Token: 0x04003A37 RID: 14903
			ChildWindowOpen,
			// Token: 0x04003A38 RID: 14904
			WebWindowClose,
			// Token: 0x04003A39 RID: 14905
			WebWindowOpen,
			// Token: 0x04003A3A RID: 14906
			Idle,
			// Token: 0x04003A3B RID: 14907
			Out,
			// Token: 0x04003A3C RID: 14908
			End
		}

		// Token: 0x020009C5 RID: 2501
		public enum eTabButton
		{
			// Token: 0x04003A3E RID: 14910
			Notice,
			// Token: 0x04003A3F RID: 14911
			Mente,
			// Token: 0x04003A40 RID: 14912
			Update,
			// Token: 0x04003A41 RID: 14913
			Max
		}

		// Token: 0x020009C6 RID: 2502
		private enum eType
		{
			// Token: 0x04003A43 RID: 14915
			Top,
			// Token: 0x04003A44 RID: 14916
			Details,
			// Token: 0x04003A45 RID: 14917
			Max
		}
	}
}
