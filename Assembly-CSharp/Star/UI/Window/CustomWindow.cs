﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Window
{
	// Token: 0x02000814 RID: 2068
	public class CustomWindow : MonoBehaviour
	{
		// Token: 0x14000061 RID: 97
		// (add) Token: 0x06002AE9 RID: 10985 RVA: 0x000E28B4 File Offset: 0x000E0CB4
		// (remove) Token: 0x06002AEA RID: 10986 RVA: 0x000E28EC File Offset: 0x000E0CEC
		public event Action OnClickFooterButton;

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x06002AEB RID: 10987 RVA: 0x000E2922 File Offset: 0x000E0D22
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x06002AEC RID: 10988 RVA: 0x000E2947 File Offset: 0x000E0D47
		public RectTransform WindowRectTransform
		{
			get
			{
				return this.m_WindowRectTransform;
			}
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x06002AED RID: 10989 RVA: 0x000E294F File Offset: 0x000E0D4F
		public GameObject BGDark
		{
			get
			{
				return this.m_BGDark;
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x06002AEE RID: 10990 RVA: 0x000E2957 File Offset: 0x000E0D57
		public int LastPressedFooterButtonID
		{
			get
			{
				return this.m_LastPressedFooterButtonID;
			}
		}

		// Token: 0x06002AEF RID: 10991 RVA: 0x000E295F File Offset: 0x000E0D5F
		protected virtual void Update()
		{
			this.UpdateAndroidBackKey();
		}

		// Token: 0x06002AF0 RID: 10992 RVA: 0x000E2968 File Offset: 0x000E0D68
		public void Open()
		{
			this.m_LastPressedFooterButtonID = -1;
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				this.m_Anims[i].PlayIn();
			}
			if (this.m_BGDark != null)
			{
				FixSortOrder component = this.RectTransform.GetComponent<FixSortOrder>();
				this.m_BGDark.GetComponent<FixSortOrder>().SetSortOrder(component.GetSortOrderID(), component.GetOffset() - 1);
			}
		}

		// Token: 0x06002AF1 RID: 10993 RVA: 0x000E29E0 File Offset: 0x000E0DE0
		public void Close()
		{
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				this.m_Anims[i].PlayOut();
			}
		}

		// Token: 0x06002AF2 RID: 10994 RVA: 0x000E2A14 File Offset: 0x000E0E14
		public bool IsPlayingAnim()
		{
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				if (!this.m_Anims[i].IsEnd)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002AF3 RID: 10995 RVA: 0x000E2A4F File Offset: 0x000E0E4F
		public void SetSortOrder(UIDefine.eSortOrderTypeID sortOrderType, int offset)
		{
			this.RectTransform.GetComponent<FixSortOrder>().SetSortOrder(sortOrderType, offset);
			if (this.m_BGDark != null)
			{
				this.m_BGDark.GetComponent<FixSortOrder>().SetSortOrder(sortOrderType, offset - 1);
			}
		}

		// Token: 0x06002AF4 RID: 10996 RVA: 0x000E2A88 File Offset: 0x000E0E88
		public void SetTitle(string titleText)
		{
			if (this.m_Title.m_GameObject == null)
			{
				return;
			}
			if (string.IsNullOrEmpty(titleText))
			{
				if (this.m_Title.m_GameObject.activeSelf)
				{
					this.m_Title.m_GameObject.SetActive(false);
				}
			}
			else
			{
				this.m_Title.m_GameObject.SetActive(true);
				this.m_Title.m_TextObj.text = titleText;
			}
		}

		// Token: 0x06002AF5 RID: 10997 RVA: 0x000E2B04 File Offset: 0x000E0F04
		public void SetContent(RectTransform contentObj)
		{
			if (contentObj == null)
			{
				return;
			}
			contentObj.SetParent(this.m_Content.m_ContentParent, false);
			contentObj.anchorMin = new Vector2(0.5f, 1f);
			contentObj.anchorMax = new Vector2(0.5f, 1f);
			contentObj.pivot = new Vector2(0.5f, 1f);
			contentObj.anchoredPosition = new Vector2(0f, 0f);
			contentObj.localScale = Vector3.one;
		}

		// Token: 0x06002AF6 RID: 10998 RVA: 0x000E2B90 File Offset: 0x000E0F90
		public void ClearFooterButton()
		{
			for (int i = 0; i < this.m_FooterButtonList.Count; i++)
			{
				UnityEngine.Object.Destroy(this.m_FooterButtonList[i].gameObject);
			}
			this.m_FooterButtonList.Clear();
			this.m_TargetBackButton = null;
		}

		// Token: 0x06002AF7 RID: 10999 RVA: 0x000E2BE4 File Offset: 0x000E0FE4
		public FooterButton AddFooterButton(int id, string text)
		{
			FooterButton footerButton = UnityEngine.Object.Instantiate<FooterButton>(this.m_Footer.m_FooterButtonPrefab, this.m_Footer.m_FooterButtonParent);
			footerButton.Create(id);
			Text text2 = footerButton.GetComponentsInChildren<Text>(true)[0];
			text2.text = text;
			float num = text2.preferredWidth + 32f;
			float width = footerButton.CustomButton.GetComponent<RectTransform>().rect.width;
			if (num > width)
			{
				float num2 = width / num;
				text2.rectTransform.localScale = new Vector3(num2, num2);
			}
			FooterButton footerButton2 = footerButton;
			footerButton2.OnClick = (Action<int>)Delegate.Combine(footerButton2.OnClick, new Action<int>(this.OnClickFooterButtonCallBack));
			this.m_FooterButtonList.Add(footerButton);
			return footerButton;
		}

		// Token: 0x06002AF8 RID: 11000 RVA: 0x000E2C9B File Offset: 0x000E109B
		public void OnClickFooterButtonCallBack(int buttonID)
		{
			this.m_LastPressedFooterButtonID = buttonID;
			this.OnClickFooterButton.Call();
		}

		// Token: 0x06002AF9 RID: 11001 RVA: 0x000E2CAF File Offset: 0x000E10AF
		public void SetFooterOK()
		{
			this.ClearFooterButton();
			this.AddFooterButton(0, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonOK));
		}

		// Token: 0x06002AFA RID: 11002 RVA: 0x000E2CD4 File Offset: 0x000E10D4
		public void SetFooterClose()
		{
			this.ClearFooterButton();
			FooterButton footerButton = this.AddFooterButton(0, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonClose));
			if (footerButton != null)
			{
				this.m_TargetBackButton = footerButton.CustomButton;
			}
		}

		// Token: 0x06002AFB RID: 11003 RVA: 0x000E2D1C File Offset: 0x000E111C
		public void SetFooterYesNo()
		{
			this.ClearFooterButton();
			FooterButton footerButton = this.AddFooterButton(1, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonNo));
			if (footerButton != null)
			{
				this.m_TargetBackButton = footerButton.CustomButton;
			}
			this.AddFooterButton(0, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonYES));
		}

		// Token: 0x06002AFC RID: 11004 RVA: 0x000E2D80 File Offset: 0x000E1180
		public void SetFooterCancel()
		{
			this.ClearFooterButton();
			FooterButton footerButton = this.AddFooterButton(0, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonCancel));
			if (footerButton != null)
			{
				this.m_TargetBackButton = footerButton.CustomButton;
			}
		}

		// Token: 0x06002AFD RID: 11005 RVA: 0x000E2DC8 File Offset: 0x000E11C8
		public FooterButton GetFooterButton(int id)
		{
			for (int i = 0; i < this.m_FooterButtonList.Count; i++)
			{
				if (this.m_FooterButtonList[i].GetID() == id)
				{
					return this.m_FooterButtonList[i];
				}
			}
			return null;
		}

		// Token: 0x06002AFE RID: 11006 RVA: 0x000E2E18 File Offset: 0x000E1218
		private void UpdateAndroidBackKey()
		{
			if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape))
			{
				if (InputTouch.Inst.GetAvailableTouchCnt() > 0)
				{
					return;
				}
				if (!this.m_IsAvailableAndroidBackKey)
				{
					return;
				}
				if (this.IsPlayingAnim())
				{
					return;
				}
				if (this.m_TargetBackButton == null)
				{
					this.m_TargetBackButton = this.FindTargetBackButton();
				}
				if (this.m_TargetBackButton == null)
				{
					return;
				}
				if (!this.m_TargetBackButton.IsEnableInput)
				{
					return;
				}
				if (!this.m_TargetBackButton.Interactable)
				{
					return;
				}
				if (!Utility.CheckTapUI(this.m_TargetBackButton.gameObject))
				{
					return;
				}
				this.m_TargetBackButton.ExecuteClick();
			}
		}

		// Token: 0x06002AFF RID: 11007 RVA: 0x000E2ED8 File Offset: 0x000E12D8
		private CustomButton FindTargetBackButton()
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>();
			if (componentsInChildren != null)
			{
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					Text[] componentsInChildren2 = componentsInChildren[i].GetComponentsInChildren<Text>();
					if (componentsInChildren2 != null)
					{
						for (int j = 0; j < componentsInChildren2.Length; j++)
						{
							if (this.CompareTargetBackButtonText(componentsInChildren2[j].text))
							{
								return componentsInChildren[i];
							}
						}
					}
				}
			}
			return null;
		}

		// Token: 0x06002B00 RID: 11008 RVA: 0x000E2F44 File Offset: 0x000E1344
		private bool CompareTargetBackButtonText(string text)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			for (int i = 0; i < this.TARGET_BACK_BUTTON_ENUMS_Common.Length; i++)
			{
				if (text == dbMng.GetTextCommon(this.TARGET_BACK_BUTTON_ENUMS_Common[i]))
				{
					return true;
				}
			}
			for (int j = 0; j < this.TARGET_BACK_BUTTON_ENUMS_Message.Length; j++)
			{
				if (text == dbMng.GetTextMessage(this.TARGET_BACK_BUTTON_ENUMS_Message[j]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x04003157 RID: 12631
		public const int FOOTER_YES = 0;

		// Token: 0x04003158 RID: 12632
		public const int FOOTER_NO = 1;

		// Token: 0x04003159 RID: 12633
		public const int FOOTER_OK = 0;

		// Token: 0x0400315A RID: 12634
		public const int FOOTER_CLOSE = 0;

		// Token: 0x0400315B RID: 12635
		private const float FOOTER_EXTEND_MARGIN = 32f;

		// Token: 0x0400315C RID: 12636
		private eText_CommonDB[] TARGET_BACK_BUTTON_ENUMS_Common = new eText_CommonDB[]
		{
			eText_CommonDB.CommonOK,
			eText_CommonDB.CommonNo,
			eText_CommonDB.CommonCancel,
			eText_CommonDB.CommonClose,
			eText_CommonDB.CommonGoBack,
			eText_CommonDB.BattleResultNext,
			eText_CommonDB.ADVSkipCancelButton,
			eText_CommonDB.GachaPlayResultWindowCloseButton,
			eText_CommonDB.MissionButtonChancel,
			eText_CommonDB.TitleTitleMainAgreeNoAgreeButton,
			eText_CommonDB.StoreReviewNo
		};

		// Token: 0x0400315D RID: 12637
		private eText_MessageDB[] TARGET_BACK_BUTTON_ENUMS_Message = new eText_MessageDB[]
		{
			eText_MessageDB.GachaPlayReplayCheck,
			eText_MessageDB.MovieSkipNo
		};

		// Token: 0x0400315E RID: 12638
		[SerializeField]
		protected AnimUIPlayer[] m_Anims;

		// Token: 0x0400315F RID: 12639
		[SerializeField]
		private VerticalLayoutGroup m_VerticalLayoutGroup;

		// Token: 0x04003160 RID: 12640
		[SerializeField]
		private RectTransform m_WindowRectTransform;

		// Token: 0x04003161 RID: 12641
		[SerializeField]
		private GameObject m_BGDark;

		// Token: 0x04003162 RID: 12642
		[SerializeField]
		protected CustomWindow.WindowTitle m_Title;

		// Token: 0x04003163 RID: 12643
		[SerializeField]
		protected CustomWindow.WindowContent m_Content;

		// Token: 0x04003164 RID: 12644
		[SerializeField]
		protected CustomWindow.WindowFooter m_Footer;

		// Token: 0x04003165 RID: 12645
		[SerializeField]
		protected bool m_IsAvailableAndroidBackKey = true;

		// Token: 0x04003166 RID: 12646
		protected List<FooterButton> m_FooterButtonList = new List<FooterButton>();

		// Token: 0x04003167 RID: 12647
		protected int m_LastPressedFooterButtonID = -1;

		// Token: 0x04003168 RID: 12648
		protected RectTransform m_RectTransform;

		// Token: 0x04003169 RID: 12649
		public CustomButton m_TargetBackButton;

		// Token: 0x02000815 RID: 2069
		[Serializable]
		public class WindowTitle
		{
			// Token: 0x0400316B RID: 12651
			public GameObject m_GameObject;

			// Token: 0x0400316C RID: 12652
			public Text m_TextObj;
		}

		// Token: 0x02000816 RID: 2070
		[Serializable]
		public class WindowContent
		{
			// Token: 0x0400316D RID: 12653
			public GameObject m_GameObject;

			// Token: 0x0400316E RID: 12654
			public RectTransform m_ContentParent;
		}

		// Token: 0x02000817 RID: 2071
		[Serializable]
		public class WindowFooter
		{
			// Token: 0x0400316F RID: 12655
			public FooterButton m_FooterButtonPrefab;

			// Token: 0x04003170 RID: 12656
			public RectTransform m_FooterButtonParent;
		}
	}
}
