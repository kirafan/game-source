﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI.Window
{
	// Token: 0x02000818 RID: 2072
	public class FooterButton : MonoBehaviour
	{
		// Token: 0x170002BF RID: 703
		// (get) Token: 0x06002B05 RID: 11013 RVA: 0x000E2FEC File Offset: 0x000E13EC
		public CustomButton CustomButton
		{
			get
			{
				return this.m_Button;
			}
		}

		// Token: 0x06002B06 RID: 11014 RVA: 0x000E2FF4 File Offset: 0x000E13F4
		public void Create(int id)
		{
			this.m_ID = id;
			this.m_Button = UnityEngine.Object.Instantiate<CustomButton>(this.m_ButtonPrefab, base.GetComponent<RectTransform>(), false);
			RectTransform component = this.m_Button.GetComponent<RectTransform>();
			component.localPosition = Vector2.zero;
			component.anchorMin = new Vector2(0f, 0f);
			component.anchorMax = new Vector2(1f, 1f);
			component.sizeDelta = new Vector2(0f, 0f);
			this.m_Button.m_OnClick.AddListener(new UnityAction(this.OnClickCallBack));
		}

		// Token: 0x06002B07 RID: 11015 RVA: 0x000E3097 File Offset: 0x000E1497
		public int GetID()
		{
			return this.m_ID;
		}

		// Token: 0x06002B08 RID: 11016 RVA: 0x000E309F File Offset: 0x000E149F
		public void OnClickCallBack()
		{
			this.OnClick.Call(this.m_ID);
		}

		// Token: 0x04003171 RID: 12657
		[SerializeField]
		private CustomButton m_ButtonPrefab;

		// Token: 0x04003172 RID: 12658
		private CustomButton m_Button;

		// Token: 0x04003173 RID: 12659
		private int m_ID = -1;

		// Token: 0x04003174 RID: 12660
		public Action<int> OnClick;
	}
}
