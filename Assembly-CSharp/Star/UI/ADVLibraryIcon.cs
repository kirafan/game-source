﻿using System;

namespace Star.UI
{
	// Token: 0x02000854 RID: 2132
	public class ADVLibraryIcon : ASyncImage
	{
		// Token: 0x06002C5C RID: 11356 RVA: 0x000EA70C File Offset: 0x000E8B0C
		public void Apply(int advLibID)
		{
			if (this.m_AdvLibID != advLibID)
			{
				this.Apply();
				if (advLibID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVLibraryIcon(advLibID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_AdvLibID = advLibID;
			}
		}

		// Token: 0x06002C5D RID: 11357 RVA: 0x000EA772 File Offset: 0x000E8B72
		public override void Destroy()
		{
			base.Destroy();
			this.m_AdvLibID = -1;
		}

		// Token: 0x0400334B RID: 13131
		protected int m_AdvLibID = -1;
	}
}
