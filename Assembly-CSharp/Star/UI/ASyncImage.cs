﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000855 RID: 2133
	public class ASyncImage : MonoBehaviour
	{
		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06002C5F RID: 11359 RVA: 0x000EA4A9 File Offset: 0x000E88A9
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06002C60 RID: 11360 RVA: 0x000EA4B1 File Offset: 0x000E88B1
		public Image Image
		{
			get
			{
				return this.m_Image;
			}
		}

		// Token: 0x06002C61 RID: 11361 RVA: 0x000EA4B9 File Offset: 0x000E88B9
		public virtual void Awake()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_Image = base.GetComponent<Image>();
		}

		// Token: 0x06002C62 RID: 11362 RVA: 0x000EA4D4 File Offset: 0x000E88D4
		public virtual void Start()
		{
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			if (this.m_Image == null)
			{
				this.m_Image = base.GetComponent<Image>();
			}
			if (!this.m_IsDoneApply && this.m_Image != null)
			{
				this.m_Image.enabled = false;
			}
		}

		// Token: 0x06002C63 RID: 11363 RVA: 0x000EA544 File Offset: 0x000E8944
		public virtual void LateUpdate()
		{
			if (this.m_Image.sprite == null && this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable())
			{
				this.ApplyLoadedSprite();
			}
			else if (this.m_Image.enabled && this.m_SpriteHandler != null && !this.m_SpriteHandler.IsAvailable())
			{
				this.ReleaseSprite();
			}
		}

		// Token: 0x06002C64 RID: 11364 RVA: 0x000EA5BE File Offset: 0x000E89BE
		protected virtual void ApplyLoadedSprite()
		{
			this.m_Image.sprite = this.m_SpriteHandler.Obj;
			this.m_Image.enabled = true;
		}

		// Token: 0x06002C65 RID: 11365 RVA: 0x000EA5E2 File Offset: 0x000E89E2
		protected virtual void ReleaseSprite()
		{
			this.m_Image.sprite = null;
			this.m_Image.enabled = false;
		}

		// Token: 0x06002C66 RID: 11366 RVA: 0x000EA5FC File Offset: 0x000E89FC
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x06002C67 RID: 11367 RVA: 0x000EA604 File Offset: 0x000E8A04
		public Sprite GetSprite()
		{
			if (this.IsDoneLoad())
			{
				return this.m_SpriteHandler.Obj;
			}
			return null;
		}

		// Token: 0x06002C68 RID: 11368 RVA: 0x000EA61E File Offset: 0x000E8A1E
		public virtual bool IsDoneLoad()
		{
			return this.m_SpriteHandler != null && this.m_SpriteHandler.IsAvailable();
		}

		// Token: 0x06002C69 RID: 11369 RVA: 0x000EA638 File Offset: 0x000E8A38
		public virtual SpriteHandler GetSpriteHandler()
		{
			return this.m_SpriteHandler;
		}

		// Token: 0x06002C6A RID: 11370 RVA: 0x000EA640 File Offset: 0x000E8A40
		protected virtual void Apply()
		{
			if (this.m_Image == null)
			{
				this.m_Image = base.GetComponent<Image>();
			}
			if (this.m_IsDoneApply)
			{
				this.Destroy();
			}
			this.m_IsDoneApply = true;
		}

		// Token: 0x06002C6B RID: 11371 RVA: 0x000EA678 File Offset: 0x000E8A78
		public virtual void Destroy()
		{
			if (this.m_Image != null)
			{
				this.m_Image.enabled = false;
				this.m_Image.sprite = null;
			}
			if (this.m_SpriteHandler != null && SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_SpriteHandler);
			}
			this.m_SpriteHandler = null;
			this.m_IsDoneApply = false;
		}

		// Token: 0x0400334C RID: 13132
		protected RectTransform m_RectTransform;

		// Token: 0x0400334D RID: 13133
		protected Image m_Image;

		// Token: 0x0400334E RID: 13134
		protected SpriteHandler m_SpriteHandler;

		// Token: 0x0400334F RID: 13135
		protected bool m_IsDoneApply;
	}
}
