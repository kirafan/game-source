﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008C2 RID: 2242
	public class UIGroupController : MonoBehaviour
	{
		// Token: 0x06002E88 RID: 11912 RVA: 0x000F40A2 File Offset: 0x000F24A2
		private void Prepare()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_UIGroups = this.m_RectTransform.GetComponentsInChildren<UIGroup>();
			this.m_UIGroupControllers = this.m_RectTransform.GetComponentsInChildren<UIGroupController>();
			this.m_IsDonePrepare = true;
		}

		// Token: 0x06002E89 RID: 11913 RVA: 0x000F40DC File Offset: 0x000F24DC
		public void SetEnableInput(bool flg)
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			this.m_IsEnableInputSelf = flg;
			if (this.m_UIGroupControllers != null)
			{
				for (int i = 0; i < this.m_UIGroupControllers.Length; i++)
				{
					if (i != 0)
					{
						this.m_UIGroupControllers[i].SetEnableInputParent(this.IsEnableInput());
					}
				}
			}
			if (this.m_UIGroups != null)
			{
				for (int j = 0; j < this.m_UIGroups.Length; j++)
				{
					this.m_UIGroups[j].SetEnableInputParent(flg);
				}
			}
		}

		// Token: 0x06002E8A RID: 11914 RVA: 0x000F4178 File Offset: 0x000F2578
		public void SetEnableInputParent(bool flg)
		{
			if (!this.m_IsDonePrepare)
			{
				this.Prepare();
			}
			this.m_IsEnableInputParent = flg;
			if (this.m_UIGroupControllers != null)
			{
				for (int i = 0; i < this.m_UIGroupControllers.Length; i++)
				{
					if (i != 0)
					{
						this.m_UIGroupControllers[i].SetEnableInputParent(this.IsEnableInput());
					}
				}
			}
			if (this.m_UIGroups != null)
			{
				for (int j = 0; j < this.m_UIGroups.Length; j++)
				{
					this.m_UIGroups[j].SetEnableInputParent(flg);
				}
			}
		}

		// Token: 0x06002E8B RID: 11915 RVA: 0x000F4211 File Offset: 0x000F2611
		public bool IsEnableInput()
		{
			return this.m_IsEnableInputSelf && this.m_IsEnableInputParent;
		}

		// Token: 0x040035B0 RID: 13744
		private UIGroup[] m_UIGroups;

		// Token: 0x040035B1 RID: 13745
		private UIGroupController[] m_UIGroupControllers;

		// Token: 0x040035B2 RID: 13746
		private bool m_IsEnableInputSelf = true;

		// Token: 0x040035B3 RID: 13747
		private bool m_IsEnableInputParent = true;

		// Token: 0x040035B4 RID: 13748
		private bool m_IsDonePrepare;

		// Token: 0x040035B5 RID: 13749
		private RectTransform m_RectTransform;
	}
}
