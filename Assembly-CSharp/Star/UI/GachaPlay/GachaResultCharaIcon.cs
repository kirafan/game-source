﻿using System;
using System.Collections.Generic;
using Star.UI.Edit;
using Star.UI.Global;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000996 RID: 2454
	public class GachaResultCharaIcon : MonoBehaviour
	{
		// Token: 0x17000325 RID: 805
		// (get) Token: 0x060032D8 RID: 13016 RVA: 0x0010260D File Offset: 0x00100A0D
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x060032D9 RID: 13017 RVA: 0x00102632 File Offset: 0x00100A32
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x060032DA RID: 13018 RVA: 0x00102657 File Offset: 0x00100A57
		public CanvasGroup CanvasGroup
		{
			get
			{
				if (this.m_CanvasGroup == null)
				{
					this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
				}
				return this.m_CanvasGroup;
			}
		}

		// Token: 0x060032DB RID: 13019 RVA: 0x0010267C File Offset: 0x00100A7C
		public void Load(int charaID)
		{
			Debug.Log("GachaResultCharaIcon.Load() = " + charaID);
			this.m_CharaId = charaID;
			this.m_CharaIconWithFrameCloner.GetInst<CharaIconWithFrame>().ApplyCharaID(charaID);
			this.m_CharaIconWithFrameCloner.GetInst<CharaIconWithFrame>().SetNew(true);
		}

		// Token: 0x060032DC RID: 13020 RVA: 0x001026BC File Offset: 0x00100ABC
		public void SetItemID(int itemID)
		{
			if (itemID != -1)
			{
				this.m_ItemObj.SetActive(true);
				this.m_ItemIcon.Apply(itemID);
			}
			else
			{
				this.m_ItemObj.SetActive(false);
				this.m_ItemIcon.Destroy();
			}
		}

		// Token: 0x060032DD RID: 13021 RVA: 0x001026F9 File Offset: 0x00100AF9
		public void PlayIn()
		{
			this.GameObject.SetActive(true);
			this.m_Animation.Play();
		}

		// Token: 0x060032DE RID: 13022 RVA: 0x00102713 File Offset: 0x00100B13
		public void Destroy()
		{
			this.m_CharaIconWithFrameCloner.GetInst<CharaIconWithFrame>().Destroy();
			this.m_ItemIcon.Destroy();
		}

		// Token: 0x060032DF RID: 13023 RVA: 0x00102730 File Offset: 0x00100B30
		public void OnClickButton()
		{
			UserCharacterData userCharacterData = null;
			List<UserCharacterData> userCharaDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas;
			for (int i = 0; i < userCharaDatas.Count; i++)
			{
				if (userCharaDatas[i].Param.CharaID == this.m_CharaId)
				{
					userCharacterData = userCharaDatas[i];
				}
			}
			if (userCharacterData == null)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonIconType(GlobalSceneInfoWindow.eBackButtonIconType.Back);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonOnly(true);
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharacterData, null), CharaDetailUI.eMode.GachaResult, 0);
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.OnClose += this.OnCloseCharaDetail;
		}

		// Token: 0x060032E0 RID: 13024 RVA: 0x001027EE File Offset: 0x00100BEE
		public void OnCloseCharaDetail()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonOnly(false);
		}

		// Token: 0x040038E0 RID: 14560
		[SerializeField]
		private PrefabCloner m_CharaIconWithFrameCloner;

		// Token: 0x040038E1 RID: 14561
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x040038E2 RID: 14562
		[SerializeField]
		private GameObject m_ItemObj;

		// Token: 0x040038E3 RID: 14563
		[SerializeField]
		private ItemIcon m_ItemIcon;

		// Token: 0x040038E4 RID: 14564
		private GameObject m_GameObject;

		// Token: 0x040038E5 RID: 14565
		private RectTransform m_RectTransform;

		// Token: 0x040038E6 RID: 14566
		private CanvasGroup m_CanvasGroup;

		// Token: 0x040038E7 RID: 14567
		private int m_CharaId;
	}
}
