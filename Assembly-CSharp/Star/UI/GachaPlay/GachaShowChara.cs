﻿using System;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000997 RID: 2455
	public class GachaShowChara : UIGroup
	{
		// Token: 0x060032E2 RID: 13026 RVA: 0x0010281E File Offset: 0x00100C1E
		public void LoadCharaImage(int charaID)
		{
			this.m_CharaID = charaID;
			this.m_CharaIllust.Apply(this.m_CharaID, CharaIllust.eCharaIllustType.Card);
		}

		// Token: 0x060032E3 RID: 13027 RVA: 0x00102839 File Offset: 0x00100C39
		public bool IsLoading()
		{
			return !this.m_CharaIllust.IsDoneLoad();
		}

		// Token: 0x060032E4 RID: 13028 RVA: 0x00102849 File Offset: 0x00100C49
		public void PlayAnim(int idx)
		{
			this.m_Animation.Play("GachaCharaIn" + idx);
		}

		// Token: 0x060032E5 RID: 13029 RVA: 0x00102867 File Offset: 0x00100C67
		public void Destroy()
		{
			this.m_CharaIllust.Destroy();
		}

		// Token: 0x060032E6 RID: 13030 RVA: 0x00102874 File Offset: 0x00100C74
		public override void OnFinishPlayOut()
		{
			this.m_CharaIllust.Destroy();
			base.OnFinishPlayOut();
		}

		// Token: 0x040038E8 RID: 14568
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x040038E9 RID: 14569
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x040038EA RID: 14570
		private int m_CharaID = -1;
	}
}
