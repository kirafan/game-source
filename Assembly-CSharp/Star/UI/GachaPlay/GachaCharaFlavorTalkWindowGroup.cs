﻿using System;
using Star.ADV;
using Star.UI.ADV;

namespace Star.UI.GachaPlay
{
	// Token: 0x0200098D RID: 2445
	public class GachaCharaFlavorTalkWindowGroup : UIGroup
	{
		// Token: 0x0600327F RID: 12927 RVA: 0x00101239 File Offset: 0x000FF639
		public void SetCharaID(int charaID)
		{
			this.m_CharaId = charaID;
		}

		// Token: 0x06003280 RID: 12928 RVA: 0x00101244 File Offset: 0x000FF644
		public override void Open()
		{
			base.Open();
			if (this.m_TalkWindow == null)
			{
				this.m_TalkWindow = this.m_CustomWindow.GetComponent<ADVTalkCustomWindow>();
				this.m_TalkWindow.Setup();
			}
			this.m_Parser.Setup(null, this.m_TalkWindow);
			this.m_TalkWindow.SetupParser(this.m_Parser);
			this.m_TalkWindow.SetAutoPage(true);
			this.m_TalkWindow.SetIsActiveTapToNextPage(false);
			this.m_TalkWindow.SetIsActiveTapToProgressSkip(false);
			this.m_TalkWindow.SetAutoWait(0f);
			this.m_TalkWindow.SetHeightVariable(false);
			this.m_TalkWindow.SetTapToStartOverRowProgress(false);
			this.m_TalkWindow.SetTalker(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.m_CharaId).m_Name);
			this.m_TalkWindow.SetText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaFlavorTextDB.GetFlavorText(this.m_CharaId), ADVTalkCustomWindow.eTextState.Start);
		}

		// Token: 0x06003281 RID: 12929 RVA: 0x00101346 File Offset: 0x000FF746
		public void Destroy()
		{
		}

		// Token: 0x06003282 RID: 12930 RVA: 0x00101348 File Offset: 0x000FF748
		public void GoNext()
		{
			if (this.m_TalkWindow != null)
			{
				this.m_TalkWindow.OnClickScreen();
			}
		}

		// Token: 0x06003283 RID: 12931 RVA: 0x00101366 File Offset: 0x000FF766
		public ADVTalkCustomWindow.eTextState GetTalkWindowTextState()
		{
			return this.m_TalkWindow.GetTextState();
		}

		// Token: 0x04003889 RID: 14473
		private ADVTalkCustomWindow m_TalkWindow;

		// Token: 0x0400388A RID: 14474
		private ADVParser m_Parser = new ADVParser();

		// Token: 0x0400388B RID: 14475
		private int m_CharaId;
	}
}
