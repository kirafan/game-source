﻿using System;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000998 RID: 2456
	public class GachaWaitTapNpc : MonoBehaviour
	{
		// Token: 0x060032E8 RID: 13032 RVA: 0x0010289A File Offset: 0x00100C9A
		public void LoadNpc()
		{
			this.m_NpcIllust.Apply(NpcIllust.eNpc.Summon);
		}

		// Token: 0x060032E9 RID: 13033 RVA: 0x001028A8 File Offset: 0x00100CA8
		private void Update()
		{
			if (this.m_IsEnableAnim && !this.m_Anim.isPlaying)
			{
				this.m_Wait -= Time.deltaTime;
				if (this.m_Wait <= 0f)
				{
					this.m_Anim.Play();
					this.m_Wait = UnityEngine.Random.Range(2f, 2f);
				}
			}
		}

		// Token: 0x060032EA RID: 13034 RVA: 0x00102913 File Offset: 0x00100D13
		public void SetEnableAnim(bool flg)
		{
			this.m_IsEnableAnim = flg;
			if (!flg)
			{
				this.m_Anim.Stop();
				this.m_NpcIllust.GetComponent<RectTransform>().localPosition = Vector3.zero;
			}
		}

		// Token: 0x040038EB RID: 14571
		private const float MAX_WAIT = 2f;

		// Token: 0x040038EC RID: 14572
		private const float MIN_WAIT = 2f;

		// Token: 0x040038ED RID: 14573
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x040038EE RID: 14574
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x040038EF RID: 14575
		private bool m_IsEnableAnim;

		// Token: 0x040038F0 RID: 14576
		private float m_Wait = 2f;
	}
}
