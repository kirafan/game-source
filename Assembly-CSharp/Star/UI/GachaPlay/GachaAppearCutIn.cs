﻿using System;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000989 RID: 2441
	public class GachaAppearCutIn : MonoBehaviour
	{
		// Token: 0x0600326C RID: 12908 RVA: 0x00100C94 File Offset: 0x000FF094
		private void Update()
		{
			switch (this.m_Step)
			{
			case GachaAppearCutIn.eStep.Preparing:
				if (this.m_CharaIllust.IsDoneLoad())
				{
					this.m_IsDonePrepare = true;
					this.m_Step = GachaAppearCutIn.eStep.None;
				}
				break;
			case GachaAppearCutIn.eStep.Playing_0:
				this.StartIllustMove();
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(Color.white, this.m_FadeInSec, null);
				this.m_t = 0f;
				this.m_Step = GachaAppearCutIn.eStep.Playing_1;
				break;
			case GachaAppearCutIn.eStep.Playing_1:
				this.UpdateIllustMove();
				this.m_t += Time.deltaTime;
				if (this.m_t >= this.m_DisplayCutSec)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, this.m_FadeOutSec, null);
					this.m_t = 0f;
					this.m_Step = GachaAppearCutIn.eStep.Playing_2;
				}
				break;
			case GachaAppearCutIn.eStep.Playing_2:
				this.UpdateIllustMove();
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_t += Time.deltaTime;
					if (this.m_t >= this.m_FadeWaitSec)
					{
						this.m_CutInIndex++;
						if (this.m_CutInIndex >= 2)
						{
							this.m_CutInIndex = 0;
							this.m_Step = GachaAppearCutIn.eStep.None;
						}
						else
						{
							this.m_t = 0f;
							this.m_Step = GachaAppearCutIn.eStep.Playing_0;
						}
					}
				}
				break;
			}
		}

		// Token: 0x0600326D RID: 12909 RVA: 0x00100E08 File Offset: 0x000FF208
		private void StartIllustMove()
		{
			RectTransform rectTransform = this.m_CharaIllust.RectTransform;
			float y = Mathf.Clamp(this.m_IllustPosOffsets[this.m_CutInIndex], 0f, 1f);
			rectTransform.pivot = new Vector2(rectTransform.pivot.x, y);
		}

		// Token: 0x0600326E RID: 12910 RVA: 0x00100E58 File Offset: 0x000FF258
		private void UpdateIllustMove()
		{
			RectTransform rectTransform = this.m_CharaIllust.RectTransform;
			float y = Mathf.Clamp(rectTransform.pivot.y + this.m_Speed * Time.deltaTime, 0f, 1f);
			rectTransform.pivot = new Vector2(rectTransform.pivot.x, y);
		}

		// Token: 0x0600326F RID: 12911 RVA: 0x00100EB8 File Offset: 0x000FF2B8
		public void Prepare(int charaID, float[] illustPosOffsets)
		{
			this.m_CutInIndex = 0;
			this.m_IllustPosOffsets = illustPosOffsets;
			if (this.m_IllustPosOffsets == null || illustPosOffsets.Length == 0)
			{
				this.m_IllustPosOffsets = this.DEFAULT_ILLUST_POS_OFFSETS;
			}
			base.gameObject.SetActive(true);
			this.m_CharaIllust.Apply(charaID, CharaIllust.eCharaIllustType.Full);
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetColor(Color.white);
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
			this.m_IsDonePrepare = false;
			this.m_Step = GachaAppearCutIn.eStep.Preparing;
		}

		// Token: 0x06003270 RID: 12912 RVA: 0x00100F41 File Offset: 0x000FF341
		public bool IsDonePrepare()
		{
			return this.m_IsDonePrepare;
		}

		// Token: 0x06003271 RID: 12913 RVA: 0x00100F49 File Offset: 0x000FF349
		public void Destroy()
		{
			this.m_IsDonePrepare = false;
			this.m_CharaIllust.Destroy();
			base.gameObject.SetActive(false);
		}

		// Token: 0x06003272 RID: 12914 RVA: 0x00100F69 File Offset: 0x000FF369
		public bool Play()
		{
			if (!this.m_IsDonePrepare)
			{
				return false;
			}
			this.m_Step = GachaAppearCutIn.eStep.Playing_0;
			return true;
		}

		// Token: 0x06003273 RID: 12915 RVA: 0x00100F80 File Offset: 0x000FF380
		public bool IsPlaying()
		{
			return this.m_Step >= GachaAppearCutIn.eStep.Playing_0 && this.m_Step <= GachaAppearCutIn.eStep.Playing_2;
		}

		// Token: 0x04003865 RID: 14437
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x04003866 RID: 14438
		[SerializeField]
		private float m_Speed = 0.01f;

		// Token: 0x04003867 RID: 14439
		[SerializeField]
		private float m_DisplayCutSec = 3f;

		// Token: 0x04003868 RID: 14440
		[SerializeField]
		private float m_FadeOutSec = 1f;

		// Token: 0x04003869 RID: 14441
		[SerializeField]
		private float m_FadeWaitSec;

		// Token: 0x0400386A RID: 14442
		[SerializeField]
		private float m_FadeInSec = 1f;

		// Token: 0x0400386B RID: 14443
		private const int CUT_NUM = 2;

		// Token: 0x0400386C RID: 14444
		private float[] DEFAULT_ILLUST_POS_OFFSETS = new float[]
		{
			0.1f,
			0.7f
		};

		// Token: 0x0400386D RID: 14445
		private GachaAppearCutIn.eStep m_Step = GachaAppearCutIn.eStep.None;

		// Token: 0x0400386E RID: 14446
		private bool m_IsDonePrepare;

		// Token: 0x0400386F RID: 14447
		private float m_t;

		// Token: 0x04003870 RID: 14448
		private float[] m_IllustPosOffsets;

		// Token: 0x04003871 RID: 14449
		private int m_CutInIndex;

		// Token: 0x0200098A RID: 2442
		private enum eStep
		{
			// Token: 0x04003873 RID: 14451
			None = -1,
			// Token: 0x04003874 RID: 14452
			Preparing,
			// Token: 0x04003875 RID: 14453
			Playing_0,
			// Token: 0x04003876 RID: 14454
			Playing_1,
			// Token: 0x04003877 RID: 14455
			Playing_2
		}
	}
}
