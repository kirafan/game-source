﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000992 RID: 2450
	public class GachaPlayUI : MenuUIBase
	{
		// Token: 0x14000083 RID: 131
		// (add) Token: 0x06003291 RID: 12945 RVA: 0x0010193C File Offset: 0x000FFD3C
		// (remove) Token: 0x06003292 RID: 12946 RVA: 0x00101974 File Offset: 0x000FFD74
		public event Action OnClick;

		// Token: 0x14000084 RID: 132
		// (add) Token: 0x06003293 RID: 12947 RVA: 0x001019AC File Offset: 0x000FFDAC
		// (remove) Token: 0x06003294 RID: 12948 RVA: 0x001019E4 File Offset: 0x000FFDE4
		public event Action OnCloseWaitTouch;

		// Token: 0x14000085 RID: 133
		// (add) Token: 0x06003295 RID: 12949 RVA: 0x00101A1C File Offset: 0x000FFE1C
		// (remove) Token: 0x06003296 RID: 12950 RVA: 0x00101A54 File Offset: 0x000FFE54
		public event Action OnCloseCutIn;

		// Token: 0x14000086 RID: 134
		// (add) Token: 0x06003297 RID: 12951 RVA: 0x00101A8C File Offset: 0x000FFE8C
		// (remove) Token: 0x06003298 RID: 12952 RVA: 0x00101AC4 File Offset: 0x000FFEC4
		public event Action OnCloseShowChara;

		// Token: 0x14000087 RID: 135
		// (add) Token: 0x06003299 RID: 12953 RVA: 0x00101AFC File Offset: 0x000FFEFC
		// (remove) Token: 0x0600329A RID: 12954 RVA: 0x00101B34 File Offset: 0x000FFF34
		public event Action OnCloseCharaFlavors;

		// Token: 0x14000088 RID: 136
		// (add) Token: 0x0600329B RID: 12955 RVA: 0x00101B6C File Offset: 0x000FFF6C
		// (remove) Token: 0x0600329C RID: 12956 RVA: 0x00101BA4 File Offset: 0x000FFFA4
		public event Action OnClickCloseResult;

		// Token: 0x14000089 RID: 137
		// (add) Token: 0x0600329D RID: 12957 RVA: 0x00101BDC File Offset: 0x000FFFDC
		// (remove) Token: 0x0600329E RID: 12958 RVA: 0x00101C14 File Offset: 0x00100014
		public event Action OnCloseResult;

		// Token: 0x0600329F RID: 12959 RVA: 0x00101C4A File Offset: 0x0010004A
		private void Start()
		{
			this.SetOnClickAllSkipButtonCallback(null);
		}

		// Token: 0x060032A0 RID: 12960 RVA: 0x00101C53 File Offset: 0x00100053
		private void Update()
		{
			UIUtility.UpdateAndroidBackKey(this.m_AllSkipButton, null);
		}

		// Token: 0x060032A1 RID: 12961 RVA: 0x00101C64 File Offset: 0x00100064
		public void Setup()
		{
			this.m_NpcIllust.Apply(NpcIllust.eNpc.Summon);
			this.m_WaitTapNpc.LoadNpc();
			this.m_WaitTouchGroup.OnClose += this.OnCloseWaitTouchCallBack;
			this.m_GachaCutIn.OnClose += this.OnCloseCutInCallBack;
			this.m_GachaShowChara.OnEnd += this.OnCloseShowCharaCallBack;
			this.m_CharaFlavor.OnEnd += this.OnCloseCharaFlavorsCallBack;
			this.m_GachaResult.Setup();
			this.m_GachaResult.OnClickClose += this.OnClickCloseResultCallBack;
			this.m_GachaResult.OnEnd += this.OnCloseResultCallBack;
			this.m_StateController.Setup(7, 1, false);
			this.m_StateController.AddTransitEach(1, 2, true);
			this.m_StateController.AddTransitEach(1, 3, true);
			this.m_StateController.AddTransitEach(1, 4, true);
			this.m_StateController.AddTransitEach(1, 5, true);
			this.m_StateController.AddTransitEach(1, 6, true);
		}

		// Token: 0x060032A2 RID: 12962 RVA: 0x00101D71 File Offset: 0x00100171
		private bool ChangeState(GachaPlayUI.eUIState state)
		{
			if (this.m_StateController.ChangeState((int)state))
			{
				this.m_State = (int)state;
				return true;
			}
			return false;
		}

		// Token: 0x060032A3 RID: 12963 RVA: 0x00101D8E File Offset: 0x0010018E
		public override void PlayIn()
		{
		}

		// Token: 0x060032A4 RID: 12964 RVA: 0x00101D90 File Offset: 0x00100190
		public override void PlayOut()
		{
		}

		// Token: 0x060032A5 RID: 12965 RVA: 0x00101D92 File Offset: 0x00100192
		public override bool IsMenuEnd()
		{
			return this.m_StateController.NowState == 0;
		}

		// Token: 0x060032A6 RID: 12966 RVA: 0x00101DA2 File Offset: 0x001001A2
		public override void Destroy()
		{
			base.Destroy();
			this.m_GachaShowChara.Destroy();
			this.m_CharaFlavor.Destroy();
			this.m_GachaAppearCutIn.Destroy();
		}

		// Token: 0x060032A7 RID: 12967 RVA: 0x00101DCB File Offset: 0x001001CB
		public void LoadCharaIllust(int charaID)
		{
			this.m_GachaShowChara.LoadCharaImage(charaID);
		}

		// Token: 0x060032A8 RID: 12968 RVA: 0x00101DDC File Offset: 0x001001DC
		public void SetOnClickAllSkipButtonCallback(Action callback)
		{
			this.m_AllSkipButton.gameObject.SetActive(callback != null);
			this.m_AllSkipButton.m_OnClick.RemoveAllListeners();
			if (callback != null)
			{
				this.m_AllSkipButton.m_OnClick.AddListener(new UnityAction(callback.Invoke));
			}
		}

		// Token: 0x060032A9 RID: 12969 RVA: 0x00101E32 File Offset: 0x00100232
		public bool IsLoadingCharaIllust()
		{
			return this.m_GachaShowChara.IsLoading();
		}

		// Token: 0x060032AA RID: 12970 RVA: 0x00101E3F File Offset: 0x0010023F
		public bool IsIdle()
		{
			return this.m_StateController.NowState == 1;
		}

		// Token: 0x060032AB RID: 12971 RVA: 0x00101E50 File Offset: 0x00100250
		public void OnClickCallBack()
		{
			if (this.m_GachaItemLabel.IsPlaying())
			{
				this.m_GachaItemLabel.Stop();
			}
			this.OnClick.Call();
			if (this.m_CharaFlavor.isActiveAndEnabled)
			{
				this.m_CharaFlavor.GoNext();
			}
		}

		// Token: 0x060032AC RID: 12972 RVA: 0x00101E9E File Offset: 0x0010029E
		public void AbortOnAllSkip()
		{
			this.m_GachaCutIn.Hide();
			this.m_GachaShowChara.Hide();
			this.m_GachaShowItem.Hide();
		}

		// Token: 0x060032AD RID: 12973 RVA: 0x00101EC1 File Offset: 0x001002C1
		public void PlayInNpc()
		{
			this.m_NpcIllust.GetComponent<AnimUIPlayer>().PlayIn();
		}

		// Token: 0x060032AE RID: 12974 RVA: 0x00101ED3 File Offset: 0x001002D3
		public void PlayOutNpc()
		{
			this.m_NpcIllust.GetComponent<AnimUIPlayer>().PlayOut();
		}

		// Token: 0x060032AF RID: 12975 RVA: 0x00101EE5 File Offset: 0x001002E5
		public UIGroup GetWaitTouchGroup()
		{
			return this.m_WaitTouchGroup;
		}

		// Token: 0x060032B0 RID: 12976 RVA: 0x00101EED File Offset: 0x001002ED
		public void OpenWaitTouchGroup()
		{
			if (this.ChangeState(GachaPlayUI.eUIState.WaitTouch))
			{
				this.m_WaitTouchGroup.Open();
				this.m_WaitTapNpc.SetEnableAnim(true);
			}
		}

		// Token: 0x060032B1 RID: 12977 RVA: 0x00101F12 File Offset: 0x00100312
		public void CloseWaitTouchGroup()
		{
			this.m_WaitTapNpc.SetEnableAnim(false);
			this.m_WaitTouchGroup.Close();
		}

		// Token: 0x060032B2 RID: 12978 RVA: 0x00101F2B File Offset: 0x0010032B
		private void OnCloseWaitTouchCallBack()
		{
			this.ChangeState(GachaPlayUI.eUIState.Idle);
			this.OnCloseWaitTouch.Call();
		}

		// Token: 0x060032B3 RID: 12979 RVA: 0x00101F40 File Offset: 0x00100340
		public GachaCutIn GetCutInGroup()
		{
			return this.m_GachaCutIn;
		}

		// Token: 0x060032B4 RID: 12980 RVA: 0x00101F48 File Offset: 0x00100348
		public void OpenCutIn(string text, string cueSheet, string cueID)
		{
			if (this.ChangeState(GachaPlayUI.eUIState.CutIn))
			{
				this.m_GachaCutIn.Open(text, cueSheet, cueID);
			}
		}

		// Token: 0x060032B5 RID: 12981 RVA: 0x00101F64 File Offset: 0x00100364
		public void OpenCutIn(string message)
		{
			if (this.ChangeState(GachaPlayUI.eUIState.CutIn))
			{
				this.m_GachaCutIn.OpenMessageOnly(message, true);
				this.m_GachaCutIn.SetTalker(-1);
			}
		}

		// Token: 0x060032B6 RID: 12982 RVA: 0x00101F8B File Offset: 0x0010038B
		public void CloseCutIn()
		{
			this.m_GachaCutIn.Close();
		}

		// Token: 0x060032B7 RID: 12983 RVA: 0x00101F98 File Offset: 0x00100398
		private void OnCloseCutInCallBack()
		{
			this.ChangeState(GachaPlayUI.eUIState.Idle);
			this.OnCloseCutIn.Call();
		}

		// Token: 0x060032B8 RID: 12984 RVA: 0x00101FAD File Offset: 0x001003AD
		public void OpenShowChara(bool highAnim)
		{
			this.m_GachaShowChara.Open();
			this.m_GachaShowChara.PlayAnim((!highAnim) ? 0 : 1);
		}

		// Token: 0x060032B9 RID: 12985 RVA: 0x00101FD2 File Offset: 0x001003D2
		public void OpenShowItem(int itemID)
		{
			this.m_GachaShowItem.Open();
			this.m_GachaItemLabel.Play(itemID);
		}

		// Token: 0x060032BA RID: 12986 RVA: 0x00101FEB File Offset: 0x001003EB
		public bool IsCompleteOpenShowChara()
		{
			return this.m_GachaShowChara.IsDonePlayIn;
		}

		// Token: 0x060032BB RID: 12987 RVA: 0x00101FF8 File Offset: 0x001003F8
		public bool IsCompleteOpenShowItem()
		{
			return !this.m_GachaItemLabel.IsPlaying();
		}

		// Token: 0x060032BC RID: 12988 RVA: 0x00102008 File Offset: 0x00100408
		public void CloseShowItem()
		{
			if (this.m_GachaShowItem.IsOpenOrIn())
			{
				this.m_GachaShowItem.Close();
			}
		}

		// Token: 0x060032BD RID: 12989 RVA: 0x00102025 File Offset: 0x00100425
		public void CloseShowChara()
		{
			this.m_GachaShowChara.Close();
		}

		// Token: 0x060032BE RID: 12990 RVA: 0x00102032 File Offset: 0x00100432
		public bool IsCompleteCloseShowChara()
		{
			return this.m_GachaShowChara.IsDonePlayOut;
		}

		// Token: 0x060032BF RID: 12991 RVA: 0x0010203F File Offset: 0x0010043F
		public bool IsCompleteCloseShowItem()
		{
			return this.m_GachaShowItem.IsDonePlayOut;
		}

		// Token: 0x060032C0 RID: 12992 RVA: 0x0010204C File Offset: 0x0010044C
		private void OnCloseShowCharaCallBack()
		{
			this.ChangeState(GachaPlayUI.eUIState.Idle);
			this.OnCloseShowChara.Call();
		}

		// Token: 0x060032C1 RID: 12993 RVA: 0x00102061 File Offset: 0x00100461
		public GachaAppearCutIn GetAppearCutIn()
		{
			return this.m_GachaAppearCutIn;
		}

		// Token: 0x060032C2 RID: 12994 RVA: 0x00102069 File Offset: 0x00100469
		private void OnClickCloseResultCallBack()
		{
			this.OnClickCloseResult.Call();
		}

		// Token: 0x060032C3 RID: 12995 RVA: 0x00102076 File Offset: 0x00100476
		private void OnCloseResultCallBack()
		{
			this.ChangeState(GachaPlayUI.eUIState.Idle);
			this.OnCloseResult.Call();
		}

		// Token: 0x060032C4 RID: 12996 RVA: 0x0010208B File Offset: 0x0010048B
		public void OnClickReplayYesCallBack()
		{
			this.OnClickReplayYes.Call();
			this.m_ReplayWindow.Close();
		}

		// Token: 0x060032C5 RID: 12997 RVA: 0x001020A3 File Offset: 0x001004A3
		public void OnClickReplayNoCallBack()
		{
			this.OnClickReplayNo.Call();
			this.m_ReplayWindow.Close();
		}

		// Token: 0x060032C6 RID: 12998 RVA: 0x001020BB File Offset: 0x001004BB
		private void OnCloseCharaFlavorsCallBack()
		{
			this.ChangeState(GachaPlayUI.eUIState.Idle);
			this.OnCloseCharaFlavors.Call();
		}

		// Token: 0x060032C7 RID: 12999 RVA: 0x001020D0 File Offset: 0x001004D0
		public void OpenReplayWindow(Action OnClickYes, Action OnClickNo)
		{
			this.OnClickReplayYes = OnClickYes;
			this.OnClickReplayNo = OnClickNo;
			this.m_ReplayWindow.Open();
		}

		// Token: 0x060032C8 RID: 13000 RVA: 0x001020EB File Offset: 0x001004EB
		public void OpenCharaFlavors(int[] charaIDs)
		{
			this.m_CharaFlavor.SetCharaIDs(charaIDs);
			this.m_CharaFlavor.Open();
		}

		// Token: 0x060032C9 RID: 13001 RVA: 0x00102104 File Offset: 0x00100504
		public void OpenResult(List<Gacha.Result> gachaResults)
		{
			this.OnCloseResult = null;
			this.OnClickCloseResult = null;
			this.m_GachaResult.Open(gachaResults);
		}

		// Token: 0x060032CA RID: 13002 RVA: 0x00102120 File Offset: 0x00100520
		public void CloseResult()
		{
			this.m_GachaResult.Close();
		}

		// Token: 0x040038AE RID: 14510
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x040038AF RID: 14511
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040038B0 RID: 14512
		[SerializeField]
		private UIGroup m_WaitTouchGroup;

		// Token: 0x040038B1 RID: 14513
		[SerializeField]
		private GachaCutIn m_GachaCutIn;

		// Token: 0x040038B2 RID: 14514
		[SerializeField]
		private GachaShowChara m_GachaShowChara;

		// Token: 0x040038B3 RID: 14515
		[SerializeField]
		private GachaAppearCutIn m_GachaAppearCutIn;

		// Token: 0x040038B4 RID: 14516
		[SerializeField]
		private UIGroup m_GachaShowItem;

		// Token: 0x040038B5 RID: 14517
		[SerializeField]
		private GachaItemLabel m_GachaItemLabel;

		// Token: 0x040038B6 RID: 14518
		[SerializeField]
		private GachaCharaFlavor m_CharaFlavor;

		// Token: 0x040038B7 RID: 14519
		[SerializeField]
		private GachaResult m_GachaResult;

		// Token: 0x040038B8 RID: 14520
		[SerializeField]
		private NpcIllust m_NpcIllust;

		// Token: 0x040038B9 RID: 14521
		[SerializeField]
		private GachaWaitTapNpc m_WaitTapNpc;

		// Token: 0x040038BA RID: 14522
		[SerializeField]
		private UIGroup m_ReplayWindow;

		// Token: 0x040038BB RID: 14523
		[SerializeField]
		private CustomButton m_AllSkipButton;

		// Token: 0x040038C3 RID: 14531
		private Action OnClickReplayYes;

		// Token: 0x040038C4 RID: 14532
		private Action OnClickReplayNo;

		// Token: 0x02000993 RID: 2451
		public enum eUIState
		{
			// Token: 0x040038C6 RID: 14534
			End,
			// Token: 0x040038C7 RID: 14535
			Idle,
			// Token: 0x040038C8 RID: 14536
			WaitTouch,
			// Token: 0x040038C9 RID: 14537
			CutIn,
			// Token: 0x040038CA RID: 14538
			ShowChara,
			// Token: 0x040038CB RID: 14539
			CharaFlavor,
			// Token: 0x040038CC RID: 14540
			Result,
			// Token: 0x040038CD RID: 14541
			Num
		}
	}
}
