﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x02000994 RID: 2452
	public class GachaResult : UIGroup
	{
		// Token: 0x1400008A RID: 138
		// (add) Token: 0x060032CC RID: 13004 RVA: 0x00102144 File Offset: 0x00100544
		// (remove) Token: 0x060032CD RID: 13005 RVA: 0x0010217C File Offset: 0x0010057C
		public event Action OnClickClose;

		// Token: 0x060032CE RID: 13006 RVA: 0x001021B4 File Offset: 0x001005B4
		public void Setup()
		{
			for (int i = 0; i < this.m_CharaIcons.Length; i++)
			{
				this.m_CharaIcons[i] = UnityEngine.Object.Instantiate<GachaResultCharaIcon>(this.m_IconPrefab, this.m_CharaIconParent);
			}
		}

		// Token: 0x060032CF RID: 13007 RVA: 0x001021F4 File Offset: 0x001005F4
		public void Open(List<Gacha.Result> gachaResults)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			int num = -1;
			for (int i = 0; i < gachaResults.Count; i++)
			{
				int rare = dbMng.CharaListDB.GetParam(gachaResults[i].m_CharaID).m_Rare;
				if (num < rare)
				{
					num = rare;
				}
			}
			eText_CommonDB id = eText_CommonDB.GachaPlayResultWindowTalkMessage;
			if (3 < num)
			{
				id = eText_CommonDB.GachaPlayResultWindowTalkMessageRare;
			}
			this.m_CutIn.OpenMessageOnly(dbMng.GetTextCommon(id), false);
			base.Open();
			this.m_GachaResults = gachaResults;
			this.m_Step = GachaResult.eStep.Prepare;
		}

		// Token: 0x060032D0 RID: 13008 RVA: 0x0010228C File Offset: 0x0010068C
		public override void Close()
		{
			base.Close();
			this.m_CutIn.Close();
			this.m_Step = GachaResult.eStep.WaitClose;
		}

		// Token: 0x060032D1 RID: 13009 RVA: 0x001022A8 File Offset: 0x001006A8
		public override void Update()
		{
			switch (this.m_Step)
			{
			case GachaResult.eStep.Prepare:
				for (int i = 0; i < this.m_CharaIcons.Length; i++)
				{
					if (i >= this.m_GachaResults.Count)
					{
						this.m_CharaIcons[i].GameObject.SetActive(false);
					}
					else if (this.m_GachaResults[i].m_CharaID == -1)
					{
						this.m_CharaIcons[i].GameObject.SetActive(false);
					}
					else
					{
						this.m_CharaIcons[i].GameObject.SetActive(true);
						this.m_CharaIcons[i].CanvasGroup.alpha = 0f;
						this.m_CharaIcons[i].Load(this.m_GachaResults[i].m_CharaID);
						this.m_CharaIcons[i].SetItemID(this.m_GachaResults[i].m_ItemID);
					}
				}
				this.MoveResultGroupTitleBar();
				this.m_Step = GachaResult.eStep.WaitOpen;
				break;
			case GachaResult.eStep.WaitOpen:
				if (base.IsDonePlayIn)
				{
					this.PlayResultVoice();
					this.m_ResultCharaPlayInCount = 0f;
					this.m_ResultCharaPlayInIdx = 0;
					this.m_Step = GachaResult.eStep.Main;
				}
				break;
			case GachaResult.eStep.Main:
			{
				this.m_ResultCharaPlayInCount += Time.deltaTime * 16f;
				int resultCharaPlayInIdx = this.m_ResultCharaPlayInIdx;
				this.m_ResultCharaPlayInIdx = (int)this.m_ResultCharaPlayInCount;
				int resultCharaPlayInIdx2 = this.m_ResultCharaPlayInIdx;
				if (resultCharaPlayInIdx2 < this.m_CharaIcons.Length && resultCharaPlayInIdx2 > resultCharaPlayInIdx)
				{
					for (int j = resultCharaPlayInIdx; j <= resultCharaPlayInIdx2; j++)
					{
						if (this.m_CharaIcons[j].isActiveAndEnabled)
						{
							this.m_CharaIcons[j].PlayIn();
						}
					}
				}
				break;
			}
			case GachaResult.eStep.WaitClose:
				if (base.IsDonePlayOut)
				{
					this.m_Step = GachaResult.eStep.End;
				}
				break;
			}
			base.Update();
		}

		// Token: 0x060032D2 RID: 13010 RVA: 0x0010249B File Offset: 0x0010089B
		public void OnClickCancelButtonCallBack()
		{
			this.OnClickClose.Call();
		}

		// Token: 0x060032D3 RID: 13011 RVA: 0x001024A8 File Offset: 0x001008A8
		public override void OnFinishPlayOut()
		{
			for (int i = 0; i < this.m_CharaIcons.Length; i++)
			{
				this.m_CharaIcons[i].Destroy();
			}
			base.OnFinishPlayOut();
		}

		// Token: 0x060032D4 RID: 13012 RVA: 0x001024E4 File Offset: 0x001008E4
		private void PlayResultVoice()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			bool flag = false;
			for (int i = 0; i < this.m_GachaResults.Count; i++)
			{
				if (dbMng.CharaListDB.GetParam(this.m_GachaResults[i].m_CharaID).m_Rare >= 4)
				{
					flag = true;
					break;
				}
			}
			if (flag)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request("Voice_Original_006", "voice_gacha_003", null, null, true);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request("Voice_Original_006", "voice_gacha_004", null, null, true);
			}
		}

		// Token: 0x060032D5 RID: 13013 RVA: 0x00102590 File Offset: 0x00100990
		private void MoveResultGroupTitleBar()
		{
			this.m_CustomWindow.GetComponent<Canvas>().overrideSorting = false;
		}

		// Token: 0x060032D6 RID: 13014 RVA: 0x001025A4 File Offset: 0x001009A4
		private void MoveResultGroupTitleBarPart(RectTransform transform)
		{
			float num = transform.sizeDelta.x / -2f + 16f;
			float num2 = 768f;
			float num3 = 1282f - num2;
			transform.sizeDelta = new Vector2(-num3, transform.sizeDelta.y);
			transform.localPosX(num - num3 / 2f);
		}

		// Token: 0x040038CE RID: 14542
		private const int ICON_MAX = 10;

		// Token: 0x040038CF RID: 14543
		private const float ICON_POP_SPEED = 16f;

		// Token: 0x040038D0 RID: 14544
		private List<Gacha.Result> m_GachaResults;

		// Token: 0x040038D1 RID: 14545
		[SerializeField]
		private GachaResultCharaIcon m_IconPrefab;

		// Token: 0x040038D2 RID: 14546
		[SerializeField]
		private RectTransform m_CharaIconParent;

		// Token: 0x040038D3 RID: 14547
		[SerializeField]
		private GachaCutIn m_CutIn;

		// Token: 0x040038D4 RID: 14548
		private GachaResult.eStep m_Step;

		// Token: 0x040038D5 RID: 14549
		private GachaResultCharaIcon[] m_CharaIcons = new GachaResultCharaIcon[10];

		// Token: 0x040038D6 RID: 14550
		private float m_ResultCharaPlayInCount;

		// Token: 0x040038D7 RID: 14551
		private int m_ResultCharaPlayInIdx;

		// Token: 0x02000995 RID: 2453
		private enum eStep
		{
			// Token: 0x040038DA RID: 14554
			None,
			// Token: 0x040038DB RID: 14555
			Prepare,
			// Token: 0x040038DC RID: 14556
			WaitOpen,
			// Token: 0x040038DD RID: 14557
			Main,
			// Token: 0x040038DE RID: 14558
			WaitClose,
			// Token: 0x040038DF RID: 14559
			End
		}
	}
}
