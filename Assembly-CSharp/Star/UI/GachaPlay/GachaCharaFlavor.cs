﻿using System;
using UnityEngine;

namespace Star.UI.GachaPlay
{
	// Token: 0x0200098B RID: 2443
	public class GachaCharaFlavor : MonoBehaviour
	{
		// Token: 0x14000082 RID: 130
		// (add) Token: 0x06003275 RID: 12917 RVA: 0x00100FA8 File Offset: 0x000FF3A8
		// (remove) Token: 0x06003276 RID: 12918 RVA: 0x00100FE0 File Offset: 0x000FF3E0
		public event Action OnEnd;

		// Token: 0x06003277 RID: 12919 RVA: 0x00101016 File Offset: 0x000FF416
		public void SetCharaIDs(int[] charaIDs)
		{
			this.m_CharaIDs = charaIDs;
		}

		// Token: 0x06003278 RID: 12920 RVA: 0x0010101F File Offset: 0x000FF41F
		public void Open()
		{
			this.m_CurrentCharaIdx = 0;
			this.LoadChara();
			this.m_Step = GachaCharaFlavor.eStep.Prepare;
		}

		// Token: 0x06003279 RID: 12921 RVA: 0x00101038 File Offset: 0x000FF438
		public void Update()
		{
			switch (this.m_Step)
			{
			case GachaCharaFlavor.eStep.Prepare:
				if (this.m_CharaIllust.IsDoneLoad())
				{
					this.m_Step = GachaCharaFlavor.eStep.CharaIn;
				}
				break;
			case GachaCharaFlavor.eStep.CharaIn:
				this.m_CharaIllustAnim.PlayIn();
				this.m_Step = GachaCharaFlavor.eStep.CharaInWait;
				break;
			case GachaCharaFlavor.eStep.CharaInWait:
				if (this.m_CharaIllustAnim.State == 2)
				{
					this.m_RequestVoiceUniqueID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.RequestFlavorText(this.m_CharaIDs[this.m_CurrentCharaIdx], false);
					this.m_TalkWindowGroup.SetCharaID(this.m_CharaIDs[this.m_CurrentCharaIdx]);
					this.m_TalkWindowGroup.Open();
					this.m_Step = GachaCharaFlavor.eStep.Talk;
				}
				break;
			case GachaCharaFlavor.eStep.CharaOut:
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_RequestVoiceUniqueID);
				this.m_CharaIllustAnim.PlayOut();
				this.m_TalkWindowGroup.Close();
				this.m_Step = GachaCharaFlavor.eStep.CharaOutWait;
				break;
			case GachaCharaFlavor.eStep.CharaOutWait:
				if (this.m_CharaIllustAnim.State == 0 && !this.m_TalkWindowGroup.IsOpen())
				{
					if (this.m_CharaIDs.Length - 1 > this.m_CurrentCharaIdx)
					{
						this.m_CurrentCharaIdx++;
						this.LoadChara();
						this.m_Step = GachaCharaFlavor.eStep.Prepare;
					}
					else
					{
						this.m_Step = GachaCharaFlavor.eStep.End;
						this.Destroy();
						this.OnEnd();
					}
				}
				break;
			}
		}

		// Token: 0x0600327A RID: 12922 RVA: 0x001011B5 File Offset: 0x000FF5B5
		public void Destroy()
		{
			this.m_CharaIllust.Destroy();
			if (this.m_TalkWindowGroup != null)
			{
				this.m_TalkWindowGroup.Destroy();
			}
		}

		// Token: 0x0600327B RID: 12923 RVA: 0x001011DE File Offset: 0x000FF5DE
		public void Close()
		{
			this.m_TalkWindowGroup.Close();
		}

		// Token: 0x0600327C RID: 12924 RVA: 0x001011EB File Offset: 0x000FF5EB
		private void LoadChara()
		{
			this.m_CharaIllust.Apply(this.m_CharaIDs[this.m_CurrentCharaIdx], CharaIllust.eCharaIllustType.Bust);
		}

		// Token: 0x0600327D RID: 12925 RVA: 0x00101206 File Offset: 0x000FF606
		public void GoNext()
		{
			this.m_TalkWindowGroup.GoNext();
			if (this.m_Step == GachaCharaFlavor.eStep.Talk)
			{
				this.m_Step = GachaCharaFlavor.eStep.CharaOut;
			}
		}

		// Token: 0x04003878 RID: 14456
		private int[] m_CharaIDs;

		// Token: 0x04003879 RID: 14457
		private int m_CurrentCharaIdx;

		// Token: 0x0400387A RID: 14458
		[SerializeField]
		private CharaIllust m_CharaIllust;

		// Token: 0x0400387B RID: 14459
		[SerializeField]
		private AnimUIPlayer m_CharaIllustAnim;

		// Token: 0x0400387C RID: 14460
		[SerializeField]
		private GachaCharaFlavorTalkWindowGroup m_TalkWindowGroup;

		// Token: 0x0400387E RID: 14462
		private GachaCharaFlavor.eStep m_Step;

		// Token: 0x0400387F RID: 14463
		private int m_RequestVoiceUniqueID;

		// Token: 0x0200098C RID: 2444
		private enum eStep
		{
			// Token: 0x04003881 RID: 14465
			None,
			// Token: 0x04003882 RID: 14466
			Prepare,
			// Token: 0x04003883 RID: 14467
			CharaIn,
			// Token: 0x04003884 RID: 14468
			CharaInWait,
			// Token: 0x04003885 RID: 14469
			Talk,
			// Token: 0x04003886 RID: 14470
			CharaOut,
			// Token: 0x04003887 RID: 14471
			CharaOutWait,
			// Token: 0x04003888 RID: 14472
			End
		}
	}
}
