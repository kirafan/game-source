﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x0200098F RID: 2447
	public class GachaItemLabel : MonoBehaviour
	{
		// Token: 0x0600328C RID: 12940 RVA: 0x00101683 File Offset: 0x000FFA83
		public bool IsPlaying()
		{
			return this.m_Step != GachaItemLabel.eStep.None;
		}

		// Token: 0x0600328D RID: 12941 RVA: 0x00101694 File Offset: 0x000FFA94
		public void Play(int itemID)
		{
			this.m_GameObject = base.gameObject;
			this.m_GameObject.SetActive(true);
			this.m_Animation.gameObject.SetActive(false);
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			SpriteManager spriteMng = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng;
			for (int i = 0; i < 3; i++)
			{
				if (this.m_Handler[i] != null)
				{
					spriteMng.Unload(this.m_Handler[i]);
				}
			}
			for (int j = 0; j < dbMng.GachaItemLabelListDB.m_Params.Length; j++)
			{
				if (dbMng.GachaItemLabelListDB.m_Params[j].m_ItemID == itemID)
				{
					GachaItemLabelListDB_Param gachaItemLabelListDB_Param = dbMng.GachaItemLabelListDB.m_Params[j];
					this.m_Handler[0] = spriteMng.LoadAsyncGachaItemLabelIcon(gachaItemLabelListDB_Param.m_ResourcePath0);
					this.m_Handler[1] = spriteMng.LoadAsyncGachaItemLabelIcon(gachaItemLabelListDB_Param.m_ResourcePath1);
					this.m_Handler[2] = spriteMng.LoadAsyncGachaItemLabelIcon("GachaItem_" + gachaItemLabelListDB_Param.m_ItemID);
					break;
				}
			}
			for (int k = 0; k < 3; k++)
			{
				this.m_Image[k].enabled = false;
			}
			this.m_Step = GachaItemLabel.eStep.Prepare;
		}

		// Token: 0x0600328E RID: 12942 RVA: 0x001017DE File Offset: 0x000FFBDE
		public void Stop()
		{
			this.m_Step = GachaItemLabel.eStep.None;
			if (this.m_GameObject != null)
			{
				this.m_GameObject.SetActive(false);
			}
			this.m_Step = GachaItemLabel.eStep.None;
		}

		// Token: 0x0600328F RID: 12943 RVA: 0x0010180C File Offset: 0x000FFC0C
		private void Update()
		{
			switch (this.m_Step)
			{
			case GachaItemLabel.eStep.Prepare:
			{
				bool flag = true;
				for (int i = 0; i < 3; i++)
				{
					if (this.m_Handler[i] != null)
					{
						if (this.m_Handler[i].IsAvailable())
						{
							this.m_Image[i].sprite = this.m_Handler[i].Obj;
							this.m_Image[i].enabled = true;
						}
						else
						{
							flag = false;
						}
					}
				}
				if (flag)
				{
					this.m_Step = GachaItemLabel.eStep.Play;
				}
				break;
			}
			case GachaItemLabel.eStep.Play:
				this.m_Animation.gameObject.SetActive(true);
				this.m_Animation.Play();
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.GACHA_04, 1f, 0, -1, -1);
				this.m_Step = GachaItemLabel.eStep.PlayWait;
				break;
			case GachaItemLabel.eStep.PlayWait:
				if (!this.m_Animation.isPlaying)
				{
					this.m_Step = GachaItemLabel.eStep.Finish;
				}
				break;
			case GachaItemLabel.eStep.Finish:
				this.m_GameObject.SetActive(false);
				this.m_Step = GachaItemLabel.eStep.None;
				break;
			}
		}

		// Token: 0x0400389E RID: 14494
		[SerializeField]
		private Image[] m_Image;

		// Token: 0x0400389F RID: 14495
		private SpriteHandler[] m_Handler = new SpriteHandler[3];

		// Token: 0x040038A0 RID: 14496
		private GameObject m_GameObject;

		// Token: 0x040038A1 RID: 14497
		[SerializeField]
		private Animation m_Animation;

		// Token: 0x040038A2 RID: 14498
		private GachaItemLabel.eStep m_Step;

		// Token: 0x02000990 RID: 2448
		public enum eImage
		{
			// Token: 0x040038A4 RID: 14500
			Top,
			// Token: 0x040038A5 RID: 14501
			Bottom,
			// Token: 0x040038A6 RID: 14502
			Item,
			// Token: 0x040038A7 RID: 14503
			Num
		}

		// Token: 0x02000991 RID: 2449
		public enum eStep
		{
			// Token: 0x040038A9 RID: 14505
			None,
			// Token: 0x040038AA RID: 14506
			Prepare,
			// Token: 0x040038AB RID: 14507
			Play,
			// Token: 0x040038AC RID: 14508
			PlayWait,
			// Token: 0x040038AD RID: 14509
			Finish
		}
	}
}
