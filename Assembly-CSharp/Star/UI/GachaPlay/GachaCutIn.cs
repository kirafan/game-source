﻿using System;
using Star.UI.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.GachaPlay
{
	// Token: 0x0200098E RID: 2446
	public class GachaCutIn : UIGroup
	{
		// Token: 0x06003285 RID: 12933 RVA: 0x001013D3 File Offset: 0x000FF7D3
		public void Open(string message, string cueSheet, string cueName)
		{
			this.OpenMessageOnly(message, true);
			this.m_VoiceCtrlReqID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(cueSheet, cueName, null, null, true);
		}

		// Token: 0x06003286 RID: 12934 RVA: 0x001013F8 File Offset: 0x000FF7F8
		public void OpenMessageOnly(string message, bool isAutoClose = true)
		{
			base.Open();
			if (this.m_CustomWindow != null)
			{
				this.m_TalkWindow = this.m_CustomWindow.GetComponent<ADVTalkCustomWindow>();
			}
			if (this.m_TalkWindow != null)
			{
				this.m_TalkWindow.Setup();
				if (this.m_TalkWindowFrameSprite != null)
				{
					this.m_TalkWindow.SetTalkWindowFrameSprite(this.m_TalkWindowFrameSprite);
				}
				if (this.m_SetTalkWindowDefaultFontSize)
				{
					this.m_TalkWindow.SetFontSize(this.m_TalkWindowDefaultFontSize);
				}
				if (this.m_SetTalkWindowTextSizeDeltaY)
				{
					this.m_TalkWindow.SetTalkWindowFrameSizeDeltaY(this.m_TalkWindowTextSizeDeltaY);
				}
				if (this.m_SetTalkWindowTextLocalPositionY)
				{
					this.m_TalkWindow.AddTalkWindowTextLocalPositionY(this.m_TalkWindowTextLocalPositionY);
				}
				if (this.m_SetTalkWindowTextLineSpacing)
				{
					this.m_TalkWindow.SetLineSpacing(this.m_TalkWindowTextLineSpacing);
				}
				this.m_TalkWindow.SetBalloonProtrsionData(this.m_BalloonProtrsionSprite, this.m_FitBalloonProtrsionRect);
				this.m_TalkWindow.SetPenActive(false);
				this.m_TalkWindow.SetHeightVariable(false);
				this.m_TalkWindow.SetText(message, ADVTalkCustomWindow.eTextState.Start);
			}
			if (this.m_Text != null)
			{
				this.m_Text.text = message;
			}
			this.m_IsAutoClose = isAutoClose;
			this.m_TimeCount = 0f;
		}

		// Token: 0x06003287 RID: 12935 RVA: 0x00101546 File Offset: 0x000FF946
		public override void Close()
		{
			base.Close();
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceCtrlReqID);
		}

		// Token: 0x06003288 RID: 12936 RVA: 0x00101564 File Offset: 0x000FF964
		public override void Update()
		{
			base.Update();
			if (base.State == UIGroup.eState.Idle && this.m_IsAutoClose)
			{
				if (this.m_VoiceCtrlReqID != -1)
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_VoiceCtrlReqID))
					{
						this.m_TimeCount += Time.deltaTime;
						if (this.m_TimeCount > this.m_DisplayTimeAfterPlayedVoice)
						{
							this.Close();
						}
					}
				}
				else
				{
					this.m_TimeCount += Time.deltaTime;
					if (0f <= this.m_DisplayTimeNoVoice && this.m_TimeCount > this.m_DisplayTimeNoVoice)
					{
						this.Close();
					}
				}
			}
		}

		// Token: 0x06003289 RID: 12937 RVA: 0x00101620 File Offset: 0x000FFA20
		public void SetTalker(int charaId)
		{
			this.SetTalker(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaId).m_Name);
		}

		// Token: 0x0600328A RID: 12938 RVA: 0x00101650 File Offset: 0x000FFA50
		public void SetTalker(string name)
		{
			if (this.m_TalkWindow != null)
			{
				this.m_TalkWindow.SetTalker(name);
			}
		}

		// Token: 0x0400388C RID: 14476
		[SerializeField]
		[Tooltip("文字列を出力するターゲット.プレハブ生成タイプの場合は必要がなければ設定は不要.")]
		private Text m_Text;

		// Token: 0x0400388D RID: 14477
		[SerializeField]
		[Tooltip("ボイス再生がない場合の表示時間")]
		private float m_DisplayTimeNoVoice = 4.5f;

		// Token: 0x0400388E RID: 14478
		[SerializeField]
		[Tooltip("ボイス再生がある場合の ボイス再生が終わった後の表示時間")]
		private float m_DisplayTimeAfterPlayedVoice = 1f;

		// Token: 0x0400388F RID: 14479
		private float m_TimeCount;

		// Token: 0x04003890 RID: 14480
		private bool m_IsAutoClose = true;

		// Token: 0x04003891 RID: 14481
		private ADVTalkCustomWindow m_TalkWindow;

		// Token: 0x04003892 RID: 14482
		[SerializeField]
		private bool m_SetTalkWindowDefaultFontSize;

		// Token: 0x04003893 RID: 14483
		[SerializeField]
		private int m_TalkWindowDefaultFontSize = -1;

		// Token: 0x04003894 RID: 14484
		[SerializeField]
		private bool m_SetTalkWindowTextSizeDeltaY;

		// Token: 0x04003895 RID: 14485
		[SerializeField]
		private float m_TalkWindowTextSizeDeltaY = -1f;

		// Token: 0x04003896 RID: 14486
		[SerializeField]
		private bool m_SetTalkWindowTextLocalPositionY;

		// Token: 0x04003897 RID: 14487
		[SerializeField]
		private float m_TalkWindowTextLocalPositionY = -1f;

		// Token: 0x04003898 RID: 14488
		[SerializeField]
		private bool m_SetTalkWindowTextLineSpacing;

		// Token: 0x04003899 RID: 14489
		[SerializeField]
		private float m_TalkWindowTextLineSpacing = -1f;

		// Token: 0x0400389A RID: 14490
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x0400389B RID: 14491
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x0400389C RID: 14492
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x0400389D RID: 14493
		private int m_VoiceCtrlReqID = -1;
	}
}
