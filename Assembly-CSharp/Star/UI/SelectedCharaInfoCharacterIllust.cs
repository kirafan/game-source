﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007B2 RID: 1970
	public class SelectedCharaInfoCharacterIllust : SelectedCharaInfoCharacterViewASyncImage
	{
		// Token: 0x060028A8 RID: 10408 RVA: 0x000D98B8 File Offset: 0x000D7CB8
		protected override ASyncImageAdapter CreateASyncImageAdapter()
		{
			return new CharaIllustAdapter(this.m_CharacterIllust, this.m_CharacterIllustType);
		}

		// Token: 0x04002F98 RID: 12184
		[SerializeField]
		[Tooltip("Modelの親.")]
		private CharaIllust m_CharacterIllust;

		// Token: 0x04002F99 RID: 12185
		protected CharaIllust.eCharaIllustType m_CharacterIllustType = CharaIllust.eCharaIllustType.Card;
	}
}
