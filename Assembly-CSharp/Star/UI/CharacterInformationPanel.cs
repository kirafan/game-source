﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020007AD RID: 1965
	public class CharacterInformationPanel : MonoBehaviour
	{
		// Token: 0x06002820 RID: 10272 RVA: 0x000D7328 File Offset: 0x000D5728
		public virtual void Setup(UserCharacterData userCharaData)
		{
			this.Setup();
			this.SetUserCharacterData(userCharaData);
		}

		// Token: 0x06002821 RID: 10273 RVA: 0x000D7338 File Offset: 0x000D5738
		public virtual void Setup()
		{
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Setup();
			}
			if (this.m_InfoTitle != null)
			{
				this.m_InfoTitle.Setup();
			}
			if (this.m_BustImage != null)
			{
			}
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.Setup();
			}
			if (this.m_InfoState != null)
			{
				this.m_InfoState.Setup();
			}
			if (this.m_Friendship != null)
			{
				this.m_Friendship.Setup();
			}
			if (this.m_SkillList != null)
			{
				this.m_SkillList.Setup();
			}
			if (this.m_ProfileMessage)
			{
				this.m_ProfileMessage.Setup();
			}
			if (this.m_CharacterVoiceName)
			{
				this.m_CharacterVoiceName.Setup();
			}
			if (this.m_VoiceList)
			{
				this.m_VoiceList.Setup();
			}
			this.ResetDirtyFlag();
		}

		// Token: 0x06002822 RID: 10274 RVA: 0x000D7458 File Offset: 0x000D5858
		public virtual void Destroy()
		{
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Destroy();
			}
			if (this.m_InfoTitle != null)
			{
				this.m_InfoTitle.Destroy();
			}
			if (this.m_BustImage != null)
			{
				this.m_BustImage.Destroy();
			}
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.Destroy();
			}
			if (this.m_InfoState != null)
			{
				this.m_InfoState.Destroy();
			}
			if (this.m_Friendship != null)
			{
				this.m_Friendship.Destroy();
			}
			if (this.m_SkillList != null)
			{
				this.m_SkillList.Destroy();
			}
			if (this.m_ProfileMessage)
			{
				this.m_ProfileMessage.Destroy();
			}
			if (this.m_CharacterVoiceName)
			{
				this.m_CharacterVoiceName.Destroy();
			}
			if (this.m_VoiceList)
			{
				this.m_VoiceList.Destroy();
			}
		}

		// Token: 0x06002823 RID: 10275 RVA: 0x000D757A File Offset: 0x000D597A
		public void OnDirtyResource()
		{
			this.m_IsDirtyResource = true;
		}

		// Token: 0x06002824 RID: 10276 RVA: 0x000D7583 File Offset: 0x000D5983
		public void OnDirtyParameter()
		{
			this.m_IsDirtyParameter = true;
		}

		// Token: 0x06002825 RID: 10277 RVA: 0x000D758C File Offset: 0x000D598C
		protected virtual void ResetDirtyFlag()
		{
			this.m_IsDirtyResource = true;
			this.m_IsDirtyParameter = true;
		}

		// Token: 0x06002826 RID: 10278 RVA: 0x000D759C File Offset: 0x000D599C
		public virtual void ApplyAll(UserCharacterData userCharaData)
		{
			long mngID = userCharaData.MngID;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharaData.Param.NamedType);
			UserWeaponData userWeaponData = null;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.SetTitleImage(dbMng.NamedListDB.GetTitleType(userNamedData.NamedType));
			this.SetCharaInfoTitle(userCharaData.Param.CharaID);
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(userCharaData, userNamedData, userWeaponData);
			this.SetBeforeLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetAfterLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetBeforeLevel(userCharaData.Param.Lv, userCharaData.Param.MaxLv, null);
			this.SetAfterLevel(userCharaData.Param.Lv, userCharaData.Param.MaxLv, null);
			this.SetExpData(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharaData, null), outputCharaParam);
			this.ApplyState(outputCharaParam);
			this.SetCost(userCharaData.Param.Cost);
			this.SetCharaInfoTitleCost(userCharaData.Param.Cost, 0);
		}

		// Token: 0x06002827 RID: 10279 RVA: 0x000D76AC File Offset: 0x000D5AAC
		public virtual void ApplyAll(UserSupportData supportData)
		{
			EquipWeaponUnmanagedSupportPartyMemberController partyMember = new EquipWeaponUnmanagedSupportPartyMemberController().Setup(supportData);
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(supportData.CharaID);
			UserNamedData userNamedData = userDataMng.GetUserNamedData((eCharaNamedType)param.m_NamedType);
			this.SetTitleImage(dbMng.NamedListDB.GetTitleType(userNamedData.NamedType));
			this.SetCharaInfoTitleForCharacterListDBParam(param);
			this.SetBeforeLimitBreakIconValue(supportData.LimitBreak);
			this.SetAfterLimitBreakIconValue(supportData.LimitBreak);
			this.SetBeforeLevel(supportData.Lv, supportData.MaxLv, null);
			this.SetAfterLevel(supportData.Lv, supportData.MaxLv, null);
			this.SetExpData(partyMember, true, true);
			this.ApplyState(partyMember);
			this.SetCost(param.m_Cost);
			this.SetCharaInfoTitleCost(param.m_Cost, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(supportData.WeaponID).m_Cost);
		}

		// Token: 0x06002828 RID: 10280 RVA: 0x000D77A8 File Offset: 0x000D5BA8
		[Obsolete]
		public virtual void ApplyCharaDetail(long charaMngId)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(charaMngId);
			UserCharacterData userCharacterData = userCharaData;
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharacterData.Param.CharaID);
			UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharacterData.Param.NamedType);
			NamedListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam((eCharaNamedType)param.m_NamedType);
			NamedFriendshipExpDB namedFriendshipExpDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedFriendshipExpDB;
			this.SetCharaInfoTitle(userCharacterData.Param.CharaID);
			this.SetBeforeLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetAfterLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetBeforeLevel(userCharacterData.Param.Lv, userCharacterData.Param.MaxLv, null);
			this.SetExpData(userCharacterData);
			this.ApplyState(userCharacterData);
			this.GetFriendship().SetFriendship(userNamedData.FriendShip, userNamedData.FriendShipExp, param2.m_FriendshipTableID);
			this.ApplySkillDataAll(userCharacterData);
			this.SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.Weapon, false);
			this.SetTitleImage(param.m_CharaID);
			this.SetProfileMessageFieldText(param2.m_ProfileText);
			this.SetCharacterVoiceNameFieldText(param2.m_CVText);
			this.SetVoiceListItems(userNamedData.NamedType, userNamedData.FriendShip);
			this.SetCharaInfoTitleCost(userCharaData.Param.Cost, 0);
			this.RequestLoadCharacterViewCharacterIllustOnly(userCharaData.MngID, true);
		}

		// Token: 0x06002829 RID: 10281 RVA: 0x000D7914 File Offset: 0x000D5D14
		public void ApplyCharaDetail(EquipWeaponCharacterDataController cdc)
		{
			NamedListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(cdc.GetCharaNamedType());
			this.SetCharaInfoTitle(cdc.GetCharaId());
			this.SetBeforeLimitBreakIconValue(cdc.GetData().GetLimitBreak());
			this.SetAfterLimitBreakIconValue(cdc.GetData().GetLimitBreak());
			this.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			this.SetExpData(cdc, true, true);
			this.ApplyState(cdc);
			this.GetFriendship().SetFriendship(cdc);
			this.ApplySkillDataAll(cdc);
			if (cdc.GetCharacterDataControllerType() == eCharacterDataControllerType.UnmanagedUserCharacterData)
			{
				this.SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.Weapon, false);
			}
			else
			{
				this.SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.Weapon, true);
			}
			this.SetTitleImage(cdc.GetCharaId());
			this.SetProfileMessageFieldText(param.m_ProfileText);
			this.SetCharacterVoiceNameFieldText(param.m_CVText);
			this.SetVoiceListItems(cdc.GetCharaNamedType(), cdc.GetFriendship());
			this.SetCharaInfoTitleCost(cdc);
			this.RequestLoadCharacterViewCharacterIllustOnly(cdc, true);
		}

		// Token: 0x0600282A RID: 10282 RVA: 0x000D7A08 File Offset: 0x000D5E08
		public virtual void ApplyCharaDetailStateDetail(EquipWeaponCharacterDataController partyMember)
		{
			this.ApplyState(partyMember);
			this.ApplyStateBeforeAfterFromPartyMember(partyMember);
		}

		// Token: 0x0600282B RID: 10283 RVA: 0x000D7A18 File Offset: 0x000D5E18
		public virtual void SetUserCharacterData(EquipWeaponCharacterDataController cdc)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.SetTitleImage(cdc.GetCharaTitleType());
			this.SetCharaInfoTitle(cdc.GetCharaId());
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(cdc, true, true, true);
			this.SetBeforeLimitBreakIconValue(cdc.GetCharaLimitBreak());
			this.SetAfterLimitBreakIconValue(cdc.GetCharaLimitBreak());
			this.SetBeforeLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			this.SetAfterLevel(cdc.GetCharaLv(), cdc.GetCharaMaxLv(), null);
			this.SetExpData(cdc, outputCharaParam);
			this.ApplyState(outputCharaParam);
			this.SetCost(cdc.GetCost());
			this.SetCharaInfoTitleCost(cdc.GetCharaCost(), 0);
		}

		// Token: 0x0600282C RID: 10284 RVA: 0x000D7AB8 File Offset: 0x000D5EB8
		public virtual void SetUserCharacterData(UserCharacterData userCharaData)
		{
			long mngID = userCharaData.MngID;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserNamedData userNamedData = userDataMng.GetUserNamedData(userCharaData.Param.NamedType);
			UserWeaponData userWeaponData = null;
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			this.SetTitleImage(dbMng.NamedListDB.GetTitleType(userNamedData.NamedType));
			this.SetCharaInfoTitle(userCharaData.Param.CharaID);
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(userCharaData, userNamedData, userWeaponData);
			this.SetBeforeLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetAfterLimitBreakIconValue(userCharaData.Param.LimitBreak);
			this.SetBeforeLevel(userCharaData.Param.Lv, userCharaData.Param.MaxLv, null);
			this.SetAfterLevel(userCharaData.Param.Lv, userCharaData.Param.MaxLv, null);
			this.SetExpData(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharaData, null), outputCharaParam);
			this.ApplyState(outputCharaParam);
			this.SetCost(userCharaData.Param.Cost);
			this.SetCharaInfoTitleCost(userCharaData.Param.Cost, 0);
		}

		// Token: 0x0600282D RID: 10285 RVA: 0x000D7BC8 File Offset: 0x000D5FC8
		public virtual void SetUserSupportData(UserSupportData supportData)
		{
			EquipWeaponUnmanagedSupportPartyMemberController partyMember = new EquipWeaponUnmanagedSupportPartyMemberController().Setup(supportData);
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			CharacterListDB_Param param = dbMng.CharaListDB.GetParam(supportData.CharaID);
			UserNamedData userNamedData = userDataMng.GetUserNamedData((eCharaNamedType)param.m_NamedType);
			this.SetTitleImage(dbMng.NamedListDB.GetTitleType(userNamedData.NamedType));
			this.SetCharaInfoTitleForCharacterListDBParam(param);
			EditUtility.OutputCharaParam beforeParam = EditUtility.CalcCharaParam(supportData);
			this.SetBeforeLimitBreakIconValue(supportData.LimitBreak);
			this.SetAfterLimitBreakIconValue(supportData.LimitBreak);
			this.SetBeforeLevel(supportData.Lv, supportData.MaxLv, null);
			this.SetAfterLevel(supportData.Lv, supportData.MaxLv, null);
			this.SetExpData(partyMember, true, true);
			this.ApplyState(beforeParam);
			this.SetCost(param.m_Cost);
			this.SetCharaInfoTitleCost(param.m_Cost, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(supportData.WeaponID).m_Cost);
		}

		// Token: 0x0600282E RID: 10286 RVA: 0x000D7CCC File Offset: 0x000D60CC
		public void SetTitleImage(int charaId)
		{
			this.SetTitleImage((eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaId).m_NamedType);
		}

		// Token: 0x0600282F RID: 10287 RVA: 0x000D7CFC File Offset: 0x000D60FC
		public void SetTitleImage(eCharaNamedType charaNamed)
		{
			this.SetTitleImage(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetTitleType(charaNamed));
		}

		// Token: 0x06002830 RID: 10288 RVA: 0x000D7D19 File Offset: 0x000D6119
		public void SetTitleImage(eTitleType title)
		{
			if (this.m_TitleImage != null)
			{
				this.m_TitleImage.Prepare(title);
				this.m_TitleImage.AutoDisplay();
			}
		}

		// Token: 0x06002831 RID: 10289 RVA: 0x000D7D43 File Offset: 0x000D6143
		public void SetCharaInfoTitle(int charaId)
		{
			this.SetCharaInfoTitleForCharaId(charaId);
		}

		// Token: 0x06002832 RID: 10290 RVA: 0x000D7D4C File Offset: 0x000D614C
		public void SetCharaInfoTitleForCharaId(int charaId)
		{
			this.SetCharaInfoTitleForCharacterListDBParam(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaId));
		}

		// Token: 0x06002833 RID: 10291 RVA: 0x000D7D6C File Offset: 0x000D616C
		public void SetCharaInfoTitleForCharacterListDBParam(CharacterListDB_Param charaListDBParam)
		{
			if (this.m_InfoTitle != null)
			{
				this.m_InfoTitle.SetName(charaListDBParam.m_Name);
				this.m_InfoTitle.SetClassIcon((eClassType)charaListDBParam.m_Class);
				this.m_InfoTitle.SetElementIcon((eElementType)charaListDBParam.m_Element);
				this.m_InfoTitle.SetCharacterRareStars((eRare)charaListDBParam.m_Rare);
			}
		}

		// Token: 0x06002834 RID: 10292 RVA: 0x000D7DD4 File Offset: 0x000D61D4
		public void SetCharaInfoTitleCost(EquipWeaponCharacterDataController cdc)
		{
			int weaponCost = 0;
			if (!EditUtility.CheckDefaultWeaponNaked(cdc.GetWeaponId()))
			{
				weaponCost = cdc.GetWeaponCost();
			}
			this.SetCharaInfoTitleCost(cdc.GetCharaCost(), weaponCost);
		}

		// Token: 0x06002835 RID: 10293 RVA: 0x000D7E07 File Offset: 0x000D6207
		public void SetCharaInfoTitleCost(int charaCost, int weaponCost)
		{
			if (this.m_InfoTitle != null)
			{
				this.m_InfoTitle.SetCostText(charaCost, weaponCost);
			}
		}

		// Token: 0x06002836 RID: 10294 RVA: 0x000D7E27 File Offset: 0x000D6227
		[Obsolete]
		public void ApplyBustImage(int charaId)
		{
			if (this.m_BustImage != null)
			{
				this.m_BustImage.Apply(charaId, BustImage.eBustImageType.Chara);
			}
			if (this.m_CharaViewController != null)
			{
			}
		}

		// Token: 0x06002837 RID: 10295 RVA: 0x000D7E58 File Offset: 0x000D6258
		public void RequestLoadCharacterViewModelOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Begin, partyMember, automaticDisplay);
			}
		}

		// Token: 0x06002838 RID: 10296 RVA: 0x000D7E79 File Offset: 0x000D6279
		[Obsolete]
		public void RequestLoadCharacterViewModelOnly(long charaMngID, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadModelOnly(charaMngID, automaticDisplay);
			}
		}

		// Token: 0x06002839 RID: 10297 RVA: 0x000D7E99 File Offset: 0x000D6299
		public void RequestLoadCharacterViewModelOnly(int charaId, int weaponId, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Begin, charaId, weaponId, automaticDisplay);
			}
		}

		// Token: 0x0600283A RID: 10298 RVA: 0x000D7EBB File Offset: 0x000D62BB
		public void RequestLoadCharacterViewCharacterIllustOnly(CharacterDataController partyMember, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Illust, partyMember, automaticDisplay);
			}
		}

		// Token: 0x0600283B RID: 10299 RVA: 0x000D7EDC File Offset: 0x000D62DC
		public void RequestLoadCharacterViewCharacterIllustOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Illust, partyMember, automaticDisplay);
			}
		}

		// Token: 0x0600283C RID: 10300 RVA: 0x000D7EFD File Offset: 0x000D62FD
		[Obsolete]
		public void RequestLoadCharacterViewCharacterIllustOnly(long charaMngID, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadCharacterIllustOnly(charaMngID, automaticDisplay);
			}
		}

		// Token: 0x0600283D RID: 10301 RVA: 0x000D7F1D File Offset: 0x000D631D
		public void RequestLoadCharacterViewCharacterIllustOnly(int charaId, int weaponId, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Illust, charaId, weaponId, automaticDisplay);
			}
		}

		// Token: 0x0600283E RID: 10302 RVA: 0x000D7F3F File Offset: 0x000D633F
		public void RequestLoadCharacterViewCharacterBustImageOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Bust, partyMember, automaticDisplay);
			}
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x000D7F60 File Offset: 0x000D6360
		[Obsolete]
		public void RequestLoadCharacterViewCharacterBustImageOnly(long charaMngID, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadCharacterBustImageOnly(charaMngID, automaticDisplay);
			}
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x000D7F80 File Offset: 0x000D6380
		public void RequestLoadCharacterViewCharacterBustImageOnly(int charaId, int weaponId, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Illust, charaId, weaponId, automaticDisplay);
			}
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x000D7FA2 File Offset: 0x000D63A2
		public void RequestLoadCharacterViewCharacterIconOnly(EquipWeaponCharacterDataController partyMember, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Icon, partyMember, automaticDisplay);
			}
		}

		// Token: 0x06002842 RID: 10306 RVA: 0x000D7FC3 File Offset: 0x000D63C3
		[Obsolete]
		public void RequestLoadCharacterViewCharacterIconOnly(long charaMngID, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadCharacterIconOnly(charaMngID, automaticDisplay);
			}
		}

		// Token: 0x06002843 RID: 10307 RVA: 0x000D7FE3 File Offset: 0x000D63E3
		public void RequestLoadCharacterViewCharacterIconOnly(int charaId, int weaponId, bool automaticDisplay = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.RequestLoadView(SelectedCharaInfoCharacterViewController.eViewType.Icon, charaId, weaponId, automaticDisplay);
			}
		}

		// Token: 0x06002844 RID: 10308 RVA: 0x000D8005 File Offset: 0x000D6405
		public void ForceRequestLoadCharacterView(SelectedCharaInfoCharacterViewController.eViewType viewType, EquipWeaponCharacterDataController ewcdc)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.ForceRequestLoadView(viewType, ewcdc, true);
			}
		}

		// Token: 0x06002845 RID: 10309 RVA: 0x000D8026 File Offset: 0x000D6426
		public void ForceRequestLoadCharacterView(SelectedCharaInfoCharacterViewController.eViewType viewType, CharacterDataController cdc)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.ForceRequestLoadView(viewType, cdc, true);
			}
		}

		// Token: 0x06002846 RID: 10310 RVA: 0x000D8047 File Offset: 0x000D6447
		public void SetCharacterIconLimitBreak(int lb)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.SetLimitBreak(lb);
			}
		}

		// Token: 0x06002847 RID: 10311 RVA: 0x000D8066 File Offset: 0x000D6466
		public void PlayInCharacterView(bool autoIdle = true)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.PlayIn(autoIdle);
			}
		}

		// Token: 0x06002848 RID: 10312 RVA: 0x000D8085 File Offset: 0x000D6485
		public void FinishInCharacterView()
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.FinishIn();
			}
		}

		// Token: 0x06002849 RID: 10313 RVA: 0x000D80A3 File Offset: 0x000D64A3
		public void PlayOutCharacterView()
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.PlayOut();
			}
		}

		// Token: 0x0600284A RID: 10314 RVA: 0x000D80C1 File Offset: 0x000D64C1
		public void SetEnableRenderCharacterView(bool flg)
		{
			if (this.m_CharaViewController != null)
			{
				this.m_CharaViewController.gameObject.SetActive(flg);
			}
		}

		// Token: 0x0600284B RID: 10315 RVA: 0x000D80E5 File Offset: 0x000D64E5
		public SelectedCharaInfoCharacterViewBase.eState GetCharacterViewState()
		{
			if (this.m_CharaViewController != null)
			{
				return this.m_CharaViewController.GetState();
			}
			return SelectedCharaInfoCharacterViewBase.eState.Invalid;
		}

		// Token: 0x0600284C RID: 10316 RVA: 0x000D8105 File Offset: 0x000D6505
		public void SetBeforeLevel(int nowLevel, int maxLevel, SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = null)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetBeforeLevel(nowLevel, maxLevel, info);
			}
		}

		// Token: 0x0600284D RID: 10317 RVA: 0x000D8126 File Offset: 0x000D6526
		public void SetAfterLevel(int nowLevel, int maxLevel, SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = null)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetAfterLevel(nowLevel, maxLevel, info);
			}
		}

		// Token: 0x0600284E RID: 10318 RVA: 0x000D8147 File Offset: 0x000D6547
		public void PlayAfterLevelAnimation()
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.PlayAfterLevelAnimation();
			}
		}

		// Token: 0x0600284F RID: 10319 RVA: 0x000D8165 File Offset: 0x000D6565
		public int GetAfterLevelAnimationState()
		{
			if (this.m_InfoState != null)
			{
				return this.m_InfoState.GetAfterLevelAnimationState();
			}
			return -1;
		}

		// Token: 0x06002850 RID: 10320 RVA: 0x000D8185 File Offset: 0x000D6585
		public void SetBeforeLimitBreakIconValue(int value)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetBeforeLimitBreakIconValue(value);
			}
		}

		// Token: 0x06002851 RID: 10321 RVA: 0x000D81A4 File Offset: 0x000D65A4
		public void SetAfterLimitBreakIconValue(int value)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetAfterLimitBreakIconValue(value);
			}
		}

		// Token: 0x06002852 RID: 10322 RVA: 0x000D81C3 File Offset: 0x000D65C3
		public void SetExpData(CharacterDataController cdc, bool appliedFriendship = true, bool appliedTownBuff = true)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(cdc, appliedFriendship, appliedTownBuff);
			}
		}

		// Token: 0x06002853 RID: 10323 RVA: 0x000D81E4 File Offset: 0x000D65E4
		public void SetExpData(EquipWeaponCharacterDataController partyMember, bool appliedFriendship = true, bool appliedWeapon = true)
		{
			if (partyMember == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			EditUtility.OutputCharaParam param = EditUtility.CalcCharaParam(partyMember, appliedFriendship, true, appliedWeapon);
			this.SetExpData(partyMember, param);
		}

		// Token: 0x06002854 RID: 10324 RVA: 0x000D8214 File Offset: 0x000D6614
		public void SetExpData(long charaMngID)
		{
			this.SetExpData(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID));
		}

		// Token: 0x06002855 RID: 10325 RVA: 0x000D822C File Offset: 0x000D662C
		public void SetExpData(UserCharacterData userCharaData)
		{
			if (userCharaData == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			this.SetExpData(userCharaData, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserNamedData(userCharaData.Param.NamedType), null);
		}

		// Token: 0x06002856 RID: 10326 RVA: 0x000D8264 File Offset: 0x000D6664
		public void SetExpData(UserCharacterData userCharaData, UserNamedData userNamedData, UserWeaponData userWeaponData)
		{
			if (userCharaData == null)
			{
				Debug.LogError("Invalid CharaMngID");
				return;
			}
			if (userNamedData == null)
			{
				Debug.LogError("Invalid NamedType");
				return;
			}
			EditUtility.OutputCharaParam param = EditUtility.CalcCharaParam(userCharaData, userNamedData, userWeaponData);
			this.SetExpData(new EquipWeaponUnmanagedUserCharacterDataController().Setup(userCharaData, null), param);
		}

		// Token: 0x06002857 RID: 10327 RVA: 0x000D82AF File Offset: 0x000D66AF
		public void SetExpData(CharacterDataController partyMember, EditUtility.OutputCharaParam param)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(partyMember, param);
			}
		}

		// Token: 0x06002858 RID: 10328 RVA: 0x000D82CF File Offset: 0x000D66CF
		public void SetExpData(int maxLevel, EditUtility.OutputCharaParam? param)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(maxLevel, param);
			}
		}

		// Token: 0x06002859 RID: 10329 RVA: 0x000D82EF File Offset: 0x000D66EF
		public void SetExpData(CharacterDataController cdc, CharacterDataController after)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(cdc, after);
			}
		}

		// Token: 0x0600285A RID: 10330 RVA: 0x000D830F File Offset: 0x000D670F
		public void SetExpData(int maxLevel, CharacterDataController cdc)
		{
			this.SetExpData(cdc.GetCharaLv(), cdc.GetCharaNowExp(), maxLevel, cdc.GetCharaNowMaxExp());
		}

		// Token: 0x0600285B RID: 10331 RVA: 0x000D832A File Offset: 0x000D672A
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(level, nowexp, maxLevel, needExp);
			}
		}

		// Token: 0x0600285C RID: 10332 RVA: 0x000D834D File Offset: 0x000D674D
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
			}
		}

		// Token: 0x0600285D RID: 10333 RVA: 0x000D8376 File Offset: 0x000D6776
		public void PlayExpGauge()
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.PlayExpGauge();
			}
		}

		// Token: 0x0600285E RID: 10334 RVA: 0x000D8394 File Offset: 0x000D6794
		public bool IsPlayingGauge()
		{
			return this.m_InfoState != null && this.m_InfoState.IsPlayingGauge();
		}

		// Token: 0x0600285F RID: 10335 RVA: 0x000D83B4 File Offset: 0x000D67B4
		public void SkipExpGaugePlaying()
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SkipExpGaugePlaying();
			}
		}

		// Token: 0x06002860 RID: 10336 RVA: 0x000D83D2 File Offset: 0x000D67D2
		public void SetSimulateExpGauge(long exp, long needExp, int addLv)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetSimulateExpGauge(exp, needExp, addLv);
			}
		}

		// Token: 0x06002861 RID: 10337 RVA: 0x000D83F3 File Offset: 0x000D67F3
		public void SetCost(int cost)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetCost(cost);
			}
		}

		// Token: 0x06002862 RID: 10338 RVA: 0x000D8412 File Offset: 0x000D6812
		public void ApplyState(EquipWeaponCharacterDataController managedBattlePartyMember, bool appliedFriendship = true, bool appliedWeapon = true)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(managedBattlePartyMember, appliedFriendship, appliedWeapon);
			}
		}

		// Token: 0x06002863 RID: 10339 RVA: 0x000D8433 File Offset: 0x000D6833
		public void ApplyState(EquipWeaponManagedSupportPartyMemberController managedSupportPartyMember)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(managedSupportPartyMember);
			}
		}

		// Token: 0x06002864 RID: 10340 RVA: 0x000D8452 File Offset: 0x000D6852
		public void ApplyState(long charaMngID)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(charaMngID);
			}
		}

		// Token: 0x06002865 RID: 10341 RVA: 0x000D8471 File Offset: 0x000D6871
		public void ApplyState(UserCharacterData userCharaData)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(userCharaData, null);
			}
		}

		// Token: 0x06002866 RID: 10342 RVA: 0x000D8491 File Offset: 0x000D6891
		public void ApplyState(UserCharacterData userCharaData, UserNamedData userNamedData, UserWeaponData userWeaponData)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(userCharaData, userWeaponData);
			}
		}

		// Token: 0x06002867 RID: 10343 RVA: 0x000D84B1 File Offset: 0x000D68B1
		public void ApplyState(UserSupportDataWrapper userSupportDataWrapper)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(userSupportDataWrapper);
			}
		}

		// Token: 0x06002868 RID: 10344 RVA: 0x000D84D0 File Offset: 0x000D68D0
		public void ApplyState(EditUtility.OutputCharaParam beforeParam)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(beforeParam);
			}
		}

		// Token: 0x06002869 RID: 10345 RVA: 0x000D84EF File Offset: 0x000D68EF
		public void ApplyState(EquipWeaponCharacterDataController cdc, EquipWeaponCharacterDataController after)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(cdc, after);
			}
		}

		// Token: 0x0600286A RID: 10346 RVA: 0x000D850F File Offset: 0x000D690F
		public void ApplyState(CharacterDataController cdc, CharacterDataController after)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(cdc, after);
			}
		}

		// Token: 0x0600286B RID: 10347 RVA: 0x000D852F File Offset: 0x000D692F
		public void ApplyState(EquipWeaponCharacterDataController cdc, EditUtility.OutputCharaParam? afterParam)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(cdc, afterParam);
			}
		}

		// Token: 0x0600286C RID: 10348 RVA: 0x000D854F File Offset: 0x000D694F
		public void ApplyState(EditUtility.OutputCharaParam? beforeParam, EditUtility.OutputCharaParam? afterParam)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(new EditUtility.OutputCharaParam?(beforeParam.Value), new EditUtility.OutputCharaParam?(afterParam.Value));
			}
		}

		// Token: 0x0600286D RID: 10349 RVA: 0x000D8585 File Offset: 0x000D6985
		public virtual void ApplyState(int[] states, int[] adds = null)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(states, adds);
			}
		}

		// Token: 0x0600286E RID: 10350 RVA: 0x000D85A5 File Offset: 0x000D69A5
		public void ApplyStateBeforeAfterFromPartyMember(EquipWeaponCharacterDataController partyMember)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyStateBeforeAfterFromPartyMember(partyMember);
			}
		}

		// Token: 0x0600286F RID: 10351 RVA: 0x000D85C4 File Offset: 0x000D69C4
		public void ApplyState(EquipWeaponCharacterDataController partyMember)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.ApplyState(partyMember);
			}
		}

		// Token: 0x06002870 RID: 10352 RVA: 0x000D85E3 File Offset: 0x000D69E3
		public void SetStateWeaponIcon(int weaponId)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetWeaponIcon(weaponId);
			}
		}

		// Token: 0x06002871 RID: 10353 RVA: 0x000D8602 File Offset: 0x000D6A02
		public void SetStateWeaponIcon(UserWeaponData weaponData)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetWeaponIcon(weaponData);
			}
		}

		// Token: 0x06002872 RID: 10354 RVA: 0x000D8621 File Offset: 0x000D6A21
		public void SetStateWeaponIcon(EquipWeaponCharacterDataController partyMember)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetWeaponIcon(partyMember);
			}
		}

		// Token: 0x06002873 RID: 10355 RVA: 0x000D8640 File Offset: 0x000D6A40
		public void SetStateWeaponIconMode(WeaponIconWithFrame.eMode mode)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetWeaponIconMode(mode);
			}
		}

		// Token: 0x06002874 RID: 10356 RVA: 0x000D865F File Offset: 0x000D6A5F
		public void SetStateHaveTitleFieldTitle(int index, string title)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetHaveTitleFieldTitle(index, title);
			}
		}

		// Token: 0x06002875 RID: 10357 RVA: 0x000D867F File Offset: 0x000D6A7F
		public void SetStateHaveTitleFieldText(int index, string text)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetHaveTitleFieldText(index, text);
			}
		}

		// Token: 0x06002876 RID: 10358 RVA: 0x000D869F File Offset: 0x000D6A9F
		public void SetStateHaveTitleFieldTitleActive(int index, bool active)
		{
			if (this.m_InfoState != null)
			{
				this.m_InfoState.SetHaveTitleFieldTitleActive(index, active);
			}
		}

		// Token: 0x06002877 RID: 10359 RVA: 0x000D86BF File Offset: 0x000D6ABF
		public SelectedCharaInfoFriendship GetFriendship()
		{
			return this.m_Friendship;
		}

		// Token: 0x06002878 RID: 10360 RVA: 0x000D86C8 File Offset: 0x000D6AC8
		public void ApplySkillDataAll(EquipWeaponCharacterDataController partyMember)
		{
			CharacterDataWrapperBase data = partyMember.GetData();
			this.ApplyUniqueSkill(data);
			for (int i = 0; i < data.GetClassSkillsCount(); i++)
			{
				this.ApplyClassSkill(data, i);
			}
			this.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.Weapon, 0, new SelectedCharaInfoWeaponInfo.ApplyArgumentEquipWeaponPartyMemberController(partyMember));
			UserScheduleData.DropState in_dropState = UserCharaUtil.GetScheduleDropState(data.GetCharaId())[0];
			this.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.ScheduleDropState, 0, new SelectedCharaInfoScheduleDropStateInfo.ApplyArgument(data.GetCharaId(), in_dropState));
		}

		// Token: 0x06002879 RID: 10361 RVA: 0x000D8734 File Offset: 0x000D6B34
		public void ApplySkillDataAll(UserCharacterData userCharaData)
		{
			int num;
			int num2;
			int max;
			int num3;
			int[] array;
			int[] array2;
			EditUtility.SimulateLimitBreak(userCharaData.MngID, out num, out num2, out max, out num3, out array, out array2);
			this.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.UniquieSkill, 0, new SelectedCharaInfoSkillInfo.ApplyArgument(SkillIcon.eSkillDBType.Player, userCharaData.Param.UniqueSkillLearnData.SkillID, userCharaData.Param.ElementType, new LevelInformation(userCharaData.Param.UniqueSkillLearnData.SkillLv, max), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetNeedRemainExp(userCharaData.Param.UniqueSkillLearnData.SkillLv, userCharaData.Param.UniqueSkillLearnData.TotalUseNum, userCharaData.Param.UniqueSkillLearnData.ExpTableID)));
			for (int i = 0; i < userCharaData.Param.ClassSkillLearnDatas.Count; i++)
			{
				this.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.GeneralSkill, i, new SelectedCharaInfoSkillInfo.ApplyArgument(SkillIcon.eSkillDBType.Player, userCharaData.Param.ClassSkillLearnDatas[i].SkillID, userCharaData.Param.ElementType, new LevelInformation(userCharaData.Param.ClassSkillLearnDatas[i].SkillLv, userCharaData.Param.ClassSkillLearnDatas[i].SkillMaxLv), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetNeedRemainExp(userCharaData.Param.ClassSkillLearnDatas[i].SkillLv, userCharaData.Param.ClassSkillLearnDatas[i].TotalUseNum, userCharaData.Param.ClassSkillLearnDatas[i].ExpTableID)));
			}
			Debug.LogError("要調整.");
			UserScheduleData.DropState in_dropState = UserCharaUtil.GetScheduleDropState(userCharaData.Param.CharaID)[0];
			this.ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.ScheduleDropState, 0, new SelectedCharaInfoScheduleDropStateInfo.ApplyArgument(userCharaData.Param.CharaID, in_dropState));
		}

		// Token: 0x0600287A RID: 10362 RVA: 0x000D88F8 File Offset: 0x000D6CF8
		public void ApplyUniqueSkill(CharacterDataWrapperBase characterData)
		{
			SkillLearnData uniqueSkillLearnData = characterData.GetUniqueSkillLearnData();
			this.ApplyCharacterSkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.UniquieSkill, 0, characterData.GetElementType(), uniqueSkillLearnData);
		}

		// Token: 0x0600287B RID: 10363 RVA: 0x000D891C File Offset: 0x000D6D1C
		public void ApplyClassSkill(CharacterDataWrapperBase characterData, int index)
		{
			SkillLearnData classSkillLearnData = characterData.GetClassSkillLearnData(index);
			this.ApplyCharacterSkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType.GeneralSkill, index, characterData.GetElementType(), classSkillLearnData);
		}

		// Token: 0x0600287C RID: 10364 RVA: 0x000D8940 File Offset: 0x000D6D40
		protected void ApplyCharacterSkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, int index, eElementType ownerElement, SkillLearnData sld)
		{
			long needRemainExp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetNeedRemainExp(sld.SkillLv, sld.TotalUseNum, sld.ExpTableID);
			this.ApplySkill(groupType, index, new SelectedCharaInfoSkillInfo.ApplyArgument(SkillIcon.eSkillDBType.Player, sld.SkillID, ownerElement, new LevelInformation(sld.SkillLv, sld.SkillMaxLv), needRemainExp));
		}

		// Token: 0x0600287D RID: 10365 RVA: 0x000D89A1 File Offset: 0x000D6DA1
		public void ApplySkill(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, int index, SelectedCharaInfoDetailInfoBase.ApplyArgumentBase argument)
		{
			if (this.m_SkillList != null)
			{
				this.m_SkillList.ApplySkill(groupType, index, argument);
			}
		}

		// Token: 0x0600287E RID: 10366 RVA: 0x000D89C2 File Offset: 0x000D6DC2
		public void SetDetailInfoGroupActive(SelectedCharaInfoDetailInfoGroupBase.eSkillInfoGroupType groupType, bool active)
		{
			if (this.m_SkillList != null)
			{
				this.m_SkillList.SetDetailInfoGroupActive(groupType, active);
			}
		}

		// Token: 0x0600287F RID: 10367 RVA: 0x000D89E2 File Offset: 0x000D6DE2
		public void SetProfileMessageFieldTitle(string title)
		{
			if (this.m_ProfileMessage != null)
			{
				this.m_ProfileMessage.SetTitle(title);
			}
		}

		// Token: 0x06002880 RID: 10368 RVA: 0x000D8A01 File Offset: 0x000D6E01
		public void SetProfileMessageFieldText(string text)
		{
			if (this.m_ProfileMessage != null)
			{
				this.m_ProfileMessage.SetText(text);
			}
		}

		// Token: 0x06002881 RID: 10369 RVA: 0x000D8A20 File Offset: 0x000D6E20
		public void SetProfileMessageFieldTitleActive(bool active)
		{
			if (this.m_ProfileMessage != null)
			{
				this.m_ProfileMessage.SetTitleActive(active);
			}
		}

		// Token: 0x06002882 RID: 10370 RVA: 0x000D8A3F File Offset: 0x000D6E3F
		public void SetCharacterVoiceNameFieldTitle(string title)
		{
			if (this.m_CharacterVoiceName != null)
			{
				this.m_CharacterVoiceName.SetTitle(title);
			}
		}

		// Token: 0x06002883 RID: 10371 RVA: 0x000D8A5E File Offset: 0x000D6E5E
		public void SetCharacterVoiceNameFieldText(string text)
		{
			if (this.m_CharacterVoiceName != null)
			{
				this.m_CharacterVoiceName.SetCharacterVoiceName(text);
			}
		}

		// Token: 0x06002884 RID: 10372 RVA: 0x000D8A7D File Offset: 0x000D6E7D
		public void SetCharacterVoiceNameFieldTitleActive(bool active)
		{
			if (this.m_CharacterVoiceName != null)
			{
				this.m_CharacterVoiceName.SetTitleActive(active);
			}
		}

		// Token: 0x06002885 RID: 10373 RVA: 0x000D8A9C File Offset: 0x000D6E9C
		public void SetVoiceListItems(eCharaNamedType charaNamed, int nowFriendship)
		{
			if (this.m_VoiceList)
			{
				this.m_VoiceList.SetVoiceListItems(charaNamed, nowFriendship);
			}
		}

		// Token: 0x06002886 RID: 10374 RVA: 0x000D8ABC File Offset: 0x000D6EBC
		public void SetHaveTitleFieldTitle(int index, string title)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetTitle(title);
			}
		}

		// Token: 0x06002887 RID: 10375 RVA: 0x000D8B0C File Offset: 0x000D6F0C
		public void SetHaveTitleFieldText(int index, string text)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetText(text);
			}
		}

		// Token: 0x06002888 RID: 10376 RVA: 0x000D8B5C File Offset: 0x000D6F5C
		public void SetHaveTitleFieldTitleActive(int index, bool active)
		{
			if (this.m_HaveTitleTextFields != null && 0 <= index && index < this.m_HaveTitleTextFields.Length && this.m_HaveTitleTextFields[index] != null)
			{
				this.m_HaveTitleTextFields[index].SetTitleActive(active);
			}
		}

		// Token: 0x04002F77 RID: 12151
		[SerializeField]
		private TitleImage m_TitleImage;

		// Token: 0x04002F78 RID: 12152
		[SerializeField]
		private SelectedCharaInfoTitle m_InfoTitle;

		// Token: 0x04002F79 RID: 12153
		[SerializeField]
		[Tooltip("SelectedCharaInfoCharacterViewControllerに結合.以後使用禁止.")]
		private BustImage m_BustImage;

		// Token: 0x04002F7A RID: 12154
		[SerializeField]
		private SelectedCharaInfoCharacterViewController m_CharaViewController;

		// Token: 0x04002F7B RID: 12155
		[SerializeField]
		private SelectedCharaInfoState m_InfoState;

		// Token: 0x04002F7C RID: 12156
		[SerializeField]
		private SelectedCharaInfoFriendship m_Friendship;

		// Token: 0x04002F7D RID: 12157
		[SerializeField]
		private SelectedCharaInfoDetailInfoList m_SkillList;

		// Token: 0x04002F7E RID: 12158
		[SerializeField]
		private SelectedCharaInfoProfileMessageField m_ProfileMessage;

		// Token: 0x04002F7F RID: 12159
		[SerializeField]
		private SelectedCharaInfoCharacterVoiceNameField m_CharacterVoiceName;

		// Token: 0x04002F80 RID: 12160
		[SerializeField]
		private SelectedCharaInfoVoiceList m_VoiceList;

		// Token: 0x04002F81 RID: 12161
		[SerializeField]
		[Tooltip("汎用テキスト出力領域.")]
		private SelectedCharaInfoHaveTitleTextField[] m_HaveTitleTextFields;

		// Token: 0x04002F82 RID: 12162
		protected bool m_IsDirtyResource = true;

		// Token: 0x04002F83 RID: 12163
		protected bool m_IsDirtyParameter = true;
	}
}
