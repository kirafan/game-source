﻿using System;
using Star.UI.Window;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200088C RID: 2188
	public class DataInheritenceSelectContnt : CustomWindow
	{
		// Token: 0x06002D6B RID: 11627 RVA: 0x000EFD80 File Offset: 0x000EE180
		public void Setup(bool isCofiguration)
		{
			if (this.m_ALButtonUsePlatform.m_Button != null)
			{
				this.m_ALButtonUsePlatform.m_Button.gameObject.SetActive(false);
			}
			if (this.m_ALButtonUsePlatform.m_ButtonText != null)
			{
				this.m_ALButtonUsePlatform.m_ButtonText.enabled = false;
			}
			if (this.m_ALButtonUsePlatform.m_ButtonTextImage != null)
			{
				this.m_ALButtonUsePlatform.m_ButtonTextImage.enabled = false;
			}
			if (isCofiguration)
			{
				if (this.m_Title != null && this.m_Title.m_TextObj != null)
				{
					this.m_Title.m_TextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingSetting);
				}
				if (this.m_ALButtonUsePlatform.m_ButtonText != null)
				{
					this.m_ALButtonUsePlatform.m_ButtonText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingButtonTitle_Android_UseMenu);
				}
			}
			else
			{
				if (this.m_Title != null && this.m_Title.m_TextObj != null)
				{
					this.m_Title.m_TextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingConfirm);
				}
				if (this.m_ALButtonUsePlatform.m_ButtonText != null)
				{
					this.m_ALButtonUsePlatform.m_ButtonText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingButtonTitle_Android_UseTitle);
				}
			}
			if (this.m_ALButtonUsePlatform.m_ButtonTextImage != null)
			{
				if (this.m_AndroidIconTexture == null)
				{
					this.m_ALButtonUsePlatform.m_ButtonTextImage.enabled = false;
				}
				else
				{
					this.m_ALButtonUsePlatform.m_ButtonTextImage.enabled = true;
					this.m_ALButtonUsePlatform.m_ButtonTextImage.color = Color.white;
					this.m_ALButtonUsePlatform.m_ButtonTextImage.sprite = this.m_AndroidIconTexture;
				}
			}
			this.m_ALButtonUseIDAndPassword.m_ButtonText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingButtonTitle_UseIDPassword);
		}

		// Token: 0x06002D6C RID: 11628 RVA: 0x000EFFAD File Offset: 0x000EE3AD
		public void SetCallback(Action Callback_ALButtonUsePlatform, Action Callback_ALButtonIDAndPassword, Action Callback_CloseButton)
		{
			this.m_OnClickALButtonUsePlatform = Callback_ALButtonUsePlatform;
			this.m_OnClickALButtonUseIDAndPassword = Callback_ALButtonIDAndPassword;
			this.m_OnCkickCloseButton = Callback_CloseButton;
		}

		// Token: 0x06002D6D RID: 11629 RVA: 0x000EFFC4 File Offset: 0x000EE3C4
		public void SetCallbackALButtonUsePlatform(Action callback)
		{
			this.m_OnClickALButtonUsePlatform = callback;
		}

		// Token: 0x06002D6E RID: 11630 RVA: 0x000EFFCD File Offset: 0x000EE3CD
		public void SetCallbackALButtonIDAndPassword(Action callback)
		{
			this.m_OnClickALButtonUseIDAndPassword = callback;
		}

		// Token: 0x06002D6F RID: 11631 RVA: 0x000EFFD6 File Offset: 0x000EE3D6
		public void SetCallbackCloseButton(Action callback)
		{
			this.m_OnCkickCloseButton = callback;
		}

		// Token: 0x06002D70 RID: 11632 RVA: 0x000EFFDF File Offset: 0x000EE3DF
		public void OnClickALButtonUsePlatform()
		{
			if (this.m_OnClickALButtonUsePlatform != null)
			{
				this.m_OnClickALButtonUsePlatform();
			}
		}

		// Token: 0x06002D71 RID: 11633 RVA: 0x000EFFF7 File Offset: 0x000EE3F7
		public void OnClickALButtonUseIDAndPassword()
		{
			if (this.m_OnClickALButtonUseIDAndPassword != null)
			{
				this.m_OnClickALButtonUseIDAndPassword();
			}
		}

		// Token: 0x06002D72 RID: 11634 RVA: 0x000F000F File Offset: 0x000EE40F
		public void OnCkickCloseButton()
		{
			if (this.m_OnCkickCloseButton != null)
			{
				this.m_OnCkickCloseButton();
			}
		}

		// Token: 0x04003470 RID: 13424
		[SerializeField]
		private DataInheritenceSelectContnt.ButtonStruct m_ALButtonUsePlatform;

		// Token: 0x04003471 RID: 13425
		[SerializeField]
		private DataInheritenceSelectContnt.ButtonStruct m_ALButtonUseIDAndPassword;

		// Token: 0x04003472 RID: 13426
		[SerializeField]
		private DataInheritenceSelectContnt.ButtonStruct m_CloseButton;

		// Token: 0x04003473 RID: 13427
		[SerializeField]
		private Sprite m_AndroidIconTexture;

		// Token: 0x04003474 RID: 13428
		[SerializeField]
		private Sprite m_IOSIconTexture;

		// Token: 0x04003475 RID: 13429
		private DataInheritenceWindowContent.eType m_WindowType;

		// Token: 0x04003476 RID: 13430
		private Action m_OnClickALButtonUsePlatform;

		// Token: 0x04003477 RID: 13431
		private Action m_OnClickALButtonUseIDAndPassword;

		// Token: 0x04003478 RID: 13432
		private Action m_OnCkickCloseButton;

		// Token: 0x0200088D RID: 2189
		private enum eMode
		{
			// Token: 0x0400347A RID: 13434
			Invalid = -1,
			// Token: 0x0400347B RID: 13435
			Open,
			// Token: 0x0400347C RID: 13436
			Main,
			// Token: 0x0400347D RID: 13437
			AccountLinkOpen,
			// Token: 0x0400347E RID: 13438
			AccountLinkProc,
			// Token: 0x0400347F RID: 13439
			AccountLinkClose,
			// Token: 0x04003480 RID: 13440
			Close
		}

		// Token: 0x0200088E RID: 2190
		[Serializable]
		private struct ButtonStruct
		{
			// Token: 0x04003481 RID: 13441
			public CustomButton m_Button;

			// Token: 0x04003482 RID: 13442
			public Image m_ButtonTextImage;

			// Token: 0x04003483 RID: 13443
			public Text m_ButtonText;
		}
	}
}
