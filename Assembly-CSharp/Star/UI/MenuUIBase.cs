﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000898 RID: 2200
	public abstract class MenuUIBase : MonoBehaviour
	{
		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06002DA3 RID: 11683 RVA: 0x000CA706 File Offset: 0x000C8B06
		public int State
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06002DA4 RID: 11684 RVA: 0x000CA70E File Offset: 0x000C8B0E
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x06002DA5 RID: 11685
		public abstract void PlayIn();

		// Token: 0x06002DA6 RID: 11686
		public abstract void PlayOut();

		// Token: 0x06002DA7 RID: 11687
		public abstract bool IsMenuEnd();

		// Token: 0x06002DA8 RID: 11688 RVA: 0x000CA733 File Offset: 0x000C8B33
		public virtual void Destroy()
		{
		}

		// Token: 0x06002DA9 RID: 11689 RVA: 0x000CA735 File Offset: 0x000C8B35
		public virtual void OnDisable()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
			}
		}

		// Token: 0x06002DAA RID: 11690 RVA: 0x000CA76C File Offset: 0x000C8B6C
		public virtual void SetEnableInput(bool flg)
		{
			if (this.m_RayCaster == null)
			{
				this.m_RayCaster = this.GameObject.GetComponent<GraphicRaycaster>();
			}
			if (this.m_RayCaster != null)
			{
				this.m_RayCaster.enabled = flg;
			}
			if (this.m_UIGroupController == null)
			{
				this.m_UIGroupController = base.GetComponent<UIGroupController>();
			}
			if (this.m_UIGroupController != null)
			{
				this.m_UIGroupController.SetEnableInput(flg);
			}
		}

		// Token: 0x06002DAB RID: 11691 RVA: 0x000CA7F4 File Offset: 0x000C8BF4
		public Vector2 WorldToUIPosition(Vector3 worldPoint)
		{
			Vector2 rawPos = RectTransformUtility.WorldToScreenPoint(Camera.main, worldPoint);
			return this.RawScreenPositionToUICameraPosition(rawPos);
		}

		// Token: 0x06002DAC RID: 11692 RVA: 0x000CA814 File Offset: 0x000C8C14
		protected Vector2 RawScreenPositionToUICameraPosition(Vector2 rawPos)
		{
			if (this.m_UICanvasScaler == null)
			{
				this.m_UICanvasScaler = base.GetComponent<CanvasScaler>();
			}
			Vector2 b = UIUtility.GetScaledScreenSize(this.m_UICanvasScaler) / 2f;
			return UIUtility.RawScreenPositionToScaledScreenPosition(this.m_UICanvasScaler, rawPos) - b;
		}

		// Token: 0x040034B3 RID: 13491
		protected CanvasScaler m_UICanvasScaler;

		// Token: 0x040034B4 RID: 13492
		private GraphicRaycaster m_RayCaster;

		// Token: 0x040034B5 RID: 13493
		protected UIGroupController m_UIGroupController;

		// Token: 0x040034B6 RID: 13494
		protected int m_State;

		// Token: 0x040034B7 RID: 13495
		protected GameObject m_GameObject;
	}
}
