﻿using System;

namespace Star.UI
{
	// Token: 0x02000955 RID: 2389
	public abstract class EnumerationCharaPanelAdapterExGen<ParentType> : EnumerationCharaPanelAdapter where ParentType : EnumerationPanelBase
	{
		// Token: 0x06003194 RID: 12692 RVA: 0x000FBF9A File Offset: 0x000FA39A
		public virtual void Setup(ParentType parent)
		{
			this.m_Core = this.CreateCore();
			this.m_Parent = parent;
			this.m_SharedInstance = this.CreateArgument();
			this.m_Core.Setup(this.m_SharedInstance);
		}

		// Token: 0x06003195 RID: 12693 RVA: 0x000FBFCC File Offset: 0x000FA3CC
		protected override EnumerationPanelBase GetParent()
		{
			return this.m_Parent;
		}

		// Token: 0x040037FB RID: 14331
		protected ParentType m_Parent;
	}
}
