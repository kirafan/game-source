﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000872 RID: 2162
	public class SkillIcon : MonoBehaviour
	{
		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06002CC8 RID: 11464 RVA: 0x000EC215 File Offset: 0x000EA615
		// (set) Token: 0x06002CC9 RID: 11465 RVA: 0x000EC21D File Offset: 0x000EA61D
		public bool DefaultBlack { get; set; }

		// Token: 0x06002CCA RID: 11466 RVA: 0x000EC228 File Offset: 0x000EA628
		public void Apply(SkillIcon.eSkillDBType skillDBType, int skillID, eElementType ownerElement)
		{
			this.m_SkillIcon.gameObject.SetActive(true);
			SkillListDB_Param skillListDB_Param;
			switch (skillDBType)
			{
			case SkillIcon.eSkillDBType.Player:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Enemy:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_EN.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Master:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Weapon:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_WPN.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Card:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_CARD.GetParam(skillID);
				break;
			default:
				skillListDB_Param = default(SkillListDB_Param);
				break;
			}
			if (!string.IsNullOrEmpty(skillListDB_Param.m_UniqueSkillVoiceCueSheet))
			{
				this.m_SkillIcon.sprite = this.m_UniqueSprite;
			}
			else
			{
				Color col = Color.white;
				if (this.DefaultBlack)
				{
					col = Color.black;
				}
				switch (skillListDB_Param.m_SkillType)
				{
				case 3:
					this.m_SkillIcon.sprite = this.m_AtkSprite;
					col = UIUtility.GetIconColor(SkillIcon.GetSkillElement(skillID, skillDBType, ownerElement)[0]);
					break;
				case 4:
					this.m_SkillIcon.sprite = this.m_MgcSprite;
					col = UIUtility.GetIconColor(SkillIcon.GetSkillElement(skillID, skillDBType, ownerElement)[0]);
					break;
				case 5:
					this.m_SkillIcon.sprite = this.m_RecoverSprite;
					break;
				case 6:
					this.m_SkillIcon.sprite = this.m_BuffSprite;
					break;
				case 7:
					this.m_SkillIcon.sprite = this.m_DebuffSprite;
					break;
				}
				this.m_ColorGroup.ChangeColor(col, ColorGroup.eColorBlendMode.Mul, 1f);
			}
		}

		// Token: 0x06002CCB RID: 11467 RVA: 0x000EC408 File Offset: 0x000EA808
		public static List<eElementType> GetSkillElement(int skillID, SkillIcon.eSkillDBType skillDBType, eElementType ownerType)
		{
			List<eElementType> list = new List<eElementType>();
			SkillListDB_Param skillListDB_Param;
			SkillContentListDB_Param skillContentListDB_Param;
			switch (skillDBType)
			{
			case SkillIcon.eSkillDBType.Player:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_PL.GetParam(skillID);
				skillContentListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_PL.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Enemy:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_EN.GetParam(skillID);
				skillContentListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_EN.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Master:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_MST.GetParam(skillID);
				skillContentListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_MST.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Weapon:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_WPN.GetParam(skillID);
				skillContentListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_WPN.GetParam(skillID);
				break;
			case SkillIcon.eSkillDBType.Card:
				skillListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillListDB_CARD.GetParam(skillID);
				skillContentListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillContentListDB_CARD.GetParam(skillID);
				break;
			default:
				skillListDB_Param = default(SkillListDB_Param);
				skillContentListDB_Param = default(SkillContentListDB_Param);
				break;
			}
			switch (skillListDB_Param.m_SkillType)
			{
			case 0:
			case 1:
			case 3:
			case 4:
				for (int i = 0; i < skillContentListDB_Param.m_Datas.Length; i++)
				{
					if (skillContentListDB_Param.m_Datas[i].m_Type == 0)
					{
						for (eElementType eElementType = eElementType.Fire; eElementType < eElementType.Num; eElementType++)
						{
							if (skillContentListDB_Param.m_Datas[i].m_Args[(int)(eElementType + 2)] != 0f)
							{
								list.Add(eElementType);
							}
						}
						if (list.Count <= 0)
						{
							list.Add(ownerType);
						}
						break;
					}
				}
				break;
			}
			if (list.Count <= 0)
			{
				list.Add(eElementType.None);
			}
			return list;
		}

		// Token: 0x040033BA RID: 13242
		[SerializeField]
		private Image m_SkillIcon;

		// Token: 0x040033BB RID: 13243
		[SerializeField]
		private Sprite m_UniqueSprite;

		// Token: 0x040033BC RID: 13244
		[SerializeField]
		private Sprite m_AtkSprite;

		// Token: 0x040033BD RID: 13245
		[SerializeField]
		private Sprite m_MgcSprite;

		// Token: 0x040033BE RID: 13246
		[SerializeField]
		private Sprite m_RecoverSprite;

		// Token: 0x040033BF RID: 13247
		[SerializeField]
		private Sprite m_BuffSprite;

		// Token: 0x040033C0 RID: 13248
		[SerializeField]
		private Sprite m_DebuffSprite;

		// Token: 0x040033C1 RID: 13249
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x02000873 RID: 2163
		public enum eSkillDBType
		{
			// Token: 0x040033C4 RID: 13252
			Player,
			// Token: 0x040033C5 RID: 13253
			Enemy,
			// Token: 0x040033C6 RID: 13254
			Master,
			// Token: 0x040033C7 RID: 13255
			Weapon,
			// Token: 0x040033C8 RID: 13256
			Card
		}
	}
}
