﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000B18 RID: 2840
	public class TutorialTipsUIItemData : InfiniteScrollItemData
	{
		// Token: 0x06003BB5 RID: 15285 RVA: 0x00130DA6 File Offset: 0x0012F1A6
		public TutorialTipsUIItemData(Text destTitleText, string titleText, Sprite imageSprite, string message)
		{
			this.m_DestTitleText = destTitleText;
			this.m_TitleText = titleText;
			this.m_ImageSprite = imageSprite;
			this.m_Message = message;
		}

		// Token: 0x06003BB6 RID: 15286 RVA: 0x00130DCB File Offset: 0x0012F1CB
		public TutorialTipsUIItemData()
		{
		}

		// Token: 0x06003BB7 RID: 15287 RVA: 0x00130DD3 File Offset: 0x0012F1D3
		public void Destroy()
		{
			this.m_ImageSprite = null;
		}

		// Token: 0x0400434D RID: 17229
		public Text m_DestTitleText;

		// Token: 0x0400434E RID: 17230
		public string m_TitleText;

		// Token: 0x0400434F RID: 17231
		public Sprite m_ImageSprite;

		// Token: 0x04004350 RID: 17232
		public string m_Message;
	}
}
