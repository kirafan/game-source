﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007DD RID: 2013
	public class SelectedCharaInfoLevelInfo : MonoBehaviour
	{
		// Token: 0x0600297E RID: 10622 RVA: 0x000DBCA8 File Offset: 0x000DA0A8
		public void Setup()
		{
			this.m_Frames = 0;
		}

		// Token: 0x0600297F RID: 10623 RVA: 0x000DBCB4 File Offset: 0x000DA0B4
		private void Update()
		{
			if (0 < this.m_Frames)
			{
				RectTransform rectTransform = base.transform as RectTransform;
				for (int i = 0; i < 0; i++)
				{
					rectTransform = (rectTransform.parent as RectTransform);
				}
				LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
				this.m_Frames--;
			}
		}

		// Token: 0x06002980 RID: 10624 RVA: 0x000DBD10 File Offset: 0x000DA110
		public void SetLevel(int nowLevel, int maxLevel, SelectedCharaInfoLevelInfo.EmphasisExpressionInfo info = null)
		{
			if (info == null)
			{
				info = new SelectedCharaInfoLevelInfo.EmphasisExpressionInfo();
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(info.now) + nowLevel.ToString().PadLeft(3) + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(info.now));
			stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(info.slash) + " / " + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(info.slash));
			stringBuilder.Append(SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagStartString(info.max) + maxLevel.ToString().PadLeft(3) + SelectedCharaInfoLevelInfo.EmphasisExpressionInfo.eEmphasisExpressionTypeToTagEndString(info.max));
			this.m_LevelText.text = stringBuilder.ToString();
			this.m_Frames = 30;
		}

		// Token: 0x06002981 RID: 10625 RVA: 0x000DBDE1 File Offset: 0x000DA1E1
		public void PlayAnimation()
		{
			if (this.m_LevelTextAnim != null)
			{
				this.m_LevelTextAnim.PlayIn();
			}
		}

		// Token: 0x06002982 RID: 10626 RVA: 0x000DBDFF File Offset: 0x000DA1FF
		public int GetAnimationState()
		{
			if (this.m_LevelTextAnim != null)
			{
				return this.m_LevelTextAnim.State;
			}
			return -1;
		}

		// Token: 0x06002983 RID: 10627 RVA: 0x000DBE1F File Offset: 0x000DA21F
		public void SetLimitBreakIconValue(int value)
		{
			if (this.m_LimitBreakIcon != null)
			{
				this.m_LimitBreakIcon.SetValue(value);
			}
		}

		// Token: 0x06002984 RID: 10628 RVA: 0x000DBE3E File Offset: 0x000DA23E
		public Text GetLevelText()
		{
			return this.m_LevelText;
		}

		// Token: 0x04003009 RID: 12297
		public const int REBUILD_FRAMES = 30;

		// Token: 0x0400300A RID: 12298
		[SerializeField]
		private LimitBreakIcon m_LimitBreakIcon;

		// Token: 0x0400300B RID: 12299
		[SerializeField]
		private Text m_LevelText;

		// Token: 0x0400300C RID: 12300
		[SerializeField]
		private AnimUIPlayer m_LevelTextAnim;

		// Token: 0x0400300D RID: 12301
		private int m_Frames;

		// Token: 0x020007DE RID: 2014
		public enum eEmphasisExpressionType
		{
			// Token: 0x0400300F RID: 12303
			Normal,
			// Token: 0x04003010 RID: 12304
			Up,
			// Token: 0x04003011 RID: 12305
			Down
		}

		// Token: 0x020007DF RID: 2015
		public class EmphasisExpressionInfo
		{
			// Token: 0x06002985 RID: 10629 RVA: 0x000DBE46 File Offset: 0x000DA246
			public EmphasisExpressionInfo()
			{
				this.now = SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal;
				this.slash = SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal;
				this.max = SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Normal;
			}

			// Token: 0x06002986 RID: 10630 RVA: 0x000DBE63 File Offset: 0x000DA263
			public static string eEmphasisExpressionTypeToTagStartString(SelectedCharaInfoLevelInfo.eEmphasisExpressionType tag)
			{
				if (tag == SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
				{
					return "<color=#00c882>";
				}
				if (tag == SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down)
				{
					return "<color=#ff4141>";
				}
				return string.Empty;
			}

			// Token: 0x06002987 RID: 10631 RVA: 0x000DBE84 File Offset: 0x000DA284
			public static string eEmphasisExpressionTypeToTagEndString(SelectedCharaInfoLevelInfo.eEmphasisExpressionType tag)
			{
				if (tag == SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Up)
				{
					return "</color>";
				}
				if (tag == SelectedCharaInfoLevelInfo.eEmphasisExpressionType.Down)
				{
					return "</color>";
				}
				return string.Empty;
			}

			// Token: 0x04003012 RID: 12306
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType now;

			// Token: 0x04003013 RID: 12307
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType slash;

			// Token: 0x04003014 RID: 12308
			public SelectedCharaInfoLevelInfo.eEmphasisExpressionType max;
		}
	}
}
