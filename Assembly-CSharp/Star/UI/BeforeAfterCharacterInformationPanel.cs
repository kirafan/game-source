﻿using System;

namespace Star.UI
{
	// Token: 0x020007AC RID: 1964
	public class BeforeAfterCharacterInformationPanel : CharacterInformationPanel
	{
		// Token: 0x0600281C RID: 10268 RVA: 0x000D8BB2 File Offset: 0x000D6FB2
		public void Apply(CharacterDataController cdc, CharacterDataController after)
		{
			if (this.m_IsDirtyResource)
			{
				this.ApplyResource(cdc, after);
			}
			if (this.m_IsDirtyParameter)
			{
				this.ApplyParameter(cdc, after);
			}
			this.ResetDirtyFlag();
		}

		// Token: 0x0600281D RID: 10269 RVA: 0x000D8BE0 File Offset: 0x000D6FE0
		protected virtual void ApplyResource(CharacterDataController cdc, CharacterDataController after)
		{
		}

		// Token: 0x0600281E RID: 10270 RVA: 0x000D8BE2 File Offset: 0x000D6FE2
		protected virtual void ApplyParameter(CharacterDataController cdc, CharacterDataController after)
		{
		}
	}
}
