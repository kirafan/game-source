﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000809 RID: 2057
	public class Badge : MonoBehaviour
	{
		// Token: 0x06002A96 RID: 10902 RVA: 0x000E1360 File Offset: 0x000DF760
		public void Apply(int value)
		{
			if (value <= 0)
			{
				base.gameObject.SetActive(false);
			}
			else
			{
				base.gameObject.SetActive(true);
			}
			if (value > 99)
			{
				this.m_Text.text = "+" + 99.ToString();
			}
			else
			{
				this.m_Text.text = value.ToString();
			}
		}

		// Token: 0x040030FF RID: 12543
		public const int MAX_VALUE = 99;

		// Token: 0x04003100 RID: 12544
		[SerializeField]
		private Text m_Text;
	}
}
