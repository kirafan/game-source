﻿using System;

namespace Star.UI
{
	// Token: 0x0200086F RID: 2159
	public class QuestGroupIcon : ASyncImage
	{
		// Token: 0x06002CBF RID: 11455 RVA: 0x000EC0BC File Offset: 0x000EA4BC
		public void Apply(int groupID)
		{
			if (this.m_GroupID != groupID)
			{
				this.Apply();
				if (groupID != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncQuestGroupIcon(groupID);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_GroupID = groupID;
			}
		}

		// Token: 0x06002CC0 RID: 11456 RVA: 0x000EC122 File Offset: 0x000EA522
		public override void Destroy()
		{
			base.Destroy();
			this.m_GroupID = -1;
		}

		// Token: 0x040033B6 RID: 13238
		protected int m_GroupID = -1;
	}
}
