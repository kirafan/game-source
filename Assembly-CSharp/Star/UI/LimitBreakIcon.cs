﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000866 RID: 2150
	public class LimitBreakIcon : MonoBehaviour
	{
		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06002CA2 RID: 11426 RVA: 0x000EBA4D File Offset: 0x000E9E4D
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x06002CA3 RID: 11427 RVA: 0x000EBA74 File Offset: 0x000E9E74
		public void SetValue(int value)
		{
			for (int i = 0; i < this.m_IconImageObjs.Length; i++)
			{
				this.m_IconImageObjs[i].SetActive(i < value);
			}
		}

		// Token: 0x0400339F RID: 13215
		[SerializeField]
		private GameObject[] m_IconImageObjs;

		// Token: 0x040033A0 RID: 13216
		private GameObject m_GameObject;
	}
}
