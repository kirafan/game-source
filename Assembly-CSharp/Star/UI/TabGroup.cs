﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008AB RID: 2219
	public class TabGroup : MonoBehaviour
	{
		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06002E01 RID: 11777 RVA: 0x000F2203 File Offset: 0x000F0603
		// (set) Token: 0x06002E02 RID: 11778 RVA: 0x000F220B File Offset: 0x000F060B
		public int SelectIdx
		{
			get
			{
				return this.m_SelectIdx;
			}
			set
			{
				this.ChangeIdx(value);
			}
		}

		// Token: 0x1400006E RID: 110
		// (add) Token: 0x06002E03 RID: 11779 RVA: 0x000F2214 File Offset: 0x000F0614
		// (remove) Token: 0x06002E04 RID: 11780 RVA: 0x000F224C File Offset: 0x000F064C
		public event Action<int> OnChangeIdx;

		// Token: 0x06002E05 RID: 11781 RVA: 0x000F2284 File Offset: 0x000F0684
		public void Setup(string[] buttonTexts, float buttonWidth = -1f)
		{
			HorizontalLayoutGroup component = this.m_ButtonParent.GetComponent<HorizontalLayoutGroup>();
			if (component != null)
			{
				component.enabled = false;
			}
			RectTransform component2 = this.m_ScrollRect.GetComponent<RectTransform>();
			component2.anchorMin = new Vector2(0f, 0f);
			component2.anchorMax = new Vector2(1f, 1f);
			component2.sizeDelta = Vector2.zero;
			this.m_Texts = buttonTexts;
			float num = 0f;
			for (int i = 0; i < this.m_Texts.Length; i++)
			{
				TabButton tabButton = UnityEngine.Object.Instantiate<TabButton>(this.m_TabButtonPrefab, this.m_ButtonParent, false);
				this.m_TabButtonList.Add(tabButton);
				tabButton.Setup(i, this.m_Texts[i]);
				tabButton.OnClick += this.OnClickTabButtonCallBack;
				Vector2 lowestSize = tabButton.GetLowestSize();
				if (num < lowestSize.x)
				{
					num = lowestSize.x;
				}
			}
			if (buttonWidth > 0f)
			{
				num = buttonWidth;
			}
			this.m_ButtonParent.sizeDelta = new Vector2(num * (float)this.m_TabButtonList.Count + 16f, this.m_ButtonParent.sizeDelta.y);
			this.m_TabButtonList[this.m_SelectIdx].Button.IsDecided = true;
			RectTransform component3 = this.m_ScrollRect.viewport.GetComponent<RectTransform>();
			component3.sizeDelta = new Vector2(-24f, component3.sizeDelta.y);
			component3.pivot = new Vector2(0.5f, 0f);
			component3.anchoredPosition = Vector3.zero;
			if (component3.rect.width < num * (float)this.m_TabButtonList.Count)
			{
				this.m_ScrollRect.horizontal = true;
				component3.sizeDelta = new Vector2(-112f, component3.sizeDelta.y);
				for (int j = 0; j < this.m_TabButtonList.Count; j++)
				{
					this.m_TabButtonList[j].RectTransform.pivot = new Vector2(0f, 0f);
					this.m_TabButtonList[j].SetWidth(num);
					this.m_TabButtonList[j].RectTransform.anchoredPosition = new Vector2(8f + (float)j * num, 0f);
				}
				if (this.m_LeftButton != null)
				{
					this.m_LeftButton.gameObject.SetActive(true);
				}
				if (this.m_RightButton != null)
				{
					this.m_RightButton.gameObject.SetActive(true);
				}
			}
			else
			{
				this.m_ScrollRect.horizontal = false;
				float num2 = (component3.rect.width - 16f) / (float)this.m_TabButtonList.Count;
				if (buttonWidth > 0f)
				{
					num2 = buttonWidth;
				}
				for (int k = 0; k < this.m_TabButtonList.Count; k++)
				{
					this.m_TabButtonList[k].RectTransform.pivot = new Vector2(0f, 0f);
					this.m_TabButtonList[k].SetWidth(num2);
					this.m_TabButtonList[k].RectTransform.anchoredPosition = new Vector2(8f + (float)k * num2, 0f);
				}
				if (this.m_LeftButton != null)
				{
					this.m_LeftButton.gameObject.SetActive(false);
				}
				if (this.m_RightButton != null)
				{
					this.m_RightButton.gameObject.SetActive(false);
				}
			}
			this.m_TabButtonList[this.m_SelectIdx].Button.IsDecided = true;
			this.m_TabButtonList[this.m_SelectIdx].RectTransform.SetAsLastSibling();
		}

		// Token: 0x06002E06 RID: 11782 RVA: 0x000F2694 File Offset: 0x000F0A94
		public void Setup(float buttonWidth = -1f)
		{
			HorizontalLayoutGroup component = this.m_ButtonParent.GetComponent<HorizontalLayoutGroup>();
			if (component != null)
			{
				component.enabled = false;
			}
			RectTransform component2 = this.m_ScrollRect.GetComponent<RectTransform>();
			component2.anchorMin = new Vector2(0f, 0f);
			component2.anchorMax = new Vector2(1f, 1f);
			component2.sizeDelta = Vector2.zero;
			this.m_Texts = null;
			float num = 0f;
			for (int i = 0; i < this.m_ButtonSprites.Length; i++)
			{
				TabButton tabButton = UnityEngine.Object.Instantiate<TabButton>(this.m_TabButtonPrefab, this.m_ButtonParent, false);
				this.m_TabButtonList.Add(tabButton);
				tabButton.Setup(i, this.m_ButtonSprites[i]);
				tabButton.OnClick += this.OnClickTabButtonCallBack;
				Vector2 lowestSize = tabButton.GetLowestSize();
				if (num < lowestSize.x)
				{
					num = lowestSize.x;
				}
			}
			if (buttonWidth > 0f)
			{
				num = buttonWidth;
			}
			LayoutGroup componentInParent = base.GetComponentInParent<LayoutGroup>();
			if (componentInParent != null)
			{
				RectTransform component3 = componentInParent.GetComponent<RectTransform>();
				if (component3 != null)
				{
					LayoutRebuilder.ForceRebuildLayoutImmediate(component3);
				}
			}
			RectTransform component4 = this.m_ScrollRect.viewport.GetComponent<RectTransform>();
			component4.sizeDelta = new Vector2(-24f, component4.sizeDelta.y);
			component4.pivot = new Vector2(0.5f, 0f);
			component4.anchoredPosition = Vector3.zero;
			if (component4.rect.width < num * (float)this.m_TabButtonList.Count)
			{
				this.m_ScrollRect.horizontal = true;
				component4.sizeDelta = new Vector2(-112f, component4.sizeDelta.y);
				for (int j = 0; j < this.m_TabButtonList.Count; j++)
				{
					this.m_TabButtonList[j].RectTransform.pivot = new Vector2(0f, 0f);
					this.m_TabButtonList[j].SetWidth(num);
					this.m_TabButtonList[j].RectTransform.anchoredPosition = new Vector2(8f + (float)j * num, 0f);
				}
				if (this.m_LeftButton != null)
				{
					this.m_LeftButton.gameObject.SetActive(true);
				}
				if (this.m_RightButton != null)
				{
					this.m_RightButton.gameObject.SetActive(true);
				}
			}
			else
			{
				this.m_ScrollRect.horizontal = false;
				float num2 = (component4.rect.width - 16f) / (float)this.m_TabButtonList.Count;
				if (buttonWidth > 0f)
				{
					num2 = buttonWidth;
				}
				for (int k = 0; k < this.m_TabButtonList.Count; k++)
				{
					this.m_TabButtonList[k].RectTransform.pivot = new Vector2(0f, 0f);
					this.m_TabButtonList[k].SetWidth(num2);
					this.m_TabButtonList[k].RectTransform.anchoredPosition = new Vector2(8f + (float)k * num2, 0f);
				}
				if (this.m_LeftButton != null)
				{
					this.m_LeftButton.gameObject.SetActive(false);
				}
				if (this.m_RightButton != null)
				{
					this.m_RightButton.gameObject.SetActive(false);
				}
			}
			this.m_TabButtonList[this.m_SelectIdx].Button.IsDecided = true;
			this.m_TabButtonList[this.m_SelectIdx].RectTransform.SetAsLastSibling();
		}

		// Token: 0x06002E07 RID: 11783 RVA: 0x000F2A81 File Offset: 0x000F0E81
		public void SetIntaractable(bool flg, int idx)
		{
			this.m_TabButtonList[idx].Button.Interactable = flg;
		}

		// Token: 0x06002E08 RID: 11784 RVA: 0x000F2A9A File Offset: 0x000F0E9A
		public void SetButtonActive(bool flg, int idx)
		{
			this.m_TabButtonList[idx].gameObject.SetActive(flg);
		}

		// Token: 0x06002E09 RID: 11785 RVA: 0x000F2AB3 File Offset: 0x000F0EB3
		public int GetButtonNum()
		{
			return this.m_TabButtonList.Count;
		}

		// Token: 0x06002E0A RID: 11786 RVA: 0x000F2AC0 File Offset: 0x000F0EC0
		public TabButton GetButton(int idx)
		{
			return this.m_TabButtonList[idx];
		}

		// Token: 0x06002E0B RID: 11787 RVA: 0x000F2ACE File Offset: 0x000F0ECE
		private void OnClickTabButtonCallBack(int idx)
		{
			this.ChangeIdx(idx);
		}

		// Token: 0x06002E0C RID: 11788 RVA: 0x000F2AD8 File Offset: 0x000F0ED8
		public void ChangeIdx(int idx)
		{
			if (idx < 0 || idx >= this.m_TabButtonList.Count)
			{
				return;
			}
			if (idx == this.m_SelectIdx)
			{
				return;
			}
			this.m_TabButtonList[this.m_SelectIdx].Button.IsDecided = false;
			this.m_TabButtonList[this.m_SelectIdx].Button.IsEnableInput = true;
			this.m_SelectIdx = idx;
			this.m_TabButtonList[this.m_SelectIdx].Button.IsDecided = true;
			this.m_TabButtonList[this.m_SelectIdx].Button.IsEnableInput = false;
			this.m_TabButtonList[this.m_SelectIdx].RectTransform.SetAsLastSibling();
			if (this.m_ScrollRect.horizontal)
			{
				RectTransform rectTransform = this.m_TabButtonList[this.m_SelectIdx].RectTransform;
				float horizontalNormalizedPosition = Mathf.Clamp01((rectTransform.localPosition.x - this.m_ButtonParent.rect.width * 0.5f) / (this.m_ButtonParent.rect.width - this.m_ScrollRect.viewport.rect.width) + 0.5f);
				this.m_ScrollRect.horizontalNormalizedPosition = horizontalNormalizedPosition;
			}
			if (this.m_OnChangeIdx != null)
			{
				this.m_OnChangeIdx.Invoke();
			}
			this.OnChangeIdx.Call(idx);
			if (this.m_LeftButton != null)
			{
				if (this.m_SelectIdx <= 0)
				{
					this.m_LeftButton.Interactable = false;
				}
				else
				{
					this.m_LeftButton.Interactable = true;
				}
			}
			if (this.m_RightButton != null)
			{
				if (this.m_SelectIdx >= this.m_TabButtonList.Count - 1)
				{
					this.m_RightButton.Interactable = false;
				}
				else
				{
					this.m_RightButton.Interactable = true;
				}
			}
		}

		// Token: 0x06002E0D RID: 11789 RVA: 0x000F2CD5 File Offset: 0x000F10D5
		public void OnClickRightButtonCallBack()
		{
			this.ChangeIdx(this.m_SelectIdx + 1);
		}

		// Token: 0x06002E0E RID: 11790 RVA: 0x000F2CE5 File Offset: 0x000F10E5
		public void OnClickLeftButtonCallBack()
		{
			this.ChangeIdx(this.m_SelectIdx - 1);
		}

		// Token: 0x0400350F RID: 13583
		private const float SCROLL_SIZEDELTA = -24f;

		// Token: 0x04003510 RID: 13584
		private const float SCROLL_SIZEDELTA_SCROLLMODE = -112f;

		// Token: 0x04003511 RID: 13585
		private const float MARGIN = 8f;

		// Token: 0x04003512 RID: 13586
		[SerializeField]
		private TabButton m_TabButtonPrefab;

		// Token: 0x04003513 RID: 13587
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x04003514 RID: 13588
		[SerializeField]
		private ScrollRect m_ScrollRect;

		// Token: 0x04003515 RID: 13589
		[SerializeField]
		private CustomButton m_LeftButton;

		// Token: 0x04003516 RID: 13590
		[SerializeField]
		private CustomButton m_RightButton;

		// Token: 0x04003517 RID: 13591
		[SerializeField]
		private Sprite[] m_ButtonSprites;

		// Token: 0x04003518 RID: 13592
		[SerializeField]
		public UnityEvent m_OnChangeIdx;

		// Token: 0x04003519 RID: 13593
		private string[] m_Texts;

		// Token: 0x0400351A RID: 13594
		private List<TabButton> m_TabButtonList = new List<TabButton>();

		// Token: 0x0400351B RID: 13595
		private int m_SelectIdx;
	}
}
