﻿using System;
using Star.UI.WeaponList;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000848 RID: 2120
	public class WeaponListFilterSortController : MonoBehaviour
	{
		// Token: 0x06002C2D RID: 11309 RVA: 0x000E9580 File Offset: 0x000E7980
		private void LateUpdate()
		{
			if (this.m_HaveNumText != null)
			{
				this.m_HaveNumText.text = this.m_WeaponScroll.FilteredNum.ToString();
			}
		}

		// Token: 0x06002C2E RID: 11310 RVA: 0x000E95C4 File Offset: 0x000E79C4
		public void OpenFilterWindow()
		{
			if (this.m_WeaponScroll.IsEquipMode())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.WeaponEquip, new Action(this.OnExecuteFilter)), null, null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.WeaponList, new Action(this.OnExecuteFilter)), null, null);
			}
		}

		// Token: 0x06002C2F RID: 11311 RVA: 0x000E962E File Offset: 0x000E7A2E
		public void OnExecuteFilter()
		{
			this.m_WeaponScroll.DoFilter();
			this.m_WeaponScroll.Refresh();
		}

		// Token: 0x06002C30 RID: 11312 RVA: 0x000E9646 File Offset: 0x000E7A46
		public void OpenSortWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Sort, new SortUIArg(UISettings.eUsingType_Sort.WeaponList, new Action(this.OnExecuteSort)), null, null);
		}

		// Token: 0x06002C31 RID: 11313 RVA: 0x000E966C File Offset: 0x000E7A6C
		public void OnExecuteSort()
		{
			this.m_WeaponScroll.DoSort();
			this.m_WeaponScroll.Refresh();
		}

		// Token: 0x040032FE RID: 13054
		[SerializeField]
		private WeaponScroll m_WeaponScroll;

		// Token: 0x040032FF RID: 13055
		[SerializeField]
		private Text m_HaveNumText;
	}
}
