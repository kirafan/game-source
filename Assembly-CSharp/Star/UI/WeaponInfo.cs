﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000829 RID: 2089
	public class WeaponInfo : MonoBehaviour
	{
		// Token: 0x06002B91 RID: 11153 RVA: 0x000E4F40 File Offset: 0x000E3340
		public void SetCharaCost(int cost)
		{
			this.m_CharaCost = cost;
		}

		// Token: 0x06002B92 RID: 11154 RVA: 0x000E4F4C File Offset: 0x000E334C
		public void Apply(int weaponID)
		{
			if (this.m_Exist != null && !this.m_Exist.activeSelf)
			{
				this.m_Exist.SetActive(true);
			}
			if (this.m_NoExist != null && this.m_NoExist.activeSelf)
			{
				this.m_NoExist.SetActive(false);
			}
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponID);
			this.m_NameText.text = param.m_WeaponName;
			if (this.m_ClassIcon != null)
			{
				this.m_ClassIcon.Apply((eClassType)param.m_ClassType);
			}
			if (this.m_ClassText != null)
			{
				this.m_ClassText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WeaponEquipClass, new object[]
				{
					SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + param.m_ClassType)
				});
			}
			if (this.m_LvText != null)
			{
				this.m_LvText.text = UIUtility.WeaponLvValueToString(1, param.m_LimitLv, 0, 0);
			}
			if (this.m_CostText != null)
			{
				this.m_CostText.text = param.m_Cost.ToString();
			}
			if (this.m_TotalCost != null)
			{
				if (this.m_CharaCost >= 0)
				{
					this.m_TotalCost.SetActive(true);
					StringBuilder stringBuilder = new StringBuilder();
					stringBuilder.Append(this.m_CharaCost);
					stringBuilder.Append("+");
					stringBuilder.Append(param.m_Cost);
					this.m_TotalCostText.text = stringBuilder.ToString();
				}
				else
				{
					this.m_TotalCost.SetActive(false);
				}
			}
			if (this.m_Rare != null)
			{
				this.m_Rare.Apply((eRare)param.m_Rare);
			}
			if (this.m_WeaponIconCloner != null)
			{
				this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>().SetMode(WeaponIconWithFrame.eMode.NakedBGOnly);
				this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>().Apply(weaponID);
			}
			if (this.m_WeaponIcon != null)
			{
				this.m_WeaponIcon.Apply(weaponID);
			}
			this.m_SkillInfo = this.m_SkillInfoPrefabClone.GetInst<SkillInfo>();
			if (-1 < param.m_SkillID)
			{
				this.m_SkillInfo.gameObject.SetActive(true);
				this.m_SkillInfo.Apply(SkillIcon.eSkillDBType.Weapon, param.m_SkillID, eElementType.None, 1, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetMaxLv(param.m_SkillExpTableID), -1L);
				if (this.m_NoSkillExist != null)
				{
					this.m_NoSkillExist.SetActive(false);
				}
			}
			else
			{
				if (this.m_NoSkillExist != null)
				{
					this.m_NoSkillExist.SetActive(true);
				}
				this.m_SkillInfo.gameObject.SetActive(true);
				this.m_SkillInfo.ApplyEmpty();
			}
			if (this.m_AtkText != null)
			{
				this.m_AtkText.text = param.m_InitAtk.ToString().PadLeft(6);
				this.m_MgcText.text = param.m_InitMgc.ToString().PadLeft(6);
				this.m_DefText.text = param.m_InitDef.ToString().PadLeft(6);
				this.m_MDefText.text = param.m_InitMDef.ToString().PadLeft(6);
			}
		}

		// Token: 0x06002B93 RID: 11155 RVA: 0x000E52F0 File Offset: 0x000E36F0
		public void Apply(UserWeaponData weaponData)
		{
			this.Apply(weaponData.Param.WeaponID);
			if (weaponData.Param.Lv > 0)
			{
				this.m_LvText.text = UIUtility.WeaponLvValueToString(weaponData.Param.Lv, weaponData.Param.MaxLv, 0, 0);
				if (this.m_ExpGauge != null)
				{
					sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(weaponData.Param.WeaponID).m_ExpTableID;
					this.m_ExpGauge.SetExpData(weaponData.Param.Lv, EditUtility.GetWeaponNowExp(weaponData.Param.WeaponID, weaponData.Param.Lv, weaponData.Param.Exp), weaponData.Param.MaxLv, (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB.GetNextExp(weaponData.Param.Lv, expTableID));
				}
			}
			WeaponParam param = weaponData.Param;
			this.m_SkillInfo = this.m_SkillInfoPrefabClone.GetInst<SkillInfo>();
			this.m_SkillInfo.Apply(SkillIcon.eSkillDBType.Weapon, param.SkillLearnData.SkillID, eElementType.None, param.SkillLearnData.SkillLv, param.SkillLearnData.SkillMaxLv, (long)EditUtility.GetSkillNowRemainExp(param.SkillLearnData.SkillLv, param.SkillLearnData.TotalUseNum, param.SkillLearnData.ExpTableID));
			if (this.m_AtkText != null)
			{
				this.m_AtkText.text = weaponData.Param.Atk.ToString().PadLeft(6);
				this.m_MgcText.text = weaponData.Param.Mgc.ToString().PadLeft(6);
				this.m_DefText.text = weaponData.Param.Def.ToString().PadLeft(6);
				this.m_MDefText.text = weaponData.Param.MDef.ToString().PadLeft(6);
			}
		}

		// Token: 0x06002B94 RID: 11156 RVA: 0x000E5514 File Offset: 0x000E3914
		public void Apply(UserSupportData userSupportData)
		{
			this.Apply(userSupportData.WeaponID);
			WeaponListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userSupportData.WeaponID);
			int weaponLv = userSupportData.WeaponLv;
			if (this.m_LvText != null && weaponLv > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(weaponLv.ToString());
				stringBuilder.Append(" / ");
				stringBuilder.Append(param.m_LimitLv.ToString());
				this.m_LvText.text = UIUtility.WeaponLvValueToString(weaponLv, param.m_LimitLv, 0, 0);
				if (this.m_ExpGauge != null)
				{
					sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userSupportData.WeaponID).m_ExpTableID;
					this.m_ExpGauge.SetExpData(weaponLv, 0L, param.m_LimitLv, (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB.GetNextExp(weaponLv, expTableID));
				}
			}
			WeaponListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(userSupportData.WeaponID);
			int skillID = param2.m_SkillID;
			sbyte skillExpTableID = param2.m_SkillExpTableID;
			int maxLv = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetMaxLv(skillExpTableID);
			this.m_SkillInfo = this.m_SkillInfoPrefabClone.GetInst<SkillInfo>();
			this.m_SkillInfo.Apply(SkillIcon.eSkillDBType.Weapon, skillID, eElementType.None, userSupportData.WeaponSkillLv, maxLv, (long)EditUtility.GetSkillNowRemainExp(userSupportData.WeaponSkillLv, 0L, skillExpTableID));
			if (this.m_AtkText != null)
			{
				int num = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, weaponLv);
				this.m_AtkText.text = num.ToString().PadLeft(6);
				int num2 = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, weaponLv);
				this.m_MgcText.text = num2.ToString().PadLeft(6);
				int num3 = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, weaponLv);
				this.m_DefText.text = num3.ToString().PadLeft(6);
				int num4 = EditUtility.CalcParamCorrectLv(param.m_InitAtk, param.m_MaxAtk, 1, param.m_LimitLv, weaponLv);
				this.m_MDefText.text = num4.ToString().PadLeft(6);
			}
		}

		// Token: 0x06002B95 RID: 11157 RVA: 0x000E57A0 File Offset: 0x000E3BA0
		public void Apply(EquipWeaponCharacterDataController cdc)
		{
			this.Apply(cdc.GetWeaponId());
			if (cdc.GetWeaponLv() > 0)
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(cdc.GetWeaponLv().ToString());
				stringBuilder.Append(" / ");
				stringBuilder.Append(cdc.GetWeaponMaxLv().ToString());
				this.m_LvText.text = stringBuilder.ToString();
				if (this.m_ExpGauge != null)
				{
					sbyte expTableID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(cdc.GetWeaponId()).m_ExpTableID;
					this.m_ExpGauge.SetExpData(cdc.GetWeaponLv(), cdc.GetWeaponExp(), cdc.GetWeaponMaxLv(), (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponExpDB.GetNextExp(cdc.GetWeaponLv(), expTableID));
				}
			}
			this.m_SkillInfo = this.m_SkillInfoPrefabClone.GetInst<SkillInfo>();
			this.m_SkillInfo.Apply(SkillIcon.eSkillDBType.Weapon, cdc.GetWeaponSkillID(), eElementType.None, cdc.GetWeaponSkillLv(), cdc.GetWeaponSkillMaxLv(), (long)EditUtility.GetSkillNowRemainExp(cdc.GetWeaponSkillLv(), 0L, cdc.GetWeaponSkillExpTableID()));
			if (this.m_AtkText != null)
			{
				this.m_AtkText.text = cdc.GetWeaponParam().Atk.ToString().PadLeft(6);
				this.m_MgcText.text = cdc.GetWeaponParam().Mgc.ToString().PadLeft(6);
				this.m_DefText.text = cdc.GetWeaponParam().Def.ToString().PadLeft(6);
				this.m_MDefText.text = cdc.GetWeaponParam().MDef.ToString().PadLeft(6);
			}
		}

		// Token: 0x06002B96 RID: 11158 RVA: 0x000E59A0 File Offset: 0x000E3DA0
		public void ApplyEmpty()
		{
			if (this.m_Exist != null && this.m_Exist.activeSelf)
			{
				this.m_Exist.SetActive(false);
			}
			if (this.m_NoExist != null && !this.m_NoExist.activeSelf)
			{
				this.m_NoExist.SetActive(true);
			}
		}

		// Token: 0x06002B97 RID: 11159 RVA: 0x000E5A08 File Offset: 0x000E3E08
		public void ApplyNoEquip()
		{
			this.ApplyEmpty();
			if (this.m_WeaponIconCloner != null)
			{
				this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>().SetMode(WeaponIconWithFrame.eMode.NakedBGOnly);
				this.m_WeaponIconCloner.GetInst<WeaponIconWithFrame>().Apply(-1);
			}
			if (this.m_WeaponIcon != null)
			{
				this.m_WeaponIcon.Apply(-1);
			}
			this.m_SkillInfo = this.m_SkillInfoPrefabClone.GetInst<SkillInfo>();
			this.m_SkillInfo.gameObject.SetActive(false);
			if (this.m_NoSkillExist != null)
			{
				this.m_NoSkillExist.SetActive(true);
			}
		}

		// Token: 0x040031EF RID: 12783
		[SerializeField]
		private Text m_NameText;

		// Token: 0x040031F0 RID: 12784
		[SerializeField]
		private PrefabCloner m_WeaponIconCloner;

		// Token: 0x040031F1 RID: 12785
		[SerializeField]
		[Tooltip("直接WeaponIconを配置する場合")]
		private WeaponIcon m_WeaponIcon;

		// Token: 0x040031F2 RID: 12786
		[SerializeField]
		private Text m_LvText;

		// Token: 0x040031F3 RID: 12787
		[SerializeField]
		private ClassIcon m_ClassIcon;

		// Token: 0x040031F4 RID: 12788
		[SerializeField]
		[Tooltip("～専用ぶき")]
		private Text m_ClassText;

		// Token: 0x040031F5 RID: 12789
		[SerializeField]
		private Text m_CostText;

		// Token: 0x040031F6 RID: 12790
		[SerializeField]
		private GameObject m_TotalCost;

		// Token: 0x040031F7 RID: 12791
		[SerializeField]
		private Text m_TotalCostText;

		// Token: 0x040031F8 RID: 12792
		[SerializeField]
		private RareStar m_Rare;

		// Token: 0x040031F9 RID: 12793
		[SerializeField]
		private ExpGauge m_ExpGauge;

		// Token: 0x040031FA RID: 12794
		[SerializeField]
		private Text m_AtkText;

		// Token: 0x040031FB RID: 12795
		[SerializeField]
		private Text m_MgcText;

		// Token: 0x040031FC RID: 12796
		[SerializeField]
		private Text m_DefText;

		// Token: 0x040031FD RID: 12797
		[SerializeField]
		private Text m_MDefText;

		// Token: 0x040031FE RID: 12798
		[SerializeField]
		private PrefabCloner m_SkillInfoPrefabClone;

		// Token: 0x040031FF RID: 12799
		private SkillInfo m_SkillInfo;

		// Token: 0x04003200 RID: 12800
		[SerializeField]
		private GameObject m_Exist;

		// Token: 0x04003201 RID: 12801
		[SerializeField]
		private GameObject m_NoExist;

		// Token: 0x04003202 RID: 12802
		[SerializeField]
		private GameObject m_NoSkillExist;

		// Token: 0x04003203 RID: 12803
		private int m_CharaCost = -1;
	}
}
