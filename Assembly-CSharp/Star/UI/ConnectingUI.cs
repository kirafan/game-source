﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008D6 RID: 2262
	public class ConnectingUI : MonoBehaviour
	{
		// Token: 0x06002EE1 RID: 12001 RVA: 0x000F559E File Offset: 0x000F399E
		private void Start()
		{
			this.m_ConnectingIcon.Setup();
			this.m_Destroyed = false;
			this.m_Step = ConnectingUI.eStep.None;
		}

		// Token: 0x06002EE2 RID: 12002 RVA: 0x000F55B9 File Offset: 0x000F39B9
		public void OnDestroy()
		{
			if (!this.m_Destroyed)
			{
				this.Destroy();
			}
		}

		// Token: 0x06002EE3 RID: 12003 RVA: 0x000F55CC File Offset: 0x000F39CC
		public void Destroy()
		{
			this.m_Destroyed = true;
		}

		// Token: 0x06002EE4 RID: 12004 RVA: 0x000F55D5 File Offset: 0x000F39D5
		public void Abort()
		{
			this.m_IsOpen = false;
			this.m_Group.Close();
			this.m_Step = ConnectingUI.eStep.Out;
		}

		// Token: 0x06002EE5 RID: 12005 RVA: 0x000F55F0 File Offset: 0x000F39F0
		public void Open()
		{
			this.m_ConnectingIcon.Open();
			this.m_Group.Open();
			this.m_IsOpen = true;
			this.m_Step = ConnectingUI.eStep.In;
		}

		// Token: 0x06002EE6 RID: 12006 RVA: 0x000F5616 File Offset: 0x000F3A16
		public void Close()
		{
			this.m_IsOpen = false;
		}

		// Token: 0x06002EE7 RID: 12007 RVA: 0x000F5620 File Offset: 0x000F3A20
		private void Update()
		{
			ConnectingUI.eStep step = this.m_Step;
			switch (step + 1)
			{
			case ConnectingUI.eStep.Idle:
				this.UpdateIn();
				break;
			case ConnectingUI.eStep.Out:
				this.UpdateIdle();
				break;
			case (ConnectingUI.eStep)3:
				this.UpdateOut();
				break;
			}
		}

		// Token: 0x06002EE8 RID: 12008 RVA: 0x000F5677 File Offset: 0x000F3A77
		private void UpdateIn()
		{
			if (!this.m_Group.IsDonePlayIn)
			{
				return;
			}
			this.m_Step = ConnectingUI.eStep.Idle;
		}

		// Token: 0x06002EE9 RID: 12009 RVA: 0x000F5691 File Offset: 0x000F3A91
		private void UpdateIdle()
		{
			if (!this.m_IsOpen && this.m_ConnectingIcon.IsAbleClose())
			{
				this.m_Group.Close();
				this.m_Step = ConnectingUI.eStep.Out;
			}
		}

		// Token: 0x06002EEA RID: 12010 RVA: 0x000F56C0 File Offset: 0x000F3AC0
		private void UpdateOut()
		{
			if (!this.m_Group.IsDonePlayOut)
			{
				return;
			}
			this.m_ConnectingIcon.Close();
			this.m_Step = ConnectingUI.eStep.None;
		}

		// Token: 0x06002EEB RID: 12011 RVA: 0x000F56E5 File Offset: 0x000F3AE5
		private bool IsOpenConnecting()
		{
			return this.m_Group.State != UIGroup.eState.Hide && this.m_Group.State != UIGroup.eState.Prepare && this.m_Group.State != UIGroup.eState.WaitFinalize;
		}

		// Token: 0x040035F0 RID: 13808
		[SerializeField]
		private UIGroup m_Group;

		// Token: 0x040035F1 RID: 13809
		[SerializeField]
		private ConnectingIcon m_ConnectingIcon;

		// Token: 0x040035F2 RID: 13810
		private bool m_IsOpen;

		// Token: 0x040035F3 RID: 13811
		private bool m_Destroyed;

		// Token: 0x040035F4 RID: 13812
		private ConnectingUI.eStep m_Step = ConnectingUI.eStep.None;

		// Token: 0x020008D7 RID: 2263
		public enum eStep
		{
			// Token: 0x040035F6 RID: 13814
			None = -1,
			// Token: 0x040035F7 RID: 13815
			In,
			// Token: 0x040035F8 RID: 13816
			Idle,
			// Token: 0x040035F9 RID: 13817
			Out
		}
	}
}
