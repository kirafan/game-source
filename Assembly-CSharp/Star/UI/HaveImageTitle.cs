﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000853 RID: 2131
	public class HaveImageTitle : MonoBehaviour
	{
		// Token: 0x06002C58 RID: 11352 RVA: 0x000EA472 File Offset: 0x000E8872
		public void Setup()
		{
		}

		// Token: 0x06002C59 RID: 11353 RVA: 0x000EA474 File Offset: 0x000E8874
		public void SetTitle(string title)
		{
			if (this.m_Title != null)
			{
				this.m_Title.text = title;
			}
		}

		// Token: 0x06002C5A RID: 11354 RVA: 0x000EA493 File Offset: 0x000E8893
		public void SetActive(bool active)
		{
			base.gameObject.SetActive(active);
		}

		// Token: 0x04003349 RID: 13129
		[SerializeField]
		private Text m_Title;

		// Token: 0x0400334A RID: 13130
		[SerializeField]
		private Image m_Line;
	}
}
