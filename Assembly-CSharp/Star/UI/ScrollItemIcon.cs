﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200081F RID: 2079
	public class ScrollItemIcon : MonoBehaviour
	{
		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06002B40 RID: 11072 RVA: 0x000C39AB File Offset: 0x000C1DAB
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06002B41 RID: 11073 RVA: 0x000C39D0 File Offset: 0x000C1DD0
		public RectTransform RectTransform
		{
			get
			{
				if (this.m_RectTransform == null)
				{
					this.m_RectTransform = base.GetComponent<RectTransform>();
				}
				return this.m_RectTransform;
			}
		}

		// Token: 0x170002C6 RID: 710
		// (set) Token: 0x06002B42 RID: 11074 RVA: 0x000C39F5 File Offset: 0x000C1DF5
		public ScrollViewBase Scroll
		{
			set
			{
				this.m_Scroll = value;
			}
		}

		// Token: 0x14000063 RID: 99
		// (add) Token: 0x06002B43 RID: 11075 RVA: 0x000C3A00 File Offset: 0x000C1E00
		// (remove) Token: 0x06002B44 RID: 11076 RVA: 0x000C3A38 File Offset: 0x000C1E38
		public event Action<ScrollItemIcon> OnHoldIcon;

		// Token: 0x06002B45 RID: 11077 RVA: 0x000C3A6E File Offset: 0x000C1E6E
		protected virtual void Awake()
		{
		}

		// Token: 0x06002B46 RID: 11078 RVA: 0x000C3A70 File Offset: 0x000C1E70
		public void SetItemData(ScrollItemData data)
		{
			this.m_Data = data;
			data.SetIcon(this);
			this.ApplyData();
		}

		// Token: 0x06002B47 RID: 11079 RVA: 0x000C3A86 File Offset: 0x000C1E86
		public ScrollItemData GetItemData()
		{
			return this.m_Data;
		}

		// Token: 0x06002B48 RID: 11080 RVA: 0x000C3A90 File Offset: 0x000C1E90
		protected virtual void ApplyData()
		{
			if (this.m_Button == null)
			{
				this.m_Button = base.GetComponent<CustomButton>();
			}
			if (this.m_Button != null)
			{
				this.m_Button.Interactable = !this.m_IsDark;
			}
		}

		// Token: 0x06002B49 RID: 11081 RVA: 0x000C3ADF File Offset: 0x000C1EDF
		public void OnClickCallBack()
		{
			if (!this.m_IsEnableClick)
			{
				return;
			}
			if (this.m_Data != null)
			{
				this.m_Data.OnClickItemCallBack();
			}
			this.OnClickIcon.Call(this);
		}

		// Token: 0x06002B4A RID: 11082 RVA: 0x000C3B0F File Offset: 0x000C1F0F
		public void OnHoldCallBack()
		{
			if (!this.m_IsEnableHold)
			{
				return;
			}
			this.OnHoldIcon.Call(this);
		}

		// Token: 0x06002B4B RID: 11083 RVA: 0x000C3B29 File Offset: 0x000C1F29
		public virtual void Destroy()
		{
		}

		// Token: 0x06002B4C RID: 11084 RVA: 0x000C3B2B File Offset: 0x000C1F2B
		public virtual void Refresh()
		{
			this.ApplyData();
		}

		// Token: 0x0400319F RID: 12703
		protected ScrollItemData m_Data;

		// Token: 0x040031A0 RID: 12704
		protected GameObject m_GameObject;

		// Token: 0x040031A1 RID: 12705
		protected RectTransform m_RectTransform;

		// Token: 0x040031A2 RID: 12706
		protected bool m_IsDark;

		// Token: 0x040031A3 RID: 12707
		protected bool m_IsEnableClick = true;

		// Token: 0x040031A4 RID: 12708
		protected bool m_IsEnableHold = true;

		// Token: 0x040031A5 RID: 12709
		protected ScrollViewBase m_Scroll;

		// Token: 0x040031A6 RID: 12710
		private CustomButton m_Button;

		// Token: 0x040031A7 RID: 12711
		public Action<ScrollItemIcon> OnClickIcon;
	}
}
