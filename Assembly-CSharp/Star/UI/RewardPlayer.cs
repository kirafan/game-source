﻿using System;
using System.Collections.Generic;
using Star.UI.BackGround;
using WWWTypes;

namespace Star.UI
{
	// Token: 0x02000A67 RID: 2663
	public class RewardPlayer : SingletonMonoBehaviour<RewardPlayer>
	{
		// Token: 0x0600376D RID: 14189 RVA: 0x00118BD3 File Offset: 0x00116FD3
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x0600376E RID: 14190 RVA: 0x00118BDB File Offset: 0x00116FDB
		private void AddDisplayData(RewardPlayer.DisplayData data)
		{
			this.m_DisplayStack.Add(data);
		}

		// Token: 0x0600376F RID: 14191 RVA: 0x00118BE9 File Offset: 0x00116FE9
		private void FlushDisplay()
		{
			this.m_DisplayStack.Clear();
		}

		// Token: 0x06003770 RID: 14192 RVA: 0x00118BF6 File Offset: 0x00116FF6
		private int GetDisplayStackNum()
		{
			return this.m_DisplayStack.Count;
		}

		// Token: 0x06003771 RID: 14193 RVA: 0x00118C03 File Offset: 0x00117003
		public bool IsOpen()
		{
			return this.m_Step != RewardPlayer.eStep.Hide;
		}

		// Token: 0x06003772 RID: 14194 RVA: 0x00118C11 File Offset: 0x00117011
		public void SetReserveOpen(bool flg)
		{
			this.m_IsReserveOpen = flg;
		}

		// Token: 0x06003773 RID: 14195 RVA: 0x00118C1C File Offset: 0x0011701C
		public void Open(BattleResult result, RewardPlayer.ADVReward advReward, Action OnEnd)
		{
			if (!this.m_IsReserveOpen)
			{
				return;
			}
			this.m_IsReserveOpen = false;
			this.m_OnEndCallBack = OnEnd;
			this.FlushDisplay();
			if (result != null)
			{
				string text = null;
				QuestManager.QuestData questData = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetQuestData(result.m_QuestID);
				if (questData.IsChapterQuest())
				{
					QuestManager.ChapterData chapterDataFromQuestID = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetChapterDataFromQuestID(result.m_QuestID);
					if (chapterDataFromQuestID != null)
					{
						text = chapterDataFromQuestID.m_ChapterName;
					}
				}
				else if (questData.IsEventQuest())
				{
					QuestManager.EventGroupData eventGroupDataFromEventID = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetEventGroupDataFromEventID(questData.m_EventID);
					if (eventGroupDataFromEventID != null)
					{
						text = eventGroupDataFromEventID.m_GroupName;
					}
				}
				if (result.m_RewardFirst != null)
				{
					this.AddDisplayData(new RewardPlayer.DisplayData(RewardPlayer.eRewardType.FirstClear, result.m_RewardFirst, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardFirstClearMessage)));
				}
				if (result.m_RewardGroup != null)
				{
					this.AddDisplayData(new RewardPlayer.DisplayData(RewardPlayer.eRewardType.GroupClear, result.m_RewardGroup, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardGroupClearMessage, new object[]
					{
						text
					})));
				}
				if (result.m_RewardComp != null)
				{
					this.AddDisplayData(new RewardPlayer.DisplayData(RewardPlayer.eRewardType.Complete, result.m_RewardComp, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RewardCompleteClearMessage, new object[]
					{
						text
					})));
				}
			}
			if (advReward != null)
			{
				this.AddDisplayData(new RewardPlayer.DisplayData(RewardPlayer.eRewardType.ADV, advReward));
			}
			if (this.m_DisplayStack.Count <= 0)
			{
				this.m_OnEndCallBack.Call();
				return;
			}
			if (this.m_Step != RewardPlayer.eStep.Hide)
			{
				return;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(-1f, null);
			this.m_Step = RewardPlayer.eStep.WaitFadeOut;
		}

		// Token: 0x06003774 RID: 14196 RVA: 0x00118DDD File Offset: 0x001171DD
		public void ForceHide()
		{
			this.m_Step = RewardPlayer.eStep.Hide;
		}

		// Token: 0x06003775 RID: 14197 RVA: 0x00118DE8 File Offset: 0x001171E8
		private void Update()
		{
			switch (this.m_Step)
			{
			case RewardPlayer.eStep.WaitFadeOut:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_Step = RewardPlayer.eStep.LoadBG;
				}
				break;
			case RewardPlayer.eStep.LoadBG:
				SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.OverlayUI, UIBackGroundManager.eBackGroundID.Quest, false);
				this.m_Step = RewardPlayer.eStep.LoadWaitBG;
				break;
			case RewardPlayer.eStep.LoadWaitBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = RewardPlayer.eStep.LoadUI;
				}
				break;
			case RewardPlayer.eStep.LoadUI:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.RewardUI, true);
				this.m_Step = RewardPlayer.eStep.LoadWaitUI;
				break;
			case RewardPlayer.eStep.LoadWaitUI:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.RewardUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.RewardUI);
					this.m_Step = RewardPlayer.eStep.SetupAndPlayIn;
				}
				break;
			case RewardPlayer.eStep.SetupAndPlayIn:
				this.m_UI = UIUtility.GetMenuComponent<RewardUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.RewardUI].m_SceneName);
				if (!(this.m_UI == null))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(-1f, null);
					this.m_UI.ApplyDisplayData(this.m_DisplayStack[0]);
					this.m_DisplayStack.RemoveAt(0);
					this.m_UI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
					this.m_Step = RewardPlayer.eStep.Main;
				}
				break;
			case RewardPlayer.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (this.m_DisplayStack.Count > 0)
					{
						this.m_UI.ApplyDisplayData(this.m_DisplayStack[0]);
						this.m_DisplayStack.RemoveAt(0);
						this.m_UI.PlayIn();
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(-1f, null);
						this.m_Step = RewardPlayer.eStep.WaitFinalizeFadeOut;
					}
				}
				break;
			case RewardPlayer.eStep.WaitFinalizeFadeOut:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					this.m_UI = null;
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(SceneDefine.eChildSceneID.RewardUI);
					this.m_Step = RewardPlayer.eStep.UnloadWait;
				}
				break;
			case RewardPlayer.eStep.UnloadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.RewardUI))
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.OverlayUI);
					this.m_OnEndCallBack.Call();
					this.m_Step = RewardPlayer.eStep.Hide;
				}
				break;
			}
		}

		// Token: 0x04003E21 RID: 15905
		private const SceneDefine.eChildSceneID SCENE_ID = SceneDefine.eChildSceneID.RewardUI;

		// Token: 0x04003E22 RID: 15906
		private RewardPlayer.eStep m_Step;

		// Token: 0x04003E23 RID: 15907
		private RewardUI m_UI;

		// Token: 0x04003E24 RID: 15908
		private bool m_IsReserveOpen;

		// Token: 0x04003E25 RID: 15909
		private Action m_OnEndCallBack;

		// Token: 0x04003E26 RID: 15910
		public List<RewardPlayer.DisplayData> m_DisplayStack = new List<RewardPlayer.DisplayData>();

		// Token: 0x02000A68 RID: 2664
		private enum eStep
		{
			// Token: 0x04003E28 RID: 15912
			Hide,
			// Token: 0x04003E29 RID: 15913
			WaitFadeOut,
			// Token: 0x04003E2A RID: 15914
			LoadBG,
			// Token: 0x04003E2B RID: 15915
			LoadWaitBG,
			// Token: 0x04003E2C RID: 15916
			LoadUI,
			// Token: 0x04003E2D RID: 15917
			LoadWaitUI,
			// Token: 0x04003E2E RID: 15918
			SetupAndPlayIn,
			// Token: 0x04003E2F RID: 15919
			Main,
			// Token: 0x04003E30 RID: 15920
			WaitFinalizeFadeOut,
			// Token: 0x04003E31 RID: 15921
			UnloadWait
		}

		// Token: 0x02000A69 RID: 2665
		public enum eRewardType
		{
			// Token: 0x04003E33 RID: 15923
			None,
			// Token: 0x04003E34 RID: 15924
			FirstClear,
			// Token: 0x04003E35 RID: 15925
			GroupClear,
			// Token: 0x04003E36 RID: 15926
			Complete,
			// Token: 0x04003E37 RID: 15927
			ADV
		}

		// Token: 0x02000A6A RID: 2666
		public class ADVReward
		{
			// Token: 0x04003E38 RID: 15928
			public List<eCharaNamedType> m_NamedList;

			// Token: 0x04003E39 RID: 15929
			public int m_AdvGem;
		}

		// Token: 0x02000A6B RID: 2667
		public class DisplayData
		{
			// Token: 0x06003777 RID: 14199 RVA: 0x0011905C File Offset: 0x0011745C
			public DisplayData(RewardPlayer.eRewardType type, QuestReward reward, string text)
			{
				this.m_RewardType = type;
				this.m_Reward = reward;
				this.m_Text = text;
				this.m_ADVReward = null;
			}

			// Token: 0x06003778 RID: 14200 RVA: 0x00119080 File Offset: 0x00117480
			public DisplayData(RewardPlayer.eRewardType type, RewardPlayer.ADVReward advReward)
			{
				this.m_RewardType = type;
				this.m_Reward = null;
				this.m_ADVReward = advReward;
			}

			// Token: 0x04003E3A RID: 15930
			public RewardPlayer.eRewardType m_RewardType;

			// Token: 0x04003E3B RID: 15931
			public QuestReward m_Reward;

			// Token: 0x04003E3C RID: 15932
			public string m_Text;

			// Token: 0x04003E3D RID: 15933
			public RewardPlayer.ADVReward m_ADVReward;
		}
	}
}
