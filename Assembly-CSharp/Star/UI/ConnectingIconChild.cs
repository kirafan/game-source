﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008D5 RID: 2261
	public class ConnectingIconChild : MonoBehaviour
	{
		// Token: 0x06002EDB RID: 11995 RVA: 0x000F54D4 File Offset: 0x000F38D4
		public ConnectingIcon.ProgressState GetHaveProgressState()
		{
			return new ConnectingIcon.ProgressState(this.m_Image.color, this.m_Image.rectTransform.localScale);
		}

		// Token: 0x06002EDC RID: 11996 RVA: 0x000F54F6 File Offset: 0x000F38F6
		public void Setup(ConnectingIcon.ProgressState[] progressStates)
		{
			this.m_ProgressStates = progressStates;
		}

		// Token: 0x06002EDD RID: 11997 RVA: 0x000F54FF File Offset: 0x000F38FF
		public void Advance()
		{
			this.m_Progress--;
			if (this.m_Progress < 0)
			{
				this.m_Progress = this.m_ProgressStates.Length - 1;
			}
			this.Apply();
		}

		// Token: 0x06002EDE RID: 11998 RVA: 0x000F5534 File Offset: 0x000F3934
		public void Apply()
		{
			this.m_Image.color = this.m_ProgressStates[this.m_Progress].color;
			this.m_Image.transform.localScale = this.m_ProgressStates[this.m_Progress].scale;
		}

		// Token: 0x06002EDF RID: 11999 RVA: 0x000F5580 File Offset: 0x000F3980
		public void SetProgress(int progress)
		{
			this.m_Progress = progress;
			this.Apply();
		}

		// Token: 0x040035ED RID: 13805
		[SerializeField]
		private Image m_Image;

		// Token: 0x040035EE RID: 13806
		private ConnectingIcon.ProgressState[] m_ProgressStates;

		// Token: 0x040035EF RID: 13807
		private int m_Progress;
	}
}
