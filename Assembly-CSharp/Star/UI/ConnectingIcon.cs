﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008D3 RID: 2259
	public class ConnectingIcon : MonoBehaviour
	{
		// Token: 0x06002ED1 RID: 11985 RVA: 0x000F5165 File Offset: 0x000F3565
		private void Start()
		{
		}

		// Token: 0x06002ED2 RID: 11986 RVA: 0x000F5167 File Offset: 0x000F3567
		public void Setup()
		{
			this.m_Body.SetActive(false);
			this.m_InvisibleGraphic.SetActive(false);
		}

		// Token: 0x06002ED3 RID: 11987 RVA: 0x000F5184 File Offset: 0x000F3584
		private void SetupIconChild()
		{
			List<ConnectingIcon.ProgressState> list = new List<ConnectingIcon.ProgressState>();
			for (int i = 0; i < this.m_ChildImages.Length; i++)
			{
				list.Add(this.m_ChildImages[i].GetHaveProgressState());
			}
			for (int j = 0; j < this.m_ProgressStates.Length; j++)
			{
				list.Add(this.m_ProgressStates[j]);
			}
			this.m_perfectProgressStates = list.ToArray();
			for (int k = 0; k < this.m_ChildImages.Length; k++)
			{
				this.m_ChildImages[k].Setup(this.m_perfectProgressStates);
				this.m_ChildImages[k].SetProgress(k);
			}
		}

		// Token: 0x06002ED4 RID: 11988 RVA: 0x000F5230 File Offset: 0x000F3630
		public void Open()
		{
			this.m_Body.SetActive(true);
			this.m_InvisibleGraphic.SetActive(true);
			this.m_IsOpen = true;
			this.m_OpenStartTime = Time.realtimeSinceStartup;
			this.SetupIconChild();
		}

		// Token: 0x06002ED5 RID: 11989 RVA: 0x000F5264 File Offset: 0x000F3664
		private void Update()
		{
			if (this.m_IsOpen)
			{
				if (this.m_Count > 4f)
				{
					this.m_Count = 4f;
				}
				if (this.m_Count >= 4f)
				{
					this.m_Count = 0f;
				}
				float deltaTime = Time.deltaTime;
				this.m_Count += 2f * deltaTime;
				this.m_Wait += deltaTime;
				if (this.m_ActionTarget != null)
				{
					for (int i = 0; i < this.m_ActionTarget.Length; i++)
					{
						this.m_ActionTarget[i].eulerAngles = new Vector3(this.m_ActionTarget[i].eulerAngles.x, this.m_ActionTarget[i].eulerAngles.y, this.m_ActionTarget[i].eulerAngles.z + deltaTime * this.m_ActionTargetRotateZParSecond);
					}
				}
				if (0.1f <= this.m_Wait)
				{
					this.m_Wait -= 0.1f;
					for (int j = 0; j < this.m_ChildImages.Length; j++)
					{
						this.m_ChildImages[j].Advance();
					}
				}
			}
			else if (this.m_Body.activeSelf)
			{
				int num = (int)((Time.realtimeSinceStartup - this.m_OpenStartTime) * this.m_FrameParSecond);
				if (this.m_OpenFrameMin <= (float)num)
				{
					this.m_Body.SetActive(false);
					this.m_InvisibleGraphic.SetActive(false);
				}
			}
		}

		// Token: 0x06002ED6 RID: 11990 RVA: 0x000F53F7 File Offset: 0x000F37F7
		public void Close()
		{
			this.m_IsOpen = false;
		}

		// Token: 0x06002ED7 RID: 11991 RVA: 0x000F5400 File Offset: 0x000F3800
		public bool IsAbleClose()
		{
			return this.m_OpenFrameMin <= (float)((int)((Time.realtimeSinceStartup - this.m_OpenStartTime) * this.m_FrameParSecond));
		}

		// Token: 0x040035DD RID: 13789
		private const float m_WaitForSecond = 0.1f;

		// Token: 0x040035DE RID: 13790
		[SerializeField]
		private GameObject m_InvisibleGraphic;

		// Token: 0x040035DF RID: 13791
		[SerializeField]
		private GameObject m_Body;

		// Token: 0x040035E0 RID: 13792
		[SerializeField]
		private RectTransform[] m_ActionTarget;

		// Token: 0x040035E1 RID: 13793
		[SerializeField]
		private float m_ActionTargetRotateZParSecond = -180f;

		// Token: 0x040035E2 RID: 13794
		[SerializeField]
		private ConnectingIconChild[] m_ChildImages;

		// Token: 0x040035E3 RID: 13795
		[SerializeField]
		[Tooltip("追加分.")]
		private ConnectingIcon.ProgressState[] m_ProgressStates;

		// Token: 0x040035E4 RID: 13796
		private ConnectingIcon.ProgressState[] m_perfectProgressStates;

		// Token: 0x040035E5 RID: 13797
		private bool m_IsOpen;

		// Token: 0x040035E6 RID: 13798
		private float m_Count;

		// Token: 0x040035E7 RID: 13799
		private float m_Wait;

		// Token: 0x040035E8 RID: 13800
		private float m_OpenStartTime;

		// Token: 0x040035E9 RID: 13801
		[SerializeField]
		private float m_OpenFrameMin = 10f;

		// Token: 0x040035EA RID: 13802
		[SerializeField]
		private float m_FrameParSecond = 30f;

		// Token: 0x020008D4 RID: 2260
		[Serializable]
		public class ProgressState
		{
			// Token: 0x06002ED8 RID: 11992 RVA: 0x000F5424 File Offset: 0x000F3824
			public ProgressState(Color in_color, Vector3 in_scale)
			{
				this.color = in_color;
				this.scale = in_scale;
			}

			// Token: 0x06002ED9 RID: 11993 RVA: 0x000F5480 File Offset: 0x000F3880
			public ProgressState()
			{
			}

			// Token: 0x040035EB RID: 13803
			public Color color = new Color(1f, 1f, 1f, 1f);

			// Token: 0x040035EC RID: 13804
			public Vector3 scale = new Vector3(1f, 1f, 1f);
		}
	}
}
