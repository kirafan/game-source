﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x020008D8 RID: 2264
	public class DownloadImageItemData : InfiniteScrollItemData
	{
		// Token: 0x040035FA RID: 13818
		public Sprite m_Sprite;
	}
}
