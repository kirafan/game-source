﻿using System;

namespace Star.UI
{
	// Token: 0x02000A3B RID: 2619
	public class OverlaySortPlayer : OverlayUIPlayerBase
	{
		// Token: 0x06003670 RID: 13936 RVA: 0x00112A2E File Offset: 0x00110E2E
		public override SceneDefine.eChildSceneID GetUISceneID()
		{
			return this.m_SceneID;
		}

		// Token: 0x06003671 RID: 13937 RVA: 0x00112A38 File Offset: 0x00110E38
		protected override void Setup(OverlayUIArgBase arg)
		{
			this.m_SortUI = this.GetUI<SortUI>();
			SortUIArg sortUIArg = (SortUIArg)arg;
			this.m_SortUI.Setup(sortUIArg.m_UsingType, sortUIArg.m_OnFilterExecute);
			SortUI sortUI = this.m_SortUI;
			sortUI.OnStartOut = (Action)Delegate.Combine(sortUI.OnStartOut, new Action(this.GoToMenuEnd));
		}

		// Token: 0x06003672 RID: 13938 RVA: 0x00112A97 File Offset: 0x00110E97
		protected override void OnClickBackButtonCallBack(bool isShortCut)
		{
			base.OnClickBackButtonCallBack(isShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x04003CC5 RID: 15557
		private SceneDefine.eChildSceneID m_SceneID = SceneDefine.eChildSceneID.SortUI;

		// Token: 0x04003CC6 RID: 15558
		protected SortUI m_SortUI;
	}
}
