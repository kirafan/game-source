﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A15 RID: 2581
	public class MissionPopUpExchangeComplete : MissionPopUpWindow
	{
		// Token: 0x06003569 RID: 13673 RVA: 0x0010ED21 File Offset: 0x0010D121
		public void OnCloseButtonCallBack()
		{
			this.Close();
		}
	}
}
