﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A1E RID: 2590
	public class MissionScrollItemData : ScrollItemData
	{
		// Token: 0x04003C2A RID: 15402
		public MissionScroll m_RootList;

		// Token: 0x04003C2B RID: 15403
		public IMissionUIData m_UIData;
	}
}
