﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A17 RID: 2583
	public class MissionPopUpItemExchange : MissionPopUpWindow
	{
		// Token: 0x0600356D RID: 13677 RVA: 0x0010ED41 File Offset: 0x0010D141
		public void OnYesButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x0600356E RID: 13678 RVA: 0x0010ED49 File Offset: 0x0010D149
		public void OnCloseButtonCallBack()
		{
			this.Close();
		}
	}
}
