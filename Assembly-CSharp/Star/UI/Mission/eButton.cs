﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A20 RID: 2592
	public enum eButton
	{
		// Token: 0x04003C2F RID: 15407
		None,
		// Token: 0x04003C30 RID: 15408
		Yes,
		// Token: 0x04003C31 RID: 15409
		No,
		// Token: 0x04003C32 RID: 15410
		Cancel,
		// Token: 0x04003C33 RID: 15411
		Back
	}
}
