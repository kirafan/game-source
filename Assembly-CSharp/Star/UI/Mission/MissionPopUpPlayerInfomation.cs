﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A19 RID: 2585
	public class MissionPopUpPlayerInfomation : MissionPopUpWindow
	{
		// Token: 0x06003572 RID: 13682 RVA: 0x0010ED69 File Offset: 0x0010D169
		public void OnCloseButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003573 RID: 13683 RVA: 0x0010ED71 File Offset: 0x0010D171
		public void OnAdoptButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06003574 RID: 13684 RVA: 0x0010ED79 File Offset: 0x0010D179
		public void OnNoAdoptButtonCallBack()
		{
			this.Close();
		}
	}
}
