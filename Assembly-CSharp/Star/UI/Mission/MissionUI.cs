﻿using System;
using UnityEngine;

namespace Star.UI.Mission
{
	// Token: 0x02000A22 RID: 2594
	public class MissionUI : MenuUIBase
	{
		// Token: 0x140000AB RID: 171
		// (add) Token: 0x0600359E RID: 13726 RVA: 0x0010FC64 File Offset: 0x0010E064
		// (remove) Token: 0x0600359F RID: 13727 RVA: 0x0010FC9C File Offset: 0x0010E09C
		public event Action OnEnd;

		// Token: 0x060035A0 RID: 13728 RVA: 0x0010FCD4 File Offset: 0x0010E0D4
		public void Setup()
		{
			this.m_StateController.Setup(5, 0, false);
			this.m_StateController.AddTransit(0, 1, true);
			this.m_StateController.AddTransit(1, 4, true);
			this.m_StateController.AddTransit(4, 2, true);
			this.m_StateController.AddTransit(2, 3, true);
			string[] buttonTexts = new string[]
			{
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabNormal),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabLimit),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDay),
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionTabDrop)
			};
			this.m_TabWindow.Setup(buttonTexts, -1f);
			this.m_ListObject = new GameObject[4];
			this.m_ListObject[0] = this.m_ScrollNormal.gameObject;
			this.m_ListObject[1] = this.m_ScrollLimit.gameObject;
			this.m_ListObject[2] = this.m_ScrollDay.gameObject;
			this.m_ListObject[3] = this.m_ScrollDrop.gameObject;
			this.m_ScrollNormal.Setup();
			this.m_ScrollLimit.Setup();
			this.m_ScrollDay.Setup();
			this.m_ScrollDrop.Setup();
			this.m_MissionList = new MissionScroll[4];
			if (this.m_Badges == null)
			{
				this.m_Badges = new Badge[this.m_MissionList.Length];
				for (int i = 0; i < this.m_TabWindow.GetButtonNum(); i++)
				{
					RectTransform component = this.m_TabWindow.GetButton(i).Button.GetComponent<RectTransform>();
					this.m_Badges[i] = UnityEngine.Object.Instantiate<Badge>(this.m_BadgePrefab, component);
					RectTransform component2 = this.m_Badges[i].GetComponent<RectTransform>();
					component2.anchoredPosition = new Vector2(108f, -4f);
				}
			}
			this.m_MissionList[0] = new MissionScroll();
			this.m_MissionList[0].SetupBoard(this.m_ScrollNormal, eMissionCategory.Day, this.m_MissionListInfo, this.m_MissionLimitInfo, this.m_Badges[0]);
			this.m_MissionList[1] = new MissionScroll();
			this.m_MissionList[1].SetupBoard(this.m_ScrollLimit, eMissionCategory.Week, this.m_MissionListInfo, this.m_MissionLimitInfo, this.m_Badges[1]);
			this.m_MissionList[2] = new MissionScroll();
			this.m_MissionList[2].SetupBoard(this.m_ScrollDay, eMissionCategory.Unlock, this.m_MissionListInfo, this.m_MissionLimitInfo, this.m_Badges[2]);
			this.m_MissionList[3] = new MissionScroll();
			this.m_MissionList[3].SetupBoard(this.m_ScrollDrop, eMissionCategory.Event, this.m_MissionListInfo, this.m_MissionLimitInfo, this.m_Badges[3]);
			MissionManager.EntryRefreshCallback(new RefreshCallback(this.CallbackRefresh), 0);
			this.ChangeCategory(eMissionCategory.Day);
		}

		// Token: 0x060035A1 RID: 13729 RVA: 0x0010FFA0 File Offset: 0x0010E3A0
		public override void PlayIn()
		{
			if (this.m_StateController.ChangeState(1))
			{
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					this.m_AnimList[i].PlayIn();
				}
				this.m_MainWindow.Open();
				UIUtility.PlayVoiceScheduleChara(eSoundVoiceListDB.voice_012);
			}
		}

		// Token: 0x060035A2 RID: 13730 RVA: 0x0010FFF8 File Offset: 0x0010E3F8
		public override void PlayOut()
		{
			if (this.m_StateController.ChangeState(2))
			{
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					this.m_AnimList[i].PlayOut();
				}
				this.m_MainWindow.Close();
				for (int j = 0; j < 4; j++)
				{
					this.m_MissionList[j].ReleaseUp();
					this.m_MissionList[j] = null;
				}
				MissionManager.ReleaseRefreshCallback(0);
				this.m_MissionList = null;
			}
		}

		// Token: 0x060035A3 RID: 13731 RVA: 0x0011007C File Offset: 0x0010E47C
		public void PlayAllComplete()
		{
			int num = 0;
			for (int i = 0; i < 4; i++)
			{
				num += this.m_MissionList[i].CheckAllComplete();
			}
			if (num > 0)
			{
				int currentCategory = (int)this.m_CurrentCategory;
				this.m_MissionList[currentCategory].RefreshList();
				this.m_MissionList[currentCategory].SetUMissionListInfo();
				UIUtility.PlayVoiceScheduleChara(eSoundVoiceListDB.voice_013);
			}
		}

		// Token: 0x060035A4 RID: 13732 RVA: 0x001100DC File Offset: 0x0010E4DC
		private void Update()
		{
			switch (this.m_StateController.NowState)
			{
			case 1:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag && this.m_MainWindow.IsDonePlayIn)
				{
					this.m_StateController.ChangeState(4);
				}
				break;
			}
			case 2:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2 && this.m_MainWindow.IsDonePlayOut)
				{
					this.m_StateController.ChangeState(3);
					this.OnEnd.Call();
				}
				break;
			}
			case 4:
				this.m_TimeChk += Time.deltaTime;
				if (this.m_TimeChk >= 1f)
				{
					this.m_MissionList[(int)this.m_CurrentCategory].SetUMissionLimitInfo();
					this.m_TimeChk -= 1f;
				}
				break;
			}
		}

		// Token: 0x060035A5 RID: 13733 RVA: 0x00110231 File Offset: 0x0010E631
		public override bool IsMenuEnd()
		{
			return this.m_StateController.NowState == 3;
		}

		// Token: 0x060035A6 RID: 13734 RVA: 0x00110241 File Offset: 0x0010E641
		public void OpenMainWindow()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.m_MainWindow.Open();
			}
		}

		// Token: 0x060035A7 RID: 13735 RVA: 0x0011025F File Offset: 0x0010E65F
		public void CloseMainWindow()
		{
			this.m_MainWindow.Close();
		}

		// Token: 0x060035A8 RID: 13736 RVA: 0x0011026C File Offset: 0x0010E66C
		public void ChangeTab()
		{
			if (this.m_CurrentCategory != (eMissionCategory)this.m_TabWindow.SelectIdx)
			{
				this.ChangeCategory((eMissionCategory)this.m_TabWindow.SelectIdx);
			}
		}

		// Token: 0x060035A9 RID: 13737 RVA: 0x00110295 File Offset: 0x0010E695
		public void OnClickCategoryButtonCallBack(int category)
		{
			if (this.m_CurrentCategory == (eMissionCategory)category)
			{
				return;
			}
			this.ChangeCategory((eMissionCategory)category);
		}

		// Token: 0x060035AA RID: 13738 RVA: 0x001102AC File Offset: 0x0010E6AC
		private void ChangeCategory(eMissionCategory category)
		{
			this.m_CurrentCategory = category;
			for (eMissionCategory eMissionCategory = eMissionCategory.Day; eMissionCategory < eMissionCategory.Max; eMissionCategory++)
			{
				this.m_ListObject[(int)eMissionCategory].SetActive(eMissionCategory == category);
			}
			this.m_MissionList[(int)category].RefreshList();
			this.m_MissionList[(int)category].SetUMissionListInfo();
			this.m_MissionList[(int)category].SetUMissionLimitInfo();
		}

		// Token: 0x060035AB RID: 13739 RVA: 0x0011030C File Offset: 0x0010E70C
		public void CallbackRefresh()
		{
			for (eMissionCategory eMissionCategory = eMissionCategory.Day; eMissionCategory < eMissionCategory.Max; eMissionCategory++)
			{
				this.m_MissionList[(int)eMissionCategory].ListRefresh(this.m_CurrentCategory == eMissionCategory);
			}
		}

		// Token: 0x04003C34 RID: 15412
		private const int BADGE_POS_X = 108;

		// Token: 0x04003C35 RID: 15413
		private const int BADGE_POS_Y = -4;

		// Token: 0x04003C36 RID: 15414
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04003C37 RID: 15415
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003C38 RID: 15416
		[SerializeField]
		private UIGroup m_MainWindow;

		// Token: 0x04003C39 RID: 15417
		[SerializeField]
		private TabGroup m_TabWindow;

		// Token: 0x04003C3A RID: 15418
		[SerializeField]
		private ScrollViewBase m_ScrollNormal;

		// Token: 0x04003C3B RID: 15419
		[SerializeField]
		private ScrollViewBase m_ScrollLimit;

		// Token: 0x04003C3C RID: 15420
		[SerializeField]
		private ScrollViewBase m_ScrollDay;

		// Token: 0x04003C3D RID: 15421
		[SerializeField]
		private ScrollViewBase m_ScrollDrop;

		// Token: 0x04003C3E RID: 15422
		[SerializeField]
		private GameObject m_MissionListInfo;

		// Token: 0x04003C3F RID: 15423
		[SerializeField]
		private GameObject m_MissionLimitInfo;

		// Token: 0x04003C40 RID: 15424
		[SerializeField]
		private Badge m_BadgePrefab;

		// Token: 0x04003C41 RID: 15425
		private Badge[] m_Badges;

		// Token: 0x04003C42 RID: 15426
		private GameObject[] m_ListObject;

		// Token: 0x04003C43 RID: 15427
		private MissionScroll[] m_MissionList;

		// Token: 0x04003C44 RID: 15428
		private float m_TimeChk;

		// Token: 0x04003C45 RID: 15429
		private eMissionCategory m_CurrentCategory = eMissionCategory.None;

		// Token: 0x02000A23 RID: 2595
		private enum eUIState
		{
			// Token: 0x04003C48 RID: 15432
			Hide,
			// Token: 0x04003C49 RID: 15433
			In,
			// Token: 0x04003C4A RID: 15434
			Out,
			// Token: 0x04003C4B RID: 15435
			End,
			// Token: 0x04003C4C RID: 15436
			Main,
			// Token: 0x04003C4D RID: 15437
			Num
		}
	}
}
