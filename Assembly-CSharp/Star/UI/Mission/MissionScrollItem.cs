﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000A1C RID: 2588
	public class MissionScrollItem : ScrollItemIcon
	{
		// Token: 0x06003586 RID: 13702 RVA: 0x0010F41F File Offset: 0x0010D81F
		public eMissionCategory GetCategory()
		{
			return this.m_Category;
		}

		// Token: 0x06003587 RID: 13703 RVA: 0x0010F427 File Offset: 0x0010D827
		protected override void ApplyData()
		{
			base.ApplyData();
			this.SetUIData(((MissionScrollItemData)this.m_Data).m_UIData);
		}

		// Token: 0x06003588 RID: 13704 RVA: 0x0010F448 File Offset: 0x0010D848
		public virtual void SetUIData(IMissionUIData uiData)
		{
			this.m_Category = uiData.m_Type;
			this.SetID(uiData.m_ManageID);
			this.SetComplete(uiData.m_Rate, uiData.m_RateMax, uiData.m_State);
			this.SetTarget(uiData.m_TargetMessage);
			this.SetReward(uiData.m_RewardType, uiData.m_RewardID);
		}

		// Token: 0x06003589 RID: 13705 RVA: 0x0010F4A3 File Offset: 0x0010D8A3
		private void SetID(long fmanageid)
		{
			this.m_MissionManageID = fmanageid;
		}

		// Token: 0x0600358A RID: 13706 RVA: 0x0010F4AC File Offset: 0x0010D8AC
		private void SetComplete(int rateValue, int rateMax, eMissionState fstate)
		{
			this.m_State = fstate;
			switch (this.m_State)
			{
			case eMissionState.Non:
			case eMissionState.Comp:
				if (this.m_CompleteButton)
				{
					this.m_CompleteButton.IsEnableInput = false;
				}
				break;
			case eMissionState.PreComp:
			case eMissionState.PreView:
				if (this.m_CompleteButton)
				{
					this.m_CompleteButton.IsEnableInput = true;
				}
				break;
			}
			Transform transform = UIUtility.FindHrcTransform(base.transform, "Rate");
			Transform transform2 = UIUtility.FindHrcTransform(base.transform, "PreComp");
			Transform transform3 = UIUtility.FindHrcTransform(base.transform, "Comp");
			Transform transform4 = UIUtility.FindHrcTransform(base.transform, "GetReady");
			switch (this.m_State)
			{
			default:
				transform.gameObject.SetActive(true);
				transform4.gameObject.SetActive(false);
				transform3.gameObject.SetActive(false);
				transform2.gameObject.SetActive(false);
				this.m_Blink = null;
				break;
			case eMissionState.PreComp:
			case eMissionState.PreView:
				transform.gameObject.SetActive(false);
				transform2.gameObject.SetActive(true);
				transform4.gameObject.SetActive(true);
				transform3.gameObject.SetActive(false);
				this.m_Blink = new MissionScrollItem.BlinkUIImage();
				this.m_Blink.SetGameObject(transform4.gameObject);
				break;
			case eMissionState.Comp:
				transform.gameObject.SetActive(false);
				transform2.gameObject.SetActive(true);
				transform4.gameObject.SetActive(false);
				transform3.gameObject.SetActive(true);
				this.m_Blink = null;
				break;
			}
			if (this.m_RateValueTextObj)
			{
				this.m_RateValueTextObj.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionSegMsgHaveFix), rateValue, rateMax);
			}
		}

		// Token: 0x0600358B RID: 13707 RVA: 0x0010F699 File Offset: 0x0010DA99
		private void SetTarget(string targetText)
		{
			if (this.m_TargetTextObj)
			{
				this.m_TargetTextObj.text = targetText;
			}
		}

		// Token: 0x0600358C RID: 13708 RVA: 0x0010F6B8 File Offset: 0x0010DAB8
		private void SetReward(eMissionRewardCategory rewardType, int rewardID)
		{
			switch (rewardType)
			{
			case eMissionRewardCategory.Non:
				this.m_RewardTextObj.text = "non";
				break;
			case eMissionRewardCategory.Money:
				this.m_RewardTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonType_Gold);
				break;
			case eMissionRewardCategory.Item:
			{
				ItemListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ItemListDB.GetParam(rewardID);
				this.m_RewardTextObj.text = param.m_Name;
				break;
			}
			case eMissionRewardCategory.KRRPoint:
				this.m_RewardTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonType_Kirara);
				break;
			case eMissionRewardCategory.Gem:
				this.m_RewardTextObj.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonType_Gem);
				break;
			}
		}

		// Token: 0x0600358D RID: 13709 RVA: 0x0010F794 File Offset: 0x0010DB94
		public void OnClickCompleteButtonCallBack()
		{
			MissionScrollItemData missionScrollItemData = this.m_Data as MissionScrollItemData;
			this.SetComplete(missionScrollItemData.m_UIData.m_Rate, missionScrollItemData.m_UIData.m_RateMax, eMissionState.Comp);
			missionScrollItemData.m_RootList.CompleteMission(this.m_MissionManageID);
			UIUtility.PlayVoiceScheduleChara(eSoundVoiceListDB.voice_013);
		}

		// Token: 0x0600358E RID: 13710 RVA: 0x0010F7E4 File Offset: 0x0010DBE4
		protected void SetChara(CharaIcon pcharicon, Text pcharaname, long charaMngID)
		{
			if (charaMngID == -1L)
			{
				charaMngID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[0].MngID;
			}
			int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID).Param.CharaID;
			pcharicon.Apply(charaID);
			pcharaname.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_Name;
		}

		// Token: 0x0600358F RID: 13711 RVA: 0x0010F85A File Offset: 0x0010DC5A
		protected void SetCharaIcon(CharaIcon pcharicon, Text pcharaname, int charaID)
		{
			pcharicon.Apply(charaID);
		}

		// Token: 0x06003590 RID: 13712 RVA: 0x0010F863 File Offset: 0x0010DC63
		private void GetTimeTickToTimes(out int fhour, out int fminute, out int fsec, long ftimes)
		{
			ftimes /= 10000000L;
			fhour = (int)(ftimes / 3600L);
			fminute = (int)(ftimes / 60L);
			fsec = (int)ftimes;
		}

		// Token: 0x06003591 RID: 13713 RVA: 0x0010F88C File Offset: 0x0010DC8C
		protected void SetItemTime(Text ptarget, DateTime fsettime, bool fdebugs)
		{
			long ticks;
			if (fdebugs)
			{
				ticks = ScheduleTimeUtil.GetManageUniversalTime().Ticks;
			}
			else
			{
				ticks = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Ticks;
			}
			if (fsettime.Ticks > ticks)
			{
				int num;
				int num2;
				int num3;
				this.GetTimeTickToTimes(out num, out num2, out num3, fsettime.Ticks - ticks);
				if (num != 0)
				{
					ptarget.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeHour), num);
				}
				else if (num2 != 0)
				{
					ptarget.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeMinute), num2);
				}
				else
				{
					ptarget.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeSec), num3);
				}
			}
			else
			{
				ptarget.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeOver);
			}
		}

		// Token: 0x06003592 RID: 13714 RVA: 0x0010F990 File Offset: 0x0010DD90
		private void Update()
		{
			if (this.m_Blink != null)
			{
				this.m_Blink.UpdateFunc();
			}
		}

		// Token: 0x04003C20 RID: 15392
		private long m_MissionManageID = -1L;

		// Token: 0x04003C21 RID: 15393
		private eMissionState m_State;

		// Token: 0x04003C22 RID: 15394
		private eMissionCategory m_Category;

		// Token: 0x04003C23 RID: 15395
		[SerializeField]
		private CustomButton m_CompleteButton;

		// Token: 0x04003C24 RID: 15396
		[SerializeField]
		private Text m_TargetTextObj;

		// Token: 0x04003C25 RID: 15397
		[SerializeField]
		private Text m_RewardTextObj;

		// Token: 0x04003C26 RID: 15398
		[SerializeField]
		private Text m_RateValueTextObj;

		// Token: 0x04003C27 RID: 15399
		private MissionScrollItem.BlinkUIImage m_Blink;

		// Token: 0x02000A1D RID: 2589
		public class BlinkUIImage
		{
			// Token: 0x06003594 RID: 13716 RVA: 0x0010F9B0 File Offset: 0x0010DDB0
			public void SetGameObject(GameObject pobj)
			{
				this.m_Image = pobj.GetComponentInChildren<Image>();
			}

			// Token: 0x06003595 RID: 13717 RVA: 0x0010F9C0 File Offset: 0x0010DDC0
			public void UpdateFunc()
			{
				if (this.m_Image != null)
				{
					this.m_Time += Time.deltaTime;
					if (this.m_Time >= 2f)
					{
						this.m_Time -= 2f;
					}
					float num = Mathf.Sin(this.m_Time * 3.1415927f);
					Color white = Color.white;
					white.a = num * 0.5f + 0.5f;
					this.m_Image.color = white;
				}
			}

			// Token: 0x04003C28 RID: 15400
			public Image m_Image;

			// Token: 0x04003C29 RID: 15401
			public float m_Time;
		}
	}
}
