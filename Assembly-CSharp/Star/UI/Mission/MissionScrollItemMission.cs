﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000A1F RID: 2591
	public class MissionScrollItemMission : MissionScrollItem
	{
		// Token: 0x06003598 RID: 13720 RVA: 0x0010FA5C File Offset: 0x0010DE5C
		public override void SetUIData(IMissionUIData uiData)
		{
			base.SetUIData(uiData);
			Text text = UIUtility.FindHrcObject(base.transform, "Reward/RewardNum", typeof(Text)) as Text;
			if (text != null)
			{
				string arg = string.Empty;
				switch (uiData.m_RewardType)
				{
				case eMissionRewardCategory.Money:
					arg = UIUtility.GoldValueToString(uiData.m_RewardNum, false);
					goto IL_B2;
				case eMissionRewardCategory.KRRPoint:
					arg = UIUtility.KPValueToString(uiData.m_RewardNum, false);
					goto IL_B2;
				case eMissionRewardCategory.Gem:
					arg = UIUtility.GemValueToString(uiData.m_RewardNum, false);
					goto IL_B2;
				}
				arg = uiData.m_RewardNum.ToString();
				IL_B2:
				text.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionRewardNum), arg);
			}
			Image image = UIUtility.FindHrcObject(base.transform, "Rate/Bar", typeof(Image)) as Image;
			if (image != null)
			{
				if (uiData.m_Rate == 0)
				{
					image.gameObject.SetActive(false);
				}
				else
				{
					image.gameObject.SetActive(true);
					RectTransform component = image.gameObject.GetComponent<RectTransform>();
					Vector2 sizeDelta = component.sizeDelta;
					sizeDelta.x = (float)uiData.m_Rate / (float)uiData.m_RateMax * 256f;
					component.sizeDelta = sizeDelta;
				}
			}
			switch (uiData.m_RewardType)
			{
			case eMissionRewardCategory.Money:
				this.m_RewardIcon.GetInst<VariableIconWithFrame>().ApplyGold();
				break;
			case eMissionRewardCategory.Item:
				this.m_RewardIcon.GetInst<VariableIconWithFrame>().ApplyItem(uiData.m_RewardID);
				break;
			case eMissionRewardCategory.KRRPoint:
				this.m_RewardIcon.GetInst<VariableIconWithFrame>().ApplyKirara();
				break;
			case eMissionRewardCategory.Gem:
				this.m_RewardIcon.GetInst<VariableIconWithFrame>().ApplyGem();
				break;
			}
		}

		// Token: 0x04003C2C RID: 15404
		[SerializeField]
		private PrefabCloner m_RewardIcon;

		// Token: 0x04003C2D RID: 15405
		[SerializeField]
		private Text m_LimitTermText;
	}
}
