﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Mission
{
	// Token: 0x02000A1B RID: 2587
	public class MissionScroll
	{
		// Token: 0x06003577 RID: 13687 RVA: 0x0010ED89 File Offset: 0x0010D189
		public int GetAllListNum()
		{
			return this.m_ListUp.Count;
		}

		// Token: 0x06003578 RID: 13688 RVA: 0x0010ED96 File Offset: 0x0010D196
		public int GetCompListNum()
		{
			return this.m_CompNum;
		}

		// Token: 0x06003579 RID: 13689 RVA: 0x0010ED9E File Offset: 0x0010D19E
		public int GetPreCompNum()
		{
			return this.m_PreCompNum;
		}

		// Token: 0x0600357A RID: 13690 RVA: 0x0010EDA6 File Offset: 0x0010D1A6
		public DateTime GetLimitTime()
		{
			return this.m_LimitTime;
		}

		// Token: 0x0600357B RID: 13691 RVA: 0x0010EDB0 File Offset: 0x0010D1B0
		public void SetupBoard(ScrollViewBase pscroll, eMissionCategory category, GameObject plistinfo, GameObject plimitinfo, Badge badge)
		{
			MissionUIDataPack categoryToMissionUIData = MissionManager.Inst.GetCategoryToMissionUIData(category);
			this.m_ListUp = new List<IMissionUIData>();
			this.m_ListInfo = plistinfo;
			this.m_LimitInfo = plimitinfo;
			this.m_Scroll = pscroll;
			this.m_Category = category;
			if (categoryToMissionUIData.m_Table.Length != 0)
			{
				this.m_LimitTime = categoryToMissionUIData.m_Table[0].m_LimitTime;
			}
			for (int i = 0; i < categoryToMissionUIData.GetListNum(); i++)
			{
				if (categoryToMissionUIData.m_Table[i].m_State == eMissionState.PreComp || categoryToMissionUIData.m_Table[i].m_State == eMissionState.PreView)
				{
					this.m_ListUp.Add(categoryToMissionUIData.m_Table[i]);
					this.m_PreCompNum++;
				}
			}
			for (int j = 0; j < categoryToMissionUIData.GetListNum(); j++)
			{
				if (categoryToMissionUIData.m_Table[j].m_State == eMissionState.Non)
				{
					this.m_ListUp.Add(categoryToMissionUIData.m_Table[j]);
				}
			}
			for (int k = 0; k < categoryToMissionUIData.GetListNum(); k++)
			{
				if (categoryToMissionUIData.m_Table[k].m_State == eMissionState.Comp)
				{
					this.m_ListUp.Add(categoryToMissionUIData.m_Table[k]);
					this.m_CompNum++;
				}
			}
			pscroll.RemoveAll();
			for (int l = 0; l < this.m_ListUp.Count; l++)
			{
				pscroll.AddItem(new MissionScrollItemData
				{
					m_UIData = this.m_ListUp[l],
					m_RootList = this
				});
			}
			this.m_Badge = badge;
			this.m_Badge.Apply(this.GetPreCompNum());
		}

		// Token: 0x0600357C RID: 13692 RVA: 0x0010EF60 File Offset: 0x0010D360
		public void ReleaseUp()
		{
		}

		// Token: 0x0600357D RID: 13693 RVA: 0x0010EF64 File Offset: 0x0010D364
		public int CheckAllComplete()
		{
			int num = 0;
			for (int i = 0; i < this.m_ListUp.Count; i++)
			{
				if (this.m_ListUp[i].m_State == eMissionState.PreComp || this.m_ListUp[i].m_State == eMissionState.PreView)
				{
					MissionManager.CompleteMission(this.m_ListUp[i].m_ManageID);
					this.m_ListUp[i].m_State = eMissionState.Comp;
					this.m_PreCompNum--;
					this.m_CompNum++;
					num++;
				}
			}
			this.m_Badge.Apply(this.GetPreCompNum());
			return num;
		}

		// Token: 0x0600357E RID: 13694 RVA: 0x0010F018 File Offset: 0x0010D418
		public void CompleteMission(long fmanageid)
		{
			MissionManager.CompleteMission(fmanageid);
			for (int i = 0; i < this.m_ListUp.Count; i++)
			{
				if (this.m_ListUp[i].m_ManageID == fmanageid)
				{
					this.m_ListUp[i].m_State = eMissionState.Comp;
					break;
				}
			}
			this.m_PreCompNum--;
			this.m_CompNum++;
			this.m_Badge.Apply(this.GetPreCompNum());
			this.SetUMissionListInfo();
		}

		// Token: 0x0600357F RID: 13695 RVA: 0x0010F0A8 File Offset: 0x0010D4A8
		public void RefreshList()
		{
			this.m_Scroll.RemoveAll();
			for (int i = 0; i < this.m_ListUp.Count; i++)
			{
				MissionScrollItemData missionScrollItemData = new MissionScrollItemData();
				missionScrollItemData.m_UIData = this.m_ListUp[i];
				missionScrollItemData.m_RootList = this;
				this.m_Scroll.AddItem(missionScrollItemData);
			}
		}

		// Token: 0x06003580 RID: 13696 RVA: 0x0010F108 File Offset: 0x0010D508
		public void ListRefresh(bool factivemenu)
		{
			MissionUIDataPack categoryToMissionUIData = MissionManager.Inst.GetCategoryToMissionUIData(this.m_Category);
			for (int i = 0; i < categoryToMissionUIData.GetListNum(); i++)
			{
				bool flag = false;
				for (int j = 0; j < this.m_ListUp.Count; j++)
				{
					if (this.m_ListUp[j].m_ManageID == categoryToMissionUIData.m_Table[i].m_ManageID)
					{
						this.m_ListUp[j].m_State = categoryToMissionUIData.m_Table[i].m_State;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					this.m_ListUp.Add(categoryToMissionUIData.m_Table[i]);
					MissionScrollItemData missionScrollItemData = new MissionScrollItemData();
					missionScrollItemData.m_UIData = categoryToMissionUIData.m_Table[i];
					missionScrollItemData.m_RootList = this;
					this.m_Scroll.AddItem(missionScrollItemData);
				}
			}
			if (factivemenu)
			{
				this.SetUMissionListInfo();
			}
		}

		// Token: 0x06003581 RID: 13697 RVA: 0x0010F1F4 File Offset: 0x0010D5F4
		public void SetUMissionListInfo()
		{
			Text text = UIUtility.FindHrcObject(this.m_ListInfo.transform, "ListUp", typeof(Text)) as Text;
			if (text != null)
			{
				text.text = string.Format(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionSegMsgHaveFix), this.GetCompListNum(), this.GetAllListNum());
			}
		}

		// Token: 0x06003582 RID: 13698 RVA: 0x0010F268 File Offset: 0x0010D668
		public void SetUMissionLimitInfo()
		{
			Text text = UIUtility.FindHrcObject(this.m_LimitInfo.transform, "TimeUp", typeof(Text)) as Text;
			if (text != null)
			{
				switch (this.m_Category)
				{
				case eMissionCategory.Day:
				case eMissionCategory.Week:
					this.m_LimitInfo.SetActive(true);
					this.SetItemTime(text, this.m_LimitTime, false);
					break;
				case eMissionCategory.Unlock:
					this.m_LimitInfo.SetActive(false);
					break;
				case eMissionCategory.Event:
					if (this.GetAllListNum() == 0)
					{
						this.m_LimitInfo.SetActive(false);
					}
					else
					{
						this.m_LimitInfo.SetActive(true);
						this.SetItemTime(text, this.m_LimitTime, false);
					}
					break;
				}
			}
		}

		// Token: 0x06003583 RID: 13699 RVA: 0x0010F334 File Offset: 0x0010D734
		private void GetTimeTickToTimes(out int fday, out int fhour, out int fminute, out int fsec, long ftimes)
		{
			ftimes /= 10000000L;
			fday = (int)(ftimes / 86400L);
			ftimes %= 86400L;
			fhour = (int)(ftimes / 3600L);
			ftimes %= 3600L;
			fminute = (int)(ftimes / 60L);
			fsec = (int)(ftimes % 60L);
		}

		// Token: 0x06003584 RID: 13700 RVA: 0x0010F390 File Offset: 0x0010D790
		protected void SetItemTime(Text ptarget, DateTime fsettime, bool fdebugs)
		{
			long ticks;
			if (fdebugs)
			{
				ticks = ScheduleTimeUtil.GetManageUniversalTime().Ticks;
			}
			else
			{
				ticks = SingletonMonoBehaviour<GameSystem>.Inst.ServerTime.Ticks;
			}
			if (fsettime.Ticks > ticks)
			{
				ptarget.text = UIUtility.GetMissionLimitSpanString(fsettime - new DateTime(ticks));
			}
			else
			{
				ptarget.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MissionLimitTimeOver);
			}
		}

		// Token: 0x04003C17 RID: 15383
		private List<IMissionUIData> m_ListUp;

		// Token: 0x04003C18 RID: 15384
		private ScrollViewBase m_Scroll;

		// Token: 0x04003C19 RID: 15385
		private DateTime m_LimitTime;

		// Token: 0x04003C1A RID: 15386
		private int m_CompNum;

		// Token: 0x04003C1B RID: 15387
		private int m_PreCompNum;

		// Token: 0x04003C1C RID: 15388
		private eMissionCategory m_Category;

		// Token: 0x04003C1D RID: 15389
		private GameObject m_ListInfo;

		// Token: 0x04003C1E RID: 15390
		private GameObject m_LimitInfo;

		// Token: 0x04003C1F RID: 15391
		private Badge m_Badge;
	}
}
