﻿using System;

namespace Star.UI.Mission
{
	// Token: 0x02000A18 RID: 2584
	public class MissionPopUpMissionComplete : MissionPopUpWindow
	{
		// Token: 0x06003570 RID: 13680 RVA: 0x0010ED59 File Offset: 0x0010D159
		public void OnCloseButtonCallBack()
		{
			this.Close();
		}
	}
}
