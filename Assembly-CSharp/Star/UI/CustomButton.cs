﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Star.UI
{
	// Token: 0x0200082C RID: 2092
	[RequireComponent(typeof(InvisibleGraphic), typeof(ColorGroup))]
	public class CustomButton : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler, IEventSystemHandler
	{
		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06002B9B RID: 11163 RVA: 0x000E5C6C File Offset: 0x000E406C
		public ColorGroup ColorGroup
		{
			get
			{
				if (this.m_ColorGroup != null)
				{
					return this.m_ColorGroup;
				}
				this.m_ColorGroup = base.GetComponent<ColorGroup>();
				if (this.m_ColorGroup != null)
				{
					return this.m_ColorGroup;
				}
				return this.m_ColorGroup;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06002B9C RID: 11164 RVA: 0x000E5CBB File Offset: 0x000E40BB
		// (set) Token: 0x06002B9D RID: 11165 RVA: 0x000E5CD1 File Offset: 0x000E40D1
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInputParent && this.m_IsEnableInputSelf;
			}
			set
			{
				this.m_IsEnableInputSelf = value;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06002B9E RID: 11166 RVA: 0x000E5CDA File Offset: 0x000E40DA
		// (set) Token: 0x06002B9F RID: 11167 RVA: 0x000E5CE2 File Offset: 0x000E40E2
		public bool IsEnableInputParent
		{
			get
			{
				return this.m_IsEnableInputParent;
			}
			set
			{
				this.m_IsEnableInputParent = value;
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06002BA0 RID: 11168 RVA: 0x000E5CEB File Offset: 0x000E40EB
		// (set) Token: 0x06002BA1 RID: 11169 RVA: 0x000E5D01 File Offset: 0x000E4101
		public bool Interactable
		{
			get
			{
				return this.m_Interactable && this.m_InteractableParent;
			}
			set
			{
				this.m_Interactable = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06002BA2 RID: 11170 RVA: 0x000E5D10 File Offset: 0x000E4110
		// (set) Token: 0x06002BA3 RID: 11171 RVA: 0x000E5D18 File Offset: 0x000E4118
		public bool InteractableParent
		{
			get
			{
				return this.m_InteractableParent;
			}
			set
			{
				this.m_InteractableParent = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06002BA5 RID: 11173 RVA: 0x000E5D36 File Offset: 0x000E4136
		// (set) Token: 0x06002BA4 RID: 11172 RVA: 0x000E5D27 File Offset: 0x000E4127
		public bool IsDecided
		{
			get
			{
				return this.m_IsDecided;
			}
			set
			{
				this.m_IsDecided = value;
				this.ApplyStandardColor();
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06002BA6 RID: 11174 RVA: 0x000E5D3E File Offset: 0x000E413E
		// (set) Token: 0x06002BA7 RID: 11175 RVA: 0x000E5D46 File Offset: 0x000E4146
		public bool IgnoreGroup
		{
			get
			{
				return this.m_IsIgnoreGroup;
			}
			set
			{
				this.m_IsIgnoreGroup = value;
			}
		}

		// Token: 0x06002BA8 RID: 11176 RVA: 0x000E5D4F File Offset: 0x000E414F
		public void AddListerner(UnityAction onClickCallBack)
		{
			this.m_OnClick.AddListener(onClickCallBack);
		}

		// Token: 0x06002BA9 RID: 11177 RVA: 0x000E5D5D File Offset: 0x000E415D
		public void RemoveListener(UnityAction onClickCallBack)
		{
			this.m_OnClick.RemoveListener(onClickCallBack);
		}

		// Token: 0x06002BAA RID: 11178 RVA: 0x000E5D6C File Offset: 0x000E416C
		private void ApplyPreset()
		{
			if (this.m_IsDoneApplyPreset)
			{
				return;
			}
			this.m_IsDoneApplyPreset = true;
			switch (this.m_Preset)
			{
			case CustomButton.ePreset.CommonButton:
				this.m_TransitSec = 0.2f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_ExtendWidth = 0f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				this.m_DecideColorChange = false;
				break;
			case CustomButton.ePreset.Button:
				this.m_TransitSec = 0.2f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_ExtendWidth = 0f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				this.m_DecideColorChange = false;
				break;
			case CustomButton.ePreset.Icon:
				this.m_TransitSec = 0.2f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_ExtendWidth = 0f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				this.m_DecideColorChange = false;
				break;
			case CustomButton.ePreset.Panel:
				this.m_TransitSec = 0.2f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_ExtendWidth = 0f;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(1f, 0.7f, 0.4f);
				this.m_ColorTintMaterial = null;
				this.m_PlaySwapObj = true;
				this.m_DecideColorChange = false;
				break;
			case CustomButton.ePreset.Tab:
				this.m_TransitSec = 0.2f;
				this.m_PlayMove = false;
				this.m_PlayScale = false;
				this.m_PlayExtend = false;
				this.m_PlayAnim = false;
				this.m_PlayColorTint = true;
				this.m_ColorTintMaterial = null;
				this.m_PressColor = new Color(0.75f, 0.75f, 0.75f);
				this.m_DecideColor = new Color(0.7f, 1f, 0.4f);
				this.m_PlaySwapObj = false;
				break;
			}
			this.Interactable = this.m_Interactable;
		}

		// Token: 0x06002BAB RID: 11179 RVA: 0x000E6058 File Offset: 0x000E4458
		private void Start()
		{
			this.ApplyPreset();
			this.m_SwapObjs = base.GetComponentsInChildren<SwapObj>(true);
			this.m_GraphForButtons = base.GetComponentsInChildren<GraphForButton>(true);
			if (this.m_CurrentButtonState == CustomButton.eButtonState.None)
			{
				this.ChangeButtonState(CustomButton.eButtonState.Wait);
			}
			if (this.m_BodyRectTransform == null)
			{
				this.m_BodyRectTransform = (RectTransform)base.GetComponent<RectTransform>().Find("body");
			}
			if (this.m_BodyRectTransform != null)
			{
				this.m_DefaultLocalPosition = this.m_BodyRectTransform.localPosition;
				this.m_DefaultLocalScale = this.m_BodyRectTransform.localScale;
			}
			this.ApplyStandardColor();
		}

		// Token: 0x06002BAC RID: 11180 RVA: 0x000E60FC File Offset: 0x000E44FC
		private void Update()
		{
			if (!this.IsEnableInput || (!this.Interactable && !this.m_EnableInputHoldOnNotInteractable) || (!this.m_EnableInputOnInputBlock && SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput()))
			{
				this.m_IsHold = false;
			}
			if (this.m_BodyRectTransform != null && this.m_DefaultRectWidth == 0f)
			{
				this.m_DefaultRectWidth = this.m_BodyRectTransform.rect.width;
			}
			this.m_HoldCount += Time.realtimeSinceStartup - this.m_BeforeTime;
			this.m_BeforeTime = Time.realtimeSinceStartup;
			if (this.m_IsHold)
			{
				if (this.m_HoldCount > 0.5f)
				{
					this.ExecuteHold();
					this.m_IsHold = false;
				}
			}
			else if (this.m_PlayAnim && this.m_CurrentButtonState == CustomButton.eButtonState.Release && (this.m_Animation == null || this.m_ReleaseAnim == null || !this.m_Animation.IsPlaying(this.m_ReleaseAnim.name)))
			{
				if (this.m_Animation != null)
				{
					this.m_Animation.Stop();
				}
				this.ChangeButtonState(CustomButton.eButtonState.Wait);
			}
			if (this.m_PlayMove || this.m_PlayScale || this.m_PlayExtend || this.m_PlayColorTint)
			{
				if (this.m_CurrentButtonState == CustomButton.eButtonState.Press || this.m_CurrentButtonState == CustomButton.eButtonState.Release)
				{
					float num;
					if (this.m_TransitSec > 0f)
					{
						num = Time.deltaTime * 1f / this.m_TransitSec;
					}
					else
					{
						num = 1f;
					}
					if (this.m_CurrentButtonState == CustomButton.eButtonState.Press)
					{
						this.m_TransitRate += num;
					}
					else if (this.m_CurrentButtonState == CustomButton.eButtonState.Release)
					{
						this.m_TransitRate -= num;
					}
					if (this.m_TransitRate > 1f)
					{
						this.m_TransitRate = 1f;
					}
					else if (this.m_TransitRate < 0f)
					{
						this.m_TransitRate = 0f;
						if (this.m_CurrentButtonState == CustomButton.eButtonState.Release)
						{
							this.ChangeButtonState(CustomButton.eButtonState.Wait);
						}
					}
					if (this.m_PlayMove)
					{
						Vector3 localPosition = this.m_DefaultLocalPosition + this.m_MoveVec * this.m_TransitRate;
						if (this.m_BodyRectTransform)
						{
							this.m_BodyRectTransform.localPosition = localPosition;
						}
					}
					if (this.m_PlayScale || this.m_PlayExtend)
					{
						Vector3 vector = this.m_DefaultLocalScale;
						if (this.m_PlayScale)
						{
							vector = this.m_DefaultLocalScale + (this.m_ScaleVec - this.m_DefaultLocalScale) * this.m_TransitRate;
						}
						if (this.m_PlayExtend && this.m_DefaultRectWidth > 0f)
						{
							float num2 = this.m_ExtendWidth / this.m_DefaultRectWidth;
							vector *= 1f + num2 * this.m_TransitRate;
						}
						if (this.m_BodyRectTransform)
						{
							this.m_BodyRectTransform.localScale = vector;
						}
					}
					if (this.m_PlayColorTint)
					{
						this.ApplyStandardColor();
					}
				}
				else if (this.m_BodyRectTransform)
				{
					this.m_BodyRectTransform.localPosition = this.m_DefaultLocalPosition;
					this.m_BodyRectTransform.localScale = this.m_DefaultLocalScale;
				}
			}
			if (this.m_CurrentButtonState == CustomButton.eButtonState.Press && InputTouch.Inst != null && InputTouch.Inst.GetAvailableTouchCnt() == 0)
			{
				this.ChangeButtonState(CustomButton.eButtonState.Release);
			}
		}

		// Token: 0x06002BAD RID: 11181 RVA: 0x000E64B8 File Offset: 0x000E48B8
		public void OnPointerClick(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!this.IsEnableInput)
			{
				return;
			}
			if (!this.m_EnableInputOnInputBlock && SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput())
			{
				return;
			}
			if (!this.Interactable)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_NG, 1f, 0, -1, -1);
			}
			if (!this.Interactable && !this.m_EnableInputOnNotInteractable)
			{
				return;
			}
			if (!this.m_IsHold)
			{
				return;
			}
			if (this.m_HoldCount > 0.5f)
			{
				this.ExecuteHold();
				this.m_IsHold = false;
			}
			else
			{
				this.ExecuteClick();
				this.m_IsHold = false;
			}
			this.ApplyStandardColor();
			if (this.m_CurrentButtonState == CustomButton.eButtonState.Press)
			{
				this.ChangeButtonState(CustomButton.eButtonState.Release);
				this.ResetHold();
			}
		}

		// Token: 0x06002BAE RID: 11182 RVA: 0x000E6598 File Offset: 0x000E4998
		public void OnPointerDown(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!this.IsEnableInput)
			{
				return;
			}
			if (!this.m_EnableInputOnInputBlock && SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.IsBlockInput())
			{
				return;
			}
			if (!this.Interactable && !this.m_EnableInputOnNotInteractable && !this.m_EnableInputHoldOnNotInteractable)
			{
				return;
			}
			this.ApplyStandardColor();
			this.ChangeButtonState(CustomButton.eButtonState.Press);
			this.ResetHold();
			this.m_IsHold = true;
		}

		// Token: 0x06002BAF RID: 11183 RVA: 0x000E661A File Offset: 0x000E4A1A
		public void OnPointerUp(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (!eventData.IsPointerMoving() && this.m_CurrentButtonState == CustomButton.eButtonState.Press)
			{
				this.ForceRelease();
				this.m_IsHold = false;
			}
		}

		// Token: 0x06002BB0 RID: 11184 RVA: 0x000E664D File Offset: 0x000E4A4D
		public void ForceRelease()
		{
			this.ApplyStandardColor();
			if (this.m_CurrentButtonState == CustomButton.eButtonState.Press && this.m_HoldCount < 0.5f)
			{
				this.ExecuteClick();
				this.m_IsHold = false;
			}
			this.m_IsHold = false;
			this.ChangeButtonState(CustomButton.eButtonState.Release);
		}

		// Token: 0x06002BB1 RID: 11185 RVA: 0x000E668C File Offset: 0x000E4A8C
		public void OnPointerExit(PointerEventData eventData)
		{
			if (eventData.pointerId > 0)
			{
				return;
			}
			if (this.m_CurrentButtonState == CustomButton.eButtonState.Press)
			{
				this.ApplyStandardColor();
				this.ChangeButtonState(CustomButton.eButtonState.Release);
				this.m_IsHold = false;
			}
		}

		// Token: 0x06002BB2 RID: 11186 RVA: 0x000E66BC File Offset: 0x000E4ABC
		public void ForceEnableHold(bool flg)
		{
			if (!this.IsEnableInput || (!this.Interactable && !this.m_EnableInputHoldOnNotInteractable))
			{
				return;
			}
			if (flg)
			{
				this.ChangeButtonState(CustomButton.eButtonState.Press);
				this.ResetHold();
				this.m_TransitRate = 0f;
			}
			else if (this.m_CurrentButtonState == CustomButton.eButtonState.Press)
			{
				this.ChangeButtonState(CustomButton.eButtonState.Release);
				this.ResetHold();
			}
		}

		// Token: 0x06002BB3 RID: 11187 RVA: 0x000E6727 File Offset: 0x000E4B27
		private void OnEnable()
		{
			this.ResetHold();
		}

		// Token: 0x06002BB4 RID: 11188 RVA: 0x000E672F File Offset: 0x000E4B2F
		private void ResetHold()
		{
			this.m_IsHold = false;
			this.m_HoldCount = 0f;
			this.m_BeforeTime = Time.realtimeSinceStartup;
		}

		// Token: 0x06002BB5 RID: 11189 RVA: 0x000E6750 File Offset: 0x000E4B50
		public void ExecuteClick()
		{
			if (this.m_PlaySEClick && this.Interactable)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_ClickSound, 1f, 0, -1, -1);
			}
			if (this.m_OnClick != null)
			{
				this.m_OnClick.Invoke();
			}
			this.ForceEnableHold(false);
		}

		// Token: 0x06002BB6 RID: 11190 RVA: 0x000E67B0 File Offset: 0x000E4BB0
		private void ExecuteHold()
		{
			if (this.m_OnHold != null)
			{
				this.m_OnHold.Invoke();
			}
			if (this.m_PlaySEHold)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_HoldSound, 1f, 0, -1, -1);
			}
		}

		// Token: 0x06002BB7 RID: 11191 RVA: 0x000E67FC File Offset: 0x000E4BFC
		private void ChangeButtonState(CustomButton.eButtonState state)
		{
			if (this.m_CurrentButtonState != state && state == CustomButton.eButtonState.Press && this.m_PlaySEPress)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_PressSound, 1f, 0, -1, -1);
			}
			this.m_CurrentButtonState = state;
			if (this.m_PlayAnim)
			{
				AnimationClip clip = null;
				switch (state)
				{
				case CustomButton.eButtonState.Wait:
					clip = this.m_WaitAnim;
					break;
				case CustomButton.eButtonState.Press:
					clip = this.m_PressAnim;
					if (this.m_PlaySEPress)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(this.m_PressSound, 1f, 0, -1, -1);
					}
					break;
				case CustomButton.eButtonState.Release:
					clip = this.m_ReleaseAnim;
					break;
				case CustomButton.eButtonState.Decide:
					clip = this.m_DecideAnim;
					break;
				}
				this.PlayAnim(clip);
			}
			if (this.m_GraphForButtons != null && this.m_GraphForButtons.Length > 0)
			{
				GraphForButton.eState state2 = GraphForButton.eState.Default;
				if (!this.m_Interactable)
				{
					state2 = GraphForButton.eState.Disable;
				}
				else if (state == CustomButton.eButtonState.Press)
				{
					state2 = GraphForButton.eState.Press;
				}
				for (int i = 0; i < this.m_GraphForButtons.Length; i++)
				{
					this.m_GraphForButtons[i].ChangeState(state2);
				}
			}
			if (this.m_PlaySwapObj)
			{
				CustomButton.eSwapObjState eSwapObjState = CustomButton.eSwapObjState.Default;
				if (!this.Interactable)
				{
					eSwapObjState = CustomButton.eSwapObjState.NotInteractable;
				}
				else if (this.m_CurrentButtonState == CustomButton.eButtonState.Press)
				{
					eSwapObjState = CustomButton.eSwapObjState.Press;
				}
				else if (this.m_IsDecided)
				{
					eSwapObjState = CustomButton.eSwapObjState.Decide;
				}
				if (this.m_SwapObjs != null && this.m_SwapObjs.Length > 0)
				{
					for (int j = 0; j < this.m_SwapObjs.Length; j++)
					{
						if (!this.m_SwapObjs[j].IgnoreCustomButton)
						{
							Transform transform = this.m_SwapObjs[j].GetComponent<Transform>();
							while (transform != null)
							{
								if (transform.GetComponent<CustomButton>() != null)
								{
									break;
								}
								transform = transform.parent;
							}
							if (!(transform.GetComponent<CustomButton>() != this))
							{
								if (eSwapObjState < (CustomButton.eSwapObjState)this.m_SwapObjs[j].GetTargetObjNum())
								{
									this.m_SwapObjs[j].Swap((int)eSwapObjState);
								}
								else if (eSwapObjState == CustomButton.eSwapObjState.Decide)
								{
									this.m_SwapObjs[j].Swap(2);
								}
							}
						}
					}
				}
			}
		}

		// Token: 0x06002BB8 RID: 11192 RVA: 0x000E6A58 File Offset: 0x000E4E58
		private void PlayAnim(AnimationClip clip)
		{
			if (this.m_Animation == null)
			{
				this.m_Animation = base.GetComponent<Animation>();
				if (this.m_Animation == null)
				{
					this.m_Animation = base.gameObject.AddComponent<Animation>();
					if (this.m_Animation == null)
					{
						return;
					}
				}
				if (this.m_WaitAnim != null)
				{
					this.m_Animation.AddClip(this.m_WaitAnim, this.m_WaitAnim.name);
				}
				if (this.m_PressAnim != null)
				{
					this.m_Animation.AddClip(this.m_PressAnim, this.m_PressAnim.name);
				}
				if (this.m_ReleaseAnim != null)
				{
					this.m_Animation.AddClip(this.m_ReleaseAnim, this.m_ReleaseAnim.name);
				}
				if (this.m_DecideAnim != null)
				{
					this.m_Animation.AddClip(this.m_DecideAnim, this.m_DecideAnim.name);
				}
			}
			this.m_Animation.playAutomatically = false;
			if (clip != null)
			{
				this.m_Animation.CrossFade(clip.name, 0.2f);
			}
			else
			{
				this.m_BodyRectTransform.localPosition = this.m_DefaultLocalPosition;
			}
		}

		// Token: 0x06002BB9 RID: 11193 RVA: 0x000E6BB0 File Offset: 0x000E4FB0
		private void ApplyStandardColor()
		{
			this.ApplyPreset();
			if (this.Interactable && this.ColorGroup != null && this.m_InteractableColorChange)
			{
				if (this.m_IsDecided && this.m_DecideColorChange)
				{
					if (this.ColorGroup != null)
					{
						if (this.m_ColorTintMaterial == null)
						{
							this.ColorGroup.ChangeColor(this.m_DecideColor, ColorGroup.eColorBlendMode.Mul, 1f);
						}
						else
						{
							this.ColorGroup.ChangeColor(this.m_DecideColor, this.m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set, 1f);
						}
						this.m_TransitRate = 0f;
					}
				}
				else if (this.ColorGroup != null && this.m_PlayColorTint)
				{
					if (this.m_ColorTintMaterial == null)
					{
						this.ColorGroup.ChangeColor(this.m_PressColor, ColorGroup.eColorBlendMode.Mul, this.m_TransitRate);
					}
					else
					{
						this.ColorGroup.ChangeColor(this.m_PressColor, this.m_ColorTintMaterial, ColorGroup.eColorBlendMode.Set, this.m_TransitRate);
					}
				}
			}
			if (this.m_GraphForButtons != null)
			{
				for (int i = 0; i < this.m_GraphForButtons.Length; i++)
				{
					this.m_GraphForButtons[i].SetDecide(this.m_IsDecided);
					if (!this.Interactable)
					{
						this.m_GraphForButtons[i].ChangeState(GraphForButton.eState.Disable);
					}
					else
					{
						this.m_GraphForButtons[i].ChangeState(GraphForButton.eState.Default);
					}
				}
			}
			this.ChangeButtonState(this.m_CurrentButtonState);
			if (!this.Interactable && this.ColorGroup != null && this.m_InteractableColorChange)
			{
				this.ColorGroup.ChangeColor(Color.gray, null, ColorGroup.eColorBlendMode.Mul, 1f);
			}
		}

		// Token: 0x0400320C RID: 12812
		public const float SCROLL_THRESHOLD = 32f;

		// Token: 0x0400320D RID: 12813
		private const float m_HoldThreshold = 0.5f;

		// Token: 0x0400320E RID: 12814
		[SerializeField]
		private bool m_Interactable = true;

		// Token: 0x0400320F RID: 12815
		private bool m_InteractableParent = true;

		// Token: 0x04003210 RID: 12816
		[SerializeField]
		private bool m_InteractableColorChange = true;

		// Token: 0x04003211 RID: 12817
		[SerializeField]
		private bool m_EnableInputOnNotInteractable;

		// Token: 0x04003212 RID: 12818
		[SerializeField]
		private bool m_EnableInputHoldOnNotInteractable;

		// Token: 0x04003213 RID: 12819
		[SerializeField]
		private bool m_EnableInputOnInputBlock;

		// Token: 0x04003214 RID: 12820
		[SerializeField]
		private bool m_IsIgnoreGroup;

		// Token: 0x04003215 RID: 12821
		[SerializeField]
		private RectTransform m_BodyRectTransform;

		// Token: 0x04003216 RID: 12822
		private CustomButton.eButtonState m_CurrentButtonState = CustomButton.eButtonState.Wait;

		// Token: 0x04003217 RID: 12823
		private float m_TransitRate;

		// Token: 0x04003218 RID: 12824
		[SerializeField]
		private CustomButton.ePreset m_Preset;

		// Token: 0x04003219 RID: 12825
		private bool m_IsDoneApplyPreset;

		// Token: 0x0400321A RID: 12826
		[SerializeField]
		private float m_TransitSec;

		// Token: 0x0400321B RID: 12827
		[SerializeField]
		private bool m_PlayMove;

		// Token: 0x0400321C RID: 12828
		[SerializeField]
		private Vector3 m_MoveVec = new Vector3(0f, -10f, 0f);

		// Token: 0x0400321D RID: 12829
		[SerializeField]
		private bool m_PlayScale;

		// Token: 0x0400321E RID: 12830
		[SerializeField]
		private Vector3 m_ScaleVec = Vector3.one;

		// Token: 0x0400321F RID: 12831
		[SerializeField]
		private bool m_PlayExtend = true;

		// Token: 0x04003220 RID: 12832
		[SerializeField]
		private float m_ExtendWidth = 5f;

		// Token: 0x04003221 RID: 12833
		[SerializeField]
		private bool m_PlayAnim;

		// Token: 0x04003222 RID: 12834
		[SerializeField]
		private AnimationClip m_WaitAnim;

		// Token: 0x04003223 RID: 12835
		[SerializeField]
		private AnimationClip m_PressAnim;

		// Token: 0x04003224 RID: 12836
		[SerializeField]
		private AnimationClip m_ReleaseAnim;

		// Token: 0x04003225 RID: 12837
		[SerializeField]
		private AnimationClip m_DecideAnim;

		// Token: 0x04003226 RID: 12838
		[SerializeField]
		private bool m_PlayColorTint;

		// Token: 0x04003227 RID: 12839
		[SerializeField]
		private Material m_ColorTintMaterial;

		// Token: 0x04003228 RID: 12840
		[SerializeField]
		private Color m_PressColor = Color.gray;

		// Token: 0x04003229 RID: 12841
		[SerializeField]
		private Color m_DecideColor = new Color(0.7f, 0.6f, 0.5f, 1f);

		// Token: 0x0400322A RID: 12842
		[SerializeField]
		private bool m_PlaySwapObj;

		// Token: 0x0400322B RID: 12843
		private SwapObj[] m_SwapObjs;

		// Token: 0x0400322C RID: 12844
		[SerializeField]
		private bool m_PlaySEPress;

		// Token: 0x0400322D RID: 12845
		[SerializeField]
		private eSoundSeListDB m_PressSound;

		// Token: 0x0400322E RID: 12846
		[SerializeField]
		private bool m_PlaySEClick;

		// Token: 0x0400322F RID: 12847
		[SerializeField]
		private eSoundSeListDB m_ClickSound;

		// Token: 0x04003230 RID: 12848
		[SerializeField]
		private bool m_PlaySEHold;

		// Token: 0x04003231 RID: 12849
		[SerializeField]
		private eSoundSeListDB m_HoldSound;

		// Token: 0x04003232 RID: 12850
		[SerializeField]
		public UnityEvent m_OnClick;

		// Token: 0x04003233 RID: 12851
		[SerializeField]
		public UnityEvent m_OnHold;

		// Token: 0x04003234 RID: 12852
		private GraphForButton[] m_GraphForButtons;

		// Token: 0x04003235 RID: 12853
		private ColorGroup m_ColorGroup;

		// Token: 0x04003236 RID: 12854
		private Animation m_Animation;

		// Token: 0x04003237 RID: 12855
		private bool m_IsHold;

		// Token: 0x04003238 RID: 12856
		private float m_HoldCount;

		// Token: 0x04003239 RID: 12857
		private float m_BeforeTime;

		// Token: 0x0400323A RID: 12858
		private bool m_IsEnableInputParent = true;

		// Token: 0x0400323B RID: 12859
		private bool m_IsEnableInputSelf = true;

		// Token: 0x0400323C RID: 12860
		private bool m_IsDecided;

		// Token: 0x0400323D RID: 12861
		private bool m_DecideColorChange = true;

		// Token: 0x0400323E RID: 12862
		private Vector3 m_DefaultLocalPosition;

		// Token: 0x0400323F RID: 12863
		private Vector3 m_DefaultLocalScale;

		// Token: 0x04003240 RID: 12864
		private float m_DefaultRectWidth;

		// Token: 0x0200082D RID: 2093
		private enum eButtonState
		{
			// Token: 0x04003242 RID: 12866
			None,
			// Token: 0x04003243 RID: 12867
			Wait,
			// Token: 0x04003244 RID: 12868
			Press,
			// Token: 0x04003245 RID: 12869
			Release,
			// Token: 0x04003246 RID: 12870
			Decide
		}

		// Token: 0x0200082E RID: 2094
		public enum ePreset
		{
			// Token: 0x04003248 RID: 12872
			None,
			// Token: 0x04003249 RID: 12873
			CommonButton,
			// Token: 0x0400324A RID: 12874
			Button,
			// Token: 0x0400324B RID: 12875
			Icon,
			// Token: 0x0400324C RID: 12876
			Panel,
			// Token: 0x0400324D RID: 12877
			Tab
		}

		// Token: 0x0200082F RID: 2095
		private enum eSwapObjState
		{
			// Token: 0x0400324F RID: 12879
			Default,
			// Token: 0x04003250 RID: 12880
			Press,
			// Token: 0x04003251 RID: 12881
			NotInteractable,
			// Token: 0x04003252 RID: 12882
			Decide
		}
	}
}
