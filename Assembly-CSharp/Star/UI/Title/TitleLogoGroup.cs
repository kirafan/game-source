﻿using System;
using UnityEngine;

namespace Star.UI.Title
{
	// Token: 0x02000AE0 RID: 2784
	public class TitleLogoGroup : UIGroup
	{
		// Token: 0x06003A43 RID: 14915 RVA: 0x001297C8 File Offset: 0x00127BC8
		public override void Open()
		{
			base.Open();
			this.m_IsEnd = false;
			this.m_IsSkipped = false;
			this.m_CurrentIndex = 0;
			for (int i = 0; i < this.m_LogoObjs.Length; i++)
			{
				this.m_LogoObjs[i].SetActive(false);
			}
			this.PlayInLogo();
		}

		// Token: 0x06003A44 RID: 14916 RVA: 0x00129820 File Offset: 0x00127C20
		public override void Update()
		{
			base.Update();
			if (base.State == UIGroup.eState.Idle)
			{
				switch (this.m_LogoStep)
				{
				case TitleLogoGroup.eLogoStep.In:
					if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
					{
						this.PlayLogoVoice();
						this.m_DispTimeCount = 0f;
						this.m_LogoStep = TitleLogoGroup.eLogoStep.Idle;
					}
					break;
				case TitleLogoGroup.eLogoStep.Idle:
					this.m_DispTimeCount += Time.deltaTime;
					if (this.m_DispTimeCount > 1.1f)
					{
						this.PlayOutLogo();
					}
					break;
				case TitleLogoGroup.eLogoStep.Out:
					if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
					{
						this.m_LogoObjs[this.m_CurrentIndex].SetActive(false);
						this.m_LogoStep = TitleLogoGroup.eLogoStep.End;
					}
					break;
				case TitleLogoGroup.eLogoStep.End:
					this.m_CurrentIndex++;
					this.PlayInLogo();
					break;
				}
			}
		}

		// Token: 0x06003A45 RID: 14917 RVA: 0x00129910 File Offset: 0x00127D10
		private void PlayInLogo()
		{
			if (this.m_CurrentIndex < this.m_LogoObjs.Length)
			{
				this.m_LogoObjs[this.m_CurrentIndex].SetActive(true);
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.SetFadeRatio(1f);
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeIn(this.LOGO_FADE_COLOR, 0.3f, null);
				this.m_LogoStep = TitleLogoGroup.eLogoStep.In;
			}
			else
			{
				this.m_IsEnd = true;
			}
			if (this.m_IsEnd)
			{
				this.Close();
			}
		}

		// Token: 0x06003A46 RID: 14918 RVA: 0x00129996 File Offset: 0x00127D96
		private void PlayOutLogo()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(this.LOGO_FADE_COLOR, 0.3f, null);
			this.m_LogoStep = TitleLogoGroup.eLogoStep.Out;
		}

		// Token: 0x06003A47 RID: 14919 RVA: 0x001299BC File Offset: 0x00127DBC
		private void PlayLogoVoice()
		{
			eSoundVoiceControllListDB id = eSoundVoiceControllListDB.Logo_A;
			switch (this.m_CurrentIndex)
			{
			case 0:
				id = eSoundVoiceControllListDB.Logo_A;
				break;
			case 1:
				id = eSoundVoiceControllListDB.Logo_B;
				break;
			case 2:
				id = eSoundVoiceControllListDB.Logo_C;
				break;
			case 3:
				id = eSoundVoiceControllListDB.Logo_D;
				break;
			}
			if (APIUtility.IsNeedSignup())
			{
				this.m_RequestCueSheetIndex = 0;
				this.m_RequestNamedIndex = -1;
			}
			if (this.m_RequestCueSheetIndex != -1 || this.m_RequestNamedIndex != -1)
			{
				int num;
				this.m_VoiceCtrlReqID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(id, out num, out num, this.m_RequestCueSheetIndex, this.m_RequestNamedIndex, false);
			}
			else
			{
				this.m_VoiceCtrlReqID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(id, out this.m_RequestCueSheetIndex, out this.m_RequestNamedIndex, -1, -1, false);
			}
		}

		// Token: 0x06003A48 RID: 14920 RVA: 0x00129A8A File Offset: 0x00127E8A
		public bool IsSkipped()
		{
			return this.m_IsSkipped;
		}

		// Token: 0x06003A49 RID: 14921 RVA: 0x00129A94 File Offset: 0x00127E94
		public void OnClick()
		{
			if (this.m_LogoStep <= TitleLogoGroup.eLogoStep.Idle)
			{
				this.PlayOutLogo();
			}
			this.m_IsEnd = true;
			this.m_IsSkipped = true;
			if (this.m_VoiceCtrlReqID != -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoiceCtrlReqID);
				this.m_VoiceCtrlReqID = -1;
			}
		}

		// Token: 0x040041AD RID: 16813
		private const float LOGO_DISPLAY_TIME = 1.1f;

		// Token: 0x040041AE RID: 16814
		private const float LOGO_FADE_TIME = 0.3f;

		// Token: 0x040041AF RID: 16815
		private readonly Color LOGO_FADE_COLOR = Color.white;

		// Token: 0x040041B0 RID: 16816
		[SerializeField]
		private GameObject[] m_LogoObjs;

		// Token: 0x040041B1 RID: 16817
		private int m_CurrentIndex;

		// Token: 0x040041B2 RID: 16818
		private float m_DispTimeCount;

		// Token: 0x040041B3 RID: 16819
		private int m_RequestCueSheetIndex = -1;

		// Token: 0x040041B4 RID: 16820
		private int m_RequestNamedIndex = -1;

		// Token: 0x040041B5 RID: 16821
		private int m_VoiceCtrlReqID = -1;

		// Token: 0x040041B6 RID: 16822
		private bool m_IsEnd;

		// Token: 0x040041B7 RID: 16823
		private bool m_IsSkipped;

		// Token: 0x040041B8 RID: 16824
		private TitleLogoGroup.eLogoStep m_LogoStep;

		// Token: 0x02000AE1 RID: 2785
		private enum eLogoStep
		{
			// Token: 0x040041BA RID: 16826
			Hide,
			// Token: 0x040041BB RID: 16827
			In,
			// Token: 0x040041BC RID: 16828
			Idle,
			// Token: 0x040041BD RID: 16829
			Out,
			// Token: 0x040041BE RID: 16830
			End
		}
	}
}
