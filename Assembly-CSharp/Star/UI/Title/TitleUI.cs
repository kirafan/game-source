﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Title
{
	// Token: 0x02000AE2 RID: 2786
	public class TitleUI : MenuUIBase
	{
		// Token: 0x140000EF RID: 239
		// (add) Token: 0x06003A4B RID: 14923 RVA: 0x00129B00 File Offset: 0x00127F00
		// (remove) Token: 0x06003A4C RID: 14924 RVA: 0x00129B38 File Offset: 0x00127F38
		public event Action OnClickGameStartButton;

		// Token: 0x140000F0 RID: 240
		// (add) Token: 0x06003A4D RID: 14925 RVA: 0x00129B70 File Offset: 0x00127F70
		// (remove) Token: 0x06003A4E RID: 14926 RVA: 0x00129BA8 File Offset: 0x00127FA8
		public event Action OnClickPlayerResetButton;

		// Token: 0x140000F1 RID: 241
		// (add) Token: 0x06003A4F RID: 14927 RVA: 0x00129BE0 File Offset: 0x00127FE0
		// (remove) Token: 0x06003A50 RID: 14928 RVA: 0x00129C18 File Offset: 0x00128018
		public event Action<string, string> OnClickPlayerMoveButton;

		// Token: 0x140000F2 RID: 242
		// (add) Token: 0x06003A51 RID: 14929 RVA: 0x00129C50 File Offset: 0x00128050
		// (remove) Token: 0x06003A52 RID: 14930 RVA: 0x00129C88 File Offset: 0x00128088
		public event Action OnClickCacheClearButton;

		// Token: 0x140000F3 RID: 243
		// (add) Token: 0x06003A53 RID: 14931 RVA: 0x00129CC0 File Offset: 0x001280C0
		// (remove) Token: 0x06003A54 RID: 14932 RVA: 0x00129CF8 File Offset: 0x001280F8
		public event Action OnSuccessPlayerMoveCallback;

		// Token: 0x140000F4 RID: 244
		// (add) Token: 0x06003A55 RID: 14933 RVA: 0x00129D30 File Offset: 0x00128130
		// (remove) Token: 0x06003A56 RID: 14934 RVA: 0x00129D68 File Offset: 0x00128168
		private event Action<bool> OnCloseAgreement;

		// Token: 0x06003A57 RID: 14935 RVA: 0x00129D9E File Offset: 0x0012819E
		public void Setup()
		{
			this.SetGoToNextSheetActive(true);
			this.m_IsAgreed = false;
			this.m_GameObject = base.gameObject;
			this.m_ActionAggregation.Setup();
		}

		// Token: 0x06003A58 RID: 14936 RVA: 0x00129DC8 File Offset: 0x001281C8
		private void Update()
		{
			switch (this.m_Step)
			{
			case TitleUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					if (!this.m_AnimList[i].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (!this.m_MainGroup.IsDonePlayIn)
				{
					flag = false;
				}
				if (flag)
				{
					this.m_DataClearBotton.Interactable = !APIUtility.IsNeedSignup();
					DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
					this.m_VersionText.text = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainUIVersionHeadText) + "1.0.3";
					this.m_IdText.text = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainUIIDHeadText);
					this.m_DataClearWindowMessage.text = dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageFrontCount) + dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageCount) + dbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearMessageAfterCount);
					this.m_IdText.gameObject.SetActive(!string.IsNullOrEmpty(AccessSaveData.Inst.MyCode));
					if (!string.IsNullOrEmpty(AccessSaveData.Inst.MyCode))
					{
						Text idText = this.m_IdText;
						idText.text += AccessSaveData.Inst.MyCode;
					}
					this.m_DataInheritanceWindowGroup.SetOnClickButtonCallback(new Action<int>(this.OnClickDataInheritanceWindowButtonCallback));
					this.m_DataInheritanceWindowGroup.SetOnPlayerMoveSuccessCallback(new Action(this.OnSuccessPlayerMove));
					this.m_ActionAggregation.PlayStart();
					this.ChangeStep(TitleUI.eStep.Idle);
				}
				break;
			}
			case TitleUI.eStep.AgreeOut:
				if (this.m_AgreeWindowGroup.IsDonePlayOut)
				{
					this.ChangeStep(TitleUI.eStep.Out);
				}
				break;
			case TitleUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (this.m_MainGroup.IsPlayingInOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(TitleUI.eStep.End);
				}
				if (this.m_IsAgreed)
				{
					this.OnCloseAgreement.Call(true);
				}
				break;
			}
			}
		}

		// Token: 0x06003A59 RID: 14937 RVA: 0x0012A014 File Offset: 0x00128414
		private void ChangeStep(TitleUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case TitleUI.eStep.Hide:
				this.SetEnableInput(false);
				this.m_GameObject.SetActive(false);
				break;
			case TitleUI.eStep.In:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case TitleUI.eStep.Idle:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case TitleUI.eStep.Out:
				this.m_GameObject.SetActive(true);
				this.SetEnableInput(false);
				break;
			case TitleUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x06003A5A RID: 14938 RVA: 0x0012A0C2 File Offset: 0x001284C2
		public void PlayInLogo()
		{
			this.m_LogoGroup.Open();
		}

		// Token: 0x06003A5B RID: 14939 RVA: 0x0012A0CF File Offset: 0x001284CF
		public bool IsPlayingLogo()
		{
			return this.m_LogoGroup.IsOpen();
		}

		// Token: 0x06003A5C RID: 14940 RVA: 0x0012A0DC File Offset: 0x001284DC
		public override void PlayIn()
		{
			this.ChangeStep(TitleUI.eStep.In);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayIn();
			}
			this.m_MainGroup.Open();
			this.SetGoToNextSheetActive(true);
		}

		// Token: 0x06003A5D RID: 14941 RVA: 0x0012A128 File Offset: 0x00128528
		public override void PlayOut()
		{
			this.ChangeStep(TitleUI.eStep.Out);
			for (int i = 0; i < this.m_AnimList.Length; i++)
			{
				this.m_AnimList[i].PlayOut();
			}
			this.m_MainGroup.Close();
		}

		// Token: 0x06003A5E RID: 14942 RVA: 0x0012A16D File Offset: 0x0012856D
		public override bool IsMenuEnd()
		{
			return this.m_Step == TitleUI.eStep.End;
		}

		// Token: 0x06003A5F RID: 14943 RVA: 0x0012A178 File Offset: 0x00128578
		public void SetGoToNextSheetActive(bool active)
		{
			if (this.m_GoToNextSheet != null)
			{
				this.m_GoToNextSheet.gameObject.SetActive(active);
			}
			if (this.m_InputBlock != null)
			{
				this.m_InputBlock.SetActive(!active);
			}
		}

		// Token: 0x06003A60 RID: 14944 RVA: 0x0012A1C8 File Offset: 0x001285C8
		public void OnClickStartButtonCallBack()
		{
			if (this.m_ActionAggregation.IsFinished())
			{
				this.m_RequestCueSheetIndex = this.m_TitleCallAction.GetRequestCueSheetIndex();
				this.m_RequestNamedIndex = this.m_TitleCallAction.GetRequestNamedIndex();
				if (this.m_RequestCueSheetIndex != -1 || this.m_RequestNamedIndex != -1)
				{
					int num;
					SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleStart, out num, out num, this.m_RequestCueSheetIndex, this.m_RequestNamedIndex, false);
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundVoiceControllListDB.TitleStart, out this.m_RequestCueSheetIndex, out this.m_RequestNamedIndex, -1, -1, false);
				}
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_TITLE_DECISION, 1f, 0, -1, -1);
				this.SetGoToNextSheetActive(false);
				this.OnClickGameStartButton.Call();
			}
			else
			{
				this.m_ActionAggregation.Skip();
			}
		}

		// Token: 0x06003A61 RID: 14945 RVA: 0x0012A2A5 File Offset: 0x001286A5
		public void OpenAgreement(Action<bool> onCloseAgreementCallback)
		{
			this.OnCloseAgreement = onCloseAgreementCallback;
			this.m_AgreeWindowGroup.Open();
		}

		// Token: 0x06003A62 RID: 14946 RVA: 0x0012A2BC File Offset: 0x001286BC
		public void OnClickAgreementWindowButtonCallback(int index)
		{
			if (index == 2)
			{
				string idtoAdress = WebDataListUtil.GetIDToAdress(1000);
				if (idtoAdress != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.WebView.Open(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.WVAgreementTitle), idtoAdress);
				}
			}
			else if (index == 1)
			{
				this.m_AgreeWindowGroup.Close();
				this.OnCloseAgreement.Call(false);
			}
			else if (index == 0)
			{
				this.m_AgreeWindowGroup.Close();
				this.m_IsAgreed = true;
				this.ChangeStep(TitleUI.eStep.AgreeOut);
			}
		}

		// Token: 0x06003A63 RID: 14947 RVA: 0x0012A34C File Offset: 0x0012874C
		public CustomButton GetCacheClearButton()
		{
			return this.m_CacheClearButton;
		}

		// Token: 0x06003A64 RID: 14948 RVA: 0x0012A354 File Offset: 0x00128754
		public void OnClickCacheClearButtonCallback()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainCacheClearTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainCacheClearMessage), string.Empty, new Action<int>(this.OnClickCheckCacheClearWindowButtonCallback));
		}

		// Token: 0x06003A65 RID: 14949 RVA: 0x0012A3AA File Offset: 0x001287AA
		public void OnClickCheckCacheClearWindowButtonCallback(int index)
		{
			if (index == 0)
			{
				this.OnClickCacheClearButton();
			}
		}

		// Token: 0x06003A66 RID: 14950 RVA: 0x0012A3C2 File Offset: 0x001287C2
		public void OnClickDataInheritanceButtonCallback()
		{
			this.m_DataInheritanceWindowGroup.Open();
		}

		// Token: 0x06003A67 RID: 14951 RVA: 0x0012A3CF File Offset: 0x001287CF
		public void OnSuccessPlayerMove()
		{
			this.OnSuccessPlayerMoveCallback.Call();
		}

		// Token: 0x06003A68 RID: 14952 RVA: 0x0012A3DC File Offset: 0x001287DC
		public void OnClickDataInheritanceWindowButtonCallback(int index)
		{
			if (index == 0)
			{
				this.OnClickPlayerMoveButton(this.m_DataInheritanceWindowGroup.GetIdString(), this.m_DataInheritanceWindowGroup.GetPasswordString());
			}
			else
			{
				this.m_DataInheritanceWindowGroup.Close();
			}
		}

		// Token: 0x06003A69 RID: 14953 RVA: 0x0012A415 File Offset: 0x00128815
		public void OnClickDataClearButtonCallback()
		{
			this.m_DataClearWindowGroup.Open();
		}

		// Token: 0x06003A6A RID: 14954 RVA: 0x0012A424 File Offset: 0x00128824
		public void OnClickDataClearWindowButtonCallback(int index)
		{
			this.m_DataClearWindowGroup.Close();
			if (index == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearFinalTitle), SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainDataClearFinalMessage), string.Empty, new Action<int>(this.OnClickDataClearFinalWindowButtonCallback));
			}
		}

		// Token: 0x06003A6B RID: 14955 RVA: 0x0012A48B File Offset: 0x0012888B
		public void OnClickDataClearFinalWindowButtonCallback(int index)
		{
			if (index == 0)
			{
				this.OnClickPlayerResetButton();
			}
		}

		// Token: 0x06003A6C RID: 14956 RVA: 0x0012A4A0 File Offset: 0x001288A0
		public bool IsOpenAnyWindow()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.IsActive || this.m_DataInheritanceWindowGroup.IsOpen() || this.m_DataClearWindowGroup.IsOpen() || this.m_AgreeWindowGroup.IsOpen();
		}

		// Token: 0x040041BF RID: 16831
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x040041C0 RID: 16832
		[SerializeField]
		private UIGroup m_MainGroup;

		// Token: 0x040041C1 RID: 16833
		[SerializeField]
		private TitleLogoGroup m_LogoGroup;

		// Token: 0x040041C2 RID: 16834
		[SerializeField]
		private DataInheritenceUIGroup m_DataInheritanceWindowGroup;

		// Token: 0x040041C3 RID: 16835
		[SerializeField]
		private UIGroup m_DataClearWindowGroup;

		// Token: 0x040041C4 RID: 16836
		[SerializeField]
		private UIGroup m_AgreeWindowGroup;

		// Token: 0x040041C5 RID: 16837
		[SerializeField]
		private CustomButton m_CacheClearButton;

		// Token: 0x040041C6 RID: 16838
		[SerializeField]
		[Tooltip("データ削除はデータがないと押せなくなる.")]
		private CustomButton m_DataClearBotton;

		// Token: 0x040041C7 RID: 16839
		[SerializeField]
		[Tooltip("タイトルアニメーション.")]
		private ActionAggregation m_ActionAggregation;

		// Token: 0x040041C8 RID: 16840
		[SerializeField]
		private Text m_VersionText;

		// Token: 0x040041C9 RID: 16841
		[SerializeField]
		private Text m_IdText;

		// Token: 0x040041CA RID: 16842
		[SerializeField]
		[Tooltip("データ削除ウィンドウ内のメッセージ.")]
		private Text m_DataClearWindowMessage;

		// Token: 0x040041CB RID: 16843
		[SerializeField]
		private CustomButton m_GoToNextSheet;

		// Token: 0x040041CC RID: 16844
		[SerializeField]
		private GameObject m_InputBlock;

		// Token: 0x040041CD RID: 16845
		private TitleUI.eStep m_Step;

		// Token: 0x040041CE RID: 16846
		private int m_RequestCueSheetIndex = -1;

		// Token: 0x040041CF RID: 16847
		private int m_RequestNamedIndex = -1;

		// Token: 0x040041D0 RID: 16848
		[SerializeField]
		private ActionAggregationChildVoiceActionAdapter m_TitleCallAction;

		// Token: 0x040041D1 RID: 16849
		private bool m_IsAgreed;

		// Token: 0x02000AE3 RID: 2787
		private enum eStep
		{
			// Token: 0x040041D9 RID: 16857
			Hide,
			// Token: 0x040041DA RID: 16858
			Logo,
			// Token: 0x040041DB RID: 16859
			In,
			// Token: 0x040041DC RID: 16860
			Idle,
			// Token: 0x040041DD RID: 16861
			AgreeOut,
			// Token: 0x040041DE RID: 16862
			Out,
			// Token: 0x040041DF RID: 16863
			End
		}
	}
}
