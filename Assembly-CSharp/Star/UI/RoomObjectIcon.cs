﻿using System;

namespace Star.UI
{
	// Token: 0x02000870 RID: 2160
	public class RoomObjectIcon : ASyncImage
	{
		// Token: 0x06002CC2 RID: 11458 RVA: 0x000EC148 File Offset: 0x000EA548
		public void Apply(eRoomObjectCategory category, int roomObjectId)
		{
			if (category != this.m_RoomObjectCategory || this.m_RoomObjectId != roomObjectId)
			{
				this.Apply();
				if (roomObjectId != -1)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncRoomObjectIcon(category, roomObjectId);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_RoomObjectCategory = category;
				this.m_RoomObjectId = roomObjectId;
			}
		}

		// Token: 0x06002CC3 RID: 11459 RVA: 0x000EC1C2 File Offset: 0x000EA5C2
		public override void Destroy()
		{
			base.Destroy();
			this.m_RoomObjectCategory = eRoomObjectCategory.Appliances;
			this.m_RoomObjectId = -1;
		}

		// Token: 0x040033B7 RID: 13239
		protected eRoomObjectCategory m_RoomObjectCategory = eRoomObjectCategory.Appliances;

		// Token: 0x040033B8 RID: 13240
		protected int m_RoomObjectId = -1;
	}
}
