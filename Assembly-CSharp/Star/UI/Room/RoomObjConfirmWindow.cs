﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A79 RID: 2681
	public class RoomObjConfirmWindow : UIGroup
	{
		// Token: 0x060037A8 RID: 14248 RVA: 0x0011A210 File Offset: 0x00118610
		public void Open(eRoomObjectCategory category, int objID, RoomObjConfirmWindow.eMode mode, bool enableNextPlacement, UnityAction OnClose)
		{
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_ObjNameTextObj.text = objectParam.m_Name;
			this.m_RoomObjectIcon.Apply(category, objID);
			base.Open();
			if (mode != RoomObjConfirmWindow.eMode.Store)
			{
				if (mode != RoomObjConfirmWindow.eMode.BuyAndPlacement)
				{
					if (mode == RoomObjConfirmWindow.eMode.BuyAndStock)
					{
						this.m_CustomWindow.ClearFooterButton();
						this.m_CustomWindow.SetFooterClose();
						this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyResultTitle));
						this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyResultText);
					}
				}
				else
				{
					this.m_CustomWindow.ClearFooterButton();
					this.m_CustomWindow.AddFooterButton(0, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonClose));
					if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
					{
						this.m_CustomWindow.AddFooterButton(1, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyResultNextPlacement));
						if (UserRoomUtil.GetUserRoomObjPlacementNum() >= 50)
						{
							this.m_CustomWindow.GetFooterButton(1).CustomButton.Interactable = false;
						}
						else if (UserRoomUtil.GetUserRoomObjData(category, objID).HaveNum >= (int)RoomObjectListUtil.GetObjectParam(category, objID).m_MaxNum)
						{
							this.m_CustomWindow.GetFooterButton(1).CustomButton.Interactable = false;
						}
						else
						{
							this.m_CustomWindow.GetFooterButton(1).CustomButton.Interactable = enableNextPlacement;
						}
					}
					this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyResultTitle));
					this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyResultPlacement);
				}
			}
			else
			{
				this.m_CustomWindow.ClearFooterButton();
				this.m_CustomWindow.SetFooterYesNo();
				this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomStoreResultTitle));
				this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomStoreResultText);
			}
			this.m_CustomWindow.OnClickFooterButton += this.OnClickFooterButtonCallBack;
			this.m_CloseAction = OnClose;
		}

		// Token: 0x060037A9 RID: 14249 RVA: 0x0011A454 File Offset: 0x00118854
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			this.m_CloseAction();
		}

		// Token: 0x060037AA RID: 14250 RVA: 0x0011A467 File Offset: 0x00118867
		public void OnClickFooterButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x04003E80 RID: 16000
		[SerializeField]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x04003E81 RID: 16001
		[SerializeField]
		private Text m_ObjNameTextObj;

		// Token: 0x04003E82 RID: 16002
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04003E83 RID: 16003
		private UnityAction m_CloseAction;

		// Token: 0x02000A7A RID: 2682
		public enum eMode
		{
			// Token: 0x04003E85 RID: 16005
			Store,
			// Token: 0x04003E86 RID: 16006
			BuyAndPlacement,
			// Token: 0x04003E87 RID: 16007
			BuyAndStock,
			// Token: 0x04003E88 RID: 16008
			Sell
		}
	}
}
