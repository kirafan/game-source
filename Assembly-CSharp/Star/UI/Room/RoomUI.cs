﻿using System;
using System.Collections.Generic;
using Star.UI.Schedule;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A90 RID: 2704
	public class RoomUI : MenuUIBase
	{
		// Token: 0x140000C3 RID: 195
		// (add) Token: 0x06003821 RID: 14369 RVA: 0x0011CE60 File Offset: 0x0011B260
		// (remove) Token: 0x06003822 RID: 14370 RVA: 0x0011CE98 File Offset: 0x0011B298
		public event Action OnClickMoveButton;

		// Token: 0x140000C4 RID: 196
		// (add) Token: 0x06003823 RID: 14371 RVA: 0x0011CED0 File Offset: 0x0011B2D0
		// (remove) Token: 0x06003824 RID: 14372 RVA: 0x0011CF08 File Offset: 0x0011B308
		public event Action OnClickInfoButton;

		// Token: 0x140000C5 RID: 197
		// (add) Token: 0x06003825 RID: 14373 RVA: 0x0011CF40 File Offset: 0x0011B340
		// (remove) Token: 0x06003826 RID: 14374 RVA: 0x0011CF78 File Offset: 0x0011B378
		public event Action OnClickPutAwayButton;

		// Token: 0x140000C6 RID: 198
		// (add) Token: 0x06003827 RID: 14375 RVA: 0x0011CFB0 File Offset: 0x0011B3B0
		// (remove) Token: 0x06003828 RID: 14376 RVA: 0x0011CFE8 File Offset: 0x0011B3E8
		public event Action OnClickPlacementButton;

		// Token: 0x140000C7 RID: 199
		// (add) Token: 0x06003829 RID: 14377 RVA: 0x0011D020 File Offset: 0x0011B420
		// (remove) Token: 0x0600382A RID: 14378 RVA: 0x0011D058 File Offset: 0x0011B458
		public event Action OnClickBuyButton;

		// Token: 0x140000C8 RID: 200
		// (add) Token: 0x0600382B RID: 14379 RVA: 0x0011D090 File Offset: 0x0011B490
		// (remove) Token: 0x0600382C RID: 14380 RVA: 0x0011D0C8 File Offset: 0x0011B4C8
		public event Action OnClickFlipButton;

		// Token: 0x140000C9 RID: 201
		// (add) Token: 0x0600382D RID: 14381 RVA: 0x0011D100 File Offset: 0x0011B500
		// (remove) Token: 0x0600382E RID: 14382 RVA: 0x0011D138 File Offset: 0x0011B538
		public event Action OnClickPlaceCancelButton;

		// Token: 0x140000CA RID: 202
		// (add) Token: 0x0600382F RID: 14383 RVA: 0x0011D170 File Offset: 0x0011B570
		// (remove) Token: 0x06003830 RID: 14384 RVA: 0x0011D1A8 File Offset: 0x0011B5A8
		public event Action OnCloseGreet;

		// Token: 0x140000CB RID: 203
		// (add) Token: 0x06003831 RID: 14385 RVA: 0x0011D1E0 File Offset: 0x0011B5E0
		// (remove) Token: 0x06003832 RID: 14386 RVA: 0x0011D218 File Offset: 0x0011B618
		public event Action OnCancelSelectBed;

		// Token: 0x140000CC RID: 204
		// (add) Token: 0x06003833 RID: 14387 RVA: 0x0011D250 File Offset: 0x0011B650
		// (remove) Token: 0x06003834 RID: 14388 RVA: 0x0011D288 File Offset: 0x0011B688
		public event Action OnOpenObjList;

		// Token: 0x140000CD RID: 205
		// (add) Token: 0x06003835 RID: 14389 RVA: 0x0011D2C0 File Offset: 0x0011B6C0
		// (remove) Token: 0x06003836 RID: 14390 RVA: 0x0011D2F8 File Offset: 0x0011B6F8
		public event Action OnClickScheduleButton;

		// Token: 0x140000CE RID: 206
		// (add) Token: 0x06003837 RID: 14391 RVA: 0x0011D330 File Offset: 0x0011B730
		// (remove) Token: 0x06003838 RID: 14392 RVA: 0x0011D368 File Offset: 0x0011B768
		public event Action<long[]> OnDecideLiveCharas;

		// Token: 0x140000CF RID: 207
		// (add) Token: 0x06003839 RID: 14393 RVA: 0x0011D3A0 File Offset: 0x0011B7A0
		// (remove) Token: 0x0600383A RID: 14394 RVA: 0x0011D3D8 File Offset: 0x0011B7D8
		public event Action OnClosedScheduleWindow;

		// Token: 0x140000D0 RID: 208
		// (add) Token: 0x0600383B RID: 14395 RVA: 0x0011D410 File Offset: 0x0011B810
		// (remove) Token: 0x0600383C RID: 14396 RVA: 0x0011D448 File Offset: 0x0011B848
		public event Action OnClickBackButton_Selecting;

		// Token: 0x140000D1 RID: 209
		// (add) Token: 0x0600383D RID: 14397 RVA: 0x0011D480 File Offset: 0x0011B880
		// (remove) Token: 0x0600383E RID: 14398 RVA: 0x0011D4B8 File Offset: 0x0011B8B8
		public event Action OnClickStoreAllButton;

		// Token: 0x140000D2 RID: 210
		// (add) Token: 0x0600383F RID: 14399 RVA: 0x0011D4F0 File Offset: 0x0011B8F0
		// (remove) Token: 0x06003840 RID: 14400 RVA: 0x0011D528 File Offset: 0x0011B928
		public event Action OnClickRoomChangeButton;

		// Token: 0x140000D3 RID: 211
		// (add) Token: 0x06003841 RID: 14401 RVA: 0x0011D560 File Offset: 0x0011B960
		// (remove) Token: 0x06003842 RID: 14402 RVA: 0x0011D598 File Offset: 0x0011B998
		public event Action OnClickFriendRoomButton;

		// Token: 0x140000D4 RID: 212
		// (add) Token: 0x06003843 RID: 14403 RVA: 0x0011D5D0 File Offset: 0x0011B9D0
		// (remove) Token: 0x06003844 RID: 14404 RVA: 0x0011D608 File Offset: 0x0011BA08
		public event Action OnClickTownButton;

		// Token: 0x140000D5 RID: 213
		// (add) Token: 0x06003845 RID: 14405 RVA: 0x0011D640 File Offset: 0x0011BA40
		// (remove) Token: 0x06003846 RID: 14406 RVA: 0x0011D678 File Offset: 0x0011BA78
		public event Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x06003847 RID: 14407 RVA: 0x0011D6B0 File Offset: 0x0011BAB0
		public void SetInteractableAllButton(bool flg)
		{
			CustomButton[] componentsInChildren = base.GetComponentsInChildren<CustomButton>(true);
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].Interactable = flg;
			}
		}

		// Token: 0x06003848 RID: 14408 RVA: 0x0011D6E4 File Offset: 0x0011BAE4
		public void RefreshTutorialStep()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(false);
			RoomUI.eRoomTutorialStep tutorialStep = this.m_TutorialStep;
			if (tutorialStep != RoomUI.eRoomTutorialStep.OpenSlide)
			{
				if (tutorialStep == RoomUI.eRoomTutorialStep.OpenShop)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
					this.m_ShopButton.GetComponent<TutorialTarget>().SetEnable(false);
				}
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
				this.m_SlideButton.GetComponent<TutorialTarget>().SetEnable(false);
			}
			if (!this.m_ShopUI.IsOpen() && this.m_StateController.NowState != 6 && !this.m_SlideMenu.GetComponent<SlideMenu>().IsOpen)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(true, 1f, true);
				this.m_IsDirtyTutorialUI = true;
				this.m_TutorialStep = RoomUI.eRoomTutorialStep.OpenSlide;
			}
			else if (!this.m_ShopUI.IsOpen() && this.m_StateController.NowState != 6 && this.m_SlideMenu.GetComponent<SlideMenu>().IsOpen)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(true, 1f, true);
				this.m_IsDirtyTutorialUI = true;
				this.m_TutorialStep = RoomUI.eRoomTutorialStep.OpenShop;
			}
			else
			{
				this.m_TutorialStep = RoomUI.eRoomTutorialStep.None;
			}
		}

		// Token: 0x06003849 RID: 14409 RVA: 0x0011D848 File Offset: 0x0011BC48
		public void ToggleSlideMenu()
		{
			this.m_SlideMenu.GetComponent<SlideMenu>().ToggleSlideMenu();
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				this.RefreshTutorialStep();
			}
		}

		// Token: 0x0600384A RID: 14410 RVA: 0x0011D874 File Offset: 0x0011BC74
		public void Setup(RoomMain owner, RoomShopListUI shopUI, long playerID)
		{
			this.m_Owner = owner;
			this.m_ShopUI = shopUI;
			this.m_ScheduleWindow.Setup();
			this.m_ScheduleWindow.OnChangeChara += this.OnDecideLiveMember;
			this.m_ScheduleWindow.OnClose += this.OnCloseScheduleCallBack;
			this.m_StateController.Setup(11, 0, true);
			this.m_StateController.AddOnExit(4, new Action(this.CloseMainWindow));
			this.m_StateController.AddOnExit(5, new Action(this.CloseSelectWindow));
			this.m_RoomChangeButton.Interactable = UserRoomUtil.IsHaveSubRoom(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID);
			this.m_FriendListWindow.OnRoomChange += this.OnRoomChangeCallBack;
			this.m_Invisible.SetActive(true);
			this.m_Greet.OnEnd += this.OnCloseGreetCallBack;
			if (playerID != -1L)
			{
				this.m_RoomChangeButton.gameObject.SetActive(false);
				this.m_ReturnButton.gameObject.SetActive(true);
				this.m_SlideMenu.SetActive(false);
				this.m_IsPlayerRoom = false;
				this.m_FriendInfo.gameObject.SetActive(true);
				this.m_FriendInfo.Apply(playerID);
			}
			else
			{
				this.m_IsPlayerRoom = true;
				this.m_RoomChangeButton.gameObject.SetActive(true);
				this.m_ReturnButton.gameObject.SetActive(false);
				this.m_SlideMenu.SetActive(true);
				this.m_FriendInfo.gameObject.SetActive(false);
			}
			this.m_SelectWindow.SetFriendmode(!this.m_IsPlayerRoom);
		}

		// Token: 0x0600384B RID: 14411 RVA: 0x0011DA20 File Offset: 0x0011BE20
		public override void PlayIn()
		{
			if (this.m_StateController.ChangeState(1))
			{
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					this.m_AnimList[i].PlayIn();
				}
				this.m_Owner.RefreshTutorialStep();
			}
		}

		// Token: 0x0600384C RID: 14412 RVA: 0x0011DA70 File Offset: 0x0011BE70
		public override void PlayOut()
		{
			if (this.m_StateController.ChangeState(2))
			{
				for (int i = 0; i < this.m_AnimList.Length; i++)
				{
					this.m_AnimList[i].PlayOut();
				}
				this.m_MainWindow.Close();
				this.m_SelectWindow.Close();
			}
		}

		// Token: 0x0600384D RID: 14413 RVA: 0x0011DACC File Offset: 0x0011BECC
		private void LateUpdate()
		{
			if (this.m_IsDirtyTutorialUI)
			{
				RoomUI.eRoomTutorialStep tutorialStep = this.m_TutorialStep;
				if (tutorialStep != RoomUI.eRoomTutorialStep.OpenSlide)
				{
					if (tutorialStep == RoomUI.eRoomTutorialStep.OpenShop)
					{
						this.m_ShopButton.GetComponent<TutorialTarget>().SetEnable(true);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftBottom);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowPosition(this.m_ShopButton.GetComponent<RectTransform>().position);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.ROOM_OPENSHOP);
					}
				}
				else
				{
					this.m_SlideButton.GetComponent<TutorialTarget>().SetEnable(true);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftBottom);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowPosition(this.m_SlideButton.GetComponent<RectTransform>().position);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableMessage(true);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetMessage(eTutorialMessageListDB.ROOM_OPENSHOP);
				}
				this.m_IsDirtyTutorialUI = false;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial() && this.m_TutorialStep == RoomUI.eRoomTutorialStep.OpenShop)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftBottom);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowPosition(this.m_ShopButton.GetComponent<RectTransform>().position);
			}
		}

		// Token: 0x0600384E RID: 14414 RVA: 0x0011DC58 File Offset: 0x0011C058
		private void Update()
		{
			for (int i = 0; i < this.m_GreetStack.Count; i++)
			{
				this.m_GreetStack[i].m_StockTime += Time.deltaTime;
				if (this.m_GreetStack[i].m_StockTime > 600f)
				{
					this.m_GreetStack.RemoveAt(i);
					i--;
				}
			}
			switch (this.m_StateController.NowState)
			{
			case 1:
			{
				bool flag = true;
				for (int j = 0; j < this.m_AnimList.Length; j++)
				{
					if (!this.m_AnimList[j].IsEnd)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.OpenMainWindow();
				}
				break;
			}
			case 2:
			{
				bool flag2 = true;
				for (int k = 0; k < this.m_AnimList.Length; k++)
				{
					if (!this.m_AnimList[k].IsEnd)
					{
						flag2 = false;
						break;
					}
				}
				if (flag2 && this.m_MainWindow.IsDonePlayOut)
				{
					this.m_StateController.ChangeState(3);
				}
				break;
			}
			case 4:
				this.OpenGreet();
				break;
			case 5:
				UIUtility.UpdateAndroidBackKey(this.m_SelectWindow.GetBuyCancelButton(), null);
				UIUtility.UpdateAndroidBackKey(this.m_SelectWindow.GetEditCancelButton(), null);
				break;
			case 10:
			{
				bool flag3 = false;
				this.m_UIHideCount += Time.deltaTime;
				Vector2 vector;
				if (this.m_UIHideCount > 0.1f && (InputTouch.Inst.DetectTriggerOfSingleTouch(out vector, 0) || UIUtility.GetInputAndroidBackKey(null)))
				{
					flag3 = true;
				}
				if (flag3)
				{
					this.EndUIHide();
				}
				break;
			}
			}
		}

		// Token: 0x0600384F RID: 14415 RVA: 0x0011DE48 File Offset: 0x0011C248
		public override bool IsMenuEnd()
		{
			return this.m_StateController.NowState == 3;
		}

		// Token: 0x06003850 RID: 14416 RVA: 0x0011DE58 File Offset: 0x0011C258
		public void SetEnableObjInput(bool flg)
		{
			this.m_Invisible.GetComponent<InvisibleGraphic>().raycastTarget = !flg;
		}

		// Token: 0x06003851 RID: 14417 RVA: 0x0011DE6E File Offset: 0x0011C26E
		public bool CheckRaycast()
		{
			return this.m_RaycastCheck.Check();
		}

		// Token: 0x06003852 RID: 14418 RVA: 0x0011DE7B File Offset: 0x0011C27B
		public void SetEnableGreet(bool flg)
		{
			this.m_IsEnableOpenGreet = flg;
		}

		// Token: 0x06003853 RID: 14419 RVA: 0x0011DE84 File Offset: 0x0011C284
		public void OpenMainWindow()
		{
			if (this.m_StateController.ChangeState(4))
			{
				this.SetEnableObjInput(true);
				this.m_MainWindow.Open();
			}
		}

		// Token: 0x06003854 RID: 14420 RVA: 0x0011DEA9 File Offset: 0x0011C2A9
		public void CloseMainWindow()
		{
			this.m_MainWindow.Close();
		}

		// Token: 0x06003855 RID: 14421 RVA: 0x0011DEB6 File Offset: 0x0011C2B6
		public UIGroup GetObjInfoWindow()
		{
			return this.m_ObjInfoWindow;
		}

		// Token: 0x06003856 RID: 14422 RVA: 0x0011DEBE File Offset: 0x0011C2BE
		public void OpenObjInfoWindow(eRoomObjectCategory category, int roomObjID)
		{
			if (this.m_StateController.ChangeState(6))
			{
				this.m_ObjInfoWindow.Open(category, roomObjID);
			}
		}

		// Token: 0x06003857 RID: 14423 RVA: 0x0011DEDE File Offset: 0x0011C2DE
		public UIGroup GetObjConfirmWindow()
		{
			return this.m_ObjConfirmWindow;
		}

		// Token: 0x06003858 RID: 14424 RVA: 0x0011DEE8 File Offset: 0x0011C2E8
		public void OpenObjConfirmWindow(eRoomObjectCategory category, int roomObjID, RoomObjConfirmWindow.eMode mode, UnityAction OnClose)
		{
			if (this.m_StateController.ChangeState(6))
			{
				this.m_ObjConfirmWindow.Open(category, roomObjID, mode, !SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)this.m_Owner.GetShopObjList().GetItem(category, roomObjID).cost), OnClose);
			}
		}

		// Token: 0x06003859 RID: 14425 RVA: 0x0011DF45 File Offset: 0x0011C345
		public void OpenObjConfirmWindow2(eRoomObjectCategory category, int roomObjID, RoomObjConfirmWindow.eMode mode, UnityAction OnClose)
		{
			if (this.m_StateController.ChangeState(6))
			{
				this.m_ObjConfirmWindow.Open(category, roomObjID, mode, false, OnClose);
			}
		}

		// Token: 0x0600385A RID: 14426 RVA: 0x0011DF69 File Offset: 0x0011C369
		public RoomObjBuyWindow GetObjBuyWindow()
		{
			return this.m_ObjBuyWindow;
		}

		// Token: 0x0600385B RID: 14427 RVA: 0x0011DF71 File Offset: 0x0011C371
		public void OpenObjBuyWindow(RoomObjShopList shopList, eRoomObjectCategory category, int roomObjID, UnityAction OnClose)
		{
			if (this.m_StateController.ChangeState(6))
			{
				this.m_ObjBuyWindow.Open(shopList.GetItem(category, roomObjID), RoomObjBuyWindow.eMode.Buy, OnClose);
			}
		}

		// Token: 0x0600385C RID: 14428 RVA: 0x0011DF9A File Offset: 0x0011C39A
		public RoomObjSelectWindow GetRoomObjSelectWindow()
		{
			return this.m_SelectWindow;
		}

		// Token: 0x0600385D RID: 14429 RVA: 0x0011DFA2 File Offset: 0x0011C3A2
		public void OpenSelectWindowInfo(eRoomObjectCategory category, int objID)
		{
			if (this.m_StateController.NowState == 5 || this.m_StateController.ChangeState(5))
			{
				this.m_SelectWindow.OpenInfo(category, objID);
			}
		}

		// Token: 0x0600385E RID: 14430 RVA: 0x0011DFD4 File Offset: 0x0011C3D4
		public void OpenSelectWindowEdit(eRoomObjectCategory category, int objID)
		{
			if (this.m_StateController.NowState == 5 || this.m_StateController.ChangeState(5))
			{
				bool canFlip = true;
				if (category == eRoomObjectCategory.WallDecoration)
				{
					canFlip = false;
				}
				this.m_SelectWindow.OpenEdit(category, objID, canFlip);
			}
		}

		// Token: 0x0600385F RID: 14431 RVA: 0x0011E01C File Offset: 0x0011C41C
		public void OpenSelectWindowBuy(eRoomObjectCategory category, int objID)
		{
			if (this.m_StateController.NowState == 5 || this.m_StateController.ChangeState(5))
			{
				bool canFlip = true;
				if (category == eRoomObjectCategory.WallDecoration)
				{
					canFlip = false;
				}
				this.m_SelectWindow.OpenBuy(category, objID, canFlip);
			}
		}

		// Token: 0x06003860 RID: 14432 RVA: 0x0011E064 File Offset: 0x0011C464
		public void OpenSelectWindowMission()
		{
			if (this.m_StateController.NowState == 5 || this.m_StateController.ChangeState(5))
			{
				this.m_SelectWindow.OpenMission();
			}
		}

		// Token: 0x06003861 RID: 14433 RVA: 0x0011E093 File Offset: 0x0011C493
		public void CloseSelectWindow()
		{
			this.m_SelectWindow.Close();
		}

		// Token: 0x06003862 RID: 14434 RVA: 0x0011E0A0 File Offset: 0x0011C4A0
		public void RequestGreet(long charaMngID, RoomGreet.eCategory category)
		{
			if (this.m_GreetStack != null)
			{
				bool flag = false;
				for (int i = 0; i < this.m_GreetStack.Count; i++)
				{
					if (this.m_GreetStack[i].m_Category == category)
					{
						bool flag2 = false;
						long[] roomInCharaManageID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInCharaManageID();
						for (int j = 0; j < roomInCharaManageID.Length; j++)
						{
							if (j > this.m_GreetStack[i].m_CharaMngIDs.Count)
							{
								break;
							}
							if (roomInCharaManageID[j] == charaMngID)
							{
								this.m_GreetStack[i].m_CharaMngIDs.Insert(j, charaMngID);
								flag2 = true;
							}
						}
						if (!flag2)
						{
							this.m_GreetStack[i].m_CharaMngIDs.Add(charaMngID);
						}
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					bool flag3 = false;
					for (int k = 0; k < this.m_GreetStack.Count; k++)
					{
						if (this.m_GreetStack[k].m_Category > category)
						{
							this.m_GreetStack.Insert(k, new RoomUI.GreetRequest(charaMngID, category));
							flag3 = true;
							break;
						}
					}
					if (!flag3)
					{
						this.m_GreetStack.Add(new RoomUI.GreetRequest(charaMngID, category));
					}
				}
			}
			else
			{
				this.m_GreetStack.Add(new RoomUI.GreetRequest(charaMngID, category));
			}
		}

		// Token: 0x06003863 RID: 14435 RVA: 0x0011E214 File Offset: 0x0011C614
		public void OpenGreet()
		{
			if (this.m_StateController.NowState == 4 && this.m_IsEnableOpenGreet && this.m_Greet.IsEnableOpen() && !this.m_Greet.IsOpen() && this.m_GreetStack != null && this.m_GreetStack.Count > 0)
			{
				if (this.m_GreetStack[0].m_Category != RoomGreet.eCategory.Night)
				{
					if (this.m_GreetStack[0].m_Category == RoomGreet.eCategory.Morning)
					{
						if (!LocalSaveData.Inst.TownSkipGreet)
						{
							this.m_SpecialTweet.PlayVoiceOnly(this.m_GreetStack[0].m_CharaMngIDs[0], eSoundVoiceListDB.voice_002);
						}
					}
					else
					{
						this.m_Greet.Open(this.m_GreetStack[0].m_CharaMngIDs, this.m_GreetStack[0].m_Category);
					}
				}
				this.m_GreetStack.RemoveAt(0);
			}
		}

		// Token: 0x06003864 RID: 14436 RVA: 0x0011E31C File Offset: 0x0011C71C
		public void OnCloseGreetCallBack()
		{
			this.OnCloseGreet.Call();
			this.OpenGreet();
		}

		// Token: 0x06003865 RID: 14437 RVA: 0x0011E32F File Offset: 0x0011C72F
		public void SetEnablePlacementButton(bool flg)
		{
			this.m_SelectWindow.SetPlaceButtonEnable(flg);
		}

		// Token: 0x06003866 RID: 14438 RVA: 0x0011E33D File Offset: 0x0011C73D
		public ADVButton GetADVButton()
		{
			return null;
		}

		// Token: 0x06003867 RID: 14439 RVA: 0x0011E340 File Offset: 0x0011C740
		public void OnClickMoveButtonCallBack()
		{
			this.OnClickMoveButton.Call();
		}

		// Token: 0x06003868 RID: 14440 RVA: 0x0011E34D File Offset: 0x0011C74D
		public void OnClickInfoButtonCallBack()
		{
			this.OnClickInfoButton.Call();
		}

		// Token: 0x06003869 RID: 14441 RVA: 0x0011E35A File Offset: 0x0011C75A
		public void OnClickPutAwayButtonCallBack()
		{
			this.OnClickPutAwayButton.Call();
		}

		// Token: 0x0600386A RID: 14442 RVA: 0x0011E367 File Offset: 0x0011C767
		public void OnClickPlaceButtonCallBack()
		{
			this.OnClickPlacementButton.Call();
		}

		// Token: 0x0600386B RID: 14443 RVA: 0x0011E374 File Offset: 0x0011C774
		public void OnClickBuyButtonCallBack()
		{
			this.OnClickBuyButton.Call();
		}

		// Token: 0x0600386C RID: 14444 RVA: 0x0011E381 File Offset: 0x0011C781
		public void OnClickFlipButtonCallBack()
		{
			this.OnClickFlipButton.Call();
		}

		// Token: 0x0600386D RID: 14445 RVA: 0x0011E38E File Offset: 0x0011C78E
		public void OnClickPlaceCancelButtonCallBack()
		{
			this.OnClickPlaceCancelButton.Call();
		}

		// Token: 0x0600386E RID: 14446 RVA: 0x0011E39B File Offset: 0x0011C79B
		public void OnClickHaveButtonCallBack()
		{
			this.OpenMainWindow();
			this.OnOpenObjList.Call();
			this.m_ShopUI.SetMode(RoomObjListWindow.eMode.Have);
			this.m_ShopUI.PlayIn();
		}

		// Token: 0x0600386F RID: 14447 RVA: 0x0011E3C5 File Offset: 0x0011C7C5
		public void OnClickShopButtonCallBack()
		{
			this.OpenMainWindow();
			this.OnOpenObjList.Call();
			this.m_ShopUI.SetMode(RoomObjListWindow.eMode.Shop);
			this.m_ShopUI.PlayIn();
			this.RefreshTutorialStep();
		}

		// Token: 0x06003870 RID: 14448 RVA: 0x0011E3F5 File Offset: 0x0011C7F5
		public void OnClickStoreAllButtonCallBack()
		{
			this.OnClickStoreAllButton.Call();
		}

		// Token: 0x06003871 RID: 14449 RVA: 0x0011E402 File Offset: 0x0011C802
		public void OpenObjSelectGroup()
		{
			if (this.m_StateController.ChangeState(9))
			{
				this.m_ObjSelectGroup.Open();
			}
		}

		// Token: 0x06003872 RID: 14450 RVA: 0x0011E421 File Offset: 0x0011C821
		public void CloseObjSelectGroup()
		{
			this.m_ObjSelectGroup.Close();
		}

		// Token: 0x06003873 RID: 14451 RVA: 0x0011E42E File Offset: 0x0011C82E
		public void OnClickCancelObjSelect()
		{
			this.m_ObjSelectGroup.Close();
			this.OnCancelSelectBed.Call();
		}

		// Token: 0x06003874 RID: 14452 RVA: 0x0011E446 File Offset: 0x0011C846
		public void OnClickScheduleButtonCallBack()
		{
			this.OnClickScheduleButton.Call();
		}

		// Token: 0x06003875 RID: 14453 RVA: 0x0011E453 File Offset: 0x0011C853
		public void OpenScheduleWindow(int roomIdx, long[] liveCharaMngID)
		{
			if (this.m_StateController.ChangeState(8))
			{
				this.m_ScheduleWindow.Open(roomIdx, liveCharaMngID);
			}
		}

		// Token: 0x06003876 RID: 14454 RVA: 0x0011E473 File Offset: 0x0011C873
		public void OnDecideLiveMember()
		{
			this.OnDecideLiveCharas.Call(this.m_ScheduleWindow.LiveCharaMngIDs);
		}

		// Token: 0x06003877 RID: 14455 RVA: 0x0011E48B File Offset: 0x0011C88B
		public void OnCloseScheduleCallBack()
		{
			if (this.m_ScheduleWindow.MemberIdx == -1)
			{
				this.OnClosedScheduleWindow.Call();
				this.OpenMainWindow();
			}
		}

		// Token: 0x06003878 RID: 14456 RVA: 0x0011E4B0 File Offset: 0x0011C8B0
		public void StartUIHide()
		{
			if (this.m_StateController.ChangeState(10))
			{
				this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
				this.m_UIHideGroup.Open();
			}
		}

		// Token: 0x06003879 RID: 14457 RVA: 0x0011E500 File Offset: 0x0011C900
		public void EndUIHide()
		{
			if (this.m_StateController.NowState == 10)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
				this.m_UIHideGroup.Close();
				this.OpenMainWindow();
				this.m_UIHideCount = 0f;
			}
		}

		// Token: 0x0600387A RID: 14458 RVA: 0x0011E55F File Offset: 0x0011C95F
		public bool IsUIHide()
		{
			return this.m_StateController.NowState == 10;
		}

		// Token: 0x0600387B RID: 14459 RVA: 0x0011E570 File Offset: 0x0011C970
		public void OnClickRoomChangeButtonCallBack()
		{
			this.OnClickRoomChangeButton.Call();
		}

		// Token: 0x0600387C RID: 14460 RVA: 0x0011E57D File Offset: 0x0011C97D
		public void OnClickBackButtonCallBack_Main()
		{
			this.OnClickTownButton.Call();
		}

		// Token: 0x0600387D RID: 14461 RVA: 0x0011E58A File Offset: 0x0011C98A
		public void OnClickBackButtonCallBack_Selecting()
		{
			this.OnClickBackButton_Selecting.Call();
		}

		// Token: 0x0600387E RID: 14462 RVA: 0x0011E597 File Offset: 0x0011C997
		public void OnClickBackButtonCallBack_StoreEdit()
		{
			this.OnClickPlaceCancelButton.Call();
			this.m_ShopUI.PlayIn();
		}

		// Token: 0x0600387F RID: 14463 RVA: 0x0011E5AF File Offset: 0x0011C9AF
		public void OnClickBackButtonCallBack_ShopEdit()
		{
			this.OnClickPlaceCancelButton.Call();
			this.m_ShopUI.PlayIn();
		}

		// Token: 0x06003880 RID: 14464 RVA: 0x0011E5C7 File Offset: 0x0011C9C7
		public void OpenFriendListWindow()
		{
			this.m_FriendListWindow.Open(true);
		}

		// Token: 0x06003881 RID: 14465 RVA: 0x0011E5D5 File Offset: 0x0011C9D5
		public void OnRoomChangeCallBack()
		{
			this.m_FriendListWindow.Close();
			this.m_MainWindow.Close();
			this.OnClickFriendRoomButton.Call();
		}

		// Token: 0x06003882 RID: 14466 RVA: 0x0011E5F8 File Offset: 0x0011C9F8
		public void OnReturnPlayerRoomButtonCallBack()
		{
			this.m_FriendListWindow.Close();
			this.m_MainWindow.Close();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomPlayerID = -1L;
			this.OnRoomChangeCallBack();
		}

		// Token: 0x06003883 RID: 14467 RVA: 0x0011E627 File Offset: 0x0011CA27
		public void OpenSpecial(long mngID, int tweetID)
		{
			this.m_SpecialTweet.Play(mngID, tweetID);
		}

		// Token: 0x04003F14 RID: 16148
		[SerializeField]
		private UIStateController m_StateController;

		// Token: 0x04003F15 RID: 16149
		[SerializeField]
		private AnimUIPlayer[] m_AnimList;

		// Token: 0x04003F16 RID: 16150
		[SerializeField]
		private Text m_ClockTextObj;

		// Token: 0x04003F17 RID: 16151
		[SerializeField]
		private RoomMainWindow m_MainWindow;

		// Token: 0x04003F18 RID: 16152
		[SerializeField]
		private RoomGreet m_Greet;

		// Token: 0x04003F19 RID: 16153
		[SerializeField]
		private RoomObjSelectWindow m_SelectWindow;

		// Token: 0x04003F1A RID: 16154
		[SerializeField]
		private RoomObjInfoWindow m_ObjInfoWindow;

		// Token: 0x04003F1B RID: 16155
		[SerializeField]
		private RoomObjConfirmWindow m_ObjConfirmWindow;

		// Token: 0x04003F1C RID: 16156
		[SerializeField]
		private RoomObjBuyWindow m_ObjBuyWindow;

		// Token: 0x04003F1D RID: 16157
		[SerializeField]
		private UIGroup m_ObjSelectGroup;

		// Token: 0x04003F1E RID: 16158
		[SerializeField]
		private ScheduleWindow m_ScheduleWindow;

		// Token: 0x04003F1F RID: 16159
		[SerializeField]
		private UIGroup m_UIHideGroup;

		// Token: 0x04003F20 RID: 16160
		[SerializeField]
		private RoomFriendInfo m_FriendInfo;

		// Token: 0x04003F21 RID: 16161
		[SerializeField]
		private CustomButton m_HideButton;

		// Token: 0x04003F22 RID: 16162
		[SerializeField]
		private CustomButton m_RoomChangeButton;

		// Token: 0x04003F23 RID: 16163
		[SerializeField]
		private RoomFriendListWindow m_FriendListWindow;

		// Token: 0x04003F24 RID: 16164
		[SerializeField]
		private GameObject m_ReturnButton;

		// Token: 0x04003F25 RID: 16165
		[SerializeField]
		private GameObject m_SlideMenu;

		// Token: 0x04003F26 RID: 16166
		[SerializeField]
		private GameObject m_Invisible;

		// Token: 0x04003F27 RID: 16167
		[SerializeField]
		private SpecialTweet m_SpecialTweet;

		// Token: 0x04003F28 RID: 16168
		[SerializeField]
		private CustomButton m_SlideButton;

		// Token: 0x04003F29 RID: 16169
		[SerializeField]
		private CustomButton m_ShopButton;

		// Token: 0x04003F2A RID: 16170
		[SerializeField]
		private UIRaycastChecker m_RaycastCheck;

		// Token: 0x04003F2B RID: 16171
		private bool m_IsPlayerRoom = true;

		// Token: 0x04003F2C RID: 16172
		private List<RoomUI.GreetRequest> m_GreetStack = new List<RoomUI.GreetRequest>();

		// Token: 0x04003F2D RID: 16173
		private bool m_IsEnableOpenGreet;

		// Token: 0x04003F2E RID: 16174
		private RoomMain m_Owner;

		// Token: 0x04003F2F RID: 16175
		private float m_UIHideCount;

		// Token: 0x04003F30 RID: 16176
		private RoomShopListUI m_ShopUI;

		// Token: 0x04003F44 RID: 16196
		private RoomUI.eRoomTutorialStep m_TutorialStep;

		// Token: 0x04003F45 RID: 16197
		private bool m_IsDirtyTutorialUI;

		// Token: 0x02000A91 RID: 2705
		private enum eUIState
		{
			// Token: 0x04003F47 RID: 16199
			Hide,
			// Token: 0x04003F48 RID: 16200
			In,
			// Token: 0x04003F49 RID: 16201
			Out,
			// Token: 0x04003F4A RID: 16202
			End,
			// Token: 0x04003F4B RID: 16203
			Main,
			// Token: 0x04003F4C RID: 16204
			Select,
			// Token: 0x04003F4D RID: 16205
			Window,
			// Token: 0x04003F4E RID: 16206
			Greet,
			// Token: 0x04003F4F RID: 16207
			Schedule,
			// Token: 0x04003F50 RID: 16208
			ObjSelect,
			// Token: 0x04003F51 RID: 16209
			UIHide,
			// Token: 0x04003F52 RID: 16210
			Num
		}

		// Token: 0x02000A92 RID: 2706
		private class GreetRequest
		{
			// Token: 0x06003884 RID: 14468 RVA: 0x0011E636 File Offset: 0x0011CA36
			public GreetRequest(long charaMngID, RoomGreet.eCategory category)
			{
				this.m_CharaMngIDs.Add(charaMngID);
				this.m_Category = category;
			}

			// Token: 0x04003F53 RID: 16211
			public const float LIFE = 600f;

			// Token: 0x04003F54 RID: 16212
			public List<long> m_CharaMngIDs = new List<long>();

			// Token: 0x04003F55 RID: 16213
			public RoomGreet.eCategory m_Category;

			// Token: 0x04003F56 RID: 16214
			public float m_StockTime;
		}

		// Token: 0x02000A93 RID: 2707
		public enum eRoomTutorialStep
		{
			// Token: 0x04003F58 RID: 16216
			None,
			// Token: 0x04003F59 RID: 16217
			OpenSlide,
			// Token: 0x04003F5A RID: 16218
			OpenShop
		}
	}
}
