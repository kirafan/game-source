﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Star.UI.Room
{
	// Token: 0x02000A8B RID: 2699
	public class RoomShopConfirmGroup : UIGroup
	{
		// Token: 0x17000350 RID: 848
		// (get) Token: 0x0600380C RID: 14348 RVA: 0x0011C618 File Offset: 0x0011AA18
		public RoomShopConfirmGroup.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x0600380D RID: 14349 RVA: 0x0011C620 File Offset: 0x0011AA20
		public void Open(RoomObjShopList.RoomShopObjData shopObjData, UnityAction onClose)
		{
			this.Open(shopObjData.category, shopObjData.id, (long)shopObjData.cost, onClose);
		}

		// Token: 0x0600380E RID: 14350 RVA: 0x0011C63C File Offset: 0x0011AA3C
		public void Open(eRoomObjectCategory category, int objID, long amount, UnityAction OnClose)
		{
			this.m_RoomObjInfo.Apply(category, objID, amount);
			this.m_CloseAction = OnClose;
			UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(category, objID);
			if (UserRoomUtil.GetUserRoomObjPlacementNum() >= 50)
			{
				this.m_PlacementButton.Interactable = false;
			}
			else if (userRoomObjData != null && userRoomObjData.HaveNum >= (int)RoomObjectListUtil.GetObjectParam(category, objID).m_MaxNum)
			{
				this.m_PlacementButton.Interactable = false;
			}
			else
			{
				this.m_PlacementButton.Interactable = true;
			}
			if (UserRoomUtil.GetUserRoomObjRemainNum() >= SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit)
			{
				this.m_StoreButton.Interactable = false;
			}
			else if (userRoomObjData != null && userRoomObjData.HaveNum >= (int)RoomObjectListUtil.GetObjectParam(category, objID).m_MaxNum)
			{
				this.m_StoreButton.Interactable = false;
			}
			else
			{
				this.m_StoreButton.Interactable = true;
			}
			base.Open();
		}

		// Token: 0x0600380F RID: 14351 RVA: 0x0011C734 File Offset: 0x0011AB34
		public override void LateUpdate()
		{
			base.LateUpdate();
			if (base.IsIdle() && SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial() && !this.m_AttachedCanvas)
			{
				this.m_TutorialTarget.SetEnable(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowPosition(this.m_PlacementButton.GetComponent<RectTransform>().position);
				this.m_AttachedCanvas = true;
			}
		}

		// Token: 0x06003810 RID: 14352 RVA: 0x0011C7C3 File Offset: 0x0011ABC3
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_CloseAction != null)
			{
				this.m_CloseAction();
			}
		}

		// Token: 0x06003811 RID: 14353 RVA: 0x0011C7E1 File Offset: 0x0011ABE1
		public void OnClickStoreButtonCallBack()
		{
			this.m_SelectButton = RoomShopConfirmGroup.eButton.Store;
			this.Close();
		}

		// Token: 0x06003812 RID: 14354 RVA: 0x0011C7F0 File Offset: 0x0011ABF0
		public void OnClickPlaceButtonCallBack()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				this.m_TutorialTarget.SetEnable(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
				this.m_AttachedCanvas = true;
			}
			this.m_SelectButton = RoomShopConfirmGroup.eButton.Place;
			this.Close();
		}

		// Token: 0x06003813 RID: 14355 RVA: 0x0011C857 File Offset: 0x0011AC57
		public void OnClickCloseButtonCallBack()
		{
			this.m_SelectButton = RoomShopConfirmGroup.eButton.Close;
			this.Close();
		}

		// Token: 0x04003EF1 RID: 16113
		[SerializeField]
		private RoomObjInfo m_RoomObjInfo;

		// Token: 0x04003EF2 RID: 16114
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x04003EF3 RID: 16115
		[SerializeField]
		private CustomButton m_StoreButton;

		// Token: 0x04003EF4 RID: 16116
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x04003EF5 RID: 16117
		private RoomShopConfirmGroup.eButton m_SelectButton;

		// Token: 0x04003EF6 RID: 16118
		private UnityAction m_CloseAction;

		// Token: 0x04003EF7 RID: 16119
		private bool m_AttachedCanvas;

		// Token: 0x02000A8C RID: 2700
		public enum eButton
		{
			// Token: 0x04003EF9 RID: 16121
			None,
			// Token: 0x04003EFA RID: 16122
			Close,
			// Token: 0x04003EFB RID: 16123
			Store,
			// Token: 0x04003EFC RID: 16124
			Place
		}
	}
}
