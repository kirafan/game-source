﻿using System;

namespace Star.UI.Room
{
	// Token: 0x02000A80 RID: 2688
	public class RoomObjectItemData : ScrollItemData
	{
		// Token: 0x060037C1 RID: 14273 RVA: 0x0011ACA3 File Offset: 0x001190A3
		public RoomObjectItemData()
		{
			this.m_Mode = RoomObjectItemData.eMode.Have;
			this.m_ObjCategory = eRoomObjectCategory.Appliances;
			this.m_ObjID = -1;
			this.m_Cost = 0;
			this.m_IsSale = false;
			this.m_IsPickup = false;
		}

		// Token: 0x140000BE RID: 190
		// (add) Token: 0x060037C2 RID: 14274 RVA: 0x0011ACD8 File Offset: 0x001190D8
		// (remove) Token: 0x060037C3 RID: 14275 RVA: 0x0011AD10 File Offset: 0x00119110
		public event Action<eRoomObjectCategory, int> OnPlacement;

		// Token: 0x140000BF RID: 191
		// (add) Token: 0x060037C4 RID: 14276 RVA: 0x0011AD48 File Offset: 0x00119148
		// (remove) Token: 0x060037C5 RID: 14277 RVA: 0x0011AD80 File Offset: 0x00119180
		public event Action<eRoomObjectCategory, int> OnSell;

		// Token: 0x140000C0 RID: 192
		// (add) Token: 0x060037C6 RID: 14278 RVA: 0x0011ADB8 File Offset: 0x001191B8
		// (remove) Token: 0x060037C7 RID: 14279 RVA: 0x0011ADF0 File Offset: 0x001191F0
		public event Action<eRoomObjectCategory, int> OnBuy;

		// Token: 0x060037C8 RID: 14280 RVA: 0x0011AE26 File Offset: 0x00119226
		public void OnPlacementButtonCallBack()
		{
			this.OnPlacement.Call(this.m_ObjCategory, this.m_ObjID);
		}

		// Token: 0x060037C9 RID: 14281 RVA: 0x0011AE3F File Offset: 0x0011923F
		public void OnSellButtonCallBack()
		{
			this.OnSell.Call(this.m_ObjCategory, this.m_ObjID);
		}

		// Token: 0x060037CA RID: 14282 RVA: 0x0011AE58 File Offset: 0x00119258
		public void OnBuyButtonCallBack()
		{
			this.OnBuy.Call(this.m_ObjCategory, this.m_ObjID);
		}

		// Token: 0x04003EA4 RID: 16036
		public RoomObjectItemData.eMode m_Mode;

		// Token: 0x04003EA5 RID: 16037
		public eRoomObjectCategory m_ObjCategory;

		// Token: 0x04003EA6 RID: 16038
		public int m_ObjID;

		// Token: 0x04003EA7 RID: 16039
		public int m_Cost;

		// Token: 0x04003EA8 RID: 16040
		public bool m_IsSale;

		// Token: 0x04003EA9 RID: 16041
		public bool m_IsPickup;

		// Token: 0x02000A81 RID: 2689
		public enum eMode
		{
			// Token: 0x04003EAE RID: 16046
			Have,
			// Token: 0x04003EAF RID: 16047
			Shop
		}
	}
}
