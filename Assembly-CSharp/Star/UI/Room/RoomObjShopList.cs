﻿using System;
using System.Collections.Generic;
using Meige;
using RoomObjectRequestTypes;
using RoomObjectResponseTypes;
using WWWTypes;

namespace Star.UI.Room
{
	// Token: 0x02000A89 RID: 2697
	public class RoomObjShopList
	{
		// Token: 0x140000C1 RID: 193
		// (add) Token: 0x060037F1 RID: 14321 RVA: 0x0011BB9C File Offset: 0x00119F9C
		// (remove) Token: 0x060037F2 RID: 14322 RVA: 0x0011BBD4 File Offset: 0x00119FD4
		private event Action<long> OnResponseBuy;

		// Token: 0x140000C2 RID: 194
		// (add) Token: 0x060037F3 RID: 14323 RVA: 0x0011BC0C File Offset: 0x0011A00C
		// (remove) Token: 0x060037F4 RID: 14324 RVA: 0x0011BC44 File Offset: 0x0011A044
		private event Action OnResponseSale;

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x060037F5 RID: 14325 RVA: 0x0011BC7A File Offset: 0x0011A07A
		public bool IsAvailable
		{
			get
			{
				return this.m_IsAvailable;
			}
		}

		// Token: 0x060037F6 RID: 14326 RVA: 0x0011BC82 File Offset: 0x0011A082
		public void Open()
		{
			this.m_IsOpen = true;
			if (!this.m_IsAvailable)
			{
				this.Request_GetList(new MeigewwwParam.Callback(this.OnResponse_GetList));
			}
		}

		// Token: 0x060037F7 RID: 14327 RVA: 0x0011BCA8 File Offset: 0x0011A0A8
		public bool UpdateOpen()
		{
			return this.m_IsOpen && this.m_IsAvailable;
		}

		// Token: 0x060037F8 RID: 14328 RVA: 0x0011BCBD File Offset: 0x0011A0BD
		public void MarkRefresh()
		{
			this.m_IsAvailable = false;
			this.m_ObjList.Clear();
			this.m_FilteredObjList.Clear();
		}

		// Token: 0x060037F9 RID: 14329 RVA: 0x0011BCDC File Offset: 0x0011A0DC
		public RoomObjShopList.RoomShopObjData GetItemAt(int idx)
		{
			if (this.m_IsAvailable && this.m_ObjList.Count > idx)
			{
				return this.m_ObjList[idx];
			}
			return null;
		}

		// Token: 0x060037FA RID: 14330 RVA: 0x0011BD08 File Offset: 0x0011A108
		public RoomObjShopList.RoomShopObjData GetItem(eRoomObjectCategory category, int id)
		{
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				if (this.m_ObjList[i].category == category && this.m_ObjList[i].id == id)
				{
					return this.m_ObjList[i];
				}
			}
			return null;
		}

		// Token: 0x060037FB RID: 14331 RVA: 0x0011BD6D File Offset: 0x0011A16D
		public int GetItemNum()
		{
			if (this.m_IsAvailable)
			{
				return this.m_ObjList.Count;
			}
			return 0;
		}

		// Token: 0x060037FC RID: 14332 RVA: 0x0011BD88 File Offset: 0x0011A188
		public void ClearFilter()
		{
			this.m_FilteredObjList.Clear();
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				this.m_FilteredObjList.Add(this.m_ObjList[i]);
			}
		}

		// Token: 0x060037FD RID: 14333 RVA: 0x0011BDD4 File Offset: 0x0011A1D4
		public void DoFilter(eRoomObjectCategory category)
		{
			this.m_FilteredObjList.Clear();
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				if (this.m_ObjList[i].category == category)
				{
					this.m_FilteredObjList.Add(this.m_ObjList[i]);
				}
			}
		}

		// Token: 0x060037FE RID: 14334 RVA: 0x0011BE36 File Offset: 0x0011A236
		public List<RoomObjShopList.RoomShopObjData> GetFilteredItemList()
		{
			if (this.m_IsAvailable)
			{
				return this.m_FilteredObjList;
			}
			return null;
		}

		// Token: 0x060037FF RID: 14335 RVA: 0x0011BE4B File Offset: 0x0011A24B
		public RoomObjShopList.RoomShopObjData GetFilteredItem(int idx)
		{
			if (this.m_IsAvailable && this.m_FilteredObjList.Count > idx)
			{
				return this.m_FilteredObjList[idx];
			}
			return null;
		}

		// Token: 0x06003800 RID: 14336 RVA: 0x0011BE77 File Offset: 0x0011A277
		public int GetFilteredItemNum()
		{
			if (this.m_IsAvailable)
			{
				return this.m_FilteredObjList.Count;
			}
			return 0;
		}

		// Token: 0x06003801 RID: 14337 RVA: 0x0011BE94 File Offset: 0x0011A294
		public void Request_GetList(MeigewwwParam.Callback callback)
		{
			RoomObjectRequestTypes.GetList param = new RoomObjectRequestTypes.GetList();
			MeigewwwParam list = RoomObjectRequest.GetList(param, callback);
			NetworkQueueManager.Request(list);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06003802 RID: 14338 RVA: 0x0011BEC4 File Offset: 0x0011A2C4
		private void OnResponse_GetList(MeigewwwParam wwwParam)
		{
			RoomObjectResponseTypes.GetList list = RoomObjectResponse.GetList(wwwParam, ResponseCommon.DialogType.None, null);
			if (list == null)
			{
				return;
			}
			ResultCode result = list.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				this.m_ObjList = new List<RoomObjShopList.RoomShopObjData>();
				this.m_FilteredObjList = new List<RoomObjShopList.RoomShopObjData>();
				for (int i = 0; i < list.roomObjects.Length; i++)
				{
					int id = list.roomObjects[i].id;
					if (list.roomObjects[i].shopFlag != 0)
					{
						for (int j = 0; j < SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RoomObjectListDB.m_Params.Length; j++)
						{
							RoomObjectListDB_Param roomObjectListDB_Param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RoomObjectListDB.m_Params[j];
							if (roomObjectListDB_Param.m_DBAccessID == id)
							{
								RoomObjShopList.RoomShopObjData roomShopObjData = new RoomObjShopList.RoomShopObjData();
								roomShopObjData.category = (eRoomObjectCategory)roomObjectListDB_Param.m_Category;
								roomShopObjData.id = roomObjectListDB_Param.m_ID;
								roomShopObjData.cost = list.roomObjects[i].buyAmount;
								if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
								{
									if ((int)roomObjectListDB_Param.m_Tutorial == 0)
									{
										goto IL_1A2;
									}
									roomShopObjData.cost = 0;
								}
								roomShopObjData.isPickup = ((int)roomObjectListDB_Param.m_PickUp == 1);
								roomShopObjData.sortID = roomObjectListDB_Param.m_Sort;
								roomShopObjData.sale = (list.roomObjects[i].bargainFlag == 1 && SingletonMonoBehaviour<GameSystem>.Inst.ServerTime > list.roomObjects[i].bargainStartAt && SingletonMonoBehaviour<GameSystem>.Inst.ServerTime < list.roomObjects[i].bargainEndAt);
								if (roomShopObjData.sale)
								{
									roomShopObjData.cost = list.roomObjects[i].bargainBuyAmount;
								}
								this.m_ObjList.Add(roomShopObjData);
							}
							IL_1A2:;
						}
					}
				}
				this.m_ObjList.Sort(new Comparison<RoomObjShopList.RoomShopObjData>(this.Comp));
				this.ClearFilter();
				this.m_IsAvailable = true;
			}
		}

		// Token: 0x06003803 RID: 14339 RVA: 0x0011C0E4 File Offset: 0x0011A4E4
		private int Comp(RoomObjShopList.RoomShopObjData x, RoomObjShopList.RoomShopObjData y)
		{
			if (x.isPickup && !y.isPickup)
			{
				return -1;
			}
			if (!x.isPickup && y.isPickup)
			{
				return 1;
			}
			if (x.sale && !y.sale)
			{
				return -1;
			}
			if (!x.sale && y.sale)
			{
				return 1;
			}
			if (x.sortID < 0 && y.sortID >= 0)
			{
				return 1;
			}
			if (x.sortID >= 0 && y.sortID < 0)
			{
				return -1;
			}
			if (x.sortID > y.sortID)
			{
				return 1;
			}
			if (x.sortID < y.sortID)
			{
				return -1;
			}
			if (x.id > y.id)
			{
				return 1;
			}
			if (x.id < y.id)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06003804 RID: 14340 RVA: 0x0011C1D2 File Offset: 0x0011A5D2
		public void ExecuteBuy(eRoomObjectCategory category, int objID, int objNum, Action<long> OnRespone)
		{
			this.OnResponseBuy = OnRespone;
			this.Request_Buy(category, objID, objNum, new MeigewwwParam.Callback(this.OnResponse_Buy));
		}

		// Token: 0x06003805 RID: 14341 RVA: 0x0011C1F4 File Offset: 0x0011A5F4
		private void Request_Buy(eRoomObjectCategory category, int objID, int objNum, MeigewwwParam.Callback callback)
		{
			this.m_ObjAccessID = RoomObjectListUtil.CategoryIDToAccessKey(category, objID);
			RoomObjectRequestTypes.Buy buy = new RoomObjectRequestTypes.Buy();
			int tryBargain = 0;
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				if (this.m_ObjList[i].category == category && this.m_ObjList[i].id == objID && this.m_ObjList[i].sale)
				{
					tryBargain = 1;
					break;
				}
			}
			buy.roomObjectId = RoomObjectListUtil.CategoryIDToAccessKey(category, objID).ToString();
			buy.amount = objNum.ToString();
			buy.tryBargain = tryBargain;
			MeigewwwParam wwwParam = RoomObjectRequest.Buy(buy, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06003806 RID: 14342 RVA: 0x0011C2D4 File Offset: 0x0011A6D4
		private void OnResponse_Buy(MeigewwwParam wwwParam)
		{
			RoomObjectResponseTypes.Buy buy = RoomObjectResponse.Buy(wwwParam, ResponseCommon.DialogType.None, null);
			if (buy == null)
			{
				return;
			}
			ResultCode result = buy.GetResult();
			switch (result)
			{
			case ResultCode.BUY_OUT_OF_PERIOD:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomShopBuyErrTitle, eText_MessageDB.RoomShopBuyOutOfPeriod, null);
				this.MarkRefresh();
				this.OnResponseBuy.Call(-1L);
				break;
			default:
				if (result != ResultCode.SUCCESS)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
					APIUtility.wwwToUserData(buy.player, userDataMng.UserData);
					string[] array = buy.managedRoomObjectIds.Split(new char[]
					{
						','
					});
					long num = -1L;
					for (int i = 0; i < array.Length; i++)
					{
						if (long.TryParse(array[i], out num))
						{
							RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(this.m_ObjAccessID);
							UserRoomObjectData userRoomObjectData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
							if (userRoomObjectData == null)
							{
								userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
								userRoomObjectData.SetServerData(num, this.m_ObjAccessID);
								SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomObjDatas.Add(userRoomObjectData);
							}
							else
							{
								userRoomObjectData.SetServerData(num, this.m_ObjAccessID);
							}
						}
					}
					this.OnResponseBuy.Call(num);
				}
				break;
			case ResultCode.BARGAIN_OUT_OF_PERIOD:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.RoomShopBuyErrTitle, eText_MessageDB.RoomShopBargainOutOfPeriod, null);
				this.MarkRefresh();
				this.OnResponseBuy.Call(-1L);
				break;
			}
		}

		// Token: 0x06003807 RID: 14343 RVA: 0x0011C488 File Offset: 0x0011A888
		public void ExecuteSale(eRoomObjectCategory category, int objID, int objNum, Action OnRespone)
		{
			this.OnResponseSale = OnRespone;
			this.Request_Sale(category, objID, objNum, new MeigewwwParam.Callback(this.OnResponse_Sale));
		}

		// Token: 0x06003808 RID: 14344 RVA: 0x0011C4A8 File Offset: 0x0011A8A8
		public void Request_Sale(eRoomObjectCategory category, int objID, int objNum, MeigewwwParam.Callback callback)
		{
			RoomObjectRequestTypes.Sale sale = new RoomObjectRequestTypes.Sale();
			UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(category, objID);
			this.m_SaleObjData = userRoomObjData;
			this.m_SaleMngIDList.Clear();
			for (int i = 0; i < userRoomObjData.HaveNum; i++)
			{
				if (!userRoomObjData.m_MngIDList[i].m_LinkUse)
				{
					this.m_SaleMngIDList.Add(userRoomObjData.m_MngIDList[i].m_ManageID);
					if (this.m_SaleMngIDList.Count >= objNum)
					{
						break;
					}
				}
			}
			sale.managedRoomObjectId = APIUtility.ArrayToStr<long>(this.m_SaleMngIDList.ToArray());
			MeigewwwParam wwwParam = RoomObjectRequest.Sale(sale, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06003809 RID: 14345 RVA: 0x0011C568 File Offset: 0x0011A968
		private void OnResponse_Sale(MeigewwwParam wwwParam)
		{
			RoomObjectResponseTypes.Sale sale = RoomObjectResponse.Sale(wwwParam, ResponseCommon.DialogType.None, null);
			if (sale == null)
			{
				return;
			}
			ResultCode result = sale.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				APIUtility.wwwToUserData(sale.player, userDataMng.UserData);
				for (int i = 0; i < this.m_SaleMngIDList.Count; i++)
				{
					this.m_SaleObjData.ReleaseLinkMngID(this.m_SaleMngIDList[i]);
				}
				this.OnResponseSale.Call();
			}
		}

		// Token: 0x04003EE2 RID: 16098
		private bool m_IsAvailable;

		// Token: 0x04003EE3 RID: 16099
		private bool m_IsOpen;

		// Token: 0x04003EE4 RID: 16100
		private List<RoomObjShopList.RoomShopObjData> m_ObjList;

		// Token: 0x04003EE5 RID: 16101
		private List<RoomObjShopList.RoomShopObjData> m_FilteredObjList;

		// Token: 0x04003EE6 RID: 16102
		private int m_ObjAccessID = -1;

		// Token: 0x04003EE7 RID: 16103
		private UserRoomObjectData m_SaleObjData;

		// Token: 0x04003EE8 RID: 16104
		private List<long> m_SaleMngIDList = new List<long>();

		// Token: 0x02000A8A RID: 2698
		public class RoomShopObjData
		{
			// Token: 0x04003EEB RID: 16107
			public eRoomObjectCategory category;

			// Token: 0x04003EEC RID: 16108
			public int id;

			// Token: 0x04003EED RID: 16109
			public int cost;

			// Token: 0x04003EEE RID: 16110
			public bool sale;

			// Token: 0x04003EEF RID: 16111
			public bool isPickup;

			// Token: 0x04003EF0 RID: 16112
			public int sortID;
		}
	}
}
