﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A83 RID: 2691
	public class RoomObjListWindow : UIGroup
	{
		// Token: 0x1700034B RID: 843
		// (get) Token: 0x060037CD RID: 14285 RVA: 0x0011AE93 File Offset: 0x00119293
		public RoomObjListWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x060037CE RID: 14286 RVA: 0x0011AE9B File Offset: 0x0011929B
		public int SelectObjID
		{
			get
			{
				return this.m_SelectObjID;
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x060037CF RID: 14287 RVA: 0x0011AEA3 File Offset: 0x001192A3
		public eRoomObjectCategory SelectObjCategory
		{
			get
			{
				return this.m_SelectObjCategory;
			}
		}

		// Token: 0x060037D0 RID: 14288 RVA: 0x0011AEAC File Offset: 0x001192AC
		public void Setup(RoomObjShopList shopList)
		{
			this.m_ShopList = shopList;
			this.m_GameObject = base.gameObject;
			this.m_GameObject.SetActive(false);
			this.m_Scroll.Setup();
			string[] array = new string[14];
			for (int i = 0; i < array.Length; i++)
			{
				if (i == 0)
				{
					array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonAll);
				}
				else
				{
					array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomObjectCategoryDesk + i - 1);
				}
			}
			this.m_TabGroup.Setup(array, -1f);
			this.m_TabGroup.OnChangeIdx += this.OnClickTabCallBack;
		}

		// Token: 0x060037D1 RID: 14289 RVA: 0x0011AF64 File Offset: 0x00119364
		public int CategoryToTabIdx(eRoomObjectCategory category)
		{
			return (int)(category + 1);
		}

		// Token: 0x060037D2 RID: 14290 RVA: 0x0011AF6C File Offset: 0x0011936C
		public void Open(RoomObjListWindow.eMode mode)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(UserRoomUtil.GetUserRoomObjPlacementNum().ToString());
			stringBuilder.Append(" / ");
			stringBuilder.Append(50.ToString());
			this.m_PlacementNumText.text = stringBuilder.ToString();
			stringBuilder.Length = 0;
			stringBuilder.Append(UserRoomUtil.GetUserRoomObjRemainNum().ToString());
			stringBuilder.Append(" / ");
			stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit.ToString());
			this.m_StoreNumText.text = stringBuilder.ToString();
			this.m_HaveGoldText.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			this.m_SelectButton = RoomObjListWindow.eButton.None;
			if (this.m_Mode != mode)
			{
				this.m_IsDonePrepare = false;
			}
			this.m_Mode = mode;
			if (mode != RoomObjListWindow.eMode.Have)
			{
				if (mode != RoomObjListWindow.eMode.HaveBedding)
				{
					if (mode == RoomObjListWindow.eMode.Shop)
					{
						this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomShop));
						if (!this.m_ShopList.IsAvailable)
						{
							this.m_ShopList.Open();
							this.m_IsDonePrepare = false;
						}
					}
				}
				else
				{
					this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomStore));
					this.m_HaveList.Open();
					this.m_IsDonePrepare = false;
				}
			}
			else
			{
				this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomStore));
				this.m_HaveList.Open();
				this.m_IsDonePrepare = false;
			}
			this.PrepareStart();
			if (this.m_IsDonePrepare)
			{
				this.Apply();
				base.Open();
			}
			this.m_GameObject.SetActive(true);
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(true, 1f, true);
				this.m_Scroll.GetComponent<ScrollRect>().vertical = false;
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
				this.m_Scroll.GetComponent<ScrollRect>().vertical = true;
			}
		}

		// Token: 0x060037D3 RID: 14291 RVA: 0x0011B1D8 File Offset: 0x001195D8
		public override void Update()
		{
			base.Update();
			if (!this.m_IsDonePrepare)
			{
				RoomObjListWindow.eMode mode = this.m_Mode;
				if (mode != RoomObjListWindow.eMode.Have && mode != RoomObjListWindow.eMode.HaveBedding)
				{
					if (mode == RoomObjListWindow.eMode.Shop)
					{
						if (this.m_ShopList != null)
						{
							if (this.m_ShopList.IsAvailable)
							{
								this.m_IsDonePrepare = true;
							}
							else
							{
								this.m_ShopList.UpdateOpen();
							}
						}
					}
				}
				else if (this.m_HaveList != null)
				{
					if (this.m_HaveList.IsAvailable)
					{
						this.m_IsDonePrepare = true;
					}
					else
					{
						this.m_HaveList.UpdateOpen();
					}
				}
				if (this.m_IsDonePrepare)
				{
					this.Apply();
					base.Open();
				}
			}
			if (!this.m_IsOpenTutorial && SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial() && base.IsIdle())
			{
				this.m_IsOpenTutorial = true;
				base.GetComponentInChildren<RoomObjListItem>().SetAttachCanvas();
			}
		}

		// Token: 0x060037D4 RID: 14292 RVA: 0x0011B2DC File Offset: 0x001196DC
		private void Apply()
		{
			if (this.m_Mode == RoomObjListWindow.eMode.HaveBedding)
			{
				for (int i = 0; i < this.m_TabGroup.GetButtonNum(); i++)
				{
					this.m_TabGroup.SetIntaractable(i == this.CategoryToTabIdx(eRoomObjectCategory.Bedding), i);
				}
				this.m_TabGroup.ChangeIdx(this.CategoryToTabIdx(eRoomObjectCategory.Bedding));
				this.m_HaveList.DoFilter(eRoomObjectCategory.Bedding);
				this.ApplyScroll();
			}
			else
			{
				for (int j = 0; j < this.m_TabGroup.GetButtonNum(); j++)
				{
					this.m_TabGroup.SetIntaractable(true, j);
				}
				this.ChangeCategory(this.m_TabGroup.SelectIdx);
			}
		}

		// Token: 0x060037D5 RID: 14293 RVA: 0x0011B38C File Offset: 0x0011978C
		private void ApplyScroll()
		{
			this.m_Scroll.RemoveAll();
			RoomObjListWindow.eMode mode = this.m_Mode;
			if (mode != RoomObjListWindow.eMode.Have)
			{
				if (mode != RoomObjListWindow.eMode.HaveBedding)
				{
					if (mode == RoomObjListWindow.eMode.Shop)
					{
						for (int i = 0; i < this.m_ShopList.GetFilteredItemNum(); i++)
						{
							RoomObjShopList.RoomShopObjData filteredItem = this.m_ShopList.GetFilteredItem(i);
							RoomObjectItemData roomObjectItemData = new RoomObjectItemData();
							roomObjectItemData.m_Mode = RoomObjectItemData.eMode.Shop;
							roomObjectItemData.m_ObjCategory = filteredItem.category;
							roomObjectItemData.m_ObjID = filteredItem.id;
							roomObjectItemData.m_Cost = filteredItem.cost;
							roomObjectItemData.m_IsSale = filteredItem.sale;
							roomObjectItemData.m_IsPickup = filteredItem.isPickup;
							roomObjectItemData.OnPlacement += this.OnPlacementButtonCallBack;
							roomObjectItemData.OnSell += this.OnSellButtonCallBack;
							roomObjectItemData.OnBuy += this.OnBuyButtonCallBack;
							this.m_Scroll.AddItem(roomObjectItemData);
						}
					}
				}
				else
				{
					for (int j = 0; j < this.m_HaveList.GetFilteredItemNum(); j++)
					{
						RoomObjHaveList.RoomObj filteredItem2 = this.m_HaveList.GetFilteredItem(j);
						RoomObjectItemData roomObjectItemData2 = new RoomObjectItemData();
						roomObjectItemData2.m_Mode = RoomObjectItemData.eMode.Have;
						roomObjectItemData2.m_ObjCategory = filteredItem2.category;
						roomObjectItemData2.m_ObjID = filteredItem2.id;
						roomObjectItemData2.m_IsSale = false;
						roomObjectItemData2.m_IsPickup = false;
						roomObjectItemData2.OnPlacement += this.OnPlacementButtonCallBack;
						roomObjectItemData2.OnSell += this.OnSellButtonCallBack;
						roomObjectItemData2.OnBuy += this.OnBuyButtonCallBack;
						this.m_Scroll.AddItem(roomObjectItemData2);
					}
				}
			}
			else
			{
				for (int k = 0; k < this.m_HaveList.GetFilteredItemNum(); k++)
				{
					RoomObjHaveList.RoomObj filteredItem3 = this.m_HaveList.GetFilteredItem(k);
					RoomObjectItemData roomObjectItemData3 = new RoomObjectItemData();
					roomObjectItemData3.m_Mode = RoomObjectItemData.eMode.Have;
					roomObjectItemData3.m_ObjCategory = filteredItem3.category;
					roomObjectItemData3.m_ObjID = filteredItem3.id;
					roomObjectItemData3.m_IsSale = false;
					roomObjectItemData3.m_IsPickup = false;
					roomObjectItemData3.OnPlacement += this.OnPlacementButtonCallBack;
					roomObjectItemData3.OnSell += this.OnSellButtonCallBack;
					roomObjectItemData3.OnBuy += this.OnBuyButtonCallBack;
					this.m_Scroll.AddItem(roomObjectItemData3);
				}
			}
			this.m_Scroll.Refresh();
		}

		// Token: 0x060037D6 RID: 14294 RVA: 0x0011B5F6 File Offset: 0x001199F6
		public void OnPlacementButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
			this.m_SelectButton = RoomObjListWindow.eButton.Placement;
			this.m_SelectObjID = roomObjID;
			this.m_SelectObjCategory = category;
			this.Close();
		}

		// Token: 0x060037D7 RID: 14295 RVA: 0x0011B613 File Offset: 0x00119A13
		public void OnSellButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
			this.m_SelectButton = RoomObjListWindow.eButton.Sell;
			this.m_SelectObjID = roomObjID;
			this.m_SelectObjCategory = category;
			this.Close();
		}

		// Token: 0x060037D8 RID: 14296 RVA: 0x0011B630 File Offset: 0x00119A30
		public void OnBuyButtonCallBack(eRoomObjectCategory category, int roomObjID)
		{
			this.m_SelectButton = RoomObjListWindow.eButton.Buy;
			this.m_SelectObjID = roomObjID;
			this.m_SelectObjCategory = category;
			this.Close();
		}

		// Token: 0x060037D9 RID: 14297 RVA: 0x0011B64D File Offset: 0x00119A4D
		public void OnClickTabCallBack(int tabIdx)
		{
			this.ChangeCategory(tabIdx);
		}

		// Token: 0x060037DA RID: 14298 RVA: 0x0011B658 File Offset: 0x00119A58
		private void ChangeCategory(int tabIdx)
		{
			this.m_Scroll.RemoveAll();
			RoomObjListWindow.eMode mode = this.m_Mode;
			if (mode != RoomObjListWindow.eMode.Have)
			{
				if (mode == RoomObjListWindow.eMode.Shop)
				{
					if (tabIdx == 0)
					{
						this.m_ShopList.ClearFilter();
					}
					else
					{
						this.m_ShopList.DoFilter((eRoomObjectCategory)(tabIdx - 1));
					}
				}
			}
			else if (tabIdx == 0)
			{
				this.m_HaveList.ClearFilter();
			}
			else
			{
				this.m_HaveList.DoFilter((eRoomObjectCategory)(tabIdx - 1));
			}
			this.ApplyScroll();
		}

		// Token: 0x04003EB0 RID: 16048
		private RoomObjListWindow.eMode m_Mode;

		// Token: 0x04003EB1 RID: 16049
		private RoomObjListWindow.eButton m_SelectButton;

		// Token: 0x04003EB2 RID: 16050
		private int m_SelectObjID = -1;

		// Token: 0x04003EB3 RID: 16051
		private eRoomObjectCategory m_SelectObjCategory;

		// Token: 0x04003EB4 RID: 16052
		private RoomObjHaveList m_HaveList = new RoomObjHaveList();

		// Token: 0x04003EB5 RID: 16053
		private RoomObjShopList m_ShopList;

		// Token: 0x04003EB6 RID: 16054
		[SerializeField]
		private Text m_PlacementNumText;

		// Token: 0x04003EB7 RID: 16055
		[SerializeField]
		private Text m_StoreNumText;

		// Token: 0x04003EB8 RID: 16056
		[SerializeField]
		private Text m_HaveGoldText;

		// Token: 0x04003EB9 RID: 16057
		[SerializeField]
		private ScrollViewBase m_Scroll;

		// Token: 0x04003EBA RID: 16058
		[SerializeField]
		private TabGroup m_TabGroup;

		// Token: 0x04003EBB RID: 16059
		[SerializeField]
		private CustomButton m_CloseButton;

		// Token: 0x04003EBC RID: 16060
		private bool m_IsDonePrepare;

		// Token: 0x04003EBD RID: 16061
		private GameObject m_GameObject;

		// Token: 0x04003EBE RID: 16062
		private bool m_IsOpenTutorial;

		// Token: 0x02000A84 RID: 2692
		public enum eMode
		{
			// Token: 0x04003EC0 RID: 16064
			Have,
			// Token: 0x04003EC1 RID: 16065
			HaveBedding,
			// Token: 0x04003EC2 RID: 16066
			Shop
		}

		// Token: 0x02000A85 RID: 2693
		public enum eButton
		{
			// Token: 0x04003EC4 RID: 16068
			None,
			// Token: 0x04003EC5 RID: 16069
			Placement,
			// Token: 0x04003EC6 RID: 16070
			Sell,
			// Token: 0x04003EC7 RID: 16071
			Buy
		}
	}
}
