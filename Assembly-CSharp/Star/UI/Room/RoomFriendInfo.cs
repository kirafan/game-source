﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A71 RID: 2673
	public class RoomFriendInfo : MonoBehaviour
	{
		// Token: 0x0600378D RID: 14221 RVA: 0x001197D0 File Offset: 0x00117BD0
		public void Apply(long playerID)
		{
			FriendManager.FriendData friendData = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.GetFriendData(playerID);
			this.m_IconCloner.GetInst<CharaIconWithFrame>().ApplySupport(friendData.m_LeaderCharaData);
			this.m_NameText.text = friendData.m_Name;
			this.m_Rank.SetValue(friendData.m_Lv);
		}

		// Token: 0x04003E53 RID: 15955
		[SerializeField]
		private PrefabCloner m_IconCloner;

		// Token: 0x04003E54 RID: 15956
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003E55 RID: 15957
		[SerializeField]
		private ImageNumbers m_Rank;
	}
}
