﻿using System;
using Star.UI.Menu;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000A72 RID: 2674
	public class RoomFriendListWindow : UIGroup
	{
		// Token: 0x140000BD RID: 189
		// (add) Token: 0x0600378F RID: 14223 RVA: 0x00119830 File Offset: 0x00117C30
		// (remove) Token: 0x06003790 RID: 14224 RVA: 0x00119868 File Offset: 0x00117C68
		public event Action OnRoomChange;

		// Token: 0x06003791 RID: 14225 RVA: 0x0011989E File Offset: 0x00117C9E
		public void Open(bool request)
		{
			this.m_Scroll.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.FriendMng.Request_GetAll(FriendDefine.eGetAllType.Both, new Action<bool>(this.OnResponse_GetAll));
		}

		// Token: 0x06003792 RID: 14226 RVA: 0x001198C8 File Offset: 0x00117CC8
		private void OnResponse_GetAll(bool isError)
		{
			FriendManager friendMng = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng;
			for (int i = 0; i < friendMng.GetFriendListNum(); i++)
			{
				IFriendUIData friendUIData = new IFriendUIData(friendMng.GetFriendDataAt(i), new Action<MenuFriendListItem.eAction>(this.OnActionResponse));
				friendUIData.m_Select = IFriendUIData.eSelect.RoomGuest;
				switch (friendMng.GetFriendDataAt(i).m_Type)
				{
				case FriendDefine.eType.Friend:
					friendUIData.m_Select = IFriendUIData.eSelect.RoomBase;
					break;
				case FriendDefine.eType.OpponentPropose:
					friendUIData.m_Select = IFriendUIData.eSelect.RoomOpponentPropose;
					break;
				case FriendDefine.eType.SelfPropose:
					friendUIData.m_Select = IFriendUIData.eSelect.RoomSelfPropose;
					break;
				case FriendDefine.eType.Guest:
					friendUIData.m_Select = IFriendUIData.eSelect.RoomGuest;
					break;
				}
				this.m_Scroll.AddItem(friendUIData);
			}
			base.Open();
		}

		// Token: 0x06003793 RID: 14227 RVA: 0x00119984 File Offset: 0x00117D84
		private void OnActionResponse(MenuFriendListItem.eAction action)
		{
			if (action == MenuFriendListItem.eAction.RoomIn)
			{
				this.OnRoomChange.Call();
			}
			else
			{
				FriendManager friendMng = SingletonMonoBehaviour<GameSystem>.Inst.FriendMng;
				for (int i = 0; i < this.m_Scroll.GetDataList().Count; i++)
				{
					IFriendUIData friendUIData = (IFriendUIData)this.m_Scroll.GetDataList()[i];
					friendUIData.m_Select = IFriendUIData.eSelect.RoomGuest;
					if (friendMng.GetFriendData(friendUIData.m_PlayerID) == null)
					{
						this.m_Scroll.GetDataList().RemoveAt(i);
						i--;
					}
					else
					{
						friendUIData.ApplyFriendData(friendMng.GetFriendData(friendUIData.m_PlayerID));
						switch (friendMng.GetFriendDataAt(i).m_Type)
						{
						case FriendDefine.eType.Friend:
							friendUIData.m_Select = IFriendUIData.eSelect.RoomBase;
							break;
						case FriendDefine.eType.OpponentPropose:
							friendUIData.m_Select = IFriendUIData.eSelect.RoomOpponentPropose;
							break;
						case FriendDefine.eType.SelfPropose:
							friendUIData.m_Select = IFriendUIData.eSelect.RoomSelfPropose;
							break;
						case FriendDefine.eType.Guest:
							friendUIData.m_Select = IFriendUIData.eSelect.RoomGuest;
							break;
						}
					}
				}
				this.m_Scroll.Refresh();
			}
		}

		// Token: 0x04003E56 RID: 15958
		[SerializeField]
		private ScrollViewBase m_Scroll;
	}
}
