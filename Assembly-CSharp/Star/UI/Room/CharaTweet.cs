﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A6F RID: 2671
	public class CharaTweet : MonoBehaviour
	{
		// Token: 0x06003786 RID: 14214 RVA: 0x00119761 File Offset: 0x00117B61
		public void Open(string text)
		{
			this.m_Text.text = text;
			base.gameObject.SetActive(true);
		}

		// Token: 0x06003787 RID: 14215 RVA: 0x0011977B File Offset: 0x00117B7B
		public void Close()
		{
			base.gameObject.SetActive(false);
		}

		// Token: 0x04003E4F RID: 15951
		[SerializeField]
		private Text m_Text;

		// Token: 0x04003E50 RID: 15952
		private RectTransform m_Transform;
	}
}
