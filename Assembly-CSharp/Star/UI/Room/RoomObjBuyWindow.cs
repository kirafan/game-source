﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A76 RID: 2678
	public class RoomObjBuyWindow : UIGroup
	{
		// Token: 0x17000349 RID: 841
		// (get) Token: 0x0600379D RID: 14237 RVA: 0x00119D55 File Offset: 0x00118155
		public RoomObjBuyWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x0600379E RID: 14238 RVA: 0x00119D5D File Offset: 0x0011815D
		public void Open(RoomObjShopList.RoomShopObjData shopObjData, RoomObjBuyWindow.eMode mode, UnityAction onClose)
		{
			this.Open(shopObjData.category, shopObjData.id, (long)shopObjData.cost, mode, onClose);
		}

		// Token: 0x0600379F RID: 14239 RVA: 0x00119D7C File Offset: 0x0011817C
		public void Open(eRoomObjectCategory category, int objID, long amount, RoomObjBuyWindow.eMode mode, UnityAction OnClose)
		{
			this.m_Amount = amount;
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				this.m_Amount = 0L;
			}
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_RoomObjectIcon.Apply(category, objID);
			this.m_ObjNameTextObj.text = objectParam.m_Name;
			base.Open();
			this.m_Mode = mode;
			if (mode != RoomObjBuyWindow.eMode.Buy)
			{
				if (mode != RoomObjBuyWindow.eMode.BuyNumSelect)
				{
					if (mode == RoomObjBuyWindow.eMode.SellNumSelect)
					{
						this.m_Amount = (long)objectParam.m_SellAmount;
						int remainNum = UserRoomUtil.GetUserRoomObjData(category, objID).RemainNum;
						this.m_NumSelectObj.SetActive(true);
						this.m_NumSelect.SetChangeCurrentNumCallBack(new Action(this.OnChangeCurrentNumCallBack));
						this.m_NumSelect.SetRange(1, remainNum);
						this.m_NumSelect.SetCurrentNum(1);
						this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomSellTitle));
						this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomSellMessage);
					}
				}
				else
				{
					int num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit;
					UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(category, objID);
					if (userRoomObjData != null)
					{
						num = (int)objectParam.m_MaxNum - userRoomObjData.HaveNum;
					}
					else
					{
						num = (int)objectParam.m_MaxNum;
					}
					int num2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.RoomObjectLimit - UserRoomUtil.GetUserRoomObjRemainNum();
					if (num > num2)
					{
						num = num2;
					}
					if (objectParam.m_BuyAmount > 0)
					{
						long num3 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold / amount;
						if ((long)num > num3)
						{
							num = (int)num3;
						}
					}
					this.ApplyUseGold(this.m_Amount);
					this.m_NumSelectObj.SetActive(true);
					this.m_NumSelect.SetChangeCurrentNumCallBack(new Action(this.OnChangeCurrentNumCallBack));
					this.m_NumSelect.SetRange(1, num);
					this.m_NumSelect.SetCurrentNum(1);
					this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyTitle));
					this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyMessage);
				}
			}
			else
			{
				if (this.m_NumSelectObj != null)
				{
					this.m_NumSelectObj.SetActive(false);
				}
				this.m_CustomWindow.SetTitle(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyTitle));
				this.m_ConfirmText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RoomBuyMessage);
				this.ApplyUseGold(this.m_Amount);
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(true, 1f, true);
				this.m_IsReserveAttach = true;
			}
			this.m_CloseAction = OnClose;
		}

		// Token: 0x060037A0 RID: 14240 RVA: 0x0011A060 File Offset: 0x00118460
		public override void Update()
		{
			base.Update();
			if (this.m_IsReserveAttach && SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				TutorialMessage tutorialMessage = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage;
				tutorialMessage.SetEnableArrow(true);
				tutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				tutorialMessage.SetArrowPosition(this.m_YesButton.GetComponent<RectTransform>().position);
				this.m_TutorialTarget.SetEnable(true);
				this.m_IsReserveAttach = false;
			}
		}

		// Token: 0x060037A1 RID: 14241 RVA: 0x0011A0D4 File Offset: 0x001184D4
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_CloseAction != null)
			{
				this.m_CloseAction();
			}
		}

		// Token: 0x060037A2 RID: 14242 RVA: 0x0011A0F2 File Offset: 0x001184F2
		public int GetSelectNum()
		{
			if (this.m_Mode == RoomObjBuyWindow.eMode.Buy)
			{
				return 1;
			}
			return this.m_NumSelect.GetCurrentNum();
		}

		// Token: 0x060037A3 RID: 14243 RVA: 0x0011A10C File Offset: 0x0011850C
		public void OnChangeCurrentNumCallBack()
		{
			this.ApplyUseGold(this.m_Amount * (long)this.GetSelectNum());
		}

		// Token: 0x060037A4 RID: 14244 RVA: 0x0011A124 File Offset: 0x00118524
		private void ApplyUseGold(long priceChange)
		{
			long num = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold;
			if (this.m_Mode == RoomObjBuyWindow.eMode.SellNumSelect)
			{
				num += priceChange;
			}
			else
			{
				num -= priceChange;
			}
			this.m_BeforeGoldObj.text = UIUtility.GoldValueToString(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Gold, true);
			this.m_AfterGoldObj.text = UIUtility.GoldValueToString(num, true);
			if (num < 0L)
			{
				this.m_YesButton.Interactable = false;
			}
			else
			{
				this.m_YesButton.Interactable = true;
			}
		}

		// Token: 0x060037A5 RID: 14245 RVA: 0x0011A1BB File Offset: 0x001185BB
		public void OnClickYesButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableDark(false, 1f, true);
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
			this.m_IsReserveAttach = false;
			this.m_SelectButton = RoomObjBuyWindow.eButton.Yes;
			this.Close();
		}

		// Token: 0x060037A6 RID: 14246 RVA: 0x0011A1F7 File Offset: 0x001185F7
		public void OnClickNoButtonCallBack()
		{
			this.m_SelectButton = RoomObjBuyWindow.eButton.No;
			this.Close();
		}

		// Token: 0x04003E6B RID: 15979
		[SerializeField]
		private RoomObjectIcon m_RoomObjectIcon;

		// Token: 0x04003E6C RID: 15980
		[SerializeField]
		private Text m_ObjNameTextObj;

		// Token: 0x04003E6D RID: 15981
		[SerializeField]
		private Text m_BeforeGoldObj;

		// Token: 0x04003E6E RID: 15982
		[SerializeField]
		private Text m_AfterGoldObj;

		// Token: 0x04003E6F RID: 15983
		[SerializeField]
		private Text m_ConfirmText;

		// Token: 0x04003E70 RID: 15984
		[SerializeField]
		private GameObject m_NumSelectObj;

		// Token: 0x04003E71 RID: 15985
		[SerializeField]
		private NumberSelect m_NumSelect;

		// Token: 0x04003E72 RID: 15986
		[SerializeField]
		private CustomButton m_YesButton;

		// Token: 0x04003E73 RID: 15987
		[SerializeField]
		private TutorialTarget m_TutorialTarget;

		// Token: 0x04003E74 RID: 15988
		private RoomObjBuyWindow.eMode m_Mode;

		// Token: 0x04003E75 RID: 15989
		private long m_Amount;

		// Token: 0x04003E76 RID: 15990
		private UnityAction m_CloseAction;

		// Token: 0x04003E77 RID: 15991
		private RoomObjBuyWindow.eButton m_SelectButton = RoomObjBuyWindow.eButton.No;

		// Token: 0x04003E78 RID: 15992
		private bool m_IsReserveAttach;

		// Token: 0x02000A77 RID: 2679
		public enum eMode
		{
			// Token: 0x04003E7A RID: 15994
			Buy,
			// Token: 0x04003E7B RID: 15995
			BuyNumSelect,
			// Token: 0x04003E7C RID: 15996
			SellNumSelect
		}

		// Token: 0x02000A78 RID: 2680
		public enum eButton
		{
			// Token: 0x04003E7E RID: 15998
			Yes,
			// Token: 0x04003E7F RID: 15999
			No
		}
	}
}
