﻿using System;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000A7E RID: 2686
	public class RoomObjInfoWindow : UIGroup
	{
		// Token: 0x060037BA RID: 14266 RVA: 0x0011A888 File Offset: 0x00118C88
		public void Open(eRoomObjectCategory category, int objID)
		{
			this.m_ObjInfo.Apply(category, objID, 0L);
			base.Open();
		}

		// Token: 0x04003E97 RID: 16023
		[SerializeField]
		private RoomObjInfo m_ObjInfo;
	}
}
