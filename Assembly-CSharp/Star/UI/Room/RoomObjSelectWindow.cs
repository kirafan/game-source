﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A86 RID: 2694
	public class RoomObjSelectWindow : UIGroup
	{
		// Token: 0x1700034E RID: 846
		// (get) Token: 0x060037DC RID: 14300 RVA: 0x0011B6E9 File Offset: 0x00119AE9
		public RoomObjSelectWindow.eButton SelectButton
		{
			get
			{
				return this.m_SelectButton;
			}
		}

		// Token: 0x060037DD RID: 14301 RVA: 0x0011B6F1 File Offset: 0x00119AF1
		public CustomButton GetEditCancelButton()
		{
			return this.m_EditCancelButton;
		}

		// Token: 0x060037DE RID: 14302 RVA: 0x0011B6F9 File Offset: 0x00119AF9
		public CustomButton GetBuyCancelButton()
		{
			return this.m_BuyCancelButton;
		}

		// Token: 0x060037DF RID: 14303 RVA: 0x0011B704 File Offset: 0x00119B04
		protected override bool CheckInEnd()
		{
			return base.CheckInEnd() && (this.m_TextAnim.State != 1 && this.m_InfoButtonsAnim.State != 1 && this.m_EditButtonsAnim.State != 1) && this.m_BuyButtonsAnim.State != 1;
		}

		// Token: 0x060037E0 RID: 14304 RVA: 0x0011B768 File Offset: 0x00119B68
		protected override bool CheckOutEnd()
		{
			return base.CheckInEnd() && (this.m_TextAnim.State != 3 && this.m_InfoButtonsAnim.State != 3 && this.m_EditButtonsAnim.State != 3) && this.m_BuyButtonsAnim.State != 3;
		}

		// Token: 0x060037E1 RID: 14305 RVA: 0x0011B7CC File Offset: 0x00119BCC
		public void OpenInfo(eRoomObjectCategory category, int objID)
		{
			base.Open();
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_ObjName.text = objectParam.m_Name;
			if (this.m_TextAnim.State != 2)
			{
				this.m_TextAnim.PlayIn();
			}
			if (this.m_InfoButtonsAnim.State != 2)
			{
				this.m_InfoButtonsAnim.PlayIn();
			}
			if (this.m_EditButtonsAnim.State != 0)
			{
				this.m_EditButtonsAnim.PlayOut();
			}
			if (this.m_BuyButtonsAnim.State != 0)
			{
				this.m_BuyButtonsAnim.PlayOut();
			}
		}

		// Token: 0x060037E2 RID: 14306 RVA: 0x0011B868 File Offset: 0x00119C68
		public void OpenEdit(eRoomObjectCategory category, int objID, bool canFlip = true)
		{
			base.Open();
			this.m_FlipButton.Interactable = canFlip;
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_ObjName.text = objectParam.m_Name;
			if (this.m_TextAnim.State != 2)
			{
				this.m_TextAnim.PlayIn();
			}
			if (this.m_InfoButtonsAnim.State != 0)
			{
				this.m_InfoButtonsAnim.PlayOut();
			}
			if (this.m_EditButtonsAnim.State != 2)
			{
				this.m_EditButtonsAnim.PlayIn();
			}
			if (this.m_BuyButtonsAnim.State != 0)
			{
				this.m_BuyButtonsAnim.PlayOut();
			}
		}

		// Token: 0x060037E3 RID: 14307 RVA: 0x0011B910 File Offset: 0x00119D10
		public void OpenBuy(eRoomObjectCategory category, int objID, bool canFlip = true)
		{
			base.Open();
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_BuyFlipButton.Interactable = canFlip;
			this.m_BuyCancelButton.Interactable = !SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial();
			this.m_ObjName.text = objectParam.m_Name;
			if (this.m_TextAnim.State != 2)
			{
				this.m_TextAnim.PlayIn();
			}
			if (this.m_InfoButtonsAnim.State != 0)
			{
				this.m_InfoButtonsAnim.PlayOut();
			}
			if (this.m_EditButtonsAnim.State != 0)
			{
				this.m_EditButtonsAnim.PlayOut();
			}
			if (this.m_BuyButtonsAnim.State != 2)
			{
				this.m_BuyButtonsAnim.PlayIn();
			}
		}

		// Token: 0x060037E4 RID: 14308 RVA: 0x0011B9D4 File Offset: 0x00119DD4
		public void OpenMission()
		{
			base.Open();
			this.m_ObjName.text = "メッセージボード";
			if (this.m_TextAnim.State != 2)
			{
				this.m_TextAnim.PlayIn();
			}
			if (this.m_InfoButtonsAnim.State != 0)
			{
				this.m_InfoButtonsAnim.PlayOut();
			}
			if (this.m_EditButtonsAnim.State != 0)
			{
				this.m_EditButtonsAnim.PlayOut();
			}
			if (this.m_BuyButtonsAnim.State != 0)
			{
				this.m_BuyButtonsAnim.PlayOut();
			}
		}

		// Token: 0x060037E5 RID: 14309 RVA: 0x0011BA64 File Offset: 0x00119E64
		public override void Close()
		{
			base.Close();
			if (this.m_TextAnim.State != 0)
			{
				this.m_TextAnim.PlayOut();
			}
			if (this.m_InfoButtonsAnim.State != 0)
			{
				this.m_InfoButtonsAnim.PlayOut();
			}
			if (this.m_EditButtonsAnim.State != 0)
			{
				this.m_EditButtonsAnim.PlayOut();
			}
			if (this.m_BuyButtonsAnim.State != 0)
			{
				this.m_BuyButtonsAnim.PlayOut();
			}
		}

		// Token: 0x060037E6 RID: 14310 RVA: 0x0011BAE3 File Offset: 0x00119EE3
		public void SetPlaceButtonEnable(bool flg)
		{
			this.m_PlacementButton.Interactable = flg;
			this.m_BuyButton.Interactable = flg;
		}

		// Token: 0x060037E7 RID: 14311 RVA: 0x0011BB00 File Offset: 0x00119F00
		public void SetFriendmode(bool flg)
		{
			for (int i = 0; i < this.m_FriendInvalidButtons.Length; i++)
			{
				this.m_FriendInvalidButtons[i].Interactable = !flg;
			}
		}

		// Token: 0x060037E8 RID: 14312 RVA: 0x0011BB37 File Offset: 0x00119F37
		public void ResetSelectButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.None;
		}

		// Token: 0x060037E9 RID: 14313 RVA: 0x0011BB40 File Offset: 0x00119F40
		public void OnClickInfoButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Info;
		}

		// Token: 0x060037EA RID: 14314 RVA: 0x0011BB49 File Offset: 0x00119F49
		public void OnClickMoveButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Move;
		}

		// Token: 0x060037EB RID: 14315 RVA: 0x0011BB52 File Offset: 0x00119F52
		public void OnClickPutAwayButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.PutAway;
		}

		// Token: 0x060037EC RID: 14316 RVA: 0x0011BB5B File Offset: 0x00119F5B
		public void OnClickPlacementButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Placement;
		}

		// Token: 0x060037ED RID: 14317 RVA: 0x0011BB64 File Offset: 0x00119F64
		public void OnClickBuyButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Buy;
		}

		// Token: 0x060037EE RID: 14318 RVA: 0x0011BB6D File Offset: 0x00119F6D
		public void OnClickFlipButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Flip;
		}

		// Token: 0x060037EF RID: 14319 RVA: 0x0011BB76 File Offset: 0x00119F76
		public void OnClickCancelButton()
		{
			this.m_SelectButton = RoomObjSelectWindow.eButton.Cancel;
		}

		// Token: 0x04003EC8 RID: 16072
		private RoomObjSelectWindow.eButton m_SelectButton;

		// Token: 0x04003EC9 RID: 16073
		[SerializeField]
		private Text m_ObjName;

		// Token: 0x04003ECA RID: 16074
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x04003ECB RID: 16075
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x04003ECC RID: 16076
		[SerializeField]
		private CustomButton m_FlipButton;

		// Token: 0x04003ECD RID: 16077
		[SerializeField]
		private CustomButton m_EditCancelButton;

		// Token: 0x04003ECE RID: 16078
		[SerializeField]
		private CustomButton m_BuyFlipButton;

		// Token: 0x04003ECF RID: 16079
		[SerializeField]
		private CustomButton m_BuyCancelButton;

		// Token: 0x04003ED0 RID: 16080
		[SerializeField]
		private AnimUIPlayer m_TextAnim;

		// Token: 0x04003ED1 RID: 16081
		[SerializeField]
		private AnimUIPlayer m_InfoButtonsAnim;

		// Token: 0x04003ED2 RID: 16082
		[SerializeField]
		private AnimUIPlayer m_EditButtonsAnim;

		// Token: 0x04003ED3 RID: 16083
		[SerializeField]
		private AnimUIPlayer m_BuyButtonsAnim;

		// Token: 0x04003ED4 RID: 16084
		[SerializeField]
		[Tooltip("フレンドの場合無効になるボタン")]
		private CustomButton[] m_FriendInvalidButtons;

		// Token: 0x02000A87 RID: 2695
		public enum eMode
		{
			// Token: 0x04003ED6 RID: 16086
			Info,
			// Token: 0x04003ED7 RID: 16087
			Edit,
			// Token: 0x04003ED8 RID: 16088
			Buy
		}

		// Token: 0x02000A88 RID: 2696
		public enum eButton
		{
			// Token: 0x04003EDA RID: 16090
			None,
			// Token: 0x04003EDB RID: 16091
			Info,
			// Token: 0x04003EDC RID: 16092
			Move,
			// Token: 0x04003EDD RID: 16093
			PutAway,
			// Token: 0x04003EDE RID: 16094
			Placement,
			// Token: 0x04003EDF RID: 16095
			Buy,
			// Token: 0x04003EE0 RID: 16096
			Flip,
			// Token: 0x04003EE1 RID: 16097
			Cancel
		}
	}
}
