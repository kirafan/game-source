﻿using System;
using System.Collections.Generic;

namespace Star.UI.Room
{
	// Token: 0x02000A7B RID: 2683
	public class RoomObjHaveList
	{
		// Token: 0x1700034A RID: 842
		// (get) Token: 0x060037AC RID: 14252 RVA: 0x0011A477 File Offset: 0x00118877
		public bool IsAvailable
		{
			get
			{
				return this.m_IsAvailable;
			}
		}

		// Token: 0x060037AD RID: 14253 RVA: 0x0011A47F File Offset: 0x0011887F
		public void Open()
		{
			this.m_IsAvailable = false;
			this.m_IsOpen = true;
		}

		// Token: 0x060037AE RID: 14254 RVA: 0x0011A490 File Offset: 0x00118890
		public bool UpdateOpen()
		{
			if (!this.m_IsOpen)
			{
				return false;
			}
			if (this.m_IsAvailable)
			{
				return false;
			}
			this.m_ObjList = new List<RoomObjHaveList.RoomObj>();
			this.m_FilteredObjList = new List<RoomObjHaveList.RoomObj>();
			List<UserRoomObjectData> userRoomObjDatas = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomObjDatas;
			for (int i = 0; i < userRoomObjDatas.Count; i++)
			{
				if (userRoomObjDatas[i].RemainNum > 0)
				{
					RoomObjHaveList.RoomObj roomObj = new RoomObjHaveList.RoomObj();
					roomObj.category = userRoomObjDatas[i].ObjCategory;
					roomObj.id = userRoomObjDatas[i].ObjID;
					roomObj.sortID = RoomObjectListUtil.GetObjectParam(userRoomObjDatas[i].ObjCategory, userRoomObjDatas[i].ObjID).m_Sort;
					this.m_ObjList.Add(roomObj);
				}
			}
			this.m_ObjList.Sort(new Comparison<RoomObjHaveList.RoomObj>(this.Comp));
			this.ClearFilter();
			this.m_IsAvailable = true;
			return true;
		}

		// Token: 0x060037AF RID: 14255 RVA: 0x0011A594 File Offset: 0x00118994
		private int Comp(RoomObjHaveList.RoomObj x, RoomObjHaveList.RoomObj y)
		{
			if (x.sortID < 0 && y.sortID >= 0)
			{
				return 1;
			}
			if (x.sortID >= 0 && y.sortID < 0)
			{
				return -1;
			}
			if (x.sortID > y.sortID)
			{
				return 1;
			}
			if (x.sortID < y.sortID)
			{
				return -1;
			}
			if (x.id > y.id)
			{
				return 1;
			}
			if (x.id < y.id)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x060037B0 RID: 14256 RVA: 0x0011A622 File Offset: 0x00118A22
		public RoomObjHaveList.RoomObj GetItem(int idx)
		{
			if (this.m_IsAvailable && this.m_ObjList.Count > idx)
			{
				return this.m_ObjList[idx];
			}
			return null;
		}

		// Token: 0x060037B1 RID: 14257 RVA: 0x0011A64E File Offset: 0x00118A4E
		public int GetItemNum()
		{
			if (this.m_IsAvailable)
			{
				return this.m_ObjList.Count;
			}
			return 0;
		}

		// Token: 0x060037B2 RID: 14258 RVA: 0x0011A668 File Offset: 0x00118A68
		public void ClearFilter()
		{
			this.m_FilteredObjList.Clear();
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				this.m_FilteredObjList.Add(this.m_ObjList[i]);
			}
		}

		// Token: 0x060037B3 RID: 14259 RVA: 0x0011A6B4 File Offset: 0x00118AB4
		public void DoFilter(eRoomObjectCategory category)
		{
			this.m_FilteredObjList.Clear();
			for (int i = 0; i < this.m_ObjList.Count; i++)
			{
				if (this.m_ObjList[i].category == category)
				{
					this.m_FilteredObjList.Add(this.m_ObjList[i]);
				}
			}
		}

		// Token: 0x060037B4 RID: 14260 RVA: 0x0011A716 File Offset: 0x00118B16
		public RoomObjHaveList.RoomObj GetFilteredItem(int idx)
		{
			if (this.m_IsAvailable && this.m_FilteredObjList.Count > idx)
			{
				return this.m_FilteredObjList[idx];
			}
			return null;
		}

		// Token: 0x060037B5 RID: 14261 RVA: 0x0011A742 File Offset: 0x00118B42
		public int GetFilteredItemNum()
		{
			if (this.m_IsAvailable)
			{
				return this.m_FilteredObjList.Count;
			}
			return 0;
		}

		// Token: 0x04003E89 RID: 16009
		private bool m_IsAvailable;

		// Token: 0x04003E8A RID: 16010
		private bool m_IsOpen;

		// Token: 0x04003E8B RID: 16011
		private List<RoomObjHaveList.RoomObj> m_ObjList;

		// Token: 0x04003E8C RID: 16012
		private List<RoomObjHaveList.RoomObj> m_FilteredObjList;

		// Token: 0x02000A7C RID: 2684
		public class RoomObj
		{
			// Token: 0x04003E8D RID: 16013
			public eRoomObjectCategory category;

			// Token: 0x04003E8E RID: 16014
			public int id;

			// Token: 0x04003E8F RID: 16015
			public int sortID;
		}
	}
}
