﻿using System;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000A8D RID: 2701
	public class RoomShopListUI : MenuUIBase
	{
		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06003815 RID: 14357 RVA: 0x0011C875 File Offset: 0x0011AC75
		public RoomShopListUI.eTransit Transit
		{
			get
			{
				return this.m_Transit;
			}
		}

		// Token: 0x06003816 RID: 14358 RVA: 0x0011C87D File Offset: 0x0011AC7D
		public void SetMode(RoomObjListWindow.eMode mode)
		{
			this.m_Mode = mode;
		}

		// Token: 0x06003817 RID: 14359 RVA: 0x0011C888 File Offset: 0x0011AC88
		public void Setup(RoomObjShopList shopList)
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst == null || !SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				Debug.LogError("GameSystem is failed.");
				return;
			}
			this.m_Transit = RoomShopListUI.eTransit.None;
			this.m_ShopList = shopList;
			this.m_ObjListGroup.Setup(shopList);
		}

		// Token: 0x06003818 RID: 14360 RVA: 0x0011C8D9 File Offset: 0x0011ACD9
		public bool IsOpen()
		{
			return this.m_Step != RoomShopListUI.eStep.Hide && this.m_Step != RoomShopListUI.eStep.End;
		}

		// Token: 0x06003819 RID: 14361 RVA: 0x0011C8F8 File Offset: 0x0011ACF8
		private void Update()
		{
			switch (this.m_Step)
			{
			case RoomShopListUI.eStep.In:
			{
				bool flag = true;
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					if (this.m_AnimUIPlayerArray[i].State == 1)
					{
						flag = false;
						break;
					}
				}
				if (flag)
				{
					this.ChangeStep(RoomShopListUI.eStep.Idle);
				}
				break;
			}
			case RoomShopListUI.eStep.Idle:
				if (this.m_ObjListGroup.IsHideOrOut())
				{
					if (this.m_ObjListGroup.SelectButton == RoomObjListWindow.eButton.Buy)
					{
						this.ChangeStep(RoomShopListUI.eStep.Confirm);
					}
					else if (this.m_ObjListGroup.SelectButton == RoomObjListWindow.eButton.Placement)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = new GameGlobalParameter.RoomShopRequestParam(false, this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID);
						this.m_Transit = RoomShopListUI.eTransit.Room;
						this.PlayOut();
					}
					else if (this.m_ObjListGroup.SelectButton == RoomObjListWindow.eButton.Sell)
					{
						this.ChangeStep(RoomShopListUI.eStep.Sale);
					}
					else
					{
						this.PlayOut();
						this.m_Transit = RoomShopListUI.eTransit.Back;
					}
				}
				break;
			case RoomShopListUI.eStep.Confirm:
				if (this.m_ConfirmGroup.IsHideOrOut())
				{
					RoomShopConfirmGroup.eButton selectButton = this.m_ConfirmGroup.SelectButton;
					if (selectButton != RoomShopConfirmGroup.eButton.Place)
					{
						if (selectButton != RoomShopConfirmGroup.eButton.Store)
						{
							this.ChangeStep(RoomShopListUI.eStep.Idle);
						}
						else
						{
							this.ChangeStep(RoomShopListUI.eStep.NumSelect);
						}
					}
					else
					{
						SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.RoomShopReqParam = new GameGlobalParameter.RoomShopRequestParam(true, this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID);
						this.m_Transit = RoomShopListUI.eTransit.Room;
						this.PlayOut();
					}
				}
				break;
			case RoomShopListUI.eStep.NumSelect:
				if (this.m_BuyWindow.IsHideOrOut())
				{
					if (this.m_BuyWindow.SelectButton == RoomObjBuyWindow.eButton.Yes)
					{
						this.m_ShopList.ExecuteBuy(this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID, this.m_BuyWindow.GetSelectNum(), new Action<long>(this.OnResponseBuy));
						this.ChangeStep(RoomShopListUI.eStep.BuyResponseWait);
					}
					else
					{
						this.ChangeStep(RoomShopListUI.eStep.Confirm);
					}
				}
				break;
			case RoomShopListUI.eStep.Sale:
				if (this.m_BuyWindow.IsHideOrOut())
				{
					if (this.m_BuyWindow.SelectButton == RoomObjBuyWindow.eButton.Yes)
					{
						this.m_ShopList.ExecuteSale(this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID, this.m_BuyWindow.GetSelectNum(), new Action(this.OnResponseSale));
						this.ChangeStep(RoomShopListUI.eStep.SaleResponseWait);
					}
					else
					{
						this.ChangeStep(RoomShopListUI.eStep.Idle);
					}
				}
				break;
			case RoomShopListUI.eStep.Out:
			{
				bool flag2 = true;
				for (int j = 0; j < this.m_AnimUIPlayerArray.Length; j++)
				{
					if (this.m_AnimUIPlayerArray[j].State == 3)
					{
						flag2 = false;
						break;
					}
				}
				if (!this.m_ObjListGroup.IsDonePlayOut)
				{
					flag2 = false;
				}
				if (flag2)
				{
					this.ChangeStep(RoomShopListUI.eStep.End);
				}
				break;
			}
			}
		}

		// Token: 0x0600381A RID: 14362 RVA: 0x0011CC08 File Offset: 0x0011B008
		private void ChangeStep(RoomShopListUI.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case RoomShopListUI.eStep.Hide:
				this.SetEnableInput(false);
				base.gameObject.SetActive(false);
				break;
			case RoomShopListUI.eStep.In:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
				{
					this.m_AnimUIPlayerArray[i].PlayIn();
				}
				break;
			case RoomShopListUI.eStep.Idle:
				this.m_ObjListGroup.Open(this.m_Mode);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case RoomShopListUI.eStep.Confirm:
				this.m_ConfirmGroup.Open(this.m_ShopList.GetItem(this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID), null);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case RoomShopListUI.eStep.NumSelect:
				this.m_BuyWindow.Open(this.m_ShopList.GetItem(this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID), RoomObjBuyWindow.eMode.BuyNumSelect, null);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case RoomShopListUI.eStep.Sale:
				this.m_BuyWindow.Open(this.m_ObjListGroup.SelectObjCategory, this.m_ObjListGroup.SelectObjID, 0L, RoomObjBuyWindow.eMode.SellNumSelect, null);
				base.gameObject.SetActive(true);
				this.SetEnableInput(true);
				break;
			case RoomShopListUI.eStep.Out:
				base.gameObject.SetActive(true);
				this.SetEnableInput(false);
				this.m_ObjListGroup.Close();
				break;
			case RoomShopListUI.eStep.End:
				this.SetEnableInput(false);
				break;
			}
		}

		// Token: 0x0600381B RID: 14363 RVA: 0x0011CDCC File Offset: 0x0011B1CC
		public void OnResponseBuy(long mngID)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_003, "voice_214", true);
			this.ChangeStep(RoomShopListUI.eStep.Idle);
		}

		// Token: 0x0600381C RID: 14364 RVA: 0x0011CDED File Offset: 0x0011B1ED
		public void OnResponseSale()
		{
			this.ChangeStep(RoomShopListUI.eStep.Idle);
		}

		// Token: 0x0600381D RID: 14365 RVA: 0x0011CDF6 File Offset: 0x0011B1F6
		public override void PlayIn()
		{
			this.ChangeStep(RoomShopListUI.eStep.In);
		}

		// Token: 0x0600381E RID: 14366 RVA: 0x0011CE00 File Offset: 0x0011B200
		public override void PlayOut()
		{
			this.ChangeStep(RoomShopListUI.eStep.Out);
			for (int i = 0; i < this.m_AnimUIPlayerArray.Length; i++)
			{
				this.m_AnimUIPlayerArray[i].PlayOut();
			}
		}

		// Token: 0x0600381F RID: 14367 RVA: 0x0011CE3A File Offset: 0x0011B23A
		public override bool IsMenuEnd()
		{
			return this.m_Step == RoomShopListUI.eStep.End;
		}

		// Token: 0x04003EFD RID: 16125
		private RoomShopListUI.eStep m_Step;

		// Token: 0x04003EFE RID: 16126
		[SerializeField]
		private AnimUIPlayer[] m_AnimUIPlayerArray;

		// Token: 0x04003EFF RID: 16127
		[SerializeField]
		private RoomObjListWindow m_ObjListGroup;

		// Token: 0x04003F00 RID: 16128
		[SerializeField]
		private RoomObjBuyWindow m_BuyWindow;

		// Token: 0x04003F01 RID: 16129
		[SerializeField]
		private RoomShopConfirmGroup m_ConfirmGroup;

		// Token: 0x04003F02 RID: 16130
		private RoomObjListWindow.eMode m_Mode = RoomObjListWindow.eMode.Shop;

		// Token: 0x04003F03 RID: 16131
		private RoomShopListUI.eTransit m_Transit;

		// Token: 0x04003F04 RID: 16132
		private RoomObjShopList m_ShopList;

		// Token: 0x02000A8E RID: 2702
		private enum eStep
		{
			// Token: 0x04003F06 RID: 16134
			Hide,
			// Token: 0x04003F07 RID: 16135
			In,
			// Token: 0x04003F08 RID: 16136
			Idle,
			// Token: 0x04003F09 RID: 16137
			Confirm,
			// Token: 0x04003F0A RID: 16138
			NumSelect,
			// Token: 0x04003F0B RID: 16139
			BuyResponseWait,
			// Token: 0x04003F0C RID: 16140
			Sale,
			// Token: 0x04003F0D RID: 16141
			SaleResponseWait,
			// Token: 0x04003F0E RID: 16142
			Out,
			// Token: 0x04003F0F RID: 16143
			End
		}

		// Token: 0x02000A8F RID: 2703
		public enum eTransit
		{
			// Token: 0x04003F11 RID: 16145
			None,
			// Token: 0x04003F12 RID: 16146
			Back,
			// Token: 0x04003F13 RID: 16147
			Room
		}
	}
}
