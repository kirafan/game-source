﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A73 RID: 2675
	public class RoomGreet : UIGroup
	{
		// Token: 0x06003795 RID: 14229 RVA: 0x00119AB8 File Offset: 0x00117EB8
		public bool IsEnableOpen()
		{
			Vector2 position = RectTransformUtility.WorldToScreenPoint(SingletonMonoBehaviour<GameSystem>.Inst.UICamera, this.m_RayCastCheckRect.position);
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = position;
			List<RaycastResult> list = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list);
			return list.Count == 0;
		}

		// Token: 0x06003796 RID: 14230 RVA: 0x00119B10 File Offset: 0x00117F10
		public void Open(List<long> charaMngIDs, RoomGreet.eCategory category)
		{
			base.gameObject.SetActive(true);
			this.m_CharaID = -1;
			for (int i = 0; i < this.m_CharaIconList.Count; i++)
			{
				if (i < charaMngIDs.Count)
				{
					this.m_CharaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngIDs[i]).Param.CharaID;
					this.m_CharaIconList[i].SetActive(true);
					this.m_CharaIconList[i].GetComponentInChildren<CharaIcon>().Apply(this.m_CharaID);
				}
				else
				{
					this.m_CharaIconList[i].SetActive(false);
				}
			}
			this.m_Category = category;
			this.Prepare(this.m_CharaID);
			this.m_IsOpen = true;
			base.Open();
		}

		// Token: 0x06003797 RID: 14231 RVA: 0x00119BE3 File Offset: 0x00117FE3
		private void Prepare(int charaID)
		{
			this.m_WaitTime = 0f;
			this.m_IsDonePrepare = false;
		}

		// Token: 0x06003798 RID: 14232 RVA: 0x00119BF8 File Offset: 0x00117FF8
		private bool UpdatePrepare()
		{
			if (!this.m_IsDonePrepare)
			{
				string text = null;
				switch (this.m_Category)
				{
				case RoomGreet.eCategory.WakeUp:
					text = "おはよう";
					break;
				case RoomGreet.eCategory.GoTown:
					text = "いってきます";
					break;
				case RoomGreet.eCategory.GoHome:
					text = "ただいま";
					break;
				case RoomGreet.eCategory.Sleep:
					text = "おやすみ";
					break;
				case RoomGreet.eCategory.Visit:
					text = "おじゃまします";
					break;
				}
				this.m_Text.text = text;
				this.m_IsDonePrepare = true;
				if (this.m_Category == RoomGreet.eCategory.Visit)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ROOM_ACT_INTERPHONE, 1f, 0, -1, -1);
				}
			}
			return this.m_IsDonePrepare;
		}

		// Token: 0x06003799 RID: 14233 RVA: 0x00119CC2 File Offset: 0x001180C2
		public override void Close()
		{
			this.m_IsOpen = false;
			base.Close();
		}

		// Token: 0x0600379A RID: 14234 RVA: 0x00119CD4 File Offset: 0x001180D4
		public override void Update()
		{
			if (this.m_IsOpen)
			{
				if (!this.m_IsDonePrepare)
				{
					this.UpdatePrepare();
				}
				if (base.IsIdle())
				{
					this.m_WaitTime += 1f * Time.deltaTime;
					if (this.m_WaitTime > this.m_DisplayTime)
					{
						this.Close();
					}
				}
			}
			base.Update();
		}

		// Token: 0x04003E58 RID: 15960
		[SerializeField]
		[Tooltip("表示時間")]
		private float m_DisplayTime = 2f;

		// Token: 0x04003E59 RID: 15961
		[SerializeField]
		private List<GameObject> m_CharaIconList = new List<GameObject>();

		// Token: 0x04003E5A RID: 15962
		[SerializeField]
		private Text m_Text;

		// Token: 0x04003E5B RID: 15963
		[SerializeField]
		private RectTransform m_RayCastCheckRect;

		// Token: 0x04003E5C RID: 15964
		private int m_CharaID = -1;

		// Token: 0x04003E5D RID: 15965
		private RoomGreet.eCategory m_Category;

		// Token: 0x04003E5E RID: 15966
		private string m_TweetText;

		// Token: 0x04003E5F RID: 15967
		private bool m_IsOpen;

		// Token: 0x04003E60 RID: 15968
		private float m_WaitTime;

		// Token: 0x04003E61 RID: 15969
		private bool m_IsDonePrepare;

		// Token: 0x02000A74 RID: 2676
		public enum eCategory
		{
			// Token: 0x04003E63 RID: 15971
			None,
			// Token: 0x04003E64 RID: 15972
			Morning,
			// Token: 0x04003E65 RID: 15973
			Night,
			// Token: 0x04003E66 RID: 15974
			WakeUp,
			// Token: 0x04003E67 RID: 15975
			GoTown,
			// Token: 0x04003E68 RID: 15976
			GoHome,
			// Token: 0x04003E69 RID: 15977
			Sleep,
			// Token: 0x04003E6A RID: 15978
			Visit
		}
	}
}
