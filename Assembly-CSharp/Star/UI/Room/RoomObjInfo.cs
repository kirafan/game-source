﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A7D RID: 2685
	public class RoomObjInfo : MonoBehaviour
	{
		// Token: 0x060037B8 RID: 14264 RVA: 0x0011A76C File Offset: 0x00118B6C
		public void Apply(eRoomObjectCategory category, int objID, long buyPrice)
		{
			this.m_Icon.Apply(category, objID);
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(category, objID);
			this.m_NameText.text = objectParam.m_Name;
			this.m_Descript.text = objectParam.m_Description;
			UserRoomObjectData userRoomObjData = UserRoomUtil.GetUserRoomObjData(category, objID);
			if (userRoomObjData != null)
			{
				this.m_HaveNumText.text = UserRoomUtil.GetUserRoomObjData(category, objID).HaveNum + " / " + objectParam.m_MaxNum;
			}
			else
			{
				this.m_HaveNumText.text = "0 / " + objectParam.m_MaxNum;
			}
			this.m_SizeText.text = objectParam.m_SizeX + "×" + objectParam.m_SizeY;
			if (this.m_BuyPriceText != null)
			{
				this.m_BuyPriceText.text = UIUtility.GoldValueToString(buyPrice, true);
			}
			this.m_SalePriceText.text = UIUtility.GoldValueToString(objectParam.m_SellAmount, true);
		}

		// Token: 0x04003E90 RID: 16016
		[SerializeField]
		private Text m_NameText;

		// Token: 0x04003E91 RID: 16017
		[SerializeField]
		private RoomObjectIcon m_Icon;

		// Token: 0x04003E92 RID: 16018
		[SerializeField]
		private Text m_Descript;

		// Token: 0x04003E93 RID: 16019
		[SerializeField]
		private Text m_HaveNumText;

		// Token: 0x04003E94 RID: 16020
		[SerializeField]
		private Text m_SizeText;

		// Token: 0x04003E95 RID: 16021
		[SerializeField]
		private Text m_BuyPriceText;

		// Token: 0x04003E96 RID: 16022
		[SerializeField]
		private Text m_SalePriceText;
	}
}
