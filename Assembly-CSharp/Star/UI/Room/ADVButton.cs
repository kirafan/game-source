﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000A6E RID: 2670
	public class ADVButton : MonoBehaviour
	{
		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06003782 RID: 14210 RVA: 0x001196E3 File Offset: 0x00117AE3
		public List<int> ScheduleAdvIDs
		{
			get
			{
				return this.m_ScheduleAdvIDs;
			}
		}

		// Token: 0x06003783 RID: 14211 RVA: 0x001196EC File Offset: 0x00117AEC
		public void SetEnableRender(bool flg)
		{
			this.m_IsEnableRender = flg;
			if (this.m_IsEnableRender && this.m_ScheduleAdvIDs != null && this.m_ScheduleAdvIDs.Count > 0)
			{
				base.gameObject.SetActive(true);
			}
			else
			{
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x06003784 RID: 14212 RVA: 0x00119744 File Offset: 0x00117B44
		public void SetScheduleAdvIDs(List<int> scheduleAdvIDs)
		{
			this.m_ScheduleAdvIDs = scheduleAdvIDs;
			this.SetEnableRender(this.m_IsEnableRender);
		}

		// Token: 0x04003E4C RID: 15948
		[SerializeField]
		private CustomButton m_Button;

		// Token: 0x04003E4D RID: 15949
		private List<int> m_ScheduleAdvIDs;

		// Token: 0x04003E4E RID: 15950
		private bool m_IsEnableRender = true;
	}
}
