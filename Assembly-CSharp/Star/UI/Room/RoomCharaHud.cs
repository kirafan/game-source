﻿using System;
using UnityEngine;

namespace Star.UI.Room
{
	// Token: 0x02000A70 RID: 2672
	public class RoomCharaHud : MonoBehaviour
	{
		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06003789 RID: 14217 RVA: 0x00119791 File Offset: 0x00117B91
		public RectTransform CacheTransform
		{
			get
			{
				return this.m_Transform;
			}
		}

		// Token: 0x0600378A RID: 14218 RVA: 0x00119799 File Offset: 0x00117B99
		private void Awake()
		{
			this.m_Transform = base.gameObject.GetComponent<RectTransform>();
			this.m_CharaTweet.gameObject.SetActive(false);
		}

		// Token: 0x0600378B RID: 14219 RVA: 0x001197BD File Offset: 0x00117BBD
		public CharaTweet GetCharaTweet()
		{
			return this.m_CharaTweet;
		}

		// Token: 0x04003E51 RID: 15953
		[SerializeField]
		private CharaTweet m_CharaTweet;

		// Token: 0x04003E52 RID: 15954
		private RectTransform m_Transform;
	}
}
