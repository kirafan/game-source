﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Room
{
	// Token: 0x02000A7F RID: 2687
	public class RoomObjListItem : ScrollItemIcon
	{
		// Token: 0x060037BC RID: 14268 RVA: 0x0011A8A8 File Offset: 0x00118CA8
		protected override void ApplyData()
		{
			RoomObjectItemData roomObjectItemData = (RoomObjectItemData)this.m_Data;
			RoomObjectListDB_Param objectParam = RoomObjectListUtil.GetObjectParam(roomObjectItemData.m_ObjCategory, roomObjectItemData.m_ObjID);
			this.m_Icon.Apply(roomObjectItemData.m_ObjCategory, roomObjectItemData.m_ObjID);
			UserRoomObjectData userRoomObjData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData(roomObjectItemData.m_ObjCategory, roomObjectItemData.m_ObjID);
			this.m_NameTextObj.text = objectParam.m_Name;
			if (roomObjectItemData.m_Mode == RoomObjectItemData.eMode.Have)
			{
				if (userRoomObjData != null)
				{
					this.m_HaveNumTextObj.text = userRoomObjData.RemainNum.ToString() + "/" + objectParam.m_MaxNum.ToString();
				}
				else
				{
					this.m_HaveNumTextObj.text = 0 + "/" + objectParam.m_MaxNum.ToString().PadLeft(2);
				}
				if (!this.m_HaveObj.activeSelf)
				{
					this.m_HaveObj.SetActive(true);
				}
				if (this.m_ShopObj.activeSelf)
				{
					this.m_ShopObj.SetActive(false);
				}
				if (RoomUtility.IsPlacementCountObj(roomObjectItemData.m_ObjCategory) && SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjPlacementNum() >= 50)
				{
					this.m_PlacementButton.Interactable = false;
				}
				else
				{
					this.m_PlacementButton.Interactable = true;
				}
				this.m_SalePriceTextObj.text = UIUtility.GoldValueToString(objectParam.m_SellAmount, true);
				this.m_SaleIconObj.SetActive(false);
				this.m_PickupIconObj.SetActive(false);
			}
			else
			{
				if (userRoomObjData != null)
				{
					this.m_HaveNumTextObj.text = userRoomObjData.HaveNum.ToString().PadLeft(2) + "/" + objectParam.m_MaxNum.ToString().PadLeft(2);
				}
				else
				{
					this.m_HaveNumTextObj.text = 0 + "/" + objectParam.m_MaxNum.ToString().PadLeft(2);
				}
				this.m_CostTextObj.text = UIUtility.GoldValueToString(roomObjectItemData.m_Cost, true);
				if (this.m_HaveObj.activeSelf)
				{
					this.m_HaveObj.SetActive(false);
				}
				if (!this.m_ShopObj.activeSelf)
				{
					this.m_ShopObj.SetActive(true);
				}
				if (userRoomObjData != null && userRoomObjData.HaveNum >= (int)objectParam.m_MaxNum)
				{
					this.m_BuyButton.Interactable = false;
				}
				else if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.IsShortOfGold((long)roomObjectItemData.m_Cost))
				{
					this.m_BuyButton.Interactable = false;
				}
				else
				{
					this.m_BuyButton.Interactable = true;
				}
				this.m_SaleIconObj.SetActive(roomObjectItemData.m_IsSale && !roomObjectItemData.m_IsPickup);
				this.m_PickupIconObj.SetActive(roomObjectItemData.m_IsPickup);
			}
		}

		// Token: 0x060037BD RID: 14269 RVA: 0x0011ABBC File Offset: 0x00118FBC
		public void SetAttachCanvas()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsRoomTutorial())
			{
				this.m_TutorialTarget.SetEnable(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(true);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowDirection(TutorialMessage.eArrowDirection.LeftTop);
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetArrowPosition(this.m_BuyButton.GetComponent<RectTransform>().position);
			}
		}

		// Token: 0x060037BE RID: 14270 RVA: 0x0011AC28 File Offset: 0x00119028
		public void OnPlacementButtonCallBack()
		{
			RoomObjectItemData roomObjectItemData = (RoomObjectItemData)this.m_Data;
			roomObjectItemData.OnPlacementButtonCallBack();
		}

		// Token: 0x060037BF RID: 14271 RVA: 0x0011AC48 File Offset: 0x00119048
		public void OnSellButtonCallBack()
		{
			RoomObjectItemData roomObjectItemData = (RoomObjectItemData)this.m_Data;
			roomObjectItemData.OnSellButtonCallBack();
		}

		// Token: 0x060037C0 RID: 14272 RVA: 0x0011AC68 File Offset: 0x00119068
		public void OnBuyButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.TutorialMessage.SetEnableArrow(false);
			this.m_TutorialTarget.SetEnable(false);
			RoomObjectItemData roomObjectItemData = (RoomObjectItemData)this.m_Data;
			roomObjectItemData.OnBuyButtonCallBack();
		}

		// Token: 0x04003E98 RID: 16024
		[SerializeField]
		private RoomObjectIcon m_Icon;

		// Token: 0x04003E99 RID: 16025
		[SerializeField]
		private Text m_NameTextObj;

		// Token: 0x04003E9A RID: 16026
		[SerializeField]
		[Tooltip("")]
		private Text m_HaveNumTextObj;

		// Token: 0x04003E9B RID: 16027
		[SerializeField]
		private Text m_CostTextObj;

		// Token: 0x04003E9C RID: 16028
		[SerializeField]
		private Text m_SalePriceTextObj;

		// Token: 0x04003E9D RID: 16029
		[SerializeField]
		private GameObject m_HaveObj;

		// Token: 0x04003E9E RID: 16030
		[SerializeField]
		private GameObject m_ShopObj;

		// Token: 0x04003E9F RID: 16031
		[SerializeField]
		private CustomButton m_PlacementButton;

		// Token: 0x04003EA0 RID: 16032
		[SerializeField]
		private CustomButton m_BuyButton;

		// Token: 0x04003EA1 RID: 16033
		[SerializeField]
		private GameObject m_SaleIconObj;

		// Token: 0x04003EA2 RID: 16034
		[SerializeField]
		private GameObject m_PickupIconObj;

		// Token: 0x04003EA3 RID: 16035
		[SerializeField]
		private TutorialTarget m_TutorialTarget;
	}
}
