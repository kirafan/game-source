﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Star.UI
{
	// Token: 0x02000B1C RID: 2844
	[RequireComponent(typeof(InvisibleGraphic))]
	public class UIRaycastChecker : MonoBehaviour
	{
		// Token: 0x06003BBE RID: 15294 RVA: 0x00130F6A File Offset: 0x0012F36A
		private void Start()
		{
			this.m_Graph = base.GetComponent<InvisibleGraphic>();
			this.m_Graph.raycastTarget = false;
			this.m_Rect = this.m_Graph.GetComponent<RectTransform>();
		}

		// Token: 0x06003BBF RID: 15295 RVA: 0x00130F98 File Offset: 0x0012F398
		public bool Check()
		{
			if (this.m_Graph == null)
			{
				return false;
			}
			this.m_Graph.raycastTarget = true;
			Vector2 position = RectTransformUtility.WorldToScreenPoint(SingletonMonoBehaviour<GameSystem>.Inst.UICamera, this.m_Rect.position);
			PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
			pointerEventData.position = position;
			List<RaycastResult> list = new List<RaycastResult>();
			EventSystem.current.RaycastAll(pointerEventData, list);
			this.m_Graph.raycastTarget = false;
			return list.Count > 0 && list[0].gameObject == base.gameObject;
		}

		// Token: 0x0400437E RID: 17278
		private InvisibleGraphic m_Graph;

		// Token: 0x0400437F RID: 17279
		private RectTransform m_Rect;
	}
}
