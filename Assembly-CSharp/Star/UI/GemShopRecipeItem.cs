﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020009A7 RID: 2471
	public class GemShopRecipeItem : ScrollItemIcon
	{
		// Token: 0x06003327 RID: 13095 RVA: 0x001043D8 File Offset: 0x001027D8
		protected override void ApplyData()
		{
			GemShopRecipeItemData gemShopRecipeItemData = (GemShopRecipeItemData)this.m_Data;
			this.m_TitleNameText.text = gemShopRecipeItemData.m_Title;
			this.m_DetailText.text = gemShopRecipeItemData.m_Detail;
			this.m_AmountText.text = gemShopRecipeItemData.m_Amount;
			this.m_SpecialObj.SetActive(gemShopRecipeItemData.m_UIType != StoreDefine.eUIType.Normal);
		}

		// Token: 0x0400395A RID: 14682
		[SerializeField]
		private Text m_TitleNameText;

		// Token: 0x0400395B RID: 14683
		[SerializeField]
		private GameObject m_SpecialObj;

		// Token: 0x0400395C RID: 14684
		[SerializeField]
		private Text m_DetailText;

		// Token: 0x0400395D RID: 14685
		[SerializeField]
		private Text m_AmountText;
	}
}
