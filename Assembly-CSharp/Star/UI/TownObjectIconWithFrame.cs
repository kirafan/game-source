﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000875 RID: 2165
	public class TownObjectIconWithFrame : MonoBehaviour
	{
		// Token: 0x06002CD0 RID: 11472 RVA: 0x000EC6FA File Offset: 0x000EAAFA
		public void Apply(int townObjectID, int lv)
		{
			this.m_Icon.Apply(townObjectID, lv);
		}

		// Token: 0x06002CD1 RID: 11473 RVA: 0x000EC709 File Offset: 0x000EAB09
		public void Destroy()
		{
			if (this.m_Icon != null)
			{
				this.m_Icon.Destroy();
			}
		}

		// Token: 0x040033CB RID: 13259
		[SerializeField]
		private TownObjectIcon m_Icon;
	}
}
