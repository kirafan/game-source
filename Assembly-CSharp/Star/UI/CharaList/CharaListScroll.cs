﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.CharaList
{
	// Token: 0x020007FA RID: 2042
	public class CharaListScroll : ScrollViewBase
	{
		// Token: 0x14000059 RID: 89
		// (add) Token: 0x06002A3B RID: 10811 RVA: 0x000DEB24 File Offset: 0x000DCF24
		// (remove) Token: 0x06002A3C RID: 10812 RVA: 0x000DEB5C File Offset: 0x000DCF5C
		public event Action<long> OnClickIcon;

		// Token: 0x1400005A RID: 90
		// (add) Token: 0x06002A3D RID: 10813 RVA: 0x000DEB94 File Offset: 0x000DCF94
		// (remove) Token: 0x06002A3E RID: 10814 RVA: 0x000DEBCC File Offset: 0x000DCFCC
		public event Action<long> OnHoldIcon;

		// Token: 0x1400005B RID: 91
		// (add) Token: 0x06002A3F RID: 10815 RVA: 0x000DEC04 File Offset: 0x000DD004
		// (remove) Token: 0x06002A40 RID: 10816 RVA: 0x000DEC3C File Offset: 0x000DD03C
		public event Action OnClickRemoveIcon;

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x06002A41 RID: 10817 RVA: 0x000DEC72 File Offset: 0x000DD072
		public int CharaNum
		{
			get
			{
				if (this.m_FilteredDataList != null)
				{
					return this.m_FilteredDataList.Count;
				}
				return 0;
			}
		}

		// Token: 0x170002AD RID: 685
		// (get) Token: 0x06002A42 RID: 10818 RVA: 0x000DEC8C File Offset: 0x000DD08C
		public CharaListScroll.eMode Mode
		{
			get
			{
				return this.m_Mode;
			}
		}

		// Token: 0x06002A43 RID: 10819 RVA: 0x000DEC94 File Offset: 0x000DD094
		public void SetViewMode()
		{
			this.m_Mode = CharaListScroll.eMode.View;
			this.SetEnbaleRemoveIcon(false);
			this.ApplyMode();
		}

		// Token: 0x06002A44 RID: 10820 RVA: 0x000DECAA File Offset: 0x000DD0AA
		public void SetPartyEditMode(int partyIdx, int memberIdx)
		{
			this.m_PartyIdx = partyIdx;
			this.m_MemberIdx = memberIdx;
			this.m_Mode = CharaListScroll.eMode.PartyEdit;
			this.SetEnbaleRemoveIcon(true);
			this.ApplyMode();
		}

		// Token: 0x06002A45 RID: 10821 RVA: 0x000DECCE File Offset: 0x000DD0CE
		public void SetSupportEditMode(int supportPartyIdx, int supportMemberIdx)
		{
			this.m_PartyIdx = supportPartyIdx;
			this.m_MemberIdx = supportMemberIdx;
			this.m_Mode = CharaListScroll.eMode.SupportEdit;
			this.SetEnbaleRemoveIcon(true);
			this.ApplyMode();
		}

		// Token: 0x06002A46 RID: 10822 RVA: 0x000DECF2 File Offset: 0x000DD0F2
		public void SetUpgradeMode()
		{
			this.m_Mode = CharaListScroll.eMode.Upgrade;
			this.SetEnbaleRemoveIcon(false);
			this.ApplyMode();
		}

		// Token: 0x06002A47 RID: 10823 RVA: 0x000DED08 File Offset: 0x000DD108
		public void SetLimitBreakMode()
		{
			this.m_Mode = CharaListScroll.eMode.LimitBreak;
			this.SetEnbaleRemoveIcon(false);
			this.ApplyMode();
		}

		// Token: 0x06002A48 RID: 10824 RVA: 0x000DED1E File Offset: 0x000DD11E
		public void SetEvolutionMode()
		{
			this.m_Mode = CharaListScroll.eMode.Evolution;
			this.SetEnbaleRemoveIcon(false);
			this.ApplyMode();
		}

		// Token: 0x06002A49 RID: 10825 RVA: 0x000DED34 File Offset: 0x000DD134
		public void SetRoomMemberMode(long[] liveCharaMngIDs, int memberIdx)
		{
			this.m_Mode = CharaListScroll.eMode.Room;
			this.m_LiveCharaMngIDs = liveCharaMngIDs;
			this.m_RoomMemberIdx = memberIdx;
			if (liveCharaMngIDs[memberIdx] == -1L || memberIdx == 0)
			{
				this.SetEnbaleRemoveIcon(false);
			}
			else
			{
				this.SetEnbaleRemoveIcon(true);
			}
			this.ApplyMode();
		}

		// Token: 0x06002A4A RID: 10826 RVA: 0x000DED74 File Offset: 0x000DD174
		public void SetEnbaleRemoveIcon(bool flg)
		{
			this.m_EnableRemoveIcon = flg;
		}

		// Token: 0x06002A4B RID: 10827 RVA: 0x000DED80 File Offset: 0x000DD180
		private void ApplyMode()
		{
			if (this.m_ItemDataList != null)
			{
				if (this.m_EnableRemoveIcon && (this.m_Mode != CharaListScroll.eMode.PartyEdit || EditUtility.CanBeRemoveFromParty(this.m_PartyIdx, this.m_MemberIdx)))
				{
					this.AddRemoveIcon();
				}
				else
				{
					if (this.m_RemoveIconViewItem != null)
					{
						this.m_RemoveIconViewItem.m_ItemIconObject.gameObject.SetActive(false);
					}
					this.m_ViewItemStartIdx = 0;
				}
				for (int i = 0; i < this.m_ItemDataList.Count; i++)
				{
					if (this.m_ItemDataList[i] != null)
					{
						CharaListScrollItemData charaListScrollItemData = (CharaListScrollItemData)this.m_ItemDataList[i];
						UserCharacterData userCharacterData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[charaListScrollItemData.m_CharaIdx];
						charaListScrollItemData.m_IsEnable = true;
						charaListScrollItemData.m_CantSelect = false;
						switch (this.m_Mode)
						{
						case CharaListScroll.eMode.PartyEdit:
							if (charaListScrollItemData.m_MemberIdx == -1 && EditUtility.IsSameNamedOnChange(this.m_PartyIdx, this.m_MemberIdx, userCharacterData.MngID))
							{
								charaListScrollItemData.m_IsEnable = false;
							}
							break;
						case CharaListScroll.eMode.SupportEdit:
							if (!EditUtility.CanBeAssignToSupportParty(this.m_PartyIdx, this.m_MemberIdx, userCharacterData.MngID))
							{
								charaListScrollItemData.m_IsEnable = false;
							}
							break;
						case CharaListScroll.eMode.Upgrade:
							if (!EditUtility.CanUpgrade(userCharacterData.MngID))
							{
								charaListScrollItemData.m_IsEnable = false;
							}
							break;
						case CharaListScroll.eMode.LimitBreak:
							if (EditUtility.IsMaxLimitBreak(userCharacterData.MngID))
							{
								charaListScrollItemData.m_IsEnable = false;
							}
							else if (!EditUtility.CanLimitBreak(userCharacterData.MngID))
							{
								charaListScrollItemData.m_CantSelect = true;
							}
							break;
						case CharaListScroll.eMode.Evolution:
							if (!EditUtility.IsExistEvolution(userCharacterData.MngID) || EditUtility.IsHaveEvolutionChara(userCharacterData.MngID))
							{
								charaListScrollItemData.m_IsEnable = false;
							}
							else if (!EditUtility.CanEvolution(userCharacterData.MngID, false) && !EditUtility.CanEvolution(userCharacterData.MngID, true))
							{
								charaListScrollItemData.m_CantSelect = true;
							}
							break;
						case CharaListScroll.eMode.Room:
						{
							eCharaNamedType namedType = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharacterData.Param.CharaID).m_NamedType;
							charaListScrollItemData.m_RoomInIdx = -1;
							for (int j = 0; j < this.m_LiveCharaMngIDs.Length; j++)
							{
								if (this.m_LiveCharaMngIDs[j] != -1L)
								{
									if (userCharacterData.MngID == this.m_LiveCharaMngIDs[j])
									{
										if (this.m_RoomMemberIdx == j)
										{
											charaListScrollItemData.m_RoomInIdx = j;
										}
										charaListScrollItemData.m_IsEnable = false;
									}
									else if (this.m_RoomMemberIdx != j)
									{
										int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(this.m_LiveCharaMngIDs[j]).Param.CharaID;
										eCharaNamedType namedType2 = (eCharaNamedType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(charaID).m_NamedType;
										if (namedType == namedType2)
										{
											charaListScrollItemData.m_IsEnable = false;
											break;
										}
									}
								}
							}
							break;
						}
						}
					}
				}
				this.RebuildScroll();
				base.Refresh();
			}
		}

		// Token: 0x06002A4C RID: 10828 RVA: 0x000DF0B7 File Offset: 0x000DD4B7
		protected override void HideItem(ScrollViewBase.DispItem item)
		{
			base.HideItem(item);
			item.m_ItemIconObject.GetComponent<Canvas>().enabled = false;
		}

		// Token: 0x06002A4D RID: 10829 RVA: 0x000DF0D1 File Offset: 0x000DD4D1
		protected override void ShowItem(ScrollViewBase.DispItem item)
		{
			base.ShowItem(item);
			item.m_ItemIconObject.GetComponent<Canvas>().enabled = true;
		}

		// Token: 0x06002A4E RID: 10830 RVA: 0x000DF0EC File Offset: 0x000DD4EC
		public void AddRemoveIcon()
		{
			this.m_ViewItemStartIdx = 1;
			if (this.m_RemoveViewIconNum <= 0)
			{
				ScrollViewBase.DispItem dispItem = new ScrollViewBase.DispItem();
				dispItem.m_Idx = 0;
				dispItem.m_Visible = false;
				dispItem.m_PositionX = 0;
				dispItem.m_PositionY = 0;
				CharaListScrollRemoveItem charaListScrollRemoveItem = UnityEngine.Object.Instantiate<CharaListScrollRemoveItem>(this.m_RemoveIconPrefab, this.m_Content);
				charaListScrollRemoveItem.Scroll = this;
				charaListScrollRemoveItem.RectTransform.pivot = new Vector2(0f, 1f);
				charaListScrollRemoveItem.RectTransform.anchorMin = new Vector2(0f, 1f);
				charaListScrollRemoveItem.RectTransform.anchorMax = new Vector2(0f, 1f);
				charaListScrollRemoveItem.RectTransform.anchoredPosition = new Vector3(0f * (this.m_ItemWidth + this.m_SpaceX) + this.m_MarginX, --0f * (this.m_ItemHeight + this.m_SpaceY) - this.m_MarginY, 0f);
				charaListScrollRemoveItem.RectTransform.localScale = Vector3.one;
				CharaListScrollRemoveItem charaListScrollRemoveItem2 = charaListScrollRemoveItem;
				charaListScrollRemoveItem2.OnClickIcon = (Action<ScrollItemIcon>)Delegate.Combine(charaListScrollRemoveItem2.OnClickIcon, new Action<ScrollItemIcon>(this.OnClickRemoveIconCallBack));
				charaListScrollRemoveItem.gameObject.SetActive(true);
				dispItem.m_ItemIconObject = charaListScrollRemoveItem;
				this.m_RemoveIconViewItem = dispItem;
				this.m_RemoveViewIconNum++;
			}
			else if (this.m_RemoveIconViewItem != null)
			{
				this.m_RemoveIconViewItem.m_ItemIconObject.gameObject.SetActive(true);
			}
			this.m_IsDirty = true;
		}

		// Token: 0x06002A4F RID: 10831 RVA: 0x000DF26A File Offset: 0x000DD66A
		protected override void OnClickIconCallBack(ScrollItemIcon itemIcon)
		{
			this.OnClickIcon.Call(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[((CharaListScrollItemData)itemIcon.GetItemData()).m_CharaIdx].MngID);
		}

		// Token: 0x06002A50 RID: 10832 RVA: 0x000DF2A0 File Offset: 0x000DD6A0
		protected override void OnHoldIconCallBack(ScrollItemIcon itemIcon)
		{
			this.OnHoldIcon.Call(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[((CharaListScrollItemData)itemIcon.GetItemData()).m_CharaIdx].MngID);
		}

		// Token: 0x06002A51 RID: 10833 RVA: 0x000DF2D6 File Offset: 0x000DD6D6
		public void OnClickRemoveIconCallBack(ScrollItemIcon itemIcon)
		{
			this.OnClickRemoveIcon.Call();
		}

		// Token: 0x06002A52 RID: 10834 RVA: 0x000DF2E4 File Offset: 0x000DD6E4
		public void SetupList()
		{
			this.m_ViewItemStartIdx = 0;
			this.m_RawDataList.Clear();
			int count = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas.Count;
			for (int i = 0; i < count; i++)
			{
				CharaListScrollItemData charaListScrollItemData = new CharaListScrollItemData();
				charaListScrollItemData.m_CharaIdx = i;
				charaListScrollItemData.m_IsEnable = false;
				if (this.m_Mode == CharaListScroll.eMode.PartyEdit)
				{
					charaListScrollItemData.m_MemberIdx = EditUtility.CheckPartyChara(this.m_PartyIdx, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[i].MngID);
				}
				else if (this.m_Mode == CharaListScroll.eMode.SupportEdit)
				{
					charaListScrollItemData.m_MemberIdx = EditUtility.CheckSupportPartyChara(this.m_PartyIdx, SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[i].MngID);
				}
				else
				{
					charaListScrollItemData.m_MemberIdx = -1;
				}
				this.m_RawDataList.Add(charaListScrollItemData);
			}
			this.RemoveAll();
			this.DoFilter();
			this.DoSort();
			for (int j = 0; j < this.m_FilteredDataList.Count; j++)
			{
				this.AddItem(this.m_FilteredDataList[j]);
			}
			this.ApplyMode();
		}

		// Token: 0x06002A53 RID: 10835 RVA: 0x000DF410 File Offset: 0x000DD810
		private void DoFilter()
		{
			this.m_FilteredDataList.Clear();
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			for (int i = 0; i < this.m_RawDataList.Count; i++)
			{
				CharacterParam param = inst.UserDataMng.UserCharaDatas[this.m_RawDataList[i].m_CharaIdx].Param;
				if (inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.CharaListView, UISettings.eFilterCategory.Class, (int)param.ClassType))
				{
					if (inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.CharaListView, UISettings.eFilterCategory.Rare, (int)param.RareType))
					{
						if (inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.CharaListView, UISettings.eFilterCategory.Element, (int)param.ElementType))
						{
							int titleType = (int)inst.DbMng.NamedListDB.GetTitleType(param.NamedType);
							if (inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.CharaListView, UISettings.eFilterCategory.Title, titleType))
							{
								this.m_FilteredDataList.Add(this.m_RawDataList[i]);
							}
						}
					}
				}
			}
		}

		// Token: 0x06002A54 RID: 10836 RVA: 0x000DF510 File Offset: 0x000DD910
		private void DoSort()
		{
			this.m_FilteredDataList.Sort(new Comparison<CharaListScrollItemData>(this.SortCompare));
			for (int i = 0; i < this.m_FilteredDataList.Count; i++)
			{
				this.m_FilteredDataList[i].m_SortedIdx = i;
			}
		}

		// Token: 0x06002A55 RID: 10837 RVA: 0x000DF564 File Offset: 0x000DD964
		private int SortCompare(CharaListScrollItemData r, CharaListScrollItemData l)
		{
			int num = -1;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamOrderType(UISettings.eUsingType_Sort.CharaListView) == 1)
			{
				num = 1;
			}
			if (this.m_Mode == CharaListScroll.eMode.PartyEdit || this.m_Mode == CharaListScroll.eMode.SupportEdit)
			{
				if (r.m_MemberIdx != -1 && l.m_MemberIdx == -1)
				{
					return -1;
				}
				if (r.m_MemberIdx == -1 && l.m_MemberIdx != -1)
				{
					return 1;
				}
				if (r.m_MemberIdx != -1 && l.m_MemberIdx != -1)
				{
					if (r.m_MemberIdx < l.m_MemberIdx)
					{
						return -1;
					}
					return 1;
				}
			}
			if (this.GetSortKey(r.m_CharaIdx) < this.GetSortKey(l.m_CharaIdx))
			{
				return num;
			}
			if (this.GetSortKey(r.m_CharaIdx) > this.GetSortKey(l.m_CharaIdx))
			{
				return -num;
			}
			int charaID = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[r.m_CharaIdx].Param.CharaID;
			int charaID2 = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[l.m_CharaIdx].Param.CharaID;
			if (charaID < charaID2)
			{
				return -1;
			}
			if (charaID > charaID2)
			{
				return 1;
			}
			if (r.m_CharaIdx < l.m_CharaIdx)
			{
				return -1;
			}
			if (r.m_CharaIdx > l.m_CharaIdx)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06002A56 RID: 10838 RVA: 0x000DF6C8 File Offset: 0x000DDAC8
		private long GetSortKey(int idx)
		{
			switch (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.CharaListView))
			{
			case 0:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Lv;
			case 1:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.RareType;
			case 2:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Cost;
			case 3:
			{
				eCharaNamedType namedType = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.NamedType;
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(namedType).m_TitleType;
			}
			case 4:
				return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.MngID;
			case 5:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Hp;
			case 6:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Atk;
			case 7:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Mgc;
			case 8:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Def;
			case 9:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.MDef;
			case 10:
				return (long)SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[idx].Param.Spd;
			default:
				return (long)idx;
			}
		}

		// Token: 0x06002A57 RID: 10839 RVA: 0x000DF8A8 File Offset: 0x000DDCA8
		protected override void UpdateItemPosition()
		{
			base.UpdateItemPosition();
			if (this.m_RemoveIconViewItem != null)
			{
				this.m_RemoveIconViewItem.m_ItemIconObject.RectTransform.anchoredPosition = new Vector3(0f * (this.m_ItemWidth + this.m_SpaceX) + this.m_MarginX, --0f * (this.m_ItemHeight + this.m_SpaceY) - this.m_MarginY, 0f);
			}
		}

		// Token: 0x040030A0 RID: 12448
		private CharaListScroll.eMode m_Mode;

		// Token: 0x040030A1 RID: 12449
		private List<CharaListScrollItemData> m_RawDataList = new List<CharaListScrollItemData>();

		// Token: 0x040030A2 RID: 12450
		private List<CharaListScrollItemData> m_FilteredDataList = new List<CharaListScrollItemData>();

		// Token: 0x040030A3 RID: 12451
		[SerializeField]
		private CharaListScrollRemoveItem m_RemoveIconPrefab;

		// Token: 0x040030A4 RID: 12452
		private ScrollViewBase.DispItem m_RemoveIconViewItem;

		// Token: 0x040030A5 RID: 12453
		private int m_RemoveViewIconNum;

		// Token: 0x040030A6 RID: 12454
		private int m_PartyIdx = -1;

		// Token: 0x040030A7 RID: 12455
		private int m_MemberIdx = -1;

		// Token: 0x040030A8 RID: 12456
		private long[] m_LiveCharaMngIDs;

		// Token: 0x040030A9 RID: 12457
		private int m_RoomMemberIdx = -1;

		// Token: 0x040030AA RID: 12458
		private bool m_EnableRemoveIcon = true;

		// Token: 0x020007FB RID: 2043
		public enum eMode
		{
			// Token: 0x040030AC RID: 12460
			View,
			// Token: 0x040030AD RID: 12461
			PartyEdit,
			// Token: 0x040030AE RID: 12462
			SupportEdit,
			// Token: 0x040030AF RID: 12463
			Upgrade,
			// Token: 0x040030B0 RID: 12464
			LimitBreak,
			// Token: 0x040030B1 RID: 12465
			Evolution,
			// Token: 0x040030B2 RID: 12466
			Room
		}
	}
}
