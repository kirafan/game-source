﻿using System;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.CharaList
{
	// Token: 0x02000801 RID: 2049
	public class CharaListWindow : UIGroup
	{
		// Token: 0x170002AE RID: 686
		// (get) Token: 0x06002A60 RID: 10848 RVA: 0x000DFB69 File Offset: 0x000DDF69
		public bool IsCanceled
		{
			get
			{
				return this.m_IsCanceled;
			}
		}

		// Token: 0x170002AF RID: 687
		// (get) Token: 0x06002A61 RID: 10849 RVA: 0x000DFB71 File Offset: 0x000DDF71
		public long SelectCharaMngID
		{
			get
			{
				return this.m_SelectCharaMngID;
			}
		}

		// Token: 0x1400005C RID: 92
		// (add) Token: 0x06002A62 RID: 10850 RVA: 0x000DFB7C File Offset: 0x000DDF7C
		// (remove) Token: 0x06002A63 RID: 10851 RVA: 0x000DFBB4 File Offset: 0x000DDFB4
		public event Action<long> OnClickChara;

		// Token: 0x1400005D RID: 93
		// (add) Token: 0x06002A64 RID: 10852 RVA: 0x000DFBEC File Offset: 0x000DDFEC
		// (remove) Token: 0x06002A65 RID: 10853 RVA: 0x000DFC24 File Offset: 0x000DE024
		public event Action<long> OnHoldChara;

		// Token: 0x1400005E RID: 94
		// (add) Token: 0x06002A66 RID: 10854 RVA: 0x000DFC5C File Offset: 0x000DE05C
		// (remove) Token: 0x06002A67 RID: 10855 RVA: 0x000DFC94 File Offset: 0x000DE094
		public event Action OnClickRemove;

		// Token: 0x1400005F RID: 95
		// (add) Token: 0x06002A68 RID: 10856 RVA: 0x000DFCCC File Offset: 0x000DE0CC
		// (remove) Token: 0x06002A69 RID: 10857 RVA: 0x000DFD04 File Offset: 0x000DE104
		public event Action<long> OnSelect;

		// Token: 0x06002A6A RID: 10858 RVA: 0x000DFD3C File Offset: 0x000DE13C
		public void Open(long[] liveCharaMngIDs, int memberIdx)
		{
			this.m_SavedBackButtonCallBack = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.GetBackButtonCallBack();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButtonCallBack));
			this.m_SceneInfo = SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.AddSceneInfoStack(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharaList);
			this.ExecuteOpen();
			this.m_Scroll.SetRoomMemberMode(liveCharaMngIDs, memberIdx);
		}

		// Token: 0x06002A6B RID: 10859 RVA: 0x000DFDA4 File Offset: 0x000DE1A4
		public override void Open()
		{
			this.m_SavedBackButtonCallBack = null;
			this.ExecuteOpen();
		}

		// Token: 0x06002A6C RID: 10860 RVA: 0x000DFDB3 File Offset: 0x000DE1B3
		public override void Close()
		{
			base.Close();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
		}

		// Token: 0x06002A6D RID: 10861 RVA: 0x000DFDCC File Offset: 0x000DE1CC
		private void ExecuteOpen()
		{
			if (!this.m_IsDonePrepare)
			{
				this.m_Scroll.OnClickIcon += this.OnClickCharaIconCallBack;
				this.m_Scroll.OnHoldIcon += this.OnHoldCharaIconCallBack;
				this.m_Scroll.OnClickRemoveIcon += this.OnClickRemoveCallBack;
				this.m_Scroll.Setup();
				this.m_IsDonePrepare = true;
			}
			this.m_Scroll.SetupList();
			this.m_Scroll.Refresh();
			this.m_IsCanceled = true;
			this.m_SelectCharaMngID = -1L;
			this.m_CharaNumText.text = this.m_Scroll.CharaNum.ToString();
			base.Open();
		}

		// Token: 0x06002A6E RID: 10862 RVA: 0x000DFE8C File Offset: 0x000DE28C
		public override void Update()
		{
			base.Update();
			CharaListWindow.eStep step = this.m_Step;
			if (step != CharaListWindow.eStep.Idle)
			{
				if (step == CharaListWindow.eStep.CharaDetail)
				{
					if (!SingletonMonoBehaviour<CharaDetailPlayer>.Inst.IsOpen())
					{
						base.SetEnableInput(true);
						this.m_Step = CharaListWindow.eStep.Idle;
					}
				}
			}
		}

		// Token: 0x06002A6F RID: 10863 RVA: 0x000DFEE0 File Offset: 0x000DE2E0
		public override void OnFinishFinalize()
		{
			base.OnFinishFinalize();
			if (this.m_SceneInfo != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.RemoveSceneInfoStack(this.m_SceneInfo);
			}
			if (this.m_SavedBackButtonCallBack != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(this.m_SavedBackButtonCallBack);
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
				{
					this.m_SavedBackButtonCallBack(true);
				}
			}
		}

		// Token: 0x06002A70 RID: 10864 RVA: 0x000DFF54 File Offset: 0x000DE354
		private void OnClickCharaIconCallBack(long charaMngID)
		{
			this.m_SelectCharaMngID = charaMngID;
			if (this.m_Scroll.Mode == CharaListScroll.eMode.View)
			{
				this.OpenCharaDetail(charaMngID);
				return;
			}
			if (this.m_Scroll.Mode == CharaListScroll.eMode.Room)
			{
				this.OnSelect.Call(charaMngID);
				return;
			}
			this.DecideChara();
		}

		// Token: 0x06002A71 RID: 10865 RVA: 0x000DFFA4 File Offset: 0x000DE3A4
		public void DecideChara()
		{
			if (this.m_SelectCharaMngID == -1L)
			{
				this.OnClickRemove.Call();
			}
			else
			{
				this.OnClickChara.Call(this.m_SelectCharaMngID);
			}
			this.m_IsCanceled = false;
			this.Close();
		}

		// Token: 0x06002A72 RID: 10866 RVA: 0x000DFFE4 File Offset: 0x000DE3E4
		private void OpenCharaDetail(long charaMngID)
		{
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.Open(charaMngID, false);
			SingletonMonoBehaviour<CharaDetailPlayer>.Inst.OnClose += this.OnCloseCharaDetail;
			base.SetEnableInput(false);
			this.m_Step = CharaListWindow.eStep.CharaDetail;
			this.OnHoldChara.Call(this.m_SelectCharaMngID);
		}

		// Token: 0x06002A73 RID: 10867 RVA: 0x000E0032 File Offset: 0x000DE432
		private void OnCloseCharaDetail()
		{
			this.m_Scroll.ForceApply();
		}

		// Token: 0x06002A74 RID: 10868 RVA: 0x000E003F File Offset: 0x000DE43F
		private void OnHoldCharaIconCallBack(long charaMngID)
		{
			this.m_SelectCharaMngID = charaMngID;
			this.OpenCharaDetail(charaMngID);
		}

		// Token: 0x06002A75 RID: 10869 RVA: 0x000E0050 File Offset: 0x000DE450
		private void OnClickRemoveCallBack()
		{
			this.m_SelectCharaMngID = -1L;
			if (this.m_Scroll.Mode == CharaListScroll.eMode.View)
			{
				this.OpenCharaDetail(this.m_SelectCharaMngID);
				return;
			}
			if (this.m_Scroll.Mode == CharaListScroll.eMode.Room)
			{
				this.OnSelect.Call(-1L);
				return;
			}
			this.DecideChara();
		}

		// Token: 0x06002A76 RID: 10870 RVA: 0x000E00A7 File Offset: 0x000DE4A7
		private void OnConfirmRoomCharaChange(int btn)
		{
			if (btn == 0)
			{
				this.m_IsCanceled = false;
				this.DecideChara();
			}
		}

		// Token: 0x06002A77 RID: 10871 RVA: 0x000E00BC File Offset: 0x000DE4BC
		public void Destory()
		{
			this.m_Scroll.Destroy();
		}

		// Token: 0x06002A78 RID: 10872 RVA: 0x000E00C9 File Offset: 0x000DE4C9
		public void OnClickBackButtonCallBack(bool isCallFromShortCut)
		{
			this.Close();
		}

		// Token: 0x06002A79 RID: 10873 RVA: 0x000E00D1 File Offset: 0x000DE4D1
		public void OpenFilterWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Filter, new FilterUIArg(UISettings.eUsingType_Filter.CharaListView, new Action(this.OnExecuteFilter)), null, null);
		}

		// Token: 0x06002A7A RID: 10874 RVA: 0x000E00F8 File Offset: 0x000DE4F8
		public void OnExecuteFilter()
		{
			this.m_Scroll.SetupList();
			this.m_Scroll.Refresh();
			this.m_CharaNumText.text = this.m_Scroll.CharaNum.ToString();
		}

		// Token: 0x06002A7B RID: 10875 RVA: 0x000E013F File Offset: 0x000DE53F
		public void OpenSortWindow()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.OverlayUIMng.Open(OverlayUIPlayerManager.eOverlaySceneID.Sort, new SortUIArg(UISettings.eUsingType_Sort.CharaListView, new Action(this.OnExecuteSort)), null, null);
		}

		// Token: 0x06002A7C RID: 10876 RVA: 0x000E0165 File Offset: 0x000DE565
		public void OnExecuteSort()
		{
			this.m_Scroll.SetupList();
			this.m_Scroll.Refresh();
		}

		// Token: 0x040030CC RID: 12492
		[SerializeField]
		private CharaListScroll m_Scroll;

		// Token: 0x040030CD RID: 12493
		[SerializeField]
		private Text m_CharaNumText;

		// Token: 0x040030CE RID: 12494
		private long m_SelectCharaMngID = -1L;

		// Token: 0x040030CF RID: 12495
		private bool m_IsDonePrepare;

		// Token: 0x040030D0 RID: 12496
		private bool m_IsCanceled = true;

		// Token: 0x040030D1 RID: 12497
		private Action<bool> m_SavedBackButtonCallBack;

		// Token: 0x040030D2 RID: 12498
		private CharaListWindow.eStep m_Step;

		// Token: 0x040030D3 RID: 12499
		private GlobalUI.SceneInfoStack m_SceneInfo;

		// Token: 0x02000802 RID: 2050
		private enum eStep
		{
			// Token: 0x040030D9 RID: 12505
			Idle,
			// Token: 0x040030DA RID: 12506
			CharaDetail
		}
	}
}
