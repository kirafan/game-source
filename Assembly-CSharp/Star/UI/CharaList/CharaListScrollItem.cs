﻿using System;
using UnityEngine;

namespace Star.UI.CharaList
{
	// Token: 0x020007FC RID: 2044
	public class CharaListScrollItem : ScrollItemIcon
	{
		// Token: 0x06002A59 RID: 10841 RVA: 0x000DF928 File Offset: 0x000DDD28
		protected override void ApplyData()
		{
			this.m_CharaScroll = (CharaListScroll)this.m_Scroll;
			this.m_CharaData = (CharaListScrollItemData)this.m_Data;
			CharacterParam param = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserCharaDatas[this.m_CharaData.m_CharaIdx].Param;
			this.m_CharaIcon = this.m_CharaIconClone.GetInst<CharaIconWithFrame>();
			this.m_CharaIcon.ApplyCharaMngID(param.MngID);
			this.m_CharaIcon.ChangeDetailTextToCharaListSort();
			if (this.m_IsPartyCharaObj.activeSelf != (this.m_CharaData.m_MemberIdx != -1))
			{
				this.m_IsPartyCharaObj.SetActive(this.m_CharaData.m_MemberIdx != -1);
			}
			if (this.m_IsScheduleRoomInCharaObj.activeSelf != (this.m_CharaData.m_RoomInIdx != -1))
			{
				this.m_IsScheduleRoomInCharaObj.SetActive(this.m_CharaData.m_RoomInIdx != -1);
			}
			if (this.m_CharaData.m_MemberIdx != -1 || this.m_CharaData.m_RoomInIdx != -1)
			{
				this.m_CharaIcon.SetNew(false);
			}
			if (this.m_CharaData.m_IsEnable)
			{
				this.m_IsDark = false;
				this.m_IsEnableClick = true;
				this.m_IsEnableHold = true;
			}
			else
			{
				this.m_IsDark = true;
				this.m_IsEnableClick = false;
				this.m_IsEnableHold = true;
			}
			this.m_CharaIconButton.Interactable = !this.m_IsDark;
			if (this.m_CharaData.m_CantSelect)
			{
				this.m_ColorGroup.ChangeColor(UIDefine.CANT_COLOR, ColorGroup.eColorBlendMode.Mul, 1f);
				this.m_IsCantSelect = true;
			}
			else if (!this.m_CharaData.m_CantSelect)
			{
				this.m_ColorGroup.RevertColor();
				this.m_IsCantSelect = false;
			}
		}

		// Token: 0x06002A5A RID: 10842 RVA: 0x000DFAF8 File Offset: 0x000DDEF8
		public override void Destroy()
		{
			if (this.m_CharaIcon != null)
			{
				this.m_CharaIcon.Destroy();
			}
		}

		// Token: 0x040030B3 RID: 12467
		[SerializeField]
		private CustomButton m_CharaIconButton;

		// Token: 0x040030B4 RID: 12468
		[SerializeField]
		private PrefabCloner m_CharaIconClone;

		// Token: 0x040030B5 RID: 12469
		private CharaIconWithFrame m_CharaIcon;

		// Token: 0x040030B6 RID: 12470
		[SerializeField]
		private ColorGroup m_ColorGroup;

		// Token: 0x040030B7 RID: 12471
		[SerializeField]
		private GameObject m_IsPartyCharaObj;

		// Token: 0x040030B8 RID: 12472
		[SerializeField]
		private GameObject m_IsScheduleRoomInCharaObj;

		// Token: 0x040030B9 RID: 12473
		protected CharaListScroll m_CharaScroll;

		// Token: 0x040030BA RID: 12474
		protected CharaListScrollItemData m_CharaData;

		// Token: 0x040030BB RID: 12475
		private bool m_IsCantSelect;

		// Token: 0x020007FD RID: 2045
		public enum eCharaListScrollItemState
		{
			// Token: 0x040030BD RID: 12477
			Normal,
			// Token: 0x040030BE RID: 12478
			Disactive
		}
	}
}
