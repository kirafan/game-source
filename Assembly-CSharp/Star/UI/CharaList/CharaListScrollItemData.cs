﻿using System;

namespace Star.UI.CharaList
{
	// Token: 0x020007FE RID: 2046
	public class CharaListScrollItemData : ScrollItemData
	{
		// Token: 0x06002A5B RID: 10843 RVA: 0x000DFB16 File Offset: 0x000DDF16
		public CharaListScrollItemData()
		{
			this.m_CharaIdx = -1;
			this.m_CantSelect = false;
			this.m_State = CharaListScrollItemData.eCharaListScrollItemDataState.Normal;
		}

		// Token: 0x040030BF RID: 12479
		public int m_CharaIdx;

		// Token: 0x040030C0 RID: 12480
		public int m_SortedIdx;

		// Token: 0x040030C1 RID: 12481
		public bool m_IsEnable;

		// Token: 0x040030C2 RID: 12482
		public int m_MemberIdx;

		// Token: 0x040030C3 RID: 12483
		public int m_RoomInIdx = -1;

		// Token: 0x040030C4 RID: 12484
		public bool m_CantSelect;

		// Token: 0x040030C5 RID: 12485
		public CharaListScrollItemData.eCharaListScrollItemDataState m_State;

		// Token: 0x020007FF RID: 2047
		public enum eCharaListScrollItemDataState
		{
			// Token: 0x040030C7 RID: 12487
			Normal,
			// Token: 0x040030C8 RID: 12488
			Disactive,
			// Token: 0x040030C9 RID: 12489
			NotEnough
		}
	}
}
