﻿using System;

namespace Star.UI.CharaList
{
	// Token: 0x02000800 RID: 2048
	public class CharaListScrollRemoveItem : ScrollItemIcon
	{
		// Token: 0x06002A5D RID: 10845 RVA: 0x000DFB42 File Offset: 0x000DDF42
		protected override void Awake()
		{
			base.Awake();
		}

		// Token: 0x06002A5E RID: 10846 RVA: 0x000DFB4A File Offset: 0x000DDF4A
		protected override void ApplyData()
		{
			base.ApplyData();
		}

		// Token: 0x040030CA RID: 12490
		protected CharaListScroll m_CharaScroll;

		// Token: 0x040030CB RID: 12491
		protected CharaListScrollItemData m_CharaData;
	}
}
