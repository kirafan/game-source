﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007EA RID: 2026
	public class ExpGaugeWrapper : MonoBehaviour
	{
		// Token: 0x060029D1 RID: 10705 RVA: 0x000DCEEA File Offset: 0x000DB2EA
		public void Setup()
		{
		}

		// Token: 0x060029D2 RID: 10706 RVA: 0x000DCEEC File Offset: 0x000DB2EC
		private void Update()
		{
			if (this.m_Source != null && this.m_Destination != null)
			{
				this.m_Destination.text = this.m_Source.text;
			}
		}

		// Token: 0x060029D3 RID: 10707 RVA: 0x000DCF26 File Offset: 0x000DB326
		public void SetDestination(Text destination)
		{
			this.m_Destination = destination;
		}

		// Token: 0x060029D4 RID: 10708 RVA: 0x000DCF2F File Offset: 0x000DB32F
		public void SetExpData(int level, long nowexp, int maxLevel, long needExp)
		{
			this.m_Gauge.SetExpData(level, nowexp, maxLevel, needExp);
		}

		// Token: 0x060029D5 RID: 10709 RVA: 0x000DCF41 File Offset: 0x000DB341
		public void SetExpData(long rewardExp, int beforeLevel, long beforeExp, int afterLevel, long afterExp, int maxLevel, long[] needExp)
		{
			this.m_Gauge.SetExpData(rewardExp, beforeLevel, beforeExp, afterLevel, afterExp, maxLevel, needExp);
		}

		// Token: 0x060029D6 RID: 10710 RVA: 0x000DCF59 File Offset: 0x000DB359
		public void PlayExpGauge()
		{
			this.m_Gauge.Play(true);
		}

		// Token: 0x060029D7 RID: 10711 RVA: 0x000DCF67 File Offset: 0x000DB367
		public bool IsPlayingExpGauge()
		{
			return this.m_Gauge.IsPlaying;
		}

		// Token: 0x060029D8 RID: 10712 RVA: 0x000DCF74 File Offset: 0x000DB374
		public void SkipExpGaugePlaying()
		{
			this.m_Gauge.SkipUpdateExp();
		}

		// Token: 0x04003040 RID: 12352
		[SerializeField]
		private ExpGauge m_Gauge;

		// Token: 0x04003041 RID: 12353
		[SerializeField]
		private Text m_Source;

		// Token: 0x04003042 RID: 12354
		[SerializeField]
		private Text m_Destination;
	}
}
