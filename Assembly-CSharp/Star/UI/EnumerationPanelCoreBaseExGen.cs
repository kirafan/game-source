﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200096A RID: 2410
	public abstract class EnumerationPanelCoreBaseExGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreBase where ParentType : MonoBehaviour where CharaPanelType : MonoBehaviour where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreBase.SharedInstanceBaseExGen<SetupArgumentType, ParentType, CharaPanelType>
	{
		// Token: 0x060031D3 RID: 12755 RVA: 0x000FD657 File Offset: 0x000FBA57
		public virtual void Setup(SetupArgumentType argument)
		{
			this.SetSharedIntance(argument);
		}

		// Token: 0x060031D4 RID: 12756
		public abstract void Destroy();

		// Token: 0x060031D5 RID: 12757 RVA: 0x000FD660 File Offset: 0x000FBA60
		public virtual void Update()
		{
		}

		// Token: 0x060031D6 RID: 12758
		protected abstract CharaPanelType InstantiateProcess();

		// Token: 0x060031D7 RID: 12759 RVA: 0x000FD662 File Offset: 0x000FBA62
		protected int HowManyAllChildren()
		{
			if (this.m_PartyData == null)
			{
				return -1;
			}
			return this.m_PartyData.m_Party.HowManyPartyMemberAll();
		}

		// Token: 0x060031D8 RID: 12760 RVA: 0x000FD68B File Offset: 0x000FBA8B
		protected int HowManyEnableChildren()
		{
			if (this.m_PartyData == null)
			{
				return -1;
			}
			return this.m_PartyData.m_Party.HowManyPartyMemberEnable();
		}

		// Token: 0x060031D9 RID: 12761 RVA: 0x000FD6B4 File Offset: 0x000FBAB4
		public int GetNowSelectedCharaSlotIndex()
		{
			return this.m_PartyData.m_NowSelectedCharaSlotIndex;
		}

		// Token: 0x060031DA RID: 12762 RVA: 0x000FD6C6 File Offset: 0x000FBAC6
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
			this.m_Mode = mode;
			this.GetSharedIntance().parent.GetComponent<CanvasGroup>().blocksRaycasts = EnumerationPanelCoreBase.ModeToTouchActive(mode);
		}

		// Token: 0x060031DB RID: 12763 RVA: 0x000FD6F5 File Offset: 0x000FBAF5
		public void SetNowSelectedCharaSlotIndex(int index)
		{
			this.m_PartyData.m_NowSelectedCharaSlotIndex = index;
		}

		// Token: 0x060031DC RID: 12764 RVA: 0x000FD708 File Offset: 0x000FBB08
		public virtual void Apply(EnumerationPanelData data)
		{
			this.SetPartyData((BasedDataType)((object)data));
		}

		// Token: 0x060031DD RID: 12765 RVA: 0x000FD718 File Offset: 0x000FBB18
		protected virtual void ApplyCharaData(int idx)
		{
			if (!this.AcquirePanelsArray())
			{
				return;
			}
			if (this.m_CharaPanels[idx] == null)
			{
				return;
			}
			BasedDataType partyData = this.GetPartyData();
			if (partyData == null)
			{
				return;
			}
			if (idx >= partyData.m_Party.HowManyPartyMemberAll())
			{
				return;
			}
			CharaPanelType charaPanel = this.m_CharaPanels[idx];
			this.ApplyProcess(charaPanel, partyData.m_Party.GetMember(idx), idx);
		}

		// Token: 0x060031DE RID: 12766
		protected abstract void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx);

		// Token: 0x060031DF RID: 12767 RVA: 0x000FD79E File Offset: 0x000FBB9E
		protected int GetInstantiatePerFrame()
		{
			return 6;
		}

		// Token: 0x060031E0 RID: 12768 RVA: 0x000FD7A1 File Offset: 0x000FBBA1
		protected RectTransform GetCharaPanelParent()
		{
			return this.GetSharedIntance().m_CharaPanelParent;
		}

		// Token: 0x060031E1 RID: 12769 RVA: 0x000FD7B3 File Offset: 0x000FBBB3
		protected BasedDataType GetPartyData()
		{
			return this.m_PartyData;
		}

		// Token: 0x060031E2 RID: 12770 RVA: 0x000FD7BC File Offset: 0x000FBBBC
		protected virtual void SetPartyData(BasedDataType data)
		{
			this.m_PartyData = data;
			if (!this.AcquireChildrenNumsInfo())
			{
				return;
			}
			for (int i = 0; i < this.HowManyAllChildren(); i++)
			{
				this.ApplyCharaData(i);
			}
		}

		// Token: 0x060031E3 RID: 12771 RVA: 0x000FD7FA File Offset: 0x000FBBFA
		protected bool AcquireChildrenNumsInfo()
		{
			return this.m_PartyData != null;
		}

		// Token: 0x060031E4 RID: 12772 RVA: 0x000FD80D File Offset: 0x000FBC0D
		protected bool AcquirePanelsArray()
		{
			if (this.m_CharaPanels != null)
			{
				return true;
			}
			if (!this.AcquireChildrenNumsInfo())
			{
				return false;
			}
			this.m_CharaPanels = new CharaPanelType[this.HowManyAllChildren()];
			return this.m_CharaPanels != null;
		}

		// Token: 0x060031E5 RID: 12773
		public abstract SetupArgumentType GetSharedIntance();

		// Token: 0x060031E6 RID: 12774
		public abstract void SetSharedIntance(SetupArgumentType arg);

		// Token: 0x04003821 RID: 14369
		private const int INSTANTIATE_PER_FRAME = 6;

		// Token: 0x04003822 RID: 14370
		protected EnumerationPanelCoreBase.eMode m_Mode;

		// Token: 0x04003823 RID: 14371
		protected BasedDataType m_PartyData;

		// Token: 0x04003824 RID: 14372
		protected CharaPanelType[] m_CharaPanels;

		// Token: 0x04003825 RID: 14373
		protected int m_CharaPanelNum;
	}
}
