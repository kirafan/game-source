﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000793 RID: 1939
	public class TitlePlayer : MonoBehaviour
	{
		// Token: 0x06002780 RID: 10112 RVA: 0x000D315C File Offset: 0x000D155C
		private void Update()
		{
			TitlePlayer.eMode mode = this.m_Mode;
			if (mode != TitlePlayer.eMode.None)
			{
				if (mode == TitlePlayer.eMode.Playing)
				{
					if (!this.m_Anim.isPlaying)
					{
						if (this.m_RenderOffOnPlayEnd)
						{
							this.SetEnableRender(false);
						}
						this.m_Mode = TitlePlayer.eMode.None;
					}
				}
			}
		}

		// Token: 0x06002781 RID: 10113 RVA: 0x000D31B5 File Offset: 0x000D15B5
		public bool IsPlaying()
		{
			return this.m_Mode == TitlePlayer.eMode.Playing;
		}

		// Token: 0x06002782 RID: 10114 RVA: 0x000D31C0 File Offset: 0x000D15C0
		public void Play()
		{
			this.m_Mode = TitlePlayer.eMode.Playing;
			this.SetEnableRender(true);
			this.m_Anim.Play();
		}

		// Token: 0x06002783 RID: 10115 RVA: 0x000D31DC File Offset: 0x000D15DC
		public void SetEnableRender(bool flg)
		{
			if (this.m_GameObject == null)
			{
				this.m_GameObject = base.gameObject;
			}
			this.m_GameObject.SetActive(flg);
		}

		// Token: 0x04002E78 RID: 11896
		[SerializeField]
		private Animation m_Anim;

		// Token: 0x04002E79 RID: 11897
		[SerializeField]
		private bool m_RenderOffOnPlayEnd;

		// Token: 0x04002E7A RID: 11898
		private TitlePlayer.eMode m_Mode = TitlePlayer.eMode.None;

		// Token: 0x04002E7B RID: 11899
		private GameObject m_GameObject;

		// Token: 0x02000794 RID: 1940
		private enum eMode
		{
			// Token: 0x04002E7D RID: 11901
			None = -1,
			// Token: 0x04002E7E RID: 11902
			Playing
		}
	}
}
