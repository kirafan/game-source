﻿using System;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x0200096D RID: 2413
	public abstract class EnumerationPanelCoreGen<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType> where ParentType : EnumerationPanelBase where CharaPanelType : EnumerationCharaPanelAdapterExGen<ParentType> where BasedDataType : EnumerationPanelData where SetupArgumentType : EnumerationPanelCoreGenBase<ParentType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>
	{
		// Token: 0x060031F1 RID: 12785 RVA: 0x000FD988 File Offset: 0x000FBD88
		protected override CharaPanelType InstantiateProcess()
		{
			if (this.GetSharedIntance().m_CharaPanelPrefab == null)
			{
				return (CharaPanelType)((object)null);
			}
			RectTransform charaPanelParent = base.GetCharaPanelParent();
			if (charaPanelParent == null && charaPanelParent == null)
			{
				this.GetSharedIntance().m_CharaPanelParent = (this.GetSharedIntance().parent.transform as RectTransform);
				charaPanelParent = base.GetCharaPanelParent();
			}
			CharaPanelType result = UnityEngine.Object.Instantiate<CharaPanelType>(this.GetSharedIntance().m_CharaPanelPrefab, base.GetCharaPanelParent(), false);
			result.Setup(this.GetSharedIntance().parent);
			result.SetMode(EnumerationPanelCoreBase.ConvertEnumerationPanelCoreBaseeModeToEnumerationCharaPanelBaseeMode(this.m_Mode));
			return result;
		}

		// Token: 0x060031F2 RID: 12786 RVA: 0x000FDA65 File Offset: 0x000FBE65
		protected override void ApplyProcess(CharaPanelType charaPanel, EquipWeaponPartyMemberController partyMemberController, int idx)
		{
			if (charaPanel == null)
			{
				return;
			}
			charaPanel.Apply(partyMemberController);
		}
	}
}
