﻿using System;

namespace Star.UI
{
	// Token: 0x02000964 RID: 2404
	public class EnumerationCharaPanelScrollItemIcon : EnumerationCharaPanelBaseExGen<EnumerationCharaPanelScrollItemIconAdapterBase, EnumerationCharaPanelScrollItemIcon.SharedInstanceEx>
	{
		// Token: 0x060031C5 RID: 12741 RVA: 0x000FF90F File Offset: 0x000FDD0F
		public void Apply(EquipWeaponPartyMemberController partyMemberController)
		{
			this.ApplyProcess(partyMemberController);
		}

		// Token: 0x02000965 RID: 2405
		[Serializable]
		public class SharedInstanceEx : EnumerationCharaPanelBase.SharedInstanceExGen<EnumerationCharaPanelScrollItemIconAdapterBase>
		{
			// Token: 0x060031C6 RID: 12742 RVA: 0x000FF918 File Offset: 0x000FDD18
			public SharedInstanceEx()
			{
			}

			// Token: 0x060031C7 RID: 12743 RVA: 0x000FF920 File Offset: 0x000FDD20
			public SharedInstanceEx(EnumerationCharaPanelBase.SharedInstance argument) : base(argument)
			{
			}
		}
	}
}
