﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000A37 RID: 2615
	public class OverlayUIPlayerManager : MonoBehaviour
	{
		// Token: 0x06003668 RID: 13928 RVA: 0x001127A8 File Offset: 0x00110BA8
		public void Open(OverlayUIPlayerManager.eOverlaySceneID id, OverlayUIArgBase arg, Action OnClose = null, Action OnEnd = null)
		{
			OverlayUIPlayerBase overlayUIPlayerBase = null;
			if (id != OverlayUIPlayerManager.eOverlaySceneID.Filter)
			{
				if (id != OverlayUIPlayerManager.eOverlaySceneID.Sort)
				{
					if (id == OverlayUIPlayerManager.eOverlaySceneID.Info)
					{
						overlayUIPlayerBase = new OverlayInfoPlayer();
					}
				}
				else
				{
					overlayUIPlayerBase = new OverlaySortPlayer();
				}
			}
			else
			{
				overlayUIPlayerBase = new OverlayFilterPlayer();
			}
			overlayUIPlayerBase.Open(arg, OnClose, OnEnd);
			if (overlayUIPlayerBase != null)
			{
				this.m_PlayerList.Add(new OverlayUIPlayerManager.PlayerInfo(id, overlayUIPlayerBase));
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, true);
				this.m_IsLoading = true;
			}
		}

		// Token: 0x06003669 RID: 13929 RVA: 0x00112830 File Offset: 0x00110C30
		public bool IsOpen(OverlayUIPlayerManager.eOverlaySceneID id)
		{
			for (int i = 0; i < this.m_PlayerList.Count; i++)
			{
				if (this.m_PlayerList[i].m_ID == id)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600366A RID: 13930 RVA: 0x00112873 File Offset: 0x00110C73
		public void Clear()
		{
			this.m_PlayerList.Clear();
		}

		// Token: 0x0600366B RID: 13931 RVA: 0x00112880 File Offset: 0x00110C80
		public T GetUI<T>(OverlayUIPlayerManager.eOverlaySceneID id) where T : OverlayUIPlayerBase
		{
			for (int i = 0; i < this.m_PlayerList.Count; i++)
			{
				if (this.m_PlayerList[i].m_ID == id)
				{
					return (T)((object)this.m_PlayerList[i].m_Player);
				}
			}
			return (T)((object)null);
		}

		// Token: 0x0600366C RID: 13932 RVA: 0x001128E0 File Offset: 0x00110CE0
		private void Update()
		{
			if (this.m_IsLoading)
			{
				this.m_IsLoading = false;
				for (int i = 0; i < this.m_PlayerList.Count; i++)
				{
					if (!this.m_PlayerList[i].m_Player.IsDoneLoad())
					{
						this.m_IsLoading = true;
					}
				}
				if (!this.m_IsLoading)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(base.gameObject, false);
				}
			}
			List<OverlayUIPlayerManager.PlayerInfo> list = new List<OverlayUIPlayerManager.PlayerInfo>();
			for (int j = 0; j < this.m_PlayerList.Count; j++)
			{
				this.m_PlayerList[j].m_Player.UpdateStep();
				if (!this.m_PlayerList[j].m_Player.IsOpen())
				{
					list.Add(this.m_PlayerList[j]);
					break;
				}
			}
			for (int k = 0; k < list.Count; k++)
			{
				this.m_PlayerList.Remove(list[k]);
			}
		}

		// Token: 0x04003CBA RID: 15546
		private List<OverlayUIPlayerManager.PlayerInfo> m_PlayerList = new List<OverlayUIPlayerManager.PlayerInfo>();

		// Token: 0x04003CBB RID: 15547
		private bool m_IsLoading;

		// Token: 0x02000A38 RID: 2616
		public enum eOverlaySceneID
		{
			// Token: 0x04003CBD RID: 15549
			Filter,
			// Token: 0x04003CBE RID: 15550
			Sort,
			// Token: 0x04003CBF RID: 15551
			Info,
			// Token: 0x04003CC0 RID: 15552
			Num
		}

		// Token: 0x02000A39 RID: 2617
		public class PlayerInfo
		{
			// Token: 0x0600366D RID: 13933 RVA: 0x001129F2 File Offset: 0x00110DF2
			public PlayerInfo(OverlayUIPlayerManager.eOverlaySceneID id, OverlayUIPlayerBase player)
			{
				this.m_ID = id;
				this.m_Player = player;
			}

			// Token: 0x04003CC1 RID: 15553
			public OverlayUIPlayerBase m_Player;

			// Token: 0x04003CC2 RID: 15554
			public OverlayUIPlayerManager.eOverlaySceneID m_ID;
		}
	}
}
