﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020007F7 RID: 2039
	public class BlinkColor : MonoBehaviour
	{
		// Token: 0x06002A2F RID: 10799 RVA: 0x000DE85D File Offset: 0x000DCC5D
		public void Start()
		{
			this.m_IsSaved = true;
			this.m_Color = this.m_TargetGraphic.color;
		}

		// Token: 0x06002A30 RID: 10800 RVA: 0x000DE878 File Offset: 0x000DCC78
		private void Update()
		{
			if (this.m_Sync)
			{
				this.m_TimeCount = Time.timeSinceLevelLoad;
			}
			else
			{
				this.m_TimeCount += Time.deltaTime;
			}
			float num = 0.5f + Mathf.Sin(this.m_TimeCount * this.m_Speed) * 0.5f;
			Color color = default(Color);
			color.r = this.m_Color.r * (1f - num) + this.m_BlinkColor.r * num;
			color.g = this.m_Color.g * (1f - num) + this.m_BlinkColor.g * num;
			color.b = this.m_Color.b * (1f - num) + this.m_BlinkColor.b * num;
			color.a = this.m_Color.a * (1f - num) + this.m_BlinkColor.a * num;
			this.m_TargetGraphic.color = color;
		}

		// Token: 0x06002A31 RID: 10801 RVA: 0x000DE985 File Offset: 0x000DCD85
		public void Reset()
		{
			if (this.m_IsSaved)
			{
				this.m_TargetGraphic.color = this.m_Color;
			}
		}

		// Token: 0x04003090 RID: 12432
		[SerializeField]
		private Graphic m_TargetGraphic;

		// Token: 0x04003091 RID: 12433
		[SerializeField]
		private Color m_BlinkColor = Color.white;

		// Token: 0x04003092 RID: 12434
		[SerializeField]
		private float m_Speed = 1f;

		// Token: 0x04003093 RID: 12435
		[SerializeField]
		private bool m_Sync = true;

		// Token: 0x04003094 RID: 12436
		private bool m_IsSaved;

		// Token: 0x04003095 RID: 12437
		private Color m_Color;

		// Token: 0x04003096 RID: 12438
		private float m_TimeCount;
	}
}
