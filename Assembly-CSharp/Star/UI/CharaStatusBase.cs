﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200080D RID: 2061
	public class CharaStatusBase : MonoBehaviour
	{
		// Token: 0x06002AA1 RID: 10913 RVA: 0x000E168E File Offset: 0x000DFA8E
		protected void Apply(string[] values)
		{
			this.Apply(this.m_ValueText, values);
		}

		// Token: 0x06002AA2 RID: 10914 RVA: 0x000E16A0 File Offset: 0x000DFAA0
		protected void Apply(Text[] targets, string[] values)
		{
			if (values == null || values.Length != 6)
			{
				if (values != null)
				{
					for (int i = 0; i < values.Length; i++)
					{
						if (targets[i] != null)
						{
							targets[i].gameObject.SetActive(false);
						}
					}
				}
				return;
			}
			for (int j = 0; j < 6; j++)
			{
				if (targets[j] != null)
				{
					string text = values[j];
					if (text != null)
					{
						targets[j].gameObject.SetActive(true);
						targets[j].text = text;
					}
					else
					{
						targets[j].gameObject.SetActive(false);
					}
				}
			}
		}

		// Token: 0x0400310D RID: 12557
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x0200080E RID: 2062
		public enum eStatus
		{
			// Token: 0x0400310F RID: 12559
			Hp,
			// Token: 0x04003110 RID: 12560
			Atk,
			// Token: 0x04003111 RID: 12561
			Mgc,
			// Token: 0x04003112 RID: 12562
			Def,
			// Token: 0x04003113 RID: 12563
			MDef,
			// Token: 0x04003114 RID: 12564
			Spd,
			// Token: 0x04003115 RID: 12565
			Num
		}
	}
}
