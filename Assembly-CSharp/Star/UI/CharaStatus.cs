﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200080B RID: 2059
	public class CharaStatus : MonoBehaviour
	{
		// Token: 0x06002A9F RID: 10911 RVA: 0x000E1530 File Offset: 0x000DF930
		public void Apply(int[] values, int[] addValues = null)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < 6; i++)
			{
				if (this.m_ValueText[i] != null)
				{
					this.m_ValueText[i].text = values[i].ToString();
				}
			}
			if (addValues != null)
			{
				for (int j = 0; j < 6; j++)
				{
					if (this.m_AddValueText[j] != null)
					{
						this.m_AddValueText[j].gameObject.SetActive(true);
						stringBuilder.Length = 0;
						if (addValues[j] > 0)
						{
							stringBuilder.Append("<color=#00c882>");
						}
						else if (addValues[j] < 0)
						{
							stringBuilder.Append("<color=#ff4141>");
						}
						stringBuilder.Append(values[j] + addValues[j]);
						if (addValues[j] != 0)
						{
							stringBuilder.Append("</color>");
						}
						this.m_AddValueText[j].text = stringBuilder.ToString();
					}
				}
			}
			else if (this.m_AddValueText != null && this.m_AddValueText.Length > 0)
			{
				for (int k = 0; k < 6; k++)
				{
					if (this.m_AddValueText[k] != null)
					{
						this.m_AddValueText[k].gameObject.SetActive(false);
					}
				}
			}
		}

		// Token: 0x04003103 RID: 12547
		[SerializeField]
		private Text[] m_ValueText;

		// Token: 0x04003104 RID: 12548
		[SerializeField]
		[Tooltip("デザイン次第で消す")]
		private Text[] m_AddValueText;

		// Token: 0x0200080C RID: 2060
		public enum eStatus
		{
			// Token: 0x04003106 RID: 12550
			Hp,
			// Token: 0x04003107 RID: 12551
			Atk,
			// Token: 0x04003108 RID: 12552
			Mgc,
			// Token: 0x04003109 RID: 12553
			Def,
			// Token: 0x0400310A RID: 12554
			MDef,
			// Token: 0x0400310B RID: 12555
			Spd,
			// Token: 0x0400310C RID: 12556
			Num
		}
	}
}
