﻿using System;

namespace Star.UI
{
	// Token: 0x020008B8 RID: 2232
	public enum eAxis
	{
		// Token: 0x04003570 RID: 13680
		Error,
		// Token: 0x04003571 RID: 13681
		Vertical,
		// Token: 0x04003572 RID: 13682
		Horizontal
	}
}
