﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x02000891 RID: 2193
	public class DataInheritenceWindowContent : MonoBehaviour
	{
		// Token: 0x06002D86 RID: 11654 RVA: 0x000F0534 File Offset: 0x000EE934
		public void Setup()
		{
			if (this.m_IdInputCloner != null)
			{
				this.m_IdInputCloner.Setup(true);
				this.m_IdInput = this.m_IdInputCloner.GetInst<InputFieldController>();
			}
			if (this.m_IdInput != null)
			{
				this.m_IdInput.SetTitleSprite(this.m_IdInputTitleSprite);
				this.m_IdInput.SetInputFieldActive(false);
			}
			if (this.m_PassInputCloner != null)
			{
				this.m_PassInputCloner.Setup(true);
				this.m_PassInput = this.m_PassInputCloner.GetInst<InputFieldController>();
			}
			if (this.m_PassInput != null)
			{
				this.m_PassInput.SetTitleSprite(this.m_PassInputTitleSprite);
				this.m_PassInput.SetInputFieldActive(false);
			}
			this.SetDeadlineString(null);
			if (this.m_Message != null)
			{
				this.m_Message.text = string.Empty;
			}
			this.SetButtonPrefabClonerAdapterIntsActive(this.m_ExecuteWindowButtons, false);
			this.SetButtonPrefabClonerAdapterIntsActive(this.m_ConfigurationWindowButtons, false);
		}

		// Token: 0x06002D87 RID: 11655 RVA: 0x000F063C File Offset: 0x000EEA3C
		private void Update()
		{
			if (this.m_InputWaitButtons != null)
			{
				for (int i = 0; i < this.m_InputWaitButtons.Length; i++)
				{
					if (this.m_InputWaitButtons[i] != null && this.m_InputWaitButtons[i].GetInst<CustomButton>() != null)
					{
						this.m_InputWaitButtons[i].GetInst<CustomButton>().Interactable = (!string.IsNullOrEmpty(this.m_IdInput.GetString()) && !string.IsNullOrEmpty(this.m_PassInput.GetString()));
					}
				}
			}
		}

		// Token: 0x06002D88 RID: 11656 RVA: 0x000F06D5 File Offset: 0x000EEAD5
		public string GetIdString()
		{
			if (this.m_IdInput != null)
			{
				return this.m_IdInput.GetString();
			}
			return null;
		}

		// Token: 0x06002D89 RID: 11657 RVA: 0x000F06F5 File Offset: 0x000EEAF5
		public string GetPasswordString()
		{
			if (this.m_PassInput != null)
			{
				return this.m_PassInput.GetString();
			}
			return null;
		}

		// Token: 0x06002D8A RID: 11658 RVA: 0x000F0718 File Offset: 0x000EEB18
		public void SetWindowType(DataInheritenceWindowContent.eType windowType)
		{
			string text = string.Empty;
			if (windowType == DataInheritenceWindowContent.eType.ExecuteWindow)
			{
				if (this.m_IdInput != null)
				{
					this.m_IdInput.SetInputFieldActive(true);
				}
				if (this.m_PassInput != null)
				{
					this.m_PassInput.SetInputFieldActive(true);
				}
				text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.TitleTitleMainInheritanceMessage);
				this.SetupButtonPrefabClonerAdapterIntsActive(this.m_ExecuteWindowButtons, true);
				this.SetButtonPrefabClonerAdapterIntsActive(this.m_ExecuteWindowButtons, true);
				this.SetButtonPrefabClonerAdapterIntsActive(this.m_ConfigurationWindowButtons, false);
			}
			else
			{
				text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDescript);
				this.SetupButtonPrefabClonerAdapterIntsActive(this.m_ConfigurationWindowButtons, true);
				this.SetButtonPrefabClonerAdapterIntsActive(this.m_ExecuteWindowButtons, false);
				this.SetButtonPrefabClonerAdapterIntsActive(this.m_ConfigurationWindowButtons, true);
			}
			if (this.m_Message != null)
			{
				this.m_Message.text = text;
			}
		}

		// Token: 0x06002D8B RID: 11659 RVA: 0x000F0805 File Offset: 0x000EEC05
		public void SetIdString(string id)
		{
			if (this.m_IdInput != null)
			{
				this.m_IdInput.SetInputFieldString(id);
			}
		}

		// Token: 0x06002D8C RID: 11660 RVA: 0x000F0824 File Offset: 0x000EEC24
		public void SetPasswordString(string password)
		{
			if (this.m_PassInput != null)
			{
				this.m_PassInput.SetInputFieldString(password);
			}
		}

		// Token: 0x06002D8D RID: 11661 RVA: 0x000F0844 File Offset: 0x000EEC44
		public void SetDeadlineString(string deadline)
		{
			if (this.m_DeadlineText != null)
			{
				if (deadline != null)
				{
					this.m_Deadline.SetActive(true);
					this.m_DeadlineText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.MenuPlayerMoveConfigureDeadline) + " " + deadline;
				}
				else
				{
					this.m_Deadline.SetActive(false);
				}
			}
		}

		// Token: 0x06002D8E RID: 11662 RVA: 0x000F08B0 File Offset: 0x000EECB0
		protected void SetupButtonPrefabClonerAdapterIntsActive(ButtonPrefabClonerAdapterInt[] buttonPrefabClonerAdapterInts, bool active)
		{
			if (buttonPrefabClonerAdapterInts == null)
			{
				return;
			}
			for (int i = 0; i < buttonPrefabClonerAdapterInts.Length; i++)
			{
				if (buttonPrefabClonerAdapterInts[i] != null)
				{
					buttonPrefabClonerAdapterInts[i].Setup(true);
					buttonPrefabClonerAdapterInts[i].SetOnClickButtonCallback(new Action<int>(this.OnClickButton));
				}
			}
		}

		// Token: 0x06002D8F RID: 11663 RVA: 0x000F0904 File Offset: 0x000EED04
		protected void SetButtonPrefabClonerAdapterIntsActive(ButtonPrefabClonerAdapterInt[] buttonPrefabClonerAdapterInts, bool active)
		{
			if (buttonPrefabClonerAdapterInts == null)
			{
				return;
			}
			for (int i = 0; i < buttonPrefabClonerAdapterInts.Length; i++)
			{
				if (buttonPrefabClonerAdapterInts[i] != null && buttonPrefabClonerAdapterInts[i].gameObject != null)
				{
					buttonPrefabClonerAdapterInts[i].gameObject.SetActive(active);
				}
			}
		}

		// Token: 0x06002D90 RID: 11664 RVA: 0x000F095B File Offset: 0x000EED5B
		public void SetOnClickButtonCallback(Action<int> callback)
		{
			this.m_OnClickCallback = callback;
		}

		// Token: 0x06002D91 RID: 11665 RVA: 0x000F0964 File Offset: 0x000EED64
		public void OnClickButton(int index)
		{
			if (this.m_OnClickCallback != null)
			{
				this.m_OnClickCallback(index);
			}
		}

		// Token: 0x04003493 RID: 13459
		[SerializeField]
		private PrefabCloner m_IdInputCloner;

		// Token: 0x04003494 RID: 13460
		[SerializeField]
		private Sprite m_IdInputTitleSprite;

		// Token: 0x04003495 RID: 13461
		private InputFieldController m_IdInput;

		// Token: 0x04003496 RID: 13462
		[SerializeField]
		private PrefabCloner m_PassInputCloner;

		// Token: 0x04003497 RID: 13463
		[SerializeField]
		private Sprite m_PassInputTitleSprite;

		// Token: 0x04003498 RID: 13464
		private InputFieldController m_PassInput;

		// Token: 0x04003499 RID: 13465
		[SerializeField]
		private GameObject m_Deadline;

		// Token: 0x0400349A RID: 13466
		[SerializeField]
		private Text m_DeadlineText;

		// Token: 0x0400349B RID: 13467
		[SerializeField]
		private Text m_Message;

		// Token: 0x0400349C RID: 13468
		[SerializeField]
		[Tooltip("引き継ぎ設定ウィンドウでのみ有効なボタン.")]
		private ButtonPrefabClonerAdapterInt[] m_ConfigurationWindowButtons;

		// Token: 0x0400349D RID: 13469
		[SerializeField]
		[Tooltip("引き継ぎ実行ウィンドウでのみ有効なボタン.")]
		private ButtonPrefabClonerAdapterInt[] m_ExecuteWindowButtons;

		// Token: 0x0400349E RID: 13470
		[SerializeField]
		[Tooltip("入力に不備があると押せなくなるボタン.")]
		private ButtonPrefabClonerAdapterInt[] m_InputWaitButtons;

		// Token: 0x0400349F RID: 13471
		private Action<int> m_OnClickCallback;

		// Token: 0x02000892 RID: 2194
		public enum eType
		{
			// Token: 0x040034A1 RID: 13473
			ExecuteWindow,
			// Token: 0x040034A2 RID: 13474
			ConfigurationWindow
		}
	}
}
