﻿using System;

namespace Star.UI
{
	// Token: 0x020007AF RID: 1967
	public class SelectedCharaInfoCharacterBattlePartyIllust : SelectedCharaInfoCharacterIllust
	{
		// Token: 0x0600289C RID: 10396 RVA: 0x000D98D3 File Offset: 0x000D7CD3
		public override void Setup()
		{
			this.m_CharacterIllustType = CharaIllust.eCharaIllustType.Edit;
			base.Setup();
		}
	}
}
