﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008AD RID: 2221
	[ExecuteInEditMode]
	public class ToggleImage : MonoBehaviour
	{
		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06002E19 RID: 11801 RVA: 0x000F2E58 File Offset: 0x000F1258
		// (set) Token: 0x06002E1A RID: 11802 RVA: 0x000F2E60 File Offset: 0x000F1260
		public bool Enable
		{
			get
			{
				return this.m_Enable;
			}
			set
			{
				this.m_Enable = value;
				if (this.m_Enable)
				{
					this.m_TargetImage.sprite = this.m_SpriteOn;
				}
				else
				{
					this.m_TargetImage.sprite = this.m_SpriteOff;
				}
			}
		}

		// Token: 0x06002E1B RID: 11803 RVA: 0x000F2E9B File Offset: 0x000F129B
		private void OnValidate()
		{
			if (this.m_Enable)
			{
				this.m_TargetImage.sprite = this.m_SpriteOn;
			}
			else
			{
				this.m_TargetImage.sprite = this.m_SpriteOff;
			}
		}

		// Token: 0x04003521 RID: 13601
		[SerializeField]
		private bool m_Enable;

		// Token: 0x04003522 RID: 13602
		[SerializeField]
		private Image m_TargetImage;

		// Token: 0x04003523 RID: 13603
		[SerializeField]
		private Sprite m_SpriteOff;

		// Token: 0x04003524 RID: 13604
		[SerializeField]
		private Sprite m_SpriteOn;
	}
}
