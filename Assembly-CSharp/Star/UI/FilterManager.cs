﻿using System;

namespace Star.UI
{
	// Token: 0x02000834 RID: 2100
	public class FilterManager
	{
		// Token: 0x06002BE2 RID: 11234 RVA: 0x000E7D08 File Offset: 0x000E6108
		public void SetupDefault()
		{
			for (int i = 0; i < 4; i++)
			{
				this.m_FilterParams[i] = new FilterManager.FilterParam();
				this.SetDefault(this.m_FilterParams[i], (FilterManager.eListType)i);
			}
		}

		// Token: 0x06002BE3 RID: 11235 RVA: 0x000E7D43 File Offset: 0x000E6143
		public void SetDefault(FilterManager.eListType type)
		{
			this.SetDefault(this.GetParam(type), type);
		}

		// Token: 0x06002BE4 RID: 11236 RVA: 0x000E7D53 File Offset: 0x000E6153
		public FilterManager.FilterParam GetParam(FilterManager.eListType type)
		{
			return this.m_FilterParams[(int)type];
		}

		// Token: 0x06002BE5 RID: 11237 RVA: 0x000E7D60 File Offset: 0x000E6160
		private void SetDefault(FilterManager.FilterParam filterParam, FilterManager.eListType type)
		{
			filterParam.m_Type = type;
			filterParam.m_TypeParams = new FilterManager.FilterParam.FilterCategoryParam[this.FILTER_CATEGORY[(int)type].Length];
			for (int i = 0; i < filterParam.m_TypeParams.Length; i++)
			{
				filterParam.m_TypeParams[i] = new FilterManager.FilterParam.FilterCategoryParam();
				filterParam.m_TypeParams[i].m_Category = this.FILTER_CATEGORY[(int)type][i];
				switch (filterParam.m_TypeParams[i].m_Category)
				{
				case FilterManager.eFilterCategory.Class:
					filterParam.m_TypeParams[i].m_Flags = new bool[5];
					break;
				case FilterManager.eFilterCategory.Rare:
					filterParam.m_TypeParams[i].m_Flags = new bool[5];
					break;
				case FilterManager.eFilterCategory.Element:
					filterParam.m_TypeParams[i].m_Flags = new bool[6];
					break;
				case FilterManager.eFilterCategory.Title:
					filterParam.m_TypeParams[i].m_Flags = new bool[new eTitleTypeConverter().Setup().HowManyTitles()];
					break;
				case FilterManager.eFilterCategory.DeadLine:
					filterParam.m_TypeParams[i].m_Flags = new bool[2];
					break;
				case FilterManager.eFilterCategory.PresentType:
					filterParam.m_TypeParams[i].m_Flags = new bool[5];
					break;
				}
				filterParam.m_TypeParams[i].m_All = true;
			}
		}

		// Token: 0x0400327C RID: 12924
		public readonly FilterManager.eFilterCategory[][] FILTER_CATEGORY = new FilterManager.eFilterCategory[][]
		{
			new FilterManager.eFilterCategory[]
			{
				FilterManager.eFilterCategory.Class,
				FilterManager.eFilterCategory.Rare,
				FilterManager.eFilterCategory.Element,
				FilterManager.eFilterCategory.Title
			},
			new FilterManager.eFilterCategory[]
			{
				FilterManager.eFilterCategory.Class,
				FilterManager.eFilterCategory.Rare
			},
			new FilterManager.eFilterCategory[]
			{
				FilterManager.eFilterCategory.Class,
				FilterManager.eFilterCategory.Rare,
				FilterManager.eFilterCategory.Element,
				FilterManager.eFilterCategory.Title
			},
			new FilterManager.eFilterCategory[]
			{
				FilterManager.eFilterCategory.DeadLine,
				FilterManager.eFilterCategory.PresentType
			}
		};

		// Token: 0x0400327D RID: 12925
		public FilterManager.FilterParam[] m_FilterParams = new FilterManager.FilterParam[4];

		// Token: 0x02000835 RID: 2101
		public enum eListType
		{
			// Token: 0x0400327F RID: 12927
			None = -1,
			// Token: 0x04003280 RID: 12928
			Chara,
			// Token: 0x04003281 RID: 12929
			Weapon,
			// Token: 0x04003282 RID: 12930
			Support,
			// Token: 0x04003283 RID: 12931
			Present,
			// Token: 0x04003284 RID: 12932
			Num
		}

		// Token: 0x02000836 RID: 2102
		public enum eFilterCategory
		{
			// Token: 0x04003286 RID: 12934
			Class,
			// Token: 0x04003287 RID: 12935
			Rare,
			// Token: 0x04003288 RID: 12936
			Element,
			// Token: 0x04003289 RID: 12937
			Title,
			// Token: 0x0400328A RID: 12938
			DeadLine,
			// Token: 0x0400328B RID: 12939
			PresentType,
			// Token: 0x0400328C RID: 12940
			Num
		}

		// Token: 0x02000837 RID: 2103
		public enum eFilterDeadLine
		{
			// Token: 0x0400328E RID: 12942
			Exist,
			// Token: 0x0400328F RID: 12943
			NoExist,
			// Token: 0x04003290 RID: 12944
			Num
		}

		// Token: 0x02000838 RID: 2104
		public enum eFilterPresentType
		{
			// Token: 0x04003292 RID: 12946
			Chara,
			// Token: 0x04003293 RID: 12947
			Weapon,
			// Token: 0x04003294 RID: 12948
			Item,
			// Token: 0x04003295 RID: 12949
			Gold,
			// Token: 0x04003296 RID: 12950
			Etc,
			// Token: 0x04003297 RID: 12951
			Num
		}

		// Token: 0x02000839 RID: 2105
		public class FilterParam
		{
			// Token: 0x06002BE7 RID: 11239 RVA: 0x000E7EAC File Offset: 0x000E62AC
			public FilterManager.FilterParam.FilterCategoryParam GetCategoryParam(FilterManager.eFilterCategory category)
			{
				for (int i = 0; i < this.m_TypeParams.Length; i++)
				{
					if (category == this.m_TypeParams[i].m_Category)
					{
						return this.m_TypeParams[i];
					}
				}
				return null;
			}

			// Token: 0x06002BE8 RID: 11240 RVA: 0x000E7EF0 File Offset: 0x000E62F0
			public bool Check(FilterManager.eFilterCategory category, int idx)
			{
				FilterManager.FilterParam.FilterCategoryParam categoryParam = this.GetCategoryParam(category);
				return categoryParam.m_All || categoryParam.m_Flags[idx];
			}

			// Token: 0x04003298 RID: 12952
			public FilterManager.eListType m_Type;

			// Token: 0x04003299 RID: 12953
			public FilterManager.FilterParam.FilterCategoryParam[] m_TypeParams;

			// Token: 0x0200083A RID: 2106
			public class FilterCategoryParam
			{
				// Token: 0x0400329A RID: 12954
				public FilterManager.eFilterCategory m_Category;

				// Token: 0x0400329B RID: 12955
				public bool m_All;

				// Token: 0x0400329C RID: 12956
				public bool[] m_Flags;
			}
		}
	}
}
