﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x0200089B RID: 2203
	public class RareStar : UIBehaviour, ILayoutGroup, ILayoutController
	{
		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06002DB6 RID: 11702 RVA: 0x000F0DE3 File Offset: 0x000EF1E3
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x06002DB7 RID: 11703 RVA: 0x000F0E08 File Offset: 0x000EF208
		public void Apply(eRare rare)
		{
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			if (this.m_Stars == null)
			{
				this.m_Stars = this.m_RectTransform.GetComponentsInChildren<Image>();
			}
			for (int i = 0; i < 5; i++)
			{
				this.m_Stars[i].gameObject.SetActive(i <= (int)rare);
			}
		}

		// Token: 0x06002DB8 RID: 11704 RVA: 0x000F0E79 File Offset: 0x000EF279
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x06002DB9 RID: 11705 RVA: 0x000F0E87 File Offset: 0x000EF287
		protected override void OnDisable()
		{
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06002DBA RID: 11706 RVA: 0x000F0E9A File Offset: 0x000EF29A
		protected override void OnRectTransformDimensionsChange()
		{
			this.SetDirty();
		}

		// Token: 0x06002DBB RID: 11707 RVA: 0x000F0EA2 File Offset: 0x000EF2A2
		protected virtual void OnTransformChildrenChanged()
		{
			this.SetDirty();
		}

		// Token: 0x06002DBC RID: 11708 RVA: 0x000F0EAA File Offset: 0x000EF2AA
		protected override void OnTransformParentChanged()
		{
			this.SetDirty();
		}

		// Token: 0x06002DBD RID: 11709 RVA: 0x000F0EB2 File Offset: 0x000EF2B2
		protected void SetDirty()
		{
			if (!this.IsActive())
			{
				return;
			}
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
		}

		// Token: 0x06002DBE RID: 11710 RVA: 0x000F0ECB File Offset: 0x000EF2CB
		public virtual void SetLayoutHorizontal()
		{
		}

		// Token: 0x06002DBF RID: 11711 RVA: 0x000F0ECD File Offset: 0x000EF2CD
		public virtual void SetLayoutVertical()
		{
			this.Rebuild();
		}

		// Token: 0x06002DC0 RID: 11712 RVA: 0x000F0ED8 File Offset: 0x000EF2D8
		private void Rebuild()
		{
			if (!this.m_FitParent)
			{
				return;
			}
			RectTransform component = this.rectTransform.parent.GetComponent<RectTransform>();
			if (component.rect.width < this.rectTransform.rect.width)
			{
				this.rectTransform.localScale = new Vector3(component.rect.width / this.rectTransform.rect.width, component.rect.width / this.rectTransform.rect.width, 1f);
			}
		}

		// Token: 0x040034BD RID: 13501
		private RectTransform m_RectTransform;

		// Token: 0x040034BE RID: 13502
		private Image[] m_Stars;

		// Token: 0x040034BF RID: 13503
		[SerializeField]
		[Tooltip("親のRectTransform内に収まるように変形する(アイコン内用)")]
		private bool m_FitParent;

		// Token: 0x040034C0 RID: 13504
		[NonSerialized]
		private RectTransform m_Rect;
	}
}
