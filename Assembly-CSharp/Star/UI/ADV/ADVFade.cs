﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000742 RID: 1858
	public class ADVFade : MonoBehaviour
	{
		// Token: 0x17000266 RID: 614
		// (get) Token: 0x060024AA RID: 9386 RVA: 0x000C597C File Offset: 0x000C3D7C
		public bool IsFade
		{
			get
			{
				return this.m_IsFade;
			}
		}

		// Token: 0x060024AB RID: 9387 RVA: 0x000C5984 File Offset: 0x000C3D84
		public void Set(Color rgba)
		{
			this.m_NowColor = rgba;
			this.m_Duration = 0f;
			this.m_IsFade = false;
			this.m_Image.color = this.m_NowColor;
		}

		// Token: 0x060024AC RID: 9388 RVA: 0x000C59B0 File Offset: 0x000C3DB0
		public void Fade(Color rgba, float time)
		{
			if (time <= 0f)
			{
				this.Set(rgba);
				return;
			}
			this.m_StartColor = this.m_NowColor;
			this.m_EndColor = rgba;
			this.m_Duration = time;
			this.m_Time = 0f;
			this.m_IsFade = true;
		}

		// Token: 0x060024AD RID: 9389 RVA: 0x000C59FC File Offset: 0x000C3DFC
		private void Update()
		{
			if (this.m_IsFade)
			{
				this.m_Time += Time.deltaTime;
				if (this.m_Time > this.m_Duration)
				{
					this.m_Time = this.m_Duration;
					this.m_IsFade = false;
				}
				this.m_NowColor = Color.Lerp(this.m_StartColor, this.m_EndColor, this.m_Time / this.m_Duration);
				this.m_Image.color = this.m_NowColor;
			}
		}

		// Token: 0x04002B91 RID: 11153
		[SerializeField]
		private Image m_Image;

		// Token: 0x04002B92 RID: 11154
		private Color m_NowColor = Color.black;

		// Token: 0x04002B93 RID: 11155
		private bool m_IsFade;

		// Token: 0x04002B94 RID: 11156
		private Color m_StartColor = Color.black;

		// Token: 0x04002B95 RID: 11157
		private Color m_EndColor = Color.black;

		// Token: 0x04002B96 RID: 11158
		private float m_Duration;

		// Token: 0x04002B97 RID: 11159
		private float m_Time;
	}
}
