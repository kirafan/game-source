﻿using System;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02000746 RID: 1862
	public class ADVPenParentChildPen : ADVPenParentChild<iTweenLocalMoveAddWrapper>
	{
		// Token: 0x060024BE RID: 9406 RVA: 0x000C5C5C File Offset: 0x000C405C
		public override void Setup()
		{
			base.Setup();
			this.m_BeforeLocalPosition = this.m_RectTransform.anchoredPosition;
			this.m_AfterLocalPosition = new Vector3(this.m_BeforeLocalPosition.x, this.m_BeforeLocalPosition.y + (float)this.m_MoveY, this.m_BeforeLocalPosition.z);
		}

		// Token: 0x060024BF RID: 9407 RVA: 0x000C5CBC File Offset: 0x000C40BC
		public override void ChangeStateIdleUp()
		{
			this.m_RectTransform.anchoredPosition = this.m_BeforeLocalPosition;
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 1.2f,
				easeType = iTween.EaseType.easeInOutQuad
			}.AddYParameter((float)this.m_MoveY));
			this.m_State = ePenChildAnimationState.IdleUp;
		}

		// Token: 0x060024C0 RID: 9408 RVA: 0x000C5D28 File Offset: 0x000C4128
		public override void ChangeStateIdleDown()
		{
			this.m_RectTransform.anchoredPosition = this.m_AfterLocalPosition;
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 1.2f,
				easeType = iTween.EaseType.easeInOutQuad
			}.AddYParameter((float)(-(float)this.m_MoveY)));
			this.m_State = ePenChildAnimationState.IdleDown;
		}

		// Token: 0x060024C1 RID: 9409 RVA: 0x000C5D95 File Offset: 0x000C4195
		public override void ChangeStateDecided()
		{
			this.m_RectTransform.anchoredPosition = this.m_AfterLocalPosition;
			this.m_iTweenWrapper.Stop();
			this.m_State = ePenChildAnimationState.Decided;
		}

		// Token: 0x060024C2 RID: 9410 RVA: 0x000C5DC0 File Offset: 0x000C41C0
		public override void ChangeStateDecideDown()
		{
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 0.05f,
				easeType = iTween.EaseType.linear
			}.AddYParameter((float)(-(float)this.m_MoveY)));
			this.m_State = ePenChildAnimationState.DecideDown;
		}

		// Token: 0x060024C3 RID: 9411 RVA: 0x000C5E18 File Offset: 0x000C4218
		public override void ChangeStateDecideUp()
		{
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 0.05f,
				easeType = iTween.EaseType.linear
			}.AddYParameter((float)this.m_MoveY));
			this.m_State = ePenChildAnimationState.DecideUp;
		}

		// Token: 0x04002BA5 RID: 11173
		private Vector3 m_BeforeLocalPosition;

		// Token: 0x04002BA6 RID: 11174
		private Vector3 m_AfterLocalPosition;

		// Token: 0x04002BA7 RID: 11175
		[SerializeField]
		[Tooltip("Y移動量.")]
		private int m_MoveY;
	}
}
