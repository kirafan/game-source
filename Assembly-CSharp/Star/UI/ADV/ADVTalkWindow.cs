﻿using System;
using System.Collections.Generic;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200074B RID: 1867
	public class ADVTalkWindow : UIGroup
	{
		// Token: 0x17000267 RID: 615
		// (get) Token: 0x060024E7 RID: 9447 RVA: 0x000C664D File Offset: 0x000C4A4D
		public ADVTalkCustomWindow.eTextState TextState
		{
			get
			{
				if (this.m_TalkCustomWindow == null)
				{
					return ADVTalkCustomWindow.eTextState.None;
				}
				return this.m_TalkCustomWindow.TextState;
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x060024E8 RID: 9448 RVA: 0x000C666D File Offset: 0x000C4A6D
		public float DefaultFontSize
		{
			get
			{
				if (this.m_TalkCustomWindow == null)
				{
					return 0f;
				}
				return this.m_TalkCustomWindow.DefaultFontSize;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x060024E9 RID: 9449 RVA: 0x000C6691 File Offset: 0x000C4A91
		public ADVCalcText CalcTextObj
		{
			get
			{
				if (this.m_TalkCustomWindow == null)
				{
					return null;
				}
				return this.m_TalkCustomWindow.CalcTextObj;
			}
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x060024EA RID: 9450 RVA: 0x000C66B1 File Offset: 0x000C4AB1
		public ADVRubyPool RubyPool
		{
			get
			{
				if (this.m_TalkCustomWindow == null)
				{
					return null;
				}
				return this.m_TalkCustomWindow.RubyPool;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x060024EB RID: 9451 RVA: 0x000C66D1 File Offset: 0x000C4AD1
		// (set) Token: 0x060024EC RID: 9452 RVA: 0x000C66F1 File Offset: 0x000C4AF1
		public bool IsEnableAuto
		{
			get
			{
				return !(this.m_TalkCustomWindow == null) && this.m_TalkCustomWindow.IsEnableAuto;
			}
			set
			{
				this.EnableAuto(value);
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x060024ED RID: 9453 RVA: 0x000C66FA File Offset: 0x000C4AFA
		public GameObject AutoIcon
		{
			get
			{
				if (this.m_TalkCustomWindow == null)
				{
					return null;
				}
				return this.m_TalkCustomWindow.AutoIcon;
			}
		}

		// Token: 0x060024EE RID: 9454 RVA: 0x000C671A File Offset: 0x000C4B1A
		public void Setup()
		{
			this.AcquireTalkCustomWindow();
		}

		// Token: 0x060024EF RID: 9455 RVA: 0x000C6723 File Offset: 0x000C4B23
		public void SetupParser(ADVParser parser)
		{
			if (this.m_TalkCustomWindow == null)
			{
				return;
			}
			this.m_TalkCustomWindow.SetupParser(parser);
		}

		// Token: 0x060024F0 RID: 9456 RVA: 0x000C6743 File Offset: 0x000C4B43
		public void Destroy()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.Destroy();
			}
		}

		// Token: 0x060024F1 RID: 9457 RVA: 0x000C6761 File Offset: 0x000C4B61
		public override void Open()
		{
			if (!base.IsOpenOrIn())
			{
				base.Open();
			}
			if (this.m_CustomWindowPrefab != null)
			{
				this.m_TalkCustomWindow = this.m_CustomWindow.GetComponent<ADVTalkCustomWindow>();
				this.SetupCustomWindow();
			}
		}

		// Token: 0x060024F2 RID: 9458 RVA: 0x000C679D File Offset: 0x000C4B9D
		public override void Close()
		{
			base.Close();
		}

		// Token: 0x060024F3 RID: 9459 RVA: 0x000C67A5 File Offset: 0x000C4BA5
		public override void Update()
		{
			base.Update();
		}

		// Token: 0x060024F4 RID: 9460 RVA: 0x000C67AD File Offset: 0x000C4BAD
		public void ManualUpdate()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.ManualUpdate();
			}
		}

		// Token: 0x060024F5 RID: 9461 RVA: 0x000C67CB File Offset: 0x000C4BCB
		public void ManualUpdate2()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.ManualUpdate2();
			}
		}

		// Token: 0x060024F6 RID: 9462 RVA: 0x000C67E9 File Offset: 0x000C4BE9
		public void SetTalker(string talker)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.SetTalker(talker);
			}
		}

		// Token: 0x060024F7 RID: 9463 RVA: 0x000C6808 File Offset: 0x000C4C08
		public Vector2 CalcLastPosition(string text)
		{
			if (this.m_TalkCustomWindow == null)
			{
				return default(Vector2);
			}
			return this.m_TalkCustomWindow.CalcLastPosition(text);
		}

		// Token: 0x060024F8 RID: 9464 RVA: 0x000C683C File Offset: 0x000C4C3C
		public void SetText(string text, ADVTalkCustomWindow.eTextState state = ADVTalkCustomWindow.eTextState.Start)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.SetText(text, state);
			}
		}

		// Token: 0x060024F9 RID: 9465 RVA: 0x000C685C File Offset: 0x000C4C5C
		public void StartDisplayText()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.StartDisplayText();
			}
		}

		// Token: 0x060024FA RID: 9466 RVA: 0x000C687A File Offset: 0x000C4C7A
		public void SkipTextProgress()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.SkipTextProgress();
			}
		}

		// Token: 0x060024FB RID: 9467 RVA: 0x000C6898 File Offset: 0x000C4C98
		public void FlushText()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.FlushText();
			}
		}

		// Token: 0x060024FC RID: 9468 RVA: 0x000C68B6 File Offset: 0x000C4CB6
		public void AddRuby(ADVParser.RubyData rubyData)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.AddRuby(rubyData);
			}
		}

		// Token: 0x060024FD RID: 9469 RVA: 0x000C68D5 File Offset: 0x000C4CD5
		public void ClearRuby()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.ClearRuby();
			}
		}

		// Token: 0x060024FE RID: 9470 RVA: 0x000C68F3 File Offset: 0x000C4CF3
		public List<ADVParser.RubyData> GetRubyList()
		{
			if (this.m_TalkCustomWindow == null)
			{
				return new List<ADVParser.RubyData>();
			}
			return this.m_TalkCustomWindow.GetRubyList();
		}

		// Token: 0x060024FF RID: 9471 RVA: 0x000C6917 File Offset: 0x000C4D17
		public void EnableAuto(bool flag)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.EnableAuto(flag);
			}
		}

		// Token: 0x06002500 RID: 9472 RVA: 0x000C6936 File Offset: 0x000C4D36
		public void EnableAutoIcon(bool flag)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.EnableAutoIcon(flag);
			}
		}

		// Token: 0x06002501 RID: 9473 RVA: 0x000C6955 File Offset: 0x000C4D55
		public string GetNowDisplayMessage()
		{
			if (this.m_TalkCustomWindow != null)
			{
				return this.m_TalkCustomWindow.GetNowDisplayMessage();
			}
			return null;
		}

		// Token: 0x06002502 RID: 9474 RVA: 0x000C6975 File Offset: 0x000C4D75
		public void SetPenActive(bool active)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.SetPenActive(active);
			}
		}

		// Token: 0x06002503 RID: 9475 RVA: 0x000C6994 File Offset: 0x000C4D94
		public void SetAutoPage(bool active)
		{
			this.m_AutoPage = active;
		}

		// Token: 0x06002504 RID: 9476 RVA: 0x000C699D File Offset: 0x000C4D9D
		public void SetIsActiveTapToNextPage(bool active)
		{
			this.m_IsActiveTapToNextPage = active;
		}

		// Token: 0x06002505 RID: 9477 RVA: 0x000C69A6 File Offset: 0x000C4DA6
		public void SetIsActiveTapToProgressSkip(bool active)
		{
			this.m_IsActiveTapToProgressSkip = active;
		}

		// Token: 0x06002506 RID: 9478 RVA: 0x000C69AF File Offset: 0x000C4DAF
		public void SetHeightVariable(bool active)
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.SetHeightVariable(active);
			}
		}

		// Token: 0x06002507 RID: 9479 RVA: 0x000C69CE File Offset: 0x000C4DCE
		public void OnClickScreen()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.OnClickScreen();
			}
		}

		// Token: 0x06002508 RID: 9480 RVA: 0x000C69EC File Offset: 0x000C4DEC
		public void OnClickScreenTalkWindowProcess()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.OnClickScreenTalkWindowProcess();
			}
		}

		// Token: 0x06002509 RID: 9481 RVA: 0x000C6A0A File Offset: 0x000C4E0A
		public void OnClickScreenPenProcess()
		{
			if (this.m_TalkCustomWindow != null)
			{
				this.m_TalkCustomWindow.OnClickScreenPenProcess();
			}
		}

		// Token: 0x0600250A RID: 9482 RVA: 0x000C6A28 File Offset: 0x000C4E28
		protected bool AcquireTalkCustomWindow()
		{
			if (this.m_TalkCustomWindow != null)
			{
				return true;
			}
			this.Open();
			this.Close();
			return this.m_TalkCustomWindow != null;
		}

		// Token: 0x0600250B RID: 9483 RVA: 0x000C6A58 File Offset: 0x000C4E58
		protected bool SetupCustomWindow()
		{
			if (this.m_TalkCustomWindow == null && !this.AcquireTalkCustomWindow())
			{
				return false;
			}
			if (!this.m_TalkCustomWindow.IsSetuped)
			{
				this.m_TalkCustomWindow.Setup(true);
				this.m_TalkCustomWindow.SetTextProgressPerSec(this.m_TextProgressPerSec);
				if (this.m_TalkWindowFrameSprite != null)
				{
					this.m_TalkCustomWindow.SetTalkWindowFrameSprite(this.m_TalkWindowFrameSprite);
				}
				this.m_TalkCustomWindow.SetBalloonProtrsionData(this.m_BalloonProtrsionSprite, this.m_FitBalloonProtrsionRect);
				this.m_TalkCustomWindow.SetPenActive(this.m_IsPenActive);
				this.m_TalkCustomWindow.SetHeightVariable(this.m_IsHeightVariable);
				this.m_TalkCustomWindow.SetAutoPage(this.m_AutoPage);
				this.m_TalkCustomWindow.SetIsActiveTapToNextPage(this.m_IsActiveTapToNextPage);
				this.m_TalkCustomWindow.SetIsActiveTapToProgressSkip(this.m_IsActiveTapToProgressSkip);
				this.m_TalkCustomWindow.SetTapToStartOverRowProgress(false);
			}
			return this.m_TalkCustomWindow.IsSetuped;
		}

		// Token: 0x04002BBE RID: 11198
		private ADVTalkCustomWindow m_TalkCustomWindow;

		// Token: 0x04002BBF RID: 11199
		[SerializeField]
		[Tooltip("一秒間に表示される文字数 0以下で一瞬")]
		private float m_TextProgressPerSec = 50f;

		// Token: 0x04002BC0 RID: 11200
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x04002BC1 RID: 11201
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x04002BC2 RID: 11202
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x04002BC3 RID: 11203
		[SerializeField]
		[Tooltip("ペンを表示するか.")]
		private bool m_IsPenActive = true;

		// Token: 0x04002BC4 RID: 11204
		[SerializeField]
		private bool m_AutoPage;

		// Token: 0x04002BC5 RID: 11205
		[SerializeField]
		private bool m_IsActiveTapToNextPage = true;

		// Token: 0x04002BC6 RID: 11206
		[SerializeField]
		private bool m_IsActiveTapToProgressSkip = true;

		// Token: 0x04002BC7 RID: 11207
		[SerializeField]
		[Tooltip("縦を行数に応じて4行まで増やすか.")]
		private bool m_IsHeightVariable;
	}
}
