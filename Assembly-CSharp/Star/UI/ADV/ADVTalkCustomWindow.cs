﻿using System;
using System.Collections.Generic;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000811 RID: 2065
	public class ADVTalkCustomWindow : MonoBehaviour
	{
		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x06002AB0 RID: 10928 RVA: 0x000E196D File Offset: 0x000DFD6D
		public ADVTalkCustomWindow.eTextState TextState
		{
			get
			{
				return this.m_TextState;
			}
		}

		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x06002AB1 RID: 10929 RVA: 0x000E1975 File Offset: 0x000DFD75
		public float DefaultFontSize
		{
			get
			{
				return this.m_DefaultFontSize;
			}
		}

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x06002AB2 RID: 10930 RVA: 0x000E197D File Offset: 0x000DFD7D
		public ADVCalcText CalcTextObj
		{
			get
			{
				return this.m_CalcTextObj;
			}
		}

		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x06002AB3 RID: 10931 RVA: 0x000E1985 File Offset: 0x000DFD85
		public ADVRubyPool RubyPool
		{
			get
			{
				return this.m_RubyPool;
			}
		}

		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x06002AB4 RID: 10932 RVA: 0x000E198D File Offset: 0x000DFD8D
		// (set) Token: 0x06002AB5 RID: 10933 RVA: 0x000E1995 File Offset: 0x000DFD95
		public bool IsEnableAuto
		{
			get
			{
				return this.m_IsAuto;
			}
			set
			{
				this.EnableAuto(value);
			}
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x06002AB6 RID: 10934 RVA: 0x000E199E File Offset: 0x000DFD9E
		public GameObject AutoIcon
		{
			get
			{
				return this.m_AutoIcon;
			}
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x06002AB7 RID: 10935 RVA: 0x000E19A6 File Offset: 0x000DFDA6
		public bool IsSetuped
		{
			get
			{
				return this.m_IsSetuped;
			}
		}

		// Token: 0x06002AB8 RID: 10936 RVA: 0x000E19AE File Offset: 0x000DFDAE
		public void Setup()
		{
			this.Setup(this.m_ManualUpdate);
		}

		// Token: 0x06002AB9 RID: 10937 RVA: 0x000E19BC File Offset: 0x000DFDBC
		public void Setup(bool manualUpdate)
		{
			if (this.m_IsHeightVariable)
			{
				this.SetHeightVariable(this.m_IsHeightVariable);
			}
			if (this.m_TapToStartOverRowProgress)
			{
				this.SetTapToStartOverRowProgress(this.m_TapToStartOverRowProgress);
			}
			this.m_AutoWaitCount = 0f;
			this.m_ManualUpdate = manualUpdate;
			this.m_DefaultFontSize = (float)this.m_TextObj.fontSize;
			this.m_TextRectTransform = this.m_TextObj.rectTransform;
			this.m_RubyPool.Setup();
			Transform transform = null;
			if (this.m_AutoIcon != null)
			{
				transform = this.m_AutoIcon.transform.FindChild("Image");
			}
			if (transform != null)
			{
				this.m_AutoIconImageRectTransform = (RectTransform)transform;
			}
			if (this.m_PenParent != null)
			{
				this.m_PenParent.Setup();
			}
			this.SetTalker(this.m_TalkerName);
			this.SetPenActive(this.m_IsPenActive);
			this.SetHeightVariable(false);
			this.EnableAutoIcon(false);
			if (this.m_TalkWindowFrame != null)
			{
				this.SetTalkWindowFrameSprite(this.m_TalkWindowFrameSprite);
			}
			this.SetImageData(this.m_BalloonProtrsion, this.m_BalloonProtrsionSprite, this.m_FitBalloonProtrsionRect);
			if (this.m_TalkWindowFrame != null)
			{
				this.m_TalkWindowRectTransform = this.m_TalkWindowFrame.rectTransform;
			}
			this.m_TalkerLabelRectTransform = this.m_TalkerGameObject.GetComponent<RectTransform>();
			this.m_TextBaseHeight = this.m_TalkWindowRectTransform.sizeDelta.y;
			this.m_TextRowHeight = 56f;
			this.m_DefaultRows = 2f;
			this.m_TalkerLabelBaseY = this.m_TalkerLabelRectTransform.localPosition.y;
			this.m_IsSetuped = true;
		}

		// Token: 0x06002ABA RID: 10938 RVA: 0x000E1B71 File Offset: 0x000DFF71
		public void SetupParser(ADVParser parser)
		{
			if (parser == null)
			{
				parser = new ADVParser();
			}
			this.m_Parser = parser;
		}

		// Token: 0x06002ABB RID: 10939 RVA: 0x000E1B87 File Offset: 0x000DFF87
		public void Destroy()
		{
			this.m_RubyPool.Destroy();
			this.m_TalkWindowFrameSprite = null;
			this.m_TalkWindowFrame.sprite = null;
		}

		// Token: 0x06002ABC RID: 10940 RVA: 0x000E1BA8 File Offset: 0x000DFFA8
		private void Update()
		{
			if (!this.m_ManualUpdate)
			{
				this.UpdateProcess();
				this.UpdateProcess2();
			}
			if (this.m_AutoIconImageRectTransform != null)
			{
				this.m_AutoIconImageRectTransform.localEulerAngles = new Vector3(0f, 0f, this.m_AutoIconImageRectTransform.localEulerAngles.z - 120f * Time.deltaTime);
			}
		}

		// Token: 0x06002ABD RID: 10941 RVA: 0x000E1C16 File Offset: 0x000E0016
		public void ManualUpdate()
		{
			if (this.m_ManualUpdate)
			{
				this.UpdateProcess();
			}
		}

		// Token: 0x06002ABE RID: 10942 RVA: 0x000E1C29 File Offset: 0x000E0029
		public void ManualUpdate2()
		{
			if (this.m_ManualUpdate)
			{
				this.UpdateProcess2();
			}
		}

		// Token: 0x06002ABF RID: 10943 RVA: 0x000E1C3C File Offset: 0x000E003C
		private void UpdateProcess()
		{
			if (this.m_WindowType == ADVTalkCustomWindow.eTalkWindowType.HeightVariable && this.m_TalkWindowRectTransform != null)
			{
				float num = (float)this.GetNowDisplayMessage().Split(new char[]
				{
					'\n'
				}).Length;
				float num2 = (num - this.m_DefaultRows) * this.m_TextRowHeight;
				num2 = Mathf.Clamp(num2, 0f, this.m_TextRowHeight * 2f);
				this.m_TalkWindowRectTransform.sizeDelta = new Vector2(this.m_TalkWindowRectTransform.sizeDelta.x, this.m_TextBaseHeight + num2);
				if (this.m_TalkerLabelRectTransform != null)
				{
					this.m_TalkerLabelRectTransform.localPosY(this.m_TalkerLabelBaseY + num2);
				}
			}
			if (this.m_WaitTimer > 0f)
			{
				this.m_WaitTimer -= Time.deltaTime;
				if (this.m_WaitTimer < 0f)
				{
					this.SetWait(0f);
				}
				return;
			}
			if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress)
			{
				return;
			}
			if (this.m_TextState != ADVTalkCustomWindow.eTextState.None && this.m_TextState == ADVTalkCustomWindow.eTextState.Progress)
			{
				int i;
				if (this.m_TextProgressPerSec <= 0f || this.m_StrongTextFlag)
				{
					i = this.m_NowMessageAll.Length;
				}
				else
				{
					this.m_TextCountTimer = (float)this.m_TextCount + (this.m_TextCountTimer - (float)((int)this.m_TextCountTimer));
					if (this.m_TextProgressPerSec > 0f)
					{
						this.m_TextCountTimer += Time.deltaTime * this.m_TextProgressPerSec;
					}
					i = (int)this.m_TextCountTimer;
					if (i > this.m_NowMessageAll.Length)
					{
						i = this.m_NowMessageAll.Length;
					}
				}
				if (i < 0)
				{
					i = 0;
				}
				while (i > this.m_TextCount)
				{
					bool flag = false;
					this.m_RubyList.Clear();
					string text = this.m_NowMessageAll;
					List<ADVParser.RubyData> list = new List<ADVParser.RubyData>();
					if (this.m_Parser != null)
					{
						text = this.m_Parser.Parse(ref this.m_NowMessageAll, ref this.m_TextCount, ref this.m_msgEventList, true, true, out flag, this.CalcTextObj, ref list);
					}
					else
					{
						this.m_TextCount = i;
					}
					this.UpdateText(text);
					for (int j = 0; j < list.Count; j++)
					{
						this.AddRuby(list[j]);
					}
					if (this.m_TextProgressPerSec <= 0f || this.m_StrongTextFlag)
					{
						i = this.m_NowMessageAll.Length;
					}
					else if (i > this.m_NowMessageAll.Length)
					{
						i = this.m_NowMessageAll.Length;
					}
					if (i < 0)
					{
						i = 0;
					}
					if (this.m_TextCount >= this.m_NowMessageAll.Length)
					{
						this.m_TextState = ADVTalkCustomWindow.eTextState.TapWait;
						break;
					}
					if (flag && this.m_TextProgressPerSec > 0f)
					{
						this.m_TextCountTimer = (float)this.m_TextCount;
						break;
					}
				}
			}
		}

		// Token: 0x06002AC0 RID: 10944 RVA: 0x000E1F3C File Offset: 0x000E033C
		private void UpdateProcess2()
		{
			if ((this.m_IsAuto || (this.m_AutoPage && this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress)) && (this.m_TextState == ADVTalkCustomWindow.eTextState.TapWait || this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress))
			{
				this.m_AutoWaitCount += Time.deltaTime;
				if (this.m_AutoWait < this.m_AutoWaitCount)
				{
					if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapWait)
					{
						this.FlushText();
					}
					else if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress)
					{
						this.m_TextState = ADVTalkCustomWindow.eTextState.Progress;
						this.m_NowDisplayPageIndex++;
					}
					this.m_AutoWaitCount = 0f;
				}
			}
		}

		// Token: 0x06002AC1 RID: 10945 RVA: 0x000E1FE9 File Offset: 0x000E03E9
		public void NewPage()
		{
			this.NewPage(this.GetNowDisplayMessage());
		}

		// Token: 0x06002AC2 RID: 10946 RVA: 0x000E1FF7 File Offset: 0x000E03F7
		public void NewPage(string oldPageTextsAll)
		{
			this.SetNowDisplayMessage(this.ConvertNowAllTextToNowDisplayText(oldPageTextsAll));
			this.m_TextState = ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress;
		}

		// Token: 0x06002AC3 RID: 10947 RVA: 0x000E200D File Offset: 0x000E040D
		public void SetTalker(string talker)
		{
			this.m_TalkerName = talker;
			if (string.IsNullOrEmpty(talker))
			{
				this.m_TalkerGameObject.SetActive(false);
			}
			else
			{
				this.m_TalkerGameObject.SetActive(true);
				this.m_TalkerObj.text = talker;
			}
		}

		// Token: 0x06002AC4 RID: 10948 RVA: 0x000E204A File Offset: 0x000E044A
		public Vector2 CalcLastPosition(string text)
		{
			return this.m_CalcTextObj.CalcLastPosition(text);
		}

		// Token: 0x06002AC5 RID: 10949 RVA: 0x000E2058 File Offset: 0x000E0458
		private void InitializeText(string text, ADVTalkCustomWindow.eTextState state)
		{
			this.SetWait(0f);
			this.ClearRuby();
			this.m_NowMessageAll = text;
			this.m_TextState = state;
			this.m_TextCountTimer = 0f;
			this.m_TextCount = 0;
			this.m_NowMessageDisplayeds = new List<string>();
			this.m_NowDisplayPageIndex = 0;
			this.UpdateText(string.Empty);
		}

		// Token: 0x06002AC6 RID: 10950 RVA: 0x000E20B3 File Offset: 0x000E04B3
		public void SetText(string text, ADVTalkCustomWindow.eTextState state = ADVTalkCustomWindow.eTextState.Start)
		{
			this.InitializeText(text, state);
			if (state == ADVTalkCustomWindow.eTextState.Progress)
			{
				this.UpdateText(text);
			}
			this.StartDisplayText();
		}

		// Token: 0x06002AC7 RID: 10951 RVA: 0x000E20D4 File Offset: 0x000E04D4
		private void UpdateText(string text)
		{
			this.SetNowDisplayMessage(this.ConvertNowAllTextToNowDisplayText(text));
			string text2;
			if (this.m_WindowType == ADVTalkCustomWindow.eTalkWindowType.TapToStartOverRowProgress)
			{
				text2 = string.Empty;
				string[] array = this.GetNowDisplayMessage().Split(new char[]
				{
					'\n'
				});
				if (3 <= array.Length && this.GetNowDisplayMessage() != this.ConvertNowAllTextToNowDisplayText(this.m_NowMessageAll))
				{
					this.NewPage();
				}
				text2 = this.GetNowDisplayMessage();
			}
			else
			{
				text2 = this.GetNowDisplayMessage();
			}
			this.m_TextObj.text = text2;
		}

		// Token: 0x06002AC8 RID: 10952 RVA: 0x000E2168 File Offset: 0x000E0568
		public void StartDisplayText()
		{
			switch (this.m_TextState)
			{
			case ADVTalkCustomWindow.eTextState.StandBy:
			case ADVTalkCustomWindow.eTextState.Progress:
			case ADVTalkCustomWindow.eTextState.Invalid:
				return;
			case ADVTalkCustomWindow.eTextState.Start:
				this.m_TextState = ADVTalkCustomWindow.eTextState.Progress;
				return;
			}
			this.m_TextState = ADVTalkCustomWindow.eTextState.Invalid;
		}

		// Token: 0x06002AC9 RID: 10953 RVA: 0x000E21C4 File Offset: 0x000E05C4
		public void SkipTextProgress()
		{
			while (this.m_TextCount < this.m_NowMessageAll.Length)
			{
				bool flag = false;
				this.m_RubyList.Clear();
				string text = this.m_NowMessageAll;
				List<ADVParser.RubyData> list = new List<ADVParser.RubyData>();
				if (this.m_Parser != null)
				{
					text = this.m_Parser.Parse(ref this.m_NowMessageAll, ref this.m_TextCount, ref this.m_msgEventList, false, true, out flag, this.CalcTextObj, ref list);
				}
				else
				{
					this.m_TextCount = this.m_NowMessageAll.Length;
				}
				this.UpdateText(text);
				if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress)
				{
					return;
				}
				for (int i = 0; i < list.Count; i++)
				{
					this.AddRuby(list[i]);
				}
			}
			this.SetWait(0f);
			this.m_TextState = ADVTalkCustomWindow.eTextState.TapWait;
		}

		// Token: 0x06002ACA RID: 10954 RVA: 0x000E229C File Offset: 0x000E069C
		public void FlushText()
		{
			ADVPlayer advplayer = this.m_Parser.GetADVPlayer();
			if (advplayer != null)
			{
				advplayer.AddBackLogText();
			}
			this.m_TextState = ADVTalkCustomWindow.eTextState.None;
			this.ClearRuby();
			this.m_NowMessageDisplayeds = new List<string>();
			this.UpdateText(string.Empty);
			this.m_NowDisplayPageIndex = -1;
		}

		// Token: 0x06002ACB RID: 10955 RVA: 0x000E22F4 File Offset: 0x000E06F4
		public void AddRuby(ADVParser.RubyData rubyData)
		{
			this.m_RubyList.Add(rubyData);
			Text text = this.m_RubyPool.ScoopRuby();
			text.text = rubyData.m_Text;
			RectTransform rectTransform = text.rectTransform;
			rectTransform.SetParent(this.m_TextRectTransform, false);
			rectTransform.anchoredPosition = (rubyData.m_StartPos + rubyData.m_EndPos) * 0.5f;
			this.m_RubyObjList.Add(text);
		}

		// Token: 0x06002ACC RID: 10956 RVA: 0x000E2368 File Offset: 0x000E0768
		public void ClearRuby()
		{
			this.m_RubyList.Clear();
			for (int i = 0; i < this.m_RubyObjList.Count; i++)
			{
				this.m_RubyPool.SinkRuby(this.m_RubyObjList[i]);
			}
			this.m_RubyObjList.Clear();
		}

		// Token: 0x06002ACD RID: 10957 RVA: 0x000E23BE File Offset: 0x000E07BE
		public List<ADVParser.RubyData> GetRubyList()
		{
			return this.m_RubyList;
		}

		// Token: 0x06002ACE RID: 10958 RVA: 0x000E23C6 File Offset: 0x000E07C6
		public void EnableAuto(bool flag)
		{
			this.m_IsAuto = flag;
			this.m_AutoWaitCount = 0f;
			this.EnableAutoIcon(flag);
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x000E23E1 File Offset: 0x000E07E1
		public void EnableAutoIcon(bool flag)
		{
			if (this.m_AutoIcon != null)
			{
				this.m_AutoIcon.SetActive(flag);
			}
		}

		// Token: 0x06002AD0 RID: 10960 RVA: 0x000E2400 File Offset: 0x000E0800
		public string ConvertNowAllTextToNowDisplayText(string nowAllText)
		{
			for (int i = this.m_NowMessageDisplayeds.Count; i <= this.m_NowDisplayPageIndex; i++)
			{
				this.m_NowMessageDisplayeds.Add(null);
			}
			string text = nowAllText;
			for (int j = 0; j < this.m_NowDisplayPageIndex; j++)
			{
				string text2 = this.m_NowMessageDisplayeds[j];
				string text3 = null;
				if (text2 != null && text2.Length < text.Length)
				{
					text3 = text.Substring(0, text2.Length);
				}
				if (text3 == null)
				{
					text3 = string.Empty;
				}
				if (text2 == text3)
				{
					text = text.Remove(0, text2.Length);
				}
			}
			return text;
		}

		// Token: 0x06002AD1 RID: 10961 RVA: 0x000E24B8 File Offset: 0x000E08B8
		public ADVTalkCustomWindow.eTextState GetTextState()
		{
			return this.m_TextState;
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x000E24C0 File Offset: 0x000E08C0
		public string GetNowDisplayMessage()
		{
			for (int i = this.m_NowMessageDisplayeds.Count; i <= this.m_NowDisplayPageIndex; i++)
			{
				this.m_NowMessageDisplayeds.Add(null);
			}
			return this.m_NowMessageDisplayeds[this.m_NowDisplayPageIndex];
		}

		// Token: 0x06002AD3 RID: 10963 RVA: 0x000E250C File Offset: 0x000E090C
		private void SetNowDisplayMessage(string text)
		{
			for (int i = this.m_NowMessageDisplayeds.Count; i <= this.m_NowDisplayPageIndex; i++)
			{
				this.m_NowMessageDisplayeds.Add(null);
			}
			this.m_NowMessageDisplayeds[this.m_NowDisplayPageIndex] = text;
		}

		// Token: 0x06002AD4 RID: 10964 RVA: 0x000E2558 File Offset: 0x000E0958
		public void SetTalkWindowFrameSizeDeltaY(float y)
		{
			this.m_TextObj.rectTransform.sizeDelta = new Vector2(this.m_TextObj.rectTransform.sizeDelta.x, y);
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x000E2594 File Offset: 0x000E0994
		public void AddTalkWindowTextLocalPositionY(float y)
		{
			this.m_TextObj.rectTransform.localPosY(this.m_TextObj.rectTransform.localPosition.y + y);
		}

		// Token: 0x06002AD6 RID: 10966 RVA: 0x000E25CB File Offset: 0x000E09CB
		public void SetLineSpacing(float lineSpacing)
		{
			this.m_TextObj.lineSpacing = lineSpacing;
		}

		// Token: 0x06002AD7 RID: 10967 RVA: 0x000E25D9 File Offset: 0x000E09D9
		public void SetFontSize(int fontSize)
		{
			this.m_DefaultFontSize = (float)fontSize;
			this.m_TextObj.fontSize = fontSize;
		}

		// Token: 0x06002AD8 RID: 10968 RVA: 0x000E25EF File Offset: 0x000E09EF
		public void SetTextProgressPerSec(float textProgressPerSec)
		{
			this.m_TextProgressPerSec = textProgressPerSec;
		}

		// Token: 0x06002AD9 RID: 10969 RVA: 0x000E25F8 File Offset: 0x000E09F8
		public void SetTalkWindowFrameSprite(Sprite talkWindowFrameSprite)
		{
			this.m_TalkWindowFrameSprite = talkWindowFrameSprite;
			if (this.m_TalkWindowFrame != null)
			{
				this.m_TalkWindowFrame.sprite = this.m_TalkWindowFrameSprite;
				if (this.m_TalkWindowFrameSprite == null)
				{
					this.m_TalkWindowFrame.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x06002ADA RID: 10970 RVA: 0x000E2650 File Offset: 0x000E0A50
		public void SetBalloonProtrsionData(Sprite balloonProtrsionSprite, RectTransform fitBalloonProtrsionRect)
		{
			this.m_BalloonProtrsionSprite = balloonProtrsionSprite;
			this.m_FitBalloonProtrsionRect = fitBalloonProtrsionRect;
			this.SetImageData(this.m_BalloonProtrsion, this.m_BalloonProtrsionSprite, this.m_FitBalloonProtrsionRect);
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x000E2678 File Offset: 0x000E0A78
		public void SetImageData(Image destination, Sprite sprite, RectTransform fitRect)
		{
			if (destination != null)
			{
				destination.gameObject.SetActive(true);
				destination.sprite = sprite;
				if (sprite == null)
				{
					destination.gameObject.SetActive(false);
				}
				if (fitRect != null)
				{
					destination.rectTransform.localPosition = fitRect.localPosition;
					destination.rectTransform.sizeDelta = fitRect.sizeDelta;
				}
				else
				{
					destination.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x06002ADC RID: 10972 RVA: 0x000E26FB File Offset: 0x000E0AFB
		public void SetWait(float time)
		{
			this.m_WaitTimer = time;
		}

		// Token: 0x06002ADD RID: 10973 RVA: 0x000E2704 File Offset: 0x000E0B04
		public void SetPenActive(bool active)
		{
			if (this.m_PenParent != null)
			{
				this.m_PenParent.SetPenActive(active);
			}
		}

		// Token: 0x06002ADE RID: 10974 RVA: 0x000E2723 File Offset: 0x000E0B23
		public void SetAutoPage(bool active)
		{
			this.m_AutoPage = active;
		}

		// Token: 0x06002ADF RID: 10975 RVA: 0x000E272C File Offset: 0x000E0B2C
		public void SetIsActiveTapToNextPage(bool active)
		{
			this.m_IsActiveTapToNextPage = active;
		}

		// Token: 0x06002AE0 RID: 10976 RVA: 0x000E2735 File Offset: 0x000E0B35
		public void SetIsActiveTapToProgressSkip(bool active)
		{
			this.m_IsActiveTapToProgressSkip = active;
		}

		// Token: 0x06002AE1 RID: 10977 RVA: 0x000E273E File Offset: 0x000E0B3E
		public void SetAutoWait(float wait)
		{
			this.m_AutoWait = wait;
		}

		// Token: 0x06002AE2 RID: 10978 RVA: 0x000E2747 File Offset: 0x000E0B47
		public void SetHeightVariable(bool active)
		{
			this.m_IsHeightVariable = active;
			if (active)
			{
				this.SetTalkWindowType(ADVTalkCustomWindow.eTalkWindowType.HeightVariable);
			}
		}

		// Token: 0x06002AE3 RID: 10979 RVA: 0x000E275D File Offset: 0x000E0B5D
		public void SetTapToStartOverRowProgress(bool active)
		{
			this.m_TapToStartOverRowProgress = active;
			if (active)
			{
				this.SetTalkWindowType(ADVTalkCustomWindow.eTalkWindowType.TapToStartOverRowProgress);
			}
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x000E2773 File Offset: 0x000E0B73
		public void SetTalkWindowType(ADVTalkCustomWindow.eTalkWindowType type)
		{
			this.m_WindowType = type;
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x000E277C File Offset: 0x000E0B7C
		public void OnClickScreen()
		{
			this.OnClickScreenTalkWindowProcess();
			this.OnClickScreenPenProcess();
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x000E278C File Offset: 0x000E0B8C
		public void OnClickScreenTalkWindowProcess()
		{
			if (this.m_IsAuto)
			{
				this.EnableAuto(false);
				return;
			}
			if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapToStartOverRowProgress)
			{
				if (this.m_IsActiveTapToNextPage && this.m_TapToStartOverRowProgress)
				{
					this.m_TextState = ADVTalkCustomWindow.eTextState.Progress;
					this.m_NowDisplayPageIndex++;
				}
			}
			else if (this.m_TextState == ADVTalkCustomWindow.eTextState.Progress)
			{
				if (this.m_IsActiveTapToProgressSkip)
				{
					this.m_Parser.SetTapWaitWithTapFlag(true);
					this.SkipTextProgress();
				}
			}
			else if (this.m_TextState == ADVTalkCustomWindow.eTextState.TapWait)
			{
				this.m_Parser.SetTapWaitWithTapFlag(false);
				this.FlushText();
			}
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x000E2834 File Offset: 0x000E0C34
		public void OnClickScreenPenProcess()
		{
			if (this.m_PenParent != null)
			{
				this.m_PenParent.OnClickScreen();
			}
		}

		// Token: 0x04003119 RID: 12569
		private ADVTalkCustomWindow.eTextState m_TextState;

		// Token: 0x0400311A RID: 12570
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x0400311B RID: 12571
		private RectTransform m_TextRectTransform;

		// Token: 0x0400311C RID: 12572
		private float m_DefaultFontSize = 36f;

		// Token: 0x0400311D RID: 12573
		private ADVParser m_Parser = new ADVParser();

		// Token: 0x0400311E RID: 12574
		private const int DEFAULT_RUBY_INSTANCE = 8;

		// Token: 0x0400311F RID: 12575
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04003120 RID: 12576
		private List<ADVParser.RubyData> m_RubyList = new List<ADVParser.RubyData>();

		// Token: 0x04003121 RID: 12577
		private List<Text> m_RubyObjList = new List<Text>();

		// Token: 0x04003122 RID: 12578
		[SerializeField]
		[Tooltip("別のインスタンスから更新処理を行う場合はtrue.")]
		private bool m_ManualUpdate;

		// Token: 0x04003123 RID: 12579
		[SerializeField]
		[Tooltip("一秒間に表示される文字数 0以下で一瞬")]
		private float m_TextProgressPerSec = 50f;

		// Token: 0x04003124 RID: 12580
		[SerializeField]
		private ADVCalcText m_CalcTextObj;

		// Token: 0x04003125 RID: 12581
		private string m_NowMessageAll;

		// Token: 0x04003126 RID: 12582
		private List<string> m_NowMessageDisplayeds;

		// Token: 0x04003127 RID: 12583
		private int m_TextCount;

		// Token: 0x04003128 RID: 12584
		private float m_TextCountTimer;

		// Token: 0x04003129 RID: 12585
		private bool m_IsAuto;

		// Token: 0x0400312A RID: 12586
		[SerializeField]
		private bool m_AutoPage;

		// Token: 0x0400312B RID: 12587
		[SerializeField]
		private bool m_IsActiveTapToNextPage = true;

		// Token: 0x0400312C RID: 12588
		[SerializeField]
		private bool m_IsActiveTapToProgressSkip = true;

		// Token: 0x0400312D RID: 12589
		[SerializeField]
		private float m_AutoWait = 1f;

		// Token: 0x0400312E RID: 12590
		private float m_AutoWaitCount;

		// Token: 0x0400312F RID: 12591
		[SerializeField]
		private Image m_TalkWindowFrame;

		// Token: 0x04003130 RID: 12592
		[SerializeField]
		private Sprite m_TalkWindowFrameSprite;

		// Token: 0x04003131 RID: 12593
		[SerializeField]
		private Text m_TalkerObj;

		// Token: 0x04003132 RID: 12594
		[SerializeField]
		private GameObject m_TalkerGameObject;

		// Token: 0x04003133 RID: 12595
		[SerializeField]
		[Tooltip("初期状態表示名.")]
		private string m_TalkerName;

		// Token: 0x04003134 RID: 12596
		[SerializeField]
		private Image m_BalloonProtrsion;

		// Token: 0x04003135 RID: 12597
		[SerializeField]
		private Sprite m_BalloonProtrsionSprite;

		// Token: 0x04003136 RID: 12598
		[SerializeField]
		public RectTransform m_FitBalloonProtrsionRect;

		// Token: 0x04003137 RID: 12599
		[SerializeField]
		private ADVPenParent m_PenParent;

		// Token: 0x04003138 RID: 12600
		[SerializeField]
		[Tooltip("ペンを表示するか.")]
		private bool m_IsPenActive = true;

		// Token: 0x04003139 RID: 12601
		[SerializeField]
		private ADVTalkCustomWindow.eTalkWindowType m_WindowType;

		// Token: 0x0400313A RID: 12602
		[SerializeField]
		[Tooltip("旧仕様.m_WindowTypeを使うこと.縦を行数に応じて4行まで増やすか.")]
		private bool m_IsHeightVariable;

		// Token: 0x0400313B RID: 12603
		private RectTransform m_TalkerLabelRectTransform;

		// Token: 0x0400313C RID: 12604
		private RectTransform m_TalkWindowRectTransform;

		// Token: 0x0400313D RID: 12605
		private float m_TextBaseHeight;

		// Token: 0x0400313E RID: 12606
		private float m_TextRowHeight;

		// Token: 0x0400313F RID: 12607
		private float m_DefaultRows;

		// Token: 0x04003140 RID: 12608
		private float m_TalkerLabelBaseY;

		// Token: 0x04003141 RID: 12609
		[SerializeField]
		[Tooltip("旧仕様.m_WindowTypeを使うこと.行数に応じて改ページするか.")]
		private bool m_TapToStartOverRowProgress;

		// Token: 0x04003142 RID: 12610
		private int m_NowDisplayPageIndex;

		// Token: 0x04003143 RID: 12611
		private RectTransform m_AutoIconImageRectTransform;

		// Token: 0x04003144 RID: 12612
		private const float m_AutoIconImageRotateZ = 120f;

		// Token: 0x04003145 RID: 12613
		[SerializeField]
		private GameObject m_AutoIcon;

		// Token: 0x04003146 RID: 12614
		private float m_WaitTimer;

		// Token: 0x04003147 RID: 12615
		private bool m_IsSetuped;

		// Token: 0x04003148 RID: 12616
		private List<ADVParser.EventType> m_msgEventList = new List<ADVParser.EventType>();

		// Token: 0x04003149 RID: 12617
		private bool m_StrongTextFlag;

		// Token: 0x02000812 RID: 2066
		public enum eTalkWindowType
		{
			// Token: 0x0400314B RID: 12619
			Normal,
			// Token: 0x0400314C RID: 12620
			HeightVariable,
			// Token: 0x0400314D RID: 12621
			TapToStartOverRowProgress
		}

		// Token: 0x02000813 RID: 2067
		public enum eTextState
		{
			// Token: 0x0400314F RID: 12623
			None,
			// Token: 0x04003150 RID: 12624
			StandBy,
			// Token: 0x04003151 RID: 12625
			Start,
			// Token: 0x04003152 RID: 12626
			Progress,
			// Token: 0x04003153 RID: 12627
			TapToStartOverRowProgress,
			// Token: 0x04003154 RID: 12628
			TapWait,
			// Token: 0x04003155 RID: 12629
			Stop,
			// Token: 0x04003156 RID: 12630
			Invalid
		}
	}
}
