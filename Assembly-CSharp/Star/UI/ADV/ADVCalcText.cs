﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000741 RID: 1857
	public class ADVCalcText : MonoBehaviour
	{
		// Token: 0x17000265 RID: 613
		// (get) Token: 0x060024A6 RID: 9382 RVA: 0x000C575F File Offset: 0x000C3B5F
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x060024A7 RID: 9383 RVA: 0x000C5784 File Offset: 0x000C3B84
		public Vector2 CalcLastPosition(string text)
		{
			Text text2;
			return this.CalcLastPosition(text, out text2);
		}

		// Token: 0x060024A8 RID: 9384 RVA: 0x000C579C File Offset: 0x000C3B9C
		public Vector2 CalcLastPosition(string text, out Text parentObj)
		{
			bool[] array = new bool[this.m_TextObjs.Length];
			for (int i = 0; i < this.m_TextObjs.Length; i++)
			{
				array[i] = this.m_TextObjs[i].enabled;
				this.m_TextObjs[i].enabled = true;
			}
			Vector2 result = new Vector2(0f, 0f);
			string[] array2 = text.Split(new char[]
			{
				'\n'
			});
			float num = 0f;
			if (array2.Length > 1)
			{
				for (int j = 0; j < this.m_TextObjs.Length; j++)
				{
					this.m_TextObjs[j].text = array2[j];
					this.m_TextObjs[j].GetComponent<ContentSizeFitter>().SetLayoutHorizontal();
					this.m_TextObjs[j].GetComponent<ContentSizeFitter>().SetLayoutVertical();
					num = this.m_TextObjs[j].rectTransform.rect.height;
				}
			}
			Text text2 = this.m_TextObjs[0];
			if (this.m_TextObjs.Length >= array2.Length)
			{
				text2 = this.m_TextObjs[array2.Length - 1];
			}
			text2.text = array2[array2.Length - 1];
			text2.GetComponent<ContentSizeFitter>().SetLayoutHorizontal();
			text2.GetComponent<ContentSizeFitter>().SetLayoutVertical();
			float width = text2.rectTransform.rect.width;
			result = new Vector2(width, -num);
			for (int k = 0; k < this.m_TextObjs.Length; k++)
			{
				this.m_TextObjs[k].enabled = array[k];
			}
			parentObj = text2;
			result.y += 8f;
			return result;
		}

		// Token: 0x04002B8E RID: 11150
		private const float RUBY_OFFSET_Y = 8f;

		// Token: 0x04002B8F RID: 11151
		[SerializeField]
		private Text[] m_TextObjs;

		// Token: 0x04002B90 RID: 11152
		private GameObject m_GameObject;
	}
}
