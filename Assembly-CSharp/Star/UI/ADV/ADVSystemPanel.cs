﻿using System;
using System.Collections;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000749 RID: 1865
	public class ADVSystemPanel : UIGroup
	{
		// Token: 0x060024D3 RID: 9427 RVA: 0x000C6254 File Offset: 0x000C4654
		public void Start()
		{
			this.m_Transform = base.GetComponent<RectTransform>();
			this.m_ShowPos = this.m_Transform.localPosition;
			this.m_HidePos = this.m_ShowPos + new Vector3(640f, 0f, 0f);
			this.m_AutoPos = this.m_ShowPos + new Vector3(800f, 0f, 0f);
			if (this.m_MenuState == ADVSystemPanel.eMenuState.Idle)
			{
				this.m_Transform.localPosition = this.m_ShowPos;
			}
			else if (this.m_MenuState == ADVSystemPanel.eMenuState.Hide)
			{
				this.m_Transform.localPosition = this.m_HidePos;
			}
			else
			{
				this.m_Transform.localPosition = this.m_AutoPos;
			}
			this.m_AutoDisableInHide = false;
		}

		// Token: 0x060024D4 RID: 9428 RVA: 0x000C6323 File Offset: 0x000C4723
		private void OnDestroy()
		{
			this.m_Player = null;
		}

		// Token: 0x060024D5 RID: 9429 RVA: 0x000C632C File Offset: 0x000C472C
		public override void Open()
		{
			this.ChangeMenuState(ADVSystemPanel.eMenuState.Idle, false);
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x000C6336 File Offset: 0x000C4736
		public override void Close()
		{
			this.ChangeMenuState(ADVSystemPanel.eMenuState.Hide, false);
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x000C6340 File Offset: 0x000C4740
		public void Auto(bool immediate)
		{
			this.ChangeMenuState(ADVSystemPanel.eMenuState.AutoMode, immediate);
		}

		// Token: 0x060024D8 RID: 9432 RVA: 0x000C634A File Offset: 0x000C474A
		public void StopAuto(bool immediate)
		{
			this.ChangeMenuState(ADVSystemPanel.eMenuState.Hide, immediate);
		}

		// Token: 0x060024D9 RID: 9433 RVA: 0x000C6354 File Offset: 0x000C4754
		public void Toggle()
		{
			if (this.m_MenuState == ADVSystemPanel.eMenuState.Hide)
			{
				this.Open();
			}
			else if (this.m_MenuState == ADVSystemPanel.eMenuState.Idle)
			{
				this.Close();
			}
			else if (this.m_MenuState == ADVSystemPanel.eMenuState.AutoMode)
			{
			}
		}

		// Token: 0x060024DA RID: 9434 RVA: 0x000C6390 File Offset: 0x000C4790
		private void ChangeMenuState(ADVSystemPanel.eMenuState state, bool immediate = false)
		{
			switch (state)
			{
			case ADVSystemPanel.eMenuState.Hide:
				this.ChangeMenuStateClose(immediate);
				break;
			case ADVSystemPanel.eMenuState.Idle:
				this.ChangeMenuStateOpen(immediate);
				break;
			case ADVSystemPanel.eMenuState.AutoMode:
				this.ChangeMenuStateAuto(immediate);
				break;
			}
		}

		// Token: 0x060024DB RID: 9435 RVA: 0x000C63E0 File Offset: 0x000C47E0
		private void ChangeMenuStateOpen(bool immediate = false)
		{
			base.Open();
			this.m_MenuState = ADVSystemPanel.eMenuState.Idle;
			this.SetToggleArrowScaleX(1f);
			if (immediate)
			{
				this.m_Transform.localPosition = this.m_ShowPos;
			}
			else
			{
				this.MoveToMenu(this.m_HidePos, this.m_ShowPos);
			}
		}

		// Token: 0x060024DC RID: 9436 RVA: 0x000C6434 File Offset: 0x000C4834
		private void ChangeMenuStateClose(bool immediate = false)
		{
			if (immediate)
			{
				this.m_Transform.localPosition = this.m_HidePos;
			}
			else if (this.m_MenuState == ADVSystemPanel.eMenuState.Idle)
			{
				base.Close();
				this.MoveToMenu(this.m_ShowPos, this.m_HidePos);
			}
			else if (this.m_MenuState == ADVSystemPanel.eMenuState.AutoMode)
			{
				this.MoveToMenu(this.m_AutoPos, this.m_HidePos);
			}
			this.SetToggleArrowScaleX(-1f);
			this.m_MenuState = ADVSystemPanel.eMenuState.Hide;
		}

		// Token: 0x060024DD RID: 9437 RVA: 0x000C64B8 File Offset: 0x000C48B8
		private void ChangeMenuStateAuto(bool immediate = false)
		{
			base.Close();
			this.m_MenuState = ADVSystemPanel.eMenuState.AutoMode;
			this.SetToggleArrowScaleX(-1f);
			if (immediate)
			{
				this.m_Transform.localPosition = this.m_AutoPos;
			}
			else
			{
				this.MoveToMenu(this.m_ShowPos, this.m_AutoPos);
			}
		}

		// Token: 0x060024DE RID: 9438 RVA: 0x000C650B File Offset: 0x000C490B
		public void EnableRender()
		{
			this.m_Player.ToggleShowUI();
		}

		// Token: 0x060024DF RID: 9439 RVA: 0x000C6518 File Offset: 0x000C4918
		private void MoveToMenu(Vector3 initPos, Vector3 destPos)
		{
			this.m_Transform.localPosition = initPos;
			Hashtable hashtable = new Hashtable();
			hashtable.Add("isLocal", true);
			hashtable.Add("x", destPos.x);
			hashtable.Add("y", destPos.y);
			hashtable.Add("time", this.m_AnimDuration);
			hashtable.Add("easeType", this.m_EaseType);
			iTween.MoveTo(base.GameObject, hashtable);
		}

		// Token: 0x060024E0 RID: 9440 RVA: 0x000C65B0 File Offset: 0x000C49B0
		private void SetToggleArrowScaleX(float scale)
		{
			RectTransform component = this.m_ToggleButton.GetComponent<RectTransform>();
			component.localScaleX(scale);
		}

		// Token: 0x060024E1 RID: 9441 RVA: 0x000C65D0 File Offset: 0x000C49D0
		public bool IsMenuOpen()
		{
			return this.m_MenuState == ADVSystemPanel.eMenuState.Idle;
		}

		// Token: 0x060024E2 RID: 9442 RVA: 0x000C65DB File Offset: 0x000C49DB
		protected override bool CheckInEnd()
		{
			return base.gameObject.GetComponent<iTween>() == null;
		}

		// Token: 0x060024E3 RID: 9443 RVA: 0x000C65EE File Offset: 0x000C49EE
		protected override bool CheckOutEnd()
		{
			return base.gameObject.GetComponent<iTween>() == null;
		}

		// Token: 0x060024E4 RID: 9444 RVA: 0x000C6601 File Offset: 0x000C4A01
		public void OnClickToggle()
		{
			this.Toggle();
		}

		// Token: 0x060024E5 RID: 9445 RVA: 0x000C6609 File Offset: 0x000C4A09
		public void OnClickAuto()
		{
			this.m_Player.EnableAuto(!this.m_Player.IsEnableAuto, true);
		}

		// Token: 0x04002BB1 RID: 11185
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04002BB2 RID: 11186
		[SerializeField]
		private float m_AnimDuration = 0.2f;

		// Token: 0x04002BB3 RID: 11187
		[SerializeField]
		private iTween.EaseType m_EaseType = iTween.EaseType.linear;

		// Token: 0x04002BB4 RID: 11188
		[SerializeField]
		private Button m_ToggleButton;

		// Token: 0x04002BB5 RID: 11189
		private Vector3 m_ShowPos;

		// Token: 0x04002BB6 RID: 11190
		private Vector3 m_HidePos;

		// Token: 0x04002BB7 RID: 11191
		private Vector3 m_AutoPos;

		// Token: 0x04002BB8 RID: 11192
		private ADVSystemPanel.eMenuState m_MenuState;

		// Token: 0x04002BB9 RID: 11193
		private RectTransform m_Transform;

		// Token: 0x0200074A RID: 1866
		public enum eMenuState
		{
			// Token: 0x04002BBB RID: 11195
			Hide,
			// Token: 0x04002BBC RID: 11196
			Idle,
			// Token: 0x04002BBD RID: 11197
			AutoMode
		}
	}
}
