﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000748 RID: 1864
	public class ADVRubyPool : MonoBehaviour
	{
		// Token: 0x060024CC RID: 9420 RVA: 0x000C6084 File Offset: 0x000C4484
		public void Setup()
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			for (int i = 0; i < 16; i++)
			{
				this.CreateRubyText();
			}
		}

		// Token: 0x060024CD RID: 9421 RVA: 0x000C60B8 File Offset: 0x000C44B8
		private void CreateRubyText()
		{
			Text text = UnityEngine.Object.Instantiate<Text>(this.m_RubyPrefab, this.m_RectTransform, false);
			text.gameObject.SetActive(false);
			this.m_RubyListPool.Add(text);
		}

		// Token: 0x060024CE RID: 9422 RVA: 0x000C60F0 File Offset: 0x000C44F0
		public Text ScoopRuby()
		{
			if (this.m_RubyListPool.Count <= 0)
			{
				this.CreateRubyText();
			}
			if (this.m_RubyListPool.Count <= 0)
			{
				return null;
			}
			Text text = this.m_RubyListPool[0];
			text.gameObject.SetActive(true);
			this.m_RubyListPool.Remove(text);
			this.m_ActiveRubyList.Add(text);
			return text;
		}

		// Token: 0x060024CF RID: 9423 RVA: 0x000C615C File Offset: 0x000C455C
		public void SinkRuby(Text obj)
		{
			obj.gameObject.SetActive(false);
			obj.rectTransform.SetParent(this.m_RectTransform, false);
			obj.color = this.m_RubyPrefab.color;
			this.m_ActiveRubyList.Remove(obj);
			this.m_RubyListPool.Add(obj);
		}

		// Token: 0x060024D0 RID: 9424 RVA: 0x000C61B1 File Offset: 0x000C45B1
		public void ClearRuby()
		{
			while (this.m_ActiveRubyList.Count > 0)
			{
				this.SinkRuby(this.m_ActiveRubyList[0]);
			}
		}

		// Token: 0x060024D1 RID: 9425 RVA: 0x000C61DC File Offset: 0x000C45DC
		public void Destroy()
		{
			this.ClearRuby();
			while (this.m_RubyListPool.Count > 0)
			{
				Text text = this.m_RubyListPool[0];
				text.gameObject.SetActive(false);
				this.m_RubyListPool.Remove(text);
				UnityEngine.Object.Destroy(text.gameObject);
			}
		}

		// Token: 0x04002BAB RID: 11179
		private const int DEFAULT_INSTANCE_NUM = 16;

		// Token: 0x04002BAC RID: 11180
		[SerializeField]
		private Text m_RubyPrefab;

		// Token: 0x04002BAD RID: 11181
		private List<Text> m_RubyListPool = new List<Text>();

		// Token: 0x04002BAE RID: 11182
		private List<Text> m_ActiveRubyList = new List<Text>();

		// Token: 0x04002BAF RID: 11183
		private RectTransform m_RectTransform;

		// Token: 0x04002BB0 RID: 11184
		private Color m_DefaultColor;
	}
}
