﻿using System;

namespace Star.UI.ADV
{
	// Token: 0x02000744 RID: 1860
	public enum ePenChildAnimationState
	{
		// Token: 0x04002B9C RID: 11164
		None,
		// Token: 0x04002B9D RID: 11165
		IdleUp,
		// Token: 0x04002B9E RID: 11166
		IdleDown,
		// Token: 0x04002B9F RID: 11167
		Decided,
		// Token: 0x04002BA0 RID: 11168
		DecideDown,
		// Token: 0x04002BA1 RID: 11169
		DecideUp
	}
}
