﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200001F RID: 31
	public class ADVNovel : UIGroup
	{
		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000B8 RID: 184 RVA: 0x00006716 File Offset: 0x00004B16
		public ADVNovelCaption LastCaption
		{
			get
			{
				return this.m_LastCaption;
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00006720 File Offset: 0x00004B20
		public void Setup()
		{
			this.m_Anim = base.GetComponent<AnimUIPlayer>();
			this.m_ADVNovelCaptionList = new List<ADVNovelCaption>();
			this.m_StartPosition = Vector2.zero;
			this.m_LastPosition = Vector2.zero;
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_LastCaption = null;
			this.SetAnchorPositionDefault();
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00006773 File Offset: 0x00004B73
		public override void Open()
		{
			if (this.m_Anim.State == 0 || this.m_Anim.State == 3)
			{
				base.Open();
			}
		}

		// Token: 0x060000BB RID: 187 RVA: 0x0000679C File Offset: 0x00004B9C
		public override void Close()
		{
			if (this.m_Anim.State == 2 || this.m_Anim.State == 1)
			{
				base.Close();
			}
		}

		// Token: 0x060000BC RID: 188 RVA: 0x000067C8 File Offset: 0x00004BC8
		public void SetAnchorPosition(eADVAnchor anchor, Vector2 pos)
		{
			pos.x = this.m_RectTransform.rect.width * pos.x;
			pos.y = -this.m_RectTransform.rect.height * pos.y;
			this.m_StartPosition = pos;
			this.m_LastPosition = pos;
			this.m_Anchor = anchor;
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00006830 File Offset: 0x00004C30
		public void SetAnchorPositionDefault()
		{
			Vector2 vector = new Vector2(0.1f, 0.1f);
			vector.x = this.m_RectTransform.rect.width * vector.x;
			vector.y = -this.m_RectTransform.rect.height * vector.y;
			this.m_StartPosition = vector;
			this.m_LastPosition = vector;
			this.m_Anchor = eADVAnchor.MiddleLeft;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x000068A8 File Offset: 0x00004CA8
		public void ClearText()
		{
			this.m_LastPosition = this.m_StartPosition;
			for (int i = 0; i < this.m_ADVNovelCaptionList.Count; i++)
			{
				this.m_ADVNovelCaptionList[i].ClearRuby();
				UnityEngine.Object.Destroy(this.m_ADVNovelCaptionList[i].gameObject);
			}
			this.m_ADVNovelCaptionList.Clear();
			this.m_LastCaption = null;
		}

		// Token: 0x060000BF RID: 191 RVA: 0x00006918 File Offset: 0x00004D18
		public ADVNovelCaption AddText()
		{
			ADVNovelCaption advnovelCaption = UnityEngine.Object.Instantiate<ADVNovelCaption>(this.m_CaptionPrefab, this.m_RectTransform, false);
			advnovelCaption.Setup(this.m_RubyPool);
			advnovelCaption.RectTransform.localScale = Vector2.one;
			if (this.m_LastCaption != null)
			{
				this.m_LastPosition.y = this.m_LastCaption.RectTransform.anchoredPosition.y - this.m_LastCaption.RectTransform.sizeDelta.y - 12f;
				advnovelCaption.IsNeedPositionAdjust = false;
			}
			else
			{
				advnovelCaption.IsNeedPositionAdjust = true;
			}
			advnovelCaption.SetPosition(this.m_LastPosition);
			advnovelCaption.SetAnchor(this.m_Anchor);
			advnovelCaption.SetText(string.Empty);
			this.m_ADVNovelCaptionList.Add(advnovelCaption);
			this.m_LastCaption = advnovelCaption;
			return this.m_LastCaption;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x000069FB File Offset: 0x00004DFB
		public void SetText(string text)
		{
			this.m_LastCaption.SetText(text);
		}

		// Token: 0x040000BC RID: 188
		private const float LINESPACE = 12f;

		// Token: 0x040000BD RID: 189
		[SerializeField]
		private ADVNovelCaption m_CaptionPrefab;

		// Token: 0x040000BE RID: 190
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x040000BF RID: 191
		private List<ADVNovelCaption> m_ADVNovelCaptionList;

		// Token: 0x040000C0 RID: 192
		private Vector2 m_StartPosition;

		// Token: 0x040000C1 RID: 193
		private Vector2 m_LastPosition;

		// Token: 0x040000C2 RID: 194
		private eADVAnchor m_Anchor;

		// Token: 0x040000C3 RID: 195
		private ADVNovelCaption m_LastCaption;

		// Token: 0x040000C4 RID: 196
		private RectTransform m_RectTransform;

		// Token: 0x040000C5 RID: 197
		private AnimUIPlayer m_Anim;
	}
}
