﻿using System;
using System.Collections.Generic;
using Star.ADV;

namespace Star.UI.ADV
{
	// Token: 0x0200073F RID: 1855
	public class ADVBackLogScrollItemData : ScrollItemData
	{
		// Token: 0x06002498 RID: 9368 RVA: 0x000C4228 File Offset: 0x000C2628
		public ADVBackLogScrollItemData()
		{
			this.m_ADVCharaID = string.Empty;
			this.m_Talker = string.Empty;
			this.m_Text = string.Empty;
			this.m_RubyList = null;
		}

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x0600249A RID: 9370 RVA: 0x000C426C File Offset: 0x000C266C
		// (set) Token: 0x06002499 RID: 9369 RVA: 0x000C4263 File Offset: 0x000C2663
		public string ADVCharaID
		{
			get
			{
				return this.m_ADVCharaID;
			}
			set
			{
				this.m_ADVCharaID = value;
			}
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x0600249C RID: 9372 RVA: 0x000C427D File Offset: 0x000C267D
		// (set) Token: 0x0600249B RID: 9371 RVA: 0x000C4274 File Offset: 0x000C2674
		public string Talker
		{
			get
			{
				return this.m_Talker;
			}
			set
			{
				this.m_Talker = value;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x0600249E RID: 9374 RVA: 0x000C428E File Offset: 0x000C268E
		// (set) Token: 0x0600249D RID: 9373 RVA: 0x000C4285 File Offset: 0x000C2685
		public string Text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				this.m_Text = value;
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x060024A0 RID: 9376 RVA: 0x000C429F File Offset: 0x000C269F
		// (set) Token: 0x0600249F RID: 9375 RVA: 0x000C4296 File Offset: 0x000C2696
		public List<ADVParser.RubyData> RubyList
		{
			get
			{
				return this.m_RubyList;
			}
			set
			{
				this.m_RubyList = value;
			}
		}

		// Token: 0x04002B88 RID: 11144
		private string m_ADVCharaID;

		// Token: 0x04002B89 RID: 11145
		private string m_Talker;

		// Token: 0x04002B8A RID: 11146
		private string m_Text;

		// Token: 0x04002B8B RID: 11147
		private List<ADVParser.RubyData> m_RubyList = new List<ADVParser.RubyData>();
	}
}
