﻿using System;
using System.Collections.Generic;
using Star.ADV;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x02000020 RID: 32
	public class ADVNovelCaption : MonoBehaviour
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000C2 RID: 194 RVA: 0x00006A27 File Offset: 0x00004E27
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000C3 RID: 195 RVA: 0x00006A2F File Offset: 0x00004E2F
		// (set) Token: 0x060000C4 RID: 196 RVA: 0x00006A37 File Offset: 0x00004E37
		public bool IsNeedPositionAdjust
		{
			get
			{
				return this.m_IsNeedPositionAdjust;
			}
			set
			{
				this.m_IsNeedPositionAdjust = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000C5 RID: 197 RVA: 0x00006A40 File Offset: 0x00004E40
		public ADVCalcText CalcTextObj
		{
			get
			{
				return this.m_CalcText;
			}
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00006A48 File Offset: 0x00004E48
		public void Setup(ADVRubyPool rubyPool)
		{
			this.m_RectTransform = base.GetComponent<RectTransform>();
			this.m_Width = new float[this.m_TextObj.Length];
			this.m_Height = new float[this.m_TextObj.Length];
			this.m_RubyPool = rubyPool;
			this.m_CalcText = base.GetComponent<ADVCalcText>();
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x00006A9A File Offset: 0x00004E9A
		private void Update()
		{
			if (this.m_Dirty)
			{
				this.UpdatePosition();
				this.m_Dirty = false;
			}
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x00006AB4 File Offset: 0x00004EB4
		private void UpdatePosition()
		{
			Vector2 vector = this.m_Pos;
			if (this.m_IsNeedPositionAdjust)
			{
				vector += new Vector2(0f, this.m_Height[0] * this.m_Pivot.y);
			}
			this.m_RectTransform.anchoredPosition = vector;
			vector = Vector2.zero;
			for (int i = 0; i < this.m_TextObj.Length; i++)
			{
				if (this.m_Height[i] == 0f)
				{
					break;
				}
				vector.x = -this.m_Pivot.x * this.m_Width[i];
				this.m_TextObj[i].rectTransform.localPosition = vector;
				vector.y -= this.m_Height[i];
			}
			this.m_RectTransform.sizeDelta = new Vector2(this.m_RectTransform.sizeDelta.x, -vector.y);
		}

		// Token: 0x060000C9 RID: 201 RVA: 0x00006BB0 File Offset: 0x00004FB0
		public void SetText(string text)
		{
			for (int i = 0; i < this.m_TextObj.Length; i++)
			{
				this.m_TextObj[i].text = string.Empty;
			}
			string text2 = text;
			for (int j = 0; j < this.m_TextObj.Length; j++)
			{
				int num = text2.IndexOf('\n');
				if (num == -1)
				{
					this.m_TextObj[j].text = text2;
					break;
				}
				this.m_TextObj[j].text = text2.Substring(0, num);
				text2 = text2.Remove(0, num + 1);
			}
		}

		// Token: 0x060000CA RID: 202 RVA: 0x00006C48 File Offset: 0x00005048
		public void CalcSize(string text)
		{
			string[] array = new string[this.m_TextObj.Length];
			for (int i = 0; i < this.m_TextObj.Length; i++)
			{
				array[i] = this.m_TextObj[i].text;
				this.m_Width[i] = 0f;
				this.m_Height[i] = 0f;
			}
			string text2 = text;
			for (int j = 0; j < this.m_TextObj.Length; j++)
			{
				int num = text2.IndexOf('\n');
				string text3;
				if (num == -1)
				{
					text3 = text2;
				}
				else
				{
					text3 = text2.Substring(0, num);
				}
				if (string.IsNullOrEmpty(text3))
				{
					break;
				}
				this.m_TextObj[j].text = text3;
				this.m_TextObj[j].GetComponent<ContentSizeFitter>().SetLayoutHorizontal();
				this.m_TextObj[j].GetComponent<ContentSizeFitter>().SetLayoutVertical();
				this.m_Width[j] = this.m_TextObj[j].rectTransform.rect.width;
				this.m_Height[j] = this.m_TextObj[j].rectTransform.rect.height;
				this.m_TextObj[j].text = array[j];
				if (num == -1)
				{
					break;
				}
				text2 = text2.Remove(0, num + 1);
			}
			this.m_Dirty = true;
		}

		// Token: 0x060000CB RID: 203 RVA: 0x00006DA9 File Offset: 0x000051A9
		public void SetPosition(Vector2 position)
		{
			this.m_Pos = position;
			this.m_RectTransform.anchoredPosition = position;
			this.m_Dirty = true;
		}

		// Token: 0x060000CC RID: 204 RVA: 0x00006DC8 File Offset: 0x000051C8
		public void SetAnchor(eADVAnchor anchor)
		{
			switch (anchor)
			{
			case eADVAnchor.UpperLeft:
				this.m_Pivot.x = 0f;
				this.m_Pivot.y = 1f;
				break;
			case eADVAnchor.UpperCenter:
				this.m_Pivot.x = 0.5f;
				this.m_Pivot.y = 1f;
				break;
			case eADVAnchor.UpperRight:
				this.m_Pivot.x = 1f;
				this.m_Pivot.y = 1f;
				break;
			case eADVAnchor.MiddleLeft:
				this.m_Pivot.x = 0f;
				this.m_Pivot.y = 0.5f;
				break;
			case eADVAnchor.MiddleCenter:
				this.m_Pivot.x = 0.5f;
				this.m_Pivot.y = 0.5f;
				break;
			case eADVAnchor.MiddleRight:
				this.m_Pivot.x = 1f;
				this.m_Pivot.y = 0.5f;
				break;
			case eADVAnchor.LowerLeft:
				this.m_Pivot.x = 0f;
				this.m_Pivot.y = 0f;
				break;
			case eADVAnchor.LowerCenter:
				this.m_Pivot.x = 0.5f;
				this.m_Pivot.y = 0f;
				break;
			case eADVAnchor.LowerRight:
				this.m_Pivot.x = 1f;
				this.m_Pivot.y = 0f;
				break;
			}
			this.m_Dirty = true;
		}

		// Token: 0x060000CD RID: 205 RVA: 0x00006F58 File Offset: 0x00005358
		public void AddRuby(ADVParser.RubyData rubyData)
		{
			this.m_RubyList.Add(rubyData);
			Text text = this.m_RubyPool.ScoopRuby();
			text.text = rubyData.m_Text;
			text.color = Color.white;
			RectTransform rectTransform = text.rectTransform;
			rectTransform.SetParent(rubyData.m_TextObj.rectTransform, false);
			rectTransform.anchoredPosition = (rubyData.m_StartPos + rubyData.m_EndPos) * 0.5f;
			this.m_RubyObjList.Add(text);
		}

		// Token: 0x060000CE RID: 206 RVA: 0x00006FDC File Offset: 0x000053DC
		public void ClearRuby()
		{
			this.m_RubyList.Clear();
			for (int i = 0; i < this.m_RubyObjList.Count; i++)
			{
				this.m_RubyPool.SinkRuby(this.m_RubyObjList[i]);
			}
			this.m_RubyObjList.Clear();
		}

		// Token: 0x060000CF RID: 207 RVA: 0x00007032 File Offset: 0x00005432
		public List<ADVParser.RubyData> GetRubyList()
		{
			return this.m_RubyList;
		}

		// Token: 0x040000C6 RID: 198
		[SerializeField]
		private Text[] m_TextObj;

		// Token: 0x040000C7 RID: 199
		[SerializeField]
		private RectTransform m_RectTransform;

		// Token: 0x040000C8 RID: 200
		private ADVCalcText m_CalcText;

		// Token: 0x040000C9 RID: 201
		private ADVRubyPool m_RubyPool;

		// Token: 0x040000CA RID: 202
		private List<ADVParser.RubyData> m_RubyList = new List<ADVParser.RubyData>();

		// Token: 0x040000CB RID: 203
		private List<Text> m_RubyObjList = new List<Text>();

		// Token: 0x040000CC RID: 204
		private Vector2 m_Pivot;

		// Token: 0x040000CD RID: 205
		private float[] m_Width;

		// Token: 0x040000CE RID: 206
		private float[] m_Height;

		// Token: 0x040000CF RID: 207
		private bool m_Dirty;

		// Token: 0x040000D0 RID: 208
		private Vector2 m_Pos;

		// Token: 0x040000D1 RID: 209
		private bool m_IsNeedPositionAdjust;
	}
}
