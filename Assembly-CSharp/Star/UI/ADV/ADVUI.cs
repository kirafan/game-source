﻿using System;
using System.Collections.Generic;
using Star.ADV;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x0200074C RID: 1868
	public class ADVUI : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		// Token: 0x1700026D RID: 621
		// (get) Token: 0x0600250D RID: 9485 RVA: 0x000C6B6B File Offset: 0x000C4F6B
		public bool IsRunning
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x0600250E RID: 9486 RVA: 0x000C6B6E File Offset: 0x000C4F6E
		public ADVTalkWindow talkWindow
		{
			get
			{
				return this.m_TalkWindow;
			}
		}

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x0600250F RID: 9487 RVA: 0x000C6B76 File Offset: 0x000C4F76
		public ADVNovel novel
		{
			get
			{
				return this.m_ADVNovel;
			}
		}

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06002510 RID: 9488 RVA: 0x000C6B7E File Offset: 0x000C4F7E
		public ADVSystemPanel systemPanel
		{
			get
			{
				return this.m_SystemPanel;
			}
		}

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06002511 RID: 9489 RVA: 0x000C6B86 File Offset: 0x000C4F86
		public ADVFade fade
		{
			get
			{
				return this.m_Fade;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06002512 RID: 9490 RVA: 0x000C6B8E File Offset: 0x000C4F8E
		public ADVUI.eTextMode TextMode
		{
			get
			{
				return this.m_TextMode;
			}
		}

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06002513 RID: 9491 RVA: 0x000C6B96 File Offset: 0x000C4F96
		// (set) Token: 0x06002514 RID: 9492 RVA: 0x000C6B9E File Offset: 0x000C4F9E
		public bool IsEnableInput
		{
			get
			{
				return this.m_IsEnableInput;
			}
			set
			{
				this.EnableInput(value);
			}
		}

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06002515 RID: 9493 RVA: 0x000C6BA7 File Offset: 0x000C4FA7
		// (set) Token: 0x06002516 RID: 9494 RVA: 0x000C6BAF File Offset: 0x000C4FAF
		public bool IsEnableRender
		{
			get
			{
				return this.m_IsEnableRender;
			}
			set
			{
				this.EnableRender(value);
			}
		}

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06002517 RID: 9495 RVA: 0x000C6BB8 File Offset: 0x000C4FB8
		public RectTransform RectTransform
		{
			get
			{
				return this.m_RectTransform;
			}
		}

		// Token: 0x06002518 RID: 9496 RVA: 0x000C6BC0 File Offset: 0x000C4FC0
		public void Setup(ADVParser parser)
		{
			this.m_TalkWindow.Setup();
			this.m_TalkWindow.SetupParser(parser);
			this.m_ADVNovel.Setup();
			this.m_BackLog.Setup();
			this.m_SkipLog.Setup();
			this.m_SkipLog.StartScrollPosition = 1f;
			this.m_BackLogItem = UnityEngine.Object.Instantiate<ADVBackLogItem>(this.m_BackLogItemPrefab);
			this.m_RectTransform = base.GetComponent<RectTransform>();
		}

		// Token: 0x06002519 RID: 9497 RVA: 0x000C6C32 File Offset: 0x000C5032
		public void Destroy()
		{
			this.m_TalkWindow.Destroy();
			this.m_BackLog.Destroy();
			this.m_SkipLog.Destroy();
		}

		// Token: 0x0600251A RID: 9498 RVA: 0x000C6C55 File Offset: 0x000C5055
		private void Update()
		{
			this.UpdateAndroidBackKey();
		}

		// Token: 0x0600251B RID: 9499 RVA: 0x000C6C60 File Offset: 0x000C5060
		private void UpdateAndroidBackKey()
		{
			if (Application.platform == RuntimePlatform.Android && Input.GetKeyDown(KeyCode.Escape))
			{
				if (InputTouch.Inst.GetAvailableTouchCnt() > 0)
				{
					return;
				}
				if (!this.IsIdle())
				{
					return;
				}
				if (!this.m_Player.IsPlay)
				{
					return;
				}
				if (this.m_Player.IsEnd)
				{
					return;
				}
				if (!this.IsEnableInput)
				{
					return;
				}
				if (!this.IsEnableRender)
				{
					this.m_Player.ToggleShowUI();
				}
				else
				{
					this.OpenSkipWindow();
				}
			}
		}

		// Token: 0x0600251C RID: 9500 RVA: 0x000C6CF1 File Offset: 0x000C50F1
		private bool IsIdle()
		{
			return !this.m_SkipWindow.IsOpen() && !this.m_BackLog.IsOpen() && !this.m_SkipLog.IsOpen();
		}

		// Token: 0x0600251D RID: 9501 RVA: 0x000C6D24 File Offset: 0x000C5124
		public void OnPointerClick(PointerEventData eventData)
		{
			if (!this.m_IsEnableInput)
			{
				return;
			}
			this.m_Player.Tap();
		}

		// Token: 0x0600251E RID: 9502 RVA: 0x000C6D3D File Offset: 0x000C513D
		public void EnableInput(bool flag)
		{
			this.m_IsEnableInput = flag;
			if (this.m_IsEnableInput)
			{
				this.m_BG.enabled = false;
			}
			else
			{
				this.m_BG.enabled = true;
			}
		}

		// Token: 0x0600251F RID: 9503 RVA: 0x000C6D6E File Offset: 0x000C516E
		public void EnableRender(bool flag)
		{
			this.m_IsEnableRender = flag;
			this.m_TalkWindow.gameObject.SetActive(this.m_IsEnableRender);
			this.m_SystemPanel.gameObject.SetActive(this.m_IsEnableRender);
		}

		// Token: 0x06002520 RID: 9504 RVA: 0x000C6DA3 File Offset: 0x000C51A3
		public void EnableAuto(bool flag, bool anim = false)
		{
			this.m_TalkWindow.EnableAuto(flag);
			this.EnableMenuPanelAutoAnimation(flag, !anim);
		}

		// Token: 0x06002521 RID: 9505 RVA: 0x000C6DBC File Offset: 0x000C51BC
		public void EnableAutoIcon(bool flag, bool anim = false)
		{
			this.m_TalkWindow.AutoIcon.SetActive(flag);
			this.EnableMenuPanelAutoAnimation(flag, !anim);
		}

		// Token: 0x06002522 RID: 9506 RVA: 0x000C6DDA File Offset: 0x000C51DA
		public void EnableMenuPanelAutoAnimation(bool flag, bool immediate)
		{
			if (flag)
			{
				this.systemPanel.Auto(immediate);
			}
			else
			{
				this.systemPanel.StopAuto(immediate);
			}
		}

		// Token: 0x06002523 RID: 9507 RVA: 0x000C6DFF File Offset: 0x000C51FF
		public void OpenBackLog()
		{
			this.m_BackLog.Open();
			this.EnableInput(false);
			this.m_Player.EnableAuto(false, true);
		}

		// Token: 0x06002524 RID: 9508 RVA: 0x000C6E20 File Offset: 0x000C5220
		public void CloseBackLog()
		{
			this.m_BackLog.Close();
			this.EnableInput(true);
		}

		// Token: 0x06002525 RID: 9509 RVA: 0x000C6E34 File Offset: 0x000C5234
		public void SetTextMode(ADVUI.eTextMode mode)
		{
			if (this.m_TextMode != mode)
			{
				ADVUI.eTextMode textMode = this.m_TextMode;
				if (textMode != ADVUI.eTextMode.TalkWindow)
				{
					if (textMode == ADVUI.eTextMode.Novel)
					{
						this.m_ADVNovel.Close();
					}
				}
				else
				{
					this.m_TalkWindow.Close();
				}
			}
			this.m_TextMode = mode;
		}

		// Token: 0x06002526 RID: 9510 RVA: 0x000C6E8D File Offset: 0x000C528D
		public void AddBackLogText(string ADVCharaID, string dispTalker, string text, List<ADVParser.RubyData> rubyList)
		{
			this.m_BackLog.AddText(ADVCharaID, dispTalker, text, rubyList);
		}

		// Token: 0x06002527 RID: 9511 RVA: 0x000C6EA0 File Offset: 0x000C52A0
		public void OpenSkipWindow()
		{
			this.EnableInput(false);
			if (!this.m_SkipWindow.IsOpenOrIn())
			{
				this.m_SkipWindow.Open();
			}
			this.m_SkipWindow.GetComponent<CanvasGroup>().blocksRaycasts = true;
			this.m_Player.EnableAuto(false, true);
		}

		// Token: 0x06002528 RID: 9512 RVA: 0x000C6EED File Offset: 0x000C52ED
		public void CloseSkipWindow()
		{
			this.EnableInput(true);
			this.m_SkipWindow.Close();
			this.m_SkipWindow.GetComponent<CanvasGroup>().blocksRaycasts = false;
		}

		// Token: 0x06002529 RID: 9513 RVA: 0x000C6F12 File Offset: 0x000C5312
		public void AddSkipLogText(string ADVCharaID, string dispTalker, string text, List<ADVParser.RubyData> rubyList)
		{
			this.m_SkipLog.AddText(ADVCharaID, dispTalker, text, rubyList);
		}

		// Token: 0x0600252A RID: 9514 RVA: 0x000C6F24 File Offset: 0x000C5324
		public void OpenSkipLog()
		{
			this.m_Player.CreateSkipLogText();
			this.m_SkipLog.Open();
			this.EnableInput(false);
			this.m_Player.EnableAuto(false, true);
			this.CloseSkipWindow();
			this.EnableInput(false);
			this.m_SkipLog.GetComponent<CanvasGroup>().blocksRaycasts = true;
		}

		// Token: 0x0600252B RID: 9515 RVA: 0x000C6F79 File Offset: 0x000C5379
		public void CloseSkipLog()
		{
			this.m_SkipLog.RemoveText();
			this.m_SkipLog.Close();
			this.EnableInput(false);
			this.OpenSkipWindow();
			this.m_SkipLog.GetComponent<CanvasGroup>().blocksRaycasts = false;
		}

		// Token: 0x0600252C RID: 9516 RVA: 0x000C6FAF File Offset: 0x000C53AF
		public void Skip()
		{
			this.CloseSkipLog();
			this.CloseSkipWindow();
			this.EnableInput(false);
			this.m_Player.SkipScript();
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x000C6FCF File Offset: 0x000C53CF
		public ADVCalcText GetBackLogCalcText()
		{
			return this.m_BackLogItem.GetCalcTextObj();
		}

		// Token: 0x0600252E RID: 9518 RVA: 0x000C6FDC File Offset: 0x000C53DC
		public Vector3 ConvertPositionTo3D(Vector2 pos)
		{
			pos.x /= this.m_RectTransform.sizeDelta.y;
			pos.y /= this.m_RectTransform.sizeDelta.y;
			return pos;
		}

		// Token: 0x0600252F RID: 9519 RVA: 0x000C7033 File Offset: 0x000C5433
		public void OnClickSkipWindowFooter()
		{
			if (this.m_SkipWindow.Window.LastPressedFooterButtonID == 0)
			{
				this.Skip();
			}
			else
			{
				this.CloseSkipWindow();
			}
		}

		// Token: 0x04002BC8 RID: 11208
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04002BC9 RID: 11209
		[SerializeField]
		private ADVTalkWindow m_TalkWindow;

		// Token: 0x04002BCA RID: 11210
		[SerializeField]
		private ADVNovel m_ADVNovel;

		// Token: 0x04002BCB RID: 11211
		[SerializeField]
		private ADVSystemPanel m_SystemPanel;

		// Token: 0x04002BCC RID: 11212
		[SerializeField]
		private CustomButton m_AutoButton;

		// Token: 0x04002BCD RID: 11213
		[SerializeField]
		private Image m_BG;

		// Token: 0x04002BCE RID: 11214
		[SerializeField]
		private ADVBackLog m_BackLog;

		// Token: 0x04002BCF RID: 11215
		[SerializeField]
		private UIGroup m_SkipWindow;

		// Token: 0x04002BD0 RID: 11216
		[SerializeField]
		private ADVBackLog m_SkipLog;

		// Token: 0x04002BD1 RID: 11217
		[SerializeField]
		private ADVFade m_Fade;

		// Token: 0x04002BD2 RID: 11218
		[SerializeField]
		private ADVRubyPool m_RubyPool;

		// Token: 0x04002BD3 RID: 11219
		private ADVUI.eTextMode m_TextMode;

		// Token: 0x04002BD4 RID: 11220
		private bool m_IsEnableInput = true;

		// Token: 0x04002BD5 RID: 11221
		private bool m_IsEnableRender = true;

		// Token: 0x04002BD6 RID: 11222
		[SerializeField]
		private ADVBackLogItem m_BackLogItemPrefab;

		// Token: 0x04002BD7 RID: 11223
		[SerializeField]
		private ADVBackLogItem m_BackLogItem;

		// Token: 0x04002BD8 RID: 11224
		private RectTransform m_RectTransform;

		// Token: 0x0200074D RID: 1869
		public enum eTextMode
		{
			// Token: 0x04002BDA RID: 11226
			TalkWindow,
			// Token: 0x04002BDB RID: 11227
			Novel
		}
	}
}
