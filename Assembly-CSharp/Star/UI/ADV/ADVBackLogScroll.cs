﻿using System;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02000740 RID: 1856
	public class ADVBackLogScroll : ScrollViewBase
	{
		// Token: 0x17000263 RID: 611
		// (get) Token: 0x060024A2 RID: 9378 RVA: 0x000C56F0 File Offset: 0x000C3AF0
		public ADVPlayer Player
		{
			get
			{
				return this.m_Player;
			}
		}

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x060024A3 RID: 9379 RVA: 0x000C56F8 File Offset: 0x000C3AF8
		public ADVRubyPool RubyPool
		{
			get
			{
				return this.m_RubyPool;
			}
		}

		// Token: 0x060024A4 RID: 9380 RVA: 0x000C5700 File Offset: 0x000C3B00
		public override void Destroy()
		{
			base.Destroy();
			base.Refresh();
			for (int i = 0; i < this.m_DispItemList.Count; i++)
			{
				UnityEngine.Object.Destroy(this.m_DispItemList[i].m_ItemIconObject.gameObject);
			}
			this.m_IsDirty = true;
		}

		// Token: 0x04002B8C RID: 11148
		[SerializeField]
		private ADVPlayer m_Player;

		// Token: 0x04002B8D RID: 11149
		[SerializeField]
		private ADVRubyPool m_RubyPool;
	}
}
