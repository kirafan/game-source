﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.ADV
{
	// Token: 0x0200073E RID: 1854
	public class ADVBackLogItem : ScrollItemIcon
	{
		// Token: 0x0600248E RID: 9358 RVA: 0x000C3B51 File Offset: 0x000C1F51
		protected override void Awake()
		{
			base.Awake();
			this.m_BodySpriteHandler = null;
			this.m_FaceSpriteHandler = null;
			this.m_ADVCharaID = string.Empty;
		}

		// Token: 0x0600248F RID: 9359 RVA: 0x000C3B74 File Offset: 0x000C1F74
		protected override void ApplyData()
		{
			if (this.m_TextRectTransform == null)
			{
				this.m_TextRectTransform = this.m_TextObj.GetComponent<RectTransform>();
			}
			ADVBackLogScrollItemData advbackLogScrollItemData = (ADVBackLogScrollItemData)this.m_Data;
			this.m_TalkerTextObj.text = advbackLogScrollItemData.Talker;
			this.m_TextObj.text = advbackLogScrollItemData.Text;
			this.ClearRuby();
			for (int i = 0; i < advbackLogScrollItemData.RubyList.Count; i++)
			{
				this.AddRuby(advbackLogScrollItemData.RubyList[i].m_Text, advbackLogScrollItemData.RubyList[i].m_StartPos, advbackLogScrollItemData.RubyList[i].m_EndPos);
			}
		}

		// Token: 0x06002490 RID: 9360 RVA: 0x000C3C2C File Offset: 0x000C202C
		public void AddRuby(string text, Vector2 startPos, Vector2 endPos)
		{
			ADVBackLogScroll advbackLogScroll = (ADVBackLogScroll)this.m_Scroll;
			Text text2 = advbackLogScroll.RubyPool.ScoopRuby();
			text2.text = text;
			text2.color = Color.white;
			RectTransform rectTransform = text2.rectTransform;
			rectTransform.SetParent(this.m_TextRectTransform, false);
			rectTransform.anchoredPosition = (startPos + endPos) * 0.5f;
			this.m_RubyList.Add(text2);
		}

		// Token: 0x06002491 RID: 9361 RVA: 0x000C3C9C File Offset: 0x000C209C
		public void ClearRuby()
		{
			ADVBackLogScroll advbackLogScroll = (ADVBackLogScroll)this.m_Scroll;
			for (int i = 0; i < this.m_RubyList.Count; i++)
			{
				advbackLogScroll.RubyPool.SinkRuby(this.m_RubyList[i]);
			}
			this.m_RubyList.Clear();
		}

		// Token: 0x06002492 RID: 9362 RVA: 0x000C3CF4 File Offset: 0x000C20F4
		protected void Update()
		{
			ADVBackLogScrollItemData advbackLogScrollItemData = (ADVBackLogScrollItemData)this.m_Data;
			if (advbackLogScrollItemData == null)
			{
				return;
			}
			if (this.m_ADVCharaID != advbackLogScrollItemData.ADVCharaID)
			{
				this.m_ADVCharaID = advbackLogScrollItemData.ADVCharaID;
				this.Load();
			}
			else if (this.m_BodyImage.sprite == null && this.IsBodyAvailable() && (this.m_FaceSpriteHandler == null || this.m_FaceSpriteHandler.IsAvailable()))
			{
				this.m_BodyImage.gameObject.SetActive(true);
				if (this.m_BodySpriteHandler != null && this.m_BodySpriteHandler.IsAvailable())
				{
					this.m_BodyImage.sprite = this.m_BodySpriteHandler.Obj;
				}
				if (this.m_FaceSpriteHandler != null && this.m_FaceSpriteHandler.IsAvailable())
				{
					this.m_FaceImage.gameObject.SetActive(true);
					this.m_FaceImage.sprite = this.m_FaceSpriteHandler.Obj;
				}
				ADVCharacterListDB_Param advcharacterParam = ((ADVBackLogScroll)this.m_Scroll).Player.GetADVCharacterParam(this.m_ADVCharaID);
				if (advcharacterParam.m_Datas[0].m_FaceReferenceImageType == 0)
				{
					this.m_BodyImage.rectTransform.anchoredPosition = new Vector2(-(-this.m_BodyImage.sprite.rect.width * 0.5f + advcharacterParam.m_Datas[0].m_FaceX), -(this.m_BodyImage.sprite.rect.height - advcharacterParam.m_Datas[0].m_FaceY) * 0.5f);
				}
				else
				{
					this.m_BodyImage.rectTransform.anchoredPosition = new Vector2(-(advcharacterParam.m_Datas[0].m_FacePicX - this.m_FaceImage.sprite.rect.width * advcharacterParam.m_Datas[0].m_FacePivotX + advcharacterParam.m_Datas[0].m_FaceX), -(advcharacterParam.m_Datas[0].m_FacePicY + this.m_FaceImage.sprite.rect.height * (1f - advcharacterParam.m_Datas[0].m_FacePivotY) - advcharacterParam.m_Datas[0].m_FaceY)) * 0.5f;
				}
			}
		}

		// Token: 0x06002493 RID: 9363 RVA: 0x000C3F7E File Offset: 0x000C237E
		private void OnDestroy()
		{
			this.Destroy();
		}

		// Token: 0x06002494 RID: 9364 RVA: 0x000C3F88 File Offset: 0x000C2388
		public override void Destroy()
		{
			base.Destroy();
			this.m_BodyImage.gameObject.SetActive(false);
			this.m_BodyImage.sprite = null;
			this.m_FaceImage.gameObject.SetActive(false);
			this.m_FaceImage.sprite = null;
			if (this.m_BodySpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_BodySpriteHandler);
				this.m_BodySpriteHandler = null;
			}
			if (this.m_FaceSpriteHandler != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_FaceSpriteHandler);
				this.m_FaceSpriteHandler = null;
			}
		}

		// Token: 0x06002495 RID: 9365 RVA: 0x000C4024 File Offset: 0x000C2424
		private void Load()
		{
			this.Destroy();
			if (string.IsNullOrEmpty(this.m_ADVCharaID))
			{
				return;
			}
			ADVCharacterListDB_Param advcharacterParam = ((ADVBackLogScroll)this.m_Scroll).Player.GetADVCharacterParam(this.m_ADVCharaID);
			if (advcharacterParam.m_ResourceBaseName != null && advcharacterParam.m_ResourceBaseName != string.Empty)
			{
				if ((int)advcharacterParam.m_Datas[0].m_DummyStandPic == 0)
				{
					this.m_BodySpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVStandPic(advcharacterParam.m_ResourceBaseName, 0);
				}
				if (advcharacterParam.m_Datas[0].m_FacePattern != -1)
				{
					this.m_FaceSpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncADVFace(advcharacterParam.m_ResourceBaseName, advcharacterParam.m_Datas[0].m_FacePattern, "Default");
					this.m_FaceImage.rectTransform.pivot = new Vector2(advcharacterParam.m_Datas[0].m_FacePivotX, advcharacterParam.m_Datas[0].m_FacePivotY);
					this.m_FaceImage.rectTransform.anchoredPosition = new Vector2(advcharacterParam.m_Datas[0].m_FacePicX, -advcharacterParam.m_Datas[0].m_FacePicY);
				}
			}
		}

		// Token: 0x06002496 RID: 9366 RVA: 0x000C417C File Offset: 0x000C257C
		private bool IsBodyAvailable()
		{
			return !string.IsNullOrEmpty(this.m_ADVCharaID) && ((int)((ADVBackLogScroll)this.m_Scroll).Player.GetADVCharacterParam(this.m_ADVCharaID).m_Datas[0].m_DummyStandPic != 0 || (this.m_BodySpriteHandler != null && this.m_BodySpriteHandler.IsAvailable()));
		}

		// Token: 0x06002497 RID: 9367 RVA: 0x000C41EF File Offset: 0x000C25EF
		public ADVCalcText GetCalcTextObj()
		{
			return this.m_CalcText;
		}

		// Token: 0x04002B7D RID: 11133
		[SerializeField]
		private Image m_BodyImage;

		// Token: 0x04002B7E RID: 11134
		[SerializeField]
		private Image m_FaceImage;

		// Token: 0x04002B7F RID: 11135
		[SerializeField]
		private RectTransform m_MaskImage;

		// Token: 0x04002B80 RID: 11136
		[SerializeField]
		private Text m_TalkerTextObj;

		// Token: 0x04002B81 RID: 11137
		[SerializeField]
		private Text m_TextObj;

		// Token: 0x04002B82 RID: 11138
		[SerializeField]
		private ADVCalcText m_CalcText;

		// Token: 0x04002B83 RID: 11139
		private RectTransform m_TextRectTransform;

		// Token: 0x04002B84 RID: 11140
		private List<Text> m_RubyList = new List<Text>();

		// Token: 0x04002B85 RID: 11141
		private string m_ADVCharaID = string.Empty;

		// Token: 0x04002B86 RID: 11142
		private SpriteHandler m_BodySpriteHandler;

		// Token: 0x04002B87 RID: 11143
		private SpriteHandler m_FaceSpriteHandler;
	}
}
