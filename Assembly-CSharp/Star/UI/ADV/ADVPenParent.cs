﻿using System;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02000743 RID: 1859
	public class ADVPenParent : MonoBehaviour
	{
		// Token: 0x060024AF RID: 9391 RVA: 0x000C5A8E File Offset: 0x000C3E8E
		public void Setup()
		{
			this.m_Pen.Setup();
			this.m_PenShadow.Setup();
			this.SetPenActive(true);
		}

		// Token: 0x060024B0 RID: 9392 RVA: 0x000C5AAD File Offset: 0x000C3EAD
		private void Update()
		{
		}

		// Token: 0x060024B1 RID: 9393 RVA: 0x000C5AB0 File Offset: 0x000C3EB0
		public void SetPenActive(bool active)
		{
			this.m_IsPenActive = active;
			if (this.m_Pen != null)
			{
				this.m_Pen.SetActive(this.m_IsPenActive);
			}
			if (this.m_PenShadow != null)
			{
				this.m_PenShadow.SetActive(this.m_IsPenActive);
			}
		}

		// Token: 0x060024B2 RID: 9394 RVA: 0x000C5B08 File Offset: 0x000C3F08
		public void OnClickScreen()
		{
			if (this.m_Pen != null)
			{
				this.m_Pen.OnClickScreen();
			}
			if (this.m_PenShadow != null)
			{
				this.m_PenShadow.OnClickScreen();
			}
		}

		// Token: 0x04002B98 RID: 11160
		private bool m_IsPenActive = true;

		// Token: 0x04002B99 RID: 11161
		[SerializeField]
		private ADVPenParentChildPen m_Pen;

		// Token: 0x04002B9A RID: 11162
		[SerializeField]
		private ADVPenParentChildPenShadow m_PenShadow;
	}
}
