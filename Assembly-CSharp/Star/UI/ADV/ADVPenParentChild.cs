﻿using System;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02000745 RID: 1861
	public class ADVPenParentChild<iTweenWrapperType> : MonoBehaviour where iTweenWrapperType : iTweenWrapper
	{
		// Token: 0x060024B4 RID: 9396 RVA: 0x000C5B4A File Offset: 0x000C3F4A
		public virtual void Setup()
		{
			this.m_RectTransform = this.m_iTweenWrapper.GetComponent<RectTransform>();
		}

		// Token: 0x060024B5 RID: 9397 RVA: 0x000C5B64 File Offset: 0x000C3F64
		private void Update()
		{
			if (this.m_RectTransform == null)
			{
				return;
			}
			if (this.m_iTweenWrapper != null && !this.m_iTweenWrapper.IsPlaying())
			{
				if (this.m_State == ePenChildAnimationState.None || this.m_State == ePenChildAnimationState.IdleDown)
				{
					this.ChangeStateIdleUp();
				}
				else if (this.m_State == ePenChildAnimationState.IdleUp || this.m_State == ePenChildAnimationState.DecideUp)
				{
					this.ChangeStateIdleDown();
				}
				else if (this.m_State == ePenChildAnimationState.Decided)
				{
					this.ChangeStateDecideDown();
				}
				else if (this.m_State == ePenChildAnimationState.DecideDown)
				{
					this.ChangeStateDecideUp();
				}
			}
		}

		// Token: 0x060024B6 RID: 9398 RVA: 0x000C5C1D File Offset: 0x000C401D
		public virtual void ChangeStateIdleUp()
		{
		}

		// Token: 0x060024B7 RID: 9399 RVA: 0x000C5C1F File Offset: 0x000C401F
		public virtual void ChangeStateIdleDown()
		{
		}

		// Token: 0x060024B8 RID: 9400 RVA: 0x000C5C21 File Offset: 0x000C4021
		public virtual void ChangeStateDecided()
		{
		}

		// Token: 0x060024B9 RID: 9401 RVA: 0x000C5C23 File Offset: 0x000C4023
		public virtual void ChangeStateDecideDown()
		{
		}

		// Token: 0x060024BA RID: 9402 RVA: 0x000C5C25 File Offset: 0x000C4025
		public virtual void ChangeStateDecideUp()
		{
		}

		// Token: 0x060024BB RID: 9403 RVA: 0x000C5C27 File Offset: 0x000C4027
		public void SetActive(bool active)
		{
			if (this.m_RectTransform != null)
			{
				this.m_RectTransform.gameObject.SetActive(active);
			}
		}

		// Token: 0x060024BC RID: 9404 RVA: 0x000C5C4B File Offset: 0x000C404B
		public void OnClickScreen()
		{
			this.ChangeStateDecided();
		}

		// Token: 0x04002BA2 RID: 11170
		protected ePenChildAnimationState m_State;

		// Token: 0x04002BA3 RID: 11171
		[SerializeField]
		protected iTweenWrapperType m_iTweenWrapper;

		// Token: 0x04002BA4 RID: 11172
		protected RectTransform m_RectTransform;
	}
}
