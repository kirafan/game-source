﻿using System;
using System.Collections.Generic;
using Star.ADV;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x0200073D RID: 1853
	public class ADVBackLog : UIGroup
	{
		// Token: 0x1700025E RID: 606
		// (set) Token: 0x06002483 RID: 9347 RVA: 0x000C384E File Offset: 0x000C1C4E
		public float StartScrollPosition
		{
			set
			{
				this.m_StartScrollPosition = value;
			}
		}

		// Token: 0x06002484 RID: 9348 RVA: 0x000C3857 File Offset: 0x000C1C57
		public void Setup()
		{
			this.m_Scroll.Setup();
		}

		// Token: 0x06002485 RID: 9349 RVA: 0x000C3864 File Offset: 0x000C1C64
		public override void Open()
		{
			if (this.m_WindowAnim.State != 0 && this.m_WindowAnim.State != 3)
			{
				return;
			}
			base.Open();
			this.m_Scroll.Refresh();
		}

		// Token: 0x06002486 RID: 9350 RVA: 0x000C3899 File Offset: 0x000C1C99
		public override void Close()
		{
			if (this.m_WindowAnim.State != 2 && this.m_WindowAnim.State != 1)
			{
				return;
			}
			base.Close();
		}

		// Token: 0x06002487 RID: 9351 RVA: 0x000C38C4 File Offset: 0x000C1CC4
		public void Destroy()
		{
			this.m_Scroll.Destroy();
		}

		// Token: 0x06002488 RID: 9352 RVA: 0x000C38D4 File Offset: 0x000C1CD4
		public override void Update()
		{
			base.Update();
			if (this.m_WindowAnim != null && this.m_WindowAnim.State == 1)
			{
				this.m_Scroll.SetScrollNormalizedPosition(this.m_StartScrollPosition);
			}
			this.UpdateAndroidBackKey();
		}

		// Token: 0x06002489 RID: 9353 RVA: 0x000C3920 File Offset: 0x000C1D20
		private void UpdateAndroidBackKey()
		{
			UIUtility.UpdateAndroidBackKey(this.m_CloseButton, new Func<bool>(this.CheckUpdateAndroidBackKey));
		}

		// Token: 0x0600248A RID: 9354 RVA: 0x000C3939 File Offset: 0x000C1D39
		private bool CheckUpdateAndroidBackKey()
		{
			return base.IsIdle();
		}

		// Token: 0x0600248B RID: 9355 RVA: 0x000C394C File Offset: 0x000C1D4C
		public void AddText(string ADVCharaID, string talker, string str, List<ADVParser.RubyData> rubyList)
		{
			ADVBackLogScrollItemData advbackLogScrollItemData = new ADVBackLogScrollItemData();
			advbackLogScrollItemData.ADVCharaID = ADVCharaID;
			advbackLogScrollItemData.Talker = talker;
			advbackLogScrollItemData.Text = str;
			advbackLogScrollItemData.RubyList = rubyList;
			this.m_Scroll.AddItem(advbackLogScrollItemData);
		}

		// Token: 0x0600248C RID: 9356 RVA: 0x000C3988 File Offset: 0x000C1D88
		public void RemoveText()
		{
			this.m_Scroll.RemoveAll();
		}

		// Token: 0x04002B79 RID: 11129
		[SerializeField]
		private ADVBackLogScroll m_Scroll;

		// Token: 0x04002B7A RID: 11130
		[SerializeField]
		private AnimUIPlayer m_WindowAnim;

		// Token: 0x04002B7B RID: 11131
		[SerializeField]
		private CustomButton m_CloseButton;

		// Token: 0x04002B7C RID: 11132
		private float m_StartScrollPosition;
	}
}
