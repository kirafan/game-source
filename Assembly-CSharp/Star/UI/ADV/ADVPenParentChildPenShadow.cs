﻿using System;
using UnityEngine;

namespace Star.UI.ADV
{
	// Token: 0x02000747 RID: 1863
	public class ADVPenParentChildPenShadow : ADVPenParentChild<iTweenScaleToWrapper>
	{
		// Token: 0x060024C5 RID: 9413 RVA: 0x000C5E77 File Offset: 0x000C4277
		public override void Setup()
		{
			base.Setup();
			this.m_BeforeLocalScale = this.m_RectTransform.localScale;
			this.m_AfterLocalScale = this.m_BeforeLocalScale;
			this.m_AfterLocalScale.x = this.m_AfterLocalScaleX;
		}

		// Token: 0x060024C6 RID: 9414 RVA: 0x000C5EB0 File Offset: 0x000C42B0
		public override void ChangeStateIdleUp()
		{
			this.m_RectTransform.localScale = this.m_BeforeLocalScale;
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 1.2f,
				easeType = iTween.EaseType.easeInOutQuad
			}.AddXParameter(this.m_AfterLocalScale.x));
			this.m_State = ePenChildAnimationState.IdleUp;
		}

		// Token: 0x060024C7 RID: 9415 RVA: 0x000C5F1C File Offset: 0x000C431C
		public override void ChangeStateIdleDown()
		{
			this.m_RectTransform.localScale = this.m_AfterLocalScale;
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 1.2f,
				easeType = iTween.EaseType.easeInOutQuad
			}.AddXParameter(this.m_BeforeLocalScale.x));
			this.m_State = ePenChildAnimationState.IdleDown;
		}

		// Token: 0x060024C8 RID: 9416 RVA: 0x000C5F87 File Offset: 0x000C4387
		public override void ChangeStateDecided()
		{
			this.m_RectTransform.localScale = this.m_AfterLocalScale;
			this.m_iTweenWrapper.Stop();
			this.m_State = ePenChildAnimationState.Decided;
		}

		// Token: 0x060024C9 RID: 9417 RVA: 0x000C5FAC File Offset: 0x000C43AC
		public override void ChangeStateDecideDown()
		{
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 0.05f,
				easeType = iTween.EaseType.linear
			}.AddXParameter(this.m_BeforeLocalScale.x));
			this.m_State = ePenChildAnimationState.DecideDown;
		}

		// Token: 0x060024CA RID: 9418 RVA: 0x000C6008 File Offset: 0x000C4408
		public override void ChangeStateDecideUp()
		{
			this.m_iTweenWrapper.Play(new iTweenWrapperPlayArgument(this.m_RectTransform.gameObject)
			{
				isLocal = true,
				time = 0.05f,
				easeType = iTween.EaseType.linear
			}.AddXParameter(this.m_AfterLocalScale.x));
			this.m_State = ePenChildAnimationState.DecideUp;
		}

		// Token: 0x04002BA8 RID: 11176
		private Vector3 m_BeforeLocalScale;

		// Token: 0x04002BA9 RID: 11177
		private Vector3 m_AfterLocalScale;

		// Token: 0x04002BAA RID: 11178
		[SerializeField]
		[Tooltip("X伸縮量.")]
		private float m_AfterLocalScaleX;
	}
}
