﻿using System;
using Star.UI.Window;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star.UI
{
	// Token: 0x020008BE RID: 2238
	[RequireComponent(typeof(CustomButtonGroup))]
	[RequireComponent(typeof(CanvasGroup))]
	public class UIGroup : MonoBehaviour
	{
		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06002E5F RID: 11871 RVA: 0x00005930 File Offset: 0x00003D30
		public UIGroup.eState State
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x06002E60 RID: 11872 RVA: 0x00005938 File Offset: 0x00003D38
		public bool IsOpen()
		{
			return this.m_State != UIGroup.eState.Hide;
		}

		// Token: 0x06002E61 RID: 11873 RVA: 0x00005946 File Offset: 0x00003D46
		public bool IsIdle()
		{
			return this.m_State == UIGroup.eState.Idle;
		}

		// Token: 0x06002E62 RID: 11874 RVA: 0x00005951 File Offset: 0x00003D51
		public bool IsOpenOrIn()
		{
			return this.m_State == UIGroup.eState.In || this.m_State == UIGroup.eState.Idle;
		}

		// Token: 0x06002E63 RID: 11875 RVA: 0x0000596B File Offset: 0x00003D6B
		public bool IsHideOrOut()
		{
			return this.m_State == UIGroup.eState.Hide || this.m_State == UIGroup.eState.Out;
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06002E64 RID: 11876 RVA: 0x00005984 File Offset: 0x00003D84
		public bool IsPlayingInOut
		{
			get
			{
				return this.m_State == UIGroup.eState.In || this.m_State == UIGroup.eState.Out;
			}
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06002E65 RID: 11877 RVA: 0x0000599E File Offset: 0x00003D9E
		public bool IsDonePlayIn
		{
			get
			{
				return this.m_State == UIGroup.eState.Idle;
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06002E66 RID: 11878 RVA: 0x000059A9 File Offset: 0x00003DA9
		public bool IsDonePlayOut
		{
			get
			{
				return this.m_State == UIGroup.eState.Hide;
			}
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06002E67 RID: 11879 RVA: 0x000059B4 File Offset: 0x00003DB4
		public CustomButtonGroup CustomButtonGroup
		{
			get
			{
				if (this.m_CustomButtonGroup == null)
				{
					this.m_CustomButtonGroup = base.GetComponent<CustomButtonGroup>();
				}
				return this.m_CustomButtonGroup;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06002E68 RID: 11880 RVA: 0x000059D9 File Offset: 0x00003DD9
		public CanvasGroup CanvasGroup
		{
			get
			{
				if (this.m_CanvasGroup == null)
				{
					this.m_CanvasGroup = base.GetComponent<CanvasGroup>();
				}
				return this.m_CanvasGroup;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06002E69 RID: 11881 RVA: 0x000059FE File Offset: 0x00003DFE
		public GameObject GameObject
		{
			get
			{
				if (this.m_GameObject == null)
				{
					this.m_GameObject = base.gameObject;
				}
				return this.m_GameObject;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06002E6A RID: 11882 RVA: 0x00005A23 File Offset: 0x00003E23
		public int LastPressedFooterButtonID
		{
			get
			{
				if (this.m_CustomWindow == null)
				{
					return -1;
				}
				return this.m_CustomWindow.LastPressedFooterButtonID;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06002E6B RID: 11883 RVA: 0x00005A43 File Offset: 0x00003E43
		public CustomWindow Window
		{
			get
			{
				return this.m_CustomWindow;
			}
		}

		// Token: 0x1400006F RID: 111
		// (add) Token: 0x06002E6C RID: 11884 RVA: 0x00005A4C File Offset: 0x00003E4C
		// (remove) Token: 0x06002E6D RID: 11885 RVA: 0x00005A84 File Offset: 0x00003E84
		public event Action OnIdle;

		// Token: 0x14000070 RID: 112
		// (add) Token: 0x06002E6E RID: 11886 RVA: 0x00005ABC File Offset: 0x00003EBC
		// (remove) Token: 0x06002E6F RID: 11887 RVA: 0x00005AF4 File Offset: 0x00003EF4
		public event Action OnClose;

		// Token: 0x14000071 RID: 113
		// (add) Token: 0x06002E70 RID: 11888 RVA: 0x00005B2C File Offset: 0x00003F2C
		// (remove) Token: 0x06002E71 RID: 11889 RVA: 0x00005B64 File Offset: 0x00003F64
		public event Action OnEnd;

		// Token: 0x06002E72 RID: 11890 RVA: 0x00005B9C File Offset: 0x00003F9C
		public virtual void PrepareStart()
		{
			this.m_State = UIGroup.eState.Prepare;
			if (this.m_AnimsPrepareStart != null)
			{
				for (int i = 0; i < this.m_AnimsPrepareStart.Length; i++)
				{
					if (!(this.m_AnimsPrepareStart[i] == null))
					{
						this.m_AnimsPrepareStart[i].PlayIn();
					}
				}
			}
			if (this.m_WindowStatus.m_ContentObj != null)
			{
				this.m_WindowStatus.m_ContentObj.gameObject.SetActive(false);
			}
		}

		// Token: 0x06002E73 RID: 11891 RVA: 0x00005C28 File Offset: 0x00004028
		public virtual void Open()
		{
			if (this.m_WindowStatus.m_ContentObj != null)
			{
				this.m_WindowStatus.m_ContentObj.gameObject.SetActive(true);
			}
			if (this.m_CustomWindow == null && this.m_CustomWindowPrefab != null)
			{
				this.m_CustomWindow = UnityEngine.Object.Instantiate<CustomWindow>(this.m_CustomWindowPrefab, base.GetComponent<RectTransform>());
				this.m_CustomWindow.RectTransform.localPosition = Vector2.zero;
				this.m_CustomWindow.RectTransform.localScale = Vector3.one;
				this.m_CustomWindow.RectTransform.sizeDelta = Vector2.zero;
				if (this.m_CustomWindow.BGDark != null)
				{
					this.m_CustomWindow.BGDark.GetComponent<Image>().enabled = this.m_WindowStatus.m_BGDark;
				}
				this.m_CustomWindow.SetSortOrder(this.m_WindowStatus.m_SortOrderTypeID, this.m_WindowStatus.m_SortOrderOffset);
				if (this.m_WindowStatus.m_FitWindowRect != null)
				{
					this.m_CustomWindow.WindowRectTransform.localPosition = this.m_WindowStatus.m_FitWindowRect.localPosition;
					this.m_CustomWindow.WindowRectTransform.sizeDelta = this.m_WindowStatus.m_FitWindowRect.sizeDelta;
				}
				if (this.m_WindowStatus.m_TitleText != null)
				{
					this.m_CustomWindow.SetTitle(this.m_WindowStatus.m_TitleText.text);
					this.m_WindowStatus.m_TitleText.gameObject.SetActive(false);
				}
				else
				{
					this.m_CustomWindow.SetTitle(null);
				}
				this.m_CustomWindow.SetContent(this.m_WindowStatus.m_ContentObj);
				switch (this.m_WindowStatus.m_FooterType)
				{
				case UIGroup.InstantiateWindowStatus.eFooterType.OK:
					this.m_CustomWindow.SetFooterOK();
					break;
				case UIGroup.InstantiateWindowStatus.eFooterType.Close:
					this.m_CustomWindow.SetFooterClose();
					break;
				case UIGroup.InstantiateWindowStatus.eFooterType.YesNo:
					this.m_CustomWindow.SetFooterYesNo();
					break;
				case UIGroup.InstantiateWindowStatus.eFooterType.Cancel:
					this.m_CustomWindow.SetFooterCancel();
					break;
				}
				this.m_CustomWindow.OnClickFooterButton += this.OnClickFooterButtonCallBack;
			}
			this.GameObject.SetActive(true);
			if (this.m_CustomWindow != null)
			{
				this.m_CustomWindow.Open();
			}
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				if (!(this.m_Anims[i] == null))
				{
					this.m_Anims[i].PlayIn();
				}
			}
			this.OpenChangeStateProcess();
		}

		// Token: 0x06002E74 RID: 11892 RVA: 0x00005EE5 File Offset: 0x000042E5
		protected virtual void OpenChangeStateProcess()
		{
			this.SetEnableInput(false);
			this.m_State = UIGroup.eState.In;
			if (this.m_InputBlock)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, true);
			}
		}

		// Token: 0x06002E75 RID: 11893 RVA: 0x00005F18 File Offset: 0x00004318
		public virtual void Close()
		{
			this.GameObject.SetActive(true);
			if (this.m_CustomWindow != null)
			{
				this.m_CustomWindow.Close();
			}
			if (this.m_AnimsPrepareStart != null)
			{
				for (int i = 0; i < this.m_AnimsPrepareStart.Length; i++)
				{
					if (!(this.m_AnimsPrepareStart[i] == null))
					{
						this.m_AnimsPrepareStart[i].PlayOut();
					}
				}
			}
			for (int j = 0; j < this.m_Anims.Length; j++)
			{
				if (!(this.m_Anims[j] == null))
				{
					this.m_Anims[j].PlayOut();
				}
			}
			this.SetEnableInput(false);
			this.OnClose.Call();
			this.m_State = UIGroup.eState.Out;
			if (this.m_InputBlock)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, true);
			}
		}

		// Token: 0x06002E76 RID: 11894 RVA: 0x00006010 File Offset: 0x00004410
		public virtual void Hide()
		{
			for (int i = 0; i < this.m_AnimsPrepareStart.Length; i++)
			{
				if (!(this.m_AnimsPrepareStart[i] == null))
				{
					this.m_AnimsPrepareStart[i].Hide();
				}
			}
			for (int j = 0; j < this.m_Anims.Length; j++)
			{
				if (!(this.m_Anims[j] == null))
				{
					this.m_Anims[j].Hide();
				}
			}
			this.m_State = UIGroup.eState.Hide;
			if (this.m_InputBlock)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, false);
			}
		}

		// Token: 0x06002E77 RID: 11895 RVA: 0x000060C4 File Offset: 0x000044C4
		public virtual void Update()
		{
			switch (this.m_State)
			{
			case UIGroup.eState.Hide:
				if (this.m_AutoDisableInHide && this.GameObject.activeSelf)
				{
					this.GameObject.SetActive(false);
				}
				break;
			case UIGroup.eState.In:
				if (this.CheckInEnd())
				{
					this.m_State = UIGroup.eState.Idle;
					this.OnFinishPlayIn();
					if (this.m_AutoEnableInput)
					{
						this.SetEnableInput(true);
					}
					this.OnIdle.Call();
				}
				break;
			case UIGroup.eState.Out:
				if (this.CheckOutEnd())
				{
					this.m_State = UIGroup.eState.WaitFinalize;
					this.OnFinishPlayOut();
				}
				break;
			case UIGroup.eState.WaitFinalize:
				if (this.IsCompleteFinalize())
				{
					this.m_State = UIGroup.eState.Hide;
					this.OnFinishFinalize();
				}
				break;
			}
		}

		// Token: 0x06002E78 RID: 11896 RVA: 0x0000619B File Offset: 0x0000459B
		public virtual void LateUpdate()
		{
			if (this.m_State == UIGroup.eState.Hide && this.m_AutoDisableInHide && this.GameObject.activeSelf)
			{
				this.GameObject.SetActive(false);
			}
		}

		// Token: 0x06002E79 RID: 11897 RVA: 0x000061D0 File Offset: 0x000045D0
		private void OnDestroy()
		{
			this.m_CustomWindowPrefab = null;
			if (this.m_CustomWindow != null)
			{
				UnityEngine.Object.Destroy(this.m_CustomWindow);
				this.m_CustomWindow = null;
			}
			if (SingletonMonoBehaviour<GameSystem>.Inst != null && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, false);
			}
			this.m_CustomWindowPrefab = null;
		}

		// Token: 0x06002E7A RID: 11898 RVA: 0x00006243 File Offset: 0x00004643
		public virtual void OnFinishPlayIn()
		{
			if (this.m_InputBlock)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, false);
			}
		}

		// Token: 0x06002E7B RID: 11899 RVA: 0x00006266 File Offset: 0x00004666
		public virtual void OnFinishPlayOut()
		{
		}

		// Token: 0x06002E7C RID: 11900 RVA: 0x00006268 File Offset: 0x00004668
		public virtual bool IsCompleteFinalize()
		{
			return true;
		}

		// Token: 0x06002E7D RID: 11901 RVA: 0x0000626C File Offset: 0x0000466C
		public virtual void OnFinishFinalize()
		{
			if (this.m_AutoDisableInHide)
			{
				this.GameObject.SetActive(false);
			}
			if (this.m_InputBlock)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.InputBlock.SetBlockObj(this.GameObject, false);
			}
			this.OnEnd.Call();
		}

		// Token: 0x06002E7E RID: 11902 RVA: 0x000062BC File Offset: 0x000046BC
		public void SetEnableInput(bool flg)
		{
			this.m_IsEnableInputSelf = flg;
			if (this.m_IsEnableInputSelf && this.m_IsEnableInputParent)
			{
				this.CustomButtonGroup.IsEnableInput = true;
				this.CanvasGroup.blocksRaycasts = true;
				if (this.m_EnableGlobalUI)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetEnableInputFromUIGroup(this, true);
				}
			}
			else if (this.m_EnableGlobalUI)
			{
				this.CustomButtonGroup.IsEnableInput = false;
				this.CanvasGroup.blocksRaycasts = false;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetEnableInputFromUIGroup(this, false);
			}
		}

		// Token: 0x06002E7F RID: 11903 RVA: 0x00006354 File Offset: 0x00004754
		public void SetEnableInputParent(bool flg)
		{
			this.m_IsEnableInputParent = flg;
			if (this.m_IsEnableInputSelf && this.m_IsEnableInputParent)
			{
				this.CustomButtonGroup.IsEnableInput = true;
				this.CanvasGroup.blocksRaycasts = true;
				if (this.m_EnableGlobalUI)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetEnableInputFromUIGroup(this, true);
				}
			}
			else
			{
				this.CustomButtonGroup.IsEnableInput = false;
				this.CanvasGroup.blocksRaycasts = false;
				if (this.m_EnableGlobalUI)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetEnableInputFromUIGroup(this, false);
				}
			}
		}

		// Token: 0x06002E80 RID: 11904 RVA: 0x000063EB File Offset: 0x000047EB
		public bool IsEnableInput()
		{
			return this.m_IsEnableInputParent && this.m_IsEnableInputSelf;
		}

		// Token: 0x06002E81 RID: 11905 RVA: 0x00006404 File Offset: 0x00004804
		protected void AddPlayAnim(AnimUIPlayer anim)
		{
			int num = -1;
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				if (this.m_Anims[i] == anim)
				{
					return;
				}
				if (num == -1 && this.m_Anims[i] == null)
				{
					num = i;
				}
			}
			if (num == -1)
			{
				AnimUIPlayer[] array = new AnimUIPlayer[this.m_Anims.Length + 1];
				for (int j = 0; j < this.m_Anims.Length; j++)
				{
					array[j] = this.m_Anims[j];
				}
				this.m_Anims = array;
				this.m_Anims[this.m_Anims.Length - 1] = anim;
			}
			else
			{
				this.m_Anims[num] = anim;
			}
		}

		// Token: 0x06002E82 RID: 11906 RVA: 0x000064C0 File Offset: 0x000048C0
		protected void RemovePlayAnim(AnimUIPlayer anim)
		{
			for (int i = 0; i < this.m_Anims.Length; i++)
			{
				if (this.m_Anims[i] == anim)
				{
					this.m_Anims[i] = null;
					return;
				}
			}
		}

		// Token: 0x06002E83 RID: 11907 RVA: 0x00006504 File Offset: 0x00004904
		protected virtual bool CheckInEnd()
		{
			bool flag = true;
			if (this.m_CustomWindow != null && this.m_CustomWindow.IsPlayingAnim())
			{
				flag = false;
			}
			if (flag && this.m_AnimsPrepareStart != null)
			{
				for (int i = 0; i < this.m_AnimsPrepareStart.Length; i++)
				{
					if (!(this.m_AnimsPrepareStart[i] == null))
					{
						if (this.m_AnimsPrepareStart[i].State == 1)
						{
							flag = false;
							break;
						}
					}
				}
			}
			if (flag && this.m_Anims != null)
			{
				for (int j = 0; j < this.m_Anims.Length; j++)
				{
					if (!(this.m_Anims[j] == null))
					{
						if (this.m_Anims[j].State == 1)
						{
							flag = false;
							break;
						}
					}
				}
			}
			return flag;
		}

		// Token: 0x06002E84 RID: 11908 RVA: 0x000065F0 File Offset: 0x000049F0
		protected virtual bool CheckOutEnd()
		{
			bool flag = true;
			if (this.m_CustomWindow != null && this.m_CustomWindow.IsPlayingAnim())
			{
				flag = false;
			}
			if (flag && this.m_AnimsPrepareStart != null)
			{
				for (int i = 0; i < this.m_AnimsPrepareStart.Length; i++)
				{
					if (!(this.m_AnimsPrepareStart[i] == null))
					{
						if (this.m_AnimsPrepareStart[i].State == 3)
						{
							flag = false;
							break;
						}
					}
				}
			}
			if (flag && this.m_Anims != null)
			{
				for (int j = 0; j < this.m_Anims.Length; j++)
				{
					if (!(this.m_Anims[j] == null))
					{
						if (this.m_Anims[j].isActiveAndEnabled && this.m_Anims[j].State == 3)
						{
							flag = false;
							break;
						}
					}
				}
			}
			return flag;
		}

		// Token: 0x06002E85 RID: 11909 RVA: 0x000066ED File Offset: 0x00004AED
		private void OnClickFooterButtonCallBack()
		{
			this.m_WindowStatus.m_OnClickFooterButton.Invoke();
		}

		// Token: 0x04003589 RID: 13705
		[SerializeField]
		[Tooltip("PlayIn終了時に自動的に入力を有効化する")]
		private bool m_AutoEnableInput = true;

		// Token: 0x0400358A RID: 13706
		[SerializeField]
		[Tooltip("InOut時に全入力を阻害する")]
		private bool m_InputBlock = true;

		// Token: 0x0400358B RID: 13707
		[SerializeField]
		[Tooltip("入力有効時に同時にGlobalUIも有効にする")]
		private bool m_EnableGlobalUI;

		// Token: 0x0400358C RID: 13708
		[SerializeField]
		[Tooltip("PrepareStart実行時に再生されるAnim")]
		protected AnimUIPlayer[] m_AnimsPrepareStart;

		// Token: 0x0400358D RID: 13709
		[SerializeField]
		[Tooltip("Open時に再生される")]
		protected AnimUIPlayer[] m_Anims;

		// Token: 0x0400358E RID: 13710
		[SerializeField]
		[Tooltip("実行時生成する場合指定")]
		protected CustomWindow m_CustomWindowPrefab;

		// Token: 0x0400358F RID: 13711
		[SerializeField]
		[Tooltip("シーン内に既にある場合指定")]
		protected CustomWindow m_CustomWindow;

		// Token: 0x04003590 RID: 13712
		[SerializeField]
		protected UIGroup.InstantiateWindowStatus m_WindowStatus;

		// Token: 0x04003591 RID: 13713
		private UIGroup.eState m_State;

		// Token: 0x04003592 RID: 13714
		private CustomButtonGroup m_CustomButtonGroup;

		// Token: 0x04003593 RID: 13715
		private CanvasGroup m_CanvasGroup;

		// Token: 0x04003594 RID: 13716
		private bool m_IsEnableInputSelf;

		// Token: 0x04003595 RID: 13717
		private bool m_IsEnableInputParent = true;

		// Token: 0x04003596 RID: 13718
		protected bool m_AutoDisableInHide = true;

		// Token: 0x04003597 RID: 13719
		private GameObject m_GameObject;

		// Token: 0x020008BF RID: 2239
		[Serializable]
		public class InstantiateWindowStatus
		{
			// Token: 0x0400359B RID: 13723
			[SerializeField]
			[Tooltip("描画順の設定")]
			public UIDefine.eSortOrderTypeID m_SortOrderTypeID = UIDefine.eSortOrderTypeID.UniqueWindow;

			// Token: 0x0400359C RID: 13724
			[SerializeField]
			[Tooltip("描画順の設定にオフセットをかける")]
			public int m_SortOrderOffset;

			// Token: 0x0400359D RID: 13725
			[SerializeField]
			[Tooltip("後ろを暗くするかどうか")]
			public bool m_BGDark;

			// Token: 0x0400359E RID: 13726
			[SerializeField]
			[Tooltip("指定したRectTransformにウィンドウサイズをあわせる")]
			public RectTransform m_FitWindowRect;

			// Token: 0x0400359F RID: 13727
			[SerializeField]
			[Tooltip("指定したTextのtextをタイトルに表示")]
			public Text m_TitleText;

			// Token: 0x040035A0 RID: 13728
			[SerializeField]
			[Tooltip("指定したRectTransformをウィンドウ内へ移動")]
			public RectTransform m_ContentObj;

			// Token: 0x040035A1 RID: 13729
			[SerializeField]
			public UIGroup.InstantiateWindowStatus.eFooterType m_FooterType;

			// Token: 0x040035A2 RID: 13730
			[SerializeField]
			public UnityEvent m_OnClickFooterButton;

			// Token: 0x020008C0 RID: 2240
			public enum eFooterType
			{
				// Token: 0x040035A4 RID: 13732
				None,
				// Token: 0x040035A5 RID: 13733
				OK,
				// Token: 0x040035A6 RID: 13734
				Close,
				// Token: 0x040035A7 RID: 13735
				YesNo,
				// Token: 0x040035A8 RID: 13736
				Cancel
			}
		}

		// Token: 0x020008C1 RID: 2241
		public enum eState
		{
			// Token: 0x040035AA RID: 13738
			Hide,
			// Token: 0x040035AB RID: 13739
			Prepare,
			// Token: 0x040035AC RID: 13740
			In,
			// Token: 0x040035AD RID: 13741
			Idle,
			// Token: 0x040035AE RID: 13742
			Out,
			// Token: 0x040035AF RID: 13743
			WaitFinalize
		}
	}
}
