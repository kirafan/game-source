﻿using System;
using Star.UI.Edit;
using UnityEngine;

namespace Star.UI
{
	// Token: 0x02000972 RID: 2418
	public abstract class EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> : EnumerationPanelScrollItemIconBase where ThisType : EnumerationPanelScrollItemIconBaseExGen<ThisType, CharaPanelType, BasedDataType, CoreType, SetupArgumentType> where CharaPanelType : EnumerationCharaPanelScrollItemIconAdapterBase where BasedDataType : EnumerationPanelData where CoreType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType> where SetupArgumentType : EnumerationPanelScrollItemIconCoreGen<ThisType, CharaPanelType, BasedDataType, SetupArgumentType>.SharedInstanceEx<SetupArgumentType>, new()
	{
		// Token: 0x06003204 RID: 12804
		public abstract CoreType CreateCore();

		// Token: 0x06003205 RID: 12805 RVA: 0x000FFAF8 File Offset: 0x000FDEF8
		public virtual SetupArgumentType CreateArgument()
		{
			if (this.m_CharaPanelPrefab != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelPrefab = this.m_CharaPanelPrefab;
			}
			if (this.m_CharaPanelParent != null)
			{
				this.m_SerializedConstructInstances.m_CharaPanelParent = this.m_CharaPanelParent;
			}
			SetupArgumentType setupArgumentType = Activator.CreateInstance<SetupArgumentType>();
			setupArgumentType.parent = (ThisType)((object)this);
			return setupArgumentType.Clone(this.m_SerializedConstructInstances);
		}

		// Token: 0x06003206 RID: 12806 RVA: 0x000FFB82 File Offset: 0x000FDF82
		public virtual void Setup()
		{
			this.m_Core = this.CreateCore();
			this.m_SharedInstance = this.CreateArgument();
			this.m_Core.Setup(this.m_SharedInstance);
		}

		// Token: 0x06003207 RID: 12807 RVA: 0x000FFBB3 File Offset: 0x000FDFB3
		public virtual void Destroy()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Destroy();
			}
		}

		// Token: 0x06003208 RID: 12808 RVA: 0x000FFBD6 File Offset: 0x000FDFD6
		private void Update()
		{
			if (this.m_Core != null)
			{
				this.m_Core.Update();
			}
		}

		// Token: 0x06003209 RID: 12809 RVA: 0x000FFBF9 File Offset: 0x000FDFF9
		public void SetEditMode(EnumerationPanelCoreBase.eMode mode)
		{
			if (this.m_Core != null)
			{
				this.m_Core.SetEditMode(mode);
			}
		}

		// Token: 0x0600320A RID: 12810 RVA: 0x000FFC1D File Offset: 0x000FE01D
		public virtual void Apply(EnumerationPanelData data)
		{
			if (this.m_Core != null)
			{
				this.m_Core.Apply(data);
			}
		}

		// Token: 0x04003828 RID: 14376
		private const int INSTANTIATE_PER_FRAME = 6;

		// Token: 0x04003829 RID: 14377
		[SerializeField]
		[Tooltip("パネル内キャラクタ情報部の元Prefabリソース.")]
		protected CharaPanelType m_CharaPanelPrefab;

		// Token: 0x0400382A RID: 14378
		[SerializeField]
		[Tooltip("生成したCloneの親.")]
		protected RectTransform m_CharaPanelParent;

		// Token: 0x0400382B RID: 14379
		protected PartyEditUIBase.eMode m_Mode;

		// Token: 0x0400382C RID: 14380
		protected BasedDataType m_PartyData;

		// Token: 0x0400382D RID: 14381
		protected int m_CharaPanelNum;

		// Token: 0x0400382E RID: 14382
		[SerializeField]
		protected SetupArgumentType m_SerializedConstructInstances;

		// Token: 0x0400382F RID: 14383
		private CoreType m_Core;

		// Token: 0x04003830 RID: 14384
		protected SetupArgumentType m_SharedInstance;
	}
}
