﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Filter
{
	// Token: 0x02000846 RID: 2118
	public class SortWindow : UIGroup
	{
		// Token: 0x14000066 RID: 102
		// (add) Token: 0x06002C1B RID: 11291 RVA: 0x000E91DC File Offset: 0x000E75DC
		// (remove) Token: 0x06002C1C RID: 11292 RVA: 0x000E9214 File Offset: 0x000E7614
		public event Action OnExecute;

		// Token: 0x06002C1D RID: 11293 RVA: 0x000E924C File Offset: 0x000E764C
		public void Setup(UISettings.eUsingType_Sort usingType)
		{
			this.m_UsingType = usingType;
			UISettings.eUsingType_Sort usingType2 = this.m_UsingType;
			if (usingType2 != UISettings.eUsingType_Sort.CharaListView)
			{
				if (usingType2 != UISettings.eUsingType_Sort.WeaponList)
				{
					if (usingType2 == UISettings.eUsingType_Sort.SupportSelect)
					{
						this.SetupButtons(this.SUPPORT_SORT, 4);
					}
				}
				else
				{
					this.SetupButtons(this.WEAPONLIST_SORT, 5);
				}
			}
			else
			{
				this.SetupButtons(this.CHARALIST_SORT, 5);
			}
			this.SetOrderValue(SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(usingType));
			this.SetOrderType((UISettings.eSortOrderType)SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamOrderType(usingType));
		}

		// Token: 0x06002C1E RID: 11294 RVA: 0x000E92E4 File Offset: 0x000E76E4
		public void SetupButtons(eText_CommonDB[] keyNames, int columnMax)
		{
			string[] array = new string[keyNames.Length];
			for (int i = 0; i < keyNames.Length; i++)
			{
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(keyNames[i]);
			}
			this.SetupButtons(array, columnMax);
		}

		// Token: 0x06002C1F RID: 11295 RVA: 0x000E932C File Offset: 0x000E772C
		public void SetupButtons(string[] keyNames, int columnMax = 5)
		{
			for (int i = 0; i < keyNames.Length; i++)
			{
				RectTransform parent = this.m_ButtonParent;
				if (columnMax <= i)
				{
					parent = this.m_ButtonParentLow;
				}
				FilterButton filterButton = UnityEngine.Object.Instantiate<FilterButton>(this.m_ButtonPrefab, parent);
				filterButton.Idx = i;
				filterButton.GetComponentInChildren<Text>().text = keyNames[i];
				filterButton.OnClick += this.OnClickSortKeyButtonCallBack;
				this.m_OrderKeyButtonList.Add(filterButton);
			}
			for (int j = 0; j < 2; j++)
			{
				FilterButton filterButton2 = UnityEngine.Object.Instantiate<FilterButton>(this.m_ButtonPrefab, this.m_OrderParent);
				filterButton2.Idx = j;
				filterButton2.GetComponentInChildren<Text>().text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.SortOrderASC + j);
				filterButton2.OnClick += this.OnClickSortOrderTypeButtonCallBack;
				this.m_OrderTypeButtonList.Add(filterButton2);
			}
			this.SetOrderValue(0);
		}

		// Token: 0x06002C20 RID: 11296 RVA: 0x000E9418 File Offset: 0x000E7818
		public ushort GetSelectSortKey()
		{
			return this.m_SortOrderValue;
		}

		// Token: 0x06002C21 RID: 11297 RVA: 0x000E9420 File Offset: 0x000E7820
		public UISettings.eSortOrderType GetSortOrderType()
		{
			return this.m_SortOrderType;
		}

		// Token: 0x06002C22 RID: 11298 RVA: 0x000E9428 File Offset: 0x000E7828
		public void SetOrderValue(ushort idx)
		{
			this.m_SortOrderValue = idx;
			for (int i = 0; i < this.m_OrderKeyButtonList.Count; i++)
			{
				this.m_OrderKeyButtonList[i].SetDecided((int)this.m_SortOrderValue == i);
			}
		}

		// Token: 0x06002C23 RID: 11299 RVA: 0x000E9474 File Offset: 0x000E7874
		public void SetOrderType(UISettings.eSortOrderType orderType)
		{
			this.m_SortOrderType = orderType;
			for (int i = 0; i < this.m_OrderTypeButtonList.Count; i++)
			{
				this.m_OrderTypeButtonList[i].SetDecided(this.m_SortOrderType == (UISettings.eSortOrderType)i);
			}
		}

		// Token: 0x06002C24 RID: 11300 RVA: 0x000E94BE File Offset: 0x000E78BE
		public void SetHeaderText(string header)
		{
		}

		// Token: 0x06002C25 RID: 11301 RVA: 0x000E94C0 File Offset: 0x000E78C0
		public override void Open()
		{
			if (this.m_CustomWindow == null)
			{
				this.m_WindowStatus.m_FooterType = UIGroup.InstantiateWindowStatus.eFooterType.None;
			}
			base.Open();
		}

		// Token: 0x06002C26 RID: 11302 RVA: 0x000E94E5 File Offset: 0x000E78E5
		public override void Close()
		{
			base.Close();
		}

		// Token: 0x06002C27 RID: 11303 RVA: 0x000E94F0 File Offset: 0x000E78F0
		public void OnClickDecideButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetSortParamOrderType(this.m_UsingType, this.GetSortOrderType());
			SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetSortParamValue(this.m_UsingType, this.GetSelectSortKey());
			this.OnExecute.Call();
			this.Close();
		}

		// Token: 0x06002C28 RID: 11304 RVA: 0x000E9544 File Offset: 0x000E7944
		public void OnClickCancelButtonCallBack()
		{
			this.Close();
		}

		// Token: 0x06002C29 RID: 11305 RVA: 0x000E954C File Offset: 0x000E794C
		public void OnClickDefaultButtonCallBack()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetSortDefault(this.m_UsingType);
		}

		// Token: 0x06002C2A RID: 11306 RVA: 0x000E9563 File Offset: 0x000E7963
		private void OnClickSortKeyButtonCallBack(int idx)
		{
			this.SetOrderValue((ushort)idx);
		}

		// Token: 0x06002C2B RID: 11307 RVA: 0x000E956D File Offset: 0x000E796D
		private void OnClickSortOrderTypeButtonCallBack(int idx)
		{
			this.SetOrderType((UISettings.eSortOrderType)idx);
		}

		// Token: 0x040032ED RID: 13037
		private readonly eText_CommonDB[] CHARALIST_SORT = new eText_CommonDB[]
		{
			eText_CommonDB.SortOrderLv,
			eText_CommonDB.SortOrderRare,
			eText_CommonDB.SortOrderCost,
			eText_CommonDB.SortOrderTitle,
			eText_CommonDB.SortOrderGetTime,
			eText_CommonDB.SortOrderHp,
			eText_CommonDB.SortOrderAtk,
			eText_CommonDB.SortOrderMgc,
			eText_CommonDB.SortOrderDef,
			eText_CommonDB.SortOrderMDef,
			eText_CommonDB.SortOrderSpd
		};

		// Token: 0x040032EE RID: 13038
		private readonly eText_CommonDB[] WEAPONLIST_SORT = new eText_CommonDB[]
		{
			eText_CommonDB.SortOrderLv,
			eText_CommonDB.SortOrderRare,
			eText_CommonDB.SortOrderCost,
			eText_CommonDB.SortOrderHaveNum,
			eText_CommonDB.SortOrderGetTime,
			eText_CommonDB.SortOrderAtk,
			eText_CommonDB.SortOrderMgc,
			eText_CommonDB.SortOrderDef,
			eText_CommonDB.SortOrderMDef
		};

		// Token: 0x040032EF RID: 13039
		private readonly eText_CommonDB[] SUPPORT_SORT = new eText_CommonDB[]
		{
			eText_CommonDB.SortOrderLv,
			eText_CommonDB.SortOrderRare,
			eText_CommonDB.SortOrderCost,
			eText_CommonDB.SortOrderHp,
			eText_CommonDB.SortOrderAtk,
			eText_CommonDB.SortOrderMgc,
			eText_CommonDB.SortOrderDef,
			eText_CommonDB.SortOrderMDef,
			eText_CommonDB.SortOrderSpd
		};

		// Token: 0x040032F0 RID: 13040
		[SerializeField]
		private FilterButton m_ButtonPrefab;

		// Token: 0x040032F1 RID: 13041
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x040032F2 RID: 13042
		[SerializeField]
		private RectTransform m_ButtonParentLow;

		// Token: 0x040032F3 RID: 13043
		[SerializeField]
		private RectTransform m_OrderParent;

		// Token: 0x040032F4 RID: 13044
		private List<FilterButton> m_OrderKeyButtonList = new List<FilterButton>();

		// Token: 0x040032F5 RID: 13045
		private List<FilterButton> m_OrderTypeButtonList = new List<FilterButton>();

		// Token: 0x040032F6 RID: 13046
		private ushort m_SortOrderValue;

		// Token: 0x040032F7 RID: 13047
		private UISettings.eSortOrderType m_SortOrderType;

		// Token: 0x040032F9 RID: 13049
		private UISettings.eUsingType_Sort m_UsingType = UISettings.eUsingType_Sort.None;

		// Token: 0x02000847 RID: 2119
		public enum eSortOrderType
		{
			// Token: 0x040032FB RID: 13051
			Ascending,
			// Token: 0x040032FC RID: 13052
			Descending,
			// Token: 0x040032FD RID: 13053
			Num
		}
	}
}
