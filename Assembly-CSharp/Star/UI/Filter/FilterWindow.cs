﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Filter
{
	// Token: 0x0200083C RID: 2108
	public class FilterWindow : UIGroup
	{
		// Token: 0x14000065 RID: 101
		// (add) Token: 0x06002BF9 RID: 11257 RVA: 0x000E857C File Offset: 0x000E697C
		// (remove) Token: 0x06002BFA RID: 11258 RVA: 0x000E85B4 File Offset: 0x000E69B4
		public event Action OnExecute;

		// Token: 0x06002BFB RID: 11259 RVA: 0x000E85EC File Offset: 0x000E69EC
		public void Setup(UISettings.eUsingType_Filter usingType)
		{
			this.m_UsingType = usingType;
			UISettings.eFilterCategory[] categories = SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetUsingFilterMap(usingType).m_Categories;
			for (int i = 0; i < categories.Length; i++)
			{
				switch (categories[i])
				{
				case UISettings.eFilterCategory.Class:
					this.AddClassFilterType();
					break;
				case UISettings.eFilterCategory.Rare:
					this.AddRareFilterType();
					break;
				case UISettings.eFilterCategory.Element:
					this.AddElementFilterType();
					break;
				case UISettings.eFilterCategory.Title:
					this.AddTitleFilterType();
					break;
				case UISettings.eFilterCategory.DeadLine:
					this.AddDeadlineFilterType();
					break;
				case UISettings.eFilterCategory.PresentType:
					this.AddPresentTypeFilterType();
					break;
				case UISettings.eFilterCategory.StatusPriority:
					this.AddStatusPriorityFilterType();
					break;
				case UISettings.eFilterCategory.TitleSelect:
					this.AddTitleSelectType();
					break;
				}
				eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
				bool[] array = new bool[SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterCategoryMax(categories[i])];
				int num = 0;
				if (categories[i] == UISettings.eFilterCategory.Title || categories[i] == UISettings.eFilterCategory.TitleSelect)
				{
					num = array.Length - eTitleTypeConverter.HowManyTitles();
				}
				for (int j = 0; j < array.Length; j++)
				{
					int bitIndex = j;
					if (categories[i] == UISettings.eFilterCategory.Title || categories[i] == UISettings.eFilterCategory.TitleSelect)
					{
						if (num < j)
						{
							bitIndex = (int)(eTitleTypeConverter.ConvertFromOrderToTitleType(j - num) + num);
						}
					}
					else if (categories[i] == UISettings.eFilterCategory.Element)
					{
						bitIndex = (int)UIUtility.ElementSortIdxToEnum(j);
					}
					array[j] = SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlag(this.m_UsingType, i, bitIndex);
				}
				this.SetFilter(i, SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagIsAll(this.m_UsingType, i), array);
			}
			switch (this.m_UsingType)
			{
			case UISettings.eUsingType_Filter.CharaListView:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterHeaderChara));
				break;
			case UISettings.eUsingType_Filter.WeaponList:
			case UISettings.eUsingType_Filter.WeaponEquip:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterHeaderWeapon));
				break;
			case UISettings.eUsingType_Filter.SupportSelect:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterHeaderSupport));
				break;
			case UISettings.eUsingType_Filter.SupportTitle:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterHeaderSelectTitle));
				break;
			case UISettings.eUsingType_Filter.Present:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterHeaderPresent));
				break;
			case UISettings.eUsingType_Filter.Recommend:
				this.SetHeaderText(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterRecommendParty));
				break;
			}
		}

		// Token: 0x06002BFC RID: 11260 RVA: 0x000E8884 File Offset: 0x000E6C84
		public void AddType(FilterWindow.FilterTypeData type)
		{
			this.m_TypeList.Add(type);
			FilterTypeCell filterTypeCell;
			if (type.m_ContentTitleMode)
			{
				filterTypeCell = UnityEngine.Object.Instantiate<FilterTypeCell>(this.m_CellContentTitlePrefab, this.m_Scroll.content);
			}
			else
			{
				filterTypeCell = UnityEngine.Object.Instantiate<FilterTypeCell>(this.m_CellPrefab, this.m_Scroll.content);
			}
			filterTypeCell.Setup(type.m_CellMode, type.m_eTextCommon, type.m_ButtonNames, type.m_ContentTitleMode);
			this.m_CellList.Add(filterTypeCell);
			this.m_IsDirty = true;
		}

		// Token: 0x06002BFD RID: 11261 RVA: 0x000E890C File Offset: 0x000E6D0C
		public void AddElementFilterType()
		{
			string[] array = new string[6];
			for (int i = 0; i < UIDefine.ELEMENT_SORT.Length; i++)
			{
				eElementType eElementType = UIUtility.ElementSortIdxToEnum(i);
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ElementFire + (int)eElementType);
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Filter, false, eText_CommonDB.Element, array));
		}

		// Token: 0x06002BFE RID: 11262 RVA: 0x000E896C File Offset: 0x000E6D6C
		public void AddRareFilterType()
		{
			string[] array = new string[5];
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			for (int i = 0; i < 5; i++)
			{
				stringBuilder.Append("☆");
				array[i] = stringBuilder.ToString();
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Filter, false, eText_CommonDB.Rare, array));
		}

		// Token: 0x06002BFF RID: 11263 RVA: 0x000E89C8 File Offset: 0x000E6DC8
		public void AddClassFilterType()
		{
			string[] array = new string[5];
			for (int i = 0; i < 5; i++)
			{
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.ClassFighter + i);
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Filter, false, eText_CommonDB.Class, array));
		}

		// Token: 0x06002C00 RID: 11264 RVA: 0x000E8A1A File Offset: 0x000E6E1A
		public void AddTitleFilterType()
		{
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Filter, true, eText_CommonDB.ContentTitle, new eTitleTypeConverter().Setup().GetOrderTitles()));
		}

		// Token: 0x06002C01 RID: 11265 RVA: 0x000E8A3D File Offset: 0x000E6E3D
		public void AddTitleChoiceType()
		{
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Choice, true, eText_CommonDB.ContentTitle, new eTitleTypeConverter().Setup().GetOrderTitles()));
		}

		// Token: 0x06002C02 RID: 11266 RVA: 0x000E8A60 File Offset: 0x000E6E60
		public void AddDeadlineFilterType()
		{
			string[] array = new string[2];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.PresentFilterExistDeadLine + i);
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.FilterSingleSelect, false, eText_CommonDB.FilterDeadline, array));
		}

		// Token: 0x06002C03 RID: 11267 RVA: 0x000E8AB4 File Offset: 0x000E6EB4
		public void AddPresentTypeFilterType()
		{
			string[] array = new string[5];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(this.FILTER_LABEL_COMMONTYPE[i]);
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Filter, false, eText_CommonDB.FilterPresentType, array));
		}

		// Token: 0x06002C04 RID: 11268 RVA: 0x000E8B0C File Offset: 0x000E6F0C
		public void AddStatusPriorityFilterType()
		{
			string[] array = new string[6];
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Length = 0;
			for (int i = 0; i < 6; i++)
			{
				stringBuilder.Length = 0;
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.StatusHp + i));
				stringBuilder.Append(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.RecommendPriority));
				array[i] = stringBuilder.ToString();
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Choice, false, eText_CommonDB.RecommendStatusPriority, array));
		}

		// Token: 0x06002C05 RID: 11269 RVA: 0x000E8B9C File Offset: 0x000E6F9C
		public void AddTitleSelectType()
		{
			eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
			string[] array = new string[eTitleTypeConverter.HowManyTitles() + 1];
			for (int i = 0; i < array.Length; i++)
			{
				if (i == 0)
				{
					array[i] = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.FilterRandom);
				}
				else
				{
					array[i] = eTitleTypeConverter.ConvertFromOrderToTitleName(i - 1);
				}
			}
			this.AddType(new FilterWindow.FilterTypeData(FilterWindow.eCellMode.Choice, true, eText_CommonDB.ContentTitle, array));
		}

		// Token: 0x06002C06 RID: 11270 RVA: 0x000E8C17 File Offset: 0x000E7017
		public void SetFilterFlag(int filterTypeIdx, bool[] flags)
		{
			this.m_CellList[filterTypeIdx].SetFilter(flags);
		}

		// Token: 0x06002C07 RID: 11271 RVA: 0x000E8C2B File Offset: 0x000E702B
		public void SetFilterAll(int filterTypeIdx, bool flag)
		{
			this.m_CellList[filterTypeIdx].SetAll(flag);
		}

		// Token: 0x06002C08 RID: 11272 RVA: 0x000E8C3F File Offset: 0x000E703F
		public void SetFilter(int filterTypeIdx, bool all, bool[] flags)
		{
			this.m_CellList[filterTypeIdx].SetForceApply(all, flags);
		}

		// Token: 0x06002C09 RID: 11273 RVA: 0x000E8C54 File Offset: 0x000E7054
		public bool[] GetFilterFlag(int filterTypeIdx)
		{
			return this.m_CellList[filterTypeIdx].GetFilterFlagsWithAll();
		}

		// Token: 0x06002C0A RID: 11274 RVA: 0x000E8C67 File Offset: 0x000E7067
		public FilterTypeCell GetFilterType(int filterTypeIdx)
		{
			return this.m_CellList[filterTypeIdx];
		}

		// Token: 0x06002C0B RID: 11275 RVA: 0x000E8C78 File Offset: 0x000E7078
		public int GetFirstChoice(int filterTypeIdx)
		{
			bool[] filterFlagsWithAll = this.m_CellList[filterTypeIdx].GetFilterFlagsWithAll();
			for (int i = 0; i < filterFlagsWithAll.Length; i++)
			{
				if (filterFlagsWithAll[i])
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x06002C0C RID: 11276 RVA: 0x000E8CB6 File Offset: 0x000E70B6
		public void SetHeaderText(string header)
		{
			this.m_HeaderText.text = header;
		}

		// Token: 0x06002C0D RID: 11277 RVA: 0x000E8CC4 File Offset: 0x000E70C4
		public override void Open()
		{
			if (this.m_CustomWindow == null)
			{
				this.m_WindowStatus.m_FooterType = UIGroup.InstantiateWindowStatus.eFooterType.None;
			}
			base.Open();
		}

		// Token: 0x06002C0E RID: 11278 RVA: 0x000E8CE9 File Offset: 0x000E70E9
		public override void Close()
		{
			base.Close();
		}

		// Token: 0x06002C0F RID: 11279 RVA: 0x000E8CF4 File Offset: 0x000E70F4
		public void OnClickDecideButtonCallBack()
		{
			eTitleTypeConverter eTitleTypeConverter = new eTitleTypeConverter().Setup();
			for (int i = 0; i < this.m_CellList.Count; i++)
			{
				if (this.m_CellList[i].CellMode != FilterWindow.eCellMode.Choice)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetFilterFlagIsAll(this.m_UsingType, i, this.m_CellList[i].GetFilterFlagAll());
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetFilterFlagIsAll(this.m_UsingType, i, false);
				}
				bool[] filterFlags = this.m_CellList[i].GetFilterFlags();
				int num = 0;
				if (this.m_CellList[i].GeteTextCommon() == eText_CommonDB.ContentTitle)
				{
					num = filterFlags.Length - eTitleTypeConverter.HowManyTitles();
				}
				for (int j = 0; j < filterFlags.Length; j++)
				{
					int bitIndex = j;
					if (this.m_CellList[i].GeteTextCommon() == eText_CommonDB.ContentTitle)
					{
						if (num < j)
						{
							bitIndex = (int)(eTitleTypeConverter.ConvertFromOrderToTitleType(j - num) + num);
						}
					}
					else if (this.m_CellList[i].GeteTextCommon() == eText_CommonDB.Element)
					{
						bitIndex = (int)UIUtility.ElementSortIdxToEnum(j);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.UISettings.SetFilterFlag(this.m_UsingType, i, bitIndex, filterFlags[j]);
				}
			}
			SingletonMonoBehaviour<GameSystem>.Inst.UISettings.Save();
			this.OnExecute.Call();
			this.Close();
		}

		// Token: 0x06002C10 RID: 11280 RVA: 0x000E8E70 File Offset: 0x000E7270
		public void OnClickDefaultButtonCallBack()
		{
			UISettings.eFilterCategory[] categories = SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetUsingFilterMap(this.m_UsingType).m_Categories;
			for (int i = 0; i < categories.Length; i++)
			{
				bool[] array = new bool[SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterCategoryMax(categories[i])];
				if (categories[i] == UISettings.eFilterCategory.StatusPriority || categories[i] == UISettings.eFilterCategory.TitleSelect)
				{
					array[0] = true;
					this.SetFilter(i, false, array);
				}
				else
				{
					this.SetFilter(i, true, array);
				}
			}
		}

		// Token: 0x06002C11 RID: 11281 RVA: 0x000E8EF4 File Offset: 0x000E72F4
		private void AlignmentCells()
		{
			this.m_Scroll.content.sizeDelta = new Vector2(this.m_Scroll.content.sizeDelta.x, 0f);
			for (int i = 0; i < this.m_CellList.Count; i++)
			{
				this.m_CellList[i].RebuildLayoutImmdiate();
				RectTransform component = this.m_CellList[i].GetComponent<RectTransform>();
				component.anchorMin = new Vector2(0f, 1f);
				component.anchorMax = new Vector2(0f, 1f);
				component.anchoredPosition = new Vector2(0f, -this.m_Scroll.content.rect.height);
				this.m_Scroll.content.sizeDelta = new Vector2(0f, this.m_Scroll.content.rect.height + component.rect.height);
				if (this.m_Scroll.content.rect.height < this.m_Scroll.viewport.rect.height)
				{
					this.m_Scroll.vertical = false;
				}
				else
				{
					this.m_Scroll.vertical = true;
				}
			}
			this.m_Scroll.content.sizeDelta = new Vector2(0f, this.m_Scroll.content.rect.height + 12f);
		}

		// Token: 0x06002C12 RID: 11282 RVA: 0x000E9095 File Offset: 0x000E7495
		public override void LateUpdate()
		{
			base.LateUpdate();
			if (this.m_IsDirty)
			{
				this.AlignmentCells();
				this.m_IsDirty = false;
			}
		}

		// Token: 0x040032AD RID: 12973
		private readonly eText_CommonDB[] FILTER_LABEL_COMMONTYPE = new eText_CommonDB[]
		{
			eText_CommonDB.CommonType_Character,
			eText_CommonDB.CommonType_Weapon,
			eText_CommonDB.CommonType_Item,
			eText_CommonDB.CommonType_Gold,
			eText_CommonDB.CommonEtc
		};

		// Token: 0x040032AE RID: 12974
		private List<FilterWindow.FilterTypeData> m_TypeList = new List<FilterWindow.FilterTypeData>();

		// Token: 0x040032AF RID: 12975
		[SerializeField]
		public FilterTypeCell m_CellPrefab;

		// Token: 0x040032B0 RID: 12976
		[SerializeField]
		private FilterTypeCell m_CellChoicePrefab;

		// Token: 0x040032B1 RID: 12977
		[SerializeField]
		private FilterTypeCell m_CellContentTitlePrefab;

		// Token: 0x040032B2 RID: 12978
		private List<FilterTypeCell> m_CellList = new List<FilterTypeCell>();

		// Token: 0x040032B3 RID: 12979
		[SerializeField]
		private ScrollRect m_Scroll;

		// Token: 0x040032B4 RID: 12980
		[SerializeField]
		private Text m_HeaderText;

		// Token: 0x040032B5 RID: 12981
		private bool m_IsDirty;

		// Token: 0x040032B7 RID: 12983
		private UISettings.eUsingType_Filter m_UsingType = UISettings.eUsingType_Filter.None;

		// Token: 0x0200083D RID: 2109
		public enum eCellMode
		{
			// Token: 0x040032B9 RID: 12985
			Choice,
			// Token: 0x040032BA RID: 12986
			Filter,
			// Token: 0x040032BB RID: 12987
			FilterSingleSelect
		}

		// Token: 0x0200083E RID: 2110
		public class FilterTypeData
		{
			// Token: 0x06002C13 RID: 11283 RVA: 0x000E90B5 File Offset: 0x000E74B5
			public FilterTypeData(FilterWindow.eCellMode cellMode, bool contentTitleMode, eText_CommonDB eTextCommon, string[] buttonNames)
			{
				this.m_CellMode = cellMode;
				this.m_ContentTitleMode = contentTitleMode;
				this.m_eTextCommon = eTextCommon;
				this.m_ButtonNames = buttonNames;
			}

			// Token: 0x040032BC RID: 12988
			public FilterWindow.eCellMode m_CellMode;

			// Token: 0x040032BD RID: 12989
			public bool m_ContentTitleMode;

			// Token: 0x040032BE RID: 12990
			public eText_CommonDB m_eTextCommon;

			// Token: 0x040032BF RID: 12991
			public string[] m_ButtonNames;
		}
	}
}
