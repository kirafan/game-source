﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Star.UI.Filter
{
	// Token: 0x0200083B RID: 2107
	public class FilterTypeCell : MonoBehaviour
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06002BEB RID: 11243 RVA: 0x000E7F3C File Offset: 0x000E633C
		public FilterWindow.eCellMode CellMode
		{
			get
			{
				return this.m_CellMode;
			}
		}

		// Token: 0x06002BEC RID: 11244 RVA: 0x000E7F44 File Offset: 0x000E6344
		public void Setup(FilterWindow.eCellMode cellMode, eText_CommonDB eTextCommon, string[] buttonNames, bool contentTitle)
		{
			this.m_CellMode = cellMode;
			this.m_TitleMode = contentTitle;
			this.m_eTextCommon = eTextCommon;
			this.m_TitleText.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eTextCommon);
			this.m_Flags = new bool[buttonNames.Length];
			this.m_ButtonList.Clear();
			this.m_ButtonNum = buttonNames.Length;
			if (this.m_CellMode != FilterWindow.eCellMode.Choice)
			{
				this.m_ButtonList.Add(this.CreateButton(-1, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextCommon(eText_CommonDB.CommonAll)));
			}
			for (int i = 0; i < buttonNames.Length; i++)
			{
				this.m_ButtonList.Add(this.CreateButton(i, buttonNames[i]));
			}
			if (this.m_CellMode != FilterWindow.eCellMode.Choice)
			{
				this.SetAll(true);
			}
			else
			{
				this.Choice(0);
			}
		}

		// Token: 0x06002BED RID: 11245 RVA: 0x000E801C File Offset: 0x000E641C
		public void Choice(int idx)
		{
			for (int i = 0; i < this.m_Flags.Length; i++)
			{
				this.m_Flags[i] = (i == idx);
			}
			this.SetFilter(this.m_Flags);
		}

		// Token: 0x06002BEE RID: 11246 RVA: 0x000E805C File Offset: 0x000E645C
		public void SetFilter(bool[] flags)
		{
			int num = 0;
			for (int i = 0; i < flags.Length; i++)
			{
				if (flags[i])
				{
					num++;
				}
			}
			if (num == 0)
			{
				this.SetAll(true);
				return;
			}
			this.m_All = false;
			this.m_Flags = flags;
			int num2 = 0;
			if (this.m_CellMode != FilterWindow.eCellMode.Choice)
			{
				this.m_ButtonList[0].SetDecided(false);
				num2++;
			}
			for (int j = 0; j < flags.Length; j++)
			{
				this.m_ButtonList[num2 + j].SetDecided(flags[j]);
			}
		}

		// Token: 0x06002BEF RID: 11247 RVA: 0x000E80F4 File Offset: 0x000E64F4
		public void SetAll(bool flag)
		{
			if (this.m_CellMode == FilterWindow.eCellMode.Choice)
			{
				this.m_All = false;
				return;
			}
			this.m_All = flag;
			this.m_ButtonList[0].SetDecided(flag);
			for (int i = 0; i < this.m_ButtonList.Count - 1; i++)
			{
				this.m_Flags[i] = false;
				this.m_ButtonList[i + 1].SetDecided(false);
			}
		}

		// Token: 0x06002BF0 RID: 11248 RVA: 0x000E8168 File Offset: 0x000E6568
		public void SetForceApply(bool all, bool[] flags)
		{
			if (this.m_CellMode != FilterWindow.eCellMode.Choice)
			{
				this.m_All = all;
				this.m_Flags = flags;
				this.m_ButtonList[0].SetDecided(all);
				for (int i = 0; i < this.m_ButtonList.Count - 1; i++)
				{
					this.m_ButtonList[i + 1].SetDecided(flags[i]);
				}
			}
			else
			{
				this.m_Flags = flags;
				int num = 0;
				for (int j = 0; j < this.m_Flags.Length; j++)
				{
					if (flags[j])
					{
						num++;
					}
				}
				if (num == 0)
				{
					this.m_Flags[0] = true;
				}
				for (int k = 0; k < this.m_ButtonList.Count; k++)
				{
					this.m_ButtonList[k].SetDecided(flags[k]);
				}
			}
		}

		// Token: 0x06002BF1 RID: 11249 RVA: 0x000E8248 File Offset: 0x000E6648
		private FilterButton CreateButton(int idx, string name)
		{
			RectTransform rectTransform = this.m_ButtonParent;
			if (this.m_TitleMode && idx == -1)
			{
				rectTransform = this.m_TitleButtonAllParent;
			}
			FilterButton original = this.m_BodyButtonPrefab;
			if (this.m_ButtonList.Count == 0)
			{
				original = this.m_HeadButtonPrefab;
			}
			else if (idx == this.m_ButtonNum - 1)
			{
				original = this.m_TailButtonPrefab;
			}
			FilterButton filterButton = UnityEngine.Object.Instantiate<FilterButton>(original, rectTransform, false);
			RectTransform component = filterButton.GetComponent<RectTransform>();
			if (idx == -1 && this.m_CellMode != FilterWindow.eCellMode.Choice)
			{
				if (this.m_TitleMode)
				{
					component.anchorMin = new Vector2(0f, 0f);
					component.anchorMax = new Vector2(1f, 1f);
					component.sizeDelta = new Vector2(0f, 0f);
				}
				else
				{
					component.sizeDelta = new Vector2(203f, component.sizeDelta.y);
				}
			}
			else if (this.m_CellMode == FilterWindow.eCellMode.Choice)
			{
				component.sizeDelta = new Vector2(rectTransform.rect.width / (float)this.m_ButtonNum, component.sizeDelta.y);
			}
			else
			{
				component.sizeDelta = new Vector2((rectTransform.rect.width - 203f) / (float)this.m_ButtonNum, component.sizeDelta.y);
			}
			filterButton.Idx = idx;
			Text componentInChildren = filterButton.GetComponentInChildren<Text>();
			componentInChildren.text = name;
			filterButton.OnClick += this.OnClickFilterButtonCallBack;
			return filterButton;
		}

		// Token: 0x06002BF2 RID: 11250 RVA: 0x000E83E4 File Offset: 0x000E67E4
		public void RebuildLayoutImmdiate()
		{
			if (this.m_TitleButtonParent != null)
			{
				LayoutRebuilder.ForceRebuildLayoutImmediate(this.m_TitleButtonParent);
				RectTransform component = base.GetComponent<RectTransform>();
				component.sizeDelta = new Vector2(component.sizeDelta.x, this.m_TitleButtonParent.rect.height + 84f);
			}
		}

		// Token: 0x06002BF3 RID: 11251 RVA: 0x000E8448 File Offset: 0x000E6848
		private void OnClickFilterButtonCallBack(int idx)
		{
			if (this.m_CellMode == FilterWindow.eCellMode.Choice)
			{
				this.Choice(idx);
			}
			else if (idx == -1)
			{
				this.SetAll(!this.m_All);
			}
			else
			{
				if (this.m_CellMode == FilterWindow.eCellMode.FilterSingleSelect)
				{
					for (int i = 0; i < this.m_Flags.Length; i++)
					{
						this.m_Flags[i] = false;
					}
				}
				this.m_Flags[idx] = !this.m_Flags[idx];
				this.SetFilter(this.m_Flags);
			}
		}

		// Token: 0x06002BF4 RID: 11252 RVA: 0x000E84D4 File Offset: 0x000E68D4
		public bool[] GetFilterFlagsWithAll()
		{
			bool[] array = new bool[this.m_Flags.Length];
			if (this.m_All)
			{
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = true;
				}
			}
			else
			{
				this.m_Flags.CopyTo(array, 0);
			}
			return array;
		}

		// Token: 0x06002BF5 RID: 11253 RVA: 0x000E8525 File Offset: 0x000E6925
		public bool GetFilterFlagAll()
		{
			return this.m_All;
		}

		// Token: 0x06002BF6 RID: 11254 RVA: 0x000E852D File Offset: 0x000E692D
		public bool[] GetFilterFlags()
		{
			return this.m_Flags;
		}

		// Token: 0x06002BF7 RID: 11255 RVA: 0x000E8535 File Offset: 0x000E6935
		public eText_CommonDB GeteTextCommon()
		{
			return this.m_eTextCommon;
		}

		// Token: 0x0400329D RID: 12957
		public const int ALLBUTTON_IDX = -1;

		// Token: 0x0400329E RID: 12958
		public const float ALLBUTTON_WIDTH = 203f;

		// Token: 0x0400329F RID: 12959
		[SerializeField]
		private FilterButton m_HeadButtonPrefab;

		// Token: 0x040032A0 RID: 12960
		[SerializeField]
		private FilterButton m_BodyButtonPrefab;

		// Token: 0x040032A1 RID: 12961
		[SerializeField]
		private FilterButton m_TailButtonPrefab;

		// Token: 0x040032A2 RID: 12962
		[SerializeField]
		private RectTransform m_ButtonParent;

		// Token: 0x040032A3 RID: 12963
		[SerializeField]
		private RectTransform m_TitleButtonAllParent;

		// Token: 0x040032A4 RID: 12964
		[SerializeField]
		private RectTransform m_TitleButtonParent;

		// Token: 0x040032A5 RID: 12965
		[SerializeField]
		private Text m_TitleText;

		// Token: 0x040032A6 RID: 12966
		private eText_CommonDB m_eTextCommon;

		// Token: 0x040032A7 RID: 12967
		private int m_ButtonNum;

		// Token: 0x040032A8 RID: 12968
		private List<FilterButton> m_ButtonList = new List<FilterButton>();

		// Token: 0x040032A9 RID: 12969
		private bool m_All;

		// Token: 0x040032AA RID: 12970
		private bool[] m_Flags;

		// Token: 0x040032AB RID: 12971
		private FilterWindow.eCellMode m_CellMode = FilterWindow.eCellMode.Filter;

		// Token: 0x040032AC RID: 12972
		private bool m_TitleMode;
	}
}
