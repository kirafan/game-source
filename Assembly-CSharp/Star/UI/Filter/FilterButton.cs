﻿using System;
using UnityEngine;

namespace Star.UI.Filter
{
	// Token: 0x02000833 RID: 2099
	public class FilterButton : MonoBehaviour
	{
		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06002BDB RID: 11227 RVA: 0x000E7BF3 File Offset: 0x000E5FF3
		// (set) Token: 0x06002BDC RID: 11228 RVA: 0x000E7BFB File Offset: 0x000E5FFB
		public int Idx { get; set; }

		// Token: 0x14000064 RID: 100
		// (add) Token: 0x06002BDD RID: 11229 RVA: 0x000E7C04 File Offset: 0x000E6004
		// (remove) Token: 0x06002BDE RID: 11230 RVA: 0x000E7C3C File Offset: 0x000E603C
		public event Action<int> OnClick;

		// Token: 0x06002BDF RID: 11231 RVA: 0x000E7C72 File Offset: 0x000E6072
		public void OnClickCallBack()
		{
			this.OnClick.Call(this.Idx);
		}

		// Token: 0x06002BE0 RID: 11232 RVA: 0x000E7C85 File Offset: 0x000E6085
		public void SetDecided(bool flag)
		{
			this.m_CustomButton.IsDecided = flag;
		}

		// Token: 0x04003279 RID: 12921
		[SerializeField]
		private CustomButton m_CustomButton;
	}
}
