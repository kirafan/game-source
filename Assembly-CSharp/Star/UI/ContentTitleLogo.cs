﻿using System;

namespace Star.UI
{
	// Token: 0x0200085F RID: 2143
	public class ContentTitleLogo : ASyncImage
	{
		// Token: 0x06002C90 RID: 11408 RVA: 0x000EB65C File Offset: 0x000E9A5C
		public void Apply(eTitleType titleType)
		{
			if (this.m_TitleType != titleType)
			{
				this.Apply();
				if (titleType != eTitleType.None)
				{
					this.m_SpriteHandler = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncContentTitleLogo(titleType);
					this.m_Image.enabled = false;
					this.m_Image.sprite = null;
				}
				else
				{
					this.Destroy();
				}
				this.m_TitleType = titleType;
			}
		}

		// Token: 0x06002C91 RID: 11409 RVA: 0x000EB6C2 File Offset: 0x000E9AC2
		public override void Destroy()
		{
			base.Destroy();
			this.m_TitleType = eTitleType.None;
		}

		// Token: 0x0400337E RID: 13182
		protected eTitleType m_TitleType = eTitleType.None;
	}
}
