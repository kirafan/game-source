﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000679 RID: 1657
	public class TownCameraSize : ITownCameraKey
	{
		// Token: 0x06002185 RID: 8581 RVA: 0x000B2956 File Offset: 0x000B0D56
		public TownCameraSize(TownCamera ptarget, float fbasepos, float ftargetpos, float ftime)
		{
			this.m_LineType = TownCameraSize.ePathType.Linear;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BaseSize = fbasepos;
			this.m_TargetSize = ftargetpos;
			this.m_Camera = ptarget;
			this.m_Active = true;
		}

		// Token: 0x06002186 RID: 8582 RVA: 0x000B2994 File Offset: 0x000B0D94
		public TownCameraSize(TownCamera ptarget, float fbasepos, float fpoint1, float fpoint2, float ftargetpos, float ftime)
		{
			this.m_LineType = TownCameraSize.ePathType.Bez;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_BaseSize = fbasepos;
			this.m_TargetSize = ftargetpos;
			this.m_Point1 = fpoint1;
			this.m_Point2 = fpoint2;
			this.m_Camera = ptarget;
			this.m_Active = true;
		}

		// Token: 0x06002187 RID: 8583 RVA: 0x000B29F0 File Offset: 0x000B0DF0
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Camera.SetZoom(this.m_TargetSize, true);
				this.m_Active = false;
			}
			else
			{
				float num = this.m_Time / this.m_MaxTime;
				if (this.m_LineType == TownCameraSize.ePathType.Linear)
				{
					this.m_Camera.SetZoom((this.m_TargetSize - this.m_BaseSize) * num + this.m_BaseSize, true);
				}
				else
				{
					float num2 = 1f - num;
					float num3 = num2 * num2;
					float num4 = num * num;
					float num5 = num2 * num3;
					float num6 = num * num4;
					num3 = num3 * num * 3f;
					num4 = num4 * num2 * 3f;
					this.m_Camera.SetZoom(this.m_BaseSize * num5 + this.m_Point1 * num3 + this.m_Point2 * num4 + this.m_TargetSize * num6, true);
				}
			}
			return this.m_Active;
		}

		// Token: 0x040027E7 RID: 10215
		private float m_BaseSize;

		// Token: 0x040027E8 RID: 10216
		private float m_TargetSize;

		// Token: 0x040027E9 RID: 10217
		private float m_Point1;

		// Token: 0x040027EA RID: 10218
		private float m_Point2;

		// Token: 0x040027EB RID: 10219
		private float m_Time;

		// Token: 0x040027EC RID: 10220
		private float m_MaxTime;

		// Token: 0x040027ED RID: 10221
		private TownCameraSize.ePathType m_LineType;

		// Token: 0x0200067A RID: 1658
		private enum ePathType
		{
			// Token: 0x040027EF RID: 10223
			Linear,
			// Token: 0x040027F0 RID: 10224
			Bez
		}
	}
}
