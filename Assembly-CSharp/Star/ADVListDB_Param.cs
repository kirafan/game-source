﻿using System;

namespace Star
{
	// Token: 0x02000160 RID: 352
	[Serializable]
	public struct ADVListDB_Param
	{
		// Token: 0x04000956 RID: 2390
		public int m_AdvID;

		// Token: 0x04000957 RID: 2391
		public int m_Category;

		// Token: 0x04000958 RID: 2392
		public string m_ScriptName;

		// Token: 0x04000959 RID: 2393
		public string m_ScriptTextName;

		// Token: 0x0400095A RID: 2394
		public string m_MovieFileName;

		// Token: 0x0400095B RID: 2395
		public string m_Title;

		// Token: 0x0400095C RID: 2396
		public int m_LibraryID;

		// Token: 0x0400095D RID: 2397
		public int[] m_NamedType;

		// Token: 0x0400095E RID: 2398
		public int[] m_LimitNamedType;

		// Token: 0x0400095F RID: 2399
		public int m_LimitFriendShipLv;

		// Token: 0x04000960 RID: 2400
		public int m_PresentGemNum;

		// Token: 0x04000961 RID: 2401
		public int m_PresentMessageID;
	}
}
