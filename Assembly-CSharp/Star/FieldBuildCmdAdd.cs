﻿using System;

namespace Star
{
	// Token: 0x02000317 RID: 791
	public class FieldBuildCmdAdd : FieldBuilCmdBase
	{
		// Token: 0x06000F10 RID: 3856 RVA: 0x00050B7F File Offset: 0x0004EF7F
		public FieldBuildCmdAdd(long fmanageid, int fpoint, int fobjid)
		{
			this.m_BuildPoint = fpoint;
			this.m_ManageID = fmanageid;
			this.m_ObjID = fobjid;
			this.m_Cmd = FieldBuilCmdBase.eCmd.Add;
		}

		// Token: 0x04001695 RID: 5781
		public int m_BuildPoint;

		// Token: 0x04001696 RID: 5782
		public long m_ManageID;

		// Token: 0x04001697 RID: 5783
		public int m_ObjID;
	}
}
