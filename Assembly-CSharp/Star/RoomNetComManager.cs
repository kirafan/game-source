﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000596 RID: 1430
	public class RoomNetComManager : IFldNetComManager
	{
		// Token: 0x06001BB5 RID: 7093 RVA: 0x00092AB4 File Offset: 0x00090EB4
		private void SetNextStep(RoomNetComManager.eComStep fnextup)
		{
			bool flag = true;
			while (flag)
			{
				switch (fnextup)
				{
				case RoomNetComManager.eComStep.ListIn:
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.IsSetUpActive())
					{
						base.AddModule(new FldComDefaultDB());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.TownObjSetUp;
					}
					continue;
				case RoomNetComManager.eComStep.DefObjSetUp:
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.IsSetUpActive())
					{
						base.AddModule(new RoomComObjSetUp());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.TownObjSetUp;
					}
					continue;
				case RoomNetComManager.eComStep.DefSetUp:
					base.AddModule(new RoomComSetUp());
					flag = false;
					continue;
				case RoomNetComManager.eComStep.DefRoomList:
					if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.IsSetUpActive())
					{
						base.AddModule(new RoomComListSetUp());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.TownObjSetUp;
					}
					continue;
				case RoomNetComManager.eComStep.TownObjSetUp:
					if (!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.IsSetUpConnect())
					{
						base.AddModule(new FldComDefaultDB());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.Update;
					}
					continue;
				case RoomNetComManager.eComStep.TownObjSetUpCom:
					if (!UserTownObjectData.IsSetUpConnect())
					{
						base.AddModule(new TownComObjSetUp());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.TownBuildSetUp;
					}
					continue;
				case RoomNetComManager.eComStep.TownBuildSetUp:
					if (!SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.IsSetUpConnect())
					{
						base.AddModule(new TownComSetUp());
						flag = false;
					}
					else
					{
						fnextup = RoomNetComManager.eComStep.Update;
					}
					continue;
				case RoomNetComManager.eComStep.UserDataUp:
					fnextup = RoomNetComManager.eComStep.Update;
					flag = false;
					continue;
				case RoomNetComManager.eComStep.RoomPlaceGet:
					base.AddModule(new RoomComPlayerPlaceObj(this.m_EditPlayerId));
					flag = false;
					continue;
				case RoomNetComManager.eComStep.FriendPartyGet:
				{
					RoomFriendFieldCharaIO pio = base.gameObject.AddComponent<RoomFriendFieldCharaIO>();
					base.AddModule(new RoomComPlayerFieldParty(this.m_EditPlayerId, pio));
					flag = false;
					continue;
				}
				}
				flag = false;
			}
			this.m_Step = fnextup;
		}

		// Token: 0x06001BB6 RID: 7094 RVA: 0x00092C94 File Offset: 0x00091094
		public static void SetUpManager(GameObject ptarget, long fplayerId)
		{
			RoomNetComManager.Inst = ptarget.AddComponent<RoomNetComManager>();
			RoomNetComManager.Inst.AwakeCom();
			RoomNetComManager.Inst.m_EditPlayerId = fplayerId;
			if (fplayerId == SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID)
			{
				RoomNetComManager.Inst.m_FriendPlaceData = false;
				RoomNetComManager.Inst.SetNextStep(RoomNetComManager.eComStep.ListIn);
			}
			else
			{
				RoomNetComManager.Inst.m_FriendPlaceData = true;
				RoomNetComManager.Inst.SetNextStep(RoomNetComManager.eComStep.RoomPlaceGet);
			}
		}

		// Token: 0x06001BB7 RID: 7095 RVA: 0x00092D0C File Offset: 0x0009110C
		public static bool IsSetUp()
		{
			return RoomNetComManager.Inst.m_Step == RoomNetComManager.eComStep.Update;
		}

		// Token: 0x06001BB8 RID: 7096 RVA: 0x00092D1C File Offset: 0x0009111C
		private void Update()
		{
			base.UpCom();
			if (!base.UpModule() && this.m_Step != RoomNetComManager.eComStep.Update)
			{
				this.SetNextStep(this.m_Step + 1);
			}
		}

		// Token: 0x06001BB9 RID: 7097 RVA: 0x00092D4B File Offset: 0x0009114B
		private void OnDestroy()
		{
			if (this.m_FriendPlaceData)
			{
				UserRoomUtil.ReleaseCheckRoomData(this.m_EditPlayerId);
			}
			if (RoomNetComManager.Inst != null)
			{
				RoomNetComManager.Inst.DestroyCom();
				RoomNetComManager.Inst = null;
			}
		}

		// Token: 0x06001BBA RID: 7098 RVA: 0x00092D84 File Offset: 0x00091184
		public static bool IsCheckRoomSetUp(int fcheckkey)
		{
			bool result = false;
			if (RoomComSetUp.CheckDefaultDataUp((eDefCheckUpType)fcheckkey))
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06001BBB RID: 7099 RVA: 0x00092DA4 File Offset: 0x000911A4
		public static void SetUpRoomDefault(int fcheckkey)
		{
			RoomComUnlockDefault roomComUnlockDefault = new RoomComUnlockDefault(fcheckkey);
			roomComUnlockDefault.PlaySend();
		}

		// Token: 0x0400229A RID: 8858
		private static RoomNetComManager Inst;

		// Token: 0x0400229B RID: 8859
		private RoomNetComManager.eComStep m_Step;

		// Token: 0x0400229C RID: 8860
		private long m_EditPlayerId;

		// Token: 0x0400229D RID: 8861
		private bool m_FriendPlaceData;

		// Token: 0x02000597 RID: 1431
		public enum eComStep
		{
			// Token: 0x0400229F RID: 8863
			ListIn,
			// Token: 0x040022A0 RID: 8864
			DefObjSetUp,
			// Token: 0x040022A1 RID: 8865
			DefSetUp,
			// Token: 0x040022A2 RID: 8866
			DefRoomList,
			// Token: 0x040022A3 RID: 8867
			TownObjSetUp,
			// Token: 0x040022A4 RID: 8868
			TownObjSetUpCom,
			// Token: 0x040022A5 RID: 8869
			TownBuildSetUp,
			// Token: 0x040022A6 RID: 8870
			UserDataUp,
			// Token: 0x040022A7 RID: 8871
			RoomPlaceGet,
			// Token: 0x040022A8 RID: 8872
			FriendPartyGet,
			// Token: 0x040022A9 RID: 8873
			Update
		}
	}
}
