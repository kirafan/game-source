﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000537 RID: 1335
	public class RoomActionCommand
	{
		// Token: 0x04002128 RID: 8488
		public RoomActionCommand.eActionCode m_Command;

		// Token: 0x04002129 RID: 8489
		public int m_ParamI1;

		// Token: 0x0400212A RID: 8490
		public int m_ParamI2;

		// Token: 0x0400212B RID: 8491
		public int m_ParamI3;

		// Token: 0x0400212C RID: 8492
		public int m_ParamI4;

		// Token: 0x0400212D RID: 8493
		public Vector4 m_ParamF;

		// Token: 0x0400212E RID: 8494
		public string m_ParamName;

		// Token: 0x0400212F RID: 8495
		public bool m_Lock;

		// Token: 0x02000538 RID: 1336
		public enum eActionCode
		{
			// Token: 0x04002131 RID: 8497
			Non,
			// Token: 0x04002132 RID: 8498
			Stop,
			// Token: 0x04002133 RID: 8499
			Walk,
			// Token: 0x04002134 RID: 8500
			Jump,
			// Token: 0x04002135 RID: 8501
			Action,
			// Token: 0x04002136 RID: 8502
			Event,
			// Token: 0x04002137 RID: 8503
			Erase,
			// Token: 0x04002138 RID: 8504
			Sleep
		}

		// Token: 0x02000539 RID: 1337
		public enum eTweetAction
		{
			// Token: 0x0400213A RID: 8506
			Idle,
			// Token: 0x0400213B RID: 8507
			Enjoy,
			// Token: 0x0400213C RID: 8508
			Tweet,
			// Token: 0x0400213D RID: 8509
			Rejoice
		}
	}
}
