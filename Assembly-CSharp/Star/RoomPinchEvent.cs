﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200057F RID: 1407
	public class RoomPinchEvent
	{
		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06001B44 RID: 6980 RVA: 0x000905C4 File Offset: 0x0008E9C4
		// (set) Token: 0x06001B45 RID: 6981 RVA: 0x000905CC File Offset: 0x0008E9CC
		public float InitialPinchDist { get; set; }

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x06001B46 RID: 6982 RVA: 0x000905D5 File Offset: 0x0008E9D5
		// (set) Token: 0x06001B47 RID: 6983 RVA: 0x000905DD File Offset: 0x0008E9DD
		public bool IsPinching { get; set; }

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x06001B48 RID: 6984 RVA: 0x000905E6 File Offset: 0x0008E9E6
		// (set) Token: 0x06001B49 RID: 6985 RVA: 0x000905EE File Offset: 0x0008E9EE
		public Vector2 FocusedPos { get; set; }

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x06001B4A RID: 6986 RVA: 0x000905F7 File Offset: 0x0008E9F7
		// (set) Token: 0x06001B4B RID: 6987 RVA: 0x000905FF File Offset: 0x0008E9FF
		public float PinchRatio { get; set; }

		// Token: 0x06001B4C RID: 6988 RVA: 0x00090608 File Offset: 0x0008EA08
		public void UpdatePinch(RoomPinchState pstate)
		{
			pstate.m_Event = false;
			if (Input.touchCount == 2)
			{
				if (!this.IsPinching)
				{
					this.InitialPinchDist = this.CalcPinchDist();
					this.m_CurrentPinchDist = this.InitialPinchDist;
					this.PinchRatio = 1f;
					this.FocusedPos = this.CalcFocusPos();
					this.IsPinching = true;
					pstate.m_Event = true;
					pstate.IsPinching = this.IsPinching;
					pstate.PinchRatio = this.PinchRatio;
				}
				else
				{
					this.m_OldPinchRatio = this.PinchRatio;
					this.m_CurrentPinchDist = this.CalcPinchDist();
					this.PinchRatio = this.m_CurrentPinchDist / this.InitialPinchDist;
					if (this.m_OldPinchRatio != this.m_CurrentPinchDist)
					{
						pstate.m_Event = true;
						pstate.IsPinching = this.IsPinching;
						pstate.PinchRatio = this.PinchRatio;
					}
				}
			}
			else if (this.IsPinching)
			{
				this.m_CurrentPinchDist = 1f;
				this.InitialPinchDist = 1f;
				this.PinchRatio = -1f;
				this.IsPinching = false;
				pstate.m_Event = true;
				pstate.IsPinching = this.IsPinching;
				pstate.PinchRatio = this.PinchRatio;
			}
		}

		// Token: 0x06001B4D RID: 6989 RVA: 0x00090740 File Offset: 0x0008EB40
		private Vector2 CalcFocusPos()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position + position2) * 0.5f;
			}
			return Input.mousePosition;
		}

		// Token: 0x06001B4E RID: 6990 RVA: 0x00090798 File Offset: 0x0008EB98
		private float CalcPinchDist()
		{
			if (Input.touchCount == 2)
			{
				Vector2 position = Input.touches[0].position;
				Vector2 position2 = Input.touches[1].position;
				return (position - position2).magnitude;
			}
			return 0f;
		}

		// Token: 0x04002246 RID: 8774
		private float m_CurrentPinchDist;

		// Token: 0x04002247 RID: 8775
		private float m_OldPinchRatio;
	}
}
