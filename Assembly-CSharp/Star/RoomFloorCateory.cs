﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005EA RID: 1514
	[Serializable]
	public class RoomFloorCateory
	{
		// Token: 0x04002479 RID: 9337
		public int m_RoomID;

		// Token: 0x0400247A RID: 9338
		public RoomFloorCateory.eCategory m_Type;

		// Token: 0x0400247B RID: 9339
		public float m_PosX;

		// Token: 0x0400247C RID: 9340
		public float m_PosY;

		// Token: 0x0400247D RID: 9341
		public int m_SizeX;

		// Token: 0x0400247E RID: 9342
		public int m_SizeY;

		// Token: 0x0400247F RID: 9343
		public int m_SizeZ;

		// Token: 0x04002480 RID: 9344
		public Vector2 m_RoomOffset;

		// Token: 0x04002481 RID: 9345
		public Vector2 m_WallOffset;

		// Token: 0x020005EB RID: 1515
		public enum eCategory
		{
			// Token: 0x04002483 RID: 9347
			Main,
			// Token: 0x04002484 RID: 9348
			Sleep
		}
	}
}
