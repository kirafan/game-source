﻿using System;

namespace Star
{
	// Token: 0x0200053A RID: 1338
	public class RoomActionCreate
	{
		// Token: 0x06001AAE RID: 6830 RVA: 0x0008E76C File Offset: 0x0008CB6C
		public static ICharaRoomAction CreateEventAction(eRoomObjectEventType ftype)
		{
			ICharaRoomAction result;
			switch (ftype)
			{
			case eRoomObjectEventType.Lift:
				result = new CharaActionPlayAnime();
				break;
			case eRoomObjectEventType.Sit:
				result = new CharaActionSit();
				break;
			case eRoomObjectEventType.Sleep:
				result = new CharaActionSleep();
				break;
			case eRoomObjectEventType.PlayAnime:
				result = new CharaActionPlayAnime();
				break;
			case eRoomObjectEventType.TriggerChange:
				result = new CharaActionPlayAnime();
				break;
			case eRoomObjectEventType.SitBook:
				result = new CharaActionSitBook();
				break;
			default:
				result = new CharaActionPlayAnime();
				break;
			}
			return result;
		}
	}
}
