﻿using System;

namespace Star
{
	// Token: 0x020002AC RID: 684
	public class CharacterEditResultSceneBeforeAfterPairPartCharacterDataWrapper<ResultType> : CharacterEditResultSceneCharacterParamWrapper where ResultType : EditMain.EditAdditiveSceneResultData
	{
		// Token: 0x06000CDE RID: 3294 RVA: 0x00048FEC File Offset: 0x000473EC
		public CharacterEditResultSceneBeforeAfterPairPartCharacterDataWrapper(eCharacterDataType characterDataType, ResultType result) : base(characterDataType, result)
		{
			this.m_Result = result;
		}

		// Token: 0x04001549 RID: 5449
		protected ResultType m_Result;
	}
}
