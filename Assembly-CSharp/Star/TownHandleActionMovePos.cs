﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006AA RID: 1706
	public class TownHandleActionMovePos : ITownHandleAction
	{
		// Token: 0x06002219 RID: 8729 RVA: 0x000B5563 File Offset: 0x000B3963
		public TownHandleActionMovePos(long fmanageid, int flayerid)
		{
			this.m_Type = eTownEventAct.MovePos;
			this.m_ManageID = fmanageid;
			this.m_LayerID = flayerid;
		}

		// Token: 0x040028A1 RID: 10401
		public Vector3 m_MovePos;

		// Token: 0x040028A2 RID: 10402
		public long m_ManageID;

		// Token: 0x040028A3 RID: 10403
		public int m_LayerID;
	}
}
