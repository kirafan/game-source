﻿using System;

namespace Star
{
	// Token: 0x02000B58 RID: 2904
	public class UserWeaponData
	{
		// Token: 0x06003DA4 RID: 15780 RVA: 0x00137338 File Offset: 0x00135738
		public UserWeaponData()
		{
			this.MngID = -1L;
			this.Param = new WeaponParam();
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06003DA5 RID: 15781 RVA: 0x00137353 File Offset: 0x00135753
		// (set) Token: 0x06003DA6 RID: 15782 RVA: 0x0013735B File Offset: 0x0013575B
		public long MngID { get; set; }

		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x06003DA7 RID: 15783 RVA: 0x00137364 File Offset: 0x00135764
		// (set) Token: 0x06003DA8 RID: 15784 RVA: 0x0013736C File Offset: 0x0013576C
		public WeaponParam Param { get; set; }
	}
}
