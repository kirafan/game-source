﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200007F RID: 127
	[Serializable]
	public class ActionAggregationChildAction
	{
		// Token: 0x060003E7 RID: 999 RVA: 0x00013C23 File Offset: 0x00012023
		public virtual void Setup()
		{
			this.m_ChildActionAdapter.Setup();
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x00013C30 File Offset: 0x00012030
		public virtual void Update()
		{
			this.m_ChildActionAdapter.Update();
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x00013C3D File Offset: 0x0001203D
		public virtual void PlayStart()
		{
			this.m_ChildActionAdapter.PlayStart();
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x00013C4A File Offset: 0x0001204A
		public virtual void Skip()
		{
			this.m_ChildActionAdapter.Skip();
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x00013C57 File Offset: 0x00012057
		public virtual void RecoveryFinishToWait()
		{
			if (this.m_ChildActionAdapter != null)
			{
				this.m_ChildActionAdapter.RecoveryFinishToWait();
			}
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x00013C75 File Offset: 0x00012075
		public virtual void ForceRecoveryFinishToWait()
		{
			this.m_ChildActionAdapter.ForceRecoveryFinishToWait();
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x00013C82 File Offset: 0x00012082
		public eActionAggregationState GetState()
		{
			return this.m_ChildActionAdapter.GetState();
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x00013C8F File Offset: 0x0001208F
		public int GetPlayStartFrame()
		{
			return this.m_PlayStartFrame;
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x00013C97 File Offset: 0x00012097
		public bool IsExecuteEvenIfSkip()
		{
			return this.m_IsExecuteEvenIfSkip;
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00013C9F File Offset: 0x0001209F
		public bool IsFinished()
		{
			return true;
		}

		// Token: 0x04000225 RID: 549
		[SerializeField]
		[Tooltip("実行するするアクション.")]
		private ActionAggregationChildActionAdapter m_ChildActionAdapter;

		// Token: 0x04000226 RID: 550
		[SerializeField]
		[Tooltip("アニメーションの再生を開始するフレーム.")]
		private int m_PlayStartFrame;

		// Token: 0x04000227 RID: 551
		[SerializeField]
		[Tooltip("スキップした際にも必ず実行するかどうか.")]
		private bool m_IsExecuteEvenIfSkip;
	}
}
