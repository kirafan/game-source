﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000733 RID: 1843
	public class TownPakageDestroy : ITownEventCommand
	{
		// Token: 0x0600244A RID: 9290 RVA: 0x000C2AAD File Offset: 0x000C0EAD
		public TownPakageDestroy(TownBuildLinkPoint pnode)
		{
			this.m_Node = pnode;
			this.m_Enable = true;
		}

		// Token: 0x0600244B RID: 9291 RVA: 0x000C2AC4 File Offset: 0x000C0EC4
		public override bool CalcRequest(TownBuilder pbuilder)
		{
			if (this.m_Node != null)
			{
				if (!this.m_Node.m_Pakage.enabled)
				{
					UnityEngine.Object.Destroy(this.m_Node.m_Pakage);
					this.m_Enable = false;
					this.m_Node = null;
				}
			}
			else
			{
				this.m_Enable = false;
			}
			return this.m_Enable;
		}

		// Token: 0x04002B48 RID: 11080
		private TownBuildLinkPoint m_Node;
	}
}
