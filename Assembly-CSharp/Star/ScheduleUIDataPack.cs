﻿using System;

namespace Star
{
	// Token: 0x02000379 RID: 889
	public class ScheduleUIDataPack
	{
		// Token: 0x060010EE RID: 4334 RVA: 0x00059304 File Offset: 0x00057704
		public int GetListNum()
		{
			return this.m_Table.Length;
		}

		// Token: 0x060010EF RID: 4335 RVA: 0x0005930E File Offset: 0x0005770E
		public ScheduleUIData GetTable(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x040017DC RID: 6108
		public ScheduleUIData[] m_Table;
	}
}
