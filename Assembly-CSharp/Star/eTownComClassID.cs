﻿using System;

namespace Star
{
	// Token: 0x020004A7 RID: 1191
	public enum eTownComClassID
	{
		// Token: 0x04001E12 RID: 7698
		ComUI = 2,
		// Token: 0x04001E13 RID: 7699
		Build,
		// Token: 0x04001E14 RID: 7700
		CompleteBuild,
		// Token: 0x04001E15 RID: 7701
		CharaPopup,
		// Token: 0x04001E16 RID: 7702
		MoveDecision,
		// Token: 0x04001E17 RID: 7703
		AddBuildComEnd
	}
}
