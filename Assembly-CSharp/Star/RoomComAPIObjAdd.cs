﻿using System;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x02000592 RID: 1426
	public class RoomComAPIObjAdd : INetComHandle
	{
		// Token: 0x06001BB0 RID: 7088 RVA: 0x00092A03 File Offset: 0x00090E03
		public RoomComAPIObjAdd()
		{
			this.ApiName = "player/room_object/add";
			this.Request = true;
			this.ResponseType = typeof(Add);
		}

		// Token: 0x04002294 RID: 8852
		public string roomObjectId;

		// Token: 0x04002295 RID: 8853
		public string amount;
	}
}
