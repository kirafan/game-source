﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Star
{
	// Token: 0x02000642 RID: 1602
	public static class DEBUG
	{
		// Token: 0x06001FAC RID: 8108 RVA: 0x000ABE0F File Offset: 0x000AA20F
		[Conditional("UNITY_EDITOR")]
		public static void Log(object o)
		{
			UnityEngine.Debug.Log(o);
		}

		// Token: 0x06001FAD RID: 8109 RVA: 0x000ABE17 File Offset: 0x000AA217
		[Conditional("UNITY_EDITOR")]
		public static void Log(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.Log(message, context);
		}

		// Token: 0x06001FAE RID: 8110 RVA: 0x000ABE20 File Offset: 0x000AA220
		[Conditional("UNITY_EDITOR")]
		public static void LogAssertion(object message)
		{
		}

		// Token: 0x06001FAF RID: 8111 RVA: 0x000ABE22 File Offset: 0x000AA222
		[Conditional("UNITY_EDITOR")]
		public static void LogAssertion(object message, UnityEngine.Object context)
		{
		}

		// Token: 0x06001FB0 RID: 8112 RVA: 0x000ABE24 File Offset: 0x000AA224
		[Conditional("UNITY_EDITOR")]
		public static void LogAssertionFormat(string format, params object[] args)
		{
		}

		// Token: 0x06001FB1 RID: 8113 RVA: 0x000ABE26 File Offset: 0x000AA226
		[Conditional("UNITY_EDITOR")]
		public static void LogAssertionFormat(UnityEngine.Object context, string format, params object[] args)
		{
		}

		// Token: 0x06001FB2 RID: 8114 RVA: 0x000ABE28 File Offset: 0x000AA228
		[Conditional("UNITY_EDITOR")]
		public static void LogError(object message)
		{
			UnityEngine.Debug.LogError(message);
		}

		// Token: 0x06001FB3 RID: 8115 RVA: 0x000ABE30 File Offset: 0x000AA230
		[Conditional("UNITY_EDITOR")]
		public static void LogError(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogError(message, context);
		}

		// Token: 0x06001FB4 RID: 8116 RVA: 0x000ABE39 File Offset: 0x000AA239
		[Conditional("UNITY_EDITOR")]
		public static void LogErrorFormat(string format, params object[] args)
		{
			UnityEngine.Debug.LogErrorFormat(format, args);
		}

		// Token: 0x06001FB5 RID: 8117 RVA: 0x000ABE42 File Offset: 0x000AA242
		[Conditional("UNITY_EDITOR")]
		public static void LogErrorFormat(UnityEngine.Object context, string format, params object[] args)
		{
			UnityEngine.Debug.LogErrorFormat(context, format, args);
		}

		// Token: 0x06001FB6 RID: 8118 RVA: 0x000ABE4C File Offset: 0x000AA24C
		[Conditional("UNITY_EDITOR")]
		public static void LogException(Exception exception)
		{
			UnityEngine.Debug.LogException(exception);
		}

		// Token: 0x06001FB7 RID: 8119 RVA: 0x000ABE54 File Offset: 0x000AA254
		[Conditional("UNITY_EDITOR")]
		public static void LogException(Exception exception, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogException(exception, context);
		}

		// Token: 0x06001FB8 RID: 8120 RVA: 0x000ABE5D File Offset: 0x000AA25D
		[Conditional("UNITY_EDITOR")]
		public static void LogFormat(string format, params object[] args)
		{
			UnityEngine.Debug.LogFormat(format, args);
		}

		// Token: 0x06001FB9 RID: 8121 RVA: 0x000ABE66 File Offset: 0x000AA266
		[Conditional("UNITY_EDITOR")]
		public static void LogFormat(UnityEngine.Object context, string format, params object[] args)
		{
			UnityEngine.Debug.LogFormat(context, format, args);
		}

		// Token: 0x06001FBA RID: 8122 RVA: 0x000ABE70 File Offset: 0x000AA270
		[Conditional("UNITY_EDITOR")]
		public static void LogWarning(object message)
		{
			UnityEngine.Debug.LogWarning(message);
		}

		// Token: 0x06001FBB RID: 8123 RVA: 0x000ABE78 File Offset: 0x000AA278
		[Conditional("UNITY_EDITOR")]
		public static void LogWarning(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogWarning(message, context);
		}

		// Token: 0x06001FBC RID: 8124 RVA: 0x000ABE81 File Offset: 0x000AA281
		[Conditional("UNITY_EDITOR")]
		public static void LogWarningFormat(string format, params object[] args)
		{
			UnityEngine.Debug.LogWarningFormat(format, args);
		}

		// Token: 0x06001FBD RID: 8125 RVA: 0x000ABE8A File Offset: 0x000AA28A
		[Conditional("UNITY_EDITOR")]
		public static void LogWarningFormat(UnityEngine.Object context, string format, params object[] args)
		{
			UnityEngine.Debug.LogWarningFormat(context, format, args);
		}
	}
}
