﻿using System;

namespace Star
{
	// Token: 0x020001C9 RID: 457
	[Serializable]
	public struct LeaderSkillListDB_Data
	{
		// Token: 0x04000AED RID: 2797
		public int m_CondClass;

		// Token: 0x04000AEE RID: 2798
		public int m_CondElement;

		// Token: 0x04000AEF RID: 2799
		public int m_CondTitleType;

		// Token: 0x04000AF0 RID: 2800
		public int m_CondNamedType;

		// Token: 0x04000AF1 RID: 2801
		public int m_Type;

		// Token: 0x04000AF2 RID: 2802
		public float[] m_Args;
	}
}
