﻿using System;

namespace Star
{
	// Token: 0x0200056B RID: 1387
	public class ActXlsKeySE : ActXlsKeyBase
	{
		// Token: 0x06001B13 RID: 6931 RVA: 0x0008F722 File Offset: 0x0008DB22
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_SE);
		}

		// Token: 0x04002200 RID: 8704
		public int m_SE;
	}
}
