﻿using System;

namespace Star
{
	// Token: 0x020004AA RID: 1194
	public class MainComResultPlayerItem : IMainComCommand
	{
		// Token: 0x06001766 RID: 5990 RVA: 0x000794B7 File Offset: 0x000778B7
		public MainComResultPlayerItem(MainComResultPlayerItem.eCategory fcategory, long faddnum)
		{
			this.m_Category = fcategory;
			this.m_AddNum = faddnum;
		}

		// Token: 0x06001767 RID: 5991 RVA: 0x000794CD File Offset: 0x000778CD
		public override int GetHashCode()
		{
			return 1;
		}

		// Token: 0x04001E1A RID: 7706
		public MainComResultPlayerItem.eCategory m_Category;

		// Token: 0x04001E1B RID: 7707
		public long m_AddNum;

		// Token: 0x020004AB RID: 1195
		public enum eCategory
		{
			// Token: 0x04001E1D RID: 7709
			Money,
			// Token: 0x04001E1E RID: 7710
			KRRPoint,
			// Token: 0x04001E1F RID: 7711
			Stamina
		}
	}
}
