﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000082 RID: 130
	[Serializable]
	public class ActionAggregationChildActionAdapterExGen<GenType> : ActionAggregationChildActionAdapter where GenType : UnityEngine.Object
	{
		// Token: 0x06000409 RID: 1033 RVA: 0x00013F5F File Offset: 0x0001235F
		private void OnValidate()
		{
			if (this.m_Action == null)
			{
				this.m_Action = base.gameObject.GetComponent<GenType>();
			}
		}

		// Token: 0x0400022E RID: 558
		[SerializeField]
		[Tooltip("再生するアクション.")]
		protected GenType m_Action;
	}
}
