﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006C7 RID: 1735
	public class TownPartsModelBlink : ITownPartsAction
	{
		// Token: 0x0600226E RID: 8814 RVA: 0x000B7831 File Offset: 0x000B5C31
		public TownPartsModelBlink(GameObject phitnode, float fper, float fline, float fblinktime = 1f)
		{
			this.m_Active = true;
			this.m_CalcStep = TownPartsModelBlink.eCalcStep.Init;
			this.m_Per = fper;
			this.m_Line = fline;
			this.m_BlinkMaxTime = fblinktime;
			this.m_MsbHndl = phitnode.GetComponentsInChildren<MsbHandler>();
		}

		// Token: 0x0600226F RID: 8815 RVA: 0x000B7869 File Offset: 0x000B5C69
		public void SetEndMark()
		{
			this.m_Active = false;
		}

		// Token: 0x06002270 RID: 8816 RVA: 0x000B7874 File Offset: 0x000B5C74
		public override bool PartsUpdate()
		{
			TownPartsModelBlink.eCalcStep calcStep = this.m_CalcStep;
			if (calcStep != TownPartsModelBlink.eCalcStep.Init)
			{
				if (calcStep != TownPartsModelBlink.eCalcStep.Calc)
				{
					if (calcStep != TownPartsModelBlink.eCalcStep.End)
					{
					}
				}
				else
				{
					this.m_BlinkTime += Time.deltaTime;
					if (this.m_BlinkTime >= this.m_BlinkMaxTime)
					{
						this.m_BlinkTime -= this.m_BlinkMaxTime;
					}
					if (!this.m_Active)
					{
						this.m_BlinkTime = 0f;
						this.ResetBlinkModel();
						this.m_MsbHndl = null;
						this.m_CalcStep = TownPartsModelBlink.eCalcStep.End;
					}
					else
					{
						this.SetBlinkModel();
					}
				}
			}
			else
			{
				this.m_CalcStep = TownPartsModelBlink.eCalcStep.Calc;
			}
			return this.m_Active;
		}

		// Token: 0x06002271 RID: 8817 RVA: 0x000B7928 File Offset: 0x000B5D28
		private void SetBlinkModel()
		{
			float num = this.m_BlinkTime / this.m_BlinkMaxTime;
			Color white = Color.white;
			white.r = (white.g = (white.b = (Mathf.Cos(num * 3.1415927f * 2f) * 0.5f + 0.5f) * this.m_Per + this.m_Line));
			white.a = 1f;
			int num2 = this.m_MsbHndl.Length;
			for (int i = 0; i < num2; i++)
			{
				int msbObjectHandlerNum = this.m_MsbHndl[i].GetMsbObjectHandlerNum();
				for (int j = 0; j < msbObjectHandlerNum; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl[i].GetMsbObjectHandler(j);
					white.a = msbObjectHandler.GetWork().m_MeshColor.a;
					msbObjectHandler.GetWork().m_MeshColor = white;
				}
			}
		}

		// Token: 0x06002272 RID: 8818 RVA: 0x000B7A14 File Offset: 0x000B5E14
		private void ResetBlinkModel()
		{
			int num = this.m_MsbHndl.Length;
			for (int i = 0; i < num; i++)
			{
				int msbObjectHandlerNum = this.m_MsbHndl[i].GetMsbObjectHandlerNum();
				for (int j = 0; j < msbObjectHandlerNum; j++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl[i].GetMsbObjectHandler(j);
					Color white = Color.white;
					white.a = msbObjectHandler.GetWork().m_MeshColor.a;
					msbObjectHandler.GetWork().m_MeshColor = white;
				}
			}
		}

		// Token: 0x0400292B RID: 10539
		private TownPartsModelBlink.eCalcStep m_CalcStep;

		// Token: 0x0400292C RID: 10540
		private MsbHandler[] m_MsbHndl;

		// Token: 0x0400292D RID: 10541
		private float m_Per;

		// Token: 0x0400292E RID: 10542
		private float m_Line;

		// Token: 0x0400292F RID: 10543
		private float m_BlinkTime;

		// Token: 0x04002930 RID: 10544
		private float m_BlinkMaxTime;

		// Token: 0x020006C8 RID: 1736
		public enum eCalcStep
		{
			// Token: 0x04002932 RID: 10546
			Init,
			// Token: 0x04002933 RID: 10547
			Calc,
			// Token: 0x04002934 RID: 10548
			End
		}
	}
}
