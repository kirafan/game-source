﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020003B1 RID: 945
	public class RoomServerForm
	{
		// Token: 0x020003B2 RID: 946
		public enum eDataType
		{
			// Token: 0x04001885 RID: 6277
			RoomList,
			// Token: 0x04001886 RID: 6278
			RoomData
		}

		// Token: 0x020003B3 RID: 947
		public class RoomData
		{
			// Token: 0x04001887 RID: 6279
			public long m_BG;

			// Token: 0x04001888 RID: 6280
			public long m_Wall;

			// Token: 0x04001889 RID: 6281
			public long m_Floor;

			// Token: 0x0400188A RID: 6282
			public int m_RoomID;

			// Token: 0x0400188B RID: 6283
			public List<UserRoomData.PlacementData> m_Table;
		}

		// Token: 0x020003B4 RID: 948
		public class RoomList
		{
			// Token: 0x0400188C RID: 6284
			public long m_ActiveRoomID;

			// Token: 0x0400188D RID: 6285
			public long[] m_List;
		}

		// Token: 0x020003B5 RID: 949
		public class DataChunk
		{
			// Token: 0x0400188E RID: 6286
			public int m_Ver;

			// Token: 0x0400188F RID: 6287
			public int m_DataType;

			// Token: 0x04001890 RID: 6288
			public byte[] m_Data;
		}
	}
}
