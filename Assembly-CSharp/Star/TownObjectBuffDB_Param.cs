﻿using System;

namespace Star
{
	// Token: 0x02000243 RID: 579
	[Serializable]
	public struct TownObjectBuffDB_Param
	{
		// Token: 0x0400131C RID: 4892
		public int m_ID;

		// Token: 0x0400131D RID: 4893
		public TownObjectBuffDB_Data[] m_Datas;
	}
}
