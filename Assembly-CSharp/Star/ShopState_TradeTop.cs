﻿using System;
using ItemTradeResponseTypes;
using Meige;
using Star.UI;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x0200048B RID: 1163
	public class ShopState_TradeTop : ShopState
	{
		// Token: 0x060016C5 RID: 5829 RVA: 0x00076CC7 File Offset: 0x000750C7
		public ShopState_TradeTop(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016C6 RID: 5830 RVA: 0x00076CDF File Offset: 0x000750DF
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x060016C7 RID: 5831 RVA: 0x00076CE2 File Offset: 0x000750E2
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_TradeTop.eStep.First;
		}

		// Token: 0x060016C8 RID: 5832 RVA: 0x00076CEB File Offset: 0x000750EB
		public override void OnStateExit()
		{
		}

		// Token: 0x060016C9 RID: 5833 RVA: 0x00076CED File Offset: 0x000750ED
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016CA RID: 5834 RVA: 0x00076CF8 File Offset: 0x000750F8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_TradeTop.eStep.First:
			{
				int tradeListNum = SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.GetTradeListNum();
				if (tradeListNum == 0 || this.m_Owner.IsTradeListReset)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_GetTradeRecipe(new Action<MeigewwwParam, Getrecipe>(this.OnResponse_GetRecipe));
					this.m_Owner.IsTradeListReset = false;
					this.m_Step = ShopState_TradeTop.eStep.Request_Wait;
				}
				else
				{
					this.m_Step = ShopState_TradeTop.eStep.LoadStart;
				}
				break;
			}
			case ShopState_TradeTop.eStep.LoadStart:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_TradeTop.eStep.LoadWait;
				break;
			case ShopState_TradeTop.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeTop.eStep.PlayIn;
				}
				break;
			case ShopState_TradeTop.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_TradeTop.eStep.Main;
				}
				break;
			case ShopState_TradeTop.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case ShopTradeTopUI.eMainButton.Trade:
							this.m_NextState = 3;
							this.m_Owner.IsLimitList = false;
							break;
						case ShopTradeTopUI.eMainButton.Limit:
							this.m_NextState = 3;
							this.m_Owner.IsLimitList = true;
							break;
						case ShopTradeTopUI.eMainButton.Sale:
							this.m_NextState = 4;
							break;
						default:
							this.m_NextState = 1;
							break;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_TradeTop.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_TradeTop.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_TradeTop.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016CB RID: 5835 RVA: 0x00076EF0 File Offset: 0x000752F0
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopTradeTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			bool existLimitTrade = SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.GetTradeList(true).Count > 0;
			this.m_UI.Setup(existLimitTrade);
			this.m_UI.OnClickMainButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.ShopTrade);
			this.m_UI.PlayIn();
			if (this.m_Owner.IsRequestVocie)
			{
				this.m_Owner.m_NpcUI.PlayInVoice(eSoundVoiceControllListDB.Cork_in);
				this.m_Owner.IsRequestVocie = false;
			}
			this.m_Owner.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Trade);
			return true;
		}

		// Token: 0x060016CC RID: 5836 RVA: 0x00076FE8 File Offset: 0x000753E8
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_007, "voice_213", true);
			this.m_Owner.m_NpcUI.PlayOut();
			this.GoToMenuEnd();
		}

		// Token: 0x060016CD RID: 5837 RVA: 0x0007701F File Offset: 0x0007541F
		private void OnClickButtonCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060016CE RID: 5838 RVA: 0x00077027 File Offset: 0x00075427
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060016CF RID: 5839 RVA: 0x00077044 File Offset: 0x00075444
		private void OnResponse_GetRecipe(MeigewwwParam wwwParam, Getrecipe param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				this.m_Step = ShopState_TradeTop.eStep.LoadStart;
			}
		}

		// Token: 0x04001D7F RID: 7551
		private ShopState_TradeTop.eStep m_Step = ShopState_TradeTop.eStep.None;

		// Token: 0x04001D80 RID: 7552
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopTradeTopUI;

		// Token: 0x04001D81 RID: 7553
		private ShopTradeTopUI m_UI;

		// Token: 0x0200048C RID: 1164
		private enum eStep
		{
			// Token: 0x04001D83 RID: 7555
			None = -1,
			// Token: 0x04001D84 RID: 7556
			First,
			// Token: 0x04001D85 RID: 7557
			Request_Wait,
			// Token: 0x04001D86 RID: 7558
			LoadStart,
			// Token: 0x04001D87 RID: 7559
			LoadWait,
			// Token: 0x04001D88 RID: 7560
			PlayIn,
			// Token: 0x04001D89 RID: 7561
			Main,
			// Token: 0x04001D8A RID: 7562
			UnloadChildSceneWait
		}
	}
}
