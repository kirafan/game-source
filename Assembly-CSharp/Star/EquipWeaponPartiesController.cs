﻿using System;

namespace Star
{
	// Token: 0x020002E5 RID: 741
	public class EquipWeaponPartiesController
	{
		// Token: 0x06000E6E RID: 3694 RVA: 0x0004DFC4 File Offset: 0x0004C3C4
		public virtual EquipWeaponPartiesController Setup()
		{
			return this;
		}

		// Token: 0x06000E6F RID: 3695 RVA: 0x0004DFC7 File Offset: 0x0004C3C7
		public virtual int HowManyParties()
		{
			return -1;
		}

		// Token: 0x06000E70 RID: 3696 RVA: 0x0004DFCA File Offset: 0x0004C3CA
		public virtual int HowManyPartyMemberAll()
		{
			return -1;
		}

		// Token: 0x06000E71 RID: 3697 RVA: 0x0004DFCD File Offset: 0x0004C3CD
		public virtual int HowManyPartyMemberEnable()
		{
			return -1;
		}

		// Token: 0x06000E72 RID: 3698 RVA: 0x0004DFD0 File Offset: 0x0004C3D0
		public virtual EquipWeaponPartyController GetParty(int index)
		{
			return null;
		}
	}
}
