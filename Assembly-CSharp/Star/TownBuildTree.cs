﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Star
{
	// Token: 0x020006FD RID: 1789
	public class TownBuildTree
	{
		// Token: 0x0600236F RID: 9071 RVA: 0x000BE3F1 File Offset: 0x000BC7F1
		public TownBuildTree(TownBuilder pbuilder)
		{
			this.m_Builder = pbuilder;
		}

		// Token: 0x06002370 RID: 9072 RVA: 0x000BE40C File Offset: 0x000BC80C
		public void AddBuildPoint(TownBuildLinkPoint ppoint)
		{
			UserTownData.BuildObjectData townPointToBuildData = UserTownUtil.GetTownPointToBuildData(ppoint.m_AccessID);
			if (townPointToBuildData != null)
			{
				ppoint.m_AccessMngID = townPointToBuildData.m_ManageID;
			}
			this.m_List.Add(ppoint);
			this.SortToBuildPoint();
		}

		// Token: 0x06002371 RID: 9073 RVA: 0x000BE44C File Offset: 0x000BC84C
		public void DeletePoint(int fbuildpoint)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].m_AccessID == fbuildpoint)
				{
					if (this.m_List[i].m_Pakage != null)
					{
						this.m_List[i].m_Pakage.DestroyBuildMode();
						this.m_Builder.EventRequest(new TownPakageDestroy(this.m_List[i]));
					}
					this.m_List.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x06002372 RID: 9074 RVA: 0x000BE4F0 File Offset: 0x000BC8F0
		public void Destroy()
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				this.m_List[i].Release();
			}
			this.m_List = null;
		}

		// Token: 0x06002373 RID: 9075 RVA: 0x000BE533 File Offset: 0x000BC933
		public int GetTreeNodeNum()
		{
			return this.m_List.Count;
		}

		// Token: 0x06002374 RID: 9076 RVA: 0x000BE540 File Offset: 0x000BC940
		public TownBuildLinkPoint GetBuildPointTreeNode(int fbuildpoint)
		{
			int count = this.m_List.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_List[i].m_AccessID == fbuildpoint)
				{
					return this.m_List[i];
				}
			}
			return null;
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x000BE590 File Offset: 0x000BC990
		public TownBuildLinkPoint GetBuildMngIDTreeNode(long fbuildmngid, int fbuildpoint = -1)
		{
			int count = this.m_List.Count;
			if (fbuildmngid != -1L)
			{
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_AccessMngID == fbuildmngid)
					{
						return this.m_List[i];
					}
				}
			}
			if (fbuildpoint != -1)
			{
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_AccessID == fbuildpoint)
					{
						this.m_List[i].ChangeManageID(fbuildmngid);
						return this.m_List[i];
					}
				}
			}
			return null;
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x000BE637 File Offset: 0x000BCA37
		public TownBuildLinkPoint GetIndexToNode(int findex)
		{
			return this.m_List[findex];
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x000BE645 File Offset: 0x000BCA45
		private static int CompToBuildPoint(TownBuildLinkPoint pcomp1, TownBuildLinkPoint pcomp2)
		{
			if (pcomp1.m_AccessID > pcomp2.m_AccessID)
			{
				return 1;
			}
			if (pcomp1.m_AccessID < pcomp2.m_AccessID)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x06002378 RID: 9080 RVA: 0x000BE66E File Offset: 0x000BCA6E
		public void SortToBuildPoint()
		{
			List<TownBuildLinkPoint> list = this.m_List;
			if (TownBuildTree.<>f__mg$cache0 == null)
			{
				TownBuildTree.<>f__mg$cache0 = new Comparison<TownBuildLinkPoint>(TownBuildTree.CompToBuildPoint);
			}
			list.Sort(TownBuildTree.<>f__mg$cache0);
		}

		// Token: 0x04002A3F RID: 10815
		private List<TownBuildLinkPoint> m_List = new List<TownBuildLinkPoint>();

		// Token: 0x04002A40 RID: 10816
		private TownBuilder m_Builder;

		// Token: 0x04002A41 RID: 10817
		[CompilerGenerated]
		private static Comparison<TownBuildLinkPoint> <>f__mg$cache0;
	}
}
