﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200066C RID: 1644
	[Serializable]
	public class TownCacheObjectData
	{
		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06002156 RID: 8534 RVA: 0x000B150C File Offset: 0x000AF90C
		public GameObject Prefab
		{
			get
			{
				return this.m_Prefab;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06002157 RID: 8535 RVA: 0x000B1514 File Offset: 0x000AF914
		public int CacheSize
		{
			get
			{
				return this.m_CacheSize;
			}
		}

		// Token: 0x06002158 RID: 8536 RVA: 0x000B151C File Offset: 0x000AF91C
		public void Initialize(GameObject prefab, int cacheSize, Transform parent)
		{
			this.m_Prefab = prefab;
			this.m_CacheSize = cacheSize;
			this.Initialize(parent);
		}

		// Token: 0x06002159 RID: 8537 RVA: 0x000B1534 File Offset: 0x000AF934
		public void Initialize(Transform parent)
		{
			this.m_Parent = parent;
			this.m_Objects = new TownCacheObjectData.TCacheObjectState[this.m_CacheSize];
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				this.m_Objects[i] = new TownCacheObjectData.TCacheObjectState();
				GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.m_Prefab);
				this.m_Objects[i].m_Object = new GameObject(gameObject.name + i);
				this.m_Objects[i].m_Object.transform.SetParent(this.m_Parent, false);
				this.m_Objects[i].m_Object.SetActive(false);
				gameObject.transform.SetParent(this.m_Objects[i].m_Object.transform, false);
			}
		}

		// Token: 0x0600215A RID: 8538 RVA: 0x000B15FC File Offset: 0x000AF9FC
		public void Destroy()
		{
			if (this.m_Objects != null)
			{
				for (int i = 0; i < this.m_Objects.Length; i++)
				{
					UnityEngine.Object.Destroy(this.m_Objects[i].m_Object);
					this.m_Objects[i].m_Object = null;
				}
				this.m_Objects = null;
			}
			this.m_Prefab = null;
			this.m_CacheSize = 0;
		}

		// Token: 0x0600215B RID: 8539 RVA: 0x000B1664 File Offset: 0x000AFA64
		public GameObject GetNextObjectInCache()
		{
			GameObject gameObject = null;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				if (!this.m_Objects[i].m_Active)
				{
					gameObject = this.m_Objects[i].m_Object;
					this.m_Objects[i].m_Active = true;
					break;
				}
			}
			if (gameObject == null)
			{
				TownCacheObjectData.TCacheObjectState[] array = new TownCacheObjectData.TCacheObjectState[this.m_CacheSize + 4];
				for (int j = 0; j < this.m_CacheSize; j++)
				{
					array[j] = this.m_Objects[j];
				}
				for (int j = this.m_CacheSize; j < this.m_CacheSize + 4; j++)
				{
					array[j] = new TownCacheObjectData.TCacheObjectState();
					GameObject gameObject2 = UnityEngine.Object.Instantiate<GameObject>(this.m_Prefab);
					array[j].m_Object = new GameObject(gameObject2.name + j);
					array[j].m_Object.transform.SetParent(this.m_Parent, false);
					array[j].m_Object.SetActive(false);
					gameObject2.transform.SetParent(array[j].m_Object.transform, false);
				}
				this.m_Objects = null;
				this.m_Objects = array;
				gameObject = this.m_Objects[this.m_CacheSize].m_Object;
				this.m_Objects[this.m_CacheSize].m_Active = true;
				this.m_CacheSize += 4;
			}
			return gameObject;
		}

		// Token: 0x0600215C RID: 8540 RVA: 0x000B17DC File Offset: 0x000AFBDC
		public void FreeObjectInCache(GameObject pobj)
		{
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				if (this.m_Objects[i].m_Object == pobj)
				{
					this.m_Objects[i].m_Active = false;
					this.m_Objects[i].m_Object.transform.SetParent(this.m_Parent, false);
					this.m_Objects[i].m_Object.SetActive(false);
					break;
				}
			}
		}

		// Token: 0x040027AF RID: 10159
		[SerializeField]
		private GameObject m_Prefab;

		// Token: 0x040027B0 RID: 10160
		[SerializeField]
		private int m_CacheSize = 1;

		// Token: 0x040027B1 RID: 10161
		private TownCacheObjectData.TCacheObjectState[] m_Objects;

		// Token: 0x040027B2 RID: 10162
		private Transform m_Parent;

		// Token: 0x0200066D RID: 1645
		public class TCacheObjectState
		{
			// Token: 0x040027B3 RID: 10163
			public GameObject m_Object;

			// Token: 0x040027B4 RID: 10164
			public bool m_Active;
		}
	}
}
