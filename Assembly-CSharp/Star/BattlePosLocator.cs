﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000DD RID: 221
	public class BattlePosLocator : MonoBehaviour
	{
		// Token: 0x06000605 RID: 1541 RVA: 0x0001E933 File Offset: 0x0001CD33
		public Transform GetLocator(int index)
		{
			if (this.m_Locators != null && index >= 0 && index < this.m_Locators.Length)
			{
				return this.m_Locators[index];
			}
			return null;
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x0001E960 File Offset: 0x0001CD60
		public Vector3 GetLocatorPos(int index)
		{
			if (this.m_Locators != null && index >= 0 && index < this.m_Locators.Length)
			{
				return new Vector3(this.m_Locators[index].position.x / this.m_Locators[index].lossyScale.x, this.m_Locators[index].position.y / this.m_Locators[index].lossyScale.y, this.m_Locators[index].position.z / this.m_Locators[index].lossyScale.z);
			}
			return Vector3.zero;
		}

		// Token: 0x06000607 RID: 1543 RVA: 0x0001EA1B File Offset: 0x0001CE1B
		public Transform GetBackLocator(int index)
		{
			if (this.m_Locators != null && index >= 0 && index < this.m_BackLocators.Length)
			{
				return this.m_BackLocators[index];
			}
			return null;
		}

		// Token: 0x06000608 RID: 1544 RVA: 0x0001EA48 File Offset: 0x0001CE48
		public Vector3 GetBackLocatorPos(int index)
		{
			if (this.m_Locators != null && index >= 0 && index < this.m_BackLocators.Length)
			{
				return new Vector3(this.m_BackLocators[index].position.x / this.m_BackLocators[index].lossyScale.x, this.m_BackLocators[index].position.y / this.m_BackLocators[index].lossyScale.y, this.m_BackLocators[index].position.z / this.m_BackLocators[index].lossyScale.z);
			}
			return Vector3.zero;
		}

		// Token: 0x040004A6 RID: 1190
		[SerializeField]
		private Transform[] m_Locators;

		// Token: 0x040004A7 RID: 1191
		[SerializeField]
		private Transform[] m_BackLocators;
	}
}
