﻿using System;
using Star.UI;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x020004C5 RID: 1221
	public class TrainingState_Init : TrainingState
	{
		// Token: 0x06001801 RID: 6145 RVA: 0x0007CE6D File Offset: 0x0007B26D
		public TrainingState_Init(TrainingMain owner) : base(owner)
		{
		}

		// Token: 0x06001802 RID: 6146 RVA: 0x0007CE85 File Offset: 0x0007B285
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001803 RID: 6147 RVA: 0x0007CE88 File Offset: 0x0007B288
		public override void OnStateEnter()
		{
			this.m_Step = TrainingState_Init.eStep.First;
		}

		// Token: 0x06001804 RID: 6148 RVA: 0x0007CE91 File Offset: 0x0007B291
		public override void OnStateExit()
		{
		}

		// Token: 0x06001805 RID: 6149 RVA: 0x0007CE93 File Offset: 0x0007B293
		public override void OnDispose()
		{
		}

		// Token: 0x06001806 RID: 6150 RVA: 0x0007CE98 File Offset: 0x0007B298
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case TrainingState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					this.m_Step = TrainingState_Init.eStep.Load_Wait;
				}
				break;
			case TrainingState_Init.eStep.Load_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Training, false);
					this.m_Step = TrainingState_Init.eStep.Load_WaitBG;
				}
				break;
			case TrainingState_Init.eStep.Load_WaitBG:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					this.m_Step = TrainingState_Init.eStep.SetupUI;
				}
				break;
			case TrainingState_Init.eStep.SetupUI:
				if (this.SetupUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = TrainingState_Init.eStep.End;
				}
				break;
			case TrainingState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = TrainingState_Init.eStep.None;
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001807 RID: 6151 RVA: 0x0007CFF8 File Offset: 0x0007B3F8
		private bool SetupUI()
		{
			this.m_Owner.m_NpcUI = UIUtility.GetMenuComponent<NPCCharaDisplayUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.m_NpcUI == null)
			{
				return false;
			}
			this.m_Owner.m_NpcUI.Setup();
			return true;
		}

		// Token: 0x06001808 RID: 6152 RVA: 0x0007D056 File Offset: 0x0007B456
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x04001EC6 RID: 7878
		private TrainingState_Init.eStep m_Step = TrainingState_Init.eStep.None;

		// Token: 0x04001EC7 RID: 7879
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.NPCCharaDisplayUI;

		// Token: 0x020004C6 RID: 1222
		private enum eStep
		{
			// Token: 0x04001EC9 RID: 7881
			None = -1,
			// Token: 0x04001ECA RID: 7882
			First,
			// Token: 0x04001ECB RID: 7883
			Load_Wait,
			// Token: 0x04001ECC RID: 7884
			Load_WaitBG,
			// Token: 0x04001ECD RID: 7885
			SetupUI,
			// Token: 0x04001ECE RID: 7886
			End
		}
	}
}
