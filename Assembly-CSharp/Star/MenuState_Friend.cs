﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x0200042F RID: 1071
	public class MenuState_Friend : MenuState
	{
		// Token: 0x06001491 RID: 5265 RVA: 0x0006C5F3 File Offset: 0x0006A9F3
		public MenuState_Friend(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x06001492 RID: 5266 RVA: 0x0006C60B File Offset: 0x0006AA0B
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001493 RID: 5267 RVA: 0x0006C60E File Offset: 0x0006AA0E
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Friend.eStep.First;
		}

		// Token: 0x06001494 RID: 5268 RVA: 0x0006C617 File Offset: 0x0006AA17
		public override void OnStateExit()
		{
		}

		// Token: 0x06001495 RID: 5269 RVA: 0x0006C619 File Offset: 0x0006AA19
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001496 RID: 5270 RVA: 0x0006C624 File Offset: 0x0006AA24
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Friend.eStep.First:
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuFriendStart = false;
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Friend.eStep.LoadWait;
				break;
			case MenuState_Friend.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Friend.eStep.PlayIn;
				}
				break;
			case MenuState_Friend.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Friend.eStep.Main;
				}
				break;
			case MenuState_Friend.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (this.m_IsRoomInMode)
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
						this.m_NextState = 2147483646;
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome)
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Friend.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Friend.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Friend.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001497 RID: 5271 RVA: 0x0006C7AC File Offset: 0x0006ABAC
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuFriendUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			if (!SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsMenuBackToHome)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Friend);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Friend);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.OnFriendRoom += this.OnFriendRoomCallBack;
			return true;
		}

		// Token: 0x06001498 RID: 5272 RVA: 0x0006C87C File Offset: 0x0006AC7C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Friend)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001499 RID: 5273 RVA: 0x0006C8D5 File Offset: 0x0006ACD5
		private void OnFriendRoomCallBack()
		{
			this.m_IsRoomInMode = true;
			this.GoToMenuEnd();
		}

		// Token: 0x0600149A RID: 5274 RVA: 0x0006C8E4 File Offset: 0x0006ACE4
		private void OnClickButton()
		{
		}

		// Token: 0x0600149B RID: 5275 RVA: 0x0006C8E6 File Offset: 0x0006ACE6
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001B7C RID: 7036
		private MenuState_Friend.eStep m_Step = MenuState_Friend.eStep.None;

		// Token: 0x04001B7D RID: 7037
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuFriendUI;

		// Token: 0x04001B7E RID: 7038
		private MenuFriendUI m_UI;

		// Token: 0x04001B7F RID: 7039
		private bool m_IsRoomInMode;

		// Token: 0x02000430 RID: 1072
		private enum eStep
		{
			// Token: 0x04001B81 RID: 7041
			None = -1,
			// Token: 0x04001B82 RID: 7042
			First,
			// Token: 0x04001B83 RID: 7043
			LoadWait,
			// Token: 0x04001B84 RID: 7044
			PlayIn,
			// Token: 0x04001B85 RID: 7045
			Main,
			// Token: 0x04001B86 RID: 7046
			UnloadChildSceneWait
		}
	}
}
