﻿using System;

namespace Star
{
	// Token: 0x02000315 RID: 789
	public class FieldBuilCmdBase
	{
		// Token: 0x06000F0F RID: 3855 RVA: 0x00050B66 File Offset: 0x0004EF66
		public void CheckCallback(bool fsuccess)
		{
			if (this.m_Callback != null)
			{
				this.m_Callback(fsuccess);
			}
		}

		// Token: 0x0400168C RID: 5772
		public FieldBuilCmdBase.eCmd m_Cmd;

		// Token: 0x0400168D RID: 5773
		public uint m_AccessKey;

		// Token: 0x0400168E RID: 5774
		public Action<bool> m_Callback;

		// Token: 0x02000316 RID: 790
		public enum eCmd
		{
			// Token: 0x04001690 RID: 5776
			Add,
			// Token: 0x04001691 RID: 5777
			Move,
			// Token: 0x04001692 RID: 5778
			Swap,
			// Token: 0x04001693 RID: 5779
			Remove,
			// Token: 0x04001694 RID: 5780
			LevelUp
		}
	}
}
