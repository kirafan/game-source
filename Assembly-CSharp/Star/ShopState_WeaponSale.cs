﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerResponseTypes;
using Star.UI.Global;
using Star.UI.Shop;
using Star.UI.WeaponList;
using WWWTypes;

namespace Star
{
	// Token: 0x0200048F RID: 1167
	public class ShopState_WeaponSale : ShopState
	{
		// Token: 0x060016DB RID: 5851 RVA: 0x0007738A File Offset: 0x0007578A
		public ShopState_WeaponSale(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016DC RID: 5852 RVA: 0x000773AA File Offset: 0x000757AA
		public override int GetStateID()
		{
			return 14;
		}

		// Token: 0x060016DD RID: 5853 RVA: 0x000773AE File Offset: 0x000757AE
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponSale.eStep.First;
		}

		// Token: 0x060016DE RID: 5854 RVA: 0x000773B7 File Offset: 0x000757B7
		public override void OnStateExit()
		{
		}

		// Token: 0x060016DF RID: 5855 RVA: 0x000773B9 File Offset: 0x000757B9
		public override void OnDispose()
		{
			this.m_ListUI = null;
			this.m_UI = null;
		}

		// Token: 0x060016E0 RID: 5856 RVA: 0x000773CC File Offset: 0x000757CC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponSale.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponSale.eStep.LoadWait;
				break;
			case ShopState_WeaponSale.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponSale.eStep.LoadStartList;
				}
				break;
			case ShopState_WeaponSale.eStep.LoadStartList:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ListSceneID, true);
				this.m_Step = ShopState_WeaponSale.eStep.LoadWaitList;
				break;
			case ShopState_WeaponSale.eStep.LoadWaitList:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ListSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ListSceneID);
					this.m_Step = ShopState_WeaponSale.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponSale.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_WeaponSale.eStep.Main;
				}
				break;
			case ShopState_WeaponSale.eStep.Main:
				if (this.m_ListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 10;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ListSceneID);
					this.m_Step = ShopState_WeaponSale.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponSale.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID) && SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ListSceneID))
				{
					this.m_Step = ShopState_WeaponSale.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016E1 RID: 5857 RVA: 0x00077570 File Offset: 0x00075970
		private bool SetupAndPlayUI()
		{
			this.m_ListUI = UIUtility.GetMenuComponent<WeaponListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ListSceneID].m_SceneName);
			if (this.m_ListUI == null)
			{
				return false;
			}
			this.m_UI = UIUtility.GetMenuComponent<ShopWeaponSaleUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_ListUI.SetupSale();
			this.m_ListUI.OnClickButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponSale);
			this.m_ListUI.PlayIn();
			this.m_UI.Setup();
			this.m_UI.OnClickExecuteButton += this.OnClickExecuteButton;
			return true;
		}

		// Token: 0x060016E2 RID: 5858 RVA: 0x00077667 File Offset: 0x00075A67
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060016E3 RID: 5859 RVA: 0x00077678 File Offset: 0x00075A78
		protected void OnClickButtonCallBack()
		{
			if (this.m_ListUI.SelectButton == WeaponListUI.eButton.Sale)
			{
				this.m_UI.PlayIn(this.m_ListUI.SelectedWeaponList);
			}
			else if (this.m_ListUI.SelectButton != WeaponListUI.eButton.Weapon)
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060016E4 RID: 5860 RVA: 0x000776CC File Offset: 0x00075ACC
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_ListUI.PlayOut();
			this.m_UI.PlayOut();
		}

		// Token: 0x060016E5 RID: 5861 RVA: 0x000776F3 File Offset: 0x00075AF3
		private void OnClickExecuteButton(List<long> weaponMngIDs)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_WeaponSale(weaponMngIDs, new Action<MeigewwwParam, Weaponsale>(this.OnResponse_WeaponSale));
		}

		// Token: 0x060016E6 RID: 5862 RVA: 0x00077714 File Offset: 0x00075B14
		private void OnResponse_WeaponSale(MeigewwwParam wwwParam, Weaponsale param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_SELL, 1f, 0, -1, -1);
				this.m_ListUI.Refresh();
				this.m_UI.PlayOut();
			}
		}

		// Token: 0x04001D96 RID: 7574
		private ShopState_WeaponSale.eStep m_Step = ShopState_WeaponSale.eStep.None;

		// Token: 0x04001D97 RID: 7575
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopWeaponSaleUI;

		// Token: 0x04001D98 RID: 7576
		private SceneDefine.eChildSceneID m_ListSceneID = SceneDefine.eChildSceneID.WeaponListUI;

		// Token: 0x04001D99 RID: 7577
		private WeaponListUI m_ListUI;

		// Token: 0x04001D9A RID: 7578
		private ShopWeaponSaleUI m_UI;

		// Token: 0x02000490 RID: 1168
		private enum eStep
		{
			// Token: 0x04001D9C RID: 7580
			None = -1,
			// Token: 0x04001D9D RID: 7581
			First,
			// Token: 0x04001D9E RID: 7582
			LoadWait,
			// Token: 0x04001D9F RID: 7583
			LoadStartList,
			// Token: 0x04001DA0 RID: 7584
			LoadWaitList,
			// Token: 0x04001DA1 RID: 7585
			PlayIn,
			// Token: 0x04001DA2 RID: 7586
			List,
			// Token: 0x04001DA3 RID: 7587
			Main,
			// Token: 0x04001DA4 RID: 7588
			UnloadChildSceneWait
		}
	}
}
