﻿using System;

namespace Star
{
	// Token: 0x0200021A RID: 538
	public enum eSoundVoiceControllListDB
	{
		// Token: 0x04000DF9 RID: 3577
		TitleCall,
		// Token: 0x04000DFA RID: 3578
		TitleStart,
		// Token: 0x04000DFB RID: 3579
		Logo_A,
		// Token: 0x04000DFC RID: 3580
		Logo_B,
		// Token: 0x04000DFD RID: 3581
		Logo_C,
		// Token: 0x04000DFE RID: 3582
		Logo_D,
		// Token: 0x04000DFF RID: 3583
		Kanna_in,
		// Token: 0x04000E00 RID: 3584
		Raine_in,
		// Token: 0x04000E01 RID: 3585
		Poruka_in,
		// Token: 0x04000E02 RID: 3586
		Claire_in,
		// Token: 0x04000E03 RID: 3587
		Cork_in,
		// Token: 0x04000E04 RID: 3588
		Max
	}
}
