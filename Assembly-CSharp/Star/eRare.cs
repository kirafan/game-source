﻿using System;

namespace Star
{
	// Token: 0x02000226 RID: 550
	public enum eRare
	{
		// Token: 0x04000F02 RID: 3842
		None = -1,
		// Token: 0x04000F03 RID: 3843
		Star1,
		// Token: 0x04000F04 RID: 3844
		Star2,
		// Token: 0x04000F05 RID: 3845
		Star3,
		// Token: 0x04000F06 RID: 3846
		Star4,
		// Token: 0x04000F07 RID: 3847
		Star5,
		// Token: 0x04000F08 RID: 3848
		Num
	}
}
