﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200028C RID: 652
	public class PrefabEffectViewerMain : MonoBehaviour
	{
		// Token: 0x06000C2F RID: 3119 RVA: 0x000476CA File Offset: 0x00045ACA
		private void Start()
		{
			this.m_Step = PrefabEffectViewerMain.eStep.First;
			this.m_UseCreatorNum = Mathf.Clamp(this.m_UseCreatorNum, 1, this.m_Creators.Length);
		}

		// Token: 0x06000C30 RID: 3120 RVA: 0x000476F0 File Offset: 0x00045AF0
		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Space))
			{
				Debug.Break();
			}
			PrefabEffectViewerMain.eStep step = this.m_Step;
			if (step != PrefabEffectViewerMain.eStep.First)
			{
				if (step != PrefabEffectViewerMain.eStep.Main)
				{
				}
			}
			else if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				for (int i = 0; i < this.m_UseCreatorNum; i++)
				{
					this.m_Creators[i].Setup();
				}
				this.m_Step = PrefabEffectViewerMain.eStep.Main;
			}
		}

		// Token: 0x06000C31 RID: 3121 RVA: 0x00047784 File Offset: 0x00045B84
		public void OnClickPlayOneShotButton()
		{
			for (int i = 0; i < this.m_UseCreatorNum; i++)
			{
				base.StartCoroutine(this.m_Creators[i].PlayOneShot(Vector3.zero, null));
			}
		}

		// Token: 0x06000C32 RID: 3122 RVA: 0x000477C4 File Offset: 0x00045BC4
		public void OnClickPlayLoopButton()
		{
			for (int i = 0; i < this.m_UseCreatorNum; i++)
			{
				base.StartCoroutine(this.m_Creators[i].PlayLoop(Vector3.zero, null));
			}
		}

		// Token: 0x06000C33 RID: 3123 RVA: 0x00047802 File Offset: 0x00045C02
		public void OnChangedZoom(float value)
		{
			this.m_Camera.orthographicSize = this.m_CameraSizeMin + (this.m_CameraSizeMax - this.m_CameraSizeMin) * value;
		}

		// Token: 0x040014DD RID: 5341
		[SerializeField]
		private PrefabEffectCreator[] m_Creators;

		// Token: 0x040014DE RID: 5342
		[SerializeField]
		private int m_UseCreatorNum = 1;

		// Token: 0x040014DF RID: 5343
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x040014E0 RID: 5344
		[SerializeField]
		private float m_CameraSizeMin = 0.5f;

		// Token: 0x040014E1 RID: 5345
		[SerializeField]
		private float m_CameraSizeMax = 1.3f;

		// Token: 0x040014E2 RID: 5346
		private PrefabEffectViewerMain.eStep m_Step = PrefabEffectViewerMain.eStep.None;

		// Token: 0x0200028D RID: 653
		private enum eStep
		{
			// Token: 0x040014E4 RID: 5348
			None = -1,
			// Token: 0x040014E5 RID: 5349
			First,
			// Token: 0x040014E6 RID: 5350
			Main
		}
	}
}
