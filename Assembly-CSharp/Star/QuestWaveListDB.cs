﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001EC RID: 492
	public class QuestWaveListDB : ScriptableObject
	{
		// Token: 0x04000BEC RID: 3052
		public QuestWaveListDB_Param[] m_Params;
	}
}
