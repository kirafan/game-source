﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002E8 RID: 744
	public class EquipWeaponManagedBattlePartiesController : EquipWeaponManagedPartiesControllerExGen<EquipWeaponManagedBattlePartiesController, EquipWeaponManagedBattlePartyController>
	{
		// Token: 0x06000E7A RID: 3706 RVA: 0x0004E019 File Offset: 0x0004C419
		public EquipWeaponManagedBattlePartiesController SetupEx(CharacterDataWrapperBase supportMember = null)
		{
			return this.SetupProcess(supportMember);
		}

		// Token: 0x06000E7B RID: 3707 RVA: 0x0004E022 File Offset: 0x0004C422
		public override EquipWeaponManagedBattlePartiesController SetupEx()
		{
			return this.SetupProcess(null);
		}

		// Token: 0x06000E7C RID: 3708 RVA: 0x0004E02C File Offset: 0x0004C42C
		protected EquipWeaponManagedBattlePartiesController SetupProcess(CharacterDataWrapperBase supportMember = null)
		{
			base.Setup();
			this.m_SupportMember = supportMember;
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			List<UserBattlePartyData> userBattlePartyDatas = userDataMng.UserBattlePartyDatas;
			this.parties = new EquipWeaponManagedBattlePartyController[userBattlePartyDatas.Count];
			for (int i = 0; i < this.parties.Length; i++)
			{
				this.parties[i] = new EquipWeaponManagedBattlePartyController().SetupEx(i, this.m_SupportMember);
			}
			return this;
		}

		// Token: 0x06000E7D RID: 3709 RVA: 0x0004E09D File Offset: 0x0004C49D
		public override int HowManyPartyMemberAll()
		{
			return EquipWeaponManagedBattlePartyController.SupportMemberToPartyMemberAllsNum(this.m_SupportMember);
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x0004E0AA File Offset: 0x0004C4AA
		public override int HowManyPartyMemberEnable()
		{
			return 5;
		}

		// Token: 0x040015A7 RID: 5543
		private CharacterDataWrapperBase m_SupportMember;
	}
}
