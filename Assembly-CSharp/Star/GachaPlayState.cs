﻿using System;

namespace Star
{
	// Token: 0x02000422 RID: 1058
	public class GachaPlayState : GameStateBase
	{
		// Token: 0x0600144A RID: 5194 RVA: 0x0006BF23 File Offset: 0x0006A323
		public GachaPlayState(GachaPlayMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600144B RID: 5195 RVA: 0x0006BF32 File Offset: 0x0006A332
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600144C RID: 5196 RVA: 0x0006BF35 File Offset: 0x0006A335
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600144D RID: 5197 RVA: 0x0006BF37 File Offset: 0x0006A337
		public override void OnStateExit()
		{
		}

		// Token: 0x0600144E RID: 5198 RVA: 0x0006BF39 File Offset: 0x0006A339
		public override void OnDispose()
		{
		}

		// Token: 0x0600144F RID: 5199 RVA: 0x0006BF3B File Offset: 0x0006A33B
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x06001450 RID: 5200 RVA: 0x0006BF3E File Offset: 0x0006A33E
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001B34 RID: 6964
		protected GachaPlayMain m_Owner;
	}
}
