﻿using System;

namespace Star
{
	// Token: 0x02000162 RID: 354
	public enum eADVCategory
	{
		// Token: 0x04000964 RID: 2404
		None,
		// Token: 0x04000965 RID: 2405
		Story,
		// Token: 0x04000966 RID: 2406
		Event,
		// Token: 0x04000967 RID: 2407
		Chara,
		// Token: 0x04000968 RID: 2408
		Cross,
		// Token: 0x04000969 RID: 2409
		Num,
		// Token: 0x0400096A RID: 2410
		Test = 1000
	}
}
