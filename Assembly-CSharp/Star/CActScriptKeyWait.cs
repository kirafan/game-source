﻿using System;

namespace Star
{
	// Token: 0x02000542 RID: 1346
	public class CActScriptKeyWait : IRoomScriptData
	{
		// Token: 0x06001ABD RID: 6845 RVA: 0x0008EC2C File Offset: 0x0008D02C
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyWait actXlsKeyWait = (ActXlsKeyWait)pbase;
			this.m_DummyNo = actXlsKeyWait.m_DummyNo;
			this.m_WaitType = actXlsKeyWait.m_WaitType;
		}

		// Token: 0x0400216D RID: 8557
		public int m_DummyNo;

		// Token: 0x0400216E RID: 8558
		public CActScriptKeyWait.eWait m_WaitType;

		// Token: 0x02000543 RID: 1347
		public enum eWait
		{
			// Token: 0x04002170 RID: 8560
			Non,
			// Token: 0x04002171 RID: 8561
			Move,
			// Token: 0x04002172 RID: 8562
			Popup
		}
	}
}
