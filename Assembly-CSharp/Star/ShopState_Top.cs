﻿using System;
using Star.UI.Global;
using Star.UI.Shop;

namespace Star
{
	// Token: 0x02000485 RID: 1157
	public class ShopState_Top : ShopState
	{
		// Token: 0x060016A4 RID: 5796 RVA: 0x0007637B File Offset: 0x0007477B
		public ShopState_Top(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016A5 RID: 5797 RVA: 0x00076393 File Offset: 0x00074793
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x060016A6 RID: 5798 RVA: 0x00076396 File Offset: 0x00074796
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_Top.eStep.First;
		}

		// Token: 0x060016A7 RID: 5799 RVA: 0x0007639F File Offset: 0x0007479F
		public override void OnStateExit()
		{
		}

		// Token: 0x060016A8 RID: 5800 RVA: 0x000763A1 File Offset: 0x000747A1
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060016A9 RID: 5801 RVA: 0x000763AC File Offset: 0x000747AC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_Top.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_Top.eStep.LoadWait;
				break;
			case ShopState_Top.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_Top.eStep.PlayIn;
				}
				break;
			case ShopState_Top.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_Top.eStep.Main;
				}
				break;
			case ShopState_Top.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case ShopTopUI.eMainButton.Trade:
							this.m_NextState = 2;
							this.m_Owner.IsRequestVocie = true;
							break;
						case ShopTopUI.eMainButton.Weapon:
							this.m_NextState = 10;
							this.m_Owner.IsRequestVocie = true;
							break;
						case ShopTopUI.eMainButton.Build:
							this.m_NextState = 20;
							this.m_Owner.IsRequestVocie = true;
							break;
						default:
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
							this.m_NextState = 2147483646;
							break;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_Top.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_Top.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_Top.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060016AA RID: 5802 RVA: 0x00076550 File Offset: 0x00074950
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.OnClickMainButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Shop);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060016AB RID: 5803 RVA: 0x000765FE File Offset: 0x000749FE
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Shop)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060016AC RID: 5804 RVA: 0x0007663C File Offset: 0x00074A3C
		private void OnClickButton()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060016AD RID: 5805 RVA: 0x00076644 File Offset: 0x00074A44
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001D5F RID: 7519
		private ShopState_Top.eStep m_Step = ShopState_Top.eStep.None;

		// Token: 0x04001D60 RID: 7520
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopTopUI;

		// Token: 0x04001D61 RID: 7521
		private ShopTopUI m_UI;

		// Token: 0x02000486 RID: 1158
		private enum eStep
		{
			// Token: 0x04001D63 RID: 7523
			None = -1,
			// Token: 0x04001D64 RID: 7524
			First,
			// Token: 0x04001D65 RID: 7525
			LoadWait,
			// Token: 0x04001D66 RID: 7526
			PlayIn,
			// Token: 0x04001D67 RID: 7527
			Main,
			// Token: 0x04001D68 RID: 7528
			UnloadChildSceneWait
		}
	}
}
