﻿using System;

namespace Star
{
	// Token: 0x02000377 RID: 887
	public enum eScheduleSegName
	{
		// Token: 0x040017D2 RID: 6098
		Sleep,
		// Token: 0x040017D3 RID: 6099
		Room,
		// Token: 0x040017D4 RID: 6100
		Office,
		// Token: 0x040017D5 RID: 6101
		Buf,
		// Token: 0x040017D6 RID: 6102
		System
	}
}
