﻿using System;

namespace Star
{
	// Token: 0x0200038A RID: 906
	public class UserTownDataCheck
	{
		// Token: 0x06001126 RID: 4390 RVA: 0x0005A080 File Offset: 0x00058480
		public static int CharaManageIDToBuildPoint(long fcharamngid)
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			int result = -1;
			if (fcharamngid != -1L)
			{
				UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(fcharamngid);
				if (roomChara.TownBuildManageID == -1L)
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(TownDefine.ROOM_ACCESS_MNG_ID);
					if (buildObjectDataAtBuildMngID != null)
					{
						result = buildObjectDataAtBuildMngID.m_BuildPointIndex;
					}
				}
				else
				{
					UserTownData.BuildObjectData buildObjectDataAtBuildMngID = userTownData.GetBuildObjectDataAtBuildMngID(roomChara.TownBuildManageID);
					if (buildObjectDataAtBuildMngID != null)
					{
						result = buildObjectDataAtBuildMngID.m_BuildPointIndex;
					}
				}
			}
			return result;
		}
	}
}
