﻿using System;

namespace Star
{
	// Token: 0x0200020B RID: 523
	public enum eSkillType
	{
		// Token: 0x04000CE3 RID: 3299
		NormalAttack_atk,
		// Token: 0x04000CE4 RID: 3300
		NormalAttack_mgc,
		// Token: 0x04000CE5 RID: 3301
		Guard,
		// Token: 0x04000CE6 RID: 3302
		Attack_atk,
		// Token: 0x04000CE7 RID: 3303
		Attack_mgc,
		// Token: 0x04000CE8 RID: 3304
		Recovery,
		// Token: 0x04000CE9 RID: 3305
		AssistBuff,
		// Token: 0x04000CEA RID: 3306
		AssistDeBuff,
		// Token: 0x04000CEB RID: 3307
		Other,
		// Token: 0x04000CEC RID: 3308
		Num
	}
}
