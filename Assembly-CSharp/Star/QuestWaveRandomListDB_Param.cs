﻿using System;

namespace Star
{
	// Token: 0x020001ED RID: 493
	[Serializable]
	public struct QuestWaveRandomListDB_Param
	{
		// Token: 0x04000BED RID: 3053
		public int m_ID;

		// Token: 0x04000BEE RID: 3054
		public int m_Num;

		// Token: 0x04000BEF RID: 3055
		public float[] m_Prob;

		// Token: 0x04000BF0 RID: 3056
		public int[] m_QuestEnemyIDs;

		// Token: 0x04000BF1 RID: 3057
		public int[] m_QuestRandomIDs;

		// Token: 0x04000BF2 RID: 3058
		public int[] m_QuestEnemyLvs;

		// Token: 0x04000BF3 RID: 3059
		public int[] m_DropIDs;
	}
}
