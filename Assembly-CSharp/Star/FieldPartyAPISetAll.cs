﻿using System;
using FieldPartyMemberResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x0200034A RID: 842
	public class FieldPartyAPISetAll : INetComHandle
	{
		// Token: 0x0600101B RID: 4123 RVA: 0x00055C59 File Offset: 0x00054059
		public FieldPartyAPISetAll()
		{
			this.ApiName = "player/field_party/member/set_all";
			this.Request = true;
			this.ResponseType = typeof(SetAll);
		}

		// Token: 0x0400172B RID: 5931
		public PlayerFieldPartyMember[] fieldPartyMembers;
	}
}
