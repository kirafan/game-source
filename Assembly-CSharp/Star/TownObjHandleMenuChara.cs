﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006EB RID: 1771
	public class TownObjHandleMenuChara : ITownObjectHandler
	{
		// Token: 0x06002307 RID: 8967 RVA: 0x000BD217 File Offset: 0x000BB617
		public TownCharaModel GetModel()
		{
			return this.m_CharaIcons;
		}

		// Token: 0x06002308 RID: 8968 RVA: 0x000BD220 File Offset: 0x000BB620
		public void SetUpChara(long fmngid, float fsize, TownObjHandleMenuChara.eMenuType ftype)
		{
			this.m_State = TownObjHandleMenuChara.eState.Init;
			this.m_ManageID = fmngid;
			this.m_MenuType = ftype;
			this.m_RenderObject = this.m_Builder.GetGeneralObj(eTownObjectType.CHARA_MODEL);
			this.m_CharaIcons = this.m_RenderObject.AddComponent<TownCharaModel>();
			this.m_CharaIcons.SetCharaModel(this, this.m_ManageID, 0.75f * fsize, Vector3.zero);
			this.m_RenderObject.transform.SetParent(this.m_Transform, false);
			this.Prepare();
			this.m_CharaIcons.PlayMotion(0);
			this.m_RenderObject.SetActive(true);
		}

		// Token: 0x06002309 RID: 8969 RVA: 0x000BD2B8 File Offset: 0x000BB6B8
		protected override bool PrepareMain()
		{
			bool result = false;
			if (base.PrepareMain())
			{
				result = true;
				this.m_State = TownObjHandleMenuChara.eState.Main;
			}
			return result;
		}

		// Token: 0x0600230A RID: 8970 RVA: 0x000BD2DC File Offset: 0x000BB6DC
		public override void DestroyRequest()
		{
			Vector3 position = this.m_Transform.position;
			this.m_Transform.SetParent(null, true);
			this.m_Transform.position = position;
			this.m_State = TownObjHandleMenuChara.eState.EndStart;
		}

		// Token: 0x0600230B RID: 8971 RVA: 0x000BD315 File Offset: 0x000BB715
		public override void Destroy()
		{
			if (this.m_RenderObject != null)
			{
				this.m_Builder.ReleaseCharaMarker(this.m_RenderObject);
				this.m_RenderObject = null;
			}
			base.Destroy();
		}

		// Token: 0x0600230C RID: 8972 RVA: 0x000BD348 File Offset: 0x000BB748
		private void Update()
		{
			this.m_Action.PakageUpdate();
			switch (this.m_State)
			{
			case TownObjHandleMenuChara.eState.Init:
				if (this.m_IsPreparing)
				{
					this.PrepareMain();
					return;
				}
				break;
			case TownObjHandleMenuChara.eState.EndStart:
				this.m_CharaIcons.PlayMotion(2);
				this.m_State = TownObjHandleMenuChara.eState.End;
				break;
			case TownObjHandleMenuChara.eState.End:
				if (!this.m_CharaIcons.IsPlaying(2))
				{
					this.Destroy();
					UnityEngine.Object.Destroy(base.gameObject);
				}
				break;
			}
		}

		// Token: 0x0600230D RID: 8973 RVA: 0x000BD3E8 File Offset: 0x000BB7E8
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			if (pact.m_Type == eTownEventAct.MovePos)
			{
				TownHandleActionMovePos townHandleActionMovePos = (TownHandleActionMovePos)pact;
				this.MovePosition(townHandleActionMovePos.m_MovePos, townHandleActionMovePos.m_LayerID);
			}
			return result;
		}

		// Token: 0x0600230E RID: 8974 RVA: 0x000BD420 File Offset: 0x000BB820
		private void MovePosition(Vector3 ftarget, int flayerid)
		{
			TownPartsTweenPos townPartsTweenPos = (TownPartsTweenPos)this.m_Action.GetAction(typeof(TownPartsTweenPos));
			if (townPartsTweenPos != null)
			{
				townPartsTweenPos.ChangePos(ftarget, 0.01f);
			}
			else
			{
				this.m_Transform.localPosition = ftarget;
			}
			this.m_CharaIcons.ChangeLayer(flayerid + 10);
		}

		// Token: 0x0600230F RID: 8975 RVA: 0x000BD47A File Offset: 0x000BB87A
		private void BindFade(Transform parent, int flayerid)
		{
			if (parent == null)
			{
				this.m_Transform.SetParent(null, true);
				this.m_CharaIcons.PlayMotion(2);
				this.m_State = TownObjHandleMenuChara.eState.Sleep;
			}
			this.m_CharaIcons.ChangeLayer(flayerid + 10);
		}

		// Token: 0x06002310 RID: 8976 RVA: 0x000BD4B8 File Offset: 0x000BB8B8
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate == ITownObjectHandler.eTouchState.End)
			{
				TownObjHandleMenuChara.eMenuType menuType = this.m_MenuType;
				if (menuType == TownObjHandleMenuChara.eMenuType.Traning)
				{
					pbuilder.StackEventQue(new TownComUI(true)
					{
						m_SelectHandle = this,
						m_OpenUI = eTownUICategory.CharaTraningView
					});
				}
			}
			return true;
		}

		// Token: 0x040029E7 RID: 10727
		private TownCharaModel m_CharaIcons;

		// Token: 0x040029E8 RID: 10728
		private GameObject m_RenderObject;

		// Token: 0x040029E9 RID: 10729
		private TownObjHandleMenuChara.eMenuType m_MenuType;

		// Token: 0x040029EA RID: 10730
		private TownObjHandleMenuChara.eState m_State;

		// Token: 0x020006EC RID: 1772
		public enum eMenuType
		{
			// Token: 0x040029EC RID: 10732
			Non,
			// Token: 0x040029ED RID: 10733
			Traning
		}

		// Token: 0x020006ED RID: 1773
		private enum eState
		{
			// Token: 0x040029EF RID: 10735
			Init,
			// Token: 0x040029F0 RID: 10736
			Main,
			// Token: 0x040029F1 RID: 10737
			Sleep,
			// Token: 0x040029F2 RID: 10738
			EndStart,
			// Token: 0x040029F3 RID: 10739
			End
		}
	}
}
