﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000191 RID: 401
	public static class CharacterParamGrowthListDB_Ext
	{
		// Token: 0x06000AE5 RID: 2789 RVA: 0x000410C4 File Offset: 0x0003F4C4
		public static int CalcHp(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthHp[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AE6 RID: 2790 RVA: 0x0004110C File Offset: 0x0003F50C
		public static int CalcAtk(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthAtk[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AE7 RID: 2791 RVA: 0x00041154 File Offset: 0x0003F554
		public static int CalcMgc(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthMgc[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AE8 RID: 2792 RVA: 0x0004119C File Offset: 0x0003F59C
		public static int CalcDef(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthDef[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AE9 RID: 2793 RVA: 0x000411E4 File Offset: 0x0003F5E4
		public static int CalcMDef(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthMDef[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AEA RID: 2794 RVA: 0x0004122C File Offset: 0x0003F62C
		public static int CalcSpd(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthSpd[(int)tableID]);
			}
			return num;
		}

		// Token: 0x06000AEB RID: 2795 RVA: 0x00041274 File Offset: 0x0003F674
		public static int CalcLuck(this CharacterParamGrowthListDB self, int initValue, int lv, sbyte tableID)
		{
			int num = initValue;
			int num2 = lv - 1;
			if (num2 >= 0 && num2 < self.m_Params.Length)
			{
				num = (int)Mathf.Ceil((float)num * self.m_Params[num2].m_GrowthLuck[(int)tableID]);
			}
			return num;
		}
	}
}
