﻿using System;
using Star.UI;
using Star.UI.GachaSelect;
using Star.UI.Global;

namespace Star
{
	// Token: 0x0200041F RID: 1055
	public class GachaState_Main : GachaState
	{
		// Token: 0x06001430 RID: 5168 RVA: 0x0006B85B File Offset: 0x00069C5B
		public GachaState_Main(GachaMain owner) : base(owner)
		{
		}

		// Token: 0x06001431 RID: 5169 RVA: 0x0006B889 File Offset: 0x00069C89
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001432 RID: 5170 RVA: 0x0006B88C File Offset: 0x00069C8C
		public override void OnStateEnter()
		{
			this.m_Step = GachaState_Main.eStep.First;
		}

		// Token: 0x06001433 RID: 5171 RVA: 0x0006B895 File Offset: 0x00069C95
		public override void OnStateExit()
		{
		}

		// Token: 0x06001434 RID: 5172 RVA: 0x0006B897 File Offset: 0x00069C97
		public override void OnDispose()
		{
			this.m_UI = null;
			this.m_NpcUI = null;
		}

		// Token: 0x06001435 RID: 5173 RVA: 0x0006B8A8 File Offset: 0x00069CA8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case GachaState_Main.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_NpcSceneID, true);
				this.m_Step = GachaState_Main.eStep.LoadWait;
				break;
			case GachaState_Main.eStep.LoadWait:
			{
				SceneLoader inst = SingletonMonoBehaviour<SceneLoader>.Inst;
				if (inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID) && inst.IsCompleteAdditiveChildScene(this.m_NpcSceneID))
				{
					inst.ActivationChildScene(this.m_ChildSceneID);
					inst.ActivationChildScene(this.m_NpcSceneID);
					this.m_Step = GachaState_Main.eStep.PlayIn;
				}
				break;
			}
			case GachaState_Main.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = GachaState_Main.eStep.Main;
				}
				break;
			case GachaState_Main.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_IsSuccessGachaPlay)
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.GachaPlay;
						this.m_NextState = 2147483646;
					}
					else if (this.m_IsOutOfPeriod)
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Gacha;
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						this.m_NextState = 2147483646;
					}
					this.m_UI.Destroy();
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = GachaState_Main.eStep.UnloadChildSceneWait;
				}
				break;
			case GachaState_Main.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = GachaState_Main.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001436 RID: 5174 RVA: 0x0006BA60 File Offset: 0x00069E60
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<GachaSelectUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_NpcUI = UIUtility.GetMenuComponent<NPCCharaDisplayUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_NpcSceneID].m_SceneName);
			if (this.m_NpcUI == null)
			{
				return false;
			}
			this.m_UI.Setup(SingletonMonoBehaviour<GameSystem>.Inst.Gacha);
			this.m_UI.OnClickDecideButton += this.OnClickDecideButtonCallBack;
			this.m_UI.OnClickDecideStepButton += this.OnClickDecideStepButtonCallBack;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			eTutorialSeq tutoarialSeq = inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq != eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay)
			{
				inst.GlobalUI.Open();
				inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Gacha);
				inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			}
			this.m_UI.PlayIn();
			this.m_NpcUI.PlayInVoice(eSoundVoiceControllListDB.Claire_in);
			this.m_NpcUI.PlayInNpc(NpcIllust.eNpc.Summon);
			return true;
		}

		// Token: 0x06001437 RID: 5175 RVA: 0x0006BB90 File Offset: 0x00069F90
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Gacha)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001438 RID: 5176 RVA: 0x0006BBE8 File Offset: 0x00069FE8
		public void OnClickDecideButtonCallBack(int gachaID, GachaDefine.ePlayType playType)
		{
			this.m_IsSuccessGachaPlay = false;
			this.m_WorkGachaID = gachaID;
			this.m_WorkGachaPlayType = playType;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			CommonMessageWindow.eType type = CommonMessageWindow.eType.OK;
			string titleText = null;
			string text = null;
			GachaDefine.eCheckPlay eCheckPlay = GachaUtility.CheckPlay(inst.Gacha.GetGachaData(gachaID), playType, out type, out titleText, out text);
			if (eCheckPlay != GachaDefine.eCheckPlay.GemIsShort)
			{
				if (eCheckPlay != GachaDefine.eCheckPlay.ItemIsShort)
				{
					inst.CmnMsgWindow.Open(type, titleText, text, null, new Action<int>(this.OnConfirmGachaPlay));
				}
				else
				{
					inst.CmnMsgWindow.Open(type, titleText, text, null, null);
				}
			}
			else
			{
				inst.CmnMsgWindow.Open(type, titleText, text, null, new Action<int>(this.OnConfirmPurchaseGem));
			}
		}

		// Token: 0x06001439 RID: 5177 RVA: 0x0006BC9C File Offset: 0x0006A09C
		public void OnClickDecideStepButtonCallBack(int gachaID, GachaDefine.ePlayType playType)
		{
			this.m_IsSuccessGachaPlay = false;
			this.m_WorkGachaID = gachaID;
			this.m_WorkGachaPlayType = playType;
			GameSystem inst = SingletonMonoBehaviour<GameSystem>.Inst;
			CommonMessageWindow.eType type = CommonMessageWindow.eType.OK;
			string titleText = null;
			string text = null;
			GachaDefine.eCheckPlay eCheckPlay = GachaUtility.CheckStepPlay(inst.Gacha.GetStepGachaData(gachaID), out type, out titleText, out text);
			if (eCheckPlay != GachaDefine.eCheckPlay.GemIsShort)
			{
				if (eCheckPlay != GachaDefine.eCheckPlay.ItemIsShort)
				{
					inst.CmnMsgWindow.Open(type, titleText, text, null, new Action<int>(this.OnConfirmStepGachaPlay));
				}
				else
				{
					inst.CmnMsgWindow.Open(type, titleText, text, null, null);
				}
			}
			else
			{
				inst.CmnMsgWindow.Open(type, titleText, text, null, new Action<int>(this.OnConfirmPurchaseGem));
			}
		}

		// Token: 0x0600143A RID: 5178 RVA: 0x0006BD4D File Offset: 0x0006A14D
		private void OnConfirmPurchaseGem(int answer)
		{
			if (answer == 0)
			{
				SingletonMonoBehaviour<GemShopPlayer>.Inst.Open(0, new Action(this.RefreshUI));
			}
		}

		// Token: 0x0600143B RID: 5179 RVA: 0x0006BD6C File Offset: 0x0006A16C
		private void OnConfirmGachaPlay(int answer)
		{
			if (answer == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.Gacha.Request_GachaPlay(this.m_WorkGachaID, this.m_WorkGachaPlayType, new Action<GachaDefine.eReturnGachaPlay, string>(this.OnResponse_GachaPlay));
			}
		}

		// Token: 0x0600143C RID: 5180 RVA: 0x0006BD9C File Offset: 0x0006A19C
		private void OnConfirmStepGachaPlay(int answer)
		{
			if (answer == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.Gacha.Request_StepGachaPlay(this.m_WorkGachaID, new Action<GachaDefine.eReturnGachaPlay, string>(this.OnResponse_GachaPlay));
			}
		}

		// Token: 0x0600143D RID: 5181 RVA: 0x0006BDC6 File Offset: 0x0006A1C6
		private void RefreshUI()
		{
			this.m_UI.Refresh();
		}

		// Token: 0x0600143E RID: 5182 RVA: 0x0006BDD3 File Offset: 0x0006A1D3
		private void GoToMenuEnd()
		{
			this.m_NpcUI.PlayOut();
			this.m_UI.PlayOut();
		}

		// Token: 0x0600143F RID: 5183 RVA: 0x0006BDEC File Offset: 0x0006A1EC
		private void OnResponse_GachaPlay(GachaDefine.eReturnGachaPlay ret, string errorMessage)
		{
			switch (ret)
			{
			case GachaDefine.eReturnGachaPlay.Success:
				this.m_IsSuccessGachaPlay = true;
				this.GoToMenuEnd();
				return;
			case GachaDefine.eReturnGachaPlay.GachaOutOfPeriod:
				this.m_IsOutOfPeriod = true;
				APIUtility.ShowErrorWindowOk(errorMessage, new Action(this.OnConfirmOutOfPeriod));
				return;
			}
			APIUtility.ShowErrorWindowOk(errorMessage, null);
		}

		// Token: 0x06001440 RID: 5184 RVA: 0x0006BE5C File Offset: 0x0006A25C
		private void OnConfirmOutOfPeriod()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x04001B22 RID: 6946
		private GachaState_Main.eStep m_Step = GachaState_Main.eStep.None;

		// Token: 0x04001B23 RID: 6947
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.GachaSelectUI;

		// Token: 0x04001B24 RID: 6948
		private SceneDefine.eChildSceneID m_NpcSceneID = SceneDefine.eChildSceneID.NPCCharaDisplayUI;

		// Token: 0x04001B25 RID: 6949
		private GachaSelectUI m_UI;

		// Token: 0x04001B26 RID: 6950
		private NPCCharaDisplayUI m_NpcUI;

		// Token: 0x04001B27 RID: 6951
		private bool m_IsOutOfPeriod;

		// Token: 0x04001B28 RID: 6952
		private bool m_IsSuccessGachaPlay;

		// Token: 0x04001B29 RID: 6953
		private int m_WorkGachaID = -1;

		// Token: 0x04001B2A RID: 6954
		private GachaDefine.ePlayType m_WorkGachaPlayType = GachaDefine.ePlayType.Gem1;

		// Token: 0x02000420 RID: 1056
		private enum eStep
		{
			// Token: 0x04001B2C RID: 6956
			None = -1,
			// Token: 0x04001B2D RID: 6957
			First,
			// Token: 0x04001B2E RID: 6958
			LoadWait,
			// Token: 0x04001B2F RID: 6959
			PlayIn,
			// Token: 0x04001B30 RID: 6960
			Main,
			// Token: 0x04001B31 RID: 6961
			UnloadChildSceneWait
		}
	}
}
