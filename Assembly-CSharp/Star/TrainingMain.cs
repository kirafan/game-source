﻿using System;
using Star.UI;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x020004C3 RID: 1219
	public class TrainingMain : GameStateMain
	{
		// Token: 0x060017F4 RID: 6132 RVA: 0x0007CDC0 File Offset: 0x0007B1C0
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x060017F5 RID: 6133 RVA: 0x0007CDC9 File Offset: 0x0007B1C9
		private void OnDestroy()
		{
			this.m_NpcUI = null;
		}

		// Token: 0x060017F6 RID: 6134 RVA: 0x0007CDD2 File Offset: 0x0007B1D2
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x060017F7 RID: 6135 RVA: 0x0007CDDC File Offset: 0x0007B1DC
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new TrainingState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new TrainingState_Main(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x060017F8 RID: 6136 RVA: 0x0007CE23 File Offset: 0x0007B223
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
			if (base.NextTransitSceneID == SceneDefine.eSceneID.Title)
			{
				APIUtility.ReturnTitle(false);
			}
		}

		// Token: 0x060017F9 RID: 6137 RVA: 0x0007CE46 File Offset: 0x0007B246
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001EC1 RID: 7873
		public const int STATE_INIT = 0;

		// Token: 0x04001EC2 RID: 7874
		public const int STATE_MAIN = 1;

		// Token: 0x04001EC3 RID: 7875
		public NPCCharaDisplayUI m_NpcUI;
	}
}
