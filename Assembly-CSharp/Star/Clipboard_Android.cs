﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000641 RID: 1601
	public static class Clipboard_Android
	{
		// Token: 0x06001FAA RID: 8106 RVA: 0x000ABD34 File Offset: 0x000AA134
		public static void SetClipboard(string value)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject activity = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			activity.Call("runOnUiThread", new object[]
			{
				new AndroidJavaRunnable(delegate()
				{
					AndroidJavaObject androidJavaObject = activity.Call<AndroidJavaObject>("getSystemService", new object[]
					{
						"clipboard"
					});
					AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("android.content.ClipData");
					AndroidJavaObject androidJavaObject2 = androidJavaClass2.CallStatic<AndroidJavaObject>("newPlainText", new object[]
					{
						"clip text",
						value
					});
					androidJavaObject.Call("setPrimaryClip", new object[]
					{
						androidJavaObject2
					});
				})
			});
		}

		// Token: 0x06001FAB RID: 8107 RVA: 0x000ABD8F File Offset: 0x000AA18F
		public static string GetClipboard()
		{
			return string.Empty;
		}
	}
}
