﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020001AB RID: 427
	public static class SoundBgmListDB_Ext
	{
		// Token: 0x06000B1F RID: 2847 RVA: 0x00042470 File Offset: 0x00040870
		public static SoundBgmListDB_Param GetParam(this SoundBgmListDB self, eSoundBgmListDB cueID, Dictionary<eSoundBgmListDB, int> indices)
		{
			if (indices != null)
			{
				int num = 0;
				if (indices.TryGetValue(cueID, out num))
				{
					return self.m_Params[num];
				}
			}
			return default(SoundBgmListDB_Param);
		}

		// Token: 0x06000B20 RID: 2848 RVA: 0x000424B0 File Offset: 0x000408B0
		public static bool FindCueSheet(this SoundBgmListDB self, string cueName, out SoundBgmListDB_Param out_param)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CueName == cueName)
				{
					out_param = self.m_Params[i];
					return true;
				}
			}
			out_param = default(SoundBgmListDB_Param);
			return false;
		}
	}
}
