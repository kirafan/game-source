﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000693 RID: 1683
	public class TownComAPIObjRemove : INetComHandle
	{
		// Token: 0x060021C4 RID: 8644 RVA: 0x000B3B64 File Offset: 0x000B1F64
		public TownComAPIObjRemove()
		{
			this.ApiName = "player/town_facility/remove";
			this.Request = true;
			this.ResponseType = typeof(Remove);
		}

		// Token: 0x0400283F RID: 10303
		public long managedTownFacilityId;
	}
}
