﻿using System;

namespace Star
{
	// Token: 0x02000095 RID: 149
	[Serializable]
	public class BattleAICommandCondition
	{
		// Token: 0x040002AE RID: 686
		public eBattleAICommandConditionType m_Type;

		// Token: 0x040002AF RID: 687
		public float[] m_Args;
	}
}
