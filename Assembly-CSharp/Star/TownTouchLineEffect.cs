﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006A0 RID: 1696
	public class TownTouchLineEffect : MonoBehaviour
	{
		// Token: 0x06002205 RID: 8709 RVA: 0x000B4F73 File Offset: 0x000B3373
		public void TouchPos(Vector2 fpos, Vector2 ftarget)
		{
			this.m_TouchSt = fpos;
			this.m_TouchEnd = ftarget;
		}

		// Token: 0x06002206 RID: 8710 RVA: 0x000B4F84 File Offset: 0x000B3384
		private void LateUpdate()
		{
			ParticleSystem componentInChildren = base.gameObject.GetComponentInChildren<ParticleSystem>();
			if (componentInChildren != null)
			{
				ParticleSystem.Particle[] array = new ParticleSystem.Particle[componentInChildren.main.maxParticles];
				int particles = componentInChildren.GetParticles(array);
				if (particles > 0)
				{
					Vector2 vector = this.m_TouchEnd - this.m_TouchSt;
					float num = Mathf.Atan2(vector.y, vector.x);
					for (int i = 0; i < particles; i++)
					{
						float d = (float)i / (float)particles;
						array[i].position = (this.m_TouchEnd - this.m_TouchSt) * d + this.m_TouchSt;
						float num2;
						if ((i & 1) == 0)
						{
							num2 = num + 1.5707964f;
						}
						else
						{
							num2 = num - 1.5707964f;
						}
						Quaternion rotation = Quaternion.Euler(0f, 0f, num2 * 57.29578f);
						array[i].velocity = rotation * array[i].velocity;
					}
					componentInChildren.SetParticles(array, particles);
					UnityEngine.Object.Destroy(this);
				}
			}
		}

		// Token: 0x04002877 RID: 10359
		private Vector2 m_TouchSt;

		// Token: 0x04002878 RID: 10360
		private Vector2 m_TouchEnd;
	}
}
