﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002EA RID: 746
	public class EquipWeaponUnmanagedSupportPartiesController : EquipWeaponPartiesController
	{
		// Token: 0x06000E84 RID: 3716 RVA: 0x0004E140 File Offset: 0x0004C540
		public EquipWeaponUnmanagedSupportPartiesController SetupEx(List<EquipWeaponUnmanagedSupportPartiesController.UnmanagedSupportPartyInformation> partyInfos, int supportLimit)
		{
			if (partyInfos == null)
			{
				return null;
			}
			base.Setup();
			this.parties = new EquipWeaponUnmanagedSupportPartyController[partyInfos.Count];
			for (int i = 0; i < this.parties.Length; i++)
			{
				if (partyInfos != null)
				{
					this.parties[i] = new EquipWeaponUnmanagedSupportPartyController().Setup(i, partyInfos[i].supportDatas, partyInfos[i].partyName, supportLimit);
				}
			}
			return this;
		}

		// Token: 0x06000E85 RID: 3717 RVA: 0x0004E1B9 File Offset: 0x0004C5B9
		public override int HowManyParties()
		{
			return this.parties.Length;
		}

		// Token: 0x06000E86 RID: 3718 RVA: 0x0004E1C3 File Offset: 0x0004C5C3
		public override int HowManyPartyMemberAll()
		{
			return 8;
		}

		// Token: 0x06000E87 RID: 3719 RVA: 0x0004E1C6 File Offset: 0x0004C5C6
		public override int HowManyPartyMemberEnable()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.SupportLimit;
		}

		// Token: 0x06000E88 RID: 3720 RVA: 0x0004E1DC File Offset: 0x0004C5DC
		public virtual EquipWeaponUnmanagedSupportPartyController GetPartyEx(int index)
		{
			return this.parties[index];
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x0004E1E6 File Offset: 0x0004C5E6
		public override EquipWeaponPartyController GetParty(int index)
		{
			return this.GetPartyEx(index);
		}

		// Token: 0x040015A8 RID: 5544
		protected EquipWeaponUnmanagedSupportPartyController[] parties;

		// Token: 0x020002EB RID: 747
		public class UnmanagedSupportPartyInformation
		{
			// Token: 0x040015A9 RID: 5545
			public List<UserSupportData> supportDatas;

			// Token: 0x040015AA RID: 5546
			public string partyName;
		}
	}
}
