﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Star
{
	// Token: 0x02000106 RID: 262
	public static class BattleUtility
	{
		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000759 RID: 1881 RVA: 0x0002C99A File Offset: 0x0002AD9A
		private static BattleDefineDB CacheBtlDefDB
		{
			get
			{
				if (BattleUtility.m_CacheBtlDefDB == null)
				{
					BattleUtility.m_CacheBtlDefDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.BattleDefineDB;
				}
				return BattleUtility.m_CacheBtlDefDB;
			}
		}

		// Token: 0x0600075A RID: 1882 RVA: 0x0002C9C5 File Offset: 0x0002ADC5
		public static float DefVal(eBattleDefineDB def)
		{
			return BattleUtility.CacheBtlDefDB.m_Params[(int)def].m_Value;
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x0002C9DC File Offset: 0x0002ADDC
		public static void SetupCharaNameOnBattle(List<CharacterHandler> charas)
		{
			if (charas == null)
			{
				return;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			for (int i = 0; i < charas.Count; i++)
			{
				CharacterHandler characterHandler = charas[i];
				if (characterHandler != null)
				{
					if (characterHandler.IsUserControll)
					{
						characterHandler.CharaBattle.Name = dbMng.CharaListDB.GetParam(characterHandler.CharaID).m_Name;
					}
					else
					{
						characterHandler.CharaBattle.Name = dbMng.QuestEnemyListDB.GetParam(characterHandler.CharaID).m_CharaName;
					}
				}
			}
		}

		// Token: 0x0600075C RID: 1884 RVA: 0x0002CA80 File Offset: 0x0002AE80
		public static string GetBattleBgResourcePath(int bgID)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Battle/BattleBG/BattleBG_");
			stringBuilder.Append(bgID);
			stringBuilder.Append(".muast");
			return stringBuilder.ToString();
		}

		// Token: 0x0600075D RID: 1885 RVA: 0x0002CABC File Offset: 0x0002AEBC
		public static int[] GetBorderFromSkillLvs()
		{
			return new int[]
			{
				(int)BattleUtility.CacheBtlDefDB.m_Params[60].m_Value,
				(int)BattleUtility.CacheBtlDefDB.m_Params[61].m_Value,
				(int)BattleUtility.CacheBtlDefDB.m_Params[62].m_Value
			};
		}

		// Token: 0x0600075E RID: 1886 RVA: 0x0002CB20 File Offset: 0x0002AF20
		public static void CreateOccurEnemy(ref QuestListDB_Param questParam, out BattleOccurEnemy out_occurEnemy)
		{
			QuestWaveListDB questWaveListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveListDB;
			QuestWaveRandomListDB questWaveRandomListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveRandomListDB;
			out_occurEnemy = new BattleOccurEnemy();
			out_occurEnemy.Setup(ref questParam, ref questWaveListDB, ref questWaveRandomListDB);
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x0002CB60 File Offset: 0x0002AF60
		public static void CreateScheduleDropItems(BattleOccurEnemy occurEnemy, float probAdd, out List<BattleDropItems> out_scheduleDropItems)
		{
			out_scheduleDropItems = new List<BattleDropItems>();
			QuestWaveDropItemDB questWaveDropItemDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.QuestWaveDropItemDB;
			List<BattleOccurEnemy.OneWave> waves = occurEnemy.GetWaves();
			if (waves != null)
			{
				for (int i = 0; i < waves.Count; i++)
				{
					BattleOccurEnemy.OneWave oneWave = waves[i];
					BattleDropItems battleDropItems = new BattleDropItems();
					battleDropItems.Items = new BattleDropItem[oneWave.m_Enemies.Count];
					for (int j = 0; j < battleDropItems.Items.Length; j++)
					{
						BattleDropItem battleDropItem = new BattleDropItem();
						int dropID = oneWave.m_Enemies[j].m_DropID;
						if (dropID != -1)
						{
							QuestWaveDropItemDB_Param param = questWaveDropItemDB.GetParam(dropID);
							battleDropItem.Setup(ref param, probAdd);
						}
						battleDropItems.Items[j] = battleDropItem;
					}
					out_scheduleDropItems.Add(battleDropItems);
				}
			}
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x0002CC38 File Offset: 0x0002B038
		public static float CalcDropItemProbAdd(List<int> charaIDs, ref QuestListDB_Param questParam)
		{
			float result = 0f;
			if (!string.IsNullOrEmpty(questParam.m_ItemDropCorrectCharaIDs))
			{
				string[] array = questParam.m_ItemDropCorrectCharaIDs.Split(new char[]
				{
					';'
				});
				for (int i = 0; i < array.Length; i++)
				{
					int num = 0;
					if (int.TryParse(array[i], out num))
					{
						bool flag = false;
						for (int j = 0; j < charaIDs.Count; j++)
						{
							if (charaIDs[j] == num)
							{
								flag = true;
								break;
							}
						}
						if (flag)
						{
							result = (float)questParam.m_ItemDropProbabilityAdd;
							break;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x0002CCE0 File Offset: 0x0002B0E0
		public static string BSDToSaveResponse(BattleSystemData bsd)
		{
			string result;
			try
			{
				if (bsd == null)
				{
					result = string.Empty;
				}
				else
				{
					string text = JsonUtility.ToJson(bsd);
					text = text.Remove(0, 1);
					text = text.Remove(text.Length - 1, 1);
					string text2 = Base64Compress.Base64CompressFromString(text);
					result = text2;
				}
			}
			catch (Exception ex)
			{
				Debug.LogFormat("BSDToSaveResponse except:{0}", new object[]
				{
					ex
				});
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x0002CD60 File Offset: 0x0002B160
		public static BattleSystemData SaveResponseToBSD(string save)
		{
			BattleSystemData result;
			try
			{
				if (string.IsNullOrEmpty(save))
				{
					result = null;
				}
				else
				{
					string text = Base64Compress.StringFromBase64Compress(save);
					text = "{" + text + "}";
					BattleSystemData battleSystemData = JsonUtility.FromJson<BattleSystemData>(text);
					result = battleSystemData;
				}
			}
			catch (Exception ex)
			{
				Debug.LogFormat("SaveResponseToBSD except:{0}", new object[]
				{
					ex
				});
				result = null;
			}
			return result;
		}

		// Token: 0x06000763 RID: 1891 RVA: 0x0002CDD4 File Offset: 0x0002B1D4
		public static int GetQuestIDFromSaveResponse(string save)
		{
			BattleSystemData battleSystemData = BattleUtility.SaveResponseToBSD(save);
			if (battleSystemData != null)
			{
				return battleSystemData.QuestID;
			}
			return -1;
		}

		// Token: 0x040006BC RID: 1724
		private static BattleDefineDB m_CacheBtlDefDB;
	}
}
