﻿using System;

namespace Star
{
	// Token: 0x02000098 RID: 152
	[Serializable]
	public class BattleAICommandTargetSingleCondition
	{
		// Token: 0x040002B7 RID: 695
		public eBattleAICommandTargetSingleConditionType m_Type;

		// Token: 0x040002B8 RID: 696
		public float[] m_Args;
	}
}
