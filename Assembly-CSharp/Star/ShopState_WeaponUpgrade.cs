﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerResponseTypes;
using Star.UI.Global;
using Star.UI.Shop;
using WWWTypes;

namespace Star
{
	// Token: 0x02000495 RID: 1173
	public class ShopState_WeaponUpgrade : ShopState
	{
		// Token: 0x060016FE RID: 5886 RVA: 0x00077F74 File Offset: 0x00076374
		public ShopState_WeaponUpgrade(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x060016FF RID: 5887 RVA: 0x00077F8C File Offset: 0x0007638C
		public override int GetStateID()
		{
			return 13;
		}

		// Token: 0x06001700 RID: 5888 RVA: 0x00077F90 File Offset: 0x00076390
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponUpgrade.eStep.First;
		}

		// Token: 0x06001701 RID: 5889 RVA: 0x00077F99 File Offset: 0x00076399
		public override void OnStateExit()
		{
		}

		// Token: 0x06001702 RID: 5890 RVA: 0x00077F9B File Offset: 0x0007639B
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001703 RID: 5891 RVA: 0x00077FA4 File Offset: 0x000763A4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponUpgrade.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponUpgrade.eStep.LoadWait;
				break;
			case ShopState_WeaponUpgrade.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponUpgrade.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponUpgrade.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_WPN_UPGRADE, null, null);
					this.m_Step = ShopState_WeaponUpgrade.eStep.Main;
				}
				break;
			case ShopState_WeaponUpgrade.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI.Destroy();
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 12;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponUpgrade.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponUpgrade.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_WeaponUpgrade.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001704 RID: 5892 RVA: 0x000780E8 File Offset: 0x000764E8
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<ShopWeaponUpgradeUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.TargetWeaponMngID);
			this.m_UI.OnClickExecuteButton += this.OnClickExecuteButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponUpgradeConfirm);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001705 RID: 5893 RVA: 0x00078192 File Offset: 0x00076592
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x06001706 RID: 5894 RVA: 0x000781A1 File Offset: 0x000765A1
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x06001707 RID: 5895 RVA: 0x000781BD File Offset: 0x000765BD
		private void OnClickExecuteButton(long weaponMngID, List<int> itemIDs, List<int> amounts)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.ShopMng.Request_WeaponUpgrade(weaponMngID, itemIDs, amounts, new Action<MeigewwwParam, Weaponupgrade>(this.OnResponse_WeaponUpgrade));
		}

		// Token: 0x06001708 RID: 5896 RVA: 0x000781E0 File Offset: 0x000765E0
		private void OnResponse_WeaponUpgrade(MeigewwwParam wwwParam, Weaponupgrade param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.GOLD_IS_SHORT && result != ResultCode.ITEM_IS_SHORT)
			{
				if (result == ResultCode.SUCCESS)
				{
					this.m_UI.StartUpgradeEffect(param.successType);
					return;
				}
				if (result != ResultCode.WEAPON_UPGRADE_LIMIT)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
					return;
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
		}

		// Token: 0x04001DBB RID: 7611
		private ShopState_WeaponUpgrade.eStep m_Step = ShopState_WeaponUpgrade.eStep.None;

		// Token: 0x04001DBC RID: 7612
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.ShopWeaponUpgradeUI;

		// Token: 0x04001DBD RID: 7613
		private ShopWeaponUpgradeUI m_UI;

		// Token: 0x02000496 RID: 1174
		private enum eStep
		{
			// Token: 0x04001DBF RID: 7615
			None = -1,
			// Token: 0x04001DC0 RID: 7616
			First,
			// Token: 0x04001DC1 RID: 7617
			LoadStart,
			// Token: 0x04001DC2 RID: 7618
			LoadWait,
			// Token: 0x04001DC3 RID: 7619
			PlayIn,
			// Token: 0x04001DC4 RID: 7620
			Main,
			// Token: 0x04001DC5 RID: 7621
			UnloadChildSceneWait
		}
	}
}
