﻿using System;

namespace Star
{
	// Token: 0x02000509 RID: 1289
	public class MissionPakageChara : IMissionPakage
	{
		// Token: 0x06001949 RID: 6473 RVA: 0x000835DC File Offset: 0x000819DC
		public override string GetTargetMessage()
		{
			NamedListDB namedListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB;
			if (namedListDB.IsExist((eCharaNamedType)this.m_SubCodeID))
			{
				NamedListDB_Param param = namedListDB.GetParam((eCharaNamedType)this.m_SubCodeID);
				return string.Format(this.m_TargetMessage, this.m_RateBase, param.m_NickName);
			}
			return string.Empty;
		}
	}
}
