﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x0200058A RID: 1418
	public class RoomComAPIFriendGetAll : INetComHandle
	{
		// Token: 0x06001BA8 RID: 7080 RVA: 0x000928AC File Offset: 0x00090CAC
		public RoomComAPIFriendGetAll()
		{
			this.ApiName = "player/room/get_all";
			this.Request = false;
			this.floorId = 1;
			this.ResponseType = typeof(GetAll);
		}

		// Token: 0x04002288 RID: 8840
		public long playerId;

		// Token: 0x04002289 RID: 8841
		public int floorId;
	}
}
