﻿using System;

namespace Star
{
	// Token: 0x0200000B RID: 11
	[Serializable]
	public struct ADVCharacterListDB_Data
	{
		// Token: 0x0400004A RID: 74
		public int m_PoseID;

		// Token: 0x0400004B RID: 75
		public string m_PoseName;

		// Token: 0x0400004C RID: 76
		public float m_OffsetX;

		// Token: 0x0400004D RID: 77
		public float m_OffsetY;

		// Token: 0x0400004E RID: 78
		public sbyte m_DummyStandPic;

		// Token: 0x0400004F RID: 79
		public int m_FacePattern;

		// Token: 0x04000050 RID: 80
		public float m_FacePivotX;

		// Token: 0x04000051 RID: 81
		public float m_FacePivotY;

		// Token: 0x04000052 RID: 82
		public float m_FacePicX;

		// Token: 0x04000053 RID: 83
		public float m_FacePicY;

		// Token: 0x04000054 RID: 84
		public int m_FaceReferenceImageType;

		// Token: 0x04000055 RID: 85
		public float m_FaceX;

		// Token: 0x04000056 RID: 86
		public float m_FaceY;

		// Token: 0x04000057 RID: 87
		public float m_EmoOffsetX;

		// Token: 0x04000058 RID: 88
		public float m_EmoOffsetY;

		// Token: 0x04000059 RID: 89
		public float m_FootOffsetX;

		// Token: 0x0400005A RID: 90
		public float m_FootOffsetY;
	}
}
