﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000443 RID: 1091
	public class MenuState_PurchaseHistory : MenuState
	{
		// Token: 0x060014FF RID: 5375 RVA: 0x0006E450 File Offset: 0x0006C850
		public MenuState_PurchaseHistory(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x06001500 RID: 5376 RVA: 0x0006E468 File Offset: 0x0006C868
		public override int GetStateID()
		{
			return 7;
		}

		// Token: 0x06001501 RID: 5377 RVA: 0x0006E46B File Offset: 0x0006C86B
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_PurchaseHistory.eStep.First;
		}

		// Token: 0x06001502 RID: 5378 RVA: 0x0006E474 File Offset: 0x0006C874
		public override void OnStateExit()
		{
		}

		// Token: 0x06001503 RID: 5379 RVA: 0x0006E476 File Offset: 0x0006C876
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x06001504 RID: 5380 RVA: 0x0006E480 File Offset: 0x0006C880
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_PurchaseHistory.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_PurchaseHistory.eStep.LoadWait;
				break;
			case MenuState_PurchaseHistory.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_PurchaseHistory.eStep.RequestWait;
					SingletonMonoBehaviour<GameSystem>.Inst.StoreMng.Request_PurchaseLog(new Action(this.OnResponse));
				}
				break;
			case MenuState_PurchaseHistory.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_PurchaseHistory.eStep.Main;
				}
				break;
			case MenuState_PurchaseHistory.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 1;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_PurchaseHistory.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_PurchaseHistory.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_PurchaseHistory.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001505 RID: 5381 RVA: 0x0006E5C0 File Offset: 0x0006C9C0
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuPurchaseHistoryUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.PurchaseHistory);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001506 RID: 5382 RVA: 0x0006E657 File Offset: 0x0006CA57
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
		}

		// Token: 0x06001507 RID: 5383 RVA: 0x0006E675 File Offset: 0x0006CA75
		private void OnClickButton()
		{
		}

		// Token: 0x06001508 RID: 5384 RVA: 0x0006E677 File Offset: 0x0006CA77
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x06001509 RID: 5385 RVA: 0x0006E684 File Offset: 0x0006CA84
		private void OnResponse()
		{
			this.m_Step = MenuState_PurchaseHistory.eStep.PlayIn;
		}

		// Token: 0x04001BE7 RID: 7143
		private MenuState_PurchaseHistory.eStep m_Step = MenuState_PurchaseHistory.eStep.None;

		// Token: 0x04001BE8 RID: 7144
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuPurchaseHistory;

		// Token: 0x04001BE9 RID: 7145
		private MenuPurchaseHistoryUI m_UI;

		// Token: 0x02000444 RID: 1092
		private enum eStep
		{
			// Token: 0x04001BEB RID: 7147
			None = -1,
			// Token: 0x04001BEC RID: 7148
			First,
			// Token: 0x04001BED RID: 7149
			LoadWait,
			// Token: 0x04001BEE RID: 7150
			RequestWait,
			// Token: 0x04001BEF RID: 7151
			PlayIn,
			// Token: 0x04001BF0 RID: 7152
			Main,
			// Token: 0x04001BF1 RID: 7153
			UnloadChildSceneWait
		}
	}
}
