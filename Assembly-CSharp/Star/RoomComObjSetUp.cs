﻿using System;
using System.Collections.Generic;
using RoomObjectResponseTypes;

namespace Star
{
	// Token: 0x020005A0 RID: 1440
	public class RoomComObjSetUp : IFldNetComModule
	{
		// Token: 0x06001BDA RID: 7130 RVA: 0x00093683 File Offset: 0x00091A83
		public RoomComObjSetUp() : base(IFldNetComManager.Instance)
		{
			this.m_Step = RoomComObjSetUp.eStep.GetState;
		}

		// Token: 0x06001BDB RID: 7131 RVA: 0x00093698 File Offset: 0x00091A98
		public bool SetUp()
		{
			RoomComAPIObjGetAll phandle = new RoomComAPIObjGetAll();
			this.m_NetMng.SendCom(phandle, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06001BDC RID: 7132 RVA: 0x000936C8 File Offset: 0x00091AC8
		public bool DefaultObjAdd()
		{
			if (this.m_SendQue != null && this.m_SendQue.Count != 0)
			{
				RoomComObjSetUp.DefaultSetUpQue defaultSetUpQue = this.m_SendQue[0];
				defaultSetUpQue.SendUp(this.m_NetMng);
				return true;
			}
			return false;
		}

		// Token: 0x06001BDD RID: 7133 RVA: 0x0009370C File Offset: 0x00091B0C
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case RoomComObjSetUp.eStep.GetState:
				this.SetUp();
				this.m_Step = RoomComObjSetUp.eStep.WaitCheck;
				break;
			case RoomComObjSetUp.eStep.DefaultObjUp:
				if (this.DefaultObjAdd())
				{
					this.m_Step = RoomComObjSetUp.eStep.SetUpCheck;
				}
				else
				{
					this.m_Step = RoomComObjSetUp.eStep.End;
				}
				break;
			case RoomComObjSetUp.eStep.SetUpCheck:
				if (this.m_SendQue[0].m_ListUp)
				{
					this.m_SendQue.RemoveAt(0);
					if (this.DefaultObjAdd())
					{
						this.m_Step = RoomComObjSetUp.eStep.SetUpCheck;
					}
					else
					{
						this.m_Step = RoomComObjSetUp.eStep.End;
					}
				}
				break;
			case RoomComObjSetUp.eStep.End:
				result = false;
				break;
			}
			return result;
		}

		// Token: 0x06001BDE RID: 7134 RVA: 0x000937CC File Offset: 0x00091BCC
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				WWWComUtil.SetRoomObjectList(getAll.managedRoomObjects, true, false);
				if (this.CheckRoomObjectListKey(getAll))
				{
					this.m_Step = RoomComObjSetUp.eStep.End;
				}
				else
				{
					this.m_Step = RoomComObjSetUp.eStep.DefaultObjUp;
				}
			}
		}

		// Token: 0x06001BDF RID: 7135 RVA: 0x00093818 File Offset: 0x00091C18
		public static int MakeSetUpToDBAccessKey(ref DefaultObjectListDB_Param pparam)
		{
			int result = -1;
			switch (pparam.m_Type)
			{
			case 2:
				result = RoomObjectListUtil.CategoryIDToAccessKey(eRoomObjectCategory.Background, pparam.m_ObjectNo);
				break;
			case 3:
				result = RoomObjectListUtil.CategoryIDToAccessKey(eRoomObjectCategory.Wall, pparam.m_ObjectNo);
				break;
			case 4:
				result = RoomObjectListUtil.CategoryIDToAccessKey(eRoomObjectCategory.Floor, pparam.m_ObjectNo);
				break;
			case 5:
				result = RoomObjectListUtil.CategoryIDToAccessKey(eRoomObjectCategory.Bedding, pparam.m_ObjectNo);
				break;
			}
			return result;
		}

		// Token: 0x06001BE0 RID: 7136 RVA: 0x00093898 File Offset: 0x00091C98
		private bool CheckRoomObjectListKey(GetAll pres)
		{
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			bool result = true;
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 1 && db.m_Params[i].m_WakeUp == 0)
				{
					int num = RoomComObjSetUp.MakeSetUpToDBAccessKey(ref db.m_Params[i]);
					if (num >= 0)
					{
						bool flag = false;
						for (int j = 0; j < pres.managedRoomObjects.Length; j++)
						{
							if (num == pres.managedRoomObjects[j].roomObjectId)
							{
								flag = true;
								break;
							}
						}
						if (!flag)
						{
							if (this.m_SendQue == null)
							{
								this.m_SendQue = new List<RoomComObjSetUp.DefaultSetUpQue>();
							}
							this.m_SendQue.Add(new RoomComObjSetUp.DefaultSetUpQue(num, db.m_Params[i].m_Num));
							result = false;
						}
					}
				}
			}
			return result;
		}

		// Token: 0x040022D1 RID: 8913
		private RoomComObjSetUp.eStep m_Step;

		// Token: 0x040022D2 RID: 8914
		private List<RoomComObjSetUp.DefaultSetUpQue> m_SendQue;

		// Token: 0x020005A1 RID: 1441
		public enum eStep
		{
			// Token: 0x040022D4 RID: 8916
			GetState,
			// Token: 0x040022D5 RID: 8917
			WaitCheck,
			// Token: 0x040022D6 RID: 8918
			DefaultObjUp,
			// Token: 0x040022D7 RID: 8919
			SetUpCheck,
			// Token: 0x040022D8 RID: 8920
			End
		}

		// Token: 0x020005A2 RID: 1442
		public class DefaultSetUpQue
		{
			// Token: 0x06001BE1 RID: 7137 RVA: 0x0009398B File Offset: 0x00091D8B
			public DefaultSetUpQue(int fobjid, int fnum)
			{
				this.m_ObjResID = fobjid;
				this.m_Num = fnum;
			}

			// Token: 0x06001BE2 RID: 7138 RVA: 0x000939A4 File Offset: 0x00091DA4
			public void SendUp(IFldNetComManager pmanager)
			{
				pmanager.SendCom(new RoomComAPIObjAdd
				{
					roomObjectId = this.m_ObjResID.ToString(),
					amount = "1"
				}, new INetComHandle.ResponseCallbak(this.CallbackDefaultAdd), false);
			}

			// Token: 0x06001BE3 RID: 7139 RVA: 0x000939F0 File Offset: 0x00091DF0
			public void CallbackDefaultAdd(INetComHandle phandle)
			{
				Add add = phandle.GetResponse() as Add;
				if (add != null)
				{
					long fmanageid;
					if (long.TryParse(add.managedRoomObjectIds, out fmanageid))
					{
						RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(this.m_ObjResID);
						UserRoomObjectData userRoomObjectData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
						if (userRoomObjectData == null)
						{
							userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
							userRoomObjectData.SetServerData(fmanageid, this.m_ObjResID);
							SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomObjDatas.Add(userRoomObjectData);
						}
						else
						{
							userRoomObjectData.SetServerData(fmanageid, this.m_ObjResID);
						}
					}
					this.m_ListUp = true;
				}
			}

			// Token: 0x040022D9 RID: 8921
			public int m_ObjResID;

			// Token: 0x040022DA RID: 8922
			public int m_Num;

			// Token: 0x040022DB RID: 8923
			public bool m_ListUp;
		}
	}
}
