﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000346 RID: 838
	public class ResourceFileReader : BinaryReader
	{
		// Token: 0x06001015 RID: 4117 RVA: 0x00055A10 File Offset: 0x00053E10
		public bool OpenFile(string filename)
		{
			TextAsset textAsset = Resources.Load(filename) as TextAsset;
			if (textAsset != null)
			{
				base.SetBinary(textAsset.bytes, textAsset.bytes.Length);
				return true;
			}
			return false;
		}

		// Token: 0x06001016 RID: 4118 RVA: 0x00055A4C File Offset: 0x00053E4C
		public void ReadFile()
		{
		}

		// Token: 0x06001017 RID: 4119 RVA: 0x00055A4E File Offset: 0x00053E4E
		public void CloseFile()
		{
			this.m_Buffer = null;
		}
	}
}
