﻿using System;

namespace Star
{
	// Token: 0x02000273 RID: 627
	public class BattleDebugState_Main : BattleDebugState
	{
		// Token: 0x06000BA3 RID: 2979 RVA: 0x00043CDC File Offset: 0x000420DC
		public BattleDebugState_Main(BattleDebugMain owner) : base(owner)
		{
		}

		// Token: 0x06000BA4 RID: 2980 RVA: 0x00043CEC File Offset: 0x000420EC
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06000BA5 RID: 2981 RVA: 0x00043CEF File Offset: 0x000420EF
		public override void OnStateEnter()
		{
			this.m_Step = BattleDebugState_Main.eStep.First;
		}

		// Token: 0x06000BA6 RID: 2982 RVA: 0x00043CF8 File Offset: 0x000420F8
		public override void OnStateExit()
		{
		}

		// Token: 0x06000BA7 RID: 2983 RVA: 0x00043CFC File Offset: 0x000420FC
		public override int OnStateUpdate()
		{
			BattleDebugState_Main.eStep step = this.m_Step;
			if (step != BattleDebugState_Main.eStep.First)
			{
				if (step == BattleDebugState_Main.eStep.Main)
				{
					if (this.m_Owner.System.IsBattleFinished())
					{
						this.m_Step = BattleDebugState_Main.eStep.None;
					}
				}
			}
			else if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
			{
				this.m_Step = BattleDebugState_Main.eStep.Main;
			}
			return -1;
		}

		// Token: 0x04001410 RID: 5136
		private BattleDebugState_Main.eStep m_Step = BattleDebugState_Main.eStep.None;

		// Token: 0x02000274 RID: 628
		private enum eStep
		{
			// Token: 0x04001412 RID: 5138
			None = -1,
			// Token: 0x04001413 RID: 5139
			First,
			// Token: 0x04001414 RID: 5140
			Main
		}
	}
}
