﻿using System;

namespace Star
{
	// Token: 0x020001BD RID: 445
	public static class WeaponExpDB_Ext
	{
		// Token: 0x06000B3B RID: 2875 RVA: 0x00042D78 File Offset: 0x00041178
		public static int GetNextExp(this WeaponExpDB self, int currentLv, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				int num2 = num + currentLv - 1;
				if (num2 >= 0 && num2 < self.m_Params.Length)
				{
					return self.m_Params[num2].m_NextExp;
				}
			}
			return 0;
		}

		// Token: 0x06000B3C RID: 2876 RVA: 0x00042DF4 File Offset: 0x000411F4
		public static long[] GetNextMaxExps(this WeaponExpDB self, int lv_a, int lv_b, sbyte expTableID)
		{
			int num = lv_b - lv_a + 1;
			if (num <= 0)
			{
				return null;
			}
			long[] array = new long[num];
			for (int i = 0; i < num; i++)
			{
				array[i] = (long)self.GetNextExp(lv_a + i, expTableID);
			}
			return array;
		}

		// Token: 0x06000B3D RID: 2877 RVA: 0x00042E38 File Offset: 0x00041238
		public static int GetUpgradeAmount(this WeaponExpDB self, int currentLv, sbyte expTableID)
		{
			int num = -1;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == (int)expTableID)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				int num2 = num + currentLv - 1;
				if (num2 >= 0 && num2 < self.m_Params.Length)
				{
					return self.m_Params[num2].m_UpgradeAmount;
				}
			}
			return 0;
		}
	}
}
