﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006CF RID: 1743
	public class TownPartsTouchScale : ITownPartsAction
	{
		// Token: 0x06002282 RID: 8834 RVA: 0x000B820C File Offset: 0x000B660C
		public TownPartsTouchScale(Transform pself, Vector3 fbase, Vector3 ftarget, float ftime)
		{
			this.m_Active = true;
			this.m_Marker = pself;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Target = ftarget;
			this.m_Base = fbase;
		}

		// Token: 0x06002283 RID: 8835 RVA: 0x000B8243 File Offset: 0x000B6643
		public void SetStepIn()
		{
			this.m_Step = 0;
		}

		// Token: 0x06002284 RID: 8836 RVA: 0x000B824C File Offset: 0x000B664C
		public void SetStepOut()
		{
			this.m_Step = 2;
		}

		// Token: 0x06002285 RID: 8837 RVA: 0x000B8258 File Offset: 0x000B6658
		public override bool PartsUpdate()
		{
			int step = this.m_Step;
			if (step != 0)
			{
				if (step != 1)
				{
					if (step == 2)
					{
						this.m_Time -= Time.deltaTime;
						if (this.m_Time <= 0f)
						{
							this.m_Marker.localScale = this.m_Base;
							this.m_Step++;
							this.m_Active = false;
						}
						if (this.m_Active)
						{
							float num = this.m_Time / this.m_MaxTime;
							this.m_Marker.localScale = (this.m_Target - this.m_Base) * Mathf.Sin(num * 3.1415927f * 0.5f) + this.m_Base;
						}
					}
				}
			}
			else
			{
				this.m_Time += Time.deltaTime;
				if (this.m_Time >= this.m_MaxTime)
				{
					this.m_Marker.localScale = this.m_Target;
					this.m_Step++;
				}
				else
				{
					float num = this.m_Time / this.m_MaxTime;
					this.m_Marker.localScale = (this.m_Target - this.m_Base) * Mathf.Sin(num * 3.1415927f * 0.5f) + this.m_Base;
				}
			}
			return this.m_Active;
		}

		// Token: 0x04002951 RID: 10577
		private Transform m_Marker;

		// Token: 0x04002952 RID: 10578
		private Vector3 m_Target;

		// Token: 0x04002953 RID: 10579
		private Vector3 m_Base;

		// Token: 0x04002954 RID: 10580
		private float m_Time;

		// Token: 0x04002955 RID: 10581
		private float m_MaxTime;

		// Token: 0x04002956 RID: 10582
		private int m_Step;
	}
}
