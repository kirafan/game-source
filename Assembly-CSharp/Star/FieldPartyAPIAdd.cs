﻿using System;
using FieldPartyMemberResponseTypes;
using WWWTypes;

namespace Star
{
	// Token: 0x02000348 RID: 840
	public class FieldPartyAPIAdd : INetComHandle
	{
		// Token: 0x06001019 RID: 4121 RVA: 0x00055C05 File Offset: 0x00054005
		public FieldPartyAPIAdd()
		{
			this.ApiName = "player/field_party/member/add";
			this.Request = true;
			this.ResponseType = typeof(Add);
		}

		// Token: 0x04001729 RID: 5929
		public PlayerFieldPartyMember fieldPartyMember;
	}
}
