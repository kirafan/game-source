﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006DD RID: 1757
	public class TownObjHandleContent : TownObjModelHandle
	{
		// Token: 0x060022C9 RID: 8905 RVA: 0x000BB67C File Offset: 0x000B9A7C
		public override void SetupHandle(int objID, int buildPointIndex, long fmanageid, int sortingOrder, byte ftimekey, bool finitbuild)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_MakerPos = param.m_MarkerPos;
			this.m_BaseSize = param.m_BaseSize;
			this.m_Offset.x = param.m_OffsetX;
			this.m_Offset.y = param.m_OffsetY;
			this.m_Offset.z = 1f;
			this.m_ResourceID = param.m_ResourceID * 100;
			bool building = true;
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(fmanageid, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(building);
			}
			base.SetupHandle(objID, buildPointIndex, fmanageid, sortingOrder, ftimekey, finitbuild);
			FieldMapManager.EntryBuildState(fmanageid, new FeedBackResultCallback(this.CallbackContentsBuild));
			this.m_BindCtrl = this.m_Builder.GetCharaBind(this.m_ManageID, true);
			this.m_BindCtrl.SetLinkBindKey(this.m_Transform, this.m_LayerID, this.m_MakerPos);
			this.m_SyncKey = -1;
		}

		// Token: 0x060022CA RID: 8906 RVA: 0x000BB77C File Offset: 0x000B9B7C
		protected override bool PrepareMain()
		{
			if (!base.PrepareMain())
			{
				return false;
			}
			this.m_Obj.transform.localPosition = this.m_Offset;
			base.MakeColliderLink(this.m_Obj);
			this.m_Obj.transform.localScale = Vector3.one * this.m_BaseSize;
			if (!this.m_InitUp)
			{
			}
			return true;
		}

		// Token: 0x060022CB RID: 8907 RVA: 0x000BB7E4 File Offset: 0x000B9BE4
		private void Update()
		{
			switch (this.m_State)
			{
			case TownObjHandleContent.eState.Init:
				if (this.m_IsPreparing && this.PrepareMain())
				{
					this.m_State = TownObjHandleContent.eState.Main;
					if (this.m_SyncKey >= 0)
					{
						this.m_Builder.SetActiveSyncKey(this.m_SyncKey);
					}
					else
					{
						this.m_Builder.SetActiveSyncKey(TownUtility.SyncFixKey(this.m_ObjID));
					}
				}
				break;
			case TownObjHandleContent.eState.Main:
				if (this.m_ReBindPos)
				{
					this.m_BindCtrl.CalcCharaPopIconPos(this.m_ManageID, this.m_LayerID, this.m_MakerPos);
					this.m_ReBindPos = false;
				}
				break;
			case TownObjHandleContent.eState.EndStart:
				this.m_State = TownObjHandleContent.eState.End;
				break;
			case TownObjHandleContent.eState.End:
				base.Destroy();
				break;
			}
			this.m_Action.PakageUpdate();
		}

		// Token: 0x060022CC RID: 8908 RVA: 0x000BB8CE File Offset: 0x000B9CCE
		public override void DestroyRequest()
		{
			this.m_State = TownObjHandleContent.eState.EndStart;
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, TownUtility.SyncFixKey(this.m_ObjID));
		}

		// Token: 0x060022CD RID: 8909 RVA: 0x000BB90C File Offset: 0x000B9D0C
		public override void Destroy()
		{
			FieldMapManager.EntryBuildState(this.m_ManageID, null);
			TownBuildLinkPoint buildMngIDTreeNode = this.m_Builder.GetBuildTree().GetBuildMngIDTreeNode(this.m_ManageID, -1);
			if (buildMngIDTreeNode != null)
			{
				buildMngIDTreeNode.SetBuilding(false);
				buildMngIDTreeNode.ChangeManageID(-1L);
			}
			this.m_State = TownObjHandleContent.eState.EndStart;
			base.Destroy();
		}

		// Token: 0x060022CE RID: 8910 RVA: 0x000BB960 File Offset: 0x000B9D60
		private void ReSetUpHandle(int objID, long fmanageid)
		{
			base.SetupHandle(objID, this.m_BuildAccessID, fmanageid, this.m_LayerID, this.m_TimeKey, false);
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID);
			this.m_MakerPos = param.m_MarkerPos;
			this.m_BaseSize = param.m_BaseSize;
			this.m_Offset.x = param.m_OffsetX;
			this.m_Offset.y = param.m_OffsetY;
			this.m_Offset.z = 1f;
			this.m_ResourceID = param.m_ResourceID * 100;
			this.m_TownObjResource.SetupResource(this);
			this.m_TownObjResource.Prepare();
			this.m_IsPreparing = true;
			this.m_IsAvailable = false;
			this.m_State = TownObjHandleContent.eState.Init;
			TownBuildLinkPoint buildPointTreeNode = this.m_Builder.GetBuildTree().GetBuildPointTreeNode(this.m_BuildAccessID);
			if (buildPointTreeNode != null)
			{
				buildPointTreeNode.ChangeManageID(this.m_ManageID);
			}
			this.m_BindCtrl.SetLinkBindKey(this.m_Transform, this.m_LayerID, this.m_MakerPos);
			this.m_BindCtrl.ChangeManageID(fmanageid);
		}

		// Token: 0x060022CF RID: 8911 RVA: 0x000BBA7C File Offset: 0x000B9E7C
		public override bool RecvEvent(ITownHandleAction pact)
		{
			bool result = false;
			switch (pact.m_Type)
			{
			case eTownEventAct.ReFlesh:
				this.m_ReBindPos = true;
				break;
			case eTownEventAct.ChangeObject:
			{
				TownHandleActionChangeObj townHandleActionChangeObj = (TownHandleActionChangeObj)pact;
				TownPartsChangeModel paction = new TownPartsChangeModel(this.m_Obj, this, townHandleActionChangeObj.m_ObjectID, townHandleActionChangeObj.m_ManageID);
				this.m_Action.EntryAction(paction, 0);
				break;
			}
			case eTownEventAct.ChangeModel:
			{
				TownHandleActionModelObj townHandleActionModelObj = (TownHandleActionModelObj)pact;
				this.m_SyncKey = this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				FieldMapManager.EntryBuildState(townHandleActionModelObj.m_ManageID, new FeedBackResultCallback(this.CallbackContentsBuild));
				TownBuildLinkPoint buildPointTreeNode = this.m_Builder.GetBuildTree().GetBuildPointTreeNode(this.m_BuildAccessID);
				if (buildPointTreeNode != null)
				{
					buildPointTreeNode.ChangeManageID(townHandleActionModelObj.m_ManageID);
				}
				base.DesModel();
				this.ReSetUpHandle(townHandleActionModelObj.m_ObjectID, townHandleActionModelObj.m_ManageID);
				TownEffectPlayer townEffectPlayer = TownEffectManager.CreateEffect(5);
				townEffectPlayer.SetTrs(base.transform.position, Quaternion.identity, Vector3.one, this.m_LayerID + 30);
				break;
			}
			case eTownEventAct.StepIn:
				this.m_Obj.transform.localScale = Vector3.one * this.m_BaseSize;
				break;
			case eTownEventAct.SelectOff:
			{
				TownPartsModelBlink townPartsModelBlink = (TownPartsModelBlink)this.m_Action.GetAction(typeof(TownPartsModelBlink));
				if (townPartsModelBlink != null)
				{
					townPartsModelBlink.SetEndMark();
				}
				break;
			}
			case eTownEventAct.SelectTouch:
			{
				TownHandleActionTouchSelect townHandleActionTouchSelect = pact as TownHandleActionTouchSelect;
				TownPartsModelBlink townPartsModelBlink2 = (TownPartsModelBlink)this.m_Action.GetAction(typeof(TownPartsModelBlink));
				if (townHandleActionTouchSelect.m_Select)
				{
					if (townPartsModelBlink2 == null)
					{
						townPartsModelBlink2 = new TownPartsModelBlink(this.m_Obj, 0.3f, 0.7f, 1.5f);
						this.m_Action.EntryAction(townPartsModelBlink2, 0);
					}
				}
				else if (townPartsModelBlink2 != null)
				{
					townPartsModelBlink2.SetEndMark();
				}
				break;
			}
			case eTownEventAct.MenuMode:
			{
				TownHandleActionMenuMode townHandleActionMenuMode = (TownHandleActionMenuMode)pact;
				this.m_BindCtrl.SetViewActive(townHandleActionMenuMode.m_Mode);
				break;
			}
			case eTownEventAct.PlayEffect:
			{
				TownHandleActionEffectPlay townHandleActionEffectPlay = (TownHandleActionEffectPlay)pact;
				TownEffectPlayer townEffectPlayer2 = TownEffectManager.CreateEffect(townHandleActionEffectPlay.m_EffectNo);
				townEffectPlayer2.SetTrs(this.m_Obj.transform.position + townHandleActionEffectPlay.m_OffsetPos, Quaternion.identity, townHandleActionEffectPlay.m_Size, this.m_LayerID + 30);
				break;
			}
			}
			return result;
		}

		// Token: 0x060022D0 RID: 8912 RVA: 0x000BBD18 File Offset: 0x000BA118
		public void CallbackContentsBuild(eCallBackType ftype, FieldObjHandleBuild pbase, bool fup)
		{
			if (ftype != eCallBackType.MoveBuild)
			{
				if (ftype != eCallBackType.SwapBuild)
				{
					if (ftype != eCallBackType.CompBuild && ftype != eCallBackType.LevelUp)
					{
					}
				}
				else
				{
					int fsynckey = this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
					TownBuildMoveBufParam prequest = new TownBuildMoveBufParam(this.m_BuildAccessID, pbase.m_BuildPoint, this.m_ManageID, this.m_ObjID, fsynckey, null);
					this.m_Builder.BuildRequest(prequest);
					this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				}
			}
			else
			{
				int fsynckey2 = this.m_BindCtrl.BackLinkCut(this.m_Builder, this.m_ManageID, this.m_LayerID, -1);
				TownBuildMoveBufParam prequest2 = new TownBuildMoveBufParam(this.m_BuildAccessID, pbase.m_BuildPoint, this.m_ManageID, this.m_ObjID, fsynckey2, null);
				this.m_Builder.BuildRequest(prequest2);
			}
		}

		// Token: 0x060022D1 RID: 8913 RVA: 0x000BBE09 File Offset: 0x000BA209
		public void CallbackItemUpResult(IFldNetComModule phandle)
		{
		}

		// Token: 0x060022D2 RID: 8914 RVA: 0x000BBE0C File Offset: 0x000BA20C
		public override bool CalcTouchEvent(ITownObjectHandler.eTouchState fstate, TownBuilder pbuilder)
		{
			if (fstate != ITownObjectHandler.eTouchState.Begin)
			{
				if (fstate != ITownObjectHandler.eTouchState.Cancel)
				{
					if (fstate == ITownObjectHandler.eTouchState.End)
					{
						bool flag = true;
						FieldObjHandleBuild buildState = FieldMapManager.GetBuildState(this.m_ManageID);
						if (buildState != null && this.m_ItemUp)
						{
							buildState.SendDropItemCom(new IFldNetComModule.CallBack(this.CallbackItemUpResult));
							this.m_ItemUp = false;
							flag = false;
						}
						if (flag)
						{
							pbuilder.StackEventQue(new TownComUI(true)
							{
								m_SelectHandle = this,
								m_BuildPoint = base.GetBuildAccessID(),
								m_OpenUI = eTownUICategory.Content,
								m_BuildPoint = this.m_BuildAccessID
							});
							SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.SYS_DECISION_1, 1f, 0, -1, -1);
						}
						TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
						if (townPartsTouchScale != null)
						{
							townPartsTouchScale.SetStepOut();
						}
					}
				}
				else
				{
					TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
					if (townPartsTouchScale != null)
					{
						townPartsTouchScale.SetStepOut();
					}
				}
			}
			else
			{
				TownPartsTouchScale townPartsTouchScale = (TownPartsTouchScale)this.m_Action.GetAction(typeof(TownPartsTouchScale));
				if (townPartsTouchScale == null)
				{
					townPartsTouchScale = new TownPartsTouchScale(this.m_Obj.transform, Vector3.one * this.m_BaseSize, Vector3.one * this.m_BaseSize * 1.1f, 0.1f);
					this.m_Action.EntryAction(townPartsTouchScale, 0);
				}
				townPartsTouchScale.SetStepIn();
			}
			return true;
		}

		// Token: 0x040029A7 RID: 10663
		private float m_MakerPos;

		// Token: 0x040029A8 RID: 10664
		private float m_BaseSize;

		// Token: 0x040029A9 RID: 10665
		private Vector3 m_Offset;

		// Token: 0x040029AA RID: 10666
		private bool m_ItemUp;

		// Token: 0x040029AB RID: 10667
		private BindTownCharaMap m_BindCtrl;

		// Token: 0x040029AC RID: 10668
		private bool m_ReBindPos;

		// Token: 0x040029AD RID: 10669
		private int m_SyncKey;

		// Token: 0x040029AE RID: 10670
		public TownObjHandleContent.eState m_State;

		// Token: 0x020006DE RID: 1758
		public enum eState
		{
			// Token: 0x040029B0 RID: 10672
			Init,
			// Token: 0x040029B1 RID: 10673
			Main,
			// Token: 0x040029B2 RID: 10674
			Sleep,
			// Token: 0x040029B3 RID: 10675
			EndStart,
			// Token: 0x040029B4 RID: 10676
			End
		}
	}
}
