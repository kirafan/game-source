﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002E4 RID: 740
	public class EquipWeaponUnmanagedSupportPartyController : EquipWeaponPartyController
	{
		// Token: 0x06000E66 RID: 3686 RVA: 0x0004DE47 File Offset: 0x0004C247
		public EquipWeaponUnmanagedSupportPartyController() : base(eCharacterDataControllerType.UnmanagedSupportParty)
		{
		}

		// Token: 0x06000E67 RID: 3687 RVA: 0x0004DE50 File Offset: 0x0004C250
		public EquipWeaponUnmanagedSupportPartyController Setup(int partyIndex, List<CharacterDataWrapperBase> supportDatas, string partyName, int PartyPanelCharaAll, int PartyPanelCharaEnables)
		{
			this.SetupBase(partyIndex);
			return this.SetupProcess(partyIndex, supportDatas, partyName, PartyPanelCharaAll, PartyPanelCharaEnables);
		}

		// Token: 0x06000E68 RID: 3688 RVA: 0x0004DE67 File Offset: 0x0004C267
		public EquipWeaponUnmanagedSupportPartyController Setup(int partyIndex, List<UserSupportData> supportDatas, string partyName, int supportLimit)
		{
			this.SetupBase(partyIndex);
			return this.SetupProcess(partyIndex, supportDatas, partyName, supportLimit);
		}

		// Token: 0x06000E69 RID: 3689 RVA: 0x0004DE7C File Offset: 0x0004C27C
		protected EquipWeaponUnmanagedSupportPartyController SetupProcess(int partyIndex, List<UserSupportData> supportDatas, string partyName, int supportLimit)
		{
			List<CharacterDataWrapperBase> list = new List<CharacterDataWrapperBase>();
			for (int i = 0; i < supportDatas.Count; i++)
			{
				UserSupportData userSupportData = supportDatas[i];
				CharacterDataWrapperBase item;
				if (userSupportData != null)
				{
					item = new UserSupportDataWrapper(userSupportData);
				}
				else
				{
					item = new CharacterDataEmpty();
				}
				list.Add(item);
			}
			this.SetupProcess(partyIndex, list, partyName, 8, supportLimit);
			return this;
		}

		// Token: 0x06000E6A RID: 3690 RVA: 0x0004DEE0 File Offset: 0x0004C2E0
		protected EquipWeaponUnmanagedSupportPartyController SetupProcess(int partyIndex, List<CharacterDataWrapperBase> supportDatas, string partyName, int PartyPanelCharaAll, int PartyPanelCharaEnables)
		{
			this.partyMembers = new EquipWeaponPartyMemberController[PartyPanelCharaAll];
			this.enablePartyMembers = new EquipWeaponUnmanagedSupportPartyMemberController[PartyPanelCharaEnables];
			this.m_MngID = -1L;
			this.m_Name = partyName;
			for (int i = 0; i < PartyPanelCharaEnables; i++)
			{
				CharacterDataWrapperBase characterDataWrapperBase = null;
				if (i < supportDatas.Count)
				{
					characterDataWrapperBase = supportDatas[i];
				}
				if (characterDataWrapperBase == null)
				{
					characterDataWrapperBase = new CharacterDataEmpty();
				}
				this.enablePartyMembers[i] = new EquipWeaponUnmanagedSupportPartyMemberController().Setup(characterDataWrapperBase, this.m_PartyIndex, i);
				this.partyMembers[i] = this.enablePartyMembers[i];
			}
			for (int j = PartyPanelCharaEnables; j < PartyPanelCharaAll; j++)
			{
				this.partyMembers[j] = new EquipWeaponUnmanagedSupportPartyMemberController().Setup(new CharacterDataLocked(), this.m_PartyIndex, j);
			}
			return this;
		}

		// Token: 0x06000E6B RID: 3691 RVA: 0x0004DFA8 File Offset: 0x0004C3A8
		public override int HowManyPartyMemberEnable()
		{
			return this.enablePartyMembers.Length;
		}

		// Token: 0x06000E6C RID: 3692 RVA: 0x0004DFB2 File Offset: 0x0004C3B2
		public EquipWeaponUnmanagedSupportPartyMemberController GetMemberEx(int index)
		{
			return this.enablePartyMembers[index];
		}

		// Token: 0x040015A4 RID: 5540
		public const int PARTYPANEL_CHARA_ALL = 8;

		// Token: 0x040015A5 RID: 5541
		protected EquipWeaponUnmanagedSupportPartyMemberController[] enablePartyMembers;
	}
}
