﻿using System;

namespace Star
{
	// Token: 0x0200010E RID: 270
	public enum eDmgEffectType
	{
		// Token: 0x040006EA RID: 1770
		None = -1,
		// Token: 0x040006EB RID: 1771
		PL_Single,
		// Token: 0x040006EC RID: 1772
		PL_All,
		// Token: 0x040006ED RID: 1773
		EN_Slash,
		// Token: 0x040006EE RID: 1774
		EN_Blow,
		// Token: 0x040006EF RID: 1775
		EN_Bite,
		// Token: 0x040006F0 RID: 1776
		EN_Claw
	}
}
