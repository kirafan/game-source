﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000734 RID: 1844
	public class TownRenderSync : MonoBehaviour
	{
		// Token: 0x0600244D RID: 9293 RVA: 0x000C2B29 File Offset: 0x000C0F29
		public void SetLinkRender(Camera pcamera, RenderTexture prender, RenderTexture pdepth)
		{
			this.m_Camera = pcamera;
			this.m_Camera.SetTargetBuffers(prender.colorBuffer, pdepth.depthBuffer);
		}

		// Token: 0x0600244E RID: 9294 RVA: 0x000C2B49 File Offset: 0x000C0F49
		private void OnPostRender()
		{
			if (!this.m_Sync)
			{
				this.m_Camera.targetTexture = null;
				Graphics.SetRenderTarget(null);
				this.m_Sync = true;
				this.m_Camera = null;
			}
		}

		// Token: 0x04002B49 RID: 11081
		private Camera m_Camera;

		// Token: 0x04002B4A RID: 11082
		public bool m_Sync;
	}
}
