﻿using System;
using System.Collections.Generic;
using RoomObjectResponseTypes;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x020005A9 RID: 1449
	public class RoomComUnlockDefault : IFldNetComModule
	{
		// Token: 0x06001BF7 RID: 7159 RVA: 0x000942BA File Offset: 0x000926BA
		public RoomComUnlockDefault(int fupkey) : base(IFldNetComManager.Instance)
		{
			this.m_SetUp = (eDefCheckUpType)fupkey;
		}

		// Token: 0x06001BF8 RID: 7160 RVA: 0x000942CE File Offset: 0x000926CE
		public void PlaySend()
		{
			this.m_NetMng.AddModule(this);
		}

		// Token: 0x06001BF9 RID: 7161 RVA: 0x000942DC File Offset: 0x000926DC
		public override bool UpFunc()
		{
			bool result = true;
			switch (this.m_Step)
			{
			case RoomComUnlockDefault.eStep.GetDatabase:
				FldDefaultObjectUtil.DatabaseSetUp();
				this.m_Step++;
				break;
			case RoomComUnlockDefault.eStep.WaitResourceUp:
				if (FldDefaultObjectUtil.IsSetUp())
				{
					this.m_Step++;
				}
				break;
			case RoomComUnlockDefault.eStep.SetUpSettingObject:
				this.m_ComUp = false;
				this.MakeRoomDefaultObjectListUp();
				this.m_Step++;
				break;
			case RoomComUnlockDefault.eStep.SettingUpCheck:
				if (this.m_ComUp)
				{
					this.m_Step++;
				}
				break;
			case RoomComUnlockDefault.eStep.SetUpRoomFloor:
				this.m_ComUp = false;
				this.MakeSubRoomSetUp();
				this.m_Step++;
				break;
			case RoomComUnlockDefault.eStep.RoomFloorUpCheck:
				if (this.m_ComUp)
				{
					this.m_Step++;
				}
				break;
			}
			return result;
		}

		// Token: 0x06001BFA RID: 7162 RVA: 0x000943D0 File Offset: 0x000927D0
		private void CheckListUpKey(int ftableid, int fobjid)
		{
			int count = this.m_SetObjNo.Count;
			bool flag = false;
			for (int i = 0; i < count; i++)
			{
				if (this.m_SetObjNo[i].m_ObjNo == fobjid)
				{
					this.m_SetObjNo[i].m_Num++;
					this.m_SetObjNo[i].m_TableID.Add(ftableid);
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				RoomComUnlockDefault.SetUpKey setUpKey = new RoomComUnlockDefault.SetUpKey();
				setUpKey.m_ObjNo = fobjid;
				setUpKey.m_Num = 1;
				setUpKey.m_TableID = new List<int>();
				setUpKey.m_TableID.Add(ftableid);
				this.m_SetObjNo.Add(setUpKey);
			}
		}

		// Token: 0x06001BFB RID: 7163 RVA: 0x00094488 File Offset: 0x00092888
		private int GetListUpKeyToTableList(int findex)
		{
			int count = this.m_SetObjNo.Count;
			for (int i = 0; i < count; i++)
			{
				if (findex < this.m_SetObjNo[i].m_Num)
				{
					return this.m_SetObjNo[i].m_ObjNo;
				}
				findex -= this.m_SetObjNo[i].m_Num;
			}
			return 0;
		}

		// Token: 0x06001BFC RID: 7164 RVA: 0x000944F4 File Offset: 0x000928F4
		private void MakeRoomDefaultObjectListUp()
		{
			this.m_SetObjNo = new List<RoomComUnlockDefault.SetUpKey>();
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 1 && db.m_Params[i].m_Type != 1 && db.m_Params[i].m_WakeUp == (int)this.m_SetUp)
				{
					int fobjid = RoomComObjSetUp.MakeSetUpToDBAccessKey(ref db.m_Params[i]);
					this.CheckListUpKey(i, fobjid);
				}
			}
			RoomComAPIObjAdd roomComAPIObjAdd = new RoomComAPIObjAdd();
			string text = this.m_SetObjNo[0].m_ObjNo.ToString();
			string text2 = this.m_SetObjNo[0].m_Num.ToString();
			for (int i = 1; i < this.m_SetObjNo.Count; i++)
			{
				text = text + "," + this.m_SetObjNo[i].m_ObjNo.ToString();
				text2 = text2 + "," + this.m_SetObjNo[i].m_Num.ToString();
			}
			roomComAPIObjAdd.roomObjectId = text;
			roomComAPIObjAdd.amount = text2;
			this.m_NetMng.SendCom(roomComAPIObjAdd, new INetComHandle.ResponseCallbak(this.CallbackDefaultAdd), false);
		}

		// Token: 0x06001BFD RID: 7165 RVA: 0x0009466C File Offset: 0x00092A6C
		public void CallbackDefaultAdd(INetComHandle phandle)
		{
			Add add = phandle.GetResponse() as Add;
			if (add != null)
			{
				string[] array = add.managedRoomObjectIds.Split(new char[]
				{
					','
				});
				this.m_SettingObj = new long[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					if (long.TryParse(array[i], out this.m_SettingObj[i]))
					{
						int listUpKeyToTableList = this.GetListUpKeyToTableList(i);
						RoomObjectListDB_Param accessKeyToObjectParam = RoomObjectListUtil.GetAccessKeyToObjectParam(listUpKeyToTableList);
						UserRoomObjectData userRoomObjectData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserRoomObjData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
						if (userRoomObjectData == null)
						{
							userRoomObjectData = new UserRoomObjectData((eRoomObjectCategory)accessKeyToObjectParam.m_Category, accessKeyToObjectParam.m_ID);
							userRoomObjectData.SetServerData(this.m_SettingObj[i], listUpKeyToTableList);
							SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomObjDatas.Add(userRoomObjectData);
						}
						else
						{
							userRoomObjectData.SetServerData(this.m_SettingObj[i], listUpKeyToTableList);
						}
					}
				}
				this.m_ComUp = true;
			}
		}

		// Token: 0x06001BFE RID: 7166 RVA: 0x00094768 File Offset: 0x00092B68
		private void MakeSubRoomSetUp()
		{
			DefaultObjectListDB db = FldDefaultObjectUtil.GetDB();
			for (int i = 0; i < db.m_Params.Length; i++)
			{
				if (db.m_Params[i].m_Category == 1 && db.m_Params[i].m_Type == 1 && db.m_Params[i].m_WakeUp == (int)this.m_SetUp)
				{
					UserRoomData userRoomData = RoomComSetUp.SetUpDefaultRoomKey(db, this.m_SetUp);
					userRoomData.FloorID = 2;
					SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserRoomListData.CheckEntryFloor(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID, 0, userRoomData);
				}
			}
			UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(-1L);
			RoomComAPINewSet roomComAPINewSet = new RoomComAPINewSet();
			roomComAPINewSet.floorId = manageIDToUserRoomData.FloorID;
			roomComAPINewSet.arrangeData = UserRoomUtil.CreateRoomDataString(manageIDToUserRoomData);
			this.m_NetMng.SendCom(roomComAPINewSet, new INetComHandle.ResponseCallbak(this.CallbackDefaultRoomPlace), false);
		}

		// Token: 0x06001BFF RID: 7167 RVA: 0x00094860 File Offset: 0x00092C60
		private void CallbackDefaultRoomPlace(INetComHandle phandle)
		{
			Set set = phandle.GetResponse() as Set;
			if (set != null)
			{
				UserRoomData manageIDToUserRoomData = UserRoomUtil.GetManageIDToUserRoomData(-1L);
				manageIDToUserRoomData.MngID = set.managedRoomId;
				this.m_Step = RoomComUnlockDefault.eStep.End;
			}
		}

		// Token: 0x040022F5 RID: 8949
		private RoomComUnlockDefault.eStep m_Step;

		// Token: 0x040022F6 RID: 8950
		private List<RoomComUnlockDefault.SetUpKey> m_SetObjNo;

		// Token: 0x040022F7 RID: 8951
		private long[] m_SettingObj;

		// Token: 0x040022F8 RID: 8952
		private bool m_ComUp;

		// Token: 0x040022F9 RID: 8953
		private eDefCheckUpType m_SetUp;

		// Token: 0x020005AA RID: 1450
		public enum eStep
		{
			// Token: 0x040022FB RID: 8955
			GetDatabase,
			// Token: 0x040022FC RID: 8956
			WaitResourceUp,
			// Token: 0x040022FD RID: 8957
			SetUpSettingObject,
			// Token: 0x040022FE RID: 8958
			SettingUpCheck,
			// Token: 0x040022FF RID: 8959
			SetUpRoomFloor,
			// Token: 0x04002300 RID: 8960
			RoomFloorUpCheck,
			// Token: 0x04002301 RID: 8961
			End
		}

		// Token: 0x020005AB RID: 1451
		public class SetUpKey
		{
			// Token: 0x04002302 RID: 8962
			public int m_ObjNo;

			// Token: 0x04002303 RID: 8963
			public int m_Num;

			// Token: 0x04002304 RID: 8964
			public List<int> m_TableID;
		}
	}
}
