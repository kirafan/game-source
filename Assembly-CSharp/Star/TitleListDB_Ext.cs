﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Star
{
	// Token: 0x020001B4 RID: 436
	public static class TitleListDB_Ext
	{
		// Token: 0x06000B29 RID: 2857 RVA: 0x00042724 File Offset: 0x00040B24
		public static bool IsExist(this TitleListDB self, eTitleType titleType)
		{
			if (titleType == eTitleType.None)
			{
				return false;
			}
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_TitleType == (int)titleType)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000B2A RID: 2858 RVA: 0x00042770 File Offset: 0x00040B70
		public static TitleListDB_Param GetParam(this TitleListDB self, eTitleType titleType)
		{
			if (titleType == eTitleType.None)
			{
				return default(TitleListDB_Param);
			}
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_TitleType == (int)titleType)
				{
					return self.m_Params[i];
				}
			}
			return default(TitleListDB_Param);
		}

		// Token: 0x06000B2B RID: 2859 RVA: 0x000427D9 File Offset: 0x00040BD9
		public static int GetTitleNum(this TitleListDB self)
		{
			return self.m_Params.Length;
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x000427E4 File Offset: 0x00040BE4
		public static eTitleType[] CalcSortedTitleID(this TitleListDB self)
		{
			List<TitleListDB_Ext.SortData> list = new List<TitleListDB_Ext.SortData>();
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				list.Add(new TitleListDB_Ext.SortData
				{
					m_TitleType = (eTitleType)self.m_Params[i].m_TitleType,
					m_Order = self.m_Params[i].m_Order
				});
			}
			List<TitleListDB_Ext.SortData> list2 = list;
			if (TitleListDB_Ext.<>f__mg$cache0 == null)
			{
				TitleListDB_Ext.<>f__mg$cache0 = new Comparison<TitleListDB_Ext.SortData>(TitleListDB_Ext.CompareTitle);
			}
			list2.Sort(TitleListDB_Ext.<>f__mg$cache0);
			eTitleType[] array = new eTitleType[list.Count];
			for (int j = 0; j < array.Length; j++)
			{
				array[j] = list[j].m_TitleType;
			}
			return array;
		}

		// Token: 0x06000B2D RID: 2861 RVA: 0x000428A4 File Offset: 0x00040CA4
		private static int CompareTitle(TitleListDB_Ext.SortData A, TitleListDB_Ext.SortData B)
		{
			if (A.m_Order > B.m_Order)
			{
				return 1;
			}
			if (A.m_Order < B.m_Order)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x04000AC9 RID: 2761
		[CompilerGenerated]
		private static Comparison<TitleListDB_Ext.SortData> <>f__mg$cache0;

		// Token: 0x020001B5 RID: 437
		public class SortData
		{
			// Token: 0x04000ACA RID: 2762
			public eTitleType m_TitleType;

			// Token: 0x04000ACB RID: 2763
			public int m_Order;
		}
	}
}
