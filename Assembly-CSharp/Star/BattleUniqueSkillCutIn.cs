﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000FD RID: 253
	public class BattleUniqueSkillCutIn : MonoBehaviour
	{
		// Token: 0x0600072E RID: 1838 RVA: 0x0002B370 File Offset: 0x00029770
		private void OnDestroy()
		{
			this.m_MeigeAnimCtrl = null;
			this.m_ModelPrefab = null;
			this.m_MeigeAnimClipHolder = null;
		}

		// Token: 0x0600072F RID: 1839 RVA: 0x0002B388 File Offset: 0x00029788
		public void Play()
		{
			if (this.m_BodyObj == null)
			{
				this.m_BodyObj = UnityEngine.Object.Instantiate<GameObject>(this.m_ModelPrefab, Vector3.zero, Quaternion.Euler(0f, 180f, 0f), base.transform);
				string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(this.m_ModelPrefab.name, false);
				this.m_BaseTransform = this.m_BodyObj.transform.Find(name);
				Animation component = this.m_BaseTransform.gameObject.GetComponent<Animation>();
				component.cullingType = AnimationCullingType.AlwaysAnimate;
				this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
				this.m_MeigeAnimCtrl.Open();
				this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, this.m_MeigeAnimClipHolder);
				this.m_MeigeAnimCtrl.Close();
				int num = LayerMask.NameToLayer("Graphic2D");
				this.m_BodyObj.SetLayer(num, true);
				int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
				for (int i = 0; i < particleEmitterNum; i++)
				{
					this.m_MsbHndl.GetParticleEmitter(i).SetLayer(num);
				}
			}
			this.m_SaveCameraDepth = this.m_Camera.depth;
			this.m_Camera.depth = SingletonMonoBehaviour<GameSystem>.Inst.UICamera.depth - 1f;
			this.m_Camera.enabled = true;
			this.AdjustScale();
			this.m_BodyObj.SetActive(true);
			this.m_MsbHndl.SetDefaultVisibility();
			this.m_MeigeAnimCtrl.Play("Take 001", 0f, WrapMode.Loop);
		}

		// Token: 0x06000730 RID: 1840 RVA: 0x0002B518 File Offset: 0x00029918
		public void Stop()
		{
			this.m_Camera.depth = this.m_SaveCameraDepth;
			this.m_Camera.enabled = false;
			this.m_BodyObj.SetActive(false);
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x06000731 RID: 1841 RVA: 0x0002B550 File Offset: 0x00029950
		private void AdjustScale()
		{
			float num = (float)this.m_BaseHeight / (float)this.m_BaseWidth;
			float num2 = (float)Screen.height / (float)Screen.width;
			float num3 = num2 / num;
			this.m_BaseTransform.localScale = new Vector3(1f * num3, 1f * num3, 1f);
		}

		// Token: 0x0400066A RID: 1642
		private const string LAYER_MASK = "Graphic2D";

		// Token: 0x0400066B RID: 1643
		private const string ANIM_KEY = "Take 001";

		// Token: 0x0400066C RID: 1644
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x0400066D RID: 1645
		[SerializeField]
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x0400066E RID: 1646
		[SerializeField]
		private GameObject m_ModelPrefab;

		// Token: 0x0400066F RID: 1647
		[SerializeField]
		private MeigeAnimClipHolder m_MeigeAnimClipHolder;

		// Token: 0x04000670 RID: 1648
		[SerializeField]
		private int m_BaseWidth = 1334;

		// Token: 0x04000671 RID: 1649
		[SerializeField]
		private int m_BaseHeight = 750;

		// Token: 0x04000672 RID: 1650
		private GameObject m_BodyObj;

		// Token: 0x04000673 RID: 1651
		private Transform m_BaseTransform;

		// Token: 0x04000674 RID: 1652
		private MsbHandler m_MsbHndl;

		// Token: 0x04000675 RID: 1653
		private float m_SaveCameraDepth;

		// Token: 0x020000FE RID: 254
		private enum eStep
		{
			// Token: 0x04000677 RID: 1655
			None = -1,
			// Token: 0x04000678 RID: 1656
			Overlay
		}
	}
}
