﻿using System;

namespace Star
{
	// Token: 0x02000500 RID: 1280
	public enum eMissionState
	{
		// Token: 0x04001FE6 RID: 8166
		Non,
		// Token: 0x04001FE7 RID: 8167
		PreComp,
		// Token: 0x04001FE8 RID: 8168
		PreView,
		// Token: 0x04001FE9 RID: 8169
		Comp
	}
}
