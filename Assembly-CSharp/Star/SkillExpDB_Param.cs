﻿using System;

namespace Star
{
	// Token: 0x02000207 RID: 519
	[Serializable]
	public struct SkillExpDB_Param
	{
		// Token: 0x04000CD2 RID: 3282
		public int m_ID;

		// Token: 0x04000CD3 RID: 3283
		public int m_Lv;

		// Token: 0x04000CD4 RID: 3284
		public uint m_NextExp;
	}
}
