﻿using System;
using System.Collections.Generic;
using System.Text;
using Meige;
using Star.UI;
using UnityEngine;

namespace Star
{
	// Token: 0x02000003 RID: 3
	public class SpriteManager
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002154 File Offset: 0x00000554
		public SpriteManager()
		{
			this.m_HandlersArray = new Dictionary<string, SpriteHandler>[33];
			this.m_PathsArray = new List<string>[33];
			for (int i = 0; i < 33; i++)
			{
				this.m_HandlersArray[i] = new Dictionary<string, SpriteHandler>();
				this.m_PathsArray[i] = new List<string>();
			}
			this.m_RemoveKeys = new List<string>();
		}

		// Token: 0x06000006 RID: 6 RVA: 0x0000257C File Offset: 0x0000097C
		public bool IsAvailable()
		{
			return true;
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002580 File Offset: 0x00000980
		public void Update()
		{
			for (int i = 0; i < 33; i++)
			{
				Dictionary<string, SpriteHandler> dictionary = this.m_HandlersArray[i];
				List<string> list = this.m_PathsArray[i];
				this.m_RemoveKeys.Clear();
				foreach (string text in dictionary.Keys)
				{
					SpriteHandler spriteHandler = dictionary[text];
					if (!spriteHandler.IsAvailable())
					{
						spriteHandler.ApplyObj();
					}
					else if (!this.LIMIT[i].m_IsCache)
					{
						if (spriteHandler.RefCount <= 0)
						{
							spriteHandler.DestroyObj();
							this.m_RemoveKeys.Add(text);
							this.m_IsRequesetUnloadUnusedAssets = true;
						}
					}
				}
				for (int j = 0; j < this.m_RemoveKeys.Count; j++)
				{
					dictionary.Remove(this.m_RemoveKeys[j]);
					list.Remove(this.m_RemoveKeys[j]);
				}
				while (list.Count > this.LIMIT[i].m_LimitNum)
				{
					int index = 0;
					SpriteHandler spriteHandler2 = dictionary[list[index]];
					for (int k = 0; k < list.Count; k++)
					{
						if (dictionary[list[k]].RefCount <= 0)
						{
							spriteHandler2 = dictionary[list[k]];
							index = k;
							break;
						}
					}
					if (spriteHandler2 == null || !spriteHandler2.IsAvailable())
					{
						break;
					}
					Debug.Log("refcount" + spriteHandler2.RefCount);
					spriteHandler2.DestroyObj();
					dictionary.Remove(spriteHandler2.Path);
					list.RemoveAt(index);
					this.m_IsRequesetUnloadUnusedAssets = true;
				}
			}
			if (this.m_UnloadUnusedAssetsInterval > 0f)
			{
				this.m_UnloadUnusedAssetsInterval -= Time.deltaTime;
			}
			if (this.m_IsRequesetUnloadUnusedAssets && this.m_UnloadUnusedAssetsInterval <= 0f)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
				}
				this.m_UnloadUnusedAssetsInterval = 10f;
				this.m_IsRequesetUnloadUnusedAssets = false;
			}
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000027F4 File Offset: 0x00000BF4
		public SpriteHandler LoadAsyncCharaIcon(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/charaicon/charaicon_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaIcon, this.m_sb.ToString());
		}

		// Token: 0x06000009 RID: 9 RVA: 0x00002850 File Offset: 0x00000C50
		public SpriteHandler LoadAsyncBust(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/bust/bust_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.Bust, this.m_sb.ToString());
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000028AC File Offset: 0x00000CAC
		public SpriteHandler LoadAsyncBustFull(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/bustfull/bustfull_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.BustFull, this.m_sb.ToString());
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002908 File Offset: 0x00000D08
		public SpriteHandler LoadAsyncCharaCard(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/characard/characard_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaCard, this.m_sb.ToString());
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002964 File Offset: 0x00000D64
		public SpriteHandler LoadAsyncCharaIllustBust(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/charaillustbust/charaillust_bust_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaIllustBust, this.m_sb.ToString());
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000029C0 File Offset: 0x00000DC0
		public SpriteHandler LoadAsyncCharaIllustEdit(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/charaillustedit/charaillust_edit_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaIllustEdit, this.m_sb.ToString());
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002A1C File Offset: 0x00000E1C
		public SpriteHandler LoadAsyncCharaIllustChara(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/charaillustchara/charaillust_chara_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaIllustChara, this.m_sb.ToString());
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002A78 File Offset: 0x00000E78
		public SpriteHandler LoadAsyncCharaIllustFull(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/charaillustfull/charaillust_full_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.CharaIllustFull, this.m_sb.ToString());
		}

		// Token: 0x06000010 RID: 16 RVA: 0x00002AD4 File Offset: 0x00000ED4
		public SpriteHandler LoadAsyncPLOrderIcon(int charaID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/ordericon/pl_ordericon_");
			this.m_sb.Append(charaID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.OrderIcon, this.m_sb.ToString());
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002B30 File Offset: 0x00000F30
		public SpriteHandler LoadAsyncENOrderIcon(int resourceID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/charauiresource/ordericon/en_ordericon_");
			this.m_sb.Append(resourceID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.OrderIcon, this.m_sb.ToString());
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002B8C File Offset: 0x00000F8C
		public SpriteHandler LoadAsyncBackGround(string filename)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/background/");
			this.m_sb.Append(filename);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.BackGround, this.m_sb.ToString());
		}

		// Token: 0x06000013 RID: 19 RVA: 0x00002BE8 File Offset: 0x00000FE8
		public SpriteHandler LoadAsyncItemIcon(int itemID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/itemicon/itemicon_");
			this.m_sb.Append(itemID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ItemIcon, this.m_sb.ToString());
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002C44 File Offset: 0x00001044
		public SpriteHandler LoadAsyncWeaponIcon(int weaponID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/weaponicon/weaponicon_wpn_");
			this.m_sb.Append(weaponID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.WeaponIcon, this.m_sb.ToString());
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002CA0 File Offset: 0x000010A0
		public SpriteHandler LoadAsyncTownObjectIcon(int townObjectId, int lv = 1)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/townobjecticon/townobjecticon_bld_");
			this.m_sb.Append(TownUtility.GetResourceNoName(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(townObjectId).m_ResourceID, lv, 0).ToLower());
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.TownObjectIcon, this.m_sb.ToString());
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002D24 File Offset: 0x00001124
		public SpriteHandler LoadAsyncRoomObjectIcon(eRoomObjectCategory category, int roomObjectID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/roomobjecticon/");
			this.m_sb.Append("roomobjecticon_");
			this.m_sb.Append(category.ToString().ToLower());
			this.m_sb.Append("_");
			this.m_sb.Append(roomObjectID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.TownObjectIcon, this.m_sb.ToString());
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002DC0 File Offset: 0x000011C0
		public SpriteHandler LoadAsyncOrbIcon(int orbID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/orbicon/orbicon_");
			this.m_sb.Append(orbID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.OrbIcon, this.m_sb.ToString());
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002E1C File Offset: 0x0000121C
		public SpriteHandler LoadAsyncContentTitleLogo(eTitleType titleType)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/contenttitlelogo/contentslogo");
			this.m_sb.Append((int)titleType);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ContentTitleLogo, this.m_sb.ToString());
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002E78 File Offset: 0x00001278
		public SpriteHandler LoadAsyncNpcIllust(NpcIllust.eNpc npc)
		{
			string value = null;
			switch (npc)
			{
			case NpcIllust.eNpc.Summon:
				value = "summon";
				break;
			case NpcIllust.eNpc.Trade:
				value = "trade";
				break;
			case NpcIllust.eNpc.Weapon:
				value = "weapon";
				break;
			case NpcIllust.eNpc.Build:
				value = "build";
				break;
			case NpcIllust.eNpc.Training:
				value = "training";
				break;
			case NpcIllust.eNpc.Lamp:
				value = "lamp";
				break;
			}
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/npc/npc_");
			this.m_sb.Append(value);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.NpcIllust, this.m_sb.ToString());
		}

		// Token: 0x0600001A RID: 26 RVA: 0x00002F3C File Offset: 0x0000133C
		public SpriteHandler LoadAsyncMasterIllust()
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/master/master_0");
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.MasterIllust, this.m_sb.ToString());
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002F8C File Offset: 0x0000138C
		public SpriteHandler LoadAsyncMasterOrb(int orbID)
		{
			if (orbID == -1)
			{
				orbID = 1;
			}
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/master/masterorb_");
			this.m_sb.Append(orbID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.MasterOrb, this.m_sb.ToString());
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002FF4 File Offset: 0x000013F4
		public SpriteHandler LoadAsyncQuestEventTypeIcon(int eventType)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/questeventtypeicon/questeventtypeicon_");
			this.m_sb.Append(eventType);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.QuestEventTypeIcon, this.m_sb.ToString());
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00003050 File Offset: 0x00001450
		public SpriteHandler LoadAsyncQuestGroupIcon(int groupID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/questgroupicon/questgroupicon_");
			this.m_sb.Append(groupID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.QuestGroupIcon, this.m_sb.ToString());
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000030AC File Offset: 0x000014AC
		public SpriteHandler LoadAsyncQuestChapterIcon(int chapterIdx)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/questchaptericon/questchaptericon_");
			this.m_sb.Append(chapterIdx);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.QuestChapterIcon, this.m_sb.ToString());
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00003108 File Offset: 0x00001508
		public SpriteHandler LoadAsyncLibraryOriginalCharaIcon(int id)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/originalcharactericon/originalcharactericon_");
			this.m_sb.Append(id);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.LibraryOriginalCharaIcon, this.m_sb.ToString());
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00003164 File Offset: 0x00001564
		public SpriteHandler LoadAsyncLibraryOriginalCharaIllust(int id)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/originalcharacterillust/originalcharacterillust_");
			this.m_sb.Append(id);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.LibraryOriginalCharaIllust, this.m_sb.ToString());
		}

		// Token: 0x06000021 RID: 33 RVA: 0x000031C0 File Offset: 0x000015C0
		public SpriteHandler LoadAsyncADVLibraryIcon(int id)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/advlibraryicon/advlibraryicon_");
			this.m_sb.Append(id);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ADVLibraryIcon, this.m_sb.ToString());
		}

		// Token: 0x06000022 RID: 34 RVA: 0x0000321C File Offset: 0x0000161C
		public SpriteHandler LoadAsyncGachaItemLabelIcon(string filename)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/gachaitemlabel/");
			this.m_sb.Append(filename);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.GachaItemLabel, this.m_sb.ToString());
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00003278 File Offset: 0x00001678
		public SpriteHandler LoadAsyncADVStandPic(string resourceBaseName, int poseId)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("adv/standpic/");
			this.m_sb.Append(resourceBaseName.ToLower());
			this.m_sb.Append("/");
			this.m_sb.Append(resourceBaseName.ToLower());
			this.m_sb.Append("_standpic_");
			this.m_sb.Append(poseId);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ADVStandPic, this.m_sb.ToString());
		}

		// Token: 0x06000024 RID: 36 RVA: 0x0000331C File Offset: 0x0000171C
		public SpriteHandler LoadAsyncADVFace(string resourceBaseName, int facePatternID, string faceName)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("adv/standpic/");
			this.m_sb.Append(resourceBaseName.ToLower());
			this.m_sb.Append("/");
			this.m_sb.Append(resourceBaseName.ToLower());
			this.m_sb.Append("_face_");
			this.m_sb.Append(facePatternID);
			this.m_sb.Append("_");
			this.m_sb.Append(faceName);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ADVFace, this.m_sb.ToString());
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000033DC File Offset: 0x000017DC
		public SpriteHandler LoadAsyncADVBackGround(string filename)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("adv/background/");
			this.m_sb.Append(filename.ToLower());
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ADVBackGround, this.m_sb.ToString());
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000343C File Offset: 0x0000183C
		public SpriteHandler LoadAsyncADVSprite(string filename)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("adv/advsprite/");
			this.m_sb.Append(filename.ToLower());
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.ADVSprite, this.m_sb.ToString());
		}

		// Token: 0x06000027 RID: 39 RVA: 0x0000349C File Offset: 0x0000189C
		public SpriteHandler LoadAsyncTutorialTips(eTutorialTipsListDB id, int imageID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/tutorialtips/tutorialtips_");
			this.m_sb.Append((int)id);
			this.m_sb.Append("_");
			this.m_sb.Append(imageID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.TutorialTips, this.m_sb.ToString());
		}

		// Token: 0x06000028 RID: 40 RVA: 0x00003518 File Offset: 0x00001918
		public SpriteHandler LoadAsyncRetireTips(eRetireTipsListDB id, int imageID)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/retiretips/retiretips_");
			this.m_sb.Append((int)id);
			this.m_sb.Append("_");
			this.m_sb.Append(imageID);
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.RetireTips, this.m_sb.ToString());
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00003594 File Offset: 0x00001994
		public SpriteHandler LoadAsyncSceneTitle(string resourceName)
		{
			this.m_sb.Length = 0;
			this.m_sb.Append("texture/scenetitle/");
			this.m_sb.Append(resourceName.ToLower());
			this.m_sb.Append(".muast");
			return this.LoadAsync(SpriteManager.eSpriteType.SceneTitle, this.m_sb.ToString());
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000035F4 File Offset: 0x000019F4
		private SpriteHandler LoadAsync(SpriteManager.eSpriteType spriteType, string path)
		{
			SpriteHandler spriteHandler;
			if (!this.m_HandlersArray[(int)spriteType].ContainsKey(path))
			{
				MeigeResource.Handler hndl = MeigeResourceManager.LoadHandler(path, new MeigeResource.Option[0]);
				spriteHandler = new SpriteHandler(hndl, path);
				spriteHandler.DummyWidth = this.LIMIT[(int)spriteType].m_Width;
				spriteHandler.DummyHeight = this.LIMIT[(int)spriteType].m_Height;
				this.m_HandlersArray[(int)spriteType].Add(path, spriteHandler);
				this.m_PathsArray[(int)spriteType].Add(path);
			}
			else
			{
				spriteHandler = this.m_HandlersArray[(int)spriteType][path];
				spriteHandler.AddRef();
				if (spriteHandler.IsDummySprite)
				{
					spriteHandler.DestroyObj();
					MeigeResource.Handler hndl2 = MeigeResourceManager.LoadHandler(path, new MeigeResource.Option[0]);
					spriteHandler.Hndl = hndl2;
				}
				this.m_PathsArray[(int)spriteType].Remove(path);
				this.m_PathsArray[(int)spriteType].Add(path);
			}
			return spriteHandler;
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000036D6 File Offset: 0x00001AD6
		public void Unload(SpriteHandler hndl)
		{
			if (hndl == null)
			{
				return;
			}
			hndl.RemoveRef();
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000036E8 File Offset: 0x00001AE8
		public void ForceResetOnReturnTitle()
		{
			for (int i = 0; i < 33; i++)
			{
				Dictionary<string, SpriteHandler> dictionary = this.m_HandlersArray[i];
				foreach (SpriteHandler spriteHandler in dictionary.Values)
				{
					spriteHandler.ResetRef();
				}
			}
		}

		// Token: 0x04000002 RID: 2
		private const int SPRITE_TYPE_NUM = 33;

		// Token: 0x04000003 RID: 3
		private readonly SpriteManager.LimitData[] LIMIT = new SpriteManager.LimitData[]
		{
			new SpriteManager.LimitData(96, true, 128, 128),
			new SpriteManager.LimitData(32, true, 360, 512),
			new SpriteManager.LimitData(4, false, 360, 512),
			new SpriteManager.LimitData(1, false, 750, 1000),
			new SpriteManager.LimitData(2, false, 750, 1000),
			new SpriteManager.LimitData(18, false, 640, 750),
			new SpriteManager.LimitData(18, false, 750, 1000),
			new SpriteManager.LimitData(2, false, 750, 1000),
			new SpriteManager.LimitData(16, false, 128, 128),
			new SpriteManager.LimitData(2, false, 2048, 2048),
			new SpriteManager.LimitData(32, true, 128, 128),
			new SpriteManager.LimitData(64, true, 256, 256),
			new SpriteManager.LimitData(16, true, 256, 256),
			new SpriteManager.LimitData(16, true, 256, 256),
			new SpriteManager.LimitData(16, true, 256, 256),
			new SpriteManager.LimitData(16, false, 512, 154),
			new SpriteManager.LimitData(1, false, 0, 0),
			new SpriteManager.LimitData(1, false, 0, 0),
			new SpriteManager.LimitData(1, false, 0, 0),
			new SpriteManager.LimitData(8, false, 588, 132),
			new SpriteManager.LimitData(8, false, 596, 138),
			new SpriteManager.LimitData(8, false, 400, 520),
			new SpriteManager.LimitData(1, false, 150, 150),
			new SpriteManager.LimitData(1, false, 320, 700),
			new SpriteManager.LimitData(16, false, 591, 134),
			new SpriteManager.LimitData(4, false, 0, 0),
			new SpriteManager.LimitData(12, false, 0, 0),
			new SpriteManager.LimitData(128, false, 0, 0),
			new SpriteManager.LimitData(4, false, 2048, 2048),
			new SpriteManager.LimitData(4, false, 0, 0),
			new SpriteManager.LimitData(6, false, 512, 320),
			new SpriteManager.LimitData(1, false, 512, 320),
			new SpriteManager.LimitData(1, false, 220, 42)
		};

		// Token: 0x04000004 RID: 4
		private Dictionary<string, SpriteHandler>[] m_HandlersArray;

		// Token: 0x04000005 RID: 5
		private List<string>[] m_PathsArray;

		// Token: 0x04000006 RID: 6
		private List<string> m_RemoveKeys;

		// Token: 0x04000007 RID: 7
		private const float UNLOADUNUSEDASSETS_INTERVAL = 10f;

		// Token: 0x04000008 RID: 8
		private float m_UnloadUnusedAssetsInterval;

		// Token: 0x04000009 RID: 9
		private bool m_IsRequesetUnloadUnusedAssets;

		// Token: 0x0400000A RID: 10
		private StringBuilder m_sb = new StringBuilder();

		// Token: 0x02000004 RID: 4
		public enum eSpriteType
		{
			// Token: 0x0400000C RID: 12
			None = -1,
			// Token: 0x0400000D RID: 13
			CharaIcon,
			// Token: 0x0400000E RID: 14
			Bust,
			// Token: 0x0400000F RID: 15
			BustFull,
			// Token: 0x04000010 RID: 16
			CharaCard,
			// Token: 0x04000011 RID: 17
			CharaIllustBust,
			// Token: 0x04000012 RID: 18
			CharaIllustEdit,
			// Token: 0x04000013 RID: 19
			CharaIllustChara,
			// Token: 0x04000014 RID: 20
			CharaIllustFull,
			// Token: 0x04000015 RID: 21
			OrderIcon,
			// Token: 0x04000016 RID: 22
			BackGround,
			// Token: 0x04000017 RID: 23
			ItemIcon,
			// Token: 0x04000018 RID: 24
			WeaponIcon,
			// Token: 0x04000019 RID: 25
			TownObjectIcon,
			// Token: 0x0400001A RID: 26
			RoomObjectIcon,
			// Token: 0x0400001B RID: 27
			OrbIcon,
			// Token: 0x0400001C RID: 28
			ContentTitleLogo,
			// Token: 0x0400001D RID: 29
			NpcIllust,
			// Token: 0x0400001E RID: 30
			MasterIllust,
			// Token: 0x0400001F RID: 31
			MasterOrb,
			// Token: 0x04000020 RID: 32
			QuestEventTypeIcon,
			// Token: 0x04000021 RID: 33
			QuestGroupIcon,
			// Token: 0x04000022 RID: 34
			QuestChapterIcon,
			// Token: 0x04000023 RID: 35
			LibraryOriginalCharaIcon,
			// Token: 0x04000024 RID: 36
			LibraryOriginalCharaIllust,
			// Token: 0x04000025 RID: 37
			ADVLibraryIcon,
			// Token: 0x04000026 RID: 38
			GachaItemLabel,
			// Token: 0x04000027 RID: 39
			ADVStandPic,
			// Token: 0x04000028 RID: 40
			ADVFace,
			// Token: 0x04000029 RID: 41
			ADVBackGround,
			// Token: 0x0400002A RID: 42
			ADVSprite,
			// Token: 0x0400002B RID: 43
			TutorialTips,
			// Token: 0x0400002C RID: 44
			RetireTips,
			// Token: 0x0400002D RID: 45
			SceneTitle,
			// Token: 0x0400002E RID: 46
			Num
		}

		// Token: 0x02000005 RID: 5
		private struct LimitData
		{
			// Token: 0x0600002D RID: 45 RVA: 0x00003760 File Offset: 0x00001B60
			public LimitData(int limitNum, bool isCache, int width, int height)
			{
				this.m_LimitNum = limitNum;
				this.m_IsCache = isCache;
				this.m_Width = width;
				this.m_Height = height;
			}

			// Token: 0x0400002F RID: 47
			public int m_LimitNum;

			// Token: 0x04000030 RID: 48
			public bool m_IsCache;

			// Token: 0x04000031 RID: 49
			public int m_Width;

			// Token: 0x04000032 RID: 50
			public int m_Height;
		}
	}
}
