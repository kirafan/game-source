﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200009D RID: 157
	public class BattleCamera : MonoBehaviour
	{
		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000469 RID: 1129 RVA: 0x000162F0 File Offset: 0x000146F0
		public bool IsMoving
		{
			get
			{
				return this.m_Mode != BattleCamera.eMode.Default;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600046A RID: 1130 RVA: 0x00016300 File Offset: 0x00014700
		public bool IsZoomed
		{
			get
			{
				return this.m_IsZoomed;
			}
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00016308 File Offset: 0x00014708
		private void Awake()
		{
			this.m_Camera = base.GetComponent<Camera>();
			this.m_DefaultNear = this.m_Camera.nearClipPlane;
			this.m_DefaultFar = this.m_Camera.farClipPlane;
			this.m_DefaultPos = this.m_Transform.position;
			this.ApplyDefaultOrthographic();
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x0001635C File Offset: 0x0001475C
		private void Update()
		{
			BattleCamera.eMode mode = this.m_Mode;
			if (mode != BattleCamera.eMode.Default)
			{
				if (mode == BattleCamera.eMode.Zoom)
				{
					this.m_t += Time.deltaTime;
					float num = (this.m_TargetTime != 0f) ? (this.m_t / this.m_TargetTime) : 1f;
					if (num > 1f)
					{
						num = 1f;
					}
					num = 1f - num;
					float num2 = 1f - Mathf.Pow(num, 1.5f);
					Vector3 position = new Vector3(this.easing(this.m_t, this.m_StartPos.x, this.m_TargetPos.x - this.m_StartPos.x, this.m_TargetTime), this.easing(this.m_t, this.m_StartPos.y, this.m_TargetPos.y - this.m_StartPos.y, this.m_TargetTime), this.easing(this.m_t, this.m_StartPos.z, this.m_TargetPos.z - this.m_StartPos.z, this.m_TargetTime));
					float orthographicSize = this.easing(this.m_t, this.m_StartOrthographicSize, this.m_TargetOrthographicSize - this.m_StartOrthographicSize, this.m_TargetTime);
					this.m_Transform.position = position;
					this.m_Camera.orthographicSize = orthographicSize;
					bool flag = num2 >= 1f;
					if (flag)
					{
						this.m_Mode = BattleCamera.eMode.Default;
						if (this.m_IsToDefault)
						{
							this.m_IsToDefault = false;
							this.m_IsZoomed = false;
						}
					}
				}
			}
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00016508 File Offset: 0x00014908
		private float easing(float currentTime, float startValue, float changeValue, float duration)
		{
			return changeValue * (-1f * (float)Math.Pow(2.0, (double)(-10f * currentTime / duration)) + 1f) + startValue;
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x00016534 File Offset: 0x00014934
		public void Pause()
		{
			this.m_Camera.clearFlags = CameraClearFlags.Color;
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00016542 File Offset: 0x00014942
		public void Resume()
		{
			this.ResetDefault();
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x0001654C File Offset: 0x0001494C
		public void ResetDefault()
		{
			this.m_Mode = BattleCamera.eMode.Default;
			this.m_IsToDefault = false;
			this.m_IsZoomed = false;
			this.ApplyDefaultOrthographic();
			this.m_Transform.position = this.m_DefaultPos;
			this.m_Transform.rotation = Quaternion.Euler(0f, 0f, 0f);
			this.m_Camera.nearClipPlane = this.m_DefaultNear;
			this.m_Camera.farClipPlane = this.m_DefaultFar;
			this.m_Camera.clearFlags = CameraClearFlags.Color;
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x000165D4 File Offset: 0x000149D4
		public void SetZoom(BattleCamera.eZoomType zoomType)
		{
			if (zoomType >= BattleCamera.eZoomType.ToPlayer && zoomType < (BattleCamera.eZoomType)this.m_ZoomParams.Length)
			{
				BattleCamera.ZoomParam zoomParam = this.m_ZoomParams[(int)zoomType];
				this.m_Transform.position = zoomParam.m_TargetPos;
				this.m_Camera.orthographicSize = zoomParam.m_TargetOrthographicSize;
			}
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00016623 File Offset: 0x00014A23
		public void ToDefault(float zoomTime = 0f)
		{
			this.m_IsToDefault = true;
			this.m_DefaultOrthographicSize = this.CalcDefaultOrthographic();
			this.ZoomStart(this.m_DefaultPos, this.m_DefaultOrthographicSize, (zoomTime != 0f) ? zoomTime : this.m_ZoomToDefaultTime);
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00016664 File Offset: 0x00014A64
		public void ZoomStart(BattleCamera.eZoomType zoomType)
		{
			if (zoomType >= BattleCamera.eZoomType.ToPlayer && zoomType < (BattleCamera.eZoomType)this.m_ZoomParams.Length)
			{
				BattleCamera.ZoomParam zoomParam = this.m_ZoomParams[(int)zoomType];
				Vector3 targetPos = zoomParam.m_TargetPos;
				float targetOrthographicSize = zoomParam.m_TargetOrthographicSize;
				this.ZoomStart(targetPos, targetOrthographicSize, zoomParam.m_TargetTime);
			}
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x000166B0 File Offset: 0x00014AB0
		private void ZoomStart(Vector3 targetPos, float targeOrthographicSize, float time)
		{
			this.m_TargetPos = targetPos;
			this.m_TargetOrthographicSize = targeOrthographicSize;
			this.m_TargetTime = time;
			this.m_t = 0f;
			this.m_Mode = BattleCamera.eMode.Zoom;
			this.m_IsZoomed = true;
			this.m_StartPos = this.m_Transform.position;
			this.m_StartOrthographicSize = this.m_Camera.orthographicSize;
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x0001670D File Offset: 0x00014B0D
		private void ApplyDefaultOrthographic()
		{
			this.m_DefaultOrthographicSize = this.CalcDefaultOrthographic();
			this.m_Camera.orthographicSize = this.m_DefaultOrthographicSize;
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x0001672C File Offset: 0x00014B2C
		private float CalcDefaultOrthographic()
		{
			float num = (float)Screen.height / 2f / this.m_BasePixelPerUnit;
			float num2 = this.m_BaseHeight / this.m_BaseWidth;
			float num3 = (float)Screen.height / (float)Screen.width;
			if (num2 > num3)
			{
				float num4 = this.m_BaseHeight / (float)Screen.height;
				num *= num4;
			}
			else
			{
				float num5 = this.m_BaseWidth / (float)Screen.width;
				num *= num5;
			}
			return num;
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x0001679C File Offset: 0x00014B9C
		public void Shake(BattleCamera.eShakeType shakeType)
		{
			if (shakeType >= BattleCamera.eShakeType.LastBlow && shakeType < (BattleCamera.eShakeType)this.m_ShakeParams.Length)
			{
				iTween.Stop(base.gameObject);
				iTween.ShakePosition(base.gameObject, iTween.Hash(new object[]
				{
					"x",
					this.m_ShakeParams[(int)shakeType].m_Shake.x,
					"y",
					this.m_ShakeParams[(int)shakeType].m_Shake.y,
					"time",
					this.m_ShakeParams[(int)shakeType].m_Shake.z,
					"delay",
					0f,
					"easetype",
					this.m_ShakeParams[(int)shakeType].m_EaseType
				}));
			}
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x0001687B File Offset: 0x00014C7B
		public void SetEnablePostProcess(bool flg)
		{
			if (this.m_PostProcess != null)
			{
				this.m_PostProcess.enabled = flg;
			}
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x0001689A File Offset: 0x00014C9A
		public bool IsEnablePostProcess()
		{
			return this.m_PostProcess != null && this.m_PostProcess.enabled;
		}

		// Token: 0x040002D1 RID: 721
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x040002D2 RID: 722
		[SerializeField]
		private PostProcessRenderer m_PostProcess;

		// Token: 0x040002D3 RID: 723
		[SerializeField]
		private float m_BaseWidth = 1920f;

		// Token: 0x040002D4 RID: 724
		[SerializeField]
		private float m_BaseHeight = 1080f;

		// Token: 0x040002D5 RID: 725
		[SerializeField]
		private float m_BasePixelPerUnit = 100f;

		// Token: 0x040002D6 RID: 726
		[SerializeField]
		private BattleCamera.ZoomParam[] m_ZoomParams;

		// Token: 0x040002D7 RID: 727
		[SerializeField]
		private float m_ZoomToDefaultTime = 1f;

		// Token: 0x040002D8 RID: 728
		[SerializeField]
		private BattleCamera.ShakeParam[] m_ShakeParams;

		// Token: 0x040002D9 RID: 729
		private Camera m_Camera;

		// Token: 0x040002DA RID: 730
		private Vector3 m_DefaultPos = Vector3.zero;

		// Token: 0x040002DB RID: 731
		private float m_DefaultNear;

		// Token: 0x040002DC RID: 732
		private float m_DefaultFar;

		// Token: 0x040002DD RID: 733
		private float m_DefaultOrthographicSize;

		// Token: 0x040002DE RID: 734
		private BattleCamera.eMode m_Mode;

		// Token: 0x040002DF RID: 735
		private Vector3 m_StartPos = Vector3.zero;

		// Token: 0x040002E0 RID: 736
		private Vector3 m_TargetPos = Vector3.zero;

		// Token: 0x040002E1 RID: 737
		private float m_StartOrthographicSize;

		// Token: 0x040002E2 RID: 738
		private float m_TargetOrthographicSize;

		// Token: 0x040002E3 RID: 739
		private float m_TargetTime;

		// Token: 0x040002E4 RID: 740
		private float m_t;

		// Token: 0x040002E5 RID: 741
		private bool m_IsToDefault;

		// Token: 0x040002E6 RID: 742
		private bool m_IsZoomed;

		// Token: 0x0200009E RID: 158
		public enum eZoomType
		{
			// Token: 0x040002E8 RID: 744
			ToPlayer,
			// Token: 0x040002E9 RID: 745
			ToEnemy,
			// Token: 0x040002EA RID: 746
			ToPlayerOnUniqueSkillResult,
			// Token: 0x040002EB RID: 747
			ToEnemyOnUniqueSkillResult,
			// Token: 0x040002EC RID: 748
			ToPlayerOnBattleEnd,
			// Token: 0x040002ED RID: 749
			ToEnemyOnBattleEnd,
			// Token: 0x040002EE RID: 750
			ToPlayerOnStun,
			// Token: 0x040002EF RID: 751
			ToEnemyOnStun,
			// Token: 0x040002F0 RID: 752
			QuestStart,
			// Token: 0x040002F1 RID: 753
			WaveOut,
			// Token: 0x040002F2 RID: 754
			Num
		}

		// Token: 0x0200009F RID: 159
		[Serializable]
		public class ZoomParam
		{
			// Token: 0x040002F3 RID: 755
			public Vector3 m_TargetPos;

			// Token: 0x040002F4 RID: 756
			public float m_TargetOrthographicSize;

			// Token: 0x040002F5 RID: 757
			public float m_TargetTime;
		}

		// Token: 0x020000A0 RID: 160
		public enum eShakeType
		{
			// Token: 0x040002F7 RID: 759
			LastBlow,
			// Token: 0x040002F8 RID: 760
			Hit_Regist,
			// Token: 0x040002F9 RID: 761
			Hit_Default,
			// Token: 0x040002FA RID: 762
			Hit_Weak,
			// Token: 0x040002FB RID: 763
			Stun
		}

		// Token: 0x020000A1 RID: 161
		[Serializable]
		public class ShakeParam
		{
			// Token: 0x040002FC RID: 764
			public Vector3 m_Shake;

			// Token: 0x040002FD RID: 765
			public iTween.EaseType m_EaseType;
		}

		// Token: 0x020000A2 RID: 162
		public enum eMode
		{
			// Token: 0x040002FF RID: 767
			Default,
			// Token: 0x04000300 RID: 768
			Zoom
		}
	}
}
