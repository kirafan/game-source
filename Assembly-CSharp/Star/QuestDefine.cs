﻿using System;

namespace Star
{
	// Token: 0x02000521 RID: 1313
	public static class QuestDefine
	{
		// Token: 0x040020AE RID: 8366
		public const int DIFFICULTY_NUM = 3;

		// Token: 0x040020AF RID: 8367
		public const int STORY_EVENT_TYPE = -1;

		// Token: 0x02000522 RID: 1314
		public enum eClearRank
		{
			// Token: 0x040020B1 RID: 8369
			None,
			// Token: 0x040020B2 RID: 8370
			Star1,
			// Token: 0x040020B3 RID: 8371
			Star2,
			// Token: 0x040020B4 RID: 8372
			StarMax,
			// Token: 0x040020B5 RID: 8373
			Num
		}

		// Token: 0x02000523 RID: 1315
		public enum eDifficulty
		{
			// Token: 0x040020B7 RID: 8375
			Normal,
			// Token: 0x040020B8 RID: 8376
			Hard,
			// Token: 0x040020B9 RID: 8377
			Star,
			// Token: 0x040020BA RID: 8378
			Num
		}

		// Token: 0x02000524 RID: 1316
		public enum eQuestLogAddResult
		{
			// Token: 0x040020BC RID: 8380
			Success,
			// Token: 0x040020BD RID: 8381
			StaminaIsShort,
			// Token: 0x040020BE RID: 8382
			KeyItemIsShort,
			// Token: 0x040020BF RID: 8383
			AnyExConditionError
		}

		// Token: 0x02000525 RID: 1317
		public enum eReturnLogAdd
		{
			// Token: 0x040020C1 RID: 8385
			Success,
			// Token: 0x040020C2 RID: 8386
			StaminaIsShort,
			// Token: 0x040020C3 RID: 8387
			QuestOutOfPerod,
			// Token: 0x040020C4 RID: 8388
			QuestOrderLimit,
			// Token: 0x040020C5 RID: 8389
			ItemIsShort,
			// Token: 0x040020C6 RID: 8390
			Unknown
		}
	}
}
