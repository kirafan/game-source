﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020001B0 RID: 432
	public static class SoundVoiceListDB_Ext
	{
		// Token: 0x06000B27 RID: 2855 RVA: 0x000426B0 File Offset: 0x00040AB0
		public static SoundVoiceListDB_Param GetParam(this SoundVoiceListDB self, eSoundVoiceListDB cueID, Dictionary<eSoundVoiceListDB, int> indices)
		{
			if (indices != null)
			{
				int num = 0;
				if (indices.TryGetValue(cueID, out num))
				{
					return self.m_Params[num];
				}
			}
			return default(SoundVoiceListDB_Param);
		}
	}
}
