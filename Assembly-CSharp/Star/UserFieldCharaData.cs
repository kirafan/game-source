﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020003A4 RID: 932
	public class UserFieldCharaData
	{
		// Token: 0x06001180 RID: 4480 RVA: 0x0005C4C8 File Offset: 0x0005A8C8
		public void AddRoomInChara(PlayerFieldPartyMember pwwwfldchara)
		{
			int num = pwwwfldchara.characterId;
			if (num <= 0)
			{
				UserCharacterData charaData = UserCharaUtil.GetCharaData(pwwwfldchara.managedCharacterId);
				if (charaData != null)
				{
					num = charaData.Param.CharaID;
				}
			}
			UserFieldCharaData.RoomInCharaData roomInCharaData = new UserFieldCharaData.RoomInCharaData(pwwwfldchara.managedCharacterId, num, pwwwfldchara.roomId, pwwwfldchara.liveIdx);
			roomInCharaData.ServerManageID = pwwwfldchara.managedPartyMemberId;
			roomInCharaData.PlayScheduleTagID = pwwwfldchara.scheduleId;
			roomInCharaData.ScheduleTagID = pwwwfldchara.scheduleTag;
			roomInCharaData.TouchItemResultNo = pwwwfldchara.touchItemResultNo;
			roomInCharaData.TownBuildManageID = pwwwfldchara.managedFacilityId;
			roomInCharaData.Flags = pwwwfldchara.flag;
			if (roomInCharaData.PlayScheduleTagID <= 0)
			{
				roomInCharaData.PlayScheduleTagID = roomInCharaData.ScheduleTagID;
			}
			if (pwwwfldchara.scheduleTable != null)
			{
				string @string = Encoding.ASCII.GetString(Convert.FromBase64String(pwwwfldchara.scheduleTable));
				roomInCharaData.m_Schedule = JsonConvert.DeserializeObject<UserScheduleData.Play>(@string);
			}
			this.m_RoomInChara.Add(roomInCharaData);
		}

		// Token: 0x06001181 RID: 4481 RVA: 0x0005C5B4 File Offset: 0x0005A9B4
		public PlayerFieldPartyMember CreateRoomInChara(long fchrmngid)
		{
			UserFieldCharaData.RoomInCharaData roomInChara = this.GetRoomInChara(fchrmngid);
			PlayerFieldPartyMember playerFieldPartyMember = new PlayerFieldPartyMember();
			playerFieldPartyMember.managedPartyMemberId = roomInChara.ServerManageID;
			playerFieldPartyMember.managedCharacterId = roomInChara.ManageID;
			playerFieldPartyMember.characterId = roomInChara.CharacterID;
			playerFieldPartyMember.scheduleId = roomInChara.PlayScheduleTagID;
			playerFieldPartyMember.scheduleTag = roomInChara.ScheduleTagID;
			playerFieldPartyMember.managedFacilityId = roomInChara.TownBuildManageID;
			playerFieldPartyMember.roomId = roomInChara.RoomID;
			playerFieldPartyMember.liveIdx = roomInChara.LiveIdx;
			playerFieldPartyMember.touchItemResultNo = roomInChara.TouchItemResultNo;
			playerFieldPartyMember.flag = roomInChara.Flags;
			if (roomInChara.m_Schedule != null)
			{
				roomInChara.m_Schedule.m_Version = 1;
				string s = JsonConvert.SerializeObject(roomInChara.m_Schedule);
				playerFieldPartyMember.scheduleTable = Convert.ToBase64String(Encoding.ASCII.GetBytes(s));
			}
			return playerFieldPartyMember;
		}

		// Token: 0x06001182 RID: 4482 RVA: 0x0005C684 File Offset: 0x0005AA84
		public ChangeScheduleMember CreateRoomInCharaSchedule(long fchrmngid)
		{
			UserFieldCharaData.RoomInCharaData roomInChara = this.GetRoomInChara(fchrmngid);
			ChangeScheduleMember changeScheduleMember = new ChangeScheduleMember();
			changeScheduleMember.managedPartyMemberId = roomInChara.ServerManageID;
			changeScheduleMember.scheduleId = roomInChara.PlayScheduleTagID;
			changeScheduleMember.scheduleTag = roomInChara.ScheduleTagID;
			changeScheduleMember.managedFacilityId = roomInChara.TownBuildManageID;
			changeScheduleMember.touchItemResultNo = roomInChara.TouchItemResultNo;
			changeScheduleMember.flag = roomInChara.Flags;
			changeScheduleMember.scheduleTable = null;
			changeScheduleMember.dropPresent = roomInChara.CreateDropItemList();
			roomInChara.ClearDropItemList();
			if (roomInChara.m_Schedule != null)
			{
				roomInChara.m_Schedule.m_Version = 1;
				string s = JsonConvert.SerializeObject(roomInChara.m_Schedule);
				changeScheduleMember.scheduleTable = Convert.ToBase64String(Encoding.ASCII.GetBytes(s));
				roomInChara.m_NewUpSchedule = false;
			}
			return changeScheduleMember;
		}

		// Token: 0x06001183 RID: 4483 RVA: 0x0005C744 File Offset: 0x0005AB44
		public void CheckManageCharaID(UserFieldCharaData.UpFieldCharaData[] fcharamngid, UserFieldCharaData.CharaInOutEvent pcallback = null)
		{
			int num = fcharamngid.Length;
			int count = this.m_RoomInChara.Count;
			bool[] array = new bool[this.m_RoomInChara.Count];
			for (int i = 0; i < num; i++)
			{
				if (fcharamngid[i].ManageID != -1L)
				{
					bool flag = false;
					for (int j = 0; j < count; j++)
					{
						if (this.m_RoomInChara[j].ManageID == fcharamngid[i].ManageID)
						{
							array[j] = true;
							flag = true;
							bool flag2 = false;
							if (this.m_RoomInChara[j].RoomID != fcharamngid[i].RoomID || this.m_RoomInChara[j].LiveIdx != fcharamngid[i].LiveIdx)
							{
								flag2 = true;
							}
							this.m_RoomInChara[j].RoomID = fcharamngid[i].RoomID;
							this.m_RoomInChara[j].LiveIdx = fcharamngid[i].LiveIdx;
							if (flag2 && pcallback != null)
							{
								pcallback(UserFieldCharaData.eUpState.StateChg, this.m_RoomInChara[j]);
							}
							break;
						}
					}
					if (!flag)
					{
						UserFieldCharaData.RoomInCharaData pchara = this.AddRoomInChara(fcharamngid[i].ManageID, fcharamngid[i].CharaID, fcharamngid[i].RoomID, fcharamngid[i].LiveIdx);
						if (pcallback != null)
						{
							pcallback(UserFieldCharaData.eUpState.In, pchara);
						}
					}
				}
			}
			for (int i = count - 1; i >= 0; i--)
			{
				if (!array[i])
				{
					if (pcallback != null)
					{
						pcallback(UserFieldCharaData.eUpState.Out, this.m_RoomInChara[i]);
					}
					this.m_RoomInChara.RemoveAt(i);
				}
			}
		}

		// Token: 0x06001184 RID: 4484 RVA: 0x0005C914 File Offset: 0x0005AD14
		public static UserFieldCharaData.UpFieldCharaData[] CreateSetUpData(long[] charaMngIDs)
		{
			UserFieldCharaData.UpFieldCharaData[] array = new UserFieldCharaData.UpFieldCharaData[charaMngIDs.Length];
			for (int i = 0; i < charaMngIDs.Length; i++)
			{
				array[i].ManageID = charaMngIDs[i];
				if (i < 5)
				{
					array[i].RoomID = 1;
				}
				else
				{
					array[i].RoomID = 2;
				}
				array[i].LiveIdx = i;
				UserCharacterData charaData = UserCharaUtil.GetCharaData(charaMngIDs[i]);
				if (charaData != null)
				{
					array[i].CharaID = charaData.Param.CharaID;
				}
			}
			return array;
		}

		// Token: 0x06001185 RID: 4485 RVA: 0x0005C9A7 File Offset: 0x0005ADA7
		public void UpManageID(long fmanageid)
		{
			this.MngID = fmanageid;
		}

		// Token: 0x06001186 RID: 4486 RVA: 0x0005C9B0 File Offset: 0x0005ADB0
		public void ClearAll(long fmanageid = -1L)
		{
			if (fmanageid == -1L || fmanageid != this.MngID)
			{
				this.m_RoomInChara.Clear();
			}
			this.MngID = fmanageid;
		}

		// Token: 0x06001187 RID: 4487 RVA: 0x0005C9D8 File Offset: 0x0005ADD8
		public UserFieldCharaData.RoomInCharaData AddRoomInChara(long fmanageid, int fcharaid, int froomno = 0, int liveIdx = 0)
		{
			UserFieldCharaData.RoomInCharaData roomInCharaData = new UserFieldCharaData.RoomInCharaData(fmanageid, fcharaid, froomno, liveIdx);
			this.m_RoomInChara.Add(roomInCharaData);
			return roomInCharaData;
		}

		// Token: 0x06001188 RID: 4488 RVA: 0x0005CA00 File Offset: 0x0005AE00
		public void RemoveRoomInChara(long fmanageid)
		{
			for (int i = 0; i < this.m_RoomInChara.Count; i++)
			{
				if (this.m_RoomInChara[i].ManageID == fmanageid)
				{
					this.m_RoomInChara.RemoveAt(i);
					break;
				}
			}
		}

		// Token: 0x06001189 RID: 4489 RVA: 0x0005CA51 File Offset: 0x0005AE51
		public int GetRoomInCharaNum()
		{
			return this.m_RoomInChara.Count;
		}

		// Token: 0x0600118A RID: 4490 RVA: 0x0005CA5E File Offset: 0x0005AE5E
		public UserFieldCharaData.RoomInCharaData GetRoomInCharaAt(int findex)
		{
			return this.m_RoomInChara[findex];
		}

		// Token: 0x0600118B RID: 4491 RVA: 0x0005CA6C File Offset: 0x0005AE6C
		public UserFieldCharaData.RoomInCharaData GetRoomInChara(long fmanageid)
		{
			int count = this.m_RoomInChara.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_RoomInChara[i].ManageID == fmanageid)
				{
					return this.m_RoomInChara[i];
				}
			}
			return null;
		}

		// Token: 0x0600118C RID: 4492 RVA: 0x0005CABC File Offset: 0x0005AEBC
		public long[] GetRoomInCharaManageID()
		{
			long[] array = new long[10];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = -1L;
			}
			for (int j = 0; j < this.m_RoomInChara.Count; j++)
			{
				if (this.m_RoomInChara[j].LiveIdx >= 0)
				{
					array[this.m_RoomInChara[j].LiveIdx] = this.m_RoomInChara[j].ManageID;
				}
			}
			return array;
		}

		// Token: 0x0600118D RID: 4493 RVA: 0x0005CB44 File Offset: 0x0005AF44
		public long[] GetMaskBuildMngIDInCharaManageID(long fbuildmanageid)
		{
			int num = 0;
			for (int i = 0; i < this.m_RoomInChara.Count; i++)
			{
				if (this.m_RoomInChara[i].TownBuildManageID == fbuildmanageid)
				{
					num++;
				}
			}
			long[] array = new long[num];
			num = 0;
			for (int i = 0; i < this.m_RoomInChara.Count; i++)
			{
				if (this.m_RoomInChara[i].TownBuildManageID == fbuildmanageid)
				{
					array[i] = this.m_RoomInChara[i].ManageID;
					num++;
				}
			}
			return array;
		}

		// Token: 0x0600118E RID: 4494 RVA: 0x0005CBE0 File Offset: 0x0005AFE0
		public long[] GetNoMaskBuildMngIDInCharaManageID(long fbuildmanageid)
		{
			int num = 0;
			for (int i = 0; i < this.m_RoomInChara.Count; i++)
			{
				if (this.m_RoomInChara[i].TownBuildManageID != fbuildmanageid)
				{
					num++;
				}
			}
			long[] array = new long[num];
			num = 0;
			for (int i = 0; i < this.m_RoomInChara.Count; i++)
			{
				if (this.m_RoomInChara[i].TownBuildManageID != fbuildmanageid)
				{
					array[i] = this.m_RoomInChara[i].ManageID;
					num++;
				}
			}
			return array;
		}

		// Token: 0x0600118F RID: 4495 RVA: 0x0005CC7C File Offset: 0x0005B07C
		public bool IsEntryFldChara(long fmanageid)
		{
			bool result = false;
			int roomInCharaNum = this.GetRoomInCharaNum();
			for (int i = 0; i < roomInCharaNum; i++)
			{
				if (this.GetRoomInCharaAt(i).ManageID == fmanageid)
				{
					result = true;
					break;
				}
			}
			return result;
		}

		// Token: 0x04001841 RID: 6209
		public const int TOUCH_ITEM_NEW = 1;

		// Token: 0x04001842 RID: 6210
		public const int TOUCH_ITEM_SP = 2;

		// Token: 0x04001843 RID: 6211
		public const int CHANGE_SCHEDULE = 4;

		// Token: 0x04001844 RID: 6212
		public const int NEW_SCHEDULE = 8;

		// Token: 0x04001845 RID: 6213
		public const int SCHEDULE_CLEAR = 16;

		// Token: 0x04001846 RID: 6214
		public const int SCHEDULE_MARK = 32;

		// Token: 0x04001847 RID: 6215
		public const int ROOM_CATEGORY_MAIN = 1;

		// Token: 0x04001848 RID: 6216
		public const int ROOM_CATEGORY_SUB = 2;

		// Token: 0x04001849 RID: 6217
		public const int ROOM_LIVE_MAX = 5;

		// Token: 0x0400184A RID: 6218
		public const int ROOM_NUM_MAX = 2;

		// Token: 0x0400184B RID: 6219
		public const int Version = 0;

		// Token: 0x0400184C RID: 6220
		public const int ScheduleVersion = 1;

		// Token: 0x0400184D RID: 6221
		public long MngID = -1L;

		// Token: 0x0400184E RID: 6222
		public long ScheduleTime;

		// Token: 0x0400184F RID: 6223
		private List<UserFieldCharaData.RoomInCharaData> m_RoomInChara = new List<UserFieldCharaData.RoomInCharaData>();

		// Token: 0x020003A5 RID: 933
		public class RoomInCharaData
		{
			// Token: 0x06001190 RID: 4496 RVA: 0x0005CCC0 File Offset: 0x0005B0C0
			public RoomInCharaData(long fmanageid, int fcharaid, int froomno, int liveIdx)
			{
				this.ServerManageID = -1L;
				this.TownBuildManageID = -1L;
				this.CharacterID = fcharaid;
				this.ManageID = fmanageid;
				this.ScheduleTagID = -1;
				this.RoomID = froomno;
				this.LiveIdx = liveIdx;
				this.TouchItemResultNo = -1;
				this.Flags = 0;
				this.m_DropList = new List<UserFieldCharaData.ScheduleDropItem>();
			}

			// Token: 0x06001191 RID: 4497 RVA: 0x0005CD20 File Offset: 0x0005B120
			public void CheckDefaultScheduleUp(long ftimes)
			{
				this.m_isNewCreateSchedule = false;
				if (this.m_Schedule == null)
				{
					this.CreateInitCharaSchedule(ftimes);
				}
				if ((this.Flags & 8) != 0)
				{
					this.m_NewSchedule = true;
				}
				if ((this.Flags & 4) != 0)
				{
					this.m_ChangeScheduleTag = true;
				}
				if ((this.Flags & 1) != 0)
				{
					this.m_TouchItemNew = true;
				}
				if ((this.Flags & 2) != 0)
				{
					this.m_TouchItemSp = true;
				}
				if ((this.Flags & 32) != 0)
				{
					this.m_ScheduleBaseUp = true;
				}
			}

			// Token: 0x06001192 RID: 4498 RVA: 0x0005CDAC File Offset: 0x0005B1AC
			public void CreateInitCharaSchedule(long fsystemtime)
			{
				UserScheduleData.ListPack listPack = new UserScheduleData.ListPack();
				ScheduleDataBase.GetScheduleSetUpDB().CreateList(this.ManageID, fsystemtime, ref listPack, this.RoomID > 1);
				UserScheduleUtil.TimeToScheduleStateUp(listPack, eTownMoveState.SetUp, fsystemtime);
				this.SetSchedule(listPack);
				this.SetSchedulePlayTime(fsystemtime);
				this.SetLinkObjectPack(listPack, fsystemtime);
				this.m_isNewCreateSchedule = true;
			}

			// Token: 0x06001193 RID: 4499 RVA: 0x0005CE00 File Offset: 0x0005B200
			public void SetLinkObjectPack(UserScheduleData.ListPack pschedule, long fsec)
			{
				int listNum = pschedule.GetListNum();
				fsec = (long)ScheduleTimeUtil.GetDayTimeSec(fsec);
				for (int i = 0; i < listNum; i++)
				{
					UserScheduleData.Seg table = pschedule.GetTable(i);
					if ((long)table.m_WakeTime <= fsec && (long)(table.m_WakeTime + table.m_UseTime) > fsec)
					{
						UserScheduleData.eType type = table.m_Type;
						if (type != UserScheduleData.eType.Room)
						{
							if (type == UserScheduleData.eType.Sleep)
							{
								this.TownBuildManageID = TownDefine.ROOM_ACCESS_MNG_ID;
								this.ScheduleTagID = (this.PlayScheduleTagID = table.m_TagNameID);
							}
						}
						else
						{
							this.TownBuildManageID = TownDefine.ROOM_ACCESS_MNG_ID;
							this.ScheduleTagID = (this.PlayScheduleTagID = table.m_TagNameID);
						}
						break;
					}
				}
			}

			// Token: 0x06001194 RID: 4500 RVA: 0x0005CECC File Offset: 0x0005B2CC
			public void ChkUpSchedule(bool frefresh, long fsystemtime)
			{
				UserScheduleData.ListPack listPack = null;
				if (this.m_Schedule == null)
				{
					listPack = new UserScheduleData.ListPack();
					ScheduleDataBase.GetScheduleSetUpDB().CreateList(this.ManageID, fsystemtime, ref listPack, this.RoomID > 1);
					UserScheduleUtil.TimeToScheduleStateUp(listPack, eTownMoveState.SetUp, fsystemtime);
					this.m_NewSchedule = true;
				}
				else if (frefresh)
				{
					listPack = new UserScheduleData.ListPack();
					this.m_NewSchedule = true;
					ScheduleDataBase.GetScheduleSetUpDB().CreateList(this.ManageID, fsystemtime, ref listPack, this.RoomID > 1);
					this.m_ScheduleBaseUp = true;
				}
				if (this.m_NewSchedule && listPack != null)
				{
					this.SetSchedule(listPack);
				}
			}

			// Token: 0x06001195 RID: 4501 RVA: 0x0005CF6A File Offset: 0x0005B36A
			private void SetScheduleDropParam(int fscheduletag, int fcharaid, int fscheduletimes, eTownMoveState fupmove)
			{
				this.SetScheduleDropKey(-1, 0f);
				ScheduleNameUtil.CalcScheduleTagToDropItemKey(this, fscheduletag, fscheduletimes, fcharaid, fupmove);
			}

			// Token: 0x06001196 RID: 4502 RVA: 0x0005CF84 File Offset: 0x0005B384
			public void UserStateFlagUp()
			{
				this.Flags = 0;
				if (this.m_NewSchedule)
				{
					this.Flags |= 8;
				}
				if (this.m_ChangeScheduleTag)
				{
					this.Flags |= 4;
				}
				if (this.m_TouchItemNew)
				{
					this.Flags |= 1;
				}
				if (this.m_TouchItemSp)
				{
					this.Flags |= 2;
				}
				if (this.m_ScheduleBaseUp)
				{
					this.Flags |= 32;
				}
			}

			// Token: 0x06001197 RID: 4503 RVA: 0x0005D018 File Offset: 0x0005B418
			public void ChangeBuildPoint(long fpointmngid, int fscheduletag, int fnewscheudletag, int fscheduletime, int fchrid, long fkeytime, eTownMoveState fupmove)
			{
				this.PushScheduleDropItem(ScheduleTimeUtil.ChangeBaseTimeToDateTime(fkeytime).Ticks);
				this.TownBuildManageID = fpointmngid;
				this.SetScheduleDropParam(fscheduletag, fchrid, fscheduletime, fupmove);
				this.m_TouchItemNew = false;
				this.m_TouchItemSp = false;
				if (this.ScheduleTagID != fscheduletag)
				{
					this.m_ChangeScheduleTag = true;
					ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(fscheduletag);
					if (scheduleNameTag.m_GroupWakeUp.Length > 1)
					{
						int num = UnityEngine.Random.Range(0, 100);
						num -= (int)scheduleNameTag.m_GroupWakeUp[0];
						if (num <= 0)
						{
							int num2 = scheduleNameTag.m_ResultBaseNo.Length;
							if (scheduleNameTag.m_WakeBaseUp.Length < num2)
							{
								num2 = scheduleNameTag.m_WakeBaseUp.Length;
							}
							if (num2 != 0)
							{
								int num3 = UnityEngine.Random.Range(0, 100);
								for (int i = 0; i < num2; i++)
								{
									num3 -= (int)scheduleNameTag.m_WakeBaseUp[i];
									if (num3 <= 0 && scheduleNameTag.m_WakeBaseUp[i] != 0)
									{
										this.m_TouchItemNew = true;
										this.TouchItemResultNo = (int)scheduleNameTag.m_ResultBaseNo[i];
										break;
									}
								}
							}
						}
						else
						{
							num -= (int)scheduleNameTag.m_GroupWakeUp[1];
							if (num <= 0)
							{
								int num2 = scheduleNameTag.m_ResultSpNo.Length;
								if (scheduleNameTag.m_WakeSpUp.Length < num2)
								{
									num2 = scheduleNameTag.m_WakeSpUp.Length;
								}
								if (num2 != 0)
								{
									int num4 = UnityEngine.Random.Range(0, 100);
									for (int i = 0; i < num2; i++)
									{
										num4 -= (int)scheduleNameTag.m_WakeSpUp[i];
										if (num4 <= 0 && scheduleNameTag.m_WakeSpUp[i] != 0)
										{
											this.m_TouchItemSp = true;
											this.m_TouchItemNew = true;
											this.TouchItemResultNo = (int)scheduleNameTag.m_ResultSpNo[i];
											break;
										}
									}
								}
							}
						}
					}
				}
				this.ScheduleTagID = fscheduletag;
				this.PlayScheduleTagID = fnewscheudletag;
				this.UserStateFlagUp();
			}

			// Token: 0x06001198 RID: 4504 RVA: 0x0005D1E3 File Offset: 0x0005B5E3
			public void ClearTouchItemEvent()
			{
				this.m_TouchItemNew = false;
				this.UserStateFlagUp();
			}

			// Token: 0x06001199 RID: 4505 RVA: 0x0005D1F2 File Offset: 0x0005B5F2
			public void ClearTouchItemNew()
			{
				this.m_TouchItemNew = false;
				this.m_TouchItemSp = false;
				this.TouchItemResultNo = -1;
				this.UserStateFlagUp();
			}

			// Token: 0x0600119A RID: 4506 RVA: 0x0005D210 File Offset: 0x0005B610
			public DropPresent[] CreateDropItemList()
			{
				DropPresent[] array = new DropPresent[this.m_DropList.Count];
				for (int i = 0; i < this.m_DropList.Count; i++)
				{
					array[i] = new DropPresent();
					array[i].itemNo = this.m_DropList[i].m_ItemNo;
					array[i].presentAt = this.m_DropList[i].m_DropTime;
					array[i].magnification = this.m_DropList[i].m_Mag;
				}
				return array;
			}

			// Token: 0x0600119B RID: 4507 RVA: 0x0005D29E File Offset: 0x0005B69E
			public void ReserveReCreateSchedule()
			{
				this.m_isNewCreateSchedule = true;
			}

			// Token: 0x0600119C RID: 4508 RVA: 0x0005D2A7 File Offset: 0x0005B6A7
			public bool IsReservedReCreateSchedule()
			{
				return this.m_isNewCreateSchedule;
			}

			// Token: 0x0600119D RID: 4509 RVA: 0x0005D2AF File Offset: 0x0005B6AF
			public void ClearReservedReCreateSchedule()
			{
				this.m_isNewCreateSchedule = false;
			}

			// Token: 0x0600119E RID: 4510 RVA: 0x0005D2B8 File Offset: 0x0005B6B8
			public void SetSchedule(UserScheduleData.ListPack pschedule)
			{
				if (this.m_Schedule == null)
				{
					this.m_Schedule = new UserScheduleData.Play();
				}
				this.m_Schedule.StackList(pschedule);
				this.m_NewUpSchedule = true;
			}

			// Token: 0x0600119F RID: 4511 RVA: 0x0005D2E3 File Offset: 0x0005B6E3
			public UserScheduleData.ListPack GetScheduleList(long fsettingtime)
			{
				if (this.m_Schedule == null)
				{
					return null;
				}
				return this.m_Schedule.m_DayListUp[0];
			}

			// Token: 0x060011A0 RID: 4512 RVA: 0x0005D2FF File Offset: 0x0005B6FF
			public long GetSchedulePlayTime()
			{
				if (this.m_Schedule == null)
				{
					return 0L;
				}
				return this.m_Schedule.m_PlayTime;
			}

			// Token: 0x060011A1 RID: 4513 RVA: 0x0005D31A File Offset: 0x0005B71A
			public void SetSchedulePlayTime(long fplaytime)
			{
				this.m_Schedule.m_PlayTime = fplaytime;
			}

			// Token: 0x060011A2 RID: 4514 RVA: 0x0005D328 File Offset: 0x0005B728
			public void PushScheduleDropItem(long fdroptime)
			{
				if (this.m_Schedule.m_DropItemID != -1)
				{
					DateTime dateTime = new DateTime(fdroptime);
					Debug.Log(string.Concat(new object[]
					{
						"Present For You!!!!!!! ",
						this.m_Schedule.m_DropItemID,
						" (",
						dateTime,
						")"
					}));
					UserFieldCharaData.ScheduleDropItem scheduleDropItem = new UserFieldCharaData.ScheduleDropItem();
					scheduleDropItem.m_ItemNo = this.m_Schedule.m_DropItemID;
					scheduleDropItem.m_DropTime = fdroptime;
					scheduleDropItem.m_Mag = this.m_Schedule.m_DropMagKey;
					this.m_DropList.Add(scheduleDropItem);
				}
				this.m_Schedule.m_DropItemID = -1;
				this.m_Schedule.m_DropMagKey = 0f;
			}

			// Token: 0x060011A3 RID: 4515 RVA: 0x0005D3E9 File Offset: 0x0005B7E9
			public void SetScheduleDropKey(int fdropkey, float fmagkey)
			{
				this.m_Schedule.m_DropItemID = fdropkey;
				this.m_Schedule.m_DropMagKey = fmagkey;
			}

			// Token: 0x060011A4 RID: 4516 RVA: 0x0005D403 File Offset: 0x0005B803
			public void ClearDropItemList()
			{
				this.m_DropList.Clear();
			}

			// Token: 0x04001850 RID: 6224
			public long ServerManageID;

			// Token: 0x04001851 RID: 6225
			public long ManageID;

			// Token: 0x04001852 RID: 6226
			public int CharacterID;

			// Token: 0x04001853 RID: 6227
			public int PlayScheduleTagID;

			// Token: 0x04001854 RID: 6228
			public int ScheduleTagID;

			// Token: 0x04001855 RID: 6229
			public long TownBuildManageID;

			// Token: 0x04001856 RID: 6230
			public int RoomID;

			// Token: 0x04001857 RID: 6231
			public int LiveIdx;

			// Token: 0x04001858 RID: 6232
			public int TouchItemResultNo;

			// Token: 0x04001859 RID: 6233
			public int Flags;

			// Token: 0x0400185A RID: 6234
			public bool m_NewUpSchedule;

			// Token: 0x0400185B RID: 6235
			public bool m_isNewCreateSchedule;

			// Token: 0x0400185C RID: 6236
			public UserScheduleData.Play m_Schedule;

			// Token: 0x0400185D RID: 6237
			public int m_ScheduleDropItemIcon;

			// Token: 0x0400185E RID: 6238
			public bool m_NewSchedule;

			// Token: 0x0400185F RID: 6239
			public bool m_ChangeScheduleTag;

			// Token: 0x04001860 RID: 6240
			public bool m_TouchItemNew;

			// Token: 0x04001861 RID: 6241
			public bool m_TouchItemSp;

			// Token: 0x04001862 RID: 6242
			public bool m_ScheduleBaseUp;

			// Token: 0x04001863 RID: 6243
			public List<UserFieldCharaData.ScheduleDropItem> m_DropList;
		}

		// Token: 0x020003A6 RID: 934
		public enum eUpState
		{
			// Token: 0x04001865 RID: 6245
			In,
			// Token: 0x04001866 RID: 6246
			Out,
			// Token: 0x04001867 RID: 6247
			StateChg
		}

		// Token: 0x020003A7 RID: 935
		// (Invoke) Token: 0x060011A6 RID: 4518
		public delegate void CharaInOutEvent(UserFieldCharaData.eUpState finout, UserFieldCharaData.RoomInCharaData pchara);

		// Token: 0x02000B49 RID: 2889
		public struct UpFieldCharaData
		{
			// Token: 0x040044AC RID: 17580
			public long ManageID;

			// Token: 0x040044AD RID: 17581
			public int RoomID;

			// Token: 0x040044AE RID: 17582
			public int LiveIdx;

			// Token: 0x040044AF RID: 17583
			public int CharaID;
		}

		// Token: 0x02000B4A RID: 2890
		public class ScheduleDropItem
		{
			// Token: 0x040044B0 RID: 17584
			public int m_ItemNo;

			// Token: 0x040044B1 RID: 17585
			public long m_DropTime;

			// Token: 0x040044B2 RID: 17586
			public float m_Mag;
		}
	}
}
