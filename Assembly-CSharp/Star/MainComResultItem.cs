﻿using System;

namespace Star
{
	// Token: 0x020004A8 RID: 1192
	public class MainComResultItem : IMainComCommand
	{
		// Token: 0x06001760 RID: 5984 RVA: 0x0007949E File Offset: 0x0007789E
		public MainComResultItem(int fresultno, int faddnum)
		{
			this.m_ResultNo = fresultno;
			this.m_AddNum = faddnum;
		}

		// Token: 0x06001761 RID: 5985 RVA: 0x000794B4 File Offset: 0x000778B4
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x04001E18 RID: 7704
		public int m_ResultNo;

		// Token: 0x04001E19 RID: 7705
		public int m_AddNum;

		// Token: 0x020004A9 RID: 1193
		// (Invoke) Token: 0x06001763 RID: 5987
		private delegate void CallbackResult(MainComResultItem pup);
	}
}
