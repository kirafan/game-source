﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000554 RID: 1364
	public class CActScriptKeyPosAnime : IRoomScriptData
	{
		// Token: 0x06001AD7 RID: 6871 RVA: 0x0008EFF0 File Offset: 0x0008D3F0
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyPosAnime actXlsKeyPosAnime = (ActXlsKeyPosAnime)pbase;
			this.m_TargetName = actXlsKeyPosAnime.m_TargetName;
			this.m_Pos = actXlsKeyPosAnime.m_Pos;
			this.m_Frame = (float)actXlsKeyPosAnime.m_Times / 30f;
			this.m_CRCKey = CRC32.Calc(this.m_TargetName);
		}

		// Token: 0x040021AB RID: 8619
		public string m_TargetName;

		// Token: 0x040021AC RID: 8620
		public uint m_CRCKey;

		// Token: 0x040021AD RID: 8621
		public Vector3 m_Pos;

		// Token: 0x040021AE RID: 8622
		public float m_Frame;
	}
}
