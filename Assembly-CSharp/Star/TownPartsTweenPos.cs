﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006D0 RID: 1744
	public class TownPartsTweenPos : ITownPartsAction
	{
		// Token: 0x06002286 RID: 8838 RVA: 0x000B83CC File Offset: 0x000B67CC
		public TownPartsTweenPos(Transform pself, Vector3 ftarget, float ftime)
		{
			this.m_Active = true;
			this.m_Marker = pself;
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Target = ftarget;
			this.m_Base = pself.localPosition;
		}

		// Token: 0x06002287 RID: 8839 RVA: 0x000B8407 File Offset: 0x000B6807
		public void ChangePos(Vector3 ftarget, float ftime)
		{
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Target = ftarget;
		}

		// Token: 0x06002288 RID: 8840 RVA: 0x000B8424 File Offset: 0x000B6824
		public override bool PartsUpdate()
		{
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Marker.localPosition = this.m_Target;
				this.m_Active = false;
			}
			if (this.m_Active)
			{
				float num = this.m_Time / this.m_MaxTime;
				this.m_Marker.localPosition = (this.m_Target - this.m_Base) * Mathf.Sin(num * 3.1415927f * 0.5f) + this.m_Base;
			}
			return this.m_Active;
		}

		// Token: 0x04002957 RID: 10583
		private Transform m_Marker;

		// Token: 0x04002958 RID: 10584
		private Vector3 m_Target;

		// Token: 0x04002959 RID: 10585
		private Vector3 m_Base;

		// Token: 0x0400295A RID: 10586
		private float m_Time;

		// Token: 0x0400295B RID: 10587
		private float m_MaxTime;
	}
}
