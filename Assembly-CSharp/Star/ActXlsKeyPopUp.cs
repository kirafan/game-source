﻿using System;

namespace Star
{
	// Token: 0x0200056A RID: 1386
	public class ActXlsKeyPopUp : ActXlsKeyBase
	{
		// Token: 0x06001B11 RID: 6929 RVA: 0x0008F6F9 File Offset: 0x0008DAF9
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_PopUpID);
			pio.Int(ref this.m_PopUpTime);
		}

		// Token: 0x040021FE RID: 8702
		public int m_PopUpID;

		// Token: 0x040021FF RID: 8703
		public int m_PopUpTime;
	}
}
