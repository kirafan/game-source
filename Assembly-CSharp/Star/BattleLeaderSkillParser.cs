﻿using System;

namespace Star
{
	// Token: 0x020000CD RID: 205
	public static class BattleLeaderSkillParser
	{
		// Token: 0x0600057B RID: 1403 RVA: 0x0001C1C8 File Offset: 0x0001A5C8
		public static BattleLeaderSkill Parse(int leaderSkillID, CharacterHandler charaHndl)
		{
			BattleLeaderSkill battleLeaderSkill = new BattleLeaderSkill();
			if (leaderSkillID == -1)
			{
				return battleLeaderSkill;
			}
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			LeaderSkillListDB_Param param = dbMng.LeaderSkillListDB.GetParam(leaderSkillID);
			for (int i = 0; i < param.m_Datas.Length; i++)
			{
				eClassType condClass = (eClassType)param.m_Datas[i].m_CondClass;
				if (condClass == eClassType.None || condClass == charaHndl.CharaParam.ClassType)
				{
					eElementType condElement = (eElementType)param.m_Datas[i].m_CondElement;
					if (condElement == eElementType.None || condElement == charaHndl.CharaParam.ElementType)
					{
						eTitleType condTitleType = (eTitleType)param.m_Datas[i].m_CondTitleType;
						if (condTitleType == eTitleType.None || condTitleType == dbMng.NamedListDB.GetTitleType(charaHndl.CharaParam.NamedType))
						{
							eCharaNamedType condNamedType = (eCharaNamedType)param.m_Datas[i].m_CondNamedType;
							if (condNamedType == eCharaNamedType.None || condNamedType == charaHndl.CharaParam.NamedType)
							{
								eLeaderSkillType type = (eLeaderSkillType)param.m_Datas[i].m_Type;
								if (type == eLeaderSkillType.Status)
								{
									BattleLeaderSkillParser.Solve_Status(param.m_Datas[i].m_Args, battleLeaderSkill);
								}
							}
						}
					}
				}
			}
			return battleLeaderSkill;
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0001C32B File Offset: 0x0001A72B
		public static string LeaderSkillTypeToString(eLeaderSkillType leaderSkillType)
		{
			if (leaderSkillType != eLeaderSkillType.Status)
			{
				return "文字列未定義";
			}
			return "ステータス";
		}

		// Token: 0x0600057D RID: 1405 RVA: 0x0001C344 File Offset: 0x0001A744
		private static void Solve_Status(float[] args, BattleLeaderSkill out_leaderSkill)
		{
			out_leaderSkill.StatusHp += (args[0] *= 0.01f);
			out_leaderSkill.StatusAtk += (args[1] *= 0.01f);
			out_leaderSkill.StatusMgc += (args[2] *= 0.01f);
			out_leaderSkill.StatusDef += (args[3] *= 0.01f);
			out_leaderSkill.StatusMDef += (args[4] *= 0.01f);
			out_leaderSkill.StatusSpd += (args[5] *= 0.01f);
			out_leaderSkill.StatusLuck += (args[6] *= 0.01f);
		}
	}
}
