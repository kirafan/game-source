﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000613 RID: 1555
	public class RoomObjectOption
	{
		// Token: 0x06001E72 RID: 7794 RVA: 0x000A43ED File Offset: 0x000A27ED
		public static void DatabaseSetUp()
		{
			if (RoomObjectOption.ms_RoomObjectOptionListDB == null)
			{
				RoomObjectOption.ms_RoomObjectOptionListDB = new RoomObjectOption.RoomObjecOptiontDataList();
				RoomObjectOption.ms_RoomObjectOptionListDB.LoadResInManager("Prefab/Room/Action/RoomObjectOptionList", ".asset");
			}
		}

		// Token: 0x06001E73 RID: 7795 RVA: 0x000A4417 File Offset: 0x000A2817
		public static bool IsSetUp()
		{
			return RoomObjectOption.ms_RoomObjectOptionListDB.m_Active;
		}

		// Token: 0x06001E74 RID: 7796 RVA: 0x000A4423 File Offset: 0x000A2823
		public static void RoomObjectDatabaseRelease()
		{
			if (RoomObjectOption.ms_RoomObjectOptionListDB != null)
			{
				RoomObjectOption.ms_RoomObjectOptionListDB.Release();
			}
			RoomObjectOption.ms_RoomObjectOptionListDB = null;
		}

		// Token: 0x06001E75 RID: 7797 RVA: 0x000A4440 File Offset: 0x000A2840
		public static RoomObjectOption.RoomOptionTool GetRoomObjectOption(uint fkeyid)
		{
			if (RoomObjectOption.ms_RoomObjectOptionListDB != null)
			{
				for (int i = 0; i < RoomObjectOption.ms_RoomObjectOptionListDB.m_DataID.Length; i++)
				{
					if (RoomObjectOption.ms_RoomObjectOptionListDB.m_DataID[i].m_ID == fkeyid)
					{
						return RoomObjectOption.ms_RoomObjectOptionListDB.m_DataID[i];
					}
				}
			}
			return null;
		}

		// Token: 0x0400250C RID: 9484
		public static RoomObjectOption.RoomObjecOptiontDataList ms_RoomObjectOptionListDB;

		// Token: 0x02000614 RID: 1556
		public struct RoomObjOptionKey
		{
			// Token: 0x0400250D RID: 9485
			public byte m_Key;

			// Token: 0x0400250E RID: 9486
			public byte m_MakeID;

			// Token: 0x0400250F RID: 9487
			public float m_PosX;

			// Token: 0x04002510 RID: 9488
			public float m_PosY;
		}

		// Token: 0x02000615 RID: 1557
		public class RoomOptionTool
		{
			// Token: 0x04002511 RID: 9489
			public uint m_ID;

			// Token: 0x04002512 RID: 9490
			public RoomObjectOption.RoomObjOptionKey[] m_Table;
		}

		// Token: 0x02000616 RID: 1558
		public class RoomObjecOptiontDataList : IDataBaseResource
		{
			// Token: 0x06001E78 RID: 7800 RVA: 0x000A44AC File Offset: 0x000A28AC
			public override void SetUpResource(UnityEngine.Object pdataobj)
			{
				RoomObjectOptionListDB roomObjectOptionListDB = pdataobj as RoomObjectOptionListDB;
				RoomObjectOption.OptionPakage optionPakage = new RoomObjectOption.OptionPakage();
				int num = roomObjectOptionListDB.m_Params.Length;
				for (int i = 0; i < num; i++)
				{
					RoomObjectOption.OptionPakageKey optionPakageKey = optionPakage.GetKey(roomObjectOptionListDB.m_Params[i].m_Group);
					optionPakageKey.m_TableID.Add(i);
				}
				num = optionPakage.GetKeyNum();
				this.m_DataID = new RoomObjectOption.RoomOptionTool[optionPakage.GetKeyNum()];
				for (int i = 0; i < num; i++)
				{
					RoomObjectOption.OptionPakageKey optionPakageKey = optionPakage.GetIndexToKey(i);
					this.m_DataID[i] = new RoomObjectOption.RoomOptionTool();
					this.m_DataID[i].m_ID = optionPakageKey.m_GroupID;
					this.m_DataID[i].m_Table = new RoomObjectOption.RoomObjOptionKey[optionPakageKey.GetOptionKeyNum(roomObjectOptionListDB)];
					optionPakageKey.MakeOptionKeyData(roomObjectOptionListDB, ref this.m_DataID[i]);
				}
			}

			// Token: 0x04002513 RID: 9491
			public RoomObjectOption.RoomOptionTool[] m_DataID;
		}

		// Token: 0x02000617 RID: 1559
		public class OptionPakageKey
		{
			// Token: 0x06001E7A RID: 7802 RVA: 0x000A459C File Offset: 0x000A299C
			public int GetOptionKeyNum(RoomObjectOptionListDB pdata)
			{
				int num = 0;
				int count = this.m_TableID.Count;
				for (int i = 0; i < count; i++)
				{
					if (pdata.m_Params[this.m_TableID[i]].m_AttachType != 0)
					{
						if (pdata.m_Params[this.m_TableID[i]].m_AttachType == 1)
						{
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 1) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 2) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 4) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 8) != 0)
							{
								num++;
							}
						}
						else
						{
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 1) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 2) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 4) != 0)
							{
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 8) != 0)
							{
								num++;
							}
						}
					}
				}
				return num;
			}

			// Token: 0x06001E7B RID: 7803 RVA: 0x000A474C File Offset: 0x000A2B4C
			public void MakeOptionKeyData(RoomObjectOptionListDB pdata, ref RoomObjectOption.RoomOptionTool pout)
			{
				int num = 0;
				int count = this.m_TableID.Count;
				for (int i = 0; i < count; i++)
				{
					if (pdata.m_Params[this.m_TableID[i]].m_AttachType != 0)
					{
						if (pdata.m_Params[this.m_TableID[i]].m_AttachType == 1)
						{
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 1) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID0;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX0;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY0;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 2) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID1;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX1;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY1;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 4) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID2;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX2;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY2;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 8) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID3;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX3;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY3;
								num++;
							}
						}
						else
						{
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 1) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID0;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX0;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY0;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 2) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID1;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX1;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY1;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 4) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID2;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX2;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY2;
								num++;
							}
							if ((pdata.m_Params[this.m_TableID[i]].m_ActiveNum & 8) != 0)
							{
								pout.m_Table[num].m_Key = (byte)pdata.m_Params[this.m_TableID[i]].m_AttachType;
								pout.m_Table[num].m_MakeID = pdata.m_Params[this.m_TableID[i]].m_MakeID3;
								pout.m_Table[num].m_PosX = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetX3;
								pout.m_Table[num].m_PosY = pdata.m_Params[this.m_TableID[i]].m_ObjOffsetY3;
								num++;
							}
						}
					}
				}
			}

			// Token: 0x04002514 RID: 9492
			public uint m_GroupID;

			// Token: 0x04002515 RID: 9493
			public List<int> m_TableID = new List<int>();
		}

		// Token: 0x02000618 RID: 1560
		public class OptionPakage
		{
			// Token: 0x06001E7D RID: 7805 RVA: 0x000A4ED4 File Offset: 0x000A32D4
			public RoomObjectOption.OptionPakageKey GetKey(uint fkey)
			{
				RoomObjectOption.OptionPakageKey optionPakageKey = null;
				int count = this.m_List.Count;
				for (int i = 0; i < count; i++)
				{
					if (this.m_List[i].m_GroupID == fkey)
					{
						optionPakageKey = this.m_List[i];
						break;
					}
				}
				if (optionPakageKey == null)
				{
					optionPakageKey = new RoomObjectOption.OptionPakageKey();
					optionPakageKey.m_GroupID = fkey;
					this.m_List.Add(optionPakageKey);
				}
				return optionPakageKey;
			}

			// Token: 0x06001E7E RID: 7806 RVA: 0x000A4F4A File Offset: 0x000A334A
			public int GetKeyNum()
			{
				return this.m_List.Count;
			}

			// Token: 0x06001E7F RID: 7807 RVA: 0x000A4F57 File Offset: 0x000A3357
			public RoomObjectOption.OptionPakageKey GetIndexToKey(int findex)
			{
				return this.m_List[findex];
			}

			// Token: 0x04002516 RID: 9494
			public List<RoomObjectOption.OptionPakageKey> m_List = new List<RoomObjectOption.OptionPakageKey>();
		}
	}
}
