﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x02000413 RID: 1043
	public class EditState_Upgrade : EditState
	{
		// Token: 0x060013F4 RID: 5108 RVA: 0x0006A4EF File Offset: 0x000688EF
		public EditState_Upgrade(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013F5 RID: 5109 RVA: 0x0006A507 File Offset: 0x00068907
		public override int GetStateID()
		{
			return 9;
		}

		// Token: 0x060013F6 RID: 5110 RVA: 0x0006A50B File Offset: 0x0006890B
		public override void OnStateEnter()
		{
			this.m_Step = EditState_Upgrade.eStep.First;
		}

		// Token: 0x060013F7 RID: 5111 RVA: 0x0006A514 File Offset: 0x00068914
		public override void OnStateExit()
		{
		}

		// Token: 0x060013F8 RID: 5112 RVA: 0x0006A516 File Offset: 0x00068916
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060013F9 RID: 5113 RVA: 0x0006A520 File Offset: 0x00068920
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_Upgrade.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_Upgrade.eStep.LoadWait;
				break;
			case EditState_Upgrade.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_Upgrade.eStep.PlayIn;
				}
				break;
			case EditState_Upgrade.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, true);
					SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.OpenTipsWindowIfNotOccurred(eTutorialTipsListDB.FIRST_CHARA_UPGRADE, null, null);
					this.m_Step = EditState_Upgrade.eStep.Main;
				}
				break;
			case EditState_Upgrade.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton != CharaEditSceneUIBase.eButton.Decide)
						{
							this.m_NextState = 8;
						}
						else
						{
							this.m_NextState = 10;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_Upgrade.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_Upgrade.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_Upgrade.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060013FA RID: 5114 RVA: 0x0006A684 File Offset: 0x00068A84
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<UpgradeUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID);
			this.m_UI.OnClickButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Upgrade);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060013FB RID: 5115 RVA: 0x0006A732 File Offset: 0x00068B32
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060013FC RID: 5116 RVA: 0x0006A744 File Offset: 0x00068B44
		private void OnClickButton()
		{
			CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
			if (selectButton == CharaEditSceneUIBase.eButton.Decide)
			{
				this.Request_Upgrade(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID, this.m_UI.GetSelectItemInfos(), new MeigewwwParam.Callback(this.OnResponse_Upgrade));
			}
		}

		// Token: 0x060013FD RID: 5117 RVA: 0x0006A799 File Offset: 0x00068B99
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060013FE RID: 5118 RVA: 0x0006A7B8 File Offset: 0x00068BB8
		public void Request_Upgrade(long charaMngID, SelectItemPanel.UseItemInfos selectItem, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Characterupgrade characterupgrade = new PlayerRequestTypes.Characterupgrade();
			List<int> list = new List<int>();
			List<int> list2 = new List<int>();
			for (int i = 0; i < selectItem.HowManyAdapters(); i++)
			{
				if (selectItem.GetId(i) != -1)
				{
					list.Add(selectItem.GetId(i));
					list2.Add(selectItem.GetNum(i));
				}
			}
			characterupgrade.managedCharacterId = charaMngID;
			characterupgrade.itemId = APIUtility.ArrayToStr<int>(list.ToArray());
			characterupgrade.amount = APIUtility.ArrayToStr<int>(list2.ToArray());
			MeigewwwParam wwwParam = PlayerRequest.Characterupgrade(characterupgrade, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x060013FF RID: 5119 RVA: 0x0006A860 File Offset: 0x00068C60
		private void OnResponse_Upgrade(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Characterupgrade characterupgrade = PlayerResponse.Characterupgrade(wwwParam, ResponseCommon.DialogType.None, null);
			if (characterupgrade == null)
			{
				return;
			}
			ResultCode result = characterupgrade.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.CHARACTER_UPGRADE_LIMIT && result != ResultCode.GOLD_IS_SHORT)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(characterupgrade.managedCharacter.managedCharacterId);
				EditMain.UpgradeResultData upgradeResultData = new EditMain.UpgradeResultData();
				upgradeResultData.m_CharaMngID = characterupgrade.managedCharacter.managedCharacterId;
				upgradeResultData.m_SuccessType = characterupgrade.successType;
				upgradeResultData.m_BeforeLv = userCharaData.Param.Lv;
				upgradeResultData.m_BeforeExp = userCharaData.Param.Exp;
				APIUtility.wwwToUserData(characterupgrade.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(characterupgrade.managedCharacter, userCharaData);
				userDataMng.UserData.Gold = characterupgrade.gold;
				upgradeResultData.m_AfterLv = userCharaData.Param.Lv;
				upgradeResultData.m_AfterExp = userCharaData.Param.Exp;
				this.m_Owner.UpgradeResult = upgradeResultData;
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, false);
				MissionManager.UnlockAction(eXlsMissionSeg.Edit, eXlsMissionEditFuncType.CharaStorength);
				this.GoToMenuEnd();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
			}
		}

		// Token: 0x04001AD1 RID: 6865
		private EditState_Upgrade.eStep m_Step = EditState_Upgrade.eStep.None;

		// Token: 0x04001AD2 RID: 6866
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.UpgradeUI;

		// Token: 0x04001AD3 RID: 6867
		public UpgradeUI m_UI;

		// Token: 0x02000414 RID: 1044
		private enum eStep
		{
			// Token: 0x04001AD5 RID: 6869
			None = -1,
			// Token: 0x04001AD6 RID: 6870
			First,
			// Token: 0x04001AD7 RID: 6871
			LoadWait,
			// Token: 0x04001AD8 RID: 6872
			PlayIn,
			// Token: 0x04001AD9 RID: 6873
			Main,
			// Token: 0x04001ADA RID: 6874
			UnloadChildSceneWait
		}
	}
}
