﻿using System;

namespace Star
{
	// Token: 0x020001A0 RID: 416
	public static class QuestWaveDropItemDB_Ext
	{
		// Token: 0x06000B0B RID: 2827 RVA: 0x00041C70 File Offset: 0x00040070
		public static QuestWaveDropItemDB_Param GetParam(this QuestWaveDropItemDB self, int dropID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == dropID)
				{
					return self.m_Params[i];
				}
			}
			return default(QuestWaveDropItemDB_Param);
		}
	}
}
