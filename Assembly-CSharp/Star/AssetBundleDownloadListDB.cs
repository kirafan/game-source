﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000164 RID: 356
	public class AssetBundleDownloadListDB : ScriptableObject
	{
		// Token: 0x0400096C RID: 2412
		public AssetBundleDownloadListDB_Param[] m_Params;
	}
}
