﻿using System;

namespace Star
{
	// Token: 0x02000227 RID: 551
	public enum eClassType
	{
		// Token: 0x04000F0A RID: 3850
		None = -1,
		// Token: 0x04000F0B RID: 3851
		Fighter,
		// Token: 0x04000F0C RID: 3852
		Magician,
		// Token: 0x04000F0D RID: 3853
		Priest,
		// Token: 0x04000F0E RID: 3854
		Knight,
		// Token: 0x04000F0F RID: 3855
		Alchemist,
		// Token: 0x04000F10 RID: 3856
		Num
	}
}
