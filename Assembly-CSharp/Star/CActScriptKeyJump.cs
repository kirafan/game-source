﻿using System;

namespace Star
{
	// Token: 0x0200054E RID: 1358
	public class CActScriptKeyJump : IRoomScriptData
	{
		// Token: 0x06001ACD RID: 6861 RVA: 0x0008EE88 File Offset: 0x0008D288
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyJump actXlsKeyJump = (ActXlsKeyJump)pbase;
			this.m_Randam = actXlsKeyJump.m_Randam;
			this.m_Offset = actXlsKeyJump.m_Offset;
			this.m_FrameReset = actXlsKeyJump.m_FrameReset;
		}

		// Token: 0x04002198 RID: 8600
		public int m_Randam;

		// Token: 0x04002199 RID: 8601
		public int m_Offset;

		// Token: 0x0400219A RID: 8602
		public bool m_FrameReset;
	}
}
