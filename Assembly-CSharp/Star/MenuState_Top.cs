﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000447 RID: 1095
	public class MenuState_Top : MenuState
	{
		// Token: 0x06001515 RID: 5397 RVA: 0x0006E8F4 File Offset: 0x0006CCF4
		public MenuState_Top(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x06001516 RID: 5398 RVA: 0x0006E90C File Offset: 0x0006CD0C
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001517 RID: 5399 RVA: 0x0006E90F File Offset: 0x0006CD0F
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_Top.eStep.First;
		}

		// Token: 0x06001518 RID: 5400 RVA: 0x0006E918 File Offset: 0x0006CD18
		public override void OnStateExit()
		{
		}

		// Token: 0x06001519 RID: 5401 RVA: 0x0006E91A File Offset: 0x0006CD1A
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600151A RID: 5402 RVA: 0x0006E924 File Offset: 0x0006CD24
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_Top.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_Top.eStep.LoadWait;
				break;
			case MenuState_Top.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_Top.eStep.PlayIn;
				}
				break;
			case MenuState_Top.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_Top.eStep.Main;
				}
				break;
			case MenuState_Top.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						switch (this.m_UI.SelectButton)
						{
						case MenuTopUI.eButton.UserInfomation:
							this.m_NextState = 2;
							goto IL_1A7;
						case MenuTopUI.eButton.Purchase:
							this.m_NextState = 16;
							goto IL_1A7;
						case MenuTopUI.eButton.Friend:
							this.m_NextState = 3;
							goto IL_1A7;
						case MenuTopUI.eButton.Help:
							this.m_NextState = 5;
							goto IL_1A7;
						case MenuTopUI.eButton.Option:
							this.m_NextState = 6;
							goto IL_1A7;
						case MenuTopUI.eButton.Notification:
							this.m_NextState = 7;
							goto IL_1A7;
						case MenuTopUI.eButton.Title:
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Title;
							this.m_NextState = 2147483646;
							goto IL_1A7;
						case MenuTopUI.eButton.PlayerMoveConfiguration:
							this.m_NextState = 15;
							goto IL_1A7;
						case MenuTopUI.eButton.Library:
							this.m_NextState = 4;
							goto IL_1A7;
						}
						this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						this.m_NextState = 2147483646;
					}
					IL_1A7:
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_Top.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_Top.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_Top.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600151B RID: 5403 RVA: 0x0006EB20 File Offset: 0x0006CF20
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuTopUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Home, eSceneInfo.Menu);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x0600151C RID: 5404 RVA: 0x0006EBB8 File Offset: 0x0006CFB8
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Menu)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
				this.GoToMenuEnd();
			}
		}

		// Token: 0x0600151D RID: 5405 RVA: 0x0006EC11 File Offset: 0x0006D011
		private void OnClickButton()
		{
		}

		// Token: 0x0600151E RID: 5406 RVA: 0x0006EC13 File Offset: 0x0006D013
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BFD RID: 7165
		private MenuState_Top.eStep m_Step = MenuState_Top.eStep.None;

		// Token: 0x04001BFE RID: 7166
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuTopUI;

		// Token: 0x04001BFF RID: 7167
		private MenuTopUI m_UI;

		// Token: 0x02000448 RID: 1096
		private enum eStep
		{
			// Token: 0x04001C01 RID: 7169
			None = -1,
			// Token: 0x04001C02 RID: 7170
			First,
			// Token: 0x04001C03 RID: 7171
			LoadWait,
			// Token: 0x04001C04 RID: 7172
			PlayIn,
			// Token: 0x04001C05 RID: 7173
			Main,
			// Token: 0x04001C06 RID: 7174
			UnloadChildSceneWait
		}
	}
}
