﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000200 RID: 512
	public class SkillContentAndEffectCombiDB : ScriptableObject
	{
		// Token: 0x04000CA6 RID: 3238
		public SkillContentAndEffectCombiDB_Param[] m_Params;
	}
}
