﻿using System;

namespace Star
{
	// Token: 0x020006B3 RID: 1715
	public class ITownPartsAction
	{
		// Token: 0x06002236 RID: 8758 RVA: 0x000B5B70 File Offset: 0x000B3F70
		public virtual bool PartsUpdate()
		{
			return false;
		}

		// Token: 0x06002237 RID: 8759 RVA: 0x000B5B73 File Offset: 0x000B3F73
		public virtual void Destory()
		{
		}

		// Token: 0x040028B7 RID: 10423
		public int m_UID;

		// Token: 0x040028B8 RID: 10424
		public bool m_Active;
	}
}
