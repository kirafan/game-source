﻿using System;
using System.IO;

namespace Star
{
	// Token: 0x02000338 RID: 824
	public class BinaryFileWrite : BinaryWrite
	{
		// Token: 0x06000FC3 RID: 4035 RVA: 0x00055860 File Offset: 0x00053C60
		public bool CreateFile(string filename)
		{
			this.m_File = new FileStream(filename, FileMode.Create, FileAccess.Write);
			return this.m_File != null;
		}

		// Token: 0x06000FC4 RID: 4036 RVA: 0x00055880 File Offset: 0x00053C80
		public void WriteFile()
		{
			int count = this.m_Table.Count;
			int num = 0;
			for (int i = 0; i < count; i++)
			{
				BinaryWrite.IWriteData writeData = this.m_Table[i];
				switch (writeData.m_Type)
				{
				case BinaryWrite.eData.Byte:
				{
					BinaryWrite.ByteWriteData byteWriteData = (BinaryWrite.ByteWriteData)writeData;
					this.m_File.Write(BitConverter.GetBytes((short)byteWriteData.m_Data), 0, 1);
					num++;
					break;
				}
				case BinaryWrite.eData.Short:
				{
					BinaryWrite.ShortWriteData shortWriteData = (BinaryWrite.ShortWriteData)writeData;
					this.m_File.Write(BitConverter.GetBytes(shortWriteData.m_Data), 0, 2);
					num += 2;
					break;
				}
				case BinaryWrite.eData.Int:
				{
					BinaryWrite.IntWriteData intWriteData = (BinaryWrite.IntWriteData)writeData;
					this.m_File.Write(BitConverter.GetBytes(intWriteData.m_Data), 0, 4);
					num += 4;
					break;
				}
				case BinaryWrite.eData.Long:
				{
					BinaryWrite.LongWriteData longWriteData = (BinaryWrite.LongWriteData)writeData;
					this.m_File.Write(BitConverter.GetBytes(longWriteData.m_Data), 0, 8);
					num += 8;
					break;
				}
				case BinaryWrite.eData.Float:
				{
					BinaryWrite.FloatWriteData floatWriteData = (BinaryWrite.FloatWriteData)writeData;
					this.m_File.Write(BitConverter.GetBytes(floatWriteData.m_Data), 0, 4);
					num += 4;
					break;
				}
				case BinaryWrite.eData.Table:
				{
					BinaryWrite.TableWriteData tableWriteData = (BinaryWrite.TableWriteData)writeData;
					this.m_File.Write(tableWriteData.m_Data, 0, tableWriteData.m_Data.Length);
					num += tableWriteData.m_Data.Length;
					break;
				}
				}
			}
		}

		// Token: 0x06000FC5 RID: 4037 RVA: 0x000559F1 File Offset: 0x00053DF1
		public void CloseFile()
		{
			this.m_File.Close();
			this.m_File = null;
		}

		// Token: 0x0400170A RID: 5898
		private FileStream m_File;
	}
}
