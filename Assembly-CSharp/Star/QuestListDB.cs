﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001E8 RID: 488
	public class QuestListDB : ScriptableObject
	{
		// Token: 0x04000BE1 RID: 3041
		public QuestListDB_Param[] m_Params;
	}
}
