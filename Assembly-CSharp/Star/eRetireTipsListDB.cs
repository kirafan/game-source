﻿using System;

namespace Star
{
	// Token: 0x020001EF RID: 495
	public enum eRetireTipsListDB
	{
		// Token: 0x04000BF6 RID: 3062
		Retire_000,
		// Token: 0x04000BF7 RID: 3063
		Retire_001,
		// Token: 0x04000BF8 RID: 3064
		Retire_002,
		// Token: 0x04000BF9 RID: 3065
		Retire_003,
		// Token: 0x04000BFA RID: 3066
		Retire_004,
		// Token: 0x04000BFB RID: 3067
		Retire_005,
		// Token: 0x04000BFC RID: 3068
		Retire_006,
		// Token: 0x04000BFD RID: 3069
		Max
	}
}
