﻿using System;

namespace Star
{
	// Token: 0x02000319 RID: 793
	public class FieldBuildCmdLevelUp : FieldBuilCmdBase
	{
		// Token: 0x06000F12 RID: 3858 RVA: 0x00050BC0 File Offset: 0x0004EFC0
		public FieldBuildCmdLevelUp(long fmanageid)
		{
			this.m_ManageID = fmanageid;
			this.m_Cmd = FieldBuilCmdBase.eCmd.LevelUp;
		}

		// Token: 0x0400169A RID: 5786
		public long m_ManageID;
	}
}
