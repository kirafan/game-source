﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200050D RID: 1293
	public class IMissionServerApi : MonoBehaviour
	{
		// Token: 0x06001955 RID: 6485 RVA: 0x0008396D File Offset: 0x00081D6D
		public static IMissionServerApi CreateServerApi()
		{
			return MissionServerApi.CreateServer();
		}

		// Token: 0x06001956 RID: 6486 RVA: 0x00083974 File Offset: 0x00081D74
		public static void DestroyServerApi()
		{
			MissionServerApi.DestroyServer();
		}

		// Token: 0x06001957 RID: 6487 RVA: 0x0008397B File Offset: 0x00081D7B
		public virtual bool IsAvailable()
		{
			return true;
		}

		// Token: 0x06001958 RID: 6488 RVA: 0x0008397E File Offset: 0x00081D7E
		public virtual List<IMissionPakage> GetMissionList()
		{
			return null;
		}

		// Token: 0x06001959 RID: 6489 RVA: 0x00083981 File Offset: 0x00081D81
		public virtual void StarApi()
		{
		}

		// Token: 0x0600195A RID: 6490 RVA: 0x00083983 File Offset: 0x00081D83
		public virtual int GetPreCompNum()
		{
			return 0;
		}

		// Token: 0x0600195B RID: 6491 RVA: 0x00083986 File Offset: 0x00081D86
		public virtual bool IsListUp()
		{
			return false;
		}

		// Token: 0x0600195C RID: 6492 RVA: 0x00083989 File Offset: 0x00081D89
		public virtual bool IsPreComp()
		{
			return false;
		}

		// Token: 0x0600195D RID: 6493 RVA: 0x0008398C File Offset: 0x00081D8C
		public virtual IMissionPakage GetPreCompMission(PopupMaskFunc fmask)
		{
			return null;
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x0008398F File Offset: 0x00081D8F
		public virtual void UnlockAction(eXlsMissionSeg fcategory, int factionno)
		{
		}

		// Token: 0x0600195F RID: 6495 RVA: 0x00083991 File Offset: 0x00081D91
		public virtual void CompleteMission(long fcompmangeid)
		{
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x00083993 File Offset: 0x00081D93
		public virtual void EntryRefreshCallback(RefreshCallback pcallback, int fkeyid)
		{
		}

		// Token: 0x06001961 RID: 6497 RVA: 0x00083995 File Offset: 0x00081D95
		public virtual void ReleaseRefreshCallback(int fkeyid)
		{
		}

		// Token: 0x06001962 RID: 6498 RVA: 0x00083997 File Offset: 0x00081D97
		public virtual void RebuildMissionList()
		{
		}
	}
}
