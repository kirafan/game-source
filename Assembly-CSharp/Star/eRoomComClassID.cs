﻿using System;

namespace Star
{
	// Token: 0x02000469 RID: 1129
	public enum eRoomComClassID
	{
		// Token: 0x04001CA8 RID: 7336
		OpenUI = 2,
		// Token: 0x04001CA9 RID: 7337
		ActionPopup,
		// Token: 0x04001CAA RID: 7338
		ModelChange,
		// Token: 0x04001CAB RID: 7339
		MoveEditMode
	}
}
