﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000129 RID: 297
	public class SkillActionMovementPenetrate : SkillActionMovementBase
	{
		// Token: 0x060007AD RID: 1965 RVA: 0x0002F0C0 File Offset: 0x0002D4C0
		public SkillActionMovementPenetrate(Transform moveObj, bool isLocalPos, Vector3 startPos, List<Vector3> targetPosList, float speed, float delaySec, float aliveSec) : base(moveObj, isLocalPos)
		{
			this.m_StartPos = startPos;
			this.m_TargetPosList = targetPosList;
			this.m_Speed = speed;
			this.m_DelaySec = delaySec;
			this.m_AliveSec = aliveSec;
			if (this.m_TargetPosList[0].x < startPos.x)
			{
				this.m_vDir = new Vector3(-1f, 0f, 0f);
			}
			else
			{
				this.m_vDir = new Vector3(1f, 0f, 0f);
			}
			this.m_vDir.Normalize();
			this.m_IsArrivals = new List<bool>();
			for (int i = 0; i < this.m_TargetPosList.Count; i++)
			{
				this.m_IsArrivals.Add(false);
			}
			if (isLocalPos)
			{
				this.m_MoveObj.localPosition = this.m_StartPos;
			}
			else
			{
				this.m_MoveObj.position = this.m_StartPos;
			}
		}

		// Token: 0x060007AE RID: 1966 RVA: 0x0002F1CC File Offset: 0x0002D5CC
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			out_arrivalIndex = -1;
			out_arrivalPos = Vector3.zero;
			if (!this.m_IsMoving)
			{
				return false;
			}
			this.m_DelaySec -= Time.deltaTime;
			if (this.m_DelaySec > 0f)
			{
				return true;
			}
			if (this.m_t < this.m_AliveSec)
			{
				this.m_t += Time.deltaTime;
				Vector3 vector = Vector3.zero;
				Vector3 b = this.m_vDir * this.m_Speed * Time.deltaTime;
				if (this.m_IsLocalPos)
				{
					this.m_MoveObj.localPosition += b;
					vector = this.m_MoveObj.localPosition;
				}
				else
				{
					this.m_MoveObj.position += b;
					vector = this.m_MoveObj.position;
				}
				for (int i = 0; i < this.m_TargetPosList.Count; i++)
				{
					if (!this.m_IsArrivals[i])
					{
						if (this.m_vDir.x < 0f)
						{
							if (vector.x <= this.m_TargetPosList[i].x)
							{
								out_arrivalIndex = i;
								out_arrivalPos = this.m_TargetPosList[i];
								this.m_IsArrivals[i] = true;
								break;
							}
						}
						else if (vector.x >= this.m_TargetPosList[i].x)
						{
							out_arrivalIndex = i;
							out_arrivalPos = this.m_TargetPosList[i];
							this.m_IsArrivals[i] = true;
							break;
						}
					}
				}
			}
			else
			{
				this.m_IsMoving = false;
			}
			return this.m_IsMoving;
		}

		// Token: 0x04000779 RID: 1913
		private Vector3 m_StartPos;

		// Token: 0x0400077A RID: 1914
		private List<Vector3> m_TargetPosList;

		// Token: 0x0400077B RID: 1915
		private float m_Speed;

		// Token: 0x0400077C RID: 1916
		private float m_DelaySec;

		// Token: 0x0400077D RID: 1917
		private float m_AliveSec;

		// Token: 0x0400077E RID: 1918
		private Vector3 m_vDir = Vector3.zero;

		// Token: 0x0400077F RID: 1919
		private List<bool> m_IsArrivals;

		// Token: 0x04000780 RID: 1920
		private float m_t;
	}
}
