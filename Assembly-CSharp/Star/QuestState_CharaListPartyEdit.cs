﻿using System;
using Star.UI.Edit;

namespace Star
{
	// Token: 0x02000459 RID: 1113
	public class QuestState_CharaListPartyEdit : QuestState
	{
		// Token: 0x0600158F RID: 5519 RVA: 0x000702B1 File Offset: 0x0006E6B1
		public QuestState_CharaListPartyEdit(QuestMain owner) : base(owner)
		{
		}

		// Token: 0x06001590 RID: 5520 RVA: 0x000702C9 File Offset: 0x0006E6C9
		public override int GetStateID()
		{
			return 6;
		}

		// Token: 0x06001591 RID: 5521 RVA: 0x000702CC File Offset: 0x0006E6CC
		public override void OnStateEnter()
		{
			this.m_Step = QuestState_CharaListPartyEdit.eStep.First;
		}

		// Token: 0x06001592 RID: 5522 RVA: 0x000702D5 File Offset: 0x0006E6D5
		public override void OnStateExit()
		{
		}

		// Token: 0x06001593 RID: 5523 RVA: 0x000702D7 File Offset: 0x0006E6D7
		public override void OnDispose()
		{
		}

		// Token: 0x06001594 RID: 5524 RVA: 0x000702DC File Offset: 0x0006E6DC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case QuestState_CharaListPartyEdit.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = QuestState_CharaListPartyEdit.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = QuestState_CharaListPartyEdit.eStep.LoadWait;
				}
				break;
			case QuestState_CharaListPartyEdit.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = QuestState_CharaListPartyEdit.eStep.PlayIn;
				}
				break;
			case QuestState_CharaListPartyEdit.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = QuestState_CharaListPartyEdit.eStep.Main;
				}
				break;
			case QuestState_CharaListPartyEdit.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
						if (selectButton != CharaListUI.eButton.Remove)
						{
							if (selectButton == CharaListUI.eButton.Chara)
							{
								EditUtility.AssignPartyMember(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex, this.m_Owner.CharaListUI.SelectCharaMngID);
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
							}
						}
						else
						{
							EditUtility.RemovePartyMember(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex);
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.IsDirtyPartyEdit = true;
						}
						this.m_NextState = 5;
					}
					this.m_Step = QuestState_CharaListPartyEdit.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001595 RID: 5525 RVA: 0x0007048C File Offset: 0x0006E88C
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.m_Owner.CharaListUI.SetPartyEditMode(LocalSaveData.Inst.SelectedPartyIndex, SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedPartyMemberIndex);
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x06001596 RID: 5526 RVA: 0x00070540 File Offset: 0x0006E940
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (isCallFromShortCut && globalParam.IsDirtyPartyEdit)
			{
				this.m_Owner.Request_BattlePartySetAll(new Action(this.GoToMenuEnd));
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001597 RID: 5527 RVA: 0x00070592 File Offset: 0x0006E992
		private void GoToMenuEnd()
		{
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x04001C52 RID: 7250
		private QuestState_CharaListPartyEdit.eStep m_Step = QuestState_CharaListPartyEdit.eStep.None;

		// Token: 0x04001C53 RID: 7251
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x0200045A RID: 1114
		private enum eStep
		{
			// Token: 0x04001C55 RID: 7253
			None = -1,
			// Token: 0x04001C56 RID: 7254
			First,
			// Token: 0x04001C57 RID: 7255
			LoadWait,
			// Token: 0x04001C58 RID: 7256
			PlayIn,
			// Token: 0x04001C59 RID: 7257
			Main
		}
	}
}
