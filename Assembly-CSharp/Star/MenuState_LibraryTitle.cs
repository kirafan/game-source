﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x0200043B RID: 1083
	public class MenuState_LibraryTitle : MenuState
	{
		// Token: 0x060014CC RID: 5324 RVA: 0x0006D788 File Offset: 0x0006BB88
		public MenuState_LibraryTitle(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014CD RID: 5325 RVA: 0x0006D7A0 File Offset: 0x0006BBA0
		public override int GetStateID()
		{
			return 12;
		}

		// Token: 0x060014CE RID: 5326 RVA: 0x0006D7A4 File Offset: 0x0006BBA4
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_LibraryTitle.eStep.First;
		}

		// Token: 0x060014CF RID: 5327 RVA: 0x0006D7AD File Offset: 0x0006BBAD
		public override void OnStateExit()
		{
		}

		// Token: 0x060014D0 RID: 5328 RVA: 0x0006D7AF File Offset: 0x0006BBAF
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014D1 RID: 5329 RVA: 0x0006D7B8 File Offset: 0x0006BBB8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_LibraryTitle.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_LibraryTitle.eStep.LoadWait;
				break;
			case MenuState_LibraryTitle.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryTitle.eStep.PlayIn;
				}
				break;
			case MenuState_LibraryTitle.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_LibraryTitle.eStep.Main;
				}
				break;
			case MenuState_LibraryTitle.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 4;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryTitle.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_LibraryTitle.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_LibraryTitle.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014D2 RID: 5330 RVA: 0x0006D8D8 File Offset: 0x0006BCD8
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuLibraryWordUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(this.m_Owner.LibraryStartMode);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			if (this.m_Owner.LibraryStartMode == MenuLibraryWordUI.eMode.ContentTitle)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LibraryTitle);
			}
			else
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.LibraryWord);
			}
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014D3 RID: 5331 RVA: 0x0006D9A1 File Offset: 0x0006BDA1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
		}

		// Token: 0x060014D4 RID: 5332 RVA: 0x0006D9BF File Offset: 0x0006BDBF
		private void OnClickButton()
		{
		}

		// Token: 0x060014D5 RID: 5333 RVA: 0x0006D9C1 File Offset: 0x0006BDC1
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BB8 RID: 7096
		private MenuState_LibraryTitle.eStep m_Step = MenuState_LibraryTitle.eStep.None;

		// Token: 0x04001BB9 RID: 7097
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuLibraryWordUI;

		// Token: 0x04001BBA RID: 7098
		private MenuLibraryWordUI m_UI;

		// Token: 0x0200043C RID: 1084
		private enum eStep
		{
			// Token: 0x04001BBC RID: 7100
			None = -1,
			// Token: 0x04001BBD RID: 7101
			First,
			// Token: 0x04001BBE RID: 7102
			LoadWait,
			// Token: 0x04001BBF RID: 7103
			PlayIn,
			// Token: 0x04001BC0 RID: 7104
			Main,
			// Token: 0x04001BC1 RID: 7105
			UnloadChildSceneWait
		}
	}
}
