﻿using System;

namespace Star
{
	// Token: 0x020006CE RID: 1742
	public class TownPartsPreModelLoad : ITownPartsAction
	{
		// Token: 0x06002281 RID: 8833 RVA: 0x000B8204 File Offset: 0x000B6604
		public override bool PartsUpdate()
		{
			return this.m_Active;
		}
	}
}
