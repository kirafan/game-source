﻿using System;

namespace Star
{
	// Token: 0x02000391 RID: 913
	public class IDataBaseResource : IObjectResourceHandle
	{
		// Token: 0x0600113D RID: 4413 RVA: 0x0005A4ED File Offset: 0x000588ED
		public void LoadResource(string filename, string extname)
		{
			ObjectResourceManager.EntryResQue(this, filename, extname);
		}

		// Token: 0x0600113E RID: 4414 RVA: 0x0005A4F7 File Offset: 0x000588F7
		public void LoadResInManager(string filename, string extname)
		{
			ObjectResourceManager.EntryResMngQue(this, filename, extname);
		}
	}
}
