﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020003CC RID: 972
	public class GachaObjectHandler : MonoBehaviour
	{
		// Token: 0x0600126D RID: 4717 RVA: 0x00061EA5 File Offset: 0x000602A5
		private void Update()
		{
			this.UpdateFade();
		}

		// Token: 0x0600126E RID: 4718 RVA: 0x00061EB0 File Offset: 0x000602B0
		public void Setup(string resourceName, MeigeAnimClipHolder[] meigeAnimClipHolders)
		{
			string name = CharacterUtility.ConvertMeigeFbx2PrefabObjPath(resourceName, false);
			this.m_BaseTransform = base.transform.Find(name);
			if (this.m_BaseTransform == null)
			{
				return;
			}
			this.m_Anim = this.m_BaseTransform.gameObject.GetComponent<Animation>();
			this.m_Anim.cullingType = AnimationCullingType.AlwaysAnimate;
			this.m_MeigeAnimCtrl = this.m_BaseTransform.gameObject.AddComponent<MeigeAnimCtrl>();
			this.m_MsbHndl = this.m_BaseTransform.gameObject.GetComponent<MsbHandler>();
			this.m_MeigeAnimCtrl.Open();
			for (int i = 0; i < meigeAnimClipHolders.Length; i++)
			{
				this.m_MeigeAnimCtrl.AddClip(this.m_MsbHndl, meigeAnimClipHolders[i]);
			}
			this.m_MeigeAnimCtrl.Close();
			this.m_MeigeAnimEvNotify = this.m_BaseTransform.gameObject.GetComponent<MeigeAnimEventNotifier>();
			if (this.m_MeigeAnimEvNotify != null)
			{
				this.m_MeigeAnimEvNotify.AddNotify(new MeigeAnimEventNotifier.OnEvent(this.OnMeigeAnimEvent));
			}
		}

		// Token: 0x0600126F RID: 4719 RVA: 0x00061FB4 File Offset: 0x000603B4
		public void Destroy()
		{
			this.DetachCamera();
			this.m_BaseTransform = null;
			this.m_MeigeAnimCtrl = null;
			if (this.m_MeigeAnimEvNotify != null)
			{
				this.m_MeigeAnimEvNotify.ClearNotify();
				this.m_MeigeAnimEvNotify = null;
			}
			this.m_MsbHndl = null;
		}

		// Token: 0x06001270 RID: 4720 RVA: 0x00061FF4 File Offset: 0x000603F4
		public void SetMagicCircle(eClassType classType)
		{
			for (eClassType eClassType = eClassType.Fighter; eClassType < eClassType.Num; eClassType++)
			{
				GameObject[] magicCircleObject = this.GetMagicCircleObject(eClassType);
				if (magicCircleObject != null)
				{
					if (eClassType == classType)
					{
						for (int i = 0; i < magicCircleObject.Length; i++)
						{
							magicCircleObject[i].SetActive(true);
						}
					}
					else
					{
						for (int j = 0; j < magicCircleObject.Length; j++)
						{
							magicCircleObject[j].SetActive(false);
						}
					}
				}
			}
		}

		// Token: 0x06001271 RID: 4721 RVA: 0x00062068 File Offset: 0x00060468
		private GameObject[] GetMagicCircleObject(eClassType classType)
		{
			int length = GachaObjectHandler.MAGIC_CIRCLE_PATHS.GetLength(1);
			GameObject[] array = new GameObject[length];
			for (int i = 0; i < length; i++)
			{
				Transform transform = this.m_BaseTransform.Find(GachaObjectHandler.MAGIC_CIRCLE_PATHS[(int)classType, i]);
				if (transform != null)
				{
					array[i] = transform.gameObject;
				}
			}
			return array;
		}

		// Token: 0x06001272 RID: 4722 RVA: 0x000620C8 File Offset: 0x000604C8
		public void AttachCamera(Camera camera)
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.AttachCamera(0, camera);
		}

		// Token: 0x06001273 RID: 4723 RVA: 0x000620E9 File Offset: 0x000604E9
		public void DetachCamera()
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			this.m_MsbHndl.DetachCamera();
		}

		// Token: 0x06001274 RID: 4724 RVA: 0x00062108 File Offset: 0x00060508
		public void ResetCamera()
		{
			MsbCameraHandler msbCameraHandler = this.m_MsbHndl.GetMsbCameraHandler(0);
			if (msbCameraHandler != null)
			{
				msbCameraHandler.GetWork().m_OrthographicsSize = msbCameraHandler.m_Src.m_OrthographicsSize;
			}
		}

		// Token: 0x06001275 RID: 4725 RVA: 0x00062140 File Offset: 0x00060540
		public void Play(GachaObjectHandler.eAnimType animType, float blendSec = 0f)
		{
			if (this.m_MsbHndl == null)
			{
				return;
			}
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			WrapMode wrap = WrapMode.ClampForever;
			switch (animType)
			{
			case GachaObjectHandler.eAnimType.Branch_lv1_loop:
			case GachaObjectHandler.eAnimType.Branch_lv2_loop:
			case GachaObjectHandler.eAnimType.Branch_lv3_cutIn_loop:
			case GachaObjectHandler.eAnimType.Branch_lv3_loop:
				wrap = WrapMode.Loop;
				break;
			}
			this.m_MeigeAnimCtrl.Play(GachaObjectHandler.ANIM_KEYS[(int)animType], blendSec, wrap);
		}

		// Token: 0x06001276 RID: 4726 RVA: 0x000621B3 File Offset: 0x000605B3
		public void Pause()
		{
			if (this.m_MeigeAnimCtrl == null)
			{
				return;
			}
			this.m_MeigeAnimCtrl.Pause();
		}

		// Token: 0x06001277 RID: 4727 RVA: 0x000621D4 File Offset: 0x000605D4
		public bool IsPlaying()
		{
			WrapMode wrapMode = this.m_MeigeAnimCtrl.m_WrapMode;
			if (wrapMode != WrapMode.ClampForever)
			{
				if (this.m_MeigeAnimCtrl.GetPlayState() == MeigeAnimCtrl.ePlayState.ePlayState_Play)
				{
					return true;
				}
			}
			else if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime < 1f)
			{
				return true;
			}
			return false;
		}

		// Token: 0x06001278 RID: 4728 RVA: 0x0006222D File Offset: 0x0006062D
		public void ResetMeshVisibility()
		{
			this.m_MsbHndl.SetDefaultVisibility();
		}

		// Token: 0x06001279 RID: 4729 RVA: 0x0006223C File Offset: 0x0006063C
		public void KillParticle()
		{
			int particleEmitterNum = this.m_MsbHndl.GetParticleEmitterNum();
			for (int i = 0; i < particleEmitterNum; i++)
			{
				this.m_MsbHndl.GetParticleEmitter(i).Kill();
			}
		}

		// Token: 0x0600127A RID: 4730 RVA: 0x0006227C File Offset: 0x0006067C
		public void Fade(float startAlpha, float endAlpha, float sec)
		{
			this.m_StartAlpha = startAlpha;
			this.m_EndAlpha = endAlpha;
			this.m_FadeTime = sec;
			this.m_FadeTimer = 0f;
			this.SetMeshAlpha(this.m_StartAlpha);
			if (this.m_FadeTime <= 0f)
			{
				this.m_IsUpdateMeshColor = false;
			}
			else
			{
				this.m_IsUpdateMeshColor = true;
			}
		}

		// Token: 0x0600127B RID: 4731 RVA: 0x000622D8 File Offset: 0x000606D8
		private void UpdateFade()
		{
			if (!this.m_IsUpdateMeshColor)
			{
				return;
			}
			this.m_FadeTimer += Time.deltaTime;
			float num = this.m_FadeTimer / this.m_FadeTime;
			if (num > 1f)
			{
				num = 1f;
			}
			float meshAlpha = Mathf.Lerp(this.m_StartAlpha, this.m_EndAlpha, num);
			this.SetMeshAlpha(meshAlpha);
		}

		// Token: 0x0600127C RID: 4732 RVA: 0x0006233C File Offset: 0x0006073C
		public void ResetMeshColor()
		{
			this.m_IsUpdateMeshColor = false;
			this.SetMeshColor(Color.white);
		}

		// Token: 0x0600127D RID: 4733 RVA: 0x00062350 File Offset: 0x00060750
		private void SetMeshColor(Color color)
		{
			if (this.m_MsbHndl != null)
			{
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				for (int i = 0; i < msbObjectHandlerNum; i++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
					if (msbObjectHandler.m_Name != "gacha_com_bg_rgb")
					{
						msbObjectHandler.GetWork().m_MeshColor = color;
					}
				}
			}
		}

		// Token: 0x0600127E RID: 4734 RVA: 0x000623BC File Offset: 0x000607BC
		private void SetMeshAlpha(float a)
		{
			if (this.m_MsbHndl != null)
			{
				int msbObjectHandlerNum = this.m_MsbHndl.GetMsbObjectHandlerNum();
				for (int i = 0; i < msbObjectHandlerNum; i++)
				{
					MsbObjectHandler msbObjectHandler = this.m_MsbHndl.GetMsbObjectHandler(i);
					if (msbObjectHandler.m_Name != "gacha_com_bg_rgb")
					{
						Color meshColor = msbObjectHandler.GetWork().m_MeshColor;
						meshColor.a *= a;
						msbObjectHandler.GetWork().m_MeshColor = meshColor;
					}
				}
			}
		}

		// Token: 0x0600127F RID: 4735 RVA: 0x00062444 File Offset: 0x00060844
		public void OnMeigeAnimEvent(MabHandler.MabAnimEvent ev)
		{
			GachaObjectHandler.eAnimEvent eAnimEvent = (GachaObjectHandler.eAnimEvent)ev.m_iParam[0];
			if (eAnimEvent == GachaObjectHandler.eAnimEvent.FadeOut)
			{
				float time = (float)ev.m_iParam[1] * (1f / (float)Application.targetFrameRate);
				SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(Color.white, time, null);
			}
		}

		// Token: 0x06001280 RID: 4736 RVA: 0x000624A0 File Offset: 0x000608A0
		public float GetCurrentAnimationEndTime(GachaObjectHandler.eAnimType animType)
		{
			MeigeAnimCtrl.ReferenceAnimHandler referenceAnimHandler = this.m_MeigeAnimCtrl.GetReferenceAnimHandler(GachaObjectHandler.ANIM_KEYS[(int)animType]);
			if (referenceAnimHandler != null)
			{
				return referenceAnimHandler.m_AnimationTime;
			}
			return 0f;
		}

		// Token: 0x06001281 RID: 4737 RVA: 0x000624D2 File Offset: 0x000608D2
		public float GetCurrentAnimationNowTime()
		{
			return this.m_MeigeAnimCtrl.m_AnimationPlayTime;
		}

		// Token: 0x06001282 RID: 4738 RVA: 0x000624E0 File Offset: 0x000608E0
		// Note: this type is marked as 'beforefieldinit'.
		static GachaObjectHandler()
		{
			string[,] array = new string[5, 3];
			array[0, 0] = "loc_gacha_lv1_item_Fighter";
			array[0, 1] = "loc_gacha_lv2_item_Fighter";
			array[0, 2] = "loc_gacha_lv3_item_Fighter";
			array[1, 0] = "loc_gacha_lv1_item_Magician";
			array[1, 1] = "loc_gacha_lv2_item_Magician";
			array[1, 2] = "loc_gacha_lv3_item_Magician";
			array[2, 0] = "loc_gacha_lv1_item_Priest";
			array[2, 1] = "loc_gacha_lv2_item_Priest";
			array[2, 2] = "loc_gacha_lv3_item_Priest";
			array[3, 0] = "loc_gacha_lv1_item_Knight";
			array[3, 1] = "loc_gacha_lv2_item_Knight";
			array[3, 2] = "loc_gacha_lv3_item_Knight";
			array[4, 0] = "loc_gacha_lv1_item_Alchemist";
			array[4, 1] = "loc_gacha_lv2_item_Alchemist";
			array[4, 2] = "loc_gacha_lv3_item_Alchemist";
			GachaObjectHandler.MAGIC_CIRCLE_PATHS = array;
			GachaObjectHandler.ANIM_KEYS = new string[]
			{
				"start",
				"branch_lv1",
				"branch_lv1_loop",
				"branch_lv2",
				"branch_lv2_loop",
				"branch_lv3",
				"branch_lv3_cutin_loop",
				"branch_lv3_loop"
			};
		}

		// Token: 0x04001902 RID: 6402
		public const int LV_NUM = 3;

		// Token: 0x04001903 RID: 6403
		private static readonly string[,] MAGIC_CIRCLE_PATHS;

		// Token: 0x04001904 RID: 6404
		private const string BG_OBJ_NAME = "gacha_com_bg_rgb";

		// Token: 0x04001905 RID: 6405
		private static readonly string[] ANIM_KEYS;

		// Token: 0x04001906 RID: 6406
		private Transform m_BaseTransform;

		// Token: 0x04001907 RID: 6407
		private Animation m_Anim;

		// Token: 0x04001908 RID: 6408
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04001909 RID: 6409
		private MeigeAnimEventNotifier m_MeigeAnimEvNotify;

		// Token: 0x0400190A RID: 6410
		private MsbHandler m_MsbHndl;

		// Token: 0x0400190B RID: 6411
		private float m_StartAlpha;

		// Token: 0x0400190C RID: 6412
		private float m_EndAlpha;

		// Token: 0x0400190D RID: 6413
		private float m_FadeTime;

		// Token: 0x0400190E RID: 6414
		private float m_FadeTimer;

		// Token: 0x0400190F RID: 6415
		private bool m_IsUpdateMeshColor;

		// Token: 0x020003CD RID: 973
		public enum eAnimType
		{
			// Token: 0x04001911 RID: 6417
			Start,
			// Token: 0x04001912 RID: 6418
			Branch_lv1,
			// Token: 0x04001913 RID: 6419
			Branch_lv1_loop,
			// Token: 0x04001914 RID: 6420
			Branch_lv2,
			// Token: 0x04001915 RID: 6421
			Branch_lv2_loop,
			// Token: 0x04001916 RID: 6422
			Branch_lv3,
			// Token: 0x04001917 RID: 6423
			Branch_lv3_cutIn_loop,
			// Token: 0x04001918 RID: 6424
			Branch_lv3_loop,
			// Token: 0x04001919 RID: 6425
			Num
		}

		// Token: 0x020003CE RID: 974
		public enum eAnimEvent
		{
			// Token: 0x0400191B RID: 6427
			FadeOut = 1
		}
	}
}
