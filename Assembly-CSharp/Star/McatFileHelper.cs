﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000392 RID: 914
	public class McatFileHelper : MonoBehaviour
	{
		// Token: 0x04001815 RID: 6165
		[SerializeField]
		public McatFileHelper.MabFileState[] m_Table;

		// Token: 0x02000393 RID: 915
		[Serializable]
		public struct MabFileState
		{
			// Token: 0x04001816 RID: 6166
			public GameObject m_MabFile;

			// Token: 0x04001817 RID: 6167
			public int m_AccessKey;

			// Token: 0x04001818 RID: 6168
			public string m_AnimeName;
		}
	}
}
