﻿using System;

namespace Star
{
	// Token: 0x020001FE RID: 510
	public enum eSkillContentAndEffectCombiDB
	{
		// Token: 0x04000C88 RID: 3208
		Recover,
		// Token: 0x04000C89 RID: 3209
		StatusChange_Up,
		// Token: 0x04000C8A RID: 3210
		StatusChange_Down,
		// Token: 0x04000C8B RID: 3211
		Abnormal,
		// Token: 0x04000C8C RID: 3212
		AbnormalRecover,
		// Token: 0x04000C8D RID: 3213
		AbnormalAdditionalProbability,
		// Token: 0x04000C8E RID: 3214
		ElementResist_FireUp,
		// Token: 0x04000C8F RID: 3215
		ElementResist_WaterUp,
		// Token: 0x04000C90 RID: 3216
		ElementResist_EarthUp,
		// Token: 0x04000C91 RID: 3217
		ElementResist_WindUp,
		// Token: 0x04000C92 RID: 3218
		ElementResist_MoonUp,
		// Token: 0x04000C93 RID: 3219
		ElementResist_SunUp,
		// Token: 0x04000C94 RID: 3220
		ElementResist_FireDown,
		// Token: 0x04000C95 RID: 3221
		ElementResist_WaterDown,
		// Token: 0x04000C96 RID: 3222
		ElementResist_EarthDown,
		// Token: 0x04000C97 RID: 3223
		ElementResist_WindDown,
		// Token: 0x04000C98 RID: 3224
		ElementResist_MoonDown,
		// Token: 0x04000C99 RID: 3225
		ElementResist_SunDown,
		// Token: 0x04000C9A RID: 3226
		ElementChange,
		// Token: 0x04000C9B RID: 3227
		WeakElementBonus,
		// Token: 0x04000C9C RID: 3228
		NextAttackUp,
		// Token: 0x04000C9D RID: 3229
		NextAttackCritical,
		// Token: 0x04000C9E RID: 3230
		Barrier,
		// Token: 0x04000C9F RID: 3231
		RecastChange,
		// Token: 0x04000CA0 RID: 3232
		HateChange,
		// Token: 0x04000CA1 RID: 3233
		ChargeChange,
		// Token: 0x04000CA2 RID: 3234
		ChainCoefChange,
		// Token: 0x04000CA3 RID: 3235
		Max
	}
}
