﻿using System;

namespace Star
{
	// Token: 0x020002B3 RID: 691
	public class LimitBreakResultBeforeAfterPairAfterPartCharacterWrapper : LimitBreakResultBeforeAfterPairPartCharacterWrapper
	{
		// Token: 0x06000CF2 RID: 3314 RVA: 0x000491C6 File Offset: 0x000475C6
		public LimitBreakResultBeforeAfterPairAfterPartCharacterWrapper(EditMain.LimitBreakResultData result) : base(eCharacterDataType.AfterLimitBreakParam, result)
		{
		}

		// Token: 0x06000CF3 RID: 3315 RVA: 0x000491D0 File Offset: 0x000475D0
		public override int GetMaxLv()
		{
			if (this.m_Result == null)
			{
				return base.GetMaxLv();
			}
			return this.m_Result.m_AfterMaxLv;
		}

		// Token: 0x06000CF4 RID: 3316 RVA: 0x000491EF File Offset: 0x000475EF
		public override int GetLimitBreak()
		{
			if (this.m_Result == null)
			{
				return base.GetLimitBreak();
			}
			return base.GetLimitBreak();
		}

		// Token: 0x06000CF5 RID: 3317 RVA: 0x00049209 File Offset: 0x00047609
		public override int GetUniqueSkillMaxLv()
		{
			return this.m_Result.m_AfterMaxUniqueSkillLv;
		}

		// Token: 0x06000CF6 RID: 3318 RVA: 0x00049216 File Offset: 0x00047616
		public override int[] GetClassSkillLevels()
		{
			return this.m_Result.m_AfterMaxClassSkillLv;
		}
	}
}
