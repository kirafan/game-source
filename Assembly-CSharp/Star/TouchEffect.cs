﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000AD7 RID: 2775
	public class TouchEffect : ActionAggregation
	{
		// Token: 0x06003A12 RID: 14866 RVA: 0x00128131 File Offset: 0x00126531
		private void Update()
		{
			base.UpdateChildren();
			if (base.GetState() == eActionAggregationState.Finish)
			{
				base.gameObject.SetActive(false);
			}
		}

		// Token: 0x06003A13 RID: 14867 RVA: 0x00128154 File Offset: 0x00126554
		public void Play(Vector2 pos, Canvas canvas, Camera uicamera, Camera wcamera)
		{
			if (canvas == null)
			{
				return;
			}
			Vector2 zero = Vector2.zero;
			RectTransform component = canvas.GetComponent<RectTransform>();
			RectTransformUtility.ScreenPointToLocalPointInRectangle(component, pos, uicamera, out zero);
			base.GetComponent<RectTransform>().localPosition = zero;
			base.gameObject.SetActive(true);
			if (this.m_RectTransform == null)
			{
				this.m_RectTransform = base.GetComponent<RectTransform>();
			}
			this.m_RectTransform.localEulerAngles = new Vector3(0f, 0f, UnityEngine.Random.Range(0f, 360f));
			this.ForceRecoveryChildFinishToWait();
			base.PlayStart();
		}

		// Token: 0x04004154 RID: 16724
		private RectTransform m_RectTransform;
	}
}
