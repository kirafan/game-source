﻿using System;

namespace Star
{
	// Token: 0x02000425 RID: 1061
	public abstract class GameStateBase
	{
		// Token: 0x06001459 RID: 5209
		public abstract int GetStateID();

		// Token: 0x0600145A RID: 5210
		public abstract void OnStateEnter();

		// Token: 0x0600145B RID: 5211
		public abstract void OnStateExit();

		// Token: 0x0600145C RID: 5212
		public abstract int OnStateUpdate();

		// Token: 0x0600145D RID: 5213
		public abstract void OnDispose();

		// Token: 0x0600145E RID: 5214
		protected abstract void OnClickBackButton(bool isCallFromShortCut);
	}
}
