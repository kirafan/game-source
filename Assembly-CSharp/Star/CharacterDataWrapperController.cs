﻿using System;

namespace Star
{
	// Token: 0x020002D8 RID: 728
	public class CharacterDataWrapperController : CharacterDataWrapperControllerBase
	{
		// Token: 0x06000E3D RID: 3645 RVA: 0x0004DA76 File Offset: 0x0004BE76
		public CharacterDataWrapperController() : base(eCharacterDataControllerType.UnmanagedCharacterDataWrapper)
		{
		}

		// Token: 0x06000E3E RID: 3646 RVA: 0x0004DA7F File Offset: 0x0004BE7F
		public CharacterDataWrapperController Setup(CharacterDataWrapperBase in_cdw)
		{
			this.cdw = in_cdw;
			return this;
		}

		// Token: 0x06000E3F RID: 3647 RVA: 0x0004DA89 File Offset: 0x0004BE89
		protected override CharacterDataWrapperBase GetDataBaseInternal()
		{
			return this.cdw;
		}

		// Token: 0x04001598 RID: 5528
		private CharacterDataWrapperBase cdw;
	}
}
