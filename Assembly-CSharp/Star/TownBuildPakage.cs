﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006F7 RID: 1783
	public class TownBuildPakage : MonoBehaviour
	{
		// Token: 0x06002356 RID: 9046 RVA: 0x000BDE80 File Offset: 0x000BC280
		private void Awake()
		{
			base.enabled = false;
			this.m_ReqTable = new TownBuildPakage.HandleChangeReq[4];
			this.m_ReqNum = 0;
		}

		// Token: 0x06002357 RID: 9047 RVA: 0x000BDE9C File Offset: 0x000BC29C
		private void StackReq(TownBuildPakage.eStep fmode, ITownObjectHandler phandle)
		{
			if (this.m_ReqNum < 4)
			{
				this.m_ReqTable[(int)this.m_ReqNum].m_NextMode = fmode;
				this.m_ReqTable[(int)this.m_ReqNum].m_Handle = phandle;
				this.m_ReqNum += 1;
			}
		}

		// Token: 0x06002358 RID: 9048 RVA: 0x000BDEF2 File Offset: 0x000BC2F2
		public void SetLinkHandle(ITownObjectHandler plink)
		{
			this.StackReq(TownBuildPakage.eStep.ChangeHandle, plink);
			base.enabled = true;
		}

		// Token: 0x06002359 RID: 9049 RVA: 0x000BDF04 File Offset: 0x000BC304
		private void Update()
		{
			if (this.m_Mode == TownBuildPakage.eStep.Non && this.m_ReqNum != 0)
			{
				this.m_Mode = this.m_ReqTable[0].m_NextMode;
				if (this.m_ReqTable[0].m_Handle != null)
				{
					if (this.m_CtrlHandle != null)
					{
						this.m_BackNode = this.m_CtrlHandle;
						this.m_BackNode.RecvEvent(ITownHandleAction.CreateAction(eTownEventAct.StateChange));
					}
					this.m_CtrlHandle = this.m_ReqTable[0].m_Handle;
				}
				this.m_ReqTable[0].Clear();
				for (int i = 1; i < (int)this.m_ReqNum; i++)
				{
					this.m_ReqTable[i - 1] = this.m_ReqTable[i];
				}
				this.m_ReqNum -= 1;
				this.m_ReqTable[(int)this.m_ReqNum].Clear();
			}
			switch (this.m_Mode)
			{
			case TownBuildPakage.eStep.Non:
				base.enabled = false;
				break;
			case TownBuildPakage.eStep.ChangeHandle:
				if (this.m_CtrlHandle.IsAvailable())
				{
					if (this.m_BackNode != null)
					{
						this.m_BackNode.RecvEvent(ITownHandleAction.CreateAction(eTownEventAct.StepOut));
					}
					this.m_CtrlHandle.RecvEvent(ITownHandleAction.CreateAction(eTownEventAct.StepIn));
					this.m_Mode = TownBuildPakage.eStep.Non;
				}
				break;
			case TownBuildPakage.eStep.BackHandle:
				this.m_CtrlHandle = this.m_BackNode;
				if (this.m_CtrlHandle != null)
				{
					this.m_CtrlHandle.RecvEvent(ITownHandleAction.CreateAction(eTownEventAct.StepIn));
				}
				this.m_BackNode = null;
				this.m_Mode = TownBuildPakage.eStep.Non;
				break;
			case TownBuildPakage.eStep.DestroyQue:
				if (this.m_CtrlHandle)
				{
					this.m_CtrlHandle.enabled = true;
					this.m_CtrlHandle.DestroyRequest();
				}
				this.m_CtrlHandle = this.m_BackNode;
				if (this.m_CtrlHandle != null)
				{
					this.m_CtrlHandle.enabled = true;
					this.m_CtrlHandle.DestroyRequest();
				}
				this.m_BackNode = null;
				this.m_CtrlHandle = null;
				this.m_Mode = TownBuildPakage.eStep.Non;
				break;
			}
		}

		// Token: 0x0600235A RID: 9050 RVA: 0x000BE147 File Offset: 0x000BC547
		public ITownObjectHandler GetHandle()
		{
			return this.m_CtrlHandle;
		}

		// Token: 0x0600235B RID: 9051 RVA: 0x000BE14F File Offset: 0x000BC54F
		public bool IsFreeArea()
		{
			return this.m_CtrlHandle == null;
		}

		// Token: 0x0600235C RID: 9052 RVA: 0x000BE15D File Offset: 0x000BC55D
		public void BackBuildMode()
		{
			if (this.m_CtrlHandle)
			{
				this.m_CtrlHandle.DestroyRequest();
			}
			this.StackReq(TownBuildPakage.eStep.BackHandle, null);
			base.enabled = true;
		}

		// Token: 0x0600235D RID: 9053 RVA: 0x000BE189 File Offset: 0x000BC589
		public void DestroyBuildMode()
		{
			this.StackReq(TownBuildPakage.eStep.DestroyQue, null);
			base.enabled = true;
		}

		// Token: 0x0600235E RID: 9054 RVA: 0x000BE19A File Offset: 0x000BC59A
		public bool IsSetUpHandle(long ptargetmngid)
		{
			return this.m_CtrlHandle != null && ptargetmngid == this.m_CtrlHandle.GetManageID() && this.m_CtrlHandle.IsAvailable();
		}

		// Token: 0x0600235F RID: 9055 RVA: 0x000BE1CC File Offset: 0x000BC5CC
		public void Release()
		{
			if (this.m_CtrlHandle != null)
			{
				this.m_CtrlHandle.Destroy();
				UnityEngine.Object.Destroy(this.m_CtrlHandle);
				this.m_CtrlHandle = null;
			}
			if (this.m_BackNode != null)
			{
				this.m_BackNode.Destroy();
				UnityEngine.Object.Destroy(this.m_BackNode);
				this.m_BackNode = null;
			}
			this.m_Mode = TownBuildPakage.eStep.Non;
		}

		// Token: 0x04002A1F RID: 10783
		public ITownObjectHandler m_CtrlHandle;

		// Token: 0x04002A20 RID: 10784
		public ITownObjectHandler m_BackNode;

		// Token: 0x04002A21 RID: 10785
		public int m_Index;

		// Token: 0x04002A22 RID: 10786
		public int m_Group;

		// Token: 0x04002A23 RID: 10787
		public int m_Layer;

		// Token: 0x04002A24 RID: 10788
		private const short MAX_TABLE = 4;

		// Token: 0x04002A25 RID: 10789
		private TownBuildPakage.eStep m_Mode;

		// Token: 0x04002A26 RID: 10790
		public TownBuildPakage.HandleChangeReq[] m_ReqTable;

		// Token: 0x04002A27 RID: 10791
		public short m_ReqNum;

		// Token: 0x020006F8 RID: 1784
		public enum eStep
		{
			// Token: 0x04002A29 RID: 10793
			Non,
			// Token: 0x04002A2A RID: 10794
			ChangeHandle,
			// Token: 0x04002A2B RID: 10795
			BackHandle,
			// Token: 0x04002A2C RID: 10796
			DestroyQue
		}

		// Token: 0x020006F9 RID: 1785
		public struct HandleChangeReq
		{
			// Token: 0x06002360 RID: 9056 RVA: 0x000BE23C File Offset: 0x000BC63C
			public void Clear()
			{
				this.m_Handle = null;
			}

			// Token: 0x04002A2D RID: 10797
			public TownBuildPakage.eStep m_NextMode;

			// Token: 0x04002A2E RID: 10798
			public ITownObjectHandler m_Handle;
		}
	}
}
