﻿using System;

namespace Star
{
	// Token: 0x020002E7 RID: 743
	public class EquipWeaponManagedPartiesControllerExGen<ThisType, PartyType> : EquipWeaponManagedPartiesController where ThisType : EquipWeaponManagedPartiesControllerExGen<ThisType, PartyType> where PartyType : EquipWeaponManagedPartyController
	{
		// Token: 0x06000E75 RID: 3701 RVA: 0x0004DFE3 File Offset: 0x0004C3E3
		public virtual ThisType SetupEx()
		{
			return (ThisType)((object)this);
		}

		// Token: 0x06000E76 RID: 3702 RVA: 0x0004DFEB File Offset: 0x0004C3EB
		public override int HowManyParties()
		{
			return this.parties.Length;
		}

		// Token: 0x06000E77 RID: 3703 RVA: 0x0004DFF5 File Offset: 0x0004C3F5
		public virtual PartyType GetPartyEx(int index)
		{
			return this.parties[index];
		}

		// Token: 0x06000E78 RID: 3704 RVA: 0x0004E003 File Offset: 0x0004C403
		public override EquipWeaponPartyController GetParty(int index)
		{
			return this.GetPartyEx(index);
		}

		// Token: 0x040015A6 RID: 5542
		protected PartyType[] parties;
	}
}
