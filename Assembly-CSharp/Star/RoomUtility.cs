﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Star
{
	// Token: 0x020005F2 RID: 1522
	public static class RoomUtility
	{
		// Token: 0x06001E0B RID: 7691 RVA: 0x000A1704 File Offset: 0x0009FB04
		public static string GetSpriteResourcePath(eRoomObjectCategory category, int objID, int fsubkey, CharacterDefine.eDir fdir)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Prefab/Room/");
			if (category != eRoomObjectCategory.Floor)
			{
				if (category != eRoomObjectCategory.Wall)
				{
					if (category == eRoomObjectCategory.Background)
					{
						stringBuilder.Append("Background/Background_");
						stringBuilder.Append(objID);
						if (fsubkey == 0)
						{
							stringBuilder.Append("_0");
						}
						else
						{
							stringBuilder.Append("_1");
						}
					}
				}
				else
				{
					stringBuilder.Append("Wall/Wall_");
					stringBuilder.Append(objID);
					if (fsubkey != 0)
					{
						stringBuilder.Append("_B");
					}
					else
					{
						stringBuilder.Append("_M");
					}
				}
			}
			else
			{
				stringBuilder.Append("Floor/Floor_");
				stringBuilder.Append(objID);
				if (fsubkey != 0)
				{
					stringBuilder.Append("_B");
				}
				else
				{
					stringBuilder.Append("_M");
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001E0C RID: 7692 RVA: 0x000A17FC File Offset: 0x0009FBFC
		public static string GetModelResourcePath(eRoomObjectCategory category, int objID)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("Prefab/Room/");
			switch (category)
			{
			case eRoomObjectCategory.Desk:
				stringBuilder.Append("Desk/Desk_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Chair:
				stringBuilder.Append("Chair/Chair_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Storage:
				stringBuilder.Append("Storage/Storage_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Bedding:
				stringBuilder.Append("Bedding/Bedding_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Appliances:
				stringBuilder.Append("Appliances/Appliances_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Goods:
				stringBuilder.Append("Goods/Goods_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Hobby:
				stringBuilder.Append("Hobby/Hobby_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.WallDecoration:
				stringBuilder.Append("WallDecoration/WallDecoration_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Carpet:
				stringBuilder.Append("Carpet/Carpet_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Screen:
				stringBuilder.Append("Screen/Screen_");
				stringBuilder.Append(objID);
				break;
			case eRoomObjectCategory.Msgboard:
				stringBuilder.Append("MsgBoard/MsgBoard_");
				stringBuilder.Append(objID);
				break;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001E0D RID: 7693 RVA: 0x000A1978 File Offset: 0x0009FD78
		public static void IsPlacementObject(ref RoomBuilder.EditObjectState pret, eRoomObjectCategory category, int floorID)
		{
			pret.m_MenuType = false;
			RoomBuilder.eEditObject state;
			switch (category)
			{
			case eRoomObjectCategory.Desk:
			case eRoomObjectCategory.Chair:
			case eRoomObjectCategory.Storage:
			case eRoomObjectCategory.Appliances:
			case eRoomObjectCategory.Goods:
			case eRoomObjectCategory.Hobby:
			case eRoomObjectCategory.Carpet:
			case eRoomObjectCategory.Screen:
				state = RoomBuilder.eEditObject.Floor;
				goto IL_7C;
			case eRoomObjectCategory.Bedding:
				state = RoomBuilder.eEditObject.Floor;
				if (floorID != 0)
				{
					pret.m_MenuType = true;
				}
				goto IL_7C;
			case eRoomObjectCategory.WallDecoration:
				state = RoomBuilder.eEditObject.Wall;
				goto IL_7C;
			case eRoomObjectCategory.Msgboard:
				state = RoomBuilder.eEditObject.Msgboard;
				goto IL_7C;
			}
			state = RoomBuilder.eEditObject.Chara;
			IL_7C:
			pret.m_State = state;
		}

		// Token: 0x06001E0E RID: 7694 RVA: 0x000A1A08 File Offset: 0x0009FE08
		public static string GetRoomShadowResourcePath()
		{
			return "model/shadow/shadow_room";
		}

		// Token: 0x06001E0F RID: 7695 RVA: 0x000A1A0F File Offset: 0x0009FE0F
		public static bool IsPlacementCountObj(eRoomObjectCategory category)
		{
			return category != eRoomObjectCategory.Background && category != eRoomObjectCategory.Wall && category != eRoomObjectCategory.Floor;
		}

		// Token: 0x06001E10 RID: 7696 RVA: 0x000A1A2C File Offset: 0x0009FE2C
		public static bool IsRoomDatabaseActive()
		{
			return RoomAnimePlay.ms_RoomAnimeTagDB != null;
		}

		// Token: 0x06001E11 RID: 7697 RVA: 0x000A1A3C File Offset: 0x0009FE3C
		public static void GetRoomCharacterAnimSettings(List<AnimSettings> settings, eCharaResourceType resourceType, int resourceID, int partsIndex, CharacterParam param)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			RoomAnimePlay.GetRoomCharacterAnimSettings(settings, partsIndex, dbMng.CharaListDB.GetParam(param.CharaID).m_HeadID, 1);
		}

		// Token: 0x06001E12 RID: 7698 RVA: 0x000A1A78 File Offset: 0x0009FE78
		public static Transform FindTransform(Transform ptarget, string findname, int fchkcall)
		{
			if (ptarget.name == findname)
			{
				return ptarget;
			}
			if (fchkcall <= 0)
			{
				return null;
			}
			fchkcall--;
			int childCount = ptarget.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform transform = RoomUtility.FindTransform(ptarget.GetChild(i), findname, fchkcall);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x06001E13 RID: 7699 RVA: 0x000A1AE0 File Offset: 0x0009FEE0
		public static Transform FindStrTransform(Transform ptarget, string findname, int fchkcall)
		{
			if (findname.IndexOf(ptarget.name) >= 0)
			{
				return ptarget;
			}
			if (fchkcall <= 0)
			{
				return null;
			}
			fchkcall--;
			int childCount = ptarget.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform transform = RoomUtility.FindStrTransform(ptarget.GetChild(i), findname, fchkcall);
				if (transform != null)
				{
					return transform;
				}
			}
			return null;
		}

		// Token: 0x06001E14 RID: 7700 RVA: 0x000A1B48 File Offset: 0x0009FF48
		public static CharacterDefine.eDir CalcObjectLockDir(IRoomObjectControll ptarget, CharacterHandler pbase)
		{
			Vector3 position = ptarget.CacheTransform.position;
			CharacterDefine.eDir result;
			if (pbase.CacheTransform.position.x > position.x)
			{
				result = CharacterDefine.eDir.L;
			}
			else
			{
				result = CharacterDefine.eDir.R;
			}
			return result;
		}

		// Token: 0x06001E15 RID: 7701 RVA: 0x000A1B8A File Offset: 0x0009FF8A
		public static bool CalcObjectChildDir(IRoomObjectControll ptarget, RoomObjectCtrlChara pbase)
		{
			return ptarget.GetDir() == pbase.GetDir();
		}

		// Token: 0x06001E16 RID: 7702 RVA: 0x000A1B9A File Offset: 0x0009FF9A
		public static CharacterDefine.eDir ChangeObjectDir(CharacterDefine.eDir fdir)
		{
			if (fdir == CharacterDefine.eDir.R)
			{
				fdir = CharacterDefine.eDir.L;
			}
			else
			{
				fdir = CharacterDefine.eDir.R;
			}
			return fdir;
		}

		// Token: 0x06001E17 RID: 7703 RVA: 0x000A1BAF File Offset: 0x0009FFAF
		public static CharacterDefine.eDir ChangeObjectRevDir(CharacterDefine.eDir fdir)
		{
			if (fdir == CharacterDefine.eDir.R)
			{
				fdir = CharacterDefine.eDir.R;
			}
			else
			{
				fdir = CharacterDefine.eDir.L;
			}
			return fdir;
		}

		// Token: 0x06001E18 RID: 7704 RVA: 0x000A1BC4 File Offset: 0x0009FFC4
		public static Transform GetMeigeControllNode(GameObject pobj)
		{
			MsbHandler componentInChildren = pobj.GetComponentInChildren<MsbHandler>();
			if (componentInChildren != null)
			{
				return componentInChildren.gameObject.transform;
			}
			return null;
		}

		// Token: 0x06001E19 RID: 7705 RVA: 0x000A1BF1 File Offset: 0x0009FFF1
		public static int GetFloorLayer(int fgroupno)
		{
			return -960 - fgroupno * 4000;
		}

		// Token: 0x06001E1A RID: 7706 RVA: 0x000A1C00 File Offset: 0x000A0000
		public static int GetWallLayer(int fgroupno)
		{
			return -1000 - fgroupno * 4000;
		}

		// Token: 0x06001E1B RID: 7707 RVA: 0x000A1C10 File Offset: 0x000A0010
		public static float GetCharaMoveTime(Vector3 ftarget, Vector3 fposition)
		{
			Vector3 vector = ftarget - fposition;
			vector.z = 0f;
			return vector.magnitude / 0.45f;
		}

		// Token: 0x06001E1C RID: 7708 RVA: 0x000A1C40 File Offset: 0x000A0040
		public static Vector2 GridToTilePos(float gridX, float gridY)
		{
			Vector2 a = new Vector2(50f * (gridX - gridY), 25f * (gridX + gridY));
			return a / 100f;
		}

		// Token: 0x06001E1D RID: 7709 RVA: 0x000A1C71 File Offset: 0x000A0071
		public static Vector2 GetBlockSize(float fblockx, float fblocky)
		{
			return RoomUtility.GridToTilePos(fblockx, fblocky);
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x000A1C7C File Offset: 0x000A007C
		public static Vector2 CalcDummyGridPos(Vector3 fpos)
		{
			Vector2 zero = Vector2.zero;
			Vector2 zero2 = Vector2.zero;
			zero2.x = -fpos.x / 0.5f;
			zero2.y = fpos.y / 0.5f;
			zero.x = 0.89493436f * zero2.x + 0.446197f * zero2.y;
			zero.y = -0.446197f * zero2.x + 0.89493436f * zero2.y;
			return zero;
		}
	}
}
