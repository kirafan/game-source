﻿using System;

namespace Star
{
	// Token: 0x02000247 RID: 583
	[Serializable]
	public struct TownObjectBuildSubCodeDB_Param
	{
		// Token: 0x04001335 RID: 4917
		public int m_ID;

		// Token: 0x04001336 RID: 4918
		public ushort m_Category;

		// Token: 0x04001337 RID: 4919
		public int m_ActionNo;

		// Token: 0x04001338 RID: 4920
		public ushort m_ExtentionScriptID;

		// Token: 0x04001339 RID: 4921
		public int m_ExtentionFunKey;
	}
}
