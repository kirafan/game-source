﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020002A3 RID: 675
	public class UserSupportDataWrapper : CharacterDataWrapperBaseExGen<UserSupportData>
	{
		// Token: 0x06000CA6 RID: 3238 RVA: 0x00048A3C File Offset: 0x00046E3C
		public UserSupportDataWrapper(UserSupportData in_data) : base(eCharacterDataType.UserSupportData, in_data)
		{
			if (in_data != null)
			{
				this.cldbp = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(in_data.CharaID);
				this.usld = new SkillLearnData(this.cldbp.m_CharaSkillID, in_data.UniqueSkillLv, EditUtility.GetSkillMaxLv(this.cldbp.m_CharaID, in_data.LimitBreak), -1L, this.cldbp.m_CharaSkillExpTableID, true);
				if (in_data.ClassSkillLvs != null)
				{
					this.cslds = new List<SkillLearnData>();
					for (int i = 0; i < in_data.ClassSkillLvs.Length; i++)
					{
						this.cslds.Add(new SkillLearnData(this.cldbp.m_ClassSkillIDs[i], this.data.ClassSkillLvs[i], EditUtility.GetSkillMaxLv(this.cldbp.m_CharaID, in_data.LimitBreak), -1L, (sbyte)this.cldbp.m_ClassSkillIDs[i], true));
					}
				}
			}
		}

		// Token: 0x06000CA7 RID: 3239 RVA: 0x00048B37 File Offset: 0x00046F37
		public override long GetMngId()
		{
			return this.data.MngID;
		}

		// Token: 0x06000CA8 RID: 3240 RVA: 0x00048B44 File Offset: 0x00046F44
		public override int GetCharaId()
		{
			return this.data.CharaID;
		}

		// Token: 0x06000CA9 RID: 3241 RVA: 0x00048B51 File Offset: 0x00046F51
		public override eCharaNamedType GetCharaNamedType()
		{
			return (eCharaNamedType)this.cldbp.m_NamedType;
		}

		// Token: 0x06000CAA RID: 3242 RVA: 0x00048B5E File Offset: 0x00046F5E
		public override eClassType GetClassType()
		{
			return (eClassType)this.cldbp.m_Class;
		}

		// Token: 0x06000CAB RID: 3243 RVA: 0x00048B6B File Offset: 0x00046F6B
		public override eElementType GetElementType()
		{
			return (eElementType)this.cldbp.m_Element;
		}

		// Token: 0x06000CAC RID: 3244 RVA: 0x00048B78 File Offset: 0x00046F78
		public override eRare GetRarity()
		{
			return (eRare)this.cldbp.m_Rare;
		}

		// Token: 0x06000CAD RID: 3245 RVA: 0x00048B85 File Offset: 0x00046F85
		public override int GetCost()
		{
			return EditUtility.CalcCharaCostFromUserSupportData(this.data, false);
		}

		// Token: 0x06000CAE RID: 3246 RVA: 0x00048B93 File Offset: 0x00046F93
		public override int GetLv()
		{
			return this.data.Lv;
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x00048BA0 File Offset: 0x00046FA0
		public override int GetMaxLv()
		{
			return this.data.MaxLv;
		}

		// Token: 0x06000CB0 RID: 3248 RVA: 0x00048BAD File Offset: 0x00046FAD
		public override long GetExp()
		{
			return this.data.Exp;
		}

		// Token: 0x06000CB1 RID: 3249 RVA: 0x00048BBA File Offset: 0x00046FBA
		public override int GetLimitBreak()
		{
			return this.data.LimitBreak;
		}

		// Token: 0x06000CB2 RID: 3250 RVA: 0x00048BC8 File Offset: 0x00046FC8
		public int GetWeaponId()
		{
			int num = this.GetWeaponIdNaked();
			if (num < 0)
			{
				CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(this.GetCharaId());
				WeaponParam weaponParam = new WeaponParam();
				CharacterUtility.SetupDefaultWeaponParam(weaponParam, (eClassType)param.m_Class);
				num = weaponParam.WeaponID;
			}
			return num;
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x00048C1C File Offset: 0x0004701C
		public int GetWeaponIdNaked()
		{
			return this.data.WeaponID;
		}

		// Token: 0x06000CB4 RID: 3252 RVA: 0x00048C36 File Offset: 0x00047036
		public int GetWeaponLv()
		{
			if (this.data.WeaponID <= -1)
			{
				return -1;
			}
			return this.data.WeaponLv;
		}

		// Token: 0x06000CB5 RID: 3253 RVA: 0x00048C58 File Offset: 0x00047058
		public int GetWeaponMaxLv()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.GetWeaponId()).m_LimitLv;
		}

		// Token: 0x06000CB6 RID: 3254 RVA: 0x00048C8B File Offset: 0x0004708B
		public long GetWeaponExp()
		{
			return 0L;
		}

		// Token: 0x06000CB7 RID: 3255 RVA: 0x00048C8F File Offset: 0x0004708F
		public WeaponListDB_Param GetWeaponListDBParam()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.WeaponListDB.GetParam(this.GetWeaponId());
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x00048CAC File Offset: 0x000470AC
		public int GetWeaponSkillID()
		{
			return this.GetWeaponListDBParam().m_SkillID;
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x00048CC8 File Offset: 0x000470C8
		public sbyte GetWeaponSkillExpTableID()
		{
			return this.GetWeaponListDBParam().m_SkillExpTableID;
		}

		// Token: 0x06000CBA RID: 3258 RVA: 0x00048CE3 File Offset: 0x000470E3
		public int GetWeaponSkillLv()
		{
			return this.data.WeaponSkillLv;
		}

		// Token: 0x06000CBB RID: 3259 RVA: 0x00048CF0 File Offset: 0x000470F0
		public int GetWeaponSkillMaxLv()
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.SkillExpDB.GetMaxLv(this.GetWeaponSkillExpTableID());
		}

		// Token: 0x06000CBC RID: 3260 RVA: 0x00048D0C File Offset: 0x0004710C
		public override int GetFriendship()
		{
			return this.data.FriendShip;
		}

		// Token: 0x06000CBD RID: 3261 RVA: 0x00048D1C File Offset: 0x0004711C
		public override int GetFriendshipMax()
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			return dbMng.NamedFriendshipExpDB.GetFriendShipMax(dbMng.NamedListDB.GetParam(this.GetCharaNamedType()).m_FriendshipTableID);
		}

		// Token: 0x06000CBE RID: 3262 RVA: 0x00048D58 File Offset: 0x00047158
		public override long GetFriendshipExp()
		{
			return this.data.FriendShipExp;
		}

		// Token: 0x06000CBF RID: 3263 RVA: 0x00048D65 File Offset: 0x00047165
		public override SkillLearnData GetUniqueSkillLearnData()
		{
			return this.usld;
		}

		// Token: 0x06000CC0 RID: 3264 RVA: 0x00048D6D File Offset: 0x0004716D
		public override int GetUniqueSkillLv()
		{
			return this.data.UniqueSkillLv;
		}

		// Token: 0x06000CC1 RID: 3265 RVA: 0x00048D7A File Offset: 0x0004717A
		public override int GetUniqueSkillMaxLv()
		{
			base.Assert();
			return this.usld.SkillMaxLv;
		}

		// Token: 0x06000CC2 RID: 3266 RVA: 0x00048D8D File Offset: 0x0004718D
		public override int GetClassSkillsCount()
		{
			if (this.data == null || this.data.ClassSkillLvs == null)
			{
				return 0;
			}
			return this.data.ClassSkillLvs.Length;
		}

		// Token: 0x06000CC3 RID: 3267 RVA: 0x00048DB9 File Offset: 0x000471B9
		public override List<SkillLearnData> GetClassSkillLearnDatas()
		{
			return this.cslds;
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x00048DC1 File Offset: 0x000471C1
		public override int[] GetClassSkillLevels()
		{
			return this.data.ClassSkillLvs;
		}

		// Token: 0x06000CC5 RID: 3269 RVA: 0x00048DD0 File Offset: 0x000471D0
		public override int GetClassSkillLevel(int index)
		{
			int[] classSkillLevels = this.GetClassSkillLevels();
			if (classSkillLevels == null || index < 0 || classSkillLevels.Length <= index)
			{
				return 1;
			}
			return classSkillLevels[index];
		}

		// Token: 0x04001544 RID: 5444
		protected CharacterListDB_Param cldbp;

		// Token: 0x04001545 RID: 5445
		protected SkillLearnData usld;

		// Token: 0x04001546 RID: 5446
		protected List<SkillLearnData> cslds;
	}
}
