﻿using System;

namespace Star
{
	// Token: 0x02000557 RID: 1367
	public class CActScriptKeyFrameChg : IRoomScriptData
	{
		// Token: 0x06001ADD RID: 6877 RVA: 0x0008F0FC File Offset: 0x0008D4FC
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyFrameChg actXlsKeyFrameChg = (ActXlsKeyFrameChg)pbase;
			this.m_Frame = (float)actXlsKeyFrameChg.m_Frame / 30f;
		}

		// Token: 0x040021B6 RID: 8630
		public float m_Frame;
	}
}
