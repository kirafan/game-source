﻿using System;

namespace Star
{
	// Token: 0x02000550 RID: 1360
	public class CActScriptKeyObjectDir : IRoomScriptData
	{
		// Token: 0x06001AD1 RID: 6865 RVA: 0x0008EF20 File Offset: 0x0008D320
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyObjectDir actXlsKeyObjectDir = (ActXlsKeyObjectDir)pbase;
			this.m_Func = actXlsKeyObjectDir.m_Func;
		}

		// Token: 0x0400219E RID: 8606
		public CActScriptKeyObjectDir.eDirFunc m_Func;

		// Token: 0x02000551 RID: 1361
		public enum eDirFunc
		{
			// Token: 0x040021A0 RID: 8608
			TargetDir,
			// Token: 0x040021A1 RID: 8609
			TargetRevDir,
			// Token: 0x040021A2 RID: 8610
			Left,
			// Token: 0x040021A3 RID: 8611
			Right
		}
	}
}
