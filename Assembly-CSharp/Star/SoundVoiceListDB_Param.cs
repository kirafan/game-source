﻿using System;

namespace Star
{
	// Token: 0x0200021F RID: 543
	[Serializable]
	public struct SoundVoiceListDB_Param
	{
		// Token: 0x04000E57 RID: 3671
		public int m_ID;

		// Token: 0x04000E58 RID: 3672
		public string m_CueName;
	}
}
