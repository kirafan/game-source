﻿using System;

namespace Star
{
	// Token: 0x02000190 RID: 400
	public static class CharacterListDB_Ext
	{
		// Token: 0x06000AE3 RID: 2787 RVA: 0x0004102C File Offset: 0x0003F42C
		public static CharacterListDB_Param GetParam(this CharacterListDB self, int charaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CharaID == charaID)
				{
					return self.m_Params[i];
				}
			}
			return default(CharacterListDB_Param);
		}

		// Token: 0x06000AE4 RID: 2788 RVA: 0x00041084 File Offset: 0x0003F484
		public static bool IsExistParam(this CharacterListDB self, int charaID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_CharaID == charaID)
				{
					return true;
				}
			}
			return false;
		}
	}
}
