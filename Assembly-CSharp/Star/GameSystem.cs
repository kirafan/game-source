﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using PreviewLabs;
using Star.UI;
using Star.UI.BackGround;
using Star.UI.Global;
using UnityEngine;
using UnityEngine.SceneManagement;
using WWWTypes;

namespace Star
{
	// Token: 0x0200064A RID: 1610
	public sealed class GameSystem : SingletonMonoBehaviour<GameSystem>
	{
		// Token: 0x17000200 RID: 512
		// (get) Token: 0x0600200D RID: 8205 RVA: 0x000AC3C4 File Offset: 0x000AA7C4
		// (set) Token: 0x0600200E RID: 8206 RVA: 0x000AC3CC File Offset: 0x000AA7CC
		public DateTime ServerTime
		{
			get
			{
				return this.m_ServerTime;
			}
			set
			{
				this.m_ServerTime = value;
				this.m_LastServerTime = value;
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x0600200F RID: 8207 RVA: 0x000AC3DC File Offset: 0x000AA7DC
		public LoginManager LoginManager
		{
			get
			{
				return this.m_LoginManager;
			}
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06002010 RID: 8208 RVA: 0x000AC3E4 File Offset: 0x000AA7E4
		public GameGlobalParameter GlobalParam
		{
			get
			{
				return this.m_GlobalParameter;
			}
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06002011 RID: 8209 RVA: 0x000AC3EC File Offset: 0x000AA7EC
		public DatabaseManager DbMng
		{
			get
			{
				return this.m_DatabaseManager;
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06002012 RID: 8210 RVA: 0x000AC3F4 File Offset: 0x000AA7F4
		public UserDataManager UserDataMng
		{
			get
			{
				return this.m_UserDataManager;
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06002013 RID: 8211 RVA: 0x000AC3FC File Offset: 0x000AA7FC
		public TutorialManager TutorialMng
		{
			get
			{
				return this.m_TutorialManager;
			}
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06002014 RID: 8212 RVA: 0x000AC404 File Offset: 0x000AA804
		public StoreManager StoreMng
		{
			get
			{
				return this.m_StoreManager;
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06002015 RID: 8213 RVA: 0x000AC40C File Offset: 0x000AA80C
		public QuestManager QuestMng
		{
			get
			{
				return this.m_QuestManager;
			}
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06002016 RID: 8214 RVA: 0x000AC414 File Offset: 0x000AA814
		public PresentManager PresentMng
		{
			get
			{
				return this.m_PresentManager;
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06002017 RID: 8215 RVA: 0x000AC41C File Offset: 0x000AA81C
		public FriendManager FriendMng
		{
			get
			{
				return this.m_FriendManager;
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06002018 RID: 8216 RVA: 0x000AC424 File Offset: 0x000AA824
		public ShopManager ShopMng
		{
			get
			{
				return this.m_ShopManager;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06002019 RID: 8217 RVA: 0x000AC42C File Offset: 0x000AA82C
		public MissionManager MissionMng
		{
			get
			{
				return this.m_MissionManager;
			}
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x0600201A RID: 8218 RVA: 0x000AC434 File Offset: 0x000AA834
		public Gacha Gacha
		{
			get
			{
				return this.m_Gacha;
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x0600201B RID: 8219 RVA: 0x000AC43C File Offset: 0x000AA83C
		public CharacterManager CharaMng
		{
			get
			{
				return this.m_CharacterManager;
			}
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x0600201C RID: 8220 RVA: 0x000AC444 File Offset: 0x000AA844
		public CharacterResourceManager CharaResMng
		{
			get
			{
				return this.m_CharacterResourceManager;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x0600201D RID: 8221 RVA: 0x000AC44C File Offset: 0x000AA84C
		public TownResourceManager TownResMng
		{
			get
			{
				return this.m_TownResourceManager;
			}
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x0600201E RID: 8222 RVA: 0x000AC454 File Offset: 0x000AA854
		public RoomObjectResourceManager RoomResMng
		{
			get
			{
				return this.m_RoomResourceManager;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x0600201F RID: 8223 RVA: 0x000AC45C File Offset: 0x000AA85C
		public SpriteManager SpriteMng
		{
			get
			{
				return this.m_SpriteManager;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06002020 RID: 8224 RVA: 0x000AC464 File Offset: 0x000AA864
		public SoundManager SoundMng
		{
			get
			{
				return this.m_SoundManager;
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06002021 RID: 8225 RVA: 0x000AC46C File Offset: 0x000AA86C
		public VoiceController VoiceCtrl
		{
			get
			{
				return this.m_VoiceController;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06002022 RID: 8226 RVA: 0x000AC474 File Offset: 0x000AA874
		public MovieManager MovieMng
		{
			get
			{
				return this.m_MovieManager;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06002023 RID: 8227 RVA: 0x000AC47C File Offset: 0x000AA87C
		public CRIFileInstaller CRIFileInstaller
		{
			get
			{
				return this.m_CRIFileInstaller;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06002024 RID: 8228 RVA: 0x000AC484 File Offset: 0x000AA884
		public UISettings UISettings
		{
			get
			{
				return this.m_UISettings;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06002025 RID: 8229 RVA: 0x000AC48C File Offset: 0x000AA88C
		public EffectManager EffectMng
		{
			get
			{
				return this.m_EffectManager;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06002026 RID: 8230 RVA: 0x000AC494 File Offset: 0x000AA894
		public GlobalUI GlobalUI
		{
			get
			{
				return this.m_GlobalUI;
			}
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06002027 RID: 8231 RVA: 0x000AC49C File Offset: 0x000AA89C
		public FadeManager FadeMng
		{
			get
			{
				return this.m_FadeManager;
			}
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06002028 RID: 8232 RVA: 0x000AC4A4 File Offset: 0x000AA8A4
		public LoadingUI LoadingUI
		{
			get
			{
				return this.m_LoadingUI;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06002029 RID: 8233 RVA: 0x000AC4AC File Offset: 0x000AA8AC
		public TutorialTipsUI TutorialTipsUI
		{
			get
			{
				return this.m_TutorialTipsUI;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x0600202A RID: 8234 RVA: 0x000AC4B4 File Offset: 0x000AA8B4
		public CommonMessageWindow CmnMsgWindow
		{
			get
			{
				return this.m_CommonMessageWindow;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x0600202B RID: 8235 RVA: 0x000AC4BC File Offset: 0x000AA8BC
		public ItemDetailWindow ItemDetailWindow
		{
			get
			{
				return this.m_ItemDetailWindow;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600202C RID: 8236 RVA: 0x000AC4C4 File Offset: 0x000AA8C4
		public CharaUnlockWindow CharaUnlockWindow
		{
			get
			{
				return this.m_CharaUnlockWindow;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x0600202D RID: 8237 RVA: 0x000AC4CC File Offset: 0x000AA8CC
		public ConnectingUI ConnectingIcon
		{
			get
			{
				return this.m_ConnectingUI;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x0600202E RID: 8238 RVA: 0x000AC4D4 File Offset: 0x000AA8D4
		public WebViewWindow WebView
		{
			get
			{
				return this.m_WebViewWindow;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x0600202F RID: 8239 RVA: 0x000AC4DC File Offset: 0x000AA8DC
		public UIBackGroundManager BackGroundManager
		{
			get
			{
				return this.m_BGManager;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06002030 RID: 8240 RVA: 0x000AC4E4 File Offset: 0x000AA8E4
		public TutorialMessage TutorialMessage
		{
			get
			{
				return this.m_TutorialMessage;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06002031 RID: 8241 RVA: 0x000AC4EC File Offset: 0x000AA8EC
		public UIInputBlock InputBlock
		{
			get
			{
				return this.m_UIInputBlock;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06002032 RID: 8242 RVA: 0x000AC4F4 File Offset: 0x000AA8F4
		public OverlayUIPlayerManager OverlayUIMng
		{
			get
			{
				return this.m_OverlayUIManager;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06002033 RID: 8243 RVA: 0x000AC4FC File Offset: 0x000AA8FC
		public SceneLoader SceneLoader
		{
			get
			{
				return this.m_SceneLoader;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06002034 RID: 8244 RVA: 0x000AC504 File Offset: 0x000AA904
		public Camera UICamera
		{
			get
			{
				return this.m_UICamera;
			}
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06002035 RID: 8245 RVA: 0x000AC50C File Offset: 0x000AA90C
		public Camera SaveAreaCamera
		{
			get
			{
				return this.m_SaveAreaCamera;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06002036 RID: 8246 RVA: 0x000AC514 File Offset: 0x000AA914
		public NotificationController NotificationCtrl
		{
			get
			{
				return this.m_NotificationCtrl;
			}
		}

		// Token: 0x06002037 RID: 8247 RVA: 0x000AC51C File Offset: 0x000AA91C
		protected override void Awake()
		{
			if (base.CheckInstance())
			{
				UnityEngine.Random.InitState(Environment.TickCount);
				Application.targetFrameRate = 30;
				InputTouch.Create();
				Frame.Create();
				JobQueue.Create();
				if (SceneManager.GetActiveScene().name == "Title")
				{
					this.m_IsAwakeFromStartup = true;
				}
				else
				{
					this.m_IsAwakeFromStartup = false;
				}
				this.m_NotificationCtrl.OnAwake();
			}
		}

		// Token: 0x06002038 RID: 8248 RVA: 0x000AC594 File Offset: 0x000AA994
		private void Start()
		{
			LocalSaveData.Inst.Load();
			AccessSaveData.Inst.Load();
			this.m_LoginManager = new LoginManager();
			this.m_SoundManager = new SoundManager();
			this.m_VoiceController = new VoiceController();
			this.m_MovieManager = new MovieManager();
			this.m_CRIFileInstaller = new CRIFileInstaller();
			this.m_UISettings = new UISettings();
			this.m_GlobalParameter = new GameGlobalParameter();
			this.m_DatabaseManager = new DatabaseManager();
			this.m_UserDataManager = new UserDataManager();
			this.m_TutorialManager = new TutorialManager();
			this.m_StoreManager = new StoreManager();
			this.m_QuestManager = new QuestManager();
			this.m_PresentManager = new PresentManager();
			this.m_FriendManager = new FriendManager();
			this.m_ShopManager = new ShopManager();
			this.m_MissionManager = new MissionManager();
			this.m_Gacha = new Gacha();
			this.m_CharacterManager = new CharacterManager();
			this.m_CharacterResourceManager = new CharacterResourceManager();
			this.m_TownResourceManager = new TownResourceManager();
			this.m_RoomResourceManager = new RoomObjectResourceManager();
			this.m_SpriteManager = new SpriteManager();
			this.m_GlobalUI.Setup();
			this.m_CommonMessageWindow.Setup();
			this.m_BGManager.Setup();
			base.gameObject.RemoveComponent<CriWareErrorHandler>();
			this.m_PrepareStep = GameSystem.ePrepareStep.Start;
		}

		// Token: 0x06002039 RID: 8249 RVA: 0x000AC6DC File Offset: 0x000AAADC
		private void Update()
		{
			this.m_ServerTime = this.m_ServerTime.AddSeconds((double)Time.unscaledDeltaTime);
			if (this.LoginManager != null && this.LoginManager.IsLogined && this.m_ServerTime - this.m_LastServerTime > this.m_LastCheckedTimeWithThreshold)
			{
				this.Request_ResumeGet();
				this.m_LastServerTime = this.m_ServerTime;
			}
			Frame.Instance.Update();
			InputTouch.Inst.Update();
			this.m_CRIFileInstaller.Update();
			this.m_SoundManager.Update();
			this.m_VoiceController.Update();
			this.m_MovieManager.Update();
			this.m_DatabaseManager.Update();
			this.m_UserDataManager.Update();
			this.m_StoreManager.Update();
			this.m_CharacterResourceManager.Update();
			this.m_TownResourceManager.Update();
			this.m_RoomResourceManager.Update();
			this.m_SpriteManager.Update();
			this.m_MissionManager.Update();
			this.m_CharacterManager.Update();
			this.UpdatePrepare();
			for (int i = this.m_UnloadUnusedAsyncOps.Count - 1; i >= 0; i--)
			{
				if (this.m_UnloadUnusedAsyncOps[i].isDone)
				{
					this.m_UnloadUnusedAsyncOps.RemoveAt(i);
					if (this.m_UnloadUnusedAsyncOps.Count <= 0)
					{
						GC.Collect();
					}
				}
			}
		}

		// Token: 0x0600203A RID: 8250 RVA: 0x000AC850 File Offset: 0x000AAC50
		public void PreparePDB()
		{
			ProjDepend.SetRHKBinary(PDBUtility.Load("pdb/rh.pdb"));
			ProjDepend.SetSIKBinary(PDBUtility.Load("pdb/si.pdb"));
			ProjDepend.SetRKBinary(PDBUtility.Load("pdb/rk.pdb"));
			ProjDepend.SetABKBinary(PDBUtility.Load("pdb/abk.pdb"));
			ProjDepend.SetACIVBinary(PDBUtility.Load("pdb/aciv.pdb"));
			this.m_IsCompletePreparePDB = true;
		}

		// Token: 0x0600203B RID: 8251 RVA: 0x000AC8AF File Offset: 0x000AACAF
		public bool IsCompletePreparePDB()
		{
			return this.m_IsCompletePreparePDB;
		}

		// Token: 0x0600203C RID: 8252 RVA: 0x000AC8B7 File Offset: 0x000AACB7
		public void PrepareNetwork()
		{
			this.m_IsCompletePrepareNetwork = false;
			NetworkQueueManager.SetTimeoutSec(30);
			MeigeResourceManager.SetAssetbundleServer(AssetServerSettings.assetServerURL);
			this.m_CRIFileInstaller.Setup(AssetServerSettings.assetServerURL + "CRI/");
		}

		// Token: 0x0600203D RID: 8253 RVA: 0x000AC8EB File Offset: 0x000AACEB
		public bool IsCompletePrepareNetowrk()
		{
			if (!MeigeResourceManager.isReadyAssetbundle)
			{
				return false;
			}
			if (!this.m_IsCompletePrepareNetwork)
			{
				this.m_IsCompletePrepareNetwork = true;
			}
			return true;
		}

		// Token: 0x0600203E RID: 8254 RVA: 0x000AC90C File Offset: 0x000AAD0C
		public void PrepareAssetBundles()
		{
			this.m_IsCompletePrepareAssetBundles = false;
			this.m_ResidentShaderPackResourceHandler = MeigeResourceManager.LoadHandler("residentshaderpack/residentshaderpack.muast", new MeigeResource.Option[0]);
		}

		// Token: 0x0600203F RID: 8255 RVA: 0x000AC92C File Offset: 0x000AAD2C
		public bool IsCompletePrepareAssetBundles()
		{
			bool flag = true;
			if (this.m_ResidentShaderPackResourceHandler.IsDone)
			{
				if (this.m_ResidentShaderPackResourceHandler.IsError)
				{
					MeigeResourceManager.RetryHandler(this.m_ResidentShaderPackResourceHandler);
					flag = false;
				}
			}
			else
			{
				flag = false;
			}
			if (flag)
			{
				if (!this.m_IsCompletePrepareAssetBundles)
				{
					for (int i = 0; i < this.m_ResidentShaderPackResourceHandler.assets.Length; i++)
					{
						ShaderVariantCollection shaderVariantCollection = this.m_ResidentShaderPackResourceHandler.assets[i] as ShaderVariantCollection;
						if (shaderVariantCollection != null)
						{
							shaderVariantCollection.WarmUp();
							break;
						}
					}
				}
				this.m_IsCompletePrepareAssetBundles = true;
			}
			return this.m_IsCompletePrepareAssetBundles;
		}

		// Token: 0x06002040 RID: 8256 RVA: 0x000AC9D5 File Offset: 0x000AADD5
		public void PrepareDatabase()
		{
			this.m_IsCompletePrepareDatabase = false;
			this.m_DatabaseManager.Prepare();
		}

		// Token: 0x06002041 RID: 8257 RVA: 0x000AC9EC File Offset: 0x000AADEC
		public bool IsCompletePrepareDatabase()
		{
			if (!this.m_DatabaseManager.IsPreparing() && !this.m_IsCompletePrepareDatabase)
			{
				this.m_IsCompletePrepareDatabase = true;
				this.m_EffectManager.SetupFromDB(this.DbMng.EffectListDB);
				this.m_EffectManager.SetupSound(this.m_SoundManager);
				this.m_SoundManager.Setup(this.DbMng.SoundCueSheetDB, this.DbMng.SoundSeListDB, this.DbMng.SoundBgmListDB, this.DbMng.SoundVoiceListDB, this.DbMng.NamedListDB);
				this.m_VoiceController.Setup(this.m_SoundManager);
				this.m_UISettings.Setup(LocalSaveData.Inst);
			}
			return this.m_IsCompletePrepareDatabase;
		}

		// Token: 0x06002042 RID: 8258 RVA: 0x000ACAAC File Offset: 0x000AAEAC
		public void PrepareSound(bool isRegisterInstalledAcf)
		{
			this.m_IsCompletePrepareSound = false;
			if (isRegisterInstalledAcf)
			{
				this.m_SoundManager.RegisterInstalledAcf();
			}
			int length = SoundDefine.BUILTIN_DATAS.GetLength(0);
			for (int i = 0; i < length; i++)
			{
				this.m_SoundManager.LoadCueSheet(SoundDefine.BUILTIN_DATAS[i, 0], SoundDefine.BUILTIN_DATAS[i, 1], SoundDefine.BUILTIN_DATAS[i, 2]);
			}
		}

		// Token: 0x06002043 RID: 8259 RVA: 0x000ACB1F File Offset: 0x000AAF1F
		public bool IsCompletePrepareSound()
		{
			if (!this.m_SoundManager.IsLoadingCueSheets() && !this.m_IsCompletePrepareSound)
			{
				this.m_SoundManager.ApplySaveData(LocalSaveData.Inst);
				this.m_IsCompletePrepareSound = true;
			}
			return this.m_IsCompletePrepareSound;
		}

		// Token: 0x06002044 RID: 8260 RVA: 0x000ACB5C File Offset: 0x000AAF5C
		private void UpdatePrepare()
		{
			switch (this.m_PrepareStep)
			{
			case GameSystem.ePrepareStep.Start:
				if (this.m_IsAwakeFromStartup)
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_PreparePDB;
				}
				else
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_PreparePDB;
				}
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_PreparePDB:
				if (!this.IsCompletePreparePDB())
				{
					this.PreparePDB();
				}
				if (this.m_IsAwakeFromStartup)
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.End;
				}
				else
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_PrepareNetwork;
				}
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_PrepareNetwork:
				this.PrepareNetwork();
				this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_PrepareNetworkWait;
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_PrepareNetworkWait:
				if (this.IsCompletePrepareNetowrk())
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_AB;
				}
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_AB:
				this.PrepareAssetBundles();
				this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_ABWait;
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_ABWait:
				if (this.IsCompletePrepareAssetBundles())
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_Database;
				}
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_Database:
				this.PrepareDatabase();
				this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_DatabaseWait;
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_DatabaseWait:
				if (this.IsCompletePrepareDatabase())
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_Sound;
				}
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_Sound:
				this.PrepareSound(false);
				this.m_SoundManager.LoadCueSheet("BGM", "BGM.acb", "BGM.awb");
				this.m_PrepareStep = GameSystem.ePrepareStep.PreLogin_NonStartup_SoundWait;
				break;
			case GameSystem.ePrepareStep.PreLogin_NonStartup_SoundWait:
				if (this.IsCompletePrepareSound())
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.AutoLogin;
				}
				break;
			case GameSystem.ePrepareStep.AutoLogin:
			{
				bool flag = this.LoginManager.Request_Login(new Action<MeigewwwParam, PlayerResponseTypes.Login>(this.OnResponse_Login), new Action<MeigewwwParam, PlayerResponseTypes.Getall>(this.OnResponse_GetAll));
				if (flag)
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.AutoLogin_Wait;
				}
				else
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.AutoSignup;
				}
				break;
			}
			case GameSystem.ePrepareStep.PostAutoLogin_QuestGetAll:
				SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestGetAll(new Action(this.OnResponse_QuestGetAll), true);
				this.m_PrepareStep = GameSystem.ePrepareStep.PostAutoLogin_QuestGetAllWait;
				break;
			case GameSystem.ePrepareStep.PostAutoLogin_Mission:
				this.m_MissionManager.Prepare();
				this.m_PrepareStep = GameSystem.ePrepareStep.PostAutoLogin_MissionWait;
				break;
			case GameSystem.ePrepareStep.PostAutoLogin_MissionWait:
				if (this.m_MissionManager.IsDonePrepare())
				{
					this.m_PrepareStep = GameSystem.ePrepareStep.PostAutoLogin_End;
				}
				break;
			case GameSystem.ePrepareStep.PostAutoLogin_End:
				this.m_PrepareStep = GameSystem.ePrepareStep.End;
				break;
			case GameSystem.ePrepareStep.AutoSignup:
				this.LoginManager.Request_Signup("FULLOPEN", string.Empty, new Action<MeigewwwParam, PlayerResponseTypes.Signup>(this.OnResponse_Signup));
				this.m_PrepareStep = GameSystem.ePrepareStep.AutoSignup_Wait;
				break;
			}
		}

		// Token: 0x06002045 RID: 8261 RVA: 0x000ACDF0 File Offset: 0x000AB1F0
		private void OnResponse_Login(MeigewwwParam wwwParam, PlayerResponseTypes.Login param)
		{
			if (param == null)
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.None;
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.AutoSignup;
			}
		}

		// Token: 0x06002046 RID: 8262 RVA: 0x000ACE30 File Offset: 0x000AB230
		private void OnResponse_GetAll(MeigewwwParam wwwParam, PlayerResponseTypes.Getall param)
		{
			if (param == null)
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.None;
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowNone(result.ToMessageString());
			}
			else
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.PostAutoLogin_QuestGetAll;
			}
		}

		// Token: 0x06002047 RID: 8263 RVA: 0x000ACE7A File Offset: 0x000AB27A
		private void OnResponse_QuestGetAll()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestChapterGetAll(new Action(this.OnResponse_QuestChapterGetAll), true);
		}

		// Token: 0x06002048 RID: 8264 RVA: 0x000ACE99 File Offset: 0x000AB299
		private void OnResponse_QuestChapterGetAll()
		{
			this.m_PrepareStep = GameSystem.ePrepareStep.PostAutoLogin_Mission;
		}

		// Token: 0x06002049 RID: 8265 RVA: 0x000ACEA4 File Offset: 0x000AB2A4
		private void OnResponse_Signup(MeigewwwParam wwwParam, PlayerResponseTypes.Signup param)
		{
			if (param == null)
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.None;
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowNone(result.ToMessageString());
			}
			else
			{
				this.m_PrepareStep = GameSystem.ePrepareStep.AutoLogin;
			}
		}

		// Token: 0x0600204A RID: 8266 RVA: 0x000ACEEE File Offset: 0x000AB2EE
		private void LateUpdate()
		{
			Frame.Instance.LateUpdate();
			if (this.m_IsRequestedUnloadUnusedAsync)
			{
				this.m_IsRequestedUnloadUnusedAsync = false;
				this.m_UnloadUnusedAsyncOps.Add(Resources.UnloadUnusedAssets());
			}
		}

		// Token: 0x0600204B RID: 8267 RVA: 0x000ACF1C File Offset: 0x000AB31C
		protected override void OnDestroy()
		{
			if (this == SingletonMonoBehaviour<GameSystem>.Inst)
			{
				if (this.m_SoundManager != null)
				{
					this.m_SoundManager.Destroy();
				}
				if (this.m_MovieManager != null)
				{
					this.m_MovieManager.Destroy();
				}
				if (this.m_CRIFileInstaller != null)
				{
					this.m_CRIFileInstaller.Destroy();
				}
				if (this.m_TutorialTipsUI != null)
				{
					this.m_TutorialTipsUI.Destroy();
				}
				JobQueue.Release();
				MeigeResourceManager.UnloadHandlerAll();
			}
			base.OnDestroy();
		}

		// Token: 0x0600204C RID: 8268 RVA: 0x000ACFA7 File Offset: 0x000AB3A7
		private void OnApplicationQuit()
		{
			PreviewLabs.PlayerPrefs.Flush();
		}

		// Token: 0x0600204D RID: 8269 RVA: 0x000ACFAE File Offset: 0x000AB3AE
		private void OnApplicationPause(bool isPause)
		{
			if (isPause)
			{
				this.OnSuspend();
			}
			else
			{
				this.OnResume();
			}
		}

		// Token: 0x0600204E RID: 8270 RVA: 0x000ACFC7 File Offset: 0x000AB3C7
		private void OnSuspend()
		{
			if (this.m_IsSuspended)
			{
				return;
			}
			this.m_IsSuspended = true;
		}

		// Token: 0x0600204F RID: 8271 RVA: 0x000ACFDC File Offset: 0x000AB3DC
		private void OnResume()
		{
			if (!this.m_IsSuspended)
			{
				return;
			}
			this.m_IsSuspended = false;
			this.Request_ResumeGet();
		}

		// Token: 0x06002050 RID: 8272 RVA: 0x000ACFF8 File Offset: 0x000AB3F8
		public void Request_ResumeGet()
		{
			if (!this.IsAvailable())
			{
				return;
			}
			MeigewwwParam wwwParam = PlayerRequest.Resumeget(new PlayerRequestTypes.Resumeget
			{
				playerId = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID
			}, new MeigewwwParam.Callback(this.OnResponse_ResumeGet));
			NetworkQueueManager.Request(wwwParam);
		}

		// Token: 0x06002051 RID: 8273 RVA: 0x000AD04C File Offset: 0x000AB44C
		private void OnResponse_ResumeGet(MeigewwwParam wwwParam)
		{
			if (wwwParam.IsDone())
			{
				if (!wwwParam.IsSystemError())
				{
					string responseJson = wwwParam.GetResponseJson();
					PlayerResponseTypes.Resumeget resumeget = ResponseCommon.Deserialize<PlayerResponseTypes.Resumeget>(responseJson);
					if (resumeget != null)
					{
						ResultCode result = resumeget.GetResult();
						if (result == ResultCode.SUCCESS)
						{
							UserData userData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData;
							userData.Stamina.SetValueMax(resumeget.staminaMax);
							userData.Stamina.SetValue(resumeget.stamina, false);
							userData.Stamina.SetRemainSecMax((float)resumeget.recastTimeMax);
							userData.Stamina.SetRemainSec((float)resumeget.recastTime);
						}
					}
				}
			}
		}

		// Token: 0x06002052 RID: 8274 RVA: 0x000AD0F6 File Offset: 0x000AB4F6
		private bool IsPreparing()
		{
			return this.m_PrepareStep != GameSystem.ePrepareStep.End;
		}

		// Token: 0x06002053 RID: 8275 RVA: 0x000AD108 File Offset: 0x000AB508
		public bool IsAvailable()
		{
			return !this.IsPreparing() && this.m_GlobalParameter != null && this.m_DatabaseManager != null && this.m_QuestManager != null && this.m_FriendManager != null && this.m_ShopManager != null && this.m_Gacha != null && this.m_UserDataManager != null && this.m_UserDataManager.IsAvailable() && this.m_CharacterManager != null && this.m_CharacterManager.IsAvailable() && this.m_CharacterResourceManager != null && this.m_CharacterResourceManager.IsAvailable() && this.m_TownResourceManager != null && this.m_TownResourceManager.IsAvailable() && this.m_RoomResourceManager != null && this.m_RoomResourceManager.IsAvailable() && this.m_SpriteManager != null && !(this.m_EffectManager == null);
		}

		// Token: 0x06002054 RID: 8276 RVA: 0x000AD222 File Offset: 0x000AB622
		public void RequesetUnloadUnusedAssets(bool isGCCollect = true)
		{
			if (this.m_IsRequestedUnloadUnusedAsync)
			{
				return;
			}
			this.m_IsRequestedUnloadUnusedAsync = true;
		}

		// Token: 0x06002055 RID: 8277 RVA: 0x000AD237 File Offset: 0x000AB637
		public bool IsCompleteUnloadUnusedAssets()
		{
			return !this.m_IsRequestedUnloadUnusedAsync && this.m_UnloadUnusedAsyncOps.Count <= 0;
		}

		// Token: 0x06002056 RID: 8278 RVA: 0x000AD258 File Offset: 0x000AB658
		public void RegisterPushNotificationService()
		{
			if (!this.m_IsRegisteredPushNotificationService && this.m_PushNotificationService != null)
			{
				this.m_PushNotificationService.RegisterDevice();
				this.m_IsRegisteredPushNotificationService = true;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06002057 RID: 8279 RVA: 0x000AD288 File Offset: 0x000AB688
		public CharacterHandler CharaHndlForMenu
		{
			get
			{
				return this.m_CharaHndlForMenu;
			}
		}

		// Token: 0x06002058 RID: 8280 RVA: 0x000AD290 File Offset: 0x000AB690
		public CharacterHandler PrepareCharaHndlForMenu(int charaID, int weaponID, Transform parent = null)
		{
			this.DestroyCharaHndlForMenu();
			CharacterListDB_Param param = this.DbMng.CharaListDB.GetParam(charaID);
			CharacterParam characterParam = new CharacterParam();
			CharacterUtility.SetupCharacterParam(characterParam, -1L, charaID, 1, 99, 0L, 0);
			WeaponParam srcWeaponParam = null;
			this.m_CharaHndlForMenu = this.CharaMng.InstantiateCharacter(characterParam, null, srcWeaponParam, eCharaResourceType.Player, param.m_ResourceID, CharacterDefine.eMode.Menu, true, CharacterDefine.eFriendType.None, "CHARA_MENU", parent);
			if (this.m_CharaHndlForMenu != null)
			{
				this.m_CharaHndlForMenu.Prepare();
				this.m_CharaHndlForMenu.CacheTransform.position = new Vector3(0f, 10000f, 0f);
			}
			return this.m_CharaHndlForMenu;
		}

		// Token: 0x06002059 RID: 8281 RVA: 0x000AD338 File Offset: 0x000AB738
		public CharacterHandler PrepareCharaHndlForMenu(long charaMngID, Transform parent = null)
		{
			this.DestroyCharaHndlForMenu();
			UserCharacterData userCharaData = this.UserDataMng.GetUserCharaData(charaMngID);
			if (userCharaData == null)
			{
				return null;
			}
			CharacterParam param = userCharaData.Param;
			CharacterListDB_Param param2 = this.DbMng.CharaListDB.GetParam(param.CharaID);
			WeaponParam srcWeaponParam = null;
			this.m_CharaHndlForMenu = this.CharaMng.InstantiateCharacter(param, null, srcWeaponParam, eCharaResourceType.Player, param2.m_ResourceID, CharacterDefine.eMode.Menu, true, CharacterDefine.eFriendType.None, "CHARA_MENU", parent);
			if (this.m_CharaHndlForMenu != null)
			{
				this.m_CharaHndlForMenu.Prepare();
				this.m_CharaHndlForMenu.CacheTransform.position = new Vector3(0f, 10000f, 0f);
			}
			return this.m_CharaHndlForMenu;
		}

		// Token: 0x0600205A RID: 8282 RVA: 0x000AD3EB File Offset: 0x000AB7EB
		public void DestroyCharaHndlForMenu()
		{
			if (this.m_CharaHndlForMenu != null)
			{
				this.CharaMng.DestroyChara(this.m_CharaHndlForMenu);
				this.m_CharaHndlForMenu = null;
			}
		}

		// Token: 0x0600205B RID: 8283 RVA: 0x000AD416 File Offset: 0x000AB816
		public void SuspendCharaHndlForMenu()
		{
			if (this.m_CharaHndlForMenu != null)
			{
				this.m_CharaHndlForMenu.SetEnableRender(false);
			}
		}

		// Token: 0x0600205C RID: 8284 RVA: 0x000AD435 File Offset: 0x000AB835
		public void ResumeCharaHndlForMenu()
		{
			if (this.m_CharaHndlForMenu != null)
			{
				this.m_CharaHndlForMenu.SetEnableRender(true);
			}
		}

		// Token: 0x0600205D RID: 8285 RVA: 0x000AD454 File Offset: 0x000AB854
		[Conditional("APP_DEBUG")]
		public void DebugLogCrashReport()
		{
			CrashReport[] reports = CrashReport.reports;
			if (reports != null)
			{
				UnityEngine.Debug.Log("Crash reports (" + reports.Length + ")");
				foreach (CrashReport crashReport in reports)
				{
					UnityEngine.Debug.Log(string.Concat(new object[]
					{
						" Crash: ",
						crashReport.time,
						" >> ",
						crashReport.text
					}));
				}
			}
			else
			{
				UnityEngine.Debug.Log("Crash reports is not found.");
			}
		}

		// Token: 0x04002670 RID: 9840
		private DateTime m_ServerTime;

		// Token: 0x04002671 RID: 9841
		private DateTime m_LastServerTime;

		// Token: 0x04002672 RID: 9842
		private LoginManager m_LoginManager;

		// Token: 0x04002673 RID: 9843
		private GameGlobalParameter m_GlobalParameter;

		// Token: 0x04002674 RID: 9844
		private DatabaseManager m_DatabaseManager;

		// Token: 0x04002675 RID: 9845
		private UserDataManager m_UserDataManager;

		// Token: 0x04002676 RID: 9846
		private TutorialManager m_TutorialManager;

		// Token: 0x04002677 RID: 9847
		private StoreManager m_StoreManager;

		// Token: 0x04002678 RID: 9848
		private QuestManager m_QuestManager;

		// Token: 0x04002679 RID: 9849
		private PresentManager m_PresentManager;

		// Token: 0x0400267A RID: 9850
		private FriendManager m_FriendManager;

		// Token: 0x0400267B RID: 9851
		private ShopManager m_ShopManager;

		// Token: 0x0400267C RID: 9852
		private MissionManager m_MissionManager;

		// Token: 0x0400267D RID: 9853
		private Gacha m_Gacha;

		// Token: 0x0400267E RID: 9854
		private CharacterManager m_CharacterManager;

		// Token: 0x0400267F RID: 9855
		private CharacterResourceManager m_CharacterResourceManager;

		// Token: 0x04002680 RID: 9856
		private TownResourceManager m_TownResourceManager;

		// Token: 0x04002681 RID: 9857
		private RoomObjectResourceManager m_RoomResourceManager;

		// Token: 0x04002682 RID: 9858
		private SpriteManager m_SpriteManager;

		// Token: 0x04002683 RID: 9859
		private SoundManager m_SoundManager;

		// Token: 0x04002684 RID: 9860
		private VoiceController m_VoiceController;

		// Token: 0x04002685 RID: 9861
		private MovieManager m_MovieManager;

		// Token: 0x04002686 RID: 9862
		private CRIFileInstaller m_CRIFileInstaller;

		// Token: 0x04002687 RID: 9863
		private UISettings m_UISettings;

		// Token: 0x04002688 RID: 9864
		[SerializeField]
		private EffectManager m_EffectManager;

		// Token: 0x04002689 RID: 9865
		[SerializeField]
		private GlobalUI m_GlobalUI;

		// Token: 0x0400268A RID: 9866
		[SerializeField]
		private FadeManager m_FadeManager;

		// Token: 0x0400268B RID: 9867
		[SerializeField]
		private LoadingUI m_LoadingUI;

		// Token: 0x0400268C RID: 9868
		[SerializeField]
		private TutorialTipsUI m_TutorialTipsUI;

		// Token: 0x0400268D RID: 9869
		[SerializeField]
		private CommonMessageWindow m_CommonMessageWindow;

		// Token: 0x0400268E RID: 9870
		[SerializeField]
		private ItemDetailWindow m_ItemDetailWindow;

		// Token: 0x0400268F RID: 9871
		[SerializeField]
		private CharaUnlockWindow m_CharaUnlockWindow;

		// Token: 0x04002690 RID: 9872
		[SerializeField]
		private ConnectingUI m_ConnectingUI;

		// Token: 0x04002691 RID: 9873
		[SerializeField]
		private WebViewWindow m_WebViewWindow;

		// Token: 0x04002692 RID: 9874
		[SerializeField]
		private UIBackGroundManager m_BGManager;

		// Token: 0x04002693 RID: 9875
		[SerializeField]
		private TutorialMessage m_TutorialMessage;

		// Token: 0x04002694 RID: 9876
		[SerializeField]
		private UIInputBlock m_UIInputBlock;

		// Token: 0x04002695 RID: 9877
		[SerializeField]
		private OverlayUIPlayerManager m_OverlayUIManager;

		// Token: 0x04002696 RID: 9878
		[SerializeField]
		private SceneLoader m_SceneLoader;

		// Token: 0x04002697 RID: 9879
		[SerializeField]
		private Camera m_UICamera;

		// Token: 0x04002698 RID: 9880
		[SerializeField]
		private Camera m_SaveAreaCamera;

		// Token: 0x04002699 RID: 9881
		[SerializeField]
		private PushNotificationService m_PushNotificationService;

		// Token: 0x0400269A RID: 9882
		private NotificationController m_NotificationCtrl = new NotificationController();

		// Token: 0x0400269B RID: 9883
		private bool m_IsRegisteredPushNotificationService;

		// Token: 0x0400269C RID: 9884
		private List<AsyncOperation> m_UnloadUnusedAsyncOps = new List<AsyncOperation>();

		// Token: 0x0400269D RID: 9885
		private bool m_IsRequestedUnloadUnusedAsync;

		// Token: 0x0400269E RID: 9886
		private MeigeResource.Handler m_ResidentShaderPackResourceHandler;

		// Token: 0x0400269F RID: 9887
		private const string STARTUP_SCENE = "Title";

		// Token: 0x040026A0 RID: 9888
		private bool m_IsAwakeFromStartup;

		// Token: 0x040026A1 RID: 9889
		private bool m_IsSuspended;

		// Token: 0x040026A2 RID: 9890
		private bool m_IsCompletePrepareNetwork;

		// Token: 0x040026A3 RID: 9891
		private bool m_IsCompletePrepareAssetBundles;

		// Token: 0x040026A4 RID: 9892
		private bool m_IsCompletePrepareDatabase;

		// Token: 0x040026A5 RID: 9893
		private bool m_IsCompletePrepareSound;

		// Token: 0x040026A6 RID: 9894
		private bool m_IsCompletePreparePDB;

		// Token: 0x040026A7 RID: 9895
		private GameSystem.ePrepareStep m_PrepareStep = GameSystem.ePrepareStep.None;

		// Token: 0x040026A8 RID: 9896
		private readonly TimeSpan m_LastCheckedTimeWithThreshold = new TimeSpan(1, 0, 0);

		// Token: 0x040026A9 RID: 9897
		private CharacterHandler m_CharaHndlForMenu;

		// Token: 0x0200064B RID: 1611
		private enum ePrepareStep
		{
			// Token: 0x040026AB RID: 9899
			None = -1,
			// Token: 0x040026AC RID: 9900
			Start,
			// Token: 0x040026AD RID: 9901
			PreLogin_NonStartup_PreparePDB,
			// Token: 0x040026AE RID: 9902
			PreLogin_NonStartup_PrepareNetwork,
			// Token: 0x040026AF RID: 9903
			PreLogin_NonStartup_PrepareNetworkWait,
			// Token: 0x040026B0 RID: 9904
			PreLogin_NonStartup_AB,
			// Token: 0x040026B1 RID: 9905
			PreLogin_NonStartup_ABWait,
			// Token: 0x040026B2 RID: 9906
			PreLogin_NonStartup_Database,
			// Token: 0x040026B3 RID: 9907
			PreLogin_NonStartup_DatabaseWait,
			// Token: 0x040026B4 RID: 9908
			PreLogin_NonStartup_Sound,
			// Token: 0x040026B5 RID: 9909
			PreLogin_NonStartup_SoundWait,
			// Token: 0x040026B6 RID: 9910
			AutoLogin,
			// Token: 0x040026B7 RID: 9911
			AutoLogin_Wait,
			// Token: 0x040026B8 RID: 9912
			PostAutoLogin_QuestGetAll,
			// Token: 0x040026B9 RID: 9913
			PostAutoLogin_QuestGetAllWait,
			// Token: 0x040026BA RID: 9914
			PostAutoLogin_Mission,
			// Token: 0x040026BB RID: 9915
			PostAutoLogin_MissionWait,
			// Token: 0x040026BC RID: 9916
			PostAutoLogin_End,
			// Token: 0x040026BD RID: 9917
			AutoSignup,
			// Token: 0x040026BE RID: 9918
			AutoSignup_Wait,
			// Token: 0x040026BF RID: 9919
			End
		}
	}
}
