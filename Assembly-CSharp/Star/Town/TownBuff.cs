﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star.Town
{
	// Token: 0x020006F4 RID: 1780
	[Serializable]
	public class TownBuff
	{
		// Token: 0x0600234B RID: 9035 RVA: 0x000BD668 File Offset: 0x000BBA68
		public static TownBuff CalcTownBuff()
		{
			TownBuff townBuff = new TownBuff();
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			TownObjectListDB townObjListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB;
			for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				int num = buildObjectDataAt.m_ObjID;
				num = TownUtility.CalcAreaToContentID(num);
				if (buildObjectDataAt.m_Lv != 0)
				{
					TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(num);
					TownObjectBuffDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjBuffDB.GetParam(param.m_BuffParamID);
					int lv = buildObjectDataAt.m_Lv;
					if (lv > 0)
					{
						for (int j = 0; j < param2.m_Datas.Length; j++)
						{
							TownBuff.BuffData buffData = new TownBuff.BuffData();
							buffData.m_CondType = (eTownObjectBuffTargetConditionType)param2.m_Datas[j].m_TargetConditionType;
							buffData.m_Cond = param2.m_Datas[j].m_TargetCondition;
							buffData.m_Param.m_Type = (eTownObjectBuffType)param2.m_Datas[j].m_BuffType;
							if (param2.m_Datas[j].m_Value.Length > lv - 1)
							{
								buffData.m_Param.m_Val = param2.m_Datas[j].m_Value[lv - 1];
							}
							else
							{
								Debug.LogWarning(string.Concat(new object[]
								{
									"not found TownObjectBuffDB_Param BuffParamID=",
									param.m_BuffParamID,
									" Lv=",
									(lv - 1).ToString()
								}));
							}
							townBuff.AddBuffData(buffData);
						}
					}
				}
			}
			return townBuff;
		}

		// Token: 0x0600234C RID: 9036 RVA: 0x000BD831 File Offset: 0x000BBC31
		private void AddBuffData(TownBuff.BuffData data)
		{
			this.m_List.Add(data);
		}

		// Token: 0x0600234D RID: 9037 RVA: 0x000BD840 File Offset: 0x000BBC40
		public static long CalcTownGold()
		{
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			long num = 0L;
			for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
			{
				UserTownData.BuildObjectData buildObjectDataAt = userTownData.GetBuildObjectDataAt(i);
				int objID = buildObjectDataAt.m_ObjID;
				if (buildObjectDataAt.m_IsOpen)
				{
					TownItemDropDatabase.DropItemTool keyIDToDatabase = TownItemDropDatabase.GetKeyIDToDatabase(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_LevelUpListID);
					if (keyIDToDatabase != null)
					{
						int lv = buildObjectDataAt.m_Lv;
						if (lv > 0)
						{
							for (int j = 0; j < keyIDToDatabase.m_Table.Length; j++)
							{
								if (keyIDToDatabase.m_Table[j].m_Level == lv)
								{
									int resultNo = keyIDToDatabase.m_Table[j].m_Table[0].m_ResultNo;
									UserScheduleData.DropState fieldDropItems = UserTownUtil.GetFieldDropItems(resultNo);
									if (fieldDropItems != null && fieldDropItems.m_Category == UserScheduleData.eDropCategory.Money)
									{
										num += (long)fieldDropItems.m_Num;
									}
									break;
								}
							}
						}
					}
				}
			}
			return num;
		}

		// Token: 0x0600234E RID: 9038 RVA: 0x000BD95C File Offset: 0x000BBD5C
		public List<TownBuff.BuffParam> GetBuffData(eTownObjectBuffTargetConditionType conditionType, int condition)
		{
			List<TownBuff.BuffParam> list = new List<TownBuff.BuffParam>();
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].m_CondType == conditionType && this.m_List[i].m_Cond == condition)
				{
					bool flag = false;
					for (int j = 0; j < list.Count; j++)
					{
						if (list[j].m_Type == this.m_List[i].m_Param.m_Type)
						{
							flag = true;
							list[j].m_Val += this.m_List[i].m_Param.m_Val;
							break;
						}
					}
					if (!flag)
					{
						list.Add(new TownBuff.BuffParam
						{
							m_Type = this.m_List[i].m_Param.m_Type,
							m_Val = this.m_List[i].m_Param.m_Val
						});
					}
				}
			}
			return list;
		}

		// Token: 0x0600234F RID: 9039 RVA: 0x000BDA7C File Offset: 0x000BBE7C
		public List<TownBuff.BuffParam> GetTypeData(eTownObjectBuffTargetConditionType conditionType, eTownObjectBuffType buffType)
		{
			List<TownBuff.BuffParam> list = new List<TownBuff.BuffParam>();
			for (int i = 0; i < this.m_List.Count; i++)
			{
				if (this.m_List[i].m_CondType == conditionType && this.m_List[i].m_Param.m_Type == buffType)
				{
					bool flag = false;
					for (int j = 0; j < list.Count; j++)
					{
						if (list[j].m_Type == this.m_List[i].m_Param.m_Type)
						{
							flag = true;
							list[j].m_Val += this.m_List[i].m_Param.m_Val;
							break;
						}
					}
					if (!flag)
					{
						list.Add(new TownBuff.BuffParam
						{
							m_Type = this.m_List[i].m_Param.m_Type,
							m_Val = this.m_List[i].m_Param.m_Val
						});
					}
				}
			}
			return list;
		}

		// Token: 0x06002350 RID: 9040 RVA: 0x000BDBA0 File Offset: 0x000BBFA0
		public List<TownBuff.BuffParam> GetBuffParam(eTitleType titleType, eClassType classType, eElementType elementType)
		{
			List<TownBuff.BuffParam> list = new List<TownBuff.BuffParam>();
			for (int i = 0; i < this.m_List.Count; i++)
			{
				bool flag = false;
				if (this.m_List[i].m_CondType == eTownObjectBuffTargetConditionType.None)
				{
					flag = true;
				}
				else if (this.m_List[i].m_CondType == eTownObjectBuffTargetConditionType.TitleType && this.m_List[i].m_Cond == (int)titleType)
				{
					flag = true;
				}
				else if (this.m_List[i].m_CondType == eTownObjectBuffTargetConditionType.ClassType && this.m_List[i].m_Cond == (int)classType)
				{
					flag = true;
				}
				else if (this.m_List[i].m_CondType == eTownObjectBuffTargetConditionType.ElementType && this.m_List[i].m_Cond == (int)elementType)
				{
					flag = true;
				}
				if (flag)
				{
					bool flag2 = false;
					for (int j = 0; j < list.Count; j++)
					{
						if (list[j].m_Type == this.m_List[i].m_Param.m_Type)
						{
							flag2 = true;
							list[j].m_Val += this.m_List[i].m_Param.m_Val;
							break;
						}
					}
					if (!flag2)
					{
						list.Add(new TownBuff.BuffParam
						{
							m_Type = this.m_List[i].m_Param.m_Type,
							m_Val = this.m_List[i].m_Param.m_Val
						});
					}
				}
			}
			return list;
		}

		// Token: 0x06002351 RID: 9041 RVA: 0x000BDD54 File Offset: 0x000BC154
		public float[] GetStatusBuff(eTitleType titleType, eClassType classType, eElementType elementType)
		{
			float[] array = new float[6];
			List<TownBuff.BuffParam> buffParam = this.GetBuffParam(titleType, classType, elementType);
			int i = 0;
			while (i < buffParam.Count)
			{
				EditUtility.eState eState;
				switch (buffParam[i].m_Type)
				{
				case eTownObjectBuffType.Hp:
					eState = EditUtility.eState.Hp;
					goto IL_7B;
				case eTownObjectBuffType.Atk:
					eState = EditUtility.eState.Atk;
					goto IL_7B;
				case eTownObjectBuffType.Mgc:
					eState = EditUtility.eState.Mgc;
					goto IL_7B;
				case eTownObjectBuffType.Def:
					eState = EditUtility.eState.Def;
					goto IL_7B;
				case eTownObjectBuffType.MDef:
					eState = EditUtility.eState.MDef;
					goto IL_7B;
				case eTownObjectBuffType.Spd:
					eState = EditUtility.eState.Spd;
					goto IL_7B;
				}
				IL_92:
				i++;
				continue;
				IL_7B:
				array[(int)eState] += buffParam[i].m_Val;
				goto IL_92;
			}
			return array;
		}

		// Token: 0x06002352 RID: 9042 RVA: 0x000BDE04 File Offset: 0x000BC204
		public float[] GetBuffParamArray(eTitleType titleType, eClassType classType, eElementType elementType)
		{
			float[] array = new float[13];
			List<TownBuff.BuffParam> buffParam = this.GetBuffParam(titleType, classType, elementType);
			for (int i = 0; i < buffParam.Count; i++)
			{
				array[(int)buffParam[i].m_Type] += buffParam[i].m_Val;
			}
			return array;
		}

		// Token: 0x04002A19 RID: 10777
		[SerializeField]
		private List<TownBuff.BuffData> m_List = new List<TownBuff.BuffData>();

		// Token: 0x020006F5 RID: 1781
		[Serializable]
		public class BuffParam
		{
			// Token: 0x04002A1A RID: 10778
			public eTownObjectBuffType m_Type;

			// Token: 0x04002A1B RID: 10779
			public float m_Val;
		}

		// Token: 0x020006F6 RID: 1782
		[Serializable]
		public class BuffData
		{
			// Token: 0x04002A1C RID: 10780
			public eTownObjectBuffTargetConditionType m_CondType;

			// Token: 0x04002A1D RID: 10781
			public int m_Cond;

			// Token: 0x04002A1E RID: 10782
			public TownBuff.BuffParam m_Param = new TownBuff.BuffParam();
		}
	}
}
