﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000115 RID: 277
	[Serializable]
	public class SkillActionEvent_EffectProjectile_Straight : SkillActionEvent_Base
	{
		// Token: 0x04000705 RID: 1797
		public string m_CallbackKey;

		// Token: 0x04000706 RID: 1798
		public string m_EffectID;

		// Token: 0x04000707 RID: 1799
		public short m_UniqueID;

		// Token: 0x04000708 RID: 1800
		public float m_Speed;

		// Token: 0x04000709 RID: 1801
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x0400070A RID: 1802
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x0400070B RID: 1803
		public Vector2 m_StartPosOffset;

		// Token: 0x0400070C RID: 1804
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x0400070D RID: 1805
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x0400070E RID: 1806
		public Vector2 m_TargetPosOffset;
	}
}
