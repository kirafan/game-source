﻿using System;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using Star.UI.Battle;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020003E5 RID: 997
	public class BattleState_Result : BattleState
	{
		// Token: 0x060012F1 RID: 4849 RVA: 0x00065276 File Offset: 0x00063676
		public BattleState_Result(BattleMain owner) : base(owner)
		{
		}

		// Token: 0x060012F2 RID: 4850 RVA: 0x00065286 File Offset: 0x00063686
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x060012F3 RID: 4851 RVA: 0x00065289 File Offset: 0x00063689
		public override void OnStateEnter()
		{
			this.m_Step = BattleState_Result.eStep.First;
		}

		// Token: 0x060012F4 RID: 4852 RVA: 0x00065292 File Offset: 0x00063692
		public override void OnStateExit()
		{
		}

		// Token: 0x060012F5 RID: 4853 RVA: 0x00065294 File Offset: 0x00063694
		public override void OnDispose()
		{
			this.m_ResultUI = null;
		}

		// Token: 0x060012F6 RID: 4854 RVA: 0x000652A0 File Offset: 0x000636A0
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case BattleState_Result.eStep.First:
			{
				bool flag;
				if (this.m_Owner.System.QuestResult.m_Status == BattleResult.eStatus.Clear)
				{
					this.m_Owner.System.CalcResultPreRequest();
					flag = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestSuccess(this.m_Owner.System.PlayerParty, new Action<MeigewwwParam, Questlogset>(this.OnResponse_QuestSuccess));
					SingletonMonoBehaviour<RewardPlayer>.Inst.SetReserveOpen(true);
				}
				else
				{
					flag = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestFailed(this.m_Owner.System.PlayerParty, new Action<MeigewwwParam, Questlogset>(this.OnResponse_QuestFailed));
				}
				if (flag)
				{
					this.m_Step = BattleState_Result.eStep.Request_Wait;
				}
				else
				{
					this.m_Step = BattleState_Result.eStep.RequestPostProcess;
				}
				break;
			}
			case BattleState_Result.eStep.RequestPostProcess:
				for (int i = 0; i < 5; i++)
				{
					CharacterHandler memberAt = this.m_Owner.System.PlayerParty.GetMemberAt(i);
					if (memberAt != null && !memberAt.IsDonePreparePerfect())
					{
						memberAt.PrepareWithoutResourceSetup(false);
					}
				}
				this.m_Owner.System.SystemDestoryOnlyUI();
				this.m_Step = BattleState_Result.eStep.RequestPostProcess_Wait;
				break;
			case BattleState_Result.eStep.RequestPostProcess_Wait:
			{
				bool flag2 = true;
				for (int j = 0; j < 5; j++)
				{
					CharacterHandler memberAt2 = this.m_Owner.System.PlayerParty.GetMemberAt(j);
					if (memberAt2 != null && !memberAt2.IsDonePreparePerfect())
					{
						flag2 = false;
						break;
					}
				}
				if (flag2)
				{
					if (this.m_Owner.System.IsComplteDestoryOnlyUI())
					{
						if (this.m_Owner.System.QuestResult.m_Status == BattleResult.eStatus.Clear)
						{
							this.m_Step = BattleState_Result.eStep.ResultLoad;
						}
						else
						{
							this.m_Step = BattleState_Result.eStep.FailedTips;
						}
					}
				}
				break;
			}
			case BattleState_Result.eStep.ResultLoad:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(SceneDefine.eChildSceneID.BattleResultUI, true);
				this.m_Step = BattleState_Result.eStep.ResultLoad_Wait;
				break;
			case BattleState_Result.eStep.ResultLoad_Wait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(SceneDefine.eChildSceneID.BattleResultUI))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(SceneDefine.eChildSceneID.BattleResultUI);
					this.m_Step = BattleState_Result.eStep.ResultPlayIn;
				}
				break;
			case BattleState_Result.eStep.ResultPlayIn:
				if (this.SetupAndPlay_Result())
				{
					this.m_Step = BattleState_Result.eStep.Result_Wait;
				}
				break;
			case BattleState_Result.eStep.Result_Wait:
				if (this.m_ResultUI.IsMenuEnd())
				{
					this.m_Step = BattleState_Result.eStep.FriendPropose;
				}
				break;
			case BattleState_Result.eStep.FriendPropose:
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestFriendData == null || SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestFriendData.m_Type != FriendDefine.eType.Guest)
				{
					this.m_Step = BattleState_Result.eStep.StoreReview;
				}
				else
				{
					this.m_ResultUI.ProposeGroup.Open(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.QuestFriendData);
					this.m_Step = BattleState_Result.eStep.FriendProposeWait;
				}
				break;
			case BattleState_Result.eStep.FriendProposeWait:
				if (!this.m_ResultUI.ProposeGroup.IsOpen())
				{
					this.m_Step = BattleState_Result.eStep.StoreReview;
				}
				break;
			case BattleState_Result.eStep.StoreReview:
			{
				BattleResult result = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult();
				if (this.m_ResultUI.StoreReviewController.CheckConditionFull(result.m_BeforeUserLv, result.m_AfterUserLv, result.m_QuestID))
				{
					bool flag3 = this.m_ResultUI.StoreReviewController.Review();
					if (flag3)
					{
						this.m_Step = BattleState_Result.eStep.StoreReview_NativeOpenWait;
					}
					else
					{
						this.m_Step = BattleState_Result.eStep.StoreReview_AppOpenWait;
					}
				}
				else
				{
					this.m_Step = BattleState_Result.eStep.CheckBranch;
				}
				break;
			}
			case BattleState_Result.eStep.StoreReview_AppOpenWait:
				if (!this.m_ResultUI.StoreReviewController.IsOpenWindow())
				{
					this.m_Step = BattleState_Result.eStep.CheckBranch;
				}
				break;
			case BattleState_Result.eStep.StoreReview_NativeOpenWait:
				if (!this.m_ResultUI.StoreReviewController.IsOpenWindow())
				{
					this.m_Step = BattleState_Result.eStep.CheckBranch;
				}
				break;
			case BattleState_Result.eStep.CheckBranch:
			{
				bool flag4 = false;
				if (this.m_Owner.System.QuestParam.m_AdvID_After != -1)
				{
					flag4 = true;
					if (LocalSaveData.Inst.ADVSkipOnBattle && !this.m_Owner.System.QuestResult.m_IsFirstClear)
					{
						flag4 = false;
					}
				}
				if (!flag4)
				{
					this.m_Step = BattleState_Result.eStep.RewardUIOpen;
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					this.m_Step = BattleState_Result.eStep.ResultAfterFadeOut_Wait;
				}
				break;
			}
			case BattleState_Result.eStep.RewardUIOpen:
				SingletonMonoBehaviour<RewardPlayer>.Inst.Open(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult(), null, null);
				this.m_Step = BattleState_Result.eStep.RewardUIWait;
				break;
			case BattleState_Result.eStep.RewardUIWait:
				if (!SingletonMonoBehaviour<RewardPlayer>.Inst.IsOpen())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
					this.m_Step = BattleState_Result.eStep.ResultAfterFadeOut_Wait;
				}
				break;
			case BattleState_Result.eStep.ResultAfterFadeOut_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = BattleState_Result.eStep.ResultUnload;
				}
				break;
			case BattleState_Result.eStep.ResultUnload:
				this.m_ResultUI = null;
				SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.UnloadChildSceneAsync(SceneDefine.eChildSceneID.BattleResultUI);
				this.m_Step = BattleState_Result.eStep.ResultUnload_Wait;
				break;
			case BattleState_Result.eStep.ResultUnload_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.SceneLoader.IsCompleteUnloadChildScene(SceneDefine.eChildSceneID.BattleResultUI))
				{
					this.m_Step = BattleState_Result.eStep.Transit;
				}
				break;
			case BattleState_Result.eStep.FailedTips:
			{
				LocalSaveData.Inst.RetireTipsIdx = (LocalSaveData.Inst.RetireTipsIdx + 1) % SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RetireTipsListDB.m_Params.Length;
				LocalSaveData.Inst.Save();
				eRetireTipsListDB id = (eRetireTipsListDB)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.RetireTipsListDB.m_Params[LocalSaveData.Inst.RetireTipsIdx].m_ID;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialTipsUI.Open(id);
				this.m_Step = BattleState_Result.eStep.FailedTipsWait;
				break;
			}
			case BattleState_Result.eStep.FailedTipsWait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialTipsUI.IsOpen())
				{
					this.m_Step = BattleState_Result.eStep.Failed;
				}
				break;
			case BattleState_Result.eStep.Failed:
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				this.m_Step = BattleState_Result.eStep.Failed_Wait;
				break;
			case BattleState_Result.eStep.Failed_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = BattleState_Result.eStep.Transit;
				}
				break;
			case BattleState_Result.eStep.Transit:
				this.m_Step = BattleState_Result.eStep.None;
				return 3;
			}
			return -1;
		}

		// Token: 0x060012F7 RID: 4855 RVA: 0x000658DC File Offset: 0x00063CDC
		private bool SetupAndPlay_Result()
		{
			this.m_ResultUI = UIUtility.GetMenuComponent<BattleResultUI>(SceneDefine.CHILD_SCENE_INFOS[SceneDefine.eChildSceneID.BattleResultUI].m_SceneName);
			if (this.m_ResultUI == null)
			{
				return false;
			}
			this.m_Owner.System.ResultCamera.gameObject.SetActive(true);
			this.m_Owner.System.ResultCamera.cullingMask |= 1 << LayerMask.NameToLayer("UI");
			float planeDistance = 120f;
			this.m_ResultUI.Setup(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult(), this.m_Owner.System.ResultCamera, planeDistance, this);
			this.m_ResultUI.PlayIn();
			return true;
		}

		// Token: 0x060012F8 RID: 4856 RVA: 0x000659A0 File Offset: 0x00063DA0
		public void HideCharactersForResult()
		{
			for (int i = 0; i < 6; i++)
			{
				CharacterHandler memberAt = this.m_Owner.System.PlayerParty.GetMemberAt(i);
				if (!(memberAt == null))
				{
					memberAt.CharaBattle.SetupForResult(true, new Vector3(0f, 10f, 0f), 1f);
					memberAt.CharaBattle.SetStencilParamForResult(i);
					memberAt.CharaBattle.RequestWinMode(true);
				}
			}
			for (int j = 0; j < 6; j++)
			{
				CharacterHandler member = this.m_Owner.System.EnemyParty.GetMember((BattleDefine.eJoinMember)j);
				if (!(member == null))
				{
					member.CharaBattle.SetupForResult(false, Vector3.zero, 1f);
				}
			}
			this.m_Owner.System.BattleCamera.SetEnablePostProcess(false);
		}

		// Token: 0x060012F9 RID: 4857 RVA: 0x00065A8C File Offset: 0x00063E8C
		public void ShowCharactersForResult()
		{
			this.m_Owner.System.ResultCamera.cullingMask |= 1 << LayerMask.NameToLayer("Character");
			this.m_Owner.System.MainCamera.cullingMask &= ~(1 << LayerMask.NameToLayer("Character"));
			for (int i = 0; i < 5; i++)
			{
				CharacterHandler memberAt = this.m_Owner.System.PlayerParty.GetMemberAt(i);
				if (!(memberAt == null))
				{
					memberAt.gameObject.SetLayer(LayerMask.NameToLayer("Character"), true);
					Vector3 pos = BattleState_Result.CHARA_POS_IN_RESULT[i];
					if (memberAt.CharaBattle.BattleWinID < BattleState_Result.CHARA_POS_IN_RESULT.Length)
					{
						pos.x += BattleState_Result.CHARA_POS_OFFSET_EACH_WIN[memberAt.CharaBattle.BattleWinID];
					}
					memberAt.CharaBattle.SetupForResult(true, pos, 6.5f);
				}
			}
		}

		// Token: 0x060012FA RID: 4858 RVA: 0x00065B9C File Offset: 0x00063F9C
		private void OnResponse_QuestSuccess(MeigewwwParam wwwParam, Questlogset param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, null);
				this.m_Step = BattleState_Result.eStep.Request_Wait;
			}
			else
			{
				this.m_Step = BattleState_Result.eStep.RequestPostProcess;
			}
		}

		// Token: 0x060012FB RID: 4859 RVA: 0x00065BE4 File Offset: 0x00063FE4
		private void OnResponse_QuestFailed(MeigewwwParam wwwParam, Questlogset param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, null);
				this.m_Step = BattleState_Result.eStep.Request_Wait;
			}
			else
			{
				this.m_Step = BattleState_Result.eStep.RequestPostProcess;
			}
		}

		// Token: 0x040019C6 RID: 6598
		private static readonly Vector3[] CHARA_POS_IN_RESULT = new Vector3[]
		{
			new Vector3(-7f, -4f, 0f),
			new Vector3(-3.5f, -4f, 0f),
			new Vector3(0f, -4f, 0f),
			new Vector3(3.5f, -4f, 0f),
			new Vector3(7f, -4f, 0f)
		};

		// Token: 0x040019C7 RID: 6599
		private static readonly float[] CHARA_POS_OFFSET_EACH_WIN = new float[]
		{
			-0.3f,
			-0.25f,
			0.6f,
			0.45f,
			-0.7f
		};

		// Token: 0x040019C8 RID: 6600
		private const float CHARA_SCALE_IN_RESULT = 6.5f;

		// Token: 0x040019C9 RID: 6601
		private BattleResultUI m_ResultUI;

		// Token: 0x040019CA RID: 6602
		private BattleState_Result.eStep m_Step = BattleState_Result.eStep.None;

		// Token: 0x020003E6 RID: 998
		private enum eStep
		{
			// Token: 0x040019CC RID: 6604
			None = -1,
			// Token: 0x040019CD RID: 6605
			First,
			// Token: 0x040019CE RID: 6606
			Request_Wait,
			// Token: 0x040019CF RID: 6607
			RequestPostProcess,
			// Token: 0x040019D0 RID: 6608
			RequestPostProcess_Wait,
			// Token: 0x040019D1 RID: 6609
			ResultLoad,
			// Token: 0x040019D2 RID: 6610
			ResultLoad_Wait,
			// Token: 0x040019D3 RID: 6611
			ResultPlayIn,
			// Token: 0x040019D4 RID: 6612
			Result_Wait,
			// Token: 0x040019D5 RID: 6613
			FriendPropose,
			// Token: 0x040019D6 RID: 6614
			FriendProposeWait,
			// Token: 0x040019D7 RID: 6615
			StoreReview,
			// Token: 0x040019D8 RID: 6616
			StoreReview_AppOpenWait,
			// Token: 0x040019D9 RID: 6617
			StoreReview_NativeOpenWait,
			// Token: 0x040019DA RID: 6618
			CheckBranch,
			// Token: 0x040019DB RID: 6619
			RewardUIOpen,
			// Token: 0x040019DC RID: 6620
			RewardUIWait,
			// Token: 0x040019DD RID: 6621
			ResultAfterFadeOut_Wait,
			// Token: 0x040019DE RID: 6622
			ResultUnload,
			// Token: 0x040019DF RID: 6623
			ResultUnload_Wait,
			// Token: 0x040019E0 RID: 6624
			FailedTips,
			// Token: 0x040019E1 RID: 6625
			FailedTipsWait,
			// Token: 0x040019E2 RID: 6626
			Failed,
			// Token: 0x040019E3 RID: 6627
			Failed_Wait,
			// Token: 0x040019E4 RID: 6628
			Transit
		}
	}
}
