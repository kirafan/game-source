﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200057C RID: 1404
	public class RoomGameCamera : MonoBehaviour
	{
		// Token: 0x06001B36 RID: 6966 RVA: 0x0008FC9C File Offset: 0x0008E09C
		private void Awake()
		{
			this.m_MoveRange = Rect.zero;
			this.m_PinchEventListener = new RoomPinchEvent();
			this.m_PinchState = new RoomPinchState();
			this.m_ThresholdMovePixel = (float)((Screen.width <= Screen.height) ? Screen.width : Screen.height) * this.m_ThresholdMoveRatio;
		}

		// Token: 0x06001B37 RID: 6967 RVA: 0x0008FCF6 File Offset: 0x0008E0F6
		private void Update()
		{
			this.m_PinchEventListener.UpdatePinch(this.m_PinchState);
			if (this.m_PinchState.m_Event)
			{
				this.OnPinchEvent(this.m_PinchState);
			}
			this.UpdateMove();
		}

		// Token: 0x06001B38 RID: 6968 RVA: 0x0008FD2B File Offset: 0x0008E12B
		public void SetIsControllable(bool flg)
		{
			this.m_IsControllable = flg;
			if (!this.m_IsControllable)
			{
				this.IsDragging = false;
				this.m_IsPinching = false;
			}
		}

		// Token: 0x06001B39 RID: 6969 RVA: 0x0008FD50 File Offset: 0x0008E150
		private void SetZoom(float size)
		{
			if (float.IsInfinity(size) || float.IsNaN(size))
			{
				return;
			}
			float orthographicSize = Mathf.Clamp(size, this.m_SizeMin, this.m_SizeMax);
			this.CalcMoveRange(orthographicSize);
			this.m_Camera.orthographicSize = orthographicSize;
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x06001B3A RID: 6970 RVA: 0x0008FF40 File Offset: 0x0008E340
		public void OnPinchEvent(RoomPinchState pevent)
		{
			if (!this.m_IsControllable)
			{
				return;
			}
			if (UIUtility.CheckPointOverUI(InputTouch.Inst.GetInfo(0).m_CurrentPos) || UIUtility.CheckPointOverUI(InputTouch.Inst.GetInfo(1).m_CurrentPos))
			{
				return;
			}
			if (pevent.IsPinching)
			{
				if (!this.m_IsPinching)
				{
					this.m_IsPinching = true;
					this.m_SizeOnStartPinch = this.m_Camera.orthographicSize;
				}
				this.SetZoom(this.m_SizeOnStartPinch / pevent.PinchRatio);
			}
			else
			{
				this.m_IsPinching = false;
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06001B3B RID: 6971 RVA: 0x0008FFE1 File Offset: 0x0008E3E1
		// (set) Token: 0x06001B3C RID: 6972 RVA: 0x0008FFEC File Offset: 0x0008E3EC
		public bool IsDragging
		{
			get
			{
				return this.m_DragState == RoomGameCamera.eDragState.Dragging;
			}
			set
			{
				if (value)
				{
					this.m_DragState = RoomGameCamera.eDragState.Dragging;
				}
				else
				{
					this.m_DragState = RoomGameCamera.eDragState.None;
				}
			}
		}

		// Token: 0x06001B3D RID: 6973 RVA: 0x00090007 File Offset: 0x0008E407
		public void SetMoveRange(Rect frect)
		{
			this.m_MoveRect = frect;
			this.m_MoveCenter = frect.center;
			this.UpdateMoveRange();
		}

		// Token: 0x06001B3E RID: 6974 RVA: 0x00090023 File Offset: 0x0008E423
		private void UpdateMoveRange()
		{
			this.CalcMoveRange(this.m_Camera.orthographicSize);
		}

		// Token: 0x06001B3F RID: 6975 RVA: 0x00090038 File Offset: 0x0008E438
		private void CalcMoveRange(float orthographicSize)
		{
			float num = orthographicSize * this.m_Camera.aspect;
			float num2 = this.m_MoveRect.xMin - this.m_SpaceLimitX + num;
			float num3 = this.m_MoveRect.xMax + this.m_SpaceLimitX - num;
			if (num2 > this.m_MoveCenter.x)
			{
				num2 = this.m_MoveCenter.x;
				num3 = this.m_MoveCenter.x;
			}
			this.m_MoveRange.xMin = num2;
			this.m_MoveRange.xMax = num3;
			num2 = this.m_MoveRect.yMin - this.m_SpaceLimitY + orthographicSize;
			num3 = this.m_MoveRect.yMax + this.m_SpaceLimitY - orthographicSize;
			if (num2 > this.m_MoveCenter.y)
			{
				num2 = this.m_MoveCenter.y;
				num3 = this.m_MoveCenter.y;
			}
			this.m_MoveRange.yMin = num2;
			this.m_MoveRange.yMax = num3;
		}

		// Token: 0x06001B40 RID: 6976 RVA: 0x0009012C File Offset: 0x0008E52C
		private void UpdateMove()
		{
			if (this.m_IsControllable)
			{
				int availableTouchCnt = InputTouch.Inst.GetAvailableTouchCnt();
				RoomGameCamera.eDragState dragState = this.m_DragState;
				if (dragState != RoomGameCamera.eDragState.None)
				{
					if (dragState != RoomGameCamera.eDragState.Ready)
					{
						if (dragState == RoomGameCamera.eDragState.Dragging)
						{
							bool flag = true;
							if (availableTouchCnt == 1 && !this.m_IsPinching)
							{
								InputTouch.TOUCH_INFO info = InputTouch.Inst.GetInfo(0);
								if (info.m_bAvailable && (info.m_TouchPhase == TouchPhase.Moved || info.m_TouchPhase == TouchPhase.Stationary))
								{
									flag = false;
									this.m_vMove = info.m_DiffFromPrev * info.m_DiffFromPrev.z;
									this.m_vMove.x = this.m_vMove.x * (this.m_Camera.orthographicSize * 2f / (float)Screen.width * this.m_Camera.aspect);
									this.m_vMove.y = this.m_vMove.y * (this.m_Camera.orthographicSize * 2f / (float)Screen.height);
									this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
								}
							}
							if (flag)
							{
								this.m_DragState = RoomGameCamera.eDragState.None;
							}
						}
					}
					else
					{
						bool flag2 = true;
						if (availableTouchCnt == 1 && !this.m_IsPinching)
						{
							InputTouch.TOUCH_INFO info2 = InputTouch.Inst.GetInfo(0);
							if (info2.m_bAvailable && (info2.m_TouchPhase == TouchPhase.Moved || info2.m_TouchPhase == TouchPhase.Stationary))
							{
								flag2 = false;
								if (info2.m_DiffFromStart.z >= this.m_ThresholdMovePixel)
								{
									this.m_DragState = RoomGameCamera.eDragState.Dragging;
								}
							}
						}
						if (flag2)
						{
							this.m_DragState = RoomGameCamera.eDragState.None;
						}
					}
				}
				else if (availableTouchCnt == 1 && !this.m_IsPinching)
				{
					InputTouch.TOUCH_INFO info3 = InputTouch.Inst.GetInfo(0);
					if (info3.m_bAvailable && info3.m_TouchPhase == TouchPhase.Began && !UIUtility.CheckPointOverUI(info3.m_StartPos))
					{
						this.m_DragState = RoomGameCamera.eDragState.Ready;
					}
				}
			}
			if (!this.IsDragging && !this.m_IsPinching)
			{
				this.m_vMove *= 1f - this.m_Inertia;
				if (this.m_vMove.sqrMagnitude > 0f)
				{
					this.m_Transform.localPosition += new Vector3(-this.m_vMove.x, -this.m_vMove.y, 0f);
				}
			}
			if (this.m_IsPinching)
			{
				this.m_vMove = Vector2.zero;
			}
			if (this.m_Transform.localPosition.x > this.m_MoveRange.xMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMax, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.x < this.m_MoveRange.xMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_MoveRange.xMin, this.m_Transform.localPosition.y, this.m_Transform.localPosition.z);
			}
			if (this.m_Transform.localPosition.y > this.m_MoveRange.yMax)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMax, this.m_Transform.localPosition.z);
			}
			else if (this.m_Transform.localPosition.y < this.m_MoveRange.yMin)
			{
				this.m_Transform.localPosition = new Vector3(this.m_Transform.localPosition.x, this.m_MoveRange.yMin, this.m_Transform.localPosition.z);
			}
		}

		// Token: 0x06001B41 RID: 6977 RVA: 0x00090590 File Offset: 0x0008E990
		public bool IsCheckTouchSlide(Vector2 fpos1, Vector2 fpos2)
		{
			return (fpos1 - fpos2).magnitude > this.m_ThresholdMovePixel;
		}

		// Token: 0x0400222C RID: 8748
		[SerializeField]
		private Camera m_Camera;

		// Token: 0x0400222D RID: 8749
		[SerializeField]
		private Transform m_Transform;

		// Token: 0x0400222E RID: 8750
		private RoomPinchEvent m_PinchEventListener;

		// Token: 0x0400222F RID: 8751
		private RoomPinchState m_PinchState;

		// Token: 0x04002230 RID: 8752
		[SerializeField]
		private float m_ThresholdMoveRatio = 0.005f;

		// Token: 0x04002231 RID: 8753
		private float m_ThresholdMovePixel;

		// Token: 0x04002232 RID: 8754
		[SerializeField]
		private float m_SizeMin = 1.5f;

		// Token: 0x04002233 RID: 8755
		[SerializeField]
		private float m_SizeMax = 4f;

		// Token: 0x04002234 RID: 8756
		private bool m_IsControllable = true;

		// Token: 0x04002235 RID: 8757
		private bool m_IsPinching;

		// Token: 0x04002236 RID: 8758
		private float m_SizeOnStartPinch = 1f;

		// Token: 0x04002237 RID: 8759
		[SerializeField]
		private float m_Inertia = 0.2f;

		// Token: 0x04002238 RID: 8760
		[SerializeField]
		private float m_SpaceLimitX = 1f;

		// Token: 0x04002239 RID: 8761
		[SerializeField]
		private float m_SpaceLimitY = 1f;

		// Token: 0x0400223A RID: 8762
		private Rect m_MoveRange;

		// Token: 0x0400223B RID: 8763
		private Rect m_MoveRect;

		// Token: 0x0400223C RID: 8764
		private Vector2 m_MoveCenter;

		// Token: 0x0400223D RID: 8765
		private Vector2 m_vMove = Vector2.zero;

		// Token: 0x0400223E RID: 8766
		private RoomGameCamera.eDragState m_DragState = RoomGameCamera.eDragState.None;

		// Token: 0x0200057D RID: 1405
		private enum eDragState
		{
			// Token: 0x04002240 RID: 8768
			None = -1,
			// Token: 0x04002241 RID: 8769
			Ready,
			// Token: 0x04002242 RID: 8770
			Dragging
		}
	}
}
