﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001E3 RID: 483
	public class QuestADVTriggerDB : ScriptableObject
	{
		// Token: 0x04000BA3 RID: 2979
		public QuestADVTriggerDB_Param[] m_Params;
	}
}
