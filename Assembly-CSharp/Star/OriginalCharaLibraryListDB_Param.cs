﻿using System;

namespace Star
{
	// Token: 0x020001DA RID: 474
	[Serializable]
	public struct OriginalCharaLibraryListDB_Param
	{
		// Token: 0x04000B2E RID: 2862
		public int m_ID;

		// Token: 0x04000B2F RID: 2863
		public string m_Title;

		// Token: 0x04000B30 RID: 2864
		public string m_Illustrator;

		// Token: 0x04000B31 RID: 2865
		public string m_CV;

		// Token: 0x04000B32 RID: 2866
		public string m_Descript;

		// Token: 0x04000B33 RID: 2867
		public int m_CondQuestID;
	}
}
