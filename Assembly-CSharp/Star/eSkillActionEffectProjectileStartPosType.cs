﻿using System;

namespace Star
{
	// Token: 0x0200010A RID: 266
	public enum eSkillActionEffectProjectileStartPosType
	{
		// Token: 0x040006DC RID: 1756
		Self,
		// Token: 0x040006DD RID: 1757
		WeaponLeft,
		// Token: 0x040006DE RID: 1758
		WeaponRight
	}
}
