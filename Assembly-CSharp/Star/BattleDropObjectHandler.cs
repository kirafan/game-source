﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020000BF RID: 191
	public abstract class BattleDropObjectHandler
	{
		// Token: 0x06000518 RID: 1304
		public abstract void Update();

		// Token: 0x06000519 RID: 1305
		public abstract bool Play(Transform parent, int sortingOrder);

		// Token: 0x0600051A RID: 1306
		public abstract void Stop();

		// Token: 0x0600051B RID: 1307
		public abstract bool IsPlaying();
	}
}
