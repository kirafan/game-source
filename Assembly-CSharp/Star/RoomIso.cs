﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200060D RID: 1549
	public class RoomIso
	{
		// Token: 0x06001E61 RID: 7777 RVA: 0x000A3CFC File Offset: 0x000A20FC
		public static RoomIso.IsoBounds CreateIsoBounds(RoomBlockData block)
		{
			RoomIso.IsoVerts isoVerts = RoomIso.GetIsoVerts(block);
			RoomIso.IsoBounds isoBounds = new RoomIso.IsoBounds();
			isoBounds.Set(isoVerts.frontDown.x, isoVerts.backUp.x, isoVerts.frontDown.y, isoVerts.backUp.y, isoVerts.leftDown.h, isoVerts.rightDown.h);
			return isoBounds;
		}

		// Token: 0x06001E62 RID: 7778 RVA: 0x000A3D68 File Offset: 0x000A2168
		public static RoomIso.IsoVerts GetIsoVerts(RoomBlockData block)
		{
			RoomIso.IsoNamedVerts isoNamedSpaceVerts = RoomIso.GetIsoNamedSpaceVerts(block);
			RoomIso.IsoVerts result = default(RoomIso.IsoVerts);
			RoomIso.SpaceToIso(ref result.leftDown, ref isoNamedSpaceVerts.leftDown);
			RoomIso.SpaceToIso(ref result.rightDown, ref isoNamedSpaceVerts.rightDown);
			RoomIso.SpaceToIso(ref result.backDown, ref isoNamedSpaceVerts.backDown);
			RoomIso.SpaceToIso(ref result.frontDown, ref isoNamedSpaceVerts.frontDown);
			RoomIso.SpaceToIso(ref result.leftUp, ref isoNamedSpaceVerts.leftUp);
			RoomIso.SpaceToIso(ref result.rightUp, ref isoNamedSpaceVerts.rightUp);
			RoomIso.SpaceToIso(ref result.backUp, ref isoNamedSpaceVerts.backUp);
			RoomIso.SpaceToIso(ref result.frontUp, ref isoNamedSpaceVerts.frontUp);
			return result;
		}

		// Token: 0x06001E63 RID: 7779 RVA: 0x000A3E20 File Offset: 0x000A2220
		public static RoomIso.IsoNamedVerts GetIsoNamedSpaceVerts(RoomBlockData block)
		{
			RoomIso.IsoNamedVerts result = default(RoomIso.IsoNamedVerts);
			result.Make(block.PosW, block.SizeW);
			return result;
		}

		// Token: 0x06001E64 RID: 7780 RVA: 0x000A3E4C File Offset: 0x000A224C
		public static void SpaceToIso(ref RoomIso.IsoSpc pret, ref RoomIso.IsoPos3 spacePos)
		{
			float z = spacePos.z;
			float num = spacePos.x + z;
			float num2 = spacePos.y + z;
			pret.Set(num, num2, (num - num2) * Mathf.Sqrt(3f) / 2f, (num + num2) / 2f);
		}

		// Token: 0x06001E65 RID: 7781 RVA: 0x000A3E98 File Offset: 0x000A2298
		public static void UpdateIsoBounds(RoomIso.IsoBounds pbox, RoomBlockData block)
		{
			RoomIso.IsoNamedVerts isoNamedVerts = default(RoomIso.IsoNamedVerts);
			isoNamedVerts.Make(block.PosW, block.SizeW);
			RoomIso.IsoVerts isoVerts = default(RoomIso.IsoVerts);
			RoomIso.SpaceToIso(ref isoVerts.leftDown, ref isoNamedVerts.leftDown);
			RoomIso.SpaceToIso(ref isoVerts.rightDown, ref isoNamedVerts.rightDown);
			RoomIso.SpaceToIso(ref isoVerts.backDown, ref isoNamedVerts.backDown);
			RoomIso.SpaceToIso(ref isoVerts.frontDown, ref isoNamedVerts.frontDown);
			RoomIso.SpaceToIso(ref isoVerts.leftUp, ref isoNamedVerts.leftUp);
			RoomIso.SpaceToIso(ref isoVerts.rightUp, ref isoNamedVerts.rightUp);
			RoomIso.SpaceToIso(ref isoVerts.backUp, ref isoNamedVerts.backUp);
			RoomIso.SpaceToIso(ref isoVerts.frontUp, ref isoNamedVerts.frontUp);
			pbox.Set(isoVerts.frontDown.x, isoVerts.backUp.x, isoVerts.frontDown.y, isoVerts.backUp.y, isoVerts.leftDown.h, isoVerts.rightDown.h);
		}

		// Token: 0x06001E66 RID: 7782 RVA: 0x000A3FB0 File Offset: 0x000A23B0
		public static int GetIsoBoxCross(RoomIso.IsoBounds bounds_a, RoomIso.IsoBounds bounds_b)
		{
			int num = 0;
			if (RoomIso.AreRangesDisjoint(bounds_a.xmin, bounds_a.xmax, bounds_b.xmin, bounds_b.xmax))
			{
				num |= 1;
			}
			if (RoomIso.AreRangesDisjoint(bounds_a.ymin, bounds_a.ymax, bounds_b.ymin, bounds_b.ymax))
			{
				num |= 2;
			}
			if (RoomIso.AreRangesDisjoint(bounds_a.hmin, bounds_a.hmax, bounds_b.hmin, bounds_b.hmax))
			{
				num |= 4;
			}
			return num;
		}

		// Token: 0x06001E67 RID: 7783 RVA: 0x000A4034 File Offset: 0x000A2434
		public static bool AreRangesDisjoint(float a_min, float a_max, float b_min, float b_max)
		{
			bool flag = (a_min >= b_min && a_min <= b_max) || (a_max >= b_min && a_max <= b_max);
			return flag | ((b_min >= a_min && b_min <= a_max) || (b_max >= a_min && b_max <= a_max));
		}

		// Token: 0x06001E68 RID: 7784 RVA: 0x000A408C File Offset: 0x000A248C
		public static RoomBlockData GetFrontBlockCross(RoomBlockData a, RoomBlockData b, int fcross)
		{
			RoomBlockData.BlockBounds bounds = a.Bounds;
			RoomBlockData.BlockBounds bounds2 = b.Bounds;
			if (bounds.xmin == bounds2.xmin && bounds.ymin == bounds2.ymin && bounds.xmax == bounds2.xmax && bounds.ymax == bounds2.ymax)
			{
				return null;
			}
			if ((fcross & 4) == 4)
			{
				if ((fcross & 1) == 1)
				{
					if (bounds.xmin >= bounds2.xmax)
					{
						return b;
					}
					if (bounds2.xmin >= bounds.xmax)
					{
						return a;
					}
				}
				if ((fcross & 2) == 2)
				{
					if (bounds.ymin >= bounds2.ymax)
					{
						return b;
					}
					if (bounds2.ymin >= bounds.ymax)
					{
						return a;
					}
				}
				if ((fcross & 4) == 4)
				{
					if (bounds.zmin >= bounds2.zmax)
					{
						return a;
					}
					if (bounds2.zmin >= bounds.zmax)
					{
						return b;
					}
				}
			}
			return null;
		}

		// Token: 0x06001E69 RID: 7785 RVA: 0x000A4198 File Offset: 0x000A2598
		public static float CalcBlockKey(RoomBlockData pblock)
		{
			float result = 10000f;
			if (!pblock.IsLinkObject())
			{
				result = 0.70710677f * (float)pblock.PosX + 0.70710677f * (float)pblock.PosY;
			}
			return result;
		}

		// Token: 0x0200060E RID: 1550
		public struct IsoSpc
		{
			// Token: 0x06001E6A RID: 7786 RVA: 0x000A41D3 File Offset: 0x000A25D3
			public IsoSpc(float _x, float _y, float _h, float _v)
			{
				this.x = _x;
				this.y = _y;
				this.h = _h;
				this.v = _v;
			}

			// Token: 0x06001E6B RID: 7787 RVA: 0x000A41F2 File Offset: 0x000A25F2
			public void Set(float _x, float _y, float _h, float _v)
			{
				this.x = _x;
				this.y = _y;
				this.h = _h;
				this.v = _v;
			}

			// Token: 0x040024EF RID: 9455
			public float x;

			// Token: 0x040024F0 RID: 9456
			public float y;

			// Token: 0x040024F1 RID: 9457
			public float h;

			// Token: 0x040024F2 RID: 9458
			public float v;
		}

		// Token: 0x0200060F RID: 1551
		public struct IsoPos3
		{
			// Token: 0x06001E6C RID: 7788 RVA: 0x000A4211 File Offset: 0x000A2611
			public IsoPos3(float _x, float _y, float _z)
			{
				this.x = _x;
				this.y = _y;
				this.z = _z;
			}

			// Token: 0x06001E6D RID: 7789 RVA: 0x000A4228 File Offset: 0x000A2628
			public void Set(float _x, float _y, float _z)
			{
				this.x = _x;
				this.y = _y;
				this.z = _z;
			}

			// Token: 0x040024F3 RID: 9459
			public float x;

			// Token: 0x040024F4 RID: 9460
			public float y;

			// Token: 0x040024F5 RID: 9461
			public float z;
		}

		// Token: 0x02000610 RID: 1552
		public class IsoBounds
		{
			// Token: 0x06001E6F RID: 7791 RVA: 0x000A4247 File Offset: 0x000A2647
			public void Set(float _xmin, float _xmax, float _ymin, float _ymax, float _hmin, float _hmax)
			{
				this.xmin = _xmin;
				this.xmax = _xmax;
				this.ymin = _ymin;
				this.ymax = _ymax;
				this.hmin = _hmin;
				this.hmax = _hmax;
			}

			// Token: 0x040024F6 RID: 9462
			public float xmin;

			// Token: 0x040024F7 RID: 9463
			public float xmax;

			// Token: 0x040024F8 RID: 9464
			public float ymin;

			// Token: 0x040024F9 RID: 9465
			public float ymax;

			// Token: 0x040024FA RID: 9466
			public float hmin;

			// Token: 0x040024FB RID: 9467
			public float hmax;
		}

		// Token: 0x02000611 RID: 1553
		public struct IsoVerts
		{
			// Token: 0x040024FC RID: 9468
			public RoomIso.IsoSpc leftDown;

			// Token: 0x040024FD RID: 9469
			public RoomIso.IsoSpc rightDown;

			// Token: 0x040024FE RID: 9470
			public RoomIso.IsoSpc backDown;

			// Token: 0x040024FF RID: 9471
			public RoomIso.IsoSpc frontDown;

			// Token: 0x04002500 RID: 9472
			public RoomIso.IsoSpc leftUp;

			// Token: 0x04002501 RID: 9473
			public RoomIso.IsoSpc rightUp;

			// Token: 0x04002502 RID: 9474
			public RoomIso.IsoSpc backUp;

			// Token: 0x04002503 RID: 9475
			public RoomIso.IsoSpc frontUp;
		}

		// Token: 0x02000612 RID: 1554
		public struct IsoNamedVerts
		{
			// Token: 0x06001E70 RID: 7792 RVA: 0x000A4278 File Offset: 0x000A2678
			public void Make(Vector3 fp, Vector3 fs)
			{
				this.leftDown.Set(fp.x, fp.y + fs.y, fp.z);
				this.rightDown.Set(fp.x + fs.x, fp.y, fp.z);
				this.backDown.Set(fp.x + fs.x, fp.y + fs.y, fp.z);
				this.frontDown.Set(fp.x, fp.y, fp.z);
				this.leftUp.Set(fp.x, fp.y + fs.y, fp.z + fs.z);
				this.rightUp.Set(fp.x + fs.x, fp.y, fp.z + fs.z);
				this.backUp.Set(fp.x + fs.x, fp.y + fs.y, fp.z + fs.z);
				this.frontUp.Set(fp.x, fp.y, fp.z + fs.z);
			}

			// Token: 0x04002504 RID: 9476
			public RoomIso.IsoPos3 leftDown;

			// Token: 0x04002505 RID: 9477
			public RoomIso.IsoPos3 rightDown;

			// Token: 0x04002506 RID: 9478
			public RoomIso.IsoPos3 backDown;

			// Token: 0x04002507 RID: 9479
			public RoomIso.IsoPos3 frontDown;

			// Token: 0x04002508 RID: 9480
			public RoomIso.IsoPos3 leftUp;

			// Token: 0x04002509 RID: 9481
			public RoomIso.IsoPos3 rightUp;

			// Token: 0x0400250A RID: 9482
			public RoomIso.IsoPos3 backUp;

			// Token: 0x0400250B RID: 9483
			public RoomIso.IsoPos3 frontUp;
		}
	}
}
