﻿using System;

namespace Star
{
	// Token: 0x02000716 RID: 1814
	public sealed class TownResourceManager
	{
		// Token: 0x060023E2 RID: 9186 RVA: 0x000C0C70 File Offset: 0x000BF070
		public TownResourceManager()
		{
			this.m_Loaders = new ResourcesLoader[2];
			for (int i = 0; i < 2; i++)
			{
				this.m_Loaders[i] = new ResourcesLoader(this.SIMULTANEOUSLY_LOAD_NUMS[i]);
			}
		}

		// Token: 0x060023E3 RID: 9187 RVA: 0x000C0CD8 File Offset: 0x000BF0D8
		public void Update()
		{
			for (int i = 0; i < 2; i++)
			{
				this.m_Loaders[i].Update();
			}
		}

		// Token: 0x060023E4 RID: 9188 RVA: 0x000C0D04 File Offset: 0x000BF104
		public bool IsAvailable()
		{
			return true;
		}

		// Token: 0x060023E5 RID: 9189 RVA: 0x000C0D08 File Offset: 0x000BF108
		public bool IsLoadingAny()
		{
			for (int i = 0; i < 2; i++)
			{
				if (this.m_Loaders[i].IsLoadingResources())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060023E6 RID: 9190 RVA: 0x000C0D3C File Offset: 0x000BF13C
		public ResourceObjectHandler LoadAsyncFromResources(TownResourceManager.eType type, string path)
		{
			return this.m_Loaders[(int)type].LoadAsyncFromResources(path, this.LOAD_TYPES[(int)type]);
		}

		// Token: 0x060023E7 RID: 9191 RVA: 0x000C0D54 File Offset: 0x000BF154
		public void UnloadFromResources(TownResourceManager.eType type, string path)
		{
			this.m_Loaders[(int)type].UnloadFromResources(path);
		}

		// Token: 0x060023E8 RID: 9192 RVA: 0x000C0D64 File Offset: 0x000BF164
		public void UnloadAllResources(TownResourceManager.eType type)
		{
			this.m_Loaders[(int)type].UnloadAllResources();
		}

		// Token: 0x060023E9 RID: 9193 RVA: 0x000C0D73 File Offset: 0x000BF173
		public bool IsCompleteUnloadAllResources(TownResourceManager.eType type)
		{
			return this.m_Loaders[(int)type].IsCompleteUnloadAllResources();
		}

		// Token: 0x060023EA RID: 9194 RVA: 0x000C0D84 File Offset: 0x000BF184
		public void UnloadAllResources()
		{
			for (int i = 0; i < this.m_Loaders.Length; i++)
			{
				this.m_Loaders[i].UnloadAllResources();
			}
		}

		// Token: 0x060023EB RID: 9195 RVA: 0x000C0DB8 File Offset: 0x000BF1B8
		public bool IsCompleteUnloadAllResources()
		{
			for (int i = 0; i < this.m_Loaders.Length; i++)
			{
				if (!this.m_Loaders[i].IsCompleteUnloadAllResources())
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04002AD8 RID: 10968
		public const int TYPE_NUM = 2;

		// Token: 0x04002AD9 RID: 10969
		private readonly int[] SIMULTANEOUSLY_LOAD_NUMS = new int[]
		{
			10,
			10
		};

		// Token: 0x04002ADA RID: 10970
		private readonly ResourceObjectHandler.eLoadType[] LOAD_TYPES = new ResourceObjectHandler.eLoadType[2];

		// Token: 0x04002ADB RID: 10971
		private ResourcesLoader[] m_Loaders;

		// Token: 0x02000717 RID: 1815
		public enum eType
		{
			// Token: 0x04002ADD RID: 10973
			Model,
			// Token: 0x04002ADE RID: 10974
			Anim,
			// Token: 0x04002ADF RID: 10975
			Num
		}
	}
}
