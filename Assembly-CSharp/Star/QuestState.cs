﻿using System;

namespace Star
{
	// Token: 0x02000458 RID: 1112
	public class QuestState : GameStateBase
	{
		// Token: 0x06001588 RID: 5512 RVA: 0x0007028D File Offset: 0x0006E68D
		public QuestState(QuestMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x06001589 RID: 5513 RVA: 0x000702A3 File Offset: 0x0006E6A3
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600158A RID: 5514 RVA: 0x000702A6 File Offset: 0x0006E6A6
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600158B RID: 5515 RVA: 0x000702A8 File Offset: 0x0006E6A8
		public override void OnStateExit()
		{
		}

		// Token: 0x0600158C RID: 5516 RVA: 0x000702AA File Offset: 0x0006E6AA
		public override void OnDispose()
		{
		}

		// Token: 0x0600158D RID: 5517 RVA: 0x000702AC File Offset: 0x0006E6AC
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0600158E RID: 5518 RVA: 0x000702AF File Offset: 0x0006E6AF
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001C50 RID: 7248
		protected QuestMain m_Owner;

		// Token: 0x04001C51 RID: 7249
		protected int m_NextState = -1;
	}
}
