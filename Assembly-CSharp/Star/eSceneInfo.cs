﻿using System;

namespace Star
{
	// Token: 0x020001FD RID: 509
	public enum eSceneInfo
	{
		// Token: 0x04000C4F RID: 3151
		None = -1,
		// Token: 0x04000C50 RID: 3152
		Home,
		// Token: 0x04000C51 RID: 3153
		Present,
		// Token: 0x04000C52 RID: 3154
		PresentHistory,
		// Token: 0x04000C53 RID: 3155
		Quest,
		// Token: 0x04000C54 RID: 3156
		QuestMain,
		// Token: 0x04000C55 RID: 3157
		QuestWeek,
		// Token: 0x04000C56 RID: 3158
		QuestChallenge,
		// Token: 0x04000C57 RID: 3159
		Edit,
		// Token: 0x04000C58 RID: 3160
		PartyEdit,
		// Token: 0x04000C59 RID: 3161
		SupportEdit,
		// Token: 0x04000C5A RID: 3162
		WeaponEquip,
		// Token: 0x04000C5B RID: 3163
		Upgrade,
		// Token: 0x04000C5C RID: 3164
		LimitBreak,
		// Token: 0x04000C5D RID: 3165
		Evolution,
		// Token: 0x04000C5E RID: 3166
		MasterEquip,
		// Token: 0x04000C5F RID: 3167
		Member,
		// Token: 0x04000C60 RID: 3168
		CharaList,
		// Token: 0x04000C61 RID: 3169
		CharaDetail,
		// Token: 0x04000C62 RID: 3170
		Shop,
		// Token: 0x04000C63 RID: 3171
		ShopTrade,
		// Token: 0x04000C64 RID: 3172
		NormalTrade,
		// Token: 0x04000C65 RID: 3173
		LimitTrade,
		// Token: 0x04000C66 RID: 3174
		TradeConfirm,
		// Token: 0x04000C67 RID: 3175
		ItemSale,
		// Token: 0x04000C68 RID: 3176
		SaleConfirm,
		// Token: 0x04000C69 RID: 3177
		ShopWeapon,
		// Token: 0x04000C6A RID: 3178
		WeaponCreate,
		// Token: 0x04000C6B RID: 3179
		WeaponCreateConfirm,
		// Token: 0x04000C6C RID: 3180
		WeaponUpgrade,
		// Token: 0x04000C6D RID: 3181
		WeaponUpgradeConfirm,
		// Token: 0x04000C6E RID: 3182
		WeaponSale,
		// Token: 0x04000C6F RID: 3183
		ShopBuild,
		// Token: 0x04000C70 RID: 3184
		ShopBuildPointSelect,
		// Token: 0x04000C71 RID: 3185
		ShopBuildObjSelect,
		// Token: 0x04000C72 RID: 3186
		Town,
		// Token: 0x04000C73 RID: 3187
		Schedule,
		// Token: 0x04000C74 RID: 3188
		Room,
		// Token: 0x04000C75 RID: 3189
		RoomMain,
		// Token: 0x04000C76 RID: 3190
		RoomSub,
		// Token: 0x04000C77 RID: 3191
		Gacha,
		// Token: 0x04000C78 RID: 3192
		Menu,
		// Token: 0x04000C79 RID: 3193
		UserData,
		// Token: 0x04000C7A RID: 3194
		Friend,
		// Token: 0x04000C7B RID: 3195
		Library,
		// Token: 0x04000C7C RID: 3196
		LibraryTitle,
		// Token: 0x04000C7D RID: 3197
		LibraryWord,
		// Token: 0x04000C7E RID: 3198
		LibraryADV,
		// Token: 0x04000C7F RID: 3199
		LibraryCharaADV,
		// Token: 0x04000C80 RID: 3200
		Option,
		// Token: 0x04000C81 RID: 3201
		Notification,
		// Token: 0x04000C82 RID: 3202
		Support,
		// Token: 0x04000C83 RID: 3203
		Inherit,
		// Token: 0x04000C84 RID: 3204
		PurchaseHistory,
		// Token: 0x04000C85 RID: 3205
		Training,
		// Token: 0x04000C86 RID: 3206
		CharacterDictionary
	}
}
