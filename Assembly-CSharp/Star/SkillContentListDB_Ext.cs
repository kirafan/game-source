﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020001A7 RID: 423
	public static class SkillContentListDB_Ext
	{
		// Token: 0x06000B12 RID: 2834 RVA: 0x00041E94 File Offset: 0x00040294
		public static SkillContentListDB_Param GetParam(this SkillContentListDB self, int skillID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == skillID)
				{
					return self.m_Params[i];
				}
			}
			return default(SkillContentListDB_Param);
		}

		// Token: 0x06000B13 RID: 2835 RVA: 0x00041EEC File Offset: 0x000402EC
		public static SkillContentListDB_Data GetData(this SkillContentListDB self, int skillID, int dataIndex)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == skillID)
				{
					return self.m_Params[i].m_Datas[dataIndex];
				}
			}
			return default(SkillContentListDB_Data);
		}

		// Token: 0x06000B14 RID: 2836 RVA: 0x00041F50 File Offset: 0x00040350
		public static List<int> GetRefIdsOnCardType(this SkillContentListDB self, int skillID)
		{
			List<int> list = new List<int>();
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == skillID)
				{
					for (int j = 0; j < self.m_Params[i].m_Datas.Length; j++)
					{
						if (self.m_Params[i].m_Datas[j].m_Type == 21)
						{
							int item = (int)self.m_Params[i].m_Datas[j].m_Args[1];
							list.Add(item);
						}
					}
				}
			}
			return list;
		}

		// Token: 0x06000B15 RID: 2837 RVA: 0x00042004 File Offset: 0x00040404
		public static int GetRecastFromSkillLv(ref SkillListDB_Param param, int skillLv, int[] borderLvs)
		{
			if (borderLvs != null)
			{
				for (int i = borderLvs.Length - 1; i >= 0; i--)
				{
					if (skillLv >= borderLvs[i])
					{
						return param.m_Recasts[i];
					}
				}
			}
			return param.m_Recasts[0];
		}

		// Token: 0x06000B16 RID: 2838 RVA: 0x00042048 File Offset: 0x00040448
		public static float GetLoadFactorFromSkillLv(ref SkillListDB_Param param, int skillLv, int[] borderLvs)
		{
			for (int i = borderLvs.Length - 1; i >= 0; i--)
			{
				if (skillLv >= borderLvs[i])
				{
					return param.m_LoadFactors[i];
				}
			}
			return param.m_LoadFactors[0];
		}
	}
}
