﻿using System;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x0200044B RID: 1099
	public class MoviePlayMain : GameStateMain
	{
		// Token: 0x1700016A RID: 362
		// (get) Token: 0x0600152C RID: 5420 RVA: 0x0006EF80 File Offset: 0x0006D380
		// (set) Token: 0x0600152D RID: 5421 RVA: 0x0006EF88 File Offset: 0x0006D388
		public MovieCanvas MovieCanvas
		{
			get
			{
				return this.m_MovieCanvas;
			}
			set
			{
				this.m_MovieCanvas = value;
			}
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x0600152E RID: 5422 RVA: 0x0006EF91 File Offset: 0x0006D391
		public MoviePlayUI MoviePlayUI
		{
			get
			{
				return this.m_UI;
			}
		}

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x0600152F RID: 5423 RVA: 0x0006EF99 File Offset: 0x0006D399
		// (set) Token: 0x06001530 RID: 5424 RVA: 0x0006EFA1 File Offset: 0x0006D3A1
		public string MoviePlayFileName
		{
			get
			{
				return this.m_MoviePlayFileName;
			}
			set
			{
				this.m_MoviePlayFileName = value;
			}
		}

		// Token: 0x1700016D RID: 365
		// (get) Token: 0x06001531 RID: 5425 RVA: 0x0006EFAA File Offset: 0x0006D3AA
		// (set) Token: 0x06001532 RID: 5426 RVA: 0x0006EFB2 File Offset: 0x0006D3B2
		public int AdvID
		{
			get
			{
				return this.m_AdvID;
			}
			set
			{
				this.m_AdvID = value;
			}
		}

		// Token: 0x06001533 RID: 5427 RVA: 0x0006EFBB File Offset: 0x0006D3BB
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x06001534 RID: 5428 RVA: 0x0006EFC4 File Offset: 0x0006D3C4
		private void OnDestroy()
		{
			this.m_MovieCanvas = null;
			this.m_UI = null;
		}

		// Token: 0x06001535 RID: 5429 RVA: 0x0006EFD4 File Offset: 0x0006D3D4
		public override void Destroy()
		{
			base.Destroy();
		}

		// Token: 0x06001536 RID: 5430 RVA: 0x0006EFDC File Offset: 0x0006D3DC
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x06001537 RID: 5431 RVA: 0x0006EFDF File Offset: 0x0006D3DF
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x06001538 RID: 5432 RVA: 0x0006EFE8 File Offset: 0x0006D3E8
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID == 0)
			{
				return new MoviePlayState_Init(this);
			}
			if (nextStateID == 1)
			{
				return new MoviePlayState_Play(this);
			}
			if (nextStateID != 2147483646)
			{
				return null;
			}
			return new CommonState_Final(this);
		}

		// Token: 0x04001C11 RID: 7185
		public const int STATE_INIT = 0;

		// Token: 0x04001C12 RID: 7186
		public const int STATE_PLAY = 1;

		// Token: 0x04001C13 RID: 7187
		[SerializeField]
		private MovieCanvas m_MovieCanvas;

		// Token: 0x04001C14 RID: 7188
		[SerializeField]
		private MoviePlayUI m_UI;

		// Token: 0x04001C15 RID: 7189
		private string m_MoviePlayFileName;

		// Token: 0x04001C16 RID: 7190
		private int m_AdvID = -1;
	}
}
