﻿using System;
using Star.UI.Global;
using Star.UI.Menu;

namespace Star
{
	// Token: 0x02000439 RID: 1081
	public class MenuState_LibraryOriginalChara : MenuState
	{
		// Token: 0x060014C2 RID: 5314 RVA: 0x0006D565 File Offset: 0x0006B965
		public MenuState_LibraryOriginalChara(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014C3 RID: 5315 RVA: 0x0006D57D File Offset: 0x0006B97D
		public override int GetStateID()
		{
			return 14;
		}

		// Token: 0x060014C4 RID: 5316 RVA: 0x0006D581 File Offset: 0x0006B981
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_LibraryOriginalChara.eStep.First;
		}

		// Token: 0x060014C5 RID: 5317 RVA: 0x0006D58A File Offset: 0x0006B98A
		public override void OnStateExit()
		{
		}

		// Token: 0x060014C6 RID: 5318 RVA: 0x0006D58C File Offset: 0x0006B98C
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x060014C7 RID: 5319 RVA: 0x0006D598 File Offset: 0x0006B998
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_LibraryOriginalChara.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_LibraryOriginalChara.eStep.LoadWait;
				break;
			case MenuState_LibraryOriginalChara.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryOriginalChara.eStep.PlayIn;
				}
				break;
			case MenuState_LibraryOriginalChara.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = MenuState_LibraryOriginalChara.eStep.Main;
				}
				break;
			case MenuState_LibraryOriginalChara.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.m_UI.Destroy();
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						this.m_NextState = 4;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = MenuState_LibraryOriginalChara.eStep.UnloadChildSceneWait;
				}
				break;
			case MenuState_LibraryOriginalChara.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_LibraryOriginalChara.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014C8 RID: 5320 RVA: 0x0006D6C4 File Offset: 0x0006BAC4
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<MenuLibraryOriginalCharaUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharacterDictionary);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060014C9 RID: 5321 RVA: 0x0006D75B File Offset: 0x0006BB5B
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.GoToMenuEnd();
		}

		// Token: 0x060014CA RID: 5322 RVA: 0x0006D779 File Offset: 0x0006BB79
		private void OnClickButton()
		{
		}

		// Token: 0x060014CB RID: 5323 RVA: 0x0006D77B File Offset: 0x0006BB7B
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x04001BAE RID: 7086
		private MenuState_LibraryOriginalChara.eStep m_Step = MenuState_LibraryOriginalChara.eStep.None;

		// Token: 0x04001BAF RID: 7087
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuLibraryOriginalCharaUI;

		// Token: 0x04001BB0 RID: 7088
		private MenuLibraryOriginalCharaUI m_UI;

		// Token: 0x0200043A RID: 1082
		private enum eStep
		{
			// Token: 0x04001BB2 RID: 7090
			None = -1,
			// Token: 0x04001BB3 RID: 7091
			First,
			// Token: 0x04001BB4 RID: 7092
			LoadWait,
			// Token: 0x04001BB5 RID: 7093
			PlayIn,
			// Token: 0x04001BB6 RID: 7094
			Main,
			// Token: 0x04001BB7 RID: 7095
			UnloadChildSceneWait
		}
	}
}
