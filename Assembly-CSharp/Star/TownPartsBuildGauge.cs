﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006B8 RID: 1720
	public class TownPartsBuildGauge : ITownPartsAction
	{
		// Token: 0x06002246 RID: 8774 RVA: 0x000B5FB8 File Offset: 0x000B43B8
		public TownPartsBuildGauge(TownBuilder pbuilder, Transform parent, long fmanageid, int flayer, int fdays, bool finit, bool fremake)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			UserTownObjectData townObjData = UserTownUtil.GetTownObjData(fmanageid);
			this.m_Builder = pbuilder;
			this.m_ManageID = fmanageid;
			this.m_MaxTime = UserTownUtil.GetBuildUpTime(townBuildData.m_ObjID, townBuildData.m_Lv);
			this.m_CalcStep = TownPartsBuildGauge.eCalcStep.Init;
			this.m_BuildModel = this.m_Builder.GetGeneralObj(eTownObjectType.BUILDING_OBJ_00 + fdays);
			this.m_Active = false;
			this.m_BuildUpType = false;
			if (townObjData.OpenState == UserTownObjectData.eOpenState.OnWait || fremake)
			{
				this.m_BuildUpType = true;
			}
			if (this.m_BuildModel != null)
			{
				this.m_Active = true;
				this.m_BuildModel.transform.localRotation = Quaternion.Euler(0f, 180f, 0f);
				this.m_BuildModel.transform.position = parent.position;
				this.m_MeigeAnimCtrl = this.m_BuildModel.AddComponent<MeigeAnimCtrl>();
				this.m_MsbHandle = this.m_BuildModel.GetComponentInChildren<MsbHandler>();
				this.m_AnimeTable = this.m_BuildModel.GetComponentInChildren<McatFileHelper>();
				if (this.m_AnimeTable != null)
				{
					this.m_MeigeAnimCtrl.Open();
					MeigeAnimClipHolder componentInChildren = this.m_AnimeTable.m_Table[0].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
					this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
					componentInChildren = this.m_AnimeTable.m_Table[1].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
					this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
					if (!this.m_BuildUpType)
					{
						componentInChildren = this.m_AnimeTable.m_Table[3].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
						componentInChildren = this.m_AnimeTable.m_Table[4].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
						componentInChildren = this.m_AnimeTable.m_Table[2].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
					}
					else
					{
						componentInChildren = this.m_AnimeTable.m_Table[5].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
						componentInChildren = this.m_AnimeTable.m_Table[6].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
						componentInChildren = this.m_AnimeTable.m_Table[7].m_MabFile.GetComponentInChildren<MeigeAnimClipHolder>();
						this.m_MeigeAnimCtrl.AddClip(this.m_MsbHandle, componentInChildren);
					}
					this.m_MeigeAnimCtrl.Close();
					if (finit)
					{
						this.m_MeigeAnimCtrl.Play("loop", 0f, WrapMode.Loop);
						this.m_CalcStep = TownPartsBuildGauge.eCalcStep.BuildGauge;
					}
					else
					{
						this.m_MeigeAnimCtrl.Play("set", 0f, WrapMode.ClampForever);
					}
				}
				Renderer[] componentsInChildren = this.m_BuildModel.GetComponentsInChildren<Renderer>();
				for (int i = 0; i < componentsInChildren.Length; i++)
				{
					componentsInChildren[i].sortingOrder = flayer;
				}
				this.m_BuildGauge = TownUtility.FindTransform(this.m_BuildModel.transform, "gauge_2", 10);
			}
			this.m_Time = 0f;
		}

		// Token: 0x06002247 RID: 8775 RVA: 0x000B630B File Offset: 0x000B470B
		public override void Destory()
		{
			if (this.m_Builder != null && this.m_BuildModel != null)
			{
				this.m_Builder.ReleaseCharaMarker(this.m_BuildModel);
				this.m_BuildModel = null;
			}
		}

		// Token: 0x06002248 RID: 8776 RVA: 0x000B6347 File Offset: 0x000B4747
		public void SetComplete()
		{
			this.m_Time = 0f;
			if (this.m_CalcStep < TownPartsBuildGauge.eCalcStep.Complete)
			{
				this.m_MeigeAnimCtrl.Play("end", 0f, WrapMode.ClampForever);
			}
			this.m_CalcStep = TownPartsBuildGauge.eCalcStep.Complete;
		}

		// Token: 0x06002249 RID: 8777 RVA: 0x000B637D File Offset: 0x000B477D
		public void SetTerm()
		{
			this.m_Time = 0f;
			this.m_CalcStep = TownPartsBuildGauge.eCalcStep.End;
		}

		// Token: 0x0600224A RID: 8778 RVA: 0x000B6394 File Offset: 0x000B4794
		private void GaugeUpCheck()
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(this.m_ManageID);
			if (!townBuildData.m_IsOpen)
			{
				long num = UserTownUtil.GetBuildTimeMs(this.m_ManageID);
				if (num >= this.m_MaxTime)
				{
					num = this.m_MaxTime;
					this.m_CalcStep = TownPartsBuildGauge.eCalcStep.Stop_ST;
					this.m_MeigeAnimCtrl.Play("complete_st", 0f, WrapMode.ClampForever);
				}
				else if (num < 0L)
				{
					num = 0L;
				}
				if (this.m_BuildGauge != null)
				{
					this.m_BuildGauge.transform.localScale = new Vector3((float)num / (float)this.m_MaxTime * 61f, 1f, 1f);
				}
			}
			else
			{
				this.m_CalcStep = TownPartsBuildGauge.eCalcStep.Stop_ST;
				this.m_MeigeAnimCtrl.Play("complete_st", 0f, WrapMode.ClampForever);
			}
		}

		// Token: 0x0600224B RID: 8779 RVA: 0x000B6468 File Offset: 0x000B4868
		public override bool PartsUpdate()
		{
			switch (this.m_CalcStep)
			{
			case TownPartsBuildGauge.eCalcStep.Init:
				this.m_CalcStep = TownPartsBuildGauge.eCalcStep.InitGauge;
				break;
			case TownPartsBuildGauge.eCalcStep.InitGauge:
				if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime >= 1f)
				{
					this.m_MeigeAnimCtrl.Play("loop", 0f, WrapMode.Loop);
					this.m_CalcStep = TownPartsBuildGauge.eCalcStep.BuildGauge;
				}
				this.GaugeUpCheck();
				break;
			case TownPartsBuildGauge.eCalcStep.BuildGauge:
				this.GaugeUpCheck();
				break;
			case TownPartsBuildGauge.eCalcStep.Stop_ST:
				if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime >= 1f)
				{
					this.m_MeigeAnimCtrl.Play("complete_lp", 0f, WrapMode.Loop);
					this.m_CalcStep = TownPartsBuildGauge.eCalcStep.Stop;
				}
				break;
			case TownPartsBuildGauge.eCalcStep.Complete:
				if (this.m_MeigeAnimCtrl.m_NormalizedPlayTime >= 1f)
				{
					this.m_Active = false;
					this.m_Builder.ReleaseCharaMarker(this.m_BuildModel);
					this.m_BuildModel = null;
					this.m_BuildGauge = null;
					this.m_MsbHandle = null;
					this.m_MeigeAnimCtrl = null;
					this.m_AnimeTable = null;
				}
				break;
			case TownPartsBuildGauge.eCalcStep.End:
				this.m_Active = false;
				if (this.m_BuildModel != null)
				{
					this.m_Builder.ReleaseCharaMarker(this.m_BuildModel);
					this.m_BuildModel = null;
				}
				this.m_BuildGauge = null;
				this.m_MsbHandle = null;
				this.m_MeigeAnimCtrl = null;
				this.m_AnimeTable = null;
				break;
			}
			return this.m_Active;
		}

		// Token: 0x040028C6 RID: 10438
		private const float MAX_BARSIZE = 61f;

		// Token: 0x040028C7 RID: 10439
		private Transform m_BuildGauge;

		// Token: 0x040028C8 RID: 10440
		private GameObject m_BuildModel;

		// Token: 0x040028C9 RID: 10441
		private MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x040028CA RID: 10442
		private MsbHandler m_MsbHandle;

		// Token: 0x040028CB RID: 10443
		private McatFileHelper m_AnimeTable;

		// Token: 0x040028CC RID: 10444
		private TownBuilder m_Builder;

		// Token: 0x040028CD RID: 10445
		private long m_ManageID;

		// Token: 0x040028CE RID: 10446
		private long m_MaxTime;

		// Token: 0x040028CF RID: 10447
		public float m_Time;

		// Token: 0x040028D0 RID: 10448
		public bool m_BuildUpType;

		// Token: 0x040028D1 RID: 10449
		private const string PLAY_ST = "set";

		// Token: 0x040028D2 RID: 10450
		private const string PLAY_LP = "loop";

		// Token: 0x040028D3 RID: 10451
		private const string PLAY_COMP_ST = "complete_st";

		// Token: 0x040028D4 RID: 10452
		private const string PLAY_COMP_LP = "complete_lp";

		// Token: 0x040028D5 RID: 10453
		private const string PLAY_COMP_ED = "end";

		// Token: 0x040028D6 RID: 10454
		private const int PLAYANM_ST = 0;

		// Token: 0x040028D7 RID: 10455
		private const int PLAYANM_LP = 1;

		// Token: 0x040028D8 RID: 10456
		private const int PLAYANM_BASE_COMP_END = 2;

		// Token: 0x040028D9 RID: 10457
		private const int PLAYANM_BASE_COMP_ST = 3;

		// Token: 0x040028DA RID: 10458
		private const int PLAYANM_BASE_COMP_LP = 4;

		// Token: 0x040028DB RID: 10459
		private const int PLAYANM_RE_COMP_END = 7;

		// Token: 0x040028DC RID: 10460
		private const int PLAYANM_RE_COMP_ST = 5;

		// Token: 0x040028DD RID: 10461
		private const int PLAYANM_RE_COMP_LP = 6;

		// Token: 0x040028DE RID: 10462
		private TownPartsBuildGauge.eCalcStep m_CalcStep;

		// Token: 0x020006B9 RID: 1721
		public enum eCalcStep
		{
			// Token: 0x040028E0 RID: 10464
			Init,
			// Token: 0x040028E1 RID: 10465
			InitGauge,
			// Token: 0x040028E2 RID: 10466
			BuildGauge,
			// Token: 0x040028E3 RID: 10467
			Stop_ST,
			// Token: 0x040028E4 RID: 10468
			Stop,
			// Token: 0x040028E5 RID: 10469
			Complete,
			// Token: 0x040028E6 RID: 10470
			End
		}
	}
}
