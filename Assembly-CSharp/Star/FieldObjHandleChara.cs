﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000334 RID: 820
	public class FieldObjHandleChara
	{
		// Token: 0x06000F8F RID: 3983 RVA: 0x000535B3 File Offset: 0x000519B3
		public FieldObjHandleChara(long fmngid, int froomid)
		{
			this.m_ManageID = fmngid;
			this.m_RoomID = froomid;
			this.m_PopUpMessageID = new int[6];
			this.m_PopUpMsgMax = 1;
		}

		// Token: 0x06000F90 RID: 3984 RVA: 0x000535DC File Offset: 0x000519DC
		public void SetUpChara(int faccesskey, long fmngid, long fsettimesec)
		{
			this.m_ManageID = fmngid;
			this.m_PlayTimeSec = fsettimesec;
			this.m_CharaID = UserCharaUtil.GetCharaData(this.m_ManageID).Param.CharaID;
			this.InitEventTime(fsettimesec, UserCharaUtil.GetCharaParam(this.m_ManageID).m_NamedType);
			this.InitBuildUpParam();
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x00053633 File Offset: 0x00051A33
		public void LinkFeedBackCall(FeedBackCharaCallBack pback)
		{
			this.m_Callback = pback;
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x0005363C File Offset: 0x00051A3C
		public long GetStayBuildMngID()
		{
			return this.m_LinkBuildMngID;
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x00053644 File Offset: 0x00051A44
		public void ReleaseParam()
		{
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x00053646 File Offset: 0x00051A46
		public eCharaStay GetStayScene()
		{
			return this.m_StayType;
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x00053650 File Offset: 0x00051A50
		public bool InitBuildUpParam()
		{
			bool result = false;
			this.m_LinkBuildMngID = TownDefine.ROOM_ACCESS_MNG_ID;
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			this.m_LinkBuildMngID = roomChara.TownBuildManageID;
			this.m_PlayScheduleTag = roomChara.PlayScheduleTagID;
			UserScheduleData.Seg table = this.m_Schedule.GetTable(this.m_PlayIndex);
			switch (table.m_Type)
			{
			case UserScheduleData.eType.Non:
			case UserScheduleData.eType.Room:
				this.m_StayType = eCharaStay.Room;
				this.m_NextStay = eCharaStay.Room;
				break;
			case UserScheduleData.eType.Sleep:
				this.m_StayType = eCharaStay.Sleep;
				this.m_NextStay = eCharaStay.Sleep;
				break;
			case UserScheduleData.eType.Office:
				this.m_StayType = eCharaStay.Content;
				this.m_NextStay = eCharaStay.Content;
				break;
			case UserScheduleData.eType.Town:
				this.m_StayType = eCharaStay.Buf;
				this.m_NextStay = eCharaStay.Buf;
				break;
			case UserScheduleData.eType.System:
				this.m_StayType = eCharaStay.Menu;
				this.m_NextStay = eCharaStay.Menu;
				break;
			}
			table = this.m_Schedule.GetTable(this.m_PlayIndex);
			this.m_ScheduleTagID = table.m_TagNameID;
			this.m_ScheduleTagTime = table.m_UseTime * 1000;
			this.UpScheduleParamerter();
			return result;
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x00053760 File Offset: 0x00051B60
		public void Destroy()
		{
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x00053764 File Offset: 0x00051B64
		public void BackRoomMove(FieldBuildMapReq preqmng)
		{
			this.m_PlayScheduleTag = UserScheduleUtil.MoveToScheduleStateUp(this.m_Schedule, eTownMoveState.NonPointMove, this.m_PlayIndex, -1L);
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.SetScheduleDropKey(-1, 0f);
			preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CallbackRoomMove), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x000537D0 File Offset: 0x00051BD0
		public void UpdateHandle(FieldBuildMapReq preqmng)
		{
			if (this.m_ChangeCheckPoint)
			{
				switch (this.m_NextStay)
				{
				case eCharaStay.Room:
					this.m_StayType = this.m_NextStay;
					preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CallbackRoomMove), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
					break;
				case eCharaStay.Sleep:
					this.m_StayType = this.m_NextStay;
					preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CallbackRoomMove), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
					break;
				case eCharaStay.Content:
					preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CalcBackContentSearch), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
					this.m_StayType = this.m_NextStay;
					break;
				case eCharaStay.Buf:
					preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CalcBackTownSearch), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
					this.m_StayType = this.m_NextStay;
					break;
				case eCharaStay.Menu:
					preqmng.CharaMoveBuildPoint(this.m_ManageID, this.m_LinkBuildMngID, new FieldBuildMapReq.MoveBeforeCallback(this.CalcBackMenuSearch), new FieldBuildMapReq.MoveResultCallback(this.CallBackTownMove));
					this.m_StayType = this.m_NextStay;
					break;
				}
				if (this.m_Callback != null)
				{
					this.m_Callback(eCallBackType.StayChange, this);
				}
				this.m_ChangeCheckPoint = false;
			}
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x00053954 File Offset: 0x00051D54
		private bool CallbackRoomMove(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			presult.m_MoveTargetMngID = TownDefine.ROOM_ACCESS_MNG_ID;
			presult.m_UpState = eTownMoveState.TargetMove;
			return true;
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x00053969 File Offset: 0x00051D69
		private bool CalcBackContentSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return pmng.GetCheckBuildAreaMngID(this.m_ManageID, this.m_ScheduleTagID, ref presult);
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x0005397E File Offset: 0x00051D7E
		private bool CalcBackTownSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return pmng.GetFreeBufAccessMngID(this.m_ManageID, this.m_ScheduleTagID, ref presult);
		}

		// Token: 0x06000F9C RID: 3996 RVA: 0x00053993 File Offset: 0x00051D93
		private bool CalcBackMenuSearch(FieldBuildMap pmng, ref TownSearchResult presult)
		{
			return pmng.GetCheckBuildMenuMngID(this.m_ManageID, this.m_ScheduleTagID, ref presult);
		}

		// Token: 0x06000F9D RID: 3997 RVA: 0x000539A8 File Offset: 0x00051DA8
		private void CallBackTownMove(eTownMoveState fupmove, long fnextpointmngid, long ftimekey)
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			this.m_LinkBuildMngID = fnextpointmngid;
			this.m_PlayScheduleTag = UserScheduleUtil.MoveToScheduleStateUp(this.m_Schedule, fupmove, this.m_PlayIndex, fnextpointmngid);
			roomChara.ChangeBuildPoint(this.m_LinkBuildMngID, this.m_ScheduleTagID, this.m_PlayScheduleTag, this.m_ScheduleTagTime, this.m_CharaID, ftimekey, fupmove);
			if (fupmove == eTownMoveState.None || this.m_LinkBuildMngID == TownDefine.ROOM_ACCESS_MNG_ID)
			{
				if (this.m_NextStay == eCharaStay.Sleep)
				{
					this.m_StayType = this.m_NextStay;
				}
				else
				{
					this.m_StayType = (this.m_NextStay = eCharaStay.Room);
				}
				this.m_LinkBuildMngID = TownDefine.ROOM_ACCESS_MNG_ID;
			}
			if (this.m_Callback != null)
			{
				this.m_Callback(eCallBackType.MoveBuild, this);
			}
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x00053A70 File Offset: 0x00051E70
		public void ScheduleEventFunc(UserScheduleData.eType ftype, UserScheduleData.Seg pdata)
		{
			switch (ftype)
			{
			case UserScheduleData.eType.Sleep:
				this.m_ScheduleTagID = pdata.m_TagNameID;
				this.m_ScheduleTagTime = pdata.m_UseTime * 1000;
				this.m_NextStay = eCharaStay.Sleep;
				this.m_ChangeCheckPoint = true;
				break;
			case UserScheduleData.eType.Room:
				this.m_ScheduleTagID = pdata.m_TagNameID;
				this.m_ScheduleTagTime = pdata.m_UseTime * 1000;
				this.m_NextStay = eCharaStay.Room;
				this.m_ChangeCheckPoint = true;
				break;
			case UserScheduleData.eType.Office:
				this.m_ScheduleTagID = pdata.m_TagNameID;
				this.m_ScheduleTagTime = pdata.m_UseTime * 1000;
				this.m_NextStay = eCharaStay.Content;
				this.m_ChangeCheckPoint = true;
				break;
			case UserScheduleData.eType.Town:
				this.m_ScheduleTagID = pdata.m_TagNameID;
				this.m_ScheduleTagTime = pdata.m_UseTime * 1000;
				this.m_NextStay = eCharaStay.Buf;
				this.m_ChangeCheckPoint = true;
				break;
			case UserScheduleData.eType.System:
				this.m_ScheduleTagID = pdata.m_TagNameID;
				this.m_ScheduleTagTime = pdata.m_UseTime * 1000;
				this.m_NextStay = eCharaStay.Menu;
				this.m_ChangeCheckPoint = true;
				break;
			}
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x00053B9A File Offset: 0x00051F9A
		private void ScheduleQueFunc(UserScheduleData.Seg pdata)
		{
			if (this.m_Callback != null)
			{
				this.m_Callback(eCallBackType.ScheduleChange, this);
			}
			this.UpScheduleParamerter();
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x00053BBC File Offset: 0x00051FBC
		public void UpScheduleParamerter()
		{
			ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(this.m_ScheduleTagID);
			int num = scheduleNameTag.m_BaseMessage.Length;
			if (num > 6)
			{
				num = 6;
			}
			this.m_PopUpMsgMax = (byte)num;
			for (int i = 0; i < num; i++)
			{
				this.m_PopUpMessageID[i] = scheduleNameTag.m_BaseMessage[i];
			}
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x00053C14 File Offset: 0x00052014
		public bool IsScheduleChangeTag()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			return roomChara.m_ChangeScheduleTag;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x00053C34 File Offset: 0x00052034
		public int GetScheduleTagItemLevel()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			return roomChara.m_ScheduleDropItemIcon;
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x00053C54 File Offset: 0x00052054
		public bool IsTouchItemNew()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			return roomChara.m_TouchItemNew;
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x00053C74 File Offset: 0x00052074
		public int GetTouchItemNewLevel()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			if (roomChara.m_TouchItemSp)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x00053C9C File Offset: 0x0005209C
		public void SetScheduleChangeTagUp()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.m_ChangeScheduleTag = false;
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x00053CBC File Offset: 0x000520BC
		public int GetStayToTownObjectID()
		{
			int result = 0;
			UserTownData userTownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			for (int i = 0; i < userTownData.GetBuildObjectDataNum(); i++)
			{
				if (userTownData.GetBuildObjectDataAt(i).m_ManageID == this.m_LinkBuildMngID)
				{
					result = userTownData.GetBuildObjectDataAt(i).m_ObjID;
					break;
				}
			}
			return result;
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x00053D1C File Offset: 0x0005211C
		public bool CalcScheduleChangeTagDrop(IFldNetComModule.CallBack pcallback)
		{
			bool result = false;
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			this.m_SpPopMode = false;
			if (roomChara.m_TouchItemNew)
			{
				FldComPartyScript fldComPartyScript = new FldComPartyScript();
				this.m_SpPopMode = false;
				if (roomChara.m_TouchItemSp)
				{
					this.m_SpPopMode = true;
				}
				roomChara.ClearTouchItemEvent();
				fldComPartyScript.StackSendCmd(FldComPartyScript.eCmd.TouchItem, roomChara.ManageID, pcallback);
				result = true;
				fldComPartyScript.PlaySend();
			}
			ScheduleNameDB_Param scheduleNameTag = ScheduleNameUtil.GetScheduleNameTag(this.m_ScheduleTagID);
			if (scheduleNameTag.m_SpMessage.Length > 0)
			{
				int num = UnityEngine.Random.Range(0, scheduleNameTag.m_SpMessage.Length - 1);
				this.m_SpPopUpMessageID = scheduleNameTag.m_SpMessage[num];
			}
			else
			{
				this.m_SpPopUpMessageID = 0;
			}
			return result;
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x00053DD2 File Offset: 0x000521D2
		public bool IsPopUpKeySpMode()
		{
			return this.m_SpPopMode;
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x00053DDC File Offset: 0x000521DC
		public void ClearDropItemState()
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.ClearTouchItemNew();
			this.m_SpPopMode = false;
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x00053E04 File Offset: 0x00052204
		public int GetScheduleToPopMessageID()
		{
			return this.m_PopUpMessageID[UnityEngine.Random.Range(0, (int)(this.m_PopUpMsgMax - 1))];
		}

		// Token: 0x06000FAB RID: 4011 RVA: 0x00053E28 File Offset: 0x00052228
		public int GetTagDropMessageID()
		{
			return this.m_SpPopUpMessageID;
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x00053E30 File Offset: 0x00052230
		public long GetLastScheduleSettingTime()
		{
			if (this.m_Schedule == null)
			{
				return 0L;
			}
			return ScheduleTimeUtil.GetDayKey(this.m_Schedule.m_SettingTime / 10000000L);
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x00053E57 File Offset: 0x00052257
		public long GetLastPlayScheduleTagTime()
		{
			if (this.m_Schedule == null)
			{
				return 0L;
			}
			return this.m_LastChangePlayTagTime;
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x00053E70 File Offset: 0x00052270
		public bool IsMatchIdealSchedule()
		{
			if (this.m_Schedule == null)
			{
				return true;
			}
			for (int i = 0; i < this.m_Schedule.GetListNum(); i++)
			{
				UserScheduleData.Seg table = this.m_Schedule.GetTable(i);
				int dayTimeSec = ScheduleTimeUtil.GetDayTimeSec(this.m_PlayTimeSec);
				if (dayTimeSec >= table.m_WakeTime && dayTimeSec < table.m_WakeTime + table.m_UseTime && (table.m_TagNameID != this.m_ScheduleTagID || table.m_TagNameID != this.m_PlayScheduleTag || i != this.m_PlayIndex))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x00053F10 File Offset: 0x00052310
		private void ForceChangePlayIdx(int idx)
		{
			if (idx >= this.m_Schedule.GetListNum())
			{
				return;
			}
			this.m_PlayTagTime = 0L;
			this.m_PlayIndex = idx;
			for (int i = 0; i < this.m_Schedule.GetListNum(); i++)
			{
				if (i < idx)
				{
					UserScheduleData.Seg table = this.m_Schedule.GetTable(i);
					this.m_PlayTagTime += (long)table.m_UseTime;
				}
			}
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x00053F84 File Offset: 0x00052384
		public bool UpdateTimeChk(long checkTimeSec)
		{
			bool flag = false;
			if (ScheduleTimeUtil.GetDayKey(checkTimeSec) != ScheduleTimeUtil.GetDayKey(this.m_PlayTimeSec))
			{
				flag = true;
				this.m_PlayIndex = 0;
				this.m_PlayTagTime = 0L;
			}
			else
			{
				long num = (long)ScheduleTimeUtil.GetDayTimeSec(checkTimeSec) - this.m_PlayTagTime;
				if (this.m_Schedule != null)
				{
					int listNum = this.m_Schedule.GetListNum();
					while (num >= 0L)
					{
						UserScheduleData.Seg table = this.m_Schedule.GetTable(this.m_PlayIndex);
						num -= (long)table.m_UseTime;
						if (num >= 0L)
						{
							this.m_PlayTagTime += (long)table.m_UseTime;
							this.m_PlayIndex++;
							flag = true;
							if (this.m_PlayIndex >= listNum)
							{
								break;
							}
						}
					}
				}
			}
			this.m_PlayTimeSec = checkTimeSec;
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.SetSchedulePlayTime(this.m_PlayTimeSec);
			if (!flag && !this.IsMatchIdealSchedule())
			{
				flag = true;
			}
			return flag;
		}

		// Token: 0x06000FB1 RID: 4017 RVA: 0x00054080 File Offset: 0x00052480
		public void InitEventTime(long fsettingtime, int fnamekey)
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.CheckDefaultScheduleUp(fsettingtime);
			this.m_Schedule = roomChara.GetScheduleList(fsettingtime);
			this.m_LastChangePlayTagTime = this.m_Schedule.m_ChangeStateTime / 10000000L;
			this.m_PlayIndex = 0;
			this.m_PlayTimeSec = roomChara.GetSchedulePlayTime();
			long num = this.m_PlayTagTime = (long)ScheduleTimeUtil.GetDayTimeSec(this.m_PlayTimeSec);
			int listNum = this.m_Schedule.GetListNum();
			for (int i = 0; i < listNum; i++)
			{
				UserScheduleData.Seg table = this.m_Schedule.GetTable(this.m_PlayIndex);
				if ((long)table.m_WakeTime > num)
				{
					this.m_PlayIndex--;
					table = this.m_Schedule.GetTable(this.m_PlayIndex);
					this.m_PlayTagTime = (long)table.m_WakeTime;
					break;
				}
				this.m_PlayIndex++;
			}
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x0005416C File Offset: 0x0005256C
		public void FitScheduleTime(long ftimesec)
		{
			this.m_PlayIndex = 0;
			this.m_PlayTimeSec = ftimesec;
			long num = this.m_PlayTagTime = (long)ScheduleTimeUtil.GetDayTimeSec(ftimesec);
			int listNum = this.m_Schedule.GetListNum();
			for (int i = 0; i < listNum; i++)
			{
				UserScheduleData.Seg table = this.m_Schedule.GetTable(this.m_PlayIndex);
				if ((long)table.m_WakeTime > num)
				{
					this.m_PlayIndex--;
					table = this.m_Schedule.GetTable(this.m_PlayIndex);
					this.m_PlayTagTime = (long)table.m_WakeTime;
					break;
				}
				this.m_PlayIndex++;
			}
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.SetSchedulePlayTime(this.m_PlayTimeSec);
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x00054230 File Offset: 0x00052630
		public void RefleshSchedule(long fsystemtime)
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.ChkUpSchedule(true, fsystemtime);
			this.m_Schedule = roomChara.GetScheduleList(fsystemtime);
			this.m_LastChangePlayTagTime = this.m_Schedule.m_ChangeStateTime / 10000000L;
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x00054278 File Offset: 0x00052678
		public void SetStateChangeTime(long fsystemtime)
		{
			UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
			roomChara.m_Schedule.m_DayListUp[0].m_ChangeStateTime = fsystemtime * 1000L * 1000L * 10L;
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x000542B8 File Offset: 0x000526B8
		public void SetScheduleTime(long fuptime)
		{
			this.FitScheduleTime(fuptime);
			UserScheduleData.Seg table = this.m_Schedule.GetTable(this.m_PlayIndex);
			this.ScheduleQueFunc(table);
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x000542E8 File Offset: 0x000526E8
		public void PlaySceheduleTag(int ftagno, FieldBuildMapReq preqmng)
		{
			this.ForceChangePlayIdx(ftagno);
			UserScheduleData.Seg table = this.m_Schedule.GetTable(ftagno);
			this.ScheduleEventFunc(table.m_Type, table);
			this.UpdateHandle(preqmng);
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x0005431D File Offset: 0x0005271D
		public UserScheduleData.ListPack GetScheduleList()
		{
			return this.m_Schedule;
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x00054328 File Offset: 0x00052728
		public bool IsBuildToScheduleChange()
		{
			bool result = false;
			int listNum = this.m_Schedule.GetListNum();
			for (int i = this.m_PlayIndex; i < listNum; i++)
			{
				if (UserScheduleUtil.CheckTagToBuilding(this.m_Schedule, i))
				{
					UserFieldCharaData.RoomInCharaData roomChara = UserCharaUtil.GetRoomChara(this.m_ManageID);
					roomChara.SetScheduleDropKey(-1, 0f);
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000FB9 RID: 4025 RVA: 0x00054388 File Offset: 0x00052788
		public ScheduleUIDataPack CreateScheduleUIData()
		{
			ScheduleUIDataPack scheduleUIDataPack = new ScheduleUIDataPack();
			int listNum = this.m_Schedule.GetListNum();
			int num = 0;
			for (int i = 0; i < listNum; i++)
			{
				UserScheduleData.Seg table = this.m_Schedule.GetTable(i);
				switch (table.m_Type)
				{
				case UserScheduleData.eType.Sleep:
				case UserScheduleData.eType.Room:
				case UserScheduleData.eType.Office:
				case UserScheduleData.eType.Town:
				case UserScheduleData.eType.System:
					num++;
					break;
				}
			}
			scheduleUIDataPack.m_Table = new ScheduleUIData[num];
			num = 0;
			for (int i = 0; i < listNum; i++)
			{
				UserScheduleData.Seg table = this.m_Schedule.GetTable(i);
				bool flag = false;
				eScheduleTypeUIType type = eScheduleTypeUIType.Sleep;
				eScheduleSegName segNameID = eScheduleSegName.Sleep;
				switch (table.m_Type)
				{
				case UserScheduleData.eType.Sleep:
					flag = true;
					type = eScheduleTypeUIType.Sleep;
					segNameID = eScheduleSegName.Sleep;
					break;
				case UserScheduleData.eType.Room:
					flag = true;
					type = eScheduleTypeUIType.Room;
					segNameID = eScheduleSegName.Room;
					break;
				case UserScheduleData.eType.Office:
					flag = true;
					segNameID = eScheduleSegName.Office;
					type = eScheduleTypeUIType.Office;
					break;
				case UserScheduleData.eType.Town:
					flag = true;
					segNameID = eScheduleSegName.Buf;
					type = eScheduleTypeUIType.Town;
					break;
				case UserScheduleData.eType.System:
					flag = true;
					segNameID = eScheduleSegName.System;
					type = eScheduleTypeUIType.System;
					break;
				}
				if (flag)
				{
					scheduleUIDataPack.m_Table[num].m_WakeTime = table.m_WakeTime / 60;
					scheduleUIDataPack.m_Table[num].m_NameTagID = table.m_TagNameID;
					scheduleUIDataPack.m_Table[num].m_SegNameID = (int)segNameID;
					scheduleUIDataPack.m_Table[num].m_Time = table.m_UseTime / 60;
					scheduleUIDataPack.m_Table[num].m_Type = type;
					num++;
				}
			}
			return scheduleUIDataPack;
		}

		// Token: 0x040016F0 RID: 5872
		private const int POP_STACK_MAX = 6;

		// Token: 0x040016F1 RID: 5873
		public long m_ManageID;

		// Token: 0x040016F2 RID: 5874
		public int m_RoomID;

		// Token: 0x040016F3 RID: 5875
		public int m_CharaID;

		// Token: 0x040016F4 RID: 5876
		public long m_LinkBuildMngID;

		// Token: 0x040016F5 RID: 5877
		public eCharaStay m_StayType;

		// Token: 0x040016F6 RID: 5878
		public eCharaStay m_NextStay;

		// Token: 0x040016F7 RID: 5879
		public bool m_ChangeCheckPoint;

		// Token: 0x040016F8 RID: 5880
		public int[] m_PopUpMessageID;

		// Token: 0x040016F9 RID: 5881
		public byte m_PopUpMsgMax;

		// Token: 0x040016FA RID: 5882
		public int m_SpPopUpMessageID;

		// Token: 0x040016FB RID: 5883
		public int m_ScheduleTagID;

		// Token: 0x040016FC RID: 5884
		public int m_PlayScheduleTag;

		// Token: 0x040016FD RID: 5885
		public int m_ScheduleTagTime;

		// Token: 0x040016FE RID: 5886
		public bool m_SearchActive;

		// Token: 0x040016FF RID: 5887
		private bool m_SpPopMode;

		// Token: 0x04001700 RID: 5888
		public int m_EntryPoint;

		// Token: 0x04001701 RID: 5889
		public FeedBackCharaCallBack m_Callback;

		// Token: 0x04001702 RID: 5890
		private UserScheduleData.ListPack m_Schedule;

		// Token: 0x04001703 RID: 5891
		private int m_PlayIndex;

		// Token: 0x04001704 RID: 5892
		private long m_PlayTimeSec;

		// Token: 0x04001705 RID: 5893
		private long m_PlayTagTime;

		// Token: 0x04001706 RID: 5894
		private long m_LastChangePlayTagTime;
	}
}
