﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020001CB RID: 459
	public class LeaderSkillListDB : ScriptableObject
	{
		// Token: 0x04000AF7 RID: 2807
		public LeaderSkillListDB_Param[] m_Params;
	}
}
