﻿using System;

namespace Star
{
	// Token: 0x020003A2 RID: 930
	public class UserCharaUtil
	{
		// Token: 0x06001176 RID: 4470 RVA: 0x0005C348 File Offset: 0x0005A748
		public static CharacterListDB_Param GetCharaParam(long fmanageid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			UserCharacterData userCharaData = userDataMng.GetUserCharaData(fmanageid);
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(userCharaData.Param.CharaID);
		}

		// Token: 0x06001177 RID: 4471 RVA: 0x0005C387 File Offset: 0x0005A787
		public static CharacterListDB_Param GetIDToCharaParam(int fcharaid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(fcharaid);
		}

		// Token: 0x06001178 RID: 4472 RVA: 0x0005C3A0 File Offset: 0x0005A7A0
		public static NamedListDB_Param GetCharaIDToNameParam(int fcharaid)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			return dbMng.NamedListDB.GetParam((eCharaNamedType)dbMng.CharaListDB.GetParam(fcharaid).m_NamedType);
		}

		// Token: 0x06001179 RID: 4473 RVA: 0x0005C3D8 File Offset: 0x0005A7D8
		public static UserCharacterData GetCharaData(long fmanageid)
		{
			UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
			return userDataMng.GetUserCharaData(fmanageid);
		}

		// Token: 0x0600117A RID: 4474 RVA: 0x0005C3F7 File Offset: 0x0005A7F7
		public static UserFieldCharaData.RoomInCharaData GetRoomChara(long fmanageid)
		{
			return SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInChara(fmanageid);
		}

		// Token: 0x0600117B RID: 4475 RVA: 0x0005C410 File Offset: 0x0005A810
		public static UserScheduleData.DropState[] GetScheduleDropState(int fcharaid)
		{
			int dropItemKey = UserCharaUtil.GetCharaIDToNameParam(fcharaid).m_DropItemKey;
			UserScheduleData.DropState fieldDropItems = UserTownUtil.GetFieldDropItems(dropItemKey);
			UserScheduleData.DropState[] result;
			if (fieldDropItems == null)
			{
				result = new UserScheduleData.DropState[0];
			}
			else
			{
				result = new UserScheduleData.DropState[]
				{
					fieldDropItems
				};
			}
			return result;
		}
	}
}
