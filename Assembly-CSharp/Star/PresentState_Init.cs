﻿using System;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x02000453 RID: 1107
	public class PresentState_Init : PresentState
	{
		// Token: 0x0600155E RID: 5470 RVA: 0x0006F584 File Offset: 0x0006D984
		public PresentState_Init(PresentMain owner) : base(owner)
		{
		}

		// Token: 0x0600155F RID: 5471 RVA: 0x0006F594 File Offset: 0x0006D994
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001560 RID: 5472 RVA: 0x0006F597 File Offset: 0x0006D997
		public override void OnStateEnter()
		{
			this.m_Step = PresentState_Init.eStep.First;
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x0006F5A0 File Offset: 0x0006D9A0
		public override void OnStateExit()
		{
		}

		// Token: 0x06001562 RID: 5474 RVA: 0x0006F5A2 File Offset: 0x0006D9A2
		public override void OnDispose()
		{
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x0006F5A4 File Offset: 0x0006D9A4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case PresentState_Init.eStep.First:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Open(UIBackGroundManager.eBackGroundOrderID.Low, UIBackGroundManager.eBackGroundID.Common, false);
					eSoundBgmListDB townBgmID = TownUtility.GetTownBgmID();
					if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(townBgmID))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(townBgmID, 1f, 0, -1, -1);
					}
					SingletonMonoBehaviour<GameSystem>.Inst.PresentMng.Request_GetPresentList(new Action<bool>(this.OnResponse_GetPresentList));
					this.m_Step = PresentState_Init.eStep.Request_Wait;
				}
				break;
			case PresentState_Init.eStep.Load_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.IsLoadingAny())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					this.m_Step = PresentState_Init.eStep.End;
				}
				break;
			case PresentState_Init.eStep.End:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					this.m_Step = PresentState_Init.eStep.None;
					return 1;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001564 RID: 5476 RVA: 0x0006F6C1 File Offset: 0x0006DAC1
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x0006F6CA File Offset: 0x0006DACA
		private void OnResponse_GetPresentList(bool isError)
		{
			if (!isError)
			{
				this.m_Step = PresentState_Init.eStep.Load_Wait;
			}
		}

		// Token: 0x04001C30 RID: 7216
		private PresentState_Init.eStep m_Step = PresentState_Init.eStep.None;

		// Token: 0x02000454 RID: 1108
		private enum eStep
		{
			// Token: 0x04001C32 RID: 7218
			None = -1,
			// Token: 0x04001C33 RID: 7219
			First,
			// Token: 0x04001C34 RID: 7220
			Request_Wait,
			// Token: 0x04001C35 RID: 7221
			Load_Wait,
			// Token: 0x04001C36 RID: 7222
			End
		}
	}
}
