﻿using System;
using FieldPartyMemberResponseTypes;

namespace Star
{
	// Token: 0x0200034E RID: 846
	public class FieldPartyAPIItemUp : INetComHandle
	{
		// Token: 0x0600101F RID: 4127 RVA: 0x00055D01 File Offset: 0x00054101
		public FieldPartyAPIItemUp()
		{
			this.ApiName = "player/field_party/member/item_up";
			this.Request = true;
			this.ResponseType = typeof(Itemup);
		}

		// Token: 0x0400172E RID: 5934
		public long managedPartyMemberId;

		// Token: 0x0400172F RID: 5935
		public int touchItemResultNo;

		// Token: 0x04001730 RID: 5936
		public int amount;

		// Token: 0x04001731 RID: 5937
		public int flag;
	}
}
