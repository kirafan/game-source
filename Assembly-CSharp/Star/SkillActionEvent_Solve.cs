﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000111 RID: 273
	[Serializable]
	public class SkillActionEvent_Solve : SkillActionEvent_Base
	{
		// Token: 0x040006FC RID: 1788
		public byte m_Index;

		// Token: 0x040006FD RID: 1789
		public float m_Option_0;

		// Token: 0x040006FE RID: 1790
		[Tooltip("効果が「攻撃」の場合、ダメージエフェクトを発生するか")]
		public bool m_IsEnableDamageEffect;

		// Token: 0x040006FF RID: 1791
		public SkillActionInfo_DamageEffect m_DamageEffect;
	}
}
