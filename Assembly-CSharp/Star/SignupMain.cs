﻿using System;
using Star.UI.Signup;
using UnityEngine;

namespace Star
{
	// Token: 0x02000499 RID: 1177
	public class SignupMain : GameStateMain
	{
		// Token: 0x1700017A RID: 378
		// (get) Token: 0x06001714 RID: 5908 RVA: 0x000784DA File Offset: 0x000768DA
		public SignupUI SignupUI
		{
			get
			{
				return this.m_UI;
			}
		}

		// Token: 0x06001715 RID: 5909 RVA: 0x000784E2 File Offset: 0x000768E2
		private void Start()
		{
			this.m_UI.Setup();
			base.SetNextState(0);
		}

		// Token: 0x06001716 RID: 5910 RVA: 0x000784F6 File Offset: 0x000768F6
		private void OnDestroy()
		{
			this.m_UI = null;
		}

		// Token: 0x06001717 RID: 5911 RVA: 0x000784FF File Offset: 0x000768FF
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x06001718 RID: 5912 RVA: 0x00078508 File Offset: 0x00076908
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			if (nextStateID != 0)
			{
				return null;
			}
			return new SignupState_Main(this);
		}

		// Token: 0x04001DD1 RID: 7633
		public const int STATE_MAIN = 0;

		// Token: 0x04001DD2 RID: 7634
		[SerializeField]
		private SignupUI m_UI;
	}
}
