﻿using System;

namespace Star
{
	// Token: 0x020002D5 RID: 725
	public class EquipWeaponUnmanagedSupportPartyMemberController : EquipWeaponCharacterDataControllerExistExGen<EquipWeaponUnmanagedSupportPartyMemberController, UserSupportDataWrapper>
	{
		// Token: 0x06000E20 RID: 3616 RVA: 0x0004D758 File Offset: 0x0004BB58
		public EquipWeaponUnmanagedSupportPartyMemberController() : base(eCharacterDataControllerType.UnmanagedSupportParty)
		{
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000E21 RID: 3617 RVA: 0x0004D761 File Offset: 0x0004BB61
		private UserSupportDataWrapper memberDataEx
		{
			get
			{
				return base.GetDataInternal();
			}
		}

		// Token: 0x06000E22 RID: 3618 RVA: 0x0004D769 File Offset: 0x0004BB69
		public EquipWeaponUnmanagedSupportPartyMemberController Setup(CharacterDataWrapperBase supportData, int in_partyIndex = -1, int in_partySlotIndex = -1)
		{
			base.SetupBase(in_partyIndex, in_partySlotIndex);
			return this.Apply(supportData, in_partyIndex, in_partySlotIndex);
		}

		// Token: 0x06000E23 RID: 3619 RVA: 0x0004D77D File Offset: 0x0004BB7D
		public EquipWeaponUnmanagedSupportPartyMemberController Setup(UserSupportData userSupportData)
		{
			return this.Setup(new UserSupportDataWrapper(userSupportData), -1, -1);
		}

		// Token: 0x06000E24 RID: 3620 RVA: 0x0004D78D File Offset: 0x0004BB8D
		private EquipWeaponUnmanagedSupportPartyMemberController Apply(CharacterDataWrapperBase supportData, int in_partyIndex, int in_partySlotIndex)
		{
			base.ApplyCharacterDataEx(supportData);
			return (EquipWeaponUnmanagedSupportPartyMemberController)this.ApplyBase(in_partyIndex, in_partySlotIndex);
		}

		// Token: 0x06000E25 RID: 3621 RVA: 0x0004D7A4 File Offset: 0x0004BBA4
		protected override EquipWeaponPartyMemberController ApplyBase(int in_partyIndex, int in_partySlotIndex)
		{
			return this;
		}

		// Token: 0x06000E26 RID: 3622 RVA: 0x0004D7A8 File Offset: 0x0004BBA8
		public override int GetWeaponIdNaked()
		{
			int result = -1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponIdNaked();
			}
			return result;
		}

		// Token: 0x06000E27 RID: 3623 RVA: 0x0004D7D0 File Offset: 0x0004BBD0
		public override int GetWeaponSkillID()
		{
			int result = -1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponSkillID();
			}
			return result;
		}

		// Token: 0x06000E28 RID: 3624 RVA: 0x0004D7F8 File Offset: 0x0004BBF8
		public override sbyte GetWeaponSkillExpTableID()
		{
			sbyte result = -1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponSkillExpTableID();
			}
			return result;
		}

		// Token: 0x06000E29 RID: 3625 RVA: 0x0004D820 File Offset: 0x0004BC20
		public override int GetWeaponLv()
		{
			int result = 1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponLv();
			}
			return result;
		}

		// Token: 0x06000E2A RID: 3626 RVA: 0x0004D848 File Offset: 0x0004BC48
		public override int GetWeaponMaxLv()
		{
			int result = 1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponMaxLv();
			}
			return result;
		}

		// Token: 0x06000E2B RID: 3627 RVA: 0x0004D870 File Offset: 0x0004BC70
		public override long GetWeaponExp()
		{
			long result = 1L;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponExp();
			}
			return result;
		}

		// Token: 0x06000E2C RID: 3628 RVA: 0x0004D898 File Offset: 0x0004BC98
		public override int GetWeaponSkillLv()
		{
			int result = 1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponSkillLv();
			}
			return result;
		}

		// Token: 0x06000E2D RID: 3629 RVA: 0x0004D8C0 File Offset: 0x0004BCC0
		public override int GetWeaponSkillMaxLv()
		{
			int result = 1;
			if (this.memberDataEx != null)
			{
				result = this.memberDataEx.GetWeaponSkillMaxLv();
			}
			return result;
		}
	}
}
