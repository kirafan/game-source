﻿using System;

namespace Star
{
	// Token: 0x0200021B RID: 539
	[Serializable]
	public struct SoundVoiceControllListDB_Data
	{
		// Token: 0x04000E05 RID: 3589
		public int m_HourMin;

		// Token: 0x04000E06 RID: 3590
		public int m_HourMax;

		// Token: 0x04000E07 RID: 3591
		public string[] m_CueSheetNames;

		// Token: 0x04000E08 RID: 3592
		public string[] m_CueNames;

		// Token: 0x04000E09 RID: 3593
		public string m_NamedCueName;
	}
}
