﻿using System;

namespace Star
{
	// Token: 0x020001B6 RID: 438
	public static class TownObjectBuffDB_Ext
	{
		// Token: 0x06000B2F RID: 2863 RVA: 0x000428D8 File Offset: 0x00040CD8
		public static TownObjectBuffDB_Param GetParam(this TownObjectBuffDB self, int buffListID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == buffListID)
				{
					return self.m_Params[i];
				}
			}
			return new TownObjectBuffDB_Param
			{
				m_Datas = new TownObjectBuffDB_Data[0]
			};
		}

		// Token: 0x06000B30 RID: 2864 RVA: 0x00042940 File Offset: 0x00040D40
		public static TownObjectBuffDB_Param GetParamByObjID(this TownObjectBuffDB self, int objID)
		{
			int levelUpListID = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(objID).m_LevelUpListID;
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == levelUpListID)
				{
					return self.m_Params[i];
				}
			}
			return new TownObjectBuffDB_Param
			{
				m_Datas = new TownObjectBuffDB_Data[0]
			};
		}
	}
}
