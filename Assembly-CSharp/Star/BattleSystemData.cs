﻿using System;
using System.Collections.Generic;
using Star.Town;

namespace Star
{
	// Token: 0x020000E9 RID: 233
	[Serializable]
	public class BattleSystemData
	{
		// Token: 0x060006D9 RID: 1753 RVA: 0x00028E8E File Offset: 0x0002728E
		public bool VersionCheck(string version)
		{
			return !string.IsNullOrEmpty(this.Ver) && this.Ver == version;
		}

		// Token: 0x04000548 RID: 1352
		public const string SUBVERSION = "4";

		// Token: 0x04000549 RID: 1353
		public string Ver;

		// Token: 0x0400054A RID: 1354
		public string SubVer = "4";

		// Token: 0x0400054B RID: 1355
		public int Phase = -1;

		// Token: 0x0400054C RID: 1356
		public long RecvID = -1L;

		// Token: 0x0400054D RID: 1357
		public int QuestID = -1;

		// Token: 0x0400054E RID: 1358
		public long PtyMngID = -1L;

		// Token: 0x0400054F RID: 1359
		public int ContCount;

		// Token: 0x04000550 RID: 1360
		public int WaveIdx;

		// Token: 0x04000551 RID: 1361
		[NonSerialized]
		public int WaveNum;

		// Token: 0x04000552 RID: 1362
		public int MstSkillCount;

		// Token: 0x04000553 RID: 1363
		public bool MstSkillFlg;

		// Token: 0x04000554 RID: 1364
		public int FrdType;

		// Token: 0x04000555 RID: 1365
		public int FrdCharaID = -1;

		// Token: 0x04000556 RID: 1366
		public int FrdCharaLv = 1;

		// Token: 0x04000557 RID: 1367
		public int FrdLB;

		// Token: 0x04000558 RID: 1368
		public int FrdUSLv = 1;

		// Token: 0x04000559 RID: 1369
		public int[] FrdCSLvs;

		// Token: 0x0400055A RID: 1370
		public int FrdWpID = -1;

		// Token: 0x0400055B RID: 1371
		public int FrdWpLv = 1;

		// Token: 0x0400055C RID: 1372
		public int FrdWpSLv = 1;

		// Token: 0x0400055D RID: 1373
		public int Frdship;

		// Token: 0x0400055E RID: 1374
		public int FrdRemainTurn;

		// Token: 0x0400055F RID: 1375
		public int FrdUseCount;

		// Token: 0x04000560 RID: 1376
		public bool FrdUsed;

		// Token: 0x04000561 RID: 1377
		public BattleTogetherGauge Gauge;

		// Token: 0x04000562 RID: 1378
		public TownBuff TBuff;

		// Token: 0x04000563 RID: 1379
		public BattleOccurEnemy Enemies;

		// Token: 0x04000564 RID: 1380
		public List<BattleDropItems> ScheduleItems;

		// Token: 0x04000565 RID: 1381
		public BattleSystemDataForPL[] PLs;

		// Token: 0x04000566 RID: 1382
		public int[] PLJoinIdxs;
	}
}
