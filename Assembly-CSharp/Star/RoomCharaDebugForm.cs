﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x020005AC RID: 1452
	public class RoomCharaDebugForm : DebugWindowForm
	{
		// Token: 0x06001C02 RID: 7170 RVA: 0x000948B4 File Offset: 0x00092CB4
		public void Awake()
		{
			this.m_Form = base.transform;
			this.m_SelectNo = -1;
			GameObject gameObject = GameObject.Find("RoomBuilder");
			if (gameObject != null)
			{
				this.m_Builder = gameObject.GetComponent<RoomBuilder>();
				RoomCharaManager roomCharaManager = this.m_Builder.m_CharaManager as RoomCharaManager;
				if (roomCharaManager != null)
				{
					int charaPakageMax = roomCharaManager.GetCharaPakageMax();
					for (int i = 0; i < charaPakageMax; i++)
					{
						RoomCharaPakage charaPakage = roomCharaManager.GetCharaPakage(i);
						if (charaPakage != null && charaPakage.m_Handle != null)
						{
							this.m_SelectNo = i;
							break;
						}
					}
				}
			}
			Button button = DebugWindowBase.FindHrcObject(base.transform, "CharaSelect", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackNextChara));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "ActionButton", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackAction));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "MovePosX/UpButton", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackMoveXDown));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "MovePosX/DownButton", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackMoveXUp));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "MovePosY/UpButton", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackMoveYUp));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "MovePosY/DownButton", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackMoveYDown));
			}
			Toggle toggle = DebugWindowBase.FindHrcObject(base.transform, "DebugMode", typeof(Toggle)) as Toggle;
			if (toggle != null)
			{
				toggle.onValueChanged.AddListener(new UnityAction<bool>(this.CallbackDebugMode));
			}
			this.m_MovePosX = (DebugWindowBase.FindHrcObject(base.transform, "MovePosX/Pos", typeof(Text)) as Text);
			this.m_MovePosY = (DebugWindowBase.FindHrcObject(base.transform, "MovePosY/Pos", typeof(Text)) as Text);
			this.m_CharaName = (DebugWindowBase.FindHrcObject(base.transform, "CharaInfo/Names", typeof(Text)) as Text);
			this.SetUpCharaViewForm();
		}

		// Token: 0x06001C03 RID: 7171 RVA: 0x00094BC0 File Offset: 0x00092FC0
		public void CallbackNextChara()
		{
			if (this.m_Builder != null)
			{
				RoomCharaManager roomCharaManager = this.m_Builder.m_CharaManager as RoomCharaManager;
				if (roomCharaManager != null)
				{
					int charaPakageMax = roomCharaManager.GetCharaPakageMax();
					for (int i = 0; i < charaPakageMax; i++)
					{
						this.m_SelectNo++;
						if (this.m_SelectNo >= charaPakageMax)
						{
							this.m_SelectNo = 0;
						}
						RoomCharaPakage charaPakage = roomCharaManager.GetCharaPakage(this.m_SelectNo);
						if (charaPakage != null && charaPakage.m_RoomObj != null)
						{
							this.SetUpCharaViewForm();
							break;
						}
					}
				}
			}
		}

		// Token: 0x06001C04 RID: 7172 RVA: 0x00094C64 File Offset: 0x00093064
		private RoomCharaPakage GetSelectChara()
		{
			if (this.m_Builder != null)
			{
				RoomCharaManager roomCharaManager = this.m_Builder.m_CharaManager as RoomCharaManager;
				if (roomCharaManager != null)
				{
					return roomCharaManager.GetCharaPakage(this.m_SelectNo);
				}
			}
			return null;
		}

		// Token: 0x06001C05 RID: 7173 RVA: 0x00094CB0 File Offset: 0x000930B0
		private void CallbackAction()
		{
			RoomCharaPakage selectChara = this.GetSelectChara();
			if (selectChara != null && selectChara.m_RoomObj != null)
			{
				RoomActionCommand roomActionCommand = new RoomActionCommand();
				roomActionCommand.m_Command = RoomActionCommand.eActionCode.Event;
				selectChara.m_RoomObj.RequestDebugCommand(roomActionCommand);
			}
		}

		// Token: 0x06001C06 RID: 7174 RVA: 0x00094CF4 File Offset: 0x000930F4
		private void CallbackMoveXUp()
		{
			int num = int.Parse(this.m_MovePosX.text);
			num++;
			if (num >= 9)
			{
				num = 0;
			}
			this.m_MovePosX.text = num.ToString();
			this.JumpCharacter();
		}

		// Token: 0x06001C07 RID: 7175 RVA: 0x00094D40 File Offset: 0x00093140
		private void CallbackMoveXDown()
		{
			int num = int.Parse(this.m_MovePosX.text);
			num--;
			if (num < 0)
			{
				num = 8;
			}
			this.m_MovePosX.text = num.ToString();
			this.JumpCharacter();
		}

		// Token: 0x06001C08 RID: 7176 RVA: 0x00094D8C File Offset: 0x0009318C
		private void CallbackMoveYUp()
		{
			int num = int.Parse(this.m_MovePosY.text);
			num++;
			if (num >= 9)
			{
				num = 0;
			}
			this.m_MovePosY.text = num.ToString();
			this.JumpCharacter();
		}

		// Token: 0x06001C09 RID: 7177 RVA: 0x00094DD8 File Offset: 0x000931D8
		private void CallbackMoveYDown()
		{
			int num = int.Parse(this.m_MovePosY.text);
			num--;
			if (num < 0)
			{
				num = 0;
			}
			this.m_MovePosY.text = num.ToString();
			this.JumpCharacter();
		}

		// Token: 0x06001C0A RID: 7178 RVA: 0x00094E24 File Offset: 0x00093224
		private void CallbackDebugMode(bool fmode)
		{
			RoomCharaPakage selectChara = this.GetSelectChara();
			if (selectChara != null && selectChara.m_RoomObj != null)
			{
				selectChara.m_RoomObj.m_DebugKey = fmode;
			}
		}

		// Token: 0x06001C0B RID: 7179 RVA: 0x00094E5C File Offset: 0x0009325C
		private void MoveCharacter()
		{
			RoomCharaPakage selectChara = this.GetSelectChara();
			if (selectChara != null && selectChara.m_RoomObj != null)
			{
				RoomActionCommand roomActionCommand = new RoomActionCommand();
				roomActionCommand.m_Command = RoomActionCommand.eActionCode.Walk;
				roomActionCommand.m_ParamI1 = int.Parse(this.m_MovePosX.text);
				roomActionCommand.m_ParamI2 = int.Parse(this.m_MovePosY.text);
				selectChara.m_RoomObj.RequestDebugCommand(roomActionCommand);
			}
		}

		// Token: 0x06001C0C RID: 7180 RVA: 0x00094ECC File Offset: 0x000932CC
		private void JumpCharacter()
		{
			RoomCharaPakage selectChara = this.GetSelectChara();
			if (selectChara != null && selectChara.m_RoomObj != null)
			{
				RoomActionCommand roomActionCommand = new RoomActionCommand();
				roomActionCommand.m_Command = RoomActionCommand.eActionCode.Jump;
				roomActionCommand.m_ParamI1 = int.Parse(this.m_MovePosX.text);
				roomActionCommand.m_ParamI2 = int.Parse(this.m_MovePosY.text);
				selectChara.m_RoomObj.RequestDebugCommand(roomActionCommand);
			}
		}

		// Token: 0x06001C0D RID: 7181 RVA: 0x00094F3C File Offset: 0x0009333C
		private void SetUpCharaViewForm()
		{
			RoomCharaPakage selectChara = this.GetSelectChara();
			if (selectChara != null && selectChara.m_RoomObj != null)
			{
				this.m_CharaName.text = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB.GetParam(selectChara.m_Named).m_FullName;
				this.m_MovePosX.text = selectChara.m_RoomObj.BlockData.PosX.ToString();
				this.m_MovePosY.text = selectChara.m_RoomObj.BlockData.PosY.ToString();
			}
		}

		// Token: 0x06001C0E RID: 7182 RVA: 0x00094FE6 File Offset: 0x000933E6
		public override void SetActive(bool factive)
		{
			this.m_Form.gameObject.SetActive(factive);
		}

		// Token: 0x04002305 RID: 8965
		private RoomBuilder m_Builder;

		// Token: 0x04002306 RID: 8966
		private int m_SelectNo;

		// Token: 0x04002307 RID: 8967
		private Text m_MovePosX;

		// Token: 0x04002308 RID: 8968
		private Text m_MovePosY;

		// Token: 0x04002309 RID: 8969
		private Text m_CharaName;
	}
}
