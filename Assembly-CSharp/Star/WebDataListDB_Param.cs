﻿using System;

namespace Star
{
	// Token: 0x02000266 RID: 614
	[Serializable]
	public struct WebDataListDB_Param
	{
		// Token: 0x040013E0 RID: 5088
		public int m_ID;

		// Token: 0x040013E1 RID: 5089
		public string m_WebAdress;
	}
}
