﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004FE RID: 1278
	public class MissionFuncTown : IMissionFunction
	{
		// Token: 0x0600192B RID: 6443 RVA: 0x00082C6C File Offset: 0x0008106C
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			switch (pparam.m_ExtensionScriptID)
			{
			case 0:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 1:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 2:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 3:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 4:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 5:
				pparam.m_Rate++;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			}
			return result;
		}
	}
}
