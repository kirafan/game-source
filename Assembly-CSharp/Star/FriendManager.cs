﻿using System;
using System.Collections.Generic;
using FriendRequestTypes;
using FriendResponseTypes;
using Meige;
using Star.UI;
using UnityEngine;
using WWWTypes;

namespace Star
{
	// Token: 0x020003BE RID: 958
	public sealed class FriendManager
	{
		// Token: 0x06001225 RID: 4645 RVA: 0x0005FD80 File Offset: 0x0005E180
		public int GetFriendListNum(FriendDefine.eType type)
		{
			int num = 0;
			for (int i = 0; i < this.m_FriendList.Count; i++)
			{
				if (this.m_FriendList[i].m_Type == type)
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06001226 RID: 4646 RVA: 0x0005FDC7 File Offset: 0x0005E1C7
		public int GetFriendListNum()
		{
			return this.m_FriendList.Count;
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x0005FDD4 File Offset: 0x0005E1D4
		public FriendManager.FriendData GetFriendDataAt(int index)
		{
			if (index >= 0 && index < this.m_FriendList.Count)
			{
				return this.m_FriendList[index];
			}
			return null;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x0005FDFC File Offset: 0x0005E1FC
		public FriendManager.FriendData GetFriendData(long id)
		{
			for (int i = 0; i < this.m_FriendList.Count; i++)
			{
				if (this.m_FriendList[i].m_ID == id)
				{
					return this.m_FriendList[i];
				}
			}
			return null;
		}

		// Token: 0x06001229 RID: 4649 RVA: 0x0005FE4C File Offset: 0x0005E24C
		public FriendManager.FriendData GetFriendDataByMngFriendID(long mngFriendID)
		{
			for (int i = 0; i < this.m_FriendList.Count; i++)
			{
				if (this.m_FriendList[i].m_MngFriendID == mngFriendID)
				{
					return this.m_FriendList[i];
				}
			}
			return null;
		}

		// Token: 0x0600122A RID: 4650 RVA: 0x0005FE9A File Offset: 0x0005E29A
		public void RefreshFriendSupportList()
		{
			this.m_FriendSupportDataList = null;
		}

		// Token: 0x0600122B RID: 4651 RVA: 0x0005FEA4 File Offset: 0x0005E2A4
		public bool FilterFriendList(eTitleType filterTitleType, long partyMngID, ref List<FriendManager.FriendData> friendDatas, ref List<UserSupportData> userSupportDatas)
		{
			if (friendDatas == null)
			{
				friendDatas = new List<FriendManager.FriendData>();
			}
			else
			{
				friendDatas.Clear();
			}
			if (userSupportDatas == null)
			{
				userSupportDatas = new List<UserSupportData>();
			}
			else
			{
				userSupportDatas.Clear();
			}
			List<eCharaNamedType> list = new List<eCharaNamedType>();
			if (partyMngID != -1L)
			{
				UserBattlePartyData userBattlePartyData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserBattlePartyData(partyMngID);
				for (int i = 0; i < userBattlePartyData.GetSlotNum(); i++)
				{
					long memberAt = userBattlePartyData.GetMemberAt(i);
					if (memberAt != -1L)
					{
						eCharaNamedType namedType = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(memberAt).Param.NamedType;
						list.Add(namedType);
					}
				}
			}
			List<FriendManager.FriendSupportData> list2 = new List<FriendManager.FriendSupportData>();
			CharacterListDB charaListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB;
			NamedListDB namedListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.NamedListDB;
			for (int j = 0; j < this.m_FriendList.Count; j++)
			{
				FriendManager.FriendData friendData = this.m_FriendList[j];
				if (friendData != null)
				{
					if (friendData.m_UserSupportData != null)
					{
						int num = -1;
						if (filterTitleType == eTitleType.None)
						{
							if (this.m_FriendSupportDataList != null)
							{
								for (int k = 0; k < this.m_FriendSupportDataList.Count; k++)
								{
									if (this.m_FriendSupportDataList[k].m_FriendData == friendData)
									{
										num = this.m_FriendSupportDataList[k].m_SupportIdx;
										break;
									}
								}
							}
							if (num == -1)
							{
								num = UnityEngine.Random.Range(0, friendData.m_UserSupportData.Count);
							}
						}
						if (num == -1)
						{
							num = 0;
						}
						num--;
						bool flag = false;
						for (int l = 0; l < friendData.m_UserSupportData.Count; l++)
						{
							num++;
							if (num >= friendData.m_UserSupportData.Count)
							{
								num = 0;
							}
							UserSupportData userSupportData = friendData.m_UserSupportData[num];
							CharacterListDB_Param param = charaListDB.GetParam(userSupportData.CharaID);
							eCharaNamedType namedType2 = (eCharaNamedType)param.m_NamedType;
							NamedListDB_Param param2 = namedListDB.GetParam(namedType2);
							if (filterTitleType == eTitleType.None || param2.m_TitleType == (int)filterTitleType)
							{
								bool flag2 = false;
								for (int m = 0; m < list.Count; m++)
								{
									if (namedType2 == list[m])
									{
										flag2 = true;
										break;
									}
								}
								if (flag2)
								{
									if (filterTitleType != eTitleType.None)
									{
										break;
									}
								}
								else if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.SupportSelect, UISettings.eFilterCategory.Class, param.m_Class))
								{
									if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.SupportSelect, UISettings.eFilterCategory.Element, param.m_Element))
									{
										if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetFilterFlagWithAll(UISettings.eUsingType_Filter.SupportSelect, UISettings.eFilterCategory.Rare, param.m_Rare))
										{
											flag = true;
											break;
										}
									}
								}
							}
						}
						if (flag && num >= 0)
						{
							list2.Add(new FriendManager.FriendSupportData(friendData, friendData.m_UserSupportData[num], num));
						}
					}
				}
			}
			this.m_FriendSupportDataList = list2;
			this.m_FriendSupportDataList.Sort(new Comparison<FriendManager.FriendSupportData>(this.SortCompare));
			for (int n = 0; n < this.m_FriendSupportDataList.Count; n++)
			{
				friendDatas.Add(this.m_FriendSupportDataList[n].m_FriendData);
				userSupportDatas.Add(this.m_FriendSupportDataList[n].m_UserSupportData);
			}
			return friendDatas.Count > 0;
		}

		// Token: 0x0600122C RID: 4652 RVA: 0x0006024C File Offset: 0x0005E64C
		private int SortCompare(FriendManager.FriendSupportData r, FriendManager.FriendSupportData l)
		{
			int num = -1;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamOrderType(UISettings.eUsingType_Sort.SupportSelect) == 1)
			{
				num = 1;
			}
			if (this.GetSortKey(r) < this.GetSortKey(l))
			{
				return num;
			}
			if (this.GetSortKey(r) > this.GetSortKey(l))
			{
				return -num;
			}
			return 0;
		}

		// Token: 0x0600122D RID: 4653 RVA: 0x000602A0 File Offset: 0x0005E6A0
		private long GetSortKey(FriendManager.FriendSupportData data)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(data.m_UserSupportData.CharaID);
			EditUtility.OutputCharaParam outputCharaParam = EditUtility.CalcCharaParam(data.m_UserSupportData);
			switch (SingletonMonoBehaviour<GameSystem>.Inst.UISettings.GetSortParamValue(UISettings.eUsingType_Sort.SupportSelect))
			{
			case 0:
				return (long)data.m_UserSupportData.Lv;
			case 1:
				return (long)param.m_Rare;
			case 2:
				return (long)param.m_Cost;
			case 3:
				return (long)outputCharaParam.Hp;
			case 4:
				return (long)outputCharaParam.Atk;
			case 5:
				return (long)outputCharaParam.Mgc;
			case 6:
				return (long)outputCharaParam.Def;
			case 7:
				return (long)outputCharaParam.MDef;
			case 8:
				return (long)outputCharaParam.Spd;
			default:
				return 0L;
			}
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x0600122E RID: 4654 RVA: 0x00060370 File Offset: 0x0005E770
		// (remove) Token: 0x0600122F RID: 4655 RVA: 0x000603A8 File Offset: 0x0005E7A8
		private event Action<bool> m_OnResponse;

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x06001230 RID: 4656 RVA: 0x000603DE File Offset: 0x0005E7DE
		public FriendManager.FriendData SearchedFriendData
		{
			get
			{
				return this.m_SearchedFriendData;
			}
		}

		// Token: 0x06001231 RID: 4657 RVA: 0x000603E6 File Offset: 0x0005E7E6
		private void OnConfirmRequestError()
		{
			this.m_OnResponse.Call(true);
			this.m_OnResponse = null;
		}

		// Token: 0x06001232 RID: 4658 RVA: 0x000603FC File Offset: 0x0005E7FC
		public bool Request_GetAll(FriendDefine.eGetAllType type, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			MeigewwwParam all = FriendRequest.GetAll(new FriendRequestTypes.GetAll
			{
				type = (int)type
			}, new MeigewwwParam.Callback(this.OnResponse_GetAll));
			NetworkQueueManager.Request(all);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001233 RID: 4659 RVA: 0x00060458 File Offset: 0x0005E858
		private void OnResponse_GetAll(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.GetAll all = FriendResponse.GetAll(wwwParam, ResponseCommon.DialogType.None, null);
			if (all == null)
			{
				return;
			}
			ResultCode result = all.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
			}
			else
			{
				List<FriendManager.FriendData> list = new List<FriendManager.FriendData>();
				List<FriendManager.FriendData> list2 = new List<FriendManager.FriendData>();
				FriendManager.wwwConvert(all.friends, list);
				FriendManager.wwwConvert(all.guests, list2);
				this.m_FriendList.Clear();
				this.m_FriendList.AddRange(list);
				this.m_FriendList.AddRange(list2);
				this.UpdateNoticeFriendCount();
				this.m_OnResponse.Call(false);
			}
		}

		// Token: 0x06001234 RID: 4660 RVA: 0x00060504 File Offset: 0x0005E904
		public bool Request_Propose(long playerID, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			this.m_ProposePlayerID = playerID;
			MeigewwwParam wwwParam = FriendRequest.Propose(new FriendRequestTypes.Propose
			{
				targetPlayerId = playerID
			}, new MeigewwwParam.Callback(this.OnResponse_Propose));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001235 RID: 4661 RVA: 0x00060568 File Offset: 0x0005E968
		private void OnResponse_Propose(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Propose propose = FriendResponse.Propose(wwwParam, ResponseCommon.DialogType.None, null);
			if (propose == null)
			{
				return;
			}
			ResultCode result = propose.GetResult();
			if (result != ResultCode.NOT_YET_FRIEND && result != ResultCode.ALREADY_FRIEND)
			{
				if (result == ResultCode.SUCCESS)
				{
					FriendManager.FriendData friendData = this.GetFriendData(this.m_ProposePlayerID);
					if (friendData != null)
					{
						friendData.m_Type = FriendDefine.eType.SelfPropose;
						friendData.m_MngFriendID = propose.managedFriendId;
					}
					else if (this.m_SearchedFriendData != null && this.m_SearchedFriendData.m_ID == this.m_ProposePlayerID)
					{
						this.m_SearchedFriendData.m_Type = FriendDefine.eType.SelfPropose;
						this.m_SearchedFriendData.m_MngFriendID = propose.managedFriendId;
						this.m_FriendList.Add(this.m_SearchedFriendData);
					}
					this.m_OnResponse.Call(false);
					return;
				}
				if (result != ResultCode.PLAYER_NOT_FOUND && result != ResultCode.FRIEND_LIMIT)
				{
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
		}

		// Token: 0x06001236 RID: 4662 RVA: 0x0006066C File Offset: 0x0005EA6C
		public bool Request_Accept(long mngFriendID, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			this.m_AcceptMngFriendID = mngFriendID;
			MeigewwwParam wwwParam = FriendRequest.Accept(new FriendRequestTypes.Accept
			{
				managedFriendId = mngFriendID
			}, new MeigewwwParam.Callback(this.OnResponse_Accept));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001237 RID: 4663 RVA: 0x000606D0 File Offset: 0x0005EAD0
		private void OnResponse_Accept(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Accept accept = FriendResponse.Accept(wwwParam, ResponseCommon.DialogType.None, null);
			if (accept == null)
			{
				return;
			}
			ResultCode result = accept.GetResult();
			if (result != ResultCode.NOT_YET_FRIEND && result != ResultCode.ALREADY_FRIEND)
			{
				if (result == ResultCode.SUCCESS)
				{
					FriendManager.FriendData friendDataByMngFriendID = this.GetFriendDataByMngFriendID(this.m_AcceptMngFriendID);
					if (friendDataByMngFriendID != null)
					{
						friendDataByMngFriendID.m_Type = FriendDefine.eType.Friend;
					}
					else if (this.m_SearchedFriendData != null)
					{
						this.m_SearchedFriendData.m_Type = FriendDefine.eType.Friend;
						this.m_FriendList.Add(this.m_SearchedFriendData);
					}
					this.UpdateNoticeFriendCount();
					this.m_OnResponse.Call(false);
					return;
				}
				if (result != ResultCode.FRIEND_LIMIT)
				{
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
		}

		// Token: 0x06001238 RID: 4664 RVA: 0x0006079C File Offset: 0x0005EB9C
		public bool Request_Refuse(long mngFriendID, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			this.m_RefuseMngFriendID = mngFriendID;
			MeigewwwParam wwwParam = FriendRequest.Refuse(new FriendRequestTypes.Refuse
			{
				managedFriendId = mngFriendID
			}, new MeigewwwParam.Callback(this.OnResponse_Refuse));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001239 RID: 4665 RVA: 0x00060800 File Offset: 0x0005EC00
		private void OnResponse_Refuse(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Refuse refuse = FriendResponse.Refuse(wwwParam, ResponseCommon.DialogType.None, null);
			if (refuse == null)
			{
				return;
			}
			ResultCode result = refuse.GetResult();
			if (result != ResultCode.NOT_YET_FRIEND && result != ResultCode.ALREADY_FRIEND)
			{
				if (result == ResultCode.SUCCESS)
				{
					for (int i = 0; i < this.m_FriendList.Count; i++)
					{
						if (this.m_FriendList[i].m_MngFriendID == this.m_RefuseMngFriendID)
						{
							this.m_FriendList[i].m_Type = FriendDefine.eType.Guest;
							break;
						}
					}
					this.UpdateNoticeFriendCount();
					this.m_OnResponse.Call(false);
					return;
				}
				if (result != ResultCode.FRIEND_LIMIT)
				{
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
		}

		// Token: 0x0600123A RID: 4666 RVA: 0x000608D4 File Offset: 0x0005ECD4
		public bool Request_Cancel(long mngFriendID, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			this.m_CancelMngFriendID = mngFriendID;
			MeigewwwParam wwwParam = FriendRequest.Cancel(new FriendRequestTypes.Cancel
			{
				managedFriendId = mngFriendID
			}, new MeigewwwParam.Callback(this.OnResponse_Cancel));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x0600123B RID: 4667 RVA: 0x00060938 File Offset: 0x0005ED38
		private void OnResponse_Cancel(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Cancel cancel = FriendResponse.Cancel(wwwParam, ResponseCommon.DialogType.None, null);
			if (cancel == null)
			{
				return;
			}
			ResultCode result = cancel.GetResult();
			if (result != ResultCode.NOT_YET_FRIEND && result != ResultCode.ALREADY_FRIEND)
			{
				if (result == ResultCode.SUCCESS)
				{
					for (int i = 0; i < this.m_FriendList.Count; i++)
					{
						if (this.m_FriendList[i].m_MngFriendID == this.m_CancelMngFriendID)
						{
							this.m_FriendList[i].m_Type = FriendDefine.eType.Guest;
							this.m_FriendList[i].m_MngFriendID = -1L;
							break;
						}
					}
					this.m_OnResponse.Call(false);
					return;
				}
				if (result != ResultCode.FRIEND_LIMIT)
				{
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
		}

		// Token: 0x0600123C RID: 4668 RVA: 0x00060A18 File Offset: 0x0005EE18
		public bool Request_Terminate(long mngFriendID, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			this.m_TerminateMngFriendID = mngFriendID;
			MeigewwwParam wwwParam = FriendRequest.Terminate(new FriendRequestTypes.Terminate
			{
				managedFriendId = mngFriendID
			}, new MeigewwwParam.Callback(this.OnResponse_Terminate));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x0600123D RID: 4669 RVA: 0x00060A7C File Offset: 0x0005EE7C
		private void OnResponse_Terminate(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Terminate terminate = FriendResponse.Terminate(wwwParam, ResponseCommon.DialogType.None, null);
			if (terminate == null)
			{
				return;
			}
			ResultCode result = terminate.GetResult();
			if (result != ResultCode.NOT_YET_FRIEND && result != ResultCode.ALREADY_FRIEND)
			{
				if (result == ResultCode.SUCCESS)
				{
					for (int i = 0; i < this.m_FriendList.Count; i++)
					{
						if (this.m_FriendList[i].m_MngFriendID == this.m_TerminateMngFriendID)
						{
							this.m_FriendList.RemoveAt(i);
							break;
						}
					}
					this.m_OnResponse.Call(false);
					return;
				}
				if (result != ResultCode.FRIEND_LIMIT)
				{
				}
			}
			APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
		}

		// Token: 0x0600123E RID: 4670 RVA: 0x00060B44 File Offset: 0x0005EF44
		public bool Request_Search(string myCode, Action<bool> onResponse)
		{
			this.m_OnResponse = onResponse;
			MeigewwwParam wwwParam = FriendRequest.Search(new FriendRequestTypes.Search
			{
				myCode = myCode
			}, new MeigewwwParam.Callback(this.OnResponse_Search));
			NetworkQueueManager.Request(wwwParam);
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x0600123F RID: 4671 RVA: 0x00060BA0 File Offset: 0x0005EFA0
		private void OnResponse_Search(MeigewwwParam wwwParam)
		{
			FriendResponseTypes.Search search = FriendResponse.Search(wwwParam, ResponseCommon.DialogType.None, null);
			if (search == null)
			{
				return;
			}
			ResultCode result = search.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.PLAYER_NOT_FOUND)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), new Action(this.OnConfirmRequestError));
				}
				else
				{
					this.m_SearchedFriendData = null;
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.FriendConfirmSearchTitle, eText_MessageDB.FriendConfirmSearchNotFound, null);
					this.OnConfirmRequestError();
				}
			}
			else
			{
				FriendManager.wwwConvert(search.friend, this.m_SearchedFriendData);
				this.m_OnResponse.Call(false);
			}
		}

		// Token: 0x06001240 RID: 4672 RVA: 0x00060C48 File Offset: 0x0005F048
		private static void wwwConvert(FriendList[] src, List<FriendManager.FriendData> dest)
		{
			dest.Clear();
			for (int i = 0; i < src.Length; i++)
			{
				FriendManager.FriendData friendData = new FriendManager.FriendData();
				FriendManager.wwwConvert(src[i], friendData);
				dest.Add(friendData);
			}
		}

		// Token: 0x06001241 RID: 4673 RVA: 0x00060C88 File Offset: 0x0005F088
		private static void wwwConvert(FriendList src, FriendManager.FriendData dest)
		{
			if (src.state == 2)
			{
				dest.m_Type = FriendDefine.eType.Friend;
			}
			else if (src.state == 1)
			{
				if (src.direction == 2)
				{
					dest.m_Type = FriendDefine.eType.OpponentPropose;
				}
				else
				{
					dest.m_Type = FriendDefine.eType.SelfPropose;
				}
			}
			else
			{
				dest.m_Type = FriendDefine.eType.Guest;
			}
			dest.m_MngFriendID = src.managedFriendId;
			dest.m_ID = src.playerId;
			dest.m_Name = src.name;
			dest.m_Comment = src.comment;
			dest.m_Lv = src.level;
			dest.m_MyCode = src.myCode;
			dest.m_LastLoginAt = src.lastLoginAt;
			dest.m_SupportPartyName = src.supportName;
			dest.m_SupportLimit = src.supportLimit;
			if (src.fieldPartyMember != null)
			{
				dest.m_LeaderCharaData = new UserSupportData();
				APIUtility.wwwToUserData(src.fieldPartyMember, dest.m_LeaderCharaData);
			}
			else
			{
				dest.m_LeaderCharaData = null;
			}
			dest.m_UserSupportData = new List<UserSupportData>();
			if (src.supportCharacters != null)
			{
				for (int i = 0; i < src.supportCharacters.Length; i++)
				{
					UserSupportData userSupportData = new UserSupportData();
					APIUtility.wwwToUserData(src.supportCharacters[i], userSupportData);
					dest.m_UserSupportData.Add(userSupportData);
				}
			}
		}

		// Token: 0x06001242 RID: 4674 RVA: 0x00060DD2 File Offset: 0x0005F1D2
		private void UpdateNoticeFriendCount()
		{
			if (SingletonMonoBehaviour<GameSystem>.Inst != null)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.NoticeFriendCount = this.GetFriendListNum(FriendDefine.eType.OpponentPropose);
			}
		}

		// Token: 0x040018A5 RID: 6309
		private List<FriendManager.FriendData> m_FriendList = new List<FriendManager.FriendData>();

		// Token: 0x040018A6 RID: 6310
		private List<FriendManager.FriendSupportData> m_FriendSupportDataList;

		// Token: 0x040018A8 RID: 6312
		private long m_ProposePlayerID = -1L;

		// Token: 0x040018A9 RID: 6313
		private long m_AcceptMngFriendID = -1L;

		// Token: 0x040018AA RID: 6314
		private long m_RefuseMngFriendID = -1L;

		// Token: 0x040018AB RID: 6315
		private long m_CancelMngFriendID = -1L;

		// Token: 0x040018AC RID: 6316
		private long m_TerminateMngFriendID = -1L;

		// Token: 0x040018AD RID: 6317
		private FriendManager.FriendData m_SearchedFriendData = new FriendManager.FriendData();

		// Token: 0x020003BF RID: 959
		public class FriendData
		{
			// Token: 0x06001244 RID: 4676 RVA: 0x00060E04 File Offset: 0x0005F204
			public UserSupportData GetUserSupportData(long charaMngID)
			{
				for (int i = 0; i < this.m_UserSupportData.Count; i++)
				{
					if (this.m_UserSupportData[i].MngID == charaMngID)
					{
						return this.m_UserSupportData[i];
					}
				}
				return null;
			}

			// Token: 0x040018AE RID: 6318
			public FriendDefine.eType m_Type;

			// Token: 0x040018AF RID: 6319
			public long m_MngFriendID;

			// Token: 0x040018B0 RID: 6320
			public long m_ID;

			// Token: 0x040018B1 RID: 6321
			public string m_Name;

			// Token: 0x040018B2 RID: 6322
			public string m_Comment;

			// Token: 0x040018B3 RID: 6323
			public int m_Lv;

			// Token: 0x040018B4 RID: 6324
			public string m_MyCode;

			// Token: 0x040018B5 RID: 6325
			public DateTime m_LastLoginAt;

			// Token: 0x040018B6 RID: 6326
			public UserSupportData m_LeaderCharaData;

			// Token: 0x040018B7 RID: 6327
			public List<UserSupportData> m_UserSupportData;

			// Token: 0x040018B8 RID: 6328
			public string m_SupportPartyName;

			// Token: 0x040018B9 RID: 6329
			public int m_SupportLimit;
		}

		// Token: 0x020003C0 RID: 960
		public class FriendSupportData
		{
			// Token: 0x06001245 RID: 4677 RVA: 0x00060E52 File Offset: 0x0005F252
			public FriendSupportData(FriendManager.FriendData friend, UserSupportData support, int supportIdx)
			{
				this.m_FriendData = friend;
				this.m_UserSupportData = support;
				this.m_SupportIdx = supportIdx;
			}

			// Token: 0x040018BA RID: 6330
			public FriendManager.FriendData m_FriendData;

			// Token: 0x040018BB RID: 6331
			public UserSupportData m_UserSupportData;

			// Token: 0x040018BC RID: 6332
			public int m_SupportIdx = -1;
		}
	}
}
