﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000729 RID: 1833
	public class TownCharaModel : MonoBehaviour
	{
		// Token: 0x06002424 RID: 9252 RVA: 0x000C1F08 File Offset: 0x000C0308
		public void SetCharaModel(ITownObjectHandler pchara, long fmanageid, float fsize, Vector3 fcenter)
		{
			this.m_BaseHandle = pchara;
			this.m_Handle = SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.LoadAsyncCharaIcon(UserCharaUtil.GetCharaParam(fmanageid).m_CharaID);
			this.m_Center = fcenter;
			this.m_BaseSize = fsize;
			this.m_FrameNo = 0;
		}

		// Token: 0x06002425 RID: 9253 RVA: 0x000C1F55 File Offset: 0x000C0355
		public void ChangeFrame(int frameno)
		{
			if (frameno == 1)
			{
				frameno = 2;
			}
			if (this.m_FrameNo != frameno)
			{
				this.m_FrameNo = frameno;
				this.ChangeFrame();
			}
		}

		// Token: 0x06002426 RID: 9254 RVA: 0x000C1F7C File Offset: 0x000C037C
		private void SetStarPos()
		{
			this.m_StarTime = 0f;
			float num = UnityEngine.Random.Range(0f, 6.2831855f);
			float num2 = UnityEngine.Random.Range(40f, 70f);
			Vector3 zero = Vector3.zero;
			zero.x = Mathf.Cos(num) * num2;
			zero.y = Mathf.Sin(num) * num2;
			this.m_Star.transform.localPosition = zero;
			num = UnityEngine.Random.Range(0f, 360f);
			this.m_Star.transform.localEulerAngles = new Vector3(0f, 0f, num);
		}

		// Token: 0x06002427 RID: 9255 RVA: 0x000C201C File Offset: 0x000C041C
		private void SetUpModel(Sprite psprite, float fsize, Vector3 fcenter)
		{
			Transform transform = TownUtility.FindTransform(base.transform, "Image", 10);
			if (transform != null)
			{
				Image component = transform.gameObject.GetComponent<Image>();
				if (component != null)
				{
					component.sprite = psprite;
				}
			}
			this.m_FrameObj = new GameObject[4];
			transform = TownUtility.FindTransform(base.transform, "Lv1", 10);
			if (transform != null)
			{
				this.m_FrameObj[0] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "Lv3", 10);
			if (transform != null)
			{
				this.m_FrameObj[2] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "Lv4", 10);
			if (transform != null)
			{
				this.m_FrameObj[3] = transform.gameObject;
			}
			transform = TownUtility.FindTransform(base.transform, "Star", 10);
			if (transform != null)
			{
				this.m_Star = transform.gameObject;
				this.SetStarPos();
			}
			SphereCollider componentInChildren = base.gameObject.GetComponentInChildren<SphereCollider>();
			if (componentInChildren != null)
			{
				TownObjectConnect townObjectConnect = componentInChildren.gameObject.AddComponent<TownObjectConnect>();
				townObjectConnect.m_Link = this.m_BaseHandle;
			}
			this.ChangeFrame();
			int childCount = base.transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				base.transform.GetChild(i).gameObject.SetActive(this.m_ActiveView);
			}
		}

		// Token: 0x06002428 RID: 9256 RVA: 0x000C21A4 File Offset: 0x000C05A4
		private void ChangeFrame()
		{
			if (this.m_FrameObj != null)
			{
				int num = this.m_FrameObj.Length;
				for (int i = 0; i < num; i++)
				{
					if (i == this.m_FrameNo)
					{
						this.m_FrameObj[i].SetActive(true);
					}
					else if (this.m_FrameObj[i] != null)
					{
						this.m_FrameObj[i].SetActive(false);
					}
				}
			}
		}

		// Token: 0x06002429 RID: 9257 RVA: 0x000C2218 File Offset: 0x000C0618
		public void SetRenderActive(bool factive)
		{
			this.m_ActiveView = factive;
			if (this.m_SetUp)
			{
				int childCount = base.transform.childCount;
				for (int i = 0; i < childCount; i++)
				{
					base.transform.GetChild(i).gameObject.SetActive(factive);
				}
			}
		}

		// Token: 0x0600242A RID: 9258 RVA: 0x000C226C File Offset: 0x000C066C
		public void PlayMotion(int findex)
		{
		}

		// Token: 0x0600242B RID: 9259 RVA: 0x000C226E File Offset: 0x000C066E
		public bool IsPlaying(int findex)
		{
			return false;
		}

		// Token: 0x0600242C RID: 9260 RVA: 0x000C2271 File Offset: 0x000C0671
		public bool IsAvailable()
		{
			return this.m_Handle.IsAvailable();
		}

		// Token: 0x0600242D RID: 9261 RVA: 0x000C2280 File Offset: 0x000C0680
		private void Update()
		{
			if (this.m_Star != null)
			{
				this.m_StarTime += Time.deltaTime;
				if (this.m_StarTime >= 0.75f)
				{
					this.SetStarPos();
				}
				this.m_Star.transform.localScale = Vector3.one * this.m_StarTime;
			}
			else if (this.m_Handle.IsAvailable())
			{
				this.SetUpModel(this.m_Handle.Obj, this.m_BaseSize, this.m_Center);
				this.m_SetUp = true;
			}
		}

		// Token: 0x0600242E RID: 9262 RVA: 0x000C231F File Offset: 0x000C071F
		public void ChangeLayer(int flayerid)
		{
		}

		// Token: 0x0600242F RID: 9263 RVA: 0x000C2324 File Offset: 0x000C0724
		private void OnDestroy()
		{
			if (this.m_Handle != null)
			{
				if (SingletonMonoBehaviour<GameSystem>.Inst != null)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SpriteMng.Unload(this.m_Handle);
				}
				this.m_Handle = null;
			}
			this.m_BaseHandle = null;
			this.m_Star = null;
			if (this.m_FrameObj != null)
			{
				for (int i = 0; i < this.m_FrameObj.Length; i++)
				{
					this.m_FrameObj[i] = null;
				}
				this.m_FrameObj = null;
			}
		}

		// Token: 0x04002B24 RID: 11044
		private ITownObjectHandler m_BaseHandle;

		// Token: 0x04002B25 RID: 11045
		private SpriteHandler m_Handle;

		// Token: 0x04002B26 RID: 11046
		private float m_BaseSize;

		// Token: 0x04002B27 RID: 11047
		private Vector3 m_Center;

		// Token: 0x04002B28 RID: 11048
		private int m_FrameNo;

		// Token: 0x04002B29 RID: 11049
		private GameObject[] m_FrameObj;

		// Token: 0x04002B2A RID: 11050
		private GameObject m_Star;

		// Token: 0x04002B2B RID: 11051
		private float m_StarTime;

		// Token: 0x04002B2C RID: 11052
		private bool m_ActiveView;

		// Token: 0x04002B2D RID: 11053
		private bool m_SetUp;
	}
}
