﻿using System;

namespace Star
{
	// Token: 0x0200060B RID: 1547
	public class RoomEventQue
	{
		// Token: 0x06001E58 RID: 7768 RVA: 0x000A39E2 File Offset: 0x000A1DE2
		public RoomEventQue(int ftablenum)
		{
			this.m_Table = new IRoomEventCommand[ftablenum];
			this.m_Num = 0;
			this.m_Max = ftablenum;
		}

		// Token: 0x06001E59 RID: 7769 RVA: 0x000A3A04 File Offset: 0x000A1E04
		public void PushComCommand(IRoomEventCommand pcmd)
		{
			if (this.m_Num < this.m_Max)
			{
				this.m_Table[this.m_Num] = pcmd;
				this.m_Num++;
			}
		}

		// Token: 0x06001E5A RID: 7770 RVA: 0x000A3A33 File Offset: 0x000A1E33
		public int GetComCommandNum()
		{
			return this.m_Num;
		}

		// Token: 0x06001E5B RID: 7771 RVA: 0x000A3A3B File Offset: 0x000A1E3B
		public IRoomEventCommand GetComCommand(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x06001E5C RID: 7772 RVA: 0x000A3A48 File Offset: 0x000A1E48
		public void Flush()
		{
			int num = 0;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (!this.m_Table[i].m_Enable)
				{
					this.m_Table[i] = null;
					num++;
				}
			}
			if (num != 0)
			{
				int i = 0;
				while (i < this.m_Num)
				{
					if (this.m_Table[i] == null)
					{
						for (int j = i + 1; j < this.m_Num; j++)
						{
							this.m_Table[j - 1] = this.m_Table[j];
						}
						this.m_Num--;
						this.m_Table[this.m_Num] = null;
					}
					else
					{
						i++;
					}
				}
			}
		}

		// Token: 0x040024E9 RID: 9449
		public int m_Num;

		// Token: 0x040024EA RID: 9450
		public int m_Max;

		// Token: 0x040024EB RID: 9451
		public IRoomEventCommand[] m_Table;
	}
}
