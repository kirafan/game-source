﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x020005A5 RID: 1445
	public class RoomComPlayerPlaceObj : IFldNetComModule
	{
		// Token: 0x06001BE8 RID: 7144 RVA: 0x00093BE7 File Offset: 0x00091FE7
		public RoomComPlayerPlaceObj(long fplayerid) : base(IFldNetComManager.Instance)
		{
			this.m_PlayerId = fplayerid;
			this.m_Step = RoomComPlayerPlaceObj.eStep.GetState;
		}

		// Token: 0x06001BE9 RID: 7145 RVA: 0x00093C04 File Offset: 0x00092004
		public bool SetUp()
		{
			RoomComAPIFriendGetAll roomComAPIFriendGetAll = new RoomComAPIFriendGetAll();
			roomComAPIFriendGetAll.playerId = this.m_PlayerId;
			this.m_NetMng.SendCom(roomComAPIFriendGetAll, new INetComHandle.ResponseCallbak(this.CallbackSetUp), false);
			return true;
		}

		// Token: 0x06001BEA RID: 7146 RVA: 0x00093C40 File Offset: 0x00092040
		private void CallbackSetUp(INetComHandle phandle)
		{
			GetAll getAll = phandle.GetResponse() as GetAll;
			if (getAll != null)
			{
				WWWComUtil.SetFriendRoomPlacementKey(this.m_PlayerId, getAll.managedRooms);
				this.m_Step = RoomComPlayerPlaceObj.eStep.End;
			}
		}

		// Token: 0x06001BEB RID: 7147 RVA: 0x00093C78 File Offset: 0x00092078
		public override bool UpFunc()
		{
			bool result = true;
			RoomComPlayerPlaceObj.eStep step = this.m_Step;
			if (step != RoomComPlayerPlaceObj.eStep.GetState)
			{
				if (step != RoomComPlayerPlaceObj.eStep.WaitCheck)
				{
					if (step == RoomComPlayerPlaceObj.eStep.End)
					{
						result = false;
					}
				}
			}
			else
			{
				this.SetUp();
				this.m_Step = RoomComPlayerPlaceObj.eStep.WaitCheck;
			}
			return result;
		}

		// Token: 0x040022E3 RID: 8931
		private RoomComPlayerPlaceObj.eStep m_Step;

		// Token: 0x040022E4 RID: 8932
		private long m_PlayerId;

		// Token: 0x020005A6 RID: 1446
		public enum eStep
		{
			// Token: 0x040022E6 RID: 8934
			GetState,
			// Token: 0x040022E7 RID: 8935
			WaitCheck,
			// Token: 0x040022E8 RID: 8936
			End
		}
	}
}
