﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005DF RID: 1503
	public class RoomBlockData
	{
		// Token: 0x06001D8C RID: 7564 RVA: 0x0009E38A File Offset: 0x0009C78A
		public RoomBlockData()
		{
			this.m_UniqueID = RoomBlockData.m_UniqeIDGenerator++;
		}

		// Token: 0x06001D8D RID: 7565 RVA: 0x0009E3B4 File Offset: 0x0009C7B4
		public RoomBlockData(float posX, float posY, int sizeX, int sizeY)
		{
			this.m_PosX = posX;
			this.m_PosY = posY;
			this.m_SizeX = sizeX;
			this.m_SizeY = sizeY;
			this.m_UniqueID = RoomBlockData.m_UniqeIDGenerator++;
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06001D8E RID: 7566 RVA: 0x0009E405 File Offset: 0x0009C805
		// (set) Token: 0x06001D8F RID: 7567 RVA: 0x0009E418 File Offset: 0x0009C818
		public Vector2 Pos
		{
			get
			{
				return new Vector2(this.m_PosX, this.m_PosY);
			}
			set
			{
				this.m_PosX = value.x;
				this.m_PosY = value.y;
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06001D90 RID: 7568 RVA: 0x0009E434 File Offset: 0x0009C834
		// (set) Token: 0x06001D91 RID: 7569 RVA: 0x0009E44C File Offset: 0x0009C84C
		public Vector3 PosW
		{
			get
			{
				return new Vector3(this.m_PosX, this.m_PosY, 0f);
			}
			set
			{
				this.m_PosX = value.x;
				this.m_PosY = value.y;
			}
		}

		// Token: 0x06001D92 RID: 7570 RVA: 0x0009E468 File Offset: 0x0009C868
		public void SetPos(float valuex, float valuey)
		{
			this.m_PosX = valuex;
			this.m_PosY = valuey;
		}

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06001D93 RID: 7571 RVA: 0x0009E478 File Offset: 0x0009C878
		public int PosX
		{
			get
			{
				return (int)(this.m_PosX + 1E-05f);
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06001D94 RID: 7572 RVA: 0x0009E487 File Offset: 0x0009C887
		public int PosY
		{
			get
			{
				return (int)(this.m_PosY + 1E-05f);
			}
		}

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06001D95 RID: 7573 RVA: 0x0009E496 File Offset: 0x0009C896
		public float PosSpaceX
		{
			get
			{
				return this.m_PosX;
			}
		}

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x06001D96 RID: 7574 RVA: 0x0009E49E File Offset: 0x0009C89E
		public float PosSpaceY
		{
			get
			{
				return this.m_PosY;
			}
		}

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x06001D97 RID: 7575 RVA: 0x0009E4A6 File Offset: 0x0009C8A6
		// (set) Token: 0x06001D98 RID: 7576 RVA: 0x0009E4BB File Offset: 0x0009C8BB
		public Vector2 Size
		{
			get
			{
				return new Vector2((float)this.m_SizeX, (float)this.m_SizeY);
			}
			set
			{
				this.m_SizeX = (int)value.x;
				this.m_SizeY = (int)value.y;
			}
		}

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x06001D99 RID: 7577 RVA: 0x0009E4D9 File Offset: 0x0009C8D9
		// (set) Token: 0x06001D9A RID: 7578 RVA: 0x0009E4F3 File Offset: 0x0009C8F3
		public Vector3 SizeW
		{
			get
			{
				return new Vector3((float)this.m_SizeX, (float)this.m_SizeY, 1f);
			}
			set
			{
				this.m_SizeX = (int)value.x;
				this.m_SizeY = (int)value.y;
			}
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06001D9B RID: 7579 RVA: 0x0009E511 File Offset: 0x0009C911
		// (set) Token: 0x06001D9C RID: 7580 RVA: 0x0009E519 File Offset: 0x0009C919
		public int SizeX
		{
			get
			{
				return this.m_SizeX;
			}
			set
			{
				this.m_SizeX = value;
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06001D9D RID: 7581 RVA: 0x0009E522 File Offset: 0x0009C922
		// (set) Token: 0x06001D9E RID: 7582 RVA: 0x0009E52A File Offset: 0x0009C92A
		public int SizeY
		{
			get
			{
				return this.m_SizeY;
			}
			set
			{
				this.m_SizeY = value;
			}
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06001D9F RID: 7583 RVA: 0x0009E533 File Offset: 0x0009C933
		public uint UniqueID
		{
			get
			{
				return this.m_UniqueID;
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06001DA0 RID: 7584 RVA: 0x0009E53B File Offset: 0x0009C93B
		public RoomBlockData.BlockBounds Bounds
		{
			get
			{
				return new RoomBlockData.BlockBounds(this.m_PosX, this.m_PosX + (float)this.m_SizeX, this.m_PosY, this.m_PosY + (float)this.m_SizeY, 0f, 1f);
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06001DA1 RID: 7585 RVA: 0x0009E574 File Offset: 0x0009C974
		public IRoomObjectControll RoomObject
		{
			get
			{
				return this.m_RoomObjHndl;
			}
		}

		// Token: 0x06001DA2 RID: 7586 RVA: 0x0009E57C File Offset: 0x0009C97C
		public void SetAttachObject(bool fattach)
		{
			this.m_AttachLink = fattach;
		}

		// Token: 0x06001DA3 RID: 7587 RVA: 0x0009E585 File Offset: 0x0009C985
		public bool IsLinkObject()
		{
			return this.m_AttachLink;
		}

		// Token: 0x06001DA4 RID: 7588 RVA: 0x0009E58D File Offset: 0x0009C98D
		public void SetLinkObject(IRoomObjectControll pobj)
		{
			this.m_RoomObjHndl = pobj;
		}

		// Token: 0x040023EF RID: 9199
		private float m_PosX;

		// Token: 0x040023F0 RID: 9200
		private float m_PosY;

		// Token: 0x040023F1 RID: 9201
		private int m_SizeX = 1;

		// Token: 0x040023F2 RID: 9202
		private int m_SizeY = 1;

		// Token: 0x040023F3 RID: 9203
		private uint m_UniqueID;

		// Token: 0x040023F4 RID: 9204
		private static uint m_UniqeIDGenerator;

		// Token: 0x040023F5 RID: 9205
		private bool m_AttachLink;

		// Token: 0x040023F6 RID: 9206
		private IRoomObjectControll m_RoomObjHndl;

		// Token: 0x020005E0 RID: 1504
		public struct BlockBounds
		{
			// Token: 0x06001DA6 RID: 7590 RVA: 0x0009E598 File Offset: 0x0009C998
			public BlockBounds(float _xmin, float _xmax, float _ymin, float _ymax, float _zmin, float _zmax)
			{
				this.xmin = _xmin;
				this.xmax = _xmax;
				this.ymin = _ymin;
				this.ymax = _ymax;
				this.zmin = _zmin;
				this.zmax = _zmax;
			}

			// Token: 0x040023F7 RID: 9207
			public float xmin;

			// Token: 0x040023F8 RID: 9208
			public float xmax;

			// Token: 0x040023F9 RID: 9209
			public float ymin;

			// Token: 0x040023FA RID: 9210
			public float ymax;

			// Token: 0x040023FB RID: 9211
			public float zmin;

			// Token: 0x040023FC RID: 9212
			public float zmax;
		}
	}
}
