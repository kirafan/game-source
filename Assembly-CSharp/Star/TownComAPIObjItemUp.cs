﻿using System;
using TownFacilityResponseTypes;

namespace Star
{
	// Token: 0x02000690 RID: 1680
	public class TownComAPIObjItemUp : INetComHandle
	{
		// Token: 0x060021C1 RID: 8641 RVA: 0x000B3AE6 File Offset: 0x000B1EE6
		public TownComAPIObjItemUp()
		{
			this.ApiName = "player/town_facility/item_up";
			this.Request = true;
			this.ResponseType = typeof(Itemup);
		}

		// Token: 0x04002838 RID: 10296
		public long managedTownFacilityId;

		// Token: 0x04002839 RID: 10297
		public int itemNo;

		// Token: 0x0400283A RID: 10298
		public int amount;

		// Token: 0x0400283B RID: 10299
		public long actionTime;
	}
}
