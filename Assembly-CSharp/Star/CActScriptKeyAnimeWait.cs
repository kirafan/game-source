﻿using System;

namespace Star
{
	// Token: 0x02000544 RID: 1348
	public class CActScriptKeyAnimeWait : IRoomScriptData
	{
		// Token: 0x06001ABF RID: 6847 RVA: 0x0008EC68 File Offset: 0x0008D068
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyAnimeWait actXlsKeyAnimeWait = (ActXlsKeyAnimeWait)pbase;
			this.m_OffsetTime = actXlsKeyAnimeWait.m_OffsetTime / 30f;
		}

		// Token: 0x04002173 RID: 8563
		public float m_OffsetTime;
	}
}
