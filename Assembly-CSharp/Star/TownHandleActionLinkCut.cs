﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006A8 RID: 1704
	public class TownHandleActionLinkCut : ITownHandleAction
	{
		// Token: 0x06002217 RID: 8727 RVA: 0x000B552E File Offset: 0x000B392E
		public TownHandleActionLinkCut(Transform parent, long fmanageid, int flayerid)
		{
			this.m_Type = eTownEventAct.LinkCutPos;
			this.m_ManageID = fmanageid;
			this.m_LayerID = flayerid;
		}

		// Token: 0x0400289E RID: 10398
		public long m_ManageID;

		// Token: 0x0400289F RID: 10399
		public int m_LayerID;
	}
}
