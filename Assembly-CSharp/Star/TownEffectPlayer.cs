﻿using System;
using Meige;
using UnityEngine;

namespace Star
{
	// Token: 0x0200069E RID: 1694
	public class TownEffectPlayer : MonoBehaviour
	{
		// Token: 0x060021F6 RID: 8694 RVA: 0x000B4BD2 File Offset: 0x000B2FD2
		private void Awake()
		{
			this.m_Active = true;
		}

		// Token: 0x060021F7 RID: 8695 RVA: 0x000B4BDB File Offset: 0x000B2FDB
		public bool IsActive()
		{
			return this.m_Active;
		}

		// Token: 0x060021F8 RID: 8696 RVA: 0x000B4BE3 File Offset: 0x000B2FE3
		public void SetResourceNo(int fresno)
		{
			this.m_ResourceNo = fresno;
		}

		// Token: 0x060021F9 RID: 8697 RVA: 0x000B4BEC File Offset: 0x000B2FEC
		public int GetResourceNo()
		{
			return this.m_ResourceNo;
		}

		// Token: 0x060021FA RID: 8698 RVA: 0x000B4BF4 File Offset: 0x000B2FF4
		public void SetUpObject(UnityEngine.Object psetup)
		{
			this.m_EffectNode = (UnityEngine.Object.Instantiate(psetup) as GameObject);
			this.m_EffectNode.transform.SetParent(base.transform, false);
			ParticleSystem[] componentsInChildren = this.m_EffectNode.GetComponentsInChildren<ParticleSystem>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				float num = componentsInChildren[i].main.duration + componentsInChildren[i].main.startLifetime.constantMax;
				if (this.m_MaxTime < num)
				{
					this.m_MaxTime = num;
				}
			}
			MeigeAnimClipHolder componentInChildren = this.m_EffectNode.GetComponentInChildren<MeigeAnimClipHolder>();
			if (componentInChildren != null)
			{
				MsbHandler componentInChildren2 = this.m_EffectNode.GetComponentInChildren<MsbHandler>();
				this.m_MeigeAnimCtrl = this.m_EffectNode.AddComponent<MeigeAnimCtrl>();
				this.m_MeigeAnimCtrl.Open();
				this.m_MeigeAnimCtrl.AddClip(componentInChildren2, componentInChildren);
				this.m_MeigeAnimCtrl.Close();
				this.m_MeigeAnimCtrl.Play("event_001", 0f, WrapMode.Once);
				this.m_MeigeAnimCtrl.UpdateAnimation(0f);
				if (this.m_MaxTime < componentInChildren.m_MeigeAnimClip.m_AnimTimeBySec)
				{
					this.m_MaxTime = componentInChildren.m_MeigeAnimClip.m_AnimTimeBySec;
				}
			}
			this.SetUpRenderLayer();
		}

		// Token: 0x060021FB RID: 8699 RVA: 0x000B4D38 File Offset: 0x000B3138
		public void SetTrs(Vector3 fpos, Quaternion frot, Vector3 fscl, int flayer)
		{
			frot *= new Quaternion(0f, 1f, 0f, 0f);
			base.transform.position = fpos;
			base.transform.rotation = frot;
			base.transform.localScale = fscl;
			this.m_LayerNo = flayer;
			if (this.m_EffectNode != null)
			{
				this.SetUpRenderLayer();
			}
		}

		// Token: 0x060021FC RID: 8700 RVA: 0x000B4DAC File Offset: 0x000B31AC
		public void SetUpRenderLayer()
		{
			Renderer[] componentsInChildren = base.GetComponentsInChildren<Renderer>();
			for (int i = 0; i < componentsInChildren.Length; i++)
			{
				componentsInChildren[i].sortingOrder = this.m_LayerNo;
			}
			MeigeParticleEmitter[] componentsInChildren2 = base.GetComponentsInChildren<MeigeParticleEmitter>();
			for (int i = 0; i < componentsInChildren2.Length; i++)
			{
				componentsInChildren2[i].SetSortingOrder(this.m_LayerNo);
			}
		}

		// Token: 0x060021FD RID: 8701 RVA: 0x000B4E0C File Offset: 0x000B320C
		private void Update()
		{
			if (this.m_EffectNode != null)
			{
				this.m_PlayTime += Time.deltaTime;
				if (this.m_PlayTime >= this.m_MaxTime)
				{
					this.m_Active = false;
					this.m_MeigeAnimCtrl = null;
				}
			}
		}

		// Token: 0x0400286E RID: 10350
		public bool m_Active;

		// Token: 0x0400286F RID: 10351
		public int m_ResourceNo;

		// Token: 0x04002870 RID: 10352
		public int m_LayerNo;

		// Token: 0x04002871 RID: 10353
		public float m_PlayTime;

		// Token: 0x04002872 RID: 10354
		public float m_MaxTime;

		// Token: 0x04002873 RID: 10355
		protected MeigeAnimCtrl m_MeigeAnimCtrl;

		// Token: 0x04002874 RID: 10356
		public GameObject m_EffectNode;
	}
}
