﻿using System;

namespace Star
{
	// Token: 0x020001D6 RID: 470
	[Serializable]
	public struct NamedFriendshipExpDB_Param
	{
		// Token: 0x04000B17 RID: 2839
		public int m_ID;

		// Token: 0x04000B18 RID: 2840
		public int m_Lv;

		// Token: 0x04000B19 RID: 2841
		public int m_NextExp;

		// Token: 0x04000B1A RID: 2842
		public float m_CorrectHp;

		// Token: 0x04000B1B RID: 2843
		public float m_CorrectAtk;

		// Token: 0x04000B1C RID: 2844
		public float m_CorrectMgc;

		// Token: 0x04000B1D RID: 2845
		public float m_CorrectDef;

		// Token: 0x04000B1E RID: 2846
		public float m_CorrectMDef;

		// Token: 0x04000B1F RID: 2847
		public float m_CorrectSpd;

		// Token: 0x04000B20 RID: 2848
		public float m_CorrectLuck;
	}
}
