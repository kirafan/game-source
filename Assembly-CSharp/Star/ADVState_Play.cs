﻿using System;
using System.Collections.Generic;
using Meige;
using PlayerResponseTypes;
using Star.UI;
using WWWTypes;

namespace Star
{
	// Token: 0x020003DD RID: 989
	public class ADVState_Play : ADVState
	{
		// Token: 0x060012CE RID: 4814 RVA: 0x000649BA File Offset: 0x00062DBA
		public ADVState_Play(ADVMain owner) : base(owner)
		{
		}

		// Token: 0x060012CF RID: 4815 RVA: 0x000649D5 File Offset: 0x00062DD5
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x060012D0 RID: 4816 RVA: 0x000649D8 File Offset: 0x00062DD8
		public override void OnStateEnter()
		{
			this.m_Step = ADVState_Play.eStep.Play;
		}

		// Token: 0x060012D1 RID: 4817 RVA: 0x000649E1 File Offset: 0x00062DE1
		public override void OnStateExit()
		{
		}

		// Token: 0x060012D2 RID: 4818 RVA: 0x000649E3 File Offset: 0x00062DE3
		public override void OnDispose()
		{
		}

		// Token: 0x060012D3 RID: 4819 RVA: 0x000649E8 File Offset: 0x00062DE8
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ADVState_Play.eStep.Play:
				if (this.m_Owner.Player.IsEnd)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.FadeOut(-1f, null);
					this.m_Step = ADVState_Play.eStep.FadeOut_Wait;
				}
				break;
			case ADVState_Play.eStep.FadeOut_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.FadeMng.IsComplete())
				{
					SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvOnlyQuest)
					{
						this.Request_QuestSuccessAdvOnly(this.m_Owner.Player.AdvID);
						SingletonMonoBehaviour<RewardPlayer>.Inst.SetReserveOpen(true);
						this.m_Step = ADVState_Play.eStep.Request_Wait;
					}
					else if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserAdvData.IsOccurredAdv(this.m_Owner.Player.AdvID))
					{
						this.m_ADVReward = null;
						this.m_Step = ADVState_Play.eStep.End;
					}
					else
					{
						this.m_ADVReward = null;
						ADVListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ADVListDB.GetParam(this.m_Owner.Player.AdvID);
						if (param.m_PresentGemNum > 0)
						{
							this.m_ADVReward = new RewardPlayer.ADVReward();
							List<eCharaNamedType> list = new List<eCharaNamedType>();
							for (int i = 0; i < param.m_NamedType.Length; i++)
							{
								if (param.m_NamedType[i] != -1)
								{
									list.Add((eCharaNamedType)param.m_NamedType[i]);
								}
							}
							this.m_ADVReward.m_NamedList = list;
							this.m_ADVReward.m_AdvGem = param.m_PresentGemNum;
							SingletonMonoBehaviour<RewardPlayer>.Inst.SetReserveOpen(true);
						}
						this.m_Step = ADVState_Play.eStep.OccuredRequest;
					}
				}
				break;
			case ADVState_Play.eStep.OccuredRequest:
				this.m_AdvAPI.Request_AdvAdd(this.m_Owner.Player.AdvID, new Action(this.OnResponse_AdvAdd), true);
				this.m_Step = ADVState_Play.eStep.OccuredRequest_Wait;
				break;
			case ADVState_Play.eStep.Reward:
				if (this.m_ADVReward != null)
				{
					SingletonMonoBehaviour<RewardPlayer>.Inst.Open(null, this.m_ADVReward, null);
				}
				else
				{
					SingletonMonoBehaviour<RewardPlayer>.Inst.Open(SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult(), null, null);
				}
				this.m_Step = ADVState_Play.eStep.Reward_Wait;
				break;
			case ADVState_Play.eStep.Reward_Wait:
				if (!SingletonMonoBehaviour<RewardPlayer>.Inst.IsOpen())
				{
					this.m_Step = ADVState_Play.eStep.End;
				}
				break;
			case ADVState_Play.eStep.ToBattleOnTutorial_0:
				this.m_Step = ADVState_Play.eStep.ToBattleOnTutorial_1;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.Request_TutorialQuest(new Action(this.OnCompleteTutorialQuest));
				break;
			case ADVState_Play.eStep.ToBattleOnTutorial_2:
				this.m_NextState = 2147483646;
				this.m_Step = ADVState_Play.eStep.None;
				this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Battle;
				return this.m_NextState;
			case ADVState_Play.eStep.End:
			{
				this.m_NextState = 2147483646;
				this.m_Step = ADVState_Play.eStep.None;
				if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvToNextSceneID != SceneDefine.eSceneID.None)
				{
					this.m_Owner.NextTransitSceneID = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.AdvToNextSceneID;
				}
				eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
				if (tutoarialSeq == eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay && this.m_Owner.Player.AdvID == 1000000)
				{
					this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Gacha;
				}
				else if (tutoarialSeq == eTutorialSeq.ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play && this.m_Owner.Player.AdvID == 1000001)
				{
					this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Download;
				}
				else if (tutoarialSeq == eTutorialSeq.ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play && this.m_Owner.Player.AdvID == 1000002)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000003, SceneDefine.eSceneID.Battle, false);
				}
				else if (tutoarialSeq == eTutorialSeq.ADVPrologueIntro2Played_NextIsQuestLogSet && this.m_Owner.Player.AdvID == 1000003)
				{
					this.m_NextState = -1;
					this.m_Step = ADVState_Play.eStep.ToBattleOnTutorial_0;
				}
				else if (tutoarialSeq == eTutorialSeq.ADVPrologueEndPlayed && this.m_Owner.Player.AdvID == 1000004)
				{
					SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetMoviePlayParam("mv_op", SceneDefine.eSceneID.Town);
					this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.MoviePlay;
				}
				return this.m_NextState;
			}
			}
			return -1;
		}

		// Token: 0x060012D4 RID: 4820 RVA: 0x00064E2E File Offset: 0x0006322E
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x060012D5 RID: 4821 RVA: 0x00064E37 File Offset: 0x00063237
		public void Request_QuestSuccessAdvOnly(int advID)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.Request_QuestSuccessAdvOnly(new Action<MeigewwwParam, Questlogset>(this.OnResponse_QuestSuccessAdvOnly));
		}

		// Token: 0x060012D6 RID: 4822 RVA: 0x00064E58 File Offset: 0x00063258
		private void OnResponse_QuestSuccessAdvOnly(MeigewwwParam wwwParam, Questlogset param)
		{
			if (param == null)
			{
				return;
			}
			ResultCode result = param.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
			}
			else
			{
				GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
				globalParam.IsRequestAPIWhenEnterQuestScene = true;
				globalParam.QuestFirstCleared = SingletonMonoBehaviour<GameSystem>.Inst.QuestMng.GetResult().m_IsFirstClear;
				globalParam.QuestStartMode = GameGlobalParameter.eQuestStartMode.List;
				this.m_Step = ADVState_Play.eStep.OccuredRequest;
			}
		}

		// Token: 0x060012D7 RID: 4823 RVA: 0x00064ECE File Offset: 0x000632CE
		private void OnResponse_AdvAdd()
		{
			this.m_Step = ADVState_Play.eStep.Reward;
		}

		// Token: 0x060012D8 RID: 4824 RVA: 0x00064ED7 File Offset: 0x000632D7
		private void OnCompleteTutorialQuest()
		{
			this.m_Step = ADVState_Play.eStep.ToBattleOnTutorial_2;
		}

		// Token: 0x040019A3 RID: 6563
		private ADVState_Play.eStep m_Step = ADVState_Play.eStep.None;

		// Token: 0x040019A4 RID: 6564
		private ADVAPI m_AdvAPI = new ADVAPI();

		// Token: 0x040019A5 RID: 6565
		private RewardPlayer.ADVReward m_ADVReward;

		// Token: 0x020003DE RID: 990
		private enum eStep
		{
			// Token: 0x040019A7 RID: 6567
			None = -1,
			// Token: 0x040019A8 RID: 6568
			Play,
			// Token: 0x040019A9 RID: 6569
			FadeOut_Wait,
			// Token: 0x040019AA RID: 6570
			Request_Wait,
			// Token: 0x040019AB RID: 6571
			OccuredRequest,
			// Token: 0x040019AC RID: 6572
			OccuredRequest_Wait,
			// Token: 0x040019AD RID: 6573
			Reward,
			// Token: 0x040019AE RID: 6574
			Reward_Wait,
			// Token: 0x040019AF RID: 6575
			ToBattleOnTutorial_0,
			// Token: 0x040019B0 RID: 6576
			ToBattleOnTutorial_1,
			// Token: 0x040019B1 RID: 6577
			ToBattleOnTutorial_2,
			// Token: 0x040019B2 RID: 6578
			End
		}
	}
}
