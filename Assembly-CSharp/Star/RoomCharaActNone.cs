﻿using System;

namespace Star
{
	// Token: 0x020005B7 RID: 1463
	public class RoomCharaActNone : IRoomCharaAct
	{
		// Token: 0x06001C5B RID: 7259 RVA: 0x00097E24 File Offset: 0x00096224
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			if (!pbase.GetHandle().IsPreparing())
			{
				result = false;
			}
			return result;
		}
	}
}
