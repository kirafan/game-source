﻿using System;

namespace Star
{
	// Token: 0x0200017A RID: 378
	[Serializable]
	public struct CharacterParamGrowthListDB_Param
	{
		// Token: 0x04000A04 RID: 2564
		public int m_Lv;

		// Token: 0x04000A05 RID: 2565
		public float[] m_GrowthHp;

		// Token: 0x04000A06 RID: 2566
		public float[] m_GrowthAtk;

		// Token: 0x04000A07 RID: 2567
		public float[] m_GrowthMgc;

		// Token: 0x04000A08 RID: 2568
		public float[] m_GrowthDef;

		// Token: 0x04000A09 RID: 2569
		public float[] m_GrowthMDef;

		// Token: 0x04000A0A RID: 2570
		public float[] m_GrowthSpd;

		// Token: 0x04000A0B RID: 2571
		public float[] m_GrowthLuck;
	}
}
