﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000116 RID: 278
	[Serializable]
	public class SkillActionEvent_EffectProjectile_Parabola : SkillActionEvent_Base
	{
		// Token: 0x0400070F RID: 1807
		public string m_CallbackKey;

		// Token: 0x04000710 RID: 1808
		public string m_EffectID;

		// Token: 0x04000711 RID: 1809
		public short m_UniqueID;

		// Token: 0x04000712 RID: 1810
		public float m_ArrivalFrame;

		// Token: 0x04000713 RID: 1811
		public float m_GravityAccel;

		// Token: 0x04000714 RID: 1812
		public eSkillActionEffectProjectileStartPosType m_StartPosType;

		// Token: 0x04000715 RID: 1813
		public eSkillActionLocatorType m_StartPosLocatorType;

		// Token: 0x04000716 RID: 1814
		public Vector2 m_StartPosOffset;

		// Token: 0x04000717 RID: 1815
		public eSkillActionEffectPosType m_TargetPosType;

		// Token: 0x04000718 RID: 1816
		public eSkillActionLocatorType m_TargetPosLocatorType;

		// Token: 0x04000719 RID: 1817
		public Vector2 m_TargetPosOffset;
	}
}
