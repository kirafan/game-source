﻿using System;

namespace Star
{
	// Token: 0x0200032E RID: 814
	public class FieldObjDropItem
	{
		// Token: 0x06000F7B RID: 3963 RVA: 0x00052EE0 File Offset: 0x000512E0
		public void CheckDropItem(int fresno, int fadd)
		{
			if (this.m_List == null)
			{
				this.m_List = new FieldObjDropItem.DropItemState[1];
				this.m_List[0].m_ResultNo = fresno;
				this.m_List[0].m_AddNum = fadd;
			}
			else
			{
				bool flag = false;
				for (int i = 0; i < this.m_List.Length; i++)
				{
					if (this.m_List[i].m_ResultNo == fresno)
					{
						FieldObjDropItem.DropItemState[] list = this.m_List;
						int num = i;
						list[num].m_AddNum = list[num].m_AddNum + fadd;
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					FieldObjDropItem.DropItemState[] array = new FieldObjDropItem.DropItemState[this.m_List.Length + 1];
					for (int i = 0; i < this.m_List.Length; i++)
					{
						array[i].m_ResultNo = this.m_List[i].m_ResultNo;
						array[i].m_AddNum = this.m_List[i].m_AddNum;
					}
					array[this.m_List.Length].m_ResultNo = fresno;
					array[this.m_List.Length].m_AddNum = fadd;
					this.m_List = null;
					this.m_List = array;
				}
			}
		}

		// Token: 0x040016D2 RID: 5842
		public int m_OptionKey;

		// Token: 0x040016D3 RID: 5843
		public FieldObjDropItem.DropItemState[] m_List;

		// Token: 0x040016D4 RID: 5844
		public FieldObjDropItem.DropItemInfo m_Info;

		// Token: 0x0200032F RID: 815
		public struct DropItemInfo
		{
			// Token: 0x06000F7C RID: 3964 RVA: 0x00053020 File Offset: 0x00051420
			public void CalcTimeToUp(long ftime)
			{
				int num = (int)(ftime / this.m_UpTime);
				this.m_CalcNum = num;
				this.m_Max = false;
				if (num >= this.m_LimitNum)
				{
					this.m_CalcNum = this.m_LimitNum;
					this.m_Max = true;
				}
			}

			// Token: 0x040016D5 RID: 5845
			public int m_CalcNum;

			// Token: 0x040016D6 RID: 5846
			public bool m_Max;

			// Token: 0x040016D7 RID: 5847
			public long m_UpTime;

			// Token: 0x040016D8 RID: 5848
			public int m_LimitNum;

			// Token: 0x040016D9 RID: 5849
			public int m_TableID;

			// Token: 0x040016DA RID: 5850
			public int m_FirstDropID;
		}

		// Token: 0x02000330 RID: 816
		public struct DropItemState
		{
			// Token: 0x040016DB RID: 5851
			public int m_ResultNo;

			// Token: 0x040016DC RID: 5852
			public int m_AddNum;
		}
	}
}
