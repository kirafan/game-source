﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000728 RID: 1832
	public class TownCharaHud : MonoBehaviour
	{
		// Token: 0x0600241B RID: 9243 RVA: 0x000C1C2A File Offset: 0x000C002A
		private void Awake()
		{
			this.SetUpBaseComponent();
		}

		// Token: 0x0600241C RID: 9244 RVA: 0x000C1C32 File Offset: 0x000C0032
		public void PlayMotion(int findex)
		{
			this.m_PlayAnime.Play(this.m_AnimeTable[findex].name);
		}

		// Token: 0x0600241D RID: 9245 RVA: 0x000C1C50 File Offset: 0x000C0050
		public void SetMessageID(int fmessageid, int fcharaid)
		{
			DatabaseManager dbMng = SingletonMonoBehaviour<GameSystem>.Inst.DbMng;
			eCharaNamedType namedType = (eCharaNamedType)dbMng.CharaListDB.GetParam(fcharaid).m_NamedType;
			TweetListDB tweetListDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTweetListDB(namedType);
			TweetListDB_Param param = tweetListDB.GetParam(fmessageid);
			Text componentInChildren = base.gameObject.GetComponentInChildren<Text>();
			if (componentInChildren != null)
			{
				componentInChildren.text = param.m_Text;
			}
			this.m_VoicePlayID = -1;
			if (param.m_VoiceCueName != null && param.m_VoiceCueName.Length > 1)
			{
				this.m_VoicePlayID = SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(namedType, param.m_VoiceCueName, true);
			}
			this.PlayMotion(0);
		}

		// Token: 0x0600241E RID: 9246 RVA: 0x000C1D0C File Offset: 0x000C010C
		private void SetUpBaseComponent()
		{
			this.m_PlayAnime = base.gameObject.GetComponentInChildren<Animation>();
			this.m_AnimeTable = new AnimationState[this.m_PlayAnime.GetClipCount()];
			int num = 0;
			IEnumerator enumerator = this.m_PlayAnime.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					AnimationState animationState = (AnimationState)obj;
					this.m_AnimeTable[num] = animationState;
					num++;
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			this.m_Step = 0;
		}

		// Token: 0x0600241F RID: 9247 RVA: 0x000C1DA8 File Offset: 0x000C01A8
		public void CancelPlay()
		{
			if (this.m_VoicePlayID != -1)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.StopRequest(this.m_VoicePlayID);
				this.m_VoicePlayID = -1;
			}
			this.m_TimeUp = 1000f;
		}

		// Token: 0x06002420 RID: 9248 RVA: 0x000C1DE0 File Offset: 0x000C01E0
		public void Update()
		{
			switch (this.m_Step)
			{
			case 0:
				if (!this.m_PlayAnime.isPlaying)
				{
					this.m_Step = 1;
				}
				break;
			case 1:
				if (this.m_VoicePlayID != -1)
				{
					if (!SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.IsPlaying(this.m_VoicePlayID))
					{
						this.m_TimeUp = 0f;
						this.m_Step = 2;
					}
				}
				else
				{
					this.m_TimeUp += Time.deltaTime;
					if (this.m_TimeUp >= 4f)
					{
						this.m_Step = 3;
						this.PlayMotion(1);
					}
				}
				break;
			case 2:
				this.m_TimeUp += Time.deltaTime;
				if (this.m_TimeUp >= 0.5f)
				{
					this.PlayMotion(1);
					this.m_Step = 3;
				}
				break;
			case 3:
				if (!this.m_PlayAnime.isPlaying)
				{
					this.m_Step = 4;
				}
				break;
			}
		}

		// Token: 0x06002421 RID: 9249 RVA: 0x000C1EEE File Offset: 0x000C02EE
		public bool IsPlaying()
		{
			return this.m_Step != 4;
		}

		// Token: 0x06002422 RID: 9250 RVA: 0x000C1EFC File Offset: 0x000C02FC
		public void Destroy()
		{
		}

		// Token: 0x04002B1F RID: 11039
		private int m_VoicePlayID;

		// Token: 0x04002B20 RID: 11040
		private int m_Step;

		// Token: 0x04002B21 RID: 11041
		private float m_TimeUp;

		// Token: 0x04002B22 RID: 11042
		private AnimationState[] m_AnimeTable;

		// Token: 0x04002B23 RID: 11043
		private Animation m_PlayAnime;
	}
}
