﻿using System;

namespace Star
{
	// Token: 0x02000503 RID: 1283
	public class PopupMaskFunc
	{
		// Token: 0x0600192F RID: 6447 RVA: 0x00082DE3 File Offset: 0x000811E3
		public void Clear()
		{
			this.m_ActiveFlag = 0U;
		}

		// Token: 0x06001930 RID: 6448 RVA: 0x00082DEC File Offset: 0x000811EC
		public void PopUpActive(eXlsPopupTiming fcategoryid, bool factive)
		{
			if (factive)
			{
				this.m_ActiveFlag |= 1U << (int)fcategoryid;
			}
			else
			{
				this.m_ActiveFlag &= ~(1U << (int)fcategoryid);
			}
		}

		// Token: 0x06001931 RID: 6449 RVA: 0x00082E20 File Offset: 0x00081220
		public bool IsPopUpActive(ushort fcategoryid)
		{
			return (this.m_ActiveFlag & 1U << (int)fcategoryid) != 0U;
		}

		// Token: 0x04002001 RID: 8193
		private uint m_ActiveFlag;
	}
}
