﻿using System;

namespace Star
{
	// Token: 0x0200031F RID: 799
	public struct TownSearchResult
	{
		// Token: 0x06000F33 RID: 3891 RVA: 0x000520DB File Offset: 0x000504DB
		public void ClearParam()
		{
			this.m_UpState = eTownMoveState.NonPointMove;
			this.m_MoveTargetMngID = TownDefine.ROOM_ACCESS_MNG_ID;
		}

		// Token: 0x040016AC RID: 5804
		public eTownMoveState m_UpState;

		// Token: 0x040016AD RID: 5805
		public long m_MoveTargetMngID;
	}
}
