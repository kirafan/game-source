﻿using System;

namespace Star
{
	// Token: 0x0200070E RID: 1806
	public enum eTownObjectType
	{
		// Token: 0x04002AA6 RID: 10918
		BUILD_ITEM_POP,
		// Token: 0x04002AA7 RID: 10919
		CHARA_MODEL,
		// Token: 0x04002AA8 RID: 10920
		DUMMY,
		// Token: 0x04002AA9 RID: 10921
		CHARA_TOUCH_ITEM,
		// Token: 0x04002AAA RID: 10922
		BUILD_ANIME_GROUP,
		// Token: 0x04002AAB RID: 10923
		AREA_ANIME_GROUP,
		// Token: 0x04002AAC RID: 10924
		TRANING_ICON,
		// Token: 0x04002AAD RID: 10925
		CHRA_HUD,
		// Token: 0x04002AAE RID: 10926
		GET_ITEM,
		// Token: 0x04002AAF RID: 10927
		BUILDING_OBJ_00,
		// Token: 0x04002AB0 RID: 10928
		BUILDING_OBJ_01
	}
}
