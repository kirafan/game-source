﻿using System;

namespace Star
{
	// Token: 0x020002D7 RID: 727
	public abstract class CharacterDataWrapperControllerBase : CharacterDataController
	{
		// Token: 0x06000E3C RID: 3644 RVA: 0x0004DA6D File Offset: 0x0004BE6D
		public CharacterDataWrapperControllerBase(eCharacterDataControllerType in_type) : base(in_type)
		{
		}
	}
}
