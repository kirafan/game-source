﻿using System;

namespace Star
{
	// Token: 0x02000186 RID: 390
	public static class ADVLibraryListDB_Ext
	{
		// Token: 0x06000AD3 RID: 2771 RVA: 0x000408EC File Offset: 0x0003ECEC
		public static ADVLibraryListDB_Param GetParam(this ADVLibraryListDB self, int advLibID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_LibraryListID == advLibID)
				{
					return self.m_Params[i];
				}
			}
			return default(ADVLibraryListDB_Param);
		}
	}
}
