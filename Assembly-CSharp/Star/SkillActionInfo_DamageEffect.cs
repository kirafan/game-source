﻿using System;

namespace Star
{
	// Token: 0x02000110 RID: 272
	[Serializable]
	public class SkillActionInfo_DamageEffect
	{
		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000766 RID: 1894 RVA: 0x0002CE10 File Offset: 0x0002B210
		public string TempEffectID
		{
			get
			{
				switch (this.m_EffectType)
				{
				case eDmgEffectType.PL_Single:
					return string.Format("ef_btl_dmg_single_{0:D2}", this.m_Grade);
				case eDmgEffectType.PL_All:
					return string.Format("ef_btl_dmg_all_{0}_{1:D2}", SkillActionUtility.ConvertEffectID_element(eElementType.Fire), this.m_Grade);
				case eDmgEffectType.EN_Slash:
				case eDmgEffectType.EN_Blow:
				case eDmgEffectType.EN_Bite:
				case eDmgEffectType.EN_Claw:
					return string.Format("ef_btl_dmg_enemy_attack_{0}_{1:D2}", SkillActionUtility.ConvertEffectID_dmgEnemy(this.m_EffectType), this.m_Grade);
				default:
					return string.Empty;
				}
			}
		}

		// Token: 0x040006F8 RID: 1784
		public static readonly byte[] DAMAGE_EFFECT_RANGE = new byte[]
		{
			0,
			3
		};

		// Token: 0x040006F9 RID: 1785
		public byte m_Grade;

		// Token: 0x040006FA RID: 1786
		public eDmgEffectType m_EffectType = eDmgEffectType.None;

		// Token: 0x040006FB RID: 1787
		public eDmgEffectSeType m_SeType = eDmgEffectSeType.None;
	}
}
