﻿using System;
using System.Collections.Generic;
using Meige;
using Star.UI;
using Star.UI.MoviePlay;
using UnityEngine;

namespace Star
{
	// Token: 0x020003EB RID: 1003
	public class DownloadState_Main : DownloadState
	{
		// Token: 0x06001315 RID: 4885 RVA: 0x00065F73 File Offset: 0x00064373
		public DownloadState_Main(DownloadMain owner) : base(owner)
		{
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x06001316 RID: 4886 RVA: 0x00065FA1 File Offset: 0x000643A1
		private bool IsFirstDL
		{
			get
			{
				return this.m_DLType == DownloadState_Main.eDLType.First;
			}
		}

		// Token: 0x06001317 RID: 4887 RVA: 0x00065FAC File Offset: 0x000643AC
		public override int GetStateID()
		{
			return 0;
		}

		// Token: 0x06001318 RID: 4888 RVA: 0x00065FAF File Offset: 0x000643AF
		public override void OnStateEnter()
		{
			this.m_Step = DownloadState_Main.eStep.PreProcess;
		}

		// Token: 0x06001319 RID: 4889 RVA: 0x00065FB8 File Offset: 0x000643B8
		public override void OnDispose()
		{
		}

		// Token: 0x0600131A RID: 4890 RVA: 0x00065FBA File Offset: 0x000643BA
		public override void OnStateExit()
		{
		}

		// Token: 0x0600131B RID: 4891 RVA: 0x00065FBC File Offset: 0x000643BC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case DownloadState_Main.eStep.PreProcess:
				if (!(SingletonMonoBehaviour<GameSystem>.Inst == null) && SingletonMonoBehaviour<GameSystem>.Inst.IsAvailable())
				{
					Screen.sleepTimeout = -1;
					this.m_DLType = DownloadState_Main.eDLType.Always;
					if (APIUtility.IsNeedSignup())
					{
						this.m_DLType = DownloadState_Main.eDLType.First;
					}
					bool flag = false;
					if (!SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.IsFinishedTutorialSeq())
					{
						eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
						if (tutoarialSeq < eTutorialSeq.ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play)
						{
							this.m_DLType = DownloadState_Main.eDLType.First;
						}
						else if (tutoarialSeq < eTutorialSeq.ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play)
						{
							flag = true;
						}
					}
					if (flag)
					{
						CRIFileInstaller crifileInstaller = SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller;
						if (crifileInstaller.IsExist("mv_prologue.usm"))
						{
							this.m_IsMovie = true;
						}
					}
					SingletonMonoBehaviour<GameSystem>.Inst.PrepareDatabase();
					this.m_Step = DownloadState_Main.eStep.PreProcess_Wait_0;
				}
				break;
			case DownloadState_Main.eStep.PreProcess_Wait_0:
				if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompletePrepareDatabase())
				{
					this.SetupDownloadFiles();
					this.m_Owner.DownloadUI.Setup(this.m_IsMovie);
					this.m_Owner.DownloadUI.PlayIn();
					SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Close();
					if (this.m_IsMovie)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
					}
					else if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsPlayingBGM(eSoundBgmListDB.BGM_TITLE))
					{
						SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlayBGM(eSoundBgmListDB.BGM_TITLE, 1f, 0, -1, -1);
					}
					this.m_Step = DownloadState_Main.eStep.PreProcess_Wait_1;
				}
				break;
			case DownloadState_Main.eStep.PreProcess_Wait_1:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					int loadedSprNum = this.m_Owner.DownloadUI.GetLoadedSprNum();
					if (loadedSprNum == -1 || loadedSprNum > 0)
					{
						if (this.IsFirstDL)
						{
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.DownloadCautionTitle, eText_MessageDB.DownloadCaution, new Action<int>(this.OnConfirmDownloadCaution));
							this.m_Step = DownloadState_Main.eStep.PreProcess_Wait_2;
						}
						else
						{
							this.m_Step = DownloadState_Main.eStep.PreProcess_End;
						}
					}
				}
				break;
			case DownloadState_Main.eStep.PreProcess_End:
				if (this.m_IsMovie)
				{
					this.m_Owner.MoviePlayUI.SetSkipMode(MoviePlayUI.eSkipMode.Disable);
					this.m_Owner.MovieCanvas.SetFile("mv_prologue");
					this.m_Owner.MovieCanvas.SetLoop(true);
					this.m_Owner.MovieCanvas.Play();
				}
				this.m_Step = DownloadState_Main.eStep.DownloadAssetBundle;
				break;
			case DownloadState_Main.eStep.DownloadAssetBundle:
			{
				if (this.m_ABDLDB != null)
				{
					List<string> paths = this.m_ABDLDB.GetPaths();
					for (int i = 0; i < paths.Count; i++)
					{
						MeigeResourceManager.LoadHandler(paths[i], new MeigeResource.Option[]
						{
							MeigeResource.Option.DownloadOnly
						});
					}
					this.m_AssetBundleDownloadCountMax = paths.Count;
				}
				float progress = this.CalcProgress(DownloadState_Main.eDownloadPart.AB, 0f);
				this.m_Owner.DownloadUI.UpdateDownloadStatus(progress);
				this.m_Step = DownloadState_Main.eStep.DownloadAssetBundle_Wait;
				break;
			}
			case DownloadState_Main.eStep.DownloadAssetBundle_Wait:
			{
				float progressPerPart = (this.m_AssetBundleDownloadCountMax <= 0) ? 1f : ((float)(this.m_AssetBundleDownloadCountMax - MeigeResourceManager.GetLoadingCount()) / (float)this.m_AssetBundleDownloadCountMax);
				float progress2 = this.CalcProgress(DownloadState_Main.eDownloadPart.AB, progressPerPart);
				this.m_Owner.DownloadUI.UpdateDownloadStatus(progress2);
				if (!this.m_IsErrorWindow)
				{
					if (MeigeResourceManager.isError)
					{
						string textMessage = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErr);
						SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErrTitle), textMessage, null, new Action<int>(this.OnConfirmAssetBundleDownloadError));
						this.m_IsErrorWindow = true;
						if (this.m_Owner.MovieCanvas.IsPlaying())
						{
							this.m_Owner.MovieCanvas.Pause();
						}
					}
					else
					{
						List<string> loadingList = MeigeResourceManager.GetLoadingList();
						if (loadingList.Count <= 0)
						{
							this.m_Step = DownloadState_Main.eStep.DownloadCRI;
						}
					}
				}
				break;
			}
			case DownloadState_Main.eStep.DownloadCRI:
			{
				SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Release();
				CRIFileInstaller crifileInstaller2 = SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller;
				crifileInstaller2.Install(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CRIFileVersionDB, this.IsFirstDL);
				this.m_Step = DownloadState_Main.eStep.DownloadCRI_Wait;
				break;
			}
			case DownloadState_Main.eStep.DownloadCRI_Wait:
			{
				CRIFileInstaller crifileInstaller3 = SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller;
				float progressPerPart2 = (this.m_CriFileDownloadCountMax <= 0) ? 1f : (crifileInstaller3.FinishedFileCount / (float)this.m_CriFileDownloadCountMax);
				float progress3 = this.CalcProgress(DownloadState_Main.eDownloadPart.CRI, progressPerPart2);
				this.m_Owner.DownloadUI.UpdateDownloadStatus(progress3);
				if (!crifileInstaller3.IsInstalling)
				{
					if (!this.m_IsErrorWindow)
					{
						if (crifileInstaller3.IsError)
						{
							string textMessage2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErr);
							SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.DownloadErrTitle), textMessage2, null, new Action<int>(this.OnConfirmCRIDownloadError));
							this.m_IsErrorWindow = true;
							if (this.m_Owner.MovieCanvas.IsPlaying())
							{
								this.m_Owner.MovieCanvas.Pause();
							}
						}
						else
						{
							this.m_Step = DownloadState_Main.eStep.PostProcess;
						}
					}
				}
				break;
			}
			case DownloadState_Main.eStep.PostProcess:
			{
				float progress4 = this.CalcProgress(DownloadState_Main.eDownloadPart.PostProcess, 0f);
				this.m_Owner.DownloadUI.UpdateDownloadStatus(progress4);
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.LoadCueSheet(eSoundCueSheetDB.BGM);
				SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.Load("anim/player/common_battle_body.muast", new MeigeResource.Option[0]);
				SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.LoadPack("btl_always");
				this.m_Step = DownloadState_Main.eStep.PostProcess_Wait;
				break;
			}
			case DownloadState_Main.eStep.PostProcess_Wait:
				if (!SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.IsLoadingCueSheets())
				{
					this.m_Owner.DownloadUI.UpdateDownloadStatus(this.CalcProgress(DownloadState_Main.eDownloadPart.PostProcess, 0.25f));
					if (!SingletonMonoBehaviour<GameSystem>.Inst.CharaResMng.IsLoadingAny())
					{
						this.m_Owner.DownloadUI.UpdateDownloadStatus(this.CalcProgress(DownloadState_Main.eDownloadPart.PostProcess, 0.5f));
						if (!SingletonMonoBehaviour<GameSystem>.Inst.EffectMng.IsLoadingAny())
						{
							this.m_Owner.DownloadUI.UpdateDownloadStatus(this.CalcProgress(DownloadState_Main.eDownloadPart.PostProcess, 1f));
							if (this.m_IsMovie)
							{
								this.m_Owner.MovieCanvas.SetLoop(false);
								this.m_Owner.MovieCanvas.Pause();
								SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.DownloadEndTitle, eText_MessageDB.DownloadEnd, new Action<int>(this.OnCofirmDownloadEnd));
								this.m_Step = DownloadState_Main.eStep.Movie_Wait;
							}
							else
							{
								this.m_Step = DownloadState_Main.eStep.TransitNextScene;
							}
						}
					}
				}
				break;
			case DownloadState_Main.eStep.Movie_Wait:
			{
				bool flag2 = false;
				if (!this.m_Owner.MovieCanvas.IsPaused() && !this.m_Owner.MovieCanvas.IsPlaying())
				{
					flag2 = true;
				}
				if (this.m_RequestedMovieSkip)
				{
					flag2 = true;
				}
				if (flag2)
				{
					this.m_Step = DownloadState_Main.eStep.TransitNextScene;
				}
				break;
			}
			case DownloadState_Main.eStep.TransitNextScene:
				SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.Open(LoadingUI.eDisplayMode.Default, eTipsCategory.None);
				SingletonMonoBehaviour<GameSystem>.Inst.RequesetUnloadUnusedAssets(true);
				this.m_Step = DownloadState_Main.eStep.TransitNextScene_Wait;
				break;
			case DownloadState_Main.eStep.TransitNextScene_Wait:
				if (SingletonMonoBehaviour<GameSystem>.Inst.LoadingUI.IsComplete())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.IsCompleteUnloadUnusedAssets())
					{
						Screen.sleepTimeout = -2;
						this.m_Step = DownloadState_Main.eStep.None;
						if (APIUtility.IsNeedSignup())
						{
							SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Signup);
						}
						else
						{
							switch (SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq())
							{
							case eTutorialSeq.SignupComplete_NextIsPresentGet:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Present);
								break;
							case eTutorialSeq.PresentGot_NextIsADVGachaPrevPlay:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000000, SceneDefine.eSceneID.ADV, false);
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
								break;
							case eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Gacha);
								break;
							case eTutorialSeq.GachaPlayed_NextIsGachaDecide:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.GachaPlay);
								break;
							case eTutorialSeq.GachaDecided_NextIsADVGachaAfterPlay:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000001, SceneDefine.eSceneID.ADV, false);
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
								break;
							case eTutorialSeq.ADVGachaAfterPlayed_NextIsADVPrologueIntro1Play:
								this.m_Step = DownloadState_Main.eStep.ToADVPrologueIntro2OnTutorial_0;
								break;
							case eTutorialSeq.ADVPrologueIntro1Played_NextIsADVPrologueIntro2Play:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000003, SceneDefine.eSceneID.ADV, false);
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
								break;
							case eTutorialSeq.ADVPrologueIntro2Played_NextIsQuestLogSet:
								this.m_Step = DownloadState_Main.eStep.ToBattleOnTutorial_0;
								break;
							case eTutorialSeq.QuestLogSeted_NextIsADVPrologueEnd:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetAdvParam(1000004, SceneDefine.eSceneID.ADV, false);
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.ADV);
								break;
							case eTutorialSeq.ADVPrologueEndPlayed:
								SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SetMoviePlayParam("mv_op", SceneDefine.eSceneID.Town);
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.MoviePlay);
								break;
							default:
								SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
								SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Town);
								break;
							}
						}
					}
				}
				break;
			case DownloadState_Main.eStep.ToADVPrologueIntro2OnTutorial_0:
				this.m_Step = DownloadState_Main.eStep.ToADVPrologueIntro2OnTutorial_1;
				this.m_AdvAPI = new ADVAPI();
				this.m_AdvAPI.Request_AdvAdd(1000002, new Action(this.OnCompleteTutorialAdvAdd), true);
				break;
			case DownloadState_Main.eStep.ToADVPrologueIntro2OnTutorial_2:
				this.m_Step = DownloadState_Main.eStep.TransitNextScene_Wait;
				break;
			case DownloadState_Main.eStep.ToBattleOnTutorial_0:
				this.m_Step = DownloadState_Main.eStep.ToBattleOnTutorial_1;
				SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.Request_TutorialQuest(new Action(this.OnCompleteTutorialQuest));
				break;
			case DownloadState_Main.eStep.ToBattleOnTutorial_2:
				this.m_Step = DownloadState_Main.eStep.None;
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.StopBGM();
				SingletonMonoBehaviour<SceneLoader>.Inst.TransitSceneAsync(SceneDefine.eSceneID.Battle);
				break;
			}
			return -1;
		}

		// Token: 0x0600131C RID: 4892 RVA: 0x000669FC File Offset: 0x00064DFC
		private float CalcProgress(DownloadState_Main.eDownloadPart part, float progressPerPart)
		{
			float num = 0f;
			for (int i = 0; i < (int)part; i++)
			{
				num += this.DOWNLOAD_THRESHOLDS[i];
			}
			return num + progressPerPart * this.DOWNLOAD_THRESHOLDS[(int)part];
		}

		// Token: 0x0600131D RID: 4893 RVA: 0x00066A3C File Offset: 0x00064E3C
		private void SetupDownloadFiles()
		{
			if (this.IsFirstDL)
			{
				this.m_ABDLDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ABDLDB_First;
				Debug.Log("ABDL >>> First");
			}
			else
			{
				this.m_ABDLDB = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.ABDLDB_Always;
				Debug.Log("ABDL >>> Always");
			}
			if (this.m_ABDLDB != null)
			{
				this.m_AssetBundleDownloadCountMax = this.m_ABDLDB.GetPaths().Count;
			}
			else
			{
				this.m_AssetBundleDownloadCountMax = 0;
			}
			Dictionary<string, uint> dictionary;
			List<string> list;
			this.m_CriFileDownloadCountMax = CRIFileInstaller.ParseCRIFileVersionDB(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CRIFileVersionDB, this.IsFirstDL, out dictionary, out list, null);
		}

		// Token: 0x0600131E RID: 4894 RVA: 0x00066AEF File Offset: 0x00064EEF
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
		}

		// Token: 0x0600131F RID: 4895 RVA: 0x00066AF8 File Offset: 0x00064EF8
		private void OnConfirmAssetBundleDownloadError(int answer)
		{
			this.m_IsErrorWindow = false;
			this.RetryDownloadAssetBundle();
			if (this.m_Owner.MovieCanvas.IsPaused())
			{
				this.m_Owner.MovieCanvas.Resume();
			}
		}

		// Token: 0x06001320 RID: 4896 RVA: 0x00066B2C File Offset: 0x00064F2C
		private void RetryDownloadAssetBundle()
		{
			Dictionary<string, MeigeResource.Error> error = MeigeResourceManager.GetError();
			Debug.LogFormat("errors.count:{0}", new object[]
			{
				error.Count
			});
			List<string> list = new List<string>();
			foreach (string item in error.Keys)
			{
				list.Add(item);
			}
			for (int i = 0; i < list.Count; i++)
			{
				MeigeResource.Handler handler = MeigeResourceManager.LoadHandler(list[i], new MeigeResource.Option[0]);
				if (handler != null)
				{
					Debug.LogFormat("Retry:{0}", new object[]
					{
						list[i]
					});
					MeigeResourceManager.RetryHandler(handler);
				}
			}
			Debug.LogFormat("errors.count:{0}(clear after.)", new object[]
			{
				error.Count
			});
		}

		// Token: 0x06001321 RID: 4897 RVA: 0x00066C2C File Offset: 0x0006502C
		private void OnConfirmCRIDownloadError(int answer)
		{
			this.m_IsErrorWindow = false;
			SingletonMonoBehaviour<GameSystem>.Inst.CRIFileInstaller.RetryInstall();
			if (this.m_Owner.MovieCanvas.IsPaused())
			{
				this.m_Owner.MovieCanvas.Resume();
			}
		}

		// Token: 0x06001322 RID: 4898 RVA: 0x00066C69 File Offset: 0x00065069
		private void OnCompleteTutorialAdvAdd()
		{
			this.m_Step = DownloadState_Main.eStep.ToADVPrologueIntro2OnTutorial_2;
		}

		// Token: 0x06001323 RID: 4899 RVA: 0x00066C73 File Offset: 0x00065073
		private void OnCompleteTutorialQuest()
		{
			this.m_Step = DownloadState_Main.eStep.ToBattleOnTutorial_2;
		}

		// Token: 0x06001324 RID: 4900 RVA: 0x00066C7D File Offset: 0x0006507D
		private void OnExecSkipMovie()
		{
			this.m_RequestedMovieSkip = true;
		}

		// Token: 0x06001325 RID: 4901 RVA: 0x00066C86 File Offset: 0x00065086
		private void OnCofirmDownloadEnd(int answer)
		{
			this.m_RequestedMovieSkip = true;
		}

		// Token: 0x06001326 RID: 4902 RVA: 0x00066C8F File Offset: 0x0006508F
		private void OnConfirmDownloadCaution(int answer)
		{
			this.m_Step = DownloadState_Main.eStep.PreProcess_End;
		}

		// Token: 0x040019F2 RID: 6642
		private DownloadState_Main.eStep m_Step = DownloadState_Main.eStep.None;

		// Token: 0x040019F3 RID: 6643
		private AssetBundleDownloadListDB m_ABDLDB;

		// Token: 0x040019F4 RID: 6644
		private int m_AssetBundleDownloadCountMax;

		// Token: 0x040019F5 RID: 6645
		private int m_CriFileDownloadCountMax;

		// Token: 0x040019F6 RID: 6646
		private DownloadState_Main.eDLType m_DLType = DownloadState_Main.eDLType.Always;

		// Token: 0x040019F7 RID: 6647
		private bool m_IsMovie;

		// Token: 0x040019F8 RID: 6648
		private bool m_RequestedMovieSkip;

		// Token: 0x040019F9 RID: 6649
		private bool m_IsErrorWindow;

		// Token: 0x040019FA RID: 6650
		private ADVAPI m_AdvAPI;

		// Token: 0x040019FB RID: 6651
		private float[] DOWNLOAD_THRESHOLDS = new float[]
		{
			0.5f,
			0.49f,
			0.01f
		};

		// Token: 0x020003EC RID: 1004
		private enum eStep
		{
			// Token: 0x040019FD RID: 6653
			None = -1,
			// Token: 0x040019FE RID: 6654
			PreProcess,
			// Token: 0x040019FF RID: 6655
			PreProcess_Wait_0,
			// Token: 0x04001A00 RID: 6656
			PreProcess_Wait_1,
			// Token: 0x04001A01 RID: 6657
			PreProcess_Wait_2,
			// Token: 0x04001A02 RID: 6658
			PreProcess_End,
			// Token: 0x04001A03 RID: 6659
			DownloadAssetBundle,
			// Token: 0x04001A04 RID: 6660
			DownloadAssetBundle_Wait,
			// Token: 0x04001A05 RID: 6661
			DownloadCRI,
			// Token: 0x04001A06 RID: 6662
			DownloadCRI_Wait,
			// Token: 0x04001A07 RID: 6663
			PostProcess,
			// Token: 0x04001A08 RID: 6664
			PostProcess_Wait,
			// Token: 0x04001A09 RID: 6665
			Movie_Wait,
			// Token: 0x04001A0A RID: 6666
			TransitNextScene,
			// Token: 0x04001A0B RID: 6667
			TransitNextScene_Wait,
			// Token: 0x04001A0C RID: 6668
			ToADVPrologueIntro2OnTutorial_0,
			// Token: 0x04001A0D RID: 6669
			ToADVPrologueIntro2OnTutorial_1,
			// Token: 0x04001A0E RID: 6670
			ToADVPrologueIntro2OnTutorial_2,
			// Token: 0x04001A0F RID: 6671
			ToBattleOnTutorial_0,
			// Token: 0x04001A10 RID: 6672
			ToBattleOnTutorial_1,
			// Token: 0x04001A11 RID: 6673
			ToBattleOnTutorial_2
		}

		// Token: 0x020003ED RID: 1005
		private enum eDLType
		{
			// Token: 0x04001A13 RID: 6675
			First,
			// Token: 0x04001A14 RID: 6676
			Always
		}

		// Token: 0x020003EE RID: 1006
		public enum eDownloadPart
		{
			// Token: 0x04001A16 RID: 6678
			AB,
			// Token: 0x04001A17 RID: 6679
			CRI,
			// Token: 0x04001A18 RID: 6680
			PostProcess,
			// Token: 0x04001A19 RID: 6681
			Num
		}
	}
}
