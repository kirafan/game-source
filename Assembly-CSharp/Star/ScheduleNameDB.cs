﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200030B RID: 779
	public class ScheduleNameDB : ScriptableObject
	{
		// Token: 0x04001624 RID: 5668
		public ScheduleNameDB_Param[] m_Params;
	}
}
