﻿using System;

namespace Star
{
	// Token: 0x0200022E RID: 558
	public enum ePersonalityType
	{
		// Token: 0x04000F4A RID: 3914
		Normal,
		// Token: 0x04000F4B RID: 3915
		Boke,
		// Token: 0x04000F4C RID: 3916
		Tsukkomi,
		// Token: 0x04000F4D RID: 3917
		Tennen
	}
}
