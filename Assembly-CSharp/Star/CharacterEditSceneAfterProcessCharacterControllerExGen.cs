﻿using System;

namespace Star
{
	// Token: 0x020002DA RID: 730
	public abstract class CharacterEditSceneAfterProcessCharacterControllerExGen<CharacterDataWrapperType> : CharacterEditSceneAfterProcessCharacterController where CharacterDataWrapperType : CharacterEditSceneCharacterParamWrapper
	{
		// Token: 0x06000E42 RID: 3650 RVA: 0x0004DA9A File Offset: 0x0004BE9A
		public CharacterEditSceneAfterProcessCharacterControllerExGen()
		{
		}

		// Token: 0x06000E43 RID: 3651 RVA: 0x0004DAA2 File Offset: 0x0004BEA2
		public CharacterEditSceneAfterProcessCharacterControllerExGen<CharacterDataWrapperType> Setup(CharacterDataWrapperType cdw)
		{
			this.memberData = cdw;
			return this;
		}

		// Token: 0x06000E44 RID: 3652 RVA: 0x0004DAAC File Offset: 0x0004BEAC
		public override void Refresh()
		{
		}

		// Token: 0x06000E45 RID: 3653 RVA: 0x0004DAAE File Offset: 0x0004BEAE
		protected override CharacterDataWrapperBase GetDataBaseInternal()
		{
			return this.memberData;
		}

		// Token: 0x04001599 RID: 5529
		protected CharacterDataWrapperType memberData;
	}
}
