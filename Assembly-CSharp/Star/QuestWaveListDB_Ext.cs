﻿using System;

namespace Star
{
	// Token: 0x020001A1 RID: 417
	public static class QuestWaveListDB_Ext
	{
		// Token: 0x06000B0C RID: 2828 RVA: 0x00041CC8 File Offset: 0x000400C8
		public static QuestWaveListDB_Param GetParam(this QuestWaveListDB self, int waveID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == waveID)
				{
					return self.m_Params[i];
				}
			}
			return default(QuestWaveListDB_Param);
		}
	}
}
