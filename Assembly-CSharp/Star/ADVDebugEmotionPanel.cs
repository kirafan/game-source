﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x0200026E RID: 622
	public class ADVDebugEmotionPanel : MonoBehaviour
	{
		// Token: 0x06000B8B RID: 2955 RVA: 0x00043645 File Offset: 0x00041A45
		public string GetEmoID()
		{
			return this.m_ADVEmoIDInput.text;
		}

		// Token: 0x06000B8C RID: 2956 RVA: 0x00043652 File Offset: 0x00041A52
		public void ChangeEmoPosition(int add)
		{
			this.m_Position += add;
			if (this.m_Position >= (eADVEmotionPosition)5)
			{
				this.m_Position = eADVEmotionPosition.CharaDefault;
			}
			else if (this.m_Position < eADVEmotionPosition.CharaDefault)
			{
				this.m_Position = eADVEmotionPosition.CharaBottom;
			}
		}

		// Token: 0x06000B8D RID: 2957 RVA: 0x0004368D File Offset: 0x00041A8D
		public eADVEmotionPosition GetEmoPosition()
		{
			return this.m_Position;
		}

		// Token: 0x06000B8E RID: 2958 RVA: 0x00043698 File Offset: 0x00041A98
		public Vector2 GetOffset()
		{
			float x = float.Parse(this.m_OffsetX.text);
			float y = float.Parse(this.m_OffsetY.text);
			return new Vector2(x, y);
		}

		// Token: 0x06000B8F RID: 2959 RVA: 0x000436CE File Offset: 0x00041ACE
		public bool IsUseOffset()
		{
			return this.m_OffsetUseToggle.isOn;
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x000436DB File Offset: 0x00041ADB
		private void LateUpdate()
		{
			this.m_PositionText.text = this.m_Position.ToString();
		}

		// Token: 0x040013F8 RID: 5112
		public InputField m_ADVEmoIDInput;

		// Token: 0x040013F9 RID: 5113
		public Text m_PositionText;

		// Token: 0x040013FA RID: 5114
		public eADVEmotionPosition m_Position;

		// Token: 0x040013FB RID: 5115
		public Toggle m_OffsetUseToggle;

		// Token: 0x040013FC RID: 5116
		public InputField m_OffsetX;

		// Token: 0x040013FD RID: 5117
		public InputField m_OffsetY;
	}
}
