﻿using System;

namespace Star
{
	// Token: 0x02000258 RID: 600
	[Serializable]
	public struct TutorialTipsListDB_Param
	{
		// Token: 0x040013A7 RID: 5031
		public int m_ID;

		// Token: 0x040013A8 RID: 5032
		public byte m_IsAPI;

		// Token: 0x040013A9 RID: 5033
		public byte m_IsCloseAfterViewAllPages;

		// Token: 0x040013AA RID: 5034
		public TutorialTipsListDB_Data[] m_Datas;
	}
}
