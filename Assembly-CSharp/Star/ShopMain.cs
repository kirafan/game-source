﻿using System;
using Star.UI;
using Star.UI.BackGround;

namespace Star
{
	// Token: 0x0200047B RID: 1147
	public class ShopMain : GameStateMain
	{
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x06001662 RID: 5730 RVA: 0x00075226 File Offset: 0x00073626
		// (set) Token: 0x06001663 RID: 5731 RVA: 0x0007522E File Offset: 0x0007362E
		public bool IsTradeListReset { get; set; }

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06001664 RID: 5732 RVA: 0x00075237 File Offset: 0x00073637
		// (set) Token: 0x06001665 RID: 5733 RVA: 0x0007523F File Offset: 0x0007363F
		public bool IsLimitList { get; set; }

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06001666 RID: 5734 RVA: 0x00075248 File Offset: 0x00073648
		// (set) Token: 0x06001667 RID: 5735 RVA: 0x00075250 File Offset: 0x00073650
		public bool IsRequestVocie { get; set; }

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06001668 RID: 5736 RVA: 0x00075259 File Offset: 0x00073659
		// (set) Token: 0x06001669 RID: 5737 RVA: 0x00075261 File Offset: 0x00073661
		public long TargetWeaponMngID { get; set; }

		// Token: 0x0600166A RID: 5738 RVA: 0x0007526A File Offset: 0x0007366A
		private void Start()
		{
			base.SetNextState(0);
		}

		// Token: 0x0600166B RID: 5739 RVA: 0x00075273 File Offset: 0x00073673
		private void OnDestroy()
		{
			this.m_NpcUI = null;
		}

		// Token: 0x0600166C RID: 5740 RVA: 0x0007527C File Offset: 0x0007367C
		protected override void Update()
		{
			base.Update();
		}

		// Token: 0x0600166D RID: 5741 RVA: 0x00075284 File Offset: 0x00073684
		public override GameStateBase ChangeState(int stateID)
		{
			int nextStateID = this.m_NextStateID;
			switch (nextStateID)
			{
			case 0:
				return new ShopState_Init(this);
			case 1:
				return new ShopState_Top(this);
			case 2:
				return new ShopState_TradeTop(this);
			case 3:
				return new ShopState_TradeList(this);
			case 4:
				return new ShopState_TradeSale(this);
			default:
				if (nextStateID != 2147483646)
				{
					return null;
				}
				return new CommonState_Final(this);
			case 10:
				return new ShopState_WeaponTop(this);
			case 11:
				return new ShopState_WeaponCreate(this);
			case 12:
				return new ShopState_WeaponUpgradeList(this);
			case 13:
				return new ShopState_WeaponUpgrade(this);
			case 14:
				return new ShopState_WeaponSale(this);
			case 20:
				return new ShopState_BuildTop(this);
			case 21:
				return new ShopState_BuildBuy(this);
			case 22:
				return new ShopState_BuildSale(this);
			}
		}

		// Token: 0x0600166E RID: 5742 RVA: 0x0007536D File Offset: 0x0007376D
		public override void Destroy()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.BackGroundManager.Close(UIBackGroundManager.eBackGroundOrderID.Low);
		}

		// Token: 0x0600166F RID: 5743 RVA: 0x0007537F File Offset: 0x0007377F
		public override bool IsCompleteDestroy()
		{
			return true;
		}

		// Token: 0x04001D1F RID: 7455
		public const int STATE_INIT = 0;

		// Token: 0x04001D20 RID: 7456
		public const int STATE_SHOP_TOP = 1;

		// Token: 0x04001D21 RID: 7457
		public const int STATE_SHOP_TRADE_TOP = 2;

		// Token: 0x04001D22 RID: 7458
		public const int STATE_SHOP_TRADE_LIST = 3;

		// Token: 0x04001D23 RID: 7459
		public const int STATE_SHOP_TRADE_SALE = 4;

		// Token: 0x04001D24 RID: 7460
		public const int STATE_SHOP_WEAPON_TOP = 10;

		// Token: 0x04001D25 RID: 7461
		public const int STATE_SHOP_WEAPON_CREATE = 11;

		// Token: 0x04001D26 RID: 7462
		public const int STATE_SHOP_WEAPON_UPGRADELIST = 12;

		// Token: 0x04001D27 RID: 7463
		public const int STATE_SHOP_WEAPON_UPGRADE = 13;

		// Token: 0x04001D28 RID: 7464
		public const int STATE_SHOP_WEAPON_SALE = 14;

		// Token: 0x04001D29 RID: 7465
		public const int STATE_SHOP_BUILD_TOP = 20;

		// Token: 0x04001D2A RID: 7466
		public const int STATE_SHOP_BUILD_BUY = 21;

		// Token: 0x04001D2B RID: 7467
		public const int STATE_SHOP_BUILD_SALE = 22;

		// Token: 0x04001D2E RID: 7470
		public NPCCharaDisplayUI m_NpcUI;
	}
}
