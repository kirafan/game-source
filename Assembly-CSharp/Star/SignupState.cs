﻿using System;

namespace Star
{
	// Token: 0x0200049A RID: 1178
	public class SignupState : GameStateBase
	{
		// Token: 0x06001719 RID: 5913 RVA: 0x0007852F File Offset: 0x0007692F
		public SignupState(SignupMain owner)
		{
			this.m_Owner = owner;
		}

		// Token: 0x0600171A RID: 5914 RVA: 0x0007853E File Offset: 0x0007693E
		public override int GetStateID()
		{
			return -1;
		}

		// Token: 0x0600171B RID: 5915 RVA: 0x00078541 File Offset: 0x00076941
		public override void OnStateEnter()
		{
		}

		// Token: 0x0600171C RID: 5916 RVA: 0x00078543 File Offset: 0x00076943
		public override void OnStateExit()
		{
		}

		// Token: 0x0600171D RID: 5917 RVA: 0x00078545 File Offset: 0x00076945
		public override void OnDispose()
		{
		}

		// Token: 0x0600171E RID: 5918 RVA: 0x00078547 File Offset: 0x00076947
		public override int OnStateUpdate()
		{
			return -1;
		}

		// Token: 0x0600171F RID: 5919 RVA: 0x0007854A File Offset: 0x0007694A
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
		}

		// Token: 0x04001DD3 RID: 7635
		protected SignupMain m_Owner;
	}
}
