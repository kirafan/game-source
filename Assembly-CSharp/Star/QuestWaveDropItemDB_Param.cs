﻿using System;

namespace Star
{
	// Token: 0x020001E9 RID: 489
	[Serializable]
	public struct QuestWaveDropItemDB_Param
	{
		// Token: 0x04000BE2 RID: 3042
		public int m_ID;

		// Token: 0x04000BE3 RID: 3043
		public int[] m_DropItemIDs;

		// Token: 0x04000BE4 RID: 3044
		public int[] m_DropItemNums;

		// Token: 0x04000BE5 RID: 3045
		public float[] m_DropProbabilitys;
	}
}
