﻿using System;

namespace Star
{
	// Token: 0x020001CA RID: 458
	[Serializable]
	public struct LeaderSkillListDB_Param
	{
		// Token: 0x04000AF3 RID: 2803
		public int m_ID;

		// Token: 0x04000AF4 RID: 2804
		public string m_SkillName;

		// Token: 0x04000AF5 RID: 2805
		public string m_SkillDetail;

		// Token: 0x04000AF6 RID: 2806
		public LeaderSkillListDB_Data[] m_Datas;
	}
}
