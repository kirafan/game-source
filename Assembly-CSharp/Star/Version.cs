﻿using System;

namespace Star
{
	// Token: 0x02000669 RID: 1641
	public class Version
	{
		// Token: 0x06002138 RID: 8504 RVA: 0x000B0EAE File Offset: 0x000AF2AE
		static Version()
		{
			Version.IsApplying = false;
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x0600213A RID: 8506 RVA: 0x000B0EC4 File Offset: 0x000AF2C4
		// (set) Token: 0x0600213B RID: 8507 RVA: 0x000B0ECB File Offset: 0x000AF2CB
		public static bool IsApplying { get; set; }

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x0600213C RID: 8508 RVA: 0x000B0ED3 File Offset: 0x000AF2D3
		// (set) Token: 0x0600213D RID: 8509 RVA: 0x000B0EDA File Offset: 0x000AF2DA
		public static bool IsOutdated { get; set; } = false;

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x0600213E RID: 8510 RVA: 0x000B0EE2 File Offset: 0x000AF2E2
		// (set) Token: 0x0600213F RID: 8511 RVA: 0x000B0EE9 File Offset: 0x000AF2E9
		public static int ABVersion { get; set; }

		// Token: 0x0400279E RID: 10142
		public const string CurrentVersion = "1.0.3";

		// Token: 0x0400279F RID: 10143
		public const int CurrentVersionCode = 20;
	}
}
