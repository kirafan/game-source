﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000622 RID: 1570
	public class RoomPartsSpMdlFade : IRoomPartsAction
	{
		// Token: 0x06001E98 RID: 7832 RVA: 0x000A5B60 File Offset: 0x000A3F60
		public RoomPartsSpMdlFade(GameObject ptarget, Color fincolor, Color foutcolor, float ftime)
		{
			this.m_Time = 0f;
			this.m_MaxTime = ftime;
			this.m_Base = fincolor;
			this.m_Target = foutcolor;
			this.m_Render = ptarget.GetComponentsInChildren<Renderer>();
			this.UpModelColor(this.m_Base);
			this.m_Active = true;
		}

		// Token: 0x06001E99 RID: 7833 RVA: 0x000A5BB4 File Offset: 0x000A3FB4
		public override bool PartsUpdate()
		{
			Color fcolor = this.m_Target;
			this.m_Time += Time.deltaTime;
			if (this.m_Time >= this.m_MaxTime)
			{
				this.m_Time = this.m_MaxTime;
				this.m_Active = false;
				this.UpModelColor(fcolor);
				this.m_Render = null;
			}
			else
			{
				float b = this.m_Time / this.m_MaxTime;
				fcolor = (this.m_Target - this.m_Base) * b + this.m_Base;
				this.UpModelColor(fcolor);
			}
			return this.m_Active;
		}

		// Token: 0x06001E9A RID: 7834 RVA: 0x000A5C58 File Offset: 0x000A4058
		public void UpModelColor(Color fcolor)
		{
			int num = this.m_Render.Length;
			for (int i = 0; i < num; i++)
			{
				this.m_Render[i].material.SetColor("_Color", fcolor);
			}
		}

		// Token: 0x0400254E RID: 9550
		private Renderer[] m_Render;

		// Token: 0x0400254F RID: 9551
		private Color m_Base;

		// Token: 0x04002550 RID: 9552
		private Color m_Target;

		// Token: 0x04002551 RID: 9553
		private float m_Time;

		// Token: 0x04002552 RID: 9554
		private float m_MaxTime;
	}
}
