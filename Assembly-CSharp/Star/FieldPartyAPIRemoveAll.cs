﻿using System;
using FieldPartyMemberResponseTypes;

namespace Star
{
	// Token: 0x0200034C RID: 844
	public class FieldPartyAPIRemoveAll : INetComHandle
	{
		// Token: 0x0600101D RID: 4125 RVA: 0x00055CAD File Offset: 0x000540AD
		public FieldPartyAPIRemoveAll()
		{
			this.ApiName = "player/field_party/member/remove_all";
			this.Request = true;
			this.ResponseType = typeof(RemoveAll);
		}

		// Token: 0x0400172C RID: 5932
		public long[] managedPartyMemberIds;
	}
}
