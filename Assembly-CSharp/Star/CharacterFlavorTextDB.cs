﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000173 RID: 371
	public class CharacterFlavorTextDB : ScriptableObject
	{
		// Token: 0x040009CE RID: 2510
		public CharacterFlavorTextDB_Param[] m_Params;
	}
}
