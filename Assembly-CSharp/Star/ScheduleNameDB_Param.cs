﻿using System;

namespace Star
{
	// Token: 0x0200030A RID: 778
	[Serializable]
	public struct ScheduleNameDB_Param
	{
		// Token: 0x04001610 RID: 5648
		public int m_ID;

		// Token: 0x04001611 RID: 5649
		public string m_TagName;

		// Token: 0x04001612 RID: 5650
		public ushort[] m_GroupWakeUp;

		// Token: 0x04001613 RID: 5651
		public uint[] m_ResultBaseNo;

		// Token: 0x04001614 RID: 5652
		public short[] m_WakeBaseUp;

		// Token: 0x04001615 RID: 5653
		public uint[] m_ResultSpNo;

		// Token: 0x04001616 RID: 5654
		public short[] m_WakeSpUp;

		// Token: 0x04001617 RID: 5655
		public int[] m_SpMessage;

		// Token: 0x04001618 RID: 5656
		public int[] m_BaseMessage;

		// Token: 0x04001619 RID: 5657
		public int m_MoveScriptID;

		// Token: 0x0400161A RID: 5658
		public int m_DropMakeWakeUp;

		// Token: 0x0400161B RID: 5659
		public uint[] m_DropResultNo;

		// Token: 0x0400161C RID: 5660
		public short[] m_DropWakeUp;

		// Token: 0x0400161D RID: 5661
		public int m_LiinkNo;

		// Token: 0x0400161E RID: 5662
		public uint m_Flags;

		// Token: 0x0400161F RID: 5663
		public string m_DisplayName;

		// Token: 0x04001620 RID: 5664
		public int m_DisplayColor;

		// Token: 0x04001621 RID: 5665
		public int m_BuildMoveType;

		// Token: 0x04001622 RID: 5666
		public int m_BuildMoveCode;

		// Token: 0x04001623 RID: 5667
		public int m_LinkTitle;
	}
}
