﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x020001BB RID: 443
	public static class TweetListDB_Ext
	{
		// Token: 0x06000B36 RID: 2870 RVA: 0x00042B8C File Offset: 0x00040F8C
		public static TweetListDB_Param GetParam(this TweetListDB self, int tweetID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == tweetID)
				{
					return self.m_Params[i];
				}
			}
			return default(TweetListDB_Param);
		}

		// Token: 0x06000B37 RID: 2871 RVA: 0x00042BE4 File Offset: 0x00040FE4
		public static string GetText(this TweetListDB self, int tweetID)
		{
			return self.GetParam(tweetID).m_Text;
		}

		// Token: 0x06000B38 RID: 2872 RVA: 0x00042C00 File Offset: 0x00041000
		public static TweetListDB_Param GetParam(this TweetListDB self, eTweetCategory category)
		{
			List<int> list = new List<int>();
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_Category == (int)category)
				{
					list.Add(i);
				}
			}
			if (list.Count == 0)
			{
				return default(TweetListDB_Param);
			}
			int index = UnityEngine.Random.Range(0, list.Count);
			return self.m_Params[list[index]];
		}

		// Token: 0x06000B39 RID: 2873 RVA: 0x00042C84 File Offset: 0x00041084
		public static string GetText(this TweetListDB self, eTweetCategory category)
		{
			return self.GetParam(category).m_Text;
		}
	}
}
