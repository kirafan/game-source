﻿using System;

namespace Star
{
	// Token: 0x020001BF RID: 447
	public static class WeaponRecipeListDB_Ext
	{
		// Token: 0x06000B41 RID: 2881 RVA: 0x00042FCC File Offset: 0x000413CC
		public static WeaponRecipeListDB_Param GetParamWithRecipeID(this WeaponRecipeListDB self, int recipeID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_ID == recipeID)
				{
					return self.m_Params[i];
				}
			}
			return default(WeaponRecipeListDB_Param);
		}

		// Token: 0x06000B42 RID: 2882 RVA: 0x00043024 File Offset: 0x00041424
		public static WeaponRecipeListDB_Param GetParam(this WeaponRecipeListDB self, int weaponID)
		{
			for (int i = 0; i < self.m_Params.Length; i++)
			{
				if (self.m_Params[i].m_WeaponID == weaponID)
				{
					return self.m_Params[i];
				}
			}
			return default(WeaponRecipeListDB_Param);
		}
	}
}
