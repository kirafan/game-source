﻿using System;

namespace Star
{
	// Token: 0x02000672 RID: 1650
	public class ITownCameraKey
	{
		// Token: 0x06002167 RID: 8551 RVA: 0x000B1B27 File Offset: 0x000AFF27
		public virtual bool PartsUpdate()
		{
			return false;
		}

		// Token: 0x040027BF RID: 10175
		public eTownCameraAction m_Type;

		// Token: 0x040027C0 RID: 10176
		public int m_UID;

		// Token: 0x040027C1 RID: 10177
		public bool m_Active;

		// Token: 0x040027C2 RID: 10178
		public TownCamera m_Camera;
	}
}
