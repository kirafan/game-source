﻿using System;

namespace Star
{
	// Token: 0x020005E9 RID: 1513
	public class RoomEventParameter
	{
		// Token: 0x04002477 RID: 9335
		public eRoomObjectEventType m_Type;

		// Token: 0x04002478 RID: 9336
		public int[] m_Table;
	}
}
