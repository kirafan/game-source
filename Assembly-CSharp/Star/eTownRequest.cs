﻿using System;

namespace Star
{
	// Token: 0x0200071A RID: 1818
	public enum eTownRequest
	{
		// Token: 0x04002AEC RID: 10988
		BuildBuf,
		// Token: 0x04002AED RID: 10989
		BuildArea,
		// Token: 0x04002AEE RID: 10990
		ChangeBuf,
		// Token: 0x04002AEF RID: 10991
		ChangeArea,
		// Token: 0x04002AF0 RID: 10992
		BackCharaBind
	}
}
