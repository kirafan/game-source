﻿using System;

namespace Star
{
	// Token: 0x02000292 RID: 658
	public sealed class TouchEffectDebugMain : SingletonMonoBehaviour<TouchEffectDebugMain>
	{
		// Token: 0x06000C4F RID: 3151 RVA: 0x00048292 File Offset: 0x00046692
		protected override void Awake()
		{
			if (base.CheckInstance())
			{
				InputTouch.Create();
			}
		}

		// Token: 0x06000C50 RID: 3152 RVA: 0x000482A9 File Offset: 0x000466A9
		private void Start()
		{
		}

		// Token: 0x06000C51 RID: 3153 RVA: 0x000482AB File Offset: 0x000466AB
		private void Update()
		{
			InputTouch.Inst.Update();
		}
	}
}
