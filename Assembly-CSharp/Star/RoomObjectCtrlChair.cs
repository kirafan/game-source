﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005CE RID: 1486
	public class RoomObjectCtrlChair : RoomObjectModel
	{
		// Token: 0x06001CE3 RID: 7395 RVA: 0x0009B31C File Offset: 0x0009971C
		public override void SetUpObjectKey(GameObject hbase)
		{
			this.m_ActionWait = 0;
			this.m_EntryLocater = 0;
			this.m_EntryLocaterMax = 0;
			this.m_Locator = null;
			Transform transform = RoomUtility.FindTransform(hbase.transform, "LOC_3", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			transform = RoomUtility.FindTransform(hbase.transform, "LOC_2", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			transform = RoomUtility.FindTransform(hbase.transform, "LOC_1", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			transform = RoomUtility.FindTransform(hbase.transform, "LOC_0", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			transform = RoomUtility.FindTransform(hbase.transform, "ROC_0", 16);
			if (transform != null)
			{
				this.m_EntryLocater++;
			}
			if (this.m_EntryLocater != 0)
			{
				this.m_EntryLocaterMax = this.m_EntryLocater;
				this.m_Locator = new RoomObjectCtrlChair.LocaterInfoBase[this.m_EntryLocater];
				this.m_EntryLocater = 0;
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_3", 16);
				if (transform != null)
				{
					RoomObjectCtrlChair.LocaterInfoSingle locaterInfoSingle = new RoomObjectCtrlChair.LocaterInfoSingle();
					locaterInfoSingle.SetUpLoc(transform);
					this.m_Locator[this.m_EntryLocater] = locaterInfoSingle;
					this.m_EntryLocater++;
				}
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_2", 16);
				if (transform != null)
				{
					RoomObjectCtrlChair.LocaterInfoSingle locaterInfoSingle = new RoomObjectCtrlChair.LocaterInfoSingle();
					locaterInfoSingle.SetUpLoc(transform);
					this.m_Locator[this.m_EntryLocater] = locaterInfoSingle;
					this.m_EntryLocater++;
				}
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_1", 16);
				if (transform != null)
				{
					RoomObjectCtrlChair.LocaterInfoSingle locaterInfoSingle = new RoomObjectCtrlChair.LocaterInfoSingle();
					locaterInfoSingle.SetUpLoc(transform);
					this.m_Locator[this.m_EntryLocater] = locaterInfoSingle;
					this.m_EntryLocater++;
				}
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_0", 16);
				if (transform != null)
				{
					RoomObjectCtrlChair.LocaterInfoSingle locaterInfoSingle = new RoomObjectCtrlChair.LocaterInfoSingle();
					locaterInfoSingle.SetUpLoc(transform);
					this.m_Locator[this.m_EntryLocater] = locaterInfoSingle;
					this.m_EntryLocater++;
				}
				transform = RoomUtility.FindTransform(hbase.transform, "ROC_0", 16);
				if (transform != null)
				{
					RoomObjectCtrlChair.LocaterInfoMulti locaterInfoMulti = new RoomObjectCtrlChair.LocaterInfoMulti();
					locaterInfoMulti.SetUpRoc(transform);
					this.m_Locator[this.m_EntryLocater] = locaterInfoMulti;
					this.m_EntryLocater++;
				}
			}
		}

		// Token: 0x06001CE4 RID: 7396 RVA: 0x0009B5C0 File Offset: 0x000999C0
		public override bool IsInRange(int gridX, int gridY, int fdir)
		{
			if (base.IsInRange(gridX, gridY, fdir) && gridX >= this.m_BlockData.PosX && gridX < this.m_BlockData.PosX + this.m_BlockData.SizeX && gridY >= this.m_BlockData.PosY && gridY < this.m_BlockData.PosY + this.m_BlockData.SizeY)
			{
				int num2;
				int num = this.CalcAttachIndex(out num2, (float)gridX, (float)gridY);
				if (num >= 0 && this.m_Locator[num].m_Link == null)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001CE5 RID: 7397 RVA: 0x0009B668 File Offset: 0x00099A68
		public override Transform GetAttachTransform(Vector2 fpos)
		{
			if (this.m_Locator != null)
			{
				int fsubindex;
				int num = this.CalcAttachIndex(out fsubindex, fpos.x, fpos.y);
				if (num >= 0 && this.m_Locator[num].IsBind())
				{
					return this.m_Locator[num].GetTrs(fsubindex);
				}
			}
			return this.m_OwnerTrs;
		}

		// Token: 0x06001CE6 RID: 7398 RVA: 0x0009B6C8 File Offset: 0x00099AC8
		private int CalcAttachIndex(out int fsubindex, float fgridx, float fgridy)
		{
			float num;
			float num2;
			if (this.m_Dir == CharacterDefine.eDir.L)
			{
				num = fgridx - this.m_BlockData.PosSpaceX;
				num2 = fgridy - this.m_BlockData.PosSpaceY;
			}
			else
			{
				num2 = fgridx - this.m_BlockData.PosSpaceX;
				num = fgridy - this.m_BlockData.PosSpaceY;
			}
			int result = -1;
			fsubindex = 0;
			float num3 = 1000000f;
			for (int i = 0; i < this.m_EntryLocaterMax; i++)
			{
				int num4;
				Vector2 nearPos = this.m_Locator[i].GetNearPos(out num4, num, num2);
				float num5 = nearPos.x - num;
				float num6 = nearPos.y - num2;
				float num7 = num5 * num5 + num6 * num6;
				if (num7 < num3)
				{
					result = i;
					fsubindex = num4;
					num3 = num7;
				}
			}
			return result;
		}

		// Token: 0x06001CE7 RID: 7399 RVA: 0x0009B78E File Offset: 0x00099B8E
		public override bool IsAction()
		{
			return this.m_ActionWait < this.m_EntryLocaterMax;
		}

		// Token: 0x06001CE8 RID: 7400 RVA: 0x0009B7A0 File Offset: 0x00099BA0
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
			if (plink != null)
			{
				plink.BlockData.SetAttachObject(fattach);
			}
			if (fattach)
			{
				plink.BlockData.SetAttachObject(fattach);
				if (this.m_ActionWait < this.m_EntryLocaterMax)
				{
					Vector2 pos = plink.BlockData.Pos;
					int num2;
					int num = this.CalcAttachIndex(out num2, pos.x, pos.y);
					if (num >= 0 && this.m_Locator[num].m_Link == null)
					{
						this.m_Locator[num].m_Link = plink;
						this.m_ActionWait++;
					}
				}
			}
			else if (this.m_ActionWait != 0)
			{
				for (int i = 0; i < this.m_EntryLocater; i++)
				{
					if (this.m_Locator[i].m_Link == plink)
					{
						this.m_Locator[i].m_Link = null;
						this.m_ActionWait--;
						break;
					}
				}
			}
		}

		// Token: 0x06001CE9 RID: 7401 RVA: 0x0009B8A8 File Offset: 0x00099CA8
		public override void LinkObjectParam(int forder, float fposz)
		{
			if (this.m_ActionWait != 0)
			{
				for (int i = 0; i < this.m_EntryLocaterMax; i++)
				{
					if (this.m_Locator[i].m_Link != null)
					{
						this.m_Locator[i].m_Link.SetSortingOrder(fposz - 0.3f, forder + 1 + i);
					}
				}
			}
		}

		// Token: 0x06001CEA RID: 7402 RVA: 0x0009B914 File Offset: 0x00099D14
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			switch (fstep)
			{
			case eTouchStep.Begin:
				this.m_TouchStepUp = true;
				this.m_TouchTime = 0f;
				break;
			case eTouchStep.Drag:
				if (this.m_TouchStepUp)
				{
					this.m_TouchTime += Time.deltaTime;
					if (this.m_TouchTime >= 1f)
					{
						this.m_TouchStepUp = false;
						this.m_Builder.ObjRequestToEditMode(this);
					}
				}
				else
				{
					this.m_TouchStepUp = true;
					this.m_TouchTime = 0f;
				}
				break;
			case eTouchStep.End:
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ROOM_OBJ_SELECT, 1f, 0, -1, -1);
				this.m_TouchTime = 0f;
				this.m_TouchStepUp = false;
				break;
			case eTouchStep.Cancel:
				this.m_TouchTime = 0f;
				this.m_TouchStepUp = false;
				break;
			}
			return !fgrid.IsMoveSlide();
		}

		// Token: 0x06001CEB RID: 7403 RVA: 0x0009BA04 File Offset: 0x00099E04
		public override void AbortLinkEventObj()
		{
			for (int i = 0; i < this.m_EntryLocaterMax; i++)
			{
				if (this.m_Locator[i].m_Link != null)
				{
					RoomObjectCtrlChara roomObjectCtrlChara = this.m_Locator[i].m_Link as RoomObjectCtrlChara;
					if (roomObjectCtrlChara != null)
					{
						roomObjectCtrlChara.AbortObjEventMode();
					}
				}
			}
		}

		// Token: 0x0400238E RID: 9102
		private int m_ActionWait;

		// Token: 0x0400238F RID: 9103
		private int m_EntryLocater;

		// Token: 0x04002390 RID: 9104
		private int m_EntryLocaterMax;

		// Token: 0x04002391 RID: 9105
		private RoomObjectCtrlChair.LocaterInfoBase[] m_Locator;

		// Token: 0x04002392 RID: 9106
		private bool m_TouchStepUp;

		// Token: 0x04002393 RID: 9107
		private float m_TouchTime;

		// Token: 0x020005CF RID: 1487
		public class AttachInfo
		{
			// Token: 0x04002394 RID: 9108
			public int m_SubIndex;
		}

		// Token: 0x020005D0 RID: 1488
		public class LocaterInfoBase
		{
			// Token: 0x06001CEE RID: 7406 RVA: 0x0009BA75 File Offset: 0x00099E75
			public virtual Transform GetTrs(int fsubindex)
			{
				return null;
			}

			// Token: 0x06001CEF RID: 7407 RVA: 0x0009BA78 File Offset: 0x00099E78
			public virtual Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				fsubid = 0;
				return Vector2.zero;
			}

			// Token: 0x06001CF0 RID: 7408 RVA: 0x0009BA82 File Offset: 0x00099E82
			public virtual bool IsBind()
			{
				return false;
			}

			// Token: 0x04002395 RID: 9109
			public IRoomObjectControll m_Link;
		}

		// Token: 0x020005D1 RID: 1489
		public class LocaterInfoSingle : RoomObjectCtrlChair.LocaterInfoBase
		{
			// Token: 0x06001CF2 RID: 7410 RVA: 0x0009BA8D File Offset: 0x00099E8D
			public override Transform GetTrs(int fsubindex)
			{
				return this.m_Trs;
			}

			// Token: 0x06001CF3 RID: 7411 RVA: 0x0009BA95 File Offset: 0x00099E95
			public override Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				fsubid = 0;
				return this.m_GridPos;
			}

			// Token: 0x06001CF4 RID: 7412 RVA: 0x0009BAA0 File Offset: 0x00099EA0
			public override bool IsBind()
			{
				return this.m_Trs != null;
			}

			// Token: 0x06001CF5 RID: 7413 RVA: 0x0009BAAE File Offset: 0x00099EAE
			public void SetUpLoc(Transform ptrs)
			{
				this.m_Trs = ptrs;
				this.m_Link = null;
				this.m_GridPos = RoomUtility.CalcDummyGridPos(ptrs.localPosition);
			}

			// Token: 0x04002396 RID: 9110
			public Transform m_Trs;

			// Token: 0x04002397 RID: 9111
			public Vector2 m_GridPos;
		}

		// Token: 0x020005D2 RID: 1490
		public class LocaterInfoMulti : RoomObjectCtrlChair.LocaterInfoBase
		{
			// Token: 0x06001CF7 RID: 7415 RVA: 0x0009BAD7 File Offset: 0x00099ED7
			public override Transform GetTrs(int fsubindex)
			{
				return this.m_Trs[fsubindex];
			}

			// Token: 0x06001CF8 RID: 7416 RVA: 0x0009BAE4 File Offset: 0x00099EE4
			public override Vector2 GetNearPos(out int fsubid, float fposx, float fposy)
			{
				int num = 0;
				float num2 = this.m_GridPos[0].x - fposx;
				float num3 = this.m_GridPos[0].y - fposy;
				float num4 = num2 * num2 + num3 * num3;
				for (int i = 1; i < this.m_DataNum; i++)
				{
					num2 = this.m_GridPos[i].x - fposx;
					num3 = this.m_GridPos[i].y - fposy;
					float num5 = num2 * num2 + num3 * num3;
					if (num5 < num4)
					{
						num = i;
						num4 = num5;
					}
				}
				fsubid = num;
				return this.m_GridPos[num];
			}

			// Token: 0x06001CF9 RID: 7417 RVA: 0x0009BB95 File Offset: 0x00099F95
			public override bool IsBind()
			{
				return this.m_Trs != null;
			}

			// Token: 0x06001CFA RID: 7418 RVA: 0x0009BBA4 File Offset: 0x00099FA4
			public void SetUpRoc(Transform pbase)
			{
				this.m_DataNum = pbase.childCount;
				this.m_GridPos = new Vector2[this.m_DataNum];
				this.m_Trs = new Transform[this.m_DataNum];
				for (int i = 0; i < this.m_DataNum; i++)
				{
					this.m_Trs[i] = pbase.GetChild(i);
					this.m_GridPos[i] = RoomUtility.CalcDummyGridPos(this.m_Trs[i].localPosition);
				}
			}

			// Token: 0x04002398 RID: 9112
			public Transform[] m_Trs;

			// Token: 0x04002399 RID: 9113
			public Vector2[] m_GridPos;

			// Token: 0x0400239A RID: 9114
			public int m_DataNum;
		}
	}
}
