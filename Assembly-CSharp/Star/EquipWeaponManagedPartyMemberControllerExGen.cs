﻿using System;

namespace Star
{
	// Token: 0x020002D2 RID: 722
	public abstract class EquipWeaponManagedPartyMemberControllerExGen<ThisType> : EquipWeaponManagedPartyMemberController where ThisType : EquipWeaponManagedPartyMemberControllerExGen<ThisType>
	{
		// Token: 0x06000E18 RID: 3608 RVA: 0x0004D6A9 File Offset: 0x0004BAA9
		public EquipWeaponManagedPartyMemberControllerExGen(eCharacterDataControllerType in_type) : base(in_type)
		{
		}

		// Token: 0x06000E19 RID: 3609 RVA: 0x0004D6B2 File Offset: 0x0004BAB2
		public ThisType SetupEx(int in_partyIndex, int in_partySlotIndex)
		{
			base.Setup(in_partyIndex, in_partySlotIndex);
			return (ThisType)((object)this);
		}
	}
}
