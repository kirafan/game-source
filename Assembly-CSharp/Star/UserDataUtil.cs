﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020003A3 RID: 931
	public class UserDataUtil
	{
		// Token: 0x0600117D RID: 4477 RVA: 0x0005C459 File Offset: 0x0005A859
		public static void LinkUpUtil()
		{
			UserDataUtil.TownData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData;
			UserDataUtil.TownObjData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownObjDatas;
			UserDataUtil.FieldChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara;
		}

		// Token: 0x0600117E RID: 4478 RVA: 0x0005C497 File Offset: 0x0005A897
		public static void ClearUtil()
		{
			UserDataUtil.TownData = null;
			UserDataUtil.TownObjData = null;
			UserDataUtil.FieldChara = null;
		}

		// Token: 0x0400183E RID: 6206
		public static UserTownData TownData;

		// Token: 0x0400183F RID: 6207
		public static List<UserTownObjectData> TownObjData;

		// Token: 0x04001840 RID: 6208
		public static UserFieldCharaData FieldChara;
	}
}
