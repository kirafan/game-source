﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005CB RID: 1483
	public class RoomObjectCtrlBase : RoomObjectModel
	{
		// Token: 0x06001CCC RID: 7372 RVA: 0x0009AAA4 File Offset: 0x00098EA4
		public override void SetUpObjectKey(GameObject hbase)
		{
			this.m_ActionWait = true;
			this.m_LinkObj = null;
			this.m_SubLinkObj = null;
			this.m_LocatorTrs = this.m_OwnerTrs;
			Transform transform = RoomUtility.FindTransform(hbase.transform, "LOC_1", 16);
			if (transform != null)
			{
				this.m_LocatorTrs = transform;
			}
			else
			{
				transform = RoomUtility.FindTransform(hbase.transform, "LOC_0", 16);
				if (transform != null)
				{
					this.m_LocatorTrs = transform;
				}
			}
			if ((this.m_OptionFlag & 4) != 0)
			{
				transform = RoomUtility.FindTransform(hbase.transform, "obj_0", 16);
				if (transform != null)
				{
					transform.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x06001CCD RID: 7373 RVA: 0x0009AB5B File Offset: 0x00098F5B
		public override void SetUpAnimation()
		{
			if ((this.m_OptionFlag & 2) != 0)
			{
				this.PlayMotion(0, WrapMode.Loop, 1f);
			}
		}

		// Token: 0x06001CCE RID: 7374 RVA: 0x0009AB77 File Offset: 0x00098F77
		public override bool IsAction()
		{
			return this.m_ActionWait;
		}

		// Token: 0x06001CCF RID: 7375 RVA: 0x0009AB7F File Offset: 0x00098F7F
		public override void SetAttachLink(bool flock)
		{
			this.m_ActionWait = flock;
		}

		// Token: 0x06001CD0 RID: 7376 RVA: 0x0009AB88 File Offset: 0x00098F88
		public override Transform GetAttachTransform(Vector2 flinkpos)
		{
			return this.m_LocatorTrs;
		}

		// Token: 0x06001CD1 RID: 7377 RVA: 0x0009AB90 File Offset: 0x00098F90
		public override void SetLinkObject(IRoomObjectControll plink, bool fattach)
		{
			if (plink != null)
			{
				plink.BlockData.SetAttachObject(fattach);
			}
			if (fattach)
			{
				this.m_ActionWait = false;
				plink.BlockData.SetAttachObject(fattach);
				this.m_LinkObj = plink;
			}
			else
			{
				this.m_ActionWait = true;
				this.m_LinkObj = null;
			}
		}

		// Token: 0x06001CD2 RID: 7378 RVA: 0x0009ABE8 File Offset: 0x00098FE8
		public override void SetActionSendObj(IRoomObjectControll plink)
		{
			this.m_SubLinkObj = plink;
		}

		// Token: 0x06001CD3 RID: 7379 RVA: 0x0009ABF1 File Offset: 0x00098FF1
		public override void LinkObjectParam(int forder, float fposz)
		{
			if (this.m_LinkObj != null)
			{
				this.m_LinkObj.SetSortingOrder(fposz - 0.3f, forder + 1);
			}
		}

		// Token: 0x06001CD4 RID: 7380 RVA: 0x0009AC20 File Offset: 0x00099020
		public override bool TouchAction(eTouchStep fstep, IRoomObjectControll.ObjTouchState fgrid)
		{
			switch (fstep)
			{
			case eTouchStep.Begin:
				this.m_TouchStepUp = true;
				this.m_TouchTime = 0f;
				break;
			case eTouchStep.Drag:
				if (this.m_TouchStepUp)
				{
					this.m_TouchTime += Time.deltaTime;
					if (this.m_TouchTime >= 1f)
					{
						this.m_TouchStepUp = false;
						this.m_Builder.ObjRequestToEditMode(this);
					}
				}
				else
				{
					this.m_TouchStepUp = true;
					this.m_TouchTime = 0f;
				}
				break;
			case eTouchStep.End:
				SingletonMonoBehaviour<GameSystem>.Inst.SoundMng.PlaySE(eSoundSeListDB.ROOM_OBJ_SELECT, 1f, 0, -1, -1);
				this.m_TouchTime = 0f;
				this.m_TouchStepUp = false;
				break;
			case eTouchStep.Cancel:
				this.m_TouchTime = 0f;
				this.m_TouchStepUp = false;
				break;
			}
			return !fgrid.IsMoveSlide();
		}

		// Token: 0x06001CD5 RID: 7381 RVA: 0x0009AD10 File Offset: 0x00099110
		public override void AbortLinkEventObj()
		{
			if (this.m_SubLinkObj != null)
			{
				RoomObjectCtrlChara roomObjectCtrlChara = this.m_SubLinkObj as RoomObjectCtrlChara;
				if (roomObjectCtrlChara != null)
				{
					roomObjectCtrlChara.AbortObjEventMode();
				}
				this.m_SubLinkObj = null;
			}
		}

		// Token: 0x0400237E RID: 9086
		private bool m_ActionWait;

		// Token: 0x0400237F RID: 9087
		private IRoomObjectControll m_LinkObj;

		// Token: 0x04002380 RID: 9088
		private IRoomObjectControll m_SubLinkObj;

		// Token: 0x04002381 RID: 9089
		private Transform m_LocatorTrs;

		// Token: 0x04002382 RID: 9090
		public float m_OffsetZ = 5f;

		// Token: 0x04002383 RID: 9091
		public int m_OffsetOrder = 1;

		// Token: 0x04002384 RID: 9092
		private bool m_TouchStepUp;

		// Token: 0x04002385 RID: 9093
		private float m_TouchTime;
	}
}
