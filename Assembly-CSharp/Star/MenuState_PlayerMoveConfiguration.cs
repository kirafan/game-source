﻿using System;
using Star.UI;
using Star.UI.Menu;
using UnityEngine;

namespace Star
{
	// Token: 0x02000441 RID: 1089
	public class MenuState_PlayerMoveConfiguration : MenuState
	{
		// Token: 0x060014EB RID: 5355 RVA: 0x0006DE36 File Offset: 0x0006C236
		public MenuState_PlayerMoveConfiguration(MenuMain owner) : base(owner)
		{
		}

		// Token: 0x060014EC RID: 5356 RVA: 0x0006DE4E File Offset: 0x0006C24E
		public override int GetStateID()
		{
			return 15;
		}

		// Token: 0x060014ED RID: 5357 RVA: 0x0006DE52 File Offset: 0x0006C252
		public override void OnStateEnter()
		{
			this.m_Step = MenuState_PlayerMoveConfiguration.eStep.First;
		}

		// Token: 0x060014EE RID: 5358 RVA: 0x0006DE5B File Offset: 0x0006C25B
		public override void OnStateExit()
		{
		}

		// Token: 0x060014EF RID: 5359 RVA: 0x0006DE5D File Offset: 0x0006C25D
		public override void OnDispose()
		{
			this.m_UI = null;
			this.m_DataInheritenceSelectWindow = null;
		}

		// Token: 0x060014F0 RID: 5360 RVA: 0x0006DE70 File Offset: 0x0006C270
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case MenuState_PlayerMoveConfiguration.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = MenuState_PlayerMoveConfiguration.eStep.LoadWait;
				break;
			case MenuState_PlayerMoveConfiguration.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = MenuState_PlayerMoveConfiguration.eStep.PlayIn;
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.PlayInWait);
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.PlayInWait:
				if (!this.m_DataInheritenceSelectWindow.IsPlayingAnim())
				{
					this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.MainSelect);
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.AccountLinkWait:
				if (AccountLinker.GetState() != AccountLinker.eResult.Access)
				{
					this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.AccountLinkEndDialog);
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.MainIDAndPass:
				if (this.m_UI.IsMenuEnd())
				{
					bool flag = true;
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_UI.GetActiveWindowType() == MenuPlayerMoveConfigurationUI.eWindowType.InputPassword && this.m_UI.IsDecided())
					{
						flag = false;
					}
					else
					{
						this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.PlayIn);
						flag = false;
					}
					if (flag)
					{
						SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
						this.m_Step = MenuState_PlayerMoveConfiguration.eStep.UnloadChildSceneWait;
					}
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = MenuState_PlayerMoveConfiguration.eStep.None;
					return this.m_NextState;
				}
				break;
			case MenuState_PlayerMoveConfiguration.eStep.Close:
				if (!this.m_DataInheritenceSelectWindow.IsPlayingAnim())
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.UnloadChildSceneWait);
				}
				break;
			}
			return -1;
		}

		// Token: 0x060014F1 RID: 5361 RVA: 0x0006E048 File Offset: 0x0006C448
		private void ChangeStep(MenuState_PlayerMoveConfiguration.eStep step)
		{
			this.m_Step = step;
			switch (this.m_Step)
			{
			case MenuState_PlayerMoveConfiguration.eStep.AccountLinkNotifyDialog:
				SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.YES_NO, eText_MessageDB.PlayerMoveSettingConfirm, eText_MessageDB.PlayerMoveSettingConfirm_Android, new Action<int>(this.OnClickNotifyConfirm));
				break;
			case MenuState_PlayerMoveConfiguration.eStep.AccountLinkWait:
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
				AccountLinker.CheckCreate();
				AccountLinker.SetAcountLinkData();
				break;
			case MenuState_PlayerMoveConfiguration.eStep.AccountLinkEndDialog:
			{
				AccountLinker.eResult state = AccountLinker.GetState();
				if (state != AccountLinker.eResult.Success)
				{
					if (state != AccountLinker.eResult.LogInError)
					{
						if (state == AccountLinker.eResult.ComError)
						{
							this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.Close);
						}
					}
					else
					{
						APIUtility.ShowErrorWindowOk(SingletonMonoBehaviour<GameSystem>.Inst.DbMng.GetTextMessage(eText_MessageDB.PlayerMoveSettingResultError_Android), new Action(this.OnClickResultErrorConfirm));
					}
				}
				else
				{
					SingletonMonoBehaviour<GameSystem>.Inst.CmnMsgWindow.Open(CommonMessageWindow.eType.OK, eText_MessageDB.PlayerMoveSettingResultSuccess, new Action<int>(this.OnClickResultSuccessConfirm));
				}
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Close();
				break;
			}
			case MenuState_PlayerMoveConfiguration.eStep.MainIDAndPass:
				this.m_DataInheritenceSelectWindow.Close();
				this.m_UI.PlayIn();
				break;
			case MenuState_PlayerMoveConfiguration.eStep.Close:
				this.m_DataInheritenceSelectWindow.Close();
				break;
			}
		}

		// Token: 0x060014F2 RID: 5362 RVA: 0x0006E1C0 File Offset: 0x0006C5C0
		public bool SetupAndPlayUI()
		{
			if (this.m_UI != null && this.m_DataInheritenceSelectWindow != null)
			{
				this.m_DataInheritenceSelectWindow.Open();
				return true;
			}
			this.m_UI = UIUtility.GetMenuComponent<MenuPlayerMoveConfigurationUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			DataInheritenceSelectContnt[] array = UnityEngine.Object.FindObjectsOfType(typeof(DataInheritenceSelectContnt)) as DataInheritenceSelectContnt[];
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					if (array[i].name.Contains("SelectWindow"))
					{
						this.m_DataInheritenceSelectWindow = array[i];
						break;
					}
				}
			}
			if (this.m_DataInheritenceSelectWindow == null)
			{
				this.m_UI = null;
				return false;
			}
			this.m_UI.Setup();
			this.m_UI.m_OnClickInputPasswordDecideButton += this.OnClickInputPasswordDecideButton;
			this.m_UI.m_OnClickPlayerMoveDataCloseButton += this.OnClickPlayerMoveDataCloseButton;
			this.m_UI.SetOpenWindowType(MenuPlayerMoveConfigurationUI.eWindowType.InputPassword);
			this.m_DataInheritenceSelectWindow.Setup(true);
			this.m_DataInheritenceSelectWindow.SetCallback(new Action(this.OnClickALButtonPlatform), new Action(this.OnClickALButtonUseIDAndPassword), new Action(this.OnClickCloseButton));
			this.m_DataInheritenceSelectWindow.Open();
			return true;
		}

		// Token: 0x060014F3 RID: 5363 RVA: 0x0006E32C File Offset: 0x0006C72C
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x060014F4 RID: 5364 RVA: 0x0006E33B File Offset: 0x0006C73B
		private void OnClickButton()
		{
		}

		// Token: 0x060014F5 RID: 5365 RVA: 0x0006E33D File Offset: 0x0006C73D
		private void GoToMenuEnd()
		{
			this.m_UI.PlayOut();
		}

		// Token: 0x060014F6 RID: 5366 RVA: 0x0006E34A File Offset: 0x0006C74A
		private void OnClickInputPasswordDecideButton(string moveCodePassward)
		{
			SingletonMonoBehaviour<GameSystem>.Inst.LoginManager.Request_PlayerMoveGet(moveCodePassward, new Action<bool>(this.OnResponse_PlayerMoveGet));
		}

		// Token: 0x060014F7 RID: 5367 RVA: 0x0006E368 File Offset: 0x0006C768
		private void OnResponse_PlayerMoveGet(bool isError)
		{
			if (!isError)
			{
				this.m_UI.SetOpenWindowType(MenuPlayerMoveConfigurationUI.eWindowType.PlayerMoveData);
				this.m_UI.PlayIn();
				LoginManager loginManager = SingletonMonoBehaviour<GameSystem>.Inst.LoginManager;
				this.m_UI.SetInheritanceWindowIdPassword(loginManager.MoveCode, loginManager.MovePassward, loginManager.MoveDeadLine);
			}
		}

		// Token: 0x060014F8 RID: 5368 RVA: 0x0006E3BA File Offset: 0x0006C7BA
		private void OnClickPlayerMoveDataCloseButton()
		{
			this.m_UI.PlayOut();
			this.OnClickCloseButton();
		}

		// Token: 0x060014F9 RID: 5369 RVA: 0x0006E3CD File Offset: 0x0006C7CD
		public void OnClickALButtonPlatform()
		{
			if (this.m_Step != MenuState_PlayerMoveConfiguration.eStep.MainSelect)
			{
				return;
			}
			this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.AccountLinkNotifyDialog);
		}

		// Token: 0x060014FA RID: 5370 RVA: 0x0006E3E3 File Offset: 0x0006C7E3
		public void OnClickALButtonUseIDAndPassword()
		{
			if (this.m_Step != MenuState_PlayerMoveConfiguration.eStep.MainSelect)
			{
				return;
			}
			this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.MainIDAndPass);
		}

		// Token: 0x060014FB RID: 5371 RVA: 0x0006E3F9 File Offset: 0x0006C7F9
		public void OnClickCloseButton()
		{
			if (this.m_Step != MenuState_PlayerMoveConfiguration.eStep.MainSelect && this.m_Step != MenuState_PlayerMoveConfiguration.eStep.MainIDAndPass)
			{
				return;
			}
			this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.Close);
			this.m_NextState = 1;
		}

		// Token: 0x060014FC RID: 5372 RVA: 0x0006E423 File Offset: 0x0006C823
		public void OnClickNotifyConfirm(int idx)
		{
			if (idx == 0)
			{
				this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.AccountLinkWait);
			}
			else
			{
				this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.PlayIn);
			}
		}

		// Token: 0x060014FD RID: 5373 RVA: 0x0006E43E File Offset: 0x0006C83E
		public void OnClickResultSuccessConfirm(int idx)
		{
			this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.PlayIn);
		}

		// Token: 0x060014FE RID: 5374 RVA: 0x0006E447 File Offset: 0x0006C847
		public void OnClickResultErrorConfirm()
		{
			this.ChangeStep(MenuState_PlayerMoveConfiguration.eStep.PlayIn);
		}

		// Token: 0x04001BD6 RID: 7126
		private MenuState_PlayerMoveConfiguration.eStep m_Step = MenuState_PlayerMoveConfiguration.eStep.None;

		// Token: 0x04001BD7 RID: 7127
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.MenuPlayerMoveConfiguration;

		// Token: 0x04001BD8 RID: 7128
		private MenuPlayerMoveConfigurationUI m_UI;

		// Token: 0x04001BD9 RID: 7129
		private DataInheritenceSelectContnt m_DataInheritenceSelectWindow;

		// Token: 0x02000442 RID: 1090
		private enum eStep
		{
			// Token: 0x04001BDB RID: 7131
			None = -1,
			// Token: 0x04001BDC RID: 7132
			First,
			// Token: 0x04001BDD RID: 7133
			LoadWait,
			// Token: 0x04001BDE RID: 7134
			PlayIn,
			// Token: 0x04001BDF RID: 7135
			PlayInWait,
			// Token: 0x04001BE0 RID: 7136
			MainSelect,
			// Token: 0x04001BE1 RID: 7137
			AccountLinkNotifyDialog,
			// Token: 0x04001BE2 RID: 7138
			AccountLinkWait,
			// Token: 0x04001BE3 RID: 7139
			AccountLinkEndDialog,
			// Token: 0x04001BE4 RID: 7140
			MainIDAndPass,
			// Token: 0x04001BE5 RID: 7141
			UnloadChildSceneWait,
			// Token: 0x04001BE6 RID: 7142
			Close
		}
	}
}
