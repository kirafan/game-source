﻿using System;
using TownResponseTypes;

namespace Star
{
	// Token: 0x02000688 RID: 1672
	public class TownComAPINewSet : INetComHandle
	{
		// Token: 0x060021B9 RID: 8633 RVA: 0x000B3996 File Offset: 0x000B1D96
		public TownComAPINewSet()
		{
			this.ApiName = "player/town/set";
			this.Request = true;
			this.ResponseType = typeof(Set);
		}

		// Token: 0x04002823 RID: 10275
		public string gridData;
	}
}
