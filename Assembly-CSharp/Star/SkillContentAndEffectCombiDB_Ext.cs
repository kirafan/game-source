﻿using System;

namespace Star
{
	// Token: 0x020001A6 RID: 422
	public static class SkillContentAndEffectCombiDB_Ext
	{
		// Token: 0x06000B11 RID: 2833 RVA: 0x00041E80 File Offset: 0x00040280
		public static SkillContentAndEffectCombiDB_Param GetParam(this SkillContentAndEffectCombiDB self, eSkillContentAndEffectCombiDB type)
		{
			return self.m_Params[(int)type];
		}
	}
}
