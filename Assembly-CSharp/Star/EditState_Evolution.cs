﻿using System;
using Meige;
using PlayerRequestTypes;
using PlayerResponseTypes;
using Star.UI.Edit;
using Star.UI.Global;
using WWWTypes;

namespace Star
{
	// Token: 0x02000403 RID: 1027
	public class EditState_Evolution : EditState
	{
		// Token: 0x06001387 RID: 4999 RVA: 0x000684E6 File Offset: 0x000668E6
		public EditState_Evolution(EditMain owner) : base(owner)
		{
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x000684FE File Offset: 0x000668FE
		public override int GetStateID()
		{
			return 15;
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x00068502 File Offset: 0x00066902
		public override void OnStateEnter()
		{
			this.m_Step = EditState_Evolution.eStep.First;
		}

		// Token: 0x0600138A RID: 5002 RVA: 0x0006850B File Offset: 0x0006690B
		public override void OnStateExit()
		{
		}

		// Token: 0x0600138B RID: 5003 RVA: 0x0006850D File Offset: 0x0006690D
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600138C RID: 5004 RVA: 0x00068518 File Offset: 0x00066918
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_Evolution.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_Evolution.eStep.LoadWait;
				break;
			case EditState_Evolution.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_Evolution.eStep.PlayIn;
				}
				break;
			case EditState_Evolution.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_Evolution.eStep.Main;
				}
				break;
			case EditState_Evolution.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton != CharaEditSceneUIBase.eButton.Decide)
						{
							this.m_NextState = 14;
						}
						else
						{
							this.m_NextState = 16;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_Evolution.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_Evolution.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_Evolution.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600138D RID: 5005 RVA: 0x00068664 File Offset: 0x00066A64
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<EvolutionUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID);
			this.m_UI.OnClickButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.Evolution);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x0600138E RID: 5006 RVA: 0x00068712 File Offset: 0x00066B12
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x0600138F RID: 5007 RVA: 0x00068724 File Offset: 0x00066B24
		private void OnClickButton()
		{
			CharaEditSceneUIBase.eButton selectButton = this.m_UI.SelectButton;
			if (selectButton == CharaEditSceneUIBase.eButton.Decide)
			{
				this.Request_Evolution(SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID, this.m_UI.SelectButtonIdx, new MeigewwwParam.Callback(this.OnResponse_Evolution));
			}
		}

		// Token: 0x06001390 RID: 5008 RVA: 0x00068779 File Offset: 0x00066B79
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x06001391 RID: 5009 RVA: 0x00068798 File Offset: 0x00066B98
		public void Request_Evolution(long charaMngID, int itemIndex, MeigewwwParam.Callback callback)
		{
			PlayerRequestTypes.Characterevolution characterevolution = new PlayerRequestTypes.Characterevolution();
			UserCharacterData userCharaData = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.GetUserCharaData(charaMngID);
			CharacterEvolutionListDB_Param characterEvolutionListDB_Param;
			if (itemIndex == 0)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaEvolutionListDB.GetNormalMaterialParamBySrcCharaID(userCharaData.Param.CharaID, out characterEvolutionListDB_Param);
			}
			else
			{
				if (itemIndex != 1)
				{
					return;
				}
				SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaEvolutionListDB.GetSpecifyMaterialParamBySrcCharaID(userCharaData.Param.CharaID, out characterEvolutionListDB_Param);
			}
			characterevolution.managedCharacterId = charaMngID;
			characterevolution.recipeId = characterEvolutionListDB_Param.m_RecipeID;
			MeigewwwParam wwwParam = PlayerRequest.Characterevolution(characterevolution, callback);
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
		}

		// Token: 0x06001392 RID: 5010 RVA: 0x0006884C File Offset: 0x00066C4C
		private void OnResponse_Evolution(MeigewwwParam wwwParam)
		{
			PlayerResponseTypes.Characterevolution characterevolution = PlayerResponse.Characterevolution(wwwParam, ResponseCommon.DialogType.None, null);
			if (characterevolution == null)
			{
				return;
			}
			ResultCode result = characterevolution.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				if (result != ResultCode.GOLD_IS_SHORT)
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
				else
				{
					APIUtility.ShowErrorWindowOk(result.ToMessageString(), null);
				}
			}
			else
			{
				UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
				UserCharacterData userCharaData = userDataMng.GetUserCharaData(characterevolution.managedCharacter.managedCharacterId);
				EditMain.EvolutionResultData evolutionResultData = new EditMain.EvolutionResultData();
				evolutionResultData.m_CharaMngID = characterevolution.managedCharacter.managedCharacterId;
				evolutionResultData.m_SrcCharaID = userCharaData.Param.CharaID;
				APIUtility.wwwToUserData(characterevolution.itemSummary, userDataMng.UserItemDatas);
				APIUtility.wwwToUserData(characterevolution.managedCharacter, userCharaData);
				userDataMng.UserData.Gold = characterevolution.gold;
				evolutionResultData.m_DestCharaID = userCharaData.Param.CharaID;
				this.m_Owner.EvolutionResult = evolutionResultData;
				MissionManager.SetPopUpActive(eXlsPopupTiming.PopEdit, false);
				MissionManager.UnlockAction(eXlsMissionSeg.Edit, eXlsMissionEditFuncType.CharaEvelution);
				this.GoToMenuEnd();
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Close();
			}
		}

		// Token: 0x04001A76 RID: 6774
		private EditState_Evolution.eStep m_Step = EditState_Evolution.eStep.None;

		// Token: 0x04001A77 RID: 6775
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.EvolutionUI;

		// Token: 0x04001A78 RID: 6776
		public EvolutionUI m_UI;

		// Token: 0x02000404 RID: 1028
		private enum eStep
		{
			// Token: 0x04001A7A RID: 6778
			None = -1,
			// Token: 0x04001A7B RID: 6779
			First,
			// Token: 0x04001A7C RID: 6780
			LoadWait,
			// Token: 0x04001A7D RID: 6781
			PlayIn,
			// Token: 0x04001A7E RID: 6782
			Main,
			// Token: 0x04001A7F RID: 6783
			UnloadChildSceneWait
		}
	}
}
