﻿using System;

namespace Star
{
	// Token: 0x0200010C RID: 268
	public enum eSkillActionTrailAttachTo
	{
		// Token: 0x040006E3 RID: 1763
		WeaponLeft,
		// Token: 0x040006E4 RID: 1764
		WeaponRight
	}
}
