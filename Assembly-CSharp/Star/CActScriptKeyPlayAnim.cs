﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000540 RID: 1344
	public class CActScriptKeyPlayAnim : IRoomScriptData
	{
		// Token: 0x06001ABB RID: 6843 RVA: 0x0008EBCC File Offset: 0x0008CFCC
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyPlayAnim actXlsKeyPlayAnim = (ActXlsKeyPlayAnim)pbase;
			this.m_PlayAnimNo = actXlsKeyPlayAnim.m_PlayAnimNo;
			this.m_SequenceNo = actXlsKeyPlayAnim.m_SequenceNo;
			this.m_Category = (uint)actXlsKeyPlayAnim.m_AnmType;
			this.m_Loop = ((!actXlsKeyPlayAnim.m_Loop) ? WrapMode.ClampForever : WrapMode.Loop);
		}

		// Token: 0x04002162 RID: 8546
		public int m_PlayAnimNo;

		// Token: 0x04002163 RID: 8547
		public int m_SequenceNo;

		// Token: 0x04002164 RID: 8548
		public uint m_Category;

		// Token: 0x04002165 RID: 8549
		public WrapMode m_Loop;

		// Token: 0x02000541 RID: 1345
		public enum eAnimType
		{
			// Token: 0x04002167 RID: 8551
			Idle,
			// Token: 0x04002168 RID: 8552
			Walk,
			// Token: 0x04002169 RID: 8553
			Enjoy,
			// Token: 0x0400216A RID: 8554
			Rejoice,
			// Token: 0x0400216B RID: 8555
			Lift,
			// Token: 0x0400216C RID: 8556
			Event
		}
	}
}
