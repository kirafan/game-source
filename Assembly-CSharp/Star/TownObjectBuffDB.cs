﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000244 RID: 580
	public class TownObjectBuffDB : ScriptableObject
	{
		// Token: 0x0400131E RID: 4894
		public TownObjectBuffDB_Param[] m_Params;
	}
}
