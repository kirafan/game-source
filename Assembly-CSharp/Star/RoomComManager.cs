﻿using System;

namespace Star
{
	// Token: 0x0200046C RID: 1132
	public class RoomComManager
	{
		// Token: 0x060015F3 RID: 5619 RVA: 0x000725CA File Offset: 0x000709CA
		public RoomComManager(int ftablenum)
		{
			this.m_Table = new IMainComCommand[ftablenum];
			this.m_Num = 0;
			this.m_Max = ftablenum;
		}

		// Token: 0x060015F4 RID: 5620 RVA: 0x000725EC File Offset: 0x000709EC
		public void PushComCommand(IMainComCommand pcmd)
		{
			if (this.m_Num < this.m_Max)
			{
				this.m_Table[this.m_Num] = pcmd;
				this.m_Num++;
			}
		}

		// Token: 0x060015F5 RID: 5621 RVA: 0x0007261B File Offset: 0x00070A1B
		public int GetComCommandNum()
		{
			return this.m_Num;
		}

		// Token: 0x060015F6 RID: 5622 RVA: 0x00072623 File Offset: 0x00070A23
		public IMainComCommand GetComCommand(int findex)
		{
			return this.m_Table[findex];
		}

		// Token: 0x060015F7 RID: 5623 RVA: 0x00072630 File Offset: 0x00070A30
		public void Flush()
		{
			for (int i = 0; i < this.m_Num; i++)
			{
				this.m_Table[i] = null;
			}
			this.m_Num = 0;
		}

		// Token: 0x04001CAE RID: 7342
		public int m_Num;

		// Token: 0x04001CAF RID: 7343
		public int m_Max;

		// Token: 0x04001CB0 RID: 7344
		public IMainComCommand[] m_Table;
	}
}
