﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000167 RID: 359
	public class BattleDefineDB : ScriptableObject
	{
		// Token: 0x040009B0 RID: 2480
		public BattleDefineDB_Param[] m_Params;
	}
}
