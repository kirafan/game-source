﻿using System;

namespace Star
{
	// Token: 0x02000257 RID: 599
	[Serializable]
	public struct TutorialTipsListDB_Data
	{
		// Token: 0x040013A4 RID: 5028
		public int m_ImageID;

		// Token: 0x040013A5 RID: 5029
		public string m_Title;

		// Token: 0x040013A6 RID: 5030
		public string m_Text;
	}
}
