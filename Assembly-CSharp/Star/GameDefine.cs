﻿using System;

namespace Star
{
	// Token: 0x02000644 RID: 1604
	public static class GameDefine
	{
		// Token: 0x0400263F RID: 9791
		public const int INVALID_MNG_ID = -1;

		// Token: 0x04002640 RID: 9792
		public const int INVALID_ID = -1;

		// Token: 0x04002641 RID: 9793
		public const uint U_INVALID_ID = 0U;

		// Token: 0x04002642 RID: 9794
		public const int TIMEOUT_SEC = 30;

		// Token: 0x04002643 RID: 9795
		public const int APP_TARGET_FRAMERATE = 30;

		// Token: 0x04002644 RID: 9796
		public const float APP_SEC_PER_ONEFRAME = 0.033333335f;

		// Token: 0x04002645 RID: 9797
		public const string FULLOPEN_NAME = "FULLOPEN";
	}
}
