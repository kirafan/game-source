﻿using System;
using Star.UI.Room;

namespace Star
{
	// Token: 0x0200047D RID: 1149
	public class ShopState_BuildBuy : ShopState
	{
		// Token: 0x06001677 RID: 5751 RVA: 0x000753A6 File Offset: 0x000737A6
		public ShopState_BuildBuy(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x06001678 RID: 5752 RVA: 0x000753C9 File Offset: 0x000737C9
		public override int GetStateID()
		{
			return 21;
		}

		// Token: 0x06001679 RID: 5753 RVA: 0x000753CD File Offset: 0x000737CD
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_BuildBuy.eStep.First;
		}

		// Token: 0x0600167A RID: 5754 RVA: 0x000753D6 File Offset: 0x000737D6
		public override void OnStateExit()
		{
		}

		// Token: 0x0600167B RID: 5755 RVA: 0x000753D8 File Offset: 0x000737D8
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600167C RID: 5756 RVA: 0x000753E4 File Offset: 0x000737E4
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_BuildBuy.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_BuildBuy.eStep.LoadWait;
				break;
			case ShopState_BuildBuy.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildBuy.eStep.PlayIn;
				}
				break;
			case ShopState_BuildBuy.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_BuildBuy.eStep.Main;
				}
				break;
			case ShopState_BuildBuy.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						RoomShopListUI.eTransit transit = this.m_UI.Transit;
						if (transit != RoomShopListUI.eTransit.Room)
						{
							this.m_NextState = 20;
						}
						else
						{
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Room;
							this.m_NextState = 2147483646;
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_BuildBuy.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_BuildBuy.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_BuildBuy.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600167D RID: 5757 RVA: 0x00075544 File Offset: 0x00073944
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<RoomShopListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_003, "voice_210", true);
			this.m_UI.Setup(this.m_RoomObjShopList);
			this.m_UI.SetMode(RoomObjListWindow.eMode.Shop);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x0600167E RID: 5758 RVA: 0x000755E4 File Offset: 0x000739E4
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x0600167F RID: 5759 RVA: 0x000755F3 File Offset: 0x000739F3
		private void OnClickButtonCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x06001680 RID: 5760 RVA: 0x000755FB File Offset: 0x000739FB
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001D33 RID: 7475
		private ShopState_BuildBuy.eStep m_Step = ShopState_BuildBuy.eStep.None;

		// Token: 0x04001D34 RID: 7476
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.RoomShopListUI;

		// Token: 0x04001D35 RID: 7477
		private RoomShopListUI m_UI;

		// Token: 0x04001D36 RID: 7478
		private RoomObjShopList m_RoomObjShopList = new RoomObjShopList();

		// Token: 0x0200047E RID: 1150
		private enum eStep
		{
			// Token: 0x04001D38 RID: 7480
			None = -1,
			// Token: 0x04001D39 RID: 7481
			First,
			// Token: 0x04001D3A RID: 7482
			LoadStart,
			// Token: 0x04001D3B RID: 7483
			LoadWait,
			// Token: 0x04001D3C RID: 7484
			PlayIn,
			// Token: 0x04001D3D RID: 7485
			Main,
			// Token: 0x04001D3E RID: 7486
			UnloadChildSceneWait
		}
	}
}
