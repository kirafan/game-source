﻿using System;
using Star.UI.Global;
using Star.UI.WeaponList;

namespace Star
{
	// Token: 0x02000497 RID: 1175
	public class ShopState_WeaponUpgradeList : ShopState
	{
		// Token: 0x06001709 RID: 5897 RVA: 0x0007825F File Offset: 0x0007665F
		public ShopState_WeaponUpgradeList(ShopMain owner) : base(owner)
		{
		}

		// Token: 0x0600170A RID: 5898 RVA: 0x00078277 File Offset: 0x00076677
		public override int GetStateID()
		{
			return 12;
		}

		// Token: 0x0600170B RID: 5899 RVA: 0x0007827B File Offset: 0x0007667B
		public override void OnStateEnter()
		{
			this.m_Step = ShopState_WeaponUpgradeList.eStep.First;
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x00078284 File Offset: 0x00076684
		public override void OnStateExit()
		{
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x00078286 File Offset: 0x00076686
		public override void OnDispose()
		{
			this.m_UI = null;
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x00078290 File Offset: 0x00076690
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case ShopState_WeaponUpgradeList.eStep.First:
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = ShopState_WeaponUpgradeList.eStep.LoadWait;
				break;
			case ShopState_WeaponUpgradeList.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponUpgradeList.eStep.PlayIn;
				}
				break;
			case ShopState_WeaponUpgradeList.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = ShopState_WeaponUpgradeList.eStep.Main;
				}
				break;
			case ShopState_WeaponUpgradeList.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else if (this.m_UI.SelectButton == WeaponListUI.eButton.Weapon)
					{
						this.m_Owner.TargetWeaponMngID = this.m_UI.SelectedWeaponMngID;
						this.m_NextState = 13;
					}
					else
					{
						this.m_NextState = 10;
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = ShopState_WeaponUpgradeList.eStep.UnloadChildSceneWait;
				}
				break;
			case ShopState_WeaponUpgradeList.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = ShopState_WeaponUpgradeList.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x000783E8 File Offset: 0x000767E8
		private bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<WeaponListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.SetupUpgrade();
			this.m_UI.OnClickButton += this.OnClickButtonCallBack;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.WeaponUpgrade);
			SingletonMonoBehaviour<GameSystem>.Inst.VoiceCtrl.Request(eSoundCueSheetDB.Voice_Original_005, "voice_210", true);
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x0007849F File Offset: 0x0007689F
		protected void OnClickButtonCallBack()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x000784A7 File Offset: 0x000768A7
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.GoToMenuEnd();
		}

		// Token: 0x06001712 RID: 5906 RVA: 0x000784B6 File Offset: 0x000768B6
		private void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x04001DC6 RID: 7622
		private ShopState_WeaponUpgradeList.eStep m_Step = ShopState_WeaponUpgradeList.eStep.None;

		// Token: 0x04001DC7 RID: 7623
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.WeaponListUI;

		// Token: 0x04001DC8 RID: 7624
		private WeaponListUI m_UI;

		// Token: 0x02000498 RID: 1176
		private enum eStep
		{
			// Token: 0x04001DCA RID: 7626
			None = -1,
			// Token: 0x04001DCB RID: 7627
			First,
			// Token: 0x04001DCC RID: 7628
			LoadStart,
			// Token: 0x04001DCD RID: 7629
			LoadWait,
			// Token: 0x04001DCE RID: 7630
			PlayIn,
			// Token: 0x04001DCF RID: 7631
			Main,
			// Token: 0x04001DD0 RID: 7632
			UnloadChildSceneWait
		}
	}
}
