﻿using System;

namespace Star
{
	// Token: 0x0200010D RID: 269
	public enum eSkillActionWeaponThrowPosType
	{
		// Token: 0x040006E6 RID: 1766
		Self,
		// Token: 0x040006E7 RID: 1767
		SkillTarget_Tgt,
		// Token: 0x040006E8 RID: 1768
		PartyCenter_Tgt
	}
}
