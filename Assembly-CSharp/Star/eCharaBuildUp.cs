﻿using System;

namespace Star
{
	// Token: 0x02000324 RID: 804
	public enum eCharaBuildUp
	{
		// Token: 0x040016B4 RID: 5812
		Add,
		// Token: 0x040016B5 RID: 5813
		Del,
		// Token: 0x040016B6 RID: 5814
		ChangeRoom
	}
}
