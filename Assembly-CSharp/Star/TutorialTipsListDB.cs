﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000259 RID: 601
	public class TutorialTipsListDB : ScriptableObject
	{
		// Token: 0x040013AB RID: 5035
		public TutorialTipsListDB_Param[] m_Params;
	}
}
