﻿using System;

namespace Star
{
	// Token: 0x02000376 RID: 886
	public enum eScheduleTypeUIType
	{
		// Token: 0x040017CB RID: 6091
		Non,
		// Token: 0x040017CC RID: 6092
		Sleep,
		// Token: 0x040017CD RID: 6093
		Room,
		// Token: 0x040017CE RID: 6094
		Office,
		// Token: 0x040017CF RID: 6095
		Town,
		// Token: 0x040017D0 RID: 6096
		System
	}
}
