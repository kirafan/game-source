﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x0200008B RID: 139
	public class BattleAIData : ScriptableObject
	{
		// Token: 0x04000249 RID: 585
		public int m_ID;

		// Token: 0x0400024A RID: 586
		public List<BattleAIPatternChangeData> m_PatternChangeDatas;

		// Token: 0x0400024B RID: 587
		public List<BattleAIPatternData> m_PatternDatas;

		// Token: 0x0400024C RID: 588
		public List<BattleAICommandData> m_ChargeCommandDatas = new List<BattleAICommandData>();

		// Token: 0x0400024D RID: 589
		public eSingleTargetPriorityWhenHateSame m_SingleTargetPriorityWhenHateSame;
	}
}
