﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020004FD RID: 1277
	public class MissionFuncSystem : IMissionFunction
	{
		// Token: 0x06001929 RID: 6441 RVA: 0x00082B2C File Offset: 0x00080F2C
		public bool CheckUnlock(IMissionPakage pparam, List<IMissionPakage> plist)
		{
			bool result = false;
			switch (pparam.m_ExtensionScriptID)
			{
			case 0:
				pparam.m_Rate = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.LoginDays;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 1:
				pparam.m_Rate = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ContinuousDays;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			case 2:
			{
				eMissionCategory type = pparam.m_Type;
				int num = 0;
				int num2 = 0;
				int count = plist.Count;
				for (int i = 0; i < count; i++)
				{
					if (plist[i].m_Type == type)
					{
						num2++;
						if (plist[i].m_State >= eMissionState.PreComp)
						{
							num++;
						}
					}
				}
				pparam.m_Rate = num;
				result = (num >= num2 - 1);
				break;
			}
			case 3:
				pparam.m_Rate = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.Lv;
				result = (pparam.m_Rate >= pparam.m_RateBase);
				break;
			}
			return result;
		}
	}
}
