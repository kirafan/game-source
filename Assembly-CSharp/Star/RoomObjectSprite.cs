﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005D9 RID: 1497
	public class RoomObjectSprite : IRoomObjectControll
	{
		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06001D6E RID: 7534 RVA: 0x0009DDED File Offset: 0x0009C1ED
		public int SubKeyID
		{
			get
			{
				return this.m_SubKeyID;
			}
		}

		// Token: 0x06001D6F RID: 7535 RVA: 0x0009DDF8 File Offset: 0x0009C1F8
		public void SetupSimple(eRoomObjectCategory category, int objID, int fsubkey, CharacterDefine.eDir dir, int posX, int posY, int defaultSizeX, int defaultSizeY)
		{
			this.m_Category = category;
			this.m_ObjID = objID;
			this.m_SubKeyID = fsubkey;
			this.m_DefaultSizeX = defaultSizeX;
			this.m_DefaultSizeY = defaultSizeY;
			this.m_BlockData = new RoomBlockData((float)posX, (float)posY, defaultSizeX, defaultSizeY);
			this.m_BlockData.SetLinkObject(this);
			base.SetDir(dir);
			this.SetSortingOrder = new IRoomObjectControll.SortingOrderFunc(this.SortingOrderSprite);
			this.m_RoomObjResource = new RoomObjectResSprite();
			this.m_RoomObjResource.Setup(this);
		}

		// Token: 0x06001D70 RID: 7536 RVA: 0x0009DE7C File Offset: 0x0009C27C
		public override void Destroy()
		{
			if (this.m_SpriteRender != null)
			{
				this.m_SpriteRender.sprite = null;
				this.m_SpriteRender = null;
			}
		}

		// Token: 0x06001D71 RID: 7537 RVA: 0x0009DEA2 File Offset: 0x0009C2A2
		public void AttachSprite(Sprite sprite)
		{
			if (sprite != null)
			{
				this.m_SpriteRender = base.gameObject.AddComponent<SpriteRenderer>();
				this.m_SpriteRender.sprite = sprite;
				this.m_DefaultSpriteSortID = (short)this.m_SpriteRender.sortingOrder;
			}
		}

		// Token: 0x06001D72 RID: 7538 RVA: 0x0009DEDF File Offset: 0x0009C2DF
		public void SortingOrderSprite(float flayerpos, int sortingOrder)
		{
			if (this.m_SpriteRender != null)
			{
				this.m_SpriteRender.sortingOrder = sortingOrder + (int)this.m_DefaultSpriteSortID;
			}
		}

		// Token: 0x040023DF RID: 9183
		protected int m_SubKeyID;

		// Token: 0x040023E0 RID: 9184
		private SpriteRenderer m_SpriteRender;

		// Token: 0x040023E1 RID: 9185
		protected int m_SelectingMode;

		// Token: 0x040023E2 RID: 9186
		private short m_DefaultSpriteSortID;
	}
}
