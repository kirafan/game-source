﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020006C5 RID: 1733
	public class TownPartsMaker : ITownPartsAction
	{
		// Token: 0x0600226A RID: 8810 RVA: 0x000B761C File Offset: 0x000B5A1C
		public TownPartsMaker(TownBuilder pbuilder, Transform parent, GameObject phitnode, int flayer)
		{
			this.m_Builder = pbuilder;
			this.m_Active = true;
			this.m_CalcStep = TownPartsMaker.eCalcStep.Init;
			this.m_Maker = this.m_Builder.GetMakerGauge();
			this.m_Maker.transform.position = parent.position;
			this.m_LayerID = flayer;
			this.m_BlinkTime = 0f;
			this.m_MarkRender = null;
		}

		// Token: 0x0600226B RID: 8811 RVA: 0x000B7685 File Offset: 0x000B5A85
		public void SetEndMark()
		{
			this.m_Active = false;
		}

		// Token: 0x0600226C RID: 8812 RVA: 0x000B7690 File Offset: 0x000B5A90
		public override bool PartsUpdate()
		{
			TownPartsMaker.eCalcStep calcStep = this.m_CalcStep;
			if (calcStep != TownPartsMaker.eCalcStep.Init)
			{
				if (calcStep != TownPartsMaker.eCalcStep.Calc)
				{
					if (calcStep != TownPartsMaker.eCalcStep.End)
					{
					}
				}
				else
				{
					this.m_BlinkTime += Time.deltaTime;
					if (this.m_BlinkTime >= 1.5f)
					{
						this.m_BlinkTime -= 1.5f;
					}
					if (!this.m_Active)
					{
						TownMakerPoint component = this.m_Maker.GetComponent<TownMakerPoint>();
						if (component != null)
						{
							component.SetEnable(true);
						}
						this.m_Maker.SetActive(false);
						this.m_Builder.ReleaseMakerGauge(this.m_Maker);
						this.m_BlinkTime = 0f;
						this.SetBlinkHitModel();
						if (this.m_MarkRender != null)
						{
							for (int i = 0; i < this.m_MarkRender.Length; i++)
							{
								if (this.m_MarkRender[i] != null)
								{
									this.m_MarkRender[i].enabled = false;
									this.m_MarkRender[i] = null;
								}
							}
							this.m_MarkRender = null;
						}
						this.m_CalcStep = TownPartsMaker.eCalcStep.End;
					}
					else
					{
						this.SetBlinkHitModel();
					}
				}
			}
			else
			{
				this.m_CalcStep = TownPartsMaker.eCalcStep.Calc;
				this.m_Maker.SetActive(true);
				TownMakerPoint component2 = this.m_Maker.GetComponent<TownMakerPoint>();
				if (component2 != null)
				{
					component2.SetEnable(true);
				}
				Renderer[] componentsInChildren = this.m_Maker.GetComponentsInChildren<Renderer>();
				for (int j = 0; j < componentsInChildren.Length; j++)
				{
					componentsInChildren[j].sortingOrder = this.m_LayerID;
				}
			}
			return this.m_Active;
		}

		// Token: 0x0600226D RID: 8813 RVA: 0x000B782F File Offset: 0x000B5C2F
		private void SetBlinkHitModel()
		{
		}

		// Token: 0x04002920 RID: 10528
		private TownBuilder m_Builder;

		// Token: 0x04002921 RID: 10529
		private GameObject m_Maker;

		// Token: 0x04002922 RID: 10530
		private TownPartsMaker.eCalcStep m_CalcStep;

		// Token: 0x04002923 RID: 10531
		private int m_LayerID;

		// Token: 0x04002924 RID: 10532
		private Renderer[] m_MarkRender;

		// Token: 0x04002925 RID: 10533
		private float m_BlinkTime;

		// Token: 0x04002926 RID: 10534
		private const float BlinkTime = 1.5f;

		// Token: 0x020006C6 RID: 1734
		public enum eCalcStep
		{
			// Token: 0x04002928 RID: 10536
			Init,
			// Token: 0x04002929 RID: 10537
			Calc,
			// Token: 0x0400292A RID: 10538
			End
		}
	}
}
