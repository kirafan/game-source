﻿using System;

namespace Star
{
	// Token: 0x020001A9 RID: 425
	public static class SkillListDB_Ext
	{
		// Token: 0x06000B1B RID: 2843 RVA: 0x000422E0 File Offset: 0x000406E0
		public static SkillListDB_Param GetParam(this SkillListDB self, int skillID)
		{
			if (skillID != -1)
			{
				for (int i = 0; i < self.m_Params.Length; i++)
				{
					if (self.m_Params[i].m_ID == skillID)
					{
						return self.m_Params[i];
					}
				}
			}
			return default(SkillListDB_Param);
		}

		// Token: 0x06000B1C RID: 2844 RVA: 0x00042340 File Offset: 0x00040740
		public static string GetSkillActionPlanID(this SkillListDB self, int skillID)
		{
			if (skillID != -1)
			{
				for (int i = 0; i < self.m_Params.Length; i++)
				{
					if (self.m_Params[i].m_ID == skillID)
					{
						return self.m_Params[i].m_SAP;
					}
				}
			}
			return null;
		}

		// Token: 0x06000B1D RID: 2845 RVA: 0x00042398 File Offset: 0x00040798
		public static string GetSkillActionGraphicsID(this SkillListDB self, int skillID)
		{
			if (skillID != -1)
			{
				for (int i = 0; i < self.m_Params.Length; i++)
				{
					if (self.m_Params[i].m_ID == skillID)
					{
						return self.m_Params[i].m_SAG;
					}
				}
			}
			return null;
		}
	}
}
