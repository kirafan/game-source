﻿using System;

namespace Star
{
	// Token: 0x02000109 RID: 265
	public enum eSkillActionLocatorType
	{
		// Token: 0x040006D6 RID: 1750
		Origin = -1,
		// Token: 0x040006D7 RID: 1751
		Body,
		// Token: 0x040006D8 RID: 1752
		WpnTop = 0,
		// Token: 0x040006D9 RID: 1753
		WpnCenter,
		// Token: 0x040006DA RID: 1754
		WpnBottom
	}
}
