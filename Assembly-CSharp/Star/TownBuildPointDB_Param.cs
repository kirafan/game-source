﻿using System;

namespace Star
{
	// Token: 0x020006F1 RID: 1777
	[Serializable]
	public struct TownBuildPointDB_Param
	{
		// Token: 0x04002A0D RID: 10765
		public int m_Group;

		// Token: 0x04002A0E RID: 10766
		public ushort m_AttachType;

		// Token: 0x04002A0F RID: 10767
		public ushort m_ActiveNum;

		// Token: 0x04002A10 RID: 10768
		public string m_MakeID;

		// Token: 0x04002A11 RID: 10769
		public float m_ObjOffsetX;

		// Token: 0x04002A12 RID: 10770
		public float m_ObjOffsetY;

		// Token: 0x04002A13 RID: 10771
		public int m_AccessKey;

		// Token: 0x04002A14 RID: 10772
		public int m_Layer;

		// Token: 0x04002A15 RID: 10773
		public int m_WakeUpKey;
	}
}
