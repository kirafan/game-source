﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200024C RID: 588
	public class TownObjectLevelUpDB : ScriptableObject
	{
		// Token: 0x04001354 RID: 4948
		public TownObjectLevelUpDB_Param[] m_Params;
	}
}
