﻿using System;

namespace Star
{
	// Token: 0x0200054C RID: 1356
	public class CActScriptKeyPopUp : IRoomScriptData
	{
		// Token: 0x06001AC9 RID: 6857 RVA: 0x0008EE14 File Offset: 0x0008D214
		public override void CopyDat(ActXlsKeyBase pbase)
		{
			base.CopyDat(pbase);
			ActXlsKeyPopUp actXlsKeyPopUp = (ActXlsKeyPopUp)pbase;
			this.m_PopUpID = actXlsKeyPopUp.m_PopUpID;
			this.m_PopUpTime = (float)actXlsKeyPopUp.m_PopUpTime / 30f;
		}

		// Token: 0x04002195 RID: 8597
		public int m_PopUpID;

		// Token: 0x04002196 RID: 8598
		public float m_PopUpTime;
	}
}
