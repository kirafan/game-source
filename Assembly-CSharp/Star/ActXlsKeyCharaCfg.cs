﻿using System;

namespace Star
{
	// Token: 0x02000577 RID: 1399
	public class ActXlsKeyCharaCfg : ActXlsKeyBase
	{
		// Token: 0x06001B2B RID: 6955 RVA: 0x0008FA08 File Offset: 0x0008DE08
		public override void SerializeCode(BinaryIO pio)
		{
			base.SerializeCode(pio);
			pio.Int(ref this.m_OffsetLayer);
			pio.Float(ref this.m_OffsetZ);
		}

		// Token: 0x0400221E RID: 8734
		public int m_OffsetLayer;

		// Token: 0x0400221F RID: 8735
		public float m_OffsetZ;
	}
}
