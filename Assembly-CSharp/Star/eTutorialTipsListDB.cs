﻿using System;

namespace Star
{
	// Token: 0x02000256 RID: 598
	public enum eTutorialTipsListDB
	{
		// Token: 0x0400138D RID: 5005
		TUTORIAL_FINISH,
		// Token: 0x0400138E RID: 5006
		FIRST_QUEST = 101,
		// Token: 0x0400138F RID: 5007
		FIRST_PARTY_EDIT,
		// Token: 0x04001390 RID: 5008
		FIRST_SUPPORT_EDIT,
		// Token: 0x04001391 RID: 5009
		FIRST_CHARA_UPGRADE,
		// Token: 0x04001392 RID: 5010
		FIRST_CHARA_LIMITBREAK,
		// Token: 0x04001393 RID: 5011
		FIRST_CHARA_EVOLUTION,
		// Token: 0x04001394 RID: 5012
		FIRST_WPN_CREATE,
		// Token: 0x04001395 RID: 5013
		FIRST_WPN_UPGRADE,
		// Token: 0x04001396 RID: 5014
		BTL_BATTLE = 1000,
		// Token: 0x04001397 RID: 5015
		BTL_COMMAND,
		// Token: 0x04001398 RID: 5016
		BTL_ELEMENT,
		// Token: 0x04001399 RID: 5017
		BTL_MASTER,
		// Token: 0x0400139A RID: 5018
		BTL_CHARGE,
		// Token: 0x0400139B RID: 5019
		BTL_UNIQUESKILL,
		// Token: 0x0400139C RID: 5020
		Town_001 = 2000,
		// Token: 0x0400139D RID: 5021
		Town_002,
		// Token: 0x0400139E RID: 5022
		Town_003,
		// Token: 0x0400139F RID: 5023
		Town_004,
		// Token: 0x040013A0 RID: 5024
		Room_001 = 3000,
		// Token: 0x040013A1 RID: 5025
		Room_002,
		// Token: 0x040013A2 RID: 5026
		Room_003,
		// Token: 0x040013A3 RID: 5027
		Max
	}
}
