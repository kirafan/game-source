﻿using System;
using System.Collections.Generic;

namespace Star
{
	// Token: 0x020000F6 RID: 246
	public class BattleTutorial
	{
		// Token: 0x060006F5 RID: 1781 RVA: 0x000296AA File Offset: 0x00027AAA
		public BattleTutorial()
		{
			int[] array = new int[3];
			array[0] = 2;
			array[1] = 1;
			this.NeedCommandBtnIndexHistory = array;
			this.m_SingleTargetIndex = BattleDefine.eJoinMember.None;
			base..ctor();
		}

		// Token: 0x060006F6 RID: 1782 RVA: 0x000296D4 File Offset: 0x00027AD4
		public bool IsSeq(BattleTutorial.eSeq seq)
		{
			return this.m_Seq == seq;
		}

		// Token: 0x060006F7 RID: 1783 RVA: 0x000296E5 File Offset: 0x00027AE5
		public BattleTutorial.eSeq GetSeq()
		{
			return this.m_Seq;
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x000296ED File Offset: 0x00027AED
		public void SetSeq(BattleTutorial.eSeq seq)
		{
			this.m_Seq = seq;
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x000296F6 File Offset: 0x00027AF6
		public void SetSeq(int seq)
		{
			this.m_Seq = (BattleTutorial.eSeq)seq;
		}

		// Token: 0x060006FA RID: 1786 RVA: 0x00029700 File Offset: 0x00027B00
		public BattleTutorial.eSeq ApplyNextSeq()
		{
			BattleTutorial.eSeq seq = this.m_Seq + 1;
			this.m_Seq = seq;
			return this.m_Seq;
		}

		// Token: 0x060006FB RID: 1787 RVA: 0x00029723 File Offset: 0x00027B23
		public void AddCommandBtnIndexHistory(int index)
		{
			if (this.m_CommandBtnIndexHistory == null)
			{
				this.m_CommandBtnIndexHistory = new List<int>();
			}
			this.m_CommandBtnIndexHistory.Add(index);
		}

		// Token: 0x060006FC RID: 1788 RVA: 0x00029747 File Offset: 0x00027B47
		public void ClearCommandBtnIndexHistory()
		{
			if (this.m_CommandBtnIndexHistory != null)
			{
				this.m_CommandBtnIndexHistory.Clear();
			}
		}

		// Token: 0x060006FD RID: 1789 RVA: 0x00029760 File Offset: 0x00027B60
		public bool JudgeCommandBtnIndexHistory()
		{
			if (this.m_CommandBtnIndexHistory != null)
			{
				int[] needCommandBtnIndexHistory = this.NeedCommandBtnIndexHistory;
				bool flag = true;
				for (int i = 0; i < needCommandBtnIndexHistory.Length; i++)
				{
					if (!this.m_CommandBtnIndexHistory.Contains(needCommandBtnIndexHistory[i]))
					{
						flag = false;
						break;
					}
				}
				if (flag && this.m_CommandBtnIndexHistory[this.m_CommandBtnIndexHistory.Count - 1] == needCommandBtnIndexHistory[needCommandBtnIndexHistory.Length - 1])
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060006FE RID: 1790 RVA: 0x000297DE File Offset: 0x00027BDE
		public BattleDefine.eJoinMember GetSingleTargetIndex()
		{
			return this.m_SingleTargetIndex;
		}

		// Token: 0x060006FF RID: 1791 RVA: 0x000297E6 File Offset: 0x00027BE6
		public void SetSingleTargetIndex(BattleDefine.eJoinMember join)
		{
			this.m_SingleTargetIndex = join;
		}

		// Token: 0x04000616 RID: 1558
		private BattleTutorial.eSeq m_Seq = BattleTutorial.eSeq.None;

		// Token: 0x04000617 RID: 1559
		private List<int> m_CommandBtnIndexHistory;

		// Token: 0x04000618 RID: 1560
		public int[] NeedCommandBtnIndexHistory;

		// Token: 0x04000619 RID: 1561
		private BattleDefine.eJoinMember m_SingleTargetIndex;

		// Token: 0x020000F7 RID: 247
		public enum eSeq
		{
			// Token: 0x0400061B RID: 1563
			None = -1,
			// Token: 0x0400061C RID: 1564
			_00_WAVE_IDX_0_TIPS_START,
			// Token: 0x0400061D RID: 1565
			_01_WAVE_IDX_0_ATK,
			// Token: 0x0400061E RID: 1566
			_02_WAVE_IDX_0_TIPS_COMMAND,
			// Token: 0x0400061F RID: 1567
			_03_WAVE_IDX_0_SWIPE,
			// Token: 0x04000620 RID: 1568
			_04_WAVE_IDX_0_TIPS_ELEMENT,
			// Token: 0x04000621 RID: 1569
			_05_WAVE_IDX_0_TARGET,
			// Token: 0x04000622 RID: 1570
			_06_WAVE_IDX_0_ATK_AFTER_TARGET,
			// Token: 0x04000623 RID: 1571
			_07_WAVE_IDX_1_TIPS_START,
			// Token: 0x04000624 RID: 1572
			_08_WAVE_IDX_1_OWNER_IMAGE_SLIDE,
			// Token: 0x04000625 RID: 1573
			_09_WAVE_IDX_1_TIPS_CHARGESTUN,
			// Token: 0x04000626 RID: 1574
			_10_WAVE_IDX_1_ATK_AFTER_TIPS_STUN,
			// Token: 0x04000627 RID: 1575
			_11_WAVE_IDX_1_TIPS_UNIQUE_SKILL,
			// Token: 0x04000628 RID: 1576
			_12_WAVE_IDX_1_UNIQUE_SKILL,
			// Token: 0x04000629 RID: 1577
			Max
		}
	}
}
