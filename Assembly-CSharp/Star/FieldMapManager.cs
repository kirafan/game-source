﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Star
{
	// Token: 0x02000327 RID: 807
	public class FieldMapManager : MonoBehaviour
	{
		// Token: 0x06000F4C RID: 3916 RVA: 0x0005218C File Offset: 0x0005058C
		public static void CheckWakeUpObject(GameObject plink)
		{
			if (FieldMapManager.Inst == null)
			{
				ScheduleNameUtil.DatabaseSetUp();
				ScheduleDataBase.DatabaseSetUp();
				TownItemDropDatabase.DatabaseSetUp();
				SystemScheduleLinkManager.GetManager();
				TownDefine.CalcRoomManageID();
				FieldMapManager inst = plink.AddComponent<FieldMapManager>();
				FieldMapManager.Inst = inst;
				FieldMapManager.Inst.SetUpUserDataToManageData();
				FieldMapManager.Inst.enabled = false;
			}
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x000521E5 File Offset: 0x000505E5
		public static bool IsSetUp()
		{
			if (ScheduleNameUtil.IsSetUp() && ScheduleDataBase.IsSetUp() && TownItemDropDatabase.IsSetUp())
			{
				FieldMapManager.Inst.SetResToBuildData();
				FieldMapManager.Inst.enabled = true;
				return true;
			}
			return false;
		}

		// Token: 0x06000F4E RID: 3918 RVA: 0x0005221D File Offset: 0x0005061D
		public static void StoreRelease()
		{
			if (FieldMapManager.Inst != null)
			{
				UnityEngine.Object.Destroy(FieldMapManager.Inst);
				FieldMapManager.Inst = null;
			}
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x0005223F File Offset: 0x0005063F
		public static void EntryBuildCallback(BuildUpCallback pbuildup, BuildUpCallback pchangeup)
		{
			FieldMapManager.Inst.m_BuildMap.EntryCallbackBuildUp(pbuildup, pchangeup);
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x00052254 File Offset: 0x00050654
		public static void EntryBuildState(long fmanageid, FeedBackResultCallback pcallback)
		{
			FieldObjHandleBuild fieldBuildState = FieldMapManager.Inst.m_BuildMap.GetFieldBuildState(fmanageid);
			if (fieldBuildState != null)
			{
				fieldBuildState.m_Callback = pcallback;
			}
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x0005227F File Offset: 0x0005067F
		public static FieldObjHandleBuild GetBuildState(long fmanageid)
		{
			return FieldMapManager.Inst.m_BuildMap.GetFieldBuildState(fmanageid);
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x00052291 File Offset: 0x00050691
		public static FieldObjHandleChara GetCharaState(long fmanageid)
		{
			return FieldMapManager.Inst.GetFieldCharaState(fmanageid);
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x0005229E File Offset: 0x0005069E
		private void SetUpUserDataToManageData()
		{
			ScheduleTimeUtil.SetMarkingTime();
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x000522A8 File Offset: 0x000506A8
		private void SetResToBuildData()
		{
			if (!this.m_InitSetUp)
			{
				this.m_ComQue = new FieldBuildCmdQue();
				this.m_ReqManage = new FieldBuildMapReq();
				this.m_BuildMap = new FieldBuildMap(this.m_ReqManage, this.m_ComQue);
				this.m_BuildMap.SetUpTownData();
				this.m_CharaTable = new FieldObjHandleChara[4];
				this.ChkRoomInCharaManage(null);
				this.InitRoomInCharaToBuild();
				long chkUpPlayTime = SystemScheduleLinkManager.GetChkUpPlayTime();
				this.m_UpdateTime = chkUpPlayTime;
				SystemScheduleLinkManager.ChkUpPlayTime(chkUpPlayTime);
				this.m_InitSetUp = true;
			}
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x0005232C File Offset: 0x0005072C
		public void ChkRoomInCharaManage(CharaBuildState pcallback)
		{
			this.ClearSearchFlag();
			int roomInCharaNum = UserDataUtil.FieldChara.GetRoomInCharaNum();
			for (int i = 0; i < roomInCharaNum; i++)
			{
				UserFieldCharaData.RoomInCharaData roomInCharaAt = UserDataUtil.FieldChara.GetRoomInCharaAt(i);
				if (!this.SearchChara(roomInCharaAt.ManageID))
				{
					this.AddFldCharacter(roomInCharaAt.ManageID, roomInCharaAt.RoomID, pcallback, false);
				}
				else
				{
					this.CheckChangeRoomChara(roomInCharaAt.ManageID, roomInCharaAt.RoomID, pcallback, false);
				}
			}
			this.DeleteCharaCheck(pcallback);
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x000523B0 File Offset: 0x000507B0
		public void InitRoomInCharaToBuild()
		{
			int roomInCharaNum = UserDataUtil.FieldChara.GetRoomInCharaNum();
			for (int i = 0; i < roomInCharaNum; i++)
			{
				UserFieldCharaData.RoomInCharaData roomInCharaAt = UserDataUtil.FieldChara.GetRoomInCharaAt(i);
				FieldObjHandleBuild buildPointState = this.m_BuildMap.GetBuildPointState(roomInCharaAt.TownBuildManageID);
				if (buildPointState != null)
				{
					buildPointState.AddCharaManageID(roomInCharaAt.ManageID);
				}
			}
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x0005240C File Offset: 0x0005080C
		private void Update()
		{
			ScheduleTimeUtil.SetMarkingTime();
			long systemTimeSec = ScheduleTimeUtil.GetSystemTimeSec();
			CharaScheduleTimeSimulate charaScheduleTimeSimulate = null;
			int num = this.m_CharaTable.Length;
			bool frefresh = false;
			long dayKey = ScheduleTimeUtil.GetDayKey(this.m_UpdateTime);
			long dayKey2 = ScheduleTimeUtil.GetDayKey(systemTimeSec);
			List<FieldObjHandleChara> list = null;
			for (int i = 0; i < num; i++)
			{
				FieldObjHandleChara fieldObjHandleChara = this.m_CharaTable[i];
				if (fieldObjHandleChara != null)
				{
					long lastScheduleSettingTime = fieldObjHandleChara.GetLastScheduleSettingTime();
					if (dayKey2 != lastScheduleSettingTime || (UserCharaUtil.GetRoomChara(fieldObjHandleChara.m_ManageID) != null && UserCharaUtil.GetRoomChara(fieldObjHandleChara.m_ManageID).IsReservedReCreateSchedule()))
					{
						if (list == null)
						{
							list = new List<FieldObjHandleChara>();
						}
						UserCharaUtil.GetRoomChara(fieldObjHandleChara.m_ManageID).ClearReservedReCreateSchedule();
						list.Add(fieldObjHandleChara);
					}
				}
			}
			if (list != null && list.Count > 0)
			{
				charaScheduleTimeSimulate = new CharaScheduleTimeSimulate();
				charaScheduleTimeSimulate.CreateTable(list.Count);
				for (int j = 0; j < list.Count; j++)
				{
					FieldObjHandleChara fieldObjHandleChara = list[j];
					if (fieldObjHandleChara != null)
					{
						charaScheduleTimeSimulate.AddSumilateState(fieldObjHandleChara);
					}
				}
				dayKey = ScheduleTimeUtil.GetDayKey(this.m_UpdateTime);
				charaScheduleTimeSimulate.CalcSimulate2(this.m_UpdateTime, (long)ScheduleTimeUtil.ChangeUnitTimeSec((int)(dayKey + 1L), 0, 0), this.m_BuildMap, false, true);
				this.m_UpdateTime = (long)ScheduleTimeUtil.ChangeUnitTimeSec((int)dayKey2, 0, 0);
				frefresh = true;
			}
			else
			{
				for (int i = 0; i < num; i++)
				{
					FieldObjHandleChara fieldObjHandleChara = this.m_CharaTable[i];
					if (fieldObjHandleChara != null && fieldObjHandleChara.UpdateTimeChk(systemTimeSec) && !FldComPartyScript.IsExistWaitCmd(FldComPartyScript.eCmd.ChangeSchedule))
					{
						if (charaScheduleTimeSimulate == null)
						{
							charaScheduleTimeSimulate = new CharaScheduleTimeSimulate();
							charaScheduleTimeSimulate.CreateTable(num);
						}
						charaScheduleTimeSimulate.AddSumilateState(fieldObjHandleChara);
					}
				}
			}
			SystemScheduleLinkManager.ChkUpPlayTime(systemTimeSec);
			if (charaScheduleTimeSimulate != null)
			{
				FldComPartyScript fldComPartyScript = new FldComPartyScript();
				if (charaScheduleTimeSimulate.CalcSimulate2(this.m_UpdateTime, systemTimeSec, this.m_BuildMap, frefresh, false))
				{
					charaScheduleTimeSimulate.SetUpPartyCom(fldComPartyScript);
					fldComPartyScript.PlaySend();
				}
			}
			this.m_UpdateTime = systemTimeSec;
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].UpdateHandle(this.m_ReqManage);
				}
			}
			if (this.m_ReqManage.IsRequest())
			{
				this.m_BuildMap.PlayRequest(this.m_ReqManage, ScheduleTimeUtil.GetSystemTimeBase());
				this.m_ReqManage.ClearRequest();
			}
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00052688 File Offset: 0x00050A88
		public static void AddTownBuildObject(int fbuildpoint, int fobjid, long fmanageid)
		{
			if (fbuildpoint > 0)
			{
				if (UserTownUtil.CheckPointToBuilding(fbuildpoint, fobjid))
				{
					if (fmanageid != -1L)
					{
						UserTownObjectData townObjData = UserTownUtil.GetTownObjData(fmanageid);
						if (townObjData != null)
						{
							FieldMapManager.Inst.m_BuildMap.AddTownBuildInMakeID(fbuildpoint, townObjData.ObjID, townObjData.m_ManageID, false);
						}
						else
						{
							FieldMapManager.Inst.m_BuildMap.AddTownBuildInMakeID(fbuildpoint, fobjid, fmanageid, true);
						}
					}
					else
					{
						FieldMapManager.Inst.m_BuildMap.AddTownBuildInMakeID(fbuildpoint, fobjid, fmanageid, true);
					}
				}
				else
				{
					Debug.LogWarning("Build Point Not Object\n");
				}
			}
			else
			{
				Debug.LogWarning("Build Point Error\n");
			}
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00052728 File Offset: 0x00050B28
		public static bool IsPointToBuilding(int fbuildpoint)
		{
			UserTownData townData = UserDataUtil.TownData;
			for (int i = 0; i < townData.GetBuildObjectDataNum(); i++)
			{
				if (townData.GetBuildObjectDataAt(i).m_BuildPointIndex == fbuildpoint)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000F5A RID: 3930 RVA: 0x00052767 File Offset: 0x00050B67
		public static void ChangeTownBuildObject(int fbuildpoint, int fobjid, long fmanageid)
		{
			FieldMapManager.Inst.m_BuildMap.ChangeTownBuildObject(fbuildpoint, fobjid, fmanageid, new Action<bool>(FieldMapManager.Inst.CheckBuildToCharaSchedule));
		}

		// Token: 0x06000F5B RID: 3931 RVA: 0x0005278B File Offset: 0x00050B8B
		public static void ChangeTownBuildLevelWait(int fbuildpoint, long fmanageid, bool fopen)
		{
			FieldMapManager.Inst.m_BuildMap.ChangeBuildLevelUp(fbuildpoint, fmanageid, fopen);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F5C RID: 3932 RVA: 0x000527AA File Offset: 0x00050BAA
		public static void RemoveTownBuildObject(long fmanageid, int fbuildpoint, Action<bool> callback)
		{
			FieldMapManager.Inst.m_BuildMap.RemoveTownBuildObject(fmanageid, fbuildpoint, callback);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x000527CC File Offset: 0x00050BCC
		public static void AreaSwapBuild(int fpoint1, int fpoint2)
		{
			int buildAreaID = TownUtility.GetBuildAreaID(fpoint1);
			int buildAreaID2 = TownUtility.GetBuildAreaID(fpoint2);
			FieldMapManager.Inst.m_BuildMap.AreaSwapBuildTable(buildAreaID, buildAreaID2);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x00052804 File Offset: 0x00050C04
		public static void AreaMoveBuild(int fpoint1, int fpoint2)
		{
			int buildAreaID = TownUtility.GetBuildAreaID(fpoint1);
			int buildAreaID2 = TownUtility.GetBuildAreaID(fpoint2);
			FieldMapManager.Inst.m_BuildMap.AreaMoveBuild(buildAreaID, buildAreaID2);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x0005283B File Offset: 0x00050C3B
		public static void BuildSwapPoint(int fpoint1, int fpoint2)
		{
			FieldMapManager.Inst.m_BuildMap.BuildSwapPointTable(fpoint1, fpoint2);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F60 RID: 3936 RVA: 0x00052859 File Offset: 0x00050C59
		public static void BuildMovePoint(int fnowpoint, int fnextpoint)
		{
			FieldMapManager.Inst.m_BuildMap.BuildMovePoint(fnowpoint, fnextpoint);
			FieldMapManager.Inst.CheckBuildToCharaSchedule(true);
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x00052878 File Offset: 0x00050C78
		public void SetInitBuildPoint(long fcharamngid, long fbuildmngid)
		{
			FieldObjHandleBuild buildPointState = this.m_BuildMap.GetBuildPointState(fbuildmngid);
			if (buildPointState != null)
			{
				buildPointState.AddCharaManageID(fcharamngid);
			}
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x0005289F File Offset: 0x00050C9F
		public static void RefreshManageChara(CharaBuildState pcallback)
		{
			FieldMapManager.Inst.ChkRoomInCharaManage(pcallback);
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x000528AC File Offset: 0x00050CAC
		public static void ActiveBuildObject(long fmanageid, bool fgemup = false, Action<bool> pcallback = null)
		{
			UserTownData.BuildObjectData townBuildData = UserTownUtil.GetTownBuildData(fmanageid);
			townBuildData.m_IsOpen = true;
			townBuildData.SetActionTime(ScheduleTimeUtil.GetSystemTimeMs());
			if (townBuildData.IsNextLevelUpObj())
			{
				townBuildData.AddLevel(1);
			}
			FldComBuildScript fldComBuildScript = new FldComBuildScript();
			fldComBuildScript.SetMarkTime(townBuildData.m_ActionTime);
			if (fgemup)
			{
				fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.GemLevelUpBuild, fmanageid, 0, 0, null);
			}
			else
			{
				fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.LevelUpBuild, fmanageid, 0, 0, null);
			}
			fldComBuildScript.StackSendCmd(FldComBuildScript.eCmd.ChangeBuild, fmanageid, 0, 0, new IFldNetComModule.CallBack(FieldMapManager.Inst.CallbackLevelUpCom));
			fldComBuildScript.PlaySend();
			FieldBuildCmdLevelUp pque = new FieldBuildCmdLevelUp(fmanageid);
			FieldMapManager.Inst.m_ComQue.AddComQue(pque, pcallback);
		}

		// Token: 0x06000F64 RID: 3940 RVA: 0x00052950 File Offset: 0x00050D50
		private void CallbackLevelUpCom(IFldNetComModule pmodule)
		{
			FieldBuildCmdLevelUp fieldBuildCmdLevelUp = this.m_ComQue.GetQue(typeof(FieldBuildCmdLevelUp)) as FieldBuildCmdLevelUp;
			if (fieldBuildCmdLevelUp != null)
			{
				FieldObjHandleBuild fieldBuildState = FieldMapManager.Inst.m_BuildMap.GetFieldBuildState(fieldBuildCmdLevelUp.m_ManageID);
				if (fieldBuildState != null && fieldBuildState.m_Callback != null)
				{
					fieldBuildState.m_Callback(eCallBackType.CompBuild, fieldBuildState, false);
				}
				fieldBuildCmdLevelUp.CheckCallback(true);
				this.m_ComQue.RemoveQue(typeof(FieldBuildCmdLevelUp));
			}
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x000529D0 File Offset: 0x00050DD0
		public FieldObjHandleChara GetFieldCharaState(long fmanageid)
		{
			FieldObjHandleChara result = null;
			int num = this.m_CharaTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fmanageid)
				{
					result = this.m_CharaTable[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F66 RID: 3942 RVA: 0x00052A2C File Offset: 0x00050E2C
		public void AddFldCharacter(long fmanageid, int froomid, CharaBuildState pcallback, bool fdebug = false)
		{
			bool flag = false;
			int num = 0;
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] == null)
				{
					num = i;
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				FieldObjHandleChara[] array = new FieldObjHandleChara[this.m_CharaTable.Length + 4];
				for (int i = 0; i < this.m_CharaTable.Length; i++)
				{
					array[i] = this.m_CharaTable[i];
				}
				num = this.m_CharaTable.Length;
				this.m_CharaTable = null;
				this.m_CharaTable = array;
			}
			FieldObjHandleChara fieldObjHandleChara = this.m_CharaTable[num] = new FieldObjHandleChara(fmanageid, froomid);
			fieldObjHandleChara.SetUpChara(num, fmanageid, ScheduleTimeUtil.GetSystemTimeSec());
			fieldObjHandleChara.m_SearchActive = true;
			if (pcallback != null)
			{
				pcallback(eCharaBuildUp.Add, fmanageid, froomid);
			}
		}

		// Token: 0x06000F67 RID: 3943 RVA: 0x00052AF8 File Offset: 0x00050EF8
		public bool CheckChangeRoomChara(long fmanageid, int froomid, CharaBuildState pcallback, bool fdebug = false)
		{
			bool result = false;
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fmanageid && this.m_CharaTable[i].m_RoomID != froomid)
				{
					this.m_CharaTable[i].m_RoomID = froomid;
					if (pcallback != null)
					{
						pcallback(eCharaBuildUp.ChangeRoom, fmanageid, froomid);
					}
					result = true;
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F68 RID: 3944 RVA: 0x00052B78 File Offset: 0x00050F78
		private void ClearSearchFlag()
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null)
				{
					this.m_CharaTable[i].m_SearchActive = false;
				}
			}
		}

		// Token: 0x06000F69 RID: 3945 RVA: 0x00052BBC File Offset: 0x00050FBC
		private bool SearchChara(long fid)
		{
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].m_ManageID == fid)
				{
					this.m_CharaTable[i].m_SearchActive = true;
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000F6A RID: 3946 RVA: 0x00052C14 File Offset: 0x00051014
		private bool DeleteCharaCheck(CharaBuildState pcallback)
		{
			bool result = false;
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && !this.m_CharaTable[i].m_SearchActive)
				{
					if (pcallback != null)
					{
						pcallback(eCharaBuildUp.Del, this.m_CharaTable[i].m_ManageID, -1);
					}
					this.m_CharaTable[i].ReleaseParam();
					this.m_CharaTable[i] = null;
					result = true;
				}
			}
			return result;
		}

		// Token: 0x06000F6B RID: 3947 RVA: 0x00052C90 File Offset: 0x00051090
		public static ScheduleUIDataPack GetCharaManageIDToScheduleUIData(long fmanageid)
		{
			ScheduleUIDataPack result = null;
			int num = FieldMapManager.Inst.m_CharaTable.Length;
			for (int i = 0; i < num; i++)
			{
				if (FieldMapManager.Inst.m_CharaTable[i] != null && FieldMapManager.Inst.m_CharaTable[i].m_ManageID == fmanageid)
				{
					FieldObjHandleChara fieldObjHandleChara = FieldMapManager.Inst.m_CharaTable[i];
					result = fieldObjHandleChara.CreateScheduleUIData();
					break;
				}
			}
			return result;
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x00052D00 File Offset: 0x00051100
		public static void BuildUpOptionCode(int fobjid, int flevel)
		{
			TownObjectListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjListDB.GetParam(fobjid);
			eXlsTownUseResourceType buildOptionFunc = (eXlsTownUseResourceType)SingletonMonoBehaviour<GameSystem>.Inst.DbMng.TownObjLvUpDB.GetParam(param.m_LevelUpListID, flevel).m_BuildOptionFunc;
			if (buildOptionFunc != eXlsTownUseResourceType.Non && buildOptionFunc != eXlsTownUseResourceType.ContentNum)
			{
				if (buildOptionFunc == eXlsTownUseResourceType.RoomAdd)
				{
					RoomComUnlockDefault roomComUnlockDefault = new RoomComUnlockDefault(1);
					roomComUnlockDefault.PlaySend();
				}
			}
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x00052D78 File Offset: 0x00051178
		private void CheckBuildToCharaSchedule(bool fsuccess = true)
		{
			FldComPartyScript fldComPartyScript = null;
			for (int i = 0; i < this.m_CharaTable.Length; i++)
			{
				if (this.m_CharaTable[i] != null && this.m_CharaTable[i].IsBuildToScheduleChange())
				{
					if (fldComPartyScript == null)
					{
						fldComPartyScript = new FldComPartyScript();
					}
					fldComPartyScript.StackSendCmd(FldComPartyScript.eCmd.ChangeSchedule, this.m_CharaTable[i].m_ManageID, null);
				}
			}
			if (fldComPartyScript != null)
			{
				fldComPartyScript.PlaySend();
			}
		}

		// Token: 0x040016B7 RID: 5815
		private static FieldMapManager Inst;

		// Token: 0x040016B8 RID: 5816
		private FieldBuildMapReq m_ReqManage;

		// Token: 0x040016B9 RID: 5817
		private FieldObjHandleChara[] m_CharaTable;

		// Token: 0x040016BA RID: 5818
		private FieldBuildMap m_BuildMap;

		// Token: 0x040016BB RID: 5819
		private FieldBuildCmdQue m_ComQue;

		// Token: 0x040016BC RID: 5820
		private bool m_InitSetUp;

		// Token: 0x040016BD RID: 5821
		private long m_StartTime;

		// Token: 0x040016BE RID: 5822
		private long m_UpdateTime;

		// Token: 0x02000328 RID: 808
		public class MemberComCheck
		{
			// Token: 0x06000F6E RID: 3950 RVA: 0x00052DEC File Offset: 0x000511EC
			public MemberComCheck()
			{
				this.m_Send = false;
				this.m_ComScript = new FldComPartyScript();
			}

			// Token: 0x06000F6F RID: 3951 RVA: 0x00052E08 File Offset: 0x00051208
			public void CallbackEvent(UserFieldCharaData.eUpState finout, UserFieldCharaData.RoomInCharaData pchara)
			{
				if (finout != UserFieldCharaData.eUpState.In)
				{
					if (finout != UserFieldCharaData.eUpState.StateChg)
					{
						if (finout == UserFieldCharaData.eUpState.Out)
						{
							this.m_ComScript.StackSendCmd(FldComPartyScript.eCmd.RemoveMember, pchara.ServerManageID, null);
						}
					}
					else
					{
						this.m_ComScript.StackSendCmd(FldComPartyScript.eCmd.ChangeState, pchara.ManageID, null);
					}
				}
				else
				{
					UserFieldCharaData.RoomInCharaData roomInChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.GetRoomInChara(pchara.ManageID);
					roomInChara.CheckDefaultScheduleUp(ScheduleTimeUtil.GetSystemTimeSec());
					this.m_ComScript.StackSendCmd(FldComPartyScript.eCmd.AddMember, pchara.ManageID, null);
				}
				this.m_Send = true;
			}

			// Token: 0x06000F70 RID: 3952 RVA: 0x00052EA3 File Offset: 0x000512A3
			public void SendComCheck()
			{
				if (this.m_Send)
				{
					this.m_ComScript.PlaySend();
					this.m_ComScript = null;
				}
			}

			// Token: 0x040016BF RID: 5823
			private bool m_Send;

			// Token: 0x040016C0 RID: 5824
			private FldComPartyScript m_ComScript;
		}

		// Token: 0x02000329 RID: 809
		public class CBuildStateEvent
		{
			// Token: 0x06000F71 RID: 3953 RVA: 0x00052EC2 File Offset: 0x000512C2
			public CBuildStateEvent(FieldObjHandleBuild pbase, eCallBackType ftype)
			{
				this.m_Base = pbase;
				this.m_Type = ftype;
			}

			// Token: 0x040016C1 RID: 5825
			public FieldObjHandleBuild m_Base;

			// Token: 0x040016C2 RID: 5826
			public eCallBackType m_Type;
		}
	}
}
