﻿using System;

namespace Star
{
	// Token: 0x02000225 RID: 549
	public enum eTitleType
	{
		// Token: 0x04000EED RID: 3821
		None = -1,
		// Token: 0x04000EEE RID: 3822
		Title_0000,
		// Token: 0x04000EEF RID: 3823
		Title_0001,
		// Token: 0x04000EF0 RID: 3824
		Title_0002,
		// Token: 0x04000EF1 RID: 3825
		Title_0003,
		// Token: 0x04000EF2 RID: 3826
		Title_0004,
		// Token: 0x04000EF3 RID: 3827
		Title_0005,
		// Token: 0x04000EF4 RID: 3828
		Title_0006,
		// Token: 0x04000EF5 RID: 3829
		Title_0007,
		// Token: 0x04000EF6 RID: 3830
		Title_0008,
		// Token: 0x04000EF7 RID: 3831
		Title_0009,
		// Token: 0x04000EF8 RID: 3832
		Title_0010,
		// Token: 0x04000EF9 RID: 3833
		Title_0011,
		// Token: 0x04000EFA RID: 3834
		Title_0012,
		// Token: 0x04000EFB RID: 3835
		Title_0013,
		// Token: 0x04000EFC RID: 3836
		Title_0014,
		// Token: 0x04000EFD RID: 3837
		Title_0015,
		// Token: 0x04000EFE RID: 3838
		Title_0016,
		// Token: 0x04000EFF RID: 3839
		Title_0017,
		// Token: 0x04000F00 RID: 3840
		Title_0018
	}
}
