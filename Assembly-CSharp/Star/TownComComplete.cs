﻿using System;

namespace Star
{
	// Token: 0x020004B0 RID: 1200
	public class TownComComplete : IMainComCommand
	{
		// Token: 0x0600176E RID: 5998 RVA: 0x000794FF File Offset: 0x000778FF
		public TownComComplete(TownUtility.eResCategory fcategory)
		{
			this.m_Type = fcategory;
		}

		// Token: 0x0600176F RID: 5999 RVA: 0x0007950E File Offset: 0x0007790E
		public override int GetHashCode()
		{
			return 4;
		}

		// Token: 0x04001E2E RID: 7726
		public long m_ManageID;

		// Token: 0x04001E2F RID: 7727
		public int m_BuildPoint;

		// Token: 0x04001E30 RID: 7728
		public int m_ObjID;

		// Token: 0x04001E31 RID: 7729
		public TownUtility.eResCategory m_Type;
	}
}
