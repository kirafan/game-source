﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000083 RID: 131
	[RequireComponent(typeof(ActionAggregation))]
	[AddComponentMenu("ActionAggregation/ChildActionAggregation")]
	[Serializable]
	public class ActionAggregationChildActionAggregationActionAdapter : ActionAggregationChildActionAdapterExGen<ActionAggregation>
	{
		// Token: 0x0600040B RID: 1035 RVA: 0x00013F90 File Offset: 0x00012390
		public override void Update()
		{
			if (base.GetState() == eActionAggregationState.Play && this.m_Action.IsFinished())
			{
				base.SetState(eActionAggregationState.Finish);
			}
		}

		// Token: 0x0600040C RID: 1036 RVA: 0x00013FB5 File Offset: 0x000123B5
		public override void PlayStartExtendProcess()
		{
			this.m_Action.PlayStart();
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x00013FC2 File Offset: 0x000123C2
		public override void Skip()
		{
			this.m_Action.Skip();
			base.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x00013FD6 File Offset: 0x000123D6
		public override void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x00013FD8 File Offset: 0x000123D8
		public override void ForceRecoveryFinishToWaitExtendProcess()
		{
			this.m_Action.ForceRecoveryChildFinishToWait();
		}
	}
}
