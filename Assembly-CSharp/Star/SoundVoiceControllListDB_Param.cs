﻿using System;

namespace Star
{
	// Token: 0x0200021C RID: 540
	[Serializable]
	public struct SoundVoiceControllListDB_Param
	{
		// Token: 0x04000E0A RID: 3594
		public int m_ID;

		// Token: 0x04000E0B RID: 3595
		public SoundVoiceControllListDB_Data[] m_Datas;
	}
}
