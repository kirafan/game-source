﻿using System;

namespace Star
{
	// Token: 0x0200021E RID: 542
	public enum eSoundVoiceListDB
	{
		// Token: 0x04000E0E RID: 3598
		voice_000,
		// Token: 0x04000E0F RID: 3599
		voice_001,
		// Token: 0x04000E10 RID: 3600
		voice_002,
		// Token: 0x04000E11 RID: 3601
		voice_003,
		// Token: 0x04000E12 RID: 3602
		voice_004,
		// Token: 0x04000E13 RID: 3603
		voice_005,
		// Token: 0x04000E14 RID: 3604
		voice_006,
		// Token: 0x04000E15 RID: 3605
		voice_007,
		// Token: 0x04000E16 RID: 3606
		voice_008,
		// Token: 0x04000E17 RID: 3607
		voice_009,
		// Token: 0x04000E18 RID: 3608
		voice_010,
		// Token: 0x04000E19 RID: 3609
		voice_011,
		// Token: 0x04000E1A RID: 3610
		voice_012,
		// Token: 0x04000E1B RID: 3611
		voice_013,
		// Token: 0x04000E1C RID: 3612
		voice_014,
		// Token: 0x04000E1D RID: 3613
		voice_015,
		// Token: 0x04000E1E RID: 3614
		voice_016,
		// Token: 0x04000E1F RID: 3615
		voice_017,
		// Token: 0x04000E20 RID: 3616
		voice_100 = 100,
		// Token: 0x04000E21 RID: 3617
		voice_101,
		// Token: 0x04000E22 RID: 3618
		voice_102,
		// Token: 0x04000E23 RID: 3619
		voice_103,
		// Token: 0x04000E24 RID: 3620
		voice_104,
		// Token: 0x04000E25 RID: 3621
		voice_105,
		// Token: 0x04000E26 RID: 3622
		voice_106,
		// Token: 0x04000E27 RID: 3623
		voice_107,
		// Token: 0x04000E28 RID: 3624
		voice_108,
		// Token: 0x04000E29 RID: 3625
		voice_200 = 200,
		// Token: 0x04000E2A RID: 3626
		voice_201,
		// Token: 0x04000E2B RID: 3627
		voice_300 = 300,
		// Token: 0x04000E2C RID: 3628
		voice_301,
		// Token: 0x04000E2D RID: 3629
		voice_302,
		// Token: 0x04000E2E RID: 3630
		voice_303,
		// Token: 0x04000E2F RID: 3631
		voice_304,
		// Token: 0x04000E30 RID: 3632
		voice_305,
		// Token: 0x04000E31 RID: 3633
		voice_306,
		// Token: 0x04000E32 RID: 3634
		voice_307,
		// Token: 0x04000E33 RID: 3635
		voice_308,
		// Token: 0x04000E34 RID: 3636
		voice_309,
		// Token: 0x04000E35 RID: 3637
		voice_310,
		// Token: 0x04000E36 RID: 3638
		voice_311,
		// Token: 0x04000E37 RID: 3639
		voice_312,
		// Token: 0x04000E38 RID: 3640
		voice_313,
		// Token: 0x04000E39 RID: 3641
		voice_314,
		// Token: 0x04000E3A RID: 3642
		voice_315,
		// Token: 0x04000E3B RID: 3643
		voice_316,
		// Token: 0x04000E3C RID: 3644
		voice_317,
		// Token: 0x04000E3D RID: 3645
		voice_318,
		// Token: 0x04000E3E RID: 3646
		voice_319,
		// Token: 0x04000E3F RID: 3647
		voice_320,
		// Token: 0x04000E40 RID: 3648
		voice_400 = 400,
		// Token: 0x04000E41 RID: 3649
		voice_401,
		// Token: 0x04000E42 RID: 3650
		voice_402,
		// Token: 0x04000E43 RID: 3651
		voice_403,
		// Token: 0x04000E44 RID: 3652
		voice_404,
		// Token: 0x04000E45 RID: 3653
		voice_405,
		// Token: 0x04000E46 RID: 3654
		voice_406,
		// Token: 0x04000E47 RID: 3655
		voice_407,
		// Token: 0x04000E48 RID: 3656
		voice_408,
		// Token: 0x04000E49 RID: 3657
		voice_409,
		// Token: 0x04000E4A RID: 3658
		voice_410,
		// Token: 0x04000E4B RID: 3659
		voice_411,
		// Token: 0x04000E4C RID: 3660
		voice_412,
		// Token: 0x04000E4D RID: 3661
		voice_413,
		// Token: 0x04000E4E RID: 3662
		voice_414,
		// Token: 0x04000E4F RID: 3663
		voice_415,
		// Token: 0x04000E50 RID: 3664
		voice_416,
		// Token: 0x04000E51 RID: 3665
		voice_417,
		// Token: 0x04000E52 RID: 3666
		voice_418,
		// Token: 0x04000E53 RID: 3667
		voice_419,
		// Token: 0x04000E54 RID: 3668
		voice_420,
		// Token: 0x04000E55 RID: 3669
		voice_421,
		// Token: 0x04000E56 RID: 3670
		Max
	}
}
