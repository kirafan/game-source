﻿using System;

namespace Star
{
	// Token: 0x020006AE RID: 1710
	public class TownMenuActCreate
	{
		// Token: 0x06002221 RID: 8737 RVA: 0x000B55AC File Offset: 0x000B39AC
		public static ITownMenuAct Create(eTownMenuType ftype, TownBuilder pbuilder)
		{
			ITownMenuAct townMenuAct;
			if (ftype != eTownMenuType.Traning)
			{
				if (ftype != eTownMenuType.Mission)
				{
					townMenuAct = new TownMenuActBase();
				}
				else
				{
					townMenuAct = new TownMenuActMission();
				}
			}
			else
			{
				townMenuAct = new TownMenuActTraning();
			}
			townMenuAct.SetUp(pbuilder);
			return townMenuAct;
		}
	}
}
