﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x0200061A RID: 1562
	public class RoomObjectOptionListDB : ScriptableObject
	{
		// Token: 0x04002526 RID: 9510
		public RoomObjectOptionListDB_Param[] m_Params;
	}
}
