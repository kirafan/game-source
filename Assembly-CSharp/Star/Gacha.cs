﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using GachaLogRequestTypes;
using GachaLogResponseTypes;
using GachaRequestTypes;
using GachaResponseTypes;
using Meige;
using WWWTypes;

namespace Star
{
	// Token: 0x020003C1 RID: 961
	public sealed class Gacha
	{
		// Token: 0x06001247 RID: 4679 RVA: 0x00060EDB File Offset: 0x0005F2DB
		public List<Gacha.GachaData> GetGachaList()
		{
			return this.m_GachaList;
		}

		// Token: 0x06001248 RID: 4680 RVA: 0x00060EE4 File Offset: 0x0005F2E4
		public Gacha.GachaData GetGachaData(int gachaID)
		{
			for (int i = 0; i < this.m_GachaList.Count; i++)
			{
				if (this.m_GachaList[i].m_ID == gachaID)
				{
					return this.m_GachaList[i];
				}
			}
			return null;
		}

		// Token: 0x06001249 RID: 4681 RVA: 0x00060F32 File Offset: 0x0005F332
		public List<Gacha.StepGachaData> GetStepGachaList()
		{
			return this.m_StepGachaList;
		}

		// Token: 0x0600124A RID: 4682 RVA: 0x00060F3C File Offset: 0x0005F33C
		public Gacha.StepGachaData GetStepGachaData(int gachaID)
		{
			for (int i = 0; i < this.m_StepGachaList.Count; i++)
			{
				if (this.m_StepGachaList[i].m_ID == gachaID)
				{
					return this.m_StepGachaList[i];
				}
			}
			return null;
		}

		// Token: 0x0600124B RID: 4683 RVA: 0x00060F8A File Offset: 0x0005F38A
		public int GetResultNum()
		{
			if (this.m_Results != null)
			{
				return this.m_Results.Count;
			}
			return 0;
		}

		// Token: 0x0600124C RID: 4684 RVA: 0x00060FA4 File Offset: 0x0005F3A4
		public Gacha.Result GetResultAt(int index)
		{
			if (this.m_Results != null && index >= 0 && index < this.m_Results.Count)
			{
				return this.m_Results[index];
			}
			return null;
		}

		// Token: 0x0600124D RID: 4685 RVA: 0x00060FD7 File Offset: 0x0005F3D7
		public List<Gacha.Result> GetResults()
		{
			return this.m_Results;
		}

		// Token: 0x0600124E RID: 4686 RVA: 0x00060FDF File Offset: 0x0005F3DF
		public List<Gacha.Result> GetGachaLog()
		{
			return this.m_Log;
		}

		// Token: 0x0600124F RID: 4687 RVA: 0x00060FE7 File Offset: 0x0005F3E7
		public List<int> GetBoxCharaIDs()
		{
			return this.m_BoxCharaIDs;
		}

		// Token: 0x06001250 RID: 4688 RVA: 0x00060FEF File Offset: 0x0005F3EF
		public List<int> GetBoxPickupCharaIDs()
		{
			return this.m_BoxPickupCharaIDs;
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06001251 RID: 4689 RVA: 0x00060FF8 File Offset: 0x0005F3F8
		// (remove) Token: 0x06001252 RID: 4690 RVA: 0x00061030 File Offset: 0x0005F430
		private event Action<MeigewwwParam, GachaResponseTypes.GetAll> m_OnResponse_GachaGetAll;

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06001253 RID: 4691 RVA: 0x00061068 File Offset: 0x0005F468
		// (remove) Token: 0x06001254 RID: 4692 RVA: 0x000610A0 File Offset: 0x0005F4A0
		private event Action<GachaDefine.eReturnGachaPlay, string> m_OnResponse_GachaPlay;

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06001255 RID: 4693 RVA: 0x000610D8 File Offset: 0x0005F4D8
		// (remove) Token: 0x06001256 RID: 4694 RVA: 0x00061110 File Offset: 0x0005F510
		private event Action<GachaDefine.eReturnGachaPlay, string> m_OnResponse_StepGachaPlay;

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06001257 RID: 4695 RVA: 0x00061148 File Offset: 0x0005F548
		// (remove) Token: 0x06001258 RID: 4696 RVA: 0x00061180 File Offset: 0x0005F580
		private event Action m_OnResponse_GachaLogGetLatest;

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06001259 RID: 4697 RVA: 0x000611B8 File Offset: 0x0005F5B8
		// (remove) Token: 0x0600125A RID: 4698 RVA: 0x000611F0 File Offset: 0x0005F5F0
		private event Action m_OnResponse_GachaBox;

		// Token: 0x0600125B RID: 4699 RVA: 0x00061228 File Offset: 0x0005F628
		public bool Request_GachaGetAll(Action<MeigewwwParam, GachaResponseTypes.GetAll> onResponse, bool isOpenConnectionIcon = true)
		{
			this.m_OnResponse_GachaGetAll = onResponse;
			GachaRequestTypes.GetAll getAll = new GachaRequestTypes.GetAll();
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay)
			{
				getAll.gachaIds = 1.ToString();
			}
			else
			{
				getAll.gachaIds = string.Empty;
			}
			MeigewwwParam all = GachaRequest.GetAll(getAll, new MeigewwwParam.Callback(this.OnResponse_GachaGetAll));
			NetworkQueueManager.Request(all);
			if (isOpenConnectionIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x0600125C RID: 4700 RVA: 0x000612B0 File Offset: 0x0005F6B0
		private void OnResponse_GachaGetAll(MeigewwwParam wwwParam)
		{
			GachaResponseTypes.GetAll all = GachaResponse.GetAll(wwwParam, ResponseCommon.DialogType.None, null);
			if (all == null)
			{
				if (this.m_OnResponse_GachaGetAll != null)
				{
					this.m_OnResponse_GachaGetAll(wwwParam, all);
				}
				return;
			}
			ResultCode result = all.GetResult();
			if (result == ResultCode.SUCCESS)
			{
				this.m_GachaList.Clear();
				for (int i = 0; i < all.gachas.Length; i++)
				{
					Gacha.GachaData gachaData = new Gacha.GachaData();
					GachaList gachaList = all.gachas[i];
					gachaData.m_ID = gachaList.gacha.id;
					gachaData.m_Name = gachaList.gacha.name;
					gachaData.m_BannerID = gachaList.gacha.bannerId;
					gachaData.m_Type = (eGachaType)gachaList.gacha.type;
					gachaData.m_WebViewUrl = gachaList.gacha.webViewUrl;
					gachaData.m_StartAt = gachaList.gacha.startAt;
					gachaData.m_EndAt = gachaList.gacha.endAt;
					gachaData.m_WonLimit = gachaList.gacha.box2Limit;
					gachaData.m_WonIsReset = (gachaList.gacha.box2Type == 1);
					gachaData.m_Cost_Gem1 = gachaList.gacha.gem1;
					gachaData.m_Cost_Gem10 = gachaList.gacha.gem10;
					gachaData.m_Cost_First10 = gachaList.gacha.first10;
					gachaData.m_Cost_UnlimitedGem1 = gachaList.gacha.unlimitedGem;
					gachaData.m_Cost_ItemID = gachaList.gacha.itemId;
					gachaData.m_Cost_ItemAmount = gachaList.gacha.itemAmount;
					gachaData.m_PlayNum_Gem1Total = gachaList.gem1Total;
					gachaData.m_PlayNum_Gem1Daily = gachaList.gem1Daily;
					gachaData.m_PlayNum_Gem10Total = gachaList.gem10Total;
					gachaData.m_PlayNum_Gem10Daily = gachaList.gem10Daily;
					gachaData.m_PlayNum_UnlimitedGem1Total = gachaList.uGem1Total;
					gachaData.m_PlayNum_UnlimitedGem1Daily = gachaList.uGem1Daily;
					gachaData.m_PlayNum_Item1Total = gachaList.itemTotal;
					gachaData.m_PlayNum_Item1Daily = gachaList.itemDaily;
					this.m_GachaList.Add(gachaData);
				}
				this.m_StepGachaList.Clear();
				for (int j = 0; j < all.gachaSteps.Length; j++)
				{
					Gacha.StepGachaData stepGachaData = new Gacha.StepGachaData();
					GachaStepList gachaStepList = all.gachaSteps[j];
					stepGachaData.m_ID = gachaStepList.gachaStep.id;
					stepGachaData.m_Name = gachaStepList.gachaStep.name;
					stepGachaData.m_BannerID = gachaStepList.gachaStep.bannerId;
					stepGachaData.m_WebViewUrl = gachaStepList.gachaStep.webViewUrl;
					stepGachaData.m_StartAt = gachaStepList.gachaStep.startAt;
					stepGachaData.m_EndAt = gachaStepList.gachaStep.endAt;
					stepGachaData.m_StepCurrent = gachaStepList.step;
					stepGachaData.m_StepMax = gachaStepList.gachaStep.steps;
					if (gachaStepList.gachaStep.reset == 1 && stepGachaData.m_StepCurrent == stepGachaData.m_StepMax)
					{
						stepGachaData.m_StepCurrent = 0;
					}
					stepGachaData.m_Steps = new Gacha.StepGachaData.StepPerOne[stepGachaData.m_StepMax];
					for (int k = 0; k < stepGachaData.m_StepMax; k++)
					{
						GachaOneStep gachaOneStep = gachaStepList.gachaStep.gachaSteps[k];
						stepGachaData.m_Steps[k] = new Gacha.StepGachaData.StepPerOne();
						stepGachaData.m_Steps[k].m_Cost_LimitedGem = gachaOneStep.limitedGem;
						stepGachaData.m_Steps[k].m_Cost_UnlimitedGem = gachaOneStep.unlimitedGem;
						stepGachaData.m_Steps[k].m_PlayNum = gachaOneStep.count;
					}
					this.m_StepGachaList.Add(stepGachaData);
				}
			}
			if (this.m_OnResponse_GachaGetAll != null)
			{
				this.m_OnResponse_GachaGetAll(wwwParam, all);
			}
		}

		// Token: 0x0600125D RID: 4701 RVA: 0x0006166C File Offset: 0x0005FA6C
		public bool Request_GachaPlay(int gachaID, GachaDefine.ePlayType playType, Action<GachaDefine.eReturnGachaPlay, string> onResponse)
		{
			this.m_OnResponse_GachaPlay = onResponse;
			this.m_PlayedGachaID = gachaID;
			this.m_PlayedType = playType;
			GachaRequestTypes.Draw draw = new GachaRequestTypes.Draw();
			draw.gachaId = gachaID;
			draw.drawType = (int)playType;
			draw.reDraw = false;
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay)
			{
				draw.stepCode = 4;
			}
			if (tutoarialSeq == eTutorialSeq.GachaPlayed_NextIsGachaDecide)
			{
				draw.reDraw = true;
			}
			MeigewwwParam wwwParam = GachaRequest.Draw(draw, new MeigewwwParam.Callback(this.OnResponse_GachaPlay));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x0600125E RID: 4702 RVA: 0x00061700 File Offset: 0x0005FB00
		public bool Request_GachaRePlay(Action<GachaDefine.eReturnGachaPlay, string> onResponse)
		{
			eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
			if (tutoarialSeq == eTutorialSeq.GachaPlayed_NextIsGachaDecide)
			{
				this.m_PlayedGachaID = 1;
				this.m_PlayedType = GachaDefine.ePlayType.Gem10;
			}
			return this.Request_GachaPlay(this.m_PlayedGachaID, this.m_PlayedType, onResponse);
		}

		// Token: 0x0600125F RID: 4703 RVA: 0x00061748 File Offset: 0x0005FB48
		private void OnResponse_GachaPlay(MeigewwwParam wwwParam)
		{
			GachaResponseTypes.Draw draw = GachaResponse.Draw(wwwParam, ResponseCommon.DialogType.None, null);
			if (draw == null)
			{
				return;
			}
			ResultCode result = draw.GetResult();
			switch (result)
			{
			case ResultCode.ITEM_IS_SHORT:
				this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.ItemIsShort, result.ToMessageString());
				break;
			case ResultCode.UNLIMITED_GEM_IS_SHORT:
				this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.UnlimitedGemIsShort, result.ToMessageString());
				break;
			default:
				if (result != ResultCode.SUCCESS)
				{
					if (result != ResultCode.GACHA_DRAW_LIMIT)
					{
						if (result != ResultCode.GACHA_STEP_DRAW_LIMIT)
						{
							if (result != ResultCode.GACHA_OUT_OF_PERIOD)
							{
								APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
							}
							else
							{
								this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaOutOfPeriod, result.ToMessageString());
							}
						}
						else
						{
							this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaStepDrawLimit, result.ToMessageString());
						}
					}
					else
					{
						this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaDrawLimit, result.ToMessageString());
					}
				}
				else
				{
					this.m_Results.Clear();
					for (int i = 0; i < draw.gachaResults.Length; i++)
					{
						Gacha.Result item = new Gacha.Result(draw.gachaResults[i].characterId, draw.gachaResults[i].itemId, draw.gachaResults[i].itemAmount);
						this.m_Results.Add(item);
					}
					UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
					APIUtility.wwwToUserData(draw.player, userDataMng.UserData);
					APIUtility.wwwToUserData(draw.itemSummary, userDataMng.UserItemDatas);
					APIUtility.wwwToUserData(draw.managedCharacters, userDataMng.UserCharaDatas);
					APIUtility.wwwToUserData(draw.managedNamedTypes, userDataMng.UserNamedDatas);
					eTutorialSeq tutoarialSeq = SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.GetTutoarialSeq();
					if (tutoarialSeq == eTutorialSeq.ADVGachaPrevPlayed_NextIsGachaPlay)
					{
						SingletonMonoBehaviour<GameSystem>.Inst.TutorialMng.ApplyNextTutorialSeq();
					}
					this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.Success, null);
				}
				break;
			case ResultCode.GEM_IS_SHORT:
				this.m_OnResponse_GachaPlay.Call(GachaDefine.eReturnGachaPlay.GemIsShort, result.ToMessageString());
				break;
			}
		}

		// Token: 0x06001260 RID: 4704 RVA: 0x0006193C File Offset: 0x0005FD3C
		public bool Request_StepGachaPlay(int gachaID, Action<GachaDefine.eReturnGachaPlay, string> onResponse)
		{
			this.m_OnResponse_StepGachaPlay = onResponse;
			this.m_PlayedGachaID = gachaID;
			this.m_PlayedType = GachaDefine.ePlayType.Gem1;
			MeigewwwParam wwwParam = GachaRequest.Stepdraw(new GachaRequestTypes.Stepdraw
			{
				gachaId = gachaID
			}, new MeigewwwParam.Callback(this.OnResponse_StepGachaPlay));
			NetworkQueueManager.Request(wwwParam);
			SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			return true;
		}

		// Token: 0x06001261 RID: 4705 RVA: 0x00061994 File Offset: 0x0005FD94
		private void OnResponse_StepGachaPlay(MeigewwwParam wwwParam)
		{
			GachaResponseTypes.Stepdraw stepdraw = GachaResponse.Stepdraw(wwwParam, ResponseCommon.DialogType.None, null);
			if (stepdraw == null)
			{
				return;
			}
			ResultCode result = stepdraw.GetResult();
			switch (result)
			{
			case ResultCode.ITEM_IS_SHORT:
				this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.ItemIsShort, result.ToMessageString());
				break;
			case ResultCode.UNLIMITED_GEM_IS_SHORT:
				this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.UnlimitedGemIsShort, result.ToMessageString());
				break;
			default:
				if (result != ResultCode.SUCCESS)
				{
					if (result != ResultCode.GACHA_DRAW_LIMIT)
					{
						if (result != ResultCode.GACHA_STEP_DRAW_LIMIT)
						{
							if (result != ResultCode.GACHA_OUT_OF_PERIOD)
							{
								APIUtility.ShowErrorWindowRetry(wwwParam, result.ToMessageString());
							}
							else
							{
								this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaOutOfPeriod, result.ToMessageString());
							}
						}
						else
						{
							this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaStepDrawLimit, result.ToMessageString());
						}
					}
					else
					{
						this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.GachaDrawLimit, result.ToMessageString());
					}
				}
				else
				{
					this.m_Results.Clear();
					for (int i = 0; i < stepdraw.gachaResults.Length; i++)
					{
						Gacha.Result item = new Gacha.Result(stepdraw.gachaResults[i].characterId, stepdraw.gachaResults[i].itemId, stepdraw.gachaResults[i].itemAmount);
						this.m_Results.Add(item);
					}
					UserDataManager userDataMng = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng;
					APIUtility.wwwToUserData(stepdraw.player, userDataMng.UserData);
					APIUtility.wwwToUserData(stepdraw.itemSummary, userDataMng.UserItemDatas);
					APIUtility.wwwToUserData(stepdraw.managedCharacters, userDataMng.UserCharaDatas);
					APIUtility.wwwToUserData(stepdraw.managedNamedTypes, userDataMng.UserNamedDatas);
					this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.Success, null);
				}
				break;
			case ResultCode.GEM_IS_SHORT:
				this.m_OnResponse_StepGachaPlay.Call(GachaDefine.eReturnGachaPlay.GemIsShort, result.ToMessageString());
				break;
			}
		}

		// Token: 0x06001262 RID: 4706 RVA: 0x00061B60 File Offset: 0x0005FF60
		public bool Request_GachaLogGetLatest(Action onResponse, bool isOpenConnectionIcon = true)
		{
			this.m_OnResponse_GachaLogGetLatest = onResponse;
			GachaLogRequestTypes.GetLatest param = new GachaLogRequestTypes.GetLatest();
			MeigewwwParam latest = GachaLogRequest.GetLatest(param, new MeigewwwParam.Callback(this.OnResponse_GachaLogGetLatest));
			NetworkQueueManager.Request(latest);
			if (isOpenConnectionIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001263 RID: 4707 RVA: 0x00061BAC File Offset: 0x0005FFAC
		private void OnResponse_GachaLogGetLatest(MeigewwwParam wwwParam)
		{
			GachaLogResponseTypes.GetLatest latest = GachaLogResponse.GetLatest(wwwParam, ResponseCommon.DialogType.None, null);
			if (latest == null)
			{
				return;
			}
			ResultCode result = latest.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToString());
			}
			else
			{
				this.m_Log.Clear();
				for (int i = 0; i < latest.gachaResults.Length; i++)
				{
					Gacha.Result item = new Gacha.Result(latest.gachaResults[i].characterId, latest.gachaResults[i].itemId, latest.gachaResults[i].itemAmount);
					this.m_Log.Add(item);
				}
				this.m_OnResponse_GachaLogGetLatest.Call();
			}
		}

		// Token: 0x06001264 RID: 4708 RVA: 0x00061C60 File Offset: 0x00060060
		public bool Request_GachaBox(int gachaID, Action onResponse, bool isOpenConnectionIcon = true)
		{
			this.m_OnResponse_GachaBox = onResponse;
			MeigewwwParam box = GachaRequest.GetBox(new GachaRequestTypes.GetBox
			{
				gachaId = gachaID
			}, new MeigewwwParam.Callback(this.OnResponse_GachaBox));
			NetworkQueueManager.Request(box);
			if (isOpenConnectionIcon)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.ConnectingIcon.Open();
			}
			return true;
		}

		// Token: 0x06001265 RID: 4709 RVA: 0x00061CB0 File Offset: 0x000600B0
		private void OnResponse_GachaBox(MeigewwwParam wwwParam)
		{
			GachaResponseTypes.GetBox box = GachaResponse.GetBox(wwwParam, ResponseCommon.DialogType.None, null);
			if (box == null)
			{
				return;
			}
			ResultCode result = box.GetResult();
			if (result != ResultCode.SUCCESS)
			{
				APIUtility.ShowErrorWindowRetry(wwwParam, result.ToString());
			}
			else
			{
				this.m_BoxCharaIDs.Clear();
				if (box.characters != null)
				{
					for (int i = 0; i < box.characters.Length; i++)
					{
						this.m_BoxCharaIDs.Add(box.characters[i]);
					}
				}
				this.m_BoxPickupCharaIDs.Clear();
				if (box.characters != null)
				{
					for (int j = 0; j < box.pickups.Length; j++)
					{
						this.m_BoxPickupCharaIDs.Add(box.pickups[j]);
					}
				}
				List<int> boxCharaIDs = this.m_BoxCharaIDs;
				if (Gacha.<>f__mg$cache0 == null)
				{
					Gacha.<>f__mg$cache0 = new Comparison<int>(Gacha.SortComp);
				}
				boxCharaIDs.Sort(Gacha.<>f__mg$cache0);
				List<int> boxPickupCharaIDs = this.m_BoxPickupCharaIDs;
				if (Gacha.<>f__mg$cache1 == null)
				{
					Gacha.<>f__mg$cache1 = new Comparison<int>(Gacha.SortComp);
				}
				boxPickupCharaIDs.Sort(Gacha.<>f__mg$cache1);
				this.m_OnResponse_GachaBox.Call();
			}
		}

		// Token: 0x06001266 RID: 4710 RVA: 0x00061DDC File Offset: 0x000601DC
		private static int SortComp(int x, int y)
		{
			CharacterListDB_Param param = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(x);
			CharacterListDB_Param param2 = SingletonMonoBehaviour<GameSystem>.Inst.DbMng.CharaListDB.GetParam(y);
			if (param.m_Rare > param2.m_Rare)
			{
				return -1;
			}
			if (param.m_Rare < param2.m_Rare)
			{
				return 1;
			}
			if (x < y)
			{
				return -1;
			}
			if (x > y)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x040018BD RID: 6333
		private List<Gacha.GachaData> m_GachaList = new List<Gacha.GachaData>();

		// Token: 0x040018BE RID: 6334
		private List<Gacha.StepGachaData> m_StepGachaList = new List<Gacha.StepGachaData>();

		// Token: 0x040018BF RID: 6335
		private List<Gacha.Result> m_Results = new List<Gacha.Result>();

		// Token: 0x040018C0 RID: 6336
		private List<Gacha.Result> m_Log = new List<Gacha.Result>();

		// Token: 0x040018C1 RID: 6337
		private List<int> m_BoxCharaIDs = new List<int>();

		// Token: 0x040018C2 RID: 6338
		private List<int> m_BoxPickupCharaIDs = new List<int>();

		// Token: 0x040018C8 RID: 6344
		private int m_PlayedGachaID = -1;

		// Token: 0x040018C9 RID: 6345
		private GachaDefine.ePlayType m_PlayedType = GachaDefine.ePlayType.Gem1;

		// Token: 0x040018CA RID: 6346
		[CompilerGenerated]
		private static Comparison<int> <>f__mg$cache0;

		// Token: 0x040018CB RID: 6347
		[CompilerGenerated]
		private static Comparison<int> <>f__mg$cache1;

		// Token: 0x020003C2 RID: 962
		public class GachaDataBase
		{
			// Token: 0x040018CC RID: 6348
			public int m_ID;

			// Token: 0x040018CD RID: 6349
			public string m_Name;

			// Token: 0x040018CE RID: 6350
			public string m_BannerID;

			// Token: 0x040018CF RID: 6351
			public string m_WebViewUrl;

			// Token: 0x040018D0 RID: 6352
			public DateTime m_StartAt;

			// Token: 0x040018D1 RID: 6353
			public DateTime m_EndAt;
		}

		// Token: 0x020003C3 RID: 963
		public class GachaData : Gacha.GachaDataBase
		{
			// Token: 0x040018D2 RID: 6354
			public eGachaType m_Type;

			// Token: 0x040018D3 RID: 6355
			public int m_WonLimit;

			// Token: 0x040018D4 RID: 6356
			public bool m_WonIsReset;

			// Token: 0x040018D5 RID: 6357
			public int m_Cost_Gem1;

			// Token: 0x040018D6 RID: 6358
			public int m_Cost_Gem10;

			// Token: 0x040018D7 RID: 6359
			public int m_Cost_First10;

			// Token: 0x040018D8 RID: 6360
			public int m_Cost_UnlimitedGem1;

			// Token: 0x040018D9 RID: 6361
			public int m_Cost_ItemID;

			// Token: 0x040018DA RID: 6362
			public int m_Cost_ItemAmount;

			// Token: 0x040018DB RID: 6363
			public int m_PlayNum_Gem1Total;

			// Token: 0x040018DC RID: 6364
			public int m_PlayNum_Gem1Daily;

			// Token: 0x040018DD RID: 6365
			public int m_PlayNum_Gem10Total;

			// Token: 0x040018DE RID: 6366
			public int m_PlayNum_Gem10Daily;

			// Token: 0x040018DF RID: 6367
			public int m_PlayNum_UnlimitedGem1Total;

			// Token: 0x040018E0 RID: 6368
			public int m_PlayNum_UnlimitedGem1Daily;

			// Token: 0x040018E1 RID: 6369
			public int m_PlayNum_Item1Total;

			// Token: 0x040018E2 RID: 6370
			public int m_PlayNum_Item1Daily;
		}

		// Token: 0x020003C4 RID: 964
		public class StepGachaData : Gacha.GachaDataBase
		{
			// Token: 0x040018E3 RID: 6371
			public int m_StepCurrent;

			// Token: 0x040018E4 RID: 6372
			public int m_StepMax;

			// Token: 0x040018E5 RID: 6373
			public Gacha.StepGachaData.StepPerOne[] m_Steps;

			// Token: 0x020003C5 RID: 965
			public class StepPerOne
			{
				// Token: 0x040018E6 RID: 6374
				public int m_Cost_LimitedGem;

				// Token: 0x040018E7 RID: 6375
				public int m_Cost_UnlimitedGem;

				// Token: 0x040018E8 RID: 6376
				public int m_PlayNum;
			}
		}

		// Token: 0x020003C6 RID: 966
		public class Result
		{
			// Token: 0x0600126B RID: 4715 RVA: 0x00061E72 File Offset: 0x00060272
			public Result(int charaID, int itemID, int itemNum)
			{
				this.m_CharaID = charaID;
				this.m_ItemID = itemID;
				this.m_ItemNum = itemNum;
			}

			// Token: 0x040018E9 RID: 6377
			public int m_CharaID = -1;

			// Token: 0x040018EA RID: 6378
			public int m_ItemID = -1;

			// Token: 0x040018EB RID: 6379
			public int m_ItemNum;
		}
	}
}
