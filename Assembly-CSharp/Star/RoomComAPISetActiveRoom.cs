﻿using System;
using RoomResponseTypes;

namespace Star
{
	// Token: 0x0200058F RID: 1423
	public class RoomComAPISetActiveRoom : INetComHandle
	{
		// Token: 0x06001BAD RID: 7085 RVA: 0x00092985 File Offset: 0x00090D85
		public RoomComAPISetActiveRoom()
		{
			this.ApiName = "player/room/active/set";
			this.Request = true;
			this.ResponseType = typeof(Remove);
		}

		// Token: 0x04002293 RID: 8851
		public long managedRoomId;
	}
}
