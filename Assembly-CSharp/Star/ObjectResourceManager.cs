﻿using System;
using System.Collections;
using System.Collections.Generic;
using Meige;
using Meige.AssetBundles;
using UnityEngine;

namespace Star
{
	// Token: 0x02000373 RID: 883
	public class ObjectResourceManager : MonoBehaviour
	{
		// Token: 0x060010B5 RID: 4277 RVA: 0x00057D44 File Offset: 0x00056144
		public static MeigeResource.Handler CreateHandle(string pfilename)
		{
			string text = pfilename + ".muast";
			MeigeResource.Handler handler = ObjectResourceManager.Inst.ChkReshandleRefCnt(text);
			if (handler == null)
			{
				handler = MeigeResourceManager.LoadHandler(text, new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
				ObjectResourceManager.Inst.AddReshandleRefCnt(handler, text, 1);
			}
			return handler;
		}

		// Token: 0x060010B6 RID: 4278 RVA: 0x00057D90 File Offset: 0x00056190
		public static MeigeResource.Handler CreateHandle(string pfilename, Action<MeigeResource.Handler> pcallback)
		{
			string text = pfilename + ".muast";
			MeigeResource.Handler handler = ObjectResourceManager.Inst.ChkReshandleRefCnt(text);
			if (handler == null)
			{
				handler = MeigeResourceManager.LoadHandler(text, pcallback, new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
				ObjectResourceManager.Inst.AddReshandleRefCnt(handler, text, 1);
			}
			else
			{
				pcallback(handler);
			}
			return handler;
		}

		// Token: 0x060010B7 RID: 4279 RVA: 0x00057DE8 File Offset: 0x000561E8
		public static void PreloadHandle(string pfilename)
		{
			string text = pfilename + ".muast";
			if (ObjectResourceManager.Inst.ChkReshandleRefCnt(text) == null)
			{
				MeigeResource.Handler phandle = MeigeResourceManager.LoadHandler(text, new MeigeResource.Option[]
				{
					MeigeResource.Option.DownloadOnly
				});
				ObjectResourceManager.Inst.AddReshandleRefCnt(phandle, text, 0);
			}
		}

		// Token: 0x060010B8 RID: 4280 RVA: 0x00057E31 File Offset: 0x00056231
		public static void RetryHandle(MeigeResource.Handler phandle)
		{
			MeigeResourceManager.ClearError();
			MeigeResourceManager.RetryHandler(phandle);
		}

		// Token: 0x060010B9 RID: 4281 RVA: 0x00057E3E File Offset: 0x0005623E
		public static void ReleaseHandle(MeigeResource.Handler phandle)
		{
			ObjectResourceManager.Inst.CheckReshandleRefCnt(phandle);
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x00057E4C File Offset: 0x0005624C
		public static void ObjectDestroy(GameObject pobj, bool funloader = false)
		{
			UnityEngine.Object.Destroy(pobj);
			if (funloader && ObjectResourceManager.Inst != null)
			{
				ObjectResourceManager.Inst.m_ResourceRefresh++;
			}
		}

		// Token: 0x060010BB RID: 4283 RVA: 0x00057E7C File Offset: 0x0005627C
		private MeigeResource.Handler ChkReshandleRefCnt(string presname)
		{
			int count = this.m_MeigeHandleTable.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MeigeHandleTable[i].m_Name == presname)
				{
					this.m_MeigeHandleTable[i].m_RefCnt++;
					return this.m_MeigeHandleTable[i].m_Handle;
				}
			}
			return null;
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x00057EF0 File Offset: 0x000562F0
		private void AddReshandleRefCnt(MeigeResource.Handler phandle, string fchkname, int fstep)
		{
			bool flag = false;
			int count = this.m_MeigeHandleTable.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MeigeHandleTable[i].m_Name == fchkname)
				{
					flag = true;
					break;
				}
			}
			if (!flag)
			{
				ObjectResourceManager.MeigeHandleRefState meigeHandleRefState = new ObjectResourceManager.MeigeHandleRefState();
				meigeHandleRefState.m_RefCnt = 1;
				meigeHandleRefState.m_Handle = phandle;
				meigeHandleRefState.m_Name = fchkname;
				this.m_MeigeHandleTable.Add(meigeHandleRefState);
			}
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x00057F70 File Offset: 0x00056370
		private bool CheckReshandleRefCnt(MeigeResource.Handler phandle)
		{
			bool result = false;
			bool flag = false;
			int count = this.m_MeigeHandleTable.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_MeigeHandleTable[i].m_Handle.resourceName == phandle.resourceName)
				{
					flag = true;
					this.m_MeigeHandleTable[i].m_RefCnt--;
					if (this.m_MeigeHandleTable[i].m_RefCnt <= 0)
					{
						this.m_MeigeHandleTable[i].m_Handle = null;
						MeigeResourceManager.UnloadHandler(phandle, false);
						this.m_MeigeHandleTable.RemoveAt(i);
						result = true;
					}
					break;
				}
			}
			if (!flag && this.m_ResMngTable != null)
			{
				for (int i = 0; i < this.m_ResMngMax; i++)
				{
					if (this.m_ResMngTable[i] != null && this.m_ResMngTable[i].m_Handle == phandle)
					{
						MeigeResourceManager.UnloadHandler(this.m_ResMngTable[i].m_Handle, false);
						this.m_ResMngTable[i] = null;
						result = true;
						break;
					}
				}
			}
			return result;
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x0005808F File Offset: 0x0005648F
		public static bool IsLoading()
		{
			return ObjectResourceManager.Inst.m_WaitResNum != 0 || ObjectResourceManager.Inst.m_WaitResMngNum != 0;
		}

		// Token: 0x060010BF RID: 4287 RVA: 0x000580B3 File Offset: 0x000564B3
		public static void SetUpManager(GameObject ptarget)
		{
			if (SingletonMonoBehaviour<MeigeResourceManager>.Instance == null)
			{
				ptarget.AddComponent<AssetBundleManager>();
				ptarget.AddComponent<MeigeResourceManager>();
			}
			ptarget.AddComponent<ObjectResourceManager>();
		}

		// Token: 0x060010C0 RID: 4288 RVA: 0x000580DA File Offset: 0x000564DA
		public static void ReleaseCheck()
		{
			if (SingletonMonoBehaviour<MeigeResourceManager>.Instance != null && ObjectResourceManager.Inst != null)
			{
				ObjectResourceManager.Inst.AllResourceRelease();
			}
			ObjectResourceManager.Inst = null;
		}

		// Token: 0x060010C1 RID: 4289 RVA: 0x0005810C File Offset: 0x0005650C
		private void Awake()
		{
			ObjectResourceManager.Inst = this;
			this.m_MeigeHandleTable = new List<ObjectResourceManager.MeigeHandleRefState>();
			this.m_ResTable = new IObjectResourceHandle[10];
			this.m_ResMax = 10;
			this.m_WaitResTable = new int[5];
			this.m_WaitResMax = 5;
			this.m_WaitResNum = 0;
			this.m_ResMngTable = new IObjectResourceHandle[5];
			this.m_ResMngMax = 5;
			this.m_WaitResMngTable = new int[5];
			this.m_WaitResMngMax = 5;
			this.m_WaitResMngNum = 0;
		}

		// Token: 0x060010C2 RID: 4290 RVA: 0x00058188 File Offset: 0x00056588
		public void Update()
		{
			if (this.m_ResUp != 0 && this.m_ResUp >= 2)
			{
				int num = this.m_WaitResTable[0];
				this.m_ResTable[num].SetUpResource(this.m_ResTable[num].Obj);
				this.m_ResUp = 0;
				for (int i = 1; i < this.m_WaitResNum; i++)
				{
					this.m_WaitResTable[i - 1] = this.m_WaitResTable[i];
				}
				this.m_WaitResNum--;
				if (this.m_WaitResNum != 0)
				{
					base.StartCoroutine(this.ResourceLoader(this.m_ResTable[this.m_WaitResTable[0]]));
				}
			}
			if (this.m_WaitResMngNum != 0)
			{
				int num = this.m_WaitResMngTable[0];
				if (this.m_ResMngTable[num].m_Handle.IsDone)
				{
					if (this.m_ResMngTable[num].m_Handle.IsError)
					{
						MeigeResourceManager.ClearError();
						MeigeResourceManager.RetryHandler(this.m_ResMngTable[num].m_Handle);
					}
					else
					{
						this.m_ResMngTable[num].Obj = this.m_ResMngTable[num].m_Handle.GetAsset<UnityEngine.Object>();
						this.m_ResMngTable[num].SetUpResource(this.m_ResMngTable[num].Obj);
						this.m_ResMngTable[num].m_ReadWait = false;
						this.m_ResMngTable[num].m_Active = true;
						for (int i = 1; i < this.m_WaitResMngNum; i++)
						{
							this.m_WaitResMngTable[i - 1] = this.m_WaitResMngTable[i];
						}
						this.m_WaitResMngNum--;
					}
				}
			}
			if (this.m_ResourceRefresh != 0 && !this.m_UncashPlay)
			{
				this.m_ResourceRefresh = 0;
				this.m_UncashPlay = true;
				base.StartCoroutine(this.UnloadObjectCash());
			}
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x00058350 File Offset: 0x00056750
		private IEnumerator UnloadObjectCash()
		{
			yield return Resources.UnloadUnusedAssets();
			this.m_UncashPlay = false;
			yield break;
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x0005836C File Offset: 0x0005676C
		private void AllResourceRelease()
		{
			if (this.m_ResMngTable != null)
			{
				for (int i = 0; i < this.m_ResMngMax; i++)
				{
					if (this.m_ResMngTable[i] != null && this.m_ResMngTable[i].m_Handle != null)
					{
						MeigeResourceManager.UnloadHandler(this.m_ResMngTable[i].m_Handle, false);
						this.m_ResMngTable[i].m_Handle = null;
					}
				}
				this.m_ResMngTable = null;
				this.m_ResMngMax = 0;
			}
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x000583E9 File Offset: 0x000567E9
		private void OnDestroy()
		{
			if (SingletonMonoBehaviour<MeigeResourceManager>.Instance != null)
			{
				this.AllResourceRelease();
			}
			this.m_ResMngTable = null;
			ObjectResourceManager.Inst = null;
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x00058410 File Offset: 0x00056810
		public static void EntryResMngQue(IObjectResourceHandle pque, string pfilename, string pext)
		{
			uint uid = CRC32.Calc(pfilename + pext);
			pque.m_UID = uid;
			int fresno = ObjectResourceManager.Inst.EntryResManager(pque, pfilename, pext);
			ObjectResourceManager.Inst.LoadQueMngCheck(fresno, true);
		}

		// Token: 0x060010C7 RID: 4295 RVA: 0x0005844C File Offset: 0x0005684C
		public static void EntryResQue(IObjectResourceHandle pque, string pfilename, string pext)
		{
			uint uid = CRC32.Calc(pfilename + pext);
			pque.m_UID = uid;
			int fresno = ObjectResourceManager.Inst.EntryResource(pque, pfilename, pext, false);
			ObjectResourceManager.Inst.LoadQueCheck(fresno);
		}

		// Token: 0x060010C8 RID: 4296 RVA: 0x00058488 File Offset: 0x00056888
		private int EntryResource(IObjectResourceHandle phandle, string filepath, string fileext, bool fweb)
		{
			int num = -1;
			for (int i = 0; i < this.m_ResMax; i++)
			{
				if (this.m_ResTable[i] == null)
				{
					num = i;
					break;
				}
			}
			if (num < 0)
			{
				IObjectResourceHandle[] array = new IObjectResourceHandle[this.m_ResMax + 5];
				for (int i = 0; i < this.m_ResMax; i++)
				{
					array[i] = this.m_ResTable[i];
				}
				this.m_ResTable = null;
				this.m_ResTable = array;
				num = this.m_ResMax;
				this.m_ResMax += 5;
			}
			this.m_ResTable[num] = phandle;
			this.m_ResTable[num].m_FileName = filepath.ToLower();
			this.m_ResTable[num].m_FileExt = fileext;
			this.m_ResTable[num].m_Web = fweb;
			return num;
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x00058558 File Offset: 0x00056958
		private void LoadQueCheck(int fresno)
		{
			if (!this.m_ResTable[fresno].m_Active && !this.m_ResTable[fresno].m_ReadWait)
			{
				if (this.m_ResUp == 0)
				{
					this.m_ResUp = 1;
					this.m_ResTable[fresno].m_ReadWait = true;
					this.m_WaitResTable[this.m_WaitResNum] = fresno;
					this.m_WaitResNum++;
					base.StartCoroutine(this.ResourceLoader(this.m_ResTable[fresno]));
				}
				else
				{
					if (this.m_WaitResNum >= this.m_WaitResMax)
					{
						int[] array = new int[this.m_WaitResMax + 5];
						for (int i = 0; i < this.m_WaitResMax; i++)
						{
							array[i] = this.m_WaitResTable[i];
						}
						this.m_WaitResTable = null;
						this.m_WaitResTable = array;
						this.m_WaitResMax += 5;
					}
					this.m_WaitResTable[this.m_WaitResNum] = fresno;
					this.m_WaitResNum++;
				}
			}
		}

		// Token: 0x060010CA RID: 4298 RVA: 0x00058658 File Offset: 0x00056A58
		private int EntryResManager(IObjectResourceHandle phandle, string filepath, string fileext)
		{
			int num = -1;
			for (int i = 0; i < this.m_ResMngMax; i++)
			{
				if (this.m_ResMngTable[i] == null)
				{
					num = i;
					break;
				}
			}
			if (num < 0)
			{
				IObjectResourceHandle[] array = new IObjectResourceHandle[this.m_ResMngMax + 5];
				for (int i = 0; i < this.m_ResMngMax; i++)
				{
					array[i] = this.m_ResMngTable[i];
				}
				this.m_ResMngTable = null;
				this.m_ResMngTable = array;
				num = this.m_ResMngMax;
				this.m_ResMngMax += 5;
			}
			this.m_ResMngTable[num] = phandle;
			this.m_ResMngTable[num].m_FileName = filepath;
			this.m_ResMngTable[num].m_FileExt = fileext;
			this.m_ResMngTable[num].m_Web = true;
			this.m_ResMngTable[num].m_Handle = MeigeResourceManager.LoadHandler(filepath + ".muast", new MeigeResource.Option[]
			{
				MeigeResource.Option.ResourceKeep
			});
			return num;
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x00058748 File Offset: 0x00056B48
		private void LoadQueMngCheck(int fresno, bool fwww = false)
		{
			if (this.m_WaitResMngNum >= this.m_WaitResMngMax)
			{
				int[] array = new int[this.m_WaitResMngMax + 5];
				for (int i = 0; i < this.m_WaitResMngMax; i++)
				{
					array[i] = this.m_WaitResMngTable[i];
				}
				this.m_WaitResMngTable = null;
				this.m_WaitResMngTable = array;
				this.m_WaitResMngMax += 5;
			}
			this.m_WaitResMngTable[this.m_WaitResMngNum] = fresno;
			this.m_WaitResMngNum++;
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x000587D0 File Offset: 0x00056BD0
		private void ReleaseRes(IObjectResourceHandle phandle)
		{
			int mappingNo = phandle.m_MappingNo;
			if (this.m_ResTable[mappingNo] != null)
			{
				this.m_ResTable[mappingNo].m_UseNum--;
				if (this.m_ResTable[mappingNo].m_UseNum <= 0)
				{
					this.m_ResTable[mappingNo].Release();
					Resources.UnloadUnusedAssets();
				}
			}
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x0005882C File Offset: 0x00056C2C
		public static string GetPlatformAssetPath()
		{
			string text;
			if (SingletonMonoBehaviour<AssetBundleManager>.Instance == null)
			{
				text = "http://star-dev-asset.s3-website-ap-northeast-1.amazonaws.com/sample/";
			}
			else
			{
				text = AssetBundleManager.BaseDownloadingURL;
				text += Utility.GetPlatformName();
			}
			return text;
		}

		// Token: 0x060010CE RID: 4302 RVA: 0x00058868 File Offset: 0x00056C68
		public static string GetPlatformExt()
		{
			return ".unity3d";
		}

		// Token: 0x060010CF RID: 4303 RVA: 0x0005887C File Offset: 0x00056C7C
		private IEnumerator ResourceLoader(IObjectResourceHandle pstate)
		{
			ResourceRequest resReq = Resources.LoadAsync(pstate.m_FileName);
			while (!resReq.isDone)
			{
				yield return 0;
			}
			pstate.Obj = resReq.asset;
			pstate.m_ReadWait = false;
			pstate.m_Active = true;
			this.m_ResUp = 2;
			yield break;
		}

		// Token: 0x060010D0 RID: 4304 RVA: 0x000588A0 File Offset: 0x00056CA0
		private IEnumerator ResABWWWLoader(IObjectResourceHandle pstate)
		{
			WWW fhandle = null;
			string fpath = ObjectResourceManager.GetPlatformAssetPath();
			string fext = ObjectResourceManager.GetPlatformExt();
			using (fhandle = WWW.LoadFromCacheOrDownload(fpath + pstate.m_FileName + fext, 0))
			{
				yield return fhandle;
				if (string.IsNullOrEmpty(fhandle.error))
				{
					AssetBundle fasset = fhandle.assetBundle;
					AssetBundleRequest prequest = fasset.LoadAllAssetsAsync(typeof(UnityEngine.Object));
					yield return prequest;
					pstate.Obj = prequest.asset;
					pstate.m_ReadWait = false;
					pstate.m_Active = true;
					fasset.Unload(false);
				}
				fhandle.Dispose();
			}
			this.m_ResUp = 2;
			yield break;
		}

		// Token: 0x060010D1 RID: 4305 RVA: 0x000588C4 File Offset: 0x00056CC4
		private IEnumerator ResourceWWWLoader(IObjectResourceHandle pstate)
		{
			WWW fhandle = null;
			string fpath = ObjectResourceManager.GetPlatformAssetPath();
			using (fhandle = new WWW(fpath + pstate.m_FileName + pstate.m_FileExt))
			{
				yield return fhandle;
				if (string.IsNullOrEmpty(fhandle.error))
				{
					AssetBundleCreateRequest pcreatereq = AssetBundle.LoadFromMemoryAsync(fhandle.bytes);
					while (!pcreatereq.isDone)
					{
						yield return 0;
					}
					AssetBundle fasset = pcreatereq.assetBundle;
					AssetBundleRequest prequest = fasset.LoadAllAssetsAsync(typeof(UnityEngine.Object));
					yield return prequest;
					pstate.Obj = prequest.asset;
					pstate.m_ReadWait = false;
					pstate.m_Active = true;
					fasset.Unload(false);
				}
				fhandle.Dispose();
			}
			this.m_ResUp = 2;
			yield break;
		}

		// Token: 0x040017AD RID: 6061
		private static ObjectResourceManager Inst;

		// Token: 0x040017AE RID: 6062
		private IObjectResourceHandle[] m_ResTable;

		// Token: 0x040017AF RID: 6063
		private int m_ResMax;

		// Token: 0x040017B0 RID: 6064
		private int[] m_WaitResTable;

		// Token: 0x040017B1 RID: 6065
		private int m_WaitResNum;

		// Token: 0x040017B2 RID: 6066
		private int m_WaitResMax;

		// Token: 0x040017B3 RID: 6067
		private byte m_ResUp;

		// Token: 0x040017B4 RID: 6068
		private IObjectResourceHandle[] m_ResMngTable;

		// Token: 0x040017B5 RID: 6069
		private int m_ResMngMax;

		// Token: 0x040017B6 RID: 6070
		private int[] m_WaitResMngTable;

		// Token: 0x040017B7 RID: 6071
		private int m_WaitResMngNum;

		// Token: 0x040017B8 RID: 6072
		private int m_WaitResMngMax;

		// Token: 0x040017B9 RID: 6073
		private int m_ResourceRefresh;

		// Token: 0x040017BA RID: 6074
		private bool m_UncashPlay;

		// Token: 0x040017BB RID: 6075
		private List<ObjectResourceManager.MeigeHandleRefState> m_MeigeHandleTable;

		// Token: 0x02000374 RID: 884
		public class MeigeHandleRefState
		{
			// Token: 0x040017BC RID: 6076
			public int m_RefCnt;

			// Token: 0x040017BD RID: 6077
			public int m_Step;

			// Token: 0x040017BE RID: 6078
			public string m_Name;

			// Token: 0x040017BF RID: 6079
			public MeigeResource.Handler m_Handle;
		}
	}
}
