﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020002F5 RID: 757
	public class TrailHandler : EffectHandlerBase
	{
		// Token: 0x06000ED0 RID: 3792 RVA: 0x0004FADC File Offset: 0x0004DEDC
		public override void OnDestroyToCache()
		{
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x0004FAE0 File Offset: 0x0004DEE0
		public void Setup(Color[] colors, float startWidth, float endWidth, float time)
		{
			for (int i = 0; i < this.m_TrailRenders.Length; i++)
			{
				this.m_TrailRenders[i].Clear();
			}
			Gradient gradient = new Gradient();
			if (colors != null && colors.Length >= 2)
			{
				int num = colors.Length;
				GradientColorKey[] array = new GradientColorKey[num];
				GradientAlphaKey[] array2 = new GradientAlphaKey[num];
				for (int j = 0; j < num; j++)
				{
					float time2;
					if (j == 0)
					{
						time2 = 1f;
					}
					else if (j == num - 1)
					{
						time2 = 0f;
					}
					else
					{
						time2 = 1f - (float)j * (1f / (float)(num - 1));
					}
					array[j].color = new Color(colors[j].r, colors[j].g, colors[j].b, 1f);
					array[j].time = time2;
					array2[j].alpha = colors[j].a;
					array2[j].time = time2;
				}
				gradient.SetKeys(array, array2);
			}
			for (int k = 0; k < this.m_TrailRenders.Length; k++)
			{
				this.m_TrailRenders[k].colorGradient = gradient;
				this.m_TrailRenders[k].startWidth = startWidth;
				this.m_TrailRenders[k].endWidth = endWidth;
				this.m_TrailRenders[k].time = time;
			}
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x0004FC74 File Offset: 0x0004E074
		public void Setup(eElementType elementType)
		{
			for (int i = 0; i < this.m_TrailRenders.Length; i++)
			{
				this.m_TrailRenders[i].enabled = (i == (int)elementType);
			}
		}

		// Token: 0x06000ED3 RID: 3795 RVA: 0x0004FCAC File Offset: 0x0004E0AC
		public void SetSortingOrder(int sortingOrder)
		{
			for (int i = 0; i < this.m_TrailRenders.Length; i++)
			{
				this.m_TrailRenders[i].sortingOrder = sortingOrder;
			}
		}

		// Token: 0x040015D5 RID: 5589
		[SerializeField]
		private TrailRenderer[] m_TrailRenders;
	}
}
