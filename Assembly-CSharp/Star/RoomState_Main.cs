﻿using System;
using Star.UI.Global;

namespace Star
{
	// Token: 0x02000475 RID: 1141
	public class RoomState_Main : RoomState
	{
		// Token: 0x06001643 RID: 5699 RVA: 0x000747A0 File Offset: 0x00072BA0
		public RoomState_Main(RoomMain owner) : base(owner)
		{
		}

		// Token: 0x06001644 RID: 5700 RVA: 0x000747B0 File Offset: 0x00072BB0
		public override int GetStateID()
		{
			return 1;
		}

		// Token: 0x06001645 RID: 5701 RVA: 0x000747B3 File Offset: 0x00072BB3
		public override void OnStateEnter()
		{
			this.m_Step = RoomState_Main.eStep.First;
		}

		// Token: 0x06001646 RID: 5702 RVA: 0x000747BC File Offset: 0x00072BBC
		public override void OnStateExit()
		{
		}

		// Token: 0x06001647 RID: 5703 RVA: 0x000747BE File Offset: 0x00072BBE
		public override void OnDispose()
		{
		}

		// Token: 0x06001648 RID: 5704 RVA: 0x000747C0 File Offset: 0x00072BC0
		public override int OnStateUpdate()
		{
			RoomState_Main.eStep step = this.m_Step;
			if (step != RoomState_Main.eStep.First)
			{
				if (step == RoomState_Main.eStep.Main)
				{
					if (this.m_Owner.RoomUI.IsMenuEnd())
					{
						if (this.m_Owner.NextSceneID != SceneDefine.eSceneID.None)
						{
							this.m_NextState = 2147483646;
							return this.m_NextState;
						}
						if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
						{
							this.m_NextState = 2147483646;
						}
						else
						{
							this.m_NextState = 2147483646;
							this.m_Owner.NextTransitSceneID = SceneDefine.eSceneID.Town;
						}
						return this.m_NextState;
					}
				}
			}
			else
			{
				this.m_Step = RoomState_Main.eStep.Main;
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			}
			return -1;
		}

		// Token: 0x06001649 RID: 5705 RVA: 0x0007488E File Offset: 0x00072C8E
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Room)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				this.GoToMenuEnd();
			}
		}

		// Token: 0x0600164A RID: 5706 RVA: 0x000748CD File Offset: 0x00072CCD
		private void GoToMenuEnd()
		{
			this.m_Owner.RoomUI.PlayOut();
		}

		// Token: 0x04001CF1 RID: 7409
		private RoomState_Main.eStep m_Step = RoomState_Main.eStep.None;

		// Token: 0x02000476 RID: 1142
		private enum eStep
		{
			// Token: 0x04001CF3 RID: 7411
			None = -1,
			// Token: 0x04001CF4 RID: 7412
			First,
			// Token: 0x04001CF5 RID: 7413
			Main,
			// Token: 0x04001CF6 RID: 7414
			End
		}
	}
}
