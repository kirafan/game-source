﻿using System;

namespace Star
{
	// Token: 0x02000676 RID: 1654
	public class TownCameraKeyPakage
	{
		// Token: 0x0600217D RID: 8573 RVA: 0x000B2600 File Offset: 0x000B0A00
		public TownCameraKeyPakage()
		{
			this.m_Table = new ITownCameraKey[8];
		}

		// Token: 0x0600217E RID: 8574 RVA: 0x000B2614 File Offset: 0x000B0A14
		public void EntryAction(ITownCameraKey paction, int fuid)
		{
			if (this.m_Num < 8)
			{
				this.m_Table[this.m_Num] = paction;
				paction.m_UID = fuid;
				this.m_Num++;
			}
			else
			{
				paction = null;
			}
		}

		// Token: 0x0600217F RID: 8575 RVA: 0x000B2650 File Offset: 0x000B0A50
		public ITownCameraKey GetAction(int fuid)
		{
			ITownCameraKey result = null;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (this.m_Table[i].m_UID == fuid)
				{
					result = this.m_Table[i];
					break;
				}
			}
			return result;
		}

		// Token: 0x06002180 RID: 8576 RVA: 0x000B2698 File Offset: 0x000B0A98
		public bool PakageUpdate()
		{
			int num = 0;
			for (int i = 0; i < this.m_Num; i++)
			{
				if (!this.m_Table[i].PartsUpdate())
				{
					this.m_Table[i] = null;
					num++;
				}
			}
			if (num != 0)
			{
				int i = 0;
				while (i < this.m_Num)
				{
					if (this.m_Table[i] == null)
					{
						for (int j = i + 1; j < this.m_Num; j++)
						{
							this.m_Table[j - 1] = this.m_Table[j];
						}
						this.m_Num--;
						this.m_Table[this.m_Num] = null;
					}
					else
					{
						i++;
					}
				}
			}
			return this.m_Num != 0;
		}

		// Token: 0x06002181 RID: 8577 RVA: 0x000B2760 File Offset: 0x000B0B60
		public void Relase()
		{
			for (int i = 0; i < 8; i++)
			{
				this.m_Table[i] = null;
			}
			this.m_Table = null;
		}

		// Token: 0x040027DB RID: 10203
		public int m_Num;

		// Token: 0x040027DC RID: 10204
		public ITownCameraKey[] m_Table;
	}
}
