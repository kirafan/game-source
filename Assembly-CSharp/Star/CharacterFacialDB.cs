﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000171 RID: 369
	public class CharacterFacialDB : ScriptableObject
	{
		// Token: 0x040009CB RID: 2507
		public CharacterFacialDB_Param[] m_Params;
	}
}
