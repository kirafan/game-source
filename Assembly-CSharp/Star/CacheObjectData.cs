﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020004DE RID: 1246
	[Serializable]
	public class CacheObjectData
	{
		// Token: 0x1700018F RID: 399
		// (get) Token: 0x060018BD RID: 6333 RVA: 0x00080E9F File Offset: 0x0007F29F
		public GameObject Prefab
		{
			get
			{
				return this.m_Prefab;
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x060018BE RID: 6334 RVA: 0x00080EA7 File Offset: 0x0007F2A7
		public int CacheSize
		{
			get
			{
				return this.m_CacheSize;
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x060018BF RID: 6335 RVA: 0x00080EAF File Offset: 0x0007F2AF
		public GameObject[] CacheObjects
		{
			get
			{
				return this.m_Objects;
			}
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x060018C0 RID: 6336 RVA: 0x00080EB8 File Offset: 0x0007F2B8
		// (remove) Token: 0x060018C1 RID: 6337 RVA: 0x00080EF0 File Offset: 0x0007F2F0
		public event Action<GameObject> OnFindActiveObject;

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x060018C2 RID: 6338 RVA: 0x00080F28 File Offset: 0x0007F328
		// (remove) Token: 0x060018C3 RID: 6339 RVA: 0x00080F60 File Offset: 0x0007F360
		public event Action<GameObject> OnInstantiateObject;

		// Token: 0x060018C4 RID: 6340 RVA: 0x00080F96 File Offset: 0x0007F396
		public void Initialize(GameObject prefab, int cacheSize, Transform parent)
		{
			this.m_Prefab = prefab;
			this.m_CacheSize = cacheSize;
			this.Initialize(parent);
		}

		// Token: 0x060018C5 RID: 6341 RVA: 0x00080FB0 File Offset: 0x0007F3B0
		public void Initialize(Transform parent)
		{
			this.m_Objects = new GameObject[this.m_CacheSize];
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				this.m_Objects[i] = UnityEngine.Object.Instantiate<GameObject>(this.m_Prefab, parent, false);
				this.m_Objects[i].SetActive(false);
				this.m_Objects[i].name = this.m_Objects[i].name + i;
				if (this.OnInstantiateObject != null)
				{
					this.OnInstantiateObject(this.m_Objects[i]);
				}
			}
		}

		// Token: 0x060018C6 RID: 6342 RVA: 0x0008104C File Offset: 0x0007F44C
		public void Destroy()
		{
			if (this.m_Objects != null)
			{
				for (int i = 0; i < this.m_Objects.Length; i++)
				{
					UnityEngine.Object.Destroy(this.m_Objects[i]);
					this.m_Objects[i] = null;
				}
				this.m_Objects = null;
			}
			this.m_Prefab = null;
			this.m_CacheSize = 0;
			this.m_CacheIndex = 0;
		}

		// Token: 0x060018C7 RID: 6343 RVA: 0x000810B0 File Offset: 0x0007F4B0
		public GameObject GetNextObjectInCache()
		{
			GameObject gameObject = null;
			for (int i = 0; i < this.m_CacheSize; i++)
			{
				gameObject = this.m_Objects[i];
				if (gameObject != null && !gameObject.activeSelf)
				{
					break;
				}
				this.m_CacheIndex = (this.m_CacheIndex + 1) % this.m_CacheSize;
			}
			if (gameObject != null && gameObject.activeSelf && this.OnFindActiveObject != null)
			{
				this.OnFindActiveObject(gameObject);
			}
			this.m_CacheIndex = (this.m_CacheIndex + 1) % this.m_CacheSize;
			return gameObject;
		}

		// Token: 0x04001F5D RID: 8029
		[SerializeField]
		private GameObject m_Prefab;

		// Token: 0x04001F5E RID: 8030
		[SerializeField]
		private int m_CacheSize = 1;

		// Token: 0x04001F5F RID: 8031
		private GameObject[] m_Objects;

		// Token: 0x04001F60 RID: 8032
		private int m_CacheIndex;
	}
}
