﻿using System;
using Star.UI.Edit;
using Star.UI.Global;

namespace Star
{
	// Token: 0x020003FD RID: 1021
	public class EditState_CharaListUpgrade : EditState
	{
		// Token: 0x0600136B RID: 4971 RVA: 0x00067D1D File Offset: 0x0006611D
		public EditState_CharaListUpgrade(EditMain owner) : base(owner)
		{
		}

		// Token: 0x0600136C RID: 4972 RVA: 0x00067D35 File Offset: 0x00066135
		public override int GetStateID()
		{
			return 8;
		}

		// Token: 0x0600136D RID: 4973 RVA: 0x00067D38 File Offset: 0x00066138
		public override void OnStateEnter()
		{
			this.m_Step = EditState_CharaListUpgrade.eStep.First;
		}

		// Token: 0x0600136E RID: 4974 RVA: 0x00067D41 File Offset: 0x00066141
		public override void OnStateExit()
		{
		}

		// Token: 0x0600136F RID: 4975 RVA: 0x00067D43 File Offset: 0x00066143
		public override void OnDispose()
		{
		}

		// Token: 0x06001370 RID: 4976 RVA: 0x00067D48 File Offset: 0x00066148
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_CharaListUpgrade.eStep.First:
				if (this.m_Owner.CharaListUI != null)
				{
					this.m_Step = EditState_CharaListUpgrade.eStep.PlayIn;
				}
				else
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
					this.m_Step = EditState_CharaListUpgrade.eStep.LoadWait;
				}
				break;
			case EditState_CharaListUpgrade.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_CharaListUpgrade.eStep.PlayIn;
				}
				break;
			case EditState_CharaListUpgrade.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.m_Step = EditState_CharaListUpgrade.eStep.Main;
				}
				break;
			case EditState_CharaListUpgrade.eStep.Main:
				if (this.m_Owner.CharaListUI.IsMenuEnd())
				{
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						CharaListUI.eButton selectButton = this.m_Owner.CharaListUI.SelectButton;
						if (selectButton != CharaListUI.eButton.Chara)
						{
							this.m_NextState = 1;
						}
						else
						{
							SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam.SelectedCharaMngID = this.m_Owner.CharaListUI.SelectCharaMngID;
							this.m_NextState = 9;
						}
					}
					this.m_Step = EditState_CharaListUpgrade.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x06001371 RID: 4977 RVA: 0x00067EA8 File Offset: 0x000662A8
		public bool SetupAndPlayUI()
		{
			this.m_Owner.CharaListUI = UIUtility.GetMenuComponent<CharaListUI>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_Owner.CharaListUI == null)
			{
				return false;
			}
			this.m_Owner.CharaListUI.SetUpgradeMode();
			this.m_Owner.CharaListUI.Setup();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.Open();
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.CharaList);
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.m_Owner.CharaListUI.PlayIn();
			return true;
		}

		// Token: 0x06001372 RID: 4978 RVA: 0x00067F63 File Offset: 0x00066363
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			if (isCallFromShortCut && SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SelectedShortCutButton == GlobalUI.eShortCutButton.Upgrade)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.ResetSelectedShortCut();
			}
			else
			{
				base.OnClickBackButton(isCallFromShortCut);
				this.GoToMenuEnd();
			}
		}

		// Token: 0x06001373 RID: 4979 RVA: 0x00067FA1 File Offset: 0x000663A1
		private void GoToMenuEnd()
		{
			this.m_Owner.CharaListUI.PlayOut();
		}

		// Token: 0x04001A5C RID: 6748
		private EditState_CharaListUpgrade.eStep m_Step = EditState_CharaListUpgrade.eStep.None;

		// Token: 0x04001A5D RID: 6749
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.CharaListUI;

		// Token: 0x020003FE RID: 1022
		private enum eStep
		{
			// Token: 0x04001A5F RID: 6751
			None = -1,
			// Token: 0x04001A60 RID: 6752
			First,
			// Token: 0x04001A61 RID: 6753
			LoadWait,
			// Token: 0x04001A62 RID: 6754
			PlayIn,
			// Token: 0x04001A63 RID: 6755
			Main
		}
	}
}
