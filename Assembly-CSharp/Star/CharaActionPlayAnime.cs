﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000530 RID: 1328
	public class CharaActionPlayAnime : RoomActionScriptPlay, ICharaRoomAction
	{
		// Token: 0x06001A8E RID: 6798 RVA: 0x0008D5A8 File Offset: 0x0008B9A8
		public void RequestObjEventMode(IRoomObjectControll roomObjHndl, RoomObjectCtrlChara pOwner, RoomEventParameter psetup)
		{
			this.m_PartsAction = new RoomPartsActionPakage();
			base.SetUpPlayScene(roomObjHndl, pOwner, psetup, new RoomActionScriptPlay.ActionSciptCallBack(this.CallbackActionEvent), this.m_PartsAction);
			this.m_Base.SetDir(RoomUtility.CalcObjectLockDir(roomObjHndl, pOwner.GetHandle()));
			if (this.m_ActionScript != null)
			{
				if (this.m_ActionScript.m_LinkDir == 0)
				{
					this.m_Target.SetLinkObject(this.m_Base, true);
				}
				else if (this.m_ActionScript.m_LinkDir == 1)
				{
					this.m_Base.SetLinkObject(this.m_Target, true);
				}
				this.m_Target.SetActionSendObj(this.m_Base);
				this.m_Base.SetActionSendObj(this.m_Target);
			}
			this.m_TargetTrs = base.CalcTargetTrs();
		}

		// Token: 0x06001A8F RID: 6799 RVA: 0x0008D678 File Offset: 0x0008BA78
		public bool UpdateObjEventMode(RoomObjectCtrlChara hbase)
		{
			bool flag = base.PlayScene(hbase);
			this.m_PartsAction.PakageUpdate();
			if (!flag && this.m_ActionScript != null)
			{
				if (this.m_ActionScript.m_LinkDir == 0)
				{
					this.m_Target.SetLinkObject(this.m_Base, false);
				}
				else if (this.m_ActionScript.m_LinkDir == 1)
				{
					this.m_Base.SetLinkObject(this.m_Target, false);
				}
			}
			return flag;
		}

		// Token: 0x06001A90 RID: 6800 RVA: 0x0008D6F4 File Offset: 0x0008BAF4
		private bool CallbackActionEvent(CActScriptKeyProgramEvent pcmd)
		{
			int eventID = pcmd.m_EventID;
			if (eventID != 0)
			{
				if (eventID != 1)
				{
					if (eventID == 2)
					{
						Vector3 backTagMark = this.m_BackTagMark;
						if (this.m_MarkPoint)
						{
							this.m_Base.SetDir(CharacterDefine.eDir.L);
						}
						else
						{
							this.m_Base.SetDir(CharacterDefine.eDir.R);
						}
						this.m_MoveKeyTime = pcmd.m_EvtTime;
						RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, backTagMark, pcmd.m_EvtTime, RoomPartsMove.ePathType.Linear);
						this.m_PartsAction.EntryAction(paction, 0U);
					}
				}
			}
			else
			{
				if (this.m_Base.trs.position.x >= this.m_Target.trs.position.x)
				{
					this.m_MarkPoint = true;
				}
				else
				{
					this.m_MarkPoint = false;
				}
				this.m_BackTagMark = this.m_Owner.CacheTransform.position;
				RoomPartsMove paction = new RoomPartsMove(this.m_Owner.CacheTransform, this.m_Owner.CacheTransform.position, this.m_TargetTrs.position, pcmd.m_EvtTime, RoomPartsMove.ePathType.Linear);
				this.m_PartsAction.EntryAction(paction, 0U);
				this.m_MoveKeyTime = pcmd.m_EvtTime;
			}
			return true;
		}

		// Token: 0x06001A91 RID: 6801 RVA: 0x0008D850 File Offset: 0x0008BC50
		public bool EventCancel(RoomObjectCtrlChara hbase, bool fcancel)
		{
			if (fcancel)
			{
				base.CancelPlay();
				this.m_PartsAction.CancelAction();
				if (this.m_ActionScript != null)
				{
					if (this.m_ActionScript.m_LinkDir == 0)
					{
						this.m_Target.SetLinkObject(this.m_Base, false);
					}
					else if (this.m_ActionScript.m_LinkDir == 1)
					{
						this.m_Base.SetLinkObject(this.m_Target, false);
					}
				}
			}
			return true;
		}

		// Token: 0x06001A92 RID: 6802 RVA: 0x0008D8CA File Offset: 0x0008BCCA
		public void Release()
		{
			base.ReleasePlay();
			this.m_PartsAction = null;
			this.m_TargetTrs = null;
		}

		// Token: 0x04002103 RID: 8451
		private Transform m_TargetTrs;

		// Token: 0x04002104 RID: 8452
		private bool m_MarkPoint;

		// Token: 0x04002105 RID: 8453
		private Vector3 m_BackTagMark;
	}
}
