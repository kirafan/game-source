﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000214 RID: 532
	public class SoundCueSheetDB : ScriptableObject
	{
		// Token: 0x04000D3B RID: 3387
		public SoundCueSheetDB_Param[] m_Params;
	}
}
