﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Star
{
	// Token: 0x0200037C RID: 892
	public class SystemScheduleLinkManager
	{
		// Token: 0x060010F8 RID: 4344 RVA: 0x000594F4 File Offset: 0x000578F4
		public static SystemScheduleLinkManager GetManager()
		{
			if (SystemScheduleLinkManager.Inst == null)
			{
				SystemScheduleLinkManager.Inst = new SystemScheduleLinkManager();
				SystemScheduleLinkManager.Inst.CreateCashFileName();
				SystemScheduleLinkManager.Inst.ReadBackUpFile();
			}
			return SystemScheduleLinkManager.Inst;
		}

		// Token: 0x060010F9 RID: 4345 RVA: 0x00059524 File Offset: 0x00057924
		private void CreateCashFileName()
		{
			string s = "TRLocalCash.dat";
			this.m_CashFileName = Application.persistentDataPath + "/" + Convert.ToBase64String(Encoding.ASCII.GetBytes(s)) + ".www";
		}

		// Token: 0x060010FA RID: 4346 RVA: 0x00059564 File Offset: 0x00057964
		public static long GetChkUpPlayTime()
		{
			UserFieldCharaData userFieldChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara;
			long num = userFieldChara.ScheduleTime;
			if (num == 0L)
			{
				num = ScheduleTimeUtil.GetSystemTimeSec();
			}
			return num;
		}

		// Token: 0x060010FB RID: 4347 RVA: 0x00059598 File Offset: 0x00057998
		public static void ChkUpPlayTime(long fsettingtime)
		{
			UserFieldCharaData userFieldChara = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara;
			if (userFieldChara.ScheduleTime != 0L && ScheduleTimeUtil.GetDayKey(userFieldChara.ScheduleTime) != ScheduleTimeUtil.GetDayKey(fsettingtime))
			{
				SystemScheduleLinkManager.Inst.ClearReserveSave();
				SystemScheduleLinkManager.Inst.SaveBackUpFile();
			}
			userFieldChara.ScheduleTime = fsettingtime;
		}

		// Token: 0x060010FC RID: 4348 RVA: 0x000595F4 File Offset: 0x000579F4
		public void ReadBackUpFile()
		{
			if (File.Exists(this.m_CashFileName))
			{
				BinaryFileReader binaryFileReader = new BinaryFileReader();
				if (binaryFileReader.OpenFile(this.m_CashFileName))
				{
					binaryFileReader.ReadFile();
					SystemScheduleLinkManager.FileCliperHeader fileCliperHeader = new SystemScheduleLinkManager.FileCliperHeader();
					fileCliperHeader.Serialize(binaryFileReader);
					if (fileCliperHeader.CliperChk(binaryFileReader))
					{
						this.ReadCustomCashFile(binaryFileReader);
					}
					binaryFileReader.CloseFile();
				}
			}
		}

		// Token: 0x060010FD RID: 4349 RVA: 0x00059654 File Offset: 0x00057A54
		private void ReadCustomCashFile(BinaryReader preader)
		{
			SystemScheduleLinkManager.FileHeader fileHeader = new SystemScheduleLinkManager.FileHeader();
			fileHeader.Serialize(preader);
			for (;;)
			{
				preader.PushPoint(preader.m_FilePoint);
				SystemScheduleLinkManager.eSaveTagKey key = (SystemScheduleLinkManager.eSaveTagKey)fileHeader.m_Key;
				if (key != SystemScheduleLinkManager.eSaveTagKey.End)
				{
					if (key != SystemScheduleLinkManager.eSaveTagKey.TimeKey)
					{
						if (key == SystemScheduleLinkManager.eSaveTagKey.ActionTag)
						{
							SystemScheduleLinkManager.RoomActionChara roomActionChara = new SystemScheduleLinkManager.RoomActionChara();
							roomActionChara.Serialize(preader);
							this.m_RoomActionKey.Add(roomActionChara);
						}
					}
					else
					{
						SystemScheduleLinkManager.SystemTimeKey systemTimeKey = new SystemScheduleLinkManager.SystemTimeKey();
						systemTimeKey.Serialize(preader);
					}
				}
				preader.PopPoint();
				preader.Offset(fileHeader.m_Size);
				if (preader.IsEOF() || fileHeader.m_Flag == 0)
				{
					break;
				}
				fileHeader.Serialize(preader);
			}
		}

		// Token: 0x060010FE RID: 4350 RVA: 0x00059708 File Offset: 0x00057B08
		public void SaveBackUpFile()
		{
			BinaryWrite binaryWrite = new BinaryWrite();
			SystemScheduleLinkManager.FileCliperHeader fileCliperHeader = new SystemScheduleLinkManager.FileCliperHeader();
			fileCliperHeader.Serialize(binaryWrite);
			this.WriteCustomCashFile(binaryWrite);
			fileCliperHeader.MakeCliper(binaryWrite);
			SystemFileWriteManager.GetManager().EntryFileWrite(this.m_CashFileName, fileCliperHeader.m_Data);
		}

		// Token: 0x060010FF RID: 4351 RVA: 0x0005974C File Offset: 0x00057B4C
		private void WriteCustomCashFile(BinaryWrite pwriter)
		{
			SystemScheduleLinkManager.FileHeader fileHeader = new SystemScheduleLinkManager.FileHeader();
			fileHeader.SetTagKey(SystemScheduleLinkManager.eSaveTagKey.TimeKey, 8, 1);
			fileHeader.Serialize(pwriter);
			new SystemScheduleLinkManager.SystemTimeKey
			{
				m_Times = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserFieldChara.ScheduleTime
			}.Serialize(pwriter);
			for (int i = 0; i < this.m_RoomActionKey.Count; i++)
			{
				fileHeader.SetTagKey(SystemScheduleLinkManager.eSaveTagKey.ActionTag, 12, 1);
				fileHeader.Serialize(pwriter);
				this.m_RoomActionKey[i].Serialize(pwriter);
			}
			fileHeader.SetTagKey(SystemScheduleLinkManager.eSaveTagKey.End, 0, 0);
			fileHeader.Serialize(pwriter);
		}

		// Token: 0x06001100 RID: 4352 RVA: 0x000597E8 File Offset: 0x00057BE8
		public bool IsReserveChara(long fmanageid)
		{
			int count = this.m_ReserveTable.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_ReserveTable[i].m_ManageID == fmanageid)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001101 RID: 4353 RVA: 0x00059830 File Offset: 0x00057C30
		public void SaveReserveChara(long fmanageid)
		{
			int count = this.m_ReserveTable.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_ReserveTable[i].m_ManageID == fmanageid)
				{
					return;
				}
			}
			SystemScheduleLinkManager.RoomReserveChara roomReserveChara = new SystemScheduleLinkManager.RoomReserveChara();
			roomReserveChara.m_ManageID = fmanageid;
			this.m_ReserveTable.Add(roomReserveChara);
		}

		// Token: 0x06001102 RID: 4354 RVA: 0x0005988C File Offset: 0x00057C8C
		public bool IsActionChara(int factionkey)
		{
			int count = this.m_RoomActionKey.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_RoomActionKey[i].m_ActionKey == factionkey)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06001103 RID: 4355 RVA: 0x000598D4 File Offset: 0x00057CD4
		public void SaveActionChara(long fmanageid, int factionkey)
		{
			int count = this.m_RoomActionKey.Count;
			for (int i = 0; i < count; i++)
			{
				if (this.m_RoomActionKey[i].m_ManageID == fmanageid && this.m_RoomActionKey[i].m_ActionKey == factionkey)
				{
					return;
				}
			}
			SystemScheduleLinkManager.RoomActionChara roomActionChara = new SystemScheduleLinkManager.RoomActionChara();
			roomActionChara.m_ManageID = fmanageid;
			roomActionChara.m_ActionKey = factionkey;
			this.m_RoomActionKey.Add(roomActionChara);
			this.SaveBackUpFile();
		}

		// Token: 0x06001104 RID: 4356 RVA: 0x00059954 File Offset: 0x00057D54
		private void ClearReserveSave()
		{
			this.m_ReserveTable.Clear();
			this.m_RoomActionKey.Clear();
		}

		// Token: 0x06001105 RID: 4357 RVA: 0x0005996C File Offset: 0x00057D6C
		public int GetReserveIndex()
		{
			return this.m_ReserveIndexCount;
		}

		// Token: 0x06001106 RID: 4358 RVA: 0x00059974 File Offset: 0x00057D74
		public int GetReserveSubIndex()
		{
			return this.m_ReserveSubIndexCount;
		}

		// Token: 0x06001107 RID: 4359 RVA: 0x0005997C File Offset: 0x00057D7C
		public void SetReserveIndex(int findex, int fsubindex)
		{
			this.m_ReserveIndexCount = findex;
			this.m_ReserveSubIndexCount = findex;
		}

		// Token: 0x040017E3 RID: 6115
		public List<SystemScheduleLinkManager.RoomReserveChara> m_ReserveTable = new List<SystemScheduleLinkManager.RoomReserveChara>();

		// Token: 0x040017E4 RID: 6116
		public int m_ReserveIndexCount;

		// Token: 0x040017E5 RID: 6117
		public int m_ReserveSubIndexCount;

		// Token: 0x040017E6 RID: 6118
		public string m_CashFileName;

		// Token: 0x040017E7 RID: 6119
		public List<SystemScheduleLinkManager.RoomActionChara> m_RoomActionKey = new List<SystemScheduleLinkManager.RoomActionChara>();

		// Token: 0x040017E8 RID: 6120
		public static SystemScheduleLinkManager Inst;

		// Token: 0x0200037D RID: 893
		public class RoomReserveChara
		{
			// Token: 0x040017E9 RID: 6121
			public long m_ManageID;
		}

		// Token: 0x0200037E RID: 894
		public enum eSaveTagKey
		{
			// Token: 0x040017EB RID: 6123
			End,
			// Token: 0x040017EC RID: 6124
			TimeKey,
			// Token: 0x040017ED RID: 6125
			ActionTag
		}

		// Token: 0x0200037F RID: 895
		public class RoomActionChara
		{
			// Token: 0x0600110A RID: 4362 RVA: 0x0005999C File Offset: 0x00057D9C
			public void Serialize(BinaryIO pio)
			{
				pio.Long(ref this.m_ManageID);
				pio.Int(ref this.m_ActionKey);
			}

			// Token: 0x040017EE RID: 6126
			public long m_ManageID;

			// Token: 0x040017EF RID: 6127
			public int m_ActionKey;

			// Token: 0x040017F0 RID: 6128
			public const int VERSION = 0;
		}

		// Token: 0x02000380 RID: 896
		public class FileHeader
		{
			// Token: 0x0600110C RID: 4364 RVA: 0x000599BE File Offset: 0x00057DBE
			public void SetTagKey(SystemScheduleLinkManager.eSaveTagKey fkey, int fsize, byte flags)
			{
				this.m_Key = (short)fkey;
				this.m_Size = fsize;
				this.m_Flag = flags;
			}

			// Token: 0x0600110D RID: 4365 RVA: 0x000599D6 File Offset: 0x00057DD6
			public void Serialize(BinaryIO pio)
			{
				pio.Short(ref this.m_Key);
				pio.Byte(ref this.m_Ver);
				pio.Byte(ref this.m_Flag);
				pio.Int(ref this.m_Size);
			}

			// Token: 0x040017F1 RID: 6129
			public int m_Size;

			// Token: 0x040017F2 RID: 6130
			public short m_Key;

			// Token: 0x040017F3 RID: 6131
			public byte m_Flag;

			// Token: 0x040017F4 RID: 6132
			public byte m_Ver;
		}

		// Token: 0x02000381 RID: 897
		public class FileCliperHeader
		{
			// Token: 0x0600110F RID: 4367 RVA: 0x00059A10 File Offset: 0x00057E10
			public void Serialize(BinaryIO pio)
			{
				pio.Int(ref this.m_Size);
				short num = (short)this.m_KeyCode;
				pio.Short(ref num);
				this.m_KeyCode = (ushort)num;
				num = (short)this.m_ChkSum;
				pio.Short(ref num);
				this.m_ChkSum = (ushort)num;
			}

			// Token: 0x06001110 RID: 4368 RVA: 0x00059A5C File Offset: 0x00057E5C
			public void MakeCliper(BinaryWrite pio)
			{
				this.m_Data = pio.PackToByte();
				this.m_Size = this.m_Data.Length - 8;
				this.m_KeyCode = BinaryFileUtil.MakeCipherCode();
				this.m_ChkSum = BinaryFileUtil.CipherCode(this.m_Data, this.m_Size, this.m_KeyCode, 8);
				Buffer.BlockCopy(BitConverter.GetBytes(this.m_Size), 0, this.m_Data, 0, 4);
				Buffer.BlockCopy(BitConverter.GetBytes(this.m_KeyCode), 0, this.m_Data, 4, 2);
				Buffer.BlockCopy(BitConverter.GetBytes(this.m_ChkSum), 0, this.m_Data, 6, 2);
			}

			// Token: 0x06001111 RID: 4369 RVA: 0x00059AF9 File Offset: 0x00057EF9
			public bool CliperChk(BinaryReader pio)
			{
				return BinaryFileUtil.DecodeCode(pio.m_Buffer, this.m_Size, this.m_KeyCode, 8) == this.m_ChkSum;
			}

			// Token: 0x040017F5 RID: 6133
			public int m_Size;

			// Token: 0x040017F6 RID: 6134
			public ushort m_KeyCode;

			// Token: 0x040017F7 RID: 6135
			public ushort m_ChkSum;

			// Token: 0x040017F8 RID: 6136
			public byte[] m_Data;
		}

		// Token: 0x02000382 RID: 898
		public class SystemTimeKey
		{
			// Token: 0x06001113 RID: 4371 RVA: 0x00059B23 File Offset: 0x00057F23
			public void Serialize(BinaryIO pio)
			{
				pio.Long(ref this.m_Times);
			}

			// Token: 0x040017F9 RID: 6137
			public long m_Times;
		}
	}
}
