﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x020005B5 RID: 1461
	public class RoomCharaActIdle : IRoomCharaAct
	{
		// Token: 0x06001C54 RID: 7252 RVA: 0x000979E2 File Offset: 0x00095DE2
		public void RequestIdleMode(RoomObjectCtrlChara pbase, bool foveranm, float foffset = 1f)
		{
			this.m_Timer = UnityEngine.Random.Range(RoomDefine.CHARA_IDLE_PROCESS_TIME[0] * foffset, RoomDefine.CHARA_IDLE_PROCESS_TIME[1] * foffset);
			if (foveranm)
			{
				pbase.PlayMotion(RoomDefine.eAnim.Idle, WrapMode.Loop);
				pbase.SetBlink(true);
			}
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x00097A18 File Offset: 0x00095E18
		public override bool UpdateMode(RoomObjectCtrlChara pbase)
		{
			bool result = true;
			this.m_Timer -= Time.deltaTime;
			if (this.m_Timer <= 0f || pbase.IsIrqRequest())
			{
				pbase.SetBlink(false);
				result = false;
			}
			return result;
		}

		// Token: 0x0400233B RID: 9019
		private float m_Timer;
	}
}
