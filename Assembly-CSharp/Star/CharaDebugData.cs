﻿using System;

namespace Star
{
	// Token: 0x02000297 RID: 663
	[Serializable]
	public class CharaDebugData
	{
		// Token: 0x04001517 RID: 5399
		public int m_ManageID;

		// Token: 0x04001518 RID: 5400
		public int m_ResID;

		// Token: 0x04001519 RID: 5401
		public int m_Level;
	}
}
