﻿using System;
using System.Runtime.CompilerServices;
using Meige;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Star
{
	// Token: 0x02000698 RID: 1688
	public class TownDebugCmdWindow : DebugWindowBase
	{
		// Token: 0x060021D9 RID: 8665 RVA: 0x000B3CE0 File Offset: 0x000B20E0
		public static void OpenCheck()
		{
			if (TownDebugCmdWindow.ms_Handle == null)
			{
				string resourceName = "Prefab/UI/TownDebugCmdWindow.muast";
				if (TownDebugCmdWindow.<>f__mg$cache0 == null)
				{
					TownDebugCmdWindow.<>f__mg$cache0 = new Action<MeigeResource.Handler>(TownDebugCmdWindow.CallbackWindowMake);
				}
				MeigeResourceManager.LoadHandler(resourceName, TownDebugCmdWindow.<>f__mg$cache0, new MeigeResource.Option[]
				{
					MeigeResource.Option.ResourceKeep
				});
			}
			TownDebugCmdWindow.ms_ResetPos = true;
		}

		// Token: 0x060021DA RID: 8666 RVA: 0x000B3D30 File Offset: 0x000B2130
		private static void CallbackWindowMake(MeigeResource.Handler phandle)
		{
			TownDebugCmdWindow.ms_Handle = phandle;
			GameObject gameObject = phandle.GetAsset<GameObject>();
			gameObject = UnityEngine.Object.Instantiate<GameObject>(gameObject);
			gameObject.AddComponent<TownDebugCmdWindow>();
		}

		// Token: 0x060021DB RID: 8667 RVA: 0x000B3D58 File Offset: 0x000B2158
		private void Start()
		{
			base.WindowSetUp();
			Button button = DebugWindowBase.FindHrcObject(base.transform, "RoomButton", typeof(Button)) as Button;
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackRoomMake));
				if (UserRoomUtil.IsHaveSubRoom(SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserData.ID))
				{
					button.interactable = false;
				}
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "TownInit", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackTownClear));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "AreaOpen", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackAreaOpen));
				Text text = DebugWindowBase.FindHrcObject(base.transform, "AreaOpen/GameObject", typeof(Text)) as Text;
				if (text != null)
				{
					text.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level + 1 + "章 仮開放";
				}
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "TimeUp", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackTimeUp));
			}
			button = (DebugWindowBase.FindHrcObject(base.transform, "MissionRebuild", typeof(Button)) as Button);
			if (button != null)
			{
				button.onClick.AddListener(new UnityAction(this.CallbackMissionRebuild));
			}
			if (ScheduleTimeUtil.m_DebugSetUp)
			{
				this.m_TimeStep = true;
				this.m_TimeUp = (DebugWindowBase.FindHrcObject(base.transform, "TimeUp/GameObject", typeof(Text)) as Text);
			}
			Dropdown dropdown = DebugWindowBase.FindHrcObject(base.transform, "Dropdown", typeof(Dropdown)) as Dropdown;
			if (dropdown != null)
			{
				dropdown.onValueChanged.AddListener(new UnityAction<int>(this.OnTimeValueChanged));
				dropdown.options.Clear();
				dropdown.options.Add(new Dropdown.OptionData("x1"));
				dropdown.options.Add(new Dropdown.OptionData("x60"));
				dropdown.options.Add(new Dropdown.OptionData("x600"));
				dropdown.options.Add(new Dropdown.OptionData("x3600"));
				dropdown.options.Add(new Dropdown.OptionData("x7200"));
				if (!ScheduleTimeUtil.m_DebugSetUp)
				{
					dropdown.gameObject.SetActive(false);
				}
			}
		}

		// Token: 0x060021DC RID: 8668 RVA: 0x000B403C File Offset: 0x000B243C
		private void Update()
		{
			base.WindowUpdata();
			if (TownDebugCmdWindow.ms_ResetPos)
			{
				base.ResetWindowPos();
				TownDebugCmdWindow.ms_ResetPos = false;
			}
			if (this.m_TimeUp != null)
			{
				DateTime manageUnixTime = ScheduleTimeUtil.GetManageUnixTime();
				if (ScheduleTimeUtil.m_DebugSetUp)
				{
					this.m_TimeUp.text = string.Format("{0:00}時 {1:00}分 {2:00}秒", manageUnixTime.Hour, manageUnixTime.Minute, manageUnixTime.Second);
				}
				else
				{
					this.m_TimeUp.text = string.Format("{0}時", manageUnixTime.Hour);
				}
			}
		}

		// Token: 0x060021DD RID: 8669 RVA: 0x000B40E8 File Offset: 0x000B24E8
		public void CallbackRoomMake()
		{
			RoomComUnlockDefault roomComUnlockDefault = new RoomComUnlockDefault(1);
			roomComUnlockDefault.PlaySend();
		}

		// Token: 0x060021DE RID: 8670 RVA: 0x000B4104 File Offset: 0x000B2504
		public void CallbackTownClear()
		{
			TownComAllDelete townComAllDelete = new TownComAllDelete();
			townComAllDelete.PlaySend();
		}

		// Token: 0x060021DF RID: 8671 RVA: 0x000B4120 File Offset: 0x000B2520
		public void CallbackAreaOpen()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level++;
			if (SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level >= 10)
			{
				SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level = 10;
			}
			Text text = DebugWindowBase.FindHrcObject(base.transform, "AreaOpen/GameObject", typeof(Text)) as Text;
			if (text != null)
			{
				text.text = SingletonMonoBehaviour<GameSystem>.Inst.UserDataMng.UserTownData.Level + 1 + "章 仮開放";
			}
			TownComDebugSetUp townComDebugSetUp = new TownComDebugSetUp(0);
			townComDebugSetUp.PlaySend();
			GameObject gameObject = GameObject.Find("TownBuilder");
			if (gameObject != null)
			{
				TownBuilder component = gameObject.GetComponent<TownBuilder>();
				if (component != null)
				{
					component.ResetBuildPoint();
				}
			}
		}

		// Token: 0x060021E0 RID: 8672 RVA: 0x000B4210 File Offset: 0x000B2610
		public void CallbackTimeUp()
		{
			this.m_TimeUp = (DebugWindowBase.FindHrcObject(base.transform, "TimeUp/GameObject", typeof(Text)) as Text);
			if (!this.m_TimeStep)
			{
				ScheduleTimeUtil.SetDebugMode(true);
				ScheduleTimeUtil.SetDebugAddTime(0, 1, 0);
				this.m_TimeStep = true;
				Dropdown dropdown = DebugWindowBase.FindHrcObject(base.transform, "Dropdown", typeof(Dropdown)) as Dropdown;
				if (dropdown != null)
				{
					dropdown.gameObject.SetActive(true);
				}
			}
			else
			{
				ScheduleTimeUtil.SetDebugAddTime(0, 1, 0);
			}
		}

		// Token: 0x060021E1 RID: 8673 RVA: 0x000B42A7 File Offset: 0x000B26A7
		public void CallbackMissionRebuild()
		{
			MissionManager.RebuildMissionList();
		}

		// Token: 0x060021E2 RID: 8674 RVA: 0x000B42B0 File Offset: 0x000B26B0
		public void OnTimeValueChanged(int fresult)
		{
			switch (fresult)
			{
			case 0:
				ScheduleTimeUtil.SetDebugSpeed(1f);
				break;
			case 1:
				ScheduleTimeUtil.SetDebugSpeed(60f);
				break;
			case 2:
				ScheduleTimeUtil.SetDebugSpeed(600f);
				break;
			case 3:
				ScheduleTimeUtil.SetDebugSpeed(3600f);
				break;
			case 4:
				ScheduleTimeUtil.SetDebugSpeed(7200f);
				break;
			}
		}

		// Token: 0x060021E3 RID: 8675 RVA: 0x000B4327 File Offset: 0x000B2727
		private void OnDestroy()
		{
			if (TownDebugCmdWindow.ms_Handle != null)
			{
				if (SingletonMonoBehaviour<MeigeResourceManager>.Instance != null)
				{
					MeigeResourceManager.UnloadHandler(TownDebugCmdWindow.ms_Handle, false);
				}
				TownDebugCmdWindow.ms_Handle = null;
			}
		}

		// Token: 0x0400284E RID: 10318
		private static MeigeResource.Handler ms_Handle;

		// Token: 0x0400284F RID: 10319
		private static bool ms_ResetPos;

		// Token: 0x04002850 RID: 10320
		private bool m_TimeStep;

		// Token: 0x04002851 RID: 10321
		private Text m_TimeUp;

		// Token: 0x04002852 RID: 10322
		[CompilerGenerated]
		private static Action<MeigeResource.Handler> <>f__mg$cache0;
	}
}
