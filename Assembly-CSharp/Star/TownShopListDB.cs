﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000252 RID: 594
	public class TownShopListDB : ScriptableObject
	{
		// Token: 0x04001376 RID: 4982
		public TownShopListDB_Param[] m_Params;
	}
}
