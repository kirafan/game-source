﻿using System;
using Star.UI.Edit;
using Star.UI.Global;

namespace Star
{
	// Token: 0x02000410 RID: 1040
	public class EditState_PartyEditBase<UIType> : EditState where UIType : PartyEditUIBase
	{
		// Token: 0x060013CF RID: 5071 RVA: 0x00069BC0 File Offset: 0x00067FC0
		public EditState_PartyEditBase(EditMain owner) : base(owner)
		{
		}

		// Token: 0x060013D0 RID: 5072 RVA: 0x00069BD7 File Offset: 0x00067FD7
		public override int GetStateID()
		{
			return 2;
		}

		// Token: 0x060013D1 RID: 5073 RVA: 0x00069BDA File Offset: 0x00067FDA
		public virtual SceneDefine.eChildSceneID GetChildSceneId()
		{
			return SceneDefine.eChildSceneID.PartyEditUI;
		}

		// Token: 0x060013D2 RID: 5074 RVA: 0x00069BDD File Offset: 0x00067FDD
		public virtual int GetCharaListSceneId()
		{
			return -1;
		}

		// Token: 0x060013D3 RID: 5075 RVA: 0x00069BE0 File Offset: 0x00067FE0
		public override void OnStateEnter()
		{
			this.m_Step = EditState_PartyEditBase<UIType>.eStep.First;
		}

		// Token: 0x060013D4 RID: 5076 RVA: 0x00069BE9 File Offset: 0x00067FE9
		public override void OnStateExit()
		{
		}

		// Token: 0x060013D5 RID: 5077 RVA: 0x00069BEB File Offset: 0x00067FEB
		public override void OnDispose()
		{
			this.m_UI = (UIType)((object)null);
		}

		// Token: 0x060013D6 RID: 5078 RVA: 0x00069BFC File Offset: 0x00067FFC
		public override int OnStateUpdate()
		{
			switch (this.m_Step)
			{
			case EditState_PartyEditBase<UIType>.eStep.First:
				this.m_ChildSceneID = this.GetChildSceneId();
				SingletonMonoBehaviour<SceneLoader>.Inst.AdditiveChildSceneAsync(this.m_ChildSceneID, true);
				this.m_Step = EditState_PartyEditBase<UIType>.eStep.LoadWait;
				break;
			case EditState_PartyEditBase<UIType>.eStep.LoadWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteAdditiveChildScene(this.m_ChildSceneID))
				{
					SingletonMonoBehaviour<SceneLoader>.Inst.ActivationChildScene(this.m_ChildSceneID);
					this.m_Step = EditState_PartyEditBase<UIType>.eStep.PlayIn;
				}
				break;
			case EditState_PartyEditBase<UIType>.eStep.PlayIn:
				if (this.SetupAndPlayUI())
				{
					this.OpenTutorialTipsWindow();
					this.m_Step = EditState_PartyEditBase<UIType>.eStep.Main;
				}
				break;
			case EditState_PartyEditBase<UIType>.eStep.Main:
				if (this.m_UI.IsMenuEnd())
				{
					this.SetSelectedPartyIndex(this.m_UI.SelectPartyIndex);
					this.SetSelectedPartyMemberIndex(this.m_UI.SelectCharaIndex);
					this.SetSelectedCharaMngId(this.m_UI.SelectCharaMngID);
					if (SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.IsSelectedShortCut)
					{
						this.m_NextState = 2147483646;
					}
					else
					{
						PartyEditUIBase.eButton selectButton = this.m_UI.SelectButton;
						if (selectButton != PartyEditUIBase.eButton.Chara)
						{
							this.m_NextState = 1;
						}
						else
						{
							this.m_NextState = this.GetCharaListSceneId();
						}
					}
					SingletonMonoBehaviour<SceneLoader>.Inst.UnloadChildSceneAsync(this.m_ChildSceneID);
					this.m_Step = EditState_PartyEditBase<UIType>.eStep.UnloadChildSceneWait;
				}
				break;
			case EditState_PartyEditBase<UIType>.eStep.UnloadChildSceneWait:
				if (SingletonMonoBehaviour<SceneLoader>.Inst.IsCompleteUnloadChildScene(this.m_ChildSceneID))
				{
					this.m_Step = EditState_PartyEditBase<UIType>.eStep.None;
					return this.m_NextState;
				}
				break;
			}
			return -1;
		}

		// Token: 0x060013D7 RID: 5079 RVA: 0x00069DAC File Offset: 0x000681AC
		public bool SetupAndPlayUI()
		{
			this.m_UI = UIUtility.GetMenuComponent<UIType>(SceneDefine.CHILD_SCENE_INFOS[this.m_ChildSceneID].m_SceneName);
			if (this.m_UI == null)
			{
				return false;
			}
			this.m_UI.Setup(PartyEditUIBase.eMode.Edit, -1, null);
			this.m_UI.OnDecideNameChange += this.OnDecideNameChange;
			this.m_UI.OnClickButton += this.OnClickButton;
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetBackButtonCallBack(new Action<bool>(this.OnClickBackButton));
			this.SetSceneInfo();
			this.m_UI.PlayIn();
			return true;
		}

		// Token: 0x060013D8 RID: 5080 RVA: 0x00069E76 File Offset: 0x00068276
		protected virtual void SetSceneInfo()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.SetSceneInfo(GlobalSceneInfoWindow.eBackButtonIconType.Back, eSceneInfo.PartyEdit);
		}

		// Token: 0x060013D9 RID: 5081 RVA: 0x00069E89 File Offset: 0x00068289
		protected void GoToMenuEnd()
		{
			SingletonMonoBehaviour<GameSystem>.Inst.GlobalUI.CloseSceneInfo();
			this.m_UI.PlayOut();
		}

		// Token: 0x060013DA RID: 5082 RVA: 0x00069EAB File Offset: 0x000682AB
		public virtual int GetSelectedPartyIndex()
		{
			return -1;
		}

		// Token: 0x060013DB RID: 5083 RVA: 0x00069EAE File Offset: 0x000682AE
		public virtual void SetSelectedPartyIndex(int index)
		{
		}

		// Token: 0x060013DC RID: 5084 RVA: 0x00069EB0 File Offset: 0x000682B0
		public virtual void SetSelectedPartyMemberIndex(int index)
		{
		}

		// Token: 0x060013DD RID: 5085 RVA: 0x00069EB2 File Offset: 0x000682B2
		public virtual void SetSelectedCharaMngId(long charaMngId)
		{
		}

		// Token: 0x060013DE RID: 5086 RVA: 0x00069EB4 File Offset: 0x000682B4
		protected override void OnClickBackButton(bool isCallFromShortCut)
		{
			base.OnClickBackButton(isCallFromShortCut);
			this.m_UI.SelectButton = PartyEditUIBase.eButton.Back;
			GameGlobalParameter globalParam = SingletonMonoBehaviour<GameSystem>.Inst.GlobalParam;
			if (globalParam.IsDirtyPartyEdit)
			{
				this.RequestSetPartyAll();
			}
			else
			{
				this.GoToMenuEnd();
			}
		}

		// Token: 0x060013DF RID: 5087 RVA: 0x00069F01 File Offset: 0x00068301
		private void OnClickButton()
		{
			this.GoToMenuEnd();
		}

		// Token: 0x060013E0 RID: 5088 RVA: 0x00069F09 File Offset: 0x00068309
		protected void OnDecideNameChange(string partyName)
		{
			this.RequestSetPartyName(partyName);
		}

		// Token: 0x060013E1 RID: 5089 RVA: 0x00069F12 File Offset: 0x00068312
		protected virtual void RequestSetPartyAll()
		{
		}

		// Token: 0x060013E2 RID: 5090 RVA: 0x00069F14 File Offset: 0x00068314
		protected virtual void RequestSetPartyName(string partyName)
		{
		}

		// Token: 0x060013E3 RID: 5091 RVA: 0x00069F16 File Offset: 0x00068316
		protected virtual void OpenTutorialTipsWindow()
		{
		}

		// Token: 0x04001AC6 RID: 6854
		private EditState_PartyEditBase<UIType>.eStep m_Step = EditState_PartyEditBase<UIType>.eStep.None;

		// Token: 0x04001AC7 RID: 6855
		private SceneDefine.eChildSceneID m_ChildSceneID = SceneDefine.eChildSceneID.PartyEditUI;

		// Token: 0x04001AC8 RID: 6856
		public UIType m_UI;

		// Token: 0x04001AC9 RID: 6857
		protected string m_RequestedPartyName;

		// Token: 0x02000411 RID: 1041
		private enum eStep
		{
			// Token: 0x04001ACB RID: 6859
			None = -1,
			// Token: 0x04001ACC RID: 6860
			First,
			// Token: 0x04001ACD RID: 6861
			LoadWait,
			// Token: 0x04001ACE RID: 6862
			PlayIn,
			// Token: 0x04001ACF RID: 6863
			Main,
			// Token: 0x04001AD0 RID: 6864
			UnloadChildSceneWait
		}
	}
}
