﻿using System;
using System.IO;

namespace Star
{
	// Token: 0x02000645 RID: 1605
	public struct AnimSettings
	{
		// Token: 0x06001FCB RID: 8139 RVA: 0x000ABFCC File Offset: 0x000AA3CC
		public AnimSettings(string actionKey, string resoucePath)
		{
			this.m_ActionKey = actionKey;
			this.m_ResoucePath = resoucePath;
			if (!this.m_ResoucePath.Contains(".muast"))
			{
				this.m_ResoucePath += ".muast";
			}
			this.m_ObjectNameWithoutExt = Path.GetFileNameWithoutExtension(this.m_ResoucePath);
			this.m_IsPack = false;
		}

		// Token: 0x06001FCC RID: 8140 RVA: 0x000AC02C File Offset: 0x000AA42C
		public AnimSettings(string actionKey, string resoucePath, string objectNameWithoutExt)
		{
			this.m_ActionKey = actionKey;
			this.m_ResoucePath = resoucePath;
			if (!this.m_ResoucePath.Contains(".muast"))
			{
				this.m_ResoucePath += ".muast";
			}
			this.m_ObjectNameWithoutExt = objectNameWithoutExt;
			this.m_IsPack = true;
		}

		// Token: 0x04002646 RID: 9798
		public string m_ResoucePath;

		// Token: 0x04002647 RID: 9799
		public string m_ObjectNameWithoutExt;

		// Token: 0x04002648 RID: 9800
		public string m_ActionKey;

		// Token: 0x04002649 RID: 9801
		public bool m_IsPack;
	}
}
