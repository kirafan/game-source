﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000081 RID: 129
	[Serializable]
	public class ActionAggregationChildActionAdapter : MonoBehaviour
	{
		// Token: 0x060003FC RID: 1020 RVA: 0x00013ED2 File Offset: 0x000122D2
		public virtual void Setup()
		{
			this.SetState(eActionAggregationState.Wait);
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x00013EDB File Offset: 0x000122DB
		public virtual void Update()
		{
			if (this.GetState() == eActionAggregationState.Play)
			{
				this.SetState(eActionAggregationState.Finish);
			}
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x00013EF0 File Offset: 0x000122F0
		public virtual void PlayStart()
		{
			if (this.GetState() == eActionAggregationState.Wait)
			{
				this.PlayStartExtendProcess();
				this.SetState(eActionAggregationState.Play);
			}
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x00013F0A File Offset: 0x0001230A
		public virtual void PlayStartExtendProcess()
		{
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x00013F0C File Offset: 0x0001230C
		public virtual void Skip()
		{
			this.SetState(eActionAggregationState.Finish);
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x00013F15 File Offset: 0x00012315
		public virtual void RecoveryFinishToWait()
		{
			if (this.GetState() == eActionAggregationState.Finish)
			{
				this.RecoveryFinishToWaitExtendProcess();
				this.SetState(eActionAggregationState.Wait);
			}
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x00013F30 File Offset: 0x00012330
		public virtual void ForceRecoveryFinishToWait()
		{
			this.ForceRecoveryFinishToWaitExtendProcess();
			this.SetState(eActionAggregationState.Wait);
		}

		// Token: 0x06000403 RID: 1027 RVA: 0x00013F3F File Offset: 0x0001233F
		public virtual void RecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000404 RID: 1028 RVA: 0x00013F41 File Offset: 0x00012341
		public virtual void ForceRecoveryFinishToWaitExtendProcess()
		{
		}

		// Token: 0x06000405 RID: 1029 RVA: 0x00013F43 File Offset: 0x00012343
		public eActionAggregationState GetState()
		{
			return this.m_State;
		}

		// Token: 0x06000406 RID: 1030 RVA: 0x00013F4B File Offset: 0x0001234B
		protected void SetState(eActionAggregationState state)
		{
			this.m_State = state;
		}

		// Token: 0x06000407 RID: 1031 RVA: 0x00013F54 File Offset: 0x00012354
		public virtual bool IsFinished()
		{
			return true;
		}

		// Token: 0x0400022D RID: 557
		private eActionAggregationState m_State;
	}
}
