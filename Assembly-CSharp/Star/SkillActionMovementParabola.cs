﻿using System;
using UnityEngine;

namespace Star
{
	// Token: 0x02000128 RID: 296
	public class SkillActionMovementParabola : SkillActionMovementBase
	{
		// Token: 0x060007AB RID: 1963 RVA: 0x0002EE74 File Offset: 0x0002D274
		public SkillActionMovementParabola(Transform moveObj, bool isLocalPos, Vector3 startPos, Vector3 targetPos, float arrivalSec, float gravityAccel) : base(moveObj, isLocalPos)
		{
			this.m_StartPos = startPos;
			this.m_TargetPos = targetPos;
			this.m_ArrivalSec = arrivalSec;
			this.m_g = gravityAccel;
			this.m_t = 0f;
			float num = this.m_TargetPos.x - this.m_StartPos.x;
			float x = num / this.m_ArrivalSec;
			float num2 = this.m_TargetPos.y - this.m_StartPos.y;
			float y = (num2 + 0.5f * this.m_ArrivalSec * this.m_ArrivalSec * this.m_g) / this.m_ArrivalSec;
			float num3 = this.m_TargetPos.z - this.m_StartPos.z;
			float z = num3 / this.m_ArrivalSec;
			this.m_vMove = new Vector3(x, y, z);
			this.m_IsArrival = false;
			if (this.m_IsLocalPos)
			{
				this.m_MoveObj.localPosition = this.m_StartPos;
			}
			else
			{
				this.m_MoveObj.position = this.m_StartPos;
			}
		}

		// Token: 0x060007AC RID: 1964 RVA: 0x0002EFA0 File Offset: 0x0002D3A0
		public override bool Update(out int out_arrivalIndex, out Vector3 out_arrivalPos)
		{
			out_arrivalIndex = -1;
			out_arrivalPos = Vector3.zero;
			if (!this.m_IsMoving)
			{
				return false;
			}
			if (this.m_t < this.m_ArrivalSec)
			{
				this.m_t = Mathf.Min(this.m_t + Time.deltaTime, this.m_ArrivalSec);
				float num = this.m_t / this.m_ArrivalSec;
				num *= this.m_ArrivalSec;
				Vector3 b = new Vector3(this.m_vMove.x * num, (this.m_vMove.y - 0.5f * this.m_g * num) * num, this.m_vMove.z * num);
				Vector3 vector = this.m_StartPos + b;
				if (this.m_IsLocalPos)
				{
					this.m_MoveObj.localPosition = vector;
				}
				else
				{
					this.m_MoveObj.position = vector;
				}
			}
			if (this.m_t >= this.m_ArrivalSec)
			{
				this.m_IsMoving = false;
				if (!this.m_IsArrival)
				{
					out_arrivalIndex = 0;
					out_arrivalPos = this.m_TargetPos;
					this.m_IsArrival = true;
				}
			}
			return this.m_IsMoving;
		}

		// Token: 0x04000772 RID: 1906
		private Vector3 m_StartPos;

		// Token: 0x04000773 RID: 1907
		private Vector3 m_TargetPos;

		// Token: 0x04000774 RID: 1908
		private float m_ArrivalSec = 1f;

		// Token: 0x04000775 RID: 1909
		private float m_g = 9.8f;

		// Token: 0x04000776 RID: 1910
		private Vector3 m_vMove = Vector3.zero;

		// Token: 0x04000777 RID: 1911
		private bool m_IsArrival;

		// Token: 0x04000778 RID: 1912
		private float m_t;
	}
}
